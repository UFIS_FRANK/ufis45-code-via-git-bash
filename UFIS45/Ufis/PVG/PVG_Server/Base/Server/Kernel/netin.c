#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/PVG/PVG_Server/Base/Server/Kernel/netin.c 1.6 2005/10/18 18:08:40SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ******************************************************************** */
/*									*/
/* CEDA Program Skeleton						*/
/*									*/
/* Author		: Ralf Hundertmark				*/
/* Date			: 09/11/93					*/
/* Description		: Network Interface 				*/
/*									*/
/* Update history	:						*/
/* 20020628 JIM: test for memory leak: allocation should be done in que */
/* 20020826 JIM: debug_level=0 as default */
/* 20040109 JIM: 'int MAIN' kollidiert mit dem neuen DEFINE 'MAIN' fuer      */
/*               Versionsausgabe in Zeile 0 jeder Log-Datei                  */
/* 20051015 JIM: added DEBUGGING() to find hang in HP-UX / WMQ environment */
/*									*/
/* 20051018 JIM: WMQ only: before getting own queue reconnect child     */
/* ******************************************************************** */

static char sccs_version[]="@(#)UFIS 4.4 (c) ABB AAT/I netin.c 44.3 / 01/02/26 13:05:23 / BST";

/* This program is a CEDA main program */

#define U_MAIN

#define CEDA_PRG	/* ---              */
#define STH_USE

/* New netin with sgs.tab-EXTAB entry*/
#define USE_EXTAB_HOSTNAME

/* Send performance messages to the CEDAMO process */
#undef SUPPORT_CEDAMO

#ifdef _HPUX_SOURCE
#ifndef USE_BCHEAD_NETWORK_BYTE_ORDER
#define USE_BCHEAD_NETWORK_BYTE_ORDER
#endif
#else
#ifdef USE_BCHEAD_NETWORK_BYTE_ORDER
#undef USE_BCHEAD_NETWORK_BYTE_ORDER    
#endif
#endif

/* The master header file */
 
#include <signal.h>
#include <sys/socket.h>
#include <netinet/in.h>
#ifndef _WINNT
#include <arpa/inet.h>
#endif
#include  "netin.h"
#include  "tcputil.h"
#include  "bchdl.h"
#include  "monutl.h"
#include  "nmghdl.h"
#include  "new_catch.h"
#include  "debugrec.h"


#define UFIS_SERVICE_NAME    	"UFIS"
#define MAX_NO_OF_CONNECTIONS 	(20)
#define QUE_TIMEOUT 		(480)
#define TCP_TIMEOUT 		(10)

#ifdef MAX_EVENT_SIZE
#undef MAX_EVENT_SIZE
#endif
#define MAX_EVENT_SIZE 4096

/* Length for event struct */
#define	PACKETDATALEN  (PACKET_LEN-sizeof(EVENT))


/* ******************************************************************** */
/* External variables							*/
/* ******************************************************************** */
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE	*outp;*/
extern int	errno;
int debug_level = DEBUG;
extern long nap(long);

/* ******************************************************************** */
/* Global variables							*/
/* ******************************************************************** */

static	ITEM	*item = NULL;			/* The queue item pointer	*/
static	EVENT	*event;			/* The event pointer		*/
static  int	item_len;		/* length of incoming item 	*/
static  int	parent_mod_id;
static  int	tag = 30000;
static  struct	sockaddr_in	my_client;	/* IP addr of client */
static  struct in_addr *prgInAddr = NULL;
static	char cgMyIpAddr[64];
static	char cgUserName[64];
static	char cgWksName[64];
static	char cgApplName[64];
static  char cgMsgBuf[1024];
static  unsigned long		my_ip=0;	/* my IP addr  */
static  int	do_unreg=FALSE;	 /* Before Shutdown, send unreg msg to bchdl */

/* ******************************************************************** */
/* variables and defines used for security check */
/* ******************************************************************** */
#define			ISECUR_SIZE			128
#define			NETMASK_SIZE			10
#define			iMIN_SIZE			128
#define			iMAX_SIZE			8192
#define			IMAX_COUNT			3
#define			SGET_PRIVILEGES		"GPR"
#define			SCHECK_PERIOD		"CPE"
#define			SCHECK_PASSWORD		"CPW"
#define 		SVALID1				"C"	
#define 		SVAILD2				"N"
#define			SSECUR_DEFAULT		SVALID1
#define			SPASS_DEFAULT		"PASS"
static	int		igLoginOK   = 0;
static	int		igCounter   = 0;
static	int		igFirst     = 1;
static	int		igTerminate = 0;
static	char	prgSecurOption[ISECUR_SIZE];
static	char	pcgNetmask[NETMASK_SIZE];
static  unsigned int     igNetmask=0xffffff00;

/* ******************************************************************** */
/* External functions							*/
/* ******************************************************************** */

extern	void	catch_all();		/* Catches all signals		*/
extern  int	send_message(int,int,int,int,char*);
					/* CEDA message handler i/f    */
extern void snap (char *buf, int len, FILE *outp);

extern pid_t      getpid(void);
extern void       time_stamp(char *);
    
/* ******************************************************************** */
/* Function prototypes							*/
/* ******************************************************************** */

static int init_netin(void);
static void  terminate(int errcode);	/* Terminate program		*/
static void  handle_sig(int sig);	/* Handles signals		*/
static void  handle_qerr(int);		/* Handles queuing errors	*/
static int   run_server(void);
static int   server_child(int socket);
static void terminate_child (int socket,int errcode);
static void handle_queues();
static int forward_data (int sock, COMMIF *data);
static void poll_q_and_sock (int sock);
static void bchead_ntoh  (BC_HEAD *Pbc_head);
static void bchead_hton  (BC_HEAD *Pbc_head);
static int check_qcp (int sock);


/* ******************************************************************** */
/*									*/
/* The MAIN program							*/
/*									*/
/* ******************************************************************** */

/* 20040109 JIM: 'int MAIN' kollidiert mit dem neuen DEFINE 'MAIN', daher: */
MAIN
{
	int rc=RC_SUCCESS;
	int ilSig = 0;

	INITIALIZE;			/* General initialization	*/
	STH_INIT();

	rc = init_netin();
	if (rc == RC_SUCCESS)
	{
		SetSignals (handle_sig);
		ilSig = sigignore(SIGCHLD);
		dbg(TRACE,"MAIN: sigignore(SIGCHLD) returns <%d>; ERRNO=<%s>",ilSig,strerror(errno));


		while (TRUE)
		{
			switch ( ctrl_sta ) 
			{
			case	HSB_ACTIVE		:
				dbg(TRACE,"Now enter ACTIVE MODE ");
				rc = run_server();
				break;
			case	HSB_STANDALONE		:
				dbg(TRACE,"Now enter STAND ALONE MODE ");
				rc = run_server();
				break;
			case	HSB_DOWN		:
				/* wait for a state change */
				sleep(30);
				terminate(1);
				break;
			case	HSB_STANDBY		:
				/* wait for a state change */
				handle_queues();
				break;
			case	HSB_ACT_TO_SBY		:
				/* wait for a HSB_ACTIVE event */
				handle_queues();
				break;
			case	HSB_COMING_UP		:
				/* wait for a HSB_ACTIVE event */
				handle_queues();
				break;
			default			:
				break;
			} /* end switch */
		} /* while */
	} /* end if rc */
	terminate (1);
} /* end of main */
		
#ifdef WMQ
/* +++ test for hanging netin, last char must be '\' +++ */
#define DEBUGGING(msg) \
    dbg(DEBUG,msg) ;
#endif

#ifndef WMQ
#define DEBUGGING(msg) \
  ;
#endif

/* ******************************************************************** */
/*									*/
/* The SERVER program							*/
/*									*/
/* ******************************************************************** */

static int run_server(void)
{
  int	rc = RC_SUCCESS;	/* Return code			*/
  int	len;	
  int	sock;
  int	current_socket;
#ifndef _WINNT
  struct mallinfo mi;
#endif
  char buf[10];
	struct in_addr* prlInAddr = NULL;

  dbg(DEBUG,"Trying to initialize the server"); 

  sock = tcp_create_socket (SOCK_STREAM, UFIS_SERVICE_NAME);
  if (sock == -1 ) {
    dbg(TRACE,"NETIN: Error open socket");
    return RC_FAIL;
  } /* end if */

  listen (sock,MAX_NO_OF_CONNECTIONS);
  
  while (1) 
  {
    DEBUGGING("SERVER: before check_qcp");  /* +++ test for hanging netin +++*/
    rc = check_qcp(sock);
    DEBUGGING("SERVER: after check_qcp");  /* +++ test for hanging netin +++*/
    tag = ((getpid()%1000)+30000);

    len = sizeof(my_client);

/*
    mi = mallinfo();
    dbg(DEBUG,"SERVER: calling accept now:arena=%d ord=%d smb=%d hbl=%d\n"
	    "hbld=%d usm=%d fsm=%d uord=%d ford=%d keep=%d",
	    mi.arena, mi.ordblks, mi.smblks, mi.hblks, mi.hblkhd, mi.usmblks,
	    mi.fsmblks, mi.uordblks, mi.fordblks, mi.keepcost); 
 */
    DEBUGGING("SERVER: before setting alarm");  /* +++ test for hanging netin +++*/
    errno = 0;
    alarm(TCP_TIMEOUT);
    DEBUGGING("SERVER: before accept");  /* +++ test for hanging netin +++*/
    current_socket = accept(sock,(struct sockaddr*)&my_client, &len);
    DEBUGGING("SERVER: after accept...");   /* +++ test for hanging netin +++*/
    alarm(0);
    DEBUGGING("SERVER: alarm cleared...");  /* +++ test for hanging netin +++*/

    if ( current_socket < 0 ) 
    {
      if (errno == EINTR)
      {	/* Timeout */
	;  /*dbg(DEBUG,"SERVER: accept Timeout: %s:",strerror(errno));*/
      }
      else
      {
	dbg(TRACE,"SERVER: accept failed because of : %s:",strerror(errno));
      } /* fi */
      
      continue;
    } /* end if */

#ifdef ODT5
    dbg(DEBUG,"SERVER: accepted on socket %d ipaddr=%x",current_socket, ccs_ntohl(my_client.sin_addr.s_addr) );
#else
    dbg(DEBUG,"SERVER: accepted on socket %d ipaddr=%x",current_socket, ntohl(my_client.sin_addr.s_addr) );
#endif

	prlInAddr = (struct in_addr *) &my_client.sin_addr.s_addr;
	sprintf(cgMyIpAddr,"%s",inet_ntoa(*prlInAddr));

	dbg(DEBUG,"SERVER: Socket addr   <%s>",cgMyIpAddr);

    do_unreg = FALSE;

#ifdef ODT5
    if ( (ccs_ntohl(my_client.sin_addr.s_addr) & igNetmask) == (my_ip & igNetmask))
    {
      dbg(DEBUG,"SERVER: Client in same network CL:%x MY:%x", ccs_ntohl (my_client.sin_addr.s_addr) & igNetmask, my_ip & igNetmask);
    }    
    else
    {
      dbg(DEBUG, "run_server: Client NOT in same network. Sending message to bchdl\n" "\tCL:%x MY:%x", ccs_ntohl (my_client.sin_addr.s_addr) & igNetmask, my_ip & igNetmask);
      sprintf (buf, "%08x", ccs_ntohl (my_client.sin_addr.s_addr));
      do_unreg = TRUE;
      rc = tools_send_sql (1900, BC_REGISTER_CMD, "", buf, "", "");
    }    
#else
    if ( (ntohl(my_client.sin_addr.s_addr) & igNetmask) == (my_ip & igNetmask))
    {
      dbg(DEBUG,"SERVER: Client in same network CL:%x MY:%x", ntohl (my_client.sin_addr.s_addr) & igNetmask, my_ip & igNetmask);
    }    
    else
    {
      dbg(DEBUG, "run_server: Client NOT in same network. Sending message to bchdl\n" "\tCL:%x MY:%x", ntohl (my_client.sin_addr.s_addr) & igNetmask, my_ip & igNetmask);
      sprintf (buf, "%08x", ntohl (my_client.sin_addr.s_addr));
      do_unreg = TRUE;
      rc = tools_send_sql (1900, BC_REGISTER_CMD, "", buf, "", "");
    }    
#endif

#ifdef WMQ
    DEBUGGING("SERVER: before WmqDisconnect for fork...");  /* +++ test for hanging netin +++*/
    rc = WmqDisconnectFromQM(1,0,FALSE);
		dbg(TRACE,"%05d:RC from WmqDisconnectFromMQ = %d",__LINE__,rc);
#endif
    if (  fork() == 0 ) {
      close(sock);
      rc = server_child (current_socket);
      dbg(TRACE,"SERVER: This message must not appear !:");
      
      exit (-1);
    }
    DEBUGGING("SERVER: closing socket...");  /* +++ test for hanging netin +++*/
    close(current_socket);
    DEBUGGING("SERVER: before init_queue...");  /* +++ test for hanging netin +++*/
    while (rc = init_que()) {
      dbg (TRACE,"NETIN:Waiting for que creation %d", rc);
      sleep(5);		   /* Wait for QCP to create queues */
    } /* end while */
  } /* end while 1 */
} /* end of run_server */


/* ******************************************************************** */
/* The check_qcp routine						*/
/* ******************************************************************** */
static int check_qcp (int sock)
{
	int rc = RC_SUCCESS;
	int     len;
	/*rc = que(QUE_GETNW,0,mod_id,0,item_len,(char *)item);*/
	len=0;
/* 20020628 JIM: test for memory leak: allocation should be done in que */
/*	item=NULL; */
	rc = que(QUE_GETBIGNW,0,mod_id,0,len,(char *)&item);
        DEBUGGING("SERVER: after QUE_GETBIGNW");  /* +++ test for hanging netin +++*/
	if (rc == RC_SUCCESS) 
	{
		rc = que(QUE_ACK,0,mod_id,0,0,NULL);
		if ( rc != RC_SUCCESS ) 
		{
			handle_qerr(rc);
		} /* fi */
    		event=(EVENT*) item->text;
		switch (event->command) 
		{
		case	HSB_ACTIVE		:
			dbg(TRACE,"got HSB_ACTIVE returning to run_server");
			break;

		case	HSB_STANDALONE		:
			dbg(TRACE,"got HSB_STANDALONE returning to run_server");
			break;

		case	HSB_DOWN		:
			dbg(TRACE,"got HSB_DOWN calling terminate()");
			close(sock);
			terminate(RC_SHUTDOWN);
			break;

		case	HSB_STANDBY		:
			dbg(TRACE,"got HSB_STANDBY calling handle_queues()");
			close(sock);
			handle_queues();
			break;

		case	HSB_ACT_TO_SBY		:
			dbg(TRACE,"got HSB_ACT_TO_SBY calling handle_queues()");
			close(sock);
			handle_queues();
			break;

		case	HSB_COMING_UP		:
			dbg(TRACE,"got HSB_COMING_UP calling handle_queues()");
			close(sock);
			handle_queues();
			break;

		case TRACE_ON:  /* 7 */
		case TRACE_OFF: /* 8 */
			dbg_handle_debug(event->command);
			break;

		case	SHUTDOWN	:
			dbg(TRACE,"NETIN: Shutdown received ");
			close(sock);
			terminate(RC_SHUTDOWN);
			break;

		case	EVENT_DATA	:
			dbg(DEBUG,"NETIN: Eventdata received ");
			break;

		default			:
			break;

		} /* end switch */
    		
	} /* end if */
	else 
	{
		/* Handle queuing errors */
		if (rc != QUE_E_NOMSG)
		{
			handle_qerr(rc);
			sleep (5);
		} 
		else
		{
			rc = RC_SUCCESS;
		} /* fi */
	} /* fi */
    
	return rc;  

} /* check_qcp */

/* ******************************************************************** */
/*									*/
/* The SERVER_CHILD program  					        */
/*									*/
/* Handles one connection                                               */
/* Should never return							*/
/* ******************************************************************** */

static int server_child (int socket)
{
  int	rc = RC_SUCCESS;	/* Return code			*/
  int packet_len = NETREAD_SIZE;
  char *Pold_packet;
  COMMIF	*packet;		/* transport data */
  COMMIF	*pdata;			/* transport data */
  COMMIF	ack;
  char  	*cur_adr=NULL;
  char  	*cur_pdata=NULL;
  int cur_len=0;
  int p_len=0;

  /* Close and open new outp log file */
  REINITIALIZE

	/* fuer WMQ */

  packet = (COMMIF *) calloc(NETREAD_SIZE, 1);
  pdata  = (COMMIF *) calloc(PACKET_LEN, 1);  
  if (packet == NULL || pdata == NULL)
  {
    /* Fatal error */
    dbg(TRACE,"Server_child: Error at malloc");
    terminate_child (socket, -1);
  }

  cur_adr = (char *)(packet->data) ;
  packet ->length =0;
  ack.command = htons (PACKET_DATA);
  ack.length = htonl (sizeof(COMMIF));
      
#ifdef WMQ
    /* 20051018 JIM: before getting own queue reconnect child */
    DEBUGGING("CHILD: before WmqConnectToQM in fork...");  /* +++ test for hanging netin +++*/
    rc = WmqConnectToQM(-1,-2,FALSE);
		dbg(TRACE,"%05d: CHILD: RC from WmqConnectToQM = %d",__LINE__,rc);
#endif
  if ( que(QUE_CREATE,0,tag,0,6,"_NTIF") != RC_SUCCESS ) {
    terminate(-2);
  } /* end if */
  
  if ( que(QUE_RESP,0,tag,0,sizeof(ITEM),(char *)item) != RC_SUCCESS ) {
    terminate(-3);
  } /* end if */
  parent_mod_id = mod_id;	
  mod_id = item->originator;
  
	dbg(TRACE, "NETIN: mod_id <%d>",mod_id);

  while (1) {
    poll_q_and_sock (socket);
    cur_pdata = (char *) pdata;
    cur_len=0;
    p_len = PACKET_LEN;
    do 
    {
      if ((rc = read(socket,cur_pdata,PACKET_LEN)) <= 0 ) {
	dbg(TRACE, "NETIN: read err:sock=%d rc=%d : %s"
	    ,socket,rc,strerror(errno)); 
	terminate_child (socket, -1);
      } /* end if */
      dbg(DEBUG,"NETIN: after read rc=%d\t",rc);
      cur_pdata += rc;
      cur_len += rc;
      if (cur_len >= sizeof (COMMIF) )
      {                      /* How many bytes will come ? */
#ifdef ODT5
	p_len = (int) ccs_ntohl(pdata->length) + sizeof(COMMIF);
#else
	p_len = (int) ntohl(pdata->length) + sizeof(COMMIF);
#endif
      } /* fi */
      rc = RC_SUCCESS;
    } while (rc == RC_SUCCESS && cur_len < PACKET_LEN && cur_len < p_len);
    dbg(DEBUG,"NETIN: after packet read curlen=%d",cur_len);
      
    dbg(DEBUG,"NETIN: command <%d> length <%d>",pdata->command,pdata->length);
    dbg(DEBUG,"NETIN: command <%02x> length <%08x>",pdata->command,pdata->length);

#ifdef ODT5
    /* ntohs does not work on SCO ! vbl */
    dbg(DEBUG,"NETIN: using own routine ccs_ntohs and ccs_ntohl");
    pdata->command = ccs_ntohs(pdata->command);
    pdata->length = ccs_ntohl(pdata->length);
#else
    dbg(DEBUG,"NETIN: using UNIX's routine ntohs and ntohl");
    pdata->command = ntohs(pdata->command);
    pdata->length = ntohl(pdata->length);
#endif

    dbg(DEBUG,"NETIN: command <%d> length <%d>",pdata->command,pdata->length);
    dbg(DEBUG,"NETIN: command <%02x> length <%08x>",pdata->command,pdata->length);

    if (pdata->command != NET_SHUTDOWN)
    {
      rc = write(socket,&ack,sizeof(COMMIF));
      dbg(DEBUG,"NETIN: Wrote ack rc=%d\t",rc);
    
      if (rc == -1)
      {
	terminate_child (socket, -1);
      } /* end if */
    } /* end if */
    
    switch ( pdata->command ) 
    {
    case NET_SHUTDOWN:
      free(packet); free(pdata);
      terminate_child (socket, 0);
      break;
      
    case PACKET_DATA:
      dbg(DEBUG,"PDATA(%d)",packet->length); 
      
      packet->length += pdata->length;
      if (packet->length > packet_len)
      {
	packet_len += NETREAD_SIZE;
	Pold_packet = (char *) packet;
	packet = realloc (packet, packet_len);
	if (packet == NULL)
	{
	  /* Fatal error */
	  dbg(TRACE,"NETIN: Error at realloc");
	  terminate_child (socket, -1);
	}
	cur_adr = (char *) packet + (cur_adr - Pold_packet); /* adjust */
      }
      memcpy (cur_adr,pdata->data,pdata->length); 
      cur_adr += pdata->length;
      break;
      
    case PACKET_END:
      dbg(DEBUG,"PDATA_END(%d)",packet->length); 

      packet->length += pdata->length;
      if (packet->length > packet_len)
	{
	packet_len += NETREAD_SIZE;
	Pold_packet = (char *) packet;
	packet = realloc (packet, packet_len);
	if (packet == NULL)
	{
	  /* Fatal error */
	  dbg(TRACE,"NETIN: Error at realloc");
	  terminate_child (socket, -1);
	}
	cur_adr = (char *) packet + (cur_adr - Pold_packet); /* adjust */
      }

      memcpy(cur_adr,pdata->data,pdata->length); 
      packet->length += pdata->length;
      /*snap ((char *)packet,MIN(220,packet->length),outp);*/

      rc = forward_data(socket,packet); 
      packet_len = NETREAD_SIZE;
      packet = realloc (packet, packet_len);
      if (packet == NULL)
      {
	/* Fatal error */
	  terminate_child (socket, -1);
      }
      cur_adr = (char *)packet->data;
      /*	       memset(packet->data,0x00,8192);*/
      packet->length =0;
      break;
      
    default:
      dbg(TRACE,"Server_child: Unknown Command received: %d",
	      pdata->command); 
      /* Fatal error */
      if (debug_level == DEBUG)
      {
	snap ((char *)pdata, 1024, outp);
      }	/* fi */
      terminate_child (socket, -1);
      break;
    } /* end switch */
  } /* end while 1 */
  /* Never reached ... */
} /* end server_child */


/* ******************************************************************** */
/* The initialization routine						*/
/* Allocates the evenb buffer and inits the que                         */
/* Return: RC_SUCCESS, RC_FAIL                                          */
/* ******************************************************************** */

static int init_netin(void)
{
  int	rc = RC_SUCCESS;	/* Return code */
  int	ilRC;
  char my_name[64];
  struct hostent *hp;
  unsigned int ilTmp=0;

  dbg(TRACE,"INIT NETIN:\n%s",sccs_version);

  /* clear buffer */
  memset((void*)prgSecurOption, 0x00, ISECUR_SIZE);
  memset((void*)pcgNetmask, 0x00, NETMASK_SIZE);

  /* get entry from sgs.tab */
  if ((ilRC = tool_search_exco_data("NET","SECUR",
  				prgSecurOption)) != RC_SUCCESS)
  {
  		/* use default entry */
		strcpy(prgSecurOption, SSECUR_DEFAULT);
  }
  else
  {
  		/* in this case we found an entry in sgs.tab */
		/* check for valid data */
		if (strcmp(prgSecurOption, SVALID1) &&
			strcmp(prgSecurOption, SVAILD2))
		{
			/* not a valid data */
			strcpy(prgSecurOption, SSECUR_DEFAULT);
		}
  }
  dbg(DEBUG,"SecurOption: %s", prgSecurOption);

  if ((ilRC = tool_search_exco_data("NET","MASK",
  				pcgNetmask)) == RC_SUCCESS)
  {
    sscanf (pcgNetmask, "%x",&ilTmp);
    /* Simple plausi */
    if (ilTmp > 0xff000000)
    {
      igNetmask = ilTmp;
    } /* fi */
  } /* fi */
  dbg(DEBUG,"Netmask: %x", igNetmask);

  /* Allocate a dynamic buffer for max event size */
  item_len = MAX_EVENT_SIZE;
/* 20020628 JIM: test for memory leak: allocation should be done in que */
/*  item = (ITEM *) malloc(MAX_EVENT_SIZE); */
  
/*    if (item == (ITEM *) NULL) {		*/
    /* Handle malloc error */
/*      rc = RC_FAIL;
/*    } /end if */
  
  if (rc == RC_SUCCESS)
  {
/*      event = (EVENT *) item->text; * set event pointer            */
  
    /* Attach to the CEDA queues */
  
    while (rc = init_que()) {
      dbg (TRACE,"NETIN:Waiting for que creation %d", rc);
      sleep(5);		   /* Wait for QCP to create queues */
    } /* end while */
    
    rc = RC_SUCCESS;
  }

#ifdef USE_EXTAB_HOSTNAME
  rc = tool_search_exco_data ("NET","HOST",my_name);
  /* Fallback */
  if (rc != RC_SUCCESS)
  {
    dbg(TRACE,"No HOST entry in sgs.tab: EXTAB! Please add");
    rc = gethostname(my_name, 64);
  } /* fi */

#else
  if (rc == RC_SUCCESS)
  {
    rc = gethostname(my_name, 64);
  } /* fi */
#endif
  
  if (rc == RC_SUCCESS)
  {
    hp = gethostbyname( my_name);
    if (hp == NULL)
    {
      dbg(TRACE,"Init: Gethostbyname(%s): %s",	my_name,strerror(errno));
      rc = RC_FAIL;
    }
    else
    {
#ifdef ODT5
      dbg (DEBUG,"Handle_tcp_req: Got host info: name=%s len=%d adr1=%x" ,hp->h_name, hp->h_length, ccs_ntohl (*((int *) (hp->h_addr))) );
      my_ip = ccs_ntohl (*((int *) (hp->h_addr)));
#else
      dbg (DEBUG,"Handle_tcp_req: Got host info: name=%s len=%d adr1=%x" ,hp->h_name, hp->h_length, ntohl (*((int *) (hp->h_addr))) );
      my_ip = ntohl (*((int *) (hp->h_addr)));
#endif
    } /* end if */
  } /* end if */

  dbg(DEBUG,"NETIN: returning %d from init ", rc); 	  
  
  return rc;
} /* end of initialize */


/* ******************************************************************** */
/* The termination routine						*/
/* ******************************************************************** */

static void terminate(int errcode)
{
  free(item);

  dbg(TRACE,"NETIN: Now terminating %d  ", errcode); 	  
	
  exit(errcode);
} /* end of terminate */


/* ******************************************************************** */
/* The terminate_child routine						*/
/* ******************************************************************** */

static void terminate_child(int socket,int errcode)
{
	char buf[10];
	ITEM 		*prlItem=NULL;
	EVENT 	*prlEvent=NULL;
	int rc=RC_SUCCESS;

	if(get_system_state() != HSB_DOWN)
	{
		if (do_unreg)
		{
#ifdef ODT5
			dbg(DEBUG, "Terminate: Sending UNREG message to bchdl CL:%x MY:%x", ccs_ntohl (my_client.sin_addr.s_addr) & igNetmask, my_ip & igNetmask);
			sprintf (buf, "%08x", ccs_ntohl (my_client.sin_addr.s_addr));
#else
			dbg(DEBUG, "Terminate: Sending UNREG message to bchdl CL:%x MY:%x", ntohl (my_client.sin_addr.s_addr) & igNetmask, my_ip & igNetmask);
			sprintf (buf, "%08x", ntohl (my_client.sin_addr.s_addr));
#endif
			rc = tools_send_sql (1900, BC_UNREGISTER_CMD, "", buf, "", "");
		} /* fi */
  
		dbg(DEBUG,"Terminate_child: shutdown and close");
		if (socket > 0)
		{
			shutdown (socket,2);
			close(socket);
		}

		/* sleep(3); *** wait for possible hsb_message ***/

		dbg(DEBUG,"Terminate_child: try to get a last message");

		/*rc = que(QUE_GETNW,0,mod_id,PRIORITY_3,item_len,(char *)item);*/
    rc = que(QUE_GETBIGNW,0,mod_id,PRIORITY_3,0,(char *) &prlItem);
		if (rc == RC_SUCCESS)
		{
			rc = que(QUE_ACK,0,mod_id,0,0,NULL);

			dbg(TRACE,"Terminate_child: got a last message");

      prlEvent = (EVENT *) prlItem->text;
			DebugPrintItem(TRACE,prlItem);
			DebugPrintEvent(TRACE,prlEvent);
			free ((char *)prlItem);
		} /* end switch */

		dbg(TRACE,"Terminate_child: delete <%d>",mod_id);

		que(QUE_DELETE,mod_id,mod_id,0,0,0);  
	}

	debug_level = TRACE;
	sprintf(cgMsgBuf,"%s;%s;%s",cgUserName,cgMyIpAddr,cgApplName);
	dbg(TRACE,"LOGOUT: user <%s> wks <%s> ip <%s> appl <%s> queue <%d>",
                           cgUserName,cgWksName,cgMyIpAddr,cgApplName,mod_id);
	send_message(IPRIO_ASCII,NETIN_USER_LOGOUT,0,0,cgMsgBuf);

	dbg(TRACE,"Terminate_child: Exiting with %d", errcode);

#ifdef WMQ
  rc = WmqDisconnectFromQM(1,0,FALSE);
	dbg(TRACE,"%05d:RC from WmqDisconnectFromMQ = %d",__LINE__,rc);
#endif

	exit(errcode);
} /* end of terminate */


/* ******************************************************************** */
/* The handle signals routine						*/
/* ******************************************************************** */

static void handle_sig(int sig)
{
	int	rc = 0;			/* Return code */


	if (sig == SIGCHLD) {
	  dbg(TRACE,"NETIN: received signal %d. Call WAITPID.",sig);
	  waitpid(-1,(int *)NULL,WNOHANG);
	} /* end if */

	if (sig == SIGTERM) {
	  dbg(TRACE,"NETIN: received signal %d. Call TERMINATE_CHILD.",
		  sig);
	  terminate_child (-1, 0);
	} /* end if */

/* NEW */
	SetSignals (handle_sig);

#ifdef OLD
	if ( sig != SIGSEGV ) {
		signal(sig,handle_sig);
	} /* end if */
#endif


	if (sig != SIGALRM)
	{
	  dbg(TRACE,"NETIN: received signal %d. Reset signal. ",sig);
	  
	} /* fi */

		
	return ;
} /* end of handle_sig */

/* ******************************************************************** */
/* The handle queuing error routine					*/
/* ******************************************************************** */

static void handle_qerr(err)
int	err;				/* Error code */
{
	
	switch(err)
	{
	case	QUE_E_FUNC	:	/* Unknown function */
		dbg(TRACE,"<%d> : unknown function",err);
		break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
		dbg(TRACE,"<%d> : malloc failed",err);
		break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
		dbg(TRACE,"<%d> : msgsnd failed",err);
		break;
	case	QUE_E_GET	:	/* Error using msgrcv */
		dbg(TRACE,"<%d> : msgrcv failed",err);
		break;
	case	QUE_E_EXISTS	:
		dbg(TRACE,"<%d> : route/queue already exists ",err);
		break;
	case	QUE_E_NOFIND	:
		dbg(TRACE,"<%d> : route not found ",err);
		break;
	case	QUE_E_ACKUNEX	:
		dbg(TRACE,"<%d> : unexpected ack received ",err);
		break;
	case	QUE_E_STATUS	:
		dbg(TRACE,"<%d> :  unknown queue status ",err);
		break;
	case	QUE_E_INACTIVE	:
		dbg(TRACE,"<%d> : queue is inaktive ",err);
		break;
	case	QUE_E_MISACK	:
		dbg(TRACE,"<%d> : missing ack ",err);
		break;
	case	QUE_E_NOQUEUES	:
		dbg(TRACE,"<%d> : queue does not exist",err);
		break;
	case	QUE_E_RESP	:	/* No response on CREATE */
		dbg(TRACE,"<%d> : no response on create",err);
		break;
	case	QUE_E_FULL	:
		dbg(TRACE,"<%d> : too many route destinations",err);
		break;
	case	QUE_E_NOMSG	:	/* No message on queue */
		/*dbg(TRACE,"<%d> : no messages on queue",err);*/
		break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
		dbg(TRACE,"<%d> : invalid originator=0",err);
		break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
		dbg(TRACE,"<%d> : queues are not initialized",err);
		break;
	case	QUE_E_ITOBIG	:
		dbg(TRACE,"<%d> : requestet itemsize to big ",err);
		break;
	case	QUE_E_BUFSIZ	:
		dbg(TRACE,"<%d> : receive buffer to small ",err);
		break;
	default			:	/* Unknown queue error */
		dbg(TRACE,"<%d> : unknown error",err);
		break;
	}
	return;
} /* end of handle_qerr */

/* ******************************************************************** */
/* Following the forward data function					*/
/* ******************************************************************** */
static	int forward_data (int sock, COMMIF *data)
{
	int			len;
	int			rc;
	int 		t = 0;
	int 		do_ack=TRUE;		/* Ack-Mode */
	char		*offs;
	ITEM 		*Presult=NULL;
	ITEM 		*prlItem=NULL;
	EVENT		*out_event;
	EVENT 		*prlEvent=NULL;
	BC_HEAD 	*bchd;                  /* Broadcast header             */
	BC_HEAD 	*bchd_out;
	CMDBLK		*cmdblk;
	COMMIF  	*packet;
	time_t 		reqtim=0;

	/* *********************************************************** */
	/* variables used for performance and timeout check */
	/* *********************************************************** */
	int 	ilTist = 0;
	int 	ilNapOut = QUE_TIMEOUT;
	int 	ilTimOut = QUE_TIMEOUT / 4;

	/* *********************************************************** */
	/* variables and defines used for security check */
	/* *********************************************************** */
	int			ilRC;
	int			ilPassNotInList;
	char			*pclSelection = NULL;
	char			*pclFields    = NULL;
	char			pclUPRFields[iMAX_SIZE]; /* this is enough */
	char			pclUPRCommand[iMIN_SIZE];
	BC_HEAD		*prlBCHead    = NULL;
	CMDBLK		*prlCmdblk    = NULL;


  	/* Convert from network to host order */
  	bchead_ntoh ((BC_HEAD*)data);

	if (igFirst && !strcmp(prgSecurOption, SSECUR_DEFAULT))
	{
		/* the first time */	
		/* we have to check the commands */

		/* first set local pointers */
		prlBCHead    = (BC_HEAD*)((char*)data->data);
		prlCmdblk    = (CMDBLK*)((char*)prlBCHead->data);
		pclSelection = (char*)prlCmdblk->data;
		pclFields    = (char*)pclSelection + strlen(pclSelection) + 1;

		/* is PASS in list? */
		/* first clear memory */
		memset((void*)pclUPRCommand, 0x00, iMIN_SIZE);
		memset((void*)pclUPRFields, 0x00, iMAX_SIZE);

		/* copy to temporary buffer */
		strcpy(pclUPRCommand, prlCmdblk->command);
		strcpy(pclUPRFields, pclFields);

		/* convert to uppercase */
		dbg(DEBUG,"Found Fields: %s", pclFields);
		StringUPR((UCHAR*)pclUPRFields);
		StringUPR((UCHAR*)pclUPRCommand);
		dbg(DEBUG,"Found toupper(Fields): %s", pclFields);

		if (strstr(pclUPRFields, SPASS_DEFAULT) != NULL)
		{
			ilPassNotInList = 0;
		}else{
			ilPassNotInList = 1;
		}

		/* dbg(DEBUG,"Found pass not in list: %s", ilPassNotInList ? "pass is net drinn" : "pass is drinn"); */

		/* compare command */
		if (
			(strcmp(pclUPRCommand, SGET_PRIVILEGES)      &&
			 strcmp(pclUPRCommand, SCHECK_PERIOD)        &&
			 strcmp(pclUPRCommand, SCHECK_PASSWORD))     ||
			(!strcmp(pclUPRCommand, SGET_PRIVILEGES)     &&
			 ilPassNotInList)
		   )
		{
			/* not a valid command */
			igCounter++;

			/* how may tries? */
			if (igCounter >= IMAX_COUNT)
			{
				/* to many trys */
				/* shutdown connection */
				igFirst   = 1;
				igCounter = 0;

				packet = (COMMIF*)calloc(PACKET_LEN,1);
				packet->command = NET_SHUTDOWN;
				packet->length 	= 200;
				bchd_out        = (BC_HEAD*)packet->data;
				bchd_out->rc    = RC_FAIL;
				sprintf(bchd_out->data, "Authority error - too many tries");

				dbg(DEBUG,">>>>>>>>>  Start of Secur-Transmition <<<<<<<<");
				ilRC = tcp_send_data(packet->data, packet->length, sock);
				dbg(DEBUG,">>>>>>>>>  End of Secur-Transmition   <<<<<<<<");
				free((void*)packet);

				terminate_child (sock, -1);
			}
			else
			{
				/* only a message */
				packet = (COMMIF*)calloc(PACKET_LEN,1);
				packet->command = NET_DATA;
				packet->length 	= 200;
				bchd_out        = (BC_HEAD*)packet->data;
				bchd_out->rc    = RC_FAIL;
				sprintf(bchd_out->data, "Authority error - not a valid command (%s)", prlCmdblk->command);	

				dbg(DEBUG,">>>>>>>>>  Start of Secur-Transmition <<<<<<<<");
				ilRC = tcp_send_data(packet->data, packet->length, sock);
				dbg(DEBUG,">>>>>>>>>  End of Secur-Transmition   <<<<<<<<");
				free((void*)packet);
			}
			/* return */
			return RC_FAIL;
		}
	}

  	len = data->length+sizeof(EVENT);

  	if ((out_event = (EVENT*)calloc(len,1)) == NULL)
  	{
		dbg(TRACE,"forward_data: Malloc Error"); 
		return RC_FAIL;
  	} 

  	dbg(DEBUG,"Sending now from mod_id= %d",mod_id);

  	out_event->command 		= EVENT_DATA;
  	out_event->originator 	= (short) mod_id;
  	out_event->data_length 	= data->length;
  	out_event->data_offset 	= sizeof(EVENT);
  	offs = (char *)out_event + sizeof(EVENT);
  	memcpy (offs,data->data,data->length);

        /* dbg(TRACE,"OUT EVENT DATA_LENGTH=%d", out_event->data_length); */

  	bchd  = (BC_HEAD *) ((char *)out_event + out_event->data_offset);

	if ((bchd->orig_name[0] >= '0') && (bchd->orig_name[0] <= '9'))
	{
		ilTimOut = atoi(bchd->orig_name);
		if ((ilTimOut < 0) || (ilTimOut > 3600))
		{
			ilTimOut = QUE_TIMEOUT / 4;
        	} /* end if */
		ilNapOut = ilTimOut * 4;
        } /* end if */
	ilTist = time(0L) % 10000;
	sprintf(bchd->orig_name,"%d,%d",ilTist,ilTimOut);

  	rc = que(QUE_PUT,1200,mod_id,1,len,(char *)out_event);

  	if (bchd->rc == NETOUT_NO_ACK)
	{
    	do_ack = FALSE;
	}

	cmdblk = (CMDBLK *) ((char *)bchd->data);

	DebugPrintBchead(DEBUG,bchd);
	DebugPrintCmdblk(DEBUG,cmdblk);
      if (debug_level == DEBUG)
      {
		snap ((char *)bchd->data,512,outp);
	}
	strcpy(cgUserName,bchd->dest_name);
	cgUserName[sizeof(bchd->dest_name)] = 0x00;
	strcpy(cgWksName,bchd->recv_name);
	cgWksName[sizeof(bchd->recv_name)] = 0x00;
	get_real_item(cgApplName,cmdblk->tw_end,3);

	/* terminate flag */
	igTerminate = 0;

  	/* Shudd there B an ACK ?? ********************************************/
  	if (do_ack == TRUE)
		{
			int ilCount = 0;

    	dbg(DEBUG,"Waiting for answer");
    	reqtim = time(NULL);

    	/* t = QUE_TIMEOUT+1; */
    	t = ilNapOut+15;
    	do 
    	{
				if (ilCount++ < 15)
				{
					nap (20);
        }
				else
				{
dbg(DEBUG,"waiting,  %d tries remaining from max %d",t,ilNapOut+15);
      		nap (250);
        }
      		rc = que(QUE_GETBIGNW,0,mod_id,PRIORITY_3,0,(char *) &prlItem);
      		t--;
    	} while (t > 0 && rc == QUE_E_NOMSG);

    	if (rc == RC_SUCCESS) 
    	{
#ifdef SUPPORT_CEDAMO
      		/* *************** CEDAMON lines **********************/
      		reqtim = time(NULL) - reqtim;
      		dbg(DEBUG,"Received answer");

      		if (reqtim > 1)
			{
				(void) MonutlSendEvent(5800, "PER", "NRT", reqtim, 0, NULL);
			}
      		/* *************** CEDAMON lines end ******************/
#endif

      		/* Acknowledge the item */
      		rc = que(QUE_ACK,0,mod_id,0,0,NULL);
      		if ( rc != RC_SUCCESS ) 
			{
				handle_qerr(rc);
			}
  
      		prlEvent = (EVENT *) prlItem->text;

      		if ( prlEvent->command != EVENT_DATA ) 
			{
      			if (debug_level == DEBUG)
      			{
				snap((char *)prlItem,128,outp); fflush(outp);
			}
				dbg(TRACE,"WRONG EVENT COMMAND !!!!(%d)",prlEvent->command);
				free ((char *)prlItem);
				free(out_event);
				return -1;
      		}

			/* set BCHead-Pointer */
      		bchd  = (BC_HEAD *) ((char *)prlEvent + prlEvent->data_offset);

			/* returncode = RC_SUCCESS? */
			dbg(DEBUG,"Status first: %d, Counter: %d", igFirst, igCounter);
			dbg(DEBUG,"BCHead-returncode: %d", bchd->rc);

			cmdblk = (CMDBLK *) ((char *)bchd->data);

			if(igLoginOK == 0)
			{
				int tmpDbgLevel;

				tmpDbgLevel = debug_level;
				debug_level = TRACE;
				if((bchd->rc != RC_SUCCESS) && strcmp(pclUPRCommand, SGET_PRIVILEGES))
				{
					sprintf(cgMsgBuf,"%s;%s;%s",cgUserName,cgMyIpAddr,cgApplName);
					dbg(TRACE,"LOGIN: ERR user <%s> wks <%s> ip <%s> appl <%s> queue <%d>",cgUserName,cgWksName,cgMyIpAddr,cgApplName,mod_id);
					send_message(IPRIO_ASCII,NETIN_USER_LOGIN_ERR,0,0,cgMsgBuf);
				} else {
					sprintf(cgMsgBuf,"%s;%s;%s",cgUserName,cgMyIpAddr,cgApplName);
					dbg(TRACE,"LOGIN: OK  user <%s> wks <%s> ip <%s> appl <%s> queue <%d>",cgUserName,cgWksName,cgMyIpAddr,cgApplName,mod_id);
					send_message(IPRIO_ASCII,NETIN_USER_LOGIN_OK,0,0,cgMsgBuf);
					igLoginOK = 1;
				}
				debug_level = tmpDbgLevel;
			}

			/* test returncode */
			if ((igFirst == 1) && (bchd->rc != RC_SUCCESS) && (!strcmp(prgSecurOption, SSECUR_DEFAULT)))
			{
				igCounter++;
				if (igCounter >= IMAX_COUNT)
				{
					/* to many trys */
					/* shutdown connection */
					igFirst     = 1;
					igCounter   = 0;
					igTerminate = 1;

					packet = (COMMIF*)calloc(PACKET_LEN,1);
					packet->command = NET_SHUTDOWN;
					packet->length 	= 200;
					bchd_out        = (BC_HEAD*)packet->data;
					bchd_out->rc    = RC_FAIL;
					sprintf(bchd_out->data, "Authority error - too many tries (RC)");
				} else {
					/* only a message */
					packet = (COMMIF*)calloc(PACKET_LEN,1);
					packet->command = NET_DATA;
					packet->length 	= 200;
					bchd_out        = (BC_HEAD*)packet->data;
					bchd_out->rc    = RC_FAIL;
					strcpy(bchd_out->data, bchd->data);
					dbg(DEBUG,"%05d:packet->length=%d",__LINE__,packet->length);
					/*********
					sprintf(bchd_out->data, "Authority error - returncode(%d)", bchd->rc);	
					*********/
				}
			} else  {
				igFirst   = 0;
				igCounter = 0;

				len =sizeof(COMMIF)+ prlEvent->data_length;

				dbg(DEBUG,"Now malloc %d bytes for transfer", prlEvent->data_length);
		
				packet = (COMMIF *) calloc(len,1);
				packet->command = NET_DATA;
				packet->length = prlEvent->data_length;

/*dbg(TRACE,"rc=%d len=%d packet->length = %d 	prlEvent->data_length =%d",
	bchd->rc ,len, packet->length ,	prlEvent->data_length );*/

/*** set this only in sysqcp version. **/
#ifndef WMQ
if (bchd->rc != RC_SUCCESS)
{
		 packet->length = 411;
		prlEvent->data_length = 411;
}
#endif

				/* Convert from host to network byte order */
				bchead_hton (bchd);
				memcpy (packet->data,bchd,prlEvent->data_length);
				dbg(DEBUG,"%05d:sending %d bytes",__LINE__,prlEvent->data_length);
			}
    	} else {

#ifdef SUPPORT_CEDAMO
		  	/* *************** CEDAMON line **********************/
		  	(void) MonutlSendEvent(5800, "PER", "OUT", 0, 0, NULL);
		 	/* *************** CEDAMON line end ******************/
#endif

      		dbg(TRACE,"Error or Timeout ! Now malloc %d bytes for error msg", PACKET_LEN); 

      		packet = (COMMIF *) calloc(PACKET_LEN,1);
      		packet->command = NET_DATA;
      		packet->length = 200;
      		bchd_out  = (BC_HEAD *) packet->data;
      		bchd_out->rc = RC_FAIL;
      		strcpy (bchd_out->data, "QCP Error or Timeout");
    	} /* end else */

    	dbg(DEBUG,">>>>>>>>>>>>>>  Start of Transmition <<<<<<<<<<");
/*snap ((char *)packet->data,packet->length,outp);*/
    	rc = tcp_send_data(packet->data, packet->length, sock);
    	dbg(DEBUG,">>>>>>>>>>>>>>  End of Transmition   <<<<<<<<<<");

    	free ((char *)prlItem);
    	prlItem= NULL;
    	free(packet);

		if (igTerminate)
		{
			dbg(DEBUG,"now terminating child");
			terminate_child (sock, -1);
		} else {
			dbg(DEBUG,"child is going on");
		}
  	} 

	/* und tschuess */
  	free(out_event);
	return rc == RC_SUCCESS ? RC_SUCCESS : RC_FAIL;
} 


/* ******************************************************************** */
/* Following the poll_q_and_sock function				*/
/* Waits for input on the socket and polls the QCP for shutdown messages*/
/* Other QCP msg are discarded						*/
/* ******************************************************************** */
static void poll_q_and_sock (int sock)
{
  int rc = RC_SUCCESS;
	int len;
  do {
    rc = tcp_wait_timeout (sock, TCP_TIMEOUT);
    if (rc == RC_FAIL || rc == RC_NOT_FOUND)
    {
	len=0;
/* 20020628 JIM: test for memory leak: allocation should be done in que */
/*	item=NULL; */
      /*rc = que(QUE_GETNW,0,mod_id,PRIORITY_3,item_len,(char *)item);*/
      rc = que(QUE_GETBIGNW,0,mod_id,PRIORITY_3,len,(char *)&item);
      if (rc == RC_SUCCESS)
      {
	event=(EVENT*) item->text;
	rc = que(QUE_ACK,0,mod_id,0,0,NULL);
	switch (event->command) 
	{
	  case TRACE_ON:  /* 7 */
	  case TRACE_OFF: /* 8 */
	    dbg_handle_debug(event->command);
	    break;
	  case	HSB_ACT_TO_SBY	:
	    dbg(TRACE,"poll_q_and_sock: ACT_TO_SBY received");
	    
	    terminate_child (sock, -1);
	    break;
		
          case	SHUTDOWN	:
	    dbg(TRACE,"poll_q_and_sock: Shutdown received(2) ");
	    
	    terminate_child (sock, -1);
	    break;
	
	  default			:
	    break;
	} /* end switch */
      } /* fi RC_SUCCESS*/
      rc = RC_FAIL; /* dont return */
    } /* fi */
  } while (rc == RC_FAIL);
} /* poll_q_and_sock */


/* ******************************************************************** */
/* Following the bchead_ntoh function					*/
/* Converts from network to host order 					*/
/* ******************************************************************** */
static void bchead_ntoh  (BC_HEAD *Pbc_head)
{
#ifdef USE_BCHEAD_NETWORK_BYTE_ORDER

#ifdef ODT5
  Pbc_head->bc_num = ccs_ntohs (Pbc_head->bc_num);
  Pbc_head->tot_buf = ccs_ntohs (Pbc_head->tot_buf);
  Pbc_head->act_buf = ccs_ntohs (Pbc_head->act_buf);
  Pbc_head->rc = ccs_ntohs (Pbc_head->rc);
  Pbc_head->tot_size = ccs_ntohs (Pbc_head->tot_size);
  Pbc_head->cmd_size = ccs_ntohs (Pbc_head->cmd_size);
  Pbc_head->data_size = ccs_ntohs (Pbc_head->data_size);
#else
  Pbc_head->bc_num = ntohs (Pbc_head->bc_num);
  Pbc_head->tot_buf = ntohs (Pbc_head->tot_buf);
  Pbc_head->act_buf = ntohs (Pbc_head->act_buf);
  Pbc_head->rc = ntohs (Pbc_head->rc);
  Pbc_head->tot_size = ntohs (Pbc_head->tot_size);
  Pbc_head->cmd_size = ntohs (Pbc_head->cmd_size);
  Pbc_head->data_size = ntohs (Pbc_head->data_size);
#endif

#endif
}  /* bchead_ntohh */


/* ******************************************************************** */
/* Following the bchead_hton function					*/
/* Converts from network to host order 					*/
/* ******************************************************************** */
static void bchead_hton  (BC_HEAD *Pbc_head)
{
#ifdef USE_BCHEAD_NETWORK_BYTE_ORDER

  Pbc_head->bc_num = htons (Pbc_head->bc_num);
  Pbc_head->tot_buf = htons (Pbc_head->tot_buf);
  Pbc_head->act_buf = htons (Pbc_head->act_buf);
  Pbc_head->rc = htons (Pbc_head->rc);
  Pbc_head->tot_size = htons (Pbc_head->tot_size);
  Pbc_head->cmd_size = htons (Pbc_head->cmd_size);
  Pbc_head->data_size = htons (Pbc_head->data_size);

#endif
}  /* bchead_hton */

/* ******************************************************************** */
/* Following the handle queues function. This function waits for an     */
/* ctrl_sta status change. 						*/
/* ******************************************************************** */

static void handle_queues()
{
	int	rc	= RC_SUCCESS;

	for(;;)
	{
		dbg(TRACE,"handle_queues: QUE_GET");

		rc = que(QUE_GET,0,mod_id,PRIORITY_3,item_len,(char *)item);
		if(rc == RC_SUCCESS)
		{
			rc = que(QUE_ACK,0,mod_id,0,0,NULL);
		} /* end if */

		switch( event->command )
		{
		case	HSB_ACT_TO_SBY	:
			dbg(TRACE,"got HSB_ACT_TO_SBY");
			ctrl_sta = event->command;
			break;

		case	HSB_STANDBY	:
			dbg(TRACE,"got HSB_STANDBY");
			ctrl_sta = event->command;
			break;

		case	HSB_COMING_UP	:
			dbg(TRACE,"got HSB_COMING_UP");
			ctrl_sta = event->command;
			break;

		case	HSB_ACTIVE	:
			dbg(TRACE,"got HSB_ACTIVE");
			ctrl_sta = event->command;
			terminate(-9);
			break;

		case	HSB_STANDALONE	:
			dbg(TRACE,"got HSB_STANDALONE");
			ctrl_sta = event->command;
			terminate(-9);
			break;

		case	HSB_DOWN	:
			dbg(TRACE,"got HSB_DOWN");
			ctrl_sta = event->command;
			terminate(-9);
			break;

		case	SHUTDOWN	:
			dbg(TRACE,"got SHUTDOWN");
			terminate(RC_SHUTDOWN);
			break;

		default			:
			dbg(TRACE,"handle_queues: UNKNOWN CMD");
			break;

		} /* end switch */

	} /* end for */

} /* end of handle_queues */

/* ******************************************************************** */
/* Space for Appendings (Source-Code Control)				*/
/* ******************************************************************** */
/* ******************************************************************** */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

