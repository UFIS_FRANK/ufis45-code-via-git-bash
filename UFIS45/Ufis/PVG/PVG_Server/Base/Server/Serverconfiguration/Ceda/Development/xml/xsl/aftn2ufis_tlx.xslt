<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="text" media-type="text" version="1.0"/>
	<xsl:template match="MSG">
		<xsl:text>&lt;CMD&gt;FDI&lt;/CMD&gt;&lt;ROUTE&gt;8450&lt;/ROUTE&gt;&lt;SELECTION&gt;TELEX,13&lt;/SELECTION&gt;&lt;DATA&gt;</xsl:text>

		<xsl:value-of select="./FMO/TelexBody"/>

		<xsl:text>&lt;/DATA&gt;</xsl:text>	
		<xsl:text>&lt;FIELDS&gt;</xsl:text><xsl:value-of select="./FMO/URNO"/><xsl:text>&lt;/FIELDS&gt;</xsl:text>
		<xsl:text>&lt;WKS&gt;</xsl:text>EXCO<xsl:text>&lt;/WKS&gt;</xsl:text>
		<xsl:text>&lt;DEST&gt;</xsl:text>AFTN<xsl:text>&lt;/DEST&gt;</xsl:text>
		<xsl:text>&lt;TWSTART&gt;</xsl:text>AFTN<xsl:text>&lt;/TWSTART&gt;</xsl:text>
		<xsl:text>&lt;TWEND&gt;</xsl:text>PVG,TAB,AFTN<xsl:text>&lt;/TWEND&gt;</xsl:text>
		<xsl:text>&lt;BCORIGINATOR&gt;</xsl:text><xsl:text>&lt;/BCORIGINATOR&gt;</xsl:text>
		<xsl:text>&lt;BCNUM&gt;</xsl:text><xsl:text>&lt;/BCNUM&gt;</xsl:text>
		<xsl:text>&lt;SEQID&gt;</xsl:text><xsl:text>&lt;/SEQID&gt;</xsl:text>
		<xsl:text>&lt;TOTALBUF&gt;</xsl:text><xsl:text>&lt;/TOTALBUF&gt;</xsl:text>
		
		<xsl:text>&lt;ACTBUF&gt;</xsl:text><xsl:text>&lt;/ACTBUF&gt;</xsl:text>
		<xsl:text>&lt;RSEQID&gt;</xsl:text><xsl:text>&lt;/RSEQID&gt;</xsl:text>
		<xsl:text>&lt;RC&gt;</xsl:text><xsl:text>&lt;/RC&gt;</xsl:text>
		<xsl:text>&lt;TOTALSIZE&gt;</xsl:text><xsl:text>&lt;/TOTALSIZE&gt;</xsl:text>
		<xsl:text>&lt;CMDSIZE&gt;</xsl:text><xsl:text>&lt;/CMDSIZE&gt;</xsl:text>
		<xsl:text>&lt;DATASIZE&gt;</xsl:text><xsl:text>&lt;/DATASIZE&gt;</xsl:text>
		<xsl:text>&lt;OBJECT&gt;</xsl:text><xsl:text>&lt;/OBJECT&gt;</xsl:text>
		<xsl:text>&lt;ORDER&gt;</xsl:text><xsl:text>&lt;/ORDER&gt;</xsl:text>
		<xsl:text>&lt;MSGLENGTH&gt;</xsl:text><xsl:text>&lt;/MSGLENGTH&gt;</xsl:text>
		<xsl:text>&lt;ORIGINATOR&gt;</xsl:text>JXMLIF<xsl:text>&lt;/ORIGINATOR&gt;</xsl:text>
		<xsl:text>&lt;BLOCKNR&gt;</xsl:text><xsl:text>&lt;/BLOCKNR&gt;</xsl:text>
		<xsl:text>&lt;EVENT_COMMAND&gt;</xsl:text>1<xsl:text>&lt;/EVENT_COMMAND&gt;</xsl:text>
		<xsl:text>&lt;EVENT_TYPE&gt;</xsl:text><xsl:text>&lt;/EVENT_TYPE&gt;</xsl:text>
	
	</xsl:template>
</xsl:stylesheet>
