<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="text" media-type="text" version="1.0"/>
	<xsl:template match="MSG/FMO">
		<xsl:text>&lt;CMD&gt;</xsl:text>		<xsl:value-of select="./Action"/>	<xsl:text>&lt;/CMD&gt;</xsl:text>
		<xsl:text>&lt;ROUTE&gt;7800&lt;/ROUTE&gt;</xsl:text>

		<xsl:choose>		
			<xsl:when test="./Action='UFR'">
				<xsl:text>&lt;SELECTION&gt;WHERE URNO=</xsl:text>
				<xsl:value-of select="./URNO"/>
				<xsl:text>&lt;/SELECTION&gt;</xsl:text>
				<xsl:text>&lt;FIELDS&gt;</xsl:text>
			</xsl:when>

			<xsl:when test="./Action='IFR'">
				<xsl:text>&lt;SELECTION&gt;WHERE </xsl:text>
				<xsl:choose>

					<xsl:when test="./Arrive-Depart='A'">
						<xsl:value-of select="./SchArrivalDateTime/SchArrivalDate"/>
						<xsl:text>,</xsl:text>
						<xsl:value-of select="./SchArrivalDateTime/SchArrivalDate"/>
						<xsl:text>,1234567,1,</xsl:text>						
						<xsl:value-of select="./SchArrivalDateTime/SchArrivalDate"/>
					</xsl:when>

					<xsl:when test="./Arrive-Depart='D'">
						<xsl:value-of select="./SchDepartDateTime/SchDepartDate"/>
						<xsl:text>,</xsl:text>
						<xsl:value-of select="./SchDepartDateTime/SchDepartDate"/>
						<xsl:text>,1234567,1,</xsl:text>						
						<xsl:value-of select="./SchDepartDateTime/SchDepartDate"/>
					</xsl:when>		
							
				</xsl:choose>
				<xsl:text>&lt;/SELECTION&gt;</xsl:text>
				<xsl:text>&lt;FIELDS&gt;FLNO,</xsl:text>
		</xsl:when>
		</xsl:choose>
			<xsl:for-each select="*">
				<xsl:choose>
					<xsl:when test="name(.)='Airline'">ALC3,</xsl:when>
					<xsl:when test="name(.)='FlightNumber'">FLTN,</xsl:when>
					<xsl:when test="name(.)='FlightSuffix'">FLNS,</xsl:when>
					<xsl:when test="name(.)='CallSign'">CSGN,</xsl:when>
					<xsl:when test="name(.)='AircraftType'">ACT5,</xsl:when>
					<xsl:when test="name(.)='AircraftRegistrationNum'">REGN,</xsl:when>
					<xsl:when test="name(.)='FlightType'">TTYP,</xsl:when>
					<xsl:when test="name(.)='Origin'">ORG4,</xsl:when>
					<xsl:when test="name(.)='Destination'">DES4,</xsl:when>
					<xsl:when test="name(.)='Via'">VIAL,</xsl:when>
					<!--xsl:when test="name(.)='ViaCount'">VIAN,</xsl:when>-->
					<xsl:when test="name(.)='Arrive-Depart'">ADID,</xsl:when>
					<!--when test="name(.)='Status'">???xsl:when>-->
					<xsl:when test="name(.)='SchArrivalDateTime'">STOA,</xsl:when>
					<xsl:when test="name(.)='EstArrivalDateTime'">ETAE,</xsl:when>
					<xsl:when test="name(.)='ActArrivalDateTime'">LNDD,</xsl:when>
					<xsl:when test="name(.)='SchDepartDateTime'">STOD,</xsl:when>
					<xsl:when test="name(.)='EstDepartDateTime'">ETDE,</xsl:when>
					<xsl:when test="name(.)='ActDepartDateTime'">AIRD,</xsl:when>
					<xsl:when test="name(.)='DelayDateTime'">ETDA,</xsl:when>
				</xsl:choose>
			</xsl:for-each>
		<xsl:text>&lt;/FIELDS&gt;</xsl:text>
		<xsl:text>&lt;DATA&gt;</xsl:text>
		<xsl:if test="./Action='IFR'">
			<xsl:value-of select="normalize-space(./Airline)"/>
			<xsl:value-of select="normalize-space(./FlightNumber)"/>
			<xsl:value-of select="normalize-space(./FlightSuffix)"/>,</xsl:if>
			<xsl:for-each select="*">
				<xsl:choose>
					<xsl:when test="name(.)='Airline'"><xsl:value-of select="normalize-space(.)"/>,</xsl:when>
					<xsl:when test="name(.)='FlightNumber'"><xsl:value-of select="normalize-space(.)"/>,</xsl:when>
					<xsl:when test="name(.)='FlightSuffix'"><xsl:value-of select="normalize-space(.)"/>,</xsl:when>
					<xsl:when test="name(.)='CallSign'"><xsl:value-of select="normalize-space(.)"/>,</xsl:when>
					<xsl:when test="name(.)='AircraftType'"><xsl:value-of select="normalize-space(.)"/>,</xsl:when>
					<xsl:when test="name(.)='AircraftRegistrationNum'"><xsl:value-of select="normalize-space(.)"/>,</xsl:when>
					<xsl:when test="name(.)='FlightType'"><xsl:value-of select="normalize-space(.)"/>,</xsl:when>
					<xsl:when test="name(.)='Origin'"><xsl:value-of select="normalize-space(.)"/>,</xsl:when>
					<xsl:when test="name(.)='Destination'"><xsl:value-of select="normalize-space(.)"/>,</xsl:when>
					<xsl:when test="name(.)='Via'"><xsl:value-of select="concat('1   ',string(.))"/>,</xsl:when>
					<xsl:when test="name(.)='ViaCount'"><xsl:value-of select="normalize-space(.)"/>,</xsl:when>
					<xsl:when test="name(.)='Arrive-Depart'"><xsl:value-of select="normalize-space(.)"/>,</xsl:when>
					<!--when test="name(.)='Status'">???xsl:when>-->
					<xsl:when test="name(.)='SchArrivalDateTime'">
						<xsl:value-of select="normalize-space(./SchArrivalDate)"/>
						<xsl:value-of select="normalize-space(./SchArrivalTime)"/>,</xsl:when>
					<xsl:when test="name(.)='EstArrivalDateTime'"><xsl:value-of select="normalize-space(./EstArrivalDate)"/><xsl:value-of select="normalize-space(./EstArrivalTime)"/>,</xsl:when>
					<xsl:when test="name(.)='ActArrivalDateTime'"><xsl:value-of select="normalize-space(./ActArrivalDate)"/><xsl:value-of select="normalize-space(./ActArrivalTime)"/>,</xsl:when>
					<xsl:when test="name(.)='SchDepartDateTime'"><xsl:value-of select="normalize-space(./SchDepartDate)"/><xsl:value-of select="normalize-space(./SchDepartTime)"/>,</xsl:when>
					<xsl:when test="name(.)='EstDepartDateTime'"><xsl:value-of select="normalize-space(./EstDepartDate)"/><xsl:value-of select="normalize-space(./EstDepartTime)"/>,</xsl:when>
					<xsl:when test="name(.)='ActDepartDateTime'"><xsl:value-of select="normalize-space(./ActDepartDate)"/><xsl:value-of select="normalize-space(./ActDepartTime)"/>,</xsl:when>		
					<xsl:when test="name(.)='DelayDateTime'"><xsl:value-of select="normalize-space(./DelayDate)"/><xsl:value-of select="normalize-space(./DelayTime)"/>,</xsl:when>		
		
		
				</xsl:choose>
			</xsl:for-each>
		<xsl:text>&lt;/DATA&gt;</xsl:text>
		<xsl:text>&lt;WKS&gt;</xsl:text>EXCO<xsl:text>&lt;/WKS&gt;</xsl:text>
		<xsl:text>&lt;DEST&gt;</xsl:text>AFTN<xsl:text>&lt;/DEST&gt;</xsl:text>
		<xsl:text>&lt;TWSTART&gt;</xsl:text>AFTN<xsl:text>&lt;/TWSTART&gt;</xsl:text>
		<xsl:text>&lt;TWEND&gt;</xsl:text>PVG,TAB,AFTN<xsl:text>&lt;/TWEND&gt;</xsl:text>
		<xsl:text>&lt;BCORIGINATOR&gt;</xsl:text><xsl:text>&lt;/BCORIGINATOR&gt;</xsl:text>
		<xsl:text>&lt;BCNUM&gt;</xsl:text><xsl:text>&lt;/BCNUM&gt;</xsl:text>
		<xsl:text>&lt;SEQID&gt;</xsl:text><xsl:text>&lt;/SEQID&gt;</xsl:text>
		<xsl:text>&lt;TOTALBUF&gt;</xsl:text><xsl:text>&lt;/TOTALBUF&gt;</xsl:text>
		
		<xsl:text>&lt;ACTBUF&gt;</xsl:text><xsl:text>&lt;/ACTBUF&gt;</xsl:text>
		<xsl:text>&lt;RSEQID&gt;</xsl:text><xsl:text>&lt;/RSEQID&gt;</xsl:text>
		<xsl:text>&lt;RC&gt;</xsl:text><xsl:text>&lt;/RC&gt;</xsl:text>
		<xsl:text>&lt;TOTALSIZE&gt;</xsl:text><xsl:text>&lt;/TOTALSIZE&gt;</xsl:text>
		<xsl:text>&lt;CMDSIZE&gt;</xsl:text><xsl:text>&lt;/CMDSIZE&gt;</xsl:text>
		<xsl:text>&lt;DATASIZE&gt;</xsl:text><xsl:text>&lt;/DATASIZE&gt;</xsl:text>
		<xsl:text>&lt;OBJECT&gt;</xsl:text><xsl:text>&lt;/OBJECT&gt;</xsl:text>
		<xsl:text>&lt;ORDER&gt;</xsl:text><xsl:text>&lt;/ORDER&gt;</xsl:text>
		<xsl:text>&lt;MSGLENGTH&gt;</xsl:text><xsl:text>&lt;/MSGLENGTH&gt;</xsl:text>
		<xsl:text>&lt;ORIGINATOR&gt;</xsl:text>JXMLIF<xsl:text>&lt;/ORIGINATOR&gt;</xsl:text>
		<xsl:text>&lt;BLOCKNR&gt;</xsl:text><xsl:text>&lt;/BLOCKNR&gt;</xsl:text>
		<xsl:text>&lt;EVENT_COMMAND&gt;</xsl:text>1<xsl:text>&lt;/EVENT_COMMAND&gt;</xsl:text>
		<xsl:text>&lt;EVENT_TYPE&gt;</xsl:text><xsl:text>&lt;/EVENT_TYPE&gt;</xsl:text>	


	
	</xsl:template>


<!--
	<xsl:template name="HandleIFR">
	das ist mein template
	</xsl:template>



						<xsl:call-template name="HandleIFR">
					</xsl:call-template>
	<xsl:template name="HandleIFR">
			<xsl:for-each select="*"><xsl:choose>
				<xsl:when test="name(.)='AirlineOperator'">AIRL,</xsl:when>
				<xsl:when test="name(.)='FlightNumber'">FLTN,</xsl:when>
				<xsl:when test="name(.)='CallSign'">CSGN,</xsl:when>
			</xsl:for-each>
	</xsl:template>
-->

</xsl:stylesheet>


