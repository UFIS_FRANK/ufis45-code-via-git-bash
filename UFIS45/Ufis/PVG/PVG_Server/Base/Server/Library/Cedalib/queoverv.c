#ifndef _DEF_mks_version_queoverv_c
  #define _DEF_mks_version_queoverv_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_queoverv_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/PVG/PVG_Server/Base/Server/Library/Cedalib/queoverv.c 1.1 2005/04/08 16:31:17SGT mcu Exp  $";
#endif /* _DEF_mks_version */
static   char  sccs_queoverv_lib[] = "@(#) UFIS 4.4 (c) ABB AAT/I queoverv.c  44.4 / 00/01/07 14:39:00 / JMU";

#ifndef WMQ

#define	QUE_INTERNAL
#include <errno.h>
#include <string.h>
#include <malloc.h>
#include <memory.h>
#include <signal.h>

#include "glbdef.h"
#include "ugccsma.h"
#include "initdef.h"
#include "quedef.h"
#include "queutil.h"
#include "uevent.h"
#include "cli.h"

extern	int	mod_id, debug_level;

extern void	DeleteQueuesByName(char *pcpQueue)
{
	ITEM		*item;
	QUE_INFO	*que_info;
	QINFO	*qinfo;
	int		info_count;
	int		len;
	int		return_value;

	if ( pcpQueue == NULL ) {
		dbg(DEBUG,"DeleteQueuesByName: No Parameter");
		return;
	} /* end if */

	dbg(DEBUG,"DeleteQueuesByName: '%s'",pcpQueue);
	/* Request the queue overview */
	return_value = que(QUE_DEL_BY_NAME,0,mod_id,3,strlen(pcpQueue),pcpQueue);
	if ( return_value != RC_SUCCESS ) {
		dbg(TRACE,"DeleteQueuesByName: Calling 'QUE_DEL_BY_NAME' returns %d",return_value);
		return;
	} /* end if */
	return;
}

extern void	DeleteOldQueues(char *pcpQueue)
{
	ITEM		*item;
	QUE_INFO	*que_info;
	QINFO	*qinfo;
	int		info_count;
	int		len;
	int		return_value;

	if ( pcpQueue == NULL ) {
		dbg(DEBUG,"DeleteOldQueues: No Parameter");
		return;
	} /* end if */

	dbg(DEBUG,"DeleteOldQueues: '%s'",pcpQueue);
	/* Request the queue overview */
	return_value = que(QUE_QOVER,0,mod_id,3,0,0);
	if ( return_value != RC_SUCCESS ) {
		dbg(TRACE,"DeleteOldQueues: Calling 'QUE_QOVER' returns %d",return_value);
		return;
	} /* end if */
	len = sizeof(ITEM) + sizeof(QUE_INFO) + (sizeof(QINFO) * 200);
	item = (ITEM *) malloc(len);
	if ( item == (ITEM *) NUL ) {
		dbg(TRACE,"DeleteOldQueues: Allocating Memory failed");
		return;
	} /* end if */
	
	if ( que(QUE_GET,0,mod_id,3,len,(char *)item) != RC_SUCCESS ) {
		free((char *) item);
		dbg(TRACE,"DeleteOldQueues: Calling 'QUE_GET' returns %d",return_value);
		return;
	} /* end if */
	
	if ( item->msg_length == FATAL ) {
		free((char *) item);
		dbg(TRACE,"DeleteOldQueues: Message Length irregular");
		return;
	} /* end if */
	
	que_info = (QUE_INFO *) &(item->text[0]);
	info_count = 0;
	qinfo = (QINFO *) que_info->data;
	while ( info_count <= que_info->entries ) {
		info_count++;
		if ( (pcpQueue[0] != 0) && (qinfo->name[0] != 0) )
		{
			if ( (strlen(pcpQueue) > 0) && (strlen(qinfo->name) > 0) )
			{
				str_trm_all(qinfo->name," ",TRUE);
				if (strcmp(pcpQueue,qinfo->name) == 0) {
					dbg(DEBUG,"Delete Queue <%d> for '%.10s'",qinfo->id,qinfo->name);
					que(QUE_DELETE,qinfo->id,qinfo->id,3,0,0);
				}
			}
			else
				break;
		}
		else
			break;
		qinfo++;
	} /* end while */
	free((char *) item);
	return;
} /* end of DeleteOldQueues */

#endif

