#ifndef _DEF_mks_version_send_tools_c
  #define _DEF_mks_version_send_tools_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_send_tools_c[] = "@(#) "UFIS_VERSION" $Id: Ufis/PVG/PVG_Server/Base/Server/Library/Cedalib/send_tools.c 1.1 2005/04/08 16:31:18SGT mcu Exp  $";
#endif /* _DEF_mks_version */
/*****************************************************************************/
/*                                                                           */
/* Author         :                                                */
/* Date           :                                                          */
/* Description    :                                                          */
/* Update history :                                                          */
/*                                                                           */
/*****************************************************************************/

static char sccs_version_send_tools_lib[]="@(#)i UFIS 4.4 (c) ABB AAT/I send_tools.c 44.3 / 00/01/07 14:37:25 / RBI";

#ifdef _LINUX 
#define __USE_GNU 1
#endif

#include <send_tools.h>

/*x////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
////////////////x*/
extern snap(char *,long,FILE *);
extern nap(long);

#ifdef WMQ
#define QUEARG (ITEM **)
#else
#define QUEARG (char *)
#endif

int send_message(int prio, int msg_no, int route, int val, char *msg_data) 
{
	int			ilRC        = RC_SUCCESS;
	int			ilLen			= 0;
	int			ilNmghdlID	= 0;
	EVENT			*prlEvent   = NULL;
	BC_HEAD		*prlBCHead  = NULL;
	CMDBLK		*prlCmdblk  = NULL;
	char		*pclMsgInfo = NULL;

	return ilRC;

	/* gucken ob die uebergabeparameter stimmen */
	if (prio < IPRIO_NOTHING ||
	    prio > IMAX_PRIO)
		return -1;
	if (msg_no < 0)
		return -2;
	if (route < 0)
		return -3;
	if (val < 0)
		return -4;

	/* maximale Laenge der Nachricht */
	ilLen = sizeof(EVENT) + sizeof(BC_HEAD) 
		    + sizeof(CMDBLK) + sizeof(MSG_INFO_BLK) + 4 + 8000;

	/* Speicher fuer die komplette Nachricht freigeben */
	if ((prlEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
	{
		/* ohne memory laeuft nix */
		dbg(TRACE,"<send_message> not enough memory to run");
		return -5;
	}
	else
	{
		/* erst mal alles leoschen */
		memset((void*)prlEvent, 0x00, ilLen);

		/* Event-Structur fuellen */
		prlEvent->type        = SYS_EVENT;
		prlEvent->command     = EVENT_DATA;
		prlEvent->originator  = mod_id;
		prlEvent->retry_count = 0;
		prlEvent->data_offset = sizeof(EVENT);
		prlEvent->data_length = ilLen - sizeof(EVENT);

		/* Pointer der bc-head-structur hinter der Event structur positionieren */
		/* BC-Head fuellen */
		prlBCHead = (BC_HEAD*)((char*)prlEvent+prlEvent->data_offset);	
		prlBCHead->rc = RC_SUCCESS;

		/* Pointer fuer Cmdblk-Structur richtig setzen */
		/* Cmdblk-Structur fuellen */
		prlCmdblk = (CMDBLK*)((char*)prlBCHead->data);
		strcpy(prlCmdblk->command, SMESSAGE_INTERN);
		sprintf(prlCmdblk->obj_name, "%d", msg_no);
		
		/* Pointer fuer Msg_info-Structur setzen */
		pclMsgInfo = (char *)prlCmdblk->data;

dbg(TRACE,"%05d:send_message Event=%p,BCHead=%p,Cmdblk=%p,MsgInfo=%p,Len=%d",__LINE__,
		prlEvent,prlBCHead,prlCmdblk,pclMsgInfo,ilLen);
		/* msg_info-structur fuellen */
		/*pclMsgInfo->lang_code[0] = '\0';*/

		sprintf(pclMsgInfo,"<LANG_CODE>%s</LANG_CODE>","");
		pclMsgInfo += strlen(pclMsgInfo);  
		/* in Abhaengigkeit vom Nummernkreis den Messagetyp bestimmen */
		if (IsUserMsg(msg_no))
		{
			sprintf(pclMsgInfo,"<MSG_ORIGIN>%d</MSG_ORIGIN>",IUSER_MSG);
			pclMsgInfo += strlen(pclMsgInfo);  
		}
		else
		{
			if (IsConfMsg(msg_no))
			{
				sprintf(pclMsgInfo,"<MSG_ORIGIN>%d</MSG_ORIGIN>",ICONFLICT_MSG);
				pclMsgInfo += strlen(pclMsgInfo);  
			}
			else
			{
				if (IsErrorMsg(msg_no))
				{
					/*prlMsgInfo->msg_origin = IERROR_MSG;*/
					sprintf(pclMsgInfo,"<MSG_ORIGIN>%d</MSG_ORIGIN>",1);
					pclMsgInfo += strlen(pclMsgInfo);  
				}
				else
				{
					if (prlEvent != NULL)
					{
						free((void*)prlEvent);
					}
					return -6;
				}
			}
		}
		
		
		/** field 1 = lang_code is always empty **/
		/** field 2 = msg_val **/
		sprintf(pclMsgInfo,"<VAL>%d</VAL>",val);
		pclMsgInfo += strlen(pclMsgInfo);
		/** field 3 = prog_name **/
		sprintf(pclMsgInfo,"<PROG_NAME>%-*s</PROG_NAME>",PROG_NAME_LEN,mod_name);
		pclMsgInfo += strlen(pclMsgInfo);
		sprintf(pclMsgInfo,"<MSG_NO>%d</MSG_NO>",msg_no);
		pclMsgInfo += strlen(pclMsgInfo);
		sprintf(pclMsgInfo,"<MSG_PRIO>%d</MSG_PRIO>",prio);
		pclMsgInfo += strlen(pclMsgInfo);
		if (msg_data)
		{
			sprintf(pclMsgInfo,"<MSG_DATA>%-*s</MSG_DATA>",MSG_DATA_LEN,msg_data);
			/*sprintf(pclMsgInfo,"<MSG_DATA>%-s</MSG_DATA>",msg_data);*/
			pclMsgInfo += strlen(pclMsgInfo);
		}

		/* get que-id of nmghdl */
		if ((ilNmghdlID = tool_get_q_id("nmghdl")) == RC_NOT_FOUND ||
			  ilNmghdlID == RC_FAIL)
		{
			dbg(TRACE,"<send_message>: tool_get_q_id returns: %d", ilNmghdlID);
			ilRC = RC_FAIL;
		}
		else
		{
			/* Message senden */
			ilRC = que(QUE_PUT, ilNmghdlID, mod_id, PRIORITY_3, ilLen, QUEARG prlEvent);
		}

		/* Event-Speicher wieder freigeben */
		if (prlEvent != NULL)
			free ((void*)prlEvent);
	}

	/* und tschuess */
	return ilRC;
} 




/*x////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
////////////////x*/
int ScheduleAction(short spOriginID, short spRouteID, char *pcpCommand, 
			 char *pcpObjName, char *pcpSelection, char *pcpFields, char *pcpData)
{
	int			ilRC			= 0;
	int			ilLen			= 0;
	EVENT			*prlEvent	= NULL;
	BC_HEAD		*prlBCHead	= NULL;
	CMDBLK		*prlCmdblk	= NULL;

	/* params ok? */
	if (spOriginID < 0)
		return -1;
	if (spRouteID < 0)
		return -2;
	if (pcpCommand == NULL)
		return -3;
	if (pcpObjName == NULL)
		return -4;
	if (pcpFields == NULL)
		return -5;
	if (pcpData == NULL)
		return -6;

	/* init */
	ilRC = RC_SUCCESS;

	/* calculate lenght of envent... */
	if (pcpSelection != NULL)
		ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 
					  strlen(pcpSelection) + strlen(pcpFields) + strlen(pcpData) + 3;
	else
		ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 
					  strlen(pcpFields) + strlen(pcpData) + 3;

	/* try to allocate n bytes... */
	if ((prlEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
	{
		dbg(TRACE,"<ScheduleAction> cannot allocate %d bytes", ilLen);
		return -7;
	}

	/* clear memory */
	memset((void*)prlEvent, 0x00, ilLen);

	/* set stucture members */
	prlEvent->type				= SYS_EVENT;
	prlEvent->command			= EVENT_DATA;
	prlEvent->originator 	= spOriginID;
	prlEvent->retry_count 	= 0;
	prlEvent->data_offset 	= sizeof(EVENT);
	prlEvent->data_length 	= ilLen - sizeof(EVENT);
	
	/* BC-Head -Structure */
	prlBCHead = (BC_HEAD*)((char*)prlEvent+sizeof(EVENT));
	prlBCHead->rc = RC_SUCCESS;

	/* Cmdblk-Structure */
	prlCmdblk = (CMDBLK*)((char*)prlBCHead->data);
	strcpy(prlCmdblk->command, pcpCommand);
	strcpy(prlCmdblk->obj_name, pcpObjName);

	/* Selection */
	if (pcpSelection != NULL)
		strcpy(prlCmdblk->data, pcpSelection);

	/* Fields */
	if (pcpSelection != NULL)
		strcpy((prlCmdblk->data+strlen(pcpSelection)+1), pcpFields);
	else
		strcpy((prlCmdblk->data+1), pcpFields);

	/* Datablk */
	if (pcpSelection != NULL)
		strcpy((prlCmdblk->data+strlen(pcpSelection)+strlen(pcpFields)+2), pcpData);
	else
		strcpy((prlCmdblk->data+strlen(pcpFields)+2), pcpData);

	/* send message */
	ilRC = que(QUE_PUT, spRouteID, spOriginID, PRIORITY_3, ilLen, QUEARG prlEvent);

	/* free memory */
	if (prlEvent != NULL)
		free((void*)prlEvent);

	/* back */
	return ilRC;
}

/*x////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
////////////////x*/
int Timsch(int ipFunction, int ipEventID, int ipParentID, int ipDestinationID, 
           PERIOD *prpTime, int ipUsrDataLen, char *pcpUsrData)
{
	int				ilRC				= 0;
	int				ilLen				= 0;
	int				ilCnt				= 0;
	int				ilItemLen		= 0;
	int				ilNTischID		= 0;
	int				ilSecondModID	= 0;
	ITEM				*prlItem			= NULL;
	EVENT				*prlEvent		= NULL;
	FUNCTION			*prlFunction	= NULL;
	DATA_PERIOD		*prlDataPeriod	= NULL;

	/* check parameter... */
	/* period could be null */
	if (ipFunction < iDELETE || ipFunction > iDEL_MODID)
		return -10;
	if (ipEventID < 0)
		return -20;
	if (ipParentID < 0)
		return -30;
	if (ipDestinationID < 0)
		return -40;
	if (pcpUsrData != NULL && ipUsrDataLen < 0)
		return -50;

	/* format of message is EVENT->FUNCTION->DATA_PERIOD->USER_DATA */
	/* here we must create second que */
	if ((ilRC = GetDynamicQueue(&ilSecondModID, "NTI_TMP")) != RC_SUCCESS)
	{
		dbg(TRACE,"<Timsch> cannot create dynamic que");
		return -60;
	}
	else
		dbg(DEBUG,"<Timsch> Create Que with mod-id: %d", ilSecondModID);
	
	/* calculate lenght of envent... */
	ilLen = sizeof(EVENT) + sizeof(FUNCTION) + 
			  sizeof(DATA_PERIOD) + ipUsrDataLen + 4;

	/* try to allocate n bytes... */
	if ((prlEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
	{
		dbg(TRACE,"<Timsch> cannot allocate %d bytes", ilLen);
		ilRC = RC_FAIL;
	}
	else
		ilRC = RC_SUCCESS;

	if (ilRC == RC_SUCCESS)
	{
		/* clear memory */
		memset((void*)prlEvent, 0x00, ilLen);

		/* set stucture members */
		prlEvent->type						= SYS_EVENT;
		prlEvent->command					= EVENT_DATA;
		prlEvent->originator 			= ilSecondModID;
		prlEvent->retry_count 			= 0;
		prlEvent->data_offset 			= sizeof(EVENT);
		prlEvent->data_length 			= ilLen - sizeof(EVENT);

		/* set members of function... */
		prlFunction = (FUNCTION*)((char*)prlEvent + sizeof(EVENT));
		prlFunction->iFunction  		= ipFunction;
		prlFunction->iEventID   		= ipEventID;
		prlFunction->iParentID			= ipParentID;
		prlFunction->iDestinationID	= ipDestinationID;

		/* set members of data_period... */
		prlDataPeriod = (DATA_PERIOD*)((char*)prlFunction->data);
		if (prpTime != NULL)
		{
			prlDataPeriod->rPeriod.day_o_week = prpTime->day_o_week;
			prlDataPeriod->rPeriod.hour_o_day = prpTime->hour_o_day;
			prlDataPeriod->rPeriod.min_o_hour = prpTime->min_o_hour;
			prlDataPeriod->rPeriod.sec_o_min  = prpTime->sec_o_min;
			prlDataPeriod->rPeriod.n_times    = prpTime->n_times;
			prlDataPeriod->rPeriod.count      = prpTime->count;
			prlDataPeriod->rPeriod.period     = prpTime->period;
		}

		/* now user specific data */
		prlDataPeriod->lDataLen = (long)ipUsrDataLen;
		if (ipUsrDataLen > 0 && pcpUsrData != NULL)
		{
			memcpy((void*)prlDataPeriod->data,
					 (const void*)pcpUsrData, (size_t)ipUsrDataLen);
		}

		/* get que-id of action */
		if ((ilNTischID = tool_get_q_id("ntisch")) == RC_NOT_FOUND ||
			  ilNTischID == RC_FAIL)
		{
			dbg(TRACE,"<Timsch>: tool_get_q_id returns: %d", ilNTischID);
			ilRC = RC_FAIL;
		}
		else
		{
			/* send it now */
			ilRC = que(QUE_PUT, ilNTischID, ilSecondModID, PRIORITY_3, ilLen, QUEARG prlEvent);
		}

		/* free memory */
		if (prlEvent != NULL)
			free((void*)prlEvent);

		if (ilRC != RC_FAIL)
		{
			/************* NEW ANSWER EVERY TIME ************
			* get a answer (only if function = ADD, HSBADD) *
			if (ipFunction == iADD || ipFunction == iHSBADD)
			{
			************************************************/

				/* get memory for answer */
				ilItemLen = MAX_EVENT_SIZE;
				if ((prlItem = (ITEM*)malloc((size_t)ilItemLen)) == NULL)
				{
					dbg(TRACE,"cannot allocate memory for item");
					ilRC = RC_FAIL;
				}
				else
				{
					/* initialize pointer */
					prlEvent = (EVENT*)prlItem->text;

					/* try to receive answer */
					do
					{
						/* call get with priority 3 */
						if ((ilRC = que(QUE_GETNW, 0, ilSecondModID, PRIORITY_3, 
											 ilItemLen,QUEARG prlItem)) != RC_SUCCESS)
						{
							/* increment counter */
							ilCnt++;

							/* wait for 10 milliseconds */
							nap(10);
						}

						/* ilCnt = 12000 -> wait max. 120 seconds */
					} while (ilRC != RC_SUCCESS && ilCnt < 12000);

					if (ilRC == RC_SUCCESS)
					{
						/* we must acknowledge the que */
						if ((ilRC = que(QUE_ACK, 0, 
											 ilSecondModID, PRIORITY_3, 0, NULL)) != RC_SUCCESS)
						{
							dbg(TRACE,"<Timsch> cannot ACK que...");
						}

						/* type of answer is EVENT->FUNCTION... */
						prlFunction = (FUNCTION*)((char*)prlEvent + sizeof(EVENT));
						
						/* this is the new EventID!!!!! */
						ilRC = prlFunction->iEventID;	
					}
					else
						dbg(TRACE,"<Timsch> QUE_GETNW error");

					/* free memory */
					if (prlItem != NULL)
						free ((void*)prlItem);
				}

			/**************************
			}
			***************************/

		}
	}

	/* delete second que */
	if (que(QUE_DELETE, ilSecondModID, ilSecondModID, PRIORITY_3, 0, NULL) != RC_SUCCESS)
	{

		/*dbg(TRACE,"<Timsch> can t delete second que");*/
	}

	/* back */
	return ilRC;
}

/*x////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
////////////////x*/
int HandleActionSection(int ipFunction, char *pcpCommand, char *pcpTCPCmd,
								int ipCmdNo, int ipTabNo)
{
	int			ilRC			= 0;
	int			ilLen			= 0;
	int			ilActionID	= 0;
	EVENT			*prlEvent	= NULL;
	BC_HEAD		*prlBCHead	= NULL;
	CMDBLK		*prlCmdblk	= NULL;

	/* params ok? */
	if (pcpCommand == NULL)
		return -1;
	if (ipFunction < iINSERT_SECTION || ipFunction > iDELETE_SECTION)
		return -2;

	/* init */
	ilRC = RC_SUCCESS;

	/* calculate lenght of envent... */
	ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK);

	/* try to allocate n bytes... */
	if ((prlEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
	{
		dbg(TRACE,"cannot allocate %d bytes", ilLen);
		return -7;
	}

	/* clear memory */
	memset((void*)prlEvent, 0x00, ilLen);

	/* set stucture members */
	prlEvent->type				= SYS_EVENT;
	prlEvent->command			= EVENT_DATA;
	prlEvent->originator 	= mod_id;
	prlEvent->retry_count 	= 0;
	prlEvent->data_offset 	= sizeof(EVENT);
	prlEvent->data_length 	= ilLen - sizeof(EVENT);
	
	/* BC-Head -Structure */
	prlBCHead = (BC_HEAD*)((char*)prlEvent+sizeof(EVENT));
	prlBCHead->rc = RC_SUCCESS;

	/* Cmdblk-Structure */
	prlCmdblk = (CMDBLK*)((char*)prlBCHead->data);
	strcpy(prlCmdblk->command, pcpCommand);
	if (ipFunction == iINSERT_SECTION)
		strcpy(prlCmdblk->obj_name, "INSERT_THIS_SECTION");
	else
		strcpy(prlCmdblk->obj_name, "DELETE_THIS_SECTION");
	sprintf(prlCmdblk->tw_start, "%s", pcpTCPCmd);
	sprintf(prlCmdblk->tw_end, "%d,%d", ipCmdNo, ipTabNo);

	/* get que-id of action */
	if ((ilActionID = tool_get_q_id("action")) == RC_NOT_FOUND ||
		  ilActionID == RC_FAIL)
	{
		dbg(TRACE,"<send_message>: tool_get_q_id returns: %d", ilActionID);
		ilRC = RC_FAIL;
	}
	else
	{
		/* Message senden */
		ilRC = que(QUE_PUT, ilActionID, mod_id, PRIORITY_3, ilLen, QUEARG prlEvent);
	}

	/* free memory */
	if (prlEvent != NULL)
		free((void*)prlEvent);

	/* back */
	return ilRC;
}

/*x////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
////////////////x*/
int GetUniqueNumber(char *pcpProcess, char *pcpKey, char *pcpFlag, 
						  int ipCnt, long *lpUniqueNo, long *lpMax, long *lpMin)
{
	int				ilLen				= 0;
	int				ilCnt				= 0;
	int				ilItemLen		= 0;
	int				ilRouterID		= 0;
	int				ilSecondModID	= 0;
	int				ilRC				= 0;
	ITEM				*prlItem			= NULL;
	EVENT				*prlEvent		= NULL;
	BC_HEAD			*prlBCHead		= NULL;
	CMDBLK			*prlCmdblk		= NULL;
	char				*pclSelection	= NULL;
	char				*pclFields		= NULL;
	char				*pclData			= NULL;
	char				*pclS				= NULL;
	char				pclTmpBuf[iMIN_BUF_SIZE];

	/* check parameter... */
	if (pcpProcess == NULL)
		return RC_FAIL;
	if (pcpKey == NULL)
		return RC_FAIL;
	if (ipCnt < 0)
		return RC_FAIL;

	/* here we must create second que */
	if ((ilRC = GetDynamicQueue(&ilSecondModID, pcpProcess)) != RC_SUCCESS)
	{
		dbg(TRACE,"<GetUniqueNumber> cannot create dynamic que");
		return RC_FAIL;
	}

	/* copy data to tmp-buffer */
	/* this is for data */
	memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
	sprintf(pclTmpBuf, "%d,%s,%s", ipCnt, pcpKey, pcpFlag);
	
	/* calculate lenght of envent... */
	ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + 
			  sizeof(CMDBLK) + strlen(pclTmpBuf) + 4;

	/* try to allocate n bytes... */
	if ((prlEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
	{
		dbg(TRACE,"<GetUniqueNumber> cannot allocate %d bytes", ilLen);
		ilRC = RC_FAIL;
	}
	else
		ilRC = RC_SUCCESS;

	if (ilRC == RC_SUCCESS)
	{
		/* clear memory */
		memset((void*)prlEvent, 0x00, ilLen);

		/* set stucture members */
		prlEvent->type						= SYS_EVENT;
		prlEvent->command					= EVENT_DATA;
		prlEvent->originator 			= ilSecondModID;
		prlEvent->retry_count 			= 0;
		prlEvent->data_offset 			= sizeof(EVENT);
		prlEvent->data_length 			= ilLen - sizeof(EVENT);

		/* BC-Head -Structure */
		prlBCHead = (BC_HEAD*)((char*)prlEvent+sizeof(EVENT));
		prlBCHead->rc = RC_SUCCESS;

		/* Cmdblk-Structure */
		prlCmdblk = (CMDBLK*)((char*)prlBCHead->data);
		strcpy(prlCmdblk->command, "GMN");

		/* Selection not currently used */
		/* Fields not currently used */
		/* data */
		strcpy(prlCmdblk->data+2, pclTmpBuf);

		/* get que-id of action */
		if ((ilRouterID = tool_get_q_id("router")) == RC_NOT_FOUND ||
			  ilRouterID == RC_FAIL)
		{
			dbg(TRACE,"<GetUniqueNumber>: tool_get_q_id returns: %d", ilRouterID);
			ilRC = RC_FAIL;
		}
		else
		{
			/* send it now */
			if ((ilRC = que(QUE_PUT, ilRouterID, 
					 ilSecondModID, PRIORITY_2, ilLen, QUEARG prlEvent)) != RC_SUCCESS)
			{
				dbg(TRACE,"<GetUniqueNumber> QUE_PUT returns: %d", ilRC);
			}
		}

		/* free memory */
		if (prlEvent != NULL)
			free((void*)prlEvent);

		if (ilRC != RC_FAIL)
		{
			/* get memory for answer */
			ilItemLen = MAX_EVENT_SIZE;
			if ((prlItem = (ITEM*)malloc((size_t)ilItemLen)) == NULL)
			{
				dbg(TRACE,"<GetUniqueNumber> allocate memory for item");
				ilRC = RC_FAIL;
			}
			else
			{
				/* initialize pointer */
				prlEvent = (EVENT*)prlItem->text;

				/* try to receive answer */
				do
				{
					/* call get with priority 3 */
					if ((ilRC = que(QUE_GETNW, 0, ilSecondModID, PRIORITY_3, 
											   ilItemLen, QUEARG prlItem)) != RC_SUCCESS)
					{
						/* increment counter */
						ilCnt++;

						/* wait 10 milliseconds */
						nap(10);
					}

					/* ilCnt = 12000 -> wait max. 120 seconds */
				} while (ilRC != RC_SUCCESS && ilCnt < 12000);

				if (ilRC == RC_SUCCESS)
				{
					/* we must acknowledge the que */
					if ((ilRC = que(QUE_ACK, 0, 
										 ilSecondModID, PRIORITY_3, 0, NULL)) != RC_SUCCESS)
					{
						dbg(TRACE,"<GetUniqueNumber> cannot ACK que...");
					}

					/* get the number */
					prlBCHead = (BC_HEAD*)((char*)prlEvent+sizeof(EVENT));
					if (prlBCHead->rc != RC_SUCCESS)
					{
						dbg(TRACE,"<GetUniqueNumber> BCHead->rc returns: %d", 
																		prlBCHead->rc);
						dbg(TRACE,"<GetUniqueNumber> String: <%s>", prlBCHead->data);
						ilRC = RC_FAIL;
					}
					else
					{
						prlCmdblk 	 = (CMDBLK*)prlBCHead->data;
						pclSelection = prlCmdblk->data;
						pclFields 	 = prlCmdblk->data + strlen(pclSelection) + 1;
						pclData 		 = prlCmdblk->data + strlen(pclSelection) + 
								  			strlen(pclFields) + 2;

						/* get unique number */
						if ((pclS = (char*)GetDataField((char*)pclData, 
															(UINT)0, (char)',')) == NULL)
						{
							dbg(TRACE,"<GetUniqueNumber> GDF(1) return NULL"); 
							ilRC = RC_FAIL;
						}
						else
						{
							/* convert unique number to long */
							*lpUniqueNo = atol(pclS);

							/* get max number */
							if ((pclS = (char*)GetDataField((char*)pclData, 
															(UINT)1, (char)',')) == NULL)
							{
								dbg(TRACE,"<GetUniqueNumber> GDF(2) return NULL"); 
								ilRC = RC_FAIL;
							}
							else
							{
								/* convert max to long */
								*lpMax = atol(pclS);

								/* get min number */
								if ((pclS = (char*)GetDataField((char*)pclData, 
															(UINT)2, (char)',')) == NULL)
								{
									dbg(TRACE,"<GetUniqueNumber> GDF(3) return NULL"); 
									ilRC = RC_FAIL;
								}
								else
								{
									/* convert min to long */
									*lpMin = atol(pclS);
									ilRC = RC_SUCCESS;
								}
							}
						}
					}
				}
				else
					dbg(TRACE,"<GetUniqueNumber> QUE_GETNW error");
					
				/* free memory */
				if (prlItem != NULL)
					free ((void*)prlItem);
			}
		}
	}

	/* delete second que */
	if (que(QUE_DELETE, ilSecondModID, ilSecondModID, PRIORITY_3, 0, NULL) != RC_SUCCESS)
	{
		dbg(TRACE,"<GetUniqueNumber> can't delete second que");
	}

	if (ilRC != RC_SUCCESS)
		*lpUniqueNo = *lpMax = *lpMin = -1;

	/* back */
	return ilRC;
}

