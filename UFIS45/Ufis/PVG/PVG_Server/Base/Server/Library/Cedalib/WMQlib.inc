#ifndef _DEF_mks_version_WMQlib_c 
#define _DEF_mks_version_WMQlib_c
#include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_WMQlib_c[] = "@(#) "UFIS_VERSION" $Id: Ufis/PVG/PVG_Server/Base/Server/Library/Cedalib/WMQlib.inc 1.16 2007/06/08 15:48:29SGT mcu Exp  $";
#endif /* _DEF_mks_version */
/* ******************************************************************** */
/*                                                    									*/
/*								                                                    	*/
/*                                                    									*/
/*                                                    									*/
/*                                                    									*/
/*                                                    									*/
/*								                                                    	*/
/*								                                                    	*/
/*								                                                    	*/
/* 20050503 JIM: avoid core while evaluating WMQ Queue Manager name list*/
/* 20050509 JIM:  PRF 7374: added QUE_WMQ_RESP for WMQ Get response with*/
/*                q>30000 to not close WMQ queue on QUE_PUT             */
/* 20051018 JIM: don't reread config, only reset flags and handles for  */
/*							 forked childs                                        	*/
/*								                                                    	*/
/* ******************************************************************** */

#include <stdio.h>   
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#define NEWQUEDEF
#include "quedef.h"
#include "uevent.h"
#include "tools.h"
/* includes for WebsphereMQ  */
#include <cmqc.h>
#include "WMQlib.h"
#include "WMQReasonCodes.h"

#define WMQ_CFG_FILE "wmqlib.cfg"

#ifndef RC_NOTFOUND
#define RC_NOTFOUND 1
#endif


static void TrimRight(char *pcpBuffer);

static char cgConfigFile[512] = "";
static char cgMqServer[2048] = "";
pid_t gChildPid;
static int igCountConfigured = -1; /* to avoid reuse of temp queues in fork */

/*Handles for Sending*/
static MQHCONN  HconSend;                   /* connection handle             */
static MQHOBJ   HobjSend;                   /* object handle                 */

/*Handles for Receiving */
static MQHCONN  HconRecBrowse;              /* connection handle             */
static MQHCONN  HconRecGet;                 /* connection handle             */
static MQHOBJ   HobjRecBrowse;              /* object handle                 */
static MQHOBJ   HobjRecGet;

static int ilsAlreadyConnected = FALSE;
static long lgBufferSize = 4*1024;

#define MAXQUEUES 2048 

struct WmqConnectionStruct
{
	char QueueName[32];
	MQHCONN hpConnectionHandle;
} WmqConnections[24];



struct WmqQueInfo
{
	char QueId[50];
	char QueName[50];
	MQHCONN hpConnectionHandle;
	MQHOBJ QueHandle;
	BOOL IsOpen;
} rgQueInfo[MAXQUEUES] = {0};


char *WmqGetReasonString(int ipReasonCode)
{
	 int ilLc;

   for (ilLc = 0; WMQReasonCodes[ilLc].Code != -1; ilLc++)
	 { 
	 	  if (ipReasonCode == WMQReasonCodes[ilLc].Code ||
					WMQReasonCodes[ilLc].Code == -1)
			{
				return (WMQReasonCodes[ilLc].Name);
			}
	 }
	return ("Unknown Reasoncode");
}


int WmqSendMessage(
char *pcpModId, /* The mod_id which is passed to the process as argv[1], this parameter will be used as originator */
  char *pcpQueId, /* Id of the queue to which this message will be send */
	MQHOBJ *phpHobj,
	int     ipPriority,    /* Priority of this message */
	char   *pcpData)
{
	int ilRc = RC_SUCCESS;
  char clMyTime[30] = "\0";
  char clNowTime[30] = "\0";
  short slFkt = 0;

  MQMD     md = {MQMD_DEFAULT};    /* Message Descriptor              */
  MQLONG   CompCode;               /* completion code                 */
  MQLONG   Reason;                 /* reason code                     */
  MQPMO    pmo = {MQPMO_DEFAULT};   /* put message options             */
	MQHOBJ   HobjSend; 

  dbg(DEBUG,"WmqSendMessage: -----SendToMq start -----");

	/* PUT MESSAGE TO MQ QUEUE */

	if (*phpHobj == -1)
	{
		ilRc = WmqOpenQueue(pcpQueId,phpHobj);
	}
	
	memcpy(md.Format,           
			 MQFMT_STRING, MQ_FORMAT_LENGTH);
	memcpy(md.MsgId, MQMI_NONE, sizeof(md.MsgId));

	pmo.Options = MQPMO_NEW_MSG_ID
		+ MQPMO_FAIL_IF_QUIESCING;

	switch (ipPriority)
	{
	case 1: md.Priority = 9;
	  break;
	case 2: md.Priority = 7;
	  break;
	case 3: md.Priority = 5;
	  break;
	case 4: md.Priority = 3;
	  break;
	case 5: md.Priority = 1;
	  break;
	default: md.Priority = MQPRI_PRIORITY_AS_Q_DEF;
	}

	MQPUT(HconRecBrowse,         /* connection handle               */
			*phpHobj,          /* object handle                   */
			&md,                 /* message descriptor              */
			&pmo,                /* default options                 */
			strlen(pcpData)+1,              /* buffer length                   */
			pcpData,              /* message buffer                  */
			&CompCode,           /* completion code                 */
			&Reason);            /* reason code                     */

	strncpy(clMyTime,md.PutDate,16);
	clMyTime[16] = '\0';

	if (Reason != MQRC_NONE)
	{
		/* report reason, if any */
		dbg(TRACE,"WmqSendMessage: ERROR: MQPUT<%s/%s> ended with ReasonCode <%d>=<%s>, Time: <%s>",mod_name,pcpQueId,Reason,WmqGetReasonString(Reason),clMyTime);
	}
	else
	{
		dbg(DEBUG,"WmqSendMessage: MQPUT<%s/%s> successful with ReasonCode <%d>=<%s>,Time: <%s>",mod_name,pcpQueId,Reason,WmqGetReasonString(Reason), clMyTime);
	}
	/* 20050509 JIM: if ((atoi(pcpQueId) >= 20000) */
	if ((atoi(pcpQueId) >= 20000) && (atoi(pcpQueId) < 30000))
	{
		/*dbg(DEBUG,"WmqSendMessage: Temp queue <%s> will be closed now.",pcpQueId);*/
		WmqCloseQueue(pcpQueId);
	}

	return ilRc;
}



int WmqCommit()
{
	  int ilRc = RC_SUCCESS;

    MQLONG   llCompCode;               /* MQOPEN completion code        */
    MQLONG   llReason;                 /* reason code                   */

		MQCMIT(HconRecBrowse,                /* connection handle                 */
				&llCompCode,
				&llReason);

		/*dbg(TRACE,"WmqCommit: MQCMIT ended with ReasonCode <%d>",llReason);*/

		return ilRc;
}
		



char* WmqGetMessage(
		char *pcpQueId, /* The id of the queue from whicht this message will be retrieved */ 
	MQHOBJ *phpHobj,
	long		lpWait,         /* -1 for blocking call, 0 return immediately fi no message is available, any positive value wait milliseconds for a message to become available */
	BOOL	  bpBrowse,       /*TRUE = the queue is only browsed, FALSE = the message will be retrieved from the queue */
	char *pcpMessageId,      /* WMQ-Id of the message */
	char **pcpData,int *ipRc)

{
	time_t llNow;

	  int ilRc;

    MQLONG   OpenBrowseOptions;              /* MQOPEN options                */
    MQHOBJ   QueueBrowseHobj;                   /* object handle                 */
    MQOD     QueueBrowseOd = {MQOD_DEFAULT};    /* Object Descriptor             */
    MQLONG   QueueBrowseOpenCode;               /* MQOPEN completion code        */
    MQLONG   QueueBrowseCompCode;               /* MQOPEN completion code        */
    MQLONG   llQueueBrowseReason;                 /* reason code                   */
    MQGMO    QueueBrowseGmo = {MQGMO_DEFAULT};   /* get message options           */
    MQMD     ReceiveBrowseMd = {MQMD_DEFAULT};    /* Message Descriptor            */
		MQLONG   llMessLen = 0;

		/*static long llRecBuffLen = 0;*/
		static int  ilsInhibitedCount=0;

    char clMsgId[30] = "\0";
    char clMyBuffer[4000] = "\0";
    char clMyTime[20] = "\0";
    short slFkt = 0;
    short slCursor = 0;
    char clData[100] = "\0";
    char clUrno[12] = "\0";
    char clNowTime[20] = "\0";
    char clSqlBuf[4500] ="\0";
    static char clAlert[256] = "\0";

		/*if (llRecBuffLen != (4*1024))*/
		if (*pcpData == NULL)
		{
			/*lgBufferSize = 4*1024;*/
			*pcpData = realloc(*pcpData,lgBufferSize+2);
			dbg(DEBUG,"WmqGetMessage: (re)allocation of <%d> Bytes for data-buffer Pointer=<%p>",lgBufferSize+2,*pcpData);
		}
	if (*phpHobj == -1)
	{
		ilRc = WmqOpenQueue(pcpQueId,phpHobj);
	}
		if (lpWait > 0)
		{
			llNow = time(NULL);
		}

	QueueBrowseGmo.WaitInterval = 0;
	QueueBrowseGmo.Options = MQGMO_WAIT | MQGMO_SYNCPOINT; 
	/*MQGMO_ACCEPT_TRUNCATED_MSG for later use */

	if (lpWait == 0)
	{
		QueueBrowseGmo.WaitInterval = MQWI_UNLIMITED;
	}
	else if (lpWait != -1)
	{
		QueueBrowseGmo.WaitInterval = lpWait * 1000;
	}
	else
	{
		QueueBrowseGmo.Options = MQGMO_NO_WAIT | MQGMO_SYNCPOINT;
		/*dbg(DEBUG,"WmqGetMessage: Gmo.Options set to  MQGMO_NO_WAIT | MQGMO_SYNCPOINT");*/
	}
		llMessLen = 0;

		do
		{
			MQGET(HconRecBrowse,         /* connection handle                 */
		      *phpHobj,                /* object handle                     */
		      &ReceiveBrowseMd,        /* message descriptor                */
		      &QueueBrowseGmo,         /* get message options               */
		      lgBufferSize,            /* buffer length                     */
		      *pcpData,                /* message buffer                    */
		      &llMessLen,              /* message length                    */
		      &QueueBrowseCompCode,    /* completion code                   */
		      &llQueueBrowseReason);   /* reason code                       */

			/*dbg(DEBUG,"WmqGetMessage: Data-len=<%ld> Data=<--\n%s\n-->",llMessLen,*pcpData);*/

			if (
						(llQueueBrowseReason != MQRC_NO_MSG_AVAILABLE) && 
						(llQueueBrowseReason != MQRC_GET_INHIBITED   ) &&
						(llQueueBrowseReason != MQRC_NONE)
				 )
			{
				dbg(TRACE,"%05d:WmqGetMessage: MQGET<%s/%s> ended with ReasonCode: <%ld>=<%s>. BrowseCompcode <%ld>",__LINE__,mod_name,pcpQueId,llQueueBrowseReason,WmqGetReasonString(llQueueBrowseReason),QueueBrowseCompCode);
			}

			if (llQueueBrowseReason == MQRC_TRUNCATED_MSG_FAILED)
			{
					if (llMessLen > 10000000)
					{
						dbg(TRACE,"WmqGetMessage: Message len <%d> larger then 10MB don't process!",llMessLen);
						*ipRc = -1;
						return *pcpData;
					}
					lgBufferSize = llMessLen+2;
					*pcpData = realloc(*pcpData,lgBufferSize+2);
					dbg(TRACE,"WmqGetMessage: Data-BuffLen increased to <%d>",lgBufferSize);
					llQueueBrowseReason = MQRC_NO_MSG_AVAILABLE;
					continue;
			}
			/* setting RC to MQRC_NO_MSG_AVAILABLE since a MQGET delivers this code */
			/* if the GET on this queue is inhibited, but in the  context of UFIS */
			/* this only means that there is no message at the moment. Depending on the */
			/* time to be waited a nap(500) removes the according from the  "top" lists */
			/* but it still tries from time to time if needed. */
			if (llQueueBrowseReason == MQRC_GET_INHIBITED)
			{
				if (ilsInhibitedCount == 0)
				{
					dbg(TRACE,"WmqGetMessage: INFO: MQGET<%s/%s> ReasonCode: <%ld>=<%s>"
								,mod_name,pcpQueId,llQueueBrowseReason,WmqGetReasonString(llQueueBrowseReason));
				}
				ilsInhibitedCount++;
				/* Show Message each minute: 120*nap(500)=60000 millisec.=60 sec.*/
				if (ilsInhibitedCount >= 120)
				{
						ilsInhibitedCount = 0;
				}

				if (lpWait >= 0)
				{
					nap(500);
				}
				llQueueBrowseReason = MQRC_NO_MSG_AVAILABLE;
			}
			if (lpWait == -1)
			{
				break;
			}
			else if (lpWait > 0)
			{
				time_t llActual;

				llActual = time(NULL);
				if ((llActual - llNow) >= lpWait)
				{
					break;
				}
			}
		} while (llQueueBrowseReason == MQRC_NO_MSG_AVAILABLE);

		if(llQueueBrowseReason == MQRC_NONE)
		{
		    dbg(DEBUG,"WmqGetMessage: MQGET<%s/%s> ended with Reasoncode: <%d>",mod_name,pcpQueId,llQueueBrowseReason);
				ilRc = RC_SUCCESS;
		}
		else if (llQueueBrowseReason == MQRC_NO_MSG_AVAILABLE)
		{
		    /*dbg(DEBUG,"WmqGetMessage: MQGET<%s/%s> ended with Reasoncode: <%d>",mod_name,pcpQueId,llQueueBrowseReason);*/
				ilRc = QUE_E_NOMSG;
		}
		else
		{
				ilRc = RC_FAIL;
		    dbg(TRACE,"WmqGetMessage: MQGET<%s/%s> ended with Reasoncode: <%d>=<%s>",mod_name,pcpQueId,llQueueBrowseReason,WmqGetReasonString(llQueueBrowseReason ));
		}
		*ipRc = ilRc;
		return *pcpData;
}		


int	init_que()
{

	int ilRc;
	/* JWE: Once init_que() is perfomrmed for WMQ we return RC_SUCCESS */
	/* since another init will cause a MQRC_ALREADY_CONNECTED return code from */
	/* MQ which leads into problems in libraries or the main-process itself */
	/* if not handled accordingly */
	/* Therefore we shield the lib and the process from accidently doing a second init_que()*/
	/* for MQ-based installations (e.g. GetDynamicQueue()-function does this!) */


	dbg(TRACE,"init_que: Connecting to Queue-Manager");
	if (ilsAlreadyConnected == FALSE)
	{
		ilRc = WmqConnectToQM(-1,-2,TRUE);
		if (ilRc == RC_SUCCESS)
		{
			ilsAlreadyConnected = TRUE;
			return RC_SUCCESS;
		}
	}
	else
	{
		return RC_SUCCESS;
	}
}

int WmqCreateDynamicQueue(char *pcpQueId, MQHOBJ *phpQueHandle, int ipQueMin)
{
	int ilRc = RC_SUCCESS;
	int ilLc;

	char clQueId[132] = "";
	MQHCONN hpConnectionHandle;

  MQLONG   OpenBrowseOptions;                 /* MQOPEN options                */
  MQHOBJ   QueueBrowseHobj;                   /* object handle                 */
  MQOD     QueueBrowseOd = {MQOD_DEFAULT};    /* Object Descriptor             */
  MQLONG   QueueBrowseOpenCode;               /* MQOPEN completion code        */
  MQLONG   QueueBrowseCompCode;               /* MQOPEN completion code        */
  MQLONG   QueueBrowseReason;                 /* reason code                   */
  MQGMO    QueueBrowseGmo = {MQGMO_DEFAULT};  /* get message options           */
  MQMD     ReceiveBrowseMd = {MQMD_DEFAULT};  /* Message Descriptor            */
	BOOL 		 blCreateQue = FALSE;
	BOOL     blOpenQue = FALSE;
	int ilDummyId;

/* OPEN QUEUE FOR BROWSING */
#if 0
	OpenBrowseOptions = MQOO_INPUT_AS_Q_DEF    /* open queue for input      */
	    + MQOO_OUTPUT
	    + MQOO_BROWSE            /* open for browsing */
	    + MQOO_FAIL_IF_QUIESCING;              /* but not if MQM stopping   */
#endif

	OpenBrowseOptions =
	    + MQOO_OUTPUT
	    + MQOO_BROWSE            /* open for browsing */
	    + MQOO_FAIL_IF_QUIESCING              /* but not if MQM stopping   */
			+ MQOO_INPUT_SHARED;

		strcpy(clQueId,"CEDATMP");

    /* 20050509 JIM: replace 20000 by ipQueMin= [20000|30000] */
		ilDummyId = ((getpid()%1000)+ipQueMin);
		for ( ; ilDummyId < ipQueMin+10000; ilDummyId++)
		{
			sprintf(QueueBrowseOd.DynamicQName,"%d",ilDummyId);
	
			dbg(DEBUG,"WmqCreateDynamicQueue: Que <%s/%d> will be created.",clQueId,ilDummyId);
			strcpy(QueueBrowseOd.ObjectName,clQueId);

		/* open a new dynamic queue queue */
	MQOPEN(HconRecBrowse,                      /* connection handle            */
		   &QueueBrowseOd,                /* object descriptor for queue  */
		   OpenBrowseOptions,                 /* open options                 */
		   &QueueBrowseHobj,                     /* object handle                */
		   &QueueBrowseOpenCode,                 /* completion code              */
		   &QueueBrowseReason);                  /* reason code                  */

		
		if (QueueBrowseReason == MQRC_OBJECT_ALREADY_EXISTS)
		{
			continue;
		}
		else if (QueueBrowseReason != MQRC_NONE)
	    {
			  dbg(TRACE,"WmqCreateDynamicQueue: failed to open queue <%s>",pcpQueId);
				dbg(TRACE,"WmqCreateDynamicQueue: MQOPEN ended with ReasonCode <%d>=<%s>",QueueBrowseReason,WmqGetReasonString(QueueBrowseReason ));
		    dbg(TRACE,"WmqCreateDynamicQueue: No connection to que manager, try to reconnect.");
			ilRc = WmqConnectToQM(-1,-2,FALSE);
	    }
		else
		{
			char clTmpQueId[132];

			strncpy(clTmpQueId,QueueBrowseOd.ObjectName,48);
			clTmpQueId[48] = '\0';
			TrimRight(clTmpQueId);
			strcpy(pcpQueId,clTmpQueId);
			dbg(DEBUG,"WmqCreateDynamicQueue: MQOPEN created dynamic queue <%s> sucessfull with ReasonCode <%d> OpenCode <%d>",pcpQueId,QueueBrowseReason,QueueBrowseOpenCode);
			for(ilLc = 0; ilLc < MAXQUEUES; ilLc++)
			{
				if  (*rgQueInfo[ilLc].QueId == 0)
				{
						strcpy(rgQueInfo[ilLc].QueId ,pcpQueId);			
						rgQueInfo[ilLc].IsOpen = TRUE;
						rgQueInfo[ilLc].QueHandle = QueueBrowseHobj;
						*phpQueHandle = QueueBrowseHobj;
						ilRc = RC_SUCCESS;
						break;
				}
			}
		break;
		}

    if (QueueBrowseOpenCode == MQCC_FAILED)
    {
			dbg(TRACE,"WmqCreateDynamicQueue: Unable to open queue!");
			ilRc = RC_FAIL;
    }
	}
	return ilRc;
}

int WmqCloseQueue(char *pcpQueId)
{
	int ilRc = RC_SUCCESS;
	int ilLc;

	char clQueId[132] = "";
	MQHCONN hpConnectionHandle;

  MQLONG   CloseOptions;                 /* options                */
  MQHOBJ   CloseHobj;                   /* object handle                 */
  MQLONG   CloseCompCode;               /* completion code        */
  MQLONG   CloseReason;                 /* reason code                   */

	BOOL     blIsOpen = FALSE;


/* OPEN QUEUE FOR BROWSING */
	CloseOptions =
	    + MQCO_NONE;


	/* get queue status */
	for(ilLc = 0; ilLc < MAXQUEUES; ilLc++)
	{
		if  (strcmp(rgQueInfo[ilLc].QueId,pcpQueId) == 0)
		{
			/* que exist already */
			strcpy(clQueId,rgQueInfo[ilLc].QueId);
			break;
		}
	}
	
	if (*clQueId != '\0')
	{
		if (rgQueInfo[ilLc].IsOpen == TRUE)
		{
			blIsOpen = TRUE;
			CloseHobj = 	rgQueInfo[ilLc].QueHandle; 
			/*dbg(DEBUG,"WmqCloseQueue: QueHandle=%p",CloseHobj);*/
		}
		rgQueInfo[ilLc].QueHandle = MQHO_UNUSABLE_HOBJ;
		*rgQueInfo[ilLc].QueId = '\0';
	}

	if (blIsOpen == TRUE)
	{
		dbg(DEBUG,"WmqCloseQueue: Que <%s> will be closed.",clQueId);

		/* open an already existing queue */
	MQCLOSE(HconRecBrowse,                      /* connection handle            */
		   &CloseHobj,                     /* object handle                */
		   CloseOptions,                 /* open options                 */
		   &CloseCompCode,                 /* completion code              */
		   &CloseReason);                  /* reason code                  */

		if (CloseReason != MQRC_NONE)
	  {
			dbg(DEBUG,"WmqCloseQueue: MQCLOSE queue <%s> ended with ReasonCode <%d>",clQueId,CloseReason);
			ilRc = RC_FAIL;
			if (CloseReason == MQRC_CONNECTION_BROKEN)
			{
				dbg(TRACE,"WmqCloseQueue: No connection to quemanager, retry connect");
				ilRc = WmqConnectToQM(-1,-2,FALSE);
				if (ilRc == RC_SUCCESS)
				{
					ilRc = WmqCloseQueue(pcpQueId);
				}
			}
			ilRc = RC_FAIL;
	  }
		else
		{
			/*dbg(DEBUG,"WmqCloseQueue: MQCLOSE <%s> sucessfull with ReasonCode <%d> CloseCode<%d>",clQueId,CloseReason,CloseCompCode);*/
			ilRc = RC_SUCCESS;
		}
	}
	return ilRc;
}

int WmqOpenQueue(char *pcpQueId, MQHOBJ *phpQueHandle)
{
	int ilRc = RC_SUCCESS;
	int ilLc;

	char clQueId[132] = "";
	MQHCONN hpConnectionHandle;

  MQLONG   OpenBrowseOptions;                 /* MQOPEN options                */
  MQHOBJ   QueueBrowseHobj;                   /* object handle                 */
  MQOD     QueueBrowseOd = {MQOD_DEFAULT};    /* Object Descriptor             */
  MQLONG   QueueBrowseOpenCode;               /* MQOPEN completion code        */
  MQLONG   QueueBrowseCompCode;               /* MQOPEN completion code        */
  MQLONG   QueueBrowseReason;                 /* reason code                   */
  MQGMO    QueueBrowseGmo = {MQGMO_DEFAULT};  /* get message options           */
  MQMD     ReceiveBrowseMd = {MQMD_DEFAULT};  /* Message Descriptor            */
	BOOL 		 blCreateQue = FALSE;
	BOOL     blOpenQue = FALSE;

	/*dbg(DEBUG,"WmqOpenQueue: QueId=<%s>",pcpQueId);*/
/* OPEN QUEUE FOR BROWSING */
	OpenBrowseOptions =
	    + MQOO_OUTPUT
	    + MQOO_BROWSE            /* open for browsing */
	    + MQOO_FAIL_IF_QUIESCING              /* but not if MQM stopping   */
			+ MQOO_INPUT_SHARED;

	/* get queue status */
	for(ilLc = 0; ilLc < MAXQUEUES; ilLc++)
	{
		if  (strcmp(rgQueInfo[ilLc].QueId,pcpQueId) == 0)
		{
			/* que exist already */
			strcpy(clQueId,rgQueInfo[ilLc].QueId);
			break;
		}
	}

	if (*clQueId == '\0')
	{
		blCreateQue = TRUE;
		blOpenQue = TRUE;
		strcpy(clQueId,pcpQueId);
	}
	else
	{
		if (rgQueInfo[ilLc].IsOpen != TRUE)
		{
			blOpenQue = TRUE;
		}
		else
		{
				*phpQueHandle = 	rgQueInfo[ilLc].QueHandle; 
				 /*dbg(DEBUG,"WmqOpenQueue: Queue already open. QueHandle=<%p>",*phpQueHandle);*/
		}
	}

 /*	if (blOpenQue == TRUE && blCreateQue == FALSE)*/
	if (blOpenQue == TRUE)
	{
		dbg(DEBUG,"WmqOpenQueue: Que <%s> will be opened\n",clQueId);
		strcpy(QueueBrowseOd.ObjectName,clQueId);

		/* open an already existing queue */
	MQOPEN(HconRecBrowse,                      /* connection handle            */
		   &QueueBrowseOd,                /* object descriptor for queue  */
		   OpenBrowseOptions,                 /* open options                 */
		   &QueueBrowseHobj,                     /* object handle                */
		   &QueueBrowseOpenCode,                 /* completion code              */
		   &QueueBrowseReason);                  /* reason code                  */

	  if (QueueBrowseReason != MQRC_NONE)
	  {
			dbg(DEBUG,"WmqOpenQueue: MQOPEN queue <%s> ended with ReasonCode <%d>",clQueId,QueueBrowseReason);
			ilRc = RC_FAIL;
			if (QueueBrowseReason == MQRC_CONNECTION_BROKEN)
			{
				dbg(TRACE,"WmqOpenQueue: No connection to quemanager, retry connect");
				ilRc = WmqConnectToQM(-1,-2,FALSE);
				if (ilRc == RC_SUCCESS)
				{
					ilRc = WmqOpenQueue(pcpQueId,phpQueHandle);
				}
			}
	  }
		else if (blCreateQue == FALSE)
		{
			rgQueInfo[ilLc].IsOpen = TRUE;
			rgQueInfo[ilLc].QueHandle = QueueBrowseHobj;
			*phpQueHandle = QueueBrowseHobj;
			/*dbg(DEBUG,"WmqOpenQueue: QueHandle=<%p>",*phpQueHandle);
			dbg(DEBUG,"WmqOpenQueue: MQOPEN <%s> sucessfull with ReasonCode <%d> BrowseOpenCode <%d>", 
				QueueBrowseOd.ObjectName,QueueBrowseReason,QueueBrowseOpenCode);*/
		}
		else
		{
			/*dbg(DEBUG,"WmqOpenQueue: MQOPEN<%s/%s> sucessfull with ReasonCode <%d> BrowseOpenCode <%d>", 
				QueueBrowseOd.ObjectName,pcpQueId,QueueBrowseReason,QueueBrowseOpenCode);*/
				for(ilLc = 0; ilLc < MAXQUEUES; ilLc++)
				{
					if  (*rgQueInfo[ilLc].QueId == 0)
					{
							strcpy(rgQueInfo[ilLc].QueId,QueueBrowseOd.ObjectName);			
							/*strcpy(rgQueInfo[ilLc].QueId,pcpQueId);*/
							strcpy(rgQueInfo[ilLc].QueName ,pcpQueId);			
							rgQueInfo[ilLc].IsOpen = TRUE;
							rgQueInfo[ilLc].QueHandle = QueueBrowseHobj;
							*phpQueHandle = QueueBrowseHobj;
							/*dbg(DEBUG,"WmqOpenQueue: Que <%s/%s> opened.",QueueBrowseOd.ObjectName,pcpQueId);
							dbg(DEBUG,"WmqOpenQueue: QueHandle=<%p>",*phpQueHandle);*/
							break;
					}
				}
		}


    if (QueueBrowseOpenCode == MQCC_FAILED)
    {
			/*dbg(TRACE,"WmqOpenQueue: unable to open queue.");*/
			ilRc = RC_FAIL;
    }
	}
	else
	{
		/*dbg(TRACE,"WmqOpenQueue: Queue <%s> is already open.",pcpQueId);*/
	}
	return ilRc;
}


int WmqDisconnectFromQM(int ipTries, long lpWait,BOOL bpInitialize)
{
	int ilRc = RC_SUCCESS;
	int ilLc;
	int ilTry,ilServer;
  MQLONG   CompCode = MQCC_FAILED;               /* completion code               */
  MQLONG   CReason = 0L;                /* reason code for MQCONN        */
	char *pclMqServer;
	int ilServerCount;
	char clMqServer[124];

  static char clMqServerList[500] = "\0";
  static char clMyMQServer[500] = "\0";
  static char clNowTime[20] = "\0";
  static char clAlert[256] = "\0";
	static char clMqManager[124] = "\0";

	if (*cgConfigFile == '\0')
	{
		sprintf(cgConfigFile,"%s/%s",getenv("CFG_PATH"),WMQ_CFG_FILE);
		dbg(TRACE,"WmqDisconnectFromQM: ConfigFile <%s>",cgConfigFile);
	}

	if (*clMqServerList == '\0')
	{
		/* we visit this function first time, load server list from config */

			ilRc = iGetConfigEntry(cgConfigFile,"MAIN","MQSERVER",CFG_STRING,clMqServerList);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"WmqDisconnectFromQM: MQServerlist not found in <%s>",cgConfigFile);
				return RC_FAIL;
			} 
			else
			{
					dbg(DEBUG,"WmqDisconnectFromQM: MqServerlist <%s> found.",clMqServerList);
			}
		}

  pclMqServer = clMqServerList;
	
  /* CONNECT TO GIVEN QMANAGER */
  CompCode = MQCC_FAILED;
	ilServerCount = get_no_of_items(clMqServerList);
	

	ilRc = RC_FAIL; /* initialize return code with FAIL */
	ipTries = 1;
	for (ilTry = 0; ilTry <= ipTries &&(CompCode == MQCC_FAILED) ; ilTry++)
	{
		dbg(TRACE,"WmqDisconnectFromQM: Try <%d/%d> Server: <%s>",ilTry,ipTries,pclMqServer);
  	while (CompCode == MQCC_FAILED)
  	{

		char *pclDelimiter;
		char clMqServer[124];
		char clMqServer1[124];

  		pclDelimiter = strchr(pclMqServer,';');
	  	if (pclDelimiter != NULL)
		  {
			  *pclDelimiter = '\0';
    	}

			get_real_item(clMqServer1,pclMqServer,1);
			get_real_item(clMqManager,pclMqServer,2);
		
   		/* SET ENVIRONMENT TO CONNECT TO THE RIGHT QMANAGER */
   		sprintf(clMqServer,"MQSERVER=%s",clMqServer1);
   		ilRc=putenv(clMqServer);

			dbg(TRACE,"WmqDisconnectFromQM: <%s>",clMqServer);

			MQDISC(
				&HconRecBrowse,              /* connection handle             */
	       &CompCode,                    /* completion code                */
	       &CReason);                    /* reason code                    */
			/* report reason and stop if it failed     */
			if (CompCode == MQCC_FAILED)
			{

				dbg(TRACE,"WmqDisconnectFromQM: MQDISC to server <%s> QMGR=<%s>ended with ReasonCode"
						" <%d>. connect to next server",clMqServer, 
							clMqManager,CReason);
				ilRc = RC_FAIL;
			}
			else
			{
					dbg(DEBUG,"WmqDisconnectFromQM: MQDISC disconnected from QManager on server <%s>",clMqServer);
					ilRc = RC_SUCCESS;
					break;
			}
      /* 20050503 JIM: BEG1 avoid core: */
	  	if (pclDelimiter != NULL)
		  {
  			pclMqServer = pclDelimiter + 1;
	  		*pclDelimiter = ';';
        if (pclMqServer[0]==0)
        {
           ilTry = ipTries + 1; /* leave for loop */
           break;               /* leave while loop */
        }
    	}
      else
      { /* leave loop */
         ilTry = ipTries + 1; /* leave for loop */
         break;               /* leave while loop */
      }/* 20050503 JIM: END1 avoid core */
   }
	 if (ilRc != RC_SUCCESS)
	 {
		 pclMqServer = clMqServerList;
		 dbg(TRACE,"WmqDisconnectFromQM: MQDISC failed for all configured servers. Now wait <%d> seconds.",lpWait);
		 sleep(lpWait);
	 }
	 else
	 {
	 	break;
	 }
	}
	ilsAlreadyConnected = FALSE;
	return ilRc;
}

int WmqResetQuetable()
{
	int ilRc = RC_SUCCESS;
	int ilLc;

		for(ilLc = 0; ilLc <= igCountConfigured; ilLc++)
		{
			dbg(DEBUG,"WmqResetQuetable: resetting flags for <%s> <%s>",rgQueInfo[ilLc].QueName,rgQueInfo[ilLc].QueId);
			rgQueInfo[ilLc].IsOpen = 0;
			rgQueInfo[ilLc].QueHandle = 0;
		}
		/* we didnt memset in this tree, so clear rest manually: */
		for(ilLc = igCountConfigured+1; ilLc < MAXQUEUES; ilLc++)
		{
			if  (*rgQueInfo[ilLc].QueId != 0)
			{
				dbg(DEBUG,"WmqResetQuetable: removing que <%s> <%s> from list",rgQueInfo[ilLc].QueName,rgQueInfo[ilLc].QueId);
				rgQueInfo[ilLc].QueName[0] = 0;
				rgQueInfo[ilLc].QueId[0] = 0;
				rgQueInfo[ilLc].IsOpen = 0;
				rgQueInfo[ilLc].QueHandle = 0;
			}
		}
	return ilRc;
}

int WmqConnectToQM(int ipTries, long lpWait,BOOL bpInitialize)
{
	int ilRc = RC_SUCCESS;
	int ilLc;
	int ilTry,ilServer;
  MQLONG   CompCode = MQCC_FAILED;               /* completion code               */
  MQLONG   CReason = 0L;                /* reason code for MQCONN        */
	char *pclMqServer;
	int ilServerCount;
	char clMqServer[124];

  static char clMqServerList[500] = "\0";
  static char clMyMQServer[500] = "\0";
  static char clNowTime[20] = "\0";
  static char clAlert[256] = "\0";
	static char clMqManager[124] = "\0";

	if (*cgConfigFile == '\0')
	{
		sprintf(cgConfigFile,"%s/%s",getenv("CFG_PATH"),WMQ_CFG_FILE);
		dbg(TRACE,"WmqConnectToQM: ConfigFile <%s>",cgConfigFile);
	}

	if (*clMqServerList == '\0')
	{
		/* we visit this function first time, load server list from config */

			ilRc = iGetConfigEntry(cgConfigFile,"MAIN","MQSERVER",CFG_STRING,clMqServerList);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"WmqConnectToQM: MQServerlist not found in <%s>",cgConfigFile);
				return RC_FAIL;
			} 
			else
			{
					dbg(DEBUG,"WmqConnectToQM: MqServerlist <%s> found",clMqServerList);
			}
		}

	if (bpInitialize == TRUE)
	{
		/* 20051018 JIM: read config only at process start, not in fork (i.e. NETIN) */
		if  (igCountConfigured == -1)
		{
			memset(&rgQueInfo,0,sizeof(rgQueInfo));

			for(ilLc = 0; ilLc < MAXQUEUES; ilLc++)
			{

					char clTmpQueNumber[24];
					char clTmpLSQueue[24];
					char clTmpQueId[32];
					char clTmpBufferSize[132];

					sprintf(clTmpQueNumber,"QUE%03d",ilLc);
					ilRc = iGetConfigEntry(cgConfigFile,"QUEUES",clTmpQueNumber,CFG_STRING,clTmpQueId);
					if (ilRc == RC_SUCCESS)
					{
						igCountConfigured++;
						strcpy(rgQueInfo[ilLc].QueName,clTmpQueId);
						strcpy(clTmpLSQueue,clTmpQueId);
						ilRc = iGetConfigEntry(cgConfigFile,"LSQUEUES",clTmpLSQueue,CFG_STRING,clTmpQueId);
						if (ilRc == RC_SUCCESS)
						{
							dbg(DEBUG,"WmqConnectToQM: <%s> is <%s> LS-Queue <%s>",clTmpQueNumber,clTmpLSQueue,clTmpQueId);
							if (atoi(clTmpLSQueue) == mod_id)
							{
								mod_id = atoi(clTmpQueId);
								dbg(DEBUG,"WmqConnectToQM: mod_id set to LS-Queue <%d>",mod_id);
							}
						}
						else
						{
							dbg(DEBUG,"WmqConnectToQM: <%s> is <%s>",clTmpQueNumber,clTmpQueId);
						}
						ilRc = iGetConfigEntry(cgConfigFile,"MQBUFFERSIZE",clTmpLSQueue,CFG_STRING,clTmpBufferSize);
						if (ilRc == RC_SUCCESS)
						{
							lgBufferSize = atol(clTmpBufferSize)* 1024;
							if (lgBufferSize < (4*1024))
							{
							    lgBufferSize = (4*1024);
									dbg(TRACE,"BufferSize too small, set to %d",lgBufferSize);
						  }		
							else if (lgBufferSize > (4000*1024))
							{
							    lgBufferSize = (4000*1024);
									dbg(TRACE,"BufferSize too large, set to %d",lgBufferSize);
							}
							else
							{
									dbg(TRACE,"BufferSize for queue %s  set to %d",clTmpQueNumber,lgBufferSize);
							}
						}
						strcpy(rgQueInfo[ilLc].QueId,clTmpQueId);
					}
					else
					{
						/*dbg(DEBUG,"WmqConnectToQM: <%s> not existing! STOP reading configured queues!",clTmpQueNumber);*/
						break;
					}
			}
		}
		/* 20051018 JIM: don't reread config, only reset flag and handle: */
		else
		{
			for(ilLc = 0; ilLc <= igCountConfigured; ilLc++)
			{
				dbg(DEBUG,"WmqConnectToQM: resetting flags for <%s> <%s>",rgQueInfo[ilLc].QueName,rgQueInfo[ilLc].QueId);
				rgQueInfo[ilLc].IsOpen = 0;
				rgQueInfo[ilLc].QueHandle = 0;
			}
			/* we didnt memset in this tree, so clear rest manually: */
			for(ilLc = igCountConfigured+1; ilLc < MAXQUEUES; ilLc++)
			{
				if  (*rgQueInfo[ilLc].QueId != 0)
				{
					dbg(DEBUG,"WmqConnectToQM: removing que <%s> <%s> from list",rgQueInfo[ilLc].QueName,rgQueInfo[ilLc].QueId);
					rgQueInfo[ilLc].QueName[0] = 0;
					rgQueInfo[ilLc].QueId[0] = 0;
					rgQueInfo[ilLc].IsOpen = 0;
					rgQueInfo[ilLc].QueHandle = 0;
				}
			}
		}
	}


	if (ipTries < 0)
	{
		/* Connect try count not passed as parameter, read from config file */
		  char clTmpStr[124];

			ilRc = iGetConfigEntry(cgConfigFile,"MAIN","MQTRYCONNECT",
					CFG_STRING,clTmpStr);
			if(ilRc != RC_SUCCESS)
			{
				ipTries = 5;
				dbg(TRACE,"WmqConnectToQM: MQTRYCONNECT not found in <%s>! Set retry-count to <%d>",
						cgConfigFile,ipTries);
				return RC_FAIL;
			} 
			else
			{
				  ipTries = atoi(clTmpStr);
					dbg(DEBUG,"WmqConnectToQM: MQTRYCONNECT set to <%s/%d>",clTmpStr,ipTries);
			}
	}

	if (lpWait < -1)
	{
		/* Connect try count not passed as parameter, read from config file */
		  char clTmpStr[124];

			ilRc = iGetConfigEntry(cgConfigFile,"MAIN","MQWAITCONNECT",
					CFG_STRING,clTmpStr);
			if(ilRc != RC_SUCCESS)
			{
				lpWait = 5;
				dbg(TRACE,"WmqConnectToQM: MQWAITCONNECT not found in <%s>! Set wait to <%d>",
						cgConfigFile,lpWait);
				return RC_FAIL;
			} 
			else
			{
				  lpWait = atol(clTmpStr);
					dbg(DEBUG,"WmqConnectToQM: MQWAITCONNECT set to <%s/%d>",clTmpStr,lpWait);
			}
	}
  pclMqServer = clMqServerList;
	
  /* CONNECT TO GIVEN QMANAGER */
  CompCode = MQCC_FAILED;
	ilServerCount = get_no_of_items(clMqServerList);
	

	ilRc = RC_FAIL; /* initialize return code with FAIL */
	for (ilTry = 0; ilTry <= ipTries &&(CompCode == MQCC_FAILED) ; ilTry++)
	{
		dbg(DEBUG,"WmqConnectToQM: Try <%d/%d> Server: <%s>",ilTry,ipTries,pclMqServer);
  	/*while (CompCode == MQCC_FAILED)*/
  	{

		char *pclDelimiter;
		char clMqServer[124];
		char clMqServer1[124];

		pclDelimiter = strchr(pclMqServer,';');
		/*dbg(TRACE,"WmqConnectToQM: pclDelimiter <%p>",pclDelimiter);*/
		if (pclDelimiter != NULL)
		{
			*pclDelimiter = '\0';
			}
			get_real_item(clMqServer1,pclMqServer,1);
			get_real_item(clMqManager,pclMqServer,2);
		
   		/* SET ENVIRONMENT TO CONNECT TO THE RIGHT QMANAGER */
   		sprintf(clMqServer,"MQSERVER=%s",clMqServer1);
			ilRc=putenv(clMqServer);

	{
	char *pclTmp = NULL;

	pclTmp=getenv("MQSERVER");
	if (pclTmp != NULL)
	{
		dbg(DEBUG,"WmqConnectToQM: MQSERVER = <%s>",pclTmp);
	}
	else
	{
		dbg(TRACE,"WmqConnectToQM: MQSERVER not defined as environment variable!");
	}
	}
			dbg(TRACE,"WmqConnectToQM: Server=<%s> QMGR=<%s>",clMqServer,clMqManager);

			MQCONN(clMqManager,               /* queue manager                  */
				&HconRecBrowse,              /* connection handle             */
	       &CompCode,                    /* completion code                */
	       &CReason);                    /* reason code                    */
			/* report reason and stop if it failed     */
			/*if (CompCode == MQCC_FAILED)*/
			/*dbg(TRACE,"WmqConnectToQM: MQCONN<%s> to server <%s> QMGR=<%s>ended with ReasonCode"
						" <%d>  CompCode <%d> connect to next server",mod_name,clMqServer,clMqManager,CReason,CompCode);*/
			if (CReason != MQRC_NONE)
			{
				if (CReason == MQRC_ALREADY_CONNECTED )
				{
						dbg(TRACE,"Already connected to queue manager <%s>",clMqManager);
						ilsAlreadyConnected = TRUE;
						break;
				}
				else
				{
					dbg(TRACE,"WmqConnectToQM: MQCONN<%s> to server <%s> QMGR=<%s> ended with ReasonCode"
							" <%d>! Connect to next server",mod_name,clMqServer, 
								clMqManager,CReason);
					ilRc = RC_FAIL;
				}
			}
			else
			{
					dbg(TRACE,"WmqConnectToQM: MQCONN<%s> connected to OMGR <%s> on server <%s>",mod_name,clMqManager,clMqServer);
					ilRc = RC_SUCCESS;
					break;
			}
      /* 20050503 JIM: BEG2 avoid core: */
	  	if (pclDelimiter != NULL)
		  {
  			pclMqServer = pclDelimiter + 1;
	  		*pclDelimiter = ';';
        if (pclMqServer[0]==0)
        {
           ilTry = ipTries + 1; /* leave for loop */
           break;               /* leave while loop */
        }
    	}
      else
      { /* leave loop */
         ilTry = ipTries + 1; /* leave for loop */
         break;               /* leave while loop */
      }/* 20050503 JIM: END2 avoid core */

		/*}
		else
		{
			break;
		}
		*/
   }
	 if (ilRc != RC_SUCCESS)
	 {
		 pclMqServer = clMqServerList;
		 dbg(TRACE,"WmqConnectToQM: MQCONN failed for all configured servers! Now wait <%d> sec.",lpWait);
		 sleep(lpWait);
	 }
	 else
	 {
	 	break;
	 }
	}
    
	return ilRc;
}


	 /*************************
	 1. QUE_PUT				Queue a message ( end of queue )
	 2. QUE_PUT_FRONT	Queue a message to front of queue
   3. QUE_GET				Get a message from queue
   4. QUE_ACK				Acknowledge a message ( msg is removed)
   5. QUE_CREATE		Create a new queue
   6. QUE_DELETE		Delete an existing queue
   7. QUE_EMPTY			Discard all items from a queue
	 8. QUE_GO				Set queue on go
	 9. QUE_HOLD			Set queue on hold
	10. QUE_WAKE_UP		Wake up signal from scheduler
	11. QUE_DUMP			Dump queue headers
	12. QUE_RESP			Get response on QUE_CREATE request
	21. QUE_GETNW			Get a message without wait
	26. QUE_GETBIG		Get a message of an unknown size
										que(...,char *msg) must point to a ichar pointer which
										will be used to allocate enough space for the message
										this pointer must be freed maually after usage

struct  Item_Header {
  struct msgbuf msg_header;
  int   function;
  int   route;
  int   priority;
  int   msg_length;
  int   originator;
  int   block_nr;
  char    text[1];
};

typedef struct Item_Header ITEM;
#define ITEM_SIZE sizeof(ITEM)

	 *********************/


int que(int func,int route,int mod_id,int priority,int len,ITEM **msg)
{
	int ilRc = RC_SUCCESS;
	int ilMqRc=0;
	long llActSize;
	char clModId[24];
	char clQueId[24];
	char clDestQue[24];
	char *pclDest;
	long llWait = 0;
  int  ilQueMin = 0;

	static char *pclData = NULL;
	static char *pclTmpPtr = NULL;
	static char clMessageId[124] = "";
	static ITEM *prlItem = NULL;
	EVENT *prlEvent = NULL;
	static char *pclSendText = NULL;
	static long llSendTextLen = 0;

	MQHOBJ hlQueHandle = -1;

	if (prlItem == NULL)
	{
			/*prlItem = calloc(1,max(sizeof(ITEM)| MAX_EVENT_SIZE));*/
			prlItem = calloc(1, MAX_EVENT_SIZE);
			if (prlItem == NULL)
			{
				dbg(TRACE,"que: ERROR: Unable to Alloc. of <%d> bytes for ITEM. Return with RC_FAIL!",sizeof(ITEM));
				return RC_FAIL;
			}
			/*dbg(DEBUG,"que: Alloc. of <%d> bytes for ITEM.",sizeof(ITEM));*/
	}
	


	switch (func)
	{
		case QUE_PUT:
	  case QUE_PUT_FRONT:
				prlEvent = (EVENT *)msg;
				/*dbg(DEBUG,"que: CedaToMqUsingEvent follows");*/
				ilRc = CedaToMqUsingEvent(prlEvent,&pclSendText,&llSendTextLen,priority,NULL,NULL);
				/*dbg(DEBUG,"que: CedaToMqUSingEvent() returns <%d> <\n%s>",ilRc,pclSendText);*/

				if (ilRc == RC_SUCCESS) 
				{
					sprintf(clDestQue,"%d",route);
					sprintf(clModId,"%d",mod_id);
					ilRc = WmqSendMessage(clModId,clDestQue,&hlQueHandle,priority,pclSendText);
				}		
			break;
		case QUE_GETBIGNW:
		case QUE_GETNW:
					 llWait = -1;   
		case QUE_GET:
		case QUE_GETBIG:
				 	/*dbg(TRACE,"que: msg=%p %x *msg=%p", msg ,(ITEM *)msg,(*(ITEM **)msg));*/
				  if ( *msg == NULL && (func == QUE_GETBIG | func == QUE_GETBIGNW) )
					{
						(*(ITEM **)msg) = realloc((*(ITEM **)msg),((I_SIZE*2) + 4));
						/*dbg(DEBUG,"que: QUE_GET(x) realloc <%d> Bytes for msg!",((I_SIZE*2) + 4));*/
					}
					sprintf(clModId,"%d",mod_id);

					pclTmpPtr = WmqGetMessage(clModId,&hlQueHandle,llWait,TRUE,clMessageId,&pclData,&ilMqRc);

					ilRc = ilMqRc;

					/*if (ilRc != QUE_E_NOMSG)
						dbg(DEBUG,"que: WmqGetMessage() returns <%d> <\n%s>",ilRc,pclData);*/
					if (ilRc == RC_SUCCESS)
					{
						switch (func)
						{
							case QUE_GETBIGNW:
							case QUE_GETBIG:
								/*ilRc = MqToCeda(&prlItem,pclData,NULL,NULL);*/
								ilRc = MqToCeda(msg,pclData,NULL,NULL);
								/*(ITEM *) *msg = prlItem;*/
								/* prlItem = *msg;*/

								break;
							case QUE_GET:
							case QUE_GETNW: 
								ilRc = MqToCeda(&prlItem,pclData,NULL,NULL);
								memcpy(msg,prlItem,prlItem->msg_length+sizeof(ITEM));
								break; 
						}
					}
			break;
		case QUE_ACK:
					ilRc = WmqCommit();
			break;
		case QUE_CREATE:
				ilRc = RC_SUCCESS;
			break;
		case QUE_EMPTY:
		case QUE_WAKE_UP:
			break;
		case QUE_RESP:
		case QUE_WMQ_RESP:
				*clQueId = '\0';
        ilQueMin= 20000;
				if (func==QUE_WMQ_RESP)
				{  /* 20050509 JIM: */
           ilQueMin= 30000;
        }
			 ilRc = WmqCreateDynamicQueue(clQueId,&hlQueHandle,ilQueMin);
			 dbg(DEBUG,"que: WmqCreateDynamicQueue() for <%s> returns <%d>",clQueId,ilRc);
			 if (ilRc == RC_SUCCESS)
			 {
			 	 ITEM *prlItem;
				 int ilQueId;

			 	 prlItem = (ITEM *)msg;
				 ilQueId = atoi(clQueId);
				 prlItem->originator = atoi(clQueId);
			 	 /*dbg(DEBUG,"que: originator = <%d>",prlItem->originator);*/
			 }
			break;
		case QUE_DELETE:
		sprintf(clModId,"%d",mod_id);
		ilRc =  WmqCloseQueue(clModId);
		dbg(DEBUG,"que: WmqCloseQueue() for <%s> returns <%d>",clModId,ilRc);
		break;
	}
	return ilRc;
}

static void TrimRight(char *pcpBuffer)
{

  if (strlen(pcpBuffer) > 0)
  {
  	char *pclBlank = &pcpBuffer[strlen(pcpBuffer)-1];

     while (isspace(*pclBlank) && pclBlank != pcpBuffer)
     {
        *pclBlank = '\0';
        pclBlank--;
     }
  }

} /* End of TrimRight */

void SendNextForQueue(int module)
{
	return ;
}

int DeleteOldQueues(char pcpQueName)
{
	return RC_SUCCESS;
}
