
#ifndef BOOL
#define BOOL int
#endif


extern int WmqOpenQueue(char *pcpQueId, MQHOBJ *phpQueHandle);
/*extern int que(int func,int route,int module,
			int priority,int len,char *msg);*/
extern int que(int func,int route,int module,
			int priority,int len,ITEM **msg);

extern int WmqDisconnectFromQM(int ipTries, long lpWait,int bpInitialize);

int WmqSendMessage(
char *pcpModId, /* The mod_id which is passed to the process as argv[1], this parameter will be used as originator */
  char *pcpQueId, /* Id of the queue to which this message will be send */
	MQHOBJ *phpHobj,
	int     ipPriority,    /* Priority of this message */
	char   *pcpData);

char* WmqGetMessage(
		char *pcpQueueId, /* The id of the queue from whicht this message will be retrieved */ 
	MQHOBJ *phpHobj,
	long		lpWait,         /* -1 for blocking call, 0 return immediately fi no message is available, any positive value wait milliseconds for a message to become available */
	BOOL	  bpBrowse,       /*TRUE = the queue is only browsed, FALSE = the message will be retrieved from the queue */
	char *pcpMessageId,      /* WMQ-Id of the message */
	char **pcpData,int *ipRc);




extern int WmqRemoveMessage(
		char *pcpQueueId, /* The id of the queue from whicht this message will be retrieved */
  MQHCONN *phpConnectionHandle);      /* Connection Handle */

extern int WmqOverview(
			char *pcpQueueId, /* The id of the queue from whicht this message will be retrieved */ 
  MQHCONN *phpConnectionHandle);      /* Connection Handle */
	
	
/*extern int MqToCeda(ITEM **prpItem, char *pcpData);*/
extern int	CedaToMq(EVENT *prpEvent,char **pcpDest,char *pcpAddFields,char *pcpAddData);
extern int MqToCeda(ITEM **prpItem, char *pcpData,char **pcpAddFields,char **pcpAddData);

