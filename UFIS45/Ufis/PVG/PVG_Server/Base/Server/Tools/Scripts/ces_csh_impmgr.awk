# $Id: Ufis/PVG/PVG_Server/Base/Server/Tools/Scripts/ces_csh_impmgr.awk 1.4 2005/11/15 23:35:33SGT jim Exp  $
# 20051115 JIM: Overnight flights: arrival: changing STD to day before
#                                  departure: changing STA to day later
BEGIN {
# print arrival and departure field list
processed=-1;
}
END {
# main loop will process pairs of arrival and departure. after last pair
# impmgr expects an emty line
  if (processed>0)
  {
     print ""
     print "\" wait=1 tws=\" \" sel=\"IMP,IMPORT\" user=sched wks=impmgr twe=\"PVG,TAB,impmgr\""
  }
}
function DayBefore(YYYYmmdd) {
   Y=substr(YYYYmmdd,1,4)+0
   m=substr(YYYYmmdd,5,2)+0
   d=substr(YYYYmmdd,7,2)-1
   if (d==0)
   {
     m--
     if (m==0)
     {
       m=12
       Y--
     }
     if ((m==1) || (m==3) || (m==5)  || (m==7)  || (m==8)  || (m==10)  || (m==12) )
     {
       d=31
     }
     else if (m==2)
     {
       d=28
     }
     else 
     {
       d=30
     }

     if (d==28)
     {
        if ((Y%4 == 0) && (Y%100 !=0))
        {
           d=29
        }
     }
   }
   return sprintf("%04d%02d%02d",Y,m,d)
}

function DayAfter(YYYYmmdd) {
   Y=substr(YYYYmmdd,1,4)+0
   m=substr(YYYYmmdd,5,2)+0
   d=substr(YYYYmmdd,7,2)+1
   if ((m==1) || (m==3) || (m==5)  || (m==7)  || (m==8)  || (m==10)  || (m==12) )
   {
     InNextM=32
   }
   else if (m==2)
   {
     InNextM=29
     if ((Y%4 == 0) && (Y%100 !=0))
     {
       InNextM=30
     }
   }
   else 
   {
     InNextM=31
   }
   if (d==InNextM)
   {
     d=1
     m++
     if (m==13)
     {
       m=1
       Y++
     }
   }
   return sprintf("%04d%02d%02d",Y,m,d)
}

length >0 {
 staa=substr($0,151,4)
 stda=substr($0,155,4)
 stad=substr($0,322,4)
 stdd=substr($0,326,4)
# if (sta="1111")
# {
# }
 vpfra=substr($0,84,8)staa
 vptoa=substr($0,92,8)stda
 vpfrd=substr($0,255,8)stad
 vptod=substr($0,263,8)stdd
      if (processed==-1)
      {
        print "qput impmgr IMP tab=AFTTAB \\"
        print "fld=\"FLNO,AIRL,FLTN,FLNS,JFNO,JCNT,ACT3,VPFR,VPTO,FRQD,FRQW,ADID,FLTI,TTYP,ORG4,DES4,VIAL,VIAN,STOD,STOA,REGN,FTYP,STAT";
        print "FLNO,AIRL,FLTN,FLNS,JFNO,JCNT,ACT3,VPFR,VPTO,FRQD,FRQW,ADID,FLTI,TTYP,ORG4,DES4,VIAL,VIAN,STOD,STOA,REGN,FTYP,STAT\" \\";
        printf "dat=\""
        processed++;
      }
 
    DefineLenght();
    arr="\n"
    dep="\n"
    if (length > 175) 
    {
       # print "rotation/arrival"
       shifted=", arrival"
       if (staa>stda)
       {
         shifted=shifted", departed day before"
         vpfra=DayBefore(substr($0,84,8))staa
       }
       if ((vpfra>REJ) || (vptoa>REJ))
       {
          ProcessLine();
          arr=rline
          print "processed: "substr($0,1,8),vpfra" or "vptoa" > "REJ,"( times: "substr($0,151,8)")"shifted >"prossed.log"
          processed++;
       }
       else
       {
          print "rejected:  "substr($0,1,8),vpfra" and "vptoa" < "REJ,"( times: "substr($0,151,8)")"shifted >"prossed.log"
       }
       # print "rotation/departure"
       shifted=", departure"
       if (stad>stdd)
       {
         shifted=shifted", arriving day later"
         vptod=DayAfter(substr($0,263,8))stdd
       }
       if ((vpfrd>REJ) || (vptod>REJ)) 
       {
          ProcessLine();
          dep=rline
          print "processed: "substr($0,172,8),vpfrd" or "vptod" > "REJ,"( times: "substr($0,322,8)")"shifted >"prossed.log"
          processed++;
       }
       else
       {
          print "rejected:  "substr($0,172,8),vpfrd" and "vptod" < "REJ,"( times: "substr($0,322,8)")"shifted >"prossed.log"
       }
    }
    else if (substr($0,80+26+1,1) == "A")
    {
       # print "single arrival"
       shifted=", arrival"
       if (staa>stda)
       {
         shifted=shifted", departed day before"
         vpfra=DayBefore(substr($0,84,8))staa
       }
       if ((vpfra>REJ) || (vptoa>REJ))
       {
         ProcessLine();
         arr=rline
         print "processed: "substr($0,1,8),vpfra" or "vptoa" > "REJ,"( times: "substr($0,151,8)")"shifted >"prossed.log"
         processed++;
       }
       else
       {
          print "rejected:  "substr($0,1,8),vpfra" and "vptoa" < "REJ,"( times: "substr($0,151,8)")"shifted >"prossed.log"
       }
    }
    else 
    {
       # print "single departure"
       shifted=", departure"
       if (staa>stda)
       {
         shifted=shifted", arriving day later"
         vptoa=DayAfter(substr($0,84,8))stda
       }
       if ((vpfra>REJ) || (vptoa>REJ))
       {
         ProcessLine();
         dep=rline
         print "processed: "substr($0,1,8),vpfra" or "vptoa" > "REJ,"( times: "substr($0,151,8)")"shifted >"prossed.log"
         processed++;
       }
       else
       {
          print "rejected:  "substr($0,1,8),vpfra" and "vptoa" < "REJ,"( times: "substr($0,151,8)")"shifted >"prossed.log"
       }
    }
    if ((arr!="\n") || (dep!="\n"))
    {
      printf arr
      printf dep
    }
  if (processed>7)
  {
     print ""
     print "\" wait=1 tws=\" \" sel=\"IMP,IMPORT\" user=sched wks=impmgr twe=\"PVG,TAB,impmgr\""
     processed=-1;
  }
}

function ProcessLine()
{
    rline=""
    ProcFlnu(substr($0,v_start,v_flnu_len));
    v_start=v_start+v_flnu_len;

    pcgJfno="";
    lgJcnt=0;
    for (v_jfno_cnt=1 ; v_jfno_cnt <= jfno_counter; v_jfno_cnt++ ) {
      if (v_jfno_cnt <= v_jfno_max) {
        pcgJfno=ProcJfno(substr($0,v_start, v_jfno_len));
        if (pcgJfno != v_jfno_empty)
        {
          rline=rline""sprintf("%s", pcgJfno);
          lgJcnt++;
        }
      }
      v_start=v_start+v_jfno_len;
    }
    rline=rline""sprintf(",%d", lgJcnt);
#    for (v_jfno_cnt=v_jfno_cnt ; v_jfno_cnt <= v_jfno_max; v_jfno_cnt++ ) {
#      printf("%-9s",v_jfno_empty);
#    }

  rline=rline""sprintf(",%-*s,",v_act3_len,substr($0,v_start,v_act3_len));
    v_start=v_start+v_act3_len;

    VPFR=sprintf("%-*s",v_vpfr_len,substr($0,v_start,v_vpfr_len));
  rline=rline""sprintf("%-*s,",v_vpfr_len,substr($0,v_start,v_vpfr_len));
    v_start=v_start+v_vpfr_len;
  rline=rline""sprintf("%-*s,",v_vpto_len,substr($0,v_start,v_vpto_len));
    v_start=v_start+v_vpto_len;

  rline=rline""sprintf("%-*s,",v_freq_len,substr($0,v_start,v_freq_len));
    v_start=v_start+v_freq_len;

    # FRQW: week frequency
    rline=rline""sprintf("1,");

    v_adid=substr($0,v_start,v_adid_len) 
    v_start=v_start+v_adid_len;
    rline=rline""sprintf("%s,",v_adid);

    rline=rline""sprintf("%-*s,",v_flti_len,substr($0,v_start,v_flti_len));
    v_start=v_start+v_flti_len;

    rline=rline""sprintf("%-*s,",v_ttyp_len,substr($0,v_start,v_ttyp_len));
    v_start=v_start+v_ttyp_len;

    if (substr(v_adid,1,1) == "A") {
      rline=rline""sprintf("%-*s,",v_orig_len,substr($0,v_start,v_orig_len));
      v_start=v_start+v_orig_len;
      rline=rline""sprintf("ZSPD,");
      v_start=v_start+v_dest_len;
    }
    else {
      rline=rline""sprintf("ZSPD,");
      v_start=v_start+v_orig_len;
      rline=rline""sprintf("%-*s,",v_dest_len,substr($0,v_start,v_dest_len));
      v_start=v_start+v_dest_len;
    }

    for (v_via_cnt=1 ; v_via_cnt <= 6; v_via_cnt++ ) {
      # if there is a given via, increase vian (number of via's)
      if (substr($0,v_start,v_via_len) != v_via_empty) {
        v_vian = v_vian + 1;
      }
     rline=rline""sprintf("%-*c",v_vial_start_byte_len,v_vial_start_byte);
     rline=rline""sprintf("%-*c",v_via3_len,v_via3_empty);
     rline=rline""sprintf("%-*s",v_via_len,substr($0,v_start,v_via_len));
     rline=rline""sprintf("%-*c",v_vial_len-v_vial_start_byte_len-v_via_len-v_via3_len,v_one_space);
      v_start=v_start+v_via_len+v_comma_sep_len;
    }
    # substract v_start by one because of the loop 
    # add one v_comma_sep_len more than needed
    v_start=v_start-v_comma_sep_len;
    # do not change the start_pos for VIAN, VIAN not from input file
    rline=rline""sprintf(",%-*d,",v_vian_len,v_vian);

    STOD=sprintf("%-*s",v_stod_len,substr($0,v_start,v_stod_len));
    v_start=v_start+v_stod_len;
    STOA=sprintf("%-*s",v_stoa_len,substr($0,v_start,v_stoa_len));
    v_start=v_start+v_stoa_len;
    if (STOD == "    ")
    {
      rline=rline""sprintf (" ,");
    }
    else
    {
      rline=rline""sprintf ("%s%s00,",VPFR,STOD);
    }
    if (STOA == "    ")
    {
      rline=rline""sprintf (" ,");
    }
    else
    {
      rline=rline""sprintf ("%s%s00,",VPFR,STOA);
    }

    rline=rline""sprintf("%-*s,",v_regn_len,substr($0,v_start,v_regn_len));
    v_start=v_start+v_regn_len;

    FTYP=sprintf("%-*s",v_ftyp_len,toupper(substr($0,v_start,v_ftyp_len)));
    rline=rline""sprintf("%s,",FTYP);
    # printf("#FTYP=O",v_ftyp_len,toupper(substr($0,v_start,v_ftyp_len)));
    v_start=v_start+v_ftyp_len;

    if (FTYP == " ")
    {
       STAT = " ";
    } 
    else if (FTYP == "S")
    {
       STAT = " ";
    } 
    else if (FTYP == "O")
    {
       STAT = " ";
    } 
    else if (FTYP == "D")
    {
       STAT = "DIV";
    } 
    else if (FTYP == "X")
    {
       STAT = "CXX";
    } 
    else if (FTYP == "R")
    {
       STAT = "RRO";
    } 
    else if (FTYP == "N")
    {
       STAT = "NOP";
    } 
    else if (FTYP == "T")
    {
       STAT = " ";
    } 
    else if (FTYP == "G")
    {
       STAT = " ";
    } 
    else 
    {
       STAT = " ";
    }
    rline=rline""sprintf("%s",STAT);

    rline=rline""sprintf("\n");

} 
function ProcFlnu(pclFlight)
{

#  printf("%s,",pclFlight);
  flcd=substr(pclFlight,1,3) 
  flnt=substr(pclFlight,4) 

  flcdl=substr(flcd,3,1) 
  if ((flcdl >=0) && (flcdl <=9) ) {
    flcd = substr(pclFlight,1,2)
    flnt = substr(pclFlight,3);
  }
#  printf("%-3s,",flcd)

  flno = "" 
  flsd = "" 
  for (i=1 ; i <= length(flnt); i++ ) {
    cp=substr(flnt,i,1)
    if ((cp >=0) && (cp <= 9)) {
      flno = flno cp;
    } else {
      flsd = flsd cp;  
    }
  }
rline=rline""sprintf("%-3s%3s%-1c,",flcd,flno,flsd)
 rline=rline""sprintf("%-3s,",flcd)
rline=rline""sprintf("%-5s,",flno);
rline=rline""sprintf("%-1c,",flsd);
  return;
}

# like ProcFlno but without delimiter "," 
function ProcJfno(pclFlight)
{

  flcd=substr(pclFlight,1,3) 
  flnt=substr(pclFlight,4) 

  flcdl=substr(flcd,3,1) 
  if ((flcdl >=0) && (flcdl <=9) ) {
    flcd = substr(pclFlight,1,2)
    flnt = substr(pclFlight,3);
  }
# printf("%-3s",flcd)

  flno = "" 
  flsd = "" 
  for (i=1 ; i <= length(flnt); i++ ) {
    cp=substr(flnt,i,1)
    if ((cp >=0) && (cp <= 9)) {
      flno = flno cp;
    } else {
      flsd = flsd cp;  
    }
  }
#  printf("%-5s",flno);
#  printf("%-1c",flsd);
  
  return sprintf("%-3s%-5s%-1c",flcd,flno,flsd);
}

function DefineLenght()
{
  v_start=1;
  v_comma_sep_len = 1;
  v_one_space = " ";

  v_flnu_len = 8;
  v_jfno_len = v_flnu_len;
  v_jfno_empty = "         ";
  v_jfno_max = 10;
  jfno_counter = 9;
  v_act3_len = 3;
  v_vpfr_len = 8;
  v_vpto_len = 8;
  v_freq_len = 7;
  v_adid_len = 1;
  v_flti_len = 1;
  v_ttyp_len = 5;
  v_orig_len = 4;
  v_dest_len = 4;
  v_via_len = 4;
  v_via_empty = "    ";
  v_via3_len = 3;
  v_via3_empty = "   ";
  v_vial_len = 120;
  v_vial_start_byte = " ";
  v_vial_start_byte_len = 1;
  v_vian = 0;
  v_vian_len = 1;
  v_stod_len = 4;
  v_stoa_len = 4;
  v_regn_len = 12;
  v_ftyp_len = 1;
  return;
}
