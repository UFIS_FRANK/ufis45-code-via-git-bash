# $Id: Ufis/PVG/PVG_Server/Base/Server/Tools/Scripts/schedule.sh 1.4 2005/11/15 23:34:30SGT jim Exp  $
# Please note: every output (i.e. via "echo") that is not redirected to
# the file schedule.log or an other file will be written to  a mail, if 
# this skript is running in a cron-job. To avoid this, pipe the output
# of the cron-job to a file or to /dev/null.
# 20051115 JIM: times in input file are local, so reject with local time
#
NOW=`date |awk '{ print $2""$3""$4 }'`    # used for protocol output
# REJ=`date -u +%Y%m%d%H%M`                 # used to reject records less than REJ
REJ=`date +%Y%m%d%H%M`                 # used to reject records less than REJ
CES_NDS_FILE=ces_nds.dat
cd /home/cdi

if [ -f ${CES_NDS_FILE} ] ; then 
   echo 'processing import_via_impmgr ' $CES_NDS_FILE
   echo "cat $CES_NDS_FILE | awk -v REJ=${REJ} -f ces_csh_impmgr_REJ.awk | ksh"
   cat $CES_NDS_FILE | awk -v REJ=${REJ} -f ces_csh_impmgr_REJ.awk  | ksh
   echo "mv $CES_NDS_FILE Old_Sched/`basename $CES_NDS_FILE`.$NOW"
   mv $CES_NDS_FILE Old_Sched/`basename $CES_NDS_FILE`.$NOW
   echo "$CES_NDS_FILE Processed! `date` / `date -u`" >> schedule.log
else
   echo "$CES_NDS_FILE NOT FOUND! Not Processed! `date` / `date -u`" >> schedule.log
fi

