// UfisWatchDogMainWnd.cpp : implementation file
//

#include "stdafx.h"
#include "ufiswatchdog.h"
#include "UfisWatchDogMainWnd.h"
#include "PsApi.h"
#include "Vdmdbg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

typedef BOOL (CALLBACK *PROCENUMPROC)(DWORD, WORD, LPSTR, LPARAM);

typedef struct {
   DWORD          dwPID;
   PROCENUMPROC   lpProc;
   DWORD          lParam;
   BOOL           bEnd;
} EnumInfoStruct;

typedef struct
{
	DWORD	idProcess;
	WORD	hTask;
}	InfoStruct16Bit;

BOOL WINAPI EnumProcs(PROCENUMPROC lpProc, LPARAM lParam);

BOOL WINAPI Enum16(DWORD dwThreadId, WORD hMod16, WORD hTask16,PSZ pszModName, PSZ pszFileName, LPARAM lpUserDefined);

BOOL CALLBACK MyProcessEnumerator(DWORD dwPID, WORD wTask,char * szProcess, LPARAM lParam) 
{
	CString olProcess(strrchr(szProcess,'\\')+1);

	if (wTask == 0)	// 32 bit process
	{
		InfoStruct16Bit *polInfo = NULL;
		if (!CUfisWatchDogMainWnd::somThis->om16BitProcesses.Lookup(olProcess,(void *&)polInfo))
		{
			polInfo = new InfoStruct16Bit;
			polInfo->idProcess = dwPID;
			polInfo->hTask = wTask;
			CUfisWatchDogMainWnd::somThis->om16BitProcesses.SetAt(olProcess,polInfo);
		}
		
	}
	else	// 16 bit process
	{
		InfoStruct16Bit *polInfo = NULL;
		if (!CUfisWatchDogMainWnd::somThis->om16BitProcesses.Lookup(olProcess,(void *&)polInfo))
		{
			polInfo = new InfoStruct16Bit;
			polInfo->idProcess = dwPID;
			polInfo->hTask = wTask;
			CUfisWatchDogMainWnd::somThis->om16BitProcesses.SetAt(olProcess,polInfo);
		}
	}
	return TRUE;
}

BOOL WINAPI Enum16(DWORD dwThreadId, WORD hMod16, WORD hTask16,PSZ pszModName, PSZ pszFileName, LPARAM lpUserDefined) 
{

   BOOL bRet;

   EnumInfoStruct *psInfo = (EnumInfoStruct *)lpUserDefined;

   bRet = psInfo->lpProc(psInfo->dwPID, hTask16, pszFileName,psInfo->lParam);

   if (!bRet) 
      psInfo->bEnd = TRUE;

   return !bRet;
} 


/////////////////////////////////////////////////////////////////////////////
// CUfisWatchDogMainWnd
CUfisWatchDogMainWnd*	CUfisWatchDogMainWnd::somThis = NULL;


IMPLEMENT_DYNCREATE(CUfisWatchDogMainWnd, CFrameWnd)

CUfisWatchDogMainWnd::CUfisWatchDogMainWnd()
:imTimer(0)
,omApplName("WatchDog")
,imCheckDelay(0)
{
	DWORD dwStyle   = WS_OVERLAPPEDWINDOW;
	DWORD dwExStyle = WS_EX_TOOLWINDOW;
	CRect olRect(100,100,900,600);
	this->Create(NULL,_T("Ufis Watch dog"),dwStyle,olRect,NULL,NULL,dwExStyle,NULL);
	
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	somThis = this;

	NOTIFYICONDATA nId;
	nId.cbSize = sizeof(nId);
	nId.hIcon  = this->m_hIcon;
	nId.hWnd   = this->m_hWnd;
	strcpy(nId.szTip,"Ufis Watch dog active");
	nId.uID	   = ID_NOTIFY_ICON;
	nId.uFlags = NIF_ICON|NIF_TIP|NIF_MESSAGE;
	nId.uCallbackMessage = ID_SHELL_NOTIFY;

	this->LogMessage(" Starting up..");

	::Shell_NotifyIcon(NIM_ADD,&nId);

	EvaluateConfiguration();
}

CUfisWatchDogMainWnd::~CUfisWatchDogMainWnd()
{
	NOTIFYICONDATA nId;
	nId.cbSize = sizeof(nId);
	nId.hIcon  = this->m_hIcon;
	nId.hWnd   = this->m_hWnd;
	strcpy(nId.szTip,"Ufis Watch dog active");
	nId.uID	   = ID_NOTIFY_ICON;
	nId.uFlags = NIF_ICON|NIF_TIP|NIF_MESSAGE;
	nId.uCallbackMessage = ID_SHELL_NOTIFY;

	this->LogMessage(" Shutting down..");

	KillTimer(this->imTimer);
	
	::Shell_NotifyIcon(NIM_DELETE,&nId);
}


BEGIN_MESSAGE_MAP(CUfisWatchDogMainWnd, CFrameWnd)
	//{{AFX_MSG_MAP(CUfisWatchDogMainWnd)
	ON_WM_TIMER()
	ON_WM_ACTIVATE()
    ON_MESSAGE(ID_SHELL_NOTIFY, OnNotifyIcon)
	ON_WM_CREATE()
	ON_COMMAND(ID_RESTORE,	OnMenuRestore)
	ON_COMMAND(ID_MINIMIZE, OnMenuMinimize)
	ON_COMMAND(ID_MAXIMIZE, OnMenuMaximize)
	ON_COMMAND(ID_CLOSE,	OnMenuClose)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUfisWatchDogMainWnd message handlers

void CUfisWatchDogMainWnd::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	static int blInProgress = 0;

	if (!this->IsIconic())
		this->ShowWindow(SW_SHOWMINIMIZED);

	if (blInProgress == 0)
	{
		blInProgress = 1;
		if (this->bmCheckActive && nIDEvent == this->imTimer)
		{
			CheckApplications();
			Check16BitApplications(false);
			Check32BitApplications(false);

			this->LogMessage(" Sleeping...");
			Sleep(this->imCheckDelay);
		}

		CheckApplications();
		Check16BitApplications(true);
		Check32BitApplications(true);

		blInProgress = 0;
	}
	
	CFrameWnd::OnTimer(nIDEvent);
}

void CUfisWatchDogMainWnd::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized) 
{
	CFrameWnd::OnActivate(nState, pWndOther, bMinimized);
	
	// TODO: Add your message handler code here
}

void CUfisWatchDogMainWnd::Check16BitApplications(bool bpTimed)
{
	this->LogMessage(" Checking 16 bit applications..");


	CTime olCurrentTime = CTime::GetCurrentTime();


	if (bpTimed)
	{
		this->bmTerminate16BitProcesses = false;
		for (int i = 0; i < this->om16BitTimeCheck.GetSize(); i++)
		{
			if (this->om16BitTimeCheck[i] == -1)	// don't start !
				continue;

			// check the time frame (1 minute should be enough)
			CTime olTime((time_t)this->om16BitTimeCheck[i]);
			if (olTime.GetHour() != olCurrentTime.GetHour() || olTime.GetMinute() != olCurrentTime.GetMinute())
				continue;
			
			this->bmTerminate16BitProcesses = true;
			break;
		}

		if (this->bmTerminate16BitProcesses)
		{
			this->Terminate16BitProcesses();
		}

		this->Restart16BitProcesses(bpTimed);
	}
	else
	{
		this->bmTerminate16BitProcesses = false;
		for (int i = 0; i < this->om16BitRestartAppl.GetSize();i++)
		{
			InfoStruct16Bit *polInfo = NULL;
			if (!this->om16BitProcesses.Lookup(this->om16BitRestartAppl[i],(void *&)polInfo))
			{
				this->bmTerminate16BitProcesses = true;
				break;
			}
		}

		if (this->bmTerminate16BitProcesses)
		{
			this->Terminate16BitProcesses();
		}

		this->Restart16BitProcesses(bpTimed);
	}
	
}

void CUfisWatchDogMainWnd::Check32BitApplications(bool bpTimed)
{
	this->LogMessage(" Checking 32 bit applications..");
	
	CTime olCurrentTime = CTime::GetCurrentTime();

	if (bpTimed)
	{
		this->bmTerminate32BitProcesses = false;
		for (int i = 0; i < this->om32BitTimeCheck.GetSize(); i++)
		{
			if (this->om32BitTimeCheck[i] == -1)	// don't start !
				continue;

			// check the time frame (1 minute should be enough)
			CTime olTime((time_t)this->om32BitTimeCheck[i]);
			if (olTime.GetHour() != olCurrentTime.GetHour() || olTime.GetMinute() != olCurrentTime.GetMinute())
				continue;
			
			this->bmTerminate32BitProcesses = true;
			break;
		}

		if (this->bmTerminate32BitProcesses)
		{
			this->Terminate32BitProcesses();
		}

		this->Restart32BitProcesses(bpTimed);
	}
	else
	{
		if (this->bmTerminate32BitProcesses)
		{
			this->Terminate32BitProcesses();
		}

		this->Restart32BitProcesses(bpTimed);
	}
}

void CUfisWatchDogMainWnd::CheckApplications()
{
	CString	olMsg;
	CString olProcess;
	HMODULE	hModules[2048];
	HANDLE  hProcess;
	char    szFileName[MAX_PATH];
	char	szModuleName[MAX_PATH];
	DWORD	dwSize2;
	EnumInfoStruct sInfo;
	DWORD	lParam = 0;

	DWORD	idProcesses[2048 * sizeof(DWORD)];
	DWORD	size;



	for (POSITION pos = this->om16BitProcesses.GetStartPosition(); pos != NULL;)
	{
		CString olName;
		InfoStruct16Bit *polInfo = NULL;
		this->om16BitProcesses.GetNextAssoc(pos,olName,(void *&)polInfo);
		delete polInfo;
	}

	this->om16BitProcesses.RemoveAll();
	this->om32BitProcesses.RemoveAll();

	this->bmTerminate32BitProcesses = true;

	if (!EnumProcesses(idProcesses,2048 * sizeof(DWORD),&size))
	{
		this->LogMessage(" Enum Processes failed..");
		return;
	}

	size /= sizeof(DWORD);

	for (int dwIndex = 0; dwIndex < size; dwIndex++)
	{
		szFileName[0] = '\0';

		// Open the process (if we can... security does not
		// permit every process in the system to be opened).
        hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ,FALSE, idProcesses[dwIndex]);
        if (hProcess != NULL) 
		{
			// Here we call EnumProcessModules to get only the
            // first module in the process. This will be the 
            // EXE module for which we will retrieve the name.
			if (EnumProcessModules(hProcess, hModules,sizeof(hModules), &dwSize2)) 
			{
				dwSize2 /= sizeof(HMODULE);				
				for (int i = 0; i < dwSize2; i++)
				{
					// Get the module names
					if (!GetModuleBaseName(hProcess, hModules[i],szModuleName, sizeof(szModuleName)))
						 szModuleName[0] = 0;
					if (i == 0)
					{
						strcpy(szFileName,szModuleName);
						olProcess = szFileName;
						olProcess.MakeUpper();
					}

					// C++ applications containg Ufis32.dll
					if (_stricmp(szModuleName,"UFIS32.DLL") == 0)
					{
						this->om32BitProcesses.SetAt(olProcess,(void *)idProcesses[dwIndex]);
						if (this->FindName(this->om32BitApplications,olProcess) == -1)
						{
							this->bmTerminate32BitProcesses = false;
						}
					}
				}
			}

			// check on VB applications or non Ufis32.dll containg processes
			if (this->FindName(this->omVBApplications,olProcess) >= 0)
			{
				this->om32BitProcesses.SetAt(olProcess,(void *)idProcesses[dwIndex]);
				this->bmTerminate32BitProcesses = false;
			}
			else if (this->FindName(this->om32BitApplications,olProcess) >= 0)
			{
				this->om32BitProcesses.SetAt(olProcess,(void *)idProcesses[dwIndex]);
			}

            CloseHandle(hProcess);
		}

        // Did we just bump into an NTVDM?
        if (_stricmp(szFileName, "NTVDM.EXE") == 0) 
		{
			InfoStruct16Bit *polInfo = new InfoStruct16Bit;
			polInfo->idProcess = idProcesses[dwIndex];
			polInfo->hTask = 0;
			this->om16BitProcesses.SetAt(olProcess,polInfo);
						
			// Fill in some info for the 16-bit enum proc.
            sInfo.dwPID = idProcesses[dwIndex];
            sInfo.lpProc = &MyProcessEnumerator;
            sInfo.lParam = (DWORD) lParam;
            sInfo.bEnd = FALSE;

            // Enum the 16-bit stuff.
            VDMEnumTaskWOWEx(idProcesses[dwIndex],(TASKENUMPROCEX) Enum16, (LPARAM) &sInfo);

            // Did our main enum func say quit?
            if (sInfo.bEnd)
				break;
		}
			
	}

}


void CUfisWatchDogMainWnd::Terminate16BitProcesses()
{
	// terminate 16 bit tasks at first
	int	  ilNTVDMProcess = -1;	
	DWORD dwNTVDMProcessId = -1; 
	for (int i = 0; i < this->om16BitApplications.GetSize(); i++)
	{
		InfoStruct16Bit *polInfo = NULL;
		if (this->om16BitProcesses.Lookup(this->om16BitApplications[i],(void *&)polInfo) && polInfo != NULL)
		{
			if (polInfo->hTask == 0)
			{
				ilNTVDMProcess	 = i;
				dwNTVDMProcessId = polInfo->idProcess;
			}
			else
			{
				if (VDMTerminateTaskWOW(polInfo->idProcess,polInfo->hTask))
				{
					this->om16BitProcesses.RemoveKey(this->om16BitApplications[i]);
					delete polInfo;
					this->LogMessage(" Process[%s] with id=%u and task=%d terminated",this->om16BitApplications[i],polInfo->idProcess,polInfo->hTask);
				}
				else
				{
					this->LogMessage(" Can\'t terminate process[%s] with id=%u and task=%d",this->om16BitApplications[i],polInfo->idProcess,polInfo->hTask);
				}
			}
		}
	}

	// terminate NTVDM process at least
	// Open the process (if we can... security does not
	// permit every process in the system to be opened).
	HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ | PROCESS_TERMINATE,FALSE, dwNTVDMProcessId);
	if (hProcess != NULL) 
	{
		if (TerminateProcess(hProcess,4711))
		{
			this->om16BitProcesses.RemoveKey(this->om16BitApplications[ilNTVDMProcess]);
			this->LogMessage(" Process[%s] with id=%u terminated",this->om16BitApplications[ilNTVDMProcess],dwNTVDMProcessId);
		}
		else
		{
			this->LogMessage(" Can\'t terminate process[%s] with id=%u",this->om16BitApplications[ilNTVDMProcess],dwNTVDMProcessId);
		}

		CloseHandle(hProcess);
	}
}

void CUfisWatchDogMainWnd::Terminate32BitProcesses()
{
	for (int i = 0; i < this->om32BitApplications.GetSize(); i++)
	{
		DWORD idProcess;
		if (this->om32BitProcesses.Lookup(this->om32BitApplications[i],(void *&)idProcess))
		{
			// Open the process (if we can... security does not
			// permit every process in the system to be opened).
			HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ | PROCESS_TERMINATE,FALSE, idProcess);
			if (hProcess != NULL) 
			{
				if (TerminateProcess(hProcess,4711))
				{
					this->om32BitProcesses.RemoveKey(this->om32BitApplications[i]);
					this->LogMessage(" Process[%s] with id=%u terminated",this->om32BitApplications[i],idProcess);
				}
				else
				{
					this->LogMessage(" Can\'t terminate process[%s] with id=%u, errno = %u",this->om32BitApplications[i],idProcess,GetLastError());
				}

				CloseHandle(hProcess);
			}
			else
			{
				this->LogMessage(" Can\'t open process[%s] with id=%u, errno = %u",this->om32BitApplications[i],idProcess,GetLastError());
			}
		}
	}
}

void CUfisWatchDogMainWnd::Restart32BitProcesses(bool bpTimed)
{
	if (bpTimed && this->om32BitTimeCheck.GetSize() == 0)
		return;

	ASSERT (!bpTimed || this->om32BitTimeCheck.GetSize() == this->om32BitRestartAppl.GetSize());

	CTime olCurrentTime = CTime::GetCurrentTime();
	for (int i = 0; i < this->om32BitRestartAppl.GetSize(); i++)
	{
		if (bpTimed)
		{
			if (this->om32BitTimeCheck[i] == -1)	// don't start !
				continue;

			// check the time frame (1 minute should be enough)
			CTime olTime((time_t)this->om32BitTimeCheck[i]);
			if (olTime.GetHour() != olCurrentTime.GetHour() || olTime.GetMinute() != olCurrentTime.GetMinute())
				continue;
		}

		DWORD idProcess;
		if (!this->om32BitProcesses.Lookup(this->om32BitRestartAppl[i],(void *&)idProcess))
		{
			STARTUPINFO olStartupInfo;
			olStartupInfo.cb = sizeof(STARTUPINFO);
			olStartupInfo.cbReserved2 = '\0';
			olStartupInfo.lpDesktop   = NULL;
			olStartupInfo.lpTitle     = NULL;
			olStartupInfo.dwX		  = 100;
			olStartupInfo.dwY		  = 100;
			olStartupInfo.dwXSize	  = 500;
			olStartupInfo.dwYSize	  = 400;
			olStartupInfo.dwXCountChars =  250;
			olStartupInfo.dwYCountChars	= 2500;
			olStartupInfo.dwFillAttribute = FOREGROUND_RED | BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE;
			olStartupInfo.dwFlags     = 0;
			olStartupInfo.wShowWindow = SW_SHOWNORMAL;
			olStartupInfo.hStdInput   = NULL;
			olStartupInfo.hStdOutput  = NULL;
			olStartupInfo.hStdError   = NULL;
			PROCESS_INFORMATION olProcessInfo;

			if (!CreateProcess(NULL,this->om32BitStartCmdLine[i].GetBuffer(0),NULL,NULL,FALSE,NORMAL_PRIORITY_CLASS,NULL,NULL,&olStartupInfo,&olProcessInfo))
			{
				this->LogMessage(" Can\'t restart process[%s] error code=%u",this->om32BitApplications[i],::GetLastError());
			}
			else
			{
				this->om32BitProcesses.SetAt(this->om32BitRestartAppl[i],(void *)olProcessInfo.dwProcessId);
				this->LogMessage(" Process[%s] restarted with process id=%u",this->om32BitApplications[i],olProcessInfo.dwProcessId);
				CloseHandle(olProcessInfo.hProcess);
			}
		}
	}
}

void CUfisWatchDogMainWnd::Restart16BitProcesses(bool bpTimed)
{
	if (bpTimed && this->om16BitTimeCheck.GetSize() == 0)
		return;

	ASSERT (!bpTimed || this->om16BitTimeCheck.GetSize() == this->om16BitRestartAppl.GetSize());

	CTime olCurrentTime = CTime::GetCurrentTime();

	for (int i = 0; i < this->om16BitRestartAppl.GetSize(); i++)
	{
		if (bpTimed)
		{
			if (this->om16BitTimeCheck[i] == -1)	// don't start !
				continue;

			// check the time frame (1 minute should be enough)
			CTime olTime((time_t)this->om16BitTimeCheck[i]);
			if (olTime.GetHour() != olCurrentTime.GetHour() || olTime.GetMinute() != olCurrentTime.GetMinute())
				continue;
		}

		InfoStruct16Bit* polInfo = NULL;
		if (!this->om16BitProcesses.Lookup(this->om16BitRestartAppl[i],(void *&)polInfo))
		{
			STARTUPINFO olStartupInfo;
			olStartupInfo.cb = sizeof(STARTUPINFO);
			olStartupInfo.cbReserved2 = '\0';
			olStartupInfo.lpDesktop   = NULL;
			olStartupInfo.lpTitle     = NULL;
			olStartupInfo.dwX		  = 100;
			olStartupInfo.dwY		  = 100;
			olStartupInfo.dwXSize	  = 500;
			olStartupInfo.dwYSize	  = 400;
			olStartupInfo.dwXCountChars =  250;
			olStartupInfo.dwYCountChars	= 2500;
			olStartupInfo.dwFillAttribute = FOREGROUND_RED | BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE;
			olStartupInfo.dwFlags     = 0;
			olStartupInfo.wShowWindow = SW_SHOWNORMAL;
			olStartupInfo.hStdInput   = NULL;
			olStartupInfo.hStdOutput  = NULL;
			olStartupInfo.hStdError   = NULL;

			PROCESS_INFORMATION olProcessInfo;
			if (!CreateProcess(NULL,this->om16BitStartCmdLine[i].GetBuffer(0),NULL,NULL,FALSE,NORMAL_PRIORITY_CLASS,NULL,NULL,&olStartupInfo,&olProcessInfo))
			{
				this->LogMessage(" Can\'t restart process[%s] error code=%u",this->om16BitRestartAppl[i],::GetLastError());
			}
			else
			{
				polInfo = new InfoStruct16Bit;
				polInfo->idProcess = olProcessInfo.dwProcessId;
				polInfo->hTask     = -1;
				this->om16BitProcesses.SetAt(this->om16BitRestartAppl[i],(void *)polInfo);
				this->LogMessage(" Process[%s] started with process id=%u",this->om16BitRestartAppl[i],olProcessInfo.dwProcessId);
			}
		}

	}
}

int CUfisWatchDogMainWnd::FindName(const CStringArray& ropNames,const CString& ropName)
{
	for (int i = 0;i < ropNames.GetSize(); i++)
	{
		if (ropName.CompareNoCase(ropNames[i]) == 0)
			return i;
	}

	return -1;
}

int CUfisWatchDogMainWnd::ExtractItemList(const CString opSubString, CStringArray &ropStrArray, char cpTrenner)
{
	int ilSepaLen = 1;//
	int ilCount = 0;
	char *currPtr;
	char *ptrOrig;
	char *pclTextLine;
	char pclSepa[100]="";
	sprintf(pclSepa, "%c", cpTrenner);
	pclTextLine = (char*)malloc((size_t)opSubString.GetLength()+1);
	memset((void*)pclTextLine, 0x00, opSubString.GetLength()+1);
	strcpy(pclTextLine, opSubString);
	ptrOrig = pclTextLine;
	currPtr = strstr(pclTextLine , pclSepa);
	while (currPtr != NULL)
	{
		*currPtr = '\0';
		ropStrArray.Add(pclTextLine );
		ilCount++;
		pclTextLine  = currPtr + ilSepaLen;
		currPtr		= strstr(pclTextLine , pclSepa);
	}
	if(strcmp(pclTextLine , "") != 0)
	{
		ropStrArray.Add(pclTextLine );
		ilCount++;
	}
	free(ptrOrig);
	return ilCount;
}

void CUfisWatchDogMainWnd::LogMessage(const char *prpFormat,...)
{
	va_list prpArg;
	va_start (prpArg,prpFormat);

	char pclBuffer[2048];
	vsprintf(pclBuffer,prpFormat,prpArg);

	CString olMsg;
	olMsg.Format("%s : %s",CTime::GetCurrentTime().Format("%Y/%m/%d %H:%M:%S"),pclBuffer);

	if (::IsWindow(this->m_LoggingList.m_hWnd))
	{
		if (this->m_LoggingList.GetCount() > 2048)
			this->m_LoggingList.ResetContent();

		this->m_LoggingList.AddString(olMsg);
	}
}

void CUfisWatchDogMainWnd::EvaluateConfiguration()
{
	CString	olMsg;

	char pclConfigPath[512];
	char pclTimerCheck[512];

    // retrieve the configuration path from OS environment
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(omApplName, "Check_Frequency", "0",pclTimerCheck, sizeof pclTimerCheck, pclConfigPath);
	int ilTimerCheck;
	if (sscanf(pclTimerCheck,"%d",&ilTimerCheck) == 1)
	{
		if (ilTimerCheck > 0)
		{
			SetTimer(imTimer, (UINT) ilTimerCheck * 1000, NULL);
			this->LogMessage(" Checking Timer every %d seconds",ilTimerCheck);
		}
	}
	else
	{
		this->LogMessage(" Invalid format for variable 'Check_Frequency'");
	}

	GetPrivateProfileString(omApplName, "Check_Delay", "0",pclTimerCheck, sizeof pclTimerCheck, pclConfigPath);
	if (sscanf(pclTimerCheck,"%d",&ilTimerCheck) == 1)
	{
		if (ilTimerCheck > 0)
		{
			this->imCheckDelay = ilTimerCheck * 1000;
			this->LogMessage(" Timer delay is set to %d seconds",ilTimerCheck);
		}
	}
	else
	{
		this->LogMessage(" Invalid format for variable 'Check_Delay'");
	}

	GetPrivateProfileString(omApplName, "Check_Active", "YES",pclTimerCheck, sizeof pclTimerCheck, pclConfigPath);
	if (stricmp(pclTimerCheck,"YES") == 0)
	{
		this->bmCheckActive = true;
		this->LogMessage(" Timer check is active");
	}
	else if (stricmp(pclTimerCheck,"NO") == 0)
	{
		this->bmCheckActive = false;
		this->LogMessage(" Timer check is not active");
	}
	else
	{
		this->LogMessage(" Invalid format for variable 'Check_Active'");
	}

	GetPrivateProfileString(omApplName, "16BitStartCmdLine", "",pclTimerCheck, sizeof pclTimerCheck, pclConfigPath);
	CString olTimerCheck(pclTimerCheck);
	olTimerCheck.MakeUpper();
	ExtractItemList(olTimerCheck,this->om16BitStartCmdLine);			
	
	GetPrivateProfileString(omApplName, "32BitStartCmdLine", "",pclTimerCheck, sizeof pclTimerCheck, pclConfigPath);
	olTimerCheck = pclTimerCheck;
	olTimerCheck.MakeUpper();
	ExtractItemList(olTimerCheck,this->om32BitStartCmdLine);			
	
	GetPrivateProfileString(omApplName, "16BitApplications", "",pclTimerCheck, sizeof pclTimerCheck, pclConfigPath);
	olTimerCheck = pclTimerCheck;
	olTimerCheck.MakeUpper();
	ExtractItemList(olTimerCheck,this->om16BitApplications);			
	
	GetPrivateProfileString(omApplName, "32BitApplications", "",pclTimerCheck, sizeof pclTimerCheck, pclConfigPath);
	olTimerCheck = pclTimerCheck;
	olTimerCheck.MakeUpper();
	ExtractItemList(olTimerCheck,this->om32BitApplications);			
	
	GetPrivateProfileString(omApplName, "16BitRestartAppl", "",pclTimerCheck, sizeof pclTimerCheck, pclConfigPath);
	olTimerCheck = pclTimerCheck;
	olTimerCheck.MakeUpper();
	ExtractItemList(olTimerCheck,this->om16BitRestartAppl);			
	
	GetPrivateProfileString(omApplName, "32BitRestartAppl", "",pclTimerCheck, sizeof pclTimerCheck, pclConfigPath);
	olTimerCheck = pclTimerCheck;
	olTimerCheck.MakeUpper();
	ExtractItemList(olTimerCheck,this->om32BitRestartAppl);			
	
	GetPrivateProfileString(omApplName, "16BitTimeCheck", "",pclTimerCheck, sizeof pclTimerCheck, pclConfigPath);
	CStringArray ol16BitTimeCheck;
	olTimerCheck = pclTimerCheck;
	olTimerCheck.MakeUpper();
	ExtractItemList(olTimerCheck,ol16BitTimeCheck);
	
	CTime olCurrentTime = CTime::GetCurrentTime();
	for (int i = 0; i < ol16BitTimeCheck.GetSize(); i++)
	{
		int ilHour,ilMinute;
		if (sscanf(ol16BitTimeCheck[i],"%d:%d",&ilHour,&ilMinute) == 2)
		{
			CTime olTime(olCurrentTime.GetYear(),olCurrentTime.GetMonth(),olCurrentTime.GetDay(),ilHour,ilMinute,0);
			this->om16BitTimeCheck.Add(olTime.GetTime());
			
		}
		else
		{
			this->om16BitApplications.Add(-1);
		}
	}
	
	GetPrivateProfileString(omApplName, "32BitTimeCheck", "",pclTimerCheck, sizeof pclTimerCheck, pclConfigPath);
	CStringArray ol32BitTimeCheck;
	ExtractItemList(pclTimerCheck,ol32BitTimeCheck);			
	
	for (i = 0; i < ol32BitTimeCheck.GetSize(); i++)
	{
		int ilHour,ilMinute;
		if (sscanf(ol32BitTimeCheck[i],"%d:%d",&ilHour,&ilMinute) == 2)
		{
			CTime olTime(olCurrentTime.GetYear(),olCurrentTime.GetMonth(),olCurrentTime.GetDay(),ilHour,ilMinute,0);
			this->om32BitTimeCheck.Add(olTime.GetTime());
			
		}
		else
		{
			this->om32BitApplications.Add(-1);
		}
	}

	GetPrivateProfileString(omApplName, "VBApplications", "",pclTimerCheck, sizeof pclTimerCheck, pclConfigPath);
	CString olVBApplications = pclTimerCheck;
	olVBApplications.MakeUpper();
	ExtractItemList(olVBApplications,this->omVBApplications);			


}

void CUfisWatchDogMainWnd::OnNotifyIcon(WPARAM wParam,LPARAM lParam)
{
    CPoint point;
    ::GetCursorPos(&point);

	switch(lParam)
	{
	case WM_LBUTTONDOWN:
		{
			CMenu olMenu;
			olMenu.CreatePopupMenu();

			if (this->IsIconic() || this->IsZoomed())
			{
				olMenu.AppendMenu(MF_STRING,ID_RESTORE, "Restore");//	
			}
			else
			{
				olMenu.AppendMenu(MF_STRING|MF_GRAYED,ID_RESTORE, "Restore");//	
			}
			if (this->IsIconic())
			{
				olMenu.AppendMenu(MF_STRING|MF_CHECKED|MF_GRAYED,ID_MINIMIZE,"Minimize");//	
			}
			else
			{
				olMenu.AppendMenu(MF_STRING,ID_MINIMIZE,"Minimize");//	
			}

			if (this->IsZoomed())
			{
				olMenu.AppendMenu(MF_STRING|MF_CHECKED|MF_GRAYED,ID_MAXIMIZE,"Maximize");//	
			}
			else
			{
				olMenu.AppendMenu(MF_STRING,ID_MAXIMIZE,"Maximize");//	
			}

			olMenu.AppendMenu(MF_SEPARATOR,0,"");//	
			olMenu.AppendMenu(MF_STRING,ID_CLOSE,"Close");//	
			olMenu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON,point.x, point.y, this, NULL);
			
		}
	break;
	case WM_LBUTTONDBLCLK:
		this->ShowWindow(SW_SHOWNORMAL);
	break;
	case WM_RBUTTONDOWN:
	break;
	}

}

int CUfisWatchDogMainWnd::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	DWORD dwStyle = WS_CHILD|WS_VISIBLE|WS_BORDER|WS_VSCROLL|LBS_NOINTEGRALHEIGHT;
	CRect olRect(10,10,700,400);
	this->m_LoggingList.Create(dwStyle,olRect,this,IDC_LOGGING_LIST);	


	return 0;
}

void CUfisWatchDogMainWnd::OnMenuRestore()
{
	this->ShowWindow(SW_RESTORE);
}

void CUfisWatchDogMainWnd::OnMenuMinimize()
{
	this->ShowWindow(SW_SHOWMINIMIZED);
}

void CUfisWatchDogMainWnd::OnMenuMaximize()
{
	this->ShowWindow(SW_SHOWMAXIMIZED);
}

void CUfisWatchDogMainWnd::OnMenuClose()
{
	this->PostMessage(WM_QUIT,0,0l);
}
