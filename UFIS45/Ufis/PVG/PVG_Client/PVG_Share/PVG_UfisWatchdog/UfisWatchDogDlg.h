// UfisWatchDogDlg.h : header file
//

#if !defined(AFX_UFISWATCHDOGDLG_H__D0F3C3C7_F895_11D6_BFEB_00010215BFDE__INCLUDED_)
#define AFX_UFISWATCHDOGDLG_H__D0F3C3C7_F895_11D6_BFEB_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CUfisWatchDogDlg dialog

class CUfisWatchDogDlg : public CDialog
{
// Construction
public:
	CUfisWatchDogDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CUfisWatchDogDlg)
	enum { IDD = IDD_UFISWATCHDOG_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUfisWatchDogDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CUfisWatchDogDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UFISWATCHDOGDLG_H__D0F3C3C7_F895_11D6_BFEB_00010215BFDE__INCLUDED_)
