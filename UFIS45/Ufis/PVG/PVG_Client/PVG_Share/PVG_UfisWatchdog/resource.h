//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by UfisWatchDog.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define ID_SHELL_NOTIFY                 101
#define IDD_UFISWATCHDOG_DIALOG         102
#define ID_NOTIFY_ICON                  102
#define IDC_LOGGING_LIST                103
#define ID_MINIMIZE                     104
#define ID_MAXIMIZE                     105
#define ID_CLOSE                        106
#define ID_RESTORE                      107
#define IDR_MAINFRAME                   128

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           108
#endif
#endif
