// UfisWatchDog.h : main header file for the UFISWATCHDOG application
//

#if !defined(AFX_UFISWATCHDOG_H__D0F3C3C5_F895_11D6_BFEB_00010215BFDE__INCLUDED_)
#define AFX_UFISWATCHDOG_H__D0F3C3C5_F895_11D6_BFEB_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CUfisWatchDogApp:
// See UfisWatchDog.cpp for the implementation of this class
//

class CUfisWatchDogApp : public CWinApp
{
public:
	CUfisWatchDogApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUfisWatchDogApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CUfisWatchDogApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UFISWATCHDOG_H__D0F3C3C5_F895_11D6_BFEB_00010215BFDE__INCLUDED_)
