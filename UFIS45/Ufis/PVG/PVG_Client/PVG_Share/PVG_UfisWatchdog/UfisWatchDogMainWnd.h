#if !defined(AFX_UFISWATCHDOGMAINWND_H__D0F3C3D3_F895_11D6_BFEB_00010215BFDE__INCLUDED_)
#define AFX_UFISWATCHDOGMAINWND_H__D0F3C3D3_F895_11D6_BFEB_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UfisWatchDogMainWnd.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CUfisWatchDogMainWnd frame

class CUfisWatchDogMainWnd : public CFrameWnd
{
	DECLARE_DYNCREATE(CUfisWatchDogMainWnd)
public:
	CUfisWatchDogMainWnd();           // protected constructor used by dynamic creation
	static CUfisWatchDogMainWnd *somThis;
// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUfisWatchDogMainWnd)
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CUfisWatchDogMainWnd();

	// Generated message map functions
	//{{AFX_MSG(CUfisWatchDogMainWnd)
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	afx_msg void OnNotifyIcon(WPARAM wParam,LPARAM lParam); 
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnMenuRestore();
	afx_msg void OnMenuMinimize();
	afx_msg void OnMenuMaximize();
	afx_msg void OnMenuClose();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	int		ExtractItemList(CString opSubString, CStringArray &ropStrArray, char cpTrenner = ',');
	void	EvaluateConfiguration();
	void	Check16BitApplications(bool bpTimed);
	void	Check32BitApplications(bool bpTimed);	
	void	Terminate16BitProcesses();
	void	Terminate32BitProcesses();
	void	Restart16BitProcesses(bool bpTimed);
	void	Restart32BitProcesses(bool bpTimed);
	void	CheckApplications();
	void	LogMessage(const char *prpFormat,...);
	int		FindName(const CStringArray& ropNames,const CString& ropName);
	friend BOOL CALLBACK MyProcessEnumerator(DWORD dwPID, WORD wTask,char * szProcess, LPARAM lParam); 
private:
	const int		imTimer;
	const char*		omApplName;

	HICON			m_hIcon;
	CStringArray	om16BitStartCmdLine;
	CStringArray	om32BitStartCmdLine;

	CStringArray	om16BitApplications;
	CStringArray	om32BitApplications;

	CStringArray	om16BitRestartAppl;
	CStringArray	om32BitRestartAppl;

	CDWordArray		om16BitTimeCheck;
	CDWordArray		om32BitTimeCheck;

	CMapStringToPtr	om16BitProcesses;
	CMapStringToPtr	om32BitProcesses;

	bool			bmTerminate16BitProcesses;
	bool			bmTerminate32BitProcesses;

	bool			bmCheckActive;

	DWORD			imCheckDelay;
	CListBox		m_LoggingList;

	CStringArray	omVBApplications;

};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UFISWATCHDOGMAINWND_H__D0F3C3D3_F895_11D6_BFEB_00010215BFDE__INCLUDED_)
