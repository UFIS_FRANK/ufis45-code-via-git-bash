# Microsoft Developer Studio Project File - Name="CCSClass" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=CCSClass - Win32 Release
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "CCSClass.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "CCSClass.mak" CFG="CCSClass - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "CCSClass - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "CCSClass - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE "CCSClass - Win32 Dll Release" (based on "Win32 (x86) Static Library")
!MESSAGE "CCSClass - Win32 Dll Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "CCSClass - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 1
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "C:\Ufis_Bin\ClassLib\Release"
# PROP Intermediate_Dir "C:\Ufis_Intermediate\ClassLib\Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I ".\\" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "PVG" /FR /YX /FD /c
# ADD BASE RSC /l 0x407
# ADD RSC /l 0x407
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "CCSClass - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 1
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "C:\Ufis_Bin\ClassLib\Debug"
# PROP Intermediate_Dir "C:\Ufis_Intermediate\ClassLib\Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /Z7 /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /MTd /W3 /GX /ZI /Od /I ".\\" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "PVG" /FR /YX /FD /c
# ADD BASE RSC /l 0x407
# ADD RSC /l 0x407
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo /o"Debug/CCSClass.bsc"
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "CCSClass - Win32 Dll Release"

# PROP BASE Use_MFC 1
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "CCSClass___Win32_Dll_Release"
# PROP BASE Intermediate_Dir "CCSClass___Win32_Dll_Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "C:\Ufis_Bin\ClassLib\SharedDll\Release"
# PROP Intermediate_Dir "C:\Ufis_Intermediate\ClassLib\SharedDll\Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /FR /YX /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /I ".\\" /D "NDEBUG" /D "_AFXDLL" /D "WIN32" /D "_WINDOWS" /D "PVG" /FR /YX /FD /c
# ADD BASE RSC /l 0x407
# ADD RSC /l 0x407 /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "CCSClass - Win32 Dll Debug"

# PROP BASE Use_MFC 1
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "CCSClass___Win32_Dll_Debug"
# PROP BASE Intermediate_Dir "CCSClass___Win32_Dll_Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "C:\Ufis_Bin\ClassLib\SharedDll\Debug"
# PROP Intermediate_Dir "C:\Ufis_Intermediate\ClassLib\SharedDll\Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /FR /YX /FD /c
# ADD CPP /nologo /MDd /W3 /GX /ZI /Od /I ".\\" /D "_DEBUG" /D "_AFXDLL" /D "WIN32" /D "_WINDOWS" /D "PVG" /FR /YX /FD /c
# ADD BASE RSC /l 0x407
# ADD RSC /l 0x407 /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ENDIF 

# Begin Target

# Name "CCSClass - Win32 Release"
# Name "CCSClass - Win32 Debug"
# Name "CCSClass - Win32 Dll Release"
# Name "CCSClass - Win32 Dll Debug"
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\AatBitmapComboBox.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\AatBitmapComboBox.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\AatColorCheckBox.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\AatColorCheckBox.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\AatHelp.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\AatHelp.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\BcCCmdTarget.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\BcCCmdTarget.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Wrapper\bccomclient.cpp
# End Source File
# Begin Source File

SOURCE=..\_Standard_Wrapper\bccomclient.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Wrapper\bccomserver.cpp
# End Source File
# Begin Source File

SOURCE=..\_Standard_Wrapper\bccomserver.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Wrapper\bcproxy.cpp
# End Source File
# Begin Source File

SOURCE=..\_Standard_Wrapper\bcproxy.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\blockingsocket.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\blockingsocket.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCS3DStatic.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCS3DStatic.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSBar.Cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSBar.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSBasic.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSBasic.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSBasicFunc.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSBasicFunc.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSBchandle.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSBchandle.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSButtonctrl.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSButtonctrl.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSCedacom.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSCedacom.h
# End Source File
# Begin Source File

SOURCE=.\CCSCedadata.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSCedadata.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSCell.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSCell.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSClientWnd.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSClientWnd.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSColorButton.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSColorButton.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSDDX.CPP
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSDDX.H
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSDefines.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSDefines.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSDragDropCtrl.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSDragDropCtrl.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSEdit.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSEdit.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSLog.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSLog.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSMessageBox.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSMessageBox.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSPrint.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSPtrArray.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSTable.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSTable.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSTime.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSTime.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSTimeScale.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSTimeScale.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSTree.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CCSTree.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CedaAPTData.Cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CedaAPTData.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CedaAptLocalUtc.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CedaAptLocalUtc.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CedaBasicData.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CedaBasicData.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CedaObject.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CedaObject.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CedaSEAData.Cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CedaSEAData.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CedaSysTabData.Cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\CedaSysTabData.H
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\ClassLib.rc
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\excel9.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\excel9.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\FastCedaConnection.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\FastCedaConnection.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\FieldSet.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\FieldSet.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\FlightData.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\FlightData.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\LibGlobl.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\ModulData.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\ModulData.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\NameConfigurationDlg.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\NameConfigurationDlg.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\RecordSet.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\RecordSet.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\resource.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\StdAfx.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\StdAfx.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\TimePacket.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\UFIS.H
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\UfisOdbc.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\UfisOdbc.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\VersionInfo.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Classlib\VersionInfo.h
# End Source File
# End Target
# End Project
