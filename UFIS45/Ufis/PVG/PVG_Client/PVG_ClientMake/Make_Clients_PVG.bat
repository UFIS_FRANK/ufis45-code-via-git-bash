Rem BEGIN
date /T
time /T 


Rem ---Check for folder c:\ufis\system---
IF EXIST c:\ufis\system\nul GOTO cont1
Echo Create folder C:\ufis\system > con
md c:\ufis\system
:cont1

Rem ---Check for folder c:\ufis_Bin\Runtimedlls---
IF EXIST C:\Ufis_Bin\Runtimedlls\nul GOTO cont2
Echo Create folder C:\ufis_Bin\Runtimedlls > con
md C:\Ufis_Bin\Runtimedlls
:cont2

Rem ---Check for folder c:\tmp---
IF EXIST C:\tmp\nul goto cont4
Echo Create folder C:\tmp > con
md C:\tmp
:cont4


ECHO Project Shanghai Upgrade 4.5 > C:\Ufis_Bin\PVG.txt

mkdir c:\Ufis_Bin\ClassLib
mkdir c:\Ufis_Bin\ClassLib\Include
mkdir c:\Ufis_Bin\ClassLib\Include\jm
mkdir C:\Ufis_Bin\ForaignLibs
mkdir C:\Ufis_Bin\Help

copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Regularexpressionlib\lib\*.lib C:\Ufis_Bin\ForaignLibs
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Regularexpressionlib\lib\*.pdb C:\Ufis_Bin\ForaignLibs
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Regularexpressionlib\lib\*.dll C:\Ufis_Bin\Runtimedlls
copy ..\..\..\_Standard\_Standard_client\_Standard_configuration\Clientruntime\*.* C:\Ufis_Bin\Runtimedlls
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Regularexpressionlib\include\*.h c:\Ufis_Bin\ClassLib\Include
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Regularexpressionlib\include\jm\*.h c:\Ufis_Bin\ClassLib\Include\jm
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_ClassLib\*.h c:\Ufis_Bin\ClassLib\Include
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Wrapper\*.h c:\Ufis_Bin\ClassLib\Include

copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Classlib\*.h c:\Ufis_Bin\ClassLib\Include
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Classlib\Classlib.rc c:\Ufis_Bin\ClassLib\Include

copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_HtmlHelp\lib\*.lib C:\Ufis_Bin\ForaignLibs
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_HtmlHelp\include\*.h c:\Ufis_Bin\ClassLib\Include
copy ..\..\..\_Standard\_Standard_client\_Standard_help\*.chm c:\Ufis_Bin\help


copy ..\PVG_configuration\ceda.ini c:\Ufis_Bin
copy ..\PVG_configuration\Telexpool.ini c:\Ufis_Bin
copy ..\PVG_configuration\bdps_sec.cfg c:\Ufis_Bin
copy ..\PVG_configuration\notvalid*.bmp c:\Ufis_Bin
copy ..\PVG_configuration\loatab*.* c:\Ufis_Bin
copy ..\PVG_configuration\airportlogo.bmp c:\Ufis_Bin
copy ..\PVG_configuration\DATACHANGE.ini c:\Ufis_Bin
copy copy_for_install.bat c:\Ufis_Bin



REM ------------------Cients build from _Standard-Project-----------------------------------

REM ------------------------Share-----------------------------------------------------------

cd ..\..\..\_Standard\_Standard_client

ECHO Build Ufis32.dll... > con
cd _Standard_share\_Standard_Ufis32dll
msdev Ufis32.dsp /MAKE "Ufis32 - Debug" /REBUILD
msdev Ufis32.dsp /MAKE "Ufis32 - Release" /REBUILD
If exist c:\Ufis_Bin\Debug\Ufis32.ilk del c:\Ufis_Bin\Debug\Ufis32.ilk
If exist c:\Ufis_Bin\Debug\Ufis32.pdb del c:\Ufis_Bin\Debug\Ufis32.pdb
If exist c:\Ufis_Bin\Debug\Ufis32.pdb del c:\Ufis_Bin\Debug\Ufis32.exp
If exist c:\Ufis_Bin\Release\Ufis32.ilk del c:\Ufis_Bin\Release\Ufis32.ilk
If exist c:\Ufis_Bin\Release\Ufis32.pdb del c:\Ufis_Bin\Release\Ufis32.pdb
If exist c:\Ufis_Bin\Release\Ufis32.pdb del c:\Ufis_Bin\Release\Ufis32.exp
copy c:\ufis_bin\release\Ufis32.dll c:\ufis_bin\debug\*.*
copy c:\ufis_bin\release\Ufis32.lib c:\ufis_bin\debug\*.*
cd ..\..

cd ..\..\PVG\PVG_Client

ECHO Build Classlib... > con
cd PVG_share\PVG_ClassLib
msdev CCSClass.dsp /MAKE "CCSClass - Win32 Debug" /REBUILD
msdev CCSClass.dsp /MAKE "CCSClass - Win32 Release" /REBUILD
msdev CCSClass.dsp /MAKE "CCSClass - Win32 Dll Debug" /REBUILD
msdev CCSClass.dsp /MAKE "CCSClass - Win32 Dll Release" /REBUILD
cd ..\..

cd ..\..\_Standard\_Standard_Client

ECHO Build BcServ... > con
cd _Standard_share\_Standard_BcServ
msdev BcServ32.dsp /MAKE "BcServ - Release" /REBUILD
If exist c:\Ufis_Bin\Release\BcServ32.ilk del c:\Ufis_Bin\Release\BcServ32.ilk
If exist c:\Ufis_Bin\Release\BcServ32.pdb del c:\Ufis_Bin\Release\BcServ32.pdb
cd ..\..

ECHO Build BcProxy... > con
cd _Standard_share\_Standard_BcProxy
Rem ---remove read-only state---
attrib *.* -r
msdev BcProxy.dsp /MAKE "BcProxy - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Release\BcProxy.ilk del c:\Ufis_Bin\Release\BcProxy.ilk
If exist c:\Ufis_Bin\Release\BcProxy.pdb del c:\Ufis_Bin\Release\BcProxy.pdb
cd ..\..

ECHO Build Tabocx... > con
cd _Standard_share\_Standard_TABocx
msdev TAB.dsp /MAKE "TAB - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Release\TAB.ilk del c:\Ufis_Bin\Release\TAB.ilk
If exist c:\Ufis_Bin\Release\TAB.pdb del c:\Ufis_Bin\Release\TAB.pdb
cd ..\..

ECHO Build UfisCedaocx... > con
cd _Standard_share\_Standard_Ufiscedaocx
msdev UfisCom.dsp /MAKE "UfisCom - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Release\UfisCom.ilk del c:\Ufis_Bin\Release\UfisCom.ilk
If exist c:\Ufis_Bin\Release\UfisCom.pdb del c:\Ufis_Bin\Release\UfisCom.pdb
cd ..\..

ECHO Build UComocx... > con
cd _Standard_Share\_Standard_UCom
msdev UCom.dsp /MAKE "UCom - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Release\UCom.ilk del c:\Ufis_Bin\Release\UCom.ilk
If exist c:\Ufis_Bin\Release\UCom.pdb del c:\Ufis_Bin\Release\UCom.pdb
cd ..\..

ECHO Build Ganttocx... > con
cd _Standard_share\_Standard_Ganttocx
msdev UGantt.dsp /MAKE "UGantt - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Release\UGantt.ilk del c:\Ufis_Bin\Release\UGantt.ilk
If exist c:\Ufis_Bin\Release\UGantt.pdb del c:\Ufis_Bin\Release\UGantt.pdb
cd ..\..

ECHO Build AatLoginocx... > con
cd _Standard_Share\_Standard_Login
rem msdev AatLogin.dsp /MAKE "AatLogin - Win32 Debug" /REBUILD
msdev AatLogin.dsp /MAKE "AatLogin - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Release\AatLogin.ilk del c:\Ufis_Bin\Release\AatLogin.ilk
If exist c:\Ufis_Bin\Release\AatLogin.pdb del c:\Ufis_Bin\Release\AatLogin.pdb
cd ..\..

ECHO Build UfisAppMng... > con
cd _Standard_share\_Standard_UfisApManager
Rem ---remove read-only state---
attrib *.* -r
msdev UfisAppMng.dsp /MAKE "UfisAppMng - Win32 Release" /REBUILD
copy C:\Ufis_Bin\Release\UfisAppMng.tlb c:\ufis\system
If exist c:\Ufis_Bin\Release\UfisAppMng.ilk del c:\Ufis_Bin\Release\UfisAppMng.ilk
If exist c:\Ufis_Bin\Release\UfisAppMng.pdb del c:\Ufis_Bin\Release\UfisAppMng.pdb
cd ..\..

ECHO Build UfisApplMgr... > con
cd _Standard_share\_Standard_UfisApplMgr
Rem ---remove read-only state---
attrib *.* -r
msdev UfisApplMgr.dsp /MAKE "UfisApplMgr - Win32 Release" /REBUILD
copy C:\Ufis_Bin\Release\UFISApplMgr.tlb c:\ufis\system
If exist c:\Ufis_Bin\Release\UfisApplMgr.ilk del c:\Ufis_Bin\Release\UfisApplMgr.ilk
If exist c:\Ufis_Bin\Release\UfisApplMgr.pdb del c:\Ufis_Bin\Release\UfisApplMgr.pdb
cd ..\..

ECHO Build BCComServer... > con
cd _Standard_share\_Standard_BCComServer
Rem ---remove read-only state---
attrib *.* -r
msdev BCComServer.dsp /MAKE "BCComServer - Win32 Release" /REBUILD
copy BCComServer.tlb c:\Ufis_Bin\Release
If exist c:\Ufis_Bin\Release\BCComServer.ilk del c:\Ufis_Bin\Release\BCComServer.ilk
If exist c:\Ufis_Bin\Release\BCComServer.pdb del c:\Ufis_Bin\Release\BCComServer.pdb
cd ..\..

ECHO Build BdpsPass... > con
cd _Standard_share\_Standard_Bdpspass
msdev Bdpspass.dsp /MAKE "Bdpspass - Win32 Debug" /REBUILD
If exist c:\Ufis_Bin\Debug\Bdpspass.ilk del c:\Ufis_Bin\Debug\Bdpspass.ilk
If exist c:\Ufis_Bin\Debug\Bdpspass.pdb del c:\Ufis_Bin\Debug\Bdpspass.pdb
cd ..\..

ECHO Build BdpsSec... > con
cd _Standard_share\_Standard_Bdpssec
msdev Bdps_sec.dsp /MAKE "Bdps_sec - Win32 Debug" /REBUILD
If exist c:\Ufis_Bin\Debug\Bdps_sec.ilk del c:\Ufis_Bin\Debug\Bdps_sec.ilk
If exist c:\Ufis_Bin\Debug\Bdps_sec.pdb del c:\Ufis_Bin\Debug\Bdps_sec.pdb
cd ..\..

ECHO Build BdpsUif... > con
cd _Standard_share\_Standard_Bdpsuif
msdev Bdpsuif.dsp /MAKE "Bdpsuif - Win32 Debug" /REBUILD
rem msdev Bdpsuif.dsp /MAKE "Bdpsuif - Win32 WatsonDebug" /REBUILD
If exist c:\Ufis_Bin\Debug\Bdpsuif.ilk del c:\Ufis_Bin\Debug\Bdpsuif.ilk
If exist c:\Ufis_Bin\Debug\Bdpsuif.pdb del c:\Ufis_Bin\Debug\Bdpsuif.pdb
cd ..\..

REM ---Flight

ECHO Build Fips... > con
cd _Standard_Flight\_Standard_Fips
msdev FPMS.dsp /MAKE "FPMS - Win32 Debug" /REBUILD
If exist c:\Ufis_Bin\Debug\FIPS.ilk del c:\Ufis_Bin\Debug\FIPS.ilk
If exist c:\Ufis_Bin\Debug\FIPS.pdb del c:\Ufis_Bin\Debug\FIPS.pdb
cd ..\..

ECHO Build RulesFips... > con
cd _Standard_flight\_Standard_Ruleseditorflight
msdev Rules.dsp /MAKE "Rules - Win32 Debug" /REBUILD
rem msdev Rules.dsp /MAKE "Rules - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\Rules.ilk del c:\Ufis_Bin\Debug\Rules.ilk
If exist c:\Ufis_Bin\Debug\Rules.pdb del c:\Ufis_Bin\Debug\Rules.pdb
cd ..\..


REM Visal Bacic Clients have to be build
REM ------------------------------------

ECHO Build Telexpool... > con
cd _Standard_Flight\_Standard_Telexpool
vb6 /make Telexpool.vbp /out ..\..\..\vbappl.log
cd ..\..

ECHO Build LoadTabViewer... > con
cd _Standard_Flight\_Standard_LoaTabViewer
vb6 /make LoadTabViewer.vbp /out ..\..\..\vbappl.log
cd ..\..

ECHO Build LoggingTableViewer... > con
cd _Standard_Share\_Standard_Loggingtableviewer
vb6 /make LogTab_Viewer.vbp /out ..\..\vbappl.log
cd ..\..


REM C# Clients have to be build
REM ---------------------------

ECHO Build Ufis.Utils.dll... > con
cd _Standard_UfisAddOn\_Standard_Utilities
devenv Utilities.sln /Rebuild Release /project ufis.utils
cd ..\..

ECHO Build ZedGraph... > con
cd _Standard_UfisAddOn\_Standard_ZedGraph
devenv ZedGraph.sln /Rebuild Release /project ZedGraph
cd ..\..

ECHO Build ZedGraph... > con
cd _Standard_UfisAddOn\_Standard_ZedGraph
devenv ZedGraph.sln /Rebuild Release /project ZedGraph
cd ..\..

ECHO Build LocationChanges... > con
cd _Standard_UfisAddOn\_Standard_DataChanges\Data_Changes
devenv Data_Changes.sln /Rebuild Release /project Data_Changes
cd ..\..\..


REM ------------------Cients build from Customer-Project------------------------------------

cd ..\..\PVG\PVG_Client

ECHO Build UfisWatchdog... > con
cd PVG_share\PVG_UfisWatchdog
msdev UfisWatchdog.dsp /MAKE "UfisWatchdog - Win32 Debug" /REBUILD
If exist c:\Ufis_Bin\Debug\UfisWatchdog.ilk del c:\Ufis_Bin\Debug\UfisWatchdog.ilk
If exist c:\Ufis_Bin\Debug\UfisWatchdog.pdb del c:\Ufis_Bin\Debug\UfisWatchdog.pdb
cd ..\..

ECHO Build StatusManager... > con
cd PVG_UfisAddOn\PVG_StatusManager
devenv Status_Manager.sln /Rebuild Release /project Status_Manager
cd ..\..

ECHO Build FIPS Reports... > con
cd PVG_Flight\PVG_FIPSReport
devenv FIPS_Reports.sln /Rebuild Release /project FIPS_Reports
cd ..\..


cd PVG_ClientMake


:end

ECHO ...Done > con
Rem END
time/T
