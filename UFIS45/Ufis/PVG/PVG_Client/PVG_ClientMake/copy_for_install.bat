rem Copies all generated files from the SCM-Structue into the Install-Shield directory.
rem 

rem set drive to J: (IS 6.2) or K: (IS 8.0) 
set drive=k:

rem Copy ini- and config-files
copy c:\ufis_bin\ceda.ini %drive%\PVG_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\telexpool.ini %drive%\PVG_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\bdps_sec.cfg %drive%\PVG_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\*.bmp %drive%\PVG_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\loatab*.* %drive%\PVG_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\DATACHANGE.ini %drive%\PVG_build\ufis_client_appl\system\*.*


rem copy applications
copy c:\ufis_bin\debug\bdps_sec.exe %drive%\PVG_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpspass.exe %drive%\PVG_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpsuif.exe %drive%\PVG_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\fips.exe %drive%\PVG_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\rules.exe %drive%\PVG_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\ufiswatchdog.exe %drive%\PVG_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\telexpool.exe %drive%\PVG_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\loadtabviewer.exe %drive%\PVG_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\logtab_viewer.exe %drive%\PVG_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\data_changes.exe %drive%\PVG_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\status_manager.exe %drive%\PVG_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\fips_reports.exe %drive%\PVG_build\ufis_client_appl\applications\*.*


REM Copy system-files
copy c:\ufis_bin\release\bcproxy.exe %drive%\PVG_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcproxy.tlb %drive%\PVG_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcserv32.exe %drive%\PVG_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufis32.dll %drive%\PVG_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.exe %drive%\PVG_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.tlb %drive%\PVG_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\BCComServer.exe %drive%\PVG_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\BCComServer.tlb %drive%\PVG_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.ocx %drive%\PVG_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.tlb %drive%\PVG_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.ocx %drive%\PVG_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.tlb %drive%\PVG_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucom.ocx %drive%\PVG_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucom.tlb %drive%\PVG_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.ocx %drive%\PVG_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.tlb %drive%\PVG_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\aatlogin.ocx %drive%\PVG_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\aatlogin.tlb %drive%\PVG_build\ufis_client_appl\system\*.*

rem copy interop dll's
copy c:\ufis_bin\release\*lib.dll %drive%\PVG_build\ufis_client_appl\applications\interopdll\*.*
copy c:\ufis_bin\release\ufis.utils.dll %drive%\PVG_build\ufis_client_appl\applications\interopdll\*.*
copy c:\ufis_bin\release\zedgraph.dll %drive%\PVG_build\ufis_client_appl\applications\interopdll\*.*
copy c:\ufis_bin\release\usercontrols.dll %drive%\PVG_build\ufis_client_appl\applications\interopdll\*.*
copy c:\ufis_bin\release\zH-CHS\*.dll %drive%\PVG_build\ufis_client_appl\applications\zH-CHS\*.*
copy c:\ufis_bin\release\zH-CN\*.dll %drive%\PVG_build\ufis_client_appl\applications\zH-CN\*.*


rem Copy Help-Files
rem copy c:\ufis_bin\Help\*.chm %drive%\PVG_build\ufis_client_appl\help\*.*
