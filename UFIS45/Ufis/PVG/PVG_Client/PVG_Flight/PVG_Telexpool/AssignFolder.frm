VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form AssignFolder 
   Caption         =   "Assign Telex to Folder and Date"
   ClientHeight    =   6600
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11220
   Icon            =   "AssignFolder.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6600
   ScaleWidth      =   11220
   ShowInTaskbar   =   0   'False
   Begin VB.CheckBox chkWork 
      Caption         =   "Flights"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   7
      Left            =   5370
      Style           =   1  'Graphical
      TabIndex        =   28
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Navigate"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   11
      Left            =   2640
      Style           =   1  'Graphical
      TabIndex        =   26
      Top             =   30
      Width           =   855
   End
   Begin VB.Frame fraAssPanel 
      Caption         =   "Assign Telex to Flight"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2415
      Index           =   3
      Left            =   8040
      TabIndex        =   10
      Top             =   720
      Width           =   7005
      Begin VB.CheckBox chkWork 
         Caption         =   "Refresh"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   8
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   23
         Top             =   240
         Width           =   855
      End
      Begin VB.TextBox txtFltDate 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   1860
         TabIndex        =   22
         Text            =   "11.12.2003"
         Top             =   240
         Width           =   1095
      End
      Begin VB.TextBox txtFltDate 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   2970
         TabIndex        =   21
         Text            =   "12:00"
         Top             =   240
         Width           =   615
      End
      Begin VB.TextBox txtFltDate 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   3600
         TabIndex        =   20
         Text            =   "-120"
         Top             =   240
         Width           =   615
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Arr"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   9
         Left            =   960
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   240
         Width           =   435
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Dep"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   10
         Left            =   1410
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   240
         Width           =   435
      End
      Begin VB.TextBox txtFltDate 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   4230
         TabIndex        =   17
         Text            =   "+45"
         Top             =   240
         Width           =   615
      End
      Begin TABLib.TAB Flights 
         Height          =   825
         Index           =   0
         Left            =   90
         TabIndex        =   24
         Top             =   570
         Width           =   1965
         _Version        =   65536
         _ExtentX        =   3466
         _ExtentY        =   1455
         _StockProps     =   64
      End
      Begin TABLib.TAB Flights 
         Height          =   855
         Index           =   1
         Left            =   390
         TabIndex        =   25
         Top             =   1440
         Visible         =   0   'False
         Width           =   1875
         _Version        =   65536
         _ExtentX        =   3307
         _ExtentY        =   1508
         _StockProps     =   64
      End
   End
   Begin VB.Frame fraAssPanel 
      Caption         =   "Assign Telex to Folder"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3045
      Index           =   2
      Left            =   2310
      TabIndex        =   9
      Top             =   3210
      Width           =   5655
      Begin VB.CheckBox chkWork 
         Caption         =   "Create"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   4
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   240
         Width           =   855
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Update"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   5
         Left            =   960
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   240
         Width           =   855
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Delete"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   6
         Left            =   1830
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   240
         Width           =   855
      End
      Begin TABLib.TAB ValidFolders 
         Height          =   2385
         Left            =   90
         TabIndex        =   16
         Top             =   570
         Width           =   5475
         _Version        =   65536
         _ExtentX        =   9657
         _ExtentY        =   4207
         _StockProps     =   64
      End
   End
   Begin VB.Frame fraAssPanel 
      Caption         =   "Assign Telex to Date"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3045
      Index           =   1
      Left            =   60
      TabIndex        =   8
      Top             =   3210
      Width           =   2175
      Begin VB.TextBox txtAssDate 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   990
         TabIndex        =   12
         Text            =   "11.12.2003"
         Top             =   720
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.TextBox txtAssDate 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   1470
         TabIndex        =   11
         Text            =   "12:00"
         Top             =   240
         Width           =   615
      End
      Begin MSComCtl2.DTPicker MonthView 
         Height          =   315
         Left            =   90
         TabIndex        =   27
         Top             =   240
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   55050241
         CurrentDate     =   37967
      End
   End
   Begin VB.Frame fraAssPanel 
      Caption         =   "List of Assigned Folders"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2385
      Index           =   0
      Left            =   60
      TabIndex        =   6
      Top             =   720
      Width           =   7905
      Begin TABLib.TAB AssignTlx 
         Height          =   2055
         Left            =   90
         TabIndex        =   7
         Top             =   240
         Width           =   7725
         _Version        =   65536
         _ExtentX        =   13626
         _ExtentY        =   3625
         _StockProps     =   64
      End
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Remove"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   3
      Left            =   1770
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Change"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   900
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Save"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   30
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "&Close"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   3510
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   30
      Width           =   855
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   6315
      Width           =   11220
      _ExtentX        =   19791
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   19262
         EndProperty
      EndProperty
   End
   Begin VB.Label lblTlxText 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "This will publish the Telex Extract Text (Subject)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   60
      TabIndex        =   2
      Top             =   360
      Width           =   6765
   End
End
Attribute VB_Name = "AssignFolder"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub chkWork_Click(Index As Integer)
    If chkWork(Index).Value = 1 Then
        chkWork(Index).BackColor = LightGreen
        Select Case Index
            Case 0  'Close
                'Me.Hide
                'TelexPoolHead.chkAssFile.Value = 0
                chkWork(Index).Value = 0
            Case 7  'Flights
                If Me.WindowState <> vbMaximized Then Me.Width = 11180
                chkWork(10).Value = 1
            Case 9  'Arr
                chkWork(10).Value = 0
                Flights(0).Visible = True
                Flights(1).Visible = False
            Case 10 'Dep
                chkWork(9).Value = 0
                Flights(1).Visible = True
                Flights(0).Visible = False
            Case Else
                chkWork(Index).Value = 0
        End Select
    Else
        chkWork(Index).BackColor = vbButtonFace
        Select Case Index
            Case 7  'Flights
                If Me.WindowState <> vbMaximized Then Me.Width = 8130
            Case 9  'Arr
                chkWork(10).Value = 1
            Case 10 'Dep
                chkWork(9).Value = 1
            Case Else
        End Select
    End If
End Sub

Private Sub Form_Load()
    InitMyForm
End Sub
Private Sub InitMyForm()
    Dim i As Integer
    Me.Width = 8130
    Me.Left = TelexPoolHead.Left + ((TelexPoolHead.Width - Me.Width) / 2)
    'Me.Height = 7080
    Me.Top = TelexPoolHead.Top + ((TelexPoolHead.Height - Me.Height) / 2)
    MonthView.Value = Now
    AssignTlx.ResetContent
    AssignTlx.FontName = "Courier New"
    AssignTlx.SetTabFontBold True
    AssignTlx.HeaderFontSize = 17
    AssignTlx.FontSize = 17
    AssignTlx.LineHeight = 17
    AssignTlx.HeaderString = "Flight,Date,Time,TAB,Full Name,Remark"
    AssignTlx.HeaderLengthString = "10,10,10,10,10,10"
    AssignTlx.SetMainHeaderValues "6", "Assigned Folders", ""
    AssignTlx.SetMainHeaderFont 17, False, False, True, 0, "Courier New"
    AssignTlx.ShowHorzScroller True
    AssignTlx.ShowVertScroller True
    AssignTlx.LifeStyle = True
    AssignTlx.CursorLifeStyle = True
    AssignTlx.MainHeader = True
    AssignTlx.AutoSizeByHeader = True
    AssignTlx.InsertTextLine "SQ 365,10.12.03,12:00,MVT,12345678901234567890123456789012,123456789012", False
    AssignTlx.InsertTextLine "SQ 365,11.12.03,12:00,MVT,12345678901234567890123456789012,123456789012", False
    AssignTlx.InsertTextLine "SQ 365,12.12.03,12:00,MVT,12345678901234567890123456789012,123456789012", False
    AssignTlx.InsertTextLine "SQ 365,13.12.03,12:00,MVT,12345678901234567890123456789012,123456789012", False
    AssignTlx.AutoSizeColumns
    
    ValidFolders.ResetContent
    ValidFolders.FontName = "Courier New"
    ValidFolders.SetTabFontBold True
    ValidFolders.HeaderFontSize = 17
    ValidFolders.FontSize = 17
    ValidFolders.LineHeight = 17
    ValidFolders.HeaderString = "TAB,Full Name,Remark"
    ValidFolders.HeaderLengthString = "10,10,10"
    ValidFolders.SetMainHeaderValues "3", "Valid Folders", ""
    ValidFolders.SetMainHeaderFont 17, False, False, True, 0, "Courier New"
    ValidFolders.ShowHorzScroller True
    ValidFolders.ShowVertScroller True
    ValidFolders.LifeStyle = True
    ValidFolders.CursorLifeStyle = True
    ValidFolders.MainHeader = True
    ValidFolders.AutoSizeByHeader = True
    ValidFolders.InsertTextLine "ABC,12345678901234567890123456789012,123456789012", False
    ValidFolders.InsertTextLine "MVT,12345678901234567890123456789012,123456789012", False
    ValidFolders.InsertTextLine "PFA,12345678901234567890123456789012,123456789012", False
    ValidFolders.InsertTextLine "PFB,12345678901234567890123456789012,123456789012", False
    ValidFolders.InsertTextLine "PAX,12345678901234567890123456789012,123456789012", False
    ValidFolders.InsertTextLine "PTM,12345678901234567890123456789012,123456789012", False
    ValidFolders.InsertTextLine "TRM,12345678901234567890123456789012,123456789012", False
    ValidFolders.InsertTextLine "OTH,12345678901234567890123456789012,123456789012", False
    ValidFolders.Sort "0", True, True
    ValidFolders.AutoSizeColumns
    
    For i = 0 To 1
        Flights(i).ResetContent
        Flights(i).FontName = "Courier New"
        Flights(i).SetTabFontBold True
        Flights(i).HeaderFontSize = 17
        Flights(i).FontSize = 17
        Flights(i).LineHeight = 17
        Flights(i).ShowHorzScroller True
        Flights(i).ShowVertScroller True
        Flights(i).LifeStyle = True
        Flights(i).CursorLifeStyle = True
        Flights(i).AutoSizeByHeader = True
    Next
    Flights(0).HeaderString = "Flight,Date,Time,Org,Via,A/C,Remark"
    Flights(0).HeaderLengthString = "10,10,10,10,10,10,10"
    Flights(0).HeaderAlignmentString = "L,C,C,L,L,C,L"
    Flights(0).SetMainHeaderValues "7", "Arrival Flights", ""
    Flights(0).SetMainHeaderFont 17, False, False, True, 0, "Courier New"
    Flights(0).MainHeader = True
    
    Flights(1).HeaderString = "Flight,Date,Time,Des,Via,A/C,Remark"
    Flights(1).HeaderLengthString = "10,10,10,10,10,10,10"
    Flights(1).HeaderAlignmentString = "L,C,C,L,L,C,L"
    Flights(1).SetMainHeaderValues "7", "Departure Flights", ""
    Flights(1).SetMainHeaderFont 17, False, False, True, 0, "Courier New"
    Flights(1).MainHeader = True
    
    Flights(0).InsertTextLine "SQ 1234 F,12.12.2003,12:00,FRA,BKK,B774,", False
    Flights(1).InsertTextLine "SQ 1235 F,12.12.2003,12:30,FRA,BKK,B774,", False
    Flights(0).AutoSizeColumns
    Flights(1).AutoSizeColumns
    
    Flights(1).Top = Flights(0).Top
    Flights(1).Left = Flights(0).Left
    
    StatusBar1.ZOrder
    
End Sub

Private Sub Form_Resize()
    Dim NewSize As Long
    NewSize = Me.ScaleWidth - (lblTlxText.Left * 2)
    If NewSize > 600 Then
        lblTlxText.Width = NewSize - 30
        NewSize = NewSize - 180
    End If
    NewSize = Me.ScaleWidth - fraAssPanel(3).Left - lblTlxText.Left
    If NewSize > 600 Then
        fraAssPanel(3).Width = NewSize
        NewSize = NewSize - 180
        Flights(0).Width = NewSize
        Flights(1).Width = NewSize
    End If
    NewSize = Me.ScaleHeight - StatusBar1.Height - fraAssPanel(1).Top - 60
    If NewSize > 600 Then
        fraAssPanel(1).Height = NewSize
        fraAssPanel(2).Height = NewSize
    End If
    NewSize = NewSize - ValidFolders.Top - 90
    If NewSize > 600 Then
        ValidFolders.Height = NewSize
    End If
    NewSize = Me.ScaleHeight - StatusBar1.Height - fraAssPanel(3).Top - 60
    If NewSize > 600 Then
        fraAssPanel(3).Height = NewSize
    End If
    NewSize = NewSize - Flights(0).Top - 90
    If NewSize > 600 Then
        Flights(0).Height = NewSize
        Flights(1).Height = NewSize
    End If
End Sub

Private Sub MonthView_DateDblClick(ByVal DateDblClicked As Date)
    'MsgBox DateDblClicked
End Sub
