Attribute VB_Name = "StartModule"
Option Explicit
Public ReadFolderCount As Boolean
Public StopOnline As Boolean
Public LockBroadcasts As Boolean
Public MainIsOnline As Boolean
Public MainIsDeploy As Boolean
Public MainIsFltDeploy As Boolean
Public StartUpMain As Boolean
Public SaveLocalTestData As Boolean
Public BrowserColumns As String
Public AllTypesCol As Long
Public OtherTypesCol As Long
Public LastFolderIndex As Long
Public FirstAssignFolder As Long
Public FolderSize As Long
Public AllTelexTypes As String
Public FolderCaptions As String
Public FolderTypeList As String
Public TypeActionList As String
Public InfoTypeList As String
Public RecvNoShow As String
Public MainNoShow As String
Public AddrNoCheck As String
Public IgnoreAllTlxText As String
Public TextFilterList As String
Public IgnoreEmptyText As Boolean
Public MyFontSize As Integer
Public MyFontBold As Boolean
Public MySitaOutType As String
Public MySitaDefPrio As String
Public MySitaOutOrig As Boolean
Public MySitaOutMsgId As Boolean
Public MySitaOutSmi As Boolean
Public MySitaOutDblSign As Boolean
Public TelexTemplates As Boolean
Public TestCaseMode As Boolean
Public OnlineWindows As Integer
Public LoadedWindows As Integer
Public MaxOnlineCount As Long
Public MaxFolderRows As Integer
Public MyLifeStyleValue As Integer
Public RecvTypeAction As String
Public SendTypeAction As String
Public ImportIsConnected As Boolean
Public ImportIsPending As Boolean
Public PathToImportTool As String

Public Function GetModuleVersion()
    GetModuleVersion = ""
End Function

Sub Main()
    Dim LoginIsOk As Boolean
    Dim CmdLine As String
    Dim tmpData As String
    Dim tmpMainIsDeploy As Boolean
    MyFontSize = 14
    MyFontBold = False
    Screen.MousePointer = 11
    ApplicationIsStarted = False
    AutoArrange = False
    StartUpMain = True
    ShutDownRequested = False
    OnlineWindows = 0
    If App.PrevInstance = True Then
        MyMsgBox.InfoApi 1, "", ""
        With Screen
            If MyMsgBox.AskUser(0, .Width / 2, .Height / 2, App.EXEName & ": Application Control", "This module is already open!", "stop", "", UserAnswer) >= 0 Then ShutDownRequested = True
        End With
        End
    End If
    Screen.MousePointer = 11
    Load UfisServer
    LoginIsOk = DetermineIniFile
    GetFileAndPath DEFAULT_CEDA_INI, myIniFile, myIniPath
    tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "TEST_MODE_LOCAL", "NO")
    If tmpData = "YES" Then
        UfisServer.HostName.Text = "LOCAL"
        TelexPoolHead.CedaStatus(3).Text = "LOCAL"
        UfisServer.aCeda.CleanupCom
        CedaIsConnected = False
    End If
    
    CmdLine = Command
    LoginIsOk = True
    If (InStr(CmdLine, "CONNECTED") = 0) And (InStr(CmdLine, "NOLOG") = 0) Then
        If UfisServer.HostName <> "LOCAL" Then
            Screen.MousePointer = 0
            LoginIsOk = LoginProcedure
            Screen.MousePointer = 11
        End If
    End If
    If Not LoginIsOk Then
        UfisServer.aCeda.CleanupCom
        End
    End If
    
    Load PoolConfig
    If (InStr(CmdLine, "NOMAIN") > 0) Or (PoolConfig.OnLineCfg(0).Value = 0) Then StartUpMain = False
    HiddenFocus.Show
    HiddenFocus.Refresh
    Screen.MousePointer = 11
    If GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SHOW_INTRO", "YES") = "YES" Then
        HiddenMain.Show
        HiddenMain.Refresh
    End If
    HiddenMain.CedaStatus(1).Text = "CONNECTING TO '" & UfisServer.HostName & "'"
    Screen.MousePointer = 11
    MyFontSize = Val(GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "FONT_SIZE", "14"))
    If GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "FONT_BOLD", "NO") = "YES" Then MyFontBold = True Else MyFontBold = False
    Load TelexPoolHead
    tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "MAIN_ONLINE", "NO")
    StopOnline = False
    LockBroadcasts = False
    ReadFolderCount = True
    If tmpData = "YES" Then
        MainIsOnline = True
        StartUpMain = True
    Else
        MainIsOnline = False
    End If
    tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "MAIN_FOL_DEPLOY", "NO")
    If tmpData = "YES" Then
        tmpMainIsDeploy = True
    Else
        tmpMainIsDeploy = False
    End If
    tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "MAIN_FLT_DEPLOY", "NO")
    If tmpData = "YES" Then
        MainIsFltDeploy = True
    Else
        MainIsFltDeploy = False
    End If
    
    Screen.MousePointer = 11
    If StartUpMain = True Then
        TelexPoolHead.Show
        TelexPoolHead.Refresh
    End If
    TelexPoolHead.InitMyForm
    
    Screen.MousePointer = 11
    If (PoolConfig.OnLineCfg(1).Value = 1) Or (MainIsOnline = True) Then
        Load RcvTelex
        OnlineWindows = OnlineWindows + 1
    End If
    If PoolConfig.OnLineCfg(2).Value = 1 Then
        Load SndTelex
        OnlineWindows = OnlineWindows + 1
    End If
    If (PoolConfig.OnLineCfg(3).Value = 1) Or (MainIsOnline = True) Then
        Load OutTelex
        OnlineWindows = OnlineWindows + 1
    End If
    If PoolConfig.OnLineCfg(4).Value = 1 Then
        Load RcvInfo
        OnlineWindows = OnlineWindows + 1
    End If
    ApplicationIsStarted = True
    TelexPoolHead.InitMemButtons
    If GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "START_ON_TOP", "YES") = "YES" Then TelexPoolHead.OnTop.Value = 1
    If OnlineWindows > 0 Then
        TelexPoolHead.OnLine.Value = 1
        TelexPoolHead.FixWin.Value = 1
        TelexPoolHead.FixWin.Value = 0
        HiddenFocus.SetFocus
    Else
        TelexPoolHead.WindowState = vbNormal
    End If
    With HiddenMain
        .Move .Left, -.Height - 1000
    End With
    Screen.MousePointer = 11
    TelexPoolHead.MyStatusBar.Panels(1).Text = "Starting Broadcast Handler (BcProxy) ..."
    If CedaIsConnected Then UfisServer.ConnectToBcProxy
    'UfisServer.aCeda.StayConnected = True
    TelexPoolHead.MyStatusBar.Panels(1).Text = ""
    If Not MainIsOnline Then
        TelexPoolHead.RefreshOnlineWindows "RCV"
        TelexPoolHead.RefreshOnlineWindows "OUT"
        TelexPoolHead.RefreshOnlineWindows "SND"
        TelexPoolHead.RefreshOnlineWindows "INF"
    End If
    SetAllFormsOnTop True
    'Due to problems in bcserv32 (ignoring first broadcasts)
    '-------------------------------------------------------
    If UfisServer.HostName <> "LOCAL" Then
        If (Not MainIsOnline) And (CedaIsConnected) Then
            Screen.MousePointer = 11
            UfisServer.CallCeda UserAnswer, "SBC", "TLXTAB", "LOGIN", "TELEXPOOL", "1/6", "", 0, True, False
            UfisServer.CallCeda UserAnswer, "SBC", "TLXTAB", "LOGIN", "TELEXPOOL", "2/6", "", 0, True, False
            UfisServer.CallCeda UserAnswer, "SBC", "TLXTAB", "LOGIN", "TELEXPOOL", "3/6", "", 0, True, False
            UfisServer.CallCeda UserAnswer, "SBC", "TLXTAB", "LOGIN", "TELEXPOOL", "4/6", "", 0, True, False
            UfisServer.CallCeda UserAnswer, "SBC", "TLXTAB", "LOGIN", "TELEXPOOL", "5/6", "", 0, True, False
            UfisServer.CallCeda UserAnswer, "SBC", "TLXTAB", "LOGIN", "TELEXPOOL", "6/6", "", 0, True, False
        End If
    End If
    '-------------------------------------------------------
    TelexPoolHead.MyStatusBar.Panels(1).Text = "Starting Ufis Application Manager ..."
    UfisServer.ConnectToApplMgr
    TelexPoolHead.MyStatusBar.Panels(1).Text = ""
    UfisServer.aCeda.CleanupCom
    'UfisServer.aCeda.StayConnected = False
    MainIsDeploy = tmpMainIsDeploy
    UfisServer.UrnoPoolInit 100
    UfisServer.UrnoPoolPrepare 100
    Screen.MousePointer = 0
End Sub

Private Function DetermineIniFile() As Boolean
    Dim tmpStr As String
    GetFileAndPath DEFAULT_CEDA_INI, myIniFile, myIniPath
    tmpStr = GetItem(Command, 2, "INI=")
    If tmpStr <> "" Then
        myIniFile = GetItem(tmpStr, 1, " ")
        myIniFullName = myIniPath & "\" & myIniFile
        tmpStr = Dir(myIniFullName)
        If tmpStr = "" Then
            tmpStr = "Sorry, can't find config file:" & vbNewLine
            tmpStr = tmpStr & Chr(34) & myIniFullName & Chr(34) & vbNewLine
            tmpStr = tmpStr & "Using default configuration."
            MsgBox tmpStr
            tmpStr = ""
        End If
    End If
    If UCase(tmpStr) <> UCase(myIniFile) Then
        myIniFile = App.EXEName & UfisServer.HOPO & ".ini"
        myIniFullName = myIniPath & "\" & myIniFile
        tmpStr = Dir(myIniFullName)
    End If
    If UCase(tmpStr) <> UCase(myIniFile) Then
        myIniFile = App.EXEName & ".ini"
        myIniFullName = myIniPath & "\" & myIniFile
        tmpStr = Dir(myIniFullName)
    End If
    If UCase(tmpStr) <> UCase(myIniFile) Then
        GetFileAndPath DEFAULT_CEDA_INI, myIniFile, myIniPath
        myIniFullName = myIniPath & "\" & myIniFile
        tmpStr = Dir(myIniFullName)
    End If
    If UCase(tmpStr) <> UCase(myIniFile) Then
        DetermineIniFile = False
    Else
        DetermineIniFile = True
    End If
End Function

Private Function LoginProcedure() As Boolean
    Dim LoginAnsw As String
    Dim tmpRegStrg As String
    Set UfisServer.LoginCall.UfisComCtrl = UfisServer.aCeda
    UfisServer.LoginCall.ApplicationName = "TelexPool"
    'LoginAnsw = UfisServer.LoginCall.DoLoginSilentMode("UFIS$ADMIN", "Passwort")
    tmpRegStrg = ""
    tmpRegStrg = tmpRegStrg & "TelexPool" & ","
    '                  FKTTAB:  SUBD   ,  FUNC  ,    FUAL              ,TYPE,STAT
    tmpRegStrg = tmpRegStrg & "InitModu,InitModu,Initialisieren (InitModu),B,-"
    'tmpRegStrg = tmpRegStrg & ",Entry,Entry,Entry,F,1"
    'tmpRegStrg = tmpRegStrg & ",Feature,Feature,Feature,F,1"
    UfisServer.LoginCall.InfoAppVersion = UfisServer.GetApplVersion(True)
    UfisServer.LoginCall.VersionString = UfisServer.GetApplVersion(True)
    UfisServer.LoginCall.RegisterApplicationString = tmpRegStrg
    UfisServer.LoginCall.InfoCaption = "Info about Telexpool"
    UfisServer.LoginCall.InfoButtonVisible = True
    UfisServer.LoginCall.InfoUfisVersion = "UFIS Version 4.5"
    UfisServer.LoginCall.InfoAppVersion = CStr("Telexpool " + UfisServer.GetApplVersion(True))
    UfisServer.LoginCall.InfoCopyright = "� 2001-2003 ABB Airport Technologies GmbH"
    UfisServer.LoginCall.InfoAAT = "ABB Airport Technologies GmbH"
    UfisServer.LoginCall.UserNameLCase = True
    
    UfisServer.LoginCall.ShowLoginDialog
    LoginAnsw = UfisServer.LoginCall.GetPrivileges("InitModu")
    If LoginAnsw <> "" Then LoginProcedure = True Else LoginProcedure = False
End Function

Public Sub RefreshMain()
    'Nothing to do
End Sub

Public Function CheckCancelExit(CurForm As Form, MaxForms As Integer) As Boolean
    If CountVisibleForms <= MaxForms Then
        CheckCancelExit = False
        ShutDownApplication
        'we might come back ... (when the user cancels the request)
    Else
        CurForm.Hide
    End If
    CheckCancelExit = True
End Function
Public Sub ShutDownApplication()
Dim i As Integer
Dim cnt As Integer
    On Error GoTo ErrorHandler
    MyMsgBox.InfoApi 0, "The 'On Line' functions will be disconnected from the server" & vbNewLine & "after leaving the Telex Pool.", "Tip"
    'With TelexPoolHead
        If MyMsgBox.CallAskUser(0, 0, 0, "Application Control", "Do you want to exit ?", "ask", "Yes,No;F", UserAnswer) = 1 Then
            ShutDownRequested = True
            CloseHtmlHelp
            Unload UfisServer
            'UfisServer.aCeda.CleanupCom
            'UFISAppMng.DetachApp TlxPool
            'cnt = Forms.count - 1
            'For i = cnt To 0 Step -1
            '    Unload Forms(i)
            'Next
            
            End
        End If
    'End With
ErrorHandler:
    Resume Next
End Sub

Public Sub SetAllFormsOnTop(SetValue As Boolean)
    If FormIsLoaded("RcvInfo") Then CheckFormOnTop RcvTelex, SetValue
    If FormIsLoaded("RcvTelex") Then CheckFormOnTop RcvTelex, SetValue
    If FormIsLoaded("SndTelex") Then CheckFormOnTop SndTelex, SetValue
    If FormIsLoaded("OutTelex") Then CheckFormOnTop OutTelex, SetValue
    If FormIsLoaded("EditTelex") Then CheckFormOnTop EditTelex, SetValue
    CheckFormOnTop TelexPoolHead, SetValue
End Sub

Public Sub HandleBroadCast(ReqId As String, DestName As String, RecvName As String, CedaCmd As String, ObjName As String, Seq As String, Tws As String, Twe As String, CedaSqlKey As String, Fields As String, Data As String, BcNum As String)
    Dim s As String
    Dim clDest As String
    Dim clRecv As String
    Dim clCmd As String
    Dim clTable As String
    Dim clSelKey As String
    Dim clFields As String
    Dim clData As String
    clTable = ObjName
    If InStr("TLXTAB,AFTTAB,TFRTAB,TFNTAB,TLKTAB", clTable) > 0 Then
        clCmd = CedaCmd
        clSelKey = CedaSqlKey
        clFields = Fields
        clData = Data
        TelexPoolHead.EvaluateBc 0, BcNum, DestName, RecvName, clTable, clCmd, clSelKey, clFields, clData
    End If
End Sub

Public Sub MsgFromUFISAppManager(Orig As Long, Data As String)
Dim tmpFldLst As String
Dim tmpDatLst As String
Dim nlPos As Integer
    'TelexPoolHead.Text1.Text = TelexPoolHead.Text1.Text & vbNewLine & "FROM APP MNG" & vbNewLine & Data
    If ApplicationIsStarted And Not ModalMsgIsOpen Then
        TelexPoolHead.Show
        TelexPoolHead.Refresh
        nlPos = InStr(Data, vbLf)
        If nlPos > 1 Then
            tmpFldLst = Left(Data, nlPos - 1)
            tmpDatLst = Mid(Data, nlPos + 1)
            'TelexPoolHead.Text1.Text = TelexPoolHead.Text1.Text & vbNewLine & tmpFldLst & vbNewLine & tmpDatLst & vbNewLine & "------------------"
            If Left(tmpDatLst, 2) <> "-1" Then
                CheckFormOnTop TelexPoolHead, True
                TelexPoolHead.SearchFlightTelexes tmpFldLst, tmpDatLst
            End If
        Else
            'If MyMsgBox.CallAskUser(0, 0, 0, "Communication Control", "Undefined message received from FIPS", "hand", "", UserAnswer) > 0 Then DoNothing
        End If
    End If
End Sub

Public Sub HandleDisplayChanged()
    With TelexPoolHead
        If .Width > UfisTools.SysInfo.WorkAreaWidth Then
            .Left = UfisTools.SysInfo.WorkAreaLeft
            .Width = UfisTools.SysInfo.WorkAreaWidth
        End If
        If .Height > UfisTools.SysInfo.WorkAreaHeight Then
            .Top = UfisTools.SysInfo.WorkAreaTop
            .Height = UfisTools.SysInfo.WorkAreaHeight
        End If
    End With
End Sub

Public Sub HandleSysColorsChanged()
    TelexPoolHead.ArrangeAllWin
End Sub

Public Sub HandleTimeChanged()
    TelexPoolHead.InitDateFields
End Sub

Public Function FilterTelexAddress(UseText As String, AddrList As String, NoCheckTypes As String, CurTtyp As String, HeaderOnly As Boolean) As Boolean
    Dim CurAddress As String
    Dim CurItem As Long
    Dim ShowTelex As Boolean
    Dim CurText As String
    Dim CurOrig As String
    ShowTelex = False
    If (NoCheckTypes <> "") And (CurTtyp <> "") Then
        If InStr(NoCheckTypes, CurTtyp) > 0 Then ShowTelex = True
    End If
    If Not ShowTelex Then
        CurText = UseText
        CurOrig = "xx"
        If HeaderOnly Then
            If InStr(UseText, "=DESTINATION TYPE B") > 0 Then
                GetKeyItem CurText, UseText, "=DESTINATION TYPE B", "="
                CurText = Replace(CurText, ".", " ", 1, -1, vbBinaryCompare)
                GetKeyItem CurOrig, UseText, "=ORIGIN", "="
                CurOrig = "." & Mid(CurOrig, 2)
            Else
                CurText = GetItem(UseText, 1, ".")
                CurOrig = "." & Left(GetItem(UseText, 2, "."), 10)
            End If
        End If
        CurItem = 0
        CurAddress = GetRealItem(AddrList, CurItem, ",")
        While (CurAddress <> "") And (ShowTelex = False)
            If InStr(CurText, CurAddress) > 0 Then ShowTelex = True
            If InStr(CurOrig, CurAddress) > 0 Then ShowTelex = True
            CurItem = CurItem + 1
            CurAddress = GetRealItem(AddrList, CurItem, ",")
        Wend
    End If
    FilterTelexAddress = ShowTelex
End Function

Public Function FilterAddressHeader(UseText As String, AddrList As String, NoCheckTypes As String, CurTtyp As String, RetVal As Boolean) As Boolean
    Dim CurAddress As String
    Dim CurItem As Long
    Dim ShowTelex As Boolean
    Dim CurText As String
    Dim CurOrig As String
    ShowTelex = RetVal
    If ShowTelex Then
        If (AddrList <> "") And (NoCheckTypes <> "") And (CurTtyp <> "") Then
            If InStr(NoCheckTypes, CurTtyp) > 0 Then ShowTelex = False
        End If
        If (ShowTelex) And (AddrList <> "") Then
            If InStr(UseText, "=DESTINATION TYPE B") > 0 Then
                GetKeyItem CurText, UseText, "=DESTINATION TYPE B", "="
                CurText = Replace(CurText, ".", " ", 1, -1, vbBinaryCompare)
                GetKeyItem CurOrig, UseText, "=ORIGIN", "="
                CurOrig = "." & Mid(CurOrig, 2)
            Else
                CurText = GetItem(UseText, 1, ".")
                CurOrig = "." & Left(GetItem(UseText, 2, "."), 10)
            End If
            CurItem = 0
            CurAddress = GetRealItem(AddrList, CurItem, ",")
            While (CurAddress <> "") And (ShowTelex = True)
                If (InStr(CurText, CurAddress) = 0) And (InStr(CurOrig, CurAddress) = 0) Then ShowTelex = False
                CurItem = CurItem + 1
                CurAddress = GetRealItem(AddrList, CurItem, ",")
            Wend
        End If
    End If
    FilterAddressHeader = ShowTelex
End Function

Public Function FilterTelexText(CurText As String, PatternList As String, DefRetVal As Boolean) As Boolean
    Dim CurPattern As String
    Dim CurItem As Long
    Dim ShowTelex As Boolean
    ShowTelex = DefRetVal
    If ShowTelex Then
        CurItem = 0
        CurPattern = GetRealItem(PatternList, CurItem, ",")
        While (CurPattern <> "") And (ShowTelex = True)
            If InStr(CurText, CurPattern) > 0 Then ShowTelex = False
            CurItem = CurItem + 1
            CurPattern = GetRealItem(PatternList, CurItem, ",")
        Wend
    End If
    FilterTelexText = ShowTelex
End Function
Public Function FilterTelexTypeText(CurType As String, CurText As String, FilterList As String, DefRetVal As Boolean) As Boolean
    Dim PatternList As String
    Dim CurCode As String
    Dim CurPattern As String
    Dim CurItem As Long
    Dim ShowTelex As Boolean
    ShowTelex = DefRetVal
    If ShowTelex Then
        If CurType = "" Then
            CurCode = "{=FREE=}"
        Else
            CurCode = "{=" & CurType & "=}"
        End If
        GetKeyItem PatternList, FilterList, CurCode, "{="
        If PatternList <> "" Then
            CurItem = 0
            CurPattern = GetRealItem(PatternList, CurItem, ",")
            While (CurPattern <> "") And (ShowTelex = True)
                If InStr(CurText, CurPattern) > 0 Then ShowTelex = False
                CurItem = CurItem + 1
                CurPattern = GetRealItem(PatternList, CurItem, ",")
            Wend
        End If
    End If
    FilterTelexTypeText = ShowTelex
End Function

Public Function DrawBackGround(MyPanel As PictureBox, MyColor As Integer, DrawHoriz As Boolean, DrawDown As Boolean) As Long
    Const intBLUESTART% = 255
    Const intBLUEEND% = 0
    Const intBANDHEIGHT% = 15
    Const intSHADOWSTART% = 64
    Const intSHADOWCOLOR% = 0
    Const intTEXTSTART% = 0
    Const intTEXTCOLOR% = 15
    Const intRed% = 1
    Const intGreen% = 2
    Const intBlue% = 4
    Const intBackRed% = 8
    Const intBackGreen% = 16
    Const intBackBlue% = 32
    Dim sngBlueCur As Single
    Dim sngBlueStep As Single
    Dim intFormHeight As Single
    Dim intFormWidth As Single
    Dim intX As Single
    Dim intY As Single
    Dim iColor As Integer
    Dim iRed As Single, iBlue As Single, iGreen As Single
    Dim ReturnColor As Long
    ReturnColor = vbWhite
    If MyColor >= 0 Then
        intFormHeight = MyPanel.ScaleHeight
        intFormWidth = MyPanel.ScaleWidth
    
        iColor = MyColor
        sngBlueCur = intBLUESTART
    
        If DrawDown Then
            If DrawHoriz Then
                sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormHeight
                For intY = 0 To intFormHeight Step intBANDHEIGHT
                    If iColor And intBlue Then iBlue = sngBlueCur
                    If iColor And intRed Then iRed = sngBlueCur
                    If iColor And intGreen Then iGreen = sngBlueCur
                    If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                    If iColor And intBackRed Then iRed = 255 - sngBlueCur
                    If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                    MyPanel.Line (-1, intY - 1)-(intFormWidth, intY + intBANDHEIGHT), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep
                    If intY = 0 Then ReturnColor = RGB(iRed, iGreen, iBlue)
                Next intY
            Else
                sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormWidth
                For intX = 0 To intFormWidth Step intBANDHEIGHT
                    If iColor And intBlue Then iBlue = sngBlueCur
                    If iColor And intRed Then iRed = sngBlueCur
                    If iColor And intGreen Then iGreen = sngBlueCur
                    If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                    If iColor And intBackRed Then iRed = 255 - sngBlueCur
                    If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                    MyPanel.Line (intX - 1, -1)-(intX + intBANDHEIGHT, intFormHeight), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep
                Next intX
            End If
        Else
            If DrawHoriz Then
                sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormHeight
                For intY = intFormHeight To 0 Step -intBANDHEIGHT
                    If iColor And intBlue Then iBlue = sngBlueCur
                    If iColor And intRed Then iRed = sngBlueCur
                    If iColor And intGreen Then iGreen = sngBlueCur
                    If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                    If iColor And intBackRed Then iRed = 255 - sngBlueCur
                    If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                    MyPanel.Line (-1, intY - 1)-(intFormWidth, intY + intBANDHEIGHT), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep
                Next intY
            Else
                sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormWidth
                For intX = intFormWidth To 0 Step -intBANDHEIGHT
                    If iColor And intBlue Then iBlue = sngBlueCur
                    If iColor And intRed Then iRed = sngBlueCur
                    If iColor And intGreen Then iGreen = sngBlueCur
                    If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                    If iColor And intBackRed Then iRed = 255 - sngBlueCur
                    If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                    MyPanel.Line (intX - 1, -1)-(intX + intBANDHEIGHT, intFormHeight), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep
                Next intX
            End If
        End If
    
    End If
    DrawBackGround = ReturnColor
End Function

Public Function CleanTrimString(CurText As String) As String
    Dim UseText As String
    Dim tmpChr As String
    UseText = CurText
    UseText = Replace(UseText, Chr(1), "", 1, -1, vbBinaryCompare)
    UseText = Replace(UseText, Chr(3), "", 1, -1, vbBinaryCompare)
    tmpChr = Left(UseText, 1)
    While (tmpChr = vbLf) Or (tmpChr = vbCr)
        UseText = Mid(UseText, 2)
        tmpChr = Left(UseText, 1)
    Wend
    tmpChr = Right(UseText, 1)
    While (tmpChr = vbLf) Or (tmpChr = vbCr)
        UseText = Left(UseText, Len(UseText) - 1)
        tmpChr = Right(UseText, 1)
    Wend
    tmpChr = vbCr & vbCr
    UseText = Replace(UseText, tmpChr, vbCr, 1, -1, vbBinaryCompare)
    CleanTrimString = UseText
End Function

Public Sub ArrangeTabCursor(CurTab As TABLib.Tab)
    Dim VisLines As Long
    Dim SelLine As Long
    Dim TopLine As Long
    Dim BotLine As Long
    SelLine = CurTab.GetCurrentSelected
    If SelLine >= 0 Then
        TopLine = CurTab.GetVScrollPos
        VisLines = ((CurTab.Height / 15) / CurTab.LineHeight) - 1
        VisLines = VisLines - 1
        BotLine = TopLine + VisLines - 1
        If (SelLine < TopLine) Or (SelLine > BotLine) Then
            CurTab.OnVScrollTo SelLine - 1
        End If
    End If
End Sub
