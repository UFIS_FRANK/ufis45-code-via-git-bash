VERSION 5.00
Begin VB.Form ColorPool 
   Caption         =   "Status Colors Configuration"
   ClientHeight    =   705
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   3300
   Icon            =   "ColorPool.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   705
   ScaleWidth      =   3300
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox Text2 
      BackColor       =   &H00C0FFFF&
      ForeColor       =   &H00008000&
      Height          =   285
      Left            =   690
      TabIndex        =   2
      Text            =   "Text2"
      Top             =   240
      Width           =   555
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   210
      TabIndex        =   1
      Text            =   "Text1"
      Top             =   240
      Width           =   465
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   285
      Left            =   1350
      TabIndex        =   0
      Top             =   270
      Width           =   1605
   End
End
Attribute VB_Name = "ColorPool"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Sub GetStatusColor(TextStatus As String, BackStatus As String, TextColor As Long, BackColor As Long)
    Select Case TextStatus
        Case "R"
            TextColor = vbBlue
        Case "S"
            TextColor = vbBlue
        Case "V"
            TextColor = &H8000&         'darkest visible green
        Case "E"
            TextColor = vbRed
        Case "X"
            TextColor = vbMagenta
        Case Else
            TextColor = vbBlack
    End Select
    Select Case BackStatus
        Case "E"
            BackColor = vbRed
            TextColor = vbWhite
        Case "R"
            BackColor = vbRed
            TextColor = vbWhite
        Case "N"
            BackColor = vbRed
            TextColor = vbWhite
        Case "A"
            BackColor = vbYellow
        Case "U"
            BackColor = vbGreen
        Case "I"
            BackColor = vbYellow
        Case "T"
            BackColor = vbWhite
        Case Else
            BackColor = vbWhite
    End Select
End Sub

Private Sub Command1_Click()
  'UfisTools.CommonDialog.CancelError = True
  'On Error GoTo ErrHandler
  UfisTools.CommonDialog.Flags = cdlCCRGBInit Or cdlCCPreventFullOpen
  UfisTools.CommonDialog.ShowColor
  Me.BackColor = UfisTools.CommonDialog.Color
  Exit Sub

ErrHandler:
  ' User pressed the Cancel button

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Cancel = CheckCancelExit(Me, 3)
    End If
End Sub

