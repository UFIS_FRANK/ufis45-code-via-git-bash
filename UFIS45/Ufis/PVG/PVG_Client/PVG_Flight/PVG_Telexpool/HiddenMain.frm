VERSION 5.00
Begin VB.Form HiddenMain 
   Caption         =   "Telex Pool"
   ClientHeight    =   7545
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7170
   Icon            =   "HiddenMain.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7545
   ScaleWidth      =   7170
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      Height          =   315
      Left            =   0
      TabIndex        =   0
      Top             =   7200
      Width           =   7605
      Begin VB.TextBox CedaStatus 
         Alignment       =   2  'Center
         BackColor       =   &H00008000&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   1
         Left            =   0
         Locked          =   -1  'True
         TabIndex        =   1
         Text            =   "CONNECTING"
         Top             =   30
         Width           =   7160
      End
   End
   Begin VB.Image Image3 
      Height          =   855
      Left            =   30
      Picture         =   "HiddenMain.frx":030A
      Stretch         =   -1  'True
      Top             =   720
      Width           =   885
   End
   Begin VB.Image Image2 
      BorderStyle     =   1  'Fixed Single
      Height          =   7155
      Left            =   0
      Picture         =   "HiddenMain.frx":9C7C
      Top             =   30
      Width           =   7155
   End
   Begin VB.Image Image1 
      BorderStyle     =   1  'Fixed Single
      Height          =   960
      Left            =   2295
      Picture         =   "HiddenMain.frx":34709
      Top             =   30
      Visible         =   0   'False
      Width           =   4860
   End
   Begin VB.Image Image4 
      BorderStyle     =   1  'Fixed Single
      Height          =   960
      Left            =   0
      Picture         =   "HiddenMain.frx":36940
      Stretch         =   -1  'True
      Top             =   30
      Visible         =   0   'False
      Width           =   2250
   End
End
Attribute VB_Name = "HiddenMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Sub Form_Activate()
    If Not ModalMsgIsOpen And ApplicationIsStarted And Not ShutDownRequested Then
        HiddenFocus.SetFocus
        If TelexPoolHead.Visible = True Then
            If TelexPoolHead.WindowState = vbMinimized Then TelexPoolHead.WindowState = vbNormal
            TelexPoolHead.SetFocus
        Else
            If StartUpMain Then
                TelexPoolHead.Show
                TelexPoolHead.SetFocus
            End If
        End If
    End If
End Sub

Private Sub Form_Load()
    Top = Screen.Height / 2 - Height / 2
    Left = Top
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Cancel = CheckCancelExit(Me, 3)
    End If
End Sub
