VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form RcvTelex 
   Caption         =   "Received Telexes"
   ClientHeight    =   2370
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4380
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "RcvTelex.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   2370
   ScaleWidth      =   4380
   StartUpPosition =   3  'Windows Default
   Tag             =   "Received Telexes"
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   3570
      Top             =   270
   End
   Begin TABLib.TAB tabTelexList 
      Height          =   1170
      Index           =   1
      Left            =   270
      TabIndex        =   7
      Top             =   1110
      Visible         =   0   'False
      Width           =   3750
      _Version        =   65536
      _ExtentX        =   6615
      _ExtentY        =   2064
      _StockProps     =   64
      Columns         =   9
      Lines           =   10
   End
   Begin VB.CheckBox chkLock 
      Caption         =   "Lock"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1740
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   0
      Width           =   855
   End
   Begin VB.CheckBox OpenPool 
      Caption         =   "Main"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   0
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   0
      Width           =   855
   End
   Begin VB.CheckBox chkLoad 
      Caption         =   "Refresh"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   870
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   0
      Width           =   855
   End
   Begin VB.CheckBox OnTop 
      Caption         =   "On Top"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2610
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   0
      Width           =   855
   End
   Begin TABLib.TAB tabTelexList 
      Height          =   1170
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   480
      Width           =   3030
      _Version        =   65536
      _ExtentX        =   5345
      _ExtentY        =   2064
      _StockProps     =   64
      Columns         =   9
      Lines           =   10
   End
   Begin VB.TextBox txtTelexList 
      BackColor       =   &H8000000F&
      CausesValidation=   0   'False
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0C0C0&
      Height          =   1455
      Left            =   0
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   300
      Width           =   3405
   End
   Begin VB.TextBox txtDummy 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   150
      TabIndex        =   2
      Top             =   1800
      Width           =   1125
   End
End
Attribute VB_Name = "RcvTelex"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub chkLock_Click()
    If chkLock.Value = 1 Then
        tabTelexList(0).LockScroll True
        TelexPoolHead.tabTelexList(2).LockScroll True
    Else
        tabTelexList(0).LockScroll False
        TelexPoolHead.tabTelexList(2).LockScroll False
    End If
End Sub

Private Sub Form_Load()
    InitForm 6
    InitPosSize
End Sub

Private Sub InitPosSize()
    Me.Width = 3600
    Me.Top = 3675
    Me.Left = 10100
    Me.Height = 6000
End Sub

Private Sub InitForm(ipLines As Integer)
    Dim RcvIdx As Integer
    RcvTelex.Tag = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "RECV_CAPTION", "Received Telexes")
    RcvTelex.Caption = RcvTelex.Tag
    For RcvIdx = 0 To 1
        tabTelexList(RcvIdx).ResetContent
        tabTelexList(RcvIdx).FontName = "Courier New"
        tabTelexList(RcvIdx).HeaderFontSize = MyFontSize
        tabTelexList(RcvIdx).FontSize = MyFontSize
        tabTelexList(RcvIdx).LineHeight = MyFontSize
        tabTelexList(RcvIdx).SetTabFontBold MyFontBold
        tabTelexList(RcvIdx).Top = txtTelexList.Top + 1 * Screen.TwipsPerPixelY
        tabTelexList(RcvIdx).Left = txtTelexList.Left + 1 * Screen.TwipsPerPixelX
        tabTelexList(RcvIdx).Width = txtTelexList.Width - 2 * Screen.TwipsPerPixelX
        tabTelexList(RcvIdx).Height = ipLines * tabTelexList(0).LineHeight * Screen.TwipsPerPixelY
        tabTelexList(RcvIdx).HeaderString = "S,W,Type,ST,Time,No,Text Extract,URNO,Index"
        tabTelexList(RcvIdx).HeaderLengthString = "12,12,32,22,40,31,819,81,41"
        tabTelexList(RcvIdx).LifeStyle = True
        tabTelexList(RcvIdx).ShowHorzScroller True
        tabTelexList(RcvIdx).DateTimeSetColumn 4
        tabTelexList(RcvIdx).DateTimeSetInputFormatString 4, "YYYYMMDDhhmmss"
        tabTelexList(RcvIdx).DateTimeSetOutputFormatString 4, "hh':'mm"
        tabTelexList(RcvIdx).AutoSizeByHeader = True
        tabTelexList(RcvIdx).AutoSizeColumns
    Next
    tabTelexList(1).Left = 1200
    txtTelexList.Height = tabTelexList(0).Height + 2 * Screen.TwipsPerPixelY
    Me.Height = txtTelexList.Top + txtTelexList.Height + (Me.Height - Me.ScaleHeight)
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Cancel = CheckCancelExit(Me, 3)
    End If
End Sub

Private Sub Form_Resize()
    If Me.WindowState <> vbMinimized Then
        txtTelexList.Width = Me.ScaleWidth - 2 * txtTelexList.Left
        tabTelexList(0).Width = Me.ScaleWidth - 2 * tabTelexList(0).Left
        tabTelexList(1).Width = tabTelexList(0).Width
        If Me.ScaleHeight > 500 Then
            txtTelexList.Height = Me.ScaleHeight - txtTelexList.Top - txtTelexList.Left
            tabTelexList(0).Height = Me.ScaleHeight - tabTelexList(0).Top - tabTelexList(0).Left
            tabTelexList(1).Height = tabTelexList(0).Height
        End If
    End If
End Sub

Private Sub chkLoad_Click()
    txtDummy.SetFocus
    If chkLoad.Value = 1 Then
        chkLoad.BackColor = vbGreen
        TelexPoolHead.RefreshOnlineWindows "RCV"
        chkLoad.Value = 0
    Else
        chkLoad.BackColor = vbButtonFace
    End If
End Sub

Private Sub OnTop_Click()
    If OnTop.Value = 1 Then OnTop.BackColor = LightGreen Else OnTop.BackColor = vbButtonFace
    If OnTop.Value <> 1 Then SetFormOnTop Me, False
    SetAllFormsOnTop True
End Sub

Private Sub OpenPool_Click()
    If OpenPool.Value = 1 Then
        If TelexPoolHead.Visible = False Then TelexPoolHead.Show
        If TelexPoolHead.WindowState = vbMinimized Then TelexPoolHead.WindowState = vbNormal
        OpenPool.Value = 0
        TelexPoolHead.SetFocus
    End If
End Sub

Private Sub tabTelexList_SendLButtonClick(Index As Integer, ByVal Line As Long, ByVal Col As Long)
    Dim tmpUrno As String
    Dim tmpWsta As String
    Dim clSqlKey As String
    Dim RetCode As Integer
    Dim RetVal As String
    tmpWsta = Trim(tabTelexList(Index).GetColumnValue(Line, 1))
    tmpUrno = Trim(tabTelexList(Index).GetColumnValue(Line, 7))
    If tmpUrno <> "" Then
        clSqlKey = "WHERE URNO=" & tmpUrno
        If tmpWsta = "" Then
            RetCode = UfisServer.CallCeda(RetVal, "URT", DataPool.TableName, "WSTA", "V", clSqlKey, "", 0, True, False)
        End If
        TelexPoolHead.LoadTelexData 2, clSqlKey, "", "ALL", "RCV"
    End If
End Sub

Private Sub tabTelexList_TimerExpired(Index As Integer, ByVal LineNo As Long, ByVal LineStatus As Long)
    Dim tmpUrno As String
    If Index = 1 Then
        If MainIsOnline Then
            tmpUrno = Trim(tabTelexList(1).GetColumnValue(LineNo, 7))
            DataPool.ClearOnlineTelex tmpUrno
        End If
        tabTelexList(Index).DeleteLine LineNo
        If tabTelexList(Index).GetLineCount < 1 Then Timer1.Enabled = False
    End If
End Sub

Private Sub Timer1_Timer()
    tabTelexList(1).TimerCheck
End Sub

Public Sub ShowHiddenTelex(LineIdx As Long)
    Dim tmpData As String
    Dim tmpSere As String
    Dim tmpTtyp As String
    Dim tmpText As String
    Dim FilterLookUp As String
    Dim RcvTlxLine As String
    Dim LineNo As Long
    Dim MaxLinCnt As Long
    Dim myTextColor As Long
    Dim myBackColor As Long
    Dim ShowTelex As Boolean
    Dim tmpUrno As String
    ShowTelex = True
    tmpTtyp = Trim(tabTelexList(1).GetColumnValue(LineIdx, 2))
    tmpText = Trim(tabTelexList(1).GetColumnValue(LineIdx, 6))
    If MySetUp.AddrFilter.Value = 1 Then ShowTelex = FilterTelexAddress(tmpText, MySetUp.HomeTlxAddr.Text, AddrNoCheck, tmpTtyp, True)
    ShowTelex = FilterTelexText(tmpText, IgnoreAllTlxText, ShowTelex)
    ShowTelex = FilterTelexTypeText(tmpTtyp, tmpText, TextFilterList, ShowTelex)
    If (tmpText = "") And (IgnoreEmptyText) Then ShowTelex = False
    If ShowTelex Then
        ShowTelex = False
        FilterLookUp = MySetUp.CheckOnlineFilter("LOOKUP_RCV", tmpTtyp)
        If FilterLookUp = "OK" Then
            ShowTelex = True
            LineNo = -1
            If MainIsOnline Then
                tmpUrno = Trim(tabTelexList(1).GetColumnValue(LineIdx, 7))
                LineNo = DataPool.ReleaseOnlineTelex(tmpUrno)
            End If
            tmpSere = "R"
            tmpData = TelexPoolHead.GetTelexExtract(tmpSere, tmpTtyp, tmpText, 80)
            tabTelexList(1).SetColumnValue LineIdx, 6, tmpData
            RcvTlxLine = tabTelexList(1).GetLineValues(LineIdx)
            tabTelexList(0).InsertTextLineAt 0, RcvTlxLine, True
            tabTelexList(1).GetLineColor LineIdx, myTextColor, myBackColor
            tabTelexList(0).SetLineColor 0, myTextColor, myBackColor
            If MainIsOnline Then
                TelexPoolHead.tabTelexList(2).InsertTextLineAt 0, RcvTlxLine, True
                TelexPoolHead.tabTelexList(2).SetLineColor 0, myTextColor, myBackColor
                TelexPoolHead.tabTelexList(2).SetColumnValue 0, 8, CStr(LineNo)
                TelexPoolHead.tabTelexList(2).AutoSizeColumns
            End If
            MaxLinCnt = tabTelexList(0).GetLineCount
            If Not MainIsOnline Then
                If MaxLinCnt > MaxOnlineCount Then
                    MaxLinCnt = MaxLinCnt - 1
                    tabTelexList(0).DeleteLine MaxLinCnt
                End If
            End If
            Me.Caption = CStr(MaxLinCnt) & " " & Me.Tag
            tabTelexList(0).Refresh
        End If
    End If
    If (MainIsOnline) And (Not ShowTelex) Then
        tmpUrno = Trim(tabTelexList(1).GetColumnValue(LineIdx, 7))
        DataPool.ClearOnlineTelex tmpUrno
    End If
    tabTelexList(1).DeleteLine LineIdx
    If tabTelexList(1).GetLineCount < 1 Then Timer1.Enabled = False
End Sub

