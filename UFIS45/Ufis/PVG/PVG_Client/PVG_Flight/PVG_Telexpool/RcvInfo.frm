VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form RcvInfo 
   Caption         =   "Flight Interface"
   ClientHeight    =   2370
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4380
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "RcvInfo.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   2370
   ScaleWidth      =   4380
   StartUpPosition =   3  'Windows Default
   Tag             =   "Flight Interface"
   Begin VB.CheckBox chkImp 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   4
      Left            =   2610
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   300
      Width           =   855
   End
   Begin VB.CheckBox chkImp 
      Caption         =   "Select"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   3
      Left            =   1740
      Style           =   1  'Graphical
      TabIndex        =   11
      Top             =   300
      Width           =   855
   End
   Begin VB.CheckBox chkImp 
      Caption         =   "Spooler"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   870
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   300
      Width           =   855
   End
   Begin VB.CheckBox chkImp 
      Caption         =   "AI"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   435
      Style           =   1  'Graphical
      TabIndex        =   9
      ToolTipText     =   "Auto Import"
      Top             =   300
      Width           =   420
   End
   Begin VB.CheckBox chkImp 
      Caption         =   "AT"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   0
      Style           =   1  'Graphical
      TabIndex        =   8
      ToolTipText     =   "Auto Transmit"
      Top             =   300
      Width           =   420
   End
   Begin TABLib.TAB tabTelexList 
      Height          =   1170
      Index           =   1
      Left            =   1170
      TabIndex        =   7
      Top             =   870
      Visible         =   0   'False
      Width           =   3030
      _Version        =   65536
      _ExtentX        =   5345
      _ExtentY        =   2064
      _StockProps     =   64
      Columns         =   9
      Lines           =   10
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   3630
      Top             =   1710
   End
   Begin VB.CheckBox chkLock 
      Caption         =   "Lock"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1740
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   0
      Width           =   855
   End
   Begin VB.CheckBox OpenPool 
      Caption         =   "Main"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   0
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   0
      Width           =   855
   End
   Begin VB.CheckBox chkLoad 
      Caption         =   "Refresh"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   870
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   0
      Width           =   855
   End
   Begin VB.CheckBox OnTop 
      Caption         =   "On Top"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2610
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   0
      Width           =   855
   End
   Begin TABLib.TAB tabTelexList 
      Height          =   1170
      Index           =   0
      Left            =   150
      TabIndex        =   0
      Top             =   1170
      Width           =   3030
      _Version        =   65536
      _ExtentX        =   5345
      _ExtentY        =   2064
      _StockProps     =   64
      Columns         =   9
      Lines           =   10
   End
   Begin VB.TextBox txtTelexList 
      BackColor       =   &H8000000F&
      CausesValidation=   0   'False
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0C0C0&
      Height          =   1455
      Left            =   0
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   600
      Width           =   3405
   End
   Begin VB.TextBox txtDummy 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   150
      TabIndex        =   2
      Top             =   1800
      Width           =   1125
   End
End
Attribute VB_Name = "RcvInfo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub chkImp_Click(Index As Integer)
    Dim tmpData As String
    If chkImp(Index).Value = 1 Then
        chkImp(Index).BackColor = LightGreen
        Select Case Index
            Case 0  'AT
                TelexPoolHead.MsgToImportTool "0,ATSET"
                chkImp(2).Value = 1
                chkImp(3).Value = 0
            Case 1  'AI
                TelexPoolHead.MsgToImportTool "0,AISET"
            Case 2
                TelexPoolHead.chkTool(1).Value = 1
                chkImp(3).Value = 1
            Case Else
        End Select
    Else
        Select Case Index
            Case 0  'AT
                TelexPoolHead.MsgToImportTool "0,ATOFF"
            Case 1  'AI
                TelexPoolHead.MsgToImportTool "0,AIOFF"
            Case 2
                TelexPoolHead.chkTool(1).Value = 0
                chkImp(0).Value = 0
            Case Else
        End Select
        chkImp(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub chkLock_Click()
    If chkLock.Value = 1 Then
        tabTelexList(0).LockScroll True
    Else
        tabTelexList(0).LockScroll False
    End If
    txtDummy.SetFocus
End Sub

Private Sub Form_Load()
    InitForm 6
    InitPosSize
End Sub

Private Sub InitPosSize()
    Me.Width = 3600
    Me.Top = 3675
    Me.Left = 10100
    Me.Height = 6000
End Sub

Private Sub InitForm(ipLines As Integer)
    Dim i As Integer
    For i = 0 To 1
        tabTelexList(i).ResetContent
        tabTelexList(i).FontName = "Courier New"
        tabTelexList(i).HeaderFontSize = MyFontSize
        tabTelexList(i).FontSize = MyFontSize
        tabTelexList(i).LineHeight = MyFontSize
        tabTelexList(i).SetTabFontBold MyFontBold
        tabTelexList(i).Top = txtTelexList.Top + 1 * Screen.TwipsPerPixelY
        tabTelexList(i).Left = txtTelexList.Left + 1 * Screen.TwipsPerPixelX
        tabTelexList(i).Width = txtTelexList.Width - 2 * Screen.TwipsPerPixelX
        tabTelexList(i).Height = ipLines * tabTelexList(i).LineHeight * Screen.TwipsPerPixelY
        txtTelexList.Height = tabTelexList(i).Height + 2 * Screen.TwipsPerPixelY
        tabTelexList(i).HeaderString = "S,W,Type,ST,Time,No,Text Extract,URNO,Index"
        tabTelexList(i).HeaderLengthString = "12,12,32,22,40,31,819,81,41"
        tabTelexList(i).LifeStyle = True
        tabTelexList(i).ShowHorzScroller True
        tabTelexList(i).DateTimeSetColumn 4
        tabTelexList(i).DateTimeSetInputFormatString 4, "YYYYMMDDhhmmss"
        tabTelexList(i).DateTimeSetOutputFormatString 4, "hh':'mm"
        tabTelexList(i).AutoSizeByHeader = True
        tabTelexList(i).AutoSizeColumns
    Next
    Me.Height = txtTelexList.Top + txtTelexList.Height + (Me.Height - Me.ScaleHeight)
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Cancel = CheckCancelExit(Me, 3)
    End If
End Sub

Private Sub Form_Resize()
    If Me.WindowState <> vbMinimized Then
        txtTelexList.Width = Me.ScaleWidth - 2 * txtTelexList.Left
        tabTelexList(0).Width = Me.ScaleWidth - 2 * tabTelexList(0).Left
        If Me.ScaleHeight > 500 Then
            txtTelexList.Height = Me.ScaleHeight - txtTelexList.Top - txtTelexList.Left
            tabTelexList(0).Height = Me.ScaleHeight - tabTelexList(0).Top - tabTelexList(0).Left
        End If
    End If
End Sub

Private Sub chkLoad_Click()
    txtDummy.SetFocus
    If chkLoad.Value = 1 Then
        chkLoad.BackColor = vbGreen
        TelexPoolHead.RefreshOnlineWindows "INF"
        chkLoad.Value = 0
    Else
        chkLoad.BackColor = vbButtonFace
    End If
End Sub

Private Sub OnTop_Click()
    If OnTop.Value = 1 Then OnTop.BackColor = LightGreen Else OnTop.BackColor = vbButtonFace
    If OnTop.Value <> 1 Then SetFormOnTop Me, False
    SetAllFormsOnTop True
End Sub

Private Sub OpenPool_Click()
    If OpenPool.Value = 1 Then
        If TelexPoolHead.Visible = False Then TelexPoolHead.Show
        If TelexPoolHead.WindowState = vbMinimized Then TelexPoolHead.WindowState = vbNormal
        OpenPool.Value = 0
        TelexPoolHead.SetFocus
    End If
End Sub

Private Sub tabTelexList_SendLButtonClick(Index As Integer, ByVal Line As Long, ByVal Col As Long)
    Dim tmpUrno As String
    Dim tmpWsta As String
    Dim clSqlKey As String
    Dim RetCode As Integer
    Dim RetVal As String
    Dim LineNo As Long
    tmpWsta = tabTelexList(Index).GetColumnValue(Line, 1)
    tmpUrno = tabTelexList(Index).GetColumnValue(Line, 7)
    If tmpUrno <> "" Then
        clSqlKey = "WHERE URNO=" & tmpUrno
        If tmpWsta = "" Then
            tmpWsta = "V"
            RetCode = UfisServer.CallCeda(RetVal, "URT", DataPool.TableName, "WSTA", tmpWsta, clSqlKey, "", 0, True, False)
        End If
        If chkImp(3).Value = 0 Then TelexPoolHead.LoadTelexData 2, clSqlKey, "", "ALL", "RCV"
    End If
End Sub

Private Sub tabTelexList_SendLButtonDblClick(Index As Integer, ByVal Line As Long, ByVal ColNo As Long)
    Dim tmpUrno As String
    Dim tmpWsta As String
    Dim clSqlKey As String
    Dim RetCode As Integer
    Dim RetVal As String
    Dim LineNo As Long
    If Index = 0 Then
        If Line < 0 Then
            tabTelexList(0).Sort CStr(ColNo), True, True
            tabTelexList(0).AutoSizeColumns
        Else
            tmpWsta = tabTelexList(Index).GetColumnValue(Line, 1)
            tmpUrno = tabTelexList(Index).GetColumnValue(Line, 7)
            If tmpUrno <> "" Then
                clSqlKey = "WHERE URNO=" & tmpUrno
                If tmpWsta = "" Then
                    tmpWsta = "V"
                    RetCode = UfisServer.CallCeda(RetVal, "URT", DataPool.TableName, "WSTA", tmpWsta, clSqlKey, "", 0, True, False)
                End If
                If chkImp(3).Value = 1 Then
                    TelexPoolHead.LoadTelexData 7, clSqlKey, "", "", ""
                    LineNo = DataPool.TelexData(7).GetLineCount
                    If LineNo > 0 Then TelexPoolHead.ScoreToImportTool 7, 0
                End If
            End If
        End If
    End If
End Sub

Public Sub ShowHiddenTelex(LineIdx As Long)
    Dim tmpData As String
    Dim tmpWsta As String
    Dim tmpStat As String
    Dim tmpSere As String
    Dim tmpTtyp As String
    Dim tmpText As String
    Dim FilterLookUp As String
    Dim RcvTlxLine As String
    Dim clSqlKey As String
    Dim tmpUrno As String
    Dim MaxLinCnt As Long
    Dim LineNo As Long
    Dim myTextColor As Long
    Dim myBackColor As Long
    Dim ShowTelex As Boolean
    Dim JustShown As Boolean
    ShowTelex = True
    tmpTtyp = Trim(tabTelexList(1).GetColumnValue(LineIdx, 2))
    tmpText = Trim(tabTelexList(1).GetColumnValue(LineIdx, 6))
    If MySetUp.AddrFilter.Value = 1 Then ShowTelex = FilterTelexAddress(tmpText, MySetUp.HomeTlxAddr.Text, AddrNoCheck, tmpTtyp, True)
    ShowTelex = FilterTelexText(tmpText, IgnoreAllTlxText, ShowTelex)
    ShowTelex = FilterTelexTypeText(tmpTtyp, tmpText, TextFilterList, ShowTelex)
    If (tmpText = "") And (IgnoreEmptyText) Then ShowTelex = False
    If ShowTelex Then
        FilterLookUp = MySetUp.CheckOnlineFilter("LOOKUP_RCV", tmpTtyp)
        If FilterLookUp = "OK" Then
            tmpSere = "R"
            tmpData = TelexPoolHead.GetTelexExtract(tmpSere, tmpTtyp, tmpText, 80)
            tabTelexList(1).SetColumnValue LineIdx, 6, tmpData
            RcvTlxLine = tabTelexList(1).GetLineValues(LineIdx)
            tabTelexList(0).InsertTextLineAt 0, RcvTlxLine, True
            JustShown = True
            tmpStat = Trim(tabTelexList(0).GetColumnValue(0, 0))
            tmpWsta = Trim(tabTelexList(0).GetColumnValue(0, 1))
            If tmpWsta <> "" Then
                ColorPool.GetStatusColor tmpWsta, "", myTextColor, myBackColor
            Else
                ColorPool.GetStatusColor tmpStat, "", myTextColor, myBackColor
            End If
            tabTelexList(0).SetLineColor 0, myTextColor, myBackColor
            MaxLinCnt = tabTelexList(0).GetLineCount
            If MaxLinCnt > MaxOnlineCount Then
                MaxLinCnt = MaxLinCnt - 1
                tabTelexList(0).DeleteLine MaxLinCnt
            End If
            Me.Caption = CStr(MaxLinCnt) & " " & Me.Tag
            tabTelexList(0).Refresh
        End If
        tabTelexList(1).DeleteLine LineIdx
        If tabTelexList(1).GetLineCount < 1 Then Timer1.Enabled = False
        If chkImp(0).Value = 1 Then
            If (JustShown) And (tmpTtyp = "SCOR") Then
                tmpUrno = tabTelexList(0).GetColumnValue(0, 7)
                If tmpUrno <> "" Then
                    clSqlKey = "WHERE URNO=" & tmpUrno
                    TelexPoolHead.LoadTelexData 7, clSqlKey, "", "", ""
                    LineNo = DataPool.TelexData(7).GetLineCount
                    If LineNo > 0 Then TelexPoolHead.ScoreToImportTool 7, 0
                End If
            End If
        End If
    End If
End Sub

Private Sub tabTelexList_TimerExpired(Index As Integer, ByVal LineNo As Long, ByVal LineStatus As Long)
    If Index = 1 Then ShowHiddenTelex LineNo
End Sub

Private Sub Timer1_Timer()
    tabTelexList(1).TimerCheck
End Sub
