VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Begin VB.Form SndTelex 
   Caption         =   "Created Telexes"
   ClientHeight    =   2355
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   3510
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "SndTelex.frx":0000
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   ScaleHeight     =   2355
   ScaleWidth      =   3510
   StartUpPosition =   3  'Windows Default
   Tag             =   "Created Telexes"
   Begin VB.CheckBox chkLock 
      Caption         =   "Lock"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1740
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   0
      Width           =   855
   End
   Begin VB.CheckBox chkLoad 
      Caption         =   "Refresh"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   870
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   0
      Width           =   855
   End
   Begin VB.CheckBox OpenPool 
      Caption         =   "Main"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   0
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   0
      Width           =   855
   End
   Begin VB.CheckBox OnTop 
      Caption         =   "On Top"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2610
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   0
      Width           =   855
   End
   Begin TABLib.TAB tabTelexList 
      Height          =   1170
      Left            =   150
      TabIndex        =   0
      Top             =   600
      Width           =   3030
      _Version        =   65536
      _ExtentX        =   5345
      _ExtentY        =   2064
      _StockProps     =   64
      Columns         =   9
      Lines           =   10
   End
   Begin VB.TextBox txtTelexList 
      BackColor       =   &H8000000F&
      CausesValidation=   0   'False
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0C0C0&
      Height          =   1365
      Left            =   0
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   300
      Width           =   3315
   End
   Begin VB.TextBox txtDummy 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   180
      TabIndex        =   3
      Top             =   1770
      Width           =   1125
   End
End
Attribute VB_Name = "SndTelex"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub chkLock_Click()
    If chkLock.Value = 1 Then
        tabTelexList.LockScroll True
    Else
        tabTelexList.LockScroll False
    End If
    txtDummy.SetFocus
End Sub

Private Sub Form_Load()
    InitForm 6
    InitPosSize
End Sub
Private Sub InitPosSize()
    Me.Width = 3600
    Me.Top = 3675
    Me.Left = 10100
    Me.Height = 6000
End Sub

Private Sub InitForm(ipLines As Integer)
    tabTelexList.ResetContent
    tabTelexList.FontName = "Courier New"
    tabTelexList.HeaderFontSize = MyFontSize
    tabTelexList.FontSize = MyFontSize
    tabTelexList.LineHeight = MyFontSize
    tabTelexList.SetTabFontBold MyFontBold
    tabTelexList.Top = txtTelexList.Top + 1 * Screen.TwipsPerPixelY
    tabTelexList.Left = txtTelexList.Left + 1 * Screen.TwipsPerPixelX
    tabTelexList.Width = txtTelexList.Width - 2 * Screen.TwipsPerPixelX
    tabTelexList.Height = ipLines * tabTelexList.LineHeight * Screen.TwipsPerPixelY
    txtTelexList.Height = tabTelexList.Height + 2 * Screen.TwipsPerPixelY
    tabTelexList.HeaderLengthString = "12,12,32,22,61,47,41,41,41,81,81"
    tabTelexList.HeaderString = "S,W,Type,ST,Flight,Regis,Plan,Time1,Time2,URNO,Index"
    tabTelexList.DateTimeSetColumn 6
    tabTelexList.DateTimeSetInputFormatString 6, "YYYYMMDDhhmmss"
    tabTelexList.DateTimeSetOutputFormatString 6, "hh':'mm"
    tabTelexList.DateTimeSetColumn 7
    tabTelexList.DateTimeSetInputFormatString 7, "YYYYMMDDhhmmss"
    tabTelexList.DateTimeSetOutputFormatString 7, "hh':'mm"
    tabTelexList.DateTimeSetColumn 8
    tabTelexList.DateTimeSetInputFormatString 8, "YYYYMMDDhhmmss"
    tabTelexList.DateTimeSetOutputFormatString 8, "hh':'mm"
    tabTelexList.LifeStyle = True
    tabTelexList.ShowHorzScroller True
    tabTelexList.AutoSizeByHeader = True
    tabTelexList.AutoSizeColumns
    Me.Height = txtTelexList.Top + txtTelexList.Height + (Me.Height - Me.ScaleHeight)
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Cancel = CheckCancelExit(Me, 3)
    End If
End Sub

Private Sub Form_Resize()
    If Me.WindowState <> vbMinimized Then
        txtTelexList.Width = Me.ScaleWidth - 2 * txtTelexList.Left
        tabTelexList.Width = Me.ScaleWidth - 2 * tabTelexList.Left
        If Me.ScaleHeight > 500 Then
            txtTelexList.Height = Me.ScaleHeight - txtTelexList.Top - txtTelexList.Left
            tabTelexList.Height = Me.ScaleHeight - tabTelexList.Top - tabTelexList.Left
        End If
    End If
End Sub
Private Sub chkLoad_Click()
    txtDummy.SetFocus
    If chkLoad.Value = 1 Then
        chkLoad.BackColor = vbGreen
        TelexPoolHead.RefreshOnlineWindows "SND"
        chkLoad.Value = 0
    Else
        chkLoad.BackColor = vbButtonFace
    End If
End Sub

Private Sub OnTop_Click()
    If OnTop.Value = 1 Then OnTop.BackColor = LightGreen Else OnTop.BackColor = vbButtonFace
    If OnTop.Value <> 1 Then SetFormOnTop Me, False
    SetAllFormsOnTop True
End Sub

Private Sub OpenPool_Click()
    If OpenPool.Value = 1 Then
        If TelexPoolHead.Visible = False Then TelexPoolHead.Show
        If TelexPoolHead.WindowState = vbMinimized Then TelexPoolHead.WindowState = vbNormal
        OpenPool.Value = 0
        TelexPoolHead.SetFocus
    End If
End Sub

Private Sub tabTelexList_SendLButtonClick(ByVal Line As Long, ByVal Col As Long)
    Dim tmpUrno As String
    Dim tmpWsta As String
    Dim clSqlKey As String
    Dim RetCode As Integer
    Dim RetVal As String
    tmpWsta = Trim(tabTelexList.GetColumnValue(Line, 1))
    tmpUrno = Trim(tabTelexList.GetColumnValue(Line, 9))
    If tmpUrno <> "" Then
        clSqlKey = "WHERE URNO=" & tmpUrno
        If tmpWsta = "" Then
            RetCode = UfisServer.CallCeda(RetVal, "URT", DataPool.TableName, "WSTA", "V", clSqlKey, "", 0, True, False)
        End If
        TelexPoolHead.LoadTelexData 3, clSqlKey, "", "ALL", "SND"
    End If
End Sub
