VERSION 5.00
Begin VB.Form MySetUp 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Setup"
   ClientHeight    =   8310
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7155
   Icon            =   "MySetUp.frx":0000
   LinkTopic       =   "Form3"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8310
   ScaleWidth      =   7155
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   60000
      Left            =   2850
      Top             =   0
   End
   Begin VB.CheckBox OnTop 
      Caption         =   "On Top"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6120
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   1560
      Width           =   855
   End
   Begin VB.CommandButton Reset 
      Caption         =   "Reset"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6120
      TabIndex        =   1
      Top             =   1860
      Width           =   855
   End
   Begin VB.CommandButton btnClose 
      Cancel          =   -1  'True
      Caption         =   "&Close"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6120
      TabIndex        =   0
      Top             =   2160
      Width           =   855
   End
   Begin VB.Frame SetupGroup 
      Caption         =   "System"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2475
      Index           =   0
      Left            =   60
      TabIndex        =   3
      Top             =   90
      Visible         =   0   'False
      Width           =   7035
      Begin VB.CheckBox RcvTabList 
         Caption         =   "First Browser"
         Enabled         =   0   'False
         Height          =   315
         Left            =   2760
         TabIndex        =   79
         Top             =   1410
         Value           =   1  'Checked
         Width           =   2115
      End
      Begin VB.CheckBox FlightBrowser 
         Caption         =   "Flight Browser"
         Enabled         =   0   'False
         Height          =   315
         Left            =   2760
         TabIndex        =   76
         Top             =   1950
         Width           =   2115
      End
      Begin VB.CheckBox SndTabList 
         Caption         =   "Second Browser"
         Enabled         =   0   'False
         Height          =   315
         Left            =   2760
         TabIndex        =   75
         Top             =   1680
         Width           =   2115
      End
      Begin VB.CheckBox FolderMode 
         Caption         =   "Show Folders "
         Enabled         =   0   'False
         Height          =   315
         Left            =   2760
         TabIndex        =   74
         Top             =   1140
         Width           =   2115
      End
      Begin VB.CheckBox TestCases 
         Caption         =   "Test Case Mode"
         Enabled         =   0   'False
         Height          =   315
         Left            =   2760
         TabIndex        =   73
         Top             =   870
         Width           =   2115
      End
      Begin VB.CheckBox AddrFilter 
         Caption         =   "Address Filter"
         Enabled         =   0   'False
         Height          =   315
         Left            =   2760
         TabIndex        =   72
         Top             =   600
         Width           =   2115
      End
      Begin VB.TextBox sndTlxTxtDefault 
         Height          =   1815
         Left            =   120
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         TabIndex        =   7
         ToolTipText     =   "Default Text of SITA File Interface"
         Top             =   540
         Width           =   2565
      End
      Begin VB.TextBox HomeTlxAddr 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   120
         Locked          =   -1  'True
         TabIndex        =   6
         Text            =   "WWWWWWW"
         ToolTipText     =   "Own SITA Type B Address"
         Top             =   240
         Width           =   5475
      End
      Begin VB.TextBox OrderKey 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5280
         Locked          =   -1  'True
         TabIndex        =   5
         ToolTipText     =   "SQL ORDER BY ..."
         Top             =   630
         Visible         =   0   'False
         Width           =   1605
      End
      Begin VB.TextBox DefDateFormat 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5640
         TabIndex        =   4
         ToolTipText     =   "Used Date Format"
         Top             =   240
         Width           =   1260
      End
   End
   Begin VB.Frame SetupGroup 
      Caption         =   "Loader"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2475
      Index           =   1
      Left            =   60
      TabIndex        =   8
      Top             =   2640
      Visible         =   0   'False
      Width           =   7035
      Begin VB.CommandButton Command1 
         Caption         =   "Data"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   840
         TabIndex        =   78
         Top             =   1530
         Visible         =   0   'False
         Width           =   1065
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Prep"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   840
         TabIndex        =   77
         Top             =   1800
         Visible         =   0   'False
         Width           =   1065
      End
      Begin VB.TextBox MyUtcTimeDiff 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5130
         Locked          =   -1  'True
         TabIndex        =   20
         Text            =   "60"
         ToolTipText     =   "UTC Time Difference"
         Top             =   330
         Width           =   660
      End
      Begin VB.TextBox ServerTimeType 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4560
         Locked          =   -1  'True
         TabIndex        =   19
         Text            =   "LOC"
         Top             =   330
         Width           =   525
      End
      Begin VB.TextBox OnlineFrom 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2430
         TabIndex        =   15
         Text            =   "-2"
         Top             =   1020
         Width           =   495
      End
      Begin VB.TextBox OnlineTo 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2970
         TabIndex        =   14
         Text            =   "+1"
         Top             =   1020
         Width           =   525
      End
      Begin VB.TextBox ServerTime 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2430
         Locked          =   -1  'True
         TabIndex        =   11
         Tag             =   "200205240800"
         Text            =   "24.05.2002 / 08:00"
         Top             =   330
         Width           =   2070
      End
      Begin VB.TextBox MainTo 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2970
         TabIndex        =   10
         Text            =   "+1"
         Top             =   690
         Width           =   525
      End
      Begin VB.TextBox MainFrom 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2430
         TabIndex        =   9
         Text            =   "-2"
         Top             =   690
         Width           =   495
      End
      Begin VB.Label Label5 
         Caption         =   "Server Reference Time"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   150
         TabIndex        =   18
         Top             =   390
         Width           =   2085
      End
      Begin VB.Label Label4 
         Caption         =   "Hours"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3540
         TabIndex        =   17
         Top             =   1080
         Width           =   555
      End
      Begin VB.Label Label3 
         Caption         =   "Hours"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3540
         TabIndex        =   16
         Top             =   750
         Width           =   555
      End
      Begin VB.Label Label2 
         Caption         =   "Online Windows Refresh"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   150
         TabIndex        =   13
         Top             =   1080
         Width           =   2145
      End
      Begin VB.Label Label1 
         Caption         =   "Main Data Loader"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   150
         TabIndex        =   12
         Top             =   750
         Width           =   2085
      End
   End
   Begin VB.Frame SetupGroup 
      Caption         =   "Telex Types"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2475
      Index           =   2
      Left            =   60
      TabIndex        =   21
      Top             =   5190
      Visible         =   0   'False
      Width           =   7035
      Begin VB.Frame Frame2 
         Caption         =   "Sender"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2085
         Left            =   4380
         TabIndex        =   47
         Top             =   270
         Width           =   1125
         Begin VB.CheckBox chkRecv 
            Caption         =   "ELSE"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   47
            Left            =   120
            TabIndex        =   55
            Top             =   1770
            Value           =   1  'Checked
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Caption         =   "AAD"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   41
            Left            =   120
            TabIndex        =   54
            Top             =   510
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Caption         =   "BRS"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   42
            Left            =   120
            TabIndex        =   53
            Top             =   720
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Caption         =   "FREE"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   43
            Left            =   120
            TabIndex        =   52
            Top             =   930
            Value           =   1  'Checked
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   44
            Left            =   120
            TabIndex        =   51
            Top             =   1140
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   45
            Left            =   120
            TabIndex        =   50
            Top             =   1350
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   46
            Left            =   120
            TabIndex        =   49
            Top             =   1560
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Caption         =   "MVT"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   40
            Left            =   120
            TabIndex        =   48
            Top             =   300
            Value           =   1  'Checked
            Width           =   765
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Receiver"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2085
         Left            =   120
         TabIndex        =   22
         Top             =   270
         Width           =   4215
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   31
            Left            =   2550
            TabIndex        =   71
            Top             =   1770
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   32
            Left            =   3360
            TabIndex        =   70
            Top             =   300
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   33
            Left            =   3360
            TabIndex        =   69
            Top             =   510
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   34
            Left            =   3360
            TabIndex        =   68
            Top             =   720
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   35
            Left            =   3360
            TabIndex        =   67
            Top             =   930
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   36
            Left            =   3360
            TabIndex        =   66
            Top             =   1140
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   37
            Left            =   3360
            TabIndex        =   65
            Top             =   1350
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   38
            Left            =   3360
            TabIndex        =   64
            Top             =   1560
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   30
            Left            =   2550
            TabIndex        =   63
            Top             =   1560
            Width           =   735
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   24
            Left            =   2550
            TabIndex        =   62
            Top             =   300
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   25
            Left            =   2550
            TabIndex        =   61
            Top             =   510
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   26
            Left            =   2550
            TabIndex        =   60
            Top             =   720
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   27
            Left            =   2550
            TabIndex        =   59
            Top             =   930
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   28
            Left            =   2550
            TabIndex        =   58
            Top             =   1140
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   29
            Left            =   2550
            TabIndex        =   57
            Top             =   1350
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   23
            Left            =   1740
            TabIndex        =   56
            Top             =   1770
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   22
            Left            =   1740
            TabIndex        =   46
            Top             =   1560
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   16
            Left            =   1740
            TabIndex        =   45
            Top             =   300
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   17
            Left            =   1740
            TabIndex        =   44
            Top             =   510
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   18
            Left            =   1740
            TabIndex        =   43
            Top             =   720
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   19
            Left            =   1740
            TabIndex        =   42
            Top             =   930
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   20
            Left            =   1740
            TabIndex        =   41
            Top             =   1140
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   21
            Left            =   1740
            TabIndex        =   40
            Top             =   1350
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   15
            Left            =   930
            TabIndex        =   39
            Top             =   1770
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   14
            Left            =   930
            TabIndex        =   38
            Top             =   1560
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Caption         =   "SMA"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   9
            Left            =   930
            TabIndex        =   37
            Top             =   510
            Value           =   1  'Checked
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Caption         =   "ELSE"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   39
            Left            =   3360
            TabIndex        =   36
            Top             =   1770
            Value           =   1  'Checked
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Caption         =   "AAD"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   10
            Left            =   930
            TabIndex        =   35
            Top             =   720
            Value           =   1  'Checked
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Caption         =   "RQM"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   11
            Left            =   930
            TabIndex        =   34
            Top             =   930
            Value           =   1  'Checked
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Caption         =   "BSM"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   12
            Left            =   930
            TabIndex        =   33
            Top             =   1140
            Value           =   1  'Checked
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Caption         =   "KRIS"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   13
            Left            =   930
            TabIndex        =   32
            Top             =   1350
            Value           =   1  'Checked
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Caption         =   "SCR"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   8
            Left            =   930
            TabIndex        =   31
            Top             =   300
            Value           =   1  'Checked
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Caption         =   "MVT"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   0
            Left            =   120
            TabIndex        =   30
            Top             =   300
            Value           =   1  'Checked
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Caption         =   "SRM"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   6
            Left            =   120
            TabIndex        =   29
            Top             =   1560
            Value           =   1  'Checked
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Caption         =   "SAM"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   5
            Left            =   120
            TabIndex        =   28
            Top             =   1350
            Value           =   1  'Checked
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Caption         =   "ATC"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   4
            Left            =   120
            TabIndex        =   27
            Top             =   1140
            Value           =   1  'Checked
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Caption         =   "PTM"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   3
            Left            =   120
            TabIndex        =   26
            Top             =   930
            Value           =   1  'Checked
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Caption         =   "CPM"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   2
            Left            =   120
            TabIndex        =   25
            Top             =   720
            Value           =   1  'Checked
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Caption         =   "LDM"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   1
            Left            =   120
            TabIndex        =   24
            Top             =   510
            Value           =   1  'Checked
            Width           =   765
         End
         Begin VB.CheckBox chkRecv 
            Caption         =   "SLC"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   7
            Left            =   120
            TabIndex        =   23
            Top             =   1770
            Value           =   1  'Checked
            Width           =   765
         End
      End
   End
End
Attribute VB_Name = "MySetUp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private MyParent As Form
Private Sub Command1_Click()
    DataPool.Show
End Sub
Private Sub Command2_Click()
    TelexPoolHead.PrepareTelexExtract CurMem
End Sub


Public Sub RegisterMyParent(Caller As Form, CallButton As Control, TaskValue As String)
    Set MyParent = Caller
    With MyParent
        OnTop.Value = .OnTop.Value
    End With
End Sub

Private Sub btnClose_Click()
    Me.Hide
End Sub

Private Sub FlightBrowser_Click()
    If ApplicationIsStarted Then
        If FlightBrowser.Value = 1 Then
            TelexPoolHead.ArrangeFlightList True
        Else
            TelexPoolHead.ArrangeFlightList False
        End If
    End If
End Sub

Private Sub FolderMode_Click()
    If ApplicationIsStarted Then
        If FolderMode.Value = 1 Then
            TelexPoolHead.ArrangeLayout True
        Else
            TelexPoolHead.ArrangeLayout False
        End If
    End If
End Sub

Private Sub Reset_Click()
    Dim UseIdx As Integer
    On Error Resume Next
    With MyParent
        OnTop.Value = .OnTop.Value
    End With
    If SetupGroup(0).Tag <> "" Then
        UseIdx = Val(SetupGroup(0).Tag)
        InitSetupConfig UseIdx
    End If
End Sub

Private Sub Form_Load()
    Me.Height = 3000
    Me.Width = 7260
    If Me.ScaleHeight < (SetupGroup(0).Height + 180) Then
        Me.Height = Me.Height + (SetupGroup(0).Height + 180 - Me.ScaleHeight)
    End If
    
    'Left = .Left + 50 * Screen.TwipsPerPixelX
    'Top = .Top + 50 * Screen.TwipsPerPixelY
    InitSetupConfig 0
    InitSetupConfig 1
    InitSetupConfig 2
End Sub

Private Sub InitSetupConfig(GroupNo As Integer)
    Dim X As String
    Dim tmpData As String
    Dim clTxtDef As String
    Dim tmpTime As String
    Dim CliSec As Long
    Dim SrvSec As Long
    Select Case GroupNo
        Case 0
            tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "TEST_SAVE_LOCAL_DATA", "NO")
            If tmpData = "YES" Then
                SaveLocalTestData = True
                UfisServer.HostName.Tag = "TEST"
            Else
                SaveLocalTestData = False
                UfisServer.HostName.Tag = ""
            End If
            tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "HOLD_FILES", "NO")
            If tmpData = "YES" Then
                TelexPoolHead.chkAssFolder.Enabled = True
            Else
                TelexPoolHead.chkAssFolder.Enabled = False
            End If
            tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "MAX_FOLDER_ROWS", "3")
            MaxFolderRows = Val(tmpData)
            If MaxFolderRows < 1 Then MaxFolderRows = 1
            If MaxFolderRows > 6 Then MaxFolderRows = 6
            tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "MAIN_LIFE_STYLE", "7")
            MyLifeStyleValue = Val(tmpData)
            If MyLifeStyleValue < 0 Then MyLifeStyleValue = 0
            If MyLifeStyleValue > 7 Then MyLifeStyleValue = 7
            TelexPoolHead.Tag = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "MAIN_CAPTION", "Telex Pool")
            TelexPoolHead.chkTlxTitleBar(0).Caption = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "RECV_LIST_CAPTION", "Received Telexes")
            TelexPoolHead.chkTlxTitleBar(1).Caption = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SENT_LIST_CAPTION", "Sent Telexes")
            TelexPoolHead.chkTlxTitleBar(0).Tag = TelexPoolHead.chkTlxTitleBar(0).Caption
            TelexPoolHead.chkTlxTitleBar(1).Tag = TelexPoolHead.chkTlxTitleBar(1).Caption
            tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "HOMETLXADDR", "[????]")
            tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "WKS_SITA_ADDR", tmpData)
            HomeTlxAddr.Text = tmpData
            tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "ADDRESS_FILTER", "NO")
            If tmpData = "YES" Then AddrFilter.Value = 1 Else AddrFilter.Value = 0
            AddrNoCheck = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "ADDRESS_NOCHECK", "KRIS,ATC,AFTN,SCOR")
            tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "TELEX_TEMPLATES", "NO")
            If tmpData = "NO" Then
                TelexTemplates = False
            Else
                TelexTemplates = True
            End If
            tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "TESTCASE_MODE", "NO")
            tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "TEST_CASE_MODE", tmpData)
            If tmpData = "YES" Then TestCases.Value = 1 Else TestCases.Value = 0
            If tmpData = "YES" Then TestCaseMode = True Else TestCaseMode = False
            tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SHOW_FOLDERS", "YES")
            If tmpData = "YES" Then FolderMode.Value = 1 Else FolderMode.Value = 0
            If PoolConfig.OnLineCfg(3).Value = 1 Then clTxtDef = "YES" Else clTxtDef = "NO"
            tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SHOW_SENT_LIST", clTxtDef)
            If tmpData = "YES" Then
                SndTabList.Value = 1
                TelexPoolHead.chkNew.Enabled = True
            Else
                SndTabList.Value = 0
                TelexPoolHead.chkNew.Enabled = False
            End If
            
            tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SHOW_FLIGHT_LIST", "NO")
            If tmpData = "YES" Then
                FlightBrowser.Value = 1
            Else
                FlightBrowser.Value = 0
            End If
            
            clTxtDef = "=PRIORITY" & vbNewLine & "QU" & vbNewLine & "=DBLSIG" & vbNewLine & ".." & vbNewLine & "=DESTINATION TYPE B" & vbNewLine & "STX," & vbNewLine & "=TEXT" & vbNewLine
            sndTlxTxtDefault.Text = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "DEFAULTTEXT", clTxtDef)
            OrderKey.Text = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SQL_ORDER", "CDAT")
            DefDateFormat.Text = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "DATE_FORMAT", "dd.mm.yyyy")
            AllTelexTypes = GetAllTelexTypes
            FolderCaptions = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "FOLDER_CAPTIONS", AllTelexTypes)
            clTxtDef = FolderCaptions
            FolderCaptions = Replace(FolderCaptions, "|", ",", 1, -1, vbBinaryCompare)
            FolderTypeList = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "FOLDER_TYP_CODES", clTxtDef)
            FolderTypeList = Replace(FolderTypeList, "|", ",", 1, -1, vbBinaryCompare)
            tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "FOLDER_SIZE", "43")
            FolderSize = Val(tmpData) * 15
            If FolderSize < 600 Then FolderSize = 600
            If FolderSize > 1050 Then FolderSize = 1050
            MainNoShow = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "MAIN_NOSHOW", "")
            tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "IGNORE_EMPTY", "YES")
            If tmpData = "YES" Then IgnoreEmptyText = True Else IgnoreEmptyText = False
            
            ' "SITA_NODE" or "SITA_FILE"
            MySitaOutType = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SITA_OUT_TYPE", "SITA_NODE")
            MySitaDefPrio = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SITA_OUT_PRIO", "QU")
            tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SITA_OUT_ORIGIN", "NO")
            If tmpData = "YES" Then MySitaOutOrig = True Else MySitaOutOrig = False
            tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SITA_OUT_MSGID", "NO")
            If tmpData = "YES" Then MySitaOutMsgId = True Else MySitaOutMsgId = False
            tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SITA_OUT_SMI", "NO")
            If tmpData = "YES" Then MySitaOutSmi = True Else MySitaOutSmi = False
            tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SITA_OUT_DBLSIG", "NO")
            If tmpData = "YES" Then MySitaOutDblSign = True Else MySitaOutDblSign = False
            
            RecvTypeAction = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "RECV_TYPE_ACTION", "")
            RecvTypeAction = Replace(RecvTypeAction, ",", "|", 1, -1, vbBinaryCompare)
            SendTypeAction = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SEND_TYPE_ACTION", "")
            SendTypeAction = Replace(SendTypeAction, ",", "|", 1, -1, vbBinaryCompare)
            InitTypeTextFilter
            PathToImportTool = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "IMPORT_TOOL", "C:\Ufis\Appl\ImportFlights.exe")

        Case 1
            MainFrom.Text = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "MAIN_PRIOR", "-4")
            MainTo.Text = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "MAIN_AHEAD", "+16")
            OnlineFrom.Text = GetIniEntry(myIniFullName, "ONLINE_WINDOWS", "", "ONLINE_PRIOR", "-2")
            OnlineTo.Text = GetIniEntry(myIniFullName, "ONLINE_WINDOWS", "", "ONLINE_AHEAD", "+16")
            TelexPoolHead.fraTimeFilter.Tag = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "MAIN_MAX_TIMEFRAME", "")
            tmpTime = UfisServer.GetServerConfigItem("SRVT", 3, "")
            If tmpTime <> "" Then
                ServerTimeType.Text = "UTC"
            Else
                tmpTime = UfisServer.GetServerConfigItem("SRVT", 1, Format(Now, "yyyymmddhhmmss"))
                ServerTimeType.Text = UfisServer.GetServerConfigItem("SRVT", 2, "LOC")
            End If
            SrvSec = Val(Right(tmpTime, 2))
            '====================== TEST ============================
            tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "TEST_SET_SERVER_TIME", "")
            If tmpData <> "" Then
                tmpTime = tmpData
                MsgBox "Test Time set to: " & tmpTime
                TelexPoolHead.Tag = TelexPoolHead.Tag & " <TEST> "
            End If
            '====================== TEST ============================
            Mid(tmpTime, 13, 2) = "00"
            ServerTime.Tag = tmpTime
            ServerTime.Text = Format(CedaFullDateToVb(ServerTime.Tag), (DefDateFormat & " \/ hh:mm"))
            MyUtcTimeDiff.Text = CStr(UtcTimeDiff)
            CliSec = (60 - SrvSec) * 1000
            If CliSec <= 0 Then CliSec = 60000
            Timer1.Interval = CliSec
            Timer1.Enabled = True
        Case 2
            InitOnlineFilter
        Case Else
    End Select
End Sub
Private Function GetAllTelexTypes() As String
    Dim tmpFdiResult As String
    Dim RetVal As Integer
    tmpFdiResult = ""
    RetVal = -1
    If CedaIsConnected Then RetVal = UfisServer.CallCeda(tmpFdiResult, "GSI", "CONFIG", "TlxTypes", "/ceda/conf/fdihdl.cfg", "TELEX", "", 0, True, False)
    tmpFdiResult = Replace(tmpFdiResult, " ", "", 1, -1, vbBinaryCompare)
    If (RetVal < 0) Or (tmpFdiResult = "") Then tmpFdiResult = "MVT,LDM,CPM,UCM,PTM,TPM,PDM,PSM,ETM,FWD,DLS,BTM,SAM,SRM,SLC,SCR,SMA,RQM,BSM,ATC,DFS,KRIS,FREE"
    tmpFdiResult = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "ALL_TELEX_TYPES", tmpFdiResult)
    GetAllTelexTypes = tmpFdiResult
End Function
Private Sub InitTypeTextFilter()
    Dim tmpTypeList As String
    Dim tmpType As String
    Dim tmpCode As String
    Dim tmpFilter As String
    Dim itm As Integer
    IgnoreAllTlxText = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "ALL_TYPES_TEXT_FILTER", "")
    tmpTypeList = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "TEXT_FILTER_TYPES", "")
    TextFilterList = ""
    itm = 1
    tmpType = GetItem(tmpTypeList, itm, ",")
    While tmpType <> ""
        tmpCode = tmpType & "_TEXT_FILTER"
        tmpFilter = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", tmpCode, "")
        If tmpFilter <> "" Then
            TextFilterList = TextFilterList & "{=" & tmpType & "=}" & tmpFilter
        End If
        itm = itm + 1
        tmpType = GetItem(tmpTypeList, itm, ",")
    Wend

End Sub
Private Sub InitOnlineFilter()
    Dim UsedList As String
    Dim CodeList As String
    Dim TypeCode As String
    Dim tmpMainTypeList As String
    Dim i As Integer
    Dim itm As Integer
    Dim IsFound As Boolean
    If PoolConfig.OnLineCfg(4).Value = 1 Then
        InfoTypeList = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "FLIGHT_INFO_TYPES", "")
    End If
    MaxOnlineCount = Val(GetIniEntry(myIniFullName, "ONLINE_WINDOWS", "", "ONLINE_MAX_LINES", "500"))
    If MaxOnlineCount < 50 Then MaxOnlineCount = 50
    If MaxOnlineCount > 5000 Then MaxOnlineCount = 5000
    UsedList = AllTelexTypes
    If InfoTypeList <> "" Then UsedList = UsedList & "," & InfoTypeList
    tmpMainTypeList = ""
    itm = 1
    TypeCode = GetItem(FolderTypeList, itm, ",")
    While TypeCode <> ""
        If InStr(TypeCode, ":") > 0 Then TypeCode = GetRealItem(TypeCode, 1, ":")
        If InStr(UsedList, TypeCode) = 0 Then UsedList = UsedList & "," & TypeCode
        If InStr(tmpMainTypeList, TypeCode) = 0 Then tmpMainTypeList = tmpMainTypeList & "," & TypeCode
        itm = itm + 1
        TypeCode = GetItem(FolderTypeList, itm, ",")
    Wend
    UsedList = Replace(UsedList, ";", ",", 1, -1, vbBinaryCompare)
    tmpMainTypeList = Replace(tmpMainTypeList, ";", ",", 1, -1, vbBinaryCompare)
    tmpMainTypeList = Mid(tmpMainTypeList, 2)
    CodeList = GetIniEntry(myIniFullName, "ONLINE_WINDOWS", "", "RECV_TYPES", UsedList)
    If CodeList <> "" Then
        UsedList = ""
        itm = 0
        For i = 0 To 38
            chkRecv(i).Caption = ""
            chkRecv(i).Enabled = False
            chkRecv(i).Value = 0
            itm = itm + 1
            TypeCode = Trim(GetItem(CodeList, itm, ","))
            While (TypeCode <> "") And (InStr(UsedList, TypeCode) > 0)
                itm = itm + 1
                TypeCode = Trim(GetItem(CodeList, itm, ","))
            Wend
            If TypeCode <> "" Then
                chkRecv(i).Caption = TypeCode
                chkRecv(i).Enabled = True
                chkRecv(i).Value = 1
                UsedList = UsedList & TypeCode & ","
            End If
        Next
        chkRecv(39).Caption = "ELSE"
        chkRecv(39).Enabled = True
        chkRecv(39).Value = 1
    End If
    CodeList = GetIniEntry(myIniFullName, "ONLINE_WINDOWS", "", "ONLINE_RECV", "")
    If CodeList <> "" Then
        For i = 0 To 39
            chkRecv(i).Value = 0
        Next
        TypeCode = "START"
        itm = 0
        While TypeCode <> ""
            itm = itm + 1
            TypeCode = Trim(GetItem(CodeList, itm, ","))
            If TypeCode <> "" Then
                IsFound = False
                For i = 0 To 39
                    If chkRecv(i).Caption = TypeCode Then
                        chkRecv(i).Value = 1
                        IsFound = True
                        Exit For
                    End If
                Next
            End If
        Wend
    End If
    CodeList = GetIniEntry(myIniFullName, "ONLINE_WINDOWS", "", "ONLINE_NOSHOW", MainNoShow)
    CodeList = GetIniEntry(myIniFullName, "ONLINE_WINDOWS", "", "RECV_NOSHOW", CodeList)
    RecvNoShow = ""
    If CodeList <> "" Then
        If CodeList <> "ELSE" Then
            TypeCode = "START"
            itm = 0
            While TypeCode <> ""
                itm = itm + 1
                TypeCode = Trim(GetItem(CodeList, itm, ","))
                If TypeCode <> "" Then
                    IsFound = False
                    For i = 0 To 39
                        If chkRecv(i).Caption = TypeCode Then
                            chkRecv(i).Value = 0
                            IsFound = True
                            Exit For
                        End If
                    Next
                    If Not IsFound Then RecvNoShow = RecvNoShow & "'" & TypeCode & "',"
                End If
            Wend
        Else
            For i = 0 To 39
                chkRecv(i).Value = 0
            Next
            CodeList = tmpMainTypeList
            TypeCode = "START"
            itm = 0
            While TypeCode <> ""
                itm = itm + 1
                TypeCode = Trim(GetItem(CodeList, itm, ","))
                If TypeCode <> "" Then
                    IsFound = False
                    For i = 0 To 39
                        If chkRecv(i).Caption = TypeCode Then
                            chkRecv(i).Value = 1
                            IsFound = True
                            Exit For
                        End If
                    Next
                End If
            Wend
            For i = 0 To 39
                If chkRecv(i).Value = 0 Then RecvNoShow = RecvNoShow & "'" & TypeCode & "',"
            Next
        End If
    End If
    If RecvNoShow <> "" Then RecvNoShow = Left(RecvNoShow, Len(RecvNoShow) - 1)
    
    CodeList = GetIniEntry(myIniFullName, "ONLINE_WINDOWS", "", "SEND_TYPES", "")
    If CodeList <> "" Then
        itm = 0
        For i = 40 To 46
            chkRecv(i).Caption = ""
            chkRecv(i).Enabled = False
            chkRecv(i).Value = 0
            itm = itm + 1
            TypeCode = Trim(GetItem(CodeList, itm, ","))
            If TypeCode <> "" Then
                chkRecv(i).Caption = TypeCode
                chkRecv(i).Enabled = True
                chkRecv(i).Value = 1
            End If
        Next
        chkRecv(47).Caption = "ELSE"
        chkRecv(47).Enabled = True
        chkRecv(47).Value = 1
    End If
    CodeList = GetIniEntry(myIniFullName, "ONLINE_WINDOWS", "", "ONLINE_SEND", "")
    If CodeList <> "" Then
        For i = 40 To 47
            chkRecv(i).Value = 0
        Next
        TypeCode = "START"
        itm = 0
        While TypeCode <> ""
            itm = itm + 1
            TypeCode = Trim(GetItem(CodeList, itm, ","))
            If TypeCode <> "" Then
                For i = 40 To 47
                    If chkRecv(i).Caption = TypeCode Then
                        chkRecv(i).Value = 1
                        Exit For
                    End If
                Next
            End If
        Wend
    End If
End Sub
Public Sub ShowSetupGroup(GroupNo As Integer, GroupCaption As String)
    SetupGroup(0).Visible = False
    SetupGroup(1).Visible = False
    SetupGroup(GroupNo).Caption = GroupCaption
    SetupGroup(GroupNo).Top = 90
    SetupGroup(GroupNo).Left = 60
    SetupGroup(GroupNo).Visible = True
    SetupGroup(0).Tag = Str(GroupNo)
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Cancel = CheckCancelExit(Me, 3)
    End If
End Sub

Private Sub SndTabList_Click()
    If ApplicationIsStarted Then
        If SndTabList.Value = 1 Then
            TelexPoolHead.ArrangeBrowsers True
        Else
            TelexPoolHead.ArrangeBrowsers False
        End If
    End If
End Sub

Private Sub Timer1_Timer()
    Dim CurTime
    CurTime = CedaFullDateToVb(ServerTime.Tag)
    CurTime = DateAdd("n", 1, CurTime)
    ServerTime.Text = Format(CurTime, (DefDateFormat & " \/ hh:mm"))
    ServerTime.Tag = Format(CurTime, "yyyymmddhhmmss")
    TelexPoolHead.Caption = TelexPoolHead.Tag & " (" & ServerTime.Text & " " & ServerTimeType & ")"
    If Timer1.Interval < 60000 Then Timer1.Interval = 60000
End Sub

Public Function CheckOnlineFilter(Task As String, CheckCode As String) As String
    Dim FullCheck As Boolean
    Dim Result As String
    Dim tmpChkType As String
    Dim ActList As String
    Dim NotList As String
    Dim NoShowList As String
    Dim FromIdx As Integer
    Dim ToIdx As Integer
    Dim CurIdx As Integer
    Dim ElseIdx As Integer
    Dim LookOnly As Boolean
    Dim CheckInfoNoShow As Boolean
    Dim TypeIsValid As Boolean
    Dim FoundIdx As Integer
    FullCheck = True
    CheckInfoNoShow = False
    Result = ""
    ActList = ""
    NotList = ""
    NoShowList = ""
    tmpChkType = Trim(CheckCode)
    If tmpChkType = "" Then tmpChkType = "FREE"
    Select Case Task
        Case "LOOKUP_RCV", "RCV_FILTER"
            FromIdx = 0
            ToIdx = 38
            ElseIdx = 39
            If Task = "LOOKUP_RCV" Then LookOnly = True Else LookOnly = False
            NoShowList = RecvNoShow
            If PoolConfig.OnLineCfg(4).Value = 1 Then CheckInfoNoShow = True
        Case "LOOKUP_OUT", "OUT_FILTER"
            FromIdx = 40
            ToIdx = 46
            ElseIdx = 47
            If Task = "LOOKUP_OUT" Then LookOnly = True Else LookOnly = False
            NoShowList = ""
        Case "LOOKUP_INF", "INF_FILTER"
            If InfoTypeList <> "" Then
                ActList = "'" & InfoTypeList & "'"
                ActList = Replace(ActList, ",", "','", 1, -1, vbBinaryCompare)
                Result = "TTYP IN (" & ActList & ")"
            End If
            FullCheck = False
        Case Else
            FullCheck = False
    End Select
    If FullCheck Then
        FoundIdx = -1
        For CurIdx = FromIdx To ToIdx
            If chkRecv(CurIdx).Enabled = True Then
                If LookOnly Then
                    If chkRecv(CurIdx).Caption = tmpChkType Then
                        FoundIdx = CurIdx
                        Exit For
                    End If
                Else
                    If chkRecv(CurIdx).Value = 1 Then
                        TypeIsValid = True
                        If CheckInfoNoShow Then
                            If InStr(InfoTypeList, chkRecv(CurIdx).Caption) > 0 Then
                                TypeIsValid = False
                            End If
                        End If
                    Else
                        TypeIsValid = False
                    End If
                    If TypeIsValid Then
                        ActList = ActList & ",'" & chkRecv(CurIdx).Caption & "'"
                    Else
                        NotList = NotList & ",'" & chkRecv(CurIdx).Caption & "'"
                    End If
                End If
            End If
        Next
        If LookOnly Then
            If FoundIdx >= 0 Then
                If chkRecv(FoundIdx).Value = 1 Then Result = "OK" Else Result = "NOK"
            Else
                If chkRecv(ElseIdx).Value = 1 Then Result = "OK" Else Result = "NOK"
                If (tmpChkType <> "") And (NoShowList <> "") Then
                    If InStr(NoShowList, tmpChkType) > 0 Then Result = "NOK"
                End If
            End If
        Else
            ActList = Mid(ActList, 2)
            If NoShowList <> "" Then NotList = NotList & "," & NoShowList
            NotList = Mid(NotList, 2)
            If (NotList = "") And (chkRecv(ElseIdx).Value = 1) Then NotList = ActList
            If ActList <> "" Then Result = "TTYP IN (" & ActList & ")"
            If NotList <> "" Then
                If chkRecv(ElseIdx).Value = 1 Then
                    If ActList <> "" Then Result = Result & " OR "
                    Result = Result & "TTYP NOT IN (" & NotList & ")"
                    If ActList <> "" Then Result = "(" & Result & ")"
                End If
            End If
            Result = Replace(Result, "FREE", " ", 1, -1, vbBinaryCompare)
        End If
    End If
    CheckOnlineFilter = Result
End Function

Public Function GetUtcServerTime() As String
    Dim UseServerTime
    UseServerTime = CedaFullDateToVb(ServerTime.Tag)
    If ServerTimeType <> "UTC" Then UseServerTime = DateAdd("n", -UtcTimeDiff, UseServerTime)
    GetUtcServerTime = Format(UseServerTime, "yyyymmddhhmm")
End Function
