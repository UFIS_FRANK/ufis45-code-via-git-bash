VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form TelexPoolHead 
   Caption         =   "Telex Pool"
   ClientHeight    =   7905
   ClientLeft      =   1065
   ClientTop       =   900
   ClientWidth     =   12150
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   11.25
      Charset         =   161
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "TelexPoolHead.frx":0000
   KeyPreview      =   -1  'True
   LinkMode        =   1  'Source
   LinkTopic       =   "InfoServer"
   LockControls    =   -1  'True
   ScaleHeight     =   7905
   ScaleWidth      =   12150
   ShowInTaskbar   =   0   'False
   WindowState     =   1  'Minimized
   Begin VB.TextBox MsgFromClient 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   90
      Locked          =   -1  'True
      TabIndex        =   174
      Text            =   "MsgFromClient"
      Top             =   6870
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.TextBox MsgToClient 
      BackColor       =   &H0000FFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1380
      TabIndex        =   173
      Text            =   "MsgToClient"
      Top             =   6870
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.Frame fraLoadProgress 
      Caption         =   "Loading in Progress ..."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      Left            =   8940
      TabIndex        =   134
      Top             =   6060
      Visible         =   0   'False
      Width           =   2865
      Begin VB.OptionButton Option2 
         BackColor       =   &H0080FF80&
         Caption         =   "U"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2490
         Style           =   1  'Graphical
         TabIndex        =   142
         ToolTipText     =   "Times in UTC (ATH Local - 180 min.)"
         Top             =   300
         Value           =   -1  'True
         Width           =   300
      End
      Begin VB.OptionButton Option1 
         Caption         =   "L"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2490
         Style           =   1  'Graphical
         TabIndex        =   141
         ToolTipText     =   "Times in ATH local (UTC + 180 min.)"
         Top             =   630
         Width           =   300
      End
      Begin VB.TextBox txtLoadTimeTo 
         Alignment       =   2  'Center
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1800
         Locked          =   -1  'True
         MaxLength       =   5
         TabIndex        =   140
         ToolTipText     =   "Time hh:mm"
         Top             =   630
         Width           =   645
      End
      Begin VB.TextBox txtLoadTimeFrom 
         Alignment       =   2  'Center
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1800
         Locked          =   -1  'True
         MaxLength       =   5
         TabIndex        =   139
         ToolTipText     =   "Time hh:mm"
         Top             =   300
         Width           =   645
      End
      Begin VB.CheckBox Check2 
         Caption         =   "To"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   138
         ToolTipText     =   "Confirm Time Filter"
         Top             =   630
         Width           =   660
      End
      Begin VB.CheckBox Check1 
         Caption         =   "From"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   137
         ToolTipText     =   "Reset Time Filter"
         Top             =   300
         Width           =   660
      End
      Begin VB.TextBox txtLoadTo 
         Alignment       =   2  'Center
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   720
         Locked          =   -1  'True
         MaxLength       =   10
         TabIndex        =   136
         ToolTipText     =   "Day Date dd.mm.yyyy"
         Top             =   630
         Width           =   1065
      End
      Begin VB.TextBox txtLoadFrom 
         Alignment       =   2  'Center
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   720
         Locked          =   -1  'True
         MaxLength       =   10
         TabIndex        =   135
         ToolTipText     =   "Day Date dd.mm.yyyy"
         Top             =   300
         Width           =   1065
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "DataPool"
      Height          =   405
      Left            =   10080
      TabIndex        =   133
      Top             =   0
      Visible         =   0   'False
      Width           =   1365
   End
   Begin VB.Timer SpoolTimer 
      Enabled         =   0   'False
      Interval        =   5
      Left            =   6930
      Top             =   7110
   End
   Begin TABLib.TAB InfoSpooler 
      Height          =   555
      Left            =   2790
      TabIndex        =   128
      Top             =   6930
      Visible         =   0   'False
      Width           =   3945
      _Version        =   65536
      _ExtentX        =   6959
      _ExtentY        =   979
      _StockProps     =   64
   End
   Begin VB.TextBox InfoFromClient 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1380
      Locked          =   -1  'True
      TabIndex        =   127
      Text            =   "InfoFromClient"
      Top             =   7170
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.TextBox InfoToClient 
      BackColor       =   &H0000FFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   90
      TabIndex        =   126
      Text            =   "InfoToClient"
      Top             =   7170
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.Frame FolderButtons 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1140
      Index           =   2
      Left            =   9750
      TabIndex        =   122
      Top             =   2880
      Visible         =   0   'False
      Width           =   1170
      Begin VB.CheckBox chkTool 
         Caption         =   "Imp.Tool"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   75
         Style           =   1  'Graphical
         TabIndex        =   125
         Top             =   180
         Width           =   1020
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Spooler"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   75
         Style           =   1  'Graphical
         TabIndex        =   124
         Top             =   480
         Width           =   1020
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Transmit"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   75
         Style           =   1  'Graphical
         TabIndex        =   123
         Top             =   780
         Width           =   1020
      End
   End
   Begin VB.PictureBox RightCover 
      AutoRedraw      =   -1  'True
      Height          =   525
      Left            =   10920
      ScaleHeight     =   465
      ScaleWidth      =   1065
      TabIndex        =   111
      Top             =   90
      Width           =   1125
   End
   Begin VB.PictureBox Picture1 
      AutoSize        =   -1  'True
      Height          =   150
      Index           =   3
      Left            =   8730
      Picture         =   "TelexPoolHead.frx":030A
      ScaleHeight     =   90
      ScaleWidth      =   90
      TabIndex        =   100
      Top             =   6090
      Visible         =   0   'False
      Width           =   150
   End
   Begin VB.PictureBox Picture1 
      AutoSize        =   -1  'True
      Height          =   150
      Index           =   2
      Left            =   8730
      Picture         =   "TelexPoolHead.frx":035E
      ScaleHeight     =   90
      ScaleWidth      =   90
      TabIndex        =   99
      Top             =   5940
      Visible         =   0   'False
      Width           =   150
   End
   Begin VB.PictureBox Picture1 
      AutoSize        =   -1  'True
      Height          =   540
      Index           =   1
      Left            =   8190
      Picture         =   "TelexPoolHead.frx":03B2
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   98
      Top             =   5940
      Visible         =   0   'False
      Width           =   540
   End
   Begin VB.PictureBox Picture1 
      AutoSize        =   -1  'True
      Height          =   540
      Index           =   0
      Left            =   7650
      Picture         =   "TelexPoolHead.frx":06BC
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   97
      Top             =   5940
      Visible         =   0   'False
      Width           =   540
   End
   Begin VB.Frame fraListPanel 
      Height          =   5355
      Left            =   30
      TabIndex        =   77
      Top             =   1530
      Width           =   7575
      Begin VB.Frame fraToggleRcv 
         BorderStyle     =   0  'None
         Height          =   675
         Left            =   3000
         TabIndex        =   164
         Top             =   630
         Visible         =   0   'False
         Width           =   1665
         Begin VB.CheckBox chkShowTelex 
            Caption         =   "V"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   1320
            Style           =   1  'Graphical
            TabIndex        =   171
            ToolTipText     =   "Open Telex Viewer"
            Top             =   390
            Width           =   270
         End
         Begin VB.CheckBox chkSize 
            BackColor       =   &H80000002&
            Caption         =   " +"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Index           =   0
            Left            =   1320
            Style           =   1  'Graphical
            TabIndex        =   170
            Top             =   0
            Width           =   315
         End
         Begin VB.CheckBox chkTlxCnt 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   1
            Left            =   660
            Style           =   1  'Graphical
            TabIndex        =   168
            ToolTipText     =   "Sent Telex in Buffer"
            Top             =   390
            Width           =   645
         End
         Begin VB.CheckBox chkTlxCnt 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   167
            ToolTipText     =   "Rcvd Telex in Buffer"
            Top             =   390
            Width           =   645
         End
         Begin VB.CheckBox chkToggleRcv 
            Caption         =   "SENT"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   660
            Style           =   1  'Graphical
            TabIndex        =   166
            Top             =   0
            Width           =   645
         End
         Begin VB.CheckBox chkToggleRcv 
            Caption         =   "RCVD"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   165
            Top             =   0
            Width           =   645
         End
         Begin VB.TextBox txtTelexList 
            BackColor       =   &H8000000F&
            CausesValidation=   0   'False
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C0C0C0&
            Height          =   435
            Index           =   4
            Left            =   -150
            TabIndex        =   169
            TabStop         =   0   'False
            Top             =   345
            Width           =   1785
         End
      End
      Begin VB.TextBox txtTelexList 
         BackColor       =   &H0080C0FF&
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C0C0C0&
         Height          =   975
         Index           =   3
         Left            =   2490
         TabIndex        =   163
         TabStop         =   0   'False
         Top             =   570
         Visible         =   0   'False
         Width           =   345
      End
      Begin VB.TextBox txtTelexList 
         BackColor       =   &H0080C0FF&
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C0C0C0&
         Height          =   975
         Index           =   2
         Left            =   2100
         TabIndex        =   162
         TabStop         =   0   'False
         Top             =   570
         Visible         =   0   'False
         Width           =   345
      End
      Begin VB.Frame RcvButtonPanel 
         BorderStyle     =   0  'None
         Height          =   285
         Index           =   1
         Left            =   60
         TabIndex        =   159
         Top             =   1290
         Visible         =   0   'False
         Width           =   1815
         Begin VB.CheckBox Check3 
            Caption         =   "Refresh"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   1
            Left            =   45
            Style           =   1  'Graphical
            TabIndex        =   161
            Top             =   0
            Width           =   855
         End
         Begin VB.CheckBox chkLock 
            Caption         =   "Lock"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   1
            Left            =   915
            Style           =   1  'Graphical
            TabIndex        =   160
            Top             =   0
            Width           =   855
         End
         Begin VB.Line Line2 
            BorderColor     =   &H80000010&
            X1              =   0
            X2              =   0
            Y1              =   0
            Y2              =   300
         End
         Begin VB.Line Line1 
            X1              =   15
            X2              =   15
            Y1              =   0
            Y2              =   300
         End
      End
      Begin TABLib.TAB tabTelexList 
         Height          =   1275
         Index           =   3
         Left            =   3810
         TabIndex        =   158
         Top             =   1860
         Visible         =   0   'False
         Width           =   2850
         _Version        =   65536
         _ExtentX        =   5027
         _ExtentY        =   2249
         _StockProps     =   64
         Lines           =   10
         HeaderString    =   "Type,Time,Number,Text Extract"
         HeaderLengthString=   "30,30,30,30"
      End
      Begin VB.CheckBox chkAssignBar 
         BackColor       =   &H80000002&
         Caption         =   " +"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Index           =   1
         Left            =   7170
         Style           =   1  'Graphical
         TabIndex        =   157
         Top             =   210
         Visible         =   0   'False
         Width           =   300
      End
      Begin VB.CheckBox chkAssignBar 
         BackColor       =   &H80000002&
         Caption         =   "Assign Telex to Folder"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Index           =   0
         Left            =   4860
         Style           =   1  'Graphical
         TabIndex        =   156
         Top             =   210
         Visible         =   0   'False
         Width           =   2295
      End
      Begin TABLib.TAB tabAssign 
         Height          =   465
         Index           =   0
         Left            =   4860
         TabIndex        =   154
         Top             =   870
         Visible         =   0   'False
         Width           =   1920
         _Version        =   65536
         _ExtentX        =   3387
         _ExtentY        =   820
         _StockProps     =   64
         Lines           =   10
         HeaderString    =   "Type,Time,Number,Text Extract"
         HeaderLengthString=   "30,30,30,30"
      End
      Begin VB.Frame AssignButtonPanel 
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   4860
         TabIndex        =   151
         Top             =   570
         Visible         =   0   'False
         Width           =   1815
         Begin VB.CheckBox chkLoadAss 
            Caption         =   "Refresh"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   45
            Style           =   1  'Graphical
            TabIndex        =   153
            Top             =   0
            Width           =   855
         End
         Begin VB.CheckBox chkTlxAssign 
            Caption         =   "&Save"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   915
            Style           =   1  'Graphical
            TabIndex        =   152
            Top             =   0
            Width           =   855
         End
         Begin VB.Line Line6 
            BorderColor     =   &H00000000&
            X1              =   15
            X2              =   15
            Y1              =   0
            Y2              =   300
         End
         Begin VB.Line Line5 
            BorderColor     =   &H80000010&
            X1              =   0
            X2              =   0
            Y1              =   0
            Y2              =   300
         End
      End
      Begin VB.Frame RcvButtonPanel 
         BorderStyle     =   0  'None
         Height          =   285
         Index           =   0
         Left            =   60
         TabIndex        =   148
         Top             =   570
         Visible         =   0   'False
         Width           =   1845
         Begin VB.CheckBox chkLock 
            Caption         =   "Lock"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   915
            Style           =   1  'Graphical
            TabIndex        =   150
            Top             =   0
            Width           =   855
         End
         Begin VB.CheckBox Check3 
            Caption         =   "Refresh"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   45
            Style           =   1  'Graphical
            TabIndex        =   149
            Top             =   0
            Width           =   855
         End
         Begin VB.Line Line4 
            BorderColor     =   &H00000000&
            X1              =   15
            X2              =   15
            Y1              =   0
            Y2              =   300
         End
         Begin VB.Line Line3 
            BorderColor     =   &H80000010&
            X1              =   0
            X2              =   0
            Y1              =   0
            Y2              =   300
         End
      End
      Begin VB.CheckBox chkTlxTitleBar 
         BackColor       =   &H80000002&
         Caption         =   " +"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Index           =   3
         Left            =   1680
         Style           =   1  'Graphical
         TabIndex        =   147
         Top             =   930
         Visible         =   0   'False
         Width           =   300
      End
      Begin VB.CheckBox chkTlxTitleBar 
         BackColor       =   &H80000002&
         Caption         =   " +"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Index           =   2
         Left            =   4350
         Style           =   1  'Graphical
         TabIndex        =   146
         Top             =   210
         Width           =   300
      End
      Begin TABLib.TAB tabTelexList 
         Height          =   1275
         Index           =   2
         Left            =   510
         TabIndex        =   132
         Top             =   1980
         Visible         =   0   'False
         Width           =   2850
         _Version        =   65536
         _ExtentX        =   5027
         _ExtentY        =   2249
         _StockProps     =   64
         Lines           =   10
         HeaderString    =   "Type,Time,Number,Text Extract"
         HeaderLengthString=   "30,30,30,30"
      End
      Begin VB.Frame fraSplitter 
         Appearance      =   0  'Flat
         BackColor       =   &H000040C0&
         BorderStyle     =   0  'None
         ClipControls    =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C0C0C0&
         Height          =   135
         Index           =   4
         Left            =   3465
         MousePointer    =   15  'Size All
         TabIndex        =   93
         Tag             =   "-1"
         Top             =   3180
         Width           =   165
      End
      Begin VB.Frame fraSplitter 
         Appearance      =   0  'Flat
         BackColor       =   &H000080FF&
         BorderStyle     =   0  'None
         ClipControls    =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C0C0C0&
         Height          =   1725
         Index           =   3
         Left            =   3480
         MousePointer    =   9  'Size W E
         TabIndex        =   92
         Tag             =   "-1"
         Top             =   3330
         Width           =   120
      End
      Begin VB.TextBox txtTelexText 
         Height          =   945
         Left            =   240
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   84
         Top             =   4080
         Width           =   3165
      End
      Begin VB.CheckBox chkTlxTitleBar 
         BackColor       =   &H80000002&
         Caption         =   "Sent Telexes"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Index           =   1
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   79
         Top             =   930
         Value           =   1  'Checked
         Visible         =   0   'False
         Width           =   1605
      End
      Begin VB.CheckBox chkTlxTitleBar 
         BackColor       =   &H80000002&
         Caption         =   "Received Telexes"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Index           =   0
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   78
         Top             =   210
         Value           =   1  'Checked
         Width           =   4260
      End
      Begin TABLib.TAB tabTelexList 
         Height          =   1275
         Index           =   0
         Left            =   300
         TabIndex        =   81
         Top             =   1650
         Width           =   2850
         _Version        =   65536
         _ExtentX        =   5027
         _ExtentY        =   2249
         _StockProps     =   64
         Lines           =   10
         HeaderString    =   "Type,Time,Number,Text Extract"
         HeaderLengthString=   "30,30,30,30"
      End
      Begin VB.TextBox txtTelexList 
         BackColor       =   &H8000000F&
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C0C0C0&
         Height          =   2355
         Index           =   0
         Left            =   270
         TabIndex        =   80
         TabStop         =   0   'False
         Top             =   1620
         Width           =   3165
      End
      Begin TABLib.TAB tabTelexList 
         Height          =   1275
         Index           =   1
         Left            =   3570
         TabIndex        =   83
         Top             =   1650
         Width           =   2850
         _Version        =   65536
         _ExtentX        =   5027
         _ExtentY        =   2249
         _StockProps     =   64
         Lines           =   10
         HeaderString    =   "Type,Time,Number,Text Extract"
         HeaderLengthString=   "30,30,30,30"
      End
      Begin VB.TextBox txtTelexList 
         BackColor       =   &H8000000F&
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C0C0C0&
         Height          =   1455
         Index           =   1
         Left            =   3540
         TabIndex        =   82
         TabStop         =   0   'False
         Top             =   1620
         Width           =   3255
      End
      Begin VB.Frame fraSplitter 
         Appearance      =   0  'Flat
         BackColor       =   &H000000C0&
         BorderStyle     =   0  'None
         ClipControls    =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C0C0C0&
         Height          =   195
         Index           =   1
         Left            =   7260
         MousePointer    =   15  'Size All
         TabIndex        =   87
         Tag             =   "-1"
         Top             =   5070
         Width           =   225
      End
      Begin VB.Frame fraSplitter 
         Appearance      =   0  'Flat
         BackColor       =   &H000080FF&
         BorderStyle     =   0  'None
         ClipControls    =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C0C0C0&
         Height          =   2175
         Index           =   0
         Left            =   7320
         MousePointer    =   9  'Size W E
         TabIndex        =   86
         Tag             =   "-1"
         Top             =   2850
         Width           =   135
      End
      Begin VB.Frame fraSplitter 
         Appearance      =   0  'Flat
         BackColor       =   &H000080FF&
         BorderStyle     =   0  'None
         ClipControls    =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C0C0C0&
         Height          =   135
         Index           =   2
         Left            =   180
         MousePointer    =   7  'Size N S
         TabIndex        =   85
         Tag             =   "-1"
         Top             =   5100
         Width           =   7035
      End
      Begin VB.Frame fraFlightList 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1815
         Left            =   3660
         TabIndex        =   90
         Top             =   3060
         Visible         =   0   'False
         Width           =   3585
         Begin VB.CheckBox chkFlightList 
            Caption         =   "&Save"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   7
            Left            =   2220
            Style           =   1  'Graphical
            TabIndex        =   172
            Top             =   465
            Width           =   615
         End
         Begin VB.CheckBox chkFlightBar 
            BackColor       =   &H80000002&
            Caption         =   " +"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Index           =   1
            Left            =   3240
            Style           =   1  'Graphical
            TabIndex        =   145
            Top             =   135
            Width           =   300
         End
         Begin VB.CheckBox chkFlightList 
            Caption         =   "&Link"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   6
            Left            =   1590
            Style           =   1  'Graphical
            TabIndex        =   143
            Top             =   465
            Width           =   615
         End
         Begin VB.CheckBox chkFlightList 
            Caption         =   "Multi"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   5
            Left            =   2850
            Style           =   1  'Graphical
            TabIndex        =   130
            Top             =   465
            Width           =   615
         End
         Begin TABLib.TAB tabFlightList 
            Height          =   705
            Index           =   2
            Left            =   870
            TabIndex        =   108
            Top             =   1380
            Width           =   2190
            _Version        =   65536
            _ExtentX        =   3863
            _ExtentY        =   1244
            _StockProps     =   64
         End
         Begin TABLib.TAB tabFlightList 
            Height          =   705
            Index           =   1
            Left            =   270
            TabIndex        =   106
            Top             =   1020
            Width           =   2190
            _Version        =   65536
            _ExtentX        =   3863
            _ExtentY        =   1244
            _StockProps     =   64
         End
         Begin VB.CheckBox chkFlightList 
            Caption         =   "Count"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   4
            Left            =   1590
            Style           =   1  'Graphical
            TabIndex        =   107
            Top             =   135
            Visible         =   0   'False
            Width           =   675
         End
         Begin VB.CheckBox chkFlightList 
            Caption         =   "Scroll"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   3
            Left            =   2280
            Style           =   1  'Graphical
            TabIndex        =   105
            Top             =   135
            Visible         =   0   'False
            Width           =   615
         End
         Begin VB.CheckBox chkFlightList 
            Caption         =   "Dep"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   2
            Left            =   1110
            Style           =   1  'Graphical
            TabIndex        =   104
            Top             =   465
            Width           =   465
         End
         Begin VB.CheckBox chkFlightList 
            Caption         =   "Arr"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   1
            Left            =   630
            Style           =   1  'Graphical
            TabIndex        =   103
            Top             =   465
            Width           =   465
         End
         Begin VB.CheckBox chkFlightList 
            Caption         =   "Load"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   30
            Style           =   1  'Graphical
            TabIndex        =   102
            Top             =   465
            Width           =   585
         End
         Begin VB.CheckBox chkFlightBar 
            BackColor       =   &H80000002&
            Caption         =   "0 Flights"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Index           =   0
            Left            =   30
            Style           =   1  'Graphical
            TabIndex        =   101
            Top             =   135
            Width           =   1125
         End
         Begin TABLib.TAB tabFlightList 
            Height          =   765
            Index           =   0
            Left            =   30
            TabIndex        =   91
            Top             =   765
            Width           =   2010
            _Version        =   65536
            _ExtentX        =   3545
            _ExtentY        =   1349
            _StockProps     =   64
         End
      End
      Begin VB.Line panelLine 
         BorderColor     =   &H00FFFFFF&
         Index           =   1
         X1              =   0
         X2              =   1125
         Y1              =   30
         Y2              =   30
      End
      Begin VB.Line panelLine 
         BorderColor     =   &H80000010&
         Index           =   0
         X1              =   0
         X2              =   1125
         Y1              =   0
         Y2              =   0
      End
   End
   Begin VB.Frame fraFolderToggles 
      Height          =   915
      Left            =   8520
      TabIndex        =   69
      Top             =   1740
      Width           =   1170
      Begin VB.CheckBox chkScroll 
         Caption         =   "Hidden"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   75
         Top             =   570
         Width           =   795
      End
      Begin VB.HScrollBar FolderScroll 
         Height          =   285
         Left            =   90
         TabIndex        =   72
         Top             =   210
         Width           =   795
      End
      Begin VB.CheckBox chkToggle 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   915
         Style           =   1  'Graphical
         TabIndex        =   71
         ToolTipText     =   "Filter Multi Select"
         Top             =   570
         Value           =   2  'Grayed
         Width           =   180
      End
      Begin VB.CheckBox chkToggle 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   915
         Style           =   1  'Graphical
         TabIndex        =   70
         ToolTipText     =   "Folder Multi Select"
         Top             =   210
         Width           =   180
      End
      Begin VB.Line BorderLine 
         BorderColor     =   &H00FFFFFF&
         Index           =   22
         X1              =   0
         X2              =   1125
         Y1              =   525
         Y2              =   525
      End
      Begin VB.Line BorderLine 
         BorderColor     =   &H80000014&
         Index           =   21
         X1              =   0
         X2              =   390
         Y1              =   135
         Y2              =   135
      End
      Begin VB.Line BorderLine 
         BorderColor     =   &H8000000F&
         BorderWidth     =   2
         Index           =   20
         X1              =   15
         X2              =   15
         Y1              =   150
         Y2              =   870
      End
      Begin VB.Line BorderLine 
         BorderColor     =   &H80000010&
         Index           =   24
         X1              =   0
         X2              =   0
         Y1              =   120
         Y2              =   885
      End
   End
   Begin VB.Frame FolderButtons 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2055
      Index           =   1
      Left            =   10950
      TabIndex        =   58
      Top             =   690
      Visible         =   0   'False
      Width           =   1170
      Begin VB.CheckBox AtcTransmit 
         Caption         =   "Transmit"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   75
         Style           =   1  'Graphical
         TabIndex        =   63
         Top             =   480
         Width           =   1020
      End
      Begin VB.TextBox UseCSGN 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   75
         TabIndex        =   62
         ToolTipText     =   "Callsign used by UFIS"
         Top             =   1005
         Width           =   1020
      End
      Begin VB.TextBox AtcCSGN 
         Alignment       =   2  'Center
         BackColor       =   &H0000FFFF&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   75
         Locked          =   -1  'True
         TabIndex        =   61
         ToolTipText     =   "Callsign used by ATC"
         Top             =   1335
         Width           =   1020
      End
      Begin VB.TextBox CsgnStatus 
         Alignment       =   2  'Center
         BackColor       =   &H00008000&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   75
         TabIndex        =   60
         Text            =   "UPDATE"
         Top             =   1665
         Width           =   1020
      End
      Begin VB.CheckBox AtcPreview 
         Caption         =   "Preview"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   75
         Style           =   1  'Graphical
         TabIndex        =   59
         Top             =   180
         Width           =   1020
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "CallSign"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   165
         TabIndex        =   64
         Top             =   795
         Width           =   750
      End
   End
   Begin VB.Frame fraStatusFilter 
      Caption         =   "IF Filter"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      Left            =   8880
      TabIndex        =   46
      Top             =   420
      Width           =   1035
      Begin VB.CheckBox chkFilter 
         Caption         =   "SD"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   510
         Style           =   1  'Graphical
         TabIndex        =   49
         ToolTipText     =   "Load Sent Telexes"
         Top             =   630
         Width           =   435
      End
      Begin VB.CheckBox chkFilter 
         Caption         =   "RC"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   48
         ToolTipText     =   "Load Received Telexes"
         Top             =   630
         Width           =   435
      End
      Begin VB.CheckBox chkFilter 
         Caption         =   "Status"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   47
         ToolTipText     =   "Select a Status Filter"
         Top             =   300
         Width           =   855
      End
   End
   Begin VB.Frame CedaBusyFlag 
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1275
      Left            =   7650
      TabIndex        =   17
      Top             =   4590
      Width           =   1185
      Begin VB.TextBox CedaStatus 
         Alignment       =   2  'Center
         BackColor       =   &H000000FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Index           =   3
         Left            =   0
         TabIndex        =   30
         Text            =   "NO LINK"
         Top             =   900
         Visible         =   0   'False
         Width           =   1125
      End
      Begin VB.TextBox CedaStatus 
         Alignment       =   2  'Center
         BackColor       =   &H000000FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Index           =   2
         Left            =   0
         TabIndex        =   20
         Text            =   "LOG OFF"
         Top             =   600
         Visible         =   0   'False
         Width           =   1125
      End
      Begin VB.TextBox CedaStatus 
         Alignment       =   2  'Center
         BackColor       =   &H00004000&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Index           =   0
         Left            =   0
         TabIndex        =   18
         Text            =   "LINE BUSY"
         Top             =   0
         Visible         =   0   'False
         Width           =   1125
      End
      Begin VB.TextBox CedaStatus 
         Alignment       =   2  'Center
         BackColor       =   &H00008000&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Index           =   1
         Left            =   0
         TabIndex        =   19
         Text            =   "LOG ON"
         Top             =   300
         Visible         =   0   'False
         Width           =   1125
      End
   End
   Begin MSComctlLib.StatusBar MyStatusBar 
      Align           =   2  'Align Bottom
      Height          =   345
      Left            =   0
      TabIndex        =   14
      Top             =   7560
      Width           =   12150
      _ExtentX        =   21431
      _ExtentY        =   609
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   4
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   5292
            MinWidth        =   5292
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   7408
            MinWidth        =   7408
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   6182
            MinWidth        =   176
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1940
            MinWidth        =   1940
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame TopButtons 
      BackColor       =   &H00404040&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1230
      Left            =   9960
      TabIndex        =   26
      Top             =   390
      Width           =   900
      Begin VB.CheckBox FixWin 
         Caption         =   "Arrange"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   31
         Top             =   630
         Width           =   855
      End
      Begin VB.CheckBox chkClose 
         Caption         =   "&Close"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   29
         Top             =   930
         Width           =   855
      End
      Begin VB.CheckBox OnTop 
         Caption         =   "On Top"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   28
         Top             =   30
         Width           =   855
      End
      Begin VB.CheckBox OnLine 
         Caption         =   "Online"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   27
         Top             =   330
         Width           =   855
      End
   End
   Begin VB.Frame basicButtons 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2385
      Left            =   8880
      TabIndex        =   15
      Top             =   3090
      Width           =   855
      Begin VB.CheckBox chkRedirect 
         Caption         =   "&Redirect"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   129
         Top             =   300
         Width           =   855
      End
      Begin VB.CheckBox chkStatus 
         Caption         =   "Status"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   39
         Top             =   1800
         Width           =   855
      End
      Begin VB.CheckBox chkPrintTxt 
         Caption         =   "&Print"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   25
         Top             =   600
         Width           =   855
      End
      Begin VB.CheckBox chkAssign 
         Caption         =   "Assign"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   24
         Top             =   1200
         Width           =   855
      End
      Begin VB.CheckBox chkFlight 
         Caption         =   "Flight"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   23
         Top             =   900
         Width           =   855
      End
      Begin VB.CheckBox chkReject 
         Caption         =   "Reject"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   22
         Top             =   1500
         Width           =   855
      End
      Begin VB.CheckBox chkNew 
         Caption         =   "New"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox FullText 
         Caption         =   "Fulltext"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   2100
         Width           =   855
      End
   End
   Begin VB.Frame MainButtons 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   30
      TabIndex        =   34
      Top             =   -30
      Width           =   10050
      Begin VB.CheckBox chkConfig 
         Caption         =   "Config"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6090
         Style           =   1  'Graphical
         TabIndex        =   155
         Top             =   60
         Width           =   855
      End
      Begin VB.CheckBox chkStop 
         Caption         =   "Stop"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   870
         MousePointer    =   1  'Arrow
         Style           =   1  'Graphical
         TabIndex        =   144
         Top             =   210
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CheckBox chkOnlineRcv 
         Caption         =   "Online"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   870
         Style           =   1  'Graphical
         TabIndex        =   131
         Top             =   60
         Width           =   855
      End
      Begin VB.CheckBox chkAssFolder 
         Caption         =   "Folder"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1740
         Style           =   1  'Graphical
         TabIndex        =   96
         Top             =   60
         Width           =   855
      End
      Begin VB.CheckBox chkExit 
         Caption         =   "Exit"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8700
         MousePointer    =   1  'Arrow
         Style           =   1  'Graphical
         TabIndex        =   88
         Top             =   60
         Width           =   855
      End
      Begin VB.CheckBox chkHelp 
         Caption         =   "Help"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7830
         Style           =   1  'Graphical
         TabIndex        =   50
         Top             =   60
         Width           =   855
      End
      Begin VB.CheckBox chkReset 
         Caption         =   "Reset"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4350
         Style           =   1  'Graphical
         TabIndex        =   45
         Top             =   60
         Width           =   855
      End
      Begin VB.Frame optMemButtons 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2610
         TabIndex        =   40
         Tag             =   "3"
         Top             =   60
         Width           =   1725
         Begin VB.OptionButton optMem 
            Caption         =   "MF"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   1
            Left            =   420
            Style           =   1  'Graphical
            TabIndex        =   43
            Top             =   0
            Width           =   435
         End
         Begin VB.OptionButton optMem 
            BackColor       =   &H0080FF80&
            Caption         =   "MS"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   3
            Left            =   1290
            Style           =   1  'Graphical
            TabIndex        =   41
            Top             =   0
            Value           =   -1  'True
            Width           =   435
         End
         Begin VB.OptionButton optMem 
            Caption         =   "MU"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   44
            Top             =   0
            Width           =   435
         End
         Begin VB.OptionButton optMem 
            Caption         =   "MR"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   2
            Left            =   870
            Style           =   1  'Graphical
            TabIndex        =   42
            Top             =   0
            Width           =   435
         End
      End
      Begin VB.CheckBox chkLoad 
         Caption         =   "Load"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   38
         Top             =   60
         Width           =   855
      End
      Begin VB.CheckBox chkPrint 
         Caption         =   "Print"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5220
         Style           =   1  'Graphical
         TabIndex        =   37
         Top             =   270
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CheckBox chkSetup 
         Caption         =   "Setup"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5220
         Style           =   1  'Graphical
         TabIndex        =   36
         Top             =   60
         Width           =   855
      End
      Begin VB.CheckBox chkAbout 
         Caption         =   "About"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6960
         Style           =   1  'Graphical
         TabIndex        =   35
         Top             =   60
         Width           =   855
      End
   End
   Begin VB.Frame FolderButtons 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1140
      Index           =   0
      Left            =   9750
      TabIndex        =   13
      Top             =   1710
      Visible         =   0   'False
      Width           =   1170
      Begin VB.CheckBox LoaInfo 
         Caption         =   "ULD Info"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   75
         Style           =   1  'Graphical
         TabIndex        =   57
         Top             =   780
         Width           =   1020
      End
      Begin VB.CheckBox LoaInfo 
         Caption         =   "Loading"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   75
         Style           =   1  'Graphical
         TabIndex        =   56
         Top             =   480
         Width           =   1020
      End
      Begin VB.CheckBox LoaInfo 
         Caption         =   "PAX Info"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   75
         Style           =   1  'Graphical
         TabIndex        =   55
         Top             =   180
         Width           =   1020
      End
   End
   Begin VB.Frame fraTextFilter 
      Caption         =   "Telex Text Filter"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      Left            =   5910
      TabIndex        =   9
      Top             =   420
      Width           =   2925
      Begin VB.CheckBox chkTextFilt 
         Caption         =   "Text"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   117
         Top             =   630
         Visible         =   0   'False
         Width           =   600
      End
      Begin VB.CheckBox chkAddrFilt 
         Caption         =   "Addr."
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   116
         Top             =   300
         Visible         =   0   'False
         Width           =   600
      End
      Begin VB.OptionButton optTlxText 
         Caption         =   "T"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2550
         Style           =   1  'Graphical
         TabIndex        =   113
         ToolTipText     =   "Address Filter: Look in whole Telex Text"
         Top             =   630
         Width           =   300
      End
      Begin VB.OptionButton optTlxAddr 
         BackColor       =   &H0080FF80&
         Caption         =   "A"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2550
         Style           =   1  'Graphical
         TabIndex        =   112
         ToolTipText     =   "Address Filter: Look in Telex Header"
         Top             =   300
         Value           =   -1  'True
         Width           =   300
      End
      Begin VB.TextBox txtAddr 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   660
         TabIndex        =   12
         ToolTipText     =   "In the Telex Header"
         Top             =   300
         Width           =   1845
      End
      Begin VB.TextBox txtText 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   660
         TabIndex        =   10
         ToolTipText     =   "Somewhere in the Telex Data"
         Top             =   630
         Width           =   1845
      End
      Begin VB.Label lblFilter 
         Caption         =   "Text"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   3
         Left            =   150
         TabIndex        =   121
         Top             =   690
         Width           =   465
      End
      Begin VB.Label lblFilter 
         Caption         =   "Addr."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   2
         Left            =   150
         TabIndex        =   120
         Top             =   360
         Width           =   495
      End
   End
   Begin VB.Frame fraFlightFilter 
      Caption         =   "Flight Filter"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      Left            =   2940
      TabIndex        =   5
      Top             =   420
      Width           =   2925
      Begin VB.CheckBox chkRegList 
         Caption         =   "Regist."
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   115
         Top             =   630
         Visible         =   0   'False
         Width           =   780
      End
      Begin VB.CheckBox chkFltList 
         Caption         =   "Flight"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   114
         Top             =   300
         Visible         =   0   'False
         Width           =   780
      End
      Begin VB.OptionButton optLookTlx 
         BackColor       =   &H0080FF80&
         Caption         =   "T"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2550
         Style           =   1  'Graphical
         TabIndex        =   110
         ToolTipText     =   "Look in Telex Table"
         Top             =   300
         Value           =   -1  'True
         Width           =   300
      End
      Begin VB.OptionButton optLookAft 
         Caption         =   "F"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2550
         Style           =   1  'Graphical
         TabIndex        =   109
         ToolTipText     =   "Look in Flight Table"
         Top             =   630
         Width           =   300
      End
      Begin VB.TextBox txtRegn 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   840
         TabIndex        =   11
         ToolTipText     =   "Reg.No. with Dash (-)"
         Top             =   630
         Width           =   1665
      End
      Begin VB.TextBox txtFlns 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2160
         MaxLength       =   1
         TabIndex        =   8
         ToolTipText     =   "Flight Suffix"
         Top             =   300
         Width           =   345
      End
      Begin VB.TextBox txtFltn 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1590
         MaxLength       =   5
         TabIndex        =   7
         ToolTipText     =   "Flight Number"
         Top             =   300
         Width           =   555
      End
      Begin VB.TextBox txtFlca 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   840
         MaxLength       =   3
         TabIndex        =   6
         ToolTipText     =   "Flight Carrier (2LC or 3LC)"
         Top             =   300
         Width           =   735
      End
      Begin VB.Label lblFilter 
         Caption         =   "Regist."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   150
         TabIndex        =   119
         Top             =   690
         Width           =   645
      End
      Begin VB.Label lblFilter 
         Caption         =   "Flight"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   150
         TabIndex        =   118
         Top             =   360
         Width           =   645
      End
   End
   Begin VB.Frame fraTimeFilter 
      Caption         =   "Date/Time Filter"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      Left            =   30
      TabIndex        =   0
      Top             =   420
      Width           =   2865
      Begin VB.OptionButton optLocal 
         Caption         =   "L"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2490
         Style           =   1  'Graphical
         TabIndex        =   95
         ToolTipText     =   "Times in ATH local (UTC + 180 min.)"
         Top             =   630
         Width           =   300
      End
      Begin VB.OptionButton optUtc 
         BackColor       =   &H0080FF80&
         Caption         =   "U"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2490
         Style           =   1  'Graphical
         TabIndex        =   94
         ToolTipText     =   "Times in UTC (ATH Local - 180 min.)"
         Top             =   300
         Value           =   -1  'True
         Width           =   300
      End
      Begin VB.CheckBox ResetFilter 
         Caption         =   "From"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   89
         ToolTipText     =   "Reset Time Filter"
         Top             =   300
         Width           =   660
      End
      Begin VB.CheckBox chkFromTo 
         Caption         =   "To"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   76
         ToolTipText     =   "Confirm Time Filter"
         Top             =   630
         Width           =   660
      End
      Begin VB.TextBox txtDateFrom 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   720
         MaxLength       =   10
         TabIndex        =   4
         ToolTipText     =   "Day Date dd.mm.yyyy"
         Top             =   300
         Width           =   1065
      End
      Begin VB.TextBox txtTimeFrom 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1800
         MaxLength       =   5
         TabIndex        =   3
         ToolTipText     =   "Time hh:mm"
         Top             =   300
         Width           =   645
      End
      Begin VB.TextBox txtTimeTo 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1800
         MaxLength       =   5
         TabIndex        =   2
         ToolTipText     =   "Time hh:mm"
         Top             =   630
         Width           =   645
      End
      Begin VB.TextBox txtDateTo 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   720
         MaxLength       =   10
         TabIndex        =   1
         ToolTipText     =   "Day Date dd.mm.yyyy"
         Top             =   630
         Width           =   1065
      End
   End
   Begin VB.TextBox CurrentUrno 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   9030
      TabIndex        =   32
      Text            =   "CurrentUrno"
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.TextBox CurrentRecord 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   10290
      TabIndex        =   33
      Text            =   "CurrentRecord"
      Top             =   60
      Visible         =   0   'False
      Width           =   1905
   End
   Begin VB.Frame UfisFolder 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1245
      Left            =   7650
      TabIndex        =   51
      Top             =   1710
      Width           =   825
      Begin VB.Frame fraFolderLine 
         BackColor       =   &H80000010&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   15
         Index           =   2
         Left            =   0
         TabIndex        =   74
         Top             =   810
         Width           =   1965
      End
      Begin VB.Frame fraFolderLine 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   15
         Index           =   1
         Left            =   0
         TabIndex        =   73
         Top             =   870
         Width           =   1965
      End
      Begin VB.Frame fraFolderLine 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   15
         Index           =   0
         Left            =   0
         TabIndex        =   54
         Top             =   210
         Visible         =   0   'False
         Width           =   1965
      End
      Begin VB.Frame fraFolder 
         BackColor       =   &H00404040&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Index           =   0
         Left            =   60
         TabIndex        =   52
         Top             =   240
         Visible         =   0   'False
         Width           =   705
         Begin VB.Frame fraLine 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   161
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   15
            Index           =   0
            Left            =   0
            TabIndex        =   67
            Top             =   420
            Visible         =   0   'False
            Width           =   645
         End
         Begin VB.CheckBox chkTabs 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   66
            ToolTipText     =   "Telex Type Load Filter"
            Top             =   600
            Visible         =   0   'False
            Width           =   645
         End
         Begin VB.CheckBox chkFolder 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   65
            ToolTipText     =   "Telex Type Folder"
            Top             =   60
            Visible         =   0   'False
            Width           =   645
         End
         Begin VB.Frame fraBlend 
            BackColor       =   &H000000FF&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   161
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   60
            Index           =   0
            Left            =   0
            TabIndex        =   68
            Top             =   480
            Visible         =   0   'False
            Width           =   645
         End
      End
   End
   Begin TABLib.TAB HelperTab 
      Height          =   1545
      Left            =   7650
      TabIndex        =   53
      Top             =   3000
      Visible         =   0   'False
      Width           =   2025
      _Version        =   65536
      _ExtentX        =   3572
      _ExtentY        =   2725
      _StockProps     =   64
   End
End
Attribute VB_Name = "TelexPoolHead"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim FdiTimeFields As String
Private Type AdminMemory
    Folder As String
    AllTab As String
    ElsTab As String
    TabTag As String    'Tags of TypeFolderTabs
    TabCap As String    'Captions of TypeFolderTabs
    TabVal As String    'Values of TypeFolderTabs
    ChkTag As String    'Tags of TypeFilterButtons
    ChkCap As String    'Captions of TypeFilterButtons
    ChkVal As String    'Values of TypeFilterButtons
    FldVal As String    'FieldValues of the FilterDialog Used in The SqlKey
    TlxKey As String    'WhereClause for TLXTAB built
    TfrKey As String    'New WhereClause TFRTAB
    ExtKey As String    'Additional WhereClause from External (not yet used)
    LookTlx As Boolean
    LookAft As Boolean
    TlxAddr As Boolean
    TlxText As Boolean
End Type
Private ActFilterValues(0 To 8) As AdminMemory
Private NewFilterValues(0 To 8) As AdminMemory
Dim ShowTabButtons As Boolean
Dim ChkTabsIsBusy As Boolean
Dim ReorgFolders As Boolean
Dim MeIsVisible As Boolean
Dim CurButtonPanel As Integer
Dim RcvLoadCapt As String
Dim RcvOnlineCapt As String
Dim SndOnlineCapt As String
Dim SndLoadCapt As String
Dim StopLoading As Boolean
Dim DontRefreshList As Boolean
Dim AddFolderList As String
Dim EditMode As Boolean
Dim InsertTabAssign As Boolean
Dim SaveDateFrom As String
Dim SaveDateTo As String
Dim bmFolderButtons0 As Boolean
Dim LastTlxUrno(0 To 3) As String
Dim bmSortDirection As Boolean

Private Sub StoreFolderValues(MemIdx As Integer)
    Dim i As Integer
    Dim tmpData As String
    NewFilterValues(MemIdx).TabCap = ""
    NewFilterValues(MemIdx).TabTag = ""
    NewFilterValues(MemIdx).TabVal = ""
    NewFilterValues(MemIdx).ChkCap = ""
    NewFilterValues(MemIdx).ChkTag = ""
    NewFilterValues(MemIdx).ChkVal = ""
    For i = 0 To LastFolderIndex
        NewFilterValues(MemIdx).TabCap = NewFilterValues(MemIdx).TabCap & chkFolder(i).Caption & vbTab
        NewFilterValues(MemIdx).TabTag = NewFilterValues(MemIdx).TabTag & chkFolder(i).Tag & vbTab
        NewFilterValues(MemIdx).TabVal = NewFilterValues(MemIdx).TabVal & CStr(chkFolder(i).Value) & vbTab
        NewFilterValues(MemIdx).ChkCap = NewFilterValues(MemIdx).ChkCap & chkTabs(i).Caption & vbTab
        NewFilterValues(MemIdx).ChkTag = NewFilterValues(MemIdx).ChkTag & chkTabs(i).Tag & vbTab
        NewFilterValues(MemIdx).ChkVal = NewFilterValues(MemIdx).ChkVal & CStr(chkTabs(i).Value) & vbTab
    Next
    
    NewFilterValues(MemIdx).AllTab = GetFolderConfig(CInt(AllTypesCol))
    NewFilterValues(MemIdx).ElsTab = GetFolderConfig(CInt(OtherTypesCol))
    
    NewFilterValues(MemIdx).FldVal = ""
    NewFilterValues(MemIdx).FldVal = NewFilterValues(MemIdx).FldVal & txtDateFrom & vbTab
    NewFilterValues(MemIdx).FldVal = NewFilterValues(MemIdx).FldVal & txtTimeFrom & vbTab
    NewFilterValues(MemIdx).FldVal = NewFilterValues(MemIdx).FldVal & txtDateTo & vbTab
    NewFilterValues(MemIdx).FldVal = NewFilterValues(MemIdx).FldVal & txtTimeTo & vbTab
    NewFilterValues(MemIdx).FldVal = NewFilterValues(MemIdx).FldVal & txtFlca & vbTab
    NewFilterValues(MemIdx).FldVal = NewFilterValues(MemIdx).FldVal & txtFltn & vbTab
    NewFilterValues(MemIdx).FldVal = NewFilterValues(MemIdx).FldVal & txtFlns & vbTab
    NewFilterValues(MemIdx).FldVal = NewFilterValues(MemIdx).FldVal & txtRegn & vbTab
    NewFilterValues(MemIdx).FldVal = NewFilterValues(MemIdx).FldVal & txtAddr & vbTab
    NewFilterValues(MemIdx).FldVal = NewFilterValues(MemIdx).FldVal & txtText & vbTab
    NewFilterValues(MemIdx).LookTlx = optLookTlx.Value
    NewFilterValues(MemIdx).LookAft = optLookAft.Value
    NewFilterValues(MemIdx).TlxAddr = optTlxAddr.Value
    NewFilterValues(MemIdx).TlxText = optTlxText.Value
End Sub

Private Function GetFolderConfig(Index As Integer)
    Dim tmpData As String
    tmpData = ""
    tmpData = tmpData & chkFolder(Index).Caption & vbTab
    tmpData = tmpData & chkFolder(Index).Tag & vbTab
    tmpData = tmpData & CStr(chkFolder(Index).Value) & vbTab
    tmpData = tmpData & chkTabs(Index).Caption & vbTab
    tmpData = tmpData & chkTabs(Index).Tag & vbTab
    tmpData = tmpData & CStr(chkTabs(Index).Value) & vbTab
    GetFolderConfig = tmpData
End Function

Private Sub RestoreFolderValues(MemIdx As Integer, DoRefresh As Boolean)
    Dim i As Integer
    Dim itm As Integer
    Dim tmpData As String
    optLookTlx.Value = NewFilterValues(MemIdx).LookTlx
    optLookAft.Value = NewFilterValues(MemIdx).LookAft
    optTlxAddr.Value = NewFilterValues(MemIdx).TlxAddr
    optTlxText.Value = NewFilterValues(MemIdx).TlxText
    ReorgFolders = True
    For i = 0 To LastFolderIndex
        itm = i + 1
        If GetItem(NewFilterValues(MemIdx).TabCap, itm, vbTab) <> "" Then
            chkFolder(i).Tag = GetItem(NewFilterValues(MemIdx).TabTag, itm, vbTab)
            chkFolder(i).Caption = GetItem(NewFilterValues(MemIdx).TabCap, itm, vbTab)
            chkTabs(i).Tag = GetItem(NewFilterValues(MemIdx).ChkTag, itm, vbTab)
            chkTabs(i).Caption = GetItem(NewFilterValues(MemIdx).ChkCap, itm, vbTab)
            chkTabs(i).Value = Val(GetItem(NewFilterValues(MemIdx).ChkVal, itm, vbTab))
        End If
        ReorgFolderStatus i
    Next
    
    SetFolderConfig CInt(AllTypesCol), NewFilterValues(MemIdx).AllTab
    SetFolderConfig CInt(OtherTypesCol), NewFilterValues(MemIdx).ElsTab
    
    txtDateFrom.Text = GetItem(NewFilterValues(MemIdx).FldVal, 1, vbTab)
    txtTimeFrom.Text = GetItem(NewFilterValues(MemIdx).FldVal, 2, vbTab)
    txtDateTo.Text = GetItem(NewFilterValues(MemIdx).FldVal, 3, vbTab)
    txtTimeTo.Text = GetItem(NewFilterValues(MemIdx).FldVal, 4, vbTab)
    txtFlca.Text = GetItem(NewFilterValues(MemIdx).FldVal, 5, vbTab)
    txtFltn.Text = GetItem(NewFilterValues(MemIdx).FldVal, 6, vbTab)
    txtFlns.Text = GetItem(NewFilterValues(MemIdx).FldVal, 7, vbTab)
    txtRegn.Text = GetItem(NewFilterValues(MemIdx).FldVal, 8, vbTab)
    txtAddr.Text = GetItem(NewFilterValues(MemIdx).FldVal, 9, vbTab)
    txtText.Text = GetItem(NewFilterValues(MemIdx).FldVal, 10, vbTab)
    ReorgFolders = False
    If Not DontRefreshList Then
        If DoRefresh Then LoadFolderData "", ""
        If fraFlightList.Visible Then chkFlightList(4).Value = 1
    End If
End Sub

Private Sub SetFolderConfig(Index As Integer, CfgList As String)
    chkFolder(Index).Caption = GetItem(CfgList, 1, vbTab)
    chkFolder(Index).Tag = GetItem(CfgList, 2, vbTab)
    chkTabs(Index).Caption = GetItem(CfgList, 4, vbTab)
    chkTabs(Index).Tag = GetItem(CfgList, 5, vbTab)
    chkTabs(Index).Value = Val(GetItem(CfgList, 6, vbTab))
    ReorgFolderStatus Index
End Sub
Private Sub ResetFolderValues(MemIdx As Integer)
    Dim i As Integer
    Dim itm As Integer
    ReorgFolders = True
    optLookTlx.Value = NewFilterValues(MemIdx).LookTlx
    optLookAft.Value = NewFilterValues(MemIdx).LookAft
    optTlxAddr.Value = NewFilterValues(MemIdx).TlxAddr
    optTlxText.Value = NewFilterValues(MemIdx).TlxText
    For i = 0 To LastFolderIndex
        itm = i + 1
        chkFolder(i).Tag = GetItem(ActFilterValues(MemIdx).TabTag, itm, vbTab)
        chkFolder(i).Caption = GetItem(ActFilterValues(MemIdx).TabCap, itm, vbTab)
        chkTabs(i).Tag = GetItem(ActFilterValues(MemIdx).ChkTag, itm, vbTab)
        chkTabs(i).Caption = GetItem(ActFilterValues(MemIdx).ChkCap, itm, vbTab)
        chkTabs(i).Value = Val(GetItem(ActFilterValues(MemIdx).ChkVal, itm, vbTab))
        ReorgFolderStatus i
    Next
    txtDateFrom.Text = GetItem(ActFilterValues(MemIdx).FldVal, 1, vbTab)
    txtTimeFrom.Text = GetItem(ActFilterValues(MemIdx).FldVal, 2, vbTab)
    txtDateTo.Text = GetItem(ActFilterValues(MemIdx).FldVal, 3, vbTab)
    txtTimeTo.Text = GetItem(ActFilterValues(MemIdx).FldVal, 4, vbTab)
    txtFlca.Text = GetItem(ActFilterValues(MemIdx).FldVal, 5, vbTab)
    txtFltn.Text = GetItem(ActFilterValues(MemIdx).FldVal, 6, vbTab)
    txtFlns.Text = GetItem(ActFilterValues(MemIdx).FldVal, 7, vbTab)
    txtRegn.Text = GetItem(ActFilterValues(MemIdx).FldVal, 8, vbTab)
    txtAddr.Text = GetItem(ActFilterValues(MemIdx).FldVal, 9, vbTab)
    txtText.Text = GetItem(ActFilterValues(MemIdx).FldVal, 10, vbTab)
    ReorgFolders = False
    LoadFolderData "", ""
End Sub

Private Sub InitFolderByName(cpName As String, bpSetTab As Boolean)
Dim i As Integer
Dim blFound As Boolean
    blFound = False
    If cpName <> "" Then
        i = 0
        While (i < chkFolder.count) And (Not blFound)
            If (UCase(chkFolder(i).Caption) = UCase(cpName)) Or (InStr(UCase(chkFolder(i).Tag), UCase(cpName)) > 0) Then
                If bpSetTab = True Then chkTabs(i).Value = 1
                chkFolder(i).Tag = "0"
                chkFolder(i).Value = 1
                blFound = True
            End If
            i = i + 1
        Wend
    End If
End Sub

Private Sub AtcCSGN_Change()
    If Trim(UseCSGN.Text) <> Trim(AtcCSGN.Text) Then
        CsgnStatus.Visible = True
    Else
        CsgnStatus.Visible = False
    End If
End Sub

Private Sub AtcPreview_Click()
    Dim AskRetVal As Integer
    If AtcPreview.Value = 1 Then
        AtcPreview.BackColor = LightestGreen
        AskRetVal = MyMsgBox.CallAskUser(0, 0, 0, "ATC Interface Preview", "Still under construction.", "hand", "", UserAnswer)
        AtcPreview.Value = 0
    Else
        AtcPreview.BackColor = vbButtonFace
    End If
End Sub

Private Sub AtcTransmit_Click()
    Dim MsgText As String
    Dim OutFldLst As String
    Dim OldFldLst As String
    Dim OutDatLst As String
    Dim OldDatLst As String
    Dim CurLinTxt As String
    Dim CurNewDat As String
    Dim CurOldDat As String
    Dim CurFldNam As String
    Dim UseFldNam As String
    Dim CurFldDat As String
    Dim CedaFields As String
    Dim CedaData As String
    Dim linNbr As Integer
    Dim CedaResult As String
    Dim CedaRetVal As Integer
    Dim AskRetVal As Integer
    
'PALLAS: ARCID,ADEP,ADEPOLD,ADES,ADESOLD,ETD ,ETDOLD ,ETA ,ATD ,ATA ,RWYID ,ARCTYP,STND
'UFIS:   CSGN ,ORG4,ORG4old,DES4,DES4old,ETDA,ETDAold,ETAA,AIRA,LNDA,RWYA/D,ACT3,------

    If AtcTransmit.Value = 1 Then
        AtcTransmit.BackColor = LightestGreen
        AskRetVal = 1
        If Trim(UseCSGN.Text) <> Trim(AtcCSGN.Text) Then
            MsgText = ""
            MsgText = MsgText & "Do you want to change the" & vbNewLine
            MsgText = MsgText & "currently used callsign" & vbNewLine
            MsgText = MsgText & "from " & Trim(UseCSGN.Text) & " to " & Trim(AtcCSGN.Text) & " ?"
            MyMsgBox.InfoApi 0, "Updates the flight", "Hint"
            AskRetVal = MyMsgBox.CallAskUser(0, 0, 0, "ATC Interface Transmit", MsgText, "ask", "Yes,No", UserAnswer)
            If AskRetVal = 1 Then
                AskRetVal = MyMsgBox.CallAskUser(0, 0, 0, "ATC CSGN Update", "Still under construction.", "hand", "", UserAnswer)
                AskRetVal = 1
            End If
        End If
        MsgText = Trim(txtTelexText.Text)
        If MsgText = "" Then AskRetVal = -1
        If AskRetVal = 1 Then
            Screen.MousePointer = 11
            OutFldLst = ""
            OldFldLst = ""
            OutDatLst = ""
            OldDatLst = ""
            linNbr = 0
            Do
                linNbr = linNbr + 1
                CurLinTxt = GetItem(MsgText, linNbr, vbNewLine)
                If CurLinTxt <> "" Then
                    CurNewDat = GetItem(CurLinTxt, 1, "OLD:")
                    CurOldDat = Trim(GetItem(CurLinTxt, 2, "OLD:"))
                    CurFldNam = Trim(GetItem(CurNewDat, 1, ":"))
                    CurFldDat = Trim(GetItem(CurNewDat, 2, ":"))
                    UseFldNam = CurFldNam
                    Select Case CurFldNam
                        Case "CSGN"
                        Case "ORG4"
                            'If CurFldDat = "LGAT" Then CurFldDat = "LGAV"
                            'If CurOldDat = "LGAT" Then CurOldDat = "LGAV"
                        Case "DES4"
                            'If CurFldDat = "LGAT" Then CurFldDat = "LGAV"
                            'If CurOldDat = "LGAT" Then CurOldDat = "LGAV"
                        Case "ETOD"
                            If CurFldDat <> "" Then CurFldDat = Left(CurFldDat & "00", 14)
                            If CurOldDat <> "" Then CurOldDat = Left(CurOldDat & "00", 14)
                            UseFldNam = "ETDA"
                        Case "ETOA"
                            If CurFldDat <> "" Then CurFldDat = Left(CurFldDat & "00", 14)
                            UseFldNam = "ETAA"
                        Case "LAND"
                            If CurFldDat <> "" Then CurFldDat = Left(CurFldDat & "00", 14)
                            UseFldNam = "LNDA"
                        Case "AIRB"
                            If CurFldDat <> "" Then CurFldDat = Left(CurFldDat & "00", 14)
                            UseFldNam = "AIRA"
                        Case "RWYA"
                        Case "RWYD"
                        Case "ACT3"
                        Case "DONE"
                            UseFldNam = ""
                            CurLinTxt = ""
                        Case Else
                            UseFldNam = ""
                    End Select
                    If UseFldNam <> "" Then
                        OutFldLst = OutFldLst & UseFldNam & ","
                        OutDatLst = OutDatLst & CurFldDat & ","
                        If CurOldDat <> "" Then
                            OldFldLst = OldFldLst & UseFldNam & ","
                            OldDatLst = OldDatLst & CurOldDat & ","
                        End If
                    End If
                End If
            Loop While CurLinTxt <> ""
            If OutFldLst <> "" Then
                OutFldLst = Left(OutFldLst, Len(OutFldLst) - 1)
                OutDatLst = Left(OutDatLst, Len(OutDatLst) - 1)
            End If
            If OldFldLst <> "" Then
                OldFldLst = Left(OldFldLst, Len(OldFldLst) - 1)
                OldDatLst = Left(OldDatLst, Len(OldDatLst) - 1)
            End If
            CedaFields = OutFldLst & vbLf & OldFldLst
            CedaData = OutDatLst & vbLf & OldDatLst
            'MsgBox CedaFields & vbNewLine & CedaData
            CedaRetVal = UfisServer.CallCeda(CedaResult, "EXCO", "AFTTAB", CedaFields, CedaData, "ATC", "", 0, True, False)
            Screen.MousePointer = 0
        End If
        AtcTransmit.Value = 0
    Else
        AtcTransmit.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkAbout_Click()
    Dim myControls() As Control
    Dim CompLine As String
    Dim CompList As String
    If chkAbout.Value = 1 Then
        CompLine = "BcProxy.exe,"
        CompLine = CompLine & UfisServer.GetComponentVersion("BcProxy")
        CompList = CompList & CompLine & vbLf
        CompLine = "UfisAppMng.exe,"
        CompLine = CompLine & UfisServer.GetComponentVersion("ufisappmng")
        CompList = CompList & CompLine & vbLf
        
        MyAboutBox.SetComponents myControls(), CompList
        'MyAboutBox.SetGoldenBorder = True
        MyAboutBox.SetLifeStyle = True
        ShowChildForm Me, chkAbout, MyAboutBox, ""
        
        chkAbout.Value = 0
    End If
End Sub

Private Sub chkAddrFilt_Click()
    If chkAddrFilt.Value = 1 Then
        chkAddrFilt.Value = 0
    End If
End Sub

'Private Sub chkAssFile_Click()
'    If chkAssFile.Value = 1 Then
'        chkAssFile.BackColor = LightGreen
'        If OnTop.Value = 1 Then SetFormOnTop Me, False
'        'AssignFolder.Show , Me
'        If OnTop.Value = 1 Then SetFormOnTop Me, True
'    Else
'        'AssignFolder.Hide
'        chkAssFile.BackColor = vbButtonFace
'    End If
'End Sub

Private Sub chkAssFolder_Click()
    Dim TabList As String
    Dim TabTags As String
    Dim TabActs As String
    Dim tmpElse As String
    Dim tmpAll As String
    Dim OldOtherIdx As Long
    Dim OldAllIdx As Long
    
    tmpElse = GetFolderConfig(CInt(OtherTypesCol))
    tmpAll = GetFolderConfig(CInt(AllTypesCol))
    OldOtherIdx = OtherTypesCol
    OldAllIdx = AllTypesCol
    StoreFolderValues CurMem
    If chkAssFolder.Value = 1 Then
        chkLoadAss.Value = 1
        TabList = AddFolderList
        'Must be present, but might be empty
        TabTags = ""
        TabActs = ""
        chkTabs(OldOtherIdx).Value = 0
        chkTabs(OldAllIdx).Value = 0
        InitFolderList TabList, TabTags, TabActs
        RestoreFolderValues CurMem, False
        ReorgFolders = True
        SetFolderConfig CInt(OtherTypesCol), tmpElse
        SetFolderConfig CInt(AllTypesCol), tmpAll
        ReorgFolders = False
        StoreFolderValues CurMem
        SetFolderValues CurMem
        chkAssFolder.BackColor = LightGreen
    Else
        InitFolderList "", "", ""
        RestoreFolderValues CurMem, False
        ReorgFolders = True
        SetFolderConfig CInt(OtherTypesCol), tmpElse
        SetFolderConfig CInt(AllTypesCol), tmpAll
        ReorgFolders = False
        StoreFolderValues CurMem
        chkAssFolder.BackColor = vbButtonFace
    End If
    'StoreFolderValues CurMem
End Sub

Private Sub chkAssign_Click()
    If chkAssign.Value = 1 Then
        ShowChildForm Me, chkAssign, AssignFlight, CurrentRecord
        chkAssign.Value = 0
    End If
End Sub

Private Sub chkAssignBar_Click(Index As Integer)
    Dim tmpTag As String
    If Index = 1 Then
        If chkAssignBar(Index).Value = 1 Then
            tmpTag = chkTlxTitleBar(3).Tag
            If tmpTag = "" Then
                chkTlxTitleBar(3).Tag = "+"
                chkAssignBar(Index).Caption = " -"
                txtTelexList(1).ZOrder
                tabAssign(0).ZOrder
                AssignButtonPanel.ZOrder
                Form_Resize
            Else
                chkTlxTitleBar(3).Tag = ""
                chkAssignBar(Index).Caption = " +"
                ResizeTabs
                ArrangeTabCursor tabAssign(0)
            End If
            chkAssignBar(Index).Value = 0
        End If
        
    End If
End Sub

Private Sub chkClose_Click()
    Dim tmpCancel As Boolean
    If chkClose.Value = 1 Then
        tmpCancel = CheckCancelExit(Me, 2)
        If Not tmpCancel Then Me.Hide
        chkClose.Value = 0
    End If
End Sub

Private Sub chkExit_Click()
    If chkExit.Value = 1 Then
        chkExit.BackColor = LightGreen
        ShowLoaTabViewer "CLOSE"
        ShutDownApplication
        chkExit.Value = 0
    Else
        chkExit.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkFilter_Click(Index As Integer)
    If chkFilter(Index).Value = 1 Then chkFilter(Index).BackColor = LightestGreen
    Select Case Index
        Case 0  'Received
            If chkFilter(2).Value = 1 Then
                If chkFilter(Index).Value = 1 Then
                    ShowChildForm Me, chkFilter(Index), TlxRcvStat, ""
                    TlxRcvStat.SetMultiSelect True
                Else
                    TlxRcvStat.Hide
                End If
            End If
        Case 1  'Sent
            If chkFilter(2).Value = 1 Then
                If chkFilter(Index).Value = 1 Then
                    ShowChildForm Me, chkFilter(Index), TlxSndStat, ""
                    TlxSndStat.SetMultiSelect True
                Else
                    TlxSndStat.Hide
                End If
            End If
        Case 2  'Status
            chkStatus.Value = 0
            If chkFilter(Index).Value = 1 Then
                If chkFilter(1).Value = 1 Then
                    ShowChildForm Me, chkFilter(Index), TlxSndStat, ""
                    TlxSndStat.SetMultiSelect True
                End If
                If chkFilter(0).Value = 1 Then
                    ShowChildForm Me, chkFilter(Index), TlxRcvStat, ""
                    TlxRcvStat.SetMultiSelect True
                End If
            Else
                TlxRcvStat.Hide
                TlxSndStat.Hide
            End If
    End Select
    CheckLoadButton False
    If chkFilter(Index).Value = 0 Then chkFilter(Index).BackColor = vbButtonFace
End Sub

Private Sub chkFlight_Click()
Dim RetVal As Boolean
Dim tmpData As String
Dim tmpVpfr As String
Dim tmpVpto As String
Dim FipsFldLst As String
Dim FipsDatLst As String
Dim FipsMsgTxt As String
Dim CurTelexRecord As String
    If chkFlight.Value = 1 Then
        If Trim(CurrentUrno.Text) <> "" Then
            CurTelexRecord = CurrentRecord.Text
            'VPFR , VPTO, ALC3, Fltn, Flns, Regn, Time, FLNU
            '20001210000000,20001210235900,CSA,420, , , ,27641709
            FipsFldLst = "VPFR,VPTO,ALC3,FLTN,FLNS,REGN,TIME,FLNU"
            FipsDatLst = ""
            RetVal = GetValidTimeFrame(tmpVpfr, tmpVpto, True)
            If RetVal = True Then
                FipsDatLst = FipsDatLst & tmpVpfr & ","
                FipsDatLst = FipsDatLst & tmpVpto & ","
                tmpData = Trim(GetFieldValue("ALC3", CurTelexRecord, DataPool.TlxTabFields))
                If tmpData = "" Then tmpData = Trim(txtFlca.Text)
                FipsDatLst = FipsDatLst & tmpData & ","
                tmpData = Trim(GetFieldValue("FLTN", CurTelexRecord, DataPool.TlxTabFields))
                If tmpData = "" Then tmpData = txtFltn.Tag
                FipsDatLst = FipsDatLst & tmpData & ","
                tmpData = Trim(GetFieldValue("FLNS", CurTelexRecord, DataPool.TlxTabFields))
                If tmpData = "" Then tmpData = Trim(txtFlns.Text)
                FipsDatLst = FipsDatLst & tmpData & ","
                tmpData = Trim(txtRegn.Text)
                tmpData = Replace(tmpData, "-", "", 1, -1, vbBinaryCompare)
                FipsDatLst = FipsDatLst & tmpData & ","
                tmpData = Trim(GetFieldValue("TIME", CurTelexRecord, DataPool.TlxTabFields))
                FipsDatLst = FipsDatLst & tmpData & ","
                tmpData = Trim(GetFieldValue("FLNU", CurTelexRecord, DataPool.TlxTabFields))
                If Val(tmpData) < 1 Then tmpData = ""
                FipsDatLst = FipsDatLst & tmpData
                FipsMsgTxt = FipsFldLst & vbLf & FipsDatLst
                CheckFormOnTop Me, False
                UfisServer.SendMessageToFips FipsMsgTxt
            Else
                MyMsgBox.CallAskUser 0, 0, 0, "Show Flight Filter", "The actual filter does not match your selection.", "stop", "", UserAnswer
            End If
            chkAssign.Value = 0
        Else
            MyMsgBox.CallAskUser 0, 0, 0, "Show Flight", "No telex selected.", "hand", "", UserAnswer
            chkAssign.Value = 0
        End If
        chkFlight.Value = 0
    Else
        'Unload FlightRotation
    End If
End Sub

Private Sub chkFlightBar_Click(Index As Integer)
    Dim idx As Integer
    Dim SetVal As Boolean
    Dim tmpTag As String
    Select Case Index
        Case 0
            If chkFlightBar(Index).Value = 0 Then SetVal = True Else SetVal = False
            tabFlightList(0).Enabled = SetVal
            tabFlightList(1).Enabled = SetVal
            tabFlightList(2).Enabled = SetVal
            For idx = 0 To chkFlightList.UBound
                chkFlightList(idx).Enabled = SetVal
            Next
        Case 1
            If chkFlightBar(Index).Value = 1 Then
                tmpTag = chkFlightBar(Index).Tag
                If tmpTag = "" Then
                    chkFlightBar(Index).Tag = "+"
                    chkFlightBar(Index).Caption = " -"
                    fraFlightList.Top = -105
                    fraFlightList.ZOrder
                    Form_Resize
                Else
                    chkFlightBar(Index).Tag = ""
                    chkFlightBar(Index).Caption = " +"
                    fraSplitter(2).ZOrder
                    fraSplitter(1).ZOrder
                    fraSplitter(4).ZOrder
                    Form_Resize
                    ArrangeTabCursor tabFlightList(0)
                    ArrangeTabCursor tabFlightList(1)
                End If
                chkFlightBar(Index).Value = 0
            End If
    End Select
End Sub

Private Sub chkFlightList_Click(Index As Integer)
    Dim OldVal As Integer
    Dim tmpCount As Long
    Dim tmpCheck As String
    Dim SqlKey As String
    
    If chkFlightList(Index).Value = 1 Then
        chkFlightList(Index).BackColor = LightGreen
        Select Case Index
            Case 0  'Refresh
                chkFlightList(0).Refresh
                Me.Refresh
                tmpCheck = CheckValidFilterFields(False)
                If tmpCheck = "OK" Then
                    chkFlightList(0).Tag = ""
                    OldVal = chkFlightList(1).Value
                    chkFlightList(1).Value = 1
                    chkFlightList(1).Refresh
                    chkFlightList(2).Refresh
                    LoadFlightData 0, True
                    If chkFlightList(0).Tag <> "" Then
                        tmpCount = tabFlightList(0).GetLineCount
                        chkFlightBar(0).Caption = CStr(tmpCount) & " Arr. Flights"
                    End If
                    chkFlightList(2).Value = 1
                    chkFlightList(1).Refresh
                    chkFlightList(2).Refresh
                    LoadFlightData 1, True
                    If chkFlightList(0).Tag <> "" Then
                        tmpCount = tabFlightList(1).GetLineCount
                        chkFlightBar(0).Caption = CStr(tmpCount) & " Dep. Flights"
                        chkFlightList(4).Value = 1
                    End If
                    chkFlightList(1).Value = OldVal
                    chkFlightList(1).Refresh
                    chkFlightList(2).Refresh
                Else
                    If chkFromTo.Tag <> "CHK" Then
                        tmpCheck = "Wrong Filter Values!" & vbNewLine & tmpCheck
                    End If
                    MyMsgBox.CallAskUser 0, 0, 0, "Load Flights", tmpCheck, "stop", "", UserAnswer
                End If
                chkFlightList(Index).Value = 0
            Case 1  'Arr
                chkFlightList(2).Value = 0
                tmpCount = tabFlightList(0).GetLineCount
                chkFlightBar(0).Caption = CStr(tmpCount) & " Arr. Flights"
                tabFlightList(0).Visible = True
                tabFlightList(1).Visible = False
                tabFlightList(2).Visible = False
                Me.Refresh
            Case 2  'Dep
                chkFlightList(1).Value = 0
                tmpCount = tabFlightList(1).GetLineCount
                chkFlightBar(0).Caption = CStr(tmpCount) & " Dep. Flights"
                tabFlightList(1).Visible = True
                tabFlightList(0).Visible = False
                tabFlightList(2).Visible = False
                Me.Refresh
            Case 3  'Scroll
                SyncFlightFlist
                chkFlightList(Index).Value = 0
            Case 4  'Count
                CountFlightTelex
                chkFlightList(Index).Value = 0
            Case 6  'Link Telex and Flight
'                StopOnline = True
                chkFlightList(0).Enabled = False
                chkFlightList(1).Enabled = False
                chkFlightList(2).Enabled = False
                chkFlightList(7).Enabled = True
                chkFlightList(7).BackColor = vbRed
                chkFlightList(7).ForeColor = vbWhite
                chkFlightList(1).Value = 0
                chkFlightList(2).Value = 0
                tabFlightList(0).Visible = False
                tabFlightList(1).Visible = False
                tabFlightList(2).Visible = True
                EditFlightsToTelex
            Case 7  'Insert Links of Telex and Flight
                AssignFlightsToTelex
                If chkTlxAssign.BackColor = vbRed Then
                    chkTlxAssign.Value = 1
                End If
                chkFlightList(7).Value = 0
            Case Else
        End Select
    Else
        chkFlightList(Index).BackColor = vbButtonFace
        Select Case Index
            Case 1  'Arr
                If chkFlightList(6).Value = 0 Then
                    chkFlightList(2).Value = 1
                End If
            Case 2  'Dep
                If chkFlightList(6).Value = 0 Then
                    chkFlightList(1).Value = 1
                End If
            Case 6  'Link Telex and Flight
                'chkFlightList(5).BackColor = vbButtonFace
                'chkFlightList(5).Value = 0
                'chkFlightList(5).Enabled = False
                If EditMode Then
                    chkFlightList(6).Value = 1
                Else
                    chkFlightList(0).Enabled = True
                    chkFlightList(1).Enabled = True
                    chkFlightList(2).Enabled = True
                    chkFlightList(7).Enabled = False
                    chkFlightList(7).BackColor = vbButtonFace
                    chkFlightList(7).ForeColor = vbBlack
                    chkFlightList(0).Value = 1
'                    AddToOnlineWindow
                End If
            Case 7  'Insert Links of Telex and Flight
                chkFlightList(7).BackColor = vbRed
                chkFlightList(7).ForeColor = vbWhite
            Case Else
        End Select
    End If
End Sub

Private Sub chkFltList_Click()
    If chkFltList.Value = 1 Then
        chkFltList.Value = 0
    End If
End Sub

Private Sub chkFolder_Click(Index As Integer)
    Dim AddToOnline As Boolean
    
    If (chkFolder(Index).Value = 1) And (Not ReorgFolders) Then
        AddToOnline = False
        If chkFolder(Index).Tag <> "1" Then
            If Not ChkTabsIsBusy Then CheckToggledTabs Index, chkFolder, 0
            chkFolder(Index).Top = 0
            chkFolder(Index).Height = 375
            chkFolder(Index).BackColor = DarkestBlue
            chkFolder(Index).ForeColor = vbWhite
            fraBlend(Index).ZOrder
'            If InStr(AddFolderList, chkFolder(Index).Caption) > 0 Then
'                StopOnline = True
'            End If
        Else
            If Not ChkTabsIsBusy Then CheckToggledTabs Index, chkFolder, 0
            chkFolder(Index).Tag = "0"
            chkFolder(Index).Top = 60
            chkFolder(Index).Height = 345
            chkFolder(Index).BackColor = vbButtonFace
            chkFolder(Index).ForeColor = vbBlack
            fraLine(Index).ZOrder
'            If InStr(AddFolderList, chkFolder(Index).Caption) > 0 Then
'                AddToOnline = True
'            End If
        End If
        If Not StopNestedCalls Then LoadFolderData "", ""
'        If AddToOnline Then
'            AddToOnlineWindow
'        End If
        chkFolder(Index).Value = 0
    End If
End Sub
Private Sub ReorgFolderStatus(Index As Integer)
    If (chkFolder(Index).Tag <> "") And (chkFolder(Index).Tag <> "0") Then
        chkFolder(Index).Top = 0
        chkFolder(Index).Height = 375
        chkFolder(Index).BackColor = DarkestBlue
        chkFolder(Index).ForeColor = vbWhite
        fraBlend(Index).ZOrder
    Else
        chkFolder(Index).Top = 60
        chkFolder(Index).Height = 345
        chkFolder(Index).BackColor = vbButtonFace
        chkFolder(Index).ForeColor = vbBlack
        fraLine(Index).ZOrder
    End If
End Sub
Private Sub CheckToggledTabs(Index As Integer, CheckObj, ToggleIdx As Integer)
    Dim MaxIdx As Integer
    Dim LastIdx As Integer
    Dim DownValue As Integer
    Dim UpValue As Integer
    Dim i As Integer
    Dim j As Integer
    If Not ChkTabsIsBusy Then
        StopNestedCalls = True
        ChkTabsIsBusy = True
        If ToggleIdx = 0 Then UpValue = 1 Else UpValue = 0
        DownValue = 1
        If chkToggle(ToggleIdx).Value = 0 Then
            For i = 0 To CheckObj.UBound
                If (i <> Index) And (CheckObj(i).Tag <> "0") Then
                    CheckObj(i).Value = UpValue
                    CheckObj(i).Tag = "0"
                End If
            Next
            If CheckObj(Index).Tag = "1" Then CheckObj(Index).Tag = "0" Else CheckObj(Index).Tag = "1"
        Else
            LastIdx = CheckObj.UBound
            MaxIdx = CheckObj.UBound - 1
            If Index <= MaxIdx Then
                If (CheckObj(LastIdx).Tag = "1") Then
                    CheckObj(LastIdx).Value = UpValue
                    CheckObj(LastIdx).Tag = "0"
                End If
                For i = 0 To MaxIdx
                    If CheckObj(i).Tag = "2" Then CheckObj(i).Tag = "0"
                Next
            ElseIf Index = LastIdx Then
                If CheckObj(LastIdx).Tag = "0" Then
                    For i = 0 To MaxIdx
                        If CheckObj(i).Tag <> "0" Then
                            CheckObj(i).Value = UpValue
                            CheckObj(i).Tag = "2"
                        End If
                    Next
                Else
                    For i = 0 To MaxIdx
                        If CheckObj(i).Tag = "2" Then
                            CheckObj(i).Value = DownValue
                            CheckObj(i).Tag = "1"
                        End If
                    Next
                End If
            End If
            If CheckObj(Index).Tag = "1" Then CheckObj(Index).Tag = "0" Else CheckObj(Index).Tag = "1"
        End If
        ChkTabsIsBusy = False
        StopNestedCalls = False
    End If
End Sub

Private Sub LoadFolderData(AftUrno As String, Context As String)
    Dim ShowAllTypes As Boolean
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim CurTag As String
    Dim CurTyp As String
    Dim LineList As String
    Dim TlxLine As String
    Dim LineNbr As String
    Dim LineNo As String
    Dim AddTxt As String
    Dim SereCol As Long
    Dim Txt1Col As Long
    Dim TtypCol As Long
    Dim CurSere As String
    Dim CurTtyp As String
    Dim LineCount0 As Long
    Dim LineCount1 As Long
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    Dim i As Integer
    Dim itm As Integer
    Dim RcvIdx As Integer
    Dim SndIdx As Integer
    Dim AddCapt As String
    Dim tmpDatFrom As String
    Dim tmpDatTo As String
    Dim ActResult As String
    Dim ActCmd As String
    Dim ActTable As String
    Dim ActFldLst As String
    Dim ActCondition As String
    Dim ActOrder As String
    Dim ActDatLst As String
    Dim RetVal As Integer
    Dim count As Integer
    Dim CurLin As Integer
    Dim TlxUrnoList As String
    Dim CurUrno As String
    Dim UrnoCol As Long
    Dim memCount As Long
    Dim memLine As Long
    
    If chkOnlineRcv.Value = 0 Then
        RcvIdx = 0
        SndIdx = 1
        AddCapt = "(Loaded)"
    Else
        RcvIdx = 2
        SndIdx = 3
        AddCapt = "(Online)"
    End If
    
    Screen.MousePointer = 11
    If AftUrno <> "" Then
        For i = 0 To chkFolder.UBound
            If chkFolder(i).Tag = "1" Then chkFolder(i).Value = 1
        Next
    End If
    If Context <> "" Then AddTxt = " of " & Context
    txtTelexText.Text = ""
    txtTelexText.Refresh
    tabTelexList(RcvIdx).ResetContent
    tabTelexList(RcvIdx).Refresh
    tabTelexList(SndIdx).ResetContent
    tabTelexList(SndIdx).Refresh
    TtypCol = CLng(GetRealItemNo(DataPool.TlxTabFields, "TTYP"))
    SereCol = CLng(GetRealItemNo(DataPool.TlxTabFields, "SERE"))
    Txt1Col = CLng(GetRealItemNo(DataPool.TlxTabFields, "TXT1"))
    UrnoCol = CLng(GetRealItemNo(DataPool.TlxTabFields, "URNO"))
    LineCount0 = 0
    LineCount1 = 0
    DataPool.TelexData(CurMem).SetInternalLineBuffer False
    ShowAllTypes = False
    For i = 0 To chkFolder.UBound
        If (chkFolder(i).Tag = "1") Or (AftUrno <> "") Then
            If CLng(i) = AllTypesCol Then ShowAllTypes = True
            If AftUrno <> "" Then
                ShowAllTypes = False
                CurTag = AftUrno
            Else
                CurTag = fraFolder(i).Tag
                If CurTag = "" Then CurTag = "xxxx"
            End If
            itm = 1
            CurTyp = GetItem(CurTag, itm, ",")
            While CurTyp <> ""
                If Not ShowAllTypes Then
                    If InStr(AddFolderList, chkFolder(i).Caption) > 0 Then
                        tmpDatFrom = Mid(txtDateFrom.Text, 7, 4) & Mid(txtDateFrom.Text, 4, 2) & Mid(txtDateFrom.Text, 1, 2)
                        tmpDatTo = Mid(txtDateTo.Text, 7, 4) & Mid(txtDateTo.Text, 4, 2) & Mid(txtDateTo.Text, 1, 2)
                        If tmpDatFrom <> tmpDatTo Then
                            tmpDatFrom = "WHERE (DATC = '" & tmpDatFrom & "' OR DATC = '" & tmpDatTo & "')"
                        Else
                            tmpDatFrom = "WHERE DATC = '" & tmpDatFrom & "'"
                        End If
                        ActResult = ""
                        ActCmd = "RTA"
                        ActTable = "TFNTAB"
                        ActFldLst = "DISTINCT TURN"
                        ActCondition = tmpDatFrom & " AND FNAM = '" & chkFolder(i).Caption & "'"
                        ActOrder = ""
                        ActDatLst = ""
                        RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, False, False)
                        If RetVal >= 0 Then
                            count = UfisServer.DataBuffer(0).GetLineCount - 1
                            If count >= 0 Then
                                For CurLin = 0 To count
                                    TlxUrnoList = TlxUrnoList & UfisServer.DataBuffer(0).GetLineValues(CurLin) & ","
                                Next
                                TlxUrnoList = Left(TlxUrnoList, Len(TlxUrnoList) - 1)
                            End If
                            If Len(TlxUrnoList) > 0 Then
                                count = DataPool.TelexData(CurMem).GetLineCount - 1
                                For CurLin = 0 To count
                                    CurUrno = DataPool.TelexData(CurMem).GetColumnValue(CurLin, UrnoCol)
                                    If InStr(TlxUrnoList, CurUrno) > 0 Then
                                        LineList = LineList & CurLin & ","
                                    End If
                                Next
                                If Len(LineList) > 0 Then
                                    LineList = Left(LineList, Len(LineList) - 1)
                                End If
                            End If
                        End If
                    Else
                        If CLng(i) = OtherTypesCol Then
                            LineList = DataPool.TelexData(CurMem).GetLinesByStatusValue(2, 1)
                        ElseIf AftUrno = "" Then
                            LineList = DataPool.TelexData(CurMem).GetLinesByIndexValue("TTYP", CurTyp, 0)
                        Else
                            LineList = DataPool.TelexData(CurMem).GetLinesByIndexValue("FLNU", CurTyp, 0)
                            If chkAssFolder.Enabled Then
                                ActResult = ""
                                ActCmd = "RTA"
                                ActTable = "TLKTAB"
                                ActFldLst = "DISTINCT TURN"
                                ActCondition = "WHERE FURN = " & CurTyp
                                ActOrder = ""
                                ActDatLst = ""
                                RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, False, False)
                                If RetVal >= 0 Then
                                    count = UfisServer.DataBuffer(0).GetLineCount - 1
                                    If count >= 0 Then
                                        memCount = DataPool.TelexData(CurMem).GetLineCount - 1
                                        For CurLin = 0 To count
                                            CurUrno = UfisServer.DataBuffer(0).GetLineValues(CurLin)
                                            For memLine = 0 To memCount
                                                If CurUrno = DataPool.TelexData(CurMem).GetColumnValue(memLine, UrnoCol) Then
                                                    If LineList = "" Then
                                                        LineList = memLine
                                                    Else
                                                        LineList = LineList & "," & memLine
                                                    End If
                                                    Exit For
                                                End If
                                            Next
                                        Next
                                    End If
                                End If
                            End If
                        End If
                    End If
                    HelperTab.ResetContent
                    HelperTab.InsertBuffer LineList, ","
                    MaxLine = HelperTab.GetLineCount - 1
                Else
                    MaxLine = DataPool.TelexData(CurMem).GetLineCount - 1
                End If
                For CurLine = 0 To MaxLine
                    If (CurLine Mod 200) = 0 Then DoEvents
                    If Not ShowAllTypes Then
                        LineNo = CLng(HelperTab.GetColumnValue(CurLine, 0))
                    Else
                        LineNo = CurLine
                    End If
                    TlxLine = DataPool.TelexData(CurMem).GetColumnValues(LineNo, BrowserColumns) & "," & CStr(LineNo)
                    CurSere = DataPool.TelexData(CurMem).GetColumnValue(LineNo, SereCol)
                    CurTtyp = DataPool.TelexData(CurMem).GetColumnValue(LineNo, TtypCol)
                    DataPool.TelexData(CurMem).GetLineColor LineNo, CurForeColor, CurBackColor
                    If (CurSere = "R") Or (CurSere = "I") Then
                        tabTelexList(RcvIdx).InsertTextLine TlxLine, False
                        If CurTtyp = "FREE" Then tabTelexList(RcvIdx).SetColumnValue LineCount0, 2, ""
                        tabTelexList(RcvIdx).SetLineColor LineCount0, CurForeColor, CurBackColor
                        LineCount0 = LineCount0 + 1
                    Else
                        tabTelexList(SndIdx).InsertTextLine TlxLine, False
                        If CurTtyp = "FREE" Then tabTelexList(SndIdx).SetColumnValue LineCount1, 2, ""
                        If CurSere = "C" Then CurBackColor = LightestYellow
                        tabTelexList(SndIdx).SetLineColor LineCount1, CurForeColor, CurBackColor
                        LineCount1 = LineCount1 + 1
                    End If
                Next
                HelperTab.ResetContent
                itm = itm + 1
                CurTyp = GetItem(CurTag, itm, ",")
            Wend
        End If
        If AftUrno <> "" Then Exit For
    Next
    chkTlxTitleBar(0).Caption = CStr(tabTelexList(RcvIdx).GetLineCount) & " " & chkTlxTitleBar(0).Tag & " " & AddCapt & AddTxt
    chkTlxTitleBar(1).Caption = CStr(tabTelexList(SndIdx).GetLineCount) & " " & chkTlxTitleBar(1).Tag & " " & AddCapt & AddTxt
    chkTlxCnt(0).Caption = CStr(tabTelexList(RcvIdx).GetLineCount)
    chkTlxCnt(1).Caption = CStr(tabTelexList(SndIdx).GetLineCount)
    tabTelexList(RcvIdx).Tag = AddTxt
    tabTelexList(SndIdx).Tag = AddTxt
    If RcvIdx = 1 Then
        tabTelexList(RcvIdx).Sort "4", True, True
    End If
    tabTelexList(RcvIdx).AutoSizeColumns
    tabTelexList(RcvIdx).Refresh
    If SndIdx = 1 Then
        tabTelexList(SndIdx).Sort "4", True, True
    End If
    tabTelexList(SndIdx).AutoSizeColumns
    tabTelexList(SndIdx).Refresh
    ShowFolderButtons "0", "", ""
    If tabTelexList(RcvIdx).GetLineCount > 0 Then
        tabTelexList(RcvIdx).SetCurrentSelection 0
    ElseIf tabTelexList(SndIdx).GetLineCount > 0 Then
        tabTelexList(SndIdx).SetCurrentSelection 0
    End If

    tabTelexList(0).Sort "4", bmSortDirection, True
    tabTelexList(1).Sort "4", bmSortDirection, True
    tabTelexList(0).RedrawTab
    tabTelexList(1).RedrawTab

    If ApplicationIsStarted Then
        If tabTelexList(RcvIdx).Visible = True Then
            tabTelexList(RcvIdx).SetFocus
        Else
            If tabTelexList(SndIdx).Visible = True Then
                tabTelexList(SndIdx).SetFocus
            End If
        End If
    End If
    Screen.MousePointer = 0
End Sub

'Private Sub chkFolder_DragDrop(Index As Integer, Source As Control, X As Single, Y As Single)
'    Dim tmpText As String
'    If TypeOf Source Is TABLib.Tab Then
'        tmpText = Source.GetLineValues(Source.GetCurrentSelected)
'        'chkAssFile.Value = 1
'    End If
'End Sub

'Private Sub chkFolder_DragOver(Index As Integer, Source As Control, X As Single, Y As Single, State As Integer)
'    Dim IsAllowed As Boolean
'    Dim tmpName As String
'    If TypeOf Source Is TABLib.Tab Then
'        If State = 0 Then IsAllowed = True
'        tmpName = chkFolder(Index).Caption
'        If InStr(AddFolderList, tmpName) > 0 Then IsAllowed = True Else IsAllowed = False
'        If State = 1 Then IsAllowed = False
'        If IsAllowed Then
'            Source.DragIcon = Picture1(1).Picture
'        Else
'            Source.DragIcon = Picture1(0).Picture
'        End If
'    End If
'End Sub

Private Sub chkFromTo_Click()
    Dim tmpMsg As String
    Dim RetVal As Integer
    Dim tmpMaxTag As String
    Dim tmpChkTag As String
    Dim tmpDate As String
    Dim maxDiff As Long
    Dim tmpDiff As Long
    Dim tmpVpfr
    Dim tmpVpto
    If chkFromTo.Value = 1 Then
        chkFromTo.BackColor = LightGreen
        chkFromTo.ForeColor = vbBlack
        tmpMaxTag = fraTimeFilter.Tag
        tmpChkTag = chkFromTo.Tag
        If tmpChkTag = "CHK" Then
            If tmpMaxTag <> "" Then
                maxDiff = Val(GetItem(tmpMaxTag, 1, ","))
                tmpVpfr = CedaFullDateToVb(txtDateFrom.Tag & txtTimeFrom.Tag)
                tmpVpto = CedaFullDateToVb(txtDateTo.Tag & txtTimeTo.Tag)
                tmpDiff = DateDiff("n", tmpVpfr, tmpVpto) / 1400
                If tmpDiff > maxDiff Then
                    tmpVpto = DateAdd("d", maxDiff, tmpVpfr)
                    tmpDate = Format(tmpVpto, MySetUp.DefDateFormat)
                    tmpMsg = ""
                    tmpMsg = tmpMsg & "Your input: " & txtDateFrom.Text & " - " & txtDateTo.Text & " (" & CStr(tmpDiff) & " Days)" & vbNewLine
                    tmpMsg = tmpMsg & "Limitation: " & txtDateFrom.Text & " - " & tmpDate & " (" & CStr(maxDiff) & " Days)" & vbNewLine
                    tmpMsg = tmpMsg & "Do you want to reduce the timeframe?"
                    RetVal = MyMsgBox.CallAskUser(0, 0, 0, "Time Filter Confirmation", tmpMsg, "ask", "Yes,No", UserAnswer)
                    If RetVal = 1 Then
                        txtDateTo.Text = tmpDate
                        'txtTimeTo.Text = Format(tmpVpto, "hh:mm")
                        tmpChkTag = "OK"
                    ElseIf RetVal = 2 Then
                        tmpChkTag = "ACK"
                    End If
                End If
                txtDateFrom.BackColor = vbWhite
                txtDateTo.BackColor = vbWhite
            End If
        End If
        chkFromTo.Tag = tmpChkTag
        chkFromTo.Value = 0
    Else
        chkFromTo.BackColor = vbButtonFace
        chkFromTo.ForeColor = vbBlack
    End If
End Sub

Private Sub chkHelp_Click()
    If chkHelp.Value = 1 Then
        ShowHtmlHelp Me, "", ""
        'DisplayHtmHelpFile "", Me.Name, Me.ActiveControl.Name
        chkHelp.Value = 0
    End If
End Sub

Private Sub chkLoad_Click()
    Dim tmpCheck As String
    Dim MemIdx As Integer

    MemIdx = CurMem
    If chkLoad.Value = 1 Then
        If chkFlightList(6).Value = 1 Then
            chkFlightList(6).Value = 0
        End If
        chkLoad.BackColor = vbGreen
        chkLoad.ForeColor = vbBlack
        chkLoad.Enabled = False
        StopLoading = False
        chkStop.Top = chkOnlineRcv.Top
        chkStop.Left = chkOnlineRcv.Left
        chkStop.Value = 0
        If MemIdx = 8 Then
            RefreshOnlineWindows "REF"
            If Not StopLoading Then
                If fraFlightList.Visible Then
                    chkFlightList(0).Value = 1
                    If tabTelexList(2).GetLineCount > 0 Then
                        tabTelexList(2).SetCurrentSelection 0
                    Else
                        tabTelexList(3).SetCurrentSelection 0
                    End If
                End If
            End If
        Else
            If Left(NewFilterValues(MemIdx).TlxKey, 11) = "WHERE URNO=" Then
                LoadTelexData MemIdx, NewFilterValues(MemIdx).TlxKey, NewFilterValues(MemIdx).TfrKey, "ALL", ""
                chkLoad.BackColor = vbButtonFace
                chkLoad.ForeColor = vbBlack
            Else
                tmpCheck = CheckValidFilterFields(True)
                If tmpCheck = "OK" Then
                    txtLoadFrom.Text = txtDateFrom.Text
                    txtLoadTo.Text = txtDateTo.Text
                    txtLoadTimeFrom.Text = txtTimeFrom.Text
                    txtLoadTimeTo.Text = txtTimeTo.Text
                    fraLoadProgress.Visible = True
                    tmpCheck = txtFlca.Tag
                    If Len(tmpCheck) > 3 Then txtFlca.Tag = AskForUniqueAlc(tmpCheck, txtFlca.Text)
                    chkStop.Visible = True
                    LoadTelexData MemIdx, NewFilterValues(MemIdx).TlxKey, NewFilterValues(MemIdx).TfrKey, "ALL", ""
                    tabTelexList(0).Refresh
                    tabTelexList(1).Refresh
                    Me.Refresh
                    If Not StopLoading Then
                        If fraFlightList.Visible Then
                            chkFlightList(0).Value = 1
                            If tabTelexList(0).GetLineCount > 0 Then
                                tabTelexList(0).SetCurrentSelection 0
                            Else
                                tabTelexList(1).SetCurrentSelection 0
                            End If
                        End If
                    End If
                    fraLoadProgress.Visible = False
                Else
                    If chkFromTo.Tag <> "CHK" Then
                        tmpCheck = "Wrong Filter Values!" & vbNewLine & tmpCheck
                    End If
                    MyMsgBox.CallAskUser 0, 0, 0, "Load Telexes", tmpCheck, "stop", "", UserAnswer
                End If
                CheckLoadButton False
            End If
        End If
        chkStop.Value = 0
        chkStop.Visible = False
        StopLoading = False
        chkLoad.Value = 0
        chkLoad.Enabled = True
        If MemIdx <> 8 Then
            CheckLoadButton False
        End If
    Else
        chkLoad.BackColor = vbButtonFace
        chkLoad.ForeColor = vbBlack
    End If
End Sub

Public Function AskForUniqueAlc(AlcList As String, Context As String) As String
    Dim UseAlc As String
    Dim CurAlcd As String
    Dim AlfnList As String
    Dim CurAlfn As String
    Dim ReqList As String
    Dim AlcdItemNo As Long
    Dim AlfnItemNo As Long
    Dim AskText As String
    ReqList = ""
    AlcdItemNo = 0
    CurAlcd = GetRealItem(AlcList, AlcdItemNo, vbLf)
    While CurAlcd <> ""
        AlfnList = UfisServer.BasicLookUp(1, 1, 2, CurAlcd, 0, False)
        AlfnItemNo = 0
        CurAlfn = GetRealItem(AlfnList, AlfnItemNo, vbLf)
        While CurAlfn <> ""
            ReqList = ReqList & CurAlcd & "," & CurAlfn & vbLf
            AlfnItemNo = AlfnItemNo + 1
            CurAlfn = GetRealItem(AlfnList, AlfnItemNo, vbLf)
        Wend
        AlcdItemNo = AlcdItemNo + 1
        CurAlcd = GetRealItem(AlcList, AlcdItemNo, vbLf)
    Wend
    AskText = "{=LABEL=}Please select the desired airline"
    AskText = AskText & "{=DATA=}" & ReqList
    AskText = AskText & "{=HEAD=}3LC,Full Name"
    MyMsgBox.CallAskUser 1, 0, 0, "Basic Data Check for '" & Context & "'", AskText, "hand", "", UserAnswer
    AskForUniqueAlc = UserAnswer
End Function
Private Function CheckValidFilterFields(FullFilter As Boolean) As String
    Dim tmpResult As String
    Dim tmpFlca As String
    Dim tmpAlc3 As String
    Dim tmpMaxTag As String
    Dim tmpChkTag As String
    Dim maxDiff As Long
    Dim tmpDiff As Long
    Dim tmpVpfr
    Dim tmpVpto
    tmpResult = ""
    If txtDateFrom.Tag = "" Then tmpResult = tmpResult & vbNewLine & "Date From: " & txtDateFrom.Text
    If txtDateTo.Tag = "" Then tmpResult = tmpResult & vbNewLine & "Date To: " & txtDateTo.Text
    If txtTimeFrom.Tag = "" Then tmpResult = tmpResult & vbNewLine & "Time From: " & txtTimeFrom.Text
    If txtTimeTo.Tag = "" Then tmpResult = tmpResult & vbNewLine & "Time To: " & txtTimeTo.Text
    If FullFilter Then
        tmpFlca = Trim(txtFlca.Text)
        tmpAlc3 = Trim(txtFlca.Tag)
        If (tmpFlca <> "") And (tmpAlc3 = "") Then
            tmpResult = tmpResult & vbNewLine & "Airline Code"
        End If
    End If
    If tmpResult = "" Then
        tmpResult = "OK"
    End If
    If tmpResult = "OK" Then
        tmpMaxTag = fraTimeFilter.Tag
        tmpChkTag = chkFromTo.Tag
        If tmpChkTag = "CHK" Then tmpChkTag = ""
        If (tmpMaxTag <> "") And (tmpChkTag = "") Then
            maxDiff = Val(GetItem(tmpMaxTag, 1, ","))
            tmpVpfr = CedaFullDateToVb(txtDateFrom.Tag & txtTimeFrom.Tag)
            tmpVpto = CedaFullDateToVb(txtDateTo.Tag & txtTimeTo.Tag)
            tmpDiff = DateDiff("n", tmpVpfr, tmpVpto) / 1400
            If tmpDiff > maxDiff Then
                txtDateFrom.BackColor = vbYellow
                txtDateTo.BackColor = vbYellow
                chkFromTo.BackColor = vbRed
                chkFromTo.ForeColor = vbWhite
                tmpResult = vbNewLine & "The actual timeframe of " & CStr(tmpDiff) & " days"
                tmpResult = tmpResult & vbNewLine & "exceeds the limit of " & CStr(maxDiff) & " days."
                tmpResult = tmpResult & vbNewLine & "Please confirm your input."
                tmpChkTag = "CHK"
            End If
        End If
        chkFromTo.Tag = tmpChkTag
    End If
    If tmpResult <> "OK" Then tmpResult = Mid(tmpResult, 3)
    CheckValidFilterFields = tmpResult
End Function

Public Sub CheckLoadButton(AskBack As Boolean)
    If ApplicationIsStarted Then
        If TlxSqlKeyChanged(CurMem) Then
            If Not ReorgFolders Then
                chkLoad.BackColor = vbRed
                chkLoad.ForeColor = vbWhite
                chkReset.Enabled = True
            Else
                chkLoad.BackColor = vbButtonFace
                chkLoad.ForeColor = vbBlack
            End If
        Else
            If CurMem = 8 Then
                chkLoad.BackColor = vbButtonFace
                chkLoad.ForeColor = vbBlack
            Else
                If optMem(CurMem).Tag > 0 Then
                    chkLoad.BackColor = vbButtonFace
                    chkLoad.ForeColor = vbBlack
                Else
                    chkLoad.BackColor = DarkGreen
                    chkLoad.ForeColor = vbWhite
                End If
            End If
            chkReset.Enabled = False
        End If
    End If
End Sub

Private Function TlxSqlKeyChanged(MemIdx As Integer) As Boolean
Dim clSqlKey As String
Dim clTimeFilter As String
Dim clFlightFilter As String
Dim clTtypFilter As String
Dim clTextFilter As String
Dim clSereFilter As String
Dim clRcvStatFilter As String
Dim clSndStatFilter As String
Dim clOrderKey As String
Dim tmpData As String
Dim tmpRegn As String
Dim clDateFrom As String
Dim clDateTo As String
Dim clTimeFrom As String
Dim clTimeTo As String
Dim tmpLoopTimes As String
Dim i As Integer
    If ApplicationIsStarted Then
        clSqlKey = ""
        'Building the TimeFilter
        GetValidTimeFrame clDateFrom, clDateTo, True
        If optLookTlx.Value = True Then
            clTimeFilter = "TIME BETWEEN '[LOOPVPFR]00' AND '[LOOPVPTO]59'"
            clSqlKey = "(" & clTimeFilter & ")"
        End If
        tmpLoopTimes = clDateFrom & "," & clDateTo
        
        clFlightFilter = BuildFlightFilter("TLXTAB", clDateFrom, clDateTo, txtFlca.Tag, txtFltn.Tag, txtFlns.Text, txtRegn.Text)
        If clFlightFilter <> "" Then
            If clSqlKey <> "" Then clSqlKey = clSqlKey & " AND "
            clSqlKey = clSqlKey & "(" & clFlightFilter & ")"
        End If
        
        'Building the TTYP-Filter
        clTtypFilter = BuildTtypFilter
        If clTtypFilter <> "" Then
            clSqlKey = clSqlKey & " AND (" & clTtypFilter & ")"
        End If
        
        'Building the TextFilter
        clTextFilter = ""
        If optLookTlx.Value = True Then
            'REGN
            tmpData = Trim(txtRegn)
            If tmpData <> "" Then
                tmpRegn = Replace(tmpData, "-", "", 1, -1, vbBinaryCompare)
                If tmpRegn <> "" Then
                    If clTextFilter <> "" Then clTextFilter = clTextFilter & " AND "
                    If tmpData <> tmpRegn Then
                        clTextFilter = clTextFilter & "((TXT1 LIKE '%" & tmpRegn & "%' OR TXT1 LIKE '%" & tmpData & "%') OR "
                        clTextFilter = clTextFilter & "(TXT2 LIKE '%" & tmpRegn & "%' OR TXT2 LIKE '%" & tmpData & "%'))"
                    Else
                        clTextFilter = clTextFilter & "(TXT1 LIKE '%" & txtRegn & "%' OR TXT2 LIKE '%" & txtRegn & "%')"
                    End If
                End If
            End If
        End If
        If Trim(txtAddr) <> "" Then
            If clTextFilter <> "" Then clTextFilter = clTextFilter & " AND "
            tmpData = txtAddr.Text
            tmpData = Replace(tmpData, ".", "", 1, -1, vbBinaryCompare)
            clTextFilter = clTextFilter & "(TXT1 LIKE '%" & tmpData & "%' OR TXT2 LIKE '%" & tmpData & "%')"
        End If
        If Trim(txtText) <> "" Then
            If clTextFilter <> "" Then clTextFilter = clTextFilter & " AND "
            clTextFilter = clTextFilter & "(TXT1 LIKE '%" & txtText & "%' OR TXT2 LIKE '%" & txtText & "%')"
        End If
        If clTextFilter <> "" Then clSqlKey = clSqlKey & " AND " & clTextFilter
        
        'Building the SERE/STAT-Filter
        clSereFilter = ""
        If chkFilter(2).Value = 0 Then
            If chkFilter(0).Value = chkFilter(1).Value Then
                clSereFilter = "SERE IN ('R','I','S','C')"
            ElseIf chkFilter(0).Value = 1 Then
                clSereFilter = "SERE IN ('R','I')"
            Else
                clSereFilter = "SERE IN ('S','C')"
            End If
        Else
            clRcvStatFilter = ""
            clSndStatFilter = ""
            If chkFilter(2).Value = 1 Then
                clRcvStatFilter = TlxRcvStat.GetStatusFilter
                clSndStatFilter = TlxSndStat.GetStatusFilter
            End If
            If chkFilter(0).Value = chkFilter(1).Value Then
                If (clRcvStatFilter = "") And (clSndStatFilter = "") Then
                    clSereFilter = "SERE IN ('R','I','S','C')"
                ElseIf (clRcvStatFilter <> "") And (clSndStatFilter <> "") Then
                    clSereFilter = "(SERE IN ('R','I') AND " & clRcvStatFilter & ") OR (SERE IN ('S','C') AND " & clSndStatFilter & ")"
                ElseIf clRcvStatFilter <> "" Then
                    clSereFilter = "(SERE IN ('R','I') AND " & clRcvStatFilter & ") OR SERE IN ('S','C')"
                Else
                    clSereFilter = "SERE IN ('R','I') OR (SERE IN ('S','C') AND " & clSndStatFilter & ")"
                End If
            ElseIf chkFilter(0).Value = 1 Then
                If clRcvStatFilter = "" Then
                    clSereFilter = "SERE IN ('R','I')"
                Else
                    clSereFilter = "SERE IN ('R','I') AND " & clRcvStatFilter
                End If
            Else
                If clSndStatFilter = "" Then
                    clSereFilter = "SERE IN ('S','C')"
                Else
                    clSereFilter = "SERE IN ('S','C') AND " & clSndStatFilter
                End If
            End If
        End If
        If clSereFilter <> "" Then clSqlKey = clSqlKey & " AND (" & clSereFilter & ")"
        clSqlKey = clSqlKey & "[LOOPTIMES]" & tmpLoopTimes
        NewFilterValues(MemIdx).TlxKey = "WHERE " & clSqlKey
        If optLookAft.Value = True Then GetValidTimeFrame clDateFrom, clDateTo, True
        NewFilterValues(MemIdx).TfrKey = BuildFlightFilter("TFRTAB", clDateFrom, clDateTo, txtFlca.Tag, txtFltn.Tag, txtFlns.Text, txtRegn.Text)
        If NewFilterValues(MemIdx).TlxKey <> ActFilterValues(MemIdx).TlxKey Then TlxSqlKeyChanged = True Else TlxSqlKeyChanged = False
        NewFilterValues(MemIdx).TlxAddr = optTlxAddr.Value
        NewFilterValues(MemIdx).TlxText = optTlxText.Value
        If NewFilterValues(MemIdx).TlxAddr <> ActFilterValues(MemIdx).TlxAddr Then TlxSqlKeyChanged = True
    End If
End Function

Private Function BuildTtypFilter() As String
    Dim clTtypFilter As String
    Dim ActTypes As String
    Dim NotTypes As String
    Dim CurType As String
    Dim ShowElseTypes As Boolean
    Dim i As Integer
    ActTypes = ""
    NotTypes = ""
    clTtypFilter = ""
    If InStr(MainNoShow, "ELSE") > 0 Then
        ShowElseTypes = False
        chkTabs(OtherTypesCol).Enabled = False
        chkFolder(OtherTypesCol).Enabled = False
    Else
        ShowElseTypes = True
        chkTabs(OtherTypesCol).Enabled = True
        chkFolder(OtherTypesCol).Enabled = True
    End If
    If chkTabs(AllTypesCol).Value = 0 Then
        'Individual Types selected
        For i = 0 To LastFolderIndex
            CurType = fraFolder(i).Tag
            CurType = Replace(CurType, ",", "','", 1, -1, vbBinaryCompare)
            If chkTabs(i).Value = 1 Then
                ActTypes = ActTypes & ",'" & CurType & "'"
            Else
                If ShowElseTypes Then NotTypes = NotTypes & ",'" & CurType & "'"
            End If
        Next
    Else
        'All Types selected
        If Not ShowElseTypes Then
            For i = 0 To LastFolderIndex
                CurType = fraFolder(i).Tag
                CurType = Replace(CurType, ",", "','", 1, -1, vbBinaryCompare)
                ActTypes = ActTypes & ",'" & CurType & "'"
            Next
        End If
    End If
    If ShowElseTypes Then
        i = 1
        CurType = GetItem(MainNoShow, i, ",")
        While CurType <> ""
            If InStr(NotTypes, CurType) = 0 Then NotTypes = NotTypes & ",'" & CurType & "'"
            i = i + 1
            CurType = GetItem(MainNoShow, i, ",")
        Wend
    End If
    ActTypes = Mid(ActTypes, 2)
    NotTypes = Mid(NotTypes, 2)
    ActTypes = Replace(ActTypes, "FREE", " ", 1, -1, vbBinaryCompare)
    NotTypes = Replace(NotTypes, "FREE", " ", 1, -1, vbBinaryCompare)
    If ActTypes = "" Then ActTypes = "'--.--'"
    If NotTypes = "" Then NotTypes = "'--.--'"
    If chkTabs(AllTypesCol).Value = 1 Then
        'All Types wanted
        If ShowElseTypes Then
            'Include Else types wanted
            'Excluding NoShow types
            clTtypFilter = "TTYP NOT IN (" & NotTypes & ")"
        Else
            clTtypFilter = "TTYP IN (" & ActTypes & ")"
        End If
    Else
        'Individual Types selected
        If (chkTabs(OtherTypesCol).Value = 0) Or (Not ShowElseTypes) Then
            'No Else types wanted
            clTtypFilter = "TTYP IN (" & ActTypes & ")"
        Else
            'Include Else types wanted
            'Exclude NoShow types
            clTtypFilter = "(TTYP IN (" & ActTypes & ") OR TTYP NOT IN (" & NotTypes & "))"
        End If
    End If
    BuildTtypFilter = clTtypFilter
End Function

Private Function BuildFlightFilter(ForWhat As String, DateFrom As String, DateTo As String, _
                AftAlc3 As String, AftFltn As String, AftFlns As String, AftRegn As String)
Dim tlxFlightFilter As String
Dim tfrFlightFilter As String
Dim tmpTfrFilter As String
Dim tmpAftFilter As String
Dim tmpStoaFilter As String
Dim tmpStodFilter As String
Dim Result As String
'Dim tmpDateFrom As String
'Dim tmpDateTo As String
Dim tmpAlc3 As String
Dim tmpFltn As String
Dim tmpFlns As String
Dim tmpRegn As String
Dim tmpData As String
Dim tmpDateFrom
    Result = ""
    tmpFltn = Trim(AftFltn)
    tmpAlc3 = Trim(AftAlc3)
    tmpFlns = Trim(AftFlns)
    tmpRegn = Trim(AftRegn)
    tmpRegn = Replace(tmpRegn, "-", "", 1, -1, vbBinaryCompare)
    If ForWhat = "TLXTAB" Then
        If optLookTlx.Value = True Then
            tlxFlightFilter = AppendToFilter("", "AND", "FLTN", tmpFltn)
            tlxFlightFilter = AppendToFilter(tlxFlightFilter, "AND", "ALC3", tmpAlc3)
            tlxFlightFilter = AppendToFilter(tlxFlightFilter, "AND", "FLNS", tmpFlns)
        Else
            tmpStoaFilter = LoadFlightData(0, False)
            tmpStodFilter = LoadFlightData(1, False)
            tlxFlightFilter = ""
            tlxFlightFilter = AppendToFilter(tlxFlightFilter, "AND", "FLTN", tmpFltn)
            tlxFlightFilter = AppendToFilter(tlxFlightFilter, "AND", "ALC3", tmpAlc3)
            tlxFlightFilter = AppendToFilter(tlxFlightFilter, "AND", "FLNS", tmpFlns)
            If tlxFlightFilter <> "" Then
                tmpStoaFilter = tmpStoaFilter & " AND " & tlxFlightFilter
                tmpStodFilter = tmpStodFilter & " AND " & tlxFlightFilter
            End If
            If tmpRegn <> "" Then
                If Len(tmpRegn) > 4 Then
                    tmpStoaFilter = tmpStoaFilter & " AND REGN='" & tmpRegn & "'"
                    tmpStodFilter = tmpStodFilter & " AND REGN='" & tmpRegn & "'"
                Else
                    tmpStoaFilter = tmpStoaFilter & " AND REGN LIKE '" & tmpRegn & "%'"
                    tmpStodFilter = tmpStodFilter & " AND REGN LIKE '" & tmpRegn & "%'"
                End If
            End If
            tmpAftFilter = "FLNU IN (SELECT URNO FROM AFTTAB WHERE ("
            tmpAftFilter = tmpAftFilter & "(" & tmpStoaFilter & ") OR "
            tmpAftFilter = tmpAftFilter & "(" & tmpStodFilter & ")))"
            tlxFlightFilter = tmpAftFilter
        End If
        Result = tlxFlightFilter
    ElseIf ForWhat = "TFRTAB" Then
        NewFilterValues(CurMem).TfrKey = ""
        'tmpDateFrom = Trim(Left(DateFrom, 8))
        'tmpDateTo = Trim(Left(DateTo, 8))
        tmpData = tmpAlc3 & tmpFltn & tmpFlns & tmpRegn
        If tmpData <> "" Then
            tfrFlightFilter = "(VATO>='" & DateFrom & "' AND VAFR<='" & DateTo & "')"
            tmpTfrFilter = ""
            If tmpFltn <> "" Then
                tmpTfrFilter = AppendToFilter(tmpTfrFilter, "AND", "FLTN", tmpFltn)
                tmpTfrFilter = AppendToFilter(tmpTfrFilter, "AND", "ALC3", tmpAlc3)
                tmpTfrFilter = AppendToFilter(tmpTfrFilter, "AND", "FLNS", tmpFlns)
                If tmpTfrFilter <> "" Then tfrFlightFilter = tfrFlightFilter & " AND ((" & tmpTfrFilter & ")"
            Else
                tmpTfrFilter = AppendToFilter(tmpTfrFilter, "AND", "ALC3", tmpAlc3)
                tmpTfrFilter = AppendToFilter(tmpTfrFilter, "AND", "FLNS", tmpFlns)
                If tmpTfrFilter <> "" Then tfrFlightFilter = tfrFlightFilter & " AND ((" & tmpTfrFilter & ")"
            End If
            If tmpRegn <> "" Then
                If tmpTfrFilter <> "" Then
                    tfrFlightFilter = tfrFlightFilter & " OR "
                Else
                    tfrFlightFilter = tfrFlightFilter & " AND ("
                End If
                tmpTfrFilter = ""
                If Len(tmpRegn) > 4 Then
                    tmpTfrFilter = AppendToFilter(tmpTfrFilter, "AND", "REGN", tmpRegn)
                Else
                    tmpTfrFilter = AppendToFilter(tmpTfrFilter, "AND", "REGN", tmpRegn & "%")
                End If
                tfrFlightFilter = tfrFlightFilter & "(" & tmpTfrFilter & ")"
            End If
            tfrFlightFilter = tfrFlightFilter & "))"
            Result = "WHERE URNO IN (SELECT DISTINCT(TURN) FROM " & "TFRTAB" & " WHERE (" & tfrFlightFilter & ")"
        End If
    End If
    BuildFlightFilter = Result
End Function
Private Function AppendToFilter(CurFilter As String, AppOperator As String, _
                                FieldName As String, FieldValue As String)
Dim Result As String
    Result = CurFilter
    If FieldValue <> "" Then
        If Result <> "" Then Result = Result & " " & AppOperator & " "
        If InStr(FieldValue, "%") = 0 Then
            Result = Result & FieldName & "='" & FieldValue & "'"
        Else
            Result = Result & FieldName & " LIKE '" & FieldValue & "'"
        End If
    End If
    AppendToFilter = Result
End Function

Private Sub chkLoadAss_Click()
    Dim tmpMax As Long
    Dim i As Integer
    Dim tmpFolderName As String
    Dim LastFolderName As String
    Dim SqlKey As String
    
    If chkLoadAss.Value = 1 Then
        chkLoadAss.BackColor = LightGreen
        tabAssign(0).ResetContent
        tabAssign(0).CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
        tabAssign(0).CedaHopo = UfisServer.HOPO
        tabAssign(0).CedaIdentifier = "IDX"
        tabAssign(0).CedaPort = "3357"
        tabAssign(0).CedaReceiveTimeout = "250"
        tabAssign(0).CedaRecordSeparator = vbLf
        tabAssign(0).CedaSendTimeout = "250"
        tabAssign(0).CedaServerName = UfisServer.HostName
        tabAssign(0).CedaTabext = UfisServer.TblExt
        tabAssign(0).CedaUser = UfisServer.ModName
        tabAssign(0).CedaWorkstation = UfisServer.GetMyWorkStationName
        If CedaIsConnected Then
            If Len(CurrentUrno.Text) = 0 Then
                SqlKey = "WHERE TURN = 0 ORDER BY FNAM"
            Else
                SqlKey = "WHERE TURN = 0 OR TURN = " & CurrentUrno.Text & " ORDER BY FNAM,DATC"
            End If
            tabAssign(0).CedaAction "RTA", "TFNTAB", "FNAM,FDAT,FREM,URNO,TURN,FTYP", " ", SqlKey
        End If
        tabAssign(0).InsertTextLine " , , , , , ", False
        tabAssign(0).AutoSizeColumns
        tabAssign(0).RedrawTab
        AddFolderList = ""
        LastFolderName = ""
        tmpMax = tabAssign(0).GetLineCount - 2
        For i = 0 To tmpMax
            tmpFolderName = tabAssign(0).GetColumnValue(i, 0)
            If tmpFolderName <> LastFolderName Then
                If Len(AddFolderList) = 0 Then
                    AddFolderList = tmpFolderName
                Else
                    If InStr(AddFolderList, tmpFolderName) <= 0 Then
                        AddFolderList = AddFolderList & "," & tmpFolderName
                    End If
                End If
                LastFolderName = tmpFolderName
            Else
                tabAssign(0).SetColumnValue i, 0, " "
            End If
        Next
        chkLoadAss.Value = 0
        chkTlxAssign.BackColor = vbButtonFace
        chkTlxAssign.ForeColor = vbBlack
    Else
        chkLoadAss.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkTlxAssign_Click()
    Dim ActResult As String
    Dim ActCmd As String
    Dim ActTable As String
    Dim ActFldLst As String
    Dim ActCondition As String
    Dim ActOrder As String
    Dim ActDatLst As String
    Dim RetVal As String
    Dim MaxLine As Integer
    Dim i As Integer
    Dim j As Integer
    Dim tmpFkt As String
    Dim tmpFnam As String
    Dim tmpFdat As String
    Dim tmpFrem As String
    Dim tmpUrno As String
    Dim tmpTurn As String
    Dim ErrorMsg As String
    Dim tmpDat As String
    Dim LineNo As Long
    Dim LastFolder As String
    Dim CurrentFolderList As String
    Dim tmpOldFnam As String
    Dim RecallFolder As Boolean
    Dim DeleteFolder As Boolean
    Dim InsertFolder As Boolean
    Dim TlxList As String
    Dim count As Integer
    Dim idx As Long
    Dim SaveTlxUrno As String
    Dim tmpIdx As Integer
    
    If chkTlxAssign.Value = 1 Then
        RecallFolder = False
        DeleteFolder = False
        InsertFolder = False
        TlxList = ""
        chkTlxAssign.BackColor = LightGreen
        chkTlxAssign.ForeColor = vbBlack
        ErrorMsg = ""
        chkTlxAssign.Value = 0
        CurrentFolderList = AddFolderList
        LineNo = tabAssign(0).GetCurrentSelected
        MaxLine = tabAssign(0).GetLineCount - 1
        For i = 0 To MaxLine
            If i = LineNo Then
                tabAssign(0).SetColumnValue i, 5, "N"
            End If
            tmpFkt = tabAssign(0).GetColumnValue(i, 5)
            If tmpFkt = "N" Then
                If Len(Trim(tabAssign(0).GetColumnValue(i, 0))) > 0 Then
                    LastFolder = Trim(tabAssign(0).GetColumnValue(i, 0))
                Else
                    For j = i - 1 To 0 Step -1
                        If Len(Trim(tabAssign(0).GetColumnValue(j, 0))) > 0 Then
                            LastFolder = Trim(tabAssign(0).GetColumnValue(j, 0))
                            Exit For
                        End If
                    Next
                End If
                tmpFrem = Trim(tabAssign(0).GetColumnValue(i, 2))
                If Len(tmpFrem) = 0 Then tmpFrem = " "
                tmpDat = Trim(tabAssign(0).GetColumnValue(i, 1))
                If Len(tmpDat) > 0 Then
                    If Len(tmpDat) = 2 Then tmpDat = ExpandMonth(tmpDat)
                    If Len(tmpDat) = 7 Then
                        tmpFdat = DecodeSsimDayFormat(tmpDat, "SSIM2", "CEDA")
                    Else
                        tmpFdat = "20" & Mid(tmpDat, 5, 2) & Mid(tmpDat, 3, 2) & Mid(tmpDat, 1, 2)
                        tmpDat = DecodeSsimDayFormat(tmpFdat, "CEDA", "SSIM2")
                    End If
                    If CheckValidDate(tmpFdat) = False Then
                        tabAssign(0).SetCurrentSelection i
                        ErrorMsg = "Invalid Date"
                        MyMsgBox.CallAskUser 0, 0, 0, "Save Telex Folder", ErrorMsg, "hand", "", UserAnswer
                        Exit For
                    End If
                Else
                    tmpFdat = ""
                End If
                tmpFnam = Trim(tabAssign(0).GetColumnValue(i, 0))
                tmpUrno = Trim(tabAssign(0).GetColumnValue(i, 3))
                tmpTurn = Trim(tabAssign(0).GetColumnValue(i, 4))
                If Len(tmpUrno) = 0 Then
                    If Len(tmpFnam) > 0 Then
                        ' Insert new folder
                        If InStr(CurrentFolderList, tmpFnam) <= 0 Then
                            ActResult = ""
                            ActCmd = "IRT"
                            ActTable = "TFNTAB"
                            ActFldLst = "URNO,HOPO,FNAM"
                            ActCondition = ""
                            ActOrder = ""
                            ActDatLst = UfisServer.UrnoPoolGetNext & "," & _
                                        UfisServer.HOPO & "," & _
                                        tmpFnam
                            RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
                            CurrentFolderList = CurrentFolderList & "," & tmpFnam
                            If chkAssFolder.Enabled = True And chkAssFolder.Value = 1 Then
                                RecallFolder = True
                            End If
                        End If
                    Else
                        tmpFnam = LastFolder
                    End If
                    If Len(tmpDat) > 0 Then
                        ' Insert folder with date
                        If Len(CurrentUrno.Text) = 0 Then
                            ErrorMsg = "No Telex selected"
                            MyMsgBox.CallAskUser 0, 0, 0, "Save Telex Folder", ErrorMsg, "hand", "", UserAnswer
                            Exit For
                        Else
                            If Len(tmpFnam) > 0 Then
                                ActResult = ""
                                ActCmd = "IRT"
                                ActTable = "TFNTAB"
                                ActFldLst = "URNO,HOPO,FNAM,FDAT,FREM,TURN,DATC"
                                ActCondition = ""
                                ActOrder = ""
                                ActDatLst = UfisServer.UrnoPoolGetNext & "," & _
                                            UfisServer.HOPO & "," & _
                                            tmpFnam & "," & _
                                            tmpDat & "," & _
                                            tmpFrem & "," & _
                                            CurrentUrno.Text & "," & _
                                            tmpFdat
                                RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
                                InsertFolder = True
                            End If
                        End If
                    End If
                Else
                    If Len(tmpDat) = 0 Then
                        If tmpTurn = "0" Then
                            ActResult = ""
                            ActCmd = "RTA"
                            ActTable = "TFNTAB"
                            ActFldLst = "FNAM"
                            ActCondition = "WHERE URNO = " & tmpUrno
                            ActOrder = ""
                            ActDatLst = ""
                            RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
                            If Len(tmpFnam) = 0 Then
                                ' Delete complete folder
                                tmpFnam = Trim(ActResult)
                                If MyMsgBox.CallAskUser(0, 0, 0, "Save Telex Folder", "Do you really want to delete complete folder " & tmpFnam & " ?", "ask", "Yes,No;F", UserAnswer) = 1 Then
                                    ActResult = ""
                                    ActCmd = "RTA"
                                    ActTable = "TFNTAB"
                                    ActFldLst = "TURN"
                                    ActCondition = "WHERE FNAM = '" & tmpFnam & "'"
                                    ActOrder = ""
                                    ActDatLst = ""
                                    RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, False, False)
                                    TlxList = ""
                                    If RetVal >= 0 Then
                                        count = UfisServer.DataBuffer(0).GetLineCount - 1
                                        If count >= 0 Then
                                            For idx = 0 To count
                                                TlxList = TlxList & UfisServer.DataBuffer(0).GetLineValues(idx) & ","
                                            Next
                                            TlxList = Left(TlxList, Len(TlxList) - 1)
                                        End If
                                    End If
                                    ActResult = ""
                                    ActCmd = "DRT"
                                    ActTable = "TFNTAB"
                                    ActFldLst = ""
                                    ActCondition = "WHERE FNAM = '" & tmpFnam & "'"
                                    ActOrder = ""
                                    ActDatLst = ""
                                    RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
                                    If chkAssFolder.Enabled = True And chkAssFolder.Value = 1 Then
                                        RecallFolder = True
                                    End If
                                    DeleteFolder = True
                                End If
                            Else
                                ' Rename folder
                                tmpOldFnam = Trim(ActResult)
                                If tmpFnam <> tmpOldFnam Then
                                    If MyMsgBox.CallAskUser(0, 0, 0, "Save Telex Folder", "Do you really want to change folder name " & tmpOldFnam & " to " & tmpFnam & " ?", "ask", "Yes,No;F", UserAnswer) = 1 Then
                                        ActResult = ""
                                        ActCmd = "URT"
                                        ActTable = "TFNTAB"
                                        ActFldLst = "FNAM"
                                        ActCondition = "WHERE FNAM = '" & tmpOldFnam & "'"
                                        ActOrder = ""
                                        ActDatLst = tmpFnam
                                        RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
                                        If chkAssFolder.Enabled = True And chkAssFolder.Value = 1 Then
                                            RecallFolder = True
                                        End If
                                    End If
                                End If
                            End If
                        Else
                            ' Delete folder with date
                            ActResult = ""
                            ActCmd = "DRT"
                            ActTable = "TFNTAB"
                            ActFldLst = ""
                            ActCondition = "WHERE URNO = " & tmpUrno
                            ActOrder = ""
                            ActDatLst = ""
                            RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
                            TlxList = CurrentUrno.Text
                            DeleteFolder = True
                        End If
                    Else
                        ' Insert folder with date
                        If Len(CurrentUrno.Text) = 0 Then
                            ErrorMsg = "No Telex selected"
                            MyMsgBox.CallAskUser 0, 0, 0, "Save Telex Folder", ErrorMsg, "hand", "", UserAnswer
                            Exit For
                        Else
                            If Len(tmpFnam) = 0 Then tmpFnam = LastFolder
                            If Len(tmpFnam) > 0 Then
                                If Len(tmpTurn) = 0 Or tmpTurn = "0" Then
                                    ActResult = ""
                                    ActCmd = "IRT"
                                    ActTable = "TFNTAB"
                                    ActFldLst = "URNO,HOPO,FNAM,FDAT,FREM,TURN,DATC"
                                    ActCondition = ""
                                    ActOrder = ""
                                    ActDatLst = UfisServer.UrnoPoolGetNext & "," & _
                                                UfisServer.HOPO & "," & _
                                                tmpFnam & "," & _
                                                tmpDat & "," & _
                                                tmpFrem & "," & _
                                                CurrentUrno.Text & "," & _
                                                tmpFdat
                                    RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
                                Else
                                    ActResult = ""
                                    ActCmd = "URT"
                                    ActTable = "TFNTAB"
                                    ActFldLst = "FDAT,FREM,TURN,DATC"
                                    ActCondition = "WHERE URNO = " & tmpUrno
                                    ActOrder = ""
                                    ActDatLst = tmpDat & "," & _
                                                tmpFrem & "," & _
                                                CurrentUrno.Text & "," & _
                                                tmpFdat
                                    RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
                                End If
                                InsertFolder = True
                            End If
                        End If
                    End If
                End If
            End If
        Next
        If Len(ErrorMsg) = 0 Then
            If DeleteFolder = True Then
                count = ItemCount(TlxList, ",") - 1
                If count >= 0 Then
                    SaveTlxUrno = CurrentUrno.Text
                    For idx = 0 To count
                        CurrentUrno.Text = GetRealItem(TlxList, idx, ",")
                        If CurrentUrno.Text <> "0" Then
                            ActResult = ""
                            ActCmd = "RTA"
                            ActTable = "TFNTAB"
                            ActFldLst = "TURN"
                            ActCondition = "WHERE TURN = " & CurrentUrno.Text
                            ActOrder = ""
                            ActDatLst = ""
                            RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
                            If RetVal < 0 Then
                                ActResult = ""
                                ActCmd = "RTA"
                                ActTable = "TLKTAB"
                                ActFldLst = "TURN"
                                ActCondition = "WHERE TURN = " & CurrentUrno.Text
                                ActOrder = ""
                                ActDatLst = ""
                                RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
                                If RetVal < 0 Then
                                    UpdateTelexStatus "V"
                                End If
                            End If
                        End If
                    Next
                    CurrentUrno.Text = SaveTlxUrno
                End If
            Else
                If InsertFolder = True Then
                    If tabTelexList(2).Visible = True Then
                        tmpIdx = tabTelexList(2).GetCurrentSelected
                        tabTelexList(2).SetColumnValue tmpIdx, 1, "A"
                        tabTelexList(2).Refresh
                    End If
                    If tabTelexList(3).Visible = True Then
                        tmpIdx = tabTelexList(3).GetCurrentSelected
                        tabTelexList(3).SetColumnValue tmpIdx, 1, "A"
                        tabTelexList(3).Refresh
                    End If
                    UpdateTelexStatus "A"
                End If
            End If
            chkLoadAss.Value = 1
            If RecallFolder = True Then
                chkAssFolder.Value = 0
                chkAssFolder.Value = 1
            End If
            SetFolderValues CurMem
            If chkFlightList(7).Enabled = True Then
                chkFlightList(7).Value = 1
            End If
        End If
'        AddToOnlineWindow
    Else
        chkTlxAssign.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkLock_Click(Index As Integer)
    Dim LineNo As Integer
    Dim tmpDatTim As String
    Dim tmpDat As String
    Dim tmpTim As String
    Dim TimeVal
    
    If chkLock(Index).Value = 1 Then
        chkLock(Index).BackColor = LightGreen
        tabTelexList(Index + 2).LockScroll True
'        LockBroadcasts = True
    Else
        tabTelexList(Index + 2).LockScroll False
        chkLock(Index).BackColor = vbButtonFace
'        LockBroadcasts = False
'        LineNo = tabTelexList(Index + 2).GetCurrentSelected
'        tmpDatTim = Left(tabTelexList(Index + 2).GetColumnValue(LineNo, 4), 12) & "00"
'        TimeVal = CedaFullDateToVb(tmpDatTim)
'        If optLocal.Value = True Then
'            TimeVal = DateAdd("n", UtcTimeDiff, TimeVal)
'        End If
'        txtDateFrom.Text = Format(TimeVal, MySetUp.DefDateFormat)
'        txtTimeFrom.Text = Format(TimeVal, "hh:mm")
'        chkLoad.Value = 1
    End If
End Sub

Private Sub chkNew_Click()
    Dim CurTlxRec As String
    Dim TlxText As String
    Dim tmpSere As String
    Dim tmpTtyp As String
    If chkNew.Value = 1 Then
        ShowChildForm Me, chkNew, EditTelex, ""
        EditTelex.SetTelexDefault EditTelex.TlxPrioTab, MySitaDefPrio
        CurTlxRec = CurrentRecord.Text
        tmpSere = GetFieldValue("SERE", CurTlxRec, DataPool.TlxTabFields.Text)
        If tmpSere = "C" Then
            tmpTtyp = GetFieldValue("TTYP", CurTlxRec, DataPool.TlxTabFields.Text)
            TlxText = BuildTelexText(CurTlxRec, "TEXT")
            EditTelex.BuildAutoSendTelex "S", tmpSere, tmpTtyp, CurTlxRec, TlxText
        End If
        chkNew.Value = 0
    End If
End Sub

Private Sub chkOnlineRcv_Click()
    Dim i As Integer
    If chkOnlineRcv.Value = 1 Then
        chkOnlineRcv.BackColor = LightGreen
        For i = 0 To optMem.UBound
            optMem(i).Value = False
            optMem(i).BackColor = vbButtonFace
        Next
        chkLoad.Caption = "Refresh"
        chkLoad.ForeColor = vbBlack
        chkLoad.BackColor = vbButtonFace
        CurMem = 8
        tabTelexList(2).Visible = True
        tabTelexList(3).Visible = True
        tabTelexList(0).Visible = False
        tabTelexList(1).Visible = False
        RcvButtonPanel(0).Visible = True
        RcvButtonPanel(1).Visible = True
        'fraFlightFilter.Enabled = False
        'fraTextFilter.Enabled = False
        'fraStatusFilter.Enabled = False
        RcvTelex.Hide
        OutTelex.Hide
        If Len(SaveDateFrom) > 0 Then
            txtDateFrom.Text = Left(SaveDateFrom, 10)
            txtTimeFrom.Text = Mid(SaveDateFrom, 11)
            txtDateTo.Text = Left(SaveDateTo, 10)
            txtTimeTo.Text = Mid(SaveDateTo, 11)
        End If
        'SetFolderValues (CurMem)
        'StoreFolderValues (CurMem)
        'ActFilterValues(CurMem) = NewFilterValues(CurMem)
        PrepareTelexExtract CurMem
        If fraFlightList.Visible Then chkFlightList(0).Value = 1
        If MainIsDeploy = False Then
            chkTlxTitleBar(0).Caption = CStr(tabTelexList(2).GetLineCount) & " " & RcvOnlineCapt
            chkTlxTitleBar(1).Caption = CStr(tabTelexList(3).GetLineCount) & " " & SndOnlineCapt
            If tabTelexList(2).GetLineCount > 0 Then
                tabTelexList(2).SetCurrentSelection 0
            Else
                If tabTelexList(3).GetLineCount > 0 Then
                    tabTelexList(3).SetCurrentSelection 0
                End If
            End If
        End If
    Else
        SaveDateFrom = txtDateFrom.Text & txtTimeFrom.Text
        SaveDateTo = txtDateTo.Text & txtTimeTo.Text
        chkOnlineRcv.BackColor = vbButtonFace
        chkLoad.Caption = "Load"
        chkLoad.ForeColor = vbBlack
        chkLoad.BackColor = vbButtonFace
        If MainIsDeploy = False Then
            chkTlxTitleBar(0).Caption = RcvLoadCapt
            chkTlxTitleBar(1).Caption = SndLoadCapt
        End If
        RcvButtonPanel(0).Visible = False
        RcvButtonPanel(1).Visible = False
        tabTelexList(1).Visible = True
        tabTelexList(0).Visible = True
        tabTelexList(2).Visible = False
        tabTelexList(3).Visible = False
        fraFlightFilter.Enabled = True
        fraTextFilter.Enabled = True
        fraStatusFilter.Enabled = True
        optMem(0).Value = True
        RcvTelex.Show
        OutTelex.Show
        InitDateFields
    End If
    If MainIsDeploy = True Then
        If chkToggleRcv(1).Value = 0 Then
            chkToggleRcv(1).Value = 1
            chkToggleRcv(1).Value = 0
        Else
            chkToggleRcv(1).Value = 0
            chkToggleRcv(1).Value = 1
        End If
        chkTlxCnt(0).Caption = tabTelexList(2).GetLineCount
        chkTlxCnt(1).Caption = tabTelexList(3).GetLineCount
    End If
    tabTelexList(0).ZOrder
    tabTelexList(1).ZOrder
    tabTelexList(2).ZOrder
    tabTelexList(3).ZOrder
    RcvButtonPanel(0).ZOrder
    RcvButtonPanel(1).ZOrder
    AssignButtonPanel.ZOrder
    fraSplitter(0).ZOrder
End Sub

Private Sub chkPrint_Click()
    If chkPrint.Value = 1 Then
        MyMsgBox.CallAskUser 0, 0, 0, "Printer Manager", "Can't find a printer", "stop", "", UserAnswer
        chkPrint.Value = 0
    End If
End Sub

Private Sub chkPrintTxt_Click()
    Dim tmpUrno As String
    TelexPoolHead.MousePointer = 11
    If chkPrintTxt.Value = 1 Then
        tmpUrno = Trim(CurrentUrno.Text)
        If tmpUrno <> "" Then
            PrintServer.PrintOutput txtTelexText
        Else
            MyMsgBox.CallAskUser 0, 0, 0, "Print Manager", "No data selected for print.", "stop", "", UserAnswer
        End If
        chkPrintTxt.Value = 0
    End If
    TelexPoolHead.MousePointer = 0
End Sub

Private Sub chkRedirect_Click()
    Dim CurTlxRec As String
    Dim TlxText As String
    Dim tmpSere As String
    Dim tmpTtyp As String
    If chkRedirect.Value = 1 Then
        ShowChildForm Me, chkRedirect, EditTelex, ""
        EditTelex.SetTelexDefault EditTelex.TlxPrioTab, MySitaDefPrio
        CurTlxRec = CurrentRecord.Text
        tmpSere = GetFieldValue("SERE", CurTlxRec, DataPool.TlxTabFields.Text)
        tmpTtyp = GetFieldValue("TTYP", CurTlxRec, DataPool.TlxTabFields.Text)
        If tmpTtyp = "" Then tmpTtyp = "FREE"
        If tmpSere = "C" Then
            TlxText = BuildTelexText(CurTlxRec, "TEXT")
        Else
            TlxText = GetFieldValue("TXT1", CurTlxRec, DataPool.TlxTabFields.Text)
            TlxText = TlxText & GetFieldValue("TXT2", CurTlxRec, DataPool.TlxTabFields.Text)
            TlxText = CleanString(TlxText, FOR_CLIENT, True)
        End If
        EditTelex.BuildAutoSendTelex "R", tmpSere, tmpTtyp, CurTlxRec, TlxText
        chkRedirect.Value = 0
    End If
End Sub

Private Sub chkRegList_Click()
    If chkRegList.Value = 1 Then
        chkRegList.Value = 0
    End If
End Sub

Private Sub chkReject_Click()
    If chkReject.Value = 1 Then
        UpdateTelexStatus "X"
        chkReject.Value = 0
    End If
End Sub
Private Sub UpdateTelexStatus(SetStat As String)
Dim tmpUrno As String
Dim tmpSqlKey As String
Dim RetVal As String
Dim RetCode As Integer
    tmpUrno = Trim(CurrentUrno.Text)
    If tmpUrno <> "" Then
        tmpSqlKey = "WHERE URNO=" & tmpUrno
        RetCode = UfisServer.CallCeda(RetVal, "URT", DataPool.TableName, "WSTA", SetStat, tmpSqlKey, "", 0, True, False)
    Else
        MyMsgBox.CallAskUser 0, 0, 0, "Update Manager", "No data selected to update.", "stop", "", UserAnswer
    End If
End Sub
Private Sub chkReset_Click()
    If chkReset.Value = 1 Then
        ResetFolderValues CurMem
        chkReset.Value = 0
    End If
End Sub

Private Sub chkScroll_Click()
    If chkScroll.Value = 1 Then chkScroll.BackColor = LightGreen Else chkScroll.BackColor = vbButtonFace
    ScrollFolderList
End Sub

Private Sub chkSetup_Click()
    If chkSetup.Value = 1 Then
        chkSetup.BackColor = LightestGreen
        'OnTop.Value = 0
        SetAllFormsOnTop False
        'ShowChildForm Me, chkSetup, PoolConfig, ""
        PoolConfig.Show
    Else
        PoolConfig.Hide
        SetAllFormsOnTop True
        chkSetup.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkShowTelex_Click(Index As Integer)
    If Index = 0 Then
        If chkShowTelex(0).Value = 1 Then
            chkShowTelex(0).BackColor = LightGreen
            If chkTlxTitleBar(2).Tag = "" Then chkSize(0).Value = 1
            frmTelex.Show , Me
            fraSplitter_MouseMove 0, -1, 0, 0, 0
            frmTelex.txtTelex.Text = txtTelexText.Text
        Else
            chkShowTelex(0).BackColor = vbButtonFace
            frmTelex.Hide
            If chkTlxTitleBar(2).Tag <> "" Then chkSize(0).Value = 1
        End If
    End If
End Sub

Private Sub chkShowTelex_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Index = 0 Then
        If (chkShowTelex(0).Value = 1) And (Button = 2) Then frmTelex.Show
    End If
End Sub

Private Sub chkSize_Click(Index As Integer)
    Dim tmpTag As String
    If chkSize(Index).Value = 1 Then
        tmpTag = chkTlxTitleBar(2).Tag
        If tmpTag = "" Then
            chkTlxTitleBar(2).Tag = "+"
            chkSize(Index).Caption = " -"
            txtTelexList(0).ZOrder
            RcvButtonPanel(0).ZOrder
            RcvButtonPanel(1).ZOrder
            fraToggleRcv.ZOrder
            tabTelexList(0).ZOrder
            tabTelexList(1).ZOrder
            tabTelexList(2).ZOrder
            tabTelexList(3).ZOrder
            AssignButtonPanel.ZOrder
            fraSplitter(0).ZOrder
            Form_Resize
        Else
            chkTlxTitleBar(2).Tag = ""
            chkSize(Index).Caption = " +"
            ResizeTabs
            If tabTelexList(0).Visible Then ArrangeTabCursor tabTelexList(0)
            If tabTelexList(1).Visible Then ArrangeTabCursor tabTelexList(1)
            If tabTelexList(2).Visible Then ArrangeTabCursor tabTelexList(2)
            If tabTelexList(3).Visible Then ArrangeTabCursor tabTelexList(3)
        End If
        chkSize(Index).Value = 0
    End If
End Sub

Private Sub chkStatus_Click()
    Dim SelLine As Long
    Dim tmpUrno As String
    Dim tmpStat As String
    If chkStatus.Value = 1 Then
        tmpUrno = Trim(CurrentUrno.Text)
        If tmpUrno <> "" Then
            If chkTlxTitleBar(1).Value = 0 Then
                ShowChildForm Me, chkStatus, TlxSndStat, ""
            End If
            If chkTlxTitleBar(0).Value = 0 Then
                ShowChildForm Me, chkStatus, TlxRcvStat, ""
                SelLine = tabTelexList(0).GetCurrentSelected
                tmpStat = tabTelexList(0).GetColumnValue(SelLine, 0)
                TlxRcvStat.ExplainStatus "STAT", tmpStat, True
                tmpStat = tabTelexList(0).GetColumnValue(SelLine, 1)
                TlxRcvStat.ExplainStatus "WSTA", tmpStat, True
            End If
        Else
            chkStatus.Value = 0
        End If
    Else
        TlxRcvStat.Hide
        TlxSndStat.Hide
    End If
End Sub


Public Sub SearchFlightTelexes(tmpFldLst As String, tmpDatLst As String)
    Dim tmpData As String
    Dim UseServerTime
    Dim BeginTime
    Dim EndTime
    optMem(1).Value = True
    tmpData = GetFieldValue("ADID", tmpDatLst, tmpFldLst)
    If tmpData = "D" Then
        tmpData = GetFieldValue("STOD", tmpDatLst, tmpFldLst)
    Else
        tmpData = GetFieldValue("STOA", tmpDatLst, tmpFldLst)
    End If
    
    UseServerTime = CedaFullDateToVb(tmpData)
    If optLocal.Value = True Then UseServerTime = DateAdd("n", UtcTimeDiff, UseServerTime)
    BeginTime = DateAdd("h", -Abs(Val(MySetUp.MainFrom)), UseServerTime)
    EndTime = DateAdd("h", Abs(Val(MySetUp.MainTo)), UseServerTime)
    txtDateFrom.Text = Format(BeginTime, MySetUp.DefDateFormat)
    txtDateFrom.Tag = Format(BeginTime, "yyyymmdd")
    txtDateTo.Text = Format(EndTime, MySetUp.DefDateFormat)
    txtDateTo.Tag = Format(EndTime, "yyyymmdd")
    txtTimeFrom.Text = Format(BeginTime, "hh:mm")
    txtTimeTo.Text = Format(EndTime, "hh:mm")
    
    txtFlca = GetFieldValue("ALC3", tmpDatLst, tmpFldLst)
    txtFltn = GetFieldValue("FLTN", tmpDatLst, tmpFldLst)
    txtFlns = GetFieldValue("FLNS", tmpDatLst, tmpFldLst)
    Me.Refresh
    chkLoad.Value = 1
End Sub

Private Sub chkStop_Click()
    Dim tmpTxt As String
    Dim RetVal As Integer
    If chkStop.Value = 1 Then
        chkStop.BackColor = LightGreen
        tmpTxt = ""
        tmpTxt = tmpTxt & "Do you want to stop this procedure ?"
        RetVal = MyMsgBox.CallAskUser(0, 0, 0, "Server Access Control", tmpTxt, "ask", "Yes,No", UserAnswer)
        If RetVal = 1 Then StopLoading = True
        chkStop.Value = 0
    Else
        chkStop.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkTabs_Click(Index As Integer)
    If (Not ChkTabsIsBusy) And (Not ReorgFolders) Then CheckToggledTabs Index, chkTabs, 1
    If chkTabs(Index).Value = 1 Then
        chkTabs(Index).BackColor = LightGreen
    Else
        chkTabs(Index).BackColor = vbButtonFace
    End If
    If (Not StopNestedCalls) And (Not ReorgFolders) Then CheckLoadButton False
End Sub

Private Sub chkTextFilt_Click()
    If chkTextFilt.Value = 1 Then
        chkTextFilt.Value = 0
    End If
End Sub

Private Sub chkTlxCnt_Click(Index As Integer)
    chkTlxCnt(Index).Value = 0
End Sub

Private Sub chkToggle_Click(Index As Integer)
    If chkToggle(Index).Value = 1 Then chkToggle(Index).BackColor = LightGreen Else chkToggle(Index).BackColor = vbButtonFace
End Sub

Private Sub InitFolderList(AddList As String, AddTags As String, AddActs As String)
    Dim i As Long
    Dim j As Long
    Dim TabList As String
    Dim TabTags As String
    Dim TabActs As String
    Dim SetCaption As String
    Dim SetTag As String
    Dim SetAction As String
    
    ShowTabButtons = True
    For i = 0 To fraFolder.UBound
        fraFolder(i).Visible = False
    Next
    fraFolder(0).Height = chkFolder(0).Height
    chkFolder(0).Height = 345
    fraBlend(0).Height = 60
    'chkToggle(0).Height = chkFolder(0).Height - 60
    fraFolder(0).Height = chkFolder(0).Height + fraBlend(0).Height + chkTabs(0).Height + 60
    
    FirstAssignFolder = ItemCount(FolderCaptions, ",")
    
    If AddList <> "" Then
        TabList = FolderCaptions & "," & AddList & ",ELSE,ALL"
        TabTags = FolderTypeList & "," & AddTags & ",,"
        TabActs = TypeActionList & "," & AddActs & ",,"
    Else
        TabList = FolderCaptions & ",ELSE,ALL"
        TabTags = FolderTypeList & ",,"
        TabActs = TypeActionList & ",,"
    End If
    j = ItemCount(TabList, ",") - 1
    AllTypesCol = j
    OtherTypesCol = j - 1
    LastFolderIndex = j - 2
    For i = 0 To j
        SetCaption = GetRealItem(TabList, i, ",")
        SetTag = GetRealItem(TabTags, i, ",")
        SetAction = GetRealItem(TabActs, i, ",")
        If InStr(SetCaption, ":") > 0 Then
            SetCaption = GetRealItem(SetCaption, 0, ":")
            SetTag = GetRealItem(SetTag, 1, ":")
            SetTag = Replace(SetTag, ";", ",", 1, -1, vbBinaryCompare)
        End If
        DefineFolderTab CInt(i), SetCaption, SetTag, SetAction
    Next
    fraFolderLine(0).Left = UfisFolder.Left
    fraFolderLine(1).Left = UfisFolder.Left
    fraFolderLine(2).Left = UfisFolder.Left
    fraFolderLine(0).Top = 510
    fraFolderLine(1).Top = 885
    fraFolderLine(2).Top = 870
    fraFolderLine(0).Width = UfisFolder.Width
    fraFolderLine(1).Width = UfisFolder.Width
    fraFolderLine(2).Width = UfisFolder.Width
    fraFolderLine(0).Visible = True
    FolderButtons(0).Top = UfisFolder.Top + fraFolderToggles.Height - 150
    
    FolderButtons(0).Left = RightCover.Left - 15
    FolderScroll.Max = j
    FolderScroll.Min = 0
    chkScroll.Tag = "-1"
    If Not ShowTabButtons Then chkToggle(1).Visible = False
    chkToggle(1).Value = 1
    Form_Resize
End Sub

Private Sub DefineFolderTab(Index As Integer, SetCaption As String, SetTag As String, SetAction As String)
    Dim NewLeft As Long
    Dim ButtonTop As Long
    Dim FolderTop As Long
    Dim BlendTop As Long
    Dim SelTop As Long
    Dim LineTop As Long
    Dim RightSide As Long
    Dim tmpTxt As String
    
    FolderTop = 135
    ButtonTop = FolderTop + 60
    LineTop = ButtonTop + chkFolder(0).Height - 30
    BlendTop = LineTop - 30
    SelTop = ButtonTop + chkFolder(0).Height + 30
    NewLeft = fraFolder(0).Width * Index + 90
    RightSide = NewLeft + fraFolder(0).Width - 15
    If Index > fraFolder.UBound Then
        Load fraFolder(Index)
        Load chkFolder(Index)
        Load chkTabs(Index)
        Load fraLine(Index)
        Load fraBlend(Index)
        Set chkFolder(Index).Container = fraFolder(Index)
        Set chkTabs(Index).Container = fraFolder(Index)
        Set fraLine(Index).Container = fraFolder(Index)
        Set fraBlend(Index).Container = fraFolder(Index)
    End If
    chkFolder(Index).Width = FolderSize
    fraFolder(Index).Left = NewLeft
    fraFolder(Index).Width = chkFolder(Index).Width
    chkTabs(Index).Width = chkFolder(Index).Width
    fraBlend(Index).Width = chkFolder(Index).Width
    fraLine(Index).Width = chkFolder(Index).Width
    
    chkFolder(Index).Left = 0
    chkTabs(Index).Left = 0
    fraLine(Index).Left = 0
    fraBlend(Index).Left = 0
    fraFolder(Index).Top = FolderTop
    chkFolder(Index).Top = 60
    fraLine(Index).Top = 375
    fraBlend(Index).Top = 345
    chkTabs(Index).Top = 420
    
    fraFolder(Index).BackColor = vbButtonFace
    fraBlend(Index).BackColor = vbButtonFace
    
    chkFolder(Index).Caption = SetCaption
    fraFolder(Index).Tag = SetTag
    fraBlend(Index).Tag = SetAction
    chkFolder(Index).Tag = "0"
    chkTabs(Index).Tag = "0"
    chkTabs(Index).Caption = ""
    
    If (SetTag <> "") And (SetTag <> SetCaption) Then
        If InStr(SetTag, ",") > 0 Then
            tmpTxt = "Telex Types: " & SetTag
        Else
            tmpTxt = "Telex Type: " & SetTag
        End If
    ElseIf SetTag <> "" Then
        tmpTxt = "Telex Type Folder"
    Else
        'Else or All
    End If
    chkFolder(Index).ToolTipText = tmpTxt
    
    If (CLng(Index) >= FirstAssignFolder) And (CLng(Index) < OtherTypesCol) Then
        chkFolder(Index).Picture = Picture1(2).Picture
        chkFolder(Index).ToolTipText = "Hold-File Folder"
    Else
        chkFolder(Index).Picture = LoadPicture("")
    End If
    
    fraFolder(Index).Visible = True
    chkFolder(Index).Visible = True
    fraLine(Index).Visible = True
    fraBlend(Index).Visible = True
    fraFolder(Index).ZOrder
    'chkFolder(Index).ZOrder
    fraBlend(Index).ZOrder
    fraLine(Index).ZOrder
    If ShowTabButtons Then
        chkTabs(Index).Visible = True
        'chkTabs(Index).ZOrder
    End If
End Sub

Private Sub ScrollFolderList()
    Dim NewTop As Long
    Dim CurTop As Long
    CurTop = fraListPanel.Top
    NewTop = ArrangeFolderTabs(CurTop)
    If NewTop <> CurTop Then Form_Resize
End Sub

Private Sub chkToggleRcv_Click(Index As Integer)
    If chkToggleRcv(Index).Value = 1 Then
        chkToggleRcv(Index).BackColor = LightGreen
        Select Case Index
            Case 0
                chkToggleRcv(1).Value = 0
                If chkOnlineRcv.Value = 0 Then
                    chkTlxTitleBar(0).Caption = RcvLoadCapt
                    tabTelexList(0).Visible = True
                    tabTelexList(1).Visible = False
                Else
                    chkTlxTitleBar(0).Caption = tabTelexList(2).GetLineCount & " " & RcvOnlineCapt
                    RcvButtonPanel(0).Visible = True
                    RcvButtonPanel(1).Visible = False
                    tabTelexList(2).Visible = True
                    tabTelexList(3).Visible = False
                    tabTelexList(2).SetCurrentSelection tabTelexList(2).GetCurrentSelected
                End If
            Case 1
                chkToggleRcv(0).Value = 0
                If chkOnlineRcv.Value = 0 Then
                    chkTlxTitleBar(0).Caption = SndLoadCapt
                    tabTelexList(1).Visible = True
                    tabTelexList(0).Visible = False
                Else
                    chkTlxTitleBar(0).Caption = tabTelexList(3).GetLineCount & " " & SndOnlineCapt
                    RcvButtonPanel(1).Visible = True
                    RcvButtonPanel(0).Visible = False
                    tabTelexList(3).Visible = True
                    tabTelexList(2).Visible = False
                    tabTelexList(3).SetCurrentSelection tabTelexList(3).GetCurrentSelected
                    If tabTelexList(3).GetCurrentSelected = -1 Then
                        If tabTelexList(3).GetLineCount > 0 Then
                            tabTelexList(3).SetCurrentSelection 0
                        Else
                            txtTelexText.Text = ""
                            CurrentUrno.Text = ""
                        End If
                    End If
                End If
            Case Else
        End Select
    Else
        chkToggleRcv(Index).BackColor = vbButtonFace
        Select Case Index
            Case 0
                chkToggleRcv(1).Value = 1
            Case 1
                chkToggleRcv(0).Value = 1
            Case Else
        End Select
    End If
End Sub

Private Sub chkTool_Click(Index As Integer)
    If chkTool(Index).Value = 1 Then
        chkTool(Index).BackColor = LightGreen
        Select Case Index
            Case 0
                If Not ImportIsConnected Then MsgToImportTool "0,START"
            Case 1
                'If ImportIsConnected Then MsgToImportTool "0,SPOOL" Else chkTool(Index).Value = 0
                MsgToImportTool "0,SPOOL"
            Case 2
                CheckImportType
                chkTool(Index).Value = 0
            Case Else
        End Select
    Else
        If ImportIsConnected Then
            Select Case Index
                Case 0
                    MsgToImportTool "0,CLOSE"
                Case 1
                    MsgToImportTool "0,ACTIV"
                Case 2
                    'CheckImportType
                Case Else
            End Select
        End If
        chkTool(Index).BackColor = vbButtonFace
    End If
End Sub
Private Sub CheckImportType()
    Dim LineNo As Long
    Dim Index As Integer
    Dim tmpTtyp As String
    Dim TlxLine As String
    Dim RecNbr As String
    Index = -1
    If chkTlxTitleBar(0).Value = 0 Then
        Index = 0
    ElseIf chkTlxTitleBar(1).Value = 0 Then
        Index = 1
    End If
    If Index >= 0 Then
        LineNo = tabTelexList(Index).GetCurrentSelected
        If LineNo >= 0 Then
            tmpTtyp = tabTelexList(Index).GetColumnValue(LineNo, 2)
            Select Case tmpTtyp
                Case "SCOR"
                    TlxLine = tabTelexList(Index).GetLineValues(LineNo)
                    RecNbr = GetItem(TlxLine, 9, ",")
                    If RecNbr <> "" Then
                        LineNo = Val(RecNbr)
                        ScoreToImportTool CurMem, LineNo
                    End If
                Case Else
            End Select
        End If
    End If
End Sub
Public Sub ScoreToImportTool(MemIdx As Integer, LineNo As Long)
    Dim CurTlxRec As String
    Dim tmpText As String
    Dim tmpData As String
    Dim tmpWsta As String
    Dim tmpUrno As String
    Dim clSqlKey As String
    Dim RetCode As Integer
    Dim RetVal As String
    Dim CurItm As Long
    Dim tmpMsg As String
    If LineNo >= 0 Then
        CurTlxRec = DataPool.TelexData(MemIdx).GetLineValues(LineNo)
        tmpWsta = GetFieldValue("WSTA", CurTlxRec, DataPool.TlxTabFields.Text)
        tmpUrno = GetFieldValue("URNO", CurTlxRec, DataPool.TlxTabFields.Text)
        If tmpUrno <> "" Then
            clSqlKey = "WHERE URNO=" & tmpUrno
            If (tmpWsta = "") Or (InStr("TIM", tmpWsta) = 0) Then
                tmpWsta = "T"
                RetCode = UfisServer.CallCeda(RetVal, "URT", DataPool.TableName, "WSTA", tmpWsta, clSqlKey, "", 0, True, False)
            End If
            tmpText = GetFieldValue("TXT1", CurTlxRec, DataPool.TlxTabFields.Text)
            tmpText = tmpText & GetFieldValue("TXT2", CurTlxRec, DataPool.TlxTabFields.Text)
            CurItm = 0
            tmpData = GetRealItem(tmpText, CurItm, Chr(28))
            If tmpData <> "" Then
                MsgToImportTool "0,PCK.BGN"
            End If
            While tmpData <> ""
                CurItm = CurItm + 1
                tmpMsg = CStr(CurItm) & "," & Left(tmpData, 8) & "," & tmpData & "," & tmpUrno
                MsgToImportTool tmpMsg
                tmpData = GetRealItem(tmpText, CurItm, Chr(28))
            Wend
            If CurItm > 0 Then
                MsgToImportTool "0,PCK.END"
            End If
        End If
    End If
End Sub

Private Sub Command1_Click()
    UfisServer.Show , Me
End Sub

Private Sub FixWin_Click()
    If FixWin.Value = 1 Then
        AutoArrange = True
        FixWin.BackColor = LightGreen
        ArrangeAllWin
    Else
        AutoArrange = False
        FixWin.BackColor = vbButtonFace
    End If
End Sub

Public Sub ArrangeAllWin()
    If AutoArrange Then
        If Me.WindowState <> vbNormal Then Me.WindowState = vbNormal
        Me.Refresh
        InitPosSize True
    End If
End Sub

Private Sub FolderScroll_Change()
    ScrollFolderList
End Sub

Private Sub FolderScroll_Scroll()
    ScrollFolderList
End Sub

Private Sub Form_Activate()
    If Not MeIsVisible Then
        'chkAssFile.Enabled = chkAssFolder.Enabled
        chkFilter(0).Value = MySetUp.RcvTabList.Value
        chkFilter(1).Value = MySetUp.SndTabList.Value
        chkFilter(0).Refresh
        chkFilter(1).Refresh
        RcvLoadCapt = "0 " & chkTlxTitleBar(0).Tag & " (Loaded)"
        RcvOnlineCapt = chkTlxTitleBar(0).Tag & " (Online)"
        SndLoadCapt = "0 " & chkTlxTitleBar(1).Tag & " (Loaded)"
        SndOnlineCapt = chkTlxTitleBar(1).Tag & " (Online)"
        AddFolderList = ""
        ArrangeMainOnline
        MeIsVisible = True
        EditMode = False
        InsertTabAssign = False
    End If
End Sub

Public Sub ArrangeMainOnline()
    chkOnlineRcv.Top = 60
    chkOnlineRcv.Visible = True
    RcvButtonPanel(0).Visible = False
    If bmFolderButtons0 = True Then FolderButtons(0).Visible = True
    If MainIsOnline = True Then
        chkOnlineRcv.Enabled = True
        RcvTelex.Hide
        OutTelex.Hide
        FixWin.Value = 1
        FixWin.Value = 0
        chkOnlineRcv.Value = 1
    Else
        chkOnlineRcv.Enabled = False
    End If
    chkConfig.Enabled = False
    If MainIsDeploy = True Then
        'chkTlxTitleBar(1).Visible = False 'sent telexe
        chkTlxTitleBar(2).Visible = False
        'chkTlxTitleBar(3).Visible = False 'sent telexe
        fraToggleRcv.Visible = True
        AssignButtonPanel.Visible = True
        tabAssign(0).Visible = True
        chkAssignBar(0).Visible = True
        chkAssignBar(1).Visible = True
        If MainIsFltDeploy Then
            chkFlightList(6).Visible = True
            chkFlightList(7).Visible = True
        Else
            chkFlightList(6).Visible = False
            chkFlightList(7).Visible = False
        End If
        chkFlightList(5).Visible = False
        If chkToggleRcv(1).Value = 0 Then
            chkToggleRcv(1).Value = 1
            chkToggleRcv(1).Value = 0
        Else
            chkToggleRcv(1).Value = 0
            chkToggleRcv(1).Value = 1
        End If
    Else
        'chkTlxTitleBar(1).Visible = True 'sent telexe
        chkTlxTitleBar(2).Visible = True
        'chkTlxTitleBar(3).Visible = True 'sent telexe
        fraToggleRcv.Visible = False
        AssignButtonPanel.Visible = False
        tabAssign(0).Visible = False
        chkAssignBar(0).Visible = False
        chkAssignBar(1).Visible = False
        If MainIsFltDeploy Then
            chkFlightList(6).Visible = True
            chkFlightList(7).Visible = True
        Else
            chkFlightList(6).Visible = False
            chkFlightList(7).Visible = False
        End If
        chkFlightList(5).Visible = False
        chkTlxTitleBar(0).Value = 1
        chkTlxTitleBar(0).Value = 0
    End If
    If MainIsDeploy Or chkAssFolder.Enabled Then
        chkLoadAss.Value = 1
    End If
    Form_Resize
    If chkAssFolder.Enabled = True Then chkAssFolder.Value = 1
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    'If KeyCode = vbKeyF1 Then DisplayHtmHelpFile "", Me.Name, Me.ActiveControl.Name
    If KeyCode = vbKeyF1 Then ShowHtmlHelp Me, "", ""
    If KeyCode = vbKeyF2 Then chkNew.Value = 1
End Sub

Private Sub Form_LinkClose()
    'MsgBox "LinkClose"
    ImportIsConnected = False
    ImportIsPending = False
    InfoSpooler.ResetContent
    InfoSpooler.Refresh
    SpoolTimer.Enabled = False
    chkTool(0).Value = 0
    chkTool(1).Value = 0
    chkTool(2).Value = 0
End Sub

Private Sub Form_LinkError(LinkErr As Integer)
    MsgBox "LinkError: " & CStr(LinkErr)
End Sub

Private Sub Form_LinkExecute(CmdStr As String, Cancel As Integer)
    MsgBox "LinkExec:" & CmdStr
End Sub

Private Sub Form_LinkOpen(Cancel As Integer)
    'MsgBox "LinkOpen"
    InfoFromClient.ZOrder
    InfoToClient.ZOrder
    ImportIsConnected = True
    ImportIsPending = False
    chkTool(0).Value = 1
    CheckCompareInfo
End Sub

Private Sub Form_Load()
    MeIsVisible = False
    CurButtonPanel = -1
    InfoSpooler.ResetContent
    InfoSpooler.HeaderString = "Message"
    InfoSpooler.HeaderLengthString = "1000"
    InfoSpooler.SetFieldSeparator Chr(16)
    InitPosSize False
    ReorgLayers
    UfisServer.SetIndicator CedaStatus(0).Left, CedaStatus(0).Top, CedaStatus(0), CedaStatus(3), CedaStatus(1), CedaStatus(2)
    UfisServer.ConnectToCeda
    UfisServer.SetUtcTimeDiff
    optUtc.ToolTipText = "Times in UTC (" & HomeAirport & " Local -" & Str(UtcTimeDiff) & " min.)"
    optLocal.ToolTipText = "Times in " & HomeAirport & " Local (UTC +" & Str(UtcTimeDiff) & " min.)"
    FdiTimeFields = "NXTI,ETDE,OFBD,AIRD,ETAE,LNDD,ONBD"
    InitLoaInfo
    HandleTlxTitleBar
    InfoFromClient.Text = "WAIT"
    InfoToClient.Text = "0,0,INIT"
    InfoFromClient.ZOrder
    InfoToClient.ZOrder
    InfoSpooler.ZOrder
    If GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SORT_ASCENDING", "TRUE") = "FALSE" Then
        bmSortDirection = False
    Else
        bmSortDirection = True
    End If
End Sub

Private Sub InitLoaInfo()
    LoaInfo(0).Tag = GetIniEntry(myIniFullName, "LOATAB_INFO", "", "PAX_INFO", "A,B")
    LoaInfo(1).Tag = GetIniEntry(myIniFullName, "LOATAB_INFO", "", "LOA_INFO", "D,C")
    LoaInfo(2).Tag = GetIniEntry(myIniFullName, "LOATAB_INFO", "", "ULD_INFO", "E")

    ' rro 08.11.04: hide the buttons if no LoatabViewer is installed
    Dim strTmp As String
    strTmp = GetIniEntry(myIniFullName, "LOATAB_INFO", "", "LOADTAB_VIEWER", "")
    strTmp = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "LOADTAB_VIEWER", strTmp)
    If strTmp = "" Then
        LoaInfo(0).Visible = False
        LoaInfo(1).Visible = False
        LoaInfo(2).Visible = False
        FolderButtons(0).Visible = False
        bmFolderButtons0 = False
    Else
        LoaInfo(0).Visible = True
        LoaInfo(1).Visible = True
        LoaInfo(2).Visible = True
        FolderButtons(0).Visible = True
        bmFolderButtons0 = True
    End If
End Sub

Public Sub InitPosSize(CheckOnline As Boolean)
    Dim NewHeight As Long
    Dim NewTop As Long
    Dim NewLeft As Long
    Dim WinCnt As Long
    If CheckOnline = True Then
        WinCnt = 0
        If FormIsVisible("RcvTelex") Then WinCnt = WinCnt + 1
        If FormIsVisible("SndTelex") Then WinCnt = WinCnt + 1
        If FormIsVisible("OutTelex") Then WinCnt = WinCnt + 1
        If FormIsVisible("RcvInfo") Then WinCnt = WinCnt + 1
        If WinCnt > 0 Then
            NewHeight = Me.Height
            NewLeft = Me.Left + Me.Width
            NewTop = Me.Top
            NewHeight = NewHeight / WinCnt
            If FormIsVisible("RcvInfo") Then
                RcvInfo.WindowState = vbNormal
                RcvInfo.Show
                RcvInfo.Top = NewTop
                RcvInfo.Left = NewLeft
                RcvInfo.Height = NewHeight
                RcvInfo.OnTop.Value = Me.OnTop.Value
                RcvInfo.Refresh
                NewTop = NewTop + NewHeight
                WinCnt = WinCnt - 1
                If WinCnt = 1 Then NewHeight = Me.Top + Me.Height - NewTop
            End If
            If FormIsVisible("RcvTelex") Then
                RcvTelex.WindowState = vbNormal
                RcvTelex.Show
                RcvTelex.Top = NewTop
                RcvTelex.Left = NewLeft
                RcvTelex.Height = NewHeight
                RcvTelex.OnTop.Value = Me.OnTop.Value
                RcvTelex.Refresh
                NewTop = NewTop + NewHeight
                WinCnt = WinCnt - 1
                If WinCnt = 1 Then NewHeight = Me.Top + Me.Height - NewTop
            End If
            If FormIsVisible("SndTelex") Then
                SndTelex.WindowState = vbNormal
                SndTelex.Show
                SndTelex.Top = NewTop
                SndTelex.Left = NewLeft
                SndTelex.Height = NewHeight
                SndTelex.OnTop.Value = Me.OnTop.Value
                SndTelex.Refresh
                NewTop = NewTop + NewHeight
                WinCnt = WinCnt - 1
                If WinCnt = 1 Then NewHeight = Me.Top + Me.Height - NewTop
            End If
            If FormIsVisible("OutTelex") Then
                OutTelex.WindowState = vbNormal
                OutTelex.Show
                OutTelex.Top = NewTop
                OutTelex.Left = NewLeft
                OutTelex.Height = NewHeight
                OutTelex.OnTop.Value = Me.OnTop.Value
                OutTelex.Refresh
                NewTop = NewTop + NewHeight
                WinCnt = WinCnt - 1
                If WinCnt = 1 Then NewHeight = Me.Top + Me.Height - NewTop
            End If
            If Me.Visible = True Then
                Me.SetFocus
                Me.Refresh
            End If
        End If
    Else
        Me.Left = 300
        Me.Top = 650
        Me.Height = Screen.Height - Me.Top - 300
        Me.Width = fraStatusFilter.Left + fraStatusFilter.Width + RightCover.Width + 150
    End If
End Sub

Public Sub InitMyForm() 'Called by StartModule
    UfisFolder.Left = fraTimeFilter.Left
    UfisFolder.Top = fraTimeFilter.Top + fraTimeFilter.Height + 15
    fraFolderToggles.Top = UfisFolder.Top - 15
    FolderSize = 645
    DefineFolderTab 0, "", "", ""
    UfisFolder.Width = fraStatusFilter.Left + fraStatusFilter.Width - fraTimeFilter.Left
    fraListPanel.Left = UfisFolder.Left
    fraListPanel.Width = UfisFolder.Width
    fraListPanel.Height = UfisFolder.Height
    fraListPanel.Top = UfisFolder.Top + 1020
    panelLine(1).Y1 = 15
    panelLine(1).Y2 = 15
    fraFolderLine(0).Top = fraLine(0).Top
    fraFolderLine(0).Width = UfisFolder.Width
    chkTlxTitleBar(0).Top = 180
    chkTlxTitleBar(0).Left = 60
    chkTlxTitleBar(1).Top = chkTlxTitleBar(0).Top
    chkTlxTitleBar(1).Left = fraTextFilter.Left
    chkTlxTitleBar(1).Width = fraListPanel.Width - chkTlxTitleBar(1).Left - 60
    chkTlxTitleBar(2).Top = chkTlxTitleBar(0).Top
    chkTlxTitleBar(3).Top = chkTlxTitleBar(0).Top
    txtTelexList(2).Top = chkTlxTitleBar(0).Top + chkTlxTitleBar(0).Height + 30
    txtTelexList(2).Left = chkTlxTitleBar(0).Left
    txtTelexList(2).Width = chkTlxTitleBar(0).Width
    txtTelexList(3).Top = txtTelexList(2).Top
    txtTelexList(3).Left = chkTlxTitleBar(1).Left + 120
    txtTelexList(3).Width = chkTlxTitleBar(1).Width
    fraSplitter(0).BackColor = vbButtonFace
    fraSplitter(1).BackColor = vbButtonFace
    fraSplitter(2).BackColor = vbButtonFace
    fraSplitter(3).BackColor = vbButtonFace
    fraSplitter(4).BackColor = vbButtonFace
    InitGrids 12
    InitHiddenFields
    Load MySetUp
    Load DataPool
    InitDateFields
    fraLoadProgress.Top = fraTimeFilter.Top
    fraLoadProgress.Left = fraTimeFilter.Left
    fraLoadProgress.ZOrder
    InitFolderList "", "", ""
    Me.Caption = Me.Tag
    UfisServer.BasicDataInit 1, 0, "ALTTAB", "ALC2,ALC3,ALFN,URNO", ""
    InitFolderByName "All", True
    If MySetUp.FolderMode.Value = 1 Then
        ArrangeLayout True
    Else
        ArrangeLayout False
    End If
    If MySetUp.SndTabList.Value = 1 Then
        ArrangeBrowsers True
    Else
        ArrangeBrowsers False
    End If
    If MySetUp.FlightBrowser.Value = 1 Then
        ArrangeFlightList True
    Else
        ArrangeFlightList False
    End If
    tabTelexList(2).Visible = False
    tabTelexList(3).Visible = False
    Me.Caption = Me.Tag & " (" & MySetUp.ServerTime.Text & " " & MySetUp.ServerTimeType & ")"
End Sub
Public Sub ArrangeLayout(SetValue As Boolean)
    UfisFolder.Visible = SetValue
    fraFolderToggles.Visible = SetValue
    panelLine(0).Visible = SetValue
    panelLine(1).Visible = SetValue
    If SetValue Then
        fraListPanel.Top = UfisFolder.Top + 1020
    Else
        fraListPanel.Top = UfisFolder.Top - 60
    End If
    Form_Resize
End Sub

Public Sub ArrangeBrowsers(SetValue As Boolean)
    chkTlxTitleBar(1).Visible = SetValue
    chkTlxTitleBar(3).Visible = SetValue
    txtTelexList(1).Visible = SetValue
    tabTelexList(1).Visible = SetValue
    tabTelexList(3).Visible = SetValue
    fraSplitter(0).Visible = SetValue
    fraSplitter(1).Visible = SetValue
    chkTlxTitleBar(0).Value = 0
    Form_Resize
End Sub
Public Sub ArrangeFlightList(SetValue As Boolean)
    fraFlightList.Visible = SetValue
    fraSplitter(3).Visible = SetValue
    fraSplitter(3).Visible = SetValue
    fraSplitter(4).Visible = SetValue
    Form_Resize
End Sub
Public Sub InitMemButtons()
    For CurMem = 0 To 3
        optMem(CurMem).Tag = -2
        optMem(CurMem).ToolTipText = "Ready for use"
        CheckLoadButton False
        StoreFolderValues (CurMem)
        ActFilterValues(CurMem) = NewFilterValues(CurMem)
    Next
    If Not MainIsOnline Then
        optMem(0).Value = True
        CurMem = 0
    End If
End Sub
Public Sub InitDateFields()
    Dim ToggleLocalTime As Boolean
    Dim tmpVal As String
    Dim tmpTimeFrom As String
    Dim tmpTimeTo As String
    Dim UseServerTime
    Dim BeginTime
    Dim EndTime
    ToggleLocalTime = optLocal.Value
    optUtc.Value = True
    If chkOnlineRcv.Value = 0 Then
        txtDateFrom.ToolTipText = "Day Date " & MySetUp.DefDateFormat.Text
        txtDateTo.ToolTipText = "Day Date " & MySetUp.DefDateFormat.Text
        UseServerTime = CedaFullDateToVb(MySetUp.ServerTime.Tag)
        If MySetUp.ServerTimeType <> "UTC" Then UseServerTime = DateAdd("n", -UtcTimeDiff, UseServerTime)
        BeginTime = DateAdd("h", -Abs(Val(MySetUp.MainFrom)), UseServerTime)
        EndTime = DateAdd("h", Abs(Val(MySetUp.MainTo)), UseServerTime)
        txtDateFrom.Text = Format(BeginTime, MySetUp.DefDateFormat)
        txtDateFrom.Tag = Format(BeginTime, "yyyymmdd")
        txtDateTo.Text = Format(EndTime, MySetUp.DefDateFormat)
        txtDateTo.Tag = Format(EndTime, "yyyymmdd")
        txtTimeFrom.Text = Format(BeginTime, "hh:mm")
        txtTimeTo.Text = Format(EndTime, "hh:mm")
    Else
        tmpVal = GetOnlineTimeFrame("RCV")
        tmpTimeFrom = GetItem(tmpVal, 1, ",")
        BeginTime = CedaFullDateToVb(tmpTimeFrom)
        tmpTimeTo = GetItem(tmpVal, 2, ",")
        EndTime = CedaFullDateToVb(tmpTimeTo)
        txtDateFrom.Text = Format(BeginTime, MySetUp.DefDateFormat)
        txtDateFrom.Tag = Format(BeginTime, "yyyymmdd")
        txtDateTo.Text = Format(EndTime, MySetUp.DefDateFormat)
        txtDateTo.Tag = Format(EndTime, "yyyymmdd")
        txtTimeFrom.Text = Format(BeginTime, "hh:mm")
        txtTimeTo.Text = Format(EndTime, "hh:mm")
    End If
    optLocal.Value = ToggleLocalTime
End Sub
Private Sub InitHiddenFields()
    CurrentUrno = ""
    CurrentRecord = ""
End Sub
Private Sub InitGrids(ipLines As Integer)
    Dim i As Integer
    For i = 0 To 3
        tabTelexList(i).ResetContent
        tabTelexList(i).FontName = "Courier New"
        tabTelexList(i).HeaderFontSize = MyFontSize
        tabTelexList(i).FontSize = MyFontSize
        tabTelexList(i).LineHeight = MyFontSize
        tabTelexList(i).SetTabFontBold MyFontBold
        tabTelexList(i).HeaderString = "S,W,Type,ST,Time,No,Text Extract                      ,URNO,INDEX"
        'tabTelexList(i).HeaderString = CStr(i) & ",W,Type,ST,Time,No,Text Extract                      ,URNO,INDEX"
        tabTelexList(i).HeaderLengthString = "1,1,4,2,5,3,200,10,10"
        tabTelexList(i).ShowHorzScroller True
        tabTelexList(i).LifeStyle = True
        tabTelexList(i).DateTimeSetColumn 4
        tabTelexList(i).DateTimeSetInputFormatString 4, "YYYYMMDDhhmmss"
        tabTelexList(i).DateTimeSetOutputFormatString 4, "hh':'mm"
        tabTelexList(i).AutoSizeByHeader = True
        If i < 2 Then
            tabTelexList(i).Height = ipLines * tabTelexList(i).LineHeight * Screen.TwipsPerPixelY
            txtTelexList(i + 2).Height = tabTelexList(i).Height + 30
        End If
    Next
        
    For i = 0 To 1
        tabFlightList(i).ResetContent
        tabFlightList(i).FontName = "Courier New"
        tabFlightList(i).HeaderFontSize = MyFontSize
        tabFlightList(i).FontSize = MyFontSize
        tabFlightList(i).LineHeight = MyFontSize
        tabFlightList(i).SetTabFontBold MyFontBold
        tabFlightList(i).AutoSizeByHeader = True
        tabFlightList(i).HeaderString = "TLX,S,Flight,Date/Time,I,D,Reg.No,A/C,APC,Via,Time Status,S,FIDS,Remark 1,Remark 2,URNO,RKEY"
        tabFlightList(i).HeaderLengthString = "10,10,10,10,10,-1,10,10,10,10,10,10,10,10,10,10,10"
        tabFlightList(i).ColumnWidthString = "3,1,9,14,1,1,12,3,3,3,14,1,5,20,20,10,10"
        tabFlightList(i).ColumnAlignmentString = "R,L,L,L,L,L,L,L,L,L,L,L,L,L,L,L,L"
        tabFlightList(i).DateTimeSetColumn 3
        tabFlightList(i).DateTimeSetInputFormatString 3, "YYYYMMDDhhmmss"
        tabFlightList(i).DateTimeSetOutputFormatString 3, "DDMMMYY'/'hh':'mm"
        tabFlightList(i).DateTimeSetColumn 10
        tabFlightList(i).DateTimeSetInputFormatString 10, "YYYYMMDDhhmmss"
        tabFlightList(i).DateTimeSetOutputFormatString 10, "DDMMMYY'/'hh':'mm"
        tabFlightList(i).LifeStyle = True
        tabFlightList(i).CursorLifeStyle = True
        tabFlightList(i).ShowHorzScroller True
        tabFlightList(i).AutoSizeByHeader = True
        tabFlightList(i).AutoSizeColumns
        tabFlightList(i).Refresh
    Next i
    tabFlightList(2).ResetContent
    tabFlightList(2).FontName = "Courier New"
    tabFlightList(2).HeaderFontSize = MyFontSize
    tabFlightList(2).FontSize = MyFontSize
    tabFlightList(2).LineHeight = MyFontSize
    tabFlightList(2).SetTabFontBold MyFontBold
    tabFlightList(2).AutoSizeByHeader = True
    tabFlightList(2).LogicalFieldList = "ADID,FLNO,FDAT,URNO,TURN,FURN,DATC"
    tabFlightList(2).HeaderString = "T,Flight   ,Date/Time    "
    tabFlightList(2).HeaderLengthString = "10,10,10"
    tabFlightList(2).ColumnWidthString = "1,9,7"
    tabFlightList(2).ColumnAlignmentString = "L,L,L"
'    tabFlightList(2).DateTimeSetColumn 3
'    tabFlightList(2).DateTimeSetInputFormatString 3, "YYYYMMDDhhmmss"
'    tabFlightList(2).DateTimeSetOutputFormatString 3, "DDMMMYY'/'hh':'mm"
'    tabFlightList(2).DateTimeSetColumn 10
'    tabFlightList(2).DateTimeSetInputFormatString 10, "YYYYMMDDhhmmss"
'    tabFlightList(2).DateTimeSetOutputFormatString 10, "DDMMMYY'/'hh':'mm"
    tabFlightList(2).LifeStyle = True
    tabFlightList(2).CursorLifeStyle = True
    tabFlightList(2).ShowHorzScroller True
    tabFlightList(2).AutoSizeByHeader = True
    tabFlightList(2).AutoSizeColumns
    tabFlightList(2).SetColumnCount 7
    tabFlightList(2).EnableInlineEdit True
    tabFlightList(2).Refresh
    
    tabFlightList(0).Width = fraFlightList.Width - 75
    tabFlightList(1).Width = tabFlightList(0).Width
    tabFlightList(2).Width = tabFlightList(0).Width
    tabFlightList(1).Left = tabFlightList(0).Left
    tabFlightList(1).Top = tabFlightList(0).Top
    tabFlightList(1).Visible = False
    tabFlightList(2).Left = tabFlightList(0).Left
    tabFlightList(2).Top = tabFlightList(0).Top
    tabFlightList(2).Visible = False
    
    fraSplitter(0).Top = chkTlxTitleBar(0).Top
    fraSplitter(0).Left = chkTlxTitleBar(0).Left + chkTlxTitleBar(0).Width
    fraSplitter(0).Height = chkTlxTitleBar(0).Height + txtTelexList(2).Height + 2 * Screen.TwipsPerPixelY
    fraSplitter(2).Left = chkTlxTitleBar(0).Left
    fraSplitter(2).Top = txtTelexList(2).Top + txtTelexList(2).Height
    fraSplitter(2).Width = txtTelexList(3).Left + txtTelexList(3).Width - txtTelexList(2).Left
    fraSplitter(1).Top = fraSplitter(2).Top - 4 * Screen.TwipsPerPixelY
    fraSplitter(1).Left = fraSplitter(0).Left - 4 * Screen.TwipsPerPixelX
    fraSplitter(4).Top = fraSplitter(2).Top
    txtTelexText.Top = txtTelexList(2).Top + txtTelexList(2).Height + 9 * Screen.TwipsPerPixelY
    txtTelexText.Left = txtTelexList(2).Left
    txtTelexText.Width = txtTelexList(3).Left + txtTelexList(3).Width - txtTelexList(2).Left + 15
    txtTelexText.FontSize = MyFontSize - 5
    txtTelexText.FontBold = MyFontBold
    RightCover.Top = 0
    tabTelexList(2).AutoSizeColumns
    tabTelexList(3).AutoSizeColumns
    
    tabAssign(0).ResetContent
    tabAssign(0).HeaderString = "Folder Name,Date   ,Remark                "
    tabAssign(0).LogicalFieldList = "FNAM,FDAT,FREM,URNO,TURN,FTYP"
    tabAssign(0).FontName = "Courier New"
    tabAssign(0).HeaderFontSize = MyFontSize
    tabAssign(0).FontSize = MyFontSize
    tabAssign(0).LineHeight = MyFontSize
    tabAssign(0).SetTabFontBold MyFontBold
    tabAssign(0).HeaderLengthString = "20,10,128,10,10,1"
    tabAssign(0).ColumnWidthString = "8,7,32"
    tabAssign(0).ColumnAlignmentString = "L,L,L"
    tabAssign(0).LifeStyle = True
    tabAssign(0).CursorLifeStyle = True
    tabAssign(0).ShowHorzScroller True
    'tabAssign(0).SetColumnBoolProperty 0, "1", "0"
    tabAssign(0).AutoSizeByHeader = True
    tabAssign(0).AutoSizeColumns
    tabAssign(0).NoFocusColumns = "3,4,5"
    tabAssign(0).SetColumnCount 6
    tabAssign(0).PostEnterBehavior = 3
    tabAssign(0).EnableInlineEdit True
    tabAssign(0).Refresh
    
    ResizeTabs
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Cancel = CheckCancelExit(Me, 3)
    End If
End Sub

Private Sub Form_Resize()
    Dim NewSize As Long
    Dim NewLeft As Long
    Dim MaxSize As Long
    Dim SizeDiff As Long
    Dim NewPanelTop As Long
    Dim MinFolderHeight As Long
    Dim MinFolderWidth As Long
    Dim NewFolderHeight As Long
    Dim NewFolderBottom As Long
    Dim NewFolderWidth As Long
    If Me.WindowState <> vbMinimized Then
        If Me.ScaleHeight > MyStatusBar.Height + 120 Then
            NewPanelTop = fraListPanel.Top
            RightCover.Left = TelexPoolHead.ScaleWidth - RightCover.Width
            fraFolderToggles.Left = RightCover.Left - 75
            RightCover.Height = Me.ScaleHeight - MyStatusBar.Height
            NewLeft = RightCover.Left + (RightCover.Width - OnTop.Width) / 2
            TopButtons.Left = NewLeft - 30
            basicButtons.Left = NewLeft
            FolderButtons(0).Left = RightCover.Left - 75
            If CurButtonPanel > 0 Then
                FolderButtons(CurButtonPanel).Left = FolderButtons(0).Left
                If FolderButtons(0).Visible Then
                    FolderButtons(CurButtonPanel).Top = FolderButtons(0).Top + FolderButtons(0).Height
                Else
                    FolderButtons(CurButtonPanel).Top = FolderButtons(0).Top
                End If
            End If
            basicButtons.Top = TelexPoolHead.ScaleHeight - basicButtons.Height - MyStatusBar.Height - 120
            If basicButtons.Top < TopButtons.Top + TopButtons.Height Then
                TopButtons.BackColor = vbBlack
            Else
                TopButtons.BackColor = vbButtonFace
            End If
            MinFolderWidth = 1500
            NewFolderWidth = RightCover.Left - UfisFolder.Left * 2 - 15
            If NewFolderWidth > MinFolderWidth Then
                SizeDiff = NewFolderWidth - UfisFolder.Width
                UfisFolder.Width = UfisFolder.Width + SizeDiff
                fraListPanel.Width = fraListPanel.Width + SizeDiff
                panelLine(0).X2 = fraListPanel.Width - 15
                panelLine(1).X2 = fraListPanel.Width
                NewPanelTop = ArrangeFolderTabs(fraListPanel.Top)
                fraFolderLine(0).Width = UfisFolder.Width
                fraFolderLine(1).Width = UfisFolder.Width
                fraFolderLine(2).Width = UfisFolder.Width
                fraSplitter(2).Width = UfisFolder.Width - 135
                If MySetUp.SndTabList.Value = 1 Then
                    If chkTlxTitleBar(1).Value = 0 Then
                        NewSize = fraListPanel.Width - chkTlxTitleBar(1).Left - 60
                        If NewSize <= 600 Then
                            fraSplitter(0).Left = fraSplitter(0).Left + SizeDiff
                            fraSplitter(1).Left = fraSplitter(1).Left + SizeDiff
                        End If
                    Else
                        NewLeft = fraListPanel.Width - txtTelexList(3).Width - fraSplitter(0).Width - 75
                        If NewLeft > 600 Then
                            SizeDiff = NewLeft - fraSplitter(0).Left
                            fraSplitter(0).Left = fraSplitter(0).Left + SizeDiff
                            fraSplitter(1).Left = fraSplitter(1).Left + SizeDiff
                        End If
                    End If
                    If (fraListPanel.Width - 900) <= fraSplitter(0).Left Then
                        fraSplitter(0).Left = fraListPanel.Width - 900
                        fraSplitter(1).Left = fraSplitter(0).Left - 30
                    End If
                    txtTelexList(2).Width = fraSplitter(0).Left - 60
                    txtTelexList(3).Left = fraSplitter(0).Left + fraSplitter(0).Width
                    txtTelexList(3).Width = UfisFolder.Width - txtTelexList(3).Left - 75
                Else
                    txtTelexList(2).Width = UfisFolder.Width - 120
                End If
            End If
            fraListPanel.Top = NewPanelTop
            NewFolderHeight = Me.ScaleHeight - UfisFolder.Top - MyStatusBar.Height - 30
            If NewFolderHeight > 3000 Then
                UfisFolder.Height = NewFolderHeight
                NewFolderHeight = UfisFolder.Height - fraListPanel.Top + UfisFolder.Top
                If NewFolderHeight > 2500 Then
                    fraListPanel.Height = NewFolderHeight
                    NewSize = NewFolderHeight - txtTelexList(2).Top - 60
                    If NewSize > 600 Then
                        If chkTlxTitleBar(2).Tag <> "" Then
                            txtTelexList(0).Height = NewSize
                        End If
                        If chkTlxTitleBar(3).Tag <> "" Then
                            txtTelexList(1).Height = NewSize
                        End If
                    End If
                End If
                NewSize = NewFolderHeight - txtTelexText.Top - 60
                If NewSize > 300 Then txtTelexText.Height = NewSize
            End If
            If fraFlightList.Visible Then
                NewLeft = fraSplitter(2).Width - fraFlightList.Width + fraSplitter(2).Left
                If NewLeft > 900 Then
                    fraFlightList.Left = NewLeft
                Else
                    fraFlightList.Left = 900
                    NewSize = fraSplitter(2).Left + fraSplitter(2).Width - 900
                    fraFlightList.Width = NewSize
                    tabFlightList(0).Width = NewSize - 120
                    tabFlightList(1).Width = tabFlightList(0).Width
                    tabFlightList(2).Width = tabFlightList(0).Width
                End If
                chkFlightBar(0).Width = fraFlightList.Width - chkFlightBar(1).Width - 90
                chkFlightBar(1).Left = chkFlightBar(0).Width + 45
                fraSplitter(3).Left = fraFlightList.Left - fraSplitter(3).Width
                fraSplitter(4).Left = fraSplitter(3).Left - 15
                txtTelexText.Width = fraSplitter(3).Left - txtTelexText.Left - 15
                If chkFlightBar(1).Tag = "" Then
                    fraFlightList.Top = txtTelexText.Top - 105
                    fraFlightList.Height = txtTelexText.Height + 105
                Else
                    fraFlightList.Height = fraListPanel.Height + 105
                End If
                NewSize = fraFlightList.Height - tabFlightList(0).Top - 45
                If NewSize > 750 Then
                    tabFlightList(0).Height = NewSize
                    tabFlightList(1).Height = tabFlightList(0).Height
                    tabFlightList(2).Height = tabFlightList(0).Height
                End If
                fraSplitter(3).Top = txtTelexText.Top
                fraSplitter(3).Height = txtTelexText.Height
            Else
                txtTelexText.Width = fraSplitter(2).Width
            End If
            CedaBusyFlag.Top = TelexPoolHead.ScaleHeight - MyStatusBar.Height + 2 * Screen.TwipsPerPixelY
            CedaBusyFlag.Left = TelexPoolHead.ScaleWidth - CedaBusyFlag.Width - 12 * Screen.TwipsPerPixelX
            If FixWin.Value = 1 Then InitPosSize True
        End If
        DrawBackGround RightCover, MyLifeStyleValue, True, True
        ResizeTabs
        Me.Refresh
    End If
End Sub
Private Sub ResizeTabs()
    Dim Left0 As Long
    Dim Left1 As Long
    Dim Size0 As Long
    Dim Size1 As Long
    Dim Idx1 As Integer
    Dim Idx2 As Integer
    Dim Idx3 As Integer
    On Error Resume Next
    If MainIsDeploy = False Then
        Size0 = txtTelexList(2).Width
        Left0 = txtTelexList(2).Left
        Size1 = txtTelexList(3).Width
        Left1 = txtTelexList(3).Left
        Idx1 = 1
        Idx2 = 2
        Idx3 = 3
    Else
        Left0 = txtTelexList(2).Left
        Size0 = txtTelexList(2).Width - fraToggleRcv.Width - 15
        If Size0 < 15 Then
            Left0 = Size0
            Size0 = 15
        End If
        Left1 = txtTelexList(2).Left + Size0
        Size1 = txtTelexList(2).Width - Size0
        Idx1 = 0
        Idx2 = 2
        Idx3 = 3
    End If
    chkTlxTitleBar(0).Left = Left0 + 15
    If MainIsDeploy Then
        chkTlxTitleBar(0).Width = Size0
        fraToggleRcv.Top = chkTlxTitleBar(0).Top
        fraToggleRcv.Left = chkTlxTitleBar(0).Left + chkTlxTitleBar(0).Width + 30
    Else
        chkTlxTitleBar(0).Width = Size0 - chkTlxTitleBar(2).Width - 30
        chkTlxTitleBar(2).Left = chkTlxTitleBar(0).Left + chkTlxTitleBar(0).Width + 15
    End If
    chkTlxTitleBar(1).Left = Left1 + 15
    chkTlxTitleBar(1).Width = Size1 - chkTlxTitleBar(3).Width - 30
    chkTlxTitleBar(3).Left = chkTlxTitleBar(1).Left + chkTlxTitleBar(1).Width + 15
    
    txtTelexList(0).Top = txtTelexList(Idx2).Top
    txtTelexList(0).Left = txtTelexList(Idx2).Left
    If chkTlxTitleBar(2).Tag = "" Then
        txtTelexList(0).Height = txtTelexList(Idx2).Height
    End If
    txtTelexList(0).Width = txtTelexList(Idx2).Width
    tabTelexList(0).Top = txtTelexList(0).Top + 15
    tabTelexList(0).Height = txtTelexList(0).Height - 45
    tabTelexList(0).Left = txtTelexList(0).Left + 15
    tabTelexList(0).Width = txtTelexList(0).Width - 45
    
    txtTelexList(1).Top = txtTelexList(Idx3).Top
    txtTelexList(1).Left = txtTelexList(Idx3).Left
    If chkTlxTitleBar(3).Tag = "" Then
        txtTelexList(1).Height = txtTelexList(Idx3).Height
    End If
    txtTelexList(1).Width = txtTelexList(Idx3).Width
    tabTelexList(1).Top = txtTelexList(Idx1).Top + 15
    tabTelexList(1).Height = txtTelexList(Idx1).Height - 45
    tabTelexList(1).Left = txtTelexList(Idx1).Left + 15
    tabTelexList(1).Width = txtTelexList(Idx1).Width - 45
    
    RcvButtonPanel(0).Top = tabTelexList(0).Top + 30
    RcvButtonPanel(0).Left = tabTelexList(0).Left - 15
    tabTelexList(2).Top = tabTelexList(0).Top + RcvButtonPanel(0).Height + 45
    tabTelexList(2).Left = tabTelexList(0).Left
    tabTelexList(2).Height = tabTelexList(0).Height - RcvButtonPanel(0).Height - 45
    tabTelexList(2).Width = tabTelexList(0).Width
    
    RcvButtonPanel(1).Top = tabTelexList(1).Top + 30
    RcvButtonPanel(1).Left = tabTelexList(1).Left - 15
    tabTelexList(3).Top = tabTelexList(1).Top + RcvButtonPanel(1).Height + 45
    tabTelexList(3).Left = tabTelexList(1).Left
    tabTelexList(3).Height = tabTelexList(1).Height - RcvButtonPanel(1).Height - 45
    tabTelexList(3).Width = tabTelexList(1).Width
    
    If MainIsDeploy Then
        chkAssignBar(0).Top = chkTlxTitleBar(0).Top
        chkAssignBar(1).Top = chkTlxTitleBar(0).Top
        chkAssignBar(0).Left = txtTelexList(1).Left + 15
        chkAssignBar(0).Width = txtTelexList(1).Width - chkAssignBar(1).Width - 30
        chkAssignBar(1).Left = chkAssignBar(0).Left + chkAssignBar(0).Width + 15
        tabAssign(0).Left = txtTelexList(1).Left + 15
        tabAssign(0).Top = txtTelexList(1).Top + AssignButtonPanel.Height + 60
        tabAssign(0).Height = txtTelexList(1).Height - AssignButtonPanel.Height - 90
        tabAssign(0).Width = txtTelexList(1).Width - 45
        AssignButtonPanel.Top = RcvButtonPanel(0).Top
        AssignButtonPanel.Left = txtTelexList(1).Left
    End If
    
End Sub
Private Function ArrangeFolderTabs(LastPanelTop As Long) As Long
    Dim VisibleTabs As Integer
    Dim CurTabList As String
    Dim NewTabList As String
    Dim NewSize As Long
    Dim NewTop As Long
    Dim NewPanelTop As Long
    Dim NewLeft As Long
    Dim TabsCount As Integer
    Dim RowCount As Integer
    Dim LastVisible As Integer
    Dim TabIsVisible As Boolean
    Dim i As Integer
    NewPanelTop = LastPanelTop
    If MySetUp.FolderMode.Value = 1 Then
        NewTabList = ""
        CurTabList = chkScroll.Tag
        NewSize = UfisFolder.Width - 120
        VisibleTabs = Int(NewSize / fraFolder(0).Width)
        If (VisibleTabs > 2) Or (Not fraFolder(0).Visible) Then
            LastVisible = -1
            TabsCount = 0
            RowCount = 0
            NewTop = fraFolder(0).Top
            NewLeft = 90
            NewTabList = ""
            'For i = 0 To fraFolder.UBound
            For i = 0 To AllTypesCol
                TabIsVisible = False
                If chkScroll.Value = 0 Then
                    If (i >= FolderScroll.Value) And (RowCount < MaxFolderRows) Then TabIsVisible = True
                Else
                    If GetRealItem(CurTabList, CLng(i), ",") = "H" Then TabIsVisible = True
                End If
                If TabIsVisible Then
                    fraFolder(i).Top = NewTop
                    fraFolder(i).Left = NewLeft
                    fraFolder(i).Visible = True
                    If chkScroll.Value = 0 Then NewTabList = NewTabList & "V,"
                    LastVisible = i
                    NewLeft = NewLeft + fraFolder(i).Width
                    TabsCount = TabsCount + 1
                    If TabsCount >= VisibleTabs Then
                        RowCount = RowCount + 1
                        NewLeft = 90
                        NewTop = fraFolder(i).Top + fraFolder(i).Height + 15
                        TabsCount = 0
                    End If
                Else
                    fraFolder(i).Visible = False
                    If chkScroll.Value = 0 Then NewTabList = NewTabList & "H,"
                End If
            Next
            If LastVisible >= 0 Then NewPanelTop = UfisFolder.Top + fraFolder(LastVisible).Top + fraFolder(LastVisible).Height - 15
        End If
        If NewTabList <> "" Then
            chkScroll.Tag = NewTabList
            If InStr(NewTabList, "H") > 0 Then chkScroll.Enabled = True Else chkScroll.Enabled = False
        End If
        fraFolderLine(1).ZOrder
        fraFolderLine(2).ZOrder
    End If
    ArrangeFolderTabs = NewPanelTop
End Function

Private Sub fraSplitter_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    fraSplitter(0).Tag = X
    fraSplitter(2).Tag = Y
End Sub

Private Sub fraSplitter_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim ilDiffX As Long
    Dim ilDiffY As Long
    Dim leftWidth As Long
    Dim rightWidth As Long
    Dim topHeight As Long
    Dim bottomHeight As Long
    Dim ilDone As Boolean
    
    If Button > 0 Then
        If Index >= 3 Then
            If (fraSplitter(0).Tag > 0) And (X <> fraSplitter(0).Tag) Then
                ilDiffX = X - fraSplitter(0).Tag
                leftWidth = txtTelexText.Width + ilDiffX
                rightWidth = fraFlightList.Width - ilDiffX
                If (leftWidth > 690) And (rightWidth > 690) Then
                    fraSplitter(3).Left = fraSplitter(3).Left + ilDiffX
                    fraSplitter(4).Left = fraSplitter(3).Left - 30
                    fraFlightList.Left = fraFlightList.Left + ilDiffX
                    fraFlightList.Width = fraFlightList.Width - ilDiffX
                    tabFlightList(0).Width = fraFlightList.Width - 75
                    tabFlightList(1).Width = tabFlightList(0).Width
                    tabFlightList(2).Width = tabFlightList(0).Width
                    txtTelexText.Width = txtTelexText.Width + ilDiffX
                    chkFlightBar(0).Width = fraFlightList.Width - chkFlightBar(1).Width - 90
                    chkFlightBar(1).Left = chkFlightBar(0).Width + 45
                    Me.Refresh
                    ilDone = True
                End If
            End If
        End If
        If Index <= 1 Then
            If (fraSplitter(0).Tag > 0) And (X <> fraSplitter(0).Tag) Then
                ilDiffX = X - fraSplitter(0).Tag
                leftWidth = txtTelexList(2).Width + ilDiffX
                rightWidth = txtTelexList(3).Width - ilDiffX
                If (leftWidth > 690) And (rightWidth > 690) Then
                    fraSplitter(0).Left = fraSplitter(0).Left + ilDiffX
                    fraSplitter(1).Left = fraSplitter(1).Left + ilDiffX
                    txtTelexList(2).Width = txtTelexList(2).Width + ilDiffX
                    txtTelexList(3).Left = txtTelexList(3).Left + ilDiffX
                    txtTelexList(3).Width = txtTelexList(3).Width - ilDiffX
                    ResizeTabs
                    Me.Refresh
                    ilDone = True
                End If
            End If
        End If
        If (Index >= 1) And (Index <> 3) Then
            If fraSplitter(2).Tag > 0 And Y <> fraSplitter(2).Tag Then
                ilDiffY = Y - fraSplitter(2).Tag
                If chkTlxTitleBar(2).Tag = "" Then
                    topHeight = txtTelexList(2).Height + ilDiffY
                ElseIf chkTlxTitleBar(3).Tag = "" Then
                    topHeight = txtTelexList(3).Height + ilDiffY
                Else
                    topHeight = fraSplitter(0).Height + ilDiffY
                End If
                bottomHeight = txtTelexText.Height - ilDiffY
                If (topHeight > 750) And (bottomHeight > 465) Then
                    fraSplitter(0).Height = fraSplitter(0).Height + ilDiffY
                    fraSplitter(1).Top = fraSplitter(1).Top + ilDiffY
                    fraSplitter(4).Top = fraSplitter(1).Top + 60
                    fraSplitter(2).Top = fraSplitter(2).Top + ilDiffY
                    txtTelexList(2).Height = txtTelexList(2).Height + ilDiffY
                    txtTelexList(3).Height = txtTelexList(3).Height + ilDiffY
                    ResizeTabs
                    Me.Refresh
                    txtTelexText.Top = txtTelexText.Top + ilDiffY
                    txtTelexText.Height = txtTelexText.Height - ilDiffY
                    fraSplitter(3).Top = txtTelexText.Top
                    fraSplitter(3).Height = txtTelexText.Height
                    If chkFlightBar(1).Tag = "" Then
                        fraFlightList.Top = txtTelexText.Top - 90
                        fraFlightList.Height = txtTelexText.Height + 90
                        topHeight = fraFlightList.Height - tabFlightList(0).Top - 45
                        If topHeight > 750 Then
                            tabFlightList(0).Height = topHeight
                            tabFlightList(1).Height = topHeight
                            tabFlightList(2).Height = topHeight
                        End If
                    End If
                    ilDone = True
                    Me.Refresh
                End If
            End If
        End If
    End If
    If Button < 0 Then ilDone = True
    If (chkShowTelex(0).Value = 1) And (ilDone = True) Then
        If Index = 0 Then
            ilDiffX = fraSplitter(0).Left - fraSplitter(3).Left
            If ilDiffX <> 0 Then
                fraSplitter(3).Left = fraSplitter(3).Left + ilDiffX
                fraSplitter(4).Left = fraSplitter(3).Left - 30
                fraFlightList.Left = fraFlightList.Left + ilDiffX
                fraFlightList.Width = fraFlightList.Width - ilDiffX
                tabFlightList(0).Width = fraFlightList.Width - 75
                tabFlightList(1).Width = tabFlightList(0).Width
                tabFlightList(2).Width = tabFlightList(0).Width
                txtTelexText.Width = txtTelexText.Width + ilDiffX
                chkFlightBar(0).Width = fraFlightList.Width - chkFlightBar(1).Width - 90
                chkFlightBar(1).Left = chkFlightBar(0).Width + 45
                Me.Refresh
            End If
        End If
        If Index = 3 Then
            ilDiffX = fraSplitter(3).Left - fraSplitter(0).Left
            If ilDiffX <> 0 Then
                fraSplitter(0).Left = fraSplitter(0).Left + ilDiffX
                fraSplitter(1).Left = fraSplitter(1).Left + ilDiffX
                txtTelexList(2).Width = txtTelexList(2).Width + ilDiffX
                txtTelexList(3).Left = txtTelexList(3).Left + ilDiffX
                txtTelexList(3).Width = txtTelexList(3).Width - ilDiffX
                ResizeTabs
                Me.Refresh
            End If
        End If
    End If
End Sub
Private Sub fraSplitter_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    fraSplitter(0).Tag = -1
    fraSplitter(2).Tag = -1
End Sub
Public Sub LoadTelexData(MemIdx As Integer, tlxSqlKey As String, TfrSqlKey As String, ChooseFolder As String, ClickOnList As String)
    Dim ReqLoop As Long
    Dim ActSqlKey As String
    Dim UseSqlKey As String
    Dim tmpCmd As String
    Dim tmpTable As String
    Dim tmpFields As String
    Dim tmpData As String
    Dim Line As String
    Dim i As Long
    Dim RetCode As Boolean
    Dim TabLineCount As Long
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim tmpErrMsg As String
    Dim sbText1 As String
    Dim sbText2 As String
    Dim tmpLoopTimes As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim SqlTimeBegin
    Dim SqlTimeEnd
    Dim SqlTimeMax
    Dim ActResult As String
    Dim ActCmd As String
    Dim ActTable As String
    Dim ActFldLst As String
    Dim ActCondition As String
    Dim ActOrder As String
    Dim ActDatLst As String
    Dim RetVal As Integer
    Dim tmpDatFrom As String
    Dim tmpDatTo As String
    Dim TlxUrnoList As String
    Dim count As Integer
    
    If MemIdx <= optMem.UBound Then
        MyStatusBar.Panels(1).Text = ""
        MyStatusBar.Panels(2).Text = ""
        sbText1 = MyStatusBar.Panels(1).Text
        sbText2 = MyStatusBar.Panels(2).Text
        If Me.Visible <> True Then Me.Show
        If Me.WindowState = vbMinimized Then Me.WindowState = vbNormal
        Me.Refresh
        TelexPoolHead.MousePointer = 11
        optMem(MemIdx).Tag = -1
        optMem(MemIdx).Value = True     'Changes global CurMem also
        ResetTlxTypeFolder
        tabTelexList(0).ResetContent
        tabTelexList(1).ResetContent
        tabTelexList(0).Refresh
        tabTelexList(1).Refresh
        txtTelexText.Text = ""
        DataPool.TelexData(MemIdx).ResetContent
        For i = 0 To chkTabs.UBound
            chkTabs(i).Caption = ""
        Next
        NewFilterValues(MemIdx).TlxKey = tlxSqlKey
        NewFilterValues(MemIdx).TfrKey = TfrSqlKey
    Else
        DataPool.TelexData(MemIdx).ResetContent
    End If
    If Len(AddFolderList) > 0 And Len(ClickOnList) = 0 Then
        TlxUrnoList = ""
        tmpDatFrom = Mid(txtDateFrom.Text, 7, 4) & Mid(txtDateFrom.Text, 4, 2) & Mid(txtDateFrom.Text, 1, 2)
        tmpDatTo = Mid(txtDateTo.Text, 7, 4) & Mid(txtDateTo.Text, 4, 2) & Mid(txtDateTo.Text, 1, 2)
        If tmpDatFrom <> tmpDatTo Then
            tmpDatFrom = "WHERE (DATC = '" & tmpDatFrom & "' OR DATC = '" & tmpDatTo & "')"
        Else
            tmpDatFrom = "WHERE DATC = '" & tmpDatFrom & "'"
        End If
        TlxUrnoList = ""
        ActResult = ""
        ActCmd = "RTA"
        ActTable = "TFNTAB"
        ActFldLst = "DISTINCT TURN"
        ActCondition = tmpDatFrom
        ActOrder = ""
        ActDatLst = ""
        RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, False, False)
        If RetVal >= 0 Then
            count = UfisServer.DataBuffer(0).GetLineCount - 1
            If count >= 0 Then
                For i = 0 To count
                    TlxUrnoList = TlxUrnoList & UfisServer.DataBuffer(0).GetLineValues(i) & ","
                Next
                TlxUrnoList = Left(TlxUrnoList, Len(TlxUrnoList) - 1)
                'TlxUrnoList = "WHERE URNO IN (" & TlxUrnoList & ")"
            End If
        End If
    End If
    tmpFields = DataPool.TlxTabFields.Text
    tmpData = ""
    tmpCmd = "RTA"
    tmpTable = DataPool.TableName.Text
    ActSqlKey = tlxSqlKey
    tmpErrMsg = ""
    Me.Refresh
    For ReqLoop = 1 To 2
        If ActSqlKey <> "" Then
            MyStatusBar.Panels(1).Text = "Loading Data ..."
            If ReqLoop = 1 Then
                fraLoadProgress.Caption = "Loading Telexes ..."
                MyStatusBar.Panels(2).Text = "Valid Telexes ..."
            Else
                MyStatusBar.Panels(2).Text = "Assigned Telexes ..."
                fraLoadProgress.Caption = "Loading Assigned Telexes ..."
            End If
            DataPool.TelexData(MemIdx).CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
            DataPool.TelexData(MemIdx).CedaHopo = UfisServer.HOPO
            DataPool.TelexData(MemIdx).CedaIdentifier = "IDX"
            DataPool.TelexData(MemIdx).CedaPort = "3357"
            DataPool.TelexData(MemIdx).CedaReceiveTimeout = "250"
            DataPool.TelexData(MemIdx).CedaRecordSeparator = vbLf
            DataPool.TelexData(MemIdx).CedaSendTimeout = "250"
            DataPool.TelexData(MemIdx).CedaServerName = UfisServer.HostName
            DataPool.TelexData(MemIdx).CedaTabext = UfisServer.TblExt
            DataPool.TelexData(MemIdx).CedaUser = UfisServer.ModName
            DataPool.TelexData(MemIdx).CedaWorkstation = UfisServer.GetMyWorkStationName
            CedaStatus(0).Visible = True
            CedaStatus(0).ZOrder
            CedaStatus(0).Refresh
            If CedaIsConnected Then
                UseSqlKey = GetItem(ActSqlKey, 1, "[LOOPTIMES]")
                tmpLoopTimes = GetItem(ActSqlKey, 2, "[LOOPTIMES]")
                If tmpLoopTimes <> "" Then
                    ActSqlKey = UseSqlKey
                    tmpVpfr = GetItem(tmpLoopTimes, 1, ",")
                    tmpVpto = GetItem(tmpLoopTimes, 2, ",")
                    SqlTimeBegin = CedaFullDateToVb(tmpVpfr)
                    SqlTimeMax = CedaFullDateToVb(tmpVpto)
                    While (SqlTimeBegin <= SqlTimeMax) And (Not StopLoading)
                        If Not StopLoading Then
                            SqlTimeEnd = DateAdd("n", 1439, SqlTimeBegin)
                            If SqlTimeEnd > SqlTimeMax Then SqlTimeEnd = SqlTimeMax
                            tmpVpfr = Format(SqlTimeBegin, "yyyymmddhhmm")
                            tmpVpto = Format(SqlTimeEnd, "yyyymmddhhmm")
                            txtLoadFrom.Text = Format(SqlTimeBegin, MySetUp.DefDateFormat)
                            txtLoadTo.Text = Format(SqlTimeEnd, MySetUp.DefDateFormat)
                            txtLoadTimeFrom.Text = Format(SqlTimeBegin, "hh:mm")
                            txtLoadTimeTo.Text = Format(SqlTimeEnd, "hh:mm")
                            MaxLine = DataPool.TelexData(MemIdx).GetLineCount
                            fraLoadProgress.Caption = CStr(MaxLine) & " Telexes Loaded ..."
                            Me.Refresh
                            DoEvents
                            UseSqlKey = ActSqlKey
                            UseSqlKey = Replace(UseSqlKey, "[LOOPVPFR]", tmpVpfr, 1, -1, vbBinaryCompare)
                            UseSqlKey = Replace(UseSqlKey, "[LOOPVPTO]", tmpVpto, 1, -1, vbBinaryCompare)
                            RetCode = DataPool.TelexData(MemIdx).CedaAction(tmpCmd, tmpTable, tmpFields, "", UseSqlKey)
                            If (RetCode = False) And (tmpErrMsg = "") Then tmpErrMsg = tmpErrMsg & DataPool.TelexData(MemIdx).GetLastCedaError
                            Me.Refresh
                            SqlTimeBegin = DateAdd("n", 1, SqlTimeEnd)
                        End If
                    Wend
                Else
                    Me.Refresh
                    RetCode = DataPool.TelexData(MemIdx).CedaAction(tmpCmd, tmpTable, tmpFields, "", UseSqlKey)
                    If RetCode = False Then tmpErrMsg = tmpErrMsg & DataPool.TelexData(MemIdx).GetLastCedaError
                    Me.Refresh
                End If
            Else
                If SaveLocalTestData Then
                    If (UfisServer.HostName = "LOCAL") And (ReqLoop = 1) Then
                        DataPool.LoadTestData MemIdx
                    End If
                End If
            End If
            CedaStatus(0).Visible = False
            MyStatusBar.Panels(1).Text = sbText1
            MyStatusBar.Panels(2).Text = sbText2
            Me.Refresh
        End If
        If ReqLoop = 1 Then TabLineCount = DataPool.TelexData(MemIdx).GetLineCount
        ActSqlKey = TfrSqlKey
        If StopLoading Then Exit For
    Next
    If Len(TlxUrnoList) > 0 And CedaIsConnected Then
        TlxUrnoList = CheckTlxUrnolist(TlxUrnoList, MemIdx)
        If Len(TlxUrnoList) > 0 Then
            RetCode = DataPool.TelexData(MemIdx).CedaAction(tmpCmd, "TLXTAB", tmpFields, "", TlxUrnoList)
        End If
    End If
    If MemIdx <> CurMem Then
        If MemIdx <= optMem.UBound Then
            DontRefreshList = True
            optMem(MemIdx).Tag = -1
            optMem(MemIdx).Value = True     'Changes global CurMem also
            DontRefreshList = False
            'CheckLoadButton False
        End If
    End If
    If Not StopLoading Then
        If tmpErrMsg <> "" Then
            tmpErrMsg = Replace(tmpErrMsg, vbLf, vbNewLine, 1, -1, vbBinaryCompare)
            MyMsgBox.CallAskUser 0, 0, 0, "Server Access Report", tmpErrMsg, "stop", "", UserAnswer
        End If
        If UfisServer.HostName <> "LOCAL" Then
            If SaveLocalTestData Then DataPool.SaveTestData MemIdx
        End If
        If MemIdx <= optMem.UBound Then
            MaxLine = DataPool.TelexData(MemIdx).GetLineCount
            MyStatusBar.Panels(2).Text = "Preparing Telex Folder View ... "
            fraLoadProgress.Caption = CStr(MaxLine) & " Telexes Loaded ..."
            Me.Refresh
            DoEvents
            MaxLine = MaxLine - 1
            For CurLine = TabLineCount To MaxLine
                DataPool.TelexData(MemIdx).SetLineStatusValue CurLine, 1
            Next
            If optMem(0).Value = False Then
                ReadFolderCount = False
            End If
            PrepareTelexExtract MemIdx
            ReadFolderCount = True
            Me.Refresh
            DoEvents
            If ChooseFolder <> "" Then
                MyStatusBar.Panels(2).Text = "Publishing Telex Folder Data ... "
                InitFolderByName ChooseFolder, False
            End If
            MyStatusBar.Panels(2).Text = ""
            Me.Refresh
            DoEvents
            If ClickOnList <> "" Then
                If ClickOnList = "RCV" Then
                    tabTelexList(0).SetCurrentSelection 0
                ElseIf ClickOnList = "SND" Then
                    tabTelexList(1).SetCurrentSelection 0
                End If
            Else
                If tabTelexList(0).GetLineCount > 0 Then
                    tabTelexList(0).SetCurrentSelection 0
                ElseIf tabTelexList(1).GetLineCount > 0 Then
                    tabTelexList(1).SetCurrentSelection 0
                End If
            End If
            MaxLine = DataPool.TelexData(MemIdx).GetLineCount
            optMem(MemIdx).Tag = MaxLine
            Select Case optMem(MemIdx).Tag
                Case Is < 0
                    optMem(MemIdx).ToolTipText = tmpErrMsg
                Case Is = 0
                    optMem(MemIdx).ToolTipText = "No data found for that filter."
                Case Else
                    optMem(MemIdx).ToolTipText = optMem(MemIdx).Tag & " Telexes "
            End Select
            StoreFolderValues MemIdx
            ActFilterValues(MemIdx) = NewFilterValues(MemIdx)
            If Left(NewFilterValues(MemIdx).TlxKey, 11) = "WHERE URNO=" Then
                chkLoad.BackColor = vbButtonFace
                chkLoad.ForeColor = vbBlack
            End If
        End If
    Else
        DataPool.TelexData(MemIdx).ResetContent
        chkLoad.Value = 0
    End If
    Me.Refresh
    TelexPoolHead.MousePointer = 0
End Sub

Private Sub chkTlxTitleBar_Click(Index As Integer)
    Dim tmpTag As String
    Dim tmpTabIdx As Integer
    Dim tmpTxtIdx As Integer
    Dim tmpWinIdx As Integer
    If chkLoad.Value = 0 Then
        If Index < 2 Then
            If MainIsDeploy = False Then
                If MySetUp.SndTabList.Value = 1 Then
                    chkTlxTitleBar(1 - Index).Value = 1 - chkTlxTitleBar(Index).Value
                    If chkOnlineRcv.Value = 0 Then
                        FillTelexTextField (-1)
                    End If
                    If MySetUp.FolderMode.Value = 1 Then
                        If chkTlxTitleBar(0).Value = 0 And bmFolderButtons0 = True Then
                            FolderButtons(0).Visible = True
                        Else
                            FolderButtons(0).Visible = False
                        End If
                    End If
                Else
                    If MySetUp.FolderMode.Value = 0 Then FolderButtons(0).Top = fraListPanel.Top + 15
                    If bmFolderButtons0 = True Then FolderButtons(0).Visible = True
                    If chkTlxTitleBar(0).Value = 1 Then chkTlxTitleBar(0).Value = 0
                End If
            Else
                chkTlxTitleBar(Index).Value = 0
            End If
        Else
            If chkTlxTitleBar(Index).Value = 1 Then
                tmpTag = chkTlxTitleBar(Index).Tag
                Select Case Index
                    Case 2
                        tmpTxtIdx = 0
                        tmpWinIdx = 2
                        If chkOnlineRcv.Value = 0 Then
                            tmpTabIdx = 0
                        Else
                            tmpTabIdx = 2
                        End If
                    Case 3
                        tmpTxtIdx = 1
                        tmpWinIdx = 3
                        If chkOnlineRcv.Value = 0 Then
                            tmpTabIdx = 1
                        Else
                            tmpTabIdx = 3
                        End If
                    Case Else
                End Select
                If tmpTag = "" Then
                    chkTlxTitleBar(Index).Tag = "+"
                    chkTlxTitleBar(Index).Caption = " -"
                    txtTelexList(tmpTxtIdx).ZOrder
                    RcvButtonPanel(0).ZOrder
                    RcvButtonPanel(1).ZOrder
                    tabTelexList(tmpTabIdx).ZOrder
                    fraSplitter(0).ZOrder
                    Form_Resize
                Else
                    chkTlxTitleBar(Index).Tag = ""
                    chkTlxTitleBar(Index).Caption = " +"
                    ResizeTabs
                    ArrangeTabCursor tabTelexList(tmpTabIdx)
                End If
                chkTlxTitleBar(Index).Value = 0
            End If
        End If
    End If
End Sub

Private Sub FullText_Click()
    If FullText.Value = 1 Then FullText.BackColor = LightGreen Else FullText.BackColor = vbButtonFace
    FillTelexTextField (-1)
End Sub

Private Sub LoaInfo_Click(Index As Integer)
    If LoaInfo(Index).Value = 1 Then
        If Trim(CurrentUrno.Text) <> "" Then
            CheckLoaTabViewer CurrentRecord.Text, Index
        Else
            MyMsgBox.CallAskUser 0, 0, 0, "Show " & LoaInfo(Index).Caption, "No telex selected.", "hand", "", UserAnswer
        End If
        LoaInfo(Index).Value = 0
    End If
End Sub

Private Sub MsgFromClient_Change()
    Dim tmpData As String
    Dim ImpMsgNum As Long
    Dim ImpPakNum As Long
    Dim tmpVal As Integer
    Dim tmpList As String
    Dim ImpCmd As String
    Dim FldSep As String
    Dim LineNo As Long
    tmpData = MsgFromClient.Text
    If tmpData <> "" Then
        FldSep = Chr(15)
        ImpMsgNum = Val(GetItem(tmpData, 1, FldSep))
        ImpPakNum = Val(GetItem(tmpData, 2, FldSep))
        If ImpPakNum > 0 Then
        Else
            ImpCmd = GetItem(tmpData, 3, FldSep)
            Select Case ImpCmd
                Case "SPOOL"
                Case "ACTIV"
                Case "CLOSE"
                Case "ATSET"
                    RcvInfo.chkImp(0).Value = 1
                Case "ATOFF"
                    RcvInfo.chkImp(0).Value = 0
                Case "AISET"
                    RcvInfo.chkImp(1).Value = 1
                Case "AIOFF"
                    RcvInfo.chkImp(1).Value = 0
                Case "ERROR"
                    tmpList = GetItem(tmpData, 4, FldSep)
                    HandleImpUpdate tmpList, "STAT", "E"
                Case "IMPORTED"
                    tmpList = GetItem(tmpData, 4, FldSep)
                    HandleImpUpdate tmpList, "WSTA", "I"
                Case "MANUALLY"
                    tmpList = GetItem(tmpData, 4, FldSep)
                    HandleImpUpdate tmpList, "WSTA", "M"
                Case Else
            End Select
            MsgToClient.Text = "GOT:" & GetItem(tmpData, 1, FldSep)
        End If
        'LastMsgNum = ImpMsgNum + 1
    End If
End Sub
Private Sub HandleImpUpdate(UrnoList As String, UseField As String, UseValue As String)
    Dim RetCode As Integer
    Dim RetVal As String
    Dim clSqlKey As String
    Dim tmpUrno As String
    Dim tmpStat As String
    Dim itm As Integer
    itm = 1
    tmpUrno = GetItem(UrnoList, itm, ";")
    While tmpUrno <> ""
        clSqlKey = "WHERE URNO=" & tmpUrno
        RetCode = UfisServer.CallCeda(RetVal, "URT", DataPool.TableName, UseField, UseValue, clSqlKey, "", 0, True, False)
        itm = itm + 1
        tmpUrno = GetItem(UrnoList, itm, ";")
    Wend
End Sub
Private Sub MyStatusBar_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = 2 Then
        ShowFdiInfo 2, False
    End If
End Sub

Private Sub MyStatusBar_PanelDblClick(ByVal Panel As MSComctlLib.Panel)
    ShowFdiInfo Panel.Index, True
End Sub

Private Sub ShowFdiInfo(Index As Integer, SsimDate As Boolean)
    Dim tmpTag As String
    Dim tmpFields As String
    Dim tmpData As String
    Dim tmpText As String
    Dim tmpInfo As String
    tmpTag = MyStatusBar.Panels(Index).Tag
    Select Case Index
        Case 1
            'MsgBox tmpTag
        Case 2
            tmpFields = GetRealItem(tmpTag, 0, "/")
            tmpData = GetRealItem(tmpTag, 1, "/")
            tmpFields = Replace(tmpFields, "#", ",", 1, -1, vbBinaryCompare)
            tmpData = Replace(tmpData, "#", ",", 1, -1, vbBinaryCompare)
            tmpText = PrepareFdiInfo(tmpFields, tmpData, SsimDate)
            If tmpText <> "" Then
                tmpInfo = MyStatusBar.Panels(1).Text
                If Left(tmpInfo, 1) = "(" Then tmpInfo = "(No flight assigned)"
                tmpText = "Updates on Flight" & vbNewLine & tmpInfo & vbNewLine & vbNewLine & tmpText
                MyMsgBox.CallAskUser 0, 0, 0, "Telex Interpreter", tmpText, "hand", "", UserAnswer
            End If
        Case Else
    End Select
End Sub
Private Function PrepareFdiInfo(FieldList As String, DataList As String, SsimDate As Boolean) As String
    Dim Result As String
    Dim CurItm As Long
    Dim CurFld As String
    Dim CurDat As String
    Dim tmpDat As String
    Dim tmpTime
    Result = ""
    CurItm = 0
    CurFld = GetRealItem(FieldList, CurItm, ",")
    While CurFld <> ""
        CurDat = GetRealItem(DataList, CurItm, ",")
        If CurDat <> "" Then
            If InStr(FdiTimeFields, CurFld) > 0 Then
                If SsimDate Then
                    tmpTime = CedaFullDateToVb(CurDat)
                    CurDat = Format(tmpTime, "ddmmmyy \/ hh:mm")
                    CurDat = UCase(CurDat)
                Else
                    tmpDat = Left(CurDat, 4) & "." & Mid(CurDat, 5, 2) & "." & Mid(CurDat, 7, 2) & " / "
                    tmpDat = tmpDat & Mid(CurDat, 9, 2) & ":" & Mid(CurDat, 11, 2)
                    CurDat = tmpDat
                End If
            End If
        Else
            CurDat = "--"
        End If
        Result = Result & CurFld & ": " & CurDat & vbNewLine
        CurItm = CurItm + 1
        CurFld = GetRealItem(FieldList, CurItm, ",")
    Wend
    PrepareFdiInfo = Result
End Function
Private Sub OnLine_Click()
    If OnLine.Value = 1 Then
        OnLine.BackColor = LightGreen
        If PoolConfig.OnLineCfg(1).Value = 1 Then
            RcvTelex.Show
            RcvTelex.WindowState = vbNormal
        Else
            If FormIsVisible("RcvTelex") Then RcvTelex.Hide
        End If
        If PoolConfig.OnLineCfg(2).Value = 1 Then
            SndTelex.Show
            SndTelex.WindowState = vbNormal
        Else
            If FormIsVisible("SndTelex") Then SndTelex.Hide
        End If
        If PoolConfig.OnLineCfg(3).Value = 1 Then
            OutTelex.Show
            OutTelex.WindowState = vbNormal
        Else
            If FormIsVisible("OutTelex") Then OutTelex.Hide
        End If
        If PoolConfig.OnLineCfg(4).Value = 1 Then
            RcvInfo.Show
            RcvInfo.WindowState = vbNormal
        Else
            If FormIsVisible("RcvInfo") Then RcvInfo.Hide
        End If
    Else
        OnLine.BackColor = vbButtonFace
        If FormIsVisible("RcvTelex") Then RcvTelex.Hide
        If FormIsVisible("SndTelex") Then SndTelex.Hide
        If FormIsVisible("OutTelex") Then OutTelex.Hide
        If FormIsVisible("RcvInfo") Then RcvInfo.Hide
    End If
End Sub

Private Sub OnTop_Click()
    If OnTop.Value = 1 Then OnTop.BackColor = LightGreen Else OnTop.BackColor = vbButtonFace
    If OnTop.Value <> 1 Then SetFormOnTop Me, False
    SetAllFormsOnTop True
End Sub

Private Sub optLookAft_Click()
    optLookAft.BackColor = LightestGreen
    optLookTlx.BackColor = vbButtonFace
    If Not ReorgFolders Then CheckLoadButton False
End Sub

Private Sub optLookTlx_Click()
    optLookTlx.BackColor = LightestGreen
    optLookAft.BackColor = vbButtonFace
    If Not ReorgFolders Then CheckLoadButton False
End Sub

Private Sub optMem_Click(Index As Integer)
    On Error Resume Next
    If optMem(Index).Value = True Then
        chkOnlineRcv.Value = 0
        optMem(optMemButtons.Tag).BackColor = vbButtonFace
        StoreFolderValues CurMem
        CurMem = Index
        Me.Refresh
        DoEvents
        RestoreFolderValues CurMem, True
        optMem(Index).BackColor = LightGreen
        optMemButtons.Tag = Index
        If Left(NewFilterValues(Index).TlxKey, 11) = "WHERE URNO=" Then
            chkLoad.BackColor = vbButtonFace
            chkLoad.ForeColor = vbBlack
        Else
            CheckLoadButton False
        End If
    End If
End Sub

Private Sub optLocal_Click()
    Dim Vpfr As String
    Dim Vpto As String
    Dim TimeVal
    optLocal.BackColor = LightestGreen
    optUtc.BackColor = vbButtonFace
    GetValidTimeFrame Vpfr, Vpto, False
    TimeVal = CedaFullDateToVb(Vpfr)
    TimeVal = DateAdd("n", UtcTimeDiff, TimeVal)
    txtDateFrom = Format(TimeVal, MySetUp.DefDateFormat)
    txtTimeFrom = Format(TimeVal, "hh:mm")
    TimeVal = CedaFullDateToVb(Vpto)
    TimeVal = DateAdd("n", UtcTimeDiff, TimeVal)
    txtDateTo = Format(TimeVal, MySetUp.DefDateFormat)
    txtTimeTo = Format(TimeVal, "hh:mm")
    SetTabUtcDiff UtcTimeDiff
End Sub

Private Sub optTlxAddr_Click()
    If MySetUp.AddrFilter.Value = 1 Then
        optTlxAddr.BackColor = LightestGreen
    Else
        optTlxAddr.BackColor = LightestGreen
    End If
    txtAddr.ToolTipText = "In the Telex Header"
    optTlxText.BackColor = vbButtonFace
    CheckLoadButton False
End Sub

Private Sub optTlxText_Click()
    optTlxText.BackColor = LightestGreen
    optTlxAddr.BackColor = vbButtonFace
    txtAddr.ToolTipText = "Somewhere in the Telex Data"
    CheckLoadButton False
End Sub

Private Sub optUtc_Click()
    Dim Vpfr As String
    Dim Vpto As String
    Dim TimeVal
    optUtc.BackColor = LightestGreen
    optLocal.BackColor = vbButtonFace
    GetValidTimeFrame Vpfr, Vpto, False
    TimeVal = CedaFullDateToVb(Vpfr)
    TimeVal = DateAdd("n", -UtcTimeDiff, TimeVal)
    txtDateFrom = Format(TimeVal, MySetUp.DefDateFormat)
    txtTimeFrom = Format(TimeVal, "hh:mm")
    TimeVal = CedaFullDateToVb(Vpto)
    TimeVal = DateAdd("n", -UtcTimeDiff, TimeVal)
    txtDateTo = Format(TimeVal, MySetUp.DefDateFormat)
    txtTimeTo = Format(TimeVal, "hh:mm")
    SetTabUtcDiff 0
End Sub
Private Sub SetTabUtcDiff(SetDiff As Integer)
    Dim CurDiff As Long
    Dim count As Integer
    Dim idx As Integer
    Dim tmpDat As String
    Dim UseServerTime
    
    CurDiff = SetDiff
    tabTelexList(0).DateTimeSetUTCOffsetMinutes 4, CurDiff
    tabTelexList(0).Refresh
    tabTelexList(1).DateTimeSetUTCOffsetMinutes 4, CurDiff
    tabTelexList(1).Refresh
    tabTelexList(2).DateTimeSetUTCOffsetMinutes 4, CurDiff
    tabTelexList(2).Refresh
    tabTelexList(3).DateTimeSetUTCOffsetMinutes 4, CurDiff
    tabTelexList(3).Refresh
    tabFlightList(0).DateTimeSetUTCOffsetMinutes 3, CurDiff
    tabFlightList(0).DateTimeSetUTCOffsetMinutes 10, CurDiff
    tabFlightList(0).Refresh
    tabFlightList(1).DateTimeSetUTCOffsetMinutes 3, CurDiff
    tabFlightList(1).DateTimeSetUTCOffsetMinutes 10, CurDiff
    tabFlightList(1).Refresh
    count = tabFlightList(2).GetLineCount - 1
    If count >= 0 Then
        For idx = 0 To count
            tmpDat = tabFlightList(2).GetColumnValue(idx, 6)
            If Len(tmpDat) > 1 Then
                UseServerTime = CedaFullDateToVb(tmpDat)
                If optLocal.Value = True Then
                    UseServerTime = DateAdd("n", UtcTimeDiff, UseServerTime)
                End If
                tmpDat = Format(UseServerTime, "ddmmmyy\/hh:mm")
                tmpDat = UCase(tmpDat)
                tabFlightList(2).SetColumnValue idx, 2, tmpDat
            End If
        Next
    tabFlightList(2).Refresh
    End If
    
    If FormIsLoaded("RcvInfo") Then
        RcvInfo.tabTelexList(0).DateTimeSetUTCOffsetMinutes 4, CurDiff
        RcvInfo.tabTelexList(0).Refresh
    End If
    If FormIsLoaded("RcvTelex") Then
        RcvTelex.tabTelexList(0).DateTimeSetUTCOffsetMinutes 4, CurDiff
        RcvTelex.tabTelexList(0).Refresh
    End If
    If FormIsLoaded("SndTelex") Then
        SndTelex.tabTelexList.DateTimeSetUTCOffsetMinutes 6, CurDiff
        SndTelex.tabTelexList.DateTimeSetUTCOffsetMinutes 7, CurDiff
        SndTelex.tabTelexList.DateTimeSetUTCOffsetMinutes 8, CurDiff
        SndTelex.tabTelexList.Refresh
    End If
    If FormIsLoaded("OutTelex") Then
        OutTelex.tabTelexList.DateTimeSetUTCOffsetMinutes 4, CurDiff
        OutTelex.tabTelexList.Refresh
    End If
End Sub
Private Sub CheckLoaTabViewer(TlxTabRec As String, Index As Integer)
    Dim AftUrno As String
    Dim ShowWhat As String
    Dim LoaCode As String
    Dim InfoTag As String
    Dim CmdLine As String
    AftUrno = Trim(GetFieldValue("FLNU", TlxTabRec, DataPool.TlxTabFields))
    If (AftUrno <> "") And (AftUrno <> "0") Then
        InfoTag = LoaInfo(Index).Tag
        LoaCode = GetItem(InfoTag, 1, ",")
        If InStr(InfoTag, ",") > 0 Then
            ShowWhat = Trim(GetFieldValue("TTYP", TlxTabRec, DataPool.TlxTabFields))
            Select Case Index
                Case 0
                    If ShowWhat = "PTM" Then LoaCode = GetItem(InfoTag, 2, ",")
                Case 1
                Case 2
                Case Else
            End Select
        End If
        CmdLine = AftUrno & "," & LoaCode & "," & CStr(MyFontSize) & "," & CStr(CInt(MyFontBold))
        ShowLoaTabViewer CmdLine
    Else
        MyMsgBox.CallAskUser 0, 0, 0, "Show Flight Details", "No relation to a flight.", "hand", "", UserAnswer
    End If
End Sub
Private Sub ShowLoaTabViewer(CmdLineParameter As String)
    Static ViewerPath As String
    Static ViewerAppId 'As Variant
    Dim MsgTxt As String
    Dim RetVal As Integer
    On Error Resume Next
    If ViewerAppId > 0 Then
        Err.Description = ""
        AppActivate ViewerAppId, False
        If Err.Description = "" Then
            SendKeys "%{F4}", True
        End If
    End If
    If CmdLineParameter <> "CLOSE" Then
        If ViewerPath = "" Then
            ViewerPath = GetIniEntry(myIniFullName, "LOATAB_INFO", "", "LOADTAB_VIEWER", "c:\ufis\appl\LoadTabViewer.exe")
            ViewerPath = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "LOADTAB_VIEWER", ViewerPath)
        End If
        Err.Description = ""
        ViewerAppId = Shell(ViewerPath & " " & CmdLineParameter, vbNormalFocus)
        If Err.Description <> "" Then
            MsgTxt = "Program not found:" & vbNewLine & ViewerPath
            RetVal = MyMsgBox.CallAskUser(0, 0, 0, "UFIS Components Control", MsgTxt, "warn1", "", UserAnswer)
        End If
    End If
End Sub

Private Sub ResetFilter_Click()
    If ResetFilter.Value = 1 Then InitDateFields
    ResetFilter.Value = 0
End Sub

Private Sub SpoolTimer_Timer()
    Dim tmpMsg As String
    If (ImportIsConnected) And (InfoFromClient.Text <> "WAIT") Then
        SpoolTimer.Enabled = False
        tmpMsg = InfoSpooler.GetColumnValue(0, 0)
        InfoSpooler.DeleteLine 0
        InfoSpooler.Refresh
        InfoFromClient = "WAIT"
        InfoToClient.Text = tmpMsg
        If InfoSpooler.GetLineCount > 0 Then SpoolTimer.Enabled = True
    End If
End Sub

Private Sub tabAssign_GotFocus(Index As Integer)
    If tabAssign(0).GetLineCount > 0 Then chkTlxTitleBar(1).Value = 0
End Sub

Private Sub tabAssign_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo >= 0 Then
        chkTlxAssign.BackColor = vbRed
        chkTlxAssign.ForeColor = vbWhite
        tabAssign(0).SetColumnValue LineNo, 5, "N"
'        StopOnline = True
    End If
End Sub

Private Sub tabFlightList_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Static lastColNo(2) As Long
    Dim tmpAftUrno As String
    Dim tmpAftFlno As String
    If Index < 2 Then
        If LineNo >= 0 Then
            tmpAftUrno = tabFlightList(Index).GetColumnValue(LineNo, 15)
            tmpAftFlno = "Flight " & tabFlightList(Index).GetColumnValue(LineNo, 2)
            LoadFolderData tmpAftUrno, tmpAftFlno
        Else
            If lastColNo(Index) = ColNo Then
                If tabFlightList(Index).SortOrderASC Then
                    tabFlightList(Index).Sort CStr(ColNo), False, True
                Else
                    tabFlightList(Index).Sort CStr(ColNo), True, True
                End If
            Else
                tabFlightList(Index).Sort CStr(ColNo), True, True
            End If
            tabFlightList(Index).AutoSizeColumns
            lastColNo(Index) = ColNo
        End If
    End If
End Sub

Private Sub tabTelexList_RowSelectionChanged(Index As Integer, ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim idx As Integer
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    
    idx = Index
    If idx > 1 Then idx = idx - 2
    If LineNo >= 0 Then
        If Selected Then
            chkTlxTitleBar(idx).Value = 0
            If Len(LastTlxUrno(Index)) = 0 Or LastTlxUrno(Index) <> tabTelexList(Index).GetColumnValue(LineNo, 7) Then
                LastTlxUrno(Index) = tabTelexList(Index).GetColumnValue(LineNo, 7)
                If Index >= 2 Then
                    If chkOnlineRcv.Value = 1 Then
                        FillTelexTextField (Index)
                    End If
                Else
                    FillTelexTextField (Index)
                End If
                If MainIsOnline And tabTelexList(Index).GetColumnValue(LineNo, 1) = "" Then
                    UpdateTelexStatus "V"
                    tabTelexList(Index).SetColumnValue LineNo, 1, "V"
                    ColorPool.GetStatusColor "V", "", CurForeColor, CurBackColor
                    tabTelexList(Index).SetLineColor LineNo, CurForeColor, CurBackColor
                End If
                If FormIsVisible("AssignFlight") Then chkAssign.Value = 1
                If FormIsVisible("frmTelex") Then frmTelex.txtTelex.Text = txtTelexText.Text
                If MainIsDeploy Then
                    chkLoadAss.Value = 1
                End If
            End If
        End If
    End If
End Sub

Private Sub FillTelexTextField(Index As Integer)
    Dim tmpLen As Integer
    Dim LineNo As Long
    Dim clTlxLin As String
    Dim CurTlxRec As String
    Dim tmpTtyp As String
    Dim tmpStyp As String
    Dim tmpContext As String
    Dim tmpFldDat As String
    Dim tmpSere As String
    Dim tmpRema As String
    Dim tmpFlnu As String
    Dim tmpAftUrno As String
    Dim tmpStat As String
    Dim tmpText As String
    Dim RecNbr As String
    Dim TlxIsFileType As Boolean
    txtTelexText.Text = ""
    txtTelexText = ""
    CurrentUrno = ""
    CurrentRecord = ""
    AtcCSGN.Text = ""
    UseCSGN.Text = ""
    tmpSere = ""
    tmpTtyp = ""
    tmpFlnu = "0"
    If Index = -1 Then
        If chkTlxTitleBar(0).Value = 0 Then
            Index = 0
        ElseIf chkTlxTitleBar(1).Value = 0 Then
            Index = 1
        End If
        If chkOnlineRcv.Value = 1 And Index >= 0 Then Index = Index + 2
    End If
    'TelexTabToDataRelation (CurMem)
    If Index >= 0 Then
        LineNo = tabTelexList(Index).GetCurrentSelected
        If LineNo >= 0 Then
            clTlxLin = tabTelexList(Index).GetLineValues(LineNo)
            If MainIsOnline Then
                RecNbr = GetMemRecNbr(Index, LineNo, CurMem)
            Else
                RecNbr = GetItem(clTlxLin, 9, ",")
            End If
            If RecNbr <> "" Then
                TelexPoolHead.MousePointer = 11
                LineNo = Val(RecNbr)
                CurTlxRec = DataPool.TelexData(CurMem).GetLineValues(LineNo)
                CurrentRecord = CurTlxRec
                tmpSere = GetFieldValue("SERE", CurTlxRec, DataPool.TlxTabFields.Text)
                If tmpSere = "C" Then
                    TlxIsFileType = True
                    clTlxLin = BuildTelexText(CurTlxRec, "TEXT")
                    chkRedirect.Enabled = False
                    chkAssign.Enabled = False
                    chkNew.Caption = "Send"
                Else
                    clTlxLin = GetFieldValue("TXT1", CurTlxRec, DataPool.TlxTabFields.Text) & _
                                GetFieldValue("TXT2", CurTlxRec, DataPool.TlxTabFields.Text)
                    If InStr(clTlxLin, "=TEXT") > 0 Then TlxIsFileType = True
                    chkRedirect.Enabled = True
                    chkAssign.Enabled = True
                    chkNew.Caption = "New"
                End If
                tmpTtyp = Trim(GetFieldValue("TTYP", CurTlxRec, DataPool.TlxTabFields.Text))
                tmpStyp = Trim(GetFieldValue("STYP", CurTlxRec, DataPool.TlxTabFields.Text))
                Select Case tmpTtyp
                    Case "KRIS"
                        tmpText = CleanString(clTlxLin, FOR_CLIENT, False)
                    Case "ATC"
                        If InStr(clTlxLin, "MESSAGE CONT.") > 0 Then
                            clTlxLin = Replace(clTlxLin, Chr(179), Chr(28), 1, -1, vbBinaryCompare)
                            clTlxLin = Replace(clTlxLin, "/", Chr(28), 1, -1, vbBinaryCompare)
                        End If
                        tmpText = CleanString(clTlxLin, FOR_CLIENT, True)
                        tmpFldDat = GetCsgnFromTelex(clTlxLin)
                        UseCSGN.Text = tmpFldDat
                        AtcCSGN.Text = tmpFldDat
                    Case Else
                        tmpText = TextString(clTlxLin, FullText.Value)
                        If FullText.Value = 0 Then
                            If TlxIsFileType Then
                                GetKeyItem tmpContext, clTlxLin, "=SMI", "="
                                tmpContext = CleanString(tmpContext, FOR_CLIENT, True)
                                tmpContext = CleanTrimString(tmpContext)
                                tmpLen = Len(tmpContext)
                                If tmpLen > 0 Then
                                    If InStr(tmpText, (vbLf & tmpContext)) > 0 Then
                                        tmpContext = ""
                                    ElseIf Left(tmpText, tmpLen) = tmpContext Then
                                        tmpContext = ""
                                    Else
                                        tmpContext = tmpContext & vbNewLine
                                    End If
                                End If
                            End If
                        End If
                End Select
                If tmpText = "" Then tmpText = "EMPTY TEXT BODY"
                tmpText = tmpContext & tmpText
                txtTelexText.Text = CleanTrimString(tmpText)
                CurrentUrno.Text = GetFieldValue("URNO", CurTlxRec, DataPool.TlxTabFields.Text)
                TelexPoolHead.MousePointer = 0
                tmpFlnu = GetFieldValue("FLNU", CurTlxRec, DataPool.TlxTabFields.Text)
                tmpAftUrno = tmpFlnu
                If tmpFlnu = "" Then tmpFlnu = "0"
                tmpRema = GetFieldValue("BEME", CurTlxRec, DataPool.TlxTabFields.Text)
                MyStatusBar.Panels(2).Tag = tmpRema
                tmpRema = GetItem(tmpRema, 1, "/")
                tmpRema = Replace(tmpRema, "#", "/", 1, -1, vbBinaryCompare)
                MyStatusBar.Panels(2).Text = tmpRema
                tmpRema = GetFieldValue("MSTX", CurTlxRec, DataPool.TlxTabFields.Text)
                If (tmpRema = "") And (tmpFlnu = "0") Then
                    tmpStat = GetFieldValue("STAT", CurTlxRec, DataPool.TlxTabFields.Text)
                    tmpRema = TlxRcvStat.ExplainStatus("STAT", tmpStat, False)
                    tmpRema = "(" & tmpRema & ")"
                End If
                MyStatusBar.Panels(1).Text = tmpRema
                MyStatusBar.Panels(1).Tag = tmpRema & vbLf & tmpFlnu
                If fraFlightList.Visible Then
                    If chkFlightList(6).Value = 1 Then
                        EditFlightsToTelex
                    Else
                        SelectFlightInfo tmpAftUrno, LineNo
                    End If
                End If
            End If
        End If
        On Error Resume Next
        If ApplicationIsStarted Then
            If tabTelexList(Index).Visible Then
                tabTelexList(Index).SetFocus
            End If
        End If
        ShowFolderButtons tmpFlnu, tmpSere, tmpTtyp
    End If
End Sub
Private Sub ShowFolderButtons(CurFlnu As String, CurSere As String, CurTtyp As String)
    Dim CurIdx As Integer
    Dim ShowLoad As Boolean
    Dim UseActTypes As String
    Dim ChkActCode As String
    Dim CurActCode As String
    CurIdx = -1
    If CurFlnu <> "0" Then ShowLoad = True Else ShowLoad = False
    If (CurSere <> "") And (CurTtyp <> "") Then
        Select Case CurSere
            Case "R"
                UseActTypes = RecvTypeAction
            Case "I"
                UseActTypes = RecvTypeAction
            Case Else
                'ShowLoad = False
        End Select
        CurIdx = -1
        ChkActCode = CurTtyp & ";"
        CurActCode = GetItem(UseActTypes, 2, ChkActCode)
        CurActCode = GetItem(CurActCode, 1, "|")
        Select Case CurActCode
            Case "ATC"
                CurIdx = 1
            Case "IMP"
                CurIdx = 2
            Case Else
                CurIdx = -1
        End Select
    End If
    If bmFolderButtons0 = True Then FolderButtons(0).Visible = ShowLoad
    If CurButtonPanel <> CurIdx Then
        If CurIdx > 0 Then
            FolderButtons(CurIdx).Left = FolderButtons(0).Left
            If FolderButtons(0).Visible Then
                FolderButtons(CurIdx).Top = FolderButtons(0).Top + FolderButtons(0).Height
            Else
                FolderButtons(CurIdx).Top = FolderButtons(0).Top
            End If
            FolderButtons(CurIdx).Visible = True
            FolderButtons(CurIdx).Tag = CurFlnu & "," & CurSere & "," & CurTtyp
        End If
        If CurButtonPanel > 0 Then FolderButtons(CurButtonPanel).Visible = False
        CurButtonPanel = CurIdx
    End If
End Sub
Private Function BuildTelexText(cpTlxRec As String, FormatType As String) As String
    Dim Result As String
    Dim tmpTtyp As String
    Dim tmpStyp As String
    Dim tmpStat As String
    Dim tmpSere As String
    Dim tmpTlxCode As String
    Dim tmpFldLst As String
    Dim tmpDatLst As String
    Dim tmpTxt1 As String
    Dim tmpTxt2 As String
    Dim tmpOrg3 As String
    Dim tmpDes3 As String
    Dim tmpAdid As String
    Dim tmpFlda As String
    Dim tmpPlan As String
    Dim tmpAct1 As String
    Dim tmpAct2 As String
    Dim tmpData As String
    Dim FlightLine As String
    Dim TimeLine As String
    Dim DelayLine As String
    Dim TextPart As String
    Dim clFields As String
    Dim tmpTlxAdrLst As String
    Dim tmpPaye As String
    Dim LineSep As String
    Result = ""
    clFields = DataPool.TlxTabFields.Text
    tmpSere = GetFieldValue("SERE", cpTlxRec, clFields)
    tmpTtyp = GetFieldValue("TTYP", cpTlxRec, clFields)
    tmpStyp = GetFieldValue("STYP", cpTlxRec, clFields)
    tmpStat = GetFieldValue("STAT", cpTlxRec, clFields)
    If tmpSere = "C" Then
        If FormatType = "TEXT" Then
            LineSep = vbNewLine
        Else
            LineSep = "|"
        End If
        If tmpTtyp = "" Then tmpTtyp = "MVT"
        If tmpTtyp = "MVT" Then
            tmpTxt1 = GetFieldValue("TXT1", cpTlxRec, clFields)
            tmpFldLst = GetItem(tmpTxt1, 1, Chr(30))
            tmpFldLst = CleanString(tmpFldLst, FOR_CLIENT, False)
            tmpDatLst = GetItem(tmpTxt1, 2, Chr(30))
            tmpDatLst = CleanString(tmpDatLst, FOR_CLIENT, False)
            'MsgBox tmpFldLst & vbNewLine & tmpDatLst
            If Left(tmpDatLst, 1) = "," Then tmpDatLst = Mid(tmpDatLst, 2)
            tmpOrg3 = GetFieldValue("ORG3", tmpDatLst, tmpFldLst)
            tmpDes3 = GetFieldValue("DES3", tmpDatLst, tmpFldLst)
            tmpFlda = GetFieldValue("FLDA", tmpDatLst, tmpFldLst)
            If tmpFlda = "" Then tmpFlda = GetFieldValue("STOD", tmpDatLst, tmpFldLst)
            If tmpStyp = "" Then
                tmpAdid = GetFieldValue("ADID", tmpDatLst, tmpFldLst)
                If tmpAdid = "A" Then
                    If tmpStyp = "" Then tmpStyp = "AA"
                ElseIf tmpAdid = "D" Then
                    If tmpStyp = "" Then tmpStyp = "AD"
                Else
                End If
            End If
            FlightLine = ""
            FlightLine = FlightLine & GetFieldValue("FLNO", tmpDatLst, tmpFldLst) & "/"
            FlightLine = FlightLine & Mid(tmpFlda, 7, 2) & "."
            FlightLine = FlightLine & GetFieldValue("REGN", tmpDatLst, tmpFldLst) & "."
            Select Case tmpStyp
                Case "AA"
                    FlightLine = FlightLine & tmpDes3 & LineSep
                    tmpPlan = GetFieldValue("STOA", tmpDatLst, tmpFldLst)
                    tmpAct1 = GetFieldValue("LAND", tmpDatLst, tmpFldLst)
                    tmpAct2 = GetFieldValue("ONBL", tmpDatLst, tmpFldLst)
                    TimeLine = tmpStyp
                    TimeLine = TimeLine & Mid(tmpAct1, 9, 4) & "/"
                    TimeLine = TimeLine & Mid(tmpAct2, 9, 4) & LineSep
                    TextPart = FlightLine & TimeLine
                Case "AD"
                    FlightLine = FlightLine & tmpOrg3 & LineSep
                    tmpPlan = GetFieldValue("STOD", tmpDatLst, tmpFldLst)
                    tmpAct1 = GetFieldValue("OFBL", tmpDatLst, tmpFldLst)
                    tmpAct2 = GetFieldValue("AIRB", tmpDatLst, tmpFldLst)
                    TimeLine = tmpStyp
                    TimeLine = TimeLine & Mid(tmpAct1, 9, 4) & "/"
                    TimeLine = TimeLine & Mid(tmpAct2, 9, 4)
                    tmpData = GetFieldValue("ETAI", tmpDatLst, tmpFldLst)
                    'If tmpData <> "" Then
                        TimeLine = TimeLine & " EA" & Mid(tmpData, 9, 4)
                        tmpData = Left(GetFieldValue("VIAL", tmpDatLst, tmpFldLst), 3)
                        If tmpData = "" Then tmpData = tmpDes3
                        TimeLine = TimeLine & " " & tmpData
                    'End If
                    TextPart = FlightLine & TimeLine
                    DelayLine = BuildDelayLine(tmpFldLst, tmpDatLst)
                    If DelayLine <> "" Then TextPart = TextPart & LineSep & DelayLine
                Case "ED"
                    FlightLine = FlightLine & tmpOrg3 & LineSep
                    tmpAct1 = GetFieldValue("ETDI", tmpDatLst, tmpFldLst)
                    TimeLine = tmpStyp & Mid(tmpAct1, 7, 6)
                    TextPart = FlightLine & TimeLine
                    DelayLine = BuildDelayLine(tmpFldLst, tmpDatLst)
                    If DelayLine <> "" Then TextPart = TextPart & LineSep & DelayLine
                Case "NI"
                    FlightLine = FlightLine & tmpOrg3 & LineSep
                    tmpAct1 = GetFieldValue("NXTI", tmpDatLst, tmpFldLst)
                    TimeLine = tmpStyp
                    TextPart = FlightLine & TimeLine
                    DelayLine = BuildDelayLine(tmpFldLst, tmpDatLst)
                    If DelayLine <> "" Then TextPart = TextPart & LineSep & DelayLine
                Case Else
            End Select
            If FormatType = "LINE" Then
                'Browser Sent Telexes
                Result = TextPart
            Else
                'Telex Text Field
                Result = "=PRIORITY" & vbNewLine & "QU" & vbNewLine
                tmpTxt2 = GetFieldValue("TXT2", cpTlxRec, clFields)
                BuildTlxAdressList tmpTxt2, tmpTlxAdrLst, tmpPaye
                Result = Result & tmpTlxAdrLst
                If tmpPaye <> "" Then Result = Result & tmpPaye
                Result = Result & "=TEXT" & vbNewLine & tmpTtyp & vbNewLine
                Result = Result & TextPart & vbNewLine
            End If
        End If
    End If
    BuildTelexText = Result
End Function
Private Function BuildDelayLine(UseFldLst As String, UseDatLst As String) As String
    Dim tmpDtd1 As String
    Dim tmpDtd2 As String
    Dim tmpDcd1 As String
    Dim tmpDcd2 As String
    Dim DelayLine As String
    DelayLine = ""
    tmpDtd1 = GetFieldValue("DTD1", UseDatLst, UseFldLst)
    tmpDtd2 = GetFieldValue("DTD2", UseDatLst, UseFldLst)
    tmpDcd1 = GetFieldValue("DCD1", UseDatLst, UseFldLst)
    tmpDcd2 = GetFieldValue("DCD2", UseDatLst, UseFldLst)
    If Val(tmpDtd1) <= 0 Then tmpDtd1 = ""
    If Val(tmpDtd2) <= 0 Then tmpDtd2 = ""
    If Val(tmpDtd1) > 0 And tmpDcd1 = "" Then tmpDcd1 = ".."
    If Val(tmpDtd2) > 0 And tmpDcd2 = "" Then tmpDcd2 = ".."
    If (tmpDcd1 & tmpDcd2) <> "" Then
        DelayLine = "DL" & tmpDcd1
        If tmpDcd2 <> "" Then DelayLine = DelayLine & "/" & tmpDcd2
        If tmpDtd1 <> "" Then DelayLine = DelayLine & "/" & tmpDtd1
        If tmpDtd2 <> "" Then DelayLine = DelayLine & "/" & tmpDtd2
    End If
    BuildDelayLine = DelayLine
End Function

Private Sub BuildTlxAdressList(FromTxt2 As String, OutAdrLst As String, OutPaye As String)
    Dim tmpAdrLst As String
    Dim ItmCnt As Integer
    Dim CurItm As Integer
    Dim ItmDat As String
    Dim ilLen As Integer
    OutAdrLst = ""
    OutPaye = ""
    tmpAdrLst = CleanString(FromTxt2, FOR_CLIENT, False)
    'MsgBox tmpAdrLst
    ItmCnt = ItemCount(tmpAdrLst, ",")
    'MsgBox ItmCnt
    For CurItm = 1 To ItmCnt
        ItmDat = GetItem(tmpAdrLst, CurItm, ",")
        ilLen = Len(ItmDat)
        If ilLen > 3 Then
            OutAdrLst = OutAdrLst & "STX," & ItmDat & vbNewLine
        Else
            If ilLen > 1 Then OutPaye = OutPaye & ItmDat & ","
        End If
    Next
    If OutAdrLst <> "" Then
        OutAdrLst = "=DESTINATION TYPE B" & vbNewLine & OutAdrLst
    End If
    If OutPaye <> "" Then
        OutPaye = "=DBLSIG" & vbNewLine & GetItem(OutPaye, 1, ",") & vbNewLine
    End If
    'MsgBox OutAdrLst
    'MsgBox OutPaye
End Sub
Private Function GetCsgnFromTelex(TlxTxt As String)
    Dim Result As String
    Result = Trim(GetItem(TlxTxt, 2, "CSGN:"))
    Result = Trim(GetItem(Result, 1, vbNewLine))
    GetCsgnFromTelex = Result
End Function
Private Function TextString(cpText As String, ipMeth As Integer) As String
    Dim clTlxLin As String
    Dim tmpData As String
    Dim ilOffs As Integer
    Dim ilTxtBgn As Integer
    Dim chkTxtCod As String
    Dim CutTxtCod As Boolean
    clTlxLin = CleanString(cpText, FOR_CLIENT, True)
    If FullText.Value = 0 Then
        If InStr(clTlxLin, "=TEXT") > 0 Then
            chkTxtCod = vbLf & "=TEXT"
            CutTxtCod = True
        Else
            chkTxtCod = vbLf & "."
            CutTxtCod = False
        End If
        ilTxtBgn = 0
        ilOffs = 1
        While ilOffs > 0
            ilOffs = InStr(ilOffs, clTlxLin, chkTxtCod)
            If ilOffs > 0 Then
                ilTxtBgn = ilOffs + 1
                ilOffs = ilOffs + 1
                ilOffs = 0 'we need it only once
            End If
        Wend
        If ilTxtBgn > 0 Then
            If CutTxtCod Then ilTxtBgn = ilTxtBgn + Len(chkTxtCod)
            clTlxLin = Mid(clTlxLin, ilTxtBgn)
            chkTxtCod = Left(clTlxLin, 1)
            While (chkTxtCod = vbLf) Or (chkTxtCod = vbCr)
                clTlxLin = Mid(clTlxLin, 2)
                chkTxtCod = Left(clTlxLin, 1)
            Wend
        End If
    End If
    TextString = clTlxLin
End Function

Private Sub tabTelexList_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim LastSortCol As Long
    Dim SortAsc As Boolean
    If LineNo < 0 Then
        LastSortCol = Val(tabTelexList(Index).Tag)
        If LastSortCol <> ColNo Then
            SortAsc = True
        Else
            SortAsc = Not tabTelexList(Index).SortOrderASC
        End If
        tabTelexList(Index).Sort Str(ColNo), SortAsc, True
        tabTelexList(Index).AutoSizeColumns
        tabTelexList(Index).Tag = CStr(ColNo)
    End If
End Sub

'Private Sub tabTelexList_SendMouseMove(Index As Integer, ByVal Line As Long, ByVal Col As Long, ByVal Flags As String)
'Static lastLine(0 To 4) As Long
'Dim RecIdx 'as variant
'Dim fldidx As Integer
'Dim tmpData As String
'    If Flags = "LBUTTON" Then
'        If chkAssFolder.Enabled Then
'            tabTelexList(Index).DragIcon = Picture1(0).Picture
'            tabTelexList(Index).Drag vbBeginDrag
'        End If
'    Else
'
'    'If (Line + 1) <> lastLine(Index) Then
'        RecIdx = tabTelexList(Index).GetColumnValue(Line, 8)
'        If RecIdx <> "" Then
'            tmpData = ""
'            Select Case Col
'                Case 0
'                    fldidx = GetItemNo(DataPool.TlxTabFields, "STAT") - 1
'                    If fldidx >= 0 Then
'                        tmpData = DataPool.TelexData(CurMem).GetColumnValue(RecIdx, fldidx)
'                        tmpData = TlxRcvStat.ExplainStatus("STAT", tmpData, False)
'                    End If
'                Case 1
'                    fldidx = GetItemNo(DataPool.TlxTabFields, "WSTA") - 1
'                    If fldidx >= 0 Then
'                        tmpData = DataPool.TelexData(CurMem).GetColumnValue(RecIdx, fldidx)
'                        tmpData = TlxRcvStat.ExplainStatus("WSTA", tmpData, False)
'                    End If
'                Case 4
'                    fldidx = GetItemNo(DataPool.TlxTabFields, "CDAT") - 1
'                    If fldidx >= 0 Then
'                        tmpData = DataPool.TelexData(CurMem).GetColumnValue(RecIdx, fldidx)
'                        tmpData = Format(CedaDateToVb(tmpData), MySetUp.DefDateFormat)
'                    End If
'                Case Else
'                    'ShowTip = False
'            End Select
'            tabTelexList(Index).ToolTipText = tmpData   'Set or Reset the ToolTipText
'        End If
'        lastLine(Index) = Line + 1  'to force line 0
'    'End If
'    End If
'End Sub

Private Sub tabTelexList_SendRButtonClick(Index As Integer, ByVal Line As Long, ByVal Col As Long)
    'MsgBox "R-Click"
End Sub

Public Function BuildTlxBrowser(FormatType As String, cpFields As String, cpTlxRec As String, TxtSubject As Boolean) As String
    Dim clTlxLin As String
    Dim tmpTtyp As String
    Dim tmpStyp As String
    Dim tmpStat As String
    Dim tmpWsta As String
    Dim tmpSere As String
    Dim tmpTlxCode As String
    Dim tmpFldLst As String
    Dim tmpDatLst As String
    Dim tmpTxt1 As String
    Dim tmpOrg3 As String
    Dim tmpDes3 As String
    Dim tmpAdid As String
    Dim tmpPlan As String
    Dim tmpAct1 As String
    Dim tmpAct2 As String
    Dim tmpData As String
    tmpSere = GetFieldValue("SERE", cpTlxRec, cpFields)
    tmpTtyp = GetFieldValue("TTYP", cpTlxRec, cpFields)
    tmpStyp = GetFieldValue("STYP", cpTlxRec, cpFields)
    tmpStat = GetFieldValue("STAT", cpTlxRec, cpFields)
    tmpWsta = GetFieldValue("WSTA", cpTlxRec, cpFields)
    If tmpSere = "C" And FormatType = "C" Then
        If tmpStat = "C" Then
            tmpTxt1 = GetFieldValue("TXT1", cpTlxRec, cpFields)
            tmpFldLst = GetItem(tmpTxt1, 1, Chr(30))
            tmpFldLst = CleanString(tmpFldLst, FOR_CLIENT, False)
            tmpDatLst = GetItem(tmpTxt1, 2, Chr(30))
            tmpDatLst = CleanString(tmpDatLst, FOR_CLIENT, False)
            If Left(tmpDatLst, 1) = "," Then tmpDatLst = Mid(tmpDatLst, 2)
            tmpOrg3 = GetFieldValue("ORG3", tmpDatLst, tmpFldLst)
            tmpDes3 = GetFieldValue("DES3", tmpDatLst, tmpFldLst)
            tmpAdid = GetFieldValue("ADID", tmpDatLst, tmpFldLst)
            If tmpAdid = "A" Then
                If tmpStyp = "" Then tmpStyp = "AA"
            ElseIf tmpAdid = "D" Then
                If tmpStyp = "" Then tmpStyp = "AD"
            Else
            End If
            Select Case tmpStyp
                Case "AA"
                    tmpPlan = GetFieldValue("STOA", tmpDatLst, tmpFldLst)
                    tmpAct1 = GetFieldValue("LAND", tmpDatLst, tmpFldLst)
                    tmpAct2 = GetFieldValue("ONBL", tmpDatLst, tmpFldLst)
                Case "AD"
                    tmpPlan = GetFieldValue("STOD", tmpDatLst, tmpFldLst)
                    tmpAct1 = GetFieldValue("OFBL", tmpDatLst, tmpFldLst)
                    tmpAct2 = GetFieldValue("AIRB", tmpDatLst, tmpFldLst)
                Case "ED"
                    tmpPlan = GetFieldValue("STOD", tmpDatLst, tmpFldLst)
                    tmpAct1 = GetFieldValue("ETDI", tmpDatLst, tmpFldLst)
                Case "NI"
                    tmpPlan = GetFieldValue("STOD", tmpDatLst, tmpFldLst)
                    tmpAct1 = GetFieldValue("NXTI", tmpDatLst, tmpFldLst)
                Case Else
            End Select
            clTlxLin = tmpStat & "," & tmpWsta & ","
            clTlxLin = clTlxLin & tmpTtyp & "," & tmpStyp & ","
            clTlxLin = clTlxLin & GetFieldValue("FLNO", tmpDatLst, tmpFldLst) & ","
            clTlxLin = clTlxLin & GetFieldValue("REGN", tmpDatLst, tmpFldLst) & ","
            clTlxLin = clTlxLin & tmpPlan & ","
            clTlxLin = clTlxLin & tmpAct1 & ","
            clTlxLin = clTlxLin & tmpAct2 & ","
            clTlxLin = clTlxLin & GetFieldValue("URNO", cpTlxRec, cpFields) & ","
        End If
    Else
        clTlxLin = tmpStat & "," & tmpWsta & "," & tmpTtyp & "," & tmpStyp & ","
        tmpData = GetFieldValue("CDAT", cpTlxRec, cpFields)
        clTlxLin = clTlxLin & tmpData & ","
        clTlxLin = clTlxLin & GetFieldValue("TXNO", cpTlxRec, cpFields) & ","
        tmpData = GetFieldValue("TXT1", cpTlxRec, cpFields) & GetFieldValue("TXT2", cpTlxRec, cpFields)
        If tmpSere <> "C" Then
            If TxtSubject = True Then tmpData = GetTelexExtract(tmpSere, tmpTtyp, tmpData, 80)
        Else
            tmpData = BuildTelexText(cpTlxRec, "LINE")
        End If
        clTlxLin = clTlxLin & tmpData & ","
        clTlxLin = clTlxLin & GetFieldValue("URNO", cpTlxRec, cpFields) & ","
    End If
    clTlxLin = Replace(clTlxLin, Chr(28), "|", 1, -1, vbBinaryCompare)
    BuildTlxBrowser = clTlxLin
End Function

Public Sub PrepareTelexExtract(CurMemIdx As Integer)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim SereCol As Long
    Dim TtypCol As Long
    Dim Txt1Col As Long
    Dim Txt2Col As Long
    Dim StatCol As Long
    Dim WstaCol As Long
    Dim PrflCol As Long
    Dim FlnuCol As Long
    Dim LineStatus As Long
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    Dim CurSere As String
    Dim CurText As String
    Dim CurTtyp As String
    Dim CurTypeList As String
    Dim CurStat As String
    Dim CurWsta As String
    Dim ShortText As String
    Dim i As Integer
    Dim itm As Integer
    Dim LineList As String
    Dim AllCount As Long
    Dim TypCount As Long
    Dim ShowTelex As Boolean
    SereCol = CLng(GetRealItemNo(DataPool.TlxTabFields, "SERE"))
    TtypCol = CLng(GetRealItemNo(DataPool.TlxTabFields, "TTYP"))
    Txt1Col = CLng(GetRealItemNo(DataPool.TlxTabFields, "TXT1"))
    Txt2Col = CLng(GetRealItemNo(DataPool.TlxTabFields, "TXT2"))
    PrflCol = CLng(GetRealItemNo(DataPool.TlxTabFields, "PRFL"))
    StatCol = CLng(GetRealItemNo(DataPool.TlxTabFields, "STAT"))
    WstaCol = CLng(GetRealItemNo(DataPool.TlxTabFields, "WSTA"))
    FlnuCol = CLng(GetRealItemNo(DataPool.TlxTabFields, "FLNU"))
    MaxLine = DataPool.TelexData(CurMemIdx).GetLineCount - 1
    For CurLine = 0 To MaxLine
        If (CurLine Mod 200) = 0 Then DoEvents
        CurText = DataPool.TelexData(CurMemIdx).GetColumnValue(CurLine, Txt1Col)
        CurText = Trim(CurText & DataPool.TelexData(CurMemIdx).GetColumnValue(CurLine, Txt2Col))
        CurTtyp = DataPool.TelexData(CurMemIdx).GetColumnValue(CurLine, TtypCol)
        CurSere = DataPool.TelexData(CurMemIdx).GetColumnValue(CurLine, SereCol)
        LineStatus = DataPool.TelexData(CurMemIdx).GetLineStatusValue(CurLine)
        ShowTelex = True
        If (CedaIsConnected) Or (UfisServer.HostName = "LOCAL") Then
            If CurSere <> "C" Then
                If (MySetUp.AddrFilter.Value = 1) And (LineStatus = 0) Then ShowTelex = FilterTelexAddress(CurText, MySetUp.HomeTlxAddr.Text, AddrNoCheck, CurTtyp, optTlxAddr.Value)
                If optTlxAddr.Value = True Then ShowTelex = FilterAddressHeader(CurText, Trim(txtAddr.Text), AddrNoCheck, CurTtyp, ShowTelex)
                ShowTelex = FilterTelexText(CurText, IgnoreAllTlxText, ShowTelex)
                ShowTelex = FilterTelexTypeText(CurTtyp, CurText, TextFilterList, ShowTelex)
            End If
            If (CurText = "") Then
                If (IgnoreEmptyText) Then ShowTelex = False
            End If
            If ShowTelex Then
                If CurSere = "C" Then
                    CurText = DataPool.TelexData(CurMemIdx).GetLineValues(CurLine)
                    CurText = BuildTelexText(CurText, "TEXT")
                    CurText = CleanString(CurText, FOR_SERVER, False)
                End If
                ShortText = GetTelexExtract(CurSere, CurTtyp, CurText, 80)
                DataPool.TelexData(CurMemIdx).SetColumnValue CurLine, PrflCol, ShortText
                If LineStatus = 0 Then
                    CurStat = DataPool.TelexData(CurMemIdx).GetColumnValue(CurLine, StatCol)
                    CurWsta = DataPool.TelexData(CurMemIdx).GetColumnValue(CurLine, WstaCol)
                    ColorPool.GetStatusColor CurWsta, "", CurForeColor, CurBackColor
                    DataPool.TelexData(CurMemIdx).SetLineColor CurLine, CurForeColor, CurBackColor
                Else
                    DataPool.TelexData(CurMemIdx).SetLineColor CurLine, vbBlack, LightYellow
                End If
                If CurTtyp = "" Then
                    CurTtyp = "FREE"
                    DataPool.TelexData(CurMemIdx).SetColumnValue CurLine, TtypCol, CurTtyp
                End If
                If InStr(FolderTypeList, CurTtyp) = 0 Then
                    LineStatus = LineStatus Or 2
                    DataPool.TelexData(CurMemIdx).SetLineStatusValue CurLine, LineStatus
                End If
            Else
                DataPool.TelexData(CurMemIdx).SetLineStatusValue CurLine, 99
            End If
        End If
    Next
    CurLine = 0
    While CurLine <= MaxLine
        If DataPool.TelexData(CurMemIdx).GetLineStatusValue(CurLine) = 99 Then
            DataPool.TelexData(CurMemIdx).DeleteLine CurLine
            CurLine = CurLine - 1
            MaxLine = MaxLine - 1
        End If
        CurLine = CurLine + 1
    Wend
    SetFolderValues CurMemIdx
End Sub
Public Function GetTelexExtract(TlxSere As String, TlxType As String, TlxText As String, MaxLen As Long) As String
    Dim tmpData As String
    Dim tmpText As String
    Dim tmpTlxCode As String
    Dim tmpTlxCode2 As String
    Dim NxtLine As String
    Dim FirstChar As String
    Dim DotPos As Integer
    Dim CodPos As Integer
    Dim NxtPos As Integer
    Dim CheckDotPos As Boolean
    Dim i As Integer
    Dim cnt As Integer
    Dim tmpChr As String
    Dim tmpAsc As Integer
    'If TlxSere = "C" Then
        'tmpData = BuildTelexText(TlxText, "TEXT")
    'Else
        tmpData = TlxText
    'End If
    If (TlxSere = "S") And (MySitaOutType = "SITA_FILE") Then
        tmpTlxCode = "=TEXT"
        tmpData = GetItem(TlxText, 2, tmpTlxCode)
        If tmpData = "" Then tmpData = TlxText
    End If
    tmpText = tmpData
    
    tmpTlxCode = "."
    tmpTlxCode = ""
    
    'If (TlxSere = "R") Or ((TlxSere = "S") And (MySitaOutType = "SITA_NODE")) Then
    'If TlxSere <> "C" Then
        CheckDotPos = True
        Select Case TlxType
            Case "ATC"
                If InStr(tmpText, "CSGN:") > 0 Then tmpTlxCode = "CSGN:"
                If InStr(tmpText, "Info:") > 0 Then tmpTlxCode = "Info:"
                If InStr(tmpText, "CALLSIGN") > 0 Then tmpTlxCode = "CALLSIGN"
                'For i = 1 To 20
                '    tmpChr = Mid(tmpText, i, 1)
                '    tmpAsc = Asc(tmpChr)
                'Next
            Case "BTM"
                tmpTlxCode = ".F/"
            Case "DLS"
                tmpTlxCode = "STATEMENT"
            Case "FWD"
                tmpTlxCode = "ORIGINALLY ON"
            Case "KRIS"
                If InStr(tmpText, "<=SCREENCONTENT=>") > 0 Then
                    GetKeyItem tmpData, tmpText, "<=SCREENCONTENT=>", "<=/SCREENCONTENT=>"
                    tmpText = tmpData
                End If
                CheckDotPos = False
            Case "DFS"
                tmpTlxCode = "ARCID"
            Case Else
                If InStr(tmpText, "<=SCREENCONTENT=>") > 0 Then
                    GetKeyItem tmpData, tmpText, "<=SCREENCONTENT=>", "<=/SCREENCONTENT=>"
                    tmpText = tmpData
                    CheckDotPos = False
                Else
                    tmpTlxCode = Chr(28) & TlxType
                    tmpTlxCode2 = "|" & TlxType
                End If
        End Select
        If CheckDotPos Then
            DotPos = InStr(tmpText, ".")
            If DotPos > 0 Then
                NxtLine = Mid(tmpText, DotPos)
                NxtLine = GetItem(NxtLine, 1, " ")
            End If
            While (DotPos > 0) And (Len(NxtLine) < 8)
                DotPos = DotPos + 1
                DotPos = InStr(DotPos, tmpText, ".")
                If DotPos > 0 Then
                    NxtLine = Mid(tmpText, DotPos)
                    NxtLine = GetItem(NxtLine, 1, " ")
                End If
            Wend
            If tmpTlxCode <> Chr(28) Then CodPos = InStr(tmpText, tmpTlxCode) Else CodPos = 0
            If (CodPos = 0) And (tmpTlxCode2 <> "") Then
                CodPos = InStr(tmpText, tmpTlxCode2)
            End If
            If CodPos > DotPos Then
                CodPos = CodPos + Len(tmpTlxCode)
                tmpData = Mid(tmpText, CodPos)
            ElseIf (CodPos > 0) And (DotPos > CodPos) Then
                While (CodPos > 0) And (DotPos > CodPos)
                    CodPos = CodPos + Len(tmpTlxCode)
                    tmpData = Mid(tmpText, CodPos)
                    CodPos = InStr(CodPos, tmpText, tmpTlxCode)
                Wend
                If CodPos > DotPos Then
                    CodPos = CodPos + Len(tmpTlxCode)
                    tmpData = Mid(tmpText, CodPos)
                End If
            ElseIf (CodPos = 0) And (DotPos > 0) Then
                tmpData = Mid(tmpData, DotPos)
                NxtPos = InStr(tmpData, Chr(28))
                If NxtPos > 0 Then tmpData = Mid(tmpData, NxtPos + 1)
                NxtLine = Replace(GetItem(tmpData, 1, Chr(28)), " ", "", 1, -1, vbBinaryCompare)
                If (Len(TlxType) > 0) And (Left(NxtLine, Len(TlxType)) = TlxType) Then
                    NxtPos = InStr(tmpData, Chr(28))
                    If NxtPos > 0 Then tmpData = Mid(tmpData, NxtPos + 1)
                Else
                    NxtLine = GetItem(tmpData, 1, " ")
                    If (Len(NxtLine) = 2) And (Left(NxtLine, 1) = "Q") Then
                        DotPos = InStr(tmpData, ".")
                        If DotPos > 0 Then tmpData = Mid(tmpData, DotPos)
                        NxtPos = InStr(tmpData, Chr(28))
                        If NxtPos > 0 Then tmpData = Mid(tmpData, NxtPos + 1)
                    End If
                End If
            End If
            tmpData = LTrim(tmpData)
            If Left(tmpData, 1) = ":" Then tmpData = Mid(tmpData, 2)
            tmpData = Replace(tmpData, Chr(179), Chr(28), 1, -1, vbBinaryCompare)
        End If
    'Else
    'ElseIf TlxSere = "C" Then
    '    tmpData = tmpData
    'End If
    If InStr(tmpData, "=TEXT") > 0 Then
        tmpData = GetItem(tmpData, 2, "=TEXT")
    End If
    tmpData = LTrim(tmpData)
    FirstChar = Left(tmpData, 1)
    While ((FirstChar = Chr(28)) Or (FirstChar = Chr(29)) Or (FirstChar = "|"))
        tmpData = Mid(tmpData, 2)
        tmpData = LTrim(tmpData)
        FirstChar = Left(tmpData, 1)
    Wend
    If tmpData = "" Then
        tmpData = tmpText
        FirstChar = Left(tmpData, 1)
        While ((FirstChar = Chr(28)) Or (FirstChar = Chr(29)) Or (FirstChar = "|"))
            tmpData = Mid(tmpData, 2)
            tmpData = LTrim(tmpData)
            FirstChar = Left(tmpData, 1)
        Wend
    End If
    tmpData = Replace(tmpData, Chr(29), "", 1, -1, vbBinaryCompare)
    tmpData = Replace(tmpData, Chr(28), "|", 1, -1, vbBinaryCompare)
    tmpData = Replace(tmpData, " |", "|", 1, -1, vbBinaryCompare)
    tmpText = tmpData
    cnt = ItemCount(tmpText, "|")
    tmpData = ""
    For i = 1 To cnt
        tmpChr = GetItem(tmpText, i, "|")
        If tmpChr <> "" Then tmpData = tmpData & tmpChr & "|"
    Next
    If MaxLen > 32 Then tmpData = Left(tmpData, MaxLen)
    GetTelexExtract = tmpData
End Function
Private Sub ResetTlxTypeFolder()
    Dim ilOldTab As Integer
    'ilOldTab = Val(tabTlxTypeFolder.Tag)
    'If ilOldTab > 0 Then
    '    tabTlxTypeFolder.Tabs(ilOldTab).HighLighted = False
    '    tabTlxTypeFolder.Tag = 0
    'End If
    chkTlxTitleBar(0).Value = 1
    chkTlxTitleBar(1).Value = 1
End Sub

Private Sub txtAddr_Change()
    CheckLoadButton False
End Sub

Private Sub txtAddr_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtDateFrom_Change()
    If chkFromTo.BackColor = vbRed Then
        chkFromTo.BackColor = vbButtonFace
        chkFromTo.ForeColor = vbBlack
    End If
    ResetFromToCheck
    If CheckDateField(txtDateFrom, MySetUp.DefDateFormat, False) <> True Then DoNothing
    CheckLoadButton False
End Sub

Private Sub txtDateFrom_KeyPress(KeyAscii As Integer)
    If (KeyAscii <> 8) Then
        If InStr(".-/", Chr(KeyAscii)) > 0 Then
            If InStr(MySetUp.DefDateFormat, Chr(KeyAscii)) = 0 Then KeyAscii = 0
        Else
            If Not IsNumeric(Chr(KeyAscii)) Then KeyAscii = 0
        End If
    End If
End Sub

Private Sub txtDateFrom_Validate(Cancel As Boolean)
    If CheckDateField(txtDateFrom, MySetUp.DefDateFormat, True) <> True Then DoNothing
End Sub

Private Sub txtDateTo_Change()
    If chkFromTo.BackColor = vbRed Then
        chkFromTo.BackColor = vbButtonFace
        chkFromTo.ForeColor = vbBlack
    End If
    ResetFromToCheck
    If CheckDateField(txtDateTo, MySetUp.DefDateFormat, False) <> True Then DoNothing
    CheckLoadButton False
End Sub

Private Sub txtDateTo_KeyPress(KeyAscii As Integer)
    If (KeyAscii <> 8) Then
        If InStr(".-/", Chr(KeyAscii)) > 0 Then
            If InStr(MySetUp.DefDateFormat, Chr(KeyAscii)) = 0 Then KeyAscii = 0
        Else
            If Not IsNumeric(Chr(KeyAscii)) Then KeyAscii = 0
        End If
    End If
End Sub

Private Sub txtDateTo_Validate(Cancel As Boolean)
    If CheckDateField(txtDateTo, MySetUp.DefDateFormat, True) <> True Then DoNothing
End Sub

Private Sub txtFlca_Change()
    txtFlca.Tag = CheckAlcLookUp(txtFlca.Text, False)
    CheckLoadButton False
End Sub

Private Sub txtFlca_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtFlca_LostFocus()
    txtFlca.Tag = CheckAlcLookUp(txtFlca.Text, False)
    CheckLoadButton False
End Sub
Public Function CheckAlcLookUp(AlcCode As String, AskBack As Boolean) As String
    Dim CurTxt As String
    Dim CurAlc As String
    CurTxt = Trim(AlcCode)
    CurAlc = ""
    If CurTxt <> "" Then
        If Len(CurTxt) = 2 Then
            CurAlc = UfisServer.BasicLookUp(1, 0, 1, CurTxt, 0, False)
        Else
            CurAlc = UfisServer.BasicLookUp(1, 1, 1, CurTxt, 0, False)
        End If
    End If
    CheckAlcLookUp = CurAlc
End Function
Private Sub txtFlns_Change()
    CheckLoadButton False
End Sub

Private Sub txtFlns_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtFltn_Change()
    Dim tmpFltn As String
    tmpFltn = Trim(txtFltn.Text)
    If tmpFltn <> "" Then
        If Len(tmpFltn) < 3 Then tmpFltn = Right("000" & tmpFltn, 3)
    End If
    txtFltn.Tag = tmpFltn
    CheckLoadButton False
End Sub

Private Sub txtFltn_KeyPress(KeyAscii As Integer)
    If KeyAscii <> 8 Then
        If Not IsNumeric(Chr(KeyAscii)) Then KeyAscii = 0
    End If
End Sub

Private Sub txtRegn_Change()
    CheckLoadButton False
End Sub

Private Sub txtRegn_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtText_Change()
    CheckLoadButton False
End Sub

Private Sub txtText_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtTimeFrom_Change()
    Dim tmpVal As String
    Dim tmpHour As String
    Dim tmpMin As String
    Dim ValIsOk As Boolean
    ValIsOk = True
    ResetFromToCheck
    tmpVal = txtTimeFrom.Text
    If InStr(tmpVal, ":") > 0 Then
        If Len(tmpVal) <> 5 Then
            ValIsOk = False
        Else
            tmpHour = GetItem(tmpVal, 1, ":")
            tmpMin = GetItem(tmpVal, 2, ":")
        End If
    ElseIf Len(tmpVal) <> 4 Then
        ValIsOk = False
    Else
        tmpHour = Left(tmpVal, 2)
        tmpMin = Right(tmpVal, 2)
    End If
    If ValIsOk Then
        If Val(tmpHour) > 23 Then ValIsOk = False
        If Val(tmpHour) < 0 Then ValIsOk = False
        If Val(tmpMin) > 59 Then ValIsOk = False
        If Val(tmpMin) < 0 Then ValIsOk = False
    End If
    If ValIsOk Then
        txtTimeFrom.BackColor = vbWhite
        txtTimeFrom.ForeColor = vbBlack
        txtTimeFrom.Tag = Replace(tmpVal, ":", "", 1, -1, vbBinaryCompare)
    Else
        txtTimeFrom.BackColor = vbRed
        txtTimeFrom.ForeColor = vbWhite
        txtTimeFrom.Tag = ""
    End If
    CheckLoadButton False
End Sub
Private Sub ResetFromToCheck()
    If chkFromTo.Tag <> "" Then
        chkFromTo.Tag = ""
        chkFromTo.BackColor = vbButtonFace
        chkFromTo.ForeColor = vbBlack
        txtDateFrom.BackColor = vbWhite
        txtDateTo.BackColor = vbWhite
    End If
End Sub

Private Sub txtTimeFrom_KeyPress(KeyAscii As Integer)
    If (KeyAscii <> 8) And (Chr(KeyAscii) <> ":") Then
        If Not IsNumeric(Chr(KeyAscii)) Then KeyAscii = 0
    End If
End Sub

Private Sub txtTimeTo_Change()
    Dim tmpVal As String
    Dim tmpHour As String
    Dim tmpMin As String
    Dim ValIsOk As Boolean
    ValIsOk = True
    ResetFromToCheck
    tmpVal = txtTimeTo.Text
    If InStr(tmpVal, ":") > 0 Then
        If Len(tmpVal) <> 5 Then
            ValIsOk = False
        Else
            tmpHour = GetItem(tmpVal, 1, ":")
            tmpMin = GetItem(tmpVal, 2, ":")
        End If
    ElseIf Len(tmpVal) <> 4 Then
        ValIsOk = False
    Else
        tmpHour = Left(tmpVal, 2)
        tmpMin = Right(tmpVal, 2)
    End If
    If ValIsOk Then
        If Val(tmpHour) > 23 Then ValIsOk = False
        If Val(tmpHour) < 0 Then ValIsOk = False
        If Val(tmpMin) > 59 Then ValIsOk = False
        If Val(tmpMin) < 0 Then ValIsOk = False
    End If
    If ValIsOk Then
        txtTimeTo.BackColor = vbWhite
        txtTimeTo.ForeColor = vbBlack
        txtTimeTo.Tag = Replace(tmpVal, ":", "", 1, -1, vbBinaryCompare)
    Else
        txtTimeTo.BackColor = vbRed
        txtTimeTo.ForeColor = vbWhite
        txtTimeTo.Tag = ""
    End If
    CheckLoadButton False
End Sub

Public Sub DistributeBroadcast(BcNum As String, DestName As String, RecvName As String, cpTable As String, cpCmd As String, cpSelKey As String, cpFields As String, cpData As String)
    Dim tmpUrno As String
    Dim tmpStat As String
    Dim RcvIdx As Integer
    Dim InfIdx As Integer
    Dim UrnoIdx As Integer
    Dim LineIdx As Long
    Dim fldidx As Integer
    Dim tmpList As String
    Dim tmpRcvList As String
    Dim tmpInfList As String
    Dim CurRecDat As String
    Dim RcvTlxLine As String
    Dim tmpFldNam As String
    Dim tmpFldVal As String
    Dim ItmNbr As Integer
    Dim myTextColor As Long
    Dim myBackColor As Long
    Dim tmpData As String
    Dim tmpTxtData As String
    Dim RcvRefresh As Boolean
    Dim InfRefresh As Boolean
    Dim ShowTelex As Boolean
    Dim MaxLinCnt As Long
    Dim FilterLookUp As String
    Dim FlnuCol As Long
    Dim tmpFlnu As String
    Dim tmpLine As Long
    Dim tmpFnam As String
    Dim tmpFdat As String
    Dim tmpDatFrom As String
    Dim tmpDatTo As String
    Dim i As Integer
    Dim tmpFolderCount As String
    Dim tmpPos As Integer
    Dim tmpVal As String
    Dim idx As Integer
    'only called by URT or UBT
    'Should be redesigned
    
    If MainIsOnline Then
        DataPool.UpdateOnlineTelex cpFields, cpData, cpSelKey
        tabTelexList(2).Refresh
        tabTelexList(3).Refresh
        If chkAssFolder.Enabled And MainIsDeploy = False Then
            If cpTable = "TFNTAB" And (cpCmd = "URT" Or cpCmd = "DRT") Then
                tmpFnam = GetFieldValue("FNAM", cpData, cpFields)
                tmpFdat = GetFieldValue("FDAT", cpData, cpFields)
                If Len(tmpFdat) = 0 And (InStr(AddFolderList, tmpFnam) = 0 Or cpCmd = "DRT") Then
                    chkAssFolder.Value = 0
                    chkAssFolder.Value = 1
                End If
            End If
            If cpCmd = "IRT" And cpTable = "TFNTAB" Then
                tmpFnam = GetFieldValue("FNAM", cpData, cpFields)
                tmpFdat = GetFieldValue("FDAT", cpData, cpFields)
                If Len(tmpFdat) = 0 Then
                    chkAssFolder.Value = 0
                    chkAssFolder.Value = 1
                Else
                    tmpDatFrom = Mid(txtDateFrom.Text, 7, 4) & Mid(txtDateFrom.Text, 4, 2) & Mid(txtDateFrom.Text, 1, 2)
                    tmpDatTo = Mid(txtDateTo.Text, 7, 4) & Mid(txtDateTo.Text, 4, 2) & Mid(txtDateTo.Text, 1, 2)
                    tmpDatFrom = DecodeSsimDayFormat(tmpDatFrom, "CEDA", "SSIM2")
                    tmpDatTo = DecodeSsimDayFormat(tmpDatTo, "CEDA", "SSIM2")
                    If tmpFdat = tmpDatFrom Or tmpFdat = tmpDatTo Then
                        For i = 0 To fraFolder.UBound - 2
                            If chkFolder(i).Caption = tmpFnam Then
                                tmpFolderCount = chkTabs(i).Caption
                                tmpPos = InStr(tmpFolderCount, "/")
                                If tmpPos > 0 Then
                                    tmpVal = Mid(tmpFolderCount, tmpPos + 1)
                                    tmpVal = tmpVal + 1
                                    tmpFolderCount = Left(tmpFolderCount, tmpPos)
                                    tmpFolderCount = tmpFolderCount & tmpVal
                                Else
                                    tmpFolderCount = tmpFolderCount & "/1"
                                End If
                                chkTabs(i).Caption = tmpFolderCount
                                chkTabs(i).BackColor = vbYellow
                                chkLoad.BackColor = vbRed
                                chkLoad.ForeColor = vbWhite
                                Exit For
                            End If
                        Next
                    End If
                End If
            End If
        End If
        If chkAssFolder.Enabled And fraFlightList.Visible Then
            If cpCmd = "IRT" And cpTable = "TLKTAB" Then
                tmpFdat = GetFieldValue("FDAT", cpData, cpFields)
                tmpFdat = Left(tmpFdat, 7)
                tmpDatFrom = Mid(txtDateFrom.Text, 7, 4) & Mid(txtDateFrom.Text, 4, 2) & Mid(txtDateFrom.Text, 1, 2)
                tmpDatTo = Mid(txtDateTo.Text, 7, 4) & Mid(txtDateTo.Text, 4, 2) & Mid(txtDateTo.Text, 1, 2)
                tmpDatFrom = DecodeSsimDayFormat(tmpDatFrom, "CEDA", "SSIM2")
                tmpDatTo = DecodeSsimDayFormat(tmpDatTo, "CEDA", "SSIM2")
                If tmpFdat = tmpDatFrom Or tmpFdat = tmpDatTo Then
                    chkFlightList(4).Value = 1
                End If
            End If
        End If
    End If
    tmpUrno = GetFieldValue("URNO", cpData, cpFields)
    If tmpUrno = "" Then tmpUrno = GetUrnoFromSqlKey(cpSelKey)
    If tmpUrno <> "" Then
        RcvRefresh = False
        InfRefresh = False
        RcvIdx = -1
        InfIdx = -1
        UrnoIdx = GetItemNo(DataPool.TlxTabFields, "URNO") - 1
        tmpList = DataPool.TelexData(CurMem).GetLinesByColumnValue(UrnoIdx, tmpUrno, 1)
        If tmpList <> "" Then
            LineIdx = Val(GetItem(tmpList, 1, ","))
            DataPool.TelexData(CurMem).SetFieldValues LineIdx, cpFields, cpData
            RefreshListData 0, tmpUrno
            RefreshListData 1, tmpUrno
        End If
        
        tmpRcvList = ""
        If FormIsLoaded("RcvTelex") Then
            RcvIdx = 1
            tmpRcvList = RcvTelex.tabTelexList(RcvIdx).GetLinesByColumnValue(7, tmpUrno, 1)
            If tmpRcvList = "" Then
                RcvIdx = 0
                tmpRcvList = RcvTelex.tabTelexList(RcvIdx).GetLinesByColumnValue(7, tmpUrno, 1)
            End If
            If tmpRcvList <> "" Then
                LineIdx = Val(GetItem(tmpRcvList, 1, ","))
                tmpData = GetFieldValue("TTYP", cpData, cpFields)
                If tmpData <> "" Then RcvTelex.tabTelexList(RcvIdx).SetColumnValue LineIdx, 2, tmpData
                tmpData = GetFieldValue("STYP", cpData, cpFields)
                If tmpData <> "" Then RcvTelex.tabTelexList(RcvIdx).SetColumnValue LineIdx, 3, tmpData
                tmpData = GetFieldValue("CDAT", cpData, cpFields)
                If tmpData <> "" Then RcvTelex.tabTelexList(RcvIdx).SetColumnValue LineIdx, 4, tmpData
                tmpData = GetFieldValue("TXNO", cpData, cpFields)
                If tmpData <> "" Then RcvTelex.tabTelexList(RcvIdx).SetColumnValue LineIdx, 5, tmpData
                If InStr(cpFields, "TXT1") > 0 Then
                    tmpData = Trim(GetFieldValue("TXT1", cpData, cpFields) & GetFieldValue("TXT2", cpData, cpFields))
                    If (tmpData <> "") Or (Not IgnoreEmptyText) Then
                        RcvTelex.tabTelexList(RcvIdx).SetColumnValue LineIdx, 6, tmpData
                    Else
                        If RcvIdx = 1 Then
                            RcvTelex.tabTelexList(RcvIdx).DeleteLine LineIdx
                            If RcvTelex.tabTelexList(RcvIdx).GetLineCount < 1 Then RcvTelex.Timer1.Enabled = False
                            RcvIdx = -1
                            tmpRcvList = ""
                        End If
                    End If
                End If
                RcvRefresh = True
            End If
        End If
        
        If (tmpRcvList = "") And (RcvIdx >= 0) Then
            If FormIsLoaded("RcvInfo") Then
                InfIdx = 1
                tmpInfList = RcvInfo.tabTelexList(InfIdx).GetLinesByColumnValue(7, tmpUrno, 1)
                If tmpInfList = "" Then
                    InfIdx = 0
                    tmpInfList = RcvInfo.tabTelexList(InfIdx).GetLinesByColumnValue(7, tmpUrno, 1)
                End If
                If tmpInfList <> "" Then
                    LineIdx = Val(GetItem(tmpInfList, 1, ","))
                    tmpData = GetFieldValue("TTYP", cpData, cpFields)
                    If tmpData <> "" Then RcvInfo.tabTelexList(InfIdx).SetColumnValue LineIdx, 2, tmpData
                    tmpData = GetFieldValue("STYP", cpData, cpFields)
                    If tmpData <> "" Then RcvInfo.tabTelexList(InfIdx).SetColumnValue LineIdx, 3, tmpData
                    tmpData = GetFieldValue("CDAT", cpData, cpFields)
                    If tmpData <> "" Then RcvInfo.tabTelexList(InfIdx).SetColumnValue LineIdx, 4, tmpData
                    tmpData = GetFieldValue("TXNO", cpData, cpFields)
                    If tmpData <> "" Then RcvInfo.tabTelexList(InfIdx).SetColumnValue LineIdx, 5, tmpData
                    If InStr(cpFields, "TXT1") > 0 Then
                        tmpData = Trim(GetFieldValue("TXT1", cpData, cpFields) & GetFieldValue("TXT2", cpData, cpFields))
                        If (tmpData <> "") Or (Not IgnoreEmptyText) Then
                            RcvInfo.tabTelexList(InfIdx).SetColumnValue LineIdx, 6, tmpData
                        Else
                            If InfIdx = 1 Then
                                RcvInfo.tabTelexList(InfIdx).DeleteLine LineIdx
                                If RcvInfo.tabTelexList(InfIdx).GetLineCount < 1 Then RcvInfo.Timer1.Enabled = False
                                InfIdx = -1
                                tmpInfList = ""
                            End If
                        End If
                    End If
                    InfRefresh = True
                End If
            End If
        End If
        
        tmpStat = GetFieldValue("STAT", cpData, cpFields)
        If tmpStat <> "" Then
            ColorPool.GetStatusColor tmpStat, "", myTextColor, myBackColor
            If FormIsLoaded("RcvTelex") Then
                If tmpRcvList <> "" Then
                    LineIdx = Val(GetItem(tmpRcvList, 1, ","))
                    RcvTelex.tabTelexList(RcvIdx).SetColumnValue LineIdx, 0, tmpStat
                    RcvTelex.tabTelexList(RcvIdx).SetLineColor LineIdx, myTextColor, myBackColor
                    RcvRefresh = True
                End If
            End If
            If FormIsLoaded("SndTelex") Then
                tmpList = SndTelex.tabTelexList.GetLinesByColumnValue(9, tmpUrno, 1)
                If tmpList <> "" Then
                    LineIdx = Val(GetItem(tmpList, 1, ","))
                    SndTelex.tabTelexList.SetColumnValue LineIdx, 0, tmpStat
                    SndTelex.tabTelexList.SetLineColor LineIdx, myTextColor, myBackColor
                    SndTelex.tabTelexList.Refresh
                End If
            End If
            If FormIsLoaded("OutTelex") Then
                tmpList = OutTelex.tabTelexList.GetLinesByColumnValue(7, tmpUrno, 1)
                If tmpList <> "" Then
                    LineIdx = Val(GetItem(tmpList, 1, ","))
                    OutTelex.tabTelexList.SetColumnValue LineIdx, 0, tmpStat
                    OutTelex.tabTelexList.SetLineColor LineIdx, myTextColor, myBackColor
                    OutTelex.tabTelexList.Refresh
                End If
            End If
            If FormIsLoaded("RcvInfo") Then
                If tmpInfList <> "" Then
                    LineIdx = Val(GetItem(tmpInfList, 1, ","))
                    RcvInfo.tabTelexList(InfIdx).SetColumnValue LineIdx, 0, tmpStat
                    RcvInfo.tabTelexList(InfIdx).SetLineColor LineIdx, myTextColor, myBackColor
                    InfRefresh = True
                End If
            End If
        End If
        
        tmpStat = GetFieldValue("WSTA", cpData, cpFields)
        If tmpStat <> "" Then
            ColorPool.GetStatusColor tmpStat, "", myTextColor, myBackColor
            If FormIsLoaded("RcvTelex") Then
                If tmpRcvList <> "" Then
                    LineIdx = Val(GetItem(tmpRcvList, 1, ","))
                    RcvTelex.tabTelexList(RcvIdx).SetColumnValue LineIdx, 1, tmpStat
                    RcvTelex.tabTelexList(RcvIdx).SetLineColor LineIdx, myTextColor, myBackColor
                    RcvRefresh = True
                End If
            End If
            If FormIsLoaded("SndTelex") Then
                tmpList = SndTelex.tabTelexList.GetLinesByColumnValue(9, tmpUrno, 1)
                If tmpList <> "" Then
                    LineIdx = Val(GetItem(tmpList, 1, ","))
                    SndTelex.tabTelexList.SetColumnValue LineIdx, 1, tmpStat
                    SndTelex.tabTelexList.SetLineColor LineIdx, myTextColor, myBackColor
                    SndTelex.tabTelexList.Refresh
                End If
            End If
            If FormIsLoaded("OutTelex") Then
                tmpList = OutTelex.tabTelexList.GetLinesByColumnValue(7, tmpUrno, 1)
                If tmpList <> "" Then
                    LineIdx = Val(GetItem(tmpList, 1, ","))
                    OutTelex.tabTelexList.SetColumnValue LineIdx, 1, tmpStat
                    OutTelex.tabTelexList.SetLineColor LineIdx, myTextColor, myBackColor
                    OutTelex.tabTelexList.Refresh
                End If
            End If
            If FormIsLoaded("RcvInfo") Then
                If tmpInfList <> "" Then
                    LineIdx = Val(GetItem(tmpInfList, 1, ","))
                    RcvInfo.tabTelexList(InfIdx).SetColumnValue LineIdx, 1, tmpStat
                    RcvInfo.tabTelexList(InfIdx).SetLineColor LineIdx, myTextColor, myBackColor
                    InfRefresh = True
                End If
            End If
            If MainIsOnline Then
                For idx = 2 To 3
                    tmpList = tabTelexList(idx).GetLinesByColumnValue(7, tmpUrno, 1)
                    If tmpList <> "" Then
                        LineIdx = Val(GetItem(tmpList, 1, ","))
                        tabTelexList(idx).SetColumnValue LineIdx, 1, tmpStat
                        tabTelexList(idx).SetLineColor LineIdx, myTextColor, myBackColor
                        tabTelexList(idx).Refresh
                    End If
                Next
            End If
        End If
        
        If RcvIdx = 1 Then
            If tmpRcvList <> "" Then
                LineIdx = Val(GetItem(tmpRcvList, 1, ","))
                tmpStat = RcvTelex.tabTelexList(1).GetColumnValue(LineIdx, 0)
                If (tmpStat <> "") And (InStr("W1", tmpStat) = 0) Then ShowTelex = True Else ShowTelex = False
                If ShowTelex Then
                    If StopOnline = False Then
                        RcvTelex.ShowHiddenTelex LineIdx
                        If MainIsOnline Then
                            ReadFolderCount = False
                            PrepareTelexExtract CurMem
                            ReadFolderCount = True
                            If fraFlightList.Visible Then
                                If chkAssFolder.Enabled = False Then
                                    chkFlightList(4).Value = 1
                                Else
                                    FlnuCol = CLng(GetRealItemNo(DataPool.TlxTabFields, "FLNU"))
                                    tmpLine = DataPool.TelexData(CurMem).GetLineCount - 1
                                    tmpFlnu = DataPool.TelexData(CurMem).GetColumnValue(tmpLine, FlnuCol)
                                    If tmpFlnu <> "" Then
                                        AddCountFlightTelex tmpFlnu
                                    End If
                                End If
                            End If
                            chkTlxTitleBar(0).Caption = CStr(tabTelexList(2).GetLineCount) & " " & chkTlxTitleBar(0).Tag & " (Online)"
                            chkTlxTitleBar(1).Caption = CStr(tabTelexList(3).GetLineCount) & " " & chkTlxTitleBar(1).Tag & " (Online)"
                            chkTlxCnt(0).Caption = CStr(tabTelexList(2).GetLineCount)
                            chkTlxCnt(1).Caption = CStr(tabTelexList(3).GetLineCount)
                        End If
                    End If
                End If
            End If
        End If
        If InfIdx = 1 Then
            If tmpInfList <> "" Then
                LineIdx = Val(GetItem(tmpInfList, 1, ","))
                tmpStat = RcvInfo.tabTelexList(1).GetColumnValue(LineIdx, 0)
                If (tmpStat <> "") And (InStr("W1", tmpStat) = 0) Then ShowTelex = True Else ShowTelex = False
                If ShowTelex Then RcvInfo.ShowHiddenTelex LineIdx
            End If
        End If
        
        If RcvRefresh Then RcvTelex.tabTelexList(RcvIdx).Refresh
        If InfRefresh Then RcvInfo.tabTelexList(InfIdx).Refresh
    End If
End Sub
Private Sub RefreshListData(Index As Integer, RecUrno As String)
    Dim tmpList As String
    Dim LineIdx 'as Variant
    Dim DataIdx 'as long
    Dim RecData As String
    Dim RecLine As String
    Dim tmpStat As String
    Dim myTextColor As Long
    Dim myBackColor As Long
    tmpList = tabTelexList(Index).GetLinesByColumnValue(7, RecUrno, 1)
    If tmpList <> "" Then
        LineIdx = GetItem(tmpList, 1, ",")
        DataIdx = tabTelexList(Index).GetColumnValue(LineIdx, 8)
        If DataIdx <> "" Then
            RecData = DataPool.TelexData(CurMem).GetLineValues(DataIdx)
            RecLine = BuildTlxBrowser("RS", DataPool.TlxTabFields, RecData, True) & DataIdx
            tabTelexList(Index).UpdateTextLine LineIdx, RecLine, True
            tmpStat = GetFieldValue("STAT", RecData, DataPool.TlxTabFields.Text)
            ColorPool.GetStatusColor tmpStat, "", myTextColor, myBackColor
            tabTelexList(Index).SetLineColor LineIdx, myTextColor, myBackColor
            tabTelexList(Index).RedrawTab
        End If
    End If
End Sub

Public Function EvaluateBc(LineNo As Long, BcNum As String, DestName As String, RecvName As String, cpTable As String, cpCmd As String, cpSelKey As String, cpFields As String, cpData As String) As Long
    Dim clTlxLin As String
    Dim tmpSere As String
    Dim tmpStat As String
    Dim tmpWsta As String
    Dim tmpType As String
    Dim tmpTxt1 As String
    Dim tmpUrno As String
    Dim FilterLookUp As String
    Dim RcvIdx As Integer
    Dim InfIdx As Integer
    Dim myTextColor As Long
    Dim myBackColor As Long
    Dim MaxLinCnt As Long
    Dim UseLineNo As Long
    Dim ShowTelex As Boolean
    Dim PutInRcv As Boolean
    Dim PutInInf As Boolean
    Dim NewLine As Long
    Dim OnlLineNo As Long
    
    MaxLinCnt = 0
    If LockBroadcasts = False Then
        If cpTable = "TLXTAB" Then
            tmpSere = GetFieldValue("SERE", cpData, cpFields)
            'Also called from RefreshOnlineWindows
            If InStr("IBT,IRT", cpCmd) > 0 Then
                tmpTxt1 = Trim(GetFieldValue("TXT1", cpData, cpFields) & GetFieldValue("TXT2", cpData, cpFields))
                If (tmpTxt1 <> "") Or (Not IgnoreEmptyText) Then
                    ShowTelex = True
                    If ShowTelex Then
                        tmpType = GetFieldValue("TTYP", cpData, cpFields)
                        tmpStat = GetFieldValue("STAT", cpData, cpFields)
                        tmpWsta = GetFieldValue("WSTA", cpData, cpFields)
                        If tmpWsta <> "" Then
                            ColorPool.GetStatusColor tmpWsta, "", myTextColor, myBackColor
                        Else
                            ColorPool.GetStatusColor tmpStat, "", myTextColor, myBackColor
                        End If
                        If tmpSere = "R" Then
                            If FormIsLoaded("RcvTelex") Then
                                If InStr("W1", tmpStat) > 0 Then
                                    If MainIsOnline Then DataPool.InsertOnlineTelex 9, cpFields, cpData
                                    RcvIdx = 1
                                    clTlxLin = BuildTlxBrowser("RS", cpFields, cpData, False) & ",0"
                                    RcvTelex.tabTelexList(RcvIdx).InsertTextLineAt 0, clTlxLin, False
                                    RcvTelex.tabTelexList(RcvIdx).TimerSetValue 0, 60
                                    RcvTelex.Timer1.Enabled = True
                                Else
                                    RcvIdx = 0
                                    If MySetUp.AddrFilter.Value = 1 Then ShowTelex = FilterTelexAddress(tmpTxt1, MySetUp.HomeTlxAddr.Text, AddrNoCheck, tmpType, True) Else ShowTelex = True
                                    ShowTelex = FilterTelexText(tmpTxt1, IgnoreAllTlxText, ShowTelex)
                                    ShowTelex = FilterTelexTypeText(tmpType, tmpTxt1, TextFilterList, ShowTelex)
                                    'If (tmpTxt1 = "") And (IgnoreEmptyText) Then ShowTelex = False
                                    If ShowTelex Then
                                        FilterLookUp = MySetUp.CheckOnlineFilter("LOOKUP_RCV", tmpType)
                                        If FilterLookUp = "OK" Then
                                            PutInRcv = True
                                            If PutInRcv Then
                                                If MainIsOnline Then DataPool.InsertOnlineTelex 9, cpFields, cpData
                                                MaxLinCnt = RcvTelex.tabTelexList(RcvIdx).GetLineCount
                                                If LineNo >= 0 Then UseLineNo = LineNo Else UseLineNo = MaxLinCnt
                                                clTlxLin = BuildTlxBrowser("RS", cpFields, cpData, True) & ",0"
                                                RcvTelex.tabTelexList(RcvIdx).InsertTextLineAt UseLineNo, clTlxLin, True
                                                RcvTelex.tabTelexList(RcvIdx).SetLineColor UseLineNo, myTextColor, myBackColor
                                                If MainIsOnline Then
                                                    tabTelexList(2).InsertTextLineAt UseLineNo, clTlxLin, True
                                                    tabTelexList(2).SetLineColor UseLineNo, myTextColor, myBackColor
                                                    tmpUrno = Trim(tabTelexList(2).GetColumnValue(UseLineNo, 7))
                                                    OnlLineNo = DataPool.ReleaseOnlineTelex(tmpUrno)
                                                    tabTelexList(2).SetColumnValue UseLineNo, 8, CStr(OnlLineNo)
                                                    'tabTelexList(2).AutoSizeColumns
                                                End If
                                                MaxLinCnt = MaxLinCnt + 1
                                                If LineNo = 0 Then
                                                    If MaxLinCnt > MaxOnlineCount Then
                                                        MaxLinCnt = MaxLinCnt - 1
                                                        RcvTelex.tabTelexList(RcvIdx).DeleteLine MaxLinCnt
                                                        If MainIsOnline Then
                                                            MaxLinCnt = MaxLinCnt + 1
                                                            'tabTelexList(2).DeleteLine MaxLinCnt
                                                        End If
                                                    End If
                                                    RcvTelex.Caption = CStr(MaxLinCnt) & " " & RcvTelex.Tag
                                                End If
                                                If MainIsOnline Then
                                                    chkTlxTitleBar(0).Caption = CStr(MaxLinCnt) & " " & chkTlxTitleBar(0).Tag & " (Online)"
                                                    chkTlxCnt(0).Caption = CStr(tabTelexList(2).GetLineCount)
                                                    'tabTelexList(2).RedrawTab
                                                Else
                                                    RcvTelex.tabTelexList(RcvIdx).RedrawTab
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        ElseIf tmpSere = "I" Then
                            If FormIsLoaded("RcvInfo") Then
                                If InStr("W1", tmpStat) > 0 Then
                                    InfIdx = 1
                                    clTlxLin = BuildTlxBrowser("RS", cpFields, cpData, False) & ",0"
                                    RcvInfo.tabTelexList(InfIdx).InsertTextLineAt 0, clTlxLin, False
                                    RcvInfo.tabTelexList(InfIdx).TimerSetValue 0, 60
                                    RcvInfo.Timer1.Enabled = True
                                Else
                                    InfIdx = 0
                                    If MySetUp.AddrFilter.Value = 1 Then ShowTelex = FilterTelexAddress(tmpTxt1, MySetUp.HomeTlxAddr.Text, AddrNoCheck, tmpType, True) Else ShowTelex = True
                                    ShowTelex = FilterTelexText(tmpTxt1, IgnoreAllTlxText, ShowTelex)
                                    ShowTelex = FilterTelexTypeText(tmpType, tmpTxt1, TextFilterList, ShowTelex)
                                    'If (tmpTxt1 = "") And (IgnoreEmptyText) Then ShowTelex = False
                                    If ShowTelex Then
                                        FilterLookUp = MySetUp.CheckOnlineFilter("LOOKUP_RCV", tmpType)
                                        If FilterLookUp = "OK" Then
                                            PutInInf = True
                                            If InfoTypeList <> "" And tmpType <> "" Then
                                                If InStr(InfoTypeList, tmpType) = 0 Then PutInInf = False
                                            End If
                                            If PutInInf Then
                                                MaxLinCnt = RcvInfo.tabTelexList(0).GetLineCount
                                                If LineNo >= 0 Then UseLineNo = LineNo Else UseLineNo = MaxLinCnt
                                                clTlxLin = BuildTlxBrowser("RS", cpFields, cpData, True) & ",0"
                                                RcvInfo.tabTelexList(0).InsertTextLineAt UseLineNo, clTlxLin, True
                                                RcvInfo.tabTelexList(0).SetLineColor UseLineNo, myTextColor, myBackColor
                                                MaxLinCnt = MaxLinCnt + 1
                                                If LineNo = 0 Then
                                                    If MaxLinCnt > MaxOnlineCount Then
                                                        MaxLinCnt = MaxLinCnt - 1
                                                        RcvInfo.tabTelexList(0).DeleteLine MaxLinCnt
                                                    End If
                                                    RcvInfo.Caption = CStr(MaxLinCnt) & " " & RcvTelex.Tag
                                                End If
                                                RcvInfo.tabTelexList(0).RedrawTab
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        ElseIf tmpSere = "S" Then
                            If FormIsLoaded("OutTelex") Then
                                If MySetUp.AddrFilter.Value = 1 Then ShowTelex = FilterTelexAddress(tmpTxt1, MySetUp.HomeTlxAddr.Text, AddrNoCheck, tmpType, True) Else ShowTelex = True
                                ShowTelex = FilterTelexText(tmpTxt1, IgnoreAllTlxText, ShowTelex)
                                ShowTelex = FilterTelexTypeText(tmpType, tmpTxt1, TextFilterList, ShowTelex)
                                If ShowTelex Then
                                    FilterLookUp = MySetUp.CheckOnlineFilter("LOOKUP_OUT", tmpType)
                                    If FilterLookUp = "OK" Then
                                        If MainIsOnline Then DataPool.InsertOnlineTelex 9, cpFields, cpData
                                        MaxLinCnt = OutTelex.tabTelexList.GetLineCount
                                        If LineNo >= 0 Then UseLineNo = LineNo Else UseLineNo = MaxLinCnt
                                        clTlxLin = BuildTlxBrowser("RS", cpFields, cpData, True) & ",0"
                                        OutTelex.tabTelexList.InsertTextLineAt UseLineNo, clTlxLin, True
                                        OutTelex.tabTelexList.SetLineColor UseLineNo, myTextColor, myBackColor
                                        If MainIsOnline Then
                                            tabTelexList(3).InsertTextLineAt UseLineNo, clTlxLin, True
                                            tabTelexList(3).SetLineColor UseLineNo, myTextColor, myBackColor
                                            tmpUrno = Trim(tabTelexList(3).GetColumnValue(UseLineNo, 7))
                                            OnlLineNo = DataPool.ReleaseOnlineTelex(tmpUrno)
                                            tabTelexList(3).SetColumnValue UseLineNo, 8, CStr(OnlLineNo)
                                            'tabTelexList(3).AutoSizeColumns
                                        End If
                                        MaxLinCnt = MaxLinCnt + 1
                                        If LineNo = 0 Then
                                            If MaxLinCnt > MaxOnlineCount Then
                                                MaxLinCnt = MaxLinCnt - 1
                                                OutTelex.tabTelexList.DeleteLine MaxLinCnt
                                                If MainIsOnline Then
                                                    MaxLinCnt = MaxLinCnt + 1
                                                    'tabTelexList(3).DeleteLine MaxLinCnt
                                                End If
                                            End If
                                            OutTelex.Caption = CStr(MaxLinCnt) & " " & OutTelex.Tag
                                        End If
                                        If MainIsOnline Then
                                            'PrepareTelexExtract CurMem
                                            chkTlxTitleBar(1).Caption = CStr(MaxLinCnt) & " " & chkTlxTitleBar(1).Tag & " (Online)"
                                            chkTlxCnt(1).Caption = CStr(tabTelexList(3).GetLineCount)
                                            'tabTelexList(3).RedrawTab
                                        Else
                                            OutTelex.tabTelexList.RedrawTab
                                        End If
                                    End If
                                End If
                            End If
                        ElseIf tmpSere = "C" Then
                            If FormIsLoaded("SndTelex") Then
                                MaxLinCnt = SndTelex.tabTelexList.GetLineCount
                                If LineNo >= 0 Then UseLineNo = LineNo Else UseLineNo = MaxLinCnt
                                clTlxLin = BuildTlxBrowser("C", cpFields, cpData, True) & ",0"
                                SndTelex.tabTelexList.InsertTextLineAt UseLineNo, clTlxLin, True
                                SndTelex.tabTelexList.SetLineColor UseLineNo, myTextColor, myBackColor
                                MaxLinCnt = MaxLinCnt + 1
                                If LineNo = 0 Then
                                    If MaxLinCnt > MaxOnlineCount Then
                                        MaxLinCnt = MaxLinCnt - 1
                                        SndTelex.tabTelexList.DeleteLine MaxLinCnt
                                    End If
                                    SndTelex.Caption = CStr(MaxLinCnt) & " " & SndTelex.Tag
                                End If
                                SndTelex.tabTelexList.RedrawTab
                            End If
                        End If
                    End If
                End If
            ElseIf InStr("UBT,URT", cpCmd) > 0 Then
                DistributeBroadcast BcNum, DestName, RecvName, cpTable, cpCmd, cpSelKey, cpFields, cpData
            End If
        ElseIf cpTable = "AFTTAB" Then
            'Nothing to do
        ElseIf cpTable = "TFRTAB" Then
            'Nothing to do
        ElseIf cpTable = "TFNTAB" Or cpTable = "TLKTAB" Then
            DistributeBroadcast BcNum, DestName, RecvName, cpTable, cpCmd, cpSelKey, cpFields, cpData
        End If
    End If
    EvaluateBc = MaxLinCnt
End Function
Private Function GetOnlineTimeFrame(Caller As String) As String
    Dim UseServerTime
    Dim BeginTime
    Dim EndTime
    Dim TimeFrom As String
    Dim TimeTo As String
    UseServerTime = CedaFullDateToVb(MySetUp.ServerTime.Tag)
    If MySetUp.ServerTimeType <> "UTC" Then UseServerTime = DateAdd("n", -UtcTimeDiff, UseServerTime)
    If Caller <> "INF" Then
        BeginTime = DateAdd("h", -Abs(Val(MySetUp.OnlineFrom)), UseServerTime)
        EndTime = DateAdd("h", Abs(Val(MySetUp.OnlineTo)), UseServerTime)
    Else
        BeginTime = DateAdd("d", -Abs(Val("2")), UseServerTime)
        EndTime = DateAdd("d", Abs(Val("2")), UseServerTime)
    End If
    TimeFrom = Format(BeginTime, "yyyymmddhhmmss")
    TimeTo = Format(EndTime, "yyyymmddhhmmss")
    GetOnlineTimeFrame = TimeFrom & "," & TimeTo
End Function
Public Sub RefreshOnlineWindows(Caller As String)
    Dim SqlKey As String
    Dim RetVal As Integer
    Dim tmpVal As String
    Dim tmpFields As String
    Dim tmpTimeFilter As String
    Dim tmpCdatFilter As String
    Dim tmpRcvFilter As String
    Dim tmpOutFilter As String
    Dim tmpInfFilter As String
    Dim tmpRefFilter As String
    Dim UrnoCol As Long
    Dim count As Long
    Dim TlxRec As String
    Dim i As Long
    Dim j As Long
    Dim MemIdx As Integer
    Dim sbText As String
    Dim sbText1 As String
    Dim sbText2 As String
    Dim TimeFrom As String
    Dim TimeTo As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim clTtypFilter As String
    Dim tmpDatFrom As String
    Dim tmpDatTo As String
    Dim ActResult As String
    Dim ActCmd As String
    Dim ActTable As String
    Dim ActFldLst As String
    Dim ActCondition As String
    Dim ActOrder As String
    Dim ActDatLst As String
    Dim TlxUrnoList As String
    Dim tmpAddressFilter
    
    sbText1 = MyStatusBar.Panels(1).Text
    sbText2 = MyStatusBar.Panels(2).Text
    MyStatusBar.Panels(1).Text = "Loading Online Data ..."
    count = 0
    MemIdx = 7
    tmpVal = GetOnlineTimeFrame(Caller)
    TimeFrom = GetItem(tmpVal, 1, ",")
    TimeTo = GetItem(tmpVal, 2, ",")
    SqlKey = ""
    tmpTimeFilter = "(TIME BETWEEN '" & TimeFrom & "' AND '" & TimeTo & "')"
    tmpRcvFilter = ""
    If Caller = "RCV" Then
        If FormIsLoaded("RcvTelex") Then
            RcvTelex.tabTelexList(0).ResetContent
            RcvTelex.tabTelexList(1).ResetContent
            RcvTelex.Caption = count & " " & RcvTelex.Tag
            RcvTelex.tabTelexList(0).Refresh
            tmpRcvFilter = MySetUp.CheckOnlineFilter("RCV_FILTER", "")
            If tmpRcvFilter <> "" Then tmpRcvFilter = "(SERE='R' AND " & tmpRcvFilter & ")"
            SqlKey = "WHERE " & tmpTimeFilter & " AND " & tmpRcvFilter & " AND WSTA<>'X'"
            sbText = "Received Telexes ..."
            MemIdx = 4
        End If
    End If
    If Caller = "SND" Then
        If FormIsLoaded("SndTelex") Then
            SndTelex.tabTelexList.ResetContent
            SndTelex.Caption = count & " " & SndTelex.Tag
            SndTelex.tabTelexList.Refresh
            SqlKey = "WHERE " & tmpTimeFilter & " AND (SERE='C' AND STAT='C' AND TTYP<>'AAD' AND TTYP<>' ') AND WSTA<>'X'"
            sbText = "Created Telexes ..."
            MemIdx = 5
        End If
    End If
    tmpOutFilter = ""
    If Caller = "OUT" Then
        If FormIsLoaded("OutTelex") Then
            OutTelex.tabTelexList.ResetContent
            OutTelex.Caption = count & " " & OutTelex.Tag
            OutTelex.tabTelexList.Refresh
            tmpOutFilter = MySetUp.CheckOnlineFilter("OUT_FILTER", "")
            If tmpOutFilter <> "" Then tmpOutFilter = "(SERE='S' AND " & tmpOutFilter & ")"
            SqlKey = "WHERE " & tmpTimeFilter & " AND " & tmpOutFilter & " AND WSTA<>'X'"
            sbText = "Sent Telexes ..."
            MemIdx = 6
        End If
    End If
    If Caller = "INF" Then
        If FormIsLoaded("RcvInfo") Then
            RcvInfo.tabTelexList(0).ResetContent
            RcvInfo.tabTelexList(1).ResetContent
            RcvInfo.Caption = count & " " & RcvInfo.Tag
            RcvInfo.tabTelexList(0).Refresh
            tmpInfFilter = MySetUp.CheckOnlineFilter("INF_FILTER", "")
            If tmpInfFilter <> "" Then tmpInfFilter = "(SERE='I' AND " & tmpInfFilter & ")"
            SqlKey = "WHERE " & tmpTimeFilter & " AND " & tmpInfFilter
            sbText = "Info Messages ..."
            MemIdx = 7
        End If
    End If
    If Caller = "REF" Then
        GetValidTimeFrame tmpVpfr, tmpVpto, True
        tmpTimeFilter = "(TIME BETWEEN '" & tmpVpfr & "' AND '" & tmpVpto & "')"
'MsgBox NewFilterValues(8).TlxKey
'MsgBox NewFilterValues(8).TfrKey
        clTtypFilter = BuildTtypFilter
        tmpRefFilter = "(SERE IN ('R','S'))"
        tmpCdatFilter = "(CDAT >= '" & tmpVpfr & "')"
        SqlKey = "WHERE " & tmpTimeFilter & " AND (" & clTtypFilter & ") AND " & tmpRefFilter & " AND WSTA<>'X' AND " & tmpCdatFilter
        If Len(txtAddr.Text) > 0 Then
            tmpAddressFilter = " AND (TXT1 LIKE '%" & txtAddr.Text & "%')"
            SqlKey = SqlKey & tmpAddressFilter
        End If
        If Len(AddFolderList) > 0 Then
            tmpDatFrom = Mid(txtDateFrom.Text, 7, 4) & Mid(txtDateFrom.Text, 4, 2) & Mid(txtDateFrom.Text, 1, 2)
            tmpDatTo = Mid(txtDateTo.Text, 7, 4) & Mid(txtDateTo.Text, 4, 2) & Mid(txtDateTo.Text, 1, 2)
            If tmpDatFrom <> tmpDatTo Then
                tmpDatFrom = "WHERE (DATC = '" & tmpDatFrom & "' OR DATC = '" & tmpDatTo & "')"
            Else
                tmpDatFrom = "WHERE DATC = '" & tmpDatFrom & "'"
            End If
            TlxUrnoList = ""
            ActResult = ""
            ActCmd = "RTA"
            ActTable = "TFNTAB"
            ActFldLst = "DISTINCT TURN"
            ActCondition = tmpDatFrom
            ActOrder = ""
            ActDatLst = ""
            RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, False, False)
            If RetVal >= 0 Then
                count = UfisServer.DataBuffer(0).GetLineCount - 1
                If count >= 0 Then
                    For i = 0 To count
                        TlxUrnoList = TlxUrnoList & UfisServer.DataBuffer(0).GetLineValues(i) & ","
                    Next
                    TlxUrnoList = Left(TlxUrnoList, Len(TlxUrnoList) - 1)
                    'TlxUrnoList = "WHERE URNO IN (" & TlxUrnoList & ")"
                End If
            End If
        End If
        sbText = "Online Telexes ..."
        MemIdx = 8
        For i = 0 To LastFolderIndex + 1
            'MsgBox fraFolder(i).Tag & "," & fraFolder(i).Caption & vbLf & chkTabs(i).Caption
            chkTabs(i).Caption = ""
            chkTabs(i).BackColor = vbButtonFace
        Next
        chkTlxTitleBar(0).Caption = "0 " & chkTlxTitleBar(0).Tag & " (Online)"
        chkTlxTitleBar(1).Caption = "0 " & chkTlxTitleBar(1).Tag & " (Online)"
        chkTlxCnt(0).Caption = ""
        chkTlxCnt(1).Caption = ""
    End If
    
    'MsgBox SqlKey
    If SqlKey <> "" Then
        MyStatusBar.Panels(2).Text = sbText
        CedaStatus(0).Visible = True
        CedaStatus(0).ZOrder
        CedaStatus(0).Refresh
        Screen.MousePointer = 11
        tmpFields = DataPool.TlxTabFields
        DataPool.TelexData(MemIdx).ResetContent
        DataPool.TelexData(MemIdx).CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
        DataPool.TelexData(MemIdx).CedaHopo = UfisServer.HOPO
        DataPool.TelexData(MemIdx).CedaIdentifier = "IDX"
        DataPool.TelexData(MemIdx).CedaPort = "3357"
        DataPool.TelexData(MemIdx).CedaReceiveTimeout = "250"
        DataPool.TelexData(MemIdx).CedaRecordSeparator = vbLf
        DataPool.TelexData(MemIdx).CedaSendTimeout = "250"
        DataPool.TelexData(MemIdx).CedaServerName = UfisServer.HostName
        DataPool.TelexData(MemIdx).CedaTabext = UfisServer.TblExt
        DataPool.TelexData(MemIdx).CedaUser = UfisServer.ModName
        DataPool.TelexData(MemIdx).CedaWorkstation = UfisServer.GetMyWorkStationName
        If CedaIsConnected Then
            DataPool.TelexData(MemIdx).CedaAction "RTA", "TLXTAB", tmpFields, " ", SqlKey
            If Len(TlxUrnoList) > 0 Then
                TlxUrnoList = CheckTlxUrnolist(TlxUrnoList, MemIdx)
                If Len(TlxUrnoList) > 0 Then
                    DataPool.TelexData(MemIdx).CedaAction "RTA", "TLXTAB", tmpFields, " ", TlxUrnoList
                End If
            End If
            If SaveLocalTestData Then DataPool.SaveTestData MemIdx
        Else
            If SaveLocalTestData Then
                If UfisServer.HostName = "LOCAL" Then DataPool.LoadTestData MemIdx
            End If
        End If
        Me.Refresh
        DoEvents
        If Caller = "REF" Then
            tabTelexList(2).ResetContent
            tabTelexList(2).Refresh
            tabTelexList(3).ResetContent
            tabTelexList(3).Refresh
            RcvTelex.tabTelexList(0).ResetContent
            RcvTelex.tabTelexList(0).Refresh
            RcvTelex.tabTelexList(1).ResetContent
            RcvTelex.tabTelexList(1).Refresh
            OutTelex.tabTelexList.ResetContent
            OutTelex.tabTelexList.Refresh
        End If
        CedaStatus(0).Visible = False
        UrnoCol = CLng(GetRealItemNo(tmpFields, "URNO"))
        DataPool.TelexData(MemIdx).Sort CStr(UrnoCol), True, True
        count = DataPool.TelexData(MemIdx).GetLineCount - 1
        If count >= 0 Then
            For i = count To 0 Step -1
                TlxRec = DataPool.TelexData(MemIdx).GetLineValues(i)
                count = EvaluateBc(-1, "", "", "", "TLXTAB", "IBT", SqlKey, tmpFields, TlxRec)
                If Caller <> "REF" Then
                    If count >= MaxOnlineCount Then Exit For
                End If
                If (count Mod 100) = 0 Then DoEvents
            Next
        End If
        If Caller = "REF" Then
            tabTelexList(2).RedrawTab
            tabTelexList(3).RedrawTab
            tabTelexList(2).AutoSizeColumns
            tabTelexList(3).AutoSizeColumns
            'TelexTabToDataRelation (MemIdx)
        Else
            DataPool.TelexData(MemIdx).ResetContent
        End If
        If Caller = "RCV" Then
            If FormIsLoaded("RcvTelex") Then
                count = RcvTelex.tabTelexList(0).GetLineCount
                If count > 0 Then
                    RcvTelex.Caption = CStr(count) & " " & RcvTelex.Tag
                    RcvTelex.tabTelexList(0).AutoSizeColumns
                    RcvTelex.tabTelexList(0).Refresh
                End If
            End If
        End If
        If Caller = "SND" Then
            If FormIsLoaded("SndTelex") Then
                count = SndTelex.tabTelexList.GetLineCount
                If count > 0 Then
                    SndTelex.Caption = CStr(count) & " " & SndTelex.Tag
                    SndTelex.tabTelexList.AutoSizeColumns
                    SndTelex.tabTelexList.Refresh
                End If
            End If
        End If
        If Caller = "OUT" Then
            If FormIsLoaded("OutTelex") Then
                count = OutTelex.tabTelexList.GetLineCount
                If count > 0 Then
                    OutTelex.Caption = CStr(count) & " " & OutTelex.Tag
                    OutTelex.tabTelexList.AutoSizeColumns
                    OutTelex.tabTelexList.Refresh
                End If
            End If
        End If
        If Caller = "INF" Then
            If FormIsLoaded("RcvInfo") Then
                count = RcvInfo.tabTelexList(0).GetLineCount
                If count > 0 Then
                    RcvInfo.Caption = CStr(count) & " " & RcvInfo.Tag
                    RcvInfo.tabTelexList(0).AutoSizeColumns
                    RcvInfo.tabTelexList(0).Refresh
                End If
            End If
        End If
        If Caller = "REF" Then
            StoreFolderValues (MemIdx)
            ActFilterValues(MemIdx) = NewFilterValues(MemIdx)
            PrepareTelexExtract MemIdx
            If fraFlightList.Visible Then chkFlightList(4).Value = 1
        End If
        Screen.MousePointer = 0
    End If
    MyStatusBar.Panels(1).Text = sbText1
    MyStatusBar.Panels(2).Text = sbText2
End Sub
Private Sub ReorgLayers()
    Dim i As Integer
    TopButtons.Top = 120
    UfisFolder.ZOrder
    RcvButtonPanel(0).ZOrder
    fraSplitter(0).ZOrder
    txtTelexList(1).ZOrder
    tabTelexList(0).ZOrder
    tabTelexList(1).ZOrder
    tabTelexList(2).ZOrder
    tabTelexList(3).ZOrder
    AssignButtonPanel.ZOrder
    tabAssign(0).ZOrder
    fraListPanel.ZOrder
    fraTimeFilter.ZOrder
    fraFlightFilter.ZOrder
    fraTextFilter.ZOrder
    fraStatusFilter.ZOrder
    MainButtons.ZOrder
    RightCover.ZOrder
    For i = 0 To FolderButtons.UBound
        FolderButtons(i).ZOrder
    Next
    fraFolderToggles.ZOrder
    basicButtons.ZOrder
    TopButtons.ZOrder
    MyStatusBar.ZOrder
    CedaBusyFlag.ZOrder
End Sub
Private Function GetValidTimeFrame(Vpfr As String, Vpto As String, CheckUtc As Boolean) As Boolean
    Dim RetVal As Boolean
    Dim clDateFrom As String
    Dim clDateTo As String
    Dim clTimeFrom As String
    Dim clTimeTo As String
    Dim TimeVal
    RetVal = False
    clDateFrom = txtDateFrom.Tag
    clDateTo = txtDateTo.Tag
    clTimeFrom = Left(txtTimeFrom.Text, 2) & Right(txtTimeFrom.Text, 2) & "00"
    clTimeTo = Left(txtTimeTo.Text, 2) & Right(txtTimeTo.Text, 2) & "59"
    Vpfr = clDateFrom & clTimeFrom
    Vpto = clDateTo & clTimeTo
    If (CheckUtc) And (optLocal.Value = True) Then
        TimeVal = CedaFullDateToVb(Vpfr)
        TimeVal = DateAdd("n", -UtcTimeDiff, TimeVal)
        Vpfr = Format(TimeVal, "yyyymmddhhmmss")
        TimeVal = CedaFullDateToVb(Vpto)
        TimeVal = DateAdd("n", -UtcTimeDiff, TimeVal)
        Vpto = Format(TimeVal, "yyyymmddhhmmss")
    End If
    RetVal = True
    GetValidTimeFrame = RetVal
End Function

Private Sub txtTimeTo_KeyPress(KeyAscii As Integer)
    If (KeyAscii <> 8) And (Chr(KeyAscii) <> ":") Then
        If Not IsNumeric(Chr(KeyAscii)) Then KeyAscii = 0
    End If
End Sub

Private Sub UseCSGN_Change()
    If Trim(UseCSGN.Text) <> Trim(AtcCSGN.Text) Then
        CsgnStatus.Visible = True
    Else
        CsgnStatus.Visible = False
    End If
End Sub
Private Function LoadFlightData(Index As Integer, FetchData As Boolean) As String
    Dim tmpFrom As String
    Dim tmpTo As String
    Dim tmpTag As String
    Dim LastFrom As String
    Dim LastTo As String
    Dim UseSqlKey As String
    Dim tmpSqlKey As String
    Dim tmpFields As String
    Dim CurDataFile As String
    Dim sbText1 As String
    Dim sbText2 As String
    Dim tmpErrMsg As String
    Dim tmpLoopTimes As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim MaxLine As Long
    Dim RetCode As Boolean
    Dim FraWasVisible As Boolean
    Dim SqlTimeBegin
    Dim SqlTimeEnd
    Dim SqlTimeMax
    
    GetValidTimeFrame tmpFrom, tmpTo, True
    tmpTag = fraFlightList.Tag
    LastFrom = GetItem(tmpTag, 1, ",")
    LastTo = GetItem(tmpTag, 2, ",")
    If Index = 0 Then
        tmpFields = "' ',FTYP,FLNO,STOA,ADID,DOOA,REGN,ACT3,ORG3,VIA3,TIFA,TISA,REMP,REM1,REM2,URNO,RKEY"
        'If FetchData Then
        '    tmpSqlKey = "(STOA BETWEEN '" & tmpFrom & "' AND '" & tmpTo & "')"
        'Else
            tmpSqlKey = "(STOA BETWEEN '[LOOPVPFR]00' AND '[LOOPVPTO]59')"
        'End If
        tmpSqlKey = tmpSqlKey & " AND DES3='" & HomeAirport & "'"
        tmpSqlKey = tmpSqlKey & " AND FTYP IN ('O','S','X','N','D','R','B','Z')"
    Else
        tmpFields = "' ',FTYP,FLNO,STOD,ADID,DOOD,REGN,ACT3,DES3,VIA3,TIFD,TISD,REMP,REM1,REM2,URNO,RKEY"
        'If FetchData Then
        '    tmpSqlKey = "(STOD BETWEEN '" & tmpFrom & "' AND '" & tmpTo & "')"
        'Else
            tmpSqlKey = "(STOD BETWEEN '[LOOPVPFR]00' AND '[LOOPVPTO]59')"
        'End If
        tmpSqlKey = tmpSqlKey & " AND ORG3='" & HomeAirport & "'"
        tmpSqlKey = tmpSqlKey & " AND FTYP IN ('O','S','X','N','D','R','B','Z')"
    End If
    If FetchData Then
        If (LastFrom <> tmpFrom) Or (LastTo <> tmpTo) Then
            MyStatusBar.Panels(1).Text = "Loading Data ..."
            chkFlightList(0).Tag = "LOADED"
            If Index = 0 Then
                fraLoadProgress.Caption = "Loading Arrival Flights ..."
                MyStatusBar.Panels(2).Text = "Arrival Flights"
            Else
                fraLoadProgress.Caption = "Loading Departure Flights ..."
                MyStatusBar.Panels(2).Text = "Departure Flights"
            End If
            Me.MousePointer = 11
            Me.Refresh
            DoEvents
            tmpVpfr = tmpFrom
            tmpVpto = tmpTo
            tmpSqlKey = "WHERE " & tmpSqlKey
            tabFlightList(Index).ResetContent
            tabFlightList(Index).Refresh
            tabFlightList(Index).CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
            tabFlightList(Index).CedaHopo = UfisServer.HOPO
            tabFlightList(Index).CedaIdentifier = "IDX"
            tabFlightList(Index).CedaPort = "3357"
            tabFlightList(Index).CedaReceiveTimeout = "250"
            tabFlightList(Index).CedaRecordSeparator = vbLf
            tabFlightList(Index).CedaSendTimeout = "250"
            tabFlightList(Index).CedaServerName = UfisServer.HostName
            tabFlightList(Index).CedaTabext = UfisServer.TblExt
            tabFlightList(Index).CedaUser = UfisServer.ModName
            tabFlightList(Index).CedaWorkstation = UfisServer.GetMyWorkStationName
            CurDataFile = "c:\tmp\TlxAftData" & CStr(Index) & ".txt"
            If CedaIsConnected Then
                FraWasVisible = fraLoadProgress.Visible
                fraLoadProgress.Visible = True
                SqlTimeBegin = CedaFullDateToVb(tmpVpfr)
                SqlTimeMax = CedaFullDateToVb(tmpVpto)
                While (SqlTimeBegin <= SqlTimeMax) And (Not StopLoading)
                    If Not StopLoading Then
                        SqlTimeEnd = DateAdd("n", 1439, SqlTimeBegin)
                        If SqlTimeEnd > SqlTimeMax Then SqlTimeEnd = SqlTimeMax
                        tmpVpfr = Format(SqlTimeBegin, "yyyymmddhhmm")
                        tmpVpto = Format(SqlTimeEnd, "yyyymmddhhmm")
                        txtLoadFrom.Text = Format(SqlTimeBegin, MySetUp.DefDateFormat)
                        txtLoadTo.Text = Format(SqlTimeEnd, MySetUp.DefDateFormat)
                        txtLoadTimeFrom.Text = Format(SqlTimeBegin, "hh:mm")
                        txtLoadTimeTo.Text = Format(SqlTimeEnd, "hh:mm")
                        MaxLine = tabFlightList(Index).GetLineCount
                        fraLoadProgress.Caption = CStr(MaxLine) & " Flights Loaded ..."
                        Me.Refresh
                        DoEvents
                        UseSqlKey = tmpSqlKey
                        UseSqlKey = Replace(UseSqlKey, "[LOOPVPFR]", tmpVpfr, 1, -1, vbBinaryCompare)
                        UseSqlKey = Replace(UseSqlKey, "[LOOPVPTO]", tmpVpto, 1, -1, vbBinaryCompare)
                        RetCode = tabFlightList(Index).CedaAction("GFR", "AFTTAB", tmpFields, "", UseSqlKey)
                        If (RetCode = False) And (tmpErrMsg = "") Then tmpErrMsg = tmpErrMsg & tabFlightList(Index).GetLastCedaError
                        Me.Refresh
                        SqlTimeBegin = DateAdd("n", 1, SqlTimeEnd)
                    End If
                Wend
                If SaveLocalTestData Then tabFlightList(Index).WriteToFile CurDataFile, False
                fraLoadProgress.Visible = FraWasVisible
            Else
                If SaveLocalTestData Then tabFlightList(Index).ReadFromFile CurDataFile
            End If
            tabFlightList(Index).Sort "2", True, True
            tabFlightList(Index).AutoSizeColumns
            tabFlightList(Index).IndexCreate "AFTURNO", 15
            tabFlightList(Index).IndexCreate "AFTRKEY", 16
            If Index = 1 Then fraFlightList.Tag = tmpFrom & "," & tmpTo
            MyStatusBar.Panels(1).Text = sbText1
            MyStatusBar.Panels(2).Text = sbText2
            Me.MousePointer = 0
            Me.Refresh
            DoEvents
        End If
    End If
    LoadFlightData = tmpSqlKey
End Function

Private Sub SelectFlightInfo(AftUrno As String, MemLineNo As Long)
    Dim LineNo As Long
    Dim CurIdx As Integer
    Dim HitList As String
    Dim tmpRkey As String
    Dim tmpFTag As String
    Dim tmpTag As String
    Dim tmpFld As String
    Dim UrnoList As String
    Dim ActResult As String
    Dim ActCmd As String
    Dim ActTable As String
    Dim ActFldLst As String
    Dim ActCondition As String
    Dim ActOrder As String
    Dim ActDatLst As String
    Dim RetVal As Integer
    Dim count As Integer
    Dim CurLin As Integer
    Dim itm As Integer
    Dim CurUrno As String
    Dim i As Integer
    Dim AftuCol As Long
    Dim tmpData As String
    Dim gPos As Integer
    
    HitList = ""
    For CurIdx = 0 To 1
        tmpFTag = tabFlightList(CurIdx).Tag
        tmpFld = GetItem(tmpFTag, 1, ",")
        itm = 2
        tmpTag = GetItem(tmpFTag, itm, ",")
        While tmpTag <> ""
            If tmpFld = "URNO" Then HitList = tabFlightList(CurIdx).GetLinesByIndexValue("AFTURNO", tmpTag, 0)
            If tmpFld = "RKEY" Then HitList = tabFlightList(CurIdx).GetLinesByIndexValue("AFTRKEY", tmpTag, 0)
            If HitList <> "" Then
                LineNo = Val(HitList)
                tabFlightList(CurIdx).SetLineColor LineNo, vbBlack, vbWhite
            End If
            itm = itm + 1
            tmpTag = GetItem(tmpFTag, itm, ",")
        Wend
        tabFlightList(CurIdx).Refresh
        tabFlightList(CurIdx).Tag = ""
        chkFlightList(CurIdx + 1).ForeColor = vbBlack
    Next
    UrnoList = AftUrno
    If chkAssFolder.Enabled Then
'        ActResult = ""
'        ActCmd = "RTA"
'        ActTable = "TLKTAB"
'        ActFldLst = "FURN"
'        ActCondition = "WHERE TURN = " & CurrentUrno.Text
'        ActOrder = ""
'        ActDatLst = ""
'        RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, False, False)
'        If RetVal >= 0 Then
'            count = UfisServer.DataBuffer(0).GetLineCount - 1
'            If count >= 0 Then
'                UrnoList = ""
'                For CurLin = count To 0 Step -1
'                    UrnoList = UrnoList & UfisServer.DataBuffer(0).GetLineValues(CurLin) & ","
'                Next
'                UrnoList = Left(UrnoList, Len(UrnoList) - 1)
'                If AftUrno <> "" And AftUrno <> "0" Then
'                    UrnoList = UrnoList & "," & AftUrno
'                End If
'            End If
'        End If
        AftuCol = CLng(GetRealItemNo(DataPool.TlxTabFields, "USEC"))
        tmpData = DataPool.TelexData(CurMem).GetColumnValue(MemLineNo, AftuCol)
        If AftUrno <> "" And AftUrno <> "0" Then
            If Len(tmpData) > 0 Then
                tmpData = tmpData & "," & AftUrno
            Else
                tmpData = AftUrno
            End If
        End If
        gPos = InStr(tmpData, "#")
        While gPos > 0
            Mid(tmpData, gPos, 1) = ","
            gPos = InStr(tmpData, "#")
        Wend
        UrnoList = tmpData
    End If
    itm = 1
    CurUrno = GetItem(UrnoList, itm, ",")
    While CurUrno <> ""
        chkFlightList(3).Enabled = False
        If CurUrno <> "" And Len(CurUrno) <= 10 Then
            CurIdx = 0
            HitList = tabFlightList(CurIdx).GetLinesByIndexValue("AFTURNO", CurUrno, 0)
            If HitList = "" Then
                CurIdx = 1
                HitList = tabFlightList(CurIdx).GetLinesByIndexValue("AFTURNO", CurUrno, 0)
            End If
            If HitList <> "" Then
                LineNo = Val(HitList)
                If tabFlightList(CurIdx).Tag = "" Then tabFlightList(CurIdx).OnVScrollTo LineNo
                If CurIdx = 0 Then
                    tabFlightList(CurIdx).SetLineColor LineNo, vbBlack, vbYellow
                Else
                    tabFlightList(CurIdx).SetLineColor LineNo, vbBlack, vbGreen
                End If
                If Len(tabFlightList(CurIdx).Tag) = 0 Then
                    tabFlightList(CurIdx).Tag = "URNO," & CurUrno
                Else
                    tabFlightList(CurIdx).Tag = tabFlightList(CurIdx).Tag & "," & CurUrno
                End If
                chkFlightList(CurIdx + 1).Value = 1
                tmpRkey = tabFlightList(CurIdx).GetColumnValue(LineNo, 16)
                chkFlightList(3).Enabled = True
                If CurIdx = 1 Then CurIdx = 0 Else CurIdx = 1
                HitList = tabFlightList(CurIdx).GetLinesByIndexValue("AFTRKEY", tmpRkey, 0)
                If HitList <> "" Then
                    LineNo = Val(HitList)
                    tabFlightList(CurIdx).OnVScrollTo LineNo
                    If CurIdx = 0 Then
                        tabFlightList(CurIdx).SetLineColor LineNo, vbBlack, vbYellow
                    Else
                        tabFlightList(CurIdx).SetLineColor LineNo, vbBlack, vbGreen
                    End If
                    chkFlightList(CurIdx + 1).ForeColor = vbBlue
                    If Len(tabFlightList(CurIdx).Tag) = 0 Then
                        tabFlightList(CurIdx).Tag = "RKEY," & tmpRkey
                    Else
                        tabFlightList(CurIdx).Tag = tabFlightList(CurIdx).Tag & "," & tmpRkey
                    End If
                End If
            End If
        End If
        itm = itm + 1
        CurUrno = GetItem(UrnoList, itm, ",")
    Wend
    For CurIdx = 0 To 1
        tabFlightList(CurIdx).Refresh
    Next
End Sub

Private Sub SyncFlightFlist()
    Dim LineNo As Long
    Dim CurIdx As Integer
    Dim tmpTag As String
    Dim HitList As String
    Dim tmpFld As String
    For CurIdx = 0 To 1
        tmpTag = tabFlightList(CurIdx).Tag
        tmpFld = GetItem(tmpTag, 1, ",")
        tmpTag = GetItem(tmpTag, 2, ",")
        If tmpFld = "URNO" Then HitList = tabFlightList(CurIdx).GetLinesByIndexValue("AFTURNO", tmpTag, 0)
        If tmpFld = "RKEY" Then HitList = tabFlightList(CurIdx).GetLinesByIndexValue("AFTRKEY", tmpTag, 0)
        If HitList <> "" Then
            LineNo = Val(HitList)
            tabFlightList(CurIdx).OnVScrollTo LineNo
        End If
        tabFlightList(CurIdx).Refresh
    Next
End Sub
Private Sub CountFlightTelex()
    Dim CurIdx As Integer
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim AftUrno As String
    Dim HitList As String
    Dim TlxCurLine As Long
    Dim TlxMaxLine As Long
    Dim TlxFlnu As String
    Dim TlxHitList As Long
    Dim AftUrnoList As String
    Dim ActResult As String
    Dim ActCmd As String
    Dim ActTable As String
    Dim ActFldLst As String
    Dim ActCondition As String
    Dim ActOrder As String
    Dim ActDatLst As String
    Dim RetVal As String
    Dim count As Long
    Dim itm As Integer
    Dim tmpDatFrom As String
    Dim tmpDatTo As String
    Dim TlxUrnoList As String
    Dim tmpData As String
    Dim TlxUrno As String
    
    Screen.MousePointer = 11
    DataPool.TelexData(CurMem).SetInternalLineBuffer True
    MyStatusBar.Panels(1).Text = "Counting Telexes"
    MyStatusBar.Panels(2).Text = "For Arrival Flights"
    For CurIdx = 0 To 1
        If CurIdx = 1 Then
            MyStatusBar.Panels(2).Text = "For Departure Flights"
        End If
        MaxLine = tabFlightList(CurIdx).GetLineCount - 1
        For CurLine = 0 To MaxLine
            AftUrno = tabFlightList(CurIdx).GetColumnValue(CurLine, 15)
            HitList = DataPool.TelexData(CurMem).GetLinesByIndexValue("FLNU", AftUrno, 0)
            If Val(HitList) = 0 Then HitList = ""
'            If CurMem <> 8 Then
'                HitList = DataPool.TelexData(CurMem).GetLinesByIndexValue("FLNU", AftUrno, 0)
'                If Val(HitList) = 0 Then HitList = ""
'            Else
'                TlxHitList = 0
'                TlxMaxLine = DataPool.TelexData(CurMem).GetLineCount - 1
'                For TlxCurLine = 0 To TlxMaxLine
'                    TlxFlnu = DataPool.TelexData(CurMem).GetColumnValue(TlxCurLine, 6)
'                    If TlxFlnu = AftUrno Then
'                        TlxHitList = TlxHitList + 1
'                    End If
'                Next
'                If TlxHitList = 0 Then
'                    HitList = ""
'                Else
'                    HitList = CStr(TlxHitList)
'                End If
'            End If
            tabFlightList(CurIdx).SetColumnValue CurLine, 0, HitList
        Next
        tabFlightList(CurIdx).Refresh
    Next
    DataPool.TelexData(CurMem).SetInternalLineBuffer False
    If chkAssFolder.Enabled Then
        MyStatusBar.Panels(1).Text = "Counting Assigned Telexes"
        MyStatusBar.Panels(2).Text = ""
        tmpDatFrom = Mid(txtDateFrom.Text, 7, 4) & Mid(txtDateFrom.Text, 4, 2) & Mid(txtDateFrom.Text, 1, 2)
        If Len(Trim(txtTimeFrom.Text)) = 5 Then
            tmpDatFrom = tmpDatFrom & Mid(txtTimeFrom.Text, 1, 2) & Mid(txtTimeFrom.Text, 4, 2) & "00"
        Else
            tmpDatFrom = tmpDatFrom & Mid(txtTimeFrom.Text, 1, 2) & Mid(txtTimeFrom.Text, 3, 2) & "00"
        End If
        tmpDatTo = Mid(txtDateTo.Text, 7, 4) & Mid(txtDateTo.Text, 4, 2) & Mid(txtDateTo.Text, 1, 2)
        If Len(Trim(txtTimeTo.Text)) = 5 Then
            tmpDatTo = tmpDatTo & Mid(txtTimeTo.Text, 1, 2) & Mid(txtTimeTo.Text, 4, 2) & "00"
        Else
            tmpDatTo = tmpDatTo & Mid(txtTimeTo.Text, 1, 2) & Mid(txtTimeTo.Text, 3, 2) & "00"
        End If
        tmpDatFrom = "WHERE DATC BETWEEN '" & tmpDatFrom & "' AND '" & tmpDatTo & "'"
        AftUrnoList = ""
        TlxUrnoList = ""
        ActResult = ""
        ActCmd = "RTA"
        ActTable = "TLKTAB"
        ActFldLst = "FURN,TURN"
        ActCondition = tmpDatFrom
        ActOrder = ""
        ActDatLst = ""
        RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, False, False)
        If RetVal >= 0 Then
            count = UfisServer.DataBuffer(0).GetLineCount - 1
            If count >= 0 Then
                For CurLine = 0 To count
                    tmpData = UfisServer.DataBuffer(0).GetLineValues(CurLine)
                    AftUrnoList = AftUrnoList & GetItem(tmpData, 1, ",") & ","
                    TlxUrnoList = TlxUrnoList & GetItem(tmpData, 2, ",") & ","
                Next
                If AftUrnoList <> "" Then
                    AftUrnoList = Left(AftUrnoList, Len(AftUrnoList) - 1)
                    TlxUrnoList = Left(TlxUrnoList, Len(TlxUrnoList) - 1)
                    itm = 1
                    AftUrno = GetItem(AftUrnoList, itm, ",")
                    TlxUrno = GetItem(TlxUrnoList, itm, ",")
                    While AftUrno <> ""
                        AddCountFlightTelex AftUrno
                        If TlxUrno <> "" Then
                            AssignFlightTelex TlxUrno, AftUrno
                        End If
                        itm = itm + 1
                        AftUrno = GetItem(AftUrnoList, itm, ",")
                        TlxUrno = GetItem(TlxUrnoList, itm, ",")
                    Wend
                End If
            End If
        End If
    End If
    MyStatusBar.Panels(1).Text = ""
    MyStatusBar.Panels(2).Text = ""
    Screen.MousePointer = 0
End Sub

Private Sub AssignFlightTelex(TlxUrno As String, AftUrno As String)
    Dim LineList As String
    Dim UrnoCol As Long
    Dim i As Integer
    Dim AftuCol As Long
    Dim tmpData As String
    
    UrnoCol = CLng(GetRealItemNo(DataPool.TlxTabFields, "URNO"))
    AftuCol = CLng(GetRealItemNo(DataPool.TlxTabFields, "USEC"))
    UrnoCol = GetItemNo(DataPool.TlxTabFields, "URNO") - 1
'MsgBox "AKL1" & vbLf & "<" & UrnoCol & ">" & vbLf & "<" & TlxUrno & ">"
'LineList = DataPool.TelexData(CurMem).GetLinesByColumnValue(UrnoCol, TlxUrno, 1)
'MsgBox "AKL2 " & i & vbLf & LineList
    For i = 0 To DataPool.TelexData(CurMem).GetLineCount - 1
        If DataPool.TelexData(CurMem).GetColumnValue(i, UrnoCol) = TlxUrno Then
            tmpData = DataPool.TelexData(CurMem).GetColumnValue(i, AftuCol)
            If Len(tmpData) = 0 Then
                DataPool.TelexData(CurMem).SetColumnValue i, AftuCol, AftUrno
            Else
                If InStr(tmpData, AftUrno) <= 0 Then
                    tmpData = tmpData & "#" & AftUrno
                    DataPool.TelexData(CurMem).SetColumnValue i, AftuCol, tmpData
                End If
            End If
            Exit For
        End If
    Next
End Sub
Private Sub AddCountFlightTelex(AftUrno As String)
    Dim CurIdx As Integer
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim tmpCount As String
    Dim found As Boolean
    
    Screen.MousePointer = 11
    found = False
    For CurIdx = 0 To 1
        MaxLine = tabFlightList(CurIdx).GetLineCount - 1
        For CurLine = 0 To MaxLine
            If AftUrno = tabFlightList(CurIdx).GetColumnValue(CurLine, 15) Then
                tmpCount = tabFlightList(CurIdx).GetColumnValue(CurLine, 0)
                If tmpCount = "" Then
                    tmpCount = "1"
                Else
                    tmpCount = tmpCount + 1
                End If
                tabFlightList(CurIdx).SetColumnValue CurLine, 0, tmpCount
                found = True
                Exit For
            End If
        Next
        tabFlightList(CurIdx).Refresh
        If found Then Exit For
    Next
    Screen.MousePointer = 0
End Sub

Public Sub MsgToImportTool(MyMsg As String)
    Static CurMsgNum As Long
    Dim tmpMsg As String
    Dim tmpData As String
    On Error GoTo ErrHdl
    If (Not ImportIsConnected) And (Not ImportIsPending) Then
        ImportIsPending = True
        InfoFromClient.Text = "WAIT"
        If Me.WindowState <> vbMinimized Then
            InfoToClient.Text = CStr(CurMsgNum) & ",0,INIT," & CStr(Me.Left + 300) & "," & CStr(Me.Top + 300) & "," & CStr(OnTop.Value)
        Else
            InfoToClient.Text = CStr(CurMsgNum) & ",0,INIT,1200,1200" & "," & CStr(OnTop.Value)
        End If
        tmpData = PathToImportTool & " COMPARE"
        Shell tmpData
        If RcvInfo.chkImp(0).Value = 1 Then
            CurMsgNum = CurMsgNum + 1
            tmpMsg = CStr(CurMsgNum) & "," & "0,ATSET"
            tmpMsg = Replace(tmpMsg, ",", Chr(15), 1, -1, vbBinaryCompare)
            InfoSpooler.InsertTextLine tmpMsg, False
        End If
        If RcvInfo.chkImp(1).Value = 1 Then
            CurMsgNum = CurMsgNum + 1
            tmpMsg = CStr(CurMsgNum) & "," & "0,AISET"
            tmpMsg = Replace(tmpMsg, ",", Chr(15), 1, -1, vbBinaryCompare)
            InfoSpooler.InsertTextLine tmpMsg, False
        End If
    End If
    CurMsgNum = CurMsgNum + 1
    tmpMsg = CStr(CurMsgNum) & "," & MyMsg
    tmpMsg = Replace(tmpMsg, ",", Chr(15), 1, -1, vbBinaryCompare)
    InfoSpooler.InsertTextLine tmpMsg, False
    InfoSpooler.AutoSizeColumns
    InfoSpooler.Refresh
    SpoolTimer.Enabled = True
    Exit Sub
ErrHdl:
    tmpMsg = Err.Description & vbNewLine & PathToImportTool
    If MyMsgBox.CallAskUser(0, 0, 0, "Application Control", tmpMsg, "stop", "", UserAnswer) = 1 Then DoNothing
    InfoSpooler.ResetContent
    InfoSpooler.Refresh
    SpoolTimer.Enabled = False
End Sub

Private Sub CheckCompareInfo()
    If ImportIsConnected Then
        If InfoFromClient.LinkMode = 0 Then
            InfoFromClient.LinkTopic = "ImportFlights|CompareInfo"
            InfoFromClient.LinkItem = "InfoToServer"
            InfoFromClient.LinkMode = vbLinkAutomatic
        End If
        If MsgFromClient.LinkMode = 0 Then
            MsgFromClient.LinkTopic = "ImportFlights|CompareInfo"
            MsgFromClient.LinkItem = "MsgToServer"
            MsgFromClient.LinkMode = vbLinkAutomatic
        End If
    End If
End Sub

Private Sub chkConfig_Click()

    chkConfig.Value = 0
    ConfigTool.Visible = True
End Sub

Private Sub TelexTabToDataRelation(MemIdx As Long)
    Dim TlxTabMax As Long
    Dim TlxDataMax As Long
    Dim i As Long
    Dim j As Long
    Dim TlxTabUrno As String
    Dim TlxDataUrno As String

    TlxDataMax = DataPool.TelexData(MemIdx).GetLineCount - 1
    TlxTabMax = tabTelexList(2).GetLineCount - 1
    For i = 0 To TlxTabMax
        tabTelexList(2).SetColumnValue i, 8, " "
        TlxTabUrno = tabTelexList(2).GetColumnValue(i, 7)
        For j = 0 To TlxDataMax
            TlxDataUrno = DataPool.TelexData(MemIdx).GetColumnValue(j, 23)
            If TlxDataUrno = TlxTabUrno Then
                tabTelexList(2).SetColumnValue i, 8, CStr(j)
                Exit For
            End If
        Next
    Next
    TlxTabMax = tabTelexList(3).GetLineCount - 1
    For i = 0 To TlxTabMax
        tabTelexList(3).SetColumnValue i, 8, " "
        TlxTabUrno = tabTelexList(3).GetColumnValue(i, 7)
        For j = 0 To TlxDataMax
            TlxDataUrno = DataPool.TelexData(MemIdx).GetColumnValue(j, 23)
            If TlxDataUrno = TlxTabUrno Then
                tabTelexList(3).SetColumnValue i, 8, CStr(j)
                Exit For
            End If
        Next
    Next
End Sub

Private Function GetMemRecNbr(Index As Integer, LineNo As Long, MemIdx As Integer) As String
    Dim TlxDataMax As Long
    Dim i As Long
    Dim TlxTabUrno As String
    Dim TlxDataUrno As String
    Dim Result As String

    Result = ""
    TlxDataMax = DataPool.TelexData(MemIdx).GetLineCount - 1
    tabTelexList(Index).SetColumnValue LineNo, 8, " "
    TlxTabUrno = tabTelexList(Index).GetColumnValue(LineNo, 7)
    For i = 0 To TlxDataMax
        TlxDataUrno = DataPool.TelexData(MemIdx).GetColumnValue(i, 23)
        If TlxDataUrno = TlxTabUrno Then
            tabTelexList(Index).SetColumnValue LineNo, 8, CStr(i)
            Result = CStr(i)
            Exit For
        End If
    Next
    GetMemRecNbr = Result
End Function

Private Sub tabAssign_InplaceEditCell(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal NewValue As String, ByVal OldValue As String)
'MsgBox Index & vbLf & LineNo & vbLf & ColNo & vbLf & NewValue & vbLf & OldValue

End Sub

Private Sub tabAssign_EditPositionChanged(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal Value As String)

    If LineNo >= 0 Then
        tabAssign(0).SetColumnValue LineNo, 5, "N"
        If InsertTabAssign Then
            tabAssign(0).RedrawTab
            tabAssign(0).Refresh
            If tabAssign(0).Visible Then
                tabAssign(0).SetFocus
            End If
            tabAssign(0).PostEnterBehavior = 3
            tabAssign(0).SetCurrentSelection LineNo
            tabAssign(0).SetInplaceEdit LineNo, 1, False
            InsertTabAssign = False
        End If
    End If
End Sub

Private Sub tabAssign_HitKeyOnLine(Index As Integer, ByVal Key As Integer, ByVal LineNo As Long)
    If LineNo >= 0 Then
        If Key = 13 Then
            chkTlxAssign.BackColor = vbRed
            chkTlxAssign.ForeColor = vbWhite
            tabAssign(0).SetColumnValue LineNo, 5, "N"
            tabAssign(0).InsertTextLineAt LineNo + 1, " , , , , , ", False
            InsertTabAssign = True
        End If
    End If
End Sub

Private Sub tabFlightList_HitKeyOnLine(Index As Integer, ByVal Key As Integer, ByVal LineNo As Long)
    
    If Index = 2 And LineNo >= 0 Then
        If Key = 13 Then
            EditMode = True
            chkFlightList(6).Value = 0
        End If
    End If
End Sub

Private Sub SetFolderValues(CurMemIdx As Integer)
    Dim i As Integer
    Dim AllCount As Long
    Dim CurTypeList As String
    Dim TypCount As Long
    Dim itm As Integer
    Dim CurTtyp As String
    Dim LineList As String
    Dim TtypCol As Long
    Dim FlnuCol As Long
    Dim tmpDatFrom As String
    Dim tmpDatTo As String
    Dim ActResult As String
    Dim ActCmd As String
    Dim ActTable As String
    Dim ActFldLst As String
    Dim ActCondition As String
    Dim ActOrder As String
    Dim ActDatLst As String
    Dim RetVal As Integer
    
    TtypCol = CLng(GetRealItemNo(DataPool.TlxTabFields, "TTYP"))
    FlnuCol = CLng(GetRealItemNo(DataPool.TlxTabFields, "FLNU"))
    DataPool.TelexData(CurMemIdx).IndexCreate "TTYP", TtypCol
    DataPool.TelexData(CurMemIdx).SetInternalLineBuffer True
    AllCount = 0
    For i = 0 To fraFolder.UBound - 2
        CurTypeList = fraFolder(i).Tag
        If InStr(AddFolderList, chkFolder(i).Caption) > 0 Then
            If ReadFolderCount Then
                tmpDatFrom = Mid(txtDateFrom.Text, 7, 4) & Mid(txtDateFrom.Text, 4, 2) & Mid(txtDateFrom.Text, 1, 2)
                tmpDatTo = Mid(txtDateTo.Text, 7, 4) & Mid(txtDateTo.Text, 4, 2) & Mid(txtDateTo.Text, 1, 2)
                If tmpDatFrom <> tmpDatTo Then
                    tmpDatFrom = "WHERE (DATC = '" & tmpDatFrom & "' OR DATC = '" & tmpDatTo & "')"
                Else
                    tmpDatFrom = "WHERE DATC = '" & tmpDatFrom & "'"
                End If
                ActResult = ""
                ActCmd = "RTA"
                ActTable = "TFNTAB"
                ActFldLst = "DISTINCT TURN"
                ActCondition = tmpDatFrom & " AND FNAM = '" & chkFolder(i).Caption & "'"
                ActOrder = ""
                ActDatLst = ""
                RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, False, False)
                If RetVal >= 0 Then
                    chkTabs(i).Caption = UfisServer.DataBuffer(0).GetLineCount
                Else
                    chkTabs(i).Caption = ""
                End If
                chkTabs(i).BackColor = vbButtonFace
            End If
        Else
            TypCount = 0
            itm = 1
            CurTtyp = GetItem(CurTypeList, itm, ",")
            While CurTtyp <> ""
                LineList = DataPool.TelexData(CurMemIdx).GetLinesByIndexValue("TTYP", CurTtyp, 0)
                TypCount = TypCount + Val(LineList)
                itm = itm + 1
                CurTtyp = GetItem(CurTypeList, itm, ",")
            Wend
            If TypCount > 0 Then
                chkTabs(i).Caption = CStr(TypCount)
            Else
                chkTabs(i).Caption = ""
            End If
            chkTabs(i).BackColor = vbButtonFace
            AllCount = AllCount + TypCount
        End If
    Next
    TypCount = DataPool.TelexData(CurMemIdx).GetLineCount
    chkTabs(AllTypesCol).Caption = CStr(TypCount)
    'If (TypCount - AllCount) > 0 Then chkTabs(OtherTypesCol).Caption = CStr(TypCount - AllCount)
    chkTabs(OtherTypesCol).Caption = CStr(TypCount - AllCount)
    DataPool.TelexData(CurMemIdx).SetInternalLineBuffer False
    DataPool.TelexData(CurMemIdx).IndexCreate "FLNU", FlnuCol
End Sub

Private Sub EditFlightsToTelex()
    Dim LineNo As Long
    Dim SqlKey As String
    Dim UseServerTime
    Dim count As Integer
    Dim idx As Integer
    Dim tmpDat As String
    Dim tmpStat As String
    Dim tmpIdx As Integer
    Dim ActResult As String
    Dim ActCmd As String
    Dim ActTable As String
    Dim ActFldLst As String
    Dim ActCondition As String
    Dim ActOrder As String
    Dim ActDatLst As String
    Dim RetVal As String
    
    If Not EditMode Then
        tabFlightList(2).ResetContent
        If Len(CurrentUrno.Text) > 0 Then
            tabFlightList(2).CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
            tabFlightList(2).CedaHopo = UfisServer.HOPO
            tabFlightList(2).CedaIdentifier = "IDX"
            tabFlightList(2).CedaPort = "3357"
            tabFlightList(2).CedaReceiveTimeout = "250"
            tabFlightList(2).CedaRecordSeparator = vbLf
            tabFlightList(2).CedaSendTimeout = "250"
            tabFlightList(2).CedaServerName = UfisServer.HostName
            tabFlightList(2).CedaTabext = UfisServer.TblExt
            tabFlightList(2).CedaUser = UfisServer.ModName
            tabFlightList(2).CedaWorkstation = UfisServer.GetMyWorkStationName
            If CedaIsConnected Then
                SqlKey = "WHERE TURN = " & CurrentUrno.Text & " ORDER BY FLNO,DATC"
                tabFlightList(2).CedaAction "RTA", "TLKTAB", "ADID,FLNO,FDAT,URNO,TURN,FURN,DATC", " ", SqlKey
            End If
            count = tabFlightList(2).GetLineCount - 1
            If count >= 0 Then
                For idx = 0 To count
                    tmpDat = tabFlightList(2).GetColumnValue(idx, 6)
                    UseServerTime = CedaFullDateToVb(tmpDat)
                    If optLocal.Value = True Then
                        UseServerTime = DateAdd("n", UtcTimeDiff, UseServerTime)
                    End If
                    tmpDat = Format(UseServerTime, "ddmmmyy\/hh:mm")
                    tmpDat = UCase(tmpDat)
                    tabFlightList(2).SetColumnValue idx, 2, tmpDat
                Next
            End If
            tabFlightList(2).AutoSizeColumns
            tabFlightList(2).RedrawTab
            tmpStat = ""
            If count >= 0 Then
                tmpStat = "A"
            Else
                ActResult = ""
                ActCmd = "RTA"
                ActTable = "TFNTAB"
                ActFldLst = "TURN"
                ActCondition = "WHERE TURN = " & CurrentUrno.Text
                ActOrder = ""
                ActDatLst = ""
                RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
                If RetVal < 0 Then
                    tmpStat = "V"
                End If
            End If
            If Len(tmpStat) > 0 Then
                For idx = 2 To 3
                    If tabTelexList(idx).Visible = True Then
                        tmpIdx = tabTelexList(idx).GetCurrentSelected
                        tabTelexList(idx).SetColumnValue tmpIdx, 1, tmpStat
                        tabTelexList(idx).Refresh
                    End If
                Next
                UpdateTelexStatus tmpStat
            End If
        End If
    End If
    tabFlightList(2).InsertTextLine " , , , , , , ", False
    tabFlightList(2).RedrawTab
    tabFlightList(2).Refresh
    If tabFlightList(2).Visible Then
        tabFlightList(2).SetFocus
    End If
    tabFlightList(2).PostEnterBehavior = 3
    LineNo = tabFlightList(2).GetLineCount - 1
    tabFlightList(2).SetCurrentSelection LineNo
    tabFlightList(2).SetInplaceEdit LineNo, 0, False
    EditMode = False
End Sub

Private Sub AssignFlightsToTelex()
    Dim MaxLine As Long
    Dim i As Long
    Dim j As Long
    Dim tmpFlno As String
    Dim tmpDat As String
    Dim tmpFdat As String
    Dim tmpAdid As String
    Dim tmpFurn As String
    Dim ErrorMsg As String
    Dim ActResult As String
    Dim ActCmd As String
    Dim ActTable As String
    Dim ActFldLst As String
    Dim ActCondition As String
    Dim ActOrder As String
    Dim ActDatLst As String
    Dim RetVal As String
    Dim tmpResult As String
    Dim tmpTime As String
    Dim SqlKey As String
    Dim tmpAlc2 As String
    Dim tmpAlc3 As String
    Dim tmpFltn As String
    Dim tmpFlns As String
    Dim tmpFkey As String
    
    If Len(CurrentUrno.Text) > 0 Then
        TelexPoolHead.MousePointer = 11
        MaxLine = tabFlightList(2).GetLineCount - 1
        For i = 0 To MaxLine
            tmpAdid = Trim(tabFlightList(2).GetColumnValue(i, 0))
            tmpFlno = Trim(tabFlightList(2).GetColumnValue(i, 1))
            If Len(Trim(tabFlightList(2).GetColumnValue(i, 2))) < 13 Then
                tmpDat = Mid(Trim(tabFlightList(2).GetColumnValue(i, 2)), 1, 7)
                If Len(tmpDat) = 0 Then
                    tmpDat = Mid(GetTimeStamp(0), 7, 2)
                End If
                If Len(tmpDat) = 2 Then tmpDat = ExpandMonth(tmpDat)
                If Len(tmpDat) = 6 Then
                    tmpFdat = "20" & Mid(tmpDat, 5, 2) & Mid(tmpDat, 3, 2) & Mid(tmpDat, 1, 2)
                    tmpDat = DecodeSsimDayFormat(tmpFdat, "CEDA", "SSIM2")
                End If
                If Len(tmpFlno) > 0 And Len(tmpDat) > 0 Then
                    tmpFlno = StripAftFlno(tmpFlno, "", "", "")
                    tmpFdat = DecodeSsimDayFormat(tmpDat, "SSIM2", "CEDA")
                    If CheckValidDate(tmpFdat) = False Then
                        ErrorMsg = "Invalid Flight Date"
                    End If
                    If tmpAdid <> "A" And tmpAdid <> "D" Then
                        ErrorMsg = "Invalid Flight Type"
                    End If
                    If Len(ErrorMsg) = 0 Then
                        tmpAlc2 = Trim(Left(tmpFlno, 3))
                        tmpFltn = Trim(Mid(tmpFlno, 4, 5))
                        tmpFlns = Trim(Mid(tmpFlno, 9, 1))
                        If Len(tmpAlc2) = 2 Then
                            tmpAlc3 = UfisServer.BasicLookUp(1, 0, 1, tmpAlc2, 0, True)
                        Else
                            tmpAlc3 = Left(tmpAlc2, 3)
                        End If
                        tmpFkey = BuildAftFkey(tmpAlc3, tmpFltn, tmpFlns, tmpFdat, tmpAdid)
                        ActResult = ""
                        ActCmd = "RTA"
                        ActTable = "AFTTAB"
                        If tmpAdid = "A" Then
                            ActFldLst = "URNO,STOA"
                            'ActCondition = "WHERE FLNO = '" & tmpFlno & "' AND STOA LIKE '" & tmpFdat & "%' AND ADID = 'A'"
                        Else
                            ActFldLst = "URNO,STOD"
                            'ActCondition = "WHERE FLNO = '" & tmpFlno & "' AND STOD LIKE '" & tmpFdat & "%' AND ADID = 'D'"
                        End If
                        ActCondition = "WHERE FKEY = '" & tmpFkey & "'"
                        ActOrder = ""
                        ActDatLst = ""
                        RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, False, False)
                        If RetVal >= 0 Then
                            tmpResult = UfisServer.DataBuffer(0).GetLineValues(0)
                            tmpTime = Mid(GetItem(tmpResult, 2, ","), 9, 4)
                            tabFlightList(2).SetColumnValue i, 1, tmpFlno
                            tabFlightList(2).SetColumnValue i, 2, tmpDat & "/" & Mid(tmpTime, 1, 2) & ":" & Mid(tmpTime, 3, 2)
                            tabFlightList(2).SetColumnValue i, 3, "0"
                            tabFlightList(2).SetColumnValue i, 4, "0"
                            tabFlightList(2).SetColumnValue i, 5, GetItem(tmpResult, 1, ",")
                            tabFlightList(2).SetColumnValue i, 6, GetItem(tmpResult, 2, ",")
                        Else
                            ErrorMsg = "Flight not found"
                        End If
                    End If
                    If Len(ErrorMsg) > 0 Then
                        tabFlightList(2).SetCurrentSelection i
                        MyMsgBox.CallAskUser 0, 0, 0, "Link Flight to Telex", ErrorMsg, "hand", "", UserAnswer
                        Exit For
                    End If
                Else
                    tabFlightList(2).SetColumnValue i, 5, ""
                End If
            Else
                If Len(tmpFlno) = 0 Then
                    tabFlightList(2).SetColumnValue i, 5, ""
                End If
            End If
        Next
        If Len(ErrorMsg) = 0 Then
            ActResult = ""
            ActCmd = "DRT"
            ActTable = "TLKTAB"
            ActFldLst = ""
            ActCondition = "WHERE TURN = " & CurrentUrno.Text
            ActOrder = ""
            ActDatLst = ""
            RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
            For i = 0 To MaxLine
                tmpFurn = Trim(tabFlightList(2).GetColumnValue(i, 5))
                If Len(tmpFurn) > 0 Then
                    For j = i - 1 To 0 Step -1
                        If tmpFurn = Trim(tabFlightList(2).GetColumnValue(j, 5)) Then
                            tmpFurn = ""
                            Exit For
                        End If
                    Next
                End If
                If Len(tmpFurn) > 0 Then
                    ActResult = ""
                    ActCmd = "IRT"
                    ActTable = "TLKTAB"
                    ActFldLst = "URNO,HOPO,TURN,FURN,ADID,FLNO,FDAT,DATC"
                    ActCondition = ""
                    ActOrder = ""
                    ActDatLst = UfisServer.UrnoPoolGetNext & "," & _
                                UfisServer.HOPO & "," & _
                                CurrentUrno.Text & "," & _
                                tabFlightList(2).GetColumnValue(i, 5) & "," & _
                                tabFlightList(2).GetColumnValue(i, 0) & "," & _
                                tabFlightList(2).GetColumnValue(i, 1) & "," & _
                                tabFlightList(2).GetColumnValue(i, 2) & "," & _
                                tabFlightList(2).GetColumnValue(i, 6)
                    RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
                End If
            Next
            EditFlightsToTelex
        End If
        TelexPoolHead.MousePointer = 0
    Else
        ErrorMsg = "No Telex selected"
        MyMsgBox.CallAskUser 0, 0, 0, "Link Flight to Telex", ErrorMsg, "hand", "", UserAnswer
    End If
End Sub

Private Function ExpandMonth(Day As String) As String
    Dim tmpCurDat1 As String
    Dim tmpCurDat2 As String
    Dim tmpYear As String
    Dim tmpMonth As String
    Dim Result As String
    
    tmpCurDat1 = Left(GetTimeStamp(0), 8)
    tmpCurDat2 = Left(tmpCurDat1, 6) & Day
    If tmpCurDat2 < tmpCurDat1 Then
        tmpYear = Mid(tmpCurDat1, 1, 4)
        tmpMonth = Mid(tmpCurDat1, 5, 2)
        tmpMonth = tmpMonth + 1
        If Len(tmpMonth) = 1 Then tmpMonth = "0" & tmpMonth
        If tmpMonth = "13" Then
            tmpMonth = "01"
            tmpYear = tmpYear + 1
        End If
        Result = tmpYear & tmpMonth & Day
    Else
        Result = tmpCurDat2
    End If
    Result = DecodeSsimDayFormat(Result, "CEDA", "SSIM2")
    ExpandMonth = Result
End Function

Private Sub AddToOnlineWindow()
    Dim count As Long
    Dim FlnuCol As Long
    Dim tmpLine As Long
    Dim tmpFlnu As String
    
    DataPool.TelexData(9).Refresh
    While DataPool.TelexData(9).GetLineCount > 0
        count = DataPool.TelexData(9).GetLineCount - 1
        RcvTelex.ShowHiddenTelex count
        ReadFolderCount = False
        PrepareTelexExtract CurMem
        ReadFolderCount = True
        If fraFlightList.Visible Then
            FlnuCol = CLng(GetRealItemNo(DataPool.TlxTabFields, "FLNU"))
            tmpLine = DataPool.TelexData(CurMem).GetLineCount - 1
            tmpFlnu = DataPool.TelexData(CurMem).GetColumnValue(tmpLine, FlnuCol)
            If tmpFlnu <> "" Then
                AddCountFlightTelex tmpFlnu
            End If
        End If
        chkTlxTitleBar(0).Caption = CStr(tabTelexList(2).GetLineCount) & " " & chkTlxTitleBar(0).Tag & " (Online)"
        chkTlxTitleBar(1).Caption = CStr(tabTelexList(3).GetLineCount) & " " & chkTlxTitleBar(1).Tag & " (Online)"
        chkTlxCnt(0).Caption = CStr(tabTelexList(2).GetLineCount)
        chkTlxCnt(1).Caption = CStr(tabTelexList(3).GetLineCount)
        DataPool.TelexData(9).Refresh
    Wend
    StopOnline = False
End Sub

Private Sub HandleTlxTitleBar()
    Dim strTmp As String

    strTmp = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SHOW_SENT", "NO")
    If strTmp = "NO" Then
        chkTlxTitleBar(1).Visible = False
        chkTlxTitleBar(3).Visible = False
    Else
        chkTlxTitleBar(1).Visible = True
        chkTlxTitleBar(3).Visible = True
    End If

    strTmp = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SHOW_RECV", "NO")
    If strTmp = "NO" Then
        chkTlxTitleBar(0).Visible = False
        chkTlxTitleBar(2).Visible = False
    Else
        chkTlxTitleBar(0).Visible = True
        chkTlxTitleBar(2).Visible = True
    End If
End Sub

Private Function CheckTlxUrnolist(TlxUrnoList As String, MemIdx As Integer)
    Dim tmpData As String
    Dim i As Integer
    Dim j As Integer
    Dim UrnoCol As Long
    Dim tmpUrno As String
    Dim tmpItem As Long
    
    UrnoCol = GetItemNo(DataPool.TlxTabFields, "URNO") - 1
    For i = 0 To DataPool.TelexData(MemIdx).GetLineCount - 1
        tmpUrno = DataPool.TelexData(MemIdx).GetColumnValue(i, UrnoCol)
        tmpItem = GetItemNo(TlxUrnoList, tmpUrno)
        If tmpItem > 0 Then
            tmpData = RemoveItem(TlxUrnoList, tmpItem)
            TlxUrnoList = tmpData
        End If
    Next
    If Len(TlxUrnoList) > 0 Then
        TlxUrnoList = "WHERE URNO IN (" & TlxUrnoList & ")"
    End If
    CheckTlxUrnolist = TlxUrnoList
End Function

Private Function RemoveItem(ItemList As String, ItemNo As Long)
    Dim tmpData As String
    Dim count As Long
    Dim i As Integer
    
    tmpData = ""
    count = ItemCount(ItemList, ",")
    For i = 1 To ItemNo - 1
        tmpData = tmpData & GetItem(ItemList, i, ",") & ","
    Next
    For i = ItemNo + 1 To count
        tmpData = tmpData & GetItem(ItemList, i, ",") & ","
    Next
    If Len(tmpData) > 0 Then
        tmpData = Left(tmpData, Len(tmpData) - 1)
    End If
    RemoveItem = tmpData
End Function

