using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Ufis.Utils;
using Ufis.Data;

namespace FIPS_Reports
{
	/// <summary>
	/// Summary description for frmCounterScheduleForPVG.
	/// </summary>
	public class frmCounterScheduleForPVG : System.Windows.Forms.Form
	{
		#region _My Members

		/// <summary>
		/// The where statement: all "@@xx" fields will be replaced
		/// with the true values according to the user's entry.
		/// </summary>
		private string strWhereRawA = "(STOA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B')))";
		/// <summary>
		/// The where statement: all "@@xx" fields will be replaced
		/// with the true values according to the user's entry.
		/// </summary>
		private string strWhereRawD = "(STOD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('D','B')))";
		/// <summary>
		/// The where statement: all "@@xx" fields will be replaced
		/// with the true values according to the user's entry.
		/// </summary>
		private string strWhereRawB = "(STOA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))) OR (STOD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('D','B')))";
		/// <summary>
		/// The where statement for the dedicated check counters.
		/// </summary>
		private string strDedicatedCCAWhere = "WHERE FLNU IN (@@FLNU) AND CKIC<>' '";
		/// <summary>
		/// The Where statement for the common check-in counters.
		/// </summary>
		private string strCommonCCAWhere = "WHERE CKBS<='@@TO' AND CKES>='@@FROM' AND CKIC<>' ' AND CTYP='C'";
		/// <summary>
		/// The where statement for the blocking times.
		/// </summary>
		private string strBLKWhere = "WHERE (NATO>='@@FROM' AND NAFR <='@@TO') OR (NAFR<='@@TO' AND NATO=' ') OR (NATO>='@@FROM' AND NAFR=' ') AND TABN='CIC'";
		/// <summary>
		/// The logical field names of the tab control for internal 
		/// development purposes
		/// </summary>
		private string strLogicalFields = "ALC,FLTN,ACTYPE,ROUTING,DES,STDDATE,STDTIME,SUM,COUNTER,MINUTES,TIMEFRAME";
		private string strColWidths = "6,5,3,5,3,10,5,3,5,5,15";
		private string strColAlignment = "L,L,L,L,L,L,L,L,L,L,L";
		/// <summary>
		/// The header columns, which must be used for the report output
		/// </summary>
		private string strTabHeader =     "Airlines,Flight  number,A/C  ,Routing  ,Des  ,S Date  ,S Time    ,Counters,Counter  from - to,Minutes,from - to";
		/// <summary>
		/// The lengths, which must be used for the report's column widths
		/// </summary>
		private string strTabHeaderLens = "80,100,60,60,40,80,80,70,130,60,80";
		/// <summary>
		/// String for empty line generation in the tab control.
		/// </summary>
		private string strEmptyLine = ",,,,,,,,,,";
		/// <summary>
		/// The one and only IDatabase obejct.
		/// </summary>
		private IDatabase myDB;
		/// <summary>
		/// ITable object for the AFTTAB
		/// </summary>
		private ITable myAFT;
		/// <summary>
		/// ITable object for the nature codes of NATTAB
		/// </summary>
		private ITable myCCA;
		/// <summary>
		/// Blocking times table for blocked counters.
		/// </summary>
		/// <summary>
		/// Total of arrival flights in the report
		/// </summary>
		private int imArrivals = 0;
		/// <summary>
		/// Total of departure flights in the report
		/// </summary>
		private int imDepartures = 0;
		/// <summary>
		/// Total of rotationein the report
		/// </summary>
		private int imRotations = 0;
		/// <summary>
		/// Total of all flights in the report
		/// </summary>
		private int imTotalFlights = 0;
		private ITable myBLK;
		#endregion _My Members ------------------------------------------

		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.Label lblResults;
		private System.Windows.Forms.Panel panelTab;
		private System.Windows.Forms.Button btnLoadPrint;
		private System.Windows.Forms.Button btnPrintPreview;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOK;
		private AxTABLib.AxTAB tabResult;
		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.ProgressBar progressBar1;
		private UserControls.ucView ucView1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmCounterScheduleForPVG()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			myDB = UT.GetMemDB();
			this.ucView1.viewEvent += new UserControls.ucView.ViewChangedEvent(ucView1_viewEvent);
			this.ucView1.Report = "CounterSchedule";
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmCounterScheduleForPVG));
			this.panelTop = new System.Windows.Forms.Panel();
			this.lblProgress = new System.Windows.Forms.Label();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.btnLoadPrint = new System.Windows.Forms.Button();
			this.btnPrintPreview = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.panelBody = new System.Windows.Forms.Panel();
			this.panelTab = new System.Windows.Forms.Panel();
			this.tabResult = new AxTABLib.AxTAB();
			this.lblResults = new System.Windows.Forms.Label();
			this.ucView1 = new UserControls.ucView();
			this.panelTop.SuspendLayout();
			this.panelBody.SuspendLayout();
			this.panelTab.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
			this.SuspendLayout();
			// 
			// panelTop
			// 
			this.panelTop.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelTop.Controls.Add(this.ucView1);
			this.panelTop.Controls.Add(this.lblProgress);
			this.panelTop.Controls.Add(this.progressBar1);
			this.panelTop.Controls.Add(this.btnLoadPrint);
			this.panelTop.Controls.Add(this.btnPrintPreview);
			this.panelTop.Controls.Add(this.btnCancel);
			this.panelTop.Controls.Add(this.btnOK);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(0, 0);
			this.panelTop.Name = "panelTop";
			this.panelTop.Size = new System.Drawing.Size(644, 136);
			this.panelTop.TabIndex = 0;
			// 
			// lblProgress
			// 
			this.lblProgress.Location = new System.Drawing.Point(408, 52);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(212, 16);
			this.lblProgress.TabIndex = 20;
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(408, 68);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(216, 23);
			this.progressBar1.TabIndex = 6;
			this.progressBar1.Visible = false;
			// 
			// btnLoadPrint
			// 
			this.btnLoadPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnLoadPrint.Location = new System.Drawing.Point(244, 72);
			this.btnLoadPrint.Name = "btnLoadPrint";
			this.btnLoadPrint.TabIndex = 4;
			this.btnLoadPrint.Text = "Loa&d + Print";
			this.btnLoadPrint.Click += new System.EventHandler(this.btnLoadPrint_Click);
			// 
			// btnPrintPreview
			// 
			this.btnPrintPreview.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnPrintPreview.Location = new System.Drawing.Point(168, 72);
			this.btnPrintPreview.Name = "btnPrintPreview";
			this.btnPrintPreview.TabIndex = 3;
			this.btnPrintPreview.Text = "&Print Preview";
			this.btnPrintPreview.Click += new System.EventHandler(this.btnPrintPreview_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCancel.Location = new System.Drawing.Point(320, 72);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 5;
			this.btnCancel.Text = "&Close";
			// 
			// btnOK
			// 
			this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnOK.Location = new System.Drawing.Point(92, 72);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 2;
			this.btnOK.Text = "&Load Data";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// panelBody
			// 
			this.panelBody.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelBody.Controls.Add(this.panelTab);
			this.panelBody.Controls.Add(this.lblResults);
			this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelBody.Location = new System.Drawing.Point(0, 136);
			this.panelBody.Name = "panelBody";
			this.panelBody.Size = new System.Drawing.Size(644, 294);
			this.panelBody.TabIndex = 1;
			// 
			// panelTab
			// 
			this.panelTab.Controls.Add(this.tabResult);
			this.panelTab.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelTab.Location = new System.Drawing.Point(0, 16);
			this.panelTab.Name = "panelTab";
			this.panelTab.Size = new System.Drawing.Size(644, 278);
			this.panelTab.TabIndex = 3;
			// 
			// tabResult
			// 
			this.tabResult.ContainingControl = this;
			this.tabResult.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabResult.Location = new System.Drawing.Point(0, 0);
			this.tabResult.Name = "tabResult";
			this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
			this.tabResult.Size = new System.Drawing.Size(644, 278);
			this.tabResult.TabIndex = 0;
			this.tabResult.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabResult_SendLButtonDblClick);
			// 
			// lblResults
			// 
			this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lblResults.Location = new System.Drawing.Point(0, 0);
			this.lblResults.Name = "lblResults";
			this.lblResults.Size = new System.Drawing.Size(644, 16);
			this.lblResults.TabIndex = 2;
			this.lblResults.Text = "Report Results";
			this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// ucView1
			// 
			this.ucView1.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.ucView1.Location = new System.Drawing.Point(20, 8);
			this.ucView1.Name = "ucView1";
			this.ucView1.Report = "";
			this.ucView1.Size = new System.Drawing.Size(304, 40);
			this.ucView1.TabIndex = 21;
			// 
			// frmCounterScheduleForPVG
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(644, 430);
			this.Controls.Add(this.panelBody);
			this.Controls.Add(this.panelTop);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "frmCounterScheduleForPVG";
			this.Text = "Counter Schedule ...";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.frmCounterScheduleForPVG_Closing);
			this.Load += new System.EventHandler(this.frmCounterScheduleForPVG_Load);
			this.panelTop.ResumeLayout(false);
			this.panelBody.ResumeLayout(false);
			this.panelTab.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion


		/// <summary>
		/// Load the form. Prepare and init all controls.
		/// </summary>
		/// <param name="sender">Form.</param>
		/// <param name="e">Event arguments.</param>
		private void frmCounterScheduleForPVG_Load(object sender, System.EventArgs e)
		{
			InitTab();
			PrepareFilterData();
		}
		/// <summary>
		/// Runs the data queries according to the reports needs and puts
		///	Calls the data prepare method and puts the prepared data into
		///	the tab control. After this is finished it calls the reports
		///	by sending the tab and the headers to the generic report.
		/// </summary>
		/// <param name="sender">Originator object of the call</param>
		/// <param name="e">Event arguments</param>
		private void btnOK_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			InitTab();
			LoadReportData();
			PrepareReportData();
			//RunReport();
			this.Cursor = Cursors.Arrow;
		}
		/// <summary>
		/// Start the print preview without data selection.
		/// This can be used to adjust the column widths and
		/// the sorting of the tab before printing.
		/// </summary>
		/// <param name="sender">Originator is the form</param>
		/// <param name="e">Event arguments.</param>
		private void btnPrintPreview_Click(object sender, System.EventArgs e)
		{
			RunReport();
		}
		/// <summary>
		/// Loads data and dirctly calls the print preview.
		/// </summary>
		/// <param name="sender">Originator is the form</param>
		/// <param name="e">Event arguments.</param>
		private void btnLoadPrint_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			InitTab();
			LoadReportData();
			PrepareReportData();
			RunReport();
			this.Cursor = Cursors.Arrow;
		}
		/// <summary>
		/// Initializes the tab controll.
		/// </summary>
		private void InitTab()
		{
			tabResult.ResetContent();
			tabResult.ShowHorzScroller(true);
			tabResult.EnableHeaderSizing(true);
			tabResult.SetTabFontBold(true);
			tabResult.LifeStyle = true;
			tabResult.LineHeight = 16;
			tabResult.FontName = "Arial";
			tabResult.FontSize = 14;
			tabResult.HeaderFontSize = 14;
			tabResult.AutoSizeByHeader = true;
			tabResult.ColumnWidthString = strColWidths;
			tabResult.ColumnAlignmentString = strColAlignment;

			tabResult.HeaderString = strTabHeader;
			tabResult.LogicalFieldList = strLogicalFields;
			tabResult.HeaderLengthString = strTabHeaderLens;
		}
		/// <summary>
		/// Loads and prepare the data necessary for the filter. 
		/// In this case loads the nature codes and puts the
		/// data into the combobox.
		/// </summary>
		private void PrepareFilterData()
		{
		}
		/// <summary>
		/// Validates the user entry for nonsense.
		/// </summary>
		/// <returns></returns>
		private string ValidateUserEntry()
		{
			string strRet = "";
			int ilErrorCount = 1;

			if (this.ucView1.Viewer.CurrentView == "")
			{
				strRet += ilErrorCount.ToString() +  ". A view must be selected!\n";
				ilErrorCount++;
			}

			if (this.ucView1.Viewer.PeriodFrom >= this.ucView1.Viewer.PeriodTo)
			{
				strRet += ilErrorCount.ToString() +  ". Date From is later than Date To!\n";
				ilErrorCount++;
			}

			if (this.ucView1.Viewer.Arrival == true)
			{
				strRet += ilErrorCount.ToString() +  ". Arrival must not be selected!\n";
				ilErrorCount++;
			}

			if (this.ucView1.Viewer.Departure == false)
			{
				strRet += ilErrorCount.ToString() +  ". Departure must be selected!\n";
				ilErrorCount++;
			}

			if (this.ucView1.Viewer.Rotation == true)
			{
				strRet += ilErrorCount.ToString() +  ". Rotation must not be selected!\n";
				ilErrorCount++;
			}

			return strRet;
		}
		/// <summary>
		/// Loads all necessary tables with filter criteria from the 
		/// database ==> to memoryDB.
		/// </summary>
		private void LoadReportData()
		{
			string strTmpAFTWhere;
			//Clear the tab
			tabResult.ResetContent();

			// In the first step change the times to UTC if this is
			// necessary.
			DateTime datFrom;
			DateTime datTo;
			string strDateFrom = "";
			string strDateTo = "";
			datFrom = this.ucView1.Viewer.PeriodFrom;
			datTo   = this.ucView1.Viewer.PeriodTo;
			if(UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(this.ucView1.Viewer.PeriodFrom);
				datTo   = UT.LocalToUtc(this.ucView1.Viewer.PeriodTo);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
			}
			else
			{
				strDateFrom = UT.DateTimeToCeda(this.ucView1.Viewer.PeriodFrom);
				strDateTo = UT.DateTimeToCeda(this.ucView1.Viewer.PeriodTo);
			}

			myDB.Unbind("AFT");
			myDB.Unbind("DED_CCA");
			//Load the data from AFTTAB
			myAFT = myDB.Bind("AFT", "AFT", "ALC2,ALC3,FLTN,FLNO,DES3,STOD,TIFD,ACT3,VIA3,URNO", "2,3,5,12,3,14,14,3,3,10", "ALC2,ALC3,FLTN,FLNO,DES3,STOD,TIFD,ACT3,VIA3,URNO");
			myAFT.Clear();
			myAFT.TimeFields = "STOA,STOD,TIFA,TIFD,ONBL,OFBL";
			myAFT.TimeFieldsInitiallyInUtc = true;
			//Prepare loop reading day by day
			DateTime datReadFrom = datFrom;
			DateTime datReadTo;
			TimeSpan tsDays = (datTo - datFrom);
			progressBar1.Value = 0;
			int ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) ilTotal = 1;
			lblProgress.Text = "Loading Flight Data";
			lblProgress.Refresh();
			progressBar1.Show();
			int loopCnt = 1;
			do
			{
				int percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
				datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
				if( datReadTo > datTo) datReadTo = datTo;
				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);

				strTmpAFTWhere = "WHERE ";
				// check on code share enabled
				if (this.ucView1.Viewer.IncludeCodeShare)
				{
					string strTmpWhere1 = "";
					//patch the where statement according to the user's entry
					if (this.ucView1.Viewer.Arrival && this.ucView1.Viewer.Departure)
						strTmpWhere1 = strWhereRawB;
					else if (this.ucView1.Viewer.Arrival)
						strTmpWhere1 = strWhereRawA;
					else if (this.ucView1.Viewer.Departure)
						strTmpWhere1 = strWhereRawD;
					strTmpWhere1+= this.ucView1.Viewer.AdditionalSelection(0);
					strTmpWhere1+= this.ucView1.Viewer.GeneralFilter;

					string strTmpWhere2 = "";
					//patch the where statement according to the user's entry
					strTmpWhere2+= this.ucView1.Viewer.AdditionalSelection(1);

					strTmpAFTWhere = string.Format("WHERE ({0}) AND ({1})",strTmpWhere1,strTmpWhere2);
				}
				else
				{
					//patch the where statement according to the user's entry
					if (this.ucView1.Viewer.Arrival && this.ucView1.Viewer.Departure)
						strTmpAFTWhere += strWhereRawB;
					else if (this.ucView1.Viewer.Arrival)
						strTmpAFTWhere += strWhereRawA;
					else if (this.ucView1.Viewer.Departure)
						strTmpAFTWhere += strWhereRawD;
					strTmpAFTWhere+= this.ucView1.Viewer.AdditionalSelection(0);
					strTmpAFTWhere+= this.ucView1.Viewer.GeneralFilter;

				}

				//patch the where statement according to the user's entry
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@FROM", strDateFrom);
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@TO", strDateTo );

				//Load the data from AFTTAB
				myAFT.Load(strTmpAFTWhere);
				datReadFrom += new TimeSpan(1, 0, 0, 0, 0);
				loopCnt++;
			}while(datReadFrom <= datReadTo);
			myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
			myAFT.Sort("STOD", true);
			//-------------------------------------------------------------------
			//Load dedicated check-in counter according to the AFT.URNO->CCA.FLNU
			myDB.Unbind("DED_CCA");
			myCCA = myDB.Bind("DED_CCA", "CCA", "FLNU,CKIC,CKBS,CKES,CKBA,CKEA,CTYP,CICF,CICT,ANZ", "10,5,14,14,14,14,3,5,5,3", "FLNU,CKIC,CKBS,CKES,CKBA,CKEA,CTYP");
			myCCA.Clear();
			lblProgress.Text = "Loading Counter Data";
			lblProgress.Refresh();
			int loops = 0;
			progressBar1.Value = 0;
			progressBar1.Refresh();
			ilTotal = (int)(myAFT.Count/300);
			if(ilTotal == 0) ilTotal = 1;
			string strFlnus = "";
			string strTmpCCAWhere = strDedicatedCCAWhere;
			for(int i = 0; i < myAFT.Count; i++)
			{
				strFlnus += myAFT[i]["URNO"] + ",";
				if((i % 300) == 0 && i > 0)
				{
					loops++;
					int percent = Convert.ToInt32((loops * 100)/ilTotal);
					if (percent > 100) 
						percent = 100;
					progressBar1.Value = percent;
					strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
					strTmpCCAWhere = strDedicatedCCAWhere;
					strTmpCCAWhere = strTmpCCAWhere.Replace("@@FLNU", strFlnus);
					myCCA.Load(strTmpCCAWhere);
					strFlnus = "";
				}
			}
			//Load the rest of the dedicated Counters.
			if(strFlnus.Length > 0)
			{
				strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
				strTmpCCAWhere = strDedicatedCCAWhere;
				strTmpCCAWhere = strTmpCCAWhere.Replace("@@FLNU", strFlnus);
				myCCA.Load(strTmpCCAWhere);
				strFlnus = "";
			}
			myCCA.Sort("CKBS,CKBA,CKES,CKEA", true);
			myCCA.CreateIndex("FLNU", "FLNU");
			lblProgress.Text = "";
			progressBar1.Hide();
			//-------------------------------------------------------------------
			//Load common check-in counter TO DO Was und wie soll das in den Report rein
//			myCCA = myDB.Bind("COM_CCA", "CCA", "FLNU,CKIC,CKBS,CKES,CKBA,CKEA", "10,5,14,14,14,14", "FLNU,CKIC,CKBS,CKES,CKBA,CKEA");
//			myCCA.Clear();
//			strTmpCCAWhere = strCommonCCAWhere;
//			strTmpCCAWhere = strTmpCCAWhere.Replace("@@FROM", strDateFrom);
//			strTmpCCAWhere = strTmpCCAWhere.Replace("@@TO", strDateTo);
//			myCCA.Load(strTmpCCAWhere);
			//-------------------------------------------------------------------
			//Load blocking times for counters
//			myBLK = myDB.Bind("BLK", "BLK", "", "", "");
//			myBLK.Clear();
//			string strTmpBlkWhere = strBLKWhere;
//			strTmpBlkWhere = strTmpBlkWhere.Replace("@@FROM", strDateFrom);
//			strTmpBlkWhere = strTmpBlkWhere.Replace("@@TO", strDateTo);


		}



		private void CompressCounterAllocations(string strFlightValues,IRow[] rows,StringBuilder sb)
		{
			// copy members to array list
			ArrayList myRows = new ArrayList(rows);
			// initialize members
			int ilAnz  = 1;
			for (int i = 0; i < myRows.Count; i++)
			{
				((IRow)myRows[i])["ANZ"] = 	ilAnz.ToString();
				((IRow)myRows[i])["CICF"]=	((IRow)myRows[i])["CKIC"];
				((IRow)myRows[i])["CICT"]=	((IRow)myRows[i])["CKIC"];
			}

			for (int i = 0; i < myRows.Count; i++)
			{
				ilAnz = int.Parse(((IRow)myRows[i])["ANZ"]);
				int ilCicf = int.Parse(((IRow)myRows[i])["CICF"]);
				int ilCict = int.Parse(((IRow)myRows[i])["CICT"]);

				for (int j = 0; j < myRows.Count; j++)
				{
					// Stimmen Typ, �ffnungszeitpunkt und Schlie�ungszeitpunkt �berein?
					if (((IRow)myRows[i])["CTYP"] == ((IRow)myRows[j])["CTYP"] && 
						((IRow)myRows[i])["CKBS"] == ((IRow)myRows[j])["CKBS"] &&
						((IRow)myRows[i])["CKES"] == ((IRow)myRows[j])["CKES"])
					{
						// Sind Schalternummern fortlaufend?
						if (int.Parse(((IRow)myRows[j])["CICT"]) + 1 == ilCicf ||
							int.Parse(((IRow)myRows[j])["CICF"]) - 1 == ilCict)
						{	
							// Anzahl der Schalter addieren
							ilAnz = int.Parse(((IRow)myRows[i])["ANZ"]) + int.Parse(((IRow)myRows[j])["ANZ"]);
							((IRow)myRows[i])["ANZ"] = ilAnz.ToString();

							if (int.Parse(((IRow)myRows[j])["CICT"]) + 1 == ilCicf)
							{
								// �ffnungzeitpunkt anpassen
								((IRow)myRows[i])["CICF"] = ((IRow)myRows[j])["CICF"];
							}
							else
							{
								// Schlie�ungszeitpunkt anpassen
								((IRow)myRows[i])["CICT"] = ((IRow)myRows[j])["CICT"];
							}

							// Schalterbelegung l�schen
							myRows.RemoveAt(j);
							// Wenn sich der Schalter vor dem untersuchten Schalter im Array befunden hat,
							//   Z�hler entsprechend anpassen
							if ( j < i ) 
								i--;
							// Gleiche Schalterbelegung nochmals untersuchen
							i--;
							break;
						}
					}
				}
			}

			string strValues;
			string strCounterFrom;
			string strCounterTo;
			DateTime datCounterFrom;
			DateTime datCounterTo;
			TimeSpan timeSpan;
			string strTmpFrom;
			string strTmpTo;
			for (int i = 0; i < myRows.Count; i++)
			{
				strValues = strFlightValues + ((IRow)myRows[i])["ANZ"] + ",";
				strValues += ((IRow)myRows[i])["CICF"] + "-";
				strValues += ((IRow)myRows[i])["CICT"] + ",";
				if (((IRow)myRows[i])["CKBA"] == "") 
					strCounterFrom = ((IRow)myRows[i])["CKBS"];
				else
					strCounterFrom = ((IRow)myRows[i])["CKBA"];
				if (((IRow)myRows[i])["CKEA"] == "") 
					strCounterTo = ((IRow)myRows[i])["CKES"];
				else
					strCounterTo = ((IRow)myRows[i])["CKEA"];
				datCounterFrom = UT.CedaFullDateToDateTime(strCounterFrom);
				datCounterTo = UT.CedaFullDateToDateTime(strCounterTo);
				strTmpFrom = datCounterFrom.ToShortTimeString();
				strTmpTo = datCounterTo.ToShortTimeString();
				timeSpan = datCounterTo - datCounterFrom;
				strValues += timeSpan.TotalMinutes.ToString() + "," + strTmpFrom + "-" + strTmpTo + "\n";
				sb.Append(strValues);
			}
		}

		/// <summary>
		/// Prepare the loaded data according to the report's needs.
		/// Prepare line by line the data a logical records and put
		/// the data into the Tab control.
		/// </summary>
		private void PrepareReportData()
		{
			//"FLNO,ADID,ORGDES,SCHEDULE,ACTUAL,ACTYPE"
			int ilLine = 0;
			myCCA = myDB["DED_CCA"];
			lblProgress.Text = "Preparing Data";
			lblProgress.Refresh();
			progressBar1.Visible = true;
			progressBar1.Value = 0;
			int ilTotal = myAFT.Count;
			if(ilTotal == 0) ilTotal = 1;
			imArrivals = 0;
			imDepartures =0 ;
			imRotations = 0;
			imTotalFlights = 0;
			if(myCCA != null)
			{
				StringBuilder sb = new StringBuilder(10000);
				for(int i = 0; i < myAFT.Count; i++)
				{	
					imDepartures++;
					imTotalFlights++;
					if( i % 100 == 0)
					{
						int percent = Convert.ToInt32((i * 100)/ilTotal);
						if (percent > 100) 
							percent = 100;
						progressBar1.Value = percent;
					}
					string strFlightValues = "";
					string strValues = "";
					ilLine = tabResult.GetLineCount() - 1;
					strFlightValues += myAFT[i]["ALC2"];
					if (myAFT[i]["ALC3"].Length > 0)
						strFlightValues += "/" + myAFT[i]["ALC3"];

					strFlightValues += ",";						
					strFlightValues += myAFT[i]["FLTN"] + ",";

					strFlightValues += myAFT[i]["ACT3"] + ",";
					strFlightValues += myAFT[i]["VIA3"] + ",";
					strFlightValues += myAFT[i]["DES3"] + ",";
					strFlightValues += Helper.DateString(myAFT[i]["STOD"], "yyyy-MM-dd") + ",";
					strFlightValues += Helper.DateString(myAFT[i]["STOD"], "HH:mm") + ",";
					IRow [] rows = myCCA.RowsByIndexValue("FLNU", myAFT[i]["URNO"]);
					if(rows.Length == 0)
					{
						strValues = strFlightValues + ",,,\n";
//						sb.Append(strValues);
					}
					else
					{
						this.CompressCounterAllocations(strFlightValues,rows,sb);
					}
				}
				tabResult.InsertBuffer(sb.ToString(), "\n");
				tabResult.Refresh();
			}
			//tabResult.Sort("4", true, true);
			//tabResult.AutoSizeColumns();
			progressBar1.Visible = false;
			lblProgress.Text = "";
			tabResult.Refresh();
			lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
		}
		/// <summary>
		/// Calls the generic report. The contructor must have the Tab 
		/// control and the reports headers as well as the types for
		/// each column to enable the reports to transform data, e.g.
		/// the data/time formatting. The lengths of the columns are
		/// dynamically read out of the tab control's header.
		/// </summary>
		private void RunReport()
		{
			string strSubHeader = "";
			strSubHeader = "From: " + this.ucView1.Viewer.PeriodFrom.ToString("dd.MM.yy'/'HH:mm") + " to " + this.ucView1.Viewer.PeriodTo.ToString("dd.MM.yy'/'HH:mm");
			strSubHeader += " View: " + this.ucView1.Viewer.CurrentView;
			strSubHeader += "       (Flights: " + imTotalFlights.ToString();
			strSubHeader += " ARR: " + imArrivals.ToString();
			strSubHeader += " DEP: " + imDepartures.ToString() + ")";
			prtFIPS_Landscape rpt = new prtFIPS_Landscape(tabResult, "Counter Schedule", strSubHeader, "", 10);
			frmPrintPreview frm = new frmPrintPreview(rpt);
			frm.Show();
		}
		/// <summary>
		/// Sent by the tab control when the user double click.
		/// This is necessary for sorting.
		/// </summary>
		/// <param name="sender">The Tab control</param>
		/// <param name="e">Double click event parameters.</param>
		private void tabResult_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo == -1)
			{
				if( e.colNo == tabResult.CurrentSortColumn)
				{
					if( tabResult.SortOrderASC == true)
					{
						tabResult.Sort(e.colNo.ToString(), false, true);
					}
					else
					{
						tabResult.Sort(e.colNo.ToString(), true, true);
					}
				}
				else
				{
					tabResult.Sort( e.colNo.ToString(), true, true);
				}
				tabResult.Refresh();
			}
		}

		private void frmCounterScheduleForPVG_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			myDB.Unbind("AFT");
			myDB.Unbind("DED_CCA");
			//myDB.Unbind("COM_CCA");
			//myDB.Unbind("BLK");
		}

		private void ucView1_viewEvent(string view)
		{
			btnOK_Click(this,null);
		}

	}
}
