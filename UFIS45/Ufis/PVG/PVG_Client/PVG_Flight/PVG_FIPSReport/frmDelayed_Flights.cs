using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Ufis.Utils;
using Ufis.Data;

namespace FIPS_Reports
{
	/// <summary>
	/// Summary description for frmDelayed_Flights.
	/// </summary>
	public class frmDelayed_Flights : System.Windows.Forms.Form
	{
		#region _My Members

		/// <summary>
		/// The where statement: all "@@xx" fields will be replaced
		/// with the true values according to the user's entry.
		/// </summary>
		private string strWhereRaw = "WHERE (((STOA BETWEEN '@@FROM' AND '@@TO') OR (STOD BETWEEN '@@FROM' AND '@@TO')) OR " +
			"((TIFA BETWEEN '@@FROM' AND '@@TO') OR (TIFD BETWEEN '@@FROM' AND '@@TO'))) AND " + 
			"(FTYP<>'G' AND FTYP<>'S' AND FTYP<>'T')" ;
		private string strWhereDelayPart = " AND (DCD1='@@DEN' OR DCD2='@@DEN')";
		private string strWhereAirlinePart = " AND (ALC2='@@ALC2')";
		/// <summary>
		/// The logical field names of the tab control for internal 
		/// development purposes
		/// </summary>
		private string strLogicalFields = "FLNO,ADID,ORGDES,SCHEDULE,ACTUAL,ONOFBL,ACTYPE,DEL1,MIN1,DEL2,MIN2,DIFF";
		/// <summary>
		/// The header columns, which must be used for the report output
		/// </summary>
		private string strTabHeader =     "Flight  ,A/D  ,Org/Des  ,Schedule  ,Actual ,Onbl/Ofbl ,A/C  ,Del1 ,Min1 ,Del2 ,Min2 ,Diff ";
		/// <summary>
		/// The lengths, which must be used for the report's column widths
		/// </summary>
		private string strTabHeaderLens = "70,30,60,70,70,70,70,40,40,40,40,50";
		/// <summary>
		/// String for empty line generation in the tab control.
		/// </summary>
		private string strEmptyLine = ",,,,,,,,,,,";
		/// <summary>
		/// The one and only IDatabase obejct.
		/// </summary>
		private IDatabase myDB;
		/// <summary>
		/// ITable object for the AFTTAB
		/// </summary>
		private ITable myAFT;
		/// <summary>
		/// ITable object for the nature codes of DENTAB
		/// </summary>
		private ITable myDEN;
		/// <summary>
		/// The nature code as global valid member.
		/// </summary>
		private string strDecn = "";
		/// <summary>
		/// Airline if filter was set
		/// </summary>
		private string strAlc2 = "";
		/// <summary>
		/// Read from Ceda.ini [FIPS] ==> SHOW_DELAYCODE=ALPHABETICAL
		/// </summary>
		private bool DelayAlpha = true;
		/// <summary>
		/// Total of arrival flights in the report
		/// </summary>
		private int imArrivals = 0;
		/// <summary>
		/// Total of departure flights in the report
		/// </summary>
		private int imDepartures = 0;
		/// <summary>
		/// Total of rotationein the report
		/// </summary>
		private int imRotations = 0;
		/// <summary>
		/// Total of all flights in the report
		/// </summary>
		private int imTotalFlights = 0;
		#endregion _My Members

		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.DateTimePicker dtFrom;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.DateTimePicker dtTo;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Panel panelTab;
		private AxTABLib.AxTAB tabResult;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.ComboBox cbDelayCodes;
		private System.Windows.Forms.Label lblResults;
		private System.Windows.Forms.Button btnPrintPreview;
		private System.Windows.Forms.Button btnLoadPrint;
		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox txtAirline;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.NumericUpDown txtDelay;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmDelayed_Flights()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmDelayed_Flights));
			this.panelTop = new System.Windows.Forms.Panel();
			this.txtDelay = new System.Windows.Forms.NumericUpDown();
			this.label5 = new System.Windows.Forms.Label();
			this.txtAirline = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.lblProgress = new System.Windows.Forms.Label();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.btnLoadPrint = new System.Windows.Forms.Button();
			this.btnPrintPreview = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.cbDelayCodes = new System.Windows.Forms.ComboBox();
			this.label3 = new System.Windows.Forms.Label();
			this.dtTo = new System.Windows.Forms.DateTimePicker();
			this.label2 = new System.Windows.Forms.Label();
			this.dtFrom = new System.Windows.Forms.DateTimePicker();
			this.label1 = new System.Windows.Forms.Label();
			this.panelBody = new System.Windows.Forms.Panel();
			this.panelTab = new System.Windows.Forms.Panel();
			this.tabResult = new AxTABLib.AxTAB();
			this.lblResults = new System.Windows.Forms.Label();
			this.panelTop.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtDelay)).BeginInit();
			this.panelBody.SuspendLayout();
			this.panelTab.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
			this.SuspendLayout();
			// 
			// panelTop
			// 
			this.panelTop.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelTop.Controls.Add(this.txtDelay);
			this.panelTop.Controls.Add(this.label5);
			this.panelTop.Controls.Add(this.txtAirline);
			this.panelTop.Controls.Add(this.label4);
			this.panelTop.Controls.Add(this.lblProgress);
			this.panelTop.Controls.Add(this.progressBar1);
			this.panelTop.Controls.Add(this.btnLoadPrint);
			this.panelTop.Controls.Add(this.btnPrintPreview);
			this.panelTop.Controls.Add(this.btnCancel);
			this.panelTop.Controls.Add(this.btnOK);
			this.panelTop.Controls.Add(this.cbDelayCodes);
			this.panelTop.Controls.Add(this.label3);
			this.panelTop.Controls.Add(this.dtTo);
			this.panelTop.Controls.Add(this.label2);
			this.panelTop.Controls.Add(this.dtFrom);
			this.panelTop.Controls.Add(this.label1);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(0, 0);
			this.panelTop.Name = "panelTop";
			this.panelTop.Size = new System.Drawing.Size(636, 144);
			this.panelTop.TabIndex = 0;
			// 
			// txtDelay
			// 
			this.txtDelay.Location = new System.Drawing.Point(296, 64);
			this.txtDelay.Maximum = new System.Decimal(new int[] {
																	 1000,
																	 0,
																	 0,
																	 0});
			this.txtDelay.Name = "txtDelay";
			this.txtDelay.Size = new System.Drawing.Size(100, 20);
			this.txtDelay.TabIndex = 4;
			this.txtDelay.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.txtDelay.Value = new System.Decimal(new int[] {
																   10,
																   0,
																   0,
																   0});
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(212, 66);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(80, 16);
			this.label5.TabIndex = 17;
			this.label5.Text = "Delay Time >=";
			// 
			// txtAirline
			// 
			this.txtAirline.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtAirline.Location = new System.Drawing.Point(92, 64);
			this.txtAirline.Name = "txtAirline";
			this.txtAirline.TabIndex = 3;
			this.txtAirline.Text = "";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(12, 68);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(72, 16);
			this.label4.TabIndex = 15;
			this.label4.Text = "Airline:";
			// 
			// lblProgress
			// 
			this.lblProgress.Location = new System.Drawing.Point(416, 88);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(212, 16);
			this.lblProgress.TabIndex = 14;
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(412, 104);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(216, 23);
			this.progressBar1.TabIndex = 9;
			this.progressBar1.Visible = false;
			// 
			// btnLoadPrint
			// 
			this.btnLoadPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnLoadPrint.Location = new System.Drawing.Point(244, 104);
			this.btnLoadPrint.Name = "btnLoadPrint";
			this.btnLoadPrint.TabIndex = 7;
			this.btnLoadPrint.Text = "Loa&d + Print";
			this.btnLoadPrint.Click += new System.EventHandler(this.btnLoadPrint_Click);
			// 
			// btnPrintPreview
			// 
			this.btnPrintPreview.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnPrintPreview.Location = new System.Drawing.Point(168, 104);
			this.btnPrintPreview.Name = "btnPrintPreview";
			this.btnPrintPreview.TabIndex = 6;
			this.btnPrintPreview.Text = "&Print Preview";
			this.btnPrintPreview.Click += new System.EventHandler(this.btnPrintPreview_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCancel.Location = new System.Drawing.Point(320, 104);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 8;
			this.btnCancel.Text = "&Close";
			// 
			// btnOK
			// 
			this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnOK.Location = new System.Drawing.Point(92, 104);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 5;
			this.btnOK.Text = "&Load Data";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// cbDelayCodes
			// 
			this.cbDelayCodes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbDelayCodes.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cbDelayCodes.Location = new System.Drawing.Point(92, 32);
			this.cbDelayCodes.MaxDropDownItems = 16;
			this.cbDelayCodes.Name = "cbDelayCodes";
			this.cbDelayCodes.Size = new System.Drawing.Size(304, 24);
			this.cbDelayCodes.TabIndex = 2;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(12, 36);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(72, 16);
			this.label3.TabIndex = 4;
			this.label3.Text = "Delay Code:";
			// 
			// dtTo
			// 
			this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtTo.Location = new System.Drawing.Point(268, 4);
			this.dtTo.Name = "dtTo";
			this.dtTo.Size = new System.Drawing.Size(128, 20);
			this.dtTo.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(232, 8);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(24, 16);
			this.label2.TabIndex = 2;
			this.label2.Text = "to:";
			// 
			// dtFrom
			// 
			this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtFrom.Location = new System.Drawing.Point(92, 4);
			this.dtFrom.Name = "dtFrom";
			this.dtFrom.Size = new System.Drawing.Size(128, 20);
			this.dtFrom.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(12, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Date from:";
			// 
			// panelBody
			// 
			this.panelBody.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelBody.Controls.Add(this.panelTab);
			this.panelBody.Controls.Add(this.lblResults);
			this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelBody.Location = new System.Drawing.Point(0, 144);
			this.panelBody.Name = "panelBody";
			this.panelBody.Size = new System.Drawing.Size(636, 422);
			this.panelBody.TabIndex = 1;
			// 
			// panelTab
			// 
			this.panelTab.Controls.Add(this.tabResult);
			this.panelTab.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelTab.Location = new System.Drawing.Point(0, 16);
			this.panelTab.Name = "panelTab";
			this.panelTab.Size = new System.Drawing.Size(636, 406);
			this.panelTab.TabIndex = 2;
			// 
			// tabResult
			// 
			this.tabResult.ContainingControl = this;
			this.tabResult.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabResult.Location = new System.Drawing.Point(0, 0);
			this.tabResult.Name = "tabResult";
			this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
			this.tabResult.Size = new System.Drawing.Size(636, 406);
			this.tabResult.TabIndex = 0;
			this.tabResult.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabResult_SendLButtonDblClick);
			// 
			// lblResults
			// 
			this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lblResults.Location = new System.Drawing.Point(0, 0);
			this.lblResults.Name = "lblResults";
			this.lblResults.Size = new System.Drawing.Size(636, 16);
			this.lblResults.TabIndex = 1;
			this.lblResults.Text = "Report Results";
			this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// frmDelayed_Flights
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(636, 566);
			this.Controls.Add(this.panelBody);
			this.Controls.Add(this.panelTop);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "frmDelayed_Flights";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Delayed Flights ...";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.frmFlightByNatureCode_Closing);
			this.Load += new System.EventHandler(this.frmFlightByNatureCode_Load);
			this.panelTop.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.txtDelay)).EndInit();
			this.panelBody.ResumeLayout(false);
			this.panelTab.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion
		
		/// <summary>
		/// Load the form. Prepare and init all controls.
		/// </summary>
		/// <param name="sender">Form.</param>
		/// <param name="e">Event arguments.</param>
		private void frmFlightByNatureCode_Load(object sender, System.EventArgs e)
		{
			//SHOW_DELAYCODE=ALPHABETICAL
			Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\ceda.ini");
			string strAlpha = myIni.IniReadValue("FIPS", "SHOW_DELAYCODE");
			if(strAlpha == "ALPHABETICAL")
				DelayAlpha = true;
			else
				DelayAlpha = false;
			myDB = UT.GetMemDB();
			InitTimePickers();
			InitTab();
			PrepareFilterData();
		}
		/// <summary>
		/// initializes the From/to time control with the current day in
		/// the custom format "dd.MM.yyyy - HH:mm".
		/// </summary>
		private void InitTimePickers()
		{
			DateTime olFrom;
			DateTime olTo;
			
			olFrom = DateTime.Now;
			olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 0,0,0);
			olTo   = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 23,59,0);
			dtFrom.Value = olFrom;
			dtTo.Value = olTo;
			dtFrom.CustomFormat = "dd.MM.yyyy - HH:mm";
			dtTo.CustomFormat = "dd.MM.yyyy - HH:mm";
		}
		/// <summary>
		/// Initializes the tab controll.
		/// </summary>
		private void InitTab()
		{
			tabResult.ResetContent();
			tabResult.ShowHorzScroller(true);
			tabResult.EnableHeaderSizing(true);
			tabResult.SetTabFontBold(true);
			tabResult.LifeStyle = true;
			tabResult.LineHeight = 16;
			tabResult.FontName = "Arial";
			tabResult.FontSize = 14;
			tabResult.HeaderFontSize = 14;
			tabResult.AutoSizeByHeader = true;

			tabResult.HeaderString = strTabHeader;
			tabResult.LogicalFieldList = strLogicalFields;
			tabResult.HeaderLengthString = strTabHeaderLens;
			tabResult.ColumnWidthString = "12,1,3,8,8,8,3,2,4,2,4,4";
			tabResult.ColumnAlignmentString = "L,L,L,L,L,L,L,L,R,L,R,R";
		}
		/// <summary>
		/// Loads and prepare the data necessary for the filter. 
		/// In this case loads the nature codes and puts the
		/// data into the combobox.
		/// </summary>
		private void PrepareFilterData()
		{
			myDEN = myDB["DEN"];
			if(myDEN == null)
			{
				myDEN = myDB.Bind("DEN", "DEN", "DECA,DECN,DENA", "2,2,75", "DECA,DECN,DENA");
				myDEN.Load("");
			}
			else if(myDEN.Count == 0)
			{
				myDEN = myDB.Bind("DEN", "DEN", "DECA,DECN,DENA", "2,2,75", "DECA,DECN,DENA");
				myDEN.Load("ORDER BY TTYP");
			}
			cbDelayCodes.Items.Add("<None>");
			for(int i = 0; i < myDEN.Count; i++)
			{
				string strValue = myDEN[i]["DECA"] + " | " + myDEN[i]["DECN"] + " | " + myDEN[i]["DENA"];
				cbDelayCodes.Items.Add(strValue);
			}
			if(cbDelayCodes.Items.Count > 0)
				cbDelayCodes.SelectedIndex = 0;
		}
		/// <summary>
		/// Runs the data queries according to the reports needs and puts
		///	Calls the data prepare method and puts the prepared data into
		///	the tab control. After this is finished it calls the reports
		///	by sending the tab and the headers to the generic report.
		/// </summary>
		/// <param name="sender">Originator object of the call</param>
		/// <param name="e">Event arguments</param>
		private void btnOK_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			LoadReportData();
			PrepareReportData();
			//RunReport();
			this.Cursor = Cursors.Arrow;
		}
		/// <summary>
		/// Validates the user entry for nonsense.
		/// </summary>
		/// <returns></returns>
		private string ValidateUserEntry()
		{
			string strRet = "";
			int ilErrorCount = 1;


			//			TimeSpan span = dtTo.Value - dtFrom.Value;
			//			if(span.TotalDays > 10)
			//			{
			//				strRet += ilErrorCount.ToString() +  ". Please do not load more the 10 days!\n";
			//				ilErrorCount++;
			//			}
			if(dtFrom.Value >= dtTo.Value)
			{
				strRet += ilErrorCount.ToString() +  ". Date From is later than Date To!\n";
				ilErrorCount++;
			}
			if(cbDelayCodes.Text == "")
			{
				strRet += ilErrorCount.ToString() +  ". Please select a nature code!\n";
				ilErrorCount++;
			}
			return strRet;
		}
		/// <summary>
		/// Loads all necessary tables with filter criteria from the 
		/// database ==> to memoryDB.
		/// </summary>
		private void LoadReportData()
		{
			string strTmpWhere = strWhereRaw;
			//Clear the tab
			tabResult.ResetContent();

			// In the first step change the times to UTC if this is
			// necessary.
			DateTime datFrom;
			DateTime datTo;
			string strDateFrom = "";
			string strDateTo = "";
			datFrom = dtFrom.Value;
			datTo   = dtTo.Value;
			if(UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(dtFrom.Value);
				datTo   = UT.LocalToUtc(dtTo.Value);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
			}
			else
			{
				strDateFrom = UT.DateTimeToCeda(dtFrom.Value);
				strDateTo = UT.DateTimeToCeda(dtTo.Value);
			}
			// Extract the true Nature code from the selected entry of the combobox
			strDecn = "";
			if(cbDelayCodes.Text != "<None>")
			{
				string [] strArr = cbDelayCodes.Text.Split('|');
				if(DelayAlpha == true)
					strDecn = strArr[0].Trim();
				else
					strDecn = strArr[1].Trim();
			}
			strAlc2 = txtAirline.Text;

			//Load the data from AFTTAB
			myAFT = myDB.Bind("AFT", "AFT", "FLNO,ADID,ORG3,DES3,STOA,STOD,TIFA,TIFD,ONBL,OFBL,ACT3,DCD1,DCD2,DTD1,DTD2", "12,2,3,3,14,14,14,14,14,14,3,2,2,4,4", "FLNO,ADID,ORG3,DES3,STOA,STOD,TIFA,TIFD,ONBL,OFBL,ACT3,DCD1,DCD2,DTD1,DTD2");
			myAFT.Clear();
			myAFT.TimeFields = "STOA,STOD,TIFA,TIFD,ONBL,OFBL";
			myAFT.TimeFieldsInitiallyInUtc = true;
			//Prepare loop reading day by day
			DateTime datReadFrom = datFrom;
			DateTime datReadTo;
			TimeSpan tsDays = (datTo - datFrom);
			progressBar1.Value = 0;
			int ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) ilTotal = 1;
			lblProgress.Text = "Loading Data";
			lblProgress.Refresh();
			progressBar1.Show();
			int loopCnt = 1;
			do
			{
				int percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
				datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
				if( datReadTo > datTo) datReadTo = datTo;
				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);
				//patch the where statement according to the user's entry
				strTmpWhere = strWhereRaw;
				if(strAlc2 != "") strTmpWhere += strWhereAirlinePart;
				if(strDecn != "") strTmpWhere += strWhereDelayPart;
				strTmpWhere = strTmpWhere.Replace("@@FROM", strDateFrom);
				strTmpWhere = strTmpWhere.Replace("@@TO", strDateTo );
				strTmpWhere = strTmpWhere.Replace("@@DEN", strDecn  );
				strTmpWhere = strTmpWhere.Replace("@@ALC2", strAlc2 );

				//Load the data from AFTTAB
				myAFT.Load(strTmpWhere);
				datReadFrom += new TimeSpan(1, 0, 0, 0, 0);
				loopCnt++;
			}while(datReadFrom <= datReadTo);
			myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
			lblProgress.Text = "";
			progressBar1.Hide();


		}
		/// <summary>
		/// Prepare the loaded data according to the report's needs.
		/// Prepare line by line the data a logical records and put
		/// the data into the Tab control.
		/// </summary>
		private void PrepareReportData()
		{
			//"FLNO,ADID,ORGDES,SCHEDULE,ACTUAL,ACTYPE"
			imArrivals = 0;
			imDepartures =0 ;
			imRotations = 0;
			imTotalFlights = 0;

			//FLNO,ADID,ORGDES,SCHEDULE,ACTUAL,ONOFBL,ACTYPE,DEL1,MIN1,DEL2,MIN2,DIFF
			StringBuilder sb = new StringBuilder(10000);			
			for(int i = 0; i < myAFT.Count; i++)
			{
				string strValues = "";
				string strAdid = myAFT[i]["ADID"];
				if(strAdid == "A")
				{
					long llDelay = 0;
					if(myAFT[i]["DCD1"] != "" || myAFT[i]["DCD2"] != "" ||
					   (myAFT[i]["STOA"] != "" && myAFT[i]["ONBL"] != "" && myAFT[i]["STOA"] != myAFT[i]["ONBL"]) )
					{
						DateTime olStoa = UT.CedaFullDateToDateTime(myAFT[i]["STOA"]);
						DateTime olOnbl = UT.CedaFullDateToDateTime(myAFT[i]["ONBL"]);
						TimeSpan olDelay = olOnbl - olStoa;
						long llTxtDelay = Convert.ToInt64(txtDelay.Text);
						llDelay = Convert.ToInt64(olDelay.TotalMinutes);
						if( llDelay >= llTxtDelay)
						{
							imArrivals++;
							imTotalFlights++;
							strValues += myAFT[i]["FLNO"] + ",";
							strValues += myAFT[i]["ADID"] + ",";
							strValues += myAFT[i]["ORG3"] + ",";
							strValues += Helper.DateString(myAFT[i]["STOA"], "dd'/'HH:mm") + ",";
							strValues += Helper.DateString(myAFT[i]["TIFA"], "dd'/'HH:mm") + ",";
							strValues += Helper.DateString(myAFT[i]["ONBL"], "dd'/'HH:mm") + ",";
							strValues += myAFT[i]["ACT3"] + ",";
							strValues += myAFT[i]["DCD1"] + ",";
							strValues += myAFT[i]["DTD1"] + ",";
							strValues += myAFT[i]["DCD2"] + ",";
							strValues += myAFT[i]["DTD2"] + ",";
							strValues += llDelay.ToString() + "\n";
							sb.Append(strValues);
						}
					}
				}
				if(strAdid == "D")
				{
					long llDelay = 0;
					if(myAFT[i]["DCD1"] != "" || myAFT[i]["DCD2"] != "" ||
						(myAFT[i]["STOD"] != "" && myAFT[i]["OFBL"] != "" && myAFT[i]["STOD"] != myAFT[i]["OFBL"]) )
					{
						DateTime olStod = UT.CedaFullDateToDateTime(myAFT[i]["STOD"]);
						DateTime olOfbl = UT.CedaFullDateToDateTime(myAFT[i]["OFBL"]);
						TimeSpan olDelay = olOfbl - olStod;
						long llTxtDelay = Convert.ToInt64(txtDelay.Text);
						llDelay = Convert.ToInt64(olDelay.TotalMinutes);
						if( llDelay >= llTxtDelay)
						{
							imDepartures++;
							imTotalFlights++;
							strValues += myAFT[i]["FLNO"] + ",";
							strValues += myAFT[i]["ADID"] + ",";
							strValues += myAFT[i]["DES3"] + ",";
							strValues += Helper.DateString(myAFT[i]["STOD"], "dd'/'HH:mm") + ",";
							strValues += Helper.DateString(myAFT[i]["TIFD"], "dd'/'HH:mm") + ",";
							strValues += Helper.DateString(myAFT[i]["OFBL"], "dd'/'HH:mm") + ",";
							strValues += myAFT[i]["ACT3"] + ",";
							strValues += myAFT[i]["DCD1"] + ",";
							strValues += myAFT[i]["DTD1"] + ",";
							strValues += myAFT[i]["DCD2"] + ",";
							strValues += myAFT[i]["DTD2"] + ",";
							strValues += llDelay.ToString() + "\n";
							sb.Append(strValues);
						}
					}
					//					tabResult.InsertTextLine(strEmptyLine, false);
//					ilLine = tabResult.GetLineCount() - 1;
//					tabResult.SetFieldValues(ilLine, "FLNO", myAFT[i]["FLNO"]);
//					tabResult.SetFieldValues(ilLine, "ADID", strAdid);
//					tabResult.SetFieldValues(ilLine, "ORGDES", myAFT[i]["DES3"]);
//					strSchedule = myAFT[i]["STOD"];
//					tmpDate = UT.CedaFullDateToDateTime(strSchedule);
//					tabResult.SetFieldValues(ilLine, "SCHEDULE", tmpDate.ToString("dd'/'HH:mm"));
//					strActual = myAFT[i]["TIFD"];
//					tmpDate = UT.CedaFullDateToDateTime(strActual);
//					tabResult.SetFieldValues(ilLine, "ACTUAL", tmpDate.ToString("dd'/'HH:mm"));
//					tabResult.SetFieldValues(ilLine, "ACTYPE", myAFT[i]["ACT3"]);
				}
			}
			tabResult.InsertBuffer(sb.ToString(), "\n");
			//tabResult.Sort("4", true, true);
			//tabResult.AutoSizeColumns();
			tabResult.Refresh();
			lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
		}
		/// <summary>
		/// Calls the generic report. The contructor must have the Tab 
		/// control and the reports headers as well as the types for
		/// each column to enable the reports to transform data, e.g.
		/// the data/time formatting. The lengths of the columns are
		/// dynamically read out of the tab control's header.
		/// </summary>
		private void RunReport()
		{
			string strSubHeader = "";
			string strHeader = "";
			strSubHeader = "From: " + dtFrom.Value.ToString("dd.MM.yy'/'HH:mm") + " to " + 
				dtTo.Value.ToString("dd.MM.yy'/'HH:mm");
			strSubHeader += "       (Flights: " + imTotalFlights.ToString();
			strSubHeader += " ARR: " + imArrivals.ToString();
			strSubHeader += " DEP: " + imDepartures.ToString() + ")";
			strHeader = "Delayed Flights";
			if(strDecn != "")
				strHeader += " ==> Reason: " + strDecn;
			if(strAlc2 != "")
				strHeader += " Airline: " + strAlc2;
			rptFIPS rpt = new rptFIPS(tabResult, strHeader, strSubHeader, "", 10);
			frmPrintPreview frm = new frmPrintPreview(rpt);
			frm.Show();
		}
		/// <summary>
		/// Start the print preview without data selection.
		/// This can be used to adjust the column widths and
		/// the sorting of the tab before printing.
		/// </summary>
		/// <param name="sender">Originator is the form</param>
		/// <param name="e">Event arguments.</param>
		private void btnPrintPreview_Click(object sender, System.EventArgs e)
		{
			RunReport();
		}
		/// <summary>
		/// Loads data and dirctly calls the print preview.
		/// </summary>
		/// <param name="sender">Originator is the form</param>
		/// <param name="e">Event arguments.</param>
		private void btnLoadPrint_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			LoadReportData();
			PrepareReportData();
			RunReport();
			this.Cursor = Cursors.Arrow;
		}
		/// <summary>
		/// Sent by the tab control when the user double click.
		/// This is necessary for sorting.
		/// </summary>
		/// <param name="sender">The Tab control</param>
		/// <param name="e">Double click event parameters.</param>
		private void tabResult_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo == -1)
			{
				if( e.colNo == tabResult.CurrentSortColumn)
				{
					if( tabResult.SortOrderASC == true)
					{
						tabResult.Sort(e.colNo.ToString(), false, true);
					}
					else
					{
						tabResult.Sort(e.colNo.ToString(), true, true);
					}
				}
				else
				{
					tabResult.Sort( e.colNo.ToString(), true, true);
				}
				tabResult.Refresh();
			}
		}

		private void frmFlightByNatureCode_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			myDB.Unbind("DEN");
			myDB.Unbind("AFT");
		}
	}
}

