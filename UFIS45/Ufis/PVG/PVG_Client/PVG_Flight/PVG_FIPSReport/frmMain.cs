using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Ufis.Utils;
using Ufis.Data; 

namespace FIPS_Reports
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class frmMain : System.Windows.Forms.Form
	{
		#region My Members
		/// <summary>
		/// The one and only IDatabase member reference within 
		/// the application. This is reused in all other modules.
		/// </summary>
		private IDatabase myDB;
		private ITable myCfg;
		private int isCustomerPVG = -1;

		#endregion My Members

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label lblVersion;
		private System.Windows.Forms.Button btnFlightsByNatureCode;
		private System.Windows.Forms.CheckBox cbTimeInLocal;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label lblServer;
		private System.Windows.Forms.Button btnCounterSchedule;
		private System.Windows.Forms.Button btnStatisticCancelledFlights;
		private System.Windows.Forms.Button btnBeltAllocations;
		private System.Windows.Forms.Button btnOvernightFlights;
		private System.Windows.Forms.Button btnInventory;
		private System.Windows.Forms.Button btnGPUUsage;
		private System.Windows.Forms.Button btnDailyFlightLog;
		private System.Windows.Forms.Button btnSpotUsingRate;
		private System.Windows.Forms.Button btnStatistAccToArrDep;
		private System.Windows.Forms.Button btnStatistCancelledFlightSummary;
		private System.Windows.Forms.Button btnStatistOvernightArrSummary;
		private System.Windows.Forms.Button btnStatistAccArrDep;
		private System.Windows.Forms.Button btnAircraftMovementsPerHour;
		private System.Windows.Forms.Button btnSeatStatisDomIntComb;
		private System.Windows.Forms.Button btnStatistDomesticIntComb;
		private System.Windows.Forms.Button btnStatistRushHour;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button5;
		private System.ComponentModel.IContainer components;

		/// <summary>
		/// Constructor of the main form.
		/// </summary>
		public frmMain()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				} 
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmMain));
			this.panel1 = new System.Windows.Forms.Panel();
			this.lblServer = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.cbTimeInLocal = new System.Windows.Forms.CheckBox();
			this.lblVersion = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.panel2 = new System.Windows.Forms.Panel();
			this.button3 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.btnSeatStatisDomIntComb = new System.Windows.Forms.Button();
			this.btnStatistDomesticIntComb = new System.Windows.Forms.Button();
			this.btnStatistRushHour = new System.Windows.Forms.Button();
			this.btnAircraftMovementsPerHour = new System.Windows.Forms.Button();
			this.btnStatistAccArrDep = new System.Windows.Forms.Button();
			this.btnStatistOvernightArrSummary = new System.Windows.Forms.Button();
			this.btnStatistCancelledFlightSummary = new System.Windows.Forms.Button();
			this.btnStatistAccToArrDep = new System.Windows.Forms.Button();
			this.btnSpotUsingRate = new System.Windows.Forms.Button();
			this.btnDailyFlightLog = new System.Windows.Forms.Button();
			this.btnGPUUsage = new System.Windows.Forms.Button();
			this.btnInventory = new System.Windows.Forms.Button();
			this.btnOvernightFlights = new System.Windows.Forms.Button();
			this.btnBeltAllocations = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.btnStatisticCancelledFlights = new System.Windows.Forms.Button();
			this.btnCounterSchedule = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.btnFlightsByNatureCode = new System.Windows.Forms.Button();
			this.button5 = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.White;
			this.panel1.Controls.Add(this.lblServer);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.cbTimeInLocal);
			this.panel1.Controls.Add(this.lblVersion);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.pictureBox1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(704, 64);
			this.panel1.TabIndex = 0;
			// 
			// lblServer
			// 
			this.lblServer.Location = new System.Drawing.Point(128, 4);
			this.lblServer.Name = "lblServer";
			this.lblServer.Size = new System.Drawing.Size(136, 16);
			this.lblServer.TabIndex = 5;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(80, 4);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(48, 16);
			this.label3.TabIndex = 4;
			this.label3.Text = "Server:";
			// 
			// cbTimeInLocal
			// 
			this.cbTimeInLocal.Location = new System.Drawing.Point(80, 48);
			this.cbTimeInLocal.Name = "cbTimeInLocal";
			this.cbTimeInLocal.Size = new System.Drawing.Size(160, 16);
			this.cbTimeInLocal.TabIndex = 3;
			this.cbTimeInLocal.Text = "Times in local Time";
			this.cbTimeInLocal.Visible = false;
			this.cbTimeInLocal.CheckedChanged += new System.EventHandler(this.cbTimeInLocal_CheckedChanged);
			// 
			// lblVersion
			// 
			this.lblVersion.Location = new System.Drawing.Point(608, 0);
			this.lblVersion.Name = "lblVersion";
			this.lblVersion.Size = new System.Drawing.Size(96, 23);
			this.lblVersion.TabIndex = 2;
			this.lblVersion.Text = "Version: 4.5.1.2";
			this.lblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(536, 48);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(168, 16);
			this.label2.TabIndex = 1;
			this.label2.Text = "(c) UFIS Airport Solutions GmbH";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(8, 0);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(64, 64);
			this.pictureBox1.TabIndex = 0;
			this.pictureBox1.TabStop = false;
			// 
			// imageList1
			// 
			this.imageList1.ImageSize = new System.Drawing.Size(59, 59);
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
			// 
			// panel2
			// 
			this.panel2.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panel2.Controls.Add(this.button5);
			this.panel2.Controls.Add(this.button3);
			this.panel2.Controls.Add(this.button2);
			this.panel2.Controls.Add(this.button1);
			this.panel2.Controls.Add(this.btnSeatStatisDomIntComb);
			this.panel2.Controls.Add(this.btnStatistDomesticIntComb);
			this.panel2.Controls.Add(this.btnStatistRushHour);
			this.panel2.Controls.Add(this.btnAircraftMovementsPerHour);
			this.panel2.Controls.Add(this.btnStatistAccArrDep);
			this.panel2.Controls.Add(this.btnStatistOvernightArrSummary);
			this.panel2.Controls.Add(this.btnStatistCancelledFlightSummary);
			this.panel2.Controls.Add(this.btnStatistAccToArrDep);
			this.panel2.Controls.Add(this.btnSpotUsingRate);
			this.panel2.Controls.Add(this.btnDailyFlightLog);
			this.panel2.Controls.Add(this.btnGPUUsage);
			this.panel2.Controls.Add(this.btnInventory);
			this.panel2.Controls.Add(this.btnOvernightFlights);
			this.panel2.Controls.Add(this.btnBeltAllocations);
			this.panel2.Controls.Add(this.button4);
			this.panel2.Controls.Add(this.btnStatisticCancelledFlights);
			this.panel2.Controls.Add(this.btnCounterSchedule);
			this.panel2.Controls.Add(this.label1);
			this.panel2.Controls.Add(this.btnFlightsByNatureCode);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(0, 64);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(704, 478);
			this.panel2.TabIndex = 1;
			this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
			// 
			// button3
			// 
			this.button3.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.button3.Location = new System.Drawing.Point(372, 228);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(324, 23);
			this.button3.TabIndex = 25;
			this.button3.Text = "Aircraft financial account";
			this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// button2
			// 
			this.button2.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.button2.Location = new System.Drawing.Point(372, 200);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(324, 23);
			this.button2.TabIndex = 24;
			this.button2.Text = "Statistics of abnormal flights at Pudong Airport";
			this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// button1
			// 
			this.button1.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.button1.Location = new System.Drawing.Point(372, 172);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(324, 23);
			this.button1.TabIndex = 23;
			this.button1.Text = "Statistics of blocked Resources";
			this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// btnSeatStatisDomIntComb
			// 
			this.btnSeatStatisDomIntComb.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnSeatStatisDomIntComb.Location = new System.Drawing.Point(372, 144);
			this.btnSeatStatisDomIntComb.Name = "btnSeatStatisDomIntComb";
			this.btnSeatStatisDomIntComb.Size = new System.Drawing.Size(324, 23);
			this.btnSeatStatisDomIntComb.TabIndex = 22;
			this.btnSeatStatisDomIntComb.Text = "Seat Statistics according to Domestic/Internations/Combined";
			this.btnSeatStatisDomIntComb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btnStatistDomesticIntComb
			// 
			this.btnStatistDomesticIntComb.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnStatistDomesticIntComb.Location = new System.Drawing.Point(372, 116);
			this.btnStatistDomesticIntComb.Name = "btnStatistDomesticIntComb";
			this.btnStatistDomesticIntComb.Size = new System.Drawing.Size(324, 23);
			this.btnStatistDomesticIntComb.TabIndex = 17;
			this.btnStatistDomesticIntComb.Text = "Statistics according to Domestic/International/Combined";
			this.btnStatistDomesticIntComb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnStatistDomesticIntComb.Click += new System.EventHandler(this.btnStatistDomesticIntComb_Click);
			// 
			// btnStatistRushHour
			// 
			this.btnStatistRushHour.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnStatistRushHour.Location = new System.Drawing.Point(372, 88);
			this.btnStatistRushHour.Name = "btnStatistRushHour";
			this.btnStatistRushHour.Size = new System.Drawing.Size(324, 23);
			this.btnStatistRushHour.TabIndex = 16;
			this.btnStatistRushHour.Text = "Check-in Counter using";
			this.btnStatistRushHour.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnStatistRushHour.Click += new System.EventHandler(this.btnStatistRushHour_Click);
			// 
			// btnAircraftMovementsPerHour
			// 
			this.btnAircraftMovementsPerHour.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnAircraftMovementsPerHour.Location = new System.Drawing.Point(372, 60);
			this.btnAircraftMovementsPerHour.Name = "btnAircraftMovementsPerHour";
			this.btnAircraftMovementsPerHour.Size = new System.Drawing.Size(324, 23);
			this.btnAircraftMovementsPerHour.TabIndex = 15;
			this.btnAircraftMovementsPerHour.Text = "Aircraft Movements per Hour";
			this.btnAircraftMovementsPerHour.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnAircraftMovementsPerHour.Click += new System.EventHandler(this.btnAircraftMovementsPerHour_Click);
			// 
			// btnStatistAccArrDep
			// 
			this.btnStatistAccArrDep.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnStatistAccArrDep.Location = new System.Drawing.Point(372, 32);
			this.btnStatistAccArrDep.Name = "btnStatistAccArrDep";
			this.btnStatistAccArrDep.Size = new System.Drawing.Size(324, 23);
			this.btnStatistAccArrDep.TabIndex = 14;
			this.btnStatistAccArrDep.Text = "Statistics According to Arrival / Departure ";
			this.btnStatistAccArrDep.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnStatistAccArrDep.Click += new System.EventHandler(this.btnStatistAccArrDep_Click);
			// 
			// btnStatistOvernightArrSummary
			// 
			this.btnStatistOvernightArrSummary.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnStatistOvernightArrSummary.Location = new System.Drawing.Point(16, 368);
			this.btnStatistOvernightArrSummary.Name = "btnStatistOvernightArrSummary";
			this.btnStatistOvernightArrSummary.Size = new System.Drawing.Size(324, 23);
			this.btnStatistOvernightArrSummary.TabIndex = 13;
			this.btnStatistOvernightArrSummary.Text = "Statistics Overnight Arrival Flights (Summary)";
			this.btnStatistOvernightArrSummary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnStatistOvernightArrSummary.Click += new System.EventHandler(this.btnStatistOvernightArrSummary_Click);
			// 
			// btnStatistCancelledFlightSummary
			// 
			this.btnStatistCancelledFlightSummary.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnStatistCancelledFlightSummary.Location = new System.Drawing.Point(16, 340);
			this.btnStatistCancelledFlightSummary.Name = "btnStatistCancelledFlightSummary";
			this.btnStatistCancelledFlightSummary.Size = new System.Drawing.Size(324, 23);
			this.btnStatistCancelledFlightSummary.TabIndex = 12;
			this.btnStatistCancelledFlightSummary.Text = "Statistics Cancelled Flights (Summary)";
			this.btnStatistCancelledFlightSummary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnStatistCancelledFlightSummary.Click += new System.EventHandler(this.btnStatistCancelledFlightSummary_Click);
			// 
			// btnStatistAccToArrDep
			// 
			this.btnStatistAccToArrDep.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnStatistAccToArrDep.Location = new System.Drawing.Point(16, 312);
			this.btnStatistAccToArrDep.Name = "btnStatistAccToArrDep";
			this.btnStatistAccToArrDep.Size = new System.Drawing.Size(324, 23);
			this.btnStatistAccToArrDep.TabIndex = 11;
			this.btnStatistAccToArrDep.Text = "Statistics according to arr /dep.";
			this.btnStatistAccToArrDep.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnStatistAccToArrDep.Click += new System.EventHandler(this.btnStatistAccToArrDep_Click);
			// 
			// btnSpotUsingRate
			// 
			this.btnSpotUsingRate.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnSpotUsingRate.Location = new System.Drawing.Point(16, 284);
			this.btnSpotUsingRate.Name = "btnSpotUsingRate";
			this.btnSpotUsingRate.Size = new System.Drawing.Size(324, 23);
			this.btnSpotUsingRate.TabIndex = 10;
			this.btnSpotUsingRate.Text = "Spot using Rate";
			this.btnSpotUsingRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnSpotUsingRate.Click += new System.EventHandler(this.btnSpotUsingRate_Click);
			// 
			// btnDailyFlightLog
			// 
			this.btnDailyFlightLog.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnDailyFlightLog.Location = new System.Drawing.Point(16, 256);
			this.btnDailyFlightLog.Name = "btnDailyFlightLog";
			this.btnDailyFlightLog.Size = new System.Drawing.Size(324, 23);
			this.btnDailyFlightLog.TabIndex = 9;
			this.btnDailyFlightLog.Text = "Daily Flight Log";
			this.btnDailyFlightLog.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnDailyFlightLog.Click += new System.EventHandler(this.btnDailyFlightLog_Click);
			// 
			// btnGPUUsage
			// 
			this.btnGPUUsage.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnGPUUsage.Location = new System.Drawing.Point(16, 228);
			this.btnGPUUsage.Name = "btnGPUUsage";
			this.btnGPUUsage.Size = new System.Drawing.Size(324, 23);
			this.btnGPUUsage.TabIndex = 8;
			this.btnGPUUsage.Text = "GPU Usage";
			this.btnGPUUsage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnGPUUsage.Click += new System.EventHandler(this.btnGPUUsage_Click);
			// 
			// btnInventory
			// 
			this.btnInventory.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnInventory.Location = new System.Drawing.Point(16, 200);
			this.btnInventory.Name = "btnInventory";
			this.btnInventory.Size = new System.Drawing.Size(324, 23);
			this.btnInventory.TabIndex = 7;
			this.btnInventory.Text = "Inventory (A/C on Ground)";
			this.btnInventory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnInventory.Click += new System.EventHandler(this.btnInventory_Click);
			// 
			// btnOvernightFlights
			// 
			this.btnOvernightFlights.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnOvernightFlights.Location = new System.Drawing.Point(16, 172);
			this.btnOvernightFlights.Name = "btnOvernightFlights";
			this.btnOvernightFlights.Size = new System.Drawing.Size(324, 23);
			this.btnOvernightFlights.TabIndex = 6;
			this.btnOvernightFlights.Text = "Overnight Flights";
			this.btnOvernightFlights.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnOvernightFlights.Click += new System.EventHandler(this.btnOvernightFlights_Click);
			// 
			// btnBeltAllocations
			// 
			this.btnBeltAllocations.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnBeltAllocations.Location = new System.Drawing.Point(16, 144);
			this.btnBeltAllocations.Name = "btnBeltAllocations";
			this.btnBeltAllocations.Size = new System.Drawing.Size(324, 23);
			this.btnBeltAllocations.TabIndex = 5;
			this.btnBeltAllocations.Text = "Baggage Belt Allocation Schedule";
			this.btnBeltAllocations.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnBeltAllocations.Click += new System.EventHandler(this.btnBeltAllocations_Click);
			// 
			// button4
			// 
			this.button4.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.button4.Location = new System.Drawing.Point(16, 116);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(324, 23);
			this.button4.TabIndex = 4;
			this.button4.Text = "Statistic: Delayed Flights";
			this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.button4.Click += new System.EventHandler(this.button4_Click);
			// 
			// btnStatisticCancelledFlights
			// 
			this.btnStatisticCancelledFlights.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnStatisticCancelledFlights.Location = new System.Drawing.Point(16, 88);
			this.btnStatisticCancelledFlights.Name = "btnStatisticCancelledFlights";
			this.btnStatisticCancelledFlights.Size = new System.Drawing.Size(324, 23);
			this.btnStatisticCancelledFlights.TabIndex = 3;
			this.btnStatisticCancelledFlights.Text = "Statistic: Cancelled Flights";
			this.btnStatisticCancelledFlights.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnStatisticCancelledFlights.Click += new System.EventHandler(this.btnStatisticCancelledFlights_Click);
			// 
			// btnCounterSchedule
			// 
			this.btnCounterSchedule.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCounterSchedule.Location = new System.Drawing.Point(16, 60);
			this.btnCounterSchedule.Name = "btnCounterSchedule";
			this.btnCounterSchedule.Size = new System.Drawing.Size(324, 23);
			this.btnCounterSchedule.TabIndex = 2;
			this.btnCounterSchedule.Text = "Counter Schedule";
			this.btnCounterSchedule.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnCounterSchedule.Click += new System.EventHandler(this.btnCounterSchedule_Click);
			// 
			// label1
			// 
			this.label1.Dock = System.Windows.Forms.DockStyle.Top;
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(704, 32);
			this.label1.TabIndex = 1;
			this.label1.Text = "Please click one of the following buttons to activate the respective report:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// btnFlightsByNatureCode
			// 
			this.btnFlightsByNatureCode.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnFlightsByNatureCode.Location = new System.Drawing.Point(16, 32);
			this.btnFlightsByNatureCode.Name = "btnFlightsByNatureCode";
			this.btnFlightsByNatureCode.Size = new System.Drawing.Size(324, 23);
			this.btnFlightsByNatureCode.TabIndex = 0;
			this.btnFlightsByNatureCode.Text = "Flights By Natures/Aircraft/Airlines/Origin/Destinations/Reg.";
			this.btnFlightsByNatureCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnFlightsByNatureCode.Click += new System.EventHandler(this.btnFlightsByNatureCode_Click);
			// 
			// button5
			// 
			this.button5.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.button5.Location = new System.Drawing.Point(372, 256);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(324, 23);
			this.button5.TabIndex = 26;
			this.button5.Text = "Average flight time";
			this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.button5.Click += new System.EventHandler(this.button5_Click);
			// 
			// frmMain
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(704, 542);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "frmMain";
			this.Text = "FIPS Reports";
			this.Load += new System.EventHandler(this.frmMain_Load);
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// Instantiates the frmMain, which is the
		/// main form of the application and starts it.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new frmMain());
		}

		/// <summary>
		/// Opens the Flights by Nature Code Filter dialog
		/// </summary>
		/// <param name="sender">Originator of the call</param>
		/// <param name="e">The event arguments</param>
		private void btnFlightsByNatureCode_Click(object sender, System.EventArgs e)
		{
			if (IsCustomerPVG())
			{
				frmFlightByNatureCodeForPVG frm = new frmFlightByNatureCodeForPVG();
				frm.ShowDialog(this);
			}
			else
			{
				frmFlightByNatureCode frm = new frmFlightByNatureCode();
				frm.ShowDialog(this);
			}
		}
		/// <summary>
		/// Initialize the main form of the application.
		/// Connect to CEDA, init the version and so on.
		/// </summary>
		/// <param name="sender">Originator of the call</param>
		/// <param name="e">Event arguments</param>
		private void frmMain_Load(object sender, System.EventArgs e)
		{
			Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\ceda.ini");
			UT.ServerName = myIni.IniReadValue("GLOBAL", "HOSTNAME");
			UT.Hopo = myIni.IniReadValue("GLOBAL", "HOMEAIRPORT");

			string strUTCConvertion = "";
			strUTCConvertion = myIni.IniReadValue("GLOBAL", "UTCCONVERTIONS");
			if (strUTCConvertion == "NO")
			{
				UT.UTCOffset = 0;
			}
			else
			{
				DateTime datLocal = DateTime.Now;
				DateTime datUTC = DateTime.UtcNow;//CFC.GetUTC();
				TimeSpan span = datLocal - datUTC;

				UT.UTCOffset = (short)span.Hours;
			}

			myDB = UT.GetMemDB();
			bool result = myDB.Connect(UT.ServerName,UT.UserName,UT.Hopo,"TAB","FIPS_RPT");

			UT.IsTimeInUtc = true;

			lblVersion.Text = "Version: ";
			lblVersion.Text += Application.ProductVersion;
			lblServer.Text = UT.ServerName;

			// check, if column for Abnormal flight report is available in delay code table
			if (!IsCustomerPVG())
			{
				this.button2.Enabled = false;
				this.button2.Visible = false;
				this.button3.Enabled = false;
				this.button3.Visible = false;
				this.button5.Enabled = false;
				this.button5.Visible = false;
			}

		}
		/// <summary>
		/// Sets the UT namespace internal indicator to local times or UTC times,
		/// depending on the user's click onto the cbTimeInLocal check box.
		/// </summary>
		/// <param name="sender">Originator of the call</param>
		/// <param name="e">Event arguments</param>
		private void cbTimeInLocal_CheckedChanged(object sender, System.EventArgs e)
		{
			UT.IsTimeInUtc = !cbTimeInLocal.Checked;
		}
		/// <summary>
		/// Opens the Counter Schedule Filter dialog
		/// </summary>
		/// <param name="sender">Originator of the call</param>
		/// <param name="e">The event arguments</param>
		private void btnCounterSchedule_Click(object sender, System.EventArgs e)
		{
			if (this.IsCustomerPVG())
			{
				frmCounterScheduleForPVG frm = new frmCounterScheduleForPVG();
				frm.ShowDialog();
			}
			else
			{
				frmCounterSchedule frm = new frmCounterSchedule();
				frm.ShowDialog();
			}
		}
		/// <summary>
		/// Open the cancelled flights
		/// </summary>
		/// <param name="sender">Originator of the call</param>
		/// <param name="e">The event arguments</param>
		private void btnStatisticCancelledFlights_Click(object sender, System.EventArgs e)
		{
			if (this.IsCustomerPVG())
			{
				frmCancelled_FlightsForPVG frm = new frmCancelled_FlightsForPVG();
				frm.ShowDialog();
			}
			else
			{
				frmCancelled_Flights frm = new frmCancelled_Flights();
				frm.ShowDialog();
			}
		}
		/// <summary>
		/// Opens delayed flights
		/// </summary>
		/// <param name="sender">Originator of the call</param>
		/// <param name="e">The event arguments</param>
		private void button4_Click(object sender, System.EventArgs e)
		{
			if (this.IsCustomerPVG())
			{
				frmDelayed_FlightsForPVG frm = new frmDelayed_FlightsForPVG();
				frm.ShowDialog();
			}
			else
			{
				frmDelayed_Flights frm = new frmDelayed_Flights();
				frm.ShowDialog();
			}
		}
		/// <summary>
		/// Opens the belt allocations
		/// </summary>
		/// <param name="sender">Originator of the call</param>
		/// <param name="e">The event arguments</param>
		private void btnBeltAllocations_Click(object sender, System.EventArgs e)
		{
			if (this.IsCustomerPVG())
			{
				frmBeltAllocationsForPVG frm = new frmBeltAllocationsForPVG();
				frm.ShowDialog();
			}
			else
			{
				frmBeltAllocations frm = new frmBeltAllocations();
				frm.ShowDialog();
			}
		}
		/// <summary>
		/// Opens the overnight flights.
		/// </summary>
		/// <param name="sender">Originator of the call</param>
		/// <param name="e">The event arguments</param>
		private void btnOvernightFlights_Click(object sender, System.EventArgs e)
		{
			if (this.IsCustomerPVG())
			{
				frmOvernightFlightsForPVG frm = new frmOvernightFlightsForPVG();
				frm.ShowDialog();
			}
			else
			{
				frmOvernightFlights frm = new frmOvernightFlights();
				frm.ShowDialog();
			}
		}
		/// <summary>
		/// Opens the Inventory list (flights on ground)
		/// </summary>
		/// <param name="sender">Originator of the call</param>
		/// <param name="e">The event arguments</param>
		private void btnInventory_Click(object sender, System.EventArgs e)
		{
			ITable mySYS = myDB.Bind("SYS", "SYS", "TANA", "10", "TANA");
			mySYS.Load("WHERE TANA='FOG'");
			if(mySYS.Count > 0)
			{
				frmInventory frm = new frmInventory();
				frm.ShowDialog();
			}
			else
			{
				MessageBox.Show(this, "Flights on Ground is not supported (FOGTAB missing)!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}
			myDB.Unbind("SYS");
		}
		/// <summary>
		/// Opens the form for GPU usage
		/// </summary>
		/// <param name="sender">Originator of the call</param>
		/// <param name="e">The event arguments</param>
		private void btnGPUUsage_Click(object sender, System.EventArgs e)
		{
			if (this.IsCustomerPVG())
			{
				frmGPUUsageForPVG frm = new frmGPUUsageForPVG();
				frm.ShowDialog();
			}
			else
			{
				frmGPUUsage frm = new frmGPUUsage();
				frm.ShowDialog();
			}
		}
		/// <summary>
		/// Opens the form for Daily flight log.
		/// </summary>
		/// <param name="sender">Originator of the call</param>
		/// <param name="e">The event arguments</param>
		private void btnDailyFlightLog_Click(object sender, System.EventArgs e)
		{
			try
			{
				if (this.IsCustomerPVG())
				{
					frmDaily_FlightLogForPVG frm = new frmDaily_FlightLogForPVG();
					frm.ShowDialog();
				}
				else
				{
					frmDaily_FlightLog frm = new frmDaily_FlightLog();
					frm.ShowDialog();
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show(this, ex.Message);
			}
		}


		private void panel2_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
		
		}

		private void btnAircraftsByHour_Click(object sender, System.EventArgs e)
		{
		}

		private void btnSpotUsingRate_Click(object sender, System.EventArgs e)
		{
			if (IsCustomerPVG())
			{
				frmSpot_UsingRateForPVG frm = new frmSpot_UsingRateForPVG();
				frm.ShowDialog();
			}
			else
			{
				frmSpot_UsingRate frm = new frmSpot_UsingRate();
				frm.ShowDialog();
			}
		}

		private void btnStatistAccToArrDep_Click(object sender, System.EventArgs e)
		{
			if (IsCustomerPVG())
			{
				frmStatistics_Arr_DepForPVG frm = new frmStatistics_Arr_DepForPVG();
				frm.ShowDialog();
			}
			else
			{
				frmStatistics_Arr_Dep frm = new frmStatistics_Arr_Dep();
				frm.ShowDialog();
			}
		}

		private void btnStatistCancelledFlightSummary_Click(object sender, System.EventArgs e)
		{
			if (IsCustomerPVG())
			{
				frmCancelled_FlightDetailForPVG frm = new frmCancelled_FlightDetailForPVG();
				frm.ShowDialog();
			}
			else
			{
				frmCancelled_FlightDetail frm = new frmCancelled_FlightDetail();
				frm.ShowDialog();
			}
		}

		private void btnStatistOvernightArrSummary_Click(object sender, System.EventArgs e)
		{
			if (IsCustomerPVG())
			{
				frmOverNight_FlightSummaryForPVG frm = new frmOverNight_FlightSummaryForPVG();
				frm.ShowDialog();
			}
			else
			{
				frmOverNight_FlightSummary frm = new frmOverNight_FlightSummary();
				frm.ShowDialog();
			}
		}

		private void btnStatistAccArrDep_Click(object sender, System.EventArgs e)
		{
			if (IsCustomerPVG())
			{
				frmStatisticsAccordingToArrivalDepartureForPVG frm = new frmStatisticsAccordingToArrivalDepartureForPVG();
				frm.ShowDialog();
			}
			else
			{
				frmStatisticsAccordingToArrivalDeparture frm = new frmStatisticsAccordingToArrivalDeparture();
				frm.ShowDialog();
			}
		}

		private void btnAircraftMovementsPerHour_Click(object sender, System.EventArgs e)
		{
			if (IsCustomerPVG())
			{
				frmAircraftMovementsperHourForPVG frm = new frmAircraftMovementsperHourForPVG();
				frm.ShowDialog();
			}
			else
			{
				frmAircraftMovementsperHour frm = new frmAircraftMovementsperHour();
				frm.ShowDialog();
			}
		}

		private void btnStatistRushHour_Click(object sender, System.EventArgs e)
		{
			if (IsCustomerPVG())
			{
				frmCheckin_statisticGraphForPVG frm = new frmCheckin_statisticGraphForPVG();
				frm.ShowDialog();
			}
			else
			{
				frmCheckin_statisticGraph frm = new frmCheckin_statisticGraph();
				frm.ShowDialog();
			}
		}

		private void btnStatistDomesticIntComb_Click(object sender, System.EventArgs e)
		{
			frmFlightStatisticsDomesticInternationalCombined frm = new frmFlightStatisticsDomesticInternationalCombined();
			frm.ShowDialog();
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			if (IsCustomerPVG())
			{
				frmBlockedTimesForPVG frm = new frmBlockedTimesForPVG();
				frm.ShowDialog();
			}
			else
			{
				frmBlockedTimes frm = new frmBlockedTimes();
				frm.ShowDialog();
			}
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			frmAbnormal_Flights frm = new frmAbnormal_Flights();
			frm.ShowDialog();
		}

		private bool IsColumnForAbnormalFlightsReportAvailable()
		{
			if (myDB == null)
				return false;
			return myDB.Reader.CedaAction("RT","SYSTAB","TANA,FINA","","WHERE Tana = 'DEN' AND Fina = 'RCOL'") && myDB.Reader.GetBufferCount() == 1;
		}

		private bool IsCustomerPVG()
		{
			if (this.isCustomerPVG == -1)
			{
				this.isCustomerPVG = this.IsColumnForAbnormalFlightsReportAvailable() == true ? 1 : 0;
			}
			
			if (this.isCustomerPVG == 1)
				return true;
			else
				return false;
		}

		private void button3_Click(object sender, System.EventArgs e)
		{
			frmAircraftFinancialAccountForPVG frm = new frmAircraftFinancialAccountForPVG();
			frm.ShowDialog();
		}

		private void button5_Click(object sender, System.EventArgs e)
		{
			frmAverageFlightTimeForPVG frm = new frmAverageFlightTimeForPVG();
			frm.ShowDialog();
		}
	}
}
