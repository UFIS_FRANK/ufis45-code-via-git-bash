using System;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using System.Drawing;
using ZedGraph;
using Ufis.Utils;

namespace FIPS_Reports
{
	public class rptSpotUsing : ActiveReport
	{
		#region _MyMembers
		double []  YFlightPositions;
		double [] YBlockingData;
		bool	WithValidity = true;
		private ZedGraph.ZedGraphControl myGraph;
		private string strMainHeader;
		private string strSubHeader;
		private string strReportTitle,
			           strYTitle, 
					   strXAxis, 
					   strCurveTitle1, 
					   strCurveTitle2;
		#endregion _MyMembers

		public rptSpotUsing(string mainHeader, string subHeader, double [] pYFlightPositions, double [] pYBlockingData, bool pWithValidity,
			                string reportTitle, string yTitle, string xAxis, string curveTitle1, string curveTitle2)
		{
			YFlightPositions = pYFlightPositions;
			YBlockingData = pYBlockingData;
			WithValidity = pWithValidity;
			strMainHeader = mainHeader;
			strSubHeader = subHeader;

			strReportTitle = reportTitle;
			strYTitle = yTitle;
			strXAxis = xAxis;
			strCurveTitle1 = curveTitle1;
			strCurveTitle2 = curveTitle2;

			InitializeReport();
		}

		private void rptGenericChart_ReportStart(object sender, System.EventArgs eArgs)
		{
			//*** Read the logo if exists and set it
			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			string strPrintLogoPath = myIni.IniReadValue("GLOBAL", "PRINTLOGO");
			Image myImage = null;
			try
			{
				if(strPrintLogoPath != "")
				{
					myImage = new Bitmap(strPrintLogoPath);
					Picture1.Image = myImage;
				}
			}
			catch(Exception)
			{
			}

			// MaxBarValue stores maximum of all bars during the loaded timeframe
			int MaxBarValue = 0;
			ZedGraph.BarItem mySecondCurve = null;

			lblHeader1.Text = strMainHeader;
			lblHeader2.Text = strSubHeader;
			txtDate.Text = DateTime.Now.ToString("dd.MM.yy'/'HH:mm");
			if(YFlightPositions == null)
				return;
			myGraph = ((ZedGraph.ZedGraphControl)this.zGraph.Control);
			ZedGraph.GraphPane myPane = myGraph.GraphPane;
			myPane.GraphItemList.Clear();
			myGraph.Controls.Clear();
			myPane.CurveList.Clear();
			// Set the titles and axis labels
			myPane.Title = strReportTitle;//"Number of spots"
			myPane.MarginLeft = 0.5f;
			myPane.MarginRight = 0f;
			myPane.XAxis.Title = strXAxis;//"time (in hours)";
			myPane.XAxis.TitleFontSpec.Size = 10f;
			myPane.YAxis.Title = strYTitle;//"Number of Spots/Parking stands";
			myPane.YAxis.ScaleFontSpec.Size = 8f;

			// Make up some random data points
			string [] str = new string[YFlightPositions.Length];
			int ilCurrHour = 0;
			for(int i = 0; i < YFlightPositions.Length; i++)
			{
				if(ilCurrHour == 24)
				{
					ilCurrHour = 0;
				}
				str[i] = ilCurrHour.ToString();
				ilCurrHour++;
			}
			MaxBarValue = 0;
			for (int i = 0; i < YFlightPositions.Length; i++)
			{
				if(YFlightPositions[i] > MaxBarValue) MaxBarValue = Convert.ToInt32(YFlightPositions[i]);

			}
			// Add a bar to the graph
			//"Occupied Spots"
			ZedGraph.BarItem myCurve = myPane.AddBar( strCurveTitle1, null, YFlightPositions, Color.White );
			myCurve.Bar.Fill = new Fill( Color.Blue, Color.White, Color.Blue, 0F );
			// turn off the bar border
			myCurve.Bar.Border.IsVisible = false;
			if(WithValidity == true && YBlockingData != null)
			{
				//"Not valid Spots"
				ZedGraph.BarItem myCurve2 = myPane.AddBar( strCurveTitle2, null, YBlockingData, Color.Red );
				mySecondCurve = myCurve2;
				myCurve2.Bar.Fill = new Fill( Color.Red, Color.White, Color.Red, 0F );
				myCurve2.Bar.Border.IsVisible = false;
				for (int i = 0; i < YBlockingData.Length; i++)
				{
					if(YBlockingData[i] > MaxBarValue) MaxBarValue = Convert.ToInt32(YBlockingData[i]);

				}
			}

			
			// Draw the X tics between the labels instead of at the labels
			myPane.XAxis.IsTicsBetweenLabels = true;
			myPane.XAxis.ScaleFontSpec.Size = 8f;

			// Set the XAxis labels
			myPane.XAxis.TextLabels = str;

			// Set the XAxis to Text type
			myPane.XAxis.Type = AxisType.Text;

			// Fill the axis background with a color gradient
			myPane.AxisFill = new Fill( Color.White, Color.LightGray, 45.0f );

			// disable the legend
			myPane.Legend.IsVisible = true;
			
			myPane.AxisChange( myGraph.CreateGraphics());
			//if more than 3 days should be shown in the report than the values will not be written to the bars
			//because the valuse can no longer be read (oferlapping texts)
			if(YFlightPositions.Length <= 72)
			{
				// The ValueHandler is a helper that does some position calculations for us.
				ValueHandler valueHandler = new ValueHandler( myPane, true );
				// Display a value for the maximum of each bar cluster
				// Shift the text items by x user scale units above the bars
				//const float shift = 0.5f;
				float shift = (3/100*MaxBarValue)*MaxBarValue;
				int ord = 0;
				foreach ( CurveItem curve in myPane.CurveList )
				{
					BarItem bar = curve as BarItem;

					if ( bar != null )
					{
						
						PointPairList points = curve.Points;

						for ( int i=0; i<points.Count; i++ )
						{
							
							double xVal = points[i].X ;//- 0.3d;

							// Calculate the Y value at the center of each bar
							//double yVal = valueHandler.BarCenterValue( curve, curve.GetBarWidth( myPane ),i, points[i].Y, ord );
							double yVal = points[i].Y;

							// format the label string to have 1 decimal place
							//string lab = xVal.ToString( "F0" );
							string lab = yVal.ToString();
				
							// don't show value "0" in the chart
							if(yVal != 0)
							{
										
								// create the text item (assumes the x axis is ordinal or text)
								// for negative bars, the label appears just above the zero value
				
								TextItem text = new TextItem( lab,(float) xVal ,(float) yVal + ( yVal >= 0 ? shift : -shift ));

								// tell Zedgraph to use user scale units for locating the TextItem
								text.Location.CoordinateFrame = CoordType.AxisXYScale;
								//text.Location.CoordinateFrame = CoordType.AxisXY2Scale;
								text.FontSpec.Size = 6;
								// AlignH the left-center of the text to the specified point
								if(WithValidity == false)
								{
									text.Location.AlignH =  AlignH.Center;
								}
								else
								{
									if(curve == myCurve)
									{
										text.Location.AlignH =  AlignH.Right;
									}
									if(curve == mySecondCurve)
									{
										text.Location.AlignH =  AlignH.Left;
									}
								}
								text.Location.AlignV =  yVal > 0 ? AlignV.Bottom : AlignV.Top;
								text.FontSpec.Border.IsVisible = false;
								// rotate the text 90 degrees
								text.FontSpec.Angle = 0;
								text.FontSpec.Fill.IsVisible = false;
								// add the TextItem to the list
								myPane.GraphItemList.Add( text );
							}
						}
					}

					ord++;
				}
			}
		}

		#region ActiveReports Designer generated code
		private PageHeader PageHeader = null;
		private Picture Picture1 = null;
		private Label lblHeader1 = null;
		private Label lblHeader2 = null;
		private Detail Detail = null;
		private CustomControl zGraph = null;
		private PageFooter PageFooter = null;
		private TextBox txtDate = null;
		private Label Label34 = null;
		public void InitializeReport()
		{
			this.LoadLayout(this.GetType(), "FIPS_Reports.rptSpotUsing.rpx");
			this.PageHeader = ((DataDynamics.ActiveReports.PageHeader)(this.Sections["PageHeader"]));
			this.Detail = ((DataDynamics.ActiveReports.Detail)(this.Sections["Detail"]));
			this.PageFooter = ((DataDynamics.ActiveReports.PageFooter)(this.Sections["PageFooter"]));
			this.Picture1 = ((DataDynamics.ActiveReports.Picture)(this.PageHeader.Controls[0]));
			this.lblHeader1 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[1]));
			this.lblHeader2 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[2]));
			this.zGraph = ((DataDynamics.ActiveReports.CustomControl)(this.Detail.Controls[0]));
			this.txtDate = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[0]));
			this.Label34 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[1]));
			// Attach Report Events
			this.ReportStart += new System.EventHandler(this.rptGenericChart_ReportStart);
		}

		#endregion
	}
}
