using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Ufis.Data;
using Ufis.Utils;
using ZedGraph;

namespace FIPS_Reports
{
	/// <summary>
	/// Summary description for frmAircraftMovementsperHour.
	/// This report creates bar-chart for Aircraft Movements per Hour.
	/// It can be selected if the chart is based on STA/STD, ONBL/OFBL or LAND/AIRB
	/// </summary>
	public class frmAircraftMovementsperHourForPVG : System.Windows.Forms.Form
	{
		#region _My_Members
		/// <summary>
		/// The where statement for Arrivals: all "@@xx" fields will be replaced
		/// with the true values according to the user's entry.
		/// </summary>
		private string strWhereRawA = "";
		/// <summary>
		/// The where statement for Departures: all "@@xx" fields will be replaced
		/// with the true values according to the user's entry.
		/// </summary>
		private string strWhereRawD = "";

		private string strWhereRawB = "";

		/// <summary>
		/// The logical field names of the tab control for internal 
		/// development purposes
		/// </summary>
		private string strLogicalFields = "URNO,FLNO,ADID,ORG3,DES3,STOA,STOD";
		/// <summary>
		/// The header columns, which must be used for the report output
		/// </summary>
		private string strTabHeader =     "URNO,FLNO,ADID,ORG3,DES3,STOA,STOD";
		/// <summary>
		/// The lengths, which must be used for the report's column widths
		/// </summary>
		private string strTabHeaderLens = "50,50,50,50,50,70,70";
		/// <summary>
		/// The one and only IDatabase obejct.
		/// </summary>
		private IDatabase myDB;
		/// <summary>
		/// ITable object for the AFTTAB
		/// </summary>
		private ITable myAFT;
		/// <summary>
		/// strUserSelection will contain the information on which times the chart will be based on
		/// </summary>
		private string strUserSelection = "";
		/// <summary>
		/// NumberOfMovements stores the number of Movements per Hour for every Hour in the selected timeframe
		/// </summary>
		private double [] NumberOfMovements;

		/// <summary>
		/// iHours gives the number of Hou-bars to be shown in the chart
		/// </summary>
		private int iHours = 0;

		/// <summary>
		/// strArrivalTimeKey identifies the afttab field to be loaded into iTable for Arrivals
		/// </summary>
		private string strArrivalTimeKey = "STOA";

		/// <summary>
		/// strDepartureTimeKey identifies the afttab field to be loaded into iTable for Departures
		/// </summary>
		private string strDepartureTimeKey = "STOD";

		/// <summary>
		/// strTimeFields identifies which timefields are used for the current time-base-selection
		/// </summary>
		private string strTimeFields = "STOA,STOD";
		/// <summary>
		/// FlightsTotal will contain the total number of flight shown in the report
		/// </summary>
		private int FlightsTotal = 0;

		/// <summary>
		/// FlightArrival will contain the total number of Arrival-Flights shown in this report
		/// </summary>
		private int FlightsArrival = 0;
		/// <summary>
		/// FlightDeparture will contain the total number of Departure-Flights shown in this report
		/// </summary>
		private int FlightsDeparture = 0;
		/// <summary>
		/// containts info if in 2.Header it should be stated UTC or LOCAL_TIMES
		/// </summary>
		private string strLOCAL_UTC = "";
		/// <summary>
		/// 
		/// </summary>
		private ZedGraph.ZedGraphControl myGraph;
		/// <summary>
		/// MaxNumbersOfMovements gives stores the maximum number of movements per hour during the loaded timeframe
		/// </summary>
		private int MaxNumberOfMovements = 0;


		#endregion _My_Members

		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.RadioButton btnOnblOfbl;
		private System.Windows.Forms.RadioButton btnLandAirb;
		private System.Windows.Forms.RadioButton btnStaStd;
		private System.Windows.Forms.Button btnPrint;
		private System.Windows.Forms.Label lblResults;
		private System.Windows.Forms.Button btnLoadData;
		private System.Windows.Forms.Panel panel_Result;
		private ZedGraph.ZedGraphControl zedGraphControl;
		private System.Windows.Forms.Button btnPrintPreview;
		private UserControls.ucView ucView1;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmAircraftMovementsperHourForPVG()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			myDB = UT.GetMemDB();
			this.ucView1.Report = "AircraftMovements";
			this.ucView1.viewEvent +=new UserControls.ucView.ViewChangedEvent(ucView1_viewEvent);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.panelTop = new System.Windows.Forms.Panel();
			this.ucView1 = new UserControls.ucView();
			this.btnPrintPreview = new System.Windows.Forms.Button();
			this.btnLoadData = new System.Windows.Forms.Button();
			this.btnStaStd = new System.Windows.Forms.RadioButton();
			this.btnLandAirb = new System.Windows.Forms.RadioButton();
			this.btnOnblOfbl = new System.Windows.Forms.RadioButton();
			this.lblProgress = new System.Windows.Forms.Label();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.btnPrint = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.lblResults = new System.Windows.Forms.Label();
			this.panel_Result = new System.Windows.Forms.Panel();
			this.zedGraphControl = new ZedGraph.ZedGraphControl();
			this.panelTop.SuspendLayout();
			this.panel_Result.SuspendLayout();
			this.SuspendLayout();
			// 
			// panelTop
			// 
			this.panelTop.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelTop.Controls.Add(this.ucView1);
			this.panelTop.Controls.Add(this.btnPrintPreview);
			this.panelTop.Controls.Add(this.btnLoadData);
			this.panelTop.Controls.Add(this.btnStaStd);
			this.panelTop.Controls.Add(this.btnLandAirb);
			this.panelTop.Controls.Add(this.btnOnblOfbl);
			this.panelTop.Controls.Add(this.lblProgress);
			this.panelTop.Controls.Add(this.progressBar1);
			this.panelTop.Controls.Add(this.btnPrint);
			this.panelTop.Controls.Add(this.btnCancel);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(0, 0);
			this.panelTop.Name = "panelTop";
			this.panelTop.Size = new System.Drawing.Size(848, 240);
			this.panelTop.TabIndex = 4;
			this.panelTop.Paint += new System.Windows.Forms.PaintEventHandler(this.panelTop_Paint);
			// 
			// ucView1
			// 
			this.ucView1.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.ucView1.Location = new System.Drawing.Point(32, 8);
			this.ucView1.Name = "ucView1";
			this.ucView1.Report = "";
			this.ucView1.Size = new System.Drawing.Size(304, 40);
			this.ucView1.TabIndex = 23;
			// 
			// btnPrintPreview
			// 
			this.btnPrintPreview.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnPrintPreview.Location = new System.Drawing.Point(144, 144);
			this.btnPrintPreview.Name = "btnPrintPreview";
			this.btnPrintPreview.TabIndex = 6;
			this.btnPrintPreview.Text = "&Print Preview";
			this.btnPrintPreview.Click += new System.EventHandler(this.btnPrintPreview_Click);
			// 
			// btnLoadData
			// 
			this.btnLoadData.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnLoadData.Location = new System.Drawing.Point(64, 144);
			this.btnLoadData.Name = "btnLoadData";
			this.btnLoadData.TabIndex = 5;
			this.btnLoadData.Text = "&Load Data";
			this.btnLoadData.Click += new System.EventHandler(this.btnLoadData_Click);
			// 
			// btnStaStd
			// 
			this.btnStaStd.Location = new System.Drawing.Point(200, 56);
			this.btnStaStd.Name = "btnStaStd";
			this.btnStaStd.TabIndex = 2;
			this.btnStaStd.Text = "STA / STD";
			this.btnStaStd.CheckedChanged += new System.EventHandler(this.btnStaStd_CheckedChanged);
			// 
			// btnLandAirb
			// 
			this.btnLandAirb.Location = new System.Drawing.Point(200, 104);
			this.btnLandAirb.Name = "btnLandAirb";
			this.btnLandAirb.TabIndex = 4;
			this.btnLandAirb.Text = "LAND / AIRB";
			this.btnLandAirb.CheckedChanged += new System.EventHandler(this.btnLandAirb_CheckedChanged);
			// 
			// btnOnblOfbl
			// 
			this.btnOnblOfbl.Location = new System.Drawing.Point(200, 80);
			this.btnOnblOfbl.Name = "btnOnblOfbl";
			this.btnOnblOfbl.TabIndex = 3;
			this.btnOnblOfbl.Text = "ONBL / OFBL";
			this.btnOnblOfbl.CheckedChanged += new System.EventHandler(this.btnOnblOfbl_CheckedChanged);
			// 
			// lblProgress
			// 
			this.lblProgress.Location = new System.Drawing.Point(404, 128);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(212, 16);
			this.lblProgress.TabIndex = 22;
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(404, 144);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(216, 23);
			this.progressBar1.TabIndex = 21;
			this.progressBar1.Visible = false;
			// 
			// btnPrint
			// 
			this.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnPrint.Location = new System.Drawing.Point(224, 144);
			this.btnPrint.Name = "btnPrint";
			this.btnPrint.TabIndex = 7;
			this.btnPrint.Text = "Loa&d + Print";
			this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCancel.Location = new System.Drawing.Point(304, 144);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 8;
			this.btnCancel.Text = "&Close";
			// 
			// lblResults
			// 
			this.lblResults.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lblResults.Location = new System.Drawing.Point(0, 240);
			this.lblResults.Name = "lblResults";
			this.lblResults.Size = new System.Drawing.Size(848, 16);
			this.lblResults.TabIndex = 6;
			this.lblResults.Text = "Report Results";
			this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// panel_Result
			// 
			this.panel_Result.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panel_Result.Controls.Add(this.zedGraphControl);
			this.panel_Result.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel_Result.Location = new System.Drawing.Point(0, 256);
			this.panel_Result.Name = "panel_Result";
			this.panel_Result.Size = new System.Drawing.Size(848, 502);
			this.panel_Result.TabIndex = 7;
			// 
			// zedGraphControl
			// 
			this.zedGraphControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.zedGraphControl.IsEnableHPan = true;
			this.zedGraphControl.IsEnableVPan = true;
			this.zedGraphControl.IsEnableZoom = true;
			this.zedGraphControl.IsScrollY2 = false;
			this.zedGraphControl.IsShowContextMenu = true;
			this.zedGraphControl.IsShowHScrollBar = false;
			this.zedGraphControl.IsShowPointValues = false;
			this.zedGraphControl.IsShowVScrollBar = false;
			this.zedGraphControl.IsZoomOnMouseCenter = false;
			this.zedGraphControl.Location = new System.Drawing.Point(0, 0);
			this.zedGraphControl.Name = "zedGraphControl";
			this.zedGraphControl.PanButtons = System.Windows.Forms.MouseButtons.Left;
			this.zedGraphControl.PanButtons2 = System.Windows.Forms.MouseButtons.Middle;
			this.zedGraphControl.PanModifierKeys = System.Windows.Forms.Keys.Shift;
			this.zedGraphControl.PanModifierKeys2 = System.Windows.Forms.Keys.None;
			this.zedGraphControl.PointDateFormat = "g";
			this.zedGraphControl.PointValueFormat = "G";
			this.zedGraphControl.ScrollMaxX = 0;
			this.zedGraphControl.ScrollMaxY = 0;
			this.zedGraphControl.ScrollMaxY2 = 0;
			this.zedGraphControl.ScrollMinX = 0;
			this.zedGraphControl.ScrollMinY = 0;
			this.zedGraphControl.ScrollMinY2 = 0;
			this.zedGraphControl.Size = new System.Drawing.Size(848, 502);
			this.zedGraphControl.TabIndex = 0;
			this.zedGraphControl.ZoomButtons = System.Windows.Forms.MouseButtons.Left;
			this.zedGraphControl.ZoomButtons2 = System.Windows.Forms.MouseButtons.None;
			this.zedGraphControl.ZoomModifierKeys = System.Windows.Forms.Keys.None;
			this.zedGraphControl.ZoomModifierKeys2 = System.Windows.Forms.Keys.None;
			this.zedGraphControl.ZoomStepFraction = 0.1;
			// 
			// frmAircraftMovementsperHourForPVG
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(848, 758);
			this.Controls.Add(this.panel_Result);
			this.Controls.Add(this.lblResults);
			this.Controls.Add(this.panelTop);
			this.Name = "frmAircraftMovementsperHourForPVG";
			this.Text = "Aircraft Movements per Hour";
			this.Load += new System.EventHandler(this.frmAircraftMovementsperHourForPVG_Load);
			this.panelTop.ResumeLayout(false);
			this.panel_Result.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// Load the form. Prepare and init all controls.
		/// </summary>
		/// <param name="sender">Form.</param>
		/// <param name="e">Event arguments.</param>
		private void frmAircraftMovementsperHourForPVG_Load(object sender, System.EventArgs e)
		{
			InitTimePickers();
			btnStaStd.Checked = true;
			zedGraphControl.Visible =true;
		}

		/// <summary>
		/// initializes the From/to time control with the current day in
		/// the custom format "dd.MM.yyyy - HH:mm".
		/// </summary>
		private void InitTimePickers()
		{
		}

		/// <summary>
		/// Loads and prepare the data necessary for the filter. 
		/// In this case loads the nature codes and puts the
		/// data into the combobox.
		/// </summary>
		private void PrepareFilterData()
		{
		}
		/// <summary>
		/// Just start the print proview.
		/// </summary>
		private void RunReport()
		{
			this.Cursor = Cursors.WaitCursor;
			//PrepareReportData();
			

			rptZedGraph rpt = new rptZedGraph();
			rpt.YArray = NumberOfMovements;
			rpt.NumberOfHours = iHours;
			rpt.datStart = this.ucView1.Viewer.PeriodFrom;

			if(strUserSelection.Equals("StaStd")) rpt.MainHeader = "Aircraft Movements per Hour with STA/STD";
			if(strUserSelection.Equals("OnblOfbl")) rpt.MainHeader = "Aircraft Movements per Hour with ONBL/OFBL";
			if(strUserSelection.Equals("LandAirb")) rpt.MainHeader = "Aircraft Movements per Hour with LAND/AIRB";

			if(UT.IsTimeInUtc == true)
				strLOCAL_UTC = "UTC TIMES";
			else
				strLOCAL_UTC = "LOCAL TIMES";

			rpt.SubHeader = "From: " + this.ucView1.Viewer.PeriodFrom.ToString("dd.MM.yyyy") + " to: " + this.ucView1.Viewer.PeriodTo.ToString("dd.MM.yyyy");
			rpt.SubHeader += " View: " + this.ucView1.Viewer.CurrentView;
			rpt.SubHeader += " (Flights: " + FlightsTotal + " / ARR: " + FlightsArrival + " /DEP: " + FlightsDeparture + ")-" + strLOCAL_UTC;
			if(UT.IsTimeInUtc == true)
				strLOCAL_UTC = "UTC TIMES";
			else
				strLOCAL_UTC = "LOCAL TIMES";

			frmPrintPreview frm = new frmPrintPreview(rpt);
			frm.ShowDialog();
			this.Cursor = Cursors.Arrow;
		}
		/// <summary>
		/// Prints the Report
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnPrint_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			LoadReportData();
			//PrepareReportData();
			//RunReport();
			this.Cursor = Cursors.Arrow;

			/// Added by to show the graph*********************************************************************
			myGraph = zedGraphControl;//((ZedGraph.ZedGraphControl)this.zGraph.Control);
			//double [] YArray = yArray;
			ZedGraph.GraphPane myPane = myGraph.GraphPane;
			myPane.CurveList.Clear();
			myPane.GraphItemList.Clear();
			
			//Graphics g = myPane.FontSpec.MeasureString(


			// Set the titles and axis labels
			//myPane.Title = graphHeader;
			myPane.Title = "Aircraft Movements per Hour";
			myPane.MarginLeft = 0.5f;
			myPane.MarginRight = 0f;
			//myPane.XAxis.Title = xAxisHeader;
			myPane.XAxis.TitleFontSpec.Size = 10f;
			myPane.XAxis.Title = "Time (Hours)";
			myPane.YAxis.ScaleFontSpec.Size = 8f;
			myPane.YAxis.Title = "Number of Aircrafts ";
			myPane.YAxis.ScaleFontSpec.Size = 8f;

			string [] str;
			//			TimeSpan ts = datEnd - datStart;
			DateTime datCurrent = this.ucView1.Viewer.PeriodFrom;
			//
			//			int ilHours = Convert.ToInt32(ts.TotalMinutes/60);
			int ilLoopCnt = 0;
			str = new string[iHours];
			while(ilLoopCnt < iHours)//ilHours)
			{
				str[ilLoopCnt] = datCurrent.Hour.ToString();
				ilLoopCnt++;
				datCurrent = datCurrent.AddHours(1);
			}

			// Make up some random data points
			//double[] y = { 80, 70, 65, 78, 40, 80, 70, 65, 78, 40 , 80, 70, 65, 78, 40 ,80, 70, 65, 178, 40 ,80, 70, 65, 78 };
			//			string[] str = {"00:00", "01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00", "10:00"
			//							   , "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00"
			//							   , "21:00", "22:00", "23:00"};
			// Add a bar to the graph
			BarItem myCurve = myPane.AddBar( "Curve 1", null, NumberOfMovements, Color.White );
			// Access a image from the resources
			//Image image = Picture1.Image;//Bitmap.FromStream(GetType().Assembly.GetManifestResourceStream("ZedGraph.Demo.ngc4414.jpg") );
			// create a brush with the image
			//TextureBrush brush = new TextureBrush( image);
			// use the image for the bar fill
			//myCurve.Bar.Fill = new Fill( Brushes.Blue  );
			myCurve.Bar.Fill = new Fill( Color.Blue, Color.White, Color.Blue, 0F );
			// turn off the bar border
			myCurve.Bar.Border.IsVisible = false;

			
			// Draw the X tics between the labels instead of at the labels
			myPane.XAxis.IsTicsBetweenLabels = true;
			myPane.XAxis.ScaleFontSpec.Size = 8f;

			// Set the XAxis labels
			myPane.XAxis.TextLabels = str;

			// Set the XAxis to Text type
			myPane.XAxis.Type = AxisType.Text;

			// Fill the axis background with a color gradient
			myPane.AxisFill = new Fill( Color.White, Color.LightGray, 45.0f );

			// disable the legend
			myPane.Legend.IsVisible = false;
			
			myPane.AxisChange( myGraph.CreateGraphics());

			//if more than 3 days should be shown in the report than the values will not be written to the bars
			//because the valuse can no longer be read (oferlapping texts)
			if(iHours <= 72)
			{
				// The ValueHandler is a helper that does some position calculations for us.
				ValueHandler valueHandler = new ValueHandler( myPane, true );
				// Display a value for the maximum of each bar cluster
				// Shift the text items by x user scale units above the bars
				//const float shift = 0.5f;
				float shift = (3/100*MaxNumberOfMovements)*MaxNumberOfMovements;
				int ord = 0;
				foreach ( CurveItem curve in myPane.CurveList )
				{
					BarItem bar = curve as BarItem;

					if ( bar != null )
					{
						PointPairList points = curve.Points;

						for ( int i=0; i<points.Count; i++ )
						{
							double xVal = points[i].X ;//- 0.3d;

							// Calculate the Y value at the center of each bar
							//double yVal = valueHandler.BarCenterValue( curve, curve.GetBarWidth( myPane ),i, points[i].Y, ord );
							double yVal = points[i].Y;

							// format the label string to have 1 decimal place
							//string lab = xVal.ToString( "F0" );
							string lab = yVal.ToString();
				
							// don't show value "0" in the chart
							if(yVal != 0)
							{
										
								// create the text item (assumes the x axis is ordinal or text)
								// for negative bars, the label appears just above the zero value
				
								TextItem text = new TextItem( lab,(float) xVal ,(float) yVal + ( yVal >= 0 ? shift : -shift ));

								// tell Zedgraph to use user scale units for locating the TextItem
								text.Location.CoordinateFrame = CoordType.AxisXYScale;
								//text.Location.CoordinateFrame = CoordType.AxisXY2Scale;
								text.FontSpec.Size = 6;
								// AlignH the left-center of the text to the specified point

								text.Location.AlignH =  AlignH.Center;
								text.Location.AlignV =  yVal > 0 ? AlignV.Bottom : AlignV.Top;
								text.FontSpec.Border.IsVisible = false;
								// rotate the text 90 degrees
								text.FontSpec.Angle = 0;
								text.FontSpec.Fill.IsVisible = false;
								// add the TextItem to the list
								myPane.GraphItemList.Add( text );
							}
						}
					}

					ord++;
				}
			}
			zedGraphControl.Show();
			zedGraphControl.Refresh();
 
			
			/// ***********************************************************************************************
			
			
			
			rptZedGraph rpt = new rptZedGraph();
			rpt.YArray = NumberOfMovements;
			rpt.NumberOfHours = iHours;
			rpt.datStart = this.ucView1.Viewer.PeriodFrom;
			rpt.MaximumNumberOfMovements = MaxNumberOfMovements;

			if(strUserSelection.Equals("StaStd")) rpt.MainHeader = "Aircraft Movements Per Hour with STA/STD";
			if(strUserSelection.Equals("OnblOfbl")) rpt.MainHeader = "Aircraft Movements Per Hour with ONBL/OFBL";
			if(strUserSelection.Equals("LandAirb")) rpt.MainHeader = "Aircraft Movements Per Hour with LAND/AIRB";

			if(UT.IsTimeInUtc == true)
				strLOCAL_UTC = "UTC TIMES";
			else
				strLOCAL_UTC = "LOCAL TIMES";

			rpt.SubHeader = "From: " + this.ucView1.Viewer.PeriodFrom.ToString("dd.MM.yyyy") + " to: " + this.ucView1.Viewer.PeriodTo.ToString("dd.MM.yyyy") + " (Flights: " + FlightsTotal + " / ARR: " + FlightsArrival + " /DEP: " + FlightsDeparture + ")-" + strLOCAL_UTC;
			if(UT.IsTimeInUtc == true)
				strLOCAL_UTC = "UTC TIMES";
			else
				strLOCAL_UTC = "LOCAL TIMES";

			zedGraphControl.Show();
			zedGraphControl.Refresh();
			frmPrintPreview frm = new frmPrintPreview(rpt);
			frm.ShowDialog();
		}

		private void btnStaStd_CheckedChanged(object sender, System.EventArgs e)
		{
			if(btnStaStd.Checked == true)
			{
				strUserSelection = "StaStd";
				strWhereRawA = "(STOA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B')))";
				strWhereRawD = "(STOD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN ('D','B')))";
				strArrivalTimeKey = "STOA";
				strDepartureTimeKey = "STOD";
				strLogicalFields = "URNO,FLNO,ADID,ORG3,DES3,STOA,STOD";
				strTimeFields = "STOA,STOD";
			}
		}

		private void btnOnblOfbl_CheckedChanged(object sender, System.EventArgs e)
		{
			if (btnOnblOfbl.Checked == true)
			{
				strUserSelection = "OnblOfbl";
				strWhereRawA = "(TIFA BETWEEN '@@FROM' AND '@@TO' AND ONBL BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B')))";
				strWhereRawD = "(TIFD BETWEEN '@@FROM' AND '@@TO' AND OFBL BETWEEN '@@FROM' AND '@@TO' AND (ADID IN ('D','B')))";
				strArrivalTimeKey = "ONBL";
				strDepartureTimeKey = "OFBL";
				strLogicalFields = "URNO,FLNO,ADID,ORG3,DES3,ONBL,OFBL";
				strTimeFields = "ONBL,OFBL";
			}
		}

		private void btnLandAirb_CheckedChanged(object sender, System.EventArgs e)
		{
			if (btnLandAirb.Checked == true)
			{
				strUserSelection = "LandAirb";
				strWhereRawA = "(TIFA BETWEEN '@@FROM' AND '@@TO' AND LAND BETWEEN '@@FROM' AND (ADID IN ('A','B')))";
				strWhereRawD = "(TIFD BETWEEN '@@FROM' AND '@@TO' AND AIRB BETWEEN '@@FROM' AND (ADID IN ('D','B')))";

				strArrivalTimeKey = "LAND";
				strDepartureTimeKey = "AIRB";
				strLogicalFields = "URNO,FLNO,ADID,ORG3,DES3,LAND,AIRB";
				strTimeFields = "LAND,AIRB";
			}
		}


		/// <summary>
		/// Validates the user entry for nonsense.
		/// </summary>
		/// <returns></returns>
		private string ValidateUserEntry()
		{
			string strRet = "";
			int ilErrorCount = 1;

			if (this.ucView1.Viewer.CurrentView == "")
			{
				strRet += ilErrorCount.ToString() +  ". A view must be selected!\n";
				ilErrorCount++;
			}

			if (this.ucView1.Viewer.PeriodFrom >= this.ucView1.Viewer.PeriodTo)
			{
				strRet += ilErrorCount.ToString() +  ". Date From is later than Date To!\n";
				ilErrorCount++;
			}

			if (this.ucView1.Viewer.Arrival == false || this.ucView1.Viewer.Departure == false)
			{
				strRet += ilErrorCount.ToString() +  ". Both Arrival and Departure must be selected!\n";
				ilErrorCount++;
			}

			if (this.ucView1.Viewer.Rotation == true)
			{
				strRet += ilErrorCount.ToString() +  ". Rotation must not be selected!\n";
				ilErrorCount++;
			}

			return strRet;
		}

		/// <summary>
		/// Loads all necessary tables with filter criteria from the 
		/// database ==> to memoryDB.
		/// Data will be loaded day by day and for every day by arrival DES3=HOPO and then by departuere ORG3=HOPO
		/// This assures that with one loop throug the data loaded in every step the required data can be calculated
		/// </summary>
		private void LoadReportData()
		{

			FlightsArrival = 0;
			FlightsDeparture = 0;
			FlightsTotal = 0;
			string strTmpAFTWhere = "";
			//Clear the tab
			//tabResult.ResetContent();

			// In the first step change the times to UTC if this is
			// necessary.
			DateTime datFrom;
			DateTime datTo;

			string strDateFrom = "";
			string strDateTo = "";
			datFrom = this.ucView1.Viewer.PeriodFrom;
			datTo   = this.ucView1.Viewer.PeriodTo;
//
			TimeSpan ts = datTo - datFrom;
			iHours = Convert.ToInt32(ts.TotalHours);
			NumberOfMovements = new double[iHours];
			for (int i = 0 ; i < NumberOfMovements.Length; i++)
			{
				NumberOfMovements[i] = 0;
			}
//
			if(UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(this.ucView1.Viewer.PeriodFrom);
				datTo   = UT.LocalToUtc(this.ucView1.Viewer.PeriodTo);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
			}
			else
			{
				strDateFrom = UT.DateTimeToCeda(this.ucView1.Viewer.PeriodFrom);
				strDateTo = UT.DateTimeToCeda(this.ucView1.Viewer.PeriodTo);
			}

			//Load the data from AFTTAB
			myDB.Unbind("AFT");
			myAFT = myDB.Bind("AFT", "AFT", strLogicalFields, 
				"10,12,2,3,3,14,14", 
				strLogicalFields);
			myAFT.TimeFields = strTimeFields;
			myAFT.Clear();
			myAFT.TimeFieldsInitiallyInUtc = true;
			//Prepare loop reading day by day
			DateTime datReadFrom = datFrom;
			DateTime datReadTo;
			TimeSpan tsDays = (datTo - datFrom);
			progressBar1.Show();
			progressBar1.Value = 0;
			int ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) ilTotal = 1;
			lblProgress.Text = "Loading Flight Data";
			lblProgress.Refresh();
			progressBar1.Show();
			int loopCnt = 1;
			DateTime datTMP;
			do
			{
				int percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
				datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
				if( datReadTo > datTo) datReadTo = datTo;
				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);
				//patch the where statement according to the user's entry for Arrival-Flights
				strTmpAFTWhere = "WHERE ";

				// check on code share enabled
				if (this.ucView1.Viewer.IncludeCodeShare)
				{
					string strTmpWhere1 = "";
					strTmpWhere1 = strWhereRawA;
					strTmpWhere1+= this.ucView1.Viewer.AdditionalSelection(0);
					strTmpWhere1+= this.ucView1.Viewer.GeneralFilter;

					string strTmpWhere2 = "";
					//patch the where statement according to the user's entry
					strTmpWhere2+= this.ucView1.Viewer.AdditionalSelection(1);

					strTmpAFTWhere = string.Format("WHERE ({0}) AND ({1})",strTmpWhere1,strTmpWhere2);
				}
				else
				{
					//patch the where statement according to the user's entry
					strTmpAFTWhere += strWhereRawA;
					strTmpAFTWhere+= this.ucView1.Viewer.AdditionalSelection(0);
					strTmpAFTWhere+= this.ucView1.Viewer.GeneralFilter;
				}


				strTmpAFTWhere = strTmpAFTWhere.Replace("@@FROM", strDateFrom);
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@TO", strDateTo );
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@HOPO", UT.Hopo);
				//Load the data from AFTTAB for Arrival-Flights
				myAFT.Load(strTmpAFTWhere);
				myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
				for(int i = 0; i < myAFT.Count; i++)
				{
					datTMP = UT.CedaDateToDateTime( myAFT[i][strArrivalTimeKey]);
					NumberOfMovements[datTMP.Hour+(24*(loopCnt-1))]++;
					FlightsArrival++;
				}
				myAFT.Clear();
				strTmpAFTWhere = "WHERE ";

				// check on code share enabled
				if (this.ucView1.Viewer.IncludeCodeShare)
				{
					string strTmpWhere1 = "";
					//patch the where statement according to the user's entry
					strTmpWhere1 = strWhereRawD;
					strTmpWhere1+= this.ucView1.Viewer.AdditionalSelection(0);
					strTmpWhere1+= this.ucView1.Viewer.GeneralFilter;

					string strTmpWhere2 = "";
					//patch the where statement according to the user's entry
					strTmpWhere2+= this.ucView1.Viewer.AdditionalSelection(1);

					strTmpAFTWhere = string.Format("WHERE ({0}) AND ({1})",strTmpWhere1,strTmpWhere2);
				}
				else
				{
					//patch the where statement according to the user's entry
					strTmpAFTWhere += strWhereRawD;
					strTmpAFTWhere+= this.ucView1.Viewer.AdditionalSelection(0);
					strTmpAFTWhere+= this.ucView1.Viewer.GeneralFilter;

				}

				strTmpAFTWhere = strTmpAFTWhere.Replace("@@FROM", strDateFrom);
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@TO", strDateTo );
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@HOPO", UT.Hopo);

				//Load the data from AFTTAB for Arrival-Flights
				myAFT.Load(strTmpAFTWhere);
				myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
				for(int i = 0; i < myAFT.Count; i++)
				{
					datTMP = UT.CedaDateToDateTime( myAFT[i][strDepartureTimeKey]);
					NumberOfMovements[datTMP.Hour+(24*(loopCnt-1))]++;
					FlightsDeparture++;
				}
				myAFT.Clear();
				datReadFrom += new TimeSpan(1, 0, 0, 0, 0);
				loopCnt++;
			}while(datReadFrom <= datReadTo);
					
			lblProgress.Text  = "";
			progressBar1.Value = 0;
			progressBar1.Visible = false;
			//			progressBar1.Hide();
			FlightsTotal = FlightsArrival + FlightsDeparture;
			MaxNumberOfMovements = 0;
			int help = 0;
			for (int i = 0; i < NumberOfMovements.Length; i++)
			{
				if(NumberOfMovements[i] > MaxNumberOfMovements) MaxNumberOfMovements = Convert.ToInt32(NumberOfMovements[i]);

			}
			

		}

		private void btnLoadData_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			LoadReportData();
			//PrepareReportData();
			//RunReport();
			this.Cursor = Cursors.Arrow;


			
			myGraph = zedGraphControl;//((ZedGraph.ZedGraphControl)this.zGraph.Control);
			//double [] YArray = yArray;
			ZedGraph.GraphPane myPane = myGraph.GraphPane;
			myPane.CurveList.Clear();
			myPane.GraphItemList.Clear();
			
			//Graphics g = myPane.FontSpec.MeasureString(


			// Set the titles and axis labels
			//myPane.Title = graphHeader;
			myPane.Title = "Aircraft Movements per Hour";
			myPane.MarginLeft = 0.5f;
			myPane.MarginRight = 0f;
			//myPane.XAxis.Title = xAxisHeader;
			myPane.XAxis.TitleFontSpec.Size = 10f;
			myPane.XAxis.Title = "Time (Hours)";
			myPane.YAxis.ScaleFontSpec.Size = 8f;
			myPane.YAxis.Title = "Number of Aircrafts";
			myPane.YAxis.ScaleFontSpec.Size = 8f;

			string [] str;
			//			TimeSpan ts = datEnd - datStart;
			DateTime datCurrent = this.ucView1.Viewer.PeriodFrom;
			//
			//			int ilHours = Convert.ToInt32(ts.TotalMinutes/60);
			int ilLoopCnt = 0;
			str = new string[iHours];
			while(ilLoopCnt < iHours)//ilHours)
			{
				str[ilLoopCnt] = datCurrent.Hour.ToString();
				ilLoopCnt++;
				datCurrent = datCurrent.AddHours(1);
			}

			// Make up some random data points
			//double[] y = { 80, 70, 65, 78, 40, 80, 70, 65, 78, 40 , 80, 70, 65, 78, 40 ,80, 70, 65, 178, 40 ,80, 70, 65, 78 };
			//			string[] str = {"00:00", "01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00", "10:00"
			//							   , "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00"
			//							   , "21:00", "22:00", "23:00"};
			// Add a bar to the graph
			BarItem myCurve = myPane.AddBar( "Curve 1", null, NumberOfMovements, Color.White );
			// Access a image from the resources
			//Image image = Picture1.Image;//Bitmap.FromStream(GetType().Assembly.GetManifestResourceStream("ZedGraph.Demo.ngc4414.jpg") );
			// create a brush with the image
			//TextureBrush brush = new TextureBrush( image);
			// use the image for the bar fill
			//myCurve.Bar.Fill = new Fill( Brushes.Blue  );
			myCurve.Bar.Fill = new Fill( Color.Blue, Color.White, Color.Blue, 0F );
			// turn off the bar border
			myCurve.Bar.Border.IsVisible = false;

			
			// Draw the X tics between the labels instead of at the labels
			myPane.XAxis.IsTicsBetweenLabels = true;
			myPane.XAxis.ScaleFontSpec.Size = 8f;

			// Set the XAxis labels
			myPane.XAxis.TextLabels = str;

			// Set the XAxis to Text type
			myPane.XAxis.Type = AxisType.Text;

			// Fill the axis background with a color gradient
			myPane.AxisFill = new Fill( Color.White, Color.LightGray, 45.0f );

			// disable the legend
			myPane.Legend.IsVisible = false;
			
			myPane.AxisChange( myGraph.CreateGraphics());

			//if more than 3 days should be shown in the report than the values will not be written to the bars
			//because the valuse can no longer be read (oferlapping texts)
			if(iHours <= 72)
			{
				// The ValueHandler is a helper that does some position calculations for us.
				ValueHandler valueHandler = new ValueHandler( myPane, true );
				// Display a value for the maximum of each bar cluster
				// Shift the text items by x user scale units above the bars
				//const float shift = 0.5f;
				float shift = (3/100*MaxNumberOfMovements)*MaxNumberOfMovements;
				int ord = 0;
				foreach ( CurveItem curve in myPane.CurveList )
				{
					BarItem bar = curve as BarItem;

					if ( bar != null )
					{
						PointPairList points = curve.Points;

						for ( int i=0; i<points.Count; i++ )
						{
							double xVal = points[i].X ;//- 0.3d;

							// Calculate the Y value at the center of each bar
							//double yVal = valueHandler.BarCenterValue( curve, curve.GetBarWidth( myPane ),i, points[i].Y, ord );
							double yVal = points[i].Y;

							// format the label string to have 1 decimal place
							//string lab = xVal.ToString( "F0" );
							string lab = yVal.ToString();
				
							// don't show value "0" in the chart
							if(yVal != 0)
							{
										
								// create the text item (assumes the x axis is ordinal or text)
								// for negative bars, the label appears just above the zero value
				
								TextItem text = new TextItem( lab,(float) xVal ,(float) yVal + ( yVal >= 0 ? shift : -shift ));

								// tell Zedgraph to use user scale units for locating the TextItem
								text.Location.CoordinateFrame = CoordType.AxisXYScale;
								//text.Location.CoordinateFrame = CoordType.AxisXY2Scale;
								text.FontSpec.Size = 6;
								// AlignH the left-center of the text to the specified point

								text.Location.AlignH =  AlignH.Center;
								text.Location.AlignV =  yVal > 0 ? AlignV.Bottom : AlignV.Top;
								text.FontSpec.Border.IsVisible = false;
								// rotate the text 90 degrees
								text.FontSpec.Angle = 0;
								text.FontSpec.Fill.IsVisible = false;
								// add the TextItem to the list
								myPane.GraphItemList.Add( text );
							}
						}
					}

					ord++;
				}
			}
			zedGraphControl.Show();
			zedGraphControl.Refresh();
		}

		private void btnPrintPreview_Click(object sender, System.EventArgs e)
		{
			RunReport();		
		}

		private void panelTop_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
		
		}

		private void ucView1_viewEvent(string view)
		{
			btnLoadData_Click(this,null);
		}

	}
}
