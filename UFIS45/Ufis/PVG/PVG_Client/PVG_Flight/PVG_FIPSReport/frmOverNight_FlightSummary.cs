using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Ufis.Utils;
using Ufis.Data;

namespace FIPS_Reports
{
	/// <summary>
	/// Summary description for frmOverNight_FlightSummary.
	/// </summary>
	public class frmOverNight_FlightSummary : System.Windows.Forms.Form
	{
		#region _My Members

		/// <summary>
		/// The where statements: all "@@xx" fields will be replaced
		/// with the true values according to the user's entry.
		/// </summary>
		
		//		private string strWhereRaw = "WHERE (((STOA BETWEEN '@@FROM' AND '@@TO' AND DES3='@@HOPO' ) OR ( STOD BETWEEN '@@FROM' AND '@@TO' AND ORG3='@@HOPO')) AND (FTYP='O' OR FTYP='S' OR FTYP='X')) AND STAT<>'DEL'";
		//		private string strWhereRaw = "WHERE (((STOA BETWEEN '@@FROM' AND '@@TO') AND (STOD BETWEEN '@@FROM' AND '@@TO'))) AND (FTYP='O' OR FTYP='S' OR FTYP='X') AND STAT<>'DEL' " ;
		private string strWhereRaw = "WHERE (((STOA BETWEEN '@@FROM' AND '@@TO') OR (STOD BETWEEN '@@FROM' AND '@@TO')) OR " +
			"((TIFA BETWEEN '@@FROM' AND '@@TO') OR (TIFD BETWEEN '@@FROM' AND '@@TO'))) AND (FTYP NOT IN ('T','G','N','X')) AND STAT<>'DEL'" ;
		private string strNatWhere			= " AND (TTYP='@@NAT')";
		private string strORGDESWhere	= " AND (ORG3='@@APT' OR DES3='@@APT')";
		/// <summary>
		/// The logical field names of the tab control for internal 
		/// development purposes
		/// </summary>
		private string stractLogicalFields = "ARRIVAL,STA,COUNT";
		/// <summary>
		/// The logical field names of the tab control for internal 
		/// development purposes ==> for Rotation
		/// </summary>
		
		//DB-Fields
		// 10 , 10 , 9  ,  8 , 1  , 1  , 14 , 1  ,  5 , 1  , 2  , 3  , 4  , 3  , 4  , 2  , 5  , 5  , 5  , 5  , 5  , 1  , 3  , 14 , 1  , 3  , 4  , 5  ,  5 , 5  , 5  , 5
		//RKEY,URNO,FLNO,CSGN,FLTI,JCNT,STOA,DOOA,TTYP,STEV,STYP,ORG3,ORG4,VIA3,VIA4,VIAN,PSTA,GTA1,GTA2,BLT1,BLT2,ISRE,ACT3,STOD,DOOD,DES3,DES4,PSTD,GTD1,GTD2,CKIF,CKIT"
		private string strLogicalFieldsRotation = "ALC2,Arrival_A,ID_A,J_A,Sched_A,Departure_D,ID_D,J_D,STD_D";
		/// <summary>
		/// The header columns, which must be used for the report output
		/// </summary>
		/// <summary>
		private string stractTabHeader =     "Airlines,  Date  ,Overnight flights";
		
		/// The header columns, which must be used for the report output
		/// for rotation
		/// </summary>
		private string strTabHeaderRotation = "Airlines,ADid,Arrival,STA,Departure,STD";
		/// <summary>
		/// The lengths, which must be used for the report's column widths
		/// </summary>
		private string stractTabHeaderLens = "60,80,100";
		/// <summary>
		/// The lengths, which must be used for the report's column widths
		/// for rotation
		/// </summary>
		private string strTabHeaderLensRotation = "40,20,50,60,50,60";
		/// <summary>
		/// The one and only IDatabase obejct.
		/// </summary>
		private IDatabase myDB;
		/// <summary>
		/// ITable object for the AFTTAB
		/// </summary>
		private ITable myAFT;
		/// <summary>
		/// ITable object for the nature codes of NATTAB
		/// </summary>
		private ITable myNAT;
		/// <summary>
		/// The nature code as global valid member.
		/// </summary>
		private string strNatureCode = "";
		/// <summary>
		/// Airline 2-3 letter code
		/// </summary>
		private string strAirportCode = "";
		/// <summary>
		/// Different variables to be used for counts
		/// </summary>
		private int iCount = 0;
		private int iarrv=0;
		private int iprevcount=0;
		private int iTotal=0;
		private string prevAirline;
		private string currAirline;
		private string currDate;
		private string	prevDate;
		private double [] NumberofFlights;
		StringBuilder sb = new StringBuilder(10000);
		private string strValues;
		private string strEmptyLine = ",,";
		//End here
		
		#endregion _My Members

		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.Panel panelTab;
		private AxTABLib.AxTAB tabResult;
		private System.Windows.Forms.Label lblResults;
		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.TextBox txtAirport;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Button btnLoadPrint;
		private System.Windows.Forms.Button btnPrintPreview;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.DateTimePicker dtTo;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.DateTimePicker dtFrom;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox cbNatures;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.DateTimePicker dtDuration;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox txtLens;
		private System.Windows.Forms.Button button1;
		private AxTABLib.AxTAB actualtabResults;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmOverNight_FlightSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmOverNight_FlightSummary));
			this.panelBody = new System.Windows.Forms.Panel();
			this.panelTab = new System.Windows.Forms.Panel();
			this.actualtabResults = new AxTABLib.AxTAB();
			this.tabResult = new AxTABLib.AxTAB();
			this.lblResults = new System.Windows.Forms.Label();
			this.panelTop = new System.Windows.Forms.Panel();
			this.button1 = new System.Windows.Forms.Button();
			this.txtLens = new System.Windows.Forms.TextBox();
			this.dtDuration = new System.Windows.Forms.DateTimePicker();
			this.label5 = new System.Windows.Forms.Label();
			this.txtAirport = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.lblProgress = new System.Windows.Forms.Label();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.btnLoadPrint = new System.Windows.Forms.Button();
			this.btnPrintPreview = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.cbNatures = new System.Windows.Forms.ComboBox();
			this.label3 = new System.Windows.Forms.Label();
			this.dtTo = new System.Windows.Forms.DateTimePicker();
			this.label2 = new System.Windows.Forms.Label();
			this.dtFrom = new System.Windows.Forms.DateTimePicker();
			this.label1 = new System.Windows.Forms.Label();
			this.panelBody.SuspendLayout();
			this.panelTab.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.actualtabResults)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
			this.panelTop.SuspendLayout();
			this.SuspendLayout();
			// 
			// panelBody
			// 
			this.panelBody.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelBody.Controls.Add(this.panelTab);
			this.panelBody.Controls.Add(this.lblResults);
			this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelBody.Location = new System.Drawing.Point(0, 172);
			this.panelBody.Name = "panelBody";
			this.panelBody.Size = new System.Drawing.Size(780, 350);
			this.panelBody.TabIndex = 3;
			// 
			// panelTab
			// 
			this.panelTab.Controls.Add(this.actualtabResults);
			this.panelTab.Controls.Add(this.tabResult);
			this.panelTab.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelTab.Location = new System.Drawing.Point(0, 16);
			this.panelTab.Name = "panelTab";
			this.panelTab.Size = new System.Drawing.Size(780, 334);
			this.panelTab.TabIndex = 2;
			// 
			// actualtabResults
			// 
			this.actualtabResults.ContainingControl = this;
			this.actualtabResults.Dock = System.Windows.Forms.DockStyle.Fill;
			this.actualtabResults.Location = new System.Drawing.Point(0, 0);
			this.actualtabResults.Name = "actualtabResults";
			this.actualtabResults.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("actualtabResults.OcxState")));
			this.actualtabResults.Size = new System.Drawing.Size(780, 334);
			this.actualtabResults.TabIndex = 1;
			this.actualtabResults.SendLButtonClick += new AxTABLib._DTABEvents_SendLButtonClickEventHandler(this.actualtabResults_SendLButtonClick);
			// 
			// tabResult
			// 
			this.tabResult.ContainingControl = this;
			this.tabResult.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabResult.Location = new System.Drawing.Point(0, 0);
			this.tabResult.Name = "tabResult";
			this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
			this.tabResult.Size = new System.Drawing.Size(780, 334);
			this.tabResult.TabIndex = 0;
			this.tabResult.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabResult_SendLButtonDblClick);
			// 
			// lblResults
			// 
			this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lblResults.Location = new System.Drawing.Point(0, 0);
			this.lblResults.Name = "lblResults";
			this.lblResults.Size = new System.Drawing.Size(780, 16);
			this.lblResults.TabIndex = 1;
			this.lblResults.Text = "Report Results";
			this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// panelTop
			// 
			this.panelTop.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelTop.Controls.Add(this.button1);
			this.panelTop.Controls.Add(this.txtLens);
			this.panelTop.Controls.Add(this.dtDuration);
			this.panelTop.Controls.Add(this.label5);
			this.panelTop.Controls.Add(this.txtAirport);
			this.panelTop.Controls.Add(this.label4);
			this.panelTop.Controls.Add(this.lblProgress);
			this.panelTop.Controls.Add(this.progressBar1);
			this.panelTop.Controls.Add(this.btnLoadPrint);
			this.panelTop.Controls.Add(this.btnPrintPreview);
			this.panelTop.Controls.Add(this.btnCancel);
			this.panelTop.Controls.Add(this.btnOK);
			this.panelTop.Controls.Add(this.cbNatures);
			this.panelTop.Controls.Add(this.label3);
			this.panelTop.Controls.Add(this.dtTo);
			this.panelTop.Controls.Add(this.label2);
			this.panelTop.Controls.Add(this.dtFrom);
			this.panelTop.Controls.Add(this.label1);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(0, 0);
			this.panelTop.Name = "panelTop";
			this.panelTop.Size = new System.Drawing.Size(780, 172);
			this.panelTop.TabIndex = 2;
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(224)), ((System.Byte)(192)));
			this.button1.Location = new System.Drawing.Point(456, 12);
			this.button1.Name = "button1";
			this.button1.TabIndex = 20;
			this.button1.Text = "button1";
			this.button1.Visible = false;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// txtLens
			// 
			this.txtLens.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(224)), ((System.Byte)(192)));
			this.txtLens.Location = new System.Drawing.Point(276, 36);
			this.txtLens.Name = "txtLens";
			this.txtLens.Size = new System.Drawing.Size(488, 20);
			this.txtLens.TabIndex = 19;
			this.txtLens.Text = "textBox1";
			this.txtLens.Visible = false;
			// 
			// dtDuration
			// 
			this.dtDuration.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtDuration.Location = new System.Drawing.Point(80, 32);
			this.dtDuration.Name = "dtDuration";
			this.dtDuration.ShowUpDown = true;
			this.dtDuration.Size = new System.Drawing.Size(128, 20);
			this.dtDuration.TabIndex = 2;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(12, 36);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(64, 16);
			this.label5.TabIndex = 18;
			this.label5.Text = "Duration >=";
			// 
			// txtAirport
			// 
			this.txtAirport.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtAirport.Location = new System.Drawing.Point(80, 92);
			this.txtAirport.Name = "txtAirport";
			this.txtAirport.TabIndex = 5;
			this.txtAirport.Text = "";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(12, 96);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(64, 16);
			this.label4.TabIndex = 17;
			this.label4.Text = "Airport:";
			// 
			// lblProgress
			// 
			this.lblProgress.Location = new System.Drawing.Point(412, 116);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(212, 16);
			this.lblProgress.TabIndex = 12;
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(412, 132);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(216, 23);
			this.progressBar1.TabIndex = 10;
			this.progressBar1.Visible = false;
			// 
			// btnLoadPrint
			// 
			this.btnLoadPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnLoadPrint.Location = new System.Drawing.Point(244, 132);
			this.btnLoadPrint.Name = "btnLoadPrint";
			this.btnLoadPrint.TabIndex = 8;
			this.btnLoadPrint.Text = "Loa&d + Print";
			this.btnLoadPrint.Click += new System.EventHandler(this.btnLoadPrint_Click);
			// 
			// btnPrintPreview
			// 
			this.btnPrintPreview.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnPrintPreview.Location = new System.Drawing.Point(168, 132);
			this.btnPrintPreview.Name = "btnPrintPreview";
			this.btnPrintPreview.TabIndex = 7;
			this.btnPrintPreview.Text = "&Print Preview";
			this.btnPrintPreview.Click += new System.EventHandler(this.btnPrintPreview_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCancel.Location = new System.Drawing.Point(320, 132);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 9;
			this.btnCancel.Text = "&Close";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnOK
			// 
			this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnOK.Location = new System.Drawing.Point(92, 132);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 6;
			this.btnOK.Text = "&Load Data";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// cbNatures
			// 
			this.cbNatures.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbNatures.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cbNatures.ItemHeight = 16;
			this.cbNatures.Location = new System.Drawing.Point(80, 60);
			this.cbNatures.MaxDropDownItems = 16;
			this.cbNatures.Name = "cbNatures";
			this.cbNatures.Size = new System.Drawing.Size(288, 24);
			this.cbNatures.TabIndex = 4;
			// 
			// label3
			// 
			this.label3.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.label3.Location = new System.Drawing.Point(12, 64);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(64, 16);
			this.label3.TabIndex = 4;
			this.label3.Text = "Nature:";
			// 
			// dtTo
			// 
			this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtTo.Location = new System.Drawing.Point(240, 4);
			this.dtTo.Name = "dtTo";
			this.dtTo.Size = new System.Drawing.Size(128, 20);
			this.dtTo.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(212, 8);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(24, 16);
			this.label2.TabIndex = 2;
			this.label2.Text = "to:";
			// 
			// dtFrom
			// 
			this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtFrom.Location = new System.Drawing.Point(80, 4);
			this.dtFrom.Name = "dtFrom";
			this.dtFrom.Size = new System.Drawing.Size(128, 20);
			this.dtFrom.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(12, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Date from:";
			// 
			// frmOverNight_FlightSummary
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(780, 522);
			this.Controls.Add(this.panelBody);
			this.Controls.Add(this.panelTop);
			this.Name = "frmOverNight_FlightSummary";
			this.Text = "Statistics of overnight Arrival flights(summary)";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.frmOverNight_FlightSummary_Closing);
			this.Load += new System.EventHandler(this.frmOverNight_FlightSummary_Load);
			this.panelBody.ResumeLayout(false);
			this.panelTab.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.actualtabResults)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
			this.panelTop.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion
		
		/// <summary>
		/// Loads the from and inits the necessary things.
		/// </summary>
		/// <param name="sender">Originator of the method call</param>
		/// <param name="e">Event args</param>
		private void frmOverNight_FlightSummary_Load(object sender, System.EventArgs e)
		{
			myDB = UT.GetMemDB();
			InitTimePickers();
			InitTab();
			PrepareFilterData();
		}
		/// <summary>
		/// initializes the From/to time control with the current day in
		/// the custom format "dd.MM.yyyy - HH:mm".
		/// </summary>
		private void InitTimePickers()
		{
			DateTime olFrom;
			DateTime olTo;
			DateTime olDura;
			
			olFrom = DateTime.Now;
			olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 0,0,0);
			olTo   = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 23,59,0);
			olDura = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 3,0,0);
			dtFrom.Value = olFrom;
			dtTo.Value = olTo;
			dtDuration.Value = olDura;
			dtFrom.CustomFormat = "dd.MM.yyyy - HH:mm";
			dtTo.CustomFormat = "dd.MM.yyyy - HH:mm";
			dtDuration.CustomFormat = "HH:mm";
		}
		/// <summary>
		/// Initializes the tab control.
		/// </summary>
		private void InitTab()
		{
			tabResult.ResetContent();
			tabResult.ShowHorzScroller(true);
			tabResult.EnableHeaderSizing(true);
			tabResult.SetTabFontBold(true);
			tabResult.LifeStyle = true;
			tabResult.LineHeight = 16;
			tabResult.FontName = "Arial";
			tabResult.FontSize = 14;
			tabResult.HeaderFontSize = 14;
			tabResult.AutoSizeByHeader = true;
			tabResult.HeaderString = strTabHeaderRotation;
			tabResult.LogicalFieldList = strLogicalFieldsRotation;
			tabResult.HeaderLengthString = strTabHeaderLensRotation;

			actualtabResults.ResetContent();
			actualtabResults.ShowHorzScroller(true);
			actualtabResults.EnableHeaderSizing(true);
			actualtabResults.SetTabFontBold(true);
			actualtabResults.LifeStyle = true;
			actualtabResults.LineHeight = 16;
			actualtabResults.FontName = "Arial";
			actualtabResults.FontSize = 14;
			actualtabResults.HeaderFontSize = 14;
			actualtabResults.AutoSizeByHeader = true;
			actualtabResults.HeaderString = stractTabHeader;
			actualtabResults.LogicalFieldList = stractLogicalFields;
			actualtabResults.HeaderLengthString = stractTabHeaderLens;
			PrepareReportDataRotations();
		}
		/// <summary>
		/// Loads and prepare the data necessary for the filter. 
		/// In this case loads the nature codes and puts the
		/// data into the combobox.
		/// </summary>
		private void PrepareFilterData()
		{
			myNAT = myDB["NAT"];
			cbNatures.Items.Add("<None>");
			if(myNAT == null)
			{
				myNAT = myDB.Bind("NAT", "NAT", "TTYP,TNAM", "5,30", "TTYP,TNAM");
				myNAT.Load("ORDER BY TTYP");
			}
			else if(myNAT.Count == 0)
			{
				myNAT = myDB.Bind("NAT", "NAT", "TTYP,TNAM", "5,30", "TTYP,TNAM");
				myNAT.Load("ORDER BY TTYP");
			}
			for(int i = 0; i < myNAT.Count; i++)
			{
				string strValue = myNAT[i]["TTYP"] + " / " + myNAT[i]["TNAM"];
				cbNatures.Items.Add(strValue);
			}
			if(cbNatures.Items.Count > 0)
				cbNatures.SelectedIndex = 0;
		}

		/// <summary>
		/// Runs the data queries according to the reports needs and puts
		///	Calls the data prepare method and puts the prepared data into
		///	the tab control. After this is finished it calls the reports
		///	by sending the tab and the headers to the generic report.
		/// </summary>
		/// <param name="sender">Originator object of the call</param>
		/// <param name="e">Event arguments</param>
		private void btnOK_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			tabResult.ResetContent();
			actualtabResults.ResetContent();
			LoadReportData();
			PrepareReportCount();
			PrepareFillReportData();
		//	PrepareReportDataRotations();
			//RunReport();
			this.Cursor = Cursors.Arrow;
			txtLens.Text = tabResult.HeaderLengthString;
		}
		/// <summary>
		/// Validates the user entry for nonsense.
		/// </summary>
		/// <returns></returns>
		private string ValidateUserEntry()
		{
			string strRet = "";
			int ilErrorCount = 1;
			if(dtFrom.Value >= dtTo.Value)
			{
				strRet += ilErrorCount.ToString() +  ". Date From is later than Date To!\n";
				ilErrorCount++;
			}
			if(cbNatures.Text == "")
			{
				strRet += ilErrorCount.ToString() +  ". Please select a nature code!\n";
				ilErrorCount++;
			}
			return strRet;
		}
		/// <summary>
		/// Loads all necessary tables with filter criteria from the 
		/// database ==> to memoryDB.
		/// </summary>
		private void LoadReportData()
		{
			string strTmpWhere = strWhereRaw;
			//Clear the tab
			//tabResult.ResetContent();
			// In the first step change the times to UTC if this is
			// necessary.
			DateTime datFrom;
			DateTime datTo;
			string strDateFrom = "";
			string strDateTo = "";
			datFrom = dtFrom.Value;
			datTo   = dtTo.Value;
			if(UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(dtFrom.Value);
				datTo   = UT.LocalToUtc(dtTo.Value);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
			}
			else
			{
				strDateFrom = UT.DateTimeToCeda(dtFrom.Value);
				strDateTo = UT.DateTimeToCeda(dtTo.Value);
			}
			// Extract the true Nature code from the selected entry of the combobox
			strNatureCode = "";
			string [] strArr = cbNatures.Text.Split('/');
			strNatureCode = strArr[0].Trim();
			strAirportCode = txtAirport.Text;

			//Now reading day by day the flight records in order to 
			//not stress the database too much
			myDB.Unbind("AFT");
			// 10 , 10 , 9  ,  8 , 1  , 1  , 14 , 1  ,  5 , 1  , 2  , 3  , 4  , 3  , 4  , 2  , 5  , 5  , 5  , 5  , 5  , 1  , 3  , 14 , 1  , 3  , 4  , 5  ,  5 , 5  , 5  , 5
			//RKEY,URNO,FLNO,CSGN,FLTI,JCNT,STOA,DOOA,TTYP,STEV,STYP,ORG3,ORG4,VIA3,VIA4,VIAN,PSTA,GTA1,GTA2,BLT1,BLT2,ISRE,ACT3,STOD,DOOD,DES3,DES4,PSTD,GTD1,GTD2,CKIF,CKIT
			myAFT = myDB.Bind("AFT", "AFT", 
				"ALC2,RKEY,URNO,ADID,FLNO,CSGN,FLTI,JCNT,STOA,TIFA,DOOA,TTYP,STEV,STYP,ORG3,ORG4,VIA3,VIA4,VIAN,PSTA,GTA1,GTA2,BLT1,BLT2,ISRE,ACT3,STOD,TIFD,DOOD,DES3,DES4,PSTD,GTD1,GTD2,CKIF,CKIT", 
				"2,10,10,1,9,8,1,1,14,14,1,5,1,2,3,4,3,4,2,5,5,5,5,5,1,3,14,14,1,3,4,5,5,5,5,5",
				"ALC2,RKEY,URNO,ADID,FLNO,CSGN,FLTI,JCNT,STOA,TIFA,DOOA,TTYP,STEV,STYP,ORG3,ORG4,VIA3,VIA4,VIAN,PSTA,GTA1,GTA2,BLT1,BLT2,ISRE,ACT3,STOD,TIFD,DOOD,DES3,DES4,PSTD,GTD1,GTD2,CKIF,CKIT");
			myAFT.Clear();
			myAFT.TimeFields = "STOA,STOD,TIFA,TIFD,ONBL,OFBL";
			myAFT.Command("read",",GFR,");
			ArrayList myUniques = new ArrayList();
			myUniques.Add("URNO");
			myAFT.UniqueFields = myUniques;
			myAFT.TimeFieldsInitiallyInUtc = true;

			//Prepare loop reading day by day
			DateTime datReadFrom = datFrom;
			DateTime datReadTo;
			TimeSpan tsDays = (datTo - datFrom);
			progressBar1.Value = 0;
			int ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) ilTotal = 1;
			lblProgress.Text = "Loading Data";
			lblProgress.Refresh();
			progressBar1.Show();
			int loopCnt = 1;
			do
			{
				int percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
				datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
				if( datReadTo > datTo) datReadTo = datTo;
				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);
				//patch the where statement according to the user's entry
				strTmpWhere = strWhereRaw;
				if(strNatureCode != "<None>")
					strTmpWhere += strNatWhere;
				if(strAirportCode != "" )
					strTmpWhere += strORGDESWhere;
				strTmpWhere = strTmpWhere.Replace("@@FROM", strDateFrom);
				strTmpWhere = strTmpWhere.Replace("@@TO", strDateTo );
				strTmpWhere = strTmpWhere.Replace("@@NAT", strNatureCode );
				strTmpWhere = strTmpWhere.Replace("@@APT", strAirportCode);
				strTmpWhere += "[ROTATIONS]";
				//Load the data from AFTTAB
				myAFT.Load(strTmpWhere);
			//	myAFT.Sort("ALC2",true);
				NumberofFlights= new double[2];
				myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
				PrepareReportDataRotations();
					//	datTMP = UT.CedaDateToDateTime( myAFT[i][strDepartureTimeKey]);
	//			PrepareReportCount();
				tabResult.Visible = false;
				myAFT.Clear();
				//New function added
				datReadFrom += new TimeSpan(1, 0, 0, 0, 0);
				loopCnt++;
			}while(datReadFrom <= datReadTo);
			lblProgress.Text = "";
			progressBar1.Hide();

			//Filter out the FTYPs which are selected due to rotations
			for(int i = myAFT.Count-1; i >= 0; i--)
			{
				switch(myAFT[i]["FTYP"])
				{
					case "S":
					case "O":
					case "X":
						break;
					default:
						myAFT.Remove(i);
					break;
				}
			}


		//	myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
		}

		///<summary>
		///Test function to count 
		///PrepareReportCount
		///</summary>
		private void PrepareReportCount()
		{
			StringBuilder sb = new StringBuilder(10000);

			for(int k=0;k<tabResult.GetLineCount ();k++)
			{

				currAirline = 	tabResult .GetColumnValue(k,0);
				currDate = tabResult .GetColumnValue(k,3);
			//	From.ToLocalTime().Date.ToShortDateString();
			 
				if (k==0 && tabResult .GetColumnValue(k,1) == "A")
				{
					prevAirline = currAirline ;
					prevDate = currDate;
				}
				if (k==32)
				{
					tabResult.GetLineCount ();
				}
				if (prevAirline == currAirline)
				{
					//Start with the count of Arrival flights
					if (tabResult .GetColumnValue(k,1) == "A")
					{
						if (prevDate == currDate)
						{
							NumberofFlights[0]++;
							NumberofFlights[1]++;
						}
						if (prevDate != currDate)
						{
							strValues += prevAirline + "," + Helper.DateString(prevDate, "yyyyMMdd") +"," + NumberofFlights[0].ToString () +" \n" ;
							sb.Append(strValues);
							strValues = "";
							NumberofFlights[0] = 1;
							NumberofFlights[1]++;
							prevAirline=currAirline;
							prevDate = currDate;
						}

						if (k ==tabResult .GetLineCount() -1)
						{
							strValues += currAirline + "," + Helper.DateString(tabResult .GetColumnValue(k,3), "yyyyMMdd") +"," + NumberofFlights[0].ToString () +" \n" ;
							sb.Append(strValues);
							strValues = "";
						}
					}
				}
				if (prevAirline != currAirline)
				{
					strValues += prevAirline + "," + Helper.DateString(prevDate, "yyyyMMdd") +"," + NumberofFlights[0].ToString () +" \n" ;
					NumberofFlights[0] = 0;
					sb.Append(strValues);
					strValues = "";
					//Start with the count of Arrival flights
					if (tabResult .GetColumnValue(k,1) == "A")
					{
						NumberofFlights[0]++;
						NumberofFlights[1]++;
						prevAirline = currAirline;
						prevDate = currDate;
					
						if (k ==tabResult .GetLineCount() -1)
						{
							strValues += currAirline + "," + Helper.DateString(tabResult .GetColumnValue(k,3), "yyyyMMdd") +"," + NumberofFlights[0].ToString () +" \n" ;
							sb.Append(strValues);
							strValues = "";
						}
					}
				}
			}
				actualtabResults .InsertBuffer(sb.ToString(), "\n");
		}

/// <summary>
/// 
/// </summary>

		private void PrepareFillReportData()
		{
			lblProgress.Text = "";
			progressBar1.Visible = false;
			actualtabResults.Sort ("0",true,true);
			actualtabResults.Sort ("1",true,true);
			insertTotal();
			actualtabResults.Refresh();
			tabResult.Visible =false;
			actualtabResults.Visible =true;
			lblResults.Text = "Report Results: (" + actualtabResults.GetLineCount().ToString() + ")";
		}
		///<summary>
		///This function is written to insert Total at end
		///</summary>
		private void insertTotal()
		{
			StringBuilder sb1 = new StringBuilder(10000);
			string strValuesactTotal = "";
			strValuesactTotal  +=  "--" +","+"Total" +","+ NumberofFlights[1].ToString()+"\n" ;
			sb1.Append(strValuesactTotal);
			actualtabResults .InsertBuffer(sb1.ToString(), "\n");
			strValuesactTotal ="";
		}
		/// <summary>
		/// Prepare the loaded data according to the report's needs.
		/// First organize the rotations and then put the result
		/// into the tabResult
		/// </summary>
		private void PrepareReportDataRotations()
		{
			if(myAFT == null)
				return;

			myAFT.Sort("RKEY", true);
			int llCurr = 0;
			int ilStep = 1;
			string strCurrAdid = "";
			string strNextAdid = "";
			string strCurrRkey = "";
			string strNextRkey = "";
			string strTime = "";
			DateTime tmpDate;
			StringBuilder sb = new StringBuilder(10000);
			while(llCurr < myAFT.Count)
			{
				string strValues = "";
				strCurrRkey = myAFT[llCurr]["RKEY"];
				strCurrAdid = myAFT[llCurr]["ADID"];
				if ((llCurr + 1) < myAFT.Count)
				{
					strNextRkey = myAFT[llCurr+1]["RKEY"];
					strNextAdid = myAFT[llCurr+1]["ADID"];
				}
				else
				{
					strNextRkey = "";
					strNextAdid = "";
				}

				if(strCurrRkey == strNextRkey)
				{//Is a rotation
					ilStep = 2;
					if( strCurrAdid == "A" && strNextAdid == "D")
					{
						DateTime timeARR = UT.CedaFullDateToDateTime(myAFT[llCurr]["STOA"]);
						DateTime timeDEP = UT.CedaFullDateToDateTime(myAFT[llCurr+1]["STOD"]);
						strTime = myAFT[llCurr]["TIFA"];
						if(strTime != "")
							timeARR = UT.CedaFullDateToDateTime(strTime);
						strTime = myAFT[llCurr+1]["TIFD"];
						if(strTime != "")
							timeDEP = UT.CedaFullDateToDateTime(strTime);
						int ilDura = (dtDuration.Value.Hour * 60) + dtDuration.Value.Minute;
						TimeSpan ts = timeDEP - timeARR;
						if( (ts.TotalMinutes >= ilDura) && timeARR.Day != timeDEP.Day )
						{
							//The arrival part of rotation
							//RKEY,URNO,FLNO,CSGN,FLTI,JCNT,STOA,DOOA,TTYP,STEV,STYP,ORG3,ORG4,VIA3,VIA4,VIAN,PSTA,GTA1,GTA2,BLT1,BLT2,ISRE,ACT3,STOD,DOOD,DES3,DES4,PSTD,GTD1,GTD2,CKIF,CKIT
							//Arrival,ID,Sched,A,Na,K,S,ORG,VIA,POS,Gate1,Gate2,Belt,I,Type,Departure,ID,J,STD,A,Na,K,S,VIA,DES,POS,Gate1,Gate2,I
							//ARR:,,,,,,,,,,,,,,,
							//DEP:,,,,,,,,,,,,,
							string strFLNO = myAFT[llCurr]["FLNO"];
							if(strFLNO == "") strFLNO = myAFT[llCurr]["CSGN"];
							string strALC2A = myAFT[llCurr]["ALC2"];
							strValues += strALC2A + ",";
							strValues += myAFT[llCurr]["ADID"] +",";
							strValues += strFLNO + ",";
							strValues += Helper.DateString(myAFT[llCurr]["STOA"], "yyyyMMdd") + ",";
							//-----------------------------------The departure part of rotation
							strFLNO = myAFT[llCurr+1]["FLNO"];
							if(strFLNO == "") strFLNO = myAFT[llCurr+1]["CSGN"];
							strValues += myAFT[llCurr]["ADID"] +",";
							strValues += strFLNO + "\n";
							//	strValues += Helper.DateString(myAFT[llCurr+1]["STOD"], "yyyyMMdd") +  "\n";
							sb.Append(strValues);
						}
					}
					//					if( strCurrAdid == "D" && strNextAdid == "A")
					//					{
					//						DateTime timeARR = UT.CedaFullDateToDateTime(myAFT[llCurr+1]["STOA"]);
					//						DateTime timeDEP = UT.CedaFullDateToDateTime(myAFT[llCurr]["STOD"]);
					//						strTime = myAFT[llCurr+1]["TIFA"];
					//						if(strTime != "")
					//							timeARR = UT.CedaFullDateToDateTime(strTime);
					//						strTime = myAFT[llCurr]["TIFD"];
					//						if(strTime != "")
					//							timeDEP = UT.CedaFullDateToDateTime(strTime);
					//						int ilDura = (dtDuration.Value.Hour * 60) + dtDuration.Value.Minute;
					//						TimeSpan ts = timeDEP - timeARR;
					//						if( (ts.TotalMinutes >= ilDura) && timeARR.Day != timeDEP.Day )
					//						{
					//							//The arrival part of rotation
					//							string strFLNO = myAFT[llCurr+1]["FLNO"];
					//							if(strFLNO == "") strFLNO = myAFT[llCurr+1]["CSGN"];
					//							string strALC2AA = myAFT[llCurr]["ALC2"];
					//							strValues += strALC2AA + ",";
					//							strValues += myAFT[llCurr]["ADID"] +",";
					//							strValues += strFLNO + ",";
					//							strValues += Helper.DateString(myAFT[llCurr+1]["STOA"], "yyyyMMdd") + ",";
					//				//-----------------------------------The departure part of rotation
					//							strFLNO = myAFT[llCurr]["FLNO"];
					//							if(strFLNO == "") strFLNO = myAFT[llCurr]["CSGN"];
					//							strValues += myAFT[llCurr]["ADID"] +",";
					//							strValues += strFLNO + "\n";
					//						//	strValues += Helper.DateString(myAFT[llCurr]["STOD"], "yyyyMMdd") + "\n";
					//							sb.Append(strValues);
					//						}
					//					}
				}//if(strCurrRkey == strNextRkey)
				else
				{
					ilStep = 1;
				}
				llCurr = llCurr + ilStep;
			}//while(llCurr < myAFT.Count)
			tabResult.InsertBuffer(sb.ToString(), "\n");
			lblProgress.Text = "";
			progressBar1.Visible = true;
			tabResult.Sort("0", true, true);
			tabResult.Sort("3", true, true);
			tabResult.Refresh();
			tabResult.Visible = false;
	//		actualtabResults.Visible =true;
	//		prepareActualReportdata();
		}
	
		/// <summary>
		/// Calls the generic report. The contructor must have the Tab 
		/// control and the reports headers as well as the types for
		/// each column to enable the reports to transform data, e.g.
		/// the data/time formatting. The lengths of the columns are
		/// dynamically read out of the tab control's header.
		/// </summary>
		private void RunReport()
		{
			string strSubHeader = "";
			strSubHeader = "From: " + dtFrom.Value.ToString("dd.MM.yy'/'HH:mm") + " to " + 
				dtTo.Value.ToString("dd.MM.yy'/'HH:mm");
			strSubHeader += "(NAT: " + strNatureCode +  " "+" APT: " + strAirportCode + "  "+ " Duration: " + dtDuration.Value.ToShortTimeString()+"  " +" Overnight Flights: " + NumberofFlights[1].ToString() + ")";
			
			rptFIPS rpt = new rptFIPS(actualtabResults , 
				"Statistics of Overnight Arrival Flights (summary):" , 
				strSubHeader, "", 10);
			frmPrintPreview frm = new frmPrintPreview(rpt);
			frm.Show();
		}

		/// <summary>
		/// Start the print preview without data selection.
		/// This can be used to adjust the column widths and
		/// the sorting of the tab before printing.
		/// </summary>
		/// <param name="sender">Originator is the form</param>
		/// <param name="e">Event arguments.</param>
		private void btnPrintPreview_Click(object sender, System.EventArgs e)
		{
			if(actualtabResults.GetLineCount ()==0)
			{
				
				MessageBox.Show(this, "No Data to preview");
			}
			else
			{
				RunReport();
			}
		
		}

		/// <summary>
		/// Loads data and dirctly calls the print preview.
		/// </summary>
		/// <param name="sender">Originator is the form</param>
		/// <param name="e">Event arguments.</param>
		private void btnLoadPrint_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			tabResult.ResetContent();
			actualtabResults.ResetContent ();
			LoadReportData();
			PrepareFillReportData();
		//	PrepareReportDataRotations();
			RunReport();
			this.Cursor = Cursors.Arrow;
		}

		/// <summary>
		/// Sent by the tab control when the user double click.
		/// This is necessary for sorting.
		/// </summary>
		/// <param name="sender">The Tab control</param>
		/// <param name="e">Double click event parameters.</param>
		private void tabResult_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo == -1)
			{
				if( e.colNo == tabResult.CurrentSortColumn)
				{
					if( tabResult.SortOrderASC == true)
					{
						tabResult.Sort(e.colNo.ToString(), false, true);
					}
					else
					{
						tabResult.Sort(e.colNo.ToString(), true, true);
					}
				}
				else
				{
					tabResult.Sort( e.colNo.ToString(), true, true);
				}
				tabResult.Refresh();
			}
		}

		private void frmOverNight_FlightSummary_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			myDB.Unbind("NAT");
			myDB.Unbind("AFT");
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			txtLens.Text = tabResult.HeaderLengthString;
		}

		private void actualtabResults_SendLButtonClick(object sender, AxTABLib._DTABEvents_SendLButtonClickEvent e)
		{
			if(e.lineNo == -1)
			{
				if( e.colNo == actualtabResults.CurrentSortColumn)
				{
					if( actualtabResults.SortOrderASC == true)
					{
						actualtabResults.DeleteLine(actualtabResults.GetLineCount()-1);
						actualtabResults.Sort(e.colNo.ToString(), false, true);
						insertTotal();
					}
					else
					{
						actualtabResults.DeleteLine(actualtabResults.GetLineCount()-1);
						actualtabResults.Sort(e.colNo.ToString(), true, true);
						insertTotal();
					}
				}
				else
				{
					actualtabResults.DeleteLine(actualtabResults.GetLineCount()-1);
					actualtabResults.Sort( e.colNo.ToString(), true, true);
				insertTotal();
				}
				actualtabResults.Refresh();
			}
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			myDB.Unbind("NAT");
			myDB.Unbind("AFT");
			this.Close ();
		}
	}
}
