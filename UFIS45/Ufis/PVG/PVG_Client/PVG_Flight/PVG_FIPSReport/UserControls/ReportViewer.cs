using System;
using System.Data;
using System.Collections;
using Ufis.Utils;
using Ufis.Data;

namespace UserControls
{
	/// <summary>
	/// Summary description for ReportViewer.
	/// </summary>
	public class ReportViewer
	{
		private	ViewFilter					filter = null;
		private	ViewFilter.ReportRow		report = null;
		private ViewFilter.ViewRow			view   = null;
		private	ViewFilter.ViewSelectionRow	selection = null;
		private	ViewFilter.GeneralFilterRow	generalFilter = null;	
		private	Ufis.Data.IDatabase			myDB   = null;
		private	Ufis.Data.ITable			myACR  = null;
		private	Ufis.Data.ITable			myHAI  = null;
		private	Ufis.Data.ITable			myALT  = null;
		private	Ufis.Data.ITable			myHTY  = null;

		public delegate void ViewChangedEvent(string view);
		public	event ViewChangedEvent viewEvent;	

		public ReportViewer()
		{
			//
			// TODO: Add constructor logic here
			//
			this.filter = new ViewFilter();
			this.filter.View.RowChanged +=new DataRowChangeEventHandler(View_RowChanged);
			this.filter.View.RowDeleted +=new DataRowChangeEventHandler(View_RowDeleted);

			this.myDB	= UT.GetMemDB();
			this.myHAI = myDB["ReportViewer_HAI"];
			if (this.myHAI == null)
			{
				myHAI = myDB.Bind("ReportViewer_HAI","HAI","HSNA,URNO,ALTU,TASK","5,10,10,128","HSNA,URNO,ALTU,TASK");
				myHAI.Load("");
				myHAI.CreateIndex("HSNA","HSNA");
				myHAI.CreateIndex("TASK","TASK");
			}

			this.myALT = myDB["ReportViewer_ALT"];
			if (this.myALT == null)
			{
				myALT = myDB.Bind("ReportViewer_ALT","ALT","URNO,ALC3","10,3","URNO,ALC3");
				myALT.Load("");
				myALT.CreateIndex("URNO","URNO");
			}

			this.myHTY = myDB["ReportViewer_HTY"];
			if (this.myHTY == null)
			{
				myHTY = myDB.Bind("ReportViewer_HTY","HTY","HTYP,URNO,HNAM","2,10,30","HTYP,URNO,HNAM");
				myHTY.Load("");
				myHTY.CreateIndex("HTYP","HTYP");
			}

		}

		internal bool		Load()
		{
			try
			{
				this.filter.ReadXml("C:\\Ufis\\System\\REPORT_VIEWER.xml");
				this.filter.AcceptChanges();
				return true;
			}
			catch(Exception exc)
			{
				System.Diagnostics.Debug.WriteLine(exc.Message);
				this.filter.RejectChanges();
				return false;
			}
		}

		internal bool SaveCurrentViewAs(string name)
		{
			try
			{
				this.view.Name = name;
				this.filter.AcceptChanges();
				filter.WriteXml("C:\\Ufis\\System\\REPORT_VIEWER.xml");
				return true;
			}
			catch(Exception exc)
			{
				System.Diagnostics.Debug.WriteLine(exc.Message);
				this.filter.RejectChanges();
				return false;
			}
		}

		internal bool RenameCurrentViewTo(string name)
		{
			try
			{
				if (this.report.Current == this.view.Name)
					this.report.Current = name;
				this.view.Name = name;
				return true;
			}
			catch(Exception exc)
			{
				System.Diagnostics.Debug.WriteLine(exc.Message);
				return false;
			}
		}

		public string	Report
		{
			get
			{
				if (this.report == null)
					return "";
				else
					return this.report.Type;
			}

			set
			{
				string selection = string.Format("TYPE='{0}'",value);
				DataRow[] rows = this.filter.Report.Select(selection);
				if (rows != null && rows.Length == 1)
				{
					this.report = (ViewFilter.ReportRow)rows[0];
					if (this.report.Current.Length > 0)
					{
						this.View	= this.report.Current;
					}
					else
					{
						this.report.Current = "<DEFAULT>";
						this.filter.AcceptChanges();
						this.View = this.report.Current;
					}
					this.filter.AcceptChanges();
				}
				else if (value.Length > 0)
				{
					this.report = this.filter.Report.NewReportRow();
					this.report.Type = value;
					this.report.Current = "<DEFAULT>";
					this.filter.Report.AddReportRow(this.report);
					this.filter.AcceptChanges();
					this.View = this.report.Current;
					this.filter.AcceptChanges();
				}
				
			}
		}

		public	string CurrentView
		{
			get
			{
				return this.View;
			}
		}

		internal string	View
		{
			get
			{
				if (this.view == null)
					return "";
				else
					return this.view.Name;
			}

			set
			{
				this.filter.RejectChanges();

				if (this.report == null)
					return;

				string selection = string.Format("Report_Id = {0} AND NAME='{1}'",this.report["Report_Id"],value);
				DataRow[] rows = this.filter.View.Select(selection);
				if (rows != null && rows.Length == 1)
				{
					this.view = (ViewFilter.ViewRow)rows[0];
					this.report.Current = this.view.Name;

					// check on page data
					bool setDefaultFilterNecessary = false;
					ViewFilter.ViewSelectionRow[] rows1 = this.view.GetViewSelectionRows();
					if (rows1 == null || rows1.Length == 0)
					{
						this.selection = this.filter.ViewSelection.NewViewSelectionRow();
						this.selection.ViewRow = this.view;
						this.filter.ViewSelection.AddViewSelectionRow(this.selection);
						setDefaultFilterNecessary = true;
					}
					else
					{
						ViewFilter.ViewSelectionRow row = rows1[0];
						this.selection = row;
					}

					ViewFilter.GeneralFilterRow[] rows2 = this.view.GetGeneralFilterRows();
					if (rows2 == null || rows2.Length == 0)
					{
						this.generalFilter = this.filter.GeneralFilter.NewGeneralFilterRow();
						this.generalFilter.ViewRow = this.view;
						this.filter.GeneralFilter.AddGeneralFilterRow(this.generalFilter);
						setDefaultFilterNecessary = true;
					}
					else
					{
						ViewFilter.GeneralFilterRow row = rows2[0];
						this.generalFilter = row;
					}

					if (setDefaultFilterNecessary)
					{
						this.SetDefaultFilter();
						this.filter.AcceptChanges();
					}
				}
				else
				{
					this.view = filter.View.NewViewRow();
					this.view.ReportRow = this.report;
					this.view.Name = value;
					this.filter.View.AddViewRow(this.view);
					this.report.Current = this.view.Name;

					this.selection = this.filter.ViewSelection.NewViewSelectionRow();
					this.selection.ViewRow = this.view;
					this.filter.ViewSelection.AddViewSelectionRow(this.selection);

					this.generalFilter = this.filter.GeneralFilter.NewGeneralFilterRow();
					this.generalFilter.ViewRow = this.view;
					this.filter.GeneralFilter.AddGeneralFilterRow(this.generalFilter);

					this.SetDefaultFilter();

				}
				
			}
		}

		internal string[] Views
		{
			get
			{
				if (this.report == null)
					return new string[0];
				else
				{
					ViewFilter.ViewRow[] rows = this.report.GetViewRows();
					string[] names = new string[rows.Length];
					for (int i = 0; i < rows.Length; i++)
					{
						names[i] = rows[i].Name;
					}

					return names;
				}
			}
		}

		public string[] Origins
		{
			get
			{
				if (this.report == null || this.view == null || this.selection == null)
					return new string[0];
				else
				{
					ViewFilter.AirportRow[] rows = this.selection.GetAirportRows();
					string[] names = new string[rows.Length];
					for (int i = 0; i < rows.Length; i++)
					{
						names[i] = rows[i].Name;
					}

					return names;
				}
			}
		}

		public bool CurrentViewIsModified()
		{
			if (this.report == null)
				return false;
			if (this.view == null)
				return false;
			if (this.view.RowState == DataRowState.Modified)
				return true;
			else
				return false;
		}

		public bool CopyCurrentView()
		{
			if (this.report == null)
				return false;
			if (this.view == null)
				return false;

			ViewFilter.ViewRow newViewRow = (ViewFilter.ViewRow)this.CopyCurrentRow(this.view,this.view.Table,this.report);
			newViewRow.Name = "Copy of " + this.view.Name;
			return true;
		}

		internal DataRow CopyCurrentRow(DataRow row,DataTable table, DataRow parentRow)
		{
			DataRow newRow = table.NewRow();
			foreach (DataColumn column in table.Columns)
			{
				if (column.ColumnName.IndexOf("_Id") < 0)
					newRow[column] = row[column];
			}
//			object[] items = row.ItemArray;
//			newRow.ItemArray = items;
			newRow.SetParentRow(parentRow);
			table.Rows.Add(newRow);

			foreach (DataRelation relation in table.DataSet.Relations)
			{
				if (relation.ParentTable != table)
					continue;

				try
				{
					foreach(DataRow childRow in row.GetChildRows(relation))
					{
						CopyCurrentRow(childRow,childRow.Table,newRow);
					}
				}
				catch(Exception exc)
				{
					System.Diagnostics.Debug.WriteLine(exc.Message);
				}
			}

			return newRow;
		}

		internal bool CreateView(string name)
		{
			if (this.report == null)
				return false;

			this.filter.RejectChanges();

			this.view = filter.View.NewViewRow();
			this.view.ReportRow = this.report;
			this.view.Name = name;
			this.filter.View.AddViewRow(this.view);

			this.selection = this.filter.ViewSelection.NewViewSelectionRow();
			this.selection.ViewRow = this.view;
			this.filter.ViewSelection.AddViewSelectionRow(this.selection);

			this.generalFilter = this.filter.GeneralFilter.NewGeneralFilterRow();
			this.generalFilter.ViewRow = this.view;
			this.filter.GeneralFilter.AddGeneralFilterRow(this.generalFilter);

			this.SetDefaultFilter();

			return true;
		}

		internal bool UpdateCurrentViewAs(string name,bool acceptChanges)
		{
			if (this.report == null)
				return false;

			if (acceptChanges)
			{
				this.view.Name = name;
				this.report.Current = name;
				this.filter.AcceptChanges();
			}
			else
				this.filter.RejectChanges();
			return true;
		}

		internal bool DeleteView(string name)
		{
			if (this.report == null)
				return false;

			string selection = string.Format("Report_Id = {0} AND NAME='{1}'",this.report["Report_Id"],name);
			DataRow[] rows = filter.View.Select(selection);

			if (rows != null && rows.Length != 0)
			{
				foreach (DataRow row in rows)
				{
					DataRow[] childRows = null;
					foreach(DataRelation relation in this.filter.View.ChildRelations)
					{
						childRows = row.GetChildRows(relation);
						foreach (DataRow childRow in childRows)
						{
							childRow.Delete();
						}
					}
					row.Delete();
				}

				if (name == this.report.Current)
				{
					this.view = null;
					this.selection = null;
					this.generalFilter = null;
					this.report.Current = "";
				}
				this.filter.AcceptChanges();
				filter.WriteXml("C:\\Ufis\\System\\REPORT_VIEWER.xml");
			}
			else
			{
				return false;
			}


			return true;
		}

		internal ViewFilter.ViewRow CurrentViewRow
		{
			get
			{
				return this.view;
			}
		}

		internal ViewFilter		CurrentFilter
		{
			get
			{
				return this.filter;
			}
		}

		public DateTime PeriodFrom
		{
			get
			{
				if (this.selection["PeriodFrom"] != System.DBNull.Value)
				{
					return this.selection.PeriodFrom;
				}
				else
				{
					DateTime olCurrent = DateTime.Now;
					if (this.selection["RelativeBefore"] != System.DBNull.Value)
					{
						olCurrent -= new TimeSpan(0,this.selection.RelativeBefore,0,0,0);
					}

					return olCurrent;
				}
			}

			set
			{

			}
		}

		public DateTime PeriodTo
		{
			get
			{
				if (this.selection["PeriodTo"] != System.DBNull.Value)
				{
					return this.selection.PeriodTo;
				}
				else
				{
					DateTime olCurrent = DateTime.Now;
					if (this.selection["RelativeAfter"] != System.DBNull.Value)
					{
						olCurrent += new TimeSpan(0,this.selection.RelativeAfter,0,0,0);
					}

					return olCurrent;
				}
			}

			set
			{

			}
		}

		public bool	IncludeCodeShare
		{
			get
			{
				if (this.selection["IncludeCodeShare"] != System.DBNull.Value)
					return this.selection.IncludeCodeShare;
				else
					return false;
			}
		}

		public bool	Arrival
		{
			get
			{
				// get flight type
				string selection = string.Format("ViewSelection_Id = {0}",this.selection["ViewSelection_Id"]);
				ViewFilter.FlightTypeRow[] flightTypeRows = (ViewFilter.FlightTypeRow[])this.filter.FlightType.Select(selection);
				if (flightTypeRows != null && flightTypeRows.Length != 0)
				{
					if (flightTypeRows[0]["Arrival"] != System.DBNull.Value)
						return flightTypeRows[0].Arrival;
					else
						return false;
				}
				else
					return false;
			}
		}

		public bool Departure
		{
			get
			{
				// get flight type
				string selection = string.Format("ViewSelection_Id = {0}",this.selection["ViewSelection_Id"]);
				ViewFilter.FlightTypeRow[] flightTypeRows = (ViewFilter.FlightTypeRow[])this.filter.FlightType.Select(selection);
				if (flightTypeRows != null && flightTypeRows.Length != 0)
				{
					if (flightTypeRows[0]["Departure"] != System.DBNull.Value)
						return flightTypeRows[0].Departure;
					else
						return false;
				}
				else
					return false;
			}
		}

		public bool Rotation
		{
			get
			{
				// get flight type
				if (this.filter == null || this.selection == null)
					return false;

				string selection = string.Format("ViewSelection_Id = {0}",this.selection["ViewSelection_Id"]);
				ViewFilter.FlightTypeRow[] flightTypeRows = (ViewFilter.FlightTypeRow[])this.filter.FlightType.Select(selection);
				if (flightTypeRows != null && flightTypeRows.Length != 0)
				{
					if (flightTypeRows[0]["Rotation"] != System.DBNull.Value)
						return flightTypeRows[0].Rotation;
					else
						return false;
				}
				else
					return false;
			}
		}

		public string AdditionalSelection(int loop)
		{
			string olAlc = "";
			string olFltn= "";
			string olFlns= "";

			System.Text.StringBuilder sb = new System.Text.StringBuilder();

			if (loop == 0)
			{
				if (this.selection["DAYS"] != System.DBNull.Value)
				{
					string olDoo="";
					for (int i = 0; i < this.selection.Days.Length;i++)
					{
						olDoo += string.Format(",'{0}'",this.selection.Days[i].ToString());						
					}

					if (olDoo.Length > 0)
					{
						sb.AppendFormat(" AND ((DOOA IN ({0}) AND ADID IN ('A','B')) OR (DOOD IN ({0}) AND ADID IN ('D','B')))",olDoo.Substring(1));
					}
				}

				if (this.selection["FlicoFrom"] != System.DBNull.Value)
					sb.AppendFormat(" AND FDAT >= '{0}'",UT.DateTimeToCeda(this.selection.FlicoFrom));

				if (this.selection["ChangesFrom"] != System.DBNull.Value)
					sb.AppendFormat(" AND LSTU >= '{0}'",UT.DateTimeToCeda(this.selection.ChangesFrom));

				if (this.selection["Callsign"] != System.DBNull.Value)
					sb.AppendFormat(" AND CSGN = '{0}'",this.selection.Callsign);

				if (this.selection["Registration"] != System.DBNull.Value)
					sb.AppendFormat(" AND REGN = '{0}'",this.selection.Registration);

				if (this.selection["FlightId"] != System.DBNull.Value)
					sb.AppendFormat(" AND FLTI = '{0}'",this.selection.FlightId);

				if (this.selection["StatisticId"] != System.DBNull.Value)
					sb.AppendFormat(" AND STEV = '{0}'",this.selection.StatisticId);



				// registration owner
				if (this.selection["Owner"] != System.DBNull.Value)
				{
#if	UseExplicitValues
					//	read registration of specified owner if necessary
					if (this.selection.Owner.Length > 0)
					{
						myACR = myDB.Bind("ReportViewer_ACR","ACR","REGN,OWNE","12,10","REGN,OWNE");
						string strWhere = string.Format("WHERE OWNE='{0}'",this.selection.Owner);
						myACR.Load(strWhere);
						myACR.CreateIndex("REGN","REGN");
						System.Text.StringBuilder sb1 = new System.Text.StringBuilder(1024);
						for (int i = 0; i < myACR.Count; i++)
						{
							sb1.Append(',');
							sb1.Append('\'');
							sb1.Append(myACR[i]["REGN"]);
							sb1.Append('\'');
						}

						// check whether we have a valid owner
						if (myACR.Count > 0)
						{
							sb.AppendFormat(" AND REGN IN ({0})",sb1.ToString(1,sb1.Length-1));
						}
						else
						{
							sb.AppendFormat(" AND REGN = '{0}'","__INVALID__");
						}

						myDB.Unbind("ReportViewer_ACR");
					}
#else
					if (this.selection.Owner.Length > 0)
					{
						sb.AppendFormat(" AND REGN IN (select REGN from ACRTAB WHERE OWNE='{0}')",this.selection.Owner);
					}
#endif

				}

				// get aircraft types
				string act3 = "";
				string act5 = "";
				string selection = string.Format("ViewSelection_Id = {0}",this.selection["ViewSelection_Id"]);
				ViewFilter.AircraftTypeRow[] aircraftTypeRows = (ViewFilter.AircraftTypeRow[])this.filter.AircraftType.Select(selection);
				if (aircraftTypeRows != null && aircraftTypeRows.Length != 0)
				{
					foreach(ViewFilter.AircraftTypeRow row in aircraftTypeRows)
					{
						if (row["Name"] != System.DBNull.Value)
						{
							string[] columns = row.Name.Split(',');
							if (columns != null && columns.Length == 2)
							{
								act3 += string.Format(",'{0}'",columns[0]);
								act5 += string.Format(",'{0}'",columns[1]);
							}
						}
					}
				}

				if (act3.Length > 0 || act5.Length > 0)
				{
					sb.AppendFormat(" AND (ACT3 IN ({0}) OR ACT5 IN ({1}))",act3.Substring(1),act5.Substring(1));					
				}

				// get origin
				string org3="";
				string org4="";
				selection = string.Format("ViewSelection_Id = {0}",this.selection["ViewSelection_Id"]);
				ViewFilter.AirportRow[] airportRows = (ViewFilter.AirportRow[])this.filter.Airport.Select(selection);
				if (airportRows != null && airportRows.Length != 0)
				{
					foreach(ViewFilter.AirportRow row in airportRows)
					{
						if (row["Name"] != System.DBNull.Value)
						{
							string[] columns = row.Name.Split(',');
							if (columns != null && columns.Length == 2)
							{
								org3 += string.Format(",'{0}'",columns[0]);
								org4 += string.Format(",'{0}'",columns[1]);
							}
						}
					}
				}


				// get destination
				string des3="";
				string des4="";
				selection = string.Format("ViewSelection_Id = {0}",this.selection["ViewSelection_Id"]);
				ViewFilter.DestinationRow[] destRows = (ViewFilter.DestinationRow[])this.filter.Destination.Select(selection);
				if (destRows != null && destRows.Length != 0)
				{
					foreach(ViewFilter.DestinationRow row in destRows)
					{
						if (row["Name"] != System.DBNull.Value)
						{
							string[] columns = row.Name.Split(',');
							if (columns != null && columns.Length == 2)
							{
								des3 += string.Format(",'{0}'",columns[0]);
								des4 += string.Format(",'{0}'",columns[1]);
							}
						}
					}
				}

				if (org3.Length > 0 || org4.Length > 0)
				{
					if (des3.Length > 0 || des4.Length > 0)
					{
						sb.AppendFormat(" AND ((ORG3 IN ({0}) OR ORG4 IN ({1})) OR (ORG3 IN ({2}) OR ORG4 IN ({3})))",org3.Substring(1),org4.Substring(1),des3.Substring(1),des4.Substring(1));					
					}
					else
					{
						sb.AppendFormat(" AND (ORG3 IN ({0}) OR ORG4 IN ({1}))",org3.Substring(1),org4.Substring(1));					
					}
				}
				else if (des3.Length > 0 || des4.Length > 0)
				{
					sb.AppendFormat(" AND (DES3 IN ({0}) OR DES4 IN ({1}))",des3.Substring(1),des4.Substring(1));					
				}


				// get via
				string via3="";
				string via4="";
				selection = string.Format("ViewSelection_Id = {0}",this.selection["ViewSelection_Id"]);
				ViewFilter.ViaRow[] viaRows = (ViewFilter.ViaRow[])this.filter.Via.Select(selection);
				if (viaRows != null && viaRows.Length != 0)
				{
					foreach(ViewFilter.ViaRow row in viaRows)
					{
						if (row["Name"] != System.DBNull.Value)
						{
							string[] columns = row.Name.Split(',');
							if (columns != null && columns.Length == 2)
							{
								via3 += string.Format(",'{0}'",columns[0]);
								via4 += string.Format(",'{0}'",columns[1]);
							}
						}
					}
				}

				if (via3.Length > 0 || via4.Length > 0)
				{
					sb.AppendFormat(" AND (VIA3 IN ({0}) OR VIA4 IN ({1}))",via3.Substring(1),via4.Substring(1));					
				}


				// get natures
				string ttyp = "";
				selection = string.Format("ViewSelection_Id = {0}",this.selection["ViewSelection_Id"]);
				ViewFilter.NatureRow[] natureRows = (ViewFilter.NatureRow[])this.filter.Nature.Select(selection);
				if (natureRows != null && natureRows.Length != 0)
				{
					foreach(ViewFilter.NatureRow row in natureRows)
					{
						if (row["Name"] != System.DBNull.Value)
						{
							ttyp += string.Format(",'{0}'",row.Name);
						}
					}
				}

				if (ttyp.Length > 0)
				{
					sb.AppendFormat(" AND TTYP IN ({0})",ttyp.Substring(1));					
				}

#if	UseExplicitValues
				// get handling agent
				if (this.selection["HandlingAgent"] != System.DBNull.Value)
				{
					// get handling type
					ArrayList handlingTypes = new ArrayList();
					if (this.selection["HandlingType"] != System.DBNull.Value)
					{
						IRow[] rows = myHTY.RowsByIndexValue("HTYP",this.selection.HandlingType);
						if (rows != null && rows.Length == 1)
						{
							IRow[] haiRows = myHAI.RowsByIndexValue("TASK",rows[0]["HNAM"]);
							for (int i = 0; haiRows != null && i < haiRows.Length; i++)
							{
								handlingTypes.Add((string)haiRows[i]["URNO"]);		
							}
						}
					}

					string alc3="";
					IRow[] haiRows2 = myHAI.RowsByIndexValue("HSNA",this.selection.HandlingAgent);
					for (int i = 0; haiRows2 != null && i < haiRows2.Length; i++)
					{
						if (handlingTypes.Count != 0)
						{
							if (handlingTypes.IndexOf((string)haiRows2[i]["URNO"]) < 0)
								continue;
						}

						IRow[] altRows = myALT.RowsByIndexValue("URNO",haiRows2[i]["ALTU"]);
						if (altRows != null && altRows.Length == 1)
						{
							alc3 += string.Format(",'{0}'",altRows[0]["ALC3"]);
						}
					}

					if (alc3.Length > 0)
					{
						sb.AppendFormat(" AND ALC3 IN ({0})",alc3.Substring(1));					
					}

				}
				// get handling type
				else if (this.selection["HandlingType"] != System.DBNull.Value)
				{
					string alc3="";
					IRow[] rows = myHTY.RowsByIndexValue("HTYP",this.selection.HandlingType);
					if (rows != null && rows.Length == 1)
					{
						IRow[] haiRows = myHAI.RowsByIndexValue("TASK",rows[0]["HNAM"]);
						for (int i = 0; haiRows != null && i < haiRows.Length; i++)
						{
							IRow[] altRows = myALT.RowsByIndexValue("URNO",haiRows[i]["ALTU"]);
							if (altRows != null && altRows.Length == 1)
							{
								alc3 += string.Format(",'{0}'",altRows[0]["ALC3"]);
							}
						}
					}

					if (alc3.Length > 0)
					{
						sb.AppendFormat(" AND ALC3 IN ({0})",alc3.Substring(1));					
					}
				}
#else
				// get handling agent
				if (this.selection["HandlingAgent"] != System.DBNull.Value)
				{
					if (this.selection["HandlingType"] != System.DBNull.Value)
					{
						sb.AppendFormat(" AND ALC3 IN (SELECT ALC3 from ALTTAB WHERE URNO IN (SELECT ALTU FROM HAITAB WHERE HSNA = '{0}' AND TASK IN (SELECT TRIM (HNAM) FROM HTYTAB WHERE HTYP='{1}')))",this.selection.HandlingAgent,this.selection.HandlingType);					
					}
					else
					{
						sb.AppendFormat(" AND ALC3 IN (SELECT ALC3 from ALTTAB WHERE URNO IN (SELECT ALTU FROM HAITAB WHERE HSNA='{0}'))",this.selection.HandlingAgent);					
					}
				}
					// get handling type
				else if (this.selection["HandlingType"] != System.DBNull.Value)
				{
					sb.AppendFormat(" AND ALC3 IN (SELECT ALC3 from ALTTAB WHERE URNO IN (SELECT ALTU FROM HAITAB WHERE TASK IN (SELECT TRIM (HNAM) FROM HTYTAB WHERE HTYP='{0}')))",this.selection.HandlingType);					
				}

#endif

				// get flight mode
				string olFtyp = "";
				ViewFilter.FlightModeRow[] flightModeRows = (ViewFilter.FlightModeRow[])this.filter.FlightMode.Select(selection);
				if (flightModeRows != null && flightModeRows.Length != 0)
				{
					if (flightModeRows[0]["Operation"] != System.DBNull.Value)
						olFtyp += flightModeRows[0].Operation ? ",'O','Z','B'":"";

					if (flightModeRows[0]["Planning"] != System.DBNull.Value)
						olFtyp += flightModeRows[0].Planning ? ",'S'":"";

					if (flightModeRows[0]["Prognosis"] != System.DBNull.Value)
						olFtyp += flightModeRows[0].Prognosis ? ",' '":"";
				}


				// get flight status
				ViewFilter.FlightStatusRow[] flightStatusRows = (ViewFilter.FlightStatusRow[])this.filter.FlightStatus.Select(selection);
				if (flightStatusRows != null && flightStatusRows.Length != 0)
				{
					if (flightStatusRows[0]["Cancelled"] != System.DBNull.Value)
						olFtyp += flightStatusRows[0].Cancelled ? ",'X'":"";

					if (flightStatusRows[0]["Noop"] != System.DBNull.Value)
						olFtyp += flightStatusRows[0].Noop ? ",'N'":"";

					if (flightStatusRows[0]["Diverted"] != System.DBNull.Value)
						olFtyp += flightStatusRows[0].Diverted ? ",'D'":"";

					if (flightStatusRows[0]["Rerouted"] != System.DBNull.Value)
						olFtyp += flightStatusRows[0].Rerouted ? ",'R'":"";
				}

				// get ground handling
				ViewFilter.GroundHandlingRow[] groundHandlingRows = (ViewFilter.GroundHandlingRow[])this.filter.GroundHandling.Select(selection);
				if (groundHandlingRows != null && groundHandlingRows.Length != 0)
				{
					if (groundHandlingRows[0]["Movement"] != System.DBNull.Value)
						olFtyp += groundHandlingRows[0].Movement ? ",'G'":"";

					if (groundHandlingRows[0]["Towing"] != System.DBNull.Value)
						olFtyp += groundHandlingRows[0].Towing ? ",'T'":"";
				}
				
				if (olFtyp.Length > 0)
				{
					sb.AppendFormat(" AND FTYP IN ({0})",olFtyp.Substring(1));
				}

			}

			if ((loop == 0 && this.IncludeCodeShare == false) || (loop == 1 && this.IncludeCodeShare == true))
			{
				if (this.selection["AirlineCode"] != System.DBNull.Value)
					sb.AppendFormat(" AND (ALC2 = '{0}' OR ALC3 = '{0}')",this.selection.AirlineCode);

				if (this.selection["FlightNumber"] != System.DBNull.Value)
					sb.AppendFormat(" AND FLTN = '{0}'",this.selection.FlightNumber);

				if (this.selection["FlightSuffix"] != System.DBNull.Value)
					sb.AppendFormat(" AND FLNS = '{0}'",this.selection.FlightSuffix);
			}

			// special code share handling
			if (loop == 1 && this.IncludeCodeShare)
			{
				if (this.selection["AirlineCode"] != System.DBNull.Value)
					olAlc = this.selection.AirlineCode;

				if (this.selection["FlightNumber"] != System.DBNull.Value)
					olFltn = this.selection.FlightNumber;

				if (this.selection["FlightSuffix"] != System.DBNull.Value)
					olFlns = this.selection.FlightSuffix;

				string olJfno = this.CreateFlno(olAlc,olFltn,olFlns, true);

				olJfno = olJfno.Trim();

				string olCondition1 = sb.Length > 0 ? sb.ToString(4,sb.Length-4): sb.ToString();

				sb.Length = 0;
				sb.AppendFormat(" AND (({0}) OR (JFNO LIKE '%{1}%'))",olCondition1,olJfno);
			}

			if (loop == 0)
			{
				if (sb.Length > 0)
					return string.Format(" AND ({0})",sb.ToString(4,sb.Length-4));
				else
					return sb.ToString();				
			}
			else
			{
				if (sb.Length > 0)
					return string.Format("({0})",sb.ToString(4,sb.Length-4));
				else
					return sb.ToString();				
			}
		}

		private string CreateFlno(string opAlc, string opFltn, string opFlns, bool bpWithEmptyFields)
		{
			opAlc = opAlc.TrimEnd();	
			opFltn= opFltn.TrimEnd();	
	
			if (opAlc.Length == 0 || opFltn.Length == 0)
			{
				if (!bpWithEmptyFields)
					return "";
			}


			if (opAlc.Length == 0)
				opAlc = "   ";

			if (opAlc.Length == 1)
				opAlc += "  ";

			if (opAlc.Length == 2)
				opAlc += " ";

			if (opFltn.Length == 0)
				opFltn = "   ";

			if (opFltn.Length == 1)
				opFltn = "00" + opFltn;

			if (opFltn.Length == 2)
				opFltn = "0" + opFltn;

			if (opFltn.Length == 3)
				opFltn += "  ";

			if (opFltn.Length == 4)
				opFltn += " ";

			if (opFlns.Length == 0)
				opFlns = " ";


			string olFlno = opAlc + opFltn + opFlns;

			olFlno = olFlno.TrimEnd();

			if (olFlno.Length == 0)
				return "";
			else
				return opAlc + opFltn + opFlns;
		}

		public string GeneralFilter
		{
			get
			{
				System.Text.StringBuilder sb = new System.Text.StringBuilder();
				if (this.generalFilter != null)
				{
					ViewFilter.FilterRow[] rows = this.generalFilter.GetFilterRows();
					foreach (ViewFilter.FilterRow row in rows)
					{
						if (sb.Length == 0)
							sb.Append(string.Format("{0} {1} {2} ",row.Field,row.Operator, row.Condition));
						else
							sb.Append(string.Format("{0} {1} {2} {3} ",row.Connection,row.Field,row.Operator, row.Condition));
					}

				}
				
				if (sb.Length > 0)
					return string.Format(" AND ({0})",sb.ToString());
				else
					return sb.ToString();				
			}
		}

		public string FlightTypes
		{
			get
			{
				// get flight mode
				string olFtyp = "";
				string selection = string.Format("ViewSelection_Id = {0}",this.selection["ViewSelection_Id"]);
				ViewFilter.FlightModeRow[] flightModeRows = (ViewFilter.FlightModeRow[])this.filter.FlightMode.Select(selection);
				if (flightModeRows != null && flightModeRows.Length != 0)
				{
					if (flightModeRows[0]["Operation"] != System.DBNull.Value)
						olFtyp += flightModeRows[0].Operation ? ",'O','Z','B'":"";

					if (flightModeRows[0]["Planning"] != System.DBNull.Value)
						olFtyp += flightModeRows[0].Planning ? ",'S'":"";

					if (flightModeRows[0]["Prognosis"] != System.DBNull.Value)
						olFtyp += flightModeRows[0].Prognosis ? ",' '":"";
				}


				// get flight status
				ViewFilter.FlightStatusRow[] flightStatusRows = (ViewFilter.FlightStatusRow[])this.filter.FlightStatus.Select(selection);
				if (flightStatusRows != null && flightStatusRows.Length != 0)
				{
					if (flightStatusRows[0]["Cancelled"] != System.DBNull.Value)
						olFtyp += flightStatusRows[0].Cancelled ? ",'X'":"";

					if (flightStatusRows[0]["Noop"] != System.DBNull.Value)
						olFtyp += flightStatusRows[0].Noop ? ",'N'":"";

					if (flightStatusRows[0]["Diverted"] != System.DBNull.Value)
						olFtyp += flightStatusRows[0].Diverted ? ",'D'":"";

					if (flightStatusRows[0]["Rerouted"] != System.DBNull.Value)
						olFtyp += flightStatusRows[0].Rerouted ? ",'R'":"";
				}

				// get ground handling
				ViewFilter.GroundHandlingRow[] groundHandlingRows = (ViewFilter.GroundHandlingRow[])this.filter.GroundHandling.Select(selection);
				if (groundHandlingRows != null && groundHandlingRows.Length != 0)
				{
					if (groundHandlingRows[0]["Movement"] != System.DBNull.Value)
						olFtyp += groundHandlingRows[0].Movement ? ",'G'":"";

					if (groundHandlingRows[0]["Towing"] != System.DBNull.Value)
						olFtyp += groundHandlingRows[0].Towing ? ",'T'":"";
				}

				if (olFtyp.Length > 0)
					return olFtyp.Substring(1);
				else
					return olFtyp;
			}
		}

		private bool SetDefaultFilter()
		{
			if (this.report == null || this.view == null || this.selection == null)
				return false;

			if (report.Type == "FlightByNatureCode")
			{

				this.selection.PeriodFrom = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,0,0,0,0);

				this.selection.PeriodTo = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,23,59,59,0);

				string selection = string.Format("ViewSelection_Id = {0}",this.selection["ViewSelection_Id"]);
				ViewFilter.FlightModeRow[] flightModeRows = (ViewFilter.FlightModeRow[])this.filter.FlightMode.Select(selection);
				if (flightModeRows != null && flightModeRows.Length != 0)
				{
					flightModeRows[0].Operation = true;

					flightModeRows[0].Planning  = true;
				}
				else
				{
					ViewFilter.FlightModeRow row = this.filter.FlightMode.NewFlightModeRow();
					row.Operation = true;
					row.Planning = true;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightMode.AddFlightModeRow(row);
				}

				// set flight status
				ViewFilter.FlightStatusRow[] flightStatusRows = (ViewFilter.FlightStatusRow[])this.filter.FlightStatus.Select(selection);
				if (flightStatusRows != null && flightStatusRows.Length != 0)
				{
					flightStatusRows[0].Cancelled = true;
				}
				else
				{
					ViewFilter.FlightStatusRow row = this.filter.FlightStatus.NewFlightStatusRow();
					row.Cancelled = true;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightStatus.AddFlightStatusRow(row);
				}

				ViewFilter.FlightTypeRow[] flightTypeRows = (ViewFilter.FlightTypeRow[])this.filter.FlightType.Select(selection);
				if (flightTypeRows != null && flightTypeRows.Length != 0)
				{
					flightTypeRows[0].Arrival = true;
					flightTypeRows[0].Departure = true;
				}
				else
				{
					ViewFilter.FlightTypeRow row = this.filter.FlightType.NewFlightTypeRow();
					row.Arrival = true;
					row.Departure = true;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightType.AddFlightTypeRow(row);
				}
			
			}
			else if (report.Type == "CounterSchedule")
			{

				this.selection.PeriodFrom = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,0,0,0,0);

				this.selection.PeriodTo = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,23,59,59,0);

				string selection = string.Format("ViewSelection_Id = {0}",this.selection["ViewSelection_Id"]);
				ViewFilter.FlightModeRow[] flightModeRows = (ViewFilter.FlightModeRow[])this.filter.FlightMode.Select(selection);
				if (flightModeRows != null && flightModeRows.Length != 0)
				{
					flightModeRows[0].Operation = true;

					flightModeRows[0].Planning  = true;
				}
				else
				{
					ViewFilter.FlightModeRow row = this.filter.FlightMode.NewFlightModeRow();
					row.Operation = true;
					row.Planning = true;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightMode.AddFlightModeRow(row);
				}

				ViewFilter.FlightTypeRow[] flightTypeRows = (ViewFilter.FlightTypeRow[])this.filter.FlightType.Select(selection);
				if (flightTypeRows != null && flightTypeRows.Length != 0)
				{
					flightTypeRows[0].Departure = true;
					flightTypeRows[0].Arrival   = false;
					flightTypeRows[0].Rotation  = false;
				}
				else
				{
					ViewFilter.FlightTypeRow row = this.filter.FlightType.NewFlightTypeRow();
					row.Departure = true;
					row.Arrival   = false;
					row.Rotation  = false;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightType.AddFlightTypeRow(row);
				}
			
			}
			else if (report.Type == "CancelledFlights")
			{

				this.selection.PeriodFrom = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,0,0,0,0);

				this.selection.PeriodTo = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,23,59,59,0);

				string selection = string.Format("ViewSelection_Id = {0}",this.selection["ViewSelection_Id"]);

				// set flight status
				ViewFilter.FlightStatusRow[] flightStatusRows = (ViewFilter.FlightStatusRow[])this.filter.FlightStatus.Select(selection);
				if (flightStatusRows != null && flightStatusRows.Length != 0)
				{
					flightStatusRows[0].Cancelled = true;
				}
				else
				{
					ViewFilter.FlightStatusRow row = this.filter.FlightStatus.NewFlightStatusRow();
					row.Cancelled = true;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightStatus.AddFlightStatusRow(row);
				}

				ViewFilter.FlightTypeRow[] flightTypeRows = (ViewFilter.FlightTypeRow[])this.filter.FlightType.Select(selection);
				if (flightTypeRows != null && flightTypeRows.Length != 0)
				{
					flightTypeRows[0].Departure = true;
					flightTypeRows[0].Arrival   = true;
					flightTypeRows[0].Rotation  = false;
				}
				else
				{
					ViewFilter.FlightTypeRow row = this.filter.FlightType.NewFlightTypeRow();
					row.Departure = true;
					row.Arrival   = true;
					row.Rotation  = false;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightType.AddFlightTypeRow(row);
				}
			
			}
			else if (report.Type == "DelayedFlights")
			{

				this.selection.PeriodFrom = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,0,0,0,0);

				this.selection.PeriodTo = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,23,59,59,0);

				string selection = string.Format("ViewSelection_Id = {0}",this.selection["ViewSelection_Id"]);

				// set flight mode
				ViewFilter.FlightModeRow[] flightModeRows = (ViewFilter.FlightModeRow[])this.filter.FlightMode.Select(selection);
				if (flightModeRows != null && flightModeRows.Length != 0)
				{
					flightModeRows[0].Operation = true;
				}
				else
				{
					ViewFilter.FlightModeRow row = this.filter.FlightMode.NewFlightModeRow();
					row.Operation = true;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightMode.AddFlightModeRow(row);
				}

				// set flight status
				ViewFilter.FlightStatusRow[] flightStatusRows = (ViewFilter.FlightStatusRow[])this.filter.FlightStatus.Select(selection);
				if (flightStatusRows != null && flightStatusRows.Length != 0)
				{
					flightStatusRows[0].Cancelled = true;
					flightStatusRows[0].Noop = true;
					flightStatusRows[0].Rerouted = true;
					flightStatusRows[0].Diverted = true;
				}
				else
				{
					ViewFilter.FlightStatusRow row = this.filter.FlightStatus.NewFlightStatusRow();
					row.Cancelled = true;
					row.Noop	 = true;
					row.Rerouted = true;
					row.Diverted = true;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightStatus.AddFlightStatusRow(row);
				}

				ViewFilter.FlightTypeRow[] flightTypeRows = (ViewFilter.FlightTypeRow[])this.filter.FlightType.Select(selection);
				if (flightTypeRows != null && flightTypeRows.Length != 0)
				{
					flightTypeRows[0].Departure = true;
					flightTypeRows[0].Arrival   = false;
					flightTypeRows[0].Rotation  = false;
				}
				else
				{
					ViewFilter.FlightTypeRow row = this.filter.FlightType.NewFlightTypeRow();
					row.Departure = true;
					row.Arrival   = false;
					row.Rotation  = false;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightType.AddFlightTypeRow(row);
				}
			
			}
			else if (report.Type == "BeltSchedule")
			{

				this.selection.PeriodFrom = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,0,0,0,0);

				this.selection.PeriodTo = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,23,59,59,0);

				string selection = string.Format("ViewSelection_Id = {0}",this.selection["ViewSelection_Id"]);


				// set flight mode
				ViewFilter.FlightModeRow[] flightModeRows = (ViewFilter.FlightModeRow[])this.filter.FlightMode.Select(selection);
				if (flightModeRows != null && flightModeRows.Length != 0)
				{
					flightModeRows[0].Operation = true;
				}
				else
				{
					ViewFilter.FlightModeRow row = this.filter.FlightMode.NewFlightModeRow();
					row.Operation = true;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightMode.AddFlightModeRow(row);
				}

				ViewFilter.FlightTypeRow[] flightTypeRows = (ViewFilter.FlightTypeRow[])this.filter.FlightType.Select(selection);
				if (flightTypeRows != null && flightTypeRows.Length != 0)
				{
					flightTypeRows[0].Departure = false;
					flightTypeRows[0].Arrival   = true;
					flightTypeRows[0].Rotation  = false;
				}
				else
				{
					ViewFilter.FlightTypeRow row = this.filter.FlightType.NewFlightTypeRow();
					row.Departure = false;
					row.Arrival   = true;
					row.Rotation  = false;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightType.AddFlightTypeRow(row);
				}
			
			}
			else if (report.Type == "OvernightFlights")
			{

				this.selection.PeriodFrom = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,0,0,0,0);

				this.selection.PeriodTo = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,23,59,59,0);

				string selection = string.Format("ViewSelection_Id = {0}",this.selection["ViewSelection_Id"]);

				// set flight mode
				ViewFilter.FlightModeRow[] flightModeRows = (ViewFilter.FlightModeRow[])this.filter.FlightMode.Select(selection);
				if (flightModeRows != null && flightModeRows.Length != 0)
				{
					flightModeRows[0].Operation = true;
					flightModeRows[0].Planning	= true;
				}
				else
				{
					ViewFilter.FlightModeRow row = this.filter.FlightMode.NewFlightModeRow();
					row.Operation = true;
					row.Planning  = true;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightMode.AddFlightModeRow(row);
				}

				ViewFilter.FlightTypeRow[] flightTypeRows = (ViewFilter.FlightTypeRow[])this.filter.FlightType.Select(selection);
				if (flightTypeRows != null && flightTypeRows.Length != 0)
				{
					flightTypeRows[0].Departure = true;
					flightTypeRows[0].Arrival   = true;
					flightTypeRows[0].Rotation  = true;
				}
				else
				{
					ViewFilter.FlightTypeRow row = this.filter.FlightType.NewFlightTypeRow();
					row.Departure = true;
					row.Arrival   = true;
					row.Rotation  = true;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightType.AddFlightTypeRow(row);
				}
			
			}
			else if (report.Type == "GPUUsage")
			{

				this.selection.PeriodFrom = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,0,0,0,0);

				this.selection.PeriodTo = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,23,59,59,0);

				string selection = string.Format("ViewSelection_Id = {0}",this.selection["ViewSelection_Id"]);

				ViewFilter.FlightTypeRow[] flightTypeRows = (ViewFilter.FlightTypeRow[])this.filter.FlightType.Select(selection);
				if (flightTypeRows != null && flightTypeRows.Length != 0)
				{
					flightTypeRows[0].Departure = true;
					flightTypeRows[0].Arrival   = true;
					flightTypeRows[0].Rotation  = false;
				}
				else
				{
					ViewFilter.FlightTypeRow row = this.filter.FlightType.NewFlightTypeRow();
					row.Departure = true;
					row.Arrival   = true;
					row.Rotation  = false;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightType.AddFlightTypeRow(row);
				}


			}
			else if (report.Type == "DailyFlightLog")
			{

				this.selection.PeriodFrom = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,0,0,0,0);

				this.selection.PeriodTo = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,23,59,59,0);

				string selection = string.Format("ViewSelection_Id = {0}",this.selection["ViewSelection_Id"]);

				// set flight mode
				ViewFilter.FlightModeRow[] flightModeRows = (ViewFilter.FlightModeRow[])this.filter.FlightMode.Select(selection);
				if (flightModeRows != null && flightModeRows.Length != 0)
				{
					flightModeRows[0].Operation = true;
					flightModeRows[0].Planning	= true;
				}
				else
				{
					ViewFilter.FlightModeRow row = this.filter.FlightMode.NewFlightModeRow();
					row.Operation = true;
					row.Planning  = true;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightMode.AddFlightModeRow(row);
				}

				ViewFilter.FlightTypeRow[] flightTypeRows = (ViewFilter.FlightTypeRow[])this.filter.FlightType.Select(selection);
				if (flightTypeRows != null && flightTypeRows.Length != 0)
				{
					flightTypeRows[0].Departure = true;
					flightTypeRows[0].Arrival   = true;
					flightTypeRows[0].Rotation  = true;
				}
				else
				{
					ViewFilter.FlightTypeRow row = this.filter.FlightType.NewFlightTypeRow();
					row.Departure = true;
					row.Arrival   = true;
					row.Rotation  = true;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightType.AddFlightTypeRow(row);
				}
			
			}
			else if (report.Type == "SpotUsingRate")
			{

				this.selection.PeriodFrom = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,0,0,0,0);

				this.selection.PeriodTo = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,23,59,59,0);

				string selection = string.Format("ViewSelection_Id = {0}",this.selection["ViewSelection_Id"]);

				// set flight mode
				ViewFilter.FlightModeRow[] flightModeRows = (ViewFilter.FlightModeRow[])this.filter.FlightMode.Select(selection);
				if (flightModeRows != null && flightModeRows.Length != 0)
				{
					flightModeRows[0].Operation = true;
					flightModeRows[0].Planning = true;
				}
				else
				{
					ViewFilter.FlightModeRow row = this.filter.FlightMode.NewFlightModeRow();
					row.Operation = true;
					row.Planning  = true;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightMode.AddFlightModeRow(row);
				}


				ViewFilter.FlightTypeRow[] flightTypeRows = (ViewFilter.FlightTypeRow[])this.filter.FlightType.Select(selection);
				if (flightTypeRows != null && flightTypeRows.Length != 0)
				{
					flightTypeRows[0].Departure = true;
					flightTypeRows[0].Arrival   = true;
					flightTypeRows[0].Rotation  = true;
				}
				else
				{
					ViewFilter.FlightTypeRow row = this.filter.FlightType.NewFlightTypeRow();
					row.Departure = true;
					row.Arrival   = true;
					row.Rotation  = true;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightType.AddFlightTypeRow(row);
				}

				// get ground handling
				ViewFilter.GroundHandlingRow[] groundHandlingRows = (ViewFilter.GroundHandlingRow[])this.filter.GroundHandling.Select(selection);
				if (groundHandlingRows != null && groundHandlingRows.Length != 0)
				{
					groundHandlingRows[0].Movement = true;
					groundHandlingRows[0].Towing   = true;
				}
				else
				{
					ViewFilter.GroundHandlingRow row = this.filter.GroundHandling.NewGroundHandlingRow();
					row.Movement = true;
					row.Towing   = true;
					row.ViewSelectionRow = this.selection;
					this.filter.GroundHandling.AddGroundHandlingRow(row);

				}
			
			}
			else if (report.Type == "ArrivalDepartureSummary")
			{

				this.selection.PeriodFrom = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,0,0,0,0);

				this.selection.PeriodTo = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,23,59,59,0);

				string selection = string.Format("ViewSelection_Id = {0}",this.selection["ViewSelection_Id"]);

				// set flight mode
				ViewFilter.FlightModeRow[] flightModeRows = (ViewFilter.FlightModeRow[])this.filter.FlightMode.Select(selection);
				if (flightModeRows != null && flightModeRows.Length != 0)
				{
					flightModeRows[0].Operation = true;
					flightModeRows[0].Planning = true;
				}
				else
				{
					ViewFilter.FlightModeRow row = this.filter.FlightMode.NewFlightModeRow();
					row.Operation = true;
					row.Planning  = true;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightMode.AddFlightModeRow(row);
				}


				ViewFilter.FlightTypeRow[] flightTypeRows = (ViewFilter.FlightTypeRow[])this.filter.FlightType.Select(selection);
				if (flightTypeRows != null && flightTypeRows.Length != 0)
				{
					flightTypeRows[0].Departure = true;
					flightTypeRows[0].Arrival   = true;
					flightTypeRows[0].Rotation  = false;
				}
				else
				{
					ViewFilter.FlightTypeRow row = this.filter.FlightType.NewFlightTypeRow();
					row.Departure = true;
					row.Arrival   = true;
					row.Rotation  = false;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightType.AddFlightTypeRow(row);
				}
		
			}
			else if (report.Type == "CancelledFlightSummary")
			{

				this.selection.PeriodFrom = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,0,0,0,0);

				this.selection.PeriodTo = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,23,59,59,0);

				string selection = string.Format("ViewSelection_Id = {0}",this.selection["ViewSelection_Id"]);

				// set flight status
				ViewFilter.FlightStatusRow[] flightStatusRows = (ViewFilter.FlightStatusRow[])this.filter.FlightStatus.Select(selection);
				if (flightStatusRows != null && flightStatusRows.Length != 0)
				{
					flightStatusRows[0].Cancelled = true;
				}
				else
				{
					ViewFilter.FlightStatusRow row = this.filter.FlightStatus.NewFlightStatusRow();
					row.Cancelled = true;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightStatus.AddFlightStatusRow(row);
				}

				ViewFilter.FlightTypeRow[] flightTypeRows = (ViewFilter.FlightTypeRow[])this.filter.FlightType.Select(selection);
				if (flightTypeRows != null && flightTypeRows.Length != 0)
				{
					flightTypeRows[0].Departure = true;
					flightTypeRows[0].Arrival   = true;
					flightTypeRows[0].Rotation  = false;
				}
				else
				{
					ViewFilter.FlightTypeRow row = this.filter.FlightType.NewFlightTypeRow();
					row.Departure = true;
					row.Arrival   = true;
					row.Rotation  = false;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightType.AddFlightTypeRow(row);
				}
		
			}

			else if (report.Type == "OvernightFlightSummary")
			{

				this.selection.PeriodFrom = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,0,0,0,0);

				this.selection.PeriodTo = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,23,59,59,0);

				string selection = string.Format("ViewSelection_Id = {0}",this.selection["ViewSelection_Id"]);

				ViewFilter.FlightTypeRow[] flightTypeRows = (ViewFilter.FlightTypeRow[])this.filter.FlightType.Select(selection);
				if (flightTypeRows != null && flightTypeRows.Length != 0)
				{
					flightTypeRows[0].Departure = true;
					flightTypeRows[0].Arrival   = true;
					flightTypeRows[0].Rotation  = true;
				}
				else
				{
					ViewFilter.FlightTypeRow row = this.filter.FlightType.NewFlightTypeRow();
					row.Departure = true;
					row.Arrival   = true;
					row.Rotation  = true;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightType.AddFlightTypeRow(row);
				}
		
			}
			else if (report.Type == "ArrivalDepartureStatistics")
			{

				this.selection.PeriodFrom = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,0,0,0,0);

				this.selection.PeriodTo = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,23,59,59,0);

				string selection = string.Format("ViewSelection_Id = {0}",this.selection["ViewSelection_Id"]);

				ViewFilter.FlightTypeRow[] flightTypeRows = (ViewFilter.FlightTypeRow[])this.filter.FlightType.Select(selection);
				if (flightTypeRows != null && flightTypeRows.Length != 0)
				{
					flightTypeRows[0].Departure = true;
					flightTypeRows[0].Arrival   = false;
					flightTypeRows[0].Rotation  = false;
				}
				else
				{
					ViewFilter.FlightTypeRow row = this.filter.FlightType.NewFlightTypeRow();
					row.Departure = false;
					row.Arrival   = true;
					row.Rotation  = false;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightType.AddFlightTypeRow(row);
				}

				// set flight mode
				ViewFilter.FlightModeRow[] flightModeRows = (ViewFilter.FlightModeRow[])this.filter.FlightMode.Select(selection);
				if (flightModeRows != null && flightModeRows.Length != 0)
				{
					flightModeRows[0].Operation = true;
					flightModeRows[0].Planning = true;
				}
				else
				{
					ViewFilter.FlightModeRow row = this.filter.FlightMode.NewFlightModeRow();
					row.Operation = true;
					row.Planning  = true;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightMode.AddFlightModeRow(row);
				}
		
			}
			else if (report.Type == "AircraftMovements")
			{

				this.selection.PeriodFrom = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,0,0,0,0);

				this.selection.PeriodTo = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,23,59,59,0);

				string selection = string.Format("ViewSelection_Id = {0}",this.selection["ViewSelection_Id"]);

				ViewFilter.FlightTypeRow[] flightTypeRows = (ViewFilter.FlightTypeRow[])this.filter.FlightType.Select(selection);
				if (flightTypeRows != null && flightTypeRows.Length != 0)
				{
					flightTypeRows[0].Departure = true;
					flightTypeRows[0].Arrival   = true;
					flightTypeRows[0].Rotation  = false;
				}
				else
				{
					ViewFilter.FlightTypeRow row = this.filter.FlightType.NewFlightTypeRow();
					row.Departure = true;
					row.Arrival   = true;
					row.Rotation  = false;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightType.AddFlightTypeRow(row);
				}

				// set flight mode
				ViewFilter.FlightModeRow[] flightModeRows = (ViewFilter.FlightModeRow[])this.filter.FlightMode.Select(selection);
				if (flightModeRows != null && flightModeRows.Length != 0)
				{
					flightModeRows[0].Operation = true;
					flightModeRows[0].Planning = true;
				}
				else
				{
					ViewFilter.FlightModeRow row = this.filter.FlightMode.NewFlightModeRow();
					row.Operation = true;
					row.Planning  = true;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightMode.AddFlightModeRow(row);
				}
		
			}
			else if (report.Type == "CheckinCounterUsage")
			{

				this.selection.PeriodFrom = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,0,0,0,0);

				this.selection.PeriodTo = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,23,59,59,0);

				string selection = string.Format("ViewSelection_Id = {0}",this.selection["ViewSelection_Id"]);

				ViewFilter.FlightTypeRow[] flightTypeRows = (ViewFilter.FlightTypeRow[])this.filter.FlightType.Select(selection);
				if (flightTypeRows != null && flightTypeRows.Length != 0)
				{
					flightTypeRows[0].Departure = true;
					flightTypeRows[0].Arrival   = false;
					flightTypeRows[0].Rotation  = false;
				}
				else
				{
					ViewFilter.FlightTypeRow row = this.filter.FlightType.NewFlightTypeRow();
					row.Departure = true;
					row.Arrival   = false;
					row.Rotation  = false;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightType.AddFlightTypeRow(row);
				}

				// set flight mode
				ViewFilter.FlightModeRow[] flightModeRows = (ViewFilter.FlightModeRow[])this.filter.FlightMode.Select(selection);
				if (flightModeRows != null && flightModeRows.Length != 0)
				{
					flightModeRows[0].Operation = true;
					flightModeRows[0].Planning = true;
				}
				else
				{
					ViewFilter.FlightModeRow row = this.filter.FlightMode.NewFlightModeRow();
					row.Operation = true;
					row.Planning  = true;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightMode.AddFlightModeRow(row);
				}
		
			}
			else if (report.Type == "BlockedResources")
			{

				this.selection.PeriodFrom = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,0,0,0,0);

				this.selection.PeriodTo = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,23,59,59,0);

				string selection = string.Format("ViewSelection_Id = {0}",this.selection["ViewSelection_Id"]);

				ViewFilter.FlightTypeRow[] flightTypeRows = (ViewFilter.FlightTypeRow[])this.filter.FlightType.Select(selection);
				if (flightTypeRows != null && flightTypeRows.Length != 0)
				{
					flightTypeRows[0].Departure = true;
					flightTypeRows[0].Arrival   = true;
					flightTypeRows[0].Rotation  = true;
				}
				else
				{
					ViewFilter.FlightTypeRow row = this.filter.FlightType.NewFlightTypeRow();
					row.Departure = true;
					row.Arrival   = true;
					row.Rotation  = true;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightType.AddFlightTypeRow(row);
				}

				// set flight mode
				ViewFilter.FlightModeRow[] flightModeRows = (ViewFilter.FlightModeRow[])this.filter.FlightMode.Select(selection);
				if (flightModeRows != null && flightModeRows.Length != 0)
				{
					flightModeRows[0].Operation = true;
					flightModeRows[0].Planning = true;
				}
				else
				{
					ViewFilter.FlightModeRow row = this.filter.FlightMode.NewFlightModeRow();
					row.Operation = true;
					row.Planning  = true;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightMode.AddFlightModeRow(row);
				}
		
			}
			else if (report.Type == "AbnormalFlights")
			{

				this.selection.PeriodFrom = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,0,0,0,0);

				this.selection.PeriodTo = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,23,59,59,0);

				string selection = string.Format("ViewSelection_Id = {0}",this.selection["ViewSelection_Id"]);

				ViewFilter.FlightTypeRow[] flightTypeRows = (ViewFilter.FlightTypeRow[])this.filter.FlightType.Select(selection);
				if (flightTypeRows != null && flightTypeRows.Length != 0)
				{
					flightTypeRows[0].Departure = true;
					flightTypeRows[0].Arrival   = true;
					flightTypeRows[0].Rotation  = false;
				}
				else
				{
					ViewFilter.FlightTypeRow row = this.filter.FlightType.NewFlightTypeRow();
					row.Departure = true;
					row.Arrival   = true;
					row.Rotation  = false;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightType.AddFlightTypeRow(row);
				}

				// set flight mode
				ViewFilter.FlightModeRow[] flightModeRows = (ViewFilter.FlightModeRow[])this.filter.FlightMode.Select(selection);
				if (flightModeRows != null && flightModeRows.Length != 0)
				{
					flightModeRows[0].Operation = true;
					flightModeRows[0].Planning = true;
				}
				else
				{
					ViewFilter.FlightModeRow row = this.filter.FlightMode.NewFlightModeRow();
					row.Operation = true;
					row.Planning  = true;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightMode.AddFlightModeRow(row);
				}
		
			}
			else if (report.Type == "AircraftFinancialAccount")
			{

				this.selection.PeriodFrom = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,0,0,0,0);

				this.selection.PeriodTo = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,23,59,59,0);

				string selection = string.Format("ViewSelection_Id = {0}",this.selection["ViewSelection_Id"]);

				ViewFilter.FlightTypeRow[] flightTypeRows = (ViewFilter.FlightTypeRow[])this.filter.FlightType.Select(selection);
				if (flightTypeRows != null && flightTypeRows.Length != 0)
				{
					flightTypeRows[0].Departure = true;
					flightTypeRows[0].Arrival   = true;
					flightTypeRows[0].Rotation  = true;
				}
				else
				{
					ViewFilter.FlightTypeRow row = this.filter.FlightType.NewFlightTypeRow();
					row.Departure = true;
					row.Arrival   = true;
					row.Rotation  = true;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightType.AddFlightTypeRow(row);
				}

				// set flight mode
				ViewFilter.FlightModeRow[] flightModeRows = (ViewFilter.FlightModeRow[])this.filter.FlightMode.Select(selection);
				if (flightModeRows != null && flightModeRows.Length != 0)
				{
					flightModeRows[0].Operation = true;
					flightModeRows[0].Planning = true;
				}
				else
				{
					ViewFilter.FlightModeRow row = this.filter.FlightMode.NewFlightModeRow();
					row.Operation = true;
					row.Planning  = true;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightMode.AddFlightModeRow(row);
				}
		
			}
			else if (report.Type == "AverageFlightTime")
			{

				this.selection.PeriodFrom = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,0,0,0,0);

				this.selection.PeriodTo = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,23,59,59,0);

				string selection = string.Format("ViewSelection_Id = {0}",this.selection["ViewSelection_Id"]);

				ViewFilter.FlightTypeRow[] flightTypeRows = (ViewFilter.FlightTypeRow[])this.filter.FlightType.Select(selection);
				if (flightTypeRows != null && flightTypeRows.Length != 0)
				{
					flightTypeRows[0].Departure = false;
					flightTypeRows[0].Arrival   = true;
					flightTypeRows[0].Rotation  = false;
				}
				else
				{
					ViewFilter.FlightTypeRow row = this.filter.FlightType.NewFlightTypeRow();
					row.Departure = false;
					row.Arrival   = true;
					row.Rotation  = false;
					row.ViewSelectionRow = this.selection;
					this.filter.FlightType.AddFlightTypeRow(row);
				}

			}

			
			return true;
		}

		private void View_RowChanged(object sender, DataRowChangeEventArgs e)
		{
			if (this.report == null)
				return;

			if (e.Action == DataRowAction.Commit)
			{
				if (this.viewEvent != null)
				{
					try
					{
						string name = (string)e.Row["Name"];
						this.viewEvent(name);
					}
					catch(Exception exc)
					{
						System.Diagnostics.Debug.WriteLine(exc.Message);
						this.viewEvent("");
					}
				}
			}
		}

		private void View_RowDeleted(object sender, DataRowChangeEventArgs e)
		{

		}
	}
}

