using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace UserControls
{
	/// <summary>
	/// Summary description for ucView.
	/// </summary>
	public class ucView : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.Button btnView;
		private System.Windows.Forms.ComboBox cbViews;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public delegate void ViewChangedEvent(string view);

		#region implementation data
		private ReportViewer	viewer	= new ReportViewer();
		private	frmView			view	= new frmView();
		public	event ViewChangedEvent viewEvent;	
		#endregion

		public ReportViewer	Viewer
		{
			get
			{
				return this.viewer;
			}
		}

		public string Report
		{
			get
			{
				return this.viewer.Report;
			}

			set
			{
				this.viewer.Report = value;
			}
		}

		public ucView()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
			this.viewer.viewEvent +=new UserControls.ReportViewer.ViewChangedEvent(viewer_viewEvent);
			this.viewer.Load();
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				this.viewer.viewEvent -=new UserControls.ReportViewer.ViewChangedEvent(viewer_viewEvent);
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ucView));
			this.btnView = new System.Windows.Forms.Button();
			this.cbViews = new System.Windows.Forms.ComboBox();
			this.SuspendLayout();
			// 
			// btnView
			// 
			this.btnView.AccessibleDescription = resources.GetString("btnView.AccessibleDescription");
			this.btnView.AccessibleName = resources.GetString("btnView.AccessibleName");
			this.btnView.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnView.Anchor")));
			this.btnView.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnView.BackgroundImage")));
			this.btnView.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnView.Dock")));
			this.btnView.Enabled = ((bool)(resources.GetObject("btnView.Enabled")));
			this.btnView.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnView.FlatStyle")));
			this.btnView.Font = ((System.Drawing.Font)(resources.GetObject("btnView.Font")));
			this.btnView.Image = ((System.Drawing.Image)(resources.GetObject("btnView.Image")));
			this.btnView.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnView.ImageAlign")));
			this.btnView.ImageIndex = ((int)(resources.GetObject("btnView.ImageIndex")));
			this.btnView.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnView.ImeMode")));
			this.btnView.Location = ((System.Drawing.Point)(resources.GetObject("btnView.Location")));
			this.btnView.Name = "btnView";
			this.btnView.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnView.RightToLeft")));
			this.btnView.Size = ((System.Drawing.Size)(resources.GetObject("btnView.Size")));
			this.btnView.TabIndex = ((int)(resources.GetObject("btnView.TabIndex")));
			this.btnView.Text = resources.GetString("btnView.Text");
			this.btnView.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnView.TextAlign")));
			this.btnView.Visible = ((bool)(resources.GetObject("btnView.Visible")));
			this.btnView.Click += new System.EventHandler(this.btnView_Click);
			// 
			// cbViews
			// 
			this.cbViews.AccessibleDescription = resources.GetString("cbViews.AccessibleDescription");
			this.cbViews.AccessibleName = resources.GetString("cbViews.AccessibleName");
			this.cbViews.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cbViews.Anchor")));
			this.cbViews.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbViews.BackgroundImage")));
			this.cbViews.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cbViews.Dock")));
			this.cbViews.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbViews.Enabled = ((bool)(resources.GetObject("cbViews.Enabled")));
			this.cbViews.Font = ((System.Drawing.Font)(resources.GetObject("cbViews.Font")));
			this.cbViews.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cbViews.ImeMode")));
			this.cbViews.IntegralHeight = ((bool)(resources.GetObject("cbViews.IntegralHeight")));
			this.cbViews.ItemHeight = ((int)(resources.GetObject("cbViews.ItemHeight")));
			this.cbViews.Location = ((System.Drawing.Point)(resources.GetObject("cbViews.Location")));
			this.cbViews.MaxDropDownItems = ((int)(resources.GetObject("cbViews.MaxDropDownItems")));
			this.cbViews.MaxLength = ((int)(resources.GetObject("cbViews.MaxLength")));
			this.cbViews.Name = "cbViews";
			this.cbViews.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cbViews.RightToLeft")));
			this.cbViews.Size = ((System.Drawing.Size)(resources.GetObject("cbViews.Size")));
			this.cbViews.Sorted = true;
			this.cbViews.TabIndex = ((int)(resources.GetObject("cbViews.TabIndex")));
			this.cbViews.Text = resources.GetString("cbViews.Text");
			this.cbViews.Visible = ((bool)(resources.GetObject("cbViews.Visible")));
			this.cbViews.SelectedIndexChanged += new System.EventHandler(this.cbViews_SelectedIndexChanged);
			// 
			// ucView
			// 
			this.AccessibleDescription = resources.GetString("$this.AccessibleDescription");
			this.AccessibleName = resources.GetString("$this.AccessibleName");
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMargin")));
			this.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMinSize")));
			this.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.Controls.Add(this.cbViews);
			this.Controls.Add(this.btnView);
			this.Enabled = ((bool)(resources.GetObject("$this.Enabled")));
			this.Font = ((System.Drawing.Font)(resources.GetObject("$this.Font")));
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.Name = "ucView";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.Size = ((System.Drawing.Size)(resources.GetObject("$this.Size")));
			this.Load += new System.EventHandler(this.ucView_Load);
			this.ResumeLayout(false);

		}
		#endregion


		private void btnView_Click(object sender, System.EventArgs e)
		{
			string currentView = this.cbViews.Text;

			this.view.Text = "View " + this.Report;
			DialogResult result = this.view.ShowDialog();

			this.cbViews.Items.Clear();
			this.cbViews.Items.AddRange(this.viewer.Views);
			// check on default viewer
			if (this.cbViews.FindStringExact("<DEFAULT>") < 0)
				this.cbViews.Items.Add("<DEFAULT>");

			if (result == DialogResult.OK)
			{
				this.cbViews.Text = this.viewer.View;
			}
			else
			{
				if (this.cbViews.FindStringExact(currentView) < 0)
					this.viewer.View = "<DEFAULT>";
				else
					this.viewer.View = currentView;

				this.cbViews.Text = this.viewer.View;
			}

			this.cbViews.Refresh();

			if (result == DialogResult.OK)
			{
				if (this.viewEvent != null)
				{
					this.viewEvent(this.cbViews.Text);
				}
			}

		}

		private void ucView_Load(object sender, System.EventArgs e)
		{
			this.cbViews.Items.Clear();
			this.cbViews.Items.AddRange(this.viewer.Views);
			// check on default viewer
			if (this.cbViews.FindStringExact("<DEFAULT>") < 0)
				this.cbViews.Items.Add("<DEFAULT>");
			this.cbViews.Text = this.viewer.View;
			this.cbViews.Refresh();

			this.view.Viewer = this.viewer;
		
		}

		private void cbViews_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (this.viewer.View != this.cbViews.Text)
			{
				this.viewer.View = this.cbViews.Text;
				if (this.viewEvent != null)
				{
					this.viewEvent(this.viewer.View);
				}
			}
		}

		private void viewer_viewEvent(string view)
		{
			ucView_Load(this,null);

		}
	}
}
