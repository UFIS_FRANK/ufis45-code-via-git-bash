using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace UserControls
{
	/// <summary>
	/// Summary description for ucGeneralSort.
	/// </summary>
	public class ucGeneralSort : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.ListBox lbSort;
		private System.Windows.Forms.ListBox lbSortFields;
		private System.Windows.Forms.Button btnNew;
		private System.Windows.Forms.GroupBox grpSortOrder;
		private System.Windows.Forms.RadioButton rbAscending;
		private System.Windows.Forms.RadioButton rbDescending;
		private System.Windows.Forms.Button btnRemove;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private	ViewFilter					filter	= null;
		private ViewFilter.GeneralSortRow	view = null;

		public ucGeneralSort()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		public ViewFilter	Filter
		{
			get
			{
				return this.filter;
			}
			set
			{
				this.filter = value;
//				UpdateData();
			}
		}

		public ViewFilter.GeneralSortRow View
		{
			get
			{
				return this.view;
			}

			set
			{
				this.view = value;
			}
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ucGeneralSort));
			this.lbSort = new System.Windows.Forms.ListBox();
			this.lbSortFields = new System.Windows.Forms.ListBox();
			this.btnNew = new System.Windows.Forms.Button();
			this.grpSortOrder = new System.Windows.Forms.GroupBox();
			this.rbDescending = new System.Windows.Forms.RadioButton();
			this.rbAscending = new System.Windows.Forms.RadioButton();
			this.btnRemove = new System.Windows.Forms.Button();
			this.grpSortOrder.SuspendLayout();
			this.SuspendLayout();
			// 
			// lbSort
			// 
			this.lbSort.AccessibleDescription = resources.GetString("lbSort.AccessibleDescription");
			this.lbSort.AccessibleName = resources.GetString("lbSort.AccessibleName");
			this.lbSort.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lbSort.Anchor")));
			this.lbSort.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("lbSort.BackgroundImage")));
			this.lbSort.ColumnWidth = ((int)(resources.GetObject("lbSort.ColumnWidth")));
			this.lbSort.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lbSort.Dock")));
			this.lbSort.Enabled = ((bool)(resources.GetObject("lbSort.Enabled")));
			this.lbSort.Font = ((System.Drawing.Font)(resources.GetObject("lbSort.Font")));
			this.lbSort.HorizontalExtent = ((int)(resources.GetObject("lbSort.HorizontalExtent")));
			this.lbSort.HorizontalScrollbar = ((bool)(resources.GetObject("lbSort.HorizontalScrollbar")));
			this.lbSort.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lbSort.ImeMode")));
			this.lbSort.IntegralHeight = ((bool)(resources.GetObject("lbSort.IntegralHeight")));
			this.lbSort.ItemHeight = ((int)(resources.GetObject("lbSort.ItemHeight")));
			this.lbSort.Location = ((System.Drawing.Point)(resources.GetObject("lbSort.Location")));
			this.lbSort.Name = "lbSort";
			this.lbSort.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lbSort.RightToLeft")));
			this.lbSort.ScrollAlwaysVisible = ((bool)(resources.GetObject("lbSort.ScrollAlwaysVisible")));
			this.lbSort.Size = ((System.Drawing.Size)(resources.GetObject("lbSort.Size")));
			this.lbSort.TabIndex = ((int)(resources.GetObject("lbSort.TabIndex")));
			this.lbSort.Visible = ((bool)(resources.GetObject("lbSort.Visible")));
			// 
			// lbSortFields
			// 
			this.lbSortFields.AccessibleDescription = resources.GetString("lbSortFields.AccessibleDescription");
			this.lbSortFields.AccessibleName = resources.GetString("lbSortFields.AccessibleName");
			this.lbSortFields.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lbSortFields.Anchor")));
			this.lbSortFields.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("lbSortFields.BackgroundImage")));
			this.lbSortFields.ColumnWidth = ((int)(resources.GetObject("lbSortFields.ColumnWidth")));
			this.lbSortFields.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lbSortFields.Dock")));
			this.lbSortFields.Enabled = ((bool)(resources.GetObject("lbSortFields.Enabled")));
			this.lbSortFields.Font = ((System.Drawing.Font)(resources.GetObject("lbSortFields.Font")));
			this.lbSortFields.HorizontalExtent = ((int)(resources.GetObject("lbSortFields.HorizontalExtent")));
			this.lbSortFields.HorizontalScrollbar = ((bool)(resources.GetObject("lbSortFields.HorizontalScrollbar")));
			this.lbSortFields.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lbSortFields.ImeMode")));
			this.lbSortFields.IntegralHeight = ((bool)(resources.GetObject("lbSortFields.IntegralHeight")));
			this.lbSortFields.ItemHeight = ((int)(resources.GetObject("lbSortFields.ItemHeight")));
			this.lbSortFields.Location = ((System.Drawing.Point)(resources.GetObject("lbSortFields.Location")));
			this.lbSortFields.Name = "lbSortFields";
			this.lbSortFields.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lbSortFields.RightToLeft")));
			this.lbSortFields.ScrollAlwaysVisible = ((bool)(resources.GetObject("lbSortFields.ScrollAlwaysVisible")));
			this.lbSortFields.Size = ((System.Drawing.Size)(resources.GetObject("lbSortFields.Size")));
			this.lbSortFields.TabIndex = ((int)(resources.GetObject("lbSortFields.TabIndex")));
			this.lbSortFields.Visible = ((bool)(resources.GetObject("lbSortFields.Visible")));
			// 
			// btnNew
			// 
			this.btnNew.AccessibleDescription = resources.GetString("btnNew.AccessibleDescription");
			this.btnNew.AccessibleName = resources.GetString("btnNew.AccessibleName");
			this.btnNew.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnNew.Anchor")));
			this.btnNew.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNew.BackgroundImage")));
			this.btnNew.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnNew.Dock")));
			this.btnNew.Enabled = ((bool)(resources.GetObject("btnNew.Enabled")));
			this.btnNew.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnNew.FlatStyle")));
			this.btnNew.Font = ((System.Drawing.Font)(resources.GetObject("btnNew.Font")));
			this.btnNew.Image = ((System.Drawing.Image)(resources.GetObject("btnNew.Image")));
			this.btnNew.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnNew.ImageAlign")));
			this.btnNew.ImageIndex = ((int)(resources.GetObject("btnNew.ImageIndex")));
			this.btnNew.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnNew.ImeMode")));
			this.btnNew.Location = ((System.Drawing.Point)(resources.GetObject("btnNew.Location")));
			this.btnNew.Name = "btnNew";
			this.btnNew.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnNew.RightToLeft")));
			this.btnNew.Size = ((System.Drawing.Size)(resources.GetObject("btnNew.Size")));
			this.btnNew.TabIndex = ((int)(resources.GetObject("btnNew.TabIndex")));
			this.btnNew.Text = resources.GetString("btnNew.Text");
			this.btnNew.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnNew.TextAlign")));
			this.btnNew.Visible = ((bool)(resources.GetObject("btnNew.Visible")));
			// 
			// grpSortOrder
			// 
			this.grpSortOrder.AccessibleDescription = resources.GetString("grpSortOrder.AccessibleDescription");
			this.grpSortOrder.AccessibleName = resources.GetString("grpSortOrder.AccessibleName");
			this.grpSortOrder.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("grpSortOrder.Anchor")));
			this.grpSortOrder.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("grpSortOrder.BackgroundImage")));
			this.grpSortOrder.Controls.Add(this.rbDescending);
			this.grpSortOrder.Controls.Add(this.rbAscending);
			this.grpSortOrder.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("grpSortOrder.Dock")));
			this.grpSortOrder.Enabled = ((bool)(resources.GetObject("grpSortOrder.Enabled")));
			this.grpSortOrder.Font = ((System.Drawing.Font)(resources.GetObject("grpSortOrder.Font")));
			this.grpSortOrder.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("grpSortOrder.ImeMode")));
			this.grpSortOrder.Location = ((System.Drawing.Point)(resources.GetObject("grpSortOrder.Location")));
			this.grpSortOrder.Name = "grpSortOrder";
			this.grpSortOrder.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("grpSortOrder.RightToLeft")));
			this.grpSortOrder.Size = ((System.Drawing.Size)(resources.GetObject("grpSortOrder.Size")));
			this.grpSortOrder.TabIndex = ((int)(resources.GetObject("grpSortOrder.TabIndex")));
			this.grpSortOrder.TabStop = false;
			this.grpSortOrder.Text = resources.GetString("grpSortOrder.Text");
			this.grpSortOrder.Visible = ((bool)(resources.GetObject("grpSortOrder.Visible")));
			// 
			// rbDescending
			// 
			this.rbDescending.AccessibleDescription = resources.GetString("rbDescending.AccessibleDescription");
			this.rbDescending.AccessibleName = resources.GetString("rbDescending.AccessibleName");
			this.rbDescending.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("rbDescending.Anchor")));
			this.rbDescending.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("rbDescending.Appearance")));
			this.rbDescending.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("rbDescending.BackgroundImage")));
			this.rbDescending.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("rbDescending.CheckAlign")));
			this.rbDescending.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("rbDescending.Dock")));
			this.rbDescending.Enabled = ((bool)(resources.GetObject("rbDescending.Enabled")));
			this.rbDescending.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("rbDescending.FlatStyle")));
			this.rbDescending.Font = ((System.Drawing.Font)(resources.GetObject("rbDescending.Font")));
			this.rbDescending.Image = ((System.Drawing.Image)(resources.GetObject("rbDescending.Image")));
			this.rbDescending.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("rbDescending.ImageAlign")));
			this.rbDescending.ImageIndex = ((int)(resources.GetObject("rbDescending.ImageIndex")));
			this.rbDescending.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("rbDescending.ImeMode")));
			this.rbDescending.Location = ((System.Drawing.Point)(resources.GetObject("rbDescending.Location")));
			this.rbDescending.Name = "rbDescending";
			this.rbDescending.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("rbDescending.RightToLeft")));
			this.rbDescending.Size = ((System.Drawing.Size)(resources.GetObject("rbDescending.Size")));
			this.rbDescending.TabIndex = ((int)(resources.GetObject("rbDescending.TabIndex")));
			this.rbDescending.Text = resources.GetString("rbDescending.Text");
			this.rbDescending.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("rbDescending.TextAlign")));
			this.rbDescending.Visible = ((bool)(resources.GetObject("rbDescending.Visible")));
			// 
			// rbAscending
			// 
			this.rbAscending.AccessibleDescription = resources.GetString("rbAscending.AccessibleDescription");
			this.rbAscending.AccessibleName = resources.GetString("rbAscending.AccessibleName");
			this.rbAscending.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("rbAscending.Anchor")));
			this.rbAscending.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("rbAscending.Appearance")));
			this.rbAscending.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("rbAscending.BackgroundImage")));
			this.rbAscending.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("rbAscending.CheckAlign")));
			this.rbAscending.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("rbAscending.Dock")));
			this.rbAscending.Enabled = ((bool)(resources.GetObject("rbAscending.Enabled")));
			this.rbAscending.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("rbAscending.FlatStyle")));
			this.rbAscending.Font = ((System.Drawing.Font)(resources.GetObject("rbAscending.Font")));
			this.rbAscending.Image = ((System.Drawing.Image)(resources.GetObject("rbAscending.Image")));
			this.rbAscending.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("rbAscending.ImageAlign")));
			this.rbAscending.ImageIndex = ((int)(resources.GetObject("rbAscending.ImageIndex")));
			this.rbAscending.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("rbAscending.ImeMode")));
			this.rbAscending.Location = ((System.Drawing.Point)(resources.GetObject("rbAscending.Location")));
			this.rbAscending.Name = "rbAscending";
			this.rbAscending.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("rbAscending.RightToLeft")));
			this.rbAscending.Size = ((System.Drawing.Size)(resources.GetObject("rbAscending.Size")));
			this.rbAscending.TabIndex = ((int)(resources.GetObject("rbAscending.TabIndex")));
			this.rbAscending.Text = resources.GetString("rbAscending.Text");
			this.rbAscending.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("rbAscending.TextAlign")));
			this.rbAscending.Visible = ((bool)(resources.GetObject("rbAscending.Visible")));
			// 
			// btnRemove
			// 
			this.btnRemove.AccessibleDescription = resources.GetString("btnRemove.AccessibleDescription");
			this.btnRemove.AccessibleName = resources.GetString("btnRemove.AccessibleName");
			this.btnRemove.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnRemove.Anchor")));
			this.btnRemove.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRemove.BackgroundImage")));
			this.btnRemove.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnRemove.Dock")));
			this.btnRemove.Enabled = ((bool)(resources.GetObject("btnRemove.Enabled")));
			this.btnRemove.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnRemove.FlatStyle")));
			this.btnRemove.Font = ((System.Drawing.Font)(resources.GetObject("btnRemove.Font")));
			this.btnRemove.Image = ((System.Drawing.Image)(resources.GetObject("btnRemove.Image")));
			this.btnRemove.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnRemove.ImageAlign")));
			this.btnRemove.ImageIndex = ((int)(resources.GetObject("btnRemove.ImageIndex")));
			this.btnRemove.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnRemove.ImeMode")));
			this.btnRemove.Location = ((System.Drawing.Point)(resources.GetObject("btnRemove.Location")));
			this.btnRemove.Name = "btnRemove";
			this.btnRemove.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnRemove.RightToLeft")));
			this.btnRemove.Size = ((System.Drawing.Size)(resources.GetObject("btnRemove.Size")));
			this.btnRemove.TabIndex = ((int)(resources.GetObject("btnRemove.TabIndex")));
			this.btnRemove.Text = resources.GetString("btnRemove.Text");
			this.btnRemove.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnRemove.TextAlign")));
			this.btnRemove.Visible = ((bool)(resources.GetObject("btnRemove.Visible")));
			// 
			// ucGeneralSort
			// 
			this.AccessibleDescription = resources.GetString("$this.AccessibleDescription");
			this.AccessibleName = resources.GetString("$this.AccessibleName");
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMargin")));
			this.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMinSize")));
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.Controls.Add(this.btnRemove);
			this.Controls.Add(this.grpSortOrder);
			this.Controls.Add(this.btnNew);
			this.Controls.Add(this.lbSortFields);
			this.Controls.Add(this.lbSort);
			this.Enabled = ((bool)(resources.GetObject("$this.Enabled")));
			this.Font = ((System.Drawing.Font)(resources.GetObject("$this.Font")));
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.Name = "ucGeneralSort";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.Size = ((System.Drawing.Size)(resources.GetObject("$this.Size")));
			this.grpSortOrder.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion
	}
}
