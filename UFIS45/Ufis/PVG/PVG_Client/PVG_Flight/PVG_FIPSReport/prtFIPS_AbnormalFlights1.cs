using System;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using System.Drawing;
using Ufis.Utils;

namespace FIPS_Reports
{
	public class prtFIPS_AbnormalFlights : ActiveReport
	{
		#region _My Members
		/// <summary>
		/// Stores the main header.
		/// </summary>
		private string _header1 = "";
		/// <summary>
		/// Stores the sub header string
		/// </summary>
		private string _header2 = "";
		/// <summary>
		/// Stores the data types for each column.
		/// This might be necessary for type convertions.
		/// </summary>
		private string [] arrTypes;
		/// <summary>
		/// The reference to the tab control, which is
		/// a parameter of the constructor.
		/// </summary>
		private AxTABLib.AxTAB tabResult;
		/// <summary>
		/// Stores the current line number. This is necessary
		/// to identif when EOF in the xxx_fetchdata method must
		/// return end of report. The current value is checked 
		/// against the Total line count of the tab control.
		/// </summary>
		private int currLine = 0;
		/// <summary>
		/// Stores the header text in an array. This is filled
		/// in the constructor.
		/// </summary>
		string [] arrHeader;
		/// <summary>
		/// The length list from the tab control. This is generated
		/// in the constructor.
		/// </summary>
		string [] arrLens;  
		/// <summary>
		/// Stores the logical field list of the tab control. This 
		/// names are used for the dynamically generated report controls.
		/// </summary>
		string [] arrNames; 
		/// <summary>
		/// The font size for the detail section.
		/// </summary>
		int imFontSize = 10;
		/// <summary>
		/// Defines whether a txtbox can grow or not
		/// </summary>
		bool bmFieldCanGrow = true;
		/// <summary>
		/// Property, to allow/forbid gorwing of a textbox according to the content.
		/// </summary>
		bool blFirst = true;
		public bool TextBoxCanGrow
		{
			get{return bmFieldCanGrow;}
			set{bmFieldCanGrow = value;}
		}

		float maxHeight = 0F;

		#endregion _My Members

		/// <summary>
		/// Constructor:
		/// The tab control is a parameter. This tab is used to generate
		/// the report dynamically.
		/// </summary>
		/// <param name="pTab">The tab control with settings and data</param>
		/// <param name="header1">The main header string</param>
		/// <param name="header2">Additional header string</param>
		/// <param name="fieldtypes">The types for the fields. This might be
		/// necessary to convert data to the respective format.
		/// </param>
		public prtFIPS_AbnormalFlights(AxTABLib.AxTAB pTab, string header1, string header2, string fieldtypes, int fontSize)
		{
			imFontSize = fontSize;
			_header1 = header1;
			_header2 = header2;
			arrTypes = fieldtypes.Split(',');
			tabResult = pTab;
			arrHeader = tabResult.HeaderString.Split(',');
			arrLens = tabResult.HeaderLengthString.Split(',');
			arrNames = tabResult.LogicalFieldList.Split(',');
			InitializeReport();
		}
		/// <summary>
		/// Called when the report starts. This function must call the 
		/// methods, which generate the header-, detail- and footer sections.
		/// </summary>
		/// <param name="sender">Originator of the call. This is the report itself.</param>
		/// <param name="eArgs">Event arguments.</param>
		private void prtFIPS_AbnormalFlights_ReportStart(object sender, System.EventArgs eArgs)
		{
			DateTime d		= DateTime.Now;
			txtDate.Text	= d.ToString();
			lblHeader1.Text = _header1;
			lblHeader2.Text = _header2;

			//*** Read the logo if exists and set it
			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			string strPrintLogoPath = myIni.IniReadValue("GLOBAL", "PRINTLOGO");
			Image myImage = null;
			try
			{
				if(strPrintLogoPath != "")
				{
					myImage = new Bitmap(strPrintLogoPath);
					Picture1.Image = myImage;
				}
			}
			catch(Exception)
			{
			}

//			MakeReportHeader();
			MakeReportDetail();
		}
		/// <summary>
		/// Generates the Detail section of the report according
		/// to the tab control.
		/// </summary>
		private void MakeReportDetail()
		{
			float ilX = 0;
			float ilCurrW = 0f;
			DataDynamics.ActiveReports.Border bd = new Border();
			System.Drawing.Font fnt = new System.Drawing.Font("Arial", imFontSize, System.Drawing.FontStyle.Regular);

			int size = this.InchToPix(this.CustomControl1.Width) / arrHeader.Length;
			ilX = this.CustomControl1.Left;

			for (int i = 0; i < arrHeader.Length; i++)
			{
				arrLens[i] = size.ToString();
			}

			for(int i = 0; i < arrHeader.Length; i++)
			{
				if(Convert.ToInt32(arrLens[i]) > 0)
				{
					ilCurrW = PixToInch (Convert.ToInt32(arrLens[i]));
					DataDynamics.ActiveReports.TextBox  txtBox = new TextBox();
					DataDynamics.ActiveReports.Line olLine = new Line();
					txtBox.Font = fnt;
					txtBox.Top = 0;
					txtBox.Height = 0.2f;
					txtBox.Left = ilX;
					txtBox.Width = ilCurrW;
					txtBox.CanGrow = bmFieldCanGrow;
					olLine.Y1 = 0;
					olLine.Y2 = 0.2f;
					olLine.X1 = ilX;
					olLine.X2 = ilX;
					olLine.Name = "lin" + arrNames[i];
					ilX += ilCurrW;
					//					txtBox.Border.BottomStyle = BorderLineStyle.Solid;
					//					txtBox.Border.LeftStyle = BorderLineStyle.Solid;
					//					txtBox.Border.TopStyle  = BorderLineStyle.Solid;
					//					txtBox.Border.RightStyle = BorderLineStyle.Solid;
					txtBox.Name = "txt" + arrNames[i];
					txtBox.Text = "txt" + arrNames[i];
					Detail.Controls.Add(txtBox);
					Detail.Controls.Add(olLine);
				}
			}


			DataDynamics.ActiveReports.Line olLineTop = new Line();
			olLineTop.X1 = this.CustomControl1.Left;
			olLineTop.X2 = ilX;//+ilCurrW;
			olLineTop.Y1 = 0;
			olLineTop.Y2 = 0;
			olLineTop.Name = "linTop";
			Detail.Controls.Add(olLineTop);

			DataDynamics.ActiveReports.Line olLineBottom = new Line();
			olLineBottom.X1 = this.CustomControl1.Left;
			olLineBottom.X2 = ilX;//+ilCurrW;
			olLineBottom.Y1 = 0.2f;
			olLineBottom.Y2 = 0.2f;
			olLineBottom.Name = "linBottom";
			Detail.Controls.Add(olLineBottom);
			//Last vertical separator on the right end to finish the line
			DataDynamics.ActiveReports.Line olLastVertical = new Line();
			olLastVertical.Y1 = 0;
			olLastVertical.Y2 = 0.2f;//+ilCurrW;
			olLastVertical.X1 = ilX;
			olLastVertical.X2 = ilX;
			olLastVertical.Name = "linLast";
			Detail.Controls.Add(olLastVertical);
		}
		/// <summary>
		/// Generates the report Header from the tab
		/// </summary>
		private void MakeReportHeader()
		{
			float ilX = 0;
			//DataDynamics.ActiveReports.Border bd = new Border();
			System.Drawing.Font fnt = new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Bold);
			for(int i = 0; i < arrHeader.Length; i++)
			{
				float ilCurrW = PixToInch (Convert.ToInt32(arrLens[i]));
				if(ilCurrW > 0)
				{
					Label lbl = new Label();
					lbl.Font = fnt;
					lbl.Top = 1;
					lbl.Height = 0.2f;
					lbl.Left = ilX;
					lbl.Width = ilCurrW;
					lbl.Alignment = TextAlignment.Center;
					ilX += ilCurrW;
					lbl.Border.BottomStyle = BorderLineStyle.Solid;
					lbl.Border.LeftStyle = BorderLineStyle.Solid;
					lbl.Border.TopStyle  = BorderLineStyle.Solid;
					lbl.Border.RightStyle = BorderLineStyle.Solid;
					lbl.Name = "lbl" + arrNames[i];
					lbl.Text = arrHeader[i];
					lbl.BackColor = Color.LightGray;
					PageHeader.Controls.Add(lbl);
				}
			}
		}
		/// <summary>
		/// Converts a pixel integer into inches
		/// </summary>
		/// <param name="pix">Input parameter. The number of pixels.</param>
		/// <returns>The converted pixel in inches.</returns>
		private float PixToInch(int pix)
		{
			float fRet = 0f;
			float twips = 0f;
			//First we need twips
			twips = pix * 15f; //Normally it's 15 but 16 meets better!
			//1440 twips = 1 inch
			fRet = (float)(twips / 1440f);
			return fRet;
		}

		/// <summary>
		/// Converts inches into a pixel integer 
		/// </summary>
		/// <param name="pix">Input parameter. The number of pixels.</param>
		/// <returns>The converted pixel in inches.</returns>
		private int InchToPix(float inches)
		{
			float fRet = 0f;
			fRet = (inches * 1440f / 15f);
			return (int)fRet;
		}

		private void prtFIPS_AbnormalFlights_FetchData(object sender, DataDynamics.ActiveReports.ActiveReport.FetchEventArgs eArgs)
		{
			if(tabResult.GetLineCount() == 0)
			{
				eArgs.EOF = true;
				return;
			}

			if(currLine < tabResult.GetLineCount())
			{
				TextBox txt;
				string [] arrValues = tabResult.GetLineValues(currLine).Split(',');
				for(int i = 0; i < arrNames.Length; i++)
				{
					if(Convert.ToInt32(arrLens[i]) > 0)
					{
						string strName = "txt" + arrNames[i];
						txt = (TextBox)Detail.Controls[strName];
						txt.Text = " " + arrValues[i];
						if(txt.Height > maxHeight)
							maxHeight = txt.Height;
					}
				}

				currLine++;
				eArgs.EOF = false;
			}
			else
			{
				eArgs.EOF = true;
			}
		}



		private void Detail_BeforePrint(object sender, System.EventArgs eArgs)
		{
			Line lin;
			for(int i = 0; i < arrNames.Length; i++)
			{
				if(Convert.ToInt32(arrLens[i]) > 0)
				{
					string strName = "lin" + arrNames[i];
					lin = (Line)Detail.Controls[strName];
					lin.Y2 = maxHeight;
				}
			}
			lin = (Line)Detail.Controls["linBottom"];
			lin.Y1 = maxHeight;
			lin.Y2 = maxHeight;
			lin = (Line)Detail.Controls["linLast"];
			lin.Y2 = maxHeight;
		}

		#region ActiveReports Designer generated code
		private PageHeader PageHeader = null;
		private Picture Picture1 = null;
		private Label lblHeader1 = null;
		private Label lblHeader2 = null;
		private CustomControl CustomControl1 = null;
		private Detail Detail = null;
		private PageFooter PageFooter = null;
		private TextBox txtDate = null;
		private Label Label34 = null;
		private Label Label33 = null;
		private TextBox TextBox2 = null;
		private TextBox TextBox3 = null;
		private Label Label32 = null;
		public void InitializeReport()
		{
			this.LoadLayout(this.GetType(), "FIPS_Reports.prtFIPS_AbnormalFlights.rpx");
			this.PageHeader = ((DataDynamics.ActiveReports.PageHeader)(this.Sections["PageHeader"]));
			this.Detail = ((DataDynamics.ActiveReports.Detail)(this.Sections["Detail"]));
			this.PageFooter = ((DataDynamics.ActiveReports.PageFooter)(this.Sections["PageFooter"]));
			this.Picture1 = ((DataDynamics.ActiveReports.Picture)(this.PageHeader.Controls[0]));
			this.lblHeader1 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[1]));
			this.lblHeader2 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[2]));
			this.CustomControl1 = ((DataDynamics.ActiveReports.CustomControl)(this.PageHeader.Controls[3]));
			this.txtDate = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[0]));
			this.Label34 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[1]));
			this.Label33 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[2]));
			this.TextBox2 = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[3]));
			this.TextBox3 = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[4]));
			this.Label32 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[5]));
			// Attach Report Events
			this.ReportStart += new System.EventHandler(this.prtFIPS_AbnormalFlights_ReportStart);
			this.FetchData += new DataDynamics.ActiveReports.ActiveReport.FetchEventHandler(this.prtFIPS_AbnormalFlights_FetchData);
			this.Detail.BeforePrint += new System.EventHandler(this.Detail_BeforePrint);
		}

		#endregion
	}
}
