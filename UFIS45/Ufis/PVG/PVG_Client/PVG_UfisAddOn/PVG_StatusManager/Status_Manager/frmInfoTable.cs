using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace BirdView
{
	/// <summary>
	/// Summary description for frmInfoTable.
	/// </summary>
	public class frmInfoTable : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Panel panelHeader;
		private System.Windows.Forms.Panel panelBody;
		public AxTABLib.AxTAB tabInfo;
		public System.Windows.Forms.Label lblCaption;
		private System.Windows.Forms.Panel panelArrival;
		private System.Windows.Forms.Panel panelDeparture;
		public AxTABLib.AxTAB tabDeparture;
		private System.Windows.Forms.Panel panel2;
		public System.Windows.Forms.Label lblDeparture;
		public System.Windows.Forms.TextBox txtArrival;
		public System.Windows.Forms.TextBox txtDeparture;
		public System.Windows.Forms.TextBox txtDepLastAction;
		public System.Windows.Forms.TextBox txtArrLastAction;
		private AxTABLib.AxTAB tabStatusForFlight;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmInfoTable()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmInfoTable));
			this.panelHeader = new System.Windows.Forms.Panel();
			this.lblCaption = new System.Windows.Forms.Label();
			this.panelBody = new System.Windows.Forms.Panel();
			this.panelDeparture = new System.Windows.Forms.Panel();
			this.tabDeparture = new AxTABLib.AxTAB();
			this.panel2 = new System.Windows.Forms.Panel();
			this.lblDeparture = new System.Windows.Forms.Label();
			this.panelArrival = new System.Windows.Forms.Panel();
			this.tabInfo = new AxTABLib.AxTAB();
			this.txtArrival = new System.Windows.Forms.TextBox();
			this.txtDeparture = new System.Windows.Forms.TextBox();
			this.txtDepLastAction = new System.Windows.Forms.TextBox();
			this.txtArrLastAction = new System.Windows.Forms.TextBox();
			this.tabStatusForFlight = new AxTABLib.AxTAB();
			this.panelHeader.SuspendLayout();
			this.panelBody.SuspendLayout();
			this.panelDeparture.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabDeparture)).BeginInit();
			this.panel2.SuspendLayout();
			this.panelArrival.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabInfo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabStatusForFlight)).BeginInit();
			this.SuspendLayout();
			// 
			// panelHeader
			// 
			this.panelHeader.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(224)), ((System.Byte)(224)), ((System.Byte)(224)));
			this.panelHeader.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelHeader.Controls.Add(this.lblCaption);
			this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelHeader.Location = new System.Drawing.Point(0, 0);
			this.panelHeader.Name = "panelHeader";
			this.panelHeader.Size = new System.Drawing.Size(248, 28);
			this.panelHeader.TabIndex = 1;
			// 
			// lblCaption
			// 
			this.lblCaption.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblCaption.ForeColor = System.Drawing.Color.Blue;
			this.lblCaption.Location = new System.Drawing.Point(0, 0);
			this.lblCaption.Name = "lblCaption";
			this.lblCaption.Size = new System.Drawing.Size(244, 24);
			this.lblCaption.TabIndex = 0;
			// 
			// panelBody
			// 
			this.panelBody.Controls.Add(this.panelDeparture);
			this.panelBody.Controls.Add(this.panelArrival);
			this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelBody.Location = new System.Drawing.Point(0, 0);
			this.panelBody.Name = "panelBody";
			this.panelBody.Size = new System.Drawing.Size(496, 336);
			this.panelBody.TabIndex = 2;
			// 
			// panelDeparture
			// 
			this.panelDeparture.Controls.Add(this.tabStatusForFlight);
			this.panelDeparture.Controls.Add(this.txtDepLastAction);
			this.panelDeparture.Controls.Add(this.txtDeparture);
			this.panelDeparture.Controls.Add(this.tabDeparture);
			this.panelDeparture.Controls.Add(this.panel2);
			this.panelDeparture.Dock = System.Windows.Forms.DockStyle.Left;
			this.panelDeparture.Location = new System.Drawing.Point(248, 0);
			this.panelDeparture.Name = "panelDeparture";
			this.panelDeparture.Size = new System.Drawing.Size(248, 336);
			this.panelDeparture.TabIndex = 2;
			// 
			// tabDeparture
			// 
			this.tabDeparture.ContainingControl = this;
			this.tabDeparture.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabDeparture.Location = new System.Drawing.Point(0, 28);
			this.tabDeparture.Name = "tabDeparture";
			this.tabDeparture.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabDeparture.OcxState")));
			this.tabDeparture.Size = new System.Drawing.Size(248, 308);
			this.tabDeparture.TabIndex = 0;
			// 
			// panel2
			// 
			this.panel2.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(224)), ((System.Byte)(224)), ((System.Byte)(224)));
			this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel2.Controls.Add(this.lblDeparture);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(248, 28);
			this.panel2.TabIndex = 1;
			// 
			// lblDeparture
			// 
			this.lblDeparture.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblDeparture.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblDeparture.ForeColor = System.Drawing.Color.Blue;
			this.lblDeparture.Location = new System.Drawing.Point(0, 0);
			this.lblDeparture.Name = "lblDeparture";
			this.lblDeparture.Size = new System.Drawing.Size(244, 24);
			this.lblDeparture.TabIndex = 0;
			// 
			// panelArrival
			// 
			this.panelArrival.Controls.Add(this.txtArrLastAction);
			this.panelArrival.Controls.Add(this.txtArrival);
			this.panelArrival.Controls.Add(this.tabInfo);
			this.panelArrival.Controls.Add(this.panelHeader);
			this.panelArrival.Dock = System.Windows.Forms.DockStyle.Left;
			this.panelArrival.Location = new System.Drawing.Point(0, 0);
			this.panelArrival.Name = "panelArrival";
			this.panelArrival.Size = new System.Drawing.Size(248, 336);
			this.panelArrival.TabIndex = 1;
			// 
			// tabInfo
			// 
			this.tabInfo.ContainingControl = this;
			this.tabInfo.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabInfo.Location = new System.Drawing.Point(0, 28);
			this.tabInfo.Name = "tabInfo";
			this.tabInfo.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabInfo.OcxState")));
			this.tabInfo.Size = new System.Drawing.Size(248, 308);
			this.tabInfo.TabIndex = 0;
			// 
			// txtArrival
			// 
			this.txtArrival.AcceptsReturn = true;
			this.txtArrival.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtArrival.ForeColor = System.Drawing.Color.Red;
			this.txtArrival.Location = new System.Drawing.Point(0, 204);
			this.txtArrival.Multiline = true;
			this.txtArrival.Name = "txtArrival";
			this.txtArrival.Size = new System.Drawing.Size(248, 132);
			this.txtArrival.TabIndex = 2;
			this.txtArrival.Text = "";
			// 
			// txtDeparture
			// 
			this.txtDeparture.AcceptsReturn = true;
			this.txtDeparture.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtDeparture.ForeColor = System.Drawing.Color.Red;
			this.txtDeparture.Location = new System.Drawing.Point(0, 204);
			this.txtDeparture.Multiline = true;
			this.txtDeparture.Name = "txtDeparture";
			this.txtDeparture.Size = new System.Drawing.Size(248, 132);
			this.txtDeparture.TabIndex = 3;
			this.txtDeparture.Text = "";
			// 
			// txtDepLastAction
			// 
			this.txtDepLastAction.AcceptsReturn = true;
			this.txtDepLastAction.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtDepLastAction.ForeColor = System.Drawing.Color.Blue;
			this.txtDepLastAction.Location = new System.Drawing.Point(0, 172);
			this.txtDepLastAction.Multiline = true;
			this.txtDepLastAction.Name = "txtDepLastAction";
			this.txtDepLastAction.Size = new System.Drawing.Size(248, 32);
			this.txtDepLastAction.TabIndex = 4;
			this.txtDepLastAction.Text = "";
			// 
			// txtArrLastAction
			// 
			this.txtArrLastAction.AcceptsReturn = true;
			this.txtArrLastAction.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtArrLastAction.ForeColor = System.Drawing.Color.Blue;
			this.txtArrLastAction.Location = new System.Drawing.Point(0, 172);
			this.txtArrLastAction.Multiline = true;
			this.txtArrLastAction.Name = "txtArrLastAction";
			this.txtArrLastAction.Size = new System.Drawing.Size(248, 32);
			this.txtArrLastAction.TabIndex = 5;
			this.txtArrLastAction.Text = "";
			// 
			// tabStatusForFlight
			// 
			this.tabStatusForFlight.ContainingControl = this;
			this.tabStatusForFlight.Location = new System.Drawing.Point(92, 4);
			this.tabStatusForFlight.Name = "tabStatusForFlight";
			this.tabStatusForFlight.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabStatusForFlight.OcxState")));
			this.tabStatusForFlight.Size = new System.Drawing.Size(128, 28);
			this.tabStatusForFlight.TabIndex = 5;
			this.tabStatusForFlight.Visible = false;
			// 
			// frmInfoTable
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(496, 336);
			this.Controls.Add(this.panelBody);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "frmInfoTable";
			this.Opacity = 0.8;
			this.Text = "Info Table <X>";
			this.Load += new System.EventHandler(this.frmInfoTable_Load);
			this.panelHeader.ResumeLayout(false);
			this.panelBody.ResumeLayout(false);
			this.panelDeparture.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabDeparture)).EndInit();
			this.panel2.ResumeLayout(false);
			this.panelArrival.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabInfo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabStatusForFlight)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void frmInfoTable_Load(object sender, System.EventArgs e)
		{
			tabInfo.ResetContent();
			tabInfo.HeaderString = "Item,Value";
			tabInfo.HeaderLengthString = "150,100";
			tabInfo.LogicalFieldList = tabInfo.HeaderString;
			tabInfo.LifeStyle = true;
			tabInfo.FontSize = 12;
			tabInfo.LineHeight = 14;
			tabInfo.SetTabFontBold(true);

			tabDeparture.ResetContent();
			tabDeparture.HeaderString = "Item,Value";
			tabDeparture.HeaderLengthString = "150,100";
			tabDeparture.LogicalFieldList = tabInfo.HeaderString;
			tabDeparture.LifeStyle = true;
			tabDeparture.FontSize = 12;
			tabDeparture.LineHeight = 14;
			tabDeparture.SetTabFontBold(true);
		}
	}
}
