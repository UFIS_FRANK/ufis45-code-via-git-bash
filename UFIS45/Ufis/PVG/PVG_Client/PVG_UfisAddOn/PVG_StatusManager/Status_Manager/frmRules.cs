using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Ufis.Utils;
using Ufis.Data;
using DataDynamics.ActiveReports;
using Microsoft.Win32;

namespace Status_Manager
{
	/// <summary>
	/// Summary description for frmRules.
	/// </summary>
	public class frmRules : System.Windows.Forms.Form
	{
		#region ----- myMembers

		string [] myURNOFields;
		string [] myVALUEFields;
		string [] myLookupTabs;
		string [] myLookupFields;
		string myMode = "NEW";
		IRow currRow = null;
		IRow currDBRow = null;
		IRow oldRow = null;
		IDatabase myDB = null;
		string [] myRETY_DB;  //RETY values, which have to be stored in DB   [STATUSMANAGER]RETY_DB
		string [] myRETY_GUI; //RETY values to prepare for the user Ceda.ini [STATUSMANAGER]RETY_GUI 
		string [] myRTIM_DB;  //RTIM values, which have to be stored in DB   [STATUSMANAGER]RTIM_DB
		string [] myRTIM_GUI; //RTIM values to prepare for the user Ceda.ini [STATUSMANAGER]RTIM_GUI 
		string [] myAFTF_DB_ARR;  //AFTF Arrival values, which have to be stored in DB   [STATUSMANAGER]AFTF_DB
		string [] myAFTF_GUI_ARR; //AFTF Arrival values to prepare for the user Ceda.ini   [STATUSMANAGER]AFTF_GUI
		string [] myAFTF_DB_DEP;  //AFTF Departure values, which have to be stored in DB   [STATUSMANAGER]AFTF_DB
		string [] myAFTF_GUI_DEP; //AFTF Departure values to prepare for the user Ceda.ini   [STATUSMANAGER]AFTF_GUI

		bool PredecessorVisible = true;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.Button btnRecalcFlights;
		private System.Windows.Forms.Button btnClose; //If predecessor is visible
		int myCurrEditColumn = -1;
		DateTime oldValidFrom;
		DateTime oldValidTo;

		#endregion ----- myMembers

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Button btnOpen;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Label lblEvent;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label lblACType;
		private System.Windows.Forms.TextBox txtACTValue;
		private System.Windows.Forms.TextBox txtACTUrno;
		private System.Windows.Forms.TextBox txtACTGroupUrno;
		private System.Windows.Forms.TextBox txtACTGroupValue;
		private System.Windows.Forms.TextBox txtAPTGroupUrno;
		private System.Windows.Forms.TextBox txtAPTGroupValue;
		private System.Windows.Forms.Label lblAirportGroup;
		private System.Windows.Forms.TextBox txtAPTUrno;
		private System.Windows.Forms.TextBox txtAPTValue;
		private System.Windows.Forms.Label lblAirport;
		private System.Windows.Forms.Label lblRuleName;
		private System.Windows.Forms.TextBox txtRuleName;
		private System.Windows.Forms.TextBox txtALTGroupUrno;
		private System.Windows.Forms.TextBox txtALTGroupValue;
		private System.Windows.Forms.TextBox txtALTUrno;
		private System.Windows.Forms.TextBox txtALTValue;
		private System.Windows.Forms.Label lblAirline;
		private System.Windows.Forms.TextBox txtNATGroupUrno;
		private System.Windows.Forms.Label lblNatureGroup;
		private System.Windows.Forms.TextBox txtNATUrno;
		private System.Windows.Forms.TextBox txtNATValue;
		private System.Windows.Forms.Label lblNature;
		private System.Windows.Forms.Button btnACT;
		private System.Windows.Forms.Button btnACTGroup;
		private System.Windows.Forms.Button btnAPTGroup;
		private System.Windows.Forms.Button btnAPT;
		private System.Windows.Forms.Button btnALTGroup;
		private System.Windows.Forms.Button btnALT;
		private System.Windows.Forms.Button btnNATGroup;
		private System.Windows.Forms.Button btnNAT;
		private AxTABLib.AxTAB tabDefs;
		private System.Windows.Forms.PictureBox pictureBox2;
		private System.Windows.Forms.PictureBox pictureBox3;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Label lblAirineGroup;
		private System.Windows.Forms.Label lblACGroup;
		private System.Windows.Forms.Button btnAddDef;
		private System.Windows.Forms.TextBox txtNATGroupValue;
		private System.Windows.Forms.RadioButton rbArrival;
		private System.Windows.Forms.RadioButton rbDeparture;
		private System.Windows.Forms.Label lblValidFrom;
		private System.Windows.Forms.Label lblValidTo;
		private System.Windows.Forms.DateTimePicker dtValidFrom;
		private System.Windows.Forms.DateTimePicker dtValidTo;
		private System.Windows.Forms.Label lblUSEC;
		private System.Windows.Forms.Label lblUSEU;
		private System.Windows.Forms.Label lblCDAT;
		private System.Windows.Forms.TextBox txtUSEC;
		private System.Windows.Forms.TextBox txtUSEU;
		private System.Windows.Forms.TextBox txtCDAT;
		private System.Windows.Forms.TextBox txtLSTU;
		private System.Windows.Forms.Label lblLSTU;
		private System.Windows.Forms.Label lblSection;
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.Button btnNew;
		private System.Windows.Forms.Button btnStatusSection;
		private System.Windows.Forms.Panel panelHeader;
		private System.Windows.Forms.Button btnDeleteDef;
		private System.Windows.Forms.Button btnDeleteHeader;
		private System.Windows.Forms.TextBox txtSTSValue;
		private System.Windows.Forms.Button btnSTS;
		private System.Windows.Forms.TextBox txtSTSUrno;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox txtTagTest;
		private System.Windows.Forms.ContextMenu mnuContextDetail;
		private System.Windows.Forms.MenuItem mnuReactivate;
		private System.ComponentModel.IContainer components;
		private System.Windows.Forms.Label lblMAXT;
		private System.Windows.Forms.RadioButton rbTurnaround;
		private System.Windows.Forms.NumericUpDown numericUpDown1;
		private System.Windows.Forms.TextBox txtMAXT;
		private System.Windows.Forms.Label lblInMin;
		private System.Windows.Forms.Button btnCopy;  

		
		public frmRules()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmRules));
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnClose = new System.Windows.Forms.Button();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.btnRecalcFlights = new System.Windows.Forms.Button();
			this.btnCopy = new System.Windows.Forms.Button();
			this.txtTagTest = new System.Windows.Forms.TextBox();
			this.button1 = new System.Windows.Forms.Button();
			this.btnDeleteHeader = new System.Windows.Forms.Button();
			this.btnStatusSection = new System.Windows.Forms.Button();
			this.btnNew = new System.Windows.Forms.Button();
			this.btnSave = new System.Windows.Forms.Button();
			this.btnOpen = new System.Windows.Forms.Button();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.panelHeader = new System.Windows.Forms.Panel();
			this.btnDeleteDef = new System.Windows.Forms.Button();
			this.btnAddDef = new System.Windows.Forms.Button();
			this.panel3 = new System.Windows.Forms.Panel();
			this.lblInMin = new System.Windows.Forms.Label();
			this.txtMAXT = new System.Windows.Forms.TextBox();
			this.rbTurnaround = new System.Windows.Forms.RadioButton();
			this.lblMAXT = new System.Windows.Forms.Label();
			this.txtSTSValue = new System.Windows.Forms.TextBox();
			this.txtSTSUrno = new System.Windows.Forms.TextBox();
			this.btnSTS = new System.Windows.Forms.Button();
			this.lblSection = new System.Windows.Forms.Label();
			this.txtLSTU = new System.Windows.Forms.TextBox();
			this.txtCDAT = new System.Windows.Forms.TextBox();
			this.txtUSEU = new System.Windows.Forms.TextBox();
			this.txtUSEC = new System.Windows.Forms.TextBox();
			this.lblLSTU = new System.Windows.Forms.Label();
			this.lblCDAT = new System.Windows.Forms.Label();
			this.lblUSEU = new System.Windows.Forms.Label();
			this.lblUSEC = new System.Windows.Forms.Label();
			this.dtValidTo = new System.Windows.Forms.DateTimePicker();
			this.dtValidFrom = new System.Windows.Forms.DateTimePicker();
			this.lblValidTo = new System.Windows.Forms.Label();
			this.lblValidFrom = new System.Windows.Forms.Label();
			this.lblAirportGroup = new System.Windows.Forms.Label();
			this.lblNatureGroup = new System.Windows.Forms.Label();
			this.txtNATValue = new System.Windows.Forms.TextBox();
			this.lblNature = new System.Windows.Forms.Label();
			this.txtALTGroupValue = new System.Windows.Forms.TextBox();
			this.lblAirineGroup = new System.Windows.Forms.Label();
			this.txtALTValue = new System.Windows.Forms.TextBox();
			this.txtNATGroupUrno = new System.Windows.Forms.TextBox();
			this.btnNATGroup = new System.Windows.Forms.Button();
			this.txtNATGroupValue = new System.Windows.Forms.TextBox();
			this.txtAPTUrno = new System.Windows.Forms.TextBox();
			this.txtAPTValue = new System.Windows.Forms.TextBox();
			this.txtNATUrno = new System.Windows.Forms.TextBox();
			this.btnAPT = new System.Windows.Forms.Button();
			this.lblAirport = new System.Windows.Forms.Label();
			this.txtACTGroupUrno = new System.Windows.Forms.TextBox();
			this.btnNAT = new System.Windows.Forms.Button();
			this.txtACTGroupValue = new System.Windows.Forms.TextBox();
			this.txtALTGroupUrno = new System.Windows.Forms.TextBox();
			this.btnACTGroup = new System.Windows.Forms.Button();
			this.lblACGroup = new System.Windows.Forms.Label();
			this.txtACTUrno = new System.Windows.Forms.TextBox();
			this.btnALTGroup = new System.Windows.Forms.Button();
			this.txtACTValue = new System.Windows.Forms.TextBox();
			this.txtALTUrno = new System.Windows.Forms.TextBox();
			this.btnACT = new System.Windows.Forms.Button();
			this.lblACType = new System.Windows.Forms.Label();
			this.btnALT = new System.Windows.Forms.Button();
			this.lblAirline = new System.Windows.Forms.Label();
			this.txtRuleName = new System.Windows.Forms.TextBox();
			this.lblRuleName = new System.Windows.Forms.Label();
			this.txtAPTGroupUrno = new System.Windows.Forms.TextBox();
			this.txtAPTGroupValue = new System.Windows.Forms.TextBox();
			this.btnAPTGroup = new System.Windows.Forms.Button();
			this.rbArrival = new System.Windows.Forms.RadioButton();
			this.rbDeparture = new System.Windows.Forms.RadioButton();
			this.pictureBox3 = new System.Windows.Forms.PictureBox();
			this.label1 = new System.Windows.Forms.Label();
			this.lblEvent = new System.Windows.Forms.Label();
			this.tabDefs = new AxTABLib.AxTAB();
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
			this.mnuContextDetail = new System.Windows.Forms.ContextMenu();
			this.mnuReactivate = new System.Windows.Forms.MenuItem();
			this.panel1.SuspendLayout();
			this.panelHeader.SuspendLayout();
			this.panel3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabDefs)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.AccessibleDescription = resources.GetString("panel1.AccessibleDescription");
			this.panel1.AccessibleName = resources.GetString("panel1.AccessibleName");
			this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("panel1.Anchor")));
			this.panel1.AutoScroll = ((bool)(resources.GetObject("panel1.AutoScroll")));
			this.panel1.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("panel1.AutoScrollMargin")));
			this.panel1.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("panel1.AutoScrollMinSize")));
			this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
			this.panel1.Controls.Add(this.btnClose);
			this.panel1.Controls.Add(this.btnRecalcFlights);
			this.panel1.Controls.Add(this.btnCopy);
			this.panel1.Controls.Add(this.txtTagTest);
			this.panel1.Controls.Add(this.button1);
			this.panel1.Controls.Add(this.btnDeleteHeader);
			this.panel1.Controls.Add(this.btnStatusSection);
			this.panel1.Controls.Add(this.btnNew);
			this.panel1.Controls.Add(this.btnSave);
			this.panel1.Controls.Add(this.btnOpen);
			this.panel1.Controls.Add(this.pictureBox1);
			this.panel1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("panel1.Dock")));
			this.panel1.Enabled = ((bool)(resources.GetObject("panel1.Enabled")));
			this.panel1.Font = ((System.Drawing.Font)(resources.GetObject("panel1.Font")));
			this.panel1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("panel1.ImeMode")));
			this.panel1.Location = ((System.Drawing.Point)(resources.GetObject("panel1.Location")));
			this.panel1.Name = "panel1";
			this.panel1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("panel1.RightToLeft")));
			this.panel1.Size = ((System.Drawing.Size)(resources.GetObject("panel1.Size")));
			this.panel1.TabIndex = ((int)(resources.GetObject("panel1.TabIndex")));
			this.panel1.Text = resources.GetString("panel1.Text");
			this.toolTip1.SetToolTip(this.panel1, resources.GetString("panel1.ToolTip"));
			this.panel1.Visible = ((bool)(resources.GetObject("panel1.Visible")));
			// 
			// btnClose
			// 
			this.btnClose.AccessibleDescription = resources.GetString("btnClose.AccessibleDescription");
			this.btnClose.AccessibleName = resources.GetString("btnClose.AccessibleName");
			this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnClose.Anchor")));
			this.btnClose.BackColor = System.Drawing.Color.Transparent;
			this.btnClose.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnClose.BackgroundImage")));
			this.btnClose.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnClose.Dock")));
			this.btnClose.Enabled = ((bool)(resources.GetObject("btnClose.Enabled")));
			this.btnClose.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnClose.FlatStyle")));
			this.btnClose.Font = ((System.Drawing.Font)(resources.GetObject("btnClose.Font")));
			this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
			this.btnClose.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnClose.ImageAlign")));
			this.btnClose.ImageIndex = ((int)(resources.GetObject("btnClose.ImageIndex")));
			this.btnClose.ImageList = this.imageList1;
			this.btnClose.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnClose.ImeMode")));
			this.btnClose.Location = ((System.Drawing.Point)(resources.GetObject("btnClose.Location")));
			this.btnClose.Name = "btnClose";
			this.btnClose.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnClose.RightToLeft")));
			this.btnClose.Size = ((System.Drawing.Size)(resources.GetObject("btnClose.Size")));
			this.btnClose.TabIndex = ((int)(resources.GetObject("btnClose.TabIndex")));
			this.btnClose.TabStop = false;
			this.btnClose.Text = resources.GetString("btnClose.Text");
			this.btnClose.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnClose.TextAlign")));
			this.toolTip1.SetToolTip(this.btnClose, resources.GetString("btnClose.ToolTip"));
			this.btnClose.Visible = ((bool)(resources.GetObject("btnClose.Visible")));
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// imageList1
			// 
			this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth24Bit;
			this.imageList1.ImageSize = ((System.Drawing.Size)(resources.GetObject("imageList1.ImageSize")));
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.White;
			// 
			// btnRecalcFlights
			// 
			this.btnRecalcFlights.AccessibleDescription = resources.GetString("btnRecalcFlights.AccessibleDescription");
			this.btnRecalcFlights.AccessibleName = resources.GetString("btnRecalcFlights.AccessibleName");
			this.btnRecalcFlights.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnRecalcFlights.Anchor")));
			this.btnRecalcFlights.BackColor = System.Drawing.Color.Transparent;
			this.btnRecalcFlights.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRecalcFlights.BackgroundImage")));
			this.btnRecalcFlights.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnRecalcFlights.Dock")));
			this.btnRecalcFlights.Enabled = ((bool)(resources.GetObject("btnRecalcFlights.Enabled")));
			this.btnRecalcFlights.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnRecalcFlights.FlatStyle")));
			this.btnRecalcFlights.Font = ((System.Drawing.Font)(resources.GetObject("btnRecalcFlights.Font")));
			this.btnRecalcFlights.Image = ((System.Drawing.Image)(resources.GetObject("btnRecalcFlights.Image")));
			this.btnRecalcFlights.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnRecalcFlights.ImageAlign")));
			this.btnRecalcFlights.ImageIndex = ((int)(resources.GetObject("btnRecalcFlights.ImageIndex")));
			this.btnRecalcFlights.ImageList = this.imageList1;
			this.btnRecalcFlights.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnRecalcFlights.ImeMode")));
			this.btnRecalcFlights.Location = ((System.Drawing.Point)(resources.GetObject("btnRecalcFlights.Location")));
			this.btnRecalcFlights.Name = "btnRecalcFlights";
			this.btnRecalcFlights.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnRecalcFlights.RightToLeft")));
			this.btnRecalcFlights.Size = ((System.Drawing.Size)(resources.GetObject("btnRecalcFlights.Size")));
			this.btnRecalcFlights.TabIndex = ((int)(resources.GetObject("btnRecalcFlights.TabIndex")));
			this.btnRecalcFlights.TabStop = false;
			this.btnRecalcFlights.Text = resources.GetString("btnRecalcFlights.Text");
			this.btnRecalcFlights.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnRecalcFlights.TextAlign")));
			this.toolTip1.SetToolTip(this.btnRecalcFlights, resources.GetString("btnRecalcFlights.ToolTip"));
			this.btnRecalcFlights.Visible = ((bool)(resources.GetObject("btnRecalcFlights.Visible")));
			this.btnRecalcFlights.Click += new System.EventHandler(this.btnRecalcFlights_Click);
			// 
			// btnCopy
			// 
			this.btnCopy.AccessibleDescription = resources.GetString("btnCopy.AccessibleDescription");
			this.btnCopy.AccessibleName = resources.GetString("btnCopy.AccessibleName");
			this.btnCopy.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnCopy.Anchor")));
			this.btnCopy.BackColor = System.Drawing.Color.Transparent;
			this.btnCopy.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCopy.BackgroundImage")));
			this.btnCopy.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnCopy.Dock")));
			this.btnCopy.Enabled = ((bool)(resources.GetObject("btnCopy.Enabled")));
			this.btnCopy.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnCopy.FlatStyle")));
			this.btnCopy.Font = ((System.Drawing.Font)(resources.GetObject("btnCopy.Font")));
			this.btnCopy.Image = ((System.Drawing.Image)(resources.GetObject("btnCopy.Image")));
			this.btnCopy.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnCopy.ImageAlign")));
			this.btnCopy.ImageIndex = ((int)(resources.GetObject("btnCopy.ImageIndex")));
			this.btnCopy.ImageList = this.imageList1;
			this.btnCopy.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnCopy.ImeMode")));
			this.btnCopy.Location = ((System.Drawing.Point)(resources.GetObject("btnCopy.Location")));
			this.btnCopy.Name = "btnCopy";
			this.btnCopy.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnCopy.RightToLeft")));
			this.btnCopy.Size = ((System.Drawing.Size)(resources.GetObject("btnCopy.Size")));
			this.btnCopy.TabIndex = ((int)(resources.GetObject("btnCopy.TabIndex")));
			this.btnCopy.TabStop = false;
			this.btnCopy.Text = resources.GetString("btnCopy.Text");
			this.btnCopy.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnCopy.TextAlign")));
			this.toolTip1.SetToolTip(this.btnCopy, resources.GetString("btnCopy.ToolTip"));
			this.btnCopy.Visible = ((bool)(resources.GetObject("btnCopy.Visible")));
			this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
			// 
			// txtTagTest
			// 
			this.txtTagTest.AccessibleDescription = resources.GetString("txtTagTest.AccessibleDescription");
			this.txtTagTest.AccessibleName = resources.GetString("txtTagTest.AccessibleName");
			this.txtTagTest.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtTagTest.Anchor")));
			this.txtTagTest.AutoSize = ((bool)(resources.GetObject("txtTagTest.AutoSize")));
			this.txtTagTest.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtTagTest.BackgroundImage")));
			this.txtTagTest.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtTagTest.Dock")));
			this.txtTagTest.Enabled = ((bool)(resources.GetObject("txtTagTest.Enabled")));
			this.txtTagTest.Font = ((System.Drawing.Font)(resources.GetObject("txtTagTest.Font")));
			this.txtTagTest.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtTagTest.ImeMode")));
			this.txtTagTest.Location = ((System.Drawing.Point)(resources.GetObject("txtTagTest.Location")));
			this.txtTagTest.MaxLength = ((int)(resources.GetObject("txtTagTest.MaxLength")));
			this.txtTagTest.Multiline = ((bool)(resources.GetObject("txtTagTest.Multiline")));
			this.txtTagTest.Name = "txtTagTest";
			this.txtTagTest.PasswordChar = ((char)(resources.GetObject("txtTagTest.PasswordChar")));
			this.txtTagTest.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtTagTest.RightToLeft")));
			this.txtTagTest.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtTagTest.ScrollBars")));
			this.txtTagTest.Size = ((System.Drawing.Size)(resources.GetObject("txtTagTest.Size")));
			this.txtTagTest.TabIndex = ((int)(resources.GetObject("txtTagTest.TabIndex")));
			this.txtTagTest.Text = resources.GetString("txtTagTest.Text");
			this.txtTagTest.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtTagTest.TextAlign")));
			this.toolTip1.SetToolTip(this.txtTagTest, resources.GetString("txtTagTest.ToolTip"));
			this.txtTagTest.Visible = ((bool)(resources.GetObject("txtTagTest.Visible")));
			this.txtTagTest.WordWrap = ((bool)(resources.GetObject("txtTagTest.WordWrap")));
			// 
			// button1
			// 
			this.button1.AccessibleDescription = resources.GetString("button1.AccessibleDescription");
			this.button1.AccessibleName = resources.GetString("button1.AccessibleName");
			this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("button1.Anchor")));
			this.button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button1.BackgroundImage")));
			this.button1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("button1.Dock")));
			this.button1.Enabled = ((bool)(resources.GetObject("button1.Enabled")));
			this.button1.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("button1.FlatStyle")));
			this.button1.Font = ((System.Drawing.Font)(resources.GetObject("button1.Font")));
			this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
			this.button1.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button1.ImageAlign")));
			this.button1.ImageIndex = ((int)(resources.GetObject("button1.ImageIndex")));
			this.button1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("button1.ImeMode")));
			this.button1.Location = ((System.Drawing.Point)(resources.GetObject("button1.Location")));
			this.button1.Name = "button1";
			this.button1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("button1.RightToLeft")));
			this.button1.Size = ((System.Drawing.Size)(resources.GetObject("button1.Size")));
			this.button1.TabIndex = ((int)(resources.GetObject("button1.TabIndex")));
			this.button1.Text = resources.GetString("button1.Text");
			this.button1.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("button1.TextAlign")));
			this.toolTip1.SetToolTip(this.button1, resources.GetString("button1.ToolTip"));
			this.button1.Visible = ((bool)(resources.GetObject("button1.Visible")));
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// btnDeleteHeader
			// 
			this.btnDeleteHeader.AccessibleDescription = resources.GetString("btnDeleteHeader.AccessibleDescription");
			this.btnDeleteHeader.AccessibleName = resources.GetString("btnDeleteHeader.AccessibleName");
			this.btnDeleteHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnDeleteHeader.Anchor")));
			this.btnDeleteHeader.BackColor = System.Drawing.Color.Transparent;
			this.btnDeleteHeader.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDeleteHeader.BackgroundImage")));
			this.btnDeleteHeader.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnDeleteHeader.Dock")));
			this.btnDeleteHeader.Enabled = ((bool)(resources.GetObject("btnDeleteHeader.Enabled")));
			this.btnDeleteHeader.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnDeleteHeader.FlatStyle")));
			this.btnDeleteHeader.Font = ((System.Drawing.Font)(resources.GetObject("btnDeleteHeader.Font")));
			this.btnDeleteHeader.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteHeader.Image")));
			this.btnDeleteHeader.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnDeleteHeader.ImageAlign")));
			this.btnDeleteHeader.ImageIndex = ((int)(resources.GetObject("btnDeleteHeader.ImageIndex")));
			this.btnDeleteHeader.ImageList = this.imageList1;
			this.btnDeleteHeader.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnDeleteHeader.ImeMode")));
			this.btnDeleteHeader.Location = ((System.Drawing.Point)(resources.GetObject("btnDeleteHeader.Location")));
			this.btnDeleteHeader.Name = "btnDeleteHeader";
			this.btnDeleteHeader.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnDeleteHeader.RightToLeft")));
			this.btnDeleteHeader.Size = ((System.Drawing.Size)(resources.GetObject("btnDeleteHeader.Size")));
			this.btnDeleteHeader.TabIndex = ((int)(resources.GetObject("btnDeleteHeader.TabIndex")));
			this.btnDeleteHeader.TabStop = false;
			this.btnDeleteHeader.Text = resources.GetString("btnDeleteHeader.Text");
			this.btnDeleteHeader.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnDeleteHeader.TextAlign")));
			this.toolTip1.SetToolTip(this.btnDeleteHeader, resources.GetString("btnDeleteHeader.ToolTip"));
			this.btnDeleteHeader.Visible = ((bool)(resources.GetObject("btnDeleteHeader.Visible")));
			this.btnDeleteHeader.Click += new System.EventHandler(this.btnDeleteHeader_Click);
			// 
			// btnStatusSection
			// 
			this.btnStatusSection.AccessibleDescription = resources.GetString("btnStatusSection.AccessibleDescription");
			this.btnStatusSection.AccessibleName = resources.GetString("btnStatusSection.AccessibleName");
			this.btnStatusSection.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnStatusSection.Anchor")));
			this.btnStatusSection.BackColor = System.Drawing.Color.Transparent;
			this.btnStatusSection.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnStatusSection.BackgroundImage")));
			this.btnStatusSection.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnStatusSection.Dock")));
			this.btnStatusSection.Enabled = ((bool)(resources.GetObject("btnStatusSection.Enabled")));
			this.btnStatusSection.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnStatusSection.FlatStyle")));
			this.btnStatusSection.Font = ((System.Drawing.Font)(resources.GetObject("btnStatusSection.Font")));
			this.btnStatusSection.Image = ((System.Drawing.Image)(resources.GetObject("btnStatusSection.Image")));
			this.btnStatusSection.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnStatusSection.ImageAlign")));
			this.btnStatusSection.ImageIndex = ((int)(resources.GetObject("btnStatusSection.ImageIndex")));
			this.btnStatusSection.ImageList = this.imageList1;
			this.btnStatusSection.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnStatusSection.ImeMode")));
			this.btnStatusSection.Location = ((System.Drawing.Point)(resources.GetObject("btnStatusSection.Location")));
			this.btnStatusSection.Name = "btnStatusSection";
			this.btnStatusSection.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnStatusSection.RightToLeft")));
			this.btnStatusSection.Size = ((System.Drawing.Size)(resources.GetObject("btnStatusSection.Size")));
			this.btnStatusSection.TabIndex = ((int)(resources.GetObject("btnStatusSection.TabIndex")));
			this.btnStatusSection.TabStop = false;
			this.btnStatusSection.Text = resources.GetString("btnStatusSection.Text");
			this.btnStatusSection.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnStatusSection.TextAlign")));
			this.toolTip1.SetToolTip(this.btnStatusSection, resources.GetString("btnStatusSection.ToolTip"));
			this.btnStatusSection.Visible = ((bool)(resources.GetObject("btnStatusSection.Visible")));
			this.btnStatusSection.Click += new System.EventHandler(this.btnStatusSection_Click);
			// 
			// btnNew
			// 
			this.btnNew.AccessibleDescription = resources.GetString("btnNew.AccessibleDescription");
			this.btnNew.AccessibleName = resources.GetString("btnNew.AccessibleName");
			this.btnNew.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnNew.Anchor")));
			this.btnNew.BackColor = System.Drawing.Color.Transparent;
			this.btnNew.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNew.BackgroundImage")));
			this.btnNew.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnNew.Dock")));
			this.btnNew.Enabled = ((bool)(resources.GetObject("btnNew.Enabled")));
			this.btnNew.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnNew.FlatStyle")));
			this.btnNew.Font = ((System.Drawing.Font)(resources.GetObject("btnNew.Font")));
			this.btnNew.Image = ((System.Drawing.Image)(resources.GetObject("btnNew.Image")));
			this.btnNew.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnNew.ImageAlign")));
			this.btnNew.ImageIndex = ((int)(resources.GetObject("btnNew.ImageIndex")));
			this.btnNew.ImageList = this.imageList1;
			this.btnNew.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnNew.ImeMode")));
			this.btnNew.Location = ((System.Drawing.Point)(resources.GetObject("btnNew.Location")));
			this.btnNew.Name = "btnNew";
			this.btnNew.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnNew.RightToLeft")));
			this.btnNew.Size = ((System.Drawing.Size)(resources.GetObject("btnNew.Size")));
			this.btnNew.TabIndex = ((int)(resources.GetObject("btnNew.TabIndex")));
			this.btnNew.TabStop = false;
			this.btnNew.Text = resources.GetString("btnNew.Text");
			this.btnNew.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnNew.TextAlign")));
			this.toolTip1.SetToolTip(this.btnNew, resources.GetString("btnNew.ToolTip"));
			this.btnNew.Visible = ((bool)(resources.GetObject("btnNew.Visible")));
			this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
			// 
			// btnSave
			// 
			this.btnSave.AccessibleDescription = resources.GetString("btnSave.AccessibleDescription");
			this.btnSave.AccessibleName = resources.GetString("btnSave.AccessibleName");
			this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnSave.Anchor")));
			this.btnSave.BackColor = System.Drawing.Color.Transparent;
			this.btnSave.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSave.BackgroundImage")));
			this.btnSave.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnSave.Dock")));
			this.btnSave.Enabled = ((bool)(resources.GetObject("btnSave.Enabled")));
			this.btnSave.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnSave.FlatStyle")));
			this.btnSave.Font = ((System.Drawing.Font)(resources.GetObject("btnSave.Font")));
			this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
			this.btnSave.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnSave.ImageAlign")));
			this.btnSave.ImageIndex = ((int)(resources.GetObject("btnSave.ImageIndex")));
			this.btnSave.ImageList = this.imageList1;
			this.btnSave.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnSave.ImeMode")));
			this.btnSave.Location = ((System.Drawing.Point)(resources.GetObject("btnSave.Location")));
			this.btnSave.Name = "btnSave";
			this.btnSave.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnSave.RightToLeft")));
			this.btnSave.Size = ((System.Drawing.Size)(resources.GetObject("btnSave.Size")));
			this.btnSave.TabIndex = ((int)(resources.GetObject("btnSave.TabIndex")));
			this.btnSave.TabStop = false;
			this.btnSave.Text = resources.GetString("btnSave.Text");
			this.btnSave.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnSave.TextAlign")));
			this.toolTip1.SetToolTip(this.btnSave, resources.GetString("btnSave.ToolTip"));
			this.btnSave.Visible = ((bool)(resources.GetObject("btnSave.Visible")));
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// btnOpen
			// 
			this.btnOpen.AccessibleDescription = resources.GetString("btnOpen.AccessibleDescription");
			this.btnOpen.AccessibleName = resources.GetString("btnOpen.AccessibleName");
			this.btnOpen.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnOpen.Anchor")));
			this.btnOpen.BackColor = System.Drawing.Color.Transparent;
			this.btnOpen.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnOpen.BackgroundImage")));
			this.btnOpen.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnOpen.Dock")));
			this.btnOpen.Enabled = ((bool)(resources.GetObject("btnOpen.Enabled")));
			this.btnOpen.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnOpen.FlatStyle")));
			this.btnOpen.Font = ((System.Drawing.Font)(resources.GetObject("btnOpen.Font")));
			this.btnOpen.Image = ((System.Drawing.Image)(resources.GetObject("btnOpen.Image")));
			this.btnOpen.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnOpen.ImageAlign")));
			this.btnOpen.ImageIndex = ((int)(resources.GetObject("btnOpen.ImageIndex")));
			this.btnOpen.ImageList = this.imageList1;
			this.btnOpen.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnOpen.ImeMode")));
			this.btnOpen.Location = ((System.Drawing.Point)(resources.GetObject("btnOpen.Location")));
			this.btnOpen.Name = "btnOpen";
			this.btnOpen.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnOpen.RightToLeft")));
			this.btnOpen.Size = ((System.Drawing.Size)(resources.GetObject("btnOpen.Size")));
			this.btnOpen.TabIndex = ((int)(resources.GetObject("btnOpen.TabIndex")));
			this.btnOpen.TabStop = false;
			this.btnOpen.Text = resources.GetString("btnOpen.Text");
			this.btnOpen.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnOpen.TextAlign")));
			this.toolTip1.SetToolTip(this.btnOpen, resources.GetString("btnOpen.ToolTip"));
			this.btnOpen.Visible = ((bool)(resources.GetObject("btnOpen.Visible")));
			this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
			// 
			// pictureBox1
			// 
			this.pictureBox1.AccessibleDescription = resources.GetString("pictureBox1.AccessibleDescription");
			this.pictureBox1.AccessibleName = resources.GetString("pictureBox1.AccessibleName");
			this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("pictureBox1.Anchor")));
			this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
			this.pictureBox1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("pictureBox1.Dock")));
			this.pictureBox1.Enabled = ((bool)(resources.GetObject("pictureBox1.Enabled")));
			this.pictureBox1.Font = ((System.Drawing.Font)(resources.GetObject("pictureBox1.Font")));
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("pictureBox1.ImeMode")));
			this.pictureBox1.Location = ((System.Drawing.Point)(resources.GetObject("pictureBox1.Location")));
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("pictureBox1.RightToLeft")));
			this.pictureBox1.Size = ((System.Drawing.Size)(resources.GetObject("pictureBox1.Size")));
			this.pictureBox1.SizeMode = ((System.Windows.Forms.PictureBoxSizeMode)(resources.GetObject("pictureBox1.SizeMode")));
			this.pictureBox1.TabIndex = ((int)(resources.GetObject("pictureBox1.TabIndex")));
			this.pictureBox1.TabStop = false;
			this.pictureBox1.Text = resources.GetString("pictureBox1.Text");
			this.toolTip1.SetToolTip(this.pictureBox1, resources.GetString("pictureBox1.ToolTip"));
			this.pictureBox1.Visible = ((bool)(resources.GetObject("pictureBox1.Visible")));
			this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
			// 
			// panelHeader
			// 
			this.panelHeader.AccessibleDescription = resources.GetString("panelHeader.AccessibleDescription");
			this.panelHeader.AccessibleName = resources.GetString("panelHeader.AccessibleName");
			this.panelHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("panelHeader.Anchor")));
			this.panelHeader.AutoScroll = ((bool)(resources.GetObject("panelHeader.AutoScroll")));
			this.panelHeader.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("panelHeader.AutoScrollMargin")));
			this.panelHeader.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("panelHeader.AutoScrollMinSize")));
			this.panelHeader.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelHeader.BackgroundImage")));
			this.panelHeader.Controls.Add(this.btnDeleteDef);
			this.panelHeader.Controls.Add(this.btnAddDef);
			this.panelHeader.Controls.Add(this.panel3);
			this.panelHeader.Controls.Add(this.label1);
			this.panelHeader.Controls.Add(this.lblEvent);
			this.panelHeader.Controls.Add(this.tabDefs);
			this.panelHeader.Controls.Add(this.pictureBox2);
			this.panelHeader.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("panelHeader.Dock")));
			this.panelHeader.Enabled = ((bool)(resources.GetObject("panelHeader.Enabled")));
			this.panelHeader.Font = ((System.Drawing.Font)(resources.GetObject("panelHeader.Font")));
			this.panelHeader.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("panelHeader.ImeMode")));
			this.panelHeader.Location = ((System.Drawing.Point)(resources.GetObject("panelHeader.Location")));
			this.panelHeader.Name = "panelHeader";
			this.panelHeader.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("panelHeader.RightToLeft")));
			this.panelHeader.Size = ((System.Drawing.Size)(resources.GetObject("panelHeader.Size")));
			this.panelHeader.TabIndex = ((int)(resources.GetObject("panelHeader.TabIndex")));
			this.panelHeader.Text = resources.GetString("panelHeader.Text");
			this.toolTip1.SetToolTip(this.panelHeader, resources.GetString("panelHeader.ToolTip"));
			this.panelHeader.Visible = ((bool)(resources.GetObject("panelHeader.Visible")));
			// 
			// btnDeleteDef
			// 
			this.btnDeleteDef.AccessibleDescription = resources.GetString("btnDeleteDef.AccessibleDescription");
			this.btnDeleteDef.AccessibleName = resources.GetString("btnDeleteDef.AccessibleName");
			this.btnDeleteDef.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnDeleteDef.Anchor")));
			this.btnDeleteDef.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDeleteDef.BackgroundImage")));
			this.btnDeleteDef.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnDeleteDef.Dock")));
			this.btnDeleteDef.Enabled = ((bool)(resources.GetObject("btnDeleteDef.Enabled")));
			this.btnDeleteDef.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnDeleteDef.FlatStyle")));
			this.btnDeleteDef.Font = ((System.Drawing.Font)(resources.GetObject("btnDeleteDef.Font")));
			this.btnDeleteDef.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteDef.Image")));
			this.btnDeleteDef.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnDeleteDef.ImageAlign")));
			this.btnDeleteDef.ImageIndex = ((int)(resources.GetObject("btnDeleteDef.ImageIndex")));
			this.btnDeleteDef.ImageList = this.imageList1;
			this.btnDeleteDef.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnDeleteDef.ImeMode")));
			this.btnDeleteDef.Location = ((System.Drawing.Point)(resources.GetObject("btnDeleteDef.Location")));
			this.btnDeleteDef.Name = "btnDeleteDef";
			this.btnDeleteDef.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnDeleteDef.RightToLeft")));
			this.btnDeleteDef.Size = ((System.Drawing.Size)(resources.GetObject("btnDeleteDef.Size")));
			this.btnDeleteDef.TabIndex = ((int)(resources.GetObject("btnDeleteDef.TabIndex")));
			this.btnDeleteDef.Text = resources.GetString("btnDeleteDef.Text");
			this.btnDeleteDef.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnDeleteDef.TextAlign")));
			this.toolTip1.SetToolTip(this.btnDeleteDef, resources.GetString("btnDeleteDef.ToolTip"));
			this.btnDeleteDef.Visible = ((bool)(resources.GetObject("btnDeleteDef.Visible")));
			this.btnDeleteDef.Click += new System.EventHandler(this.btnDeleteDef_Click);
			// 
			// btnAddDef
			// 
			this.btnAddDef.AccessibleDescription = resources.GetString("btnAddDef.AccessibleDescription");
			this.btnAddDef.AccessibleName = resources.GetString("btnAddDef.AccessibleName");
			this.btnAddDef.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnAddDef.Anchor")));
			this.btnAddDef.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAddDef.BackgroundImage")));
			this.btnAddDef.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnAddDef.Dock")));
			this.btnAddDef.Enabled = ((bool)(resources.GetObject("btnAddDef.Enabled")));
			this.btnAddDef.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnAddDef.FlatStyle")));
			this.btnAddDef.Font = ((System.Drawing.Font)(resources.GetObject("btnAddDef.Font")));
			this.btnAddDef.Image = ((System.Drawing.Image)(resources.GetObject("btnAddDef.Image")));
			this.btnAddDef.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnAddDef.ImageAlign")));
			this.btnAddDef.ImageIndex = ((int)(resources.GetObject("btnAddDef.ImageIndex")));
			this.btnAddDef.ImageList = this.imageList1;
			this.btnAddDef.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnAddDef.ImeMode")));
			this.btnAddDef.Location = ((System.Drawing.Point)(resources.GetObject("btnAddDef.Location")));
			this.btnAddDef.Name = "btnAddDef";
			this.btnAddDef.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnAddDef.RightToLeft")));
			this.btnAddDef.Size = ((System.Drawing.Size)(resources.GetObject("btnAddDef.Size")));
			this.btnAddDef.TabIndex = ((int)(resources.GetObject("btnAddDef.TabIndex")));
			this.btnAddDef.Text = resources.GetString("btnAddDef.Text");
			this.btnAddDef.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnAddDef.TextAlign")));
			this.toolTip1.SetToolTip(this.btnAddDef, resources.GetString("btnAddDef.ToolTip"));
			this.btnAddDef.Visible = ((bool)(resources.GetObject("btnAddDef.Visible")));
			this.btnAddDef.Click += new System.EventHandler(this.btnAddDef_Click);
			// 
			// panel3
			// 
			this.panel3.AccessibleDescription = resources.GetString("panel3.AccessibleDescription");
			this.panel3.AccessibleName = resources.GetString("panel3.AccessibleName");
			this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("panel3.Anchor")));
			this.panel3.AutoScroll = ((bool)(resources.GetObject("panel3.AutoScroll")));
			this.panel3.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("panel3.AutoScrollMargin")));
			this.panel3.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("panel3.AutoScrollMinSize")));
			this.panel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel3.BackgroundImage")));
			this.panel3.Controls.Add(this.lblInMin);
			this.panel3.Controls.Add(this.txtMAXT);
			this.panel3.Controls.Add(this.rbTurnaround);
			this.panel3.Controls.Add(this.lblMAXT);
			this.panel3.Controls.Add(this.txtSTSValue);
			this.panel3.Controls.Add(this.txtSTSUrno);
			this.panel3.Controls.Add(this.btnSTS);
			this.panel3.Controls.Add(this.lblSection);
			this.panel3.Controls.Add(this.txtLSTU);
			this.panel3.Controls.Add(this.txtCDAT);
			this.panel3.Controls.Add(this.txtUSEU);
			this.panel3.Controls.Add(this.txtUSEC);
			this.panel3.Controls.Add(this.lblLSTU);
			this.panel3.Controls.Add(this.lblCDAT);
			this.panel3.Controls.Add(this.lblUSEU);
			this.panel3.Controls.Add(this.lblUSEC);
			this.panel3.Controls.Add(this.dtValidTo);
			this.panel3.Controls.Add(this.dtValidFrom);
			this.panel3.Controls.Add(this.lblValidTo);
			this.panel3.Controls.Add(this.lblValidFrom);
			this.panel3.Controls.Add(this.lblAirportGroup);
			this.panel3.Controls.Add(this.lblNatureGroup);
			this.panel3.Controls.Add(this.txtNATValue);
			this.panel3.Controls.Add(this.lblNature);
			this.panel3.Controls.Add(this.txtALTGroupValue);
			this.panel3.Controls.Add(this.lblAirineGroup);
			this.panel3.Controls.Add(this.txtALTValue);
			this.panel3.Controls.Add(this.txtNATGroupUrno);
			this.panel3.Controls.Add(this.btnNATGroup);
			this.panel3.Controls.Add(this.txtNATGroupValue);
			this.panel3.Controls.Add(this.txtAPTUrno);
			this.panel3.Controls.Add(this.txtAPTValue);
			this.panel3.Controls.Add(this.txtNATUrno);
			this.panel3.Controls.Add(this.btnAPT);
			this.panel3.Controls.Add(this.lblAirport);
			this.panel3.Controls.Add(this.txtACTGroupUrno);
			this.panel3.Controls.Add(this.btnNAT);
			this.panel3.Controls.Add(this.txtACTGroupValue);
			this.panel3.Controls.Add(this.txtALTGroupUrno);
			this.panel3.Controls.Add(this.btnACTGroup);
			this.panel3.Controls.Add(this.lblACGroup);
			this.panel3.Controls.Add(this.txtACTUrno);
			this.panel3.Controls.Add(this.btnALTGroup);
			this.panel3.Controls.Add(this.txtACTValue);
			this.panel3.Controls.Add(this.txtALTUrno);
			this.panel3.Controls.Add(this.btnACT);
			this.panel3.Controls.Add(this.lblACType);
			this.panel3.Controls.Add(this.btnALT);
			this.panel3.Controls.Add(this.lblAirline);
			this.panel3.Controls.Add(this.txtRuleName);
			this.panel3.Controls.Add(this.lblRuleName);
			this.panel3.Controls.Add(this.txtAPTGroupUrno);
			this.panel3.Controls.Add(this.txtAPTGroupValue);
			this.panel3.Controls.Add(this.btnAPTGroup);
			this.panel3.Controls.Add(this.rbArrival);
			this.panel3.Controls.Add(this.rbDeparture);
			this.panel3.Controls.Add(this.pictureBox3);
			this.panel3.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("panel3.Dock")));
			this.panel3.Enabled = ((bool)(resources.GetObject("panel3.Enabled")));
			this.panel3.Font = ((System.Drawing.Font)(resources.GetObject("panel3.Font")));
			this.panel3.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("panel3.ImeMode")));
			this.panel3.Location = ((System.Drawing.Point)(resources.GetObject("panel3.Location")));
			this.panel3.Name = "panel3";
			this.panel3.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("panel3.RightToLeft")));
			this.panel3.Size = ((System.Drawing.Size)(resources.GetObject("panel3.Size")));
			this.panel3.TabIndex = ((int)(resources.GetObject("panel3.TabIndex")));
			this.panel3.Text = resources.GetString("panel3.Text");
			this.toolTip1.SetToolTip(this.panel3, resources.GetString("panel3.ToolTip"));
			this.panel3.Visible = ((bool)(resources.GetObject("panel3.Visible")));
			// 
			// lblInMin
			// 
			this.lblInMin.AccessibleDescription = resources.GetString("lblInMin.AccessibleDescription");
			this.lblInMin.AccessibleName = resources.GetString("lblInMin.AccessibleName");
			this.lblInMin.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblInMin.Anchor")));
			this.lblInMin.AutoSize = ((bool)(resources.GetObject("lblInMin.AutoSize")));
			this.lblInMin.BackColor = System.Drawing.Color.Transparent;
			this.lblInMin.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblInMin.Dock")));
			this.lblInMin.Enabled = ((bool)(resources.GetObject("lblInMin.Enabled")));
			this.lblInMin.Font = ((System.Drawing.Font)(resources.GetObject("lblInMin.Font")));
			this.lblInMin.Image = ((System.Drawing.Image)(resources.GetObject("lblInMin.Image")));
			this.lblInMin.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblInMin.ImageAlign")));
			this.lblInMin.ImageIndex = ((int)(resources.GetObject("lblInMin.ImageIndex")));
			this.lblInMin.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblInMin.ImeMode")));
			this.lblInMin.Location = ((System.Drawing.Point)(resources.GetObject("lblInMin.Location")));
			this.lblInMin.Name = "lblInMin";
			this.lblInMin.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblInMin.RightToLeft")));
			this.lblInMin.Size = ((System.Drawing.Size)(resources.GetObject("lblInMin.Size")));
			this.lblInMin.TabIndex = ((int)(resources.GetObject("lblInMin.TabIndex")));
			this.lblInMin.Text = resources.GetString("lblInMin.Text");
			this.lblInMin.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblInMin.TextAlign")));
			this.toolTip1.SetToolTip(this.lblInMin, resources.GetString("lblInMin.ToolTip"));
			this.lblInMin.Visible = ((bool)(resources.GetObject("lblInMin.Visible")));
			// 
			// txtMAXT
			// 
			this.txtMAXT.AccessibleDescription = resources.GetString("txtMAXT.AccessibleDescription");
			this.txtMAXT.AccessibleName = resources.GetString("txtMAXT.AccessibleName");
			this.txtMAXT.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtMAXT.Anchor")));
			this.txtMAXT.AutoSize = ((bool)(resources.GetObject("txtMAXT.AutoSize")));
			this.txtMAXT.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtMAXT.BackgroundImage")));
			this.txtMAXT.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtMAXT.Dock")));
			this.txtMAXT.Enabled = ((bool)(resources.GetObject("txtMAXT.Enabled")));
			this.txtMAXT.Font = ((System.Drawing.Font)(resources.GetObject("txtMAXT.Font")));
			this.txtMAXT.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtMAXT.ImeMode")));
			this.txtMAXT.Location = ((System.Drawing.Point)(resources.GetObject("txtMAXT.Location")));
			this.txtMAXT.MaxLength = ((int)(resources.GetObject("txtMAXT.MaxLength")));
			this.txtMAXT.Multiline = ((bool)(resources.GetObject("txtMAXT.Multiline")));
			this.txtMAXT.Name = "txtMAXT";
			this.txtMAXT.PasswordChar = ((char)(resources.GetObject("txtMAXT.PasswordChar")));
			this.txtMAXT.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtMAXT.RightToLeft")));
			this.txtMAXT.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtMAXT.ScrollBars")));
			this.txtMAXT.Size = ((System.Drawing.Size)(resources.GetObject("txtMAXT.Size")));
			this.txtMAXT.TabIndex = ((int)(resources.GetObject("txtMAXT.TabIndex")));
			this.txtMAXT.Tag = "MAXT";
			this.txtMAXT.Text = resources.GetString("txtMAXT.Text");
			this.txtMAXT.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtMAXT.TextAlign")));
			this.toolTip1.SetToolTip(this.txtMAXT, resources.GetString("txtMAXT.ToolTip"));
			this.txtMAXT.Visible = ((bool)(resources.GetObject("txtMAXT.Visible")));
			this.txtMAXT.WordWrap = ((bool)(resources.GetObject("txtMAXT.WordWrap")));
			this.txtMAXT.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMAXT_KeyDown);
			this.txtMAXT.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMAXT_KeyPress);
			this.txtMAXT.TextChanged += new System.EventHandler(this.HeaderValueChanged);
			// 
			// rbTurnaround
			// 
			this.rbTurnaround.AccessibleDescription = resources.GetString("rbTurnaround.AccessibleDescription");
			this.rbTurnaround.AccessibleName = resources.GetString("rbTurnaround.AccessibleName");
			this.rbTurnaround.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("rbTurnaround.Anchor")));
			this.rbTurnaround.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("rbTurnaround.Appearance")));
			this.rbTurnaround.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("rbTurnaround.BackgroundImage")));
			this.rbTurnaround.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("rbTurnaround.CheckAlign")));
			this.rbTurnaround.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("rbTurnaround.Dock")));
			this.rbTurnaround.Enabled = ((bool)(resources.GetObject("rbTurnaround.Enabled")));
			this.rbTurnaround.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("rbTurnaround.FlatStyle")));
			this.rbTurnaround.Font = ((System.Drawing.Font)(resources.GetObject("rbTurnaround.Font")));
			this.rbTurnaround.ForeColor = System.Drawing.SystemColors.ControlText;
			this.rbTurnaround.Image = ((System.Drawing.Image)(resources.GetObject("rbTurnaround.Image")));
			this.rbTurnaround.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("rbTurnaround.ImageAlign")));
			this.rbTurnaround.ImageIndex = ((int)(resources.GetObject("rbTurnaround.ImageIndex")));
			this.rbTurnaround.ImageList = this.imageList1;
			this.rbTurnaround.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("rbTurnaround.ImeMode")));
			this.rbTurnaround.Location = ((System.Drawing.Point)(resources.GetObject("rbTurnaround.Location")));
			this.rbTurnaround.Name = "rbTurnaround";
			this.rbTurnaround.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("rbTurnaround.RightToLeft")));
			this.rbTurnaround.Size = ((System.Drawing.Size)(resources.GetObject("rbTurnaround.Size")));
			this.rbTurnaround.TabIndex = ((int)(resources.GetObject("rbTurnaround.TabIndex")));
			this.rbTurnaround.Text = resources.GetString("rbTurnaround.Text");
			this.rbTurnaround.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("rbTurnaround.TextAlign")));
			this.toolTip1.SetToolTip(this.rbTurnaround, resources.GetString("rbTurnaround.ToolTip"));
			this.rbTurnaround.Visible = ((bool)(resources.GetObject("rbTurnaround.Visible")));
			this.rbTurnaround.CheckedChanged += new System.EventHandler(this.rbArrival_CheckedChanged);
			// 
			// lblMAXT
			// 
			this.lblMAXT.AccessibleDescription = resources.GetString("lblMAXT.AccessibleDescription");
			this.lblMAXT.AccessibleName = resources.GetString("lblMAXT.AccessibleName");
			this.lblMAXT.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblMAXT.Anchor")));
			this.lblMAXT.AutoSize = ((bool)(resources.GetObject("lblMAXT.AutoSize")));
			this.lblMAXT.BackColor = System.Drawing.Color.Transparent;
			this.lblMAXT.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblMAXT.Dock")));
			this.lblMAXT.Enabled = ((bool)(resources.GetObject("lblMAXT.Enabled")));
			this.lblMAXT.Font = ((System.Drawing.Font)(resources.GetObject("lblMAXT.Font")));
			this.lblMAXT.Image = ((System.Drawing.Image)(resources.GetObject("lblMAXT.Image")));
			this.lblMAXT.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblMAXT.ImageAlign")));
			this.lblMAXT.ImageIndex = ((int)(resources.GetObject("lblMAXT.ImageIndex")));
			this.lblMAXT.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblMAXT.ImeMode")));
			this.lblMAXT.Location = ((System.Drawing.Point)(resources.GetObject("lblMAXT.Location")));
			this.lblMAXT.Name = "lblMAXT";
			this.lblMAXT.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblMAXT.RightToLeft")));
			this.lblMAXT.Size = ((System.Drawing.Size)(resources.GetObject("lblMAXT.Size")));
			this.lblMAXT.TabIndex = ((int)(resources.GetObject("lblMAXT.TabIndex")));
			this.lblMAXT.Text = resources.GetString("lblMAXT.Text");
			this.lblMAXT.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblMAXT.TextAlign")));
			this.toolTip1.SetToolTip(this.lblMAXT, resources.GetString("lblMAXT.ToolTip"));
			this.lblMAXT.Visible = ((bool)(resources.GetObject("lblMAXT.Visible")));
			// 
			// txtSTSValue
			// 
			this.txtSTSValue.AccessibleDescription = resources.GetString("txtSTSValue.AccessibleDescription");
			this.txtSTSValue.AccessibleName = resources.GetString("txtSTSValue.AccessibleName");
			this.txtSTSValue.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtSTSValue.Anchor")));
			this.txtSTSValue.AutoSize = ((bool)(resources.GetObject("txtSTSValue.AutoSize")));
			this.txtSTSValue.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(255)), ((System.Byte)(192)));
			this.txtSTSValue.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtSTSValue.BackgroundImage")));
			this.txtSTSValue.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtSTSValue.Dock")));
			this.txtSTSValue.Enabled = ((bool)(resources.GetObject("txtSTSValue.Enabled")));
			this.txtSTSValue.Font = ((System.Drawing.Font)(resources.GetObject("txtSTSValue.Font")));
			this.txtSTSValue.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtSTSValue.ImeMode")));
			this.txtSTSValue.Location = ((System.Drawing.Point)(resources.GetObject("txtSTSValue.Location")));
			this.txtSTSValue.MaxLength = ((int)(resources.GetObject("txtSTSValue.MaxLength")));
			this.txtSTSValue.Multiline = ((bool)(resources.GetObject("txtSTSValue.Multiline")));
			this.txtSTSValue.Name = "txtSTSValue";
			this.txtSTSValue.PasswordChar = ((char)(resources.GetObject("txtSTSValue.PasswordChar")));
			this.txtSTSValue.ReadOnly = true;
			this.txtSTSValue.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtSTSValue.RightToLeft")));
			this.txtSTSValue.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtSTSValue.ScrollBars")));
			this.txtSTSValue.Size = ((System.Drawing.Size)(resources.GetObject("txtSTSValue.Size")));
			this.txtSTSValue.TabIndex = ((int)(resources.GetObject("txtSTSValue.TabIndex")));
			this.txtSTSValue.Tag = "UHSS_VAL";
			this.txtSTSValue.Text = resources.GetString("txtSTSValue.Text");
			this.txtSTSValue.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtSTSValue.TextAlign")));
			this.toolTip1.SetToolTip(this.txtSTSValue, resources.GetString("txtSTSValue.ToolTip"));
			this.txtSTSValue.Visible = ((bool)(resources.GetObject("txtSTSValue.Visible")));
			this.txtSTSValue.WordWrap = ((bool)(resources.GetObject("txtSTSValue.WordWrap")));
			// 
			// txtSTSUrno
			// 
			this.txtSTSUrno.AccessibleDescription = resources.GetString("txtSTSUrno.AccessibleDescription");
			this.txtSTSUrno.AccessibleName = resources.GetString("txtSTSUrno.AccessibleName");
			this.txtSTSUrno.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtSTSUrno.Anchor")));
			this.txtSTSUrno.AutoSize = ((bool)(resources.GetObject("txtSTSUrno.AutoSize")));
			this.txtSTSUrno.BackColor = System.Drawing.SystemColors.Window;
			this.txtSTSUrno.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtSTSUrno.BackgroundImage")));
			this.txtSTSUrno.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtSTSUrno.Dock")));
			this.txtSTSUrno.Enabled = ((bool)(resources.GetObject("txtSTSUrno.Enabled")));
			this.txtSTSUrno.Font = ((System.Drawing.Font)(resources.GetObject("txtSTSUrno.Font")));
			this.txtSTSUrno.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtSTSUrno.ImeMode")));
			this.txtSTSUrno.Location = ((System.Drawing.Point)(resources.GetObject("txtSTSUrno.Location")));
			this.txtSTSUrno.MaxLength = ((int)(resources.GetObject("txtSTSUrno.MaxLength")));
			this.txtSTSUrno.Multiline = ((bool)(resources.GetObject("txtSTSUrno.Multiline")));
			this.txtSTSUrno.Name = "txtSTSUrno";
			this.txtSTSUrno.PasswordChar = ((char)(resources.GetObject("txtSTSUrno.PasswordChar")));
			this.txtSTSUrno.ReadOnly = true;
			this.txtSTSUrno.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtSTSUrno.RightToLeft")));
			this.txtSTSUrno.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtSTSUrno.ScrollBars")));
			this.txtSTSUrno.Size = ((System.Drawing.Size)(resources.GetObject("txtSTSUrno.Size")));
			this.txtSTSUrno.TabIndex = ((int)(resources.GetObject("txtSTSUrno.TabIndex")));
			this.txtSTSUrno.TabStop = false;
			this.txtSTSUrno.Tag = "UHSS";
			this.txtSTSUrno.Text = resources.GetString("txtSTSUrno.Text");
			this.txtSTSUrno.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtSTSUrno.TextAlign")));
			this.toolTip1.SetToolTip(this.txtSTSUrno, resources.GetString("txtSTSUrno.ToolTip"));
			this.txtSTSUrno.Visible = ((bool)(resources.GetObject("txtSTSUrno.Visible")));
			this.txtSTSUrno.WordWrap = ((bool)(resources.GetObject("txtSTSUrno.WordWrap")));
			this.txtSTSUrno.TextChanged += new System.EventHandler(this.HeaderValueChanged);
			// 
			// btnSTS
			// 
			this.btnSTS.AccessibleDescription = resources.GetString("btnSTS.AccessibleDescription");
			this.btnSTS.AccessibleName = resources.GetString("btnSTS.AccessibleName");
			this.btnSTS.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnSTS.Anchor")));
			this.btnSTS.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSTS.BackgroundImage")));
			this.btnSTS.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnSTS.Dock")));
			this.btnSTS.Enabled = ((bool)(resources.GetObject("btnSTS.Enabled")));
			this.btnSTS.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnSTS.FlatStyle")));
			this.btnSTS.Font = ((System.Drawing.Font)(resources.GetObject("btnSTS.Font")));
			this.btnSTS.Image = ((System.Drawing.Image)(resources.GetObject("btnSTS.Image")));
			this.btnSTS.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnSTS.ImageAlign")));
			this.btnSTS.ImageIndex = ((int)(resources.GetObject("btnSTS.ImageIndex")));
			this.btnSTS.ImageList = this.imageList1;
			this.btnSTS.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnSTS.ImeMode")));
			this.btnSTS.Location = ((System.Drawing.Point)(resources.GetObject("btnSTS.Location")));
			this.btnSTS.Name = "btnSTS";
			this.btnSTS.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnSTS.RightToLeft")));
			this.btnSTS.Size = ((System.Drawing.Size)(resources.GetObject("btnSTS.Size")));
			this.btnSTS.TabIndex = ((int)(resources.GetObject("btnSTS.TabIndex")));
			this.btnSTS.Text = resources.GetString("btnSTS.Text");
			this.btnSTS.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnSTS.TextAlign")));
			this.toolTip1.SetToolTip(this.btnSTS, resources.GetString("btnSTS.ToolTip"));
			this.btnSTS.Visible = ((bool)(resources.GetObject("btnSTS.Visible")));
			this.btnSTS.Click += new System.EventHandler(this.btnSTS_Click);
			// 
			// lblSection
			// 
			this.lblSection.AccessibleDescription = resources.GetString("lblSection.AccessibleDescription");
			this.lblSection.AccessibleName = resources.GetString("lblSection.AccessibleName");
			this.lblSection.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblSection.Anchor")));
			this.lblSection.AutoSize = ((bool)(resources.GetObject("lblSection.AutoSize")));
			this.lblSection.BackColor = System.Drawing.Color.Transparent;
			this.lblSection.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblSection.Dock")));
			this.lblSection.Enabled = ((bool)(resources.GetObject("lblSection.Enabled")));
			this.lblSection.Font = ((System.Drawing.Font)(resources.GetObject("lblSection.Font")));
			this.lblSection.Image = ((System.Drawing.Image)(resources.GetObject("lblSection.Image")));
			this.lblSection.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblSection.ImageAlign")));
			this.lblSection.ImageIndex = ((int)(resources.GetObject("lblSection.ImageIndex")));
			this.lblSection.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblSection.ImeMode")));
			this.lblSection.Location = ((System.Drawing.Point)(resources.GetObject("lblSection.Location")));
			this.lblSection.Name = "lblSection";
			this.lblSection.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblSection.RightToLeft")));
			this.lblSection.Size = ((System.Drawing.Size)(resources.GetObject("lblSection.Size")));
			this.lblSection.TabIndex = ((int)(resources.GetObject("lblSection.TabIndex")));
			this.lblSection.Text = resources.GetString("lblSection.Text");
			this.lblSection.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblSection.TextAlign")));
			this.toolTip1.SetToolTip(this.lblSection, resources.GetString("lblSection.ToolTip"));
			this.lblSection.Visible = ((bool)(resources.GetObject("lblSection.Visible")));
			// 
			// txtLSTU
			// 
			this.txtLSTU.AccessibleDescription = resources.GetString("txtLSTU.AccessibleDescription");
			this.txtLSTU.AccessibleName = resources.GetString("txtLSTU.AccessibleName");
			this.txtLSTU.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtLSTU.Anchor")));
			this.txtLSTU.AutoSize = ((bool)(resources.GetObject("txtLSTU.AutoSize")));
			this.txtLSTU.BackColor = System.Drawing.SystemColors.Window;
			this.txtLSTU.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtLSTU.BackgroundImage")));
			this.txtLSTU.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtLSTU.Dock")));
			this.txtLSTU.Enabled = ((bool)(resources.GetObject("txtLSTU.Enabled")));
			this.txtLSTU.Font = ((System.Drawing.Font)(resources.GetObject("txtLSTU.Font")));
			this.txtLSTU.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtLSTU.ImeMode")));
			this.txtLSTU.Location = ((System.Drawing.Point)(resources.GetObject("txtLSTU.Location")));
			this.txtLSTU.MaxLength = ((int)(resources.GetObject("txtLSTU.MaxLength")));
			this.txtLSTU.Multiline = ((bool)(resources.GetObject("txtLSTU.Multiline")));
			this.txtLSTU.Name = "txtLSTU";
			this.txtLSTU.PasswordChar = ((char)(resources.GetObject("txtLSTU.PasswordChar")));
			this.txtLSTU.ReadOnly = true;
			this.txtLSTU.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtLSTU.RightToLeft")));
			this.txtLSTU.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtLSTU.ScrollBars")));
			this.txtLSTU.Size = ((System.Drawing.Size)(resources.GetObject("txtLSTU.Size")));
			this.txtLSTU.TabIndex = ((int)(resources.GetObject("txtLSTU.TabIndex")));
			this.txtLSTU.Tag = "LSTU";
			this.txtLSTU.Text = resources.GetString("txtLSTU.Text");
			this.txtLSTU.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtLSTU.TextAlign")));
			this.toolTip1.SetToolTip(this.txtLSTU, resources.GetString("txtLSTU.ToolTip"));
			this.txtLSTU.Visible = ((bool)(resources.GetObject("txtLSTU.Visible")));
			this.txtLSTU.WordWrap = ((bool)(resources.GetObject("txtLSTU.WordWrap")));
			this.txtLSTU.TextChanged += new System.EventHandler(this.HeaderValueChanged);
			// 
			// txtCDAT
			// 
			this.txtCDAT.AccessibleDescription = resources.GetString("txtCDAT.AccessibleDescription");
			this.txtCDAT.AccessibleName = resources.GetString("txtCDAT.AccessibleName");
			this.txtCDAT.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtCDAT.Anchor")));
			this.txtCDAT.AutoSize = ((bool)(resources.GetObject("txtCDAT.AutoSize")));
			this.txtCDAT.BackColor = System.Drawing.SystemColors.Window;
			this.txtCDAT.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtCDAT.BackgroundImage")));
			this.txtCDAT.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtCDAT.Dock")));
			this.txtCDAT.Enabled = ((bool)(resources.GetObject("txtCDAT.Enabled")));
			this.txtCDAT.Font = ((System.Drawing.Font)(resources.GetObject("txtCDAT.Font")));
			this.txtCDAT.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtCDAT.ImeMode")));
			this.txtCDAT.Location = ((System.Drawing.Point)(resources.GetObject("txtCDAT.Location")));
			this.txtCDAT.MaxLength = ((int)(resources.GetObject("txtCDAT.MaxLength")));
			this.txtCDAT.Multiline = ((bool)(resources.GetObject("txtCDAT.Multiline")));
			this.txtCDAT.Name = "txtCDAT";
			this.txtCDAT.PasswordChar = ((char)(resources.GetObject("txtCDAT.PasswordChar")));
			this.txtCDAT.ReadOnly = true;
			this.txtCDAT.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtCDAT.RightToLeft")));
			this.txtCDAT.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtCDAT.ScrollBars")));
			this.txtCDAT.Size = ((System.Drawing.Size)(resources.GetObject("txtCDAT.Size")));
			this.txtCDAT.TabIndex = ((int)(resources.GetObject("txtCDAT.TabIndex")));
			this.txtCDAT.Tag = "CDAT";
			this.txtCDAT.Text = resources.GetString("txtCDAT.Text");
			this.txtCDAT.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtCDAT.TextAlign")));
			this.toolTip1.SetToolTip(this.txtCDAT, resources.GetString("txtCDAT.ToolTip"));
			this.txtCDAT.Visible = ((bool)(resources.GetObject("txtCDAT.Visible")));
			this.txtCDAT.WordWrap = ((bool)(resources.GetObject("txtCDAT.WordWrap")));
			this.txtCDAT.TextChanged += new System.EventHandler(this.HeaderValueChanged);
			// 
			// txtUSEU
			// 
			this.txtUSEU.AccessibleDescription = resources.GetString("txtUSEU.AccessibleDescription");
			this.txtUSEU.AccessibleName = resources.GetString("txtUSEU.AccessibleName");
			this.txtUSEU.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtUSEU.Anchor")));
			this.txtUSEU.AutoSize = ((bool)(resources.GetObject("txtUSEU.AutoSize")));
			this.txtUSEU.BackColor = System.Drawing.SystemColors.Window;
			this.txtUSEU.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtUSEU.BackgroundImage")));
			this.txtUSEU.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtUSEU.Dock")));
			this.txtUSEU.Enabled = ((bool)(resources.GetObject("txtUSEU.Enabled")));
			this.txtUSEU.Font = ((System.Drawing.Font)(resources.GetObject("txtUSEU.Font")));
			this.txtUSEU.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtUSEU.ImeMode")));
			this.txtUSEU.Location = ((System.Drawing.Point)(resources.GetObject("txtUSEU.Location")));
			this.txtUSEU.MaxLength = ((int)(resources.GetObject("txtUSEU.MaxLength")));
			this.txtUSEU.Multiline = ((bool)(resources.GetObject("txtUSEU.Multiline")));
			this.txtUSEU.Name = "txtUSEU";
			this.txtUSEU.PasswordChar = ((char)(resources.GetObject("txtUSEU.PasswordChar")));
			this.txtUSEU.ReadOnly = true;
			this.txtUSEU.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtUSEU.RightToLeft")));
			this.txtUSEU.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtUSEU.ScrollBars")));
			this.txtUSEU.Size = ((System.Drawing.Size)(resources.GetObject("txtUSEU.Size")));
			this.txtUSEU.TabIndex = ((int)(resources.GetObject("txtUSEU.TabIndex")));
			this.txtUSEU.Tag = "USEU";
			this.txtUSEU.Text = resources.GetString("txtUSEU.Text");
			this.txtUSEU.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtUSEU.TextAlign")));
			this.toolTip1.SetToolTip(this.txtUSEU, resources.GetString("txtUSEU.ToolTip"));
			this.txtUSEU.Visible = ((bool)(resources.GetObject("txtUSEU.Visible")));
			this.txtUSEU.WordWrap = ((bool)(resources.GetObject("txtUSEU.WordWrap")));
			this.txtUSEU.TextChanged += new System.EventHandler(this.HeaderValueChanged);
			// 
			// txtUSEC
			// 
			this.txtUSEC.AccessibleDescription = resources.GetString("txtUSEC.AccessibleDescription");
			this.txtUSEC.AccessibleName = resources.GetString("txtUSEC.AccessibleName");
			this.txtUSEC.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtUSEC.Anchor")));
			this.txtUSEC.AutoSize = ((bool)(resources.GetObject("txtUSEC.AutoSize")));
			this.txtUSEC.BackColor = System.Drawing.SystemColors.Window;
			this.txtUSEC.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtUSEC.BackgroundImage")));
			this.txtUSEC.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtUSEC.Dock")));
			this.txtUSEC.Enabled = ((bool)(resources.GetObject("txtUSEC.Enabled")));
			this.txtUSEC.Font = ((System.Drawing.Font)(resources.GetObject("txtUSEC.Font")));
			this.txtUSEC.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtUSEC.ImeMode")));
			this.txtUSEC.Location = ((System.Drawing.Point)(resources.GetObject("txtUSEC.Location")));
			this.txtUSEC.MaxLength = ((int)(resources.GetObject("txtUSEC.MaxLength")));
			this.txtUSEC.Multiline = ((bool)(resources.GetObject("txtUSEC.Multiline")));
			this.txtUSEC.Name = "txtUSEC";
			this.txtUSEC.PasswordChar = ((char)(resources.GetObject("txtUSEC.PasswordChar")));
			this.txtUSEC.ReadOnly = true;
			this.txtUSEC.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtUSEC.RightToLeft")));
			this.txtUSEC.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtUSEC.ScrollBars")));
			this.txtUSEC.Size = ((System.Drawing.Size)(resources.GetObject("txtUSEC.Size")));
			this.txtUSEC.TabIndex = ((int)(resources.GetObject("txtUSEC.TabIndex")));
			this.txtUSEC.Tag = "USEC";
			this.txtUSEC.Text = resources.GetString("txtUSEC.Text");
			this.txtUSEC.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtUSEC.TextAlign")));
			this.toolTip1.SetToolTip(this.txtUSEC, resources.GetString("txtUSEC.ToolTip"));
			this.txtUSEC.Visible = ((bool)(resources.GetObject("txtUSEC.Visible")));
			this.txtUSEC.WordWrap = ((bool)(resources.GetObject("txtUSEC.WordWrap")));
			this.txtUSEC.TextChanged += new System.EventHandler(this.HeaderValueChanged);
			// 
			// lblLSTU
			// 
			this.lblLSTU.AccessibleDescription = resources.GetString("lblLSTU.AccessibleDescription");
			this.lblLSTU.AccessibleName = resources.GetString("lblLSTU.AccessibleName");
			this.lblLSTU.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblLSTU.Anchor")));
			this.lblLSTU.AutoSize = ((bool)(resources.GetObject("lblLSTU.AutoSize")));
			this.lblLSTU.BackColor = System.Drawing.Color.Transparent;
			this.lblLSTU.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblLSTU.Dock")));
			this.lblLSTU.Enabled = ((bool)(resources.GetObject("lblLSTU.Enabled")));
			this.lblLSTU.Font = ((System.Drawing.Font)(resources.GetObject("lblLSTU.Font")));
			this.lblLSTU.Image = ((System.Drawing.Image)(resources.GetObject("lblLSTU.Image")));
			this.lblLSTU.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblLSTU.ImageAlign")));
			this.lblLSTU.ImageIndex = ((int)(resources.GetObject("lblLSTU.ImageIndex")));
			this.lblLSTU.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblLSTU.ImeMode")));
			this.lblLSTU.Location = ((System.Drawing.Point)(resources.GetObject("lblLSTU.Location")));
			this.lblLSTU.Name = "lblLSTU";
			this.lblLSTU.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblLSTU.RightToLeft")));
			this.lblLSTU.Size = ((System.Drawing.Size)(resources.GetObject("lblLSTU.Size")));
			this.lblLSTU.TabIndex = ((int)(resources.GetObject("lblLSTU.TabIndex")));
			this.lblLSTU.Text = resources.GetString("lblLSTU.Text");
			this.lblLSTU.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblLSTU.TextAlign")));
			this.toolTip1.SetToolTip(this.lblLSTU, resources.GetString("lblLSTU.ToolTip"));
			this.lblLSTU.Visible = ((bool)(resources.GetObject("lblLSTU.Visible")));
			// 
			// lblCDAT
			// 
			this.lblCDAT.AccessibleDescription = resources.GetString("lblCDAT.AccessibleDescription");
			this.lblCDAT.AccessibleName = resources.GetString("lblCDAT.AccessibleName");
			this.lblCDAT.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblCDAT.Anchor")));
			this.lblCDAT.AutoSize = ((bool)(resources.GetObject("lblCDAT.AutoSize")));
			this.lblCDAT.BackColor = System.Drawing.Color.Transparent;
			this.lblCDAT.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblCDAT.Dock")));
			this.lblCDAT.Enabled = ((bool)(resources.GetObject("lblCDAT.Enabled")));
			this.lblCDAT.Font = ((System.Drawing.Font)(resources.GetObject("lblCDAT.Font")));
			this.lblCDAT.Image = ((System.Drawing.Image)(resources.GetObject("lblCDAT.Image")));
			this.lblCDAT.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblCDAT.ImageAlign")));
			this.lblCDAT.ImageIndex = ((int)(resources.GetObject("lblCDAT.ImageIndex")));
			this.lblCDAT.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblCDAT.ImeMode")));
			this.lblCDAT.Location = ((System.Drawing.Point)(resources.GetObject("lblCDAT.Location")));
			this.lblCDAT.Name = "lblCDAT";
			this.lblCDAT.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblCDAT.RightToLeft")));
			this.lblCDAT.Size = ((System.Drawing.Size)(resources.GetObject("lblCDAT.Size")));
			this.lblCDAT.TabIndex = ((int)(resources.GetObject("lblCDAT.TabIndex")));
			this.lblCDAT.Text = resources.GetString("lblCDAT.Text");
			this.lblCDAT.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblCDAT.TextAlign")));
			this.toolTip1.SetToolTip(this.lblCDAT, resources.GetString("lblCDAT.ToolTip"));
			this.lblCDAT.Visible = ((bool)(resources.GetObject("lblCDAT.Visible")));
			// 
			// lblUSEU
			// 
			this.lblUSEU.AccessibleDescription = resources.GetString("lblUSEU.AccessibleDescription");
			this.lblUSEU.AccessibleName = resources.GetString("lblUSEU.AccessibleName");
			this.lblUSEU.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblUSEU.Anchor")));
			this.lblUSEU.AutoSize = ((bool)(resources.GetObject("lblUSEU.AutoSize")));
			this.lblUSEU.BackColor = System.Drawing.Color.Transparent;
			this.lblUSEU.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblUSEU.Dock")));
			this.lblUSEU.Enabled = ((bool)(resources.GetObject("lblUSEU.Enabled")));
			this.lblUSEU.Font = ((System.Drawing.Font)(resources.GetObject("lblUSEU.Font")));
			this.lblUSEU.Image = ((System.Drawing.Image)(resources.GetObject("lblUSEU.Image")));
			this.lblUSEU.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblUSEU.ImageAlign")));
			this.lblUSEU.ImageIndex = ((int)(resources.GetObject("lblUSEU.ImageIndex")));
			this.lblUSEU.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblUSEU.ImeMode")));
			this.lblUSEU.Location = ((System.Drawing.Point)(resources.GetObject("lblUSEU.Location")));
			this.lblUSEU.Name = "lblUSEU";
			this.lblUSEU.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblUSEU.RightToLeft")));
			this.lblUSEU.Size = ((System.Drawing.Size)(resources.GetObject("lblUSEU.Size")));
			this.lblUSEU.TabIndex = ((int)(resources.GetObject("lblUSEU.TabIndex")));
			this.lblUSEU.Text = resources.GetString("lblUSEU.Text");
			this.lblUSEU.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblUSEU.TextAlign")));
			this.toolTip1.SetToolTip(this.lblUSEU, resources.GetString("lblUSEU.ToolTip"));
			this.lblUSEU.Visible = ((bool)(resources.GetObject("lblUSEU.Visible")));
			// 
			// lblUSEC
			// 
			this.lblUSEC.AccessibleDescription = resources.GetString("lblUSEC.AccessibleDescription");
			this.lblUSEC.AccessibleName = resources.GetString("lblUSEC.AccessibleName");
			this.lblUSEC.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblUSEC.Anchor")));
			this.lblUSEC.AutoSize = ((bool)(resources.GetObject("lblUSEC.AutoSize")));
			this.lblUSEC.BackColor = System.Drawing.Color.Transparent;
			this.lblUSEC.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblUSEC.Dock")));
			this.lblUSEC.Enabled = ((bool)(resources.GetObject("lblUSEC.Enabled")));
			this.lblUSEC.Font = ((System.Drawing.Font)(resources.GetObject("lblUSEC.Font")));
			this.lblUSEC.Image = ((System.Drawing.Image)(resources.GetObject("lblUSEC.Image")));
			this.lblUSEC.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblUSEC.ImageAlign")));
			this.lblUSEC.ImageIndex = ((int)(resources.GetObject("lblUSEC.ImageIndex")));
			this.lblUSEC.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblUSEC.ImeMode")));
			this.lblUSEC.Location = ((System.Drawing.Point)(resources.GetObject("lblUSEC.Location")));
			this.lblUSEC.Name = "lblUSEC";
			this.lblUSEC.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblUSEC.RightToLeft")));
			this.lblUSEC.Size = ((System.Drawing.Size)(resources.GetObject("lblUSEC.Size")));
			this.lblUSEC.TabIndex = ((int)(resources.GetObject("lblUSEC.TabIndex")));
			this.lblUSEC.Text = resources.GetString("lblUSEC.Text");
			this.lblUSEC.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblUSEC.TextAlign")));
			this.toolTip1.SetToolTip(this.lblUSEC, resources.GetString("lblUSEC.ToolTip"));
			this.lblUSEC.Visible = ((bool)(resources.GetObject("lblUSEC.Visible")));
			// 
			// dtValidTo
			// 
			this.dtValidTo.AccessibleDescription = resources.GetString("dtValidTo.AccessibleDescription");
			this.dtValidTo.AccessibleName = resources.GetString("dtValidTo.AccessibleName");
			this.dtValidTo.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("dtValidTo.Anchor")));
			this.dtValidTo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("dtValidTo.BackgroundImage")));
			this.dtValidTo.CalendarFont = ((System.Drawing.Font)(resources.GetObject("dtValidTo.CalendarFont")));
			this.dtValidTo.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("dtValidTo.Dock")));
			this.dtValidTo.DropDownAlign = ((System.Windows.Forms.LeftRightAlignment)(resources.GetObject("dtValidTo.DropDownAlign")));
			this.dtValidTo.Enabled = ((bool)(resources.GetObject("dtValidTo.Enabled")));
			this.dtValidTo.Font = ((System.Drawing.Font)(resources.GetObject("dtValidTo.Font")));
			this.dtValidTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtValidTo.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("dtValidTo.ImeMode")));
			this.dtValidTo.Location = ((System.Drawing.Point)(resources.GetObject("dtValidTo.Location")));
			this.dtValidTo.MinDate = new System.DateTime(2003, 1, 1, 0, 0, 0, 0);
			this.dtValidTo.Name = "dtValidTo";
			this.dtValidTo.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("dtValidTo.RightToLeft")));
			this.dtValidTo.Size = ((System.Drawing.Size)(resources.GetObject("dtValidTo.Size")));
			this.dtValidTo.TabIndex = ((int)(resources.GetObject("dtValidTo.TabIndex")));
			this.dtValidTo.Tag = "VATO";
			this.toolTip1.SetToolTip(this.dtValidTo, resources.GetString("dtValidTo.ToolTip"));
			this.dtValidTo.Value = new System.DateTime(2020, 12, 31, 0, 0, 0, 0);
			this.dtValidTo.Visible = ((bool)(resources.GetObject("dtValidTo.Visible")));
			this.dtValidTo.ValueChanged += new System.EventHandler(this.dtValidTo_ValueChanged);
			// 
			// dtValidFrom
			// 
			this.dtValidFrom.AccessibleDescription = resources.GetString("dtValidFrom.AccessibleDescription");
			this.dtValidFrom.AccessibleName = resources.GetString("dtValidFrom.AccessibleName");
			this.dtValidFrom.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("dtValidFrom.Anchor")));
			this.dtValidFrom.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("dtValidFrom.BackgroundImage")));
			this.dtValidFrom.CalendarFont = ((System.Drawing.Font)(resources.GetObject("dtValidFrom.CalendarFont")));
			this.dtValidFrom.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("dtValidFrom.Dock")));
			this.dtValidFrom.DropDownAlign = ((System.Windows.Forms.LeftRightAlignment)(resources.GetObject("dtValidFrom.DropDownAlign")));
			this.dtValidFrom.Enabled = ((bool)(resources.GetObject("dtValidFrom.Enabled")));
			this.dtValidFrom.Font = ((System.Drawing.Font)(resources.GetObject("dtValidFrom.Font")));
			this.dtValidFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtValidFrom.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("dtValidFrom.ImeMode")));
			this.dtValidFrom.Location = ((System.Drawing.Point)(resources.GetObject("dtValidFrom.Location")));
			this.dtValidFrom.MinDate = new System.DateTime(2003, 1, 1, 0, 0, 0, 0);
			this.dtValidFrom.Name = "dtValidFrom";
			this.dtValidFrom.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("dtValidFrom.RightToLeft")));
			this.dtValidFrom.Size = ((System.Drawing.Size)(resources.GetObject("dtValidFrom.Size")));
			this.dtValidFrom.TabIndex = ((int)(resources.GetObject("dtValidFrom.TabIndex")));
			this.dtValidFrom.Tag = "VAFR";
			this.toolTip1.SetToolTip(this.dtValidFrom, resources.GetString("dtValidFrom.ToolTip"));
			this.dtValidFrom.Value = new System.DateTime(2003, 1, 1, 0, 0, 0, 0);
			this.dtValidFrom.Visible = ((bool)(resources.GetObject("dtValidFrom.Visible")));
			this.dtValidFrom.ValueChanged += new System.EventHandler(this.dtValidFrom_ValueChanged);
			// 
			// lblValidTo
			// 
			this.lblValidTo.AccessibleDescription = resources.GetString("lblValidTo.AccessibleDescription");
			this.lblValidTo.AccessibleName = resources.GetString("lblValidTo.AccessibleName");
			this.lblValidTo.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblValidTo.Anchor")));
			this.lblValidTo.AutoSize = ((bool)(resources.GetObject("lblValidTo.AutoSize")));
			this.lblValidTo.BackColor = System.Drawing.Color.Transparent;
			this.lblValidTo.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblValidTo.Dock")));
			this.lblValidTo.Enabled = ((bool)(resources.GetObject("lblValidTo.Enabled")));
			this.lblValidTo.Font = ((System.Drawing.Font)(resources.GetObject("lblValidTo.Font")));
			this.lblValidTo.Image = ((System.Drawing.Image)(resources.GetObject("lblValidTo.Image")));
			this.lblValidTo.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblValidTo.ImageAlign")));
			this.lblValidTo.ImageIndex = ((int)(resources.GetObject("lblValidTo.ImageIndex")));
			this.lblValidTo.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblValidTo.ImeMode")));
			this.lblValidTo.Location = ((System.Drawing.Point)(resources.GetObject("lblValidTo.Location")));
			this.lblValidTo.Name = "lblValidTo";
			this.lblValidTo.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblValidTo.RightToLeft")));
			this.lblValidTo.Size = ((System.Drawing.Size)(resources.GetObject("lblValidTo.Size")));
			this.lblValidTo.TabIndex = ((int)(resources.GetObject("lblValidTo.TabIndex")));
			this.lblValidTo.Text = resources.GetString("lblValidTo.Text");
			this.lblValidTo.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblValidTo.TextAlign")));
			this.toolTip1.SetToolTip(this.lblValidTo, resources.GetString("lblValidTo.ToolTip"));
			this.lblValidTo.Visible = ((bool)(resources.GetObject("lblValidTo.Visible")));
			// 
			// lblValidFrom
			// 
			this.lblValidFrom.AccessibleDescription = resources.GetString("lblValidFrom.AccessibleDescription");
			this.lblValidFrom.AccessibleName = resources.GetString("lblValidFrom.AccessibleName");
			this.lblValidFrom.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblValidFrom.Anchor")));
			this.lblValidFrom.AutoSize = ((bool)(resources.GetObject("lblValidFrom.AutoSize")));
			this.lblValidFrom.BackColor = System.Drawing.Color.Transparent;
			this.lblValidFrom.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblValidFrom.Dock")));
			this.lblValidFrom.Enabled = ((bool)(resources.GetObject("lblValidFrom.Enabled")));
			this.lblValidFrom.Font = ((System.Drawing.Font)(resources.GetObject("lblValidFrom.Font")));
			this.lblValidFrom.Image = ((System.Drawing.Image)(resources.GetObject("lblValidFrom.Image")));
			this.lblValidFrom.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblValidFrom.ImageAlign")));
			this.lblValidFrom.ImageIndex = ((int)(resources.GetObject("lblValidFrom.ImageIndex")));
			this.lblValidFrom.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblValidFrom.ImeMode")));
			this.lblValidFrom.Location = ((System.Drawing.Point)(resources.GetObject("lblValidFrom.Location")));
			this.lblValidFrom.Name = "lblValidFrom";
			this.lblValidFrom.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblValidFrom.RightToLeft")));
			this.lblValidFrom.Size = ((System.Drawing.Size)(resources.GetObject("lblValidFrom.Size")));
			this.lblValidFrom.TabIndex = ((int)(resources.GetObject("lblValidFrom.TabIndex")));
			this.lblValidFrom.Text = resources.GetString("lblValidFrom.Text");
			this.lblValidFrom.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblValidFrom.TextAlign")));
			this.toolTip1.SetToolTip(this.lblValidFrom, resources.GetString("lblValidFrom.ToolTip"));
			this.lblValidFrom.Visible = ((bool)(resources.GetObject("lblValidFrom.Visible")));
			// 
			// lblAirportGroup
			// 
			this.lblAirportGroup.AccessibleDescription = resources.GetString("lblAirportGroup.AccessibleDescription");
			this.lblAirportGroup.AccessibleName = resources.GetString("lblAirportGroup.AccessibleName");
			this.lblAirportGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblAirportGroup.Anchor")));
			this.lblAirportGroup.AutoSize = ((bool)(resources.GetObject("lblAirportGroup.AutoSize")));
			this.lblAirportGroup.BackColor = System.Drawing.Color.Transparent;
			this.lblAirportGroup.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblAirportGroup.Dock")));
			this.lblAirportGroup.Enabled = ((bool)(resources.GetObject("lblAirportGroup.Enabled")));
			this.lblAirportGroup.Font = ((System.Drawing.Font)(resources.GetObject("lblAirportGroup.Font")));
			this.lblAirportGroup.Image = ((System.Drawing.Image)(resources.GetObject("lblAirportGroup.Image")));
			this.lblAirportGroup.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblAirportGroup.ImageAlign")));
			this.lblAirportGroup.ImageIndex = ((int)(resources.GetObject("lblAirportGroup.ImageIndex")));
			this.lblAirportGroup.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblAirportGroup.ImeMode")));
			this.lblAirportGroup.Location = ((System.Drawing.Point)(resources.GetObject("lblAirportGroup.Location")));
			this.lblAirportGroup.Name = "lblAirportGroup";
			this.lblAirportGroup.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblAirportGroup.RightToLeft")));
			this.lblAirportGroup.Size = ((System.Drawing.Size)(resources.GetObject("lblAirportGroup.Size")));
			this.lblAirportGroup.TabIndex = ((int)(resources.GetObject("lblAirportGroup.TabIndex")));
			this.lblAirportGroup.Text = resources.GetString("lblAirportGroup.Text");
			this.lblAirportGroup.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblAirportGroup.TextAlign")));
			this.toolTip1.SetToolTip(this.lblAirportGroup, resources.GetString("lblAirportGroup.ToolTip"));
			this.lblAirportGroup.Visible = ((bool)(resources.GetObject("lblAirportGroup.Visible")));
			// 
			// lblNatureGroup
			// 
			this.lblNatureGroup.AccessibleDescription = resources.GetString("lblNatureGroup.AccessibleDescription");
			this.lblNatureGroup.AccessibleName = resources.GetString("lblNatureGroup.AccessibleName");
			this.lblNatureGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblNatureGroup.Anchor")));
			this.lblNatureGroup.AutoSize = ((bool)(resources.GetObject("lblNatureGroup.AutoSize")));
			this.lblNatureGroup.BackColor = System.Drawing.Color.Transparent;
			this.lblNatureGroup.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblNatureGroup.Dock")));
			this.lblNatureGroup.Enabled = ((bool)(resources.GetObject("lblNatureGroup.Enabled")));
			this.lblNatureGroup.Font = ((System.Drawing.Font)(resources.GetObject("lblNatureGroup.Font")));
			this.lblNatureGroup.Image = ((System.Drawing.Image)(resources.GetObject("lblNatureGroup.Image")));
			this.lblNatureGroup.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblNatureGroup.ImageAlign")));
			this.lblNatureGroup.ImageIndex = ((int)(resources.GetObject("lblNatureGroup.ImageIndex")));
			this.lblNatureGroup.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblNatureGroup.ImeMode")));
			this.lblNatureGroup.Location = ((System.Drawing.Point)(resources.GetObject("lblNatureGroup.Location")));
			this.lblNatureGroup.Name = "lblNatureGroup";
			this.lblNatureGroup.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblNatureGroup.RightToLeft")));
			this.lblNatureGroup.Size = ((System.Drawing.Size)(resources.GetObject("lblNatureGroup.Size")));
			this.lblNatureGroup.TabIndex = ((int)(resources.GetObject("lblNatureGroup.TabIndex")));
			this.lblNatureGroup.Text = resources.GetString("lblNatureGroup.Text");
			this.lblNatureGroup.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblNatureGroup.TextAlign")));
			this.toolTip1.SetToolTip(this.lblNatureGroup, resources.GetString("lblNatureGroup.ToolTip"));
			this.lblNatureGroup.Visible = ((bool)(resources.GetObject("lblNatureGroup.Visible")));
			// 
			// txtNATValue
			// 
			this.txtNATValue.AccessibleDescription = resources.GetString("txtNATValue.AccessibleDescription");
			this.txtNATValue.AccessibleName = resources.GetString("txtNATValue.AccessibleName");
			this.txtNATValue.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtNATValue.Anchor")));
			this.txtNATValue.AutoSize = ((bool)(resources.GetObject("txtNATValue.AutoSize")));
			this.txtNATValue.BackColor = System.Drawing.SystemColors.Window;
			this.txtNATValue.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtNATValue.BackgroundImage")));
			this.txtNATValue.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtNATValue.Dock")));
			this.txtNATValue.Enabled = ((bool)(resources.GetObject("txtNATValue.Enabled")));
			this.txtNATValue.Font = ((System.Drawing.Font)(resources.GetObject("txtNATValue.Font")));
			this.txtNATValue.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtNATValue.ImeMode")));
			this.txtNATValue.Location = ((System.Drawing.Point)(resources.GetObject("txtNATValue.Location")));
			this.txtNATValue.MaxLength = ((int)(resources.GetObject("txtNATValue.MaxLength")));
			this.txtNATValue.Multiline = ((bool)(resources.GetObject("txtNATValue.Multiline")));
			this.txtNATValue.Name = "txtNATValue";
			this.txtNATValue.PasswordChar = ((char)(resources.GetObject("txtNATValue.PasswordChar")));
			this.txtNATValue.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtNATValue.RightToLeft")));
			this.txtNATValue.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtNATValue.ScrollBars")));
			this.txtNATValue.Size = ((System.Drawing.Size)(resources.GetObject("txtNATValue.Size")));
			this.txtNATValue.TabIndex = ((int)(resources.GetObject("txtNATValue.TabIndex")));
			this.txtNATValue.Tag = "NATU_VAL";
			this.txtNATValue.Text = resources.GetString("txtNATValue.Text");
			this.txtNATValue.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtNATValue.TextAlign")));
			this.toolTip1.SetToolTip(this.txtNATValue, resources.GetString("txtNATValue.ToolTip"));
			this.txtNATValue.Visible = ((bool)(resources.GetObject("txtNATValue.Visible")));
			this.txtNATValue.WordWrap = ((bool)(resources.GetObject("txtNATValue.WordWrap")));
			this.txtNATValue.Leave += new System.EventHandler(this.txtNATValue_Leave);
			// 
			// lblNature
			// 
			this.lblNature.AccessibleDescription = resources.GetString("lblNature.AccessibleDescription");
			this.lblNature.AccessibleName = resources.GetString("lblNature.AccessibleName");
			this.lblNature.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblNature.Anchor")));
			this.lblNature.AutoSize = ((bool)(resources.GetObject("lblNature.AutoSize")));
			this.lblNature.BackColor = System.Drawing.Color.Transparent;
			this.lblNature.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblNature.Dock")));
			this.lblNature.Enabled = ((bool)(resources.GetObject("lblNature.Enabled")));
			this.lblNature.Font = ((System.Drawing.Font)(resources.GetObject("lblNature.Font")));
			this.lblNature.Image = ((System.Drawing.Image)(resources.GetObject("lblNature.Image")));
			this.lblNature.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblNature.ImageAlign")));
			this.lblNature.ImageIndex = ((int)(resources.GetObject("lblNature.ImageIndex")));
			this.lblNature.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblNature.ImeMode")));
			this.lblNature.Location = ((System.Drawing.Point)(resources.GetObject("lblNature.Location")));
			this.lblNature.Name = "lblNature";
			this.lblNature.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblNature.RightToLeft")));
			this.lblNature.Size = ((System.Drawing.Size)(resources.GetObject("lblNature.Size")));
			this.lblNature.TabIndex = ((int)(resources.GetObject("lblNature.TabIndex")));
			this.lblNature.Text = resources.GetString("lblNature.Text");
			this.lblNature.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblNature.TextAlign")));
			this.toolTip1.SetToolTip(this.lblNature, resources.GetString("lblNature.ToolTip"));
			this.lblNature.Visible = ((bool)(resources.GetObject("lblNature.Visible")));
			// 
			// txtALTGroupValue
			// 
			this.txtALTGroupValue.AccessibleDescription = resources.GetString("txtALTGroupValue.AccessibleDescription");
			this.txtALTGroupValue.AccessibleName = resources.GetString("txtALTGroupValue.AccessibleName");
			this.txtALTGroupValue.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtALTGroupValue.Anchor")));
			this.txtALTGroupValue.AutoSize = ((bool)(resources.GetObject("txtALTGroupValue.AutoSize")));
			this.txtALTGroupValue.BackColor = System.Drawing.SystemColors.Window;
			this.txtALTGroupValue.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtALTGroupValue.BackgroundImage")));
			this.txtALTGroupValue.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtALTGroupValue.Dock")));
			this.txtALTGroupValue.Enabled = ((bool)(resources.GetObject("txtALTGroupValue.Enabled")));
			this.txtALTGroupValue.Font = ((System.Drawing.Font)(resources.GetObject("txtALTGroupValue.Font")));
			this.txtALTGroupValue.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtALTGroupValue.ImeMode")));
			this.txtALTGroupValue.Location = ((System.Drawing.Point)(resources.GetObject("txtALTGroupValue.Location")));
			this.txtALTGroupValue.MaxLength = ((int)(resources.GetObject("txtALTGroupValue.MaxLength")));
			this.txtALTGroupValue.Multiline = ((bool)(resources.GetObject("txtALTGroupValue.Multiline")));
			this.txtALTGroupValue.Name = "txtALTGroupValue";
			this.txtALTGroupValue.PasswordChar = ((char)(resources.GetObject("txtALTGroupValue.PasswordChar")));
			this.txtALTGroupValue.ReadOnly = true;
			this.txtALTGroupValue.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtALTGroupValue.RightToLeft")));
			this.txtALTGroupValue.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtALTGroupValue.ScrollBars")));
			this.txtALTGroupValue.Size = ((System.Drawing.Size)(resources.GetObject("txtALTGroupValue.Size")));
			this.txtALTGroupValue.TabIndex = ((int)(resources.GetObject("txtALTGroupValue.TabIndex")));
			this.txtALTGroupValue.Tag = "ALTG_VAL";
			this.txtALTGroupValue.Text = resources.GetString("txtALTGroupValue.Text");
			this.txtALTGroupValue.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtALTGroupValue.TextAlign")));
			this.toolTip1.SetToolTip(this.txtALTGroupValue, resources.GetString("txtALTGroupValue.ToolTip"));
			this.txtALTGroupValue.Visible = ((bool)(resources.GetObject("txtALTGroupValue.Visible")));
			this.txtALTGroupValue.WordWrap = ((bool)(resources.GetObject("txtALTGroupValue.WordWrap")));
			this.txtALTGroupValue.MouseMove += new System.Windows.Forms.MouseEventHandler(this.txtALTGroupValue_MouseMove);
			// 
			// lblAirineGroup
			// 
			this.lblAirineGroup.AccessibleDescription = resources.GetString("lblAirineGroup.AccessibleDescription");
			this.lblAirineGroup.AccessibleName = resources.GetString("lblAirineGroup.AccessibleName");
			this.lblAirineGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblAirineGroup.Anchor")));
			this.lblAirineGroup.AutoSize = ((bool)(resources.GetObject("lblAirineGroup.AutoSize")));
			this.lblAirineGroup.BackColor = System.Drawing.Color.Transparent;
			this.lblAirineGroup.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblAirineGroup.Dock")));
			this.lblAirineGroup.Enabled = ((bool)(resources.GetObject("lblAirineGroup.Enabled")));
			this.lblAirineGroup.Font = ((System.Drawing.Font)(resources.GetObject("lblAirineGroup.Font")));
			this.lblAirineGroup.Image = ((System.Drawing.Image)(resources.GetObject("lblAirineGroup.Image")));
			this.lblAirineGroup.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblAirineGroup.ImageAlign")));
			this.lblAirineGroup.ImageIndex = ((int)(resources.GetObject("lblAirineGroup.ImageIndex")));
			this.lblAirineGroup.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblAirineGroup.ImeMode")));
			this.lblAirineGroup.Location = ((System.Drawing.Point)(resources.GetObject("lblAirineGroup.Location")));
			this.lblAirineGroup.Name = "lblAirineGroup";
			this.lblAirineGroup.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblAirineGroup.RightToLeft")));
			this.lblAirineGroup.Size = ((System.Drawing.Size)(resources.GetObject("lblAirineGroup.Size")));
			this.lblAirineGroup.TabIndex = ((int)(resources.GetObject("lblAirineGroup.TabIndex")));
			this.lblAirineGroup.Text = resources.GetString("lblAirineGroup.Text");
			this.lblAirineGroup.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblAirineGroup.TextAlign")));
			this.toolTip1.SetToolTip(this.lblAirineGroup, resources.GetString("lblAirineGroup.ToolTip"));
			this.lblAirineGroup.Visible = ((bool)(resources.GetObject("lblAirineGroup.Visible")));
			// 
			// txtALTValue
			// 
			this.txtALTValue.AccessibleDescription = resources.GetString("txtALTValue.AccessibleDescription");
			this.txtALTValue.AccessibleName = resources.GetString("txtALTValue.AccessibleName");
			this.txtALTValue.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtALTValue.Anchor")));
			this.txtALTValue.AutoSize = ((bool)(resources.GetObject("txtALTValue.AutoSize")));
			this.txtALTValue.BackColor = System.Drawing.SystemColors.Window;
			this.txtALTValue.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtALTValue.BackgroundImage")));
			this.txtALTValue.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtALTValue.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtALTValue.Dock")));
			this.txtALTValue.Enabled = ((bool)(resources.GetObject("txtALTValue.Enabled")));
			this.txtALTValue.Font = ((System.Drawing.Font)(resources.GetObject("txtALTValue.Font")));
			this.txtALTValue.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtALTValue.ImeMode")));
			this.txtALTValue.Location = ((System.Drawing.Point)(resources.GetObject("txtALTValue.Location")));
			this.txtALTValue.MaxLength = ((int)(resources.GetObject("txtALTValue.MaxLength")));
			this.txtALTValue.Multiline = ((bool)(resources.GetObject("txtALTValue.Multiline")));
			this.txtALTValue.Name = "txtALTValue";
			this.txtALTValue.PasswordChar = ((char)(resources.GetObject("txtALTValue.PasswordChar")));
			this.txtALTValue.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtALTValue.RightToLeft")));
			this.txtALTValue.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtALTValue.ScrollBars")));
			this.txtALTValue.Size = ((System.Drawing.Size)(resources.GetObject("txtALTValue.Size")));
			this.txtALTValue.TabIndex = ((int)(resources.GetObject("txtALTValue.TabIndex")));
			this.txtALTValue.Tag = "ALTU_VAL";
			this.txtALTValue.Text = resources.GetString("txtALTValue.Text");
			this.txtALTValue.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtALTValue.TextAlign")));
			this.toolTip1.SetToolTip(this.txtALTValue, resources.GetString("txtALTValue.ToolTip"));
			this.txtALTValue.Visible = ((bool)(resources.GetObject("txtALTValue.Visible")));
			this.txtALTValue.WordWrap = ((bool)(resources.GetObject("txtALTValue.WordWrap")));
			this.txtALTValue.Leave += new System.EventHandler(this.txtALTValue_Leave);
			// 
			// txtNATGroupUrno
			// 
			this.txtNATGroupUrno.AccessibleDescription = resources.GetString("txtNATGroupUrno.AccessibleDescription");
			this.txtNATGroupUrno.AccessibleName = resources.GetString("txtNATGroupUrno.AccessibleName");
			this.txtNATGroupUrno.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtNATGroupUrno.Anchor")));
			this.txtNATGroupUrno.AutoSize = ((bool)(resources.GetObject("txtNATGroupUrno.AutoSize")));
			this.txtNATGroupUrno.BackColor = System.Drawing.SystemColors.Window;
			this.txtNATGroupUrno.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtNATGroupUrno.BackgroundImage")));
			this.txtNATGroupUrno.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtNATGroupUrno.Dock")));
			this.txtNATGroupUrno.Enabled = ((bool)(resources.GetObject("txtNATGroupUrno.Enabled")));
			this.txtNATGroupUrno.Font = ((System.Drawing.Font)(resources.GetObject("txtNATGroupUrno.Font")));
			this.txtNATGroupUrno.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtNATGroupUrno.ImeMode")));
			this.txtNATGroupUrno.Location = ((System.Drawing.Point)(resources.GetObject("txtNATGroupUrno.Location")));
			this.txtNATGroupUrno.MaxLength = ((int)(resources.GetObject("txtNATGroupUrno.MaxLength")));
			this.txtNATGroupUrno.Multiline = ((bool)(resources.GetObject("txtNATGroupUrno.Multiline")));
			this.txtNATGroupUrno.Name = "txtNATGroupUrno";
			this.txtNATGroupUrno.PasswordChar = ((char)(resources.GetObject("txtNATGroupUrno.PasswordChar")));
			this.txtNATGroupUrno.ReadOnly = true;
			this.txtNATGroupUrno.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtNATGroupUrno.RightToLeft")));
			this.txtNATGroupUrno.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtNATGroupUrno.ScrollBars")));
			this.txtNATGroupUrno.Size = ((System.Drawing.Size)(resources.GetObject("txtNATGroupUrno.Size")));
			this.txtNATGroupUrno.TabIndex = ((int)(resources.GetObject("txtNATGroupUrno.TabIndex")));
			this.txtNATGroupUrno.TabStop = false;
			this.txtNATGroupUrno.Tag = "NATG";
			this.txtNATGroupUrno.Text = resources.GetString("txtNATGroupUrno.Text");
			this.txtNATGroupUrno.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtNATGroupUrno.TextAlign")));
			this.toolTip1.SetToolTip(this.txtNATGroupUrno, resources.GetString("txtNATGroupUrno.ToolTip"));
			this.txtNATGroupUrno.Visible = ((bool)(resources.GetObject("txtNATGroupUrno.Visible")));
			this.txtNATGroupUrno.WordWrap = ((bool)(resources.GetObject("txtNATGroupUrno.WordWrap")));
			this.txtNATGroupUrno.TextChanged += new System.EventHandler(this.HeaderValueChanged);
			// 
			// btnNATGroup
			// 
			this.btnNATGroup.AccessibleDescription = resources.GetString("btnNATGroup.AccessibleDescription");
			this.btnNATGroup.AccessibleName = resources.GetString("btnNATGroup.AccessibleName");
			this.btnNATGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnNATGroup.Anchor")));
			this.btnNATGroup.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNATGroup.BackgroundImage")));
			this.btnNATGroup.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnNATGroup.Dock")));
			this.btnNATGroup.Enabled = ((bool)(resources.GetObject("btnNATGroup.Enabled")));
			this.btnNATGroup.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnNATGroup.FlatStyle")));
			this.btnNATGroup.Font = ((System.Drawing.Font)(resources.GetObject("btnNATGroup.Font")));
			this.btnNATGroup.Image = ((System.Drawing.Image)(resources.GetObject("btnNATGroup.Image")));
			this.btnNATGroup.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnNATGroup.ImageAlign")));
			this.btnNATGroup.ImageIndex = ((int)(resources.GetObject("btnNATGroup.ImageIndex")));
			this.btnNATGroup.ImageList = this.imageList1;
			this.btnNATGroup.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnNATGroup.ImeMode")));
			this.btnNATGroup.Location = ((System.Drawing.Point)(resources.GetObject("btnNATGroup.Location")));
			this.btnNATGroup.Name = "btnNATGroup";
			this.btnNATGroup.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnNATGroup.RightToLeft")));
			this.btnNATGroup.Size = ((System.Drawing.Size)(resources.GetObject("btnNATGroup.Size")));
			this.btnNATGroup.TabIndex = ((int)(resources.GetObject("btnNATGroup.TabIndex")));
			this.btnNATGroup.Text = resources.GetString("btnNATGroup.Text");
			this.btnNATGroup.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnNATGroup.TextAlign")));
			this.toolTip1.SetToolTip(this.btnNATGroup, resources.GetString("btnNATGroup.ToolTip"));
			this.btnNATGroup.Visible = ((bool)(resources.GetObject("btnNATGroup.Visible")));
			this.btnNATGroup.Click += new System.EventHandler(this.btnNATGroup_Click);
			// 
			// txtNATGroupValue
			// 
			this.txtNATGroupValue.AccessibleDescription = resources.GetString("txtNATGroupValue.AccessibleDescription");
			this.txtNATGroupValue.AccessibleName = resources.GetString("txtNATGroupValue.AccessibleName");
			this.txtNATGroupValue.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtNATGroupValue.Anchor")));
			this.txtNATGroupValue.AutoSize = ((bool)(resources.GetObject("txtNATGroupValue.AutoSize")));
			this.txtNATGroupValue.BackColor = System.Drawing.SystemColors.Window;
			this.txtNATGroupValue.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtNATGroupValue.BackgroundImage")));
			this.txtNATGroupValue.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtNATGroupValue.Dock")));
			this.txtNATGroupValue.Enabled = ((bool)(resources.GetObject("txtNATGroupValue.Enabled")));
			this.txtNATGroupValue.Font = ((System.Drawing.Font)(resources.GetObject("txtNATGroupValue.Font")));
			this.txtNATGroupValue.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtNATGroupValue.ImeMode")));
			this.txtNATGroupValue.Location = ((System.Drawing.Point)(resources.GetObject("txtNATGroupValue.Location")));
			this.txtNATGroupValue.MaxLength = ((int)(resources.GetObject("txtNATGroupValue.MaxLength")));
			this.txtNATGroupValue.Multiline = ((bool)(resources.GetObject("txtNATGroupValue.Multiline")));
			this.txtNATGroupValue.Name = "txtNATGroupValue";
			this.txtNATGroupValue.PasswordChar = ((char)(resources.GetObject("txtNATGroupValue.PasswordChar")));
			this.txtNATGroupValue.ReadOnly = true;
			this.txtNATGroupValue.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtNATGroupValue.RightToLeft")));
			this.txtNATGroupValue.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtNATGroupValue.ScrollBars")));
			this.txtNATGroupValue.Size = ((System.Drawing.Size)(resources.GetObject("txtNATGroupValue.Size")));
			this.txtNATGroupValue.TabIndex = ((int)(resources.GetObject("txtNATGroupValue.TabIndex")));
			this.txtNATGroupValue.Tag = "NATG_VAL";
			this.txtNATGroupValue.Text = resources.GetString("txtNATGroupValue.Text");
			this.txtNATGroupValue.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtNATGroupValue.TextAlign")));
			this.toolTip1.SetToolTip(this.txtNATGroupValue, resources.GetString("txtNATGroupValue.ToolTip"));
			this.txtNATGroupValue.Visible = ((bool)(resources.GetObject("txtNATGroupValue.Visible")));
			this.txtNATGroupValue.WordWrap = ((bool)(resources.GetObject("txtNATGroupValue.WordWrap")));
			this.txtNATGroupValue.MouseMove += new System.Windows.Forms.MouseEventHandler(this.txtNATGroupValue_MouseMove);
			// 
			// txtAPTUrno
			// 
			this.txtAPTUrno.AccessibleDescription = resources.GetString("txtAPTUrno.AccessibleDescription");
			this.txtAPTUrno.AccessibleName = resources.GetString("txtAPTUrno.AccessibleName");
			this.txtAPTUrno.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtAPTUrno.Anchor")));
			this.txtAPTUrno.AutoSize = ((bool)(resources.GetObject("txtAPTUrno.AutoSize")));
			this.txtAPTUrno.BackColor = System.Drawing.SystemColors.Window;
			this.txtAPTUrno.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtAPTUrno.BackgroundImage")));
			this.txtAPTUrno.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtAPTUrno.Dock")));
			this.txtAPTUrno.Enabled = ((bool)(resources.GetObject("txtAPTUrno.Enabled")));
			this.txtAPTUrno.Font = ((System.Drawing.Font)(resources.GetObject("txtAPTUrno.Font")));
			this.txtAPTUrno.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtAPTUrno.ImeMode")));
			this.txtAPTUrno.Location = ((System.Drawing.Point)(resources.GetObject("txtAPTUrno.Location")));
			this.txtAPTUrno.MaxLength = ((int)(resources.GetObject("txtAPTUrno.MaxLength")));
			this.txtAPTUrno.Multiline = ((bool)(resources.GetObject("txtAPTUrno.Multiline")));
			this.txtAPTUrno.Name = "txtAPTUrno";
			this.txtAPTUrno.PasswordChar = ((char)(resources.GetObject("txtAPTUrno.PasswordChar")));
			this.txtAPTUrno.ReadOnly = true;
			this.txtAPTUrno.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtAPTUrno.RightToLeft")));
			this.txtAPTUrno.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtAPTUrno.ScrollBars")));
			this.txtAPTUrno.Size = ((System.Drawing.Size)(resources.GetObject("txtAPTUrno.Size")));
			this.txtAPTUrno.TabIndex = ((int)(resources.GetObject("txtAPTUrno.TabIndex")));
			this.txtAPTUrno.TabStop = false;
			this.txtAPTUrno.Tag = "APTU";
			this.txtAPTUrno.Text = resources.GetString("txtAPTUrno.Text");
			this.txtAPTUrno.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtAPTUrno.TextAlign")));
			this.toolTip1.SetToolTip(this.txtAPTUrno, resources.GetString("txtAPTUrno.ToolTip"));
			this.txtAPTUrno.Visible = ((bool)(resources.GetObject("txtAPTUrno.Visible")));
			this.txtAPTUrno.WordWrap = ((bool)(resources.GetObject("txtAPTUrno.WordWrap")));
			this.txtAPTUrno.TextChanged += new System.EventHandler(this.HeaderValueChanged);
			// 
			// txtAPTValue
			// 
			this.txtAPTValue.AccessibleDescription = resources.GetString("txtAPTValue.AccessibleDescription");
			this.txtAPTValue.AccessibleName = resources.GetString("txtAPTValue.AccessibleName");
			this.txtAPTValue.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtAPTValue.Anchor")));
			this.txtAPTValue.AutoSize = ((bool)(resources.GetObject("txtAPTValue.AutoSize")));
			this.txtAPTValue.BackColor = System.Drawing.SystemColors.Window;
			this.txtAPTValue.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtAPTValue.BackgroundImage")));
			this.txtAPTValue.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtAPTValue.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtAPTValue.Dock")));
			this.txtAPTValue.Enabled = ((bool)(resources.GetObject("txtAPTValue.Enabled")));
			this.txtAPTValue.Font = ((System.Drawing.Font)(resources.GetObject("txtAPTValue.Font")));
			this.txtAPTValue.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtAPTValue.ImeMode")));
			this.txtAPTValue.Location = ((System.Drawing.Point)(resources.GetObject("txtAPTValue.Location")));
			this.txtAPTValue.MaxLength = ((int)(resources.GetObject("txtAPTValue.MaxLength")));
			this.txtAPTValue.Multiline = ((bool)(resources.GetObject("txtAPTValue.Multiline")));
			this.txtAPTValue.Name = "txtAPTValue";
			this.txtAPTValue.PasswordChar = ((char)(resources.GetObject("txtAPTValue.PasswordChar")));
			this.txtAPTValue.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtAPTValue.RightToLeft")));
			this.txtAPTValue.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtAPTValue.ScrollBars")));
			this.txtAPTValue.Size = ((System.Drawing.Size)(resources.GetObject("txtAPTValue.Size")));
			this.txtAPTValue.TabIndex = ((int)(resources.GetObject("txtAPTValue.TabIndex")));
			this.txtAPTValue.Tag = "APTU_VAL";
			this.txtAPTValue.Text = resources.GetString("txtAPTValue.Text");
			this.txtAPTValue.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtAPTValue.TextAlign")));
			this.toolTip1.SetToolTip(this.txtAPTValue, resources.GetString("txtAPTValue.ToolTip"));
			this.txtAPTValue.Visible = ((bool)(resources.GetObject("txtAPTValue.Visible")));
			this.txtAPTValue.WordWrap = ((bool)(resources.GetObject("txtAPTValue.WordWrap")));
			this.txtAPTValue.Leave += new System.EventHandler(this.txtAPTValue_Leave);
			// 
			// txtNATUrno
			// 
			this.txtNATUrno.AccessibleDescription = resources.GetString("txtNATUrno.AccessibleDescription");
			this.txtNATUrno.AccessibleName = resources.GetString("txtNATUrno.AccessibleName");
			this.txtNATUrno.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtNATUrno.Anchor")));
			this.txtNATUrno.AutoSize = ((bool)(resources.GetObject("txtNATUrno.AutoSize")));
			this.txtNATUrno.BackColor = System.Drawing.SystemColors.Window;
			this.txtNATUrno.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtNATUrno.BackgroundImage")));
			this.txtNATUrno.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtNATUrno.Dock")));
			this.txtNATUrno.Enabled = ((bool)(resources.GetObject("txtNATUrno.Enabled")));
			this.txtNATUrno.Font = ((System.Drawing.Font)(resources.GetObject("txtNATUrno.Font")));
			this.txtNATUrno.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtNATUrno.ImeMode")));
			this.txtNATUrno.Location = ((System.Drawing.Point)(resources.GetObject("txtNATUrno.Location")));
			this.txtNATUrno.MaxLength = ((int)(resources.GetObject("txtNATUrno.MaxLength")));
			this.txtNATUrno.Multiline = ((bool)(resources.GetObject("txtNATUrno.Multiline")));
			this.txtNATUrno.Name = "txtNATUrno";
			this.txtNATUrno.PasswordChar = ((char)(resources.GetObject("txtNATUrno.PasswordChar")));
			this.txtNATUrno.ReadOnly = true;
			this.txtNATUrno.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtNATUrno.RightToLeft")));
			this.txtNATUrno.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtNATUrno.ScrollBars")));
			this.txtNATUrno.Size = ((System.Drawing.Size)(resources.GetObject("txtNATUrno.Size")));
			this.txtNATUrno.TabIndex = ((int)(resources.GetObject("txtNATUrno.TabIndex")));
			this.txtNATUrno.TabStop = false;
			this.txtNATUrno.Tag = "NATU";
			this.txtNATUrno.Text = resources.GetString("txtNATUrno.Text");
			this.txtNATUrno.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtNATUrno.TextAlign")));
			this.toolTip1.SetToolTip(this.txtNATUrno, resources.GetString("txtNATUrno.ToolTip"));
			this.txtNATUrno.Visible = ((bool)(resources.GetObject("txtNATUrno.Visible")));
			this.txtNATUrno.WordWrap = ((bool)(resources.GetObject("txtNATUrno.WordWrap")));
			this.txtNATUrno.TextChanged += new System.EventHandler(this.HeaderValueChanged);
			// 
			// btnAPT
			// 
			this.btnAPT.AccessibleDescription = resources.GetString("btnAPT.AccessibleDescription");
			this.btnAPT.AccessibleName = resources.GetString("btnAPT.AccessibleName");
			this.btnAPT.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnAPT.Anchor")));
			this.btnAPT.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAPT.BackgroundImage")));
			this.btnAPT.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnAPT.Dock")));
			this.btnAPT.Enabled = ((bool)(resources.GetObject("btnAPT.Enabled")));
			this.btnAPT.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnAPT.FlatStyle")));
			this.btnAPT.Font = ((System.Drawing.Font)(resources.GetObject("btnAPT.Font")));
			this.btnAPT.Image = ((System.Drawing.Image)(resources.GetObject("btnAPT.Image")));
			this.btnAPT.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnAPT.ImageAlign")));
			this.btnAPT.ImageIndex = ((int)(resources.GetObject("btnAPT.ImageIndex")));
			this.btnAPT.ImageList = this.imageList1;
			this.btnAPT.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnAPT.ImeMode")));
			this.btnAPT.Location = ((System.Drawing.Point)(resources.GetObject("btnAPT.Location")));
			this.btnAPT.Name = "btnAPT";
			this.btnAPT.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnAPT.RightToLeft")));
			this.btnAPT.Size = ((System.Drawing.Size)(resources.GetObject("btnAPT.Size")));
			this.btnAPT.TabIndex = ((int)(resources.GetObject("btnAPT.TabIndex")));
			this.btnAPT.Text = resources.GetString("btnAPT.Text");
			this.btnAPT.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnAPT.TextAlign")));
			this.toolTip1.SetToolTip(this.btnAPT, resources.GetString("btnAPT.ToolTip"));
			this.btnAPT.Visible = ((bool)(resources.GetObject("btnAPT.Visible")));
			this.btnAPT.Click += new System.EventHandler(this.btnAPT_Click);
			// 
			// lblAirport
			// 
			this.lblAirport.AccessibleDescription = resources.GetString("lblAirport.AccessibleDescription");
			this.lblAirport.AccessibleName = resources.GetString("lblAirport.AccessibleName");
			this.lblAirport.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblAirport.Anchor")));
			this.lblAirport.AutoSize = ((bool)(resources.GetObject("lblAirport.AutoSize")));
			this.lblAirport.BackColor = System.Drawing.Color.Transparent;
			this.lblAirport.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblAirport.Dock")));
			this.lblAirport.Enabled = ((bool)(resources.GetObject("lblAirport.Enabled")));
			this.lblAirport.Font = ((System.Drawing.Font)(resources.GetObject("lblAirport.Font")));
			this.lblAirport.Image = ((System.Drawing.Image)(resources.GetObject("lblAirport.Image")));
			this.lblAirport.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblAirport.ImageAlign")));
			this.lblAirport.ImageIndex = ((int)(resources.GetObject("lblAirport.ImageIndex")));
			this.lblAirport.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblAirport.ImeMode")));
			this.lblAirport.Location = ((System.Drawing.Point)(resources.GetObject("lblAirport.Location")));
			this.lblAirport.Name = "lblAirport";
			this.lblAirport.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblAirport.RightToLeft")));
			this.lblAirport.Size = ((System.Drawing.Size)(resources.GetObject("lblAirport.Size")));
			this.lblAirport.TabIndex = ((int)(resources.GetObject("lblAirport.TabIndex")));
			this.lblAirport.Text = resources.GetString("lblAirport.Text");
			this.lblAirport.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblAirport.TextAlign")));
			this.toolTip1.SetToolTip(this.lblAirport, resources.GetString("lblAirport.ToolTip"));
			this.lblAirport.Visible = ((bool)(resources.GetObject("lblAirport.Visible")));
			// 
			// txtACTGroupUrno
			// 
			this.txtACTGroupUrno.AccessibleDescription = resources.GetString("txtACTGroupUrno.AccessibleDescription");
			this.txtACTGroupUrno.AccessibleName = resources.GetString("txtACTGroupUrno.AccessibleName");
			this.txtACTGroupUrno.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtACTGroupUrno.Anchor")));
			this.txtACTGroupUrno.AutoSize = ((bool)(resources.GetObject("txtACTGroupUrno.AutoSize")));
			this.txtACTGroupUrno.BackColor = System.Drawing.SystemColors.Window;
			this.txtACTGroupUrno.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtACTGroupUrno.BackgroundImage")));
			this.txtACTGroupUrno.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtACTGroupUrno.Dock")));
			this.txtACTGroupUrno.Enabled = ((bool)(resources.GetObject("txtACTGroupUrno.Enabled")));
			this.txtACTGroupUrno.Font = ((System.Drawing.Font)(resources.GetObject("txtACTGroupUrno.Font")));
			this.txtACTGroupUrno.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtACTGroupUrno.ImeMode")));
			this.txtACTGroupUrno.Location = ((System.Drawing.Point)(resources.GetObject("txtACTGroupUrno.Location")));
			this.txtACTGroupUrno.MaxLength = ((int)(resources.GetObject("txtACTGroupUrno.MaxLength")));
			this.txtACTGroupUrno.Multiline = ((bool)(resources.GetObject("txtACTGroupUrno.Multiline")));
			this.txtACTGroupUrno.Name = "txtACTGroupUrno";
			this.txtACTGroupUrno.PasswordChar = ((char)(resources.GetObject("txtACTGroupUrno.PasswordChar")));
			this.txtACTGroupUrno.ReadOnly = true;
			this.txtACTGroupUrno.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtACTGroupUrno.RightToLeft")));
			this.txtACTGroupUrno.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtACTGroupUrno.ScrollBars")));
			this.txtACTGroupUrno.Size = ((System.Drawing.Size)(resources.GetObject("txtACTGroupUrno.Size")));
			this.txtACTGroupUrno.TabIndex = ((int)(resources.GetObject("txtACTGroupUrno.TabIndex")));
			this.txtACTGroupUrno.TabStop = false;
			this.txtACTGroupUrno.Tag = "ACTG";
			this.txtACTGroupUrno.Text = resources.GetString("txtACTGroupUrno.Text");
			this.txtACTGroupUrno.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtACTGroupUrno.TextAlign")));
			this.toolTip1.SetToolTip(this.txtACTGroupUrno, resources.GetString("txtACTGroupUrno.ToolTip"));
			this.txtACTGroupUrno.Visible = ((bool)(resources.GetObject("txtACTGroupUrno.Visible")));
			this.txtACTGroupUrno.WordWrap = ((bool)(resources.GetObject("txtACTGroupUrno.WordWrap")));
			this.txtACTGroupUrno.TextChanged += new System.EventHandler(this.HeaderValueChanged);
			// 
			// btnNAT
			// 
			this.btnNAT.AccessibleDescription = resources.GetString("btnNAT.AccessibleDescription");
			this.btnNAT.AccessibleName = resources.GetString("btnNAT.AccessibleName");
			this.btnNAT.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnNAT.Anchor")));
			this.btnNAT.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNAT.BackgroundImage")));
			this.btnNAT.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnNAT.Dock")));
			this.btnNAT.Enabled = ((bool)(resources.GetObject("btnNAT.Enabled")));
			this.btnNAT.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnNAT.FlatStyle")));
			this.btnNAT.Font = ((System.Drawing.Font)(resources.GetObject("btnNAT.Font")));
			this.btnNAT.Image = ((System.Drawing.Image)(resources.GetObject("btnNAT.Image")));
			this.btnNAT.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnNAT.ImageAlign")));
			this.btnNAT.ImageIndex = ((int)(resources.GetObject("btnNAT.ImageIndex")));
			this.btnNAT.ImageList = this.imageList1;
			this.btnNAT.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnNAT.ImeMode")));
			this.btnNAT.Location = ((System.Drawing.Point)(resources.GetObject("btnNAT.Location")));
			this.btnNAT.Name = "btnNAT";
			this.btnNAT.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnNAT.RightToLeft")));
			this.btnNAT.Size = ((System.Drawing.Size)(resources.GetObject("btnNAT.Size")));
			this.btnNAT.TabIndex = ((int)(resources.GetObject("btnNAT.TabIndex")));
			this.btnNAT.Text = resources.GetString("btnNAT.Text");
			this.btnNAT.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnNAT.TextAlign")));
			this.toolTip1.SetToolTip(this.btnNAT, resources.GetString("btnNAT.ToolTip"));
			this.btnNAT.Visible = ((bool)(resources.GetObject("btnNAT.Visible")));
			this.btnNAT.Click += new System.EventHandler(this.btnNAT_Click);
			// 
			// txtACTGroupValue
			// 
			this.txtACTGroupValue.AccessibleDescription = resources.GetString("txtACTGroupValue.AccessibleDescription");
			this.txtACTGroupValue.AccessibleName = resources.GetString("txtACTGroupValue.AccessibleName");
			this.txtACTGroupValue.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtACTGroupValue.Anchor")));
			this.txtACTGroupValue.AutoSize = ((bool)(resources.GetObject("txtACTGroupValue.AutoSize")));
			this.txtACTGroupValue.BackColor = System.Drawing.SystemColors.Window;
			this.txtACTGroupValue.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtACTGroupValue.BackgroundImage")));
			this.txtACTGroupValue.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtACTGroupValue.Dock")));
			this.txtACTGroupValue.Enabled = ((bool)(resources.GetObject("txtACTGroupValue.Enabled")));
			this.txtACTGroupValue.Font = ((System.Drawing.Font)(resources.GetObject("txtACTGroupValue.Font")));
			this.txtACTGroupValue.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtACTGroupValue.ImeMode")));
			this.txtACTGroupValue.Location = ((System.Drawing.Point)(resources.GetObject("txtACTGroupValue.Location")));
			this.txtACTGroupValue.MaxLength = ((int)(resources.GetObject("txtACTGroupValue.MaxLength")));
			this.txtACTGroupValue.Multiline = ((bool)(resources.GetObject("txtACTGroupValue.Multiline")));
			this.txtACTGroupValue.Name = "txtACTGroupValue";
			this.txtACTGroupValue.PasswordChar = ((char)(resources.GetObject("txtACTGroupValue.PasswordChar")));
			this.txtACTGroupValue.ReadOnly = true;
			this.txtACTGroupValue.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtACTGroupValue.RightToLeft")));
			this.txtACTGroupValue.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtACTGroupValue.ScrollBars")));
			this.txtACTGroupValue.Size = ((System.Drawing.Size)(resources.GetObject("txtACTGroupValue.Size")));
			this.txtACTGroupValue.TabIndex = ((int)(resources.GetObject("txtACTGroupValue.TabIndex")));
			this.txtACTGroupValue.Tag = "ACTG_VAL";
			this.txtACTGroupValue.Text = resources.GetString("txtACTGroupValue.Text");
			this.txtACTGroupValue.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtACTGroupValue.TextAlign")));
			this.toolTip1.SetToolTip(this.txtACTGroupValue, resources.GetString("txtACTGroupValue.ToolTip"));
			this.txtACTGroupValue.Visible = ((bool)(resources.GetObject("txtACTGroupValue.Visible")));
			this.txtACTGroupValue.WordWrap = ((bool)(resources.GetObject("txtACTGroupValue.WordWrap")));
			this.txtACTGroupValue.MouseHover += new System.EventHandler(this.txtACTGroupValue_MouseHover);
			this.txtACTGroupValue.MouseMove += new System.Windows.Forms.MouseEventHandler(this.txtACTGroupValue_MouseMove);
			// 
			// txtALTGroupUrno
			// 
			this.txtALTGroupUrno.AccessibleDescription = resources.GetString("txtALTGroupUrno.AccessibleDescription");
			this.txtALTGroupUrno.AccessibleName = resources.GetString("txtALTGroupUrno.AccessibleName");
			this.txtALTGroupUrno.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtALTGroupUrno.Anchor")));
			this.txtALTGroupUrno.AutoSize = ((bool)(resources.GetObject("txtALTGroupUrno.AutoSize")));
			this.txtALTGroupUrno.BackColor = System.Drawing.SystemColors.Window;
			this.txtALTGroupUrno.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtALTGroupUrno.BackgroundImage")));
			this.txtALTGroupUrno.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtALTGroupUrno.Dock")));
			this.txtALTGroupUrno.Enabled = ((bool)(resources.GetObject("txtALTGroupUrno.Enabled")));
			this.txtALTGroupUrno.Font = ((System.Drawing.Font)(resources.GetObject("txtALTGroupUrno.Font")));
			this.txtALTGroupUrno.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtALTGroupUrno.ImeMode")));
			this.txtALTGroupUrno.Location = ((System.Drawing.Point)(resources.GetObject("txtALTGroupUrno.Location")));
			this.txtALTGroupUrno.MaxLength = ((int)(resources.GetObject("txtALTGroupUrno.MaxLength")));
			this.txtALTGroupUrno.Multiline = ((bool)(resources.GetObject("txtALTGroupUrno.Multiline")));
			this.txtALTGroupUrno.Name = "txtALTGroupUrno";
			this.txtALTGroupUrno.PasswordChar = ((char)(resources.GetObject("txtALTGroupUrno.PasswordChar")));
			this.txtALTGroupUrno.ReadOnly = true;
			this.txtALTGroupUrno.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtALTGroupUrno.RightToLeft")));
			this.txtALTGroupUrno.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtALTGroupUrno.ScrollBars")));
			this.txtALTGroupUrno.Size = ((System.Drawing.Size)(resources.GetObject("txtALTGroupUrno.Size")));
			this.txtALTGroupUrno.TabIndex = ((int)(resources.GetObject("txtALTGroupUrno.TabIndex")));
			this.txtALTGroupUrno.TabStop = false;
			this.txtALTGroupUrno.Tag = "ALTG";
			this.txtALTGroupUrno.Text = resources.GetString("txtALTGroupUrno.Text");
			this.txtALTGroupUrno.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtALTGroupUrno.TextAlign")));
			this.toolTip1.SetToolTip(this.txtALTGroupUrno, resources.GetString("txtALTGroupUrno.ToolTip"));
			this.txtALTGroupUrno.Visible = ((bool)(resources.GetObject("txtALTGroupUrno.Visible")));
			this.txtALTGroupUrno.WordWrap = ((bool)(resources.GetObject("txtALTGroupUrno.WordWrap")));
			this.txtALTGroupUrno.TextChanged += new System.EventHandler(this.HeaderValueChanged);
			// 
			// btnACTGroup
			// 
			this.btnACTGroup.AccessibleDescription = resources.GetString("btnACTGroup.AccessibleDescription");
			this.btnACTGroup.AccessibleName = resources.GetString("btnACTGroup.AccessibleName");
			this.btnACTGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnACTGroup.Anchor")));
			this.btnACTGroup.BackColor = System.Drawing.SystemColors.Control;
			this.btnACTGroup.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnACTGroup.BackgroundImage")));
			this.btnACTGroup.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnACTGroup.Dock")));
			this.btnACTGroup.Enabled = ((bool)(resources.GetObject("btnACTGroup.Enabled")));
			this.btnACTGroup.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnACTGroup.FlatStyle")));
			this.btnACTGroup.Font = ((System.Drawing.Font)(resources.GetObject("btnACTGroup.Font")));
			this.btnACTGroup.ForeColor = System.Drawing.SystemColors.ControlText;
			this.btnACTGroup.Image = ((System.Drawing.Image)(resources.GetObject("btnACTGroup.Image")));
			this.btnACTGroup.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnACTGroup.ImageAlign")));
			this.btnACTGroup.ImageIndex = ((int)(resources.GetObject("btnACTGroup.ImageIndex")));
			this.btnACTGroup.ImageList = this.imageList1;
			this.btnACTGroup.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnACTGroup.ImeMode")));
			this.btnACTGroup.Location = ((System.Drawing.Point)(resources.GetObject("btnACTGroup.Location")));
			this.btnACTGroup.Name = "btnACTGroup";
			this.btnACTGroup.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnACTGroup.RightToLeft")));
			this.btnACTGroup.Size = ((System.Drawing.Size)(resources.GetObject("btnACTGroup.Size")));
			this.btnACTGroup.TabIndex = ((int)(resources.GetObject("btnACTGroup.TabIndex")));
			this.btnACTGroup.Text = resources.GetString("btnACTGroup.Text");
			this.btnACTGroup.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnACTGroup.TextAlign")));
			this.toolTip1.SetToolTip(this.btnACTGroup, resources.GetString("btnACTGroup.ToolTip"));
			this.btnACTGroup.Visible = ((bool)(resources.GetObject("btnACTGroup.Visible")));
			this.btnACTGroup.Click += new System.EventHandler(this.btnACTGroup_Click);
			// 
			// lblACGroup
			// 
			this.lblACGroup.AccessibleDescription = resources.GetString("lblACGroup.AccessibleDescription");
			this.lblACGroup.AccessibleName = resources.GetString("lblACGroup.AccessibleName");
			this.lblACGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblACGroup.Anchor")));
			this.lblACGroup.AutoSize = ((bool)(resources.GetObject("lblACGroup.AutoSize")));
			this.lblACGroup.BackColor = System.Drawing.Color.Transparent;
			this.lblACGroup.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblACGroup.Dock")));
			this.lblACGroup.Enabled = ((bool)(resources.GetObject("lblACGroup.Enabled")));
			this.lblACGroup.Font = ((System.Drawing.Font)(resources.GetObject("lblACGroup.Font")));
			this.lblACGroup.Image = ((System.Drawing.Image)(resources.GetObject("lblACGroup.Image")));
			this.lblACGroup.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblACGroup.ImageAlign")));
			this.lblACGroup.ImageIndex = ((int)(resources.GetObject("lblACGroup.ImageIndex")));
			this.lblACGroup.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblACGroup.ImeMode")));
			this.lblACGroup.Location = ((System.Drawing.Point)(resources.GetObject("lblACGroup.Location")));
			this.lblACGroup.Name = "lblACGroup";
			this.lblACGroup.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblACGroup.RightToLeft")));
			this.lblACGroup.Size = ((System.Drawing.Size)(resources.GetObject("lblACGroup.Size")));
			this.lblACGroup.TabIndex = ((int)(resources.GetObject("lblACGroup.TabIndex")));
			this.lblACGroup.Text = resources.GetString("lblACGroup.Text");
			this.lblACGroup.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblACGroup.TextAlign")));
			this.toolTip1.SetToolTip(this.lblACGroup, resources.GetString("lblACGroup.ToolTip"));
			this.lblACGroup.Visible = ((bool)(resources.GetObject("lblACGroup.Visible")));
			// 
			// txtACTUrno
			// 
			this.txtACTUrno.AccessibleDescription = resources.GetString("txtACTUrno.AccessibleDescription");
			this.txtACTUrno.AccessibleName = resources.GetString("txtACTUrno.AccessibleName");
			this.txtACTUrno.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtACTUrno.Anchor")));
			this.txtACTUrno.AutoSize = ((bool)(resources.GetObject("txtACTUrno.AutoSize")));
			this.txtACTUrno.BackColor = System.Drawing.SystemColors.Window;
			this.txtACTUrno.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtACTUrno.BackgroundImage")));
			this.txtACTUrno.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtACTUrno.Dock")));
			this.txtACTUrno.Enabled = ((bool)(resources.GetObject("txtACTUrno.Enabled")));
			this.txtACTUrno.Font = ((System.Drawing.Font)(resources.GetObject("txtACTUrno.Font")));
			this.txtACTUrno.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtACTUrno.ImeMode")));
			this.txtACTUrno.Location = ((System.Drawing.Point)(resources.GetObject("txtACTUrno.Location")));
			this.txtACTUrno.MaxLength = ((int)(resources.GetObject("txtACTUrno.MaxLength")));
			this.txtACTUrno.Multiline = ((bool)(resources.GetObject("txtACTUrno.Multiline")));
			this.txtACTUrno.Name = "txtACTUrno";
			this.txtACTUrno.PasswordChar = ((char)(resources.GetObject("txtACTUrno.PasswordChar")));
			this.txtACTUrno.ReadOnly = true;
			this.txtACTUrno.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtACTUrno.RightToLeft")));
			this.txtACTUrno.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtACTUrno.ScrollBars")));
			this.txtACTUrno.Size = ((System.Drawing.Size)(resources.GetObject("txtACTUrno.Size")));
			this.txtACTUrno.TabIndex = ((int)(resources.GetObject("txtACTUrno.TabIndex")));
			this.txtACTUrno.TabStop = false;
			this.txtACTUrno.Tag = "ACTU";
			this.txtACTUrno.Text = resources.GetString("txtACTUrno.Text");
			this.txtACTUrno.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtACTUrno.TextAlign")));
			this.toolTip1.SetToolTip(this.txtACTUrno, resources.GetString("txtACTUrno.ToolTip"));
			this.txtACTUrno.Visible = ((bool)(resources.GetObject("txtACTUrno.Visible")));
			this.txtACTUrno.WordWrap = ((bool)(resources.GetObject("txtACTUrno.WordWrap")));
			this.txtACTUrno.TextChanged += new System.EventHandler(this.HeaderValueChanged);
			// 
			// btnALTGroup
			// 
			this.btnALTGroup.AccessibleDescription = resources.GetString("btnALTGroup.AccessibleDescription");
			this.btnALTGroup.AccessibleName = resources.GetString("btnALTGroup.AccessibleName");
			this.btnALTGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnALTGroup.Anchor")));
			this.btnALTGroup.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnALTGroup.BackgroundImage")));
			this.btnALTGroup.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnALTGroup.Dock")));
			this.btnALTGroup.Enabled = ((bool)(resources.GetObject("btnALTGroup.Enabled")));
			this.btnALTGroup.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnALTGroup.FlatStyle")));
			this.btnALTGroup.Font = ((System.Drawing.Font)(resources.GetObject("btnALTGroup.Font")));
			this.btnALTGroup.Image = ((System.Drawing.Image)(resources.GetObject("btnALTGroup.Image")));
			this.btnALTGroup.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnALTGroup.ImageAlign")));
			this.btnALTGroup.ImageIndex = ((int)(resources.GetObject("btnALTGroup.ImageIndex")));
			this.btnALTGroup.ImageList = this.imageList1;
			this.btnALTGroup.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnALTGroup.ImeMode")));
			this.btnALTGroup.Location = ((System.Drawing.Point)(resources.GetObject("btnALTGroup.Location")));
			this.btnALTGroup.Name = "btnALTGroup";
			this.btnALTGroup.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnALTGroup.RightToLeft")));
			this.btnALTGroup.Size = ((System.Drawing.Size)(resources.GetObject("btnALTGroup.Size")));
			this.btnALTGroup.TabIndex = ((int)(resources.GetObject("btnALTGroup.TabIndex")));
			this.btnALTGroup.Text = resources.GetString("btnALTGroup.Text");
			this.btnALTGroup.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnALTGroup.TextAlign")));
			this.toolTip1.SetToolTip(this.btnALTGroup, resources.GetString("btnALTGroup.ToolTip"));
			this.btnALTGroup.Visible = ((bool)(resources.GetObject("btnALTGroup.Visible")));
			this.btnALTGroup.Click += new System.EventHandler(this.btnALTGroup_Click);
			// 
			// txtACTValue
			// 
			this.txtACTValue.AccessibleDescription = resources.GetString("txtACTValue.AccessibleDescription");
			this.txtACTValue.AccessibleName = resources.GetString("txtACTValue.AccessibleName");
			this.txtACTValue.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtACTValue.Anchor")));
			this.txtACTValue.AutoSize = ((bool)(resources.GetObject("txtACTValue.AutoSize")));
			this.txtACTValue.BackColor = System.Drawing.SystemColors.Window;
			this.txtACTValue.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtACTValue.BackgroundImage")));
			this.txtACTValue.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtACTValue.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtACTValue.Dock")));
			this.txtACTValue.Enabled = ((bool)(resources.GetObject("txtACTValue.Enabled")));
			this.txtACTValue.Font = ((System.Drawing.Font)(resources.GetObject("txtACTValue.Font")));
			this.txtACTValue.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtACTValue.ImeMode")));
			this.txtACTValue.Location = ((System.Drawing.Point)(resources.GetObject("txtACTValue.Location")));
			this.txtACTValue.MaxLength = ((int)(resources.GetObject("txtACTValue.MaxLength")));
			this.txtACTValue.Multiline = ((bool)(resources.GetObject("txtACTValue.Multiline")));
			this.txtACTValue.Name = "txtACTValue";
			this.txtACTValue.PasswordChar = ((char)(resources.GetObject("txtACTValue.PasswordChar")));
			this.txtACTValue.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtACTValue.RightToLeft")));
			this.txtACTValue.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtACTValue.ScrollBars")));
			this.txtACTValue.Size = ((System.Drawing.Size)(resources.GetObject("txtACTValue.Size")));
			this.txtACTValue.TabIndex = ((int)(resources.GetObject("txtACTValue.TabIndex")));
			this.txtACTValue.Tag = "ACTU_VAL";
			this.txtACTValue.Text = resources.GetString("txtACTValue.Text");
			this.txtACTValue.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtACTValue.TextAlign")));
			this.toolTip1.SetToolTip(this.txtACTValue, resources.GetString("txtACTValue.ToolTip"));
			this.txtACTValue.Visible = ((bool)(resources.GetObject("txtACTValue.Visible")));
			this.txtACTValue.WordWrap = ((bool)(resources.GetObject("txtACTValue.WordWrap")));
			this.txtACTValue.Leave += new System.EventHandler(this.txtACTValue_Leave);
			// 
			// txtALTUrno
			// 
			this.txtALTUrno.AccessibleDescription = resources.GetString("txtALTUrno.AccessibleDescription");
			this.txtALTUrno.AccessibleName = resources.GetString("txtALTUrno.AccessibleName");
			this.txtALTUrno.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtALTUrno.Anchor")));
			this.txtALTUrno.AutoSize = ((bool)(resources.GetObject("txtALTUrno.AutoSize")));
			this.txtALTUrno.BackColor = System.Drawing.SystemColors.Window;
			this.txtALTUrno.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtALTUrno.BackgroundImage")));
			this.txtALTUrno.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtALTUrno.Dock")));
			this.txtALTUrno.Enabled = ((bool)(resources.GetObject("txtALTUrno.Enabled")));
			this.txtALTUrno.Font = ((System.Drawing.Font)(resources.GetObject("txtALTUrno.Font")));
			this.txtALTUrno.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtALTUrno.ImeMode")));
			this.txtALTUrno.Location = ((System.Drawing.Point)(resources.GetObject("txtALTUrno.Location")));
			this.txtALTUrno.MaxLength = ((int)(resources.GetObject("txtALTUrno.MaxLength")));
			this.txtALTUrno.Multiline = ((bool)(resources.GetObject("txtALTUrno.Multiline")));
			this.txtALTUrno.Name = "txtALTUrno";
			this.txtALTUrno.PasswordChar = ((char)(resources.GetObject("txtALTUrno.PasswordChar")));
			this.txtALTUrno.ReadOnly = true;
			this.txtALTUrno.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtALTUrno.RightToLeft")));
			this.txtALTUrno.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtALTUrno.ScrollBars")));
			this.txtALTUrno.Size = ((System.Drawing.Size)(resources.GetObject("txtALTUrno.Size")));
			this.txtALTUrno.TabIndex = ((int)(resources.GetObject("txtALTUrno.TabIndex")));
			this.txtALTUrno.TabStop = false;
			this.txtALTUrno.Tag = "ALTU";
			this.txtALTUrno.Text = resources.GetString("txtALTUrno.Text");
			this.txtALTUrno.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtALTUrno.TextAlign")));
			this.toolTip1.SetToolTip(this.txtALTUrno, resources.GetString("txtALTUrno.ToolTip"));
			this.txtALTUrno.Visible = ((bool)(resources.GetObject("txtALTUrno.Visible")));
			this.txtALTUrno.WordWrap = ((bool)(resources.GetObject("txtALTUrno.WordWrap")));
			this.txtALTUrno.TextChanged += new System.EventHandler(this.HeaderValueChanged);
			// 
			// btnACT
			// 
			this.btnACT.AccessibleDescription = resources.GetString("btnACT.AccessibleDescription");
			this.btnACT.AccessibleName = resources.GetString("btnACT.AccessibleName");
			this.btnACT.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnACT.Anchor")));
			this.btnACT.BackColor = System.Drawing.SystemColors.Control;
			this.btnACT.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnACT.BackgroundImage")));
			this.btnACT.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnACT.Dock")));
			this.btnACT.Enabled = ((bool)(resources.GetObject("btnACT.Enabled")));
			this.btnACT.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnACT.FlatStyle")));
			this.btnACT.Font = ((System.Drawing.Font)(resources.GetObject("btnACT.Font")));
			this.btnACT.ForeColor = System.Drawing.SystemColors.ControlText;
			this.btnACT.Image = ((System.Drawing.Image)(resources.GetObject("btnACT.Image")));
			this.btnACT.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnACT.ImageAlign")));
			this.btnACT.ImageIndex = ((int)(resources.GetObject("btnACT.ImageIndex")));
			this.btnACT.ImageList = this.imageList1;
			this.btnACT.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnACT.ImeMode")));
			this.btnACT.Location = ((System.Drawing.Point)(resources.GetObject("btnACT.Location")));
			this.btnACT.Name = "btnACT";
			this.btnACT.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnACT.RightToLeft")));
			this.btnACT.Size = ((System.Drawing.Size)(resources.GetObject("btnACT.Size")));
			this.btnACT.TabIndex = ((int)(resources.GetObject("btnACT.TabIndex")));
			this.btnACT.Text = resources.GetString("btnACT.Text");
			this.btnACT.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnACT.TextAlign")));
			this.toolTip1.SetToolTip(this.btnACT, resources.GetString("btnACT.ToolTip"));
			this.btnACT.Visible = ((bool)(resources.GetObject("btnACT.Visible")));
			this.btnACT.Click += new System.EventHandler(this.btnACT_Click);
			// 
			// lblACType
			// 
			this.lblACType.AccessibleDescription = resources.GetString("lblACType.AccessibleDescription");
			this.lblACType.AccessibleName = resources.GetString("lblACType.AccessibleName");
			this.lblACType.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblACType.Anchor")));
			this.lblACType.AutoSize = ((bool)(resources.GetObject("lblACType.AutoSize")));
			this.lblACType.BackColor = System.Drawing.Color.Transparent;
			this.lblACType.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblACType.Dock")));
			this.lblACType.Enabled = ((bool)(resources.GetObject("lblACType.Enabled")));
			this.lblACType.Font = ((System.Drawing.Font)(resources.GetObject("lblACType.Font")));
			this.lblACType.Image = ((System.Drawing.Image)(resources.GetObject("lblACType.Image")));
			this.lblACType.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblACType.ImageAlign")));
			this.lblACType.ImageIndex = ((int)(resources.GetObject("lblACType.ImageIndex")));
			this.lblACType.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblACType.ImeMode")));
			this.lblACType.Location = ((System.Drawing.Point)(resources.GetObject("lblACType.Location")));
			this.lblACType.Name = "lblACType";
			this.lblACType.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblACType.RightToLeft")));
			this.lblACType.Size = ((System.Drawing.Size)(resources.GetObject("lblACType.Size")));
			this.lblACType.TabIndex = ((int)(resources.GetObject("lblACType.TabIndex")));
			this.lblACType.Text = resources.GetString("lblACType.Text");
			this.lblACType.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblACType.TextAlign")));
			this.toolTip1.SetToolTip(this.lblACType, resources.GetString("lblACType.ToolTip"));
			this.lblACType.Visible = ((bool)(resources.GetObject("lblACType.Visible")));
			// 
			// btnALT
			// 
			this.btnALT.AccessibleDescription = resources.GetString("btnALT.AccessibleDescription");
			this.btnALT.AccessibleName = resources.GetString("btnALT.AccessibleName");
			this.btnALT.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnALT.Anchor")));
			this.btnALT.BackColor = System.Drawing.SystemColors.Control;
			this.btnALT.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnALT.BackgroundImage")));
			this.btnALT.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnALT.Dock")));
			this.btnALT.Enabled = ((bool)(resources.GetObject("btnALT.Enabled")));
			this.btnALT.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnALT.FlatStyle")));
			this.btnALT.Font = ((System.Drawing.Font)(resources.GetObject("btnALT.Font")));
			this.btnALT.Image = ((System.Drawing.Image)(resources.GetObject("btnALT.Image")));
			this.btnALT.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnALT.ImageAlign")));
			this.btnALT.ImageIndex = ((int)(resources.GetObject("btnALT.ImageIndex")));
			this.btnALT.ImageList = this.imageList1;
			this.btnALT.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnALT.ImeMode")));
			this.btnALT.Location = ((System.Drawing.Point)(resources.GetObject("btnALT.Location")));
			this.btnALT.Name = "btnALT";
			this.btnALT.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnALT.RightToLeft")));
			this.btnALT.Size = ((System.Drawing.Size)(resources.GetObject("btnALT.Size")));
			this.btnALT.TabIndex = ((int)(resources.GetObject("btnALT.TabIndex")));
			this.btnALT.Text = resources.GetString("btnALT.Text");
			this.btnALT.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnALT.TextAlign")));
			this.toolTip1.SetToolTip(this.btnALT, resources.GetString("btnALT.ToolTip"));
			this.btnALT.Visible = ((bool)(resources.GetObject("btnALT.Visible")));
			this.btnALT.Click += new System.EventHandler(this.btnALT_Click);
			// 
			// lblAirline
			// 
			this.lblAirline.AccessibleDescription = resources.GetString("lblAirline.AccessibleDescription");
			this.lblAirline.AccessibleName = resources.GetString("lblAirline.AccessibleName");
			this.lblAirline.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblAirline.Anchor")));
			this.lblAirline.AutoSize = ((bool)(resources.GetObject("lblAirline.AutoSize")));
			this.lblAirline.BackColor = System.Drawing.Color.Transparent;
			this.lblAirline.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblAirline.Dock")));
			this.lblAirline.Enabled = ((bool)(resources.GetObject("lblAirline.Enabled")));
			this.lblAirline.Font = ((System.Drawing.Font)(resources.GetObject("lblAirline.Font")));
			this.lblAirline.Image = ((System.Drawing.Image)(resources.GetObject("lblAirline.Image")));
			this.lblAirline.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblAirline.ImageAlign")));
			this.lblAirline.ImageIndex = ((int)(resources.GetObject("lblAirline.ImageIndex")));
			this.lblAirline.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblAirline.ImeMode")));
			this.lblAirline.Location = ((System.Drawing.Point)(resources.GetObject("lblAirline.Location")));
			this.lblAirline.Name = "lblAirline";
			this.lblAirline.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblAirline.RightToLeft")));
			this.lblAirline.Size = ((System.Drawing.Size)(resources.GetObject("lblAirline.Size")));
			this.lblAirline.TabIndex = ((int)(resources.GetObject("lblAirline.TabIndex")));
			this.lblAirline.Text = resources.GetString("lblAirline.Text");
			this.lblAirline.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblAirline.TextAlign")));
			this.toolTip1.SetToolTip(this.lblAirline, resources.GetString("lblAirline.ToolTip"));
			this.lblAirline.Visible = ((bool)(resources.GetObject("lblAirline.Visible")));
			// 
			// txtRuleName
			// 
			this.txtRuleName.AccessibleDescription = resources.GetString("txtRuleName.AccessibleDescription");
			this.txtRuleName.AccessibleName = resources.GetString("txtRuleName.AccessibleName");
			this.txtRuleName.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtRuleName.Anchor")));
			this.txtRuleName.AutoSize = ((bool)(resources.GetObject("txtRuleName.AutoSize")));
			this.txtRuleName.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(255)), ((System.Byte)(192)));
			this.txtRuleName.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtRuleName.BackgroundImage")));
			this.txtRuleName.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtRuleName.Dock")));
			this.txtRuleName.Enabled = ((bool)(resources.GetObject("txtRuleName.Enabled")));
			this.txtRuleName.Font = ((System.Drawing.Font)(resources.GetObject("txtRuleName.Font")));
			this.txtRuleName.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtRuleName.ImeMode")));
			this.txtRuleName.Location = ((System.Drawing.Point)(resources.GetObject("txtRuleName.Location")));
			this.txtRuleName.MaxLength = ((int)(resources.GetObject("txtRuleName.MaxLength")));
			this.txtRuleName.Multiline = ((bool)(resources.GetObject("txtRuleName.Multiline")));
			this.txtRuleName.Name = "txtRuleName";
			this.txtRuleName.PasswordChar = ((char)(resources.GetObject("txtRuleName.PasswordChar")));
			this.txtRuleName.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtRuleName.RightToLeft")));
			this.txtRuleName.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtRuleName.ScrollBars")));
			this.txtRuleName.Size = ((System.Drawing.Size)(resources.GetObject("txtRuleName.Size")));
			this.txtRuleName.TabIndex = ((int)(resources.GetObject("txtRuleName.TabIndex")));
			this.txtRuleName.Tag = "NAME";
			this.txtRuleName.Text = resources.GetString("txtRuleName.Text");
			this.txtRuleName.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtRuleName.TextAlign")));
			this.toolTip1.SetToolTip(this.txtRuleName, resources.GetString("txtRuleName.ToolTip"));
			this.txtRuleName.Visible = ((bool)(resources.GetObject("txtRuleName.Visible")));
			this.txtRuleName.WordWrap = ((bool)(resources.GetObject("txtRuleName.WordWrap")));
			this.txtRuleName.TextChanged += new System.EventHandler(this.HeaderValueChanged);
			// 
			// lblRuleName
			// 
			this.lblRuleName.AccessibleDescription = resources.GetString("lblRuleName.AccessibleDescription");
			this.lblRuleName.AccessibleName = resources.GetString("lblRuleName.AccessibleName");
			this.lblRuleName.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblRuleName.Anchor")));
			this.lblRuleName.AutoSize = ((bool)(resources.GetObject("lblRuleName.AutoSize")));
			this.lblRuleName.BackColor = System.Drawing.Color.Transparent;
			this.lblRuleName.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblRuleName.Dock")));
			this.lblRuleName.Enabled = ((bool)(resources.GetObject("lblRuleName.Enabled")));
			this.lblRuleName.Font = ((System.Drawing.Font)(resources.GetObject("lblRuleName.Font")));
			this.lblRuleName.Image = ((System.Drawing.Image)(resources.GetObject("lblRuleName.Image")));
			this.lblRuleName.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblRuleName.ImageAlign")));
			this.lblRuleName.ImageIndex = ((int)(resources.GetObject("lblRuleName.ImageIndex")));
			this.lblRuleName.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblRuleName.ImeMode")));
			this.lblRuleName.Location = ((System.Drawing.Point)(resources.GetObject("lblRuleName.Location")));
			this.lblRuleName.Name = "lblRuleName";
			this.lblRuleName.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblRuleName.RightToLeft")));
			this.lblRuleName.Size = ((System.Drawing.Size)(resources.GetObject("lblRuleName.Size")));
			this.lblRuleName.TabIndex = ((int)(resources.GetObject("lblRuleName.TabIndex")));
			this.lblRuleName.Text = resources.GetString("lblRuleName.Text");
			this.lblRuleName.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblRuleName.TextAlign")));
			this.toolTip1.SetToolTip(this.lblRuleName, resources.GetString("lblRuleName.ToolTip"));
			this.lblRuleName.Visible = ((bool)(resources.GetObject("lblRuleName.Visible")));
			// 
			// txtAPTGroupUrno
			// 
			this.txtAPTGroupUrno.AccessibleDescription = resources.GetString("txtAPTGroupUrno.AccessibleDescription");
			this.txtAPTGroupUrno.AccessibleName = resources.GetString("txtAPTGroupUrno.AccessibleName");
			this.txtAPTGroupUrno.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtAPTGroupUrno.Anchor")));
			this.txtAPTGroupUrno.AutoSize = ((bool)(resources.GetObject("txtAPTGroupUrno.AutoSize")));
			this.txtAPTGroupUrno.BackColor = System.Drawing.SystemColors.Window;
			this.txtAPTGroupUrno.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtAPTGroupUrno.BackgroundImage")));
			this.txtAPTGroupUrno.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtAPTGroupUrno.Dock")));
			this.txtAPTGroupUrno.Enabled = ((bool)(resources.GetObject("txtAPTGroupUrno.Enabled")));
			this.txtAPTGroupUrno.Font = ((System.Drawing.Font)(resources.GetObject("txtAPTGroupUrno.Font")));
			this.txtAPTGroupUrno.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtAPTGroupUrno.ImeMode")));
			this.txtAPTGroupUrno.Location = ((System.Drawing.Point)(resources.GetObject("txtAPTGroupUrno.Location")));
			this.txtAPTGroupUrno.MaxLength = ((int)(resources.GetObject("txtAPTGroupUrno.MaxLength")));
			this.txtAPTGroupUrno.Multiline = ((bool)(resources.GetObject("txtAPTGroupUrno.Multiline")));
			this.txtAPTGroupUrno.Name = "txtAPTGroupUrno";
			this.txtAPTGroupUrno.PasswordChar = ((char)(resources.GetObject("txtAPTGroupUrno.PasswordChar")));
			this.txtAPTGroupUrno.ReadOnly = true;
			this.txtAPTGroupUrno.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtAPTGroupUrno.RightToLeft")));
			this.txtAPTGroupUrno.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtAPTGroupUrno.ScrollBars")));
			this.txtAPTGroupUrno.Size = ((System.Drawing.Size)(resources.GetObject("txtAPTGroupUrno.Size")));
			this.txtAPTGroupUrno.TabIndex = ((int)(resources.GetObject("txtAPTGroupUrno.TabIndex")));
			this.txtAPTGroupUrno.TabStop = false;
			this.txtAPTGroupUrno.Tag = "APTG";
			this.txtAPTGroupUrno.Text = resources.GetString("txtAPTGroupUrno.Text");
			this.txtAPTGroupUrno.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtAPTGroupUrno.TextAlign")));
			this.toolTip1.SetToolTip(this.txtAPTGroupUrno, resources.GetString("txtAPTGroupUrno.ToolTip"));
			this.txtAPTGroupUrno.Visible = ((bool)(resources.GetObject("txtAPTGroupUrno.Visible")));
			this.txtAPTGroupUrno.WordWrap = ((bool)(resources.GetObject("txtAPTGroupUrno.WordWrap")));
			this.txtAPTGroupUrno.TextChanged += new System.EventHandler(this.HeaderValueChanged);
			// 
			// txtAPTGroupValue
			// 
			this.txtAPTGroupValue.AccessibleDescription = resources.GetString("txtAPTGroupValue.AccessibleDescription");
			this.txtAPTGroupValue.AccessibleName = resources.GetString("txtAPTGroupValue.AccessibleName");
			this.txtAPTGroupValue.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtAPTGroupValue.Anchor")));
			this.txtAPTGroupValue.AutoSize = ((bool)(resources.GetObject("txtAPTGroupValue.AutoSize")));
			this.txtAPTGroupValue.BackColor = System.Drawing.SystemColors.Window;
			this.txtAPTGroupValue.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtAPTGroupValue.BackgroundImage")));
			this.txtAPTGroupValue.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtAPTGroupValue.Dock")));
			this.txtAPTGroupValue.Enabled = ((bool)(resources.GetObject("txtAPTGroupValue.Enabled")));
			this.txtAPTGroupValue.Font = ((System.Drawing.Font)(resources.GetObject("txtAPTGroupValue.Font")));
			this.txtAPTGroupValue.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtAPTGroupValue.ImeMode")));
			this.txtAPTGroupValue.Location = ((System.Drawing.Point)(resources.GetObject("txtAPTGroupValue.Location")));
			this.txtAPTGroupValue.MaxLength = ((int)(resources.GetObject("txtAPTGroupValue.MaxLength")));
			this.txtAPTGroupValue.Multiline = ((bool)(resources.GetObject("txtAPTGroupValue.Multiline")));
			this.txtAPTGroupValue.Name = "txtAPTGroupValue";
			this.txtAPTGroupValue.PasswordChar = ((char)(resources.GetObject("txtAPTGroupValue.PasswordChar")));
			this.txtAPTGroupValue.ReadOnly = true;
			this.txtAPTGroupValue.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtAPTGroupValue.RightToLeft")));
			this.txtAPTGroupValue.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtAPTGroupValue.ScrollBars")));
			this.txtAPTGroupValue.Size = ((System.Drawing.Size)(resources.GetObject("txtAPTGroupValue.Size")));
			this.txtAPTGroupValue.TabIndex = ((int)(resources.GetObject("txtAPTGroupValue.TabIndex")));
			this.txtAPTGroupValue.Tag = "APTG_VAL";
			this.txtAPTGroupValue.Text = resources.GetString("txtAPTGroupValue.Text");
			this.txtAPTGroupValue.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtAPTGroupValue.TextAlign")));
			this.toolTip1.SetToolTip(this.txtAPTGroupValue, resources.GetString("txtAPTGroupValue.ToolTip"));
			this.txtAPTGroupValue.Visible = ((bool)(resources.GetObject("txtAPTGroupValue.Visible")));
			this.txtAPTGroupValue.WordWrap = ((bool)(resources.GetObject("txtAPTGroupValue.WordWrap")));
			this.txtAPTGroupValue.MouseHover += new System.EventHandler(this.txtAPTGroupValue_MouseHover);
			this.txtAPTGroupValue.MouseMove += new System.Windows.Forms.MouseEventHandler(this.txtAPTGroupValue_MouseMove);
			// 
			// btnAPTGroup
			// 
			this.btnAPTGroup.AccessibleDescription = resources.GetString("btnAPTGroup.AccessibleDescription");
			this.btnAPTGroup.AccessibleName = resources.GetString("btnAPTGroup.AccessibleName");
			this.btnAPTGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnAPTGroup.Anchor")));
			this.btnAPTGroup.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAPTGroup.BackgroundImage")));
			this.btnAPTGroup.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnAPTGroup.Dock")));
			this.btnAPTGroup.Enabled = ((bool)(resources.GetObject("btnAPTGroup.Enabled")));
			this.btnAPTGroup.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnAPTGroup.FlatStyle")));
			this.btnAPTGroup.Font = ((System.Drawing.Font)(resources.GetObject("btnAPTGroup.Font")));
			this.btnAPTGroup.Image = ((System.Drawing.Image)(resources.GetObject("btnAPTGroup.Image")));
			this.btnAPTGroup.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnAPTGroup.ImageAlign")));
			this.btnAPTGroup.ImageIndex = ((int)(resources.GetObject("btnAPTGroup.ImageIndex")));
			this.btnAPTGroup.ImageList = this.imageList1;
			this.btnAPTGroup.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnAPTGroup.ImeMode")));
			this.btnAPTGroup.Location = ((System.Drawing.Point)(resources.GetObject("btnAPTGroup.Location")));
			this.btnAPTGroup.Name = "btnAPTGroup";
			this.btnAPTGroup.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnAPTGroup.RightToLeft")));
			this.btnAPTGroup.Size = ((System.Drawing.Size)(resources.GetObject("btnAPTGroup.Size")));
			this.btnAPTGroup.TabIndex = ((int)(resources.GetObject("btnAPTGroup.TabIndex")));
			this.btnAPTGroup.Text = resources.GetString("btnAPTGroup.Text");
			this.btnAPTGroup.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnAPTGroup.TextAlign")));
			this.toolTip1.SetToolTip(this.btnAPTGroup, resources.GetString("btnAPTGroup.ToolTip"));
			this.btnAPTGroup.Visible = ((bool)(resources.GetObject("btnAPTGroup.Visible")));
			this.btnAPTGroup.Click += new System.EventHandler(this.btnAPTGroup_Click);
			// 
			// rbArrival
			// 
			this.rbArrival.AccessibleDescription = resources.GetString("rbArrival.AccessibleDescription");
			this.rbArrival.AccessibleName = resources.GetString("rbArrival.AccessibleName");
			this.rbArrival.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("rbArrival.Anchor")));
			this.rbArrival.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("rbArrival.Appearance")));
			this.rbArrival.BackColor = System.Drawing.Color.LightGreen;
			this.rbArrival.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("rbArrival.BackgroundImage")));
			this.rbArrival.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("rbArrival.CheckAlign")));
			this.rbArrival.Checked = true;
			this.rbArrival.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("rbArrival.Dock")));
			this.rbArrival.Enabled = ((bool)(resources.GetObject("rbArrival.Enabled")));
			this.rbArrival.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("rbArrival.FlatStyle")));
			this.rbArrival.Font = ((System.Drawing.Font)(resources.GetObject("rbArrival.Font")));
			this.rbArrival.ForeColor = System.Drawing.SystemColors.ControlText;
			this.rbArrival.Image = ((System.Drawing.Image)(resources.GetObject("rbArrival.Image")));
			this.rbArrival.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("rbArrival.ImageAlign")));
			this.rbArrival.ImageIndex = ((int)(resources.GetObject("rbArrival.ImageIndex")));
			this.rbArrival.ImageList = this.imageList1;
			this.rbArrival.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("rbArrival.ImeMode")));
			this.rbArrival.Location = ((System.Drawing.Point)(resources.GetObject("rbArrival.Location")));
			this.rbArrival.Name = "rbArrival";
			this.rbArrival.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("rbArrival.RightToLeft")));
			this.rbArrival.Size = ((System.Drawing.Size)(resources.GetObject("rbArrival.Size")));
			this.rbArrival.TabIndex = ((int)(resources.GetObject("rbArrival.TabIndex")));
			this.rbArrival.TabStop = true;
			this.rbArrival.Text = resources.GetString("rbArrival.Text");
			this.rbArrival.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("rbArrival.TextAlign")));
			this.toolTip1.SetToolTip(this.rbArrival, resources.GetString("rbArrival.ToolTip"));
			this.rbArrival.Visible = ((bool)(resources.GetObject("rbArrival.Visible")));
			this.rbArrival.CheckedChanged += new System.EventHandler(this.rbArrival_CheckedChanged);
			// 
			// rbDeparture
			// 
			this.rbDeparture.AccessibleDescription = resources.GetString("rbDeparture.AccessibleDescription");
			this.rbDeparture.AccessibleName = resources.GetString("rbDeparture.AccessibleName");
			this.rbDeparture.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("rbDeparture.Anchor")));
			this.rbDeparture.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("rbDeparture.Appearance")));
			this.rbDeparture.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("rbDeparture.BackgroundImage")));
			this.rbDeparture.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("rbDeparture.CheckAlign")));
			this.rbDeparture.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("rbDeparture.Dock")));
			this.rbDeparture.Enabled = ((bool)(resources.GetObject("rbDeparture.Enabled")));
			this.rbDeparture.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("rbDeparture.FlatStyle")));
			this.rbDeparture.Font = ((System.Drawing.Font)(resources.GetObject("rbDeparture.Font")));
			this.rbDeparture.ForeColor = System.Drawing.SystemColors.ControlText;
			this.rbDeparture.Image = ((System.Drawing.Image)(resources.GetObject("rbDeparture.Image")));
			this.rbDeparture.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("rbDeparture.ImageAlign")));
			this.rbDeparture.ImageIndex = ((int)(resources.GetObject("rbDeparture.ImageIndex")));
			this.rbDeparture.ImageList = this.imageList1;
			this.rbDeparture.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("rbDeparture.ImeMode")));
			this.rbDeparture.Location = ((System.Drawing.Point)(resources.GetObject("rbDeparture.Location")));
			this.rbDeparture.Name = "rbDeparture";
			this.rbDeparture.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("rbDeparture.RightToLeft")));
			this.rbDeparture.Size = ((System.Drawing.Size)(resources.GetObject("rbDeparture.Size")));
			this.rbDeparture.TabIndex = ((int)(resources.GetObject("rbDeparture.TabIndex")));
			this.rbDeparture.Text = resources.GetString("rbDeparture.Text");
			this.rbDeparture.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("rbDeparture.TextAlign")));
			this.toolTip1.SetToolTip(this.rbDeparture, resources.GetString("rbDeparture.ToolTip"));
			this.rbDeparture.Visible = ((bool)(resources.GetObject("rbDeparture.Visible")));
			// 
			// pictureBox3
			// 
			this.pictureBox3.AccessibleDescription = resources.GetString("pictureBox3.AccessibleDescription");
			this.pictureBox3.AccessibleName = resources.GetString("pictureBox3.AccessibleName");
			this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("pictureBox3.Anchor")));
			this.pictureBox3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox3.BackgroundImage")));
			this.pictureBox3.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("pictureBox3.Dock")));
			this.pictureBox3.Enabled = ((bool)(resources.GetObject("pictureBox3.Enabled")));
			this.pictureBox3.Font = ((System.Drawing.Font)(resources.GetObject("pictureBox3.Font")));
			this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
			this.pictureBox3.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("pictureBox3.ImeMode")));
			this.pictureBox3.Location = ((System.Drawing.Point)(resources.GetObject("pictureBox3.Location")));
			this.pictureBox3.Name = "pictureBox3";
			this.pictureBox3.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("pictureBox3.RightToLeft")));
			this.pictureBox3.Size = ((System.Drawing.Size)(resources.GetObject("pictureBox3.Size")));
			this.pictureBox3.SizeMode = ((System.Windows.Forms.PictureBoxSizeMode)(resources.GetObject("pictureBox3.SizeMode")));
			this.pictureBox3.TabIndex = ((int)(resources.GetObject("pictureBox3.TabIndex")));
			this.pictureBox3.TabStop = false;
			this.pictureBox3.Text = resources.GetString("pictureBox3.Text");
			this.toolTip1.SetToolTip(this.pictureBox3, resources.GetString("pictureBox3.ToolTip"));
			this.pictureBox3.Visible = ((bool)(resources.GetObject("pictureBox3.Visible")));
			this.pictureBox3.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox3_Paint);
			// 
			// label1
			// 
			this.label1.AccessibleDescription = resources.GetString("label1.AccessibleDescription");
			this.label1.AccessibleName = resources.GetString("label1.AccessibleName");
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label1.Anchor")));
			this.label1.AutoSize = ((bool)(resources.GetObject("label1.AutoSize")));
			this.label1.BackColor = System.Drawing.Color.Black;
			this.label1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label1.Dock")));
			this.label1.Enabled = ((bool)(resources.GetObject("label1.Enabled")));
			this.label1.Font = ((System.Drawing.Font)(resources.GetObject("label1.Font")));
			this.label1.ForeColor = System.Drawing.Color.Lime;
			this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
			this.label1.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label1.ImageAlign")));
			this.label1.ImageIndex = ((int)(resources.GetObject("label1.ImageIndex")));
			this.label1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label1.ImeMode")));
			this.label1.Location = ((System.Drawing.Point)(resources.GetObject("label1.Location")));
			this.label1.Name = "label1";
			this.label1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label1.RightToLeft")));
			this.label1.Size = ((System.Drawing.Size)(resources.GetObject("label1.Size")));
			this.label1.TabIndex = ((int)(resources.GetObject("label1.TabIndex")));
			this.label1.Text = resources.GetString("label1.Text");
			this.label1.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label1.TextAlign")));
			this.toolTip1.SetToolTip(this.label1, resources.GetString("label1.ToolTip"));
			this.label1.Visible = ((bool)(resources.GetObject("label1.Visible")));
			// 
			// lblEvent
			// 
			this.lblEvent.AccessibleDescription = resources.GetString("lblEvent.AccessibleDescription");
			this.lblEvent.AccessibleName = resources.GetString("lblEvent.AccessibleName");
			this.lblEvent.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblEvent.Anchor")));
			this.lblEvent.AutoSize = ((bool)(resources.GetObject("lblEvent.AutoSize")));
			this.lblEvent.BackColor = System.Drawing.Color.Black;
			this.lblEvent.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblEvent.Dock")));
			this.lblEvent.Enabled = ((bool)(resources.GetObject("lblEvent.Enabled")));
			this.lblEvent.Font = ((System.Drawing.Font)(resources.GetObject("lblEvent.Font")));
			this.lblEvent.ForeColor = System.Drawing.Color.Lime;
			this.lblEvent.Image = ((System.Drawing.Image)(resources.GetObject("lblEvent.Image")));
			this.lblEvent.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblEvent.ImageAlign")));
			this.lblEvent.ImageIndex = ((int)(resources.GetObject("lblEvent.ImageIndex")));
			this.lblEvent.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblEvent.ImeMode")));
			this.lblEvent.Location = ((System.Drawing.Point)(resources.GetObject("lblEvent.Location")));
			this.lblEvent.Name = "lblEvent";
			this.lblEvent.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblEvent.RightToLeft")));
			this.lblEvent.Size = ((System.Drawing.Size)(resources.GetObject("lblEvent.Size")));
			this.lblEvent.TabIndex = ((int)(resources.GetObject("lblEvent.TabIndex")));
			this.lblEvent.Text = resources.GetString("lblEvent.Text");
			this.lblEvent.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblEvent.TextAlign")));
			this.toolTip1.SetToolTip(this.lblEvent, resources.GetString("lblEvent.ToolTip"));
			this.lblEvent.Visible = ((bool)(resources.GetObject("lblEvent.Visible")));
			// 
			// tabDefs
			// 
			this.tabDefs.AccessibleDescription = resources.GetString("tabDefs.AccessibleDescription");
			this.tabDefs.AccessibleName = resources.GetString("tabDefs.AccessibleName");
			this.tabDefs.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("tabDefs.Anchor")));
			this.tabDefs.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabDefs.BackgroundImage")));
			this.tabDefs.ContainingControl = this;
			this.tabDefs.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("tabDefs.Dock")));
			this.tabDefs.Font = ((System.Drawing.Font)(resources.GetObject("tabDefs.Font")));
			this.tabDefs.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("tabDefs.ImeMode")));
			this.tabDefs.Location = ((System.Drawing.Point)(resources.GetObject("tabDefs.Location")));
			this.tabDefs.Name = "tabDefs";
			this.tabDefs.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabDefs.OcxState")));
			this.tabDefs.RightToLeft = ((bool)(resources.GetObject("tabDefs.RightToLeft")));
			this.tabDefs.Size = ((System.Drawing.Size)(resources.GetObject("tabDefs.Size")));
			this.tabDefs.TabIndex = ((int)(resources.GetObject("tabDefs.TabIndex")));
			this.tabDefs.TabStop = false;
			this.tabDefs.Text = resources.GetString("tabDefs.Text");
			this.toolTip1.SetToolTip(this.tabDefs, resources.GetString("tabDefs.ToolTip"));
			this.tabDefs.Visible = ((bool)(resources.GetObject("tabDefs.Visible")));
			this.tabDefs.CloseInplaceEdit += new AxTABLib._DTABEvents_CloseInplaceEditEventHandler(this.tabDefs_CloseInplaceEdit);
			this.tabDefs.ComboSelChanged += new AxTABLib._DTABEvents_ComboSelChangedEventHandler(this.tabDefs_ComboSelChanged);
			this.tabDefs.RowSelectionChanged += new AxTABLib._DTABEvents_RowSelectionChangedEventHandler(this.tabDefs_RowSelectionChanged);
			this.tabDefs.EditPositionChanged += new AxTABLib._DTABEvents_EditPositionChangedEventHandler(this.tabDefs_EditPositionChanged);
			// 
			// pictureBox2
			// 
			this.pictureBox2.AccessibleDescription = resources.GetString("pictureBox2.AccessibleDescription");
			this.pictureBox2.AccessibleName = resources.GetString("pictureBox2.AccessibleName");
			this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("pictureBox2.Anchor")));
			this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
			this.pictureBox2.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("pictureBox2.Dock")));
			this.pictureBox2.Enabled = ((bool)(resources.GetObject("pictureBox2.Enabled")));
			this.pictureBox2.Font = ((System.Drawing.Font)(resources.GetObject("pictureBox2.Font")));
			this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
			this.pictureBox2.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("pictureBox2.ImeMode")));
			this.pictureBox2.Location = ((System.Drawing.Point)(resources.GetObject("pictureBox2.Location")));
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("pictureBox2.RightToLeft")));
			this.pictureBox2.Size = ((System.Drawing.Size)(resources.GetObject("pictureBox2.Size")));
			this.pictureBox2.SizeMode = ((System.Windows.Forms.PictureBoxSizeMode)(resources.GetObject("pictureBox2.SizeMode")));
			this.pictureBox2.TabIndex = ((int)(resources.GetObject("pictureBox2.TabIndex")));
			this.pictureBox2.TabStop = false;
			this.pictureBox2.Text = resources.GetString("pictureBox2.Text");
			this.toolTip1.SetToolTip(this.pictureBox2, resources.GetString("pictureBox2.ToolTip"));
			this.pictureBox2.Visible = ((bool)(resources.GetObject("pictureBox2.Visible")));
			this.pictureBox2.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox2_Paint);
			// 
			// numericUpDown1
			// 
			this.numericUpDown1.AccessibleDescription = resources.GetString("numericUpDown1.AccessibleDescription");
			this.numericUpDown1.AccessibleName = resources.GetString("numericUpDown1.AccessibleName");
			this.numericUpDown1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("numericUpDown1.Anchor")));
			this.numericUpDown1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("numericUpDown1.Dock")));
			this.numericUpDown1.Enabled = ((bool)(resources.GetObject("numericUpDown1.Enabled")));
			this.numericUpDown1.Font = ((System.Drawing.Font)(resources.GetObject("numericUpDown1.Font")));
			this.numericUpDown1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("numericUpDown1.ImeMode")));
			this.numericUpDown1.Location = ((System.Drawing.Point)(resources.GetObject("numericUpDown1.Location")));
			this.numericUpDown1.Name = "numericUpDown1";
			this.numericUpDown1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("numericUpDown1.RightToLeft")));
			this.numericUpDown1.Size = ((System.Drawing.Size)(resources.GetObject("numericUpDown1.Size")));
			this.numericUpDown1.TabIndex = ((int)(resources.GetObject("numericUpDown1.TabIndex")));
			this.numericUpDown1.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("numericUpDown1.TextAlign")));
			this.numericUpDown1.ThousandsSeparator = ((bool)(resources.GetObject("numericUpDown1.ThousandsSeparator")));
			this.toolTip1.SetToolTip(this.numericUpDown1, resources.GetString("numericUpDown1.ToolTip"));
			this.numericUpDown1.UpDownAlign = ((System.Windows.Forms.LeftRightAlignment)(resources.GetObject("numericUpDown1.UpDownAlign")));
			this.numericUpDown1.Visible = ((bool)(resources.GetObject("numericUpDown1.Visible")));
			// 
			// mnuContextDetail
			// 
			this.mnuContextDetail.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																							 this.mnuReactivate});
			this.mnuContextDetail.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("mnuContextDetail.RightToLeft")));
			// 
			// mnuReactivate
			// 
			this.mnuReactivate.Enabled = ((bool)(resources.GetObject("mnuReactivate.Enabled")));
			this.mnuReactivate.Index = 0;
			this.mnuReactivate.Shortcut = ((System.Windows.Forms.Shortcut)(resources.GetObject("mnuReactivate.Shortcut")));
			this.mnuReactivate.ShowShortcut = ((bool)(resources.GetObject("mnuReactivate.ShowShortcut")));
			this.mnuReactivate.Text = resources.GetString("mnuReactivate.Text");
			this.mnuReactivate.Visible = ((bool)(resources.GetObject("mnuReactivate.Visible")));
			// 
			// frmRules
			// 
			this.AccessibleDescription = resources.GetString("$this.AccessibleDescription");
			this.AccessibleName = resources.GetString("$this.AccessibleName");
			this.AutoScaleBaseSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScaleBaseSize")));
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMargin")));
			this.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMinSize")));
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = ((System.Drawing.Size)(resources.GetObject("$this.ClientSize")));
			this.Controls.Add(this.panelHeader);
			this.Controls.Add(this.panel1);
			this.Enabled = ((bool)(resources.GetObject("$this.Enabled")));
			this.Font = ((System.Drawing.Font)(resources.GetObject("$this.Font")));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.KeyPreview = true;
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.MaximizeBox = false;
			this.MaximumSize = ((System.Drawing.Size)(resources.GetObject("$this.MaximumSize")));
			this.MinimizeBox = false;
			this.MinimumSize = ((System.Drawing.Size)(resources.GetObject("$this.MinimumSize")));
			this.Name = "frmRules";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.StartPosition = ((System.Windows.Forms.FormStartPosition)(resources.GetObject("$this.StartPosition")));
			this.Text = resources.GetString("$this.Text");
			this.toolTip1.SetToolTip(this, resources.GetString("$this.ToolTip"));
			this.Closing += new System.ComponentModel.CancelEventHandler(this.frmRules_Closing);
			this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmRules_KeyPress);
			this.Load += new System.EventHandler(this.frmRules_Load);
			this.Closed += new System.EventHandler(this.frmRules_Closed);
			this.panel1.ResumeLayout(false);
			this.panelHeader.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabDefs)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void pictureBox1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, pictureBox1, Color.WhiteSmoke, Color.Gray);			
		}

		private void btnACT_Click(object sender, System.EventArgs e)
		{
			System.Windows.Forms.DialogResult olResult;
			frmChoiceList olDlg = new frmChoiceList();
			olDlg.currTable = "ACT";
			olDlg.lblCaption.Text = "Aircrafts";
			olResult = olDlg.ShowDialog();
			if(olResult == DialogResult.OK)
			{
				int sel = olDlg.tabList.GetCurrentSelected();
				string strUrno = olDlg.tabList.GetColumnValue(sel,0);
				txtACTUrno.Text = strUrno;
				string val1 = olDlg.tabList.GetColumnValue(sel,1);
				string val2 = olDlg.tabList.GetColumnValue(sel,2);
				string strValue = "";
				if(val1 == "" || val2 == "")
					strValue = val1 + val2;
				else
					strValue = val1 + "/" + val2;
				txtACTValue.Text = strValue;
				txtACTGroupUrno.Text = "";
				txtACTGroupValue.Text = "";
				txtACTValue.ForeColor = Color.Black;
			}
		}

		private void btnAPT_Click(object sender, System.EventArgs e)
		{
			System.Windows.Forms.DialogResult olResult;
			frmChoiceList olDlg = new frmChoiceList();
			olDlg.currTable = "APT";
			olDlg.lblCaption.Text = "Airports";
			olResult = olDlg.ShowDialog();
			if(olResult == DialogResult.OK)
			{
				int sel = olDlg.tabList.GetCurrentSelected();
				string strUrno = olDlg.tabList.GetColumnValue(sel,0);
				txtAPTUrno.Text = strUrno;
				string val1 = olDlg.tabList.GetColumnValue(sel,1);
				string val2 = olDlg.tabList.GetColumnValue(sel,2);
				string strValue = "";
				if(val1 == "" || val2 == "")
					strValue = val1 + val2;
				else
					strValue = val1 + "/" + val2;
				txtAPTValue.Text = strValue;
				txtAPTGroupValue.Text = "";
				txtAPTGroupUrno.Text = "";
				txtAPTValue.ForeColor = Color.Black;
			}
		}

		private void btnALT_Click(object sender, System.EventArgs e)
		{
			System.Windows.Forms.DialogResult olResult;
			frmChoiceList olDlg = new frmChoiceList();
			olDlg.lblCaption.Text = "Airlines";
			olDlg.currTable = "ALT";
			olResult = olDlg.ShowDialog();
			if(olResult == DialogResult.OK)
			{
				int sel = olDlg.tabList.GetCurrentSelected();
				string strUrno = olDlg.tabList.GetColumnValue(sel,0);
				txtALTUrno.Text = strUrno;
				string val1 = olDlg.tabList.GetColumnValue(sel,1);
				string val2 = olDlg.tabList.GetColumnValue(sel,2);
				string strValue = "";
				if(val1 == "" || val2 == "")
					strValue = val1 + val2;
				else
					strValue = val1 + "/" + val2;
				txtALTValue.Text = strValue;
				txtALTGroupValue.Text = "";
				txtALTGroupUrno.Text = "";
				txtALTValue.ForeColor = Color.Black;
			}
		}

		private void btnNAT_Click(object sender, System.EventArgs e)
		{
			System.Windows.Forms.DialogResult olResult;
			frmChoiceList olDlg = new frmChoiceList();
			olDlg.currTable = "NAT";
			olDlg.lblCaption.Text = "Natures";
			olResult = olDlg.ShowDialog();
			if(olResult == DialogResult.OK)
			{
				int sel = olDlg.tabList.GetCurrentSelected();
				string strUrno = olDlg.tabList.GetColumnValue(sel,0);
				txtNATUrno.Text = strUrno;
				txtNATValue.Text = olDlg.tabList.GetColumnValue(sel,2);
				txtNATGroupValue.Text = "";
				txtNATGroupUrno.Text = "";
				txtNATValue.ForeColor = Color.Black;
			}
		}

		private void btnACTGroup_Click(object sender, System.EventArgs e)
		{
			System.Windows.Forms.DialogResult olResult;
			frmChoiceList olDlg = new frmChoiceList();
			olDlg.isGroup = true;
			olDlg.lblCaption.Text = "Aircraft-Groups";
			olDlg.currTable = "ACT";
			olResult = olDlg.ShowDialog();
			if(olResult == DialogResult.OK)
			{
				int sel = olDlg.tabList.GetCurrentSelected();
				string strUrno = olDlg.tabList.GetColumnValue(sel,0);
				txtACTGroupUrno.Text = strUrno;
				txtACTGroupValue.Text = olDlg.tabList.GetColumnValue(sel,1);
				txtACTValue.Text = "";
				txtACTUrno.Text = "";
				txtACTValue.ForeColor = Color.Black;
			}
		}

		private void btnAPTGroup_Click(object sender, System.EventArgs e)
		{
			System.Windows.Forms.DialogResult olResult;
			frmChoiceList olDlg = new frmChoiceList();
			olDlg.isGroup = true;
			olDlg.lblCaption.Text = "Airport-Groups";
			olDlg.currTable = "APT";
			olResult = olDlg.ShowDialog();
			if(olResult == DialogResult.OK)
			{
				int sel = olDlg.tabList.GetCurrentSelected();
				string strUrno = olDlg.tabList.GetColumnValue(sel,0);
				txtAPTGroupUrno.Text = strUrno;
				txtAPTGroupValue.Text = olDlg.tabList.GetColumnValue(sel,1);
				txtAPTValue.Text = "";
				txtAPTUrno.Text = "";
				txtAPTValue.ForeColor = Color.Black;
			}
		}

		private void btnALTGroup_Click(object sender, System.EventArgs e)
		{
			System.Windows.Forms.DialogResult olResult;
			frmChoiceList olDlg = new frmChoiceList();
			olDlg.isGroup = true;
			olDlg.lblCaption.Text = "Airline-Groups";
			olDlg.currTable = "ALT";
			olResult = olDlg.ShowDialog();
			if(olResult == DialogResult.OK)
			{
				int sel = olDlg.tabList.GetCurrentSelected();
				string strUrno = olDlg.tabList.GetColumnValue(sel,0);
				txtALTGroupUrno.Text = strUrno;
				txtALTGroupValue.Text = olDlg.tabList.GetColumnValue(sel,1);
				txtALTValue.Text = "";
				txtALTUrno.Text = "";
				txtALTValue.ForeColor = Color.Black;
			}
		}

		private void btnNATGroup_Click(object sender, System.EventArgs e)
		{
			System.Windows.Forms.DialogResult olResult;
			frmChoiceList olDlg = new frmChoiceList();
			olDlg.isGroup = true;
			olDlg.currTable = "NAT";
			olDlg.lblCaption.Text = "Nature-Groups";
			olResult = olDlg.ShowDialog();
			if(olResult == DialogResult.OK)
			{
				int sel = olDlg.tabList.GetCurrentSelected();
				string strUrno = olDlg.tabList.GetColumnValue(sel,0);
				txtNATGroupUrno.Text = strUrno;
				txtNATGroupValue.Text = olDlg.tabList.GetColumnValue(sel,1);
				txtNATValue.Text = "";
				txtNATUrno.Text = "";
				txtNATValue.ForeColor = Color.Black;
			}
		}
		private void btnSTS_Click(object sender, System.EventArgs e)
		{
			System.Windows.Forms.DialogResult olResult;
			frmChoiceList olDlg = new frmChoiceList();
			olDlg.lblCaption.Text = "Status Sections";
			olDlg.currTable = "HSS";
			olResult = olDlg.ShowDialog();
			if(olResult == DialogResult.OK)
			{
				int sel = olDlg.tabList.GetCurrentSelected();
				string strUrno = olDlg.tabList.GetColumnValue(sel,0);
				txtSTSUrno.Text = strUrno;
				txtSTSValue.Text = olDlg.tabList.GetColumnValue(sel,1);
			}
		}


		private void frmRules_Load(object sender, System.EventArgs e)
		{
			
			//init the configuration for dynamical accessing the fields and values
			string strURNOFields  = "UHSS,ACTU,ACTG,APTU,APTG,ALTU,ALTG,NATU,NATG";
			string strVALUEFields = "UHSS_VAL,ACTU_VAL,ACTG_VAL,APTU_VAL,APTG_VAL,ALTU_VAL,ALTG_VAL,NATU_VAL,NATG_VAL";
			string strLookupTabs  = "HSS,ACT,GRN,APT,GRN,ALT,GRN,NAT,GRN";
			string strLookupFields= "NAME,ACT3|ACT5,GRPN,APC3|APC4,GRPN,ALC2|ALC3,GRPN,TNAM,GRPN";
			Ufis.Utils.IniFile ini = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			string strRETY_DB  = ini.IniReadValue("STATUSMANAGER", "RETY_DB");
			string strRETY_GUI = ini.IniReadValue("STATUSMANAGER", "RETY_GUI");
			string strRTIM_DB  = ini.IniReadValue("STATUSMANAGER", "RTIM_DB");
			string strRTIM_GUI = ini.IniReadValue("STATUSMANAGER", "RTIM_GUI");
			string strATFF_DB_ARR  = ini.IniReadValue("STATUSMANAGER", "AFTF_DB_ARRIVAL");
			string strATFF_GUI_ARR  = ini.IniReadValue("STATUSMANAGER", "AFTF_GUI_ARRIVAL");
			string strATFF_DB_DEP  = ini.IniReadValue("STATUSMANAGER", "AFTF_DB_DEPARTURE");
			string strATFF_GUI_DEP  = ini.IniReadValue("STATUSMANAGER", "AFTF_GUI_DEPARTURE");
			myRETY_DB = strRETY_DB.Split(',');
			myRETY_GUI = strRETY_GUI.Split(',');
			myRTIM_DB = strRTIM_DB.Split(',');
			myRTIM_GUI = strRTIM_GUI.Split(',');
			myAFTF_DB_ARR = strATFF_DB_ARR .Split(',');
			myAFTF_GUI_ARR = strATFF_GUI_ARR.Split(',');
			myAFTF_DB_DEP = strATFF_DB_DEP.Split(',');
			myAFTF_GUI_DEP = strATFF_GUI_DEP.Split(',');
			//Check the ini entries
			string strMessage = "";
			strMessage = CheckCedaIniEntries();
			if(strMessage != "")
			{
				//for the case of inconsistent ini entries to no run the
				//rules editor
				strMessage += "Rule-Editor cannot be used!";
				MessageBox.Show(this, strMessage, "ERROR!!!!!!!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
				this.Close();
				return;
			}

			myDB = UT.GetMemDB();


			myURNOFields   = strURNOFields.Split(',');  
			myVALUEFields  = strVALUEFields.Split(',');
			myLookupTabs   = strLookupTabs.Split(',');
			myLookupFields =strLookupFields.Split(',');

			btnSave.Parent = pictureBox1;
			btnRecalcFlights.Parent = pictureBox1;
			btnOpen.Parent = pictureBox1;
			btnNew.Parent = pictureBox1;
			btnDeleteHeader.Parent = pictureBox1;
			btnStatusSection.Parent = pictureBox1;
			btnCopy.Parent = pictureBox1;
			btnClose.Parent = pictureBox1;
			lblACType.Parent = pictureBox3;
			lblAirline.Parent = pictureBox3;
			lblAirport.Parent = pictureBox3;
			lblAirportGroup.Parent = pictureBox3;
			lblNature.Parent = pictureBox3;
			lblNatureGroup.Parent = pictureBox3;
			lblRuleName.Parent = pictureBox3;
			lblACGroup.Parent = pictureBox3;
			lblAirineGroup.Parent = pictureBox3;
			lblValidFrom.Parent = pictureBox3;
			lblValidTo.Parent = pictureBox3;
			lblCDAT.Parent = pictureBox3;
			lblLSTU.Parent = pictureBox3;
			lblUSEC.Parent = pictureBox3;
			lblUSEU.Parent = pictureBox3;
			lblSection.Parent = pictureBox3;
			btnACT.Parent = pictureBox3;
			btnACTGroup.Parent = pictureBox3;
			txtRuleName.Parent = pictureBox3;
			rbArrival.Parent = pictureBox3;
			rbDeparture.Parent = pictureBox3;
			rbTurnaround.Parent = pictureBox3;
			lblMAXT.Parent = pictureBox3;
			lblInMin.Parent = pictureBox3;


			DateTime olFrom;
			DateTime olTo;
			olFrom = DateTime.Now;
			olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 0,0,0,0);
			olTo = new DateTime(2030, 12, 31, 23,59,59,0);
			dtValidFrom.CustomFormat = "dd.MM.yyyy - HH:mm";
			dtValidTo.CustomFormat = "dd.MM.yyyy - HH:mm";
			oldValidFrom = olFrom;
			oldValidTo = olTo;

//URNO,USRH,NAME,RETY,SDEU,AFTF,CCAF,UTPL,USRV,RTIM,FLDR,TIFR
//URNO	
//USRH	Urno auf SRHTAB
//NAME	Definitionsname
//RETY	Referenztyp(JOBx,AFT,CCA,OHNE)
//SDEU	Urno auf vorher definierten SDETAB record
//AFTF	Feldname, wenn RETY=AFT
//CCAF	Feldname, wenn RETY=CCA
//UTPL	Urno auf Template, wenn RETY = JOBx
//USRV	Evtl. URNO des Service, wenn ben�tigt
//RTIM	Referenztyp (AFT oder STAT)
//FLDR	Referenzfeld (STOA,ETAI,LAND,ONBL,STOD,ETDI,OFBL)
//TIFR	Referenz Zeitrahmen
			tabDefs.ResetContent();
			tabDefs.HeaderString = "URNO,USRH,Name,RETY,Type,AFTF,Flight,UTPL,RMS Rule-Template,Ref.-Time,SDEU,Predecessor,USRV,RTIM,RTIM_C,Time(min.),DELF";
			tabDefs.LogicalFieldList = "URNO,USRH,NAME,RETY,RETY_C,AFTF,AFTF_C,UTPL,TNAM,FLDR,SDEU,SDEU_C,USRV,RTIM,RTIM_C,TIFR,DELF";//tabDefs.HeaderString;
			tabDefs.HeaderLengthString  = "0,0,200,0,80,0,100,0,200,80,0,200,0,0,0,80,0";
			tabDefs.ColumnWidthString = "10,10,32,10,10,10,10,10,32,10,64,10,10,10,10,10,1";
			tabDefs.LifeStyle = true;
			tabDefs.LineHeight = 20;
			int hash = txtACTGroupUrno.GetHashCode();
			hash = txtACTGroupValue.GetHashCode();
			tabDefs.FontName = "Arial";
			//tabDefs.FontName = "Courier New";
			tabDefs.FontSize = 14;
			tabDefs.HeaderFontSize = 14;
			tabDefs.SetTabFontBold(true);
			tabDefs.ShowHorzScroller(true);
			string strColor = UT.colBlue + "," + UT.colBlue;
			tabDefs.CursorDecoration(tabDefs.LogicalFieldList, "B,T", "2,2", strColor);
			tabDefs.DefaultCursor = false;
			tabDefs.InplaceEditUpperCase = false;
			// 0    1    2     3    4      5    6     7     8    9   10    11    12   13   14     15
			//URNO,USRH,NAME,RETY,RETY_C,AFTF,AFTF_C,UTPL,TNAM,FLDR,SDEU,SDEU_C,USRV,RTIM,RTIM_C,TIFR
			tabDefs.NoFocusColumns = "0,1,3,4,5,6,7,8,9,10,11,12,13,14,16";

			NewRule();
			MakeTabCombos();
			tabDefs.EnableInlineEdit(true);
			ReadRegistry();
		}
		private void ReadRegistry()
		{
			int myX=-1, myY=-1, myW=-1, myH=-1;
			string regVal = "";
			string theKey = "Software\\StatusManager\\RuleEditor";
			RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			if(rk != null)
			{
				regVal = rk.GetValue("X", "-1").ToString();
				myX = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Y", "-1").ToString();
				myY = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Width", "-1").ToString();
				myW = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Height", "-1").ToString();
				myH = Convert.ToInt32(regVal);
			}
			if((myW != -1 && myH != -1) && (myX < Screen.PrimaryScreen.Bounds.Width))
			{
				this.Left = myX;
				this.Top = myY;
				this.Width = myW;
				this.Height = myH;
			}
		}

		private string CheckCedaIniEntries()
		{
			string strMessage="";
			if(myRETY_DB.Length != myRETY_GUI.Length)
			{
				strMessage += "Ceda.ini [STATUSMANAGER]-Section Failure:\n ";
				strMessage += "RETY_DB and RETY_GUI";
				strMessage += " must have the same number of entries (comma separated)!!\nUNABLE TO RUN RULES!!!\n\n";
			}
			if(myRTIM_DB.Length != myRTIM_GUI.Length)
			{
				strMessage += "Ceda.ini [STATUSMANAGER]-Section Failure:\n ";
				strMessage += "RTIM_DB and RTIM_GUI";
				strMessage += " must have the same number of entries (comma separated)!!\nUNABLE TO RUN RULES!!!\n\n";
			}
			if(myAFTF_DB_ARR.Length != myAFTF_GUI_ARR.Length)
			{
				strMessage += "Ceda.ini [STATUSMANAGER]-Section Failure:\n ";
				strMessage += "AFTF_DB_ARRIVAL and AFTF_GUI_ARRIVAL";
				strMessage += " must have the same number of entries (comma separated)!!\nUNABLE TO RUN RULES!!!\n\n";
			}
			if(myAFTF_DB_DEP.Length != myAFTF_GUI_DEP.Length)
			{
				strMessage += "Ceda.ini [STATUSMANAGER]-Section Failure:\n ";
				strMessage += "AFTF_DB_DEPARTURE and AFTF_GUI_DEPARTURE";
				strMessage += " must have the same number of entries (comma separated)!!\nUNABLE TO RUN RULES!!!\n\n";
			}
			return strMessage;
		}

		private void MakeTabCombos()
		{
			string strComboValues;
			string strName = "";
			int llIDX = -1;
			int i = 0;

			strName = "TNAM";
			llIDX = UT.GetItemNo(tabDefs.LogicalFieldList, strName);
			strComboValues = "|"; 
			ITable myTab = myDB["TPL"];
			if(myTab != null)
			{
				for( i = 0; i < myTab.Count; i++)
				{
					strComboValues += myTab[i]["TNAM"].ToString() + "|";//strComboValues + "FLIGHT|";
					//strComboValues = strComboValues + "STATUS|";
				}
			}
			tabDefs.ComboOwnerDraw = false;
			tabDefs.ComboResetObject( strName );
			tabDefs.CreateComboObject(strName, /*8*/llIDX, 1, "", 300);
			tabDefs.ComboSetColumnLengthString(strName, "64");
			tabDefs.SetComboColumn(strName, llIDX);
			tabDefs.ComboAddTextLines(strName, strComboValues, "|");

			strName = "RETY_C";
			llIDX = UT.GetItemNo(tabDefs.LogicalFieldList, strName);
			strComboValues = "|"; 
			for( i = 0; i < myRETY_DB.Length; i++)
			{
				strComboValues = strComboValues + myRETY_GUI[i] + "|";
			}
			tabDefs.ComboOwnerDraw = false;
			tabDefs.ComboResetObject( strName );
			tabDefs.CreateComboObject(strName, llIDX, 1, "", 0);
			tabDefs.ComboSetColumnLengthString(strName, "80");
			tabDefs.SetComboColumn(strName, llIDX);
			tabDefs.ComboAddTextLines(strName, strComboValues, "|");

			strName = "AFTF_C";
			llIDX = UT.GetItemNo(tabDefs.LogicalFieldList, strName);
			strComboValues = "|"; 
			if(rbArrival.Checked == true)
			{
				for( i = 0; i < myAFTF_DB_ARR.Length; i++)
				{
					strComboValues = strComboValues + myAFTF_GUI_ARR[i] + "|";
				}
			}
			else if(rbDeparture.Checked == true)
			{
				for( i = 0; i < myAFTF_DB_DEP.Length; i++)
				{
					strComboValues = strComboValues + myAFTF_GUI_DEP[i] + "|";
				}
			}
			else //Turnaround
			{
				for( i = 0; i < myAFTF_DB_ARR.Length; i++)
				{
					strComboValues = strComboValues + myAFTF_GUI_ARR[i] + "|";
				}
				for( i = 0; i < myAFTF_DB_DEP.Length; i++)
				{
					strComboValues = strComboValues + myAFTF_GUI_DEP[i] + "|";
				}
			}
			tabDefs.ComboOwnerDraw = false;
			tabDefs.ComboResetObject( strName );
			tabDefs.CreateComboObject(strName, llIDX, 1, "", 0);
			tabDefs.ComboSetColumnLengthString(strName, "80");
			tabDefs.SetComboColumn(strName, llIDX);
			tabDefs.ComboAddTextLines(strName, strComboValues, "|");


//			strName = "RTIM_C";
//			llIDX = UT.GetItemNo(tabDefs.LogicalFieldList, strName);
//			strComboValues = "|"; 
//			for( i = 0; i < myRTIM_DB.Length; i++)
//			{
//				strComboValues = strComboValues + myRTIM_GUI[i] + "|";
//			}
//			tabDefs.ComboOwnerDraw = false;
//			tabDefs.ComboResetObject( strName );
//			tabDefs.CreateComboObject(strName, llIDX, 1, "", 0);
//			tabDefs.ComboSetColumnLengthString(strName, "80");
//			tabDefs.SetComboColumn(strName, llIDX);
//			tabDefs.ComboAddTextLines(strName, strComboValues, "|");

			RebuildRefTimesCombo();
//			strName = "FLDR";
//			llIDX = UT.GetItemNo(tabDefs.LogicalFieldList, strName);
//			strComboValues = "|"; 
//			strComboValues = strComboValues + "STOA|";
//			strComboValues = strComboValues + "ETAI|";
//			strComboValues = strComboValues + "ONBL|";
//			strComboValues = strComboValues + "OFBL|";
//			strComboValues = strComboValues + "STOD|";
//			strComboValues = strComboValues + "ETDI|";
//			strComboValues = strComboValues + "CONA|";
//			tabDefs.ComboOwnerDraw = false;
//			tabDefs.ComboResetObject( strName );
//			tabDefs.CreateComboObject(strName, llIDX, 1, "", 0);
//			tabDefs.ComboSetColumnLengthString(strName, "80");
//			tabDefs.SetComboColumn(strName, llIDX);
//			tabDefs.ComboAddTextLines(strName, strComboValues, "|");

			strName = "SDEU_C";
			llIDX = UT.GetItemNo(tabDefs.LogicalFieldList, strName);
			strComboValues = "|"; 
			tabDefs.ComboOwnerDraw = false;
			tabDefs.ComboResetObject( strName );
			tabDefs.CreateComboObject(strName, llIDX, 1, "", 0);
			tabDefs.ComboSetColumnLengthString(strName, "200");
			tabDefs.SetComboColumn(strName, llIDX);
			tabDefs.ComboAddTextLines(strName, strComboValues, "|");


			tabDefs.ComboMinWidth = 10;
		}
		/// <summary>
		/// Enables/Disables the comboboxes of LineNo according to RETY ("Type") selection
		/// </summary>
		/// <param name="LineNo">Line number of tabDefs</param>
		void EnableComboboxes(int LineNo)
		{
			int idxRETY = UT.GetItemNo(tabDefs.LogicalFieldList, "RETY_C");
			string strFLDR = tabDefs.GetFieldValue(LineNo, "FLDR");
			string strRety = tabDefs.GetFieldValue(LineNo, "RETY");
			//"URNO,USRH,NAME,RETY,RETY_C,AFTF,AFTF_C,UTPL,TNAM,FLDR,SDEU,SDEU_C,USRV,RTIM,RTIM_C,TIFR"
			if( strRety == "")
			{
				tabDefs.ComboShow("TNAM", false);
				tabDefs.ComboShow("SDEU_C", false);
				tabDefs.ComboShow("FLDR", false);
				tabDefs.ComboShow("AFTF_C", false);
				PredecessorVisible = false;
			}
			if( strRety == "AFT")
			{
				tabDefs.ComboShow("TNAM", false);
				tabDefs.ComboShow("SDEU_C", false);
				tabDefs.ComboShow("FLDR", true);
				tabDefs.ComboShow("AFTF_C", true);
				PredecessorVisible = false;
				tabDefs.SetFieldValues(LineNo, "UTPL,TNAM,SDEU,SDEU_C", ",,,");
			}
			if( strRety == "CCAB" || strRety == "CCAE")
			{
				tabDefs.ComboShow("TNAM", false);
				tabDefs.ComboShow("AFTF_C", false);
				tabDefs.ComboShow("SDEU_C", true);
				tabDefs.ComboShow("FLDR", true);
				PredecessorVisible = false;
				tabDefs.SetFieldValues(LineNo, "AFTF,AFTF_C,UTPL,TNAM,", ",,,");
			}
			if( strRety == "JOBI" || strRety == "JOBF" || strRety == "JOBC")
			{
				tabDefs.ComboShow("TNAM", true);
				tabDefs.ComboShow("AFTF_C", false);
				tabDefs.ComboShow("SDEU_C", true);
				tabDefs.ComboShow("FLDR", true);
				PredecessorVisible = false;
				tabDefs.SetFieldValues(LineNo, "AFTF,AFTF_C", ",");
			}
			if(strFLDR == "CONA")
			{
				tabDefs.ComboShow("SDEU_C", true);
				//RebuildComboPredecessorStatus();
				PredecessorVisible = true;
			}
			else
			{
				tabDefs.ComboShow("SDEU_C", false);
				PredecessorVisible = false;
				tabDefs.Refresh();
			}
			RebuildRefTimesCombo();
			//CheckValuesChanged(LineNo);
		}
		/// <summary>
		/// Reload the predecessor combobox with the name's column values
		/// </summary>
		void RebuildComboPredecessorStatus()
		{
			if(PredecessorVisible == true)
			{
				int curSel = tabDefs.GetCurrentSelected();
				string strName = "SDEU_C";
				int llIDX = UT.GetItemNo(tabDefs.LogicalFieldList, strName);
				string strComboValues = "|"; 
				tabDefs.ComboResetContent(strName );
				for(int i = 0; i < tabDefs.GetLineCount(); i++)
				{
					if(i != curSel)
					{
						strComboValues += tabDefs.GetFieldValue(i, "NAME") + "|";
					}
				}
//				tabDefs.ComboOwnerDraw = false;
//				tabDefs.ComboResetObject( strName );
//				tabDefs.CreateComboObject(strName, llIDX, 1, "", 0);
//				tabDefs.ComboSetColumnLengthString(strName, "200");
//				tabDefs.SetComboColumn(strName, llIDX);
				tabDefs.ComboAddTextLines(strName, strComboValues, "|");
			}
		}
		void RebuildRefTimesCombo()
		{
			string strComboValues="";
			string strName = "FLDR";
			int llIDX = UT.GetItemNo(tabDefs.LogicalFieldList, strName);
			if(rbArrival.Checked == true)
			{
				strComboValues = "|"; 
				strComboValues = strComboValues + "STOA|";
				strComboValues = strComboValues + "ETAI|";
				strComboValues = strComboValues + "ONBL|";
				strComboValues = strComboValues + "CONA|";
			}
			else if(rbDeparture.Checked == true)
			{
				strComboValues = "|"; 
				strComboValues = strComboValues + "STOD|";
				strComboValues = strComboValues + "ETDI|";
				strComboValues = strComboValues + "OFBL|";
				strComboValues = strComboValues + "CONA|";
			}
			else //Turnaround
			{
				strComboValues = "|"; 
				strComboValues = strComboValues + "STOA|";
				strComboValues = strComboValues + "ETAI|";
				strComboValues = strComboValues + "ONBL|";
				strComboValues = strComboValues + "STOD|";
				strComboValues = strComboValues + "ETDI|";
				strComboValues = strComboValues + "OFBL|";
				strComboValues = strComboValues + "CONA|";
			}
			int LineNo = tabDefs.GetCurrentSelected();
			if(LineNo > -1)
			{
				string strRety = tabDefs.GetFieldValue(LineNo, "RETY");
				string strAFTF = tabDefs.GetFieldValue(LineNo, "AFTF");
				if( 
					(strRety == "AFT" && (strAFTF == "ONBL" || strAFTF == "OFBL")) ||
					strRety == "JOBI"
					)
				{;}
				else
				{
					strComboValues = strComboValues + "PLAN|";
				}
			}
			tabDefs.ComboOwnerDraw = false;
			tabDefs.ComboResetObject( strName );
			tabDefs.CreateComboObject(strName, llIDX, 1, "", 0);
			tabDefs.ComboSetColumnLengthString(strName, "80");
			tabDefs.SetComboColumn(strName, llIDX);
			tabDefs.ComboAddTextLines(strName, strComboValues, "|");
		}
		private void NewRule()
		{
			if(btnSave.BackColor == Color.Red )
			{
				DialogResult olResult;
				olResult = MessageBox.Show(this, "Do you want to save current rule?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question); 
				if ( olResult == DialogResult.Yes)
				{
					if(SaveRule() == false) return; // Do not NEW!!
				}
			}
			tabDefs.ResetContent();
			tabDefs.Refresh();
			ITable mySRHTab = myDB["SRH"];
			if(mySRHTab != null)
			{
				currDBRow = mySRHTab.CreateEmptyRow();
				currRow   = currDBRow.CopyRaw();
				currRow["ADID"] = "A";
				currRow["UHSS"] = txtSTSUrno.Text;
				currDBRow["UHSS"] = txtSTSUrno.Text;
				oldRow    = currRow.CopyRaw();
				myMode = "NEW";
				//rbArrival.Checked = true;
			}
			//UHSS_VAL UHSS
			
			ClearMyControls(this);
			myMode = "NEW";
			DateTime olFrom;
			DateTime olTo;
			olFrom = DateTime.Now;
			olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 0,0,0,0);
			olTo = new DateTime(2030, 12, 31, 23,59,59,0);
			oldValidFrom = olFrom;
			oldValidTo = olTo;
			dtValidFrom.Value = olFrom;
			dtValidTo.Value = olTo;
		}
		private bool CheckChanges()
		{
			if(CheckHeaderChanged() == true || CheckSDEChanges() == true)
			{
				btnSave.BackColor = Color.Red;
				return true;
			}
			else
			{
				btnSave.BackColor = Color.Transparent;
				return false;
			}
		}
		private bool CheckHeaderChanged()
		{
			bool changed = false;
			ArrayList arrCurr = currRow.FieldValueList();
			ArrayList arrOld  = oldRow.FieldValueList();
			for(int i = 0; i < arrCurr.Count && changed == false; i++)
			{
				if(arrCurr[i].ToString() != arrOld[i].ToString())
				{
					changed = true;
				}
			}
			if(oldValidFrom != dtValidFrom.Value || oldValidTo != dtValidTo.Value)
			{
				changed = true;
			}
			return changed;
		}
		private void CheckValuesChanged(int LineNo, bool NoPrdecessor)
		{
			int tCol = -1;
			int bCol = -1;
			tabDefs.GetLineColor(LineNo, ref tCol, ref bCol);
			string strCurrValues = tabDefs.GetFieldValues(LineNo, tabDefs.LogicalFieldList);//tabDefs.GetLineValues(LineNo);
			if(strCurrValues != tabDefs.GetLineTag(LineNo))
			{
				if(bCol == UT.colWhite)// || bCol == UT.colRed)
				{
					tabDefs.SetLineColor(LineNo, UT.colBlack, UT.colLightGreen);
					tabDefs.Refresh();
				}
			}
			else
			{
				if(bCol == UT.colLightGreen)
				{
					tabDefs.SetLineColor(LineNo, UT.colBlack, UT.colWhite);
					tabDefs.Refresh();
				}
			}
			if(CheckChanges() == true)
				btnSave.BackColor = Color.Red;
			else
				btnSave.BackColor = Color.Transparent;
		}
		private void CheckValuesChanged(int LineNo)
		{
			int tCol = -1;
			int bCol = -1;
			tabDefs.GetLineColor(LineNo, ref tCol, ref bCol);
			string strCurrValues = tabDefs.GetFieldValues(LineNo, tabDefs.LogicalFieldList);//tabDefs.GetLineValues(LineNo);
			if(strCurrValues != tabDefs.GetLineTag(LineNo))
			{
				if(bCol == UT.colWhite)// || bCol == UT.colRed)
				{
					tabDefs.SetLineColor(LineNo, UT.colBlack, UT.colLightGreen);
					tabDefs.Refresh();
				}
			}
			else
			{
				if(bCol == UT.colLightGreen)
				{
					tabDefs.SetLineColor(LineNo, UT.colBlack, UT.colWhite);
					tabDefs.Refresh();
				}
			}
			RebuildComboPredecessorStatus();
			if(CheckChanges() == true)
				btnSave.BackColor = Color.Red;
			else
				btnSave.BackColor = Color.Transparent;
		}
		private bool CheckSDEChanges()
		{
			int tCol = -1;
			int bCol = -1;
			int i = 0;
			bool isChanged = false;

			for( i = 0; i < tabDefs.GetLineCount() && isChanged == false; i++)
			{
				tabDefs.GetLineColor(i, ref tCol, ref bCol);
				if(bCol == UT.colYellow || bCol == UT.colRed || bCol == UT.colLightGreen )
				{
					isChanged = true;
				}
			}
			return isChanged;
		}

		private void pictureBox2_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, pictureBox2, Color.WhiteSmoke, Color.Gray);
		}

		private void pictureBox3_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, pictureBox2, Color.WhiteSmoke, Color.Gray);
		}

		private void rbArrival_CheckedChanged(object sender, System.EventArgs e)
		{
			if(rbArrival.Checked == true)
			{
				rbArrival.BackColor = Color.LightGreen;
				rbDeparture.BackColor = Color.Transparent;
				rbTurnaround.BackColor = Color.Transparent;
				txtMAXT.BackColor = Color.White;
				currRow["ADID"] = "A";
				//Now check all existing tab-lines for RFLD ==> Departure values 
				//and reset them
				string strDEP = "OFBL,STOD,ETDI";
				for(int i = 0; i < tabDefs.GetLineCount(); i++)
				{
					string strFLDR = tabDefs.GetFieldValue(i, "FLDR");
					if(strDEP.IndexOf(strFLDR, 0) > -1)
					{
						tabDefs.SetFieldValues(i, "FLDR", "");
						CheckValuesChanged(i);
					}
				}
			}
			else if(rbDeparture.Checked == true)
			{
				rbArrival.BackColor = Color.Transparent;
				rbTurnaround.BackColor = Color.Transparent;
				rbDeparture.BackColor = Color.LightGreen;
				txtMAXT.BackColor = Color.White;
				currRow["ADID"] = "D";
				string strARR = "ONBL,STOA,ETAI";
				for(int i = 0; i < tabDefs.GetLineCount(); i++)
				{
					string strFLDR = tabDefs.GetFieldValue(i, "FLDR");
					if(strARR.IndexOf(strFLDR, 0) > -1)
					{
						tabDefs.SetFieldValues(i, "FLDR", "");
						CheckValuesChanged(i);
					}
				}
				//Now check all existing tab-lines for RFLD ==> Departure values 
				//and reset them
			}
			else
			{
				rbArrival.BackColor = Color.Transparent;
				rbDeparture.BackColor = Color.Transparent;
				rbTurnaround.BackColor = Color.LightGreen;
				txtMAXT.BackColor = txtRuleName.BackColor;
				currRow["ADID"] = "T";
			}
			string strName = "AFTF_C";
			int llIDX = UT.GetItemNo(tabDefs.LogicalFieldList, strName);
			string strComboValues = "|"; 
			if(rbArrival.Checked == true)
			{
				for(int i = 0; i < myAFTF_DB_ARR.Length; i++)
				{
					strComboValues = strComboValues + myAFTF_GUI_ARR[i] + "|";
				}
			}
			else if(rbDeparture.Checked == true)
			{
				for(int i = 0; i < myAFTF_DB_DEP.Length; i++)
				{
					strComboValues = strComboValues + myAFTF_GUI_DEP[i] + "|";
				}
			}
			else //Turnaround
			{
				int i = 0;
				for( i = 0; i < myAFTF_DB_ARR.Length; i++)
				{
					strComboValues = strComboValues + myAFTF_GUI_ARR[i] + "|";
				}
				for( i = 0; i < myAFTF_DB_DEP.Length; i++)
				{
					strComboValues = strComboValues + myAFTF_GUI_DEP[i] + "|";
				}
			}
			tabDefs.ComboOwnerDraw = false;
			tabDefs.ComboResetObject( strName );
			tabDefs.CreateComboObject(strName, llIDX, 1, "", 0);
			tabDefs.ComboSetColumnLengthString(strName, "80");
			tabDefs.SetComboColumn(strName, llIDX);
			tabDefs.ComboAddTextLines(strName, strComboValues, "|");

			RebuildRefTimesCombo();
			CheckChanges();
		}

		private void btnStatusSection_Click(object sender, System.EventArgs e)
		{
			frmDataEntryGrid olDlg = new frmDataEntryGrid();
			olDlg.currTabelName = "HSS"; 
			olDlg.myCaption = "Status sections ...";
			olDlg.myHeaderLen = "0,400";
			int ilWidth = 0;
			string [] arrW = olDlg.myHeaderLen.Split(',');
			for(int i = 0; i < arrW.Length; i++)
			{
				ilWidth += Convert.ToInt32( arrW[i]);
			}
			olDlg.myWidth = ilWidth+30;
			olDlg.myHeaderString = "URNO,Name";
			olDlg.myFields = "URNO,NAME";
			olDlg.ShowDialog();
			
		}

		private void btnNew_Click(object sender, System.EventArgs e)
		{
			NewRule();
		}
		private void ClearMyControls(System.Windows.Forms.Control pControl)
		{
			foreach(System.Windows.Forms.Control olC in pControl.Controls )
			{
				string lType = olC.GetType().ToString();
				if(lType == "System.Windows.Forms.PictureBox" || lType == "System.Windows.Forms.Panel")
				{
					ClearMyControls(olC);
				}
				else if(lType == "System.Windows.Forms.TextBox" || lType == "System.Windows.Forms.ComboBox" )
				{
					if(olC.Tag != null)
					{
						if(olC.Tag.ToString() != "UHSS_VAL" && olC.Tag.ToString() != "UHSS")
						{
							olC.Text = "";
						}
					}
					else
						olC.Text = "";
				}
			}
		}
		private Control GetControlByTag(string tag, Control currCtl, ref Control myControl )
		{
			if(myControl == null)
			{
				foreach(System.Windows.Forms.Control olC in currCtl.Controls )
				{
					string lType = olC.GetType().ToString();
					if(lType == "System.Windows.Forms.PictureBox" || lType == "System.Windows.Forms.Panel")
					{
						if(olC.Tag != null)
							if(olC.Tag.ToString() != tag)
								myControl = GetControlByTag(tag.ToString(), olC, ref myControl);
							else
								myControl = olC;
						else
							myControl = GetControlByTag(tag.ToString(), olC, ref myControl);
					}
					else
					{
						if(olC.Tag != null)
						{
							if (olC.Tag.ToString() == tag)
								myControl = olC;
						}
					}
				}
			}
			return myControl;
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			Control ctl = null;
			ctl = GetControlByTag(txtTagTest.Text, this, ref ctl);
			try
			{
				ctl.Text = "Hallo";
			}
			catch(Exception err)
			{
				UT.AddException(this.Name.ToString(), "button1_Click", err.Message.ToString(),"", "");
			}
		}

		private void HeaderValueChanged(object sender, System.EventArgs e)
		{
			Control ctl = (Control)sender;
			if(ctl.Tag != null)
			{
				currRow[ctl.Tag.ToString()] = ctl.Text;
				if(ctl.Tag.ToString() == "MAXT")
				{
					//To make it easier for the stathdl to sort for MAXT
					string maxt = ctl.Text;
					maxt = maxt.PadLeft(4, ' ');
					currRow[ctl.Tag.ToString()] = maxt;
				}
				CheckChanges();
			}
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			SaveRule();
		}
		private bool SaveRule()
		{
			bool isOK = true;
			string strMessage="";
			ITable myTable = myDB["SRH"];
			if(btnSave.BackColor == Color.Transparent )
			{
				MessageBox.Show(this, "Nothing to save!!", "Info");
				return true;
			}
			//Call Validation
			strMessage = ValidateHeader();
			strMessage += ValidateDefinitions();

			if(strMessage != "")
			{
				MessageBox.Show(this, strMessage, "Save Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				isOK = false;
			}
			else //Save the rule
			{
				DateTime datNow = DateTime.Now;
				if(myMode == "NEW")
				{
					DateTime myD = new DateTime(datNow.Year, datNow.Month, datNow.Day, datNow.Hour, datNow.Minute, datNow.Second);
					txtUSEC.Text = UT.UserName;
					txtCDAT.Text = myD.ToShortDateString() + " - " + myD.ToShortTimeString();
					currRow["CDAT"] = UT.DateTimeToCeda(datNow);
					currRow["USEC"] = UT.UserName;
					currRow["DELF"] = "1";
					string maxt = currRow["MAXT"];
					maxt = maxt.PadLeft(4, ' ');
					currRow["MAXT"] = maxt;
					currDBRow.Status = State.Created;
					myTable.Add(currDBRow);

					MakeNewValtabEntry();
				}
				if(myMode == "UPDATE")
				{
					txtUSEU.Text = UT.UserName;
					txtLSTU.Text = datNow.ToShortDateString() + " - " + datNow.ToShortTimeString();
					currRow["LSTU"] = UT.DateTimeToCeda(DateTime.Now);
					currRow["USEU"] = UT.UserName;
					string maxt = currRow["MAXT"];
					maxt = maxt.PadLeft(4, ' ');
					currRow["MAXT"] = maxt;
					//currRow.Status = State.Modified;
					currDBRow.Status = State.Modified;
					ITable tabVal = myDB["VAL"];
					IRow [] rowsVal = tabVal.RowsByIndexValue("UVAL", currRow["URNO"]);
					if(rowsVal.Length > 0)
					{
						rowsVal[0]["VAFR"] = UT.DateTimeToCeda( dtValidFrom.Value);
						rowsVal[0]["VATO"] = UT.DateTimeToCeda( dtValidTo.Value);
						rowsVal[0].Status = State.Modified;
						tabVal.Save();
						oldValidFrom = dtValidFrom.Value;
						oldValidTo = dtValidTo.Value;
					}
				}
				btnSave.BackColor = Color.Transparent;
				ArrayList arrCurr = currRow.FieldValueList();
				for(int i = 0; i < arrCurr.Count; i++)
				{
					currDBRow[i] = currRow[i];
					oldRow[i] = currRow[i]; //To remember the current values, because we are in
											//updatemode from now on.
				}
				myTable.Save();
				SaveSDE();
				myMode = "UPDATE";

			}
			myTable.ReorganizeIndexes(); //TO DO THO: Das mu?intern organisiert werden
			return isOK;
		}
		void MakeNewValtabEntry()
		{
			ITable tabVal = myDB["VAL"];
			IRow rowVal = tabVal.CreateEmptyRow();
			rowVal["APPL"] = "STATMGR";
			rowVal["UVAL"] = currDBRow["URNO"];
			rowVal["FREQ"] = "1111111";
			rowVal["VAFR"] = UT.DateTimeToCeda( dtValidFrom.Value);
			rowVal["VATO"] = UT.DateTimeToCeda( dtValidTo.Value);
			rowVal.Status = State.Created;
			tabVal.Add(rowVal);
			tabVal.Save();
			tabVal.ReorganizeIndexes();
			oldValidFrom = dtValidFrom.Value;
			oldValidTo   = dtValidTo.Value;
		}
		void UpdateValtabEntry()
		{
			ITable tabVal = myDB["VAL"];
			IRow [] rowVal = tabVal.RowsByIndexValue("UVAL", currDBRow["URNO"]);
			if(rowVal.Length > 0)
			{
				rowVal[0]["VAFR"] = UT.DateTimeToCeda( dtValidFrom.Value);
				rowVal[0]["VATO"] = UT.DateTimeToCeda( dtValidTo.Value);
				rowVal[0].Status = State.Modified;
				tabVal.Save();
			}
		}
		private void SaveSDE()
		{
			int tColor = -1;
			int bColor = -1;
			ITable myTable = myDB["SDE"];
			for(int i = 0; i < tabDefs.GetLineCount(); i++)
			{
				tabDefs.GetLineColor(i, ref tColor, ref bColor);
				IRow row = null;
				ArrayList arrFlds = myTable.FieldList;
				if(bColor == UT.colYellow) //New
				{
					row = myTable.CreateEmptyRow();
					for(int j = 0; j < arrFlds.Count; j++)
					{
						string strFld = arrFlds[j].ToString();
						if(strFld != "LSTU" && 
						   strFld != "CDAT" && strFld != "USEC" && 
						   strFld != "USEU")
						{
							row[strFld] = tabDefs.GetFieldValue(i, strFld);//tabDefs.GetColumnValue(i, j);
						}
					}
					string strTime = UT.DateTimeToCeda( DateTime.Now);
					row["CDAT"] = strTime;
					tabDefs.SetFieldValues(i, "CDAT", strTime);
					row["USEC"] = UT.UserName;
					row["DELF"] = "1";
					tabDefs.SetFieldValues(i, "USEC", UT.UserName);
					tabDefs.SetFieldValues(i, "URNO", row["URNO"]);
					row.Status = State.Created;
					myTable.Add(row);
					myTable.Save();
					tabDefs.SetLineColor(i, UT.colBlack , UT.colWhite);
					tabDefs.Refresh();
				}
				if(bColor == UT.colLightGreen)//Updated
				{
					int idxUrno = UT.GetItemNo(tabDefs.LogicalFieldList, "URNO");
					if(idxUrno > -1)
					{
						IRow [] rows = myTable.RowsByIndexValue("URNO", tabDefs.GetColumnValue(i, idxUrno));
						if(rows.Length > 0)
						{
							for(int j = 0; j < arrFlds.Count; j++)
							{
								string strFld = arrFlds[j].ToString();
								if(strFld != "URNO" && strFld != "LSTU" && 
									strFld != "CDAT" && strFld != "USEC" && 
									strFld != "USEU")
								{
									rows[0][strFld] = tabDefs.GetFieldValue(i, strFld);//tabDefs.GetColumnValue(i, j);
								}
							}
							string strTime = UT.DateTimeToCeda( DateTime.Now);
							rows[0]["LSTU"] = strTime;
							tabDefs.SetFieldValues(i, "LSTU", strTime);
							rows[0]["USEU"] = UT.UserName;
							rows[0]["DELF"] = "1";
							tabDefs.SetFieldValues(i, "USEU", UT.UserName);
							rows[0].Status = State.Modified;
						}
					}
					myTable.Save();
					tabDefs.SetLineColor(i, UT.colBlack , UT.colWhite);
					tabDefs.Refresh();
				}
				if(bColor == UT.colRed) // Deleted
				{
					int idxUrno = UT.GetItemNo(tabDefs.LogicalFieldList, "URNO");
					if(idxUrno > -1)
					{
						IRow [] rows = myTable.RowsByIndexValue("URNO", tabDefs.GetColumnValue(i, idxUrno));
						if(rows.Length > 0)
						{
							rows[0]["DELF"] = "X";
							rows[0].Status = State.Modified;
							myTable.Save();
						}
					}
				}
			}
			tabDefs.Refresh();
			//Remove the deleted (red) lines
			for(int i = tabDefs.GetLineCount()-1; i >= 0; i--)
			{
				tabDefs.GetLineColor(i, ref tColor, ref bColor);
				if(bColor == UT.colRed)
				{
					//tabDefs.DeleteLine(i);
					tabDefs.SetLineColor(i, UT.colBlack, UT.colOrange);
				}
			}
			for(int i = 0; i < tabDefs.GetLineCount(); i++)
			{
				string strValues = tabDefs.GetFieldValues(i, tabDefs.LogicalFieldList);//tabDefs.GetLineValues(i);
				tabDefs.SetLineTag(i, strValues);
			}
			myTable.ReorganizeIndexes();//TO DO THO: Das mu?intern organisiert werden
			tabDefs.SetCurrentSelection(-1);
			tabDefs.Refresh();
			//handle the delete button's text
			int currSel = tabDefs.GetCurrentSelected();
			if(currSel > -1)
			{
				if(tabDefs.GetFieldValue(currSel, "DELF") == "X")
				{
					btnDeleteDef.Text = "R&eactivate";
				}
				else
				{
					btnDeleteDef.Text = "&Delete";
				}
			}
		}

		/// <summary>
		/// Validates the Header of the rule
		/// </summary>
		/// <returns>Message as Error string</returns>
		private string ValidateHeader()
		{
			string strMessage="";
			ITable myTable = myDB["SRH"];
			string strName = txtRuleName.Text;
			if(strName == "")
			{
				strMessage += "Rulename is mandatory!\n";
			}
			else
			{
				//if(myMode == "NEW")
				{
					bool isDuplicate = false;
					for (int i = 0; i < myTable.Count && isDuplicate == false; i++)
					{
						if(myTable[i]["NAME"] == strName && myTable[i]["URNO"] != currDBRow["URNO"])
						{
							isDuplicate = true;
							strMessage += "Rulename already exists\n";
						}
					}
				}
			}
			if(rbTurnaround.Checked == true)
			{
				if(txtMAXT.Text == "")
				{
					strMessage += "Max Groundtime is mandatory!\n";
				}
			}
			if(txtSTSUrno.Text == "")
			{
				strMessage += "Status Section is mandatory!\n";
			}
			DateTime datFrom = dtValidFrom.Value;
			DateTime datTo = dtValidTo.Value;
			if(datTo < datFrom)
			{
				strMessage += "Valid from is later than valid to!\n";
			}
			if(txtACTValue.ForeColor == Color.Red)
			{
				strMessage += "A/C Type is not valid!\n";
			}
			if(txtAPTValue.ForeColor == Color.Red)
			{
				strMessage += "Airport is not valid!\n";
			}
			if(txtALTValue.ForeColor == Color.Red)
			{
				strMessage += "Airline is not valid!\n";
			}
			if(txtNATValue.ForeColor == Color.Red)
			{
				strMessage += "Nature is not valid!\n";
			}
			return strMessage;
		}
		/// <summary>
		/// Validates the entries in the SDE Tab.ocx
		/// </summary>
		/// <returns>Message as error string</returns>
		private string ValidateDefinitions()
		{
			string strMessage="";
			int tCol = -1;
			int bCol = -1;
			for(int i = 0; i < tabDefs.GetLineCount(); i++)
			{
				tabDefs.GetLineColor(i, ref tCol, ref bCol);
				if(bCol == UT.colYellow || bCol == UT.colLightGreen)
				{
					string strRETY = "";
					string strFLDR = "";
					if(tabDefs.GetFieldValue(i, "NAME") == "")
					{
						strMessage += "Grid Line " + (i+1).ToString() + ": Name is mandatory!\n";
					}
					strRETY = tabDefs.GetFieldValue(i, "RETY");
					if(strRETY == "")
					{
						strMessage += "Grid Line " + (i+1).ToString() + ": Type is mandatory!\n";
					}
					else
					{
						if(strRETY == "AFT")
						{
							string strAFTF = tabDefs.GetFieldValue(i, "AFTF");
							if(strAFTF == "")
							{
								strMessage += "Grid Line " + (i+1).ToString() + ": Flight-Field is mandatory!\n";
							}
						}
						if( strRETY == "JOBI" || strRETY == "JOBF" || strRETY == "JOBC")
						{
//Do not check for mandatory: Meeting with AMA 01.12.2003
//							string strUTPL = tabDefs.GetFieldValue(i, "UTPL");
//							if(strUTPL == "")
//							{
//								strMessage += "Grid Line " + (i+1).ToString() + ": Template is mandatory for Jobs!\n";
//							}
						}

					}
					strFLDR = tabDefs.GetFieldValue(i, "FLDR");
					if(strFLDR == "")
					{
						strMessage += "Grid Line " + (i+1).ToString() + ": Ref.-Time is mandatory!\n";
					}
					else if(strFLDR == "CONA")
					{
						if(tabDefs.GetFieldValue(i, "SDEU") == "")
						{
							strMessage += "Grid Line " + (i+1).ToString() + ": Predecessor is mandatory!\n";
						}
					}
					string strTIFR = tabDefs.GetFieldValue(i, "TIFR");
					if(strTIFR != "")
					{
						bool blDigitOK = true;
						for(int j = 0; j < strTIFR.Length && blDigitOK == true; j++)
						{
							if(Char.IsDigit(strTIFR[j]) == false)
							{
								if(j == 0 && strTIFR[j] == '-')
								{;}
								else
									blDigitOK = false;
							}
						}
						if(blDigitOK == false)
						{
							strMessage += "Grid Line " + (i+1).ToString() + ": Time must be digit (e.g. -10 or 29)!\n";
						}
					}
					else
					{
						strMessage += "Grid Line " + (i+1).ToString() + ": Time is mandatory\n";
					}
				}
			}

			return strMessage;
		}

		private void btnOpen_Click(object sender, System.EventArgs e)
		{
			if(btnSave.BackColor == Color.Red )
			{
				DialogResult olResult;
				olResult = MessageBox.Show(this, "Do you want to save current rule?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question); 
				if ( olResult == DialogResult.Yes)
				{
					if(SaveRule() == false) return; // Do not NEW!!
				}
			}
			frmChoiceList olDlg = new frmChoiceList();
			olDlg.lblCaption.Text = "Rules";
			olDlg.currTable = "SRH";
			olDlg.isAddEmptyLine = false;
			
			if(olDlg.ShowDialog() == DialogResult.OK)
			{
				ITable myTable = myDB["SRH"];
				int sel = olDlg.tabList.GetCurrentSelected();
				string strUrno = olDlg.tabList.GetColumnValue(sel,0);
				IRow [] rows = myTable.RowsByIndexValue("URNO", strUrno);

				ClearMyControls(this);

				if(rows.Length > 0)
				{
					string [] arrFields = "NAME,ACTG,ACTU,ADID,ALTG,ALTU,APTG,APTU,CDAT,LSTU,NATG,NATU,UHSS,USEC,USEU,MAXT".Split(',');
					for(int i = 0; i < arrFields.Length; i++)					
					{
						Control ctl = null;
						ctl = GetControlByTag(arrFields[i], this, ref ctl);
						if(ctl != null)
						{
							string strText = rows[0][arrFields[i]];
							strText = strText.Trim();
							ctl.Text = strText;
						}
					}

					SetCedaFields(rows[0]);
					SetValueFields(rows[0]);
					currDBRow = rows[0];
					currRow = currDBRow.CopyRaw();
					oldRow = currRow.CopyRaw();
					if(rows[0]["ADID"] == "A")
						rbArrival.Checked = true;
					else if(rows[0]["ADID"] == "D")
						rbDeparture.Checked = true;
					else //Turnaround
						rbTurnaround.Checked = true;
					SetSDETab(rows[0]["URNO"]);
					txtNATValue.ForeColor = Color.Black;
					txtALTValue.ForeColor = Color.Black;
					txtAPTValue.ForeColor = Color.Black;
					txtACTValue.ForeColor = Color.Black;
				}
				btnSave.BackColor = Color.Transparent;
				myMode = "UPDATE";
				MakeTabCombos();
			}
		}
		/// <summary>
		/// Set the status definitions into the tabDefs grid
		/// </summary>
		/// <param name="strUSRH"></param>
		void SetSDETab(string strUSRH)
		{
			tabDefs.ResetContent();
			ITable myTab = myDB["SDE"];
			if(myTab != null)
			{
				IRow [] rows = myTab.RowsByIndexValue("USRH", strUSRH);
				//First insert the database field values
				int i = 0;
				if(rows.Length > 0)
				{
					int LineNo = -1;
					for(i = 0; i < rows.Length; i++)
					{
						string strValues = MakeEmptyString(tabDefs.LogicalFieldList, ',');
						tabDefs.InsertTextLine(strValues, false);
						LineNo = tabDefs.GetLineCount() - 1;
						string strFld = "";
						for(int j = 0; j < myTab.FieldList.Count; j++)
						{
							strFld = myTab.FieldList[j].ToString();
							tabDefs.SetFieldValues(LineNo, strFld, rows[i][strFld]);
							if(strFld == "RETY")
							{
								for(int k = 0; k < myRETY_DB.Length; k++)
								{
									if(myRETY_DB[k] == rows[i][strFld])
									{
										tabDefs.SetFieldValues(LineNo, "RETY_C", myRETY_GUI[k] );
									}
								}
							}
							if(strFld == "AFTF")
							{
								if(rbArrival.Checked == true)
								{
									for(int k = 0; k < myAFTF_DB_ARR.Length; k++)
									{
										if(myAFTF_DB_ARR[k] == rows[i][strFld])
										{
											tabDefs.SetFieldValues(LineNo, "AFTF_C", myAFTF_GUI_ARR[k] );
										}
									}
								}
								else if(rbDeparture.Checked == true)
								{
									for(int k = 0; k < myAFTF_DB_DEP.Length; k++)
									{
										if(myAFTF_DB_DEP[k] == rows[i][strFld])
										{
											tabDefs.SetFieldValues(LineNo, "AFTF_C", myAFTF_GUI_DEP[k] );
										}
									}
								}
								else //Turnaround
								{
									int k = 0;
									for( k = 0; k < myAFTF_DB_ARR.Length; k++)
									{
										if(myAFTF_DB_ARR[k] == rows[i][strFld])
										{
											tabDefs.SetFieldValues(LineNo, "AFTF_C", myAFTF_GUI_ARR[k] );
										}
									}
									for( k = 0; k < myAFTF_DB_DEP.Length; k++)
									{
										if(myAFTF_DB_DEP[k] == rows[i][strFld])
										{
											tabDefs.SetFieldValues(LineNo, "AFTF_C", myAFTF_GUI_DEP[k] );
										}
									}
								}
							}
							if(strFld == "RTIM")
							{
								for(int k = 0; k < myRTIM_DB.Length; k++)
								{
									if(myRTIM_DB[k] == rows[i][strFld])
									{
										tabDefs.SetFieldValues(LineNo, "RTIM_C", myRTIM_GUI[k] );
									}
								}
							}
							if(strFld == "UTPL")
							{
								ITable tplTable = myDB["TPL"];
								IRow [] tplRow = tplTable.RowsByIndexValue("URNO", rows[i][strFld]);
								if(tplRow.Length > 0)
								{
									tabDefs.SetFieldValues(LineNo, "TNAM", tplRow[0]["TNAM"] );
								}
							}
							if(rows[i]["DELF"] == "X")
							{
								tabDefs.SetLineColor(LineNo, UT.colBlack, UT.colOrange);
							}
						}	
					}
					for( i = 0; i < tabDefs.GetLineCount(); i++)
					{
						string strSDEU = tabDefs.GetFieldValue(i, "SDEU");
						if(strSDEU != "")
						{
							IRow [] tmpRows = myTab.RowsByIndexValue("URNO", strSDEU);
							if(tmpRows.Length > 0)
							{
								tabDefs.SetFieldValues(i, "SDEU_C", tmpRows[0]["NAME"]);
							}
						}
						string strValues = tabDefs.GetFieldValues(i, tabDefs.LogicalFieldList);//tabDefs.GetLineValues(i);
						tabDefs.SetLineTag(i, strValues);
					}
				}
			}
			tabDefs.Refresh();
		}
		void SetCedaFields(IRow row)
		{
			DateTime olD;
			if(row["CDAT"] != "")
			{
				olD = UT.CedaFullDateToDateTime(row["CDAT"]);
				txtCDAT.Text = olD.ToShortDateString() + " - " + olD.ToShortTimeString();
			}
			if(row["LSTU"] != "")
			{
				olD = UT.CedaFullDateToDateTime(row["LSTU"]);
				txtLSTU.Text = olD.ToShortDateString() + " - " + olD.ToShortTimeString();
			}
		}
		void SetValueFields(IRow row)
		{
			for(int i = 0; i < myURNOFields.Length; i++)
			{
				Control ctl = null;
				ctl = GetControlByTag(myVALUEFields[i], this, ref ctl);
				if(ctl != null)
				{
					ITable myTable = myDB[myLookupTabs[i]];
					if(myURNOFields[i] != "")
					{
						IRow [] rows = myTable.RowsByIndexValue("URNO", row[myURNOFields[i]]);
						if(rows.Length > 0)
						{
							string [] arrValues = myLookupFields[i].Split('|');
							string strText = "";
							for(int j = 0; j < arrValues.Length; j++)
							{
								string strCurrVal = rows[0][arrValues[j]];
								if(strCurrVal != "")
								{
									strText += rows[0][arrValues[j]] + "/";
								}
							}
							if(strText != "")
							{
								strText = strText.Substring(0, strText.Length -1);
							}
							ctl.Text = strText;
						}
					}
				}
			}
			//Set the validity fields
			ITable tabVal = myDB["VAL"];
			IRow [] rowsVal = tabVal.RowsByIndexValue("UVAL", row["URNO"]);
			if(rowsVal.Length > 0)
			{
				DateTime datFrom = UT.CedaFullDateToDateTime(rowsVal[0]["VAFR"]);
				DateTime datTo   = UT.CedaFullDateToDateTime(rowsVal[0]["VATO"]);
				dtValidFrom.Value = datFrom;
				dtValidTo.Value = datTo;
				oldValidFrom = datFrom;
				oldValidTo = datTo;
			}
		}

		private void btnDeleteHeader_Click(object sender, System.EventArgs e)
		{
			int i = 0;
			DialogResult olResult;
			olResult = MessageBox.Show(this, "Do you really want to delete the current rule?", "Question", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
			if(olResult != DialogResult.Yes)
			{
				return;
			}
			if(myMode == "UPDATE")
			{
				ITable myTable =  myDB["SRH"];
				currDBRow["DELF"] = "X";
				currDBRow.Status = State.Modified;
				ITable tabVal = myDB["VAL"];
				IRow [] rowsVal = tabVal.RowsByIndexValue("UVAL", currRow["URNO"]);
				if(rowsVal.Length > 0)
				{
					//DO not change Valtab!!
					//rowsVal[0].Status = State.Modified;
					//tabVal.Save();
				}
				myTable.Save();
				myTable = myDB["SDE"];
				for( i = 0; i < tabDefs.GetLineCount(); i++)
				{
					int idxUrno = UT.GetItemNo(tabDefs.LogicalFieldList, "URNO");
					if(idxUrno > -1)
					{
						IRow [] rows = myTable.RowsByIndexValue("URNO", tabDefs.GetColumnValue(i, idxUrno));
						if(rows.Length > 0)
						{
							rows[0]["DELF"] = "X";
							rows[0].Status = State.Modified;
							myTable.Save();
						}
					}
				}
			}
			NewRule();
		}

		private void btnAddDef_Click(object sender, System.EventArgs e)
		{
			NewSED();
		}
		private void NewSED()
		{
			ITable myTab = myDB["SDE"];
			if(myTab != null)
			{
				string strValues = MakeEmptyString(tabDefs.LogicalFieldList, ',');
				tabDefs.InsertTextLine(strValues, true);
				int cnt = tabDefs.GetLineCount() - 1;
				string strUrno = myDB.GetNextUrno();
				tabDefs.SetFieldValues(cnt, "URNO", strUrno);
				tabDefs.SetLineColor(cnt, UT.colBlack, UT.colYellow);
				tabDefs.SetFieldValues(cnt, "USRH", currDBRow["URNO"]);
				tabDefs.OnVScrollTo(cnt);
				CheckChanges();
			}
		}
		/// <summary>
		/// Crates an empty string for tab insertion
		/// </summary>
		/// <param name="strValueList">[separator] delimited string </param>
		/// <param name="separator">separator</param>
		/// <returns></returns>
		string MakeEmptyString(string strValueList, char separator)
		{
			int i = 0;
			string [] strArr = strValueList.Split(separator);
			string strValues = "";
			for(i = 0; i < strArr.Length-1; i++)
			{
				strValues += separator.ToString();
			}
			return strValues;
		}
		private void HandleSDEDelete()
		{
			int currSel = tabDefs.GetCurrentSelected();
			int bCol = -1;
			int tCol = -1;
			if(currSel > -1)
			{
				tabDefs.GetLineColor(currSel, ref tCol, ref bCol);
				if(bCol != UT.colRed && bCol != UT.colOrange)
				{
					tabDefs.SetLineColor(currSel, UT.colBlack, UT.colRed);
				}
				if(bCol == UT.colYellow)
				{
					tabDefs.DeleteLine(currSel);
				}
				if( bCol == UT.colOrange)
				{
					tabDefs.SetLineColor(currSel, UT.colBlack, UT.colLightGreen);
					tabDefs.SetFieldValues(currSel, "DELF", "1");
					btnDeleteDef.Text = "&Delete";
					CheckValuesChanged(currSel);
				}
				if(bCol == UT.colRed )
				{
					tabDefs.SetLineColor(currSel, UT.colBlack, UT.colWhite);
					CheckValuesChanged(currSel);
				}
				tabDefs.Refresh();
			}
		}

		private void btnDeleteDef_Click(object sender, System.EventArgs e)
		{
			HandleSDEDelete();
			CheckChanges();
		}

		/// <summary>
		/// Put the internal values into the DB columns according to the GUI/user selection
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tabDefs_ComboSelChanged(object sender, AxTABLib._DTABEvents_ComboSelChangedEvent e)
		{
			if(e.colNo == UT.GetItemNo(tabDefs.LogicalFieldList, "TNAM") )//Template Name Combo was selected
			{
				ITable myTab = myDB["TPL"];
				if(myTab != null)
				{
					bool blFound = false;
					for(int i = 0; i < myTab.Count && blFound == false; i++)
					{
						if(myTab[i]["TNAM"].ToString() == e.newValues)
						{
							blFound = true;
							tabDefs.SetFieldValues(e.lineNo, "UTPL", myTab[i]["URNO"]);
							tabDefs.Refresh();
						}
					}
				}
				EnableComboboxes(e.lineNo);
			}
			if(e.colNo == UT.GetItemNo(tabDefs.LogicalFieldList, "FLDR") )
			{
				EnableComboboxes(e.lineNo);
				if(e.newValues != "CONA")
				{
					tabDefs.SetFieldValues(e.lineNo, "SDEU,SDEU_C", ",");
					tabDefs.Refresh();
				}
				else
				{
					RebuildComboPredecessorStatus();
				}
			}
			if(e.colNo == UT.GetItemNo(tabDefs.LogicalFieldList, "RETY_C") )//RETY Combo was selected
			{
				bool blFound = false;
				for(int i = 0; i < myRETY_GUI.Length && blFound == false; i++)
				{
					if(myRETY_GUI[i] == e.newValues)
					{
						blFound = true;
						tabDefs.SetFieldValues(e.lineNo, "RETY", myRETY_DB[i]);
						tabDefs.Refresh();
					}
				}
				if(blFound == false)
				{
					tabDefs.SetFieldValues(e.lineNo, "RETY", "");
				}
				EnableComboboxes(e.lineNo);
			}
			if(e.colNo == UT.GetItemNo(tabDefs.LogicalFieldList, "AFTF_C") )//RETY Combo was selected
			{
				bool blFound = false;
				if(rbArrival.Checked == true)
				{
					for(int i = 0; i < myAFTF_GUI_ARR.Length && blFound == false; i++)
					{
						if(myAFTF_GUI_ARR[i] == e.newValues)
						{
							blFound = true;
							tabDefs.SetFieldValues(e.lineNo, "AFTF", myAFTF_DB_ARR[i]);
							tabDefs.Refresh();
						}
					}
				}
				else if(rbDeparture.Checked == true)
				{
					for(int i = 0; i < myAFTF_GUI_DEP.Length && blFound == false; i++)
					{
						if(myAFTF_GUI_DEP[i] == e.newValues)
						{
							blFound = true;
							tabDefs.SetFieldValues(e.lineNo, "AFTF", myAFTF_DB_DEP[i]);
							tabDefs.Refresh();
						}
					}
				}
				else //Turnaround
				{
					int i = 0;
					for( i = 0; i < myAFTF_GUI_ARR.Length && blFound == false; i++)
					{
						if(myAFTF_GUI_ARR[i] == e.newValues)
						{
							blFound = true;
							tabDefs.SetFieldValues(e.lineNo, "AFTF", myAFTF_DB_ARR[i]);
							tabDefs.Refresh();
						}
					}
					for( i = 0; i < myAFTF_GUI_DEP.Length && blFound == false; i++)
					{
						if(myAFTF_GUI_DEP[i] == e.newValues)
						{
							blFound = true;
							tabDefs.SetFieldValues(e.lineNo, "AFTF", myAFTF_DB_DEP[i]);
							tabDefs.Refresh();
						}
					}
				}

				if(blFound == false)
				{
					tabDefs.SetFieldValues(e.lineNo, "AFTF", "");
				}
				EnableComboboxes(e.lineNo);
			}
			if(e.colNo == UT.GetItemNo(tabDefs.LogicalFieldList, "RTIM_C") )//RTIM Combo was selected
			{
				bool blFound = false;
				for(int i = 0; i < myRETY_GUI.Length && blFound == false; i++)
				{
					if(myRTIM_GUI[i] == e.newValues)
					{
						blFound = true;
						tabDefs.SetFieldValues(e.lineNo, "RTIM", myRTIM_DB[i]);
						tabDefs.Refresh();
					}
				}
				if(blFound == false)
				{
					tabDefs.SetFieldValues(e.lineNo, "RTIM", "");
				}
				EnableComboboxes(e.lineNo);
			}
			if(e.colNo == UT.GetItemNo(tabDefs.LogicalFieldList, "SDEU_C") )//RTIM Combo was selected
			{
				bool blFound = false;
				for(int i = 0; i < tabDefs.GetLineCount() && blFound == false; i++)
				{
					string strName = tabDefs.GetFieldValue(i, "NAME");
					string strUrno = tabDefs.GetFieldValue(i, "URNO");
					if(strName == e.newValues)
					{
						blFound = true;
						tabDefs.SetFieldValues(e.lineNo, "SDEU", strUrno);
						tabDefs.Refresh();
					}
				}
				if(blFound == false)
				{
					tabDefs.SetFieldValues(e.lineNo, "SDEU", "");
				}
			}
			CheckValuesChanged(e.lineNo, false);
		}

		private void tabDefs_CloseInplaceEdit(object sender, AxTABLib._DTABEvents_CloseInplaceEditEvent e)
		{
			CheckValuesChanged(e.lineNo );
		}

		private void tabDefs_EditPositionChanged(object sender, AxTABLib._DTABEvents_EditPositionChangedEvent e)
		{
			CheckValuesChanged(e.lineNo );
			myCurrEditColumn = e.colNo;

		}

		private void tabDefs_RowSelectionChanged(object sender, AxTABLib._DTABEvents_RowSelectionChangedEvent e)
		{
			int txtCol = -1;
			int bkCol = -1;
			tabDefs.GetLineColor(e.lineNo, ref txtCol, ref bkCol);
			if(bkCol == UT.colOrange)
			{
				btnDeleteDef.Text = "R&eactivate";
			}
			else
			{
				btnDeleteDef.Text = "&Delete";
			}
			EnableComboboxes(e.lineNo);
			CheckValuesChanged(e.lineNo );
		}


		private void btnCopy_Click(object sender, System.EventArgs e)
		{
			int i = 0;
			string strNewUrno = "";
			ITable mySRHTab = myDB["SRH"];
			myMode = "NEW";
			oldRow = currDBRow.CopyRaw();
			oldRow["LSTU"] = "";
			oldRow["CDAT"] = UT.DateTimeToCeda(DateTime.Now);
			oldRow["USEC"] = UT.UserName;
			oldRow["USEU"] = "";
			currDBRow = mySRHTab.CreateEmptyRow();
			string strCurrName = txtRuleName.Text;
			strCurrName += "2";
			txtRuleName.Text = strCurrName;
			string strUHSS = txtSTSUrno.Text;
			currDBRow["UHSS"] = strUHSS;
			if(rbArrival.Checked == true)
				currDBRow["ADID"] = "A";
			else if(rbDeparture.Checked == true)
				currDBRow["ADID"] = "D";
			else //Turnaround
				currDBRow["ADID"] = "T";

			strNewUrno = currDBRow["URNO"];
			for(i = 0; i < oldRow.Count; i++)
			{
				currDBRow[i] = oldRow[i];
			}
			currDBRow["URNO"] = strNewUrno;
			oldRow["URNO"] = currDBRow["URNO"];
			currDBRow["NAME"] = strCurrName;
			currRow = currDBRow.CopyRaw();
			btnSave.BackColor = Color.Red;
			for(  i = 0; i < tabDefs.GetLineCount(); i++)
			{
				string strUrno = myDB.GetNextUrno();
				tabDefs.SetFieldValues(i, "URNO", strUrno);
				tabDefs.SetLineColor(i, UT.colBlack, UT.colYellow);
				tabDefs.SetFieldValues(i, "USRH", currDBRow["URNO"]);
			}
			SaveRule();
		}

		private void txtACTValue_Leave(object sender, System.EventArgs e)
		{
			string strValue = txtACTValue.Text;
			if(strValue == "")
			{
				txtACTUrno.Text = "";
				txtACTValue.ForeColor = Color.Black;
				return; //Need no check
			}
			ITable myTab = myDB["ACT"];
			bool blFound = false;
			for(int i = 0; i < myTab.Count && blFound == false; i++)
			{
				if(strValue == myTab[i]["ACT3"] || strValue == myTab[i]["ACT5"])
				{
					txtACTUrno.Text = myTab[i]["URNO"];
					blFound = true;
				}
			}
			if(blFound == true)
			{
				txtACTValue.ForeColor = Color.Black;
				txtACTGroupUrno.Text = "";
				txtACTGroupValue.Text = "";
			}
			else
				txtACTValue.ForeColor = Color.Red;
		}

		private void txtAPTValue_Leave(object sender, System.EventArgs e)
		{
			string strValue = txtAPTValue.Text;
			if(strValue == "")
			{
				txtAPTUrno.Text = "";
				txtAPTValue.ForeColor = Color.Black;
				return; //Need no check
			}
			ITable myTab = myDB["APT"];
			bool blFound = false;
			for(int i = 0; i < myTab.Count && blFound == false; i++)
			{
				if(strValue == myTab[i]["APC3"] || strValue == myTab[i]["APC4"])
				{
					txtAPTUrno.Text = myTab[i]["URNO"];
					blFound = true;
				}
			}
			if(blFound == true)
			{
				txtAPTValue.ForeColor = Color.Black;
				txtAPTGroupUrno.Text = "";
				txtAPTGroupValue.Text = "";
			}
			else
				txtAPTValue.ForeColor = Color.Red;
		}

		private void txtALTValue_Leave(object sender, System.EventArgs e)
		{
			string strValue = txtALTValue.Text;
			if(strValue == "")
			{
				txtALTUrno.Text = "";
				txtALTValue.ForeColor = Color.Black;
				return; //Need no check
			}
			ITable myTab = myDB["ALT"];
			bool blFound = false;
			for(int i = 0; i < myTab.Count && blFound == false; i++)
			{
				if(strValue == myTab[i]["ALC2"] || strValue == myTab[i]["ALC3"])
				{
					txtALTUrno.Text = myTab[i]["URNO"];
					blFound = true;
				}
			}
			if(blFound == true)
			{
				txtALTValue.ForeColor = Color.Black;
				txtALTGroupUrno.Text = "";
				txtALTGroupValue.Text = "";
			}
			else
				txtALTValue.ForeColor = Color.Red;
		}

		private void txtNATValue_Leave(object sender, System.EventArgs e)
		{
			string strValue = txtNATValue.Text;
			if(strValue == "")
			{
				txtNATUrno.Text = "";
				txtNATValue.ForeColor = Color.Black;
				return; //Need no check
			}
			ITable myTab = myDB["NAT"];
			bool blFound = false;
			for(int i = 0; i < myTab.Count && blFound == false; i++)
			{
				if(strValue == myTab[i]["TTYP"] || strValue == myTab[i]["TNAM"])
				{
					txtNATUrno.Text = myTab[i]["URNO"];
					blFound = true;
				}
			}
			if(blFound == true)
			{
				txtNATValue.ForeColor = Color.Black;
				txtNATGroupUrno.Text = "";
				txtNATGroupValue.Text = "";
			}
			else
				txtNATValue.ForeColor = Color.Red;
		}

		private void frmRules_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if(btnSave.BackColor == Color.Red)
			{
				DialogResult olResult;
				olResult = MessageBox.Show(this, "Do you want to save current rule?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question); 
				if ( olResult == DialogResult.Yes)
				{
					if(SaveRule() == false)
					{
						e.Cancel = true;
						return; 
					}
				}
				btnSave.BackColor = Color.Transparent;
			}
			//Store current geometry in Registry
			string theKey = "Software\\StatusManager\\RuleEditor";
			RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			if(rk == null)
			{
				rk = Registry.CurrentUser.OpenSubKey("Software\\StatusManager", true);
				rk.CreateSubKey("RuleEditor");
				rk.Close();
				theKey = "Software\\StatusManager\\RuleEditor";
				rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			}

			rk.SetValue("X", this.Left.ToString());
			rk.SetValue("Y", this.Top.ToString());
			rk.SetValue("Width", this.Width.ToString());
			rk.SetValue("Height", this.Height.ToString());
		}

		private void frmRules_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if( myCurrEditColumn == 15)
			{
				if (e.KeyChar >= 48 && e.KeyChar <= 57)
				{;}
				else
					e.Handled = false;
			}
		}


		private void frmRules_Closed(object sender, System.EventArgs e)
		{
			DE.Call_RuleEditorClosed();
		}

		private void txtACTGroupValue_MouseHover(object sender, System.EventArgs e)
		{
		}

		private void txtAPTGroupValue_MouseHover(object sender, System.EventArgs e)
		{
		}

		private void txtALTGroupValue_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			string strUSGR = txtALTGroupUrno.Text;
			if(strUSGR != "")
			{
				ITable myTab = myDB["SGM"];
				IRow [] rows = myTab.RowsByIndexValue("USGR", strUSGR);
				if(rows.Length > 0)
				{
					string strTip="";
					ITable myRefTab = myDB["ALT"];
					int cnt = 0;
					for(int i = 0; i < rows.Length; i++)
					{
						IRow [] rowsACT = myRefTab.RowsByIndexValue("URNO", rows[i]["UVAL"]);
						if(rowsACT.Length > 0)
						{
							if((cnt % 25 == 0) && cnt != 0)
							{
								strTip += "\n";
							}
							strTip += rowsACT[0]["ALC2"]+"-"+rowsACT[0]["ALC3"]+",";
							cnt++;
						}
					}
					if(strTip != "")
						toolTip1.SetToolTip(txtALTGroupValue, strTip);
					else
						toolTip1.SetToolTip(txtALTGroupValue, "<None>");
				}
				else 
					toolTip1.SetToolTip(txtALTGroupValue, "<None>");
			}
		}

		private void txtACTGroupValue_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			string strUSGR = txtACTGroupUrno.Text;
			if(strUSGR != "")
			{
				ITable myTab = myDB["SGM"];
				IRow [] rows = myTab.RowsByIndexValue("USGR", strUSGR);
				if(rows.Length > 0)
				{
					string strTip="";
					ITable myACT = myDB["ACT"];
					int cnt = 0;
					for(int i = 0; i < rows.Length; i++)
					{
						IRow [] rowsACT = myACT.RowsByIndexValue("URNO", rows[i]["UVAL"]);
						if(rowsACT.Length > 0)
						{
							if((cnt % 25 == 0) && cnt != 0)
							{
								strTip += "\n";
							}
							strTip += rowsACT[0]["ACT3"]+",";
							cnt++;
						}
					}
					if(strTip != "")
						toolTip1.SetToolTip(txtACTGroupValue, strTip);
					else
						toolTip1.SetToolTip(txtACTGroupValue, "<None>");
				}
				else 
					toolTip1.SetToolTip(txtACTGroupValue, "<None>");
			}
		}

		private void txtAPTGroupValue_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			string strUSGR = txtAPTGroupUrno.Text;
			if(strUSGR != "")
			{
				ITable myTab = myDB["SGM"];
				IRow [] rows = myTab.RowsByIndexValue("USGR", strUSGR);
				if(rows.Length > 0)
				{
					string strTip="";
					ITable myRefTab = myDB["APT"];
					int cnt = 0;
					for(int i = 0; i < rows.Length; i++)
					{
						IRow [] rowsACT = myRefTab.RowsByIndexValue("URNO", rows[i]["UVAL"]);
						if(rowsACT.Length > 0)
						{
							if((cnt % 25 == 0) && cnt != 0)
							{
								strTip += "\n";
							}
							strTip += rowsACT[0]["APC3"]+",";
							cnt++;
						}
					}
					if(strTip != "")
						toolTip1.SetToolTip(txtAPTGroupValue, strTip);
					else
						toolTip1.SetToolTip(txtAPTGroupValue, "<None>");
				}
				else 
					toolTip1.SetToolTip(txtAPTGroupValue, "<None>");
			}
		}

		private void txtNATGroupValue_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			string strUSGR = txtNATGroupUrno.Text;
			if(strUSGR != "")
			{
				ITable myTab = myDB["SGM"];
				IRow [] rows = myTab.RowsByIndexValue("USGR", strUSGR);
				if(rows.Length > 0)
				{
					string strTip="";
					ITable myRefTab = myDB["NAT"];
					int cnt = 0;
					for(int i = 0; i < rows.Length; i++)
					{
						IRow [] rowsACT = myRefTab.RowsByIndexValue("URNO", rows[i]["UVAL"]);
						if(rowsACT.Length > 0)
						{
							if((cnt % 25 == 0) && cnt != 0)
							{
								strTip += "\n";
							}
							strTip += rowsACT[0]["TNAM"]+",";
							cnt++;
						}
					}
					if(strTip != "")
						toolTip1.SetToolTip(txtNATGroupValue, strTip);
					else
						toolTip1.SetToolTip(txtNATGroupValue, "<None>");
				}
				else 
					toolTip1.SetToolTip(txtNATGroupValue, "<None>");
			}
		}

		private void btnRecalcFlights_Click(object sender, System.EventArgs e)
		{
			frmRecalculateRules olDlg = new frmRecalculateRules();
			if(olDlg.ShowDialog(this) == DialogResult.OK)
			{
				//Call the Server is done from the dialog itself !!
			}
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void dtValidFrom_ValueChanged(object sender, System.EventArgs e)
		{
			CheckChanges();
		}

		private void dtValidTo_ValueChanged(object sender, System.EventArgs e)
		{
			CheckChanges();
		}

		private void txtMAXT_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if ((e.KeyChar >= 48 && e.KeyChar <= 57) || e.KeyChar == 8)
			{;}
			else
				e.Handled = true;
		}

		private void txtMAXT_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
//			if (e.KeyCode >= Keys.D0 && e.KeyCode <= Keys.D9 )
//			{;}
//			else
//				e.Handled = true;
		}


	}
}
