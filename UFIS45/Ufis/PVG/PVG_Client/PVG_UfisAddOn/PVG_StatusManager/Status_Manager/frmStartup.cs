using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Ufis.Utils ;

namespace Status_Manager
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class frmStartup : System.Windows.Forms.Form
	{
		private double myOpacity = 0.9f;


		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.PictureBox pictureBox2;
		private System.Windows.Forms.TextBox txtApplication;
		public System.Windows.Forms.TextBox txtCurrentStatus;
		public System.Windows.Forms.TextBox txtDoneStatus;
		public static frmStartup myThis;
		private System.Timers.Timer timerSplash;
		private System.Timers.Timer timerClose;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmStartup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			myThis = this;
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		/// <summary>
		/// returns the this reference to get access from outside the class
		/// </summary>
		/// <returns>Reference to frmStartup</returns>
		public static frmStartup GetThis()
		{
			return myThis;
		}
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmStartup));
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.txtApplication = new System.Windows.Forms.TextBox();
			this.txtCurrentStatus = new System.Windows.Forms.TextBox();
			this.txtDoneStatus = new System.Windows.Forms.TextBox();
			this.timerSplash = new System.Timers.Timer();
			this.timerClose = new System.Timers.Timer();
			((System.ComponentModel.ISupportInitialize)(this.timerSplash)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.timerClose)).BeginInit();
			this.SuspendLayout();
			// 
			// pictureBox1
			// 
			this.pictureBox1.AccessibleDescription = resources.GetString("pictureBox1.AccessibleDescription");
			this.pictureBox1.AccessibleName = resources.GetString("pictureBox1.AccessibleName");
			this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("pictureBox1.Anchor")));
			this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
			this.pictureBox1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("pictureBox1.Dock")));
			this.pictureBox1.Enabled = ((bool)(resources.GetObject("pictureBox1.Enabled")));
			this.pictureBox1.Font = ((System.Drawing.Font)(resources.GetObject("pictureBox1.Font")));
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("pictureBox1.ImeMode")));
			this.pictureBox1.Location = ((System.Drawing.Point)(resources.GetObject("pictureBox1.Location")));
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("pictureBox1.RightToLeft")));
			this.pictureBox1.Size = ((System.Drawing.Size)(resources.GetObject("pictureBox1.Size")));
			this.pictureBox1.SizeMode = ((System.Windows.Forms.PictureBoxSizeMode)(resources.GetObject("pictureBox1.SizeMode")));
			this.pictureBox1.TabIndex = ((int)(resources.GetObject("pictureBox1.TabIndex")));
			this.pictureBox1.TabStop = false;
			this.pictureBox1.Text = resources.GetString("pictureBox1.Text");
			this.pictureBox1.Visible = ((bool)(resources.GetObject("pictureBox1.Visible")));
			// 
			// pictureBox2
			// 
			this.pictureBox2.AccessibleDescription = resources.GetString("pictureBox2.AccessibleDescription");
			this.pictureBox2.AccessibleName = resources.GetString("pictureBox2.AccessibleName");
			this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("pictureBox2.Anchor")));
			this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
			this.pictureBox2.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("pictureBox2.Dock")));
			this.pictureBox2.Enabled = ((bool)(resources.GetObject("pictureBox2.Enabled")));
			this.pictureBox2.Font = ((System.Drawing.Font)(resources.GetObject("pictureBox2.Font")));
			this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
			this.pictureBox2.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("pictureBox2.ImeMode")));
			this.pictureBox2.Location = ((System.Drawing.Point)(resources.GetObject("pictureBox2.Location")));
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("pictureBox2.RightToLeft")));
			this.pictureBox2.Size = ((System.Drawing.Size)(resources.GetObject("pictureBox2.Size")));
			this.pictureBox2.SizeMode = ((System.Windows.Forms.PictureBoxSizeMode)(resources.GetObject("pictureBox2.SizeMode")));
			this.pictureBox2.TabIndex = ((int)(resources.GetObject("pictureBox2.TabIndex")));
			this.pictureBox2.TabStop = false;
			this.pictureBox2.Text = resources.GetString("pictureBox2.Text");
			this.pictureBox2.Visible = ((bool)(resources.GetObject("pictureBox2.Visible")));
			// 
			// txtApplication
			// 
			this.txtApplication.AccessibleDescription = resources.GetString("txtApplication.AccessibleDescription");
			this.txtApplication.AccessibleName = resources.GetString("txtApplication.AccessibleName");
			this.txtApplication.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtApplication.Anchor")));
			this.txtApplication.AutoSize = ((bool)(resources.GetObject("txtApplication.AutoSize")));
			this.txtApplication.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtApplication.BackgroundImage")));
			this.txtApplication.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.txtApplication.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtApplication.Dock")));
			this.txtApplication.Enabled = ((bool)(resources.GetObject("txtApplication.Enabled")));
			this.txtApplication.Font = ((System.Drawing.Font)(resources.GetObject("txtApplication.Font")));
			this.txtApplication.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtApplication.ImeMode")));
			this.txtApplication.Location = ((System.Drawing.Point)(resources.GetObject("txtApplication.Location")));
			this.txtApplication.MaxLength = ((int)(resources.GetObject("txtApplication.MaxLength")));
			this.txtApplication.Multiline = ((bool)(resources.GetObject("txtApplication.Multiline")));
			this.txtApplication.Name = "txtApplication";
			this.txtApplication.PasswordChar = ((char)(resources.GetObject("txtApplication.PasswordChar")));
			this.txtApplication.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtApplication.RightToLeft")));
			this.txtApplication.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtApplication.ScrollBars")));
			this.txtApplication.Size = ((System.Drawing.Size)(resources.GetObject("txtApplication.Size")));
			this.txtApplication.TabIndex = ((int)(resources.GetObject("txtApplication.TabIndex")));
			this.txtApplication.Text = resources.GetString("txtApplication.Text");
			this.txtApplication.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtApplication.TextAlign")));
			this.txtApplication.Visible = ((bool)(resources.GetObject("txtApplication.Visible")));
			this.txtApplication.WordWrap = ((bool)(resources.GetObject("txtApplication.WordWrap")));
			// 
			// txtCurrentStatus
			// 
			this.txtCurrentStatus.AccessibleDescription = resources.GetString("txtCurrentStatus.AccessibleDescription");
			this.txtCurrentStatus.AccessibleName = resources.GetString("txtCurrentStatus.AccessibleName");
			this.txtCurrentStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtCurrentStatus.Anchor")));
			this.txtCurrentStatus.AutoSize = ((bool)(resources.GetObject("txtCurrentStatus.AutoSize")));
			this.txtCurrentStatus.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtCurrentStatus.BackgroundImage")));
			this.txtCurrentStatus.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.txtCurrentStatus.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtCurrentStatus.Dock")));
			this.txtCurrentStatus.Enabled = ((bool)(resources.GetObject("txtCurrentStatus.Enabled")));
			this.txtCurrentStatus.Font = ((System.Drawing.Font)(resources.GetObject("txtCurrentStatus.Font")));
			this.txtCurrentStatus.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtCurrentStatus.ImeMode")));
			this.txtCurrentStatus.Location = ((System.Drawing.Point)(resources.GetObject("txtCurrentStatus.Location")));
			this.txtCurrentStatus.MaxLength = ((int)(resources.GetObject("txtCurrentStatus.MaxLength")));
			this.txtCurrentStatus.Multiline = ((bool)(resources.GetObject("txtCurrentStatus.Multiline")));
			this.txtCurrentStatus.Name = "txtCurrentStatus";
			this.txtCurrentStatus.PasswordChar = ((char)(resources.GetObject("txtCurrentStatus.PasswordChar")));
			this.txtCurrentStatus.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtCurrentStatus.RightToLeft")));
			this.txtCurrentStatus.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtCurrentStatus.ScrollBars")));
			this.txtCurrentStatus.Size = ((System.Drawing.Size)(resources.GetObject("txtCurrentStatus.Size")));
			this.txtCurrentStatus.TabIndex = ((int)(resources.GetObject("txtCurrentStatus.TabIndex")));
			this.txtCurrentStatus.Text = resources.GetString("txtCurrentStatus.Text");
			this.txtCurrentStatus.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtCurrentStatus.TextAlign")));
			this.txtCurrentStatus.Visible = ((bool)(resources.GetObject("txtCurrentStatus.Visible")));
			this.txtCurrentStatus.WordWrap = ((bool)(resources.GetObject("txtCurrentStatus.WordWrap")));
			// 
			// txtDoneStatus
			// 
			this.txtDoneStatus.AccessibleDescription = resources.GetString("txtDoneStatus.AccessibleDescription");
			this.txtDoneStatus.AccessibleName = resources.GetString("txtDoneStatus.AccessibleName");
			this.txtDoneStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtDoneStatus.Anchor")));
			this.txtDoneStatus.AutoSize = ((bool)(resources.GetObject("txtDoneStatus.AutoSize")));
			this.txtDoneStatus.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtDoneStatus.BackgroundImage")));
			this.txtDoneStatus.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.txtDoneStatus.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtDoneStatus.Dock")));
			this.txtDoneStatus.Enabled = ((bool)(resources.GetObject("txtDoneStatus.Enabled")));
			this.txtDoneStatus.Font = ((System.Drawing.Font)(resources.GetObject("txtDoneStatus.Font")));
			this.txtDoneStatus.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtDoneStatus.ImeMode")));
			this.txtDoneStatus.Location = ((System.Drawing.Point)(resources.GetObject("txtDoneStatus.Location")));
			this.txtDoneStatus.MaxLength = ((int)(resources.GetObject("txtDoneStatus.MaxLength")));
			this.txtDoneStatus.Multiline = ((bool)(resources.GetObject("txtDoneStatus.Multiline")));
			this.txtDoneStatus.Name = "txtDoneStatus";
			this.txtDoneStatus.PasswordChar = ((char)(resources.GetObject("txtDoneStatus.PasswordChar")));
			this.txtDoneStatus.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtDoneStatus.RightToLeft")));
			this.txtDoneStatus.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtDoneStatus.ScrollBars")));
			this.txtDoneStatus.Size = ((System.Drawing.Size)(resources.GetObject("txtDoneStatus.Size")));
			this.txtDoneStatus.TabIndex = ((int)(resources.GetObject("txtDoneStatus.TabIndex")));
			this.txtDoneStatus.Text = resources.GetString("txtDoneStatus.Text");
			this.txtDoneStatus.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtDoneStatus.TextAlign")));
			this.txtDoneStatus.Visible = ((bool)(resources.GetObject("txtDoneStatus.Visible")));
			this.txtDoneStatus.WordWrap = ((bool)(resources.GetObject("txtDoneStatus.WordWrap")));
			// 
			// timerSplash
			// 
			this.timerSplash.Interval = 50;
			this.timerSplash.SynchronizingObject = this;
			this.timerSplash.Elapsed += new System.Timers.ElapsedEventHandler(this.timerSplash_Elapsed);
			// 
			// timerClose
			// 
			this.timerClose.Interval = 75;
			this.timerClose.SynchronizingObject = this;
			this.timerClose.Elapsed += new System.Timers.ElapsedEventHandler(this.timerClose_Elapsed);
			// 
			// frmStartup
			// 
			this.AccessibleDescription = resources.GetString("$this.AccessibleDescription");
			this.AccessibleName = resources.GetString("$this.AccessibleName");
			this.AutoScaleBaseSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScaleBaseSize")));
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMargin")));
			this.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMinSize")));
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = ((System.Drawing.Size)(resources.GetObject("$this.ClientSize")));
			this.Controls.Add(this.txtDoneStatus);
			this.Controls.Add(this.txtCurrentStatus);
			this.Controls.Add(this.txtApplication);
			this.Controls.Add(this.pictureBox2);
			this.Controls.Add(this.pictureBox1);
			this.Enabled = ((bool)(resources.GetObject("$this.Enabled")));
			this.Font = ((System.Drawing.Font)(resources.GetObject("$this.Font")));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.MaximumSize = ((System.Drawing.Size)(resources.GetObject("$this.MaximumSize")));
			this.MinimumSize = ((System.Drawing.Size)(resources.GetObject("$this.MinimumSize")));
			this.Name = "frmStartup";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.StartPosition = ((System.Windows.Forms.FormStartPosition)(resources.GetObject("$this.StartPosition")));
			this.Text = resources.GetString("$this.Text");
			this.Load += new System.EventHandler(this.frmStartup_Load);
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.frmStartup_Paint);
			((System.ComponentModel.ISupportInitialize)(this.timerSplash)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.timerClose)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void frmStartup_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics , 90f, this, Color.WhiteSmoke , Color.DarkGray  );			
		}

		private void frmStartup_Load(object sender, System.EventArgs e)
		{
			this.Text = "Startup Status-Manager on server " + UT.ServerName;
		}

		private void timerSplash_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			myOpacity += 0.1;
			this.Opacity = myOpacity;
			if(myOpacity >= 1)
			{
				timerSplash.Enabled = false;
			}
		}
		public void PrepareClose()
		{
			timerClose.Enabled = true;
		}

		private void timerClose_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			myOpacity -= 0.2;
			this.Opacity = myOpacity;
			pictureBox1.Refresh();
			txtCurrentStatus.Refresh();
			txtDoneStatus.Refresh();
			if(myOpacity <= 0f)
			{
				timerClose.Stop();
				timerClose.Enabled = false;
				this.Close();
			}
		}

	}
}
