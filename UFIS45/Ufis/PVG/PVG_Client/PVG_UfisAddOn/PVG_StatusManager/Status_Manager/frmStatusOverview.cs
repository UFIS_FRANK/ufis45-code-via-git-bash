using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Ufis.Data;
using Ufis.Utils;
using Microsoft.Win32;

namespace Status_Manager
{
	/// <summary>
	/// Summary description for frmStatusOverview.
	/// </summary>
	public class frmStatusOverview : System.Windows.Forms.Form
	{
		#region ---- MyMembers

		private IDatabase myDB = null;
		private ITable myOST = null;
		private ITable myAFT = null;
		private ITable mySDE = null;
		private ITable mySRH = null;
		private int myTabScrollPos = 0;
		private bool isInit = true;
		//Delegate definitions
		private DE.ConflicTimerElapsed myConflicTimerElapsed = null;
		private DE.OST_Changed	       delegateOST_Changed = null;
		private DE.ViewNameChanged	   delegateViewNameChanged = null;
		private DE.UTC_LocalTimeChanged delegateUTC_LocalTimeChanged = null;
		private DE.UpdateFlightData		delegateUpdateFlightData = null;
	
		#endregion ---- MyMembers


		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.PictureBox picTop;
		private System.Windows.Forms.ComboBox comboFlno;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.Button btnSearch;
		private AxTABLib.AxTAB tabList;
		private System.Windows.Forms.Label lblTop;
		private System.Windows.Forms.Panel panelFill;
		private System.Windows.Forms.Panel panelFillInFill;
		private System.Timers.Timer timerMinute;
		private System.Windows.Forms.Label lblFlno;
		private System.Windows.Forms.Label lblGatePos;
		private System.Windows.Forms.ComboBox comboGatePos;
		private System.Windows.Forms.Button btnPrint;
		private System.ComponentModel.IContainer components;


		public frmStatusOverview()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmStatusOverview));
			this.panelTop = new System.Windows.Forms.Panel();
			this.lblGatePos = new System.Windows.Forms.Label();
			this.comboGatePos = new System.Windows.Forms.ComboBox();
			this.lblFlno = new System.Windows.Forms.Label();
			this.btnPrint = new System.Windows.Forms.Button();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.btnSearch = new System.Windows.Forms.Button();
			this.btnClose = new System.Windows.Forms.Button();
			this.comboFlno = new System.Windows.Forms.ComboBox();
			this.picTop = new System.Windows.Forms.PictureBox();
			this.tabList = new AxTABLib.AxTAB();
			this.lblTop = new System.Windows.Forms.Label();
			this.panelFill = new System.Windows.Forms.Panel();
			this.panelFillInFill = new System.Windows.Forms.Panel();
			this.timerMinute = new System.Timers.Timer();
			this.panelTop.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabList)).BeginInit();
			this.panelFill.SuspendLayout();
			this.panelFillInFill.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.timerMinute)).BeginInit();
			this.SuspendLayout();
			// 
			// panelTop
			// 
			this.panelTop.Controls.Add(this.lblGatePos);
			this.panelTop.Controls.Add(this.comboGatePos);
			this.panelTop.Controls.Add(this.lblFlno);
			this.panelTop.Controls.Add(this.btnPrint);
			this.panelTop.Controls.Add(this.btnSearch);
			this.panelTop.Controls.Add(this.btnClose);
			this.panelTop.Controls.Add(this.comboFlno);
			this.panelTop.Controls.Add(this.picTop);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(0, 0);
			this.panelTop.Name = "panelTop";
			this.panelTop.Size = new System.Drawing.Size(864, 28);
			this.panelTop.TabIndex = 0;
			// 
			// lblGatePos
			// 
			this.lblGatePos.BackColor = System.Drawing.Color.Transparent;
			this.lblGatePos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblGatePos.Location = new System.Drawing.Point(288, 8);
			this.lblGatePos.Name = "lblGatePos";
			this.lblGatePos.Size = new System.Drawing.Size(60, 12);
			this.lblGatePos.TabIndex = 10;
			this.lblGatePos.Text = "Gate/Pos:";
			// 
			// comboGatePos
			// 
			this.comboGatePos.Location = new System.Drawing.Point(352, 4);
			this.comboGatePos.Name = "comboGatePos";
			this.comboGatePos.Size = new System.Drawing.Size(84, 21);
			this.comboGatePos.Sorted = true;
			this.comboGatePos.TabIndex = 3;
			this.comboGatePos.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboFlno_KeyPress);
			this.comboGatePos.TextChanged += new System.EventHandler(this.comboGatePos_TextChanged);
			// 
			// lblFlno
			// 
			this.lblFlno.BackColor = System.Drawing.Color.Transparent;
			this.lblFlno.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblFlno.Location = new System.Drawing.Point(76, 8);
			this.lblFlno.Name = "lblFlno";
			this.lblFlno.Size = new System.Drawing.Size(40, 12);
			this.lblFlno.TabIndex = 8;
			this.lblFlno.Text = "FLNO:";
			// 
			// btnPrint
			// 
			this.btnPrint.BackColor = System.Drawing.Color.Transparent;
			this.btnPrint.Dock = System.Windows.Forms.DockStyle.Right;
			this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnPrint.ImageIndex = 3;
			this.btnPrint.ImageList = this.imageList1;
			this.btnPrint.Location = new System.Drawing.Point(714, 0);
			this.btnPrint.Name = "btnPrint";
			this.btnPrint.Size = new System.Drawing.Size(75, 28);
			this.btnPrint.TabIndex = 6;
			this.btnPrint.TabStop = false;
			this.btnPrint.Text = "   &Print";
			this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
			// 
			// imageList1
			// 
			this.imageList1.ImageSize = new System.Drawing.Size(19, 19);
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.White;
			// 
			// btnSearch
			// 
			this.btnSearch.BackColor = System.Drawing.Color.Transparent;
			this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnSearch.ImageIndex = 0;
			this.btnSearch.ImageList = this.imageList1;
			this.btnSearch.Location = new System.Drawing.Point(0, 0);
			this.btnSearch.Name = "btnSearch";
			this.btnSearch.Size = new System.Drawing.Size(75, 28);
			this.btnSearch.TabIndex = 5;
			this.btnSearch.TabStop = false;
			this.btnSearch.Text = "   &Search";
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			// 
			// btnClose
			// 
			this.btnClose.BackColor = System.Drawing.Color.Transparent;
			this.btnClose.Dock = System.Windows.Forms.DockStyle.Right;
			this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnClose.ImageIndex = 1;
			this.btnClose.ImageList = this.imageList1;
			this.btnClose.Location = new System.Drawing.Point(789, 0);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(75, 28);
			this.btnClose.TabIndex = 7;
			this.btnClose.TabStop = false;
			this.btnClose.Text = "   &Close";
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// comboFlno
			// 
			this.comboFlno.Location = new System.Drawing.Point(120, 4);
			this.comboFlno.Name = "comboFlno";
			this.comboFlno.Size = new System.Drawing.Size(160, 21);
			this.comboFlno.Sorted = true;
			this.comboFlno.TabIndex = 1;
			this.comboFlno.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboFlno_KeyPress);
			this.comboFlno.TextChanged += new System.EventHandler(this.comboFlno_TextChanged);
			// 
			// picTop
			// 
			this.picTop.Dock = System.Windows.Forms.DockStyle.Fill;
			this.picTop.Location = new System.Drawing.Point(0, 0);
			this.picTop.Name = "picTop";
			this.picTop.Size = new System.Drawing.Size(864, 28);
			this.picTop.TabIndex = 0;
			this.picTop.TabStop = false;
			this.picTop.Paint += new System.Windows.Forms.PaintEventHandler(this.picTop_Paint);
			// 
			// tabList
			// 
			this.tabList.ContainingControl = this;
			this.tabList.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabList.Location = new System.Drawing.Point(0, 0);
			this.tabList.Name = "tabList";
			this.tabList.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabList.OcxState")));
			this.tabList.Size = new System.Drawing.Size(864, 222);
			this.tabList.TabIndex = 4;
			this.tabList.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabList_SendLButtonDblClick);
			this.tabList.OnVScroll += new AxTABLib._DTABEvents_OnVScrollEventHandler(this.tabList_OnVScroll);
			this.tabList.SendLButtonClick += new AxTABLib._DTABEvents_SendLButtonClickEventHandler(this.axTAB1_SendLButtonClick);
			// 
			// lblTop
			// 
			this.lblTop.BackColor = System.Drawing.Color.Black;
			this.lblTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblTop.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblTop.ForeColor = System.Drawing.Color.Lime;
			this.lblTop.Location = new System.Drawing.Point(0, 0);
			this.lblTop.Name = "lblTop";
			this.lblTop.Size = new System.Drawing.Size(864, 16);
			this.lblTop.TabIndex = 11;
			this.lblTop.Text = "Statuses ordered by most recent ones";
			this.lblTop.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// panelFill
			// 
			this.panelFill.Controls.Add(this.panelFillInFill);
			this.panelFill.Controls.Add(this.lblTop);
			this.panelFill.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelFill.Location = new System.Drawing.Point(0, 28);
			this.panelFill.Name = "panelFill";
			this.panelFill.Size = new System.Drawing.Size(864, 238);
			this.panelFill.TabIndex = 3;
			// 
			// panelFillInFill
			// 
			this.panelFillInFill.Controls.Add(this.tabList);
			this.panelFillInFill.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelFillInFill.Location = new System.Drawing.Point(0, 16);
			this.panelFillInFill.Name = "panelFillInFill";
			this.panelFillInFill.Size = new System.Drawing.Size(864, 222);
			this.panelFillInFill.TabIndex = 3;
			// 
			// timerMinute
			// 
			this.timerMinute.Enabled = true;
			this.timerMinute.Interval = 60000;
			this.timerMinute.SynchronizingObject = this;
			this.timerMinute.Elapsed += new System.Timers.ElapsedEventHandler(this.timerMinute_Elapsed);
			// 
			// frmStatusOverview
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(864, 266);
			this.Controls.Add(this.panelFill);
			this.Controls.Add(this.panelTop);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "frmStatusOverview";
			this.Text = "Status Overview ...";
			this.Load += new System.EventHandler(this.frmStatusOverview_Load);
			this.Closed += new System.EventHandler(this.frmStatusOverview_Closed);
			this.panelTop.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabList)).EndInit();
			this.panelFill.ResumeLayout(false);
			this.panelFillInFill.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.timerMinute)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion


		private void frmStatusOverview_Load(object sender, System.EventArgs e)
		{
			btnPrint.Parent = picTop;
			btnClose.Parent = picTop;
			btnSearch.Parent = picTop;
			lblFlno.Parent = picTop;
			lblGatePos.Parent = picTop;

			myDB = UT.GetMemDB();
			myOST = myDB["OST"];
			myAFT = myDB["AFT"];
			mySDE = myDB["SDE"];
			mySRH = myDB["SRH"];

			myConflicTimerElapsed = new DE.ConflicTimerElapsed(frmMain_OnConflicTimerElapsed);
			DE.OnConflicTimerElapsed += myConflicTimerElapsed;
			delegateOST_Changed = new Status_Manager.DE.OST_Changed(DE_OnOST_Changed);
			DE.OnOST_Changed += delegateOST_Changed;
			delegateViewNameChanged = new Status_Manager.DE.ViewNameChanged(DE_OnViewNameChanged);
			DE.OnViewNameChanged += delegateViewNameChanged; 
			delegateUTC_LocalTimeChanged = new Status_Manager.DE.UTC_LocalTimeChanged(DE_OnUTC_LocalTimeChanged);
			DE.OnUTC_LocalTimeChanged += delegateUTC_LocalTimeChanged;
			delegateUpdateFlightData = new Status_Manager.DE.UpdateFlightData(DE_OnUpdateFlightData);
			DE.OnUpdateFlightData += delegateUpdateFlightData;

//OSTTAB 
//URNO,UAFT,    USRH,  USDE,  SDAY,     CONA,            COTY,          RTAB,           RURN,        RFLD,        CONS
//Urno,AFT URNO,SRHTAB,SDETAB,Statustag,Confirmed actual,Conflict types,Referenztabelle,Referenzurno,Referenzfeld,Confirm time scheduled
			int idx = 0;
			tabList.ResetContent();
			tabList.HeaderString	   =    "URNO,FLNU,Min,Sort,Time,Rule,Status,A/D,FLNO,STA/STD,ETA/ETD,ONB/OFB,Pos,Gate";
			tabList.LogicalFieldList   =    "URNO,FLNU,MIN,SORT,TIME,RULE,STATUS,A/D,FLNO,STA/STD,ETA/ETD,ONB/OFB,POS,GATE";
			tabList.HeaderLengthString =    "-1,-1,60,-1,80,200,200,40,80,80,80,80,70,70";
			tabList.ColumnWidthString  =    "10,10,10,10,14,20,10,1,10,14,14,14,10,10";
			tabList.ColumnAlignmentString = "L,L,R,R,L,L,L,L,L,L,L,L,L,L";
			tabList.EnableHeaderSizing(true); 
			tabList.SetTabFontBold(true);
			idx = UT.GetItemNo(tabList.LogicalFieldList, "TIME");
			tabList.DateTimeSetColumnFormat( idx , "YYYYMMDDhhmmss", "hh':'mm'/'DD");
			idx = UT.GetItemNo(tabList.LogicalFieldList, "STA/STD");
			tabList.DateTimeSetColumnFormat( idx , "YYYYMMDDhhmmss", "hh':'mm'/'DD");
			idx = UT.GetItemNo(tabList.LogicalFieldList, "ETA/ETD");
			tabList.DateTimeSetColumnFormat( idx , "YYYYMMDDhhmmss", "hh':'mm'/'DD");
			idx = UT.GetItemNo(tabList.LogicalFieldList, "ONB/OFB");
			tabList.DateTimeSetColumnFormat( idx , "YYYYMMDDhhmmss", "hh':'mm'/'DD");
			tabList.LifeStyle = true;
			tabList.LineHeight = 18;
			string strColors = UT.colBlue + "," + UT.colBlue;
			tabList.CursorDecoration(tabList.LogicalFieldList, "B,T", "3,3", strColors);
			tabList.DefaultCursor = false;
			tabList.SetInternalLineBuffer(true);

			ReadRegistry();
			ReloadData();
			isInit = false;
		}

		private void ReloadData()
		{
			//tabList.HeaderString	   = "URNO,FLNU,Min,Sort,Time,Status,A/D,FLNO,STA/STD,ETA/ETD,ONB/OFB,Pos,Gate";
			//tabList.LogicalFieldList   = "URNO,FLNU,MIN,SORT,TIME,STATUS,A/D,FLNO,STA/STD,ETA/ETD,ONB/OFB,POS,GATE";
			//OSTTAB                     "URNO,USRH,USDE,SDAY,CONA,COTY,RTAB,RURN,RFLD,CONS,UAFT,LSTU"
			int i = 0;
			string strValues = "";
			string strMinutes = "";
			string strCONS = "";
			string strCONA = "";
			string strCONU = "";
			string strSDEName = "";
			string strCOTY = "";
			int ilSort = -1;
			string strSort = "";
			int lineColor = UT.colWhite;
			int textColor = UT.colBlack;
			int sortCol = tabList.CurrentSortColumn;
			bool lastSortOrderASC = tabList.SortOrderASC;
			int llCurSel = tabList.GetCurrentSelected();

			tabList.ResetContent();
			tabList.SetInternalLineBuffer(true);
			tabList.ShowVertScroller(false);

			StringBuilder strBuffer = new StringBuilder(50000);
			for( i = 0; i < myOST.Count; i++)
			{
				DateTime dat;
				DateTime datCONS;
				DateTime datCONA;
				DateTime datCONU;
				if(UT.IsTimeInUtc == true)
					dat = CFC.GetUTC();
				else
					dat = DateTime.Now;

				IRow [] aftRows = myAFT.RowsByIndexValue("URNO", myOST[i]["UAFT"]);
				IRow [] sdeRows = mySDE.RowsByIndexValue("URNO", myOST[i]["USDE"]);
				IRow aftRow = null;
				if(aftRows.Length > 0)//Just to avoid index out of range
				{
					aftRow = aftRows[0];
				}
				if(CFC.IsPassFilter(myOST[i], aftRow) == true)
				{
					strCONS = myOST[i]["CONS"];
					strCONA = myOST[i]["CONA"];
					strCONU = myOST[i]["CONU"];
					
					if(strCONU.Length == 14)
					{
						datCONU = UT.CedaFullDateToDateTime(strCONU);
						datCONS = UT.CedaFullDateToDateTime(strCONS);
						TimeSpan olTS = datCONU - datCONS;
						ilSort = 100000 - (int)olTS.TotalMinutes;
						strSort = ilSort.ToString();
						strMinutes = ((int)olTS.TotalMinutes).ToString();
					}
					else if(strCONA.Length == 14)
					{
						datCONA = UT.CedaFullDateToDateTime(strCONA);
						datCONS = UT.CedaFullDateToDateTime(strCONS);
						TimeSpan olTS = datCONA - datCONS;
						ilSort = 100000 - (int)olTS.TotalMinutes;
						strSort = ilSort.ToString();
						strMinutes = ((int)olTS.TotalMinutes).ToString();
					}
					else if(strCONS.Length == 14)
					{
						datCONS = UT.CedaFullDateToDateTime(strCONS);
						TimeSpan olTS = dat - datCONS;
						ilSort = 100000 - (int)olTS.TotalMinutes;
						strSort = ilSort.ToString();
						strMinutes = ((int)olTS.TotalMinutes).ToString();
					}
					else
					{
						strMinutes = "";
						ilSort = 1000000;
						strSort = ilSort.ToString();
					}
					strCOTY = myOST[i]["COTY"];
					textColor = UT.colBlack;
					switch (strCOTY)
					{
						case "N":
							lineColor = UT.colRed;
							textColor = UT.colWhite;
							break;
						case "A":
							lineColor = UT.colLightOrange;
							break;
						case "C":
							lineColor = UT.colLightGreen;
							break;
						default:
							lineColor = UT.colWhite;
							break;

					}
					string strRule = "";
					IRow [] srhRows = mySRH.RowsByIndexValue("URNO", myOST[i]["USRH"]);
					if(srhRows.Length > 0)
					{
						strRule = srhRows[0]["NAME"];
					}
					strValues  = myOST[i]["URNO"] + ",";
					strValues += myOST[i]["UAFT"] + ",";
					strValues  += strMinutes + "," + strSort + ",";
					//strValues += strMinutes + ",";
					strValues += strCONS + "," + strRule + ",";
					if(sdeRows.Length > 0)
						strSDEName = sdeRows[0]["NAME"];
					else
						strSDEName = "";

					strValues += strSDEName + ",";
					//Flight relevant data ==> A/D,FLNO,STA/STD,ETA/ETD,ONB/OFB,POS,GATE";
					if(aftRows.Length > 0)
					{
						if(aftRows[0]["ADID"] == "A")
						{
							strValues += "A,";
							strValues += aftRows[0]["FLNO"] + ",";
							strValues += aftRows[0]["STOA"] + ",";
							strValues += aftRows[0]["ETAI"] + ",";
							strValues += aftRows[0]["ONBL"] + ",";
							strValues += aftRows[0]["GTA1"] + ",";
							strValues += aftRows[0]["PSTA"];
						}
						else
						{
							strValues += "D,";
							strValues += aftRows[0]["FLNO"] + ",";
							strValues += aftRows[0]["STOD"] + ",";
							strValues += aftRows[0]["ETDI"] + ",";
							strValues += aftRows[0]["OFBL"] + ",";
							strValues += aftRows[0]["GTD1"] + ",";
							strValues += aftRows[0]["PSTD"];
						}
					}
					tabList.InsertTextLine(strValues, false);
					tabList.SetLineColor(tabList.GetLineCount()-1, textColor, lineColor);
				}//if(IsPassFilter(myOST[i], aftRows[0]) == true)
				//strBuffer.Append(strValues + "\n");
			}
			//tabList.InsertBuffer(strBuffer.ToString(), "\n");
			if(sortCol == -1 || isInit == true) 
			{
				sortCol = 3;
				lastSortOrderASC = true;
			}
			if(lastSortOrderASC == true)
			{
				tabList.Sort( sortCol.ToString(), true, true);
			}
			else
			{ 
				tabList.Sort( sortCol.ToString(), false, true);
			}

			tabList.IndexCreate("URNO", 0);
			tabList.IndexCreate("FLNU", 1);
			//tabList.SetCurrentSelection(0); 
			tabList.AutoSizeByHeader = true;
			tabList.AutoSizeColumns();
			tabList.ShowVertScroller(true);
			tabList.OnVScrollTo( myTabScrollPos);
			tabList.SetCurrentSelection(llCurSel);
			tabList.Refresh();
			lblTop.Text = tabList.GetLineCount().ToString() + " statuses"; 
						  //" Statuses ordered by most recent ones" + "--------"; 
		}

		private void picTop_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, picTop, Color.WhiteSmoke , Color.DarkGray  );
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{ 
			this.Close();
		}

		private void frmStatusOverview_Closed(object sender, System.EventArgs e)
		{
			DE.Call_StatusOverviewClosed();
			DE.OnConflicTimerElapsed -= myConflicTimerElapsed;
			DE.OnOST_Changed -= delegateOST_Changed;
			DE.OnViewNameChanged -= delegateViewNameChanged; 
			DE.OnUTC_LocalTimeChanged -= delegateUTC_LocalTimeChanged;
			DE.OnUpdateFlightData -= delegateUpdateFlightData;

			WriteRegistry();
		}

		private void axTAB1_SendLButtonClick(object sender, AxTABLib._DTABEvents_SendLButtonClickEvent e)
		{
		
		}

		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			Search();
		}
		private void ReadRegistry()
		{
			int myX=-1, myY=-1, myW=-1, myH=-1;
			string regVal = "";
			string theKey = "Software\\StatusManager\\StatusOverView";
			RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			if(rk != null)
			{
				regVal = rk.GetValue("X", "-1").ToString();
				myX = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Y", "-1").ToString();
				myY = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Width", "-1").ToString();
				myW = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Height", "-1").ToString();
				myH = Convert.ToInt32(regVal);
			}
			if((myW != -1 && myH != -1) && (myX < Screen.PrimaryScreen.Bounds.Width))
			{
				this.Left = myX;
				this.Top = myY;
				this.Width = myW;
				this.Height = myH;
			}
		}
		private void WriteRegistry()
		{
			string theKey = "Software\\StatusManager\\StatusOverView";
			RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			if(rk == null)
			{
				rk = Registry.CurrentUser.OpenSubKey("Software\\StatusManager", true);
				rk.CreateSubKey("StatusOverView");
				rk.Close();
				theKey = "Software\\StatusManager\\StatusOverView";
				rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			}

			rk.SetValue("X", this.Left.ToString());
			rk.SetValue("Y", this.Top.ToString());
			rk.SetValue("Width", this.Width.ToString());
			rk.SetValue("Height", this.Height.ToString());
		}

		private void comboFlno_TextChanged(object sender, System.EventArgs e)
		{
			if(comboFlno.Text != "")
			{
				comboGatePos.Text = "";
				Search();
			}
		}

		private void comboFlno_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if(e.KeyChar == (char)13)
			{
				Search();
			}
		}
		private void Search()
		{
			string strSearchFlno = comboFlno.Text;
			string strSearchGatPos = comboGatePos.Text;
			if(strSearchFlno == "" && strSearchGatPos == "" )
			{
				MessageBox.Show(this, "No search text entered\nNothing to do.", "Annotation", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			else
			{
				if(strSearchFlno != "")
				{
					if(comboFlno.FindStringExact(strSearchFlno, -1) == -1)
					{
						comboFlno.Items.Add(strSearchFlno);
					}
				}
				if(strSearchGatPos != "")
				{
					if(comboGatePos.FindStringExact(strSearchGatPos, -1) == -1)
					{
						comboGatePos.Items.Add(strSearchGatPos);
					}
				}
			}
			//Now start the ultimate search
			bool blFound = false;
			int LineNo = -1;
			if(strSearchFlno != "")
			{
				int cnt = tabList.GetLineCount();
				int curSel = tabList.GetCurrentSelected();
				int selStart = curSel;
				int idxCol = UT.GetItemNo(tabList.LogicalFieldList, "FLNO");
				if(blFound == false && curSel < cnt)
				{
					string strRet = tabList.GetNextLineByColumnValue(idxCol, strSearchFlno, 2);
					if(strRet != "")
					{
						LineNo = Convert.ToInt32( strRet );
						blFound = true;
					}
					else
					{
						curSel++;
						tabList.SetCurrentSelection(curSel);
					}
				}
				if(blFound == false && selStart > 0)
				{
					curSel = -1;
					tabList.SetCurrentSelection(curSel);
					if(blFound == false && curSel <= selStart)
					{
						string strRet = tabList.GetNextLineByColumnValue(idxCol, strSearchFlno, 2);
						if(strRet != "")
						{
							LineNo = Convert.ToInt32( strRet );
							blFound = true;
						}
						else
						{
							curSel++;
							tabList.SetCurrentSelection(curSel);
						}
					}
				}
				if(blFound == true)
				{
					tabList.SetCurrentSelection(LineNo);
					tabList.OnVScrollTo(LineNo);
				}
			}
			if(strSearchGatPos != "")
			{
				//First seach for gate
				int cnt = tabList.GetLineCount();
				int curSel = tabList.GetCurrentSelected();
				int selStart = curSel;
				int idxCol = UT.GetItemNo(tabList.LogicalFieldList, "GATE");
				if(blFound == false && curSel < cnt)
				{
					string strRet = tabList.GetNextLineByColumnValue(idxCol, strSearchGatPos, 2);
					if(strRet != "")
					{
						LineNo = Convert.ToInt32( strRet );
						blFound = true;
					}
					else
					{
						curSel++;
						tabList.SetCurrentSelection(curSel);
					}
				}
				if(blFound == false && selStart > 0)
				{
					curSel = 0;
					tabList.SetCurrentSelection(curSel);
					if(blFound == false && curSel <= selStart)
					{
						string strRet = tabList.GetNextLineByColumnValue(idxCol, strSearchGatPos, 2);
						if(strRet != "")
						{
							LineNo = Convert.ToInt32( strRet );
							blFound = true;
						}
						else
						{
							curSel++;
							tabList.SetCurrentSelection(curSel);
						}
					}
				}
				if(blFound == true)
				{
					tabList.SetCurrentSelection(LineNo);
					tabList.OnVScrollTo(LineNo);
				}
				else
				{
					//last search for position
					idxCol = UT.GetItemNo(tabList.LogicalFieldList, "POS");
					if(blFound == false && curSel < cnt)
					{
						string strRet = tabList.GetNextLineByColumnValue(idxCol, strSearchGatPos, 2);
						if(strRet != "")
						{
							LineNo = Convert.ToInt32( strRet );
							blFound = true;
						}
						else
						{
							curSel++;
							tabList.SetCurrentSelection(curSel);
						}
					}
					if(blFound == false && selStart > 0)
					{
						curSel = 0;
						tabList.SetCurrentSelection(curSel);
						if(blFound == false && curSel <= selStart)
						{
							string strRet = tabList.GetNextLineByColumnValue(idxCol, strSearchGatPos, 2);
							if(strRet != "")
							{
								LineNo = Convert.ToInt32( strRet );
								blFound = true;
							}
							else
							{
								curSel++;
								tabList.SetCurrentSelection(curSel);
							}
						}
					}
					if(blFound == true)
					{
						tabList.SetCurrentSelection(LineNo);
						tabList.OnVScrollTo(LineNo);
					}

				}
			}
			tabList.Refresh();
			
		}

		private void timerMinute_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
//			int llCurSel = tabList.GetCurrentSelected();
//			ReloadData();		
//			tabList.OnVScrollTo( myTabScrollPos);
//			tabList.SetCurrentSelection(llCurSel);
		}

		private void tabList_OnVScroll(object sender, AxTABLib._DTABEvents_OnVScrollEvent e)
		{
			myTabScrollPos = e.lineNo;
		}

		private void frmMain_OnConflicTimerElapsed()
		{
			ReloadData();		
		}

		private void tabList_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo == -1)
			{
				int sortCol = e.colNo;
				if(sortCol == 2)
				{
					sortCol = 3;
				}
				if( sortCol == tabList.CurrentSortColumn)
				{
					if(tabList.SortOrderASC == true)
					{
						tabList.Sort( sortCol.ToString(), false, true);
					}
					else
					{ 
						tabList.Sort( sortCol.ToString(), true, true);
					}
				}
				else
				{
					tabList.Sort( sortCol.ToString(), true, true);
				}
				tabList.Refresh();
			}
			else
			{
				string strUrno = tabList.GetFieldValue(e.lineNo, "FLNU");
				string strOSTUrno = tabList.GetFieldValue(e.lineNo, "URNO");
				myAFT = myDB["AFT"];
				IRow [] rows = myAFT.RowsByIndexValue("URNO", strUrno);
				if(rows.Length > 0)
				{
					DE.Call_DetailStatusDlg(rows[0]["URNO"], rows[0]["RKEY"], strOSTUrno);
					DE.Call_NavigateToFlight(this, rows[0]["URNO"]);
				}
			}
		}

		private void DE_OnOST_Changed(object sender, string strOstUrno, State state)
		{
			if(sender != (object)this)
			{
				//tabList.HeaderString	   = "URNO,FLNU,Min,Time,Status,A/D,FLNO,STA/STD,ETA/ETD,ONB/OFB,Pos,Gate";
				//tabList.LogicalFieldList   = "URNO,FLNU,MIN,TIME,STATUS,A/D,FLNO,STA/STD,ETA/ETD,ONB/OFB,POS,GATE";
				//OSTTAB                     "URNO,USRH,USDE,SDAY,CONA,COTY,RTAB,RURN,RFLD,CONS,UAFT,LSTU"
				string strValues = "";
				string strMinutes = "";
				string strSDEName = "";
				string strCOTY = "";
				int ilSort = -1;
				string strSort = "";
				int lineColor = UT.colWhite;
				int textColor = UT.colBlack;
				tabList.SetInternalLineBuffer(true);
				string strCnt = tabList.GetLinesByIndexValue("URNO", strOstUrno, 0);
				int ilLine = tabList.GetNextResultLine();
				if(ilLine > -1)
				{
					if(state == State.Deleted)
					{
						tabList.DeleteLine(ilLine);
						tabList.Refresh();
						return;
					}
				}

				IRow [] ostRows = myOST.RowsByIndexValue("URNO", strOstUrno);
				if(ostRows.Length > 0)
				{
					DateTime dat;
					DateTime datCONS;
					DateTime datCONA;
					DateTime datCONU;
					if(UT.IsTimeInUtc == true)
						dat = CFC.GetUTC();
					else
						dat = DateTime.Now;

					IRow [] aftRows = myAFT.RowsByIndexValue("URNO", ostRows[0]["UAFT"]);
					IRow [] sdeRows = mySDE.RowsByIndexValue("URNO", ostRows[0]["USDE"]);
					string strCONS = ostRows[0]["CONS"];
					string strCONA = ostRows[0]["CONA"];
					string strCONU = ostRows[0]["CONU"];
					if(strCONU.Length == 14)
					{
						datCONU = UT.CedaFullDateToDateTime(strCONU);
						datCONS = UT.CedaFullDateToDateTime(strCONS);
						TimeSpan olTS = datCONU - datCONS;
						ilSort = 100000 - (int)olTS.TotalMinutes;
						strSort = ilSort.ToString();
						strMinutes = ((int)olTS.TotalMinutes).ToString();
					}
					else if(strCONA.Length == 14)
					{
						datCONA = UT.CedaFullDateToDateTime(strCONA);
						datCONS = UT.CedaFullDateToDateTime(strCONS);
						TimeSpan olTS = datCONA - datCONS;
						ilSort = 100000 - (int)olTS.TotalMinutes;
						strSort = ilSort.ToString();
						strMinutes = ((int)olTS.TotalMinutes).ToString();
					}
					else if(strCONS.Length == 14)
					{
						datCONS = UT.CedaFullDateToDateTime(strCONS);
						TimeSpan olTS = dat - datCONS;
						ilSort = 100000 - (int)olTS.TotalMinutes;
						strSort = ilSort.ToString();
						strMinutes = ((int)olTS.TotalMinutes).ToString();
					}
					else
					{
						strMinutes = "";
						ilSort = 1000000;
						strSort = ilSort.ToString();
					}
					strCOTY = ostRows[0]["COTY"];
					textColor = UT.colBlack;
					switch (strCOTY)
					{
						case "N":
							lineColor = UT.colRed;
							textColor = UT.colWhite;
							break;
						case "A":
							lineColor = UT.colLightOrange;
							break;
						case "C":
							lineColor = UT.colLightGreen;
							break;
						default:
							lineColor = UT.colWhite;
							break;

					}
					string strRule = "";
					IRow [] srhRows = mySRH.RowsByIndexValue("URNO", ostRows[0]["USRH"]);
					if(srhRows.Length > 0)
					{
						strRule = srhRows[0]["NAME"];
					}
					strValues  = ostRows[0]["URNO"] + ",";
					strValues += ostRows[0]["UAFT"] + ",";
					strValues  += strMinutes + "," + strSort + ",";
					strValues += strCONS + "," + strRule + ",";
					if(sdeRows.Length > 0)
						strSDEName = sdeRows[0]["NAME"];
					else
						strSDEName = "";

					strValues += strSDEName + ",";
					//Flight relevant data ==> A/D,FLNO,STA/STD,ETA/ETD,ONB/OFB,POS,GATE";
					if(aftRows.Length > 0)
					{
						if(aftRows[0]["ADID"] == "A")
						{
							strValues += "A,";
							strValues += aftRows[0]["FLNO"] + ",";
							strValues += aftRows[0]["STOA"] + ",";
							strValues += aftRows[0]["ETAI"] + ",";
							strValues += aftRows[0]["ONBL"] + ",";
							strValues += aftRows[0]["GTA1"] + ",";
							strValues += aftRows[0]["PSTA"];
						}
						else
						{
							strValues += "D,";
							strValues += aftRows[0]["FLNO"] + ",";
							strValues += aftRows[0]["STOD"] + ",";
							strValues += aftRows[0]["ETDI"] + ",";
							strValues += aftRows[0]["OFBL"] + ",";
							strValues += aftRows[0]["GTD1"] + ",";
							strValues += aftRows[0]["PSTD"];
						}
					}
					strCnt = tabList.GetLinesByIndexValue("URNO", strOstUrno, 0);
					//int ilLine = tabList.GetNextResultLine();
					if(ilLine > -1)
					{
						if(state == State.Modified)
						{
							tabList.SetFieldValues(ilLine, tabList.LogicalFieldList, strValues);
							tabList.SetLineColor(ilLine, textColor, lineColor);
						}
					}
					else
					{
						if(state == State.Created)
						{
							tabList.InsertTextLine(strValues, true);
							tabList.SetLineColor(tabList.GetLineCount()-1, textColor, lineColor);
						}
					}
				}
				tabList.Refresh();
			}
		}

		private void comboGatePos_TextChanged(object sender, System.EventArgs e)
		{
			if(comboGatePos.Text != "")
			{
				comboFlno.Text = "";
				Search();
			}
		}

		private void DE_OnViewNameChanged()
		{
			ReloadData();
		}

		private void DE_OnUTC_LocalTimeChanged()
		{
			ReloadData();
		}

		private void btnPrint_Click(object sender, System.EventArgs e)
		{
			
			this.Cursor = Cursors.WaitCursor;
			rptStatusOverview rpt = new rptStatusOverview(tabList, CFC.CurrentViewName);
			frmPreview pt = new frmPreview(rpt);
			pt.Show();
			this.Cursor = Cursors.Arrow;
		}

		private void DE_OnUpdateFlightData(object sender, string strAftUrno, State state)
		{
			string strRet = "";
			int llLine = -1;
			tabList.SetInternalLineBuffer(true);
			strRet = tabList.GetLinesByIndexValue("FLNU", strAftUrno, 0);
			llLine = tabList.GetNextResultLine();
			IRow [] aftRows = myAFT.RowsByIndexValue("URNO", strAftUrno); 
			while(llLine > -1 && aftRows.Length > 0)
			{
				//"URNO,FLNU,MIN,SORT,TIME,STATUS,A/D,FLNO,STA/STD,ETA/ETD,ONB/OFB,POS,GATE";
				if(aftRows[0]["ADID"] == "A")
				{
					tabList.SetFieldValues(llLine, "A/D", "A");
					tabList.SetFieldValues(llLine, "FLNO",    aftRows[0]["FLNO"]);
					tabList.SetFieldValues(llLine, "STA/STD", aftRows[0]["STOA"]);
					tabList.SetFieldValues(llLine, "ETA/ETD", aftRows[0]["ETAI"]);
					tabList.SetFieldValues(llLine, "ONB/OFB", aftRows[0]["ONBL"]);
					tabList.SetFieldValues(llLine, "POS",     aftRows[0]["GTA1"]);
					tabList.SetFieldValues(llLine, "GATE",    aftRows[0]["PSTA"]);
				}
				if(aftRows[0]["ADID"] == "D")
				{
					tabList.SetFieldValues(llLine, "A/D", "D");
					tabList.SetFieldValues(llLine, "FLNO",    aftRows[0]["FLNO"]);
					tabList.SetFieldValues(llLine, "STA/STD", aftRows[0]["STOD"]);
					tabList.SetFieldValues(llLine, "ETA/ETD", aftRows[0]["ETDI"]);
					tabList.SetFieldValues(llLine, "ONB/OFB", aftRows[0]["OFBL"]);
					tabList.SetFieldValues(llLine, "POS",     aftRows[0]["GTD1"]);
					tabList.SetFieldValues(llLine, "GATE",    aftRows[0]["PSTD"]);
				}
				llLine = tabList.GetNextResultLine();
			}
			tabList.Refresh();
		}
	}
}
