using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Ufis.Utils;
using Ufis.Data;
using System.Diagnostics.SymbolStore;
using BirdView;
using System.Reflection;
using Microsoft.Win32;
namespace Status_Manager
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class frmMain : System.Windows.Forms.Form
	{
		#region ------ MyRegion
		private UT.ExceptionOccured		delegateExceptionOccured = null;
		private DE.RuleEditorClosed		delegateRuleEditorClosed = null;
		private DE.StatusOverviewClosed delegateStatusOverviewClosed = null;
		private DE.StatusDetailClosed	delegateStatusDetailClosed = null;
		private DE.BirdViewSaved		delegateBirdViewSaved = null;
		private DE.OpenDetailStatusDlg  delegateOpenDetailStatusDlg = null;
		private DE.FindList_Closed		delegateFindListClosed = null;
		private DE.NavigateToFlight     delegateNavigateToFlight = null;
		private DE.UpdateFlightData		delegateUpdateFlightData = null;
		private DE.ViewNameChanged		delegateViewNameChanged = null;
		private DE.UTC_LocalTimeChanged delegateUTC_LocalTimeChanged = null;
		private DE.CLO_Message			delegateCLO_Message = null;
		private DE.ReloadCompleteOnlineStatuses delegateReloadCompleteOnlineStatuses = null;

		frmDetailStatus myDetailDlg = null;
		frmFind			myFindDlg	= null;
		private frmData DataForm;
		private frmStartup StartForm;
		public int colorArrivalBar = UT.colWhite;
		public int colorDepartureBar = UT.colCyan;
		public bool bmLeft = true;
		public string [] args;
		private IDatabase myDB = null;
		private ITable myAFT = null;
		private ITable myOST = null;
		bool isInit = false;
		string [] arrAftTimeFields;
		/// <summary>
		/// The default arrival bar len (default is 15 minutes)
		/// </summary>
		public int imArrivalBarLen = 15; 
		/// <summary>
		/// The default departure bar len (default is 15 minutes)
		/// </summary>
		public int imDepartureBarLen = 15;
		private frmRules myRulesDialog = null;
		private frmStatusOverview myStatusOverview = null;
		private string omCurrentSelectedUrno = "";
		private bool bmWithBirdView = false;
		#endregion ----- MyRegion

		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.CheckBox cbOpen;
		private System.Windows.Forms.CheckBox cbReorgGantt;
		private System.Windows.Forms.CheckBox cbNow;
		private System.Windows.Forms.CheckBox cbSetup;
		private System.Windows.Forms.CheckBox cbSearch;
		private System.Windows.Forms.CheckBox cbView;
		private System.Windows.Forms.CheckBox cbLoad;
		private System.Windows.Forms.Panel panel1;
		private AxUGANTTLib.AxUGantt myGantt;
		private System.Windows.Forms.Label txtCalc;


		private System.Windows.Forms.Label lblAboveText;
		private System.Windows.Forms.Button btnDebug;
		private System.Windows.Forms.Button btnOpenVIewPoint;
		private System.Windows.Forms.Button btnEditViewPoint;
		public System.Windows.Forms.ComboBox cbViewPoints;
		private System.Windows.Forms.Button btnNewViewPoint;
		private System.Windows.Forms.Panel panel5;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.PictureBox pictureBox2;
		private System.Windows.Forms.Label lblUTCTime;
		private System.Windows.Forms.Label lblLocalTime;
		private System.Timers.Timer timer1;
		private System.Windows.Forms.CheckBox cbRules;
		private Ufis.Utils.BCBlinker bcBlinker1;
		private System.Windows.Forms.Button btnAbout;
		private System.Windows.Forms.Button btnDeleteViewPoint;
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.CheckBox cbUTC;
		public System.Timers.Timer timerConflictCheck;
		private System.Timers.Timer timerCloseStartup;
		private System.Timers.Timer timerOpacity;
		private AxAATLOGINLib.AxAatLogin LoginControl;
		private System.ComponentModel.IContainer components;

		public frmMain()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//Read ceda.ini and set it global to UT (UfisTools)
			String strServer="";
			String strHopo = "";
			Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\ceda.ini");
			strServer = myIni.IniReadValue("GLOBAL", "HOSTNAME");
			strHopo = myIni.IniReadValue("GLOBAL", "HOMEAIRPORT");
			UT.ServerName = strServer;
			UT.Hopo = strHopo;
			string strWithBirdView = myIni.IniReadValue("STATUSMANAGER", "BIRDVIEW");
			if(strWithBirdView == "TRUE")
			{
				bmWithBirdView = true;
			}
			//init BCProxy object
			//myBcProxy = new BcProxyLib.BcPrxy();
			//myBcProxy.OnBcReceive += new BcProxyLib._IBcPrxyEvents_OnBcReceiveEventHandler(BcEvent);				

		}
	
		/// <summary>
		/// Initializes the Objects and their settings, such as Gantt, and TabControl
		/// </summary>
		private void MyInit()
		{
			DateTime datLocal = DateTime.Now;
			DateTime datUTC = DateTime.UtcNow;//CFC.GetUTC();
			TimeSpan span = datLocal - datUTC;
			string strUTCConvertion = "";
			IniFile myIni = new IniFile("C:\\Ufis\\system\\ceda.ini");
			strUTCConvertion = myIni.IniReadValue("GLOBAL", "UTCCONVERTIONS");
			if(strUTCConvertion == "NO")
			{
				UT.UTCOffset = 0;
			}
			else
			{
				UT.UTCOffset = (short)span.Hours;
			}
			delegateExceptionOccured = new UT.ExceptionOccured(OnNewExeption);
			UT.OnExceptionOccured  += delegateExceptionOccured;
			delegateRuleEditorClosed = new DE.RuleEditorClosed(frmRules_OnRuleEditorClosed);
			DE.OnRuleEditorClosed += delegateRuleEditorClosed;
			delegateStatusOverviewClosed = new DE.StatusOverviewClosed(frmStatusOverview_OnStatusOverviewClosed);
			DE.OnStatusOverviewClosed += delegateStatusOverviewClosed;
			delegateStatusDetailClosed = new DE.StatusDetailClosed(frmDetailStatus_OnStatusDetailClosed);
			DE.OnStatusDetailClosed += delegateStatusDetailClosed;
			delegateOpenDetailStatusDlg = new DE.OpenDetailStatusDlg(frmStatusOverview_OnOpenDetailStatusDlg);
			DE.OnOpenDetailStatusDlg += delegateOpenDetailStatusDlg;
			delegateFindListClosed = new Status_Manager.DE.FindList_Closed(DE_OnFindList_Closed);
			DE.OnFindList_Closed += delegateFindListClosed;
			delegateNavigateToFlight = new Status_Manager.DE.NavigateToFlight(DE_OnNavigateToFlight);
			DE.OnNavigateToFlight += delegateNavigateToFlight;
			delegateUpdateFlightData = new Status_Manager.DE.UpdateFlightData(DE_OnUpdateFlightData);
			DE.OnUpdateFlightData += delegateUpdateFlightData;
			delegateViewNameChanged = new Status_Manager.DE.ViewNameChanged(DE_OnViewNameChanged);
			DE.OnViewNameChanged += delegateViewNameChanged;
			delegateUTC_LocalTimeChanged = new Status_Manager.DE.UTC_LocalTimeChanged(DE_OnUTC_LocalTimeChanged);
			DE.OnUTC_LocalTimeChanged += delegateUTC_LocalTimeChanged;
			delegateCLO_Message = new Status_Manager.DE.CLO_Message(DE_OnCLO_Message);
			DE.OnCLO_Message += delegateCLO_Message;
			delegateReloadCompleteOnlineStatuses = new Status_Manager.DE.ReloadCompleteOnlineStatuses(DE_OnReloadCompleteOnlineStatuses);
			DE.OnReloadCompleteOnlineStatuses += delegateReloadCompleteOnlineStatuses;

			InitGantt();
			myAFT = myDB["AFT"];
			myOST = myDB["OST"];
			CFC.MyInit(DataForm.tabViews);
			CFC.CurrentViewName = "<DEFAULT>";

		}
		public void InitGantt()
		{
			myGantt.ResetContent();
			myGantt.TabHeaderLengthString = "100";                  //init the width of the tab-columns
			myGantt.TabSetHeaderText ("All Flights");               //set the tab-headlines
			myGantt.SplitterPosition = 100;                         //the splitterposition depends on the
			myGantt.TabBodyFontName = "Arial";
			myGantt.TabFontSize = 14;								
			myGantt.TabHeaderFontName = "Arial";
			myGantt.TabHeaderFontSize = 14;
			myGantt.BarOverlapOffset = 10;
			myGantt.BarNumberExpandLine = 0;
			myGantt.TabBodyFontBold = true;
			myGantt.TabHeaderFontBold = true;
			myGantt.LifeStyle = true;
			//Calc the earliest and latest flight to get the timeframe for gantt
			DateTime datFrom = UT.TimeFrameFrom;
			DateTime datTo = UT.TimeFrameTo;
			DateTime datTmp;
			myAFT = myDB["AFT"];
			for(int i = 0; i < myAFT.Count; i++)
			{
				if(myAFT[i]["ADID"] == "A")
				{
					datTmp = UT.CedaFullDateToDateTime(myAFT[i]["TIFA"]);
					if(datFrom > datTmp)
						datFrom = datTmp;
				}
				if(myAFT[i]["ADID"] == "D")
				{
					datTmp = UT.CedaFullDateToDateTime(myAFT[i]["TIFD"]);
					if(datTo > datTmp)
						datTo = datTmp;
				}
			}
			if(datFrom < UT.TimeFrameFrom)
			{
				myGantt.TimeFrameFrom = datFrom.Subtract(new TimeSpan(0, 5, 0, 0, 0));
			}
			else
			{
				myGantt.TimeFrameFrom = UT.TimeFrameFrom; // CedaFullDateToVb(strTimeFrameFrom)  'dateFrom
			}
			if(datTo > UT.TimeFrameTo)
			{
				myGantt.TimeFrameTo = datTo.AddHours(5);
			}
			else
			{
				myGantt.TimeFrameTo = UT.TimeFrameTo;  // CedaFullDateToVb(strTimeFrameTo)
			}
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{

				if(DataForm != null)
				{
					DataForm.Dispose();
				}
				if (components != null) 
				{
					components.Dispose();
				}

				UT.DisposeMemDB();

			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmMain));
			this.panelTop = new System.Windows.Forms.Panel();
			this.LoginControl = new AxAATLOGINLib.AxAatLogin();
			this.cbUTC = new System.Windows.Forms.CheckBox();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.btnAbout = new System.Windows.Forms.Button();
			this.cbRules = new System.Windows.Forms.CheckBox();
			this.panel2 = new System.Windows.Forms.Panel();
			this.bcBlinker1 = new Ufis.Utils.BCBlinker();
			this.lblLocalTime = new System.Windows.Forms.Label();
			this.btnDeleteViewPoint = new System.Windows.Forms.Button();
			this.lblUTCTime = new System.Windows.Forms.Label();
			this.btnOpenVIewPoint = new System.Windows.Forms.Button();
			this.cbViewPoints = new System.Windows.Forms.ComboBox();
			this.btnNewViewPoint = new System.Windows.Forms.Button();
			this.btnEditViewPoint = new System.Windows.Forms.Button();
			this.panel5 = new System.Windows.Forms.Panel();
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.btnDebug = new System.Windows.Forms.Button();
			this.lblAboveText = new System.Windows.Forms.Label();
			this.cbOpen = new System.Windows.Forms.CheckBox();
			this.cbReorgGantt = new System.Windows.Forms.CheckBox();
			this.cbNow = new System.Windows.Forms.CheckBox();
			this.cbSetup = new System.Windows.Forms.CheckBox();
			this.cbSearch = new System.Windows.Forms.CheckBox();
			this.cbView = new System.Windows.Forms.CheckBox();
			this.cbLoad = new System.Windows.Forms.CheckBox();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.txtCalc = new System.Windows.Forms.Label();
			this.myGantt = new AxUGANTTLib.AxUGantt();
			this.timer1 = new System.Timers.Timer();
			this.timerConflictCheck = new System.Timers.Timer();
			this.timerCloseStartup = new System.Timers.Timer();
			this.timerOpacity = new System.Timers.Timer();
			this.panelTop.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.LoginControl)).BeginInit();
			this.panel2.SuspendLayout();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.myGantt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.timer1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.timerConflictCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.timerCloseStartup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.timerOpacity)).BeginInit();
			this.SuspendLayout();
			// 
			// panelTop
			// 
			this.panelTop.AccessibleDescription = resources.GetString("panelTop.AccessibleDescription");
			this.panelTop.AccessibleName = resources.GetString("panelTop.AccessibleName");
			this.panelTop.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("panelTop.Anchor")));
			this.panelTop.AutoScroll = ((bool)(resources.GetObject("panelTop.AutoScroll")));
			this.panelTop.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("panelTop.AutoScrollMargin")));
			this.panelTop.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("panelTop.AutoScrollMinSize")));
			this.panelTop.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelTop.BackgroundImage")));
			this.panelTop.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelTop.Controls.Add(this.LoginControl);
			this.panelTop.Controls.Add(this.cbUTC);
			this.panelTop.Controls.Add(this.btnAbout);
			this.panelTop.Controls.Add(this.cbRules);
			this.panelTop.Controls.Add(this.panel2);
			this.panelTop.Controls.Add(this.btnDebug);
			this.panelTop.Controls.Add(this.lblAboveText);
			this.panelTop.Controls.Add(this.cbOpen);
			this.panelTop.Controls.Add(this.cbReorgGantt);
			this.panelTop.Controls.Add(this.cbNow);
			this.panelTop.Controls.Add(this.cbSetup);
			this.panelTop.Controls.Add(this.cbSearch);
			this.panelTop.Controls.Add(this.cbView);
			this.panelTop.Controls.Add(this.cbLoad);
			this.panelTop.Controls.Add(this.pictureBox1);
			this.panelTop.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("panelTop.Dock")));
			this.panelTop.Enabled = ((bool)(resources.GetObject("panelTop.Enabled")));
			this.panelTop.Font = ((System.Drawing.Font)(resources.GetObject("panelTop.Font")));
			this.panelTop.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("panelTop.ImeMode")));
			this.panelTop.Location = ((System.Drawing.Point)(resources.GetObject("panelTop.Location")));
			this.panelTop.Name = "panelTop";
			this.panelTop.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("panelTop.RightToLeft")));
			this.panelTop.Size = ((System.Drawing.Size)(resources.GetObject("panelTop.Size")));
			this.panelTop.TabIndex = ((int)(resources.GetObject("panelTop.TabIndex")));
			this.panelTop.Text = resources.GetString("panelTop.Text");
			this.panelTop.Visible = ((bool)(resources.GetObject("panelTop.Visible")));
			// 
			// LoginControl
			// 
			this.LoginControl.AccessibleDescription = resources.GetString("LoginControl.AccessibleDescription");
			this.LoginControl.AccessibleName = resources.GetString("LoginControl.AccessibleName");
			this.LoginControl.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("LoginControl.Anchor")));
			this.LoginControl.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("LoginControl.BackgroundImage")));
			this.LoginControl.ContainingControl = this;
			this.LoginControl.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("LoginControl.Dock")));
			this.LoginControl.Enabled = ((bool)(resources.GetObject("LoginControl.Enabled")));
			this.LoginControl.Font = ((System.Drawing.Font)(resources.GetObject("LoginControl.Font")));
			this.LoginControl.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("LoginControl.ImeMode")));
			this.LoginControl.Location = ((System.Drawing.Point)(resources.GetObject("LoginControl.Location")));
			this.LoginControl.Name = "LoginControl";
			this.LoginControl.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("LoginControl.OcxState")));
			this.LoginControl.RightToLeft = ((bool)(resources.GetObject("LoginControl.RightToLeft")));
			this.LoginControl.Size = ((System.Drawing.Size)(resources.GetObject("LoginControl.Size")));
			this.LoginControl.TabIndex = ((int)(resources.GetObject("LoginControl.TabIndex")));
			this.LoginControl.Text = resources.GetString("LoginControl.Text");
			this.LoginControl.Visible = ((bool)(resources.GetObject("LoginControl.Visible")));
			// 
			// cbUTC
			// 
			this.cbUTC.AccessibleDescription = resources.GetString("cbUTC.AccessibleDescription");
			this.cbUTC.AccessibleName = resources.GetString("cbUTC.AccessibleName");
			this.cbUTC.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cbUTC.Anchor")));
			this.cbUTC.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("cbUTC.Appearance")));
			this.cbUTC.BackColor = System.Drawing.Color.Transparent;
			this.cbUTC.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbUTC.BackgroundImage")));
			this.cbUTC.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbUTC.CheckAlign")));
			this.cbUTC.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cbUTC.Dock")));
			this.cbUTC.Enabled = ((bool)(resources.GetObject("cbUTC.Enabled")));
			this.cbUTC.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("cbUTC.FlatStyle")));
			this.cbUTC.Font = ((System.Drawing.Font)(resources.GetObject("cbUTC.Font")));
			this.cbUTC.Image = ((System.Drawing.Image)(resources.GetObject("cbUTC.Image")));
			this.cbUTC.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbUTC.ImageAlign")));
			this.cbUTC.ImageIndex = ((int)(resources.GetObject("cbUTC.ImageIndex")));
			this.cbUTC.ImageList = this.imageList1;
			this.cbUTC.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cbUTC.ImeMode")));
			this.cbUTC.Location = ((System.Drawing.Point)(resources.GetObject("cbUTC.Location")));
			this.cbUTC.Name = "cbUTC";
			this.cbUTC.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cbUTC.RightToLeft")));
			this.cbUTC.Size = ((System.Drawing.Size)(resources.GetObject("cbUTC.Size")));
			this.cbUTC.TabIndex = ((int)(resources.GetObject("cbUTC.TabIndex")));
			this.cbUTC.Text = resources.GetString("cbUTC.Text");
			this.cbUTC.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbUTC.TextAlign")));
			this.cbUTC.Visible = ((bool)(resources.GetObject("cbUTC.Visible")));
			this.cbUTC.CheckedChanged += new System.EventHandler(this.cbUTC_CheckedChanged);
			// 
			// imageList1
			// 
			this.imageList1.ImageSize = ((System.Drawing.Size)(resources.GetObject("imageList1.ImageSize")));
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.White;
			// 
			// btnAbout
			// 
			this.btnAbout.AccessibleDescription = resources.GetString("btnAbout.AccessibleDescription");
			this.btnAbout.AccessibleName = resources.GetString("btnAbout.AccessibleName");
			this.btnAbout.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnAbout.Anchor")));
			this.btnAbout.BackColor = System.Drawing.Color.Transparent;
			this.btnAbout.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAbout.BackgroundImage")));
			this.btnAbout.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnAbout.Dock")));
			this.btnAbout.Enabled = ((bool)(resources.GetObject("btnAbout.Enabled")));
			this.btnAbout.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnAbout.FlatStyle")));
			this.btnAbout.Font = ((System.Drawing.Font)(resources.GetObject("btnAbout.Font")));
			this.btnAbout.Image = ((System.Drawing.Image)(resources.GetObject("btnAbout.Image")));
			this.btnAbout.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnAbout.ImageAlign")));
			this.btnAbout.ImageIndex = ((int)(resources.GetObject("btnAbout.ImageIndex")));
			this.btnAbout.ImageList = this.imageList1;
			this.btnAbout.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnAbout.ImeMode")));
			this.btnAbout.Location = ((System.Drawing.Point)(resources.GetObject("btnAbout.Location")));
			this.btnAbout.Name = "btnAbout";
			this.btnAbout.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnAbout.RightToLeft")));
			this.btnAbout.Size = ((System.Drawing.Size)(resources.GetObject("btnAbout.Size")));
			this.btnAbout.TabIndex = ((int)(resources.GetObject("btnAbout.TabIndex")));
			this.btnAbout.Text = resources.GetString("btnAbout.Text");
			this.btnAbout.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnAbout.TextAlign")));
			this.btnAbout.Visible = ((bool)(resources.GetObject("btnAbout.Visible")));
			this.btnAbout.Click += new System.EventHandler(this.btnAbout_Click);
			// 
			// cbRules
			// 
			this.cbRules.AccessibleDescription = resources.GetString("cbRules.AccessibleDescription");
			this.cbRules.AccessibleName = resources.GetString("cbRules.AccessibleName");
			this.cbRules.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cbRules.Anchor")));
			this.cbRules.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("cbRules.Appearance")));
			this.cbRules.BackColor = System.Drawing.Color.Transparent;
			this.cbRules.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbRules.BackgroundImage")));
			this.cbRules.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbRules.CheckAlign")));
			this.cbRules.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cbRules.Dock")));
			this.cbRules.Enabled = ((bool)(resources.GetObject("cbRules.Enabled")));
			this.cbRules.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("cbRules.FlatStyle")));
			this.cbRules.Font = ((System.Drawing.Font)(resources.GetObject("cbRules.Font")));
			this.cbRules.Image = ((System.Drawing.Image)(resources.GetObject("cbRules.Image")));
			this.cbRules.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbRules.ImageAlign")));
			this.cbRules.ImageIndex = ((int)(resources.GetObject("cbRules.ImageIndex")));
			this.cbRules.ImageList = this.imageList1;
			this.cbRules.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cbRules.ImeMode")));
			this.cbRules.Location = ((System.Drawing.Point)(resources.GetObject("cbRules.Location")));
			this.cbRules.Name = "cbRules";
			this.cbRules.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cbRules.RightToLeft")));
			this.cbRules.Size = ((System.Drawing.Size)(resources.GetObject("cbRules.Size")));
			this.cbRules.TabIndex = ((int)(resources.GetObject("cbRules.TabIndex")));
			this.cbRules.Text = resources.GetString("cbRules.Text");
			this.cbRules.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbRules.TextAlign")));
			this.cbRules.Visible = ((bool)(resources.GetObject("cbRules.Visible")));
			this.cbRules.CheckedChanged += new System.EventHandler(this.cbRules_CheckedChanged);
			// 
			// panel2
			// 
			this.panel2.AccessibleDescription = resources.GetString("panel2.AccessibleDescription");
			this.panel2.AccessibleName = resources.GetString("panel2.AccessibleName");
			this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("panel2.Anchor")));
			this.panel2.AutoScroll = ((bool)(resources.GetObject("panel2.AutoScroll")));
			this.panel2.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("panel2.AutoScrollMargin")));
			this.panel2.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("panel2.AutoScrollMinSize")));
			this.panel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel2.BackgroundImage")));
			this.panel2.Controls.Add(this.bcBlinker1);
			this.panel2.Controls.Add(this.lblLocalTime);
			this.panel2.Controls.Add(this.btnDeleteViewPoint);
			this.panel2.Controls.Add(this.lblUTCTime);
			this.panel2.Controls.Add(this.btnOpenVIewPoint);
			this.panel2.Controls.Add(this.cbViewPoints);
			this.panel2.Controls.Add(this.btnNewViewPoint);
			this.panel2.Controls.Add(this.btnEditViewPoint);
			this.panel2.Controls.Add(this.panel5);
			this.panel2.Controls.Add(this.pictureBox2);
			this.panel2.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("panel2.Dock")));
			this.panel2.Enabled = ((bool)(resources.GetObject("panel2.Enabled")));
			this.panel2.Font = ((System.Drawing.Font)(resources.GetObject("panel2.Font")));
			this.panel2.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("panel2.ImeMode")));
			this.panel2.Location = ((System.Drawing.Point)(resources.GetObject("panel2.Location")));
			this.panel2.Name = "panel2";
			this.panel2.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("panel2.RightToLeft")));
			this.panel2.Size = ((System.Drawing.Size)(resources.GetObject("panel2.Size")));
			this.panel2.TabIndex = ((int)(resources.GetObject("panel2.TabIndex")));
			this.panel2.Text = resources.GetString("panel2.Text");
			this.panel2.Visible = ((bool)(resources.GetObject("panel2.Visible")));
			// 
			// bcBlinker1
			// 
			this.bcBlinker1.AccessibleDescription = resources.GetString("bcBlinker1.AccessibleDescription");
			this.bcBlinker1.AccessibleName = resources.GetString("bcBlinker1.AccessibleName");
			this.bcBlinker1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("bcBlinker1.Anchor")));
			this.bcBlinker1.AutoScroll = ((bool)(resources.GetObject("bcBlinker1.AutoScroll")));
			this.bcBlinker1.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("bcBlinker1.AutoScrollMargin")));
			this.bcBlinker1.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("bcBlinker1.AutoScrollMinSize")));
			this.bcBlinker1.BackColor = System.Drawing.SystemColors.Control;
			this.bcBlinker1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bcBlinker1.BackgroundImage")));
			this.bcBlinker1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("bcBlinker1.Dock")));
			this.bcBlinker1.Enabled = ((bool)(resources.GetObject("bcBlinker1.Enabled")));
			this.bcBlinker1.Font = ((System.Drawing.Font)(resources.GetObject("bcBlinker1.Font")));
			this.bcBlinker1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("bcBlinker1.ImeMode")));
			this.bcBlinker1.Location = ((System.Drawing.Point)(resources.GetObject("bcBlinker1.Location")));
			this.bcBlinker1.Name = "bcBlinker1";
			this.bcBlinker1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("bcBlinker1.RightToLeft")));
			this.bcBlinker1.Size = ((System.Drawing.Size)(resources.GetObject("bcBlinker1.Size")));
			this.bcBlinker1.TabIndex = ((int)(resources.GetObject("bcBlinker1.TabIndex")));
			this.bcBlinker1.Visible = ((bool)(resources.GetObject("bcBlinker1.Visible")));
			this.bcBlinker1.Paint += new System.Windows.Forms.PaintEventHandler(this.bcBlinker1_Paint);
			// 
			// lblLocalTime
			// 
			this.lblLocalTime.AccessibleDescription = resources.GetString("lblLocalTime.AccessibleDescription");
			this.lblLocalTime.AccessibleName = resources.GetString("lblLocalTime.AccessibleName");
			this.lblLocalTime.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblLocalTime.Anchor")));
			this.lblLocalTime.AutoSize = ((bool)(resources.GetObject("lblLocalTime.AutoSize")));
			this.lblLocalTime.BackColor = System.Drawing.Color.Silver;
			this.lblLocalTime.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblLocalTime.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblLocalTime.Dock")));
			this.lblLocalTime.Enabled = ((bool)(resources.GetObject("lblLocalTime.Enabled")));
			this.lblLocalTime.Font = ((System.Drawing.Font)(resources.GetObject("lblLocalTime.Font")));
			this.lblLocalTime.ForeColor = System.Drawing.Color.Black;
			this.lblLocalTime.Image = ((System.Drawing.Image)(resources.GetObject("lblLocalTime.Image")));
			this.lblLocalTime.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblLocalTime.ImageAlign")));
			this.lblLocalTime.ImageIndex = ((int)(resources.GetObject("lblLocalTime.ImageIndex")));
			this.lblLocalTime.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblLocalTime.ImeMode")));
			this.lblLocalTime.Location = ((System.Drawing.Point)(resources.GetObject("lblLocalTime.Location")));
			this.lblLocalTime.Name = "lblLocalTime";
			this.lblLocalTime.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblLocalTime.RightToLeft")));
			this.lblLocalTime.Size = ((System.Drawing.Size)(resources.GetObject("lblLocalTime.Size")));
			this.lblLocalTime.TabIndex = ((int)(resources.GetObject("lblLocalTime.TabIndex")));
			this.lblLocalTime.Text = resources.GetString("lblLocalTime.Text");
			this.lblLocalTime.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblLocalTime.TextAlign")));
			this.lblLocalTime.Visible = ((bool)(resources.GetObject("lblLocalTime.Visible")));
			// 
			// btnDeleteViewPoint
			// 
			this.btnDeleteViewPoint.AccessibleDescription = resources.GetString("btnDeleteViewPoint.AccessibleDescription");
			this.btnDeleteViewPoint.AccessibleName = resources.GetString("btnDeleteViewPoint.AccessibleName");
			this.btnDeleteViewPoint.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnDeleteViewPoint.Anchor")));
			this.btnDeleteViewPoint.BackColor = System.Drawing.Color.Transparent;
			this.btnDeleteViewPoint.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDeleteViewPoint.BackgroundImage")));
			this.btnDeleteViewPoint.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnDeleteViewPoint.Dock")));
			this.btnDeleteViewPoint.Enabled = ((bool)(resources.GetObject("btnDeleteViewPoint.Enabled")));
			this.btnDeleteViewPoint.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnDeleteViewPoint.FlatStyle")));
			this.btnDeleteViewPoint.Font = ((System.Drawing.Font)(resources.GetObject("btnDeleteViewPoint.Font")));
			this.btnDeleteViewPoint.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteViewPoint.Image")));
			this.btnDeleteViewPoint.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnDeleteViewPoint.ImageAlign")));
			this.btnDeleteViewPoint.ImageIndex = ((int)(resources.GetObject("btnDeleteViewPoint.ImageIndex")));
			this.btnDeleteViewPoint.ImageList = this.imageList1;
			this.btnDeleteViewPoint.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnDeleteViewPoint.ImeMode")));
			this.btnDeleteViewPoint.Location = ((System.Drawing.Point)(resources.GetObject("btnDeleteViewPoint.Location")));
			this.btnDeleteViewPoint.Name = "btnDeleteViewPoint";
			this.btnDeleteViewPoint.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnDeleteViewPoint.RightToLeft")));
			this.btnDeleteViewPoint.Size = ((System.Drawing.Size)(resources.GetObject("btnDeleteViewPoint.Size")));
			this.btnDeleteViewPoint.TabIndex = ((int)(resources.GetObject("btnDeleteViewPoint.TabIndex")));
			this.btnDeleteViewPoint.Text = resources.GetString("btnDeleteViewPoint.Text");
			this.btnDeleteViewPoint.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnDeleteViewPoint.TextAlign")));
			this.btnDeleteViewPoint.Visible = ((bool)(resources.GetObject("btnDeleteViewPoint.Visible")));
			this.btnDeleteViewPoint.Click += new System.EventHandler(this.btnDeleteViewPoint_Click);
			// 
			// lblUTCTime
			// 
			this.lblUTCTime.AccessibleDescription = resources.GetString("lblUTCTime.AccessibleDescription");
			this.lblUTCTime.AccessibleName = resources.GetString("lblUTCTime.AccessibleName");
			this.lblUTCTime.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblUTCTime.Anchor")));
			this.lblUTCTime.AutoSize = ((bool)(resources.GetObject("lblUTCTime.AutoSize")));
			this.lblUTCTime.BackColor = System.Drawing.Color.Silver;
			this.lblUTCTime.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblUTCTime.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblUTCTime.Dock")));
			this.lblUTCTime.Enabled = ((bool)(resources.GetObject("lblUTCTime.Enabled")));
			this.lblUTCTime.Font = ((System.Drawing.Font)(resources.GetObject("lblUTCTime.Font")));
			this.lblUTCTime.ForeColor = System.Drawing.Color.Red;
			this.lblUTCTime.Image = ((System.Drawing.Image)(resources.GetObject("lblUTCTime.Image")));
			this.lblUTCTime.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblUTCTime.ImageAlign")));
			this.lblUTCTime.ImageIndex = ((int)(resources.GetObject("lblUTCTime.ImageIndex")));
			this.lblUTCTime.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblUTCTime.ImeMode")));
			this.lblUTCTime.Location = ((System.Drawing.Point)(resources.GetObject("lblUTCTime.Location")));
			this.lblUTCTime.Name = "lblUTCTime";
			this.lblUTCTime.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblUTCTime.RightToLeft")));
			this.lblUTCTime.Size = ((System.Drawing.Size)(resources.GetObject("lblUTCTime.Size")));
			this.lblUTCTime.TabIndex = ((int)(resources.GetObject("lblUTCTime.TabIndex")));
			this.lblUTCTime.Text = resources.GetString("lblUTCTime.Text");
			this.lblUTCTime.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblUTCTime.TextAlign")));
			this.lblUTCTime.Visible = ((bool)(resources.GetObject("lblUTCTime.Visible")));
			// 
			// btnOpenVIewPoint
			// 
			this.btnOpenVIewPoint.AccessibleDescription = resources.GetString("btnOpenVIewPoint.AccessibleDescription");
			this.btnOpenVIewPoint.AccessibleName = resources.GetString("btnOpenVIewPoint.AccessibleName");
			this.btnOpenVIewPoint.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnOpenVIewPoint.Anchor")));
			this.btnOpenVIewPoint.BackColor = System.Drawing.Color.Transparent;
			this.btnOpenVIewPoint.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnOpenVIewPoint.BackgroundImage")));
			this.btnOpenVIewPoint.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnOpenVIewPoint.Dock")));
			this.btnOpenVIewPoint.Enabled = ((bool)(resources.GetObject("btnOpenVIewPoint.Enabled")));
			this.btnOpenVIewPoint.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnOpenVIewPoint.FlatStyle")));
			this.btnOpenVIewPoint.Font = ((System.Drawing.Font)(resources.GetObject("btnOpenVIewPoint.Font")));
			this.btnOpenVIewPoint.Image = ((System.Drawing.Image)(resources.GetObject("btnOpenVIewPoint.Image")));
			this.btnOpenVIewPoint.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnOpenVIewPoint.ImageAlign")));
			this.btnOpenVIewPoint.ImageIndex = ((int)(resources.GetObject("btnOpenVIewPoint.ImageIndex")));
			this.btnOpenVIewPoint.ImageList = this.imageList1;
			this.btnOpenVIewPoint.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnOpenVIewPoint.ImeMode")));
			this.btnOpenVIewPoint.Location = ((System.Drawing.Point)(resources.GetObject("btnOpenVIewPoint.Location")));
			this.btnOpenVIewPoint.Name = "btnOpenVIewPoint";
			this.btnOpenVIewPoint.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnOpenVIewPoint.RightToLeft")));
			this.btnOpenVIewPoint.Size = ((System.Drawing.Size)(resources.GetObject("btnOpenVIewPoint.Size")));
			this.btnOpenVIewPoint.TabIndex = ((int)(resources.GetObject("btnOpenVIewPoint.TabIndex")));
			this.btnOpenVIewPoint.Text = resources.GetString("btnOpenVIewPoint.Text");
			this.btnOpenVIewPoint.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnOpenVIewPoint.TextAlign")));
			this.btnOpenVIewPoint.Visible = ((bool)(resources.GetObject("btnOpenVIewPoint.Visible")));
			this.btnOpenVIewPoint.Click += new System.EventHandler(this.btnOpenVIewPoint_Click);
			// 
			// cbViewPoints
			// 
			this.cbViewPoints.AccessibleDescription = resources.GetString("cbViewPoints.AccessibleDescription");
			this.cbViewPoints.AccessibleName = resources.GetString("cbViewPoints.AccessibleName");
			this.cbViewPoints.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cbViewPoints.Anchor")));
			this.cbViewPoints.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbViewPoints.BackgroundImage")));
			this.cbViewPoints.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cbViewPoints.Dock")));
			this.cbViewPoints.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbViewPoints.Enabled = ((bool)(resources.GetObject("cbViewPoints.Enabled")));
			this.cbViewPoints.Font = ((System.Drawing.Font)(resources.GetObject("cbViewPoints.Font")));
			this.cbViewPoints.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cbViewPoints.ImeMode")));
			this.cbViewPoints.IntegralHeight = ((bool)(resources.GetObject("cbViewPoints.IntegralHeight")));
			this.cbViewPoints.ItemHeight = ((int)(resources.GetObject("cbViewPoints.ItemHeight")));
			this.cbViewPoints.Location = ((System.Drawing.Point)(resources.GetObject("cbViewPoints.Location")));
			this.cbViewPoints.MaxDropDownItems = ((int)(resources.GetObject("cbViewPoints.MaxDropDownItems")));
			this.cbViewPoints.MaxLength = ((int)(resources.GetObject("cbViewPoints.MaxLength")));
			this.cbViewPoints.Name = "cbViewPoints";
			this.cbViewPoints.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cbViewPoints.RightToLeft")));
			this.cbViewPoints.Size = ((System.Drawing.Size)(resources.GetObject("cbViewPoints.Size")));
			this.cbViewPoints.Sorted = true;
			this.cbViewPoints.TabIndex = ((int)(resources.GetObject("cbViewPoints.TabIndex")));
			this.cbViewPoints.Text = resources.GetString("cbViewPoints.Text");
			this.cbViewPoints.Visible = ((bool)(resources.GetObject("cbViewPoints.Visible")));
			// 
			// btnNewViewPoint
			// 
			this.btnNewViewPoint.AccessibleDescription = resources.GetString("btnNewViewPoint.AccessibleDescription");
			this.btnNewViewPoint.AccessibleName = resources.GetString("btnNewViewPoint.AccessibleName");
			this.btnNewViewPoint.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnNewViewPoint.Anchor")));
			this.btnNewViewPoint.BackColor = System.Drawing.Color.Transparent;
			this.btnNewViewPoint.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNewViewPoint.BackgroundImage")));
			this.btnNewViewPoint.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnNewViewPoint.Dock")));
			this.btnNewViewPoint.Enabled = ((bool)(resources.GetObject("btnNewViewPoint.Enabled")));
			this.btnNewViewPoint.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnNewViewPoint.FlatStyle")));
			this.btnNewViewPoint.Font = ((System.Drawing.Font)(resources.GetObject("btnNewViewPoint.Font")));
			this.btnNewViewPoint.Image = ((System.Drawing.Image)(resources.GetObject("btnNewViewPoint.Image")));
			this.btnNewViewPoint.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnNewViewPoint.ImageAlign")));
			this.btnNewViewPoint.ImageIndex = ((int)(resources.GetObject("btnNewViewPoint.ImageIndex")));
			this.btnNewViewPoint.ImageList = this.imageList1;
			this.btnNewViewPoint.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnNewViewPoint.ImeMode")));
			this.btnNewViewPoint.Location = ((System.Drawing.Point)(resources.GetObject("btnNewViewPoint.Location")));
			this.btnNewViewPoint.Name = "btnNewViewPoint";
			this.btnNewViewPoint.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnNewViewPoint.RightToLeft")));
			this.btnNewViewPoint.Size = ((System.Drawing.Size)(resources.GetObject("btnNewViewPoint.Size")));
			this.btnNewViewPoint.TabIndex = ((int)(resources.GetObject("btnNewViewPoint.TabIndex")));
			this.btnNewViewPoint.Text = resources.GetString("btnNewViewPoint.Text");
			this.btnNewViewPoint.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnNewViewPoint.TextAlign")));
			this.btnNewViewPoint.Visible = ((bool)(resources.GetObject("btnNewViewPoint.Visible")));
			this.btnNewViewPoint.Click += new System.EventHandler(this.btnNewViewPoint_Click);
			// 
			// btnEditViewPoint
			// 
			this.btnEditViewPoint.AccessibleDescription = resources.GetString("btnEditViewPoint.AccessibleDescription");
			this.btnEditViewPoint.AccessibleName = resources.GetString("btnEditViewPoint.AccessibleName");
			this.btnEditViewPoint.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnEditViewPoint.Anchor")));
			this.btnEditViewPoint.BackColor = System.Drawing.Color.Transparent;
			this.btnEditViewPoint.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnEditViewPoint.BackgroundImage")));
			this.btnEditViewPoint.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnEditViewPoint.Dock")));
			this.btnEditViewPoint.Enabled = ((bool)(resources.GetObject("btnEditViewPoint.Enabled")));
			this.btnEditViewPoint.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnEditViewPoint.FlatStyle")));
			this.btnEditViewPoint.Font = ((System.Drawing.Font)(resources.GetObject("btnEditViewPoint.Font")));
			this.btnEditViewPoint.Image = ((System.Drawing.Image)(resources.GetObject("btnEditViewPoint.Image")));
			this.btnEditViewPoint.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnEditViewPoint.ImageAlign")));
			this.btnEditViewPoint.ImageIndex = ((int)(resources.GetObject("btnEditViewPoint.ImageIndex")));
			this.btnEditViewPoint.ImageList = this.imageList1;
			this.btnEditViewPoint.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnEditViewPoint.ImeMode")));
			this.btnEditViewPoint.Location = ((System.Drawing.Point)(resources.GetObject("btnEditViewPoint.Location")));
			this.btnEditViewPoint.Name = "btnEditViewPoint";
			this.btnEditViewPoint.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnEditViewPoint.RightToLeft")));
			this.btnEditViewPoint.Size = ((System.Drawing.Size)(resources.GetObject("btnEditViewPoint.Size")));
			this.btnEditViewPoint.TabIndex = ((int)(resources.GetObject("btnEditViewPoint.TabIndex")));
			this.btnEditViewPoint.Text = resources.GetString("btnEditViewPoint.Text");
			this.btnEditViewPoint.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnEditViewPoint.TextAlign")));
			this.btnEditViewPoint.Visible = ((bool)(resources.GetObject("btnEditViewPoint.Visible")));
			this.btnEditViewPoint.Click += new System.EventHandler(this.btnEditViewPoint_Click);
			// 
			// panel5
			// 
			this.panel5.AccessibleDescription = resources.GetString("panel5.AccessibleDescription");
			this.panel5.AccessibleName = resources.GetString("panel5.AccessibleName");
			this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("panel5.Anchor")));
			this.panel5.AutoScroll = ((bool)(resources.GetObject("panel5.AutoScroll")));
			this.panel5.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("panel5.AutoScrollMargin")));
			this.panel5.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("panel5.AutoScrollMinSize")));
			this.panel5.BackColor = System.Drawing.Color.Black;
			this.panel5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel5.BackgroundImage")));
			this.panel5.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("panel5.Dock")));
			this.panel5.Enabled = ((bool)(resources.GetObject("panel5.Enabled")));
			this.panel5.Font = ((System.Drawing.Font)(resources.GetObject("panel5.Font")));
			this.panel5.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("panel5.ImeMode")));
			this.panel5.Location = ((System.Drawing.Point)(resources.GetObject("panel5.Location")));
			this.panel5.Name = "panel5";
			this.panel5.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("panel5.RightToLeft")));
			this.panel5.Size = ((System.Drawing.Size)(resources.GetObject("panel5.Size")));
			this.panel5.TabIndex = ((int)(resources.GetObject("panel5.TabIndex")));
			this.panel5.Text = resources.GetString("panel5.Text");
			this.panel5.Visible = ((bool)(resources.GetObject("panel5.Visible")));
			// 
			// pictureBox2
			// 
			this.pictureBox2.AccessibleDescription = resources.GetString("pictureBox2.AccessibleDescription");
			this.pictureBox2.AccessibleName = resources.GetString("pictureBox2.AccessibleName");
			this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("pictureBox2.Anchor")));
			this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
			this.pictureBox2.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("pictureBox2.Dock")));
			this.pictureBox2.Enabled = ((bool)(resources.GetObject("pictureBox2.Enabled")));
			this.pictureBox2.Font = ((System.Drawing.Font)(resources.GetObject("pictureBox2.Font")));
			this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
			this.pictureBox2.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("pictureBox2.ImeMode")));
			this.pictureBox2.Location = ((System.Drawing.Point)(resources.GetObject("pictureBox2.Location")));
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("pictureBox2.RightToLeft")));
			this.pictureBox2.Size = ((System.Drawing.Size)(resources.GetObject("pictureBox2.Size")));
			this.pictureBox2.SizeMode = ((System.Windows.Forms.PictureBoxSizeMode)(resources.GetObject("pictureBox2.SizeMode")));
			this.pictureBox2.TabIndex = ((int)(resources.GetObject("pictureBox2.TabIndex")));
			this.pictureBox2.TabStop = false;
			this.pictureBox2.Text = resources.GetString("pictureBox2.Text");
			this.pictureBox2.Visible = ((bool)(resources.GetObject("pictureBox2.Visible")));
			this.pictureBox2.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox2_Paint);
			// 
			// btnDebug
			// 
			this.btnDebug.AccessibleDescription = resources.GetString("btnDebug.AccessibleDescription");
			this.btnDebug.AccessibleName = resources.GetString("btnDebug.AccessibleName");
			this.btnDebug.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnDebug.Anchor")));
			this.btnDebug.BackColor = System.Drawing.Color.Transparent;
			this.btnDebug.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDebug.BackgroundImage")));
			this.btnDebug.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnDebug.Dock")));
			this.btnDebug.Enabled = ((bool)(resources.GetObject("btnDebug.Enabled")));
			this.btnDebug.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnDebug.FlatStyle")));
			this.btnDebug.Font = ((System.Drawing.Font)(resources.GetObject("btnDebug.Font")));
			this.btnDebug.Image = ((System.Drawing.Image)(resources.GetObject("btnDebug.Image")));
			this.btnDebug.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnDebug.ImageAlign")));
			this.btnDebug.ImageIndex = ((int)(resources.GetObject("btnDebug.ImageIndex")));
			this.btnDebug.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnDebug.ImeMode")));
			this.btnDebug.Location = ((System.Drawing.Point)(resources.GetObject("btnDebug.Location")));
			this.btnDebug.Name = "btnDebug";
			this.btnDebug.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnDebug.RightToLeft")));
			this.btnDebug.Size = ((System.Drawing.Size)(resources.GetObject("btnDebug.Size")));
			this.btnDebug.TabIndex = ((int)(resources.GetObject("btnDebug.TabIndex")));
			this.btnDebug.Text = resources.GetString("btnDebug.Text");
			this.btnDebug.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnDebug.TextAlign")));
			this.btnDebug.Visible = ((bool)(resources.GetObject("btnDebug.Visible")));
			this.btnDebug.Click += new System.EventHandler(this.btnDebug_Click);
			// 
			// lblAboveText
			// 
			this.lblAboveText.AccessibleDescription = resources.GetString("lblAboveText.AccessibleDescription");
			this.lblAboveText.AccessibleName = resources.GetString("lblAboveText.AccessibleName");
			this.lblAboveText.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblAboveText.Anchor")));
			this.lblAboveText.AutoSize = ((bool)(resources.GetObject("lblAboveText.AutoSize")));
			this.lblAboveText.BackColor = System.Drawing.Color.Transparent;
			this.lblAboveText.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblAboveText.Dock")));
			this.lblAboveText.Enabled = ((bool)(resources.GetObject("lblAboveText.Enabled")));
			this.lblAboveText.Font = ((System.Drawing.Font)(resources.GetObject("lblAboveText.Font")));
			this.lblAboveText.ForeColor = System.Drawing.Color.Aqua;
			this.lblAboveText.Image = ((System.Drawing.Image)(resources.GetObject("lblAboveText.Image")));
			this.lblAboveText.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblAboveText.ImageAlign")));
			this.lblAboveText.ImageIndex = ((int)(resources.GetObject("lblAboveText.ImageIndex")));
			this.lblAboveText.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblAboveText.ImeMode")));
			this.lblAboveText.Location = ((System.Drawing.Point)(resources.GetObject("lblAboveText.Location")));
			this.lblAboveText.Name = "lblAboveText";
			this.lblAboveText.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblAboveText.RightToLeft")));
			this.lblAboveText.Size = ((System.Drawing.Size)(resources.GetObject("lblAboveText.Size")));
			this.lblAboveText.TabIndex = ((int)(resources.GetObject("lblAboveText.TabIndex")));
			this.lblAboveText.Text = resources.GetString("lblAboveText.Text");
			this.lblAboveText.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblAboveText.TextAlign")));
			this.lblAboveText.Visible = ((bool)(resources.GetObject("lblAboveText.Visible")));
			// 
			// cbOpen
			// 
			this.cbOpen.AccessibleDescription = resources.GetString("cbOpen.AccessibleDescription");
			this.cbOpen.AccessibleName = resources.GetString("cbOpen.AccessibleName");
			this.cbOpen.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cbOpen.Anchor")));
			this.cbOpen.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("cbOpen.Appearance")));
			this.cbOpen.BackColor = System.Drawing.Color.Transparent;
			this.cbOpen.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbOpen.BackgroundImage")));
			this.cbOpen.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbOpen.CheckAlign")));
			this.cbOpen.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cbOpen.Dock")));
			this.cbOpen.Enabled = ((bool)(resources.GetObject("cbOpen.Enabled")));
			this.cbOpen.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("cbOpen.FlatStyle")));
			this.cbOpen.Font = ((System.Drawing.Font)(resources.GetObject("cbOpen.Font")));
			this.cbOpen.Image = ((System.Drawing.Image)(resources.GetObject("cbOpen.Image")));
			this.cbOpen.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbOpen.ImageAlign")));
			this.cbOpen.ImageIndex = ((int)(resources.GetObject("cbOpen.ImageIndex")));
			this.cbOpen.ImageList = this.imageList1;
			this.cbOpen.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cbOpen.ImeMode")));
			this.cbOpen.Location = ((System.Drawing.Point)(resources.GetObject("cbOpen.Location")));
			this.cbOpen.Name = "cbOpen";
			this.cbOpen.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cbOpen.RightToLeft")));
			this.cbOpen.Size = ((System.Drawing.Size)(resources.GetObject("cbOpen.Size")));
			this.cbOpen.TabIndex = ((int)(resources.GetObject("cbOpen.TabIndex")));
			this.cbOpen.Text = resources.GetString("cbOpen.Text");
			this.cbOpen.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbOpen.TextAlign")));
			this.cbOpen.Visible = ((bool)(resources.GetObject("cbOpen.Visible")));
			this.cbOpen.CheckedChanged += new System.EventHandler(this.cbOpen_CheckedChanged);
			// 
			// cbReorgGantt
			// 
			this.cbReorgGantt.AccessibleDescription = resources.GetString("cbReorgGantt.AccessibleDescription");
			this.cbReorgGantt.AccessibleName = resources.GetString("cbReorgGantt.AccessibleName");
			this.cbReorgGantt.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cbReorgGantt.Anchor")));
			this.cbReorgGantt.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("cbReorgGantt.Appearance")));
			this.cbReorgGantt.BackColor = System.Drawing.Color.Transparent;
			this.cbReorgGantt.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbReorgGantt.BackgroundImage")));
			this.cbReorgGantt.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbReorgGantt.CheckAlign")));
			this.cbReorgGantt.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cbReorgGantt.Dock")));
			this.cbReorgGantt.Enabled = ((bool)(resources.GetObject("cbReorgGantt.Enabled")));
			this.cbReorgGantt.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("cbReorgGantt.FlatStyle")));
			this.cbReorgGantt.Font = ((System.Drawing.Font)(resources.GetObject("cbReorgGantt.Font")));
			this.cbReorgGantt.Image = ((System.Drawing.Image)(resources.GetObject("cbReorgGantt.Image")));
			this.cbReorgGantt.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbReorgGantt.ImageAlign")));
			this.cbReorgGantt.ImageIndex = ((int)(resources.GetObject("cbReorgGantt.ImageIndex")));
			this.cbReorgGantt.ImageList = this.imageList1;
			this.cbReorgGantt.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cbReorgGantt.ImeMode")));
			this.cbReorgGantt.Location = ((System.Drawing.Point)(resources.GetObject("cbReorgGantt.Location")));
			this.cbReorgGantt.Name = "cbReorgGantt";
			this.cbReorgGantt.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cbReorgGantt.RightToLeft")));
			this.cbReorgGantt.Size = ((System.Drawing.Size)(resources.GetObject("cbReorgGantt.Size")));
			this.cbReorgGantt.TabIndex = ((int)(resources.GetObject("cbReorgGantt.TabIndex")));
			this.cbReorgGantt.Text = resources.GetString("cbReorgGantt.Text");
			this.cbReorgGantt.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbReorgGantt.TextAlign")));
			this.cbReorgGantt.Visible = ((bool)(resources.GetObject("cbReorgGantt.Visible")));
			this.cbReorgGantt.CheckedChanged += new System.EventHandler(this.cbReorgGantt_CheckedChanged);
			// 
			// cbNow
			// 
			this.cbNow.AccessibleDescription = resources.GetString("cbNow.AccessibleDescription");
			this.cbNow.AccessibleName = resources.GetString("cbNow.AccessibleName");
			this.cbNow.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cbNow.Anchor")));
			this.cbNow.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("cbNow.Appearance")));
			this.cbNow.BackColor = System.Drawing.Color.Transparent;
			this.cbNow.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbNow.BackgroundImage")));
			this.cbNow.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbNow.CheckAlign")));
			this.cbNow.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cbNow.Dock")));
			this.cbNow.Enabled = ((bool)(resources.GetObject("cbNow.Enabled")));
			this.cbNow.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("cbNow.FlatStyle")));
			this.cbNow.Font = ((System.Drawing.Font)(resources.GetObject("cbNow.Font")));
			this.cbNow.Image = ((System.Drawing.Image)(resources.GetObject("cbNow.Image")));
			this.cbNow.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbNow.ImageAlign")));
			this.cbNow.ImageIndex = ((int)(resources.GetObject("cbNow.ImageIndex")));
			this.cbNow.ImageList = this.imageList1;
			this.cbNow.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cbNow.ImeMode")));
			this.cbNow.Location = ((System.Drawing.Point)(resources.GetObject("cbNow.Location")));
			this.cbNow.Name = "cbNow";
			this.cbNow.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cbNow.RightToLeft")));
			this.cbNow.Size = ((System.Drawing.Size)(resources.GetObject("cbNow.Size")));
			this.cbNow.TabIndex = ((int)(resources.GetObject("cbNow.TabIndex")));
			this.cbNow.Text = resources.GetString("cbNow.Text");
			this.cbNow.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbNow.TextAlign")));
			this.cbNow.Visible = ((bool)(resources.GetObject("cbNow.Visible")));
			this.cbNow.CheckedChanged += new System.EventHandler(this.cbNow_CheckedChanged);
			// 
			// cbSetup
			// 
			this.cbSetup.AccessibleDescription = resources.GetString("cbSetup.AccessibleDescription");
			this.cbSetup.AccessibleName = resources.GetString("cbSetup.AccessibleName");
			this.cbSetup.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cbSetup.Anchor")));
			this.cbSetup.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("cbSetup.Appearance")));
			this.cbSetup.BackColor = System.Drawing.Color.Transparent;
			this.cbSetup.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbSetup.BackgroundImage")));
			this.cbSetup.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbSetup.CheckAlign")));
			this.cbSetup.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cbSetup.Dock")));
			this.cbSetup.Enabled = ((bool)(resources.GetObject("cbSetup.Enabled")));
			this.cbSetup.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("cbSetup.FlatStyle")));
			this.cbSetup.Font = ((System.Drawing.Font)(resources.GetObject("cbSetup.Font")));
			this.cbSetup.Image = ((System.Drawing.Image)(resources.GetObject("cbSetup.Image")));
			this.cbSetup.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbSetup.ImageAlign")));
			this.cbSetup.ImageIndex = ((int)(resources.GetObject("cbSetup.ImageIndex")));
			this.cbSetup.ImageList = this.imageList1;
			this.cbSetup.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cbSetup.ImeMode")));
			this.cbSetup.Location = ((System.Drawing.Point)(resources.GetObject("cbSetup.Location")));
			this.cbSetup.Name = "cbSetup";
			this.cbSetup.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cbSetup.RightToLeft")));
			this.cbSetup.Size = ((System.Drawing.Size)(resources.GetObject("cbSetup.Size")));
			this.cbSetup.TabIndex = ((int)(resources.GetObject("cbSetup.TabIndex")));
			this.cbSetup.Text = resources.GetString("cbSetup.Text");
			this.cbSetup.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbSetup.TextAlign")));
			this.cbSetup.Visible = ((bool)(resources.GetObject("cbSetup.Visible")));
			// 
			// cbSearch
			// 
			this.cbSearch.AccessibleDescription = resources.GetString("cbSearch.AccessibleDescription");
			this.cbSearch.AccessibleName = resources.GetString("cbSearch.AccessibleName");
			this.cbSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cbSearch.Anchor")));
			this.cbSearch.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("cbSearch.Appearance")));
			this.cbSearch.BackColor = System.Drawing.Color.Transparent;
			this.cbSearch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbSearch.BackgroundImage")));
			this.cbSearch.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbSearch.CheckAlign")));
			this.cbSearch.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cbSearch.Dock")));
			this.cbSearch.Enabled = ((bool)(resources.GetObject("cbSearch.Enabled")));
			this.cbSearch.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("cbSearch.FlatStyle")));
			this.cbSearch.Font = ((System.Drawing.Font)(resources.GetObject("cbSearch.Font")));
			this.cbSearch.Image = ((System.Drawing.Image)(resources.GetObject("cbSearch.Image")));
			this.cbSearch.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbSearch.ImageAlign")));
			this.cbSearch.ImageIndex = ((int)(resources.GetObject("cbSearch.ImageIndex")));
			this.cbSearch.ImageList = this.imageList1;
			this.cbSearch.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cbSearch.ImeMode")));
			this.cbSearch.Location = ((System.Drawing.Point)(resources.GetObject("cbSearch.Location")));
			this.cbSearch.Name = "cbSearch";
			this.cbSearch.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cbSearch.RightToLeft")));
			this.cbSearch.Size = ((System.Drawing.Size)(resources.GetObject("cbSearch.Size")));
			this.cbSearch.TabIndex = ((int)(resources.GetObject("cbSearch.TabIndex")));
			this.cbSearch.Text = resources.GetString("cbSearch.Text");
			this.cbSearch.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbSearch.TextAlign")));
			this.cbSearch.Visible = ((bool)(resources.GetObject("cbSearch.Visible")));
			this.cbSearch.CheckedChanged += new System.EventHandler(this.cbSearch_CheckedChanged);
			// 
			// cbView
			// 
			this.cbView.AccessibleDescription = resources.GetString("cbView.AccessibleDescription");
			this.cbView.AccessibleName = resources.GetString("cbView.AccessibleName");
			this.cbView.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cbView.Anchor")));
			this.cbView.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("cbView.Appearance")));
			this.cbView.BackColor = System.Drawing.Color.Transparent;
			this.cbView.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbView.BackgroundImage")));
			this.cbView.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbView.CheckAlign")));
			this.cbView.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cbView.Dock")));
			this.cbView.Enabled = ((bool)(resources.GetObject("cbView.Enabled")));
			this.cbView.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("cbView.FlatStyle")));
			this.cbView.Font = ((System.Drawing.Font)(resources.GetObject("cbView.Font")));
			this.cbView.Image = ((System.Drawing.Image)(resources.GetObject("cbView.Image")));
			this.cbView.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbView.ImageAlign")));
			this.cbView.ImageIndex = ((int)(resources.GetObject("cbView.ImageIndex")));
			this.cbView.ImageList = this.imageList1;
			this.cbView.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cbView.ImeMode")));
			this.cbView.Location = ((System.Drawing.Point)(resources.GetObject("cbView.Location")));
			this.cbView.Name = "cbView";
			this.cbView.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cbView.RightToLeft")));
			this.cbView.Size = ((System.Drawing.Size)(resources.GetObject("cbView.Size")));
			this.cbView.TabIndex = ((int)(resources.GetObject("cbView.TabIndex")));
			this.cbView.Text = resources.GetString("cbView.Text");
			this.cbView.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbView.TextAlign")));
			this.cbView.Visible = ((bool)(resources.GetObject("cbView.Visible")));
			this.cbView.CheckedChanged += new System.EventHandler(this.cbView_CheckedChanged);
			// 
			// cbLoad
			// 
			this.cbLoad.AccessibleDescription = resources.GetString("cbLoad.AccessibleDescription");
			this.cbLoad.AccessibleName = resources.GetString("cbLoad.AccessibleName");
			this.cbLoad.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cbLoad.Anchor")));
			this.cbLoad.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("cbLoad.Appearance")));
			this.cbLoad.BackColor = System.Drawing.Color.Transparent;
			this.cbLoad.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbLoad.BackgroundImage")));
			this.cbLoad.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbLoad.CheckAlign")));
			this.cbLoad.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cbLoad.Dock")));
			this.cbLoad.Enabled = ((bool)(resources.GetObject("cbLoad.Enabled")));
			this.cbLoad.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("cbLoad.FlatStyle")));
			this.cbLoad.Font = ((System.Drawing.Font)(resources.GetObject("cbLoad.Font")));
			this.cbLoad.Image = ((System.Drawing.Image)(resources.GetObject("cbLoad.Image")));
			this.cbLoad.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbLoad.ImageAlign")));
			this.cbLoad.ImageIndex = ((int)(resources.GetObject("cbLoad.ImageIndex")));
			this.cbLoad.ImageList = this.imageList1;
			this.cbLoad.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cbLoad.ImeMode")));
			this.cbLoad.Location = ((System.Drawing.Point)(resources.GetObject("cbLoad.Location")));
			this.cbLoad.Name = "cbLoad";
			this.cbLoad.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cbLoad.RightToLeft")));
			this.cbLoad.Size = ((System.Drawing.Size)(resources.GetObject("cbLoad.Size")));
			this.cbLoad.TabIndex = ((int)(resources.GetObject("cbLoad.TabIndex")));
			this.cbLoad.Text = resources.GetString("cbLoad.Text");
			this.cbLoad.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbLoad.TextAlign")));
			this.cbLoad.Visible = ((bool)(resources.GetObject("cbLoad.Visible")));
			this.cbLoad.CheckedChanged += new System.EventHandler(this.cbLoad_CheckedChanged);
			// 
			// pictureBox1
			// 
			this.pictureBox1.AccessibleDescription = resources.GetString("pictureBox1.AccessibleDescription");
			this.pictureBox1.AccessibleName = resources.GetString("pictureBox1.AccessibleName");
			this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("pictureBox1.Anchor")));
			this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
			this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
			this.pictureBox1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("pictureBox1.Dock")));
			this.pictureBox1.Enabled = ((bool)(resources.GetObject("pictureBox1.Enabled")));
			this.pictureBox1.Font = ((System.Drawing.Font)(resources.GetObject("pictureBox1.Font")));
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("pictureBox1.ImeMode")));
			this.pictureBox1.Location = ((System.Drawing.Point)(resources.GetObject("pictureBox1.Location")));
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("pictureBox1.RightToLeft")));
			this.pictureBox1.Size = ((System.Drawing.Size)(resources.GetObject("pictureBox1.Size")));
			this.pictureBox1.SizeMode = ((System.Windows.Forms.PictureBoxSizeMode)(resources.GetObject("pictureBox1.SizeMode")));
			this.pictureBox1.TabIndex = ((int)(resources.GetObject("pictureBox1.TabIndex")));
			this.pictureBox1.TabStop = false;
			this.pictureBox1.Text = resources.GetString("pictureBox1.Text");
			this.pictureBox1.Visible = ((bool)(resources.GetObject("pictureBox1.Visible")));
			this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
			// 
			// panel1
			// 
			this.panel1.AccessibleDescription = resources.GetString("panel1.AccessibleDescription");
			this.panel1.AccessibleName = resources.GetString("panel1.AccessibleName");
			this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("panel1.Anchor")));
			this.panel1.AutoScroll = ((bool)(resources.GetObject("panel1.AutoScroll")));
			this.panel1.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("panel1.AutoScrollMargin")));
			this.panel1.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("panel1.AutoScrollMinSize")));
			this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
			this.panel1.Controls.Add(this.txtCalc);
			this.panel1.Controls.Add(this.myGantt);
			this.panel1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("panel1.Dock")));
			this.panel1.Enabled = ((bool)(resources.GetObject("panel1.Enabled")));
			this.panel1.Font = ((System.Drawing.Font)(resources.GetObject("panel1.Font")));
			this.panel1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("panel1.ImeMode")));
			this.panel1.Location = ((System.Drawing.Point)(resources.GetObject("panel1.Location")));
			this.panel1.Name = "panel1";
			this.panel1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("panel1.RightToLeft")));
			this.panel1.Size = ((System.Drawing.Size)(resources.GetObject("panel1.Size")));
			this.panel1.TabIndex = ((int)(resources.GetObject("panel1.TabIndex")));
			this.panel1.Text = resources.GetString("panel1.Text");
			this.panel1.Visible = ((bool)(resources.GetObject("panel1.Visible")));
			// 
			// txtCalc
			// 
			this.txtCalc.AccessibleDescription = resources.GetString("txtCalc.AccessibleDescription");
			this.txtCalc.AccessibleName = resources.GetString("txtCalc.AccessibleName");
			this.txtCalc.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtCalc.Anchor")));
			this.txtCalc.AutoSize = ((bool)(resources.GetObject("txtCalc.AutoSize")));
			this.txtCalc.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(224)), ((System.Byte)(224)), ((System.Byte)(224)));
			this.txtCalc.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.txtCalc.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtCalc.Dock")));
			this.txtCalc.Enabled = ((bool)(resources.GetObject("txtCalc.Enabled")));
			this.txtCalc.Font = ((System.Drawing.Font)(resources.GetObject("txtCalc.Font")));
			this.txtCalc.ForeColor = System.Drawing.Color.Blue;
			this.txtCalc.Image = ((System.Drawing.Image)(resources.GetObject("txtCalc.Image")));
			this.txtCalc.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("txtCalc.ImageAlign")));
			this.txtCalc.ImageIndex = ((int)(resources.GetObject("txtCalc.ImageIndex")));
			this.txtCalc.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtCalc.ImeMode")));
			this.txtCalc.Location = ((System.Drawing.Point)(resources.GetObject("txtCalc.Location")));
			this.txtCalc.Name = "txtCalc";
			this.txtCalc.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtCalc.RightToLeft")));
			this.txtCalc.Size = ((System.Drawing.Size)(resources.GetObject("txtCalc.Size")));
			this.txtCalc.TabIndex = ((int)(resources.GetObject("txtCalc.TabIndex")));
			this.txtCalc.Text = resources.GetString("txtCalc.Text");
			this.txtCalc.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("txtCalc.TextAlign")));
			this.txtCalc.Visible = ((bool)(resources.GetObject("txtCalc.Visible")));
			// 
			// myGantt
			// 
			this.myGantt.AccessibleDescription = resources.GetString("myGantt.AccessibleDescription");
			this.myGantt.AccessibleName = resources.GetString("myGantt.AccessibleName");
			this.myGantt.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("myGantt.Anchor")));
			this.myGantt.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("myGantt.BackgroundImage")));
			this.myGantt.ContainingControl = this;
			this.myGantt.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("myGantt.Dock")));
			this.myGantt.Font = ((System.Drawing.Font)(resources.GetObject("myGantt.Font")));
			this.myGantt.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("myGantt.ImeMode")));
			this.myGantt.Location = ((System.Drawing.Point)(resources.GetObject("myGantt.Location")));
			this.myGantt.Name = "myGantt";
			this.myGantt.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("myGantt.OcxState")));
			this.myGantt.RightToLeft = ((bool)(resources.GetObject("myGantt.RightToLeft")));
			this.myGantt.Size = ((System.Drawing.Size)(resources.GetObject("myGantt.Size")));
			this.myGantt.TabIndex = ((int)(resources.GetObject("myGantt.TabIndex")));
			this.myGantt.Text = resources.GetString("myGantt.Text");
			this.myGantt.Visible = ((bool)(resources.GetObject("myGantt.Visible")));
			this.myGantt.OnLButtonDblClkBar += new AxUGANTTLib._DUGanttEvents_OnLButtonDblClkBarEventHandler(this.myGantt_OnLButtonDblClkBar);
			this.myGantt.OnLButtonDownBar += new AxUGANTTLib._DUGanttEvents_OnLButtonDownBarEventHandler(this.myGantt_OnLButtonDownBar);
			this.myGantt.ActualLineNo += new AxUGANTTLib._DUGanttEvents_ActualLineNoEventHandler(this.myGantt_ActualLineNo);
			this.myGantt.ActualTimescaleDate += new AxUGANTTLib._DUGanttEvents_ActualTimescaleDateEventHandler(this.myGantt_ActualTimescaleDate);
			// 
			// timer1
			// 
			this.timer1.Enabled = true;
			this.timer1.Interval = 60000;
			this.timer1.SynchronizingObject = this;
			this.timer1.Elapsed += new System.Timers.ElapsedEventHandler(this.timer1_Elapsed);
			// 
			// timerConflictCheck
			// 
			this.timerConflictCheck.Enabled = true;
			this.timerConflictCheck.Interval = 60000;
			this.timerConflictCheck.SynchronizingObject = this;
			this.timerConflictCheck.Elapsed += new System.Timers.ElapsedEventHandler(this.timerConflictCheck_Elapsed);
			// 
			// timerCloseStartup
			// 
			this.timerCloseStartup.Interval = 300;
			this.timerCloseStartup.SynchronizingObject = this;
			this.timerCloseStartup.Elapsed += new System.Timers.ElapsedEventHandler(this.timerCloseStartup_Elapsed);
			// 
			// timerOpacity
			// 
			this.timerOpacity.Interval = 75;
			this.timerOpacity.SynchronizingObject = this;
			// 
			// frmMain
			// 
			this.AccessibleDescription = resources.GetString("$this.AccessibleDescription");
			this.AccessibleName = resources.GetString("$this.AccessibleName");
			this.AutoScaleBaseSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScaleBaseSize")));
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMargin")));
			this.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMinSize")));
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = ((System.Drawing.Size)(resources.GetObject("$this.ClientSize")));
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.panelTop);
			this.Enabled = ((bool)(resources.GetObject("$this.Enabled")));
			this.Font = ((System.Drawing.Font)(resources.GetObject("$this.Font")));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.MaximumSize = ((System.Drawing.Size)(resources.GetObject("$this.MaximumSize")));
			this.MinimumSize = ((System.Drawing.Size)(resources.GetObject("$this.MinimumSize")));
			this.Name = "frmMain";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.StartPosition = ((System.Windows.Forms.FormStartPosition)(resources.GetObject("$this.StartPosition")));
			this.Text = resources.GetString("$this.Text");
			this.Resize += new System.EventHandler(this.frmMain_Resize);
			this.Closing += new System.ComponentModel.CancelEventHandler(this.frmMain_Closing);
			this.Load += new System.EventHandler(this.frmMain_Load);
			this.Closed += new System.EventHandler(this.frmMain_Closed);
			this.panelTop.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.LoginControl)).EndInit();
			this.panel2.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.myGantt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.timer1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.timerConflictCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.timerCloseStartup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.timerOpacity)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string [] args) 
		{
			System.Diagnostics.Process pThis = System.Diagnostics.Process.GetCurrentProcess();

			foreach(System.Diagnostics.Process p in System.Diagnostics.Process.GetProcesses())
			{
				if(p.Id != pThis.Id && p.ProcessName == pThis.ProcessName)
				{
					MessageBox.Show("The Status Manager is already running and cannot be started twice!");

					return;
				}
			}
			frmMain olDlg = new frmMain();
			olDlg.args = args;
			Application.Run(olDlg);
		}
	
		#region Paint Metal Effect
		private void pictureBox1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			if(timerOpacity.Enabled == false)
				UT.DrawGradient(e.Graphics, 90f, pictureBox1, Color.WhiteSmoke , Color.DarkGray  );
		}
		private void pictureBox2_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			if(timerOpacity.Enabled == false)
				UT.DrawGradient(e.Graphics, 90f, pictureBox2, Color.WhiteSmoke , Color.DarkGray  );
		}


		#endregion Paint Metal Effect

		private void BcEvent(string pReqId, string pDest1, string pDest2,string pCmd, string pObject, string pSeq,string pTws, string pTwe, string pSelection, string pFields, string pData, string pBcNum)
		{
			bcBlinker1.Blink();
		}
		private void frmMain_Resize(object sender, System.EventArgs e)
		{
		}

		private void frmMain_Load(object sender, System.EventArgs e)
		{
			CFC.LoginControl = LoginControl;

			myDB = UT.GetMemDB();
			bool result = myDB.Connect(UT.ServerName,UT.UserName,UT.Hopo,"TAB","STATMGR");

			delegateBirdViewSaved = new DE.BirdViewSaved(OnBirdViewSaved);
			DE.OnBirdViewSaved += delegateBirdViewSaved;

			Ufis.Utils.IniFile ini = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			string strHiddenDataShow  = ini.IniReadValue("GLOBAL", "HIDDENDATA");

			timerConflictCheck.Interval = CFC.TimerIntervalConflicts * 6 * 10000;
			string strTimeFields = "STOA,STOD,TIFA,TIFD,TMOA,ETAI,ETDI,ONBL,OFBL,PABS,PAES,PDBS,PDES,PDBA,PDEA";
			arrAftTimeFields = strTimeFields.Split(',');
			System.Windows.Forms.DialogResult olResult;
			if(args.Length == 0) 
			{
				if(LoginProcedure() == false)
				{
					Application.Exit();
					return;
				}
			}
			else
			{
				if(args[0] != "/CONNECTED")
				{
					if(LoginProcedure() == false)
					{
						Application.Exit();
						return;
					}
				}	
			}

			HandleToolbar_BDPS_SEC();
			string theKey = "Software\\StatusManager";
			RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			if(rk == null)
			{
				rk = Registry.CurrentUser.OpenSubKey("Software", true);
				theKey = "StatusManager";
				rk.CreateSubKey(theKey);
				rk.Close();
			}
	
			string strUsr = LoginControl.GetUserName();
			if(strUsr == "")
				strUsr="Default";
			UT.UserName = LoginControl.GetUserName();//"Default";
			DataForm = new frmData();
			//MWO: DataForm.Show() is necessary to ensure, that all tabs are available !!!!!
			DataForm.Show();

			if(args.Length == 0 || strHiddenDataShow != "TRUE")
			{
				DataForm.Hide();
			}
			else if(args[0] != "/CONNECTED")
			{
				DataForm.Hide();
			} 
			else
			{
				DataForm.Show();
			}
			
			StartForm = new frmStartup();
			StartForm.Show();
			frmLoad olLoad = new frmLoad();
			this.lblAboveText.BackColor = Color.Transparent;
			//olLoad.Parent = StartForm;
			olResult = olLoad.ShowDialog();
			if (olResult == DialogResult.OK)
			{
				UT.TimeFrameFrom = olLoad.datFrom.Value;
				UT.TimeFrameTo = olLoad.datTo.Value;
				UT.TimeFrameFrom = UT.TimeFrameFrom.Subtract(new TimeSpan(0, 12, 0, 0));
				UT.TimeFrameTo = UT.TimeFrameTo.AddHours(12);

				DataForm.LoadData();

				MyInit();
				DE.InitDBObjects(bcBlinker1);

				StartForm.txtCurrentStatus.Text = "Allocating flights to gantt-chart";
				StartForm.txtCurrentStatus.Refresh();
				StartForm.txtDoneStatus.Refresh();
				AllocateFlights();
				StartForm.BringToFront();
				this.Text = "Status Manager: Timeframe [" + UT.TimeFrameFrom.ToString() + "] - [" + 
					UT.TimeFrameTo.ToString() + "]" + " --- Server: " + UT.ServerName + 
					" --- Active View: [" + CFC.CurrentViewName + "]";
			}
			else
			{
				Application.Exit();
			}
			lblAboveText.Parent = pictureBox1;
			cbLoad.Parent = pictureBox1;
			cbNow.Parent = pictureBox1;
			cbOpen.Parent = pictureBox1;
			cbReorgGantt.Parent = pictureBox1;
			cbSearch.Parent = pictureBox1;
			cbSetup.Parent = pictureBox1;
			cbView.Parent = pictureBox1;
			cbUTC.Parent = pictureBox1;
			cbUTC.Checked = true;
			cbRules.Parent = pictureBox1;
			btnAbout.Parent = pictureBox1;
			btnDebug.Parent = pictureBox1;
			btnEditViewPoint.Parent = pictureBox2;
			btnNewViewPoint.Parent = pictureBox2;
			btnOpenVIewPoint.Parent = pictureBox2;
			btnDeleteViewPoint.Parent = pictureBox2;
			lblUTCTime.Parent = pictureBox2;
			lblLocalTime.Parent = pictureBox2;
			ReloadCombo();
			this.Top = 0;
			this.Left = 0;
			this.Width = Screen.PrimaryScreen.WorkingArea.Width;
			DateTime localTime = DateTime.Now;
			DateTime utcTime = CFC.GetUTC();
			TimeSpan olTS = localTime - utcTime;
			lblUTCTime.Text = utcTime.ToShortDateString() + " " + utcTime.ToShortTimeString() + "z";
			lblLocalTime.Text = localTime.ToShortDateString() + " " + localTime.ToShortTimeString();
			//myGantt.UTCOffsetInHours = (short)-olTS.TotalHours;
			string strUTCConvertion = "";
			IniFile myIni = new IniFile("C:\\Ufis\\system\\ceda.ini");
			strUTCConvertion = myIni.IniReadValue("GLOBAL", "UTCCONVERTIONS");
			if(strUTCConvertion == "NO")
			{
				myGantt.UTCOffsetInHours = 0;
			}
			else
			{
				myGantt.UTCOffsetInHours = (short)-olTS.TotalHours;
			}
			if(UT.IsTimeInUtc == true )
			{
				TimeSpan olSpan = new TimeSpan(0, 1, 0, 0, 0);
				localTime = utcTime.Subtract(olSpan);
			}
			if(localTime < UT.TimeFrameFrom || localTime > UT.TimeFrameTo)
			{
				myGantt.ScrollTo(UT.TimeFrameFrom);
			}
			else
			{
				myGantt.ScrollTo(localTime);
			}
			ReadRegistry();

			isInit = true;
			timerCloseStartup.Enabled = true;

			// if this application does not include BirdView 
			// functionality => switch it of and hide the buttons
			if(bmWithBirdView == false)
			{
				btnNewViewPoint.Hide();
				btnEditViewPoint.Hide();
				btnDeleteViewPoint.Hide();
				cbViewPoints.Hide();
				btnOpenVIewPoint.Hide();
				panel5.Hide();
			}
			ScrollToNow();
		}
		private void HandleToolbar_BDPS_SEC()
		{
			string prv = LoginControl.GetPrivileges("Load");
			if(prv == "-" || prv == "0")
			{
				cbLoad.Enabled = false;
			}
			prv = LoginControl.GetPrivileges("View");
			if(prv == "-" || prv == "0")
			{
				cbView.Enabled = false;
			}
			prv = LoginControl.GetPrivileges("Find");
			if(prv == "-" || prv == "0")
			{
				cbSearch.Enabled = false;
			}
			prv = LoginControl.GetPrivileges("Rules");
			if(prv == "-" || prv == "0")
			{
				cbRules.Enabled = false;
			}
			prv = LoginControl.GetPrivileges("UTC");
			if(prv == "-" || prv == "0")
			{
				cbUTC.Enabled = false;
			}
			prv = LoginControl.GetPrivileges("Overview");
			if(prv == "-" || prv == "0")
			{
				cbOpen.Enabled = false;
			}
			prv = LoginControl.GetPrivileges("NewViewpoint");
			if(prv == "-" || prv == "0")
			{
				btnNewViewPoint.Enabled = false;
			}
			prv = LoginControl.GetPrivileges("EditViewpoint");
			if(prv == "-" || prv == "0")
			{
				btnEditViewPoint.Enabled = false;
			}
			prv = LoginControl.GetPrivileges("DeleteViewpoint");
			if(prv == "-" || prv == "0")
			{
				btnDeleteViewPoint.Enabled = false;
			}
		}
		private bool LoginProcedure()
		{
			string LoginAnsw = "";
			string tmpRegStrg = "";
			string strRet = "";
			//LoginControl.UfisComCtrl = UT.GetUfisCom();
			LoginControl.ApplicationName = "StatusManager";
			LoginControl.InfoCaption = "StatusManager";
			LoginControl.VersionString = Application.ProductVersion;// "4.5.0.1"; //GetApplVersion(True);
			LoginControl.InfoCaption = "Info about Status Manager";
			LoginControl.InfoButtonVisible = true;
			LoginControl.InfoUfisVersion = "Ufis Version 4.5";
			LoginControl.InfoAppVersion = "Status Manager " + Application.ProductVersion.ToString();
			LoginControl.InfoAAT = "UFIS Airport Solutions GmbH";
			LoginControl.UserNameLCase = true;
			tmpRegStrg = "StatusManager" + ",";
			//FKTTAB:  SUBD,FUNC,FUAL,TYPE,STAT
			tmpRegStrg += "InitModu,InitModu,Initialisieren (InitModu),B,-";
			tmpRegStrg += ",Load,Load,Reload Data,B,-";
			tmpRegStrg += ",View,View,View Definition,B,-";
			tmpRegStrg += ",Find,Find,Find Flight(s),B,-";
			tmpRegStrg += ",Rules,Rules,Edit Rules,B,-";
			tmpRegStrg += ",UTC,UTC,Switch Local/UTC,B,-";
			tmpRegStrg += ",Overview,Overview,Open Status-Overview,B,-";
			tmpRegStrg += ",NewViewpoint,NewViewpoint,New Viewpoint,B,-";
			tmpRegStrg += ",EditViewpoint,EditViewpoint,Edit Viewpoint,B,-";
			tmpRegStrg += ",DeleteViewpoint,DeleteViewpoint,Delete Viewpoint,B,-";
			tmpRegStrg += ",EditDetailStatus,EditDetailStatus,Edit Status Information,B,-";
			tmpRegStrg += ",CheckListProcessing,CheckListProcessing,Call Check List Processing,B,-";
			tmpRegStrg += ",CheckListProcessingSave,CheckListProcessingSave,Save,B,-";
			tmpRegStrg += ",CheckListProcessingEdit,CheckListProcessingEdit,Edit Basic Data,B,-";
			tmpRegStrg += ",CheckListProcessingPrint,CheckListProcessingPrint,Print,B,-";
			tmpRegStrg += ",CheckListProcessingTimeEdit,CheckListProcessingTimeEdit,Time Edit,B,-";

			//Append any other Privileges
			//tmpRegStrg = tmpRegStrg & ",Entry,Entry,Entry,F,1"
			//tmpRegStrg = tmpRegStrg & ",Feature,Feature,Feature,F,1"
			LoginControl.RegisterApplicationString = tmpRegStrg;
			strRet = LoginControl.ShowLoginDialog();

			//string strLoginRet = LoginControl.DoLoginSilentMode(usr, pwd);
			//strLoginRet must be "OK"
			LoginAnsw = LoginControl.GetPrivileges("InitModu");
			UT.UserName = LoginControl.GetUserName();
			if( LoginAnsw != "" && strRet != "CANCEL" )
				return true; 
			else  
				return false;
		}

		private void ReadRegistry()
		{
			int myX=-1, myY=-1, myW=-1, myH=-1;
			string regVal = "";
			string theKey = "Software\\StatusManager\\MainWindow";
			RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			if(rk != null)
			{
				regVal = rk.GetValue("X", "-1").ToString();
				myX = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Y", "-1").ToString();
				myY = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Width", "-1").ToString();
				myW = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Height", "-1").ToString();
				myH = Convert.ToInt32(regVal);
			}
			if((myW != -1 && myH != -1) && (myX < Screen.PrimaryScreen.Bounds.Width))
			{
				this.Left = myX;
				this.Top = myY;
				this.Width = myW;
				this.Height = myH;
			}
		}
		/// <summary>
		/// Assigns the flights to the gantt chart with maximized density as possible
		/// to ensure, that as much flight bars as possible are visible to get max
		/// overview
		/// </summary>
		public void AllocateFlights()
		{
			int i=0;
			DateTime d;
			DateTime datTT;
		    
			InitGantt();
			txtCalc.Visible = true;
			txtCalc.BringToFront();
			txtCalc.Text = "Please wait while calculating ...";
			txtCalc.Refresh();
			myGantt.TimeScaleDuration = 12;
		    
			myGantt.ScrollTo(UT.TimeFrameFrom); 
			datTT = UT.TimeFrameTo; 
			datTT = datTT.AddDays(1000); 
			for( i = 0; i <=  200; i++)
			{	
				int ilText;
				ilText = i + 1;
				myGantt.AddLineAt(i, i.ToString(), ilText.ToString());
				myGantt.SetLineSeparator(i, 1, UT.colBlack, 2, 1, 0);
			}
			if( UT.IsTimeInUtc == true)
				myGantt.UTCOffsetInHours = (short)-UT.UTCOffset;
			else
				myGantt.UTCOffsetInHours = 0;
			this.Cursor = Cursors.WaitCursor;

			Allocator myAllocator = new Allocator(200, myGantt.TimeFrameFrom, datTT);
	//THE ALLOCATION
			myAFT.Sort("TIFA,TIFD", true);
			for( i = 0; i < myAFT.Count; i++)
			{
				DateTime barStart = DateTime.Now;
				DateTime barEnd = DateTime.Now;
				if(myAFT[i]["ADID"] == "A") 
				{
					barStart = UT.CedaFullDateToDateTime(myAFT[i]["TIFA"]); 
					barEnd =  barStart.AddMinutes((double)imArrivalBarLen); 
				}
				else if(myAFT[i]["ADID"] == "D")
				{
					barEnd = UT.CedaFullDateToDateTime(myAFT[i]["TIFD"]);
					barStart = barEnd.Subtract(new TimeSpan(0, 0, imDepartureBarLen, 0, 0));
				}
				string strF = myAFT[i]["FLNO"];
				if(strF == "LH 3381" || strF == "MDF7704" || strF == "A3 530")
				{
					strF = "";
				}
				int gLine = myAllocator.GetLineForTimeframe(barStart, barEnd);
				if(gLine > -1)
				{
					UpdateBarForLine(myAFT[i]["URNO"], gLine, false, false);
				} 
				else
				{
					//MessageBox.Show(myAFT[i]["FLNO"]);
				}
				if(i % 100 == 0)
				{
					TimeSpan olSpan2 = new TimeSpan(0, 2, 0, 0, 0);
					d = barStart.Subtract(olSpan2); 
					myGantt.ScrollTo(d);
					lblAboveText.Text = d.ToShortDateString();
					lblAboveText.Refresh();
					myGantt.Refresh();
				}
			}
			txtCalc.Visible = false;
			myAllocator.Clear();
	//END ALLOCATION
			d = DateTime.Now;
			DateTime datUTC = CFC.GetUTC();
			TimeSpan olTS = d - datUTC;
			if(UT.IsTimeInUtc == true && UT.UTCOffset != 0)
			{
				TimeSpan olSpan = new TimeSpan(0, 1, 0, 0, 0);
				d = datUTC.Subtract(olSpan);
			}
			else
			{
				TimeSpan olSpan = new TimeSpan(0, 1, 0, 0, 0);
				d = d.Subtract(olSpan);
			}
			if(d < UT.TimeFrameFrom || d > UT.TimeFrameTo)
				myGantt.ScrollTo(UT.TimeFrameFrom);
			else
				myGantt.ScrollTo(d);
			lblAboveText.Text = d.ToShortDateString();
			lblAboveText.Refresh();
			myGantt.TimeScaleDuration = 4;

			myGantt.Refresh();
			this.Cursor = Cursors.Arrow;
		}//END public void AllocateFlights()
		/// <summary>
		/// Reloads the combobox for Viewpoint Definitions
		/// </summary>
		public void ReloadCombo()
		{
			cbViewPoints.Items.Clear();
			for(int i = 0; i < DataForm.tabVPD.GetLineCount(); i++)
			{
				cbViewPoints.Items.Add(DataForm.tabVPD.GetFieldValue(i, "VIEW"));
			}
		}

		/// <summary>
		/// Identifies the color for the bar's triangle for the
		/// flight's tisa field
		/// </summary>
		/// <param name="strTisa"></param>
		/// <returns>the color as int</returns>
		public int GetTisaColor(String strTisa)
		{										 
			int ret = 0;

			switch(strTisa)
			{
				case "S":
					ret = UT.colGray;//colGray
					break;
				case "E":
					ret = UT.colWhite;//colWhite
					break;
				case "T":
					ret = UT.colYellow;//colYellow
					break;
				case "L":
					ret = UT.colDarkGreen;//colGreen
					break;
				case "O":
					ret = UT.colGreen;//colLime
					break;
				default:
					break;
			}
			return ret;
		}
		/// <summary>
		/// Identifies the color for the bar's triangle for the
		/// flight's tisd field
		/// </summary>
		/// <param name="strTisa"></param>
		/// <returns>the color as int</returns>
		public int GetTisdColor(String strTisd)
		{
			int ret = 0;
			switch(strTisd)
			{
				case "O":
					ret = UT.colGreen;
					break;
				case "A":
					ret = UT.colDarkGreen;
					break;
				case "S":
					ret = UT.colGray; 
					break;
				case "E":
					ret = UT.colWhite;
					break;
				default:
					break;
			}
			return ret;
		}

		private void cbReorgGantt_CheckedChanged(object sender, System.EventArgs e)
		{
			if(cbReorgGantt.Checked == true)
			{
				cbReorgGantt.Checked = false;
				AllocateFlights();
			}
		}

		private void btnDebug_Click(object sender, System.EventArgs e)
		{
			frmExceptions olDlg = new frmExceptions();
			olDlg.ShowDialog(this);
			if(UT.arrExceptions.Count == 0)
			{
				btnDebug.BackColor = Control.DefaultBackColor;
				String strText = "debug(" + UT.arrExceptions.Count.ToString() + ")";
				btnDebug.Text = strText;
				btnDebug.Refresh();
			}
		}

		private void OnNewExeption(int opCnt)
		{
			String strText;
			strText = "debug(" + UT.arrExceptions.Count.ToString() + ")";
			btnDebug.Text = strText;
			btnDebug.BackColor = Color.Red;
			btnDebug.Refresh();
		}

		private void frmStatusOverview_OnStatusOverviewClosed()
		{
			cbOpen.Checked = false;
		}

		private void frmRules_OnRuleEditorClosed()
		{
			cbRules.Checked = false;
		}
		private void frmDetailStatus_OnStatusDetailClosed()
		{
			myDetailDlg = null;
		}


		private void frmMain_Closed(object sender, System.EventArgs e)
		{
			//Remove the delegates from the delegate list!!!
			UT.OnExceptionOccured  -= delegateExceptionOccured;
			DE.OnRuleEditorClosed -= delegateRuleEditorClosed;
			DE.OnStatusOverviewClosed -= delegateStatusOverviewClosed;
			DE.OnStatusDetailClosed -= delegateStatusDetailClosed;
			DE.OnOpenDetailStatusDlg -= delegateOpenDetailStatusDlg;
			DE.OnBirdViewSaved -= delegateBirdViewSaved;
			DE.OnFindList_Closed -= delegateFindListClosed;
			DE.OnNavigateToFlight -= delegateNavigateToFlight;
			DE.OnUpdateFlightData -= delegateUpdateFlightData;
			DE.OnViewNameChanged -= delegateViewNameChanged;
			DE.OnUTC_LocalTimeChanged -= delegateUTC_LocalTimeChanged;
			DE.OnCLO_Message -= delegateCLO_Message;
			DE.OnReloadCompleteOnlineStatuses -= delegateReloadCompleteOnlineStatuses;

			myDB.Disconnect();
		}

		private void myGantt_ActualLineNo(object sender, AxUGANTTLib._DUGanttEvents_ActualLineNoEvent e)
		{
		
		}

		private void myGantt_ActualTimescaleDate(object sender, AxUGANTTLib._DUGanttEvents_ActualTimescaleDateEvent e)
		{
			lblAboveText.Text = e.timescaleDate.ToShortDateString();
			lblAboveText.Refresh();
		}

		private void btnEditViewPoint_Click(object sender, System.EventArgs e)
		{
			String strViewPoint = cbViewPoints.Text;
			if(strViewPoint != "")
			{
				frmViewPoint olV = new frmViewPoint(this, DataForm, "EDIT", strViewPoint);
				olV.Show();
			}
		
		}
		private void OnBirdViewSaved()
		{
			ReloadCombo();
		}

		private void btnNewViewPoint_Click(object sender, System.EventArgs e)
		{
			frmViewPoint olV = new frmViewPoint(this, DataForm, "NEW", "");
			olV.Show();
		}

		private void btnOpenVIewPoint_Click(object sender, System.EventArgs e)
		{
			String strViewPoint = cbViewPoints.Text;
			if(strViewPoint != "")
			{
				frmRunViewPoint olV = new frmRunViewPoint(this, DataForm, strViewPoint);
				olV.Show();
			}
		}

		private void frmMain_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			DataForm.Close();
			//Store current geometry in Registry
			string theKey = "Software\\StatusManager\\MainWindow";
			RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			if(rk == null)
			{
				rk = Registry.CurrentUser.OpenSubKey("Software\\StatusManager", true);
				rk.CreateSubKey("MainWindow");
				rk.Close();
				theKey = "Software\\StatusManager\\MainWindow";
				rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			}

			rk.SetValue("X", this.Left.ToString());
			rk.SetValue("Y", this.Top.ToString());
			rk.SetValue("Width", this.Width.ToString());
			rk.SetValue("Height", this.Height.ToString());

		}

		private void timer1_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			DateTime localTime = DateTime.Now;
			DateTime utcTime = CFC.GetUTC();
			lblUTCTime.Text = utcTime.ToShortDateString() + " " + utcTime.ToShortTimeString() + "z";
			lblUTCTime.Refresh();
			lblLocalTime.Text = localTime.ToShortDateString() + " " + localTime.ToShortTimeString();
			lblLocalTime.Refresh();
		}

		private void cbRules_CheckedChanged(object sender, System.EventArgs e)
		{
			if(cbRules.Checked == true)
			{
				cbRules.BackColor = Color.LightGreen;
				cbRules.Refresh();
				myRulesDialog = new frmRules();
				myRulesDialog.Owner = this;
				myRulesDialog.Show(); 
			}
			else
			{
				if(myRulesDialog != null)
				{
					myRulesDialog.Close();
				}
				cbRules.BackColor = Color.Transparent;
			}
		}

		private void btnAbout_Click(object sender, System.EventArgs e)
		{
			frmAbout dlg = new frmAbout();
			dlg.strAppName = "Status Manager";
			Assembly [] assemblies = AppDomain.CurrentDomain.GetAssemblies();
			foreach(Assembly assembly in assemblies)
			{
				if(assembly.GetName().Name == "Status_Manager")
				{
					dlg.strAppVersion = assembly.GetName().Version.ToString();
				}
			}
				
			dlg.ShowDialog(this);
		}

		private void cbOpen_CheckedChanged(object sender, System.EventArgs e)
		{
			if(cbOpen.Checked == true)
			{
				cbOpen.BackColor = Color.LightGreen;
				cbOpen.Refresh();
				myStatusOverview = new frmStatusOverview();
				myStatusOverview.Show(); 
			}
			else
			{
				if(myStatusOverview  != null)
				{
					myStatusOverview .Close();
				}
				cbOpen.BackColor = Color.Transparent;
				cbOpen.Refresh();
			}
		}

		private void cbUTC_CheckedChanged(object sender, System.EventArgs e)
		{
			HandleUTC();
		}

		private void HandleUTC()
		{
			ITable myTmpAFT = myDB["AFTTMP"];
			if(cbUTC.Checked == true)
			{
				cbUTC.BackColor = Color.LightGreen;
				cbUTC.Refresh();
				UT.IsTimeInUtc = true;
				if(isInit == true)
				{
					this.Cursor = Cursors.WaitCursor;
					myAFT.TimeFieldsCurrentlyInUtc = true;
					myTmpAFT.TimeFieldsCurrentlyInUtc = true;
					myOST.TimeFieldsCurrentlyInUtc = true;
					DE.Call_UTC_LocalTimeChanged(this);
					this.Cursor = Cursors.Arrow;
				}
			}
			else
			{
				cbUTC.BackColor = Color.Transparent;
				cbUTC.Refresh();
				UT.IsTimeInUtc = false;
				if(isInit == true)
				{
					this.Cursor = Cursors.WaitCursor;
					myAFT.TimeFieldsCurrentlyInUtc = false; 
					myTmpAFT.TimeFieldsCurrentlyInUtc = false;
					myOST.TimeFieldsCurrentlyInUtc = false;
					DE.Call_UTC_LocalTimeChanged(this);
					this.Cursor = Cursors.Arrow;
				}
			}
		}

		private void UTCToLocal()
		{
			myAFT = myDB["AFT"];
			string strTmp = "";
			DateTime dat;
			DateTime datLocal = DateTime.Now;
			DateTime datUTC = CFC.GetUTC();
			TimeSpan olTS = datLocal - datUTC;
			myGantt.UTCOffsetInHours = (short)-UT.UTCOffset;//(short)-olTS.TotalHours;
			//MessageBox.Show(olTS.TotalHours.ToString());
			for(int i = 0; i < myAFT.Count; i++)
			{
				for(int j = 0; j < arrAftTimeFields.Length; j++)
				{
					strTmp = myAFT[i][arrAftTimeFields[j]];
					if(strTmp != "")
					{
						dat = UT.CedaFullDateToDateTime(strTmp);
						dat = dat.AddHours(olTS.TotalHours);
						strTmp = UT.DateTimeToCeda(dat);
						myAFT[i][arrAftTimeFields[j]] = strTmp;
					}
				}
			}
			AllocateFlights();
		}
		private void LocalToUTC()
		{
			myAFT = myDB["AFT"];
			string strTmp = "";
			DateTime dat;
			DateTime datLocal = DateTime.Now;
			DateTime datUTC = CFC.GetUTC();
			TimeSpan olTS = datLocal - datUTC;
			myGantt.UTCOffsetInHours = 0;
			//MessageBox.Show(olTS.TotalHours.ToString());
			for(int i = 0; i < myAFT.Count; i++)
			{
				for(int j = 0; j < arrAftTimeFields.Length; j++)
				{
					strTmp = myAFT[i][arrAftTimeFields[j]];
					if(strTmp != "")
					{
						dat = UT.CedaFullDateToDateTime(strTmp);
						dat = dat.Subtract(olTS);
						strTmp = UT.DateTimeToCeda(dat);
						myAFT[i][arrAftTimeFields[j]] = strTmp;
					}
				}
			}
			AllocateFlights();
		}

		private void bcBlinker1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, bcBlinker1, Color.WhiteSmoke, Color.LightGray);
		}

		private void cbView_CheckedChanged(object sender, System.EventArgs e)
		{
			if(cbView.Checked == true)
			{
				cbView.BackColor = Color.LightGreen;
				cbView.Refresh();
				frmView Dlg = new frmView(DataForm.tabViews);
				Dlg.ShowDialog(this);
				this.Text = "Status Manager: Timeframe [" + UT.TimeFrameFrom.ToString() + "] - [" + 
					UT.TimeFrameTo.ToString() + "]" + " --- Server: " + UT.ServerName + 
					" --- Active View: [" + CFC.CurrentViewName + "]";
				cbView.Checked = false;
				cbView.BackColor = Color.Transparent;
			}
		}

		private void myGantt_OnLButtonDblClkBar(object sender, AxUGANTTLib._DUGanttEvents_OnLButtonDblClkBarEvent e)
		{
			string strUrno = e.key;
			myAFT = myDB["AFT"];
			IRow [] rows = myAFT.RowsByIndexValue("URNO", strUrno);
			if(rows.Length > 0)
			{
				OpenDetailStatusDlg(rows[0]["URNO"], rows[0]["RKEY"], "");
				DE.Call_NavigateToFlight(this, rows[0]["URNO"]);
			}
		}

		private void OpenDetailStatusDlg(string strAftURNO, string strAftRKEY, string strOstUrno)
		{
			if(myDetailDlg == null)
			{
				myDetailDlg = new frmDetailStatus(strAftURNO, strAftRKEY, strOstUrno);
				myDetailDlg.Show();
			}
			else
			{
				myDetailDlg.SetData(strAftURNO, strAftRKEY, strOstUrno);
			}
			myDetailDlg.BringToFront();
		}

		private void timerConflictCheck_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			DE.Call_ConflicTimerElapsed();
		}

		private void frmStatusOverview_OnOpenDetailStatusDlg(string strAftURNO, string strAftRkey, string strOstUrno)
		{
			//Called from delegate
			OpenDetailStatusDlg(strAftURNO, strAftRkey, strOstUrno);
		}
		private void UpdateBar(string strAftUrno, bool blSelectBar, bool blRefresh)
		{
			int LineNo = myGantt.GetLineNoByBarKey(strAftUrno);
			DateTime myFrom;
			DateTime myTo;
			String strBarKey = "";
			String strAdid = "";
			String strTisa = "";
			String strTisd = "";
			int interval=0;
			String strFlno  = "";
			int colBody = 0;
			int leftTriaColor = 0;
			String strStoa  = "";
			DateTime datStoa;
			int rightTriaColor;
			DateTime datTimeFrameFrom = UT.TimeFrameFrom;
			DateTime datTimeFrameTo = UT.TimeFrameTo;

			//For new bars set it to the first line ==> user decides when to reorg the gantt
			if(LineNo < 0) LineNo = 0; 
			if(UT.IsTimeInUtc == true)
			{
				myTo = CFC.GetUTC();
				myFrom = CFC.GetUTC();
			}
			else
			{
				myTo = DateTime.Now;
				myFrom = DateTime.Now;
			}

			leftTriaColor = -1;
			rightTriaColor = -1;
	
			//First we delete the bar, this is correct in order to update DFR as well
			myGantt.DeleteBarByKey(strAftUrno);

			IRow [] aftRows = myAFT.RowsByIndexValue("URNO", strAftUrno);
			if(aftRows.Length > 0)
			{
				myGantt.DeleteAllTimeMarkers();
				strBarKey = aftRows[0]["URNO"];
				strAdid = aftRows[0]["ADID"];
				strFlno = aftRows[0]["FLNO"];
		        
				if( strAdid == "A" )
				{
					interval = imArrivalBarLen; 
					myFrom = UT.CedaFullDateToDateTime(aftRows[0]["TIFA"]); 
					myTo =  myFrom.AddMinutes((double)interval); 
					strStoa = aftRows[0]["STOA"];
					colBody = colorArrivalBar; //Cyan
					if(strStoa != "")
					{
						datStoa = UT.CedaFullDateToDateTime(strStoa);
					}
					strTisa = aftRows[0]["TISA"];
					leftTriaColor = GetTisaColor(strTisa);
				}
				if(strAdid == "D")
				{
					interval = imDepartureBarLen;
					TimeSpan olSpan = new TimeSpan(0, 0, interval, 0, 0);
					myTo = UT.CedaFullDateToDateTime(aftRows[0]["TIFD"]);
					myFrom = myTo.Subtract(olSpan);
					colBody = colorDepartureBar; //colLightBlue
					strTisd = aftRows[0]["TISD"];
					rightTriaColor = GetTisdColor(strTisd);
				}
				int CflCount = 0;
				int AckCount = 0;
				int TotalCount = 0;
				int OSTCount = 0;
				CFC.HasFlightStatusConflicts(strBarKey, ref CflCount, ref AckCount, ref TotalCount, ref OSTCount);
				if(OSTCount > 0)
				{
					strFlno += " *";
				}
				if(blSelectBar == false)
				//if(strAftUrno != omCurrentSelectedUrno)
				{
					if(CflCount > 0)
					{
						colBody = UT.colRed;
					}
					else if(AckCount > 0)
					{
						colBody = UT.colOrange;
					}
				}
				else
				{
					colBody = UT.colPink;
					myGantt.SetTimeMarker(myFrom, UT.colYellow);
					myGantt.SetTimeMarker(myTo, UT.colYellow);
				}
				myGantt.AddBarToLine( LineNo, strBarKey, myFrom, myTo, 
					strFlno, 1,
					UT.colCyan, UT.colBlack, UT.colRed, 
					UT.colBlack, UT.colBlack, 
					"", "", UT.colBlack, UT.colBlack);
				myGantt.SetBarColor(strBarKey, colBody);
				string strTriaID = leftTriaColor.ToString() + rightTriaColor.ToString();
				myGantt.SetBarTriangles(strBarKey, strTriaID, leftTriaColor, rightTriaColor, true);
				if(blRefresh == true)
				{
					myGantt.Refresh();
				}
			}
		}

		public void UpdateBarForLine(string strAftUrno, int LineNo, bool blSelectBar, bool blRefresh)
		{
			//int LineNo = myGantt.GetLineNoByBarKey(strAftUrno);
			DateTime myFrom;
			DateTime myTo;
			String strBarKey = "";
			String strAdid = "";
			String strTisa = "";
			String strTisd = "";
			int interval=0;
			String strFlno  = "";
			int colBody = 0;
			int leftTriaColor = 0;
			String strStoa  = "";
			DateTime datStoa;
			int rightTriaColor;
			DateTime datTimeFrameFrom = UT.TimeFrameFrom;
			DateTime datTimeFrameTo = UT.TimeFrameTo;

			//For new bars set it to the first line ==> user decides when to reorg the gantt
			if(LineNo < 0) LineNo = 0; 
			if(UT.IsTimeInUtc == true)
			{
				myTo = CFC.GetUTC();
				myFrom = CFC.GetUTC();
			}
			else
			{
				myTo = DateTime.Now;
				myFrom = DateTime.Now;
			}

			leftTriaColor = -1;
			rightTriaColor = -1;
	
			//First we delete the bar, this is correct in order to update DFR as well
			myGantt.DeleteBarByKey(strAftUrno);

			IRow [] aftRows = myAFT.RowsByIndexValue("URNO", strAftUrno);
			if(aftRows.Length > 0)
			{
				myGantt.DeleteAllTimeMarkers();
				strBarKey = aftRows[0]["URNO"];
				strAdid = aftRows[0]["ADID"];
				strFlno = aftRows[0]["FLNO"];
		        
				if( strAdid == "A" )
				{
					interval = imArrivalBarLen; 
					myFrom = UT.CedaFullDateToDateTime(aftRows[0]["TIFA"]); 
					myTo =  myFrom.AddMinutes((double)interval); 
					strStoa = aftRows[0]["STOA"];
					colBody = colorArrivalBar; //Cyan
					if(strStoa != "")
					{
						datStoa = UT.CedaFullDateToDateTime(strStoa);
					}
					strTisa = aftRows[0]["TISA"];
					leftTriaColor = GetTisaColor(strTisa);
				}
				if(strAdid == "D")
				{
					interval = imDepartureBarLen;
					TimeSpan olSpan = new TimeSpan(0, 0, interval, 0, 0);
					myTo = UT.CedaFullDateToDateTime(aftRows[0]["TIFD"]);
					myFrom = myTo.Subtract(olSpan);
					colBody = colorDepartureBar; //colLightBlue
					strTisd = aftRows[0]["TISD"];
					rightTriaColor = GetTisdColor(strTisd);
				}
				int CflCount = 0;
				int AckCount = 0;
				int TotalCount = 0;
				int OSTCount = 0;
				CFC.HasFlightStatusConflicts(strBarKey, ref CflCount, ref AckCount, ref TotalCount, ref OSTCount);
				if(OSTCount > 0)
				{
					strFlno += " *";
				}
				if(blSelectBar == false)
					//if(strAftUrno != omCurrentSelectedUrno)
				{
					if(CflCount > 0)
					{
						colBody = UT.colRed;
					}
					else if(AckCount > 0)
					{
						colBody = UT.colOrange;
					}
				}
				else
				{
					colBody = UT.colPink;
					myGantt.SetTimeMarker(myFrom, UT.colYellow);
					myGantt.SetTimeMarker(myTo, UT.colYellow);
				}
				myGantt.AddBarToLine( LineNo, strBarKey, myFrom, myTo, 
					strFlno, 1,
					UT.colCyan, UT.colBlack, UT.colRed, 
					UT.colBlack, UT.colBlack, 
					"", "", UT.colBlack, UT.colBlack);
				myGantt.SetBarColor(strBarKey, colBody);
				string strTriaID = leftTriaColor.ToString() + rightTriaColor.ToString();
				myGantt.SetBarTriangles(strBarKey, strTriaID, leftTriaColor, rightTriaColor, true);
				if(blRefresh == true)
				{
					myGantt.Refresh();
				}
			}
		}
		private void myGantt_OnLButtonDownBar(object sender, AxUGANTTLib._DUGanttEvents_OnLButtonDownBarEvent e)
		{
			UpdateBar(omCurrentSelectedUrno, false, false); 
			omCurrentSelectedUrno = e.key;
			UpdateBar(e.key, true, true);
		}

		private void cbSearch_CheckedChanged(object sender, System.EventArgs e)
		{
			if(cbSearch.Checked == true)		
			{
				cbSearch.BackColor = Color.LightGreen;
				myFindDlg = new frmFind();
				myFindDlg.Owner = this;
				myFindDlg.Show();
			}
			else
			{
				if(myFindDlg != null)
				{
					myFindDlg.Close();
					myFindDlg = null;
				}
				cbSearch.BackColor = Color.Transparent;
			}
		} 

		private void DE_OnFindList_Closed()
		{
			cbSearch.Checked = false;		
			myFindDlg = null;
		}

		private void DE_OnNavigateToFlight(object sender, string strAftUrno)
		{
			if(sender == (object)this)
			{
				return; //No update if "this" is the sender
			}
			string strTIF = "";
			DateTime datScroll;
			IRow [] aftRows =  myAFT.RowsByIndexValue("URNO", strAftUrno);
			if(aftRows.Length > 0)
			{
				if(aftRows[0]["ADID"] == "A")
				{
					strTIF = aftRows[0]["TIFA"];
				}
				if(aftRows[0]["ADID"] == "D")
				{
					strTIF = aftRows[0]["TIFD"];
				}
				datScroll = UT.CedaFullDateToDateTime(strTIF);
				TimeSpan olSpan = new TimeSpan(0, 1, 0, 0, 0);
				datScroll = datScroll.Subtract(olSpan);
				if(strAftUrno != omCurrentSelectedUrno)
				{
					UpdateBar(omCurrentSelectedUrno, false, false); 
				}
				omCurrentSelectedUrno = strAftUrno;
				UpdateBar(strAftUrno, true, true);
				myGantt.ScrollTo(datScroll);
				myGantt.Refresh();
			}
		}

		private void cbNow_CheckedChanged(object sender, System.EventArgs e)
		{
			if(cbNow.Checked == true)
			{
				ScrollToNow();
			}
		}

		private void ScrollToNow()
		{
			DateTime d = DateTime.Now;
			DateTime datUTC = CFC.GetUTC();
			TimeSpan olTS = d - datUTC;
			if(UT.IsTimeInUtc == true && UT.UTCOffset != 0)
			{
				TimeSpan olSpan = new TimeSpan(0, 1, 0, 0, 0);
				d = datUTC.Subtract(olSpan);
			}
			else
			{
				TimeSpan olSpan = new TimeSpan(0, 1, 0, 0, 0);
				d = d.Subtract(olSpan);
			}
			if(d < UT.TimeFrameFrom || d > UT.TimeFrameTo)
			{
				myGantt.ScrollTo(UT.TimeFrameFrom);
			}
			else 
			{
				myGantt.ScrollTo(d);
			}
			myGantt.Refresh();
			lblAboveText.Text = d.ToShortDateString();
			lblAboveText.Refresh();
			cbNow.Checked = false;
		}
		private void timerCloseStartup_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			timerCloseStartup.Stop();
			timerCloseStartup.Enabled = false;
			StartForm.Refresh();
			StartForm.PrepareClose();
		}


		private void DE_OnUpdateFlightData(object sender, string strAftUrno, State state)
		{
			if(omCurrentSelectedUrno == strAftUrno)
			{
				UpdateBar(strAftUrno, true, true); 
			}
			else
			{
				UpdateBar(strAftUrno, false, true); 
			}
		}

		private void DE_OnViewNameChanged()
		{ 
			txtCalc.Visible = true;
			txtCalc.BringToFront(); 
			txtCalc.Refresh();
			for(int i = 0; i < myAFT.Count; i++)
			{
				UpdateBar(myAFT[i]["URNO"], false, false);
				if((i % 50) == 0)
				{
					txtCalc.Text = "Updating View " + i.ToString() + " of " + myAFT.Count.ToString();
					txtCalc.Refresh();
				}
			}
			txtCalc.Visible = false;
			myGantt.Refresh();
		}

		private void DE_OnUTC_LocalTimeChanged()
		{
			AllocateFlights();
		}

		private void btnDeleteViewPoint_Click(object sender, System.EventArgs e)
		{
			string strViewName = cbViewPoints.Text;
			if(strViewName != "")
			{
				DialogResult r = MessageBox.Show(this, "Do you really want to delete <" + strViewName + "> ???", "Delete Question",
					MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if( r == DialogResult.Yes)
				{
					int i = 0;
					for( i = DataForm.tabVPD.GetLineCount()-1; i >= 0; i--)
					{
						if(DataForm.tabVPD.GetFieldValue(i, "VIEW") == strViewName)
						{
							DataForm.tabVPD.DeleteLine(i);
						}
					}
					for( i = DataForm.tabVPL.GetLineCount()-1; i >= 0; i--)
					{
						if(DataForm.tabVPL.GetFieldValue(i, "VIEW") == strViewName)
						{
							DataForm.tabVPL.DeleteLine(i);
						}
					}
					IUfisComWriter myUfisCom = UT.GetUfisCom();
					int ilRet = myUfisCom.CallServer("DRT", "VPDTAB", "", "", "WHERE VNAM = '" + strViewName + "'", "230");
					if(ilRet != 0)
					{
						string strERR = myUfisCom.LastErrorMessage;
						//MessageBox.Show(this, strERR);
					}
					ilRet = myUfisCom.CallServer("DRT", "VPLTAB", "", "", "WHERE VNAM = '" + strViewName + "'", "230");
					if(ilRet != 0)
					{
						string strERR = myUfisCom.LastErrorMessage;
						//MessageBox.Show(this, strERR);
					}
					ReloadCombo();
				}
				//Remove the viewpoint from the registry
				string theKey = "Software\\StatusManager";
				RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);

				if(rk != null)
				{
					theKey = "STAT_MGR_VIEWPOINT_" + strViewName;
					rk.DeleteSubKey(theKey, false );
				}
			}
		}

		private void ReloadData()
		{
			StartForm = new frmStartup(); 
			StartForm.Show();
			frmLoad olLoad = new frmLoad();
			this.lblAboveText.BackColor = Color.Transparent;
			DialogResult olResult;
			olResult = olLoad.ShowDialog();
			if (olResult == DialogResult.OK)
			{
				UT.TimeFrameFrom = olLoad.datFrom.Value;
				UT.TimeFrameTo = olLoad.datTo.Value;
				UT.TimeFrameFrom = UT.TimeFrameFrom.Subtract(new TimeSpan(0, 12, 0, 0));
				UT.TimeFrameTo = UT.TimeFrameTo.AddHours(12);

				DataForm.LoadOnlineData(true);

				StartForm.txtCurrentStatus.Text = "Allocating flights to gantt-chart";
				StartForm.txtCurrentStatus.Refresh();
				StartForm.txtDoneStatus.Refresh();
				//AllocateFlights();
				StartForm.BringToFront();
				this.Text = "Status Manager: Timeframe [" + UT.TimeFrameFrom.ToString() + "] - [" + 
					UT.TimeFrameTo.ToString() + "]" + " --- Server: " + UT.ServerName + 
					" --- Active View: [" + CFC.CurrentViewName + "]";
			}
			timerCloseStartup.Enabled = true;
		}

		private void cbLoad_CheckedChanged(object sender, System.EventArgs e)
		{
			if(cbLoad.Checked == true)
			{
				cbLoad.Checked = false;
				ReloadData();
				HandleUTC();
			}
		}

		private void DE_OnCLO_Message()
		{
			MessageBox.Show(this, "The server has been switched!!\nPlease leave Status-Manager and restart in 3 minutes",
							"Attention", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			Application.Exit();
		}

		private void DE_OnReloadCompleteOnlineStatuses()
		{
			txtCalc.Visible = true;
			txtCalc.BringToFront();
			txtCalc.Text = "Reload Online Status ...";
			txtCalc.Refresh();
			DataForm.LoadOstData(false);
			HandleUTC();
			txtCalc.Visible = false;
			AllocateFlights();
			DE.Call_ConflicTimerElapsed();
		}
	}
}

