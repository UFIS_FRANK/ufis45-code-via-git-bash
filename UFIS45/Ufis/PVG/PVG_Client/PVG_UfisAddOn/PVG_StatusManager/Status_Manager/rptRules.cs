using System;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using Ufis.Data;
using Ufis.Utils;
using System.Drawing;

namespace Status_Manager
{
	public class rptRules : ActiveReport
	{
		AxTABLib.AxTAB myTab = null;
		private int currLine = 0;
		private IDatabase myDB = null;
		public rptRules(AxTABLib.AxTAB pTab)
		{
			myTab = pTab;
			InitializeReport();
		}

		private void rptRules_FetchData(object sender, DataDynamics.ActiveReports.ActiveReport.FetchEventArgs eArgs)
		{
			string strURNO = myTab.GetFieldValue(currLine, "URNO");
			txtName.Text	= " " + myTab.GetFieldValue(currLine, "NAME");
			txtSection.Text = " " + myTab.GetFieldValue(currLine, "UHSS");
			txtFlight.Text	= " " + myTab.GetFieldValue(currLine, "ADID");
			
			ITable tabVal = myDB["VAL"];
			IRow [] rowsVal = tabVal.RowsByIndexValue("UVAL", strURNO);
			txtValidFrom.Text = "";
			txtValidTo.Text = "";
			if(rowsVal.Length > 0)
			{
				DateTime datFrom = UT.CedaFullDateToDateTime(rowsVal[0]["VAFR"]);
				DateTime datTo   = UT.CedaFullDateToDateTime(rowsVal[0]["VATO"]);
				txtValidFrom.Text = " " + datFrom.ToShortDateString();
				txtValidTo.Text = " " + datTo.ToShortDateString();
			}

			if(currLine == myTab.GetLineCount())
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
				currLine++;
			}
			sep0.Y1 = 0;
			sep1.Y1 = 0; 
			sep2.Y1 =  0;
			sep3.Y1 =  0;
			sep4.Y1 =  0;
			sep5.Y1 =  0;
			sep0.Y2 = txtName.Height;
			sep1.Y2 = txtName.Height; 
			sep2.Y2 =  txtName.Height;
			sep3.Y2 =  txtName.Height;
			sep4.Y2 =  txtName.Height;
			sep5.Y2 =  txtName.Height;
		}

		private void rptRules_ReportStart(object sender, System.EventArgs eArgs)
		{
			DateTime d = DateTime.Now;
			txtDate.Text = d.ToString();
			myDB = UT.GetMemDB();
			//*** Read the logo if exists and set it
			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			string strPrintLogoPath = myIni.IniReadValue("STATUSMANAGER", "PRINTLOGO");
			Image myImage = null;
			try
			{
				if(strPrintLogoPath != "")
				{
					myImage = new Bitmap(strPrintLogoPath);
				}
			}
			catch(Exception)
			{
			}
			if(myImage != null)
				Picture1.Image = myImage;
			//*** END Read the logo if exists and set it
		}

		#region ActiveReports Designer generated code
		private PageHeader PageHeader = null;
		private Label Label9 = null;
		private Label Label8 = null;
		private Label Label3 = null;
		private Label Label4 = null;
		private Picture Picture1 = null;
		private Line Line1 = null;
		private Line Line2 = null;
		private Label Label2 = null;
		private Line Line3 = null;
		private Line Line4 = null;
		private Line Line5 = null;
		private Line Line6 = null;
		private Line Line8 = null;
		private TextBox TextBox3 = null;
		private Line Line10 = null;
		private Line Line11 = null;
		private Detail Detail = null;
		private TextBox txtValidTo = null;
		private TextBox txtValidFrom = null;
		private TextBox txtName = null;
		private TextBox txtSection = null;
		private TextBox txtFlight = null;
		private Line sep1 = null;
		private Line sep2 = null;
		private Line sep3 = null;
		private Line Line7 = null;
		private Line sep0 = null;
		private Line sep4 = null;
		private Line sep5 = null;
		private PageFooter PageFooter = null;
		private TextBox TextBox1 = null;
		private Label Label5 = null;
		private TextBox TextBox2 = null;
		private Label Label6 = null;
		private Label Label7 = null;
		private TextBox txtDate = null;
		public void InitializeReport()
		{
			this.LoadLayout(this.GetType(), "Status_Manager.rptRules.rpx");
			this.PageHeader = ((DataDynamics.ActiveReports.PageHeader)(this.Sections["PageHeader"]));
			this.Detail = ((DataDynamics.ActiveReports.Detail)(this.Sections["Detail"]));
			this.PageFooter = ((DataDynamics.ActiveReports.PageFooter)(this.Sections["PageFooter"]));
			this.Label9 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[0]));
			this.Label8 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[1]));
			this.Label3 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[2]));
			this.Label4 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[3]));
			this.Picture1 = ((DataDynamics.ActiveReports.Picture)(this.PageHeader.Controls[4]));
			this.Line1 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[5]));
			this.Line2 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[6]));
			this.Label2 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[7]));
			this.Line3 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[8]));
			this.Line4 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[9]));
			this.Line5 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[10]));
			this.Line6 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[11]));
			this.Line8 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[12]));
			this.TextBox3 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[13]));
			this.Line10 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[14]));
			this.Line11 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[15]));
			this.txtValidTo = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[0]));
			this.txtValidFrom = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[1]));
			this.txtName = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[2]));
			this.txtSection = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[3]));
			this.txtFlight = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[4]));
			this.sep1 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[5]));
			this.sep2 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[6]));
			this.sep3 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[7]));
			this.Line7 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[8]));
			this.sep0 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[9]));
			this.sep4 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[10]));
			this.sep5 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[11]));
			this.TextBox1 = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[0]));
			this.Label5 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[1]));
			this.TextBox2 = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[2]));
			this.Label6 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[3]));
			this.Label7 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[4]));
			this.txtDate = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[5]));
			// Attach Report Events
			this.FetchData += new DataDynamics.ActiveReports.ActiveReport.FetchEventHandler(this.rptRules_FetchData);
			this.ReportStart += new System.EventHandler(this.rptRules_ReportStart);
		}

		#endregion
	}
}
