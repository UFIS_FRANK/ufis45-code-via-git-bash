using System;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using Ufis.Utils;
using System.Drawing;
namespace Status_Manager
{
	public class rptDetailStatus : ActiveReport
	{
		private string strSection = "";
		private int currLine = 0;
		AxTABLib.AxTAB tabArrival = null; 
		AxTABLib.AxTAB tabDeparture = null; 
		AxTABLib.AxTAB tabStatus = null;

		public rptDetailStatus(AxTABLib.AxTAB tArrival, AxTABLib.AxTAB tDeparture, 
						      AxTABLib.AxTAB tStatus, string sSection)
		{
			strSection = sSection;
			//FLNO,STOA,ETAI,ONBL,NA,ORIG,VIA,POS,GATE,ACT3
			tabArrival = tArrival;
			//FLNO,STOD,ETDI,OFBL,NA,VIA,DES,POS,GATE,ACT
			tabDeparture = tDeparture;
			//CONA,CONU,RULNAME,REFFIELD,STATUSNAME,CFLTYPE_DB,CONFLICTTYPE,TIME,SORT,REMA
			tabStatus = tStatus;
			InitializeReport();
			this.ShowParameterUI = true;
		}

		private void rptDetailStatus_ReportStart(object sender, System.EventArgs eArgs)
		{
			//string strtmp="";
			DateTime d		= DateTime.Now;
			DateTime dt;
			txtDate.Text	= d.ToString();
//*** Read the logo if exists and set it
			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			string strPrintLogoPath = myIni.IniReadValue("STATUSMANAGER", "PRINTLOGO");
			Image myImage = null;
			try
			{
				if(strPrintLogoPath != "")
				{
					myImage = new Bitmap(strPrintLogoPath);
				}
			}
			catch(Exception)
			{
			}
			if(myImage != null)
				Picture1.Image = myImage;
//*** END Read the logo if exists and set it
			lblSection.Text = strSection;
			txtFLNO_A.Text	= tabArrival.GetFieldValue(0, "FLNO");
			if(tabArrival.GetFieldValue(0, "STOA") != "")
			{
				dt = UT.CedaFullDateToDateTime(tabArrival.GetFieldValue(0, "STOA"));
				txtSTA.Text		= dt.ToString("HH:mm/dd");
			}
			if(tabArrival.GetFieldValue(0, "ETAI") != "")
			{
				dt = UT.CedaFullDateToDateTime(tabArrival.GetFieldValue(0, "ETAI"));
				txtETA.Text		= dt.ToString("HH:mm/dd");
			}
			if(tabArrival.GetFieldValue(0, "ONBL") != "")
			{
				dt = UT.CedaFullDateToDateTime(tabArrival.GetFieldValue(0, "ONBL"));
				txtONBL.Text	= dt.ToString("HH:mm/dd");
			}
			txtNA_A.Text	= tabArrival.GetFieldValue(0, "NA");
			txtORG.Text		= tabArrival.GetFieldValue(0, "ORIG");
			txtVIA_A.Text	= tabArrival.GetFieldValue(0, "VIA");
			txtPOS_A.Text	= tabArrival.GetFieldValue(0, "POS");
			txtGATE_A.Text	= tabArrival.GetFieldValue(0, "GATE"); 
			txtAC_A.Text	= tabArrival.GetFieldValue(0, "ACT3");

			txtFLNO_D.Text = tabDeparture.GetFieldValue(0, "FLNO");
			if(tabDeparture.GetFieldValue(0, "STOD") != "")
			{
				dt = UT.CedaFullDateToDateTime(tabDeparture.GetFieldValue(0, "STOD"));
				txtSTD.Text    = dt.ToString("HH:mm/dd");
			}
			if(tabDeparture.GetFieldValue(0, "ETDI") != "")
			{
				dt = UT.CedaFullDateToDateTime(tabDeparture.GetFieldValue(0, "ETDI"));
				txtETD.Text    = dt.ToString("HH:mm/dd");
			}
			if(tabDeparture.GetFieldValue(0, "OFBL") != "")
			{
				dt = UT.CedaFullDateToDateTime(tabDeparture.GetFieldValue(0, "OFBL"));
				txtOFBL.Text   = dt.ToString("HH:mm/dd");
			}
			txtNA_D.Text   = tabDeparture.GetFieldValue(0, "NA");
			txtVIA_D.Text  = tabDeparture.GetFieldValue(0, "VIA");
			txtDEST.Text   = tabDeparture.GetFieldValue(0, "DES");
			txtPOS_D.Text  = tabDeparture.GetFieldValue(0, "POS");
			txtGATE_D.Text = tabDeparture.GetFieldValue(0, "GATE"); 
			txtAC_D.Text   = tabDeparture.GetFieldValue(0, "ACT"); 
		}

		private void rptDetailStatus_FetchData(object sender, DataDynamics.ActiveReports.ActiveReport.FetchEventArgs eArgs)
		{
			//CONA,CONU,RULNAME,REFFIELD,STATUSNAME,CFLTYPE_DB,CONFLICTTYPE,TIME,SORT,REMA
			DateTime dt;
			sep1.Y1 = 0; 
			sep2.Y1 =  0;
			sep3.Y1 =  0;
			sep4.Y1 =  0;
			sep5.Y1 =  0;
			sep6.Y1 =  0;
			sep7.Y1 =  0;
			sep8.Y1 =  0;
			sep9.Y1 =  0;
			txtUSER.Text = "";
			txtSYSTEM.Text = "";
			txtEXPECTED.Text = "";
			if(tabStatus.GetFieldValue(currLine, "CONS") != "")
			{
				string strTmp = tabStatus.GetFieldValue(currLine, "CONS");
				if(strTmp.Length == 14)
				{
					dt = UT.CedaFullDateToDateTime(tabStatus.GetFieldValue(currLine, "CONS"));
					txtEXPECTED.Text = " " + dt.ToString("HH:mm/dd");
				}
			}
			if(tabStatus.GetFieldValue(currLine, "CONA") != "")
			{
				string strTmp = tabStatus.GetFieldValue(currLine, "CONA");
				if(strTmp.Length == 14)
				{
					dt = UT.CedaFullDateToDateTime(tabStatus.GetFieldValue(currLine, "CONA"));
					
					txtSYSTEM.Text = " " + dt.ToString("HH:mm/dd");
				}
			}
			if(tabStatus.GetFieldValue(currLine, "CONU") != "")
			{
				string strTmp = tabStatus.GetFieldValue(currLine, "CONU");
				if(strTmp.Length == 14)
				{
					dt = UT.CedaFullDateToDateTime(tabStatus.GetFieldValue(currLine, "CONU"));
					txtUSER.Text = " " + dt.ToString("HH:mm/dd");
				}
			} 
			txtRULENAME.Text = " " + tabStatus.GetFieldValue(currLine, "RULNAME");
			txtSTATUSNAME.Text = " " + tabStatus.GetFieldValue(currLine, "STATUSNAME");
			txtCFLTYPE.Text =  " " + tabStatus.GetFieldValue(currLine, "CONFLICTTYPE");
			txtTIME.Text = 	" " + tabStatus.GetFieldValue(currLine, "TIME");
			txtREMARK.Text =  " " + tabStatus.GetFieldValue(currLine, "REMA");
			if(currLine == tabStatus.GetLineCount())
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
				currLine++;
			}
		}

		private void Detail_BeforePrint(object sender, System.EventArgs eArgs)
		{
			sep1.Y2 = txtREMARK.Height; 
			sep2.Y2 =  txtREMARK.Height;
			sep3.Y2 =  txtREMARK.Height;
			sep4.Y2 =  txtREMARK.Height;
			sep5.Y2 =  txtREMARK.Height;
			sep6.Y2 =  txtREMARK.Height;
			sep7.Y2 =  txtREMARK.Height;
			sep8.Y2 =  txtREMARK.Height;
			sep9.Y2 =  txtREMARK.Height;
			sepHorz.Y1 = txtREMARK.Height;
			sepHorz.Y2 = txtREMARK.Height;
		}

		#region ActiveReports Designer generated code
		private PageHeader PageHeader = null;
		private Label Label35 = null;
		private Label Label1 = null;
		private Label lblSection = null;
		private Label Label24 = null;
		private Picture Picture1 = null;
		private Label Label2 = null;
		private Label Label4 = null;
		private Label Label5 = null;
		private Label Label6 = null;
		private Label Label7 = null;
		private Label Label8 = null;
		private Label Label9 = null;
		private Label Label10 = null;
		private Label Label11 = null;
		private Label Label12 = null;
		private Label Label13 = null;
		private Label Label15 = null;
		private Label Label16 = null;
		private Label Label17 = null;
		private Label Label18 = null;
		private Label Label19 = null;
		private Label Label21 = null;
		private Label Label22 = null;
		private Label Label23 = null;
		private Line Line4 = null;
		private Line Line25 = null;
		private Line Line26 = null;
		private TextBox txtFLNO_A = null;
		private TextBox txtSTA = null;
		private TextBox txtETA = null;
		private TextBox txtONBL = null;
		private TextBox txtNA_A = null;
		private TextBox txtORG = null;
		private TextBox txtVIA_A = null;
		private TextBox txtPOS_A = null;
		private TextBox txtGATE_A = null;
		private TextBox txtAC_A = null;
		private TextBox txtFLNO_D = null;
		private TextBox txtSTD = null;
		private TextBox txtETD = null;
		private TextBox txtOFBL = null;
		private TextBox txtNA_D = null;
		private TextBox txtVIA_D = null;
		private TextBox txtDEST = null;
		private TextBox txtPOS_D = null;
		private TextBox txtGATE_D = null;
		private TextBox txtAC_D = null;
		private Line Line28 = null;
		private Line Line29 = null;
		private Line Line30 = null;
		private Line Line31 = null;
		private Line Line32 = null;
		private Line Line33 = null;
		private Line Line34 = null;
		private Line Line35 = null;
		private Line Line36 = null;
		private Line Line37 = null;
		private Line Line38 = null;
		private Line Line39 = null;
		private Line Line40 = null;
		private Line Line41 = null;
		private Line Line42 = null;
		private Line Line43 = null;
		private Line Line44 = null;
		private Line Line45 = null;
		private Line Line46 = null;
		private Line Line47 = null;
		private Line Line48 = null;
		private Line Line49 = null;
		private Line Line50 = null;
		private Label Label25 = null;
		private Label Label26 = null;
		private Label Label27 = null;
		private Label Label28 = null;
		private Label Label29 = null;
		private Label Label30 = null;
		private Label Label31 = null;
		private Line Line51 = null;
		private Line Line52 = null;
		private Line Line53 = null;
		private Line Line54 = null;
		private Line Line55 = null;
		private Line Line56 = null;
		private Line Line57 = null;
		private Line Line58 = null;
		private Line Line59 = null;
		private Line Line60 = null;
		private Line Line70 = null;
		private Detail Detail = null;
		private TextBox txtEXPECTED = null;
		private TextBox txtSYSTEM = null;
		private TextBox txtUSER = null;
		private TextBox txtRULENAME = null;
		private TextBox txtSTATUSNAME = null;
		private TextBox txtCFLTYPE = null;
		private TextBox txtTIME = null;
		private TextBox txtREMARK = null;
		private Line sep3 = null;
		private Line sep1 = null;
		private Line sep2 = null;
		private Line sep4 = null;
		private Line sep5 = null;
		private Line sep6 = null;
		private Line sep7 = null;
		private Line sep8 = null;
		private Line sepHorz = null;
		private Line sep9 = null;
		private PageFooter PageFooter = null;
		private TextBox TextBox1 = null;
		private Label Label32 = null;
		private TextBox TextBox2 = null;
		private Label Label33 = null;
		private Label Label34 = null;
		private TextBox txtDate = null;
		public void InitializeReport()
		{
			this.LoadLayout(this.GetType(), "Status_Manager.rptDetailStatus.rpx");
			this.PageHeader = ((DataDynamics.ActiveReports.PageHeader)(this.Sections["PageHeader"]));
			this.Detail = ((DataDynamics.ActiveReports.Detail)(this.Sections["Detail"]));
			this.PageFooter = ((DataDynamics.ActiveReports.PageFooter)(this.Sections["PageFooter"]));
			this.Label35 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[0]));
			this.Label1 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[1]));
			this.lblSection = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[2]));
			this.Label24 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[3]));
			this.Picture1 = ((DataDynamics.ActiveReports.Picture)(this.PageHeader.Controls[4]));
			this.Label2 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[5]));
			this.Label4 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[6]));
			this.Label5 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[7]));
			this.Label6 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[8]));
			this.Label7 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[9]));
			this.Label8 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[10]));
			this.Label9 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[11]));
			this.Label10 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[12]));
			this.Label11 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[13]));
			this.Label12 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[14]));
			this.Label13 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[15]));
			this.Label15 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[16]));
			this.Label16 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[17]));
			this.Label17 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[18]));
			this.Label18 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[19]));
			this.Label19 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[20]));
			this.Label21 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[21]));
			this.Label22 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[22]));
			this.Label23 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[23]));
			this.Line4 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[24]));
			this.Line25 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[25]));
			this.Line26 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[26]));
			this.txtFLNO_A = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[27]));
			this.txtSTA = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[28]));
			this.txtETA = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[29]));
			this.txtONBL = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[30]));
			this.txtNA_A = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[31]));
			this.txtORG = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[32]));
			this.txtVIA_A = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[33]));
			this.txtPOS_A = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[34]));
			this.txtGATE_A = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[35]));
			this.txtAC_A = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[36]));
			this.txtFLNO_D = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[37]));
			this.txtSTD = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[38]));
			this.txtETD = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[39]));
			this.txtOFBL = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[40]));
			this.txtNA_D = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[41]));
			this.txtVIA_D = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[42]));
			this.txtDEST = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[43]));
			this.txtPOS_D = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[44]));
			this.txtGATE_D = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[45]));
			this.txtAC_D = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[46]));
			this.Line28 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[47]));
			this.Line29 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[48]));
			this.Line30 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[49]));
			this.Line31 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[50]));
			this.Line32 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[51]));
			this.Line33 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[52]));
			this.Line34 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[53]));
			this.Line35 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[54]));
			this.Line36 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[55]));
			this.Line37 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[56]));
			this.Line38 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[57]));
			this.Line39 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[58]));
			this.Line40 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[59]));
			this.Line41 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[60]));
			this.Line42 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[61]));
			this.Line43 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[62]));
			this.Line44 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[63]));
			this.Line45 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[64]));
			this.Line46 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[65]));
			this.Line47 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[66]));
			this.Line48 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[67]));
			this.Line49 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[68]));
			this.Line50 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[69]));
			this.Label25 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[70]));
			this.Label26 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[71]));
			this.Label27 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[72]));
			this.Label28 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[73]));
			this.Label29 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[74]));
			this.Label30 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[75]));
			this.Label31 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[76]));
			this.Line51 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[77]));
			this.Line52 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[78]));
			this.Line53 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[79]));
			this.Line54 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[80]));
			this.Line55 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[81]));
			this.Line56 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[82]));
			this.Line57 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[83]));
			this.Line58 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[84]));
			this.Line59 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[85]));
			this.Line60 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[86]));
			this.Line70 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[87]));
			this.txtEXPECTED = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[0]));
			this.txtSYSTEM = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[1]));
			this.txtUSER = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[2]));
			this.txtRULENAME = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[3]));
			this.txtSTATUSNAME = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[4]));
			this.txtCFLTYPE = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[5]));
			this.txtTIME = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[6]));
			this.txtREMARK = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[7]));
			this.sep3 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[8]));
			this.sep1 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[9]));
			this.sep2 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[10]));
			this.sep4 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[11]));
			this.sep5 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[12]));
			this.sep6 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[13]));
			this.sep7 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[14]));
			this.sep8 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[15]));
			this.sepHorz = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[16]));
			this.sep9 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[17]));
			this.TextBox1 = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[0]));
			this.Label32 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[1]));
			this.TextBox2 = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[2]));
			this.Label33 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[3]));
			this.Label34 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[4]));
			this.txtDate = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[5]));
			// Attach Report Events
			this.ReportStart += new System.EventHandler(this.rptDetailStatus_ReportStart);
			this.FetchData += new DataDynamics.ActiveReports.ActiveReport.FetchEventHandler(this.rptDetailStatus_FetchData);
			this.Detail.BeforePrint += new System.EventHandler(this.Detail_BeforePrint);
		}

		#endregion
	}
}
