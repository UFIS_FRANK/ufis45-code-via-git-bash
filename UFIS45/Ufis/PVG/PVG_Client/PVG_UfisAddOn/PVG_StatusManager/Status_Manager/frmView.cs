using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Ufis.Utils;
using Ufis.Data;
using Microsoft.Win32;

namespace Status_Manager
{
	/// <summary>
	/// Summary description for frmView.
	/// </summary>
	public class frmView : System.Windows.Forms.Form
	{
		#region ----- MyMembers

		private IDatabase myDB = null;
		private ITable myHSS = null;
		private ITable myALT = null;
		private ITable myNAT = null;
		private AxTABLib.AxTAB myViewsTab;
		private StringBuilder bufferHSS = new StringBuilder(10000);
		private StringBuilder bufferALT = new StringBuilder(10000);
		private StringBuilder bufferNAT = new StringBuilder(10000);

		#endregion ----- MyMembers

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.PictureBox picBody;
		private System.Windows.Forms.Label lblTo;
		private System.Windows.Forms.Label lblFrom;
		public System.Windows.Forms.DateTimePicker dtTo;
		public System.Windows.Forms.DateTimePicker dtFrom;
		private System.Windows.Forms.PictureBox picStatusSectionFilter;
		private AxTABLib.AxTAB tabSectionSource;
		private AxTABLib.AxTAB tabSectionTarget;
		private System.Windows.Forms.ImageList imageListArrows;
		private System.Windows.Forms.Button btnSectionSource;
		private System.Windows.Forms.Button bntSectionTarget;
		private System.Windows.Forms.Button btnAirlineTarget;
		private System.Windows.Forms.Button btnAirlineSource;
		private AxTABLib.AxTAB tabAirlineTarget;
		private AxTABLib.AxTAB tabAirlineSource;
		private System.Windows.Forms.Button btnNatureTarget;
		private System.Windows.Forms.Button btnNatureSource;
		private AxTABLib.AxTAB tabNatureTarget;
		private AxTABLib.AxTAB tabNatureSource;
		private System.Windows.Forms.ComboBox cbViewName;
		private System.Windows.Forms.Label lblViewName;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.ImageList imageButtons;
		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.PictureBox picAirlines;
		private System.Windows.Forms.PictureBox picNatures;
		private System.Windows.Forms.Panel panelStatusSections;
		private System.Windows.Forms.Panel panelAirlines;
		private System.Windows.Forms.Panel panelNatures;
		private System.Windows.Forms.TextBox txtSection;
		private System.Windows.Forms.TextBox txtAirline;
		private System.Windows.Forms.TextBox txtNature;
		private System.Windows.Forms.CheckBox cbStatueSections;
		private System.Windows.Forms.CheckBox cbAirlines;
		private System.Windows.Forms.CheckBox cbNatures;
		private System.Windows.Forms.Button btnNew;
		private System.Windows.Forms.Button btnDelete;
		private System.Windows.Forms.Panel panelButtons;
		private System.Windows.Forms.PictureBox picButtons;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Label lblCurrentActiveView;
		private System.ComponentModel.IContainer components;

		public frmView(AxTABLib.AxTAB pTabViews)
		{
			myViewsTab = pTabViews;
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmView));
			this.label1 = new System.Windows.Forms.Label();
			this.picBody = new System.Windows.Forms.PictureBox();
			this.lblTo = new System.Windows.Forms.Label();
			this.lblFrom = new System.Windows.Forms.Label();
			this.dtTo = new System.Windows.Forms.DateTimePicker();
			this.dtFrom = new System.Windows.Forms.DateTimePicker();
			this.picStatusSectionFilter = new System.Windows.Forms.PictureBox();
			this.tabSectionSource = new AxTABLib.AxTAB();
			this.tabSectionTarget = new AxTABLib.AxTAB();
			this.imageListArrows = new System.Windows.Forms.ImageList(this.components);
			this.btnSectionSource = new System.Windows.Forms.Button();
			this.bntSectionTarget = new System.Windows.Forms.Button();
			this.btnAirlineTarget = new System.Windows.Forms.Button();
			this.btnAirlineSource = new System.Windows.Forms.Button();
			this.tabAirlineTarget = new AxTABLib.AxTAB();
			this.tabAirlineSource = new AxTABLib.AxTAB();
			this.picAirlines = new System.Windows.Forms.PictureBox();
			this.btnNatureTarget = new System.Windows.Forms.Button();
			this.btnNatureSource = new System.Windows.Forms.Button();
			this.tabNatureTarget = new AxTABLib.AxTAB();
			this.tabNatureSource = new AxTABLib.AxTAB();
			this.picNatures = new System.Windows.Forms.PictureBox();
			this.cbViewName = new System.Windows.Forms.ComboBox();
			this.lblViewName = new System.Windows.Forms.Label();
			this.btnOK = new System.Windows.Forms.Button();
			this.imageButtons = new System.Windows.Forms.ImageList(this.components);
			this.btnCancel = new System.Windows.Forms.Button();
			this.panelBody = new System.Windows.Forms.Panel();
			this.panelButtons = new System.Windows.Forms.Panel();
			this.lblCurrentActiveView = new System.Windows.Forms.Label();
			this.btnSave = new System.Windows.Forms.Button();
			this.btnNew = new System.Windows.Forms.Button();
			this.btnDelete = new System.Windows.Forms.Button();
			this.picButtons = new System.Windows.Forms.PictureBox();
			this.panelNatures = new System.Windows.Forms.Panel();
			this.cbNatures = new System.Windows.Forms.CheckBox();
			this.txtNature = new System.Windows.Forms.TextBox();
			this.panelAirlines = new System.Windows.Forms.Panel();
			this.cbAirlines = new System.Windows.Forms.CheckBox();
			this.txtAirline = new System.Windows.Forms.TextBox();
			this.panelStatusSections = new System.Windows.Forms.Panel();
			this.cbStatueSections = new System.Windows.Forms.CheckBox();
			this.txtSection = new System.Windows.Forms.TextBox();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			((System.ComponentModel.ISupportInitialize)(this.tabSectionSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabSectionTarget)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabAirlineTarget)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabAirlineSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabNatureTarget)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabNatureSource)).BeginInit();
			this.panelBody.SuspendLayout();
			this.panelButtons.SuspendLayout();
			this.panelNatures.SuspendLayout();
			this.panelAirlines.SuspendLayout();
			this.panelStatusSections.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AccessibleDescription = resources.GetString("label1.AccessibleDescription");
			this.label1.AccessibleName = resources.GetString("label1.AccessibleName");
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label1.Anchor")));
			this.label1.AutoSize = ((bool)(resources.GetObject("label1.AutoSize")));
			this.label1.BackColor = System.Drawing.Color.Black;
			this.label1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label1.Dock")));
			this.label1.Enabled = ((bool)(resources.GetObject("label1.Enabled")));
			this.label1.Font = ((System.Drawing.Font)(resources.GetObject("label1.Font")));
			this.label1.ForeColor = System.Drawing.Color.Lime;
			this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
			this.label1.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label1.ImageAlign")));
			this.label1.ImageIndex = ((int)(resources.GetObject("label1.ImageIndex")));
			this.label1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label1.ImeMode")));
			this.label1.Location = ((System.Drawing.Point)(resources.GetObject("label1.Location")));
			this.label1.Name = "label1";
			this.label1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label1.RightToLeft")));
			this.label1.Size = ((System.Drawing.Size)(resources.GetObject("label1.Size")));
			this.label1.TabIndex = ((int)(resources.GetObject("label1.TabIndex")));
			this.label1.Text = resources.GetString("label1.Text");
			this.label1.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label1.TextAlign")));
			this.toolTip1.SetToolTip(this.label1, resources.GetString("label1.ToolTip"));
			this.label1.Visible = ((bool)(resources.GetObject("label1.Visible")));
			// 
			// picBody
			// 
			this.picBody.AccessibleDescription = resources.GetString("picBody.AccessibleDescription");
			this.picBody.AccessibleName = resources.GetString("picBody.AccessibleName");
			this.picBody.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("picBody.Anchor")));
			this.picBody.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("picBody.BackgroundImage")));
			this.picBody.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("picBody.Dock")));
			this.picBody.Enabled = ((bool)(resources.GetObject("picBody.Enabled")));
			this.picBody.Font = ((System.Drawing.Font)(resources.GetObject("picBody.Font")));
			this.picBody.Image = ((System.Drawing.Image)(resources.GetObject("picBody.Image")));
			this.picBody.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("picBody.ImeMode")));
			this.picBody.Location = ((System.Drawing.Point)(resources.GetObject("picBody.Location")));
			this.picBody.Name = "picBody";
			this.picBody.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("picBody.RightToLeft")));
			this.picBody.Size = ((System.Drawing.Size)(resources.GetObject("picBody.Size")));
			this.picBody.SizeMode = ((System.Windows.Forms.PictureBoxSizeMode)(resources.GetObject("picBody.SizeMode")));
			this.picBody.TabIndex = ((int)(resources.GetObject("picBody.TabIndex")));
			this.picBody.TabStop = false;
			this.picBody.Text = resources.GetString("picBody.Text");
			this.toolTip1.SetToolTip(this.picBody, resources.GetString("picBody.ToolTip"));
			this.picBody.Visible = ((bool)(resources.GetObject("picBody.Visible")));
			this.picBody.Paint += new System.Windows.Forms.PaintEventHandler(this.picBody_Paint);
			// 
			// lblTo
			// 
			this.lblTo.AccessibleDescription = resources.GetString("lblTo.AccessibleDescription");
			this.lblTo.AccessibleName = resources.GetString("lblTo.AccessibleName");
			this.lblTo.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblTo.Anchor")));
			this.lblTo.AutoSize = ((bool)(resources.GetObject("lblTo.AutoSize")));
			this.lblTo.BackColor = System.Drawing.Color.Transparent;
			this.lblTo.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblTo.Dock")));
			this.lblTo.Enabled = ((bool)(resources.GetObject("lblTo.Enabled")));
			this.lblTo.Font = ((System.Drawing.Font)(resources.GetObject("lblTo.Font")));
			this.lblTo.Image = ((System.Drawing.Image)(resources.GetObject("lblTo.Image")));
			this.lblTo.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblTo.ImageAlign")));
			this.lblTo.ImageIndex = ((int)(resources.GetObject("lblTo.ImageIndex")));
			this.lblTo.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblTo.ImeMode")));
			this.lblTo.Location = ((System.Drawing.Point)(resources.GetObject("lblTo.Location")));
			this.lblTo.Name = "lblTo";
			this.lblTo.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblTo.RightToLeft")));
			this.lblTo.Size = ((System.Drawing.Size)(resources.GetObject("lblTo.Size")));
			this.lblTo.TabIndex = ((int)(resources.GetObject("lblTo.TabIndex")));
			this.lblTo.Text = resources.GetString("lblTo.Text");
			this.lblTo.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblTo.TextAlign")));
			this.toolTip1.SetToolTip(this.lblTo, resources.GetString("lblTo.ToolTip"));
			this.lblTo.Visible = ((bool)(resources.GetObject("lblTo.Visible")));
			// 
			// lblFrom
			// 
			this.lblFrom.AccessibleDescription = resources.GetString("lblFrom.AccessibleDescription");
			this.lblFrom.AccessibleName = resources.GetString("lblFrom.AccessibleName");
			this.lblFrom.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblFrom.Anchor")));
			this.lblFrom.AutoSize = ((bool)(resources.GetObject("lblFrom.AutoSize")));
			this.lblFrom.BackColor = System.Drawing.Color.Transparent;
			this.lblFrom.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblFrom.Dock")));
			this.lblFrom.Enabled = ((bool)(resources.GetObject("lblFrom.Enabled")));
			this.lblFrom.Font = ((System.Drawing.Font)(resources.GetObject("lblFrom.Font")));
			this.lblFrom.Image = ((System.Drawing.Image)(resources.GetObject("lblFrom.Image")));
			this.lblFrom.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblFrom.ImageAlign")));
			this.lblFrom.ImageIndex = ((int)(resources.GetObject("lblFrom.ImageIndex")));
			this.lblFrom.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblFrom.ImeMode")));
			this.lblFrom.Location = ((System.Drawing.Point)(resources.GetObject("lblFrom.Location")));
			this.lblFrom.Name = "lblFrom";
			this.lblFrom.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblFrom.RightToLeft")));
			this.lblFrom.Size = ((System.Drawing.Size)(resources.GetObject("lblFrom.Size")));
			this.lblFrom.TabIndex = ((int)(resources.GetObject("lblFrom.TabIndex")));
			this.lblFrom.Text = resources.GetString("lblFrom.Text");
			this.lblFrom.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblFrom.TextAlign")));
			this.toolTip1.SetToolTip(this.lblFrom, resources.GetString("lblFrom.ToolTip"));
			this.lblFrom.Visible = ((bool)(resources.GetObject("lblFrom.Visible")));
			// 
			// dtTo
			// 
			this.dtTo.AccessibleDescription = resources.GetString("dtTo.AccessibleDescription");
			this.dtTo.AccessibleName = resources.GetString("dtTo.AccessibleName");
			this.dtTo.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("dtTo.Anchor")));
			this.dtTo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("dtTo.BackgroundImage")));
			this.dtTo.CalendarFont = ((System.Drawing.Font)(resources.GetObject("dtTo.CalendarFont")));
			this.dtTo.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("dtTo.Dock")));
			this.dtTo.DropDownAlign = ((System.Windows.Forms.LeftRightAlignment)(resources.GetObject("dtTo.DropDownAlign")));
			this.dtTo.Enabled = ((bool)(resources.GetObject("dtTo.Enabled")));
			this.dtTo.Font = ((System.Drawing.Font)(resources.GetObject("dtTo.Font")));
			this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtTo.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("dtTo.ImeMode")));
			this.dtTo.Location = ((System.Drawing.Point)(resources.GetObject("dtTo.Location")));
			this.dtTo.Name = "dtTo";
			this.dtTo.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("dtTo.RightToLeft")));
			this.dtTo.Size = ((System.Drawing.Size)(resources.GetObject("dtTo.Size")));
			this.dtTo.TabIndex = ((int)(resources.GetObject("dtTo.TabIndex")));
			this.toolTip1.SetToolTip(this.dtTo, resources.GetString("dtTo.ToolTip"));
			this.dtTo.Visible = ((bool)(resources.GetObject("dtTo.Visible")));
			// 
			// dtFrom
			// 
			this.dtFrom.AccessibleDescription = resources.GetString("dtFrom.AccessibleDescription");
			this.dtFrom.AccessibleName = resources.GetString("dtFrom.AccessibleName");
			this.dtFrom.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("dtFrom.Anchor")));
			this.dtFrom.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("dtFrom.BackgroundImage")));
			this.dtFrom.CalendarFont = ((System.Drawing.Font)(resources.GetObject("dtFrom.CalendarFont")));
			this.dtFrom.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("dtFrom.Dock")));
			this.dtFrom.DropDownAlign = ((System.Windows.Forms.LeftRightAlignment)(resources.GetObject("dtFrom.DropDownAlign")));
			this.dtFrom.Enabled = ((bool)(resources.GetObject("dtFrom.Enabled")));
			this.dtFrom.Font = ((System.Drawing.Font)(resources.GetObject("dtFrom.Font")));
			this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtFrom.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("dtFrom.ImeMode")));
			this.dtFrom.Location = ((System.Drawing.Point)(resources.GetObject("dtFrom.Location")));
			this.dtFrom.Name = "dtFrom";
			this.dtFrom.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("dtFrom.RightToLeft")));
			this.dtFrom.Size = ((System.Drawing.Size)(resources.GetObject("dtFrom.Size")));
			this.dtFrom.TabIndex = ((int)(resources.GetObject("dtFrom.TabIndex")));
			this.toolTip1.SetToolTip(this.dtFrom, resources.GetString("dtFrom.ToolTip"));
			this.dtFrom.Visible = ((bool)(resources.GetObject("dtFrom.Visible")));
			// 
			// picStatusSectionFilter
			// 
			this.picStatusSectionFilter.AccessibleDescription = resources.GetString("picStatusSectionFilter.AccessibleDescription");
			this.picStatusSectionFilter.AccessibleName = resources.GetString("picStatusSectionFilter.AccessibleName");
			this.picStatusSectionFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("picStatusSectionFilter.Anchor")));
			this.picStatusSectionFilter.BackColor = System.Drawing.Color.Transparent;
			this.picStatusSectionFilter.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("picStatusSectionFilter.BackgroundImage")));
			this.picStatusSectionFilter.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("picStatusSectionFilter.Dock")));
			this.picStatusSectionFilter.Enabled = ((bool)(resources.GetObject("picStatusSectionFilter.Enabled")));
			this.picStatusSectionFilter.Font = ((System.Drawing.Font)(resources.GetObject("picStatusSectionFilter.Font")));
			this.picStatusSectionFilter.Image = ((System.Drawing.Image)(resources.GetObject("picStatusSectionFilter.Image")));
			this.picStatusSectionFilter.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("picStatusSectionFilter.ImeMode")));
			this.picStatusSectionFilter.Location = ((System.Drawing.Point)(resources.GetObject("picStatusSectionFilter.Location")));
			this.picStatusSectionFilter.Name = "picStatusSectionFilter";
			this.picStatusSectionFilter.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("picStatusSectionFilter.RightToLeft")));
			this.picStatusSectionFilter.Size = ((System.Drawing.Size)(resources.GetObject("picStatusSectionFilter.Size")));
			this.picStatusSectionFilter.SizeMode = ((System.Windows.Forms.PictureBoxSizeMode)(resources.GetObject("picStatusSectionFilter.SizeMode")));
			this.picStatusSectionFilter.TabIndex = ((int)(resources.GetObject("picStatusSectionFilter.TabIndex")));
			this.picStatusSectionFilter.TabStop = false;
			this.picStatusSectionFilter.Text = resources.GetString("picStatusSectionFilter.Text");
			this.toolTip1.SetToolTip(this.picStatusSectionFilter, resources.GetString("picStatusSectionFilter.ToolTip"));
			this.picStatusSectionFilter.Visible = ((bool)(resources.GetObject("picStatusSectionFilter.Visible")));
			this.picStatusSectionFilter.Paint += new System.Windows.Forms.PaintEventHandler(this.picStatusSectionFilter_Paint);
			// 
			// tabSectionSource
			// 
			this.tabSectionSource.AccessibleDescription = resources.GetString("tabSectionSource.AccessibleDescription");
			this.tabSectionSource.AccessibleName = resources.GetString("tabSectionSource.AccessibleName");
			this.tabSectionSource.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("tabSectionSource.Anchor")));
			this.tabSectionSource.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabSectionSource.BackgroundImage")));
			this.tabSectionSource.ContainingControl = this;
			this.tabSectionSource.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("tabSectionSource.Dock")));
			this.tabSectionSource.Font = ((System.Drawing.Font)(resources.GetObject("tabSectionSource.Font")));
			this.tabSectionSource.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("tabSectionSource.ImeMode")));
			this.tabSectionSource.Location = ((System.Drawing.Point)(resources.GetObject("tabSectionSource.Location")));
			this.tabSectionSource.Name = "tabSectionSource";
			this.tabSectionSource.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabSectionSource.OcxState")));
			this.tabSectionSource.RightToLeft = ((bool)(resources.GetObject("tabSectionSource.RightToLeft")));
			this.tabSectionSource.Size = ((System.Drawing.Size)(resources.GetObject("tabSectionSource.Size")));
			this.tabSectionSource.TabIndex = ((int)(resources.GetObject("tabSectionSource.TabIndex")));
			this.tabSectionSource.Text = resources.GetString("tabSectionSource.Text");
			this.toolTip1.SetToolTip(this.tabSectionSource, resources.GetString("tabSectionSource.ToolTip"));
			this.tabSectionSource.Visible = ((bool)(resources.GetObject("tabSectionSource.Visible")));
			this.tabSectionSource.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabSectionSource_SendLButtonDblClick);
			// 
			// tabSectionTarget
			// 
			this.tabSectionTarget.AccessibleDescription = resources.GetString("tabSectionTarget.AccessibleDescription");
			this.tabSectionTarget.AccessibleName = resources.GetString("tabSectionTarget.AccessibleName");
			this.tabSectionTarget.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("tabSectionTarget.Anchor")));
			this.tabSectionTarget.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabSectionTarget.BackgroundImage")));
			this.tabSectionTarget.ContainingControl = this;
			this.tabSectionTarget.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("tabSectionTarget.Dock")));
			this.tabSectionTarget.Font = ((System.Drawing.Font)(resources.GetObject("tabSectionTarget.Font")));
			this.tabSectionTarget.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("tabSectionTarget.ImeMode")));
			this.tabSectionTarget.Location = ((System.Drawing.Point)(resources.GetObject("tabSectionTarget.Location")));
			this.tabSectionTarget.Name = "tabSectionTarget";
			this.tabSectionTarget.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabSectionTarget.OcxState")));
			this.tabSectionTarget.RightToLeft = ((bool)(resources.GetObject("tabSectionTarget.RightToLeft")));
			this.tabSectionTarget.Size = ((System.Drawing.Size)(resources.GetObject("tabSectionTarget.Size")));
			this.tabSectionTarget.TabIndex = ((int)(resources.GetObject("tabSectionTarget.TabIndex")));
			this.tabSectionTarget.Text = resources.GetString("tabSectionTarget.Text");
			this.toolTip1.SetToolTip(this.tabSectionTarget, resources.GetString("tabSectionTarget.ToolTip"));
			this.tabSectionTarget.Visible = ((bool)(resources.GetObject("tabSectionTarget.Visible")));
			this.tabSectionTarget.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabSectionTarget_SendLButtonDblClick);
			// 
			// imageListArrows
			// 
			this.imageListArrows.ImageSize = ((System.Drawing.Size)(resources.GetObject("imageListArrows.ImageSize")));
			this.imageListArrows.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListArrows.ImageStream")));
			this.imageListArrows.TransparentColor = System.Drawing.Color.White;
			// 
			// btnSectionSource
			// 
			this.btnSectionSource.AccessibleDescription = resources.GetString("btnSectionSource.AccessibleDescription");
			this.btnSectionSource.AccessibleName = resources.GetString("btnSectionSource.AccessibleName");
			this.btnSectionSource.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnSectionSource.Anchor")));
			this.btnSectionSource.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSectionSource.BackgroundImage")));
			this.btnSectionSource.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnSectionSource.Dock")));
			this.btnSectionSource.Enabled = ((bool)(resources.GetObject("btnSectionSource.Enabled")));
			this.btnSectionSource.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnSectionSource.FlatStyle")));
			this.btnSectionSource.Font = ((System.Drawing.Font)(resources.GetObject("btnSectionSource.Font")));
			this.btnSectionSource.Image = ((System.Drawing.Image)(resources.GetObject("btnSectionSource.Image")));
			this.btnSectionSource.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnSectionSource.ImageAlign")));
			this.btnSectionSource.ImageIndex = ((int)(resources.GetObject("btnSectionSource.ImageIndex")));
			this.btnSectionSource.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnSectionSource.ImeMode")));
			this.btnSectionSource.Location = ((System.Drawing.Point)(resources.GetObject("btnSectionSource.Location")));
			this.btnSectionSource.Name = "btnSectionSource";
			this.btnSectionSource.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnSectionSource.RightToLeft")));
			this.btnSectionSource.Size = ((System.Drawing.Size)(resources.GetObject("btnSectionSource.Size")));
			this.btnSectionSource.TabIndex = ((int)(resources.GetObject("btnSectionSource.TabIndex")));
			this.btnSectionSource.Text = resources.GetString("btnSectionSource.Text");
			this.btnSectionSource.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnSectionSource.TextAlign")));
			this.toolTip1.SetToolTip(this.btnSectionSource, resources.GetString("btnSectionSource.ToolTip"));
			this.btnSectionSource.Visible = ((bool)(resources.GetObject("btnSectionSource.Visible")));
			this.btnSectionSource.Click += new System.EventHandler(this.btnSectionSource_Click);
			// 
			// bntSectionTarget
			// 
			this.bntSectionTarget.AccessibleDescription = resources.GetString("bntSectionTarget.AccessibleDescription");
			this.bntSectionTarget.AccessibleName = resources.GetString("bntSectionTarget.AccessibleName");
			this.bntSectionTarget.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("bntSectionTarget.Anchor")));
			this.bntSectionTarget.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bntSectionTarget.BackgroundImage")));
			this.bntSectionTarget.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("bntSectionTarget.Dock")));
			this.bntSectionTarget.Enabled = ((bool)(resources.GetObject("bntSectionTarget.Enabled")));
			this.bntSectionTarget.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("bntSectionTarget.FlatStyle")));
			this.bntSectionTarget.Font = ((System.Drawing.Font)(resources.GetObject("bntSectionTarget.Font")));
			this.bntSectionTarget.Image = ((System.Drawing.Image)(resources.GetObject("bntSectionTarget.Image")));
			this.bntSectionTarget.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("bntSectionTarget.ImageAlign")));
			this.bntSectionTarget.ImageIndex = ((int)(resources.GetObject("bntSectionTarget.ImageIndex")));
			this.bntSectionTarget.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("bntSectionTarget.ImeMode")));
			this.bntSectionTarget.Location = ((System.Drawing.Point)(resources.GetObject("bntSectionTarget.Location")));
			this.bntSectionTarget.Name = "bntSectionTarget";
			this.bntSectionTarget.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("bntSectionTarget.RightToLeft")));
			this.bntSectionTarget.Size = ((System.Drawing.Size)(resources.GetObject("bntSectionTarget.Size")));
			this.bntSectionTarget.TabIndex = ((int)(resources.GetObject("bntSectionTarget.TabIndex")));
			this.bntSectionTarget.Text = resources.GetString("bntSectionTarget.Text");
			this.bntSectionTarget.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("bntSectionTarget.TextAlign")));
			this.toolTip1.SetToolTip(this.bntSectionTarget, resources.GetString("bntSectionTarget.ToolTip"));
			this.bntSectionTarget.Visible = ((bool)(resources.GetObject("bntSectionTarget.Visible")));
			this.bntSectionTarget.Click += new System.EventHandler(this.bntSectionTarget_Click);
			// 
			// btnAirlineTarget
			// 
			this.btnAirlineTarget.AccessibleDescription = resources.GetString("btnAirlineTarget.AccessibleDescription");
			this.btnAirlineTarget.AccessibleName = resources.GetString("btnAirlineTarget.AccessibleName");
			this.btnAirlineTarget.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnAirlineTarget.Anchor")));
			this.btnAirlineTarget.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAirlineTarget.BackgroundImage")));
			this.btnAirlineTarget.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnAirlineTarget.Dock")));
			this.btnAirlineTarget.Enabled = ((bool)(resources.GetObject("btnAirlineTarget.Enabled")));
			this.btnAirlineTarget.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnAirlineTarget.FlatStyle")));
			this.btnAirlineTarget.Font = ((System.Drawing.Font)(resources.GetObject("btnAirlineTarget.Font")));
			this.btnAirlineTarget.Image = ((System.Drawing.Image)(resources.GetObject("btnAirlineTarget.Image")));
			this.btnAirlineTarget.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnAirlineTarget.ImageAlign")));
			this.btnAirlineTarget.ImageIndex = ((int)(resources.GetObject("btnAirlineTarget.ImageIndex")));
			this.btnAirlineTarget.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnAirlineTarget.ImeMode")));
			this.btnAirlineTarget.Location = ((System.Drawing.Point)(resources.GetObject("btnAirlineTarget.Location")));
			this.btnAirlineTarget.Name = "btnAirlineTarget";
			this.btnAirlineTarget.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnAirlineTarget.RightToLeft")));
			this.btnAirlineTarget.Size = ((System.Drawing.Size)(resources.GetObject("btnAirlineTarget.Size")));
			this.btnAirlineTarget.TabIndex = ((int)(resources.GetObject("btnAirlineTarget.TabIndex")));
			this.btnAirlineTarget.Text = resources.GetString("btnAirlineTarget.Text");
			this.btnAirlineTarget.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnAirlineTarget.TextAlign")));
			this.toolTip1.SetToolTip(this.btnAirlineTarget, resources.GetString("btnAirlineTarget.ToolTip"));
			this.btnAirlineTarget.Visible = ((bool)(resources.GetObject("btnAirlineTarget.Visible")));
			this.btnAirlineTarget.Click += new System.EventHandler(this.btnAirlineTarget_Click);
			// 
			// btnAirlineSource
			// 
			this.btnAirlineSource.AccessibleDescription = resources.GetString("btnAirlineSource.AccessibleDescription");
			this.btnAirlineSource.AccessibleName = resources.GetString("btnAirlineSource.AccessibleName");
			this.btnAirlineSource.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnAirlineSource.Anchor")));
			this.btnAirlineSource.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAirlineSource.BackgroundImage")));
			this.btnAirlineSource.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnAirlineSource.Dock")));
			this.btnAirlineSource.Enabled = ((bool)(resources.GetObject("btnAirlineSource.Enabled")));
			this.btnAirlineSource.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnAirlineSource.FlatStyle")));
			this.btnAirlineSource.Font = ((System.Drawing.Font)(resources.GetObject("btnAirlineSource.Font")));
			this.btnAirlineSource.Image = ((System.Drawing.Image)(resources.GetObject("btnAirlineSource.Image")));
			this.btnAirlineSource.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnAirlineSource.ImageAlign")));
			this.btnAirlineSource.ImageIndex = ((int)(resources.GetObject("btnAirlineSource.ImageIndex")));
			this.btnAirlineSource.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnAirlineSource.ImeMode")));
			this.btnAirlineSource.Location = ((System.Drawing.Point)(resources.GetObject("btnAirlineSource.Location")));
			this.btnAirlineSource.Name = "btnAirlineSource";
			this.btnAirlineSource.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnAirlineSource.RightToLeft")));
			this.btnAirlineSource.Size = ((System.Drawing.Size)(resources.GetObject("btnAirlineSource.Size")));
			this.btnAirlineSource.TabIndex = ((int)(resources.GetObject("btnAirlineSource.TabIndex")));
			this.btnAirlineSource.Text = resources.GetString("btnAirlineSource.Text");
			this.btnAirlineSource.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnAirlineSource.TextAlign")));
			this.toolTip1.SetToolTip(this.btnAirlineSource, resources.GetString("btnAirlineSource.ToolTip"));
			this.btnAirlineSource.Visible = ((bool)(resources.GetObject("btnAirlineSource.Visible")));
			this.btnAirlineSource.Click += new System.EventHandler(this.btnAirlineSource_Click);
			// 
			// tabAirlineTarget
			// 
			this.tabAirlineTarget.AccessibleDescription = resources.GetString("tabAirlineTarget.AccessibleDescription");
			this.tabAirlineTarget.AccessibleName = resources.GetString("tabAirlineTarget.AccessibleName");
			this.tabAirlineTarget.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("tabAirlineTarget.Anchor")));
			this.tabAirlineTarget.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabAirlineTarget.BackgroundImage")));
			this.tabAirlineTarget.ContainingControl = this;
			this.tabAirlineTarget.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("tabAirlineTarget.Dock")));
			this.tabAirlineTarget.Font = ((System.Drawing.Font)(resources.GetObject("tabAirlineTarget.Font")));
			this.tabAirlineTarget.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("tabAirlineTarget.ImeMode")));
			this.tabAirlineTarget.Location = ((System.Drawing.Point)(resources.GetObject("tabAirlineTarget.Location")));
			this.tabAirlineTarget.Name = "tabAirlineTarget";
			this.tabAirlineTarget.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabAirlineTarget.OcxState")));
			this.tabAirlineTarget.RightToLeft = ((bool)(resources.GetObject("tabAirlineTarget.RightToLeft")));
			this.tabAirlineTarget.Size = ((System.Drawing.Size)(resources.GetObject("tabAirlineTarget.Size")));
			this.tabAirlineTarget.TabIndex = ((int)(resources.GetObject("tabAirlineTarget.TabIndex")));
			this.tabAirlineTarget.Text = resources.GetString("tabAirlineTarget.Text");
			this.toolTip1.SetToolTip(this.tabAirlineTarget, resources.GetString("tabAirlineTarget.ToolTip"));
			this.tabAirlineTarget.Visible = ((bool)(resources.GetObject("tabAirlineTarget.Visible")));
			this.tabAirlineTarget.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabAirlineTarget_SendLButtonDblClick);
			// 
			// tabAirlineSource
			// 
			this.tabAirlineSource.AccessibleDescription = resources.GetString("tabAirlineSource.AccessibleDescription");
			this.tabAirlineSource.AccessibleName = resources.GetString("tabAirlineSource.AccessibleName");
			this.tabAirlineSource.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("tabAirlineSource.Anchor")));
			this.tabAirlineSource.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabAirlineSource.BackgroundImage")));
			this.tabAirlineSource.ContainingControl = this;
			this.tabAirlineSource.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("tabAirlineSource.Dock")));
			this.tabAirlineSource.Font = ((System.Drawing.Font)(resources.GetObject("tabAirlineSource.Font")));
			this.tabAirlineSource.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("tabAirlineSource.ImeMode")));
			this.tabAirlineSource.Location = ((System.Drawing.Point)(resources.GetObject("tabAirlineSource.Location")));
			this.tabAirlineSource.Name = "tabAirlineSource";
			this.tabAirlineSource.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabAirlineSource.OcxState")));
			this.tabAirlineSource.RightToLeft = ((bool)(resources.GetObject("tabAirlineSource.RightToLeft")));
			this.tabAirlineSource.Size = ((System.Drawing.Size)(resources.GetObject("tabAirlineSource.Size")));
			this.tabAirlineSource.TabIndex = ((int)(resources.GetObject("tabAirlineSource.TabIndex")));
			this.tabAirlineSource.Text = resources.GetString("tabAirlineSource.Text");
			this.toolTip1.SetToolTip(this.tabAirlineSource, resources.GetString("tabAirlineSource.ToolTip"));
			this.tabAirlineSource.Visible = ((bool)(resources.GetObject("tabAirlineSource.Visible")));
			this.tabAirlineSource.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabAirlineSource_SendLButtonDblClick);
			// 
			// picAirlines
			// 
			this.picAirlines.AccessibleDescription = resources.GetString("picAirlines.AccessibleDescription");
			this.picAirlines.AccessibleName = resources.GetString("picAirlines.AccessibleName");
			this.picAirlines.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("picAirlines.Anchor")));
			this.picAirlines.BackColor = System.Drawing.Color.Transparent;
			this.picAirlines.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("picAirlines.BackgroundImage")));
			this.picAirlines.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("picAirlines.Dock")));
			this.picAirlines.Enabled = ((bool)(resources.GetObject("picAirlines.Enabled")));
			this.picAirlines.Font = ((System.Drawing.Font)(resources.GetObject("picAirlines.Font")));
			this.picAirlines.Image = ((System.Drawing.Image)(resources.GetObject("picAirlines.Image")));
			this.picAirlines.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("picAirlines.ImeMode")));
			this.picAirlines.Location = ((System.Drawing.Point)(resources.GetObject("picAirlines.Location")));
			this.picAirlines.Name = "picAirlines";
			this.picAirlines.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("picAirlines.RightToLeft")));
			this.picAirlines.Size = ((System.Drawing.Size)(resources.GetObject("picAirlines.Size")));
			this.picAirlines.SizeMode = ((System.Windows.Forms.PictureBoxSizeMode)(resources.GetObject("picAirlines.SizeMode")));
			this.picAirlines.TabIndex = ((int)(resources.GetObject("picAirlines.TabIndex")));
			this.picAirlines.TabStop = false;
			this.picAirlines.Text = resources.GetString("picAirlines.Text");
			this.toolTip1.SetToolTip(this.picAirlines, resources.GetString("picAirlines.ToolTip"));
			this.picAirlines.Visible = ((bool)(resources.GetObject("picAirlines.Visible")));
			this.picAirlines.Paint += new System.Windows.Forms.PaintEventHandler(this.picAirlines_Paint);
			// 
			// btnNatureTarget
			// 
			this.btnNatureTarget.AccessibleDescription = resources.GetString("btnNatureTarget.AccessibleDescription");
			this.btnNatureTarget.AccessibleName = resources.GetString("btnNatureTarget.AccessibleName");
			this.btnNatureTarget.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnNatureTarget.Anchor")));
			this.btnNatureTarget.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNatureTarget.BackgroundImage")));
			this.btnNatureTarget.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnNatureTarget.Dock")));
			this.btnNatureTarget.Enabled = ((bool)(resources.GetObject("btnNatureTarget.Enabled")));
			this.btnNatureTarget.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnNatureTarget.FlatStyle")));
			this.btnNatureTarget.Font = ((System.Drawing.Font)(resources.GetObject("btnNatureTarget.Font")));
			this.btnNatureTarget.Image = ((System.Drawing.Image)(resources.GetObject("btnNatureTarget.Image")));
			this.btnNatureTarget.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnNatureTarget.ImageAlign")));
			this.btnNatureTarget.ImageIndex = ((int)(resources.GetObject("btnNatureTarget.ImageIndex")));
			this.btnNatureTarget.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnNatureTarget.ImeMode")));
			this.btnNatureTarget.Location = ((System.Drawing.Point)(resources.GetObject("btnNatureTarget.Location")));
			this.btnNatureTarget.Name = "btnNatureTarget";
			this.btnNatureTarget.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnNatureTarget.RightToLeft")));
			this.btnNatureTarget.Size = ((System.Drawing.Size)(resources.GetObject("btnNatureTarget.Size")));
			this.btnNatureTarget.TabIndex = ((int)(resources.GetObject("btnNatureTarget.TabIndex")));
			this.btnNatureTarget.Text = resources.GetString("btnNatureTarget.Text");
			this.btnNatureTarget.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnNatureTarget.TextAlign")));
			this.toolTip1.SetToolTip(this.btnNatureTarget, resources.GetString("btnNatureTarget.ToolTip"));
			this.btnNatureTarget.Visible = ((bool)(resources.GetObject("btnNatureTarget.Visible")));
			this.btnNatureTarget.Click += new System.EventHandler(this.btnNatureTarget_Click);
			// 
			// btnNatureSource
			// 
			this.btnNatureSource.AccessibleDescription = resources.GetString("btnNatureSource.AccessibleDescription");
			this.btnNatureSource.AccessibleName = resources.GetString("btnNatureSource.AccessibleName");
			this.btnNatureSource.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnNatureSource.Anchor")));
			this.btnNatureSource.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNatureSource.BackgroundImage")));
			this.btnNatureSource.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnNatureSource.Dock")));
			this.btnNatureSource.Enabled = ((bool)(resources.GetObject("btnNatureSource.Enabled")));
			this.btnNatureSource.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnNatureSource.FlatStyle")));
			this.btnNatureSource.Font = ((System.Drawing.Font)(resources.GetObject("btnNatureSource.Font")));
			this.btnNatureSource.Image = ((System.Drawing.Image)(resources.GetObject("btnNatureSource.Image")));
			this.btnNatureSource.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnNatureSource.ImageAlign")));
			this.btnNatureSource.ImageIndex = ((int)(resources.GetObject("btnNatureSource.ImageIndex")));
			this.btnNatureSource.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnNatureSource.ImeMode")));
			this.btnNatureSource.Location = ((System.Drawing.Point)(resources.GetObject("btnNatureSource.Location")));
			this.btnNatureSource.Name = "btnNatureSource";
			this.btnNatureSource.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnNatureSource.RightToLeft")));
			this.btnNatureSource.Size = ((System.Drawing.Size)(resources.GetObject("btnNatureSource.Size")));
			this.btnNatureSource.TabIndex = ((int)(resources.GetObject("btnNatureSource.TabIndex")));
			this.btnNatureSource.Text = resources.GetString("btnNatureSource.Text");
			this.btnNatureSource.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnNatureSource.TextAlign")));
			this.toolTip1.SetToolTip(this.btnNatureSource, resources.GetString("btnNatureSource.ToolTip"));
			this.btnNatureSource.Visible = ((bool)(resources.GetObject("btnNatureSource.Visible")));
			this.btnNatureSource.Click += new System.EventHandler(this.btnNatureSource_Click);
			// 
			// tabNatureTarget
			// 
			this.tabNatureTarget.AccessibleDescription = resources.GetString("tabNatureTarget.AccessibleDescription");
			this.tabNatureTarget.AccessibleName = resources.GetString("tabNatureTarget.AccessibleName");
			this.tabNatureTarget.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("tabNatureTarget.Anchor")));
			this.tabNatureTarget.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabNatureTarget.BackgroundImage")));
			this.tabNatureTarget.ContainingControl = this;
			this.tabNatureTarget.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("tabNatureTarget.Dock")));
			this.tabNatureTarget.Font = ((System.Drawing.Font)(resources.GetObject("tabNatureTarget.Font")));
			this.tabNatureTarget.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("tabNatureTarget.ImeMode")));
			this.tabNatureTarget.Location = ((System.Drawing.Point)(resources.GetObject("tabNatureTarget.Location")));
			this.tabNatureTarget.Name = "tabNatureTarget";
			this.tabNatureTarget.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabNatureTarget.OcxState")));
			this.tabNatureTarget.RightToLeft = ((bool)(resources.GetObject("tabNatureTarget.RightToLeft")));
			this.tabNatureTarget.Size = ((System.Drawing.Size)(resources.GetObject("tabNatureTarget.Size")));
			this.tabNatureTarget.TabIndex = ((int)(resources.GetObject("tabNatureTarget.TabIndex")));
			this.tabNatureTarget.Text = resources.GetString("tabNatureTarget.Text");
			this.toolTip1.SetToolTip(this.tabNatureTarget, resources.GetString("tabNatureTarget.ToolTip"));
			this.tabNatureTarget.Visible = ((bool)(resources.GetObject("tabNatureTarget.Visible")));
			this.tabNatureTarget.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabNatureTarget_SendLButtonDblClick);
			// 
			// tabNatureSource
			// 
			this.tabNatureSource.AccessibleDescription = resources.GetString("tabNatureSource.AccessibleDescription");
			this.tabNatureSource.AccessibleName = resources.GetString("tabNatureSource.AccessibleName");
			this.tabNatureSource.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("tabNatureSource.Anchor")));
			this.tabNatureSource.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabNatureSource.BackgroundImage")));
			this.tabNatureSource.ContainingControl = this;
			this.tabNatureSource.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("tabNatureSource.Dock")));
			this.tabNatureSource.Font = ((System.Drawing.Font)(resources.GetObject("tabNatureSource.Font")));
			this.tabNatureSource.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("tabNatureSource.ImeMode")));
			this.tabNatureSource.Location = ((System.Drawing.Point)(resources.GetObject("tabNatureSource.Location")));
			this.tabNatureSource.Name = "tabNatureSource";
			this.tabNatureSource.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabNatureSource.OcxState")));
			this.tabNatureSource.RightToLeft = ((bool)(resources.GetObject("tabNatureSource.RightToLeft")));
			this.tabNatureSource.Size = ((System.Drawing.Size)(resources.GetObject("tabNatureSource.Size")));
			this.tabNatureSource.TabIndex = ((int)(resources.GetObject("tabNatureSource.TabIndex")));
			this.tabNatureSource.Text = resources.GetString("tabNatureSource.Text");
			this.toolTip1.SetToolTip(this.tabNatureSource, resources.GetString("tabNatureSource.ToolTip"));
			this.tabNatureSource.Visible = ((bool)(resources.GetObject("tabNatureSource.Visible")));
			this.tabNatureSource.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabNatureSource_SendLButtonDblClick);
			// 
			// picNatures
			// 
			this.picNatures.AccessibleDescription = resources.GetString("picNatures.AccessibleDescription");
			this.picNatures.AccessibleName = resources.GetString("picNatures.AccessibleName");
			this.picNatures.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("picNatures.Anchor")));
			this.picNatures.BackColor = System.Drawing.Color.Transparent;
			this.picNatures.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("picNatures.BackgroundImage")));
			this.picNatures.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("picNatures.Dock")));
			this.picNatures.Enabled = ((bool)(resources.GetObject("picNatures.Enabled")));
			this.picNatures.Font = ((System.Drawing.Font)(resources.GetObject("picNatures.Font")));
			this.picNatures.Image = ((System.Drawing.Image)(resources.GetObject("picNatures.Image")));
			this.picNatures.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("picNatures.ImeMode")));
			this.picNatures.Location = ((System.Drawing.Point)(resources.GetObject("picNatures.Location")));
			this.picNatures.Name = "picNatures";
			this.picNatures.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("picNatures.RightToLeft")));
			this.picNatures.Size = ((System.Drawing.Size)(resources.GetObject("picNatures.Size")));
			this.picNatures.SizeMode = ((System.Windows.Forms.PictureBoxSizeMode)(resources.GetObject("picNatures.SizeMode")));
			this.picNatures.TabIndex = ((int)(resources.GetObject("picNatures.TabIndex")));
			this.picNatures.TabStop = false;
			this.picNatures.Text = resources.GetString("picNatures.Text");
			this.toolTip1.SetToolTip(this.picNatures, resources.GetString("picNatures.ToolTip"));
			this.picNatures.Visible = ((bool)(resources.GetObject("picNatures.Visible")));
			this.picNatures.Paint += new System.Windows.Forms.PaintEventHandler(this.picNatures_Paint);
			// 
			// cbViewName
			// 
			this.cbViewName.AccessibleDescription = resources.GetString("cbViewName.AccessibleDescription");
			this.cbViewName.AccessibleName = resources.GetString("cbViewName.AccessibleName");
			this.cbViewName.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cbViewName.Anchor")));
			this.cbViewName.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbViewName.BackgroundImage")));
			this.cbViewName.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cbViewName.Dock")));
			this.cbViewName.Enabled = ((bool)(resources.GetObject("cbViewName.Enabled")));
			this.cbViewName.Font = ((System.Drawing.Font)(resources.GetObject("cbViewName.Font")));
			this.cbViewName.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cbViewName.ImeMode")));
			this.cbViewName.IntegralHeight = ((bool)(resources.GetObject("cbViewName.IntegralHeight")));
			this.cbViewName.ItemHeight = ((int)(resources.GetObject("cbViewName.ItemHeight")));
			this.cbViewName.Location = ((System.Drawing.Point)(resources.GetObject("cbViewName.Location")));
			this.cbViewName.MaxDropDownItems = ((int)(resources.GetObject("cbViewName.MaxDropDownItems")));
			this.cbViewName.MaxLength = ((int)(resources.GetObject("cbViewName.MaxLength")));
			this.cbViewName.Name = "cbViewName";
			this.cbViewName.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cbViewName.RightToLeft")));
			this.cbViewName.Size = ((System.Drawing.Size)(resources.GetObject("cbViewName.Size")));
			this.cbViewName.TabIndex = ((int)(resources.GetObject("cbViewName.TabIndex")));
			this.cbViewName.Text = resources.GetString("cbViewName.Text");
			this.toolTip1.SetToolTip(this.cbViewName, resources.GetString("cbViewName.ToolTip"));
			this.cbViewName.Visible = ((bool)(resources.GetObject("cbViewName.Visible")));
			this.cbViewName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbViewName_KeyPress);
			this.cbViewName.TextChanged += new System.EventHandler(this.cbViewName_TextChanged);
			this.cbViewName.SelectedValueChanged += new System.EventHandler(this.cbViewName_SelectedValueChanged);
			// 
			// lblViewName
			// 
			this.lblViewName.AccessibleDescription = resources.GetString("lblViewName.AccessibleDescription");
			this.lblViewName.AccessibleName = resources.GetString("lblViewName.AccessibleName");
			this.lblViewName.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblViewName.Anchor")));
			this.lblViewName.AutoSize = ((bool)(resources.GetObject("lblViewName.AutoSize")));
			this.lblViewName.BackColor = System.Drawing.Color.Transparent;
			this.lblViewName.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblViewName.Dock")));
			this.lblViewName.Enabled = ((bool)(resources.GetObject("lblViewName.Enabled")));
			this.lblViewName.Font = ((System.Drawing.Font)(resources.GetObject("lblViewName.Font")));
			this.lblViewName.Image = ((System.Drawing.Image)(resources.GetObject("lblViewName.Image")));
			this.lblViewName.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblViewName.ImageAlign")));
			this.lblViewName.ImageIndex = ((int)(resources.GetObject("lblViewName.ImageIndex")));
			this.lblViewName.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblViewName.ImeMode")));
			this.lblViewName.Location = ((System.Drawing.Point)(resources.GetObject("lblViewName.Location")));
			this.lblViewName.Name = "lblViewName";
			this.lblViewName.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblViewName.RightToLeft")));
			this.lblViewName.Size = ((System.Drawing.Size)(resources.GetObject("lblViewName.Size")));
			this.lblViewName.TabIndex = ((int)(resources.GetObject("lblViewName.TabIndex")));
			this.lblViewName.Text = resources.GetString("lblViewName.Text");
			this.lblViewName.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblViewName.TextAlign")));
			this.toolTip1.SetToolTip(this.lblViewName, resources.GetString("lblViewName.ToolTip"));
			this.lblViewName.Visible = ((bool)(resources.GetObject("lblViewName.Visible")));
			// 
			// btnOK
			// 
			this.btnOK.AccessibleDescription = resources.GetString("btnOK.AccessibleDescription");
			this.btnOK.AccessibleName = resources.GetString("btnOK.AccessibleName");
			this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnOK.Anchor")));
			this.btnOK.BackColor = System.Drawing.Color.Transparent;
			this.btnOK.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnOK.BackgroundImage")));
			this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOK.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnOK.Dock")));
			this.btnOK.Enabled = ((bool)(resources.GetObject("btnOK.Enabled")));
			this.btnOK.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnOK.FlatStyle")));
			this.btnOK.Font = ((System.Drawing.Font)(resources.GetObject("btnOK.Font")));
			this.btnOK.Image = ((System.Drawing.Image)(resources.GetObject("btnOK.Image")));
			this.btnOK.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnOK.ImageAlign")));
			this.btnOK.ImageIndex = ((int)(resources.GetObject("btnOK.ImageIndex")));
			this.btnOK.ImageList = this.imageButtons;
			this.btnOK.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnOK.ImeMode")));
			this.btnOK.Location = ((System.Drawing.Point)(resources.GetObject("btnOK.Location")));
			this.btnOK.Name = "btnOK";
			this.btnOK.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnOK.RightToLeft")));
			this.btnOK.Size = ((System.Drawing.Size)(resources.GetObject("btnOK.Size")));
			this.btnOK.TabIndex = ((int)(resources.GetObject("btnOK.TabIndex")));
			this.btnOK.Text = resources.GetString("btnOK.Text");
			this.btnOK.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnOK.TextAlign")));
			this.toolTip1.SetToolTip(this.btnOK, resources.GetString("btnOK.ToolTip"));
			this.btnOK.Visible = ((bool)(resources.GetObject("btnOK.Visible")));
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// imageButtons
			// 
			this.imageButtons.ImageSize = ((System.Drawing.Size)(resources.GetObject("imageButtons.ImageSize")));
			this.imageButtons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageButtons.ImageStream")));
			this.imageButtons.TransparentColor = System.Drawing.Color.White;
			// 
			// btnCancel
			// 
			this.btnCancel.AccessibleDescription = resources.GetString("btnCancel.AccessibleDescription");
			this.btnCancel.AccessibleName = resources.GetString("btnCancel.AccessibleName");
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnCancel.Anchor")));
			this.btnCancel.BackColor = System.Drawing.Color.Transparent;
			this.btnCancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.BackgroundImage")));
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnCancel.Dock")));
			this.btnCancel.Enabled = ((bool)(resources.GetObject("btnCancel.Enabled")));
			this.btnCancel.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnCancel.FlatStyle")));
			this.btnCancel.Font = ((System.Drawing.Font)(resources.GetObject("btnCancel.Font")));
			this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
			this.btnCancel.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnCancel.ImageAlign")));
			this.btnCancel.ImageIndex = ((int)(resources.GetObject("btnCancel.ImageIndex")));
			this.btnCancel.ImageList = this.imageButtons;
			this.btnCancel.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnCancel.ImeMode")));
			this.btnCancel.Location = ((System.Drawing.Point)(resources.GetObject("btnCancel.Location")));
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnCancel.RightToLeft")));
			this.btnCancel.Size = ((System.Drawing.Size)(resources.GetObject("btnCancel.Size")));
			this.btnCancel.TabIndex = ((int)(resources.GetObject("btnCancel.TabIndex")));
			this.btnCancel.Text = resources.GetString("btnCancel.Text");
			this.btnCancel.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnCancel.TextAlign")));
			this.toolTip1.SetToolTip(this.btnCancel, resources.GetString("btnCancel.ToolTip"));
			this.btnCancel.Visible = ((bool)(resources.GetObject("btnCancel.Visible")));
			// 
			// panelBody
			// 
			this.panelBody.AccessibleDescription = resources.GetString("panelBody.AccessibleDescription");
			this.panelBody.AccessibleName = resources.GetString("panelBody.AccessibleName");
			this.panelBody.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("panelBody.Anchor")));
			this.panelBody.AutoScroll = ((bool)(resources.GetObject("panelBody.AutoScroll")));
			this.panelBody.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("panelBody.AutoScrollMargin")));
			this.panelBody.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("panelBody.AutoScrollMinSize")));
			this.panelBody.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelBody.BackgroundImage")));
			this.panelBody.Controls.Add(this.panelButtons);
			this.panelBody.Controls.Add(this.panelNatures);
			this.panelBody.Controls.Add(this.panelAirlines);
			this.panelBody.Controls.Add(this.panelStatusSections);
			this.panelBody.Controls.Add(this.dtFrom);
			this.panelBody.Controls.Add(this.dtTo);
			this.panelBody.Controls.Add(this.lblTo);
			this.panelBody.Controls.Add(this.lblFrom);
			this.panelBody.Controls.Add(this.lblViewName);
			this.panelBody.Controls.Add(this.cbViewName);
			this.panelBody.Controls.Add(this.picBody);
			this.panelBody.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("panelBody.Dock")));
			this.panelBody.Enabled = ((bool)(resources.GetObject("panelBody.Enabled")));
			this.panelBody.Font = ((System.Drawing.Font)(resources.GetObject("panelBody.Font")));
			this.panelBody.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("panelBody.ImeMode")));
			this.panelBody.Location = ((System.Drawing.Point)(resources.GetObject("panelBody.Location")));
			this.panelBody.Name = "panelBody";
			this.panelBody.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("panelBody.RightToLeft")));
			this.panelBody.Size = ((System.Drawing.Size)(resources.GetObject("panelBody.Size")));
			this.panelBody.TabIndex = ((int)(resources.GetObject("panelBody.TabIndex")));
			this.panelBody.Text = resources.GetString("panelBody.Text");
			this.toolTip1.SetToolTip(this.panelBody, resources.GetString("panelBody.ToolTip"));
			this.panelBody.Visible = ((bool)(resources.GetObject("panelBody.Visible")));
			// 
			// panelButtons
			// 
			this.panelButtons.AccessibleDescription = resources.GetString("panelButtons.AccessibleDescription");
			this.panelButtons.AccessibleName = resources.GetString("panelButtons.AccessibleName");
			this.panelButtons.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("panelButtons.Anchor")));
			this.panelButtons.AutoScroll = ((bool)(resources.GetObject("panelButtons.AutoScroll")));
			this.panelButtons.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("panelButtons.AutoScrollMargin")));
			this.panelButtons.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("panelButtons.AutoScrollMinSize")));
			this.panelButtons.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelButtons.BackgroundImage")));
			this.panelButtons.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelButtons.Controls.Add(this.lblCurrentActiveView);
			this.panelButtons.Controls.Add(this.btnSave);
			this.panelButtons.Controls.Add(this.btnCancel);
			this.panelButtons.Controls.Add(this.btnNew);
			this.panelButtons.Controls.Add(this.btnDelete);
			this.panelButtons.Controls.Add(this.btnOK);
			this.panelButtons.Controls.Add(this.picButtons);
			this.panelButtons.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("panelButtons.Dock")));
			this.panelButtons.Enabled = ((bool)(resources.GetObject("panelButtons.Enabled")));
			this.panelButtons.Font = ((System.Drawing.Font)(resources.GetObject("panelButtons.Font")));
			this.panelButtons.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("panelButtons.ImeMode")));
			this.panelButtons.Location = ((System.Drawing.Point)(resources.GetObject("panelButtons.Location")));
			this.panelButtons.Name = "panelButtons";
			this.panelButtons.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("panelButtons.RightToLeft")));
			this.panelButtons.Size = ((System.Drawing.Size)(resources.GetObject("panelButtons.Size")));
			this.panelButtons.TabIndex = ((int)(resources.GetObject("panelButtons.TabIndex")));
			this.panelButtons.Text = resources.GetString("panelButtons.Text");
			this.toolTip1.SetToolTip(this.panelButtons, resources.GetString("panelButtons.ToolTip"));
			this.panelButtons.Visible = ((bool)(resources.GetObject("panelButtons.Visible")));
			// 
			// lblCurrentActiveView
			// 
			this.lblCurrentActiveView.AccessibleDescription = resources.GetString("lblCurrentActiveView.AccessibleDescription");
			this.lblCurrentActiveView.AccessibleName = resources.GetString("lblCurrentActiveView.AccessibleName");
			this.lblCurrentActiveView.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblCurrentActiveView.Anchor")));
			this.lblCurrentActiveView.AutoSize = ((bool)(resources.GetObject("lblCurrentActiveView.AutoSize")));
			this.lblCurrentActiveView.BackColor = System.Drawing.Color.Transparent;
			this.lblCurrentActiveView.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblCurrentActiveView.Dock")));
			this.lblCurrentActiveView.Enabled = ((bool)(resources.GetObject("lblCurrentActiveView.Enabled")));
			this.lblCurrentActiveView.Font = ((System.Drawing.Font)(resources.GetObject("lblCurrentActiveView.Font")));
			this.lblCurrentActiveView.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lblCurrentActiveView.Image = ((System.Drawing.Image)(resources.GetObject("lblCurrentActiveView.Image")));
			this.lblCurrentActiveView.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblCurrentActiveView.ImageAlign")));
			this.lblCurrentActiveView.ImageIndex = ((int)(resources.GetObject("lblCurrentActiveView.ImageIndex")));
			this.lblCurrentActiveView.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblCurrentActiveView.ImeMode")));
			this.lblCurrentActiveView.Location = ((System.Drawing.Point)(resources.GetObject("lblCurrentActiveView.Location")));
			this.lblCurrentActiveView.Name = "lblCurrentActiveView";
			this.lblCurrentActiveView.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblCurrentActiveView.RightToLeft")));
			this.lblCurrentActiveView.Size = ((System.Drawing.Size)(resources.GetObject("lblCurrentActiveView.Size")));
			this.lblCurrentActiveView.TabIndex = ((int)(resources.GetObject("lblCurrentActiveView.TabIndex")));
			this.lblCurrentActiveView.Text = resources.GetString("lblCurrentActiveView.Text");
			this.lblCurrentActiveView.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblCurrentActiveView.TextAlign")));
			this.toolTip1.SetToolTip(this.lblCurrentActiveView, resources.GetString("lblCurrentActiveView.ToolTip"));
			this.lblCurrentActiveView.Visible = ((bool)(resources.GetObject("lblCurrentActiveView.Visible")));
			// 
			// btnSave
			// 
			this.btnSave.AccessibleDescription = resources.GetString("btnSave.AccessibleDescription");
			this.btnSave.AccessibleName = resources.GetString("btnSave.AccessibleName");
			this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnSave.Anchor")));
			this.btnSave.BackColor = System.Drawing.Color.Transparent;
			this.btnSave.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSave.BackgroundImage")));
			this.btnSave.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnSave.Dock")));
			this.btnSave.Enabled = ((bool)(resources.GetObject("btnSave.Enabled")));
			this.btnSave.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnSave.FlatStyle")));
			this.btnSave.Font = ((System.Drawing.Font)(resources.GetObject("btnSave.Font")));
			this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
			this.btnSave.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnSave.ImageAlign")));
			this.btnSave.ImageIndex = ((int)(resources.GetObject("btnSave.ImageIndex")));
			this.btnSave.ImageList = this.imageButtons;
			this.btnSave.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnSave.ImeMode")));
			this.btnSave.Location = ((System.Drawing.Point)(resources.GetObject("btnSave.Location")));
			this.btnSave.Name = "btnSave";
			this.btnSave.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnSave.RightToLeft")));
			this.btnSave.Size = ((System.Drawing.Size)(resources.GetObject("btnSave.Size")));
			this.btnSave.TabIndex = ((int)(resources.GetObject("btnSave.TabIndex")));
			this.btnSave.Text = resources.GetString("btnSave.Text");
			this.btnSave.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnSave.TextAlign")));
			this.toolTip1.SetToolTip(this.btnSave, resources.GetString("btnSave.ToolTip"));
			this.btnSave.Visible = ((bool)(resources.GetObject("btnSave.Visible")));
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// btnNew
			// 
			this.btnNew.AccessibleDescription = resources.GetString("btnNew.AccessibleDescription");
			this.btnNew.AccessibleName = resources.GetString("btnNew.AccessibleName");
			this.btnNew.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnNew.Anchor")));
			this.btnNew.BackColor = System.Drawing.Color.Transparent;
			this.btnNew.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNew.BackgroundImage")));
			this.btnNew.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnNew.Dock")));
			this.btnNew.Enabled = ((bool)(resources.GetObject("btnNew.Enabled")));
			this.btnNew.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnNew.FlatStyle")));
			this.btnNew.Font = ((System.Drawing.Font)(resources.GetObject("btnNew.Font")));
			this.btnNew.Image = ((System.Drawing.Image)(resources.GetObject("btnNew.Image")));
			this.btnNew.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnNew.ImageAlign")));
			this.btnNew.ImageIndex = ((int)(resources.GetObject("btnNew.ImageIndex")));
			this.btnNew.ImageList = this.imageButtons;
			this.btnNew.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnNew.ImeMode")));
			this.btnNew.Location = ((System.Drawing.Point)(resources.GetObject("btnNew.Location")));
			this.btnNew.Name = "btnNew";
			this.btnNew.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnNew.RightToLeft")));
			this.btnNew.Size = ((System.Drawing.Size)(resources.GetObject("btnNew.Size")));
			this.btnNew.TabIndex = ((int)(resources.GetObject("btnNew.TabIndex")));
			this.btnNew.Text = resources.GetString("btnNew.Text");
			this.btnNew.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnNew.TextAlign")));
			this.toolTip1.SetToolTip(this.btnNew, resources.GetString("btnNew.ToolTip"));
			this.btnNew.Visible = ((bool)(resources.GetObject("btnNew.Visible")));
			this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
			// 
			// btnDelete
			// 
			this.btnDelete.AccessibleDescription = resources.GetString("btnDelete.AccessibleDescription");
			this.btnDelete.AccessibleName = resources.GetString("btnDelete.AccessibleName");
			this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnDelete.Anchor")));
			this.btnDelete.BackColor = System.Drawing.Color.Transparent;
			this.btnDelete.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDelete.BackgroundImage")));
			this.btnDelete.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnDelete.Dock")));
			this.btnDelete.Enabled = ((bool)(resources.GetObject("btnDelete.Enabled")));
			this.btnDelete.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnDelete.FlatStyle")));
			this.btnDelete.Font = ((System.Drawing.Font)(resources.GetObject("btnDelete.Font")));
			this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
			this.btnDelete.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnDelete.ImageAlign")));
			this.btnDelete.ImageIndex = ((int)(resources.GetObject("btnDelete.ImageIndex")));
			this.btnDelete.ImageList = this.imageButtons;
			this.btnDelete.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnDelete.ImeMode")));
			this.btnDelete.Location = ((System.Drawing.Point)(resources.GetObject("btnDelete.Location")));
			this.btnDelete.Name = "btnDelete";
			this.btnDelete.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnDelete.RightToLeft")));
			this.btnDelete.Size = ((System.Drawing.Size)(resources.GetObject("btnDelete.Size")));
			this.btnDelete.TabIndex = ((int)(resources.GetObject("btnDelete.TabIndex")));
			this.btnDelete.Text = resources.GetString("btnDelete.Text");
			this.btnDelete.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnDelete.TextAlign")));
			this.toolTip1.SetToolTip(this.btnDelete, resources.GetString("btnDelete.ToolTip"));
			this.btnDelete.Visible = ((bool)(resources.GetObject("btnDelete.Visible")));
			this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
			// 
			// picButtons
			// 
			this.picButtons.AccessibleDescription = resources.GetString("picButtons.AccessibleDescription");
			this.picButtons.AccessibleName = resources.GetString("picButtons.AccessibleName");
			this.picButtons.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("picButtons.Anchor")));
			this.picButtons.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("picButtons.BackgroundImage")));
			this.picButtons.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("picButtons.Dock")));
			this.picButtons.Enabled = ((bool)(resources.GetObject("picButtons.Enabled")));
			this.picButtons.Font = ((System.Drawing.Font)(resources.GetObject("picButtons.Font")));
			this.picButtons.Image = ((System.Drawing.Image)(resources.GetObject("picButtons.Image")));
			this.picButtons.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("picButtons.ImeMode")));
			this.picButtons.Location = ((System.Drawing.Point)(resources.GetObject("picButtons.Location")));
			this.picButtons.Name = "picButtons";
			this.picButtons.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("picButtons.RightToLeft")));
			this.picButtons.Size = ((System.Drawing.Size)(resources.GetObject("picButtons.Size")));
			this.picButtons.SizeMode = ((System.Windows.Forms.PictureBoxSizeMode)(resources.GetObject("picButtons.SizeMode")));
			this.picButtons.TabIndex = ((int)(resources.GetObject("picButtons.TabIndex")));
			this.picButtons.TabStop = false;
			this.picButtons.Text = resources.GetString("picButtons.Text");
			this.toolTip1.SetToolTip(this.picButtons, resources.GetString("picButtons.ToolTip"));
			this.picButtons.Visible = ((bool)(resources.GetObject("picButtons.Visible")));
			this.picButtons.Paint += new System.Windows.Forms.PaintEventHandler(this.picButtons_Paint);
			// 
			// panelNatures
			// 
			this.panelNatures.AccessibleDescription = resources.GetString("panelNatures.AccessibleDescription");
			this.panelNatures.AccessibleName = resources.GetString("panelNatures.AccessibleName");
			this.panelNatures.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("panelNatures.Anchor")));
			this.panelNatures.AutoScroll = ((bool)(resources.GetObject("panelNatures.AutoScroll")));
			this.panelNatures.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("panelNatures.AutoScrollMargin")));
			this.panelNatures.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("panelNatures.AutoScrollMinSize")));
			this.panelNatures.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelNatures.BackgroundImage")));
			this.panelNatures.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelNatures.Controls.Add(this.cbNatures);
			this.panelNatures.Controls.Add(this.txtNature);
			this.panelNatures.Controls.Add(this.tabNatureSource);
			this.panelNatures.Controls.Add(this.btnNatureSource);
			this.panelNatures.Controls.Add(this.btnNatureTarget);
			this.panelNatures.Controls.Add(this.tabNatureTarget);
			this.panelNatures.Controls.Add(this.picNatures);
			this.panelNatures.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("panelNatures.Dock")));
			this.panelNatures.Enabled = ((bool)(resources.GetObject("panelNatures.Enabled")));
			this.panelNatures.Font = ((System.Drawing.Font)(resources.GetObject("panelNatures.Font")));
			this.panelNatures.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("panelNatures.ImeMode")));
			this.panelNatures.Location = ((System.Drawing.Point)(resources.GetObject("panelNatures.Location")));
			this.panelNatures.Name = "panelNatures";
			this.panelNatures.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("panelNatures.RightToLeft")));
			this.panelNatures.Size = ((System.Drawing.Size)(resources.GetObject("panelNatures.Size")));
			this.panelNatures.TabIndex = ((int)(resources.GetObject("panelNatures.TabIndex")));
			this.panelNatures.Text = resources.GetString("panelNatures.Text");
			this.toolTip1.SetToolTip(this.panelNatures, resources.GetString("panelNatures.ToolTip"));
			this.panelNatures.Visible = ((bool)(resources.GetObject("panelNatures.Visible")));
			// 
			// cbNatures
			// 
			this.cbNatures.AccessibleDescription = resources.GetString("cbNatures.AccessibleDescription");
			this.cbNatures.AccessibleName = resources.GetString("cbNatures.AccessibleName");
			this.cbNatures.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cbNatures.Anchor")));
			this.cbNatures.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("cbNatures.Appearance")));
			this.cbNatures.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbNatures.BackgroundImage")));
			this.cbNatures.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbNatures.CheckAlign")));
			this.cbNatures.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cbNatures.Dock")));
			this.cbNatures.Enabled = ((bool)(resources.GetObject("cbNatures.Enabled")));
			this.cbNatures.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("cbNatures.FlatStyle")));
			this.cbNatures.Font = ((System.Drawing.Font)(resources.GetObject("cbNatures.Font")));
			this.cbNatures.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cbNatures.Image = ((System.Drawing.Image)(resources.GetObject("cbNatures.Image")));
			this.cbNatures.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbNatures.ImageAlign")));
			this.cbNatures.ImageIndex = ((int)(resources.GetObject("cbNatures.ImageIndex")));
			this.cbNatures.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cbNatures.ImeMode")));
			this.cbNatures.Location = ((System.Drawing.Point)(resources.GetObject("cbNatures.Location")));
			this.cbNatures.Name = "cbNatures";
			this.cbNatures.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cbNatures.RightToLeft")));
			this.cbNatures.Size = ((System.Drawing.Size)(resources.GetObject("cbNatures.Size")));
			this.cbNatures.TabIndex = ((int)(resources.GetObject("cbNatures.TabIndex")));
			this.cbNatures.Text = resources.GetString("cbNatures.Text");
			this.cbNatures.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbNatures.TextAlign")));
			this.toolTip1.SetToolTip(this.cbNatures, resources.GetString("cbNatures.ToolTip"));
			this.cbNatures.Visible = ((bool)(resources.GetObject("cbNatures.Visible")));
			// 
			// txtNature
			// 
			this.txtNature.AccessibleDescription = resources.GetString("txtNature.AccessibleDescription");
			this.txtNature.AccessibleName = resources.GetString("txtNature.AccessibleName");
			this.txtNature.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtNature.Anchor")));
			this.txtNature.AutoSize = ((bool)(resources.GetObject("txtNature.AutoSize")));
			this.txtNature.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtNature.BackgroundImage")));
			this.txtNature.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtNature.Dock")));
			this.txtNature.Enabled = ((bool)(resources.GetObject("txtNature.Enabled")));
			this.txtNature.Font = ((System.Drawing.Font)(resources.GetObject("txtNature.Font")));
			this.txtNature.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtNature.ImeMode")));
			this.txtNature.Location = ((System.Drawing.Point)(resources.GetObject("txtNature.Location")));
			this.txtNature.MaxLength = ((int)(resources.GetObject("txtNature.MaxLength")));
			this.txtNature.Multiline = ((bool)(resources.GetObject("txtNature.Multiline")));
			this.txtNature.Name = "txtNature";
			this.txtNature.PasswordChar = ((char)(resources.GetObject("txtNature.PasswordChar")));
			this.txtNature.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtNature.RightToLeft")));
			this.txtNature.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtNature.ScrollBars")));
			this.txtNature.Size = ((System.Drawing.Size)(resources.GetObject("txtNature.Size")));
			this.txtNature.TabIndex = ((int)(resources.GetObject("txtNature.TabIndex")));
			this.txtNature.Text = resources.GetString("txtNature.Text");
			this.txtNature.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtNature.TextAlign")));
			this.toolTip1.SetToolTip(this.txtNature, resources.GetString("txtNature.ToolTip"));
			this.txtNature.Visible = ((bool)(resources.GetObject("txtNature.Visible")));
			this.txtNature.WordWrap = ((bool)(resources.GetObject("txtNature.WordWrap")));
			this.txtNature.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNature_KeyPress);
			this.txtNature.TextChanged += new System.EventHandler(this.txtNature_TextChanged);
			// 
			// panelAirlines
			// 
			this.panelAirlines.AccessibleDescription = resources.GetString("panelAirlines.AccessibleDescription");
			this.panelAirlines.AccessibleName = resources.GetString("panelAirlines.AccessibleName");
			this.panelAirlines.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("panelAirlines.Anchor")));
			this.panelAirlines.AutoScroll = ((bool)(resources.GetObject("panelAirlines.AutoScroll")));
			this.panelAirlines.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("panelAirlines.AutoScrollMargin")));
			this.panelAirlines.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("panelAirlines.AutoScrollMinSize")));
			this.panelAirlines.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelAirlines.BackgroundImage")));
			this.panelAirlines.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelAirlines.Controls.Add(this.cbAirlines);
			this.panelAirlines.Controls.Add(this.txtAirline);
			this.panelAirlines.Controls.Add(this.btnAirlineTarget);
			this.panelAirlines.Controls.Add(this.btnAirlineSource);
			this.panelAirlines.Controls.Add(this.tabAirlineTarget);
			this.panelAirlines.Controls.Add(this.tabAirlineSource);
			this.panelAirlines.Controls.Add(this.picAirlines);
			this.panelAirlines.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("panelAirlines.Dock")));
			this.panelAirlines.Enabled = ((bool)(resources.GetObject("panelAirlines.Enabled")));
			this.panelAirlines.Font = ((System.Drawing.Font)(resources.GetObject("panelAirlines.Font")));
			this.panelAirlines.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("panelAirlines.ImeMode")));
			this.panelAirlines.Location = ((System.Drawing.Point)(resources.GetObject("panelAirlines.Location")));
			this.panelAirlines.Name = "panelAirlines";
			this.panelAirlines.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("panelAirlines.RightToLeft")));
			this.panelAirlines.Size = ((System.Drawing.Size)(resources.GetObject("panelAirlines.Size")));
			this.panelAirlines.TabIndex = ((int)(resources.GetObject("panelAirlines.TabIndex")));
			this.panelAirlines.Text = resources.GetString("panelAirlines.Text");
			this.toolTip1.SetToolTip(this.panelAirlines, resources.GetString("panelAirlines.ToolTip"));
			this.panelAirlines.Visible = ((bool)(resources.GetObject("panelAirlines.Visible")));
			// 
			// cbAirlines
			// 
			this.cbAirlines.AccessibleDescription = resources.GetString("cbAirlines.AccessibleDescription");
			this.cbAirlines.AccessibleName = resources.GetString("cbAirlines.AccessibleName");
			this.cbAirlines.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cbAirlines.Anchor")));
			this.cbAirlines.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("cbAirlines.Appearance")));
			this.cbAirlines.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbAirlines.BackgroundImage")));
			this.cbAirlines.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbAirlines.CheckAlign")));
			this.cbAirlines.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cbAirlines.Dock")));
			this.cbAirlines.Enabled = ((bool)(resources.GetObject("cbAirlines.Enabled")));
			this.cbAirlines.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("cbAirlines.FlatStyle")));
			this.cbAirlines.Font = ((System.Drawing.Font)(resources.GetObject("cbAirlines.Font")));
			this.cbAirlines.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cbAirlines.Image = ((System.Drawing.Image)(resources.GetObject("cbAirlines.Image")));
			this.cbAirlines.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbAirlines.ImageAlign")));
			this.cbAirlines.ImageIndex = ((int)(resources.GetObject("cbAirlines.ImageIndex")));
			this.cbAirlines.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cbAirlines.ImeMode")));
			this.cbAirlines.Location = ((System.Drawing.Point)(resources.GetObject("cbAirlines.Location")));
			this.cbAirlines.Name = "cbAirlines";
			this.cbAirlines.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cbAirlines.RightToLeft")));
			this.cbAirlines.Size = ((System.Drawing.Size)(resources.GetObject("cbAirlines.Size")));
			this.cbAirlines.TabIndex = ((int)(resources.GetObject("cbAirlines.TabIndex")));
			this.cbAirlines.Text = resources.GetString("cbAirlines.Text");
			this.cbAirlines.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbAirlines.TextAlign")));
			this.toolTip1.SetToolTip(this.cbAirlines, resources.GetString("cbAirlines.ToolTip"));
			this.cbAirlines.Visible = ((bool)(resources.GetObject("cbAirlines.Visible")));
			// 
			// txtAirline
			// 
			this.txtAirline.AccessibleDescription = resources.GetString("txtAirline.AccessibleDescription");
			this.txtAirline.AccessibleName = resources.GetString("txtAirline.AccessibleName");
			this.txtAirline.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtAirline.Anchor")));
			this.txtAirline.AutoSize = ((bool)(resources.GetObject("txtAirline.AutoSize")));
			this.txtAirline.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtAirline.BackgroundImage")));
			this.txtAirline.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtAirline.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtAirline.Dock")));
			this.txtAirline.Enabled = ((bool)(resources.GetObject("txtAirline.Enabled")));
			this.txtAirline.Font = ((System.Drawing.Font)(resources.GetObject("txtAirline.Font")));
			this.txtAirline.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtAirline.ImeMode")));
			this.txtAirline.Location = ((System.Drawing.Point)(resources.GetObject("txtAirline.Location")));
			this.txtAirline.MaxLength = ((int)(resources.GetObject("txtAirline.MaxLength")));
			this.txtAirline.Multiline = ((bool)(resources.GetObject("txtAirline.Multiline")));
			this.txtAirline.Name = "txtAirline";
			this.txtAirline.PasswordChar = ((char)(resources.GetObject("txtAirline.PasswordChar")));
			this.txtAirline.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtAirline.RightToLeft")));
			this.txtAirline.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtAirline.ScrollBars")));
			this.txtAirline.Size = ((System.Drawing.Size)(resources.GetObject("txtAirline.Size")));
			this.txtAirline.TabIndex = ((int)(resources.GetObject("txtAirline.TabIndex")));
			this.txtAirline.Text = resources.GetString("txtAirline.Text");
			this.txtAirline.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtAirline.TextAlign")));
			this.toolTip1.SetToolTip(this.txtAirline, resources.GetString("txtAirline.ToolTip"));
			this.txtAirline.Visible = ((bool)(resources.GetObject("txtAirline.Visible")));
			this.txtAirline.WordWrap = ((bool)(resources.GetObject("txtAirline.WordWrap")));
			this.txtAirline.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAirline_KeyPress);
			this.txtAirline.TextChanged += new System.EventHandler(this.txtAirline_TextChanged);
			// 
			// panelStatusSections
			// 
			this.panelStatusSections.AccessibleDescription = resources.GetString("panelStatusSections.AccessibleDescription");
			this.panelStatusSections.AccessibleName = resources.GetString("panelStatusSections.AccessibleName");
			this.panelStatusSections.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("panelStatusSections.Anchor")));
			this.panelStatusSections.AutoScroll = ((bool)(resources.GetObject("panelStatusSections.AutoScroll")));
			this.panelStatusSections.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("panelStatusSections.AutoScrollMargin")));
			this.panelStatusSections.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("panelStatusSections.AutoScrollMinSize")));
			this.panelStatusSections.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelStatusSections.BackgroundImage")));
			this.panelStatusSections.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelStatusSections.Controls.Add(this.cbStatueSections);
			this.panelStatusSections.Controls.Add(this.txtSection);
			this.panelStatusSections.Controls.Add(this.tabSectionTarget);
			this.panelStatusSections.Controls.Add(this.btnSectionSource);
			this.panelStatusSections.Controls.Add(this.tabSectionSource);
			this.panelStatusSections.Controls.Add(this.bntSectionTarget);
			this.panelStatusSections.Controls.Add(this.picStatusSectionFilter);
			this.panelStatusSections.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("panelStatusSections.Dock")));
			this.panelStatusSections.Enabled = ((bool)(resources.GetObject("panelStatusSections.Enabled")));
			this.panelStatusSections.Font = ((System.Drawing.Font)(resources.GetObject("panelStatusSections.Font")));
			this.panelStatusSections.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("panelStatusSections.ImeMode")));
			this.panelStatusSections.Location = ((System.Drawing.Point)(resources.GetObject("panelStatusSections.Location")));
			this.panelStatusSections.Name = "panelStatusSections";
			this.panelStatusSections.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("panelStatusSections.RightToLeft")));
			this.panelStatusSections.Size = ((System.Drawing.Size)(resources.GetObject("panelStatusSections.Size")));
			this.panelStatusSections.TabIndex = ((int)(resources.GetObject("panelStatusSections.TabIndex")));
			this.panelStatusSections.Text = resources.GetString("panelStatusSections.Text");
			this.toolTip1.SetToolTip(this.panelStatusSections, resources.GetString("panelStatusSections.ToolTip"));
			this.panelStatusSections.Visible = ((bool)(resources.GetObject("panelStatusSections.Visible")));
			// 
			// cbStatueSections
			// 
			this.cbStatueSections.AccessibleDescription = resources.GetString("cbStatueSections.AccessibleDescription");
			this.cbStatueSections.AccessibleName = resources.GetString("cbStatueSections.AccessibleName");
			this.cbStatueSections.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cbStatueSections.Anchor")));
			this.cbStatueSections.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("cbStatueSections.Appearance")));
			this.cbStatueSections.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbStatueSections.BackgroundImage")));
			this.cbStatueSections.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbStatueSections.CheckAlign")));
			this.cbStatueSections.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cbStatueSections.Dock")));
			this.cbStatueSections.Enabled = ((bool)(resources.GetObject("cbStatueSections.Enabled")));
			this.cbStatueSections.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("cbStatueSections.FlatStyle")));
			this.cbStatueSections.Font = ((System.Drawing.Font)(resources.GetObject("cbStatueSections.Font")));
			this.cbStatueSections.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cbStatueSections.Image = ((System.Drawing.Image)(resources.GetObject("cbStatueSections.Image")));
			this.cbStatueSections.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbStatueSections.ImageAlign")));
			this.cbStatueSections.ImageIndex = ((int)(resources.GetObject("cbStatueSections.ImageIndex")));
			this.cbStatueSections.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cbStatueSections.ImeMode")));
			this.cbStatueSections.Location = ((System.Drawing.Point)(resources.GetObject("cbStatueSections.Location")));
			this.cbStatueSections.Name = "cbStatueSections";
			this.cbStatueSections.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cbStatueSections.RightToLeft")));
			this.cbStatueSections.Size = ((System.Drawing.Size)(resources.GetObject("cbStatueSections.Size")));
			this.cbStatueSections.TabIndex = ((int)(resources.GetObject("cbStatueSections.TabIndex")));
			this.cbStatueSections.Text = resources.GetString("cbStatueSections.Text");
			this.cbStatueSections.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbStatueSections.TextAlign")));
			this.toolTip1.SetToolTip(this.cbStatueSections, resources.GetString("cbStatueSections.ToolTip"));
			this.cbStatueSections.Visible = ((bool)(resources.GetObject("cbStatueSections.Visible")));
			// 
			// txtSection
			// 
			this.txtSection.AccessibleDescription = resources.GetString("txtSection.AccessibleDescription");
			this.txtSection.AccessibleName = resources.GetString("txtSection.AccessibleName");
			this.txtSection.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtSection.Anchor")));
			this.txtSection.AutoSize = ((bool)(resources.GetObject("txtSection.AutoSize")));
			this.txtSection.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtSection.BackgroundImage")));
			this.txtSection.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtSection.Dock")));
			this.txtSection.Enabled = ((bool)(resources.GetObject("txtSection.Enabled")));
			this.txtSection.Font = ((System.Drawing.Font)(resources.GetObject("txtSection.Font")));
			this.txtSection.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtSection.ImeMode")));
			this.txtSection.Location = ((System.Drawing.Point)(resources.GetObject("txtSection.Location")));
			this.txtSection.MaxLength = ((int)(resources.GetObject("txtSection.MaxLength")));
			this.txtSection.Multiline = ((bool)(resources.GetObject("txtSection.Multiline")));
			this.txtSection.Name = "txtSection";
			this.txtSection.PasswordChar = ((char)(resources.GetObject("txtSection.PasswordChar")));
			this.txtSection.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtSection.RightToLeft")));
			this.txtSection.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtSection.ScrollBars")));
			this.txtSection.Size = ((System.Drawing.Size)(resources.GetObject("txtSection.Size")));
			this.txtSection.TabIndex = ((int)(resources.GetObject("txtSection.TabIndex")));
			this.txtSection.Text = resources.GetString("txtSection.Text");
			this.txtSection.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtSection.TextAlign")));
			this.toolTip1.SetToolTip(this.txtSection, resources.GetString("txtSection.ToolTip"));
			this.txtSection.Visible = ((bool)(resources.GetObject("txtSection.Visible")));
			this.txtSection.WordWrap = ((bool)(resources.GetObject("txtSection.WordWrap")));
			this.txtSection.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSection_KeyPress);
			this.txtSection.TextChanged += new System.EventHandler(this.txtSection_TextChanged);
			// 
			// frmView
			// 
			this.AccessibleDescription = resources.GetString("$this.AccessibleDescription");
			this.AccessibleName = resources.GetString("$this.AccessibleName");
			this.AutoScaleBaseSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScaleBaseSize")));
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMargin")));
			this.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMinSize")));
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = ((System.Drawing.Size)(resources.GetObject("$this.ClientSize")));
			this.Controls.Add(this.panelBody);
			this.Controls.Add(this.label1);
			this.Enabled = ((bool)(resources.GetObject("$this.Enabled")));
			this.Font = ((System.Drawing.Font)(resources.GetObject("$this.Font")));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.MaximizeBox = false;
			this.MaximumSize = ((System.Drawing.Size)(resources.GetObject("$this.MaximumSize")));
			this.MinimizeBox = false;
			this.MinimumSize = ((System.Drawing.Size)(resources.GetObject("$this.MinimumSize")));
			this.Name = "frmView";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.ShowInTaskbar = false;
			this.StartPosition = ((System.Windows.Forms.FormStartPosition)(resources.GetObject("$this.StartPosition")));
			this.Text = resources.GetString("$this.Text");
			this.toolTip1.SetToolTip(this, resources.GetString("$this.ToolTip"));
			this.Load += new System.EventHandler(this.frmView_Load);
			((System.ComponentModel.ISupportInitialize)(this.tabSectionSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabSectionTarget)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabAirlineTarget)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabAirlineSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabNatureTarget)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabNatureSource)).EndInit();
			this.panelBody.ResumeLayout(false);
			this.panelButtons.ResumeLayout(false);
			this.panelNatures.ResumeLayout(false);
			this.panelAirlines.ResumeLayout(false);
			this.panelStatusSections.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void picBody_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, picBody, Color.WhiteSmoke, Color.LightGray);			
		}

		private void frmView_Load(object sender, System.EventArgs e)
		{
			DateTime olFrom;
			DateTime olTo;

			myDB = UT.GetMemDB();
			myHSS = myDB["HSS"];
			myALT = myDB["ALT"];
			myNAT = myDB["NAT"];

			lblCurrentActiveView.Text = "Active View in Main Window: " + CFC.CurrentViewName;
			olFrom = DateTime.Now;
			olFrom = UT.TimeFrameFrom; //new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 0,0,0);
			olTo   = UT.TimeFrameTo; //new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 23,59,0);
			dtFrom.Value = olFrom;
			dtTo.Value = olTo;
			dtFrom.CustomFormat = "dd.MM.yyyy - HH:mm";
			dtTo.CustomFormat = "dd.MM.yyyy - HH:mm";

			lblFrom.Parent = picBody;
			lblTo.Parent = picBody;
			lblViewName.Parent = picBody;

			btnNew.Parent = picButtons;
			btnDelete.Parent = picButtons;
			btnOK.Parent = picButtons;
			btnCancel.Parent = picButtons;
			btnSave.Parent = picButtons;
			lblCurrentActiveView.Parent = picButtons;

			btnSectionSource.Parent = picStatusSectionFilter;
			bntSectionTarget.Parent = picStatusSectionFilter;
			cbStatueSections.Parent = picStatusSectionFilter;
			btnAirlineSource.Parent = picAirlines;
			btnAirlineTarget.Parent = picAirlines;
			cbAirlines.Parent = picAirlines;
			btnNatureSource.Parent = picNatures;
			btnNatureTarget.Parent = picNatures;
			cbNatures.Parent = picNatures;

			NewView();
			int i = 0;
			cbViewName.Items.Add("<DEFAULT>");
			for( i = 0; i < myViewsTab.GetLineCount(); i++)
			{
				string strName = myViewsTab.GetFieldValue(i, "NAME");
				cbViewName.Items.Add(strName);
				if(strName == CFC.CurrentViewName)
				{
					cbViewName_SelectedValueChanged(this, null);
					cbViewName.Text = CFC.CurrentViewName;
				}
			}
			if(CFC.CurrentViewName == "<DEFAULT>")
			{
				cbViewName.Text = CFC.CurrentViewName;
				EnableControlsForDefault(false);
				btnDelete.Enabled = false;
				btnSave.Enabled = false;
			}
			cbViewName.Refresh();

			UpdateButtons();

		}

		private void InitTabs()
		{
			tabSectionSource.ResetContent();
			tabSectionSource.HeaderString = "Name,URNO";
			tabSectionSource.HeaderLengthString = "180,80";
			tabSectionSource.LogicalFieldList = "NAME,URNO";
			tabSectionSource.LifeStyle = true;
			tabSectionSource.CursorLifeStyle = true;
			tabSectionSource.SetTabFontBold(true);
			tabSectionSource.FontName = "Arial";
			tabSectionSource.DisplayBackColor = UT.colYellow;
			tabSectionSource.ShowHorzScroller(true);

			tabSectionTarget.ResetContent();
			tabSectionTarget.HeaderString = "Name,URNO";
			tabSectionTarget.HeaderLengthString = "180,80";
			tabSectionTarget.LogicalFieldList = "NAME,URNO";
			tabSectionTarget.LifeStyle = true;
			tabSectionTarget.CursorLifeStyle = true;
			tabSectionTarget.SetTabFontBold(true);
			tabSectionTarget.FontName = "Arial";
			tabSectionTarget.DisplayBackColor = UT.colLightGreen;
			tabSectionTarget.DisplayTextColor = UT.colBlack;
			tabSectionTarget.ShowHorzScroller(true);

			tabAirlineSource.ResetContent();
			tabAirlineSource.HeaderString = "ALC2,ALC3,URNO";
			tabAirlineSource.HeaderLengthString = "60,60,80";
			tabAirlineSource.LogicalFieldList = "ALC2,ALC3,URNO";
			tabAirlineSource.LifeStyle = true;
			tabAirlineSource.CursorLifeStyle = true;
			//tabAirlineSource.ShowHorzScroller(true);
			tabAirlineSource.SetTabFontBold(true);
			tabAirlineSource.FontName = "Arial";
			tabAirlineSource.DisplayBackColor = UT.colYellow;
			tabAirlineSource.ShowHorzScroller(true);

			tabAirlineTarget.ResetContent();
			tabAirlineTarget.HeaderString = "ALC2,ALC3,URNO";
			tabAirlineTarget.HeaderLengthString = "60,60,80";
			tabAirlineTarget.LogicalFieldList = "ALC2,ALC3,URNO";
			tabAirlineTarget.LifeStyle = true;
			tabAirlineTarget.CursorLifeStyle = true;
			tabAirlineTarget.SetTabFontBold(true);
			tabAirlineTarget.FontName = "Arial";
			//tabAirlineTarget.ShowHorzScroller(true);
			tabAirlineTarget.DisplayBackColor = UT.colLightGreen;
			tabAirlineTarget.DisplayTextColor = UT.colBlack;
			tabAirlineTarget.ShowHorzScroller(true);

			tabNatureSource.ResetContent();
			tabNatureSource.HeaderString = "Name,URNO";
			tabNatureSource.HeaderLengthString = "180,80";
			tabNatureSource.LogicalFieldList = "TNAM,URNO";
			tabNatureSource.LifeStyle = true;
			tabNatureSource.CursorLifeStyle = true;
			tabNatureSource.SetTabFontBold(true);
			//tabNatureSource.ShowHorzScroller(true);
			tabNatureSource.FontName = "Arial";
			tabNatureSource.DisplayBackColor = UT.colYellow;
			tabNatureSource.ShowHorzScroller(true);

			tabNatureTarget.ResetContent();
			tabNatureTarget.HeaderString = "Name,URNO";
			tabNatureTarget.HeaderLengthString = "180,80";
			tabNatureTarget.LogicalFieldList = "TNAM,URNO";
			tabNatureTarget.FontName = "Arial";
			tabNatureTarget.LifeStyle = true;
			tabNatureTarget.CursorLifeStyle = true;
			tabNatureTarget.SetTabFontBold(true);
			//tabNatureTarget.ShowHorzScroller(true);
			tabNatureTarget.DisplayBackColor = UT.colLightGreen;
			tabNatureTarget.DisplayTextColor = UT.colBlack;
			tabNatureTarget.ShowHorzScroller(true);
		}

		private void picStatusSectionFilter_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 0f, picStatusSectionFilter, Color.WhiteSmoke, Color.LightGray);			
		}

		private void picAirlines_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 0f, picAirlines, Color.WhiteSmoke, Color.LightGray);			
		}

		private void picNatures_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 0f, picNatures, Color.WhiteSmoke, Color.LightGray);			
		}

		private void btnAirlineTarget_Click(object sender, System.EventArgs e)
		{
			tabAirlineSource.InsertBuffer(tabAirlineTarget.GetBuffer(0, tabAirlineTarget.GetLineCount()-1, "\n"), "\n");
			tabAirlineTarget.ResetContent();
			tabAirlineTarget.Sort("0,1", true, true);
			tabAirlineSource.Sort("0,1", true, true);
			UpdateButtons();
		}

		private void btnAirlineSource_Click(object sender, System.EventArgs e)
		{
			tabAirlineTarget.InsertBuffer(tabAirlineSource.GetBuffer(0, tabAirlineSource.GetLineCount()-1, "\n"), "\n");
			tabAirlineSource.ResetContent();
			tabAirlineTarget.Sort("0,1", true, true);
			tabAirlineSource.Sort("0,1", true, true);
			UpdateButtons();
		}

		private void btnSectionSource_Click(object sender, System.EventArgs e)
		{
			tabSectionTarget.InsertBuffer(tabSectionSource.GetBuffer(0, tabSectionSource.GetLineCount()-1, "\n"), "\n");
			tabSectionSource.ResetContent();
			tabSectionSource.Sort("0", true, true);
			tabSectionTarget.Sort("0", true, true);
			UpdateButtons();
		}

		private void bntSectionTarget_Click(object sender, System.EventArgs e)
		{
			tabSectionSource.InsertBuffer(tabSectionTarget.GetBuffer(0, tabSectionTarget.GetLineCount()-1, "\n"), "\n");
			tabSectionTarget.ResetContent();
			tabSectionSource.Sort("0", true, true);
			tabSectionTarget.Sort("0", true, true);
			UpdateButtons();
		}
		private void btnNatureSource_Click(object sender, System.EventArgs e)
		{
			tabNatureTarget.InsertBuffer(tabNatureSource.GetBuffer(0, tabNatureSource.GetLineCount()-1, "\n"), "\n");
			tabNatureSource.ResetContent();
			tabNatureSource.Sort("0", true, true);
			tabNatureTarget.Sort("0", true, true);
			UpdateButtons();
		}

		private void btnNatureTarget_Click(object sender, System.EventArgs e)
		{
			tabNatureSource.InsertBuffer(tabNatureTarget.GetBuffer(0, tabNatureTarget.GetLineCount()-1, "\n"), "\n");
			tabNatureTarget.ResetContent();
			tabNatureSource.Sort("0", true, true);
			tabNatureTarget.Sort("0", true, true);
			UpdateButtons();
		}

		private void picButtons_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, picButtons, Color.WhiteSmoke, Color.Gray);			
		}

		private void NewView()
		{
			int i = 0;
			string strValues = "";
			InitTabs();
		
			if(bufferHSS.Length == 0)
			{
				for( i = 0; i < myHSS.Count; i++)	
				{
					strValues = myHSS[i]["NAME"] + "," + myHSS[i]["URNO"];
					bufferHSS.Append(strValues + "\n");
					//tabSectionSource.InsertTextLine(strValues, false);
				}
			}
			tabSectionSource.InsertBuffer(bufferHSS.ToString(), "\n");
			btnSectionSource.Text = myHSS.Count.ToString();
			if(bufferALT.Length == 0)
			{
				for( i = 0; i < myALT.Count; i++)	
				{
					strValues = myALT[i]["ALC2"] + "," + myALT[i]["ALC3"] + "," + myALT[i]["URNO"];
					bufferALT.Append(strValues + "\n");
					//tabAirlineSource.InsertTextLine(strValues, false);
				}
			}
			tabAirlineSource.InsertBuffer(bufferALT.ToString(), "\n");
			btnAirlineSource.Text = myALT.Count.ToString();
			if(bufferNAT.Length == 0)
			{
				for( i = 0; i < myNAT.Count; i++)	
				{
					strValues = myNAT[i]["TNAM"] + "," + myNAT[i]["URNO"];
					bufferNAT.Append(strValues + "\n");
					//tabNatureSource.InsertTextLine(strValues, false);
				}
			}
			tabNatureSource.InsertBuffer(bufferNAT.ToString(), "\n");
			btnNatureSource.Text = myNAT.Count.ToString();
			tabSectionSource.Sort("0", true, true);
			tabSectionTarget.Sort("0", true, true);
			tabAirlineSource.Sort("0,1", true, true);
			tabAirlineTarget.Sort("0,1", true, true);
			tabNatureSource.Sort("0", true, true);
			tabNatureTarget.Sort("0", true, true);
			cbViewName.Focus();
		}

		private void tabSectionSource_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo > -1)
			{
				tabSectionTarget.InsertTextLine( tabSectionSource.GetFieldValues(e.lineNo, "NAME,URNO"), true);
				tabSectionSource.DeleteLine(e.lineNo);
				tabSectionSource.Sort("0", true, true);
				tabSectionTarget.Sort("0", true, true);
			}
			UpdateButtons();
		}

		private void tabSectionTarget_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo > -1)
			{
				tabSectionSource.InsertTextLine( tabSectionTarget.GetFieldValues(e.lineNo, "NAME,URNO"), true);
				tabSectionTarget.DeleteLine(e.lineNo);
				tabSectionSource.Sort("0", true, true);
				tabSectionTarget.Sort("0", true, true);
			}
			UpdateButtons();
		}

		private void tabAirlineSource_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo > -1)
			{
				tabAirlineTarget.InsertTextLine( tabAirlineSource.GetFieldValues(e.lineNo, "ALC2,ALC3,URNO"), true);
				tabAirlineSource.DeleteLine(e.lineNo);
				tabAirlineTarget.Sort("0,1", true, true);
				tabAirlineSource.Sort("0,1", true, true);
			}
			UpdateButtons();
		}

		private void tabAirlineTarget_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo > -1)
			{
				tabAirlineSource.InsertTextLine( tabAirlineTarget.GetFieldValues(e.lineNo, "ALC2,ALC3,URNO"), true);
				tabAirlineTarget.DeleteLine(e.lineNo);
				tabAirlineTarget.Sort("0,1", true, true);
				tabAirlineSource.Sort("0,1", true, true);
			}
			UpdateButtons();
		}

		private void tabNatureSource_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo > -1)
			{
				tabNatureTarget.InsertTextLine( tabNatureSource.GetFieldValues(e.lineNo, "TNAM,URNO"), true);
				tabNatureSource.DeleteLine(e.lineNo);
				tabNatureSource.Sort("0", true, true);
				tabNatureTarget.Sort("0", true, true);
			}
			UpdateButtons();
		}

		private void tabNatureTarget_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo > -1)
			{
				tabNatureSource.InsertTextLine( tabNatureTarget.GetFieldValues(e.lineNo, "TNAM,URNO"), true);
				tabNatureTarget.DeleteLine(e.lineNo);
				tabNatureSource.Sort("0", true, true);
				tabNatureTarget.Sort("0", true, true);
			}
			UpdateButtons();
		}
		private void UpdateButtons()
		{
			btnSectionSource.Text = tabSectionSource.GetLineCount().ToString();
			bntSectionTarget.Text = tabSectionTarget.GetLineCount().ToString();
			btnAirlineSource.Text = tabAirlineSource.GetLineCount().ToString();
			btnAirlineTarget.Text = tabAirlineTarget.GetLineCount().ToString();
			btnNatureSource.Text = tabNatureSource.GetLineCount().ToString();
			btnNatureTarget.Text = tabNatureTarget.GetLineCount().ToString();
			tabSectionSource.Refresh();
			tabSectionTarget.Refresh();
			tabAirlineSource.Refresh();
			tabAirlineTarget.Refresh();
			tabNatureSource.Refresh();
			tabNatureTarget.Refresh();
		}

		private void btnNew_Click(object sender, System.EventArgs e)
		{
			NewView();
			UpdateButtons();
			cbViewName.Text = "";
		}

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			string strMessage = "";
			strMessage = SaveView();
			if(strMessage != "")
			{
				MessageBox.Show(this, strMessage, "Save Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				this.DialogResult = DialogResult.None;
			}
			else
			{
				this.Cursor = Cursors.WaitCursor;

				CFC.CurrentViewName = cbViewName.Text;
				this.DialogResult = DialogResult.OK;
				this.Cursor = Cursors.Arrow;
			}
		}
		private string CheckValidity()
		{
			string strMessage = "";
			if(cbViewName.Text == "")
			{
				strMessage = "Please insert a View Name!\n";
			}
//MWO: Not necessary to check the timeframe AMA+MWO 05.01.2004
//			if(dtFrom.Value < UT.TimeFrameFrom || dtFrom.Value > UT.TimeFrameTo ||
//				dtTo.Value < UT.TimeFrameFrom || dtTo.Value > UT.TimeFrameTo)
//			{
//				strMessage += "Please define a timeframe, which is inside the loaded time frame!\n";
//			}
			return strMessage;
		}

		private void cbViewName_SelectedValueChanged(object sender, System.EventArgs e)
		{
			string strName = cbViewName.Text;
			string strValues = "";
			int i = 0;
			int j = 0;
			bool blFound = false;
			int idx = -1;
			NewView();
			cbViewName.Text = strName;
			if(strName == "<DEFAULT>")
			{
				EnableControlsForDefault(false);
				return;
			}
			EnableControlsForDefault(true);
			for( i = 0; i < myViewsTab.GetLineCount() && blFound == false; i++)
			{
				if(strName == myViewsTab.GetFieldValue(i, "NAME"))
				{
					idx = i;
					blFound = true;
				}
			}
			if(idx > -1) // then the view was found
			{
				dtFrom.Value = UT.CedaFullDateToDateTime(myViewsTab.GetFieldValue(idx, "FROM"));
				dtTo.Value   = UT.CedaFullDateToDateTime(myViewsTab.GetFieldValue(idx, "TO"));
				string [] arrHSS = myViewsTab.GetFieldValue(idx, "HSS").Split('|');
				string [] arrALT = myViewsTab.GetFieldValue(idx, "ALT").Split('|');
				string [] arrNAT = myViewsTab.GetFieldValue(idx, "NAT").Split('|');
				StringBuilder sbALT = new StringBuilder(10000);

				if(arrHSS.Length > 0)
				{
					j =  arrHSS.Length - 1;
					for( i = tabSectionSource.GetLineCount()-1; i >= 0 && j >= 0; i--)
					{
						if(tabSectionSource.GetFieldValues(i, "URNO") == arrHSS[j])
						{
							tabSectionTarget.InsertTextLine( tabSectionSource.GetFieldValues(i, "NAME,URNO"), false);
							tabSectionSource.DeleteLine(i);
							j--;
						}
					}
				}
				if(arrALT.Length > 0)
				{
					j = arrALT.Length-1;
					tabAirlineSource.ShowVertScroller(false);
					for( i = tabAirlineSource.GetLineCount()-1; i >= 0 && j >= 0; i--)
					{
						if(tabAirlineSource.GetFieldValues(i, "URNO") == arrALT[j])
						{
							strValues  = tabAirlineSource.GetColumnValue(i, 0) + ",";
							strValues += tabAirlineSource.GetColumnValue(i, 1) + ",";
							strValues += tabAirlineSource.GetColumnValue(i, 2) + ",";
							//tabAirlineTarget.InsertTextLine( tabAirlineSource.GetFieldValues(i, "ALC2,ALC3,URNO"), false);
							sbALT.Append(strValues + "\n");
							tabAirlineSource.DeleteLine(i);
							j--;
						}
					}
					tabAirlineTarget.InsertBuffer(sbALT.ToString(), "\n");
					tabAirlineSource.ShowVertScroller(true);
				}
				if(arrNAT.Length > 0)
				{
					j = arrNAT.Length-1;
					for( i = tabNatureSource.GetLineCount()-1; i >= 0 && j >= 0; i--)
					{
						if(tabNatureSource.GetFieldValues(i, "URNO") == arrNAT[j])
						{
							tabNatureTarget.InsertTextLine( tabNatureSource.GetFieldValues(i, "TNAM,URNO"), false);
							tabNatureSource.DeleteLine(i);
							j--;
						}
					}
				}
				tabSectionSource.Sort("0", true, true);
				tabSectionTarget.Sort("0", true, true);
				tabAirlineSource.Sort("0,1", true, true);
				tabAirlineTarget.Sort("0,1", true, true);
				tabNatureSource.Sort("0", true, true);
				tabNatureTarget.Sort("0", true, true);
				UpdateButtons();
			}
		}

		private void btnDelete_Click(object sender, System.EventArgs e)
		{
			string strName = cbViewName.Text;
			if(strName == "<DEFAULT>")
			{
				return; //Nothing to do because it's <DEFAULT>
			}
			bool blFound = false;
			int idx = cbViewName.FindStringExact(strName, -1);
			if(idx > -1)
			{
				cbViewName.Items.RemoveAt(idx);
			}
			for(int i = 0; i < myViewsTab.GetLineCount() && blFound == false; i++)
			{
				if(strName == myViewsTab.GetFieldValue(i, "NAME"))
				{
					blFound = true; 
					myViewsTab.DeleteLine(i);
					myViewsTab.WriteToFile("C:\\Ufis\\System\\STATMGR_VIEWS.dat", false);
				}
			}
			cbViewName.Text = "";
			if(strName == CFC.CurrentViewName)
			{
				CFC.CurrentViewName = "<DEFAULT>";
			}
			NewView();
		}

		private void txtSection_TextChanged(object sender, System.EventArgs e)
		{
			SearchForTabEntry(txtSection, tabSectionSource, 1);
		}

		private void txtAirline_TextChanged(object sender, System.EventArgs e)
		{
			SearchForTabEntry(txtAirline, tabAirlineSource, 2);
		}

		private void txtNature_TextChanged(object sender, System.EventArgs e)
		{
			SearchForTabEntry(txtNature, tabNatureSource, 1);
		}

		private void SearchForTabEntry(System.Windows.Forms.TextBox txtBox, 
									   AxTABLib.AxTAB tabTab,
									   int StartColumns)
		{
			string strText = "," + txtBox.Text;
			string strValues = "";
			int idxOrig = -1;
			bool blFound = false;
			int i = 0;
			int idx = tabTab.GetCurrentSelected();
			idxOrig = idx; //to remember for the wrap
			if(idx == -1) idx = 0;
			for( i = idx; i < tabTab.GetLineCount() && blFound == false; i++)
			{
				strValues = ",";
				for(int j = 0; j < StartColumns; j++)
				{
					strValues += tabTab.GetColumnValue(i, j) + ",";
				}
				if(strValues.IndexOf(strText, 0) > -1)
				{
					blFound = true;
					tabTab.SetCurrentSelection(i);
					tabTab.OnVScrollTo(i);
				}
			}
			if(blFound == false)
			{
				for( i = 0; i <= idxOrig && blFound == false; i++)
				{
					strValues = ",";
					for(int j = 0; j < StartColumns; j++)
					{
						strValues += tabTab.GetColumnValue(i, j) + ",";
					}
					if(strValues.IndexOf(strText, 0) > -1)
					{
						blFound = true;
						tabTab.SetCurrentSelection(i);
						tabTab.OnVScrollTo(i);
					}
				}
			}
		}

		private void txtSection_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if(e.KeyChar == (char)13)
			{
				int currSel = tabSectionSource.GetCurrentSelected();
				if(currSel >= 0)
				{
					tabSectionTarget.InsertTextLine( tabSectionSource.GetFieldValues(currSel, "NAME,URNO"), true);
					tabSectionSource.DeleteLine(currSel);
					tabSectionTarget.Sort("0", true, true);
					UpdateButtons();
				}
			}
		}

		private void txtAirline_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if(e.KeyChar == (char)13)
			{
				int currSel = tabAirlineSource.GetCurrentSelected();
				if(currSel >= 0)
				{
					tabAirlineTarget.InsertTextLine( tabAirlineSource.GetFieldValues(currSel, "ALC2,ALC3,URNO"), true);
					tabAirlineSource.DeleteLine(currSel);
					tabAirlineTarget.Sort("0,1", true, true);
					UpdateButtons();
				}
			}
		}

		private void txtNature_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if(e.KeyChar == (char)13)
			{
				int currSel = tabNatureSource.GetCurrentSelected();
				if(currSel >= 0)
				{
					tabNatureTarget.InsertTextLine( tabNatureSource.GetFieldValues(currSel, "TNAM,URNO"), true);
					tabNatureSource.DeleteLine(currSel);
					tabNatureTarget.Sort("0", true, true);
					UpdateButtons();
				}
			}
		}
		private void EnableControlsForDefault(bool enable)
		{
			if(enable == true)
			{
				tabSectionSource.Enabled = true;
				tabSectionTarget.Enabled = true;
				tabAirlineSource.Enabled = true;
				tabAirlineTarget.Enabled = true;
				tabNatureSource.Enabled = true;
				tabNatureTarget.Enabled = true;
				tabSectionSource.DisplayBackColor = UT.colYellow;
				tabAirlineSource.DisplayBackColor = UT.colYellow;
				tabNatureSource.DisplayBackColor = UT.colYellow;

				txtSection.Enabled = true;
				txtAirline.Enabled = true;
				txtNature.Enabled = true;
				btnAirlineSource.Enabled = true;
				btnAirlineTarget.Enabled = true;
				btnSectionSource.Enabled = true;
				bntSectionTarget.Enabled = true;
				btnNatureSource.Enabled = true;
				btnNatureTarget.Enabled = true;
			}
			else
			{
				tabSectionSource.Enabled = false;
				tabSectionTarget.Enabled = false;
				tabAirlineSource.Enabled = false;
				tabAirlineTarget.Enabled = false;
				tabNatureSource.Enabled = false;
				tabNatureTarget.Enabled = false;
				tabSectionSource.DisplayBackColor = UT.colLightGray;
				tabAirlineSource.DisplayBackColor = UT.colLightGray;
				tabNatureSource.DisplayBackColor = UT.colLightGray;

				txtSection.Enabled = false;
				txtAirline.Enabled = false;
				txtNature.Enabled = false;
				btnAirlineSource.Enabled = false;
				btnAirlineTarget.Enabled = false;
				btnSectionSource.Enabled = false;
				bntSectionTarget.Enabled = false;
				btnNatureSource.Enabled = false;
				btnNatureTarget.Enabled = false;
			}
		}

		private void cbViewName_TextChanged(object sender, System.EventArgs e)
		{
			string strText = cbViewName.Text;
			if(strText != "<DEFAULT>")
			{
				EnableControlsForDefault(true);
			}
			else
			{
				EnableControlsForDefault(false);
			}
			if(strText == "")
			{
				btnOK.Enabled = false;
				btnDelete.Enabled = false;
				btnSave.Enabled = false;
			}
			else
			{
				btnOK.Enabled = true;
				btnDelete.Enabled = true;
				btnSave.Enabled = true;
			}
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{	
			string strMessage = SaveView();
			if(strMessage != "")
			{
				MessageBox.Show(this, strMessage, "Save Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}
		}
		private string SaveView()
		{
			string strMessage = "";
			strMessage = CheckValidity();
			int i = 0;
			if(strMessage == "")
			{
				string strValues = "";
				string strName = cbViewName.Text;
				string strHSS = "";
				string strALT = "";
				string strNAT = "";
				string strFrom = "";
				string strTo = "";

				if(strName != "<DEFAULT>")
				{
					strHSS = tabSectionTarget.GetBufferByFieldList(0, tabSectionTarget.GetLineCount()-1, "URNO", "|");
					strALT = tabAirlineTarget.GetBufferByFieldList(0, tabAirlineTarget.GetLineCount()-1, "URNO", "|");
					strNAT = tabNatureTarget.GetBufferByFieldList(0, tabNatureTarget.GetLineCount()-1, "URNO", "|");
					strFrom = UT.DateTimeToCeda( dtFrom.Value );
					strTo   = UT.DateTimeToCeda( dtTo.Value );
					for( i = myViewsTab.GetLineCount()-1; i >= 0 ; i--)
					{
						if(myViewsTab.GetFieldValue(i, "NAME") == strName)
						{
							myViewsTab.DeleteLine(i);
						}
					}
					//"NAME,FROM,TO,HSS,ALT,NAT";
					strValues = strName + "," + strFrom + "," + strTo + "," + strHSS + "," + strALT + "," + strNAT;
					myViewsTab.InsertTextLine(strValues, true);
					myViewsTab.WriteToFile("C:\\Ufis\\System\\STATMGR_VIEWS.dat", false);
				}
				if(cbViewName.FindStringExact(strName) == -1)
				{
					cbViewName.Items.Add(strName);
				}
			}
			return strMessage;
		}

		private void cbViewName_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if(e.KeyChar == (char)44)// check for comma
			{
				e.Handled = true;
			}
		}
	}
}
