using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Collections.Specialized; 
using System.Text;
using Ufis.Utils;
using Ufis.Data;
namespace Status_Manager
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class frmData : System.Windows.Forms.Form
	{
		public AxUFISCOMLib.AxUfisCom axUfisCom1;
		public AxTABLib.AxTAB tabAFT;
		private System.Windows.Forms.Label labelAft;
		private static frmData myThis;


		public ArrayList arrAllDBTabs;
		public AxTABLib.AxTAB tabCompressedView;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		public AxTABLib.AxTAB tabPositions;
		private System.Windows.Forms.Label lbl2;
		public AxTABLib.AxTAB tabVPL;
		private System.Windows.Forms.Label lbl1;
		public AxTABLib.AxTAB tabVPD;
		private AxTABLib.AxTAB tabAPT;
		private AxTABLib.AxTAB tabACT;
		private AxTABLib.AxTAB tabGRN;
		public AxTABLib.AxTAB tabViews;
		private System.Windows.Forms.Button btnStoreViewpoints;
		private System.Windows.Forms.Button btnLoadViewpointsFromFile;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmData()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			myThis = this;
			arrAllDBTabs = new ArrayList();
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmData));
			this.axUfisCom1 = new AxUFISCOMLib.AxUfisCom();
			this.tabAFT = new AxTABLib.AxTAB();
			this.labelAft = new System.Windows.Forms.Label();
			this.tabCompressedView = new AxTABLib.AxTAB();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.tabPositions = new AxTABLib.AxTAB();
			this.lbl2 = new System.Windows.Forms.Label();
			this.tabVPL = new AxTABLib.AxTAB();
			this.lbl1 = new System.Windows.Forms.Label();
			this.tabVPD = new AxTABLib.AxTAB();
			this.tabAPT = new AxTABLib.AxTAB();
			this.tabACT = new AxTABLib.AxTAB();
			this.tabGRN = new AxTABLib.AxTAB();
			this.tabViews = new AxTABLib.AxTAB();
			this.btnStoreViewpoints = new System.Windows.Forms.Button();
			this.btnLoadViewpointsFromFile = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.axUfisCom1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabAFT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabCompressedView)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabPositions)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabVPL)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabVPD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabAPT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabACT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabGRN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabViews)).BeginInit();
			this.SuspendLayout();
			// 
			// axUfisCom1
			// 
			this.axUfisCom1.Enabled = true;
			this.axUfisCom1.Location = new System.Drawing.Point(1242, 11);
			this.axUfisCom1.Name = "axUfisCom1";
			this.axUfisCom1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axUfisCom1.OcxState")));
			this.axUfisCom1.Size = new System.Drawing.Size(128, 42);
			this.axUfisCom1.TabIndex = 0;
			// 
			// tabAFT
			// 
			this.tabAFT.Location = new System.Drawing.Point(6, 349);
			this.tabAFT.Name = "tabAFT";
			this.tabAFT.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabAFT.OcxState")));
			this.tabAFT.Size = new System.Drawing.Size(1680, 180);
			this.tabAFT.TabIndex = 1;
			this.tabAFT.Tag = "AFTTAB;URNO,RKEY,FTYP,ADID,FLNO,GTA1,GTD1,LAND,ONBS,PSTA,PSTD,STOA,STOD,TIFA,TIFD" +
				",TISA,TISD,TMOA,TTYP,ORG3,DES3,FLTI,ETAI,ETDI,ONBL,OFBL,VIA3,ACT3,PABA,PAEA,PABS" +
				",PAES,PDBA,PDEA,PDBS,PDES,VIAL,ADER; ";
			// 
			// labelAft
			// 
			this.labelAft.BackColor = System.Drawing.Color.Black;
			this.labelAft.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.labelAft.ForeColor = System.Drawing.Color.Lime;
			this.labelAft.Location = new System.Drawing.Point(6, 327);
			this.labelAft.Name = "labelAft";
			this.labelAft.Size = new System.Drawing.Size(397, 22);
			this.labelAft.TabIndex = 2;
			this.labelAft.Text = "AFTTAB:";
			// 
			// tabCompressedView
			// 
			this.tabCompressedView.Location = new System.Drawing.Point(13, 587);
			this.tabCompressedView.Name = "tabCompressedView";
			this.tabCompressedView.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabCompressedView.OcxState")));
			this.tabCompressedView.Size = new System.Drawing.Size(384, 97);
			this.tabCompressedView.TabIndex = 3;
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.Black;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.Lime;
			this.label1.Location = new System.Drawing.Point(6, 526);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(1357, 22);
			this.label1.TabIndex = 4;
			this.label1.Text = "Tabs for internal use ==> no database";
			// 
			// label2
			// 
			this.label2.BackColor = System.Drawing.Color.Black;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label2.ForeColor = System.Drawing.Color.Lime;
			this.label2.Location = new System.Drawing.Point(13, 565);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(307, 22);
			this.label2.TabIndex = 5;
			this.label2.Text = "For Compressed View";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(806, 6);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(244, 22);
			this.label3.TabIndex = 11;
			this.label3.Text = "Airport Positions:";
			// 
			// tabPositions
			// 
			this.tabPositions.Location = new System.Drawing.Point(806, 28);
			this.tabPositions.Name = "tabPositions";
			this.tabPositions.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabPositions.OcxState")));
			this.tabPositions.Size = new System.Drawing.Size(544, 166);
			this.tabPositions.TabIndex = 10;
			this.tabPositions.Tag = "PSTTAB;PNAM,POSR;";
			// 
			// lbl2
			// 
			this.lbl2.Location = new System.Drawing.Point(6, 166);
			this.lbl2.Name = "lbl2";
			this.lbl2.Size = new System.Drawing.Size(244, 22);
			this.lbl2.TabIndex = 9;
			this.lbl2.Text = "Location Defs(VPL):";
			// 
			// tabVPL
			// 
			this.tabVPL.Location = new System.Drawing.Point(6, 188);
			this.tabVPL.Name = "tabVPL";
			this.tabVPL.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabVPL.OcxState")));
			this.tabVPL.Size = new System.Drawing.Size(664, 166);
			this.tabVPL.TabIndex = 8;
			// 
			// lbl1
			// 
			this.lbl1.Location = new System.Drawing.Point(6, 6);
			this.lbl1.Name = "lbl1";
			this.lbl1.Size = new System.Drawing.Size(244, 22);
			this.lbl1.TabIndex = 7;
			this.lbl1.Text = "ViewPoints (VPD):";
			// 
			// tabVPD
			// 
			this.tabVPD.Location = new System.Drawing.Point(6, 28);
			this.tabVPD.Name = "tabVPD";
			this.tabVPD.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabVPD.OcxState")));
			this.tabVPD.Size = new System.Drawing.Size(656, 166);
			this.tabVPD.TabIndex = 6;
			// 
			// tabAPT
			// 
			this.tabAPT.Location = new System.Drawing.Point(390, 554);
			this.tabAPT.Name = "tabAPT";
			this.tabAPT.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabAPT.OcxState")));
			this.tabAPT.Size = new System.Drawing.Size(528, 111);
			this.tabAPT.TabIndex = 12;
			// 
			// tabACT
			// 
			this.tabACT.Location = new System.Drawing.Point(390, 659);
			this.tabACT.Name = "tabACT";
			this.tabACT.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabACT.OcxState")));
			this.tabACT.Size = new System.Drawing.Size(528, 111);
			this.tabACT.TabIndex = 13;
			// 
			// tabGRN
			// 
			this.tabGRN.Location = new System.Drawing.Point(826, 554);
			this.tabGRN.Name = "tabGRN";
			this.tabGRN.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabGRN.OcxState")));
			this.tabGRN.Size = new System.Drawing.Size(528, 111);
			this.tabGRN.TabIndex = 14;
			// 
			// tabViews
			// 
			this.tabViews.Location = new System.Drawing.Point(563, 188);
			this.tabViews.Name = "tabViews";
			this.tabViews.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabViews.OcxState")));
			this.tabViews.Size = new System.Drawing.Size(664, 166);
			this.tabViews.TabIndex = 15;
			// 
			// btnStoreViewpoints
			// 
			this.btnStoreViewpoints.Location = new System.Drawing.Point(851, 698);
			this.btnStoreViewpoints.Name = "btnStoreViewpoints";
			this.btnStoreViewpoints.Size = new System.Drawing.Size(327, 33);
			this.btnStoreViewpoints.TabIndex = 16;
			this.btnStoreViewpoints.Text = "Store Viewpoints";
			this.btnStoreViewpoints.Click += new System.EventHandler(this.btnStoreViewpoints_Click);
			// 
			// btnLoadViewpointsFromFile
			// 
			this.btnLoadViewpointsFromFile.Location = new System.Drawing.Point(544, 105);
			this.btnLoadViewpointsFromFile.Name = "btnLoadViewpointsFromFile";
			this.btnLoadViewpointsFromFile.Size = new System.Drawing.Size(186, 32);
			this.btnLoadViewpointsFromFile.TabIndex = 17;
			this.btnLoadViewpointsFromFile.Text = "Load Viewpoint File";
			this.btnLoadViewpointsFromFile.Click += new System.EventHandler(this.btnLoadViewpointsFromFile_Click);
			// 
			// frmData
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(8, 18);
			this.ClientSize = new System.Drawing.Size(1284, 772);
			this.Controls.Add(this.btnLoadViewpointsFromFile);
			this.Controls.Add(this.btnStoreViewpoints);
			this.Controls.Add(this.tabViews);
			this.Controls.Add(this.tabGRN);
			this.Controls.Add(this.tabACT);
			this.Controls.Add(this.tabAPT);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.tabPositions);
			this.Controls.Add(this.lbl2);
			this.Controls.Add(this.tabVPL);
			this.Controls.Add(this.lbl1);
			this.Controls.Add(this.tabVPD);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.tabCompressedView);
			this.Controls.Add(this.labelAft);
			this.Controls.Add(this.tabAFT);
			this.Controls.Add(this.axUfisCom1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "frmData";
			this.Text = "Form1";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.frmData_Closing);
			this.Load += new System.EventHandler(this.frmData_Load);
			((System.ComponentModel.ISupportInitialize)(this.axUfisCom1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabAFT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabCompressedView)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabPositions)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabVPL)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabVPD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabAPT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabACT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabGRN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabViews)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// returns the this reference to get access to the class from outside
		/// </summary>
		/// <returns>Reference to frmData</returns>
		public static frmData GetThis()
		{
			return myThis;
		}
		/// <summary>
		/// Load of the form
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void frmData_Load(object sender, System.EventArgs e)
		{

			tabCompressedView.ResetContent();
			tabCompressedView.HeaderString = "KEY,FROM,TO,TEXT,COLOR,LEFTTRIA,RIGHTTRIA";
			tabCompressedView.LogicalFieldList = "KEY,FROM,TO,TEXT,COLOR,LEFTTRIA,RIGHTTRIA";
			tabCompressedView.HeaderLengthString = "100,130,130,130,100,100,100";
			tabCompressedView.EnableHeaderSizing(true);
			tabCompressedView.ShowHorzScroller(true);
			foreach(System.Windows.Forms.Control olC in this.Controls )
			{
				if(olC.GetType().ToString() == "AxTABLib.AxTAB")
				{
					AxTABLib.AxTAB olT = (AxTABLib.AxTAB)olC;
					arrAllDBTabs.Add(olC);
				}
			}
			tabVPD.ResetContent();
			tabVPD.EnableHeaderSizing(true);
			tabVPD.AutoSizeByHeader = true;
			tabVPD.ShowHorzScroller(true);
			tabVPL.ResetContent();
			tabVPL.EnableHeaderSizing(true);
			tabVPL.AutoSizeByHeader = true;
			tabVPL.ShowHorzScroller(true);


			tabVPD.HeaderString = "URNO,VIEW,FILE,GRID,DRAWMODE,IMAGESIZE,DEFTIMEFR,HOPO";
			tabVPD.LogicalFieldList = tabVPD.HeaderString;
			tabVPD.HeaderLengthString = "80,80,80,80,80,80,80,80";
			tabVPD.ShowHorzScroller(true);
			tabVPL.HeaderString = "URNO,VIEW,POS,X,Y,ALIGN,ROTATE,TYPE,HOPO";
			tabVPL.LogicalFieldList = tabVPL.HeaderString;
			tabVPL.HeaderLengthString = "80,80,80,80,80,80,80,80,80";

//			tabVPD.ReadFromFile( "C:\\Ufis\\BV_VIEWPOINTS.dat"); 
//			tabVPL.ReadFromFile("C:\\Ufis\\BV_DETAILDEFS.dat");
		
			tabViews.ResetContent();
			tabViews.HeaderString = "NAME,FROM,TO,HSS,ALT,NAT";
			tabViews.LogicalFieldList = "NAME,FROM,TO,HSS,ALT,NAT";
			tabViews.ReadFromFile("C:\\Ufis\\System\\STATMGR_VIEWS.dat");
			tabViews.AutoSizeByHeader = true;
			tabViews.AutoSizeColumns();
		}

		/// <summary>
		/// Loads all data along the definition in the tab's tag.
		/// </summary>
		public void LoadData()
		{
			int i = 0;
			foreach(AxTABLib.AxTAB myTab in arrAllDBTabs)
			{
				String [] arr;
				if(myTab.Tag != null)
				{
					try
					{
						arr = myTab.Tag.ToString().Split(';'); 
						if (arr.Length  > 2) //Only to be done of Table and fields are available
						{
							String[] arr2;
							String   tmp;
							myTab.ResetContent();
							myTab.AutoSizeByHeader = true;
							myTab.HeaderString = "";
							myTab.HeaderLengthString = "";
							myTab.HeaderString = arr[1];
							myTab.EnableInlineEdit(true);
							myTab.CedaServerName = UT.ServerName;
							myTab.CedaPort = "3357";
							myTab.CedaHopo = UT.Hopo;
							myTab.CedaCurrentApplication = "StatMgr";
							myTab.CedaTabext = "TAB";
							myTab.CedaUser = "STATMNG";
							myTab.CedaWorkstation = "wks104";
							myTab.CedaSendTimeout = 3;
							myTab.CedaReceiveTimeout = 240;
							myTab.CedaRecordSeparator = "\n";
							myTab.CedaIdentifier = "Status-Manager";
							myTab.ShowHorzScroller(true);
							myTab.EnableHeaderSizing(true);
				
							arr2 = arr[1].Split(',');
							tmp = "";
							for( i = 0; i < arr2.Length ; i++)
							{
								if ((i+1) == arr2.Length )
									tmp += "80";
								else
									tmp += "80,";
							}
							myTab.HeaderLengthString = tmp;
							myTab.LogicalFieldList = arr[1];
							if(arr[0] == "AFTTAB")
							{
								//								String strWhere="";
								//								strWhere = "WHERE (TIFA BETWEEN '" + UT.DateTimeToCeda(UT.TimeFrameFrom) + "' AND '" + UT.DateTimeToCeda(UT.TimeFrameTo) + "') AND (TIFD BETWEEN '" + UT.DateTimeToCeda(UT.TimeFrameFrom) + "' and '" + UT.DateTimeToCeda(UT.TimeFrameTo) + "') AND FTYP IN ('S', 'O', 'X')";
								//								frmStartup.GetThis().txtCurrentStatus.Text = "Loading " + arr[0];
								//								frmStartup.GetThis().Refresh();
								//								myTab.CedaAction("RT", arr[0], arr[1], "", strWhere);
								//								myTab.IndexCreate("URNO", 0);
								//								myTab.IndexCreate("RKEY", 1);
								//								myTab.Sort("1", true, true);
								//								myTab.AutoSizeColumns();
								//								myTab.SetInternalLineBuffer(true);
								//
								//								llLineCount = myTab.GetLineCount()-1;
								//								labelAft.Text = "AFTTAB: " + llLineCount.ToString() + " Records";
								//								frmStartup.GetThis().txtDoneStatus.Text  = arr[0] + " Records " + llLineCount.ToString() + " loaded";
								//								frmStartup.GetThis().Refresh();
							}
							if(arr[0] == "PSTTAB")
							{
								//myTab.CedaAction("RT", arr[0], arr[1], "", " ORDER BY PNAM");
							}
						}//END if (arr.Length  > 2)
					}
					catch(System.Exception err)
					{
						UT.AddException("FormData", "LoadData", err.Message.ToString(), "", "");
					}
				}
			}//END foreach(AxTABLib.AxTAB myTab in arrAllDBTabs)
			//********************
			// The status manager special tables
			// VPDTAB: Viewpoint Definitions
			// VPLTAB: Viewpoint locations
			// SRHTAB: Rule Header
			// HSSTAB: Status sections
			// SDETAB: Rule events
			// OSTTAB: Online status
			IDatabase myDB = UT.GetMemDB();
			//bool result = myDB.Connect(UT.ServerName,"Default",UT.Hopo,"TAB","STATMGR");
			ITable myTable = myDB.Bind("APT", "APT", "URNO,APC3,APC4,APFN", "10,3,4,32", "URNO,APC3,APC4,APFN");
			frmStartup.GetThis().txtCurrentStatus.Text = "Loading APTTAB (Airports)";
			frmStartup.GetThis().txtCurrentStatus.Refresh();
			myTable.Load("ORDER BY APC3");
			myTable.CreateIndex("URNO", "URNO");
			frmStartup.GetThis().txtDoneStatus.Text = "APTTAB " + myTable.Count + " Records loaded";
			frmStartup.GetThis().txtDoneStatus.Refresh();
			myTable.FillVisualizer(tabAPT); 
			tabAPT.AutoSizeByHeader = true;
			tabAPT.AutoSizeColumns();
			//VPDTAB
			myTable = myDB.Bind("VPD", "VPD", "URNO,VNAM,FNAM,GRID,DMOD,IMSZ,TIMF,HOPO", "10,64,254,3,1,3,5,3", "URNO,VNAM,FNAM,GRID,DMOD,IMSZ,TIMF,HOPO");
			frmStartup.GetThis().txtCurrentStatus.Text = "Loading VPDTAB (Viewpoint Definitions)";
			frmStartup.GetThis().txtCurrentStatus.Refresh();
			myTable.Load("ORDER BY VNAM");
			myTable.CreateIndex("URNO", "URNO");
			myTable.CreateIndex("VNAM", "VNAM");
			frmStartup.GetThis().txtDoneStatus.Text = "VPDTAB " + myTable.Count + " Records loaded";
			frmStartup.GetThis().txtDoneStatus.Refresh();
			myTable.FillVisualizer(tabVPD); 
			tabVPD.AutoSizeByHeader = true;
			tabVPD.AutoSizeColumns();
			tabVPD.HeaderString = "URNO,VIEW,FILE,GRID,DRAWMODE,IMAGESIZE,DEFTIMEFR,HOPO";
			tabVPD.LogicalFieldList = tabVPD.HeaderString;
			//VPLTAB
			myTable = myDB.Bind("VPL", "VPL", "URNO,VNAM,POSI,XPOS,YPOS,ALIG,ROTA,TYPE,HOPO", 
							    "10,64,10,5,5,2,1,8,3", 
                                "URNO,VNAM,POSI,XPOS,YPOS,ALIG,ROTA,TYPE,HOPO");
			frmStartup.GetThis().txtCurrentStatus.Text = "Loading VPLTAB (Viewpoint Locations)";
			frmStartup.GetThis().txtCurrentStatus.Refresh();
			myTable.Load("ORDER BY VNAM");
			myTable.CreateIndex("URNO", "URNO");
			myTable.CreateIndex("VNAM", "VNAM");
			frmStartup.GetThis().txtDoneStatus.Text = "VPLTAB " + myTable.Count + " Records loaded";
			frmStartup.GetThis().txtDoneStatus.Refresh();
			myTable.FillVisualizer(tabVPL); 
			tabVPL.AutoSizeByHeader = true;
			tabVPL.AutoSizeColumns();
			tabVPL.HeaderString = "URNO,VIEW,POS,X,Y,ALIGN,ROTATE,TYPE,HOPO";
			tabVPL.LogicalFieldList = tabVPL.HeaderString;
			//ACTTAB
			myTable = myDB.Bind("ACT", "ACT", "URNO,ACT3,ACT5,ACFN", "10,3,5,32", "URNO,ACT3,ACT5,ACFN");
			frmStartup.GetThis().txtCurrentStatus.Text = "Loading ACTTAB (A/C-Types)";
			frmStartup.GetThis().txtCurrentStatus.Refresh();
			myTable.Load("ORDER BY ACT3");
			myTable.CreateIndex("URNO", "URNO");
			frmStartup.GetThis().txtDoneStatus.Text = "ACTTAB " + myTable.Count + " Records loaded";
			frmStartup.GetThis().txtDoneStatus.Refresh();
			myTable.FillVisualizer(tabACT); 
			tabACT.AutoSizeByHeader = true;
			tabACT.AutoSizeColumns();

			myTable = myDB.Bind("NAT", "NAT", "URNO,TTYP,TNAM", "10,3,32", "URNO,TTYP,TNAM");
			frmStartup.GetThis().txtCurrentStatus.Text = "Loading NATTAB (Nature Codes)";
			frmStartup.GetThis().txtCurrentStatus.Refresh();
			myTable.Load("ORDER BY TTYP");
			myTable.CreateIndex("URNO", "URNO");
			myTable.CreateIndex("TTYP", "TTYP");
			frmStartup.GetThis().txtDoneStatus.Text = "NATTAB " + myTable.Count + " Records loaded";
			frmStartup.GetThis().txtDoneStatus.Refresh();

			myTable = myDB.Bind("VAL", "VAL", "URNO,APPL,UVAL,FREQ,VAFR,VATO", "10,9,10,7,14,14", "URNO,APPL,UVAL,FREQ,VAFR,VATO");
			frmStartup.GetThis().txtCurrentStatus.Text = "Loading VALTAB (Validities)";
			frmStartup.GetThis().txtCurrentStatus.Refresh();
			myTable.Load("WHERE APPL='STATMGR'");
			myTable.CreateIndex("URNO", "URNO");
			myTable.CreateIndex("UVAL", "UVAL");
			frmStartup.GetThis().txtDoneStatus.Text = "VALTAB " + myTable.Count + " Records loaded";
			frmStartup.GetThis().txtDoneStatus.Refresh();

			myTable = myDB.Bind("ALT", "ALT", "URNO,ALC2,ALC3,ALFN", "10,2,3,32", "URNO,ALC2,ALC3,ALFN");
			frmStartup.GetThis().txtCurrentStatus.Text = "Loading ALTTAB (Airlines)";
			frmStartup.GetThis().txtCurrentStatus.Refresh();
			myTable.Load("ORDER BY ALC2,ALC3");
			myTable.CreateIndex("URNO", "URNO");
			myTable.CreateIndex("ALC2", "ALC2");
			myTable.CreateIndex("ALC3", "ALC3");
			frmStartup.GetThis().txtDoneStatus.Text = "ALTTAB " + myTable.Count + " Records loaded";
			frmStartup.GetThis().txtDoneStatus.Refresh();
			//SGRTAB
			myTable = myDB.Bind("GRN", "SGR", "URNO,TABN,GRPN", "10,6,12", "URNO,TABN,GRPN");
			frmStartup.GetThis().txtCurrentStatus.Text = "Loading Groups";
			frmStartup.GetThis().txtCurrentStatus.Refresh();

			//Read CEDA.ini USEALLGROUPS = NO ==> read only those
			//groups where appl='STATMGR' otherwise use all groups
				string strSQLWhere = "";
			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			string strUSEALLGROUPS = myIni.IniReadValue("STATUSMANAGER", "USEALLGROUPS");
			if(strUSEALLGROUPS == "NO")
			{
				strSQLWhere = "WHERE APPL='STATMGR'";
			}
			myTable.Load(strSQLWhere +  " ORDER BY GRPN");
			myTable.CreateIndex("URNO", "URNO");
			frmStartup.GetThis().txtDoneStatus.Text = "Groups" + myTable.Count + " Records loaded";
			frmStartup.GetThis().txtDoneStatus.Refresh();
			myTable.FillVisualizer(tabGRN); 
			tabGRN.AutoSizeByHeader = true;
			tabGRN.AutoSizeColumns();
			//SRHTAB
			myTable = myDB.Bind("SRH", "SRH", 
								"URNO,NAME,ADID,UHSS,ACTG,ACTU,ALTG,ALTU,APTG,APTU,NATG,NATU,CDAT,LSTU,USEC,USEU,DELF,MAXT", 
								"10,32,10,10,1,10,10,10,10,14,14,10,10,10,32,32,1,4", 
								"URNO,NAME,ADID,UHSS,ACTG,ACTU,ALTG,ALTU,APTG,APTU,NATG,NATU,CDAT,LSTU,USEC,USEU,DELF,MAXT");
			frmStartup.GetThis().txtCurrentStatus.Text = "Loading SRHTAB (Rules)";
			frmStartup.GetThis().txtCurrentStatus.Refresh();
			myTable.Load("ORDER BY NAME");
			myTable.CreateIndex("URNO", "URNO");
			frmStartup.GetThis().txtDoneStatus.Text = "SRHTAB" + myTable.Count + " Records loaded";
			frmStartup.GetThis().txtDoneStatus.Refresh();
			//HSSTAB
			myTable = myDB.Bind("HSS", "HSS", "URNO,NAME", "10,32", "URNO,NAME");
			frmStartup.GetThis().txtCurrentStatus.Text = "Loading HSSTAB (Status sections)";
			frmStartup.GetThis().txtCurrentStatus.Refresh();
			myTable.Load("ORDER BY NAME");
			myTable.CreateIndex("URNO", "URNO");
			frmStartup.GetThis().txtDoneStatus.Text = "HSSTAB " + myTable.Count + " Records loaded";
			frmStartup.GetThis().txtDoneStatus.Refresh();
			//TPLTAB
			myTable = myDB.Bind("TPL", "TPL", "URNO,TNAM", "10,64", "URNO,TNAM");
			frmStartup.GetThis().txtCurrentStatus.Text = "Loading TPLTAB (Rms Rule Templates)";
			frmStartup.GetThis().txtCurrentStatus.Refresh();
			myTable.Load("WHERE APPL='RULE_AFT' AND TPST='1' ORDER BY TNAM");
			//myTable.Load("ORDER BY TNAM");
			myTable.CreateIndex("URNO", "URNO");
			frmStartup.GetThis().txtDoneStatus.Text = "TPLTAB " + myTable.Count + " Records loaded";
			frmStartup.GetThis().txtDoneStatus.Refresh();
			//SGMTAB
			myTable = myDB.Bind("SGM", "SGM", "USGR,UVAL,TABN", "10,10,6", "USGR,UVAL,TABN");
			frmStartup.GetThis().txtCurrentStatus.Text = "Loading SGMTAB (Static Group-Members)";
			frmStartup.GetThis().txtCurrentStatus.Refresh();
			myTable.Load("ORDER BY USGR");
			myTable.CreateIndex("USGR", "USGR");
			frmStartup.GetThis().txtDoneStatus.Text = "SGMTAB " + myTable.Count + " Records loaded";
			frmStartup.GetThis().txtDoneStatus.Refresh();
			//PSTTAB
			myTable = myDB.Bind("PST", "PST", "URNO,PNAM,POSR", "10,10,128", "URNO,PNAM,POSR");
			frmStartup.GetThis().txtCurrentStatus.Text = "Loading PSTTAB (Airport Positions)";
			frmStartup.GetThis().txtCurrentStatus.Refresh();
			myTable.Load("ORDER BY PNAM");
			myTable.CreateIndex("PNAM", "PNAM");
			myTable.CreateIndex("URNO", "URNO");
			frmStartup.GetThis().txtDoneStatus.Text = "PSTTAB " + myTable.Count + " Records loaded";
			frmStartup.GetThis().txtDoneStatus.Refresh();
			//GATTAB
			myTable = myDB.Bind("GAT", "GAT", "URNO,GNAM,GATR", "10,10,128", "URNO,GNAM,GATR");
			frmStartup.GetThis().txtCurrentStatus.Text = "Loading GATTAB (Airport GATES)";
			frmStartup.GetThis().txtCurrentStatus.Refresh();
			myTable.Load("ORDER BY GNAM");
			myTable.CreateIndex("GNAM", "GNAM");
			myTable.CreateIndex("URNO", "URNO");
			frmStartup.GetThis().txtDoneStatus.Text = "GATTAB " + myTable.Count + " Records loaded";
			frmStartup.GetThis().txtDoneStatus.Refresh();
			//SDETAB
			myTable = myDB.Bind("SDE", "SDE", "URNO,USRH,NAME,RETY,SDEU,AFTF,CCAF,UTPL,USRV,RTIM,FLDR,TIFR,CDAT,USEC,LSTU,USEU,DELF", 
								"10,10,32,4,10,4,4,10,10,1,4,14,32,14,32,1", 
								"URNO,USRH,NAME,RETY,SDEU,AFTF,CCAF,UTPL,USRV,RTIM,FLDR,TIFR,CDAT,USEC,LSTU,USEU,DELF");
			frmStartup.GetThis().txtCurrentStatus.Text = "Loading SDETAB (Status definitions)";
			frmStartup.GetThis().txtCurrentStatus.Refresh();
			myTable.Load("");
			myTable.CreateIndex("URNO", "URNO");
			myTable.CreateIndex("USRH", "USRH");
			frmStartup.GetThis().txtDoneStatus.Text = "SDETAB " + myTable.Count + " Records loaded";
			frmStartup.GetThis().txtDoneStatus.Refresh();

			//Load AFT and OST ==> separate function to ensure a reload of data
			LoadOnlineData(true);

			//tabAFT.WriteToFile("C:\\Ufis\\MWO_AFT.txt", true);
		}// End LoadData()

		private void frmData_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			tabVPD.WriteToFile("C:\\Ufis\\BV_VIEWPOINTS.dat", false);
			tabVPL.WriteToFile("C:\\Ufis\\BV_DETAILDEFS.dat", false);
			WriteTabsToFile();
			e.Cancel = true;
		}

		public void WriteTabsToFile()
		{
			//tabPositions.WriteToFile("C:\\Ufis\\BIRDVIEW_PSTTAB.dat", false);
			//tabAFT.WriteToFile("C:\\Ufis\\BIRDVIEW_AFTTAB.dat", false);
		}

		private void btnStoreViewpoints_Click(object sender, System.EventArgs e)
		{
			for(int i = 0; i < tabVPD.GetLineCount(); i++)
			{
				SaveAllViewPoints(tabVPD.GetFieldValue(i, "VIEW"));
			}
		}

		public void SaveAllViewPoints(string strViewName)
		{
			IDatabase myDB = UT.GetMemDB();
			ITable vpdTab = myDB["VPD"];
			ITable vplTab = myDB["VPL"];
			int i = 0;
			
			//First we delete all Viewpoints(VPD) and Viewpoint locations (VPL)
			IUfisComWriter myUfisCom = UT.GetUfisCom();
			int ilRet = myUfisCom.CallServer("DRT", "VPDTAB", "", "", "WHERE VNAM = '" + strViewName + "'", "230");
			if(ilRet != 0)
			{
				string strERR = myUfisCom.LastErrorMessage;
//				MessageBox.Show(this, strERR);
			}
			ilRet = myUfisCom.CallServer("DRT", "VPLTAB", "", "", "WHERE VNAM = '" + strViewName + "'", "230");
			if(ilRet != 0)
			{
				string strERR = myUfisCom.LastErrorMessage;
//				MessageBox.Show(this, strERR);
			}
//			for( i = vpdTab.Count-1; i >= 0; i--)
//			{
//				vpdTab[i].Status = State.Deleted;
//			}
//			for( i = vplTab.Count-1; i >= 0; i--)
//			{
//				vplTab[i].Status = State.Deleted;
//			}
//			vpdTab.Release(300,"LATE,NOBC,NOACTION,NOLOG", null);
//			vplTab.Release(300,"LATE,NOBC,NOACTION,NOLOG", null);
			vpdTab.Clear();
			vplTab.Clear();

			//Reinsert all Viewpoints(VPD) and Viewpoint locations (VPL)
			for( i = 0; i < tabVPD.GetLineCount(); i++)
			{
				if(tabVPD.GetFieldValue(i, "VIEW") == strViewName)
				{
					IRow row = vpdTab.CreateEmptyRow();
					//VIEW,FILE,GRID,DRAWMODE,IMAGESIZE,DEFTIMEFR ==> tab
					row["VNAM"] = tabVPD.GetFieldValue(i, "VIEW");
					row["FNAM"] = tabVPD.GetFieldValue(i, "FILE");
					row["GRID"] = tabVPD.GetFieldValue(i, "GRID");
					row["DMOD"] = tabVPD.GetFieldValue(i, "DRAWMODE");
					row["IMSZ"] = tabVPD.GetFieldValue(i, "IMAGESIZE");
					row["TIMF"] = tabVPD.GetFieldValue(i, "DEFTIMEFR");
					row["HOPO"] = UT.Hopo;
					row.Status = State.Created;
					vpdTab.Add(row);
				}
			}
			for( i = 0; i < tabVPL.GetLineCount(); i++)
			{
				if(tabVPL.GetFieldValue(i, "VIEW") == strViewName)
				{
					//VIEW,POS,X,Y,ALIGN,ROTATE,TYPE ==> tab
					//URNO,VNAM,POSI,XPOS,YPOS,ALIG,ROTA,TYPE ==> row
					IRow row = vplTab.CreateEmptyRow();
					row["VNAM"] = tabVPL.GetFieldValue(i, "VIEW");
					row["POSI"] = tabVPL.GetFieldValue(i, "POS");
					row["XPOS"] = tabVPL.GetFieldValue(i, "X");
					row["YPOS"] = tabVPL.GetFieldValue(i, "Y");
					row["ALIG"] = tabVPL.GetFieldValue(i, "ALIGN");
					row["ROTA"] = tabVPL.GetFieldValue(i, "ROTATE");
					row["TYPE"] = tabVPL.GetFieldValue(i, "TYPE");
					row["HOPO"] = UT.Hopo;
					row.Status = State.Created;
					vplTab.Add(row);
				}
			}
			vpdTab.Release(300,"LATE,NOBC,NOACTION,NOLOG", null);
			vplTab.Release(300,"LATE,NOBC,NOACTION,NOLOG", null);
		}

		private void btnLoadViewpointsFromFile_Click(object sender, System.EventArgs e)
		{
			IDatabase myDB = UT.GetMemDB();
			ITable vpdTab = myDB["VPD"];
			ITable vplTab = myDB["VPL"];
			int i = 0;
			
			//Read the definition from file
			tabVPD.ReadFromFile( "C:\\Ufis\\BV_VIEWPOINTS.dat"); 
			tabVPL.ReadFromFile("C:\\Ufis\\BV_DETAILDEFS.dat");
			//Insert it from the tabs into the memDB
			for( i = 0; i < tabVPD.GetLineCount(); i++)
			{
				IRow row = vpdTab.CreateEmptyRow();
				row["VNAM"] = tabVPD.GetFieldValue(i, "VIEW");
				row["FNAM"] = tabVPD.GetFieldValue(i, "FILE");
				row["GRID"] = tabVPD.GetFieldValue(i, "GRID");
				row["DMOD"] = tabVPD.GetFieldValue(i, "DRAWMODE");
				row["IMSZ"] = tabVPD.GetFieldValue(i, "IMAGESIZE");
				row["TIMF"] = tabVPD.GetFieldValue(i, "DEFTIMEFR");
				row["HOPO"] = UT.Hopo;
				row.Status = State.Unchanged;
				vpdTab.Add(row);
			}
			for( i = 0; i < tabVPL.GetLineCount(); i++)
			{
				IRow row = vplTab.CreateEmptyRow();
				row["VNAM"] = tabVPL.GetFieldValue(i, "VIEW");
				row["POSI"] = tabVPL.GetFieldValue(i, "POS");
				row["XPOS"] = tabVPL.GetFieldValue(i, "X");
				row["YPOS"] = tabVPL.GetFieldValue(i, "Y");
				row["ALIG"] = tabVPL.GetFieldValue(i, "ALIGN");
				row["ROTA"] = tabVPL.GetFieldValue(i, "ROTATE");
				row["TYPE"] = tabVPL.GetFieldValue(i, "TYPE");
				row["HOPO"] = UT.Hopo;
				row.Status = State.Unchanged;
				vplTab.Add(row);
			}
			vpdTab.CreateIndex("URNO", "URNO");
			vpdTab.CreateIndex("VNAM", "VNAM");
			vplTab.CreateIndex("URNO", "URNO");
			vplTab.CreateIndex("VNAM", "VNAM");

			DE.Call_BirdViewSaved();
		}
		public void LoadOnlineData(bool updateStartupForm)
		{
			LoadOstData(updateStartupForm);
			LoadAftData(updateStartupForm);
		}
		public void LoadOstData(bool updateStartupForm)
		{
			IDatabase myDB = UT.GetMemDB();
			string strWhere = "WHERE SDAY BETWEEN '" + UT.DateTimeToCeda(UT.TimeFrameFrom).Substring(0, 8) + "' AND '" + UT.DateTimeToCeda(UT.TimeFrameTo).Substring(0, 8) + "'";
			ITable myTable = myDB["OST"];
			if(myTable == null)
			{
				myTable = myDB.Bind("OST", "OST", 
					"URNO,USRH,USDE,SDAY,CONA,CONU,COTY,RTAB,RURN,RFLD,CONS,UAFT,LSTU,UHSS,REMA,USEU", 
					"10,10,10,8,14,14,1,3,10,4,14,10,14,10,64,32", 
					"URNO,USRH,USDE,SDAY,CONA,CONU,COTY,RTAB,RURN,RFLD,CONS,UAFT,LSTU,UHSS,REMA,USEU");
			}
			else
			{
				myTable.Clear();
			}
			myTable.TimeFields = "CONA,CONU,CONS";
			myTable.TimeFieldsInitiallyInUtc = true;
			myTable.TimeFieldsCurrentlyInUtc = true;
			frmStartup.GetThis().txtCurrentStatus.Text = "Loading OSTTAB (Online Status Information)";
			frmStartup.GetThis().txtCurrentStatus.Refresh();
			myTable.Load(strWhere);
			myTable.Sort("CONS", true);
			myTable.CreateIndex("URNO", "URNO");
			myTable.CreateIndex("USRH", "USRH");
			myTable.CreateIndex("UAFT", "UAFT");
			myTable.CreateIndex("USDE", "USDE");

			if(updateStartupForm == true)
			{
				frmStartup.GetThis().txtDoneStatus.Text = "OSTTAB " + myTable.Count + " Records loaded";
				frmStartup.GetThis().txtDoneStatus.Refresh();
			}
		}
		public void LoadAftData(bool updateStartupForm)
		{
			IDatabase myDB = UT.GetMemDB();
			int i = 0;
			ITable myTable = myDB["AFT"];
			ITable myTmpAFT = myDB["AFTTMP"];
			if(myTable == null)
			{
				myTable = myDB.Bind("AFT", 
					"AFT", 
					"URNO,RKEY,FTYP,ADID,FLNO,GTA1,GTD1,GTA2,GTD2,LAND,ONBS,PSTA,PSTD,STOA,STOD,TIFA,TIFD,TISA,TISD,TMOA,TTYP,ORG3,DES3,FLTI,ETAI,ETDI,ONBL,OFBL,VIA3,ACT3,PABA,PAEA,PABS,PAES,PDBA,PDEA,PDBS,PDES,GA1X,GA1Y,GA2X,GA2Y,GD1X,GD1Y,GD2X,GD2Y,GA1B,GA1E,GA2B,GA2E,GD1B,GD1E,GD2B,GD2E,VIAL,ADER,REGN,ALC2,ALC3,AIRB,STEV", 
					"10,10,6,2,10,5,5,5,5,14,14,5,5,14,14,14,14,2,2,14,5,5,5,10,14,14,14,14,5,5,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,1000,10,1",
					"URNO,RKEY,FTYP,ADID,FLNO,GTA1,GTD1,GTA2,GTD2,LAND,ONBS,PSTA,PSTD,STOA,STOD,TIFA,TIFD,TISA,TISD,TMOA,TTYP,ORG3,DES3,FLTI,ETAI,ETDI,ONBL,OFBL,VIA3,ACT3,PABA,PAEA,PABS,PAES,PDBA,PDEA,PDBS,PDES,GA1X,GA1Y,GA2X,GA2Y,GD1X,GD1Y,GD2X,GD2Y,GA1B,GA1E,GA2B,GA2E,GD1B,GD1E,GD2B,GD2E,VIAL,ADER,REGN,ALC2,ALC3,AIRB,STEV");
			}
			else
			{
				myTable.Clear();
			}
			if(myTmpAFT == null)
			{
				myTmpAFT = myDB.Bind("AFTTMP", 
					"AFT", 
					"URNO,RKEY,FTYP,ADID,FLNO,GTA1,GTD1,GTA2,GTD2,LAND,ONBS,PSTA,PSTD,STOA,STOD,TIFA,TIFD,TISA,TISD,TMOA,TTYP,ORG3,DES3,FLTI,ETAI,ETDI,ONBL,OFBL,VIA3,ACT3,PABA,PAEA,PABS,PAES,PDBA,PDEA,PDBS,PDES,GA1X,GA1Y,GA2X,GA2Y,GD1X,GD1Y,GD2X,GD2Y,GA1B,GA1E,GA2B,GA2E,GD1B,GD1E,GD2B,GD2E,VIAL,ADER,REGN,ALC2,ALC3,AIRB,STEV", 
					"10,10,6,2,10,5,5,5,5,14,14,5,5,14,14,14,14,2,2,14,5,5,5,10,14,14,14,14,5,5,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,1000,10,1",
					"URNO,RKEY,FTYP,ADID,FLNO,GTA1,GTD1,GTA2,GTD2,LAND,ONBS,PSTA,PSTD,STOA,STOD,TIFA,TIFD,TISA,TISD,TMOA,TTYP,ORG3,DES3,FLTI,ETAI,ETDI,ONBL,OFBL,VIA3,ACT3,PABA,PAEA,PABS,PAES,PDBA,PDEA,PDBS,PDES,GA1X,GA1Y,GA2X,GA2Y,GD1X,GD1Y,GD2X,GD2Y,GA1B,GA1E,GA2B,GA2E,GD1B,GD1E,GD2B,GD2E,VIAL,ADER,REGN,ALC2,ALC3,AIRB,STEV");
			}
			else
			{
				myTmpAFT.Clear();
			}
			// 14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,
			//GA1X,GA1Y,GA2X,GA2Y,GD1X,GD1Y,GD2X,GD2Y,GA1B,GA1E,GA2B,GA2E,GD1B,GD1E,GD2B,GD2E,

			//		CCS_FIELD_DATE(Ga1x,"GA1X","Belegung Gate 1 Ankunft aktueller Beginn", 1)
			//		CCS_FIELD_DATE(Ga1y,"GA1Y","Belegung Gate 1 Ankunft aktuelles Ende  ", 1)
			//		CCS_FIELD_DATE(Ga2x,"GA2X","Belegung Gate 2 Ankunft aktueller Beginn", 1)
			//		CCS_FIELD_DATE(Ga2y,"GA2Y","Belegung Gate 2 Ankunft aktuelles Ende  ", 1)
			//		CCS_FIELD_DATE(Gd1x,"GD1X","Belegung Gate 1 Abflug aktueller Beginn ", 1)
			//		CCS_FIELD_DATE(Gd1y,"GD1Y","Belegung Gate 1 Abflug aktuelles Ende   ", 1)
			//		CCS_FIELD_DATE(Gd2x,"GD2X","Belegung Gate 2 Abflug aktueller Beginn ", 1)
			//		CCS_FIELD_DATE(Gd2y,"GD2Y","Belegung Gate 2 Abflug aktuelles Ende   ", 1)
			//SCHEDULES
			//		CCS_FIELD_DATE(Ga1b,"GA1B","Belegung Gate 1 Ankunft geplant Beginn", 1)
			//		CCS_FIELD_DATE(Ga1e,"GA1E","Belegung Gate 1 Ankunft geplant Ende  ", 1)
			//		CCS_FIELD_DATE(Ga2b,"GA2B","Belegung Gate 2 Ankunft geplant Beginn", 1)
			//		CCS_FIELD_DATE(Ga2e,"GA2E","Belegung Gate 2 Ankunft geplant Ende  ", 1)
			//		CCS_FIELD_DATE(Gd1b,"GD1B","Belegung Gate 1 Abflug geplant Beginn ", 1)
			//		CCS_FIELD_DATE(Gd1e,"GD1E","Belegung Gate 1 Abflug geplant Ende   ", 1)
			//		CCS_FIELD_DATE(Gd2b,"GD2B","Belegung Gate 2 Abflug geplant Beginn ", 1)
			//		CCS_FIELD_DATE(Gd2e,"GD2E","Belegung Gate 2 Abflug geplant Ende   ", 1)
			myTable.Command("read",",GFR,");
			myTable.TimeFields = "STOA,STOD,TIFA,TIFD,LAND,TMOA,ETAI,ETDI,ONBL,OFBL,PABS,PAES,PDBS,PDES,PDBA,PDEA,PABA,PAEA";
			myTable.TimeFieldsInitiallyInUtc = true;
			myTable.TimeFieldsCurrentlyInUtc = true;

			frmStartup.GetThis().txtCurrentStatus.Text = "Loading AFTTAB (Flights)";
			frmStartup.GetThis().txtCurrentStatus.Refresh();
			string strWhere = "WHERE (TIFA BETWEEN '" + UT.DateTimeToCeda(UT.TimeFrameFrom) + "' AND '" + UT.DateTimeToCeda(UT.TimeFrameTo) + "') OR (TIFD BETWEEN '" + UT.DateTimeToCeda(UT.TimeFrameFrom) + "' and '" + UT.DateTimeToCeda(UT.TimeFrameTo) + "') AND FTYP IN ('S', 'O', 'X') [ROTATIONS]";

			myTable.Load(strWhere);
			myTable.CreateIndex("URNO", "URNO");
			myTable.CreateIndex("RKEY", "RKEY");
			myTable.Command("insert",",ISF,IFR,IDF,");
			myTable.Command("update",",UFR,UPS,UPJ,");
			myTable.Command("delete",",DFR,");
//*** same procedure for AFTTMP
			myTmpAFT.Command("read",",GFR,");
			myTmpAFT.TimeFields = "STOA,STOD,TIFA,TIFD,LAND,TMOA,ETAI,ETDI,ONBL,OFBL,PABS,PAES,PDBS,PDES,PDBA,PDEA,PABA,PAEA";
			myTmpAFT.TimeFieldsInitiallyInUtc = true;
			myTmpAFT.TimeFieldsCurrentlyInUtc = true;
			myTmpAFT.CreateIndex("URNO", "URNO");
			myTmpAFT.CreateIndex("RKEY", "RKEY");
			myTmpAFT.Command("insert",",,"); //No insert per broadcase
			myTmpAFT.Command("update",",,"); //No update per broadcase
			myTmpAFT.Command("delete",",,"); //No delete per broadcase

//*** END same procedure for AFTTMP
			if(updateStartupForm == true)
			{
				frmStartup.GetThis().txtDoneStatus.Text = "AFTTAB " + myTable.Count + " Records loaded";
				frmStartup.GetThis().txtDoneStatus.Refresh();
			}
			myTable.FillVisualizer(tabAFT);
			tabAFT.LogicalFieldList = "URNO,RKEY,FTYP,ADID,FLNO,GTA1,GTD1,LAND,ONBS,PSTA,PSTD,STOA,STOD,TIFA,TIFD,TISA,TISD,TMOA,TTYP,ORG3,DES3,FLTI,ETAI,ETDI,ONBL,OFBL,VIA3,ACT3,PABA,PAEA,PABS,PAES,PDBA,PDEA,PDBS,PDES,VIAL,ADER";
			tabAFT.AutoSizeByHeader = true;
			tabAFT.Sort("1", true, true);
			tabAFT.IndexCreate("URNO", 0);
			tabAFT.IndexCreate("RKEY", 1);
			tabAFT.AutoSizeColumns();

			myDB.IndexUpdateImmediately = true;
			//Load all flights which are referenced by OST but currently not loaded
			ArrayList arrToLoad = new ArrayList(10000);
			myTable = myDB["OST"];
			ITable aftTab = myDB["AFT"];
			for( i = 0; i < myTable.Count; i++)
			{
				string strAftUrno = myTable[i]["UAFT"];
				IRow [] aftRows = aftTab.RowsByIndexValue("URNO", strAftUrno);
				if(aftRows.Length == 0)
				{
					arrToLoad.Add(strAftUrno);
				}
			}
			int cnt = arrToLoad.Count;
			strWhere = "WHERE URNO IN (";
			for( i = 0;  i < arrToLoad.Count; i++)
			{
				if( (i % 300) == 0 )
				{
					strWhere = strWhere + arrToLoad[i].ToString() + ") AND FTYP IN ('S', 'O', 'X') [ROTATIONS]";
					aftTab.Load(strWhere);
					strWhere = "WHERE URNO IN (";
					if(updateStartupForm == true)
					{
						frmStartup.GetThis().txtDoneStatus.Text = "Status reload AFTTAB " + aftTab.Count + " Records loaded";
						frmStartup.GetThis().txtDoneStatus.Refresh();
					}
				}
				else
				{
					if( i == (arrToLoad.Count - 1))
					{
						strWhere = strWhere + arrToLoad[i].ToString() + ") AND FTYP IN ('S', 'O', 'X') [ROTATIONS]";
					}
					else
					{
						strWhere = strWhere + arrToLoad[i].ToString() + ",";
					}
				}
			}
			if(strWhere != "WHERE URNO IN (")
				aftTab.Load(strWhere);

			ReorgDuplicate();
			PreparePositionAllocationTimes();

			if(updateStartupForm == true)
			{
				frmStartup.GetThis().txtDoneStatus.Text = "Status reload AFTTAB " + aftTab.Count + " Records loaded";
				frmStartup.GetThis().txtDoneStatus.Refresh();
			}
		}
		public void ReorgDuplicate()
		{
			int i = 0;
			IDatabase myDB = UT.GetMemDB();
			ITable myTable = myDB["AFT"];
			myTable.Sort("URNO", true);
			for( i = myTable.Count-1; i >= 0; i--)
			{
				if((i-1) >= 0)
				{
					if(myTable[i]["URNO"] == myTable[i-1]["URNO"])
					{
						myTable.Remove(i);
					}
				}
			}
		
		}
		public void PreparePositionAllocationTimes()
		{
			int i = 0;
			int cntTotal = 0;
			IDatabase myDB = UT.GetMemDB();
			ITable myTable = myDB["AFT"];
			myTable.Sort("RKEY", true);
			while (i < myTable.Count)
			{
				int incCount = 1;
				IRow rowArrival = null;
				IRow rowDeparture = null;
				if(i+1 < myTable.Count)
				{
					if(myTable[i]["FLNO"] == "SQ 226" ||  myTable[i+1]["FLNO"] == "SQ 226")
					{
						//string dbg = "";
						//dbg = "0";
					}
					if(myTable[i]["RKEY"] == myTable[i+1]["RKEY"])
					{
						incCount = 2;
						if(myTable[i]["ADID"] == "A")
						{
							rowArrival = myTable[i];
							if(myTable[i+1]["ADID"] == "D")
							{
								rowDeparture = myTable[i+1];
							}
						}
						if(myTable[i]["ADID"] == "D")
						{
							rowDeparture = myTable[i];
							if(myTable[i+1]["ADID"] == "A")
							{
								rowArrival = myTable[i+1];
							}
						}
						if( rowArrival != null && rowDeparture != null)
						{
							DateTime tifd = UT.CedaFullDateToDateTime(rowDeparture["TIFD"]);
							DateTime depFrom = tifd.Subtract(new TimeSpan(0, 1, 15, 0, 0));
							rowDeparture["PDBS"] = UT.DateTimeToCeda(depFrom);
							if(rowDeparture["TISD"] == "O" || rowDeparture["TISD"] == "A")
							{
								rowDeparture["PDES"] = rowDeparture["TIFD"];
							}
							else
							{
								DateTime depTo = DateTime.Now;
								depTo = depTo.AddDays(5);
								rowDeparture["PDES"] = UT.DateTimeToCeda(depTo);
							}

							rowArrival["PABS"] = rowArrival["TIFA"];
							rowArrival["PAES"] = rowDeparture["PDBS"];
							cntTotal++;
						}
					}//End if(myTable[i]["RKEY"] == myTable[i+1]["RKEY"])
					else
					{
						if(myTable[i]["ADID"] == "A" || myTable[i]["TISA"] == "O")
						{
							DateTime arrTo = UT.CedaFullDateToDateTime(myTable[i]["TIFA"]);
							arrTo = arrTo.AddDays(2);
							myTable[i]["PABS"] = myTable[i]["TIFA"];
							myTable[i]["PAES"] = UT.DateTimeToCeda(arrTo);
						}
					}
				}
				i += incCount;				
			}
		}
	}
}
