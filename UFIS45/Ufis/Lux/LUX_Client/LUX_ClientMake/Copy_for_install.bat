rem Copies all generated files from the SCM-Structue into the Install-Shield directory.


rem set drive to J: (IS 6.2) or K: (IS 8.0) 
set drive=k:

rem Copy ini- and config-files
copy c:\ufis_bin\ceda.ini %drive%\lux_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\bdps_sec.cfg %drive%\lux_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\loatab*.* %drive%\lux_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\telexpool.ini %drive%\lux_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\*.bmp %drive%\lux_build\ufis_client_appl\system\*.*

rem copy applications
copy c:\ufis_bin\debug\bdps_sec.exe %drive%\lux_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpspass.exe %drive%\lux_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpsuif.exe %drive%\lux_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\fips.exe %drive%\lux_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\rules.exe %drive%\lux_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\telexpool.exe %drive%\lux_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\loadtabviewer.exe %drive%\lux_build\ufis_client_appl\applications\*.*

REM copy system-files
copy c:\ufis_bin\release\bcproxy.exe %drive%\lux_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcproxy.tlb %drive%\lux_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcserv32.exe %drive%\lux_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufis32.dll %drive%\lux_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.exe %drive%\lux_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.tlb %drive%\lux_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.ocx %drive%\lux_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.tlb %drive%\lux_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.ocx %drive%\lux_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.tlb %drive%\lux_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.ocx %drive%\lux_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.tlb %drive%\lux_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\aatlogin.ocx %drive%\lux_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\aatlogin.tlb %drive%\lux_build\ufis_client_appl\system\*.*

rem copy interop dll's
rem copy c:\ufis_bin\release\*lib.dll %drive%\lux_build\ufis_client_appl\applications\interopdll\*.*
rem copy c:\ufis_bin\release\ufis.utils.dll %drive%\lux_build\ufis_client_appl\applications\interopdll\*.*

rem Copy Help-Files
rem copy c:\ufis_bin\Help\*.chm %drive%\lux_build\ufis_client_appl\help\*.*
