// PsUnifilter.cpp : implementation file
//

#include <stdafx.h>
//#include "ccsitem.h"
#include <CCSGlobl.h>
#include <PSUniFilterPage.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPsUniFilter property page

IMPLEMENT_DYNCREATE(CPsUniFilter, CPropertyPage)

CPsUniFilter::CPsUniFilter() : CPropertyPage(CPsUniFilter::IDD)
{
	//{{AFX_DATA_INIT(CPsUniFilter)
	m_EditBedingung = _T("");
	m_EditFilter = _T("");
	//m_ButtonConnect = 0;
	//}}AFX_DATA_INIT
	//pomPageBuffer = (CPageBuffer *)pomDataBuffer;
}

CPsUniFilter::~CPsUniFilter()
{
	//pomPageBuffer=NULL;
	//pomDataBuffer = NULL;
}

void CPsUniFilter::SetCaption(const char *pcpCaption)
{
	m_psp.pszTitle = pcpCaption;
	m_psp.dwFlags |= PSP_USETITLE;
}

BOOL CPsUniFilter::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	//if (HIWORD(wParam) == BN_CLICKED || HIWORD(wParam) == EN_SETFOCUS)
	//	CancelToClose();
	return CPropertyPage::OnCommand(wParam, lParam);
}

void CPsUniFilter::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPsUniFilter)
	DDX_Control(pDX, IDC_COMBO_Datenfeld, m_ComboDatenfeld);
	DDX_Control(pDX, IDC_COMBO_Operator, m_ComboOperator);
	DDX_Control(pDX, IDC_EDIT_Bedingung, m_CtrlBedingung);
	//DDX_Control(pDX, IDC_EDIT_Filter, E_EditFilter);
	DDX_Text(pDX, IDC_EDIT_Filter, m_EditFilter);
	DDX_Control(pDX, IDC_EDIT_Filter, m_Where);
	//DDX_Radio(pDX, IDC_RADIO_AND, m_ButtonConnect);
	DDX_Control(pDX, IDC_RADIO_AND, m_And);
	DDX_Control(pDX, IDC_RADIO_OR, m_Or);
	//}}AFX_DATA_MAP
	if (pDX->m_bSaveAndValidate == FALSE)
	{
		SetData();
	}
	if (pDX->m_bSaveAndValidate == TRUE)
	{
		GetData();
	}
}


BEGIN_MESSAGE_MAP(CPsUniFilter, CPropertyPage)
	//{{AFX_MSG_MAP(CPsUniFilter)
	ON_BN_CLICKED(IDC_BUTTON_Neu, OnBUTTONNeu)
	ON_BN_CLICKED(IDC_BUTTON_Setzen, OnBUTTONSetzen)
	ON_CBN_SELCHANGE(IDC_COMBO_Datenfeld, OnSelchangeCOMBODatenfeld)
	ON_CBN_SELCHANGE(IDC_COMBO_Operator, OnSelchangeCOMBOOperator)
	ON_BN_CLICKED(IDC_RADIO_AND, OnRadioAnd)
	ON_BN_CLICKED(IDC_RADIO_OR, OnRadioOr)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPsUniFilter message handlers

void CPsUniFilter::SetCalledFrom(CString opCalledFrom)
{
	omCalledFrom = opCalledFrom;
}
void CPsUniFilter::OnBUTTONNeu() 
{
	omFilter.Empty();
	m_Where.SetWindowText("");
}

void CPsUniFilter::OnBUTTONSetzen() 
{
	// TODO: Add your control notification handler code here
	// TODO: Add your control notification handler code here
	CString olFilter;
	//CString olTabelle, olDatenfeld, olOperator, olValue;

	// TODO: Add your control notification handler code here
	//UpdateData(TRUE);
	CString olOperator;
	CString olField;
	CString olValue;
	CString olPercent;
	m_ComboOperator.GetWindowText(olOperator);
	int ilIdx = m_ComboDatenfeld.GetCurSel();
	if(ilIdx != LB_ERR)
	{
		olField = omFieldArray[ilIdx];
	}
	else
	{
		return;
	}
	m_CtrlBedingung.GetWindowText(olValue);

	if(ilIdx != LB_ERR)
	{	if(omTypeArray[ilIdx] == "Date" && olOperator != "EMPTY")
		{
			if(ReverseDateString(olValue) == false)
			{
				MessageBox(GetString(IDS_STRING917), GetString(IDS_WARNING), (MB_ICONEXCLAMATION| MB_OK));
				return;
			}
		}
	}
	omConnection == "";
	if(m_And.GetCheck() == 1)
	{
		omConnection = "AND";
	}
	else if(m_Or.GetCheck() == 1)
	{
		omConnection = "OR";
	}

	if(omConnection.IsEmpty())
		return;
	if(olOperator == " LIKE ")
	{
		olPercent = CString("%");
	}
	if (olOperator == "EMPTY")
	{
		olOperator = " = ";
		olValue = " ";
	}
	olFilter = olField + olOperator + CString("'") + olValue + olPercent + CString("'");

	CWnd *polWnd = GetDlgItem(IDC_EDIT_Filter);
	if(polWnd != NULL)
		polWnd->GetWindowText(m_EditFilter);
	if(m_EditFilter.IsEmpty())
	{
		if(olField.IsEmpty() || olValue.IsEmpty() || olOperator.IsEmpty())
		{
			return;
		}
		else
		{
			m_EditFilter = olFilter;
		}
	}
	else if (m_EditFilter.GetLength() > 0)
	{
		if(olField.IsEmpty() || olValue.IsEmpty() || olOperator.IsEmpty())
		{
			return;
		}
		else
		{
			m_EditFilter+= " " + omConnection + " " + olFilter;
		}
	} // end if

	if(polWnd != NULL)
	{
		polWnd->SetWindowText(m_EditFilter);
		CEdit *polEdit = (CEdit*) GetDlgItem(IDC_EDIT_Filter);
		if (polEdit)
			polEdit->LineScroll(polEdit->GetLineCount(), 0 );
	}
	//E_EditFilter.SetWindowText(m_EditFilter);
	//UpdateData(FALSE);

}



void CPsUniFilter::OnSelchangeCOMBOOperator() 
{
	CString olOperator;
	m_ComboOperator.GetWindowText(olOperator);

	if (olOperator == "EMPTY")
	{
		m_CtrlBedingung.EnableWindow(false);
	}
	else
	{
		m_CtrlBedingung.EnableWindow(true);
	}

}









void CPsUniFilter::OnSelchangeCOMBOTabelle() 
{
	// TODO: Add your control notification handler code here

	
/*	

  
	olTana;
    int ilSize = 0, ilIdx = 0;
	BOOL ilFound = FALSE;

	if(blIsInit)
	{
		UpdateData(FALSE);
		if (pomPageBuffer != NULL)
		{
			m_ComboTabelle.GetLBText((int)m_ComboTabelle.GetCurSel(), olTana);
			if (pomPageBuffer->GetInitTableIdx(olTana) >= 0)
			{
				ilFound = TRUE;
			} // end if
			else
			{
				ilIdx++;
			} // end else if
			if (ilFound == TRUE)
				InitialValues(ilIdx);
		} // end if 

	   UpdateData(TRUE);
	}
*/	
}

void CPsUniFilter::OnSelchangeCOMBODatenfeld() 
{
 
/*	CString olDaten;
	CString olTana;
	CString olString;
//-- hole Tabellen ID
//	m_ComboTabelle.GetLBText((int)m_ComboTabelle.GetCurSel(), olTana);
	int ilTableIdx = 0; //pomPageBuffer->GetInitTableIdx(CString(olTana));

//-- hole Feld-ID
	int ilSel = m_ComboDatenfeld.GetCurSel();
	if (ilSel >= 0)
	{
		m_ComboDatenfeld.GetLBText(ilSel, olDaten);
// in comboBox steht feld .. beschreibung ==> nehme nur erstes Wort
		CString olFeldName;
		olDaten.strgGetWords(1,1,&olFeldName,' ');
		ilSel = pomPageBuffer->GetInitFieldIdx(ilTableIdx, olFeldName);
// Setze Operatoren neu
		m_ComboOperator.ResetContent();
		olString = pomPageBuffer->GetInitOperator(ilTableIdx,ilSel);
		CCSItem olItem;
		olItem.strgSetValue(olString);
		int ilMaxItem = olItem.itemCount();
		for (int ilIdx = 1; ilIdx < ilMaxItem; ilIdx++)
		{
			m_ComboOperator.AddString((olItem.itemGetValue(ilIdx)).strgGetValue()); 
		} 
	} // end if ilSel
//-- hole Formatliste f�r Tabellen-ID, Feld-ID
//-- Setze Format von m_CtrlBedingung um
*/
} // end OnSelchangeCOMBODatenfeld

void CPsUniFilter::InitMask()
{
	SetFieldAndDescriptionArrays();
	int ilCount = omFieldArray.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		char pclText[200]=""; 
		sprintf(pclText, "%s...%s", omFieldArray[i], omDescArray[i]);
		m_ComboDatenfeld.AddString(pclText);
	}
	ilCount = omOperators.GetSize();
	for(i = 0; i < ilCount; i++)
	{
		m_ComboOperator.AddString(omOperators[i]);
	}
} 


/*int CPsUniFilter::CreateFilter(CString &ropFilter, 
							  CString &ropTabelle,
							  CString &ropDatenfeld,
							  CString &ropOperator,
							  CString &ropValue)
{
	int ilRc = -1, ilSel=-1;
	CString olFilter, olStringFilter;

	ilSel = m_ComboDatenfeld.GetCurSel();
	if (ilSel >= 0)
	{
		m_ComboDatenfeld.GetLBText(ilSel, ropDatenfeld);
	} // end if ilSel
	ilSel = m_ComboOperator.GetCurSel();
	if (ilSel >= 0)
	{
		m_ComboOperator.GetLBText(ilSel, ropOperator);
	} // end if GetCurSel()

    m_CtrlBedingung.GetWindowText(m_EditBedingung);
	if (ropOperator.GetLength() > 0 && ropDatenfeld.GetLength() > 0)
	{
		ilRc = 0;
		olStringFilter.strgSetValue((char *)ropDatenfeld.GetBuffer(ropDatenfeld.GetLength()));
		olStringFilter.strgGetWords(1,1,&olFilter,' ');
		ropDatenfeld.Empty();
		ropDatenfeld = olFilter;

		ropFilter = olFilter.strgGetValue();
		ropFilter+= ropOperator + CString("'") + m_EditBedingung + CString("'");
	} // end if
	ropValue = m_EditBedingung;

	return(ilRc);
} // end CreateFilter
*/
BOOL CPsUniFilter::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
 	m_ComboDatenfeld.SetFont(&ogCourier_Regular_8);
	InitMask();
	m_And.SetCheck(1);

	CEdit *polEdit = (CEdit*) GetDlgItem(IDC_EDIT_Filter);//CWndReadOnly
	if (polEdit)
	{
		polEdit->SetReadOnly(!bgViewEditFilter);
	//	polEdit->SetSel(-1,-1);
	}


	// TODO: Add extra initialization here
/*	if (pomPageBuffer == NULL)
		pomPageBuffer = (CPageBuffer *)pomDataBuffer;
	InitialValues(-1);

	m_ButtonConnect=0;
*/
	//UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CPsUniFilter::OnRadioAnd() 
{
	// TODO: Add your control notification handler code here
	omConnection = "AND";	
}

void CPsUniFilter::OnRadioOr() 
{
	// TODO: Add your control notification handler code here
	omConnection = "OR";
}

/*void CPsUniFilter::InitialScreen()
{
	CPageBuffer *polPageBuffer = (CPageBuffer *)pomInitBuffer;
	if (polPageBuffer != NULL)
	{
		polPageBuffer->GetString(0,m_EditFilter);
		UpdateData(FALSE);
	} // end if
	
} // InitialScreen

void *CPsUniFilter::GetData()
{
	return (void *)pomPageBuffer;
}
*/

void CPsUniFilter::SetData()
{
	m_EditFilter.Empty();//CStringArray
	for(int i = 0; i< omValues.GetSize(); i++)
	{
		CString olol = omValues.GetAt(i);
		if (i == 0)
			m_EditFilter += omValues[i];
	}
	int ilIdx;
	while((ilIdx = m_EditFilter.Find('@')) != -1)
	{
		m_EditFilter.SetAt(ilIdx, ' ');
	}
	m_EditFilter.TrimLeft(); m_EditFilter.TrimRight();
	CWnd *polWnd = GetDlgItem(IDC_EDIT_Filter);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText("");
		polWnd->SetWindowText(m_EditFilter);
	}
	
}
void CPsUniFilter::GetData()
{
	omValues.RemoveAll();
	if(!m_EditFilter.IsEmpty())
	{
		m_EditFilter += CString("@");
	}
	omValues.Add(m_EditFilter);
}


bool CPsUniFilter::ReverseDateString(CString &ropSource)
{
	
	if(ropSource.GetLength() < 4)
		return false;


	//Eventuelle Punkte und Doppelpunkte rausfiltern
	StripSign(ropSource, '.');
	StripSign(ropSource, ' ');
	StripSign(ropSource, ':');
	if(ropSource.Find('%') == -1)
	{

		//String auf 14 Stellen mit 0 auff�llen
		int ilDiff = 15 - ropSource.GetLength();
		for(int i = 0; i < ilDiff; i++)
		{
			ropSource += "0";
		}
		if(atoi(ropSource.Mid(4,4)) < 1970)
			return false;
		if(atoi(ropSource.Mid(2,2)) > 12)
			return false;
		if(atoi(ropSource.Mid(0,2)) > 31)
			return false;
		if(atoi(ropSource.Mid(8,2)) > 23)
			return false;
		if(atoi(ropSource.Mid(10,2)) > 59)
			return false;
		ropSource = ropSource.Mid(4,4) + ropSource.Mid(2,2) + ropSource.Mid(0,2) +
				  ropSource.Mid(8,2) + ropSource.Mid(10,2) + ropSource.Right(2);
	}
	return true;
}

void CPsUniFilter::StripSign(CString &ropString, char cpChar)
{
	CString olSubString = ropString;
	ropString.Empty();
	if(!olSubString.IsEmpty())
	{
		int pos;
		int olPos = 0;
		bool blEnd = false;
		CString olText;
		while(blEnd == false)
		{
			pos = olSubString.Find(cpChar);
			if(pos == -1)
			{
				blEnd = true;
				olText = olSubString;
			}
			else
			{
				olText = olSubString.Mid(0, olSubString.Find(cpChar));
				olSubString = olSubString.Mid(olSubString.Find(cpChar)+1, olSubString.GetLength( )-olSubString.Find(cpChar)+1);
			}
			ropString += olText;
		}
	}
}




///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////



void CPsUniFilter::SetFieldAndDescriptionArrays()
{
	omOperators.Add(" = ");
	omOperators.Add(" <= ");
	omOperators.Add(" >= ");
	omOperators.Add(" < ");
	omOperators.Add(" > ");
	omOperators.Add(" <> ");
	omOperators.Add(" LIKE ");
	omOperators.Add("EMPTY");
	
	if(omCalledFrom == "SEASON")
	{
		omFieldArray.Add("ACT3");
		omFieldArray.Add("ACT5");
		omFieldArray.Add("ALC2");
		omFieldArray.Add("ALC3");
		omFieldArray.Add("B1BS");
		omFieldArray.Add("B1ES");
		omFieldArray.Add("B2BS");
		omFieldArray.Add("B2ES");
		omFieldArray.Add("BLT1");
		omFieldArray.Add("BLT2");
		omFieldArray.Add("CDAT");
		omFieldArray.Add("CKIF");
		omFieldArray.Add("CKIT");
		omFieldArray.Add("DES3");
		omFieldArray.Add("DES4");
		omFieldArray.Add("DOOA");
		omFieldArray.Add("DOOD");
		omFieldArray.Add("ETAI");
		omFieldArray.Add("ETAU");
		omFieldArray.Add("ETDI");
		omFieldArray.Add("ETDU");
		omFieldArray.Add("ETOA");
		omFieldArray.Add("ETOD");
		omFieldArray.Add("EXT1");
		omFieldArray.Add("EXT2");
		omFieldArray.Add("FDAT");
		omFieldArray.Add("FLNO");
		omFieldArray.Add("FLNS");
		omFieldArray.Add("FLTN");
		omFieldArray.Add("FTYP");
		omFieldArray.Add("GA1B");
		omFieldArray.Add("GA1E");
		omFieldArray.Add("GA2B");
		omFieldArray.Add("GA2E");
		omFieldArray.Add("GD1B");
		omFieldArray.Add("GD1E");
		omFieldArray.Add("GD2B");
		omFieldArray.Add("GD2E");
		omFieldArray.Add("GTA1");
		omFieldArray.Add("GTA2");
		omFieldArray.Add("GTD1");
		omFieldArray.Add("GTD2");
		omFieldArray.Add("HTYP");
		omFieldArray.Add("ISRE");
		omFieldArray.Add("JCNT");
		omFieldArray.Add("JFNO");
		omFieldArray.Add("LSTU");
		omFieldArray.Add("MING");
		omFieldArray.Add("ORG3");
		omFieldArray.Add("ORG4");
		omFieldArray.Add("PABS");
		omFieldArray.Add("PAES");
		omFieldArray.Add("PDBS");
		omFieldArray.Add("PDES");
		omFieldArray.Add("PSTA");
		omFieldArray.Add("PSTD");
		omFieldArray.Add("REM1");
		omFieldArray.Add("RTYP");
		omFieldArray.Add("SEAS");
		omFieldArray.Add("STAT");
		omFieldArray.Add("STEV");
		omFieldArray.Add("STHT");
		omFieldArray.Add("STOA");
		omFieldArray.Add("STOD");
		omFieldArray.Add("STTT");
		omFieldArray.Add("STYP");
		omFieldArray.Add("TET1");
		omFieldArray.Add("TET2");
		omFieldArray.Add("TGA1");
		omFieldArray.Add("TGA2");
		omFieldArray.Add("TGD1");
		omFieldArray.Add("TGD2");
		omFieldArray.Add("TIFA");
		omFieldArray.Add("TIFD");
		omFieldArray.Add("TISA");
		omFieldArray.Add("TISD");
		omFieldArray.Add("TMB1");
		omFieldArray.Add("TMB2");
		omFieldArray.Add("TTYP");
		omFieldArray.Add("TWR1");
		omFieldArray.Add("USEC");
		omFieldArray.Add("USEU");
		omFieldArray.Add("VERS");
		omFieldArray.Add("VIAL");
		omFieldArray.Add("VIAN");
		omFieldArray.Add("W1BS");
		omFieldArray.Add("W1ES");
		omFieldArray.Add("WRO1");

		omDescArray.Add(GetString(IDS_STRING739));
		omDescArray.Add(GetString(IDS_STRING524));
		omDescArray.Add(GetString(IDS_STRING740));
		omDescArray.Add(GetString(IDS_STRING741));
		omDescArray.Add(GetString(IDS_STRING742));
		omDescArray.Add(GetString(IDS_STRING743));
		omDescArray.Add(GetString(IDS_STRING744));
		omDescArray.Add(GetString(IDS_STRING745));
		omDescArray.Add(GetString(IDS_STRING746));
		omDescArray.Add(GetString(IDS_STRING747));
		omDescArray.Add(GetString(IDS_STRING748));
		omDescArray.Add(GetString(IDS_STRING749));
		omDescArray.Add(GetString(IDS_STRING750));
		omDescArray.Add(GetString(IDS_STRING751));
		omDescArray.Add(GetString(IDS_STRING752));
		omDescArray.Add(GetString(IDS_STRING753));
		omDescArray.Add(GetString(IDS_STRING754));
		omDescArray.Add(GetString(IDS_STRING755));
		omDescArray.Add(GetString(IDS_STRING756));
		omDescArray.Add(GetString(IDS_STRING757));
		omDescArray.Add(GetString(IDS_STRING758));
		omDescArray.Add(GetString(IDS_STRING759));
		omDescArray.Add(GetString(IDS_STRING760));
		omDescArray.Add(GetString(IDS_STRING761));
		omDescArray.Add(GetString(IDS_STRING762));
		omDescArray.Add(GetString(IDS_STRING763));
		omDescArray.Add(GetString(IDS_STRING764));
		omDescArray.Add(GetString(IDS_STRING765));
		omDescArray.Add(GetString(IDS_STRING766));
		omDescArray.Add(GetString(IDS_STRING767));
		omDescArray.Add(GetString(IDS_STRING768));
		omDescArray.Add(GetString(IDS_STRING769));
		omDescArray.Add(GetString(IDS_STRING770));
		omDescArray.Add(GetString(IDS_STRING771));
		omDescArray.Add(GetString(IDS_STRING772));
		omDescArray.Add(GetString(IDS_STRING773));
		omDescArray.Add(GetString(IDS_STRING774));
		omDescArray.Add(GetString(IDS_STRING775));
		omDescArray.Add(GetString(IDS_STRING776));
		omDescArray.Add(GetString(IDS_STRING777));
		omDescArray.Add(GetString(IDS_STRING778));
		omDescArray.Add(GetString(IDS_STRING779));
		omDescArray.Add(GetString(IDS_STRING780));
		omDescArray.Add(GetString(IDS_STRING526));
		omDescArray.Add(GetString(IDS_STRING781));
		omDescArray.Add(GetString(IDS_STRING782));
		omDescArray.Add(GetString(IDS_STRING783));
		omDescArray.Add(GetString(IDS_STRING453));
		omDescArray.Add(GetString(IDS_STRING784));
		omDescArray.Add(GetString(IDS_STRING785));
		omDescArray.Add(GetString(IDS_STRING786));
		omDescArray.Add(GetString(IDS_STRING787));
		omDescArray.Add(GetString(IDS_STRING788));
		omDescArray.Add(GetString(IDS_STRING789));
		omDescArray.Add(GetString(IDS_STRING790));
		omDescArray.Add(GetString(IDS_STRING791));
		omDescArray.Add(GetString(IDS_STRING792));
		omDescArray.Add(GetString(IDS_STRING793));
		omDescArray.Add(GetString(IDS_STRING794));
		omDescArray.Add(GetString(IDS_STRING795));
		omDescArray.Add(GetString(IDS_STRING796));
		omDescArray.Add(GetString(IDS_STRING797));
		omDescArray.Add(GetString(IDS_STRING798));
		omDescArray.Add(GetString(IDS_STRING799));
		omDescArray.Add(GetString(IDS_STRING800));
		omDescArray.Add(GetString(IDS_STRING801));
		omDescArray.Add(GetString(IDS_STRING802));
		omDescArray.Add(GetString(IDS_STRING803));
		omDescArray.Add(GetString(IDS_STRING804));
		omDescArray.Add(GetString(IDS_STRING805));
		omDescArray.Add(GetString(IDS_STRING806));
		omDescArray.Add(GetString(IDS_STRING807));
		omDescArray.Add(GetString(IDS_STRING808));
		omDescArray.Add(GetString(IDS_STRING809));
		omDescArray.Add(GetString(IDS_STRING810));
		omDescArray.Add(GetString(IDS_STRING811));
		omDescArray.Add(GetString(IDS_STRING812));
		omDescArray.Add(GetString(IDS_STRING813));
		omDescArray.Add(GetString(IDS_STRING814));
		omDescArray.Add(GetString(IDS_STRING815));
		omDescArray.Add(GetString(IDS_STRING816));
		omDescArray.Add(GetString(IDS_STRING817));
		omDescArray.Add(GetString(IDS_STRING818));
		omDescArray.Add(GetString(IDS_STRING819));
		omDescArray.Add(GetString(IDS_STRING820));
		omDescArray.Add(GetString(IDS_STRING821));
		omDescArray.Add(GetString(IDS_STRING822));
		omDescArray.Add(GetString(IDS_STRING823));

		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("Date");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("Date");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("Date");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("String");
	}

///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

	if(omCalledFrom == "ROTATION" || omCalledFrom == "DAILY" || omCalledFrom == "FLIGHTSEARCH")
	{
		omFieldArray.Add("ACT3");
		omFieldArray.Add("ACT5");
		omFieldArray.Add("AIRB");
		omFieldArray.Add("AIRU");
		omFieldArray.Add("ALC2");
		omFieldArray.Add("ALC3");
		omFieldArray.Add("B1BA");
		omFieldArray.Add("B1EA");
		omFieldArray.Add("B2BA");
		omFieldArray.Add("B2EA");
		omFieldArray.Add("BAGN");
		omFieldArray.Add("BLT1");
		omFieldArray.Add("BLT2");
		omFieldArray.Add("CDAT");
		omFieldArray.Add("CGOT");
		omFieldArray.Add("CKIF");
		omFieldArray.Add("CKIT");
		omFieldArray.Add("CSGN");
		omFieldArray.Add("CTOT");
		omFieldArray.Add("DCD1");
		omFieldArray.Add("DCD2");
		omFieldArray.Add("DES3");
		omFieldArray.Add("DES4");
		omFieldArray.Add("DIVR");
		omFieldArray.Add("DOOA");
		omFieldArray.Add("DOOD");
		omFieldArray.Add("DTD1");
		omFieldArray.Add("DTD2");
		omFieldArray.Add("ETAC");
		omFieldArray.Add("ETAI");
		omFieldArray.Add("ETAU");
		omFieldArray.Add("ETDC");
		omFieldArray.Add("ETDI");
		omFieldArray.Add("ETDU");
		omFieldArray.Add("ETOA");
		omFieldArray.Add("ETOD");
		omFieldArray.Add("EXT1");
		omFieldArray.Add("EXT2");
		omFieldArray.Add("FDAT");
		omFieldArray.Add("FLNO");
		omFieldArray.Add("FLNS");
		omFieldArray.Add("FLTN");
		omFieldArray.Add("FTYP");
		omFieldArray.Add("GA1X");
		omFieldArray.Add("GA1Y");
		omFieldArray.Add("GA2X");
		omFieldArray.Add("GA2Y");
		omFieldArray.Add("GD1X");
		omFieldArray.Add("GD1Y");
		omFieldArray.Add("GD2X");
		omFieldArray.Add("GD2Y");
		omFieldArray.Add("GTA1");
		omFieldArray.Add("GTA2");
		omFieldArray.Add("GTD1");
		omFieldArray.Add("GTD2");
		omFieldArray.Add("HTYP");
		omFieldArray.Add("IFRA");
		omFieldArray.Add("IFRD");
		omFieldArray.Add("ISKD");
		omFieldArray.Add("ISRE");
		omFieldArray.Add("JCNT");
		omFieldArray.Add("JFNO");
		omFieldArray.Add("LAND");
		omFieldArray.Add("LNDU");
		omFieldArray.Add("LSTU");
		omFieldArray.Add("MAIL");
		omFieldArray.Add("MING");
		omFieldArray.Add("NXTI");
		omFieldArray.Add("OFBL");
		omFieldArray.Add("OFBU");
		omFieldArray.Add("ONBE");
		omFieldArray.Add("ONBL");
		omFieldArray.Add("ONBU");
		omFieldArray.Add("ORG3");
		omFieldArray.Add("ORG4");
		omFieldArray.Add("PABA");
		omFieldArray.Add("PAEA");
		omFieldArray.Add("PAID");
		omFieldArray.Add("PAX1");
		omFieldArray.Add("PAX2");
		omFieldArray.Add("PAX3");
		omFieldArray.Add("PDBA");
		omFieldArray.Add("PDEA");
		omFieldArray.Add("PRFL");
		omFieldArray.Add("PSTA");
		omFieldArray.Add("PSTD");
		omFieldArray.Add("RACO");
		omFieldArray.Add("REGN");
		omFieldArray.Add("REM1");
		omFieldArray.Add("REMP");
		omFieldArray.Add("RTYP");
		omFieldArray.Add("RWYA");
		omFieldArray.Add("RWYD");
		omFieldArray.Add("SEAS");
		omFieldArray.Add("SLOT");
		omFieldArray.Add("SLOU");
		omFieldArray.Add("SSRC");
		omFieldArray.Add("STAB");
		omFieldArray.Add("STAT");
		omFieldArray.Add("STEA");
		omFieldArray.Add("STED");
		omFieldArray.Add("STEV");
		omFieldArray.Add("STHT");
		omFieldArray.Add("STLD");
		omFieldArray.Add("STOA");
		omFieldArray.Add("STOD");
		omFieldArray.Add("STOF");
		omFieldArray.Add("STON");
		omFieldArray.Add("STSL");
		omFieldArray.Add("STTM");
		omFieldArray.Add("STTT");
		omFieldArray.Add("STYP");
		omFieldArray.Add("TET1");
		omFieldArray.Add("TET2");
		omFieldArray.Add("TGA1");
		omFieldArray.Add("TGA2");
		omFieldArray.Add("TGD1");
		omFieldArray.Add("TGD2");
		omFieldArray.Add("TIFA");
		omFieldArray.Add("TIFD");
		omFieldArray.Add("TISA");
		omFieldArray.Add("TISD");
		omFieldArray.Add("TMAU");
		omFieldArray.Add("TMB1");
		omFieldArray.Add("TMB2");
		omFieldArray.Add("TMOA");
		omFieldArray.Add("TTYP");
		omFieldArray.Add("TWR1");
		omFieldArray.Add("USEC");
		omFieldArray.Add("USEU");
		omFieldArray.Add("VERS");
		omFieldArray.Add("VIAL");
		omFieldArray.Add("VIAN");
		omFieldArray.Add("W1BA");
		omFieldArray.Add("W1EA");
		omFieldArray.Add("WRO1");

		omDescArray.Add(GetString(IDS_STRING739));
		omDescArray.Add(GetString(IDS_STRING524));
		omDescArray.Add(GetString(IDS_STRING551));
		omDescArray.Add(GetString(IDS_STRING824));
		omDescArray.Add(GetString(IDS_STRING740));
		omDescArray.Add(GetString(IDS_STRING741));
		omDescArray.Add(GetString(IDS_STRING709));
		omDescArray.Add(GetString(IDS_STRING708));
		omDescArray.Add(GetString(IDS_STRING707));
		omDescArray.Add(GetString(IDS_STRING706));
		omDescArray.Add(GetString(IDS_STRING825));
		omDescArray.Add(GetString(IDS_STRING746));
		omDescArray.Add(GetString(IDS_STRING747));
		omDescArray.Add(GetString(IDS_STRING748));
		omDescArray.Add(GetString(IDS_STRING826));
		omDescArray.Add(GetString(IDS_STRING749));
		omDescArray.Add(GetString(IDS_STRING750));
		omDescArray.Add(GetString(IDS_STRING515));
		omDescArray.Add(GetString(IDS_STRING549));
		omDescArray.Add(GetString(IDS_STRING827));
		omDescArray.Add(GetString(IDS_STRING828));
		omDescArray.Add(GetString(IDS_STRING829));
		omDescArray.Add(GetString(IDS_STRING830));
		omDescArray.Add(GetString(IDS_STRING831));
		omDescArray.Add(GetString(IDS_STRING832));
		omDescArray.Add(GetString(IDS_STRING833));
		omDescArray.Add(GetString(IDS_STRING834));
		omDescArray.Add(GetString(IDS_STRING835));
		omDescArray.Add(GetString(IDS_STRING836));
		omDescArray.Add(GetString(IDS_STRING837));
		omDescArray.Add(GetString(IDS_STRING838));
		omDescArray.Add(GetString(IDS_STRING839));
		omDescArray.Add(GetString(IDS_STRING840));
		omDescArray.Add(GetString(IDS_STRING841));
		omDescArray.Add(GetString(IDS_STRING842));
		omDescArray.Add(GetString(IDS_STRING843));
		omDescArray.Add(GetString(IDS_STRING447));
		omDescArray.Add(GetString(IDS_STRING446));
		omDescArray.Add(GetString(IDS_STRING763));
		omDescArray.Add(GetString(IDS_STRING764));
		omDescArray.Add(GetString(IDS_STRING765));
		omDescArray.Add(GetString(IDS_STRING766));
		omDescArray.Add(GetString(IDS_STRING767));
		omDescArray.Add(GetString(IDS_STRING693));
		omDescArray.Add(GetString(IDS_STRING691));
		omDescArray.Add(GetString(IDS_STRING692));
		omDescArray.Add(GetString(IDS_STRING690));
		omDescArray.Add(GetString(IDS_STRING650));
		omDescArray.Add(GetString(IDS_STRING649));
		omDescArray.Add(GetString(IDS_STRING607));
		omDescArray.Add(GetString(IDS_STRING648));
		omDescArray.Add(GetString(IDS_STRING689));
		omDescArray.Add(GetString(IDS_STRING688));
		omDescArray.Add(GetString(IDS_STRING778));
		omDescArray.Add(GetString(IDS_STRING779));
		omDescArray.Add(GetString(IDS_STRING780));
		omDescArray.Add(GetString(IDS_STRING518));
		omDescArray.Add(GetString(IDS_STRING545));
		omDescArray.Add(GetString(IDS_STRING643));
		omDescArray.Add(GetString(IDS_STRING526));
		omDescArray.Add(GetString(IDS_STRING781));
		omDescArray.Add(GetString(IDS_STRING782));
		omDescArray.Add(GetString(IDS_STRING536));
		omDescArray.Add(GetString(IDS_STRING845));
		omDescArray.Add(GetString(IDS_STRING783));
		omDescArray.Add(GetString(IDS_STRING846));
		omDescArray.Add(GetString(IDS_STRING453));
		omDescArray.Add(GetString(IDS_STRING847));
		omDescArray.Add(GetString(IDS_STRING548));
		omDescArray.Add(GetString(IDS_STRING848));
		omDescArray.Add(GetString(IDS_STRING537));
		omDescArray.Add(GetString(IDS_STRING538));
		omDescArray.Add(GetString(IDS_STRING849));
		omDescArray.Add(GetString(IDS_STRING784));
		omDescArray.Add(GetString(IDS_STRING785));
		omDescArray.Add(GetString(IDS_STRING608));
		omDescArray.Add(GetString(IDS_STRING614));
		omDescArray.Add(GetString(IDS_STRING850));
		omDescArray.Add(GetString(IDS_STRING638));
		omDescArray.Add(GetString(IDS_STRING637));
		omDescArray.Add(GetString(IDS_STRING636));
		omDescArray.Add(GetString(IDS_STRING618));
		omDescArray.Add(GetString(IDS_STRING617));
		omDescArray.Add(GetString(IDS_STRING852));
		omDescArray.Add(GetString(IMFK_SEATAB_APSTA));
		omDescArray.Add(GetString(IMFK_SEATAB_DPSTD));
		omDescArray.Add(GetString(IDS_STRING853));
		omDescArray.Add(GetString(IDS_STRING341));
		omDescArray.Add(GetString(IDS_STRING792));
		omDescArray.Add(GetString(IDS_STRING527));
		omDescArray.Add(GetString(IDS_STRING793));
		omDescArray.Add(GetString(IDS_STRING521));
		omDescArray.Add(GetString(IDS_STRING550));
		omDescArray.Add(GetString(IDS_STRING794));
		omDescArray.Add(GetString(IDS_STRING632));
		omDescArray.Add(GetString(IDS_STRING854));
		omDescArray.Add(GetString(IDS_STRING855));
		omDescArray.Add(GetString(IDS_STRING856));
		omDescArray.Add(GetString(IDS_STRING795));
		omDescArray.Add(GetString(IDS_STRING857));
		omDescArray.Add(GetString(IDS_STRING858));
		omDescArray.Add(GetString(IDS_STRING796));
		omDescArray.Add(GetString(IDS_STRING797));
		omDescArray.Add(GetString(IDS_STRING859));
		omDescArray.Add(GetString(IMFK_SEATAB_STOA));
		omDescArray.Add(GetString(IDS_STRING546));
		omDescArray.Add(GetString(IDS_STRING860));
		omDescArray.Add(GetString(IDS_STRING861));
		omDescArray.Add(GetString(IDS_STRING862));
		omDescArray.Add(GetString(IDS_STRING863));
		omDescArray.Add(GetString(IDS_STRING864));
		omDescArray.Add(GetString(IDS_STRING801));
		omDescArray.Add(GetString(IDS_STRING802));
		omDescArray.Add(GetString(IDS_STRING803));
		omDescArray.Add(GetString(IDS_STRING804));
		omDescArray.Add(GetString(IDS_STRING805));
		omDescArray.Add(GetString(IDS_STRING806));
		omDescArray.Add(GetString(IDS_STRING807));
		omDescArray.Add(GetString(IDS_STRING808));
		omDescArray.Add(GetString(IDS_STRING809));
		omDescArray.Add(GetString(IDS_STRING865));
		omDescArray.Add(GetString(IDS_STRING866));
		omDescArray.Add(GetString(IDS_STRING867));
		omDescArray.Add(GetString(IDS_STRING812));
		omDescArray.Add(GetString(IDS_STRING813));
		omDescArray.Add(GetString(IDS_STRING535));
		omDescArray.Add(GetString(IDS_STRING814));
		omDescArray.Add(GetString(IDS_STRING815));
		omDescArray.Add(GetString(IDS_STRING816));
		omDescArray.Add(GetString(IDS_STRING817));
		omDescArray.Add(GetString(IDS_STRING818));
		omDescArray.Add(GetString(IDS_STRING819));
		omDescArray.Add(GetString(IDS_STRING820));
		omDescArray.Add(GetString(IDS_STRING616));
		omDescArray.Add(GetString(IDS_STRING615));
		omDescArray.Add(GetString(IDS_STRING823));

		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("Date");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("Date");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("Date");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("Date");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("Date");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("Date");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("String");
		omTypeArray.Add("Date");
		omTypeArray.Add("Date");
		omTypeArray.Add("String");
	}

}

