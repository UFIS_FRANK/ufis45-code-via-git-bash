#if !defined(AFX_DAILYSCHEDULETABLEDLG_H__A7C5B3B0_D4AC_11D2_AB07_00001C019D0B__INCLUDED_)
#define AFX_DAILYSCHEDULETABLEDLG_H__A7C5B3B0_D4AC_11D2_AB07_00001C019D0B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DailyScheduleTableDlg.h : header file
//

#include <CCSTable.h>
#include <DailyTableViewer.h>

/////////////////////////////////////////////////////////////////////////////
// DailyScheduleTableDlg dialog

class DailyScheduleTableDlg : public CDialog
{
// Construction
public:
	DailyScheduleTableDlg(const CStringArray &opColumnsToShow, CWnd* pParent = NULL);   // standard constructor
	~DailyScheduleTableDlg();

	void SetColumnsToShow(const CStringArray &opColumnsToShow);
	void ShowAnsicht();
	void SelectFlight(long lpUrno);
	bool ShowFlight(long lpUrno);
// Dialog Data
	//{{AFX_DATA(DailyScheduleTableDlg)
	enum { IDD = IDD_DAILY_SCHEDULE_TABLE_DLG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DailyScheduleTableDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DailyScheduleTableDlg)
	afx_msg LONG OnTableDragBegin(UINT wParam, LONG lParam);
    afx_msg LONG OnDragOver(UINT, LONG);
    afx_msg LONG OnDrop(UINT, LONG); 
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
    afx_msg LONG OnTableRButtonDown(UINT wParam, LONG lParam);
	virtual BOOL OnInitDialog();
	afx_msg void OnPrint();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSchliessen();
	afx_msg void OnSelchangeComboAnsicht();
	afx_msg void OnButtonAnsicht();
	afx_msg void OnTeilen();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
    afx_msg LONG OnTableLButtonDown(UINT wParam, LONG lParam);
    afx_msg LONG OnTableIPEdit(UINT wParam, LONG lParam);
	afx_msg LONG OnTableIPEditKillfocus( UINT wParam, LPARAM lParam);
	afx_msg void OnAendern();
	afx_msg void OnExcel();
	virtual void OnCancel();
	afx_msg void OnSearch();
    afx_msg LONG OnTableReturnPressed(UINT wParam, LONG lParam);
	afx_msg LONG OnTableSelChanged( UINT wParam, LPARAM lParam);
	//}}AFX_MSG
	void UpdateView();
	void UpdateComboAnsicht();


	// Join zweier Fluege
	void Verbinden(DAILYFLIGHTDATA *prpAFlight, DAILYFLIGHTDATA *prpDFlight);

private:
	bool fmDlg1;
	bool fmDlg2;
	// Tabellencontrol
	CCSTable* pomDailyScheduleTable;
	// abgeleitetes CViewer-Object
	DailyTableViewer omViewer;
	// fuer Berechnung der Groesse des Dialoges 
	int imDialogBarHeight;
	CStringArray omColumnsToShow;
	// Instanzen f�r DragAndDrop-Funktionen
	CCSDragDropCtrl omDragDropObject;
	CCSDragDropCtrl omDragDropTarget;

	BOOL HandleWindowText (BOOL bplPost);
	BOOL HandlePostFlight (BOOL bplPost);

	//tifa/tifd like rotationdlg
	void FlightDataTifa(DAILYFLIGHTDATA *prpFlight, CTime& opTime);
	void FlightDataTifd(DAILYFLIGHTDATA *prpFlight, CTime& opTime);
	bool CheckArrivalInputTimes(DAILYFLIGHTDATA *prpFlight, DAILYFLIGHTDATA *prpDFlight, const CTime& opTime);
	bool CheckDepartureInputTimes(DAILYFLIGHTDATA *prpFlight, DAILYFLIGHTDATA *prpAFlight, const CTime& opTime);

DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DAILYSCHEDULETABLEDLG_H__A7C5B3B0_D4AC_11D2_AB07_00001C019D0B__INCLUDED_)
