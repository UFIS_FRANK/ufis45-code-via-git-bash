// BltOverviewTableDlg.cpp : implementation file
//

#include <stdafx.h>
#include <fpms.h>
#include <resrc1.h>
#include <BltOverviewTableDlg.h>
#include <SeasonDlg.h>
#include <RotationDlg.h>
#include <DiaCedaFlightData.h>
#include <utils.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// BltOverviewTableDlg dialog


BltOverviewTableDlg::BltOverviewTableDlg(CWnd* pParent /*=NULL*/)
	: CDialog(BltOverviewTableDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(BltOverviewTableDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	imButtonSpace = 35;
	isCreated = false;
    CDialog::Create(BltOverviewTableDlg::IDD, NULL);
	isCreated = true;

}


BltOverviewTableDlg::~BltOverviewTableDlg()
{
	delete pomTable;
}


void BltOverviewTableDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(BltOverviewTableDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(BltOverviewTableDlg, CDialog)
	//{{AFX_MSG_MAP(BltOverviewTableDlg)
	ON_WM_SIZE()
	ON_WM_CLOSE()
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
	ON_BN_CLICKED(IDC_SAVE, OnSave)
	ON_BN_CLICKED(IDC_PRINT, OnPrint)
	ON_BN_CLICKED(IDCLOSE, OnClose)
    ON_MESSAGE(WM_TABLE_RETURN_PRESSED, OnTableReturnPressed)
	ON_MESSAGE(WM_TABLE_SELCHANGE, OnTableSelChanged)
 	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// BltOverviewTableDlg message handlers



void BltOverviewTableDlg::Activate(void)
{
	// show window
	SetWindowPos(&wndTop,0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE);
	// Rebuild table
 	omViewer.ChangeViewTo("");
	//// set caption
	CString olCaption;
	olCaption.Format(GetString(IDS_STRING1934), pomTable->GetLinesCount());
	// additional text for time mode
	CString olTimes;
	if (bgGatPosLocal)
		olTimes = GetString(IDS_STRING1921);
	else
		olTimes = GetString(IDS_STRING1920);
	olCaption += " ("+olTimes+")";
	SetWindowText(olCaption);
}

void BltOverviewTableDlg::Update(void)
{
	// Rebuild table
 	omViewer.ChangeViewTo("");
	//// set caption
	CString olCaption;
	olCaption.Format(GetString(IDS_STRING1934), pomTable->GetLinesCount());
	// additional text for time mode
	CString olTimes;
	if (bgGatPosLocal)
		olTimes = GetString(IDS_STRING1921);
	else
		olTimes = GetString(IDS_STRING1920);
	olCaption += " ("+olTimes+")";
	SetWindowText(olCaption);
	CheckPostFlightPosDia(0,this);
}




BOOL BltOverviewTableDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// create table
	pomTable = new CCSTable;
	pomTable->SetSelectMode(LBS_MULTIPLESEL | LBS_EXTENDEDSEL);
	pomTable->SetHeaderSpacing(0);
	
	CRect olRect;
    GetClientRect(&olRect);
	imMinDlgWidth = olRect.Width();

    olRect.InflateRect(1, 1);     // hiding the CTable window border
    
	pomTable->SetTableData(this, olRect.left, olRect.right, olRect.top + imButtonSpace, olRect.bottom);

	omViewer.Attach(pomTable);

	// Rebuild table
	omViewer.ChangeViewTo("");

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}



void BltOverviewTableDlg::OnSize(UINT nType, int cx, int cy) 
{
	if(isCreated != false)
	{

		{
			CDialog::OnSize(nType, cx, cy);
	
			if (nType != SIZE_MINIMIZED)
			{
				// resize table
				pomTable->SetPosition(1, cx+1, 1 + imButtonSpace, cy+1);

				// repositioning close-button
				if (cx > 300)
				{
					CWnd *polCloseB = GetDlgItem(IDCLOSE);
					if (polCloseB != NULL)
					{
						CRect olRect;
						polCloseB->GetClientRect(&olRect);
						polCloseB->MoveWindow(cx - olRect.Width() - 5, 8, olRect.Width(), olRect.Height());
					}
				}
			}
		}
	}
}



void BltOverviewTableDlg::OnClose() 
{
	// only hide window
	ShowWindow(SW_HIDE);	

	omViewer.UnRegister();

	//CDialog::OnClose();
}


void BltOverviewTableDlg::OnSave() 
{

	// Get filename
	LPOPENFILENAME polOfn = new OPENFILENAME;
	char buffer[256] = "";
	char buffer2[256] = "";
	LPSTR lpszStr;
	lpszStr = buffer;
	CString olStr = CString("c:\\tmp\\") + CString("*.xls");
	strcpy(buffer,(LPCTSTR)olStr);

	memset(polOfn, 0, sizeof(*polOfn));
	polOfn->lStructSize = sizeof(*polOfn) ;
	polOfn->lpstrFilter = "Export File(*.xls)\0*.xls\0";
	polOfn->lpstrFile = (LPSTR) buffer;
	polOfn->nMaxFile = 256;
	strcpy(buffer2, GetString(IDS_STRING1077));
	polOfn->lpstrTitle = buffer2;

	if(GetOpenFileName(polOfn) == TRUE)
	{
		CString olDefPath(polOfn->lpstrFile);
		if (olDefPath.Find('.') == -1)
			olDefPath += ".xls";
 
		// Print to file
		omViewer.PrintPlanToFile(olDefPath);
	}

	delete polOfn;

}


void BltOverviewTableDlg::OnPrint() 
{
	omViewer.PrintTableView();
}

 
////////////////////////////////////////////////////////////////////////////
// linke Maustaste auf der Flighttable gedr�ckt
//
LONG BltOverviewTableDlg::OnTableLButtonDblclk( UINT wParam, LPARAM lParam)
{
	UINT ipItem = wParam;
	const BLTOVERVIEWTABLE_LINEDATA *prlTableLine = NULL;
	// get actual table line
	prlTableLine = (BLTOVERVIEWTABLE_LINEDATA *)pomTable->GetTextLineData(ipItem);
	if (prlTableLine != NULL)
	{
		// get flight data
		const DIAFLIGHTDATA *prlFlight = ogPosDiaFlightData.GetFlightByUrno(prlTableLine->FUrno);;

		if(prlFlight != NULL)
		{
			// show Daily- or Season-Rotation Mask			
			CTime olTimeTmp;
			CString olAdid;
			if(strcmp(prlFlight->Des3, pcgHome) == 0)
			{
				olTimeTmp = prlFlight->Tifa;
				olAdid = "A";
			}
			else
			{
				olTimeTmp = prlFlight->Tifd;
				olAdid = "D";
			}

			if (olTimeTmp - CTimeSpan(2,0,0,0) < CTime::GetCurrentTime())
			{	
 				pogRotationDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno, olAdid, bgGatPosLocal);
			}
			else
			{
				pogSeasonDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno, DLG_CHANGE, bgGatPosLocal);
			}
		}
	}
	return 0L;
}



LONG BltOverviewTableDlg::OnTableReturnPressed( UINT wParam, LPARAM lParam)
{
	OnTableLButtonDblclk(wParam, lParam); 
	return 0L;
}

LONG BltOverviewTableDlg::OnTableSelChanged( UINT wParam, LPARAM lParam)
{
    int ilLineNo = pomTable->pomListBox->GetCurSel();    
	
	BLTOVERVIEWTABLE_LINEDATA *prlTableLine = NULL;
	prlTableLine = (BLTOVERVIEWTABLE_LINEDATA *)pomTable->GetTextLineData(ilLineNo);
	if (prlTableLine->FUrno != 0)
		CheckPostFlightPosDia(prlTableLine->FUrno,this);
	else
		CheckPostFlightPosDia(0,this);
	
	return 0L;
}
