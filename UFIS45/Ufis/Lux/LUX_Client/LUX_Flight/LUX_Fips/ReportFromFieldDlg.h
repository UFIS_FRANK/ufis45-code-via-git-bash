#ifndef AFX_REPORTFROMFIELDDLG_H__CDB257E4_8BF5_11D1_8127_0000B43C4B01__INCLUDED_
#define AFX_REPORTFROMFIELDDLG_H__CDB257E4_8BF5_11D1_8127_0000B43C4B01__INCLUDED_

// ReportFromFieldDlg.h : Header-Datei
//
#include <CCSEdit.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CReportFromFieldDlg 

class CReportFromFieldDlg : public CDialog
{
// Konstruktion
public:
	CReportFromFieldDlg(CWnd* pParent = NULL,CString opHeadline = "",CString opDatefield = "",CString opSelectfield = "",CTime* oDate = NULL,char *opSelect = "");

	CTime *pomDate;
	char *pcmSelect;
// Dialogfelddaten
	//{{AFX_DATA(CReportFromFieldDlg)
	enum { IDD = IDD_REPORTFROMFIELD };
	CStatic	m_CS_Datefield;
	CStatic	m_CS_Selectfield;
	CCSEdit	m_CE_Select;
	CCSEdit	m_CE_Datumvon;
	CString	m_Datumvon;
	CString	m_Select;
	CString	m_Selectfield;
	CString	m_Datefield;
	//}}AFX_DATA
	CString omHeadline;


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CReportFromFieldDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CReportFromFieldDlg)
	virtual void OnCancel();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_REPORTFROMFIELDDLG_H__CDB257E4_8BF5_11D1_8127_0000B43C4B01__INCLUDED_
