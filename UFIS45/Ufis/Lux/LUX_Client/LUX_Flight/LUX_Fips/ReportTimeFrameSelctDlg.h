#if !defined(AFX_REPORTTIMEFRAMESELCTDLG_H__4972F211_0D2B_11D3_AB47_00001C019D0B__INCLUDED_)
#define AFX_REPORTTIMEFRAMESELCTDLG_H__4972F211_0D2B_11D3_AB47_00001C019D0B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ReportTimeFrameSelctDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// ReportTimeFrameSelctDlg dialog

class ReportTimeFrameSelctDlg : public CDialog
{
// Construction
public:
	ReportTimeFrameSelctDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(ReportTimeFrameSelctDlg)
	enum { IDD = IDD_REPORT_TIME_FRAME };
	int		m_ListTableGantt;
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ReportTimeFrameSelctDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ReportTimeFrameSelctDlg)
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_REPORTTIMEFRAMESELCTDLG_H__4972F211_0D2B_11D3_AB47_00001C019D0B__INCLUDED_)
