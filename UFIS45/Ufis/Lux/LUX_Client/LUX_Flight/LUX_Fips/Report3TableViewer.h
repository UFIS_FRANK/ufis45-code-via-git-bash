#ifndef __FLUEGEZEITRAUMTABLEVIEWER_H__
#define __FLUEGEZEITRAUMTABLEVIEWER_H__

#include <stdafx.h>
#include <RotationDlgCedaFlightData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct FLUEGEZEITRAUMTABLE_LINEDATA
{
	long AUrno;
	long ARkey;
	CString		AStev;
	CString		AFlno;
	CTime		AStoa;
	CString		ATtyp;
	CString		AOrg3;
	CString		AVia3;
	CString		AAct;
	//CString		AAct5;
	CString		AGta1;
	CString		AGta2;
	CString		APsta;
	CString		ADay;

	long DUrno;
	long DRkey;
	CString		DFlno;
	CTime		DStod;
	CString		DTtyp;
	CString		DDes3;
	CString		DVia3;
	CString		DAct;
	//CString		DAct5;
	CString		DGtd1;
	CString		DGtd2;
	CString		DPstd;
	CString		DWro1;
	CTime		DLstu;
	CTime		Cicf;

	bool		ArrCanceled;
	bool		DepCanceled;

};

/////////////////////////////////////////////////////////////////////////////
// FluegezeitraumTableViewer

class FluegezeitraumTableViewer : public CViewer
{
// Constructions
public:
    FluegezeitraumTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo = NULL, char *pcpSelect = NULL);
    ~FluegezeitraumTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
    void MakeLines();
	int  MakeLine(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight);
	void MakeLineData(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight, FLUEGEZEITRAUMTABLE_LINEDATA &rpLine);
	void MakeColList(FLUEGEZEITRAUMTABLE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList);
	int  CompareFluegezeitraum(FLUEGEZEITRAUMTABLE_LINEDATA *prpFluegezeitraum1, FLUEGEZEITRAUMTABLE_LINEDATA *prpFluegezeitraum2);
	int  CompareFlight(FLUEGEZEITRAUMTABLE_LINEDATA *prpFlight1, FLUEGEZEITRAUMTABLE_LINEDATA *prpFlight2);
	int  GetVia(CCSPtrArray<VIADATA> *opVias, ROTATIONDLGFLIGHTDATA *prpFlight);
// Operations
public:
	void DeleteAll();
	int CreateLine(FLUEGEZEITRAUMTABLE_LINEDATA &prpFluegezeitraum);
	void DeleteLine(int ipLineno);
	bool PrintPlanToFile(char *pcpDefPath);
// Window refreshing routines
public:
	void UpdateDisplay();
	void DrawHeader();


// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
	int imDay;
// Attributes
private:
    CCSTable *pomTable;
	CCSPtrArray<ROTATIONDLGFLIGHTDATA> *pomData;
	CCSPtrArray<CCADATA> omDCins;
	CCSPtrArray<CCADATA> omDCinsSave;
public:
    CCSPtrArray<FLUEGEZEITRAUMTABLE_LINEDATA> omLines;
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void GetHeader(void); 
	void PrintTableView(void);
	bool PrintTableLine(FLUEGEZEITRAUMTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader(void);
	CCSPtrArray <TABLE_HEADER_COLUMN> omPrintHeadHeaderArray;
	CCSPrint *pomPrint;
	CString omTableName;
	CString omFileName;
	//CStringArray omSort;
	char *pcmInfo;
	char *pcmSelect;
	CString omFooterName;
	int imDep;
	int imArr;
};

#endif //__FLUEGEZEITRAUMTABLEVIEWER_H__
