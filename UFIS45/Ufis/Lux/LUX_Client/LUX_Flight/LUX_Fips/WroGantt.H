// stgantt.h : header file
// 27-nov-00	rkr		CheckPostFlight() for handling postflights added;
//

#ifndef _WROGANTT_H_
#define _WROGANTT_H_

// Special time value definition
#ifndef TIMENULL
#define TIMENULL    CTime((time_t)-1)
#endif

#include <WroDiaViewer.h>
#include <WroDiagram.h>
#include <DiaCedaFlightData.h>
/////////////////////////////////////////////////////////////////////////////
// WroGantt window

class WroGantt: public CListBox
{
// Operations
public:
    WroGantt::WroGantt(WroDiagramViewer *popViewer = NULL, int ipGroupno = 0,
        int ipVerticalScaleWidth = 0, int ipVerticalScaleIndent = 15,
        CFont *popVerticalScaleFont = NULL, CFont *popGanttChartFont = NULL,
        int ipGutterHeight = 2, int ipOverlapHeight = 4,
        COLORREF lpVerticalScaleTextColor = ::GetSysColor(COLOR_BTNTEXT),
        COLORREF lpVerticalScaleBackgroundColor = ::GetSysColor(COLOR_BTNFACE),
        COLORREF lpHighlightVerticalScaleTextColor = ::GetSysColor(COLOR_BTNHIGHLIGHT),
        COLORREF lpHighlightVerticalScaleBackgroundColor = ::GetSysColor(COLOR_BTNSHADOW),
        COLORREF lpGanttChartTextColor = ::GetSysColor(COLOR_BTNTEXT),
        COLORREF lpGanttChartBackgroundColor = ::GetSysColor(COLOR_BTNFACE),
        COLORREF lpHighlightGanttChartTextColor = ::GetSysColor(COLOR_BTNHIGHLIGHT),
        COLORREF lpHighlightGanttChartBackgroundColor = ::GetSysColor(COLOR_BTNSHADOW));

	 ~WroGantt();
    void SetViewer(WroDiagramViewer *popViewer, int ipGroupno);
    void SetVerticalScaleWidth(int ipWidth);
	void SetVerticalScaleIndent(int ipIndent);
    //void SetFonts(CFont *popVerticalScaleFont, CFont *popGanttChartFont);
    void SetFonts(int index1, int index2);
    void SetGutters(int ipGutterHeight, int ipOverlapHeight);
    void SetVerticalScaleColors(COLORREF lpTextColor, COLORREF lpBackgroundColor,
            COLORREF lpHighlightTextColor, COLORREF lpHighlightBackgroundColor);
    void SetGanttChartColors(COLORREF lpTextColor, COLORREF lpBackgroundColor,
            COLORREF lpHighlightTextColor, COLORREF lpHighlightBackgroundColor);

    int GetGanttChartHeight();
    int GetLineHeight(int ipMaxOverlapLevel);

    BOOL Create(DWORD dwStyle, const RECT &rect, CWnd *pParentWnd, UINT nID = 0);
    void SetStatusBar(CStatusBar *popStatusBar);
	void SetTimeScale(CCSTimeScale *popTimeScale);
    void SetBorderPrecision(int ipBorderPrecision);
	void SetTopScaleText(CCS3DStatic *popTopScaleText)
	{
		pomTopScaleText = popTopScaleText;

	};

    // This group of function will automatically repaint the window
    void SetDisplayWindow(CTime opDisplayStart, CTime opDisplayEnd, BOOL bpFixedScaling = TRUE);
    void SetDisplayStart(CTime opDisplayStart);
    void SetCurrentTime(CTime opCurrentTime);
    void SetMarkTime(CTime opMarkTimeStart, CTime opMarkTimeEnd);

    void RepaintVerticalScale(int ipLineno = -1, BOOL bpErase = TRUE);
    void RepaintGanttChart(int ipLineno = -1,
            CTime opStartTime = TIMENULL, CTime opEndTime = TIMENULL, BOOL bpErase = TRUE);
    void RepaintItemHeight(int ipLineno);
	void ProcessMarkBar(DIAFLIGHTDATA *prpFlight);
	void AttachDiagram(WroDiagram *popWnd)
	{
		pomDiagram = popWnd;
	}
	WroDiagram *pomDiagram;
// Attributes
private:
    WroDiagramViewer *pomViewer;
    int imGroupno;              // index to a group in the viewer
    int imVerticalScaleWidth;
	int imVerticalScaleIndent;	// number of pixels used to indent text in VerticalScale
    CFont *pomVerticalScaleFont;
    CFont *pomGanttChartFont;
	 CPoint omLastClickedPosition;
    int imBarHeight;            // calculated when SetGanttChartFont()
    int imGutterHeight;         // space between the GanttLine and the topmost-level bar
    int imLeadingHeight;        // space between the bar and the border
    int imOverlapHeight;        // space between the overlapped bar
    COLORREF lmVerticalScaleTextColor;
    COLORREF lmVerticalScaleBackgroundColor;
    COLORREF lmHighlightVerticalScaleTextColor;
    COLORREF lmHighlightVerticalScaleBackgroundColor;
    COLORREF lmGanttChartTextColor;
    COLORREF lmGanttChartBackgroundColor;
    COLORREF lmHighlightGanttChartTextColor;
    COLORREF lmHighlightGanttChartBackgroundColor;
	WRODIA_BARDATA rmActiveBar, *prmPreviousBar;          // Bar which is currently moved or sized
	BOOL bmActiveBarSet;
	CBrush	*pomPreviousBrush;
	CRect    omPreviousRect;
	WRODIA_BARDATA rmContextBar;  // Save bar for Context Menu handling
	BOOL bmContextBarSet;
	BOOL bmActiveLineSet;  // There is a selected line ?
	long lmActiveLine;	// Urno of last selected line

    CTime omDisplayStart;
    CTime omDisplayEnd;
    int imWindowWidth;          // in pixels, for pixel/time ratio (updated by WM_SIZE)
    BOOL bmIsFixedScaling;
        // There are two mode for updating the display window when WM_SIZE was sent,
        // variabled-scaling and fixed-scaling. The mode of this operation will be
        // given in the method SetDisplayWindow().

    CTime omCurrentTime;                    // the red vertical time line
    CTime omMarkTimeStart, omMarkTimeEnd;   // two yellow vertical time lines

    CStatusBar *pomStatusBar;   // the status bar where the notification message goes
	CCSTimeScale *pomTimeScale;	// the time scale for top scale indicators display

    BOOL bmIsMouseInWindow;
    BOOL bmIsControlKeyDown;
	 BOOL bmMarkLines;
    int imHighlightLine;        // the current line that painted with highlight color
    int imCurrentBar;           // the current bar that the user places mouse over

    UINT umResizeMode;          // HTNOWHERE, HTCAPTION (moving), HTLEFT, or HTRIGHT
    CPoint omPointResize;       // the point where moving/resizing begin
    CRect omRectResize;         // current focus rectangle for moving/resizing
    int imBorderPreLeft;        // define the sensitivity when user detect bar border
    int imBorderPreRight;       // define the sensitivity when user detect bar border

	CCS3DStatic *pomTopScaleText;	

	CMenu omWrosMenu;
	CMenu omRButtonMenu;
	DIAFLIGHTDATA *prmClickedFlight;
 	WRODIA_BARDATA *prmClickedBar;
	WRODIA_LINEDATA *prmClickedLine;
 
	int imDragItem;
	int imDragBarno;

// Implementation
public:
    virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
    virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	virtual BOOL DestroyWindow();

private:
    void DrawVerticalScale(CDC *pDC, int itemID, const CRect &rcItem);
	void DrawBackgroundBars(CDC *pDC, int itemID, const CRect &rcItem, const CRect &rcClip);
    void DrawGanttChart(CDC *pDC, int itemID, const CRect &rcItem, const CRect &rcClip);
    void DrawTimeLines(CDC *pDC, int top, int bottom, BOOL bpWithMarkLines = FALSE);
	void DrawFocusRect(CDC *pDC, const CRect &rect);
	BOOL CheckPostFlight(WRODIA_BARDATA *prlBar);
	bool CheckIfWroIsOpen(const WRODIA_BARDATA *prpBar);
	bool CheckIfWroIsOpen(const DIAFLIGHTDATA *prlFlightD, long lpWroNo);

protected:
    // Generated message map functions
    //{{AFX_MSG(WroGantt)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
    afx_msg void OnMouseMove(UINT nFlags, CPoint point);
    afx_msg void OnTimer(UINT nIDEvent);
    afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
 	afx_msg LONG OnDragEnter(UINT wParam, LONG lParam);
	afx_msg LONG OnDragLeave(UINT wParam, LONG lParam);
	afx_msg LONG OnDragTime(UINT wParam, LONG lParam);
	afx_msg LONG OnDelete( WPARAM wParam, LPARAM lParam );
	afx_msg LONG OnWrosMenu( WPARAM wParam, LPARAM lParam );
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()

private:
    void UpdateGanttChart(CPoint point, BOOL bpIsControlKeyDown);
    void UpdateBarStatus(int ipLineno, int ipBarno);
    BOOL BeginMovingOrResizing(CPoint point);
    BOOL OnMovingOrResizing(CPoint point, BOOL bpIsLButtonDown);
    int GetItemFromPoint(CPoint point);
    int GetBarnoFromPoint(int ipLineno, CPoint point, int ipPreLeft = 0, int ipPreRight = 1);
        // Precision is measured in pixel.
        // The purpose of the precision is for detecting more pixel when user want to resize.
        // The acceptable period of time is between [point.x - ipPreLeft, point.x + ipPreRight]
    int GetBkBarnoFromPoint(int ipLineno, CPoint point, int ipPreLeft = 0, int ipPreRight = 1);
        // Precision is measured in pixel.
        // The purpose of the precision is for detecting more pixel when user want to resize.
        // The acceptable period of time is between [point.x - ipPreLeft, point.x + ipPreRight]
    UINT HitTest(int ipLineno, int ipBarno, CPoint point);
        // HitTest will use the precision defined in "imBorderPrecision"
//-DTT Jul.25-----------------------------------------------------------
	BOOL IsDropOnDutyBar(CPoint point);
//----------------------------------------------------------------------
	void EnableRepaint(void);

//rkr01042001 show default allocation times
	bool ShowBestWroTime(const DIAFLIGHTDATA *prlFlight, const long llWrono, const CString &ropGat);

public:

	bool bmRepaint;
	
	
	CCSDragDropCtrl m_DragDropSource;
	CCSDragDropCtrl m_DragDropTarget;
//-DTT Jul.25-----------------------------------------------------------
	CPoint omDropPosition;
	void DragBegin(int ipLineno, int ipBarno);
//----------------------------------------------------------------------
};
 
/////////////////////////////////////////////////////////////////////////////
//extern WroGantt *pogFlightGantt;

#endif
