// MessefluegeTableViewer.h 
//
//	Statistik Messefl�ge (Abfl�ge)

#ifndef __MESSEFLUEGETABLEVIEWER_H__
#define __MESSEFLUEGETABLEVIEWER_H__

#include <stdafx.h>
#include <RotationDlgCedaFlightData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct MESSEFLUEGETABLE_LINEDATA
{
	long		Urno; 	// Eindeutige Datensatz-Nr.
	CString		ACtr;	// Anzahl der Bewegungen Ankunft
	CString		DCtr;	// Anzahl der Bewegungen Abflug
	CString		LCtr;	// Anzahl der Bewegungen Ankunft + Abflug
	//CString		ASum	// Gesamtsumme Ankunft
	//CString		DSum	// Gesamtsumme Abflug
	CString 	Tifd; 	// Zeitrahmen Abflug
	CString 	Adid; 	// Ankunft / Abflug

};

/////////////////////////////////////////////////////////////////////////////
// MessefluegeTableViewer

class MessefluegeTableViewer : public CViewer
{
// Constructions
public:
    MessefluegeTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo = NULL);
    ~MessefluegeTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
    void MakeLines();
	void MakeLine(CTime opTifdold, int ipACtr, int ipDCtr, int ipLCtr);
	void MakeResultLine(int ipACtr, int ipDCtr, int ipLCtr);
	int CompareMessefluege(MESSEFLUEGETABLE_LINEDATA *prpMessefluege1, MESSEFLUEGETABLE_LINEDATA *prpMessefluege2);
// Operations
public:
	void DeleteAll();
	void CreateLine(MESSEFLUEGETABLE_LINEDATA *prpMessefluege);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay(void);
	bool PrintPlanToFile(char *pcpDefPath);
	//void ProcessMessefluegeChange(ROTATIONDLGFLIGHTDATA *prpMessefluege);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomMessefluegeTable;
	CCSPtrArray<ROTATIONDLGFLIGHTDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<MESSEFLUEGETABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(MESSEFLUEGETABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	char *pcmInfo;
	CString omTableName;
	CString omFileName;
	CString omFooterName;

};

#endif //__MESSEFLUEGETABLEVIEWER_H__
