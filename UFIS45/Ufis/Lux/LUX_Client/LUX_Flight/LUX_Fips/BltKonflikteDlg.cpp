// BltKonflikteDlg.cpp : implementation file
//

#include <stdafx.h>
#include <fpms.h>
#include <BltKonflikteDlg.h>
#include <Konflikte.h>
#include <RotationDlg.h>
#include <SeasonDlg.h>
#include <BltDiagram.h>
#include <CCSGlobl.h>
#include <CCSDdx.h>
#include <PrivList.h>
#include <Utils.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



// Local function prototype
// Broadcast -Funktion
static void BltKonfTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);



/////////////////////////////////////////////////////////////////////////////
// BltKonflikteDlg dialog




BltKonflikteDlg::BltKonflikteDlg(CWnd* pParent /*=NULL*/)
	: CDialog(BltKonflikteDlg::IDD, pParent), imDlgBarHeight(50)
{
	//{{AFX_DATA_INIT(BltKonflikteDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	pomTable = NULL;
	bmIsActiv = false;
	bmIsCreated = false;
    CDialog::Create(BltKonflikteDlg::IDD, pParent);
	bmIsCreated = true;
}


BltKonflikteDlg::~BltKonflikteDlg()
{
	ogDdx.UnRegister(this,NOTUSED);
	delete pomTable;
}



void BltKonflikteDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(BltKonflikteDlg)
	DDX_Control(pDX, IDC_ALLCONFIRM, m_CB_AllConfirm);
	DDX_Control(pDX, IDC_CONFIRM, m_CB_Confirm);
	DDX_Control(pDX, IDCANCEL, m_CB_Cancel);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(BltKonflikteDlg, CDialog)
	//{{AFX_MSG_MAP(BltKonflikteDlg)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_CONFIRM, OnConfirm)
	ON_BN_CLICKED(IDC_ALLCONFIRM, OnAllconfirm)
	ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
    ON_MESSAGE(WM_TABLE_RETURN_PRESSED, OnTableReturnPressed)
	ON_MESSAGE(WM_TABLE_SELCHANGE, OnTableSelChanged)
 	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// BltKonflikteDlg message handlers

BOOL BltKonflikteDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// Register broadcasts
	ogDdx.Register(this, KONF_DELETE, CString("KONF_DELETE"),CString("BLTKONFLIKTEDLG"),	BltKonfTableCf);
	ogDdx.Register(this, KONF_INSERT, CString("KONF_INSERT"),CString("BLTKONFLIKTEDLG"),	BltKonfTableCf);
	ogDdx.Register(this, KONF_CHANGE, CString("KONF_CHANGE"),CString("BLTKONFLIKTEDLG"),	BltKonfTableCf);

	m_CB_Confirm.SetSecState(ogPrivList.GetStat("BLTCONFLICTS_CONFIRM"));		
	m_CB_AllConfirm.SetSecState(ogPrivList.GetStat("BLTCONFLICTS_CONFIRMALL"));		

	InitTable();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void BltKonflikteDlg::InitTable() 
{

	CRect olRect;
    GetClientRect(&olRect);
	olRect.top += imDlgBarHeight; 

	// Create Table
    pomTable = new CCSTable;
	pomTable->SetSelectMode(LBS_MULTIPLESEL | LBS_EXTENDEDSEL);
	//pomTable->SetHeaderSpacing(0);

	// Positioning table
    olRect.InflateRect(1, 1);     // hiding the CTable window border
	pomTable->SetTableData(this, olRect.left, olRect.right, olRect.top, olRect.bottom);


	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->ResetContent();
	
	// Set table header
	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Length = 1500; 
	rlHeader.Font = &ogCourier_Regular_10;
	rlHeader.Text = GetString(IDS_STRING1429);
	omHeaderDataArray.New(rlHeader);
	
	// Set table attributes
	pomTable->SetHeaderFields(omHeaderDataArray);
	pomTable->SetDefaultSeparator();
	pomTable->SetTableEditable(false);
	omHeaderDataArray.DeleteAll();
	pomTable->DisplayTable();

}


void BltKonflikteDlg::Attention() 
{
	// Show Dialog
	ShowWindow(SW_SHOW);
	SetWindowPos(&wndTop, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
	bmAttention = true;
	bmIsActiv = true;
	m_CB_AllConfirm.ShowWindow(SW_SHOW);
	m_CB_Confirm.ShowWindow(SW_SHOW);
	SetWindowText(GetString(IDS_STRING1899));
	// Rebuild table
	pomTable->ResetContent();
	ChangeViewTo();
}


void BltKonflikteDlg::ChangeViewTo()
{
	// Rebuild table
	DeleteAll();    
	MakeLines();
	UpdateDisplay();
}


void BltKonflikteDlg::DeleteAll()
{
	omLines.DeleteAll();
}

// Create Table lines
void BltKonflikteDlg::MakeLines()
{
	BLTKONF_LINEDATA rlLine;
	const KONFENTRY *prlEntry;
	const KONFDATA  *prlData;

	// Scan all global Conflicts
	for(int i = ogKonflikte.omData.GetSize() - 1; i >= 0; i--)
	{
		prlEntry = &ogKonflikte.omData[i];
		// Scan all entrys in each conflict
		for(int j = prlEntry->Data.GetSize() - 1; j >= 0; j--)
		{
			prlData = &prlEntry->Data[j];
			// Only if conflict is a baggage-belt conflict
			if(prlData->SetBy != 0 && ogKonflikte.IsBltKonfliktType(prlData->KonfId.Type))
			{
				if((bmAttention && !prlData->Confirmed) || !bmAttention)
				{
					// Make new line
					rlLine.KonfId = prlData->KonfId;
					rlLine.Text = prlData->Text;
					rlLine.TimeOfConflict = prlData->TimeOfConflict;
					CreateLine(rlLine);
				}
			}
		}
	}
}


// Create new line
int BltKonflikteDlg::CreateLine(BLTKONF_LINEDATA &rrpKonf)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareKonf(rrpKonf, omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]
	// insert line
    omLines.NewAt(ilLineno, rrpKonf);

    return ilLineno;
}


// Compare two lines for sorting
int BltKonflikteDlg::CompareKonf(const BLTKONF_LINEDATA &rrpKonf1, const BLTKONF_LINEDATA &rrpKonf2) const
{
	int	ilCompareResult = (rrpKonf1.TimeOfConflict == rrpKonf2.TimeOfConflict)? 0:
		(rrpKonf1.TimeOfConflict > rrpKonf2.TimeOfConflict)? 1: -1;
	return ilCompareResult;
}


void BltKonflikteDlg::UpdateDisplay()
{
	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;

  	pomTable->ResetContent();
	// Scan local lines
    for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		rlColumnData.Text = omLines[ilLc].Text;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		// add line into table
		pomTable->AddTextLine(olColList , &omLines[ilLc]);
		olColList.DeleteAll();
    }                                           
    pomTable->DisplayTable();
	CheckPostFlightPosDia(0,this);
}



// Confirm selected conflicts
void BltKonflikteDlg::OnConfirm() 
{

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	
	int ilItems[3000];
	int ilAnz = 3000;
	const BLTKONF_LINEDATA *prlLine;

	CListBox *polLB = pomTable->GetCTableListBox();
	if (polLB == NULL)
		return;

	// Get selected items
	ilAnz = MyGetSelItems(*polLB, ilAnz, ilItems);
	if(ilAnz <= 0)
	{
		//MessageBox(GetString(IDS_STRING968), GetString(ST_FEHLER), MB_ICONWARNING);
		return;
	}

	CPtrArray olKonfIds;
	for(int i = 0; i < ilAnz; i++)
	{
		prlLine = (BLTKONF_LINEDATA *)pomTable->GetTextLineData(ilItems[i]);
		if(prlLine != NULL)
		{
			// collect records to confirm
			olKonfIds.Add((void *)&prlLine->KonfId);
		}
	}

	ogKonflikte.Confirm(olKonfIds); 
	

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}


// confirm all conflicts
void BltKonflikteDlg::OnAllconfirm() 
{
	if (pomTable->GetLinesCount() > 0)
	{
		if (MessageBox(GetString(IDS_STRING1968), GetString(ST_FRAGE), MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2) == IDYES)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			// confirm all conflicts
			ogKonflikte.ConfirmAll(CONF_BLT);
			OnCancel();
			if (pogBltDiagram)
			{
				pogBltDiagram->m_CB_BltAttention.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			}
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		}
	}
}





void BltKonflikteDlg::OnCancel() 
{
	omLines.DeleteAll();
	bmIsActiv = false;
	// only hide window
	ShowWindow(SW_HIDE);
	//CDialog::OnCancel();
}


void BltKonflikteDlg::OnSize(UINT nType, int cx, int cy) 
{
	if(bmIsCreated != false && cx > 250)
	{
		CDialog::OnSize(nType, cx, cy);
	
		if (nType != SIZE_MINIMIZED)
		{
			// resize table
			pomTable->SetPosition(1, cx+1, imDlgBarHeight+1, cy+1);
			// reposition buttons
			m_CB_AllConfirm.SetWindowPos(this, cx-234,3,0,0, SWP_NOSIZE | SWP_NOZORDER | SWP_SHOWWINDOW);
			m_CB_Confirm.SetWindowPos(this, cx-159,3,0,0, SWP_NOSIZE | SWP_NOZORDER | SWP_SHOWWINDOW);
			m_CB_Cancel.SetWindowPos(this, cx-77,3,0,0, SWP_NOSIZE | SWP_NOZORDER | SWP_SHOWWINDOW);
			m_CB_AllConfirm.Invalidate();
			m_CB_Confirm.Invalidate();
			m_CB_Cancel.Invalidate();
		}
	}
}



// Broadcast function
static void BltKonfTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
	if (vpDataPointer == NULL) 
		return;

	BltKonflikteDlg *polDlg = (BltKonflikteDlg *)popInstance;
	KonfIdent *polIdNotify = (KonfIdent *)vpDataPointer;
	// is conflict a baggage-belt conflict?
	if (!ogKonflikte.IsBltKonfliktType(polIdNotify->Type)) return;
	// choose funktion
 	if (ipDDXType == KONF_DELETE)
		polDlg->DdxDeleteKonf(*polIdNotify);
	if (ipDDXType == KONF_INSERT)
		polDlg->DdxInsertKonf(*polIdNotify);
	if (ipDDXType == KONF_CHANGE)
		polDlg->DdxChangeKonf(*polIdNotify);

}


void BltKonflikteDlg::DdxChangeKonf(const KonfIdent &rrpIdNotify)
{
	if(!bmIsActiv)
		return;
	
	const KONFDATA *prlData = NULL;
	BLTKONF_LINEDATA *prlLine;
	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;
	// find intern table line
	int ilLineNo = FindLine(rrpIdNotify);

	if(ilLineNo >= 0)
	{
		// get conflict
		prlData = ogKonflikte.GetKonflikt(rrpIdNotify);

		if(prlData != NULL)
		{
			//  alter intern table line
			prlLine = &omLines[ilLineNo];
			prlLine->Text = prlData->Text;
			prlLine->TimeOfConflict = prlData->TimeOfConflict;

			// alter table line
			rlColumnData.Text = omLines[ilLineNo].Text;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			pomTable->ChangeTextLine(ilLineNo, &olColList , &omLines[ilLineNo]);
			olColList.DeleteAll();
		}
	}
}


void BltKonflikteDlg::DdxInsertKonf(const KonfIdent &rrpIdNotify)
{
	if(!bmIsActiv)
		return;

	const KONFDATA *prlData = NULL;
	BLTKONF_LINEDATA rlLine;
	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;
	int ilLineNo;

	// get new conflict
	prlData = ogKonflikte.GetKonflikt(rrpIdNotify);

	if(prlData != NULL)
	{

		if(prlData->SetBy != 0)
		{
			if((bmAttention && !prlData->Confirmed) || !bmAttention)
			{
				// create new intern table line
				rlLine.KonfId = rrpIdNotify;
				rlLine.Text = prlData->Text;
				rlLine.TimeOfConflict = prlData->TimeOfConflict;
				ilLineNo = CreateLine(rlLine);

				// create new line in table
				rlColumnData.Text = rlLine.Text;
				olColList.NewAt(olColList.GetSize(), rlColumnData);
				pomTable->InsertTextLine(ilLineNo, olColList , &omLines[ilLineNo]);
				olColList.DeleteAll();

			}
		}
	}
}


void BltKonflikteDlg::DdxDeleteKonf(const KonfIdent &rrpIdNotify)
{
	if(!bmIsActiv)
		return;

	// find intern table line
	int ilLineNo = FindLine(rrpIdNotify);

	if(ilLineNo >= 0)
	{
		// delete intern table line
		omLines.DeleteAt(ilLineNo);
		// delete table line
		pomTable->DeleteTextLine(ilLineNo);
	}
}


int BltKonflikteDlg::FindLine(const KonfIdent &rrpKonfId) const
{
	// Scan all intern table lines
	for(int i = omLines.GetSize() - 1; i >= 0; i--)
	{
		if(omLines[i].KonfId == rrpKonfId)
			break;
	}
	return i;
}



LONG BltKonflikteDlg::OnTableLButtonDblclk( UINT wParam, LPARAM lParam)
{
	const BLTKONF_LINEDATA *prlLine = (BLTKONF_LINEDATA*)pomTable->GetTextLineData(wParam);

	if(prlLine == NULL)
		return 0L;

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	// Activate 'Daily'- or 'Season'-Rotation-Mask
	const DIAFLIGHTDATA *prlFlight = ogPosDiaFlightData.GetFlightByUrno(prlLine->KonfId.FUrno);

	if(prlFlight != NULL)
	{
					
		CTime olTimeTmp;
		CString olAdid;
		if(strcmp(prlFlight->Des3, pcgHome) == 0)
		{
			olTimeTmp = prlFlight->Tifa;
			olAdid = "A";
		}
		else
		{
			olTimeTmp = prlFlight->Tifd;
			olAdid = "D";
		}

		if (olTimeTmp - CTimeSpan(2,0,0,0) < CTime::GetCurrentTime())
		{	
 			pogRotationDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno, olAdid, bgGatPosLocal);
		}
		else
		{
			pogSeasonDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno, DLG_CHANGE, bgGatPosLocal);
		}
	}
	
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	return 0L;
}


LONG BltKonflikteDlg::OnTableReturnPressed( UINT wParam, LPARAM lParam)
{
	OnTableLButtonDblclk(wParam, lParam); 
	return 0L;
}

LONG BltKonflikteDlg::OnTableSelChanged( UINT wParam, LPARAM lParam)
{
    int ilLineNo = pomTable->pomListBox->GetCurSel();    
	
	BLTKONF_LINEDATA *prlTableLine = NULL;
	prlTableLine = (BLTKONF_LINEDATA *)pomTable->GetTextLineData(ilLineNo);
	if (prlTableLine->KonfId.FUrno != 0)
		CheckPostFlightPosDia(prlTableLine->KonfId.FUrno,this);
	else
		CheckPostFlightPosDia(0,this);
	
	return 0L;
}


