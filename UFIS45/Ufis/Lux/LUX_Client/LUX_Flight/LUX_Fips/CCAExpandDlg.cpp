// CCAExpandDlg.cpp : implementation file
//

#include <stdafx.h>
#include <fpms.h>
#include <CCAExpandDlg.h>
#include <CCSGlobl.h>
#include <CcaCedaFlightData.h>
#include <resrc1.h>
#include <AskBox.h>
#include <process.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCAExpandDlg dialog

//
CCAExpandDlg::CCAExpandDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCAExpandDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCAExpandDlg)
		// NOTE: the ClassWizard will add member initialization here
	m_check1 = TRUE;
	m_check2 = TRUE;
	m_check3 = TRUE;
	m_check4 = TRUE;
	m_check5 = TRUE;
	m_check6 = TRUE;
	m_check7 = TRUE;
	m_common = TRUE;
	m_flight = TRUE;
	//}}AFX_DATA_INIT
}
BOOL CCAExpandDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_From.SetTypeToDate(true);
	m_To.SetTypeToDate(true);
	m_only = false;
	return TRUE;
}


void CCAExpandDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCAExpandDlg)
	DDX_Control(pDX, IDC_ALLVT, m_CB_AllVT);
	DDX_Control(pDX, IDC_PROG, m_CP_Progess);
	DDX_Control(pDX, IDC_DATEFROM, m_From);
	DDX_Control(pDX, IDC_DATETO, m_To);
	DDX_Check(pDX, IDC_CHECK1, m_check1);
	DDX_Check(pDX, IDC_CHECK2, m_check2);
	DDX_Check(pDX, IDC_CHECK3, m_check3);
	DDX_Check(pDX, IDC_CHECK4, m_check4);
	DDX_Check(pDX, IDC_CHECK5, m_check5);
	DDX_Check(pDX, IDC_CHECK6, m_check6);
	DDX_Check(pDX, IDC_CHECK7, m_check7);
	DDX_Check(pDX, IDC_COMMON, m_common);
	DDX_Check(pDX, IDC_FLIGHT, m_flight);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCAExpandDlg, CDialog)
	//{{AFX_MSG_MAP(CCAExpandDlg)
	ON_BN_CLICKED(IDC_ONLY, OnOnly)
	ON_BN_CLICKED(IDC_ALL, OnAll)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCAExpandDlg message handlers

void CCAExpandDlg::OnOnly() 
{

	m_only = true;
	OnAll();
	m_only = false;

	return;



	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	CUIntArray olUrnos;
	CUIntArray olCommonUrnos;
	CCSPtrArray<DIACCADATA> olData;

	CTime olFrom(1998,10,25,0,0,0);
	CTime olTo(1999,03,27,23,59,59);


	if(!m_From.GetStatus())
	{
		MessageBox(GetString(IDS_NO_DATE), GetString(IDS_STRING908), MB_OK);
		return;
	}
	if(!m_To.GetStatus())
	{
		MessageBox(GetString(IDS_NO_DATE), GetString(IDS_STRING908), MB_OK);
		return;
	}
	CString olDatFrom, olDatTo;
	m_From.GetWindowText(olDatFrom);
	m_To.GetWindowText(olDatTo);
	olFrom = DateStringToDate(olDatFrom);
	olTo = DateStringToDate(olDatTo);


	if(olFrom == TIMENULL || olTo == TIMENULL)
	{
		MessageBox( GetString(IDS_STRING1657) ,  GetString(ST_FEHLER));
		return;
	}



	for(int i = ogCcaDiaFlightData.omData.GetSize() - 1; i >= 0; i--)
	{
		olData.RemoveAll();
		if(ogCcaDiaFlightData.omCcaData.GetCcaArray(ogCcaDiaFlightData.omData[i].Urno, olData))
		{

			for(int j = olData.GetSize() - 1; j >= 0; j--)
			{

				if(olData[j].IsSelected)
				{
					olUrnos.Add(ogCcaDiaFlightData.omData[i].Urno);
					break;
				}
			}
		}
	}


	//CTime olTime = CTime::GetCurrentTime();
	//TRACE("\nBEGIN %s", olTime.Format("%H:%M:%S"));


	ogCcaDiaFlightData.omCcaData.GetCommonUrnos(olCommonUrnos, true);


	int ilCount = olUrnos.GetSize();
	int ilCommonCount = olCommonUrnos.GetSize();

	m_CP_Progess.SetRange(0,ilCount + 1 + ilCommonCount);
	m_CP_Progess.SetStep(1);

	CTime olRefDate = ogCcaDiaFlightData.omFrom;


	bool blAll = false;
	
	if(m_CB_AllVT.GetCheck())
		blAll = true;


	for( i = 0; i < ilCommonCount; i++)
	{
		m_CP_Progess.OffsetPos(1);
//		ogCcaDiaFlightData.omCcaData.Expand(olCommonUrnos[i], olFrom, olTo,olRefDate, blAll);
	}

/*
	for( i = 0; i < ilCount; i++)
	{
		m_CP_Progess.OffsetPos(1);
		ogCcaDiaFlightData.Expand(olUrnos[i], olFrom, olTo);
	}
*/
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	m_CP_Progess.SetPos(0);
	
	//olTime = CTime::GetCurrentTime();
	//TRACE("\nEND %s", olTime.Format("%H:%M:%S"));






	
}

void CCAExpandDlg::OnAll() 
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));


	CTime olTime = CTime::GetCurrentTime();
	TRACE("\nBEGIN %s", olTime.Format("%H:%M:%S"));


	CUIntArray olCommonUrnos;


	CTime olFrom(1998,10,25,0,0,0);
	CTime olTo(1999,03,27,23,59,59);


	if(!m_From.GetStatus())
	{
		MessageBox(GetString(IDS_NO_DATE), GetString(IDS_STRING908), MB_OK);
		return;
	}
	if(!m_To.GetStatus())
	{
		MessageBox(GetString(IDS_NO_DATE), GetString(IDS_STRING908), MB_OK);
		return;
	}
	CString olDatFrom, olDatTo;
	m_From.GetWindowText(olDatFrom);
	m_To.GetWindowText(olDatTo);
	olFrom = DateStringToDate(olDatFrom);
	olTo = DateStringToDate(olDatTo);
	
	if(olFrom == TIMENULL || olTo == TIMENULL)
	{
		MessageBox( GetString(IDS_STRING1657) ,  GetString(ST_FEHLER));
		return;
	}

	if(olFrom < olTime)
	{
		MessageBox( GetString(IDS_STRING2262) ,  GetString(ST_FEHLER));
		return;
	}


	if (UpdateData(TRUE))
		UpdateData(FALSE);
	

	CCSPtrArray<DIACCADATA> olData;
	CString olTypA ("K");
	CString olTypB ("K");
	
	if (m_common)
		olTypA = "C";
	if (m_flight)
		olTypB = "";

	if (!ogCcaDiaFlightData.omCcaData.GetCca(olData, olTypA, olTypB, m_only, true))
	{
		MessageBox( GetString(IDS_STRING2263) ,  GetString(ST_FEHLER));
		return;
	}

	int ilCount = olData.GetSize();
	if (ilCount == 0)
		return;


	m_CP_Progess.SetRange(0,ilCount + 1);


//	ogCcaDiaFlightData.omCcaData.GetCommonUrnos(olCommonUrnos, false);


//	int ilCount = ogCcaDiaFlightData.omData.GetSize();
//	int ilCommonCount = olCommonUrnos.GetSize();

//	m_CP_Progess.SetRange(0,ilCount + 1 + ilCommonCount);
	m_CP_Progess.SetStep(1);


//	bool blAll = false;
	bool blAll = true;
	
//	if(m_CB_AllVT.GetCheck())
//		blAll = true;

	CTime olRefDate = ogCcaDiaFlightData.omFrom;

/*	for(int  i = 0; i < ilCommonCount; i++)
	{
		m_CP_Progess.OffsetPos(1);
		ogCcaDiaFlightData.omCcaData.Expand(olCommonUrnos[i], olFrom, olTo, olRefDate, blAll);
	}
*/

	CUIntArray olDayArray;
	olDayArray.Add(m_check1);
	olDayArray.Add(m_check2);
	olDayArray.Add(m_check3);
	olDayArray.Add(m_check4);
	olDayArray.Add(m_check5);
	olDayArray.Add(m_check6);
	olDayArray.Add(m_check7);

//#####
		char pclConfigPath[256];
		char pclExcelPath[256];
		char opTrenner[64];
		

		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
			strcpy(pclConfigPath, getenv("CEDA"));

		GetPrivateProfileString(ogAppName, "Excel", "DEFAULT",
			pclExcelPath, sizeof pclExcelPath, pclConfigPath);


		GetPrivateProfileString(ogAppName, "ExcelSeperator", ";",
			opTrenner, sizeof opTrenner, pclConfigPath);


		if(!strcmp(pclExcelPath, "DEFAULT"))
			CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING977), GetString(ST_FEHLER), MB_ICONERROR);



	ofstream of;
	ofstream ofRes;

	char* tmpvar;
	tmpvar = getenv("TMP");
	if (tmpvar == NULL)
		tmpvar = getenv("TEMP");

	if (tmpvar == NULL)
		return;

	CString olFileName = GetString(IDS_STRING2253);
	CString olFileNameRes = GetString(IDS_STRING2260);
	olFileName.Remove('*');
	olFileName.Remove('.');
	olFileName.Remove(':');
	olFileName.Remove('/');
	olFileName.Replace(" ", "_");
	
	char pHeader[256];
	strcpy (pHeader, tmpvar);
	CString path = pHeader;
	olFileName =  path + "\\" + olFileName + ".csv";
	olFileNameRes =  path + "\\" + olFileNameRes + ".csv";

	of.open( olFileName, ios::out);
	ofRes.open( olFileNameRes, ios::out);

	int ilwidth = 1;

	CString olTimeSet = GetString(IDS_STRING1920); //UTC
	if(bgGatPosLocal)
		olTimeSet = GetString(IDS_STRING1921); //LOCAL

	of  << setw(ilwidth) << olFileName << "     " << olTimeSet << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;

	of  << setw(ilwidth) << GetString(IDS_STRING2254)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING2257)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING2258)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING2257)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING2259)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING2255)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING2257)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING2256)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING2261)
		<< endl;
	ofRes  << setw(ilwidth) << olFileNameRes << "     " << olTimeSet << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;

	ofRes  << setw(ilwidth) << GetString(IDS_STRING2254)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING2257)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING2258)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING2257)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING2259)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING2255)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING2257)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING2256)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING2261)
		<< endl;


//#####





	bool blAllert = false;

	for(int i = 0; i < ilCount; i++)
	{
		m_CP_Progess.OffsetPos(1);
//		ogCcaDiaFlightData.Expand(ogCcaDiaFlightData.omData[i].Urno, olFrom, olTo);
//		ogCcaDiaFlightData.omCcaData.Expand(ogCcaDiaFlightData.omData[i].Urno, olFrom, olTo, olRefDate, blAll);
		if (!ogCcaDiaFlightData.omCcaData.Expand(olData[i].Urno, olFrom, olTo, olRefDate, olDayArray, of, ofRes, opTrenner, blAll))
			blAllert = true;
	}

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	m_CP_Progess.SetPos(0);
	
	olTime = CTime::GetCurrentTime();
	TRACE("\nEND %s", olTime.Format("%H:%M:%S"));

	of.close();
	ofRes.close();

	if (blAllert)
	{
		AskBox olDlg(this, GetString(ST_FRAGE), GetString(IDS_STRING2252), "Yes", "No");
 		if (olDlg.DoModal() != 1)
		{
 			return;
		}

		char pclTmp[256];
		strcpy(pclTmp, olFileName); 

	   /* Set up parameters to be sent: */
		char *args[4];
		args[0] = "child";
		args[1] = pclTmp;
		args[2] = NULL;
		args[3] = NULL;

		_spawnv( _P_NOWAIT , pclExcelPath, args );


//		char pclTmp[256];
		strcpy(pclTmp, olFileNameRes); 

	   /* Set up parameters to be sent: */
//		char *args[4];
		args[0] = "child";
		args[1] = pclTmp;
		args[2] = NULL;
		args[3] = NULL;

		_spawnv( _P_NOWAIT , pclExcelPath, args );

	}


}
