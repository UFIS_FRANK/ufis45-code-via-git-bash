#ifndef __REPORT_OVERNIGHT_TABLE_VIEWER_H__
#define __REPORT_OVERNIGHT_TABLE_VIEWER_H__

#include <stdafx.h>
#include <RotationDlgCedaFlightData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct OVERNIGHTTABLE_LINEDATA
{
	long AUrno;
	CString		Date;
	CString		Alc;
	int			OverCount;
};

/////////////////////////////////////////////////////////////////////////////
// ReportOvernightTableViewer

class ReportOvernightTableViewer : public CViewer
{
// Constructions
public:
    ReportOvernightTableViewer(RotationDlgCedaFlightData *popData, char *pcpInfo = NULL, char *pcpSelect = NULL);
    ~ReportOvernightTableViewer();

    void Attach(CCSTable *popAttachWnd);
	void SetParentDlg(CDialog* ppDlg);
    virtual void ChangeViewTo(const char *pcpViewName);
	void PrintPlanToFile(char *pcpDefPath);
// Internal data processing routines
private:
    void MakeLines();
	int  MakeLine(ROTATIONDLGFLIGHTDATA *prpAFlight,int ipOvernightCount);
	void MakeLineData(ROTATIONDLGFLIGHTDATA *prpAFlight, int ipOvernightCount, OVERNIGHTTABLE_LINEDATA &rpLine);
	void MakeColList(OVERNIGHTTABLE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList);
	int  CompareGefundenefluege(OVERNIGHTTABLE_LINEDATA *prpGefundenefluege1, OVERNIGHTTABLE_LINEDATA *prpGefundenefluege2);
	int  CompareFlight(OVERNIGHTTABLE_LINEDATA *prpFlight1, OVERNIGHTTABLE_LINEDATA *prpFlight2);
	int  GetVia(CCSPtrArray<VIADATA> *opVias, ROTATIONDLGFLIGHTDATA *prpFlight);
	ROTATIONDLGFLIGHTDATA* GetDeparture(ROTATIONDLGFLIGHTDATA *prpFlight);
	bool FindLine(OVERNIGHTTABLE_LINEDATA &rpLine);
// Operations
public:
	void DeleteAll();
	int CreateLine(OVERNIGHTTABLE_LINEDATA &prpGefundenefluege);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	void DrawHeader();
	char* GetDname();


// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
	CString omDname;
// Attributes
private:
    CCSTable *pomTable;
	CDialog* pomParentDlg;
	RotationDlgCedaFlightData *pomData;

public:
    CCSPtrArray<OVERNIGHTTABLE_LINEDATA> omLines;
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void GetHeader(void); 
	void PrintTableView(void);
	bool PrintTableLine(OVERNIGHTTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader(void);
	CCSPtrArray <TABLE_HEADER_COLUMN> omPrintHeadHeaderArray;
	CCSPrint *pomPrint;
	CString omTableName;
	char *pcmInfo;
	char *pcmSelect;
	CString omFooterName;

};

#endif //__ReportOvernightTableViewer_H__
