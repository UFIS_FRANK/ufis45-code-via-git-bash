#ifndef __STATISTIKMESSETABLEVIEWER_H__
#define __STATISTIKMESSETABLEVIEWER_H__

#include <stdafx.h>
#include <RotationDlgCedaFlightData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct STATISTIKMESSETABLE_LINEDATA
{
	long		Urno; 	// Eindeutige Datensatz-Nr.
	CString		Cntr;	// Anzahl der Bewegungen
	CString 	Tifa; 	// Zeitrahmen Ankunft

};

/////////////////////////////////////////////////////////////////////////////
// StatistikMesseTableViewer

class StatistikMesseTableViewer : public CViewer
{
// Constructions
public:
    StatistikMesseTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo = NULL, char *pcpSelect = NULL);
    ~StatistikMesseTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(int* piCnt= NULL);
// Internal data processing routines
private:
    void PrepareLines();
    void MakeLines();
	void MakeLine(CTime Tifdold, int iCntr);
	void MakeResultLine(int ilCount);
// Operations
public:
	void DeleteAll();
	void CreateLine(STATISTIKMESSETABLE_LINEDATA *prpStatistikMesse);
	void DeleteLine(int ipLineno);
	bool PrintPlanToFile(char *pcpDefPath);
// Window refreshing routines
public:
	void UpdateDisplay();
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomStatistikMesseTable;
	CCSPtrArray<ROTATIONDLGFLIGHTDATA> *pomData;
public:
    CCSPtrArray<STATISTIKMESSETABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(STATISTIKMESSETABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	char *pcmInfo;
	char *pcmSelect;
	char *pcmSpan;
	int *pimCnt;
	CString omTableName;
	CString omFileName;
	CString omFooterName;

};

#endif //__STATISTIKMESSETABLEVIEWER_H__
