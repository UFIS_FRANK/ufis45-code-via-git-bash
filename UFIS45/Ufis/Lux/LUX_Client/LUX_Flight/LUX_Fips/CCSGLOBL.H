// CCSGlobl.h: interface for the CCSGlobl class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CCSGLOBL_H__55D90F34_191F_11D1_82BE_0080AD1DC701__INCLUDED_)
#define AFX_CCSGLOBL_H__55D90F34_191F_11D1_82BE_0080AD1DC701__INCLUDED_

#ifndef __CCSGLOBL_H__
#define __CCSGLOBL_H__

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include <CCSDefines.h>
#include <CCSTime.h>
#include <CCSPtrArray.h>
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>

// for memory leak detection!
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>


/////////////////////////////////////////////////////////////////////////////
// application globals 
extern char pcgHome[4];
extern char pcgHome4[5];
extern char pcgTableExt[10];
extern char pcgVersion[12];
extern char pcgInternalBuild[12];

extern CTimeSpan ogPosDefAllocDur;
extern CTimeSpan ogGatDefAllocDur;
extern CTimeSpan ogBltDefAllocDur;
extern CTimeSpan ogWroDefAllocDur;

// timspan for postfilghts
	//days in the past
extern CTimeSpan ogTimeSpanPostFlight;
	//days in the future
extern CTimeSpan ogTimeSpanPostFlightInFuture;


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Global Variable Section


class CCSBcHandle;
class CCSCedaCom;
class CCSDdx;
class CCSLog;
class SeasonCedaFlightData;
class SeasonCollectCedaFlightData;
class SeasonDlgCedaFlightData;
class RotationCedaFlightData;
class RotationDlgCedaFlightData;
class RotGDlgCedaFlightData;
class CedaCfgData;
class CCSBcHandle;
class CBasicData;
class CCSBasic;
class PrivList;
class CButtonListDlg;
class SeasonAskDlg;
class RotationTables;
class CSeasonTableDlg;
class CSeasonDlg;
class RotationDlg;
class RotGroundDlg;
class CedaBasicData;
class SeasonCollectDlg;
class CedaInfData;
class Konflikte;
class KonflikteDlg;
class DiaCedaFlightData;
class CcaCedaFlightData;
class CedaDiaCcaData;
class FlightDiagram;
class PosDiagram;
class GatDiagram;
class WroDiagram;
class BltDiagram;
class CcaDiagram;
class DataSet;
class WoResTableDlg;
class CCcaCommonTableDlg;
class CCcaCommonDlg;
class CicDemandTableDlg;
class CicNoDemandTableDlg;
class CicConfTableDlg;
class SpotAllocation;
class BltOverviewTableDlg;
class BltKonflikteDlg;
class CReportSelectDlg;
class FlightSearchTableDlg;
class BltCcaTableDlg;
class CedaResGroupData;
class CBackGround;
class CedaFlightUtilsData;

// MBR Anfang
class DailyCedaFlightData;
class DailyScheduleTableDlg;

extern DailyScheduleTableDlg *pogDailyScheduleTableDlg;
extern DailyCedaFlightData ogDailyCedaFlightData;
// MBR Ende


struct CCADATA;
struct NEW_SEASONDLG_DATA;
struct ROTATIONDLGFLIGHTDATA;
struct SEASONDLGFLIGHTDATA;


#include <UFISAmSink.h>
extern IUFISAmPtr pConnect;  // for com-interface

extern SpotAllocation ogSpotAllocation;

extern CCSBcHandle ogBcHandle;
extern CCSCedaCom ogCommHandler;  // The one and only CedaCom object
extern CCSDdx ogDdx;
extern CCSLog ogLog;
extern CedaBasicData ogBCD;

extern CTimeSpan ogLocalDiff;
extern CTime ogUtcStartTime;

extern SeasonCollectCedaFlightData ogSeasonCollectFlights;
extern CedaFlightUtilsData ogFlightUtilsData;
extern SeasonCedaFlightData ogSeasonFlights;
extern SeasonDlgCedaFlightData ogSeasonDlgFlights;
extern RotationCedaFlightData ogRotationFlights;
extern RotationDlgCedaFlightData ogRotationDlgFlights;
//extern RotGDlgCedaFlightData ogRotGDlgFlights;
extern DiaCedaFlightData ogDiaFlightData;
//extern DiaCedaFlightData ogBltDiaFlightData;
//extern DiaCedaFlightData ogGatDiaFlightData;
extern DiaCedaFlightData ogPosDiaFlightData;
//extern DiaCedaFlightData ogWroDiaFlightData;
extern CcaCedaFlightData ogCcaDiaFlightData;


extern FlightDiagram *pogFlightDiagram;
extern PosDiagram	 *pogPosDiagram;
extern GatDiagram	 *pogGatDiagram;
extern WroDiagram	 *pogWroDiagram;
extern BltDiagram	 *pogBltDiagram;
extern CcaDiagram	 *pogCcaDiagram;
extern WoResTableDlg	*pogWoResTableDlg;
extern CicDemandTableDlg *pogCicDemandTableDlg;
extern CicNoDemandTableDlg *pogCicNoDemandTableDlg;
extern CicConfTableDlg *pogCicConfTableDlg;

extern CBackGround *pogBackGround;
extern CButtonListDlg *pogButtonList;
extern SeasonAskDlg *pogSeasonAskDlg;

extern CCSBasic ogCCSBasic;

extern CBasicData ogBasicData;
extern CedaCfgData ogCfgData;
extern CedaResGroupData ogResGroupData;

extern bool bgDebug;
extern bool bgComOk;

extern RotationTables *pogRotationTables;
extern CSeasonTableDlg *pogSeasonTableDlg;
extern CSeasonDlg *pogSeasonDlg;
extern RotationDlg *pogRotationDlg;
extern RotGroundDlg *pogRotGroundDlg;

extern CReportSelectDlg *pogReportSelectDlg;
extern FlightSearchTableDlg *pogFlightSearchTableDlg;

extern SeasonCollectDlg *pogSeasonCollectDlg;
extern CedaInfData  ogInfData;

extern KonflikteDlg *pogKonflikteDlg;
extern Konflikte ogKonflikte;

extern DataSet ogDataSet;


extern 	CCcaCommonTableDlg *pogCommonCcaTable;
extern  CCcaCommonDlg *pogCommonCcaDlg;

// For Com-Interface to the new TelexPool
extern CUFISAmSink m_UFISAmSink;
//

extern PrivList ogPrivList;
extern char pcgUser[33];
extern char pcgPasswd[33];
extern CTimeSpan ogUtcDiff;

extern CStringArray ogDssfFields;
extern int ogDssfFltiId;
 
extern bool bgLocal;

extern bool bgViewBigBobs;
extern bool bmViewBigBobs;

extern bool bgSeasonLocal;
extern bool bgReportLocal;
extern bool bgGatPosLocal;
extern bool bgDailyLocal;
extern bool bgDailyWithAlt;
extern bool bgCreateDaily_OrgAtdBlue;

extern bool bgViewEditFilter;
extern bool bgViewEditSort;

extern bool bgShowOnGround;
extern bool bgCheckAllConflicts;
extern CTimeSpan ogOnGroundTimelimit;

extern CString ogFIDRemarkField;
extern bool bgAirport4LC;
extern CTimeSpan ogPosAllocBufferTime;
extern CTimeSpan ogGateAllocBufferTime;


// flags for customer functionality configuration
extern bool bgOffRelFuncGatpos;
extern bool bgOffRelFuncCheckin;

extern bool bgExtBltCheck; // additional baggage belt check
extern bool bgFlightIntYelBar; // yellow bars when flight is international
extern bool bgFlightIDEditable; // is editing of flight id allowed?

extern bool bgBltHeight;
extern bool bgGatHeight;
extern bool bgPosHeight;
extern bool bgWroHeight;
//////


extern bool bgPrintDlgOpen;

extern bool bgPosDiaAutoAllocateInWork;

extern CBrush *pogGatBrush;
extern CBrush *pogPosBrush;
extern CBrush *pogBltBrush;
extern CBrush *pogCcaBrush;
extern CBrush *pogWroBrush;
extern CBrush *pogNotAvailBrush;
extern CBrush *pogNotValidBrush;
extern bool bgKlebefunktion;

extern	CString ogInsertString;
extern	CString ogUpdateString;
extern	CString ogDeleteString;

extern bool	bgInsert;
extern bool	bgDelete;
extern bool	bgUpdate;

enum enumColorIndexes
{
	IDX_GRAY=2,IDX_GREEN,IDX_RED,IDX_BLUE,IDX_SILVER,IDX_MAROON,
	IDX_OLIVE,IDX_NAVY,IDX_PURPLE,IDX_TEAL,IDX_LIME,
	IDX_YELLOW,IDX_FUCHSIA,IDX_AQUA, IDX_WHITE,IDX_BLACK,IDX_ORANGE,
	IDX_TOWING,IDX_CREAM
};



enum DLG_ACTION 
{ 
    DLG_INSERT,DLG_UPDATE
   
};  

//enum enumBarType{BAR_FLIGHT,BAR_SPECIAL,BKBAR,GEDBAR,BAR_BREAK,BAR_ABSENT,BAR_SHADOW};


enum ChartState{ Minimized, Normal, Maximized };



#define MAXCOLORS 64
#define FIRSTCONFLICTCOLOR 31

// Symbolic colors (helper constants for CGateDiagramDialog -- testing purpose only)
#define BLACK   RGB(  0,   0,   0)
#define MAROON  RGB(128,   0,   0)          // dark red
#define GREEN   RGB(  0, 128,   0)          // dark green
#define OLIVE   RGB(128, 128,   0)          // dark yellow
#define NAVY    RGB(  0,   0, 128)          // dark blue
#define PURPLE  RGB(128,   0, 128)          // dark magenta
#define TEAL    RGB(  0, 128, 128)          // dark cyan
#define GRAY    RGB(128, 128, 128)          // dark gray
#define SILVER  RGB(192, 192, 192)          // light gray
#define RED     RGB(255,   0,   0)
#define ORANGE  RGB(255, 132,   0)
#define LIME    RGB(  0, 255,   0)          // green
#define YELLOW  RGB(255, 255,   0)
#define BLUE    RGB(  0,   0, 255)
#define FUCHSIA RGB(255,   0, 255)          // magenta
#define AQUA    RGB(  0, 255, 255)          // cyan
#define WHITE   RGB(255, 255, 255)
#define LGREEN  RGB(128, 255, 128)
#define CREAM	RGB(249, 240, 132)			// cream
#define TOWING	RGB(  0, 152, 225)			// marin

// Colors for flight table entries
#define COLOR_FLIGHT_OPERATION BLACK
#define COLOR_FLIGHT_SCHEDULED BLUE
#define COLOR_FLIGHT_PROGNOSE GREEN
#define COLOR_FLIGHT_DIVERTED BLACK
#define COLOR_FLIGHT_REROUTED BLACK
#define COLOR_FLIGHT_RETTAXI RED
#define COLOR_FLIGHT_RETFLIGHT RED
#define COLOR_FLIGHT_CXX RED
#define COLOR_FLIGHT_NOOP ORANGE


extern COLORREF ogColors[];
extern CBrush *ogBrushs[];
extern CString ogAppName;
extern CString ogCustomer;

extern COLORREF lgBkColor;
extern COLORREF lgTextColor;
extern COLORREF lgHilightColor;

extern enum enumRecordState egRecordState;

extern BOOL bgIsInitialized;

extern bool bgSeasonLocal;


// CFont : translate from logical point to physical pixel
#define PT_TO_PIXELS(dc, pt)    (- MulDiv( pt, (dc).GetDeviceCaps( LOGPIXELSY ), 72 ))


/////////////////////////////////////////////////////////////////////////////
// IDs


#define IDC_PREV            0x4008
#define IDC_NEXT            0x4009
#define IDC_INPLACEEDIT		0x400c

/////////////////////////////////////////////////////////////////////////////
// IDs from the origin of a conflict: 

#define MOD_ID_ALL			255
#define MOD_ID_ROTATION		2
#define MOD_ID_SEASON		4
#define MOD_ID_DIA			8

#define SUB_MOD_ALL        255
#define SUB_MOD_BLT        4
#define SUB_MOD_WRO        8
#define SUB_MOD_PST        16
#define SUB_MOD_GAT        32
#define SUB_MOD_NOTIMP		64



/////////////////////////////////////////////////////////////////////////////
// Messages


// Message and constants which are used to handshake viewer and the attached diagram
#define WM_POSITIONCHILD				(WM_USER + 201)
#define WM_UPDATEDIAGRAM				(WM_USER + 202)
#define UD_UPDATEGROUP                  (WM_USER + 203)
#define UD_DELETELINE                   (WM_USER + 204)
#define UD_DELETEGROUP                  (WM_USER + 205)
#define UD_INSERTLINE                   (WM_USER + 206)
#define UD_INSERTGROUP                  (WM_USER + 207)
#define UD_UPDATELINEHEIGHT             (WM_USER + 208)
#define UD_UPDATELINE					(WM_USER + 209)
#define UD_UPDATEDLG					(WM_USER + 210)
#define WM_REASSIGNFINISHED				(WM_USER + 211)
#define WM_ATTENTIONSETCOLOR			(WM_USER + 212)
#define UD_REASSIGNFINISHED				(WM_USER + 212)
#define WM_SETTIMELINES				(WM_USER + 215)
#define WM_REPAINT_ALL				(WM_USER + 216)
#define WM_SAS_PROGESS_INIT				(WM_USER + 217)
#define WM_SAS_PROGESS_UPDATE			(WM_USER + 218)
#define WM_RETURNTAXI					(WM_USER + 219)

#define WM_CCSBUTTON_RBUTTONDOWN		(WM_USER + 350)
#define BAR_CHANGED (WM_USER+11006)
#define WM_MARK_BAR						(WM_USER + 351)


// Messages for right mouse button menues
#define WM_MENU_RANGE_START				(WM_USER + 1000)
#define WM_MENU_RANGE_END				(WM_USER + 2000)


/////////////////////////////////////////////////////////////////////////////
// Font variable

extern CFont ogSmallFonts_Regular_6;
extern CFont ogSmallFonts_Regular_7;
extern CFont ogMSSansSerif_Regular_8;
extern CFont ogMSSansSerif_Bold_8;
extern CFont ogMSSansSerif_Bold_7;
extern CFont ogCourier_Bold_10;
extern CFont ogCourier_Bold_8;
extern CFont ogCourier_Regular_10;
extern CFont ogCourier_Regular_8;
extern CFont ogCourier_Regular_9;
extern CFont ogMS_Sans_Serif_8;

extern CFont ogTimesNewRoman_9;
extern CFont ogTimesNewRoman_12;
extern CFont ogTimesNewRoman_16;
extern CFont ogTimesNewRoman_30;

extern CFont ogSetupFont;


extern CFont ogScalingFonts[30];
extern int igFontIndex1;
extern int igFontIndex2;
extern int igDaysToRead;


void InitFont();
void DeleteBrushes();
void CreateBrushes();


enum{MS_SANS6, MS_SANS8, MS_SANS12, MS_SANS16, 
     MS_SANS6BOLD, MS_SANS8BOLD, MS_SANS12BOLD, MS_SANS16BOLD};

struct FONT_INDEXES
{
	int VerticalScale;
	int Chart;
	FONT_INDEXES(void)
	{VerticalScale=0;Chart=0;}
};


/////////////////////////////////////////////////////////////////////////////
// Color and brush variables

extern COLORREF ogColors[];
extern CBrush *ogBrushs[];

/////////////////////////////////////////////////////////////////////////////



struct TIMEFRAMEDATA
{
	CTime StartTime;
	CTime EndTime;
	TIMEFRAMEDATA(void)
	{StartTime=TIMENULL;EndTime=TIMENULL;}
};


extern bool bgNoScroll;
extern bool bgOnline;
extern bool bgIsButtonListMovable;

class CInitialLoadDlg;
extern CInitialLoadDlg *pogInitialLoad;

/////////////////////////////////////////////////////////////////////////////
// IDs

//***** moved from "ccsglobl.h" by Damkerng 04/30/96 18:30
//***** moved from "tscall.cpp" by Pichate May 08,96 18:15
#define IDC_TIMESCALE       0x4001

//***** moved from "ccsglobl.h" by Damkerng 04/30/96 18:30
//***** moved from "vscale.cpp" by Pichate May 08,96 18:15
#define IDC_VERTICALSCALE   0x4002

//***** moved from "ccsglobl.h" by Damkerng 04/30/96 18:30
//***** moved from "gbar.cpp" by Pichate May 08,96 18:15
#define IDC_GANTTBAR        0x4003

//#define IDD_DIAGRAM         0x4004
#define IDD_CHART           0x4004
#define IDC_CHARTBUTTON     0x4005
#define IDC_CHARTBUTTON2    0x4006
#define IDD_GANTT           0x4007

#define IDC_PREV            0x4008
#define IDC_NEXT            0x4009
#define IDC_ARRIVAL         0x400a
#define IDC_DEPARTURE       0x400b
#define IDC_INPLACEEDIT		0x400c
#define IDC_CHARTBUTTON3    0x400d
#define IDC_CHARTBUTTON4    0x400e

#define IDD_GANTTCHART      0x4101
#define IDC_CHARTBUTTON_TOWING    0x4102


#define IDM_ROTATION		11L
#define IDM_SEASON			14L
#define IDM_IMPORT			17L
#define IDM_COMMONCCA		19L
#define IDM_DIACCA			21L
#define IDM_SEASON_COL		23L


/////////////////////////////////////////////////////////////////////////////
// Messages

// Message sent from PrePlanTable to the parent window when closed
// Id 24-Sep-96
// Fix some bug here, since CCI diagram and Gate diagram shares the same message number.
// This surely will crash the machine or destroy our memory blocks if the user terminate
// the diagram with Alt-F4.
#define WM_STAFFTABLE_EXIT          (WM_APP + 20)	/* tables */
#define WM_GEQTABLE_EXIT            (WM_APP + 21)	/* tables */
#define WM_INFTABLE_EXIT            (WM_APP + 22)	/* tables */
#define WM_HWDIA_EXIT				(WM_APP + 23)	/* tables */
#define WM_FLIGHTDIA_EXIT           (WM_APP + 24)	/* tables */
#define WM_STAFFDIA_EXIT            (WM_APP + 25)	/* tables */
#define WM_CONFTABLE_EXIT           (WM_APP + 26)	/* tables */
#define WM_FLIGHTTABLE_EXIT         (WM_APP + 27)	/* tables */
#define WM_MAGNETICTABLE_EXIT       (WM_APP + 28)	/* tables */
#define WM_ATTENTIONTABLE_EXIT      (WM_APP + 29)	/* tables */
#define WM_CONFLICTTABLE_EXIT       (WM_APP + 30)	/* tables */
#define WM_LOCKWINDOWUPDATE         (WM_APP + 31)	/* tables */
#define WM_ZOLLTABLE_EXIT			(WM_APP + 32)	/* tables */

#define WM_PLACEROUND				(WM_APP + 33)	/* tables */
#define WM_TOUCHANDGO				(WM_APP + 34)	/* tables */
#define WM_MAXIMIZE_CHILD			(WM_APP + 35)	
#define WM_RETURNFLIGHT				(WM_APP + 36)	
#define WM_TOWING					(WM_APP + 37)	



/////////////////////////////////////////////////////////////////////////////
// Drag Information Type

enum 
{
	DIT_CCA,	
	DIT_FLIGHT,	
	DIT_ANSICHT_GRP,
	DIT_BLT_GANTT, //Inside the Gantt
	DIT_GAT_GANTT, //Inside the Gantt
	DIT_POS_GANTT, //Inside the Gantt
	DIT_WRO_GANTT, //Inside the Gantt
	DIT_CCA_GANTT, //Inside the Gantt
	DIT_CCA_DEMAND, //
	DIT_CCA_KKEY, //
	DIT_FROM_DAYLY //Dayly schedule ==> Position Gantt
};

// DIT 0 - 49


/*
enum
{
	DLG_NEW,
	DLG_COPY,
	DLG_CHANGE
};
*/

enum enumDDXTypes
{
	APP_EXIT,
	DATA_RELOAD,
	TABLE_FONT_CHANGED,
    UNDO_CHANGE, APP_LOCKED, APP_UNLOCK,
    BC_CFG_INSERT, BC_CFG_CHANGE, CFG_CHANGE, CFG_INSERT,CFG_DELETE,
	KONF_CHANGE,KONF_DELETE,KONF_INSERT,

    // from here, there are defines for your project
	DONT_NEED,
	S_FLIGHT_CHANGE, S_FLIGHT_DELETE, S_FLIGHT_UPDATE,
	S_FLIGHT_INSERT,
	D_FLIGHT_CHANGE, D_FLIGHT_DELETE, D_FLIGHT_UPDATE,
	D_FLIGHT_INSERT,
	DS_FLIGHT_CHANGE, DS_FLIGHT_DELETE, DS_FLIGHT_UPDATE,
	DS_FLIGHT_INSERT,
	CCA_FLIGHT_CHANGE, CCA_FLIGHT_DELETE, CCA_FLIGHT_UPDATE,
	CCA_FLIGHT_INSERT,
	R_DLG_FLIGHT_CHANGE,
	R_DLG_ROTATION_CHANGE,
	RG_DLG_FLIGHT_CHANGE,
	RG_DLG_ROTATION_CHANGE,
	S_DLG_FLIGHT_CHANGE,
	S_DLG_ROTATION_CHANGE,
	S_DLG_FLIGHT_DELETE,
	R_FLIGHT_CHANGE, R_FLIGHT_DELETE,
	C_FLIGHT_CHANGE, C_FLIGHT_DELETE,C_FLIGHT_UPDATE,
	BC_FLIGHT_CHANGE, BC_FLIGHT_DELETE,
	S_DLG_GET_NEXT,
	S_DLG_GET_PREV,
	DIACCA_DELETE,DIACCA_CHANGE,DIACCA_SELECT,CCA_KKEY_CHANGE,
	BC_ALT_CHANGE,BC_ALT_DELETE,ALT_CHANGE,ALT_DELETE,ALT_INSERT,    
	BC_ACT_CHANGE,BC_ACT_DELETE,ACT_CHANGE,ACT_DELETE,ACT_INSERT,    
	BC_ACR_CHANGE,BC_ACR_DELETE,ACR_CHANGE,ACR_DELETE,ACR_INSERT,
	BC_APT_CHANGE,BC_APT_DELETE,APT_CHANGE,APT_DELETE,APT_INSERT,    
	BC_RWY_CHANGE,BC_RWY_DELETE,RWY_CHANGE,RWY_DELETE,RWY_INSERT,    
	BC_TWY_CHANGE,BC_TWY_DELETE,TWY_CHANGE,TWY_DELETE,TWY_INSERT,   
	BC_PST_CHANGE,BC_PST_DELETE,PST_CHANGE,PST_DELETE,PST_INSERT, 
	BC_GAT_CHANGE,BC_GAT_DELETE,GAT_CHANGE,GAT_DELETE,GAT_INSERT,
	BC_CIC_CHANGE,BC_CIC_DELETE,CIC_CHANGE,CIC_DELETE,CIC_INSERT,   
	BC_BLT_CHANGE,BC_BLT_DELETE,BLT_CHANGE,BLT_DELETE,BLT_INSERT,   
	BC_EXT_CHANGE,BC_EXT_DELETE,EXT_CHANGE,EXT_DELETE,EXT_INSERT,   
	BC_DEN_CHANGE,BC_DEN_DELETE,DEN_CHANGE,DEN_DELETE,DEN_INSERT,   
	BC_MVT_CHANGE,BC_MVT_DELETE,MVT_CHANGE,MVT_DELETE,MVT_INSERT,   
	BC_HAG_CHANGE,BC_HAG_DELETE,HAG_CHANGE,HAG_DELETE,HAG_INSERT,    
	BC_NAT_CHANGE,BC_NAT_DELETE,NAT_CHANGE,NAT_DELETE,NAT_INSERT,   
	BC_WRO_CHANGE,BC_WRO_DELETE,WRO_CHANGE,WRO_DELETE,WRO_INSERT,   
	BC_HTY_CHANGE,BC_HTY_DELETE,HTY_CHANGE,HTY_DELETE,HTY_INSERT,   
	BC_STY_CHANGE,BC_STY_DELETE,STY_CHANGE,STY_DELETE,STY_INSERT,   
	BC_FID_CHANGE,BC_FID_DELETE,FID_CHANGE,FID_DELETE,FID_INSERT,
	BC_SEA_CHANGE, BC_SEA_NEW, BC_SEA_DELETE,
	BC_CFL_CHANGE, BC_CFL_NEW,
	SEA_NEW, SEA_DELETE, SEA_CHANGE,
	CFL_NEW, CFL_CHANGE, CFL_RELOAD,
	AFT_RELOAD,
	BLK_CHANGE,
	BCD_CIC_CHANGE,
	BCD_WRO_CHANGE,
	BCD_PST_CHANGE,
	BCD_BLT_CHANGE,
	BCD_GAT_CHANGE,
	BCD_NAT_CHANGE,
	BC_CCA_CHANGE, BC_CCA_NEW, BC_CCA_DELETE,
	//CCA_NEW, CCA_DELETE, CCA_CHANGE,
	CCA_COMTABLE_NEW, CCA_COMTABLE_DELETE, CCA_COMTABLE_CHANGE,
	CCA_ROTDLG_NEW, CCA_ROTDLG_DELETE, CCA_ROTDLG_CHANGE,
	CCA_SEADLG_NEW, CCA_SEADLG_CHANGE, CCA_SEADLG_DELETE,
	BC_INF_NEW,BC_INF_DELETE,BC_INF_CHANGE,INF_NEW,INF_DELETE,INF_CHANGE,
	MARK_BAR,
	REDISPLAY_ALL, STAFFDIAGRAM_UPDATETIMEBAND,
//MWO
	BC_GRM_CHANGE,BC_GRM_NEW,BC_GRM_DELETE,GRM_NEW,GRM_CHANGE,GRM_DELETE,
//END MWO
	BCD_VIP_UPDATE,BCD_VIP_DELETE,BCD_VIP_INSERT,
	BCD_HAI_UPDATE,BCD_HAI_DELETE,BCD_HAI_INSERT,
	SHOW_FLIGHT,
	SAS_VIEW_CHANGED,
	SAS_GET_CUR_VIEW,
	S_DLG_ROTATION_SPLIT,
	BC_TEST,
	BC_RAC_RELOAD
};


enum FipsModules
{ FIPSMODULES_NONE, BLTDIA, CCADIA, GATDIA, POSDIA, WRODIA, SEASONSCHEDULES, DAILYSCHEDULES, 
	DAILYROTATIONS, FLIGHTDIAGRAM, CONFLICTS, REPORTS, SETUP};




////////////////////////////////////////////////////////////////////////////////////
// used by  xxxxCedaFlightData

struct JFNODATA
{
	long Urno;
	char Alc3[4];
	char Fltn[6];
	char Flns[3];
	bool	InUse;

	JFNODATA(void)
	{
		strcpy(Alc3, "");
		strcpy(Fltn, "");
		strcpy(Flns, "");
		Urno = 0;
		InUse = false;
	}

};



struct VIADATA
{
	char	Fids[2];
	char	Apc3[4];
	char	Apc4[5];
	CTime	Stoa;
	CTime	Etoa;
	CTime	Land;
	CTime	Onbl;
	CTime	Stod;
	CTime	Etod;
	CTime	Ofbl;
	CTime	Airb;
	bool	InUse;
	VIADATA(void)
	{ 
		sprintf(Fids, "");
		sprintf(Apc3, "");
		sprintf(Apc4, "");
		Stoa = -1;
		Etoa = -1;
		Land = -1;
		Onbl = -1;
		Stod = -1;
		Etod = -1;
		Ofbl = -1;
		Airb = -1;
		InUse = false;
	}
};


CString GetString(UINT ID);



struct KONFLIST
{
	char	Text[258];
	int		Type;
	long	Urno;

	KONFLIST(void)
	{
		strcpy(Text, "");
		Type = 0;
		Urno = -1;
	};

};






extern ofstream of_catch;
extern ofstream of_debug;

extern ofstream *pogBCLog;

#define  CCS_TRY try{
#define  CCS_CATCH_ALL }\
						catch(...)\
						{\
						char pclExcText[512]="";\
						sprintf(pclExcText, "File: %s  ==> Source-Line: %d", __FILE__, __LINE__);\
						::MessageBox(NULL, pclExcText, "Error", MB_YESNO);\
						of_catch << pclExcText << endl;\
						}









/////////////////////////////////////////////////////////////////////////////
// BDPS-SEC 

#define SetWndStatAll(clStat, plWnd)\
	if(clStat=='1') {plWnd.ShowWindow(SW_SHOW);plWnd.EnableWindow(TRUE);}\
	else if(clStat=='0') {plWnd.ShowWindow(SW_SHOW);plWnd.EnableWindow(FALSE);}\
	else if(clStat=='-') plWnd.ShowWindow(SW_HIDE);

#define SetWndStatPrio_1(clStat1,clStat2, plWnd)\
	if(clStat1=='-'||clStat2=='-') plWnd.ShowWindow(SW_HIDE);\
	else if(clStat1=='0'||clStat2=='0') {plWnd.ShowWindow(SW_SHOW);plWnd.EnableWindow(FALSE);}\
	else if(clStat1=='1'||clStat2=='1') {plWnd.ShowWindow(SW_SHOW);plWnd.EnableWindow(TRUE);}

#define SetpWndStatAll(clStat, plWnd)\
	if(clStat=='1') {plWnd->ShowWindow(SW_SHOW);plWnd->EnableWindow(TRUE);}\
	else if(clStat=='0') {plWnd->ShowWindow(SW_SHOW);plWnd->EnableWindow(FALSE);}\
	else if(clStat=='-') plWnd->ShowWindow(SW_HIDE);


// end globals
/////////////////////////////////////////////////////////////////////////////


#endif //__CCSGLOBL_H__
#endif // !defined(AFX_CCSGLOBL_H__55D90F34_191F_11D1_82BE_0080AD1DC701__INCLUDED_)
