#ifndef __REPORT_SAME_DCD_TABLE_VIEWER_H__
#define __REPORT_SAME_DCD_TABLE_VIEWER_H__

#include <stdafx.h>
#include <RotationDlgCedaFlightData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct SAMEDCDTABLE_LINEDATA
{
	long AUrno;
	long ARkey;
	CString		ADate;
	CTime 		AEtai; 	
	CString		AFlno;
	CTime 		ALand; 	
	CString		AOrg3;
	CString		AOrg4;
	CTime		AStoa;
	CString		AVia3;
	CString 	AVian;
	CString		AAct;


	long DUrno;
	long DRkey;
	CString		DAct;
	CTime 		DAirb;
	CString		DDate;
	CString		DDes3;
	CString		DDes4;
	CTime 		DEtdi;
	CString		DFlno;
	CTime		DStod;
	CString		DVia3;
	CString 	DVian;

	CString		Adid;

};

/////////////////////////////////////////////////////////////////////////////
// ReportSameDCDTableViewer

class ReportSameDCDTableViewer : public CViewer
{
// Constructions
public:
    ReportSameDCDTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo = NULL, char *pcpSelect = NULL);
    ~ReportSameDCDTableViewer();

    void Attach(CCSTable *popAttachWnd);
	void SetParentDlg(CDialog* ppDlg);
    virtual void ChangeViewTo(const char *pcpViewName);
	void PrintPlanToFile(char *pcpDefPath);
// Internal data processing routines
private:
    void MakeLines();
	int  MakeLine(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight);
	void MakeLineData(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight, SAMEDCDTABLE_LINEDATA &rpLine);
	void MakeColList(SAMEDCDTABLE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList);
	int  CompareGefundenefluege(SAMEDCDTABLE_LINEDATA *prpGefundenefluege1, SAMEDCDTABLE_LINEDATA *prpGefundenefluege2);
	int  CompareFlight(SAMEDCDTABLE_LINEDATA *prpFlight1, SAMEDCDTABLE_LINEDATA *prpFlight2);
	int  GetVia(CCSPtrArray<VIADATA> *opVias, ROTATIONDLGFLIGHTDATA *prpFlight);

// Operations
public:
	void DeleteAll();
	int CreateLine(SAMEDCDTABLE_LINEDATA &prpGefundenefluege);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	void DrawHeader();
	char* GetDname();


// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
	CString omDname;
// Attributes
private:
    CCSTable *pomTable;
	CDialog* pomParentDlg;
	CCSPtrArray<ROTATIONDLGFLIGHTDATA> *pomData;

public:
    CCSPtrArray<SAMEDCDTABLE_LINEDATA> omLines;
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void GetHeader(void); 
	void PrintTableView(void);
	bool PrintTableLine(SAMEDCDTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader(void);
	CCSPtrArray <TABLE_HEADER_COLUMN> omPrintHeadHeaderArray;
	CCSPrint *pomPrint;
	CString omTableName;
	char *pcmInfo;
	char *pcmSelect;
	CString omFooterName;

};

#endif //__ReportSameDCDTableViewer_H__
