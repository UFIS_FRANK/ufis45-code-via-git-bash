#ifndef __ADDITIONAL_REPORT_11_VIEWER_H__
#define __ADDITIONAL_REPORT_11_VIEWER_H__

#include <stdafx.h>
#include <RotationDlgCedaFlightData.h>
#include <GanttChartReportWnd.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct ADDITIONAL_REPORT11_LINEDATA
{
	long AUrno;
	long ARkey;
	CString		AAct;
	CString		AAlc2;
	CString		AAlc3;
	CString		ADate;
	CTime 		AEtai; 	
	CString		AFlno;
	CTime 		ALand; 	
	CTime 		AStoa;
	CString		ATtyp;

	long DUrno;
	long DRkey;
	CString		DAct;
	CString		DAlc2;
	CString		DAlc3;
	CString		DDate;
	CString		DDes3;
	CString		DFlno;
	CString		DTtyp;
	CTime 		DStod;

};

/////////////////////////////////////////////////////////////////////////////
// AdditionalReport11TableViewer

class AdditionalReport11TableViewer : public CViewer
{
// Constructions
public:
    AdditionalReport11TableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, 
		                                 const CStringArray &opShownColumns,
		                                 char *pcpInfo = NULL, char *pcpSelect = NULL);
    ~AdditionalReport11TableViewer();

    void Attach(CCSTable *popAttachWnd);
	void SetParentDlg(CDialog* ppDlg);
    virtual void ChangeViewTo(const char *pcpViewName);
	void PrintPlanToFile(char *pcpDefPath);
// Internal data processing routines
private:
    void MakeLines();
	int  MakeLine(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight);
	void MakeLineData(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight, ADDITIONAL_REPORT11_LINEDATA &rpLine);
	void MakeColList(ADDITIONAL_REPORT11_LINEDATA *prlLine, 
		             CCSPtrArray<TABLE_COLUMN> &olColList, CString opCurrentAirline);
	int  CompareGefundenefluege(ADDITIONAL_REPORT11_LINEDATA *prpGefundenefluege1, ADDITIONAL_REPORT11_LINEDATA *prpGefundenefluege2);
	int  CompareFlight(ADDITIONAL_REPORT11_LINEDATA *prpFlight1, ADDITIONAL_REPORT11_LINEDATA *prpFlight2);
	int  GetVia(CCSPtrArray<VIADATA> *opVias, ROTATIONDLGFLIGHTDATA *prpFlight);
	void InitColumnsOneDay();
	void InitColumnsNDay();

// Operations
public:
	void DeleteAll();
	int CreateLine(ADDITIONAL_REPORT11_LINEDATA &prpGefundenefluege);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	void DrawHeader();


// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
	// Array der Header
	CStringArray omShownColumns;
	// Sapltengroese
	CUIntArray omAnzChar;
	CDialog* pomParentDlg;

    CCSTable *pomTable;
	CCSPtrArray<ROTATIONDLGFLIGHTDATA> *pomData;
	// Liste der existierenden Delays
	CStringArray omColumn0;
	CStringArray omColumn1;
	CStringArray omColumn2;
	CStringArray omColumn3;
	CStringArray omColumn4;
	// Auftreten der existierenden Delays
	CUIntArray omCountColumn0;
	CUIntArray omCountColumn1;
	CUIntArray omCountColumn2;
	int imTableLine;		// Zeilennumer fuer GetHeaderContent
	char pcmFirstDate[16];		//Time frame beginn
	char pcmLastDate[16];	//Time frame end
	long lmDays;

public:
    CCSPtrArray<ADDITIONAL_REPORT11_LINEDATA> omLines;
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
	CString GetHeaderContent(CString opCurrentColumns);
	int GetBarCount();
	int GetData(int ipIndex);
	CString GetTime(int ipIndex);

	// anzuzeigende Header als Array uebergeben
	void SetColumnsToShow(const CStringArray &opShownColumns);
	// Mapping Tabellenfeldname/Spaltenueberschrift
	CString GetFieldContent(CString opCurrentColumns);
	int GetFlightCount();

//Print 
	void GetHeader(void);
	CCSPtrArray <TABLE_HEADER_COLUMN> omPrintHeadHeaderArray;
	CCSPrint *pomPrint; 
	void PrintTableView(GanttChartReportWnd* popGanttChartReportWnd);
	bool PrintTableLine(ADDITIONAL_REPORT11_LINEDATA *prpLine,bool bpLastLine, 
		                CString opCurrentACType);
	bool PrintTableHeader(void);
	CString omTableName;
	char *pcmInfo;
	char *pcmSelect;
	CString omFooterName;
	CTime omTimeFirst;

protected:
	void SetFieldLength(CString opCurrentColumns);
	// Listen zum Zaehlen erstellen
	void MakeCountListOneDay(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight);
	void MakeCountListNDays(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight);

};

#endif