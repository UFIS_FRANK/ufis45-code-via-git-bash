// ProgressDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <fpms.h>
#include <ProgressDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CProgressDlg 


CProgressDlg::CProgressDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CProgressDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CProgressDlg)
	m_Progresstext = _T("");
	//}}AFX_DATA_INIT
	// m_CP_Progress.SetRange(0,130);
}


void CProgressDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CProgressDlg)
	DDX_Control(pDX, IDC_PROGRESSTEXT, m_CE_Progresstext);
	DDX_Control(pDX, IDC_PROGRESS, m_CP_Progress);
	DDX_Text(pDX, IDC_PROGRESSTEXT, m_Progresstext);
	//}}AFX_DATA_MAP
}
/*
void CProgressDlg::SetPosition(int iPos)
{
	m_CP_Progress.SetPos(iPos);
} */

BEGIN_MESSAGE_MAP(CProgressDlg, CDialog)
	//{{AFX_MSG_MAP(CProgressDlg)
		// HINWEIS: Der Klassen-Assistent f�gt hier Zuordnungsmakros f�r Nachrichten ein
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CProgressDlg 
