// Report19DailyCcaTableViewer.cpp : implementation file
//

/////////////////////////////////////////////////////////////////////////////
// Report19DailyCcaTableViewer

//	Liste Daily Cca-Plan

#include <stdafx.h>
#include <ReportSelectDlg.h>
#include <ReportPOPSItems.h>
#include <Report19DailyCcaTableViewer.h>
#include <CcsGlobl.h>
#include <CedaBasicData.h>
#include <CcaCedaFlightData.h>
#include <resrc1.h>


/* nur f�r Fileausgabe n�tig !
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
*/


#define	_MIT_TRACE_	FALSE

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


const int cimMaxWhere = 2048 ;	// Max. L�nge Suchstring f�r Cca Where Clause

// sortiert CCA Daten nach CTYP und CKES
static int CompareCca(const CCADATA **e1, const CCADATA **e2)
{

	int ilResult = 0 ;

	// COMMON zuerst, dann nach Zeit Ckes
	if ( CString((**e1).Ctyp) == CString((**e2).Ctyp) ) 
	{
		if( (**e1).Ckes == (**e2).Ckes ) ilResult = 0 ;
		else ilResult = (**e1).Ckes > (**e2).Ckes ? 1 : -1 ;

	}
	else
	{
		ilResult = CString((**e1).Ctyp) > CString((**e2).Ctyp) ? -1 : 1 ;	
	}




	return ilResult ;

}

/////////////////////////////////////////////////////////////////////////////
// ------------------------------------------------ Report19DailyCcaTableViewer


Report19DailyCcaTableViewer::Report19DailyCcaTableViewer(char *pcpInfo, char *pcpSelect, char *pspVR)
{
	bmSeason = false ;
	pcmInfo = pcpInfo;
	pcmSelect = pcpSelect;
    pomTable = NULL;
	omFlugUrnos.Empty() ;	// kommagetrennt Liste der Flugurnos
	omCcaData.ClearAll() ;	// CounterAllocation S�tze

	if( pspVR != NULL ) olVerkehrsRechte = pspVR ;	// hier: Where-Clause Infos ( kommagetrennt ) !
}

Report19DailyCcaTableViewer::~Report19DailyCcaTableViewer()
{
    DeleteAll();
	omBlkData.DeleteAll() ;
}

void Report19DailyCcaTableViewer::Attach(CTable *popTable)
{
    pomTable = popTable;
}



void Report19DailyCcaTableViewer::ChangeViewTo(const CTime &ropFromTime, const CTime &ropToTime)
{

	// nur Daily	
	bmSeason = false ;

    pomTable->ResetContent();
    DeleteAll();
    
	CTime olFromDayEnd(ropFromTime.GetYear(), ropFromTime.GetMonth(), ropFromTime.GetDay(), 23, 59, 59);
	CTime olToDayBegin(ropToTime.GetYear(), ropToTime.GetMonth(), ropToTime.GetDay(), 00, 00, 00);

	// TODO MakeLines und CedaDiaCcaData::MakeBlkData auf Zeitspanne umstellen
	if (olFromDayEnd - ropFromTime > ropToTime - olToDayBegin)
		MakeLines(ropFromTime);
	else
		MakeLines(ropToTime);

	UpdateDisplay();
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}


/////////////////////////////////////////////////////////////////////////////
// Report19DailyCcaTableViewer -- code specific to this class

void Report19DailyCcaTableViewer::MakeLines(const CTime &ropDay)
{

	DAILYRKEYLIST *prlRkey;
	POSITION pos;
	void *pVoid;
	
	// Ckeckin Allocation lokal (5/99)
	CcaCedaFlightData olCcaDiaFlightData ;
	
	// ToDo: evt. bereits vorher aufbauen + Vorbelegung variabel !
	int ilLc = 0 ;
	int ilCount = omDailyCedaFlightData.omData.GetSize() ;
	CStringArray olUrnoList ;
	CString olTermSelect ;	// Terminal Auswahl 
	CString olStartTermin ;
	CString olEndTermin ;
	CString olSelection ;	// Cca Where Clause
	CString olDummyString;

	olTermSelect.Empty() ;
	if( GetItemCount( olVerkehrsRechte ) != 3 )
	{
		// Unklarer Aufrufparameter pspVR !
		olStartTermin = CString( "00000000000000" ) ; 
		olEndTermin = CString( "00000000000000" ) ; 
		olVerkehrsRechte = CString("0") ;
		AfxMessageBox("MakeLines(): Parameter olVerkehrsRechte invalide !?" ) ;
	}
	else
	{
		olStartTermin = GetListItem( olVerkehrsRechte, 1, false ) ; 
		olEndTermin = GetListItem( olVerkehrsRechte, 2, false ) ; 
		olVerkehrsRechte = GetListItem( olVerkehrsRechte, 3 , false ) ;
	}
		
	// kommagetrennten String der FlightUrnos aufbauen
	for ( ilLc = 0; ilLc < ilCount; ilLc++)
	{
		if( omDailyCedaFlightData.omData[ilLc].Urno > 0)
		{
			CString olNextUrno ;
			olNextUrno.Format("%ld", omDailyCedaFlightData.omData[ilLc].Urno ) ; 
			if( ! omFlugUrnos.IsEmpty() ) 
				omFlugUrnos += "," ;
			omFlugUrnos += olNextUrno ; 
		}
	}

	// kommagetrennte FlightUrno-Liste ( je 100 Eintr�ge ) aufbauen
	ilCount = SplitItemList(omFlugUrnos, &olUrnoList, 100) ;

	char olCcaWhere[cimMaxWhere] ;	 // cimMaxWhere h�ngt von Eintr�ge (Para. 3) in SplitItemList( ..., 100 ) ab !!
	for( ilLc = 0; ilLc < ilCount; ilLc++)
	{
		olSelection = CString("WHERE ") + olTermSelect + CString("FLNU IN (");
		olSelection += olUrnoList[ilLc] + CString(")");
		// 5/99 nur Counter mit Nummer !
		olSelection += " AND CKIC <> ' '" ;
		ASSERT( olSelection.GetLength() < cimMaxWhere ) ; 
		strcpy( olCcaWhere, olSelection ) ;
		// nur fehlerfreie einlesen
		omCcaData.Read( olCcaWhere, ilLc ? false : true, false, true );
	}

	// Common CKI Counter mitausw�hlen OHNE Errorschalter
	olSelection = CString("WHERE CTYP='C' AND ") + olTermSelect ;
	
	olDummyString.Format("CKBS <='%s' AND CKES >= '%s'", olEndTermin, olStartTermin) ;
 	olSelection += olDummyString ;
	// 5/99 nur Counter mit Nummer !
	olSelection += " AND CKIC <> ' '" ;
	ASSERT( olSelection.GetLength() < cimMaxWhere ) ; 
	strcpy( olCcaWhere, olSelection ) ;
	// nur fehlerfreie einlesen
	omCcaData.Read( olCcaWhere, false, false, true );

	omCcaData.omData.Sort(CompareCca) ;
/*
	int ilAnzCca = omCcaData.omData.GetSize() ; // nur f�r TESTS !
	for( int Counter=0; Counter < ilAnzCca ; Counter++ )
	{
		TRACE("Counter [%s] [%d] [%s] [%s]\n", omCcaData.omData[Counter].Ctyp, Counter, omCcaData.omData[Counter].Ckic, omCcaData.omData[Counter].Ckes.Format("%H:%M.%S") ); 
	}
*/

	// 12/98: geblockte CIC ermitteln
	char pclBlkWhere[512]="";
	CString olBlkObj("REPBLK") ;	// Objektname BLK / REPBLK
	//CTime olFromTo ;
	//olFromTo = omTrafficDay != TIMENULL ? omTrafficDay : CTime::GetCurrentTime() ;
	sprintf(pclBlkWhere, " WHERE (NATO >= '%s' and NAFR <= '%s') OR (NAFR <= '%s' and NATO = ' ') OR  (NATO >= '%s'  and NAFR = ' ')",
		                                         ropDay.Format("%Y%m%d000000"), ropDay.Format("%Y%m%d235959"),
												 ropDay.Format("%Y%m%d235959"),
												 ropDay.Format("%Y%m%d000000"));
	ogBCD.Read(olBlkObj, pclBlkWhere);

	olCcaDiaFlightData.omCcaData.MakeBlkData( ropDay, -1, olBlkObj ) ;
	omBlkData.DeleteAll() ;
	int ilNaCount = olCcaDiaFlightData.omCcaData.omData.GetSize() ;
	for( int index = 0; index < ilNaCount; index++)
	{
		DIACCADATA *prlDiaCca = new DIACCADATA;
		*prlDiaCca = olCcaDiaFlightData.omCcaData.omData[index] ;	
		if( strcmp(prlDiaCca->Ctyp, "N" ) == 0 &&
			strcmp(prlDiaCca->Disp,GetString(IDS_STRING1487)) &&
			strcmp(prlDiaCca->Disp,GetString(IDS_STRING1488)) )	// nur geblockte CIC !
		{
 			omBlkData.Add( prlDiaCca ) ;
		}
	}
	
	bool blFirst=true; 
	for( pos=omDailyCedaFlightData.omRkeyMap.GetStartPosition(); pos != NULL; )
	{
		omDailyCedaFlightData.omRkeyMap.GetNextAssoc( pos, pVoid, (void *&)prlRkey );

		if(prlRkey->Rotation.GetSize() == 1)			// ein unverkn�pfter Flug
		{
			if(!strcmp(prlRkey->Rotation[0].Des3, pcgHome))
			{
				// hier sollten keine InBOunds ankommen !
				// wenn doch => ab nach Outbound
				MakeLine(NULL, prlRkey->Rotation[0].Urno, blFirst );
				blFirst=false ;
			}
			else if(!strcmp(prlRkey->Rotation[0].Org3, pcgHome))
			{
				MakeLine(NULL, prlRkey->Rotation[0].Urno, blFirst);
				blFirst=false ;
			}
		}

	}

//generate the headerinformation
	CString olTimeSet = GetString(IDS_STRING1920); //UTC
	if(bgReportLocal)
		olTimeSet = GetString(IDS_STRING1921); //LOCAL

	char pclHeader[256];
	sprintf(pclHeader, GetString(IDS_R_HEADER19), GetString(IDS_REPORTS_RB_CcaScheduleDaily), pcmSelect, pcmInfo, olTimeSet);
	omTableName = pclHeader;


}



//----------------------------------------------------------------------------------------
//------------------------------------------------	eine Tabellen Zeile aufbauen ---------

void Report19DailyCcaTableViewer::MakeLine(long lpAUrno, long lpDUrno, bool bpFirstCall /* =false */ )
{

	DAILYITEMS rlDailyItems( DAILYITEMS::DailyCca ) ;
	CString olCmpLine ;
	CString olBar("|") ;
	bool blLineSet = false ;
	long llUrno;
	// ToDO: evt. parametriebar machen
	bool blACT5 = true ;

	// unterschiedliche Frequenzen bei Season: siehe Report17DailyTableViwer.cpp !
	int ilFreqAnz = 1 ;

	CCSPtrArray<CCALLOCDATA> olCcAlloc ;	// Counterbelegungen pro Flug
	CTimeSpan olNullDauer(0,0,0,0) ;


	// Nur OutBound m�glich !
	if(lpAUrno || lpDUrno <= 0L )	// InBound
	{
		Beep( 500, 500 ) ;
		AfxMessageBox("Unexpected URNO in MakeLine()",MB_ICONWARNING ) ;
		return ;
	}

	rlDailyItems.GetEmptyFormatString( olCmpLine ) ;  // Preset: leere Zeile
	rlDailyItems.Folgezeile = 0 ;  

	ROTATIONDLGFLIGHTDATA *prlDf = omDailyCedaFlightData.GetFlightByUrno(lpDUrno);
	if(prlDf != NULL)
	{
		// CXX unerw�nscht
		if( strcmp( prlDf->Ftyp, "X" ) == 0 ) return ;

		CTime olStod = prlDf->Stod;
		if(bgReportLocal) ogBasicData.UtcToLocal(olStod);
		CString olStodFormat = olStod.Format("%H:%M");

/*		TRACE("\nFlugnummer [%d] [%s]", prlDf->Urno, prlDf->Flno ) ;
		if( strcmp("YP 2152  ",prlDf->Flno) == 0 )
		TRACE("Hier ist er !!\n") ;
*/				
		
		if( ! lpAUrno )		// nur OutBound Flug
		{
			// Alc2 oder Alc2/Alc3 ?
			rlDailyItems.Alc.Str = prlDf->Alc2 ;
			if( rlDailyItems.IsFagSpecial( prlDf->Alc2 ))
			{
				rlDailyItems.Alc.Str += CString("/")+prlDf->Alc3 ;
			}
			rlDailyItems.Alc.Str += olBar ;
			rlDailyItems.FltnOut.Str = prlDf->Fltn ;
			rlDailyItems.FltnOut.Str += ( strcmp( prlDf->Flns, " " ) != 0 ) ? prlDf->Flns + olBar : olBar ;  
			rlDailyItems.Act.Str = blACT5 ? prlDf->Act5 + olBar : prlDf->Act3+ olBar;

			rlDailyItems.Des.Str = prlDf->Des3 ;
			rlDailyItems.Des.Str += olBar ;

			rlDailyItems.Std.Str = olStodFormat+olBar ;
			
			if( atoi(prlDf->Vian) > 0 )	 
			{
				// return false: keine Vias trotz Vian > 0 !?
				if( ! GetVias( rlDailyItems.ViaOut.Str, prlDf ) ) 
				{
					AfxMessageBox("Indifferent Vialist !?"); 
				}

			}
			rlDailyItems.ViaOut.Str += olBar ;

			// Cca Werte besorgen 
			if(lpDUrno > 0)
			{
				// vor dem ersten Flug alle geblockten CIC aufnehmen
				if( bpFirstCall )
				{
					CCADATA rlCca ;
					int ilBlocks = omBlkData.GetSize() ;
					for( int index=0; index < ilBlocks; index++)
					{
						// CopyDiaCcaToCca( rlCca, omBlkData[index] ) ;	// nur Test
						CCALLOCDATA *prlCca = new CCALLOCDATA ;
						
						prlCca->Typ = true ;	// true => Blk oder Common
						prlCca->CTyp = ccmBlkTyp ;
						prlCca->Anz = 1 ;
						// prlCca->Disp = CString(GetString(IDS_STRING1486)) ;	// gesperrt
						prlCca->Disp = CString(omBlkData[index].Disp) ;	// Grund
						// prlCca->Disp += CString(omBlkData[index].Rema) ;	// Bemerkung
						prlCca->Cicf = omBlkData[index].Ckic ;
						prlCca->Cict = omBlkData[index].Ckic ;
						prlCca->Open = omBlkData[index].Ckbs ;
						prlCca->Close = omBlkData[index].Ckes ;
						// TRACE("blk [%s] bis [%s] = [%d] min\n", 
						//		prlCca->Open.Format("%d.%m.%y-%H:%M"),prlCca->Close.Format("%d.%m.%y-%H:%M"),prlCca->Min.GetTotalMinutes() ) ;  
						olCcAlloc.Add( prlCca ) ;
					}
				}
				
				int ilCcaSize = omCcaData.omData.GetSize() ;
				// bool blFirst = true ;
				for(int i = 0; i < ilCcaSize; i++)
				{
					if( ! strcmp(omCcaData.omData[i].Ctyp, "C") ) // &&  omCcaData.omData[i].Ckes <= prlDf->Stod )
					{
						// unbehandelter Common Check In !
						strcpy(omCcaData.omData[i].Ctyp, "O") ;	// OK
						CCALLOCDATA *prlCca = new CCALLOCDATA ;
						
						prlCca->Typ = true ;	// true => Blk oder Common
						prlCca->CTyp = ccmComTyp ;
						// Get Airline-Code
						CString olTmpStr;
						olTmpStr.Format("%ld", omCcaData.omData[i].Flnu);
						if (!ogBCD.GetField("ALT", "URNO", olTmpStr, "ALC2", prlCca->Alc2))
							prlCca->Alc2.Empty();
  						prlCca->Anz = 1 ;
						prlCca->Disp = omCcaData.omData[i].Disp ;
						prlCca->Cicf = omCcaData.omData[i].Ckic ;
						prlCca->Cict = omCcaData.omData[i].Ckic ;
						prlCca->Open = omCcaData.omData[i].Ckbs ;
						prlCca->Close = omCcaData.omData[i].Ckes ;
 						// TRACE("von [%s] bis [%s] = [%d] min\n", 
						//		prlCca->Open.Format("%d-%H:%M"),prlCca->Close.Format("%d-%H:%M"),prlCca->Min.GetTotalMinutes() ) ;  
						olCcAlloc.Add( prlCca ) ;
						
					}
					else if( lpDUrno == omCcaData.omData[i].Flnu )
					{
 						CCALLOCDATA *prlCca = new CCALLOCDATA ;
						prlCca->Anz = 1 ;
						prlCca->Cicf = omCcaData.omData[i].Ckic ;
						prlCca->Cict = omCcaData.omData[i].Ckic ;
						prlCca->Open = omCcaData.omData[i].Ckbs ;
						prlCca->Close = omCcaData.omData[i].Ckes ;
 						olCcAlloc.Add( prlCca ) ;
					}	// end if( lpDUrno == ....
				}	// end for( i ....

			}	// end if( lpDUrno > ... 
			
			
			blLineSet = true ;
		
		}
		else		// OutBound mit verkn�pftem InBound Flug 
		{
			// never 
		}

	} // if(prlDf != NULL)


	CompressCcaAlloc(olCcAlloc);  // Schalter pro Flug ggf. zusammenfassen

	llUrno = lpDUrno;  // Inhalt Keymap

	rlDailyItems.Folgezeile = 0 ;	// Summe der Folgezeilen 


	// f�r Saisondarstellung L�cken im Zeitraum als eigene Druckzeile ausgeben
	if( bmSeason )
	{
		ilFreqAnz = 1 ;		// akt. nur Daily
	}

	ilFreqAnz = olCcAlloc.GetSize() ;	// pro Belegungszeitraum eine Zeile

	for( int index=0; index < ilFreqAnz; index++ )
	{
		CString olBlank(" |") ;
		bool blCom = olCcAlloc[index].Typ ;
		char pslItoa[10] ;
		rlDailyItems.CAnz[0].Str = itoa( olCcAlloc[index].Anz, pslItoa, 10 ) ;
		rlDailyItems.CAnz[0].Str += olBar ;
		CString olOpen("   ") ;
		CString olClose(olOpen) ;

		if( olCcAlloc[index].Cicf != CString("")) olOpen = olCcAlloc[index].Cicf ;
		if( olCcAlloc[index].Cict != CString("")) olClose = olCcAlloc[index].Cict ;
		rlDailyItems.Ckift[0].Str = olOpen +"-"+ olClose ;
		rlDailyItems.Ckift[0].Str += olBar;
		olOpen = CString("    ") ;
		olClose = olOpen ;
		if(bgReportLocal)
		{
			ogBasicData.UtcToLocal(olCcAlloc[index].Open);
			ogBasicData.UtcToLocal(olCcAlloc[index].Close);
		}
		if(	olCcAlloc[index].Open != TIMENULL ) 
		{
			olOpen = olCcAlloc[index].Open.Format("%H:%M") ;
		}
		if(	olCcAlloc[index].Close != TIMENULL ) 
		{
			olClose = olCcAlloc[index].Close.Format("%H:%M") ;
		}
		rlDailyItems.CkiftTime[0].Str = olOpen + "-" + olClose ;

		if( olCcAlloc[index].Open != TIMENULL && olCcAlloc[index].Close != TIMENULL )
		{ 
//rkr25042001
/*			CTime olOpenTime(olCcAlloc[index].Open);
			CTime olCloseTime(olCcAlloc[index].Open.GetYear(), olCcAlloc[index].Open.GetMonth(), olCcAlloc[index].Open.GetDay(), 
							  olCcAlloc[index].Close.GetHour(), olCcAlloc[index].Close.GetMinute(), olCcAlloc[index].Close.GetSecond());
*/
			CTime olOpenTime(olCcAlloc[index].Open.GetYear(), olCcAlloc[index].Open.GetMonth(), olCcAlloc[index].Open.GetDay(), 
							  olCcAlloc[index].Open.GetHour(), olCcAlloc[index].Open.GetMinute(), olCcAlloc[index].Open.GetSecond());
			CTime olCloseTime(olCcAlloc[index].Close.GetYear(), olCcAlloc[index].Close.GetMonth(), olCcAlloc[index].Close.GetDay(), 
							  olCcAlloc[index].Close.GetHour(), olCcAlloc[index].Close.GetMinute(), olCcAlloc[index].Close.GetSecond());

//rkr25042001
			rlDailyItems.CMin[0].Str = itoa( labs((olCloseTime - olOpenTime).GetTotalMinutes()), pslItoa, 10 ) ;
		}
		else
		{
			rlDailyItems.CMin[0].Str = CString("0") ;
		}
		rlDailyItems.CMin[0].Str += olBar;


		if( blLineSet )		// neue Reportzeile gef�llt
		{
			if( blCom )
			{
				if( olCcAlloc[index].CTyp == ccmComTyp )
				{
					olCmpLine = olCcAlloc[index].Alc2+olBlank ;
					olCmpLine += olBlank ;
					olCmpLine += "COM"+olBlank ;
				}
				else if( olCcAlloc[index].CTyp == ccmBlkTyp )
				{
					olCmpLine = olBlank ;
					olCmpLine += olBlank ;
					olCmpLine += "BLK"+olBlank ;
				}
				else
				{
					olCmpLine = olBlank ;
					olCmpLine += olBlank ;
					olCmpLine += olBlank ;
				}
			}
			else
			{
				olCmpLine = rlDailyItems.Alc.Str;
				olCmpLine += rlDailyItems.FltnOut.Str;
				olCmpLine += rlDailyItems.Act.Str;
			}
 			olCmpLine += blCom ? olCcAlloc[index].Disp.Left(rlDailyItems.ViaOut.Size)+olBar : rlDailyItems.ViaOut.Str  ;
			olCmpLine += blCom ? olBlank : rlDailyItems.Des.Str  ;
			olCmpLine += blCom ? olBlank : rlDailyItems.Std.Str ;
			olCmpLine += rlDailyItems.CAnz[0].Str ;
			olCmpLine += rlDailyItems.Ckift[0].Str ;
			olCmpLine += rlDailyItems.CMin[0].Str ;
			olCmpLine += rlDailyItems.CkiftTime[0].Str ;
		}
		

		DAILYCCAURNOKEYLIST *prlUrno; 
		REPORT19TABLE_LINEDATA  *prlLineData = new REPORT19TABLE_LINEDATA;

		prlLineData->Urno = llUrno;
		if( blLineSet )
		{
			prlLineData->Common	=	blCom ;
 			prlLineData->FltnOut =	blCom ? olBlank : rlDailyItems.FltnOut.Str;
			// SUS Cic Typ anzeigen !
			// prlLineData->Act	=	blCom ? olBlank : rlDailyItems.Act.Str;
			if( blCom )
			{
				if( olCcAlloc[index].CTyp == ccmComTyp )
				{
					prlLineData->Alc = olCcAlloc[index].Alc2+olBlank;
					prlLineData->Act = "COM"+olBlank ;
				}
				else if( olCcAlloc[index].CTyp == ccmBlkTyp )
				{
					prlLineData->Alc = olBlank;
					prlLineData->Act = "BLK"+olBlank ;
				}
				else
				{
					prlLineData->Alc = olBlank;
					prlLineData->Act = olBlank ;
				}
			}
			else
			{
				prlLineData->Alc = rlDailyItems.Alc.Str ;
				prlLineData->Act = rlDailyItems.Act.Str;
			}
 			prlLineData->ViaOut	=	blCom ? olCcAlloc[index].Disp.Left(rlDailyItems.ViaOut.Size)+olBar : rlDailyItems.ViaOut.Str  ;
			prlLineData->Des =		blCom ? olBlank : rlDailyItems.Des.Str  ;
			if( blCom )
			{
				prlLineData->Std =	olCcAlloc[index].Close.Format("%Y%m%d%H%M%S") + olBar ;
				prlLineData->Typ =	olCcAlloc[index].CTyp ;
			}
			else
			{
				prlLineData->Std =	rlDailyItems.Std.Str ;
			}
			prlLineData->CAnz1 =	rlDailyItems.CAnz[0].Str ;
			prlLineData->Ckift1 =	rlDailyItems.Ckift[0].Str ;
			prlLineData->CMin1 =	rlDailyItems.CMin[0].Str;
			prlLineData->CkiftTime1 = rlDailyItems.CkiftTime[0].Str ;

			prlLineData->Folgezeile = rlDailyItems.Folgezeile ;
		}


		// Datensatz in die Keymaps einf�gen 
		if (olCmpLine.GetLength())
		{
			if(omLineKeyMap.Lookup(olCmpLine,(void *& )prlUrno) == TRUE)
			{
				prlUrno->Rotation.Add(prlLineData);
			}
			else
			{
				prlUrno = new DAILYCCAURNOKEYLIST;
				omLineKeyMap.SetAt(olCmpLine,prlUrno);
				prlUrno->Rotation.Add(prlLineData);
			}
		}
		else
		{
			delete prlLineData;
		}
	}	// end for( index= ....

	olCcAlloc.DeleteAll() ;
}



// Zusammenfassung von Checkin-Counter-Belegungen wenn der Typ, die Zeit der �ffnung und die
// Zeit der Schlie�ung �bereinstimmt und au�erdem die Schalternummern fortlaufend sind 
bool Report19DailyCcaTableViewer::CompressCcaAlloc(CCSPtrArray<CCALLOCDATA> &ropCcAlloc) const {
	
	for (int i=0; i < ropCcAlloc.GetSize(); i++)
	{
		int ilCicf = atoi(ropCcAlloc[i].Cicf);
		int ilCict = atoi(ropCcAlloc[i].Cict);

		for (int j=0; j < ropCcAlloc.GetSize(); j++)
		{
			// Stimmen Typ, �ffnungszeitpunkt und Schlie�ungszeitpunkt �berein?
			if( ropCcAlloc[i].CTyp == ropCcAlloc[j].CTyp && 
				ropCcAlloc[i].Open == ropCcAlloc[j].Open &&
				ropCcAlloc[i].Close == ropCcAlloc[j].Close)
			{
				// Sind Schalternummern fortlaufend?
				if ( atoi(ropCcAlloc[j].Cict) + 1 == ilCicf ||
					 atoi(ropCcAlloc[j].Cicf) - 1 == ilCict)
				{	
					// Anzahl der Schalter addieren
					ropCcAlloc[i].Anz += ropCcAlloc[j].Anz;
					if (atoi(ropCcAlloc[j].Cict) + 1 == ilCicf)
					{
						// �ffnungzeitpunkt anpassen
						ropCcAlloc[i].Cicf = ropCcAlloc[j].Cicf;
					}
					else
					{
						// Schlie�ungszeitpunkt anpassen
						ropCcAlloc[i].Cict = ropCcAlloc[j].Cict;
					}
					// Schalterbelegung l�schen
					ropCcAlloc.DeleteAt(j);
					// Wenn sich der Schalter vor dem untersuchten Schalter im Array befunden hat,
					//   Z�hler entsprechend anpassen
					if ( j < i ) i --;
					// Gleiche Schalterbelegung nochmals untersuchen
					i--;
					break;

				}
			}
		}
	}

	return true;
}


// Datensatz CedaDiaCcaData auf Datensatz CedaCcaData kopieren
// nur f�r Tests !
bool Report19DailyCcaTableViewer::CopyDiaCcaToCca( CCADATA& rrpCca, DIACCADATA rpDiaCca )
{
	bool blRet = true ;
/*
	if( &rpDiaCca != NULL )
	{
			TRACE("Blk [%s/%s] [%s/%s] from [%s] to [%s]\n",  
				rpDiaCca.Ckic, 
				rpDiaCca.Ctyp, 
				rpDiaCca.Disp, 
				rpDiaCca.Rema, 
				rpDiaCca.Ckbs.Format("%d.%m.%y %H:%M"), 
				rpDiaCca.Ckes.Format("%d.%m.%y %H:%M") ) ; 
	}
	else
	{
		blRet = false ;
	}
*/
	return( blRet ) ;
}


bool Report19DailyCcaTableViewer::GetVias(CString & popStr, ROTATIONDLGFLIGHTDATA *prpFlight )
{
	// Vialiste auswerten und in popStr zur�ckliefern
	
	int ilAnzVias = 0 ;
	CCSPtrArray<VIADATA> olViaListe ;
		
	if( (ilAnzVias = omDailyCedaFlightData.GetViaArray( &olViaListe, prpFlight )) < 1) return false ;
	
	for( int ilVia=0; ilVia < ilAnzVias; ilVia++ )
	{
		popStr += olViaListe[ilVia].Apc3 ;
		popStr += " " ;
	}

	return true ;
}


/////////////////////////////////////////////////////////////////////////////
// Report19DailyCcaTableViewer - display drawing routine


void Report19DailyCcaTableViewer::GetHeader()
{
 CString olCmpLine;
 char olHeadLine[126];
 DAILYITEMS olDailyItems( DAILYITEMS::DailyCca ) ;
 CString olFormatList ;


	if( bmSeason )
	{
		sprintf(olHeadLine, "Season ????") ; // akt. nur Daily
	}
	else
	{
		sprintf(olHeadLine,GetString(IDS_STRING1463));
	}
	pomTable->SetHeaderFields(olHeadLine);
	olDailyItems.GetDailyFormatList( olFormatList ) ;

    pomTable->SetFormatList( olFormatList ); 
}


//-----------------------------------------------------------------------------------------------
// UPDATEDISPLAY: Load data selected by filter conditions to the display by using "omTable"

void Report19DailyCcaTableViewer::UpdateDisplay()
{
	CString olCmpLine;
	DAILYCCAURNOKEYLIST *plUrno;  
	POSITION pos;
	int InOut = 0 ;	// hier nur eine OutBound Urno m�glich
	GetHeader();

	pomTable->ResetContent();


	for(pos = omLineKeyMap.GetStartPosition(); pos != NULL; )	// Schleife �ber alle Zeilen
	{
		omLineKeyMap.GetNextAssoc( pos, olCmpLine, (void *&)plUrno );
		REPORT19TABLE_LINEDATA *plUrno15Data = &plUrno->Rotation[InOut];
		
		// PtrArray omLines versorgen 
		int ilLineno = CreateLine( *plUrno15Data );
		// Bildschirmzeile:
		pomTable->InsertTextLine(ilLineno, olCmpLine, NULL);

	}	// end for( pos = ....


    pomTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

int Report19DailyCcaTableViewer::CreateLine( REPORT19TABLE_LINEDATA &rpLine )
{
    int ilLineCount = omLines.GetSize();

    // omLines r�ckw�rts durchlaufen bis Position gefunden
	for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
	{
		if (CompareCcaLine( rpLine, omLines[ilLineno-1] ) >= 0) break;  
	}

	// should be inserted after Lines[ilLineno-1]
    omLines.NewAt(ilLineno, rpLine);	// eine Urno je Ausgabezeile

	return ilLineno;
}



//  CCA Ausgabezeilen sortieren: rpLine Position finden
int Report19DailyCcaTableViewer::CompareCcaLine( REPORT19TABLE_LINEDATA &rpLine, REPORT19TABLE_LINEDATA &rpCmpLine )
{
	// Common nach vorne( 1. blocked / 2. Common ), STOD, FLNO, CKIC vergleichen
	
	int	ilCompareResult = 0;
	CString olFlno( rpLine.Alc+rpLine.FltnOut) ;
	CString olCmpFlno( rpCmpLine.Alc+rpCmpLine.FltnOut) ;
	int ilStd = atoi( rpLine.Std.Left(4) ) ; 
	int ilCmpStd = atoi( rpCmpLine.Std.Left(4) ) ; 

	if( rpLine.Common )
	{
		ilStd = atoi( rpLine.Std.Mid(8,4) ) ;
	}

	if( rpCmpLine.Common )
	{
		ilCmpStd = atoi( rpCmpLine.Std.Mid(8,4) ) ;
	}


	if ( rpLine.Typ != rpCmpLine.Typ )
	{
		if (rpLine.Typ == ccmBlkTyp) return -1;
		if (rpCmpLine.Typ == ccmBlkTyp) return 1;
		if (rpLine.Typ == ccmComTyp) return -1;
		if (rpCmpLine.Typ == ccmComTyp) return 1;
	}
 

	if( olFlno == olCmpFlno )
	{	
 		// nach Zeit ( bis! ) und CIC Nr.
		if( ilStd == ilCmpStd )
		{
			ilCompareResult = rpLine.Ckift1.Left(3) < rpCmpLine.Ckift1.Left(3) ? -1 : 1 ;
		}
		else
		{
			ilCompareResult = ilStd < ilCmpStd ? -1 : 1 ;  
		}
	}
	else
	{
 		if( ilStd == ilCmpStd ) 
		{
			ilCompareResult = olFlno < olCmpFlno ? -1 : 1;
		}
		else 
		{
			ilCompareResult = ilStd < ilCmpStd ? -1 : 1 ;
		}
	}

/*
	TRACE("[%d/%c] [%d/%c] ? [%s(%s)] == [%s(%s)] / [%d] == [%d] => [%d]\n", 
		rpLine.Common, rpLine.Typ, rpCmpLine.Common, rpCmpLine.Typ,
		olFlno, rpLine.Ckift1.Left(3), olCmpFlno, rpCmpLine.Ckift1.Left(3), ilStd, ilCmpStd, ilCompareResult ) ; 
*/

	return ilCompareResult;
}



//-----------------------------------------------------------------------------------------------

void Report19DailyCcaTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

void Report19DailyCcaTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);

	DAILYCCAURNOKEYLIST *prlUrno;  
	POSITION pos;
	CString olString;
	for( pos = omLineKeyMap.GetStartPosition(); pos != NULL; )
	{
		omLineKeyMap.GetNextAssoc( pos, olString, (void *&)prlUrno );
		// zuerst CCSPtrArray delete !
		while( prlUrno->Rotation.GetSize() > 0 )
		{
			prlUrno->Rotation.DeleteAt(0);
		}
		prlUrno->Rotation.RemoveAll();
		delete prlUrno;
	}

	omLineKeyMap.RemoveAll();

}


//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------
void Report19DailyCcaTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	char pclFooter[256];

/*rkr
	if(ogCustomer == "BVD")
	{
		sprintf(pclFooter, "%s   %s %s", GetString(IDS_STRINGFAG), GetString(IDS_STRING1224),
				(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));
	}
	else
	{
		sprintf(pclFooter, "%s %s", GetString(IDS_STRING1224),
				(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));
	}
*/
//		sprintf(pclFooter, "%s   %s %s", GetString(IDS_STRINGFAG), GetString(IDS_STRING1224),
//				(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));
	sprintf(pclFooter, omTableName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));
	omFooterName = pclFooter;

//	CString olTableName = GetString(IDS_STRING1461);
	CString olTableName = GetString(IDS_REPORTS_RB_CcaScheduleDaily);

	int ilOrientation = PRINT_PORTRAET;
	REPORT19TABLE_LINEDATA *prLeerzeile = new REPORT19TABLE_LINEDATA ;	// leere struct
	prLeerzeile->Folgezeile = imIndexLeer ;

	pomPrint = new CCSPrint((CWnd*)this,ilOrientation, 45);	// Def.: FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	if (pomPrint != NULL)
	{

		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			bool blFirstFlightCca = true ;

			pomPrint->imMaxLines = 57;	// Def. imMaxLines: 57 Portrait / 38 Landscape
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;	
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = olTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();  

//			olFooter1.Format("%s",omFooterName);
			olFooter1.Format("%s -   %s", omFooterName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );

			for(int i = 0; i < ilLines; i++ ) 
			{
				// Seitenvorschub nach COMMON
				if( ! omLines[i].Common && i == 0 ) blFirstFlightCca = false ;
				if( ! omLines[i].Common && blFirstFlightCca ) 
				{
					blFirstFlightCca = false ;
					PrintTableLine(prLeerzeile,true );
					pomPrint->imLineNo = pomPrint->imMaxLines + 1;	
				}

				if(pomPrint->imLineNo >= pomPrint->imMaxLines)	//imLineNo: PrintHeader => 0 / PrintLine => ++
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
					PrintTableLine(prLeerzeile,false );
				}
				
				if( (pomPrint->imLineNo + omLines[i].Folgezeile) > (pomPrint->imMaxLines-2) &&
					(omLines[i].Folgezeile + 1) < (pomPrint->imMaxLines-2) )
				{
					PrintTableLine(prLeerzeile,true );
					pomPrint->imLineNo = pomPrint->imMaxLines + 1;
					i-- ;
				}
				// Leerzeile beachten => -2 !
				else if(pomPrint->imLineNo == (pomPrint->imMaxLines-2) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],false );
					PrintTableLine(prLeerzeile,true );
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}

		omPrintHeadHeaderArray.DeleteAll();
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		delete pomPrint;
		pomPrint = NULL;
	}

	if( prLeerzeile != NULL ) delete prLeerzeile ;
}

//-----------------------------------------------------------------------------------------------

void Report19DailyCcaTableViewer::GetPrintHeader()
{
		
	DAILYITEMS rlDailyItems( DAILYITEMS::DailyCca ) ;
	const int ilAnzItems = rlDailyItems.GetNoOfItems() ;

	omPrintHeadHeaderArray.DeleteAll();  
	
	// Wieso + 1 !?
	TABLE_HEADER_COLUMN *prlHeader[DAILYITEMSCcaItems+1];

	// Preset
	for( int index = 0 ; index < ilAnzItems; index++ )
	{
		prlHeader[index] = new TABLE_HEADER_COLUMN;
		prlHeader[index]->Alignment = COLALIGN_CENTER;
		prlHeader[index]->Length = 35; 
		prlHeader[index]->Font = &ogCourier_Bold_10;
		prlHeader[index]->Text.Empty() ;
	}

	// ToDO: Length mit Faktor ( z.B.12.5 bei 12cpi ) �ber ITEMS Size steuern !
	prlHeader[0]->Length = 70; 
	prlHeader[0]->Text = GetString(IDS_STRING1078);	//CString("Flight");
	prlHeader[1]->Length = 55; 
	prlHeader[2]->Length = 55; 
	prlHeader[2]->Text = GetString(IDS_STRING337);	//CString("Type");
	prlHeader[3]->Length = 160; 
	prlHeader[3]->Text = GetString(IDS_STRING1468);	//CString("Routing");
	prlHeader[4]->Text = GetString(IDS_STRING1082);	//CString("To");
	prlHeader[5]->Length = 40; 
	prlHeader[5]->Text = GetString(IDS_STRING316);	//CString("STD");

	prlHeader[6]->Length = 20; 
	prlHeader[6]->Text = CString("C");	//CString("Anzahl Counter");
	prlHeader[7]->Length = 90; 
	prlHeader[7]->Text = GetString(IDS_STRING1472);	//CString("from-to");
	prlHeader[8]->Text = GetString(IDS_STRING1470);	//CString("Time");
	prlHeader[9]->Length = 110; 
	prlHeader[9]->Text = GetString(IDS_STRING1472) ;	//CString("from-to");


	for(int ili = 0; ili < ilAnzItems; ili++)
	{
		omPrintHeadHeaderArray.Add( prlHeader[ili]);
	}

}

//-----------------------------------------------------------------------------------------------

bool Report19DailyCcaTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;
	double dgCCSPrintFactor = 2.7 ;
/*	char pclHeader[100];

	sprintf(pclHeader, GetString(IDS_STRING1462), pcmSelect, pcmInfo);
	CString olTableName(pclHeader);
*/
	GetPrintHeader();

	pomPrint->imLeftOffset = 50 ;
//	pomPrint->PrintUIFHeader(omTableName,CString(pclHeader),pomPrint->imFirstLine-10);
	pomPrint->PrintUIFHeader("",omTableName,pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMETHIN;
//	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_12;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omPrintHeadHeaderArray.GetSize();

	for(int i=0;i<ilSize;i++)
	{
			rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omPrintHeadHeaderArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}


//-----------------------------------------------------------------------------------------------
// eine Tabellenzeile ausgeben

bool Report19DailyCcaTableViewer::PrintTableLine(REPORT19TABLE_LINEDATA *prpUrno, bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	double dgCCSPrintFactor = 2.7 ;
//	rlElement.pFont = &pomPrint->ogCourierNew_Regular_12;
	rlElement.pFont = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omPrintHeadHeaderArray.GetSize();
	DAILYITEMS rlDailyItems( DAILYITEMS::DailyCca ) ;

	// alle Tabellenzellen ausgeben = DAILYITMESItems
	for(int i=0;i<ilSize;i++)
	{
		rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength) rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Alignment  = PRINT_LEFT;
/*		
		if( bpLastLine )
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN;
		}
		else
		{
			rlElement.FrameBottom = PRINT_NOFRAME;
		}
*/		
		rlElement.FrameBottom = PRINT_FRAMETHIN;
//		rlElement.FrameLeft  = PRINT_NOFRAME;
//		rlElement.FrameRight = PRINT_NOFRAME;
		rlElement.FrameLeft  = PRINT_FRAMETHIN;
		rlElement.FrameRight = PRINT_FRAMETHIN;



		switch(i)
		{
			case DAILYITEMS::CALC:
			{
				SetPrintItem( rlElement, prpUrno->Alc, rlDailyItems.DoLeftFrame(DAILYITEMS::CALC) );

			}
			break;
			case DAILYITEMS::CFLTNOUT:
			{
				SetPrintItem( rlElement, prpUrno->FltnOut, rlDailyItems.DoLeftFrame(DAILYITEMS::CFLTNOUT));
			}
			break;
			case DAILYITEMS::CACT:
			{
				SetPrintItem( rlElement, prpUrno->Act, rlDailyItems.DoLeftFrame(DAILYITEMS::CACT));
			}
			break;
			case DAILYITEMS::CROUT:
			{
				SetPrintItem( rlElement, prpUrno->ViaOut, rlDailyItems.DoLeftFrame(DAILYITEMS::CROUT));
			}
			break;

			case DAILYITEMS::CDES:
			{
				SetPrintItem( rlElement, prpUrno->Des, rlDailyItems.DoLeftFrame(DAILYITEMS::CDES));
			}
			break;
			case DAILYITEMS::CSTD:
			{
				// STD Common nicht anzeigen
				if( prpUrno->Common )  
					SetPrintItem( rlElement, CString(" |"), rlDailyItems.DoLeftFrame(DAILYITEMS::CSTD));
				else
					SetPrintItem( rlElement, prpUrno->Std, rlDailyItems.DoLeftFrame(DAILYITEMS::CSTD));
			}
			break;
			case DAILYITEMS::CCNO1:
			{
				SetPrintItem( rlElement, prpUrno->CAnz1, rlDailyItems.DoLeftFrame(DAILYITEMS::CCNO1));
			}
			break;

			case DAILYITEMS::CCIFT1:
			{
				SetPrintItem( rlElement, prpUrno->Ckift1, rlDailyItems.DoLeftFrame(DAILYITEMS::CCIFT1));
			}
			break;
			case DAILYITEMS::CCKIMIN1:
			{
				SetPrintItem( rlElement, prpUrno->CMin1, rlDailyItems.DoLeftFrame(DAILYITEMS::CCKIMIN1));
			}
			break;
			case DAILYITEMS::CCKITIME1:
			{
				SetPrintItem( rlElement, prpUrno->CkiftTime1, rlDailyItems.DoLeftFrame(DAILYITEMS::CCKITIME1), true );
			}
			break;

			default :
				CString olMessage ;
				Beep(600, 600 ) ;
				olMessage.Format( GetString(IDS_STRING1460), i ) ;
				AfxMessageBox( olMessage ) ;
		

		}	// end switch( i ....


		if( _MIT_TRACE_ ) TRACE(" i [%d] : Length [%d]\n", i, rlElement.Length ) ;
		
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		
	} // end for( i = ... alle ITEMS 

	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll(); 
	return true;

}



// ein Druck (Zelle) Item aufbereiten
void Report19DailyCcaTableViewer::SetPrintItem(PRINTELEDATA& rrpItem, CString opItem, bool bpLF, bool bpRF)
{
	char olBar = '|' ;
	int ilRet ;

	// View Trenner eliminieren
	if( (ilRet = opItem.ReverseFind( olBar )) < 0 )
	{
		rrpItem.Text = opItem ;
	}
	else
	{
		CString olOhne = opItem ;
		while( ilRet >= 0 )
		{
			if( ilRet )	// rechts au�en
			{
				olOhne = olOhne.Left( ilRet ) ;
			}
			else	// gleich vorne
			{
				olOhne = olOhne.Right( olOhne.GetLength()-1 ) ;
			}
			ilRet = olOhne.ReverseFind( olBar ) ;
		}
		rrpItem.Text = olOhne ;
	}

	// R�nder ?
	if( bpLF ) rrpItem.FrameLeft = PRINT_FRAMETHIN ;
	if( bpRF ) rrpItem.FrameRight = PRINT_FRAMETHIN ;
}




// Drucken in Datei
bool Report19DailyCcaTableViewer::PrintPlanToFile(char *opTrenner)
{
	ofstream of;

	char* tmpvar;
	tmpvar = getenv("TMP");
	if (tmpvar == NULL)
		tmpvar = getenv("TEMP");

	if (tmpvar == NULL)
		return false;

	CString olFileName = omTableName;
	olFileName.Remove('*');
	olFileName.Remove('.');
	olFileName.Remove(':');
	olFileName.Remove('/');
	olFileName.Replace(" ", "_");
	
	char pHeader[256];
	strcpy (pHeader, tmpvar);
	CString path = pHeader;
	omFileName =  path + "\\" + olFileName + ".csv";

//	of.open( GetString(IDS_STRING1386), ios::out);
	of.open( omFileName, ios::out);

	int ilwidth = 1;

	of  << setw(ilwidth) << omTableName << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;

	of	<< setw(ilwidth) << GetString(IDS_STRING1078) 
		<< setw(1) << opTrenner
		<< setw(ilwidth) << " "
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING337) 
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING1468)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING1082)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING316)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << "C"
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING1472)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING1470)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING1472)
		<< endl;

	for(int i = 0; i < omLines.GetSize(); i++)
	{

		of.setf(ios::left, ios::adjustfield);
		if (omLines[i].Alc[omLines[i].Alc.GetLength()-1] == '|')
			of  << setw(ilwidth) << omLines[i].Alc.Left(omLines[i].Alc.GetLength()-1) << setw(1) << opTrenner;
		else
			of  << setw(ilwidth) << omLines[i].Alc << setw(1) << opTrenner;

		if (omLines[i].FltnOut[omLines[i].FltnOut.GetLength()-1] == '|')
			of  << setw(ilwidth) << omLines[i].FltnOut.Left(omLines[i].FltnOut.GetLength()-1) << setw(1) << opTrenner;
		else
			of  << setw(ilwidth) << omLines[i].FltnOut << setw(1) << opTrenner;

		if (omLines[i].Act[omLines[i].Act.GetLength()-1] == '|')
			of  << setw(ilwidth) << omLines[i].Act.Left(omLines[i].Act.GetLength()-1) << setw(1) << opTrenner;
		else
			of  << setw(ilwidth) << omLines[i].Act << setw(1) << opTrenner;

		if (omLines[i].Act.Left(3) != "COM")
		{
			if (omLines[i].ViaOut[omLines[i].ViaOut.GetLength()-1] == '|')
				of  << setw(ilwidth) << omLines[i].ViaOut.Left(omLines[i].ViaOut.GetLength()-1) << setw(1) << opTrenner;
			else
				of  << setw(ilwidth) << omLines[i].ViaOut << setw(1) << opTrenner;

			if (omLines[i].Des[omLines[i].Des.GetLength()-1] == '|')
				of  << setw(ilwidth) << omLines[i].Des.Left(omLines[i].Des.GetLength()-1) << setw(1) << opTrenner;
			else
				of  << setw(ilwidth) << omLines[i].Des << setw(1) << opTrenner;


			if (omLines[i].Std.GetLength() > 14)
				of  << setw(ilwidth) << " " << setw(1) << opTrenner;
			else
			{
				if (omLines[i].Std[omLines[i].Std.GetLength()-1] == '|')
					of  << setw(ilwidth) << omLines[i].Std.Left(omLines[i].Std.GetLength()-1) << setw(1) << opTrenner;
				else
					of  << setw(ilwidth) << omLines[i].Std << setw(1) << opTrenner;
			}
		}
		else
		{
			of << setw(ilwidth) << " " << setw(1) << opTrenner
			   << setw(ilwidth) << " " << setw(1) << opTrenner
			   << setw(ilwidth) << " " << setw(1) << opTrenner;
		}

		if (omLines[i].CAnz1[omLines[i].CAnz1.GetLength()-1] == '|')
			of  << setw(ilwidth) << omLines[i].CAnz1.Left(omLines[i].CAnz1.GetLength()-1) << setw(1) << opTrenner;
		else
			of  << setw(ilwidth) << omLines[i].CAnz1 << setw(1) << opTrenner;

		if (omLines[i].Ckift1[omLines[i].Ckift1.GetLength()-1] == '|')
			of  << setw(ilwidth) << omLines[i].Ckift1.Left(omLines[i].Ckift1.GetLength()-1) << setw(1) << opTrenner;
		else
			of  << setw(ilwidth) << omLines[i].Ckift1 << setw(1) << opTrenner;

		if (omLines[i].CMin1[omLines[i].CMin1.GetLength()-1] == '|')
			of  << setw(ilwidth) << omLines[i].CMin1.Left(omLines[i].CMin1.GetLength()-1) << setw(1) << opTrenner;
		else
			of  << setw(ilwidth) << omLines[i].CMin1 << setw(1) << opTrenner;

		if (omLines[i].CkiftTime1[omLines[i].CkiftTime1.GetLength()-1] == '|')
			of  << setw(ilwidth) << omLines[i].CkiftTime1.Left(omLines[i].CkiftTime1.GetLength()-1);
		else
			of  << setw(ilwidth) << omLines[i].CkiftTime1;

  		of  << endl;

	}
	of.close();
	return true;

/*
	char pclHeader[120];
 	ofstream of;
	of.open( pcpDefPath, ios::out);
	sprintf(pclHeader, GetString(IDS_STRING1462), pcmSelect, pcmInfo);
 	of << pclHeader << " generated at " 
		<< CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M") << endl << endl;

	of  << GetString(IDS_STRING1078) << "           "
		<< GetString(IDS_STRING337) << "   " 
		<< GetString(IDS_STRING1468)  << "           " 
		<< GetString(IDS_STRING1082)  << "  " 
		<< GetString(IDS_STRING316) << "  " 
		<< "C"  << "  " 
		<< GetString(IDS_STRING1472) << " " 
		<< GetString(IDS_STRING1470)  << " " 
		<< GetString(IDS_STRING1472) << "   " 
  		<< endl;
	of << "----------------------------------------------------------------------------" << endl;
		
	for(int i = 0; i < omLines.GetSize(); i++)
	{

		of.setf(ios::left, ios::adjustfield);
		if (omLines[i].Alc[omLines[i].Alc.GetLength()-1] == '|')
			of  << setw(7) << omLines[i].Alc.Left(omLines[i].Alc.GetLength()-1);
		else
			of  << setw(7) << omLines[i].Alc;

		if (omLines[i].FltnOut[omLines[i].FltnOut.GetLength()-1] == '|')
			of  << setw(10) << omLines[i].FltnOut.Left(omLines[i].FltnOut.GetLength()-1);
		else
			of  << setw(10) << omLines[i].FltnOut;

		if (omLines[i].Act[omLines[i].Act.GetLength()-1] == '|')
			of  << setw(7) << omLines[i].Act.Left(omLines[i].Act.GetLength()-1);
		else
			of  << setw(7) << omLines[i].Act;

		if (omLines[i].Act.Left(3) != "COM")
		{
			if (omLines[i].ViaOut[omLines[i].ViaOut.GetLength()-1] == '|')
				of  << setw(18) << omLines[i].ViaOut.Left(omLines[i].ViaOut.GetLength()-1);
			else
				of  << setw(18) << omLines[i].ViaOut;

			if (omLines[i].Des[omLines[i].Des.GetLength()-1] == '|')
				of  << setw(4) << omLines[i].Des.Left(omLines[i].Des.GetLength()-1);
			else
				of  << setw(4) << omLines[i].Des;

			if (omLines[i].Std[omLines[i].Std.GetLength()-1] == '|')
				of  << setw(5) << omLines[i].Std.Left(omLines[i].Std.GetLength()-1);
			else
				of  << setw(5) << omLines[i].Std;
		}
		else
			of << setw(18+4+5) << " ";
		if (omLines[i].CAnz1[omLines[i].CAnz1.GetLength()-1] == '|')
			of  << setw(3) << omLines[i].CAnz1.Left(omLines[i].CAnz1.GetLength()-1);
		else
			of  << setw(3) << omLines[i].CAnz1;

		if (omLines[i].Ckift1[omLines[i].Ckift1.GetLength()-1] == '|')
			of  << setw(8) << omLines[i].Ckift1.Left(omLines[i].Ckift1.GetLength()-1);
		else
			of  << setw(8) << omLines[i].Ckift1;

		if (omLines[i].CMin1[omLines[i].CMin1.GetLength()-1] == '|')
			of  << setw(5) << omLines[i].CMin1.Left(omLines[i].CMin1.GetLength()-1);
		else
			of  << setw(5) << omLines[i].CMin1;

		if (omLines[i].CkiftTime1[omLines[i].CkiftTime1.GetLength()-1] == '|')
			of  << setw(10) << omLines[i].CkiftTime1.Left(omLines[i].CkiftTime1.GetLength()-1);
		else
			of  << setw(10) << omLines[i].CkiftTime1;

  		of  << endl;

	}
	of.close();
*/ 
	return true;
}


//-----------------------------------------------------------------------------------------------

/////////////////////////////////////////////////////////////////////////////
// Report19DailyCcaTableViewer - end !