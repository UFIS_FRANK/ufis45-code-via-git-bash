// RotationTableViewer.cpp : implementation file
//

#include <stdafx.h>
#include <FPMS.h>
#include <CCSGlobl.h>
#include <RotationTableViewer.h>
#include <RotationTableChart.h>
#include <CCSTable.h>
#include <PrivList.h>
#include <resrc1.h>
#include <utils.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void RotationFlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);




/////////////////////////////////////////////////////////////////////////////
// RotationTableViewer


RotationTableViewer::RotationTableViewer()
{

	pomDefaultFont = &ogSetupFont;
	pomUserFont = NULL;
	pomFont = pomDefaultFont;

	bmAutoNow = false;

	ogDdx.Register(this, TABLE_FONT_CHANGED, CString("RotationTableViewer"), CString("Flight Update/new"), RotationFlightTableCf);
	ogDdx.Register(this, R_FLIGHT_CHANGE, CString("RotationTableViewer"), CString("Flight Update/new"), RotationFlightTableCf);
    ogDdx.Register(this, R_FLIGHT_DELETE, CString("RotationTableViewer"), CString("Flight Update/new"), RotationFlightTableCf);
    ogDdx.Register(this, DATA_RELOAD, CString("RotationTableViewer"), CString("Flight Update/new"), RotationFlightTableCf);
}

RotationTableViewer::~RotationTableViewer()
{
	ogDdx.UnRegister(this,NOTUSED);

	for(int i = omGroups.GetSize() - 1; i >= 0; i--)
	{
		GROUP_DATA *prlGroupData = &omGroups[i];
		
		//prlGroupData->Table->ResetContent();
		prlGroupData->Lines.DeleteAll();
	}
	
	omGroups.DeleteAll();

	if(pomUserFont != NULL)
	{
		pomUserFont->DeleteObject();
		delete pomUserFont;
	}
}


void RotationTableViewer::SetUserFont(LPLOGFONT lpLogFont)
{

	pomUserFont = new CFont;

	pomUserFont->CreateFontIndirect(lpLogFont); 

	pomFont = pomUserFont;



	DeleteAll();	
	MakeLines();
	ShowAllTables();
	
/*	
	for(int i = omGroups.GetSize() - 1; i >= 0; i--)
	{
		omGroups[i].Table->DisplayTable();
	}
*/

}


void RotationTableViewer::Repaint()
{
	DeleteAll();	
	MakeLines();
	ShowAllTables();
}






void RotationTableViewer::AddGroup(int ipGroupID,  CString opGroupText, CCSTable *popTable, RotationTableChart *popChart,  CCS3DStatic *pomTopScaleText)
{
	GROUP_DATA *prlGroupData = new GROUP_DATA;

	for(int i = omGroups.GetSize() - 1; i >= 0; i--)
	{
		if(omGroups[i].GroupID == ipGroupID)
			omGroups.DeleteAt(i);
	}
	prlGroupData->GroupID		= ipGroupID;
	prlGroupData->GroupText		= opGroupText;
	prlGroupData->Table			= popTable;
	prlGroupData->Chart			= popChart;
	prlGroupData->TopScaleText	= pomTopScaleText;
	/*
	TRACE("\n----------------------------------------------------------------------------------------------------------");
	TRACE("\nAddGroup: ID:<%d> <%s> Table:<%p> Chart:<%p>", ipGroupID, opGroupText, popTable, popChart );
	TRACE("\n----------------------------------------------------------------------------------------------------------");
	*/
	omGroups.Add(prlGroupData);
}



void RotationTableViewer::DeleteAll()
{
	for(int i = omGroups.GetSize() - 1; i >= 0; i--)
	{
		GROUP_DATA *prlGroupData = &omGroups[i];
		
		prlGroupData->Table->ResetContent();
		prlGroupData->Lines.DeleteAll();
	}
}



void RotationTableViewer::ChangeViewTo(const char *pcpViewName)
{

	if(strcmp(pcpViewName, "") != 0)
		SelectView(pcpViewName);

	CString olWhere;

	bool blRotation;

	GetFlightWhereString(olWhere, blRotation);
	SetTableSort();

	CTime olFrom;
	CTime olTo;
	CString olFtyps;


	if(strcmp(pcpViewName, "<Default>") == 0)
		olWhere = "";
	
	GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);
	ogRotationFlights.SetPreSelection(olFrom, olTo, olFtyps);

	ogRotationFlights.ReadAllFlights(olWhere, blRotation);


	DeleteAll();	
	MakeLines();
	ShowAllTables();

	for(int i = omGroups.GetSize() - 1; i >= 0; i--)
	{
		SetTopScaleText(omGroups[i].GroupID);
	}
}










void RotationTableViewer::SetTableSort()
{
	CStringArray olSpezSort;
	CStringArray olUniSort;

	omSort.RemoveAll();

	GetSort("FLIGHTSORT", olSpezSort);
	GetSort("UNISORT", olUniSort);


	//////////////////////////////////////////////////////
	// Sortarray  zusammenbasteln

	CString olTmp("|");

	for(int i = 0; i < olSpezSort.GetSize(); i ++)
	{

		if(olSpezSort[i] == "Flugnummer")
		{
			olTmp = olTmp + CString("FLNO") + CString("|") + CString("DFLNO") + CString("|");		
			omSort.Add("AFLNO+");
			omSort.Add("DFLNO+");
		}
		if(olSpezSort[i] == "A/C-Type")
		{
			olTmp = olTmp + CString("ACT5") + CString("|");
			omSort.Add("ACT5+");
		}
		if(olSpezSort[i] == "Origin")
		{
			olTmp = olTmp + CString("ORG3") + CString("|");
			omSort.Add("AORG3+");
		}
		if(olSpezSort[i] == "Destination")
		{
			olTmp = olTmp + CString("DES3") + CString("|");
			omSort.Add("DDES3+");
		}
		if(olSpezSort[i] == "Ankunftzeit")
		{
			olTmp = olTmp + CString("STOA") + CString("|");
			omSort.Add("ASTOA+");
		}
		if(olSpezSort[i] == "Abflugzeit")
		{
			olTmp = olTmp + CString("STOD") + CString("|");
			omSort.Add("DSTOD+");
		}
	}
	

	for( i = 0; i < olUniSort.GetSize(); i ++)
	{
		if(olTmp.Find(olUniSort[i].Left(5)) < 0)
		{
			omSort.Add(olUniSort[i]);
		}
	}

/*
	TRACE("\n---------------------");
	for( i = 0; i < omSort.GetSize(); i ++)
	{
		TRACE("\nSORT: <%d> Field<%s>  ", i, omSort[i]);
	}
	TRACE("\n---------------------");
*/
	//
	//////////////////////////////////////////////////////
}
















GROUP_DATA *RotationTableViewer::GetGroupData(int ipGroupID)
{
	for(int i = omGroups.GetSize() - 1; i >= 0; i--)
	{
		if(ipGroupID == omGroups[i].GroupID)
			return &omGroups[i];
	}
	return NULL;
}



void RotationTableViewer::ShowAllTables()
{
	for(int i = omGroups.GetSize() - 1; i >= 0; i--)
	{
		GROUP_DATA *prlGroupData = &omGroups[i];
		
		ShowTable(prlGroupData->GroupID);
	}
}


void RotationTableViewer::ShowTable(int ipGroupID)
{
	ShowTableHeader(ipGroupID);
	ShowTableLines(ipGroupID);
}



void RotationTableViewer::ShowTableHeader(int ipGroupID)
{
	GROUP_DATA *prlGroupData = GetGroupData(ipGroupID);

	if(prlGroupData == NULL)
		return;

	CCSTable *prlTable = prlGroupData->Table; 

	prlTable->ResetContent();
	
	
	CCSPtrArray <TABLE_HEADER_COLUMN> olHeaderDataArray;

	prlTable->SetShowSelection(true);
	prlTable->ResetContent();
	prlTable->SetDefaultSeparator();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Font = pomFont;

	if((ipGroupID == UD_ARRIVAL) || (ipGroupID == UD_AGROUND))
	{
		
		rlHeader.Length = 70;	
		rlHeader.AnzChar = 9;	
		rlHeader.String = "MWW88888X";	
		rlHeader.Text = GetString(IDS_STRING296);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 60;	
		rlHeader.AnzChar = 9;	
		rlHeader.Text = GetString(IDS_STRING297);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 35;	
		rlHeader.AnzChar = 4;	
		rlHeader.String = "MMWW";	
		rlHeader.Text = GetString(IDS_STRING298);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 35;	
		rlHeader.AnzChar = 4;	
		rlHeader.Text = GetString(IDS_STRING299);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 12;	
		rlHeader.AnzChar = 1;	
		rlHeader.String = "8";	
		rlHeader.Text = CString("");
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 13;	
		rlHeader.AnzChar = 1;	
		rlHeader.String = "V";	
		rlHeader.Text = GetString(IDS_STRING300);
		olHeaderDataArray.New(rlHeader);

		rlHeader.String = "88888";	
		rlHeader.Text = GetString(IDS_STRING301);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 40;	
		rlHeader.AnzChar = 5;	
		rlHeader.String = "88:88";	
		rlHeader.Text = GetString(IDS_STRING323);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 40;	
		rlHeader.String = "88:88";	
		rlHeader.Text = GetString(IDS_STRING302);
		olHeaderDataArray.New(rlHeader);

		// Shanghai want to see ETOA (ETA Fids)
		if (strcmp(pcgHome, "PVG") == 0)
		{
			rlHeader.Length = 40;	
			rlHeader.String = "88:88";	
			rlHeader.Text = GetString(IDS_STRING2012);
			olHeaderDataArray.New(rlHeader);
		}

		rlHeader.Length = 40;	
		rlHeader.String = "88:88";	
		rlHeader.Text = GetString(IDS_STRING303);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 40;	
		rlHeader.String = "88:88";	
		rlHeader.Text = GetString(IDS_STRING304);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 25;	
		rlHeader.AnzChar = 3;	
		rlHeader.String = "88W";	
		rlHeader.Text = GetString(IDS_STRING305);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 40;	
		rlHeader.AnzChar = 5;	
		rlHeader.String = "88:88";	
		rlHeader.Text = GetString(IDS_STRING306);
		olHeaderDataArray.New(rlHeader);
		
		rlHeader.Length = 40;		
		rlHeader.String = "88:88";	
		rlHeader.Text = GetString(IDS_STRING307);
		olHeaderDataArray.New(rlHeader);
		
		// Shanghai want to see  AIRB (ATD of Origin)
		if (strcmp(pcgHome, "PVG") == 0)
		{
			rlHeader.Length = 40;	
			rlHeader.String = "88:88";	
			rlHeader.Text = GetString(IDS_STRING321);
			olHeaderDataArray.New(rlHeader);
		}


		rlHeader.Length = 39;	
		rlHeader.AnzChar = 3;	
		rlHeader.String = "88W";	
		rlHeader.Text = GetString(IDS_STRING308);
		olHeaderDataArray.New(rlHeader);
		
		rlHeader.Length = 39;	
		rlHeader.AnzChar = 3;	
		rlHeader.String = "88W";	
		rlHeader.Text = GetString(IDS_STRING309);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 39;	
		rlHeader.AnzChar = 3;	
		rlHeader.String = "88/88";	
		rlHeader.Text = GetString(IDS_STRING1151);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 80;	 
		rlHeader.AnzChar = 9;	
		rlHeader.String = "MMMMMMMMM";	
		rlHeader.Text = GetString(IDS_STRING310);
		olHeaderDataArray.New(rlHeader);
		
		rlHeader.Length = 39;	
		rlHeader.AnzChar = 5;	
		rlHeader.String = "WWW88";	
		rlHeader.Text = GetString(IDS_STRING311);
		olHeaderDataArray.New(rlHeader);
		
		rlHeader.Length = 13;	 
		rlHeader.AnzChar = 1;	
		rlHeader.String = "+";	
		rlHeader.Text = GetString(IDS_STRING313);
		olHeaderDataArray.New(rlHeader);
		
		rlHeader.Length = 40;//102;	 
		rlHeader.AnzChar = 4;	
		rlHeader.String = "MMMM";	
		rlHeader.Text = GetString(IDS_STRING314);
		olHeaderDataArray.New(rlHeader);
	}

	if((ipGroupID == UD_DEPARTURE) || (ipGroupID == UD_DGROUND))
	{
		rlHeader.Length = 70; 
		rlHeader.AnzChar = 9;	
		rlHeader.String = "MWW88888X";	
		rlHeader.Text = GetString(IDS_STRING296);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 60; 
		rlHeader.AnzChar = 9;	
		rlHeader.Text = GetString(IDS_STRING297);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 35; 
		rlHeader.AnzChar = 4;	
		rlHeader.String = "MMWW";	
		rlHeader.Text = GetString(IDS_STRING315);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 35; 
		rlHeader.AnzChar = 4;	
		rlHeader.Text = GetString(IDS_STRING299);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 12;	
		rlHeader.AnzChar = 1;	
		rlHeader.String = "8";	
		rlHeader.Text = CString("");
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 13; 
		rlHeader.AnzChar = 1;	
		rlHeader.String = "V";	
		rlHeader.Text = GetString(IDS_STRING300);
		olHeaderDataArray.New(rlHeader);
		

		rlHeader.String = "88888";	
		rlHeader.Text = GetString(IDS_STRING301);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 37; 
		rlHeader.AnzChar = 5;	
		rlHeader.String = "88:88";	
		rlHeader.Text = GetString(IDS_STRING316);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 40; 
		rlHeader.Text = GetString(IDS_STRING317);
		olHeaderDataArray.New(rlHeader);

		// Shanghai want to see ETOD (ETD Fids)
		if (strcmp(pcgHome, "PVG") == 0)
		{
			rlHeader.Length = 40;	
			rlHeader.Text = GetString(IDS_STRING2013);
			olHeaderDataArray.New(rlHeader);
		}


		rlHeader.Length = 40; 
		rlHeader.Text = GetString(IDS_STRING318);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 40; 
		rlHeader.Text = GetString(IDS_STRING319);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 40; 
		rlHeader.Text = GetString(IDS_STRING320);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 25; 
		rlHeader.AnzChar = 3;	
		rlHeader.String = "88W";	
		rlHeader.Text = GetString(IDS_STRING305);
		olHeaderDataArray.New(rlHeader);
		
		rlHeader.Length = 40; 
		rlHeader.AnzChar = 5;	
		rlHeader.String = "88:88";	
		rlHeader.Text = GetString(IDS_STRING321);
		olHeaderDataArray.New(rlHeader);
		
		rlHeader.Length = 37; 
		rlHeader.AnzChar = 3;	
		rlHeader.String = "88W";	
		rlHeader.Text = GetString(IDS_STRING308);
		olHeaderDataArray.New(rlHeader);
		
		rlHeader.Length = 37; 
		rlHeader.AnzChar = 3;	
		rlHeader.String = "88W";	
		rlHeader.Text = GetString(IDS_STRING2014);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 37; 
		rlHeader.AnzChar = 3;	
		rlHeader.String = "88W";	
		rlHeader.Text = GetString(IDS_STRING2015);
		olHeaderDataArray.New(rlHeader);
		
		//rlHeader.Length = 37; 
		rlHeader.AnzChar = 7;	
		rlHeader.String = "000-000";	
		rlHeader.Text = GetString(IDS_STRING1508); // CKI
		olHeaderDataArray.New(rlHeader);


		rlHeader.Length = 80; 
		rlHeader.AnzChar = 9;	
		rlHeader.String = "MMMMMMMMM";	
		rlHeader.Text = GetString(IDS_STRING310);
		olHeaderDataArray.New(rlHeader);
		
		rlHeader.Length = 40;
		rlHeader.AnzChar = 5;	
		rlHeader.String = "WWW88";	
		rlHeader.Text = GetString(IDS_STRING311);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 13;
		rlHeader.AnzChar = 1;	
		rlHeader.String = "+";	
		rlHeader.Text = GetString(IDS_STRING313);
		olHeaderDataArray.New(rlHeader);

		rlHeader.Length = 40;
		rlHeader.AnzChar = 4;	
		rlHeader.String = "MMMM";	
		rlHeader.Text = GetString(IDS_STRING314);
		olHeaderDataArray.New(rlHeader);

	}
	prlTable->SetHeaderFields(olHeaderDataArray);
	olHeaderDataArray.DeleteAll();

}



void RotationTableViewer::ShowTableLines(int ipGroupID)
{

	GROUP_DATA *prlGroupData = GetGroupData(ipGroupID);

	if(prlGroupData == NULL)
		return;
    
	
	int ilCount = prlGroupData->Lines.GetSize();
	for (int ilLc = 0; ilLc < ilCount; ilLc++)
	{
		ShowTableLine(ipGroupID, ilLc, ADD_LINE, true);
	}

	//ShowTableLine(ipGroupID, popTable, 0, ADD_LINE);
 
	prlGroupData->Table->DisplayTable();
}

bool RotationTableViewer::GetFieldEditable(ROTATIONTABLE_LINEDATA* prpLine, CString& opStrFieldName)
{
	if(ogPrivList.GetStat (opStrFieldName) != '1')
		return false;	
	else
	{
		if (CheckPostFlight(prpLine))
			return false;	
		else
			return true;	
	}

	return false;
}


bool RotationTableViewer::ShowTableLine(int ipGroupID, int ipLineNo, int ipModus, bool bpReadAll)
{
	GROUP_DATA *prlGroupData = GetGroupData(ipGroupID);

	if(prlGroupData == NULL)
		return false;

	CCSTable *prlTable = prlGroupData->Table; 

		
	int ilTopIndex  = -1;

	if(!bpReadAll)
	{
		ilTopIndex = prlTable->pomListBox->GetTopIndex();
	}

	if((ipModus == CHANGE_LINE) && !((ipLineNo >= 0) && (ipLineNo < prlGroupData->Lines.GetSize())))
		return false;

	ROTATIONTABLE_LINEDATA *prlLine = 	&prlGroupData->Lines[ipLineNo]; 
	
	if(prlLine == NULL)
		return false;

	CString olTmp;

	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;

	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	//rlColumnData.Font = pomFont;

	rlColumnData.Font = pomFont;
	CString olSecId;

	CCSEDIT_ATTRIB rlAttrib;
	rlAttrib.Style = ES_UPPERCASE | ES_WANTRETURN;
	//rlAttrib.ExStyle = WS_EX_STATICEDGE | WS_EX_CLIENTEDGE;

	if((ipGroupID == UD_ARRIVAL) || (ipGroupID == UD_AGROUND))
	{
//		if((strcmp(prlLine->Ftyp, "X") == 0) || (strcmp(prlLine->Ftyp, "Z") == 0) || (strcmp(prlLine->Ftyp, "B") == 0))
		//textcolour for return taxt shall be black
		if((strcmp(prlLine->Ftyp, "X") == 0))// || (strcmp(prlLine->Ftyp, "B") == 0))
			rlColumnData.TextColor = RED;

		if(strcmp(prlLine->Ftyp, "N") == 0)
			rlColumnData.TextColor = ORANGE;


		rlColumnData.BkColor = WHITE;

//backcolour for return flights
		if( (strcmp(prlLine->Ftyp, "Z") == 0) )
			rlColumnData.BkColor = RED;

		if((prlLine->Tmoa != TIMENULL) && (prlLine->Land == TIMENULL))
			rlColumnData.BkColor = YELLOW;


		if( (prlLine->Ftyp == "T") || (prlLine->Ftyp == "G") )
		{

			if((prlLine->Onbl == TIMENULL) && (prlLine->Ofbl != TIMENULL))
//			if(prlLine->Ofbl != TIMENULL)
				rlColumnData.BkColor = LIME;
		}
		else
		{
			if((prlLine->Onbl == TIMENULL) && (prlLine->Land != TIMENULL))
				rlColumnData.BkColor = LIME;
		}

		// If requested, paint white lines of arrival flights with an !ATD! light blue (aqua)
		if (bgCreateDaily_OrgAtdBlue && CString("TGZB").Find(prlLine->Ftyp) == -1 
			&& rlColumnData.BkColor == WHITE && prlLine->Airb != TIMENULL && prlLine->Onbl == TIMENULL)
		{
			rlColumnData.BkColor = AQUA;
		}


		if(ipGroupID == UD_ARRIVAL)
			olSecId = CString("ROTATIONTAB_A_");
		else
			olSecId = CString("ROTATIONTAB_AG_");

		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("FLNO"));
/*
rkr		if(ogPrivList.GetStat( olSecId + CString("FLNO")) != '1')
			rlColumnData.blIsEditable = false;	
		else
			rlColumnData.blIsEditable = true;	
*/
		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXXXXXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 9;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
	
		if(ogPrivList.GetStat( olSecId + CString("FLNO")) != '-')
			olTmp = CString(prlLine->Flno);
		olTmp.TrimRight();
		rlColumnData.Text =	olTmp;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("CSGN"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	


		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXXXXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 8;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("CSGN")) != '-')
			rlColumnData.Text = CString(prlLine->Csgn);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.TextColor = BLACK;

		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("ORG4"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G") || (prlLine->Ftyp == "B") || (prlLine->Ftyp == "Z"))
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 4;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;

		if(ogPrivList.GetStat( olSecId + CString("ORG4")) != '-')
			rlColumnData.Text = CString(prlLine->Org4);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		
		/*
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("VIAL"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
		*/
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 4;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
		if((strcmp(prlLine->Ftyp, "Z") == 0) || (strcmp(prlLine->Ftyp, "B") == 0))
		{
			rlColumnData.blIsEditable = false;	
			rlColumnData.Text =  CString("");
		}
		else
		{
			if(ogPrivList.GetStat( olSecId + CString("VIAL")) != '-')
				rlColumnData.Text =  CString(prlLine->Vial);
		}
		olColList.NewAt(olColList.GetSize(), rlColumnData);


		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_CENTER;
		if((strcmp(prlLine->Ftyp, "Z") == 0) || (strcmp(prlLine->Ftyp, "B") == 0))
		{
			rlColumnData.Text =  CString("");
		}
		else
		{
			if(ogPrivList.GetStat( olSecId + CString("VIAL")) != '-')
				rlColumnData.Text =  CString(prlLine->Vian);
		}

		olColList.NewAt(olColList.GetSize(), rlColumnData);


		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("IFRA"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "'I'|'V'";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 1;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_CENTER;
		if(ogPrivList.GetStat( olSecId + CString("IFRA")) != '-')
			rlColumnData.Text = CString(prlLine->Ifra);         
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("TTYP"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXXX";
		rlAttrib.TextMinLenght = 2;
		rlAttrib.TextMaxLenght = 5;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_CENTER;
		if(ogPrivList.GetStat( olSecId + CString("TTYP")) != '-')
			rlColumnData.Text = CString(prlLine->Ttyp);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		
		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_CENTER;
		rlColumnData.Text = prlLine->Stoa.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("ETAI"));

		rlAttrib.Type = KT_TIME;
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 7;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_CENTER;
		if(ogPrivList.GetStat( olSecId + CString("ETAI")) != '-')
			rlColumnData.Text = prlLine->Etai.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);


		// Shanghai want to see ETOA (ETA Fids)
		if (strcmp(pcgHome, "PVG") == 0)
		{
			rlColumnData.blIsEditable = true;	
			if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
				rlColumnData.blIsEditable = false;	

			rlAttrib.Type = KT_TIME;
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 7;
			rlColumnData.EditAttrib = rlAttrib;
			rlColumnData.Alignment = COLALIGN_CENTER;
			rlColumnData.Text = prlLine->Etoa.Format("%H:%M");
			olColList.NewAt(olColList.GetSize(), rlColumnData);
		}


		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("TMOA"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_TIME;
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 7;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_CENTER;
		if(ogPrivList.GetStat( olSecId + CString("TMOA")) != '-')
			rlColumnData.Text = prlLine->Tmoa.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("LAND"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		

		if(prlLine->Ftyp == "B")
		{
			rlColumnData.Text = CString("");
		}
		else
		{
			if(ogPrivList.GetStat( olSecId + CString("LAND")) != '-')
			{
				if(prlLine->Ftyp == "T" || prlLine->Ftyp == "G")
				{
					if(prlLine->Ftyp == "T")
						rlColumnData.Text = prlLine->Ofbl.Format("T%H%M");
					else
						rlColumnData.Text = prlLine->Ofbl.Format("G%H%M");
				}
				else
				{
					rlColumnData.Text = prlLine->Land.Format("%H:%M");
				}
			}
		}


		rlAttrib.Type = KT_TIME;
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 7;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_CENTER;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("RWYA"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 4;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("RWYA")) != '-')
			rlColumnData.Text = CString(prlLine->Rwya);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_CENTER;
		rlColumnData.Text = prlLine->Onbe.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("ONBL"));

		rlAttrib.Type = KT_TIME;
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 7;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_CENTER;
		if(ogPrivList.GetStat( olSecId + CString("ONBL")) != '-')
			rlColumnData.Text = prlLine->Onbl.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		

		// Shanghai want to see AIRB (ATD of origin)
		if (strcmp(pcgHome, "PVG") == 0)
		{
			rlColumnData.blIsEditable = false;	

			rlAttrib.Type = KT_TIME;
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 7;
			rlColumnData.EditAttrib = rlAttrib;
			rlColumnData.Alignment = COLALIGN_CENTER;
			rlColumnData.Text = prlLine->Airb.Format("%H:%M");
			olColList.NewAt(olColList.GetSize(), rlColumnData);
		}

		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("PSTA"));
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 5;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("PSTA")) != '-')
			rlColumnData.Text = CString(prlLine->Psta);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("GTA1"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 5;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("GTA1")) != '-')
			rlColumnData.Text = CString(prlLine->Gta1);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 5;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("BLT1"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("BLT1")) != '-')
			rlColumnData.Text = CString(prlLine->Blt1);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("REGN"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXXXXXXXXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 12;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("REGN")) != '-')
			rlColumnData.Text = CString(prlLine->Regn);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("ACT5"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 5;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("ACT5")) != '-')
			rlColumnData.Text = CString(prlLine->Act5);
		olColList.NewAt(olColList.GetSize(), rlColumnData);


		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_CENTER;
		if(ogPrivList.GetStat( olSecId + CString("ISRE")) != '-')
			rlColumnData.Text = CString(prlLine->Isre);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("REMP")) != '-')
			rlColumnData.Text = CString(prlLine->Remp);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
	}

	if((ipGroupID == UD_DEPARTURE) || (ipGroupID == UD_DGROUND))
	{
//		if((strcmp(prlLine->Ftyp, "X") == 0) || (strcmp(prlLine->Ftyp, "Z") == 0) || (strcmp(prlLine->Ftyp, "B") == 0))
		//textcolour for return taxt shall be black
		if((strcmp(prlLine->Ftyp, "X") == 0))// || (strcmp(prlLine->Ftyp, "B") == 0))
			rlColumnData.TextColor = RED;

		if(strcmp(prlLine->Ftyp, "N") == 0)
			rlColumnData.TextColor = ORANGE;

		rlColumnData.BkColor = WHITE;


		if( (prlLine->Ftyp == "T") || (prlLine->Ftyp == "G") )
		{
		
//rkr20032001			if((prlLine->Ofbl != TIMENULL) && (prlLine->Onbl == TIMENULL))
//			if(prlLine->Ofbl != TIMENULL)
//				rlColumnData.BkColor = LIME;
		}
		else
		{
			if((prlLine->Ofbl != TIMENULL) && (prlLine->Airb == TIMENULL))
				rlColumnData.BkColor = LIME;
		}

//backcolour for return flights
		if( (strcmp(prlLine->Ftyp, "Z") == 0) )
			rlColumnData.BkColor = RED;

		if(ipGroupID == UD_DEPARTURE)
			olSecId = CString("ROTATIONTAB_D_");
		else
			olSecId = CString("ROTATIONTAB_DG_");

		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("FLNO"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXXXXXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 9;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("FLNO")) != '-')
			olTmp = CString(prlLine->Flno);
		olTmp.TrimRight();
		rlColumnData.Text =	olTmp;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("CSGN"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXXXXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 8;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("CSGN")) != '-')
			rlColumnData.Text = CString(prlLine->Csgn);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.TextColor = BLACK;

		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("DES4"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G") || (prlLine->Ftyp == "B") || (prlLine->Ftyp == "Z"))
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 4;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("DES4")) != '-')
			rlColumnData.Text = CString(prlLine->Des4);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		/*
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("VIAL"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
		*/
			rlColumnData.blIsEditable = false;	


		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 4;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_CENTER;
		if((strcmp(prlLine->Ftyp, "Z") == 0) || (strcmp(prlLine->Ftyp, "B") == 0))
		{
			rlColumnData.blIsEditable = false;	
			rlColumnData.Text =  CString("");
		}
		else
		{
			if(ogPrivList.GetStat( olSecId + CString("VIAL")) != '-')
				rlColumnData.Text =  CString(prlLine->Vial);
		}
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		

		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_CENTER;
		if((strcmp(prlLine->Ftyp, "Z") == 0) || (strcmp(prlLine->Ftyp, "B") == 0))
		{
			rlColumnData.Text =  CString("");
		}
		else
		{
			if(ogPrivList.GetStat( olSecId + CString("VIAL")) != '-')
				rlColumnData.Text =  CString(prlLine->Vian);
		}
		olColList.NewAt(olColList.GetSize(), rlColumnData);


		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("IFRD"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "'I'|'V'";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 1;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_CENTER;
		if(ogPrivList.GetStat( olSecId + CString("IFRD")) != '-')
			rlColumnData.Text =  CString(prlLine->Ifrd);         
		olColList.NewAt(olColList.GetSize(), rlColumnData);

	
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("TTYP"));
		
		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	

		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXXX";
		rlAttrib.TextMinLenght = 2;
		rlAttrib.TextMaxLenght = 5;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_CENTER;
		if(ogPrivList.GetStat( olSecId + CString("TTYP")) != '-')
			rlColumnData.Text = CString(prlLine->Ttyp);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_CENTER;
		rlColumnData.Text = prlLine->Stod.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("ETDI"));
		rlAttrib.Type = KT_TIME;
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 7;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_CENTER;
		if(ogPrivList.GetStat( olSecId + CString("ETDI")) != '-')
			rlColumnData.Text = prlLine->Etdi.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	

		// Shanghai want to see ETOD (ETD Fids)
		if (strcmp(pcgHome, "PVG") == 0)
		{
			rlColumnData.blIsEditable = true;	
			if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
				rlColumnData.blIsEditable = false;	

			rlAttrib.Type = KT_TIME;
			rlAttrib.TextMinLenght = 0;
			rlAttrib.TextMaxLenght = 7;
			rlColumnData.EditAttrib = rlAttrib;
			rlColumnData.Alignment = COLALIGN_CENTER;
			rlColumnData.Text = prlLine->Etod.Format("%H:%M");
			olColList.NewAt(olColList.GetSize(), rlColumnData);
		}

		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("ETDC"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
				
		rlAttrib.Type = KT_TIME;
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 7;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_CENTER;
		if(ogPrivList.GetStat( olSecId + CString("ETDC")) != '-')
			rlColumnData.Text = prlLine->Etdc.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("OFBL"));
		
		if((prlLine->Ftyp == "B") || (prlLine->Ftyp == "Z"))
			rlColumnData.blIsEditable = false;	
		
		
		rlAttrib.Type = KT_TIME;
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 7;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_CENTER;
		if(ogPrivList.GetStat( olSecId + CString("OFBL")) != '-')
			rlColumnData.Text = prlLine->Ofbl.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlAttrib.Type = KT_TIME;
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 7;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_CENTER;
		rlColumnData.Text = prlLine->Slot.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("RWYD"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 4;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("RWYD")) != '-')
			rlColumnData.Text = CString(prlLine->Rwyd);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("AIRB"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		
		if((prlLine->Ftyp == "B") || (prlLine->Ftyp == "Z"))
			rlColumnData.blIsEditable = false;	

		
		if(prlLine->Ftyp == "B")
		{
			rlColumnData.Text = CString("");
		}
		else
		{
			if(ogPrivList.GetStat( olSecId + CString("AIRB")) != '-')
				rlColumnData.Text = prlLine->Airb.Format("%H:%M");
		}

		rlAttrib.Type = KT_TIME;
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 7;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_CENTER;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("PSTD"));
		rlAttrib.Format = "XXXXX";
		rlAttrib.Type = KT_STRING;
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 5;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("PSTD")) != '-')
			rlColumnData.Text = CString(prlLine->Pstd);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("GTD1"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 5;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("GTD1")) != '-')
			rlColumnData.Text = CString(prlLine->Gtd1);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("GTD2"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 5;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("GTD2")) != '-')
			rlColumnData.Text = CString(prlLine->Gtd2);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.blIsEditable = false;	
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 7;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("CKI")) != '-')
			rlColumnData.Text = CString(prlLine->Cic);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("REGN"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXXXXXXXXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 12;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("REGN")) != '-')
			rlColumnData.Text = CString(prlLine->Regn);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.blIsEditable = GetFieldEditable(prlLine, olSecId + CString("ACT5"));

		if((prlLine->Ftyp == "T") || (prlLine->Ftyp == "G"))
			rlColumnData.blIsEditable = false;	
		
		rlAttrib.Type = KT_STRING;
		rlAttrib.Format = "XXXXX";
		rlAttrib.TextMinLenght = 0;
		rlAttrib.TextMaxLenght = 5;
		rlColumnData.EditAttrib = rlAttrib;
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("ACT5")) != '-')
			rlColumnData.Text = CString(prlLine->Act5);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_CENTER;
		if(ogPrivList.GetStat( olSecId + CString("ISRE")) != '-')
			rlColumnData.Text = CString(prlLine->Isre);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	
		rlColumnData.blIsEditable = false;	
		rlColumnData.Alignment = COLALIGN_LEFT;
		if(ogPrivList.GetStat( olSecId + CString("REMP")) != '-')
			rlColumnData.Text = CString(prlLine->Remp);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
	}	

	if(ipModus == ADD_LINE)
	{
		prlTable->AddTextLine(olColList, (void*)(prlLine));
	}
	else
	{
		if(ipModus == CHANGE_LINE)
		{
			prlTable->ChangeTextLine(ipLineNo, &olColList, (void*)(prlLine));
		}
		else
		{
			if(ipModus == INSERT_LINE)
			{
				prlTable->InsertTextLine(ipLineNo, olColList, (void *)prlLine);
			}
		}
	}
	olColList.DeleteAll();



	if(!bpReadAll)
	{
		if(ilTopIndex >= 0)
			prlTable->pomListBox->SetTopIndex(ilTopIndex);
	}

	
	if((!bpReadAll) && (prlGroupData->Lines.GetSize() == 1))
		prlTable->DisplayTable();
	
	return true;
}





void RotationTableViewer::MakeLine(ROTATIONFLIGHTDATA *prpFlight, ROTATIONTABLE_LINEDATA &rpLine)
{
	CString olStr;
	if(prpFlight != NULL)
	{
		rpLine.Tifa = prpFlight->Tifa;
		rpLine.Tifd = prpFlight->Tifd;
		rpLine.Urno = prpFlight->Urno;
		rpLine.Stoa = prpFlight->Stoa; 
		rpLine.Etai = prpFlight->Etai; 
		rpLine.Etoa = prpFlight->Etoa;
		rpLine.Tmoa = prpFlight->Tmoa; 
		rpLine.Land = prpFlight->Land; 
		rpLine.Onbe = prpFlight->Onbe; 
		rpLine.Onbl = prpFlight->Onbl; 
		rpLine.Stod = prpFlight->Stod; 
		rpLine.Etdi = prpFlight->Etdi;
		rpLine.Etod = prpFlight->Etod;		
		rpLine.Ofbl = prpFlight->Ofbl; 
		rpLine.Slot = prpFlight->Slot; 
		rpLine.Airb = prpFlight->Airb; 
		rpLine.Etdc = prpFlight->Etdc; 

		if (bgDailyLocal)
		{
			ogBasicData.UtcToLocal(rpLine.Tifa);
			ogBasicData.UtcToLocal(rpLine.Tifd);
			ogBasicData.UtcToLocal(rpLine.Stoa);
			ogBasicData.UtcToLocal(rpLine.Etai);
			ogBasicData.UtcToLocal(rpLine.Etoa);
			ogBasicData.UtcToLocal(rpLine.Tmoa);
			ogBasicData.UtcToLocal(rpLine.Land);
			ogBasicData.UtcToLocal(rpLine.Onbe);
			ogBasicData.UtcToLocal(rpLine.Onbl);
			ogBasicData.UtcToLocal(rpLine.Stod);
			ogBasicData.UtcToLocal(rpLine.Etdi);
			ogBasicData.UtcToLocal(rpLine.Etod);
			ogBasicData.UtcToLocal(rpLine.Ofbl);
			ogBasicData.UtcToLocal(rpLine.Slot);
			ogBasicData.UtcToLocal(rpLine.Airb);
			ogBasicData.UtcToLocal(rpLine.Etdc);
		}


		rpLine.Flno = CString(prpFlight->Flno);
		rpLine.Csgn = CString(prpFlight->Csgn); 
		if (bgAirport4LC)
			rpLine.Org4 = CString(prpFlight->Org4);
		else
			rpLine.Org4 = CString(prpFlight->Org3);
		rpLine.Ifra = CString(prpFlight->Ifra);
		rpLine.Ttyp = CString(prpFlight->Ttyp); 
		rpLine.Rwya = CString(prpFlight->Rwya);
		rpLine.Psta = CString(prpFlight->Psta);
		rpLine.Gta1 = CString(prpFlight->Gta1); 
		rpLine.Regn = CString(prpFlight->Regn);
		rpLine.Act5 = CString(prpFlight->Act5); 
		rpLine.Chgi = CString(prpFlight->Chgi);
		rpLine.Isre = CString(prpFlight->Isre); 
		rpLine.Remp = CString(prpFlight->Remp);
		if (bgAirport4LC)
			rpLine.Des4 = CString(prpFlight->Des4); 
		else
			rpLine.Des4 = CString(prpFlight->Des3); 
		rpLine.Ifrd = CString(prpFlight->Ifrd);
		rpLine.Rwyd = CString(prpFlight->Rwyd);
		rpLine.Pstd = CString(prpFlight->Pstd); 
		rpLine.Gtd1 = CString(prpFlight->Gtd1);
		rpLine.Gtd2 = CString(prpFlight->Gtd2); 
		rpLine.Ftyp = CString(prpFlight->Ftyp); 
		if (bgAirport4LC)		
			rpLine.Vial = CString(prpFlight->Via4); 
		else
			rpLine.Vial = CString(prpFlight->Via3); 
		rpLine.Vian = CString(prpFlight->Vian); 
		rpLine.Blt1 = CString(prpFlight->Blt1); 

		if(!CString(prpFlight->Blt2).IsEmpty())
		{
			rpLine.Blt1 += CString("/") + CString(prpFlight->Blt2);

		}


		rpLine.Cic.Empty();
		if (IsRegularFlight(prpFlight->Ftyp[0]))
		{
			CString opMin;
			CString opMax;

/*beg PRF 1976
			if (strcmp(pcgHome, "ATH") == 0)
			{
				// in Athens the flight set CKIF and CKIT automatically !!
				opMin = prpFlight->Ckif;
				opMax = prpFlight->Ckit;
			} 
			else
			{
				ogRotationFlights.omCcaData.GetMinMaxCkic(prpFlight->Urno, opMin, opMax);
			}
*/
			opMin = prpFlight->Ckif;
			opMax = prpFlight->Ckit;

//			if flight has information use this one, else look up cca-data
			if( opMax.IsEmpty() && opMin.IsEmpty() )
				ogRotationFlights.omCcaData.GetMinMaxCkic(prpFlight->Urno, opMin, opMax);
//end PRF 1976

			if(opMax.IsEmpty() || opMax == opMin)
			{
				rpLine.Cic = opMin;
			}
			else
			{
				rpLine.Cic = opMin + CString("-") + opMax;
			}
		}

		if((rpLine.Ftyp == "T") || (rpLine.Ftyp == "G"))
		{
			rpLine.Des4 = CString(""); 
			rpLine.Org4 = CString(""); 
		}

	}
    return;
}




void RotationTableViewer::MakeLines()
{
	ROTATIONFLIGHTDATA *prlFlight;
    ROTATIONTABLE_LINEDATA rlLine;
	int ilGroup1 = 0;
	int ilGroup2 = 0;

	for(int i = ogRotationFlights.omData.GetSize() - 1; i >= 0; i--)
	{
		prlFlight = &ogRotationFlights.omData[i];
		if(IsPassFilter(prlFlight))
		{
			MakeLine(prlFlight, rlLine);
			GetGroup(prlFlight, ilGroup1, ilGroup2);
			CreateLine(ilGroup1, rlLine);
			CreateLine(ilGroup2, rlLine);
		}
	}
}



void RotationTableViewer::DeleteLine(int ipGroupID, int ipLineNo)
{
	if(	ipGroupID == UD_FAIL)
		return;

	int ilTopIndex;

   	for(int i = omGroups.GetSize() - 1; i >= 0; i--)
	{
		if(omGroups[i].GroupID == ipGroupID)
		{
   			if((0 <= ipLineNo) && (ipLineNo < omGroups[i].Lines.GetSize()))
			{
				/*
				TRACE("\n----------------------------------------------------------------------------------------------------------");
				TRACE("\nDeleteLine: Urno:<%ld> Flno:<%s> ID:<%d> <%s> Table:<%p> Chart:<%p>", omGroups[i].Lines[ipLineNo].Urno, omGroups[i].Lines[ipLineNo].Flno, omGroups[i].GroupID, omGroups[i].GroupText, omGroups[i].Table, omGroups[i].Chart );
				TRACE("\n----------------------------------------------------------------------------------------------------------");
				*/

				ilTopIndex = omGroups[i].Table->pomListBox->GetTopIndex();
				omGroups[i].Lines.DeleteAt(ipLineNo);
				omGroups[i].Table->DeleteTextLine(ipLineNo);

				if(ilTopIndex >= 0)
					omGroups[i].Table->pomListBox->SetTopIndex(ilTopIndex);
			}
			break;
		}
	}
}




int RotationTableViewer::CreateLine(int ipGroupID, ROTATIONTABLE_LINEDATA &rpLine)
{
	if(	ipGroupID == UD_FAIL)
		return -1;

   	for(int i = omGroups.GetSize() - 1; i >= 0; i--)
	{
		if(omGroups[i].GroupID == ipGroupID)
		{
			int ilLineCount = omGroups[i].Lines.GetSize();

			for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
				if (CompareFlight(ipGroupID, &rpLine, &omGroups[i].Lines[ilLineno-1]) >= 0)
					break;  // should be inserted after Lines[ilLineno-1]

			omGroups[i].Lines.NewAt(ilLineno, rpLine);
			
			return ilLineno;
		}
	}
	return 0;
}






void RotationTableViewer::ChangeLine(int ipGroupID, ROTATIONTABLE_LINEDATA &rpLine, int ipLineNo)
{
	if(	ipGroupID == UD_FAIL)
		return;

   	for(int i = omGroups.GetSize() - 1; i >= 0; i--)
	{
		if(omGroups[i].GroupID == ipGroupID)
		{
			if(((ipLineNo >= 0) && (ipLineNo < omGroups[i].Lines.GetSize()))) 
			{
				/*
				TRACE("\n----------------------------------------------------------------------------------------------------------");
				TRACE("\nChangeLine: Urno:<%ld> Flno:<%s> ID:<%d> <%s> Table:<%p> Chart:<%p>", omGroups[i].Lines[ipLineNo].Urno, omGroups[i].Lines[ipLineNo].Flno, omGroups[i].GroupID, omGroups[i].GroupText, omGroups[i].Table, omGroups[i].Chart );
				TRACE("\n----------------------------------------------------------------------------------------------------------");
				*/
				omGroups[i].Lines.DeleteAt(ipLineNo);
				omGroups[i].Lines.NewAt(ipLineNo, rpLine);
			}
			break;
		}
	}
}




bool RotationTableViewer::IsPassFilter(ROTATIONFLIGHTDATA *prpFlight)
{
	return true;
}



void RotationTableViewer::MakeAndShowLine(ROTATIONFLIGHTDATA *prpFlight)
{
	if(prpFlight == NULL)
		return;
   	int ilGroupID1Now;
   	int ilGroupID2Now;
	int ilLineNo1Now;
	int ilLineNo2Now;

   	int ilGroupID1;
	int ilLineNo1;
   	int ilGroupID2;
	int ilLineNo2;
	int ilModus;

    ROTATIONTABLE_LINEDATA rlLine;

	MakeLine(prpFlight, rlLine);

	// the current group and line
	GetGroupAndLineByUrno(prpFlight, ilGroupID1Now, ilLineNo1Now, ilGroupID2Now, ilLineNo2Now);

	bool blTurnArround = GetGroup(prpFlight, ilGroupID1, ilGroupID2);

	
	if(((ilGroupID1Now != UD_FAIL) && (ilGroupID1 == UD_FAIL)) ||
	   ((ilGroupID2Now != UD_FAIL) && (ilGroupID2 == UD_FAIL)))
	{

		ProcessFlightDelete(prpFlight);
		ilGroupID1Now = UD_FAIL;
		ilGroupID1Now = UD_FAIL;

	}


	ilLineNo1 = GetLinePos(ilGroupID1, rlLine);

	if(	ilGroupID1Now != UD_FAIL)	
	{
			if((ilLineNo1Now == ilLineNo1) && (ilGroupID1 == ilGroupID1Now))
			{
				//TRACE("CHANGE LINE\n");
				ilModus = CHANGE_LINE;
				ChangeLine(ilGroupID1, rlLine,ilLineNo1);
				ShowTableLine(ilGroupID1, ilLineNo1, ilModus);
			}
			else
			{
				//TRACE("DELETE-INSERT LINE\n");
				DeleteLine(ilGroupID1Now, ilLineNo1Now);
				SetTopScaleText(ilGroupID1Now);
				ilModus = INSERT_LINE;
				ilLineNo1 = CreateLine(ilGroupID1, rlLine);
				ShowTableLine(ilGroupID1, ilLineNo1, ilModus);
			}
			SetTopScaleText(ilGroupID1);
	}
	else
	{
		ilModus = INSERT_LINE;
		ilLineNo1 = CreateLine(ilGroupID1, rlLine);
		ShowTableLine(ilGroupID1, ilLineNo1, ilModus);
		SetTopScaleText(ilGroupID1);
	}


	if(blTurnArround)	
	{
		ilLineNo2 = GetLinePos(ilGroupID2, rlLine);

		if(	ilGroupID2Now != UD_FAIL)	
		{
			if((ilLineNo2 == ilLineNo2Now) && (ilGroupID2 == ilGroupID2Now))
			{
				ilModus = CHANGE_LINE;
				ChangeLine(ilGroupID2, rlLine,ilLineNo2);
				ShowTableLine(ilGroupID2, ilLineNo2, ilModus);
			}
			else
			{
				DeleteLine(ilGroupID2Now, ilLineNo2Now);
				SetTopScaleText(ilGroupID2Now);
				ilModus = INSERT_LINE;
				ilLineNo2 = CreateLine(ilGroupID2, rlLine);
				ShowTableLine(ilGroupID2, ilLineNo2, ilModus);
			}
			SetTopScaleText(ilGroupID2);
		}
		else
		{
			ilModus = INSERT_LINE;
			ilLineNo2 = CreateLine(ilGroupID2, rlLine);
			ShowTableLine(ilGroupID2, ilLineNo2, ilModus);
			SetTopScaleText(ilGroupID2);
		}
	}
}



int RotationTableViewer::GetLinePos(int ipGroupID, ROTATIONTABLE_LINEDATA &rpLine)
{
	if(	ipGroupID == UD_FAIL)
		return -1;

   	for(int i = omGroups.GetSize() - 1; i >= 0; i--)
	{
		if(omGroups[i].GroupID == ipGroupID)
		{
			int ilLineCount = omGroups[i].Lines.GetSize();

			for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
				if (CompareFlight(ipGroupID, &rpLine, &omGroups[i].Lines[ilLineno-1]) > 0)
					break;  // should be inserted after Lines[ilLineno-1]
			/*
			if(ilLineno > 0)
			{
				if(omGroups[i].Lines[ilLineno-1].Urno == rpLine.Urno)
					ilLineno--;
			}

			if(ilLineno < (ilLineCount - 1))
			{
				if(omGroups[i].Lines[ilLineno + 1].Urno == rpLine.Urno)
					ilLineno++;
			}
			*/
			return ilLineno;
		}
	}
	return 0;
}




void RotationTableViewer::SetTopScaleText(int ipGroupID)
{
    ROTATIONTABLE_LINEDATA *prlLine;
	int ilLineNo;
	char pclTSText[256];

	for(int i = omGroups.GetSize() - 1; i >= 0; i--)
	{
		GROUP_DATA *prlGroup = &omGroups[i];
		if(ipGroupID == prlGroup->GroupID)
		{
			ilLineNo = GetNext(prlGroup->GroupID);

			if((ilLineNo >= 0) && (ilLineNo < prlGroup->Lines.GetSize()))
			{
				prlLine = &prlGroup->Lines[ilLineNo];
				if(ipGroupID == UD_DGROUND)
				{
					sprintf(pclTSText,"%9s  %12s  STD: %5s     OFB: %5s     POS: %5s     RWY: %5s",prlLine->Flno, prlLine->Regn, prlLine->Stod.Format("%H:%M"), prlLine->Ofbl.Format("%H:%M"), prlLine->Pstd , prlLine->Rwyd);
				}	
				if(ipGroupID == UD_DEPARTURE)
				{
					sprintf(pclTSText,"%9s  %12s  STD: %5s     ATD: %5s     POS: %5s     RWY: %5s",prlLine->Flno, prlLine->Regn, prlLine->Stod.Format("%H:%M"), prlLine->Airb.Format("%H:%M"), prlLine->Pstd  , prlLine->Rwyd);
				}	
				if(ipGroupID == UD_ARRIVAL)
				{
					sprintf(pclTSText,"%9s  %12s  STA: %5s     TMO: %5s     POS: %5s     RWY: %5s",prlLine->Flno, prlLine->Regn, prlLine->Stoa.Format("%H:%M"), prlLine->Tmoa.Format("%H:%M"), prlLine->Psta , prlLine->Rwya );
				}	
				if(ipGroupID == UD_AGROUND)
				{
					sprintf(pclTSText,"%9s  %12s  STA: %5s     ATA: %5s     POS: %5s     RWY: %5s",prlLine->Flno, prlLine->Regn, prlLine->Stoa.Format("%H:%M"), prlLine->Land.Format("%H:%M"), prlLine->Psta , prlLine->Rwya);
				}	
			}
			else
				strcpy(pclTSText,"");
				prlGroup->TopScaleText->SetWindowText(pclTSText);
				prlGroup->TopScaleText->InvalidateRect(NULL);
				prlGroup->TopScaleText->UpdateWindow();
		}
	}
}



int RotationTableViewer::GetNext(int ipGroupID)
{
/*
    ROTATIONTABLE_LINEDATA *prlLine;
	CTime olCurrTime = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olCurrTime);
	CTime olBestTime(2030,1,1,1,1,1,1);
	CTime olFutur(2030,1,1,1,1,1,1);
	CTime olTmpTime;
	int ilLineNo = -1;
*/
	GROUP_DATA *prlGroup ;

	for(int i = omGroups.GetSize() - 1; i >= 0; i--)
	{
		prlGroup = &omGroups[i];
		if(ipGroupID == prlGroup->GroupID)
			break;
	}



	if(ipGroupID == UD_ARRIVAL)
		return prlGroup->Lines.GetSize() - 1;


	if(ipGroupID == UD_DEPARTURE)
	{
		if(prlGroup->Lines.GetSize() == 0)
			return -1;
		else
			return 0;
	}


	if(ipGroupID == UD_DGROUND)
		return prlGroup->Lines.GetSize() - 1;



	if(ipGroupID == UD_AGROUND)
	{
		for(int j = prlGroup->Lines.GetSize() - 1; j >= 0; j--)
		{
			if(prlGroup->Lines[j].Onbl == TIMENULL)
				return j;

		}
	}

	return -1;


/*
	for(int i = omGroups.GetSize() - 1; i >= 0; i--)
	{
		GROUP_DATA *prlGroup = &omGroups[i];
		if(ilGroupID == prlGroup->GroupID)
		{
			for(int j = prlGroup->Lines.GetSize() - 1; j >= 0; j--)
			{
				prlLine = &prlGroup->Lines[j];

				if(ipGroupID == UD_ARRIVAL)
					olTmpTime = prlGroup->Lines[j].Stoa;

				if(ipGroupID == UD_AGROUND)
				{
					if(prlGroup->Lines[j].Onbl == TIMENULL)
						olTmpTime = prlGroup->Lines[j].Land;
					else
						olTmpTime = olFutur;
				}

				if(ipGroupID == UD_DGROUND)
				{
					if(prlGroup->Lines[j].Ofbl == TIMENULL)
						olTmpTime = prlGroup->Lines[j].Stod;
					else
						olTmpTime = prlGroup->Lines[j].Ofbl;
				}

				if(ipGroupID == UD_DEPARTURE)
				{
					if(prlGroup->Lines[j].Ofbl == TIMENULL)
						olTmpTime = olFutur;
					else
						olTmpTime = prlGroup->Lines[j].Ofbl;
				}


				if(olTmpTime < olBestTime)
				{
					olBestTime = olTmpTime;
					ilLineNo = j;		
				}
			}
		}
	}
	if(olBestTime == olFutur)
		return -1;
	else
		return ilLineNo;
*/
}



void RotationTableViewer::ShowNextLines()
{
	for(int l = omGroups.GetSize() - 1; l >= 0; l--)
	{
		GROUP_DATA *prlGroup = &omGroups[l];
		if (prlGroup->Table->IsInplaceEditMode)
			return;
	}

	int ilLineNo;

	for(int i = omGroups.GetSize() - 1; i >= 0; i--)
	{
		GROUP_DATA *prlGroup = &omGroups[i];

		ilLineNo = 0;
		ilLineNo = GetNext(prlGroup->GroupID);

		if((ilLineNo  == -1) && (prlGroup->GroupID == UD_AGROUND))
			ilLineNo  = 0;

		prlGroup->Table->EndInplaceEditing();
		
		if(prlGroup->GroupID == UD_AGROUND)
		{
			int ilFirst = prlGroup->Table->pomListBox->GetFirstVisibleItem();
			int ilLast = prlGroup->Table->pomListBox->GetLastVisibleItem();

			ilLineNo = ilLineNo - (ilLast - ilFirst) + 1;

			if(ilLineNo < 0)
				prlGroup->Table->pomListBox->SetTopIndex(0);
			else
				prlGroup->Table->pomListBox->SetTopIndex(ilLineNo);

		}
		else
		{
			prlGroup->Table->pomListBox->SetTopIndex(ilLineNo);
		}

	}

}


bool RotationTableViewer::ShowFlightLine(long lpUrno)
{
	if(lpUrno >= 0)
	{
		GROUP_DATA *prlGroupData;
 
		for(int i = omGroups.GetSize() - 1; i >= 0; i--)
		{
			prlGroupData = &omGroups[i];
			for(int j = prlGroupData->Lines.GetSize() - 1; j >= 0; j--)
			{
			
				if(prlGroupData->Lines[j].Urno == lpUrno)
				{
					prlGroupData->Table->EndInplaceEditing();
 					prlGroupData->Table->pomListBox->SetTopIndex(j);

 					return true;
				}
			}
		}
	}

	return false;
}




bool RotationTableViewer::GetGroup(ROTATIONFLIGHTDATA *prpFlight, int &ipGroup1, int &ipGroup2)
{
	ipGroup1 = UD_FAIL;
	ipGroup2 = UD_FAIL;
	bool blRet = false;

	if((strcmp(prpFlight->Des3,pcgHome) == 0) && (strcmp(prpFlight->Org3,pcgHome) == 0))
	{
		blRet = true;

//rkr20032001
/*		if((strcmp(prpFlight->Ftyp,"T") == 0) || (strcmp(prpFlight->Ftyp,"G") == 0))
		{
			ipGroup1 = UD_AGROUND;

			if(prpFlight->Ofbl != TIMENULL)
			{
				ipGroup2 = UD_DEPARTURE;
			}
			else
			{
				ipGroup2 = UD_DGROUND;
			}
		}
*/
//rkr20032001
		if((strcmp(prpFlight->Ftyp,"T") == 0) || (strcmp(prpFlight->Ftyp,"G") == 0))
		{
			if(prpFlight->Ofbl != TIMENULL)
			{
				ipGroup1 = UD_AGROUND;
//				ipGroup2 = UD_DGROUND;
				ipGroup2 = UD_DEPARTURE;
			}
			else
			{
				ipGroup1 = UD_FAIL;
				ipGroup2 = UD_DGROUND;
			}
		}
		else
		{

			if(prpFlight->Land == TIMENULL)	
				ipGroup1 = UD_ARRIVAL;
			else
				ipGroup1 = UD_AGROUND;


			if(prpFlight->Airb == TIMENULL)
				ipGroup2 = UD_DGROUND;
			else
				ipGroup2 = UD_DEPARTURE;
		}

	}
	else
	{
		if(strcmp(prpFlight->Des3,pcgHome) == 0)
		{
			if(prpFlight->Land == TIMENULL)	
				ipGroup1 = UD_ARRIVAL;
			else
				ipGroup1 = UD_AGROUND;
		}
		if(strcmp(prpFlight->Org3,pcgHome) == 0)
		{
			if(prpFlight->Airb == TIMENULL)	
				ipGroup1 = UD_DGROUND;
			else
				ipGroup1 = UD_DEPARTURE;
		}
	}
	return blRet;
}



bool RotationTableViewer::SelectFlightLine(long lpUrno)
{
	if(lpUrno >= 0)
	{
		GROUP_DATA *prlGroupData;
 
		for(int i = omGroups.GetSize() - 1; i >= 0; i--)
		{
			prlGroupData = &omGroups[i];
			for(int j = prlGroupData->Lines.GetSize() - 1; j >= 0; j--)
			{
			
				if(prlGroupData->Lines[j].Urno == lpUrno)
				{
					prlGroupData->Table->SelectLine(j, TRUE);
					return true;
				}
			}
		}
	}

	return false;
}



////////////////////////////////////////////////////////////////////////////
// DDX
//
static void RotationFlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    RotationTableViewer *polViewer = (RotationTableViewer *)popInstance;

    if (ipDDXType == R_FLIGHT_CHANGE)
        polViewer->ProcessFlightChange((ROTATIONFLIGHTDATA *)vpDataPointer);
	if (ipDDXType == R_FLIGHT_DELETE)
        polViewer->ProcessFlightDelete((ROTATIONFLIGHTDATA *)vpDataPointer);
	if (ipDDXType == DATA_RELOAD)
        polViewer->ChangeViewTo("");
	if (ipDDXType == TABLE_FONT_CHANGED)
        polViewer->Repaint();

	if(polViewer->bmAutoNow)
	{
	    if (ipDDXType == R_FLIGHT_CHANGE || ipDDXType == R_FLIGHT_DELETE || ipDDXType == DATA_RELOAD || ipDDXType == TABLE_FONT_CHANGED)
		{
			polViewer->ShowNextLines();
		}
	}

	
}



void RotationTableViewer::ProcessFlightChange(ROTATIONFLIGHTDATA *prpFlight)
{
	/*
	TRACE("\n----------------------------------------------------------------------------------------------------------");
	TRACE("\nDDX-Change: Urno:<%ld> Flno:<%s>", prpFlight->Urno, prpFlight->Flno);
	TRACE("\n----------------------------------------------------------------------------------------------------------");
	*/

	long llSelUrno = 0L;
	GROUP_DATA *prlGroupData;
	int ilLine;
	ROTATIONTABLE_LINEDATA *prlLine;

	for(int i = omGroups.GetSize() - 1; i >= 0; i--)
	{
		prlGroupData = &omGroups[i];
		
		ilLine = prlGroupData->Table->GetCurSel();
		if(ilLine >= 0)
		{
			prlLine = (ROTATIONTABLE_LINEDATA*) prlGroupData->Table->GetTextLineData(ilLine);

			if(prlLine != NULL)
				llSelUrno = prlLine->Urno;
			break;
		}
		
	}



	MakeAndShowLine(prpFlight);

	SelectFlightLine(llSelUrno);

}




void RotationTableViewer::ProcessFlightDelete(ROTATIONFLIGHTDATA *prpFlight)
{
   	int ilGroupID1;
	int ilLineNo1;
   	int ilGroupID2;
	int ilLineNo2;
	if(prpFlight == NULL)
		return;

	/*
	TRACE("\n----------------------------------------------------------------------------------------------------------");
	TRACE("\nDDX-Delete: Urno:<%ld> Flno:<%s>", prpFlight->Urno, prpFlight->Flno);
	TRACE("\n----------------------------------------------------------------------------------------------------------");
	*/
	if(GetGroupAndLineByUrno(prpFlight, ilGroupID1, ilLineNo1, ilGroupID2, ilLineNo2))
	{
		DeleteLine(ilGroupID1, ilLineNo1);
		DeleteLine(ilGroupID2, ilLineNo2);
	}
}


bool RotationTableViewer::GetGroupAndLineByUrno(ROTATIONFLIGHTDATA *prpFlight, int &ipGroupID1, int &ipLineNo1, int &ipGroupID2, int &ipLineNo2)
{
	//Holzhammermethode!!!

	ipGroupID1 = UD_FAIL;
	ipGroupID2 = UD_FAIL;
	ipLineNo1 = -1;
	ipLineNo2 = -1;
	bool blRet = false;

	long llUrno;
	long lpUrno = prpFlight->Urno;
	bool blTurnArround = false;

	bool blTreffer = false;

	if((strcmp(prpFlight->Des3,pcgHome) == 0) && (strcmp(prpFlight->Org3,pcgHome) == 0))
	{
		blTurnArround = true;
	}

	int ilGroupCount = omGroups.GetSize();

	for(int i = 0; i < ilGroupCount; i++)
	{
		GROUP_DATA *prlGroup = &omGroups[i];
		for(int j = prlGroup->Lines.GetSize() - 1; j >= 0; j--)
		{
			llUrno = prlGroup->Lines[j].Urno; 

			if( llUrno == lpUrno)
			{
				if(!blTreffer)
				{
					blTreffer = true;
					ipGroupID1 = prlGroup->GroupID;
					ipLineNo1 = j;
					blRet = true;
					break;
				}
				else
				{
					ipGroupID2 = prlGroup->GroupID;
					ipLineNo2 = j;
					blRet = true;
					break;
				}
			}
		}
	}

	return blRet;
}






















int RotationTableViewer::CompareFlight(int ipGroup, ROTATIONTABLE_LINEDATA *prpLine1, ROTATIONTABLE_LINEDATA *prpLine2)
{
	int	ilCompareResult = 0;
	int ilCount = omSort.GetSize();
	CTime olTime1;
	CTime olTime2;


	if(ipGroup == UD_ARRIVAL)
	{
		if(ilCount == 0)
		{

			if(	prpLine1->Tmoa != TIMENULL)
			{
				olTime1 = prpLine1->Tmoa;
			}
			else
			{
				if(	prpLine1->Etai != TIMENULL)
					olTime1 = prpLine1->Etai;
				else
					olTime1 = prpLine1->Stoa;

			}

			if(	prpLine2->Tmoa != TIMENULL)
			{
				olTime2 = prpLine2->Tmoa;
			}
			else
			{
				if(	prpLine2->Etai != TIMENULL)
					olTime2 = prpLine2->Etai;
				else
					olTime2 = prpLine2->Stoa;

			}

			if(olTime2 == olTime1)
			{
				ilCompareResult =  (prpLine1->Flno <  prpLine2->Flno)? 1: -1;
			}
			else
			{
				ilCompareResult =  (olTime1 < olTime2)? 1: -1;
			}


			if((prpLine1->Tmoa != TIMENULL) && (prpLine2->Tmoa == TIMENULL))
				ilCompareResult =  1;

			if((prpLine1->Tmoa == TIMENULL) && (prpLine2->Tmoa != TIMENULL))
				ilCompareResult =  -1;


			return ilCompareResult;
		}
		else
		{
			for (int i = 0; i < ilCount; i++)
			{
				if (strcmp(omSort[i], "STOA+") == 0)
					ilCompareResult = (prpLine1->Stoa == prpLine2->Stoa)? 0: (prpLine1->Stoa > prpLine2->Stoa)? 1: -1;
				else 
				if (strcmp(omSort[i], "STOA-") == 0)
					ilCompareResult = (prpLine1->Stoa == prpLine2->Stoa)? 0: (prpLine1->Stoa < prpLine2->Stoa)? 1: -1;
				else 
				if (strcmp(omSort[i], "FLNO+") == 0)
					ilCompareResult = (prpLine1->Flno == prpLine2->Flno)? 0: (prpLine1->Flno > prpLine2->Flno)? 1: -1;
				else 
				if (strcmp(omSort[i], "FLNO-") == 0)
					ilCompareResult = (prpLine1->Flno == prpLine2->Flno)? 0: (prpLine1->Flno < prpLine2->Flno)? 1: -1;
				else 
				if (strcmp(omSort[i], "ORG4+") == 0)
					ilCompareResult = (prpLine1->Org4 == prpLine2->Org4)? 0: (prpLine1->Org4 > prpLine2->Org4)? 1: -1;
				else 
				if (strcmp(omSort[i], "ORG4-") == 0)
					ilCompareResult = (prpLine1->Org4 == prpLine2->Org4)? 0: (prpLine1->Org4 < prpLine2->Org4)? 1: -1;
				else 
				if (strcmp(omSort[i], "ACT5+") == 0)
					ilCompareResult = (prpLine1->Act5 == prpLine2->Act5)? 0: (prpLine1->Act5 > prpLine2->Act5)? 1: -1;
				else 
				if (strcmp(omSort[i], "ACT5-") == 0)
					ilCompareResult = (prpLine1->Act5 == prpLine2->Act5)? 0: (prpLine1->Act5 < prpLine2->Act5)? 1: -1;
				else 
				if (strcmp(omSort[i], "REGN-") == 0)
					ilCompareResult = (prpLine1->Regn == prpLine2->Regn)? 0: (prpLine1->Regn < prpLine2->Regn)? 1: -1;
				else
				if (strcmp(omSort[i], "REGN+") == 0)
					ilCompareResult = (prpLine1->Regn == prpLine2->Regn)? 0: (prpLine1->Regn > prpLine2->Regn)? 1: -1;


				// Check the result of this sorting order, return if unequality is found
				if (ilCompareResult != 0)
					return ilCompareResult;

			}
			return 0;	// we can say that these two lines are equal
		}
	}

	if(ipGroup == UD_AGROUND)
	{
		if(ilCount == 0)
		{
			if((prpLine1->Onbl == TIMENULL) && (prpLine2->Onbl == TIMENULL))
			{

				CTime olTime1 = prpLine1->Land;
				CTime olTime2 = prpLine2->Land;

				if(prpLine1->Ftyp == "T" ||  prpLine1->Ftyp == "G")
					olTime1 = prpLine1->Ofbl;

				if(prpLine2->Ftyp == "T" ||  prpLine2->Ftyp == "G")
					olTime2 = prpLine2->Ofbl;


				if(olTime1 == olTime2)
					return (prpLine1->Flno <  prpLine2->Flno)? 1: -1;
				else
					return (olTime1 <  olTime2)? 1: -1;

				/*			
				if(prpLine1->Land == prpLine2->Land)
					return (prpLine1->Flno <  prpLine2->Flno)? 1: -1;
				else
					return (prpLine1->Land <  prpLine2->Land)? 1: -1;
				*/
			
			}

			if((prpLine1->Onbl != TIMENULL) && (prpLine2->Onbl != TIMENULL))
			{
				if(prpLine1->Onbl == prpLine2->Onbl)
					return (prpLine1->Flno <  prpLine2->Flno)? 1: -1;
				else
					return (prpLine1->Onbl <  prpLine2->Onbl)? 1: -1;
			}


			if((prpLine1->Onbl != TIMENULL) && (prpLine2->Onbl == TIMENULL))
				return 1;
			else
				return -1;
		}
		else
		{

			for (int i = 0; i < ilCount; i++)
			{
				if (strcmp(omSort[i], "STOA+") == 0)
					ilCompareResult = (prpLine1->Stoa == prpLine2->Stoa)? 0: (prpLine1->Stoa > prpLine2->Stoa)? 1: -1;
				else 
				if (strcmp(omSort[i], "STOA-") == 0)
					ilCompareResult = (prpLine1->Stoa == prpLine2->Stoa)? 0: (prpLine1->Stoa < prpLine2->Stoa)? 1: -1;
				else 
				if (strcmp(omSort[i], "FLNO+") == 0)
					ilCompareResult = (prpLine1->Flno == prpLine2->Flno)? 0: (prpLine1->Flno > prpLine2->Flno)? 1: -1;
				else 
				if (strcmp(omSort[i], "FLNO-") == 0)
					ilCompareResult = (prpLine1->Flno == prpLine2->Flno)? 0: (prpLine1->Flno < prpLine2->Flno)? 1: -1;
				else 
				if (strcmp(omSort[i], "ORG4+") == 0)
					ilCompareResult = (prpLine1->Org4 == prpLine2->Org4)? 0: (prpLine1->Org4 > prpLine2->Org4)? 1: -1;
				else 
				if (strcmp(omSort[i], "ORG4-") == 0)
					ilCompareResult = (prpLine1->Org4 == prpLine2->Org4)? 0: (prpLine1->Org4 < prpLine2->Org4)? 1: -1;
				else 
				if (strcmp(omSort[i], "ACT5+") == 0)
					ilCompareResult = (prpLine1->Act5 == prpLine2->Act5)? 0: (prpLine1->Act5 > prpLine2->Act5)? 1: -1;
				else 
				if (strcmp(omSort[i], "ACT5-") == 0)
					ilCompareResult = (prpLine1->Act5 == prpLine2->Act5)? 0: (prpLine1->Act5 < prpLine2->Act5)? 1: -1;
				else 
				if (strcmp(omSort[i], "REGN-") == 0)
					ilCompareResult = (prpLine1->Regn == prpLine2->Regn)? 0: (prpLine1->Regn < prpLine2->Regn)? 1: -1;
				else
				if (strcmp(omSort[i], "REGN+") == 0)
					ilCompareResult = (prpLine1->Regn == prpLine2->Regn)? 0: (prpLine1->Regn > prpLine2->Regn)? 1: -1;


				// Check the result of this sorting order, return if unequality is found
				if (ilCompareResult != 0)
					return ilCompareResult;

			}
			return 0;	// we can say that these two lines are equal
		}
	}

	if(ipGroup == UD_DGROUND)
	{
		if(ilCount == 0)
		{
			if((prpLine1->Ofbl == TIMENULL) && (prpLine2->Ofbl == TIMENULL))
			{
				if(prpLine1->Etdi == TIMENULL)
					olTime1 = prpLine1->Stod;
				else
					olTime1 = prpLine1->Etdi;


				if(prpLine2->Etdi == TIMENULL)
					olTime2 = prpLine2->Stod;
				else
					olTime2 = prpLine2->Etdi;


				if(olTime1 == olTime2)
					return (prpLine1->Flno <  prpLine2->Flno)? 1: -1;
				else
					return (olTime1 <  olTime2)? 1: -1;
			}


			if((prpLine1->Ofbl != TIMENULL) && (prpLine2->Ofbl != TIMENULL))
			{
				if(prpLine1->Ofbl == prpLine2->Ofbl)
					return (prpLine1->Flno <  prpLine2->Flno)? 1: -1;
				else
					return (prpLine1->Ofbl <  prpLine2->Ofbl)? 1: -1;
			}

			if((prpLine1->Ofbl != TIMENULL) && (prpLine2->Ofbl == TIMENULL))
				return 1;
			else
				return -1;
		}
		else
		{

			for (int i = 0; i < ilCount; i++)
			{
				if (strcmp(omSort[i], "STOD+") == 0)
					ilCompareResult = (prpLine1->Stod == prpLine2->Stod)? 0: (prpLine1->Stod > prpLine2->Stod)? 1: -1;
				else 
				if (strcmp(omSort[i], "STOD-") == 0)
					ilCompareResult = (prpLine1->Stod == prpLine2->Stod)? 0: (prpLine1->Stod < prpLine2->Stod)? 1: -1;
				else 
				if (strcmp(omSort[i], "FLNO+") == 0)
					ilCompareResult = (prpLine1->Flno == prpLine2->Flno)? 0: (prpLine1->Flno > prpLine2->Flno)? 1: -1;
				else 
				if (strcmp(omSort[i], "FLNO-") == 0)
					ilCompareResult = (prpLine1->Flno == prpLine2->Flno)? 0: (prpLine1->Flno < prpLine2->Flno)? 1: -1;
				else 
				if (strcmp(omSort[i], "DES4+") == 0)
					ilCompareResult = (prpLine1->Des4 == prpLine2->Des4)? 0: (prpLine1->Des4 > prpLine2->Des4)? 1: -1;
				else 
				if (strcmp(omSort[i], "DES4-") == 0)
					ilCompareResult = (prpLine1->Des4 == prpLine2->Des4)? 0: (prpLine1->Des4 < prpLine2->Des4)? 1: -1;
				else 
				if (strcmp(omSort[i], "ACT5+") == 0)
					ilCompareResult = (prpLine1->Act5 == prpLine2->Act5)? 0: (prpLine1->Act5 > prpLine2->Act5)? 1: -1;
				else 
				if (strcmp(omSort[i], "ACT5-") == 0)
					ilCompareResult = (prpLine1->Act5 == prpLine2->Act5)? 0: (prpLine1->Act5 < prpLine2->Act5)? 1: -1;
				else 
				if (strcmp(omSort[i], "REGN-") == 0)
					ilCompareResult = (prpLine1->Regn == prpLine2->Regn)? 0: (prpLine1->Regn < prpLine2->Regn)? 1: -1;
				else
				if (strcmp(omSort[i], "REGN+") == 0)
					ilCompareResult = (prpLine1->Regn == prpLine2->Regn)? 0: (prpLine1->Regn > prpLine2->Regn)? 1: -1;


				// Check the result of this sorting order, return if unequality is found
				if (ilCompareResult != 0)
					return ilCompareResult;

			}
			return 0;	// we can say that these two lines are equal
		}

	}

	if(ipGroup == UD_DEPARTURE)
	{
		if(ilCount == 0)
		{
			if(prpLine1->Airb == prpLine2->Airb)
				return (prpLine1->Flno <  prpLine2->Flno)? 1: -1;
			else
				return (prpLine1->Airb <  prpLine2->Airb)? 1: -1;
		}
		else
		{

			for (int i = 0; i < ilCount; i++)
			{
				if (strcmp(omSort[i], "STOD+") == 0)
					ilCompareResult = (prpLine1->Stod == prpLine2->Stod)? 0: (prpLine1->Stod > prpLine2->Stod)? 1: -1;
				else 
				if (strcmp(omSort[i], "STOD-") == 0)
					ilCompareResult = (prpLine1->Stod == prpLine2->Stod)? 0: (prpLine1->Stod < prpLine2->Stod)? 1: -1;
				else 
				if (strcmp(omSort[i], "FLNO+") == 0)
					ilCompareResult = (prpLine1->Flno == prpLine2->Flno)? 0: (prpLine1->Flno > prpLine2->Flno)? 1: -1;
				else 
				if (strcmp(omSort[i], "FLNO-") == 0)
					ilCompareResult = (prpLine1->Flno == prpLine2->Flno)? 0: (prpLine1->Flno < prpLine2->Flno)? 1: -1;
				else 
				if (strcmp(omSort[i], "DES4+") == 0)
					ilCompareResult = (prpLine1->Des4 == prpLine2->Des4)? 0: (prpLine1->Des4 > prpLine2->Des4)? 1: -1;
				else 
				if (strcmp(omSort[i], "DES4-") == 0)
					ilCompareResult = (prpLine1->Des4 == prpLine2->Des4)? 0: (prpLine1->Des4 < prpLine2->Des4)? 1: -1;
				else 
				if (strcmp(omSort[i], "ACT5+") == 0)
					ilCompareResult = (prpLine1->Act5 == prpLine2->Act5)? 0: (prpLine1->Act5 > prpLine2->Act5)? 1: -1;
				else 
				if (strcmp(omSort[i], "ACT5-") == 0)
					ilCompareResult = (prpLine1->Act5 == prpLine2->Act5)? 0: (prpLine1->Act5 < prpLine2->Act5)? 1: -1;
				else 
				if (strcmp(omSort[i], "REGN-") == 0)
					ilCompareResult = (prpLine1->Regn == prpLine2->Regn)? 0: (prpLine1->Regn < prpLine2->Regn)? 1: -1;
				else
				if (strcmp(omSort[i], "REGN+") == 0)
					ilCompareResult = (prpLine1->Regn == prpLine2->Regn)? 0: (prpLine1->Regn > prpLine2->Regn)? 1: -1;


				// Check the result of this sorting order, return if unequality is found
				if (ilCompareResult != 0)
					return ilCompareResult;

			}
			return 0;	// we can say that these two lines are equal
		}
	}
	return 0;
}





//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void RotationTableViewer::GetHeader(int ipGroupID)
{
	omPrintHeadHeaderArray.DeleteAll();
	

	TABLE_HEADER_COLUMN *prlHeader;

	if(ipGroupID == UD_ARRIVAL)
	{
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 66;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING296);
		omPrintHeadHeaderArray.Add( prlHeader );


		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 59;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING297);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 31;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING298);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 38;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING299);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 10;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING300);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 40;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING301);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING323);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING302);
		omPrintHeadHeaderArray.Add( prlHeader );


		// Shanghai want to see ETOA (ETA Fids)
		if (strcmp(pcgHome, "PVG") == 0)
		{
			prlHeader = new TABLE_HEADER_COLUMN;
			prlHeader->Alignment = COLALIGN_CENTER;
			prlHeader->Length = 36;	
			prlHeader->Font = pomFont;
			prlHeader->Text = GetString(IDS_STRING2012);
			omPrintHeadHeaderArray.Add( prlHeader );
		}


		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING303);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING304);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 24;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING305);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING306);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36;		
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING307);
		omPrintHeadHeaderArray.Add( prlHeader );

		
		// Shanghai want to see  AIRB (ATD of Origin)
		if (strcmp(pcgHome, "PVG") == 0)
		{
			prlHeader = new TABLE_HEADER_COLUMN;
			prlHeader->Alignment = COLALIGN_CENTER;
			prlHeader->Length = 36;	
			prlHeader->Font = pomFont;
			prlHeader->Text = GetString(IDS_STRING321);
			omPrintHeadHeaderArray.Add( prlHeader );
		}


		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 32;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING308);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 32;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING309);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 32;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING1151);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 87;	 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING310);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 38;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING311);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 65;	 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING324);
		omPrintHeadHeaderArray.Add( prlHeader );

		/*
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 25;		
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING312);
		omPrintHeadHeaderArray.Add( prlHeader );
		*/

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 10;	 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING313);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36;//102;	 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING314);
		omPrintHeadHeaderArray.Add( prlHeader );
	}


	if(ipGroupID == UD_AGROUND)
	{

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 66;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING296);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 59;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING297);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 31;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING298);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 38;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING299);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 10;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING300);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 40;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING301);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING323);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING302);
		omPrintHeadHeaderArray.Add( prlHeader );

		// Shanghai want to see ETOA (ETA Fids)
		if (strcmp(pcgHome, "PVG") == 0)
		{
			prlHeader = new TABLE_HEADER_COLUMN;
			prlHeader->Alignment = COLALIGN_CENTER;
			prlHeader->Length = 36;	
			prlHeader->Font = pomFont;
			prlHeader->Text = GetString(IDS_STRING2012);
			omPrintHeadHeaderArray.Add( prlHeader );
		}

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING303);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING304);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 24;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING305);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING306);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36;		
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING307);
		omPrintHeadHeaderArray.Add( prlHeader );

		
		// Shanghai want to see  AIRB (ATD of Origin)
		if (strcmp(pcgHome, "PVG") == 0)
		{
			prlHeader = new TABLE_HEADER_COLUMN;
			prlHeader->Alignment = COLALIGN_CENTER;
			prlHeader->Length = 36;	
			prlHeader->Font = pomFont;
			prlHeader->Text = GetString(IDS_STRING321);
			omPrintHeadHeaderArray.Add( prlHeader );
		}


		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 32;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING308);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 32;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING309);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 32;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING1151);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 87;	 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING310);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 38;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING311);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 65;	 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING1394);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 25;		
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING312);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 10;	 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING313);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36;//102;	 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING314);
		omPrintHeadHeaderArray.Add( prlHeader );

	}
	
	
	
	if(ipGroupID == UD_DGROUND)
	{
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 66; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING296);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 58; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING297);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 29; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING315);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING299);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 10; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING300);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 40;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING301);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING316);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING317);
		omPrintHeadHeaderArray.Add( prlHeader );

		// Shanghai want to see ETOD (ETD Fids)
		if (strcmp(pcgHome, "PVG") == 0)
		{
			prlHeader = new TABLE_HEADER_COLUMN;
			prlHeader->Alignment = COLALIGN_CENTER;
			prlHeader->Length = 36; 
			prlHeader->Font = pomFont;
			prlHeader->Text = GetString(IDS_STRING2013);
			omPrintHeadHeaderArray.Add( prlHeader );
		}


		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING318);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING319);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING320);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 24; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING305);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING321);
		omPrintHeadHeaderArray.Add( prlHeader );
	
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 32; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING308);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 32; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING2014);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 32; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING2015);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 87; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING310);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING311);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 65; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING1395);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 60; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING312);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 10; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING313);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		/*
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 72; 
		prlHeader->Font = pomFont;
		prlHeader->Text = CString("REM");
		omPrintHeadHeaderArray.Add( prlHeader );
*/
	}

	if(ipGroupID == UD_DEPARTURE)
	{
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 66; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING296);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 58; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING297);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 29; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING315);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING299);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 10; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING300);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 40;	
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING301);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING316);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING317);
		omPrintHeadHeaderArray.Add( prlHeader );

		// Shanghai want to see ETOD (ETD Fids)
		if (strcmp(pcgHome, "PVG") == 0)
		{
			prlHeader = new TABLE_HEADER_COLUMN;
			prlHeader->Alignment = COLALIGN_CENTER;
			prlHeader->Length = 36; 
			prlHeader->Font = pomFont;
			prlHeader->Text = GetString(IDS_STRING2013);
			omPrintHeadHeaderArray.Add( prlHeader );
		}

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING318);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING319);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING320);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 24; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING305);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING321);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 32; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING308);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 32; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING2014);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 32; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING2015);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 87; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING310);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 36; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING311);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 65; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING1396);
		omPrintHeadHeaderArray.Add( prlHeader );

		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 60; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING312);
		omPrintHeadHeaderArray.Add( prlHeader );
		
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 10; 
		prlHeader->Font = pomFont;
		prlHeader->Text = GetString(IDS_STRING313);
		omPrintHeadHeaderArray.Add( prlHeader );

		/*
		prlHeader = new TABLE_HEADER_COLUMN;
		prlHeader->Alignment = COLALIGN_CENTER;
		prlHeader->Length = 72; 
		prlHeader->Font = pomFont;
		prlHeader->Text = CString("REM");
		omPrintHeadHeaderArray.Add( prlHeader );
		*/
	}

}



void RotationTableViewer::PrintAllTableViews(CUIntArray& opPrintArray)  
{
 CString olFooter1,olFooter2;
 CString olTableName = "";
 int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
 int ilInitprinter = TRUE;

	int testPrint = 0;
	for (int k = opPrintArray.GetSize()-1; k >=0; k--)
		testPrint += opPrintArray.GetAt(k);

	if (testPrint == 0)
		return;


	ASSERT(omGroups.GetSize() == opPrintArray.GetSize());
	bool blInitPrinter = true;

 for(int ipGroupID = omGroups.GetSize()-1; ipGroupID >= 0; ipGroupID--)  // der letzte == Endekennung
 {
	switch(ipGroupID)
	{
		case 0:
			{
				olTableName  = GetString(IDS_STRING325); //"Tagesflugplan - Anflug";
			}
			break;
		case 1:
			{
				olTableName  = GetString(IDS_STRING326);//"Tagesflugplan - Boden - Anflug";
			}
			break;
		case 2:
			{
				olTableName  = GetString(IDS_STRING327);//"Tagesflugplan - Boden - Abflug";
			}
			break;
		case 3:
			{
				olTableName  = GetString(IDS_STRING328);//"Tagesflugplan - Abflug";
			}
			break;
	}

	if (opPrintArray.GetAt(ipGroupID) == 0)
		continue;

//	if (ipGroupID == omGroups.GetSize() - 1)	// beim ersten
	if (blInitPrinter)	// beim ersten
		pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)

	GROUP_DATA *prlGroupData = &omGroups[ipGroupID];
	//int ilRet = TRUE;

	if (pomPrint != NULL)
	{
//		if (ipGroupID == omGroups.GetSize() - 1)	// beim ersten
		if (blInitPrinter)	// beim ersten
			ilInitprinter = pomPrint->InitializePrinter(ilOrientation);

		if (ilInitprinter == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = olTableName;	

//			if (ipGroupID == omGroups.GetSize() - 1)	// beim ersten
			if (blInitPrinter)	// beim ersten
				pomPrint->omCdc.StartDoc( &rlDocInfo );
			blInitPrinter = false;

			pomPrint->imPageNo = 0;
			//pomPrint->imLineNo = 0;	// neu
			int ilLines = prlGroupData->Lines.GetSize();
			olFooter1.Format(GetString(IDS_STRING1445),ilLines,olTableName);

			if (ilLines == 0)
				PrintTableHeader(ipGroupID); // Druckt auch leere Tabelle
				
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
						pomPrint->imLineNo = 0;	// neu
					}
					
					PrintTableHeader(ipGroupID);
				
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&prlGroupData->Lines[i],true,ipGroupID);
					
				}
				else
				{
					PrintTableLine(&prlGroupData->Lines[i],false,ipGroupID);
					
				}
			}
			olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();	//--
			//--pomPrint->omCdc.EndDoc();	
		}
		omPrintHeadHeaderArray.DeleteAll();
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		//--delete pomPrint;
		//--pomPrint = NULL;
	}


 }

 if (ilInitprinter == TRUE)
 {
	pomPrint->omCdc.EndDoc();
 }
 delete pomPrint;
 pomPrint = NULL;

}

//-----------------------------------------------------------------------------------------------

bool RotationTableViewer::PrintTableHeader(int ipGroupID)
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;
	double dgCCSPrintFactor = 2.7;
	CString olTableName(GetString(IDS_STRING329)); //"Tagesflugplan"

	switch(ipGroupID)
	{
		case 0:
			{
				olTableName  = GetString(IDS_STRING325);//"Tagesflugplan - Anflug";
				dgCCSPrintFactor = 3.0;
			}
			break;
		case 1:
			{
				olTableName  = GetString(IDS_STRING326);//"Tagesflugplan - Boden - Anflug";
				dgCCSPrintFactor = 3.0;
			}
			break;
		case 2:
			{
				olTableName  = GetString(IDS_STRING327);//"Tagesflugplan - Boden - Abflug";
				dgCCSPrintFactor = 2.7;
			}
			break;
		case 3:
			{
				olTableName  = GetString(IDS_STRING328);//"Tagesflugplan - Abflug";
				dgCCSPrintFactor = 2.7;

			}
			break;
		default:
				olTableName  = GetString(IDS_STRING329);//"Tagesflugplan";
	}

	GetHeader(ipGroupID);
	//pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);
	pomPrint->PrintUIFHeader(olTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	//int ilSize = omHeaderDataArray.GetSize();
	int ilSize = omPrintHeadHeaderArray.GetSize();

	for(int i=0;i<ilSize;i++)
	{
			//rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			//rlElement.Text   = omHeaderDataArray[i].Text;
			rlElement.Text   = omPrintHeadHeaderArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

void RotationTableViewer::PrintTableLine(ROTATIONTABLE_LINEDATA *prpLine,bool bpLastLine,int ipGroupID)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	double dgCCSPrintFactor = 2.7;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	//int ilSize = omHeaderDataArray.GetSize();

	if((ipGroupID == UD_ARRIVAL) || (ipGroupID == UD_AGROUND))
		dgCCSPrintFactor = 3.0; 

	int ilSize = omPrintHeadHeaderArray.GetSize();
	for(int i = 0; i < ilSize; i++)
	{
		rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
		if (rlElement.Length < igCCSPrintMinLength)
			rlElement.Length+=igCCSPrintMoreLength;
		
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameTop   = PRINT_FRAMETHIN;
		if(bpLastLine == true)
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN;
		}
		else
		{
			rlElement.FrameBottom = PRINT_NOFRAME;
		}
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;


		if((ipGroupID == UD_ARRIVAL) || (ipGroupID == UD_AGROUND))
		{
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING296))
			{
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Flno;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING297))
			{
				rlElement.Text		= prpLine->Csgn;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING298))
			{
				rlElement.Text		= prpLine->Org4;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING299))
			{
				rlElement.Text		= prpLine->Vial;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING300))
			{
				rlElement.Text		= prpLine->Ifra;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING301))
			{
				//rlElement.Alignment = PRINT_RIGHT;
				rlElement.Text		= prpLine->Ttyp;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING323))
			{
				rlElement.Text		= prpLine->Stoa.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING302))
			{
				rlElement.Text		= prpLine->Etai.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING2012))
			{
				rlElement.Text		= prpLine->Etoa.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING303))
			{
				rlElement.Text		= prpLine->Tmoa.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING304))
			{
				rlElement.Text		= prpLine->Land.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING305))
			{
				rlElement.Text		= prpLine->Rwya;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING306))
			{
				rlElement.Text		= prpLine->Onbe.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING307))
			{
				rlElement.Text		= prpLine->Onbl.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING321))
			{
				rlElement.Text		= prpLine->Airb.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING308))
			{
				rlElement.Text		= prpLine->Psta;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING309))
			{
				rlElement.Text		= prpLine->Gta1;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING1151))
			{
				rlElement.Text		= prpLine->Blt1;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING310))
			{
				rlElement.Text		= prpLine->Regn;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING311))
			{
				rlElement.Text		= prpLine->Act5;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING324))
			{
				rlElement.Text		= prpLine->Stoa.Format("%d.%m.%y");
			}								
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING1394))
			{
				rlElement.Text		= prpLine->Land.Format("%d.%m.%y");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING313))
			{
				rlElement.Text		= prpLine->Isre;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING314))
			{
				rlElement.Text		= prpLine->Remp;
				rlElement.FrameRight = PRINT_FRAMETHIN;
			}

		}
		else 
		{
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING296))
			{
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Flno;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING297))
			{
				rlElement.Text		= prpLine->Csgn;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING315))
			{
				rlElement.Text		= prpLine->Des4;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING299))
			{
				rlElement.Text		= prpLine->Vial;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING300))
			{
				rlElement.Text		= prpLine->Ifrd;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING301))
			{
				rlElement.Text		= prpLine->Ttyp;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING316))
			{
				rlElement.Text		= prpLine->Stod.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING317))
			{
				rlElement.Text		= prpLine->Etdi.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING2013))
			{
				rlElement.Text		= prpLine->Etod.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING318))
			{
				rlElement.Text		= prpLine->Etdc.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING319))
			{
				rlElement.Text		= prpLine->Ofbl.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING320))
			{
				rlElement.Text		= prpLine->Slot.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING305))
			{
				rlElement.Text		= prpLine->Rwyd;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING321))
			{
				rlElement.Text		= prpLine->Airb.Format("%H:%M");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING308))
			{
				rlElement.Text		= prpLine->Pstd;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING2014))
			{
				rlElement.Text		= prpLine->Gtd1;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING2015))
			{
				rlElement.Text		= prpLine->Gtd2;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING310))
			{
				rlElement.Text		= prpLine->Regn;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING311))
			{
				rlElement.Text		= prpLine->Act5;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING1395))
			{
				rlElement.Text		= prpLine->Stod.Format("%d.%m.%y");
			}

			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING1396))
			{
					rlElement.Text		= prpLine->Airb.Format("%d.%m.%y");
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING312))
			{
				rlElement.Text		= prpLine->Isre;
			}
			if (omPrintHeadHeaderArray[i].Text == GetString(IDS_STRING313))
			{
				rlElement.Text		= prpLine->Remp;
				rlElement.FrameRight = PRINT_FRAMETHIN;
			}
		}


		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------
BOOL RotationTableViewer::CheckPostFlight(const ROTATIONTABLE_LINEDATA *prpTableLine)
{

	if (prpTableLine == NULL)
		return FALSE;

	ROTATIONFLIGHTDATA	*prlFlight = ogRotationFlights.GetFlightByUrno(prpTableLine->Urno);

	if (prlFlight == NULL)
		return FALSE;

	ROTATIONFLIGHTDATA *prlAFlight = ogRotationFlights.GetArrival(prlFlight);
	ROTATIONFLIGHTDATA *prlDFlight = ogRotationFlights.GetDeparture(prlFlight);

	return CheckPostFlight (prlAFlight, prlDFlight);

}


BOOL RotationTableViewer::CheckPostFlight(const ROTATIONFLIGHTDATA *prlAFlight, const ROTATIONFLIGHTDATA *prlDFlight)
{

	BOOL blPost = FALSE;

	if (prlAFlight && prlDFlight)
	{
		if (prlAFlight->Tifa != TIMENULL && prlDFlight->Tifd != TIMENULL)
		{
			if (IsPostFlight(prlAFlight->Tifa,false) && IsPostFlight(prlDFlight->Tifd,false))
				blPost = TRUE;
		}
	}
	else if (!prlAFlight && prlDFlight)
	{
		if (prlDFlight->Tifd != TIMENULL)
		{
			if (IsPostFlight(prlDFlight->Tifd,false))
				blPost = TRUE;
		}
	}
	else if (prlAFlight && !prlDFlight)
	{
		if (prlAFlight->Tifa != TIMENULL)
		{
			if (IsPostFlight(prlAFlight->Tifa,false))
				blPost = TRUE;
		}
	}


	return blPost;
}