/*

//{{AFX_INCLUDES()
//#include "progressbar.h"
//}}AFX_INCLUDES

*/

#ifndef AFX_PROGRESSDLG_H__4826E4C2_6242_11D1_80F9_0000B43C4B01__INCLUDED_
#define AFX_PROGRESSDLG_H__4826E4C2_6242_11D1_80F9_0000B43C4B01__INCLUDED_

// ProgressDlg.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CProgressDlg 

class CProgressDlg : public CDialog
{
// Konstruktion
public:
	CProgressDlg(CWnd* pParent = NULL);   // Standardkonstruktor
	//void SetPosition(int iPos);

// Dialogfelddaten
	//{{AFX_DATA(CProgressDlg)
	enum { IDD = IDD_PROGRESS };
	CEdit	m_CE_Progresstext;
	CProgressCtrl	m_CP_Progress;
	CString	m_Progresstext;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CProgressDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CProgressDlg)
		// HINWEIS: Der Klassen-Assistent fügt hier Member-Funktionen ein
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_PROGRESSDLG_H__4826E4C2_6242_11D1_80F9_0000B43C4B01__INCLUDED_
