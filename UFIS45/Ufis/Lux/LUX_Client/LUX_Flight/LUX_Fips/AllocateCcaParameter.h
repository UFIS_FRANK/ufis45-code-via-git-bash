#ifndef AFX_ALLOCATECCAPARAMETER_H__B45A3B11_626A_11D2_8E14_0000C002916B__INCLUDED_
#define AFX_ALLOCATECCAPARAMETER_H__B45A3B11_626A_11D2_8E14_0000C002916B__INCLUDED_

// AllocateCcaParameter.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld AllocateCcaParameter 

class AllocateCcaParameter : public CDialog
{
// Konstruktion
public:
	AllocateCcaParameter(CWnd* pParent = NULL);   // Standardkonstruktor

// Dialogfelddaten
	//{{AFX_DATA(AllocateCcaParameter)
	enum { IDD = IDD_CCAALLOCATE_PARAMETER };
		// HINWEIS: Der Klassen-Assistent fügt hier Datenelemente ein
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(AllocateCcaParameter)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(AllocateCcaParameter)
	afx_msg void OnReset();
	afx_msg void OnCancel();
	afx_msg void OnAll();
	afx_msg void OnNotallocated();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_ALLOCATECCAPARAMETER_H__B45A3B11_626A_11D2_8E14_0000C002916B__INCLUDED_
