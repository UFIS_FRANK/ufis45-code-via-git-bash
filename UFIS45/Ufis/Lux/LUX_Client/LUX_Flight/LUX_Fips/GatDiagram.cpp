// GatDiagram.cpp : implementation file
// Modification History:
// 19-feb-01	rkr	PRF 1305 Dubai: Gantchart-Anzeige unter local unterstützt
//

#include <stdafx.h>
#include <WoResTableDlg.h>
#include <GatDiagram.h>
#include <ButtonListDlg.h>
#include <GatDiaPropertySheet.h>
#include <DiaCedaFlightData.h>
#include <GatChart.h>
#include <GatGantt.h>
#include <TimePacket.h>
#include <RotationISFDlg.h>
#include <SpotAllocateDlg.h>
#include <FlightSearchTableDlg.h>
#include <PrivList.h>
#include <Utils.h>
#include <PosDiagram.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static void GatDiagramCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// GatDiagram
extern int igAnsichtPageIndex;

IMPLEMENT_DYNCREATE(GatDiagram, CFrameWnd)

GatDiagram::GatDiagram()
{
	igAnsichtPageIndex = -1;
	omMaxTrackSize = CPoint(4000/*1024*/, 1500);
	omMinTrackSize = CPoint(1024 / 4, 768 / 4);
	omViewer.SetViewerKey("POSDIA");
	omViewer.SelectView("<Default>");
    imStartTimeScalePos = 104;
    imStartTimeScalePos++;              // plus one for left border of chart
	lmBkColor = lgBkColor;
	pogGatDiagram = this;
	bmIsViewOpen = false;
	bgKlebefunktion = false;
	bmRepaintAll = false;
	pomChart = NULL;
}

GatDiagram::~GatDiagram()
{
 	pogGatDiagram = NULL;
}


BEGIN_MESSAGE_MAP(GatDiagram, CFrameWnd)
	//{{AFX_MSG_MAP(GatDiagram)
	ON_WM_CREATE()
	ON_BN_CLICKED(IDC_BEENDEN, OnBeenden)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_WORES, OnWoRes)
	ON_BN_CLICKED(IDC_ANSICHT, OnAnsicht)
	ON_BN_CLICKED(IDC_INSERT, OnInsert)
	ON_WM_CLOSE()
    ON_WM_GETMINMAXINFO()
    ON_WM_PAINT()
    ON_WM_ERASEBKGND()
    ON_WM_SIZE()
    ON_WM_HSCROLL()
    ON_WM_TIMER()
    ON_MESSAGE(WM_POSITIONCHILD, OnPositionChild)
    ON_MESSAGE(WM_UPDATEDIAGRAM, OnUpdateDiagram)
    ON_CBN_SELCHANGE(IDC_VIEW, OnViewSelChange)
	ON_CBN_CLOSEUP(IDC_VIEW, OnCloseupView)
	ON_BN_CLICKED(IDC_ZEIT, OnZeit)
	ON_BN_CLICKED(IDC_SEARCH, OnSearch)
	ON_BN_CLICKED(IDC_OFFLINE, OnOffline)
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	ON_MESSAGE(WM_REPAINT_ALL, RepaintAll)
	ON_BN_CLICKED(IDC_PRINT, OnPrint)
	ON_BN_CLICKED(IDC_ALLOCATE, OnAllocate)
	ON_WM_ACTIVATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


static void GatDiagramCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
	GatDiagram *polDiagram = (GatDiagram *)popInstance;

	TIMEPACKET *polTimePacket;

	switch (ipDDXType)
	{
	case REDISPLAY_ALL:
		polDiagram->RedisplayAll();
		break;
	case STAFFDIAGRAM_UPDATETIMEBAND:
		polTimePacket = (TIMEPACKET *)vpDataPointer;
		polDiagram->SetTimeBand(polTimePacket->StartTime, polTimePacket->EndTime);
		polDiagram->UpdateTimeBand();
		break;
	case SAS_VIEW_CHANGED:
        polDiagram->ViewSelChange( (char *) vpDataPointer);
		break;
	case SAS_GET_CUR_VIEW:
		polDiagram->GetCurView((char *) vpDataPointer);
		break;
	}
}

/////////////////////////////////////////////////////////////////////////////
// GatDiagram message handlers
void GatDiagram::PositionChild()
{
    CRect olRect;
    CRect olChartRect;
    
    int ilLastY = 0;
    omClientWnd.GetClientRect(&olRect);
         
    pomChart->GetClientRect(&olChartRect);
    olChartRect.right = olRect.right;

    olChartRect.top = ilLastY;

    ilLastY += pomChart->GetHeight();
    olChartRect.bottom = ilLastY;
        
    // check
    if ((pomChart->GetState() != Minimized) &&
		(olChartRect.top < olRect.bottom) && (olChartRect.bottom > olRect.bottom))
    {
        olChartRect.bottom = olRect.bottom;
        pomChart->SetState(Normal);
    }
    //
        
    pomChart->MoveWindow(&olChartRect, FALSE);
	pomChart->ShowWindow(SW_SHOW);

    
	omClientWnd.Invalidate(TRUE);
	//SetAllStaffAreaButtonsColor();
 	UpdateTimeBand();
}


void GatDiagram::SetTSStartTime(CTime opTSStartTime)
{
    omTSStartTime = opTSStartTime;
	/*
    if ((omTSStartTime < omStartTime) ||
		(omTSStartTime > omStartTime + omDuration))
    {
        omStartTime = CTime(omTSStartTime.GetYear(), omTSStartTime.GetMonth(), omTSStartTime.GetDay(),
            0, 0, 0) - CTimeSpan(1, 0, 0, 0);
    }
      */  


	CTime olTSStartTime(omTSStartTime);
	if(bgGatPosLocal)
		ogBasicData.UtcToLocal(olTSStartTime);

    char clBuf[8];
    sprintf(clBuf, "%02d%02d%02d",
        olTSStartTime.GetDay(), olTSStartTime.GetMonth(), olTSStartTime.GetYear() % 100);
    omTSDate.SetWindowText(clBuf);
    omTSDate.Invalidate(FALSE);
}

 

int GatDiagram::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

    SetTimer(0, (UINT) 60 * 1000, NULL);      // Update time line every minute!
    // SetTimer(1, (UINT) 60 * 1000 * ogPosDiaFlightData.cimBartimesUpdateMinute, NULL);  // Update bar times

	omDialogBar.Create( this, IDD_GATDIAGRAM, CBRS_TOP, IDD_GATDIAGRAM );
	SetWindowPos(&wndTop/*Most*/, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);

    omViewer.Attach(this);
	UpdateComboBox();


    CRect olRect; GetClientRect(&olRect);
	CTime olCurrentTime;
	olCurrentTime = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olCurrentTime);

	// Create 'Offline'-Button
	CButton *polButton = (CButton *)omDialogBar.GetDlgItem(IDC_OFFLINE2);	
 	WINDOWPLACEMENT olWndPlace;
	polButton->GetWindowPlacement( &olWndPlace ); 
	m_CB_Offline.Create(GetString(IDS_STRING1258), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW, olWndPlace.rcNormalPosition, &omDialogBar, IDC_OFFLINE);
    m_CB_Offline.SetFont(polButton->GetFont());
	m_CB_Offline.EnableWindow(true);
	//m_CB_Offline.ShowWindow(SW_HIDE);


//	m_KlebeFkt.Create( "&Klebefunktion", SS_CENTER | WS_CHILD | WS_VISIBLE, 
//						CRect(olRect.left + 400, 6,olRect.left + 480, 23), this, IDC_KLEBEFKT);

    omTSStartTime = olCurrentTime;// - CTimeSpan(0, 1, 0, 0);
    omStartTime = CTime(omTSStartTime.GetYear(), omTSStartTime.GetMonth(), omTSStartTime.GetDay(),
        0, 0, 0);// - CTimeSpan(1, 0, 0, 0);

    omDuration = CTimeSpan(3, 0, 0, 0);
    omTSDuration = CTimeSpan(0, 6, 0, 0);
    omTSInterval = CTimeSpan(0, 0, 10, 0);

	//Die wollen local
    CTime olUCT = CTime::GetCurrentTime();//olCurrentTime;
    CTime olRealUTC = olCurrentTime;
    char olBuf[16];
    sprintf(olBuf, "%02d%02d/%02d%02dz",
        olUCT.GetHour(), olUCT.GetMinute(),        
        olRealUTC.GetHour(), olRealUTC.GetMinute()
    );

    
	
	CTime olLocalTime = CTime::GetCurrentTime();
	CTime olUtcTime = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olUtcTime);

	char pclTimes[36];
	char pclDate[36];
	
	CTime olUtcTimeTmp = CTime::GetCurrentTime();
	olUtcTimeTmp -= ogUtcDiff;
	
	if(ogUtcDiff != 0)
	{
		sprintf(pclDate, "%02d.%02d.%02d z", olUtcTimeTmp.GetDay(), olUtcTimeTmp.GetMonth(), olUtcTimeTmp.GetYear() % 100);
		sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTimeTmp.GetHour(), olUtcTimeTmp.GetMinute());
	}
	else
	{
		sprintf(pclDate, "%02d.%02d.%02d z", olUtcTime.GetDay(), olUtcTime.GetMonth(), olUtcTime.GetYear() % 100);
		sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTime.GetHour(), olUtcTime.GetMinute());
	}
    
	
	omTime.Create(pclTimes, SS_CENTER | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 192, 3, olRect.right - 90, 22), this);

    omDate.Create(pclDate, SS_CENTER | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 88, 3, olRect.right - 8, 22), this);
        
    omDate.SetTextColor(RGB(255,0,0));
	
	

    sprintf(olBuf, "%02d%02d%02d",  omTSStartTime.GetDay(), omTSStartTime.GetMonth(), omTSStartTime.GetYear() % 100);
    int ilPos = (imStartTimeScalePos - olRect.left - 80) / 2;
    omTSDate.Create(olBuf, SS_CENTER | WS_CHILD | WS_VISIBLE,
        CRect(ilPos, 35, ilPos + 80, 52), this);

	omTimeScale.lmBkColor = lgBkColor;
    omTimeScale.Create(NULL, "TimeScale", WS_CHILD | WS_VISIBLE,
        CRect(imStartTimeScalePos, 28, olRect.right - (36 + 2), 62), this, 0, NULL);


//rkr
	//Set begin to local if local
	CTime olTSStartTime = omTSStartTime;
	if(bgGatPosLocal)
		ogBasicData.UtcToLocal(olTSStartTime);

//  omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
    omTimeScale.SetDisplayTimeFrame(olTSStartTime, omTSDuration, omTSInterval);

	if(bgGatPosLocal)
		omTimeScale.UpdateCurrentTimeLine(olLocalTime);
	else
		omTimeScale.UpdateCurrentTimeLine(olRealUTC);
    

    // must be static
    static UINT ilIndicators[] = { ID_SEPARATOR };
    omStatusBar.Create(this, CBRS_BOTTOM | WS_CHILD | WS_VISIBLE,0);
    omStatusBar.SetIndicators(ilIndicators, sizeof(ilIndicators) / sizeof(UINT));

    UINT nID, nStyle; int cxWidth;
    omStatusBar.GetPaneInfo(0, nID, nStyle, cxWidth);
    omStatusBar.SetPaneInfo(0, nID, SBPS_STRETCH | SBPS_NORMAL, cxWidth);

    olRect.top += 64;                   // for Dialog Bar & TimeScale
    olRect.bottom -= 20;                // for Horizontal Scroll Bar
    omClientWnd.Create(NULL, "ClientWnd", WS_CHILD /*| WS_HSCROLL */ | WS_VISIBLE, olRect, this,
        0 /* IDD_CLIENTWND */, NULL);
        
	// This will fix the bug for the horizontal scroll bar in the preplan mode
    long llTSMin = (omTSStartTime - omStartTime).GetTotalMinutes();
    long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
    SetScrollRange(SB_HORZ, 0, 1000, FALSE);
    SetScrollPos(SB_HORZ, (int) (1000 * llTSMin / llTotalMin), FALSE);

    CRect olPrevRect;
    omClientWnd.GetClientRect(&olRect);
            
    pomChart = new GatChart;
    pomChart->SetTimeScale(&omTimeScale);
    pomChart->SetViewer(&omViewer);
    pomChart->SetStatusBar(&omStatusBar);
    pomChart->SetStartTime(omTSStartTime);
    pomChart->SetInterval(omTimeScale.GetDisplayDuration());
    pomChart->Create(NULL, "GatChart", WS_OVERLAPPED | WS_BORDER | WS_CHILD | WS_VISIBLE,
		olRect, &omClientWnd, 0 /* IDD_CHART */, NULL);
        
    CButton *polCB;

	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_ALLOCATE);
	polCB->ShowWindow(SW_SHOW);

	SetpWndStatAll(ogPrivList.GetStat("GATDIAGRAMM_CB_Allocate"),polCB);


	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_PRINT);

	SetpWndStatAll(ogPrivList.GetStat("GATDIAGRAMM_CB_Print"),polCB);

	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_WORES);

	SetpWndStatAll(ogPrivList.GetStat("GATDIAGRAMM_CB_WORes"),polCB);


	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_ANSICHT);

	SetpWndStatAll(ogPrivList.GetStat("GATDIAGRAMM_CB_Ansicht"),polCB);

	if (!bgOffRelFuncGatpos)
	{
		m_CB_Offline.ShowWindow(SW_HIDE);
	}

	
	OnTimer(0);

	// Register DDX call back function
	//TRACE("GatDiagram: DDX Registration\n");
	ogDdx.Register(this, REDISPLAY_ALL, CString("STAFFDIAGRAM"),
		CString("Redisplay all from What-If"), GatDiagramCf);	// for what-if changes
	ogDdx.Register(this, STAFFDIAGRAM_UPDATETIMEBAND, CString("STAFFDIAGRAM"),
		CString("Update Time Band"), GatDiagramCf);	// for updating the yellow lines

  	ogDdx.Register(this, SAS_VIEW_CHANGED,CString(" "), CString("DATA_RELOAD"),GatDiagramCf);
 	ogDdx.Register(this, SAS_GET_CUR_VIEW,CString(" "), CString("DATA_RELOAD"),GatDiagramCf);

	
	// Connect to the GatPosFlightData-Class
	ogPosDiaFlightData.Connect();
	// Connect to the FlightsWithoutResource - Table
	pogWoResTableDlg->Connect();

	char pclView[100];
	strcpy(pclView, "<Default>");
	ogDdx.DataChanged((void *)this, SAS_GET_CUR_VIEW,(void *)pclView );
	omViewer.SelectView(pclView);

	CTime olFrom = TIMENULL;
	CTime olTo = TIMENULL;
	CString olFtyps;
	omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);
	if (olFrom != TIMENULL && olTo != TIMENULL)
	{
		omDuration = (olTo - olFrom) + CTimeSpan(1, 0, 0, 0);
		omStartTime = olFrom - CTimeSpan(1, 0, 0, 0);
	}
	ViewSelChange(pclView);
	

	// set caption
	CString olTimes;
	if (bgGatPosLocal)
		olTimes = GetString(IDS_STRING1921);
	else
		olTimes = GetString(IDS_STRING1920);
	CString olCaption = GetString(IDS_STRING1019);
	olCaption += " ("+olTimes+")";
	SetWindowText(olCaption);

	#ifdef _FIPSLIGHT
	{
		m_CB_Offline.ShowWindow(SW_HIDE);
	    CButton *polCB= (CButton *) omDialogBar.GetDlgItem(IDC_ALLOCATE);
		if (polCB)
			polCB->ShowWindow(SW_HIDE);
	}
	#else
	{
	}
	#endif

	 return 0;

}



void GatDiagram::OnBeenden() 
{
 	DestroyWindow();	
}


void GatDiagram::OnInsert()
{
	RotationISFDlg dlg;
	dlg.DoModal();
}


void GatDiagram::OnSearch()
{
	pogFlightSearchTableDlg->Activate(omViewer.GetBaseViewName(), omViewer.GetViewName(), GATDIA);
}


void GatDiagram::OnOffline()
{	
	ogPosDiaFlightData.ToggleOffline();
}


void GatDiagram::OnAnsicht()
{
 
	GatDiaPropertySheet olDlg("POSDIA", this, &omViewer, 0, GetString(IDS_STRING1647));
	bmIsViewOpen = true;
	int ilDlgRet = olDlg.DoModal();
	bmIsViewOpen = false;

	if (ilDlgRet != IDCANCEL)
	{
		if (olDlg.FilterChanged())
			LoadFlights();
 
		UpdateDia();

		char pclView[100];
		strcpy(pclView, omViewer.GetViewName());
		ogDdx.DataChanged((void *)this, SAS_VIEW_CHANGED,(void *)pclView );
		//pogWoResTableDlg->Rebuild();	

	}
	UpdateComboBox();

}




void GatDiagram::ResetStateVariables()
{
	CTime olUtcTime;
	GetCurrentUtcTime(olUtcTime);

    omTSStartTime = olUtcTime;// - CTimeSpan(0, 1, 0, 0);
    omStartTime = CTime(omTSStartTime.GetYear(), omTSStartTime.GetMonth(), omTSStartTime.GetDay(),
        0, 0, 0);// - CTimeSpan(1, 0, 0, 0);

    omDuration = CTimeSpan(3, 0, 0, 0);
    omTSDuration = CTimeSpan(0, 6, 0, 0);
    omTSInterval = CTimeSpan(0, 0, 10, 0);

}



bool GatDiagram::UpdateDia()
{
	CString olViewName = omViewer.GetViewName();
	if (olViewName == "<Default>")
	{
		ogPosDiaFlightData.ClearAll();
		ResetStateVariables();
	}

	
	if(IsIconic())  //RST
	{
		return false;
	}
	


	// set internal variables
	CTime olFrom = TIMENULL;
	CTime olTo = TIMENULL;
	CString olFtyps;
	omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);
	if (olFrom != TIMENULL && olTo != TIMENULL)
	{
		omDuration = (olTo - olFrom) + CTimeSpan(1, 0, 0, 0);
		omStartTime = olFrom - CTimeSpan(1, 0, 0, 0);
	}


	omViewer.MakeMasstab();

	CTime olT = omViewer.GetGeometrieStartTime();
//	ogBasicData.UtcToLocal(olT); //RSTL
	SetTSStartTime(olT);

	omTSDuration = omViewer.GetGeometryTimeSpan();
//rkr
	//Set begin to local if local
	CTime olTSStartTime = omTSStartTime;
	if(bgGatPosLocal)
		ogBasicData.UtcToLocal(olTSStartTime);

	omTimeScale.SetDisplayTimeFrame(olTSStartTime, omTSDuration, omTSInterval);
    omTimeScale.Invalidate(TRUE);
	ChangeViewTo(omViewer.GetViewName(), false);

	long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	if (llTotalMin > 0)
		nPos = nPos * 1000L / llTotalMin;
	SetScrollPos(SB_HORZ, int(nPos), TRUE);

	/*
	char pclView[100];
	strcpy(pclView, omViewer.GetViewName());
	ogDdx.DataChanged((void *)this, SAS_VIEW_CHANGED,(void *)pclView );
	*/

	pogWoResTableDlg->Rebuild();
	//pomDiffPosTableDlg->Rebuild();

	return true;
}





bool GatDiagram::LoadFlights(const char *pspView)
{

	if(pspView == NULL)
	{
		CString olView = omViewer.GetViewName();
		pspView = olView.GetBuffer(0);
	}
	

	omViewer.SelectView(pspView);
	if (strcmp(pspView, "<Default>") == 0)
	{
		return false;
	}



	// get Where-Clause
	CString olWhere;
	bool blRotation;
	CString olFOGTAB = "";

	bool blWithAcOnGround = bgShowOnGround;
	if (pogPosDiagram)
		blWithAcOnGround = pogPosDiagram->WithAcOnGround();

	omViewer.GetZeitraumWhereString(olWhere, blRotation, olFOGTAB, blWithAcOnGround);

	olWhere.TrimRight();
	if (olWhere.IsEmpty())
		return false;
	olWhere += CString(" AND FLNO <> ' '");



	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	// Update Status Bar
	omStatusBar.SetPaneText(0, GetString(IDS_STRING1205));
	omStatusBar.UpdateWindow();

	
	ogPosDiaFlightData.Register();	 


	// get timeframe
	CTime olFrom;
	CTime olTo;
	CString olFtyps;
	omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);

	//check timelimit for on gound (at moment we don#t know if we should use global or local)
	if (bgShowOnGround && pogPosDiagram)
	{
		if (olFrom != TIMENULL && olTo != TIMENULL)
		{
			CTimeSpan olDuration = olTo - olFrom;
			int ilTotalMinutes = olDuration.GetTotalMinutes();
			if (ogOnGroundTimelimit.GetTotalMinutes() >= ilTotalMinutes /*&& !bmMessageDone*/)
			{
				CString olMess;
				CString olTimeLimit;
				int ilHours = ogOnGroundTimelimit.GetTotalHours();
				int ilMinutes = ogOnGroundTimelimit.GetTotalMinutes() - ilHours*60;
				olTimeLimit.Format("%d.%d",ilHours,ilMinutes);
				olMess.Format(GetString(IDS_ONGROUND_TIMELIMIT), olTimeLimit );

				if (CFPMSApp::MyTopmostMessageBox(NULL, olMess, GetString(ST_WARNING), MB_OKCANCEL | MB_ICONWARNING) == IDOK)
					bool blok = true;
				else
					return false;
			}
		}
	}

	/*
	// enable/disable now button
	CTime olCurrUtc;
	GetCurrentUtcTime(olCurrUtc);

	CButton *polCB = (CButton *) omDialogBar.GetDlgItem(IDC_ZEIT);

	if(olFrom < olCurrUtc && olTo > olCurrUtc)
	{
		polCB->EnableWindow(TRUE);
	}
	else
	{
		polCB->EnableWindow(FALSE);
	}
	*/



	// set pre-filter
	ogPosDiaFlightData.SetPreSelection(olFrom, olTo, olFtyps);
	
	// read flights
	ogPosDiaFlightData.ReadAllFlights(olWhere, blRotation, olFOGTAB);

	// set internal variables
	omDuration = (olTo - olFrom) + CTimeSpan(1, 0, 0, 0);
	omStartTime = olFrom - CTimeSpan(1, 0, 0, 0);

	// upate status bar
	omStatusBar.SetPaneText(0, "");
	omStatusBar.UpdateWindow();

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	
	return true;
}


void GatDiagram::OnDestroy() 
{
 	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	ogDdx.UnRegister(&omViewer, NOTUSED);

	// Unregister DDX call back function
	TRACE("GatDiagram: DDX Unregistration %s\n",
		::IsWindow(GetSafeHwnd())? "": "(window is already destroyed)");
    ogDdx.UnRegister(this, NOTUSED);

	// Disconnect from the GatPosFlightData-Class
 	ogPosDiaFlightData.Disconnect();
	// Disconnect from the FlightsWithoutResource - Table
	if (pogWoResTableDlg)
		pogWoResTableDlg->Disconnect();

	CFrameWnd::OnDestroy();
}

void GatDiagram::OnClose() 
{
    // Ignore close -- This makes Alt-F4 keys no effect
	// don't ignore close
	CFrameWnd::OnClose();
}

void GatDiagram::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
{
    // TODO: Add your message handler code here and/or call default
    CFrameWnd::OnGetMinMaxInfo(lpMMI);

    lpMMI->ptMaxTrackSize = omMaxTrackSize;
    lpMMI->ptMinTrackSize = omMinTrackSize;
}

BOOL GatDiagram::OnEraseBkgnd(CDC* pDC) 
{
    // TODO: Add your message handler code here and/or call default
    CRect olClipRect;
    pDC->GetClipBox(&olClipRect);
    //GetClientRect(&olClipRect);

    CBrush olBrush(lmBkColor);
    CBrush *polOldBrush = pDC->SelectObject(&olBrush);
    pDC->PatBlt(olClipRect.left, olClipRect.top,
        olClipRect.Width(), olClipRect.Height(),
        PATCOPY);
    pDC->SelectObject(polOldBrush);
    
    return TRUE;
    
    //return CFrameWnd::OnEraseBkgnd(pDC);
}

void GatDiagram::OnPaint() 
{
    CPaintDC dc(this); // device context for painting
    
    // TODO: Add your message handler code here
    CRect olRect;
    GetClientRect(&olRect);
    
    // draw horizontal line
    CPen olHPen(PS_SOLID, 1, lmHilightColor);
    CPen *polOldPen = dc.SelectObject(&olHPen);
    dc.MoveTo(olRect.left, 27); dc.LineTo(olRect.right, 27);
    dc.MoveTo(olRect.left, 62); dc.LineTo(olRect.right, 62);

    CPen olTPen(PS_SOLID, 1, lmTextColor);
    dc.SelectObject(&olTPen);
    dc.MoveTo(olRect.left, 63); dc.LineTo(olRect.right, 63);
    
    
    // draw vertical line
    dc.SelectObject(&olTPen);
    dc.MoveTo(imStartTimeScalePos - 2, 27);
    dc.LineTo(imStartTimeScalePos - 2, 63);

    dc.SelectObject(&olHPen);
    dc.MoveTo(imStartTimeScalePos - 1, 27);
    dc.LineTo(imStartTimeScalePos - 1, 63);

    dc.SelectObject(polOldPen);
    // Do not call CFrameWnd::OnPaint() for painting messages
}


void GatDiagram::OnSize(UINT nType, int cx, int cy) 
{
    CFrameWnd::OnSize(nType, cx, cy);
    
    // TODO: Add your message handler code here
    CRect olRect; GetClientRect(&olRect);

    CRect olTSRect(imStartTimeScalePos, 28, olRect.right - (36 + 2), 62);
    omTimeScale.MoveWindow(&olTSRect, TRUE);

    olRect.top += 64;                   // for Dialog Bar & TimeScale
    olRect.bottom -= 20;                // for Horizontal Scroll Bar
    // LeftTop, RightBottom
    omClientWnd.MoveWindow(&olRect, TRUE);

	GetClientRect(&olRect);
	omTime.MoveWindow( CRect(olRect.right - 192, 3, olRect.right - 90, 22));
    omDate.MoveWindow( CRect(olRect.right - 88, 3, olRect.right - 8, 22));

	//PositionChild();
	ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV);
//	SetAllStaffAreaButtonsColor();
}

void GatDiagram::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
    // TODO: Add your message handler code here and/or call default
    
    //CFrameWnd::OnHScroll(nSBCode, nPos, pScrollBar);


	CUIntArray olLines;
	CTime olTSStartTime = omTSStartTime;

 	int ilCurrLine = pomChart->omGantt.GetTopIndex();

    long llTotalMin;
    int ilPos;
    
	CRect olRect;
	omTimeScale.GetClientRect(&olRect);
    switch (nSBCode)
    {
        case SB_LINEUP :
            //OutputDebugString("LineUp\n\r");
            
            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ) - int (60 * 1000L / llTotalMin);
            if (ilPos <= 0)
            {
                ilPos = 0;
                ShowTime(omStartTime);
                //SetTSStartTime(omStartTime);
            }
            else
			{
                ShowTime(omTSStartTime - CTimeSpan(0, 0, 60, 0));
                //SetTSStartTime(omTSStartTime - CTimeSpan(0, 0, 60, 0));
			}

			omViewer.UpdateLineHeights();
			/*
//rkr25042001
			olTSStartTime = omTSStartTime;
			if(bgGatPosLocal)
				ogBasicData.UtcToLocal(olTSStartTime);

            omTimeScale.SetDisplayStartTime(olTSStartTime);
//            omTimeScale.SetDisplayStartTime(omTSStartTime);

            omTimeScale.Invalidate(TRUE);
			ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV,TRUE);
            SetScrollPos(SB_HORZ, ilPos, TRUE);
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
            omClientWnd.Invalidate(FALSE);
////////////////////////////////////////////////////////////////////////
			*/
        break;
        
        case SB_LINEDOWN :
            //OutputDebugString("LineDown\n\r");

            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ) + int (60 * 1000L / llTotalMin);
            if (ilPos >= 1000)
            {
                ilPos = 1000;
                ShowTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin), 0));
                //SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin), 0));
            }
            else
			{
                ShowTime(omTSStartTime + CTimeSpan(0, 0, 60, 0));
                //SetTSStartTime(omTSStartTime + CTimeSpan(0, 0, 60, 0));
			}
			omViewer.UpdateLineHeights();
			/*
//rkr25042001
			olTSStartTime = omTSStartTime;
			if(bgGatPosLocal)
				ogBasicData.UtcToLocal(olTSStartTime);

            omTimeScale.SetDisplayStartTime(olTSStartTime);
//            omTimeScale.SetDisplayStartTime(omTSStartTime);

            omTimeScale.Invalidate(TRUE);
			ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV,TRUE);

			SetScrollPos(SB_HORZ, ilPos, TRUE);
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
            omClientWnd.Invalidate(FALSE);
////////////////////////////////////////////////////////////////////////
			*/
        break;
        
        case SB_PAGEUP :
            //OutputDebugString("PageUp\n\r");
            
            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ)
                - int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / llTotalMin);
            if (ilPos <= 0)
            {
                ilPos = 0;
                ShowTime(omStartTime);
                //SetTSStartTime(omStartTime);
            }
            else
			{
                ShowTime(omTSStartTime - CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));
                //SetTSStartTime(omTSStartTime - CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));
			}

			omViewer.UpdateLineHeights();
			/*
//rkr25042001
			olTSStartTime = omTSStartTime;
			if(bgGatPosLocal)
				ogBasicData.UtcToLocal(olTSStartTime);

            omTimeScale.SetDisplayStartTime(olTSStartTime);
//            omTimeScale.SetDisplayStartTime(omTSStartTime);

            omTimeScale.Invalidate(TRUE);
			ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV,TRUE);

            SetScrollPos(SB_HORZ, ilPos, TRUE);
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
            omClientWnd.Invalidate(FALSE);
////////////////////////////////////////////////////////////////////////
			*/
        break;
        
        case SB_PAGEDOWN :
            //OutputDebugString("PageDown\n\r");
            
            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ)
                + int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / llTotalMin);
            if (ilPos >= 1000)
            {
                ilPos = 1000;
                ShowTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin), 0));
                //SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin), 0));
            }
            else
			{
                ShowTime(omTSStartTime + CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));
                //SetTSStartTime(omTSStartTime + CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));
			}
			omViewer.UpdateLineHeights();
			/*
//rkr25042001
			olTSStartTime = omTSStartTime;
			if(bgGatPosLocal)
				ogBasicData.UtcToLocal(olTSStartTime);

            omTimeScale.SetDisplayStartTime(olTSStartTime);
//            omTimeScale.SetDisplayStartTime(omTSStartTime);

            omTimeScale.Invalidate(TRUE);
			ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV,TRUE);

            SetScrollPos(SB_HORZ, ilPos, TRUE);
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
            omClientWnd.Invalidate(FALSE);
////////////////////////////////////////////////////////////////////////
			*/
        break;
        
        case SB_THUMBTRACK /* pressed, any drag time */:
            //OutputDebugString("ThumbTrack\n\r");

            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
            
            SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin * nPos / 1000), 0));
			////ChangeViewTo(omViewer.SelectView());
            
//rkr25042001
			olTSStartTime = omTSStartTime;
			if(bgGatPosLocal)
				ogBasicData.UtcToLocal(olTSStartTime);

            omTimeScale.SetDisplayStartTime(olTSStartTime);
//            omTimeScale.SetDisplayStartTime(omTSStartTime);

            omTimeScale.Invalidate(TRUE);
            
            SetScrollPos(SB_HORZ, nPos, TRUE);

            //char clBuf[64];
            //wsprintf(clBuf, "Thumb Position : %d", nPos);
            //omStatusBar.SetWindowText(clBuf);
        break;

        //case SB_THUMBPOSITION /* released */:
            //OutputDebugString("ThumbPosition\n\r");
        //break;
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
        case SB_THUMBPOSITION:	// the thumb was just released?
			//ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV,TRUE);
			omViewer.ChangeViewTo(omViewer.SelectView(), omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));
			PositionChild();

            omClientWnd.Invalidate(FALSE);

////////////////////////////////////////////////////////////////////////

        case SB_TOP :
        //break;
        case SB_BOTTOM :
            //OutputDebugString("TopBottom\n\r");
        break;
        
        case SB_ENDSCROLL :
            //OutputDebugString("EndScroll\n\r");
            //OutputDebugString("\n\r");
        break;
    }

 	pomChart->omGantt.SetTopIndex(ilCurrLine);

}


 
void GatDiagram::OnTimer(UINT nIDEvent)
{

	/***
	if (nIDEvent == 1)
	{
		ogPosDiaFlightData.UpdateBarTimes();
	}
	***/

	///// Update time line!
	if(bmNoUpdatesNow == FALSE)
	{
		CTime olLocalTime = CTime::GetCurrentTime();
		CTime olUtcTime = CTime::GetCurrentTime();
		ogBasicData.LocalToUtc(olUtcTime);

		char pclTimes[100];
		char pclDate[100];
		
		CTime olUtcTimeTmp = CTime::GetCurrentTime();
		olUtcTimeTmp -= ogUtcDiff;
		
		if(ogUtcDiff != 0)
		{
			sprintf(pclDate, "%02d.%02d.%02d z", olUtcTimeTmp.GetDay(), olUtcTimeTmp.GetMonth(), olUtcTimeTmp.GetYear() % 100);
			sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTimeTmp.GetHour(), olUtcTimeTmp.GetMinute());
		}
		else
		{
			sprintf(pclDate, "%02d.%02d.%02d z", olUtcTime.GetDay(), olUtcTime.GetMonth(), olUtcTime.GetYear() % 100);
			sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTime.GetHour(), olUtcTime.GetMinute());
		}

		if(bgGatPosLocal)
			omTimeScale.UpdateCurrentTimeLine(olLocalTime);
		else
			omTimeScale.UpdateCurrentTimeLine(olUtcTime);
 
		omTime.SetWindowText(pclTimes);
		omTime.Invalidate(FALSE);

		omDate.SetWindowText(pclDate);
		omDate.Invalidate(FALSE);

 
		if(bgGatPosLocal)
			pomChart->GetGanttPtr()->SetCurrentTime(olLocalTime);
		else
			pomChart->GetGanttPtr()->SetCurrentTime(olUtcTime);
	
		CFrameWnd::OnTimer(nIDEvent);
	}
	///// Update time line! (END)
}

void GatDiagram::SetMarkTime(CTime opStartTime, CTime opEndTime)
{
	pomChart->SetMarkTime(opStartTime, opEndTime);
}


LONG GatDiagram::OnPositionChild(WPARAM wParam, LPARAM lParam)
{
    PositionChild();
    return 0L;
}

LONG GatDiagram::OnUpdateDiagram(WPARAM wParam, LPARAM lParam)
{
	UpdateWoResButton();

    CString olStr;
    int ilGroupNo = HIWORD(lParam);
    int ilLineNo = LOWORD(lParam);
	int ilItemID = omViewer.GetItemID(ilGroupNo, ilLineNo);

	CTime olTime = CTime((time_t) lParam);

	char clBuf[255];
	int ilCount;

    GatChart *polGatChart = pomChart;
    GatGantt *polGatGantt = polGatChart -> GetGanttPtr();
    switch (wParam)
    {
        // line message
        case UD_INSERTLINE :
            polGatGantt->InsertString(ilItemID, "");
			polGatGantt->RepaintItemHeight(ilItemID);
			
			ilCount = omViewer.GetLineCount(ilGroupNo);
			sprintf(clBuf, "%d", ilCount);
			polGatChart->GetCountTextPtr()->SetWindowText(clBuf);
			polGatChart->GetCountTextPtr()->Invalidate(TRUE);

			//SetStaffAreaButtonColor(ipGroupNo);
            PositionChild();
        break;
        
        case UD_UPDATELINE :
            polGatGantt->RepaintVerticalScale(ilItemID);
            polGatGantt->RepaintGanttChart(ilItemID);
			polGatGantt->RepaintItemHeight(ilItemID);
			//SetStaffAreaButtonColor(ipGroupNo);
        break;

        case UD_DELETELINE :
            polGatGantt->DeleteString(ilItemID);
            
			ilCount = omViewer.GetLineCount(ilGroupNo);
			sprintf(clBuf, "%d", ilCount);
			polGatChart->GetCountTextPtr()->SetWindowText(clBuf);
			polGatChart->GetCountTextPtr()->Invalidate(TRUE);

			//SetStaffAreaButtonColor(ipGroupNo);
			PositionChild();
        break;

        case UD_UPDATELINEHEIGHT :
            polGatGantt->RepaintItemHeight(ilItemID);
			//SetStaffAreaButtonColor(ipGroupNo);
            PositionChild();
		break;
    }

    return 0L;
}

 
///////////////////////////////////////////////////////////////////////////////
// Damkerng and Pichet: Date: 5 July 1995

void GatDiagram::UpdateComboBox()
{
	CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_VIEW);
	polCB->ResetContent();
	CStringArray olStrArr;
	omViewer.GetViews(olStrArr);
	for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
	{
		polCB->AddString(olStrArr[ilIndex]);
	}
	CString olViewName = omViewer.GetViewName();
	if (olViewName.IsEmpty() == TRUE)
	{

		olViewName = omViewer.SelectView();//ogCfgData.rmUserSetup.STCV;
	}

	ilIndex = polCB->FindString(-1,olViewName);
		
	if (ilIndex != CB_ERR)
	{
		polCB->SetCurSel(ilIndex);
	}
}

void GatDiagram::ChangeViewTo(const char *pcpViewName,bool RememberPositions)
{
	if (bgNoScroll == TRUE)
		return;

	if (pomChart != NULL)
	{
		if (!pomChart->omGantt.bmRepaint)
		{
			bmRepaintAll = true;
			return;
		}
    }
			
	int ilDwRef = m_dwRef;
	AfxGetApp()->DoWaitCursor(1);
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);

	CRect olRect; omTimeScale.GetClientRect(&olRect);

	/*
//rkr25042001
	//Set begin to local if local
	CTime olTSStartTime = omTSStartTime;
	if(bgGatPosLocal)
		ogBasicData.UtcToLocal(olTSStartTime);
    omViewer.ChangeViewTo(pcpViewName, olTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));
	*/
	// viewer always in UTC
    omViewer.ChangeViewTo(pcpViewName, omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));

	int ilChartState;
	int ilTopIndex;
 	if (RememberPositions == TRUE && pomChart != NULL)
	{
		ilChartState = pomChart->GetState();
		if (pomChart->GetGanttPtr() != NULL)
			ilTopIndex = pomChart->GetGanttPtr()->GetTopIndex();
	}
	// Id 30-Sep-96
	// This will remove a lot of warning message when the user change view.
	// If we just delete a staff chart, MFC will produce two warning message.
	// First, Revoke not called before the destructor.
	// Second, calling DestroyWindow() in CWnd::~CWnd.
	pomChart->DestroyWindow();


    omClientWnd.GetClientRect(&olRect);
        
	olRect.SetRect(olRect.left, 0, olRect.right, 0);
        
    pomChart = new GatChart;
    pomChart->SetTimeScale(&omTimeScale);
    pomChart->SetViewer(&omViewer);
    pomChart->SetStatusBar(&omStatusBar);
    pomChart->SetStartTime(omTSStartTime);
    pomChart->SetInterval(omTimeScale.GetDisplayDuration());
 	if (RememberPositions == TRUE)
	{
		pomChart->SetState(ilChartState);
	}
	else
	{
		pomChart->SetState(Maximized);
	}

    pomChart->Create(NULL, "GatChart", WS_OVERLAPPED | WS_BORDER | WS_CHILD | WS_VISIBLE,
            olRect, &omClientWnd, 0 /* IDD_CHART */, NULL);


 	if (RememberPositions == TRUE)
	{
		if (pomChart->GetGanttPtr() != NULL)
 			pomChart->GetGanttPtr()->SetTopIndex(ilTopIndex);
	}


	CTime olCurr = CTime::GetCurrentTime();
	//Die wollen local
 	if(!bgGatPosLocal)
		ogBasicData.LocalToUtc(olCurr);
	pomChart->GetGanttPtr()->SetCurrentTime(olCurr);
        
 
	PositionChild();
	AfxGetApp()->DoWaitCursor(-1);
	bmNoUpdatesNow = FALSE;
	omViewer.AllowUpdates(bmNoUpdatesNow);

	UpdateWoResButton();

	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//SetFocus();
}


void GatDiagram::UpdateWoResButton()
{
	int ilCount = pogWoResTableDlg->GetCount(GATDIA);

	if(ilCount >= 0)
	{
		CString olTmp;
		olTmp.Format("%s (%d)", GetString(IDS_STRING1914),ilCount);

		CButton *polSave = (CButton *)omDialogBar.GetDlgItem(IDC_WORES);	
		if(polSave != NULL)
			polSave->SetWindowText(olTmp);
	}

}


void GatDiagram::OnViewSelChange()
{
    char clText[64];
    CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_VIEW);
    polCB->GetLBText(polCB->GetCurSel(), clText);

	LoadFlights(clText);

	UpdateDia();

	ogDdx.DataChanged((void *)this, SAS_VIEW_CHANGED,(void *)clText );
	//pogWoResTableDlg->Rebuild();


	//ChangeViewTo(clText, false);
}

void GatDiagram::OnCloseupView() 
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//SetFocus();
}

void GatDiagram::OnZeit()
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//SetFocus();

	CTime olTime;
	GetCurrentUtcTime(olTime);
	olTime -= CTimeSpan(0, 1, 0, 0);

	ShowTime(olTime);


	/*
	// TODO: Add your command handler code here
	CTime olTime = CTime::GetCurrentTime();

	if(!bgGatPosLocal)	
		ogBasicData.LocalToUtc(olTime);


	//Die wollen local
	olTime -= CTimeSpan(0, 1, 0, 0);
	SetTSStartTime(olTime);
	//TRACE("Time: [%s]\n", olTime.Format("%H:%M"));

	omTimeScale.SetDisplayStartTime(omTSStartTime);
    omTimeScale.Invalidate(TRUE);
		
	// update scroll bar position
    long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	nPos = nPos * 1000L / llTotalMin;
    SetScrollPos(SB_HORZ, int(nPos), TRUE);

	ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV);
    omClientWnd.Invalidate(FALSE);
	*/
}


BOOL GatDiagram::DestroyWindow() 
{

	if ((bmIsViewOpen))
	{
		MessageBeep((UINT)-1);
		return FALSE;    // don't destroy window while view property sheet is still open
	}
	pogGatDiagram = NULL; 

	ogDdx.UnRegister(&omViewer, NOTUSED);
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	BOOL blRc = CWnd::DestroyWindow();
	return blRc;
}

/////////XXXXX
////////////////////////////////////////////////////////////////////////
// GatDiagram keyboard handling

void GatDiagram::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// check if the control key is pressed
    BOOL blIsControl = ::GetKeyState(VK_CONTROL) & 0x8080;
		// This statement has to be fixed for using both in Windows 3.11 and NT.
		// In Windows 3.11 the bit 0x80 will be turned on if control key is pressed.
		// In Windows NT, the bit 0x8000 will be turned on if control key is pressed.

	switch (nChar)
	{
	case VK_UP:		// move the bottom most gantt chart up/down one line
	case VK_DOWN:
		CListBox *polGantt;
		if ((polGantt = pomChart->GetGanttPtr()) != NULL)
			polGantt->SendMessage(WM_USERKEYDOWN, nChar);
		break;
 	case VK_LEFT:
		OnHScroll(blIsControl? SB_PAGEUP: SB_LINEUP, 0, NULL);
		break;
	case VK_RIGHT:
		OnHScroll(blIsControl? SB_PAGEDOWN: SB_LINEDOWN, 0, NULL);
		break;
	case VK_HOME:
		SetScrollPos(SB_HORZ, 0, FALSE);
		OnHScroll(SB_LINEUP, 0, NULL);
		break;
	case VK_END:
		SetScrollPos(SB_HORZ, 1000, FALSE);
		OnHScroll(SB_LINEDOWN, 0, NULL);
		break;
	default:
		omDialogBar.SendMessage(WM_KEYDOWN, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
		CFrameWnd::OnKeyDown(nChar, nRepCnt, nFlags);
		break;
	}
}

void GatDiagram::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	CFrameWnd::OnKeyUp(nChar, nRepCnt, nFlags);
}

////////////////////////////////////////////////////////////////////////


void GatDiagram::GetCurView( char * pcpView)
{

	strcpy(pcpView, omViewer.GetViewName());

}

void GatDiagram::ViewSelChange(char *pcpView)
{

	omViewer.SelectView(pcpView);
	
	CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_VIEW);
	polCB->SetCurSel(polCB->FindString( 0, pcpView ) );


	UpdateDia();


	/*
	if(IsIconic())  //RST
	{
		return;
	}



	omViewer.MakeMasstab();
	CTime olT = omViewer.GetGeometrieStartTime();
	CString olS = olT.Format("%d.%m.%Y-%H:%M");
	SetTSStartTime(olT);
	omTSDuration = omViewer.GetGeometryTimeSpan();

    
	long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	nPos = nPos * 1000L / llTotalMin;
	SetScrollPos(SB_HORZ, int(nPos), TRUE);
	
	
//rkr
	//Set begin to local if local
	CTime olTSStartTime = omTSStartTime;
	if(bgGatPosLocal)
		ogBasicData.UtcToLocal(olTSStartTime);
	
	omTimeScale.SetDisplayTimeFrame(olTSStartTime, omTSDuration, omTSInterval);
//	omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
    omTimeScale.Invalidate(TRUE);
	SetTSStartTime(omTSStartTime);
	ChangeViewTo(clText, false);
	*/
}




void GatDiagram::RedisplayAll()
{
	ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV);
}

void GatDiagram::SetTimeBand(CTime opStartTime, CTime opEndTime)
{
	omTimeBandStartTime = opStartTime;
	omTimeBandEndTime = opEndTime;
}

void GatDiagram::UpdateTimeBand()
{
 	pomChart->omGantt.SetMarkTime(omTimeBandStartTime, omTimeBandEndTime);
}

LONG GatDiagram::RepaintAll(WPARAM wParam, LPARAM lParam)
{
	ChangeViewTo(omViewer.GetViewName(), true);
	return 0L;
}

void GatDiagram::OnPrint() 
{
	omViewer.PrintGantt(pomChart);	
}

void GatDiagram::OnAllocate()
{
	if (!bgOffRelFuncGatpos)
	{
		ogPosDiaFlightData.ToggleOffline();
	}

	if (bgOffRelFuncGatpos && !ogPosDiaFlightData.bmOffLine)
	{
		MessageBox(GetString(IDS_STRING2003), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
		return;
	}

	SpotAllocateDlg *polDlg = new SpotAllocateDlg(this);
	if(polDlg->DoModal() == IDOK)
	{
		omStatusBar.SetPaneText(0, GetString(IDS_STRING1493));
		omStatusBar.UpdateWindow();
	}
	omStatusBar.SetPaneText(0, "");
	omStatusBar.UpdateWindow();
	delete polDlg;

	if (!bgOffRelFuncGatpos)
	{
		ogPosDiaFlightData.ToggleOffline();
	}

}


void GatDiagram::OnWoRes()
{
	pogWoResTableDlg->Activate(GATDIA);
}


void GatDiagram::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized) 
{
	CFrameWnd::OnActivate(nState, pWndOther, bMinimized);
	
	pogWoResTableDlg->Activate(GATDIA, false);


}



void GatDiagram::ShowTime(const CTime &ropTime)
{


	if(omTSStartTime == ropTime)
		return;

	
	SetTSStartTime(ropTime);
	//TRACE("Time: [%s]\n", olTime.Format("%H:%M"));

	CTime olTSStartTime(omTSStartTime);
	if(bgGatPosLocal)	
		ogBasicData.UtcToLocal(olTSStartTime);


	omTimeScale.SetDisplayStartTime(olTSStartTime);
    omTimeScale.Invalidate(TRUE);
		
	// update scroll bar position
    long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	nPos = nPos * 1000L / llTotalMin;
    SetScrollPos(SB_HORZ, int(nPos), TRUE);

	//ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV);

	CRect olRect;
	omTimeScale.GetClientRect(&olRect);
	omViewer.ChangeViewTo(omViewer.SelectView(), omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));
	PositionChild();

    omClientWnd.Invalidate(FALSE);


	/*
	opStart -= CTimeSpan(0, 1, 0, 0);

	if(omTSStartTime == opStart)
		return;
	
	SetTSStartTime(opStart);

	omTimeScale.SetDisplayStartTime(omTSStartTime);
    omTimeScale.Invalidate(TRUE);
		
	// update scroll bar position
    long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	nPos = nPos * 1000L / llTotalMin;
    SetScrollPos(SB_HORZ, int(nPos), TRUE);

	ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV); 
	omClientWnd.Invalidate(FALSE);
	*/
}



bool GatDiagram::ShowFlight(long lpUrno, char cpFPart)
{
	CTime olStart;
	CTime olEnd;

	DIAFLIGHTDATA *prlFlight = ogPosDiaFlightData.GetFlightByUrno(lpUrno);
	if (prlFlight == NULL)
		return false;

	omViewer.CalculateTimes(*prlFlight, cpFPart, olStart, olEnd);						

	CTime olTime = olStart - CTimeSpan(0, 1, 0, 0);
	ShowTime(olTime);
	
	if(bgGatPosLocal) ogBasicData.UtcToLocal(olStart);
	if(bgGatPosLocal) ogBasicData.UtcToLocal(olEnd);

	SetMarkTime(olStart, olEnd);
	
	return true;
}




