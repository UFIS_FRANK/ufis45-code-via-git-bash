// ccsspecfilterpage.cpp : implementation file
//

#include <stdafx.h>
#include <PSspecfilterpage.h>
#include <CCSGlobl.h>



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define CFIELD_FROM	"INHALT"
#define CFIELD_TO   "FILTER"
/////////////////////////////////////////////////////////////////////////////
// CSpecFilterPage property page

IMPLEMENT_DYNCREATE(CSpecFilterPage, CPropertyPage)

CSpecFilterPage::CSpecFilterPage() : CPropertyPage(CSpecFilterPage::IDD)
{
	//{{AFX_DATA_INIT(CSpecFilterPage)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	//pomPageBuffer= (CPageBuffer *)pomDataBuffer;
	omOldTable = CString("");
}

CSpecFilterPage::~CSpecFilterPage()
{
	//TRACE("CSpecFilterPage::~CSpecFilterPage\n");
	//pomPageBuffer=NULL;

	omSelectedItems.RemoveAll();
	omPossibleItems.RemoveAll();
	omData.DeleteAll();
}

void CSpecFilterPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSpecFilterPage)
	DDX_Control(pDX, IDC_LIST2, m_List2);
	DDX_Control(pDX, IDC_LIST1, m_List1);
	DDX_Control(pDX, IDC_COMBO_TABLE, m_Combo);
	//}}AFX_DATA_MAP

	// Extended data exchange -- for member variables with Control type
	if (pDX->m_bSaveAndValidate == FALSE)
	{
		SetData();
	}
	if (pDX->m_bSaveAndValidate == TRUE)
	{
		GetData();
	}
}


BEGIN_MESSAGE_MAP(CSpecFilterPage, CPropertyPage)
	//{{AFX_MSG_MAP(CSpecFilterPage)
	ON_BN_CLICKED(IDC_BUTTON_ADD, OnButtonAdd)
	ON_BN_CLICKED(IDC_BUTTON_REMOVE, OnButtonRemove)
	ON_CBN_SELCHANGE(IDC_COMBO_TABLE, OnSelchangeComboTable)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSpecFilterPage message handlers

void CSpecFilterPage::OnButtonAdd() 
{
	SPECFILTER_TABLES *prlCurrentTable = NULL;
	if(m_List1.GetSelCount() > 0)
	{
		prlCurrentTable = GetTable(omTable);
		if(prlCurrentTable == NULL)
		{
			prlCurrentTable = new SPECFILTER_TABLES;
			prlCurrentTable->TableName = omTable;
			omData.Add(prlCurrentTable);
		}
	}

	prlCurrentTable->SelectedItems.Empty();
	// TODO: Add your control notification handler code here
	int ilTable = 0;
	for (int i = m_List1.GetCount()-1; i >= 0; i--)
	{
		if (!m_List1.GetSel(i))	// unselected item?
			continue;
		CString s;
		m_List1.GetText(i, s);	// load string to "s"
		SPECFILTER_ITEMS rlItem;
		rlItem.Item = s;
		//prlCurrentTable->SelectedItems.NewAt(prlCurrentTable->SelectedItems.GetSize(), rlItem);
		m_List2.AddString(s);	// move string from left to right box
		m_List1.DeleteString(i);
	}	
	for(i = m_List2.GetCount()-1; i >= 0; i--)
	{
		CString olText;
		m_List2.GetText(i, olText);
		if(prlCurrentTable->SelectedItems.IsEmpty())
		{
			prlCurrentTable->SelectedItems+= olText;
		}
		else
		{
			prlCurrentTable->SelectedItems+= "|" + olText;
		}
	}
}

void CSpecFilterPage::OnButtonRemove() 
{
	// TODO: Add your control notification handler code here

	SPECFILTER_TABLES *prlCurrentTable = NULL;
	if(m_List2.GetCount() > 0)
	{
		prlCurrentTable = GetTable(omTable);
		if(prlCurrentTable == NULL)
		{
			prlCurrentTable->TableName = omTable;
			prlCurrentTable = new SPECFILTER_TABLES;
			omData.Add(prlCurrentTable);
		}
	}

	prlCurrentTable->SelectedItems.Empty();
	int ilTable = 0;
	//pomPageBuffer->DeleteResult(ilTable);
	for (int i = m_List2.GetCount()-1; i >= 0; i--)
	{
		CString s;
		if (!m_List2.GetSel(i))	// unselected item?
		{
			m_List2.GetText(i, s);	// load string to "s"
			SPECFILTER_ITEMS rlItem;
			rlItem.Item = s;
			if(prlCurrentTable->SelectedItems.IsEmpty())
			{
				prlCurrentTable->SelectedItems+= s;
			}
			else
			{
				prlCurrentTable->SelectedItems+= "|" + s;
			}
			//pomPageBuffer->SetResult(ilTable,"","Filter","=",s,"OR");
			continue;
		} // end if
		m_List2.GetText(i, s);	// load string to "s"
		//prlCurrentTable->SelectedItems.NewAt(prlCurrentTable->SelectedItems.GetSize(),rlItem);
		m_List1.AddString(s);	// move string from right to left box
		m_List2.DeleteString(i);
	}

}

void CSpecFilterPage::OnSelchangeComboTable() 
{
	// TODO: Add your control notification handler code here
//	pomPageBuffer= (CPageBuffer *)pomInitBuffer;

	CString olField, olDescription, olFormat, olOperator;
	CComboBox *polView = (CComboBox *)GetDlgItem(IDC_COMBO_TABLE);

// L�sche alte Daten
	omPossibleItems.RemoveAll();
	omSelectedItems.RemoveAll();

// Hole Tabellenname aus der ComboBox
	int ilItemID;
	if ((ilItemID = polView->GetCurSel()) == LB_ERR)
		polView->GetWindowText(omTable);
	else
		polView->GetLBText(ilItemID, omTable);


	
/*
	m_List2.ResetContent();
	m_List1.ResetContent();
	int ilCount = 0;
	if(omTable == GetString(IDS_STRING1213))
	{
		ilCount = ogHtyData.omData.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			m_List1.AddString(CString(ogHtyData.omData[i].Hnam)); // HTY.Hnam
		}
	}
	else if(omTable == GetString(IDS_STRING1214))
	{
		ilCount = ogAltData.omData.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			m_List1.AddString(CString(ogAltData.omData[i].Alc3));
		}
	}
	else if(omTable == GetString(IDS_STRING1215))
	{
		ilCount = ogAptData.omData.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			m_List1.AddString(CString(ogAptData.omData[i].Apc3));
		}
	}
	else if(omTable == GetString(IDS_STRING1216))
	{
		ilCount = ogExtData.omData.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			m_List1.AddString(CString(ogExtData.omData[i].Enam));
		}
	}
	else if(omTable == GetString(IDS_STRING1217))
	{
		ilCount = ogDenData.omData.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			m_List1.AddString(CString(ogDenData.omData[i].Deca));
		}
	}
	else if(omTable == GetString(IDS_STRING1218))
	{
		ilCount = ogGatData.omData.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			m_List1.AddString(CString(ogGatData.omData[i].Gnam));
		}
	}
	else if(omTable == GetString(IDS_STRING1219))
	{
		ilCount = ogBltData.omData.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			m_List1.AddString(CString(ogBltData.omData[i].Bnam));
		}
	}
	else if(omTable == GetString(IDS_STRING1220))
	{
		ilCount = ogPstData.omData.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			m_List1.AddString(CString(ogPstData.omData[i].Pnam));
		}
	}
	else if(omTable == GetString(IDS_STRING1221))
	{
		ilCount = ogNatData.omData.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			m_List1.AddString(CString(ogNatData.omData[i].Ttyp));
		}
	}
	else if(omTable == GetString(IDS_STRING1222))
	{
		ilCount = ogWroData.omData.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			m_List1.AddString(CString(ogWroData.omData[i].Wnam));
		}
	}
*/

	SPECFILTER_TABLES *prlCurrentFilter;
	prlCurrentFilter = GetTable(omTable);
	if(prlCurrentFilter == NULL)
	{
		//do nothing
	}
	else
	{

		CString olSubString = prlCurrentFilter->SelectedItems;
		if(!olSubString.IsEmpty())
		{
			int pos;
			int olPos = 0;
			bool blEnd = false;
			CString olText;
			while(blEnd == false)
			{
				pos = olSubString.Find('|');
				if(pos == -1)
				{
					blEnd = true;
					olText = olSubString;
				}
				else
				{
					olText = olSubString.Mid(0, olSubString.Find('|'));
					olSubString = olSubString.Mid(olSubString.Find('|')+1, olSubString.GetLength( )-olSubString.Find('|')+1);
				}
				m_List2.AddString(olText);
				int ilIdx = m_List1.FindString( -1, olText);
				if(ilIdx != LB_ERR)
				{
					m_List1.DeleteString((UINT)ilIdx);
				}
			}
		}

/*
		int ilC = prlCurrentFilter->SelectedItems.GetSize();
		for(int i = 0; i < ilC; i++)
		{
			m_List2.AddString(prlCurrentFilter->SelectedItems[i]);
			//m_List2.AddString(prlCurrentFilter->SelectedItems[i].Item);
			int ilIdx;
			ilIdx = m_List1.FindString( -1, prlCurrentFilter->SelectedItems[i]);
			//ilIdx = m_List1.FindString( -1, prlCurrentFilter->SelectedItems[i].Item);
			if(ilIdx != LB_ERR)
			{
				//Alle in der rechten Seiten aus der linken Seite rausnehmen
				m_List1.DeleteString((UINT)ilIdx);
			}
		}
*/
	}

} // end OnSelchangeComboTable
//--------------------------------------------------------------------------
BOOL CSpecFilterPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	InitMask();
	//SetData();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSpecFilterPage::GetData()
{
	omValues.RemoveAll();
	int ilCount = omData.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		CString olValue = omData[i].TableName + CString("=") + omData[i].SelectedItems;// + CString("@");
		omValues.Add(olValue);
	}
}

void CSpecFilterPage::SetData()
{
	omData.DeleteAll();
	int ilCount = omValues.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		SPECFILTER_TABLES *prlFilter = new SPECFILTER_TABLES;
		CString olValue = omValues[i];
		int ilIdx = olValue.Find('@');
		if(ilIdx != -1)
		{
			olValue.SetAt(ilIdx, ' ');
			olValue.TrimRight();
		}
		if(olValue.Find(GetString(IDS_STRING1213)) != -1)
			prlFilter->TableName = CString(GetString(IDS_STRING1213));
		if(olValue.Find(GetString(IDS_STRING1214)) != -1)
			prlFilter->TableName = CString(GetString(IDS_STRING1214));
		if(olValue.Find(GetString(IDS_STRING1215)) != -1)
			prlFilter->TableName = CString(GetString(IDS_STRING1215));
		if(olValue.Find(GetString(IDS_STRING1216)) != -1)
			prlFilter->TableName = CString(GetString(IDS_STRING1216));
		if(olValue.Find(GetString(IDS_STRING1217)) != -1)
			prlFilter->TableName = CString(GetString(IDS_STRING1217));
		if(olValue.Find(GetString(IDS_STRING1218)) != -1)
			prlFilter->TableName = CString(GetString(IDS_STRING1218));
		if(olValue.Find(GetString(IDS_STRING1219)) != -1)
			prlFilter->TableName = CString(GetString(IDS_STRING1219));
		if(olValue.Find(GetString(IDS_STRING1220)) != -1)
			prlFilter->TableName = CString(GetString(IDS_STRING1220));
		if(olValue.Find(GetString(IDS_STRING1221)) != -1)
			prlFilter->TableName = CString(GetString(IDS_STRING1221));
		if(olValue.Find(GetString(IDS_STRING1222)) != -1)
			prlFilter->TableName = CString(GetString(IDS_STRING1222));
		//Extract the Substring with values;
		prlFilter->SelectedItems = olValue.Mid(olValue.Find('=')+1, olValue.GetLength( ));
		omData.Add(prlFilter);
	}
	OnSelchangeComboTable();
}
/*void *CSpecFilterPage::GetData()

{
	UpdateData(TRUE);
	int ilTable = 0;
	for (int i = m_List2.GetCount()-1; i >= 0; i--)
	{
		CString s;
		m_List2.GetText(i, s);	// load string to "s"
		pomPageBuffer->SetResult(ilTable,"","Filter","=",s,"OR");
	}
	omSelectedItems.DeleteAll();
	omPossibleItems.DeleteAll();
	return((void *)pomPageBuffer);
} // end GetData
//---------------------------------------------------------------------------------
void CSpecFilterPage::InitialScreen()
{
	pomPageBuffer= (CPageBuffer *)pomInitBuffer;

	if (pomPageBuffer != NULL)
	{
	// TODO: Add extra initialization here
		imTable=0;
		CString olField, olDescription;
		CString olFormat;
		CString olOperator;
		olField = CFIELD_FROM;
		omPossibleItems.DeleteAll();
		omSelectedItems.DeleteAll();
		CComboBox *polView = (CComboBox *)GetDlgItem(IDC_COMBO_TABLE);
		do
		{
			omTable = pomPageBuffer->GetInitTableName(imTable);
			if (omTable.GetLength() > 0)
			{
			   polView->AddString(omTable);
			} // end if

			imTable++;

		} while (omTable.GetLength() > 0);
		polView->SetCurSel(0);
		imTable = -1;
		polView->GetLBText(0, omTable);

		pomPageBuffer->GetInit(imTable, 0, omTable, olField, 
					olDescription, olFormat, olOperator,
					&omPossibleItems);
		m_List1.ResetContent();
		for (int ilIdx = 0; ilIdx < omPossibleItems.GetSize(); ilIdx++)
		{
			m_List1.AddString(omPossibleItems[ilIdx]);	
		} // end for

		olField = CFIELD_TO;
		pomPageBuffer->GetInit(imTable, 1, omTable, olField, 
					olDescription, olFormat, olOperator,
					&omSelectedItems);
		m_List2.ResetContent();
		for (ilIdx = 0; ilIdx < omSelectedItems.GetSize(); ilIdx++)
		{
			m_List2.AddString(omSelectedItems[ilIdx]);	
		} // end for
	} // end if

}
*/
CString CSpecFilterPage::GetPrivList()
{
	CString olInitList;
	olInitList=CString("SpecFilterPage,SpecFilter_FromList,FromList,L,1")+
		       CString("SpecFilterPage,SpecFilter_ToList,ToList,L,1")+
			   CString("SpecFilterPage,SpecFilter_Insert,Zuf�gen,B,1")+
			   CString("SpecFilterPage,SpecFilter_Delete,L�schen,B,1");
	return(olInitList);
}

void CSpecFilterPage::InitMask()
{
	omTables.Add(GetString(IDS_STRING1213)); //GetString(IDS_STRING1213)
	omTables.Add(GetString(IDS_STRING1214)); // GetString(IDS_STRING1214)
	omTables.Add(GetString(IDS_STRING1215)); // GetString(IDS_STRING1215)
	omTables.Add(GetString(IDS_STRING1216)); // GetString(IDS_STRING1216)
	omTables.Add(GetString(IDS_STRING1217)); // GetString(IDS_STRING1217)
	omTables.Add(GetString(IDS_STRING1218)); // GetString(IDS_STRING1218)
	omTables.Add(GetString(IDS_STRING1219)); // GetString(IDS_STRING1219)
	omTables.Add(GetString(IDS_STRING1220)); // GetString(IDS_STRING1220)
	omTables.Add(GetString(IDS_STRING1221)); //GetString(IDS_STRING1221)
	omTables.Add(GetString(IDS_STRING1222)); //GetString(IDS_STRING1222)

	for(int i = 0; i < omTables.GetSize(); i++)
	{
		m_Combo.AddString(omTables[i]);
	}

}

SPECFILTER_TABLES *CSpecFilterPage::GetTable(CString opTable)
{
	SPECFILTER_TABLES *prlFilter = NULL;
	for(int i = 0; i < omData.GetSize(); i++)
	{
		if( omData[i].TableName == opTable)
		{
			prlFilter = &omData[i];
			i = omData.GetSize();
		}
	}

	return prlFilter;

}