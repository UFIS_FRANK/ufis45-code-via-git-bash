#ifndef AFX_REPORTSEQFIELDDLG_H__CDB257E1_8BF5_11D1_8127_0000B43C4B01__INCLUDED_
#define AFX_REPORTSEQFIELDDLG_H__CDB257E1_8BF5_11D1_8127_0000B43C4B01__INCLUDED_

// ReportSeqFieldDlg.h : Header-Datei
//
#include <CCSEdit.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CReportSeqFieldDlg 

class CReportSeqFieldDlg : public CDialog
{
// Konstruktion
public:
	CReportSeqFieldDlg(CWnd* pParent = NULL, CString opHeadline = "", CString opSelectfield = "",
		               CTime* oMinDate = NULL, CTime* oMaxDate = NULL, char *opSelect = "", 
					   CString opSetFormat = "", int ipMin = 0, int ipMax = 0);

	CTime *pomMinDate;
	CTime *pomMaxDate;
	char *pcmSelect;
// Dialogfelddaten
	//{{AFX_DATA(CReportSeqFieldDlg)
	enum { IDD = IDD_REPORTSEQFIELD };
	CStatic	m_CS_Selectfield;
	CCSEdit	m_CE_Select;
	CCSEdit	m_CE_Datumvon;
	CCSEdit	m_CE_Datumbis;
	CString	m_Datumbis;
	CString	m_Datumvon;
	CString	m_Select;
	CString	m_Selectfield;
	//}}AFX_DATA
	CString omHeadline;

// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CReportSeqFieldDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

private:
	CString omSetFormat;
	int imMax; 
	int imMin;

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CReportSeqFieldDlg)
	virtual void OnCancel();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_REPORTSEQFIELDDLG_H__CDB257E1_8BF5_11D1_8127_0000B43C4B01__INCLUDED_
