// GanttChartReportWnd.cpp : implementation file
//

#include <stdafx.h>
#include <GanttChartReportWnd.h>
#include <GanttBarReport.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



#define SPACE_TOP_BUTTON	50
#define SPACE_RIGHT_LEFT	15


/////////////////////////////////////////////////////////////////////////////
// GanttChartReportWnd

GanttChartReportWnd::GanttChartReportWnd(CWnd* ppParent, CString opXAxe, CString opYAxe, 
										 CColor opColor)
{

	// Pointer des ScrollViews initialisieren
	pomGanttChartReport = NULL;

	// Text der X-Achse an klassenweite Vraibale �bergeben
	omXAxe = opXAxe;
	// Text der Y-Achse an klassenweite Vraibale �bergeben
	omYAxe = opYAxe;

	// Farbe der Bars
	omColor.r = opColor.r;
	omColor.g = opColor.g;
	omColor.b = opColor.b;

}


GanttChartReportWnd::~GanttChartReportWnd()
{

	//delete pomGanttChartReport;

}


BEGIN_MESSAGE_MAP(GanttChartReportWnd, CWnd)
	//{{AFX_MSG_MAP(GanttChartReportWnd)
    ON_WM_CREATE()
	// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// GanttChartReportWnd message handlers

int GanttChartReportWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	CRect olRect;
	GetClientRect(&olRect);

	// Window etwas kleiner als Dialog
	olRect.bottom -= SPACE_TOP_BUTTON;
	olRect.top    += SPACE_TOP_BUTTON;
	olRect.left   += SPACE_RIGHT_LEFT;
	olRect.right  -= SPACE_RIGHT_LEFT;

	pomGanttChartReport = new GanttChartReport(this, olRect, omXAxe, omYAxe, omColor);

	pomGanttChartReport->SetXAxeText(omXAxeText);
	pomGanttChartReport->SetData(omData);
	pomGanttChartReport->SetBarText(omBarText);

	pomGanttChartReport->Create(NULL, "Chart", WS_HSCROLL, olRect, this, 0);
	pomGanttChartReport->ShowWindow(SW_SHOWNORMAL);
	pomGanttChartReport->OnInitialUpdate();


    return 0;

}


void GanttChartReportWnd::SetData(const CUIntArray& opData)
{

	omData.Copy(opData);

}


void GanttChartReportWnd::SetXAxeText(const CStringArray& opXAxeText)
{

	omXAxeText.Copy(opXAxeText);

}


void GanttChartReportWnd::SetBarText(const CStringArray& opBarText)
{

	omBarText.Copy(opBarText);

}


void GanttChartReportWnd::Print(CDC* popDC, CCSPrint* popPrint, CString opHeader, CString opFooterLeft)
{

	pomGanttChartReport->Print(popDC, popPrint, opHeader, opFooterLeft);

}


