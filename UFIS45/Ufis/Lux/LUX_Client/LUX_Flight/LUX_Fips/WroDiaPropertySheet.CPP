// flplanps.cpp : implementation file
//

// These following include files are project dependent
#include <stdafx.h>

// These following include files are necessary
#include <cviewer.h>
#include <WroDiaPropertySheet.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CStringArray ogFlightAlcd;
extern int igAnsichtPageIndex;
/////////////////////////////////////////////////////////////////////////////
// WroDiaPropertySheet
//

WroDiaPropertySheet::WroDiaPropertySheet(CString opCalledFrom, CWnd* pParentWnd,
	CViewer *popViewer, UINT iSelectPage, LPCSTR pszCaption) :
	BasePropertySheet(pszCaption, pParentWnd, popViewer, iSelectPage),
	m_ZeitraumPage(bgGatPosLocal), m_Geometrie(WRODIA)
{
	igAnsichtPageIndex = -1;
	AddPage(&m_ZeitraumPage);
	m_ZeitraumPage.SetCalledFrom(opCalledFrom);
//	AddPage(&m_SearchFlightPage);
//	m_SearchFlightPage.SetCalledFrom(opCalledFrom);
	AddPage(&m_Geometrie);
	m_Geometrie.SetCalledFrom(opCalledFrom);
//	AddPage(&m_RulesPage);
	//AddPage(&m_PSUniFilter);
	//m_PSUniFilter.SetCalledFrom(opCalledFrom);
	//AddPage(&m_SpecialFilterPage);
	//AddPage(&m_PSUniSortPage);
	//m_PSUniSortPage.SetCalledFrom(opCalledFrom);
	//AddPage(&m_SpecialSortPage);
	//m_PSUniFilter.SetCaption("Allgem. Filter");
}

void WroDiaPropertySheet::LoadDataFromViewer()
{
	//pomViewer->GetFilter("UNIFILTER", m_PSUniFilter.omValues);
	//pomViewer->GetSort("FLIGHTSORT", m_SpecialSortPage.omSortOrders);
	//pomViewer->GetSort("UNISORT", m_PSUniSortPage.omValues);
	pomViewer->GetFilter("GEOMETRIE", m_Geometrie.omValues);
	//pomViewer->GetSearch("FLIGHTSEARCH", m_SearchFlightPage.omValues);
	pomViewer->GetSearch("FLIGHTSEARCH", m_ZeitraumPage.omValues);
	//pomViewer->GetFilter("RULES", m_RulesPage.omValues);
}

void WroDiaPropertySheet::SaveDataToViewer(CString opViewName,BOOL bpSaveToDb)
{
	//m_PSUniFilter.UpdateData();
	//m_SpecialFilterPage.UpdateData();
	//SearchFlightPage.UpdateData();
	//m_PSUniSortPage.UpdateData();
	//m_SpecialSortPage.UpdateData();

	//MWO
	//pomViewer->SetFilter("UNIFILTER", m_PSUniFilter.omValues);
	//pomViewer->SetSort("FLIGHTSORT", m_SpecialSortPage.omSortOrders);
	//pomViewer->SetSort("UNISORT", m_PSUniSortPage.omValues);
	pomViewer->SetFilter("GEOMETRIE", m_Geometrie.omValues);
	//pomViewer->SetSearch("FLIGHTSEARCH", m_SearchFlightPage.omValues);
	pomViewer->SetSearch("FLIGHTSEARCH", m_ZeitraumPage.omValues);
	//pomViewer->SetFilter("RULES", m_RulesPage.omValues);
	igAnsichtPageIndex = GetActiveIndex();
	if (bpSaveToDb == TRUE)
	{
		pomViewer->SafeDataToDB(opViewName);
	}

}


bool WroDiaPropertySheet::FilterChanged()
{
	return m_ZeitraumPage.bmChanged || bmViewChanged;
}


int WroDiaPropertySheet::QueryForDiscardChanges()
{
/*	CStringArray olSelectedAirlines;
	CStringArray olSelectedArrivals;
	CStringArray olSelectedDepartures;
	CStringArray olSortOrders;
	CString olGroupBy;
	CStringArray olSelectedTime;
	pomViewer->GetFilter("Airline", olSelectedAirlines);
	pomViewer->GetFilter("Gate Arrival", olSelectedArrivals);
	pomViewer->GetFilter("Gate Departure", olSelectedDepartures);
	pomViewer->GetSort(olSortOrders);
	olGroupBy = pomViewer->GetGroup();
	pomViewer->GetFilter("Zeit", olSelectedTime);

	CStringArray olInbound, olOutbound;
	pomViewer->GetFilter("Inbound", olInbound);
	pomViewer->GetFilter("Outbound", olOutbound);

	if (!IsIdentical(olSelectedAirlines, m_pageAirline.omSelectedItems) ||
		!IsIdentical(olSelectedArrivals, m_pageArrival.omSelectedItems) ||
		!IsIdentical(olSelectedDepartures, m_pageDeparture.omSelectedItems) ||
		!IsIdentical(olSortOrders, m_pageSort.omSortOrders) ||
		olGroupBy != CString(m_pageSort.m_Group + '0') ||
		!IsIdentical(olSelectedTime, m_pageZeit.omFilterValues) ||
		olInbound[0] != CString(m_pageBound.m_Inbound + '0') ||
		olOutbound[0] != CString(m_pageBound.m_Outbound + '0'))
		return MessageBox(ID_MESSAGE_LOST_CHANGES, NULL, MB_ICONQUESTION | MB_OKCANCEL);
*/
	return IDOK;
}
