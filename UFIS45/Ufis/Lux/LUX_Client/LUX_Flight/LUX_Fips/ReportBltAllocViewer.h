#if !defined(REPORTBLTALLOCVIEWER_H__INCLUDED_)
#define REPORTBLTALLOCVIEWER_H__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
 
//#pragma warning(disable: 4786)

#include <stdafx.h>
#include <CCSTable.h>
#include <CCSDefines.h>
#include <CViewer.h>
#include <CCSPrint.h>

#include <RotationDlgCedaFlightData.h>
 
#define REPORTBLTALLOC_COLCOUNT 5

struct REPORTBLTALLOC_LINEDATA
{
 	long	UAft;		// Urno des flights
 
 	CString	Flno;	// Flightnumber
 	CString	Org3;	// Origin
 	CTime	Stoa;	// Arrival time
	CTime	Etai;	// Estimated arrival time

  	CString Blt;	// Belt 
  
 	
	REPORTBLTALLOC_LINEDATA()
	{ 
 		UAft = 0;
 		Flno.Empty();
 		Org3.Empty();
		Stoa = TIMENULL;
		Etai = TIMENULL;

  		Blt.Empty();
	}
};

 



/////////////////////////////////////////////////////////////////////////////
// Befehlsziel ReportSeasonTableViewer 


class ReportBltAllocViewer : public CViewer
{
// Constructions
public:
    ReportBltAllocViewer();
    ~ReportBltAllocViewer();

	// Connects the viewer with a table
    void Attach(CCSTable *popTable);
	// Load the intern line data from the given data and displays the table
    void ChangeViewTo(const CCSPtrArray<ROTATIONDLGFLIGHTDATA> &ropData, char *popDateStr);
 
	// Rebuild the table from the intern data
 	void UpdateDisplay(void);
	// Print table to paper
	void PrintTableView(void);
	// Print table to file
	bool PrintPlanToFile(const char *popFilePath) const;
	
	int GetFlightCount(void) const;
 	
private:

	// columns width of the table in char
	static int imTableColCharWidths[REPORTBLTALLOC_COLCOUNT];
	// columns width of the table in pixels
	int imTableColWidths[REPORTBLTALLOC_COLCOUNT];
	int imPrintColWidths[REPORTBLTALLOC_COLCOUNT];
	// table header strings
	CString omTableHeadlines[REPORTBLTALLOC_COLCOUNT];


	const int imOrientation;

	// Table fonts and widths for displaying
	CFont &romTableHeaderFont; 
	CFont &romTableLinesFont;
	const float fmTableHeaderFontWidth; 
	const float fmTableLinesFontWidth;

	// Table fonts and widths for printing
	CFont *pomPrintHeaderFont; 
	CFont *pomPrintLinesFont;
	float fmPrintHeaderFontWidth; 
	float fmPrintLinesFontWidth;

	
	void DrawTableHeader(void);
	// Transfer the data of the database to the intern data structures
	void MakeLines(const CCSPtrArray<ROTATIONDLGFLIGHTDATA> &ropData);
	// Copy the data from the db-record to the table-record
	bool MakeLineData(REPORTBLTALLOC_LINEDATA &rrpLineData, const ROTATIONDLGFLIGHTDATA &rrpFlight) const;
	// Create a intern table data record
	int  CreateLine(const REPORTBLTALLOC_LINEDATA &rrpLine);
	// Fills one row of the table
	bool MakeColList(const REPORTBLTALLOC_LINEDATA &rrpLine, CCSPtrArray<TABLE_COLUMN> &ropColList) const;
	// Compares two lines of the table
	int CompareLines(const REPORTBLTALLOC_LINEDATA &rrpLine1, const REPORTBLTALLOC_LINEDATA &rrpLine2) const;

	bool PrintTableHeader(CCSPrint &ropPrint);

 	bool PrintTableLine(CCSPrint &ropPrint, int ipLineNo);
	// delete intern table lines
    void DeleteAll(void);
	// Date string
	char *pomDateStr;

 	// Table
 	CCSTable *pomTable;

	// Table Data
	CCSPtrArray<REPORTBLTALLOC_LINEDATA> omLines;
 
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(REPORTBLTALLOCVIEWER_H__INCLUDED_)
