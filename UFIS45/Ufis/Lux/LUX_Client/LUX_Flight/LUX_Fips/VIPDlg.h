#if !defined(AFX_VERYIMPPERS_H__6FB70432_EE67_11D2_AD3D_004095436A98__INCLUDED_)
#define AFX_VERYIMPPERS_H__6FB70432_EE67_11D2_AD3D_004095436A98__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// VeryImpPers.h : header file
//
#include <PrivList.h>
#include <CCSEdit.h>
#include <resource.h>
#include <resrc1.h>
#include <RotationVipDlg.h>


/////////////////////////////////////////////////////////////////////////////
// VIPDlg dialog

class VIPDlg : public CDialog
{
// Construction
public:
	VIPDlg(CWnd* pParent = NULL, char opType = ' ', char opMode = ' ', CString opUrno = "-1");
	~VIPDlg();

	void SetAftTabUrnoA(CString opUrno);
	void SetAftTabUrnoD(CString opUrno);

	void InitDialog();
	void setStatus(RotationVipDlg::Status epStatus = RotationVipDlg::Status::UNKNOWN);
	RotationVipDlg::Status getStatus() const;

// Dialog Data
	//{{AFX_DATA(VIPDlg)
	enum { IDD = IDD_VIP };
	CButton	m_OK;
	CCSEdit	m_CdatD;
	CCSEdit	m_CdatT;
	CCSEdit	m_LstuD;
	CCSEdit	m_LstuT;
	CCSEdit	m_NoGr;
	CCSEdit	m_NoPx;
	CCSEdit	m_Paxn;
	CCSEdit	m_Paxr;
	CCSEdit	m_Usec;
	CCSEdit	m_Useu;
	CCSEdit	m_Grid;
	CString m_Caption;
	BOOL    m_CH_Arrival;
	BOOL    m_CH_Departure;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(VIPDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(VIPDlg)
	virtual void OnCancel();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnChArrival();
	afx_msg void OnChDeparture();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CString omUrno; 
	CString omUrnoA;
	CString omUrnoD;
	char    omMode;
	char    omType;
		
private:
	RotationVipDlg::Status emStatus;
	void InitialPostFlight(BOOL);
	void InitialStatus();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VERYIMPPERS_H__6FB70432_EE67_11D2_AD3D_004095436A98__INCLUDED_)
