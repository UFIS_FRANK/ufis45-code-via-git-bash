
#include <stdafx.h>
#include <CCSPrint.h>
#include <CCSGlobl.h>
#include <resource.h>
#include <resrc1.h>
#include <CCSbar.h>
#include <WINSPOOL.H>
#include <BasicData.h>
#include <FPMS.h>
 

#define INCH 0.284

#define MMX(x)    ((int)(MulDiv((x),imLogPixelsX, 72)*INCH))
#define MMY(x)    ((int)(MulDiv((x),imLogPixelsY, 72)*INCH))


CCSPtrArray <PRINTBARDATA> omDefBkBars; 
double dgCCSPrintFactor = 2.3;
int igCCSPrintMinLength = 125;
int igCCSPrintMoreLength = 20;
//-----------------------------------------------------------------------------------------------

CCSPrint::CCSPrint(CWnd *opParent)
{
	pomParent = opParent;
	imLineNo = 999;
	imPageNo = 0;
	imMaxLines = 14;
	bgPrintDlgOpen = false;
	hmBitmap = NULL;
	hmPalette= NULL;
	imLeftOffset = 50;

}

CCSPrint::CCSPrint(CWnd *opParent,int ipOrientation,
				int ipLineHeight,int ipFirstLine,int ipLeftOffset,
				CString opHeader1,CString opHeader2,CString opHeader3,CString opHeader4,
				CString opFooter1,CString opFooter2,CString opFooter3)
{
	bmIsInitialized = FALSE;

	imLineHeight = ipLineHeight;

	pomParent = opParent;
	imOrientation = ipOrientation;
	omHeader.Add(opHeader1);
	omHeader.Add(opHeader2);
	omHeader.Add(opHeader3);
	omHeader.Add(opHeader4);
	omHeader.Add(opFooter1);
	omHeader.Add(opFooter2);
	omHeader.Add(opFooter3);

	imLineNo = 999;
	imPageNo = 0;
	imLeftOffset = 50;
	if (imOrientation == PRINT_LANDSCAPE)
	{
		imMaxLines   = 38;
//		imMaxLines   = 100;
		imFirstLine  = 200;
		imLeftOffset = 50;
	}
	else
	{
		imMaxLines   = 57;
		imFirstLine  = 200;
		imLeftOffset = 200;
	}

	if(ipFirstLine != 0) imFirstLine  = ipFirstLine;
	if(ipLeftOffset!= 0) imLeftOffset = ipLeftOffset;

	hmBitmap = NULL;
	hmPalette= NULL;

}

//-----------------------------------------------------------------------------------------------

CCSPrint::~CCSPrint()
{
	if (bmIsInitialized == TRUE)
	{
		omCdc.DeleteDC();
		ThinPen.DeleteObject();
		MediumPen.DeleteObject();
		ThickPen.DeleteObject();
		DottedPen.DeleteObject();
		omRgn.DeleteObject();
	}

	if (hmBitmap)
		::DeleteObject(hmBitmap);
	if (hmPalette)
		::DeleteObject(hmPalette);
}

//-----------------------------------------------------------------------------------------------

void CCSPrint::SetBitmaps(CBitmap *popBitmap, CBitmap *popCcsBitmap)
{
	if(popBitmap != NULL)
	{
		pomBitmap = popBitmap;
	}
	if(popCcsBitmap != NULL)
	{
		pomCcsBitmap = popCcsBitmap;
	}
}

//-----------------------------------------------------------------------------------------------

BOOL CCSPrint::InitializePrinter(int ipOrientation)
{
	int ilRc;

	HDC hlHdc;

	char pclDevices[182];

	::GetProfileString("windows", "device","",pclDevices,180);
	CString olDevices = pclDevices;
	CString olDeviceName;
	int ilKomma = olDevices.Find(',');
	if (ilKomma > -1)
	{
		olDeviceName = olDevices.Left(ilKomma);
	}


	char pclDeviceName[182];
	strcpy(pclDeviceName,olDeviceName);

	HANDLE  hlPrinter = 0;
	static HANDLE hDevMode = 0;
	
	//if (hDevMode == 0)
	{

		if (OpenPrinter(pclDeviceName,&hlPrinter,NULL) != TRUE)
		{

			MessageBox(NULL, GetString(IDS_STRING1806),GetString(ST_FEHLER), MB_ICONERROR | MB_OK | MB_APPLMODAL);


			return FALSE;
		}

		{
		//	if (GetPrinter(hlPrinter,2,(unsigned char *)&rlPrinterInfo,sizeof(rlPrinterInfo),&llBytesReceived) == TRUE)
			{
				LONG llDevModeLen = DocumentProperties(NULL,hlPrinter,pclDeviceName,NULL,NULL,0);
				if (llDevModeLen > 0)
				{
					hDevMode = GlobalAlloc(GMEM_MOVEABLE,llDevModeLen);
					DEVMODE *prlDevMode = (DEVMODE *) GlobalLock(hDevMode);
					DocumentProperties(NULL,hlPrinter,pclDeviceName,prlDevMode,NULL,DM_OUT_BUFFER);
					DWORD dm_orientation =  DM_ORIENTATION;
					prlDevMode->dmFields = DM_ORIENTATION;
					if(ipOrientation == PRINT_LANDSCAPE)
					{
						prlDevMode->dmOrientation = DMORIENT_LANDSCAPE;
					}
					else
					{
						prlDevMode->dmOrientation = DMORIENT_PORTRAIT;
					}
					DocumentProperties(NULL,hlPrinter,pclDeviceName,prlDevMode,prlDevMode,DM_IN_BUFFER|DM_OUT_BUFFER);
					GlobalUnlock(hDevMode);


				}
				ClosePrinter(hlPrinter);
				if (llDevModeLen <= 0)
				{
					int ilLastError = GetLastError();

					LPVOID lpMsgBuf;
					FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, 
					NULL, ilLastError, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
					(LPTSTR) &lpMsgBuf, 0, NULL ); 
 
					MessageBox( NULL, (LPCTSTR)lpMsgBuf, GetString(ST_FEHLER), MB_ICONERROR | MB_OK | MB_APPLMODAL);
 
					LocalFree( lpMsgBuf );

					return false;
				}
			}
		}
	}
   
	
   
  
	imPageNo = 0;

	CPrintDialog *polPrintDialog = new CPrintDialog(
		  FALSE,PD_ALLPAGES|PD_NOPAGENUMS|PD_NOSELECTION|PD_USEDEVMODECOPIESANDCOLLATE,
		  pomParent);

	if (hDevMode != 0)
	{
		polPrintDialog->m_pd.hDevMode = hDevMode;
	}
	LPDEVMODE prlOldDevMode = polPrintDialog->GetDevMode( ); 

//	prlOldDevMode->dmOrientation = 2;

	bgPrintDlgOpen = true;
	ilRc = polPrintDialog->DoModal();
	if (ilRc != IDCANCEL )
	{
		bgPrintDlgOpen = false;
		LPDEVMODE prlDevMode = polPrintDialog->GetDevMode( );
		smPaperSize = prlDevMode->dmPaperSize;
		imPaperLength = prlDevMode->dmPaperLength;
		imPaperWidth = prlDevMode->dmPaperWidth;

		hlHdc = polPrintDialog->GetPrinterDC();
		if (hlHdc == NULL)
		{
			ilRc = FALSE;
			MessageBox(NULL, GetString(IDS_STRING904), GetString(IDS_STRING905), MB_OK | MB_ICONEXCLAMATION);

		}
		else
		{ 
			imPaperLength = GetDeviceCaps(hlHdc, HORZSIZE)*10;
			imPaperWidth = GetDeviceCaps(hlHdc, VERTSIZE)*10;
 		
/*			
				CString olText;
				olText.Format("InitPrinter: imPaperLength %d    imPaperWidth  %d   smPaperSize %d" , imPaperLength, imPaperWidth, smPaperSize);

				if(CFPMSApp::MyTopmostMessageBox(NULL, olText, GetString(ST_FRAGE), MB_ICONQUESTION | MB_YESNO) == IDYES)
				{
				}
*/
			omCdc.Attach(hlHdc);
			//omCdc.SetMapMode(MM_TWIPS);
			//omCdc.SetMapMode(MM_HIMETRIC);
			omCdc.SetMapMode(MM_TEXT);
		

			imLogPixelsY = omCdc.GetDeviceCaps(LOGPIXELSY);
			imLogPixelsX = omCdc.GetDeviceCaps(LOGPIXELSX);

			omRgn.CreateRectRgn(0,0,omCdc.GetDeviceCaps(HORZRES),omCdc.GetDeviceCaps(VERTRES));
			omCdc.SelectClipRgn(&omRgn);

			TRACE("X = %d, Y = %d \n",imLogPixelsX,imLogPixelsY);
			LOGFONT rlLf;
			memset(&rlLf, 0, sizeof(LOGFONT));

			//Courier New 4 
			rlLf.lfHeight = - MulDiv(4, imLogPixelsY, 72);
			rlLf.lfWeight = FW_NORMAL;
			rlLf.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
			lstrcpy(rlLf.lfFaceName, "Courier New");
			ilRc = ogCourierNew_Regular_4.CreateFontIndirect(&rlLf);
			//Courier New 4 Bold
			rlLf.lfWeight = FW_BOLD;
			ilRc = ogCourierNew_Bold_4.CreateFontIndirect(&rlLf);

			//Courier New 5 
			rlLf.lfHeight = - MulDiv(5, imLogPixelsY, 72);
			rlLf.lfWeight = FW_NORMAL;
			rlLf.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
			lstrcpy(rlLf.lfFaceName, "Courier New");
			ilRc = ogCourierNew_Regular_5.CreateFontIndirect(&rlLf);
			//Courier New 5 Bold
			rlLf.lfWeight = FW_BOLD;
			ilRc = ogCourierNew_Bold_5.CreateFontIndirect(&rlLf);

			//Courier New 6 
			rlLf.lfHeight = - MulDiv(6, imLogPixelsY, 72);
			rlLf.lfWeight = FW_NORMAL;
			rlLf.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
			lstrcpy(rlLf.lfFaceName, "Courier New");
			ilRc = ogCourierNew_Regular_6.CreateFontIndirect(&rlLf);
			//Courier New 6 Bold
			rlLf.lfWeight = FW_BOLD;
			ilRc = ogCourierNew_Bold_6.CreateFontIndirect(&rlLf); 

			//Courier New 7 
			rlLf.lfHeight = - MulDiv(7, imLogPixelsY, 72);
			rlLf.lfWeight = FW_NORMAL;
			rlLf.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
			lstrcpy(rlLf.lfFaceName, "Courier New");
			ilRc = ogCourierNew_Regular_7.CreateFontIndirect(&rlLf);
			//Courier New 7 Bold
			rlLf.lfWeight = FW_BOLD;
			ilRc = ogCourierNew_Bold_7.CreateFontIndirect(&rlLf);

			//Courier New 8 
			rlLf.lfHeight = - MulDiv(8, imLogPixelsY, 72);
			rlLf.lfWeight = FW_NORMAL;
			rlLf.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
			lstrcpy(rlLf.lfFaceName, "Courier New");
			ilRc = ogCourierNew_Regular_8.CreateFontIndirect(&rlLf);
			//Courier New 8 Bold
			rlLf.lfWeight = FW_BOLD;
			ilRc = ogCourierNew_Bold_8.CreateFontIndirect(&rlLf);

			//Courier New 11 
			rlLf.lfHeight = - MulDiv(11, imLogPixelsY, 72);
			rlLf.lfWeight = FW_NORMAL;
			rlLf.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
			lstrcpy(rlLf.lfFaceName, "Courier New");
			ilRc = ogCourierNew_Regular_11.CreateFontIndirect(&rlLf);
			//Courier New 11 Bold
			rlLf.lfWeight = FW_BOLD;
			ilRc = ogCourierNew_Bold_11.CreateFontIndirect(&rlLf);

			//Courier New 12 
			rlLf.lfHeight = - MulDiv(12, imLogPixelsY, 72);
			rlLf.lfWeight = FW_NORMAL;
			rlLf.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
			lstrcpy(rlLf.lfFaceName, "Courier New");
			ilRc = ogCourierNew_Regular_12.CreateFontIndirect(&rlLf);
			//Courier New 12 Bold
			rlLf.lfWeight = FW_BOLD;
			ilRc = ogCourierNew_Bold_12.CreateFontIndirect(&rlLf);
			
			//Arial 8
			rlLf.lfWeight = FW_NORMAL;
			rlLf.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
			lstrcpy(rlLf.lfFaceName, "Arial");
			ilRc = omSmallFont_Regular.CreateFontIndirect(&rlLf);
			// MCU 29.09.98 create small font for gantt chart only
			rlLf.lfHeight = - MulDiv(7, imLogPixelsY, 72);
//			rlLf.lfWeight = FW_NORMAL;
			rlLf.lfWeight = FW_BOLD;
			rlLf.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
			lstrcpy(rlLf.lfFaceName, "Arial");
			ilRc = omSmallFont_RegularForGantt.CreateFontIndirect(&rlLf);

			//Arial 12
			rlLf.lfHeight = - MulDiv(12, imLogPixelsY, 72); 
			ilRc = omMediumFont_Regular.CreateFontIndirect(&rlLf);
			//Arial 18
			rlLf.lfHeight =  - MulDiv(18, imLogPixelsY, 72);   
			ilRc = omLargeFont_Regular.CreateFontIndirect(&rlLf);
			//Arial 8 Bold
			rlLf.lfHeight = - MulDiv(8, imLogPixelsY, 72);
			rlLf.lfWeight = FW_BOLD;
			ilRc = omSmallFont_Bold.CreateFontIndirect(&rlLf);
			//Arial 12 Bold
			rlLf.lfHeight = - MulDiv(12, imLogPixelsY, 72); 
			ilRc = omMediumFont_Bold.CreateFontIndirect(&rlLf);
			//Arial 18 Bold
			rlLf.lfHeight =  - MulDiv(18, imLogPixelsY, 72);   
			ilRc = omLargeFont_Bold.CreateFontIndirect(&rlLf);
			///////////////

			rlLf.lfHeight = - MulDiv(10, imLogPixelsY, 72);
			rlLf.lfPitchAndFamily = 2;
			rlLf.lfCharSet = SYMBOL_CHARSET;
			rlLf.lfWeight = 400;
			lstrcpy(rlLf.lfFaceName, "ccs");
			ilRc = omCCSFont.CreateFontIndirect(&rlLf);

			ThinPen.CreatePen(PS_SOLID, MulDiv(2, imLogPixelsX, 254), RGB(0,0,0));
			MediumPen.CreatePen(PS_SOLID, MulDiv(4, imLogPixelsX, 254), RGB(0,0,0));
			ThickPen.CreatePen(PS_SOLID, MulDiv(8, imLogPixelsX, 254), RGB(0,0,0));
			DottedPen.CreatePen(PS_DOT,1, RGB(0,0,0));
			delete polPrintDialog;

			// loading the header bitmap
			// loading of the header bitmap from ceda.ini defined resource
			char pclTmpText[512];
			char pclConfigPath[512];
			if (getenv("CEDA") == NULL)
				strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
			else
				strcpy(pclConfigPath, getenv("CEDA"));

			GetPrivateProfileString(ogAppName, "LOGO","",
				pclTmpText, sizeof pclTmpText, pclConfigPath);

			// loading the header bitmap
			if (!LoadBitmapFromBMPFile(pclTmpText,&hmBitmap,&hmPalette))
			{
				hmBitmap = NULL;
				hmPalette= NULL;
				int iRet= omBitmap.LoadBitmap(IDB_UFISLOGO);
				omMemDc.CreateCompatibleDC(&omCdc);
				omMemDc.SelectObject(omBitmap);
			}




/*/###########
			int iRet= omBitmap.LoadBitmap(IDB_UFISLOGO);
			omMemDc.CreateCompatibleDC(&omCdc);
			omMemDc.SelectObject(omBitmap);
*/


			bmIsInitialized = TRUE;

			TEXTMETRIC olTm;

			omCdc.GetTextMetrics(&olTm);
			int  ilHeight = olTm.tmHeight + olTm.tmExternalLeading;

			// calculate vertical ofset of text in a bar
			CFont *polOldFont = omCdc.SelectObject(&omSmallFont_RegularForGantt);
			CSize olSize = omCdc.GetTextExtent("LH 4711");
//rkrpos
			if (!bmViewBigBobs)
			{
				imBarHeight = (olTm.tmHeight + olTm.tmExternalLeading);
				imBarVerticalTextOffset = (int) ((imBarHeight-olSize.cy)/2);
			}
			else
			{
				imBarHeight = (olTm.tmHeight + olTm.tmExternalLeading)*4;
				imBarVerticalTextOffset = (int) ((imBarHeight/4-olSize.cy)/2);
			}

			imGanttLineHeight = (int) (imBarHeight * 1.2);

			// calculate vertical ofset of text in a bar
//			CFont *polOldFont = omCdc.SelectObject(&omSmallFont_RegularForGantt);
//			CSize olSize = omCdc.GetTextExtent("LH 4711");
//			imBarVerticalTextOffset = (int) ((imBarHeight-olSize.cy)/2);

			omCdc.SelectObject(polOldFont);
		}
	}
	else
		bgPrintDlgOpen = false;

	return ilRc;
}

//-----------------------------------------------------------------------------------------------

BOOL CCSPrint::PrintHeader()
{
	PrintHeader(omHeader[0],omHeader[1],omHeader[2],omHeader[3]);
	return TRUE;
}

//-----------------------------------------------------------------------------------------------

BOOL CCSPrint::PrintHeader(CString opHeader1,CString opHeader2,CString opHeader3,CString opHeader4)
{
	CFont *polOldFont;
	CPen  *polOldPen; 
	imPageNo++;
	imLineNo = 0;
//	BOOL  ok = omCdc.StretchBlt(MMX(100),MMY(100),MMX(270),MMY(270),&omMemDc,0,0,519,519, SRCCOPY);
//	BOOL  ok = omCdc.StretchBlt(MMX(100),MMY(100),MMX(270),MMY(270),&omMemDc,0,0,MMX(462),MMY(366), SRCCOPY);

	if (hmBitmap)	// user defined logo bitmap available ?
	{
		HBITMAP  hlOldBitmap;
		HPALETTE hlOldPalette;

		BITMAP rlBitmap;
		HDC	 hlMemDC;

		::GetObject(hmBitmap,sizeof(BITMAP),&rlBitmap);	
		hlMemDC = ::CreateCompatibleDC(omCdc.m_hDC);
		hlOldBitmap = (HBITMAP)::SelectObject(hlMemDC,hmBitmap);
		hlOldPalette= ::SelectPalette(omCdc.m_hDC,hmPalette,FALSE);
		::RealizePalette(omCdc.m_hDC);
		CSize rlSize(rlBitmap.bmWidth,rlBitmap.bmHeight);
		omCdc.DPtoLP(&rlSize);
		BOOL bResult = ::StretchBlt(omCdc.m_hDC,MMX(100),MMY(100),MMX(270),MMY(270),hlMemDC,0,0,rlSize.cx,rlSize.cy,SRCCOPY);
		::SelectObject(hlMemDC,hlOldBitmap);
		::SelectPalette(omCdc.m_hDC,hlOldPalette,FALSE);
	}
	else
		omCdc.StretchBlt(MMX(100),MMY(100),MMX(270),MMY(270),&omMemDc,
		0,0,519,519, SRCCOPY);





	polOldPen = omCdc.SelectObject(&ThinPen);

//	omCdc.Rectangle(MMX(400),MMY(150),MMX(2800),MMY(330));
	omCdc.Rectangle(MMX(400),MMY(150),MMX(imPaperLength-200),MMY(330));
    omCdc.MoveTo(MMX(420),MMY(250));
//    omCdc.LineTo(MMX(2400),MMY(250));
    omCdc.LineTo(MMX(imPaperLength-600),MMY(250));

    polOldFont = omCdc.SelectObject(&omLargeFont_Regular);
	omCdc.TextOut(MMX(430),MMY(170), opHeader1, strlen(opHeader1) );

    omCdc.SelectObject(&omSmallFont_Bold);
	omCdc.TextOut(MMX(2480),MMY(210), opHeader2, strlen(opHeader2) );

    omCdc.SelectObject(&omMediumFont_Regular);
	omCdc.TextOut(MMX(430),MMY(270), opHeader3, strlen(opHeader3) );


	omCdc.SelectObject(polOldFont);
	omCdc.SelectObject(polOldPen);

	return TRUE;
}

//-----------------------------------------------------------------------------------------------

BOOL CCSPrint::PrintFooter()
{
	return TRUE;
}

//-----------------------------------------------------------------------------------------------

BOOL CCSPrint::PrintFooter(CString opFooter1,CString opFooter2)
{

	//CFont *polOldFont;
	CPen  *polOldPen; 
	int ilXOffset = MMX(200);
	int ilYOffset = omCdc.GetDeviceCaps(VERTRES)-MMY(50);
	CSize olSize;

    polOldPen = omCdc.SelectObject(&ThinPen);
	
    omCdc.MoveTo(MMX(180),ilYOffset-MMY(50));
//    omCdc.LineTo(MMX(2850),ilYOffset-MMY(50));
    omCdc.LineTo(MMX(imPaperLength-150),ilYOffset-MMY(50));

	ilXOffset += 200;

    omCdc.SelectObject(&omSmallFont_Regular);
	olSize = omCdc.GetTextExtent(opFooter1);
	omCdc.TextOut(ilXOffset,ilYOffset-olSize.cy,opFooter1);
 	ilXOffset += olSize.cx;

    omCdc.SelectObject(&omSmallFont_Bold);
	olSize = omCdc.GetTextExtent(opFooter2);
	omCdc.TextOut(ilXOffset,ilYOffset-olSize.cy+5,opFooter2);


// 	ilXOffset = MMX(2700);
 	ilXOffset = MMX(imPaperLength-300);
    omCdc.SelectObject(&omSmallFont_Regular);
	olSize = omCdc.GetTextExtent(GetString(IDS_STRING1197));
	omCdc.TextOut(ilXOffset,ilYOffset-olSize.cy,GetString(IDS_STRING1197),7);
 	ilXOffset += olSize.cx;

	char olPageNo[24];
	sprintf(olPageNo,"%3d",imPageNo);
    omCdc.SelectObject(&omSmallFont_Bold);
	olSize = omCdc.GetTextExtent(olPageNo);
	omCdc.TextOut(ilXOffset,ilYOffset-olSize.cy,olPageNo,strlen(olPageNo));


	//omCdc.SelectObject(polOldFont);
	omCdc.SelectObject(polOldPen);


	return TRUE;
}

//-----------------------------------------------------------------------------------------------

BOOL CCSPrint::SelectFramePen(int ipFrameType)
{
	int ilRc = TRUE; 
	switch (ipFrameType)
	{
	case PRINT_FRAMETHIN: 
		omCdc.SelectObject(ThinPen);
		break;
	case PRINT_FRAMEMEDIUM: 
		omCdc.SelectObject(MediumPen);
		break;
	case PRINT_FRAMETHICK: 
		omCdc.SelectObject(ThickPen);
		break;
	default: ilRc = FALSE;

	}
	return ilRc;
}

//-----------------------------------------------------------------------------------------------

void CCSPrint::PrintLeft(CRect opRect,CString opText)
{

	TEXTMETRIC olTm;

	// insert a small space at the left side
	opRect.left += MMX(10);
	omCdc.GetTextMetrics(&olTm);
	int  ilHeight = olTm.tmHeight + olTm.tmExternalLeading;
	omCdc.TextOut(opRect.left,opRect.bottom-ilHeight,opText);
	
}

//-----------------------------------------------------------------------------------------------

void CCSPrint::PrintRight(CRect opRect,CString opText)
{

	CSize olSize = omCdc.GetTextExtent(opText);

	omCdc.TextOut(opRect.right-olSize.cx,opRect.bottom-olSize.cy,opText);
	
}

//-----------------------------------------------------------------------------------------------

void CCSPrint::PrintCenter(CRect opRect,CString opText)
{

	CSize olSize = omCdc.GetTextExtent(opText);
	
	int ilOffset = max(0,(int)(opRect.right-opRect.left-olSize.cx)/2);
	omCdc.TextOut(opRect.left+ilOffset,opRect.bottom-olSize.cy,opText);
	
}

//-----------------------------------------------------------------------------------------------

BOOL CCSPrint::PrintLine(CCSPtrArray <PRINTELEDATA> &ropPrintLine)
{
	CPen *polOldPen;
	
	int ilLeftPos = imLeftOffset;
	int ilLineTop = imFirstLine + (imLineHeight*imLineNo);

	polOldPen = omCdc.SelectObject(&ThinPen);

	for(int i = 0; i < ropPrintLine.GetSize(); i++)
	{
		PRINTELEDATA *prlEle = &ropPrintLine[i];
		
		int ilLineOffset = imLineNo * imLineHeight;

		if (SelectFramePen(prlEle->FrameLeft) == TRUE)
		{
			omCdc.MoveTo(MMX(ilLeftPos),MMY(ilLineTop));
			omCdc.LineTo(MMX(ilLeftPos),MMY(ilLineTop+imLineHeight));
		}
		if (SelectFramePen(prlEle->FrameTop) == TRUE)
		{
			omCdc.MoveTo(MMX(ilLeftPos),MMY(ilLineTop));
			omCdc.LineTo(MMX(ilLeftPos+prlEle->Length),MMY(ilLineTop));
		}
		if (SelectFramePen(prlEle->FrameBottom) == TRUE)
		{
			omCdc.MoveTo(MMX(ilLeftPos),MMY(ilLineTop+imLineHeight));
			omCdc.LineTo(MMX(ilLeftPos+prlEle->Length),MMY(ilLineTop+imLineHeight));
		}
		if (SelectFramePen(prlEle->FrameRight) == TRUE)
		{
			omCdc.MoveTo(MMX(ilLeftPos+prlEle->Length),MMY(ilLineTop));
			omCdc.LineTo(MMX(ilLeftPos+prlEle->Length),MMY(ilLineTop+imLineHeight));
		}
		if (prlEle->Text.IsEmpty() == FALSE)
		{
			CRgn rlRgn;

			rlRgn.CreateRectRgn(MMX(ilLeftPos),MMY(ilLineTop+2),
				MMX(ilLeftPos+prlEle->Length),MMY(ilLineTop+imLineHeight-2));
			omCdc.SelectClipRgn(&rlRgn);

			CFont *polOldFont = omCdc.SelectObject(prlEle->pFont);
			CRect olRect(MMX(ilLeftPos+2),MMY(ilLineTop+2),
				MMX(ilLeftPos+prlEle->Length-2),MMY(ilLineTop+imLineHeight-2));
			switch (prlEle->Alignment)
			{
			case PRINT_LEFT: PrintLeft(olRect,prlEle->Text);
				break;
			case PRINT_RIGHT: PrintRight(olRect,prlEle->Text);
				break;
			case PRINT_CENTER: PrintCenter(olRect,prlEle->Text);
				break;
			}
			omCdc.SelectClipRgn(&omRgn);
			rlRgn.DeleteObject();
		}
		ilLeftPos += prlEle->Length;

	}
	ropPrintLine.DeleteAll();
	imLineNo++;
	return TRUE;
}

//-----------------------------------------------------------------------------------------------

BOOL CCSPrint::PrintTimeScale(int ipStartX,int ipEndX,CTime opStartTime,CTime opEndTime,CString opText)
{
	imYOffset = MMY(imFirstLine);
	CFont *polTextFont;
	CPen *polPen;
	int ilSteps = 0;
	int ilPixels = 0;
	int ilHourPixels = 0;
	int i;
	

	int ilTsHeight;
	int ilLongLine;
	int ilMediumLine;
	int ilShortLine;
	int ilStartX = 0;
	int ilStartY = 0;
	int ilEndX = 0;
	int ilEndY = 0; 
	int ilActX = 0;
	int ilActY = 0;
	int ilTextStartY;


	ilStartX = MMX(ipStartX);
	ilEndX   = MMX(ipEndX);
	ilActX   = ilStartX;

	imGanttStartX = ilStartX;

/*
		CString olText;
		olText.Format("ipStartX %d  ipEndX %d  ilStartX %d   ilEndX %d   ilActX %d   imGanttStartX %d" ,ipStartX,ipEndX,ilStartX,ilEndX,ilActX, imGanttEndX);

		if(CFPMSApp::MyTopmostMessageBox(NULL, olText, GetString(ST_FRAGE), MB_ICONQUESTION | MB_YESNO) == IDYES)
		{
		}

*/

	CPen  *polOldPen  = omCdc.SelectObject(&MediumPen);
	CFont *polOldFont = omCdc.SelectObject(&omSmallFont_Bold);
    
	//  rounding to full hour
    CTime olActHour(opStartTime.GetYear(),opStartTime.GetMonth(),opStartTime.GetDay(),
		opStartTime.GetHour(),0,0);
	//  add one hour if we are in the second half of the actual hour
	if (opStartTime.GetMinute() > 30)
	{
		olActHour += CTimeSpan(0,1,0,0);
	}
	omStartTime = olActHour; 
    CTimeSpan olDuration = opEndTime - omStartTime;
	int ilHours = olDuration.GetTotalHours();
	omEndTime = omStartTime + CTimeSpan(0,ilHours,0,0);
    
	ilSteps = ilHours * 6;
	ilHourPixels = (ilEndX - ilStartX) / ilHours;
	ilPixels = (int) ((int)ilHourPixels / 0.6);
	if ((ilPixels % 10) > 5)
	{
		ilPixels += 10;
	}
	ilPixels = (int) ilPixels / 10;

	if (ilHours < 9)
	{
		ilTsHeight = MMY(100);
		ilLongLine = MMY(35);
		ilMediumLine = MMY(60);
		ilShortLine  = MMY(75);
		ilStartY = imYOffset;
		ilEndY   = ilStartY+ilTsHeight; 
		ilActY   = ilStartY;
		ilTextStartY = imYOffset - MMY(20);
		polTextFont = &omMediumFont_Bold;
		polPen = &MediumPen;
	}
	else
	{
		ilTsHeight = MMY(83);
		ilLongLine = MMY(45);
		ilMediumLine = MMY(55);
		ilShortLine  = MMY(70);
		ilStartY = imYOffset;
		ilEndY   = ilStartY+ilTsHeight; 
		ilActY   = ilStartY;
		ilTextStartY = imYOffset+MMY(10);
		polTextFont = &omSmallFont_Bold;
		polPen = &ThinPen;
	}

	// save the Y position of timescale
	imGanttStartY = ilEndY;

	// first draw a full height line  with medium pen
	omCdc.MoveTo(ilStartX,ilStartY);
	omCdc.LineTo(ilStartX,ilEndY);

	// set the pen for the timescale lines
	omCdc.SelectObject(polPen);

	for ( i = 1; i  < ilSteps; i++)
	{
		ilActX += ilPixels;
/*
		olText.Format("step  %d   ilActX %d   ilPixels %d" , i, ilActX, ilPixels);

		if(CFPMSApp::MyTopmostMessageBox(NULL, olText, GetString(ST_FRAGE), MB_ICONQUESTION | MB_YESNO) == IDYES)
		{
		}
*/
		if ( i % 6 == 0)
		{
			// draw long line every hour
			omCdc.MoveTo(ilActX,ilEndY);
			omCdc.LineTo(ilActX,ilStartY+ilLongLine);
			
		}
		else
		{
			if ( i % 3 == 0)
			{
				// draw medium line every half hour
				omCdc.MoveTo(ilActX,ilEndY);
				omCdc.LineTo(ilActX,ilStartY+ilMediumLine);
			}
			else
			{
				// draw short line every 15 minutes
				omCdc.MoveTo(ilActX,ilEndY);
				omCdc.LineTo(ilActX,ilStartY+ilShortLine);
			}
		}
	}
	ilActX += ilPixels;

	// new end of gantt chart
	imGanttEndX   = ilActX;
/*
//		CString olText;
		olText.Format("Timescale_Right %d    ilActX  %d" , imGanttEndX, ilActX);

		if(CFPMSApp::MyTopmostMessageBox(NULL, olText, GetString(ST_FRAGE), MB_ICONQUESTION | MB_YESNO) == IDYES)
		{
		}


*/

	// calculate the factor for GetXFromTime
    int ilTotalMin = ilHours*60;
	int ilTotalPixels = imGanttEndX-imGanttStartX;
	dmXFactor = (double) ilTotalPixels / ilTotalMin;

	// at the end draw another full height line with medium pen
	omCdc.SelectObject(&MediumPen);
	omCdc.MoveTo(imGanttEndX,ilStartY);
	omCdc.LineTo(imGanttEndX,ilEndY);
	
	// underline the whole timescale
	omCdc.MoveTo(imLeftOffset,ilEndY);
	omCdc.LineTo(imGanttEndX,ilEndY);


	// write headers every hour
	polOldFont = omCdc.SelectObject(polTextFont);
	ilActX = ilStartX;
	for ( i = 1; i < ilHours; i++)
	{
		ilActX += ilHourPixels;
		olActHour += CTimeSpan(0,1,0,0);
		CString olText = olActHour.Format("%H:%M");
		CSize olSize = omCdc.GetTextExtent(olText);
		int ilOffset = (int) olSize.cx / 2;
		omCdc.TextOut(ilActX-ilOffset,ilTextStartY,olText);
	}

	
	if (opText.IsEmpty() == FALSE)
	{
		// print chart name left of timescale
		omCdc.SelectObject(&omMediumFont_Bold);
		CSize olSize = omCdc.GetTextExtent(opText);
		if (olSize.cx > (imGanttStartX-imLeftOffset-MMX(20)))
		{
			omCdc.TextOut(imGanttStartX-olSize.cx-MMX(10),imGanttStartY-olSize.cy-MMY(15),opText);
		}
		else
		{
			// there is space enough, we can center the text
			int ilOffset = (int) ((imGanttStartX-imLeftOffset-olSize.cx) / 2);
			omCdc.TextOut(imLeftOffset+ilOffset,imGanttStartY-olSize.cy-MMY(15),opText);
		}
	}
	imYOffset = ilEndY  + MMY(10);
    
    omCdc.SelectObject(polOldPen);
    omCdc.SelectObject(polOldFont);
	return TRUE;
}

//-----------------------------------------------------------------------------------------------

BOOL CCSPrint::PrintGanttHeader(int ipStartX,int ipEndX,CString opText)
{
	CFont *polTextFont;
	CPen *polPen;

	int ilTsHeight;
	int ilStartX;
	int ilStartY;
	int ilEndX;
	int ilEndY; 
	int ilActX;
	int ilActY;
	int ilTextStartY;


	ilStartX = MMX(ipStartX);
	ilEndX   = MMX(ipEndX);
	ilActX   = ilStartX;

	imGanttStartX = ilStartX;

	CPen  *polOldPen  = omCdc.SelectObject(&MediumPen);
	CFont *polOldFont = omCdc.SelectObject(&omSmallFont_Bold);
    
	ilTsHeight = MMY(83);
	ilStartY = imYOffset;
	ilEndY   = ilStartY+ilTsHeight; 
	ilActY   = ilStartY;
	ilTextStartY = imYOffset;
	polTextFont = &omSmallFont_Bold;
	polPen = &ThinPen;

	// save the Y position of timescale
	imGanttStartY = ilEndY;

	// first draw a full height line  with medium pen
	omCdc.MoveTo(ilStartX,ilStartY);
	omCdc.LineTo(ilStartX,ilEndY);

	// at the end draw another full height line with medium pen
	omCdc.MoveTo(imGanttEndX,ilStartY);
	omCdc.LineTo(imGanttEndX,ilEndY);
	
	// underline the whole timescale
	omCdc.MoveTo(imLeftOffset,ilEndY);
	omCdc.LineTo(imGanttEndX,ilEndY);

	
	if (opText.IsEmpty() == FALSE)
	{
		// print chart name left of timescale
		omCdc.SelectObject(&omMediumFont_Bold);
		CSize olSize = omCdc.GetTextExtent(opText);
		if (olSize.cx > (imGanttStartX-imLeftOffset-MMX(20)))
		{
			omCdc.TextOut(imGanttStartX-olSize.cx-MMX(10),imGanttStartY-olSize.cy-MMY(15),opText);
		}
		else
		{
			// there is space enough, we can center the text
			int ilOffset = (int) ((imGanttStartX-imLeftOffset-olSize.cx) / 2);
			omCdc.TextOut(imLeftOffset+ilOffset,imGanttStartY-olSize.cy-MMY(15),opText);
		}
	}
	imYOffset = ilEndY  + MMY(10);
    
    omCdc.SelectObject(polOldPen);
    omCdc.SelectObject(polOldFont);
	return TRUE;
}

//-----------------------------------------------------------------------------------------------

static int ComparePrintBar(const PRINTBARDATA **e1, const PRINTBARDATA **e2)
{
	return (int)((**e1).StartTime.GetTime() - (**e2).StartTime.GetTime());
}

//-----------------------------------------------------------------------------------------------

BOOL CCSPrint::PrintGanttLine(CString opText,CCSPtrArray<PRINTBARDATA> &ropPrintLine,
							  CCSPtrArray<PRINTBARDATA> &ropBkBars)
{
	int i,j,k;
	BOOL blFound;
	BOOL blDontOverlap;
	CPen *polOldPen;

	// first calculate max overlaplevel and line height
	ropPrintLine.Sort(ComparePrintBar); // sort for time
	// calculate overlaplevel
	int ilBarCount = ropPrintLine.GetSize();
	int ilMaxOverlapLevel = 0;
	// start the loop with 1, the first bar can't be overlapped
	for ( i = 0; i < ilBarCount; i++)
	{
		ropPrintLine[i].OverlapLevel=0;
	}
	for ( i = 1; i < ilBarCount; i++)
	{
//		if (ropPrintLine[i-1].EndTime > ropPrintLine[i].StartTime )
		if (IsReallyOverlapped(ropPrintLine[i-1].StartTime,ropPrintLine[i-1].EndTime,
								ropPrintLine[i].StartTime,ropPrintLine[i].EndTime))
		{
			// found an overlapping bar, check if we can it display at lower overlap level
			
			blDontOverlap = FALSE;
			for (j = 0; (j <= ilMaxOverlapLevel) && (blDontOverlap == FALSE); j++)
			{
				blFound = FALSE;
				for( k = 0; (k < ilBarCount) && (blFound == FALSE) && (k < i); k++)
				{
					if (ropPrintLine[k].OverlapLevel == j)
					{
						if (ropPrintLine[k].EndTime >= ropPrintLine[i].StartTime)
						{
							blFound = TRUE;
						}
					}
				}
				if (blFound == FALSE)
				{
					// no overlapping at this line, so we can use it
					ropPrintLine[i].OverlapLevel = j;
					blDontOverlap = TRUE;
				}
			}
			if (blDontOverlap == FALSE)
			{
				// we have not found a free line, extend overlaplevel
				ilMaxOverlapLevel++;
				ropPrintLine[i].OverlapLevel = ilMaxOverlapLevel;
			}
		}
	}

	ilMaxOverlapLevel++;  
	int ilHeightOfThisLine = ilMaxOverlapLevel * imGanttLineHeight;
	if ((imYOffset + ilHeightOfThisLine) < (omCdc.GetDeviceCaps(VERTRES)-MMY(200)))
	{
		// this line fit on page
		// now we know all overlaplevels and start printing bars
		CRgn rlRgn;

		rlRgn.CreateRectRgn(imGanttStartX,imGanttStartY,imGanttEndX,omCdc.GetDeviceCaps(VERTRES)-MMY(100));
		omCdc.SelectClipRgn(&rlRgn);

		// print background bars first
		for ( i = 0; i < ropBkBars.GetSize(); i++)
		{
			PrintBkBar(ropBkBars[i],ilHeightOfThisLine);
		}

		for ( i = 0; i < ilBarCount; i++)
		{
			PrintGanttBar(ropPrintLine[i]);
		}

		omCdc.SelectClipRgn(&omRgn);
		rlRgn.DeleteObject();

		int ilNewYOffset = imYOffset + ((ilMaxOverlapLevel) * imGanttLineHeight);


		// also print the line text in vertical scale

		rlRgn.CreateRectRgn(MMX(imLeftOffset),imYOffset,imGanttStartX-MMX(10),ilNewYOffset);
		omCdc.SelectClipRgn(&rlRgn);
		CFont *polOldFont = omCdc.SelectObject(&omSmallFont_Bold);
		CSize olSize = omCdc.GetTextExtent(opText);
		int ilTextOffset = ((ilNewYOffset - imYOffset) / 2) - (olSize.cy/2);
		omCdc.TextOut(MMX(imLeftOffset+4),imYOffset+ilTextOffset, opText);
		omCdc.SelectObject(polOldFont);
		omCdc.SelectClipRgn(&omRgn);
		rlRgn.DeleteObject();


		// draw a horizontal line between gate lines
		// RST polOldPen = omCdc.SelectObject(&DottedPen);
		polOldPen = omCdc.SelectObject(&MediumPen);
		omCdc.MoveTo(MMX(imLeftOffset),ilNewYOffset);
		omCdc.LineTo(imGanttEndX,ilNewYOffset);
		omCdc.SelectObject(polOldPen);
/*
		CString olText;
		olText.Format("Left %d    Right %d" , imLeftOffset,imGanttEndX);

		if(CFPMSApp::MyTopmostMessageBox(NULL, olText, GetString(ST_FRAGE), MB_ICONQUESTION | MB_YESNO) == IDYES)
		{
		}


*/

		// save new Y position 
		imYOffset = ilNewYOffset + MMY(10);
		imGanttEndY = imYOffset;

		ropPrintLine.DeleteAll();
		ropBkBars.DeleteAll();
		
		imLineNo++;
		return TRUE;
	}
	// line don't fit on page
	return FALSE;
}

//-----------------------------------------------------------------------------------------------

BOOL CCSPrint::PrintGanttBottom(void)
{
	CPen *polOldPen = omCdc.SelectObject(&MediumPen);
	// rectangle around gantt chart
    omCdc.MoveTo(imGanttStartX,imGanttStartY);
    omCdc.LineTo(imGanttStartX,imGanttEndY);
    omCdc.LineTo(imGanttEndX,imGanttEndY);
    omCdc.LineTo(imGanttEndX,imGanttStartY);
	
	// line left of vertical scale
    omCdc.MoveTo(imLeftOffset,imGanttStartY);
    omCdc.LineTo(imLeftOffset,imGanttEndY);
    omCdc.LineTo(imGanttStartX,imGanttEndY);

	omCdc.SelectObject(polOldPen);

	imYOffset = imGanttEndY + MMY(40);
	return TRUE;
}

//-----------------------------------------------------------------------------------------------

int CCSPrint::GetXFromTime(CTime opTime)
{
    long llMinutes = (opTime - omStartTime).GetTotalMinutes();
	return (int) ((llMinutes * dmXFactor) + imGanttStartX);

}

//-----------------------------------------------------------------------------------------------

BOOL CCSPrint::PrintGanttBar(PRINTBARDATA &ropBar)
{
	int ilStartX = GetXFromTime(ropBar.StartTime);
	int ilEndX   = GetXFromTime(ropBar.EndTime);
	int ilStartY = imYOffset + (ropBar.OverlapLevel * imGanttLineHeight);
	int ilEndY   = ilStartY + imBarHeight;

    CRect rcBackground(ilStartX,ilStartY,ilEndX,ilEndY); // for drawing bar background
    CRect rcMarker(rcBackground);     // for drawing bar marker
    CRect rcTriMarker(rcBackground);     // for drawing bar marker
    CRect rcBar(rcBackground);        // for drawing frame and text
    CBrush brWhite(RGB(255, 255, 255));
    CBrush brBlack(RGB(0,0,0));

	CBrush brMarkerBrush(RGB(220,220,220));
    CRect rcMarkerLeft(rcBackground);     // for drawing bar marker
    CRect rcMarkerRight(rcBackground);     // for drawing bar marker

	int halfLength = (rcBackground.right - rcBackground.left)/2;
    // draw bar background and marker
    switch (ropBar.MarkerType)
    {
    case MARKNONE:
        rcBackground.right++;   // extra pixel to help FillRect() work correctly
        omCdc.FillRect(&rcBackground, &brWhite);
        break;
    case MARKLEFT:
        rcMarker.right = (rcMarker.left + rcMarker.right) / 2;
        rcMarker.right++;       // extra pixel to help FillRect() work correctly
        omCdc.FillRect(&rcMarker, &brMarkerBrush);
        rcBackground.left = rcMarker.right;
        rcBackground.right++;   // extra pixel to help FillRect() work correctly
        omCdc.FillRect(&rcBackground, &brWhite);
        break;
    case MARKRIGHT:
        rcBackground.right = (rcBackground.left + rcBackground.right) / 2;
        rcBackground.right++;   // extra pixel to help FillRect() work correctly
        omCdc.FillRect(&rcBackground, &brWhite);
        rcMarker.left = rcBackground.right;
        rcMarker.right++;       // extra pixel to help FillRect() work correctly
        omCdc.FillRect(&rcMarker, &brMarkerBrush);
        break;
    case MARKFULL:
        rcMarker.right++;       // extra pixel to help FillRect() work correctly

		rcMarkerLeft.right -= halfLength;
		rcMarkerRight.left += halfLength;

		if( ropBar.MarkerBrush != NULL)
			omCdc.FillRect(&rcMarkerLeft, ropBar.MarkerBrush);
		else
			omCdc.FillRect(&rcMarkerLeft, &brMarkerBrush);

		if( ropBar.MarkerBrushRightSide != NULL)
			omCdc.FillRect(&rcMarkerRight, ropBar.MarkerBrushRightSide);
		else
		{
			if( ropBar.MarkerBrush != NULL)
				omCdc.FillRect(&rcMarkerRight, ropBar.MarkerBrush);
			else
				omCdc.FillRect(&rcMarkerRight, &brMarkerBrush);
		}

        break;
    }

	//create the triangles
//rkrpos
	int height = rcBackground.bottom - rcBackground.top;
	if (bmViewBigBobs)
		height = height/4;

	CPoint olPoints[3];
	olPoints[0].x = rcTriMarker.left;
	olPoints[0].y = rcTriMarker.top;

	olPoints[1].x = rcTriMarker.left; 
//	olPoints[1].y = rcTriMarker.bottom;
	olPoints[1].y = rcTriMarker.top + height;
	
	olPoints[2].x = rcTriMarker.left + height;
	olPoints[2].y = rcTriMarker.top;
	CRgn* polRgnLeft;
	polRgnLeft = new CRgn;
	polRgnLeft->CreatePolygonRgn( olPoints, 3,  WINDING);


	olPoints[0].x = rcTriMarker.right - height;
	olPoints[0].y = rcTriMarker.bottom;

	olPoints[1].x = rcTriMarker.right; 
	olPoints[1].y = rcTriMarker.bottom;
	
	olPoints[2].x = rcTriMarker.right;
//
	olPoints[2].y = rcTriMarker.bottom - height;

	CRgn* polRgnRight;
	polRgnRight = new CRgn;
	polRgnRight->CreatePolygonRgn( olPoints, 3,  WINDING);

    switch (ropBar.MarkerTri)
    {
	case TRIANGLE_BOTH:

		if( ropBar.BrushTriangleLeft != NULL)
			omCdc.FillRgn(polRgnLeft, ropBar.BrushTriangleLeft);
		else
			omCdc.FillRgn(polRgnLeft, &brBlack);

		if( ropBar.BrushTriangleRight != NULL)
			omCdc.FillRgn(polRgnRight, ropBar.BrushTriangleRight);
		else
			omCdc.FillRgn(polRgnRight, &brBlack);

		break;
    case TRIANGLE_LEFT:
		if( ropBar.BrushTriangleLeft != NULL)
			omCdc.FillRgn(polRgnLeft, ropBar.BrushTriangleLeft);
		else
			omCdc.FillRgn(polRgnLeft, &brBlack);

		break;
    case TRIANGLE_RIGHT:
		if( ropBar.BrushTriangleRight != NULL)
			omCdc.FillRgn(polRgnRight, ropBar.BrushTriangleRight);
		else
			omCdc.FillRgn(polRgnRight, &brBlack);

		break;
	}

	polRgnLeft->DeleteObject();
	delete polRgnLeft;
	polRgnRight->DeleteObject();
	delete polRgnRight;


    // extra pixel to help ExtTextOut() work correctly
    rcBar.right++;

    // draw bar text (with clipping)
	CPen  *polOldPen  = omCdc.SelectObject(&ThickPen);
	COLORREF olOldTextCol = omCdc.GetTextColor();

	if (	ropBar.MarkerBrush == ogBrushs[IDX_TOWING]
		||	ropBar.MarkerBrush == ogBrushs[IDX_GRAY]
		||	ropBar.MarkerBrush == ogBrushs[IDX_BLACK]
		||	ropBar.MarkerBrush == ogBrushs[IDX_RED]
		||	ropBar.MarkerBrush == ogBrushs[IDX_GREEN]
		||	ropBar.MarkerBrush == ogBrushs[IDX_BLUE]
		||	ropBar.MarkerBrush == ogBrushs[IDX_TEAL]
		||	ropBar.MarkerBrush == ogBrushs[IDX_MAROON]
		||	ropBar.MarkerBrush == ogBrushs[IDX_OLIVE]
		||	ropBar.MarkerBrush == ogBrushs[IDX_NAVY]
		||	ropBar.MarkerBrush == ogBrushs[IDX_PURPLE]
		)
	{
		omCdc.SetTextColor(RGB(255,255,255));//white
	}

  //  CFont *pOldFont = omCdc.SelectObject(&omSmallFont_Bold);
    CFont *pOldFont = omCdc.SelectObject(&omSmallFont_RegularForGantt);
  //  COLORREF nOldTextColor = omCdc.SetTextColor(lpTextColor);
    int nBkMode = omCdc.SetBkMode(TRANSPARENT);
    UINT nTextAlign = omCdc.SetTextAlign(TA_CENTER | TA_TOP);
    int x = (ilStartX + ilEndX) / 2;

	//check if there are more strings to be used(olConf[0] == "EXTRACT")
	//and set the best (the first is the best - olConf[1])
	CStringArray olConf;
	int ilBarWidth = 2 * halfLength;
	CString olPrintText = ropBar.Text;
	if( !ropBar.Text.IsEmpty() )
	{
		ExtractItemList(ropBar.Text, &olConf, '#');

		int il = olConf.GetSize();
		if (il >= 1 && olConf[0] == "EXTRACT")
		{
			for (int i=1; i<olConf.GetSize(); i++)
			{
				if (!olConf[i].IsEmpty())
				{
					olPrintText = olConf[i];
					CSize olTextExtent = omCdc.GetTextExtent(olPrintText, olPrintText.GetLength());
					int ilTextWidth = olTextExtent.cx;

				//	pa�t in die Bar
					if(ilTextWidth<ilBarWidth)
						break;
				}
			}
		}
		else
			olPrintText = ropBar.Text;
	}

	if (!bmViewBigBobs)
	    omCdc.ExtTextOut(x, ilStartY + imBarVerticalTextOffset, ETO_CLIPPED, &rcBar, olPrintText, lstrlen(olPrintText), NULL);
	else
	{
//		CSize olSize = omCdc.GetTextExtent("LH 4711");
		int ilYBigBobs = ilStartY + imBarVerticalTextOffset;
		int delta = imBarHeight/4;
		for (int i=1; i<olConf.GetSize(); i++)
		{
				olPrintText = olConf[i];
			    omCdc.ExtTextOut(x, ilYBigBobs+delta*(i-1), ETO_CLIPPED, &rcBar, olPrintText, lstrlen(olPrintText), NULL);
		}
	}


//    omCdc.ExtTextOut(x, ilStartY + imBarVerticalTextOffset+10, ETO_CLIPPED, &rcBar, ropBar.Text, lstrlen(ropBar.Text), NULL);
    omCdc.SetTextAlign(nTextAlign);
    omCdc.SetBkMode(nBkMode);
  //  omCdc.SetTextColor(nOldTextColor);
    omCdc.SelectObject(pOldFont);
	omCdc.SetTextColor(olOldTextCol);
    omCdc.SelectObject(polOldPen);


	//draw seperator for triangle
	polOldPen  = omCdc.SelectObject(&ThinPen);
    switch (ropBar.MarkerTri)
    {
	case TRIANGLE_BOTH:
//rkrpos
//		omCdc.MoveTo(rcTriMarker.left, rcTriMarker.bottom);
		omCdc.MoveTo(rcTriMarker.left, rcTriMarker.top + height);
		omCdc.LineTo(rcTriMarker.left + height, rcTriMarker.top);

		omCdc.MoveTo(rcTriMarker.right - height, rcTriMarker.bottom);
//		omCdc.LineTo(rcTriMarker.right, rcTriMarker.top);
		omCdc.LineTo(rcTriMarker.right, rcTriMarker.bottom  - height);

		break;
    case TRIANGLE_LEFT:

//		omCdc.MoveTo(rcTriMarker.left, rcTriMarker.bottom);
		omCdc.MoveTo(rcTriMarker.left, rcTriMarker.top + height);
		omCdc.LineTo(rcTriMarker.left + height, rcTriMarker.top);

		break;
    case TRIANGLE_RIGHT:

		omCdc.MoveTo(rcTriMarker.right - height, rcTriMarker.bottom);
//		omCdc.LineTo(rcTriMarker.right, rcTriMarker.top);
		omCdc.LineTo(rcTriMarker.right, rcTriMarker.bottom  - height);

		break;
	}
    omCdc.SelectObject(polOldPen);

	

	
	// draw frame over bar if necessary
    if (ropBar.FrameType == FRAMERECT)
    {   
		CBrush brBlack(RGB(0, 0, 0));
		for(int i = 0; i < 4; i++)
		{
			omCdc.FrameRect(&rcBar, &brBlack);
			rcBar.top++;
			rcBar.bottom--;
			rcBar.left++;
			rcBar.right--;
		}
    }
	if (ropBar.FrameType == FRAMEBACKGROUND)
	{
		CBrush brBackground(RGB(147,147,147));
		omCdc.FrameRect(&rcBar, &brBackground);
	}
	return TRUE;
}

//-----------------------------------------------------------------------------------------------

BOOL CCSPrint::PrintBkBar(PRINTBARDATA &ropBar, int ipHeightOfThisLine)
{
	int ilStartX = GetXFromTime(ropBar.StartTime);
	int ilEndX   = GetXFromTime(ropBar.EndTime);
	int ilStartY = imYOffset;
	int ilEndY   = ilStartY + ipHeightOfThisLine;

    CRect rcBackground(ilStartX,ilStartY,ilEndX,ilEndY); // for drawing bar background
    rcBackground.right++;   // extra pixel to help FillRect() work correctly
	CBrush brBackground(RGB(240,240,240));
	omCdc.FillRect(&rcBackground, &brBackground);

	return TRUE;
}

//-----------------------------------------------------------------------------------------------

BOOL CCSPrint::PrintText(int ipStartX,int ipStartY,int ipEndX,int ipEndY,int ipType,CString opText,BOOL ipUseOffset)
{
    CFont *polOldFont;
	int ilStartY;
	int ilEndY;

	switch ( ipType )
	{
	case PRINT_SMALLBOLD:
		polOldFont = omCdc.SelectObject(&omSmallFont_Bold);
		break;
	case PRINT_MEDIUMBOLD:
		polOldFont = omCdc.SelectObject(&omMediumFont_Bold);
		break;
	case PRINT_LARGEBOLD:
		polOldFont = omCdc.SelectObject(&omLargeFont_Bold);
		break;
	case PRINT_MEDIUM:
		polOldFont = omCdc.SelectObject(&omMediumFont_Regular);
		break;
	case PRINT_LARGE:
		polOldFont = omCdc.SelectObject(&omLargeFont_Regular);
		break;
	case PRINT_SMALL:
	default:
		// if we find no Type, use small Font as in PRINT_SMALL
		polOldFont = omCdc.SelectObject(&omSmallFont_Regular);
		break;
	}

	if (ipUseOffset == TRUE)
	{
			ilStartY = imYOffset + MMY(ipStartY);
			ilEndY = ilStartY + MMY(ipEndY); 
	}
	else 
	{
		if (ipStartY == 0)
		{
			ilStartY = imYOffset;
			imYOffset += MMY(ipEndY);
			ilEndY = imYOffset;
		}
		else
		{
			ilStartY = MMY(ipStartY);
			ilEndY = MMY(ipEndY);
		}
	}

	CRect olRect(MMX(ipStartX),ilStartY,MMX(ipEndX),ilEndY);
	omCdc.DrawText(opText,olRect,DT_LEFT|DT_WORDBREAK);
	omCdc.SelectObject(polOldFont);

	return TRUE;

}

//-----------------------------------------------------------------------------------------------

void CCSPrint::PrintUIFHeader(CString opHeader1,CString opHeader2,int ilFirstPos,bool blBitmap)
{
	CFont *polOldFont;
	CPen  *polOldPen; 
	CSize olSize;
	int ilHorzRes = omCdc.GetDeviceCaps(HORZRES);
	polOldPen = omCdc.SelectObject(&ThinPen);

	// determine whether a device supports StretchBlt
	//RC_STRETCHDIB:Capable of performing the StretchDIBits Windows function.->2
	
	//iR = omCdc.GetDeviceCaps( TECHNOLOGY ); //2 = DT_RASPRINTER
	//iR = omCdc.GetDeviceCaps(RASTERCAPS); // 28313	RC_STRETCHBLT=2048
	 
	//if(imOrientation == PRINT_LANDSCAPE)
	{
			
		omCdc.MoveTo(MMX(imLeftOffset),MMY(ilFirstPos));
		omCdc.LineTo(ilHorzRes-MMX(20),MMY(ilFirstPos));

		polOldFont = omCdc.SelectObject(&omLargeFont_Bold);
		omCdc.TextOut(MMX(imLeftOffset+10),MMY(ilFirstPos-80), opHeader1, strlen(opHeader1));
		
		olSize = omCdc.GetTextExtent(opHeader1);
		omCdc.SelectObject(&omMediumFont_Bold);
		omCdc.TextOut(MMX(imLeftOffset+50)+olSize.cx,MMY(ilFirstPos-65), opHeader2, strlen(opHeader2));

		if(blBitmap==true)

			//TEST 1 int blRet = omCdc.StretchBlt(ilHorzRes-MMX(2815),MMY(ilFirstPos-95),MMX(365),MMY(132),&omMemDc,0,0,365,132, SRCCOPY);
			int blRet = omCdc.StretchBlt(ilHorzRes-MMX(2864),MMY(ilFirstPos-138),MMX(365),MMY(132),&omMemDc,0,0,234,84, SRCCOPY);
			// Report: Bitmap am linken Rand positioniert

	}
	//else
	{
	}
	omCdc.SelectObject(polOldFont);
	omCdc.SelectObject(polOldPen);
}


//-----------------------------------------------------------------------------------------------

void CCSPrint::PrintUIFHeaderNoLine(CString opHeader1,CString opHeader2,int ilFirstPos)
{
	CFont *polOldFont;
	CPen  *polOldPen; 
	CSize olSize;
	int ilHorzRes = omCdc.GetDeviceCaps(HORZRES);
	polOldPen = omCdc.SelectObject(&ThinPen);
	//if(imOrientation == PRINT_LANDSCAPE)
	{
		//LOGO=>//omCdc.StretchBlt(ilHorzRes-MMX(385),MMY(ilFirstPos-140),MMX(365),MMY(132),&omMemDc,0,0,365,132, SRCCOPY);

		//omCdc.MoveTo(MMX(imLeftOffset),MMY(ilFirstPos));
		//omCdc.LineTo(ilHorzRes-MMX(20),MMY(ilFirstPos));

		polOldFont = omCdc.SelectObject(&omLargeFont_Bold);
		omCdc.TextOut(MMX(imLeftOffset+10),MMY(ilFirstPos-80), opHeader1, strlen(opHeader1));
		
		olSize = omCdc.GetTextExtent(opHeader1);
		omCdc.SelectObject(&omMediumFont_Bold);
		omCdc.TextOut(MMX(imLeftOffset+50)+olSize.cx,MMY(ilFirstPos-65), opHeader2, strlen(opHeader2));
	}
	//else
	{
	}
	omCdc.SelectObject(polOldFont);
	omCdc.SelectObject(polOldPen);
}

// Print left, middle and right header
void CCSPrint::PrintUIFHeader3(const CString &ropHeaderLeft, const CString &ropHeaderMiddle, const CString &ropHeaderRight, int ilFirstPos)
{	
	
	CFont *polOldFont;
	CPen  *polOldPen; 
	CSize olSize;

	int ilHorzRes = omCdc.GetDeviceCaps(HORZRES);
	int ilVertRes = omCdc.GetDeviceCaps(VERTRES);

	// Print line 
	polOldPen = omCdc.SelectObject(&ThinPen);
	omCdc.MoveTo(MMX(imLeftOffset),MMY(ilFirstPos));
	omCdc.LineTo(ilHorzRes-MMX(20),MMY(ilFirstPos));

	// Left header
	polOldFont = omCdc.SelectObject(&omMediumFont_Bold);
	omCdc.TextOut(MMX(imLeftOffset+50), MMY(ilFirstPos-65), ropHeaderLeft, strlen(ropHeaderLeft));

	// Middle header
 	olSize = omCdc.GetTextExtent(ropHeaderMiddle);
	omCdc.TextOut((ilHorzRes-MMX(imLeftOffset+20) - olSize.cx)/2 + MMX(imLeftOffset), MMY(ilFirstPos-65), ropHeaderMiddle, strlen(ropHeaderMiddle));
		
	// Right header
 	olSize = omCdc.GetTextExtent(ropHeaderRight);
	omCdc.TextOut(ilHorzRes-(MMX(70)+olSize.cx), MMY(ilFirstPos-65), ropHeaderRight, strlen(ropHeaderRight));

 	omCdc.SelectObject(polOldFont);
	omCdc.SelectObject(polOldPen);

}



//-----------------------------------------------------------------------------------------------
// Print left, middle and right footer
void CCSPrint::PrintUIFFooter(CString opFooter1,CString opFooter2,CString opFooter3)
{
	CFont *polOldFont;
	CPen  *polOldPen; 
	CSize olSize;

	int ilHorzRes = omCdc.GetDeviceCaps(HORZRES);
	int ilVertRes = omCdc.GetDeviceCaps(VERTRES);


	polOldPen = omCdc.SelectObject(&ThinPen);
	//if(imOrientation == PRINT_LANDSCAPE)
	{
		// Print line
		omCdc.MoveTo(MMX(imLeftOffset),ilVertRes-MMY(50));
		omCdc.LineTo(ilHorzRes-MMX(20),ilVertRes-MMY(50));

		// left footer
		polOldFont = omCdc.SelectObject(&omSmallFont_Bold);
		omCdc.TextOut(MMX(imLeftOffset+10),ilVertRes-MMY(40), opFooter1, strlen(opFooter1));

		// middle footer
 		olSize = omCdc.GetTextExtent(opFooter2);
		omCdc.TextOut((ilHorzRes-MMX(imLeftOffset+20) - olSize.cx)/2 + MMX(imLeftOffset), ilVertRes-MMY(40), opFooter2, strlen(opFooter2));
		
		// right footer
 		olSize = omCdc.GetTextExtent(opFooter3);
		omCdc.TextOut(ilHorzRes-(MMX(30)+olSize.cx),ilVertRes-MMY(40), opFooter3, strlen(opFooter3));

	}
	//else
	{
	}
	omCdc.SelectObject(polOldFont);
	omCdc.SelectObject(polOldPen);

}

//-----------------------------------------------------------------------------------------------


int CCSPrint::GetLogPixelsX()
{

	return imLogPixelsX;

}


int CCSPrint::GetLeftOffset()
{

	return imLeftOffset;

}


void CCSPrint::PrintDoubleLine(CCSPtrArray <PRINTELEDATA> &ropPrintLine, int ipColumn)
{

	CPen *polOldPen;
	
	int ilLeftPos = imLeftOffset;
	int ilLineTop = imFirstLine + (imLineHeight*imLineNo);
	int ilEndLeftPos;

	polOldPen = omCdc.SelectObject(&ThinPen);

	// Anzahl an Spalten bis zum Umbruch
	for(int i = 0; i < ipColumn; i++)
	{
		PRINTELEDATA *prlEle = &ropPrintLine[i];
		
		int ilLineOffset = imLineNo * imLineHeight;

		if (SelectFramePen(prlEle->FrameLeft) == TRUE)
		{
			omCdc.MoveTo(MMX(ilLeftPos),MMY(ilLineTop));
			omCdc.LineTo(MMX(ilLeftPos),MMY(ilLineTop+imLineHeight));
		}
		if (SelectFramePen(PRINT_FRAMEMEDIUM) == TRUE)
		{
			omCdc.MoveTo(MMX(ilLeftPos),MMY(ilLineTop));
			omCdc.LineTo(MMX(ilLeftPos+prlEle->Length),MMY(ilLineTop));
		}
		if (SelectFramePen(PRINT_FRAMETHIN) == TRUE) 
		{
			omCdc.MoveTo(MMX(ilLeftPos),MMY(ilLineTop+imLineHeight));
			omCdc.LineTo(MMX(ilLeftPos+prlEle->Length),MMY(ilLineTop+imLineHeight));
		}
		if (SelectFramePen(prlEle->FrameRight) == TRUE)
		{
			omCdc.MoveTo(MMX(ilLeftPos+prlEle->Length),MMY(ilLineTop));
			omCdc.LineTo(MMX(ilLeftPos+prlEle->Length),MMY(ilLineTop+imLineHeight));
		}
		if (prlEle->Text.IsEmpty() == FALSE)
		{
			CRgn rlRgn;

			rlRgn.CreateRectRgn(MMX(ilLeftPos),MMY(ilLineTop+2),
				MMX(ilLeftPos+prlEle->Length),MMY(ilLineTop+imLineHeight-2));
			omCdc.SelectClipRgn(&rlRgn);

			CFont *polOldFont = omCdc.SelectObject(prlEle->pFont);
			CRect olRect(MMX(ilLeftPos+2),MMY(ilLineTop+2),
				MMX(ilLeftPos+prlEle->Length-2),MMY(ilLineTop+imLineHeight-2));
			switch (prlEle->Alignment)
			{
			case PRINT_LEFT: PrintLeft(olRect,prlEle->Text);
				break;
			case PRINT_RIGHT: PrintRight(olRect,prlEle->Text);
				break;
			case PRINT_CENTER: PrintCenter(olRect,prlEle->Text);
				break;
			}
			omCdc.SelectClipRgn(&omRgn);
			rlRgn.DeleteObject();
		}
		ilLeftPos += prlEle->Length;
		// Laenge in x-Richtung f�r zweite Zeile merken
		ilEndLeftPos = ilLeftPos;

	}
	
	// ------------
	// zweite Zeile
	// ------------
	++imLineNo;
	// links von vorna anfangen
	ilLeftPos = imLeftOffset;
	// in y-Richtung eine Zeile nach unten
	ilLineTop = imFirstLine + (imLineHeight*imLineNo);
	
	for (; i < ropPrintLine.GetSize(); i++)
	{
		PRINTELEDATA *prlEle = &ropPrintLine[i];
		
		int ilLineOffset = imLineNo * imLineHeight;

		if (SelectFramePen(prlEle->FrameLeft) == TRUE)
		{
			omCdc.MoveTo(MMX(ilLeftPos),MMY(ilLineTop));
			omCdc.LineTo(MMX(ilLeftPos),MMY(ilLineTop+imLineHeight));
		}
		if (SelectFramePen(prlEle->FrameRight) == TRUE)
		{
			omCdc.MoveTo(MMX(ilLeftPos+prlEle->Length),MMY(ilLineTop));
			omCdc.LineTo(MMX(ilLeftPos+prlEle->Length),MMY(ilLineTop+imLineHeight));
		}
		if (prlEle->Text.IsEmpty() == FALSE)
		{
			CRgn rlRgn;

			rlRgn.CreateRectRgn(MMX(ilLeftPos),MMY(ilLineTop+2),
				MMX(ilLeftPos+prlEle->Length),MMY(ilLineTop+imLineHeight-2));
			omCdc.SelectClipRgn(&rlRgn);

			CFont *polOldFont = omCdc.SelectObject(prlEle->pFont);
			CRect olRect(MMX(ilLeftPos+2),MMY(ilLineTop+2),
				MMX(ilLeftPos+prlEle->Length-2),MMY(ilLineTop+imLineHeight-2));
			switch (prlEle->Alignment)
			{
			case PRINT_LEFT: PrintLeft(olRect,prlEle->Text);
				break;
			case PRINT_RIGHT: PrintRight(olRect,prlEle->Text);
				break;
			case PRINT_CENTER: PrintCenter(olRect,prlEle->Text);
				break;
			}
			omCdc.SelectClipRgn(&omRgn);
			rlRgn.DeleteObject();
		}
		ilLeftPos += prlEle->Length;

		if (ilLeftPos > ilEndLeftPos)
			break;
		
	}

	ropPrintLine.DeleteAll();
	imLineNo++;

}

BOOL CCSPrint::LoadBitmapFromBMPFile( LPTSTR szFileName, HBITMAP *phBitmap,HPALETTE *phPalette )
{
     BITMAP  bm;

     *phBitmap = NULL;
     *phPalette = NULL;

     // Use LoadImage() to get the image loaded into a DIBSection
     *phBitmap = (HBITMAP)LoadImage( NULL, szFileName, IMAGE_BITMAP, 0, 0,
                 LR_CREATEDIBSECTION | LR_LOADFROMFILE );
     if( *phBitmap == NULL )
       return FALSE;

     // Get the color depth of the DIBSection
     GetObject(*phBitmap, sizeof(BITMAP), &bm );
     // If the DIBSection is 256 color or less, it has a color table
     if( ( bm.bmBitsPixel * bm.bmPlanes ) <= 8 )
     {
       HDC           hMemDC;
       HBITMAP       hOldBitmap;
       RGBQUAD       rgb[256];
       LPLOGPALETTE  pLogPal;
       WORD          i;

       // Create a memory DC and select the DIBSection into it
       hMemDC = CreateCompatibleDC( NULL );
       hOldBitmap = (HBITMAP)SelectObject( hMemDC, *phBitmap );
       // Get the DIBSection's color table
       GetDIBColorTable( hMemDC, 0, 256, rgb );
       // Create a palette from the color table
       pLogPal = (LOGPALETTE*)malloc( sizeof(LOGPALETTE) + (256*sizeof(PALETTEENTRY)) );
       pLogPal->palVersion = 0x300;
       pLogPal->palNumEntries = 256;
       for(i=0;i<256;i++)
       {
         pLogPal->palPalEntry[i].peRed = rgb[i].rgbRed;
         pLogPal->palPalEntry[i].peGreen = rgb[i].rgbGreen;
         pLogPal->palPalEntry[i].peBlue = rgb[i].rgbBlue;
         pLogPal->palPalEntry[i].peFlags = 0;
       }
       *phPalette = CreatePalette( pLogPal );
       // Clean up
       free( pLogPal );
       SelectObject( hMemDC, hOldBitmap );
       DeleteDC( hMemDC );
     }
     else   // It has no color table, so use a halftone palette
     {
       HDC    hRefDC;

       hRefDC = GetDC( NULL );
       *phPalette = CreateHalftonePalette( hRefDC );
       ReleaseDC( NULL, hRefDC );
     }
     return TRUE;
}