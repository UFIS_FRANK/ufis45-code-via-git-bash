#ifndef __DailyTableViewer_H__
#define __DailyTableViewer_H__

#include <afxcoll.h>
#include <stdafx.h>
#include <CCSGlobl.h>
#include <CCSTable.h>
#include <DailyCedaFlightData.h>
//#include "CViewer.h"
#include <CCSPrint.h> // BWi

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct DAILYSCHEDULE_LINEDATA
{
	CString		Act3;
	CString		Act5;
	CString		Regn; 	

	CTime 		AAirb; 	
	CTime 		AAiru; 	
	CString		ACsgn; 	
	CTime 		ASlot; 		
	CString		ADes3;
	CString		ADes4;
	CTime 		AEtdc; 	
	CTime 		AEtai; 	
	CTime 		AEtdi; 	
	CString		AFlno;
	CString		AFtyp; 
	CString		AGta1; 
	CString		AGta2; 
	CString		AIfra; 
	CString		AIfrd; 
	CString		AIsre;
	CTime 		ALand; 	
	CTime 		ALstu; 	
	CTime 		AOfbl; 	
	CTime 		AOnbe; 	
	CTime 		AOnbl; 	
	CString		AOrg3; 	
	CString		AOrg4; 	
	CString		APsta; 	
	CString		APstd; 	
	CString		ARemp; 	
	long		ARkey;
	CString		ARwya; 	
	CString		ARwyd; 	
	CString		AStab; 	
	CTime 		AStoa; 	
	CTime 		AStod; 	
	CTime 		ATifa; 	
	CTime 		ATifd; 	
	CTime 		ATmoa; 	
	long		AUrno; 	
	CString 	AUseu; 	
	CString 	AVia4; 	
	CString 	AVia3; 	
	CString 	AVian; 	
	CString 	ATtyp; 	
	CString 	AChgi; 	
	CString 	ARtyp; 	
	CString 	AAdid; 	
	CTime 		ANxti; 	 
	CString 	ABlt1; 	
	CTime 		APaba; 	
	CTime 		APaea; 	
	CTime 		APdba; 	
	CTime 		APdea; 	
	CString		AMing; 	
	CString		AAder; 	
	CString     AVial;
	CString     ABlt2;
	CString		ACkif;
	CString		ACkit;
	bool		AVip;

	CTime 		DAirb; 	
	CTime 		DAiru; 	
	CString		DCsgn; 	
	CTime 		DSlot; 		
	CString		DDes3;
	CString		DDes4;
	CTime 		DEtdc; 	
	CTime 		DEtai; 	
	CTime 		DEtdi; 	
	CString		DFlno;
	CString		DFtyp; 
	CString		DGta1; 
	CString		DGta2; 
	CString		DGtd1; 
	CString		DGtd2; 
	CString		DIfra; 
	CString		DIfrd; 
	CString		DIsre;
	CTime 		DLand; 	
	CTime 		DLstu; 	
	CTime 		DOfbl; 	
	CTime 		DOnbe; 	
	CTime 		DOnbl; 	
	CString		DOrg3; 	
	CString		DOrg4; 	
	CString		DPsta; 	
	CString		DPstd; 	
	CString		DRemp; 	
	long		DRkey;
	CString		DRwya; 	
	CString		DRwyd; 	
	CString		DStab; 	
	CTime 		DStoa; 	
	CTime 		DStod; 	
	CTime 		DTifa; 	
	CTime 		DTifd; 	
	CTime 		DTmoa; 	
	long		DUrno; 	
	CString 	DUseu; 	
	CString 	DVia4; 	
	CString 	DVia3; 	
	CString 	DVian; 	
	CString 	DTtyp; 	
	CString 	DChgi; 	
	CString 	DRtyp; 	
	CString 	DAdid; 	
	CTime 		DNxti; 	 
	CString 	DBlt1; 	
	CTime 		DPaba; 	
	CTime 		DPaea; 	
	CTime 		DPdba; 	
	CTime 		DPdea; 	
	CString		DMing; 	
	CString		DDAder; 	
	CString     DVial;
	CString		DCkif;
	CString		DCkit;
	CString		DBlt2;
	bool		DVip;

	CString		AFlti;
	CString		DFlti;

	DAILYSCHEDULE_LINEDATA(void)
	{ 
		AUrno = 0;
		DUrno = 0;
		ATifa = -1;
		AStoa = -1;
		DTifd = -1;
		DStod = -1;
		DVip = false;
		AVip = false;
	}
};



/////////////////////////////////////////////////////////////////////////////
// DailyTableViewer

/////////////////////////////////////////////////////////////////////////////
// Class declaration of DailyTableViewer

//@Man:
//@Memo: DailyTableViewer
//@See:  STDAFX, CCSCedaData, CedaDAILYFLIGHTDATA, CCSPTRARRAY,  CCSTable
/*@Doc:
  No comment on this up to now.
*/
class DailyTableViewer : public CViewer
{
// Constructions
public:
    //@ManMemo: Default constructor
    DailyTableViewer();
    //@ManMemo: Default destructor
    ~DailyTableViewer();

    //@ManMemo: Attach
	void Attach(CCSTable *popAttachWnd);
    //void Attach(CTable *popAttachWnd);
	/* void ChangeViewTo(const char *pcpViewName, CString opDate); */
    //@ManMemo: ChangeViewTo
    void ChangeViewTo(const char *pcpViewName);
    //@ManMemo: omLines
    CCSPtrArray<DAILYSCHEDULE_LINEDATA> omLines;

	void SetParentDlg(CDialog* ppParentDlg);

	// Table columns that have to listed in the table
	void SetColumnsToShow(const CStringArray &opShownColumns);
	// Mapping Tabellenfeldname/Spaltenueberschrift
	CString GetHeaderContent(CString opCurrentColumns);
	CString GetColumnByIndex(int ipIndex);
	BOOL CheckPostFlight(const DAILYSCHEDULE_LINEDATA *prpTableLine);
	BOOL CheckPostFlight(const DAILYFLIGHTDATA *prpAFlight, const DAILYFLIGHTDATA *prpDFlight);

	void SelectLine(long lpUrno);
	bool ShowFlight(long lpUrno);

// Internal data processing routines
private:
	bool IsPassFilter(DAILYFLIGHTDATA *prpFlight);
	int  CompareFlight(DAILYSCHEDULE_LINEDATA *prpFlight1, DAILYSCHEDULE_LINEDATA *prpFlight2);

    void MakeLines(CCSPtrArray<DAILYFLIGHTDATA> *popFlights, bool bpInsert = false);
	int  MakeLine(DAILYFLIGHTDATA *prpAFlight, DAILYFLIGHTDATA *prpDFlight);
	void MakeLineData(DAILYFLIGHTDATA *prpAFlight, DAILYFLIGHTDATA *prpDFlight, DAILYSCHEDULE_LINEDATA &rpLine);
	void MakeColList(DAILYSCHEDULE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList);
	int  CreateLine(DAILYSCHEDULE_LINEDATA &rpLine);

	int  FindLine(long lpUrno, int &rilLineno1, int &rilLineno2);
	bool FindLine(long lpUrno, int &rilLineno);
	
//	void SelectLine(long ilCurrUrno);

	void InsertDisplayLine( int ipLineNo);
	bool UtcToLocal(DAILYSCHEDULE_LINEDATA &rpLine);


// Operations
public:
    //@ManMemo: DeleteAll
	void DeleteAll();
    //@ManMemo: DeleteLine
	void DeleteLine(int ipLineno);
 
// Window refreshing routines
public:
	void UpdateDisplay();

	void DrawHeader();
	void SetFilterString(CString opFilter);
	void SetOperatorString(CString opOperator);
	void SetStartEndTime(CTime opStart, CTime opEnd);

	
	void ProcessFlightChange(DAILYFLIGHTDATA *prpFlight);
	void ProcessFlightDelete(DAILYSCHEDULERKEYLIST *prpFlight);
	void ProcessFlightUpdate(void);

	void ProcessFlightInsert(DAILYSCHEDULERKEYLIST  *prpRotation);

	void DeleteRotation(long lpRkey);
	bool CreateExcelFile(CString opTrenner);
	
	void SetTableSort(void);

	//Print
	void GetHeader(void); // BWi
	void PrintTableView(void); // BWi
	bool PrintTableHeader(void); // BWi
	bool PrintTableLine(DAILYSCHEDULE_LINEDATA *prpLine,bool bpLastLine); // BWi
	CCSPtrArray <TABLE_HEADER_COLUMN> omPrintHeadHeaderArray; // BWi
	CCSPrint *pomPrint; //auch BWI


// Attributes used for filtering condition
private:
    CString omDate;
	bool bmIsFromSearch;
	CString omFilter;
	CString omOperator;


	// Attributes
private:
	CCSTable *pomTable;
	CStringArray omShownColumns;
	CStringArray omShownColumnsSize;
	CUIntArray omAnzChar;
	// nach einem Update den alten zustand der Tabelle wieder herzustellen
	long lmUrnoSelect;
	long lmUrnoFirst;
	int imTopIndex;
	int imSelection;
	// Tabelle zu lang, so da� ein Umbruch stattfinden mu� (=true)
	bool bmCarriageReturn;
	// Spaltennummer des Umbruchs der Tabelle
	int imColumnNoCarriageReturn;
	CDialog* pomParentDlg;

// Methods which handle changes (from Data Distributor)
public:
    //@ManMemo: omStartTime
    CTime omStartTime;
    //@ManMemo: omEndTime
	CTime omEndTime;
    //@ManMemo: omDay
	CString omDay;

	CStringArray omSort;

protected:
	CString GetFieldContent(DAILYSCHEDULE_LINEDATA* prlLine, CString opCurrentColumns);
	void SetFieldLength(CString opCurrentColumns);

	void GetSelectedRow();
	void SetSelectedRow();

};

#endif //__DailyTableViewer_H__
