#if !defined(AFX_REPORTARRDEPSELCTDLG_H__3AACD541_0386_11D3_AB3D_00001C019D0B__INCLUDED_)
#define AFX_REPORTARRDEPSELCTDLG_H__3AACD541_0386_11D3_AB3D_00001C019D0B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ReportArrDepSelctDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// ReportArrDepSelctDlg dialog

class ReportArrDepSelctDlg : public CDialog
{
// Construction
public:
	ReportArrDepSelctDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(ReportArrDepSelctDlg)
	enum { IDD = IDD_REPORT_ARR_DEP };
	int		m_ListArrDep;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ReportArrDepSelctDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ReportArrDepSelctDlg)
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_REPORTARRDEPSELCTDLG_H__3AACD541_0386_11D3_AB3D_00001C019D0B__INCLUDED_)
