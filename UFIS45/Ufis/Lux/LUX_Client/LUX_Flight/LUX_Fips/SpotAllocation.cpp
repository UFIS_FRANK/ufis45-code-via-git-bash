#include <StdAfx.h>

#include <BasicData.h>
#include <SpotAllocation.h>
#include <Konflikte.h>
#include <Utils.h>
#include <resrc1.h>

#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))
#define IsWithIn(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))


// Local function prototype
static void SpotAllocationCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);


static int ComparePstResByPrio(const PST_RESLIST **e1, const PST_RESLIST **e2)
{
	int ilRet = 0;
	if((**e1).Prio < (**e2).Prio)
	{
		ilRet = 1;
	}
	if((**e1).Prio > (**e2).Prio)
	{
		ilRet = -1;
	}
	if((**e1).Prio == (**e2).Prio)
	{
		ilRet = 0;
	}
	return ilRet;
}

static int CompareGatResByPrio(const GAT_RESLIST **e1, const GAT_RESLIST **e2)
{
	int ilRet = 0;
	if((**e1).Prio < (**e2).Prio)
	{
		ilRet = 1;
	}
	if((**e1).Prio > (**e2).Prio)
	{
		ilRet = -1;
	}
	if((**e1).Prio == (**e2).Prio)
	{
		ilRet = 0;
	}
	return ilRet;
}

SpotAllocation::SpotAllocation()
{
	pomParent = NULL;

	bmPstConf = true;
	bmGatConf = true;
	bmWroConf = true;
	bmBltConf = true;
}



void SpotAllocation::SetAllocateParameters(AllocateParameters *popParameters)
{
	omParameters = *popParameters;
}





SpotAllocation::~SpotAllocation()
{
	omPstRules.DeleteAll();
	omGatRules.DeleteAll();
	omFlights.DeleteAll();
}

void SpotAllocation::SetParent( CWnd *popParent)
{
	pomParent = popParent;
}


void SpotAllocation::Init()
{


    ogDdx.UnRegister(this, NOTUSED);

	InitNatRestriction();
	CreatePositionRules();
	CreateGateRules();

	InitPstLinksToTwy();
	
	ogDdx.Register(this, BCD_PST_CHANGE, CString("FLIGHTDIAVIEWER"), CString("PST Changed"), SpotAllocationCf);
	ogDdx.Register(this, BCD_GAT_CHANGE, CString("FLIGHTDIAVIEWER"), CString("PST Changed"), SpotAllocationCf);

}


static void SpotAllocationCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
    SpotAllocation *polViewer = (SpotAllocation *)popInstance;

	if (ipDDXType == BCD_PST_CHANGE)
		polViewer->CreatePositionRules();

	if (ipDDXType == BCD_GAT_CHANGE)
		polViewer->CreateGateRules();

	if (ipDDXType == BCD_NAT_CHANGE)
		polViewer->InitNatRestriction();
		
}


void SpotAllocation::InitPstLinksToTwy()
{
	CString olTwy;
	CString olList;	

	int ilCount = ogBCD.GetDataCount("TWY");
	int ilCount2 = ogBCD.GetDataCount("PST");

	for(int i = 0; i < ilCount; i++)
	{
		olList = "";
		olTwy = ogBCD.GetField("TWY", i, "TNAM");

		for(int j = 0; j < ilCount2; j++)
		{
			if(olTwy == ogBCD.GetField("PST", j, "TAXI"))
			{
				olList += ogBCD.GetField("PST", j, "PNAM") + CString("|");
			}
		}

		if(!olList.IsEmpty())
			omPstLinksToTwy.Add(olList);

	}

}

void SpotAllocation::InitNatRestriction()
{
	omPstNatNoAllocList = CString("");
	omGatNatNoAllocList = CString("");
	
	int ilAlga = ogBCD.GetFieldIndex("NAT", "ALGA");
	int ilAlpo = ogBCD.GetFieldIndex("NAT", "ALPO");
	int ilTtyp = ogBCD.GetFieldIndex("NAT", "TTYP");


	int ilCount = ogBCD.GetDataCount("NAT");
	for(int i = 0; i < ilCount; i++)
	{
		RecordSet rlRec;
		ogBCD.GetRecord("NAT", i, rlRec);
		//CString olAlga = rlRec[ilAlga];
		//CString olAlpo = rlRec[ilAlpo];
		if(rlRec[ilAlga] != "X")
		{
			omGatNatNoAllocList += ";" + rlRec[ilTtyp];
		}
		if(rlRec[ilAlpo] != "X")
		{
			omPstNatNoAllocList += ";" + rlRec[ilTtyp];
		}
	}
	omGatNatNoAllocList += ";";
	omPstNatNoAllocList += ";";
}



void SpotAllocation::CreatePositionRules()
{
	omPstRules.DeleteAll();



	RecordSet rlRec;
	CStringArray olAcgrValues;
	PST_RESLIST *prlPstRes; 
	CStringArray olPralArray;
	RecordSet rlGatRec;
	
	
	ogBCD.SetSort("PST","PNAM+",true);
	int ilCount = ogBCD.GetDataCount("PST");


	for(int i = 0; i < ilCount; i++)
	{
		ogBCD.GetRecord("PST", i, rlRec);
		prlPstRes = new PST_RESLIST;
		
		prlPstRes->Urno = rlRec[ogBCD.GetFieldIndex("PST", "URNO")];
		prlPstRes->Pnam = rlRec[ogBCD.GetFieldIndex("PST", "PNAM")];
		prlPstRes->Brgs = rlRec[ogBCD.GetFieldIndex("PST", "BRGS")];

		prlPstRes->Nafr = DateStringToDate(rlRec[ogBCD.GetFieldIndex("PST", "NAFR")]);
		prlPstRes->Nato = DateStringToDate(rlRec[ogBCD.GetFieldIndex("PST", "NATO")]);


		CString olAcgr = rlRec[ogBCD.GetFieldIndex("PST", "POSR")];
		ogBCD.GetField("PST", "URNO", prlPstRes->Urno, "POSR", olAcgr); 

		CString olPnam = rlRec[ogBCD.GetFieldIndex("PST", "PNAM")]; 
		
		ExtractItemList(olAcgr, &olAcgrValues, ';');

		GetAcgrFromAcgrList(prlPstRes, olAcgrValues);
		
		if(prlPstRes->Pos1 == CString(""))
		{
			prlPstRes->Mac1 = 0;
		}
		if(prlPstRes->Pos2 == CString(""))
		{
			prlPstRes->Mac2 = 0;
		}
		
		int ilC2 = ogBCD.GetDataCount("GAT");
	
		for(int j = 0; j < ilC2; j++)
		{
			ogBCD.GetRecord("GAT", j, rlGatRec);
 			if( olPnam == rlGatRec[ogBCD.GetFieldIndex("GAT", "RGA1")] )
			{
				prlPstRes->RelGats.Add(rlGatRec[ogBCD.GetFieldIndex("GAT", "GNAM")]);
			}
			if( olPnam == rlGatRec[ogBCD.GetFieldIndex("GAT", "RGA2")] )
			{
				prlPstRes->RelGats.Add(rlGatRec[ogBCD.GetFieldIndex("GAT", "GNAM")]);
			}
		}

		/*
		ExtractItemList(prlPstRes->Pral, &olPralArray, ' ');
		
		prlPstRes->Pral = CString("");

		for(j = 0; j < olPralArray.GetSize(); j++)
		{
			prlPstRes->Pral += ";" + olPralArray[j];
		}
		if(!prlPstRes->Pral.IsEmpty())
		{
			prlPstRes->Pral += ";";
		}
		*/

		omPstRules.Add(prlPstRes);
	}

	omPstRules.Sort(ComparePstResByPrio);
}



void SpotAllocation::GetAcgrFromAcgrList(PST_RESLIST *prpAcgr, CStringArray &opAcgrValues)
{
	if(opAcgrValues.GetSize() < 12)
		return;

	CStringArray olItems;

	// aircraft wing span
	opAcgrValues[0].TrimRight();
	ExtractItemList(opAcgrValues[0], &olItems, '-');
	if(olItems.GetSize() == 2)
	{
		prpAcgr->SpanMin = atof(olItems[0]);
		prpAcgr->SpanMax = atof(olItems[1]);
	}


	// aircraft length
	opAcgrValues[1].TrimRight();
	ExtractItemList(opAcgrValues[1], &olItems, '-');
	if(olItems.GetSize() == 2)
	{
		prpAcgr->LengMin = atof(olItems[0]);
		prpAcgr->LengMax = atof(olItems[1]);
	}

	// aircraft height
	opAcgrValues[2].TrimRight();
	ExtractItemList(opAcgrValues[2], &olItems, '-');
	if(olItems.GetSize() == 2)
	{
		prpAcgr->HeighMin = atof(olItems[0]);
		prpAcgr->HeighMax = atof(olItems[1]);
	}

	//handle the rest


	// get aircraft lists
	prpAcgr->ACUserStr = opAcgrValues[3];
	ParsePrefExclAssiStr(prpAcgr->ACUserStr, prpAcgr->PreferredAC, prpAcgr->ExcludedAC, prpAcgr->AssignedAC);

	/*
	CStringArray olExacArray;
	ExtractItemList(opAcgrValues[3], &olExacArray, ' ');
	for(int j = 0; j < olExacArray.GetSize(); j++)
	{
		if(olExacArray[j].GetLength() == 3)
		{
			CString olTmp;
			ogBCD.GetField("ACT", "ACT3", olExacArray[j], "ACT5", olTmp);
			prpAcgr->Exac += ";" + olTmp;
		}
		if(olExacArray[j].GetLength() > 3)
		{
			prpAcgr->Exac += ";" + olExacArray[j];
		}
		prpAcgr->Exac += ";";
	}
	prpAcgr->Exac += ";";
	//prpAcgr->Exac =	opAcgrValues[3];
	*/
	
	prpAcgr->Pos1 =	opAcgrValues[4];
	prpAcgr->Mac1 =	(double) atof(opAcgrValues[5]);
	prpAcgr->Pos2 =	opAcgrValues[6];
	prpAcgr->Mac2 =	(double) atof(opAcgrValues[7]);
	prpAcgr->Mult =	atoi(opAcgrValues[8]);
	prpAcgr->Prio =	atoi(opAcgrValues[9]);

	// get nature code lists
	prpAcgr->NAUserStr = opAcgrValues[10];
	ParsePrefExclAssiStr(prpAcgr->NAUserStr, prpAcgr->PreferredNA, prpAcgr->ExcludedNA, prpAcgr->AssignedNA);

	// get airline lists
	prpAcgr->ALUserStr = opAcgrValues[11];
	ParsePrefExclAssiStr(prpAcgr->ALUserStr, prpAcgr->PreferredAL, prpAcgr->ExcludedAL, prpAcgr->AssignedAL);
}



void SpotAllocation::ParsePrefExclAssiStr(const CString &ropStr, CStringArray &ropPreferred, CStringArray &ropExcluded, CStringArray &ropAssigned) const
{
	ropPreferred.RemoveAll();
	ropExcluded.RemoveAll();
	ropAssigned.RemoveAll();

	char cpFlag = ' ';
	CString olTmpStr;
	for (int i = 0; i < ropStr.GetLength(); i++)
	{
		if (ropStr[i] == ' ' && cpFlag == ' ')
			continue;

		if (ropStr[i] != ' ' && cpFlag != ' ') 
		{
			olTmpStr += ropStr[i];
			continue;
		}

		if (ropStr[i] == ' ')
		{
			if (cpFlag == 'P')
			{
				ropPreferred.Add(olTmpStr);
			}
			if (cpFlag == 'E')
			{
				ropExcluded.Add(olTmpStr);
			}
			if (cpFlag == 'A')
			{
				ropAssigned.Add(olTmpStr);				
			}
			olTmpStr.Empty();
			cpFlag = ' ';
		}
		else if (ropStr[i] == '!')
		{
			cpFlag = 'E';
		}
		else if (ropStr[i] == '=')
		{
			cpFlag = 'A';
		}
		else
		{
			cpFlag = 'P';
			olTmpStr += ropStr[i];
		}
	}

	if (cpFlag == 'P')
	{
		ropPreferred.Add(olTmpStr);
	}
	if (cpFlag == 'E')
	{
		ropExcluded.Add(olTmpStr);
	}
	if (cpFlag == 'A')
	{
		ropAssigned.Add(olTmpStr);				
	}


}




void SpotAllocation::CreateGateRules()
{
	omGatRules.DeleteAll();


	RecordSet rlRec;
	CStringArray olPralArray;
	GAT_RESLIST *prlGatRes;
	CString olNafr;
	CString olNato;
	CString olResb;	
	CStringArray olArray;


	int ilCount = ogBCD.GetDataCount("GAT");
	for(int i = 0; i < ilCount; i++)
	{
		ogBCD.GetRecord("GAT", i, rlRec);
		prlGatRes = new GAT_RESLIST;
		
		prlGatRes->Urno = rlRec[ogBCD.GetFieldIndex("GAT", "URNO")];
		prlGatRes->Gnam = rlRec[ogBCD.GetFieldIndex("GAT", "GNAM")];
		prlGatRes->Gnam.TrimLeft();
		prlGatRes->Gnam.TrimRight();
		prlGatRes->Busg = rlRec[ogBCD.GetFieldIndex("GAT", "BUSG")];
		prlGatRes->RelPos1 = rlRec[ogBCD.GetFieldIndex("GAT", "RGA1")];
		prlGatRes->RelPos2 = rlRec[ogBCD.GetFieldIndex("GAT", "RGA2")];
		
		olNafr = rlRec[ogBCD.GetFieldIndex("GAT", "NAFR")];
		olNato = rlRec[ogBCD.GetFieldIndex("GAT", "NATO")];
		prlGatRes->Nafr = DateStringToDate(olNafr);
		prlGatRes->Nato = DateStringToDate(olNato);

		olResb = rlRec[ogBCD.GetFieldIndex("GAT", "GATR")];
		ogBCD.GetField("GAT", "URNO", prlGatRes->Urno, "GATR", olResb); 

		ExtractItemList(olResb, &olArray, ';');
		if(olArray.GetSize() >= 9)
		{
			prlGatRes->Flti = olArray[0];//_T("");
			if(!olArray[1].IsEmpty())
			{
				prlGatRes->Maxp = atoi(olArray[1]);//0;
			}
			prlGatRes->Mult = atoi(olArray[2]);//0;
			if(olArray[3] == "X")
			{
				prlGatRes->Inbo = true;//FALSE;
			}
			if(olArray[4] == "X")
			{
				prlGatRes->Outb = true;//FALSE;
			}

			prlGatRes->Prio = atoi(olArray[5]);//0;

			// get naturecode lists
			prlGatRes->NAUserStr = olArray[6];
			ParsePrefExclAssiStr(prlGatRes->NAUserStr, prlGatRes->PreferredNA, prlGatRes->ExcludedNA, prlGatRes->AssignedNA);

			/*
			prlGatRes->Natr = olArray[6];//_T("");
			prlGatRes->Natr.TrimRight();prlGatRes->Natr.TrimLeft();
			*/

			// get airline lists
			prlGatRes->ALUserStr = olArray[7];
			ParsePrefExclAssiStr(prlGatRes->ALUserStr, prlGatRes->PreferredAL, prlGatRes->ExcludedAL, prlGatRes->AssignedAL);

			/*
			prlGatRes->Pral = olArray[7];//_T("");
			prlGatRes->Pral.TrimLeft();
			prlGatRes->Pral.TrimRight();
			ExtractItemList(prlGatRes->Pral, &olPralArray, ' ');
			prlGatRes->Pral = CString("");
			for(int j = 0; j < olPralArray.GetSize(); j++)
			{
				if(olPralArray[j].GetLength() == 2)
				{
					CString olTmp;
					ogBCD.GetField("ALT", "ALC2", olPralArray[j], "ALC3", olTmp);
					prlGatRes->Pral += ";" + olTmp;
				}
				if(olPralArray[j].GetLength() == 3)
				{
					prlGatRes->Pral += ";" + olPralArray[j];
				}
			}
			if(!prlGatRes->Pral.IsEmpty())
				prlGatRes->Pral += ";";
			*/

			// get org/des lists
			prlGatRes->OrgDesUserStr = olArray[8];
			ParsePrefExclAssiStr(prlGatRes->OrgDesUserStr, prlGatRes->PreferredOrgDes, prlGatRes->ExcludedOrgDes, prlGatRes->AssignedOrgDes);

			//prlGatRes->OrgDes = olArray[8];//_T("");


		}
		omGatRules.Add(prlGatRes);
	}
	//omGatPralList += ";";
}


bool SpotAllocation::MakeBlkData(CTime &opStartTime, CTime &opEndTime, CString &opTabn, CString &opName, CStringArray &opNafr, CStringArray &opNato, CStringArray &opResn)
{

	CTime opTrafficDay =TIMENULL;
	int ipDOO = -1;
	CString olDOO;

	opNafr.RemoveAll();
	opNato.RemoveAll();
	opResn.RemoveAll();

	CString olObjName;
	CString olUrno;

	if (strcmp(opTabn,"CIC") == 0)
		olObjName = "CNAM";
	if (strcmp(opTabn,"BLT") == 0)
		olObjName = "BNAM";
	if (strcmp(opTabn,"PST") == 0)
		olObjName = "PNAM";
	if (strcmp(opTabn,"GAT") == 0)
		olObjName = "GNAM";
	if (strcmp(opTabn,"WRO") == 0)
		olObjName = "WNAM";

	ogBCD.GetField(opTabn, olObjName, opName, "URNO", olUrno);


//	if (olUrno == "4500050")
//		int f = 1;

	CString opObject = "BLK";
	int ilBlkUrnoIdx = ogBCD.GetFieldIndex(opObject,"URNO");
	int ilBlkBurnIdx = ogBCD.GetFieldIndex(opObject,"BURN");
	int ilBlkDaysIdx = ogBCD.GetFieldIndex(opObject,"DAYS");
	int ilBlkNafrIdx = ogBCD.GetFieldIndex(opObject,"NAFR");
	int ilBlkNatoIdx = ogBCD.GetFieldIndex(opObject,"NATO");
	int ilBlkResnIdx = ogBCD.GetFieldIndex(opObject,"RESN");
	int ilBlkTabnIdx = ogBCD.GetFieldIndex(opObject,"TABN");
	int ilBlkTypeIdx = ogBCD.GetFieldIndex(opObject,"TYPE");
	int ilBlkTifrIdx = ogBCD.GetFieldIndex(opObject,"TIFR");
	int ilBlkTitoIdx = ogBCD.GetFieldIndex(opObject,"TITO");


	CCSPtrArray<RecordSet> olRecSet;
	RecordSet olRec = NULL;
//	olUrno = "9552007";


	olRecSet.RemoveAll();
	ogBCD.GetRecords(opObject, "BURN", olUrno, &olRecSet);

	for(int i = 0; i < olRecSet.GetSize(); i++)
	{
		CString olTabn = olRecSet[i].Values[ogBCD.GetFieldIndex(opObject, "TABN")];
		if (strcmp(olTabn, opTabn) == 0)
		{
			olRec = olRecSet.GetAt(i);

			CTime olFrom, olTo, olCurrent;
			CTime olNafr, olNato;
			if (opStartTime != TIMENULL)
				olCurrent = CTime(opStartTime.GetYear(),opStartTime.GetMonth(),opStartTime.GetDay(), 12, 0,0);
			else
				olCurrent = CTime::GetCurrentTime();

			CString olTifr, olTito;
			olTifr = olRec[ilBlkTifrIdx];
			olTito = olRec[ilBlkTitoIdx];

			olNafr = DBStringToDateTime(olRec[ilBlkNafrIdx]);
			olNato = DBStringToDateTime(olRec[ilBlkNatoIdx]);

			CString olVon = CTimeToDBString(olNafr, TIMENULL);
			CString olBis = CTimeToDBString(olNato, TIMENULL);


			if(olNafr == TIMENULL)
				olNafr = opStartTime - CTimeSpan(1, 0, 0, 0);

			if(olNato == TIMENULL)
				olNato = opEndTime + CTimeSpan(1, 0, 0, 0);

			CTime olStart, olEnd;

			if (olCurrent == 36000) // 01.01.1970 00:00:00
			{
				// because 'HourStringToDate' will fail if olTifr!="" or olTifto!=""
				olTifr.Empty();
				olTito.Empty();
			}
			olStart = HourStringToDate(olTifr,olCurrent); 
			olEnd   = HourStringToDate(olTito,olCurrent);

			if(!olTifr.IsEmpty() && !olTito.IsEmpty())
			{
				if (IsOverlapped(olNafr, olNato, opStartTime, opEndTime))
				{
					while(olStart <= opEndTime)
					{

						if (IsOverlapped(olStart, olEnd , opStartTime, opEndTime) && IsOverlapped(olNafr, olNato, olStart ,olEnd)  )
						{
							int ilDayOfWeek = opTrafficDay != TIMENULL ? GetDayOfWeek(opTrafficDay) : GetDayOfWeek(olCurrent) ;
							CString olWeekDay; 
							olWeekDay.Format("%ld", ilDayOfWeek);

							CString olBlkDays = olRec[ilBlkDaysIdx];
							
							bool blCreate = false;

							if(olDOO.IsEmpty())
							{
								if( olBlkDays.Find(olWeekDay) != -1 )
									blCreate = true;
							}
							else
							{
								if( ( olBlkDays.Find(olDOO) != -1) && (ilDayOfWeek == ipDOO) ) 
									blCreate = true;
							}

							if( blCreate )
							{

								CString olVonStr = CTimeToDBString(olStart, TIMENULL);
								CString olBisStr = CTimeToDBString(olEnd, TIMENULL);
								opNafr.Add(olVonStr);
								opNato.Add(olBisStr);
								opResn.Add(olRec[ilBlkResnIdx]);
							}
						}
						olCurrent += CTimeSpan(1, 0, 0, 0);
						olStart = HourStringToDate(olTifr,olCurrent);
						olEnd   = HourStringToDate(olTito,olCurrent);
					}
				}
			}
		}
	}
	olRecSet.DeleteAll();
//	olRecSet.RemoveAll();
	return true;
}



// Check the position for the given flights!
// For circular flights, this routine must be called twice! One with prpFlightA and the other one with prpFlightD!
bool SpotAllocation::CheckPst(DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD, CString opPst, CCSPtrArray<KonfItem> *popKonfList, CTimeSpan *popRightBuffer, CTimeSpan *popLeftBuffer)
{

	if(opPst.IsEmpty())
		return true;

	PST_RESLIST *prlPstRes = GetPstRes(opPst); 

	if(prlPstRes == NULL)
		return false;

	CTime olStartAlloc;
	CTime olEndAlloc;
	DIAFLIGHTDATA *prlFlight;
	bool blActFound = false;
	RecordSet olCurrActRecord;
	bool blRet = true;
	CString olConfText;


	if(prpFlightA != NULL)
		prlFlight = prpFlightA;
	else
		prlFlight = prpFlightD;

	//ogPosDiaFlightData.CalculatePstAllocTimes(ogPosDiaFlightData.GetRotationByRkey(prlFlight->Rkey));


	if (!ogPosDiaFlightData.GetPstAllocTimes(prpFlightA, prpFlightD, opPst, olStartAlloc, olEndAlloc))
	{
		return false;
	}



	char clFPart = ' ';
	if((prpFlightA != NULL) && (prpFlightD != NULL))
	{
		//olStartAlloc	= prpFlightA->StartPosArr;
		//olEndAlloc		= prpFlightD->EndPosDep;
		clFPart			= 'B';
	}
 
	if((prpFlightA != NULL) && (prpFlightD == NULL))
	{
		//olStartAlloc	= prpFlightA->StartPosArr;
		//olEndAlloc		= prpFlightA->EndPosArr;
		clFPart			= 'A';
	}

	if((prpFlightA == NULL) && (prpFlightD != NULL))
	{
		//olStartAlloc	= prpFlightD->StartPosDep;
		//olEndAlloc		= prpFlightD->EndPosDep;
		clFPart			= 'D';

	}


	CTime olStartAllocBuf(olStartAlloc);
	CTime olEndAllocBuf(olEndAlloc);
 
	if(popLeftBuffer != NULL && popRightBuffer != NULL)
	{
		olStartAllocBuf -= *popLeftBuffer;
		olStartAllocBuf -= *popRightBuffer;
		olEndAllocBuf += *popRightBuffer;
		olEndAllocBuf += *popLeftBuffer;
	}
	else
	{
		olStartAllocBuf -= ogPosAllocBufferTime;
		olStartAllocBuf -= ogPosAllocBufferTime;
		olEndAllocBuf += ogPosAllocBufferTime;
		olEndAllocBuf += ogPosAllocBufferTime;
	}

	//////////////////////////////
	// CHECK AIRCRAFTTYP
	blActFound = ogBCD.GetRecord("ACT", "ACT3", CString(prlFlight->Act3), olCurrActRecord);
	if(blActFound == false)
	{
		blActFound = ogBCD.GetRecord("ACT", "ACT5", CString(prlFlight->Act5), olCurrActRecord);
	}

	if(blActFound == false)
	{
		if(popKonfList != NULL)
		{
			//Aircrafttyp not found: %s/%s
			olConfText.Format(GetString(IDS_STRING1713), prlFlight->Act3, prlFlight->Act5);
			AddToKonfList(prpFlightA, prpFlightD, 0, ' ', IDS_STRING1713, olConfText, *popKonfList);

			blRet = false;
		}
		else
		{
			return false;
		}
	}
	////////////////////////////


	////////////////////////////////
	// CHECK POSITION AVAILABILITY
/*
	CString olTmp;
	ogBCD.GetField("PST", "PNAM", opPst, "NAFR", olTmp);
	CTime olNafr = DBStringToDateTime(olTmp);
	ogBCD.GetField("PST", "PNAM", opPst, "NATO", olTmp);
	CTime olNato = DBStringToDateTime(olTmp);

	ogBCD.GetField("PST", "PNAM", opPst, "VAFR", olTmp);
	CTime olVafr = DBStringToDateTime(olTmp);
	ogBCD.GetField("PST", "PNAM", opPst, "VATO", olTmp);
	CTime olVato = DBStringToDateTime(olTmp);


	CString oNafr = 	olNafr.Format("%H:%M %d.%m.%Y");
	CString oNato = 	olNato.Format("%H:%M %d.%m.%Y");
	CString oVafr = 	olVafr.Format("%H:%M %d.%m.%Y");
	CString oVato = 	olVato.Format("%H:%M %d.%m.%Y");

	
	//CString oStartAlloc	= olStartAlloc.Format("%H:%M %d.%m.%Y");
	//CString oEndAlloc	= olEndAlloc.Format("%H:%M %d.%m.%Y");


	if(olNafr != TIMENULL || olNato != TIMENULL)
	{	
		if(olNafr == TIMENULL)
			olNafr = CTime(1971,1,1,0,0,0);
		if(olNato == TIMENULL)
			olNato = CTime(2020,1,1,0,0,0);
	}


	if(olVafr == TIMENULL)
		olVafr = CTime(1971,1,1,0,0,0);
	if(olVato == TIMENULL)
		olVato = CTime(2020,1,1,0,0,0);


	if((olNafr != TIMENULL) && (olNato != TIMENULL))
	{
		if(IsOverlapped(olNafr,olNato, olStartAlloc, olEndAlloc) || !IsWithIn(olStartAlloc, olEndAlloc,olVafr, olVato ))
		{
			if(popKonfList != NULL)
			{
				//Position not available %s - %s reason: %s
				olConfText.Format(GetString(IDS_STRING1714), olNafr.Format("%H:%M %d.%m.%y"), olNato.Format("%H:%M %d.%m.%y"), ogBCD.GetField("PST", "PNAM", opPst, "RESN"));
				AddToKonfList(prpFlightA, prpFlightD, 0, ' ', IDS_STRING1714, olConfText, *popKonfList);

				blRet = false;
			}
			else
			{
				return false;
			}
		}
	}

	if(!IsWithIn(olStartAlloc, olEndAlloc,olVafr, olVato ))
	{
		if(popKonfList != NULL)
		{
			//Position not available %s - %s reason: %s
			olConfText.Format(GetString(IDS_STRING1814), olVafr.Format("%H:%M %d.%m.%y"), olVato.Format("%H:%M %d.%m.%y"));
			AddToKonfList(prpFlightA, prpFlightD, 0, ' ', IDS_STRING1814, olConfText, *popKonfList);
						
			blRet = false;
		}
		else
		{
			return false;
		}
	}
*/
	CStringArray opNafr;
	CStringArray opNato;
	CStringArray opResn;
	CString opTabn = "PST";
	MakeBlkData(olStartAlloc, olEndAlloc, opTabn, opPst, opNafr, opNato, opResn);

	for (int il = 0; il < opNafr.GetSize(); il++)
	{
		CTime olNafr = DBStringToDateTime(opNafr.GetAt(il));
		CTime olNato = DBStringToDateTime(opNato.GetAt(il));

		CString olVonStr = CTimeToDBString(olNafr, TIMENULL);
		CString olBisStr = CTimeToDBString(olNato, TIMENULL);

		if(olNafr != TIMENULL || olNato != TIMENULL)
		{	
			if(olNafr == TIMENULL)
				olNafr = CTime(1971,1,1,0,0,0);
			if(olNato == TIMENULL)
				olNato = CTime(2020,1,1,0,0,0);
		}


/*		if(olVafr == TIMENULL)
			olVafr = CTime(1971,1,1,0,0,0);
		if(olVato == TIMENULL)
			olVato = CTime(2020,1,1,0,0,0);
*/

		if((olNafr != TIMENULL) && (olNato != TIMENULL))
		{
			if(IsReallyOverlapped(olStartAlloc, olEndAlloc, olNafr, olNato)/*, olStartAlloc, olEndAlloc) || !IsWithIn(olStartAlloc, olEndAlloc, olNafr, olNato)*/)
			{
				if(popKonfList != NULL)
				{
					//Position not available %s - %s reason: %s
					if(bgGatPosLocal) ogBasicData.UtcToLocal(olNafr);
					if(bgGatPosLocal) ogBasicData.UtcToLocal(olNato);
					olConfText.Format(GetString(IDS_STRING1714), olNafr.Format("%H:%M %d.%m.%y"), olNato.Format("%H:%M %d.%m.%y"), opResn.GetAt(il));
					AddToKonfList(prpFlightA, prpFlightD, 0, ' ', IDS_STRING1714, olConfText, *popKonfList);

					blRet = false;
				}
				else
				{
					return false;
				}
			}
		}
/*
		if(!IsWithIn(olStartAlloc, olEndAlloc,olVafr, olVato ))
		{
			if(popKonfList != NULL)
			{
				//Position not available %s - %s reason: %s
				olConfText.Format(GetString(IDS_STRING1814), olVafr.Format("%H:%M %d.%m.%y"), olVato.Format("%H:%M %d.%m.%y"));
				AddToKonfList(prpFlightA, prpFlightD, 0, ' ', IDS_STRING1814, olConfText, *popKonfList);
							
				blRet = false;
			}
			else
			{
				return false;
			}
		}
		*/
	}



	////////////////////////////



	//////////////////////////
	// CHECK Flight ID
	CString olRelGatFlti;
	for (int i = 0; i < prlPstRes->RelGats.GetSize(); i++)
	{
		GAT_RESLIST *prlGatRes = GetGatRes(prlPstRes->RelGats[i]); 

		if(prlGatRes != NULL)
		{
			olRelGatFlti += prlGatRes->Flti + CString("/");
		}
	}
	olRelGatFlti = olRelGatFlti.Left(olRelGatFlti.GetLength() - 1);

	if(!olRelGatFlti.IsEmpty())
	{
		CString olFltiA;
		CString olFltiD;


		if(prpFlightA != NULL)
		{
			olFltiA = prpFlightA->Flti;
		}
		if(prpFlightD != NULL)
		{
			olFltiD = prpFlightD->Flti;
		}
		if((olFltiD == "I" && olFltiA == "D") || (olFltiA == "I" && olFltiD == "D"))
		{
			olFltiD = "M";
			olFltiA = "M";
		}


		if(!olFltiA.IsEmpty())
		{
			if( olRelGatFlti.Find( CString(olFltiA)) < 0)
			{
				if(popKonfList != NULL)
				{
					//Position %s: Flight ID from the related Gate is %s must be %s 
					olConfText.Format(GetString(IDS_STRING1715),  opPst, olRelGatFlti, olFltiA);
					AddToKonfList(prpFlightA, (DIAFLIGHTDATA *) NULL, 0, ' ', IDS_STRING1715, olConfText, *popKonfList);

					blRet = false;
				}
				else
				{
					return false;
				}
			}
		}

		if(!olFltiD.IsEmpty() && olFltiA != olFltiD)
		{
			if( olRelGatFlti.Find( CString(olFltiD)) < 0)
			{
				if(popKonfList != NULL)
				{
					//Position %s: Flight ID from the related Gate is %s must be %s 
					olConfText.Format(GetString(IDS_STRING1715),  opPst, olRelGatFlti, olFltiD);
					AddToKonfList(NULL, prpFlightD, 0, ' ', IDS_STRING1715, olConfText, *popKonfList);

					blRet = false;
				}
				else
				{
					return false;
				}
			}
		}
	}
	/////////////////////////////


	///////////////////////////////////////////////
	// CHECK NATURE RESTRICTION FROM NATURE TABLE
	if(prpFlightA != NULL)
	{
		if( !CString(prpFlightA->Ttyp).IsEmpty() )
		{
			if( omPstNatNoAllocList.Find(prpFlightA->Ttyp) >= 0)
			{

				if(popKonfList != NULL)
				{
					//Global nature ristriction (%s) at position %s: %s
					olConfText.Format(GetString(IDS_STRING1712), omPstNatNoAllocList, opPst, CString(prpFlightA->Ttyp));
					AddToKonfList(prpFlightA, (DIAFLIGHTDATA *) NULL, 0, ' ', IDS_STRING1712, olConfText, *popKonfList);

					blRet = false;
			}
				else
				{
					return false;
		}

	}
		}
	}

	if(prpFlightD != NULL)
	{
		if( !CString(prpFlightD->Ttyp).IsEmpty() )
		{
			if( omPstNatNoAllocList.Find(prpFlightD->Ttyp) >= 0)
			{
				if(popKonfList != NULL)
				{
					//Global nature ristriction (%s) at position %s: %s
					olConfText.Format(GetString(IDS_STRING1712), omPstNatNoAllocList, opPst, CString(prpFlightD->Ttyp));
					AddToKonfList((DIAFLIGHTDATA *) NULL, prpFlightD, 0, ' ', IDS_STRING1712, olConfText, *popKonfList);

					blRet = false;
				}
				else
				{
					return false;
				}

			}
		}
	}

	///////////////////////////////



	///////////////////////////////
	////// CHECK PUSHBACK CONF
	CCSPtrArray<DIAFLIGHTDATA> rlFlights;
	DIAFLIGHTDATA *prlFlightTmp;
	CTimeSpan olSpan10 = CTimeSpan(0,0,10,0);

	int ilLinksCount = omPstLinksToTwy.GetSize();

	if(prpFlightD != NULL)
	{
		if(!CString(opPst).IsEmpty() && prpFlightD->Airb == TIMENULL)
		{
			for(int g = 0; g < ilLinksCount; g++)
			{
				if(omPstLinksToTwy[g].Find(CString(opPst)) >= 0)
				{
					ogPosDiaFlightData.GetFlightsAtPositions( omPstLinksToTwy[g], rlFlights);

					for(int k = rlFlights.GetSize() -1; k >= 0; k--)
					{
						prlFlightTmp =  &rlFlights[k];

						if( CString(prlFlightTmp->Adid) == "A" && prlFlightTmp->Onbl == TIMENULL)
						{
							CTimeSpan olSpan = prlFlightTmp->Tifa - prpFlightD->Tifd;

							if( olSpan10.GetTotalMinutes() >  abs(olSpan.GetTotalMinutes())  )
							{

								if(popKonfList != NULL)
								{
									//Pushback conflict at position %s with flight %s
									olConfText.Format(GetString(IDS_STRING1816), opPst, prlFlightTmp->Flno);
									AddToKonfList((DIAFLIGHTDATA *)NULL, prpFlightD, 0, ' ', IDS_STRING1816, olConfText, *popKonfList);
									blRet = false;
								}
								else
								{
									return false;
								}
								
							}
						}
					}
				}
			}	
		}
	}



	if(prpFlightA != NULL)
	{
		if(!CString(opPst).IsEmpty() && prpFlightA->Onbl == TIMENULL)
		{
			for(int g = 0; g < ilLinksCount; g++)
			{
				if(omPstLinksToTwy[g].Find(CString(opPst)) >= 0)
				{
					ogPosDiaFlightData.GetFlightsAtPositions( omPstLinksToTwy[g], rlFlights);

					for(int k = rlFlights.GetSize() -1; k >= 0; k--)
					{
						prlFlightTmp =  &rlFlights[k];

						if( CString(prlFlightTmp->Adid) == "D" && prlFlightTmp->Airb == TIMENULL)
						{
							CTimeSpan olSpan = prpFlightA->Tifa - prlFlightTmp->Tifd;

							if( olSpan10.GetTotalMinutes() >  abs(olSpan.GetTotalMinutes())  )
							{

								if(popKonfList == NULL)
								{
									return false; // ?? Kein echter Konflikt?? Ama fragen!
								}
								
							}
						}
					}
				}
			}	
		}
	}
	//////////////////////////////






	// Check nature ristriction from pos-rules (  not nature table )

	if(prlPstRes->AssignedNA.GetSize() > 0 || prlPstRes->ExcludedNA.GetSize() > 0)
	{
		if(prpFlightA != NULL)
		{
			if( strlen(prpFlightA->Ttyp) > 0 )
			{
				if (CheckResToResLists(prpFlightA->Ttyp, prlPstRes->AssignedNA, prlPstRes->ExcludedNA) == false)
				{
					if(popKonfList != NULL)
					{
						//Position %s: Nature ristriction (%s): %s
						olConfText.Format(GetString(IDS_STRING1716), opPst, prlPstRes->NAUserStr, prpFlightA->Ttyp);
						AddToKonfList(prpFlightA, (DIAFLIGHTDATA *)NULL, 0, ' ', IDS_STRING1716, olConfText, *popKonfList);
						blRet = false;
					}
					else
					{
						return false;
					}
				}
			}
		}

		if(prpFlightD != NULL)
		{
			if( strlen(prpFlightD->Ttyp) > 0 )
			{
				if (CheckResToResLists(prpFlightD->Ttyp, prlPstRes->AssignedNA, prlPstRes->ExcludedNA) == false)
				{
					if(popKonfList != NULL)
					{
						//Position %s: Nature ristriction (%s): %s
									olConfText.Format(GetString(IDS_STRING1716), opPst, prlPstRes->NAUserStr, prpFlightD->Ttyp);
									AddToKonfList((DIAFLIGHTDATA *)NULL, prpFlightD, 0, ' ', IDS_STRING1716, olConfText, *popKonfList);
						blRet = false;
					}
					else
					{
						return false;
					}
				}
			}
		}
	}


	// Check aircraft restrictions at position
	if(prlPstRes->AssignedAC.GetSize() > 0 || prlPstRes->ExcludedAC.GetSize() > 0)
	{
		if (CheckResToResLists(prlFlight->Act3, prlFlight->Act5, prlPstRes->AssignedAC, prlPstRes->ExcludedAC) == false)
		{
			if(popKonfList != NULL)
			{
				// Position %s: Aircraft restriction (%s): %s
				olConfText.Format(GetString(IDS_STRING1717), opPst, prlPstRes->ACUserStr, CString(prlFlight->Act3)+"/"+CString(prlFlight->Act5));
				AddToKonfList(prpFlightA, prpFlightD, 0, ' ', IDS_STRING1717, olConfText, *popKonfList);
				blRet = false;
			}
			else
			{
				return false;
			}
		}
	}



	// Check airline restrictions at position
	if(prlPstRes->AssignedAL.GetSize() > 0 || prlPstRes->ExcludedAL.GetSize() > 0)
	{
		if (CheckResToResLists(prlFlight->Alc2, prlFlight->Alc3, prlPstRes->AssignedAL, prlPstRes->ExcludedAL) == false)
		{
			if(popKonfList != NULL)
			{
				// Position %s: Airline restriction (%s): %s
				olConfText.Format(GetString(IDS_STRING1718), opPst, prlPstRes->ALUserStr, CString(prlFlight->Alc2)+"/"+CString(prlFlight->Alc3));
				AddToKonfList(prpFlightA, prpFlightD, 0, ' ', IDS_STRING1718, olConfText, *popKonfList);
				blRet = false;
			}
			else
			{
				return false;
			}
		}
	}

	

	// Check max wing size
	double dlAcws = (double) atof(olCurrActRecord[ ogBCD.GetFieldIndex("ACT","ACWS")]);

	if((dlAcws > 0) && (dlAcws > prlPstRes->SpanMax ||  dlAcws < prlPstRes->SpanMin))
	{
		if(popKonfList != NULL)
		{
			//Aircraft wingspan at position %s: %.2f (%.2f-%.2f) %s %s
			olConfText.Format(GetString(IDS_STRING1719), opPst, dlAcws, prlPstRes->SpanMin, prlPstRes->SpanMax,  prlFlight->Act3, prlFlight->Act5);
			AddToKonfList(prpFlightA, prpFlightD, 0, ' ', IDS_STRING1719, olConfText, *popKonfList);
			blRet = false;
		}
		else
		{
			return false;
		}
	}



	// Check aircraft height
 	double dlAche = (double) atof(olCurrActRecord[ ogBCD.GetFieldIndex("ACT","ACHE")]);

	if((dlAche > 0) && ( (dlAche > prlPstRes->HeighMax && prlPstRes->HeighMax > 0) ||  ( dlAche < prlPstRes->HeighMin && prlPstRes->HeighMin > 0)))
	{
		if(popKonfList != NULL)
		{
			//Aircraft height at position %s: %.2f (%.2f-%.2f) %s/%s
			olConfText.Format(GetString(IDS_STRING1720), opPst, dlAche, prlPstRes->HeighMin, prlPstRes->HeighMax,  prlFlight->Act3, prlFlight->Act5);
			AddToKonfList(prpFlightA, prpFlightD, 0, ' ', IDS_STRING1720, olConfText, *popKonfList);
			blRet = false;
		}
		else
		{
			return false;
		}
	}


	// Check aircraft length

	double dlAcle = (double) atof(olCurrActRecord[ ogBCD.GetFieldIndex("ACT","ACLE")]);

	if((dlAcle > 0) && ( (dlAcle > prlPstRes->LengMax && prlPstRes->LengMax > 0) ||  ( dlAcle < prlPstRes->LengMin && prlPstRes->LengMin > 0)))
	{
		if(popKonfList != NULL)
		{
			//Aircraft lenght at position %s: %.2f (%.2f-%.2f) %s/%s
			olConfText.Format(GetString(IDS_STRING1721), opPst, dlAcle, prlPstRes->LengMin, prlPstRes->LengMax,  prlFlight->Act3, prlFlight->Act5);
			AddToKonfList(prpFlightA, prpFlightD, 0, ' ', IDS_STRING1721, olConfText, *popKonfList);
			blRet = false;
		}
		else
		{
			return false;
		}
	}

	/*
	if (prpFlightA != NULL && strcmp(prpFlightA->Flno, "KL 1577") == 0)
		int debug = 2;
*/

	// Check max. number of aircraft at position ( overlapp )
	int ilCountActAtPst = ogPosDiaFlightData.GetFlightsAtPosition( prpFlightA, prpFlightD, opPst, olStartAllocBuf, olEndAllocBuf);

	if( prlPstRes->Mult < ilCountActAtPst)
	{
		if(popKonfList != NULL)
		{
			//Too many aircrafts at position %s: %d Max: %d
			olConfText.Format(GetString(IDS_STRING1722), opPst, ilCountActAtPst, prlPstRes->Mult);
			AddToKonfList(prpFlightA, prpFlightD, 0, ' ', IDS_STRING1722, olConfText, *popKonfList);
			
			blRet = false;
		}
		else
		{
			return false;
		}
	}


	rlFlights.RemoveAll();
 


	///////////////////////////////
	//// POSITION-GATE RELATION
	if (ogKonflikte.IsConflictActivated(IDS_STRING2001))
	{
		if (prpFlightA != NULL)
		{
			if (!CheckPosGateRelation(prpFlightA, 'A', opPst))
			{
				if(popKonfList != NULL)
				{
					//Invalid Position - Gate Relation!
					AddToKonfList(prpFlightA, (DIAFLIGHTDATA *)NULL, 0, ' ', IDS_STRING2001, GetString(IDS_STRING2001), *popKonfList);
					blRet = false;
				}
				else
				{
					return false;
				}
			}
		}
		if (prpFlightD != NULL)
		{
			if (!CheckPosGateRelation(prpFlightD, 'D', opPst))
			{
				if(popKonfList != NULL)
				{
					//Invalid Position - Gate Relation!
					AddToKonfList((DIAFLIGHTDATA *)NULL, prpFlightD, 0, ' ', IDS_STRING2001, GetString(IDS_STRING2001), *popKonfList);
					blRet = false;
				}
				else
				{
					return false;
				}
			}
		}
	}



	//// BoardingBridge - BusGate Relation
	GAT_RESLIST *prlGatRes;
		
	bool blNoConf = true;
	if (prpFlightA != NULL)
	{
		if (strlen(prpFlightA->Gta1) > 0)
		{
			prlGatRes = GetGatRes(prpFlightA->Gta1);
			if (prlGatRes)
			{
				if ((strcmp(prlPstRes->Brgs, "X") == 0 && strcmp(prlGatRes->Busg, "X") == 0) || 
					(strcmp(prlPstRes->Brgs, "X") != 0 && strcmp(prlGatRes->Busg, "X") != 0))
					blNoConf = false;
			}
		}
		if (strlen(prpFlightA->Gta2) > 0)
		{
			prlGatRes = GetGatRes(prpFlightA->Gta2);
			if (prlGatRes)
			{
				if ((strcmp(prlPstRes->Brgs, "X") == 0 && strcmp(prlGatRes->Busg, "X") == 0) || 
					(strcmp(prlPstRes->Brgs, "X") != 0 && strcmp(prlGatRes->Busg, "X") != 0))
					blNoConf = false;
			}
		}
		if (!blNoConf)
		{
			if(popKonfList != NULL)
			{
				// Invalid 'Pass. Boarding Bridge' - 'Bus Gate' Relation!
				AddToKonfList(prpFlightA, (DIAFLIGHTDATA *)NULL, 0, ' ', IDS_STRING2065, GetString(IDS_STRING2065), *popKonfList);
				blRet = false;
			}
			else
			{
				return false;
			}
		}
	}
	blNoConf = true;
	if (prpFlightD != NULL)
	{
		if (strlen(prpFlightD->Gtd1) > 0)
		{
			prlGatRes = GetGatRes(prpFlightD->Gtd1);
			if (prlGatRes)
			{
				if ((strcmp(prlPstRes->Brgs, "X") == 0 && strcmp(prlGatRes->Busg, "X") == 0) || 
					(strcmp(prlPstRes->Brgs, "X") != 0 && strcmp(prlGatRes->Busg, "X") != 0))
					blNoConf = false;
			}
		}
		if (strlen(prpFlightD->Gtd2) > 0)
		{
			prlGatRes = GetGatRes(prpFlightD->Gtd2);
			if (prlGatRes)
			{
				if ((strcmp(prlPstRes->Brgs, "X") == 0 && strcmp(prlGatRes->Busg, "X") == 0) || 
					(strcmp(prlPstRes->Brgs, "X") != 0 && strcmp(prlGatRes->Busg, "X") != 0))
					blNoConf = false;
			}
		}
		if (!blNoConf)
		{
			if(popKonfList != NULL)
			{
				// Invalid 'Pass. Boarding Bridge' - 'Bus Gate' Relation!
				AddToKonfList((DIAFLIGHTDATA *)NULL, prpFlightD, 0, ' ', IDS_STRING2065, GetString(IDS_STRING2065), *popKonfList);
				blRet = false;
			}
			else
			{
				return false;
			}
		}
	}




	if (!CheckPstNeighborConf(prpFlightA, prpFlightD, opPst, popKonfList, popRightBuffer, popLeftBuffer))
		blRet = false;


	return blRet;
}



// Check the position for the given flights!
// For circular flights, this routine must be called twice! One with prpFlightA and the other one with prpFlightD!
// !!! ONLY check Conflicts to Neighbors !!!
bool SpotAllocation::CheckPstNeighborConf(const DIAFLIGHTDATA *prpFlightA, const DIAFLIGHTDATA *prpFlightD, 
										  const CString &ropPst, CCSPtrArray<KonfItem> *popKonfList /* = NULL */, 
										  CTimeSpan *popRightBuffer /* = NULL */, CTimeSpan *popLeftBuffer /* = NULL */)
{
	if(ropPst.IsEmpty())
		return true;

	PST_RESLIST *prlPstRes = GetPstRes(ropPst); 
	if(prlPstRes == NULL)
		return false;

	bool blRet = true;

	const DIAFLIGHTDATA *prlFlight;
	if(prpFlightA != NULL)
		prlFlight = prpFlightA;
	else if(prpFlightD != NULL)
		prlFlight = prpFlightD;
	else
		return false;


	// get alloc times
	CTime olStartAlloc;
	CTime olEndAlloc;

	if (!ogPosDiaFlightData.GetPstAllocTimes(prpFlightA, prpFlightD, ropPst, olStartAlloc, olEndAlloc))
	{
		return false;
	}
	
	
	char clFPart = ' ';
	if((prpFlightA != NULL) && (prpFlightD != NULL))
	{
		//olStartAlloc	= prpFlightA->StartPosArr;
		//olEndAlloc		= prpFlightD->EndPosDep;
		clFPart			= 'B';
	}
 
	if((prpFlightA != NULL) && (prpFlightD == NULL))
	{
		//olStartAlloc	= prpFlightA->StartPosArr;
		//olEndAlloc		= prpFlightA->EndPosArr;
		clFPart			= 'A';
	}

	if((prpFlightA == NULL) && (prpFlightD != NULL))
	{
		//olStartAlloc	= prpFlightD->StartPosDep;
		//olEndAlloc		= prpFlightD->EndPosDep;
		clFPart			= 'D';

	} 

	CTime olStartAllocBuf(olStartAlloc);
	CTime olEndAllocBuf(olEndAlloc);


	if(popLeftBuffer != NULL && popRightBuffer != NULL)
	{
		olStartAllocBuf -= *popLeftBuffer;
		olStartAllocBuf -= *popRightBuffer;
		olEndAllocBuf += *popRightBuffer;
		olEndAllocBuf += *popLeftBuffer;
	}
	else
	{
		olStartAllocBuf -= ogPosAllocBufferTime;
		olStartAllocBuf -= ogPosAllocBufferTime;
		olEndAllocBuf += ogPosAllocBufferTime;
		olEndAllocBuf += ogPosAllocBufferTime;
	}

	/*
	if(popLeftBuffer != NULL)
		olStartAllocBuf -= *popLeftBuffer;
	else
		olStartAllocBuf -= ogPosAllocBufferTime;
	
	if(popRightBuffer != NULL)
		olEndAllocBuf += *popRightBuffer;
	else
		olEndAllocBuf += ogPosAllocBufferTime;
*/

	CString olConfText;
	DIAFLIGHTDATA *prlFlightTmp = NULL;

	// get aircraft record
	RecordSet olCurrActRecord;
	bool blActFound = ogBCD.GetRecord("ACT", "ACT3", CString(prlFlight->Act3), olCurrActRecord);
	if(blActFound == false)
	{
		blActFound = ogBCD.GetRecord("ACT", "ACT5", CString(prlFlight->Act5), olCurrActRecord);
	}


	if (blActFound)
	{
		// Check wingoverlap at left position
		//double dlMaxDist;
 		//double dlMinDist = 7.5;  // Sicherheitsabstand zwischen zwei Flugzeugen
 		double dlMinDist = 0;  // Sicherheitsabstand wird bei den Angaben in den Stammdaten mit beruecksichtigt
		double dlSpan = (double) atof(olCurrActRecord[ ogBCD.GetFieldIndex("ACT","ACWS")]);

		prlFlightTmp = NULL;

		if (!CheckWingoverlap(prlFlight, olStartAllocBuf, olEndAllocBuf, CString(prlPstRes->Pos1), prlPstRes->SpanMax, prlPstRes->Mac1, dlMinDist, dlSpan, prlFlightTmp, prlPstRes->Pnam))
		{
			if(popKonfList != NULL)
			{
				//Wingoverlap with aircraft (%s %s) at position %s
				olConfText.Format(GetString(IDS_STRING1723),prlFlightTmp->Flno,prlFlightTmp->Act5, prlPstRes->Pos1);
				AddToKonfList(prpFlightA, prpFlightD, 0, ' ', IDS_STRING1723, olConfText, *popKonfList);
				blRet = false;
			}
			else
			{
				return false;
			}
 
		}
 

		// Check wingoverlap at right position
		if (!CheckWingoverlap(prlFlight, olStartAllocBuf, olEndAllocBuf, CString (prlPstRes->Pos2), prlPstRes->SpanMax, prlPstRes->Mac2, dlMinDist, dlSpan, prlFlightTmp, prlPstRes->Pnam))
		{
 			if(popKonfList != NULL)
			{
				//Wingoverlap with aircraft (%s %s) at position %s
				olConfText.Format(GetString(IDS_STRING2030),prlFlightTmp->Flno,prlFlightTmp->Act5, prlPstRes->Pos2);
				AddToKonfList(prpFlightA, prpFlightD, 0, ' ', IDS_STRING2030, olConfText, *popKonfList);
				blRet = false;
			}
			else
			{
				return false;
			}
 
		}
	}
	

	return blRet;
}



bool SpotAllocation::CheckResToResLists(const CString &ropTestStr, const CStringArray &ropAssignedStrs, const CStringArray &ropExcludedStrs) const
{
	if (ropTestStr.IsEmpty()) return true;

	if (ropAssignedStrs.GetSize() > 0 && !FindInStrArray(ropAssignedStrs, ropTestStr))
		return false;

	if (FindInStrArray(ropExcludedStrs, ropTestStr))
		return false;

	return true;
}


bool SpotAllocation::CheckResToResLists(const CString &ropTestStr1, const CString &ropTestStr2, const CStringArray &ropAssignedStrs, const CStringArray &ropExcludedStrs) const
{
	if (ropTestStr1.IsEmpty() && ropTestStr2.IsEmpty()) return true;

	if (ropAssignedStrs.GetSize() > 0 && 
		!FindInStrArray(ropAssignedStrs, ropTestStr1) && !FindInStrArray(ropAssignedStrs, ropTestStr2))
		return false;

	if (FindInStrArray(ropExcludedStrs, ropTestStr1) || FindInStrArray(ropExcludedStrs, ropTestStr2))
		return false;

	return true;
}



bool SpotAllocation::CheckPosGateRelation(DIAFLIGHTDATA *prpFlight, char cpFPart, const CString &ropPst)
{
	if (prpFlight == NULL) return true;
	/*
	PST_RESLIST *prlPstRes = GetPstRes(ropPst); 

	if(prlPstRes == NULL)
		return true;

	if (prlPstRes->RelGats.GetSize() < 1)
		return true;
	*/

	/*
	if (prpFlightA != NULL)
		if (strcmp(prpFlightA->Flno, "OAL601") == 0)
			int degug = 0;
	if (prpFlightD != NULL)
		if (strcmp(prpFlightD->Flno, "OAL151") == 0)
			int degug = 0;
	*/

	//// Position - Gate Relation
	GAT_RESLIST *prlGatRes; 

	// check arrival
	if (cpFPart == 'A')
	{
		// gate 1
		if (strlen(prpFlight->Gta1) > 0)
		{
			prlGatRes = GetGatRes(prpFlight->Gta1);
			if (prlGatRes != NULL)
			{
				if (!(prlGatRes->RelPos1.IsEmpty() && prlGatRes->RelPos2.IsEmpty()))
				{
					if (prlGatRes->RelPos1 != ropPst && prlGatRes->RelPos2 != ropPst)
						return false;
				}
			}
		}
		// gate 2
		if (strlen(prpFlight->Gta2) > 0)
		{
			prlGatRes = GetGatRes(prpFlight->Gta2);
			if (prlGatRes != NULL)
			{
				if (!(prlGatRes->RelPos1.IsEmpty() && prlGatRes->RelPos2.IsEmpty()))
				{
					if (prlGatRes->RelPos1 != ropPst && prlGatRes->RelPos2 != ropPst)
						return false;
				}
			}
		}
	}
	// check departure
	if (cpFPart == 'D')
	{
		// gate 1
		if (strlen(prpFlight->Gtd1) > 0)
		{
			prlGatRes = GetGatRes(prpFlight->Gtd1);
			if (prlGatRes != NULL)
			{
				if (!(prlGatRes->RelPos1.IsEmpty() && prlGatRes->RelPos2.IsEmpty()))
				{
					if (prlGatRes->RelPos1 != ropPst && prlGatRes->RelPos2 != ropPst)
						return false;
				}
			}
		}
		// gate 2
		if (strlen(prpFlight->Gtd2) > 0)
		{
			prlGatRes = GetGatRes(prpFlight->Gtd2);
			if (prlGatRes != NULL)
			{
				if (!(prlGatRes->RelPos1.IsEmpty() && prlGatRes->RelPos2.IsEmpty()))
				{
					if (prlGatRes->RelPos1 != ropPst && prlGatRes->RelPos2 != ropPst)
						return false;
				}
			}
		}		
	}

	return true;
}



bool SpotAllocation::CheckWingoverlap(const DIAFLIGHTDATA *prpFlight, const CTime &ropStartAlloc, const CTime &ropEndAlloc, const CString &ropPos, 
									  double dpPosSpanMax, double dpPosMac, double dpMinDist, double dpACSpan, DIAFLIGHTDATA *&prpFlightTmp, const CString &ropPosFlight)
{
	if (!prpFlight || ropPos.IsEmpty()) return true;

	CString olAcwsStr;
	CPtrArray rlFlights;
 	double dlAcwsTmp; 
	double dlNewMaxWs = (dpPosSpanMax - dpACSpan) + dpPosMac;		
	bool blAcFound = false;

 	ogPosDiaFlightData.GetFlightsAtPosition(ropStartAlloc, ropEndAlloc, ropPos, &rlFlights);

	int ilCount = rlFlights.GetSize();
	for( int i = 0 ; i < ilCount; i++)
	{
		prpFlightTmp = (DIAFLIGHTDATA *) rlFlights[i];

		if (prpFlightTmp == prpFlight || prpFlightTmp->Rkey == prpFlight->Rkey)
			continue;

		blAcFound = ogBCD.GetField("ACT", "ACT3", CString(prpFlightTmp->Act3), "ACWS", olAcwsStr );
		if (!blAcFound)
		{
			blAcFound = ogBCD.GetField("ACT", "ACT5", CString(prpFlightTmp->Act5), "ACWS", olAcwsStr );
			if (!blAcFound)
				continue;
		}

		dlAcwsTmp = (double) atof(olAcwsStr);
			
		if(dlNewMaxWs - dpMinDist < dlAcwsTmp)
		{
			prpFlightTmp = ogPosDiaFlightData.GetFlightByUrno(prpFlightTmp->Urno); // Because rlFlights will be invalid when the function returns
			return false;
		}

	//-----------------------------------------------------------
	//rkr: check back if there is conflict in the second way
		// jeder nachbar wird mit dem zu untersuchenden flight �berpr�ft
	
		// belegungsstruktur der position f�r die nachbarn
		PST_RESLIST *prlPstRes = GetPstRes(ropPos); 
		if(prlPstRes == NULL)
			return false;

		// aircraft record des nachbarn zur ermittlung der ac-Spanweite
		RecordSet olCurrActRecord;
		bool blActFound = ogBCD.GetRecord("ACT", "ACT3", CString(prpFlightTmp->Act3), olCurrActRecord);
		if(blActFound == false)
		{
			blActFound = ogBCD.GetRecord("ACT", "ACT5", CString(prpFlightTmp->Act5), olCurrActRecord);
		}

		if (! blActFound)
			return true;

		// ac-spanweite des nachbarn
		double dlSpanAcNeighbour = (double) atof(olCurrActRecord[ ogBCD.GetFieldIndex("ACT","ACWS")]);

		// maximale spannweite der nachbarposition
		double dlMaxSpanPosNeighbour = prlPstRes->SpanMax;

		// wertermittlung vom nachbar zum aktuellen flight (next position)
		double dlMacFlight = 0;
		// flight ist vom nachabar aus die linke position
		if (strcmp(ropPosFlight, prlPstRes->Pos1) == 0)
			dlMacFlight = prlPstRes->Mac1;

		// flight ist vom nachabar aus die rechte position
		if (strcmp(ropPosFlight, prlPstRes->Pos2) == 0)
			dlMacFlight = prlPstRes->Mac2;

		double dlNewMaxWsNeighbour = (dlMaxSpanPosNeighbour - dlSpanAcNeighbour) + dlMacFlight;

		if(dlNewMaxWsNeighbour - dpMinDist < dpACSpan)
		{
			prpFlightTmp = ogPosDiaFlightData.GetFlightByUrno(prpFlightTmp->Urno); // Because rlFlights will be invalid when the function returns
			return false;
		}
	//rkr: check back if there is conflict in the second way
	//-----------------------------------------------------------
	}


	prpFlightTmp = NULL;
	return true;
}



PST_RESLIST *SpotAllocation::GetPstRes(const CString &ropPst)
{
	PST_RESLIST *prlPstRes = NULL;

	for(int i = omPstRules.GetSize() - 1; i >=0; i--)
	{
		//TRACE("\n%s", omPstRules[i].Pnam);
		if(omPstRules[i].Pnam == ropPst)
		{
			prlPstRes = &omPstRules[i];
			break;
		}
	}

	return prlPstRes;
}



GAT_RESLIST *SpotAllocation::GetGatRes(const CString &ropGat)
{
	GAT_RESLIST *prlGatRes = NULL;

	for(int i = omGatRules.GetSize() - 1; i >=0; i--)
	{
		if(omGatRules[i].Gnam == ropGat)
		{
			prlGatRes = &omGatRules[i];
			break;
		}
	}
	return prlGatRes;
}


bool SpotAllocation::CheckGatFlightId(const CString &ropGat, const CString &ropFlti)
{
	bool blRet = true;

	GAT_RESLIST *prlGatRes = GetGatRes(ropGat); 

	if(prlGatRes == NULL)
		return false;


	if(strlen(prlGatRes->Flti) > 0)
	{
		if(!ropFlti.IsEmpty())
		{
			if( CString(prlGatRes->Flti).Find( ropFlti) < 0)
			{
				blRet = false;
			}
		}
	}

	return blRet;
}


CString SpotAllocation::GetGatFlightId(CString opGat)
{
	GAT_RESLIST *prlGatRes = GetGatRes(opGat); 

	if(prlGatRes == NULL)
		return "";
	return CString(prlGatRes->Flti);	

}






bool SpotAllocation::CheckGat(const DIAFLIGHTDATA *prpFlight, char cpFPart, CString opGat, int ipGatNo , CCSPtrArray<KonfItem> *popKonfList, const CTimeSpan *popRightBuffer, const CTimeSpan *popLeftBuffer) 
{
	if (prpFlight == NULL || (ipGatNo != 1 && ipGatNo != 2))
		return false;

	if(opGat.IsEmpty())
		return true;

	GAT_RESLIST *prlGatRes = GetGatRes(opGat); 
	if(prlGatRes == NULL)
		return false;

	// do not allocation one gate two times to the same flight
	if (cpFPart == 'A')
	{
		if (ipGatNo == 1 && opGat == prpFlight->Gta2)
			return false;
		if (ipGatNo == 2 && opGat == prpFlight->Gta1)
			return false;
	}
	else
	{
		if (ipGatNo == 1 && opGat == prpFlight->Gtd2)
			return false;
		if (ipGatNo == 2 && opGat == prpFlight->Gtd1)
			return false;
	}


	bool blRet = true;

	bool blActFound = false;
	RecordSet olCurrActRecord;

	CString olConfText;

	CTime olStartAlloc;
	CTime olEndAlloc;

	if (!ogPosDiaFlightData.GetGatAllocTimes(*prpFlight, cpFPart, opGat, ipGatNo, olStartAlloc, olEndAlloc))
	{
		return false;
	}

	/*
	if (cpFPart == 'A')
	{
		if (ipGatNo == 1)
		{
			olStartAlloc = prpFlight->StartGat1Arr;
			olEndAlloc = prpFlight->EndGat1Arr;
		}
		else
		{
			olStartAlloc = prpFlight->StartGat2Arr;
			olEndAlloc = prpFlight->EndGat2Arr;
		}
	}
	else
	{
		if (ipGatNo == 1)
		{
			olStartAlloc = prpFlight->StartGat1Dep;
			olEndAlloc = prpFlight->EndGat1Dep;
		}
		else
		{
			olStartAlloc = prpFlight->StartGat2Dep;
			olEndAlloc = prpFlight->EndGat2Dep;
		}
	}
	*/
	//ogPosDiaFlightData.GetBestGatTime(prpFlight, ipGatNo, cpFPart, true, olStartAlloc);
	//ogPosDiaFlightData.GetBestGatTime(prpFlight, ipGatNo, cpFPart, false, olEndAlloc);
 
	if(popRightBuffer != NULL)
		olStartAlloc -= *popRightBuffer;

	if(popLeftBuffer != NULL)
		olEndAlloc += *popLeftBuffer;


	// Check gate availablility
	CStringArray opNafr;
	CStringArray opNato;
	CStringArray opResn;
	CString opTabn = "GAT";
	MakeBlkData(olStartAlloc, olEndAlloc, opTabn, opGat, opNafr, opNato, opResn);

	for (int il = 0; il < opNafr.GetSize(); il++)
	{
		CTime olNafr = DBStringToDateTime(opNafr.GetAt(il));
		CTime olNato = DBStringToDateTime(opNato.GetAt(il));

		CString olVonStr = CTimeToDBString(olNafr, TIMENULL);
		CString olBisStr = CTimeToDBString(olNato, TIMENULL);

		if(olNafr != TIMENULL || olNato != TIMENULL)
		{	
			if(olNafr == TIMENULL)
				olNafr = CTime(1971,1,1,0,0,0);
			if(olNato == TIMENULL)
				olNato = CTime(2020,1,1,0,0,0);
		}


/*		if(olVafr == TIMENULL)
			olVafr = CTime(1971,1,1,0,0,0);
		if(olVato == TIMENULL)
			olVato = CTime(2020,1,1,0,0,0);
*/

		if((olNafr != TIMENULL) && (olNato != TIMENULL))
		{
			if(IsReallyOverlapped(olStartAlloc, olEndAlloc, olNafr, olNato)/*, olStartAlloc, olEndAlloc) || !IsWithIn(olStartAlloc, olEndAlloc, olNafr, olNato)*/)
			{
				if(popKonfList != NULL)
				{
					if(bgGatPosLocal) ogBasicData.UtcToLocal(olNafr);
					if(bgGatPosLocal) ogBasicData.UtcToLocal(olNato);
					//Gate %s not available %s - %s reason: %s
					olConfText.Format(GetString(IDS_STRING1724), opGat, olNafr.Format("%H:%M %d.%m.%y"), olNato.Format("%H:%M %d.%m.%y"), opResn.GetAt(il));
					AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING1724, olConfText, *popKonfList);

					blRet = false;
				}
				else
				{
					return false;
				}
			}
		}
	}
/*
	CString olTmp;
	ogBCD.GetField("GAT", "GNAM", opGat, "NAFR", olTmp);
	CTime olNafr = DBStringToDateTime(olTmp);
	ogBCD.GetField("GAT", "GNAM", opGat, "NATO", olTmp);
	CTime olNato = DBStringToDateTime(olTmp);

	if((olNafr != TIMENULL) && (olNato != TIMENULL))
	{
		if(IsOverlapped(olNafr,olNato, olStartAlloc, olEndAlloc))
		{
			if(popKonfList != NULL)
			{
				//Gate %s not available %s - %s reason: %s
				olConfText.Format(GetString(IDS_STRING1724), opGat, olNafr.Format("%H:%M %d.%m.%y"), olNato.Format("%H:%M %d.%m.%y"), ogBCD.GetField("GAT", "GNAM", opGat, "RESN"));
				AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING1724, olConfText, *popKonfList);
				blRet = false;
			}
			else
			{
				return false;
			}
		}
	}
*/


	// Flight ID
 	if(strlen(prlGatRes->Flti) > 0)
	{
		if(strlen(prpFlight->Flti) > 0)
		{
			if(ipGatNo == 1)
			{
				if( CString(prlGatRes->Flti).Find( CString(prpFlight->Flti)) < 0)
				{
					if(popKonfList != NULL)
					{
						//Flight ID for Gate %s: % must be %s 
						olConfText.Format(GetString(IDS_STRING1725), opGat, prpFlight->Flti, prlGatRes->Flti);
						AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING1725, olConfText, *popKonfList);

						blRet = false;
					}
					else
					{
						return false;
					}
				}
			}
		}
	}
 



	// Check max. number of flights at gate ( overlapp )
	int ilCountActAtGat = ogPosDiaFlightData.GetMaxOverlapAtGate(olStartAlloc, olEndAlloc, opGat, prpFlight);
	if( prlGatRes->Mult < ilCountActAtGat)
	{
		if(popKonfList != NULL)
		{
			//To many aircrafts at gate %s: %d Max: %d
			olConfText.Format(GetString(IDS_STRING1726), opGat, ilCountActAtGat, prlGatRes->Mult);
			AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING1726, olConfText, *popKonfList);
			blRet = false;
		}
		else
		{
			return false;
		}
	}



	// inbound gate 
	if(strcmp(prpFlight->Des3, pcgHome) == 0)
	{
		if(!prlGatRes->Inbo)
		{
			if(popKonfList != NULL)
			{
				//Gate %s can not be use for inbound 
				olConfText.Format(GetString(IDS_STRING1727), opGat);
				AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING1727, olConfText, *popKonfList);
				blRet = false;
			}
			else
			{
				return false;
			}
		}
	}
	else
	{
		if(!prlGatRes->Outb)
		{
			if(popKonfList != NULL)
			{
				//Gate %s can not be use for outbound 
				olConfText.Format(GetString(IDS_STRING1728), opGat);
				AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING1728, olConfText, *popKonfList);
				blRet = false;
			}
			else
			{
				return false;
			}
		}
	}


	//max Pax

	int ilPaxt = atoi(prpFlight->Paxt);

	if(prlGatRes->Maxp > 0)
	{
		if(prlGatRes->Maxp < ilPaxt)
		{
			if(popKonfList != NULL)
			{
				//Max Passenger of Gate %s: %s (%s)
				olConfText.Format(GetString(IDS_STRING1729), opGat, prlGatRes->Maxp, prpFlight->Paxt);
				AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING1729, olConfText, *popKonfList);
				blRet = false;
			}
			else
			{
				return false;
			}
		}
	}



	// Check airline restrictions for gate
	if(prlGatRes->AssignedAL.GetSize() > 0 || prlGatRes->ExcludedAL.GetSize() > 0)
	{
		if (CheckResToResLists(prpFlight->Alc2, prpFlight->Alc3, prlGatRes->AssignedAL, prlGatRes->ExcludedAL) == false)
		{
			if(popKonfList != NULL)
			{
				// Gate %s: Airline restriction (%s): %s
				olConfText.Format(GetString(IDS_STRING1730), opGat, prlGatRes->ALUserStr, CString(prpFlight->Alc2)+"/"+CString(prpFlight->Alc3));
				AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING1730, olConfText, *popKonfList);
				blRet = false;
			}
			else
			{
				return false;
			}
		}
	}


	// Check nature ristriction for gate
	if(prlGatRes->AssignedNA.GetSize() > 0 || prlGatRes->ExcludedNA.GetSize() > 0)
	{
		if( strlen(prpFlight->Ttyp) > 0 )
		{
			if (CheckResToResLists(prpFlight->Ttyp, prlGatRes->AssignedNA, prlGatRes->ExcludedNA) == false)
			{
				if(popKonfList != NULL)
				{
					// Gate %s: Nature restriction (%s): %s
					olConfText.Format(GetString(IDS_STRING1731), opGat, prlGatRes->NAUserStr, prpFlight->Ttyp);
					AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING1731, olConfText, *popKonfList);
					blRet = false;					
				}
				else
				{
					return false;
				}
			}
		}
	}





	/*
	// Check nature ristriction from gat-rules (  not nature table )
	bool blNatOk = true;
	CString olNat;

	if(!prlGatRes->Natr.IsEmpty())
	{

		if(!CString(prpFlight->Ttyp).IsEmpty())
		{
			if( prlGatRes->Natr.Find(prpFlight->Ttyp) == -1)
			{
				if(popKonfList != NULL)
				{
					prlKonf = new KonfItem ;
					//Nature ristriction for gate %s (%s): %s
					sprintf(prlKonf->Text, GetString(IDS_STRING1731), opGat, prlGatRes->Natr, prpFlight->Ttyp);
					prlKonf->Type = IDS_STRING1731;
					popKonfList->Add(prlKonf);
					blRet = false;
				}
				else
				{
					return false;
				}
			}
		}
	}
	*/




	// Check nature ristriction from  nature table 
	if( !CString(prpFlight->Ttyp).IsEmpty() )
	{
		if( omGatNatNoAllocList.Find(prpFlight->Ttyp) >= 0)
		{
			if(popKonfList != NULL)
			{
				//Global nature ristriction (%s) at gate %s: %s
				olConfText.Format(GetString(IDS_STRING1732), omGatNatNoAllocList,opGat, prpFlight->Ttyp);
				AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING1732, olConfText, *popKonfList);
				blRet = false;
			}
			else
			{
				return false;
			}
		}
	}




	// Check org/des restrictions for gate
	if(prlGatRes->AssignedOrgDes.GetSize() > 0 || prlGatRes->ExcludedOrgDes.GetSize() > 0)
	{
		CString olOrgDes3;
		CString olOrgDes4;
		if (cpFPart == 'A')
		{
			olOrgDes3 = prpFlight->Org3;
			olOrgDes4 = prpFlight->Org4;
		}
		else
		{
			olOrgDes3 = prpFlight->Des3;
			olOrgDes4 = prpFlight->Des4;
		}

		if (CheckResToResLists(olOrgDes3, olOrgDes4, prlGatRes->AssignedOrgDes, prlGatRes->ExcludedOrgDes) == false)
		{
			if(popKonfList != NULL)
			{
				// Gate %s: Orig/Dest restriction (%s): %s
				olConfText.Format(GetString(IDS_STRING2006), opGat, prlGatRes->OrgDesUserStr, olOrgDes3+"/"+olOrgDes4);
				AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING2006, olConfText, *popKonfList);
				blRet = false;
			}
			else
			{
				return false;
			}
		}
	}





	//// Position - Gate Relation
	if (ogKonflikte.IsConflictActivated(IDS_STRING2001))
	{
		if (!prlGatRes->RelPos1.IsEmpty() || !prlGatRes->RelPos2.IsEmpty())
		{
			bool blNoConf = true;
			if (cpFPart == 'A')
			{
				if (strlen(prpFlight->Psta) > 0)
				{
					if (strcmp(prlGatRes->RelPos1, prpFlight->Psta) != 0 &&
						strcmp(prlGatRes->RelPos2, prpFlight->Psta) != 0)
						blNoConf = false;
				}
			}
			if (cpFPart == 'D')
			{
				if (strlen(prpFlight->Pstd) > 0)
				{
					if (strcmp(prlGatRes->RelPos1, prpFlight->Pstd) != 0 &&
						strcmp(prlGatRes->RelPos2, prpFlight->Pstd) != 0)
						blNoConf = false;
				}
			}

			if (!blNoConf)
			{
				if(popKonfList != NULL)
				{
					//Invalid Position - Gate Relation!
					AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING2001, GetString(IDS_STRING2001), *popKonfList);
					blRet = false;
				}
				else
				{
					return false;
				}
			}
		}
	}


	//// BoardingBridge - BusGate Relation
	PST_RESLIST *prlPosRes;
		
	bool blNoConf = true;
	if (cpFPart == 'A')
	{
		if (strlen(prpFlight->Psta) > 0)
		{
			prlPosRes = GetPstRes(prpFlight->Psta);
			if (prlPosRes != NULL)
			{
				if ((strcmp(prlPosRes->Brgs, "X") == 0 && strcmp(prlGatRes->Busg, "X") == 0) || 
					(strcmp(prlPosRes->Brgs, "X") != 0 && strcmp(prlGatRes->Busg, "X") != 0))
					blNoConf = false;
			}
		}
	}
	if (cpFPart == 'D')
	{
		if (strlen(prpFlight->Pstd) > 0)
		{
			prlPosRes = GetPstRes(prpFlight->Pstd);
			if (prlPosRes != NULL)
			{
				if ((strcmp(prlPosRes->Brgs, "X") == 0 && strcmp(prlGatRes->Busg, "X") == 0) || 
					(strcmp(prlPosRes->Brgs, "X") != 0 && strcmp(prlGatRes->Busg, "X") != 0))
					blNoConf = false;
			}
		}
	}

	if (!blNoConf)
	{
		if(popKonfList != NULL)
		{
			// Invalid 'Pass. Boarding Bridge' - 'Bus Gate' Relation!
			AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING2065, GetString(IDS_STRING2065), *popKonfList);
			blRet = false;
		}
		else
		{
			return false;
		}
	}
	




	return blRet;
}






static int CompareAutoAllocate(const DiaCedaFlightData::RKEYLIST **e1, const DiaCedaFlightData::RKEYLIST **e2)
{
	int ilOrd1 = -1;
	int ilOrd2 = -1;
	
	CString olOrder = CString(" DRIM");

	CTime olTifa1 = TIMENULL;
	CTime olTifd1 = TIMENULL;
	CTime olTifa2 = TIMENULL;
	CTime olTifd2 = TIMENULL;


	for(int i = (**e1).Rotation.GetSize() - 1; i >= 0; i--)
	{
		ilOrd1 = max(ilOrd1, olOrder.Find(CString((**e1).Rotation[i].Flti) ));

		if(CString((**e1).Rotation[i].Adid) == "A")
			olTifa1 = (**e1).Rotation[i].Tifa;
		else
			olTifd1 = (**e1).Rotation[i].Tifd;

	}


	for( i = (**e2).Rotation.GetSize() - 1; i >= 0; i--)
	{
		ilOrd2 = max(ilOrd2, olOrder.Find(CString((**e2).Rotation[i].Flti) ));

		if(CString((**e2).Rotation[i].Adid) == "A")
			olTifa2 = (**e2).Rotation[i].Tifa;
		else
			olTifd2 = (**e2).Rotation[i].Tifd;
	
	}

	
	CTimeSpan olSpan1;
	CTimeSpan olSpan2;

	if(olTifa2 == TIMENULL || olTifd2 == TIMENULL)
		olSpan2 = CTimeSpan(0,0,30,0);
	else
		olSpan2 = olTifd2 - olTifa2;


	if(olTifa1 == TIMENULL || olTifd1 == TIMENULL)
		olSpan1 = CTimeSpan(0,0,30,0);
	else
		olSpan1 = olTifd1 - olTifa1;


	if(ilOrd1 == ilOrd2)
	{
		return (olSpan1 < olSpan2) ? 1 : -1;
	}
	else
	{
		return (ilOrd1 > ilOrd2) ? 1 : -1;
	}
}



void SpotAllocation::CreateFlightList()
{

	POSITION pos;
	void *pVoid;
	DiaCedaFlightData::RKEYLIST *prlRkey;
	DiaCedaFlightData::RKEYLIST *prlRkey2;
	DIAFLIGHTDATA *prlFlightA = NULL;
	DIAFLIGHTDATA *prlFlightD = NULL;


	omFlights.DeleteAll();

	for( pos = ogPosDiaFlightData.omRkeyMap.GetStartPosition(); pos != NULL; )
	{
		ogPosDiaFlightData.omRkeyMap.GetNextAssoc( pos, pVoid , (void *&)prlRkey );

		prlFlightA = ogPosDiaFlightData.GetFlightAInRotation(prlRkey);
		prlFlightD = ogPosDiaFlightData.GetFlightDInRotation(prlRkey);

		if (prlFlightA && CheckPostFlightPosDia(prlFlightA->Urno, NULL))
			continue;
		if (prlFlightD && CheckPostFlightPosDia(prlFlightD->Urno, NULL))
			continue;


		if(prlFlightA != NULL)
		{
			// !!! NO CIRCULAR FLIGHTS (AND ALL CONNECTED) !!!
			if((CString("SOTG ").Find(CString(prlFlightA->Ftyp)) < 0) ||
				(prlFlightA->Adid[0] == 'B'))
				prlFlightA = NULL;
		}

		if(prlFlightD != NULL)
		{
			// !!! NO CIRCULAR FLIGHTS (AND ALL CONNECTED) !!!
			if((CString("SOTG ").Find(CString(prlFlightD->Ftyp)) < 0) ||
				(prlFlightD->Adid[0] == 'B'))
				prlFlightD = NULL;
		}


		if((prlFlightA != NULL) || 	(prlFlightD != NULL))
		{

			prlRkey2 = new DiaCedaFlightData::RKEYLIST;

			if(prlFlightA != NULL)
				prlRkey2->Rotation.Add(prlFlightA);

			if(prlFlightD != NULL)
				prlRkey2->Rotation.Add(prlFlightD);

			omFlights.Add(prlRkey2);

		}

	}

	omFlights.Sort(CompareAutoAllocate);

	omChangedFlightsRkeyMap.RemoveAll();

}

  
bool SpotAllocation::AutoPstAllocate()
{

 	DiaCedaFlightData::RKEYLIST *prlRkey;
	DIAFLIGHTDATA *prlFlightA = NULL;
 	DIAFLIGHTDATA *prlFlightD = NULL;
 
	CTime olStart;
	CTime olEnd;

 
	pomParent->SendMessage(WM_SAS_PROGESS_INIT, omFlights.GetSize(), 0 );



	TRACE("\n=======================================");


	CString olRecomPos;
	for(int k = omFlights.GetSize()-1; k >= 0 ; k--)
	{
		olRecomPos.Empty();
		prlRkey = &omFlights[k];

 
		prlFlightA = ogPosDiaFlightData.GetFlightAInRotation(prlRkey);
		prlFlightD = ogPosDiaFlightData.GetFlightDInRotation(prlRkey);
	
		if(prlFlightD != NULL)
		{
			if(!CString(prlFlightD->Pstd).IsEmpty())
			{
				// the recommended position is the position of the departure flight
				olRecomPos = prlFlightD->Pstd;
				prlFlightD = NULL;
			}
		}

		if(prlFlightA != NULL)
		{
			if(!CString(prlFlightA->Psta).IsEmpty())
			{
				// the recommended position is the position of the arrival flight
				olRecomPos = prlFlightA->Psta;
				prlFlightA = NULL;
			}
		}

/*
		if(prlFlightA != NULL)
		{
			TRACE("\nPOS: %d %s %s", k, prlFlightA->Flno, prlFlightA->Flti);
		}

		if(prlFlightD != NULL)
		{
			TRACE("\nPOS: %d %s %s", k, prlFlightD->Flno, prlFlightD->Flti);
		}
		
*/

		int ilCount = 1;
		
		if(prlFlightD != NULL && prlFlightA != NULL)
		{
			CTimeSpan olDiff;
			olDiff = prlFlightD->Tifd - prlFlightA->Tifa;

			if(olDiff.GetTotalMinutes() > 1440)
			{				
				ilCount = 2;
				olRecomPos.Empty();
			}
		}


		if(prlFlightD != NULL || prlFlightA != NULL)
		{

			if (ilCount == 1)
			{
				// Allocate one position
				AutoPstAllocateFlights(prlFlightA, prlFlightD, olRecomPos);
			}
			else if (ilCount == 2)
			{
				// Allocate two positions
				AutoPstAllocateFlights(prlFlightA, NULL, olRecomPos);
 				AutoPstAllocateFlights(NULL, prlFlightD, olRecomPos);
			}

		}
		pomParent->SendMessage(WM_SAS_PROGESS_UPDATE, 1, 0 );


	}
	return true;
}



bool SpotAllocation::AutoPstAllocateFlights(DIAFLIGHTDATA *prlFlightA, DIAFLIGHTDATA *prlFlightD, const CString &ropRecomPos)
{
	
	PST_RESLIST *prlPstRes = NULL;

	/*
	DIAFLIGHTDATA *prlFlight = NULL;
	if(prlFlightA != NULL)
	{
		prlFlight = prlFlightA;
	}
	else
	{
		prlFlight = prlFlightD;
	}
	*/


	// At first: try to allocate the recommended position
	if (!ropRecomPos.IsEmpty())
	{
		/*
		// there is only a recommended position if the arrival or departure part of an rotation
		// has allready a position! Also in this case 'prlFlightA' or 'prlFlightD' must be NULL! 
		CTimeSpan olZeroSpan(0, 0, 0, 0);
		CTimeSpan *polLeftBuf, *polRightBuf;
		if (prlFlightD == NULL)
		{
			polLeftBuf = &ogPosAllocBufferTime;
			polRightBuf = &olZeroSpan;
		}
		if (prlFlightA == NULL)
		{
			polLeftBuf = &olZeroSpan;
			polRightBuf = &ogPosAllocBufferTime;
		}

		if(CheckPst(prlFlightA, prlFlightD, ropRecomPos, NULL, polLeftBuf, polRightBuf))
			*/
		if(CheckPst(prlFlightA, prlFlightD, ropRecomPos, NULL, &omParameters.omPstRightBuffer, &omParameters.omPstLeftBuffer))
		{
			AllocatePst(prlFlightA, prlFlightD, ropRecomPos);
			return true;
		}
	}


	/*
	// then try the preferred airlines
	for(int i = 0; i < omPstRules.GetSize(); i++)
	{
		prlPstRes = &omPstRules[i];

		if(!prlPstRes->Pral.IsEmpty())
		{
			if((prlPstRes->Pral.Find( CString(";") + CString(prlFlight->Alc3) + CString(";")) >= 0) ||
			   (prlPstRes->Pral.Find( CString(";") + CString(prlFlight->Alc2) + CString(";")) >= 0) )
			{
				if(CheckPst(prlFlightA, prlFlightD, prlPstRes->Pnam, NULL, &omParameters.omPstRightBuffer, &omParameters.omPstLeftBuffer))
				{
					AllocatePst(prlFlightA, prlFlightD, prlPstRes->Pnam);
					return true;
				}
			}
		}
	}
	*/

	// then collect all possible positions 
	CPtrArray polPossiblePstRules;
 	for(int i = 0; i < omPstRules.GetSize(); i++)
	{
		prlPstRes = &omPstRules[i];

		if(CheckPst(prlFlightA, prlFlightD, prlPstRes->Pnam, NULL, &omParameters.omPstRightBuffer, &omParameters.omPstLeftBuffer))
		{
			polPossiblePstRules.Add(prlPstRes);
		}
	}		

	if (polPossiblePstRules.GetSize() < 1) 
		return false;

	// get the best possible position
	PST_RESLIST *prlBestPstRule = (PST_RESLIST *) polPossiblePstRules[0];
	float flBestGoodCount;
	int ilBestBadCount;
	GetMatchCountPst(prlFlightA, prlFlightD, prlBestPstRule->PreferredAL, prlBestPstRule->PreferredAC, prlBestPstRule->PreferredNA, flBestGoodCount, ilBestBadCount);  

	float flCurrGoodCount;
	int ilCurrBadCount;
	for (i = 1; i < polPossiblePstRules.GetSize(); i++)
	{
		prlPstRes = (PST_RESLIST *) polPossiblePstRules[i];
		GetMatchCountPst(prlFlightA, prlFlightD, prlPstRes->PreferredAL, prlPstRes->PreferredAC, prlPstRes->PreferredNA, flCurrGoodCount, ilCurrBadCount);
		if ((ilCurrBadCount < ilBestBadCount) ||
			(ilCurrBadCount == ilBestBadCount && flCurrGoodCount > flBestGoodCount))
		{
			ilBestBadCount = ilCurrBadCount;
			flBestGoodCount = flCurrGoodCount;
			prlBestPstRule = prlPstRes;
		}
	}

	// assign the best position to the flight(s)
	if (prlBestPstRule != NULL)
	{
		AllocatePst(prlFlightA, prlFlightD, prlBestPstRule->Pnam);
		return true;
	} 

	return false;
}


bool SpotAllocation::GetMatchCountPst(const DIAFLIGHTDATA *prpFlightA, const DIAFLIGHTDATA *prpFlightD, const CStringArray &ropPreferredAL, const CStringArray &ropPreferredAC, const CStringArray &ropPreferredNA,
									  float &rfpGoodCount, int &ripBadCount) const
{
	rfpGoodCount = 0;
	ripBadCount = 0;

	if (prpFlightA == NULL && prpFlightD == NULL)
		return false;

	int ilPos;
	const DIAFLIGHTDATA *prlFlight = NULL;
	
	for (int i = 0; i < 2; i++)
	{
		// check arrival and departure flight
		if (i == 0)
			prlFlight = prpFlightA;
		else
			prlFlight = prpFlightD;

		if (prlFlight != NULL)
		{		
			// check preferred nature codes
			ilPos = FindIdInStrArray(ropPreferredNA, CString(prlFlight->Ttyp)); 
			if (ilPos > -1)
			{
				rfpGoodCount += 1/(float)ilPos;
			}
			else if (ropPreferredNA.GetSize() > 0)
			{
				ripBadCount++;
			}

			// check preferred airline codes			
			if ((ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc2))) == -1)
				ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc3)); 
			if (ilPos > -1)
			{
				rfpGoodCount += 1/(float)ilPos;
			}
			else if (ropPreferredAL.GetSize() > 0)
			{
				ripBadCount++;
			}

			// check preferred aircraft type
			if ((ilPos = FindIdInStrArray(ropPreferredAC, CString(prlFlight->Act3))) == -1)
				ilPos = FindIdInStrArray(ropPreferredAC, CString(prlFlight->Act5)); 
			if (ilPos > -1)
			{
				rfpGoodCount += 1/(float)ilPos;
			}
			else if (ropPreferredAC.GetSize() > 0)
			{
				ripBadCount++;
			}
		}
	}

	return true;
}


bool SpotAllocation::GetMatchCountGat(const DIAFLIGHTDATA *prpFlight, const CStringArray &ropPreferredAL, const CStringArray &ropPreferredOrgDes, const CStringArray &ropPreferredNA, 
									  float &rfpGoodCount, int &ripBadCount) const
{
	rfpGoodCount = 0;
	ripBadCount = 0;
	
	if (prpFlight == NULL)
		return false;

	int ilPos;
	// check preferred nature codes
	ilPos = FindIdInStrArray(ropPreferredNA, CString(prpFlight->Ttyp));
	if (ilPos > -1)
	{
		rfpGoodCount += 1/(float)ilPos;
	}
	else if (ropPreferredNA.GetSize() > 0)
	{
		ripBadCount++;
	}

	// check preferred airline codes
	if ((ilPos=FindIdInStrArray(ropPreferredAL, CString(prpFlight->Alc2))) == -1)
		ilPos=FindIdInStrArray(ropPreferredAL, CString(prpFlight->Alc3));
	if (ilPos > -1)
	{
		rfpGoodCount += 1/(float)ilPos;
	}
	else if (ropPreferredAL.GetSize() > 0)
	{
		ripBadCount++;
	}

	// check preferred origins/destinations
	CString olOrgDes3;
	CString olOrgDes4;
	if (IsArrivalFlight(prpFlight->Org3, prpFlight->Des3, prpFlight->Ftyp[0]))
	{
		olOrgDes3 = prpFlight->Org3;
		olOrgDes4 = prpFlight->Org4;
	}
	else
	{
		olOrgDes3 = prpFlight->Des3;
		olOrgDes4 = prpFlight->Des4;
	}
	if ((ilPos=FindIdInStrArray(ropPreferredOrgDes, olOrgDes3)) == -1)
		ilPos=FindIdInStrArray(ropPreferredOrgDes, olOrgDes4);
	if (ilPos > -1) 
	{
		rfpGoodCount += 1/(float)ilPos;
	}
	else if (ropPreferredOrgDes.GetSize() > 0)
	{
		ripBadCount++;
	}

	return true;
}





bool SpotAllocation::GetPositionsByRule(DIAFLIGHTDATA *prlFlightA, DIAFLIGHTDATA *prlFlightD, CStringArray &opPsts )
{
	PST_RESLIST *prlPstRes = NULL;
	DIAFLIGHTDATA *prlFlight;

	if(prlFlightA != NULL)
	{
		prlFlight = prlFlightA;
	}
	else
	{
		prlFlight = prlFlightD;
	}


	if(prlFlightD != NULL || prlFlightA != NULL)
	{
		for( int i = 0; i < omPstRules.GetSize(); i++)
		{
			prlPstRes = &omPstRules[i];

			if(CheckPst(prlFlightA, prlFlightD, prlPstRes->Pnam, NULL))
			{
				opPsts.Add(prlPstRes->Pnam);
			}
		}
	}
	return true;
}


bool SpotAllocation::GetBeltsByRule(DIAFLIGHTDATA *prlFlight, CStringArray &opBelts)
{
	if (prlFlight == NULL) return false;
	
	opBelts.RemoveAll();

 	int ilBnam = ogBCD.GetFieldIndex("BLT", "BNAM");

	// scan all belts
	for(int i = 0; i < ogBCD.GetDataCount("BLT"); i++)
	{
		RecordSet rlRec;
		ogBCD.GetRecord("BLT", i, rlRec);
		// check belt
		if(CheckBlt(prlFlight, rlRec[ilBnam], 1, NULL, 0))
		{
 			opBelts.Add(rlRec[ilBnam]);
		}
	}

	return true;
}


bool SpotAllocation::GetWrosByRule(DIAFLIGHTDATA *prlFlight, CStringArray &opWros)
{
	if (prlFlight == NULL) return false;
	
	opWros.RemoveAll();

 	int ilNameId = ogBCD.GetFieldIndex("WRO", "WNAM");

	// scan all waiting rooms
	for(int i = 0; i < ogBCD.GetDataCount("WRO"); i++)
	{
		RecordSet rlRec;
		ogBCD.GetRecord("WRO", i, rlRec);
		// check wro
		if(CheckWro(prlFlight, rlRec[ilNameId],1, NULL, true))
		{
 			opWros.Add(rlRec[ilNameId]);
		}
	}

	return true;
}



bool SpotAllocation::GetGatesByRule(DIAFLIGHTDATA *prlFlight, char cpFPart, CStringArray &opGates )
{
	if(prlFlight == NULL )
		return true;
	
	GAT_RESLIST *prlGatRes = NULL;

	for( int i = 0; i < omGatRules.GetSize(); i++)
	{
		prlGatRes = &omGatRules[i];

		if(CheckGat(prlFlight, cpFPart, prlGatRes->Gnam,1, NULL))
		{
			opGates.Add(prlGatRes->Gnam);
		}
	}

	return true;
}






bool SpotAllocation::AllocatePst(DIAFLIGHTDATA *prlFlightA, DIAFLIGHTDATA *prlFlightD, CString opPst)
{
 
	// store the choosen Position to the flight record in the local dataset!
	if(prlFlightD != NULL)
	{
		ogPosDiaFlightData.DeleteFromKeyMap(prlFlightD);
		strcpy(prlFlightD->Pstd, opPst);
		ogPosDiaFlightData.AddToKeyMap(prlFlightD);
		omChangedFlightsRkeyMap.SetAt((void *)prlFlightD->Rkey, NULL);

	}
	if(prlFlightA != NULL)
	{
		ogPosDiaFlightData.DeleteFromKeyMap(prlFlightA);
		strcpy(prlFlightA->Psta, opPst);
		ogPosDiaFlightData.AddToKeyMap(prlFlightA);
		omChangedFlightsRkeyMap.SetAt((void *)prlFlightA->Rkey, NULL);
	}


	// move the rule for that position to the end of the list of the rules with the same priority
	PST_RESLIST *prlPstRes = NULL;
	int ilCurrPrio = 0;

	for(int i = 0; i < omPstRules.GetSize(); i++)
	{
		if(omPstRules[i].Pnam == opPst)
		{
			prlPstRes = &omPstRules[i];
			ilCurrPrio = omPstRules[i].Prio;
			omPstRules.RemoveAt(i);
			break;

		}
	}

	bool blTreffer = false;
	for(int j = i; j < omPstRules.GetSize(); j++)
	{
		if(omPstRules[j].Prio != ilCurrPrio)
		{
			omPstRules.InsertAt(j, prlPstRes);
			blTreffer = true;
			break;
		}
	}

	if(!blTreffer)
	{
		omPstRules.InsertAt(omPstRules.GetSize(), prlPstRes);
	}


	return true;
}




bool SpotAllocation::AutoGatAllocate()
{
	AutoGatAllocatePst();
	//AutoGatAllocateWithoutPst();
	return true;
}



bool SpotAllocation::AutoGatAllocatePst()
{
	DiaCedaFlightData::RKEYLIST *prlRkey;
	DIAFLIGHTDATA *prlFlightA = NULL;
	DIAFLIGHTDATA *prlFlightD = NULL;

	pomParent->SendMessage(WM_SAS_PROGESS_INIT, omFlights.GetSize(), 0 );

	TRACE("\n=======================================");


	for(int k = omFlights.GetSize()-1; k >= 0 ; k--)
	{
		prlRkey = &omFlights[k];

		prlFlightA = ogPosDiaFlightData.GetFlightAInRotation(prlRkey);
		prlFlightD = ogPosDiaFlightData.GetFlightDInRotation(prlRkey);
	
		if(prlFlightD != NULL)
		{
			if(strlen(prlFlightD->Gtd1) != 0)
				prlFlightD = NULL;
			if(prlFlightD != NULL && strlen(prlFlightD->Pstd) == 0)
				prlFlightD = NULL;
		}
		if(prlFlightA != NULL)
		{
			if(strlen(prlFlightA->Gta1) != 0)
				prlFlightA = NULL;
			if(prlFlightA != NULL && strlen(prlFlightA->Psta) == 0)
				prlFlightA = NULL;
		}

		if(prlFlightD == NULL && prlFlightA == NULL)
			continue;


		int ilCount = 1;
		if(prlFlightD != NULL && prlFlightA != NULL)
		{
			CTimeSpan olDiff;
			olDiff = prlFlightD->Tifd - prlFlightA->Tifa;

			if(olDiff.GetTotalMinutes() > 1440 || strcmp(prlFlightA->Psta, prlFlightD->Pstd) != 0 )
			{
				ilCount = 2;
			}
		}


		/*
		if (ilCount == 1)
		{
			AutoGatAllocatePstFlight(prlFlightA, prlFlightD);
		}
		else
		*/
		{
			AutoGatAllocatePstFlight(prlFlightA, NULL);
			AutoGatAllocatePstFlight(NULL, prlFlightD);
		}


		pomParent->SendMessage(WM_SAS_PROGESS_UPDATE, prlRkey->Rotation.GetSize(), 0 );
	}
	return true;
}



bool SpotAllocation::AutoGatAllocatePstFlight(DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD)
{
	if (prpFlightA == NULL && prpFlightD == NULL)
		return false;

	bool blTrefferA = false; 
	bool blTrefferD = false;


	if(prpFlightD != NULL && prpFlightA != NULL) 					
	{
		CString olADGat1;
		if (GetBestGate(CString(""), prpFlightA, prpFlightD, 1, &omParameters.omGatLeftBuffer, &omParameters.omGatRightBuffer, olADGat1) == true)
		{
			AllocateGat(NULL, prpFlightD, olADGat1);
			AllocateGat(prpFlightA, NULL, olADGat1);
			blTrefferA = true;
			blTrefferD = true;
		}

	}


	if(prpFlightD != NULL && !blTrefferD)
	{
		CString olGat1;
		if (GetBestGate(CString(""), prpFlightD, 'D', 1, &omParameters.omGatLeftBuffer, &omParameters.omGatRightBuffer, olGat1) == true)
		{
			AllocateGat(NULL, prpFlightD, olGat1);
			blTrefferD = true;
		}
	}
	if(prpFlightA != NULL && !blTrefferA)
	{
		CString olGat1;
		if (GetBestGate(CString(""), prpFlightA, 'A', 1, &omParameters.omGatLeftBuffer, &omParameters.omGatRightBuffer, olGat1) == true)
		{
			AllocateGat(prpFlightA, NULL, olGat1);
			blTrefferA = true;
		}
	}


	return true;
}



bool SpotAllocation::GetBestGate(const CString &ropFtliGat, const DIAFLIGHTDATA *prpFlight, char cpFPart,
								 int ipGateNo, const CTimeSpan *popGatLeftBuffer, const CTimeSpan *popGatRightBuffer, CString &ropBestGate)
{
	if (prpFlight == NULL)
		return false;

	if (ipGateNo != 1 && ipGateNo != 2)
		return false;

	ropBestGate.Empty();
	
	// collect all possible gates
	CPtrArray olPossibleGatesRules;	
	CString olCurrGate;
	GAT_RESLIST *prlGatRes;

	int ilGatCount = ogBCD.GetDataCount("GAT");
	
	for( int i = 0; i < ilGatCount; i++)
	{
		olCurrGate = ogBCD.GetField("GAT", i, "GNAM");

		if(ropFtliGat.IsEmpty() || CheckGatFlightId( olCurrGate, ropFtliGat))							
		{
			if(CheckGat(prpFlight, cpFPart, olCurrGate, ipGateNo, NULL, popGatLeftBuffer, popGatRightBuffer))
			{
				prlGatRes = GetGatRes(olCurrGate);
				if (prlGatRes != NULL)
					olPossibleGatesRules.Add(prlGatRes);
			}
		}
	}

	if (olPossibleGatesRules.GetSize() < 1) 
		return false;

	//// get the best possible gate
	GAT_RESLIST *prlBestGatRule = (GAT_RESLIST *) olPossibleGatesRules[0];
	float flBestGoodCount;
	int ilBestBadCount;
	GetMatchCountGat(prpFlight, prlBestGatRule->PreferredAL, prlBestGatRule->PreferredOrgDes, prlBestGatRule->PreferredNA, flBestGoodCount, ilBestBadCount);  
		
	float flCurrGoodCount;
	int ilCurrBadCount;
	for (i = 1; i < olPossibleGatesRules.GetSize(); i++)
	{
		prlGatRes = (GAT_RESLIST *) olPossibleGatesRules[i];
		GetMatchCountGat(prpFlight, prlGatRes->PreferredAL, prlGatRes->PreferredOrgDes, prlGatRes->PreferredNA, flCurrGoodCount, ilCurrBadCount);
		// Is the examined gate better?
		if ((ilCurrBadCount < ilBestBadCount) ||
			(ilCurrBadCount == ilBestBadCount && flCurrGoodCount > flBestGoodCount))
		{
			ilBestBadCount = ilCurrBadCount;
			flBestGoodCount = flCurrGoodCount;
			prlBestGatRule = prlGatRes;
		}
	}

	// return the best gate
	if (prlBestGatRule != NULL)
	{
		ropBestGate = prlBestGatRule->Gnam;
		return true;
	} 


	return false;
}



bool SpotAllocation::GetBestGate(const CString &ropFtliGat, const DIAFLIGHTDATA *prpFlightA, const DIAFLIGHTDATA *prpFlightD,
								 int ipGateNo, const CTimeSpan *popGatLeftBuffer, const CTimeSpan *popGatRightBuffer, CString &ropBestGate)
{
	if (prpFlightA == NULL && prpFlightD == NULL)
		return false;

	if (ipGateNo != 1 && ipGateNo != 2)
		return false;

	ropBestGate.Empty();

	// collect all possible gates
	CPtrArray olPossibleGatesRules;	
	CString olCurrGate;
	GAT_RESLIST *prlGatRes;


	int ilGatCount = ogBCD.GetDataCount("GAT");

	for(int i = 0; i < ilGatCount; i++)
	{
		olCurrGate = ogBCD.GetField("GAT", i, "GNAM");

		if(ropFtliGat.IsEmpty() || CheckGatFlightId( olCurrGate, ropFtliGat))							
		{
			if(CheckGat(prpFlightD, 'D', olCurrGate, ipGateNo, NULL, NULL, popGatRightBuffer ) && 
			   CheckGat(prpFlightA, 'A', olCurrGate, ipGateNo, NULL, popGatLeftBuffer, NULL))
			{
				prlGatRes = GetGatRes(olCurrGate);
				if (prlGatRes != NULL)
					olPossibleGatesRules.Add(prlGatRes);
			}
		}
	}

	if (olPossibleGatesRules.GetSize() < 1) 
		return false;

	///// get the best possible gate
	GAT_RESLIST *prlBestGatRule = (GAT_RESLIST *) olPossibleGatesRules[0];

	float flBestGoodCount;
	float flGoodCTmp;
	int ilBestBadCount;
	int ilBadCTmp;

	// calculate benchmark for gate
	GetMatchCountGat(prpFlightA, prlBestGatRule->PreferredAL, prlBestGatRule->PreferredOrgDes, prlBestGatRule->PreferredNA, flGoodCTmp, ilBadCTmp);
	flBestGoodCount = flGoodCTmp;
	ilBestBadCount = ilBadCTmp;
	GetMatchCountGat(prpFlightD, prlBestGatRule->PreferredAL, prlBestGatRule->PreferredOrgDes, prlBestGatRule->PreferredNA, flGoodCTmp, ilBadCTmp);
	flBestGoodCount += flGoodCTmp;
	ilBestBadCount += ilBadCTmp;

	float flCurrGoodCount;
	int ilCurrBadCount;
	for (i = 1; i < olPossibleGatesRules.GetSize(); i++)
	{
		// calculate benchmark for gate
		prlGatRes = (GAT_RESLIST *) olPossibleGatesRules[i];
		GetMatchCountGat(prpFlightA, prlGatRes->PreferredAL, prlGatRes->PreferredOrgDes, prlGatRes->PreferredNA, flGoodCTmp, ilBadCTmp);
		flCurrGoodCount = flGoodCTmp;
		ilCurrBadCount = ilBadCTmp;
		GetMatchCountGat(prpFlightD, prlGatRes->PreferredAL, prlGatRes->PreferredOrgDes, prlGatRes->PreferredNA, flGoodCTmp, ilBadCTmp);
		flCurrGoodCount += flGoodCTmp;
		ilCurrBadCount += ilBadCTmp;

		// Is the examined gate better?
		if ((ilCurrBadCount < ilBestBadCount) ||
			(ilCurrBadCount == ilBestBadCount && flCurrGoodCount > flBestGoodCount))
		{
			ilBestBadCount = ilCurrBadCount;
			flBestGoodCount = flCurrGoodCount;
			prlBestGatRule = prlGatRes;
		}
	}

	// return the best gate
	if (prlBestGatRule != NULL)
	{
		ropBestGate = prlBestGatRule->Gnam;
		return true;
	} 



	return false;
}





/*
bool SpotAllocation::AutoGatAllocateWithoutPst()
{

	DiaCedaFlightData::RKEYLIST *prlRkey;
	DIAFLIGHTDATA *prlFlightA = NULL;
	DIAFLIGHTDATA *prlFlight = NULL;
	DIAFLIGHTDATA *prlFlightD = NULL;
	GAT_RESLIST *prlGatRes = NULL;


	pomParent->SendMessage(WM_SAS_PROGESS_INIT, omFlights.GetSize(), 0 );

	int ilRotCount ;

	TRACE("\n=======================================");

	for(int k = omFlights.GetSize()-1; k >= 0 ; k--)
	{
		prlRkey = &omFlights[k];

		ilRotCount = prlRkey->Rotation.GetSize();

		prlFlightA = ogPosDiaFlightData.GetFlightAInRotation(prlRkey);
		prlFlightD = ogPosDiaFlightData.GetFlightDInRotation(prlRkey);
	
		if(prlFlightD != NULL)
		{
			if(!CString(prlFlightD->Gtd1).IsEmpty())
				prlFlightD = NULL;
			if(!CString(prlFlightD->Pstd).IsEmpty())
				prlFlightD = NULL;
		}
		if(prlFlightA != NULL)
		{
			if(!CString(prlFlightA->Gta1).IsEmpty())
				prlFlightA = NULL;
			if(!CString(prlFlightA->Psta).IsEmpty())
				prlFlightA = NULL;
		}

		if(prlFlightA != NULL)
		{
			prlFlight = prlFlightA;
		}
		else
		{
			prlFlight = prlFlightD;
		}

		int ilCount = 1;
		
		if(prlFlightD != NULL && prlFlightA != NULL)
		{
			CTimeSpan olDiff;
			olDiff = prlFlightD->Tifd - prlFlightA->Tifa;

			if(olDiff.GetTotalMinutes() > 1440)
			{
				ilCount = 2;
			}
		}


		if(prlFlightD != NULL || prlFlightA != NULL)
		{
			for(int j = 0; j < ilCount; j++)
			{
				if(ilCount == 2 && j == 0)
				{
					prlFlightD = NULL;
				}
	
				if(ilCount == 2 && j == 1)
				{
					prlFlightA = NULL;
					prlFlight = prlFlightD = ogPosDiaFlightData.GetFlightDInRotation(prlRkey);
				}
				
				bool blTrefferA = false;
				bool blTrefferD = false;

				if((prlFlightA != NULL) && (prlFlightD != NULL))
				{
					for(int i = 0; i < omGatRules.GetSize(); i++)
					{
						prlGatRes = &omGatRules[i];

						if(!prlGatRes->Pral.IsEmpty())
						{
							if((prlGatRes->Pral.Find( CString(";") + CString(prlFlightA->Alc3) + CString(";")) >= 0) ||
							   (prlGatRes->Pral.Find( CString(";") + CString(prlFlightA->Alc2) + CString(";")) >= 0) ||
							   (prlGatRes->Pral.Find( CString(";") + CString(prlFlightD->Alc3) + CString(";")) >= 0) ||
							   (prlGatRes->Pral.Find( CString(";") + CString(prlFlightD->Alc2) + CString(";")) >= 0) )
							{
								if(CheckGat(prlFlightD,prlGatRes->Gnam,1, NULL, NULL, &omParameters.omGatRightBuffer ) && CheckGat(prlFlightA,prlGatRes->Gnam,1, NULL, &omParameters.omGatLeftBuffer, NULL))
								{
									AllocateGat(NULL, prlFlightD, prlGatRes->Gnam);
									AllocateGat(prlFlightA,NULL, prlGatRes->Gnam);
									blTrefferA = true;
									blTrefferD = true;
									break;
								}
							}
						}
					}
					if(!blTrefferA)
					{
						for(int i = 0; i < omGatRules.GetSize(); i++)
						{
							prlGatRes = &omGatRules[i];

							if(prlGatRes->Pral.IsEmpty())
							{
								if(CheckGat(prlFlightD,prlGatRes->Gnam,1, NULL, NULL, &omParameters.omGatRightBuffer ) && CheckGat(prlFlightA,prlGatRes->Gnam,1, NULL, &omParameters.omGatLeftBuffer, NULL))
								{
									AllocateGat(NULL, prlFlightD, prlGatRes->Gnam);
									AllocateGat(prlFlightA,NULL, prlGatRes->Gnam);
									blTrefferA = true;
									blTrefferD = true;
									break;
								}
							}
						}
					}

				}


				if(prlFlightA != NULL && !blTrefferA)
				{
					for(int i = 0; i < omGatRules.GetSize(); i++)
					{
						prlGatRes = &omGatRules[i];

						if(!prlGatRes->Pral.IsEmpty())
						{
							if((prlGatRes->Pral.Find( CString(";") + CString(prlFlightA->Alc3) + CString(";")) >= 0) ||
							   (prlGatRes->Pral.Find( CString(";") + CString(prlFlightA->Alc2) + CString(";")) >= 0) )
							{
								if(CheckGat(prlFlightA, prlGatRes->Gnam,1, NULL, &omParameters.omGatLeftBuffer, &omParameters.omGatRightBuffer))
								{
									AllocateGat(prlFlightA,NULL,  prlGatRes->Gnam);
									blTrefferA = true;
									break;
								}
		
							}
						}
					}
					if(!blTrefferA)
					{
						for(int i = 0; i < omGatRules.GetSize(); i++)
						{
							prlGatRes = &omGatRules[i];

							if(prlGatRes->Pral.IsEmpty())
							{
								if(CheckGat(prlFlightA, prlGatRes->Gnam,1, NULL, &omParameters.omGatLeftBuffer, &omParameters.omGatRightBuffer))
								{
									AllocateGat(prlFlightA,NULL,  prlGatRes->Gnam);
									blTrefferA = true;
									break;
								}
							}
						}
					}
				}

				if(prlFlightD != NULL && !blTrefferD)
				{
					for(int i = 0; i < omGatRules.GetSize(); i++)
					{
						prlGatRes = &omGatRules[i];

						if(!prlGatRes->Pral.IsEmpty())
						{
							if((prlGatRes->Pral.Find( CString(";") + CString(prlFlightD->Alc3) + CString(";")) >= 0) ||
							   (prlGatRes->Pral.Find( CString(";") + CString(prlFlightD->Alc2) + CString(";")) >= 0) )
							{
								if(CheckGat(prlFlightD, prlGatRes->Gnam,1, NULL, &omParameters.omGatLeftBuffer, &omParameters.omGatRightBuffer))
								{
									AllocateGat(NULL,prlFlightD,  prlGatRes->Gnam);
									blTrefferD = true;
									break;
								}
							}
						}
					}
					if(!blTrefferD)
					{
						for(int i = 0; i < omGatRules.GetSize(); i++)
						{
							prlGatRes = &omGatRules[i];

							if(prlGatRes->Pral.IsEmpty())
							{
								if(CheckGat(prlFlightD, prlGatRes->Gnam,1, NULL, &omParameters.omGatLeftBuffer, &omParameters.omGatRightBuffer))
								{
									AllocateGat(NULL,prlFlightD,  prlGatRes->Gnam);
									blTrefferD = true;
									break;
								}
							}
						}
					}

				}
			}
		}
		pomParent->SendMessage(WM_SAS_PROGESS_UPDATE, 1, 0 );
	}
	return true;
}
*/


bool SpotAllocation::AllocateGat(DIAFLIGHTDATA *prlFlightA, DIAFLIGHTDATA *prlFlightD, CString opGat, int ipGat /* = 1 */)
{
//	if (ipGat == 2)
//		return true;

	if(prlFlightD != NULL)
	{
		ogPosDiaFlightData.DeleteFromKeyMap(prlFlightD);
		if(ipGat == 1)
			strcpy(prlFlightD->Gtd1, opGat);
		else
			strcpy(prlFlightD->Gtd2, opGat);

		ogPosDiaFlightData.AddToKeyMap(prlFlightD);
		omChangedFlightsRkeyMap.SetAt((void *)prlFlightD->Rkey, NULL);
	}
	if(prlFlightA != NULL)
	{
		ogPosDiaFlightData.DeleteFromKeyMap(prlFlightA);
		if(ipGat == 1)
			strcpy(prlFlightA->Gta1, opGat);
		else
			strcpy(prlFlightA->Gta2, opGat);

		ogPosDiaFlightData.AddToKeyMap(prlFlightA);
		omChangedFlightsRkeyMap.SetAt((void *)prlFlightA->Rkey, NULL);
	}

	return true;
}


bool SpotAllocation::AllocateBlt(DIAFLIGHTDATA *prlFlightA, CString opBlt, int ipBelt)
{
	if(prlFlightA != NULL)
	{
		ogPosDiaFlightData.DeleteFromKeyMap(prlFlightA);
		if(ipBelt == 1)
			strcpy(prlFlightA->Blt1, opBlt);
		else
			strcpy(prlFlightA->Blt2, opBlt);

		ogPosDiaFlightData.AddToKeyMap(prlFlightA);
		omChangedFlightsRkeyMap.SetAt((void *)prlFlightA->Rkey, NULL);
	}
	return true;
}


bool SpotAllocation::AllocateWro(DIAFLIGHTDATA *prlFlightD, CString opWro, int ipWro)
{
	if(prlFlightD != NULL)
	{
		ogPosDiaFlightData.DeleteFromKeyMap(prlFlightD);
		if(ipWro == 1)
			strcpy(prlFlightD->Wro1, opWro);
		else
			strcpy(prlFlightD->Wro2, opWro);

		ogPosDiaFlightData.AddToKeyMap(prlFlightD);
		omChangedFlightsRkeyMap.SetAt((void *)prlFlightD->Rkey, NULL);
	}
	return true;
}






bool SpotAllocation::AutoWroAllocate()
{

	DiaCedaFlightData::RKEYLIST *prlRkey;
	DIAFLIGHTDATA *prlFlightD = NULL;
	CString olWro;
	CCSPtrArray<RecordSet> olWros;


	pomParent->SendMessage(WM_SAS_PROGESS_INIT, omFlights.GetSize(), 0 );

	int ilRotCount ;

	for(int k = omFlights.GetSize()-1; k >= 0 ; k--)
	{
		prlRkey = &omFlights[k];

		ilRotCount = prlRkey->Rotation.GetSize();

		prlFlightD = ogPosDiaFlightData.GetFlightDInRotation(prlRkey);
	
		if(prlFlightD != NULL)
		{
			if(!CString(prlFlightD->Wro1).IsEmpty())
				prlFlightD = NULL;
			if(!CString(prlFlightD->Wro2).IsEmpty())
				prlFlightD = NULL;
			if(CString(prlFlightD->Gtd1).IsEmpty())
				prlFlightD = NULL;
		}


		olWros.RemoveAll();

		ogBCD.GetRecords("WRO", "GTE1", CString(prlFlightD->Gtd1), &olWros);
		//ogBCD.GetRecords("WRO", "GTE2", CString(prlFlightD->Gtd1), &olWros);

		for(int i = 0; i < olWros.GetSize(); i++)
		{
			olWro = olWros[i].Values[ogBCD.GetFieldIndex("WRO", "WNAM")];

			if(CheckWro(prlFlightD, olWro, 1, NULL, true))
			{
				AllocateWro(prlFlightD, olWro, 1);
			}
		}

		pomParent->SendMessage(WM_SAS_PROGESS_UPDATE, 1, 0 );
	}
	return true;
}




bool SpotAllocation::CheckWro(DIAFLIGHTDATA *prpFlightD, CString opWro, int ipWroNo, CCSPtrArray<KonfItem> *popKonfList, bool bpPreCheck)
{

	if(opWro.IsEmpty() || prpFlightD == NULL || (ipWroNo != 1 && ipWroNo != 2))
		return true;

	bool blRet = true;

	CString olConfText;

	CTime olStartAlloc;
	CTime olEndAlloc;

	if (!ogPosDiaFlightData.GetWroAllocTimes(*prpFlightD, opWro, ipWroNo, olStartAlloc, olEndAlloc))
	{
		return false;
	}

	//olStartAlloc = prpFlightD->StartWro;
	//olEndAlloc = prpFlightD->EndWro;
	//ogPosDiaFlightData.GetBestWroTime(prpFlightD, true, olStartAlloc);
	//ogPosDiaFlightData.GetBestWroTime(prpFlightD, false, olEndAlloc);

	// Check Wro avaleblility
	CStringArray opNafr;
	CStringArray opNato;
	CStringArray opResn;
	CString opTabn = "WRO";
	MakeBlkData(olStartAlloc, olEndAlloc, opTabn, opWro, opNafr, opNato, opResn);

	for (int il = 0; il < opNafr.GetSize(); il++)
	{
		CTime olNafr = DBStringToDateTime(opNafr.GetAt(il));
		CTime olNato = DBStringToDateTime(opNato.GetAt(il));

		CString olVonStr = CTimeToDBString(olNafr, TIMENULL);
		CString olBisStr = CTimeToDBString(olNato, TIMENULL);

		if(olNafr != TIMENULL || olNato != TIMENULL)
		{	
			if(olNafr == TIMENULL)
				olNafr = CTime(1971,1,1,0,0,0);
			if(olNato == TIMENULL)
				olNato = CTime(2020,1,1,0,0,0);
		}


/*		if(olVafr == TIMENULL)
			olVafr = CTime(1971,1,1,0,0,0);
		if(olVato == TIMENULL)
			olVato = CTime(2020,1,1,0,0,0);
*/

		if((olNafr != TIMENULL) && (olNato != TIMENULL))
		{
			if(IsReallyOverlapped(olStartAlloc, olEndAlloc, olNafr, olNato)/*, olStartAlloc, olEndAlloc) || !IsWithIn(olStartAlloc, olEndAlloc, olNafr, olNato)*/)
			{
				if(popKonfList != NULL)
				{
					if(bgGatPosLocal) ogBasicData.UtcToLocal(olNafr);
					if(bgGatPosLocal) ogBasicData.UtcToLocal(olNato);
					//Gate %s not available %s - %s reason: %s
					olConfText.Format(GetString(IDS_STRING2204), olNafr.Format("%H:%M %d.%m.%y"), olNato.Format("%H:%M %d.%m.%y"),  opResn.GetAt(il));
					AddToKonfList((DIAFLIGHTDATA *)NULL , prpFlightD, 0, ' ', IDS_STRING2204, olConfText, *popKonfList);

					blRet = false;
				}
				else
				{
					return false;
				}
			}
		}
	}


/*
	CString olTmp;
	ogBCD.GetField("WRO", "WNAM", opWro, "NAFR", olTmp);
	CTime olNafr = DBStringToDateTime(olTmp);
	ogBCD.GetField("WRO", "WNAM", opWro, "NATO", olTmp);
	CTime olNato = DBStringToDateTime(olTmp);

	if((olNafr != TIMENULL) && (olNato != TIMENULL))
	{
		if(IsOverlapped(olNafr,olNato, olStartAlloc, olEndAlloc))
		{
			if(popKonfList != NULL)
			{
				//Lounge not available %s - %s reason: %s
				olConfText.Format(GetString(IDS_STRING1733), olNafr.Format("%H:%M %d.%m.%y"), olNato.Format("%H:%M %d.%m.%y"), ogBCD.GetField("WRO", "WNAM", opWro, "RESN"));
				AddToKonfList(NULL, prpFlightD, 0, ' ', IDS_STRING1733, olConfText, *popKonfList);

				blRet = false;
			}
			else
			{
				return false;
			}
		}
	}
*/

	//// Check max. number of wros ( overlapp )
	int ilCountOverlappingWro = ogPosDiaFlightData.GetMaxOverlapAtWro(olStartAlloc, olEndAlloc, opWro, prpFlightD);


	// get max count
	int	ilMaxCount = atoi( ogBCD.GetField("WRO", "WNAM", opWro, "MAXF"));

	if(ilMaxCount == 0)
		ilMaxCount = 1;


	if( ilMaxCount < ilCountOverlappingWro)
	{
		if(popKonfList != NULL)
		{
			//Too many flights at lounge %s: %d Max: %d
			olConfText.Format(GetString(IDS_STRING1734), opWro, ilCountOverlappingWro, ilMaxCount);
			AddToKonfList(NULL, prpFlightD, 0, ' ', IDS_STRING1734, olConfText, *popKonfList);
			blRet = false;
		}
		else
		{
			return false;
		}
	}




	// Check max. number of wro ( overlapp )
	/*
	int ilCountWro = ogPosDiaFlightData.GetWroCount(prpFlightD, opWro);

	if( 1 < ilCountWro)
	{
		if(popKonfList != NULL)
		{
			//To many flights at lounge %s: %d Max: %d
			olConfText.Format(GetString(IDS_STRING1734), opWro, ilCountWro, 1);
			AddToKonfList(NULL, prpFlightD, 0, ' ', IDS_STRING1734, olConfText, *popKonfList);
			blRet = false;
		}
		else
		{
			return false;
		}
	}
	*/



	return blRet;
}



bool SpotAllocation::ReCheckAllChangedFlights(bool bpCheckPst, bool bpCheckGat,bool bpCheckWro, bool bpCheckBlt)
{
 	DiaCedaFlightData::RKEYLIST *prlRkey;
	DIAFLIGHTDATA *prlPosDiaFlightA = NULL;
 	DIAFLIGHTDATA *prlPosDiaFlightD = NULL;

	DIAFLIGHTDATA rlFlightA;
	DIAFLIGHTDATA rlFlightD;

	bool blArrChanged = false;
	bool blDepChanged = false;

	POSITION pos;
	void *pDummy;
	long llRkey;
	long llRemovedConflictsCount = 0;
	for( pos = omChangedFlightsRkeyMap.GetStartPosition(); pos != NULL; )
	{
		omChangedFlightsRkeyMap.GetNextAssoc( pos, (void *&)llRkey , (void *&)pDummy );

		prlRkey = ogPosDiaFlightData.GetRotationByRkey(llRkey);

		prlPosDiaFlightA = ogPosDiaFlightData.GetFlightAInRotation(prlRkey);
		prlPosDiaFlightD = ogPosDiaFlightData.GetFlightDInRotation(prlRkey);

		if (prlPosDiaFlightA != NULL)
			rlFlightA = *prlPosDiaFlightA;
		if (prlPosDiaFlightD != NULL)
			rlFlightD = *prlPosDiaFlightD;

		blArrChanged = false;
		blDepChanged = false;

		if (bpCheckPst)
		{
			//check arrival position
			if (prlPosDiaFlightA != NULL)
			{
				if (!CheckPst(prlPosDiaFlightA, NULL, rlFlightA.Psta, NULL, &omParameters.omPstRightBuffer, &omParameters.omPstLeftBuffer))
				{
					rlFlightA.ClearAPos();
					blArrChanged = true;
					llRemovedConflictsCount++;
				}
			}
			//check departure position
			if (prlPosDiaFlightD != NULL)
			{
				if (!CheckPst(NULL, prlPosDiaFlightD, rlFlightD.Pstd, NULL, &omParameters.omPstRightBuffer, &omParameters.omPstLeftBuffer))
				{
					rlFlightD.ClearDPos();
					blDepChanged = true;
					llRemovedConflictsCount++;
				}
			}
		}
		if (bpCheckGat)
		{
			if (prlPosDiaFlightA != NULL)
			{
				// check first arrival gate
				if (!CheckGat(prlPosDiaFlightA, 'A', rlFlightA.Gta1, 1, NULL, &omParameters.omGatLeftBuffer, NULL))
				{
					rlFlightA.ClearAGat(1);
					blArrChanged = true;
					llRemovedConflictsCount++;
				}
				// check second arrival gate
				if (!CheckGat(prlPosDiaFlightA, 'A', rlFlightA.Gta2, 2, NULL, &omParameters.omGatLeftBuffer, NULL))
				{
					rlFlightA.ClearAGat(2);
					blArrChanged = true;
					llRemovedConflictsCount++;
				}
			}
			if (prlPosDiaFlightD != NULL)
			{
				// check first departure gate
				if (!CheckGat(prlPosDiaFlightD, 'D', rlFlightD.Gtd1, 1, NULL, NULL, &omParameters.omGatRightBuffer))
				{
					rlFlightD.ClearDGat(1);
					blDepChanged = true;
					llRemovedConflictsCount++;
				}
				// check second departure gate
				if (!CheckGat(prlPosDiaFlightD, 'D', rlFlightD.Gtd2, 2, NULL, NULL, &omParameters.omGatRightBuffer))
				{
					rlFlightD.ClearDGat(2);
					blDepChanged = true;
					llRemovedConflictsCount++;
				}
			}
		}
		if (bpCheckBlt)
		{
			if (prlPosDiaFlightA != NULL)
			{
				// check first baggage belt
				if (!CheckBlt(prlPosDiaFlightA, rlFlightA.Blt1, 1, NULL, 0))
				{
					rlFlightA.ClearBlt(1);
					blArrChanged = true;
					llRemovedConflictsCount++;
				}
				// check second baggage belt
				if (!CheckBlt(prlPosDiaFlightA, rlFlightA.Blt2, 2, NULL, 0))
				{
					rlFlightA.ClearBlt(2);
					blArrChanged = true;
					llRemovedConflictsCount++;
				}
			}
		}
		if (bpCheckWro)
		{
			if (prlPosDiaFlightD != NULL)
			{
				// check waiting room
				if (!CheckWro(prlPosDiaFlightD, rlFlightD.Wro1, 1, NULL))
				{
					rlFlightD.ClearWro(1);
					blDepChanged = true;
					llRemovedConflictsCount++;
				}
				if (!CheckWro(prlPosDiaFlightD, rlFlightD.Wro2, 2, NULL))
				{
					rlFlightD.ClearWro(2);
					blDepChanged = true;
					llRemovedConflictsCount++;
				}
			}
		}


		if (blArrChanged)
		{
			ogPosDiaFlightData.DeleteFromKeyMap(prlPosDiaFlightA);
			*prlPosDiaFlightA = rlFlightA;
			ogPosDiaFlightData.AddToKeyMap(prlPosDiaFlightA);
		}
 
		if (blDepChanged)
		{
			ogPosDiaFlightData.DeleteFromKeyMap(prlPosDiaFlightD);
			*prlPosDiaFlightD = rlFlightD;
			ogPosDiaFlightData.AddToKeyMap(prlPosDiaFlightD);
		}
	}

	TRACE("SpotAllocation::ReCheckAllChangedFlights: %ld Conflicts were removed!\n", llRemovedConflictsCount);

	return true;
}



bool SpotAllocation::AutoBltAllocate()
{
	DiaCedaFlightData::RKEYLIST *prlRkey;
	DIAFLIGHTDATA *prlFlightA = NULL;

	pomParent->SendMessage(WM_SAS_PROGESS_INIT, omFlights.GetSize(), 0 );

	for(int k = omFlights.GetSize()-1; k >= 0 ; k--)
	{
		prlRkey = &omFlights[k];

		prlFlightA = ogPosDiaFlightData.GetFlightAInRotation(prlRkey);
	
		// Allocate first belt
		AutoBltAllocateFlight(prlFlightA, 1);
		// Allocate second belt
		AutoBltAllocateFlight(prlFlightA, 2);		
 
		pomParent->SendMessage(WM_SAS_PROGESS_UPDATE, 1, 0 );
	}
	return true;
}



bool SpotAllocation::AutoBltAllocateFlight(DIAFLIGHTDATA *prlFlightA, int ipBltNo)
{
	if (prlFlightA == NULL)
		return false;
	
	if (ipBltNo == 1)
	{
		if (strlen(prlFlightA->Blt1) > 0)
			return true;
	}
	else if (ipBltNo == 2)
	{
		if (strlen(prlFlightA->Blt2) > 0)
			return true;
	}
	else
		return false;

  	
	// Only allocate a second baggage belt if the flight has a second arrival gate!
	if (ipBltNo == 2 && strlen(prlFlightA->Gta2) < 1)
		return false;


	CStringArray olGatBlts;
	if (!GetRelatedBlts(prlFlightA, ipBltNo, olGatBlts))
		return false;


	// At first try to allocate the flight to a free baggage belt
	for(int i = 0; i < olGatBlts.GetSize(); i++)
	{	
		if(CheckBlt(prlFlightA, olGatBlts[i], ipBltNo, NULL, 1))
		{
			AllocateBlt(prlFlightA, olGatBlts[i], ipBltNo);
			return true;
		}
	}
		
	// if no free one was found, try to allocate a occupied baggage belt which allow multiple allocations 		
	for(i = 0; i < olGatBlts.GetSize(); i++)
	{	
		if(CheckBlt(prlFlightA, olGatBlts[i], ipBltNo, NULL, 0))
		{
			AllocateBlt(prlFlightA, olGatBlts[i], ipBltNo);
			return true;
		}
	}

	

	return false;
}





void SpotAllocation::SetGatSort(CString opGat)
{
							
	GAT_RESLIST *prlGatRes = NULL;

	int ilCurrPrio = 0;

	for(int i = 0; i < omGatRules.GetSize(); i++)
	{
		if(omGatRules[i].Gnam == opGat)
		{
			prlGatRes = &omGatRules[i];
			ilCurrPrio = omGatRules[i].Prio;
			omGatRules.RemoveAt(i);
			break;

		}
	}

	bool blTreffer = false;
	for(int j = i; j < omGatRules.GetSize(); j++)
	{
		if(omGatRules[j].Prio != ilCurrPrio)
		{
			omGatRules.InsertAt(j, prlGatRes);
			blTreffer = true;
			break;
		}
	}

	if(!blTreffer)
	{
		omGatRules.InsertAt(omGatRules.GetSize(), prlGatRes);
	}


}




bool SpotAllocation::CheckBlt(DIAFLIGHTDATA *prpFlightA, CString opBlt, int ipBltNo, CCSPtrArray<KonfItem> *popKonfList, int ipMaxCount)
{

	if(opBlt.IsEmpty() || prpFlightA == NULL || (ipBltNo != 1 && ipBltNo != 2))
		return true;

	bool blRet = true;

	CString olConfText;

	CTime olStartAlloc;
	CTime olEndAlloc;


	
	if (!ogPosDiaFlightData.GetBltAllocTimes(*prpFlightA, opBlt, ipBltNo, olStartAlloc, olEndAlloc))
	{
		return false;
	}


	/*
	if (ipBltNo == 1)
	{
		olStartAlloc = prpFlightA->StartBlt1;
		olEndAlloc = prpFlightA->EndBlt1;
	}
	else
	{
		olStartAlloc = prpFlightA->StartBlt2;
		olEndAlloc = prpFlightA->EndBlt2;
	}
	*/
	
	//ogPosDiaFlightData.GetBestBltTime(prpFlightA, ipBltNo, true, olStartAlloc);
	//ogPosDiaFlightData.GetBestBltTime(prpFlightA, ipBltNo, false, olEndAlloc);

	// Check Blt availability
	CStringArray opNafr;
	CStringArray opNato;
	CStringArray opResn;
	CString opTabn = "BLT";
	MakeBlkData(olStartAlloc, olEndAlloc, opTabn, opBlt, opNafr, opNato, opResn);

	for (int il = 0; il < opNafr.GetSize(); il++)
	{
		CTime olNafr = DBStringToDateTime(opNafr.GetAt(il));
		CTime olNato = DBStringToDateTime(opNato.GetAt(il));

		CString olVonStr = CTimeToDBString(olNafr, TIMENULL);
		CString olBisStr = CTimeToDBString(olNato, TIMENULL);

		if(olNafr != TIMENULL || olNato != TIMENULL)
		{	
			if(olNafr == TIMENULL)
				olNafr = CTime(1971,1,1,0,0,0);
			if(olNato == TIMENULL)
				olNato = CTime(2020,1,1,0,0,0);
		}


/*		if(olVafr == TIMENULL)
			olVafr = CTime(1971,1,1,0,0,0);
		if(olVato == TIMENULL)
			olVato = CTime(2020,1,1,0,0,0);
*/

		if((olNafr != TIMENULL) && (olNato != TIMENULL))
		{
			if(IsReallyOverlapped(olStartAlloc, olEndAlloc, olNafr, olNato)/*, olStartAlloc, olEndAlloc) || !IsWithIn(olStartAlloc, olEndAlloc, olNafr, olNato)*/)
			{
				if(popKonfList != NULL)
				{
					if(bgGatPosLocal) ogBasicData.UtcToLocal(olNafr);
					if(bgGatPosLocal) ogBasicData.UtcToLocal(olNato);
					//Gate %s not available %s - %s reason: %s
					olConfText.Format(GetString(IDS_STRING1735), olNafr.Format("%H:%M %d.%m.%y"), olNato.Format("%H:%M %d.%m.%y"),  opResn.GetAt(il));
					AddToKonfList(prpFlightA, (DIAFLIGHTDATA *)NULL , 0, ' ', IDS_STRING1735, olConfText, *popKonfList);

					blRet = false;
				}
				else
				{
					return false;
				}
			}
		}
	}

/*
	CString olTmp;
	ogBCD.GetField("BLT", "BNAM", opBlt, "NAFR", olTmp);
	CTime olNafr = DBStringToDateTime(olTmp);
	ogBCD.GetField("BLT", "BNAM", opBlt, "NATO", olTmp);
	CTime olNato = DBStringToDateTime(olTmp);

	if((olNafr != TIMENULL) && (olNato != TIMENULL))
	{
		//TRACE("NotAv: %s - %s ; AllocT: %s - %s\n", 
		//	olNafr.Format("%d.%m.%Y %H:%M"), olNato.Format("%d.%m.%Y %H:%M"),
		//	olStartAlloc.Format("%d.%m.%Y %H:%M"), olEndAlloc.Format("%d.%m.%Y %H:%M"));

		if(IsOverlapped(olNafr,olNato, olStartAlloc, olEndAlloc))
		{
			if(popKonfList != NULL)
			{
				//Baggage belt not available %s - %s reason: %s
				olConfText.Format(GetString(IDS_STRING1735), olNafr.Format("%H:%M %d.%m.%y"), olNato.Format("%H:%M %d.%m.%y"), ogBCD.GetField("BLT", "BNAM", opBlt, "RESN"));
				AddToKonfList(prpFlightA, (DIAFLIGHTDATA *)NULL , 0, ' ', IDS_STRING1735, olConfText, *popKonfList);
				blRet = false;
			}
			else
			{
				return false;
			}
		}
	}
*/
	//// Check max. number of blt ( overlapp )
	int ilCountOverlappingBlt = ogPosDiaFlightData.GetMaxOverlapAtBlt(olStartAlloc, olEndAlloc, opBlt, prpFlightA);


	// get max count
	int ilMaxCount = 0;
	if(ipMaxCount != 0)
	{
		ilMaxCount = ipMaxCount;
	}
	else
	{
		ilMaxCount = atoi( ogBCD.GetField("BLT", "BNAM", opBlt, "MAXF"));

		if(ilMaxCount == 0)
			ilMaxCount = 1;
	}


	if( ilMaxCount < ilCountOverlappingBlt)
	{
		if(popKonfList != NULL)
		{
			//To many aircrafts at baggage belt %s: %d Max: %d
			olConfText.Format(GetString(IDS_STRING1736), opBlt, ilCountOverlappingBlt, ilMaxCount);
			AddToKonfList(prpFlightA, (DIAFLIGHTDATA *)NULL , 0, ' ', IDS_STRING1736, olConfText, *popKonfList);
			blRet = false;
		}
		else
		{
			return false;
		}
	}


	return blRet;
}









void SpotAllocation::ResetAllocation(bool bpPst, bool bpGat, bool bpBlt, bool bpWro)
{
	if (!bpPst && !bpGat && !bpBlt && !bpWro)
		return;
	
	DIAFLIGHTDATA *prlFlight = NULL;
	DIAFLIGHTDATA *prlFlightSave = new DIAFLIGHTDATA ;
	
	pomParent->SendMessage(WM_SAS_PROGESS_INIT, ogPosDiaFlightData.omData.GetSize(), 0 );

	
 	CString olData;
 	CString olSelection;
	CString olField = ogPosDiaFlightData.GetFieldList();
 
	CTime olCurr = CTime::GetCurrentTime();
	ogPosDiaFlightData.LocalToUtc(olCurr);

	for( int j = ogPosDiaFlightData.omData.GetSize() -1 ; j >= 0; j-- )
	{
		prlFlight = &ogPosDiaFlightData.omData[j];

		if (prlFlight && !CheckPostFlightPosDia(prlFlight->Urno, NULL))
		{

			*prlFlightSave = *prlFlight;

			if(prlFlight->Tifa >= olCurr)
			{
				ogPosDiaFlightData.DeleteFromKeyMap(prlFlight);
					
				if(bpPst)
				{
					prlFlight->ClearAPos();
					prlFlight->ClearDPos();
				}

				if(bpGat)
				{
					prlFlight->ClearAGat(1);
					prlFlight->ClearAGat(2);
					prlFlight->ClearDGat(1);
					prlFlight->ClearDGat(2);
				}

				if(bpBlt)
				{
					prlFlight->ClearBlt(1);
					prlFlight->ClearBlt(2);
				}

				if(bpWro)
				{
					prlFlight->ClearWro(1);
					prlFlight->ClearWro(2);
				}


 				//ogPosDiaFlightData.omCcaData.DelCcasByFlnu(prlFlight->Urno);
				
				ogPosDiaFlightData.AddToKeyMap(prlFlight);

 				
				/////////////////////////////////////////////////////

				ogPosDiaFlightData.UpdateFlight(prlFlight, prlFlightSave);
				/*
				olData="";
				ogPosDiaFlightData.MakeCedaData(olData, olField, prlFlightSave, prlFlight);
				olSelection.Format("WHERE URNO = %ld", prlFlight->Urno);
  				
				ogPosDiaFlightData.UpdateFlight(prlFlight->Urno, olSelection, olField, olData);
 				*/

			}
		}

		pomParent->SendMessage(WM_SAS_PROGESS_UPDATE, 1, 0 );

	}
 
 
	delete prlFlightSave;


}




void SpotAllocation::AddToKonfList(const DIAFLIGHTDATA *prpFlightA, const DIAFLIGHTDATA *prpFlightD, long lpRelFUrno, char cpRelFPart, int ipType, const CString &ropConfText, CCSPtrArray<KonfItem> &ropKonfList)
{
	AddToKonfList(prpFlightA, 'A', lpRelFUrno, cpRelFPart, ipType, ropConfText, ropKonfList);
	AddToKonfList(prpFlightD, 'D', lpRelFUrno, cpRelFPart, ipType, ropConfText, ropKonfList);
}




void SpotAllocation::AddToKonfList(const DIAFLIGHTDATA *prpFlight, char cpFPart, long lpRelFUrno, char cpRelFPart, int ipType, const CString &ropConfText, CCSPtrArray<KonfItem> &ropKonfList)
{
	if(prpFlight != NULL)
	{
		KonfItem *prlKonf = new KonfItem;

		prlKonf->Text = ropConfText;
		prlKonf->KonfId.RelFUrno = lpRelFUrno;
		prlKonf->KonfId.RelFPart = cpRelFPart;
		prlKonf->KonfId.Type = ipType;


		prlKonf->KonfId.FUrno = prpFlight->Urno;
		prlKonf->KonfId.FPart = cpFPart;
		ropKonfList.Add(prlKonf);
		}

}



bool SpotAllocation::GetRelatedBlts(const DIAFLIGHTDATA *prpFlight, int ipBltNo, CStringArray &ropBlts)
{
	CString olGate;

	if (ipBltNo == 1)
	{
		olGate = prpFlight->Gta1;	
	}
	else if (ipBltNo == 2)
	{
		olGate = prpFlight->Gta2;
	}
	else
		return false;


	ropBlts.RemoveAll();

	CString olBlt;
	if (!olGate.IsEmpty())
	{
		ogBCD.GetField("GAT", "GNAM", olGate, "RBAB", olBlt);
		if (!olBlt.IsEmpty())
		{
			ropBlts.Add(olBlt);
			return true;
		}
		else
			return false;
	}


	// get belts from position

	if (strlen(prpFlight->Psta) < 1)
		return false;

	CString olPos(prpFlight->Psta);
	for(int i = 0; i < omGatRules.GetSize(); i++)
	{	
		if (omGatRules[i].RelPos1 == olPos || omGatRules[i].RelPos2 == olPos)
		{
			ogBCD.GetField("GAT", CString("GNAM"), omGatRules[i].Gnam, CString("RBAB"), olBlt);
			if (!olBlt.IsEmpty())
				ropBlts.Add(olBlt);
		}
	}

	if (ropBlts.GetSize() > 0)
		return true;
	else
		return false;
}












