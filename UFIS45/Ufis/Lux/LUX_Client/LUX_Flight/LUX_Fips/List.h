#if !defined(AFX_LIST_H__38906C41_0C2E_11D3_B058_00001C019205__INCLUDED_)
#define AFX_LIST_H__38906C41_0C2E_11D3_B058_00001C019205__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// List.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// List dialog

class List : public CDialog
{
// Construction
public:
	List(CWnd* pParent = NULL, CStringArray *popItems = NULL);   // standard constructor


	CStringArray omItems;


// Dialog Data
	//{{AFX_DATA(List)
	enum { IDD = IDD_LISTE };
	CListBox	m_CL_List;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(List)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(List)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};




//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LIST_H__38906C41_0C2E_11D3_B058_00001C019205__INCLUDED_)
