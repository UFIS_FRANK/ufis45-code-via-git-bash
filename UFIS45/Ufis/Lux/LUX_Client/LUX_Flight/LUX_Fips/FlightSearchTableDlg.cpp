// FlightSearchTableDlg.cpp : implementation file
//

#include <stdafx.h>
#include <fpms.h>
#include <resrc1.h>
#include <FlightSearchTableDlg.h>
#include <SeasonDlg.h>
#include <RotationDlg.h>
#include <BltDiagram.h>
#include <CcaDiagram.h>
#include <GatDiagram.h>
#include <PosDiagram.h>
#include <WroDiagram.h>
#include <SeasonTableDlg.h>
#include <DiaCedaFlightData.h>
#include <FlightSearchPropertySheet.h>
#include <RotGroundDlg.h>
#include <RotationTables.h>
#include <utils.h>
#include <DailyScheduleTableDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
 
/////////////////////////////////////////////////////////////////////////////
// FlightSearchTableDlg dialog


FlightSearchTableDlg::FlightSearchTableDlg(CWnd* pParent /*=NULL*/)
	: CDialog(FlightSearchTableDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(FlightSearchTableDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	pomFlightData = new DiaCedaFlightData(false);
	imButtonSpace = 40;
	isCreated = false;
    CDialog::Create(FlightSearchTableDlg::IDD, NULL);
	isCreated = true;

}


FlightSearchTableDlg::~FlightSearchTableDlg()
{
	delete pomFlightData;
	delete pomTable;
}


void FlightSearchTableDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(FlightSearchTableDlg)
 		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(FlightSearchTableDlg, CDialog)
	//{{AFX_MSG_MAP(FlightSearchTableDlg)
	ON_WM_SIZE()
	ON_WM_CLOSE()
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
	ON_BN_CLICKED(IDC_SHOWOBJECT, OnShowObject)
    ON_MESSAGE(WM_TABLE_RETURN_PRESSED, OnTableReturnPressed)
	ON_MESSAGE(WM_TABLE_SELCHANGE, OnTableSelChanged)
 	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// FlightSearchTableDlg message handlers


 
void FlightSearchTableDlg::Activate(const CString &ropWhere)
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	GetDlgItem(IDC_SHOWOBJECT)->EnableWindow(false);
	imMode = FIPSMODULES_NONE;
	bmLocal = false;


	pomFlightData->SetPreSelection(TIMENULL, TIMENULL, CString("'O','Z','B','S',' ','X','N','T','G','D','R'"));
	pomFlightData->ReadAllFlights(ropWhere, true, "");

	UpdateTable();

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}




void FlightSearchTableDlg::Activate(const CString &ropViewerKey, const CString &ropView, FipsModules ipMode)
{
	
	if (ipMode != imMode || omViewer.GetViewName() == "<Default>")
//	if (true)
	{
		GetDlgItem(IDC_SHOWOBJECT)->EnableWindow(true);
		// set mode
		imMode = ipMode;
		// set local- or utc-time
		switch (imMode)
		{
		case BLTDIA:
			imModeStringId = IDS_STRING1219;
			bmLocal = bgGatPosLocal;
			break;
		case CCADIA:
			imModeStringId = IDS_STRING1821;
			bmLocal = bgGatPosLocal;
			GetDlgItem(IDC_SHOWOBJECT)->EnableWindow(false);
			break;
		case GATDIA:
			imModeStringId = IDS_STRING1218;
			bmLocal = bgGatPosLocal;
			break;
		case POSDIA:
			imModeStringId = IDS_STRING1220;
			bmLocal = bgGatPosLocal;
			break;
		case WRODIA:
			imModeStringId = IDS_STRING1222;
			bmLocal = bgGatPosLocal;
			break;
		case SEASONSCHEDULES:
			imModeStringId = IDS_STRING1932;
			bmLocal = bgSeasonLocal;
			break;			
		case DAILYSCHEDULES:
			imModeStringId = IDS_STRING1552;
			bmLocal = bgDailyLocal;
			//GetDlgItem(IDC_SHOWOBJECT)->EnableWindow(false);
			break;
		case DAILYROTATIONS:
			imModeStringId = IDS_STRING1935;
			bmLocal = bgDailyLocal;
//			GetDlgItem(IDC_SHOWOBJECT)->EnableWindow(false);
			break;
		case FLIGHTDIAGRAM:
			imModeStringId = IMFK_FLIGHTDIAGRAM;
			bmLocal = bgDailyLocal;
//			GetDlgItem(IDC_SHOWOBJECT)->EnableWindow(false);
			break;
		default:
			imModeStringId = IDS_STRING1933;
			bmLocal = bgGatPosLocal;
		}
		omViewer.SetLocal(bmLocal);		
		// set actual viewer
		omViewer.SetViewerKey(ropViewerKey);
		omViewer.SelectView(ropView);
	}
	// Show search dialog
	CString olSearchCaption;
	olSearchCaption.Format(GetString(IDS_STRING1928), GetString(imModeStringId)); 
	FlightSearchPropertySheet olDlg("FLIGHTSEARCH", this, &omViewer, 0, olSearchCaption, bmLocal);
	//FlightSearchPropertySheet olDlg(ropViewerKey, this, &omViewer, 0, olSearchCaption, bmLocal);
	if(olDlg.DoModal() != IDCANCEL)
	{
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		// Save view
		omViewer.SetViewerKey("FLIGHTSEARCH");
		//omViewer.SelectView("OnlyOne");
		//olDlg.SaveDataToViewer("Dummy");
	
		CStringArray olFilters;
		omViewer.GetFilterPage(olFilters);	// use filters in the last view
		omViewer.CreateView("OnlyOne", olFilters, false);
		omViewer.SelectView("OnlyOne");	// just make sure that CViewer() work correctly
		//omViewer.CreateView("FLIGHTSEARCH", olFilters);
		//omViewer.SelectView("FLIGHTSEARCH");	// just make sure that CViewer() work correctly
		UpdateWindow();
		olDlg.SaveDataToViewer("Dummy");
 
		pomFlightData->Register();

		// Load flights 	
		CString olWhere;
		CTime olFrom;
		CTime olTo;
		CString olFtyps;

		bool blRotation;
		omViewer.GetFlightWhereString(olWhere, blRotation);

		olWhere += CString(" AND FLNO <> ' '");

 		omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);
		pomFlightData->SetPreSelection(olFrom, olTo, olFtyps);
		pomFlightData->ReadAllFlights(olWhere, blRotation, "");

		UpdateTable();

		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}
}


bool FlightSearchTableDlg::UpdateTable() 
{
	if (pomFlightData->omData.GetSize() == 0)
	{
		// No flights found!
		MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING1930), MB_ICONEXCLAMATION | MB_OK);
		return false;
	}
	// Rebuild table
	omViewer.ChangeViewTo(pomFlightData);

	if (pomTable->GetLinesCount() == 1)
	{
		FLIGHTSEARCHTABLE_LINEDATA *prlTableLine;
		prlTableLine = (FLIGHTSEARCHTABLE_LINEDATA *)pomTable->GetTextLineData(0);

		ShowFlight(prlTableLine->AUrno, prlTableLine->DUrno);
  		ShowMask(&pomFlightData->omData[0]);
	}
	else
	{
		//// set caption
		CString olTimes;
		if (bmLocal)
			olTimes = GetString(IDS_STRING1921);
		else
			olTimes = GetString(IDS_STRING1920);
		CString olCaption;
		olCaption.Format(GetString(IDS_STRING1929), omViewer.GetFlightLegCount(), GetString(imModeStringId));
		olCaption += " ("+olTimes+")";
		SetWindowText(olCaption);
		
 		// Resize table
		// (Its necessary for displaying the scroll-bars at the first time)
		CRect olRect;
		GetClientRect(&olRect);

		pomTable->SetPosition(1, olRect.Width()+1, 1, olRect.Height()-imButtonSpace);

		// show window
		SetWindowPos(&wndTop,0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE);
	}

	return true;
}


BOOL FlightSearchTableDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetWindowPos( &wndTopMost, 40, 80,0,0, SWP_NOSIZE);

	// create table

	pomTable = new CCSTable;
	pomTable->SetSelectMode(LBS_MULTIPLESEL | LBS_EXTENDEDSEL);
	pomTable->SetHeaderSpacing(0);

	CRect olRect;
    GetClientRect(&olRect);
	
    olRect.InflateRect(1, 1);     // hiding the CTable window border
    
	pomTable->SetTableData(this, olRect.left, olRect.right, olRect.top, olRect.bottom-imButtonSpace);

	omViewer.Attach(pomTable);

	// Rebuild table
	omViewer.ChangeViewTo(NULL);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}



void FlightSearchTableDlg::OnSize(UINT nType, int cx, int cy) 
{
	if(isCreated != false)
	{
		CDialog::OnSize(nType, cx, cy);
	
		if (nType != SIZE_MINIMIZED)
		{
			// resize table
			pomTable->SetPosition(1, cx+1, 1, cy-imButtonSpace);
		}
	}
}



void FlightSearchTableDlg::OnClose() 
{
	// only hide window
	ShowWindow(SW_HIDE);
	pomFlightData->ClearAll();
	pomFlightData->UnRegister();


	omViewer.UnRegister();

	//CDialog::OnClose();
}



void FlightSearchTableDlg::OnShowObject()
{
	// (This check is necessary, because 'GetCurSel' returns 0 if nothing selected!)
	if (pomTable->GetCTableListBox()->GetSelCount() > 0)
	{
		// get actual table line
		const FLIGHTSEARCHTABLE_LINEDATA *prlTableLine = NULL;
		int ilLineNo = pomTable->GetCurSel();

		prlTableLine = (FLIGHTSEARCHTABLE_LINEDATA *)pomTable->GetTextLineData(ilLineNo);
		if (prlTableLine != NULL)
		{
			ShowFlight(prlTableLine->AUrno, prlTableLine->DUrno);
		}
	}

} 


// show the flight in the actual fips-module
void FlightSearchTableDlg::ShowFlight(long lpAUrno, long lpDUrno)
{
	if (!GetDlgItem(IDC_SHOWOBJECT)->IsWindowEnabled())
		return;

	bool blResult = false;
	bool blResultTmp = false;
	switch (imMode)
	{
		// TODO TVO: Also select bars!
		case BLTDIA:
			if (pogBltDiagram != NULL)
			{
				if (lpAUrno != -1)
					blResult = pogBltDiagram->ShowFlight(lpAUrno);
			}
		break;
		case GATDIA:
			if (pogGatDiagram != NULL)
			{
				if (lpAUrno != -1)
					blResult = pogGatDiagram->ShowFlight(lpAUrno, 'A');
				if (lpDUrno != -1)
					blResultTmp = pogGatDiagram->ShowFlight(lpDUrno, 'D');
				blResult = blResult || blResultTmp;
			}
		break;
		case POSDIA:
			if (pogPosDiagram != NULL)
			{
				if (lpAUrno != -1) 
					blResult = pogPosDiagram->ShowFlight(lpAUrno, true);
				else if (lpDUrno != -1)
					blResult = pogPosDiagram->ShowFlight(lpDUrno, false);
			}
		break;
		case WRODIA:
			if (pogWroDiagram != NULL)
			{
				if (lpDUrno != -1)
					blResult = pogWroDiagram->ShowFlight(lpDUrno);
			}
		break;
		/* TODO TVO
		case CCADIA:
			if (pogCcaDiagram != NULL)
			{
				if (lpAUrno != -1)
					blResult = pogCcaDiagram->ShowFlight(lpAUrno);
			}
		break;
		*/
		case SEASONSCHEDULES:
			if (pogSeasonTableDlg != NULL)
			{
				pogSeasonTableDlg->DeselectAll();
				if (lpAUrno != -1) {
					blResult = pogSeasonTableDlg->ShowFlight(lpAUrno);
					pogSeasonTableDlg->SelectFlight(lpAUrno);
				}
				if (lpDUrno != -1)
				{
					blResultTmp = pogSeasonTableDlg->ShowFlight(lpDUrno);
					pogSeasonTableDlg->SelectFlight(lpDUrno);
				}
				blResult = blResult || blResultTmp;
			}
		break;
		case DAILYSCHEDULES:
			if (pogRotationTables != NULL)
			{
				//pogRotationTables->DeselectAll();
				if (lpAUrno != -1) {
					blResult = pogRotationTables->ShowFlight(lpAUrno);
					pogRotationTables->SelectFlight(lpAUrno);
				}
				if (lpDUrno != -1)
				{
					blResultTmp = pogRotationTables->ShowFlight(lpDUrno);
					pogRotationTables->SelectFlight(lpDUrno);
				}
				blResult = blResult || blResultTmp;
			}
		break;
		case DAILYROTATIONS:
			if (pogDailyScheduleTableDlg != NULL)
			{
				//pogRotationTables->DeselectAll();
				if (lpAUrno != -1) {
					blResult = pogDailyScheduleTableDlg->ShowFlight(lpAUrno);
					pogDailyScheduleTableDlg->SelectFlight(lpAUrno);
				}
				if (lpDUrno != -1)
				{
					blResultTmp = pogDailyScheduleTableDlg->ShowFlight(lpDUrno);
					pogDailyScheduleTableDlg->SelectFlight(lpDUrno);
				}
				blResult = blResult || blResultTmp;
			}
		break;

	}

	if (!blResult)
		MessageBox(GetString(IDS_STRING1931), GetString(IDS_STRING1930), MB_ICONEXCLAMATION | MB_OK);
 
}



void FlightSearchTableDlg::ShowMask(const DIAFLIGHTDATA *prlFlight)
{

	CTime olTimeTmp;
	if(strcmp(prlFlight->Des3, pcgHome) == 0)
	{
		olTimeTmp = prlFlight->Tifa;
	}
	else
	{
		olTimeTmp = prlFlight->Tifd;
	}

	char clMask =' ';
	switch (imMode)
	{
 		case SEASONSCHEDULES:
 			clMask = 'S';
			break;			
		case DAILYSCHEDULES:
 		case DAILYROTATIONS:
			clMask = 'D';
			break;
	}

	CFPMSApp::ShowFlightRecordData(this, prlFlight->Ftyp[0], prlFlight->Urno, prlFlight->Rkey, 
		prlFlight->Adid[0], olTimeTmp, bmLocal, clMask);

}



LONG FlightSearchTableDlg::OnTableLButtonDblclk( UINT wParam, LPARAM lParam)
{
	// show rotation mask
	const FLIGHTSEARCHTABLE_LINEDATA *prlTableLine = NULL;
	UINT ilLineNo = pomTable->GetCurSel();
	// get actual table line
	prlTableLine = (FLIGHTSEARCHTABLE_LINEDATA *)pomTable->GetTextLineData(ilLineNo);
	if (prlTableLine != NULL)
	{
		// get flight data
		const DIAFLIGHTDATA *prlAFlight = NULL;
		const DIAFLIGHTDATA *prlDFlight = NULL;
		if (prlTableLine->AUrno != 0)
			prlAFlight = pomFlightData->GetFlightByUrno(prlTableLine->AUrno);
		if (prlTableLine->DUrno != 0)
			prlDFlight = pomFlightData->GetFlightByUrno(prlTableLine->DUrno);

		if(prlAFlight != NULL || prlDFlight != NULL )
		{
 			if (prlAFlight) 
				ShowMask(prlAFlight);
			else 
				ShowMask(prlDFlight);
		}
	}

 	return 0L;
}


LONG FlightSearchTableDlg::OnTableReturnPressed( UINT wParam, LPARAM lParam)
{
	OnTableLButtonDblclk(wParam, lParam); 
	return 0L;
}

LONG FlightSearchTableDlg::OnTableSelChanged( UINT wParam, LPARAM lParam)
{
	
    int ilLineNo = pomTable->pomListBox->GetCurSel();    
	
	FLIGHTSEARCHTABLE_LINEDATA *prlTableLine = NULL;
	prlTableLine = (FLIGHTSEARCHTABLE_LINEDATA *)pomTable->GetTextLineData(ilLineNo);
	if (prlTableLine->AUrno != 0)
		omViewer.CheckPostFlight(prlTableLine->AUrno,this);
	else
		omViewer.CheckPostFlight(prlTableLine->DUrno,this);
	
	return 0L;
	
}

