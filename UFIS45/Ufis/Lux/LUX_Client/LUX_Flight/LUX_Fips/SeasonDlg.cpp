// SeasonDlg.cpp : implementation file
// Modification History: 
// 161100		rkr		OnAgent ge�ndert (PRF Athen)
// 23-nov-00	rkr		HandlePostFlight() and CheckPostFlight() 
//						for handling postflights added;
//

#include <stdafx.h>
#include <FPMS.h>
#include <SeasonDlg.h>
#include <CCSDdx.h>
#include <CCSTime.h>
#include <CCSTable.h>
#include <SeasonDlgCedaFlightData.h>
#include <PrivList.h>
#include <BasicData.h>
#include <resrc1.h>
#include <Utils.h>
#include <SeasonAskDlg.h>
#include <RotationDlg.h>
#include <CcaCedaFlightData.h>
#include <RotationRegnDlg.h>


#include <CedaBasicData.h>
#include <AwBasicDataDlg.h>
#include <RotationHAIDlg.h>

#include <CedaFlightUtilsData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



int CSeasonDlg::CompareCcas(const CCADATA **e1, const CCADATA **e2)
{
	if (strlen((**e1).Ckic) == 0 && (**e1).Ckbs == TIMENULL)
		return 1;
	if (strlen((**e2).Ckic) == 0 && (**e2).Ckbs == TIMENULL)
		return -1;

	if (strlen((**e1).Ckic) == 0)
		return 1;
	if (strlen((**e2).Ckic) == 0)
		return -1;

	if (strcmp((**e1).Ckic, (**e2).Ckic) > 0)
		return 1;
	if (strcmp((**e1).Ckic, (**e2).Ckic) < 0)
		return -1;

	return 0;
}


// Local function prototype
static void FlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);


/////////////////////////////////////////////////////////////////////////////
// CSeasonDlg dialog

CCSPtrArray<CCADATA> omOldCcaValues;


CSeasonDlg::CSeasonDlg(CWnd* pParent)
	: omCcaData(CCA_SEADLG_NEW, CCA_SEADLG_CHANGE, CCA_SEADLG_DELETE), CDialog(CSeasonDlg::IDD, NULL)
{

	CCS_TRY

	//{{AFX_DATA_INIT(CSeasonDlg)
	m_ADes3 = _T("");
	m_AEtoa = _T("");
	m_AAlc3 = _T("");
	m_AFltn = _T("");
	m_AFlns = _T("");
	m_AFlti = _T("");
	m_ATtyp = _T("");
//	m_AHtyp = _T("");
	m_AStyp = _T("");
	m_AStev = _T("");
	m_AOrg3 = _T("");
	m_AStod = _T("");
	m_AStoa = _T("");
	m_AGta1 = _T("");
	m_AGta2 = _T("");
	m_DOrg3 = _T("");
	m_Pvom = _T("");
	m_Pbis = _T("");
	m_ATage = _T("");
	m_DDes3 = _T("");
	m_DTage = _T("");
	m_DEtdi = _T("");
	m_DStoa = _T("");
	m_DStod = _T("");
	m_DFltn = _T("");
	m_DFlns = _T("");
	m_DFlti = _T("");
	m_DStyp = _T("");
	m_DTtyp = _T("");
	m_DStev = _T("");
	m_DGta1 = _T("");
	m_DGta2 = _T("");
//	m_DHtyp = _T("");
	m_AEtai = _T("");
	m_Freq = _T("");
	m_Ming = _T("");
	m_Act3 = _T("");
	m_ABlt1 = _T("");
	m_ABlt2 = _T("");
	m_Act5 = _T("");
	m_ACxx = FALSE;
	m_ADays = _T("");
	m_AExt1 = _T("");
	m_AExt2 = _T("");
	m_ANoop = FALSE;
	m_APstd2 = _T("");
	m_ARem1 = _T("");
	m_AStatus = -1;
	m_DCicf = _T("");
	m_DCict = _T("");
	m_DCxx = FALSE;
	m_DDays = _T("");
	m_DGtd1 = _T("");
	m_DGtd2 = _T("");
	m_DNoop = FALSE;
	m_DRem1 = _T("");
	m_DStatus = -1;
	m_DTmcf = _T("");
	m_DTmct = _T("");
	m_Seas = _T("");
	m_Created = _T("");
	m_ACreated = _T("");
	m_AFluko = _T("");
	m_ALastChange = _T("");
	m_DAlc3 = _T("");
	m_DCreated = _T("");
	m_DFluko = _T("");
	m_DLastChange = _T("");
	m_AB1bs = _T("");
	m_AB1es = _T("");
	m_AB2bs = _T("");
	m_AB2es = _T("");
	m_AGa1b = _T("");
	m_AGa1e = _T("");
	m_AGa2b = _T("");
	m_AGa2e = _T("");
	m_APabs = _T("");
	m_APaes = _T("");
	m_APsta = _T("");
	m_ATet1 = _T("");
	m_ATet2 = _T("");
	m_ATga1 = _T("");
	m_ATga2 = _T("");
	m_ATmb1 = _T("");
	m_ATmb2 = _T("");
	m_DGd1b = _T("");
	m_DGd1e = _T("");
	m_DGd2b = _T("");
	m_DGd2e = _T("");
	m_DPdbs = _T("");
	m_DPdes = _T("");
	m_DTgd1 = _T("");
	m_DTgd2 = _T("");
	m_DTwr1 = _T("");
	m_DW1bs = _T("");
	m_DW1es = _T("");
	m_DWro1 = _T("");
	m_DPstd = _T("");
	m_Nose = _T("");
	m_ACht3 = _T("");
	m_DCht3 = _T("");
	m_ARem2 = _T("");
	m_DRem2 = _T("");
	m_AOrg4 = _T("");
	m_DDes4 = _T("");
	m_DBaz1 = _T("");
	m_DBaz4 = _T("");
	m_ACsgn = _T("");
	m_DCsgn = _T("");
	m_Regn = _T("");
	m_DWro2 = _T("");
	m_DW2bs = _T("");
	m_DW2es = _T("");
	m_DTwr2 = _T("");
	//}}AFX_DATA_INIT



	prmAFlight = new SEASONDLGFLIGHTDATA;
	prmDFlight = new SEASONDLGFLIGHTDATA;
	prmAFlightSave = new SEASONDLGFLIGHTDATA;
	prmDFlightSave = new SEASONDLGFLIGHTDATA;
	
    pomAJfnoTable = new CCSTable;
    pomDJfnoTable = new CCSTable;
	pomDCinsTable = new CCSTable;	


	bmAFltiUserSet = false;
	bmDFltiUserSet = false;

    CDialog::Create(CSeasonDlg::IDD, NULL);

	CCS_CATCH_ALL

}



void CSeasonDlg::NewData(CWnd* pParent, long lpRkey, long lpUrno, int ipModus, bool bpLocalTime, CString opAdid)
{

	CCS_TRY

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	


	ClearAll();

	omAdid = opAdid;
	bmLocalTime = bpLocalTime;

	imModus = ipModus; /* 	DLG_NEW, DLG_COPY, DLG_CHANGE, DLG_CHANGE_DIADATA */
	pomParent = pParent;
	lmCalledBy = lpUrno;
	lmRkey = lpRkey;

	ogDdx.Register(this, S_DLG_ROTATION_SPLIT, CString("FLIGHTDLG"), CString("Flight Update/new"), FlightTableCf);
	ogDdx.Register(this, S_DLG_ROTATION_CHANGE, CString("FLIGHTDLG"), CString("Flight Update/new"), FlightTableCf);
	ogDdx.Register(this, S_DLG_FLIGHT_CHANGE, CString("FLIGHTDLG"), CString("Flight Update/new"), FlightTableCf);
    ogDdx.Register(this, S_DLG_FLIGHT_DELETE, CString("FLIGHTDLG"), CString("Flight Update/new"), FlightTableCf);
	ogDdx.Register(this, CCA_SEADLG_NEW,	CString("CCADATA"), CString("Cca-changed"),	FlightTableCf);
	ogDdx.Register(this, CCA_SEADLG_DELETE,	CString("CCADATA"), CString("Cca-changed"),	FlightTableCf);
	ogDdx.Register(this, CCA_SEADLG_CHANGE,	CString("CCADATA"), CString("Cca-changed"),	FlightTableCf);

	ogSeasonDlgFlights.ReadRotation( lpRkey, lpUrno);

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	//SetWindowPos(&wndTop,0,0,0,0, SWP_NOSIZE | SWP_NOMOVE | SWP_SHOWWINDOW);

	SetWndPos();

	bmInit = true;

	//ShowWindow(SW_SHOW);

	CCS_CATCH_ALL

}



void CSeasonDlg::NewData(CWnd* pParent, const DIAFLIGHTDATA *prpDiaFlight, bool bpLocalTime, CString opAdid)
{

	CCS_TRY

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	
	ClearAll();

	omAdid = opAdid;
	bmLocalTime = bpLocalTime;

	imModus = DLG_CHANGE_DIADATA; /* 	DLG_NEW, DLG_COPY, DLG_CHANGE, DLG_CHANGE_DIADATA */
	pomParent = pParent;

 	lmCalledBy = prpDiaFlight->Urno;
	lmRkey = prpDiaFlight->Rkey;

 	ogDdx.Register(this, S_DLG_ROTATION_SPLIT, CString("FLIGHTDLG"), CString("Flight Update/new"), FlightTableCf);
 	ogDdx.Register(this, S_DLG_ROTATION_CHANGE, CString("FLIGHTDLG"), CString("Flight Update/new"), FlightTableCf);
	ogDdx.Register(this, S_DLG_FLIGHT_CHANGE, CString("FLIGHTDLG"), CString("Flight Update/new"), FlightTableCf);
    ogDdx.Register(this, S_DLG_FLIGHT_DELETE, CString("FLIGHTDLG"), CString("Flight Update/new"), FlightTableCf);
	ogDdx.Register(this, CCA_SEADLG_NEW,	CString("CCADATA"), CString("Cca-changed"),	FlightTableCf);
	ogDdx.Register(this, CCA_SEADLG_DELETE,	CString("CCADATA"), CString("Cca-changed"),	FlightTableCf);
	ogDdx.Register(this, CCA_SEADLG_CHANGE,	CString("CCADATA"), CString("Cca-changed"),	FlightTableCf);

	DiaCedaFlightData::RKEYLIST *prlRkey = ogPosDiaFlightData.GetRotationByRkey(prpDiaFlight->Rkey);
	DIAFLIGHTDATA *prlDiaFlightA = ogPosDiaFlightData.GetFlightAInRotation(prlRkey);
	DIAFLIGHTDATA *prlDiaFlightD = ogPosDiaFlightData.GetFlightDInRotation(prlRkey);

	ogSeasonDlgFlights.SetRotation(prlDiaFlightA, prlDiaFlightD);

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	//SetWindowPos(&wndTop,0,0,0,0, SWP_NOSIZE | SWP_NOMOVE | SWP_SHOWWINDOW);

	SetWndPos();

	bmInit = true;

	//ShowWindow(SW_SHOW);

	CCS_CATCH_ALL

}





CSeasonDlg::~CSeasonDlg()
{

	ogSeasonDlgFlights.ClearAll();
	
	ogDdx.UnRegister(this,NOTUSED);
	ClearAll();

	if(pomAJfnoTable != NULL)
		delete pomAJfnoTable;
	if(pomAViaCtrl != NULL)
		delete pomAViaCtrl;
	if(pomDJfnoTable != NULL)
		delete pomDJfnoTable;
	if(pomDViaCtrl != NULL)
		delete pomDViaCtrl;
	if(pomDCinsTable != NULL)
		delete pomDCinsTable;

	if(prmDFlight != NULL)
		delete prmDFlight;
	if(prmAFlight != NULL)
		delete prmAFlight;
	if(prmDFlightSave != NULL)
		delete prmDFlightSave;
	if(prmAFlightSave != NULL)
		delete prmAFlightSave;

	omDCins.DeleteAll();
	omOldDCins.DeleteAll();
	omDJfno.DeleteAll();
	omAJfno.DeleteAll();
	omDCinsSave.DeleteAll();
	omOldCcaValues.DeleteAll();

	if(pogSeasonAskDlg != NULL)
		pogSeasonAskDlg->SetWindowPos(&wndTop,0,0,0,0, SWP_NOSIZE | SWP_NOMOVE | SWP_SHOWWINDOW);
}




void CSeasonDlg::ClearAll()
{

	CCS_TRY

	bmInit = false;

	ogSeasonDlgFlights.ClearAll();
	omCcaData.ClearAll();
	
	ogDdx.UnRegister(this,NOTUSED);
	

	bmDlgEnabled = true;

	bmIsAFfnoShown = false;
	bmIsDFfnoShown = false;

	bmAbflug = false;
	bmAnkunft = false;
	bmIsCheckInNew = true;

	


	SEASONDLGFLIGHTDATA *prlFlight = new SEASONDLGFLIGHTDATA;	

	*prmAFlight = *prlFlight;
	*prmAFlightSave = *prlFlight;
	*prmDFlight = *prlFlight;
	*prmDFlightSave = *prlFlight;
	
	delete prlFlight;

	lmCalledBy = 0;
	lmRkey = 0;

	omAJfno.DeleteAll();
	omDJfno.DeleteAll();
	omDCins.DeleteAll();
	omOldDCins.DeleteAll();
	omDCinsSave.DeleteAll();

	CCS_CATCH_ALL
}


void CSeasonDlg::DoDataExchange(CDataExchange* pDX)
{

	CCS_TRY

	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSeasonDlg)
	DDX_Control(pDX, IDC_DCINSMAL4, m_CB_DCinsMal4);
	DDX_Control(pDX, IDC_DBAZ4, m_CE_DBaz4);
	DDX_Control(pDX, IDC_DBAZ1, m_CE_DBaz1);
	DDX_Control(pDX, IDC_DREMP, m_CB_DRemp);
	DDX_Control(pDX, IDC_AREMP, m_CB_ARemp);
	DDX_Control(pDX, IDC_DDES4, m_CE_DDes4);
	DDX_Control(pDX, IDC_AORG4, m_CE_AOrg4);
	DDX_Control(pDX, IDC_DREM2, m_CE_DRem2);
	DDX_Control(pDX, IDC_AREM2, m_CE_ARem2);
	DDX_Control(pDX, IDC_AFLTI, m_CE_AFlti);
	DDX_Control(pDX, IDC_DFLTI, m_CE_DFlti);
	DDX_Control(pDX, IDC_STATIC_ACHT3, m_CS_ACht3);
	DDX_Control(pDX, IDC_STATIC_DCHT3, m_CS_DCht3);
	DDX_Control(pDX, IDC_DCHT3, m_CE_DCht3);
	DDX_Control(pDX, IDC_ACHT3, m_CE_ACht3);
	DDX_Control(pDX, IDC_STATIC_NOSE, m_CS_Nose);
	DDX_Control(pDX, IDC_NOSE, m_CE_Nose);
	DDX_Control(pDX, IDOK, m_CB_Ok);
	DDX_Control(pDX, IDC_DDALY, m_CB_DDaly);
	DDX_Control(pDX, IDC_DALC3LIST2, m_CB_DAlc3List2);
	DDX_Control(pDX, IDC_DALC3LIST3, m_CB_DAlc3List3);
	DDX_Control(pDX, IDC_DEFAULT, m_CB_Default);
	DDX_Control(pDX, IDC_DSTODCALC, m_CB_DStodCalc);
	DDX_Control(pDX, IDC_ASTOACALC, m_CB_AStoaCalc);
	DDX_Control(pDX, IDC_ARESET, m_CB_AReset);
	DDX_Control(pDX, IDC_DRESET, m_CB_DReset);
	DDX_Control(pDX, IDC_DAILY_MASK, m_CB_DailyMask);
	DDX_Control(pDX, IDC_NEXTFLIGHT, m_CB_NextFlight);
	DDX_Control(pDX, IDC_PREVFLIGHT, m_CB_PrevFlight);
	DDX_Control(pDX, IDC_ASHOWJFNO, m_CB_AShowJfno);
	DDX_Control(pDX, IDC_ADALY, m_CB_ADaly);
	DDX_Control(pDX, IDC_AAlc3LIST, m_CB_AAlc3List);
	DDX_Control(pDX, IDC_AAlc3LIST3, m_CB_AAlc3List3);
	DDX_Control(pDX, IDC_AAlc3LIST2, m_CB_AAlc3List2);
	DDX_Control(pDX, IDC_DSTATUS, m_CB_DStatus);
	DDX_Control(pDX, IDC_DSHOWJFNO, m_CB_DShowJfno);
	DDX_Control(pDX, IDC_DNOOP, m_CB_DNoop);
	DDX_Control(pDX, IDC_DCXX, m_CB_DCxx);
	DDX_Control(pDX, IDC_ASTATUS, m_CB_AStatus);
	DDX_Control(pDX, IDC_ASTATUS2, m_CB_AStatus2);
	DDX_Control(pDX, IDC_ASTATUS3, m_CB_AStatus3);
	DDX_Control(pDX, IDC_DSTATUS2, m_CB_DStatus2);
	DDX_Control(pDX, IDC_DSTATUS3, m_CB_DStatus3);
	DDX_Control(pDX, IDC_ANOOP, m_CB_ANoop);
	DDX_Control(pDX, IDC_ACXX, m_CB_ACxx);
	DDX_Control(pDX, IDC_STATIC_ANKUNFT, m_CS_Ankunft);
	DDX_Control(pDX, IDC_STATIC_ABFLUG, m_CS_Abflug);
	DDX_Control(pDX, IDC_AORG3LIST, m_CB_AOrg3List);
	DDX_Control(pDX, IDC_DDES3LIST, m_CB_DDes3List);
	DDX_Control(pDX, IDC_DALC3LIST, m_CB_DAlc3List);
	DDX_Control(pDX, IDC_ACT35LIST, m_CB_Act35List);
	DDX_Control(pDX, IDC_DTTYPLIST, m_CB_DTtypList);
	DDX_Control(pDX, IDC_DSTYPLIST, m_CB_DStypList);
//	DDX_Control(pDX, IDC_DHTYPLIST, m_CB_DHtypList);
	DDX_Control(pDX, IDC_ATTYPLIST, m_CB_ATTypList);
	DDX_Control(pDX, IDC_ASTYPLIST, m_CB_AStypList);
//	DDX_Control(pDX, IDC_AHTYPLIST, m_CB_AHtypList);
	DDX_Control(pDX, IDC_DWRO1LIST, m_CB_DWro1List);
	DDX_Control(pDX, IDC_DPSTDLIST, m_CB_DPstdList);
	DDX_Control(pDX, IDC_DGTD2LIST, m_CB_DGtd2List);
	DDX_Control(pDX, IDC_DGTD1LIST, m_CB_DGtd1List);
	DDX_Control(pDX, IDC_DCINSLIST4, m_CB_DCinsList4);
	DDX_Control(pDX, IDC_DCINSLIST3, m_CB_DCinsList3);
	DDX_Control(pDX, IDC_DCINSLIST2, m_CB_DCinsList2);
	DDX_Control(pDX, IDC_DCINSLIST1, m_CB_DCinsList1);
	DDX_Control(pDX, IDC_ATMB2, m_CE_ATmb2);
	DDX_Control(pDX, IDC_APSTALIST, m_CB_APstaList);
	DDX_Control(pDX, IDC_AGTA2LIST, m_CB_AGta2List);
	DDX_Control(pDX, IDC_AGTA1LIST, m_CB_AGta1List);
	DDX_Control(pDX, IDC_AEXT2LIST, m_CB_AExt2List);
	DDX_Control(pDX, IDC_AEXT1LIST, m_CB_AExt1List);
	DDX_Control(pDX, IDC_ABLT2LIST, m_CB_ABlt2List);
	DDX_Control(pDX, IDC_ABLT1LIST, m_CB_ABlt1List);
	DDX_Control(pDX, IDCANCEL, m_CB_Cancel);
	DDX_Control(pDX, IDC_AGENTS, m_CB_Agents);
	DDX_Control(pDX, IDC_DJFNO_BORDER, m_CE_DJfnoBorder);
	DDX_Control(pDX, IDC_DLASTCHANGE, m_CE_DLastChange);
	DDX_Control(pDX, IDC_DFLUKO, m_CE_DFluko);
	DDX_Control(pDX, IDC_DCREATED, m_CE_DCreated);
	DDX_Control(pDX, IDC_DALC3, m_CE_DAlc3);
	DDX_Control(pDX, IDC_ALASTCHANGE, m_CE_ALastChange);
	DDX_Control(pDX, IDC_AJFNO_BORDER, m_CE_AJfnoBorder);
	DDX_Control(pDX, IDC_AFLUKO, m_CE_AFluko);
	DDX_Control(pDX, IDC_ACREATED, m_CE_ACreated);
	DDX_Control(pDX, IDC_SEAS, m_CE_Seas);
	DDX_Control(pDX, IDC_DREM1, m_CE_DRem1);
	DDX_Control(pDX, IDC_DGTD2, m_CE_DGtd2);
	DDX_Control(pDX, IDC_DGTD1, m_CE_DGtd1);
	DDX_Control(pDX, IDC_DDAYS, m_CE_DDays);
	DDX_Control(pDX, IDC_AEXT2, m_CE_AExt2);
	DDX_Control(pDX, IDC_AEXT1, m_CE_AExt1);
	DDX_Control(pDX, IDC_ADAYS, m_CE_ADays);
	DDX_Control(pDX, IDC_AREM1, m_CE_ARem1);
	DDX_Control(pDX, IDC_ACT5, m_CE_Act5);
	DDX_Control(pDX, IDC_ABLT2, m_CE_ABlt2);
	DDX_Control(pDX, IDC_ABLT1, m_CE_ABlt1);
	DDX_Control(pDX, IDC_ACT3, m_CE_Act3);
	DDX_Control(pDX, IDC_MING, m_CE_Ming);
//	DDX_Control(pDX, IDC_DHTYP, m_CE_DHtyp);
	DDX_Control(pDX, IDC_DSTEV, m_CE_DStev);
	DDX_Control(pDX, IDC_DTTYP, m_CE_DTtyp);
	DDX_Control(pDX, IDC_DSTYP, m_CE_DStyp);
	DDX_Control(pDX, IDC_DFLNS, m_CE_DFlns);
	DDX_Control(pDX, IDC_DFLTN, m_CE_DFltn);
	DDX_Control(pDX, IDC_DSTOD, m_CE_DStod);
	DDX_Control(pDX, IDC_DSTOA, m_CE_DStoa);
	DDX_Control(pDX, IDC_DETDI, m_CE_DEtdi);
	DDX_Control(pDX, IDC_AETAI, m_CE_AEtai);
	DDX_Control(pDX, IDC_DDES3, m_CE_DDes3);
	DDX_Control(pDX, IDC_PBIS, m_CE_Pbis);
	DDX_Control(pDX, IDC_PVOM, m_CE_Pvom);
	DDX_Control(pDX, IDC_FREQ, m_CE_Freq);
	DDX_Control(pDX, IDC_DORG3, m_CE_DOrg3);
	DDX_Control(pDX, IDC_AGTA2, m_CE_AGta2);
	DDX_Control(pDX, IDC_AGTA1, m_CE_AGta1);
	DDX_Control(pDX, IDC_ASTOA, m_CE_AStoa);
	DDX_Control(pDX, IDC_ASTOD, m_CE_AStod);
	DDX_Control(pDX, IDC_AORG3, m_CE_AOrg3);
	DDX_Control(pDX, IDC_ASTEV, m_CE_AStev);
	DDX_Control(pDX, IDC_ASTYP, m_CE_AStyp);
//	DDX_Control(pDX, IDC_AHTYP, m_CE_AHtyp);
	DDX_Control(pDX, IDC_ATTYP, m_CE_ATtyp);
	DDX_Control(pDX, IDC_AFLNS, m_CE_AFlns);
	DDX_Control(pDX, IDC_AFLTN, m_CE_AFltn);
	DDX_Control(pDX, IDC_AALC3, m_CE_AAlc3);
	DDX_Control(pDX, IDC_ADES3, m_CE_ADes3);
	DDX_Text(pDX, IDC_ADES3, m_ADes3);
	DDX_Text(pDX, IDC_AALC3, m_AAlc3);
	DDX_Text(pDX, IDC_AFLTN, m_AFltn);
	DDX_Text(pDX, IDC_AFLNS, m_AFlns);
	DDX_Text(pDX, IDC_AFLTI, m_AFlti);
	DDX_Text(pDX, IDC_ATTYP, m_ATtyp);
//	DDX_Text(pDX, IDC_AHTYP, m_AHtyp);
	DDX_Text(pDX, IDC_ASTYP, m_AStyp);
	DDX_Text(pDX, IDC_ASTEV, m_AStev);
	DDX_Text(pDX, IDC_AORG3, m_AOrg3);
	DDX_Text(pDX, IDC_ASTOD, m_AStod);
	DDX_Text(pDX, IDC_ASTOA, m_AStoa);
	DDX_Text(pDX, IDC_AGTA1, m_AGta1);
	DDX_Text(pDX, IDC_AGTA2, m_AGta2);
	DDX_Text(pDX, IDC_DORG3, m_DOrg3);
	DDX_Text(pDX, IDC_PVOM, m_Pvom);
	DDX_Text(pDX, IDC_PBIS, m_Pbis);
	DDX_Text(pDX, IDC_DDES3, m_DDes3);
	DDX_Text(pDX, IDC_DETDI, m_DEtdi);
	DDX_Text(pDX, IDC_DSTOA, m_DStoa);
	DDX_Text(pDX, IDC_DSTOD, m_DStod);
	DDX_Text(pDX, IDC_DFLTN, m_DFltn);
	DDX_Text(pDX, IDC_DFLNS, m_DFlns);
	DDX_Text(pDX, IDC_DFLTI, m_DFlti);
	DDX_Text(pDX, IDC_DSTYP, m_DStyp);
	DDX_Text(pDX, IDC_DTTYP, m_DTtyp);
	DDX_Text(pDX, IDC_DSTEV, m_DStev);
//	DDX_Text(pDX, IDC_DHTYP, m_DHtyp);
	DDX_Text(pDX, IDC_AETAI, m_AEtai);
	DDX_Text(pDX, IDC_FREQ, m_Freq);
	DDX_Text(pDX, IDC_MING, m_Ming);
	DDX_Text(pDX, IDC_ACT3, m_Act3);
	DDX_Text(pDX, IDC_ABLT1, m_ABlt1);
	DDX_Text(pDX, IDC_ABLT2, m_ABlt2);
	DDX_Text(pDX, IDC_ACT5, m_Act5);
	DDX_Check(pDX, IDC_ACXX, m_ACxx);
	DDX_Text(pDX, IDC_ADAYS, m_ADays);
	DDX_Text(pDX, IDC_AEXT1, m_AExt1);
	DDX_Text(pDX, IDC_AEXT2, m_AExt2);
	DDX_Check(pDX, IDC_ANOOP, m_ANoop);
	DDX_Text(pDX, IDC_AREM1, m_ARem1);
	DDX_Radio(pDX, IDC_ASTATUS, m_AStatus);
	DDX_Check(pDX, IDC_DCXX, m_DCxx);
	DDX_Text(pDX, IDC_DDAYS, m_DDays);
	DDX_Text(pDX, IDC_DGTD1, m_DGtd1);
	DDX_Text(pDX, IDC_DGTD2, m_DGtd2);
	DDX_Check(pDX, IDC_DNOOP, m_DNoop);
	DDX_Text(pDX, IDC_DREM1, m_DRem1);
	DDX_Radio(pDX, IDC_DSTATUS, m_DStatus);
	DDX_Text(pDX, IDC_SEAS, m_Seas);
	DDX_Text(pDX, IDC_ACREATED, m_ACreated);
	DDX_Text(pDX, IDC_AFLUKO, m_AFluko);
	DDX_Text(pDX, IDC_ALASTCHANGE, m_ALastChange);
	DDX_Text(pDX, IDC_DALC3, m_DAlc3);
	DDX_Text(pDX, IDC_DCREATED, m_DCreated);
	DDX_Text(pDX, IDC_DFLUKO, m_DFluko);
	DDX_Text(pDX, IDC_DLASTCHANGE, m_DLastChange);
	DDX_Text(pDX, IDC_AB1BS, m_AB1bs);
	DDX_Control(pDX, IDC_AB1BS, m_CE_AB1bs);
	DDX_Text(pDX, IDC_AB1ES, m_AB1es);
	DDX_Control(pDX, IDC_AB1ES, m_CE_AB1es);
	DDX_Text(pDX, IDC_AB2BS, m_AB2bs);
	DDX_Control(pDX, IDC_AB2BS, m_CE_AB2bs);
	DDX_Text(pDX, IDC_AB2ES, m_AB2es);
	DDX_Control(pDX, IDC_AB2ES, m_CE_AB2es);
	DDX_Text(pDX, IDC_AGA1B, m_AGa1b);
	DDX_Control(pDX, IDC_AGA1B, m_CE_AGa1b);
	DDX_Text(pDX, IDC_AGA1E, m_AGa1e);
	DDX_Control(pDX, IDC_AGA1E, m_CE_AGa1e);
	DDX_Text(pDX, IDC_AGA2B, m_AGa2b);
	DDX_Control(pDX, IDC_AGA2B, m_CE_AGa2b);
	DDX_Text(pDX, IDC_AGA2E, m_AGa2e);
	DDX_Control(pDX, IDC_AGA2E, m_CE_AGa2e);
	DDX_Text(pDX, IDC_APABS, m_APabs);
	DDX_Control(pDX, IDC_APABS, m_CE_APabs);
	DDX_Text(pDX, IDC_APAES, m_APaes);
	DDX_Control(pDX, IDC_APAES, m_CE_APaes);
	DDX_Text(pDX, IDC_APSTA, m_APsta);
	DDX_Control(pDX, IDC_APSTA, m_CE_APsta);
	DDX_Text(pDX, IDC_ATET1, m_ATet1);
	DDX_Control(pDX, IDC_ATET1, m_CE_ATet1);
	DDX_Text(pDX, IDC_ATET2, m_ATet2);
	DDX_Control(pDX, IDC_ATET2, m_CE_ATet2);
	DDX_Text(pDX, IDC_ATGA1, m_ATga1);
	DDX_Control(pDX, IDC_ATGA1, m_CE_ATga1);
	DDX_Text(pDX, IDC_ATGA2, m_ATga2);
	DDX_Control(pDX, IDC_ATGA2, m_CE_ATga2);
	DDX_Text(pDX, IDC_ATMB1, m_ATmb1);
	DDX_Control(pDX, IDC_ATMB1, m_CE_ATmb1);
	DDX_Text(pDX, IDC_ATMB2, m_ATmb2);
	DDX_Text(pDX, IDC_DGD1B, m_DGd1b);
	DDX_Control(pDX, IDC_DGD1B, m_CE_DGd1b);
	DDX_Text(pDX, IDC_DGD1E, m_DGd1e);
	DDX_Control(pDX, IDC_DGD1E, m_CE_DGd1e);
	DDX_Text(pDX, IDC_DGD2B, m_DGd2b);
	DDX_Control(pDX, IDC_DGD2B, m_CE_DGd2b);
	DDX_Text(pDX, IDC_DGD2E, m_DGd2e);
	DDX_Control(pDX, IDC_DGD2E, m_CE_DGd2e);
	DDX_Text(pDX, IDC_DPDBS, m_DPdbs);
	DDX_Control(pDX, IDC_DPDBS, m_CE_DPdbs);
	DDX_Text(pDX, IDC_DPDES, m_DPdes);
	DDX_Control(pDX, IDC_DPDES, m_CE_DPdes);
	DDX_Text(pDX, IDC_DTGD1, m_DTgd1);
	DDX_Control(pDX, IDC_DTGD1, m_CE_DTgd1);
	DDX_Text(pDX, IDC_DTGD2, m_DTgd2);
	DDX_Control(pDX, IDC_DTGD2, m_CE_DTgd2);
	DDX_Text(pDX, IDC_DTWR1, m_DTwr1);
	DDX_Control(pDX, IDC_DTWR1, m_CE_DTwr1);
	DDX_Text(pDX, IDC_DW1BS, m_DW1bs);
	DDX_Control(pDX, IDC_DW1BS, m_CE_DW1bs);
	DDX_Text(pDX, IDC_DW1ES, m_DW1es);
	DDX_Control(pDX, IDC_DW1ES, m_CE_DW1es);
	DDX_Text(pDX, IDC_DWRO1, m_DWro1);
	DDX_Control(pDX, IDC_DWRO1, m_CE_DWro1);
	DDX_Control(pDX, IDC_DCINSBORDER, m_CE_DCinsBorder);
	DDX_Text(pDX, IDC_DPSTD, m_DPstd);
	DDX_Control(pDX, IDC_DPSTD, m_CE_DPstd);
	DDX_Text(pDX, IDC_NOSE, m_Nose);
	DDX_Text(pDX, IDC_ACHT3, m_ACht3);
	DDX_Text(pDX, IDC_DCHT3, m_DCht3);
	DDX_Text(pDX, IDC_AREM2, m_ARem2);
	DDX_Text(pDX, IDC_DREM2, m_DRem2);
	DDX_Text(pDX, IDC_AORG4, m_AOrg4);
	DDX_Text(pDX, IDC_DDES4, m_DDes4);
	DDX_Text(pDX, IDC_DBAZ1, m_DBaz1);
	DDX_Text(pDX, IDC_DBAZ4, m_DBaz4);
	DDX_Control(pDX, IDC_ACSGN, m_CE_ACsgn);
	DDX_Text(pDX, IDC_ACSGN, m_ACsgn);
	DDX_Control(pDX, IDC_DCSGN, m_CE_DCsgn);
	DDX_Text(pDX, IDC_DCSGN, m_DCsgn);
	DDX_Control(pDX, IDC_REGN, m_CE_Regn);
	DDX_Text(pDX, IDC_REGN, m_Regn);
	DDX_Text(pDX, IDC_DWRO2, m_DWro2);
	DDX_Control(pDX, IDC_DWRO2, m_CE_DWro2);
	DDX_Text(pDX, IDC_DW2BS, m_DW2bs);
	DDX_Control(pDX, IDC_DW2BS, m_CE_DW2bs);
	DDX_Text(pDX, IDC_DW2ES, m_DW2es);
	DDX_Control(pDX, IDC_DW2ES, m_CE_DW2es);
	DDX_Control(pDX, IDC_DWRO2LIST, m_CB_DWro2List);
	DDX_Text(pDX, IDC_DTWR2, m_DTwr2);
	DDX_Control(pDX, IDC_DTWR2, m_CE_DTwr2);
	//}}AFX_DATA_MAP

	CCS_CATCH_ALL
}


BEGIN_MESSAGE_MAP(CSeasonDlg, CDialog)
	//{{AFX_MSG_MAP(CSeasonDlg)
	ON_BN_CLICKED(IDC_NEXTFLIGHT, OnNextflight)
	ON_BN_CLICKED(IDC_PREVFLIGHT, OnPrevflight)
	ON_BN_CLICKED(IDC_ASHOWJFNO, OnAshowjfno)
	ON_BN_CLICKED(IDC_DSHOWJFNO, OnDshowjfno)
	ON_MESSAGE(WM_EDIT_KILLFOCUS, OnEditKillfocus)
	ON_MESSAGE(WM_EDIT_CHANGED, OnEditChanged)
	ON_MESSAGE(WM_TABLE_IPEDIT_KILLFOCUS, OnTableIPEditKillfocus)
	ON_MESSAGE(WM_EDIT_RBUTTONDOWN, OnEditRButtonDown)
	ON_BN_CLICKED(IDC_DRESET, OnDreset)
	ON_BN_CLICKED(IDC_DAILY_MASK, OnDailyMask)
	ON_BN_CLICKED(IDC_DEFAULT, OnDefault)
	ON_BN_CLICKED(IDC_ARESET, OnAreset)
	ON_BN_CLICKED(IDC_ADALY, OnAdaly)
	ON_BN_CLICKED(IDC_DDALY, OnDdaly)
	ON_BN_CLICKED(IDC_ASTATUS, OnAstatus)
	ON_BN_CLICKED(IDC_ACXX, OnAcxx)
	ON_BN_CLICKED(IDC_ANOOP, OnAnoop)
	ON_BN_CLICKED(IDC_DCXX, OnDcxx)
	ON_BN_CLICKED(IDC_DNOOP, OnDnoop)
	ON_BN_CLICKED(IDC_DSTATUS, OnDstatus)
	ON_BN_CLICKED(IDC_AAlc3LIST, OnAAlc3LIST)
	ON_BN_CLICKED(IDC_AAlc3LIST2, OnAAlc3LIST2)
	ON_BN_CLICKED(IDC_AAlc3LIST3, OnAAlc3LIST3)
	ON_BN_CLICKED(IDC_ABLT1LIST, OnAblt1list)
	ON_BN_CLICKED(IDC_ABLT2LIST, OnAblt2list)
	ON_BN_CLICKED(IDC_ACT35LIST, OnAct35list)
	ON_BN_CLICKED(IDC_AEXT1LIST, OnAext1list)
	ON_BN_CLICKED(IDC_AEXT2LIST, OnAext2list)
	ON_BN_CLICKED(IDC_AGTA1LIST, OnAgta1list)
	ON_BN_CLICKED(IDC_AGTA2LIST, OnAgta2list)
//	ON_BN_CLICKED(IDC_AHTYPLIST, OnAhtyplist)
	ON_BN_CLICKED(IDC_AORG3LIST, OnAorg3list)
	ON_BN_CLICKED(IDC_APSTALIST, OnApstalist)
	ON_BN_CLICKED(IDC_ASTYPLIST, OnAstyplist)
	ON_BN_CLICKED(IDC_DALC3LIST, OnDalc3list)
	ON_BN_CLICKED(IDC_DALC3LIST2, OnDalc3list2)
	ON_BN_CLICKED(IDC_DALC3LIST3, OnDalc3list3)
	ON_BN_CLICKED(IDC_DCINSLIST1, OnDcinslist1)
	ON_BN_CLICKED(IDC_DCINSLIST2, OnDcinslist2)
	ON_BN_CLICKED(IDC_DCINSLIST3, OnDcinslist3)
	ON_BN_CLICKED(IDC_DCINSLIST4, OnDcinslist4)
	ON_BN_CLICKED(IDC_DDES3LIST, OnDdes3list)
	ON_BN_CLICKED(IDC_DGTD1LIST, OnDgtd1list)
	ON_BN_CLICKED(IDC_DGTD2LIST, OnDgtd2list)
//	ON_BN_CLICKED(IDC_DHTYPLIST, OnDhtyplist)
	ON_BN_CLICKED(IDC_DPSTDLIST, OnDpstdlist)
	ON_BN_CLICKED(IDC_DSTYPLIST, OnDstyplist)
	ON_BN_CLICKED(IDC_DTTYPLIST, OnDttyplist)
	ON_BN_CLICKED(IDC_DWRO1LIST, OnDwro1list)
	ON_BN_CLICKED(IDC_ATTYPLIST, OnAttyplist)
	ON_BN_CLICKED(IDC_DCINSMAL4, OnDcinsmal4)
	ON_BN_CLICKED(IDC_ASTATUS2, OnAstatus)
	ON_BN_CLICKED(IDC_ASTATUS3, OnAstatus)
	ON_BN_CLICKED(IDC_DSTATUS2, OnDstatus)
	ON_BN_CLICKED(IDC_DSTATUS3, OnDstatus)
	ON_BN_CLICKED(IDC_AGENTS, OnAgent)
	ON_CBN_SELCHANGE(IDC_AREMP, OnSelchangeAremp)
	ON_CBN_SELCHANGE(IDC_DREMP, OnSelchangeDremp)
	ON_MESSAGE(WM_TABLE_IPEDIT, OnTableIPEdit)
	ON_BN_CLICKED(IDC_DWRO2LIST, OnDwro2list)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

//	ON_BN_CLICKED(IDC_OK, OnOk)

/////////////////////////////////////////////////////////////////////////////
// CSeasonDlg message handlers

BOOL CSeasonDlg::OnInitDialog() 
{

	CCS_TRY

	CDialog::OnInitDialog();



	InitTables();	

	//Init editfields

	m_CE_Seas.SetReadOnly();
	m_CE_ADes3.SetReadOnly();
	m_CE_DOrg3.SetReadOnly();
	m_CE_AFluko.SetReadOnly();
	m_CE_ACreated.SetReadOnly();
	m_CE_ALastChange.SetReadOnly();
	m_CE_DFluko.SetReadOnly();
	m_CE_DCreated.SetReadOnly();
	m_CE_DLastChange.SetReadOnly();

	m_CE_AAlc3.SetBKColor(YELLOW);
	m_CE_AFltn.SetBKColor(YELLOW);
	m_CE_AStoa.SetBKColor(YELLOW);
	m_CE_AOrg3.SetBKColor(YELLOW);
	m_CE_AOrg4.SetBKColor(YELLOW);

	m_CE_DAlc3.SetBKColor(YELLOW);
	m_CE_DFltn.SetBKColor(YELLOW);
	m_CE_DStod.SetBKColor(YELLOW);
	m_CE_DDes3.SetBKColor(YELLOW);
	m_CE_DDes4.SetBKColor(YELLOW);


	m_CE_Pvom.SetTypeToDate(true);
	m_CE_Pbis.SetTypeToDate(true);
	m_CE_Freq.SetTypeToString("#",1,0);

	m_CE_Ming.SetTypeToString("###",3,0);

	CRect olRect;

	
	//Ankunft
	//
	m_CE_AAlc3.SetTypeToString("X|#X|#X|#",3,2);
	m_CE_AFltn.SetTypeToString("#####",5,2);
	m_CE_AFlns.SetTypeToString("X",1,0);
	m_CE_AFlti.SetTypeToString("A",1,0);
	m_CE_ADays.SetTypeToInt(1,9999999);
	

	pomAViaCtrl = new ViaTableCtrl(this,"APC3,FIDS,STOA,STOD");
	
	m_CE_AOrg3.GetWindowRect( olRect );

	ScreenToClient(olRect);

	pomAViaCtrl->SetSecStat(ogPrivList.GetStat("SEASONDLG_CB_AShowVia"));


	olRect.top = olRect.top;
	olRect.bottom = 0;
	olRect.left = olRect.right + 15;
	olRect.right = 0;

	pomAViaCtrl->SetPosition(olRect);


	m_CE_Act3.SetTypeToString("x|#x|#x|#",3,0);
	m_CE_Act5.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_Act3.SetBKColor(YELLOW);
	m_CE_Act5.SetBKColor(YELLOW);

//	m_CE_ACht3.ShowWindow(SW_HIDE);
//	m_CS_ACht3.ShowWindow(SW_HIDE);

//	m_CS_Nose.ShowWindow(SW_HIDE);		
//	m_CE_Nose.SetSecState('-');		
	m_CE_Nose.SetTypeToString("###",3,0);

	m_CE_ATtyp.SetBKColor(YELLOW);

	m_CE_ATtyp.SetTypeToString("XXXXX",5,1);
	
//	m_CE_AHtyp.SetTypeToString("XX",2,0);
	m_CE_AStyp.SetTypeToString("XX",2,0);
	m_CE_AStev.SetTypeToString("X",1,0);

	m_CE_AOrg3.SetTypeToString("XXX");
	m_CE_AOrg4.SetTypeToString("XXXX");
	m_CE_AStoa.SetTypeToTime(true, true);
	m_CE_AStod.SetTypeToTime(false, true);
	m_CE_AEtai.SetTypeToTime(false, true);


	m_CE_APsta.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_APabs.SetTypeToTime(false, true);
	m_CE_APaes.SetTypeToTime(false, true);
	
	
	m_CE_AGta1.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_AGta2.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_ATga1.SetTypeToString("x",1,0);
	m_CE_ATga2.SetTypeToString("x",1,0);
	m_CE_AGa1b.SetTypeToTime(false, true);
	m_CE_AGa2b.SetTypeToTime(false, true);
	m_CE_AGa1e.SetTypeToTime(false, true);
	m_CE_AGa2e.SetTypeToTime(false, true);

	
	m_CE_ABlt1.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_ABlt2.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_ATmb1.SetTypeToString("x",1,0);
	m_CE_ATmb2.SetTypeToString("x",1,0);
	m_CE_AB1bs.SetTypeToTime(false, true);
	m_CE_AB2bs.SetTypeToTime(false, true);
	m_CE_AB1es.SetTypeToTime(false, true);
	m_CE_AB2es.SetTypeToTime(false, true);

	// Disable the default menu which appear by rightclicking
	//    on the CCSEdit -box
	m_CE_AB1bs.UnsetDefaultMenu();
	m_CE_AB1es.UnsetDefaultMenu();
	m_CE_AB2bs.UnsetDefaultMenu();
	m_CE_AB2es.UnsetDefaultMenu();
	
	m_CE_AExt1.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_AExt2.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_ATet1.SetTypeToString("x",1,0);
	m_CE_ATet2.SetTypeToString("x",1,0);

	m_CE_ARem1.SetTextLimit(0,255,true);
	m_CE_ARem2.SetTextLimit(0,255,true);
/*		m_CE_ARem1.SetFont(&ogCourier_Regular_9);
		m_CE_ARem1.GetWindowRect(olRect);
		m_CE_ARem1.SetWindowPos(NULL,0,0,80,olRect.bottom - olRect.top, SWP_NOMOVE | SWP_NOZORDER | SWP_SHOWWINDOW);
//		m_CE_DRem1.SetFont(&ogCourier_Regular_9);
//		m_CE_DRem1.GetWindowRect(olRect);
//		m_CE_DRem1.SetWindowPos(NULL,0,0,80,olRect.bottom - olRect.top, SWP_NOMOVE | SWP_NOZORDER | SWP_SHOWWINDOW);


		m_CE_ARem2.SetFont(&ogCourier_Regular_9);
//		m_CE_DRem2.SetFont(&ogCourier_Regular_9);
		m_CE_ARem2.GetWindowRect(olRect);
		m_CE_ARem2.SetWindowPos(NULL,0,0,115,olRect.bottom - olRect.top, SWP_NOMOVE | SWP_NOZORDER | SWP_SHOWWINDOW);
//		m_CE_DRem2.GetWindowRect(olRect);
//		m_CE_DRem2.SetWindowPos(NULL,0,0,115,olRect.bottom - olRect.top, SWP_NOMOVE | SWP_NOZORDER | SWP_SHOWWINDOW);
*/
	m_CE_ACsgn.SetTypeToString("X|#X|#X|#X|#X|#X|#X|#X|#",8,0);
	
	//Abflug
	//

	
	pomDViaCtrl = new ViaTableCtrl(this,"APC3,FIDS,STOA,STOD");
	

	pomDViaCtrl->SetSecStat(ogPrivList.GetStat("SEASONDLG_CB_DShowVia"));


	m_CE_DOrg3.GetWindowRect( olRect );

	ScreenToClient(olRect);

	olRect.top = olRect.top;
	olRect.bottom = 0;
	olRect.left = olRect.right + 15;
	olRect.right = 0;

	pomDViaCtrl->SetPosition(olRect);
	
	
	m_CE_DAlc3.SetTypeToString("X|#X|#X|#",3,2);
	m_CE_DFltn.SetTypeToString("#####",5,1);
	m_CE_DFlns.SetTypeToString("X",1,0);
	m_CE_DFlti.SetTypeToString("A",1,0);
	m_CE_DDays.SetTypeToInt(1,9999999);
	

	//m_CE_DCht3.ShowWindow(SW_HIDE);
	//m_CS_DCht3.ShowWindow(SW_HIDE);

	m_CE_DTtyp.SetTypeToString("XXXXX",5,1);
	m_CE_DTtyp.SetBKColor(YELLOW);
	
//	m_CE_DHtyp.SetTypeToString("XX",2,0);
	m_CE_DStyp.SetTypeToString("XX",2,0);
	m_CE_DStev.SetTypeToString("X",1,0);

	m_CE_DDes3.SetTypeToString("XXX");
	m_CE_DDes4.SetTypeToString("XXXX");
	m_CE_DStod.SetTypeToTime(true, true);
	m_CE_DStoa.SetTypeToTime(false, true);
	m_CE_DEtdi.SetTypeToTime(false, true);

	m_CE_DPstd.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_DPdbs.SetTypeToTime(false, true);
	m_CE_DPdes.SetTypeToTime(false, true);

	m_CE_DGtd1.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_DGtd2.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_DTgd1.SetTypeToString("x",1,0);
	m_CE_DTgd2.SetTypeToString("x",1,0);
	m_CE_DGd1b.SetTypeToTime(false, true);
	m_CE_DGd2b.SetTypeToTime(false, true);
	m_CE_DGd1e.SetTypeToTime(false, true);
	m_CE_DGd2e.SetTypeToTime(false, true);
	
	m_CE_DWro1.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_DWro2.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_DTwr1.SetTypeToString("x",1,0);
	m_CE_DTwr2.SetTypeToString("x",1,0);
	m_CE_DW1bs.SetTypeToTime(false, true);
	m_CE_DW1es.SetTypeToTime(false, true);
	m_CE_DW2bs.SetTypeToTime(false, true);
	m_CE_DW2es.SetTypeToTime(false, true);

	m_CE_DRem1.SetTextLimit(0,255,true);
	m_CE_DRem2.SetTextLimit(0,255,true);
/*		m_CE_DRem1.SetFont(&ogCourier_Regular_9);
		m_CE_DRem1.GetWindowRect(olRect);
		m_CE_DRem1.SetWindowPos(NULL,0,0,80,olRect.bottom - olRect.top, SWP_NOMOVE | SWP_NOZORDER | SWP_SHOWWINDOW);

		m_CE_DRem2.SetFont(&ogCourier_Regular_9);
		m_CE_DRem2.GetWindowRect(olRect);
		m_CE_DRem2.SetWindowPos(NULL,0,0,115,olRect.bottom - olRect.top, SWP_NOMOVE | SWP_NOZORDER | SWP_SHOWWINDOW);
*/
	m_CE_DCsgn.SetTypeToString("X|#X|#X|#X|#X|#X|#X|#X|#",8,0);
	m_CE_Regn.SetTypeToString("X|#X|#X|#X|#X|#X|#X|#X|#X|#X|#X|#X|#",12,0);

	SetSecState();	

	omOpenTime = CTime::GetCurrentTime();


	CCS_CATCH_ALL

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}



void CSeasonDlg::ClearActualTimes()
{
	if (prmAFlight)
	{
		//arr
		prmAFlight->Urno = 0;
		prmAFlight->Rkey = 0;
		prmAFlight->Skey = 0;

		prmAFlight->Cdat = TIMENULL;
		prmAFlight->Lstu = TIMENULL;
		prmAFlight->Tifa = TIMENULL;
		prmAFlight->Tifd = TIMENULL;

		strcpy(prmAFlight->Usec, "");
		strcpy(prmAFlight->Useu, "");
		strcpy(prmAFlight->Dooa, "");
		strcpy(prmAFlight->Dood, "");
		strcpy(prmAFlight->Tisa, "");
		strcpy(prmAFlight->Tisd, "");
		strcpy(prmAFlight->Seas, "");

		prmAFlight->B1ba = TIMENULL;
		prmAFlight->B1ea = TIMENULL;
		prmAFlight->B2ba = TIMENULL;
		prmAFlight->B2ea = TIMENULL;

		prmAFlight->Ga1x = TIMENULL;
		prmAFlight->Ga1y = TIMENULL;
		prmAFlight->Ga2x = TIMENULL;
		prmAFlight->Ga2y = TIMENULL;

		prmAFlight->Gd1x = TIMENULL;
		prmAFlight->Gd1y = TIMENULL;
		prmAFlight->Gd2x = TIMENULL;
		prmAFlight->Gd2y = TIMENULL;

		prmAFlight->Paba = TIMENULL;
		prmAFlight->Paea = TIMENULL;
		prmAFlight->Pdba = TIMENULL;
		prmAFlight->Pdea = TIMENULL;

		prmAFlight->W1ba = TIMENULL;
		prmAFlight->W1ea = TIMENULL;
		prmAFlight->W2ba = TIMENULL;
		prmAFlight->W2ea = TIMENULL;
		prmAFlight->Ofbl = TIMENULL;
		prmAFlight->Onbl = TIMENULL;
	}

	if (prmDFlight)
	{
		//dep
		prmDFlight->Urno = 0;
		prmDFlight->Rkey = 0;
		prmDFlight->Skey = 0;

		prmDFlight->Cdat = TIMENULL;
		prmDFlight->Lstu = TIMENULL;
		prmDFlight->Tifa = TIMENULL;
		prmDFlight->Tifd = TIMENULL;

		strcpy(prmDFlight->Usec, "");
		strcpy(prmDFlight->Useu, "");
		strcpy(prmDFlight->Dooa, "");
		strcpy(prmDFlight->Dood, "");
		strcpy(prmDFlight->Tisa, "");
		strcpy(prmDFlight->Tisd, "");
		strcpy(prmDFlight->Seas, "");

		prmDFlight->B1ba = TIMENULL;
		prmDFlight->B1ea = TIMENULL;
		prmDFlight->B2ba = TIMENULL;
		prmDFlight->B2ea = TIMENULL;

		prmDFlight->Ga1x = TIMENULL;
		prmDFlight->Ga1y = TIMENULL;
		prmDFlight->Ga2x = TIMENULL;
		prmDFlight->Ga2y = TIMENULL;

		prmDFlight->Gd1x = TIMENULL;
		prmDFlight->Gd1y = TIMENULL;
		prmDFlight->Gd2x = TIMENULL;
		prmDFlight->Gd2y = TIMENULL;

		prmDFlight->Paba = TIMENULL;
		prmDFlight->Paea = TIMENULL;
		prmDFlight->Pdba = TIMENULL;
		prmDFlight->Pdea = TIMENULL;

		prmDFlight->W1ba = TIMENULL;
		prmDFlight->W1ea = TIMENULL;
		prmDFlight->W2ba = TIMENULL;
		prmDFlight->W2ea = TIMENULL;
		prmDFlight->Ofbl = TIMENULL;
		prmDFlight->Onbl = TIMENULL;
	}
}


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Fill the fields and show data in the dialog
//
void CSeasonDlg::InitDialog(bool bpArrival, bool bpDeparture) 
{

	CCS_TRY
	
	CCSEDIT_ATTRIB rlAttribC1;
	CCSEDIT_ATTRIB rlAttribC2;
	CCSEDIT_ATTRIB rlAttribC3;
	CCSEDIT_ATTRIB rlAttribC4;

	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;

	int ilLc ;

	if(prmAFlight != NULL)
	{
		bmAnkunft = true;
		bmAFltiUserSet = false;
		if ((int) strlen(prmAFlight->Dssf) > ogDssfFltiId && ogDssfFltiId != -1)
		{
			if (prmAFlight->Dssf[ogDssfFltiId] == 'U')
				bmAFltiUserSet = true;
		}
	}

	if(prmDFlight != NULL)
	{
		bmAbflug = true;
		bmDFltiUserSet = false;
		if ((int) strlen(prmDFlight->Dssf) > ogDssfFltiId && ogDssfFltiId != -1)
		{
			if (prmDFlight->Dssf[ogDssfFltiId] == 'U')
				bmDFltiUserSet = true;
		}
	}


	if(imModus == DLG_CHANGE || imModus == DLG_CHANGE_DIADATA)
	{
		if(bmLocalTime) ogSeasonDlgFlights.StructUtcToLocal((void*)prmDFlight);
		if(bmLocalTime) ogSeasonDlgFlights.StructUtcToLocal((void*)prmAFlight);
	}

	if(imModus == DLG_COPY)
	{
		if(bmLocalTime) ogSeasonDlgFlights.StructUtcToLocal((void*)prmDFlight);
		if(bmLocalTime) ogSeasonDlgFlights.StructUtcToLocal((void*)prmAFlight);

		
/*		prmAFlight->Urno = 0;
		prmAFlight->Rkey = 0;
		prmAFlight->Skey = 0;
		prmAFlight->Cdat = TIMENULL;
		prmAFlight->Lstu = TIMENULL;
		prmAFlight->Tifa = TIMENULL;
		prmAFlight->Tifd = TIMENULL;
		strcpy(prmAFlight->Usec, "");
		strcpy(prmAFlight->Useu, "");
		strcpy(prmAFlight->Dooa, "");
		strcpy(prmAFlight->Dood, "");
		strcpy(prmAFlight->Tisa, "");
		strcpy(prmAFlight->Tisd, "");
		strcpy(prmAFlight->Seas, "");
		*prmAFlightSave = *prmAFlight;

		prmDFlight->Cdat = TIMENULL;
		prmDFlight->Lstu = TIMENULL;
		prmDFlight->Tifa = TIMENULL;
		prmDFlight->Tifd = TIMENULL;
		strcpy(prmDFlight->Usec, "");
		strcpy(prmDFlight->Useu, "");
		strcpy(prmDFlight->Dooa, "");
		strcpy(prmDFlight->Dood, "");
		strcpy(prmDFlight->Tisa, "");
		strcpy(prmDFlight->Tisd, "");
		strcpy(prmDFlight->Seas, "");
		prmDFlight->Urno = 0;
		prmDFlight->Rkey = 0;
		prmDFlight->Skey = 0;
		*prmDFlightSave = *prmDFlight;
*/
		ClearActualTimes();
		*prmAFlightSave = *prmAFlight;
		*prmDFlightSave = *prmDFlight;
		strcpy(prmAFlight->Csgn, "");
		strcpy(prmDFlight->Csgn, "");

	}

	

// Global  ///////////////////////////////////////////////////////////////////

	if(bpArrival && bpDeparture)
	{
		if((imModus == DLG_NEW) || (imModus == DLG_COPY))
			m_Freq = "1";
		else
			m_Freq = "";

		if(strlen(prmAFlight->Act3) == 0)
		{
			m_Act3 = CString(prmDFlight->Act3);
			m_Act5 = CString(prmDFlight->Act5);
		}
		else
		{
			m_Act3 = CString(prmAFlight->Act3);
			m_Act5 = CString(prmAFlight->Act5);
		}

		if(strlen(prmAFlight->Ming) == 0)
			m_Ming = CString(prmDFlight->Ming);
		else
			m_Ming = CString(prmAFlight->Ming);

		m_Seas = CString(prmAFlight->Seas);

		if(m_Seas.IsEmpty())
			m_Seas = CString(prmDFlight->Seas);
		else
			m_Seas += CString(" / ") + CString(prmDFlight->Seas);

		if(strlen(prmAFlight->Regn) == 0)
			m_Regn = CString(prmDFlight->Regn);
		else
			m_Regn = CString(prmAFlight->Regn);


		m_CE_Freq.SetInitText(m_Freq);
		m_CE_Seas.SetInitText(m_Seas);
		m_CE_Ming.SetInitText(m_Ming);
		m_CE_Act3.SetInitText(m_Act3);
		m_CE_Act5.SetInitText(m_Act5);
		m_CE_Regn.SetInitText(m_Regn);

	}
	m_Nose = CString(prmAFlight->Nose);

	if(m_Nose.IsEmpty())
		m_Nose = CString(prmDFlight->Nose);
	m_CE_Nose.SetInitText(m_Nose);



// Ankunft ///////////////////////////////////////////////////////////////////


	CString olTmp; 
	if((prmAFlight->Urno == prmDFlight->Urno) && (prmAFlight->Urno != 0))
	{
		SEASONDLGFLIGHTDATA *prlFlight = new SEASONDLGFLIGHTDATA;	
		*prmDFlight = *prlFlight;
		*prmDFlightSave = *prlFlight;
		delete prlFlight;
		bpDeparture = true;
	}

	if (prmDFlight->Stod != TIMENULL && prmAFlight->Stoa != TIMENULL)
	{
		m_Pvom = prmAFlight->Stoa.Format("%d.%m.%Y");
		m_Pbis = prmDFlight->Stod.Format("%d.%m.%Y");
	}
	else
	{
		if (prmAFlight->Stoa != TIMENULL)
		{
			m_Pvom = prmAFlight->Stoa.Format("%d.%m.%Y");
			m_Pbis = prmAFlight->Stoa.Format("%d.%m.%Y");
		}
		else if (prmDFlight->Stod != TIMENULL)
		{
			m_Pvom = prmDFlight->Stod.Format("%d.%m.%Y");
			m_Pbis = prmDFlight->Stod.Format("%d.%m.%Y");
		}
		else
		{
			CTime olAct = CTime::GetCurrentTime();
			m_Pvom = olAct.Format("%d.%m.%Y");
			m_Pbis = olAct.Format("%d.%m.%Y");
		}
	}



	if(bpArrival)
	{

		omAAlc3 = CString(prmAFlight->Alc3);
		omAAlc2 = CString(prmAFlight->Alc2);

		m_AAlc3 = CString(prmAFlight->Flno);
		m_AAlc3 = m_AAlc3.Left(3);
		m_AAlc3.TrimRight();
		m_AFltn = CString(prmAFlight->Fltn);
		m_AFlns = CString(prmAFlight->Flns);
		m_AFlti = prmAFlight->Flti;

		m_CE_AFlti.SetBKColor(RGB(255,255,255));
		if (bmAFltiUserSet)
		{
 			m_CE_AFlti.SetBKColor(RGB(255,0,0));
		}

		if(IsCircular(prmAFlight->Org3, prmAFlight->Des3)) 
		{
			olTmp = GetString(IDS_STRING1388) + CString(" ");
			if(imModus == DLG_CHANGE || imModus == DLG_CHANGE_DIADATA)
			{
				olTmp += prmAFlight->Stoa.Format("%d.%m.%Y");
				m_CE_AStod.SetBKColor(YELLOW);
				m_CE_AStod.SetTypeToTime(true, true);
				m_CE_AStod.SetTextLimit(4,4,false);
			}
		}
		else
		{
			m_CE_AStod.SetBKColor(WHITE);
			m_CE_AStod.SetTypeToTime(false, true);
			m_CE_AStod.SetTextLimit(0,4,true);
			olTmp = GetString(IDS_STRING1389) + CString(" ");
			if(imModus == DLG_CHANGE || imModus == DLG_CHANGE_DIADATA)
				olTmp += prmAFlight->Stoa.Format("%d.%m.%Y");
		}
	
			
//		m_Pvom = prmAFlight->Stoa.Format("%d.%m.%Y");

		m_CS_Ankunft.SetWindowText(olTmp);

		m_ADays = "";
		m_CE_ADays.SetInitText(m_ADays);


		if(prmAFlight->Stoa == TIMENULL)
			omARefDat = CTime::GetCurrentTime();
		else
			omARefDat = prmAFlight->Stoa;

		// Weitere Flugnummern
		ogSeasonDlgFlights.GetJfnoArray(&omAJfno, prmAFlight);
		
		pomAJfnoTable->ResetContent();
		
		rlAttribC1.Type = KT_STRING;
		rlAttribC1.Format = "XXX";
		rlAttribC1.TextMinLenght = 0;
		rlAttribC1.TextMaxLenght = 3;
		rlAttribC1.Style = ES_UPPERCASE;

		rlAttribC2.Type = KT_STRING;
		rlAttribC2.Format = "#####";
		rlAttribC2.TextMinLenght = 0;
		rlAttribC2.TextMaxLenght = 5;

		rlAttribC3.Type = KT_STRING;
		rlAttribC3.Format = "X";
		rlAttribC3.TextMinLenght = 0;
		rlAttribC3.TextMaxLenght = 1;
		rlAttribC3.Style = ES_UPPERCASE;

		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.Font = &ogMS_Sans_Serif_8;
		rlColumnData.VerticalSeparator = SEPA_NORMAL;
		rlColumnData.HorizontalSeparator = SEPA_NONE;
		rlColumnData.Alignment = COLALIGN_LEFT;


		if(ogPrivList.GetStat("SEASONDLG_CB_AShowJfno") != '1')
			rlColumnData.BkColor = RGB(192,192,192);
		else
			rlColumnData.BkColor = RGB(255,255,255);


		for (ilLc = 0; ilLc < omAJfno.GetSize(); ilLc++)
		{
			rlColumnData.Text = omAJfno[ilLc].Alc3;
			rlColumnData.Text.TrimRight();
			rlColumnData.EditAttrib = rlAttribC1;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = omAJfno[ilLc].Fltn;
			rlColumnData.Text.TrimRight();
			rlColumnData.EditAttrib = rlAttribC2;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = omAJfno[ilLc].Flns;
			rlColumnData.Text.TrimRight();
			rlColumnData.EditAttrib = rlAttribC3;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			pomAJfnoTable->AddTextLine(olColList, (void*)(NULL));
			olColList.DeleteAll();
		}
		for (ilLc = omAJfno.GetSize(); ilLc < 9; ilLc++)
		{
			rlColumnData.Text = "";
			rlColumnData.EditAttrib = rlAttribC1;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = "";
			rlColumnData.EditAttrib = rlAttribC2;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = "";
			rlColumnData.EditAttrib = rlAttribC3;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			pomAJfnoTable->AddTextLine(olColList, (void*)(NULL));
			olColList.DeleteAll();
		}

		pomAJfnoTable->DisplayTable();
		
		
		// Vias

		if( CString(prmAFlight->Ftyp)	== "Z" || CString(prmAFlight->Ftyp)	== "B")
		{
			pomAViaCtrl->bmEdit = false;
		}
		else
		{
			pomAViaCtrl->bmEdit = true;
		}

		pomAViaCtrl->SetViaList(prmAFlight->Vial, omARefDat);
		


		m_ACxx = FALSE;
		m_CB_ACxx.SetCheck(FALSE);
		m_ANoop = FALSE;
		m_CB_ANoop.SetCheck(FALSE);
		m_AStatus = -1;
		m_CB_AStatus.SetCheck(FALSE);
		m_CB_AStatus2.SetCheck(FALSE);
		m_CB_AStatus3.SetCheck(FALSE);
		
		if(strcmp(prmAFlight->Ftyp, "X") == 0)
		{
			m_ACxx = TRUE;
			m_CB_ACxx.SetCheck(TRUE);
		}
		if(strcmp(prmAFlight->Ftyp, "N") == 0)
		{
			m_ANoop = TRUE;
			m_CB_ANoop.SetCheck(TRUE);
		}
		if(strcmp(prmAFlight->Ftyp, "S") == 0)
		{
			m_AStatus = 0;
			m_CB_AStatus.SetCheck(TRUE);
		}
		if(strcmp(prmAFlight->Ftyp, "O") == 0)
		{
			m_AStatus = 1;
			m_CB_AStatus2.SetCheck(TRUE);
		}
		if((strcmp(prmAFlight->Ftyp, "") == 0) && (strlen(prmAFlight->Vers) > 0))
		{
			m_AStatus = 2;
			m_CB_AStatus3.SetCheck(TRUE);
		}
		if((strcmp(prmAFlight->Ftyp, "") == 0) && (strlen(prmAFlight->Vers) == 0))
		{
			m_AStatus = 0;
			m_CB_AStatus.SetCheck(TRUE);
		}


		m_ACht3 = CString(prmAFlight->Cht3);
		m_ADes3 = CString(pcgHome);
		m_AOrg3 = CString(prmAFlight->Org3);
		m_ATtyp = CString(prmAFlight->Ttyp);
//		m_AHtyp = CString(prmAFlight->Htyp);
		m_AStyp = CString(prmAFlight->Styp);
		m_AStev = CString(prmAFlight->Stev);
		m_AOrg3 = CString(prmAFlight->Org3);
		m_AOrg4 = CString(prmAFlight->Org4);
		m_APsta = CString(prmAFlight->Psta);
		m_AGta1 = CString(prmAFlight->Gta1);
		m_AGta2 = CString(prmAFlight->Gta2);
		m_ABlt1 = CString(prmAFlight->Blt1);
		m_ABlt2 = CString(prmAFlight->Blt2);
		m_AExt1 = CString(prmAFlight->Ext1);
		m_AExt2 = CString(prmAFlight->Ext2);
		m_ARem1 = CString(prmAFlight->Rem1);
		m_ARem2 = CString(prmAFlight->Rem2);


		char pclDays[10];
		GetDayOfWeek(prmAFlight->Stoa, pclDays);
		m_ADays = CString(pclDays);
			
		m_AEtai = DateToHourDivString(prmAFlight->Etai, omARefDat);
		m_AStod = DateToHourDivString(prmAFlight->Stod, omARefDat);
		m_AStoa = DateToHourDivString(prmAFlight->Stoa, omARefDat);
		m_AFluko      = prmAFlight->Fdat.Format("%d.%m.%Y  %H:%M");
		m_ACreated    = prmAFlight->Cdat.Format("%d.%m.%Y  %H:%M") + CString("  ") + CString(prmAFlight->Usec);
		m_ALastChange = prmAFlight->Lstu.Format("%d.%m.%Y  %H:%M") + CString("  ") + CString(prmAFlight->Useu);

		m_APabs = DateToHourDivString(prmAFlight->Pabs, omARefDat);
		m_APaes = DateToHourDivString(prmAFlight->Paes, omARefDat);
		m_AGa1b = DateToHourDivString(prmAFlight->Ga1b, omARefDat);
		m_AGa2b = DateToHourDivString(prmAFlight->Ga2b, omARefDat);
		m_AGa1e = DateToHourDivString(prmAFlight->Ga1e, omARefDat);
		m_AGa2e = DateToHourDivString(prmAFlight->Ga2e, omARefDat);
		m_AB1bs = DateToHourDivString(prmAFlight->B1bs, omARefDat);
		m_AB2bs = DateToHourDivString(prmAFlight->B2bs, omARefDat);
		m_AB1es = DateToHourDivString(prmAFlight->B1es, omARefDat);
		m_AB2es = DateToHourDivString(prmAFlight->B2es, omARefDat);
		
		m_APsta = CString(prmAFlight->Psta);
		m_AGta1 = CString(prmAFlight->Gta1);
		m_AGta2 = CString(prmAFlight->Gta2);
		m_ATga1 = CString(prmAFlight->Tga1);
		m_ATga2 = CString(prmAFlight->Tga2);
		m_ABlt1 = CString(prmAFlight->Blt1);
		m_ABlt2 = CString(prmAFlight->Blt2);
		m_ATmb1 = CString(prmAFlight->Tmb1);
		m_ATmb2 = CString(prmAFlight->Tmb2);
		m_AExt1 = CString(prmAFlight->Ext1);
		m_AExt2 = CString(prmAFlight->Ext2);
		m_ATet1 = CString(prmAFlight->Tet1);
		m_ATet2 = CString(prmAFlight->Tet2);

		m_ACsgn = CString(prmAFlight->Csgn);
		if(strlen(prmAFlight->Regn) != 0)
			m_Regn = CString(prmAFlight->Regn);


		if(imModus != DLG_CHANGE)
		{
			m_CE_Pvom.SetInitText(m_Pvom);
			m_CE_Regn.SetInitText("");
		}
		else
		{
			m_CE_Pvom.SetInitText("");
			m_CE_Regn.SetInitText(m_Regn);
		}


		m_CE_ADays.SetInitText(m_ADays);
		m_CE_ACht3.SetInitText(m_ACht3);
		m_CE_ADes3.SetInitText(pcgHome);
		m_CE_AOrg3.SetInitText(m_AOrg3);
		m_CE_AOrg4.SetInitText(m_AOrg4);
		m_CE_AAlc3.SetInitText(m_AAlc3);
		m_CE_AFltn.SetInitText(m_AFltn);
		m_CE_AFlns.SetInitText(m_AFlns);
		m_CE_AFlti.SetInitText(m_AFlti);
		m_CE_ATtyp.SetInitText(m_ATtyp);
//		m_CE_AHtyp.SetInitText(m_AHtyp);
		m_CE_AStyp.SetInitText(m_AStyp);
		m_CE_AStev.SetInitText(m_AStev);
		m_CE_AOrg3.SetInitText(m_AOrg3);
		m_CE_ARem1.SetInitText(m_ARem1);
		m_CE_ARem2.SetInitText(m_ARem2);
		m_CE_AEtai.SetInitText(m_AEtai);
		m_CE_AStod.SetInitText(m_AStod);
		m_CE_AStoa.SetInitText(m_AStoa);


		m_CE_APsta.SetInitText(m_APsta);
		m_CE_APabs.SetInitText(m_APabs);
		m_CE_APaes.SetInitText(m_APaes);

		//is an open leg
		if (prmDFlight->Urno == 0 && prmAFlight->Onbl != TIMENULL)
			m_CE_APaes.SetReadOnly(TRUE);
		else
			m_CE_APaes.SetReadOnly(FALSE);

		m_CE_AGta1.SetInitText(m_AGta1);
		m_CE_AGta2.SetInitText(m_AGta2);
		m_CE_ATga1.SetInitText(m_ATga1);
		m_CE_ATga2.SetInitText(m_ATga2);
		m_CE_AGa1b.SetInitText(m_AGa1b);
		m_CE_AGa2b.SetInitText(m_AGa2b);
		m_CE_AGa1e.SetInitText(m_AGa1e);
		m_CE_AGa2e.SetInitText(m_AGa2e);
		m_CE_ABlt1.SetInitText(m_ABlt1);
		m_CE_ABlt2.SetInitText(m_ABlt2);
		m_CE_ATmb1.SetInitText(m_ATmb1);
		m_CE_ATmb2.SetInitText(m_ATmb2);
		m_CE_AB1bs.SetInitText(m_AB1bs);
		m_CE_AB2bs.SetInitText(m_AB2bs);
		m_CE_AB1es.SetInitText(m_AB1es);
		m_CE_AB2es.SetInitText(m_AB2es);
		m_CE_AExt1.SetInitText(m_AExt1);
		m_CE_AExt2.SetInitText(m_AExt2);
		m_CE_ATet1.SetInitText(m_ATet1);
		m_CE_ATet2.SetInitText(m_ATet2);


		m_CE_AFluko.SetInitText(m_AFluko);
		m_CE_ACreated.SetInitText(m_ACreated);
		m_CE_ALastChange.SetInitText(m_ALastChange);

		m_CE_ACsgn.SetInitText(m_ACsgn);
//		m_CE_Regn.SetInitText(m_Regn);
	}




// Abflug ///////////////////////////////////////////////////////////////////
	if(bpDeparture)
	{
		if (strlen(prmDFlight->Wro1) > 0 && prmDFlight->W1bs == TIMENULL && prmDFlight->W1es == TIMENULL && prmDFlight->Tifd != TIMENULL)
//		if (strlen(prmDFlight->Wro1) > 0 && prmDFlight->W1bs == TIMENULL && prmDFlight->W1es == TIMENULL)
		{
			prmDFlight->W1es = prmDFlight->Tifd;
			
			CTimeSpan olDura;
			GetWroDefAllocDur(prmDFlight->Wro1, olDura);
			prmDFlight->W1bs = prmDFlight->Tifd-olDura;
		}
		if (strlen(prmDFlight->Wro2) > 0 && prmDFlight->W2bs == TIMENULL && prmDFlight->W2es == TIMENULL && prmDFlight->Tifd != TIMENULL)
//		if (strlen(prmDFlight->Wro2) > 0 && prmDFlight->W2bs == TIMENULL && prmDFlight->W2es == TIMENULL)
		{
			prmDFlight->W2es = prmDFlight->Tifd;
			
			CTimeSpan olDura;
			GetWroDefAllocDur(prmDFlight->Wro2, olDura);
			prmDFlight->W2bs = prmDFlight->Tifd-olDura;
		}


		omDAlc3 = CString(prmDFlight->Alc3);
		omDAlc2 = CString(prmDFlight->Alc2);

		m_DAlc3 = CString(prmDFlight->Flno);
		m_DAlc3 = m_DAlc3.Left(3);
		m_DAlc3.TrimRight();
		m_DFltn = CString(prmDFlight->Fltn);
		m_DFlns = CString(prmDFlight->Flns);
		m_DFlti = prmDFlight->Flti;

		m_CE_DFlti.SetBKColor(RGB(255,255,255));
		if (bmDFltiUserSet)
		{
	 		m_CE_DFlti.SetBKColor(RGB(255,0,0));
		}

		if(IsArrival(prmDFlight->Org3, prmDFlight->Des3)) 
		{
			olTmp = GetString(IDS_STRING1388) + CString(" ");
			if(imModus == DLG_CHANGE || imModus == DLG_CHANGE_DIADATA)
			{
				olTmp += prmDFlight->Stod.Format("%d.%m.%Y");
				m_CE_DStoa.SetBKColor(YELLOW);
				m_CE_DStoa.SetTypeToTime(true, true);
				m_CE_DStoa.SetTextLimit(4,4,false);
			}
		}
		else
		{
			m_CE_DStoa.SetBKColor(WHITE);
			m_CE_DStoa.SetTypeToTime(false, true);
			m_CE_DStoa.SetTextLimit(0,4,true);
			olTmp = GetString(IDS_STRING1390);
			if(imModus == DLG_CHANGE || imModus == DLG_CHANGE_DIADATA)
				olTmp += prmDFlight->Stod.Format("%d.%m.%Y");
		}

		m_CS_Abflug.SetWindowText(olTmp);
		
		
//		m_Pbis = prmDFlight->Stod.Format("%d.%m.%Y");

		if(prmDFlight->Stod == TIMENULL)
			omDRefDat = CTime::GetCurrentTime();
		else
			omDRefDat = prmDFlight->Stod;

		m_DOrg3 = CString(pcgHome);

		//Weitere Flugnummern

		ogSeasonDlgFlights.GetJfnoArray(&omDJfno, prmDFlight);

		pomDJfnoTable->ResetContent();
		

		rlAttribC1.Type = KT_STRING;
		rlAttribC1.Format = "XXX";
		rlAttribC1.TextMinLenght = 2;
		rlAttribC1.TextMaxLenght = 3;
		rlAttribC1.Style = ES_UPPERCASE;

		rlAttribC2.Type = KT_STRING;
		rlAttribC2.Format = "#####";
		rlAttribC2.TextMinLenght = 1;
		rlAttribC2.TextMaxLenght = 5;

		rlAttribC3.Type = KT_STRING;
		rlAttribC3.Format = "X";
		rlAttribC3.TextMinLenght = 0;
		rlAttribC3.TextMaxLenght = 1;
		rlAttribC3.Style = ES_UPPERCASE;


		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.Font = &ogMS_Sans_Serif_8;
		rlColumnData.VerticalSeparator = SEPA_NORMAL;
		rlColumnData.HorizontalSeparator = SEPA_NONE;
		rlColumnData.Alignment = COLALIGN_LEFT;

		if(ogPrivList.GetStat("SEASONDLG_CB_DShowJfno") != '1')
			rlColumnData.BkColor = RGB(192,192,192);
		else
			rlColumnData.BkColor = RGB(255,255,255);


		for ( ilLc = 0; ilLc < omDJfno.GetSize(); ilLc++)
		{
			rlColumnData.Text = omDJfno[ilLc].Alc3;
			rlColumnData.Text.TrimRight();
			rlColumnData.EditAttrib = rlAttribC1;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = omDJfno[ilLc].Fltn;
			rlColumnData.Text.TrimRight();
			rlColumnData.EditAttrib = rlAttribC2;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = omDJfno[ilLc].Flns;
			rlColumnData.Text.TrimRight();
			rlColumnData.EditAttrib = rlAttribC3;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			pomDJfnoTable->AddTextLine(olColList, (void*)(NULL));
			olColList.DeleteAll();
		}
		for (ilLc = omDJfno.GetSize(); ilLc < 9; ilLc++)
		{
			rlColumnData.Text = "";
			rlColumnData.EditAttrib = rlAttribC1;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = "";
			rlColumnData.EditAttrib = rlAttribC2;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = "";
			rlColumnData.EditAttrib = rlAttribC3;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			pomDJfnoTable->AddTextLine(olColList, (void*)(NULL));
			olColList.DeleteAll();
		}
		pomDJfnoTable->DisplayTable();


		// Vias

		if( CString(prmDFlight->Ftyp)	== "Z" || CString(prmDFlight->Ftyp)	== "B")
		{
			pomDViaCtrl->bmEdit = false;
		}
		else
		{
			pomDViaCtrl->bmEdit = true;
		}

		pomDViaCtrl->SetViaList(prmDFlight->Vial, omARefDat);


		m_DCxx = FALSE;
		m_CB_DCxx.SetCheck(FALSE);
		m_DNoop = FALSE;
		m_CB_DNoop.SetCheck(FALSE);
		m_DStatus = -1;
		m_CB_DStatus.SetCheck(FALSE);
		m_CB_DStatus2.SetCheck(FALSE);
		m_CB_DStatus3.SetCheck(FALSE);
		
		
		if(strcmp(prmDFlight->Ftyp, "X") == 0)
		{
			m_DCxx = TRUE;
			m_CB_DCxx.SetCheck(TRUE);
		}
		if(strcmp(prmDFlight->Ftyp, "N") == 0)
		{
			m_DNoop = TRUE;
			m_CB_DNoop.SetCheck(TRUE);
		}
		if(strcmp(prmDFlight->Ftyp, "S") == 0)
		{
			m_DStatus = 0;
			m_CB_DStatus.SetCheck(TRUE);
		}
		if(strcmp(prmDFlight->Ftyp, "O") == 0)
		{
			m_DStatus = 1;
			m_CB_DStatus2.SetCheck(TRUE);
		}

		if((strcmp(prmDFlight->Ftyp, "") == 0) && (strlen(prmDFlight->Vers) > 0))
		{
			m_DStatus = 2;
			m_CB_DStatus3.SetCheck(TRUE);
		}
		if((strcmp(prmDFlight->Ftyp, "") == 0) && (strlen(prmDFlight->Vers) == 0))
		{
			m_DStatus = 0;
			m_CB_DStatus.SetCheck(TRUE);
		}

		
		m_DCht3 = CString(prmDFlight->Cht3);
		m_DOrg3 = CString(pcgHome);
		m_DDes3 = CString(prmDFlight->Des3);
		m_DDes4 = CString(prmDFlight->Des4);
		m_DTtyp = CString(prmDFlight->Ttyp);
//		m_DHtyp = CString(prmDFlight->Htyp);
		m_DStyp = CString(prmDFlight->Styp);
		m_DStev = CString(prmDFlight->Stev);
		m_DGtd1 = CString(prmDFlight->Gtd1);
		m_DGtd2 = CString(prmDFlight->Gtd2);

		m_DRem1 = CString(prmDFlight->Rem1);
		m_DRem2 = CString(prmDFlight->Rem2);

		char pclDays[10];
		GetDayOfWeek(prmDFlight->Stod, pclDays);
		m_DDays = CString(pclDays);


		m_DEtdi = DateToHourDivString(prmDFlight->Etdi, omDRefDat);
		m_DStod = DateToHourDivString(prmDFlight->Stod, omDRefDat);
		m_DStoa = DateToHourDivString(prmDFlight->Stoa, omDRefDat);

		m_DFluko      = prmDFlight->Fdat.Format("%d.%m.%Y  %H:%M");
		m_DCreated    = prmDFlight->Cdat.Format("%d.%m.%Y  %H:%M") + CString("  ") + CString(prmDFlight->Usec);
		m_DLastChange = prmDFlight->Lstu.Format("%d.%m.%Y  %H:%M") + CString("  ") + CString(prmDFlight->Useu);

		m_DPdbs = DateToHourDivString(prmDFlight->Pdbs, omDRefDat);
		m_DPdes = DateToHourDivString(prmDFlight->Pdes, omDRefDat);
		m_DGd1b = DateToHourDivString(prmDFlight->Gd1b, omDRefDat);
		m_DGd2b = DateToHourDivString(prmDFlight->Gd2b, omDRefDat);
		m_DGd1e = DateToHourDivString(prmDFlight->Gd1e, omDRefDat);
		m_DGd2e = DateToHourDivString(prmDFlight->Gd2e, omDRefDat);
		m_DW1bs = DateToHourDivString(prmDFlight->W1bs, omDRefDat);
		m_DW1es = DateToHourDivString(prmDFlight->W1es, omDRefDat);
		m_DW2bs = DateToHourDivString(prmDFlight->W2bs, omDRefDat);
		m_DW2es = DateToHourDivString(prmDFlight->W2es, omDRefDat);

		m_DDays = CString(prmDFlight->Dood);
		m_DTtyp = CString(prmDFlight->Ttyp);
//		m_DHtyp = CString(prmDFlight->Htyp);
		m_DStyp = CString(prmDFlight->Styp);
		m_DStev = CString(prmDFlight->Stev);
		m_DDes3 = CString(prmDFlight->Des3);
		m_DPstd = CString(prmDFlight->Pstd);
/*
		if (!m_DPstd.IsEmpty() && m_DPdbs.IsEmpty() && m_DPdes.IsEmpty())
		{
			CTimeSpan olDura;
			GetPosDefAllocDur(m_DPstd, atoi(prmDFlight->Ming), olDura);

			m_DPdes = DateToHourDivString(prmDFlight->Tifd, omDRefDat);
			m_DPdbs = DateToHourDivString(prmDFlight->Tifd - olDura, omDRefDat);
		}
*/
		m_DGtd1 = CString(prmDFlight->Gtd1);
		m_DGtd2 = CString(prmDFlight->Gtd2);
		m_DTgd1 = CString(prmDFlight->Tgd1);
		m_DTgd2 = CString(prmDFlight->Tgd2);
		m_DWro1 = CString(prmDFlight->Wro1);
		m_DWro2 = CString(prmDFlight->Wro2);
		m_DTwr1 = CString(prmDFlight->Twr1);
//		m_DTwr2 = CString(prmDFlight->Twr1);
		CString olTerm;
		ogBCD.GetField("WRO", "WNAM", m_DWro2, "TERM", olTerm);
		m_DTwr2 = CString(olTerm);


		m_DCsgn = CString(prmDFlight->Csgn);
		if(strlen(prmDFlight->Regn) != 0)
			m_Regn = CString(prmDFlight->Regn);

		if(imModus != DLG_CHANGE)
		{
			m_CE_Pbis.SetInitText(m_Pbis);
			m_CE_Regn.SetInitText("");
		}
		else
		{
			m_CE_Pbis.SetInitText("");
			m_CE_Regn.SetInitText(m_Regn);
		}


		GetDayOfWeek(prmDFlight->Stod, pclDays);
		m_DDays = CString(pclDays);


		m_CE_DCht3.SetInitText(m_DCht3);
		m_CE_DDays.SetInitText(m_DDays);
		m_CE_DOrg3.SetInitText(m_DOrg3);
		m_CE_DDes3.SetInitText(m_DDes3);
		m_CE_DDes4.SetInitText(m_DDes4);
		m_CE_DAlc3.SetInitText(m_DAlc3);
		m_CE_DFltn.SetInitText(m_DFltn);
		m_CE_DFlns.SetInitText(m_DFlns);
		m_CE_DFlti.SetInitText(m_DFlti);
		m_CE_DTtyp.SetInitText(m_DTtyp);
//		m_CE_DHtyp.SetInitText(m_DHtyp);
		m_CE_DStyp.SetInitText(m_DStyp);
		m_CE_DStev.SetInitText(m_DStev);
		m_CE_DRem1.SetInitText(m_DRem1);
		m_CE_DRem2.SetInitText(m_DRem2);
		m_CE_DEtdi.SetInitText(m_DEtdi);
		m_CE_DStod.SetInitText(m_DStod);
		m_CE_DStoa.SetInitText(m_DStoa);

		m_CE_DPstd.SetInitText(m_DPstd);
		m_CE_DPdbs.SetInitText(m_DPdbs);
		m_CE_DPdes.SetInitText(m_DPdes);
		m_CE_DGtd1.SetInitText(m_DGtd1);
		m_CE_DGtd2.SetInitText(m_DGtd2);
		m_CE_DTgd1.SetInitText(m_DTgd1);
		m_CE_DTgd2.SetInitText(m_DTgd2);
		m_CE_DGd1b.SetInitText(m_DGd1b);
		m_CE_DGd2b.SetInitText(m_DGd2b);
		m_CE_DGd1e.SetInitText(m_DGd1e);
		m_CE_DGd2e.SetInitText(m_DGd2e);
		m_CE_DWro1.SetInitText(m_DWro1);
		m_CE_DWro2.SetInitText(m_DWro2);
		m_CE_DTwr1.SetInitText(m_DTwr1);
		m_CE_DTwr2.SetInitText(m_DTwr2);
		m_CE_DW1bs.SetInitText(m_DW1bs);
		m_CE_DW1es.SetInitText(m_DW1es);
		m_CE_DW2bs.SetInitText(m_DW2bs);
		m_CE_DW2es.SetInitText(m_DW2es);
		
		m_CE_DFluko.SetInitText(m_DFluko);
		m_CE_DCreated.SetInitText(m_DCreated);
		m_CE_DLastChange.SetInitText(m_DLastChange);


		m_DBaz1 = CString(prmDFlight->Baz1);
		m_DBaz4 = CString(prmDFlight->Baz4);

		m_CE_DBaz1.SetInitText(m_DBaz1);
		m_CE_DBaz4.SetInitText(m_DBaz4);

		m_CE_DCsgn.SetInitText(m_DCsgn);
//		m_CE_Regn.SetInitText(m_Regn);

		if(!bmInit)
		{
			if(prmDFlight->Urno > 0)
			{
				omCcaData.Register();
				CString olFlnu;
				olFlnu.Format("%ld", prmDFlight->Urno);
				omCcaData.SetFilter(false, olFlnu);
				
				ReadCcaData();
			}
		}
		DShowCinsTable();

		bmAutoSetBaz1 = false;
		bmAutoSetBaz4 = false;

		CString olBaz1;
		CString olBaz4;
		CCADATA *prlCca;

		for( int i = 0; i < omDCins.GetSize(); i++)
		{
			prlCca = &omDCins[i];

			if(CString(prmDFlight->Flti) == "M")
			{
				if( !CString(prlCca->Ckic).IsEmpty())
				{
					if(ogBCD.GetField("CIC", "CNAM", prlCca->Ckic, "CICR") == "I")
						olBaz1 = ogBCD.GetField("CIC", "CNAM", prlCca->Ckic, "CBAZ");
					else
						olBaz4 = ogBCD.GetField("CIC", "CNAM", prlCca->Ckic, "CBAZ");
				}
			}
			else
			{
				if( !CString(prlCca->Ckic).IsEmpty())
				{
					olBaz1 = ogBCD.GetField("CIC", "CNAM", prlCca->Ckic, "CBAZ");
					if(!olBaz1.IsEmpty())
						break;
				}
			}
		}

		if( CString(prmDFlight->Baz1) == olBaz1)
			bmAutoSetBaz1 = true;

		if( CString(prmDFlight->Baz4) == olBaz4)
			bmAutoSetBaz4 = true;


	}

	m_CB_ARemp.ResetContent();
	m_CB_DRemp.ResetContent();


	m_CB_ARemp.AddString(" ");
	m_CB_DRemp.AddString(" ");
	
	int ilAIndex = 0;	
	int ilDIndex = 0;	
	CString olCode;
	CString olItem;
	
	int ilCount = ogBCD.GetDataCount("FID");
	ogBCD.SetSort("FID", ogFIDRemarkField+"+", true);

	
	CString olCodeA = CString(prmAFlight->Remp);
	CString olCodeD = CString(prmDFlight->Remp);

	olCodeA.TrimRight();
	olCodeD.TrimRight();

	for(int i = 0; i < ilCount; i++)
	{
		olItem = ogBCD.GetFields("FID",i , "CODE,"+ogFIDRemarkField, "  ", FILLBLANK);
		m_CB_ARemp.AddString(olItem);
		m_CB_DRemp.AddString(olItem);
		olCode = ogBCD.GetField("FID",i , "CODE" );

		
		if((olCode.Find(olCodeA) >= 0) && (!olCodeA.IsEmpty()))
			ilAIndex = i + 1;
		
		if((olCode.Find(olCodeD) >= 0) && (!olCodeD.IsEmpty()))
			ilDIndex = i + 1;
	}
	
	m_CB_ARemp.SetCurSel(ilAIndex);
	m_CB_DRemp.SetCurSel(ilDIndex);



	if(bpArrival && bpDeparture)
	{
		bmChanged = false;
		EnableGlobal();
		EnableArrival();
		EnableDeparture();
		EnableGatPos();
	}
	else
	{
		if(bpArrival)
		{
			EnableGlobal();
			EnableArrival();
		}
		else
		{
			EnableGlobal();
			EnableDeparture();
		}
	}


	if (prmAFlightSave != NULL)
	{
		if (prmAFlightSave->Jcnt[0] == '\0')	
			strcpy(prmAFlightSave->Jcnt, "0");
	}
	if (prmDFlightSave != NULL)
	{
		if (prmDFlightSave->Jcnt[0] == '\0')	
			strcpy(prmDFlightSave->Jcnt, "0");
	}

	HandlePostFlight();

	CCS_CATCH_ALL

}



/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Fill the SEASONDLGFLIGHTDATArecords
//
void CSeasonDlg::FillFlightData() 
{

	CCS_TRY

	//killfocus - check for the curent field
	NextDlgCtrl(); 
	PrevDlgCtrl();

	CTime olTime;

	///////////////////////////////////////////
	// globals
	//
	m_CE_Regn.GetWindowText(m_Regn);

	m_CE_Pbis.GetWindowText(m_Pbis);
	m_CE_Pvom.GetWindowText(m_Pvom);

	m_CE_ADays.GetWindowText(m_ADays);
	m_CE_DDays.GetWindowText(m_DDays);
	
	char pclDays[10];

	if(	m_Pbis.IsEmpty())
	{
		m_CE_Pbis.SetWindowText(m_Pvom);
		m_Pbis = m_Pvom;
	}	
	
	
	m_CE_Freq.GetWindowText(m_Freq);
	m_CE_Ming.GetWindowText(m_Ming);
	m_CE_Act3.GetWindowText(m_Act3);
	m_CE_Act5.GetWindowText(m_Act5);
	m_CE_Nose.GetWindowText(m_Nose);


	if((imModus == DLG_NEW) || (imModus == DLG_COPY))
	{
		if(m_Freq.IsEmpty())
			m_Freq = "1";
	}


	///////////////////////////////////////////
	// Ankunft
	//
	m_CE_ACht3.GetWindowText(m_ACht3);
	m_CE_AAlc3.GetWindowText(m_AAlc3);
	m_CE_AFltn.GetWindowText(m_AFltn);
	m_CE_AFlns.GetWindowText(m_AFlns);
	m_CE_AFlti.GetWindowText(m_AFlti);
	m_CE_ATtyp.GetWindowText(m_ATtyp);
//	m_CE_AHtyp.GetWindowText(m_AHtyp);
	m_CE_AStyp.GetWindowText(m_AStyp);
	m_CE_AStev.GetWindowText(m_AStev);
	m_CE_ADes3.GetWindowText(m_ADes3);
	m_CE_AOrg4.GetWindowText(m_AOrg4);
	m_CE_AOrg3.GetWindowText(m_AOrg3);
	m_CE_AEtai.GetWindowText(m_AEtai);
	m_CE_AStod.GetWindowText(m_AStod);
	m_CE_AStoa.GetWindowText(m_AStoa);
	m_CE_APsta.GetWindowText(m_APsta);
	m_CE_AGta1.GetWindowText(m_AGta1);
	m_CE_AGta2.GetWindowText(m_AGta2);
	m_CE_AExt1.GetWindowText(m_AExt1);
	m_CE_AExt2.GetWindowText(m_AExt2);
	m_CE_ABlt1.GetWindowText(m_ABlt1);
	m_CE_ABlt2.GetWindowText(m_ABlt2);
	m_CE_ARem1.GetWindowText(m_ARem1);
	m_CE_ARem2.GetWindowText(m_ARem2);
	
	m_CE_APsta.GetWindowText(m_APsta);
	m_CE_APabs.GetWindowText(m_APabs);
	m_CE_APaes.GetWindowText(m_APaes);
	m_CE_AGta1.GetWindowText(m_AGta1);
	m_CE_AGta2.GetWindowText(m_AGta2);
	m_CE_ATga1.GetWindowText(m_ATga1);
	m_CE_ATga2.GetWindowText(m_ATga2);
	m_CE_AGa1b.GetWindowText(m_AGa1b);
	m_CE_AGa2b.GetWindowText(m_AGa2b);
	m_CE_AGa1e.GetWindowText(m_AGa1e);
	m_CE_AGa2e.GetWindowText(m_AGa2e);
	m_CE_ABlt1.GetWindowText(m_ABlt1);
	m_CE_ABlt2.GetWindowText(m_ABlt2);
	m_CE_ATmb1.GetWindowText(m_ATmb1);
	m_CE_ATmb2.GetWindowText(m_ATmb2);
	m_CE_AB1bs.GetWindowText(m_AB1bs);
	m_CE_AB2bs.GetWindowText(m_AB2bs);
	m_CE_AB1es.GetWindowText(m_AB1es);
	m_CE_AB2es.GetWindowText(m_AB2es);
	m_CE_AExt1.GetWindowText(m_AExt1);
	m_CE_AExt2.GetWindowText(m_AExt2);
	m_CE_ATet1.GetWindowText(m_ATet1);
	m_CE_ATet2.GetWindowText(m_ATet2);

	m_CE_ACsgn.GetWindowText(m_ACsgn);




	if(m_CE_ATtyp.IsChanged() && (strcmp((LPCSTR)m_ATtyp, prmAFlightSave->Ttyp) != 0))
		strcpy(prmAFlight->Sttt,"U");


	strcpy(prmAFlight->Alc3,omAAlc3);
	strcpy(prmAFlight->Alc2,omAAlc2);
	strcpy(prmAFlight->Fltn,m_AFltn);
	strcpy(prmAFlight->Flns,m_AFlns);
	strcpy(prmAFlight->Flti,m_AFlti);

	strcpy(prmAFlight->Cht3,m_ACht3);

	strcpy(prmAFlight->Ttyp,m_ATtyp);
//	strcpy(prmAFlight->Htyp,m_AHtyp);
	strcpy(prmAFlight->Styp,m_AStyp);
	strcpy(prmAFlight->Stev,m_AStev);
	
	strcpy(prmAFlight->Des3,m_ADes3);
	strcpy(prmAFlight->Org3,m_AOrg3);
	strcpy(prmAFlight->Org4,m_AOrg4);
	strcpy(prmAFlight->Des4,pcgHome4);

	strcpy(prmAFlight->Csgn,m_ACsgn);
	strcpy(prmAFlight->Regn, m_Regn);

	omARefDat = DateStringToDate(m_Pvom);

	if(imModus == DLG_CHANGE)
	{
		prmAFlight->Stoa = HourStringToDate( m_AStoa, prmAFlight->Stoa);
	}
	else
	{
		prmAFlight->Stoa = HourStringToDate( m_AStoa, omARefDat);
	}
	omARefDat = prmAFlight->Stoa;


	prmAFlight->Etau = HourStringToDate( m_AEtai, omARefDat);
	prmAFlight->Stod = HourStringToDate( m_AStod, omARefDat);


	if((imModus == DLG_CHANGE) && (prmAFlight->Stoa != TIMENULL))
	{
		GetDayOfWeek(prmAFlight->Stoa, prmAFlight->Dooa);
	}
	
	strcpy(prmAFlight->Nose,m_Nose);
	strcpy(prmAFlight->Psta,m_APsta);
	strcpy(prmAFlight->Gta1,m_AGta1);
	strcpy(prmAFlight->Gta2,m_AGta2);
	strcpy(prmAFlight->Blt1,m_ABlt1);
	strcpy(prmAFlight->Blt2,m_ABlt2);
	strcpy(prmAFlight->Ext1,m_AExt1);
	strcpy(prmAFlight->Ext2,m_AExt2);

	strcpy(prmAFlight->Rem1,m_ARem1);
	strcpy(prmAFlight->Rem2,m_ARem2);
	if(m_ARem1.IsEmpty())
		strcpy(prmAFlight->Isre,"");
	else
		strcpy(prmAFlight->Isre,"+");

	strcpy(prmAFlight->Ming,m_Ming);
	strcpy(prmAFlight->Act3,m_Act3);
	strcpy(prmAFlight->Act5,m_Act5);

	strcpy(prmAFlight->Psta, m_APsta);
	strcpy(prmAFlight->Gta1, m_AGta1);
	strcpy(prmAFlight->Gta2, m_AGta2);
	strcpy(prmAFlight->Tga1, m_ATga1);
	strcpy(prmAFlight->Tga2, m_ATga2);
	strcpy(prmAFlight->Blt1, m_ABlt1);
	strcpy(prmAFlight->Blt2, m_ABlt2);
	strcpy(prmAFlight->Tmb1, m_ATmb1);
	strcpy(prmAFlight->Tmb2, m_ATmb2);
	strcpy(prmAFlight->Ext1, m_AExt1);
	strcpy(prmAFlight->Ext2, m_AExt2);
	strcpy(prmAFlight->Tet1, m_ATet1);
	strcpy(prmAFlight->Tet2, m_ATet2);


	olTime = HourStringToDate( m_APabs, omARefDat);
	prmAFlight->Pabs = olTime;
	
	olTime = HourStringToDate( m_APaes, omARefDat);
	prmAFlight->Paes = olTime;

	olTime = HourStringToDate( m_AB1bs, omARefDat);
	prmAFlight->B1bs = olTime;
	
	olTime = HourStringToDate( m_AB2bs, omARefDat);
	prmAFlight->B2bs = olTime;

	olTime = HourStringToDate( m_AB1es, omARefDat);
	prmAFlight->B1es = olTime;

	olTime = HourStringToDate( m_AB2es, omARefDat);
	prmAFlight->B2es = olTime;

	olTime = HourStringToDate( m_AGa1b, omARefDat);
	prmAFlight->Ga1b = olTime;

	olTime = HourStringToDate( m_AGa2b, omARefDat);
	prmAFlight->Ga2b = olTime;

	olTime = HourStringToDate( m_AGa1e, omARefDat);
	prmAFlight->Ga1e = olTime;

	olTime = HourStringToDate( m_AGa2e, omARefDat);
	prmAFlight->Ga2e = olTime;
	

	CString olFlno= ogSeasonDlgFlights.CreateFlno(m_AAlc3, m_AFltn, m_AFlns);
	CString olFlnoT(olFlno);
	olFlnoT.TrimRight();
	if (strcmp(olFlno, prmAFlight->Flno) != 0 && strcmp(olFlnoT, prmAFlight->Flno) != 0)
		strcpy(prmAFlight->Flno, olFlno);
	

	strcpy(prmAFlight->Vers, "");

	if(m_CB_AStatus.GetCheck() == TRUE)
	{
		strcpy(prmAFlight->Ftyp, "S");
	}
	if(m_CB_AStatus2.GetCheck() == TRUE)
	{
		strcpy(prmAFlight->Ftyp, "O");
	}
	if(m_CB_AStatus3.GetCheck() == TRUE)
	{
		strcpy(prmAFlight->Ftyp, "");
		strcpy(prmAFlight->Vers, "PROGNOSE");
	}
	if(m_CB_ACxx.GetCheck() == TRUE)
		strcpy(prmAFlight->Ftyp, "X");

	if(m_CB_ANoop.GetCheck() == TRUE)
		strcpy(prmAFlight->Ftyp, "N");


	int ilCurSel;
	CString olTmp;


	if((ilCurSel = m_CB_ARemp.GetCurSel()) == CB_ERR)
		strcpy(prmAFlight->Remp, " ");
	else
	{
		m_CB_ARemp.GetLBText(ilCurSel, olTmp);
		olTmp.TrimRight();
		if(!olTmp.IsEmpty())
			strcpy(prmAFlight->Remp, olTmp.Left(4));
		else
			strcpy(prmAFlight->Remp, "");
	}



	///////////////////////////////////////////////
	// weitere Flugnummern ankunft
	//
	CString olAlc3;
	CString olFltn;
	CString olFlns;

	strcpy(prmAFlight->Jfno, "");
	int ilJcnt = 0;
	CString olJfno;

	int ilLines = pomAJfnoTable->GetLinesCount();
	for(int i = 0; i < ilLines; i++)
	{
		pomAJfnoTable->GetTextFieldValue(i, 0, olAlc3);
		pomAJfnoTable->GetTextFieldValue(i, 1, olFltn);
		pomAJfnoTable->GetTextFieldValue(i, 2, olFlns);
		if(!olAlc3.IsEmpty() || !olFltn.IsEmpty())
		{
			olJfno +=   ogSeasonDlgFlights.CreateFlno(olAlc3, olFltn, olFlns);
			ilJcnt++;
		}
	}
	olJfno.TrimRight();
	strcpy(prmAFlight->Jfno, olJfno);
	itoa(ilJcnt, prmAFlight->Jcnt, 10);


	///////////////////////////////////////////////
	// vias ankunft
	//

	pomAViaCtrl->GetViaList(prmAFlight->Vial);
	
	///////////////////////////////////////////
	// Abflug
	//
	m_CE_DCht3.GetWindowText(m_DCht3);
	m_CE_DEtdi.GetWindowText(m_DEtdi);
	m_CE_DStod.GetWindowText(m_DStod);
	m_CE_DStoa.GetWindowText(m_DStoa);
	m_CE_DAlc3.GetWindowText(m_DAlc3);
	m_CE_DFltn.GetWindowText(m_DFltn);
	m_CE_DFlns.GetWindowText(m_DFlns);
	m_CE_DFlti.GetWindowText(m_DFlti);
	m_CE_DDes3.GetWindowText(m_DDes3);
	m_CE_DDes4.GetWindowText(m_DDes4);
	m_CE_DOrg3.GetWindowText(m_DOrg3);
	m_CE_DTtyp.GetWindowText(m_DTtyp);
//	m_CE_DHtyp.GetWindowText(m_DHtyp);
	m_CE_DStyp.GetWindowText(m_DStyp);
	m_CE_DStev.GetWindowText(m_DStev);
	m_CE_DPstd.GetWindowText(m_DPstd);
	m_CE_DGtd1.GetWindowText(m_DGtd1);
	m_CE_DGtd2.GetWindowText(m_DGtd2);
	m_CE_DRem1.GetWindowText(m_DRem1);
	m_CE_DRem2.GetWindowText(m_DRem2);

	m_CE_DDays.GetWindowText(m_DDays);
	m_CE_DPdbs.GetWindowText(m_DPdbs);
	m_CE_DPdes.GetWindowText(m_DPdes);
	m_CE_DTgd1.GetWindowText(m_DTgd1);
	m_CE_DTgd2.GetWindowText(m_DTgd2);
	m_CE_DGd1b.GetWindowText(m_DGd1b);
	m_CE_DGd2b.GetWindowText(m_DGd2b);
	m_CE_DGd1e.GetWindowText(m_DGd1e);
	m_CE_DGd2e.GetWindowText(m_DGd2e);
	m_CE_DWro1.GetWindowText(m_DWro1);
	m_CE_DWro2.GetWindowText(m_DWro2);
	m_CE_DTwr1.GetWindowText(m_DTwr1);
	m_CE_DTwr2.GetWindowText(m_DTwr2);
	m_CE_DW1bs.GetWindowText(m_DW1bs);
	m_CE_DW1es.GetWindowText(m_DW1es);
	m_CE_DW2bs.GetWindowText(m_DW2bs);
	m_CE_DW2es.GetWindowText(m_DW2es);

	m_CE_DCsgn.GetWindowText(m_DCsgn);


	if(m_CE_DTtyp.IsChanged() && (strcmp((LPCSTR)m_DTtyp, prmDFlightSave->Ttyp) != 0))
		strcpy(prmDFlight->Sttt,"U");

	strcpy(prmDFlight->Nose,m_Nose);

	strcpy(prmDFlight->Alc3,omDAlc3);
	strcpy(prmDFlight->Alc2,omDAlc2);
	strcpy(prmDFlight->Fltn,m_DFltn);
	strcpy(prmDFlight->Flns,m_DFlns);
	strcpy(prmDFlight->Flti,m_DFlti);
	strcpy(prmDFlight->Cht3,m_DCht3);

	strcpy(prmDFlight->Ttyp,m_DTtyp);
//	strcpy(prmDFlight->Htyp,m_DHtyp);
	strcpy(prmDFlight->Styp,m_DStyp);
	strcpy(prmDFlight->Stev,m_DStev);
	
	strcpy(prmDFlight->Des3,m_DDes3);
	strcpy(prmDFlight->Des4,m_DDes4);
	strcpy(prmDFlight->Org3,m_DOrg3);
	strcpy(prmDFlight->Org4,pcgHome4);

 	strcpy(prmDFlight->Csgn,m_DCsgn);
	strcpy(prmDFlight->Regn, m_Regn);

	omDRefDat = DateStringToDate(m_Pvom);
  
  
	m_CE_DBaz1.GetWindowText(m_DBaz1);
	m_CE_DBaz4.GetWindowText(m_DBaz4);

	strcpy(prmDFlight->Baz1, m_DBaz1);
	strcpy(prmDFlight->Baz4, m_DBaz4);


	if(imModus == DLG_CHANGE)
	{
		prmDFlight->Stod = HourStringToDate( m_DStod, prmDFlight->Stod);
	}
	else
	{
		prmDFlight->Stod = HourStringToDate( m_DStod, omDRefDat);
	}
	omDRefDat = prmDFlight->Stod;


	if((imModus == DLG_CHANGE) && (prmDFlight->Stod != TIMENULL))
	{
		GetDayOfWeek(prmDFlight->Stod, prmDFlight->Dood);
	}

	prmDFlight->Etdu = HourStringToDate( m_DEtdi, omDRefDat);
	prmDFlight->Stoa = HourStringToDate( m_DStoa, omDRefDat);

	strcpy(prmDFlight->Pstd, m_DPstd);
	strcpy(prmDFlight->Gtd1, m_DGtd1);
	strcpy(prmDFlight->Gtd2, m_DGtd2);
	strcpy(prmDFlight->Tgd1, m_DTgd1);
	strcpy(prmDFlight->Tgd2, m_DTgd2);
	strcpy(prmDFlight->Wro1, m_DWro1);
	strcpy(prmDFlight->Wro2, m_DWro2);
	strcpy(prmDFlight->Twr1, m_DTwr1);
//	strcpy(prmDFlight->Twr2, m_DTwr2);

	
	olTime = HourStringToDate( m_DPdbs, omDRefDat);
	prmDFlight->Pdbs = olTime;

	olTime = HourStringToDate( m_DPdes, omDRefDat);
	prmDFlight->Pdes = olTime;

	olTime = HourStringToDate( m_DGd1b, omDRefDat);
	prmDFlight->Gd1b = olTime;

	olTime = HourStringToDate( m_DGd2b, omDRefDat);
	prmDFlight->Gd2b = olTime;

	olTime = HourStringToDate( m_DGd1e, omDRefDat);
	prmDFlight->Gd1e = olTime;

	olTime = HourStringToDate( m_DGd2e, omDRefDat);
	prmDFlight->Gd2e = olTime;

	olTime = HourStringToDate( m_DW1bs, omDRefDat);
	prmDFlight->W1bs = olTime;

	olTime = HourStringToDate( m_DW1es, omDRefDat);
	prmDFlight->W1es = olTime;

	olTime = HourStringToDate( m_DW2bs, omDRefDat);
	prmDFlight->W2bs = olTime;

	olTime = HourStringToDate( m_DW2es, omDRefDat);
	prmDFlight->W2es = olTime;


	strcpy(prmDFlight->Rem1,m_DRem1);
	strcpy(prmDFlight->Rem2,m_DRem2);
	if(m_DRem1.IsEmpty())
		strcpy(prmDFlight->Isre,"");
	else
		strcpy(prmDFlight->Isre,"+");

	strcpy(prmDFlight->Ming,m_Ming);
	strcpy(prmDFlight->Act3,m_Act3);
	strcpy(prmDFlight->Act5,m_Act5);


	olFlno = ogSeasonDlgFlights.CreateFlno(m_DAlc3, m_DFltn, m_DFlns); 
	olFlnoT = olFlno;
	olFlnoT.TrimRight();
	if (strcmp(olFlno, prmDFlight->Flno) != 0 && strcmp(olFlnoT, prmDFlight->Flno) != 0)
		strcpy(prmDFlight->Flno, olFlno);


	strcpy(prmDFlight->Vers, "");
	if(m_CB_DStatus.GetCheck() == TRUE)
	{
		strcpy(prmDFlight->Ftyp, "S");
	}
	if(m_CB_DStatus2.GetCheck() == TRUE)
	{
		strcpy(prmDFlight->Ftyp, "O");
	}
	if(m_CB_DStatus3.GetCheck() == TRUE)
	{
		strcpy(prmDFlight->Ftyp, "");
		strcpy(prmDFlight->Vers, "PROGNOSE");
	}
	if(m_CB_DCxx.GetCheck() == TRUE)
		strcpy(prmDFlight->Ftyp, "X");

	if(m_CB_DNoop.GetCheck() == TRUE)
		strcpy(prmDFlight->Ftyp, "N");


	if((ilCurSel = m_CB_DRemp.GetCurSel()) == CB_ERR)
		strcpy(prmDFlight->Remp, " ");
	else
	{
		m_CB_DRemp.GetLBText(ilCurSel, olTmp);
		olTmp.TrimRight();
		if(!olTmp.IsEmpty())
			strcpy(prmDFlight->Remp, olTmp.Left(4));
		else
			strcpy(prmDFlight->Remp, "");
	}


	///////////////////////////////////////////////
	// weitere Flugnummern abflug
	//

	strcpy(prmDFlight->Jfno, "");
	ilJcnt = 0;
	olJfno = "";

	ilLines = pomDJfnoTable->GetLinesCount();
	for( i = 0; i < ilLines; i++)
	{
		pomDJfnoTable->GetTextFieldValue(i, 0, olAlc3);
		pomDJfnoTable->GetTextFieldValue(i, 1, olFltn);
		pomDJfnoTable->GetTextFieldValue(i, 2, olFlns);
		if(!olAlc3.IsEmpty() || !olFltn.IsEmpty())
		{
			olJfno +=  ogSeasonDlgFlights.CreateFlno(olAlc3, olFltn, olFlns);
			ilJcnt++;
		}
	}
	olJfno.TrimRight();
	strcpy(prmDFlight->Jfno, olJfno);
	itoa(ilJcnt, prmDFlight->Jcnt, 10);


	////////////////////////////////////
	// Via Abflug

	pomDViaCtrl->GetViaList(prmDFlight->Vial);


	////////////////////////////////////
	// CheckinSchalter

	CString olMax = "   ";
	CString olMin = "zzz";

	CString olTmpStr;
	CString olCkic;
	CString olCkit;
	CTime olCkbs;
	CTime olCkes;
	CCADATA *prlCca;
	if(pomDCinsTable != NULL)
	{
		ilLines = pomDCinsTable->GetLinesCount();
		for( i = 0; i < ilLines; i++)
		{
			pomDCinsTable->GetTextFieldValue(i, 0, olCkic);
			pomDCinsTable->GetTextFieldValue(i, 1, olCkit);

			
			pomDCinsTable->GetTextFieldValue(i, 2, olTmp);
			olCkbs = HourStringToDate(olTmp, omDRefDat); 
			if(bmLocalTime) omCcaData.LocalToUtc(olCkbs);

			pomDCinsTable->GetTextFieldValue(i, 3, olTmp);
			olCkes = HourStringToDate(olTmp, omDRefDat); 
			if(bmLocalTime) omCcaData.LocalToUtc(olCkes);

			
			if(i >= omDCins.GetSize())
			{
				prlCca = new CCADATA;
				omDCins.Add(prlCca);

				// set cca stod
				prlCca->Stod = prmDFlight->Stod;
				if(bmLocalTime) omCcaData.LocalToUtc(prlCca->Stod);

				// set cca flno
				olTmpStr = prmDFlight->Flno;
				olTmpStr.TrimRight();
				strcpy(prlCca->Flno, olTmpStr);

				// set cca act3
				strcpy(prlCca->Act3, prmDFlight->Act3);
			}
			else
			{
				prlCca = &omDCins[i];
			}
			strcpy(prlCca->Ckic, olCkic);
			strcpy(prlCca->Ckit, olCkit);


			prlCca->Ckbs = olCkbs;
			prlCca->Ckes = olCkes;


			//if(bmLocalTime) omCcaData.StructLocalToUtc(prlCca);
			//olData.StructLocalToUtc(prlCca);

			olCkic.TrimRight();

			if((olCkic < olMin) && !olCkic.IsEmpty())
				olMin = olCkic;

			if((olCkic > olMax) && !olCkic.IsEmpty())
				olMax = olCkic;
		}

		olMax.TrimRight();
		olMin.TrimRight();

		
		// in Athens the flight set CKIF and CKIT automatically !!
		if (strcmp(pcgHome, "ATH") != 0)
		{
			if(olMax != "   " && !olMax.IsEmpty())
				strcpy(prmDFlight->Ckit, olMax);
			else
				strcpy(prmDFlight->Ckit, "");
		
			if(olMin != "zzz" && !olMin.IsEmpty())
				strcpy(prmDFlight->Ckif, olMin);
			else
				strcpy(prmDFlight->Ckif, "");
		}	
	
		/*
		omToSaveCca.RemoveAll();
		for(int i = 0; i < omDCins.GetSize(); i++)
		{
			omDCins[i].Flnu = prmDFlight->Urno;
			omDCins[i].Stod = prmDFlight->Stod;
			strcpy(omDCins[i].Flno, prmDFlight->Flno);
			if(omDCins[i].Urno == 0)
			{
				if(strcmp(omDCins[i].Ckic, "") != 0 || strcmp(omDCins[i].Ckit, "") != 0 ||
					omDCins[i].Ckbs != TIMENULL || omDCins[i].Ckba != TIMENULL || 
					omDCins[i].Ckes != TIMENULL || omDCins[i].Ckea != TIMENULL)
				{
					omDCins[i].Cdat = CTime::GetCurrentTime();
					strcpy(omDCins[i].Usec, pcgUser);
					omDCins[i].Urno = ogBasicData.GetNextUrno();	
					omDCins[i].IsChanged = DATA_NEW;
					omToSaveCca.Add(&omDCins[i]);
				}
			}
			else
			{
				for(int j = 0; j < omOldCcaValues.GetSize(); j++)
				{
					CCADATA rlN, rlO;
					rlN = omDCins[i];
					rlO = omOldCcaValues[j];
					CString olN = rlN.Ckbs.Format("%H:%M");
					CString olO = rlO.Ckbs.Format("%H:%M");
					if(omDCins[i].Urno == omOldCcaValues[j].Urno)
					{
						bool blIsChanged = false;
						if(strcmp(omDCins[i].Ckic ,omOldCcaValues[j].Ckic) != 0)
						{
							blIsChanged = true;
						}
						if(strcmp(omDCins[i].Ckit ,omOldCcaValues[j].Ckit) != 0)
						{
							blIsChanged = true;
						}
						if(omDCins[i].Ckbs != omOldCcaValues[j].Ckbs)
						{
							blIsChanged = true;
						}
						if(omDCins[i].Ckba != omOldCcaValues[j].Ckba)
						{
							blIsChanged = true;
						}
						if(omDCins[i].Ckes != omOldCcaValues[j].Ckes)
						{
							blIsChanged = true;
						}
						if(omDCins[i].Ckea != omOldCcaValues[j].Ckea)
						{
							blIsChanged = true;
						}
						//So dann hat sich was ge�ndert ??
						if(blIsChanged == true)
						{
							strcpy(omDCins[i].Useu, pcgUser);
							omDCins[i].Lstu = olTime;
							omDCins[i].IsChanged = DATA_CHANGED;
							omDCins[i].Stod = prmDFlight->Stod;
							strcpy(omDCins[i].Flno, prmDFlight->Flno);
							omToSaveCca.Add(&omDCins[i]);
							j = omOldCcaValues.GetSize();
						}
					}
				}
			}
		}
		*/
	}

	/////////////////////////////////////////////////////////////////////////
	//// Get carousel times
	CString olBaz1;
	CString olBaz4;
	CTime opStart = TIMENULL;
	CTime opEnd	  = TIMENULL;

	for( i = 0; i < omDCins.GetSize(); i++)
	{
		prlCca = &omDCins[i];

		if(CString(prmDFlight->Flti) == "M")
		{
			if( !CString(prlCca->Ckic).IsEmpty())
			{
				if(ogBCD.GetField("CIC", "CNAM", prlCca->Ckic, "CICR") == "I")
					olBaz1 = ogBCD.GetField("CIC", "CNAM", prlCca->Ckic, "CBAZ");
				else
					olBaz4 = ogBCD.GetField("CIC", "CNAM", prlCca->Ckic, "CBAZ");
			}
		}
		else
		{
			if( !CString(prlCca->Ckic).IsEmpty())
			{
				olBaz1 = ogBCD.GetField("CIC", "CNAM", prlCca->Ckic, "CBAZ");
				if(!olBaz1.IsEmpty())
					break;
			}
		}

		if(prlCca->Ckba != TIMENULL)
		{
			opStart = prlCca->Ckba;
		}
		else
		{
			if(prlCca->Ckbs != TIMENULL)
			{
				opStart = prlCca->Ckbs;
			}
		}

		if(prlCca->Ckea != TIMENULL)
		{
			opEnd = prlCca->Ckea;
		}
		else
		{
			if(prlCca->Ckes != TIMENULL)
			{
				opEnd = prlCca->Ckes;
			}
		}

	}
	//// END (get carousel times)
	////////////////////////////////////////////////////////////////////////

	if((!m_CE_DBaz1.IsChanged() && bmAutoSetBaz1) || CString(prmDFlight->Baz1).IsEmpty())
		strcpy(prmDFlight->Baz1, olBaz1);
	if((!m_CE_DBaz4.IsChanged() && bmAutoSetBaz4) || CString(prmDFlight->Baz4).IsEmpty())
		strcpy(prmDFlight->Baz4, olBaz4);


	if(!CString(prmDFlight->Baz1).IsEmpty())
	{
		prmDFlight->Bao1 = opStart;
		prmDFlight->Bac1 = opEnd;
	}
	else
	{
		prmDFlight->Bao1 = TIMENULL;
		prmDFlight->Bac1 = TIMENULL;
	}

	if(!CString(prmDFlight->Baz4).IsEmpty())
	{
		prmDFlight->Bao4 = opStart;
		prmDFlight->Bac4 = opEnd;
	}
	else
	{
		prmDFlight->Bao4 = TIMENULL;
		prmDFlight->Bac4 = TIMENULL;
	}


	if(imModus != DLG_CHANGE)
	{

		if(m_Pvom == m_Pbis)
		{
			CTime olTime = DateStringToDate(m_Pbis);
			if( olTime != TIMENULL)
			{
				if(	(m_ADays.IsEmpty()) && (prmAFlight->Stoa != TIMENULL))
				{
					GetDayOfWeek(olTime, pclDays);
					m_ADays = CString(pclDays);
					m_CE_ADays.SetWindowText(m_ADays);
				}

				if(	(m_DDays.IsEmpty()) && (prmDFlight->Stod != TIMENULL))
				{
					GetDayOfWeek(olTime, pclDays);
					m_DDays = CString(pclDays);
					m_CE_DDays.SetWindowText(m_DDays);
				}
			}
		}
	}






	CCS_CATCH_ALL

}



// Only fill flight data with Pos/Gate/Belt/Wro allocation values!
void CSeasonDlg::FillGatPosFlightData() 
{

	CCS_TRY

	//killfocus - check for the curent field
	NextDlgCtrl(); 
	PrevDlgCtrl();

	CTime olTime;


	///////////////////////////////////////////
	// Ankunft
	//
	m_CE_APsta.GetWindowText(m_APsta);
	m_CE_AGta1.GetWindowText(m_AGta1);
	m_CE_AGta2.GetWindowText(m_AGta2);
	m_CE_ABlt1.GetWindowText(m_ABlt1);
	m_CE_ABlt2.GetWindowText(m_ABlt2);
	
	m_CE_APabs.GetWindowText(m_APabs);
	m_CE_APaes.GetWindowText(m_APaes);
	m_CE_AGa1b.GetWindowText(m_AGa1b);
	m_CE_AGa2b.GetWindowText(m_AGa2b);
	m_CE_AGa1e.GetWindowText(m_AGa1e);
	m_CE_AGa2e.GetWindowText(m_AGa2e);
	m_CE_AB1bs.GetWindowText(m_AB1bs);
	m_CE_AB2bs.GetWindowText(m_AB2bs);
	m_CE_AB1es.GetWindowText(m_AB1es);
	m_CE_AB2es.GetWindowText(m_AB2es);

	/* // No Terminals, because DIAFLIGHTDATA - Structure has no terminal fields!
	m_CE_ATmb1.GetWindowText(m_ATmb1);
	m_CE_ATmb2.GetWindowText(m_ATmb2);
	m_CE_ATga1.GetWindowText(m_ATga1);
	m_CE_ATga2.GetWindowText(m_ATga2);
	*/

	strcpy(prmAFlight->Psta,m_APsta);
	strcpy(prmAFlight->Gta1,m_AGta1);
	strcpy(prmAFlight->Gta2,m_AGta2);
	strcpy(prmAFlight->Blt1,m_ABlt1);
	strcpy(prmAFlight->Blt2,m_ABlt2);

	/*
	strcpy(prmAFlight->Tmb1, m_ATmb1);
	strcpy(prmAFlight->Tmb2, m_ATmb2);
	strcpy(prmAFlight->Tga1, m_ATga1);
	strcpy(prmAFlight->Tga2, m_ATga2);
	*/

	omARefDat = prmAFlight->Stoa;

	olTime = HourStringToDate( m_APabs, omARefDat);
	prmAFlight->Pabs = olTime;
	
	olTime = HourStringToDate( m_APaes, omARefDat);
	prmAFlight->Paes = olTime;

	olTime = HourStringToDate( m_AB1bs, omARefDat);
	prmAFlight->B1bs = olTime;
	
	olTime = HourStringToDate( m_AB2bs, omARefDat);
	prmAFlight->B2bs = olTime;

	olTime = HourStringToDate( m_AB1es, omARefDat);
	prmAFlight->B1es = olTime;

	olTime = HourStringToDate( m_AB2es, omARefDat);
	prmAFlight->B2es = olTime;

	olTime = HourStringToDate( m_AGa1b, omARefDat);
	prmAFlight->Ga1b = olTime;

	olTime = HourStringToDate( m_AGa2b, omARefDat);
	prmAFlight->Ga2b = olTime;

	olTime = HourStringToDate( m_AGa1e, omARefDat);
	prmAFlight->Ga1e = olTime;

	olTime = HourStringToDate( m_AGa2e, omARefDat);
	prmAFlight->Ga2e = olTime;
	

	///////////////////////////////////////////
	// Abflug
	//
	m_CE_DPstd.GetWindowText(m_DPstd);
	m_CE_DGtd1.GetWindowText(m_DGtd1);
	m_CE_DGtd2.GetWindowText(m_DGtd2);
	m_CE_DWro1.GetWindowText(m_DWro1);
	m_CE_DWro2.GetWindowText(m_DWro2);

	m_CE_DPdbs.GetWindowText(m_DPdbs);
	m_CE_DPdes.GetWindowText(m_DPdes);
	m_CE_DGd1b.GetWindowText(m_DGd1b);
	m_CE_DGd2b.GetWindowText(m_DGd2b);
	m_CE_DGd1e.GetWindowText(m_DGd1e);
	m_CE_DGd2e.GetWindowText(m_DGd2e);
	m_CE_DW1bs.GetWindowText(m_DW1bs);
	m_CE_DW1es.GetWindowText(m_DW1es);
	m_CE_DW2bs.GetWindowText(m_DW2bs);
	m_CE_DW2es.GetWindowText(m_DW2es);

	/* 
	m_CE_DTgd1.GetWindowText(m_DTgd1);
	m_CE_DTgd2.GetWindowText(m_DTgd2);
	m_CE_DTwr1.GetWindowText(m_DTwr1);
	*/

	strcpy(prmDFlight->Pstd, m_DPstd);
	strcpy(prmDFlight->Gtd1, m_DGtd1);
	strcpy(prmDFlight->Gtd2, m_DGtd2);
	strcpy(prmDFlight->Wro1, m_DWro1);
	strcpy(prmDFlight->Wro2, m_DWro2);

	/*
	strcpy(prmDFlight->Tgd1, m_DTgd1);
	strcpy(prmDFlight->Tgd2, m_DTgd2);
	strcpy(prmDFlight->Twr1, m_DTwr1);
	*/

	omDRefDat = prmDFlight->Stod;

	olTime = HourStringToDate( m_DPdbs, omDRefDat);
	prmDFlight->Pdbs = olTime;

	olTime = HourStringToDate( m_DPdes, omDRefDat);
	prmDFlight->Pdes = olTime;

	olTime = HourStringToDate( m_DGd1b, omDRefDat);
	prmDFlight->Gd1b = olTime;

	olTime = HourStringToDate( m_DGd2b, omDRefDat);
	prmDFlight->Gd2b = olTime;

	olTime = HourStringToDate( m_DGd1e, omDRefDat);
	prmDFlight->Gd1e = olTime;

	olTime = HourStringToDate( m_DGd2e, omDRefDat);
	prmDFlight->Gd2e = olTime;

	olTime = HourStringToDate( m_DW1bs, omDRefDat);
	prmDFlight->W1bs = olTime;

	olTime = HourStringToDate( m_DW1es, omDRefDat);
	prmDFlight->W1es = olTime;

	olTime = HourStringToDate( m_DW2bs, omDRefDat);
	prmDFlight->W2bs = olTime;

	olTime = HourStringToDate( m_DW2es, omDRefDat);
	prmDFlight->W2es = olTime;



	CCS_CATCH_ALL
}



void CSeasonDlg::ProcessFlightChange(SEASONDLGFLIGHTDATA *prpFlight)
{

	CCS_TRY

	if(prpFlight == NULL)
		return;

	if(imModus != DLG_CHANGE && imModus != DLG_CHANGE_DIADATA)
		return;

	SEASONDLGFLIGHTDATA *prlAFlight;
	SEASONDLGFLIGHTDATA *prlDFlight;
	long llAUrno = 0;
	long llDUrno = 0;

	if(strcmp(prpFlight->Des3, pcgHome) == 0)
	{
		prlAFlight = prpFlight;
		prlDFlight = ogSeasonDlgFlights.GetDeparture(prpFlight);

		llAUrno = prlAFlight->Urno;
		if(prlDFlight != NULL)
			llDUrno = prlDFlight->Urno;
	
		if((llAUrno == prmAFlight->Urno) || ((llDUrno == prmDFlight->Urno) && (llDUrno != 0)))
		{
			*prmAFlight		= *prlAFlight;
			*prmAFlightSave = *prlAFlight;

			if(llDUrno != 0)
			{
				*prmDFlight		= *prlDFlight;
				*prmDFlightSave = *prlDFlight;
			}
			else
			{
				prlDFlight = new SEASONDLGFLIGHTDATA;
				*prmDFlight		= *prlDFlight;
				*prmDFlightSave = *prlDFlight;
				delete prlDFlight;
			}

			InitDialog(true, true);

			ReadCcaData();
			DShowCinsTable();
		}

	}
	else
	{
		prlDFlight = prpFlight;
		prlAFlight = ogSeasonDlgFlights.GetArrival(prpFlight);

		llDUrno = prlDFlight->Urno;
		if(prlAFlight != NULL)
			llAUrno = prlAFlight->Urno;
	
		if((llDUrno == prmDFlight->Urno) || ((llAUrno == prmAFlight->Urno) && (llAUrno != 0)))
		{
			*prmDFlight		= *prlDFlight;
			*prmDFlightSave = *prlDFlight;

			if(llAUrno != 0)
			{
				*prmAFlight		= *prlAFlight;
				*prmAFlightSave = *prlAFlight;
			}
			else
			{
				prlAFlight = new SEASONDLGFLIGHTDATA;
				*prmAFlight		= *prlAFlight;
				*prmAFlightSave = *prlAFlight;
				delete prlAFlight;
			}

			InitDialog(true, true);

			ReadCcaData();
			DShowCinsTable();
		}

	}

	CCS_CATCH_ALL

}




void CSeasonDlg::ProcessFlightDelete(SEASONDLGFLIGHTDATA *prpFlight)
{

	CCS_TRY

	if(prpFlight == NULL)
		return;

	if(imModus != DLG_CHANGE)
		return;

	
	
	SEASONDLGFLIGHTDATA *prlFlight;
	if(prpFlight->Urno == prmAFlight->Urno)
	{
		prlFlight = new SEASONDLGFLIGHTDATA;
		*prmAFlight		= *prlFlight;
		*prmAFlightSave = *prlFlight;
		delete prlFlight;
		InitDialog(true, false);
	}

	if(prpFlight->Urno == prmDFlight->Urno)
	{
		prlFlight = new SEASONDLGFLIGHTDATA;
		*prmDFlight		= *prlFlight;
		*prmDFlightSave = *prlFlight;
		delete prlFlight;
		InitDialog(false, true);
	}

	CCS_CATCH_ALL

}



static void FlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{

	CCS_TRY

    CSeasonDlg *polDlg = (CSeasonDlg *)popInstance;

    if (ipDDXType == S_DLG_FLIGHT_CHANGE)
        polDlg->ProcessFlightChange((SEASONDLGFLIGHTDATA *)vpDataPointer);
	if (ipDDXType == S_DLG_FLIGHT_DELETE)
        polDlg->ProcessFlightDelete((SEASONDLGFLIGHTDATA *)vpDataPointer);
    if(ipDDXType == S_DLG_ROTATION_CHANGE)
        polDlg->ProcessRotationChange();
    if(ipDDXType == S_DLG_ROTATION_SPLIT)
        polDlg->ProcessRotationChange((SEASONDLGFLIGHTDATA *)vpDataPointer);


	if (ipDDXType == CCA_SEADLG_CHANGE || ipDDXType == CCA_SEADLG_NEW || ipDDXType == CCA_SEADLG_DELETE)
        polDlg->ProcessCCA((CCADATA *)vpDataPointer, ipDDXType);

	CCS_CATCH_ALL

}






///////////////////////////////////////////////////////////////////////////
// n�chster Flug von der Flugdatenansichtentabelle laden 
//
void CSeasonDlg::OnNextflight()
{
	if(bmChanged)
	{
		if (!CheckPostFlight())
		{
			if(CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING942), GetString(ST_FRAGE), MB_ICONEXCLAMATION | MB_YESNO) == IDNO)
				return;
		}
	}

	
	CCS_TRY

	NEW_SEASONDLG_DATA rlData;
	rlData.Parent = pomParent;
	rlData.CurrUrno = lmCalledBy;
	ogDdx.DataChanged((void *)this, S_DLG_GET_NEXT,(void *)&rlData );
	m_CB_Ok.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));

	CCS_CATCH_ALL

}


///////////////////////////////////////////////////////////////////////////
// vorheriger Flug von der Flugdatenansichtentabelle laden 
//
void CSeasonDlg::OnPrevflight()
{
	if(bmChanged)
	{
		if (!CheckPostFlight())
		{
			if(CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING942), GetString(ST_FRAGE), MB_ICONEXCLAMATION | MB_YESNO) == IDNO)
				return;
		}
	}

	CCS_TRY

	NEW_SEASONDLG_DATA rlData;

	rlData.Parent = pomParent;
	rlData.CurrUrno = lmCalledBy;

	ogDdx.DataChanged((void *)this, S_DLG_GET_PREV,(void *)&rlData );
	m_CB_Ok.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));

	CCS_CATCH_ALL

}


void CSeasonDlg::OnAgent() 
{
	if(prmAFlight->Urno > 0 || prmDFlight->Urno > 0)
	{
		//alc3 der Fluggesellschaften
		CString opAlc3StrArr ("");
		CString opAlc3StrDep ("");

		if(prmAFlight != NULL)
			opAlc3StrArr.Format("%s", prmAFlight->Alc3);
		if(prmDFlight != NULL)
			opAlc3StrDep.Format("%s", prmDFlight->Alc3);

		if (opAlc3StrArr.GetLength() == 0 && opAlc3StrDep.GetLength() == 0)
			return;

		RotationHAIDlg polRotationHAIDlg(this, opAlc3StrArr, opAlc3StrDep);
		if (polRotationHAIDlg.DoModal() == IDOK)
		{
		}
	}
}



///////////////////////////////////////////////////////////////////////////
// Cancel
//
void CSeasonDlg::OnCancel() 
{

	CCS_TRY

	if(bmChanged)
	{
		if (!CheckPostFlight())
		{
			if(CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING942), GetString(ST_FRAGE), MB_ICONEXCLAMATION | MB_YESNO) == IDNO)
				return;
		}
	}

	bmChanged = false;
	m_CB_Ok.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));

	ogSeasonDlgFlights.ClearAll();
	ClearAll();
	ShowWindow(SW_HIDE);

	CCS_CATCH_ALL
}

///////////////////////////////////////////////////////////////////////////
// OK
//
void CSeasonDlg::OnOK() 
{

	CCS_TRY

	CString olGMessage;
	CString olAMessage;
	CString olDMessage;

	if(!bmChanged)
		return;

	if (imModus == DLG_CHANGE_DIADATA)
		FillGatPosFlightData();
	else
		FillFlightData();


	if(!CheckAll(olGMessage, olAMessage, olDMessage))
	{
		if(olGMessage.IsEmpty() && olAMessage.IsEmpty() && olDMessage.IsEmpty())
			return;

		if(!olGMessage.IsEmpty())
			olGMessage = GetString(IDS_STRING972) + olGMessage; 

		if(!olAMessage.IsEmpty())
			olAMessage = GetString(IDS_STRING973) + olAMessage; 
		
		if(!olDMessage.IsEmpty())
			olDMessage = GetString(IDS_STRING974) + olDMessage; 
		
		CFPMSApp::MyTopmostMessageBox(this,olGMessage + olAMessage + olDMessage, GetString(ST_FEHLER), MB_ICONWARNING);
		return;
	}


	// additional belt check
	if (!AddBltCheck()) return;


	// Arrival Flight ID changed?
	if (strcmp(prmAFlight->Flti, prmAFlightSave->Flti) != 0)
	{
		m_AFlti.TrimRight();
		CString olMess;
 		if (m_AFlti.IsEmpty())
		{
			olMess.Format(GetString(IDS_STRING1994), prmAFlight->Flno);
		}
		else
		{
			olMess.Format(GetString(IDS_STRING1993), prmAFlight->Flno);
		}
		if (CFPMSApp::MyTopmostMessageBox(this,olMess, GetString(ST_FRAGE), MB_ICONQUESTION | MB_YESNO) != IDYES)
			return;
	}

	// Departure Flight ID changed?
	if (strcmp(prmDFlight->Flti, prmDFlightSave->Flti) != 0)
	{
		m_DFlti.TrimRight();
		CString olMess;
 		if (m_DFlti.IsEmpty())
		{
			olMess.Format(GetString(IDS_STRING1994), prmDFlight->Flno);
		}
		else
		{
			olMess.Format(GetString(IDS_STRING1993), prmDFlight->Flno);
		}
		if (CFPMSApp::MyTopmostMessageBox(this,olMess, GetString(ST_FRAGE), MB_ICONQUESTION | MB_YESNO) != IDYES)
			return;
	}

	if (imModus == DLG_COPY )
	{
		if (!m_CE_DFlti.IsChanged() && !bmDFltiUserSet)
		{
			prmDFlight->Flti[0]='\0';
		}
		if (!m_CE_AFlti.IsChanged() && !bmAFltiUserSet)
		{
			prmAFlight->Flti[0]='\0';
		}
	}

	m_CB_Ok.SetColors(RGB(0,255,0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_Ok.UpdateWindow();


	if(bmLocalTime) 
	{
		ogSeasonDlgFlights.StructLocalToUtc((void*)prmDFlight);
		ogSeasonDlgFlights.StructLocalToUtc((void*)prmAFlight);
	}

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));



	if(imModus == DLG_CHANGE_DIADATA)
	{
		SEASONDLGFLIGHTDATA rlDFlightBak;
		SEASONDLGFLIGHTDATA rlDFlightSaveBak;
		if(prmDFlight->Urno != 0)
		{
			// copy the departure record is necessary because the update of the arrival
			// record can change the departure records!! The reason: The DB is not really 
			// changed, because the app is offline, so the changes will be transmitted
			// directly to the data class and this dialog!
			rlDFlightBak = *prmDFlight;
			rlDFlightSaveBak = *prmDFlightSave;
		}
		
		if(	prmAFlight->Urno != 0)
		{
			ogSeasonDlgFlights.UpdateFlight(prmAFlight, prmAFlightSave, DIADATA);
		}
	
		if(prmDFlight->Urno != 0)
		{
			ogSeasonDlgFlights.UpdateFlight(&rlDFlightBak, &rlDFlightSaveBak, DIADATA);
		}
	}

	if(imModus == DLG_CHANGE)
	{
		if(	prmAFlight->Urno != 0)
		{
			ogSeasonDlgFlights.UpdateFlight(prmAFlight, prmAFlightSave);
		}
	
		if(prmDFlight->Urno != 0)
		{

			omCcaData.UpdateSpecial(omDCins, omDCinsSave, prmDFlight->Urno);
				

			//ACHTUNG MWO das ist nur eine Zwischenl�sung
			/*
			for (int ilLc = 0; ilLc < omDCins.GetSize(); ilLc++)
			{
				if(bmLocalTime)
				{
					omCcaData.StructUtcToLocal(&omDCins[ilLc]);
				}
			}
			*/
			bmChanged = false;
			//ENDE ACHTUNG MWO das ist nur eine Zwischenl�sung

			ogSeasonDlgFlights.UpdateFlight(prmDFlight, prmDFlightSave);

//##### PRF 2055 NUR POPS44
			// if both legs cxx or noop -> don�t split the rotation -> join again !
			if(	prmAFlight && prmDFlight && prmAFlight->Urno != 0 && prmDFlight->Urno != 0 )
			{
				if(		(strcmp(prmDFlight->Ftyp, "X") == 0 || strcmp(prmDFlight->Ftyp, "N") == 0)
					&&  (strcmp(prmAFlight->Ftyp, "X") == 0 || strcmp(prmAFlight->Ftyp, "N") == 0) )
				{
					SEASONDLGFLIGHTDATA* prlAFlight = NULL;
					SEASONDLGFLIGHTDATA* prlDFlight = NULL;

					char pclSelectionArr[256];
					sprintf(pclSelectionArr, " URNO = %ld", prmAFlight->Urno);

					ogSeasonDlgFlights.ReadFlights(pclSelectionArr);
					prlAFlight = ogSeasonDlgFlights.GetFlightByUrno(prmAFlight->Urno);

					char pclSelectionDep[256];
					sprintf(pclSelectionDep, " URNO = %ld", prmDFlight->Urno);

					ogSeasonDlgFlights.ReadFlights(pclSelectionDep);
					prlDFlight = ogSeasonDlgFlights.GetFlightByUrno(prmDFlight->Urno);

					if (prlAFlight && prlDFlight)
					{
						CedaFlightUtilsData olFlightUtilsData;
						olFlightUtilsData.JoinFlight(prlAFlight->Urno, prlAFlight->Rkey, prlDFlight->Urno, prlDFlight->Rkey, true);
					}
				}
			}
//##### PRF 2055 NUR POPS44



		}
	}
	if((imModus == DLG_NEW) || (imModus == DLG_COPY))
	{
		CString olText;		

		if(bmAnkunft && bmAbflug)
		{
			olText = ogSeasonDlgFlights.CheckDupFlights(DateStringToDate(m_Pvom), DateStringToDate(m_Pbis), m_ADays, m_DDays, m_Freq, omARefDat, omDRefDat, prmAFlight, prmDFlight, omDCins);
		}
		else
		{
			if(bmAnkunft)
			{
				olText = ogSeasonDlgFlights.CheckDupFlights(DateStringToDate(m_Pvom), DateStringToDate(m_Pbis), m_ADays, m_DDays, m_Freq, omARefDat, omDRefDat, prmAFlight, NULL, omDCins);
			}
			else
			{
				olText = ogSeasonDlgFlights.CheckDupFlights(DateStringToDate(m_Pvom), DateStringToDate(m_Pbis), m_ADays, m_DDays, m_Freq, omARefDat, omDRefDat, NULL, prmDFlight, omDCins);
			}
		}


		if( !olText.IsEmpty())
		{

			if (olText == GetString(IDS_STRING_INVALID_DATE))
			{
				CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING_INVALID_DATE_PROMPT), GetString(IDS_WARNING), MB_OK);
				return;
			}

			olText += GetString(IDS_STRING1698);


			if(CFPMSApp::MyTopmostMessageBox(this,olText, GetString(IDS_WARNING), MB_YESNO) != IDYES)
				return;
		}

		CString olTage;
		if(bmAnkunft && bmAbflug)
		{

 			ogSeasonDlgFlights.AddSeasonFlights(DateStringToDate(m_Pvom), DateStringToDate(m_Pbis), m_ADays, m_DDays, m_Freq, omARefDat, omDRefDat, prmAFlight, prmDFlight, omDCins);
		}
		else
		{
			if(bmAnkunft)
			{
				ogSeasonDlgFlights.AddSeasonFlights(DateStringToDate(m_Pvom), DateStringToDate(m_Pbis), m_ADays, m_DDays, m_Freq, omARefDat, omDRefDat, prmAFlight, NULL, omDCins);
			}
			else
			{
				ogSeasonDlgFlights.AddSeasonFlights(DateStringToDate(m_Pvom), DateStringToDate(m_Pbis), m_ADays, m_DDays, m_Freq, omARefDat, omDRefDat, NULL, prmDFlight, omDCins);
			}
		}
		bmChanged = false;
	}

//MWO
	omOldCcaValues.DeleteAll();
	for(int ili = 0; ili < omOldDCins.GetSize(); ili++)
	{
		omOldCcaValues.NewAt(omOldCcaValues.GetSize(), omOldDCins[ili]);
	}

//END MWO
	

	if(bmLocalTime && imModus != DLG_CHANGE_DIADATA)
	{
		// In DLG_CHANGE_DIADATA mode the new records are already in the local data
		// structures and already changed to local times if necessary
		ogSeasonDlgFlights.StructUtcToLocal((void*)prmAFlight);
		ogSeasonDlgFlights.StructUtcToLocal((void*)prmDFlight);
	}
	
	bmChanged = false;
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	m_CB_Ok.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));

	CCS_CATCH_ALL

}










void CSeasonDlg::OnAreset() 
{

	CCS_TRY

	CString olPvom; 
	m_CE_Pvom.GetWindowText(olPvom);

	SEASONDLGFLIGHTDATA *prlFlight = new SEASONDLGFLIGHTDATA;	
	*prmAFlight = *prlFlight;
	*prmAFlightSave = *prlFlight;
	delete prlFlight;
	m_ADays = "";
	omAJfno.DeleteAll();
	InitDialog(true,false);
	m_CE_Pvom.SetInitText(olPvom);

	CCS_CATCH_ALL

}


void CSeasonDlg::OnDreset() 
{

	CCS_TRY

	CString olPbis; 
	m_CE_Pbis.GetWindowText(olPbis);

	SEASONDLGFLIGHTDATA *prlFlight = new SEASONDLGFLIGHTDATA;	
	*prmDFlight = *prlFlight;
	*prmDFlightSave = *prlFlight;
	m_DDays = "";
	delete prlFlight;
	omDJfno.DeleteAll();
	InitDialog(false, true);	
	m_CE_Pbis.SetInitText(olPbis);

	CCS_CATCH_ALL

}


void CSeasonDlg::OnDailyMask() 
{
 	pogRotationDlg->NewData(pomParent, lmRkey, lmCalledBy, omAdid, bmLocalTime);
}



void CSeasonDlg::OnDefault() 
{

	CCS_TRY

	Settestdefault();	

	CCS_CATCH_ALL

}


///////////////////////////////////////////////////////////////////////////
// testdefault
//
void CSeasonDlg::Settestdefault() 
{

	CCS_TRY

	omAJfno.DeleteAll();
	omDJfno.DeleteAll();
	m_ADays = "";
	m_DDays = "";



	SEASONDLGFLIGHTDATA *prlFlight = new SEASONDLGFLIGHTDATA;	

	*prmAFlight = *prlFlight;
	*prmAFlightSave = *prlFlight;

	*prmDFlight = *prlFlight;
	*prmDFlightSave = *prlFlight;


	delete prlFlight;

	InitDialog(true, true);	

	char buffer[32];

	CTimeSpan olSpan(0,0,1,0);


	omOpenTime += olSpan;
	m_CE_AStoa.SetInitText(omOpenTime.Format("%H%M"));

	CTime olCurrTime = omOpenTime + olSpan + olSpan + olSpan + olSpan + olSpan;
	m_CE_DStod.SetInitText(olCurrTime.Format("%H%M"));


	m_CE_Act3.SetInitText("747");
	m_CE_Ming.SetInitText("37");
	m_CE_Pvom.SetInitText(omOpenTime.Format("%d%m%Y"));
	m_CE_Pbis.SetInitText(omOpenTime.Format("%d%m%Y"));
	m_CE_Freq.SetInitText("1");

	m_CE_AOrg3.SetInitText("INN");
	m_CE_AAlc3.SetInitText("DLH");
	m_CE_AFltn.SetInitText(omOpenTime.Format("%H%M"));
	m_CE_AFlns.SetInitText("X");
	m_CE_DFlns.SetInitText("Y");
	m_CE_DDes3.SetInitText("ABZ");
	m_CE_DAlc3.SetInitText("DLH");
	m_CE_DFltn.SetInitText(olCurrTime.Format("%H%M"));

	itoa(GetDayOfWeek(omOpenTime), buffer, 10);
	m_CE_ADays.SetInitText(CString(buffer));
	m_CE_DDays.SetInitText(CString(buffer));


	omAAlc3 = "DLH";
	omAAlc2 = "LH";

	omDAlc3 = "DLH";
	omDAlc2 = "LH";
	bmAnkunft = true;
	bmAbflug = true;


	//m_CE_AEtai.SetInitText("2000");
	//m_CE_AStod.SetInitText("1800");

	//m_CE_DEtdi.SetInitText("2200");
	//m_CE_DStoa.SetInitText("2300");




/*
	m_CE_ATtyp.SetInitText("V");
	m_CE_AHtyp.SetInitText("HC");
	m_CE_AStyp.SetInitText("S");
	m_CE_AStev.SetInitText("K");
	m_CE_APsta.SetInitText("POSI2");
	m_CE_AGta1.SetInitText("GATE1");
	m_CE_AGta2.SetInitText("GATE2");
	m_CE_DFlns.SetInitText("X");
	m_CE_DTtyp.SetInitText("V");
	m_CE_DHtyp.SetInitText("HC");
	m_CE_DStyp.SetInitText("S");
	m_CE_DStev.SetInitText("K");
	m_CE_DPstd.SetInitText("POSI1");
	m_CE_DGta1.SetInitText("GATE1");
	m_CE_DGta2.SetInitText("GATE2");

*/

	CCS_CATCH_ALL

}








LONG CSeasonDlg::OnEditKillfocus( UINT wParam, LPARAM lParam)
{

	CCS_TRY

	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY*)lParam;

	CString olTmp;
	CString olTmp1;

/*
//###########
	if(((UINT)m_CE_DStod.imID == wParam) && (!prlNotify->Text.IsEmpty()))
	{
					CString olStrStod;
					m_CE_DStod.GetWindowText(olStrStod);
					if (!olStrStod.IsEmpty())
					{
						CTime olStod = HourStringToDate( olStrStod, omDRefDat);
						olStrStod = olStod.Format("%H:%M  %d.%m.%Y");
						if(bmLocalTime)
						{
							ogBasicData.LocalToUtc(olStod);
						}
						olStrStod = olStod.Format("%H:%M  %d.%m.%Y");
						int z = 0;

						
					}
	}


//###########
*/

	CString olRegn;
	m_CE_Regn.GetWindowText(olRegn);

	if( !olRegn.IsEmpty() && ( ((UINT)m_CE_Act5.imID == wParam) || ((UINT)m_CE_Act3.imID == wParam) ) && prlNotify->Status)
	{
		bool blRet = false;
		CString olAct3;
		CString olAct5;

		CString olWhere;
		olRegn.TrimLeft();
		if(!olRegn.IsEmpty())
		{
			olWhere.Format("WHERE REGN = '%s'", olRegn);
			ogBCD.Read( "ACR", olWhere);
		}
		
		
		if(ogBCD.GetFields("ACR", "REGN", olRegn, "ACT3", "ACT5", olAct3, olAct5))
		{
			if((prlNotify->Text != olAct3) && (prlNotify->Text != olAct5))
			{
				if(MessageBox(GetString(IDS_STRING945), GetString(ST_FRAGE), MB_YESNO) == IDYES)				
				{
					m_CE_Act3.GetWindowText(olAct3);
					m_CE_Act5.GetWindowText(olAct5);

					ogBCD.GetField("ACT", "ACT3","ACT5", prlNotify->Text, olAct3, olAct5);

					ogBCD.SetField("ACR", "REGN", olRegn, "ACT3", olAct3, false);
					ogBCD.SetField("ACR", "REGN", olRegn, "ACT5", olAct5, false);
					ogBCD.Save("ACR");
				}
				else
				{
					m_CE_Act3.SetInitText( olAct3, true);
					m_CE_Act5.SetInitText( olAct5, true);
				}
			}
		}
	}


	if(((UINT)m_CE_Regn.imID == wParam) && (!prlNotify->Text.IsEmpty()))
	{
		CString olAct3;
		CString olAct5;
		prlNotify->Status = true;

		
		CString olWhere;
		prlNotify->Text.TrimLeft();
		if(!prlNotify->Text.IsEmpty())
		{
			olWhere.Format("WHERE REGN = '%s'", prlNotify->Text);
			ogBCD.Read( "ACR", olWhere);
		}
		
		if(prlNotify->Status = ogBCD.GetFields("ACR", "REGN", prlNotify->Text, "ACT3", "ACT5", olAct3, olAct5))
		{
			m_CE_Act3.SetInitText(olAct3, true);
			m_CE_Act5.SetInitText(olAct5, true);
		}
		else
		{
			m_CE_Act3.GetWindowText(olAct3);
			m_CE_Act5.GetWindowText(olAct5);

			RotationRegnDlg olDlg(this, prlNotify->Text, olAct3, olAct5);
			if(olDlg.DoModal() == IDOK)
			{
				m_CE_Act5.SetInitText(olDlg.m_Act5, true);
				m_CE_Act3.SetInitText(olDlg.m_Act3, true);
				prlNotify->Status = true;
			}
			else
				m_CE_Regn.SetInitText("", true);

		}
		return 0L;
	}




	if((UINT)m_CE_ADays.imID == wParam)
	{
		CString olVomStr;
		CString olBisStr;
		m_CE_Pvom.GetWindowText(olVomStr);
		m_CE_Pbis.GetWindowText(olBisStr);
		CTime olVom = DateHourStringToDate(olVomStr, CString("0900")); 
		CTime olBis = DateHourStringToDate(olBisStr, CString("1500")); 
		m_CE_ADays.GetWindowText(m_ADays);;
		if((olVom != TIMENULL) && (olBis != TIMENULL))
		{
			if(!DaysinPeriod(olVom,  olBis,  m_ADays))
			{
				prlNotify->UserStatus = true;
				prlNotify->Status = false;
			}	
		}

	}
	if((UINT)m_CE_DDays.imID == wParam)
	{
		CString olVomStr;
		CString olBisStr;
		m_CE_Pvom.GetWindowText(olVomStr);
		m_CE_Pbis.GetWindowText(olBisStr);
		CTime olVom = DateHourStringToDate(olVomStr, CString("0900")); 
		CTime olBis = DateHourStringToDate(olBisStr, CString("1500")); 
		m_CE_DDays.GetWindowText(m_DDays);;
		if((olVom != TIMENULL) && (olBis != TIMENULL))
		{
			if(!DaysinPeriod(olVom,  olBis,  m_DDays))
			{
				prlNotify->UserStatus = true;
				prlNotify->Status = false;
			}	
		}
	}


	if((UINT)m_CE_AOrg3.imID == wParam)
	{
		if(strcmp(prlNotify->Text , pcgHome) == 0)
		{
			m_CE_AStod.SetBKColor(YELLOW);
			m_CE_AStod.SetTypeToTime(true, true);
			m_CE_AStod.SetTextLimit(3,3,false);
			m_CS_Ankunft.SetWindowText(GetString(IDS_STRING1388));
		}
		else
		{
			m_CE_AStod.SetBKColor(WHITE);
			m_CE_AStod.SetTypeToTime(false, true);
			m_CE_AStod.SetTextLimit(0,3,true);
			m_CS_Ankunft.SetWindowText(GetString(IDS_STRING1389));
		}
	}

	if((UINT)m_CE_DDes3.imID == wParam)
	{
		if(strcmp(prlNotify->Text , pcgHome) == 0)
		{
			m_CE_DStoa.SetBKColor(YELLOW);
			m_CE_DStoa.SetTypeToTime(true, true);
			m_CE_DStoa.SetTextLimit(3,3,false);
			m_CS_Abflug.SetWindowText(GetString(IDS_STRING1388));
		}
		else
		{
			m_CE_DStoa.SetBKColor(WHITE);
			m_CE_DStoa.SetTypeToTime(false, true);
			m_CE_DStoa.SetTextLimit(0,3,true);
			m_CS_Abflug.SetWindowText(GetString(IDS_STRING1390));
		}
	}


	if(((UINT)m_CE_AOrg3.imID == wParam) || ((UINT)m_CE_DDes3.imID == wParam))
	{
		prlNotify->UserStatus = true;
		prlNotify->Status = ogBCD.GetField("APT", "APC3", prlNotify->Text, "APC4", olTmp );
		if((UINT)m_CE_AOrg3.imID == wParam)
		{
			m_CE_AOrg4.SetInitText(olTmp, true);
		}
		else
		{
			m_CE_DDes4.SetInitText(olTmp, true);
		}
		return 0L;
	}


	if(((UINT)m_CE_AOrg4.imID == wParam) || ((UINT)m_CE_DDes4.imID == wParam))
	{
		prlNotify->UserStatus = true;
		prlNotify->Status = ogBCD.GetField("APT", "APC4", prlNotify->Text, "APC3", olTmp );
		if((UINT)m_CE_AOrg4.imID == wParam)
		{
			m_CE_AOrg3.SetInitText(olTmp, true);
		}
		else
		{
			m_CE_DDes3.SetInitText(olTmp, true);
		}
		return 0L;
	}



	if((UINT)m_CE_DAlc3.imID == wParam)
	{
		omDAlc2 = "";
		omDAlc3 = "";
		prlNotify->UserStatus = true;
		prlNotify->Status = ogBCD.GetField("ALT", "ALC2", "ALC3", prlNotify->Text, omDAlc2, omDAlc3 );
		return 0L;
	}
	if((UINT)m_CE_AAlc3.imID == wParam)
	{
		omAAlc2 = "";
		omAAlc3 = "";
		prlNotify->UserStatus = true;
		prlNotify->Status = ogBCD.GetField("ALT", "ALC2", "ALC3", prlNotify->Text, omAAlc2, omAAlc3 );
		return 0L;
	}



	if(((UINT)m_CE_Act3.imID == wParam) && (!prlNotify->Text.IsEmpty()))
	{
		prlNotify->UserStatus = true;
		prlNotify->Status = false;
		if(prlNotify->Status = ogBCD.GetField("ACT", "ACT3", prlNotify->Text, "ACT5", olTmp ))
		{
			m_CE_Act5.SetInitText(olTmp);
		}
		return 0L;
	}

	if((UINT)m_CE_Act5.imID == wParam)
	{
		prlNotify->UserStatus = true;
		prlNotify->Status = false;
		if(prlNotify->Status = ogBCD.GetField("ACT", "ACT5", prlNotify->Text, "ACT3", olTmp ))
		{
			m_CE_Act3.SetInitText(olTmp);
		}
		return 0L;
	}




	if( (UINT)m_CE_AGta1.imID == wParam)
	{
		if(prlNotify->Text.IsEmpty())
		{
			if((UINT)m_CE_AGta1.imID == wParam)
				m_CE_ATga1.SetInitText("",true);
		}
		else
		{
			prlNotify->UserStatus = true;
			if(prlNotify->Status = ogBCD.GetField("GAT", "GNAM", prlNotify->Text, "TERM", olTmp ))
			{
				if((UINT)m_CE_AGta1.imID == wParam)
					m_CE_ATga1.SetInitText(olTmp,true);

				m_CE_AGa1b.GetWindowText(olTmp);
				m_CE_AGa1e.GetWindowText(olTmp1);

				if (prlNotify->IsChanged || olTmp.IsEmpty() || olTmp1.IsEmpty())
				{
					m_CE_AGa1b.SetWindowText(DateToHourDivString(prmAFlight->Tifa, prmAFlight->Tifa));
		
					CTimeSpan olDura;
					GetGatDefAllocDur(prlNotify->Text, olDura);
					
					m_CE_AGa1e.SetWindowText(DateToHourDivString(prmAFlight->Tifa + olDura, prmAFlight->Tifa));
				}
			}
		}
		return 0L;
	}



	if( (UINT)m_CE_AGta2.imID == wParam)
	{
		if(prlNotify->Text.IsEmpty())
		{
			if((UINT)m_CE_AGta2.imID == wParam)
				m_CE_ATga2.SetInitText("",true);
		}
		else
		{
			prlNotify->UserStatus = true;
			if(prlNotify->Status = ogBCD.GetField("GAT", "GNAM", prlNotify->Text, "TERM", olTmp ))
			{
				if((UINT)m_CE_AGta2.imID == wParam)
					m_CE_ATga2.SetInitText(olTmp,true);

				m_CE_AGa2b.GetWindowText(olTmp);
				m_CE_AGa2e.GetWindowText(olTmp1);

				if (prlNotify->IsChanged || olTmp.IsEmpty() || olTmp1.IsEmpty())
				{
					m_CE_AGa2b.SetWindowText(DateToHourDivString(prmAFlight->Tifa, prmAFlight->Tifa));

					CTimeSpan olDura;
					GetGatDefAllocDur(prlNotify->Text, olDura);
					
					m_CE_AGa2e.SetWindowText(DateToHourDivString(prmAFlight->Tifa + olDura, prmAFlight->Tifa));
				}
			}
		}
		return 0L;
	}



	if( (UINT)m_CE_DGtd1.imID == wParam)
	{
		if(prlNotify->Text.IsEmpty())
		{
			if((UINT)m_CE_DGtd1.imID == wParam)
				m_CE_DTgd1.SetInitText("",true);
		}
		else
		{
			prlNotify->UserStatus = true;
			if(prlNotify->Status = ogBCD.GetField("GAT", "GNAM", prlNotify->Text, "TERM", olTmp ))
			{
				if((UINT)m_CE_DGtd1.imID == wParam)
					m_CE_DTgd1.SetInitText(olTmp,true);

				m_CE_DGd1e.GetWindowText(olTmp);
				m_CE_DGd1b.GetWindowText(olTmp1);

				if (prlNotify->IsChanged || olTmp.IsEmpty() || olTmp1.IsEmpty())
				{
					m_CE_DGd1e.SetWindowText(DateToHourDivString(prmDFlight->Tifd, prmDFlight->Tifd));

					CTimeSpan olDura;
					GetGatDefAllocDur(prlNotify->Text, olDura);
					m_CE_DGd1b.SetWindowText(DateToHourDivString(prmDFlight->Tifd - olDura, prmDFlight->Tifd));
				}
			}
		}
		return 0L;
	}



	if( (UINT)m_CE_DGtd2.imID == wParam)
	{
		if(prlNotify->Text.IsEmpty())
		{
			if((UINT)m_CE_DGtd2.imID == wParam)
				m_CE_DTgd2.SetInitText("",true);
		}
		else
		{
			prlNotify->UserStatus = true;
			if(prlNotify->Status = ogBCD.GetField("GAT", "GNAM", prlNotify->Text, "TERM", olTmp ))
			{
				if((UINT)m_CE_DGtd2.imID == wParam)
					m_CE_DTgd2.SetInitText(olTmp,true);

				m_CE_DGd2b.GetWindowText(olTmp);
				m_CE_DGd2e.GetWindowText(olTmp1);

				if (prlNotify->IsChanged || olTmp.IsEmpty() || olTmp1.IsEmpty())
				{
					m_CE_DGd2e.SetWindowText(DateToHourDivString(prmDFlight->Tifd, prmDFlight->Tifd));

					CTimeSpan olDura;
					GetGatDefAllocDur(prlNotify->Text, olDura);			
					m_CE_DGd2b.SetWindowText(DateToHourDivString(prmDFlight->Tifd - olDura, prmDFlight->Tifd));
				}
			}
		}
		return 0L;
	}




	if( (UINT)m_CE_ABlt1.imID == wParam)
	{
		if(prlNotify->Text.IsEmpty())
		{
				m_CE_ATmb1.SetInitText("",true);
		}
		else
		{
			prlNotify->UserStatus = true;
			if(prlNotify->Status = ogBCD.GetField("BLT", "BNAM", prlNotify->Text, "TERM", olTmp ))
			{
				m_CE_ATmb1.SetInitText(olTmp,true);

				m_CE_AB1bs.GetWindowText(olTmp);
				m_CE_AB1es.GetWindowText(olTmp1);

				if (prlNotify->IsChanged || olTmp.IsEmpty() || olTmp1.IsEmpty())
				{
					m_CE_AB1bs.SetWindowText(DateToHourDivString(prmAFlight->Tifa, prmAFlight->Tifa));
				
					CTimeSpan olDura;
					GetBltDefAllocDur(prlNotify->Text, olDura);
					m_CE_AB1es.SetWindowText(DateToHourDivString(prmAFlight->Tifa + olDura, prmAFlight->Tifa));
				}
			}
		}
		return 0L;
	}


	if( (UINT)m_CE_ABlt2.imID == wParam)
	{
		if(prlNotify->Text.IsEmpty())
		{
				m_CE_ATmb2.SetInitText("",true);
		}
		else
		{
			prlNotify->UserStatus = true;
			if(prlNotify->Status = ogBCD.GetField("BLT", "BNAM", prlNotify->Text, "TERM", olTmp ))
			{
				m_CE_ATmb2.SetInitText(olTmp,true);

				m_CE_AB2bs.GetWindowText(olTmp);
				m_CE_AB2es.GetWindowText(olTmp1);

				if (prlNotify->IsChanged || olTmp.IsEmpty() || olTmp1.IsEmpty())
				{
					m_CE_AB2bs.SetWindowText(DateToHourDivString(prmAFlight->Tifa, prmAFlight->Tifa));

					CTimeSpan olDura;
					GetBltDefAllocDur(prlNotify->Text, olDura);
					m_CE_AB2es.SetWindowText(DateToHourDivString(prmAFlight->Tifa + olDura, prmAFlight->Tifa));
				}
			}
		}
		return 0L;
	}


	if(((UINT)m_CE_AExt1.imID == wParam) || ((UINT)m_CE_AExt2.imID == wParam))
	{
		if(prlNotify->Text.IsEmpty())
		{
			if((UINT)m_CE_AExt1.imID == wParam)
				m_CE_ATet1.SetInitText("", true);
			if((UINT)m_CE_AExt2.imID == wParam)
				m_CE_ATet2.SetInitText("", true);
		}
		else
		{
			prlNotify->UserStatus = true;
			if(prlNotify->Status = ogBCD.GetField("EXT", "ENAM", prlNotify->Text, "TERM", olTmp ))
			{
				if((UINT)m_CE_AExt1.imID == wParam)
					m_CE_ATet1.SetInitText(olTmp, true);
				if((UINT)m_CE_AExt2.imID == wParam)
					m_CE_ATet2.SetInitText(olTmp, true);
			}
		}
		return 0L;
	}

	if((UINT)m_CE_DWro1.imID == wParam)
	{
		if(prlNotify->Text.IsEmpty())
		{
			m_CE_DTwr1.SetInitText("", true);
		}
		else
		{
			prlNotify->UserStatus = true;
	
			
			if(prlNotify->Status = ogBCD.GetField("WRO", "WNAM", prlNotify->Text, "TERM", olTmp ))
			{
				m_CE_DTwr1.SetInitText(olTmp, true);
			
				m_CE_DW1es.GetWindowText(olTmp);
				m_CE_DW1bs.GetWindowText(olTmp1);

				if (prlNotify->IsChanged || olTmp.IsEmpty() || olTmp1.IsEmpty())
				{
					m_CE_DW1es.SetWindowText(DateToHourDivString(prmDFlight->Tifd, prmDFlight->Tifd));
					
					CTimeSpan olDura;
					GetWroDefAllocDur(prlNotify->Text, olDura);
					m_CE_DW1bs.SetWindowText(DateToHourDivString(prmDFlight->Tifd - olDura, prmDFlight->Tifd));	
				}
			}
		}
		return 0L;
	}

	if((UINT)m_CE_DWro2.imID == wParam)
	{
		if(prlNotify->Text.IsEmpty())
		{
			m_CE_DTwr2.SetInitText("", true);
		}
		else
		{
			prlNotify->UserStatus = true;
	
			
			if(prlNotify->Status = ogBCD.GetField("WRO", "WNAM", prlNotify->Text, "TERM", olTmp ))
			{
				m_CE_DTwr2.SetInitText(olTmp, true);
			
				m_CE_DW2es.GetWindowText(olTmp);
				m_CE_DW2bs.GetWindowText(olTmp1);

				if (prlNotify->IsChanged || olTmp.IsEmpty() || olTmp1.IsEmpty())
				{
					m_CE_DW2es.SetWindowText(DateToHourDivString(prmDFlight->Tifd, prmDFlight->Tifd));
					
					CTimeSpan olDura;
					GetWroDefAllocDur(prlNotify->Text, olDura);
					m_CE_DW2bs.SetWindowText(DateToHourDivString(prmDFlight->Tifd - olDura, prmDFlight->Tifd));	
				}
			}
		}
		return 0L;
	}

	CTime olVom = TIMENULL;
	CTime olBis = TIMENULL;
	CString olASeas;
	CString olDSeas;

	if((UINT)m_CE_Pvom.imID == wParam)
	{
		m_CE_Pbis.GetWindowText(m_Pbis);
		olVom = DateStringToDate(prlNotify->Text); 
		if(!m_Pbis.IsEmpty() && !prlNotify->Text.IsEmpty())
		{
			olBis = DateStringToDate(m_Pbis);
			if((olVom != TIMENULL) && (olBis != TIMENULL))
			{
				if(olVom > olBis)
				{
					prlNotify->UserStatus = true;
					prlNotify->Status = false;
					m_CE_Pbis.SetStatus(false);
				}
				else
				{
					prlNotify->UserStatus = true;
					prlNotify->Status = true;
					m_CE_Pbis.SetStatus(true);
				}
			}
		}
		if((olVom != TIMENULL) && !CheckUtcPast(olVom))
		{
			prlNotify->UserStatus = true;
			prlNotify->Status = false;
		}
	}


	if((UINT)m_CE_Pbis.imID == wParam)
	{
		m_CE_Pvom.GetWindowText(m_Pvom);
		olBis = DateStringToDate(prlNotify->Text); 
		if(!m_Pvom.IsEmpty() && !prlNotify->Text.IsEmpty())
		{
			olVom = DateStringToDate(m_Pvom);
			if((olVom != TIMENULL) && (olBis != TIMENULL))
			{
				if(olVom > olBis)
				{
					prlNotify->UserStatus = true;
					prlNotify->Status = false;
					m_CE_Pvom.SetStatus(false);
				}
				else
				{
					m_CE_Pvom.SetStatus(true);
					prlNotify->UserStatus = true;
					prlNotify->Status = true;
				}
			}
		}
		if((olBis != TIMENULL) && !CheckUtcPast(olBis))
		{
			prlNotify->UserStatus = true;
			prlNotify->Status = false;
		}
	}

	if(((UINT)m_CE_Pbis.imID == wParam) || ((UINT)m_CE_Pvom.imID == wParam))
	{
		m_CE_Pvom.GetWindowText(m_Pvom);
		m_CE_Pbis.GetWindowText(m_Pbis);
		olVom = DateStringToDate(m_Pvom); 
		olBis = DateStringToDate(m_Pbis);
		CString olVomStr = CTimeToDBString(olVom,TIMENULL);
		CString olBisStr = CTimeToDBString(olBis,TIMENULL);

		olASeas  = "";
		olDSeas  = "";
		
		if(olVom != TIMENULL)
		{
			ogBCD.GetFieldBetween("SEA", "VPFR", "VPTO", olVomStr, "SEAS", olASeas );
		}
		if(olBis != TIMENULL)
		{
			ogBCD.GetFieldBetween("SEA", "VPFR", "VPTO", olBisStr, "SEAS", olDSeas );
		}

 		if(olASeas == olDSeas)
		{
			m_Seas = olASeas;
		}
		else
		{
			if(!olASeas.IsEmpty() && !olDSeas.IsEmpty())
				m_Seas = olASeas + CString(" / ") + olDSeas;
			else
			{
				if(!olASeas.IsEmpty())
					m_Seas = olASeas ;
				else
					m_Seas = olDSeas;
			}

		}
		m_CE_Seas.SetInitText(m_Seas);

	}
	

	/////////////////////////////////////////////
	if(prlNotify->Text.IsEmpty())
		return 0L;
	
	if(((UINT)m_CE_ATtyp.imID == wParam) || ((UINT)m_CE_DTtyp.imID == wParam))
	{
		prlNotify->Status = ogBCD.GetField("NAT", "TTYP", prlNotify->Text, "URNO", olTmp );
		prlNotify->UserStatus = true;
		return 0L;
	}

/*	if(((UINT)m_CE_AHtyp.imID == wParam) || ((UINT)m_CE_DHtyp.imID == wParam))
	{
		prlNotify->Status = ogBCD.GetField("HTY", "HTYP", prlNotify->Text, "URNO", olTmp );
		prlNotify->UserStatus = true;
		return 0L;
	}
*/
	if(((UINT)m_CE_AStyp.imID == wParam) || ((UINT)m_CE_DStyp.imID == wParam))
	{
		prlNotify->Status = ogBCD.GetField("STY", "STYP", prlNotify->Text, "URNO", olTmp );
		prlNotify->UserStatus = true;
		return 0L;
	}

	if( (UINT)m_CE_APsta.imID == wParam)
	{
		prlNotify->Status = ogBCD.GetField("PST", "PNAM", prlNotify->Text, "URNO", olTmp );
		prlNotify->UserStatus = true;

		m_CE_APabs.GetWindowText(olTmp);
		m_CE_APaes.GetWindowText(olTmp1);

		if(prlNotify->Status && (prlNotify->IsChanged || olTmp.IsEmpty() || olTmp1.IsEmpty()))
		{
			CTimeSpan olDura;
			GetPosDefAllocDur(prlNotify->Text, atoi(prmAFlight->Ming), olDura);

			//is an open leg
			if (prmDFlight->Urno == 0 && prmAFlight->Onbl != TIMENULL)
				return 0L;

			if((imModus == DLG_NEW) || (imModus == DLG_COPY))
			{
				CTime olARefDat = DateStringToDate(m_Pvom);

				CString olStrStoa;
				m_CE_AStoa.GetWindowText(olStrStoa);
				CTime olStoa = HourStringToDate( olStrStoa, olARefDat);

				m_CE_APabs.SetWindowText(DateToHourDivString(olStoa, olStoa));		
				m_CE_APaes.SetWindowText(DateToHourDivString(olStoa + olDura, olStoa));		

				m_CE_DPstd.GetWindowText(m_DPstd);
				m_CE_DPdbs.GetWindowText(m_DPdbs);
				m_CE_DPdes.GetWindowText(m_DPdes);
				if (m_DPstd.IsEmpty() && m_DPdbs.IsEmpty() && m_DPdes.IsEmpty())
				{
					CString olStrStod;
					m_CE_DStod.GetWindowText(olStrStod);
					if (!olStrStod.IsEmpty())
					{
						CTime olStod = HourStringToDate( olStrStod, olARefDat);

						CTimeSpan olDuraDep;
						GetPosDefAllocDur(prlNotify->Text, atoi(prmDFlight->Ming), olDuraDep);

						m_DPdes = DateToHourDivString(olStod, olARefDat);
						m_DPdbs = DateToHourDivString(olStod - olDuraDep, olARefDat);

						m_CE_DPstd.SetWindowText(prlNotify->Text);
						m_CE_DPdbs.SetWindowText(m_DPdbs);
						m_CE_DPdes.SetWindowText(m_DPdes);
					}
				}


			}
			else
			{
				m_CE_APabs.SetWindowText(DateToHourDivString(prmAFlight->Tifa, prmAFlight->Tifa));
				m_CE_APaes.SetWindowText(DateToHourDivString(prmAFlight->Tifa + olDura, prmAFlight->Tifa));		

				if (prmDFlight && prmDFlight->Urno > 0 && prmDFlight->Tifd != TIMENULL)
				{
					m_CE_DPstd.GetWindowText(m_DPstd);
					m_CE_DPdbs.GetWindowText(m_DPdbs);
					m_CE_DPdes.GetWindowText(m_DPdes);
					if (m_DPstd.IsEmpty() && m_DPdbs.IsEmpty() && m_DPdes.IsEmpty())
					{
						CTimeSpan olDuraDep;
						GetPosDefAllocDur(m_DPstd, atoi(prmDFlight->Ming), olDuraDep);

						m_DPdes = DateToHourDivString(prmDFlight->Tifd, omDRefDat);
						m_DPdbs = DateToHourDivString(prmDFlight->Tifd - olDuraDep, omDRefDat);

						m_CE_DPstd.SetWindowText(prlNotify->Text);
						m_CE_DPdbs.SetWindowText(m_DPdbs);
						m_CE_DPdes.SetWindowText(m_DPdes);
					}
				}
			}

		}

		return 0L;
	}


	if((UINT)m_CE_DPstd.imID == wParam)
	{
		prlNotify->Status = ogBCD.GetField("PST", "PNAM", prlNotify->Text, "URNO", olTmp );
		prlNotify->UserStatus = true;

		m_CE_DPdbs.GetWindowText(olTmp);
		m_CE_DPdes.GetWindowText(olTmp1);

		if(prlNotify->Status && (prlNotify->IsChanged || olTmp.IsEmpty() || olTmp1.IsEmpty()))
		{
			m_CE_DPdes.SetWindowText(DateToHourDivString(prmDFlight->Tifd, prmDFlight->Tifd));
	
			CTimeSpan olDura;
			GetPosDefAllocDur(prlNotify->Text, atoi(prmDFlight->Ming), olDura);
			m_CE_DPdbs.SetWindowText(DateToHourDivString(prmDFlight->Tifd - olDura, prmDFlight->Tifd));
		}

		return 0L;
	}
	

	CCS_CATCH_ALL	

	return 0L;
}




LONG CSeasonDlg::OnEditChanged( UINT wParam, LPARAM lParam)
{

	CCS_TRY

// begin : reject if opening times
	CString olTmp;

//	arr-gate1
	if((UINT)m_CE_AGta1.imID == wParam)
	{
		m_CE_AGta1.GetWindowText(olTmp);

		CString olGate = prmAFlightSave->Gta1;

		if (prmAFlightSave->Ga1x != TIMENULL && !olGate.IsEmpty())
		{
			if (olTmp != olGate)
			{
				m_CE_AGta1.SetInitText(olGate);
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_GATE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
			}
		}
	}
//	arr-gate2
	if((UINT)m_CE_AGta2.imID == wParam)
	{
		m_CE_AGta2.GetWindowText(olTmp);

		CString olGate = prmAFlightSave->Gta2;

		if (prmAFlightSave->Ga2x != TIMENULL && !olGate.IsEmpty())
		{
			if (olTmp != olGate)
			{
				m_CE_AGta2.SetInitText(olGate);
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_GATE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
			}
		}
	}
//	arr-belt1
	if((UINT)m_CE_ABlt1.imID == wParam)
	{
		m_CE_ABlt1.GetWindowText(olTmp);

		CString olAkt = prmAFlightSave->Blt1;

		if (prmAFlightSave->B1ba != TIMENULL && !olAkt.IsEmpty())
		{
			if (olTmp != olAkt)
			{
				m_CE_ABlt1.SetInitText(olAkt);
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_BELT_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
			}
		}
	}
//	arr-belt2
	if((UINT)m_CE_ABlt2.imID == wParam)
	{
		m_CE_ABlt2.GetWindowText(olTmp);

		CString olAkt = prmAFlightSave->Blt2;

		if (prmAFlightSave->B2ba != TIMENULL && !olAkt.IsEmpty())
		{
			if (olTmp != olAkt)
			{
				m_CE_ABlt2.SetInitText(olAkt);
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_BELT_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
			}
		}
	}
//	arr-position
	if((UINT)m_CE_APsta.imID == wParam)
	{
		m_CE_APsta.GetWindowText(olTmp);

		CString olAkt = prmAFlightSave->Psta;

		if (prmAFlightSave->Onbl != TIMENULL && !olAkt.IsEmpty())
		{
			if (olTmp != olAkt)
			{
				m_CE_APsta.SetInitText(olAkt);
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING1546), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
			}
		}
	}
//	dep-gate1
	if((UINT)m_CE_DGtd1.imID == wParam)
	{
		m_CE_DGtd1.GetWindowText(olTmp);

		CString olGate = prmDFlightSave->Gtd1;

		if (prmDFlightSave->Gd1x != TIMENULL && !olGate.IsEmpty())
		{
			if (olTmp != olGate)
			{
				m_CE_DGtd1.SetInitText(olGate);
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_GATE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
			}
		}
	}
//	dep-gate2
	if((UINT)m_CE_DGtd2.imID == wParam)
	{
		m_CE_DGtd2.GetWindowText(olTmp);

		CString olGate = prmDFlightSave->Gtd2;

		if (prmDFlightSave->Gd2x != TIMENULL && !olGate.IsEmpty())
		{
			if (olTmp != olGate)
			{
				m_CE_DGtd2.SetInitText(olGate);
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_GATE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
			}
		}
	}
//	dep-position
	if((UINT)m_CE_DPstd.imID == wParam)
	{
		m_CE_DPstd.GetWindowText(olTmp);

		CString olAkt = prmDFlightSave->Pstd;

		if (prmDFlightSave->Ofbl != TIMENULL && !olAkt.IsEmpty())
		{
			if (olTmp != olAkt)
			{
				m_CE_DPstd.SetInitText(olAkt);
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING1546), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
			}
		}
	}
//	dep-wro
	if((UINT)m_CE_DWro1.imID == wParam)
	{
		m_CE_DWro1.GetWindowText(olTmp);

		CString olAkt = prmDFlightSave->Wro1;

		if (prmDFlightSave->W1ba != TIMENULL && !olAkt.IsEmpty())
		{
			if (olTmp != olAkt)
			{
				m_CE_DWro1.SetInitText(olAkt);
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_LOUNGE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
			}
		}
	}
//	dep-wro
	if((UINT)m_CE_DWro2.imID == wParam)
	{
		m_CE_DWro2.GetWindowText(olTmp);

		CString olAkt = prmDFlightSave->Wro2;

		if (prmDFlightSave->W2ba != TIMENULL && !olAkt.IsEmpty())
		{
			if (olTmp != olAkt)
			{
				m_CE_DWro2.SetInitText(olAkt);
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_LOUNGE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
			}
		}
	}
// end : reject if opening times

	if(!bmChanged)
	{
		m_CB_Ok.SetColors(RGB(255,0,0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	}
	bmChanged = true;

	CCS_CATCH_ALL

	return 0L;
}

LONG CSeasonDlg::OnTableIPEdit( UINT wParam, LPARAM lParam)
{
	CCSTABLENOTIFY *prlNotify = (CCSTABLENOTIFY*)lParam;

//	reject a new checkin-counter if counter is already open
	if(prlNotify && prlNotify->Column == 0)
	{
		return CcaHasNoOpentime (prlNotify->Line);
	}

//rkr05042001
	// disable the cic if open (now you can�t hit with TAB)
	if(prlNotify && (prlNotify->SourceTable == pomDCinsTable))
	{
		for (int i = 0; i < omDCinsSave.GetSize(); i++)
		{
			CCADATA *prlCca = &omDCinsSave[i];

			if(prlCca && prlCca->Ckba != TIMENULL)
			{
				bool test = pomDCinsTable->SetColumnEditable(i, 0, false);
			}
			if(prlCca && prlCca->Ckba == TIMENULL)
			{
				bool test = pomDCinsTable->SetColumnEditable(i, 0, true);
			}
		}
	}
//rkr05042001

	return 0L;
}

LONG CSeasonDlg::CcaHasNoOpentime (int ipLineNo)
{
	if (ipLineNo >= 0 && ipLineNo < omDCinsSave.GetSize())
	{
		CCADATA *prlCca = &omDCinsSave[ipLineNo];

		if(prlCca && prlCca->Ckba != TIMENULL)
		{
			CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_CCA_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
			m_CB_Ok.SetFocus();
			return -1L;
		}
	}

	return 0L;
}

LONG CSeasonDlg::OnTableIPEditKillfocus( UINT wParam, LPARAM lParam)
{

	CCS_TRY

	CCSIPEDITNOTIFY *prlNotify = (CCSIPEDITNOTIFY*)lParam;

	if(prlNotify->Text.IsEmpty())
		return 0L;
	CString olTmp;

/*
	if(((prlNotify->SourceTable == pomAViaTable) && (prlNotify->Column == 0)) ||
	   ((prlNotify->SourceTable == pomDViaTable) && (prlNotify->Column == 0)))
	{
		prlNotify->UserStatus = true;
		prlNotify->Status = ogBCD.GetField("APT", "APC3", prlNotify->Text, "APC4", olTmp );
		return 0L;
	}
	if(((prlNotify->SourceTable == pomAViaTable) && (prlNotify->Column > 1)) ||
	   ((prlNotify->SourceTable == pomDViaTable) && (prlNotify->Column > 1)))
	{

		CTime olStoa;
		CTime olStod;
		CString olTmp, olTmp2;
		pomAViaTable->GetTextFieldValue(prlNotify->Line, 2, olTmp);
		pomAViaTable->GetTextFieldValue(prlNotify->Line, 3, olTmp2);
		olStoa = HourStringToDate(olTmp, omARefDat); 
		olStod = HourStringToDate(olTmp2, omARefDat); 

		if((olStoa != TIMENULL) && (olStod != TIMENULL))
		{
			if(olStoa >= olStod)
			{
				prlNotify->UserStatus = true;
				prlNotify->Status = false;
			}
		}

	}
*/

	if(((prlNotify->SourceTable == pomAJfnoTable) && (prlNotify->Column == 0)) ||
	   ((prlNotify->SourceTable == pomDJfnoTable) && (prlNotify->Column == 0)))
	{
		if(prlNotify->Text.GetLength() == 3)
		{
			prlNotify->Status = ogBCD.GetField("ALT", "ALC3", prlNotify->Text, "ALC2", olTmp );
		}
		else
		{
			prlNotify->Status = ogBCD.GetField("ALT", "ALC2", prlNotify->Text, "ALC3", olTmp );
		}
		prlNotify->UserStatus = true;
		return 0L;
	}

	if((prlNotify->SourceTable == pomDCinsTable) && (prlNotify->Column == 0))
	{
		prlNotify->UserStatus = true;
		if(prlNotify->Status = ogBCD.GetField("CIC", "CNAM", prlNotify->Text, "TERM", olTmp ))
		{
			pomDCinsTable->SetIPValue(prlNotify->Line, 0, prlNotify->Text);
			pomDCinsTable->SetIPValue(prlNotify->Line, 1, olTmp);
		}
	}

	CCS_CATCH_ALL

	return 0L;
}

void CSeasonDlg::OnAdaly() 
{

	CCS_TRY

	CString olVomStr;
	CString olBisStr;

	m_CE_Pvom.GetWindowText(olVomStr);
	m_CE_Pbis.GetWindowText(olBisStr);

	CTime olVom = DateHourStringToDate(olVomStr, CString("0900")); 
	CTime olBis = DateHourStringToDate(olBisStr, CString("1500")); 

	m_ADays = "1234567";
	if((olVom != TIMENULL) && (olBis != TIMENULL))
	{
		DaysinPeriod(olVom,  olBis,  m_ADays);
	}

	m_CE_ADays.SetInitText(m_ADays);

	CCS_CATCH_ALL

}

void CSeasonDlg::OnDdaly() 
{

	CCS_TRY

	CString olVomStr;
	CString olBisStr;

	m_CE_Pvom.GetWindowText(olVomStr);
	m_CE_Pbis.GetWindowText(olBisStr);

	CTime olVom = DateHourStringToDate(olVomStr, CString("0900")); 
	CTime olBis = DateHourStringToDate(olBisStr, CString("1500")); 

	m_DDays = "1234567";
	if((olVom != TIMENULL) && (olBis != TIMENULL))
	{
		DaysinPeriod(olVom,  olBis,  m_DDays);
	}

	m_CE_DDays.SetInitText(m_DDays);


	CCS_CATCH_ALL

}



/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
// CHECKALL

bool CSeasonDlg::CheckAll(CString &opGMess, CString &opAMess, CString &opDMess) 
{

	CCS_TRY

	m_CE_Regn.GetWindowText(m_Regn); 
	m_Regn.Remove('\n');
	m_Regn.TrimLeft();
	m_Regn.TrimRight();
	m_CE_Regn.SetWindowText(m_Regn);

	if(m_AAlc3.IsEmpty() && m_AFltn.IsEmpty() && m_AFlns.IsEmpty() && m_AStoa.IsEmpty() && m_AOrg3.IsEmpty())
		bmAnkunft = false;
	else
		bmAnkunft = true;

	if(m_DAlc3.IsEmpty() && m_DFltn.IsEmpty() && m_DFlns.IsEmpty() && m_DStod.IsEmpty() && m_DDes3.IsEmpty())
		bmAbflug = false;
	else
		bmAbflug = true;


	if(!bmAnkunft && !bmAbflug)
	{
		return false;
	}

	bool blRet = CheckGatPos(bmAnkunft, bmAbflug, opGMess, opAMess, opDMess);

	if(imModus == DLG_CHANGE_DIADATA)
	{
		return blRet;
	}


	if((imModus == DLG_NEW) || (imModus == DLG_COPY))
	{
		if(!m_CE_Pvom.GetStatus() || !m_CE_Pbis.GetStatus()) 
			opGMess += GetString(IDS_STRING396) + CString("\n");//"Periode\n"; 

		if(!m_CE_Freq.GetStatus())
			opGMess += GetString(IDS_STRING397) + CString("\n");//"Wochenfrequenz\n"; 

		if(!m_CE_Pvom.GetStatus() && bmAnkunft) 
			opGMess += GetString(IDS_STRING398) + CString("\n");//"Ankunftsdatum \n"; 
		if(!m_CE_Pbis.GetStatus() && bmAbflug) 
			opGMess += GetString(IDS_STRING399) + CString("\n");//"Abflugsdatum! \n"; 

	// Check season for selected flight
		CString olVomStr;
		CString olBisStr;
		m_CE_Pvom.GetWindowText(olVomStr);
		m_CE_Pbis.GetWindowText(olBisStr);
		CTime olVom = DateHourStringToDate(olVomStr, CString("0900")); 
		CTime olBis = DateHourStringToDate(olBisStr, CString("1500")); 
		CString olSeason;
		if (!ogBCD.GetFieldBetween("SEA", "VPFR", "VPTO", CTimeToDBString(olVom, TIMENULL), "SEAS", olSeason ))
		{
			opGMess += GetString(IDS_STRING2173) + CString("\n");//"Wochenfrequenz\n"; 
		}
		if (!ogBCD.GetFieldBetween("SEA", "VPFR", "VPTO", CTimeToDBString(olBis, TIMENULL), "SEAS", olSeason ))
		{
			opGMess += GetString(IDS_STRING2174) + CString("\n");//"Wochenfrequenz\n"; 
		}

	}
	else
	{
		if(!m_CE_Regn.GetStatus() && !m_Regn.IsEmpty())
			opGMess += GetString(IDS_STRING341) + CString("\n");//"LFZ-Kennzeichen"; 
	}


		
	if(m_Act3.IsEmpty() && m_Act5.IsEmpty())
	{
		opGMess += GetString(IDS_STRING342);//"A/C Typ\n"; 
	}
	else
	{
//		if(!m_CE_Act3.GetStatus() && !m_CE_Act5.GetStatus())
		if(!m_CE_Act3.GetStatus() || !m_CE_Act5.GetStatus())
			opGMess += GetString(IDS_STRING342);//"A/C Typ\n";  
	}


	if(!m_CE_Ming.GetStatus())
		opGMess += GetString(IDS_STRING343) + CString("\n");//"Min.G/T \n"; 


	if (bmAnkunft && bmAbflug)
	{
		if((prmAFlight->Stoa != TIMENULL) && (prmDFlight->Stod != TIMENULL))
		{
			if(prmAFlight->Stoa > prmDFlight->Stod)
				opGMess += GetString(IDS_STRING1982) + CString("\n");//"STD is before STA!\n";
		}
	}

	CString olBuffer;
	if(bmAnkunft)
	{

		if (m_CE_AFlti.IsChanged() && 
			(!m_CE_AFlti.GetStatus() || (m_AFlti != "I" && m_AFlti != "D" && !m_AFlti.IsEmpty())))
			opAMess += GetString(IDS_STRING1992) + CString("\n");//"Flight ID\n";
		
		if(!m_CE_AAlc3.GetStatus() || !m_CE_AFltn.GetStatus() || !m_CE_AFlns.GetStatus())
			opAMess += GetString(IDS_STRING347) + CString("\n");//"Flugnummer\n"; 


		
		if(imModus != DLG_CHANGE)
		{
			CString olVomStr;
			CString olBisStr;
			m_CE_Pvom.GetWindowText(olVomStr);
			m_CE_Pbis.GetWindowText(olBisStr);
			CTime olVom = DateHourStringToDate(olVomStr, CString("0900")); 
			CTime olBis = DateHourStringToDate(olBisStr, CString("1500")); 
			CString olDays = m_ADays;
			if((olVom != TIMENULL) && (olBis != TIMENULL))
			{
				if(!DaysinPeriod(olVom,  olBis,  olDays))
				{
					opAMess += GetString(IDS_STRING401) + CString("\n");//"Wochentag\n"; 
				}	
			}
		}	

		
		// weitere Flugnummern


		bool bl0;
		bool bl1;
		bool bl2;
		CString  olAlc3;
		CString  olFltn;
		CString  olFlns;

		int ilLines = pomAJfnoTable->GetLinesCount();
		for(int i = 0; i < ilLines; i++)
		{
			pomDJfnoTable->GetTextFieldValue(i, 0, olAlc3);
			pomDJfnoTable->GetTextFieldValue(i, 1, olFltn);
			pomDJfnoTable->GetTextFieldValue(i, 2, olFlns);
			if(!olAlc3.IsEmpty() && !olFltn.IsEmpty() && !olFlns.IsEmpty())
			{
				bl0 = pomAJfnoTable->GetCellStatus(i, 0); 
				bl1 = pomAJfnoTable->GetCellStatus(i, 1); 
				bl2 = pomAJfnoTable->GetCellStatus(i, 2); 
				if((!bl0) || (!bl1) || (!bl2))
				{
					olBuffer.Format(GetString(IDS_STRING348), i);//"Weitere Flugnummer in Zeile %d\n"
					opAMess += olBuffer; 
				}
			}
		}



		if(!m_CE_AOrg3.GetStatus())
			opAMess += GetString(IDS_STRING349) + CString("\n");//"Ausgangsflughafencode!\n"; 

		if(!m_CE_AStoa.GetStatus())
			opAMess += GetString(IDS_STRING350) + CString("\n");//"Planm��ige Ankunftszeit!\n";
		
		if(!m_CE_AStod.GetStatus())
			opAMess += GetString(IDS_STRING384) + CString("\n");//"Planm��ige Abflugszeit!\n";
		
/*		if((prmAFlight->Stoa != TIMENULL) && (prmDFlight->Stod != TIMENULL) && bmAnkunft && bmAbflug)
		{
			if(prmAFlight->Stoa <= prmAFlight->Stod)
				opAMess += GetString(IDS_STRING351) + CString("\n");//"Planm��ige Ankunftszeit liegt vor Abflugszeit!\n";
		}
*/
		if(!m_CE_AEtai.GetStatus())
			opAMess += GetString(IDS_STRING352) + CString("\n");//"Erwartete Ankunftszeit!\n";


		opAMess += pomAViaCtrl->GetStatus();

/*
		
		// Vias
		int il1Count = 0;
		int il2Count = 0;
		CString olApc3;
		CString olFids;
		CString olTmp;
		CString olTmp2;
		CTime olStod;
		CTime olStoa;

		ilLines = pomAViaTable->GetLinesCount();
		for( i = 0; i < ilLines; i++)
		{

			pomAViaTable->GetTextFieldValue(i, 0, olApc3);
			pomAViaTable->GetTextFieldValue(i, 3, olFids);
			if(olFids == "1")
				il1Count++;
			if(olFids == "2")
				il2Count++;

			pomAViaTable->GetTextFieldValue(i, 1, olTmp);
			pomAViaTable->GetTextFieldValue(i, 2, olTmp2);
			olStoa = HourStringToDate(olTmp, omARefDat); 
			olStod = HourStringToDate(olTmp2, omARefDat); 
			if(olStoa != TIMENULL || olStod != TIMENULL || !olApc3.IsEmpty() || !olFids.IsEmpty())
			{
			
				if(!pomAViaTable->GetCellStatus(i, 0))
				{
					sprintf(buffer, GetString(IDS_STRING355),i);;//"Via-Flughafencode in Zeile %d\n"
					opAMess += CString(buffer) ; 
				}
				if(!pomAViaTable->GetCellStatus(i, 1))
				{
					sprintf(buffer, GetString(IDS_STRING356),i); // "Via- Planm��ige Ankunft in Zeile %d\n"
					opAMess += CString(buffer) ; 
				}
				if(!pomAViaTable->GetCellStatus(i, 2))
				{
					sprintf(buffer, GetString(IDS_STRING357),i); //"Via- Planm��iger Abflug in Zeile %d\n"
					opAMess += CString(buffer) ; 
				}
				if((olStoa != TIMENULL) && (olStod != TIMENULL))
				{
					if(olStoa >= olStod)
					{
						sprintf(buffer, GetString(IDS_STRING358),i); //"Via in Zeile %d: Planm��iger Abflug vor Ankunft\n"
						opAMess += CString(buffer) ; 
					}
				}
			}
			
		}


*/


		if(!m_CE_ATtyp.GetStatus())
			opAMess += GetString(IDS_STRING360) + CString("\n");//"Verkehrsart\n";

		if(!m_CE_AExt1.GetStatus())
			opAMess += GetString(IDS_STRING381) + CString("\n");//"Bezeichnung des Ausganges 1\n";

		if(!m_CE_AExt2.GetStatus())
			opAMess += GetString(IDS_STRING382) + CString("\n");//"Bezeichnung des Ausganges 2\n";

		// Check Status
		if(!m_CB_AStatus.GetCheck() && !m_CB_AStatus2.GetCheck() &&
		   !m_CB_AStatus3.GetCheck() && !m_CB_ACxx.GetCheck() && 
		   !m_CB_ANoop.GetCheck() && !IsCircular(m_AOrg3, m_ADes3))
			opAMess += GetString(IDS_STRING1900) + CString("\n");

	}


	//////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////
	if(bmAbflug)
	{

		if(!m_CE_DAlc3.GetStatus() || !m_CE_DFltn.GetStatus() || !m_CE_DFlns.GetStatus())
			opDMess += GetString(IDS_STRING347) + CString("\n");//"Flugnummer\n"; 

		if (m_CE_DFlti.IsChanged() && 
			(!m_CE_DFlti.GetStatus() || (m_DFlti != "I" && m_DFlti !="D" && !m_DFlti.IsEmpty())))
 			opDMess += GetString(IDS_STRING1992) + CString("\n");//"Flight ID\n";

		if(imModus != DLG_CHANGE)
		{
			CString olVomStr;
			CString olBisStr;
			m_CE_Pvom.GetWindowText(olVomStr);
			m_CE_Pbis.GetWindowText(olBisStr);
			CTime olVom = DateHourStringToDate(olVomStr, CString("0900")); 
			CTime olBis = DateHourStringToDate(olBisStr, CString("1500")); 
			CString olDays = m_DDays;
			if((olVom != TIMENULL) && (olBis != TIMENULL))
			{
				if(!DaysinPeriod(olVom,  olBis,  olDays))
				{
					opDMess += GetString(IDS_STRING401) + CString("\n");//"Wochentag\n"; 
				}	
			}
		}		
		
		// weitere Flugnummern

		CString  olAlc3;
		CString  olFltn;
		CString  olFlns;

		int ilLines = pomDJfnoTable->GetLinesCount();
		for(int i = 0; i < ilLines; i++)
		{
			pomDJfnoTable->GetTextFieldValue(i, 0, olAlc3);
			pomDJfnoTable->GetTextFieldValue(i, 1, olFltn);
			pomDJfnoTable->GetTextFieldValue(i, 2, olFlns);
			if(!olAlc3.IsEmpty() && !olFltn.IsEmpty() && !olFlns.IsEmpty())
			{
				if((!pomDJfnoTable->GetCellStatus(i, 0)) || (!pomDJfnoTable->GetCellStatus(i, 1)) || (!pomDJfnoTable->GetCellStatus(i, 2)))
				{
					olBuffer.Format(GetString(IDS_STRING348),i); ;//"Weitere Flugnummer in Zeile %d\n"
					opDMess += olBuffer; 
				}
			}
		}



		if(!m_CE_DDes3.GetStatus())
			opDMess += GetString(IDS_STRING1137) + CString("\n");//"Zielflughafencode!\n"; 

		if(!m_CE_DStod.GetStatus())
			opDMess += GetString(IDS_STRING384) + CString("\n");//"Planm��ige Abflugszeit!\n";
		
		if(!m_CE_DStoa.GetStatus())
			opDMess += GetString(IDS_STRING350) + CString("\n");//"Planm��ige Ankunftszeit!\n";

		/*		
		if((prmDFlight->Stoa != TIMENULL) && (prmDFlight->Stod != TIMENULL) && bmAnkunft && bmAbflug)
		{
			if(prmDFlight->Stod >= prmDFlight->Stoa)
				opDMess += GetString(IDS_STRING402) + CString("\n");//"Planm��ige Abflugszeit liegt vor Ankunftszeit!\n";
		}
		*/

		if(!m_CE_DEtdi.GetStatus())
			opDMess += GetString(IDS_STRING385) + CString("\n");//"Ertwartete Abflugsszeit!\n";


		
		opDMess += pomDViaCtrl->GetStatus();


		if(!m_CE_DTtyp.GetStatus())
			opDMess += GetString(IDS_STRING360) + CString("\n");//"Verkehrsart\n";


		// Check Status
		if(!m_CB_DStatus.GetCheck() &&  !m_CB_DStatus2.GetCheck() &&
		   !m_CB_DStatus3.GetCheck() && !m_CB_DCxx.GetCheck() && 
		   !m_CB_DNoop.GetCheck() && !IsCircular(m_DOrg3, m_DDes3))
			opDMess += GetString(IDS_STRING1900) + CString("\n");



		CTime olStoa;
		CTime olStod;
		CString olTmp;
		CString olTmp2;

		// Checkinschalter
		if(pomDCinsTable != NULL)
		{
			ilLines = pomDCinsTable->GetLinesCount();
			for( i = 0; i < ilLines; i++)
			{

				pomDCinsTable->GetTextFieldValue(i, 2, olTmp);
				pomDCinsTable->GetTextFieldValue(i, 3, olTmp2);
				olStoa = HourStringToDate(olTmp, omDRefDat); 
				olStod = HourStringToDate(olTmp2, omDRefDat); 
					
				if(!pomDCinsTable->GetCellStatus(i, 0))
				{
					olBuffer.Format(GetString(IDS_STRING392),i); //"Bezeichnung Check-In Schalter in Zeile %d\n"
					opDMess += olBuffer; 
				}
				if(!pomDCinsTable->GetCellStatus(i, 2))
				{
					olBuffer.Format(GetString(IDS_STRING393),i); //"Check-In Schalter �ffnungszeit von in Zeile %d\n"
					opDMess += olBuffer; 
				}
				if(!pomDCinsTable->GetCellStatus(i, 3))
				{
					olBuffer.Format(GetString(IDS_STRING394),i); //"Check-In Schalter �ffnungszeit bis in Zeile %d\n"
					opDMess += olBuffer; 
				}
				if((olStoa != TIMENULL) && (olStod != TIMENULL))
				{
					if(olStoa > olStod)
					{
						// Allocation of Check-In Counter in line %d: Start time is after end time.\n
						olBuffer.Format(GetString(IDS_STRING395),i);
						opDMess += olBuffer; 
					}
				}
				if (olStoa == TIMENULL && olStod != TIMENULL)
				{
					// Allocation of Check-In Counter in line %d: End time without start time!\n
					olBuffer.Format(GetString(IDS_STRING2011), i);
					opDMess += olBuffer; 
				}

			}
		}
	}

	CCS_CATCH_ALL

	if(opGMess.IsEmpty() && opAMess.IsEmpty() && opDMess.IsEmpty())
		return true;
	else
		return false;

}




bool CSeasonDlg::CheckGatPos(bool bpAnkunft, bool bpAbflug, CString &opGMess, CString &opAMess, CString &opDMess) 
{

	if(bpAnkunft)
	{
		if(!m_CE_APsta.GetStatus())
			opAMess += GetString(IDS_STRING361) + CString("\n");//"Postionsbezeichnung\n";

		if(!m_CE_APabs.GetStatus())
			opAMess += GetString(IDS_STRING362) + CString("\n");//"Beginn der Postionsbelegung\n";

		if(!m_CE_APaes.GetStatus())
			opAMess += GetString(IDS_STRING363) + CString("\n");//"Ende der Postionsbelegung\n";

		if((prmAFlight->Paes != TIMENULL) && (prmAFlight->Paes != TIMENULL))
		{
			if(prmAFlight->Pabs > prmAFlight->Paes)
				opAMess += GetString(IDS_STRING364) + CString("\n");//"Ende der Postionsbelegung ist vor Beginn\n";
		}


		if(!m_CE_AGta1.GetStatus())
			opAMess += GetString(IDS_STRING365) + CString("\n");//"Gatebezeichnung 1\n";

		if(!m_CE_AGa1b.GetStatus())
			opAMess += GetString(IDS_STRING366) + CString("\n");//"Beginn der Gatebelegung 1\n";

		if(!m_CE_AGa1e.GetStatus())
			opAMess += GetString(IDS_STRING367) + CString("\n");//"Ende der Gatebelegung 1\n";

		if((prmAFlight->Ga1b != TIMENULL) && (prmAFlight->Ga1e != TIMENULL))
		{
			if(prmAFlight->Ga1b > prmAFlight->Ga1e)
				opAMess += GetString(IDS_STRING368) + CString("\n");//"Ende der Gatebelegung 1 ist vor Beginn\n";
		}

		if(!m_CE_AGta2.GetStatus())
			opAMess += GetString(IDS_STRING369) + CString("\n");//"Gatebezeichnung 2\n";

		if(!m_CE_AGa2b.GetStatus())
			opAMess += GetString(IDS_STRING370) + CString("\n");//"Beginn der Gatebelegung 2\n";

		if(!m_CE_AGa2e.GetStatus())
			opAMess += GetString(IDS_STRING371) + CString("\n");//"Ende der Gatebelegung 2\n";

		if((prmAFlight->Ga2b != TIMENULL) && (prmAFlight->Ga1e != TIMENULL))
		{
			if(prmAFlight->Ga2b > prmAFlight->Ga2e)
				opAMess += GetString(IDS_STRING372) + CString("\n");//"Ende der Gatebelegung 2 ist vor Beginn\n";
		}

		if(!m_CE_ABlt1.GetStatus())
			opAMess += GetString(IDS_STRING373) + CString("\n");//"Bezeichnung des Gep�ckbandes 1\n";

		if(!m_CE_AB1bs.GetStatus())
			opAMess += GetString(IDS_STRING374) + CString("\n");//"Beginn der Belegung des Gep�ckbandes 1\n";

		if(!m_CE_AB1es.GetStatus())
			opAMess += GetString(IDS_STRING375) + CString("\n");//"Ende der Belegung des Gep�ckbandes 1\n";

		if((prmAFlight->B1bs != TIMENULL) && (prmAFlight->B1es != TIMENULL))
		{
			if(prmAFlight->B1bs > prmAFlight->B1es)
				opAMess += GetString(IDS_STRING376) + CString("\n");//"Ende der Belegung des Gep�ckbandes 1 ist vor Beginn\n";
		}

		if(!m_CE_ABlt2.GetStatus())
			opAMess += GetString(IDS_STRING377) + CString("\n");//"Bezeichnung des Gep�ckbandes 2\n";

		if(!m_CE_AB2bs.GetStatus())
			opAMess += GetString(IDS_STRING378) + CString("\n");//"Beginn der Belegung des Gep�ckbandes 2\n";

		if(!m_CE_AB2es.GetStatus())
			opAMess += GetString(IDS_STRING379) + CString("\n");//"Ende der Belegung des Gep�ckbandes 2\n";

		if((prmAFlight->B2bs != TIMENULL) && (prmAFlight->B2es != TIMENULL))
		{
			if(prmAFlight->B2bs > prmAFlight->B2es)
				opAMess += GetString(IDS_STRING380) + CString("\n");//"Ende der Belegung des Gep�ckbandes 2 ist vor Beginn\n";
		}
	}


	if(bpAbflug)
	{
		if(!m_CE_DPstd.GetStatus())
			opDMess += GetString(IDS_STRING361) + CString("\n");//"Postionsbezeichnung\n";

		if(!m_CE_DPdbs.GetStatus())
			opDMess += GetString(IDS_STRING362) + CString("\n");//"Beginn der Postionsbelegung\n";

		if(!m_CE_DPdes.GetStatus())
			opDMess += GetString(IDS_STRING363) + CString("\n");//"Ende der Postionsbelegung\n";

		if((prmDFlight->Pdes != TIMENULL) && (prmDFlight->Pdes != TIMENULL))
		{
			if(prmDFlight->Pdbs > prmDFlight->Pdes)
				opDMess += GetString(IDS_STRING364) + CString("\n");//"Ende der Postionsbelegung ist vor Beginn\n";
		}


		if(!m_CE_DGtd1.GetStatus())
			opDMess += GetString(IDS_STRING365) + CString("\n");//"Gatebezeichnung 1\n";

		if(!m_CE_DGd1b.GetStatus())
			opDMess += GetString(IDS_STRING366) + CString("\n");//"Beginn der Gatebelegung 1\n";

		if(!m_CE_DGd1e.GetStatus())
			opDMess += GetString(IDS_STRING367) + CString("\n");//"Ende der Gatebelegung 1\n";

		if((prmDFlight->Gd1b != TIMENULL) && (prmDFlight->Gd1e != TIMENULL))
		{
			if(prmDFlight->Gd1b > prmDFlight->Gd1e)
				opDMess += GetString(IDS_STRING368) + CString("\n");//"Ende der Gatebelegung 1 ist vor Beginn\n";
		}

		if(!m_CE_DGtd2.GetStatus())
			opDMess += GetString(IDS_STRING369) + CString("\n");//"Gatebezeichnung 2\n";

		if(!m_CE_DGd2b.GetStatus())
			opDMess += GetString(IDS_STRING370) + CString("\n");//"Beginn der Gatebelegung 2\n";

		if(!m_CE_DGd2e.GetStatus())
			opDMess += GetString(IDS_STRING371) + CString("\n");//"Ende der Gatebelegung 2\n";

		if((prmDFlight->Gd2b != TIMENULL) && (prmDFlight->Gd1e != TIMENULL))
		{
			if(prmDFlight->Gd2b > prmDFlight->Gd2e)
				opDMess += GetString(IDS_STRING372) + CString("\n");//"Ende der Gatebelegung 2 ist vor Beginn\n";
		}

		if(!m_CE_DWro1.GetStatus())
			opDMess += GetString(IDS_STRING388) + CString("\n");//"Bezeichnung des Warteraumes\n";

		if(!m_CE_DWro2.GetStatus())
			opDMess += GetString(IDS_STRING388) + CString("\n");//"Bezeichnung des Warteraumes\n";

		if(!m_CE_DW1bs.GetStatus())
			opDMess += GetString(IDS_STRING389) + CString("\n");//"Beginn der Belegung des Warteraumes 1\n";

		if(!m_CE_DW1es.GetStatus())
			opDMess += GetString(IDS_STRING390) + CString("\n");//"Ende der Belegung des Warteraumes \n";

		if(!m_CE_DW2bs.GetStatus())
			opDMess += GetString(IDS_STRING389) + CString("\n");//"Beginn der Belegung des Warteraumes 1\n";

		if(!m_CE_DW2es.GetStatus())
			opDMess += GetString(IDS_STRING390) + CString("\n");//"Ende der Belegung des Warteraumes \n";

		if((prmDFlight->W1bs != TIMENULL) && (prmDFlight->W1es != TIMENULL))
		{
			if(prmDFlight->W1bs > prmDFlight->W1es)
				opDMess += GetString(IDS_STRING391) + CString("\n");//"Ende der Belegung des Warteraumes ist vor Beginn\n";
		}

		if((prmDFlight->W2bs != TIMENULL) && (prmDFlight->W2es != TIMENULL))
		{
			if(prmDFlight->W2bs > prmDFlight->W2es)
				opDMess += GetString(IDS_STRING391) + CString("\n");//"Ende der Belegung des Warteraumes ist vor Beginn\n";
		}
	}


	if(opGMess.IsEmpty() && opAMess.IsEmpty() && opDMess.IsEmpty())
		return true;
	else
		return false;
}




void CSeasonDlg::OnAstatus() 
{
	
	CCS_TRY

	OnEditChanged(0,0);
	m_CB_ACxx.SetCheck(FALSE);
	m_CB_ANoop.SetCheck(FALSE);

	CCS_CATCH_ALL

}

void CSeasonDlg::OnAcxx() 
{

	CCS_TRY

	OnEditChanged(0,0);
	m_CB_AStatus.SetCheck(FALSE);
	m_CB_AStatus2.SetCheck(FALSE);
	m_CB_AStatus3.SetCheck(FALSE);
	m_CB_ANoop.SetCheck(FALSE);
	//if(m_CB_ACxx.GetState())
	//	m_CB_AStatus.SetCheck(TRUE);

	CCS_CATCH_ALL

}

void CSeasonDlg::OnAnoop() 
{

	CCS_TRY

	OnEditChanged(0,0);
	m_CB_AStatus.SetCheck(FALSE);
	m_CB_AStatus2.SetCheck(FALSE);
	m_CB_AStatus3.SetCheck(FALSE);
	m_CB_ACxx.SetCheck(FALSE);
	//if(m_CB_ANoop.GetState())
	//	m_CB_AStatus.SetCheck(TRUE);

	CCS_CATCH_ALL

}

void CSeasonDlg::OnDcxx() 
{

	CCS_TRY

	OnEditChanged(0,0);
	m_CB_DStatus.SetCheck(FALSE);
	m_CB_DStatus2.SetCheck(FALSE);
	m_CB_DStatus3.SetCheck(FALSE);
	m_CB_DNoop.SetCheck(FALSE);
	//if(m_CB_DCxx.GetState())
	//	m_CB_DStatus.SetCheck(TRUE);

	CCS_CATCH_ALL
	
}

void CSeasonDlg::OnDnoop() 
{

	CCS_TRY

	OnEditChanged(0,0);
	m_CB_DStatus.SetCheck(FALSE);
	m_CB_DStatus2.SetCheck(FALSE);
	m_CB_DStatus3.SetCheck(FALSE);
	m_CB_DCxx.SetCheck(FALSE);
	//if(m_CB_DNoop.GetState())
		//m_CB_DStatus.SetCheck(TRUE);

	CCS_CATCH_ALL	

}

void CSeasonDlg::OnDstatus() 
{

	CCS_TRY

	OnEditChanged(0,0);
	m_CB_DCxx.SetCheck(FALSE);
	m_CB_DNoop.SetCheck(FALSE);

	CCS_CATCH_ALL

}









void CSeasonDlg::InitTables()
{

	CCS_TRY

	int ili;
	CRect olRectBorder;
	m_CE_AJfnoBorder.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);

	pomAJfnoTable->SetHeaderSpacing(0);
	pomAJfnoTable->SetMiniTable();
	pomAJfnoTable->SetTableEditable(true);

	//rkr04042001
	pomAJfnoTable->SetIPEditModus(true);

    pomAJfnoTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomAJfnoTable->SetSelectMode(0);

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomAJfnoTable->SetShowSelection(false);
	pomAJfnoTable->ResetContent();
	
	TABLE_HEADER_COLUMN *prlHeader[5];
	TABLE_HEADER_COLUMN rlHeader;

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 36; 
	prlHeader[0]->Font = &ogCourier_Regular_10;
	prlHeader[0]->Text = CString("");

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 46; 
	prlHeader[1]->Font = &ogCourier_Regular_10;
	prlHeader[1]->Text = CString("");

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 15; 
	prlHeader[2]->Font = &ogCourier_Regular_10;
	prlHeader[2]->Text = CString("");

	for(ili = 0; ili < 3; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}
	pomAJfnoTable->SetHeaderFields(omHeaderDataArray);
	pomAJfnoTable->SetDefaultSeparator();
	pomAJfnoTable->SetTableEditable(true);
	omHeaderDataArray.DeleteAll();
	pomAJfnoTable->DisplayTable();





	m_CE_DJfnoBorder.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);

	pomDJfnoTable->SetHeaderSpacing(0);
	pomDJfnoTable->SetMiniTable();
	pomDJfnoTable->SetTableEditable(true);

	//rkr04042001
	pomDJfnoTable->SetIPEditModus(true);

	pomDJfnoTable->SetSelectMode(0);
    pomDJfnoTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);


	pomDJfnoTable->SetShowSelection(false);
	pomDJfnoTable->ResetContent();
	
	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 36; 
	prlHeader[0]->Font = &ogCourier_Regular_10;
	prlHeader[0]->Text = CString("");

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 46; 
	prlHeader[1]->Font = &ogCourier_Regular_10;
	prlHeader[1]->Text = CString("");

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 15; 
	prlHeader[2]->Font = &ogCourier_Regular_10;
	prlHeader[2]->Text = CString("");

	for(ili = 0; ili < 3; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}
	pomDJfnoTable->SetHeaderFields(omHeaderDataArray);
	pomDJfnoTable->SetDefaultSeparator();
	omHeaderDataArray.DeleteAll();
	pomDJfnoTable->DisplayTable();





	m_CE_DCinsBorder.GetWindowRect( olRectBorder );
	pomDCinsTable->ResetContent();
	pomDCinsTable->SetHeaderSpacing(0);
	pomDCinsTable->SetMiniTable();
	pomDCinsTable->SetTableEditable(true);

	//rkr04042001
	pomDCinsTable->SetIPEditModus(true);

	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);
    pomDCinsTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
//	pomDCinsTable->SetSelectMode(0);


	pomDCinsTable->SetShowSelection(false);
	pomDCinsTable->ResetContent();
	

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 40; 
	prlHeader[0]->Font = &ogCourier_Regular_10;
	prlHeader[0]->Text = CString("");

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 12; 
	prlHeader[1]->Font = &ogCourier_Regular_10;
	prlHeader[1]->Text = CString("");

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 46; 
	prlHeader[2]->Font = &ogCourier_Regular_10;
	prlHeader[2]->Text = CString("");

	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 46; 
	prlHeader[3]->Font = &ogCourier_Regular_10;
	prlHeader[3]->Text = CString("");

	for(ili = 0; ili < 4; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}
	pomDCinsTable->SetHeaderFields(omHeaderDataArray);
	pomDCinsTable->SetDefaultSeparator();
	omHeaderDataArray.DeleteAll();
	pomDCinsTable->DisplayTable();

	CCS_CATCH_ALL

}


BOOL CSeasonDlg::DestroyWindow() 
{

	BOOL blRet;
	CCS_TRY

	blRet =  CDialog::DestroyWindow();
	if(blRet)
	{
		delete this;
		pogSeasonDlg = NULL;
	}


	return blRet;

	CCS_CATCH_ALL
	return blRet;

}




void CSeasonDlg::OnAAlc3LIST() 
{

	CCS_TRY
			
	//AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ACR","REGN,ACT3,ACT5", "REGN+");

	//polDlg->DoModal();


	CString olText;
	m_CE_AAlc3.GetWindowText(olText);


	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT","ALC2,ALC3,ALFN", "ALFN+,ALC2+", olText);
	polDlg->SetSecState("SEASONDLG_CE_AAlc3");
	if(polDlg->DoModal() == IDOK)
	{
		omAAlc2 = polDlg->GetField("ALC2");	
		omAAlc3 = polDlg->GetField("ALC3");	
		if(omAAlc2.IsEmpty())
			m_CE_AAlc3.SetInitText(omAAlc3, true);	
		else
			m_CE_AAlc3.SetInitText(omAAlc2, true);	

	}
	delete polDlg;

	CCS_CATCH_ALL

}


void CSeasonDlg::OnDalc3list() 
{

	CCS_TRY

	CString olText;
	m_CE_DAlc3.GetWindowText(olText);


	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT","ALC2,ALC3,ALFN", "ALFN+,ALC2+", olText);
	polDlg->SetSecState("SEASONDLG_CE_DAlc3");
	if(polDlg->DoModal() == IDOK)
	{
		omDAlc2 = polDlg->GetField("ALC2");	
		omDAlc3 = polDlg->GetField("ALC3");	
		if(omDAlc2.IsEmpty())
			m_CE_DAlc3.SetInitText(omDAlc3, true);	
		else
			m_CE_DAlc3.SetInitText(omDAlc2, true);	
	}
	delete polDlg;

CCS_CATCH_ALL

}



void CSeasonDlg::OnAblt1list() 
{

	CCS_TRY

	CString olText;
	m_CE_ABlt1.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "BLT","BNAM,TERM", "BNAM+,TERM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_ABlt1");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_ABlt1.SetInitText(polDlg->GetField("BNAM"), true);	
		m_CE_ATmb1.SetInitText(polDlg->GetField("TERM"), true);	
		m_CE_ABlt1.SetFocus();
	}
	delete polDlg;

	CCS_CATCH_ALL

}


void CSeasonDlg::OnAblt2list() 
{
	
	CCS_TRY

	CString olText;
	m_CE_ABlt2.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "BLT","BNAM,TERM", "BNAM+,TERM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_ABlt2");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_ABlt2.SetInitText(polDlg->GetField("BNAM"), true);	
		m_CE_ATmb2.SetInitText(polDlg->GetField("TERM"), true);	
		m_CE_ABlt2.SetFocus();
	}
	delete polDlg;

	CCS_CATCH_ALL

}



void CSeasonDlg::OnAct35list() 
{

	CCS_TRY

	CString olText;
	m_CE_Act5.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ACT","ACT3,ACT5,ACFN", "ACFN+,ACT3+", olText);
	polDlg->SetSecState("SEASONDLG_CE_Act3");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_Act3.SetInitText(polDlg->GetField("ACT3"), true);	
		m_CE_Act5.SetInitText(polDlg->GetField("ACT5"), true);	
		m_CE_Act3.SetFocus();
	}
	delete polDlg;

	CCS_CATCH_ALL	

}



void CSeasonDlg::OnAext1list() 
{

	CCS_TRY

	CString olText;
	m_CE_AExt1.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "EXT","ENAM,TERM", "ENAM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_AExt1");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_AExt1.SetInitText(polDlg->GetField("ENAM"), true);	
		m_CE_ATet1.SetInitText(polDlg->GetField("TERM"), true);	
	}
	delete polDlg;

	CCS_CATCH_ALL

}


void CSeasonDlg::OnAext2list() 
{

	CCS_TRY

	CString olText;
	m_CE_AExt2.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "EXT","ENAM,TERM", "ENAM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_AExt2");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_AExt2.SetInitText(polDlg->GetField("ENAM"), true);	
		m_CE_ATet2.SetInitText(polDlg->GetField("TERM"), true);	
	}
	delete polDlg;

	CCS_CATCH_ALL

}

void CSeasonDlg::OnAgta1list() 
{

	CCS_TRY

	CString olText;
	m_CE_AGta1.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "GAT","GNAM,TERM", "GNAM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_AGta1");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_AGta1.SetInitText(polDlg->GetField("GNAM"), true);	
		m_CE_ATga1.SetInitText(polDlg->GetField("TERM"), true);	
		//m_CE_AGta1.SetFocus();
	}
	delete polDlg;

	CCS_CATCH_ALL

}

void CSeasonDlg::OnAgta2list() 
{

	CCS_TRY

	CString olText;
	m_CE_AGta2.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "GAT","GNAM,TERM", "GNAM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_AGta2");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_AGta2.SetInitText(polDlg->GetField("GNAM"), true);	
		m_CE_ATga2.SetInitText(polDlg->GetField("TERM"), true);	
		m_CE_AGta2.SetFocus();
	}
	delete polDlg;

	CCS_CATCH_ALL

}



void CSeasonDlg::OnAorg3list() 
{

	CCS_TRY

	CString olText;
	m_CE_AOrg3.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "APT","APC3,APC4,APFN", "APFN+", olText);
	polDlg->SetSecState("SEASONDLG_CE_AOrg4");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_AOrg3.SetInitText(polDlg->GetField("APC3"), true);	
		m_CE_AOrg4.SetInitText(polDlg->GetField("APC4"), true);	
	}
	delete polDlg;

	CCS_CATCH_ALL

}



void CSeasonDlg::OnApstalist() 
{

	CCS_TRY

	CString olText;
	m_CE_APsta.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "PST","PNAM", "PNAM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_APsta");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_APsta.SetInitText(polDlg->GetField("PNAM"), true);	
		m_CE_APsta.SetFocus();
	}
	delete polDlg;

	CCS_CATCH_ALL
}



void CSeasonDlg::OnDdes3list() 
{

	CCS_TRY

	CString olText;
	m_CE_DDes3.GetWindowText(olText);


	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "APT","APC3,APC4,APFN", "APFN+", olText);
	polDlg->SetSecState("SEASONDLG_CE_DDes4");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DDes3.SetInitText(polDlg->GetField("APC3"), true);	
		m_CE_DDes4.SetInitText(polDlg->GetField("APC4"), true);	
	}
	delete polDlg;

	CCS_CATCH_ALL

}


void CSeasonDlg::OnDgtd1list() 
{

	CCS_TRY

	CString olText;
	m_CE_DGtd1.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "GAT","GNAM,TERM", "GNAM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_DGtd1");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DGtd1.SetInitText(polDlg->GetField("GNAM"), true);	
		m_CE_DTgd1.SetInitText(polDlg->GetField("TERM"), true);	
		m_CE_DGtd1.SetFocus();
	}
	delete polDlg;

	CCS_CATCH_ALL

}

void CSeasonDlg::OnDgtd2list() 
{

	CCS_TRY

	CString olText;
	m_CE_DGtd2.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "GAT","GNAM,TERM", "GNAM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_DGtd2");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DGtd2.SetInitText(polDlg->GetField("GNAM"), true);	
		m_CE_DTgd2.SetInitText(polDlg->GetField("TERM"), true);	
		m_CE_DGtd2.SetFocus();
	}
	delete polDlg;

	CCS_CATCH_ALL

}


void CSeasonDlg::OnDpstdlist() 
{

	CCS_TRY

	CString olText;
	m_CE_DPstd.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "PST","PNAM", "PNAM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_DPstd");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DPstd.SetInitText(polDlg->GetField("PNAM"), true);	
		m_CE_DPstd.SetFocus();
	}
	delete polDlg;

	CCS_CATCH_ALL
}



void CSeasonDlg::OnDwro1list() 
{

	CCS_TRY

	CString olText;
	m_CE_DWro1.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "WRO","WNAM,TERM", "WNAM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_DWro1");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DWro1.SetInitText(polDlg->GetField("WNAM"), true);	
		m_CE_DTwr1.SetInitText(polDlg->GetField("TERM"), true);	
		m_CE_DWro1.SetFocus();
	}
	delete polDlg;

	CCS_CATCH_ALL

}

void CSeasonDlg::OnDwro2list() 
{

	CCS_TRY

	CString olText;
	m_CE_DWro2.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "WRO","WNAM,TERM", "WNAM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_DWro2");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DWro2.SetInitText(polDlg->GetField("WNAM"), true);	
		m_CE_DTwr2.SetInitText(polDlg->GetField("TERM"), true);	
		m_CE_DWro2.SetFocus();
	}
	delete polDlg;

	CCS_CATCH_ALL

}

void CSeasonDlg::OnDttyplist() 
{

	CCS_TRY

	CString olText;
	m_CE_DTtyp.GetWindowText(olText);
 
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "NAT","TTYP,TNAM", "TNAM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_DTtyp");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DTtyp.SetInitText(polDlg->GetField("TTYP"), true);	
	}
	delete polDlg;

	CCS_CATCH_ALL

}




void CSeasonDlg::OnAttyplist() 
{

	CCS_TRY

	CString olText;
	m_CE_ATtyp.GetWindowText(olText);
 
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "NAT","TTYP,TNAM", "TNAM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_ATtyp");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_ATtyp.SetInitText(polDlg->GetField("TTYP"), true);	
	}
	delete polDlg;

	CCS_CATCH_ALL

}




void CSeasonDlg::OnDstyplist() 
{

	CCS_TRY

	CString olText;
	m_CE_DStyp.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "STY","STYP,SNAM", "SNAM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_DStyp");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DStyp.SetInitText(polDlg->GetField("STYP"), true);	
	}
	delete polDlg;

	CCS_CATCH_ALL

}



void CSeasonDlg::OnAstyplist() 
{

	CCS_TRY

	CString olText;
	m_CE_AStyp.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "STY","STYP,SNAM", "SNAM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_AStyp");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_AStyp.SetInitText(polDlg->GetField("STYP"), true);	
	}
	delete polDlg;

	CCS_CATCH_ALL

}


/*
void CSeasonDlg::OnAhtyplist() 
{

	CCS_TRY

	CString olText;
	m_CE_AHtyp.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "HTY","HTYP,HNAM", "HNAM+", olText);
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_AHtyp.SetInitText(polDlg->GetField("HTYP"), true);	
	}
	delete polDlg;

	CCS_CATCH_ALL

}




void CSeasonDlg::OnDhtyplist() 
{

	CCS_TRY

	CString olText;
	m_CE_DHtyp.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "HTY","HTYP,HNAM", "HNAM+", olText);
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DHtyp.SetInitText(polDlg->GetField("HTYP"), true);	
	}
	delete polDlg;

	CCS_CATCH_ALL

}

*/


/////////////////////////////////////////////////////////////////////////////////
//Tables !!





void CSeasonDlg::OnAAlc3LIST2() 
{

	CCS_TRY

	int ilLine = pomAJfnoTable->pomListBox->GetTopIndex();
		
	CString olText;
	pomAJfnoTable->GetTextFieldValue(ilLine, 0, olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT","ALC2,ALC3,ALFN", "ALFN+", olText);
	polDlg->SetSecState("SEASONDLG_CB_AShowJfno");
	if(polDlg->DoModal() == IDOK)
	{
		pomAJfnoTable->SetIPValue(ilLine, 0, polDlg->GetField("ALC2"));
	}
	delete polDlg;

	CCS_CATCH_ALL

}

void CSeasonDlg::OnAAlc3LIST3() 
{

	CCS_TRY
	
	int ilLine = pomAJfnoTable->pomListBox->GetTopIndex() + 1;

	CString olText;
	pomAJfnoTable->GetTextFieldValue(ilLine, 0, olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT","ALC2,ALC3,ALFN", "ALFN+", olText);
	polDlg->SetSecState("SEASONDLG_CB_AShowJfno");
	if(polDlg->DoModal() == IDOK)
	{
		pomAJfnoTable->SetIPValue(ilLine, 0, polDlg->GetField("ALC2"));
	}
	delete polDlg;

	CCS_CATCH_ALL

}

void CSeasonDlg::OnDalc3list2() 
{

	CCS_TRY

	int ilLine = pomDJfnoTable->pomListBox->GetTopIndex();

	CString olText;
	pomDJfnoTable->GetTextFieldValue(ilLine, 0, olText);
	
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT","ALC2,ALC3,ALFN", "ALFN+", olText);
	polDlg->SetSecState("SEASONDLG_CB_DShowJfno");
	if(polDlg->DoModal() == IDOK)
	{
		pomDJfnoTable->SetIPValue(ilLine, 0, polDlg->GetField("ALC2"));
	}
	delete polDlg;

	CCS_CATCH_ALL
	
}

void CSeasonDlg::OnDalc3list3() 
{

	CCS_TRY

	int ilLine = pomDJfnoTable->pomListBox->GetTopIndex() + 1;

	CString olText;
	pomDJfnoTable->GetTextFieldValue(ilLine, 0, olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT","ALC2,ALC3,ALFN", "ALFN+", olText);
	polDlg->SetSecState("SEASONDLG_CB_DShowJfno");
	if(polDlg->DoModal() == IDOK)
	{
		pomDJfnoTable->SetIPValue(ilLine, 0, polDlg->GetField("ALC2"));
	}
	delete polDlg;

	CCS_CATCH_ALL

}



void CSeasonDlg::OnDcinslist1() 
{

	CCS_TRY

	int ilLine = pomDCinsTable->pomListBox->GetTopIndex();

	CString olText;
	pomDCinsTable->GetTextFieldValue(ilLine, 0, olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "CIC","CNAM,TERM", "CNAM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_DCins");
	if(polDlg->DoModal() == IDOK)
	{
		//	reject a new checkin-counter if counter is already open
		if (CcaHasNoOpentime (ilLine) == 0L)
		{
			pomDCinsTable->SetIPValue(ilLine, 0, polDlg->GetField("CNAM"));
			pomDCinsTable->SetIPValue(ilLine, 1, polDlg->GetField("TERM"));
		}
	}
	delete polDlg;

	CCS_CATCH_ALL

}

void CSeasonDlg::OnDcinslist2() 
{

	CCS_TRY

	int ilLine = pomDCinsTable->pomListBox->GetTopIndex() + 1;

	CString olText;
	pomDCinsTable->GetTextFieldValue(ilLine, 0, olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "CIC","CNAM,TERM", "CNAM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_DCins");
	if(polDlg->DoModal() == IDOK)
	{
		//	reject a new checkin-counter if counter is already open
		if (CcaHasNoOpentime (ilLine) == 0L)
		{
			pomDCinsTable->SetIPValue(ilLine, 0, polDlg->GetField("CNAM"));
			pomDCinsTable->SetIPValue(ilLine, 1, polDlg->GetField("TERM"));
		}
	}
	delete polDlg;

	CCS_CATCH_ALL

}

void CSeasonDlg::OnDcinslist3() 
{

	CCS_TRY

	int ilLine = pomDCinsTable->pomListBox->GetTopIndex() + 2;

	CString olText;
	pomDCinsTable->GetTextFieldValue(ilLine, 0, olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "CIC","CNAM,TERM", "CNAM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_DCins");
	if(polDlg->DoModal() == IDOK)
	{
		//	reject a new checkin-counter if counter is already open
		if (CcaHasNoOpentime (ilLine) == 0L)
		{
			pomDCinsTable->SetIPValue(ilLine, 0, polDlg->GetField("CNAM"));
			pomDCinsTable->SetIPValue(ilLine, 1, polDlg->GetField("TERM"));
		}
	}
	delete polDlg;

	CCS_CATCH_ALL

}

void CSeasonDlg::OnDcinslist4() 
{

	CCS_TRY

	int ilLine = pomDCinsTable->pomListBox->GetTopIndex() + 3;

	CString olText;
	pomDCinsTable->GetTextFieldValue(ilLine, 0, olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "CIC","CNAM,TERM", "CNAM+", olText);
	polDlg->SetSecState("SEASONDLG_CE_DCins");
	if(polDlg->DoModal() == IDOK)
	{
		//	reject a new checkin-counter if counter is already open
		if (CcaHasNoOpentime (ilLine) == 0L)
		{
			pomDCinsTable->SetIPValue(ilLine, 0, polDlg->GetField("CNAM"));
			pomDCinsTable->SetIPValue(ilLine, 1, polDlg->GetField("TERM"));
		}
	}
	delete polDlg;

	CCS_CATCH_ALL

}








void CSeasonDlg::OnDcinsmal4() 
{

	CCS_TRY

	int i = pomDCinsTable->pomListBox->GetTopIndex();

	CString olCkic;
	CString olCkit;
	CString olCkbs;
	CString olCkes;

	pomDCinsTable->GetTextFieldValue(i, 0, olCkic);
	pomDCinsTable->GetTextFieldValue(i, 1, olCkit);
	pomDCinsTable->GetTextFieldValue(i, 2, olCkbs);
	pomDCinsTable->GetTextFieldValue(i, 3, olCkes);


	ogBCD.SetSort("CIC","CNAM+",true);

	int ilCount = ogBCD.GetDataCount("CIC");

	CString  olCnam;

	for(int j = 0; j < ilCount; j++)
	{
		olCnam = ogBCD.GetField("CIC",j,"CNAM");

		if(olCnam == olCkic)
			break;
	}

	if (j == ilCount)
	{
		pomDCinsTable->SetIPValue(i + 1, 0, "");
		pomDCinsTable->SetIPValue(i + 1, 1, "");
		pomDCinsTable->SetIPValue(i + 2, 0, "");
		pomDCinsTable->SetIPValue(i + 2, 1, "");
		pomDCinsTable->SetIPValue(i + 3, 0, "");
		pomDCinsTable->SetIPValue(i + 3, 1, "");
	}
	else
	{
		pomDCinsTable->SetIPValue(i + 1, 0, ogBCD.GetField("CIC",j + 1,"CNAM"));
		pomDCinsTable->SetIPValue(i + 1, 1, ogBCD.GetField("CIC",j + 1,"TERM"));
		pomDCinsTable->SetIPValue(i + 2, 0, ogBCD.GetField("CIC",j + 2,"CNAM"));
		pomDCinsTable->SetIPValue(i + 2, 1, ogBCD.GetField("CIC",j + 2,"TERM"));
		pomDCinsTable->SetIPValue(i + 3, 0, ogBCD.GetField("CIC",j + 3,"CNAM"));
		pomDCinsTable->SetIPValue(i + 3, 1, ogBCD.GetField("CIC",j + 3,"TERM"));
	}

	pomDCinsTable->SetIPValue(i + 1, 2, olCkbs);
	pomDCinsTable->SetIPValue(i + 1, 3, olCkes);

	pomDCinsTable->SetIPValue(i + 2, 2, olCkbs);
	pomDCinsTable->SetIPValue(i + 2, 3, olCkes);
	
	pomDCinsTable->SetIPValue(i + 3, 2, olCkbs);
	pomDCinsTable->SetIPValue(i + 3, 3, olCkes);

	CCS_CATCH_ALL

}

void CSeasonDlg::ProcessRotationChange(SEASONDLGFLIGHTDATA *prpFlight)
{
	if (prpFlight)
		NewData(this, prpFlight->Rkey, prpFlight->Urno, imModus, bmLocalTime, prpFlight->Adid);

	return;
}


void CSeasonDlg::ProcessRotationChange()
{

	CCS_TRY

	SEASONDLGFLIGHTDATA *prlFlight;
	SEASONDLGFLIGHTDATA *prlFlight2;


	prlFlight = new SEASONDLGFLIGHTDATA;

	*prmAFlight		= *prlFlight;
	*prmAFlightSave = *prlFlight;
	*prmDFlight		= *prlFlight;
	*prmDFlightSave = *prlFlight;
	delete prlFlight;


	if((prlFlight = ogSeasonDlgFlights.GetFlightByUrno(lmCalledBy)) == NULL)
	{
		//InitDialog(true, true);
	}
	else
	{

		if((strcmp(prlFlight->Des4, pcgHome4) == 0) && (!((strcmp(prlFlight->Org4, pcgHome4) == 0) && omAdid == "D")))
		{
			*prmAFlight		= *prlFlight;
			*prmAFlightSave = *prlFlight;
			if((prlFlight2 = ogSeasonDlgFlights.GetDeparture(prlFlight)) != NULL)
			{
				*prmDFlight		= *prlFlight2;
				*prmDFlightSave = *prlFlight2;
			}
		}
		else
		{
			*prmDFlight		= *prlFlight;
			*prmDFlightSave = *prlFlight;
			if((prlFlight2 = ogSeasonDlgFlights.GetArrival(prlFlight)) != NULL)
			{
				*prmAFlight		= *prlFlight2;
				*prmAFlightSave = *prlFlight2;
			}
		}

		/*


		if(strcmp(prlFlight->Des4, pcgHome4) == 0)
		{
			*prmAFlight		= *prlFlight;
			*prmAFlightSave = *prlFlight;
			if((prlFlight2 = ogSeasonDlgFlights.GetDeparture(prlFlight)) != NULL)
			{
				*prmDFlight		= *prlFlight2;
				*prmDFlightSave = *prlFlight2;
			}
		}
		else
		{
			*prmDFlight		= *prlFlight;
			*prmDFlightSave = *prlFlight;
			if((prlFlight2 = ogSeasonDlgFlights.GetArrival(prlFlight)) != NULL)
			{
				*prmAFlight		= *prlFlight2;
				*prmAFlightSave = *prlFlight2;
			}
		}
		*/
	}
	InitDialog(true, true);

	ReadCcaData();
	DShowCinsTable();

	CCS_CATCH_ALL

}


void CSeasonDlg::SetSecState()
{

	CCS_TRY
	// Global
	
		m_CB_Ok.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_OK"));
		m_CB_PrevFlight.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_ZURUECK"));
		m_CB_NextFlight.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_WEITER"));
		m_CB_AReset.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_ARESET"));
		m_CB_DReset.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DRESET"));
		m_CB_DailyMask.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DailyMask"));
		m_CB_Agents.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_Agents"));
		m_CE_Seas.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_Seas"));		
		m_CE_Ming.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_Ming"));		
		m_CE_Pbis.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_Pbis"));		
		m_CE_Pvom.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_Pvom"));		
		m_CE_Freq.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_Freq"));		
		m_CE_Act3.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_Act3"));		
		m_CE_Regn.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_Regn"));		

	// arrival


		m_CB_ADaly.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_ADaly"));
		m_CB_AShowJfno.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_AShowJfno"));
		m_CB_AAlc3List.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_AAlc3List"));
		m_CB_AAlc3List2.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_AAlc3List23"));
		m_CB_AAlc3List3.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_AAlc3List23"));
		m_CB_AStatus.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_AStatus"));
		m_CB_AStatus2.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_AStatus2"));
		m_CB_AStatus3.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_AStatus3"));
		m_CB_AOrg3List.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_AOrg3List"));
		m_CB_Act35List.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_Act35List"));
		m_CB_ATTypList.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_ATTypList"));
		m_CB_AStypList.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_AStypList"));
//		m_CB_AHtypList.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_AHtypList"));
		m_CB_APstaList.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_APstaList"));
		m_CB_AGta2List.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_AGta2List"));
		m_CB_AGta1List.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_AGta1List"));
		m_CB_AExt2List.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_AExt2List"));
		m_CB_AExt1List.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_AExt1List"));
		m_CB_ABlt2List.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_ABlt2List"));
		m_CB_ABlt1List.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_ABlt1List"));
		m_CB_ANoop.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_ANoop"));
		m_CB_ACxx.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_ACxx"));
		m_CE_ATmb2.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ATmb2"));	
		m_CE_ALastChange.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ALastChange"));	
		m_CE_AFluko.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AFluko"));		
		m_CE_ACreated.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ACreated"));	
		m_CE_AExt2.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AExt2"));		
		m_CE_AExt1.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AExt1"));		
		m_CE_ARem1.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ARem1"));		
		m_CE_ARem2.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ARem1"));		
		m_CE_Act5.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_Act5"));		
		m_CE_ABlt2.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ABlt2"));		
		m_CE_ABlt1.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ABlt1"));		
		m_CE_AEtai.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AEtai"));		
		m_CE_AGta2.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AGta2"));		
		m_CE_AGta1.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AGta1"));		
		m_CE_AStoa.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AStoa"));		
		m_CE_AStod.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AStod"));		
		m_CE_AOrg3.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AOrg3"));		
		m_CE_AOrg4.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AOrg4"));		
		m_CE_AStev.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AStev"));		
		m_CE_AStyp.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AStyp"));		
//		m_CE_AHtyp.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AHtyp"));		
		m_CE_ATtyp.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ATtyp"));		
		m_CE_AFlns.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AFlns"));		
		m_CE_AFltn.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AFltn"));		
		m_CE_AFlti.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AFlti"));		
		m_CE_AAlc3.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AAlc3"));		
		m_CE_ADes3.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ADes3"));		
		m_CE_ADays.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ADays"));
		m_CE_APsta.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_APsta"));
		m_CE_APabs.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_APabs"));
		m_CE_APaes.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_APaes"));
		m_CE_ATet1.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ATet1"));
		m_CE_ATet2.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ATet2"));
		m_CE_AGa1b.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AGa1b"));
		m_CE_AGa2b.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AGa2b"));
		m_CE_AGa1e.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AGa1e"));
		m_CE_AGa2e.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AGa2e"));
		m_CE_ATmb1.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ATmb1"));
		m_CE_AB1bs.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AB1bs"));
		m_CE_AB2bs.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AB2bs"));
		m_CE_AB1es.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AB1es"));
		m_CE_AB2es.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_AB2es"));
		m_CE_ATga1.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ATga1"));
		m_CE_ATga2.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ATga2"));
		m_CE_ACsgn.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_ACsgn"));



	// departure

		m_CB_DDaly.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DDaly"));
		m_CB_DStatus.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DStatus"));
		m_CB_DShowJfno.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DShowJfno"));
		m_CB_DStatus2.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DStatus2"));
		m_CB_DStatus3.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DStatus3"));
		m_CB_DDes3List.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DDes3List"));
		m_CB_DAlc3List.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DAlc3List"));
		m_CB_DAlc3List2.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DAlc3List23"));
		m_CB_DAlc3List3.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DAlc3List23"));
		m_CB_DTtypList.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DTtypList"));
		m_CB_DStypList.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DStypList"));
//		m_CB_DHtypList.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DHtypList"));
		m_CB_DWro1List.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DWro1List"));
		m_CB_DWro2List.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DWro2List"));
		m_CB_DPstdList.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DPstdList"));
		m_CB_DGtd2List.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DGtd2List"));
		m_CB_DGtd1List.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DGtd1List"));
		m_CB_DCinsList1.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DCinsList"));
		m_CB_DCinsList2.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DCinsList"));
		m_CB_DCinsList3.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DCinsList"));
		m_CB_DCinsList4.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DCinsList"));
		m_CB_DCinsMal4.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DCinsList"));
		m_CB_DCxx.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DCxx"));
		m_CB_DNoop.SetSecState(ogPrivList.GetStat("SEASONDLG_CB_DNoop"));
		m_CE_DLastChange.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DLastChange"));	
		m_CE_DFluko.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DFluko"));		
		m_CE_DCreated.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DCreated"));	
		m_CE_DAlc3.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DAlc3"));		
		m_CE_DRem1.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DRem1"));		
		m_CE_DRem2.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DRem1"));		
		m_CE_DGtd2.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DGtd2"));		
		m_CE_DGtd1.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DGtd1"));		
//		m_CE_DHtyp.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DHtyp"));		
		m_CE_DStev.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DStev"));		
		m_CE_DTtyp.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DTtyp"));		
		m_CE_DStyp.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DStyp"));		
		m_CE_DFlns.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DFlns"));		
		m_CE_DFltn.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DFltn"));		
		m_CE_DFlti.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DFlti"));		
		m_CE_DStod.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DStod"));		
		m_CE_DStoa.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DStoa"));		
		m_CE_DEtdi.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DEtdi"));		
		m_CE_DDes3.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DDes3"));		
		m_CE_DDes4.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DDes4"));		
		m_CE_DOrg3.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DOrg3"));		
		m_CE_DDays.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DDays"));
		m_CE_DPstd.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DPstd"));
		m_CE_DPdbs.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DPdbs"));
		m_CE_DPdes.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DPdes"));
		m_CE_DTgd1.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DTgd1"));
		m_CE_DTgd2.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DTgd2"));
		m_CE_DGd1b.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DGd1b"));
		m_CE_DGd2b.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DGd2b"));
		m_CE_DGd1e.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DGd1e"));
		m_CE_DGd2e.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DGd2e"));
		m_CE_DWro1.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DWro1"));
		m_CE_DWro2.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DWro2"));
		m_CE_DTwr1.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DTwr1"));
		m_CE_DTwr2.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DTwr2"));
		m_CE_DW1bs.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DW1bs"));
		m_CE_DW1es.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DW1es"));
		m_CE_DW2bs.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DW2bs"));
		m_CE_DW2es.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DW2es"));

		m_CE_DBaz1.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DBaz1"));
		m_CE_DBaz4.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DBaz4"));

		m_CE_DCsgn.SetSecState(ogPrivList.GetStat("SEASONDLG_CE_DCsgn"));

		if (!bgFlightIDEditable)
		{
			// enable editing of flight id
//			m_CE_DFlti.SetSecState('0');
//			m_CE_AFlti.SetSecState('0');
		}

	CCS_CATCH_ALL

}



void CSeasonDlg::EnableGatPos()
{
	if(imModus != DLG_CHANGE_DIADATA) return;
	
	
	bool blAEnable = false;
	bool blDEnable = false;

	pomAViaCtrl->pomTable->SetTableEditable(false);
	pomDViaCtrl->pomTable->SetTableEditable(false);
//	pomAViaCtrl->pomTable->ShowWindow(SW_HIDE);
	pomDViaCtrl->Enable(false);
	pomAViaCtrl->Enable(false);

	if(prmAFlight->Urno > 0)  
		blAEnable = TRUE;

	// Arrival
	m_CB_APstaList.EnableWindow(blAEnable);
	m_CB_AGta2List.EnableWindow(blAEnable);
	m_CB_AGta1List.EnableWindow(blAEnable);
 	m_CB_ABlt2List.EnableWindow(blAEnable);
	m_CB_ABlt1List.EnableWindow(blAEnable);
 
	m_CE_ABlt2.EnableWindow(blAEnable);
	m_CE_ATmb2.EnableWindow(blAEnable);
	m_CE_ABlt1.EnableWindow(blAEnable);
	m_CE_ATmb1.EnableWindow(blAEnable);
 	m_CE_AGta2.EnableWindow(blAEnable);
	m_CE_ATga2.EnableWindow(blAEnable);
	m_CE_AGta1.EnableWindow(blAEnable);
	m_CE_ATga1.EnableWindow(blAEnable);
 	m_CE_AB1bs.EnableWindow(blAEnable);
	m_CE_AB1es.EnableWindow(blAEnable);
	m_CE_AB2bs.EnableWindow(blAEnable);
	m_CE_AB2es.EnableWindow(blAEnable);
	m_CE_AGa1b.EnableWindow(blAEnable);
	m_CE_AGa1e.EnableWindow(blAEnable);
	m_CE_AGa2b.EnableWindow(blAEnable);
	m_CE_AGa2e.EnableWindow(blAEnable);
	m_CE_APsta.EnableWindow(blAEnable);
	m_CE_APabs.EnableWindow(blAEnable);
	m_CE_APaes.EnableWindow(blAEnable);

	m_CB_AExt2List.EnableWindow(blAEnable);
	m_CB_AExt1List.EnableWindow(blAEnable);
	m_CB_AExt2List.EnableWindow(blAEnable);
	m_CB_AExt1List.EnableWindow(blAEnable);
	m_CE_AExt2.EnableWindow(blAEnable);
	m_CE_AExt1.EnableWindow(blAEnable);
	m_CE_ATet2.EnableWindow(blAEnable);
	m_CE_ATet1.EnableWindow(blAEnable);

	// Departure
	if(prmDFlight->Urno > 0)
		blDEnable = TRUE;

	m_CB_DWro1List.EnableWindow(blDEnable);
	m_CB_DWro2List.EnableWindow(blDEnable);
	m_CB_DPstdList.EnableWindow(blDEnable);
	m_CB_DGtd2List.EnableWindow(blDEnable);
	m_CB_DGtd1List.EnableWindow(blDEnable);

	m_CE_DGtd2.EnableWindow(blDEnable);
	//m_CE_DTgd2.EnableWindow(blDEnable);
	m_CE_DGtd1.EnableWindow(blDEnable);
	//m_CE_DTgd1.EnableWindow(blDEnable);
 	m_CE_DGd1b.EnableWindow(blDEnable);
	m_CE_DGd1e.EnableWindow(blDEnable);
	m_CE_DGd2b.EnableWindow(blDEnable);
	m_CE_DGd2e.EnableWindow(blDEnable);
	m_CE_DW1bs.EnableWindow(blDEnable);
	m_CE_DW1es.EnableWindow(blDEnable);
	m_CE_DW2bs.EnableWindow(blDEnable);
	m_CE_DW2es.EnableWindow(blDEnable);
	m_CE_DWro1.EnableWindow(blDEnable);
	m_CE_DWro2.EnableWindow(blDEnable);
	//m_CE_DTwr1.EnableWindow(blDEnable);
	m_CE_DPstd.EnableWindow(blDEnable);
	m_CE_DPdbs.EnableWindow(blDEnable);
	m_CE_DPdes.EnableWindow(blDEnable);
//###
	if (blDEnable)
	{
		pomDCinsTable->ShowWindow(SW_SHOW);
		pomDCinsTable->SetTableEditable(false);
		//SetpWndStatAll(ogPrivList.GetStat("SEASONDLG_CE_DCins"),pomDCinsTable);
	}
	m_CB_DCinsMal4.EnableWindow(false);
	m_CE_DBaz1.EnableWindow(blDEnable);
	m_CE_DBaz4.EnableWindow(blDEnable);

	m_CE_DTwr1.EnableWindow(blDEnable);
	m_CE_DTwr2.EnableWindow(blDEnable);
	m_CE_DTgd1.EnableWindow(blDEnable);
	m_CE_DTgd2.EnableWindow(blDEnable);

	m_CB_DCinsList4.EnableWindow(false);
	m_CB_DCinsList3.EnableWindow(false);
	m_CB_DCinsList2.EnableWindow(false);
	m_CB_DCinsList1.EnableWindow(false);

//####
	// Global
 	m_CB_Ok.EnableWindow(blAEnable || blDEnable);
	m_CB_AReset.EnableWindow(FALSE);
	m_CB_DReset.EnableWindow(FALSE);
	m_CB_DailyMask.EnableWindow(FALSE);
		
	m_CB_DDaly.EnableWindow(FALSE);
	m_CB_ADaly.EnableWindow(FALSE);
		
	m_CE_DDays.SetReadOnly();
	m_CE_ADays.SetReadOnly();
		
	m_CE_Pvom.SetReadOnly();
	m_CE_Pbis.SetReadOnly();
	m_CE_Freq.SetReadOnly();

	return;
}






void CSeasonDlg::EnableArrival()
{

	CCS_TRY

	BOOL blEnable = FALSE;

	char clStat;

	if(imModus != DLG_CHANGE_DIADATA && ((prmAFlight->Urno > 0) || (imModus != DLG_CHANGE)))  
		blEnable = TRUE;


	
	if(!blEnable)
	{
		m_CE_Freq.EnableWindow(blEnable);
		AShowJfnoButton();
	}



	if( CString(prmAFlight->Ftyp)	== "Z" || CString(prmAFlight->Ftyp)	== "B")
	{
		pomAViaCtrl->Enable(FALSE);
		m_CE_ARem1.EnableWindow(FALSE);
	}
	else
	{
		pomAViaCtrl->Enable(blEnable);
		m_CE_ARem1.EnableWindow(blEnable);
	}

	clStat = ogPrivList.GetStat("SEASONDLG_CL_ARemp");

	m_CB_ARemp.ShowWindow(SW_SHOW);

	if(clStat == '1')
	{
		m_CB_ARemp.EnableWindow(blEnable);
	}
	else
	{
		if(clStat == '0')
		{
			m_CB_ARemp.EnableWindow(FALSE);
		}
		else
		{
			m_CB_ARemp.ShowWindow(SW_HIDE);
		}

	}



	m_CE_ACsgn.EnableWindow(blEnable);
	m_CB_AOrg3List.EnableWindow(blEnable);
	m_CB_ATTypList.EnableWindow(blEnable);
	m_CB_AStypList.EnableWindow(blEnable);
//	m_CB_AHtypList.EnableWindow(blEnable);
	m_CE_ATmb2.EnableWindow(blEnable);
	m_CB_APstaList.EnableWindow(blEnable);
	m_CB_AGta2List.EnableWindow(blEnable);
	m_CB_AGta1List.EnableWindow(blEnable);
	m_CB_AExt2List.EnableWindow(blEnable);
	m_CB_AExt1List.EnableWindow(blEnable);
	m_CB_ABlt2List.EnableWindow(blEnable);
	m_CB_ABlt1List.EnableWindow(blEnable);
	m_CE_ALastChange.EnableWindow(blEnable);
	m_CE_AFluko.EnableWindow(blEnable);
	m_CB_ADaly.EnableWindow(blEnable);
	m_CE_ACreated.EnableWindow(blEnable);
	m_CB_AAlc3List.EnableWindow(blEnable);

	m_CB_AStatus.EnableWindow(blEnable);
	m_CB_AStatus2.EnableWindow(blEnable);
	m_CB_AStatus3.EnableWindow(blEnable);
	m_CE_ARem2.EnableWindow(blEnable);
	m_CE_ACht3.EnableWindow(blEnable);
	m_CB_ANoop.EnableWindow(blEnable);
	m_CE_AExt2.EnableWindow(blEnable);
	m_CE_AExt1.EnableWindow(blEnable);
	m_CE_ADays.EnableWindow(blEnable);
	m_CB_ACxx.EnableWindow(blEnable);
	m_CE_ABlt2.EnableWindow(blEnable);
	m_CE_ABlt1.EnableWindow(blEnable);
	m_CE_AEtai.EnableWindow(blEnable);
	m_CE_Freq.EnableWindow(blEnable);
	m_CE_AGta2.EnableWindow(blEnable);
	m_CE_AGta1.EnableWindow(blEnable);
	m_CE_AStoa.EnableWindow(blEnable);
	m_CE_AStod.EnableWindow(blEnable);
	m_CE_AOrg4.EnableWindow(blEnable);
	m_CE_AOrg3.EnableWindow(blEnable);
	m_CE_AStev.EnableWindow(blEnable);
	m_CE_AStyp.EnableWindow(blEnable);
//	m_CE_AHtyp.EnableWindow(blEnable);
	m_CE_ATtyp.EnableWindow(blEnable);
	m_CE_AFlns.EnableWindow(blEnable);
	m_CE_AFltn.EnableWindow(blEnable);
	m_CE_AFlti.EnableWindow(blEnable);
	m_CE_AAlc3.EnableWindow(blEnable);
	m_CE_ADes3.EnableWindow(blEnable);
	m_CE_AB1bs.EnableWindow(blEnable);
	m_CE_AB1es.EnableWindow(blEnable);
	m_CE_AB2bs.EnableWindow(blEnable);
	m_CE_AB2es.EnableWindow(blEnable);
	m_CE_AGa1b.EnableWindow(blEnable);
	m_CE_AGa1e.EnableWindow(blEnable);
	m_CE_AGa2b.EnableWindow(blEnable);
	m_CE_AGa2e.EnableWindow(blEnable);
	m_CE_APsta.EnableWindow(blEnable);
	m_CE_ATet1.EnableWindow(blEnable);
	m_CE_ATet2.EnableWindow(blEnable);
	m_CE_ATga1.EnableWindow(blEnable);
	m_CE_ATga2.EnableWindow(blEnable);
	m_CE_ATmb1.EnableWindow(blEnable);
	m_CE_APabs.EnableWindow(blEnable);
	m_CE_APaes.EnableWindow(blEnable);


	if(imModus == DLG_CHANGE)
	{
		m_CB_ADaly.EnableWindow(FALSE);
		m_CE_ADays.EnableWindow(FALSE);
	}
	else
	{
		m_CB_ADaly.EnableWindow(blEnable);
		m_CE_ADays.EnableWindow(blEnable);
	}



	if(omAJfno.GetSize() > 0)
		OnAshowjfno();
	else
		AShowJfnoButton();

	m_CB_AShowJfno.EnableWindow(blEnable);


	if(( CString(prmAFlight->Ftyp)	== "Z" ) || (CString(prmAFlight->Ftyp)	== "B" ))
	{
		m_CE_AOrg3.EnableWindow(FALSE);	
		m_CE_AOrg4.EnableWindow(FALSE);	
		m_CB_AOrg3List.EnableWindow(FALSE);	
	}
	else
	{
		m_CE_AOrg3.EnableWindow(blEnable);	
		m_CE_AOrg4.EnableWindow(blEnable);	
		m_CB_AOrg3List.EnableWindow(blEnable);	
	}


	CCS_CATCH_ALL

}


void CSeasonDlg::EnableGlobal()
{

	CCS_TRY

	BOOL blEnable = FALSE;


	if(imModus != DLG_CHANGE_DIADATA &&  ((prmAFlight->Urno > 0) || (prmDFlight->Urno > 0)  || (imModus != DLG_CHANGE)))
		blEnable = TRUE;

	if(imModus == DLG_CHANGE)
		m_CE_Regn.SetReadOnly(false);
	else
		m_CE_Regn.SetReadOnly(true);

	m_CB_Act35List.EnableWindow(blEnable);
	m_CE_Act5.EnableWindow(blEnable);
	m_CE_Act3.EnableWindow(blEnable);
	m_CE_Ming.EnableWindow(blEnable);
	m_CE_Seas.EnableWindow(blEnable);
	m_CB_Ok.EnableWindow(blEnable);
	m_CE_Nose.EnableWindow(blEnable);

	if (ogPrivList.GetStat("SEASONDLG_CB_DailyMask") == '1')
		m_CB_DailyMask.EnableWindow(blEnable);
	else
		m_CB_DailyMask.EnableWindow(FALSE);


	// additional Text for caption
	CString olTimes;
	if (bmLocalTime)
		olTimes = GetString(IDS_STRING1921);
	else
		olTimes = GetString(IDS_STRING1920);


	if(imModus == DLG_CHANGE_DIADATA)
	{
		SetWindowText(GetString(IDS_STRING403)+" ("+olTimes+")");
	}
	else if(imModus == DLG_CHANGE)
	{
		SetWindowText(GetString(IDS_STRING403)+" ("+olTimes+")");

		m_CB_PrevFlight.EnableWindow(TRUE);
		m_CB_NextFlight.EnableWindow(TRUE);
		
		m_CB_AReset.EnableWindow(FALSE);
		m_CB_DReset.EnableWindow(FALSE);
		
		m_CB_DDaly.EnableWindow(FALSE);
		m_CB_ADaly.EnableWindow(FALSE);
		
		m_CE_DDays.SetReadOnly();
		m_CE_ADays.SetReadOnly();
		
		m_CE_Pvom.SetReadOnly();
		m_CE_Pbis.SetReadOnly();
		m_CE_Freq.SetReadOnly();
	}
	else
	{
		SetWindowText(GetString(IDS_STRING404)+" ("+olTimes+")");

		m_CB_PrevFlight.EnableWindow(FALSE);
		m_CB_NextFlight.EnableWindow(FALSE);
		
		m_CB_AReset.EnableWindow(TRUE);
		m_CB_DReset.EnableWindow(TRUE);
		
		m_CB_DailyMask.EnableWindow(FALSE);

		m_CB_DDaly.EnableWindow(TRUE);
		m_CB_ADaly.EnableWindow(TRUE);
		
		m_CE_ADays.SetBKColor(YELLOW);
		m_CE_DDays.SetBKColor(YELLOW);

		m_CE_DDays.SetReadOnly(false);
		m_CE_ADays.SetReadOnly(false);
		
		m_CE_Pvom.SetReadOnly(false);
		m_CE_Pbis.SetReadOnly(false);

		m_CE_Freq.SetReadOnly(false);
		m_CE_Pvom.SetBKColor(YELLOW);
		m_CE_Pbis.SetBKColor(YELLOW);
	}


	CCS_CATCH_ALL

}



void CSeasonDlg::EnableDeparture()
{

	CCS_TRY

	BOOL blEnable = FALSE;

	if(imModus != DLG_CHANGE_DIADATA && ((prmDFlight->Urno > 0) || (imModus != DLG_CHANGE)))
		blEnable = TRUE;


	if(blEnable)
	{
		pomDCinsTable->ShowWindow(SW_SHOW);
		pomDCinsTable->SetTableEditable(true);
		SetpWndStatAll(ogPrivList.GetStat("SEASONDLG_CE_DCins"),pomDCinsTable);
	}
	else
	{
		pomDCinsTable->ShowWindow(SW_HIDE);
	}



	if( CString(prmDFlight->Ftyp)	== "Z" || CString(prmDFlight->Ftyp)	== "B")
	{
		pomDViaCtrl->Enable(FALSE);
		m_CE_DRem1.EnableWindow(FALSE);
	}
	else
	{
		pomDViaCtrl->Enable(blEnable);
		m_CE_DRem1.EnableWindow(blEnable);
	}

	char clStat = ogPrivList.GetStat("SEASONDLG_CL_DRemp");

	m_CB_DRemp.ShowWindow(SW_SHOW);

	if(clStat == '1')
	{
		m_CB_DRemp.EnableWindow(blEnable);
	}
	else
	{
		if(clStat == '0')
		{
			m_CB_DRemp.EnableWindow(FALSE);
		}
		else
		{
			m_CB_DRemp.ShowWindow(SW_HIDE);
		}

	}

	m_CE_DCsgn.EnableWindow(blEnable);

	m_CE_DBaz1.EnableWindow(blEnable);
	m_CE_DBaz4.EnableWindow(blEnable);

	m_CB_DCinsMal4.EnableWindow(blEnable);
	m_CB_DDes3List.EnableWindow(blEnable);
	m_CB_DAlc3List.EnableWindow(blEnable);
	m_CB_DTtypList.EnableWindow(blEnable);
	m_CB_DStypList.EnableWindow(blEnable);
	m_CB_Default.EnableWindow(blEnable);
	m_CE_DLastChange.EnableWindow(blEnable);
	m_CE_DFluko.EnableWindow(blEnable);
	m_CE_DCreated.EnableWindow(blEnable);
	m_CE_DAlc3.EnableWindow(blEnable);
	m_CE_DCht3.EnableWindow(blEnable);
	m_CB_DWro1List.EnableWindow(blEnable);
	m_CB_DWro2List.EnableWindow(blEnable);
	m_CB_DPstdList.EnableWindow(blEnable);
	m_CB_DGtd2List.EnableWindow(blEnable);
	m_CB_DGtd1List.EnableWindow(blEnable);
	m_CB_DCinsList4.EnableWindow(blEnable);
	m_CB_DCinsList3.EnableWindow(blEnable);
	m_CB_DCinsList2.EnableWindow(blEnable);
	m_CB_DCinsList1.EnableWindow(blEnable);
	m_CB_DStatus.EnableWindow(blEnable);
	m_CE_DRem2.EnableWindow(blEnable);
	m_CB_DNoop.EnableWindow(blEnable);
	m_CE_DGtd2.EnableWindow(blEnable);
	m_CE_DGtd1.EnableWindow(blEnable);
	m_CB_DCxx.EnableWindow(blEnable);
	m_CB_DStatus3.EnableWindow(blEnable);
	m_CB_DStatus2.EnableWindow(blEnable);
	m_CE_DDes3.EnableWindow(blEnable);
	m_CE_DDes4.EnableWindow(blEnable);
	m_CE_DStev.EnableWindow(blEnable);
	m_CE_DTtyp.EnableWindow(blEnable);
	m_CE_DStyp.EnableWindow(blEnable);
	m_CE_DFlns.EnableWindow(blEnable);
	m_CE_DFltn.EnableWindow(blEnable);
	m_CE_DFlti.EnableWindow(blEnable);
	m_CE_DStod.EnableWindow(blEnable);
	m_CE_DStoa.EnableWindow(blEnable);
	m_CE_DEtdi.EnableWindow(blEnable);
	m_CE_DOrg3.EnableWindow(blEnable);
	m_CE_DGd1b.EnableWindow(blEnable);
	m_CE_DGd1e.EnableWindow(blEnable);
	m_CE_DGd2b.EnableWindow(blEnable);
	m_CE_DGd2e.EnableWindow(blEnable);
	m_CE_DTgd1.EnableWindow(blEnable);
	m_CE_DTgd2.EnableWindow(blEnable);
	m_CE_DTwr1.EnableWindow(blEnable);
	m_CE_DTwr2.EnableWindow(blEnable);
	m_CE_DW1bs.EnableWindow(blEnable);
	m_CE_DW1es.EnableWindow(blEnable);
	m_CE_DW2bs.EnableWindow(blEnable);
	m_CE_DW2es.EnableWindow(blEnable);
	m_CE_DWro1.EnableWindow(blEnable);
	m_CE_DWro2.EnableWindow(blEnable);
	m_CE_DPstd.EnableWindow(blEnable);
	m_CE_DPdbs.EnableWindow(blEnable);
	m_CE_DPdes.EnableWindow(blEnable);


	m_CE_DJfnoBorder.EnableWindow(FALSE);
	m_CE_DCinsBorder.EnableWindow(FALSE);


	if(omDJfno.GetSize() > 0)
		OnDshowjfno();
	else
		DShowJfnoButton();


	m_CB_DShowJfno.EnableWindow(blEnable);

	if(imModus == DLG_CHANGE)
	{
		m_CB_DDaly.EnableWindow(FALSE);
		m_CE_DDays.EnableWindow(FALSE);
	}
	else
	{
		m_CB_DDaly.EnableWindow(blEnable);
		m_CE_DDays.EnableWindow(blEnable);
	}



	if(( CString(prmDFlight->Ftyp)	== "Z" ) || (CString(prmDFlight->Ftyp)	== "B" ))
	{
		m_CE_DDes3.EnableWindow(FALSE);	
		m_CE_DDes4.EnableWindow(FALSE);	
		m_CB_DDes3List.EnableWindow(FALSE);	
	}
	else
	{
		m_CE_DDes3.EnableWindow(blEnable);	
		m_CE_DDes4.EnableWindow(blEnable);	
		m_CB_DDes3List.EnableWindow(blEnable);	
	}



	CCS_CATCH_ALL

}



void CSeasonDlg::ReadCcaData()
{
	CString olWhere;
	olWhere.Format("FLNU = %d", prmDFlight->Urno);
	omCcaData.ReadSpecial(olWhere);
	
	CCAFLIGHTDATA rlCcaFlight;

	if(bmLocalTime) ogSeasonDlgFlights.StructLocalToUtc((void*)prmDFlight);
	rlCcaFlight = *prmDFlight;
 	if(bmLocalTime) ogSeasonDlgFlights.StructUtcToLocal((void*)prmDFlight);

	omCcaData.CheckDemands(&rlCcaFlight);

	GetCcas();
}



void CSeasonDlg::GetCcas()
{

	if(prmDFlight->Urno <= 0)
		return;


	// copy ccas to member array
	omDCins.DeleteAll();
	for(int i = 0; i < omCcaData.omData.GetSize(); i++)
	{
		omDCins.New(omCcaData.omData[i]);
		if(imModus == DLG_COPY)
		{
			omDCins[i].Urno = 0;
			omDCins[i].Flnu = 0;
		}
	}
	omDCins.Sort(CompareCcas);	


	// create 'save'-records
	omDCinsSave.DeleteAll();
	omOldDCins.DeleteAll();
	omOldCcaValues.DeleteAll();
	for(i = 0; i < omDCins.GetSize(); i++)
	{
		omOldDCins.New(omDCins[i]);
		omOldCcaValues.New(omDCins[i]);

		omDCinsSave.New(omDCins[i]);
	}


}




void CSeasonDlg::DShowCinsTable()
{

	CCS_TRY

	CCSEDIT_ATTRIB rlAttribC1;
	CCSEDIT_ATTRIB rlAttribC2;
	CCSEDIT_ATTRIB rlAttribC3;
	CCSEDIT_ATTRIB rlAttribC4;

	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;

	int ilLc ;
	
	
	pomDCinsTable->ResetContent();


	if(ogPrivList.GetStat("SEASONDLG_CE_DCins") != '1')
		rlColumnData.BkColor = RGB(192,192,192);


	rlAttribC1.Style = ES_UPPERCASE;


	rlAttribC2.Format = "X";
	rlAttribC2.TextMinLenght = 0;
	rlAttribC2.TextMaxLenght = 1;
	rlAttribC2.Style = ES_UPPERCASE;

	rlAttribC3.Type = KT_TIME;
	rlAttribC3.ChangeDay	= true;
	rlAttribC3.TextMaxLenght = 7;

	rlAttribC4.Type = KT_TIME;
	rlAttribC4.ChangeDay	= true;
	rlAttribC4.TextMaxLenght = 7;


	rlColumnData.Font = &ogMS_Sans_Serif_8;
	rlColumnData.Alignment = COLALIGN_LEFT;

	CTime olTmpTime;

	for (ilLc = 0; ilLc < omDCins.GetSize(); ilLc++)
	{
		rlColumnData.Text = omDCins[ilLc].Ckic;
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omDCins[ilLc].Ckit;
		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		olTmpTime = omDCins[ilLc].Ckbs;
		if(bmLocalTime) omCcaData.UtcToLocal(olTmpTime);
		rlColumnData.Text = DateToHourDivString(olTmpTime, omDRefDat); //omDCins[ilLc].Ckbs.Format("%H:%M");
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		olTmpTime = omDCins[ilLc].Ckes;
		if(bmLocalTime) omCcaData.UtcToLocal(olTmpTime);
		rlColumnData.Text = DateToHourDivString(olTmpTime, omDRefDat);//omDCins[ilLc].Ckes.Format("%H:%M");
		rlColumnData.EditAttrib = rlAttribC4;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomDCinsTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();
	}

	for (ilLc = omDCins.GetSize(); ilLc < 30; ilLc++)
	{
		rlColumnData.Text = "";
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = "";
		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = "";
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = "";
		rlColumnData.EditAttrib = rlAttribC4;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		pomDCinsTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();
	}
	pomDCinsTable->DisplayTable();

	CCS_CATCH_ALL

}



///////////////////////////////////////////////////////////////////////////////////////////////////////
//  weiter Flugnummer - Ankunft
//
void CSeasonDlg::OnAshowjfno() 
{

	CCS_TRY

	if(!bmIsAFfnoShown)
	{
		char clStat = ogPrivList.GetStat("SEASONDLG_CB_AShowJfno");

		m_CB_AShowJfno.ShowWindow(SW_HIDE);
		m_CE_AJfnoBorder.ShowWindow(SW_SHOW);
		m_CB_AAlc3List2.ShowWindow(SW_SHOW);
		m_CB_AAlc3List3.ShowWindow(SW_SHOW);
	    pomAJfnoTable->ShowWindow(SW_SHOW);
		SetpWndStatAll(clStat,pomAJfnoTable);
		
		if(clStat == '1')
			pomAJfnoTable->MakeInplaceEdit(0, 0);
	}
	bmIsAFfnoShown = true;

	CCS_CATCH_ALL

}


void CSeasonDlg::AShowJfnoButton() 
{

	CCS_TRY

	m_CB_AShowJfno.ShowWindow(SW_SHOW);
	m_CE_AJfnoBorder.ShowWindow(SW_HIDE);
	m_CB_AAlc3List2.ShowWindow(SW_HIDE);
	m_CB_AAlc3List3.ShowWindow(SW_HIDE);
    pomAJfnoTable->ShowWindow(SW_HIDE);
	bmIsAFfnoShown = false;

	CCS_CATCH_ALL

}





///////////////////////////////////////////////////////////////////////////////////////////////////////
//  weiter Flugnummer - Abflug
//
void CSeasonDlg::OnDshowjfno() 
{

	CCS_TRY

	if(!bmIsDFfnoShown)
	{
		char clStat = ogPrivList.GetStat("SEASONDLG_CB_DShowJfno");
		m_CB_DShowJfno.ShowWindow(SW_HIDE);

		m_CE_DJfnoBorder.ShowWindow(SW_SHOW);
		m_CB_DAlc3List2.ShowWindow(SW_SHOW);
		m_CB_DAlc3List3.ShowWindow(SW_SHOW);
	    pomDJfnoTable->ShowWindow(SW_SHOW);
		SetpWndStatAll(ogPrivList.GetStat("SEASONDLG_CB_DShowJfno"),pomDJfnoTable);
		if(clStat == '1')
			pomDJfnoTable->MakeInplaceEdit(0, 0);
	}
	bmIsDFfnoShown = true;

	CCS_CATCH_ALL

}


void CSeasonDlg::DShowJfnoButton() 
{

	CCS_TRY

	m_CB_DShowJfno.ShowWindow(SW_SHOW);

	m_CE_DJfnoBorder.ShowWindow(SW_HIDE);
	m_CB_DAlc3List2.ShowWindow(SW_HIDE);
	m_CB_DAlc3List3.ShowWindow(SW_HIDE);

    pomDJfnoTable->ShowWindow(SW_HIDE);
	bmIsDFfnoShown = false;

	CCS_CATCH_ALL

}




void CSeasonDlg::ProcessCCA(CCADATA *prpCca, int ipDDXType)
{

	CCS_TRY

	if((prmDFlight->Urno <= 0) || (imModus != DLG_CHANGE))
		return;

	if (ipDDXType == CCA_SEADLG_NEW || ipDDXType == CCA_SEADLG_DELETE)
	{
		if (ipDDXType == CCA_SEADLG_DELETE)
		{
			// delete the record in omCcaData at first
			omCcaData.DeleteInternal(prpCca, false);

		}

		CCAFLIGHTDATA rlCcaFlight;

		if(bmLocalTime) ogSeasonDlgFlights.StructLocalToUtc((void*)prmDFlight);
		rlCcaFlight = *prmDFlight;
 		if(bmLocalTime) ogSeasonDlgFlights.StructUtcToLocal((void*)prmDFlight);

		omCcaData.CheckDemands(&rlCcaFlight);
	}


	GetCcas();
	DShowCinsTable();

	CCS_CATCH_ALL

}



void CSeasonDlg::SetWndPos()
{

	CCS_TRY

	CRect olRect;

	GetWindowRect(&olRect);

	int ilWidth = olRect.right -olRect.left;
	int ilWhichMonitor = ogCfgData.GetMonitorForWindow(CString(MON_SEASONROTDLG_STRING));
	int ilMonitors = ogCfgData.GetMonitorForWindow(CString(MON_COUNT_STRING));
	int left, top, right, bottom;
	int ilCXMonitor;

	bottom = ::GetSystemMetrics(SM_CYSCREEN);

	top = ( (bottom - (olRect.bottom -olRect.top ) ) / 2);  


	if(ilWhichMonitor == 1)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}
	else if(ilWhichMonitor == 2)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2)+(ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2)+(ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			right = GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}
	else if(ilWhichMonitor == 3)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2)+(2*ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			right = GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}


	//MoveWindow(CRect(left, top, right, bottom));
	SetWindowPos(&wndTop,left , top, 0, 0, SWP_SHOWWINDOW | SWP_NOSIZE);

	CCS_CATCH_ALL

}





void CSeasonDlg::OnSelchangeAremp() 
{
	OnEditChanged(0,0);	
	
}

void CSeasonDlg::OnSelchangeDremp() 
{
	OnEditChanged(0,0);	
	
}


void CSeasonDlg::OnEditRButtonDown(UINT wParam, LPARAM lParam) 
{
	// Insert current time in baggage-belt begin- and end-editbox
 	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY *) lParam;
 	CTime olTime = CTime::GetCurrentTime();
	if (!bmLocalTime)
	{
		ogSeasonDlgFlights.LocalToUtc(olTime);
	}

	if (prlNotify->SourceControl == &m_CE_AB1bs)
	{
 		m_CE_AB1bs.SetWindowText(DateToHourDivString(olTime, olTime));
	}
	if (prlNotify->SourceControl == &m_CE_AB1es)
	{
 		m_CE_AB1es.SetWindowText(DateToHourDivString(olTime, olTime));
	}
	if (prlNotify->SourceControl == &m_CE_AB2bs)
	{
 		m_CE_AB2bs.SetWindowText(DateToHourDivString(olTime, olTime));
	}
	if (prlNotify->SourceControl == &m_CE_AB2es)
	{
 		m_CE_AB2es.SetWindowText(DateToHourDivString(olTime, olTime));
	}

	
}

BOOL CSeasonDlg::HandlePostFlight()
{
	// postflight: no changes allowed
	if (CheckPostFlight())
	{
		if (GetDlgItem(IDOK)) 
			m_CB_Ok.EnableWindow(FALSE);
		
		return TRUE;
	}

	return FALSE;
}

BOOL CSeasonDlg::CheckPostFlight()
{
	BOOL blPost = FALSE;

	if (prmAFlight && prmDFlight)
	{
		if (prmAFlight->Tifa != TIMENULL && prmDFlight->Tifd != TIMENULL)
		{
			if (IsPostFlight(prmAFlight->Tifa,bmLocalTime) && IsPostFlight(prmDFlight->Tifd,bmLocalTime))
				blPost = TRUE;
		}
		else if (prmDFlight->Tifd != TIMENULL)
		{
			if (IsPostFlight(prmDFlight->Tifd,bmLocalTime))
				blPost = TRUE;
		}
		else if (prmAFlight->Tifa != TIMENULL)
		{
			if (IsPostFlight(prmAFlight->Tifa,bmLocalTime))
				blPost = TRUE;
		}
	}
	else if (!prmAFlight && prmDFlight)
	{
		if (prmDFlight->Tifd != TIMENULL)
		{
			if (IsPostFlight(prmDFlight->Tifd,bmLocalTime))
				blPost = TRUE;
		}
	}
	else if (prmAFlight && !prmDFlight)
	{
		if (prmAFlight->Tifa != TIMENULL)
		{
			if (IsPostFlight(prmAFlight->Tifa,bmLocalTime))
				blPost = TRUE;
		}
	}

	if (blPost)
		ModifyWindowText(this, GetString(IDS_STRINGWND_POSTFLIGHT), FALSE);
	else
		ModifyWindowText(this, GetString(IDS_STRINGWND_POSTFLIGHT), TRUE);

	return blPost;
}




bool CSeasonDlg::AddBltCheck()
{
	if (!bgExtBltCheck)
		return true;

	// only if arrival flight present
	if (prmAFlight->Urno < 1)
		return true;

	//// check the belt type of the two baggage belts
	if (strlen(prmAFlight->Blt1) > 0 && strlen(prmAFlight->Blt2) > 0)
	{
		CString olBlt1Type;
		CString olBlt2Type;

		// get both belt types
		ogBCD.GetField("BLT", "BNAM", prmAFlight->Blt1, "BLTT", olBlt1Type);
		ogBCD.GetField("BLT", "BNAM", prmAFlight->Blt2, "BLTT", olBlt2Type);

		if (!olBlt1Type.IsEmpty() && !olBlt2Type.IsEmpty() &&
			olBlt1Type != olBlt2Type)
		{
			CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING2045) + CString("\n\n") + GetString(IDS_STRING2046), GetString(ST_FEHLER), MB_ICONERROR | MB_OK);
			return false;
		}
	}


	// only proceed if the baggage belt allocation values or STA, ETA has been changed!
	if (!m_CE_ABlt1.IsChanged() && !m_CE_ATmb1.IsChanged() && !m_CE_AB1bs.IsChanged() && !m_CE_AB1es.IsChanged() &&
		!m_CE_ABlt2.IsChanged() && !m_CE_ATmb2.IsChanged() && !m_CE_AB2bs.IsChanged() && !m_CE_AB2es.IsChanged() &&
		!m_CE_AStoa.IsChanged() && !m_CE_AEtai.IsChanged())
		return true;

	CString olWarningText;
	bool blWarning = false;
	CString olAddText;

	//// check opening time of belt 1
	if (!prmAFlight->CheckBltOpeningTime(1, olAddText))
	{
		CString olTmp;
		olTmp.Format(GetString(IDS_STRING2033), olAddText);

		olWarningText += olTmp;
		olWarningText += '\n';
		blWarning = true;
	}
	//// check closing time of belt 1
	if (!prmAFlight->CheckBltClosingTime(1, olAddText))
	{
		CString olTmp;
		olTmp.Format(GetString(IDS_STRING2035), olAddText);

		olWarningText += olTmp;
		olWarningText += '\n';
		blWarning = true;
	}
	//// check opening time of belt 2
	if (!prmAFlight->CheckBltOpeningTime(2, olAddText))
	{
		CString olTmp;
		olTmp.Format(GetString(IDS_STRING2034), olAddText);

		olWarningText += olTmp;
 		olWarningText += '\n';
		blWarning = true;
	}
	//// check closing time of belt 2
	if (!prmAFlight->CheckBltClosingTime(2, olAddText))
	{
		CString olTmp;
		olTmp.Format(GetString(IDS_STRING2036), olAddText);

		olWarningText += olTmp;
		olWarningText += '\n';
		blWarning = true;
	}
	//// check if two belts are allocated
	if (strlen(prmAFlight->Blt1) > 0 && strlen(prmAFlight->Blt2) > 0)
	{
		olWarningText += GetString(IDS_STRING2043);
		olWarningText += '\n';
		blWarning = true;
	}

	// Display message box with warnings
	if (blWarning)
	{
		olWarningText += CString("\n") + GetString(IDS_STRING2037);
		if (CFPMSApp::MyTopmostMessageBox(this, olWarningText, GetString(IDS_WARNING), MB_ICONWARNING | MB_YESNO) == IDYES)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	return true;
}











