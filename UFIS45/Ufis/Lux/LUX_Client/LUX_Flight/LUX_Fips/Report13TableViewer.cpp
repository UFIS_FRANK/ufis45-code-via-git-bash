// LvgTableViewer.cpp 
//

#include <stdafx.h>
#include <Report13TableViewer.h>
#include <CCSGlobl.h>
#include <CedaBasicData.h>
#include <resrc1.h>

#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// LvgTableViewer
//
LvgTableViewer::LvgTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo, char *pcpSelect)
{
	pomData = popData;
	pcmInfo = pcpInfo;
	pcmSelect = pcpSelect;

	bmIsFromSearch = FALSE;
    pomLvgTable = NULL;
}

//-----------------------------------------------------------------------------------------------

LvgTableViewer::~LvgTableViewer()
{
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void LvgTableViewer::Attach(CCSTable *popTable)
{
    pomLvgTable = popTable;
}


//-----------------------------------------------------------------------------------------------
void LvgTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------
void LvgTableViewer::MakeLines()
{
// AbfluegelvgTableViewer = Vorlage
	int ilTotl = 0;
	int ilSeat = 0;
	int ilTmpSeat = 0;
	int ilSeatTotl = 0;
	CString olTmpSeat;
	CString olNewLvg("");
	CString olOldLvg("");

	int ilCount = pomData->GetSize();

	for (int ilLc = 0; ilLc < ilCount; ilLc++)
	{
		ilTotl++;
		ROTATIONDLGFLIGHTDATA *prlLvgData = &pomData->GetAt(ilLc);
		if (strlen(prlLvgData->Alc2))
				olNewLvg = prlLvgData->Alc2;
		else
				olNewLvg = prlLvgData->Alc3;
/*
		//ogActData.GetSeat(prlLvgData->Act3, prlLvgData->Act5, olTmpSeat);
		if(!ogBCD.GetField("ACT", "ACT3", prlLvgData->Act3, "SEAT", olTmpSeat))
			ogBCD.GetField("ACT", "ACT5", prlLvgData->Act5, "SEAT", olTmpSeat);
*/
		olTmpSeat = prlLvgData->Nose;

		ilTmpSeat = atoi(olTmpSeat);
		ilSeatTotl += ilTmpSeat;
		if(ilLc == 0)	// der erste
		{
			if (strlen(prlLvgData->Alc2) || strlen(prlLvgData->Alc3))
				olOldLvg = olNewLvg;
			ilTotl = 0;
		}

		if (olOldLvg != olNewLvg)	// neue LVG
		{
			MakeLine(olOldLvg, ilSeat, ilTotl);
			//MakeLine(" ", " ", 0);
			ilSeat = ilTmpSeat;
			ilTotl = 0;
			olOldLvg = olNewLvg;
		}

		else
			ilSeat += ilTmpSeat;

		if((ilLc == ilCount - 1) && (ilTotl > 0))	// nach dem letzten
		//if((ilLc == 0) && (ilSubt > 0))	// nach dem letzten
		{
			MakeLine(olNewLvg, ilSeat, ilTotl);
		}
	}

	MakeResultLine(GetString(IDS_STRING1200), ilSeatTotl, ilCount);

//generate the headerinformation
	CString olTimeSet = GetString(IDS_STRING1920); //UTC
	if(bgReportLocal)
		olTimeSet = GetString(IDS_STRING1921); //LOCAL

	char pclHeader[256];
	sprintf(pclHeader, GetString(IDS_R_HEADER_12_1), /*GetString(IDS_REPORTS_RB_SeatAvailability),*/ pcmInfo, ilCount);
	omTableName = pclHeader;
}

//-----------------------------------------------------------------------------------------------
// neue Destination
void LvgTableViewer::MakeLine(CString poLvg, int ipSeat, int ipTotl)
{
 	char clbuffer[10];
   // Update viewer data for this shift record
    LVGTABLE_LINEDATA rlLvglvg;

		rlLvglvg.Alc3 = poLvg;
		if(ipSeat)
		{
			sprintf(clbuffer,"%d", ipSeat);
			rlLvglvg.Seat = clbuffer; 
		}
		if(ipTotl)
		{
			sprintf(clbuffer,"%d", ipTotl);
			rlLvglvg.Totl = clbuffer; 
		}
	//ogActData.GetSeat(prpLvglvg->Act3, prpLvglvg->Act5, rlLvglvg.Seat);	

	CreateLine(&rlLvglvg);
}


//-----------------------------------------------------------------------------------------------
// 
void LvgTableViewer::MakeResultLine(CString opNewLvg, int ipSeat, int ipCntr)
{
    // Update viewer data for this shift record
	char clbuffer[10];
    LVGTABLE_LINEDATA rlLvglvg;
		rlLvglvg.Alc3 = "";
		rlLvglvg.Seat = ""; 
		rlLvglvg.Totl = ""; 
	//CreateLine(&rlLvglvg);
		rlLvglvg.Alc3 = opNewLvg;
		sprintf(clbuffer,"%d", ipSeat);
		rlLvglvg.Seat = clbuffer;
		sprintf(clbuffer,"%d", ipCntr);
		rlLvglvg.Totl = clbuffer; 
	CreateLine(&rlLvglvg);

}


//-----------------------------------------------------------------------------------------------

void LvgTableViewer::CreateLine(LVGTABLE_LINEDATA *prpLvglvg)
{
	int ilLineno = 0;
	LVGTABLE_LINEDATA rlLvglvg;
	rlLvglvg = *prpLvglvg;
    omLines.NewAt(ilLineno, rlLvglvg);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void LvgTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 2;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomLvgTable->SetShowSelection(TRUE);
	pomLvgTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;
	rlHeader.Length = 100; 
	rlHeader.Text = GetString(IDS_STRING2139);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 100;
	rlHeader.Text = GetString(IDS_STRING1167);//CString("Sitzplštze");
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 180;
	rlHeader.Text = GetString(IDS_STRING2140);//CString("Anzahl/LVG");
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomLvgTable->SetHeaderFields(omHeaderDataArray);

	pomLvgTable->SetDefaultSeparator();
	pomLvgTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
     for (int ilLineNo = ilLines - 1; ilLineNo >= 0; ilLineNo--)
  // for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		if(omLines[ilLineNo].Alc2.GetLength())
			rlColumnData.Text = omLines[ilLineNo].Alc2;
		else
			rlColumnData.Text = omLines[ilLineNo].Alc3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Seat;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Totl;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomLvgTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomLvgTable->DisplayTable();
}


//-----------------------------------------------------------------------------------------------

void LvgTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

void LvgTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void LvgTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	char pclFooter[256];
	sprintf(pclFooter, GetString(IDS_STRING1170), pcmSelect, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));

	omFooterName = pclFooter;
	CString olTableName = GetString(IDS_REPORTS_RB_SeatAvailability);

	int ilOrientation = PRINT_PORTRAET;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			//pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			pomPrint->imLineNo = pomPrint->imMaxLines + 1 - 2;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = olTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
//			olFooter1.Format(GetString(IDS_STRING1445),ilLines,omFooterName);
			olFooter1.Format("%s -   %s", omFooterName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );
			//for(int i = 0; i < ilLines; i++ ) 
			for(int i = ilLines-1; i >= 0; i-- ) 
			{
				//if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				if(pomPrint->imLineNo >= pomPrint->imMaxLines-2)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				//if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				// drehen
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-3) || i == 0)
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;

	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool LvgTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;
/*
	char pclHeader[100];
	//sprintf(pclHeader, "Sitzplatzangebot nach LVGs und/oder Destinationen vom %s", pcmInfo);
	sprintf(pclHeader, GetString(IDS_STRING1170), pcmSelect, pcmInfo);
	CString olTableName(pclHeader);
*/
	//pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);
//	pomPrint->PrintUIFHeader("",CString(pclHeader),pomPrint->imFirstLine-10);
	pomPrint->PrintUIFHeader("", omTableName, pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Text   = omHeaderDataArray[i].Text;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool LvgTableViewer::PrintTableLine(LVGTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		
		rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameTop   = PRINT_FRAMETHIN;
		if(bpLastLine == true)
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN;
		}
		else
		{
			rlElement.FrameBottom = PRINT_NOFRAME;
		}
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		switch(i)
		{
		case 0:
			{
				rlElement.Alignment  = PRINT_LEFT;
				rlElement.FrameLeft = PRINT_FRAMETHIN;
				if(strlen(prpLine->Alc2))
					rlElement.Text = prpLine->Alc2;
				else
					rlElement.Text = prpLine->Alc3;
			}
			break;
		case 1:
			{
				rlElement.Alignment  = PRINT_LEFT;
				rlElement.Text		= prpLine->Seat;
			}
			break;
		case 2:
			{
				rlElement.Alignment  = PRINT_LEFT;
				rlElement.Text		= prpLine->Totl;
				rlElement.FrameRight = PRINT_FRAMETHIN;
			}
			break;
		}

		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		
	}

	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------


// Drucken in Datei
bool LvgTableViewer::PrintPlanToFile(char *opTrenner)
{
	ofstream of;

	char* tmpvar;
	tmpvar = getenv("TMP");
	if (tmpvar == NULL)
		tmpvar = getenv("TEMP");

	if (tmpvar == NULL)
		return false;

	CString olFileName = omTableName;
	olFileName.Remove('*');
	olFileName.Remove('.');
	olFileName.Remove(':');
	olFileName.Remove('/');
	olFileName.Replace(" ", "_");
	
	char pHeader[256];
	strcpy (pHeader, tmpvar);
	CString path = pHeader;
	omFileName =  path + "\\" + olFileName + ".csv";

//	of.open( GetString(IDS_STRING1386), ios::out);
	of.open( omFileName, ios::out);

	int ilwidth = 1;

	of  << setw(ilwidth) << omTableName << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;

	of	<< setw(ilwidth) << GetString(IDS_STRING2139) 
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING1167)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING2140) 
		<< endl;


	int ilCount = omLines.GetSize();

	for(int i = ilCount-1; i >=0; i--)
	{
		LVGTABLE_LINEDATA rlLine = omLines[i];

		CString olAlc;
		if(strlen(rlLine.Alc2))
			olAlc = rlLine.Alc2;
		else
			olAlc = rlLine.Alc3;

		of.setf(ios::left, ios::adjustfield);

		of   << setw(ilwidth) << olAlc
		     << setw(1) << opTrenner 
			 << setw(ilwidth) << rlLine.Seat
		     << setw(1) << opTrenner 
			 << setw(ilwidth) << rlLine.Totl
			 << endl;

	}

	of.close();
	return true;


/*
	char pclHeader[100];
	sprintf(pclHeader, GetString(IDS_STRING1174), pcmSelect);
	ofstream of;
	of.open( pcpDefPath, ios::out);
	//of << "Sitzplatzangebot nach LVGs und/oder Destinationen: " 
	of << GetString(IDS_STRING1172) << "  " << pclHeader << "  "
		<< CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M") << endl << endl;

	of  << GetString(IDS_STRING1136) << "       " 
		<< GetString(IDS_STRING1167) << "    " 
		<< GetString(IDS_STRING1169) 
		<< endl;


	of << "---------------------------------------------------" << endl;
	int ilCount = omLines.GetSize();
	for(int i = ilCount-1; i >=0; i--)
	{
		LVGTABLE_LINEDATA rlD = omLines[i];
		of.setf(ios::left, ios::adjustfield);
		of   << setw(10) << rlD.Alc3  
			 << setw(14) << rlD.Seat 
			 << setw(10) << rlD.Totl << endl;
	}
	of.close();
*/
	return true;
}

