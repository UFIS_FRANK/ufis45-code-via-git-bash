// stafchrt.h : header file
//

#ifndef _STAFFCHRT_
#define _STAFFCHRT_

#include <ccsdragdropctrl.h>
#include <CCS3dStatic.h>
#include <CCSGlobl.h>
#include <FlightGantt.h>

#ifndef _CHART_STATE_
#define _CHART_STATE_

//enum ChartState { Minimized, Normal, Maximized };

#endif // _CHART_STATE_

/////////////////////////////////////////////////////////////////////////////
// FlightChart frame

class FlightDiagram;

class FlightChart : public CFrameWnd
{
	friend FlightDiagram;

    DECLARE_DYNCREATE(FlightChart)
public:
    FlightChart();           // protected constructor used by dynamic creation
    virtual ~FlightChart();

// Operations
public:
    int GetHeight();
    int GetState(void) { return imState; };
    void SetState(int ipState) { imState = ipState; };
    
    CCSTimeScale *GetTimeScale(void) { return pomTimeScale; };
    void SetTimeScale(CCSTimeScale *popTimeScale)
    {
        pomTimeScale = popTimeScale;
    };

    CStatusBar *GetStatusBar(void) { return pomStatusBar; };
    void SetStatusBar(CStatusBar *popStatusBar)
    {
        pomStatusBar = popStatusBar;
    };
    
    FlightDiagramViewer *GetViewer(void) { return pomViewer; };
    int GetGroupNo() { return imGroupNo; };
    void SetViewer(FlightDiagramViewer *popViewer, int ipGroupNo)
    {
        pomViewer = popViewer;
        imGroupNo = ipGroupNo;
    };

    void SetMarkTime(CTime opStatTime, CTime opEndTime);
    CTime GetStartTime(void) { return omStartTime; };
    void SetStartTime(CTime opStartTime) { omStartTime = opStartTime; };

    CTimeSpan GetInterval(void) { return omInterval; };
    void SetInterval(CTimeSpan opInterval) { omInterval = opInterval; };

    FlightGantt *GetGanttPtr(void) { return &omGantt; };
    CCSButtonCtrl  *GetChartButtonPtr(void) { return &omButton; };
    CCS3DStatic *GetTopScaleTextPtr(void) { return pomTopScaleText; };
	CCS3DStatic *GetCountTextPtr(void) { return pomCountText; };

// Overrides
public:

// Implementation
protected:
    // Generated message map functions
    //{{AFX_MSG(FlightChart)
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
    afx_msg void OnPaint();
    afx_msg void OnChartButton();
    afx_msg LONG OnChartButtonRButtonDown(UINT, LONG);
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
	afx_msg void OnMenuAssign();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()
    
protected:
    int imState;
    int imHeight;
    
    CCSButtonCtrl omButton;
    CCS3DStatic *pomTopScaleText;
	CCS3DStatic *pomCountText;
    
    CCSTimeScale *pomTimeScale;
    CStatusBar *pomStatusBar;
    FlightDiagramViewer *pomViewer;
    int imGroupNo;
    CTime omStartTime;
    CTimeSpan omInterval;
public:
    FlightGantt omGantt;

private:    
    COLORREF lmBkColor;
    COLORREF lmTextColor;
    COLORREF lmHilightColor;
    int imStartTopScaleTextPos;
    int imStartVerticalScalePos;

// Drag-and-drop section
public:
    CCSDragDropCtrl m_ChartWindowDragDrop;
    CCSDragDropCtrl m_ChartButtonDragDrop;
    CCSDragDropCtrl m_CountTextDragDrop;
    CCSDragDropCtrl m_TopScaleTextDragDrop;

//-DTT Jul.22-----------------------------------------------------------
// No more routine ProcessPartTimeAssignment(), we use a new dialog in
// file "dassignp.h" instead.
//	void ProcessDropShifts(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
//  void ProcessPartTimeAssignment(CCSDragDropCtrl *popDragDropCtrl);
	LONG ProcessDropShifts(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
	LONG ProcessDropDutyBar(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
	LONG ProcessDropFlightBar(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
//----------------------------------------------------------------------
};

/////////////////////////////////////////////////////////////////////////////

#endif // _STAFFCHRT_
