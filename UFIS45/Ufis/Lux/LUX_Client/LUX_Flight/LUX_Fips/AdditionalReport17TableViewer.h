#ifndef __ADDITIONAL_REPORT_17_VIEWER_H__
#define __ADDITIONAL_REPORT_17_VIEWER_H__

#include <stdafx.h>
#include <RotationDlgCedaFlightData.h>
#include <DGanttChartReportWnd.h>
#include <DailyCedaFlightData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>



/////////////////////////////////////////////////////////////////////////////
// AdditionalReport17TableViewer

class AdditionalReport17TableViewer : public CViewer
{
// Constructions
public:
    AdditionalReport17TableViewer(char *pcpInfo = NULL, char *pcpSelect = NULL, 
		                          CTime opMin = TIMENULL, CTime opMax = TIMENULL);
    ~AdditionalReport17TableViewer();

private:
	void InitColumn0();
	void GetRecordsFromDB();

// Operations

// Attributes used for filtering condition
private:
	// 
	CCSPtrArray<RecordSet>omPST;
	CCSPtrArray<RecordSet>omPSTFailure;
	DailyCedaFlightData omDailyCedaFlightData;

	// Sapltengroese
	CUIntArray omAnzChar;

	// Liste der existierenden Delays
	CStringArray omColumn0;
	// Auftreten der existierenden Delays
	CUIntArray omCountColumn1;
	CUIntArray omCountColumn2;
	char pcmFirstDate[16];	//Time frame beginn (datum)
	char pcmLastDate[16];	//Time frame end
	long lmDays;
	CTime omMin;			// Datum + Zeit
	CTime omMax;
	CDialog* pomParentDlg;

public:
	int GetBarCount();
	int GetDataPst(int ipIndex);
	int GetDataPstFailure(int ipIndex);
	CString GetTime(int ipIndex);
	int NumberOfPst();

	//Print 
	void SetParentDlg(CDialog* ppDlg);
	void PrintTableView(DGanttChartReportWnd* popDGanttChartReportWnd);
	CCSPrint *pomPrint; 
	CString omTableName;
	char *pcmInfo;
	char *pcmSelect;
	CString omFooterName;
	CTime omTimeFirst;

protected:
	void SetFieldLength(CString opCurrentColumns);
	// Listen zum Zaehlen erstellen
	void MakeCountListPST();
	void MakeCountListPSTFailure();
	// Schreiben der Zahlen in die Puffer
	void WriteBuffersPST(CTime olTimeFrom, CTime olTimeTo);
	void WriteBuffersPSTFailure(CTime olTimeFrom, CTime olTimeTo);

};

#endif