// SeasonCollectAskDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <fpms.h>
#include <SeasonCollectAskDlg.h>
#include <CedaBasicData.h>
#include <AwBasicDataDlg.h>
#include <CCSTime.h>
#include <CCSGlobl.h>
#include <resrc1.h>
#include <BasicData.h>



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld SeasonCollectAskDlg 


SeasonCollectAskDlg::SeasonCollectAskDlg(CWnd* pParent /*=NULL*/)
	: CDialog(SeasonCollectAskDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(SeasonCollectAskDlg)
	m_Alc = _T("");
	m_Seas = _T("");
	m_To = _T("");
	m_From = _T("");
	m_Fltn = _T("");
	m_Flns = _T("");
	m_Tto = _T("");
	m_Tfrom = _T("");
	//}}AFX_DATA_INIT
}


void SeasonCollectAskDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(SeasonCollectAskDlg)
	DDX_Control(pDX, IDC_ORGORDES, m_CR_OrgOrDes);
	DDX_Control(pDX, IDC_ORGORDES2, m_CR_OrgOrDes2);
	DDX_Control(pDX, IDC_FLNS, m_CE_Flns);
	DDX_Control(pDX, IDC_FLTN, m_CE_Fltn);
	DDX_Control(pDX, IDC_FROM, m_CE_From);
	DDX_Control(pDX, IDC_TO, m_CE_To);
	DDX_Control(pDX, IDC_SEASON, m_CE_Seas);
	DDX_Control(pDX, IDC_ALCLIST, m_CB_AlcList);
	DDX_Control(pDX, IDC_SEALIST, m_CB_SeaList);
	DDX_Control(pDX, IDC_ALC, m_CE_Alc);
	DDX_Control(pDX, IDC_EDIT_FTIME, m_CE_Tfrom);
	DDX_Control(pDX, IDC_EDIT_TTIME, m_CE_Tto);
	DDX_Text(pDX, IDC_ALC, m_Alc);
	DDX_Text(pDX, IDC_SEASON, m_Seas);
	DDX_Text(pDX, IDC_TO, m_To);
	DDX_Text(pDX, IDC_FROM, m_From);
	DDX_Text(pDX, IDC_FLTN, m_Fltn);
	DDX_Text(pDX, IDC_FLNS, m_Flns);
	DDX_Text(pDX, IDC_EDIT_FTIME, m_Tfrom);
	DDX_Text(pDX, IDC_EDIT_TTIME, m_Tto);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(SeasonCollectAskDlg, CDialog)
	//{{AFX_MSG_MAP(SeasonCollectAskDlg)
	ON_BN_CLICKED(IDC_ALCLIST, OnAlclist)
	ON_MESSAGE(WM_EDIT_KILLFOCUS, OnEditKillfocus)
	ON_BN_CLICKED(IDC_SEALIST, OnSealist)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten SeasonCollectAskDlg 




BOOL SeasonCollectAskDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_CE_Alc.SetBKColor(YELLOW);
	m_CE_Fltn.SetBKColor(YELLOW);
	m_CE_Seas.SetBKColor(YELLOW);
	m_CE_To.SetBKColor(YELLOW);
	m_CE_From.SetBKColor(YELLOW);

	m_CE_To.SetTypeToDate();
	m_CE_From.SetTypeToDate();
		
	m_CE_Seas.SetTypeToString("X|#X|#X|#X|#X|#",5,0);
	m_CE_Alc.SetTypeToString("X|#X|#X|#",3,2);
	m_CE_Fltn.SetTypeToString("#####",5,1);
	m_CE_Flns.SetTypeToString("X",1,0);

	m_CR_OrgOrDes.SetCheck(FALSE);	
	m_CR_OrgOrDes2.SetCheck(FALSE);	
	
	m_CE_Tto.SetTypeToTime();
	m_CE_Tfrom.SetTypeToTime();

	CTime olTime = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olTime);
	CString olCurrTime = CTimeToDBString(olTime, TIMENULL);
	CString olSeas;
	ogBCD.GetFieldBetween("SEA", "VPFR", "VPTO", olCurrTime, "SEAS", m_Seas );

	CString olToStr;
	CString olFromStr;
	CTime olTo;
	CTime olFrom;

	m_CE_Seas.SetInitText(m_Seas);
	ogBCD.GetField("SEA", "SEAS", m_Seas, "VPFR", olFromStr);
	ogBCD.GetField("SEA", "SEAS", m_Seas, "VPTO", olToStr );
	olTo = DBStringToDateTime(olToStr);
	olFrom = DBStringToDateTime(olFromStr);
	m_CE_From.SetInitText(olFrom.Format("%d.%m.%Y"));
	m_CE_To.SetInitText(olTo.Format("%d.%m.%Y"));

	m_CE_Tfrom.SetInitText(olFrom.Format("%H%M"));
	m_CE_Tto.SetInitText(olTo.Format("%H%M"));


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}




void SeasonCollectAskDlg::OnOK() 
{

	m_CE_Fltn.GetWindowText(m_Fltn);
	m_CE_Alc.GetWindowText(m_Alc);
	m_CE_Flns.GetWindowText(m_Flns);
	m_CE_To.GetWindowText(m_To);
	m_CE_From.GetWindowText(m_From);
	m_CE_Tto.GetWindowText(m_Tto);
	m_CE_Tfrom.GetWindowText(m_Tfrom);
	m_CE_Seas.GetWindowText(m_Seas);

	CString olMess;

	if((m_CR_OrgOrDes.GetCheck() == 0) && (m_CR_OrgOrDes2.GetCheck() == 0))
		olMess += GetString(IDS_STRING1250);//"Selektion Ankunft/Abflug fehlt!\n"; 

	if(m_CR_OrgOrDes.GetCheck() == 0)
		omAdid = "D";
	else
		omAdid = "A";

	
	if(!m_CE_Alc.GetStatus())
		olMess += GetString(IDS_STRING1245) + CString("\n");//"Fluggesellschaft nicht in Stammdaten erfasst!\n"; 

	if(!m_CE_Fltn.GetStatus())
		olMess += GetString(IDS_STRING1339) + CString("\n");//"Flugnummer ist leer!\n"; 


	if(!m_CE_To.GetStatus() || !m_CE_From.GetStatus() || m_To.IsEmpty() || m_From.IsEmpty())
		olMess += GetString(IDS_STRING1340) + CString("\n");//"Ung�ltige Periode!\n"; 


	CString olFSeas ;
	CString olTSeas ;


	if(!m_To.IsEmpty() && !m_From.IsEmpty())
	{
//		ogBCD.GetFieldBetween("SEA", "VPFR", "VPTO", CTimeToDBString(DateHourStringToDate(m_To,  CString("2359")), TIMENULL), "SEAS", olTSeas );
//		ogBCD.GetFieldBetween("SEA", "VPFR", "VPTO", CTimeToDBString(DateHourStringToDate(m_From, CString("0001")), TIMENULL), "SEAS", olFSeas );
		ogBCD.GetFieldBetween("SEA", "VPFR", "VPTO", CTimeToDBString(DateHourStringToDate(m_To,  m_Tto), TIMENULL), "SEAS", olTSeas );
		ogBCD.GetFieldBetween("SEA", "VPFR", "VPTO", CTimeToDBString(DateHourStringToDate(m_From, m_Tfrom), TIMENULL), "SEAS", olFSeas );
		if(olFSeas != olTSeas)
		{
			CString olMesSea;
			olMesSea.Format(GetString(IDS_STRING2248),m_From,m_Tfrom,olFSeas, m_To,m_Tto,olTSeas);
			//olMess += GetString(IDS_STRING1341) + CString("\n");//"Zeitraum beinhaltet mehr als eine Saison !\n"; 
			olMess += olMesSea + CString("\n");//"Zeitraum beinhaltet mehr als eine Saison !\n"; 
		}

		if (!m_Seas.IsEmpty())
		{
			CString olToStr;
			CString olFromStr;
			CTime olTo;
			CTime olFrom;

			ogBCD.GetField("SEA", "SEAS", m_Seas, "VPFR", olFromStr);
			ogBCD.GetField("SEA", "SEAS", m_Seas, "VPTO", olToStr );

			olTo = DBStringToDateTime(olToStr);
			olFrom = DBStringToDateTime(olFromStr);
			CString olSeaBeg = olFrom.Format("%d.%m.%Y");
			CString olSeaEnd = olTo.Format("%d.%m.%Y");

			if (olSeaBeg == m_From && olSeaEnd == m_To)
			{
				m_CE_Tfrom.SetInitText(olFrom.Format("%H%M"));
				m_CE_Tto.SetInitText(olTo.Format("%H%M"));
			}
			else
			{
				m_CE_Tfrom.SetInitText("0000");
				m_CE_Tto.SetInitText("2359");
			}
		}
		else
		{
			m_CE_Tfrom.SetInitText("0000");
			m_CE_Tto.SetInitText("2359");
		}

	}

	if(!olMess.IsEmpty())
	{
		MessageBox(olMess, GetString(ST_FEHLER));
		return;
	}


	// get 3-Letter-Code if necessary
	if (m_Alc.GetLength() < 3)
	{
		omAlc3 = ogBCD.GetField("ALT", "ALC2", m_Alc, "ALC3" );
	}
	else
	{
		omAlc3 = m_Alc;
	}

	if (m_Flns.IsEmpty())
		m_Flns=" ";

	CDialog::OnOK();
}

void SeasonCollectAskDlg::OnAlclist() 
{
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT","ALC2,ALC3,ALFN", "ALFN+,ALC2+");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_Alc.SetInitText(polDlg->GetField("ALC2"), true);	
	}
	delete polDlg;
}

void SeasonCollectAskDlg::OnSealist() 
{
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "SEA","SEAS,BEME,VPFR,VPTO", "VPFR+");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_Seas.SetInitText(polDlg->GetField("SEAS"), true);	

		CString olToStr;
		CString olFromStr;
		CTime olTo;
		CTime olFrom;

		m_CE_Seas.GetWindowText(m_Seas);

		ogBCD.GetField("SEA", "SEAS", m_Seas, "VPFR", olFromStr);
		ogBCD.GetField("SEA", "SEAS", m_Seas, "VPTO", olToStr );
		olTo = DBStringToDateTime(olToStr);
		olFrom = DBStringToDateTime(olFromStr);
		m_CE_From.SetInitText(olFrom.Format("%d.%m.%Y"));
		m_CE_To.SetInitText(olTo.Format("%d.%m.%Y"));

		m_CE_Tfrom.SetInitText(olFrom.Format("%H%M"));
		m_CE_Tto.SetInitText(olTo.Format("%H%M"));

	}
	delete polDlg;
}

LONG SeasonCollectAskDlg::OnEditKillfocus( UINT wParam, LPARAM lParam)
{
	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY*)lParam;


	CTime olFrom = TIMENULL;
	CTime olTo = TIMENULL;

	if(((UINT)m_CE_To.imID == wParam) || ((UINT)m_CE_From.imID == wParam))
	{
		m_CE_To.GetWindowText(m_To);
		m_CE_From.GetWindowText(m_From);

		olFrom = DateStringToDate(m_From); 
		olTo   = DateStringToDate(m_To); 
		
		if((olFrom != TIMENULL) && (olTo != TIMENULL))
		{
			prlNotify->UserStatus = true;
			if(olFrom > olTo)
			{
				prlNotify->Status = false;
			}
			else
			{
				prlNotify->Status = true;
			}
		}
		return 0L;
	}

	if((UINT)m_CE_Seas.imID == wParam)
	{
		CString olToStr;
		CString olFromStr;
		CTime olTo;
		CTime olFrom;

		m_CE_Seas.GetWindowText(m_Seas);

		if(prlNotify->Status = ogBCD.GetField("SEA", "SEAS", prlNotify->Text, "VPFR", olFromStr))
		{
			ogBCD.GetField("SEA", "SEAS", prlNotify->Text, "VPTO", olToStr );
			olTo = DBStringToDateTime(olToStr);
			olFrom = DBStringToDateTime(olFromStr);
			m_CE_From.SetInitText(olFrom.Format("%d.%m.%Y"));
			m_CE_To.SetInitText(olTo.Format("%d.%m.%Y"));

			m_CE_Tfrom.SetInitText(olFrom.Format("%H%M"));
			m_CE_Tto.SetInitText(olTo.Format("%H%M"));
		}
		
	}

	if((UINT)m_CE_Alc.imID == wParam)
	{
		CString olAlc2;
		CString olAlc3;
		prlNotify->UserStatus = true;
		prlNotify->Status = ogBCD.GetField("ALT", "ALC2", "ALC3", prlNotify->Text, olAlc2, olAlc3 );
		return 0L;
	}


	return 0L;
}
