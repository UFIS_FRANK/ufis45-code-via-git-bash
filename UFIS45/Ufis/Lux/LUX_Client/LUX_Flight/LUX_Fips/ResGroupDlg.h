#if !defined(RESGROUPDLG_H__INCLUDED_)
#define RESGROUPDLG_H__INCLUDED_



#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ResGroupDlg.h : header file
//

#include <CedaResGroupData.h>
#include <resrc1.h>
#include <CCSButtonCtrl.h>


/////////////////////////////////////////////////////////////////////////////
// ResGroupDlg dialog

class ResGroupDlg : public CDialog
{
// Construction
public:
	ResGroupDlg(CedaResGroupData *popResGroupData, CWnd* pParent);   // standard constructor
	~ResGroupDlg();

// Dialog Data
	//{{AFX_DATA(ResGroupDlg)
	enum { IDD = IDD_RESGROUP };
	CCSButtonCtrl	m_CB_Save;
	CCSButtonCtrl	m_CB_Delete;
	CComboBox m_CB_ResTypes;
	CListBox  m_LB_ResList;
	CComboBox m_CB_GroupNames;
	CListBox  m_LB_GroupResList;
 		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ResGroupDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ResGroupDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnOK();
	afx_msg void OnClose();
	afx_msg void OnResTypesChanged();
	afx_msg void OnGroupChanged();
	afx_msg void OnAddItems();
	afx_msg void OnRemoveItems();
	afx_msg void OnDeleteGroup();
	afx_msg void OnItemUp();
	afx_msg void OnItemDown();
	afx_msg void OnSaveGroup();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CWnd *pomParent;
	CedaResGroupData *pomResGroupData;

	bool ResetDlgItems(void);

	bool GetSelectedRes(CString &ropResType, CString &ropResFieldName) const;
	bool GetSelectedGroupName(CString &ropGroupName) const;

	bool RemoveDoubleItems(void);
	int FindItem(const CListBox &ropListBox, const CString &ropItem) const;
	int FindItem(const CComboBox &ropComboBox, const CString &ropItem) const;

	bool bmGroupChanged;
 
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(RESGROUPDLG_H__INCLUDED_)
