// Report17DailyTableViewer.cpp: Implementierungsdatei
//
//	Liste Daily/Season OPS-Plan

#include <stdafx.h>
#include <ReportSelectDlg.h>
#include <ReportPOPSItems.h>
#include <Report17DailyTableViewer.h>
#include <CcsGlobl.h>
#include <CedaBasicData.h>
#include <resrc1.h>

/* nur f�r Fileausgabe n�tig !
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
*/


#define	_MIT_TRACE_	FALSE	


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// Kennung f�r Transitflug !
const char csmTransit[] = "T" ;
// Standard Datums L�nge dd.mm.yyyy
const int  cimDateLength = 10 ;


static void SusViaExt( CString& opStr, char cpSign, char cpNeu = ' ' ) ; // Via Extension auf blank


// .................................................................. Ausgabezeilen sortieren

// Zeitbereichs-Starttermine ( Season ) vergleichen
static int CompareRangeStart( CString olNewStart, CString olCmpStart )
{
	CTime olNew = DateStringToDate( olNewStart );
	CTime olCmp = DateStringToDate( olCmpStart );
	return( olNew < olCmp ? -1 : 0  );
}

// ****** CompareLines aktuell( 9/98 ) out of order ( s. CompareFlights() ) ************ 
static int CompareLines( REPORT17TABLE_LINEDATA &rpNewLine, REPORT17TABLE_LINEDATA &rpCmpLine, bool bpCheckTime = false )
{
	// bpCheckTime <=> true, wenn Einteil das vorgeschriebene Format dd.mm.yyyy-.... besitzt ( = Season ! )
	// return 0 oder 1: => rpNewLine wird hinter rpCmpLine einsortiert	
	int ilRet=0 ;
	CString olNewDepart(rpNewLine.Std) ;
	CString olCmpDepart(rpCmpLine.Std) ;

	// 1. Abflugzeit
	if( rpNewLine.Std.GetLength() <= 0 ) olNewDepart = rpNewLine.Sta ;
	if( rpCmpLine.Std.GetLength() <= 0 ) olCmpDepart = rpCmpLine.Sta ;
	 
	if( olNewDepart == olCmpDepart )
	{
		// 2. Airline
		if( rpNewLine.Alc == rpCmpLine.Alc )
		{
			// 3. OutBound Fltn
			if( rpNewLine.FltnOut == rpCmpLine.FltnOut )
			{
				
				if( bpCheckTime )
				{
					// 4. InBound Fltn
					if( rpNewLine.FltnIn == rpCmpLine.FltnIn )
					{
						// 5. Zeitbereich ( nur f�r Daily Season ! )
						ilRet = CompareRangeStart( rpNewLine.Einteil.Left(cimDateLength), rpCmpLine.Einteil.Left(cimDateLength) );
					}
					else
					{
						ilRet = rpNewLine.FltnIn < rpCmpLine.FltnIn ? -1 : 0 ;
					}
				}
				else
				{
					// 4. InBound Fltn
					if( rpNewLine.FltnIn != rpCmpLine.FltnIn )
					{
						ilRet = rpNewLine.FltnIn < rpCmpLine.FltnIn ? -1 : 0 ;
					}
				}
			}
			else
				ilRet = rpNewLine.FltnOut < rpCmpLine.FltnOut ? -1 : 0 ;

		}
		else
			ilRet = rpNewLine.Alc < rpCmpLine.Alc ? -1 : 0 ;
	}
	else
		ilRet = olNewDepart < olCmpDepart ? -1 : 0 ;

	return ilRet ;
}



/////////////////////////////////////////////////////////////////////////////
// ------------------------------------------------ Report17DailyTableViewer


Report17DailyTableViewer::Report17DailyTableViewer(char *pcpInfo, char *pcpSelect, char *pspVR, char *pcpInfoPlus )
{
	bmSeason = false ;
	pcmInfo = pcpInfo;
	pcmInfoPlus = pcpInfoPlus;
	pcmSelect = pcpSelect;
    pomTable = NULL;
	// Transit als Verkehrsrechte anzeigen
	olVerkehrsRechte = csmTransit ;
	if( pspVR != NULL ) olVerkehrsRechte += pspVR ;

}

Report17DailyTableViewer::~Report17DailyTableViewer()
{
    DeleteAll();
}

void Report17DailyTableViewer::Attach(CTable *popTable)
{
    pomTable = popTable;
}



void Report17DailyTableViewer::ChangeViewTo(const char *pcpViewName)
{

	// Daily or Seasonal ?	
	if( strcmp( pcpViewName, GetString( IDS_STRING1400 ) ) == 0 )
	{
		bmSeason = false ;
	}
	else 
	{
		bmSeason = true ;
	}
    pomTable->ResetContent();
    DeleteAll();    
    MakeLines();

	CompressLines();

	UpdateDisplay();
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}


/////////////////////////////////////////////////////////////////////////////
// Report17DailyTableViewer -- code specific to this class

void Report17DailyTableViewer::MakeLines()
{

	DAILYRKEYLIST *prlRkey;
	POSITION pos;
	void *pVoid;


	for( pos = omDailyCedaFlightData.omRkeyMap.GetStartPosition(); pos != NULL; )
	{
		omDailyCedaFlightData.omRkeyMap.GetNextAssoc( pos, pVoid, (void *&)prlRkey );

		if(prlRkey->Rotation.GetSize() == 1)			// ein unverkn�pfter Flug
		{
			if(!strcmp(prlRkey->Rotation[0].Des3, pcgHome))
			{
				MakeLine(prlRkey->Rotation[0].Urno, NULL);
			}
			else if(!strcmp(prlRkey->Rotation[0].Org3, pcgHome))
			{
				MakeLine(NULL, prlRkey->Rotation[0].Urno);
			}
		}

		else if(prlRkey->Rotation.GetSize() == 2)		//  verkn�pfter Flug
		{
			if(strcmp(prlRkey->Rotation[0].Dooa, prlRkey->Rotation[1].Dood))  // Verkn�pfung l�sen
			{
//#########
				MakeLine(prlRkey->Rotation[0].Urno, prlRkey->Rotation[1].Urno);
//				MakeLine(prlRkey->Rotation[0].Urno, NULL);
//				MakeLine(NULL, prlRkey->Rotation[1].Urno);
			}
			else
			{
				MakeLine(prlRkey->Rotation[0].Urno, prlRkey->Rotation[1].Urno);
			}
		}
	}

//generate the headerinformation
	//calculate the numbers of ARR and DEP
	int ilCntrArr = 0;
	int ilCntrDep = 0;
	int ilCntrBoth = 0;

	for(int j = 0; j < omDailyCedaFlightData.omData.GetSize(); j++ ) 
	{

		ROTATIONDLGFLIGHTDATA *prlFlight = &omDailyCedaFlightData.omData[j];
//		ROTATIONDLGFLIGHTDATA *prlFlight = &omDailyCedaFlightData.omData->GetAt(j);

		//Arrival
		if((strcmp(prlFlight->Org3, pcgHome) != 0) && (strcmp(prlFlight->Des3, pcgHome) == 0))
		{
			ilCntrArr++;
		}
		else
		{
			// Departure
			if((strcmp(prlFlight->Org3, pcgHome) == 0) && (strcmp(prlFlight->Des3, pcgHome) != 0))
			{
				ilCntrDep++;
			}
			else
			{
				//Turnaround
				if((strcmp(prlFlight->Org3, pcgHome) == 0) && (strcmp(prlFlight->Des3, pcgHome) == 0))
				{
					ilCntrBoth++;
				}
			}
		}
	}

	CString olTimeSet = GetString(IDS_STRING1920); //UTC
	if(bgReportLocal)
		olTimeSet = GetString(IDS_STRING1921); //LOCAL

	CString olKindOf ("");
	if(bmSeason)
		olKindOf = GetString(IDS_STRING1411);
	else
		olKindOf = GetString(IDS_STRING1400);


	char pclHeader[256];
	sprintf(pclHeader, GetString(IDS_R_HEADER17), olKindOf, pcmSelect, pcmInfo, ilCntrArr+ilCntrDep, ilCntrArr, ilCntrDep, olTimeSet, pcmInfoPlus);
	omTableName = pclHeader;


//		ROTATIONDLGFLIGHTDATA *prlAf = omDailyCedaFlightData.GetFlightByUrno(lpAUrno);


}



//----------------------------------------------------------------------------------------
//------------------------------------------------	eine Tabellen Zeile aufbauen ---------

void Report17DailyTableViewer::MakeLine(long lpAUrno, long lpDUrno)
{
//######
	CString olKonvZeitraum = " ";

	DAILYITEMS rlDailyItems( DAILYITEMS::DailyFlight ) ;
	CString olCmpLine ;
	CString olBar("|") ;
	CString olCanceled("CAX") ;
	bool blLineSet = false ;
	long llUrno;
	// ToDO: evt. parametriebar machen
	bool blACT5 = true;
	int ilFfRem = 0 ;
	int ilFfVia = 0 ;

	// unterschiedliche Frequenzen bei Season
	int ilFreqAnz = 1 ;
	CString olSaisonIn ;	// kommagetrennt: Zeitr�ume
	CString olSaisonOut ;
	CStringArray olStrArray ;	// Array mit Zeitraum Strings
	
	// SUS f�r Saisonbemerkung parametrisieren
	const int ilSRemLength = 15 ;	// f�r eine Saisonbemerkung
	const int ilSRemMaxLength = 22 ;	// f�r In/OutSaisonbemerkung
	CString olSaisonRem ;	

	// Zeitraum definieren !!
	CString olZeitraum ( CString(pcmSelect) + pcmInfo ) ;
	CString *polZeitraum ;


	rlDailyItems.GetEmptyFormatString( olCmpLine ) ;  // Preset: leere Zeile
	rlDailyItems.Folgezeile = 0 ;  
	olSaisonIn.Empty() ;
	olSaisonOut.Empty() ;
	olSaisonRem.Empty() ;


	if(lpAUrno)	// InBound
	{
		ROTATIONDLGFLIGHTDATA *prlAf = omDailyCedaFlightData.GetFlightByUrno(lpAUrno);
		if(prlAf != NULL)
		{
			CTime olStoa = prlAf->Stoa;
			if(bgReportLocal) ogBasicData.UtcToLocal(olStoa);
			CString olStoaFormat = olStoa.Format("%H:%M");

			// Alc2 oder Alc2/Alc3 ?
			rlDailyItems.Alc.Str = prlAf->Alc2 ;
			if( rlDailyItems.IsFagSpecial( prlAf->Alc2 ))
			{
				rlDailyItems.Alc.Str += CString("/")+prlAf->Alc3 ;
			}
			rlDailyItems.Alc.Str += olBar ;
			rlDailyItems.FltnIn.Str = prlAf->Fltn ;
			rlDailyItems.FltnIn.Str += ( strcmp( prlAf->Flns, " " ) != 0 ) ? prlAf->Flns + olBar : olBar ;  
			rlDailyItems.FltnOut.Str = olBar ;
			rlDailyItems.Act.Str = blACT5 ? prlAf->Act5+ olBar : prlAf->Act3+ olBar;
			// Cargo Flug ?
			rlDailyItems.Nose.Str = ( strcmp( prlAf->Ttyp, "CG") == 0 ) ? prlAf->Ttyp : prlAf->Nose ;
			rlDailyItems.Nose.Str += olBar;

			// Verkehrsrechte an Origin
			rlDailyItems.Org.Str = prlAf->Org3 ;
			if( olVerkehrsRechte.IsEmpty() || CString(prlAf->Htyp).FindOneOf( olVerkehrsRechte ) >=0 ) 
			{
				rlDailyItems.Org.Str += CString("(")+prlAf->Htyp+CString(")") ;
			}
			rlDailyItems.Org.Str += olBar ;

			rlDailyItems.Sta.Str = olStoaFormat+olBar ;
			rlDailyItems.Des.Str = olBar ;
			rlDailyItems.Std.Str = olBar ;
			rlDailyItems.ChtDes.Str = olBar;	
			rlDailyItems.Pos.Str = olBar ;	// Position immer von Hand !nur Dep!
			
			//  DAILY / SEASON trennen
			if( !bmSeason )
			{
				// Gates und Counter immer von Hand
//				rlDailyItems.Gp.Str = olBar ;
				rlDailyItems.Ctr.Str = olBar ;
				rlDailyItems.Pax.Str = prlAf->Paxt+olBar ;
				// Canceled ?
//				if( strcmp( prlAf->Ftyp, "X" ) == 0 ) rlDailyItems.Einteil.Str = olCanceled ;
//				if( strcmp( prlAf->Ftyp, "X" ) == 0 ) rlDailyItems.Einteil.Str = "X-" ;
				if ( strcmp( prlAf->Ftyp, "" ) == 0 )
					rlDailyItems.Einteil.Str = CString("?");
				else
					rlDailyItems.Einteil.Str = prlAf->Ftyp;
			}
			else
			{
				// ToDo: evt. neuen ITEM Status ?
/*				if( strcmp( prlAf->Ftyp, "S" ) == 0 )
				{
					rlDailyItems.Gp.Str = " *"+olBar ;
				}
				else if( strcmp( prlAf->Ftyp, "O" ) )
				{
					rlDailyItems.Gp.Str = CString("[")+prlAf->Ftyp+CString("]")+olBar ;
				}
				else
				{
					rlDailyItems.Gp.Str = olBar ;
				}
*/
				// Canceled ?
				if( strcmp( prlAf->Ftyp, "X" ) == 0 )
				{
					rlDailyItems.Gp.Str = "X";
				}
				if( strcmp( prlAf->Ftyp, "N" ) == 0 )
				{
					rlDailyItems.Gp.Str = "N";
				}

				rlDailyItems.Ctr.Str = olBar ;
				rlDailyItems.Pax.Str = olBar ;

				// validen Zeitraum ermitteln
				olSaisonIn.Empty() ;

/*
				if( strcmp(prlAf->Fltn, "989") == 0 )
				{
					TRACE("Flug [%s] Urno [%ld] Rem[%s]\n", prlAf->Fltn, prlAf->Urno, prlAf->Rem2 ) ;
				}
*/
//				polZeitraum = omDailyCedaFlightData.GetRangeByUrno(lpAUrno, olSaisonIn, false); 
				polZeitraum = omDailyCedaFlightData.GetRangeByUrno(lpAUrno, olSaisonIn, false, 0L, true); 
				olKonvZeitraum = *polZeitraum;
				olSaisonRem = CString( prlAf->Rem2 ).Left( ilSRemLength ) ;
//######################
			if(bgReportLocal)
			{
				CTime olStoa = prlAf->Stoa;
				ogBasicData.UtcToLocal(olStoa);
				olKonvZeitraum = olStoa.Format("%d.%m.%Y") + "-" + olStoa.Format("%d.%m.%Y");
			}


//######################

			}

			rlDailyItems.RemIn.Str = prlAf->Rem1 ;
			ilFfRem = rlDailyItems.GetNoOfRem( rlDailyItems.RemIn.Str ) ;
//			rlDailyItems.Einteil.Str += prlAf->Rem1;//?

			if( atoi(prlAf->Vian) > 0 )	 
			{
				if( rlDailyItems.TwoVias )
				{
					ilFfVia = (atoi(prlAf->Vian) + 1) / 2 ;
				}
				else
				{
					ilFfVia = atoi(prlAf->Vian) ;
				}
				// return false: keine Vias trotz Vian > 0 !?
				VERIFY( GetVias( rlDailyItems.ViaIn.Str, prlAf, CString() ) )  ;
			}
			
			blLineSet = true ;

		}	// if(prlAf ...

		llUrno = lpAUrno;  // Inhalt Keymap
	} // end if(lpAUrno )
	else
	{
		llUrno = lpDUrno;  // Inhalt Keymap
	}


	if(lpDUrno)	// OutBound
	{
		ROTATIONDLGFLIGHTDATA *prlDf = omDailyCedaFlightData.GetFlightByUrno(lpDUrno);
		if(prlDf != NULL)
		{
			CTime olStod = prlDf->Stod;
			if(bgReportLocal) ogBasicData.UtcToLocal(olStod);
			CString olStodFormat = olStod.Format("%H:%M");

			rlDailyItems.FltnOut.Str = prlDf->Fltn ;
			rlDailyItems.FltnOut.Str += ( strcmp( prlDf->Flns, " " ) != 0 ) ? prlDf->Flns + olBar : olBar ;  

			// Verkehrsrechte an Destination
			rlDailyItems.Des.Str = prlDf->Des3 ;
			if( olVerkehrsRechte.IsEmpty() || CString(prlDf->Htyp).FindOneOf( olVerkehrsRechte ) >=0 ) 
			{
				rlDailyItems.Des.Str += CString("(")+prlDf->Htyp+CString(")") ;
			}
			rlDailyItems.Des.Str += olBar ;

			rlDailyItems.Std.Str = olStodFormat+olBar ;
			rlDailyItems.ChtDes.Str = prlDf->Cht3+olBar;			
			rlDailyItems.Pos.Str = prlDf->Pstd + olBar ;	// immer von Hand
			
			// Season / Daily trennen
			if( !bmSeason )
			{
				CString olGate1 = CString(prlDf->Gtd1);
				CString olGate2 = CString(prlDf->Gtd2);
				// Canceled ?
				if( !lpAUrno )
					rlDailyItems.Einteil.Str = CString(" ");

				if ( strcmp( prlDf->Ftyp, "" ) == 0 )
					rlDailyItems.Einteil.Str += CString("-") + CString("?");
				else
					rlDailyItems.Einteil.Str += CString("-") + prlDf->Ftyp;
/*				if( strcmp( prlDf->Ftyp, "X" ) == 0 )
				{
//					rlDailyItems.Einteil.Str = olCanceled ;
					rlDailyItems.Einteil.Str += "-X";
				}*/
//				rlDailyItems.Gp.Str = olBar ;
				if(olGate1.GetLength() != 0 || olGate2.GetLength() != 0)
					rlDailyItems.Gp.Str = prlDf->Gtd1 + CString("/") + CString(prlDf->Gtd2);// + olBar ;
//				else
//					rlDailyItems.Gp.Str = olBar ;

				CString olCtr = CString(prlDf->Ckif) + CString("-") + CString(prlDf->Ckit) + olBar;
				rlDailyItems.Ctr.Str = olCtr;	// immer von Hand
			}
			else
			{
				// evt. neuen ITEM Status einf�gen ( f�r G/P )
/*				if( strcmp(prlDf->Ftyp,"S" ) == 0 ) 
				{
					rlDailyItems.Gp.Str = " *"+olBar ;
				}
				else if( strcmp( prlDf->Ftyp, "O" ) )
				{
					rlDailyItems.Gp.Str = CString("[")+prlDf->Ftyp+CString("]")+olBar ;
				}
				else
				{
					rlDailyItems.Gp.Str = olBar ;
				}
*/
				// Canceled ?
				if( !lpAUrno )
					rlDailyItems.Gp.Str = CString(" ");

				if( strcmp( prlDf->Ftyp, "X" ) == 0 )
				{
					rlDailyItems.Gp.Str += "-X";
				}
				if( strcmp( prlDf->Ftyp, "N" ) == 0 )
				{
					rlDailyItems.Gp.Str += "-N";
				}


				// validen Zeitraum ermitteln
				olSaisonOut.Empty() ;
				long ilRef = 0L ;
				if( lpAUrno )
				{
					ilRef = omDailyCedaFlightData.GetFlightByUrno(lpAUrno)->Urno ;
				}
//				polZeitraum = omDailyCedaFlightData.GetRangeByUrno(lpDUrno, olSaisonOut, false, ilRef ); 
				polZeitraum = omDailyCedaFlightData.GetRangeByUrno(lpDUrno, olSaisonOut, false, ilRef, true ); 
				olKonvZeitraum = *polZeitraum;
//######################
			if(bgReportLocal)
			{
				CTime olStod = prlDf->Stod;
				ogBasicData.UtcToLocal(olStod);
				olKonvZeitraum = olStod.Format("%d.%m.%Y") + "-" + olStod.Format("%d.%m.%Y");
			}


//######################
			}

			rlDailyItems.RemOut.Str = prlDf->Rem1 ;
//			rlDailyItems.Einteil.Str += prlDf->Rem1;//?
			ilFfRem = rlDailyItems.GetNoOfRem( rlDailyItems.RemOut.Str ) ;
			if( atoi(prlDf->Vian) > 0 )	 
			{
				CString olFirstVia ;
				if( rlDailyItems.TwoVias )
				{
					ilFfVia = (atoi(prlDf->Vian) + 1) / 2 ;
				}
				else
				{
					ilFfVia = atoi(prlDf->Vian) ;
				}
				// return false: keine Vias trotz Vian > 0 !?
				if( GetVias( rlDailyItems.ViaOut.Str, prlDf, olFirstVia ) ) 
				{
					rlDailyItems.Des.Str = olFirstVia+olBar ;
				}
			}

			if( ! lpAUrno )									// nur OutBound Flug
			{
				// Alc2 oder Alc2/Alc3 ?
				rlDailyItems.Alc.Str = prlDf->Alc2 ;
				if( rlDailyItems.IsFagSpecial( prlDf->Alc2 ))
				{
					rlDailyItems.Alc.Str += CString("/")+prlDf->Alc3 ;
				}
				rlDailyItems.Alc.Str += olBar ;
				rlDailyItems.FltnIn.Str = olBar ;
				rlDailyItems.Org.Str = olBar ;
				rlDailyItems.Sta.Str = olBar ;
				rlDailyItems.Act.Str = blACT5 ? prlDf->Act5 + olBar : prlDf->Act3+ olBar;
				// Cargo Flug ?
				rlDailyItems.Nose.Str = ( strcmp( prlDf->Ttyp, "CG") == 0 ) ? prlDf->Ttyp : prlDf->Nose ;
				rlDailyItems.Nose.Str += olBar;
				
				// keine Inbound Items
				rlDailyItems.FltnIn.Str = olBar ;
				rlDailyItems.Org.Str = olBar ;
				rlDailyItems.Sta.Str = olBar ;

				// Season / Daily trennen
				if( !bmSeason )
				{
//rkr					rlDailyItems.Gp.Str = olBar ;
					rlDailyItems.Ctr.Str = olBar ;
					rlDailyItems.Pax.Str = prlDf->Paxt+olBar ;
				}
				else
				{
					
/*					if( strcmp(prlDf->Ftyp,"S" ) != 0 && strcmp( prlDf->Ftyp, "O" ) != 0 )
					{
						rlDailyItems.Gp.Str = olBar ;
					}*/
					rlDailyItems.Ctr.Str = olBar ;
					rlDailyItems.Pax.Str = olBar ;
					olSaisonRem = CString( prlDf->Rem2 ).Left( ilSRemLength ) ;
				}

			}	// if( ! lpAUrno ..
			else							// Trunaround mit verkn�pftem InBound Flug 
			{
				int ilRemIn = ilFfRem ; 
				int ilViaIn = ilFfVia ; 

				ilFfRem = max( ilRemIn, ilFfRem ) ;
				ilFfVia = max( ilViaIn, ilFfVia ) ;

				if( bmSeason ) 
				{
					// validen Zeitraum InBound f�r diesen OutBound ermitteln
					olSaisonIn.Empty() ;
//					omDailyCedaFlightData.GetRangeByUrno(lpAUrno, olSaisonIn, false, lpDUrno ); 
					omDailyCedaFlightData.GetRangeByUrno(lpAUrno, olSaisonIn, false, lpDUrno, true ); 
					if( strlen(prlDf->Rem2) ) 
					{
						olSaisonRem += "/" + CString( prlDf->Rem2 ).Left( ilSRemMaxLength - olSaisonRem.GetLength() ) ;
					}
				}

			}	// end Trunaround 

			blLineSet = true ;

		}	// if(prlDf ...

		llUrno = lpDUrno;  // Inhalt Keymap

	}	// if(lpDUrno ...

//	if (!bmSeason)
		rlDailyItems.Gp.Str += olBar;

	rlDailyItems.Folgezeile = ilFfRem + ilFfVia ;	// Summe der Folgezeilen 


	// f�r Saisondarstellung L�cken im Zeitraum als eigene Druckzeile ausgeben
	// ...... macht das f�r !blLineSet Sinn ??
	if( bmSeason )
	{
		int ilInFreq ;
		int ilOutFreq ;

		olStrArray.RemoveAll() ;

		if( (ilInFreq = GetItemCount( olSaisonIn )) != (ilOutFreq = GetItemCount( olSaisonOut )) )
		{

			// kann nur f�r (0 != x) gelten: Unverbundener Flug
			if( ilOutFreq )
			{
				// Outbound 
				if( ilOutFreq > 1 )	
				{
					if( (ilOutFreq = ExtractItemList( olSaisonOut, &olStrArray)) < 2) 
					{
						ilFreqAnz = 1 ;
					}
					else
					{
						ilFreqAnz = ilOutFreq ;
					}
				}
				else
				{
//####					if( polZeitraum != NULL ) olStrArray.Add( *polZeitraum ) ;
					if( polZeitraum != NULL ) olStrArray.Add( olKonvZeitraum ) ;
					else olStrArray.Add( CString(" ") ) ;
				}
			}
			else
			{
				// Inbound 
				if( ilInFreq > 1 )	
				{
					if( (ilInFreq = ExtractItemList( olSaisonIn, &olStrArray)) < 2) 
					{
						ilFreqAnz = 1 ;
					}
					else
					{
						ilFreqAnz = ilInFreq ;
					}
				}
				else
				{
					if( polZeitraum != NULL ) 
					{
//######
//						olStrArray.Add( *polZeitraum ) ;
						olStrArray.Add( olKonvZeitraum ) ;
					}
					else
					{
						olStrArray.Add( CString(" ") ) ;
					}
				}
			}
		}
		else
		{
			// Rotation
			// olSaisonIn + olSaisonOut immer gleiche Anzahl von Items !!
			// jeder Frequenzstring mu� die vorgeschriebene Form "dd.mm.yyyy-dd.mm.yyyy......." haben !
			if( ilInFreq != 0 )
			{
				CStringArray olInArray ;
				CStringArray olOutArray ;
				CString olInTag ;
				CString olOutTag ;
				CString olTag ;
				CString olHyph("-") ;
				CString olDot(".") ;

				ExtractItemList( olSaisonIn, &olInArray) ; 
				ExtractItemList( olSaisonOut, &olOutArray) ; 
				for( int index = 0; index < ilInFreq; index++ )
				{
					olInTag = olInArray[index].Mid(6,4)+olInArray[index].Mid(3,2)+olInArray[index].Left(2) ;
					olOutTag = olOutArray[index].Mid(6,4)+olOutArray[index].Mid(3,2)+olOutArray[index].Left(2) ; 
					olTag = ( olInTag > olOutTag ) ? olInTag : olOutTag ; 
					olTag = olTag.Right(2)+olDot+olTag.Mid(4,2)+olDot+olTag.Left(4)+olHyph ;

					olInTag = olInArray[index].Mid(17,4)+olInArray[index].Mid(14,2)+olInArray[index].Mid(11,2) ;
					olOutTag = olOutArray[index].Mid(17,4)+olOutArray[index].Mid(14,2)+olOutArray[index].Mid(11,2) ; 
					olInTag = ( olInTag < olOutTag ) ? olInTag : olOutTag ; 
					olTag += olInTag.Right(2)+olDot+olInTag.Mid(4,2)+olDot+olInTag.Left(4) ;
					
					olStrArray.Add( olTag ) ;
				}
				ilFreqAnz = ilInFreq ;
				
			}
			else
			{
//####				if( polZeitraum != NULL ) olStrArray.Add( *polZeitraum ) ;
				if( polZeitraum != NULL ) olStrArray.Add( olKonvZeitraum ) ;
				else olStrArray.Add( CString(" ") ) ;
			}
		}
	
	}	// end if( bmSeason ...
	rlDailyItems.Einteil.Str += olBar ;

	

	// DailyItems definiert => pro Frequenz eine Zeile erzeugen - f�r (!bmSeason) immer nur eine ! 
	for( int index=0; index < ilFreqAnz; index++ )
	{
		REPORT17TABLE_LINEDATA  *prlLineData = new REPORT17TABLE_LINEDATA;
	
		if( bmSeason )
		{
			ShortRangeString( olStrArray[index].Left(cimFullRangeLen) ) ;
			rlDailyItems.Einteil.Str = olStrArray[index].Left(cimFullRangeLen) + CString(" ")+ olSaisonRem+ olBar ;
		}

		if( blLineSet )
		{
			// neue Tabellenzeile f�llen
			olCmpLine = rlDailyItems.Alc.Str ;
			olCmpLine += rlDailyItems.FltnIn.Str ;
			olCmpLine += rlDailyItems.FltnOut.Str;
			olCmpLine += rlDailyItems.Act.Str;
			olCmpLine += rlDailyItems.Nose.Str;
			olCmpLine += rlDailyItems.Org.Str ;
			olCmpLine += rlDailyItems.Sta.Str ;
			olCmpLine += rlDailyItems.Des.Str  ;
			olCmpLine += rlDailyItems.Std.Str ;
			olCmpLine += rlDailyItems.ChtDes.Str ;	
			olCmpLine += rlDailyItems.Pos.Str ;
			olCmpLine += rlDailyItems.Gp.Str ;
			olCmpLine += rlDailyItems.Ctr.Str  ;
			olCmpLine += rlDailyItems.Pax.Str  ;
			olCmpLine += rlDailyItems.Einteil.Str ;

			// neue Druckzeile f�llen
			prlLineData->Urno = llUrno;
			prlLineData->Alc =		rlDailyItems.Alc.Str ;
			prlLineData->FltnIn =	rlDailyItems.FltnIn.Str ;
			prlLineData->FltnOut =	rlDailyItems.FltnOut.Str;
			prlLineData->Act =		rlDailyItems.Act.Str;
			prlLineData->Nose =		rlDailyItems.Nose.Str;
			prlLineData->Org =		rlDailyItems.Org.Str ;
			prlLineData->Sta =		rlDailyItems.Sta.Str ;
			prlLineData->RemIn =	rlDailyItems.RemIn.Str  ;
			prlLineData->ViaIn =	rlDailyItems.ViaIn.Str  ;
			prlLineData->Des =		rlDailyItems.Des.Str  ;
			prlLineData->Std =		rlDailyItems.Std.Str ;
			prlLineData->ChtDes =	rlDailyItems.ChtDes.Str ;	
			prlLineData->RemOut =	rlDailyItems.RemOut.Str  ;
			prlLineData->ViaOut =	rlDailyItems.ViaOut.Str  ;
			prlLineData->Pos =		rlDailyItems.Pos.Str ;
			prlLineData->Gp =		rlDailyItems.Gp.Str ;
			prlLineData->Ctr =		rlDailyItems.Ctr.Str  ;
			prlLineData->Pax =		rlDailyItems.Pax.Str  ;
			prlLineData->Einteil = rlDailyItems.Einteil.Str ;
			prlLineData->Folgezeile = rlDailyItems.Folgezeile ;
			if( _MIT_TRACE_ )
				TRACE( "Anzahl Ff [%s / %s] [%d] \n", prlLineData->FltnIn, prlLineData->FltnOut, rlDailyItems.Folgezeile ) ;
		}


		// Datensatz in die Keymaps einf�gen 
		DAILYURNOKEYLIST *prlUrno; 
		if (olCmpLine.GetLength())	// Hier immer, da mit (||||..) vorbelegt !?
		{
			if(omLineKeyMap.Lookup(olCmpLine,(void *& )prlUrno) == TRUE)
			{
				prlUrno->Rotation.Add(prlLineData);
			}
			else
			{
				prlUrno = new DAILYURNOKEYLIST;
				omLineKeyMap.SetAt(olCmpLine,prlUrno);
				prlUrno->Rotation.Add(prlLineData);
			}
		}
		else
		{
			delete prlLineData;
		}
	}	// end for( int index ...

}



void Report17DailyTableViewer::CompressLines(void) 
{
	//TRACE("CompressLines Start: %s\n", CTime::GetCurrentTime().Format("%H:%M:%S"));

	POSITION pos1, pos2;
	CString olCmpLine1, olCmpLine2;
	DAILYURNOKEYLIST *prlUrno1, *prlUrno2;  
	REPORT17TABLE_LINEDATA *prlUrno1Data=NULL;
	REPORT17TABLE_LINEDATA *prlUrno2Data=NULL;
	CString olNewRange;
	
	for(pos1 = omLineKeyMap.GetStartPosition(); pos1 != NULL; )	// Schleife �ber alle Zeilen
	{
		omLineKeyMap.GetNextAssoc( pos1, olCmpLine1, (void *&)prlUrno1 );
		for(pos2 = omLineKeyMap.GetStartPosition(); pos2 != NULL; )	// Schleife �ber alle Zeilen
		{
			omLineKeyMap.GetNextAssoc( pos2, olCmpLine2, (void *&)prlUrno2 );
			if (pos1 == pos2) continue;

			prlUrno1Data = &prlUrno1->Rotation[0];
			prlUrno2Data = &prlUrno2->Rotation[0];

			if (prlUrno1Data && prlUrno2Data && IsEqualLine(*prlUrno1Data, *prlUrno2Data))
			{
 				// Ist Zusammenfassung m�glich
				if (MakeTimeRange(*prlUrno1Data, *prlUrno2Data, olNewRange))
				{
					// Zeitraum zusammenfassen
					CString olNewCmdLine=olCmpLine1;

					olNewCmdLine.Replace(prlUrno1Data->Einteil, olNewRange);
					prlUrno1Data->Einteil = olNewRange;
				
					// Alte Eintr�ge l�schen
					omLineKeyMap.RemoveKey(olCmpLine1);
					omLineKeyMap.RemoveKey(olCmpLine2);
					// Neuer Eintrag anlegen
					omLineKeyMap.SetAt(olNewCmdLine, prlUrno1);

					pos2=NULL;
					pos1=omLineKeyMap.GetStartPosition();
				}
			}


		}

		
	}		
	//TRACE("CompressLines End: %s\n", CTime::GetCurrentTime().Format("%H:%M:%S"));

}


bool Report17DailyTableViewer::MakeTimeRange(const REPORT17TABLE_LINEDATA &rrpLine1, const REPORT17TABLE_LINEDATA &rrpLine2, CString &ropNewRange)
{

	CTime olFrom1 = DateStringToDate(rrpLine1.Einteil.Left(10));
	CTime olTo1   = DateStringToDate(rrpLine1.Einteil.Mid(11,10)) ;
	CTime olFrom2 = DateStringToDate(rrpLine2.Einteil.Left(10));
	CTime olTo2   = DateStringToDate(rrpLine2.Einteil.Mid(11,10)) ;

	const CTimeSpan ol7Days(7,0,0,0);
	// Unterscheiden sich die from- und to-Zeiten in genau 7 Tagen?
	if (olTo1 + ol7Days == olFrom2)
	{
		ropNewRange = olFrom1.Format("%d.%m.%Y")+olTo2.Format("-%d.%m.%Y")+
			rrpLine1.Einteil.Right(rrpLine1.Einteil.GetLength()-21);
		return true;
	}
	if (olTo2 + ol7Days == olFrom1)
	{
		ropNewRange = olFrom2.Format("%d.%m.%Y")+olTo1.Format("-%d.%m.%Y")+
			rrpLine1.Einteil.Right(rrpLine1.Einteil.GetLength()-21);
		return true;
	}

	return false;
}


// Sind die beiden Tabellenzeilen bis auf den Zeitraum identisch?
bool Report17DailyTableViewer::IsEqualLine(const REPORT17TABLE_LINEDATA &rrpLine1, const REPORT17TABLE_LINEDATA &rrpLine2)
{
	if (rrpLine1.Act != rrpLine2.Act) return false;
	if (rrpLine1.FltnIn != rrpLine2.FltnIn) return false;
	if (rrpLine1.FltnOut != rrpLine2.FltnOut) return false;
	if (rrpLine1.Alc != rrpLine2.Alc) return false;
	if (rrpLine1.Nose != rrpLine2.Nose) return false;
	if (rrpLine1.Org != rrpLine2.Org) return false;
	if (rrpLine1.Sta != rrpLine2.Sta) return false;
	if (rrpLine1.ChtOrg != rrpLine2.ChtOrg) return false;
	if (rrpLine1.ViaIn != rrpLine2.ViaIn) return false;
	if (rrpLine1.RemIn != rrpLine2.RemIn) return false;
	if (rrpLine1.Des != rrpLine2.Des) return false;
	if (rrpLine1.Std != rrpLine2.Std) return false;
	if (rrpLine1.ChtDes != rrpLine2.ChtDes) return false;
	if (rrpLine1.ViaOut != rrpLine2.ViaOut) return false;
	if (rrpLine1.RemOut != rrpLine2.RemOut) return false;
	if (rrpLine1.Pos != rrpLine2.Pos) return false;
	if (rrpLine1.Gp != rrpLine2.Gp) return false;
	if (rrpLine1.Ctr != rrpLine2.Ctr) return false;
	if (rrpLine1.Pax != rrpLine2.Pax) return false;
 
	return true;
}



// Frequenzen (Zeitr�ume) eines Fluges "opStrArray" aufsteigend sortiert in "ropSortFreq" zur�ckliefern
// ****** out of order ***************
void Report17DailyTableViewer::SortFreqStrArray( CStringArray& ropSortFreq, CStringArray& opStrArray )
{
	int ilAnz = opStrArray.GetSize() ;
	int ilSortMax = 0 ;
	CTime olOld, olNew ;

	if( ilAnz == 0 ) return;
	else ropSortFreq.SetAtGrow(0, opStrArray[0] ) ;
 
	for( int index =1 ; index < ilAnz; index++)
	{
		olNew = DateStringToDate( opStrArray[index].Left(cimDateLength) );
		ilSortMax = ropSortFreq.GetSize() ;
		for( int indexSort = 0; indexSort < ilSortMax; indexSort++)
		{
			olOld = DateStringToDate( ropSortFreq[indexSort].Left(cimDateLength) );
			if( olNew < olOld ) break ;
		}
		ropSortFreq.SetAtGrow(indexSort, opStrArray[index] ) ;
	}
}


bool Report17DailyTableViewer::GetVias(CString & popStr, ROTATIONDLGFLIGHTDATA *prpFlight, CString & ropDes )
{
	// Vialiste auswerten und in popStr zur�ckliefern
	// f�r Departure Fl�ge wird Destination (ropDes) am Ende angehangen, 1. VIA nimmt Platz von Destination ein !
	
	int ilAnzVias = 0 ;
	CCSPtrArray<VIADATA> olViaListe ;
		
	if( (ilAnzVias = omDailyCedaFlightData.GetViaArray( &olViaListe, prpFlight )) < 1) return false ;
	
	for( int ilVia=0; ilVia < ilAnzVias; ilVia++ )
	{
		CString olVKCheck(olViaListe[ilVia].Fids) ;
		if( ilVia == 0 && strcmp(prpFlight->Adid,"D") == 0 ) 
		{
			ropDes = olViaListe[ilVia].Apc3 ;
			if( olVerkehrsRechte.IsEmpty() || olVKCheck.FindOneOf( olVerkehrsRechte ) >=0 ) 
			{
				ropDes += CString("(")+olViaListe[ilVia].Fids+CString(")") ;
			}
		}
		else
		{
			popStr += olViaListe[ilVia].Apc3 ;
			if( olVerkehrsRechte.IsEmpty() || olVKCheck.FindOneOf( olVerkehrsRechte ) >=0 ) 
			{
				popStr += CString("(")+olViaListe[ilVia].Fids+CString(")") ;
			}
			else
			{
				//RST popStr += "���" ;	// SUS wegen Rest on screen, s. SusViaExt() !
				popStr += "???" ;	// SUS wegen Rest on screen, s. SusViaExt() !
			}
			popStr += " " ;
		}
	}

	if( strcmp(prpFlight->Adid,"D") == 0) 
	{
		popStr += prpFlight->Des3 ;
		// Verkehrsrechte an Destination
		if( olVerkehrsRechte.IsEmpty() || CString(prpFlight->Htyp).FindOneOf( olVerkehrsRechte ) >=0 ) 
		{
			popStr += CString("(")+prpFlight->Htyp+CString(")") ;
		}
	}

	return true ;
}


/////////////////////////////////////////////////////////////////////////////
// Report17DailyTableViewer - display drawing routine


void Report17DailyTableViewer::GetHeader()
{
 CString olCmpLine;
 char olHeadLine[256];
 DAILYITEMS olDailyItems( DAILYITEMS::DailyFlight ) ;
 CString olFormatList ;


	if( bmSeason )
	{
		sprintf(olHeadLine,GetString(IDS_STRING1414));
	}
	else
	{
		sprintf(olHeadLine,GetString(IDS_STRING1402));
	}
	pomTable->SetHeaderFields(olHeadLine);
	olDailyItems.GetDailyFormatList( olFormatList ) ;

    pomTable->SetFormatList( olFormatList ); 
}


//-----------------------------------------------------------------------------------------------
// UPDATEDISPLAY: Load data selected by filter conditions to the display by using "omTable"

void Report17DailyTableViewer::UpdateDisplay()
{
	CString olCmpLine;
	DAILYURNOKEYLIST *plUrno;  
	POSITION pos;
	int InOut = 0 ;	// 0 = InBound 1 = OutBound

	GetHeader();

	pomTable->ResetContent();

	for(pos = omLineKeyMap.GetStartPosition(); pos != NULL; )	// Schleife �ber alle Zeilen
	{
		DAILYITEMS rlDailyItems( DAILYITEMS::DailyFlight ) ;
		REPORT17TABLE_LINEDATA *plFfData = new REPORT17TABLE_LINEDATA; // Preset: leer struct

		omLineKeyMap.GetNextAssoc( pos, olCmpLine, (void *&)plUrno );

		REPORT17TABLE_LINEDATA *plUrno15Data = &plUrno->Rotation[InOut];
		
		// PtrArray omLines versorgen .......................... NORMAL SATZ
		int ilLineno = CreateLine(*plUrno15Data, plUrno->Rotation[InOut].Urno);
		// Bildschirmzeile:
		pomTable->InsertTextLine(ilLineno, olCmpLine, NULL);

		if( ! CString(plUrno15Data->ViaIn+plUrno15Data->ViaOut).IsEmpty() )
		{
			// .................................... FOLGESATZ VIA
			// 2 Vias ( In+Outbound Vias 4 ) ergeben einen Folgesatz
			// die ITEMS Org+Sta und Des+Std werden als Positionen "mi�braucht"
			CString olFfLine ;
			rlDailyItems.GetEmptyFormatString( olFfLine ) ;	// Preset: leere Zeile
			CString olTeil1( olFfLine.Left(5)) ;
			CString olTeil2 ;
			CString olTeil3( olFfLine.Mid(11,(olFfLine.GetLength()-8))) ;
			CString olBar("|") ;

			CString olFfViaIn( plUrno15Data->ViaIn ) ;
			CString olFfViaOut( plUrno15Data->ViaOut ) ;
			olFfViaIn.TrimLeft() ;
			olFfViaOut.TrimLeft() ;
			int ilRest = olFfViaIn.GetLength() ;
			int ilRestO = olFfViaOut.GetLength() ;
			while( ilRest > 2 || ilRestO > 2 )	// min. 3 Ltr. Code
			{
				CString olRestIn ;
				CString olRestOut ;

				plFfData->Sta.Empty() ;
				plFfData->Std.Empty() ;

				ilRest = CutLeft( olFfViaIn, olRestIn, " " ) ;	// Vias In
				SusViaExt( olFfViaIn, '?' ) ;
				rlDailyItems.Org.Str = olFfViaIn ;
				plFfData->Urno = plUrno15Data->Urno ;
				plFfData->Org = olFfViaIn ;
				ilRestO = CutLeft( olFfViaOut, olRestOut, " " ) ;	// Vias Out
				SusViaExt( olFfViaOut, '?' ) ;
				rlDailyItems.Des.Str = olFfViaOut ;
				plFfData->Des = olFfViaOut ;

				if( rlDailyItems.TwoVias && (ilRest > 2 || ilRestO > 2) )
				{
					ilRest = CutLeft( olRestIn, olFfViaIn, " " ) ;	// Vias In
					SusViaExt( olRestIn, '?' ) ;
					rlDailyItems.Sta.Str = olRestIn ;
					plFfData->Org += " " + olRestIn ;
					plFfData->Sta = olRestIn ;
					ilRestO = CutLeft( olRestOut, olFfViaOut, " " ) ;	// Vias Out
					SusViaExt( olRestOut, '?' ) ;
					rlDailyItems.Std.Str = olRestOut ;
					plFfData->Des += " " + olRestOut ;
					plFfData->Std = olRestOut ;

				}
				else 
				{
					olFfViaIn = olRestIn ; 
					olFfViaOut = olRestOut ; 
				}

				ilLineno = CreateLine(*plFfData, plUrno->Rotation[InOut].Urno); // omLines
				
				olTeil2 = plFfData->Org + olBar + plFfData->Sta + olBar ; 
				olTeil2 += plFfData->Des + olBar + plFfData->Std + olBar + plFfData->ChtDes + olBar ;
				olFfLine = olTeil1+olTeil2+olTeil3 ;
				pomTable->InsertTextLine(ilLineno, olFfLine, NULL);
			}
		}	// end Folges�tze VIAS


		if( ! bmSeason && ! CString(plUrno15Data->RemIn+plUrno15Data->RemOut).IsEmpty() )
		{
			// .................................... FOLGESATZ REM
			CString olFfLine ;
			CString olFfRemIn(plUrno15Data->RemIn ) ;
			CString olFfRemOut(plUrno15Data->RemOut ) ;
			olFfRemIn.TrimLeft() ;
			olFfRemOut.TrimLeft() ;
			
			rlDailyItems.GetEmptyFormatString( olFfLine ) ;	// Preset: leere Zeile

			if( _MIT_TRACE_ ) TRACE( "%s %s/%s\n", rlDailyItems.Alc.Str, rlDailyItems.FltnIn.Str, rlDailyItems.FltnOut.Str  ) ;
			
			int ilRest = olFfRemIn.GetLength() + olFfRemOut.GetLength() ;
			int	ilRemSize = rlDailyItems.GetRemLength( ) ;	// max. Textl�nge RemIn und RemOut 

			while( ilRest > ilRemSize )	// RemIn + RemOut gleiche L�nge (Size) ?
			{
				CString olFfLine ;
				rlDailyItems.GetEmptyFormatString( olFfLine ) ;	// Preset: leere Zeile
				rlDailyItems.Org.Str = olFfRemIn.Left( ilRemSize )  ;
				rlDailyItems.Des.Str = olFfRemOut.Left( ilRemSize ) ;
				plFfData->Urno = plUrno15Data->Urno ;
				plFfData->Org = rlDailyItems.Org.Str ;
				plFfData->Des = rlDailyItems.Des.Str ;
				FillRightByChar( rlDailyItems.Org.Str, ' ', ilRemSize ) ;
				FillRightByChar( rlDailyItems.Des.Str, ' ', ilRemSize ) ;
				olFfLine += rlDailyItems.Org.Str+CString("  /  ")+rlDailyItems.Des.Str ;
				
				ilLineno = CreateLine(*plFfData, plUrno->Rotation[InOut].Urno); //omLines
				pomTable->InsertTextLine(ilLineno, olFfLine, NULL);
				
				olFfRemIn = olFfRemIn.Right( olFfRemIn.GetLength() - ilRemSize )  ;
				olFfRemOut = olFfRemOut.Right( olFfRemOut.GetLength() - ilRemSize )  ;
				olFfRemIn.TrimLeft() ;
				olFfRemOut.TrimLeft() ;
				ilRest = olFfRemIn.GetLength() + olFfRemOut.GetLength() ;
			}

			if( ilRest > 0 )
			{
				rlDailyItems.Org.Str = olFfRemIn  ;
				rlDailyItems.Des.Str = olFfRemOut ;
				plFfData->Urno = plUrno15Data->Urno ;
				plFfData->Org = rlDailyItems.Org.Str ;
				plFfData->Des = rlDailyItems.Des.Str ;
				FillRightByChar( rlDailyItems.Org.Str, ' ', ilRemSize ) ;
				FillRightByChar( rlDailyItems.Des.Str, ' ', ilRemSize ) ;
				olFfLine += rlDailyItems.Org.Str+CString("  /  ")+rlDailyItems.Des.Str ;

				ilLineno = CreateLine(*plFfData, plUrno->Rotation[InOut].Urno); //omLines
				pomTable->InsertTextLine(ilLineno, olFfLine, NULL);
			}
		}	// end Folges�tze Remarks



		if( plFfData != NULL ) delete plFfData ;

	}	// end for( pos = ....


    pomTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

int Report17DailyTableViewer::CreateLine(REPORT17TABLE_LINEDATA &rpLine, long lpUrno)
{
    int ilLineCount = omLines.GetSize();

    // omLines r�ckw�rts durchlaufen bis Position gefunden
	// <=> Stod || Stoa lpUrno sp�ter/gleich Werte in omLines !
	for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
	{
		if( !bmSeason )
		{
			if (CompareFlight(lpUrno, omLines[ilLineno-1].Urno) >= 0) break;  
        }
		else
		{
/*
			// mit Pr�fung auf Zeitraum 	
			if (CompareFlight(lpUrno, omLines[ilLineno-1].Urno) >= 0)
			{
				CString olDay1 = rpLine.Einteil.Left(cimDateLength) ;
				CString olDay2 = omLines[ilLineno-1].Einteil.Left(cimDateLength) ;
				if( olDay1.GetLength() == cimDateLength && olDay2.GetLength() == cimDateLength )
				{
					if( CompareRangeStart( olDay1, olDay2 ) >= 0 ) break ;
				}
				else
				{
					break ;
				}
			}
			
*/
			// ohne Zeitraum Pr�fung
			if (CompareFlight(lpUrno, omLines[ilLineno-1].Urno) >= 0) break;  

			// akt. Vergleich nach Fl�gen !
			// CompareLines( rpLine, omLines[ilLineno-1], true ) >= 0) break;  
  		}
	}

	// should be inserted after Lines[ilLineno-1]
    omLines.NewAt(ilLineno, rpLine);	// eine Urno je Ausgabezeile

	return ilLineno;
}

//-----------------------------------------------------------------------------------------------

int Report17DailyTableViewer::CompareFlight(long lpNewUrno, long lpCmpUrno)
{
	// anhand von STOD ( oder stoa ) die Zeiten vergleichen
	
	int	ilCompareResult;
	CTime olTime1;
	CTime olTime2;


	ROTATIONDLGFLIGHTDATA *prlNewF = omDailyCedaFlightData.GetFlightByUrno(lpNewUrno);
	ROTATIONDLGFLIGHTDATA *prlCmpF = omDailyCedaFlightData.GetFlightByUrno(lpCmpUrno);

	if( prlNewF != NULL && prlCmpF != NULL ) 
	{

		if(prlNewF->Stod == TIMENULL)
		{
			if(prlNewF->Stoa == TIMENULL)
			{
				// beide Zeiten undefiniert
				if(prlCmpF->Stod != TIMENULL || prlCmpF->Stoa != TIMENULL )
				{
					// den Neuen nach vorne
					return -1 ;
				}
				else
				{
					// alle 4 Zeiten undefiniert ??
					return 0 ;
				}
			}
			else
			{
				// Stoa benutzten
				olTime1 = prlNewF->Stoa;

			}

		}
		else
		{
			// Stod benutzten
			olTime1 = prlNewF->Stod;
		}

		
		if(prlCmpF->Stod == TIMENULL)
		{
			if(prlCmpF->Stoa == TIMENULL)
			{
				// beide Zeiten undefiniert
				return 1 ;
			}
			else
			{
				// Stoa benutzten
				olTime2 = prlCmpF->Stoa;
			}
		}
		else
		{
			// Stod benutzten
			olTime2 = prlCmpF->Stod;
		}

		// Sortierung HH:MM : wegen 0 bis 24 Uhr vgl. zur�ck zu local
		if(bgReportLocal)
		{
			ogBasicData.UtcToLocal(olTime1);
			ogBasicData.UtcToLocal(olTime2);
		}

		int ilhhmm1 = olTime1.GetHour() * 100 + olTime1.GetMinute() ;
		int ilhhmm2 = olTime2.GetHour() * 100 + olTime2.GetMinute() ;
		ilCompareResult = (ilhhmm1 == ilhhmm2)? 0 :	( ilhhmm1 > ilhhmm2 ) ? 1 : -1 ;
		
		// gleiche Zeit ?
		if( ilCompareResult == 0 )
		{
			// dann nach Flugnummer
			if( (ilCompareResult = CString(prlNewF->Flno).CompareNoCase(prlCmpF->Flno)) == 0 )
			{

				// und nach Flugtag
				ilCompareResult = (olTime1 == olTime2)? 0 :	(olTime1 > olTime2) ? 1 : -1;

			}
		}
		
		return ilCompareResult;
	}
	else
	{
		VERIFY( false ) ;
		if( prlNewF == NULL && prlCmpF == NULL ) return 0 ; 
		else if( prlNewF == NULL )
		{
			return -1 ; 
		}
		else {
			return 1 ;
		}
	}
}


//-----------------------------------------------------------------------------------------------

void Report17DailyTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

void Report17DailyTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);

	DAILYURNOKEYLIST *prlUrno;  
	POSITION pos;
	CString olString;
	for( pos = omLineKeyMap.GetStartPosition(); pos != NULL; )
	{
		omLineKeyMap.GetNextAssoc( pos, olString, (void *&)prlUrno );
		// zuerst CCSPtrArray delete !
		while( prlUrno->Rotation.GetSize() > 0 )
		{
			prlUrno->Rotation.DeleteAt(0);
		}
		prlUrno->Rotation.RemoveAll();
		delete prlUrno;
	}

	omLineKeyMap.RemoveAll();

}


//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------
void Report17DailyTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	char pclFooter[256];

//	sprintf(pclFooter, "%s   %s %s", GetString(IDS_STRINGFAG), GetString(IDS_STRING1224),
//			(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));
	sprintf(pclFooter, omTableName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));

	omFooterName = pclFooter;
//	CString olTableName = GetString(IDS_STRING1400);
	CString olTableName = GetString(IDS_REPORTS_RB_OPSSchedule);

	int ilOrientation = PRINT_LANDSCAPE;
	REPORT17TABLE_LINEDATA *prLeerzeile = new REPORT17TABLE_LINEDATA ;	// leere struct
	prLeerzeile->Folgezeile = imIndexLeer ;

	pomPrint = new CCSPrint((CWnd*)this,ilOrientation, 80);	// Def.: FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	if (pomPrint != NULL)
	{

		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			if( bmSeason )
			{
				pomPrint->imLineHeight = 45;	 
				pomPrint->imMaxLines = 38;	// 38 Land - f�r Height 45 
			}
			else
			{
//				pomPrint->imMaxLines = 17;	// Def.: 57 Port/ 38 Land - f�r Height 80 => 20 - Kopfbereich (5cm) = 17 
				pomPrint->imLineHeight = 45;	 
				pomPrint->imMaxLines = 31;
			}

			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;	// Def. imMaxLines: 57 Portrait / 38 Landscape
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = olTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();  

//			olFooter1.Format("%s",omFooterName);
			olFooter1.Format("%s -   %s", omFooterName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );

			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)	//imLineNo: PrintHeader => 0 / PrintLine => ++
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
					PrintTableLine(prLeerzeile,false );
				}
				
				if( (pomPrint->imLineNo + omLines[i].Folgezeile) > (pomPrint->imMaxLines-2) &&
					(omLines[i].Folgezeile + 1) < (pomPrint->imMaxLines-2) )
				{
					PrintTableLine(prLeerzeile,true );
					pomPrint->imLineNo = pomPrint->imMaxLines + 1;
					i-- ;
				}
				// Leerzeile beachten => -2 !
				else if(pomPrint->imLineNo == (pomPrint->imMaxLines-2) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],false );
					PrintTableLine(prLeerzeile,true );
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}

		omPrintHeadHeaderArray.DeleteAll();
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		delete pomPrint;
		pomPrint = NULL;
	}

	if( prLeerzeile != NULL ) delete prLeerzeile ;
}

//-----------------------------------------------------------------------------------------------

void Report17DailyTableViewer::GetPrintHeader()
{
		
	DAILYITEMS rlDailyItems( DAILYITEMS::DailyFlight ) ;
	const int ilAnzItems = (const int) rlDailyItems.GetNoOfItems() ;

	omPrintHeadHeaderArray.DeleteAll();  
	
	// Wieso + 1 !?
	TABLE_HEADER_COLUMN *prlHeader[DAILYITEMSItems+1];

	// Preset
	for( int index = 0 ; index < ilAnzItems; index++ )
	{
		prlHeader[index] = new TABLE_HEADER_COLUMN;
		prlHeader[index]->Alignment = COLALIGN_CENTER;
		prlHeader[index]->Length = 35; 
		prlHeader[index]->Font = &ogCourier_Bold_10;
		prlHeader[index]->Text = "" ; // CString();
	}

	// ToDO: Length mit Faktor ( z.B.12.5 bei 12cpi ) �ber ITEMS Size steuern !
	prlHeader[0]->Length = 70; 
	prlHeader[0]->Text = GetString(IDS_STRING1078);	//CString("Flight No");
	prlHeader[1]->Length = 55; 
	prlHeader[2]->Length = 55; 
	prlHeader[3]->Length = 55; 
	prlHeader[3]->Text = GetString(IDS_STRING337);	//CString("Typ");
	prlHeader[4]->Text = GetString(IDS_STRING1404);	//CString("Conf");
	prlHeader[5]->Text = GetString(IDS_STRING1073);	//CString("Von");
	prlHeader[5]->Length = 60; 
	prlHeader[6]->Text = GetString(IDS_STRING323);	//CString("STA");

	prlHeader[7]->Text = GetString(IDS_STRING1082);	//CString("Nach");
	prlHeader[7]->Length = 60; 
	prlHeader[8]->Text = GetString(IDS_STRING316);	//CString("STD");
	prlHeader[9]->Text = GetString(IDS_STRING1405);	//CString("CHT");
//	prlHeader[10]->Text = GetString(IDS_STRING308);	//CString("POS");
	prlHeader[10]->Text = GetString(IDS_STRING_POS);	//CString("POS");
	prlHeader[10]->Length = 50; 

	if( ! bmSeason )
	{
		prlHeader[11]->Text = GetString(IDS_STRING1406);	//CString("Gate");
		prlHeader[11]->Length = 70; 
		prlHeader[12]->Text = GetString(IDS_STRING1407);	//CString("Ctr");
		prlHeader[12]->Length = 70; 
		prlHeader[13]->Text = GetString(IDS_STRING1408);	//CString("PAX");
//		prlHeader[14]->Length = 325; 
		prlHeader[14]->Length = 280; 
//		prlHeader[14]->Text = GetString(IDS_STRING1409) ;	//CString("Einteilung"); 
		prlHeader[14]->Text = GetString(IDS_STRING_CAX) ;	//CString("Einteilung"); 
	}
	else
	{
		prlHeader[11]->Text = GetString(IDS_STRING1415);	//CString("Status");
		prlHeader[12]->Text = "";	//CString("");
		prlHeader[12]->Length = 5; 
		prlHeader[13]->Text = "";	//CString("");
		prlHeader[13]->Length = 5; 
		prlHeader[14]->Length = 385 ;  
		prlHeader[14]->Text = GetString(IDS_STRING1412) ;	//CString("Zeitraum"); 
//		prlHeader[14]->Text = GetString(IDS_STRING_CAX) ;	//CString("Zeitraum"); 
	}


	for(int ili = 0; ili < ilAnzItems; ili++)
	{
		omPrintHeadHeaderArray.Add( prlHeader[ili]);
	}

}

//-----------------------------------------------------------------------------------------------

bool Report17DailyTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;
	double dgCCSPrintFactor = 2.7 ;
/*	char pclHeader[256];

	sprintf(pclHeader, GetString(IDS_STRING1401), pcmSelect, pcmInfo);
	CString olTableName(pclHeader);
*/
	GetPrintHeader();

	if( bmSeason )
	{
//		pomPrint->PrintUIFHeader(omTableName,CString(pclHeader),pomPrint->imFirstLine-10);
		pomPrint->PrintUIFHeader("",omTableName,pomPrint->imFirstLine-10);
	}
	else
	{
		// Umstellung DAILY 07/9/98
		pomPrint->imLeftOffset = 2100 ;
		pomPrint->imFirstLine = 500 ;
		pomPrint->PrintUIFHeaderNoLine(CString(""),GetString(IDS_STRING1456),pomPrint->imFirstLine-400);
		pomPrint->PrintUIFHeaderNoLine(CString(""),GetString(IDS_STRING1457),pomPrint->imFirstLine-350);
		pomPrint->PrintUIFHeaderNoLine(CString(""),GetString(IDS_STRING1458),pomPrint->imFirstLine-300);
		pomPrint->PrintUIFHeaderNoLine(CString(""),GetString(IDS_STRING1479),pomPrint->imFirstLine-250);
		pomPrint->PrintUIFHeaderNoLine(CString(""),GetString(IDS_STRING1459),pomPrint->imFirstLine-200);
		pomPrint->imLeftOffset = 50 ;
//		pomPrint->PrintUIFHeader(omTableName,CString(pclHeader),pomPrint->imFirstLine);
		pomPrint->PrintUIFHeader("",omTableName,pomPrint->imFirstLine);
	}

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMETHIN;
//	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_12;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omPrintHeadHeaderArray.GetSize();

	for(int i=0;i<ilSize;i++)
	{
			rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omPrintHeadHeaderArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}


//-----------------------------------------------------------------------------------------------
// eine Tabellenzeile ausgeben

bool Report17DailyTableViewer::PrintTableLine(REPORT17TABLE_LINEDATA *prpUrno, bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	double dgCCSPrintFactor = 2.7 ;
//	rlElement.pFont = &pomPrint->ogCourierNew_Regular_12;
	rlElement.pFont = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omPrintHeadHeaderArray.GetSize();
	DAILYITEMS rlDailyItems( DAILYITEMS::DailyFlight ) ;
	bool blFfLine = false ;		// Flag: Folgezeile 
	bool blSkip = false ;		// Flag: Ausgabe �berspringen


	// Vias oder Bemerkungen ?
	// => in Folgeline: Org + Des �berschrieben, STD / STA und CHT* werden unterdr�ckt !
	if( prpUrno->Folgezeile != imIndexLeer )
	{
		if( CString( prpUrno->Alc + prpUrno->FltnIn + prpUrno->FltnOut ).IsEmpty() ) blFfLine = true ;
	}

	// alle Tabellenzellen ausgeben = DAILYITMESItems
	for(int i=0;i<ilSize;i++)
	{
		blSkip = false ;
		rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength) rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Alignment  = PRINT_LEFT;
		
		if( blFfLine ) rlElement.FrameTop   = PRINT_NOFRAME ;
		else		   rlElement.FrameTop   = PRINT_FRAMETHIN;
		
		if( bpLastLine ) rlElement.FrameBottom = PRINT_FRAMETHIN;
		else			 rlElement.FrameBottom = PRINT_NOFRAME;
		
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;



		switch(i)
		{
			case DAILYITEMS::ALC:
			{
				SetPrintItem( rlElement, prpUrno->Alc, rlDailyItems.DoLeftFrame(DAILYITEMS::ALC) );

			}
			break;
			case DAILYITEMS::FLTNIN:
			{
				SetPrintItem( rlElement, prpUrno->FltnIn, rlDailyItems.DoLeftFrame(DAILYITEMS::FLTNIN));
			}
			break;
			case DAILYITEMS::FLTNOUT:
			{
				SetPrintItem( rlElement, prpUrno->FltnOut, rlDailyItems.DoLeftFrame(DAILYITEMS::FLTNOUT));
			}
			break;
			case DAILYITEMS::ACT:
			{
				SetPrintItem( rlElement, prpUrno->Act, rlDailyItems.DoLeftFrame(DAILYITEMS::ACT));
			}
			break;
			case DAILYITEMS::NOSE:
			{
				SetPrintItem( rlElement, prpUrno->Nose, rlDailyItems.DoLeftFrame(DAILYITEMS::NOSE));
			}
			break;

			// 3 folgenden Positionen auf FolgeLine abfragen  
			case DAILYITEMS::ORG:
			{
				if( blFfLine )
				{
					// + STA 
					int ilLengthPlus = (int) ( omPrintHeadHeaderArray[DAILYITEMS::STA].Length * dgCCSPrintFactor) ;
					if(ilLengthPlus < igCCSPrintMinLength) ilLengthPlus += igCCSPrintMoreLength; 
					// ilLengthPlus *=	2 ;
					rlElement.Length +=	ilLengthPlus ;
					
					// beide leer => Leerzeile !
					if( ! CString(prpUrno->Org+prpUrno->Des).IsEmpty() )
					{
						// rlElement.FrameBottom = PRINT_NOFRAME ;
						rlElement.FrameTop = PRINT_NOFRAME ;
					}
				}

				SetPrintItem( rlElement, prpUrno->Org,rlDailyItems.DoLeftFrame(DAILYITEMS::ORG));
			}
			break;
			case DAILYITEMS::STA:
			{
				if( blFfLine ) 
				{
					blSkip = true ;
				}
				else
				{
					SetPrintItem( rlElement, prpUrno->Sta, rlDailyItems.DoLeftFrame(DAILYITEMS::STA));
				}
			}
			break;

			// 3 folgenden Positionen auf FolgeLine abfragen  
			case DAILYITEMS::DES:
			{
				if( blFfLine )
				{
					// + STD 
					int ilLengthPlus = (int) ( omPrintHeadHeaderArray[DAILYITEMS::STD].Length * dgCCSPrintFactor) ;
					if(ilLengthPlus < igCCSPrintMinLength) ilLengthPlus += igCCSPrintMoreLength; 
					// ilLengthPlus *=	2 ;
					rlElement.Length +=	ilLengthPlus ;

					if( ! CString(prpUrno->Org+prpUrno->Des).IsEmpty() ) 
					{
						// rlElement.FrameBottom = PRINT_NOFRAME ;
						rlElement.FrameTop = PRINT_NOFRAME ;
					}
				}
				SetPrintItem( rlElement, prpUrno->Des, rlDailyItems.DoLeftFrame(DAILYITEMS::DES));
			}
			break;
			case DAILYITEMS::STD:
			{
				if( blFfLine ) 
				{
					blSkip = true ;
				}
				else
				{
					SetPrintItem( rlElement, prpUrno->Std, rlDailyItems.DoLeftFrame(DAILYITEMS::STD));
				}
			}
			break;
			case DAILYITEMS::CHTDES:
			{
/*
				if( blFfLine ) 
				{
					blSkip = true ;
				}
				else
				{
					SetPrintItem( rlElement, prpUrno->ChtDes, rlDailyItems.DoLeftFrame(DAILYITEMS::CHTDES));
				}
*/
				SetPrintItem( rlElement, prpUrno->ChtDes, rlDailyItems.DoLeftFrame(DAILYITEMS::CHTDES));
			}
			break;

			case DAILYITEMS::POS:
			{
				SetPrintItem( rlElement, prpUrno->Pos, rlDailyItems.DoLeftFrame(DAILYITEMS::POS));
			}
			break;
			case DAILYITEMS::GP:
			{
				SetPrintItem( rlElement, prpUrno->Gp, rlDailyItems.DoLeftFrame(DAILYITEMS::GP));
			}
			break;
			case DAILYITEMS::CTR:
			{
				SetPrintItem( rlElement, prpUrno->Ctr, rlDailyItems.DoLeftFrame(DAILYITEMS::CTR) && ! bmSeason );
			}
			break;
			case DAILYITEMS::PAX:
			{
				SetPrintItem( rlElement, prpUrno->Pax, rlDailyItems.DoLeftFrame(DAILYITEMS::PAX) && ! bmSeason );
			}
			break;
			case DAILYITEMS::EINTEIL:
			{
				SetPrintItem( rlElement, prpUrno->Einteil, rlDailyItems.DoLeftFrame(DAILYITEMS::EINTEIL), true ) ;
			}
			break;
		}	// end switch( i ....


		if( _MIT_TRACE_ ) TRACE(" i [%d] : Length [%d]\n", i, rlElement.Length ) ;
		
		if( ! blSkip ) rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		
	} // end for( i = ... alle ITEMS 

	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll(); 
	return true;

}



// ein Druck (Zelle) Item aufbereiten
void Report17DailyTableViewer::SetPrintItem(PRINTELEDATA& rrpItem, CString opItem, bool bpLF, bool bpRF)
{
	char olBar = '|' ;
	int ilRet ;

	// View Trenner eliminieren
	if( (ilRet = opItem.ReverseFind( olBar )) < 0 )
	{
		rrpItem.Text = opItem ;
	}
	else
	{
		CString olOhne = opItem ;
		while( ilRet >= 0 )
		{
			if( ilRet )	// rechts au�en
			{
				olOhne = olOhne.Left( ilRet ) ;
			}
			else	// gleich vorne
			{
				olOhne = olOhne.Right( olOhne.GetLength()-1 ) ;
			}
			ilRet = olOhne.ReverseFind( olBar ) ;
		}
		rrpItem.Text = olOhne ;
	}

	// R�nder ?
	if( bpLF ) rrpItem.FrameLeft = PRINT_FRAMETHIN ;
	if( bpRF ) rrpItem.FrameRight = PRINT_FRAMETHIN ;
}




// Drucken in Datei
bool Report17DailyTableViewer::PrintPlanToFile(char *opTrenner)
{
	ofstream of;

	char* tmpvar;
	tmpvar = getenv("TMP");
	if (tmpvar == NULL)
		tmpvar = getenv("TEMP");

	if (tmpvar == NULL)
		return false;

	CString olFileName = omTableName;
	olFileName.Remove('*');
	olFileName.Remove('.');
	olFileName.Remove(':');
	olFileName.Remove('/');
	olFileName.Replace(" ", "_");
	
	char pHeader[256];
	strcpy (pHeader, tmpvar);
	CString path = pHeader;
	omFileName =  path + "\\" + olFileName + ".csv";

//	of.open( GetString(IDS_STRING1386), ios::out);
	of.open( omFileName, ios::out);

	int ilwidth = 1;
	DAILYITEMS rlDailyItems( DAILYITEMS::DailyFlight ) ;
	const int ilAnzItems = (const int) rlDailyItems.GetNoOfItems() ;

	of  << setw(ilwidth) << omTableName << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;

	if( bmSeason )
	{
		of	<< setw(ilwidth) << GetString(IDS_STRING1078) 
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING330) 
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING338)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING337)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING1404)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING1073) 
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING323)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING1082)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING316)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING1405)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING_POS)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING1415)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING1412)
			<< endl;

		int ilCount = omLines.GetSize();
		for(int index = 0; index < ilCount; index++)
		{
			of.setf(ios::left, ios::adjustfield ) ;
			
			for( int i=0; i<ilAnzItems; i++)
			{
				bool blSkip = false ;
				CString olItem(" "); 
				switch(i)
				{
					case DAILYITEMS::ALC:
					{
						olItem = omLines[index].Alc ;
					}
					break;
					case DAILYITEMS::FLTNIN:
					{
						olItem = omLines[index].FltnIn ;
					}
					break;
					case DAILYITEMS::FLTNOUT:
					{
						olItem = omLines[index].FltnOut ;
					}
					break;
					case DAILYITEMS::ACT:
					{
						olItem = omLines[index].Act ;
					}
					break;
					case DAILYITEMS::NOSE:
					{
						olItem = omLines[index].Nose ;
					}
					break;
					case DAILYITEMS::ORG:
					{
						olItem = omLines[index].Org ;
					}
					break;
					case DAILYITEMS::STA:
					{
						olItem = omLines[index].Sta ;
					}
					break;
					case DAILYITEMS::DES:
					{
						olItem = omLines[index].Des ;
					}
					break;
					case DAILYITEMS::STD:
					{
						olItem = omLines[index].Std ;
					}
					break;
					case DAILYITEMS::CHTDES:
					{
						olItem = omLines[index].ChtDes ;
					}
					break;

					case DAILYITEMS::POS:
					{
						olItem = omLines[index].Pos ;
					}
					break;
					case DAILYITEMS::GP:
					{
						olItem = omLines[index].Gp ;
					}
					break;

					case DAILYITEMS::CTR:
					case DAILYITEMS::PAX:
					{
						// in Saisondarstellung unerw�nscht !
						blSkip = true ;
					}
					break;
					
					case DAILYITEMS::EINTEIL:
					{
						// mu� der Letzte sein !
						olItem = omLines[index].Einteil ;
//						SusViaExt( olItem, '-', ',' ) ;		// 2 Felder in der Datei !
					}
					break;
				}	// end switch( i ....
				
				if( ! blSkip )
				{
					SusViaExt( olItem, '|', ' ' ) ;	// Tabellentrenner raus
					if( i != DAILYITEMS::EINTEIL )
						of<< olItem << ";" ;		// Neuer Trenner: Komma !
					else
						of<< olItem << endl ;
				}
			}
		}
	}
	else
	{
		of	<< setw(ilwidth) << GetString(IDS_STRING1078) 
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING330) 
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING338)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING337)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING1404)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING1073) 
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING323)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING1082)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING316)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING1405)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING_POS)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING1406)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING1407)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING1408)
			<< setw(1) << opTrenner
			<< setw(ilwidth) << GetString(IDS_STRING_CAX)
			<< endl;

		int ilCount = omLines.GetSize();
		for(int index = 0; index < ilCount; index++)
		{
			of.setf(ios::left, ios::adjustfield ) ;
			
			for( int i=0; i<ilAnzItems; i++)
			{
				bool blSkip = false ;
				CString olItem(" "); 
				switch(i)
				{
					case DAILYITEMS::ALC:
					{
						olItem = omLines[index].Alc ;
					}
					break;
					case DAILYITEMS::FLTNIN:
					{
						olItem = omLines[index].FltnIn ;
					}
					break;
					case DAILYITEMS::FLTNOUT:
					{
						olItem = omLines[index].FltnOut ;
					}
					break;
					case DAILYITEMS::ACT:
					{
						olItem = omLines[index].Act ;
					}
					break;
					case DAILYITEMS::NOSE:
					{
						olItem = omLines[index].Nose ;
					}
					break;
					case DAILYITEMS::ORG:
					{
						olItem = omLines[index].Org ;
					}
					break;
					case DAILYITEMS::STA:
					{
						olItem = omLines[index].Sta ;
					}
					break;
					case DAILYITEMS::DES:
					{
						olItem = omLines[index].Des ;
					}
					break;
					case DAILYITEMS::STD:
					{
						olItem = omLines[index].Std ;
					}
					break;
					case DAILYITEMS::CHTDES:
					{
						olItem = omLines[index].ChtDes ;
					}
					break;

					case DAILYITEMS::POS:
					{
						olItem = omLines[index].Pos ;
					}
					break;
					case DAILYITEMS::GP:
					{
						olItem = omLines[index].Gp ;
					}
					break;

					case DAILYITEMS::CTR:
					{
						olItem = omLines[index].Ctr ;
					}
					break;
					case DAILYITEMS::PAX:
					{
						olItem = omLines[index].Pax ;
					}
					break;
					
					case DAILYITEMS::EINTEIL:
					{
						// mu� der Letzte sein !
						olItem = omLines[index].Einteil ;
						SusViaExt( olItem, '-', ',' ) ;		// 2 Felder in der Datei !
					}
					break;
				}	// end switch( i ....
				
				if( ! blSkip )
				{
					SusViaExt( olItem, '|', ' ' ) ;	// Tabellentrenner raus
					if( i != DAILYITEMS::EINTEIL )
						of<< olItem << ";" ;		// Neuer Trenner: Komma !
					else
						of<< olItem << endl ;
				}
			}
		}

	}


	of.close();
	return true;


/*
	// 2/99 nur f�r DAILY SEASON:
	if( bmSeason )
	{
		DAILYITEMS rlDailyItems( DAILYITEMS::DailyFlight ) ;
		const int ilAnzItems = (const int) rlDailyItems.GetNoOfItems() ;

		char pclHeader[120];
		CString olSep( " / " ) ;
		ofstream of;
		of.open( pcpDefPath, ios::out);
		sprintf(pclHeader, GetString(IDS_STRING1401), pcmSelect, pcmInfo);
		of << pclHeader << "       ( "
			<< CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M") 
			<< " )" << endl << endl;

		of  << GetString(IDS_STRING1078) << ","
			<< GetString(IDS_STRING330) << ","  
			<< GetString(IDS_STRING338) << ","  
			<< GetString(IDS_STRING337) << ","  
			<< GetString(IDS_STRING1404) << ","  
			<< GetString(IDS_STRING1073) << ","  
			<< GetString(IDS_STRING323) << ","  
			<< GetString(IDS_STRING1082) << ","  
			<< GetString(IDS_STRING316) << ","  
			<< GetString(IDS_STRING1405) << ","  
			<< GetString(IDS_STRING308) << ","  
			<< GetString(IDS_STRING1415) << ","  
			<< GetString(IDS_STRING1412) << ","  
			<< endl;

//		of << "----------------------------------------------------------------------------------------------------------------------" << endl;
		int ilCount = omLines.GetSize();
		for(int index = 0; index < ilCount; index++)
		{
			of.setf(ios::left, ios::adjustfield ) ;
			
			for( int i=0; i<ilAnzItems; i++)
			{
				bool blSkip = false ;
				CString olItem(" "); 
				switch(i)
				{
					case DAILYITEMS::ALC:
					{
						olItem = omLines[index].Alc ;
					}
					break;
					case DAILYITEMS::FLTNIN:
					{
						olItem = omLines[index].FltnIn ;
					}
					break;
					case DAILYITEMS::FLTNOUT:
					{
						olItem = omLines[index].FltnOut ;
					}
					break;
					case DAILYITEMS::ACT:
					{
						olItem = omLines[index].Act ;
					}
					break;
					case DAILYITEMS::NOSE:
					{
						olItem = omLines[index].Nose ;
					}
					break;
					case DAILYITEMS::ORG:
					{
						olItem = omLines[index].Org ;
					}
					break;
					case DAILYITEMS::STA:
					{
						olItem = omLines[index].Sta ;
					}
					break;
					case DAILYITEMS::DES:
					{
						olItem = omLines[index].Des ;
					}
					break;
					case DAILYITEMS::STD:
					{
						olItem = omLines[index].Std ;
					}
					break;
					case DAILYITEMS::CHTDES:
					{
						olItem = omLines[index].ChtDes ;
					}
					break;

					case DAILYITEMS::POS:
					{
						olItem = omLines[index].Pos ;
					}
					break;
					case DAILYITEMS::GP:
					{
						olItem = omLines[index].Gp ;
					}
					break;

					case DAILYITEMS::CTR:
					case DAILYITEMS::PAX:
					{
						// in Saisondarstellung unerw�nscht !
						blSkip = true ;
					}
					break;
					
					case DAILYITEMS::EINTEIL:
					{
						// mu� der Letzte sein !
						olItem = omLines[index].Einteil ;
						SusViaExt( olItem, '-', ',' ) ;		// 2 Felder in der Datei !
					}
					break;
				}	// end switch( i ....
				
				if( ! blSkip )
				{
					SusViaExt( olItem, '|', ' ' ) ;	// Tabellentrenner raus
					if( i != DAILYITEMS::EINTEIL )
						of<< olItem << "," ;		// Neuer Trenner: Komma !
					else
						of<< olItem << endl ;
				}
			}
		}
		of.close() ;
	}
*/

	return true;
}


//-----------------------------------------------------------------------------------------------



static void SusViaExt( CString& opStr, char cpSign, char cpNeu )
{
	// SUS: Via Extension auf blank
	int ilRet = 0 ;
	while((ilRet = opStr.Find( cpSign )) >= 0) opStr.SetAt(ilRet, cpNeu) ;
}

//-----------------------------------------------------------------------------------------------
