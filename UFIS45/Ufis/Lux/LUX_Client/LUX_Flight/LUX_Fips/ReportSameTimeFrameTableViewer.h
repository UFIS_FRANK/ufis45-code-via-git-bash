#ifndef __REPORT_TIME_FRAME_TABLE_VIEWER_H__
#define __REPORT_TIME_FRAME_TABLE_VIEWER_H__



#include <stdafx.h>
#include <RotationDlgCedaFlightData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct TIMEFRAMETABLE_LINEDATA
{
	long AUrno;
	long ARkey;
	CString		AAct;
	CString		AAlc2;
	CString		AAlc3;
	CString		ADate;
	CTime 		AEtai; 	
	CString		AFlno;
	CTime 		ALand; 	
	CTime 		AStoa;
	CString		ATtyp;
	CString		APaxt;
	CString		APaxi;

	long DUrno;
	long DRkey;
	CString		DAct;
	CString		DAlc2;
	CString		DAlc3;
	CString		DDate;
	CString		DDes3;
	CString		DFlno;
	CString		DTtyp;
	CTime 		DStod;
	CString		DPaxt;
	CString		DPaxi;

};

/////////////////////////////////////////////////////////////////////////////
// ReportTimeFrameTableViewer

class ReportTimeFrameTableViewer : public CViewer
{
// Constructions
public:
    ReportTimeFrameTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, 
		                                 const CStringArray &opShownColumns,
		                                 char *pcpInfo = NULL, char *pcpSelect = NULL);
    ~ReportTimeFrameTableViewer();

    void Attach(CCSTable *popAttachWnd);
	void SetParentDlg(CDialog* ppDlg);
    virtual void ChangeViewTo(const char *pcpViewName);
	void PrintPlanToFile(char *pcpDefPath);
// Internal data processing routines
private:
    void MakeLines();
	int  MakeLine(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight);
	void MakeLineData(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight, TIMEFRAMETABLE_LINEDATA &rpLine);
	void MakeColList(TIMEFRAMETABLE_LINEDATA *prlLine, 
		             CCSPtrArray<TABLE_COLUMN> &olColList, CString opCurrentAirline);
	int  CompareGefundenefluege(TIMEFRAMETABLE_LINEDATA *prpGefundenefluege1, TIMEFRAMETABLE_LINEDATA *prpGefundenefluege2);
	int  CompareFlight(TIMEFRAMETABLE_LINEDATA *prpFlight1, TIMEFRAMETABLE_LINEDATA *prpFlight2);
	int  GetVia(CCSPtrArray<VIADATA> *opVias, ROTATIONDLGFLIGHTDATA *prpFlight);
	void InitColumnsDays();

// Operations
public:
	void DeleteAll();
	int CreateLine(TIMEFRAMETABLE_LINEDATA &prpGefundenefluege);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	void DrawHeader();


// Attributes used for filtering condition
private:
	CDialog* pomParentDlg;
	bool bmIsFromSearch;
	// Array der Header
	CStringArray omShownColumns;
	// Sapltengroese
	CUIntArray omAnzChar;
	CString omAPTT;

    CCSTable *pomTable;
	CCSPtrArray<ROTATIONDLGFLIGHTDATA> *pomData;
	// Liste der existierenden Delays
	CStringArray omColumn0;
	CStringArray omColumn1;
	CStringArray omColumn2;
	CStringArray omColumn3;
	CStringArray omColumn4;
	// Auftreten der existierenden Delays
	CUIntArray omCountColumn0;
	CUIntArray omCountColumn1;
	CUIntArray omCountColumn2;
	CUIntArray omCountColumn3;
	CUIntArray omCountColumn4;
	CUIntArray omCountColumn5;
	CUIntArray omCountColumn6;
	CUIntArray omCountColumn7;
	CUIntArray omCountColumn8;

	int imTableLine;		// Zeilennumer fuer GetHeaderContent
	char pcmFirstDate[16];		//Time frame beginn
	char pcmLastDate[16];	//Time frame end

public:
    CCSPtrArray<TIMEFRAMETABLE_LINEDATA> omLines;
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;

	// anzuzeigende Header als Array uebergeben
	void SetColumnsToShow(const CStringArray &opShownColumns);
	int GetFlightCount();

//Print 
	void GetHeader(void);
	CCSPtrArray <TABLE_HEADER_COLUMN> omPrintHeadHeaderArray;
	CCSPrint *pomPrint; 
	void PrintTableView(void);
	bool PrintTableLine(TIMEFRAMETABLE_LINEDATA *prpLine,bool bpLastLine, 
		                CString opCurrentACType);
	bool PrintTableHeader(void);
	CString omTableName;
	char *pcmInfo;
	char *pcmSelect;
	CString omFooterName;

protected:
	// Mapping Tabellenfeldname/Spaltenueberschrift
	CString GetHeaderContent(CString opCurrentColumns);
	CString GetFieldContent(TIMEFRAMETABLE_LINEDATA* prlLine, CString opCurrentColumns);
	void SetFieldLength(CString opCurrentColumns);
	// Listen zum Zaehlen erstellen
	void MakeCountListNDays(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight);
	void MakeTotalLine();

};

#endif
