#ifndef __SITZPLAETZETABLEVIEWER_H__
#define __SITZPLAETZETABLEVIEWER_H__

#include <stdafx.h>
#include <RotationDlgCedaFlightData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct SITZPLAETZETABLE_LINEDATA
{
	long		Urno; 	// Eindeutige Datensatz-Nr.
	CString 	Alc2; 	// Verkehrsart
	CString 	Alc3; 	// Verkehrsart
	CString 	Des3; 	// Bestimmungsflughafen 3-Lettercode
	CString 	Seat; 	// Anzahl Sitzplštze
	CString 	Subt; 	// Subtotal Zielflughafen
	CString 	Totl; 	// Total LVG und Zielflughafen
};

/////////////////////////////////////////////////////////////////////////////
// SitzplaetzeTableViewer

class SitzplaetzeTableViewer : public CViewer
{
// Constructions
public:
    SitzplaetzeTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo = NULL, char *pcpSelect = NULL);
    ~SitzplaetzeTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
    void MakeLines(void);
	void MakeLine(CString poLvg, CString poDes, int ipSubt, int ipTotl, int ipSeat);
	void MakeResultLine(CString opNewstr, int ipSeat, int ipCntr);
// Operations
public:
	void DeleteAll(void);
	void CreateLine(SITZPLAETZETABLE_LINEDATA *prpSitzplaetze);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay(void);
	bool PrintPlanToFile(char *pcpDefPath);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomSitzplaetzeTable;
	CCSPtrArray<ROTATIONDLGFLIGHTDATA> *pomData;
public:
    CCSPtrArray<SITZPLAETZETABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(SITZPLAETZETABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	CString omTableName;
	CString omFileName;
	CString omFooterName;
	char *pcmInfo;
	char *pcmSelect;

};

#endif //__SITZPLAETZETABLEVIEWER_H__
