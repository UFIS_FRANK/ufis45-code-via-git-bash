// SitzplaetzeTableViewer.cpp 
//

#include <stdafx.h>
#include <Report12TableViewer.h>
#include <CCSGlobl.h>
#include <CedaBasicData.h>
#include <resrc1.h>

#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// SitzplaetzeTableViewer
//
SitzplaetzeTableViewer::SitzplaetzeTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo, char *pcpSelect)
{
	pomData = popData;
	pcmInfo = pcpInfo;
	pcmSelect = pcpSelect;

	bmIsFromSearch = FALSE;
    pomSitzplaetzeTable = NULL;
}

//-----------------------------------------------------------------------------------------------

SitzplaetzeTableViewer::~SitzplaetzeTableViewer()
{
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void SitzplaetzeTableViewer::Attach(CCSTable *popTable)
{
    pomSitzplaetzeTable = popTable;
}

//-----------------------------------------------------------------------------------------------
void SitzplaetzeTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------
void SitzplaetzeTableViewer::MakeLines()
{
// AbfluegelvgTableViewer = Vorlage
	int ilSubt = 0;
	int ilTotl = 0;
	int ilSeat = 0;
	int ilTmpSeat = 0;
	int ilSeatTotl = 0;
	CString olTmpSeat;
	CString olNewstr("");
	CString olOldstr(""); 
	CString olNewLvg("");
	CString olOldLvg("");

	int ilCount = pomData->GetSize();

	for (int ilLc = 0; ilLc < ilCount; ilLc++)
	//for (int ilLc = ilCount - 1; ilLc >= 0 ; ilLc--)
	{
		ilSubt++;
		ilTotl++;
		ROTATIONDLGFLIGHTDATA *prlSitzplaetzeData = &pomData->GetAt(ilLc);
		olNewstr = prlSitzplaetzeData->Des3;
		if (strlen(prlSitzplaetzeData->Alc2))
				olNewLvg = prlSitzplaetzeData->Alc2;
		else
				olNewLvg = prlSitzplaetzeData->Alc3;
		//ogActData.GetSeat(prlSitzplaetzeData->Act3, prlSitzplaetzeData->Act5, olTmpSeat);
/*
		if(!ogBCD.GetField("ACT", "ACT3", prlSitzplaetzeData->Act3, "SEAT", olTmpSeat))
			ogBCD.GetField("ACT", "ACT5", prlSitzplaetzeData->Act5, "SEAT", olTmpSeat);
*/
		olTmpSeat = prlSitzplaetzeData->Nose;

		ilTmpSeat = atoi(olTmpSeat);
		ilSeatTotl += ilTmpSeat;
		if(ilLc == 0)	// der erste
		{
			olOldstr = olNewstr;
			if (strlen(prlSitzplaetzeData->Alc2) || strlen(prlSitzplaetzeData->Alc3))
				olOldLvg = olNewLvg;
				ilSubt = 0;
				ilTotl = 0;
		}

		if (olOldLvg != olNewLvg)	// neue LVG
		{
			if(ilSubt > 0)
			{	
				MakeLine(olOldLvg, olOldstr, ilSubt, 0, ilSeat);
				olOldstr = olNewstr;
				ilSubt = 0;
				ilSeat = ilTmpSeat;
			}
			//MakeLine(" ", " ", 0);
			MakeResultLine(olOldLvg, 0, ilTotl);
			ilTotl = 0;
			olOldLvg = olNewLvg;
		}

		else if (olNewstr != olOldstr)	// neue Destination
		{
			//MakeLine(prlSitzplaetzeData, "", olOldstr, ilSubt, 0);
			MakeLine(olOldLvg, olOldstr, ilSubt, 0, ilSeat);
			olOldstr = olNewstr;
			ilSubt = 0;
			ilSeat = ilTmpSeat;
		}
		else
			ilSeat += ilTmpSeat;

		if((ilLc == ilCount - 1) && (ilSubt > 0))	// nach dem letzten
		//if((ilLc == 0) && (ilSubt > 0))	// nach dem letzten
		{
			MakeLine(olNewLvg, olNewstr, ilSubt, 0, ilSeat);
		}
	}

	MakeResultLine(GetString(IDS_STRING1200), ilSeatTotl, ilCount);

//generate the headerinformation
	CString olTimeSet = GetString(IDS_STRING1920); //UTC
	if(bgReportLocal)
		olTimeSet = GetString(IDS_STRING1921); //LOCAL

	char pclHeader[256];
	sprintf(pclHeader, GetString(IDS_R_HEADER_12_3), /*GetString(IDS_REPORTS_RB_SeatAvailability),*/ pcmInfo, ilCount);
	omTableName = pclHeader;
}

//-----------------------------------------------------------------------------------------------
// neue Destination
void SitzplaetzeTableViewer::MakeLine(CString poLvg, CString poDes, int ipSubt, int ipTotl, int ipSeat)
{
 	char clbuffer[10];
   // Update viewer data for this shift record
    SITZPLAETZETABLE_LINEDATA rlSitzplaetzelvg;
		
		rlSitzplaetzelvg.Alc3 = poLvg;
		rlSitzplaetzelvg.Des3 = poDes; 
		if(ipSubt)
		{
			sprintf(clbuffer,"%d", ipSubt);
			rlSitzplaetzelvg.Subt = clbuffer;
		}
		if(ipTotl)
		{
			sprintf(clbuffer,"%d", ipTotl);
			rlSitzplaetzelvg.Totl = clbuffer; 
		}
		if(ipSeat)
		{
			sprintf(clbuffer,"%d", ipSeat);
			rlSitzplaetzelvg.Seat = clbuffer; 
		}
	//ogActData.GetSeat(prpSitzplaetzelvg->Act3, prpSitzplaetzelvg->Act5, rlSitzplaetzelvg.Seat);	

	CreateLine(&rlSitzplaetzelvg);
}


//-----------------------------------------------------------------------------------------------
// neue LVG
void SitzplaetzeTableViewer::MakeResultLine(CString opNewLvg, int ipSeat, int ipCntr)
{
    // Update viewer data for this shift record
	char clbuffer[10];
    SITZPLAETZETABLE_LINEDATA rlSitzplaetzelvg;

		rlSitzplaetzelvg.Alc3 = opNewLvg;

		if(ipSeat > 0)
		{
		sprintf(clbuffer,"%d", ipSeat);
		rlSitzplaetzelvg.Seat = clbuffer;
		}
		else
		rlSitzplaetzelvg.Seat = "";

		sprintf(clbuffer,"%d", ipCntr);
		rlSitzplaetzelvg.Totl = clbuffer; 
	CreateLine(&rlSitzplaetzelvg);
		rlSitzplaetzelvg.Alc3 = "";
		rlSitzplaetzelvg.Seat = "";
		rlSitzplaetzelvg.Totl = ""; 
	CreateLine(&rlSitzplaetzelvg);

}


//-----------------------------------------------------------------------------------------------

void SitzplaetzeTableViewer::CreateLine(SITZPLAETZETABLE_LINEDATA *prpSitzplaetzelvg)
{
	int ilLineno = 0;
	SITZPLAETZETABLE_LINEDATA rlSitzplaetzelvg;
	rlSitzplaetzelvg = *prpSitzplaetzelvg;
    omLines.NewAt(ilLineno, rlSitzplaetzelvg);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void SitzplaetzeTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
//	const int ilColumns = 2;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomSitzplaetzeTable->SetShowSelection(TRUE);
	pomSitzplaetzeTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	rlHeader.Length = 100; 
//	rlHeader.Text = GetString(IDS_STRING1136);
	rlHeader.Text = GetString(IDS_STRING2139);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	rlHeader.Length = 120;
	rlHeader.Text = GetString(IDS_STRING1166);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	rlHeader.Length = 100;
	rlHeader.Text = GetString(IDS_STRING1167);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	rlHeader.Length = 180;
//	rlHeader.Text = GetString(IDS_STRING1168);
	rlHeader.Text = GetString(IDS_STRING2141);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	rlHeader.Length = 180;
//	rlHeader.Text = GetString(IDS_STRING1169);
	rlHeader.Text = GetString(IDS_STRING2140);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomSitzplaetzeTable->SetHeaderFields(omHeaderDataArray);

	pomSitzplaetzeTable->SetDefaultSeparator();
	pomSitzplaetzeTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
     for (int ilLineNo = ilLines - 1; ilLineNo >= 0; ilLineNo--)
  // for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		if(omLines[ilLineNo].Alc2.GetLength())
			rlColumnData.Text = omLines[ilLineNo].Alc2;
		else
			rlColumnData.Text = omLines[ilLineNo].Alc3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Des3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Seat;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Subt;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Totl;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomSitzplaetzeTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomSitzplaetzeTable->DisplayTable();
}


//-----------------------------------------------------------------------------------------------

void SitzplaetzeTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

void SitzplaetzeTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void SitzplaetzeTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	char pclFooter[256];
//	sprintf(pclFooter, GetString(IDS_STRING1170), pcmSelect, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));
	sprintf(pclFooter, omTableName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));

	omFooterName = pclFooter;
	CString olTableName = GetString(IDS_REPORTS_RB_SeatAvailability);


	int ilOrientation = PRINT_PORTRAET;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			//pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			pomPrint->imLineNo = pomPrint->imMaxLines + 1 - 2;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = olTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
//			olFooter1.Format(GetString(IDS_STRING1445), ilLines,omFooterName);
			olFooter1.Format("%s -   %s", omFooterName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );
			//for(int i = 0; i < ilLines; i++ ) 
			for(int i = ilLines-1; i >= 0; i-- ) 
			{
				//if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				if(pomPrint->imLineNo >= pomPrint->imMaxLines-2)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				//if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				// drehen
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-3) || i == 0)
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;

	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool SitzplaetzeTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;
//	double dgCCSPrintFactor = 2.7 ;

/*
	char pclHeader[100];
	//sprintf(pclHeader, "Sitzplatzangebot nach LVGs und/oder Destinationen vom %s", pcmInfo);
	sprintf(pclHeader, GetString(IDS_STRING1170), pcmSelect, pcmInfo);
	CString olTableName(pclHeader);
*/
	//pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);
//	pomPrint->PrintUIFHeader("",CString(pclHeader),pomPrint->imFirstLine-10);
	pomPrint->PrintUIFHeader("", omTableName, pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Text   = omHeaderDataArray[i].Text;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool SitzplaetzeTableViewer::PrintTableLine(SITZPLAETZETABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		
		rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameTop   = PRINT_FRAMETHIN;
		if(bpLastLine == true)
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN;
		}
		else
		{
			rlElement.FrameBottom = PRINT_NOFRAME;
		}
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		switch(i)
		{
		case 0:
			{
				rlElement.Alignment  = PRINT_LEFT;
				rlElement.FrameLeft = PRINT_FRAMETHIN;
				if(strlen(prpLine->Alc2))
					rlElement.Text = prpLine->Alc2;
				else
					rlElement.Text = prpLine->Alc3;
			}
			break;
		case 1:
			{
				rlElement.Alignment  = PRINT_LEFT;
				rlElement.Text		= prpLine->Des3;
			}
			break;
		case 2:
			{
				rlElement.Alignment  = PRINT_LEFT;
				rlElement.Text		= prpLine->Seat;
			}
			break;
		case 3:
			{
				rlElement.Alignment  = PRINT_LEFT;
				rlElement.Text		= prpLine->Subt;
			}
			break;
		case 4:
			{
				rlElement.Alignment  = PRINT_LEFT;
				rlElement.Text		= prpLine->Totl;
				rlElement.FrameRight = PRINT_FRAMETHIN;
			}
			break;
		}

		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		
	}

	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------


// Drucken in Datei
bool SitzplaetzeTableViewer::PrintPlanToFile(char *opTrenner)
{
	ofstream of;

	char* tmpvar;
	tmpvar = getenv("TMP");
	if (tmpvar == NULL)
		tmpvar = getenv("TEMP");

	if (tmpvar == NULL)
		return false;

	CString olFileName = omTableName;
	olFileName.Remove('*');
	olFileName.Remove('.');
	olFileName.Remove(':');
	olFileName.Remove('/');
	olFileName.Replace(" ", "_");
	
	char pHeader[256];
	strcpy (pHeader, tmpvar);
	CString path = pHeader;
	omFileName =  path + "\\" + olFileName + ".csv";

//	of.open( GetString(IDS_STRING1386), ios::out);
	of.open( omFileName, ios::out);

	int ilwidth = 1;

	of  << setw(ilwidth) << omTableName << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;

	of	<< setw(ilwidth) << GetString(IDS_STRING2139) 
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING1166) 
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING1167)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING2141)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING2140) 
		<< endl;


	int ilCount = omLines.GetSize();

	for(int i = ilCount-1; i >=0; i--)
	{
		SITZPLAETZETABLE_LINEDATA rlLine = omLines[i];

		CString olAlc;
		if(strlen(rlLine.Alc2))
			olAlc = rlLine.Alc2;
		else
			olAlc = rlLine.Alc3;

		of.setf(ios::left, ios::adjustfield);

		of   << setw(ilwidth) << olAlc
		     << setw(1) << opTrenner 
			 << setw(ilwidth) << rlLine.Des3 
		     << setw(1) << opTrenner 
			 << setw(ilwidth) << rlLine.Seat
		     << setw(1) << opTrenner 
			 << setw(ilwidth) << rlLine.Subt
		     << setw(1) << opTrenner 
			 << setw(ilwidth) << rlLine.Totl
			 << endl;

	}

	of.close();
	return true;


/*
	char pclHeader[100];
	sprintf(pclHeader, GetString(IDS_STRING1171), pcmSelect);
	ofstream of;
	of.open( pcpDefPath, ios::out);
	//of << "Sitzplatzangebot nach LVGs und/oder Destinationen: " 
	of << GetString(IDS_STRING1172) << "  " << pclHeader << "  "
		<< CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M") << endl << endl;

	of  << GetString(IDS_STRING1136) << "       " 
		<< GetString(IDS_STRING1166) << "   " 
		<< GetString(IDS_STRING1167) << "    " 
		<< GetString(IDS_STRING1168) << "  " 
		<< GetString(IDS_STRING1169) 
		<< endl;

	of << "--------------------------------------------------------------------" << endl;
	int ilCount = omLines.GetSize();
	for(int i = ilCount-1; i >=0; i--)
	{
		SITZPLAETZETABLE_LINEDATA rlD = omLines[i];
		of.setf(ios::left, ios::adjustfield);
		of   << setw(10) << rlD.Alc3  
			 << setw(16) << rlD.Des3  
			 << setw(14) << rlD.Seat 
			 << setw(16) << rlD.Subt  
			 << setw(10) << rlD.Totl << endl;
	}
	of.close();
*/
//	MessageBox("Es wurde folgende Datei geschrieben:\nc:\\tmp\\Sitzplaetze.txt", "Drucken",(MB_ICONINFORMATION|MB_OK));
	return true;
}

