// DiffPosTableDlg.h



#if !defined(AFX_DIFFPOSTABLEDLG_H__39D1ABF1_278C_11D2_8588_0000C04D916B__INCLUDED_)
#define AFX_DIFFPOSTABLEDLG_H__39D1ABF1_278C_11D2_8588_0000C04D916B__INCLUDED_



#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// DiffPosTableDlg.h : header files
//

#include <DiffPosTableViewer.h>
#include <CCSTable.h>
#include <resrc1.h>

/////////////////////////////////////////////////////////////////////////////
// DiffPosTableDlg dialog

class DiffPosTableDlg : public CDialog
{
// Construction
public:
	DiffPosTableDlg(CWnd* pParent = NULL);   // standard constructor
	~DiffPosTableDlg();

	void UpdateCaption();
	void Rebuild();

	int GetCount() const;



// Dialog Data
	//{{AFX_DATA(DiffPosTableDlg)
	enum { IDD = IDD_DIFFPOSTABLE };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DiffPosTableDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	CCSTable *pomTable;
	DiffPosTableViewer *pomViewer;

	// Generated message map functions
	//{{AFX_MSG(DiffPosTableDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnClose();
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
    afx_msg LONG OnTableReturnPressed(UINT wParam, LONG lParam);
	afx_msg LONG OnTableSelChanged( UINT wParam, LPARAM lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CWnd *pomParent;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIFFPOSTABLEDLG_H__39D1ABF1_278C_11D2_8588_0000C04D916B__INCLUDED_)
