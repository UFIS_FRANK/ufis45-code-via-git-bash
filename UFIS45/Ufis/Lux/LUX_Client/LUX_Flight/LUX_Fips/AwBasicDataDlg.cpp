// AwBasicDataDlg.cpp : implementation file
//

#include <stdafx.h>
#include <fpms.h>
#include <AwBasicDataDlg.h>
#include <PrivList.h>
#include <CedaBasicData.h>
#include <CCSBasicFunc.h>
#include <CCSGlobl.h>
#include <CCSDdx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif






/////////////////////////////////////////////////////////////////////////////
// AwBasicDataDlg dialog


AwBasicDataDlg::AwBasicDataDlg(CWnd* pParent, CString opObject, CString opFields, CString opSortFields, CString opSelString)
	: CDialog(AwBasicDataDlg::IDD, pParent)
{
	omObject = opObject;
	omSortFields = opSortFields;
	omFields = opFields;
	pomRecord = NULL;
	pomParent = pParent;

	imLastSortIndex = 0;

	omSelString = opSelString;

	cmSecState = '1';
	//{{AFX_DATA_INIT(AwBasicDataDlg)
	//}}AFX_DATA_INIT
}

AwBasicDataDlg::~AwBasicDataDlg()
{
	if(pomRecord != NULL)
		delete pomRecord;
}



void AwBasicDataDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(AwBasicDataDlg)
	DDX_Control(pDX, IDOK, m_CB_OK);
	DDX_Control(pDX, IDCANCEL, m_CB_Cancel);
	DDX_Control(pDX, IDC_HEADER, m_CS_Header);
	DDX_Control(pDX, IDC_LIST, m_CL_List);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(AwBasicDataDlg, CDialog)
	//{{AFX_MSG_MAP(AwBasicDataDlg)
	ON_WM_SIZE()
	ON_LBN_DBLCLK(IDC_LIST, OnDblclkList)
	ON_WM_LBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// AwBasicDataDlg message handlers


CString AwBasicDataDlg::GetField(CString opField)
{
	CString olRet;

	if(pomRecord != NULL)
	{
		int ilIndex = ogBCD.GetFieldIndex(omObject,opField);
		if(ilIndex >= 0)
			olRet = (*pomRecord)[ilIndex];
	}

	return olRet;
}


void AwBasicDataDlg::OnOK() 
{
	int ilIndex = m_CL_List.GetCurSel();
	if(ilIndex != LB_ERR)
	{
		pomRecord = new RecordSet(ogBCD.GetFieldCount(omObject));
		ogBCD.GetRecord(omObject,ilIndex, *pomRecord);
	}
	CDialog::OnOK();
}


BOOL AwBasicDataDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if (cmSecState != '1')
		m_CB_OK.EnableWindow(FALSE);


	CString olItem;	

	SetWindowText(ogBCD.GetObjectDesc(omObject));

	CString olHeader = ogBCD.GetTableHeader(omObject, omFields, "  ");

	m_CS_Header.SetFont(&ogCourier_Regular_10);
	m_CS_Header.SetWindowText(olHeader);

	int ilCount = ogBCD.GetDataCount(omObject);

	m_CL_List.ResetContent( );
	m_CL_List.SetFont(&ogCourier_Regular_10);
	m_CL_List.InitStorage( ilCount, 100 );


	RecordSet olRecord;
	ogBCD.SetSort(omObject, omSortFields, true);


	int ilMaxCount = ogBCD.ShowAllFields(omObject, omFields, &m_CL_List, omSelString);

	if(ilMaxCount > 40)
	{

		if(pomParent == NULL)
			pomParent = GetParent();
		if(pomParent == NULL)
			return TRUE;

		int ilCharPixel = 9;

		ilCount = ilMaxCount - 40;

		CRect olRect;
		
		GetWindowRect(olRect);

		int ilCY = olRect.bottom - olRect.top;
		int ilCX = olRect.right - olRect.left + ilCount * ilCharPixel;

		CRect r;
			
		pomParent->GetClientRect(r);

		pomParent->ClientToScreen(r);

		int ilMx = ((r.right - r.left) / 2 ) + r.left;
		int ilMy = ((r.bottom - r.top) / 2 ) + r.top;

		SetWindowPos( &wndTop, ilMx - (ilCX / 2), ilMy - (ilCY / 2),  ilCX, ilCY, SWP_NOZORDER | SWP_SHOWWINDOW );
	}

	int anz = m_CL_List.GetCount();
	if (omSelString.IsEmpty())
	{
//		int k = m_CL_List.GetCurSel();
//		m_CL_List.SetSel(k, FALSE );
		m_CL_List.SetCurSel(-1);
		m_CL_List.SetTopIndex(0);
	}


	


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void AwBasicDataDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	CRect olClient;
	GetClientRect(olClient);
	ClientToScreen(olClient);

	CRect olRect;
	int ilCX;
	int ilCY;
	int ilX;
	int ilY;

	if(m_hWnd != NULL)
	{
		if(m_CS_Header.m_hWnd != NULL)
		{
			m_CS_Header.GetWindowRect(olRect);

			ilCX = olClient.right - olClient.left - 21;
			ilCY = olRect.bottom - olRect.top;

			m_CS_Header.SetWindowPos( NULL, 0, 0,  ilCX, ilCY, SWP_NOZORDER | SWP_NOMOVE | SWP_SHOWWINDOW );
		}
		
		if(m_CL_List.m_hWnd != NULL)
		{
			m_CL_List.GetWindowRect(olRect);

			ilCX = olClient.right - olClient.left - 18;
			ilCY = cy - 80;
	
			m_CL_List.SetWindowPos( NULL, 0, 0,  ilCX, ilCY, SWP_NOZORDER | SWP_NOMOVE | SWP_SHOWWINDOW );
		}

		if(m_CB_OK.m_hWnd != NULL)
		{
			m_CB_OK.GetWindowRect(olRect);

			ilY = cy - 30;
			ilX = (cx / 2) - 17 - (olRect.right - olRect.left);

			m_CB_OK.SetWindowPos( NULL, ilX, ilY, 0 ,0 ,SWP_NOZORDER | SWP_NOSIZE | SWP_SHOWWINDOW );
		}

		if(m_CB_Cancel.m_hWnd != NULL)
		{
			m_CB_Cancel.GetWindowRect(olRect);

			ilY = cy - 30;
			ilX = (cx / 2) + 17;

			m_CB_Cancel.SetWindowPos( NULL, ilX, ilY, 0 ,0 ,SWP_NOZORDER | SWP_NOSIZE | SWP_SHOWWINDOW );
		}
	}
}

void AwBasicDataDlg::OnDblclkList() 
{
	if (cmSecState == '1')
		OnOK();	
}

void AwBasicDataDlg::OnLButtonDown(UINT nFlags, CPoint point) 
{

	CRect olRect;

	m_CS_Header.GetClientRect(olRect);
	m_CS_Header.ClientToScreen(olRect);


	ScreenToClient(olRect);

	if(olRect.PtInRect(point))
	{
		/*
		CString olTmp;

		CStringArray olFields;
		ExtractItemList(omFields, olFields);

		UIntArray olLength;

		for( int i = 0; i < olFields.GetCount(); i++)
		{
			olTmp = ogBCD.GetTableHeader(omObject, olFields[i], "  ");
			olLength.Add(olTmp.GetLength());
		}
		*/

		CStringArray olFields;

		ExtractItemList(omFields, &olFields);

		/*
		CString olTmp = olFields[0];

		olFields.Add(olTmp);
		olFields.RemoveAt(0);

		omSortFields = "";

		for(int i = 0; i < olFields.GetSize(); i++)
		{
			omSortFields +=  olFields[i] + CString("+,");
		}

		if(!omSortFields.IsEmpty())
			omSortFields = omSortFields.Left(omSortFields.GetLength() - 1 );
		*/

		imLastSortIndex++;

		if(olFields.GetSize() <= imLastSortIndex )
			imLastSortIndex = 0;

		omSortFields =  olFields[imLastSortIndex] + CString("+");


		ogBCD.SetSort(omObject, omSortFields, true);
		

		m_CL_List.ResetContent();

		ogBCD.ShowAllFields(omObject, omFields, &m_CL_List, omSelString);




	}


	CDialog::OnLButtonDown(nFlags, point);
}





void AwBasicDataDlg::SetSecState(const CString &ropSecStr)
{
	cmSecState = ogPrivList.GetStat(ropSecStr);
}

