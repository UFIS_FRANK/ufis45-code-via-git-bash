// CCSGlobl.cpp: implementation of the CCSGlobl .
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <resource.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <CCSCedaCom.h>
#include <CCSDdx.h>
#include <CCSLog.h>
#include <CCSBcHandle.h>
#include <SeasonCollectCedaFlightData.h>
#include <SeasonDlgCedaFlightData.h>
#include <RotationCedaFlightData.h>
#include <RotationDlgCedaFlightData.h>
#include <RotGDlgCedaFlightData.h>
#include <BasicData.h>
#include <PrivList.h>
#include <CedaBasicData.h>
#include <CCSBasic.h>
#include <CedaInfData.h>
#include <Konflikte.h>
#include <DiaCedaFlightData.h>
#include <CcaCedaFlightData.h>
#include <DataSet.h>
#include <SeasonCedaFlightData.h>
#include <SpotAllocation.h>
#include <CedaResGroupData.h>
#include <DailyCedaFlightData.h>
#include <UFISAmSink.h>
#include <CedaFlightUtilsData.h>
CString GetString(UINT ID)
{
	CString olRet;
	olRet.LoadString(ID);
	return olRet;
}


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Global Variable Section

IUFISAmPtr pConnect; // for com-interface

CString ogAppName = "FIPS"; 
CString ogCustomer = "SHA"; 

char pcgUser[33];
char pcgPasswd[33];

char pcgHome[4] = "HAJ";
char pcgHome4[5] = "EDDV";
char pcgTableExt[10] = "HAJ";
char pcgVersion[12] = "4.4.2.9" ;
char pcgInternalBuild[12] = "009";


CTimeSpan ogPosDefAllocDur(0, 0, 30, 0);
CTimeSpan ogGatDefAllocDur(0, 0, 30, 0);
CTimeSpan ogBltDefAllocDur(0, 0, 30, 0);
CTimeSpan ogWroDefAllocDur(0, 0, 30, 0);

// timspan for postfilghts
	//days in the past
CTimeSpan ogTimeSpanPostFlight(0, 0, 0, 0);
	//days in the future
CTimeSpan ogTimeSpanPostFlightInFuture(0, 0, 0, 0);


CTimeSpan ogLocalDiff;
CTimeSpan ogUtcDiff;
CTime ogUtcStartTime;

CStringArray ogDssfFields;
int ogDssfFltiId = -1;

ofstream of_catch;
ofstream of_debug;
ofstream *pogBCLog;

bool bgDebug;

bool bgComOk = false;

CCSLog          ogLog(NULL);
CCSDdx			ogDdx;
CCSCedaCom      ogCommHandler(&ogLog, NULL);  
CCSBcHandle     ogBcHandle(&ogDdx, &ogCommHandler, &ogLog);
CedaBasicData	ogBCD(pcgHome, &ogDdx, &ogBcHandle, &ogCommHandler, &ogCCSBasic);

CCSBasic ogCCSBasic;

DailyCedaFlightData  ogDailyCedaFlightData;

CBasicData      ogBasicData;
CedaCfgData		ogCfgData;
PrivList		ogPrivList;
CedaResGroupData ogResGroupData;
CButtonListDlg	*pogButtonList;
SeasonAskDlg	*pogSeasonAskDlg;
RotationTables  *pogRotationTables;
CSeasonTableDlg *pogSeasonTableDlg;
CSeasonDlg		*pogSeasonDlg;
RotationDlg		*pogRotationDlg;
RotGroundDlg	*pogRotGroundDlg;
FlightDiagram	*pogFlightDiagram;
BltDiagram		*pogBltDiagram;
GatDiagram		*pogGatDiagram;
PosDiagram		*pogPosDiagram;
WroDiagram		*pogWroDiagram;
CcaDiagram		*pogCcaDiagram;
DailyScheduleTableDlg* pogDailyScheduleTableDlg;

CReportSelectDlg *pogReportSelectDlg;
FlightSearchTableDlg *pogFlightSearchTableDlg;

WoResTableDlg	*pogWoResTableDlg;

CicDemandTableDlg *pogCicDemandTableDlg;
CicNoDemandTableDlg *pogCicNoDemandTableDlg;
CicConfTableDlg *pogCicConfTableDlg;

KonflikteDlg	*pogKonflikteDlg;
Konflikte		ogKonflikte;

DataSet			ogDataSet;


SeasonCollectDlg *pogSeasonCollectDlg;

SeasonDlgCedaFlightData  ogSeasonDlgFlights;
SeasonCollectCedaFlightData  ogSeasonCollectFlights;
CedaFlightUtilsData ogFlightUtilsData;
SeasonCedaFlightData  ogSeasonFlights;
RotationCedaFlightData ogRotationFlights;
RotationDlgCedaFlightData ogRotationDlgFlights;
//RotGDlgCedaFlightData ogRotGDlgFlights;
CcaCedaFlightData ogCcaDiaFlightData;
DiaCedaFlightData ogDiaFlightData;
//DiaCedaFlightData ogBltDiaFlightData;
//DiaCedaFlightData ogGatDiaFlightData;
DiaCedaFlightData ogPosDiaFlightData;
//DiaCedaFlightData ogWroDiaFlightData;

SpotAllocation ogSpotAllocation;


CedaInfData	ogInfData;

CBackGround *pogBackGround;
CCcaCommonTableDlg *pogCommonCcaTable;
CCcaCommonDlg *pogCommonCcaDlg;

// For Com-Interface to the new TelexPool
CUFISAmSink m_UFISAmSink;
//

bool bgLocal = true;

bool bgViewBigBobs = false;
bool bmViewBigBobs = false;

bool bgSeasonLocal   = true;
bool bgGatPosLocal   = true;
bool bgReportLocal   = true;
bool bgDailyLocal = false;
bool bgPrintDlgOpen = false;
bool bgDailyWithAlt = false;
bool bgCreateDaily_OrgAtdBlue = false;

bool bgViewEditFilter = false;
bool bgViewEditSort = false;


bool bgShowOnGround = true;
bool bgCheckAllConflicts = true;
CTimeSpan ogOnGroundTimelimit (0,0,0,0);


CString ogFIDRemarkField = "BEMD";
bool bgAirport4LC   = true;
CTimeSpan ogPosAllocBufferTime(0,0,0,0);
CTimeSpan ogGateAllocBufferTime(0,0,0,0);


// flags for customer functionality configuration
bool bgOffRelFuncGatpos = true;
bool bgOffRelFuncCheckin = true;

bool bgExtBltCheck = false;
bool bgFlightIntYelBar = false;
bool bgFlightIDEditable = false;

CString ogInsertString = "";
CString ogUpdateString = "";
CString ogDeleteString = "";

bool	bgInsert = false;
bool	bgDelete = false;
bool	bgUpdate = false;

bool bgBltHeight = true;
bool bgGatHeight = true;
bool bgPosHeight = true;
bool bgWroHeight = true;
/////




bool bgKlebefunktion;

long  igWhatIfUrno;
CString ogWhatIfKey;


bool bgNoScroll;
bool bgOnline = true;
bool bgIsButtonListMovable = false;
CInitialLoadDlg *pogInitialLoad;

bool bgPosDiaAutoAllocateInWork = false;


CBrush *pogGatBrush;
CBrush *pogPosBrush;
CBrush *pogBltBrush;
CBrush *pogCcaBrush;
CBrush *pogWroBrush;
CBrush *pogNotAvailBrush;
CBrush *pogNotValidBrush;



CFont ogSmallFonts_Regular_6;
CFont ogSmallFonts_Regular_7;
CFont ogMSSansSerif_Regular_6;
CFont ogMSSansSerif_Regular_8;
CFont ogMSSansSerif_Regular_12;
CFont ogMSSansSerif_Regular_16;
CFont ogMSSansSerif_Bold_8;
CFont ogMSSansSerif_Bold_7;
CFont ogCourier_Bold_10;
CFont ogCourier_Bold_8;
CFont ogCourier_Regular_10;
CFont ogCourier_Regular_8;
CFont ogCourier_Regular_9;
CFont ogMS_Sans_Serif_8;

CFont ogTimesNewRoman_9;
CFont ogTimesNewRoman_12;
CFont ogTimesNewRoman_16;
CFont ogTimesNewRoman_30;

CFont ogSetupFont;


CFont ogScalingFonts[30];
FONT_INDEXES ogStaffIndex;
FONT_INDEXES ogFlightIndex;
FONT_INDEXES ogHwIndex;
FONT_INDEXES ogRotIndex;

BOOL bgIsInitialized = FALSE;

CBrush *ogBrushs[MAXCOLORS+1];
COLORREF ogColors[MAXCOLORS+1];

COLORREF lgBkColor = SILVER;
COLORREF lgTextColor = BLACK;
COLORREF lgHilightColor = WHITE;


CPoint ogMaxTrackSize = CPoint(1024, 768);
CPoint ogMinTrackSize = CPoint(1024 / 4, 768 / 4);

int   igDaysToRead;
int igFontIndex1;
int igFontIndex2;

CBitmap ogBMNotAvail;
CBitmap ogBMNotValid;


/////////////////////////////////////////////////////////////////////////////

void CreateBrushes()
{
	 int ilLc;

	for( ilLc = 0; ilLc < FIRSTCONFLICTCOLOR; ilLc++)
		ogColors[ilLc] = RED;
	ogColors[2] = GRAY;
	ogColors[3] = GREEN;
	ogColors[4] = RED;
	ogColors[5] = BLUE;
	ogColors[6] = SILVER;
	ogColors[7] = MAROON;
	ogColors[8] = OLIVE;
	ogColors[9] = NAVY;
	ogColors[10] = PURPLE;
	ogColors[11] = TEAL;
	ogColors[12] = LIME;
	ogColors[13] = YELLOW;
	ogColors[14] = FUCHSIA;
	ogColors[15] = AQUA;
	ogColors[16] = WHITE;
	ogColors[17] = BLACK;
	ogColors[18] = ORANGE;
	ogColors[19] = TOWING;
	ogColors[20] = CREAM;


// don't use index 21 it's used for Break jobs already

	for(ilLc = 0; ilLc < MAXCOLORS; ilLc++)
	{
		ogBrushs[ilLc] = new CBrush(ogColors[ilLc]);
	}

	// create a break job brush pattern
	/*
    delete ogBrushs[21];
	CBitmap omBitmap;
	omBitmap.LoadBitmap(IDB_BREAK);
	ogBrushs[21] = new CBrush(&omBitmap);
    */

	pogGatBrush = new CBrush(TEAL);
	pogPosBrush = new CBrush(NAVY);
	pogBltBrush = new CBrush(OLIVE);
	pogCcaBrush = new CBrush(MAROON);
	pogWroBrush = new CBrush(PURPLE);

	ogBMNotAvail.LoadBitmap(IDB_NOTAVAIL);
	pogNotAvailBrush = new CBrush(&ogBMNotAvail);


	ogBMNotValid.LoadBitmap(IDB_NOTVALID);
	pogNotValidBrush = new CBrush(&ogBMNotValid);


}

void DeleteBrushes()
{
	for( int ilLc = 0; ilLc < MAXCOLORS; ilLc++)
	{
		CBrush *olBrush = ogBrushs[ilLc];
		delete olBrush;
	}

	delete pogGatBrush;
	delete pogPosBrush;
	delete pogBltBrush;
	delete pogCcaBrush;
	delete pogWroBrush;
	delete pogNotAvailBrush;
	delete pogNotValidBrush;

}


void InitFont() 
{
    CDC dc;
    dc.CreateCompatibleDC(NULL);

    LOGFONT logFont;
    memset(&logFont, 0, sizeof(LOGFONT));


////////////////////

	for(int i = 0; i < 8; i++)
	{
		logFont.lfHeight = - MulDiv(i, dc.GetDeviceCaps(LOGPIXELSY), 72);
		logFont.lfWeight = FW_BOLD;
		logFont.lfQuality = PROOF_QUALITY;
		logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
		lstrcpy(logFont.lfFaceName, "Small Fonts"); 
		ogScalingFonts[i].CreateFontIndirect(&logFont);
	}
	for( i = 8; i < 20; i++)
	{ 
		logFont.lfHeight = - MulDiv(i, dc.GetDeviceCaps(LOGPIXELSY), 72);
		logFont.lfWeight = FW_BOLD;
		logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
		logFont.lfQuality = PROOF_QUALITY;

		lstrcpy(logFont.lfFaceName, "MS Sans Serif"); /*"MS Sans Serif""Arial"*/
		ogScalingFonts[i/*GANTT_S_FONT*/].CreateFontIndirect(&logFont);
	}


    // ogMS_Sans_Serif_8.CreateFont(15, 0, 0, 0, 400, FALSE, FALSE, 0, ANSI_CHARSET, 
	//							 OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, "MS Sans Serif");



    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = DEFAULT_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ogMS_Sans_Serif_8.CreateFontIndirect(&logFont);


////////////////////


    logFont.lfHeight = - MulDiv(6, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "Small Fonts");
    ogSmallFonts_Regular_6.CreateFontIndirect(&logFont);
        
    logFont.lfHeight = - MulDiv(7, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "Small Fonts");
    ogSmallFonts_Regular_7.CreateFontIndirect(&logFont);

    logFont.lfHeight = - MulDiv(10, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ogCourier_Bold_10.CreateFontIndirect(&logFont);

	 logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ogCourier_Bold_8.CreateFontIndirect(&logFont);

    logFont.lfHeight = - MulDiv(10, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ogCourier_Regular_10.CreateFontIndirect(&logFont);

    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ogCourier_Regular_8.CreateFontIndirect(&logFont);


	logFont.lfHeight = - MulDiv(9, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ogCourier_Regular_9.CreateFontIndirect(&logFont);


/*
	logFont.lfHeight = - MulDiv(9, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = 200;
    logFont.lfQuality = PROOF_QUALITY;
    logFont.lfOutPrecision = OUT_CHARACTER_PRECIS;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Fixedsys");
    ASSERT(ogCourier_Regular_9.CreateFontIndirect(&logFont));
*/
/*
    logFont.lfHeight = - MulDiv(6, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "Small Fonts"); /
    ASSERT(ogScalingFonts[MS_SANS6].CreateFontIndirect(&logFont));

    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ASSERT(ogScalingFonts[MS_SANS8].CreateFontIndirect(&logFont));

    logFont.lfHeight = - MulDiv(12, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ASSERT(ogScalingFonts[MS_SANS12].CreateFontIndirect(&logFont));

	// logFont.lfHeight = - MulDiv(16, dc.GetDeviceCaps(LOGPIXELSY), 72);
	logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ASSERT(ogScalingFonts[MS_SANS16].CreateFontIndirect(&logFont));
*/ 

	ogStaffIndex.VerticalScale = MS_SANS8;
	ogStaffIndex.Chart = MS_SANS6;
	ogFlightIndex.VerticalScale = MS_SANS8;
	ogFlightIndex.Chart = MS_SANS6;
	ogHwIndex.VerticalScale = MS_SANS8;
	ogHwIndex.Chart = MS_SANS6;

    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ogMSSansSerif_Bold_8.CreateFontIndirect(&logFont);



    logFont.lfHeight = - MulDiv(7, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "Verdana");
    ogMSSansSerif_Bold_7.CreateFontIndirect(&logFont);


	// 9 - 16 - 12 - 30

    logFont.lfHeight = - MulDiv(30, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	//logFont.lfItalic = TRUE;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Times New Roman");
    ogTimesNewRoman_30.CreateFontIndirect(&logFont);

    logFont.lfHeight = - MulDiv(16, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	//logFont.lfItalic = TRUE;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Times New Roman");
    ogTimesNewRoman_16.CreateFontIndirect(&logFont);

    logFont.lfHeight = - MulDiv(12, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	//logFont.lfItalic = TRUE;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Times New Roman");
    ogTimesNewRoman_12.CreateFontIndirect(&logFont);

    logFont.lfHeight = - MulDiv(9, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	//logFont.lfItalic = TRUE;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Times New Roman");
    ogTimesNewRoman_9.CreateFontIndirect(&logFont);


	logFont.lfHeight = - MulDiv(9, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ogSetupFont.CreateFontIndirect(&logFont);


    dc.DeleteDC();
}




