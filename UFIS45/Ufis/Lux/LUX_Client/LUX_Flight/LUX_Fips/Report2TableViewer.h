
#ifndef __ABFLUEGETABLEVIEWER_H__
#define __ABFLUEGETABLEVIEWER_H__

#include <stdafx.h>
#include <RotationDlgCedaFlightData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct ABFLUEGETABLE_LINEDATA
{
	long		Urno; 	// Eindeutige Datensatz-Nr.
	CString		Flno;	// komplette Flugnummer (Airline, Nummer, Suffix)
	CTime 		Tifd; 	// Zeitrahmen Abflug
	CString		Ttyp; 	// Verkehrsart
	CString		Des3; 	// Bestimmungsflughafen 3-Lettercode
	CString		Via3;// (Via3) 	// Liste der Zwischenstationen/-zeiten (Via3)
	CString		Act; 	// Flugzeug-3-Letter Code (IATA)
	CString		Gtd1; 	// Gate 1 Abflug
	CString		Gtd2; 	// Doppelgate Abflug
	CString		Pstd; 	// Position Abflug
	CString		Wro1; 	// Warteraum
	CTime		Cicf; 	// Checkin-Schalter


};

/////////////////////////////////////////////////////////////////////////////
// AbfluegeTableViewer

class AbfluegeTableViewer : public CViewer
{
// Constructions
public:
    AbfluegeTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo = NULL);
    ~AbfluegeTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
    void MakeLines();
	//void MakeLine(ROTATIONDLGFLIGHTDATA *prpAbfluege);
	int CompareAbfluege(ABFLUEGETABLE_LINEDATA *prpAbfluege1, ABFLUEGETABLE_LINEDATA *prpAbfluege2);
	int  GetVia(CCSPtrArray<VIADATA> *opVias, ROTATIONDLGFLIGHTDATA *prpFlight);
// Operations
public:
	void DeleteAll();
	void CreateLine(ABFLUEGETABLE_LINEDATA *prpAbfluege);
	void DeleteLine(int ipLineno);
	bool PrintPlanToFile(char *pcpDefPath);
	void PrintPlanToTxt(char *pcpDefPath);
// Window refreshing routines
public:
	void UpdateDisplay();
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomAbfluegeTable;
	CCSPtrArray<ROTATIONDLGFLIGHTDATA> *pomData;
	CCSPtrArray<CCADATA> omDCins;
	CCSPtrArray<CCADATA> omDCinsSave;
public:
    CCSPtrArray<ABFLUEGETABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(ABFLUEGETABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader(CTime popTifd);
	CCSPrint *pomPrint;
	CString omTableName;
	CString omFileName;
	char *pcmInfo;
	CString omFooterName;
};

#endif //__ABFLUEGETABLEVIEWER_H__
