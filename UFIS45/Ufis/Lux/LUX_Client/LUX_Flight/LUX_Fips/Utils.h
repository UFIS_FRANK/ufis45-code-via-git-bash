// Utils.h : header file
//
// Modification History: 
// 22-nov-00	rkr		IsPostFlight() added
// 05-dec-00	rkr		ModifyWindowText() added

#if !defined(Utils_H__INCLUDED_)
#define Utils_H__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


int InsertSortInStrArray(CStringArray &ropStrArray, const CString &ropStr);
bool FindInStrArray(const CStringArray &ropStrArray, const CString &ropStr);
int FindIdInStrArray(const CStringArray &ropStrArray, const CString &ropStr);

bool IsRegularFlight(char Ftyp);
bool IsArrivalFlight(const char *pcpOrg, const char *pcpDes, char Ftyp);
bool IsArrival(const char *pcpOrg, const char *pcpDes);
bool IsDepartureFlight(const char *pcpOrg, const char *pcpDes, char Ftyp);
bool IsDeparture(const char *pcpOrg, const char *pcpDes);
bool IsCircularFlight(const char *pcpOrg, const char *pcpDes, char Ftyp);
bool IsCircular(const char *pcpOrg, const char *pcpDes);

bool DelayCodeToAlpha(char *pcpDcd);
bool DelayCodeToNum(char *pcpDcd);

int FindInListBox(const CListBox &ropListBox, const CString &ropStr);

// A substitute for CListBox::GetSelItems(...)
// because this function dont't work on some WinNT machines
int MyGetSelItems(CListBox &ropLB, int ipMaxAnz, int *prpItems);

COLORREF GetColorOfFlight(char FTyp);

long MyGetUrnoFromSelection(const CString &ropSel);

bool GetPosDefAllocDur(const CString &ropPos, int ipMinGT, CTimeSpan &ropDura);
bool GetGatDefAllocDur(const CString &ropGate, CTimeSpan &ropDura);
bool GetBltDefAllocDur(const CString &ropBlt, CTimeSpan &ropDura);
bool GetWroDefAllocDur(const CString &ropWro, CTimeSpan &ropDura);

bool GetFltnInDBFormat(const CString &ropUserFltn, CString &ropDBFltn);

bool GetCurrentUtcTime(CTime &ropUtcTime);

// true/false; outside/inside the timelimit
//bool IsPostFlight(CTime opTimeFromFlight, bool bpLocalTime = false);
bool IsPostFlight(CTime opTimeFromFlight, bool bpLocalTime);
bool IsPostFlight(const CTime& opTimeFromFlight);
BOOL ModifyWindowText(CWnd *opWnd, CString& opStrWnd, BOOL bpClean = FALSE);
BOOL CheckPostFlightPosDia(const long lpBarUrno, CWnd* opCWnd);
BOOL CheckPostFlightCcaDia(const long lpBarUrno, CWnd* opCWnd);


bool DBStrIsEmpty(const char *prpStr);


#endif