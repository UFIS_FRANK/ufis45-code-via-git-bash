#if !defined(AFX_PSGATGEOMETRIE_H__1B055C53_8B1E_11D1_B444_0000B45A33F5__INCLUDED_)
#define AFX_PSGATGEOMETRIE_H__1B055C53_8B1E_11D1_B444_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// PSGatGeometry.h : header file
//
#include <CCSTable.h>
#include <resource.h>
#include <CCSDragDropCtrl.h>
#include <CCSPtrArray.h>
//#include "CedaPfcData.h"
//#include "CedaGegData.h"


struct PS_GAT_GEO_GROUP_DATA
{
	CString Name;
	CString Typ;
	CCSPtrArray<CString> Codes;
};

#define ONE_HOUR_POINT  (10 / 2)
/////////////////////////////////////////////////////////////////////////////
// PSGatGeometry dialog

class PSGatGeometry : public CPropertyPage
{
	DECLARE_DYNCREATE(PSGatGeometry)

// Construction
public:
	PSGatGeometry();
	~PSGatGeometry();

	CCSPtrArray<PS_GAT_GEO_GROUP_DATA> omGroupData;
	PS_GAT_GEO_GROUP_DATA *pomCurrentGroup;

	CStringArray omValues;
	CString omCalledFrom;
	bool blIsInit;
    BOOL bmNotMoreThan12;
    CTime   m_TimeScale;
	int		m_Percent;
	int		m_Hour;
	int		m_FontHeight;

	bool bmMaxDefault,
		 bmLinesDefault,
		 bmTimeDefault,
		 bmVertDefault,
		 bmDisplayBeginDefault;

	CString omCurrentGroupBy;
//	CCSPtrArray<GEGDATA>	omGegData;
//	CCSPtrArray<PFCDATA>	omPfcData;
	CCSTable *pomGroupTable;

	CCSDragDropCtrl m_DragDropSource;
	CCSDragDropCtrl m_DragDropTarget;

	void SetCalledFrom(CString opCalledFrom);
// Dialog Data
	//{{AFX_DATA(PSGatGeometry)
	enum { IDD = IDD_PSGATGEOMETRY };
	CStatic	m_TestText;
	CSliderCtrl	m_Font;
	CEdit	m_Date;
	CButton	m_Min;
	CButton	m_Max;
	CButton	m_Pfc;
	CButton	m_Geg;
	CButton	m_New;
	CListBox	m_ResList;
	CSliderCtrl	m_Height;
	CButton	m_TimeLines;
	CButton	m_Delete;
	CButton	m_24h;
	CButton	m_12h;
	CButton	m_10h;
	CButton	m_8h;
	CButton	m_6h;
	CButton	m_4h;
	CButton	m_100Proz;
	CButton	m_75Proz;
	CButton	m_50Proz;
	CStatic	m_GRUPPE;
	CCSEdit	m_Time;
	CButton m_GroupBy1;
	CButton m_GroupBy2;
	CButton m_GroupBy3;
	CButton m_GroupBy4;
	CButton m_GroupBy5;
	int		m_GroupValue;
	//}}AFX_DATA

	void ShowTable();
	void ShowResList();
	void SetData();
	void GetData();
// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(PSGatGeometry)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(PSGatGeometry)
	virtual BOOL OnInitDialog();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnNew();
	afx_msg void OnGeg();
	afx_msg void OnPfc();
	afx_msg void On10h();
	afx_msg void On12h();
	afx_msg void On24h();
	afx_msg void On4h();
	afx_msg void On6h();
	afx_msg void On8h();
	afx_msg void OnSelchangeList1();
	afx_msg LONG OnTableDragBegin(UINT wParam, LONG lParam);
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
	afx_msg LONG OnTableLButtonDown(UINT wParam, LONG lParam);
	afx_msg void OnDelete();
	afx_msg void OnGrp1();
	afx_msg void OnGrp2();
	afx_msg void OnGrp3();
	afx_msg void OnGrp4();
	afx_msg void OnGrp5();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PSGEOMETRIE_H__1B055C53_8B1E_11D1_B444_0000B45A33F5__INCLUDED_)
