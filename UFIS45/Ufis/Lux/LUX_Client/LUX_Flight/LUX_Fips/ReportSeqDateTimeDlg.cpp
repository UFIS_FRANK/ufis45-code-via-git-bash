// ReportSeqDateTimeDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <fpms.h>
#include <ReportSeqDateTimeDlg.h>
#include <CCSTime.h>
#include <CcsGlobl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CReportSeqDateTimeDlg 


CReportSeqDateTimeDlg::CReportSeqDateTimeDlg(CWnd* popParent, const CString &ropHeadline, CTime* popMinDate, CTime *popMaxDate)
 	: CDialog(CReportSeqDateTimeDlg::IDD, popParent), omHeadline(ropHeadline)
{
	//{{AFX_DATA_INIT(CReportSeqDateTimeDlg)
	m_DateFrom = _T("");
	m_DateTo = _T("");
	m_TimeFrom = _T("");
	m_TimeTo = _T("");
	//}}AFX_DATA_INIT

 	pomMinDate = popMinDate;
	pomMaxDate = popMaxDate;
}


void CReportSeqDateTimeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CReportSeqDateTimeDlg)
	DDX_Control(pDX, IDC_DATUMVON, m_CE_DateFrom);
	DDX_Control(pDX, IDC_DATUMBIS, m_CE_DateTo);
	DDX_Control(pDX, IDC_ZEITVON, m_CE_TimeFrom);
	DDX_Control(pDX, IDC_ZEITBIS, m_CE_TimeTo);
 	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CReportSeqDateTimeDlg, CDialog)
	//{{AFX_MSG_MAP(CReportSeqDateTimeDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CReportSeqDateTimeDlg 

void CReportSeqDateTimeDlg::OnOK() 
{
	m_CE_DateFrom.GetWindowText(m_DateFrom);
	m_CE_DateTo.GetWindowText(m_DateTo);
	// if only one date given, it is copied to the other
 	if(m_DateFrom.IsEmpty())
		m_CE_DateTo.GetWindowText(m_DateFrom);
	if(m_DateTo.IsEmpty())
		m_CE_DateFrom.GetWindowText(m_DateTo);
	// set time fields
	m_CE_TimeFrom.GetWindowText(m_TimeFrom);
	if (m_TimeFrom.IsEmpty())
		m_TimeFrom="00:00";
	m_CE_TimeTo.GetWindowText(m_TimeTo);
	if (m_TimeTo.IsEmpty())
		m_TimeTo="23:59";

	
	if( (m_DateTo.IsEmpty()) && (m_DateFrom.IsEmpty()) )
	{
		// no date specified
		MessageBox(GetString(IDS_STRING918), GetString(IMFK_REPORT), MB_OK);
		return;
	}
 
	if(!m_CE_DateFrom.GetStatus() || !m_CE_DateTo.GetStatus() ||
		!m_CE_TimeFrom.GetStatus() || !m_CE_TimeTo.GetStatus())
	{
		// wrong date / time format
		MessageBox(GetString(IDS_STRING934), GetString(IMFK_REPORT), MB_OK);
		return;
	}

	*pomMinDate = DateHourStringToDate(m_DateFrom, m_TimeFrom);
	*pomMaxDate = DateHourStringToDate(m_DateTo, m_TimeTo); 


	if (*pomMinDate > *pomMaxDate)
	{
		// To-Time before From-Time
		MessageBox(GetString(IDS_STRING1642), GetString(IMFK_REPORT), MB_OK);
		return;
	}
 
	CDialog::OnOK();
 
}

void CReportSeqDateTimeDlg::OnCancel() 
{
 	
	CDialog::OnCancel();
}

BOOL CReportSeqDateTimeDlg::OnInitDialog() 
{

	CDialog::OnInitDialog();
	CDialog::SetWindowText(omHeadline);	

	m_CE_DateFrom.SetTypeToDate();
	m_CE_DateTo.SetTypeToDate();
	m_CE_TimeFrom.SetTypeToTime();
	m_CE_TimeTo.SetTypeToTime();

	m_CE_DateFrom.SetBKColor(YELLOW);
	m_CE_DateTo.SetBKColor(YELLOW);
	m_CE_TimeFrom.SetBKColor(YELLOW);
	m_CE_TimeTo.SetBKColor(YELLOW);

	m_DateFrom = pomMinDate->Format("%d.%m.%Y");
	m_CE_DateFrom.SetWindowText( m_DateFrom );

	m_TimeFrom = pomMinDate->Format("%H:%M");
	m_CE_TimeFrom.SetWindowText( m_TimeFrom );

	m_DateTo = pomMaxDate->Format("%d.%m.%Y" );
	m_CE_DateTo.SetWindowText( m_DateTo );

	m_TimeTo = pomMaxDate->Format("%H:%M");
	m_CE_TimeTo.SetWindowText( m_TimeTo );
 
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}


