// RotGroundDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <fpms.h>
#include <RotGroundDlg.h>
#include <CCSDdx.h>
#include <CCSTime.h>
#include <CCSGlobl.h>
#include <RotationDlgCedaFlightData.h>
#include <CedaBasicData.h>
#include <PrivList.h>
#include <Utils.h>
#include <RotationDlg.h>

#include <resrc1.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void FlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);



/*
static int CompareRotationFlight(const ROTGDLGFLIGHTDATA **e1, const ROTGDLGFLIGHTDATA **e2)
{
	if((**e1).Rkey != (**e2).Rkey)
		return 0;


	if( strcmp((**e1).Adid ,(**e2).Adid ) == 0)
	{
		return (((**e1).Tifa == (**e2).Tifa) ? 0 : (((**e1).Tifa > (**e2).Tifa) ? 1 : -1));
	}
	else
	{
		return ( strcmp((**e1).Adid , (**e2).Adid) );
	}
	
}
*/

int RotGroundDlg::CompareRotationFlight(const ROTGDLGFLIGHTDATA **e1, const ROTGDLGFLIGHTDATA **e2)
{

	if((**e2).Adid[0] == 'B' && (**e1).Adid[0] == 'B')
	{
		CTime olTime1;
		CTime olTime2;

		if((**e1).Tifa == TIMENULL)
			olTime1 = (**e1).Tifd;
		else
			olTime1 = (**e1).Tifa;

		if((**e2).Tifa == TIMENULL)
			olTime2 = (**e2).Tifd;
		else
			olTime2 = (**e2).Tifa;

		return ((olTime1 == olTime2) ? 0 : ((olTime1 > olTime2) ? 1 : -1));
	}
	else
	{
		return strcmp((**e1).Adid, (**e2).Adid);

	}

}






/////////////////////////////////////////////////////////////////////////////
// Dialogfeld RotGroundDlg 


RotGroundDlg::RotGroundDlg(CWnd* pParent /*=NULL*/)
	: CDialog(RotGroundDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(RotGroundDlg)
	m_AFlno = _T("");
	m_AOnbl = _T("");
	m_AStoa = _T("");
	m_DEtdi = _T("");
	m_DOfbl = _T("");
	m_DPstd = _T("");
	m_DStod = _T("");
	m_DFlno = _T("");
	m_APsta = _T("");
	m_AEtai = _T("");
	//}}AFX_DATA_INIT

	//lmCallByUrno = lpCallByUrno;

    //ogDdx.Register(this, R_DLG_ROTATION_CHANGE, CString("FLIGHTDLG"), CString("Flight Update/new"), FlightTableCf);

	pomParent = pParent;

    pomTable = new CCSTable;

	prmDFlight = new ROTGDLGFLIGHTDATA;
	prmAFlight = new ROTGDLGFLIGHTDATA;

	omRefDat = TIMENULL;
	emStatus = UNKNOWN;
}



void RotGroundDlg::NewData(CWnd* pParent, long lpRkey, long lpUrno, char cpFPart, bool bpLocalTime, bool bpAuto, bool bpOk /* true */)
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	
	ClearAll();

	if(lpRkey == 0)
	{
		if (lpUrno > 0)
			lpRkey = omRotGDlgFlights.GetRkey(lpUrno);
	}

	bmLocalTime = bpLocalTime;
	// set caption
	CString olTimes;
	if (bmLocalTime)
		olTimes = GetString(IDS_STRING1921);
	else
		olTimes = GetString(IDS_STRING1920);
	CString olCaption = GetString(IDS_STRING1975);
	olCaption += " ("+olTimes+")";
	SetWindowText(olCaption);


	pomParent = pParent;
	lmCallByUrno = lpUrno;
	cmCallFPart = cpFPart;
	lmCallByRkey = lpRkey;
	
    ogDdx.Register(this, RG_DLG_FLIGHT_CHANGE, CString("FLIGHTDLG"), CString("Flight Update/new"), FlightTableCf);
    ogDdx.Register(this, RG_DLG_ROTATION_CHANGE, CString("FLIGHTDLG"), CString("Flight Update/new"), FlightTableCf);
	
	
	omRotGDlgFlights.ReadRotation( lpRkey, lpUrno, cpFPart, true);

	if (bpAuto)
		OnNewTowing();

	if (!bpOk)
	{
		m_CB_OK.EnableWindow(false);
		m_CB_Delete.EnableWindow(false);
	}

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	//SetWndPos();

	//postflight checking
	CheckPostFlight();
}






RotGroundDlg::~RotGroundDlg()
{
	omRotGDlgFlights.ClearAll();

	ogDdx.UnRegister(this,NOTUSED);

	delete pomTable;
	delete prmDFlight;
	delete prmAFlight;
	omMovements.DeleteAll();
	omMovementSave.DeleteAll();

}



void RotGroundDlg::ClearAll()
{
	ROTGDLGFLIGHTDATA *prlFlight = new ROTGDLGFLIGHTDATA;

	ogDdx.UnRegister(this,NOTUSED);

	*prmAFlight = *prlFlight;  
	*prmDFlight = *prlFlight;  

	delete prlFlight;
	
	pomTable->ResetContent();

	omMovements.DeleteAll();
	omMovementSave.DeleteAll();
}


void RotGroundDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(RotGroundDlg)
	DDX_Control(pDX, IDOK, m_CB_OK);
	DDX_Control(pDX, IDC_DELETE, m_CB_Delete);
	DDX_Control(pDX, IDC_REGN, m_CE_Regn);
	DDX_Control(pDX, IDC_ACT5, m_CE_Act5);
	DDX_Control(pDX, IDC_ACT3, m_CE_Act3);
	DDX_Control(pDX, IDC_DPSTD, m_CE_DPstd);
	DDX_Control(pDX, IDC_AFLNO, m_CE_AFlno);
	DDX_Control(pDX, IDC_GMBORDER, m_CS_GMBorder);
	DDX_Control(pDX, IDC_AETAI, m_CE_AEtai);
	DDX_Control(pDX, IDC_APSTA, m_CE_APsta);
	DDX_Control(pDX, IDC_DFLNO, m_CE_DFlno);
	DDX_Control(pDX, IDC_DSTOD, m_CE_DStod);
	DDX_Control(pDX, IDC_DOFBL, m_CE_DOfbl);
	DDX_Control(pDX, IDC_DETDI, m_CE_DEtdi);
	DDX_Control(pDX, IDC_ASTOA, m_CE_AStoa);
	DDX_Control(pDX, IDC_AONBL, m_CE_AOnbl);
	DDX_Text(pDX, IDC_AFLNO, m_AFlno);
	DDX_Text(pDX, IDC_AONBL, m_AOnbl);
	DDX_Text(pDX, IDC_ASTOA, m_AStoa);
	DDX_Text(pDX, IDC_DETDI, m_DEtdi);
	DDX_Text(pDX, IDC_DOFBL, m_DOfbl);
	DDX_Text(pDX, IDC_DPSTD, m_DPstd);
	DDX_Text(pDX, IDC_DSTOD, m_DStod);
	DDX_Text(pDX, IDC_DFLNO, m_DFlno);
	DDX_Text(pDX, IDC_APSTA, m_APsta);
	DDX_Text(pDX, IDC_AETAI, m_AEtai);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(RotGroundDlg, CDialog)
	//{{AFX_MSG_MAP(RotGroundDlg)
	ON_MESSAGE(WM_TABLE_IPEDIT_KILLFOCUS, OnTableIPEditKillfocus)
    ON_MESSAGE(WM_TABLE_LBUTTONDOWN, OnTableLButtonDown)
	ON_BN_CLICKED(IDC_DELETE, OnDelete)
	ON_BN_CLICKED(IDC_NEW_TOWING, OnNewTowing)
	ON_BN_CLICKED(IDC_FLIGHT, OnFlight)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



static void FlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    RotGroundDlg *polDlg = (RotGroundDlg *)popInstance;

    if(ipDDXType == RG_DLG_ROTATION_CHANGE)
        polDlg->InitDialog((CCSPtrArray<ROTGDLGFLIGHTDATA> *)vpDataPointer);

    if(ipDDXType == RG_DLG_FLIGHT_CHANGE)
        polDlg->ProcessFlightChange((ROTGDLGFLIGHTDATA *)vpDataPointer);

}





/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten RotGroundDlg 

BOOL RotGroundDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	bmIPEdit = true;	

	SetWndStatAll(ogPrivList.GetStat("TOWING_CB_OK"),m_CB_OK);
	SetWndStatAll(ogPrivList.GetStat("TOWING_CB_DELETE"),m_CB_Delete);

	if(ogPrivList.GetStat("TOWING_CB_Edit") == '-')
		bmIPEdit = false;
	


	InitTable();
	InitialStatus();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}



void RotGroundDlg::InitDialog(CCSPtrArray<ROTGDLGFLIGHTDATA> *prpRotation)
{
	ClearAll();

    ogDdx.Register(this, RG_DLG_FLIGHT_CHANGE, CString("FLIGHTDLG"), CString("Flight Update/new"), FlightTableCf);
    ogDdx.Register(this, RG_DLG_ROTATION_CHANGE, CString("FLIGHTDLG"), CString("Flight Update/new"), FlightTableCf);


	ROTGDLGFLIGHTDATA *prlFlight;

	prpRotation->Sort(RotGroundDlg::CompareRotationFlight);
	int ilCount = prpRotation->GetSize();
	int i = 0;


	// look for arrival or departure or selected towing!
	int ilFlightPos = -1;
	for(i = 0; i < ilCount; i++)
	{
		prlFlight = &(*prpRotation)[i];
		if (prlFlight->Urno == lmCallByUrno)
		{
			if (CString("GT").Find(prlFlight->Ftyp) >= 0)
			{
				cmCallFPart = ' '; // Given urno is a towing!!!!
			}
			else if (cmCallFPart == 'A')
			{
				*prmAFlight = *prlFlight; // arrival flight found!
			}
			else if (cmCallFPart == 'D')
			{
				*prmDFlight = *prlFlight; // departure flight found!
			}
			ilFlightPos = i;
			break;
		}
	}

	if (cmCallFPart == 'A')
	{
		// search all towings and next dep!
		for(i = ilFlightPos + 1; i < ilCount; i++)
		{
			prlFlight = &(*prpRotation)[i];

			// collect towings!
			if(  CString("GT").Find(prlFlight->Ftyp) >= 0   )
			{
				omMovementSave.New((*prpRotation)[i]);
				omMovements.New((*prpRotation)[i]);
			}

			// look for next departure
			if( IsDepartureFlight(prlFlight->Org3, prlFlight->Des3, prlFlight->Ftyp[0]) && 
				prlFlight->Urno != prmAFlight->Urno)
			{
				*prmDFlight = *prlFlight; // arrival flight found!
				break;
			}
		}
	}
	else if (cmCallFPart == 'D')
	{
		// search all towings and prev arr!
		for(i = ilFlightPos - 1; i >= 0; i--)
		{
			prlFlight = &(*prpRotation)[i];

			// collect towings!
			if(  CString("GT").Find(prlFlight->Ftyp) >= 0   )
			{
				omMovementSave.NewAt(0, (*prpRotation)[i]);
				omMovements.NewAt(0, (*prpRotation)[i]);
			}

			// look for prev arrival
			if( IsArrivalFlight(prlFlight->Org3, prlFlight->Des3, prlFlight->Ftyp[0]) && 
				prlFlight->Urno != prmAFlight->Urno)
			{
				*prmAFlight = *prlFlight; // arrival flight found!
				break;
			}
		}
	}
	else
	{
		
		// search dep
		int ilDepPos = ilCount;
		for(i = ilFlightPos + 1; i < ilCount; i++)
		{
			prlFlight = &(*prpRotation)[i];
			// look for next departure
			if( IsDepartureFlight(prlFlight->Org3, prlFlight->Des3, prlFlight->Ftyp[0]) && 
				prlFlight->Urno != prmAFlight->Urno)
			{
				*prmDFlight = *prlFlight; // arrival flight found!
				ilDepPos = i;
				break;
			}
		}

		// search arr!
		int ilArrPos = 0;
		for(i = ilFlightPos - 1; i >= 0; i--)
		{
			prlFlight = &(*prpRotation)[i];

			// look for prev arrival
			if( IsArrivalFlight(prlFlight->Org3, prlFlight->Des3, prlFlight->Ftyp[0]) && 
				prlFlight->Urno != prmAFlight->Urno)
			{
				*prmAFlight = *prlFlight; // arrival flight found!
				ilArrPos = i;
				break;
			}
		}

		// collect towings
		for (int i = ilArrPos + 1; i < ilDepPos && i < ilCount; i++)
		{
			prlFlight = &(*prpRotation)[i];
			if(  CString("GT").Find(prlFlight->Ftyp) >= 0   )
			{
				omMovementSave.New((*prpRotation)[i]);
				omMovements.New((*prpRotation)[i]);
			}
		}
	}
	

 
	if (prmAFlight->Urno > 0)
	{
		omRefDat = prmAFlight->Stoa;

		omAct3 = CString(prmAFlight->Act3);
		omAct5 = CString(prmAFlight->Act5);
		omRegn = CString(prmAFlight->Regn);
	}
	else if (prmDFlight->Urno > 0)
	{
		omRefDat = prmDFlight->Stod;

		omAct3 = CString(prmDFlight->Act3);
		omAct5 = CString(prmDFlight->Act5);
		omRegn = CString(prmDFlight->Regn);
	}


	if(omRefDat == TIMENULL)
	{
		for(i = 0; i < ilCount; i++)
		{
			prlFlight = &(*prpRotation)[i];
			if(prlFlight->Stod != TIMENULL)
			{
				omRefDat = prlFlight->Stod;
			}

			if(omAct3.IsEmpty() && omAct5.IsEmpty())
			{
				omAct3 = CString(prlFlight->Act3);
				omAct5 = CString(prlFlight->Act5);
			}

			if(omRegn.IsEmpty())
			{
				omRegn = CString(prlFlight->Regn);
			}

		}
	}


	ilCount = prpRotation->GetSize();
	

	CTime olTimeTmp;

	m_CE_AFlno.SetWindowText(prmAFlight->Flno);

	olTimeTmp=prmAFlight->Stoa;
	if (bmLocalTime) ogBasicData.UtcToLocal(olTimeTmp);
	m_CE_AStoa.SetWindowText(olTimeTmp.Format("%H:%M     %d.%m.%Y"));

	olTimeTmp=prmAFlight->Etai;
	if (bmLocalTime) ogBasicData.UtcToLocal(olTimeTmp);
	m_CE_AEtai.SetWindowText(olTimeTmp.Format("%H:%M"));

	olTimeTmp=prmAFlight->Onbl;
	if (bmLocalTime) ogBasicData.UtcToLocal(olTimeTmp);
	m_CE_AOnbl.SetWindowText(olTimeTmp.Format("%H:%M"));
	m_CE_APsta.SetWindowText(prmAFlight->Psta);

	m_CE_DFlno.SetWindowText(prmDFlight->Flno);

	olTimeTmp=prmDFlight->Stod;
	if (bmLocalTime) ogBasicData.UtcToLocal(olTimeTmp);
	m_CE_DStod.SetWindowText(olTimeTmp.Format("%H:%M     %d.%m.%Y"));

	olTimeTmp=prmDFlight->Etdi;
	if (bmLocalTime) ogBasicData.UtcToLocal(olTimeTmp);
	m_CE_DEtdi.SetWindowText(olTimeTmp.Format("%H:%M"));

	olTimeTmp=prmDFlight->Ofbl;
	if (bmLocalTime) ogBasicData.UtcToLocal(olTimeTmp);	
	m_CE_DOfbl.SetWindowText(olTimeTmp.Format("%H:%M"));

	m_CE_DPstd.SetWindowText(prmDFlight->Pstd);

	m_CE_Regn.SetWindowText(omRegn);
	m_CE_Act3.SetWindowText(omAct3);
	m_CE_Act5.SetWindowText(omAct5);


	ilCount = omMovements.GetSize();

	ROTGDLGFLIGHTDATA rlFlight;

	for(i = ilCount; i < 10; i++)
	{
			omMovementSave.New(rlFlight);
			omMovements.New(rlFlight);
	}

	InsertTableHeadline();

	ilCount = omMovements.GetSize();

	for(i = 0; i < ilCount; i++)
	{
		FillTableLine(i+1, &omMovements[i]);
	}

}


void RotGroundDlg::FillTableLine(int ipLineNo, ROTGDLGFLIGHTDATA *prpFlight)
{

	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;

	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogCourier_Regular_9;


	CCSEDIT_ATTRIB rlAttrib;
	rlAttrib.Style = ES_UPPERCASE;
	rlAttrib.ChangeDay	= true;

	rlColumnData.blIsEditable = false;	
	rlColumnData.Alignment = COLALIGN_CENTER;
	char buffer[64];
	itoa(ipLineNo, buffer, 10);
	rlColumnData.Text =	CString(buffer);
	rlColumnData.BkColor = RGB(192,192,192);
	olColList.NewAt(olColList.GetSize(), rlColumnData);


	rlColumnData.BkColor = RGB(255,255,255);
	rlAttrib.Type = KT_STRING;
	rlAttrib.Format = "'G'|'T'";
	rlAttrib.TextMinLenght = 1;
	rlAttrib.TextMaxLenght = 1;
	rlColumnData.EditAttrib = rlAttrib;
	rlColumnData.blIsEditable = bmIPEdit;	
	rlColumnData.Alignment = COLALIGN_CENTER;
	rlColumnData.Text =	CString(prpFlight->Ftyp);
	olColList.NewAt(olColList.GetSize(), rlColumnData);


	rlAttrib.Type = KT_STRING;
	rlAttrib.Format = "XXXXX";
	rlAttrib.TextMinLenght = 0;
	rlAttrib.TextMaxLenght = 5;
	rlColumnData.EditAttrib = rlAttrib;
	rlColumnData.blIsEditable = bmIPEdit;	
	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = CString(prpFlight->Pstd);
	olColList.NewAt(olColList.GetSize(), rlColumnData);



	rlAttrib.Type = KT_DATE;
	rlAttrib.TextMinLenght = 6;
	rlAttrib.TextMaxLenght = 12;
	rlColumnData.EditAttrib = rlAttrib;
	rlColumnData.blIsEditable = bmIPEdit;	
	rlColumnData.Alignment = COLALIGN_LEFT;

	CTime olTimeTmp;
	olTimeTmp=prpFlight->Stod;
	if (bmLocalTime) ogBasicData.UtcToLocal(olTimeTmp);
	rlColumnData.Text =    olTimeTmp.Format("%d.%m.%y");  
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlAttrib.Type = KT_TIME;
	rlAttrib.TextMinLenght = 0;
	rlAttrib.TextMaxLenght = 7;
	rlColumnData.EditAttrib = rlAttrib;
	rlColumnData.blIsEditable = bmIPEdit;	
	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text =    olTimeTmp.Format("%H:%M");  
	olColList.NewAt(olColList.GetSize(), rlColumnData);



	rlAttrib.Type = KT_TIME;
	rlAttrib.TextMinLenght = 0;
	rlAttrib.TextMaxLenght = 7;
	rlColumnData.EditAttrib = rlAttrib;
	rlColumnData.blIsEditable = bmIPEdit;	
	rlColumnData.Alignment = COLALIGN_LEFT;

	CTime olTimeTmp2;
	olTimeTmp = prpFlight->Ofbl;
	olTimeTmp2 = prpFlight->Stod;
	if (bmLocalTime)
	{
		ogBasicData.UtcToLocal(olTimeTmp);
		ogBasicData.UtcToLocal(olTimeTmp2);
	}
	rlColumnData.Text =    DateToHourDivString(olTimeTmp, olTimeTmp2);  
	olColList.NewAt(olColList.GetSize(), rlColumnData);


	rlAttrib.Type = KT_STRING;
	rlAttrib.Format = "XXXXX";
	rlAttrib.TextMinLenght = 0;
	rlAttrib.TextMaxLenght = 5;
	rlColumnData.EditAttrib = rlAttrib;
	rlColumnData.blIsEditable = bmIPEdit;	
	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = CString(prpFlight->Psta);
	olColList.NewAt(olColList.GetSize(), rlColumnData);


	rlAttrib.Type = KT_TIME;
	rlAttrib.TextMinLenght = 0;
	rlAttrib.TextMaxLenght = 7;
	rlColumnData.EditAttrib = rlAttrib;
	rlColumnData.blIsEditable = bmIPEdit;	
	rlColumnData.Alignment = COLALIGN_LEFT;
	olTimeTmp = prpFlight->Stoa;
	olTimeTmp2 = prpFlight->Stod;
	if (bmLocalTime)
	{
		ogBasicData.UtcToLocal(olTimeTmp);
		ogBasicData.UtcToLocal(olTimeTmp2);
	}
	rlColumnData.Text =    DateToHourDivString(olTimeTmp, olTimeTmp2);
	olColList.NewAt(olColList.GetSize(), rlColumnData);


	rlAttrib.Type = KT_TIME;
	rlAttrib.TextMinLenght = 0;
	rlAttrib.TextMaxLenght = 7;
	rlColumnData.EditAttrib = rlAttrib;
	rlColumnData.blIsEditable = bmIPEdit;	
	rlColumnData.Alignment = COLALIGN_LEFT;
	olTimeTmp = prpFlight->Onbl;
	olTimeTmp2 = prpFlight->Stod;
	if (bmLocalTime)
	{
		ogBasicData.UtcToLocal(olTimeTmp);
		ogBasicData.UtcToLocal(olTimeTmp2);
	}
	rlColumnData.Text =    DateToHourDivString(olTimeTmp, olTimeTmp2);
	olColList.NewAt(olColList.GetSize(), rlColumnData);

		
	rlAttrib.Type = KT_STRING;
	rlAttrib.Format = "X(128)";
	rlAttrib.TextMinLenght = 0;
	rlAttrib.TextMaxLenght = 128;
	rlColumnData.EditAttrib = rlAttrib;
	rlColumnData.blIsEditable = bmIPEdit;	
	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = CString(prpFlight->Rem1);
	olColList.NewAt(olColList.GetSize(), rlColumnData);
		
	
	rlColumnData.blIsEditable = false;	
	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = CString(prpFlight->Useu);
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.blIsEditable = false;	
	rlColumnData.Alignment = COLALIGN_LEFT;
	olTimeTmp = prpFlight->Lstu;
	if (bmLocalTime) ogBasicData.UtcToLocal(olTimeTmp);
	rlColumnData.Text = olTimeTmp.Format("%H:%M %d.%m.%y");
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.blIsEditable = false;	
	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = CString(prpFlight->Usec);
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.blIsEditable = false;	
	rlColumnData.Alignment = COLALIGN_LEFT;
	olTimeTmp = prpFlight->Cdat;
	if (bmLocalTime) ogBasicData.UtcToLocal(olTimeTmp);
	rlColumnData.Text = olTimeTmp.Format("%H:%M %d.%m.%y");
	olColList.NewAt(olColList.GetSize(), rlColumnData);


	if(ipLineNo < pomTable->GetLinesCount())
		pomTable->ChangeTextLine(ipLineNo, &olColList, NULL);
	else
		pomTable->AddTextLine(olColList, NULL);

	olColList.DeleteAll();
	pomTable->DisplayTable();

}




void RotGroundDlg::InitTable()
{
	CRect olRectBorder;
	m_CS_GMBorder.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);

	pomTable->SetHeaderSpacing(0);
	pomTable->SetMiniTable();
	pomTable->SetTableEditable(true);

	//rkr04042001
	pomTable->SetIPEditModus(true);

    pomTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	//pomTable->SetSelectMode(0);
	pomTable->SetSelectMode(LBS_MULTIPLESEL);

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);
	pomTable->ResetContent();
	
	TABLE_HEADER_COLUMN rlHeader;

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Font = &ogCourier_Regular_10;

	// LineNo
	rlHeader.Length = 20; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	// FTyp
	rlHeader.Length = 15; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	// PSTD
	rlHeader.Length = 40; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	// STD Date
	rlHeader.Length = 60; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	// STD Time	
	rlHeader.Length = 60; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	// Ofbl time
	rlHeader.Length = 50; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	//Psta
	rlHeader.Length = 40; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	// STA Time
	rlHeader.Length = 40; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	// Onbl Time
	rlHeader.Length = 40; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	
	rlHeader.Length = 200; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);


	// Useu
	rlHeader.Length = 80; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	// Lstu
	rlHeader.Length = 100; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	// Usec
	rlHeader.Length = 80; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	// Cdat
	rlHeader.Length = 100; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);


	pomTable->SetHeaderFields(omHeaderDataArray);
	pomTable->SetDefaultSeparator();
	pomTable->SetTableEditable(true);
	pomTable->DisplayTable();

	omHeaderDataArray.DeleteAll();


}



bool RotGroundDlg::InsertTableHeadline()
{
	// Insert header as normal table line

 	TABLE_COLUMN rlColumnData;
	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogCourier_Regular_9;
 	rlColumnData.Alignment = COLALIGN_LEFT;

 	CCSPtrArray<TABLE_COLUMN> olColList;

 	rlColumnData.blIsEditable = false;	
	rlColumnData.BkColor = RGB(192,192,192);

	rlColumnData.Text = "";
	olColList.NewAt(olColList.GetSize(), rlColumnData);

 	rlColumnData.Text = GetString(IDS_STRING1001);// T
 	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Text = GetString(IDS_STRING1924);//Pos-D
 	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Text = GetString(IDS_STRING1926);//STD Date
 	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Text = GetString(IDS_STRING1927);//STD Time
 	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Text = GetString(IDS_STRING319);//OFB
 	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Text = GetString(IDS_STRING1925);//Pos-A
 	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Text = GetString(IDS_STRING323);//STA
 	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Text = GetString(IDS_STRING307);//ONB
 	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Text = GetString(IDS_STRING792);//Remark
 	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Text = GetString(IDS_STRING817);//Updated by
 	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Text = GetString(IDS_STRING783);//Last update
 	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Text = GetString(IDS_STRING816);//Created by
 	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Text = GetString(IDS_STRING748);//Created on
 	olColList.NewAt(olColList.GetSize(), rlColumnData);


	pomTable->AddTextLine(olColList, (void*)(NULL));
	olColList.DeleteAll();


	pomTable->DisplayTable();

	return true;
}


/*
BOOL RotGroundDlg::DestroyWindow() 
{
	return CWnd::DestroyWindow();
}
*/

void RotGroundDlg::OnCancel() 
{

	//DestroyWindow();
	//pogRotGroundDlg = NULL;
	//CDialog::OnCancel();
	omRotGDlgFlights.ClearAll();
	ShowWindow(SW_HIDE);
}


void RotGroundDlg::OnOK() 
{
	int ilLine;
	int ilColumn;

	char pclTmp[256];

	CString olText;

	if(!CheckAll(ilLine,ilColumn))
	{

		sprintf(pclTmp, GetString(IDS_STRING1667) , ilLine);

		olText = CString(pclTmp);


		switch( ilColumn)
		{

		case 1: olText += GetString( IDS_STRING1668 ); // Falscher Typ: T = Towing  G = Groundmovement
			break;

		case 2: olText += GetString( IDS_STRING1669 ); // Position Departure
			break;
		case 3: olText += GetString( IDS_STRING1670 ); // Date STD
			break;
		case 4: olText += GetString( IDS_STRING1671 ); // Time STD
			break;
		case 5: olText += GetString( IDS_STRING1672 ); // Time Offblock
			break;
		case 6: olText += GetString( IDS_STRING1673 ); // Position Arrival
			break;
		case 7: olText += GetString( IDS_STRING1674 ); // Time STA
			break;
		case 8: olText += GetString( IDS_STRING1675 ); // Time Onblock
			break;
		case 9: olText += GetString( IDS_STRING1676 ); // Remark
			break;

		}


		MessageBox( olText , GetString(ST_FEHLER), MB_ICONWARNING);
		return;
	}

	GetAllData();

	if (VerifyRotation())
		SaveAll();
	else
		return;

}






LONG RotGroundDlg::OnTableIPEditKillfocus( UINT wParam, LPARAM lParam)
{
	CCSIPEDITNOTIFY *prlNotify = (CCSIPEDITNOTIFY*)lParam;
	bool	blRet = true;

	CString olTmp;
	int ilLine = prlNotify->Line;

	switch (prlNotify->Column)
	{
		case 2://PSTD
			{
				blRet = ogBCD.GetField("PST", "PNAM", prlNotify->Text, "URNO", olTmp );
			}
			break;
		case 6://PSTA
			{
				blRet = ogBCD.GetField("PST", "PNAM", prlNotify->Text, "URNO", olTmp );
			}
			break;
		default:
			break;
	}

	prlNotify->UserStatus = true;
	prlNotify->Status = blRet;
	return 0L;
}



LONG RotGroundDlg::OnTableLButtonDown(UINT wParam, LONG lParam)
{
	CCSTABLENOTIFY *prlNotify = (CCSTABLENOTIFY*)lParam;
 	pomTable->SelectLine(prlNotify->Line);
 	return 0L;
}



bool RotGroundDlg::CheckAll(int &ipFirstLine, int &ipFirstColumn)
{
	CString olTmp;
	int ilLines = pomTable->GetLinesCount();

	bool blInput = false;

	for( int i = 1; i < ilLines; i++)
	{
		blInput = false;

		for( int j = 1; j < 8; j++)
		{
			olTmp.Empty();
			pomTable->GetTextFieldValue(i, j, olTmp);
			
			
			if(!olTmp.IsEmpty())
				blInput = true;

		}

		for(  j = 1; j < 8; j++)
		{

			if(( j == 2 || j == 4 || j == 6 || j == 7) &&  olTmp.IsEmpty() && blInput)
			{
				ipFirstLine = i;
				ipFirstColumn = j;
				return false;
			}


		}

		for(  j = 1; j < 8; j++)
		{
			/*
			if(j == 1)
			{
					pomTable->GetTextFieldValue(i, j, olFtyp);
					if


			}
			*/


			if(!pomTable->GetCellStatus(i, j) && blInput)
			{
				ipFirstLine = i;
				ipFirstColumn = j;
				return false;
			}
		}
	}
	return true;
}



bool RotGroundDlg::SaveAll()
{

	bool blUpdateDepPos = false;
	// Shall the departure position be updated?
	CString olLastPos;
	if (GetLastPos(olLastPos))
	{
		// Is last position not the departure position?
		if (!olLastPos.IsEmpty() && olLastPos != prmDFlight->Pstd)
		{
			if (MessageBox(GetString(IDS_STRING1973), GetString(ST_FRAGE), MB_ICONQUESTION | MB_YESNO) == IDYES)
			{
				blUpdateDepPos = true;
			}
		}
	}



	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));	

	bool blRet = true;

	ROTGDLGFLIGHTDATA *prlFlight;
	ROTGDLGFLIGHTDATA *prlFlightSave;

	int ilCount = omMovements.GetSize();

	for(int i = 0; i < ilCount; i++)
	{
		prlFlight = &omMovements[i];
		prlFlightSave = &omMovementSave[i];

		if((prlFlight->Stoa != TIMENULL) && (prlFlight->Stod != TIMENULL))
		{


			if(prlFlight->Urno > 0)
			{
				if(prlFlight->Urno == prlFlightSave->Urno)
				{
					// update record
					omRotGDlgFlights.UpdateFlight(prlFlight, prlFlightSave);
				}

			}
			else
			{
				// insert a new record
				char pclDay[64];			

				GetDayOfWeek(prlFlight->Stoa, pclDay);
				
				long llJoinUrno;

				if(prmAFlight->Urno != 0)
					llJoinUrno = prmAFlight->Urno;
				else
					llJoinUrno = prmDFlight->Urno;

				omRotGDlgFlights.AddSeasonFlights(prlFlight->Stoa, prlFlight->Stoa, CString(pclDay) , "", "1", TIMENULL, TIMENULL, prlFlight, NULL, llJoinUrno);
			}
		}
	}


	// Update departure position if necessary
	if (blUpdateDepPos)
	{
		ROTGDLGFLIGHTDATA rlFlightDSave = *prmDFlight;
		strncpy(prmDFlight->Pstd, olLastPos, 7);
 		omRotGDlgFlights.UpdateFlight(prmDFlight, &rlFlightDSave);
	}


	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	
	return blRet;
}


// get the last arrival position of the rotation
bool RotGroundDlg::GetLastPos(CString &ropLastPos)
{
	// only if a departure flight exists
	if (prmDFlight->Urno == 0) 
		return false;

	ropLastPos.Empty();

	ROTGDLGFLIGHTDATA *prlFlight;
	CTime olLastPosTime(0);

	int ilCount = omMovements.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		prlFlight = &omMovements[i];

		if((prlFlight->Stoa != TIMENULL) && (prlFlight->Stod != TIMENULL))
		{
			if (olLastPosTime < prlFlight->Stoa)
			{
				ropLastPos = prlFlight->Psta;
				olLastPosTime = prlFlight->Stoa;
			}
		}

	}

	// No Towings present
	if (ropLastPos.IsEmpty())
	{
		// use arrival flight instead
		if (prmAFlight->Urno > 0)
		{
			ropLastPos = prmAFlight->Psta;
		}
	}

	if (ropLastPos.IsEmpty())
	{
		return false;
	}

	return true;
}


bool RotGroundDlg::GetAllData()
{
	bool blRet = true;

	ROTGDLGFLIGHTDATA *prlFlight;

	int ilLines = pomTable->GetLinesCount();

	CString olPsta;
	CString olPstd;
	CString olRem1;
	CString olFtyp;

	CTime olOnbl;
	CTime olOfbl;
	CTime olStoa;
	CTime olStod;

	CString olTmp;
	CString olTmp2;

	
	for( int i = 1; i < ilLines; i++)
	{

		pomTable->GetTextFieldValue(i, 1, olFtyp);

		if(olFtyp.IsEmpty())
			olFtyp = "T";

		pomTable->GetTextFieldValue(i, 2, olPstd);

		pomTable->GetTextFieldValue(i, 3, olTmp2);
		pomTable->GetTextFieldValue(i, 4, olTmp);

		olStod = DateStringToDate(olTmp2);

		if(olStod == TIMENULL)
			olStod = omRefDat;
		olStod = HourStringToDate(olTmp, olStod);

		pomTable->GetTextFieldValue(i, 5, olTmp);
		olOfbl = HourStringToDate(olTmp, olStod);
		if (bmLocalTime) ogBasicData.LocalToUtc(olOfbl);

		pomTable->GetTextFieldValue(i, 6, olPsta);

		pomTable->GetTextFieldValue(i, 7, olTmp);
		olStoa = HourStringToDate(olTmp, olStod);
		if (bmLocalTime) ogBasicData.LocalToUtc(olStoa);

		pomTable->GetTextFieldValue(i, 8, olTmp);
		olOnbl = HourStringToDate(olTmp, olStod); 
		if (bmLocalTime) ogBasicData.LocalToUtc(olOnbl);
		if (bmLocalTime) ogBasicData.LocalToUtc(olStod);

		pomTable->GetTextFieldValue(i, 9, olRem1);

		prlFlight = &omMovements[i-1];

		strcpy(prlFlight->Ftyp, olFtyp);
		strcpy(prlFlight->Pstd, olPstd);
		prlFlight->Stod = olStod;
		prlFlight->Ofbu = olOfbl;
		strcpy(prlFlight->Psta, olPsta);
		prlFlight->Stoa = olStoa;
		prlFlight->Onbu = olOnbl;
		strcpy(prlFlight->Rem1, olRem1);
		
		if(prmAFlight->Urno > 0)
		{
			strcpy(prlFlight->Org3, pcgHome);
			strcpy(prlFlight->Org4, pcgHome4);
			strcpy(prlFlight->Des3, pcgHome);
			strcpy(prlFlight->Des4, pcgHome4);

			strcpy(prlFlight->Alc2, prmAFlight->Alc2);
			strcpy(prlFlight->Alc3, prmAFlight->Alc3);
			strcpy(prlFlight->Fltn, prmAFlight->Fltn);
			strcpy(prlFlight->Flns, prmAFlight->Flns);
			strcpy(prlFlight->Flno, prmAFlight->Flno);
			strcpy(prlFlight->Act3, prmAFlight->Act3);
			strcpy(prlFlight->Act5, prmAFlight->Act5);
			strcpy(prlFlight->Regn, prmAFlight->Regn);


		}
		else
		{
			strcpy(prlFlight->Org3, pcgHome);
			strcpy(prlFlight->Org4, pcgHome4);
			strcpy(prlFlight->Des3, pcgHome);
			strcpy(prlFlight->Des4, pcgHome4);

			strcpy(prlFlight->Alc2, prmDFlight->Alc2);
			strcpy(prlFlight->Alc3, prmDFlight->Alc3);
			strcpy(prlFlight->Fltn, prmDFlight->Fltn);
			strcpy(prlFlight->Flns, prmDFlight->Flns);
			strcpy(prlFlight->Flno, prmDFlight->Flno);
			strcpy(prlFlight->Act3, prmDFlight->Act3);
			strcpy(prlFlight->Act5, prmDFlight->Act5);
			strcpy(prlFlight->Regn, prmDFlight->Regn);

		}

	}

	//omMovements.Sort(CompareRotationFlight);

	return blRet;

}

bool RotGroundDlg::VerifyRotation()
{
//	omMovements.Sort(CompareRotationFlight); //faild because always 10 records with TIMENULL, they are the first after sort

	//always 10 records (why?), find the real ones (they have the stod filled!)
	int il = omMovements.GetSize();
	int ilCount = 0;
	for(int k = 0; k < il; k++)
	{
		ROTGDLGFLIGHTDATA *prlMovement = NULL;
		prlMovement = &omMovements[k];
		if (prlMovement->Stod != TIMENULL)
			ilCount++;
	}

	CString olCR ("\n");
	CString olMessage("");
//	ilCount = omMovements.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		char buffer[64];
		itoa(i+1, buffer, 10);
		CString olMovementNr = CString(buffer);

		CTime opTifd = TIMENULL;
		CTime opTifa = TIMENULL;
		CString opDepTimeError ("");
		CString opArrTimeError ("");
		ROTGDLGFLIGHTDATA *prlMovement = NULL;
		prlMovement = &omMovements[i];
		if (prlMovement->Stod == TIMENULL)
			continue;

		VerifyMovement(prlMovement, opTifd, opTifa, opDepTimeError, opArrTimeError);

		//check Arrival with first movement
		if (i==0 && prmAFlight && prlMovement)
		{
			if(prmAFlight->Tifa > opTifd)
			{
				olMessage += GetString(IDS_TW_ARR) + GetString(IDS_TW_TIFA) + CString(">\t");
				olMessage += olMovementNr + GetString(IDS_TW_TOW) + GetString(IDS_TW_TIFD) + olCR;
//				olMessage += "Arr-Tifa > MoveFirst-Tifd\n";
			}
			if (strcmp(prmAFlight->Psta, prlMovement->Pstd) != 0)
			{
				olMessage += GetString(IDS_TW_ARR) + GetString(IDS_TW_POSA) + CString("!\t");
				olMessage += olMovementNr + GetString(IDS_TW_TOW) + GetString(IDS_TW_POSD) + olCR;
//				olMessage += "Arr-Posa != MoveFirst-Posd\n";
			}
		}

		if (prlMovement)
		{
			//check movement internal
			if (!opDepTimeError.IsEmpty())
			{
				olMessage += olMovementNr + GetString(IDS_TW_TOW) + GetString(IDS_TW_OFBL) + CString("<\t");
				olMessage += olMovementNr + GetString(IDS_TW_TOW) + GetString(IDS_TW_STOD) + olCR;
			}

			if (!opArrTimeError.IsEmpty())
			{
				olMessage += olMovementNr + GetString(IDS_TW_TOW) + GetString(IDS_TW_ONBL) + CString("<\t");
				olMessage += olMovementNr + GetString(IDS_TW_TOW) + GetString(IDS_TW_STOA) + olCR;
			}

			if (opTifa < opTifd)
			{
				olMessage += olMovementNr + GetString(IDS_TW_TOW) + GetString(IDS_TW_TIFA) + CString("<\t");
				olMessage += olMovementNr + GetString(IDS_TW_TOW) + GetString(IDS_TW_TIFD) + olCR;
//				olMessage += "MoveArr-Tifa < MoveDep-Tifd\n";
			}

			ROTGDLGFLIGHTDATA *prlNextMovement = NULL;
			if (i < ilCount-1)
			{
				prlNextMovement = &omMovements[i+1];
				if (prlNextMovement->Stod == TIMENULL)
					continue;

				if (prlNextMovement)
				{
					char bufferNext[64];
					itoa(i+2, bufferNext, 10);
					CString olMovementNrNext = CString(bufferNext);

					CTime opTifdNext = TIMENULL;
					CTime opTifaNext = TIMENULL;
					opDepTimeError.Empty();
					opArrTimeError.Empty();
					VerifyMovement(prlNextMovement, opTifdNext, opTifaNext, opDepTimeError, opArrTimeError);

					//check movement with next movement
					if (opTifa > opTifdNext)
					{
						olMessage += olMovementNr + GetString(IDS_TW_TOW) + GetString(IDS_TW_TIFA) + CString(">\t");
						olMessage += olMovementNrNext + GetString(IDS_TW_TOW) + GetString(IDS_TW_TIFD) + olCR;
//						olMessage += "Move-Tifa > Move-Tifd\n";
					}

/*					//check movement internal (done in first step)
					if (!opDepTimeError.IsEmpty())
					{
						olMessage += olMovementNrNext + GetString(IDS_TW_TOW) + GetString(IDS_TW_OFBL) + CString(" < ");
						olMessage += olMovementNrNext + GetString(IDS_TW_TOW) + GetString(IDS_TW_STOD) + olCR;
					}

					if (!opArrTimeError.IsEmpty())
					{
						olMessage += olMovementNrNext + GetString(IDS_TW_TOW) + GetString(IDS_TW_ONBL) + CString(" < ");
						olMessage += olMovementNrNext + GetString(IDS_TW_TOW) + GetString(IDS_TW_STOA) + olCR;
					}
*/
					//check movement with next movement
					if (strcmp(prlMovement->Psta, prlNextMovement->Pstd) != 0)
					{
						olMessage += olMovementNr + GetString(IDS_TW_TOW) + GetString(IDS_TW_POSA) + CString("!\t");
						olMessage += olMovementNrNext + GetString(IDS_TW_TOW) + GetString(IDS_TW_POSD) + olCR;
//						olMessage += "Move-Posa != MoveNext-Posd\n";
					}
				}
			}
			else
			{
				//check departure with last movement
				if (prmDFlight->Urno > 0)
				{
					if(opTifa > prmDFlight->Tifd)
					{
						olMessage += olMovementNr + GetString(IDS_TW_TOW) + GetString(IDS_TW_TIFA) + CString(">\t");
						olMessage += GetString(IDS_TW_DEP) + GetString(IDS_TW_TIFD) + olCR;
	//					olMessage += "MoveLast-Tifa > Dep-Tifd\n";
					}

					if (strcmp(prlMovement->Psta, prmDFlight->Pstd) != 0)
					{
						olMessage += olMovementNr + GetString(IDS_TW_TOW) + GetString(IDS_TW_POSA) + CString("!\t");
						olMessage += GetString(IDS_TW_DEP) + GetString(IDS_TW_POSD) + olCR;
	//					olMessage += "Dep-Posd != MoveLast-Posa\n";
					}
				}
			}
		}
	}

	if (olMessage.IsEmpty())
		return true;
	else
	{
		CString olWarningText;
		olWarningText.Format("%s\n%s", olMessage, GetString(IDS_STRING2037));
		if (CFPMSApp::MyTopmostMessageBox(this, olWarningText, GetString(IDS_WARNING), MB_ICONWARNING | MB_YESNO) == IDYES)
		{
			return true;
		}
		else
			return false;
	}

	return true;
}

void RotGroundDlg::VerifyMovement(ROTGDLGFLIGHTDATA *prpMovement, CTime& opTifd, CTime& opTifa, CString& opDepTimeError, CString& opArrTimeError)
{
	opTifd = TIMENULL;
	opTifa = TIMENULL;
	opDepTimeError.Empty();
	opArrTimeError.Empty();

	if (!prpMovement)
		return;

	if(prpMovement->Ofbu != TIMENULL)
	{
		opTifd = prpMovement->Ofbu;
	}
	else
	{
		if(prpMovement->Stod != TIMENULL)
		{
			opTifd = prpMovement->Stod;
		}
	}

	if(prpMovement->Ofbu != TIMENULL && prpMovement->Stod != TIMENULL)
	{
		if (prpMovement->Ofbu < prpMovement->Stod)
			opDepTimeError = "Ofbl < Stod\n";
	}



	if(prpMovement->Onbu != TIMENULL)
	{
		opTifa = prpMovement->Onbu;
	}
	else
	{
		if(prpMovement->Stoa != TIMENULL)
		{
			opTifa = prpMovement->Stoa;
		}
	}

	if(prpMovement->Onbu != TIMENULL && prpMovement->Stoa != TIMENULL)
	{
		if (prpMovement->Onbu < prpMovement->Stoa)
			opArrTimeError = "Onbl < Stoa\n";
	}
}

void RotGroundDlg::ProcessFlightChange(ROTGDLGFLIGHTDATA *prpFlight)
{

	if(prpFlight == NULL)
		return;

	InitDialog( &omRotGDlgFlights.omData);

}


void RotGroundDlg::OnDelete() 
{

	int ilItems[100];
	int	ilAnz = 100;
	
	CListBox *polLB = pomTable->GetCTableListBox();
	if (polLB == NULL)
		return;

	ilAnz = MyGetSelItems(*polLB, ilAnz, ilItems);


	if(ilAnz <= 0 || (ilAnz==1 && ilItems[0]==0))
	{
		MessageBox( GetString(IDS_STRING968), GetString(ST_HINWEIS) );
		return;

	}
	else
	{
		char buffer[64];

		itoa(ilAnz, buffer, 10);

		CString olText;

		olText.Format(IDS_STRING1255, buffer);
		
		if(MessageBox( olText, GetString(ST_HINWEIS), MB_YESNO ) == IDYES)
		{
			// delete the selected records
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));	
			for(int i = 0; i < ilAnz; i++)
			{
				if(omMovements[ilItems[i]-1].Urno > 0)
				{
					omRotGDlgFlights.DeleteFlight(	omMovements[ilItems[i]-1].Urno  );
					omMovements[ilItems[i]-1].Urno = 0;
					omMovements[ilItems[i]-1].Stoa = TIMENULL;
				}
			}
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));	

			// Shall the departure position be updated?
			CString olLastPos;
			if (GetLastPos(olLastPos))
			{
				// Is last position not the departure position?
				if (!olLastPos.IsEmpty() && olLastPos != prmDFlight->Pstd)
				{
					if (MessageBox(GetString(IDS_STRING1973), GetString(ST_FRAGE), MB_ICONQUESTION | MB_YESNO) == IDYES)
					{
						// update departure position
						SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));	

						ROTGDLGFLIGHTDATA rlFlightDSave = *prmDFlight;
						strncpy(prmDFlight->Pstd, olLastPos, 7);
 						omRotGDlgFlights.UpdateFlight(prmDFlight, &rlFlightDSave);

						SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));	
					}
				}
			}
		}
	}
}

// returns the index of the first free line in the table
// if the table is full, -1 is returned
int RotGroundDlg::GetFirstFreeLine()
{
	CString olTyp;
	CString olPosD;
	CString olDateD;

	for (int i=1; i < pomTable->GetLinesCount(); i++)
	{
		pomTable->GetTextFieldValue(i, 1, olTyp);
		pomTable->GetTextFieldValue(i, 2, olPosD);
		pomTable->GetTextFieldValue(i, 3, olDateD);

		if (olTyp.IsEmpty() && olPosD.IsEmpty() && olDateD.IsEmpty())
			break;
	}

	if (i < pomTable->GetLinesCount())
		return i;
	else
		return -1;
}


afx_msg void RotGroundDlg::OnNewTowing() 
{

	//// fill the first three columns of the new Towing
	int ilLineNo = GetFirstFreeLine();
	if (ilLineNo < 1)
		return;

	// at first reset whole line
	FillTableLine(ilLineNo, &omMovements[ilLineNo-1]);

	// set towing data
	pomTable->SetTextFieldValue(ilLineNo, 1, "T");

	if (ilLineNo == 1)
	{
		pomTable->SetTextFieldValue(ilLineNo, 2, prmAFlight->Psta);
		pomTable->SetTextFieldValue(ilLineNo, 3, prmAFlight->Stoa.Format("%d.%m.%y"));
	}
	else
	{
		CString olTmp;
 		pomTable->GetTextFieldValue(ilLineNo-1, 6, olTmp);
		pomTable->SetTextFieldValue(ilLineNo, 2, olTmp);
		pomTable->GetTextFieldValue(ilLineNo-1, 3, olTmp);
		pomTable->SetTextFieldValue(ilLineNo, 3, olTmp);
	}
	pomTable->DisplayTable();

}


void RotGroundDlg::InitialStatus()
{
	CWnd* plWnd = NULL;
	switch (emStatus)
	{
		case UNKNOWN:
			InitialPostFlight(TRUE);
			break;
		case POSTFLIGHT:
			InitialPostFlight(FALSE);
			break;
		default:
			InitialPostFlight(TRUE);
			break;
	}
}


afx_msg void RotGroundDlg::OnFlight() 
{
	CString olAdid ("");
	long llUrno = 0;
	if (prmAFlight)
	{
		llUrno = prmAFlight->Urno;
		olAdid = prmAFlight->Adid;
	}
	else if (prmDFlight)
	{
		llUrno = prmDFlight->Urno;
		olAdid = prmDFlight->Adid;
	}

 	pogRotationDlg->NewData(this, lmCallByRkey, llUrno, olAdid, bmLocalTime);
}


void RotGroundDlg::setStatus(Status epStatus)
{
	emStatus = epStatus;
}

RotGroundDlg::Status RotGroundDlg::getStatus() const
{
	return emStatus;
}

void RotGroundDlg::InitialPostFlight(BOOL bpPostFlight)
{
	CWnd* plWnd = NULL;
	plWnd = GetDlgItem(IDC_DELETE);
	if (plWnd) 
		plWnd->EnableWindow(bpPostFlight);

	plWnd = GetDlgItem(IDC_NEW_TOWING);
	if (plWnd) 
		plWnd->EnableWindow(bpPostFlight);

	if (pomTable) 
		pomTable->SetTableEditable(bpPostFlight);

//	ModifyWindowText(this, GetString(IDS_STRINGWND_POSTFLIGHT), bpPostFlight);
}

BOOL RotGroundDlg::CheckPostFlight()
{
	BOOL blPost = FALSE;

	ROTGDLGFLIGHTDATA *prlAFlight = omRotGDlgFlights.GetFlightAFromLongRkey(prmAFlight->Rkey);
	ROTGDLGFLIGHTDATA *prlDFlight = omRotGDlgFlights.GetFlightDFromLongRkey(prmAFlight->Rkey);

	if (prlAFlight && prlDFlight)
	{
		if (prlAFlight->Tifa != TIMENULL && prlDFlight->Tifd != TIMENULL)
		{
			if (IsPostFlight(prlAFlight->Tifa,false) && IsPostFlight(prlDFlight->Tifd,false))
				blPost = TRUE;
		}
		else if (prlDFlight->Tifd != TIMENULL)
		{
			if (IsPostFlight(prlDFlight->Tifd,false))
				blPost = TRUE;
		}
		else if (prlAFlight->Tifa != TIMENULL)
		{
			if (IsPostFlight(prlAFlight->Tifa,false))
				blPost = TRUE;
		}
	}
	else if (!prlAFlight && prlDFlight)
	{
		if (prlDFlight->Tifd != TIMENULL)
		{
			if (IsPostFlight(prlDFlight->Tifd,false))
				blPost = TRUE;
		}
	}
	else if (prlAFlight && !prlDFlight)
	{
		if (prlAFlight->Tifa != TIMENULL)
		{
			if (IsPostFlight(prlAFlight->Tifa,false))
				blPost = TRUE;
		}
	}

	if (blPost)
		ModifyWindowText(this, GetString(IDS_STRINGWND_POSTFLIGHT), FALSE);
	else
		ModifyWindowText(this, GetString(IDS_STRINGWND_POSTFLIGHT), TRUE);

	InitialPostFlight(!blPost);
	
	return blPost;
}
