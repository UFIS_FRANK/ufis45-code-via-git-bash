#ifndef __FLUGHAEFENTABLEVIEWER_H__
#define __FLUGHAEFENTABLEVIEWER_H__

#include <stdafx.h>
#include <RotationDlgCedaFlightData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct FLUGHAEFENTABLE_LINEDATA
{
	long		Urno; 	// Eindeutige Datensatz-Nr.
	CString		Cntr;	// Anzahl der Bewegungen
	CString 	Des3; 	// Bestimmungsflughafen 3-Lettercode
	long		Direct;
	long		Via1;
	long		ViaN;
	long		Total;
	CString		Airlines;

	FLUGHAEFENTABLE_LINEDATA(void)
	{ 
		Urno	= 0;
		Direct	= 0;
		Via1	= 0;
		ViaN	= 0;
		Total	= 0;
		Airlines	= CString("");
		Cntr	= CString("");
		Des3	= CString("");
	}

};

/////////////////////////////////////////////////////////////////////////////
// FlughaefenTableViewer

class FlughaefenTableViewer : public CViewer
{
// Constructions
public:
    FlughaefenTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo = NULL, bool bpArr = false, bool bpDep = true);
    ~FlughaefenTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
    void MakeLines();
	void MakeLine(CString opDes3old, int iCntr);
// Operations
public:
	void DeleteAll(void);
	void CreateLine(FLUGHAEFENTABLE_LINEDATA *prpFlughaefen);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay(void);
	bool PrintPlanToFile(char *pcpDefPath);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
	bool bmListStateArr;
// Attributes
private:
    CCSTable *pomFlughaefenTable;
	CCSPtrArray<ROTATIONDLGFLIGHTDATA> *pomData;
public:
    CCSPtrArray<FLUGHAEFENTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView(void);
	bool PrintTableLine(FLUGHAEFENTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader(void);
	CCSPrint *pomPrint;
	char *pcmInfo;
	CString omTableName;
	CString omPrintName;
	CString omFileName;
	CString omFooterName;

};

#endif //__FLUGHAEFENTABLEVIEWER_H__
