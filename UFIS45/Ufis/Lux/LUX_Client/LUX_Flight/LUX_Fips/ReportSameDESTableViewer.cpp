// ReportSameDESTableViewer.cpp : implementation file
// 
// List of all flights take off from the same destination
// Angabe einer destination

#include <stdafx.h>
#include <ReportSameDESTableViewer.h>
#include <CcsGlobl.h>
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include <BasicData.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// ReportSameDESTableViewer
//

int ReportSameDESTableViewer::GetVia(CCSPtrArray<VIADATA> *opVias, ROTATIONDLGFLIGHTDATA *prpFlight)
{
	opVias->DeleteAll();

	if(prpFlight == NULL)
		return 0;
	if(strlen(prpFlight->Vial) == 0)
		return 0;

	CString olVias(prpFlight->Vial);
	CString olFids;
	CString olApc3;
	CString olApc4;

	VIADATA *prlVia;

	while(olVias.IsEmpty() != TRUE)
	{
			prlVia = new VIADATA;
			opVias->Add(prlVia);

			if(olVias.GetLength() < 120)
			{
				olVias += "                                                                                                                             ";
				olVias = olVias.Left(120);
			}


			olApc3 = olVias.Mid(1,3);
			olApc3.TrimLeft();
			sprintf(prlVia->Apc3, olApc3);

			if(olVias.GetLength() >= 120)
				olVias = olVias.Right(olVias.GetLength() - 120);

	}
	return opVias->GetSize();
}



ReportSameDESTableViewer::ReportSameDESTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo, char *pcpSelect)
{
	pomData = popData;
	pcmInfo = pcpInfo;
	pcmSelect = pcpSelect;


	bmIsFromSearch = false;
    pomTable = NULL;
}

ReportSameDESTableViewer::~ReportSameDESTableViewer()
{
	omPrintHeadHeaderArray.DeleteAll();  // BWi
    DeleteAll();
}


void ReportSameDESTableViewer::SetParentDlg(CDialog* ppParentDlg)
{

	pomParentDlg = ppParentDlg;

}


void ReportSameDESTableViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}



void ReportSameDESTableViewer::ChangeViewTo(const char *pcpViewName)
{

    pomTable->ResetContent();
    DeleteAll();    
    MakeLines();

	UpdateDisplay();
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}


/////////////////////////////////////////////////////////////////////////////
// ReportSameDESTableViewer -- code specific to this class

void ReportSameDESTableViewer::MakeLines()
{
	ROTATIONDLGFLIGHTDATA *prlAFlight;
	ROTATIONDLGFLIGHTDATA *prlDFlight;
	ROTATIONDLGFLIGHTDATA *prlNextFlight = NULL;
	ROTATIONDLGFLIGHTDATA *prlFlight;
	int ilLineNo;

	bool blRDeparture = false;

	int ilFlightCount = pomData->GetSize();

	for (int ilLc = 0; ilLc < ilFlightCount; ilLc++)
	{
		prlFlight = &(*pomData)[ilLc];

		if(ilLc + 1 < ilFlightCount)
			prlNextFlight = &(*pomData)[ilLc + 1];	
		else
			prlNextFlight = NULL;	

		//Arrival
		if((strcmp(prlFlight->Org3, pcgHome) != 0) && (strcmp(prlFlight->Des3, pcgHome) == 0))
		{
			prlAFlight = prlFlight;
			prlDFlight = NULL;
			if(prlNextFlight != NULL)
			{
				if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
				{
					prlDFlight = prlNextFlight;

					if((strcmp(prlNextFlight->Org3, pcgHome) == 0) && (strcmp(prlNextFlight->Des3, pcgHome) != 0))
					{
						ilLc++;
					}
					else
						blRDeparture = true;
				}
			}
		}
		else
		{
			// Departure
			if((strcmp(prlFlight->Org3, pcgHome) == 0) && (strcmp(prlFlight->Des3, pcgHome) != 0))
			{
				prlAFlight = NULL;
				prlDFlight = prlFlight;
				blRDeparture = false;
			}
			else
			{
				//Turnaround
				if((strcmp(prlFlight->Org3, pcgHome) == 0) && (strcmp(prlFlight->Des3, pcgHome) == 0))
				{
					if(blRDeparture)
					{
						blRDeparture = false;
						prlAFlight = prlFlight;
						prlDFlight = NULL;
						if(prlNextFlight != NULL)
						{
							if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
							{
								prlDFlight = prlNextFlight;
								if((strcmp(prlNextFlight->Org3, pcgHome) == 0) && (strcmp(prlNextFlight->Des3, pcgHome) != 0))
								{
									ilLc++;
								}
								else
									blRDeparture = true;
							}
						}
					}
					else
					{
						prlAFlight		= prlFlight;
						prlDFlight		= prlFlight;
						if(prlNextFlight != NULL)
						{
							if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
							{
								ilLc--;
								prlAFlight	 = NULL;
								blRDeparture = true;
							}
						}
					}
				}
			}
		}
		ilLineNo = MakeLine(prlAFlight, prlDFlight);

	}


}

		




int ReportSameDESTableViewer::MakeLine(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight)
{
    SAMEDESTABLE_LINEDATA rlLine;

	if((prpAFlight != NULL) && (prpDFlight != NULL) )
	{
		MakeLineData(prpAFlight, prpDFlight, rlLine);
		return CreateLine(rlLine);
	}
	if (prpAFlight != NULL)
	{
		MakeLineData(prpAFlight, NULL, rlLine);
		return CreateLine(rlLine);
	}
	if (prpDFlight != NULL)
	{
		MakeLineData(NULL, prpDFlight, rlLine);
		return CreateLine(rlLine);
	}
	return -1;
}




void ReportSameDESTableViewer::MakeLineData(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight, SAMEDESTABLE_LINEDATA &rpLine)
{
	CString olStr;
	if(prpAFlight != NULL)
	{
		rpLine.AUrno =  prpAFlight->Urno;
		rpLine.ARkey =  prpAFlight->Rkey;
		rpLine.AFlno = CString(prpAFlight->Flno);
		rpLine.AStoa = prpAFlight->Stoa; 
		if(bgReportLocal)
			ogBasicData.UtcToLocal(rpLine.AStoa);
		rpLine.ADate = prpAFlight->Stoa.Format("%d.%m.%y");//DateToHourDivString(prpAFlight->Onbl, prpAFlight->Stoa);
		rpLine.AOrg3 = CString(prpAFlight->Org3);
		rpLine.AOrg4 = CString(prpAFlight->Org4);
		rpLine.AAct  = CString(prpAFlight->Act3);

		CCSPtrArray<VIADATA> opVias;
		int ilViaCnt = GetVia(&opVias, prpAFlight);

		if(opVias.GetSize() == 0)
			rpLine.AVia3 = "";
		else
		{
			rpLine.AVia3 =  opVias[ilViaCnt-1].Apc3;	// letzter Via vor Homeairport = letzte Zeile in Vial
		}
		opVias.DeleteAll();
	}
	else
	{

		rpLine.AUrno =  0;
		rpLine.ARkey =  0;
		rpLine.AFlno = "";
		rpLine.AStoa = TIMENULL; 
		rpLine.ALand = TIMENULL; 
		rpLine.AEtai = TIMENULL; 
		rpLine.ADate = ""; 
		rpLine.AOrg3 = "";
		rpLine.AOrg4 = "";
		rpLine.AVia3 = "";
		rpLine.AAct = "";
	}


	if(prpDFlight != NULL)
	{

		rpLine.DUrno =  prpDFlight->Urno;
		rpLine.DRkey =  prpDFlight->Rkey;
		rpLine.DFlno = CString(prpDFlight->Flno); 
		rpLine.DDes3 = CString(prpDFlight->Des3);
		rpLine.DDes4 = CString(prpDFlight->Des4);
		rpLine.DAct  = CString(prpDFlight->Act3);

		rpLine.DAirb = CTime(prpDFlight->Airb);
		if(bgReportLocal) 
			ogBasicData.UtcToLocal(rpLine.DAirb);
		rpLine.DEtdi = CTime(prpDFlight->Etdi);
		if(bgReportLocal) 
			ogBasicData.UtcToLocal(rpLine.DEtdi);
		rpLine.DStod = prpDFlight->Stod;
		if(bgReportLocal)
			ogBasicData.UtcToLocal(rpLine.DStod);
		rpLine.DDate = prpDFlight->Stod.Format("%d.%m.%y");
		rpLine.DVian = prpDFlight->Vian;

		if (rpLine.AAct.IsEmpty())
			rpLine.AAct = rpLine.DAct;


		CCSPtrArray<VIADATA> opVias;
		int ilViaCnt = GetVia(&opVias, prpDFlight);

		if(opVias.GetSize() == 0)
			rpLine.DVia3 = "";
		else
			rpLine.DVia3 =  opVias[0].Apc3;	// erster Via nach Homeairport = erste Zeile in Vial
		opVias.DeleteAll();

	
	}
	else
	{
		rpLine.DUrno =  0;
		rpLine.DRkey =  0;
		rpLine.DEtdi = TIMENULL; 
		rpLine.DStod = TIMENULL; 
		rpLine.DAirb = TIMENULL; 
		rpLine.DFlno = "";
		rpLine.DDate = ""; 
		rpLine.DDes3 = "";
		rpLine.DDes4 = "";
		rpLine.DVia3 = "";
		rpLine.DAct = "";
	}
    return;
}



int ReportSameDESTableViewer::CreateLine(SAMEDESTABLE_LINEDATA &rpLine)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareFlight(&rpLine, &omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]

    omLines.NewAt(ilLineno, rpLine);
	return ilLineno;
}


void ReportSameDESTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}



void ReportSameDESTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}


/////////////////////////////////////////////////////////////////////////////
// ReportSameDESTableViewer - display drawing routine



// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void ReportSameDESTableViewer::UpdateDisplay()
{
	pomTable->ResetContent();

	DrawHeader();

	SAMEDESTABLE_LINEDATA *prlLine;
    
	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		int ilColumnNo = 0;
		prlLine = &omLines[ilLc];
		CCSPtrArray<TABLE_COLUMN> olColList;
	
		MakeColList(prlLine, olColList);

		pomTable->AddTextLine(olColList, (void*)(prlLine));
		olColList.DeleteAll();
	}


    pomTable->DisplayTable();
}




void ReportSameDESTableViewer::DrawHeader()
{
	int  ilTotalLines = 0;
	bool blNewLogicLine = false;

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);
	pomTable->ResetContent();
	
	TABLE_HEADER_COLUMN *prlHeader[5];

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 67; 
	prlHeader[0]->Font = &ogCourier_Bold_10;
	prlHeader[0]->Text = GetString(IDS_STRING1078);

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 80; 
	prlHeader[1]->Font = &ogCourier_Bold_10;
	prlHeader[1]->Text = GetString(IDS_STRING332);

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 60; 
	prlHeader[2]->Font = &ogCourier_Bold_10;
	prlHeader[2]->Text = GetString(IDS_STRING316);

	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 70; 
	prlHeader[3]->Font = &ogCourier_Bold_10;
	prlHeader[3]->Text = GetString(IDS_STRING321);

	prlHeader[4] = new TABLE_HEADER_COLUMN;
	prlHeader[4]->Alignment = COLALIGN_CENTER;
	prlHeader[4]->Length = 35;
	prlHeader[4]->Font = &ogCourier_Bold_10;
	prlHeader[4]->Text = GetString(IDS_STRING1569);

	for(int ili = 0; ili < 5; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}

	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();

}



void ReportSameDESTableViewer::MakeColList(SAMEDESTABLE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList)
{

	TABLE_COLUMN rlColumnData;

	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogCourier_Regular_9;
	
	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = prlLine->DFlno;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = prlLine->DDate;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = prlLine->DStod.Format("%H:%M");//prlLine->DEtdi.Format("%H:%M");
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = prlLine->DAirb.Format("%H:%M");
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = prlLine->DAct;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

}




int ReportSameDESTableViewer::CompareFlight(SAMEDESTABLE_LINEDATA *prpLine1, SAMEDESTABLE_LINEDATA *prpLine2)
{
	int	ilCompareResult;
	CTime olTime1;
	CTime olTime2;

		olTime1 = prpLine1->DStod;

		olTime2 = prpLine2->DStod;

		ilCompareResult = (olTime1 == olTime2)? 0:
			(olTime1 > olTime2)? 1: -1;

		 return ilCompareResult;
}



//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------


void ReportSameDESTableViewer::GetHeader()
{
		
	omPrintHeadHeaderArray.DeleteAll();  // BWi

	TABLE_HEADER_COLUMN *prlHeader[5];
	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 67; 
	prlHeader[0]->Font = &ogCourier_Bold_10;
	prlHeader[0]->Text = GetString(IDS_STRING1078);

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 80; 
	prlHeader[1]->Font = &ogCourier_Bold_10;
	prlHeader[1]->Text = GetString(IDS_STRING332);

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 60; 
	prlHeader[2]->Font = &ogCourier_Bold_10;
	prlHeader[2]->Text = GetString(IDS_STRING316);

	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 70; 
	prlHeader[3]->Font = &ogCourier_Bold_10;
	prlHeader[3]->Text = GetString(IDS_STRING321);

	prlHeader[4] = new TABLE_HEADER_COLUMN;
	prlHeader[4]->Alignment = COLALIGN_CENTER;
	prlHeader[4]->Length = 65; 
	prlHeader[4]->Font = &ogCourier_Bold_10;
	prlHeader[4]->Text = GetString(IDS_STRING1569);


	for(int ili = 0; ili < 5; ili++)
	{
		omPrintHeadHeaderArray.Add( prlHeader[ili]);
	}

}




void ReportSameDESTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	char pclFooter[256];

	sprintf(pclFooter, GetString(IDS_STRING1574), (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));

	omFooterName = pclFooter;
	CString olTableName = GetString(IDS_STRING1574);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint(pomParentDlg,ilOrientation,45);

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = olTableName;	
			//rlDocInfo.lpszDocName = TABLENAME;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();  
			olFooter1.Format(GetString(IDS_STRING1445), ilLines,omFooterName);

			GetHeader();

			if (ilLines == 0)
				PrintTableHeader(); // Drucken leerer Tabelle
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}

		omPrintHeadHeaderArray.DeleteAll();
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		delete pomPrint;
		pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool ReportSameDESTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;
	double dgCCSPrintFactor = 2.7 ;
	char pclHeader[256];

	sprintf(pclHeader, GetString(IDS_STRING1578), pcmSelect, pcmInfo, pomData->GetSize());
	CString olTableName(pclHeader);

	pomPrint->PrintUIFHeader(omTableName,CString(pclHeader),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	//int ilSize = omHeaderDataArray.GetSize();
	int ilSize = omPrintHeadHeaderArray.GetSize();

	for(int i=0;i<ilSize;i++)
	{
		//rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
		rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)
			rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Text   = omPrintHeadHeaderArray[i].Text;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool ReportSameDESTableViewer::PrintTableLine(SAMEDESTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	double dgCCSPrintFactor = 2.7 ;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omPrintHeadHeaderArray.GetSize();

	for(int i=0;i<ilSize;i++)
	{
		rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameTop   = PRINT_FRAMETHIN;
		if(bpLastLine == true)
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN;
		}
		else
		{
			rlElement.FrameBottom = PRINT_NOFRAME;
		}
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;


		switch(i)
		{
			case 0:
			{
				rlElement.Text = prpLine->DFlno;
			}
			break;
			case 1:
			{
				rlElement.Text = prpLine->DDate;
			}
			break;
			case 2:
			{
				rlElement.Text = prpLine->DStod.Format("%H:%M");//DEtdi.Format("%H:%M");
			}
			break;
			case 3:
			{
				rlElement.Text = prpLine->DAirb.Format("%H:%M");
			}
			break;
			case 4:
			{
				rlElement.Text = prpLine->DAct;
			}
			break;
		}

		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------


// Drucken in Datei
void ReportSameDESTableViewer::PrintPlanToFile(char *pcpDefPath)
{

	CCS_TRY

	char pclHeadlineSelect[120];
	sprintf(pclHeadlineSelect, GetString(IDS_STRING1578), pcmSelect, pcmInfo, pomData->GetSize());

	ofstream of;
	of.open( pcpDefPath, ios::out);
	of << CString(pclHeadlineSelect) << " "
	   << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M") << endl << endl;

	of << GetString(IDS_STRING1078) << "     "<< GetString(IDS_STRING332) << "      " 
	   << GetString(IDS_STRING316)  << "     "  << GetString(IDS_STRING321) << "   " 
	   << GetString(IDS_STRING1569) << endl;
	of << "------------------------------------------" << endl;
	int ilCount = omLines.GetSize();

	
	for(int i = 0; i < ilCount; i++)
	{
		SAMEDESTABLE_LINEDATA rlD = omLines[i];
		of.setf(ios::left, ios::adjustfield);
		of   << setw(10) << rlD.DFlno  
			 << setw(10) << rlD.DDate
			 << setw(8)  << rlD.DStod.Format("%H:%M")/*rlD.DEtdi.Format("%H:%M")*/
			 << setw(8)  << rlD.DAirb.Format("%H:%M")
			 << setw(9)  << rlD.DAct
			 << endl;
	}
	of.close();

	CCS_CATCH_ALL

}
