#ifndef AFX_DAILYPRINTDLG_H__78313AD1_F60C_11D1_A438_0000B45A33F5__INCLUDED_
#define AFX_DAILYPRINTDLG_H__78313AD1_F60C_11D1_A438_0000B45A33F5__INCLUDED_

// Header-Datei DAILYPRINT.H 
//
#include <CCSEdit.h>


/////////////////////////////////////////////////////////////////////////////
// 
const int imMontagsID = 1159 ;	// IDS_STRING1159 "Montag"


/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDailyPrint 

class CDailyPrint : public CDialog
{
// Konstruktion
public:
	// Standardkonstruktor (?)
	CDailyPrint(CWnd* pParent, char *opSelect, int *plTyp , char* pspVerkehr, bool *pbpSeason, char *opWhere = NULL ); 

// Dialogfelddaten
	//{{AFX_DATA(CDailyPrint)
	enum { IDD = IDD_DAILYPRINT };
	CStatic	m_ST_VTag;
	CStatic	m_ST_Tag;
	CButton	m_CE_ButSeason;
	CButton	m_CE_Season;
	CEdit	m_CE_AirlineEx;
	CEdit	m_CE_Verkehrsrechte;
	CEdit	m_CE_Airlines;
	CButton	m_X_Terminal2;
	CButton	m_X_Terminal1;
	CCSEdit	m_CE_Vonzeit;
	CCSEdit	m_CE_Vomtag;
	CCSEdit	m_CE_Biszeit;
	CCSEdit	m_CE_Bistag;
	CString	m_Bistag;
	CString	m_Biszeit;
	CString	m_Vomtag;
	CString	m_Vonzeit;
	BOOL	m_Terminal1;
	BOOL	m_Terminal2;
	CString	m_Airlines;
	CString	m_Verkehrsrechte;
	CString	m_AirlineEx;
//	CButton	m_CR_List1;
//	int		m_List1;
//	BOOL	m_ListAirl;
//	BOOL	m_ListAirlEx;
	CButton	m_X_ListAirl;
	CButton	m_X_ListAirlEx;
	//}}AFX_DATA

	char *pcmSelect;	// Selektkriterium für CeDa WHERE (....)
	char *pcmWhere;		// Selektkriterium für PartialRead WHERE (....)
	int *pmTyp ;		// Terminal checks
	char *pcmVerkehr ;	// Verkehrsrechte

	class CReportSelectDlg *pomSelectDlg ;	// Zeiger auf Parent 


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDailyPrint)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDailyPrint)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnKillfocusVomtag( UINT, UINT );
	afx_msg void OnSeason();
	afx_msg void OnCheckSeason();
	afx_msg void OnKillfocusECAirlines();
	afx_msg void OnAirl();
	afx_msg void OnAirlEx();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	bool *pbmSeason;				// true => seasonal / false => daily OPS
	CString omVerkehrstag;			// gewählter Verkehrstag bei Saison

	CTime omLastEnd;
	CString omSeasonEnd;		// Zeitraum Enddatum
	CTime omLastStart;
	CString omSeasonStart;		//    "    Startdatum   
};


//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_DAILYPRINTDLG_H__78313AD1_F60C_11D1_A438_0000B45A33F5__INCLUDED_




