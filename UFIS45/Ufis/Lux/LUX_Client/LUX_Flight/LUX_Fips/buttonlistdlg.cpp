// CButtonListDlg.cpp : implementation file
//

#include <stdafx.h>
#include <process.h>
#include <FPMS.h>
#include <ButtonListDlg.h>
#include <SeasonTableDlg.h>
#include <SeasonCollectDlg.h>
#include <RotationTables.h>
#include <FlightDiagram.h>
#include <BltDiagram.h>
#include <GatDiagram.h>
#include <PosDiagram.h>
#include <WroDiagram.h>
#include <CcaDiagram.h>
#include <ProxyDlg.h>
#include <BasicData.h>
#include <CCSCedaCom.h>
#include <CCSGlobl.h>
#include <CCSBCHandle.h>
#include <BasicData.h>
#include <PrivList.h>
#include <CCSDdx.h>
#include <AcirrDlg.h>
#include <AirportInformationDlg.h>
#include <CcaCommonTableDlg.h>
#include <KonflikteDlg.h>
#include <SetupDlg.h>
#include <WoResTableDlg.h>
#include <CcaCommonDlg.h>
#include <CcaCommonDlg.h>
#include <CicDemandTableDlg.h>
#include <DailyScheduleTableDlg.h>
#include <ReportSelectDlg.h>
#include <FlightSearchTableDlg.h>
#include <RotationDlg.h>
#include <RotGroundDlg.h>
#include <Utils.h>

#include <UFISAmSink.h>
#include <ClntTypes.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#define BCActiveTestSecs 60
#define BCTestSecs 180


HDDEDATA CALLBACK DdeCallback(UINT, UINT, HCONV, HSZ, HSZ, HDDEDATA, DWORD, DWORD);


static void ButtonListDlgCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCButtonListDlg dialog

IMPLEMENT_DYNAMIC(CButtonListDlg, CDialog);

CButtonListDlg::CButtonListDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CButtonListDlg::IDD, pParent), omBcTestData("AFT")
{
	//{{AFX_DATA_INIT(CCButtonListDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDI_UFIS);
	m_pAutoProxy = NULL;
	pomParent = pParent;
	pogSeasonTableDlg = NULL;
	pogRotationTables = NULL;
	pogFlightDiagram = NULL;
 	pogCommonCcaTable = NULL;
	pogCicDemandTableDlg = NULL;
	pogDailyScheduleTableDlg = NULL;

	bmWait = false;
	bmIsCuteIFStarted = false;
	bmBcTestActive = false;
	bmBCOK = true;

    ogDdx.Register(this, INF_CHANGE, CString("BUTTONLISTDLG"), CString("ButtonListDlg Update"), ButtonListDlgCf);
    ogDdx.Register(this, INF_NEW,    CString("BUTTONLISTDLG"), CString("ButtonListDlg New"),    ButtonListDlgCf);
    ogDdx.Register(this, BC_INF_CHANGE, CString("BUTTONLISTDLG"), CString("ButtonListDlg Update"), ButtonListDlgCf);
    ogDdx.Register(this, BC_INF_NEW,    CString("BUTTONLISTDLG"), CString("ButtonListDlg New"),    ButtonListDlgCf);
    ogDdx.Register(this, BC_INF_DELETE, CString("BUTTONLISTDLG"), CString("ButtonListDlg Delete"), ButtonListDlgCf);

	ogDdx.Register(this, BC_TEST, CString("BC_TEST"), CString("CButtonListDlg"), ButtonListDlgCf);

	m_UFISAmSink.SetLauncher(this);
}

CButtonListDlg::~CButtonListDlg()
{
	// If there is an automation proxy for this dialog, set
	//  its back pointer to this dialog to NULL, so it knows
	//  the dialog has been deleted.
	if (m_pAutoProxy != NULL)
		m_pAutoProxy->m_pDialog = NULL;

	ogDdx.UnRegister(this, NOTUSED);

}

void CButtonListDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CButtonListDlg)
	DDX_Control(pDX, IDC_COMMONCCA, m_CB_CommonCCA);
	DDX_Control(pDX, IDC_WARTERAEUME, m_CB_Warteraeume);
	DDX_Control(pDX, IDC_TELEXPOOL, m_CB_Telexpool);
	DDX_Control(pDX, IDC_TAGESFLUGPLANERFASSEN, m_CB_Tagesflugplanerfassen);
	DDX_Control(pDX, IDC_STAMMDATEN, m_CB_Stammdaten);
	DDX_Control(pDX, IDC_SETUP, m_CB_Setup);
	DDX_Control(pDX, IDC_RUECKGAENGIG, m_CB_Rueckgaengig);
	DDX_Control(pDX, IDC_REGELN, m_CB_Regeln);
 	DDX_Control(pDX, IDC_REPORTS, m_CB_Reports);
	DDX_Control(pDX, IDC_LFZPOSITIONEN, m_CB_LFZPositionen);
	DDX_Control(pDX, IDC_KONFLIKTE, m_CB_Konflikte);
	DDX_Control(pDX, IDC_INFO, m_CB_Info);
	DDX_Control(pDX, IDC_HILFE, m_CB_Hilfe);
	DDX_Control(pDX, IDC_GEPAECKBAENDER, m_CB_Gepaeckbaender);
	DDX_Control(pDX, IDC_GATES, m_CB_Gates);
	DDX_Control(pDX, IDC_FLUGDIAGRAMM, m_CB_Flugdiagramm);
	DDX_Control(pDX, IDC_COVERAGE, m_CB_Coverage);
	DDX_Control(pDX, IDC_CHECKIN, m_CB_Checkin);
	DDX_Control(pDX, IDC_ACIN, m_CB_Acin);
	DDX_Control(pDX, IDC_ACHTUNG, m_CB_Achtung);
	DDX_Control(pDX, IDC_FLUGPLANPFLEGEN, m_CB_Flugplanpflegen);
	DDX_Control(pDX, IDC_FLUGPLANERFASSEN, m_CB_Flugplanerfassen);
	DDX_Control(pDX, IDC_BC_STATUS, m_CB_BCStatus);
	DDX_Control(pDX, IDC_BEENDEN, m_CB_Beenden);
	DDX_Control(pDX, IDC_ABOUT, m_CB_About);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CButtonListDlg, CDialog)
	//{{AFX_MSG_MAP(CButtonListDlg)
	ON_WM_TIMER()
	ON_WM_SYSCOMMAND()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_BEENDEN, OnBeenden)
	ON_BN_CLICKED(IDC_DRUCKEN, OnDrucken)
	ON_BN_CLICKED(IDC_HILFE, OnHilfe)
	ON_BN_CLICKED(IDC_ABOUT, OnAbout)
	ON_BN_CLICKED(IDC_SETUP, OnSetup)
	ON_BN_CLICKED(IDC_INFO, OnInfo)
	ON_BN_CLICKED(IDC_KONFLIKTE, OnKonflikte)
	ON_BN_CLICKED(IDC_COVERAGE, OnCoverage)
	ON_BN_CLICKED(IDC_FLUGPLANERFASSEN, OnFlugplanErfassen)
	ON_BN_CLICKED(IDC_FLUGPLANPFLEGEN, OnFlugplanPflegen)
	ON_BN_CLICKED(IDC_TAGESFLUGPLANERFASSEN, OnTagesflugplanErfassen)
	ON_BN_CLICKED(IDC_FLUGDIAGRAMM, OnFlugdiagramm)
	ON_BN_CLICKED(IDC_CHECKIN, OnCheckin)
	ON_BN_CLICKED(IDC_GEPAECKBAENDER, OnGepaeckbaender)
	ON_BN_CLICKED(IDC_GATES, OnGates)
	ON_BN_CLICKED(IDC_WARTERAEUME, OnWarteraeume)
	ON_BN_CLICKED(IDC_LFZPOSITIONEN, OnLFZPositionen)
	ON_BN_CLICKED(IDC_STAMMDATEN, OnStammdaten)
	ON_BN_CLICKED(IDC_REGELN, OnRegeln)
	ON_BN_CLICKED(IDC_ACHTUNG, OnAchtung)
	ON_BN_CLICKED(IDC_RUECKGAENGIG, OnRueckgaengig)
	ON_BN_CLICKED(IDC_REPORTS, OnReports)
	ON_BN_CLICKED(IDC_ACIN, OnACIn)
	ON_BN_CLICKED(IDC_TELEXPOOL, OnTelexpool)
	ON_MESSAGE(WM_BCADD,OnBcAdd)
	ON_WM_NCLBUTTONDOWN()
	ON_BN_CLICKED(IDC_COMMONCCA, OnCommoncca)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCButtonListDlg message handlers

BOOL CButtonListDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	ogCommHandler.RegisterBcWindow(this);
	ogBcHandle.GetBc();

	pogButtonList = this;



	pogKonflikteDlg = new KonflikteDlg(this);

	pogWoResTableDlg = new WoResTableDlg(this);

	pogSeasonDlg = new CSeasonDlg(this);

	pogCommonCcaDlg = new CCcaCommonDlg(this);

	pogCommonCcaDlg->ShowWindow(SW_HIDE);

//	pogCicDemandTableDlg = new CicDemandTableDlg(this);

//	pogCicDemandTableDlg->ShowWindow(SW_HIDE);

	pogRotationDlg = new RotationDlg(this);

	pogReportSelectDlg = new CReportSelectDlg(this);

	pogFlightSearchTableDlg = new FlightSearchTableDlg(this);

    // calculate the window height
    CRect rectScreen(0, 0, ::GetSystemMetrics(SM_CXSCREEN), 65); //  ::GetSystemMetrics(SM_CYSCREEN));
	CWnd::MoveWindow(&rectScreen, TRUE);

	CRect rect;
    GetClientRect(&rect);
    m_nDialogBarHeight = rect.bottom - rect.top;

	// moved from the constructor
//    SetTimer(2, (UINT) 60 * 1000, NULL);
//	SetTimer(3, 250, NULL);
	SetTimer(1, (UINT) 5 * 1000 , NULL); // Konflikt Check, update bar times

    SetWindowText(ogAppName);

	SetIcon(m_hIcon, FALSE);
	SetIcon(m_hIcon, TRUE);			// Set big icon


	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Flugplanpflegen"),m_CB_Flugplanpflegen);
	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Flugplanerfassen"),m_CB_Flugplanerfassen);

	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Tagesflugplanerfassen"),m_CB_Tagesflugplanerfassen);
	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Flugdiagramm"),m_CB_Flugdiagramm);

	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Gepaeckbaender"),m_CB_Gepaeckbaender);
	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Gates"),m_CB_Gates);
	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_LFZPositionen"),m_CB_LFZPositionen);
	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Warteraeume"),m_CB_Warteraeume);
	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Checkin"),m_CB_Checkin);

	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Regeln"),m_CB_Regeln);

	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Stammdaten"),m_CB_Stammdaten);
	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Telexpool"),m_CB_Telexpool);

	
	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Hilfe"),m_CB_Hilfe);
 	m_CB_Info.SetSecState(ogPrivList.GetStat("DESKTOP_CB_Info"));
	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Acin"),m_CB_Acin);
	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Fluggastinfo"),m_CB_CommonCCA);
	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Coverage"),m_CB_Coverage);
	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Reports"),m_CB_Reports);

	m_CB_Konflikte.SetSecState(ogPrivList.GetStat("DESKTOP_CB_Konflikte"));
	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Setup"),m_CB_Setup);
//	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_UserButton"),m_CB_Rueckgaengig);

	/*
	if (ogPrivList.GetStat("DESKTOP_CB_Achtung") != '1')
	{
		UINT ilBStyle = m_CB_Achtung.GetButtonStyle();
		m_CB_Achtung.SetButtonStyle(ilBStyle & !BS_OWNERDRAW, 1);
	}
	SetWndStatAll(ogPrivList.GetStat("DESKTOP_CB_Achtung"),m_CB_Achtung);
	*/
	m_CB_Achtung.SetSecState(ogPrivList.GetStat("DESKTOP_CB_Achtung"));

	m_CB_Flugplanerfassen.SetWindowText(GetString(IDS_STRING1552));
	


	char pclConfigPath[256];
	char pclExcelPath[256];

	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "USER_BUTTON_LABEL", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, pclConfigPath);

/*
	if(CString(pclExcelPath) != "DEFAULT")
	{
		m_CB_Rueckgaengig.SetWindowText(pclExcelPath);
		m_CB_Rueckgaengig.EnableWindow(TRUE);
	}
	else
	{
		m_CB_Rueckgaengig.SetWindowText("");
		m_CB_Rueckgaengig.EnableWindow(FALSE);
	}
*/

	m_CB_BCStatus.SetColors(LIME, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));

	m_CB_About.SetCheck(1);


	TestBc();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CButtonListDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

void CButtonListDlg::OnDestroy()
{
	WinHelp(0L, HELP_QUIT);
	CDialog::OnDestroy();
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CButtonListDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CButtonListDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

// Automation servers should not exit when a user closes the UI
//  if a controller still holds on to one of its objects.  These
//  message handlers make sure that if the proxy is still in use,
//  then the UI is hidden but the dialog remains around if it
//  is dismissed.

LONG CButtonListDlg::OnBcAdd(UINT wParam, LONG /*lParam*/)
{
	ProcessBcTest();
	
	if (bmBCOK)
	{
		m_CB_BCStatus.SetColors(RGB(236, 236,   0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		m_CB_BCStatus.UpdateWindow();
	}

	ogBcHandle.GetBc(wParam);
	//ogBcHandle.GetBc();

	if (bmBCOK)
	{
		m_CB_BCStatus.SetColors(LIME, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		m_CB_BCStatus.UpdateWindow();
	}
	return TRUE;
}



void CButtonListDlg::TestBc()
{
	// reset Bc Test Timer
	KillTimer(4);

	//Send BC
	omBcTestData.SendBC("BCTEST", "", "", "");
 
	SetTimer(5, (UINT) 1000 * BCActiveTestSecs, NULL);
}



void CButtonListDlg::OnTimer(UINT nIDEvent)
{

	if (nIDEvent == 1)
	{

		ogPosDiaFlightData.UpdateBarTimes();

	}
	else
	if (nIDEvent == 5)
	{
		// Reset BC-Active-Test Timer
		KillTimer(5);

		// BC-Test failed!!
		if (bmBCOK)
		{
			bmBCOK = false;
			m_CB_BCStatus.SetColors(RED, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			if (AfxMessageBox(GetString(IDS_STRING2094), (MB_YESNO|MB_SYSTEMMODAL|MB_ICONWARNING)) != IDYES)
			{
				Exit();
			}
		}

		// Set Bc Test Timer
		SetTimer(4, (UINT) 1000 * BCTestSecs, NULL);
	}

	if (nIDEvent == 4)
	{
		// Test BC!
		TestBc();
	}

}


bool CButtonListDlg::Exit() 
{
	if (CanExit())
	{
		CleanUp();
		CDialog::OnOK();
		return true;
	}
	return false;
}


void CButtonListDlg::OnClose() 
{
	Exit();
}

void CButtonListDlg::OnOK() 
{
	Exit();
}

void CButtonListDlg::OnCancel() 
{
	OnBeenden();
}

BOOL CButtonListDlg::CanExit()
{
	// If the proxy object is still around, then the automation
	//  controller is still holding on to this application.  Leave
	//  the dialog around, but hide its UI.
	if (m_pAutoProxy != NULL)
	{
		ShowWindow(SW_HIDE);
		return FALSE;
	}

	return TRUE;
}


void CButtonListDlg::OnBeenden() 
{
 
	if(pomParent->MessageBox(GetString(IDS_STRING980), GetString(ST_FRAGE), MB_YESNO | MB_ICONQUESTION) == IDNO)
		return;
	Exit();
}


void CButtonListDlg::CleanUp()
{ 
	// Send EXIT-Message to CuteIF
	if (bgComOk)
	{
		if (bmIsCuteIFStarted) // soll in der Zukunft von UfisAppManag ermittelt werden
		{
			CallCuteIF(CString("EXIT"));
		}
		pConnect->DetachApp(static_cast<long>(Fips));
		bgComOk = false;
	}

	//RST-REL
	ogCommHandler.CleanUpCom();	

	CCS_TRY
		if (pogFlightSearchTableDlg != NULL)
		{
			pogFlightSearchTableDlg->DestroyWindow();
			delete pogFlightSearchTableDlg;
			pogFlightSearchTableDlg = NULL;
		}

		if (pogReportSelectDlg != NULL)
		{
			pogReportSelectDlg->DestroyWindow();
			delete pogReportSelectDlg;
			pogReportSelectDlg = NULL;
		}

		if (pogRotationDlg != NULL)
		{
			pogRotationDlg->DestroyWindow();
			delete pogRotationDlg;
			pogRotationDlg = NULL;
		}

		if (pogCommonCcaDlg != NULL)
		{
			pogCommonCcaDlg->DestroyWindow();
			delete pogCommonCcaDlg;
			pogCommonCcaDlg = NULL;
		}

		if (pogSeasonDlg != NULL)
		{
			pogSeasonDlg->DestroyWindow();
			delete pogSeasonDlg;
			pogSeasonDlg = NULL;
		}


		if (pogSeasonTableDlg != NULL)
		{
			pogSeasonTableDlg->DestroyWindow();
			delete pogSeasonTableDlg;
			pogSeasonTableDlg = NULL;
		}

		if (pogRotationTables != NULL)
		{
			pogRotationTables->DestroyWindow();
			delete pogRotationTables;
			pogRotationTables = NULL;
		}

		if (pogDailyScheduleTableDlg != NULL)
		{
			pogDailyScheduleTableDlg->DestroyWindow();
			delete pogDailyScheduleTableDlg;
			pogDailyScheduleTableDlg = NULL;
		}

		if (pogFlightDiagram != NULL)
		{
			pogFlightDiagram->DestroyWindow();
			delete pogFlightDiagram;
			pogFlightDiagram = NULL;
		}

		if (pogCcaDiagram != NULL)
		{
			pogCcaDiagram->DestroyWindow();
			delete pogCcaDiagram;
			pogCcaDiagram = NULL;
		}

		if (pogPosDiagram != NULL)
		{
			pogPosDiagram->DestroyWindow();
			delete pogPosDiagram;
			pogPosDiagram = NULL;
		}

		if (pogGatDiagram != NULL)
		{
			pogGatDiagram->DestroyWindow();
			delete pogGatDiagram;
			pogGatDiagram = NULL;
		}

		if (pogBltDiagram != NULL)
		{
			pogBltDiagram->DestroyWindow();
			delete pogBltDiagram;
			pogBltDiagram = NULL;
		}

		if (pogWroDiagram != NULL)
		{
			pogWroDiagram->DestroyWindow();
			delete pogWroDiagram;
			pogWroDiagram = NULL;
		}

		if (pogWoResTableDlg != NULL)
		{
			pogWoResTableDlg->DestroyWindow();
			delete pogWoResTableDlg;
			pogWoResTableDlg = NULL;
		}

		if (pogKonflikteDlg != NULL)
		{
			pogKonflikteDlg->DestroyWindow();
			delete pogKonflikteDlg;
			pogKonflikteDlg = NULL;
		}

		if (pogRotGroundDlg != NULL)
		{
			pogRotGroundDlg->DestroyWindow();
			delete pogRotGroundDlg;
			pogRotGroundDlg = NULL;
		}


	CCS_CATCH_ALL


	//SetTimer(0, (UINT) 1000, NULL);
}



void CButtonListDlg::OnFlugplanErfassen() 
{

	// dynamische DailyTable fuer Shanghai

	// to configure the header of the table
		CStringArray olColumnsToShow; 
/*
		// Arrival
		olColumnsToShow.Add("AFLNO");
		olColumnsToShow.Add("ASTOADate");
		olColumnsToShow.Add("ASTOATime");
		olColumnsToShow.Add("ATTYP");
		olColumnsToShow.Add("AVIA4");
		olColumnsToShow.Add("AVN");
		olColumnsToShow.Add("AAIRB");
		olColumnsToShow.Add("ATMOA");
		olColumnsToShow.Add("AETAI");
		olColumnsToShow.Add("ALAND");
		olColumnsToShow.Add("APSTA");
		olColumnsToShow.Add("ABLT1/ABLT2");
		olColumnsToShow.Add("VIP");
		olColumnsToShow.Add("AREMP/DREMP");

		// Rotation
		olColumnsToShow.Add("AREGN");
		olColumnsToShow.Add("AACT5");

		// Departure
		olColumnsToShow.Add("DFLNO");
		olColumnsToShow.Add("DSTODDate");
		olColumnsToShow.Add("DSTODTime");
		olColumnsToShow.Add("DTTYP");
		olColumnsToShow.Add("DDES4");
		olColumnsToShow.Add("DVN");
		olColumnsToShow.Add("DETDI");
		olColumnsToShow.Add("DAIRB");
		olColumnsToShow.Add("DPSTD");
		olColumnsToShow.Add("DGTD1");
		olColumnsToShow.Add("DGTD2");
		olColumnsToShow.Add("DCKIF/DCKIT");
	
*/
		// Arrival
		olColumnsToShow.Add("AFLNO");		//arr flnr
		olColumnsToShow.Add("ASTOADate");	//date
		olColumnsToShow.Add("AFLTI");		//flight id
		olColumnsToShow.Add("ATTYP");		//nature
		olColumnsToShow.Add("AORG3");		//orig

		olColumnsToShow.Add("AVIA3L");		//last via
		olColumnsToShow.Add("AAIRB");		//atd
		olColumnsToShow.Add("ASTOATime");	//sta
		olColumnsToShow.Add("AETAI");		//eta
		olColumnsToShow.Add("ATMOA");		//tmo
		olColumnsToShow.Add("ALAND/AONLB");	//ata/obl
		olColumnsToShow.Add("APSTA/DPSTD");	//pos arr/dep

		// Rotation
		olColumnsToShow.Add("RACT3");		//a/c
		olColumnsToShow.Add("RREGN");		//registr

		// Departure
		olColumnsToShow.Add("DFLNO");		//dep flnr
		olColumnsToShow.Add("DSTODDate");	//date
		olColumnsToShow.Add("DFLTI");		//flight id
		olColumnsToShow.Add("DTTYP");		//nature
		olColumnsToShow.Add("DVIA3L");		//next via 
		olColumnsToShow.Add("DDES3");		//dest
		olColumnsToShow.Add("DSTODTime");	//std
		olColumnsToShow.Add("DETDI");		//etd
		olColumnsToShow.Add("DOFBL/DAIRB");	//ofbl/airb
		olColumnsToShow.Add("DCKIF/DCKIT");	//cki
		olColumnsToShow.Add("DGTD1/DGTD2");	//gate


		// Dialog nicht mehr/noch nicht offen
		if (pogDailyScheduleTableDlg == NULL)
		{
			pogDailyScheduleTableDlg = new DailyScheduleTableDlg(olColumnsToShow, this);
			// StringArray mit den anzuzeigenden Spalten uebergeben
			//pogDailyScheduleTableDlg->SetColumnsToShow(olColumnsToShow);
			pogDailyScheduleTableDlg->ShowAnsicht();
		}
		else
		{
			pogDailyScheduleTableDlg->SetWindowPos(&wndTop, 0,0,0,0, 
												   SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE);
		}

		olColumnsToShow.RemoveAll();
}

void CButtonListDlg::OnFlugplanPflegen() 
{
	if (pogSeasonTableDlg == NULL)
	{
		pogSeasonTableDlg = new CSeasonTableDlg(this);
		pogSeasonTableDlg->ShowAnsicht();
	}
	else
	{
		pogSeasonTableDlg->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
	}
}


void CButtonListDlg::OnTagesflugplanErfassen() 
{
	if (pogRotationTables == NULL)
	{
		pogRotationTables = new RotationTables(this);
	}
	else
	{
		pogRotationTables->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
	}
}


void CButtonListDlg::OnDrucken() 
{
	// TODO: Add your control notification handler code here
	
}

void CButtonListDlg::OnHilfe() 
{
	//AfxGetApp()->WinHelp(0,HELP_FINDER);
	//AfxGetApp()->WinHelp(0,HELP_CONTENTS);

	char File[64];

	strcpy(File, ogAppName);
	strcat(File, ".HLP");

	CWnd *pwnd = AfxGetMainWnd();
	
	ASSERT(pwnd);

//---------------------------------------------------
//Neu MWO: Da AatHelp das Hilfe Fenster unter den ButtonListDlg schiebt und man nicht dran kommt
	CString omFileName;
	char pclTmpText[512];
	char pclConfigPath[512];
	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString("GLOBAL", "HELPDIRECTORY", "c:\\Ufis\\Help",pclTmpText,sizeof pclTmpText,pclConfigPath);
	omFileName = pclTmpText;												
	if(!omFileName.IsEmpty())
	{
		GetPrivateProfileString(ogAppName, "HELPFILE", ogAppName + ".chm",pclTmpText,sizeof pclTmpText,pclConfigPath);
		if (!omFileName.IsEmpty())
		{
			if (omFileName[omFileName.GetLength() - 1] != '\\')
				omFileName += '\\';
			omFileName += pclTmpText;
			char *args[4];
			args[0] = "child";
			args[1] = omFileName.GetBuffer(0);
			args[2] = NULL;
			args[3] = NULL;
			int ilReturn = _spawnv(_P_NOWAIT,"C:\\Winnt\\HH.exe",args);
		}
	}

	//	AfxGetApp()->WinHelp(0, HELP_CONTENTS);
	//::WinHelp(pwnd->m_hWnd, File, HELP_FINDER, 0);
//End MWO
}

void CButtonListDlg::OnSetup() 
{
	SetupDlg olDlg;
	olDlg.DoModal();

}


void CButtonListDlg::OnInfo() 
{
	AirportInformationDlg olInfoDlg;//(pomBackGround)
	olInfoDlg.DoModal();
	m_CB_Info.SetColors(SILVER,GRAY,WHITE);
}



void CButtonListDlg::OnCoverage() 
{
	// TODO: Add your control notification handler code here
	
}


void CButtonListDlg::OnFlugdiagramm() 
{
	CRect olRect;
	GetWindowRect(&olRect);

	if(	pogFlightDiagram  == NULL)
	{	
		pogFlightDiagram = new FlightDiagram();
		pogFlightDiagram->Create(NULL, GetString(IDS_STRING1257), WS_HSCROLL | WS_CAPTION | WS_THICKFRAME | WS_VISIBLE | WS_MAXIMIZEBOX | WS_SYSMENU | WS_MINIMIZEBOX ,
			CRect(0, olRect.bottom, olRect.right/*GetSystemMetrics(SM_CXSCREEN)*/, GetSystemMetrics(SM_CYSCREEN)),
			this,NULL,0,NULL);
	}	
	else
	{
		pogFlightDiagram->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
	}
}




void CButtonListDlg::OnRegeln() 
{
	
		char pclConfigPath[256];
		char pclExcelPath[256];

		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
			strcpy(pclConfigPath, getenv("CEDA"));

		GetPrivateProfileString(ogAppName, "RULES", "DEFAULT",
			pclExcelPath, sizeof pclExcelPath, pclConfigPath);


		if(!strcmp(pclExcelPath, "DEFAULT"))
			MessageBox(GetString(IDS_STRING1478), GetString(ST_FEHLER), MB_ICONERROR);


		/*
		HWND hw=::FindWindow("#32770","Duty Preferences");
		//::ShowWindow(hw,SW_RESTORE   );
		::ShowWindow(hw,SW_MINIMIZE);
		::ShowWindow(hw,SW_SHOWNORMAL      );
		//::SetActiveWindow(hw);
		*/

		char *args[4];
		char slRunTxt[256] = "";
		args[0] = "child";
		sprintf(slRunTxt,"%s,%s",pcgUser,pcgPasswd);
		args[1] = slRunTxt;
		args[2] = NULL;
		args[3] = NULL;
		int ilReturn = _spawnv(_P_NOWAIT,pclExcelPath,args);

}


void CButtonListDlg::OnRueckgaengig() 
{
	
		char pclConfigPath[256];
		char pclExcelPath[256];

		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
			strcpy(pclConfigPath, getenv("CEDA"));

		GetPrivateProfileString(ogAppName, "USER_BUTTON_PATH", "DEFAULT",
			pclExcelPath, sizeof pclExcelPath, pclConfigPath);


		if(!strcmp(pclExcelPath, "DEFAULT"))
			MessageBox(GetString(IDS_STRING1478), GetString(ST_FEHLER), MB_ICONERROR);


		/*
		HWND hw=::FindWindow("#32770","Duty Preferences");
		//::ShowWindow(hw,SW_RESTORE   );
		::ShowWindow(hw,SW_MINIMIZE);
		::ShowWindow(hw,SW_SHOWNORMAL      );
		//::SetActiveWindow(hw);
		*/

		char *args[4];
		char slRunTxt[256] = "";
		args[0] = "child";
		sprintf(slRunTxt,"%s,%s",pcgUser,pcgPasswd);
		args[1] = slRunTxt;
		args[2] = NULL;
		args[3] = NULL;
		int ilReturn = _spawnv(_P_NOWAIT,pclExcelPath,args);


}

/*
void CButtonListDlg::OnOnline() 
{

	if(bmOnline)
	{
		m_CB_Online.SetWindowText(GetString(IDS_STRING1258));
		m_CB_Online.SetColors(RGB(255,0,0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));

		ogBcHandle.TableCommandSetOnlyOwn(CString("AFT") + CString(pcgTableExt), CString("SPR"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("AFT") + CString(pcgTableExt), CString("ISF"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("AFT") + CString(pcgTableExt), CString("UFR"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("AFT") + CString(pcgTableExt), CString("DFR"), BC_FLIGHT_DELETE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("AFT") + CString(pcgTableExt), CString("JOF"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("AFT") + CString(pcgTableExt), CString("UPS"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("AFT") + CString(pcgTableExt), CString("UPJ"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("AFT") + CString(pcgTableExt), CString("IFR"), BC_FLIGHT_CHANGE, true);

		ogBcHandle.TableCommandSetOnlyOwn(CString("SDB") + CString(pcgTableExt), CString("SPR"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("SDB") + CString(pcgTableExt), CString("ISF"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("SDB") + CString(pcgTableExt), CString("UFR"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("SDB") + CString(pcgTableExt), CString("DFR"), BC_FLIGHT_DELETE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("SDB") + CString(pcgTableExt), CString("JOF"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("SDB") + CString(pcgTableExt), CString("UPS"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("SDB") + CString(pcgTableExt), CString("UPJ"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("SDB") + CString(pcgTableExt), CString("IFR"), BC_FLIGHT_CHANGE, true);

		ogBcHandle.TableCommandSetOnlyOwn(CString("ARC") + CString(pcgTableExt), CString("SPR"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("ARC") + CString(pcgTableExt), CString("ISF"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("ARC") + CString(pcgTableExt), CString("UFR"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("ARC") + CString(pcgTableExt), CString("DFR"), BC_FLIGHT_DELETE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("ARC") + CString(pcgTableExt), CString("JOF"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("ARC") + CString(pcgTableExt), CString("UPS"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("ARC") + CString(pcgTableExt), CString("UPJ"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("ARC") + CString(pcgTableExt), CString("IFR"), BC_FLIGHT_CHANGE, true);

		ogBcHandle.TableCommandSetOnlyOwn(CString(""), CString("SPR"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString(""), CString("ISF"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString(""), CString("UFR"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString(""), CString("DFR"), BC_FLIGHT_DELETE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString(""), CString("JOF"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString(""), CString("UPS"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString(""), CString("UPJ"), BC_FLIGHT_CHANGE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString(""), CString("IFR"), BC_FLIGHT_CHANGE, true);

		ogBcHandle.TableCommandSetOnlyOwn(CString("CCA") + CString(pcgTableExt), CString("IRT"), BC_CCA_NEW, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("CCA") + CString(pcgTableExt), CString("DRT"), BC_CCA_DELETE, true);
		ogBcHandle.TableCommandSetOnlyOwn(CString("CCA") + CString(pcgTableExt), CString("URT"), BC_CCA_CHANGE, true);

	}
	else
	{
		m_CB_Online.SetWindowText(GetString(IDS_STRING1259));
		m_CB_Online.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));

		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));		

		
		ogBcHandle.TableCommandSetOnlyOwn(CString("AFT") + CString(pcgTableExt), CString("SPR"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("AFT") + CString(pcgTableExt), CString("ISF"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("AFT") + CString(pcgTableExt), CString("UFR"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("AFT") + CString(pcgTableExt), CString("DFR"), BC_FLIGHT_DELETE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("AFT") + CString(pcgTableExt), CString("JOF"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("AFT") + CString(pcgTableExt), CString("UPS"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("AFT") + CString(pcgTableExt), CString("UPJ"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("AFT") + CString(pcgTableExt), CString("IFR"), BC_FLIGHT_CHANGE, false);

		ogBcHandle.TableCommandSetOnlyOwn(CString("SDB") + CString(pcgTableExt), CString("SPR"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("SDB") + CString(pcgTableExt), CString("ISF"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("SDB") + CString(pcgTableExt), CString("UFR"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("SDB") + CString(pcgTableExt), CString("DFR"), BC_FLIGHT_DELETE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("SDB") + CString(pcgTableExt), CString("JOF"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("SDB") + CString(pcgTableExt), CString("UPS"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("SDB") + CString(pcgTableExt), CString("UPJ"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("SDB") + CString(pcgTableExt), CString("IFR"), BC_FLIGHT_CHANGE, false);

		ogBcHandle.TableCommandSetOnlyOwn(CString("ARC") + CString(pcgTableExt), CString("SPR"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("ARC") + CString(pcgTableExt), CString("ISF"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("ARC") + CString(pcgTableExt), CString("UFR"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("ARC") + CString(pcgTableExt), CString("DFR"), BC_FLIGHT_DELETE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("ARC") + CString(pcgTableExt), CString("JOF"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("ARC") + CString(pcgTableExt), CString("UPS"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("ARC") + CString(pcgTableExt), CString("UPJ"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("ARC") + CString(pcgTableExt), CString("IFR"), BC_FLIGHT_CHANGE, false);

		ogBcHandle.TableCommandSetOnlyOwn(CString(""), CString("SPR"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString(""), CString("ISF"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString(""), CString("UFR"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString(""), CString("DFR"), BC_FLIGHT_DELETE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString(""), CString("JOF"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString(""), CString("UPS"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString(""), CString("UPJ"), BC_FLIGHT_CHANGE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString(""), CString("IFR"), BC_FLIGHT_CHANGE, false);

		ogBcHandle.TableCommandSetOnlyOwn(CString("CCA") + CString(pcgTableExt), CString("IRT"), BC_CCA_NEW, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("CCA") + CString(pcgTableExt), CString("DRT"), BC_CCA_DELETE, false);
		ogBcHandle.TableCommandSetOnlyOwn(CString("CCA") + CString(pcgTableExt), CString("URT"), BC_CCA_CHANGE, false);
		
		ogDdx.DataChanged((void *)this, DATA_RELOAD, NULL);

		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));		

	}
	bmOnline = !bmOnline;

}


*/



void CButtonListDlg::OnACIn() 
{
	CAcirrDlg olAcirrDlg(this,"");
	olAcirrDlg.DoModal();
}


void CButtonListDlg::OnStammdaten() 
{
	char pclConfigPath[256];
	char pclExcelPath[256];

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

    GetPrivateProfileString(ogAppName, "BDPS-UIF", "DEFAULT",
        pclExcelPath, sizeof pclExcelPath, pclConfigPath);


	if(!strcmp(pclExcelPath, "DEFAULT"))
		MessageBox(GetString(IDS_STRING981), GetString(ST_FEHLER), MB_ICONERROR);

	//CString	olTables = "ALTACTACRAPTRWYTWYPSTGATCICBLTEXTDENMVTNATHAGWROHTYSTYFID-STRSPHSEA";//Alle
	// Update der Liste von ARE 07.05.99
	//CString	olTables = "ALT ACT ACR APT RWY TWY PST GAT CIC BLT EXT DEN MVT NAT HAG WRO HTY STY FID STR SPH SEA GHS GEG PER PEF GRM GRN ORG PFC COT ASF BSS BSD ODA TEA STF HTC WAY PRC CHT TIP HOL WGP SWG";
	// Update der Liste von ARE 17.05.99
	CString	olTables = "ALT-ACT-ACR-APT-RWY-TWY-PST-GAT-CIC-BLT-EXT-DEN-MVT-NAT-HAG-WRO-HTY-STY-FID-STR-SPH-SEA-GHS-GEG-PER-PEF-GRM-GRN-ORG-PFC-COT-ASF-BSS-BSD-ODA-TEA-STF-HTC-WAY-PRC-CHT-TIP-BLK-HOL-WGP-SWG-PGP-AWI-CCC-VIP";


	char *args[4];
	char slRunTxt[256];
	args[0] = "child";
	sprintf(slRunTxt,"%s,%s,%s,%s",ogAppName,pcgUser,pcgPasswd,olTables);
	args[1] = slRunTxt;
	args[2] = NULL;
	args[3] = NULL;
	int ilReturn = _spawnv(_P_NOWAIT,pclExcelPath,args);
}




void CButtonListDlg::OnTelexpool() 
{
	CallTelexpool(-1, "", "", "", "", "", "", "", "", TIMENULL, TIMENULL);
}



void CButtonListDlg::CallTelexpool(long lpUrno, const CString &ropAlc3, const CString &ropFltn,
								   const CString &ropFlns, const CString &ropTifFrom, const CString &ropTifTo,
								   const CString &ropRegn, const CString &ropFlda, const CString &ropAdid, const CTime &ropStoa, const CTime &ropStod) 
{
	// The product works with the new telexpool

	//if ((strcmp(pcgHome, "ATH") == 0 || strcmp(pcgHome, "DXB") == 0) && bgComOk)
	if (bgComOk)
		CallTelexpoolNew(lpUrno, ropAlc3, ropFltn, ropFlns, ropRegn, ropFlda, ropAdid, ropStoa, ropStod);
	//else
	//	CallTelexpoolOld(ropAlc3, ropFltn, ropFlns, ropTifFrom, ropTifTo);

}


void CButtonListDlg::CallTelexpoolNew(long lpUrno, const CString &ropAlc3, const CString &ropFltn, const CString &ropFlns, 
									  const CString &ropRegn, const CString &ropFlda, const CString &ropAdid, const CTime &ropStoa, const CTime &ropStod) 
{

	CString olSendStr;
	CString olFieldList("URNO,ALC3,FLTN,FLNS,REGN,FLDA,ADID,STOA,STOD");
	CString olData;

	olData.Format("%ld,%s,%s,%s,%s,%s,%s,%s,%s", lpUrno, ropAlc3, ropFltn, ropFlns, ropRegn, ropFlda,
		ropAdid, ropStoa.Format("%Y%m%d%H%M00"), ropStod.Format("%Y%m%d%H%M00"));


	olSendStr.Format("%s\n%s", olFieldList, olData);

 	_bstr_t tmpStr = olSendStr.GetBuffer(olSendStr.GetLength());


	try
	{
		pConnect->TransferData(static_cast<long>(TlxPool),
							   static_cast<long>(Fips),tmpStr);
	}
	catch(_com_error error)
	{
		CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING2010), GetString(ST_FEHLER), MB_ICONERROR | MB_OK);
		return;
	}


	return;
}



void CButtonListDlg::CallTelexpoolOld(const CString &ropAlc3, const CString &ropFltn, const CString &ropFlns,
									  const CString &ropTifFrom, const CString &ropTifTo) 
{
	
	CWaitCursor olWaitCursor; // Wait cursor displayed on construction and removed on destruction

	char pclData[500];

	CString olTtyp("");
	sprintf(pclData,"%s,%s,%s,%s,%s,%s", ropAlc3, ropFltn, ropFlns, ropTifFrom, ropTifTo, olTtyp);

	DWORD olDdeInstance = 0;
	HSZ olApplHandle;
	HSZ olTopicHandle;
	bool blRc = true;

	// initialize DDE
	if( DdeInitialize(&olDdeInstance, (PFNCALLBACK) DdeCallback, APPCMD_FILTERINITS, 0) != DMLERR_NO_ERROR )
	{
		HandleDdeError(olDdeInstance, "RunTool - Error Initializing DDE !");
		AfxMessageBox(GetString(IDS_STRING982));
		blRc = false;
	}
	else
	{
		TRACE("DDE Initialized Successfully.\n");
	}

	// DDE - get a handle to the telexpool application string
	if( blRc )
	{
		olApplHandle = DdeCreateStringHandle(olDdeInstance, "TelexPool", CP_WINANSI);
		if( olApplHandle == 0 )
		{
			HandleDdeError(olDdeInstance, "RunTool - Error Creating a DDE String handle !");
			AfxMessageBox(GetString(IDS_STRING982));
			blRc = false;
		}
		else
		{
			TRACE("Created DDE string handle for the application.\n");
		}
	}

	// DDE - get a handle to a telexpool topic string
	if( blRc )
	{
		olTopicHandle = DdeCreateStringHandle(olDdeInstance, pcgUser, CP_WINANSI);
		if( olApplHandle == 0 )
		{
			HandleDdeError(olDdeInstance, "RunTool - Error Creating a DDE String handle !");
			AfxMessageBox(GetString(IDS_STRING982));
			blRc = false;
		}
		else
		{
			TRACE("Created DDE string handle for a topic.\n");
		}
	}

	// attempt to connect to telexPool
	if( blRc )
	{
		HCONV olConversation = DdeConnect(olDdeInstance, olApplHandle, olTopicHandle, (PCONVCONTEXT) NULL);
		if( olConversation == NULL )
		{
			DWORD olErr = HandleDdeError(olDdeInstance, "RunTool - Error Connecting to the telexpool");

			if(olErr == DMLERR_NO_CONV_ESTABLISHED)
			{
				// cannot connect to telex pool so spawn a new telexpool
				RunTelexPool(ropAlc3, ropFltn, ropFlns, ropTifFrom, ropTifTo, olTtyp);
			}
			else
			{
				// error in connection attempt
				AfxMessageBox(GetString(IDS_STRING982));
			}
		}
		else // connected to the telexpool, so update the data
		{
			// create a handle to the data to be sent
			HDDEDATA olDataHandle;

			// send the data
//				DWORD ilTimeout = 20000; // 20 second timeout
			DWORD ilResult;
//				olDataHandle = DdeClientTransaction((LPBYTE) pclData, strlen(pclData)+1, olConversation, olApplHandle, CF_TEXT, XTYP_POKE, ilTimeout, &ilResult);
			olDataHandle = DdeClientTransaction((LPBYTE) pclData, strlen(pclData)+1, olConversation, olApplHandle, CF_TEXT, XTYP_POKE, TIMEOUT_ASYNC, &ilResult);
			if( olDataHandle == 0 )
			{
				switch(ilResult)
				{
				case DDE_FACK:

					// this is SUCCESS !
					TRACE("DdeClientTransaction returns DDE_FACK\n");
					break;

				case DDE_FBUSY:

					// a modal telexpool dialog was open or the process was busy - tell the user to retry later
//						AfxMessageBox("The telexpool is currently busy.\nPlease check if any telexpool dialogs are open, if so close them and retry.\nIf this error persists contact your system adminstrator.");
					TRACE("DdeClientTransaction returns DDE_FBUSY\n");
					break;

				case DDE_FNOTPROCESSED:

					AfxMessageBox(GetString(IDS_STRING982));
					TRACE("DdeClientTransaction returns DDE_FNOTPROCESSED\n");
					break;

				default:
					DWORD olRc = HandleDdeError(olDdeInstance, "RunTool - Error DdeClientTransaction()");
//						AfxMessageBox("Error cannot communicate with the telexpool, please contact your system adminstrator.");
					break;
				}
			}

			// free the data handle
//				if( DdeFreeDataHandle(olDataHandle) == 0 )
//				{
//					HandleDdeError(olDdeInstance, "RunTool - Error DdeFreeDataHandle()");
//					blRc = false;
//				}
		}
	}



	// end the DDE communication
	if( olDdeInstance != 0 )
	{
		if( DdeFreeStringHandle(olDdeInstance, olApplHandle) == FALSE )
			TRACE("Error deleting the application's DDE string handle !\n");
		else
			TRACE("Deleted the application's DDE string handle.\n");

		if( DdeFreeStringHandle(olDdeInstance, olTopicHandle) == FALSE )
			TRACE("Error deleting the topic's DDE string handle !\n");
		else
			TRACE("Deleted the topic's DDE string handle.\n");

		if(DdeUninitialize(olDdeInstance) == 0)
			TRACE("Error Uninitialzing!\n");
		else
			TRACE("Uninitialized the connection.\n");
	}


//	AfxGetApp()->DoWaitCursor(-1);


}


// debug logging of DDE errors
DWORD CButtonListDlg::HandleDdeError(DWORD opDdeInstance, const char *pcpUserText)
{
	DWORD olErr = DdeGetLastError(opDdeInstance);
	CString olErrText;

	switch(olErr)
	{
	case DMLERR_ADVACKTIMEOUT:
		olErrText = "DMLERR_ADVACKTIMEOUT";
		break;
	case DMLERR_BUSY:
		olErrText = "DMLERR_BUSY";
		break;
	case DMLERR_DATAACKTIMEOUT:
		olErrText = "DMLERR_DATAACKTIMEOUT";
		break;
	case DMLERR_EXECACKTIMEOUT:
		olErrText = "DMLERR_EXECACKTIMEOUT";
		break;
	case DMLERR_NOTPROCESSED:
		olErrText = "DMLERR_NOTPROCESSED";
		break;
	case DMLERR_POKEACKTIMEOUT:
		olErrText = "DMLERR_POKEACKTIMEOUT";
		break;
	case DMLERR_POSTMSG_FAILED:
		olErrText = "DMLERR_POSTMSG_FAILED";
		break;
	case DMLERR_REENTRANCY:
		olErrText = "DMLERR_REENTRANCY";
		break;
	case DMLERR_SERVER_DIED:
		olErrText = "DMLERR_SERVER_DIED";
		break;
	case DMLERR_UNADVACKTIMEOUT:
		olErrText = "DMLERR_UNADVACKTIMEOUT";
		break;
	case DMLERR_MEMORY_ERROR:
		olErrText = "DMLERR_MEMORY_ERROR";
		break;
	case DMLERR_NO_CONV_ESTABLISHED:
		olErrText = "DMLERR_NO_CONV_ESTABLISHED";
		break;
	case DMLERR_DLL_NOT_INITIALIZED:
		olErrText = "DMLERR_DLL_NOT_INITIALIZED";
		break;
	case DMLERR_DLL_USAGE:
		olErrText = "DMLERR_DLL_USAGE";
		break;
	case DMLERR_INVALIDPARAMETER:
		olErrText = "DMLERR_INVALIDPARAMETER";
		break;
	case DMLERR_SYS_ERROR:
		olErrText = "DMLERR_SYS_ERROR";
		break;
	case DMLERR_NO_ERROR:
		olErrText = "DMLERR_NO_ERROR";
		break;
	default:
		olErrText = "Undefined Error";
		break;
	}
	TRACE("%s - %s !\n",pcpUserText,olErrText);

	return olErr;
}


// callback function to handle DDE transactions
HDDEDATA CALLBACK
DdeCallback(UINT uType, UINT uFmt, HCONV hConv, HSZ hszTopic, HSZ hszItem, HDDEDATA hData, DWORD lData1, DWORD lData2)
{
	TRACE("DdeCallback() Received message num %d !\n",uType);
    return((HDDEDATA)FALSE);
}








// spawns a new telexpool
void CButtonListDlg::RunTelexPool(CString opAlc, CString opFltn, CString opFlns, CString opTifFrom, CString opTifTo, CString opTtyp)
{

/*
	char *pclArgs[4];
	char clCmdLine[500];

	sprintf(clCmdLine,"%s,%s,%s,%s,%s,%s,%s,%s",dlg.m_Usid,dlg.m_Pass,dlg.m_Alc3,dlg.m_Fltn,dlg.m_Flns,dlg.m_Tifr,dlg.m_Tito,dlg.m_Ttyp);

	pclArgs[0] = "child";
	pclArgs[1] = clCmdLine;
	pclArgs[2] = NULL;
	_spawnv(_P_NOWAIT,dlg.m_Appl,pclArgs);
*/


	char pclConfigPath[256];
	char pclPath[256];

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

    GetPrivateProfileString(ogAppName, "TELEXPOOL", "DEFAULT",
        pclPath, sizeof pclPath, pclConfigPath);


	if(!strcmp(pclPath, "DEFAULT"))
		MessageBox(GetString(IDS_STRING983), GetString(ST_FEHLER), MB_ICONERROR);


	char *pclArgs[4];
	char clCmdLine[500];

	sprintf(clCmdLine,"%s,%s,%s,%s,%s,%s,%s,%s",pcgUser,pcgPasswd,opAlc,opFltn,opFlns,opTifFrom,opTifTo,opTtyp);


	pclArgs[0] = "child";
	pclArgs[1] = clCmdLine;
	pclArgs[2] = NULL;
	_spawnv(_P_NOWAIT,pclPath,pclArgs);

}


static void ButtonListDlgCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	CButtonListDlg *polDlg = (CButtonListDlg *)popInstance;
	//INFDATA	*polInf = (INFDATA *)vpDataPointer;
    if (ipDDXType == BC_INF_NEW || ipDDXType == BC_INF_CHANGE)
	{
//		polDlg->m_CB_Info.SetColors(RED,GRAY,WHITE);
		polDlg->m_CB_Info.SetColors(YELLOW,GRAY,WHITE);
/*		COLORREF olButtonColor = COLORREF(RGB(0,255,0));
		CString olButtonText;//CWnd SetBk
		olButtonText.Format(GetString(IDS_STRING1936), 12);

		polDlg->m_CB_Info.SetWindowText("EXTERN");
		polDlg->m_CB_Info.SetColors(olButtonColor, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		
		CDC* pDC = polDlg->m_CB_Info.GetDC();
		COLORREF ol = pDC->SetBkColor( olButtonColor );

		polDlg->m_CB_Info.UpdateWindow();
*/
	}
} 



void CButtonListDlg::OnCommoncca() 
{
		char pclConfigPath[256];
		char pclExcelPath[256];

		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
			strcpy(pclConfigPath, getenv("CEDA"));

		GetPrivateProfileString(ogAppName, "PASSINFO", "DEFAULT",
			pclExcelPath, sizeof pclExcelPath, pclConfigPath);


		if(!strcmp(pclExcelPath, "DEFAULT"))
			MessageBox(GetString(IDS_STRING984), GetString(ST_FEHLER), MB_ICONERROR);


		char *args[4];
		char slRunTxt[256];
		args[0] = "child";
		sprintf(slRunTxt,"%s,%s",pcgUser,pcgPasswd);
		args[1] = slRunTxt;
		args[2] = NULL;
		args[3] = NULL;
		int ilReturn = _spawnv(_P_NOWAIT,pclExcelPath,args);

}


void CButtonListDlg::OnGepaeckbaender() 
{
	CRect olRect;
	GetWindowRect(&olRect);

	if(	pogBltDiagram  == NULL)
	{	
		pogBltDiagram = new BltDiagram();
		pogBltDiagram->Create(NULL, GetString(IDS_STRING1010), WS_HSCROLL | WS_CAPTION | WS_THICKFRAME | WS_VISIBLE | WS_MAXIMIZEBOX | WS_SYSMENU | WS_MINIMIZEBOX ,
			CRect(0, olRect.bottom, olRect.right/*GetSystemMetrics(SM_CXSCREEN)*/, GetSystemMetrics(SM_CYSCREEN)),
			this,NULL,0,NULL);
	}	
	else
	{
		pogBltDiagram->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
	}
}

void CButtonListDlg::OnGates() 
{
	CRect olRect;
	GetWindowRect(&olRect);

	if(	pogGatDiagram  == NULL)
	{	
		pogGatDiagram = new GatDiagram();
		pogGatDiagram->Create(NULL, GetString(IDS_STRING1019), WS_HSCROLL | WS_CAPTION | WS_THICKFRAME | WS_VISIBLE | WS_MAXIMIZEBOX | WS_SYSMENU | WS_MINIMIZEBOX ,
			CRect(0, olRect.bottom, olRect.right/*GetSystemMetrics(SM_CXSCREEN)*/, GetSystemMetrics(SM_CYSCREEN)),
			this,NULL,0,NULL);
	}	
	else
	{
		pogGatDiagram->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
	}
// Darunter wenn das echte GATPOS fertig ist
/*
	CRect olRect;
	GetWindowRect(&olRect);

	if(	pogGatPosDiagram  == NULL)
	{	
		pogGatPosDiagram = new GatposDiagram();
		pogGatPosDiagram->Create(NULL, GetString(IDS_STRING1377), WS_HSCROLL | WS_CAPTION | WS_THICKFRAME | WS_VISIBLE | WS_MAXIMIZEBOX | WS_SYSMENU | WS_MINIMIZEBOX ,
			CRect(0, olRect.bottom, olRect.right, GetSystemMetrics(SM_CYSCREEN)),
			this,NULL,0,NULL);
	}	
	else
	{
		pogGatPosDiagram->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
	}
*/
}

void CButtonListDlg::OnWarteraeume() 
{
	CRect olRect;
	GetWindowRect(&olRect);

	if(	pogWroDiagram  == NULL)
	{	
		pogWroDiagram = new WroDiagram();
		pogWroDiagram->Create(NULL, GetString(IDS_STRING823), WS_HSCROLL | WS_CAPTION | WS_THICKFRAME | WS_VISIBLE | WS_MAXIMIZEBOX | WS_SYSMENU | WS_MINIMIZEBOX ,
			CRect(0, olRect.bottom, olRect.right/*GetSystemMetrics(SM_CXSCREEN)*/, GetSystemMetrics(SM_CYSCREEN)),
			this,NULL,0,NULL);
	}	
	else
	{
		pogWroDiagram->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
	}
}

void CButtonListDlg::OnLFZPositionen() 
{
	CRect olRect;
	GetWindowRect(&olRect);

	if(	pogPosDiagram  == NULL)
	{	
		pogPosDiagram = new PosDiagram();
		pogPosDiagram->Create(NULL, GetString(IMFK_AIRCRAFT_POS), WS_HSCROLL | WS_CAPTION | WS_THICKFRAME | WS_VISIBLE | WS_MAXIMIZEBOX | WS_SYSMENU | WS_MINIMIZEBOX ,
			CRect(0, olRect.bottom, olRect.right/*GetSystemMetrics(SM_CXSCREEN)*/, GetSystemMetrics(SM_CYSCREEN)),
			this,NULL,0,NULL);
	}	
	else
	{
		pogPosDiagram->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
	}
}


void CButtonListDlg::OnCheckin() 
{
	CRect olRect;
	GetWindowRect(&olRect);

	if(	pogCcaDiagram  == NULL)
	{	
		pogCcaDiagram = new CcaDiagram();
		pogCcaDiagram->Create(NULL, GetString(IDS_STRING1012), WS_HSCROLL | WS_CAPTION | WS_THICKFRAME | WS_VISIBLE | WS_MAXIMIZEBOX | WS_SYSMENU | WS_MINIMIZEBOX ,
			CRect(0, olRect.bottom, olRect.right/*GetSystemMetrics(SM_CXSCREEN)*/, GetSystemMetrics(SM_CYSCREEN)),
			this,NULL,0,NULL);
	}	
	else
	{
		pogCcaDiagram->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
	}
	
}


void CButtonListDlg::OnReports() 
{
/*	CWnd* olpCwnd = GetDlgItem(IDC_BEENDEN);
	if (olpCwnd)
		olpCwnd->EnableWindow(FALSE);
*/
 	pogReportSelectDlg->SetWindowPos(&wndTop, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
}




void CButtonListDlg::OnAchtung() 
{
	pogKonflikteDlg->Attention();

}

 
void CButtonListDlg::OnKonflikte() 
{
	pogKonflikteDlg->Conflicts();

}


bool CButtonListDlg::CallCuteIF(CString opMessage)
{
	// Only Athens has the CuteIF-Application !!
	if (!bgComOk || strcmp(pcgHome, "ATH") != 0)
		return false;

 	_bstr_t tmpStr = opMessage.GetBuffer(opMessage.GetLength());

	try
	{
		pConnect->TransferData(static_cast<long>(FipsCUTE),
							   static_cast<long>(Fips),tmpStr);
		bmIsCuteIFStarted = true;
	}
	catch(_com_error error)
	{
		CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING2010), GetString(ST_FEHLER), MB_ICONERROR | MB_OK);
		return false;
	}

	return true;
}


void CButtonListDlg::MessTlxpool(CString opInMessage)
{

	CString olWhere;
	if (!GetWhereFromTlxpoolMsg(opInMessage, olWhere))
	{
		CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING2050) + opInMessage, GetString(ST_FEHLER), MB_ICONERROR);
		return;
	}


	if (pogFlightSearchTableDlg)
	{
		pogFlightSearchTableDlg->Activate(olWhere);
	}

}




bool CButtonListDlg::GetWhereFromTlxpoolMsg(const CString &ropInMessage, CString &ropWhere)
{
	// build where string
	CStringArray olMessageLines;

	// get fieldlist and datalist
	ExtractItemList(ropInMessage, &olMessageLines, '\n');
	if (olMessageLines.GetSize() < 2)
	{
		CString olWarn;
		olWarn.Format("WARNING (CButtonListDlg::GetWhereFromTlxpoolMsg): Not two lines received! ('%s')", ropInMessage);
		CFPMSApp::DumpDebugLog(olWarn);
		return false;
	}

	// get fields and data
	CStringArray olFieldList;
	CStringArray olDataList;

	ExtractItemList(olMessageLines[0], &olFieldList, ',');
	ExtractItemList(olMessageLines[1], &olDataList, ',');
	
	if (olFieldList.GetSize() != olDataList.GetSize())
	{
		CString olWarn;
		olWarn.Format("WARNING (CButtonListDlg::GetWhereFromTlxpoolMsg): Item count not equal! (Fields: '%s'  Data: '%s')", olMessageLines[0], olMessageLines[1]);
		CFPMSApp::DumpDebugLog(olWarn);
		return false;
	}


	ropWhere.Empty();

	int ilId;
	int ilId2;

	//// scan for fields!

	CString olData;
	CString olData2;
	ilId = FindIdInStrArray(olFieldList, "FLNU");
	if (ilId >= 0)
	{
		olData = olDataList[ilId];
		olData.TrimLeft();
		if (!olData.IsEmpty())
		{
			// VOLLTREFFER! AFT:URNO vorhanden!
			ropWhere.Format("URNO = '%s'", olData);
			return true;
		}
	}

	bool blTimeframe = false;
	bool blRegn = false;

	CString olTmpWhere;
	ilId = FindIdInStrArray(olFieldList, "VPFR");
	ilId2 = FindIdInStrArray(olFieldList, "VPTO");
	if (ilId >= 0 && ilId2 >= 0)
	{
		olData = olDataList[ilId];
		olData.TrimLeft();
		olData2 = olDataList[ilId2];
		olData2.TrimLeft();
		if (!olData.IsEmpty() && !olData2.IsEmpty())
		{
			// Zeitraum
			olTmpWhere.Format("((TIFA BETWEEN '%s' AND '%s') OR (TIFD BETWEEN '%s' AND '%s'))", olData, olData2, olData, olData2);
			ropWhere += olTmpWhere;
			blTimeframe = true;
		}
	}

	ilId = FindIdInStrArray(olFieldList, "ALC3");
	if (ilId >= 0)
	{
		// Airline 3-Letter-Code
		olData = olDataList[ilId];
		olData.TrimLeft();
		if (!olData.IsEmpty())
		{
			olTmpWhere.Format(" AND ALC3 = '%s'", olData);
			ropWhere += olTmpWhere;
		}
	}

	ilId = FindIdInStrArray(olFieldList, "FLTN");
	if (ilId >= 0)
	{
		// Flightnumber
		olData = olDataList[ilId];
		olData.TrimLeft();
		if (!olData.IsEmpty())
		{
			olTmpWhere.Format(" AND FLTN = '%s'", olData);
			ropWhere += olTmpWhere;
		}
	}

	ilId = FindIdInStrArray(olFieldList, "FLNS");
	if (ilId >= 0)
	{
		// Flight-Suffix
		olTmpWhere.Format(" AND FLNS = '%1s'", olDataList[ilId]);
		ropWhere += olTmpWhere;
	}

	ilId = FindIdInStrArray(olFieldList, "REGN");
	if (ilId >= 0)
	{
		// Registration
		CString olRegn = olDataList[ilId];
		olRegn.TrimLeft();
		// delete existing '-'
		olRegn.Remove('-');
		if (!olRegn.IsEmpty())
		{
			olTmpWhere.Format(" AND REGN = '%s'", olRegn);
			ropWhere += olTmpWhere;
			blRegn = true;
		}
	}

	if (!ropWhere.IsEmpty() && (blRegn || blTimeframe))
		return true;
	else
		return false;
}



void CButtonListDlg::ProcessBcTest()
{
	// broadcast received!
	bmBCOK = true;

	// Kill BC-Active-Test Timer
	KillTimer(5);

	// Reset Bc Test Timer
	SetTimer(4, (UINT) 1000 * BCTestSecs, NULL);
}



void CButtonListDlg::OnAbout() 
{
	if (pogBackGround->IsWindowVisible())
		pogBackGround->ShowWindow(SW_HIDE);
	else
		pogBackGround->ShowWindow(SW_SHOW);

}

