// KonflikteDlg.cpp : implementation file
//

#include <stdafx.h>
#include <fpms.h>
#include <KonflikteDlg.h>
#include <RotationDlg.h>
#include <ButtonlistDlg.h>
#include <CCSGlobl.h>
#include <CCSDdx.h>
#include <RotGroundDlg.h>
#include <SeasonDlg.h>
#include <PrivList.h>
#include <Utils.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



// Local function prototype
static void KonfTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);



/////////////////////////////////////////////////////////////////////////////
// KonflikteDlg dialog




KonflikteDlg::KonflikteDlg(CWnd* pParent /*=NULL*/)
	: CDialog(KonflikteDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(KonflikteDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	pomTable = NULL;
	bmIsActiv = false;
    CDialog::Create(KonflikteDlg::IDD, pParent);

}


KonflikteDlg::~KonflikteDlg()
{
	ogDdx.UnRegister(this,NOTUSED);
	delete pomTable;
}



void KonflikteDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(KonflikteDlg)
	DDX_Control(pDX, IDC_ALLCONFIRM, m_CB_AllConfirm);
	DDX_Control(pDX, IDC_CONFIRM, m_CB_Confirm);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(KonflikteDlg, CDialog)
	//{{AFX_MSG_MAP(KonflikteDlg)
	ON_BN_CLICKED(IDC_CONFIRM, OnConfirm)
	ON_BN_CLICKED(IDC_ALLCONFIRM, OnAllconfirm)
	ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
	ON_WM_TIMER()
    ON_MESSAGE(WM_TABLE_RETURN_PRESSED, OnTableReturnPressed)
	ON_MESSAGE(WM_TABLE_SELCHANGE, OnTableSelChanged)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// KonflikteDlg message handlers

BOOL KonflikteDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
    SetTimer(39, (UINT)  120 * 60 * 1000, NULL); // cleanup konf


	ogDdx.Register(this, KONF_DELETE,	CString(""), CString(""),	KonfTableCf);
	ogDdx.Register(this, KONF_INSERT,	CString(""), CString(""),	KonfTableCf);
	ogDdx.Register(this, KONF_CHANGE,	CString(""), CString(""),	KonfTableCf);

	m_CB_Confirm.SetSecState(ogPrivList.GetStat("CONFLICTS_CONFIRM"));		
	m_CB_AllConfirm.SetSecState(ogPrivList.GetStat("CONFLICTS_CONFIRMALL"));		

	InitTable();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void KonflikteDlg::InitTable() 
{

	CRect rect;
    GetClientRect(&rect);
	rect.top += 50; 

    pomTable = new CCSTable;
	pomTable->SetSelectMode(LBS_MULTIPLESEL | LBS_EXTENDEDSEL);
	//pomTable->SetHeaderSpacing(0);


    rect.InflateRect(1, 1);     // hiding the CTable window border
    
	pomTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom);


 

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->ResetContent();
	
	TABLE_HEADER_COLUMN rlHeader;

	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Length = 1500; 
	rlHeader.Font = &ogCourier_Regular_10;
	rlHeader.Text = GetString(IDS_STRING1429);

	omHeaderDataArray.New(rlHeader);
	
	pomTable->SetHeaderFields(omHeaderDataArray);
	pomTable->SetDefaultSeparator();
	pomTable->SetTableEditable(false);
	omHeaderDataArray.DeleteAll();
	pomTable->DisplayTable();

}


void KonflikteDlg::Attention() 
{
	ShowWindow(SW_SHOW);
	SetWindowPos(&wndTopMost, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
	pomTable->ResetContent();
	bmAttention = true;
	bmIsActiv = true;
	m_CB_AllConfirm.ShowWindow(SW_SHOW);
	m_CB_Confirm.ShowWindow(SW_SHOW);
	SetWindowText(GetString(IMFK_ATTENTION));
	ChangeViewTo();
}


void KonflikteDlg::Conflicts() 
{
	ShowWindow(SW_SHOW);
	SetWindowPos(&wndTopMost, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
	pomTable->ResetContent();
	bmAttention = false;
	bmIsActiv = true;
	m_CB_AllConfirm.ShowWindow(SW_HIDE);
	m_CB_Confirm.ShowWindow(SW_HIDE);
	SetWindowText(GetString(IMFK_KONFLICTS));
	ChangeViewTo();
}





void KonflikteDlg::ChangeViewTo()
{
	DeleteAll();    
	MakeLines();
	UpdateDisplay();
}


void KonflikteDlg::DeleteAll()
{
	omLines.DeleteAll();
}


void KonflikteDlg::MakeLines()
{
	KONFENTRY *prlEntry;
	KONFDATA  *prlData;

	for(int i = ogKonflikte.omData.GetSize() - 1; i >= 0; i--)
	{
		prlEntry = &ogKonflikte.omData[i];

		for(int j = prlEntry->Data.GetSize() - 1; j >= 0; j--)
		{
			prlData = &prlEntry->Data[j];
//rkr			if(prlData->SetBy != 0 && ogKonflikte.IsBltKonfliktType(prlData->KonfId.Type))
			if(! ogKonflikte.IsBltKonfliktType(prlData->KonfId.Type))
				MakeLine(prlData->KonfId, prlData);
		}
	}
}


 
void KonflikteDlg::MakeLine(const KonfIdent &rrpKonfId, KONFDATA *prpData)
{
	if (prpData == NULL)
		return;

	KONF_LINEDATA rlLine;

	//if(prpData->SetBy != 0)
	{
		if((bmAttention && !prpData->Confirmed) || !bmAttention)
		{
			rlLine.KonfId = rrpKonfId;
			rlLine.Text = prpData->Text;
			rlLine.TimeOfConflict = prpData->TimeOfConflict;
			CreateLine(rlLine);
		}
	}

}



int KonflikteDlg::CreateLine(KONF_LINEDATA &rpKonf)
{
	if (FindLine(rpKonf.KonfId) >= 0) return -1;

    int ilLineCount = omLines.GetSize();

    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareKonf(&rpKonf, &omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]

    omLines.NewAt(ilLineno, rpKonf);

    return ilLineno;
}


int KonflikteDlg::CompareKonf(KONF_LINEDATA *prpKonf1, KONF_LINEDATA *prpKonf2)
{
	int	ilCompareResult = (prpKonf1->TimeOfConflict == prpKonf2->TimeOfConflict)? 0:
		(prpKonf1->TimeOfConflict > prpKonf2->TimeOfConflict)? 1: -1;
	return ilCompareResult;
}



void KonflikteDlg::UpdateDisplay()
{
	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;

  	pomTable->ResetContent();
    for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		rlColumnData.Text = omLines[ilLc].Text;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		pomTable->AddTextLine(olColList , &omLines[ilLc]);
		olColList.DeleteAll();
    }                                           
    pomTable->DisplayTable();
	CheckPostFlightPosDia(0,this);
}




void KonflikteDlg::OnConfirm() 
{

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	
	int ilItems[3000];
	int ilAnz = 3000;
	KONF_LINEDATA *prlLine;
	int ilAnzSel = 0;

	CListBox *polLB = pomTable->GetCTableListBox();
	if (polLB == NULL)
		return;

	ilAnz = MyGetSelItems(*polLB, ilAnz, ilItems);
	if(ilAnz <= 0)
	{
		//MessageBox(GetString(IDS_STRING968), GetString(ST_FEHLER), MB_ICONWARNING);
		return;
	}

	CPtrArray olKonfIds;
	for(int i = 0; i < ilAnz; i++)
	{
		prlLine = (KONF_LINEDATA *)pomTable->GetTextLineData(ilItems[i]);
		if(prlLine != NULL)
		{
			// collect records to confirm
			olKonfIds.Add((void *)&prlLine->KonfId);
		}
	}
	ogKonflikte.Confirm(olKonfIds);

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));


}

void KonflikteDlg::OnAllconfirm() 
{
	if (pomTable->GetLinesCount() > 0)
	{
		if (MessageBox(GetString(IDS_STRING1967), GetString(ST_FRAGE), MB_YESNO | MB_ICONQUESTION | MB_SYSTEMMODAL | MB_DEFBUTTON2) == IDYES)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			ogKonflikte.ConfirmAll();
			OnCancel();
	
			//pogButtonList->m_CB_Achtung.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		}
	}
}





void KonflikteDlg::OnCancel() 
{
	omLines.DeleteAll();
	bmIsActiv = false;
	ShowWindow(SW_HIDE);
	//CDialog::OnCancel();
}


static void KonfTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
	if (vpDataPointer == NULL)
		return;
	KonflikteDlg *polDlg = (KonflikteDlg *)popInstance;

	if (ipDDXType == KONF_DELETE)
		polDlg->DdxDeleteKonf(*(KonfIdent *)vpDataPointer);
	if (ipDDXType == KONF_INSERT)
		polDlg->DdxInsertKonf(*(KonfIdent *)vpDataPointer);
	if (ipDDXType == KONF_CHANGE)
		polDlg->DdxChangeKonf(*(KonfIdent *)vpDataPointer);

}

void KonflikteDlg::DdxChangeKonf(KonfIdent &rrpIdNotify)
{
	if(!bmIsActiv)
		return;

	DdxDeleteKonf(rrpIdNotify);

	DdxInsertKonf(rrpIdNotify);


/*
	KONFDATA  *prlData = NULL;
	KONF_LINEDATA *prlLine;
	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;

	int ilLineNo = FindLine(prlNotify->Urno, prlNotify->Type);

	if(ilLineNo >= 0)
	{
		prlData = ogKonflikte.GetKonflikt(prlNotify->Urno, prlNotify->Type);

		if(prlData != NULL)
		{
			prlLine = &omLines[ilLineNo];
			prlLine->Text = CString(prlData->Text);
			prlLine->TimeOfConflict = prlData->TimeOfConflict;

			rlColumnData.Text = omLines[ilLineNo].Text;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			pomTable->ChangeTextLine(ilLineNo, &olColList , &omLines[ilLineNo]);
			olColList.DeleteAll();
		}
	}
	*/
}


void KonflikteDlg::DdxInsertKonf(KonfIdent &rrpIdNotify)
{
	if(!bmIsActiv)
		return;

	KONFDATA  *prlData = NULL;
	KONF_LINEDATA rlLine;
	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;
	int ilLineNo;

	prlData = ogKonflikte.GetKonflikt(rrpIdNotify);

	if(prlData != NULL)
	{

		if(prlData->SetBy != 0)
		{
			if((bmAttention && !prlData->Confirmed) || !bmAttention)
			{
				rlLine.KonfId = rrpIdNotify;
				rlLine.Text = prlData->Text;
				rlLine.TimeOfConflict = prlData->TimeOfConflict;
				ilLineNo = CreateLine(rlLine);

				if (ilLineNo != -1)
				{
					rlColumnData.Text = rlLine.Text;
					olColList.NewAt(olColList.GetSize(), rlColumnData);
					pomTable->InsertTextLine(ilLineNo, olColList , &omLines[ilLineNo]);
					olColList.DeleteAll();
				}
			}
		}
	}
}


void KonflikteDlg::DdxDeleteKonf(KonfIdent &rrpIdNotify)
{
	if(!bmIsActiv)
		return;

	int ilLineNo = FindLine(rrpIdNotify);

	if(ilLineNo >= 0)
	{
		omLines.DeleteAt(ilLineNo);
		pomTable->DeleteTextLine(ilLineNo);
		CheckPostFlightPosDia(0,this);
	}
}


int KonflikteDlg::FindLine(const KonfIdent &rrpKonfId)
{
	for(int i = omLines.GetSize() - 1; i >= 0; i--)
	{
		if(omLines[i].KonfId == rrpKonfId)
			break;
	}
	return i;
}


LONG KonflikteDlg::OnTableLButtonDblclk( UINT wParam, LPARAM lParam)
{
	CCSTABLENOTIFY olNotify = *(CCSTABLENOTIFY*)lParam;

	KONF_LINEDATA *prlLine = (KONF_LINEDATA*)pomTable->GetTextLineData(wParam);

	if(prlLine == NULL)
		return 0L;

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));


	if (ogPosDiaFlightData.bmOffLine)
	{
		// only show resource data in offline mode
 		pogSeasonDlg->NewData(this, ogPosDiaFlightData.GetFlightByUrno(prlLine->KonfId.FUrno), bgGatPosLocal);
	}
	else
	{
		// get flight data
		char clFTyp;
		long llUrno = 0;
		long llRkey;
		CTime olTifad;
		bool blLocal;
		// try dia data
		DIAFLIGHTDATA *prlDiaFlight = ogPosDiaFlightData.GetFlightByUrno(prlLine->KonfId.FUrno);
		if (prlDiaFlight)
		{
			llUrno = prlDiaFlight->Urno;
			llRkey = prlDiaFlight->Rkey;
			clFTyp = prlDiaFlight->Ftyp[0];
			if (prlLine->KonfId.FPart == 'A')
				olTifad = prlDiaFlight->Tifa;
			else
				olTifad = prlDiaFlight->Tifd;
			blLocal = bgGatPosLocal;
		}
		else
		{
			// try daily data
			ROTATIONFLIGHTDATA *prlDailyFlight = ogRotationFlights.GetFlightByUrno(prlLine->KonfId.FUrno);
			if (prlDailyFlight)
			{
				llUrno = prlDailyFlight->Urno;
				llRkey = prlDailyFlight->Rkey;
				clFTyp = prlDailyFlight->Ftyp[0];
				if (prlLine->KonfId.FPart == 'A')
					olTifad = prlDailyFlight->Tifa;
				else
					olTifad = prlDailyFlight->Tifd;
				blLocal = bgDailyLocal;
			}
			else
			{
				// flight not found!!!
				CFPMSApp::DumpDebugLog("ERROR (KonflikteDlg::OnTableLButtonDblclk): FLIGHT NOT FOUND IN INTERNAL DATA!");
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
				return 0L;
			}
		}


		// display rotation mask!
 		CFPMSApp::ShowFlightRecordData(this, clFTyp, llUrno, llRkey, prlLine->KonfId.FPart, olTifad, blLocal); 



		/*
		// try to show flight in daily rotation dialog mask
		if (!pogRotationDlg->NewData(this, 0L, prlLine->KonfId.FUrno, "X", bgDailyLocal))
		{
 			// show towing dialog mask
			if(pogRotGroundDlg == NULL)
			{
				pogRotGroundDlg = new RotGroundDlg(this);
				pogRotGroundDlg->Create(IDD_ROTGROUND);
			}

			pogRotGroundDlg->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
			pogRotGroundDlg->NewData(this, 0L, prlLine->KonfId.FUrno, bgDailyLocal);
		}
		*/
	}
 
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	return 0L;
}



void KonflikteDlg::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent == 39)	
	{
		ogKonflikte.CleanUp();

	}
}


LONG KonflikteDlg::OnTableReturnPressed( UINT wParam, LPARAM lParam)
{
	OnTableLButtonDblclk(wParam, lParam); 
	return 0L;
}

LONG KonflikteDlg::OnTableSelChanged( UINT wParam, LPARAM lParam)
{
    int ilLineNo = pomTable->pomListBox->GetCurSel();    
	
	KONF_LINEDATA *prlTableLine = NULL;
	prlTableLine = (KONF_LINEDATA *)pomTable->GetTextLineData(ilLineNo);
	if (prlTableLine->KonfId.FUrno != 0)
		CheckPostFlightPosDia(prlTableLine->KonfId.FUrno,this);
	else
		CheckPostFlightPosDia(0,this);
	
	return 0L;
}
