#ifndef AFX_CCACOMMONDLG_H__183C77C2_B4D6_11D1_8154_0000B43C4B01__INCLUDED_
#define AFX_CCACOMMONDLG_H__183C77C2_B4D6_11D1_8154_0000B43C4B01__INCLUDED_

// CheckinModifyDlg.h : Header-Datei
//
#include <resource.h>		// Hauptsymbole
#include <CCSEdit.h>
#include <CedaCcaData.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CCcaCommonDlg 

class CCcaCommonDlg : public CDialog
{
// Konstruktion
public:
	CCcaCommonDlg(CWnd* pParent = NULL);
	~CCcaCommonDlg();


	void NewData(CCADATA *popCCA);


// Dialogfelddaten
	//{{AFX_DATA(CCcaCommonDlg)
	enum { IDD = IDD_CCA_COMMON_DLG };
	CCSEdit	m_CE_Disp;
	CCSEdit	m_CE_Terminal;
	CCSEdit	m_CE_Schalter;
	CCSEdit	m_CE_Fluggs;
	CCSEdit	m_CE_Beginn_d;
	CCSEdit	m_CE_Beginn_t;
	CCSEdit	m_CE_Ende_t;
	CCSEdit	m_CE_Ende_d;
	CCSEdit	m_CE_ActStartTime;
	CCSEdit	m_CE_ActStartDate;
	CCSEdit	m_CE_ActEndTime;
	CCSEdit	m_CE_ActEndDate;
	CCSEdit	m_CE_Rem;
	CString	m_Beginn_t;
	CString	m_Beginn_d;
	CString	m_Ende_d;
	CString	m_Ende_t;
	CString m_ActStartTime;
	CString m_ActStartDate;
	CString m_ActEndTime;
	CString m_ActEndDate;
	CString	m_Rem;
	CString	m_Fluggs;
	CString	m_Schalter;
	CString	m_Terminal;
	CString	m_Disp;
	CButton m_CB_FidsRemList;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CCcaCommonDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CCcaCommonDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnFidsRemList();
	afx_msg void OnClose();
	afx_msg LONG OnEditChanged( UINT wParam, LPARAM lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	CCADATA omCCA;


	CedaCcaData omCcaData;	

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_CCACOMMONDLG_H__183C77C2_B4D6_11D1_8154_0000B43C4B01__INCLUDED_
