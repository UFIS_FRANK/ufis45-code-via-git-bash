//	Liste aller Messeflüge
#ifndef __ALLEMESSEFLUEGETABLEVIEWER_H__
#define __ALLEMESSEFLUEGETABLEVIEWER_H__

#include <stdafx.h>
#include <RotationDlgCedaFlightData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct ALLEMESSEFLUEGETABLE_LINEDATA
{
	long AUrno;
	long ARkey;
	CString		AStev;
	CString		AFlno;
	CTime		AStoa;
	CString		AOrg3;
	CString		AVia3;
	CString		AAct;
	//CString		AAct5;
	CString		AGta1;
	CString		APsta;
	CString		ATtyp;

	long DUrno;
	long DRkey;
	CString		DStev;
	CString		DFlno;
	CTime		DStod;
	CString		DTtyp;
	CString		DDes3;
	CString		DVia3;
	CString		DAct;
	//CString		DAct5;
	CString		DGtd1;
	CString		DPstd;
	CString		DWro1;
	CString		Date;
	CString		DCkic1;// Checkin-Schalter
	CString		DCkic2;// Checkin-Schalter

};

/////////////////////////////////////////////////////////////////////////////
// AllemessefluegeTableViewer

class AllemessefluegeTableViewer : public CViewer
{
// Constructions
public:
    AllemessefluegeTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo = NULL, char *pcpSelect = NULL);
    ~AllemessefluegeTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
    void MakeLines();
	int  MakeLine(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight);
	void MakeLineData(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight, ALLEMESSEFLUEGETABLE_LINEDATA &rpLine);
	void MakeColList(ALLEMESSEFLUEGETABLE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList);
	int  CompareAllemessefluege(ALLEMESSEFLUEGETABLE_LINEDATA *prpAllemessefluege1, ALLEMESSEFLUEGETABLE_LINEDATA *prpAllemessefluege2);
	int  CompareFlight(ALLEMESSEFLUEGETABLE_LINEDATA *prpFlight1, ALLEMESSEFLUEGETABLE_LINEDATA *prpFlight2);
	bool PrintFrameBottom(void);
	int  GetVia(CCSPtrArray<VIADATA> *opVias, ROTATIONDLGFLIGHTDATA *prpFlight);
// Operations
public:
	void DeleteAll(void);
	int CreateLine(ALLEMESSEFLUEGETABLE_LINEDATA &prpAllemessefluege);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay(void);
	void DrawHeader(void);
	bool PrintPlanToFile(char *pcpDefPath);


// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
	int imDay;
// Attributes
private:
    CCSTable *pomTable;
	CCSPtrArray<ROTATIONDLGFLIGHTDATA> *pomData;
	CCSPtrArray<CCADATA> omDCins;
	CCSPtrArray<CCADATA> omDCinsSave;
public:
    CCSPtrArray<ALLEMESSEFLUEGETABLE_LINEDATA> omLines;
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void GetHeader(void); 
	void PrintTableView(void);
	bool PrintTableLine(ALLEMESSEFLUEGETABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader(CTime popTifd);
	CCSPtrArray <TABLE_HEADER_COLUMN> omPrintHeadHeaderArray;
	CCSPrint *pomPrint;
	CString omTableName;
	CString omFileName;
	CStringArray omSort;
	char *pcmInfo;
	char *pcmSelect;
	CString omFooterName;
	char cmOldDate[12];
};

#endif //__ALLEMESSEFLUEGETABLEVIEWER_H__
