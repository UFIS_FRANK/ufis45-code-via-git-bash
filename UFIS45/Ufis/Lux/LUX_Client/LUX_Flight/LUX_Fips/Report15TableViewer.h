#ifndef __REPORT15TABLEVIEWER_H__
#define __REPORT15TABLEVIEWER_H__

#include <stdafx.h>
#include <SeasonCedaFlightData.h>
#include <Table.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct REPORT15TABLE_LINEDATA
{
	long		Urno; 	// Eindeutige Datensatz-Nr.
	CString		Mo;		// Act3 gesammelt f�r eine Woche
	CString		Tu;
	CString		We;
	CString		Th;
	CString		Fr;
	CString		Sa;
	CString		Su;

};


struct URNOKEYLIST
{
	CCSPtrArray<REPORT15TABLE_LINEDATA> Rotation;

}; 

/////////////////////////////////////////////////////////////////////////////
// Report15TableViewer

class Report15TableViewer : public CViewer
{
// Constructions
public:
    Report15TableViewer(char *pcpInfo = NULL, char *pcpSelect = NULL);
    ~Report15TableViewer();

    void Attach(CTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
    void MakeLines(void);
	void MakeLine(long AUrno, long DUrno);	
	int CreateLine(REPORT15TABLE_LINEDATA &rpLine, long lpUrno);
	int  CompareFlight(long lpNewUrno, long lpCmpUrno);

	void GetHeader(void);
	void GetPrintHeader(void);

// Operations
public:
	void DeleteAll(void);
	void DeleteLine(int ipLineno);
	void UpdateDisplay(void);
	void PrintTableView(void);
	bool PrintTableHeader(void);
	bool PrintPlanToFile(char *pcpDefPath);
	bool PrintTableLine(REPORT15TABLE_LINEDATA *prpLine, bool bpLastLine);
	CCSPtrArray <TABLE_HEADER_COLUMN> omPrintHeadHeaderArray;
// Attributes
private:
    CTable *pomTable;
	CString omFromStr;
	CString omToStr;
///////
//Print 
	CCSPrint *pomPrint;
//	CString omTableName;
	CString omFooterName;
	char *pcmInfo;
	char *pcmSelect;


public:
    CCSPtrArray<REPORT15TABLE_LINEDATA> omLines;
	CMapStringToPtr omLineKeyMap;
	CString omTableName;
	CString omFileName;

};

#endif //__REPORT15TABLEVIEWER_H__
