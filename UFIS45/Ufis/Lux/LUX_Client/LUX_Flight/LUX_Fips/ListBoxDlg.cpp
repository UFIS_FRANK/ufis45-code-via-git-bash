// ListBoxDlg.cpp : implementation file
//

#include <stdafx.h>
#include <fpms.h>
#include <ListBoxDlg.h>
#include <CCSGlobl.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif






/////////////////////////////////////////////////////////////////////////////
// ListBoxDlg dialog


ListBoxDlg::ListBoxDlg(CWnd* pParent, const CString &ropHeader, const CString &ropText, int *pipSel)
	: CDialog(ListBoxDlg::IDD, pParent)
{
	omHeader = ropHeader;
	omText = ropText;
	pimSel = pipSel;
	pomParent = pParent;
}


ListBoxDlg::~ListBoxDlg()
{
}



void ListBoxDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ListBoxDlg)
	DDX_Control(pDX, IDOK, m_CB_OK);
	DDX_Control(pDX, IDCANCEL, m_CB_Cancel);
	DDX_Control(pDX, IDC_HEADER, m_CS_Text);
	DDX_Control(pDX, IDC_LIST, m_CL_List);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ListBoxDlg, CDialog)
	//{{AFX_MSG_MAP(ListBoxDlg)
	ON_WM_SIZE()
	ON_LBN_DBLCLK(IDC_LIST, OnDblclkList)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ListBoxDlg message handlers


bool ListBoxDlg::AddItem(const CString &ropStr)
{
	olListBoxItems.Add(ropStr);	
	return true;
}


bool ListBoxDlg::SetSize(const CSize &ropSize)
{
	omSize = ropSize;
	return true;
}



void ListBoxDlg::OnOK() 
{
	int ilIndex = m_CL_List.GetCurSel();
	if(ilIndex != LB_ERR && pimSel != NULL)
	{
		*pimSel = ilIndex;
	}
	CDialog::OnOK();
}


BOOL ListBoxDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();


	SetWindowText(omHeader);

	m_CS_Text.SetWindowText(omText);

	m_CL_List.ResetContent( );
	m_CL_List.SetFont(&ogCourier_Regular_10);

	for (int i = 0; i < olListBoxItems.GetSize(); i++)
	{
		m_CL_List.AddString(olListBoxItems[i]);
	}

	if (pimSel != NULL)
	{
		m_CL_List.SetCurSel(*pimSel);
	}

	if (omSize != CSize())
	{
		SetWindowPos(NULL, 0, 0, omSize.cx, omSize.cy, SWP_NOMOVE | SWP_NOZORDER);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void ListBoxDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	CRect olClient;
	GetClientRect(olClient);
	ClientToScreen(olClient);

	CRect olRect;
	int ilCX;
	int ilCY;
	int ilX;
	int ilY;

	if(m_hWnd != NULL)
	{
		if(m_CS_Text.m_hWnd != NULL)
		{
			m_CS_Text.GetWindowRect(olRect);

			ilCX = olClient.right - olClient.left - 21;
			ilCY = olRect.bottom - olRect.top;

			m_CS_Text.SetWindowPos( NULL, 0, 0,  ilCX, ilCY, SWP_NOZORDER | SWP_NOMOVE | SWP_SHOWWINDOW );
		}
		
		if(m_CL_List.m_hWnd != NULL)
		{
			m_CL_List.GetWindowRect(olRect);

			ilCX = olClient.right - olClient.left - 18;
			ilCY = cy - 80;
	
			m_CL_List.SetWindowPos( NULL, 0, 0,  ilCX, ilCY, SWP_NOZORDER | SWP_NOMOVE | SWP_SHOWWINDOW );
		}

		if(m_CB_OK.m_hWnd != NULL)
		{
			m_CB_OK.GetWindowRect(olRect);

			ilY = cy - 30;
			ilX = (cx / 2) - 17 - (olRect.right - olRect.left);

			m_CB_OK.SetWindowPos( NULL, ilX, ilY, 0 ,0 ,SWP_NOZORDER | SWP_NOSIZE | SWP_SHOWWINDOW );
		}

		if(m_CB_Cancel.m_hWnd != NULL)
		{
			m_CB_Cancel.GetWindowRect(olRect);

			ilY = cy - 30;
			ilX = (cx / 2) + 17;

			m_CB_Cancel.SetWindowPos( NULL, ilX, ilY, 0 ,0 ,SWP_NOZORDER | SWP_NOSIZE | SWP_SHOWWINDOW );
		}
	}
}

void ListBoxDlg::OnDblclkList() 
{
	OnOK();	
}

