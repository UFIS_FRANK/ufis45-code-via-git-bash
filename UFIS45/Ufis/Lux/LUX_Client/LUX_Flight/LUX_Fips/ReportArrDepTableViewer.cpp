// ReportArrDepTableViewer.cpp : implementation file
// 
// according to arrival/departure flight each day


#include <stdafx.h>
#include <ReportArrDepTableViewer.h>
#include <CcsGlobl.h>
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include <BasicData.h>

#include <ReportXXXCedaData.h>
#include <CedaBasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// ReportArrDepTableViewer
//

int ReportArrDepTableViewer::GetVia(CCSPtrArray<VIADATA> *opVias, ROTATIONDLGFLIGHTDATA *prpFlight)
{
	opVias->DeleteAll();

	if(prpFlight == NULL)
		return 0;
	if(strlen(prpFlight->Vial) == 0)
		return 0;

	CString olVias(prpFlight->Vial);
	CString olFids;
	CString olApc3;
	CString olApc4;

	VIADATA *prlVia;

	while(olVias.IsEmpty() != TRUE)
	{
			prlVia = new VIADATA;
			opVias->Add(prlVia);

			if(olVias.GetLength() < 120)
			{
				olVias += "                                                                                                                             ";
				olVias = olVias.Left(120);
			}


			olApc3 = olVias.Mid(1,3);
			olApc3.TrimLeft();
			sprintf(prlVia->Apc3, olApc3);

			if(olVias.GetLength() >= 120)
				olVias = olVias.Right(olVias.GetLength() - 120);

	}
	return opVias->GetSize();
}



ReportArrDepTableViewer::ReportArrDepTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, 
												 int ipArrDep, char *pcpInfo, char *pcpSelect)
{

	pomData = popData;
	pcmInfo = pcpInfo;
	pcmSelect = pcpSelect;
	
	imArrDep = ipArrDep;

	bmIsFromSearch = false;
    pomTable = NULL;
	imFlightCount = 0;

}

ReportArrDepTableViewer::~ReportArrDepTableViewer()
{
	omPrintHeadHeaderArray.DeleteAll();  // BWi
    DeleteAll();
}


void ReportArrDepTableViewer::SetParentDlg(CDialog* ppParentDlg)
{

	pomParentDlg = ppParentDlg;

}


void ReportArrDepTableViewer::Attach(CCSTable *popTable)
{

    pomTable = popTable;

}


int ReportArrDepTableViewer::GetFlightCount()
{

	return imFlightCount;

}


void ReportArrDepTableViewer::ChangeViewTo(const char *pcpViewName)
{

    pomTable->ResetContent();
    DeleteAll();    
    MakeLines();

	UpdateDisplay();
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

}


/////////////////////////////////////////////////////////////////////////////
// ReportArrDepTableViewer -- code specific to this class

void ReportArrDepTableViewer::MakeLines()
{
	ROTATIONDLGFLIGHTDATA *prlAFlight;
	ROTATIONDLGFLIGHTDATA *prlDFlight;
	ROTATIONDLGFLIGHTDATA *prlNextFlight = NULL;
	ROTATIONDLGFLIGHTDATA *prlFlight;
	int ilLineNo;

	bool blRDeparture = false;

	int ilFlightCount = pomData->GetSize();

	for (int ilLc = 0; ilLc < ilFlightCount; ilLc++)
	{
		prlFlight = &(*pomData)[ilLc];

		if(ilLc + 1 < ilFlightCount)
			prlNextFlight = &(*pomData)[ilLc + 1];	
		else
			prlNextFlight = NULL;	

		//Arrival
		if((strcmp(prlFlight->Org3, pcgHome) != 0) && (strcmp(prlFlight->Des3, pcgHome) == 0))
		{
			prlAFlight = prlFlight;
			prlDFlight = NULL;
			if(prlNextFlight != NULL)
			{
				if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
				{
					prlDFlight = prlNextFlight;

					if((strcmp(prlNextFlight->Org3, pcgHome) == 0) && (strcmp(prlNextFlight->Des3, pcgHome) != 0))
					{
						ilLc++;
					}
					else
						blRDeparture = true;
				}
			}
		}
		else
		{
			// Departure
			if((strcmp(prlFlight->Org3, pcgHome) == 0) && (strcmp(prlFlight->Des3, pcgHome) != 0))
			{
				prlAFlight = NULL;
				prlDFlight = prlFlight;
				blRDeparture = false;
			}
			else
			{
				//Turnaround
				if((strcmp(prlFlight->Org3, pcgHome) == 0) && (strcmp(prlFlight->Des3, pcgHome) == 0))
				{
					if(blRDeparture)
					{
						blRDeparture = false;
						prlAFlight = prlFlight;
						prlDFlight = NULL;
						if(prlNextFlight != NULL)
						{
							if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
							{
								prlDFlight = prlNextFlight;
								if((strcmp(prlNextFlight->Org3, pcgHome) == 0) && (strcmp(prlNextFlight->Des3, pcgHome) != 0))
								{
									ilLc++;
								}
								else
									blRDeparture = true;
							}
						}
					}
					else
					{
						prlAFlight		= prlFlight;
						prlDFlight		= prlFlight;
						if(prlNextFlight != NULL)
						{
							if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
							{
								ilLc--;
								prlAFlight	 = NULL;
								blRDeparture = true;
							}
						}
					}
				}
			}
		}
		ilLineNo = MakeLine(prlAFlight, prlDFlight);

	}


}

		

int ReportArrDepTableViewer::MakeLine(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight)
{
    SAMEARRDEPTABLE_LINEDATA rlLine;

	if((prpAFlight != NULL) && (prpDFlight != NULL) )
	{
		imFlightCount++;
		if (imArrDep == 0)
		{
			MakeLineData(prpAFlight, NULL, rlLine);
			return CreateLine(rlLine);
		}
		if (imArrDep == 1)
		{
			MakeLineData(NULL, prpDFlight, rlLine);
			return CreateLine(rlLine);
		}
	}
	if (prpAFlight != NULL)
	{
		if (imArrDep == 0)
		{
			imFlightCount++;
			MakeLineData(prpAFlight, NULL, rlLine);
			return CreateLine(rlLine);
		}
	}
	if (prpDFlight != NULL)
	{
		if (imArrDep == 1)
		{
			imFlightCount++;
			MakeLineData(NULL, prpDFlight, rlLine);
			return CreateLine(rlLine);
		}
	}
	return -1;
}




void ReportArrDepTableViewer::MakeLineData(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight, SAMEARRDEPTABLE_LINEDATA &rpLine)
{

	CString olStr;
	if(prpAFlight != NULL)
	{
		rpLine.Adid = "A";
		rpLine.AUrno = prpAFlight->Urno;
		rpLine.ARkey = prpAFlight->Rkey;
		rpLine.AFlno = CString(prpAFlight->Flno);
		rpLine.AStoa = prpAFlight->Stoa; 
		if(bgReportLocal)
			ogBasicData.UtcToLocal(rpLine.AStoa);
		rpLine.ADate = prpAFlight->Stoa.Format("%d.%m.%y");/*DateToHourDivString(prpAFlight->Onbl, prpAFlight->Stoa);*/
		rpLine.AOrg3 = CString(prpAFlight->Org3);
		rpLine.AOrg4 = CString(prpAFlight->Org4);
		rpLine.ATtyp = CString(prpAFlight->Ttyp);
		rpLine.AAct  = CString(prpAFlight->Act3);
		rpLine.ALand = CTime(prpAFlight->Land); 
		if(bgReportLocal) 
			ogBasicData.UtcToLocal(rpLine.ALand);

		rpLine.ABlt1 = CString(prpAFlight->Blt1);
		rpLine.APsta = CString(prpAFlight->Psta);
		rpLine.ARegn = CString(prpAFlight->Regn);
		rpLine.AGta1 = CString(prpAFlight->Gta1);
		
		CCSPtrArray<VIADATA> opVias;
		int ilViaCnt = GetVia(&opVias, prpAFlight);

		if(opVias.GetSize() == 0)
			rpLine.AVia3 = "";
		else
		{
			rpLine.AVia3 =  opVias[ilViaCnt-1].Apc3;	// letzter Via vor Homeairport = letzte Zeile in Vial
		}
		opVias.DeleteAll();
	}
	else
	{
		rpLine.AUrno =  0;
		rpLine.ARkey =  0;
		rpLine.AFlno = "";
		rpLine.AStoa = TIMENULL; 
		rpLine.ALand = TIMENULL; 
		rpLine.AEtai = TIMENULL; 
		rpLine.ATtyp = "";
		rpLine.ADate = ""; 
		rpLine.AOrg3 = "";
		rpLine.AOrg4 = "";
		rpLine.AVia3 = "";
		rpLine.AAct  = "";
		rpLine.AGta1 = "";
		rpLine.ABlt1 = "";
		rpLine.ARegn = "";
		rpLine.APsta = "";
	}


	if(prpDFlight != NULL)
	{
		rpLine.Adid = "D";

		rpLine.DUrno = prpDFlight->Urno;
		rpLine.DRkey = prpDFlight->Rkey;
		rpLine.DFlno = CString(prpDFlight->Flno); 
		rpLine.DDes3 = CString(prpDFlight->Des3);
		rpLine.DDes4 = CString(prpDFlight->Des4);
		rpLine.DTtyp = CString(prpDFlight->Ttyp);
		rpLine.DAct  = CString(prpDFlight->Act3);

		rpLine.DAirb = CTime(prpDFlight->Airb);
		if(bgReportLocal) 
			ogBasicData.UtcToLocal(rpLine.DAirb);
		rpLine.DEtdi = CTime(prpDFlight->Etdi);
		if(bgReportLocal) 
			ogBasicData.UtcToLocal(rpLine.DEtdi);
		rpLine.DStod = prpDFlight->Stod;
		if(bgReportLocal)
			ogBasicData.UtcToLocal(rpLine.DStod);
		rpLine.DDate = prpDFlight->Stod.Format("%d.%m.%y");
		rpLine.DVian = prpDFlight->Vian;

		if (rpLine.AAct.IsEmpty())
			rpLine.AAct = rpLine.DAct;

		rpLine.DGtd1 = CString(prpDFlight->Gtd1);
		rpLine.DPstd = CString(prpDFlight->Pstd);
		rpLine.DRegn = CString(prpDFlight->Regn);

		char pclUrno[32];
/*		CedaCcaData olCcaData;
		CString olSelection = CString("WHERE FLNU IN (");
		olSelection += CString(ltoa(prpDFlight->Urno, pclUrno, 10));
		olSelection += CString(")");
		olCcaData.Read(olSelection.GetBuffer(0));
		
		int ilCompare;
		int ilLow;
		int ilHigh;
		int ilCcaSize = olCcaData.omData.GetSize() ;
		if (ilCcaSize > 0)
		{
			int ilLow  = atoi(olCcaData.omData[0].Ckic);
			int ilHigh = atoi(olCcaData.omData[0].Ckic);
		}

		for(int i = 0; i < ilCcaSize; i++)
		{
			if( prpDFlight->Urno == olCcaData.omData[i].Flnu )
			{

				ilCompare = atoi(olCcaData.omData[i].Ckic);
				if (ilCompare > ilHigh)
					ilHigh = ilCompare;
				if (ilCompare < ilLow)
					ilLow = ilCompare;
			}

			if (ilHigh > ilLow)
			{
				CString olCkic;
				olCkic.Format("%i-%i", ilLow, ilHigh);
				rpLine.DCkic = olCkic;
			}
			else
			{
				rpLine.DCkic = olCcaData.omData[0].Ckic;
			}

		}

*/
		ReportXXXCedaData olReportXXXCedaData("FLNU", "CKIC", "CCA", true);
		olReportXXXCedaData.ReadCleartext(CString(ltoa(prpDFlight->Urno, pclUrno, 10)));

		// groessten und kleinesten Wert extrahieren
		int ilCompare;
		if (olReportXXXCedaData.omSameFlnu.GetSize() > 0)
		{
			int ilLow  = atoi(olReportXXXCedaData.omSameFlnu[0]);
			int ilHigh = atoi(olReportXXXCedaData.omSameFlnu[0]);

			for (int i = 0; i < olReportXXXCedaData.omSameFlnu.GetSize(); i++)
			{
				ilCompare = atoi(olReportXXXCedaData.omSameFlnu[i]);
				if (ilCompare > ilHigh)
					ilHigh = ilCompare;
				if (ilCompare < ilLow)
					ilLow = ilCompare;
			}

			if (ilHigh > ilLow)
			{
				CString olCkic;
				olCkic.Format("%i-%i", ilLow, ilHigh);
				rpLine.DCkic = olCkic;
			}
			else
			{
				rpLine.DCkic = olReportXXXCedaData.omSameFlnu[0];
			}
		}


		CCSPtrArray<VIADATA> opVias;
		int ilViaCnt = GetVia(&opVias, prpDFlight);

		if(opVias.GetSize() == 0)
			rpLine.DVia3 = "";
		else
			rpLine.DVia3 =  opVias[0].Apc3;	// erster Via nach Homeairport = erste Zeile in Vial
		opVias.DeleteAll();

	
	}
	else
	{
		rpLine.DUrno =  0;
		rpLine.DRkey =  0;
		rpLine.DEtdi = TIMENULL; 
		rpLine.DAirb = TIMENULL; 
		rpLine.DStod = TIMENULL; 
		rpLine.DFlno = "";
		rpLine.DDate = ""; 
		rpLine.DDes3 = "";
		rpLine.DDes4 = "";
		rpLine.DTtyp = "";
		rpLine.DVia3 = "";
		rpLine.DAct  = "";
		rpLine.DRegn = "";
		rpLine.DGtd1 = "";
	}

    return;

}



int ReportArrDepTableViewer::CreateLine(SAMEARRDEPTABLE_LINEDATA &rpLine)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareFlight(&rpLine, &omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]

    omLines.NewAt(ilLineno, rpLine);
	return ilLineno;
}


void ReportArrDepTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}



void ReportArrDepTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}


/////////////////////////////////////////////////////////////////////////////
// ReportArrDepTableViewer - display drawing routine



// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void ReportArrDepTableViewer::UpdateDisplay()
{
	pomTable->ResetContent();

	DrawHeader();

	SAMEARRDEPTABLE_LINEDATA *prlLine;
    
	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		int ilColumnNo = 0;
		prlLine = &omLines[ilLc];
		CCSPtrArray<TABLE_COLUMN> olColList;
	
		MakeColList(prlLine, olColList);

		pomTable->AddTextLine(olColList, (void*)(prlLine));
		olColList.DeleteAll();
	}


    pomTable->DisplayTable();
}




void ReportArrDepTableViewer::DrawHeader()
{
	int  ilTotalLines = 0;
	bool blNewLogicLine = false;

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);
	pomTable->ResetContent();
	
	TABLE_HEADER_COLUMN *prlHeader[9];

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 67; 
	prlHeader[0]->Font = &ogCourier_Bold_10;
	prlHeader[0]->Text = GetString(IDS_STRING1078);//FLIGHT

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 60; 
	prlHeader[1]->Font = &ogCourier_Bold_10;
	if (imArrDep == 0)
		prlHeader[1]->Text = GetString(IDS_STRING298);// ORG
	if (imArrDep == 1)
		prlHeader[1]->Text = GetString(IDS_STRING315);// DES

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 45; 
	prlHeader[2]->Font = &ogCourier_Bold_10;
	if (imArrDep == 0)
		prlHeader[2]->Text = GetString(IDS_STRING323);//STA
	if (imArrDep == 1)
		prlHeader[2]->Text = GetString(IDS_STRING316);//STD

	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 45; 
	prlHeader[3]->Font = &ogCourier_Bold_10;
	if (imArrDep == 0)
		prlHeader[3]->Text = GetString(IDS_STRING304);//ATA
	if (imArrDep == 1)
		prlHeader[3]->Text = GetString(IDS_STRING321);//ATD

	prlHeader[4] = new TABLE_HEADER_COLUMN;
	prlHeader[4]->Alignment = COLALIGN_CENTER;
	prlHeader[4]->Length = 70; 
	prlHeader[4]->Font = &ogCourier_Bold_10;
	prlHeader[4]->Text = GetString(IDS_STRING1569);//AC-Type

	prlHeader[5] = new TABLE_HEADER_COLUMN;
	prlHeader[5]->Alignment = COLALIGN_CENTER;
	prlHeader[5]->Length = 45; 
	prlHeader[5]->Font = &ogCourier_Bold_10;
	prlHeader[5]->Text = GetString(IDS_STRING310);//REGN

	prlHeader[6] = new TABLE_HEADER_COLUMN;
	prlHeader[6]->Alignment = COLALIGN_CENTER;
	if (imArrDep == 0)
		prlHeader[6]->Length = 50;
	if (imArrDep == 1)
		prlHeader[6]->Length = 70;	
	prlHeader[6]->Font = &ogCourier_Bold_10;
	if (imArrDep == 0)
		prlHeader[6]->Text = GetString(IDS_STRING308);//POS
	if (imArrDep == 1)
		prlHeader[6]->Text = GetString(IDS_STRING1641);//Cki-Counter

	prlHeader[7] = new TABLE_HEADER_COLUMN;
	prlHeader[7]->Alignment = COLALIGN_CENTER;
	prlHeader[7]->Length = 45; 
	prlHeader[7]->Font = &ogCourier_Bold_10;
	prlHeader[7]->Text = GetString(IDS_STRING1406);//Gate

	prlHeader[8] = new TABLE_HEADER_COLUMN;
	prlHeader[8]->Alignment = COLALIGN_CENTER;
	prlHeader[8]->Length = 45; 
	prlHeader[8]->Font = &ogCourier_Bold_10;
	if (imArrDep == 0)
		prlHeader[8]->Text = GetString(IDS_STRING1151);//Belt
	if (imArrDep == 1)
		prlHeader[8]->Text = GetString(IDS_STRING308);//Pos


	for(int ili = 0; ili < 9; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}

	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();

}



void ReportArrDepTableViewer::MakeColList(SAMEARRDEPTABLE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList)
{

	TABLE_COLUMN rlColumnData;

	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogCourier_Regular_9;

	rlColumnData.Alignment = COLALIGN_LEFT;
	if (imArrDep == 0)
		rlColumnData.Text = prlLine->AFlno;
	if (imArrDep == 1)
		rlColumnData.Text = prlLine->DFlno;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if (imArrDep == 0)
		rlColumnData.Text = prlLine->AOrg3;
	if (imArrDep == 1)
		rlColumnData.Text = prlLine->DDes3;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if (imArrDep == 0)
		rlColumnData.Text = prlLine->AStoa.Format("%H:%M");
	if (imArrDep == 1)
		rlColumnData.Text = prlLine->DStod.Format("%H:%M");
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if (imArrDep == 0)
		rlColumnData.Text = prlLine->ALand.Format("%H:%M");
	if (imArrDep == 1)
		rlColumnData.Text = prlLine->DAirb.Format("%H:%M");
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if (imArrDep == 0)
		rlColumnData.Text = prlLine->AAct;
	if (imArrDep == 1)
		rlColumnData.Text = prlLine->DAct;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if (imArrDep == 0)
		rlColumnData.Text = prlLine->ARegn;
	if (imArrDep == 1)
		rlColumnData.Text = prlLine->DRegn;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if (imArrDep == 0)
		rlColumnData.Text = prlLine->APsta;
	if (imArrDep == 1)
		rlColumnData.Text = prlLine->DCkic;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if (imArrDep == 0)
		rlColumnData.Text = prlLine->AGta1;
	if (imArrDep == 1)
		rlColumnData.Text = prlLine->DGtd1;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if (imArrDep == 0)
		rlColumnData.Text = prlLine->ABlt1;
	if (imArrDep == 1)
		rlColumnData.Text = prlLine->DPstd;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

}




int ReportArrDepTableViewer::CompareFlight(SAMEARRDEPTABLE_LINEDATA *prpLine1, SAMEARRDEPTABLE_LINEDATA *prpLine2)
{
	int	ilCompareResult;
	CTime olTime1;
	CTime olTime2;

	if (prpLine1->AUrno != 0)
	{
		olTime1 = prpLine1->AStoa;
		olTime2 = prpLine2->AStoa;
	}
	else
	{
		olTime1 = prpLine1->DStod;
		olTime2 = prpLine2->DStod;
	}

 
	ilCompareResult = (olTime1 == olTime2)? 0:
			(olTime1 > olTime2)? 1: -1;

	 return ilCompareResult;
}



//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------


void ReportArrDepTableViewer::GetHeader()
{
	
	omPrintHeadHeaderArray.DeleteAll();  // BWi

	TABLE_HEADER_COLUMN *prlHeader[9];

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 67; 
	prlHeader[0]->Font = &ogCourier_Bold_10;
	prlHeader[0]->Text = GetString(IDS_STRING1078);//FLIGHT

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 60; 
	prlHeader[1]->Font = &ogCourier_Bold_10;
	if (imArrDep == 0)
		prlHeader[1]->Text = GetString(IDS_STRING298);// ORG
	if (imArrDep == 1)
		prlHeader[1]->Text = GetString(IDS_STRING315);// DES

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 45; 
	prlHeader[2]->Font = &ogCourier_Bold_10;
	if (imArrDep == 0)
		prlHeader[2]->Text = GetString(IDS_STRING323);//STA
	if (imArrDep == 1)
		prlHeader[2]->Text = GetString(IDS_STRING316);//STD

	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 45; 
	prlHeader[3]->Font = &ogCourier_Bold_10;
	if (imArrDep == 0)
		prlHeader[3]->Text = GetString(IDS_STRING304);//ATA
	if (imArrDep == 1)
		prlHeader[3]->Text = GetString(IDS_STRING321);//ATD

	prlHeader[4] = new TABLE_HEADER_COLUMN;
	prlHeader[4]->Alignment = COLALIGN_CENTER;
	prlHeader[4]->Length = 70; 
	prlHeader[4]->Font = &ogCourier_Bold_10;
	prlHeader[4]->Text = GetString(IDS_STRING1569);//AC-Type

	prlHeader[5] = new TABLE_HEADER_COLUMN;
	prlHeader[5]->Alignment = COLALIGN_CENTER;
	prlHeader[5]->Length = 45; 
	prlHeader[5]->Font = &ogCourier_Bold_10;
	prlHeader[5]->Text = GetString(IDS_STRING310);//REGN

	prlHeader[6] = new TABLE_HEADER_COLUMN;
	prlHeader[6]->Alignment = COLALIGN_CENTER;
	if (imArrDep == 0)
		prlHeader[6]->Length = 50;
	if (imArrDep == 1)
		prlHeader[6]->Length = 100;	
	prlHeader[6]->Font = &ogCourier_Bold_10;
	if (imArrDep == 0)
		prlHeader[6]->Text = GetString(IDS_STRING308);//POS
	if (imArrDep == 1)
		prlHeader[6]->Text = GetString(IDS_STRING1641);//Cki-Counter

	prlHeader[7] = new TABLE_HEADER_COLUMN;
	prlHeader[7]->Alignment = COLALIGN_CENTER;
	prlHeader[7]->Length = 45; 
	prlHeader[7]->Font = &ogCourier_Bold_10;
	prlHeader[7]->Text = GetString(IDS_STRING1406);//Gate

	prlHeader[8] = new TABLE_HEADER_COLUMN;
	prlHeader[8]->Alignment = COLALIGN_CENTER;
	prlHeader[8]->Length = 45; 
	prlHeader[8]->Font = &ogCourier_Bold_10;
	if (imArrDep == 0)
		prlHeader[8]->Text = GetString(IDS_STRING1151);//Belt
	if (imArrDep == 1)
		prlHeader[8]->Text = GetString(IDS_STRING308);//Pos

	for(int ili = 0; ili < 9; ili++)
	{
		omPrintHeadHeaderArray.Add( prlHeader[ili]);
	}

}




void ReportArrDepTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	char pclFooter[200];

	sprintf(pclFooter, GetString(IDS_STRING1630));

	omFooterName = pclFooter;
	CString olTableName = GetString(IDS_STRING1631);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET
	pomPrint = new CCSPrint(pomParentDlg,ilOrientation,45);

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = olTableName;	
			//rlDocInfo.lpszDocName = TABLENAME;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = imFlightCount;//omLines.GetSize();  
			olFooter1.Format(GetString(IDS_STRING1445), ilLines,omFooterName);

			GetHeader();

			if (ilLines == 0)
				PrintTableHeader(); // Drucken leerer Tabelle
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}

		omPrintHeadHeaderArray.DeleteAll();
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		delete pomPrint;
		pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool ReportArrDepTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;
	double dgCCSPrintFactor = 2.7 ;
	char pclHeader[200];

	sprintf(pclHeader, GetString(IDS_STRING1631), pcmInfo, imFlightCount);
	CString olTableName(pclHeader);

	pomPrint->PrintUIFHeader(omTableName,CString(pclHeader),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omPrintHeadHeaderArray.GetSize();

	for(int i=0;i<ilSize;i++)
	{
		rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Text   = omPrintHeadHeaderArray[i].Text;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool ReportArrDepTableViewer::PrintTableLine(SAMEARRDEPTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	double dgCCSPrintFactor = 2.7 ;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omPrintHeadHeaderArray.GetSize();

	for(int i=0;i<ilSize;i++)
	{
		rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameTop   = PRINT_FRAMETHIN;
		if(bpLastLine == true)
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN;
		}
		else
		{
			rlElement.FrameBottom = PRINT_NOFRAME;
		}
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;


		switch(i)
		{
			case 0:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->AFlno;
				if (imArrDep == 1)
					rlElement.Text = prpLine->DFlno;
			}
			break;
			case 1:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->AOrg3;
				if (imArrDep == 1)
					rlElement.Text = prpLine->DDes3;
			}
			break;
			case 2:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->AStoa.Format("%H:%M");
				if (imArrDep == 1)
					rlElement.Text = prpLine->DStod.Format("%H:%M");
			}
			break;
			case 3:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->ALand.Format("%H:%M");
				if (imArrDep == 1)
					rlElement.Text = prpLine->DAirb.Format("%H:%M");
			}
			break;
			case 4:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->AAct;
				if (imArrDep == 1)
					rlElement.Text = prpLine->DAct;
			}
			break;
			case 5:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->ARegn;
				if (imArrDep == 1)
					rlElement.Text = prpLine->DRegn;
			}
			break;
			case 6:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->APsta;
				if (imArrDep == 1)
					rlElement.Text = prpLine->DCkic;
			}
			break;
			case 7:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->AGta1;
				if (imArrDep == 1)
					rlElement.Text = prpLine->DGtd1;
			}
			break;
			case 8:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->ABlt1;
				if (imArrDep == 1)
					rlElement.Text = prpLine->DPstd;
			}
			break;
		}

		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------


// Drucken in Datei
void ReportArrDepTableViewer::PrintPlanToFile(char *pcpDefPath)
{

	CCS_TRY

	char pclHeadlineSelect[200];
	sprintf(pclHeadlineSelect, GetString(IDS_STRING1631), pcmInfo, imFlightCount);

	ofstream of;
	of.open( pcpDefPath, ios::out);
	of << CString(pclHeadlineSelect) << " "
	   << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M") << endl << endl;

	if (imArrDep == 0)
	{
		of << GetString(IDS_STRING1078) << "     "<< GetString(IDS_STRING298) << "   " 
		   << GetString(IDS_STRING323)  << "    "  << GetString(IDS_STRING304) << "   " 
		   << GetString(IDS_STRING1569) << "   " << GetString(IDS_STRING310) << "   "
		   << GetString(IDS_STRING308)  << "   " << GetString(IDS_STRING1406) << "   "
		   << GetString(IDS_STRING1151) << endl;
		of << "---------------------------------------------------------------" << endl;
	}

	if (imArrDep == 1)
	{
		of << GetString(IDS_STRING1078) << "     "<< GetString(IDS_STRING315) << "   " 
		   << GetString(IDS_STRING316)  << "    "  << GetString(IDS_STRING321) << "   " 
		   << GetString(IDS_STRING1569) << "   " << GetString(IDS_STRING310) << "     "
		   << GetString(IDS_STRING1641)  << "   " << GetString(IDS_STRING1406) << "   "
		   << GetString(IDS_STRING308) << endl;
		of << "-------------------------------------------------------------------------" << endl;
	}

	CStringArray olDaten;
	int ilCount = imFlightCount;//omLines.GetSize();


	for(int i = 0; i < ilCount; i++)
	{
		SAMEARRDEPTABLE_LINEDATA rlD = omLines[i];

		if (imArrDep == 0)
		{
			olDaten.Add(rlD.AFlno);
			olDaten.Add(rlD.AOrg3);
			olDaten.Add(rlD.AStoa.Format("%H:%M"));
			olDaten.Add(rlD.ALand.Format("%H:%M"));
			olDaten.Add(rlD.AAct);
			olDaten.Add(rlD.ARegn);
			olDaten.Add(rlD.APsta);
			olDaten.Add(rlD.AGta1);
			olDaten.Add(rlD.ABlt1);
		}
		if (imArrDep == 1)
		{
			olDaten.Add(rlD.DFlno);
			olDaten.Add(rlD.DDes3);
			olDaten.Add(rlD.DStod.Format("%H:%M"));
			olDaten.Add(rlD.DAirb.Format("%H:%M"));
			olDaten.Add(rlD.DAct);
			olDaten.Add(rlD.DRegn);
			olDaten.Add(rlD.DCkic);
			olDaten.Add(rlD.DGtd1);
			olDaten.Add(rlD.DPstd);
		}

	if (imArrDep == 0)
	{
		of.setf(ios::left, ios::adjustfield);
		of   << setw(11) << olDaten[0].GetBuffer(0)
			 << setw(5) << olDaten[1].GetBuffer(0)
			 << setw(7) << olDaten[2].GetBuffer(0)
			 << setw(9) << olDaten[3].GetBuffer(0)
			 << setw(7) << olDaten[4].GetBuffer(0)
			 << setw(7) << olDaten[5].GetBuffer(0)
			 << setw(7) << olDaten[6].GetBuffer(0)
			 << setw(7) << olDaten[7].GetBuffer(0)
			 << setw(6) << olDaten[8].GetBuffer(0)
			 << endl;
	}		
	if (imArrDep == 1)
	{
		of.setf(ios::left, ios::adjustfield);
		of   << setw(11) << olDaten[0].GetBuffer(0)
			 << setw(5) << olDaten[1].GetBuffer(0)
			 << setw(7) << olDaten[2].GetBuffer(0)
			 << setw(9) << olDaten[3].GetBuffer(0)
			 << setw(7) << olDaten[4].GetBuffer(0)
			 << setw(10) << olDaten[5].GetBuffer(0)
			 << setw(14) << olDaten[6].GetBuffer(0)
			 << setw(6) << olDaten[7].GetBuffer(0)
			 << setw(5) << olDaten[8].GetBuffer(0)
			 << endl;
	}		
		olDaten.RemoveAll();
	}
	of.close();

	CCS_CATCH_ALL

}
