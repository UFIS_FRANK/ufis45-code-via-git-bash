// basicdat.cpp CBasicData class for providing general used methods

#include <stdafx.h>
#include <BasicData.h>
#include <CedaBasicData.h>
#include <RecordSet.h>




////////////////////// Stringhandling /////////////////////////////////////////////////////////////

// String mit Zeichen bis zur L�nge ipLength auff�llen
int FillRightByChar( CString &opStr, char cpSign, int ipLength )
{
	// hhb 7/98, return : Anzahl neue Zeichen
	int ipAnhang = ipLength - opStr.GetLength() ;
	
	if( ipAnhang > 0 )
	{
		opStr += CString( cpSign, ipAnhang ) ; 
		return( ipAnhang ) ;
	}
	else
		return( 0 ) ;

}

int FillLeftByChar( CString &opStr, char cpSign, int ipLength )
{
	// hhb 7/98, return : Anzahl neue Zeichen
	int ipAnhang = ipLength - opStr.GetLength() ;
	
	if( ipAnhang > 0 )
	{
		opStr = CString( cpSign, ipAnhang ) + opStr; 
		return( ipAnhang ) ;
	}
	else
		return( 0 ) ;

}






// Linken Anteil von String besorgen   
int CutLeft(CString & ropLeft, CString & ropLeftover, const char * OneOfSep)
{
	// liefert alle Zeichen von ropLeft bis 1. Auftreten von einem Zeichen aus "OneOfSep"
	// in ropLeft zur�ck, den Rest von ropLeft in ropLeftover
	// der Separator wird entfernt

	int ilSepPos = ropLeft.FindOneOf( OneOfSep ) ;	

	if( ilSepPos < 0 ) 
	{
		// kein Separator => alles in ropLeft !
		ropLeftover.Empty() ;
	}
	else
	{
		ropLeftover = ropLeft.Right( ropLeft.GetLength() - ilSepPos - 1 ) ;
		ropLeft = ropLeft.Left( ilSepPos ) ;
	}
	return( ropLeftover.GetLength() ) ;

}


int CutLeft(CString & ropLeft, CString & ropLeftover, int ipNo)
{
	// liefert "ipNo" Zeichen von ropLeft in "ropLeft" zur�ck, 
	// den Rest von ropLeft in "ropLeftover"

	ropLeftover = ropLeft.Right( ropLeft.GetLength() - ipNo ) ;
	ropLeft = ropLeft.Left( ipNo ) ;
	return( ropLeftover.GetLength() ) ;
}


// ......... String( dd.mm.yyyy-dd.mm.yyyy ) in Kurzform aufbereiten
void ShortRangeString( CString &ropStr )
{
	CString olFrom = ropStr.Left(10) ; 
	CString olTo = ropStr.Mid(11,10) ;

	if( ropStr.GetLength() == cimFullRangeLen )
	{
	
		if( olFrom == olTo ) ropStr = olFrom ;	// Eintagsflieger (dd.mm.yy)
		else
		{
			if( olFrom.Right(4) == olTo.Right(4) ) // gleiches Jahr (dd.mm.-dd.mm.yy)
			{
				ropStr = olFrom.Left(6)+"-"+olTo.Left(6)+olFrom.Right(4) ;
			}
			else	//  (dd.mm.yy-dd.mm.yy)
			{
				char pslShortJahr[4+1] ;
				strcpy( pslShortJahr, olFrom.Right(4) ) ;
				int ilShortJahr = atoi(pslShortJahr) % 100 ;
				itoa(ilShortJahr, pslShortJahr, 10 ) ;
				ropStr = olFrom.Left(6)+pslShortJahr+CString("-") ;
				strcpy( pslShortJahr, olTo.Right(4) ) ;
				ilShortJahr = atoi(pslShortJahr) % 100 ;
				itoa(ilShortJahr, pslShortJahr, 10 ) ;
				ropStr += olTo.Left(6)+pslShortJahr ;
			}
		}
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


bool GetListItemByField(CString &opDataList, CString &opFieldList, CString &opField, CString &opData)
{
	CStringArray olFields;
	CStringArray olData;
	int ilFields = ExtractItemList(opFieldList, &olFields);
	int ilData   = ExtractItemList(opDataList, &olData);

	for( int i = 0; i < ilFields; i++)
	{
		if(olFields[i] == opField)
		{
			if(ilData > i)
				opData = olData[i];
				return true; 
		}
	}
	return false;
}

bool GetListItemByField(CString &opDataList, CString &opFieldList, CString &opField, CTime &opData)
{
	CStringArray olFields;
	CStringArray olData;
	int ilFields = ExtractItemList(opFieldList, &olFields);
	int ilData   = ExtractItemList(opDataList, &olData);

	for( int i = 0; i < ilFields; i++)
	{
		if(olFields[i] == opField)
		{
			if(ilData > i)
				opData = DBStringToDateTime(olData[i]);
				return true; 
		}
	}
	return false;
}



void TraceFieldAndData(CString &opFieldList,CString &opListOfData, CString opMessage)
{

	CStringArray olFields;
	CStringArray olData;
	int ilFields = ExtractItemList(opFieldList, &olFields);
	int ilData   = ExtractItemList(opListOfData, &olData);

	TRACE("\n---------------------------------------------------------------------------------------------------------------------");
	TRACE("\n-- %s --- Fields:<%d>   Data:<%d>", opMessage, ilFields, ilData);


	if(ilFields == ilData)
	{
		for( int i = 0; i < ilData; i++)
		{
			TRACE("\n<%s> <%d> <%s>", olFields[i], olData[i].GetLength(), olData[i]);
		}
	}


	TRACE("\n---------------------------------------------------------------------------------------------------------------------");
}





int GetItemCount(CString olList, char cpTrenner  )
{
	CStringArray olStrArray;
	return ExtractItemList(olList,&olStrArray,cpTrenner);

}


int ExtractItemList(CString opSubString, CStringArray *popStrArray, char cpTrenner)
{

	CString olText;

	popStrArray->RemoveAll();

	BOOL blEnd = FALSE;
	
	if(!opSubString.IsEmpty())
	{
		int pos;
		int olPos = 0;
		while(blEnd == FALSE)
		{
			pos = opSubString.Find(cpTrenner);
			if(pos == -1)
			{
				blEnd = TRUE;
				olText = opSubString;
			}
			else
			{
				olText = opSubString.Mid(0, opSubString.Find(cpTrenner));
				opSubString = opSubString.Mid(opSubString.Find(cpTrenner)+1, opSubString.GetLength( )-opSubString.Find(cpTrenner)+1);
			}
			popStrArray->Add(olText);
		}
	}
	return	popStrArray->GetSize();

}




int SplitItemList(CString opString, CStringArray *popStrArray, int ipMaxItem, char cpTrenner)
{

	CString olSubStr;

	popStrArray->RemoveAll();
	
	CStringArray olStrArray;

	int ilCount = ExtractItemList(opString, &olStrArray);
	int ilSubCount = 0;

	for(int i = 0; i < ilCount; i++)
	{
		if(ilSubCount >= ipMaxItem)
		{
			if(!olSubStr.IsEmpty())
				olSubStr = olSubStr.Right(olSubStr.GetLength() - 1);
			popStrArray->Add(olSubStr);
			ilSubCount = 0;
			olSubStr = "";
		}
		ilSubCount++;
		olSubStr = olSubStr + cpTrenner + olStrArray[i];
	}
	if(!olSubStr.IsEmpty())
	{
		olSubStr = olSubStr.Right(olSubStr.GetLength() - 1);
		popStrArray->Add(olSubStr);
	}
	
	return	popStrArray->GetSize();
}






CString GetListItem(CString &opList, int ipPos, bool bpCut, char cpTrenner )
{
	CStringArray olStrArray;
	int ilAnz = ExtractItemList(opList, &olStrArray, cpTrenner);
	CString olReturn;

	if(ipPos == -1)
		ipPos = ilAnz;

	if((ipPos <= ilAnz) && (ipPos > 0))
		olReturn = olStrArray[ipPos - 1];
	if(bpCut)
	{
		opList = "";
		for(int ilLc = 0; ilLc < ilAnz; ilLc++)
		{
			if(ilLc != (ipPos - 1))
			{
				opList = opList + olStrArray[ilLc] + cpTrenner;
			}
		}
	}
	if(bpCut)
		opList = opList.Left(opList.GetLength() - 1);
	return olReturn;
}



CString DeleteListItem(CString &opList, CString olItem)
{
	CStringArray olStrArray;
	int ilAnz = ExtractItemList(opList, &olStrArray);
	opList = "";
	for(int ilLc = 0; ilLc < ilAnz; ilLc++)
	{
			if(olStrArray[ilLc] != olItem)
				opList = opList + olStrArray[ilLc] + ",";
	}
	opList = opList.Left(opList.GetLength() - 1);
	return opList;
}


CBasicData::CBasicData(void)
{
	char pclTmpText[512];
	char pclConfigPath[512];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	imNextOrder = 4711;
	*pcmCedaCmd = '\0';


	omDiaStartTime = CTime::GetCurrentTime();
	omDiaStartTime -= CTimeSpan(0, 1, 0, 0);
	omDiaEndTime = omDiaStartTime + CTimeSpan(0, 6, 0, 0);
	

	// now reading the ceda commands
	char pclComandBuf[24];
	omDefaultComands.Empty();
	omActualComands.Empty();
 
    GetPrivateProfileString(ogAppName, "EIOHDL", "LLF",
		pclTmpText, sizeof pclTmpText, pclConfigPath);
	sprintf(pclComandBuf," %3s ","LLF");
	omDefaultComands += pclComandBuf;
	sprintf(pclComandBuf," %3s ",pclTmpText);
	omActualComands += pclComandBuf;
}



CBasicData::~CBasicData(void)
{
}


long CBasicData::GetNextUrno(void)
{
	if(omNewUrnos.GetSize() == 0)
	{
		GetManyUrnos(500, omNewUrnos);
	}

	long llUrno;

	llUrno = omNewUrnos[0];

	omNewUrnos.RemoveAt(0);

	return llUrno;


/*
	if (CedaAction("GNU") == true)
	{
		return(atol(omDataBuf[0]));
	}
	MessageBox(NULL, GetString(IDS_STRING903), GetString(ST_FEHLER), MB_OK);
	ExitProcess(1);
	return -1;	
*/
}



bool CBasicData::GetManyUrnos(int ipAnz, CUIntArray &opUrnos)
{
	bool	ilRc = false;
	char 	pclTmpDataBuf[100000];

	while(ilRc == false)
	{
		sprintf(pclTmpDataBuf, "%d", ipAnz);
		ilRc = CedaAction("GMU", "", "", pclTmpDataBuf);
	}

	if (ilRc == true)
	{
		for ( int ilItemNo=1; (ilItemNo <= ipAnz) ; ilItemNo++ )
		{
			char pclTmpBuf[128];

			GetItem(ilItemNo, pclTmpDataBuf, pclTmpBuf);

			long llNewUrno = atol(pclTmpBuf);

			opUrnos.Add(llNewUrno);
		}
	}

	return ilRc;
}




int CBasicData::GetNextOrderNo()
{
	imNextOrder++;

	return (imNextOrder);
}


char *CBasicData::GetCedaCommand(CString opCmdType)
{ 

	int ilIndex;

	if ((ilIndex = omDefaultComands.Find(opCmdType)) != -1)
	{
		strcpy(pcmCedaComand,omActualComands.Mid(ilIndex,3));
	}
	else
	{
		strcpy(pcmCedaComand,opCmdType);
	}
	return pcmCedaComand;
}


void CBasicData::GetDiagramStartTime(CTime &opStart, CTime &opEnd)
{
	opStart = omDiaStartTime;
	opEnd = omDiaEndTime;
	return;
}


void CBasicData::SetDiagramStartTime(CTime opDiagramStartTime, CTime opDiagramEndTime)
{
	omDiaStartTime = opDiagramStartTime;
	omDiaEndTime = opDiagramEndTime;
}


void CBasicData::SetWorkstationName(CString opWsName)
{
	omWorkstationName = CString("WKS234");//opWsName;
}


CString CBasicData::GetWorkstationName()
{
	return omWorkstationName;
}


bool CBasicData::GetWindowPosition(CRect& rlPos,CString olMonitor)
{

	int XResolution = 1024;
	int YResolution = 768;
	if (ogCfgData.rmUserSetup.RESO[0] == '8')
	{
		XResolution = 800;
		YResolution = 600;
	}
	else
	{
		if (ogCfgData.rmUserSetup.RESO[0] == '1')
		{
			if (ogCfgData.rmUserSetup.RESO[1] == '0')
			{
				XResolution = 1024;
				YResolution = 768;
			}
		}
		else
		{
			XResolution = 1280;
			YResolution = 1024;
		}
	}
  

	int ilMonitor;
	if (olMonitor[0] == 'L')
		ilMonitor = 0;
	if (olMonitor[0] == 'M')
		ilMonitor = 1;
	if (olMonitor[0] == 'R')
		ilMonitor = 2;
	
	rlPos.top = ilMonitor == 0 ? 56 : 0;
	rlPos.bottom = YResolution;
	rlPos.left = XResolution * ilMonitor;
	rlPos.right = XResolution * (ilMonitor+1);

	return true;
}






void CBasicData::SetLocalDiff()
{
	int		ilTdi1;
	int		ilTdi2;
	int		ilMin;
	int		ilHour;
	CTime	olTich ;	
	CTime olCurr;
	RecordSet *prlRecord;	

	CCSPtrArray<RecordSet> olData;

	CString olWhere = CString("WHERE APC3 = '") + CString(pcgHome) + CString("'");

	if(ogBCD.ReadSpecial( "APT", "TICH,TDI1,TDI2", olWhere, olData))
	{
		if(olData.GetSize() > 0)
		{
			prlRecord = &olData[0];
		
			olTich = DBStringToDateTime((*prlRecord)[0]);
			omTich = olTich;
			ilTdi1 = atoi((*prlRecord)[1]);
			ilTdi2 = atoi((*prlRecord)[2]);
			
			ilHour = ilTdi1 / 60;
			ilMin  = ilTdi1 % 60;
			
			CTimeSpan	olTdi1(0,ilHour ,ilMin ,0);

			ilHour = ilTdi2 / 60;
			ilMin  = ilTdi2 % 60;
			
			CTimeSpan	olTdi2(0,ilHour ,ilMin ,0);

			olCurr = CTime::GetCurrentTime();

			if(olTich != TIMENULL) 
			{
				omLocalDiff1 = olTdi1;
				omLocalDiff2 = olTdi2;
			}
		}
	}
	olData.DeleteAll();
}

// the same functions are in CCSCedaData !!!!

void CBasicData::LocalToUtc(CTime &opTime) 
{
	CCSCedaData::LocalToUtc(opTime);
	return;
	
	CTime olTimeSave = opTime;
	if((opTime != TIMENULL) && (omTich != TIMENULL))
	{
		if(opTime <= omTich)
		{
			opTime -= omLocalDiff1;
			int ilHourSave = olTimeSave.GetHour();
			int ilHour = opTime.GetHour();
			if (opTime.GetDay() == olTimeSave.GetDay())
			{
				ilHour = (ilHourSave - ilHour) * 60 - omLocalDiff1.GetTotalMinutes();
			}
			else
			{
				ilHour = (24 - ilHour + ilHourSave) * 60 - omLocalDiff1.GetTotalMinutes();
			}

			if (ilHour > 0)
			{
				opTime = opTime + CTimeSpan(0,0,ilHour,0);
			}
			else
			{
				ilHour = abs(ilHour);
				opTime = opTime - CTimeSpan(0,0,ilHour,0);
			}
		}
		else  
		{
			opTime -= omLocalDiff2; 
			int ilHourSave = olTimeSave.GetHour(); 
			int ilHour = opTime.GetHour();
			if (opTime.GetDay() == olTimeSave.GetDay())
			{
				ilHour = (ilHourSave - ilHour) * 60 - omLocalDiff2.GetTotalMinutes();
			}
			else
			{
				ilHour = (24 - ilHour + ilHourSave) * 60 - omLocalDiff2.GetTotalMinutes();
			}

			if (ilHour > 0)
			{
				opTime = opTime + CTimeSpan(0,0,ilHour,0);
			}
			else
			{
				ilHour = abs(ilHour);
				opTime = opTime - CTimeSpan(0,0,ilHour,0);
			}
		}
	}

}

void CBasicData::UtcToLocal(CTime &opTime)
{
	CCSCedaData::UtcToLocal(opTime);
	return;

	CTime olTichUtc;
//	olTichUtc = omTich - CTimeSpan(omLocalDiff2);
	olTichUtc = omTich - CTimeSpan(omLocalDiff1);


	CTime olTimeSave = opTime;

	if((opTime != TIMENULL) && (omTich != TIMENULL))
	{
		if(opTime <= olTichUtc)
		{
			opTime += omLocalDiff1;
//			CTimeSpan olSpan = opTime - olTimeSave;
			int ilHourSave = olTimeSave.GetHour();
			int ilHour = opTime.GetHour();
			if (opTime.GetDay() == olTimeSave.GetDay())
			{
				ilHour = (ilHour - ilHourSave) * 60 - omLocalDiff1.GetTotalMinutes();
			}
			else
			{
				ilHour = (24 + ilHour - ilHourSave) * 60 - omLocalDiff1.GetTotalMinutes();
			}

			if (ilHour != 0)
			{
				if (ilHour > 0)
				{
					opTime = opTime - CTimeSpan(0,0,ilHour,0);
				}
				else
				{
					ilHour = abs(ilHour);
					opTime = opTime + CTimeSpan(0,0,ilHour,0);
				}
			}
		}
		else
		{
			opTime += omLocalDiff2;
//			CTimeSpan olSpan = opTime - olTimeSave;
			int ilHourSave = olTimeSave.GetHour();
			int ilHour = opTime.GetHour();
			if (opTime.GetDay() == olTimeSave.GetDay())
			{
				ilHour = (ilHour - ilHourSave) * 60 - omLocalDiff2.GetTotalMinutes();
			}
			else
			{
				ilHour = (24 + ilHour - ilHourSave) * 60 - omLocalDiff2.GetTotalMinutes();
			}

				if (ilHour > 0)
				{
					opTime = opTime - CTimeSpan(0,0,ilHour,0);
				}
				else
				{
					ilHour = abs(ilHour);
					opTime = opTime + CTimeSpan(0,0,ilHour,0);
				}
/*
			if (olSpan != omLocalDiff2)
			{
				int ilMinutes = omLocalDiff2.GetTotalMinutes() - olSpan.GetTotalMinutes();
				opTime = opTime + CTimeSpan(0,0,ilMinutes,0);
			}
*/

/*
			CString olFromDate;
			olFromDate.Format("%04d%02d%02d%02d%02d", omTich.GetYear(), omTich.GetMonth(), omTich.GetDay(), omTich.GetHour()-2, omTich.GetMinute());
			CString olToDate;
			olToDate.Format("%04d%02d%02d%02d%02d", omTich.GetYear(), omTich.GetMonth(), omTich.GetDay(), omTich.GetHour()-1, omTich.GetMinute());
			CString olOpDate = opTime.Format("%Y%m%d%H%M");
			if(olOpDate >= olFromDate && olOpDate <= olToDate)
			{
				opTime += omLocalDiff1;
			}
			else
			{
				opTime += omLocalDiff2;
			}
*/
		}
	}
}



int CBasicData::GetViaArray(CCSPtrArray<VIADATA> *opVias, CString opViaList)
{
	opVias->DeleteAll();


	CString olVias(opViaList);
	CString olFids;
	CString olApc3;
	CString olApc4;

	VIADATA *prlVia;

	while(olVias.IsEmpty() != TRUE)
	{
			prlVia = new VIADATA;
			opVias->Add(prlVia);

			if(olVias.GetLength() < 120)
			{
				olVias += "                                                                                                                                                 ";
				olVias = olVias.Left(120);
			}


			olFids = olVias.Left(1);
			olApc3 = olVias.Mid(1,3);
			olApc4 = olVias.Mid(4,4);
			prlVia->Stoa = DBStringToDateTime(olVias.Mid(8,14));
			prlVia->Etoa = DBStringToDateTime(olVias.Mid(22,14));
			prlVia->Land = DBStringToDateTime(olVias.Mid(36,14));
			prlVia->Onbl = DBStringToDateTime(olVias.Mid(50,14));
			prlVia->Stod = DBStringToDateTime(olVias.Mid(64,14));
			prlVia->Etod = DBStringToDateTime(olVias.Mid(78,14));
			prlVia->Ofbl = DBStringToDateTime(olVias.Mid(92,14));
			prlVia->Airb = DBStringToDateTime(olVias.Mid(106,14));			
			olFids.TrimLeft();
			olApc3.TrimLeft();
			olApc4.TrimLeft();
			sprintf(prlVia->Fids, olFids);
			sprintf(prlVia->Apc3, olApc3);
			sprintf(prlVia->Apc4, olApc4);

			if(olVias.GetLength() >= 120)
				olVias = olVias.Right(olVias.GetLength() - 120);

			//if(olVias.GetLength() == 120)
			//	break;

	}
	return opVias->GetSize();
}

