// dataset.cpp - Class for handle each user action
//
// Description:
// This module is separated from some parts of viewers in many diagrams and
// expected to contain the routines for removing dependencies among Ceda????Data
// classes.
//
// The concept is simple. If we want a new kind of job creation or any action
// that the user can perform on the system, we create a new method here, in this
// module. Then, if that action has some more consequents actions (assign a job
// to solve a demand will change the color of the flight, for example) we will
// provide that steps here, in this module too.
//
// Some actions may require the user to make an interactive decision. To aid
// such concept, the first parameter of every functions will be "popParentWindow",
// and every function here will return IDOK or IDYES on success, IDCANCEL or IDNO
// if the user cancel this operation. The difference between IDCANCEL and IDNO is
// the user may select IDCANCEL when he/she still want to resume some data entry
// in the previous dialogs if exist. But IDNO selection will totally cancel this
// operation, including the in-process dialogs if exist.
//
// Function in this module:
// (still incomplete and need keep on working).
//
//
// Written by:
// Damkerng Thammathakerngkit   July 6, 1996
//

#include <stdafx.h>

#include <DataSet.h>
#include <CCSDDX.h>
#include <CCSGlobl.h>
#include <BasicData.h>
#include <CedaBasicData.h>
#include <ButtonListDlg.h>
#include <CedaDiaCcaData.h>
#include <CcaDiagram.h>
#include <CedaCcaData.h>
#include <Utils.h>


	int DataSet::ilGhpDopaIdx = 0;
	int DataSet::ilGhpDopdIdx = 0;
	int DataSet::ilGhpMaxtIdx = 0;
	int DataSet::ilGhpRegnIdx = 0;
	int DataSet::ilGhpAct5Idx = 0;
	int DataSet::ilGhpActmIdx = 0;
	int DataSet::ilGhpFlcaIdx = 0;
	int DataSet::ilGhpFlnaIdx = 0;
	int DataSet::ilGhpFlsaIdx = 0;
	int DataSet::ilGhpFlcdIdx = 0;
	int DataSet::ilGhpFlndIdx = 0;
	int DataSet::ilGhpFlsdIdx = 0;
	int DataSet::ilGhpAlmaIdx = 0;
	int DataSet::ilGhpAlmdIdx = 0;
	int DataSet::ilGhpOrigIdx = 0;
	int DataSet::ilGhpDestIdx = 0;
	int DataSet::ilGhpVi1aIdx = 0;
	int DataSet::ilGhpVi1dIdx = 0;
	int DataSet::ilGhpTrraIdx = 0;
	int DataSet::ilGhpTrrdIdx = 0;
	int DataSet::ilGhpSccaIdx = 0;
	int DataSet::ilGhpSccdIdx = 0;
	int DataSet::ilGhpPcaaIdx = 0;
	int DataSet::ilGhpPcadIdx = 0;
	int DataSet::ilGhpTtpaIdx = 0;
	int DataSet::ilGhpTtpdIdx = 0;
	int DataSet::ilGhpTtgaIdx = 0;
	int DataSet::ilGhpTtgdIdx = 0;
	int DataSet::ilGhpHtpaIdx = 0;
	int DataSet::ilGhpHtpdIdx = 0;
	int DataSet::ilGhpHtgaIdx = 0;
	int DataSet::ilGhpHtgdIdx = 0;
	int DataSet::ilGhpPrsnIdx = 0;
	int DataSet::ilGhpVafrIdx = 0;
	int DataSet::ilGhpVatoIdx = 0;
	int DataSet::ilGhpPrioIdx = 0;
	int DataSet::ilGhpFliaIdx = 0;
	int DataSet::ilGhpFlidIdx = 0;




#define IsReallyOverlapped(start1, end1, start2, end2)	((start1) < (end2) && (start2) < (end1))



static int CompareRulesPrio(const PREMIS_ORDER **e1, const PREMIS_ORDER **e2)
{
	if((**e1).UserPrio == (**e2).UserPrio)
	{
		return (CString((**e1).Prio) == CString((**e2).Prio))? 0:
			(CString((**e1).Prio) > CString((**e2).Prio))? -1: 1;
	}
	else
	{
		return ( (**e1).UserPrio == (**e2).UserPrio)? 0:
			( (**e1).UserPrio > (**e2).UserPrio ) ? -1: 1;
	}

}


static int CompareCcaForCkbs(const DIACCADATA **e1, const DIACCADATA **e2)
{
	return ((**e1).Ckbs == (**e2).Ckbs)? 0: ((**e1).Ckbs < (**e2).Ckbs)? 1: -1;
}

// MCU 07.28 have stolen all your macros also, you will find them doubled in conflicts.cpp
//
// To know which rank is higher in comparison of two staff, I provide a colon-separated
// string from the lowest rank to the highest rank. By searching for a string ":XX:"
// in "pclRanks", if XX is the given rank, we could easily see which rank is higher.
// By using the same technic, we could see if the given rank is a manager by searching
// for a string ":XX:" in "pclManagers".
//

// General macros for testing a point against an interval
/////////////////////////////////////////////////////////////////////////////
// DataSet -- Job creation routine

DataSet::DataSet()
{
}

DataSet::~DataSet()
{
}



int DataSet::GetGPMs(CString opRkey, CCSPtrArray<RecordSet> *popGPMs)
{

	ogBCD.GetRecords("GPM", "RKEY", opRkey, popGPMs);

	/*
	int ilCount = ogBCD.GetDataCount("GPM");
	int ilIndex = ogBCD.GetFieldIndex("GPM", "RKEY");

	RecordSet rpRecord;

	for(int i = 0; i < ilCount; i++)
	{
		ogBCD.GetRecord("GPM", i,  rpRecord);

		if(rpRecord[ilIndex] == opRkey)
		{
			popGPMs->New(rpRecord);
		}
	}
	*/
	return popGPMs->GetSize();
}





CString DataSet::GetRulesName(CCSPtrArray<RecordSet> *popGPRs, CString opTable) 
{
	CString olRet;
	RecordSet *prlRecord;

	int ilIndex = ogBCD.GetFieldIndex(opTable, "PRSN");

	for(int i = popGPRs->GetSize() - 1; i >= 0; i--)
	{
		prlRecord = &((*popGPRs)[i]);
		olRet += prlRecord->Values[ilIndex] + CString(",");
	}
	
	if(!olRet.IsEmpty())
		olRet = olRet.Left(olRet.GetLength()-1);

	return olRet;
}




// For POPS CCA - Counterdiagram

bool DataSet::GetGhpDataForFlight(CCSPtrArray<RecordSet> *polGhps, const CCAFLIGHTDATA *prpFlightA, const CCAFLIGHTDATA *prpFlightD)
{

	char pclPrio[21]="00000000000000000000";

	CCSPtrArray<PREMIS_ORDER> olRules;

	RecordSet rlPgr;
	CTimeSpan olMaxDuty;
	CString olDopa;
	CString olDopd;
	CString olAlc;
	CTime   olVafr;
	CTime   olVato;


	PREMIS_ORDER rlPremOrder;




	//RST (MWO TURBO)

	CString olDGhpActm;
	CString olAGhpActm;
	CString olGhpAlma; 
	CString olGhpAlmd; 
	CString olGhpTrra; 
	CString olGhpTrrd; 
	CString olGhpPcaa; 
	CString olGhpPcad; 
	CString olGhpTtga; 
	CString olGhpTtgd; 
	CString olGhpHtga; 
	CString olGhpHtgd; 



	if(prpFlightD != NULL)
	{
		olDGhpActm  = ogBCD.GetValueList("GRM","VALU", ogBCD.GetField2("ACT","ACT3","ACT5", CString(prpFlightD->Act3).IsEmpty() ? CString(prpFlightD->Act5).IsEmpty() ? CString("") : CString(prpFlightD->Act5): CString(prpFlightD->Act3) ,"URNO"), "GURN", ";");
		olGhpAlmd = ogBCD.GetValueList("GRM","VALU", ogBCD.GetField2("ALT","ALC2","ALC3", CString(prpFlightD->Alc2).IsEmpty() ? CString(prpFlightD->Alc3).IsEmpty() ? CString("") : CString(prpFlightD->Alc3): CString(prpFlightD->Alc2) ,"URNO"), "GURN", ";");
		olGhpTrrd = ogBCD.GetValueList("GRM","VALU", ogBCD.GetField2("APT","APC3","APC4", CString(prpFlightD->Des3).IsEmpty() ? CString(prpFlightD->Des4).IsEmpty() ? CString("") : CString(prpFlightD->Des4): CString(prpFlightD->Des3) ,"URNO"), "GURN", ";");
		olGhpPcad = ogBCD.GetValueList("GRM","VALU", ogBCD.GetField("PST","PNAM", CString(prpFlightD->Pstd) ,"URNO"), "GURN", ";");
		olGhpTtgd = ogBCD.GetValueList("GRM","VALU", ogBCD.GetField("NAT","TTYP", CString(prpFlightD->Ttyp) ,"URNO"), "GURN", ";");
		olGhpHtgd = ogBCD.GetValueList("GRM","VALU", ogBCD.GetField("HTY","HTYP", CString(prpFlightD->Htyp) ,"URNO"), "GURN", ";");
	}



	int ilCount = ogBCD.GetDataCount("GHP");
	
	for(int l = 0; l < ilCount; l++)
	{
		if(ogBCD.GetRecord("GHP" ,l , rlPgr))
		{
			olDopa = rlPgr[ilGhpDopaIdx];
			olDopd = rlPgr[ilGhpDopdIdx];

			olVafr = DBStringToDateTime(rlPgr[ilGhpVafrIdx]);
			olVato = DBStringToDateTime(rlPgr[ilGhpVatoIdx]);

			
			if((prpFlightD != NULL) && (prpFlightA == NULL))
			{
				if(prpFlightD->Tifd > olVafr && prpFlightD->Tifd < olVato )
				{
					if(olDopa.IsEmpty() && !olDopd.IsEmpty())
					{

						// Airline
						if( !Check( rlPgr[ilGhpFlcdIdx], CString("|") + CString(prpFlightD->Alc2) + CString("|") + CString(prpFlightD->Alc3) + CString("|"), FIND_IN_FLIGHT_SPEZ, 3, pclPrio   ) ) continue;
 
						// Group Airline
						if( !Check( rlPgr[ilGhpAlmdIdx], olGhpAlmd,  FIND_IN_FLIGHT, 6, pclPrio   ) ) continue;

						// Aircrafttye
						if( !Check( rlPgr[ilGhpAct5Idx], CString("|") + CString(prpFlightD->Act3) + CString("|") + CString(prpFlightD->Act5) + CString("|"),  FIND_IN_FLIGHT_SPEZ, 1, pclPrio   ) ) continue;

						// Group Aircrafttye
						if( !Check( rlPgr[ilGhpActmIdx], olDGhpActm,  FIND_IN_FLIGHT, 2, pclPrio   ) ) continue;

						// Flti
						if( !Check( rlPgr[ilGhpFlidIdx], CString(prpFlightD->Flti),  FIND_IN_RULE, 4, pclPrio   ) ) continue;

						// Flightnumber numeric
						if( !Check( rlPgr[ilGhpFlndIdx], CString(prpFlightD->Fltn),  EQUAL, 5, pclPrio   ) ) continue;

						// Flightnumber numeric
						if( !Check( rlPgr[ilGhpFlsdIdx], CString(prpFlightD->Flns),  EQUAL, 6, pclPrio   ) ) continue;

						// Dest
						if( !Check( rlPgr[ilGhpDestIdx], CString(prpFlightD->Des3),  EQUAL, 7, pclPrio   ) ) continue;

						// first Via
						if( !Check( rlPgr[ilGhpVi1dIdx], CString(prpFlightD->Via3),  EQUAL, 8, pclPrio   ) ) continue;

						// 
						if( !Check( rlPgr[ilGhpTrrdIdx], olGhpTrrd,  FIND_IN_FLIGHT, 9, pclPrio   ) ) continue;

						// Count of vias
						if( !Check( rlPgr[ilGhpSccdIdx], CString(prpFlightD->Vian),  EQUAL, 10, pclPrio   ) ) continue;

						// 
						if( !Check( rlPgr[ilGhpPcadIdx], olGhpPcad,  FIND_IN_FLIGHT, 11, pclPrio   ) ) continue;

						// Traffictype
						if( !Check( rlPgr[ilGhpTtpdIdx], CString(prpFlightD->Ttyp),  EQUAL, 12, pclPrio   ) ) continue;

						// 
						if( !Check( rlPgr[ilGhpTtgdIdx], olGhpTtgd,  FIND_IN_FLIGHT, 13, pclPrio   ) ) continue;
						
						// 
						if( !Check( rlPgr[ilGhpHtpdIdx], CString(prpFlightD->Htyp),  EQUAL, 13, pclPrio   ) ) continue;

						// 
						if( !Check( rlPgr[ilGhpHtgdIdx], olGhpHtgd,  FIND_IN_FLIGHT, 14, pclPrio   ) ) continue;

						// 
						if( !Check( rlPgr[ilGhpDopdIdx], CString(prpFlightD->Dood),  FIND_IN_RULE, 15, pclPrio   ) ) continue;

						// Registation
						if( !Check( rlPgr[ilGhpRegnIdx], CString(prpFlightA->Regn),  EQUAL, 0, pclPrio   ) ) continue;

						rlPremOrder.Pgr = rlPgr;
						rlPremOrder.UserPrio = atoi(rlPgr[ilGhpPrioIdx]);
						strcpy(rlPremOrder.Prio, pclPrio);
						olRules.NewAt(olRules.GetSize(), rlPremOrder);


					}//if(olDopa.IsEmpty() && !olDopd.IsEmpty())
				}//if((prpFlightD->Tifd > olVafr && prpFlightD->Tifd < olVato )
			}//if((prpFlightD != NULL))

		}//if(prlGhp != NULL)
	}


	bool blRet = true;

	/*
	if(prpFlightD != NULL)
	{
		if(CString(prpFlightD->Alc3) == "FTI")
			int sajda = 0;
	}
	*/

	olRules.Sort(CompareRulesPrio);



	if(olRules.GetSize() == 1)
	{
		polGhps->New(olRules[0].Pgr);
	}
	else
	{
		if(olRules.GetSize() >= 2)
		{
			if(((CString(olRules[0].Prio) != CString(olRules[1].Prio)) && (olRules[0].UserPrio == olRules[0].UserPrio) ) || 
			    (olRules[0].UserPrio != olRules[0].UserPrio) )
			{
				polGhps->New(olRules[0].Pgr);
			}
			else
			{
				polGhps->New(olRules[0].Pgr);
				polGhps->New(olRules[1].Pgr);
			}
		}
		else
		{
			if(olRules.GetSize() == 0)
			{
				blRet = false;
			}
		}
	}


	olRules.DeleteAll();
  
	return blRet;


}





bool DataSet::Check(CString &opRuleValue, CString &opFlightValue, int ilCompare, int ipPrio, char *pcpPrio)
{
	pcpPrio[ipPrio] = '0';


	if(ilCompare == FIND_IN_FLIGHT) //Fall auf Urnos ==> Find, da ein simpler Vergleich schief geht
	{
		if(!opRuleValue.IsEmpty())
		{
			if(!opFlightValue.IsEmpty())
			{
				if(opFlightValue.Find( opRuleValue) != -1)
				{
					pcpPrio[ipPrio] = '1';
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		return true;
	}

	if(ilCompare == FIND_IN_FLIGHT_SPEZ) //Fall auf Urnos ==> Find, da ein simpler Vergleich schief geht
	{
		if(!opRuleValue.IsEmpty())
		{
			if(!opFlightValue.IsEmpty())
			{
				if(opFlightValue.Find( CString("|") + opRuleValue + CString("|") ) != -1)
				{
					pcpPrio[ipPrio] = '1';
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		return true;
	}

	if(ilCompare == FIND_IN_RULE) //z.B. f�r DOOA/DOOD
	{
		if(!opRuleValue.IsEmpty())
		{
			if(!opFlightValue.IsEmpty())
			{
				if(opRuleValue.Find(opFlightValue) != -1)
				{
					pcpPrio[ipPrio] = '1';
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		return true;
	}

	
	if(ilCompare == EQUAL) 
	{
		if(!opRuleValue.IsEmpty())
		{
			if(!opFlightValue.IsEmpty())
			{
				if(opRuleValue == opFlightValue)
				{
					pcpPrio[ipPrio] = '1';
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		return true;
	}

	return false;

}






bool DataSet::DeAssignCca(long lpCcaUrno)
{
	bool blRet = false;
	DIACCADATA *prlCca = ogCcaDiaFlightData.omCcaData.GetCcaByUrno(lpCcaUrno);
	if(prlCca != NULL)
	{
		strcpy(prlCca->Ckic, "");

		prlCca->Stat[0] = '0';
		prlCca->Stat[1] = '0';
		prlCca->Stat[2] = '0';
		prlCca->Stat[3] = '0';

		/*
		prlCca->Stat[4] = '0';
		prlCca->Stat[5] = '0';
		prlCca->Stat[6] = '0';
		prlCca->Stat[7] = '0';
		prlCca->Stat[8] = '0';
		*/
		
		
		prlCca->IsChanged = DATA_CHANGED;
		//ogDdx.DataChanged((void *)this,DIACCA_CHANGE,(void *)&prlCca->Urno); //Update Viewer
		//ogCcaDiaFlightData.omCcaData.UpdateInternal(prlCca);
		ogCcaDiaFlightData.omCcaData.Save(prlCca); //RST!


		/*
		CCSPtrArray<DIACCADATA> olCcas;

		if(prlCca->Flnu > 0)
		{
			ogCcaDiaFlightData.omCcaData.GetCcasByFlnu( olCcas, prlCca->Flnu);

			bool blTreffer = false;
			bool blKonf = false;

			for( int i = olCcas.GetSize() -1 ; i >= 0; i--)
			{
				if( CString(olCcas[i].Ckic) != "")
				{
					blTreffer = true;
					break;
				}

				if( (olCcas[i].Stat[4] != '0') || 
					(olCcas[i].Stat[5] != '0') || 
					(olCcas[i].Stat[6] != '0') || 
					(olCcas[i].Stat[7] != '0') || 
					(olCcas[i].Stat[8] != '0') || 
					(olCcas[i].Stat[9] != '0'))
				{
					blKonf = true;
				}
			}
			if(!blTreffer && blKonf)
			{
				CCAFLIGHTDATA *prlFlight = ogCcaDiaFlightData.GetFlightByUrno(prlCca->Flnu);

				if(prlFlight != NULL)
				{
					RelHdl(prlFlight, true);
				}
				blRet = true;
			}
			olCcas.RemoveAll();

		}
		*/
	}
	return blRet;
}


void DataSet::GetConflictBrushByCcaStatus(DIACCADATA *prpCca, CcaDIA_BARDATA *prlBar)
{
	// Cki Bar colours:
	//
	// AQUA: Default
	// LIME: Counter open
	// GRAY: Counter closed
	// BLUE: Selected
	// ROT GESTREIFT: Not available
	// BLUE GESTREIFT: Not valid
	// TEAL (dark green): Common
	// FUCHSIA: Conflict (Wrong allocation): Confirmed
	// ORANGE: Conflict (Overlapped): Confirmed
	// RED: Conflict (Wrong allocation) not confirmed
	//      Conflict (Overlapped): not confirmed
	// GREEN: Conflict (Without demand)
	// OLIVE: Conflict (Rule changed)
	// MAROON: Conflict (Act changed)
	// PURPLE: Conflict (Std changed)
	// NAVY: Conflict (Flno changed)

	// default colors
	prlBar->MarkerBrush = ogBrushs[IDX_AQUA];
	prlBar->TextColor = COLORREF(BLACK);


	if(CString(prpCca->Ctyp) == "N") // Not Available
	{
		if((CString(prpCca->Disp).Find("g�ltig von") >= 0) ||  (CString(prpCca->Disp).Find("g�ltig bis") >= 0))
		{
			prlBar->MarkerBrush = pogNotValidBrush;
			return;
		}
		else
		{
			prlBar->MarkerBrush = pogNotAvailBrush;
			return;
		}
	}


	if(prpCca->IsSelected == true)
	{
		prlBar->MarkerBrush = ogBrushs[5];
		prlBar->TextColor = COLORREF(WHITE);
		return;
	}


	if(CString(prpCca->Ctyp) == "C") // Common
	{
		if(prpCca->Ckea != TIMENULL)
		{
			prlBar->MarkerBrush = ogBrushs[IDX_GRAY];
			return;
		}

		if(prpCca->Ckba != TIMENULL)
		{
			prlBar->MarkerBrush = ogBrushs[IDX_LIME];
			return;
		}
		prlBar->MarkerBrush = ogBrushs[IDX_TEAL];//prlBar->MarkerBrush;
		return;
	}

	if(prpCca->Ckea != TIMENULL)
	{
		prlBar->MarkerBrush = ogBrushs[IDX_GRAY];
		return;
	}

	if(prpCca->Ckba != TIMENULL)
	{
		prlBar->MarkerBrush = ogBrushs[IDX_LIME];
		return;
	}

	if(CString(prpCca->Ctyp) == "") 
	{
		if(prpCca->Stat[0] == 'J') //Wrong Allocation
		{
			if(prpCca->Stat[1] == 'J') //Conflict Confirmed
			{
				prlBar->MarkerBrush = ogBrushs[IDX_FUCHSIA];
			}
			else //Not Confirmed
			{
				prlBar->MarkerBrush = ogBrushs[IDX_RED];
			}
		}
		else if(prpCca->Stat[2] == 'J')//Overlapped
		{
			if(prpCca->Stat[3] == 'J') //Conflict Confirmed
			{
				prlBar->MarkerBrush = ogBrushs[IDX_ORANGE];
			}
			else //Not Confirmed
			{
				prlBar->MarkerBrush = ogBrushs[IDX_RED];
			}
		}
		else if(prpCca->Stat[5] == 'J')//Without demand
		{
			prlBar->MarkerBrush = ogBrushs[IDX_GREEN];
			prlBar->TextColor = COLORREF(WHITE);
		}
		else if(prpCca->Stat[6] == 'J')//Rule changed
		{
			prlBar->MarkerBrush = ogBrushs[IDX_OLIVE];
			prlBar->TextColor = COLORREF(WHITE);
		}
		else if(prpCca->Stat[7] == 'J')//Act changed
		{
			prlBar->MarkerBrush = ogBrushs[IDX_MAROON];
			prlBar->TextColor = COLORREF(WHITE);
		}
		else if(prpCca->Stat[8] == 'J')//STD changed
		{
			prlBar->MarkerBrush = ogBrushs[IDX_PURPLE];
			prlBar->TextColor = COLORREF(WHITE);
		}
		else if(prpCca->Stat[4] == 'J')//FLNO changed
		{
			prlBar->MarkerBrush = ogBrushs[IDX_NAVY];
			prlBar->TextColor = COLORREF(WHITE);
		}
	}
}


int DataSet::GetFistCcaConflict(DIACCADATA *prpCca)
{
	int ilConflIdx = -1;
	int ilLen = 10;
	int ilCurrIdx=0;
	bool blEnd = false;
	while(ilCurrIdx < ilLen && blEnd == false)
	{
		if(prpCca->Stat[ilCurrIdx] == 'J')
		{
			blEnd = true;
			ilConflIdx = ilCurrIdx;
		}
		ilCurrIdx = ilCurrIdx+2;
	}
	return ilConflIdx;
}






bool DataSet::CheckCCaForOverlapping(CString opCkic)
{	
	bool blChangesFound = false;
	CCSPtrArray<DIACCADATA> olCcaList;
	CCSPtrArray<DIACCADATA> olSaveCcaList;
	CCSPtrArray<DIACCADATA> olCcas;
	int ilCount;

	ogCcaDiaFlightData.omCcaData.GetCcasByCkic(olCcaList,opCkic);

	ilCount = olCcaList.GetSize();
	
	olCcaList.Sort(CompareCcaForCkbs);

	for( int i = 0; i < ilCount; i++)
	{
		olSaveCcaList.New( olCcaList[i]);
		olCcaList[i].Stat[2] = '0';
	}

	DIACCADATA *prlActualCca;
	DIACCADATA *prlPreviousCca = NULL;
	
	
	CTime olPrevCcaBegin;
	CTime olPrevCcaEnd;
	CTime olActCcaBegin;
	CTime olActCcaEnd;
	for( i = 0; i < ilCount; i++)
	{
		prlActualCca = &olCcaList[i];

		if (strcmp(prlActualCca->Ckic, "159") == 0 && strcmp(prlActualCca->Flno, "OAL756") == 0)
			int devbuge = 0;

		for(int j = i + 1; j < ilCount; j++)
		{
			if(i != j)
			{
				prlPreviousCca = &olCcaList[j];

				if(prlPreviousCca->Urno != prlActualCca->Urno)
				{
					// get best times of previous cca
					CCAFLIGHTDATA *prlFlight;
					prlFlight = ogCcaDiaFlightData.GetFlightByUrno(prlPreviousCca->Flnu);							
					pogCcaDiagram->omViewer.GetStartAllocTime(prlPreviousCca, prlFlight, olPrevCcaBegin);
					pogCcaDiagram->omViewer.GetEndAllocTime(prlPreviousCca, prlFlight, olPrevCcaEnd);
					if(olPrevCcaEnd < olPrevCcaBegin)
					{
						olPrevCcaEnd = olPrevCcaBegin +  CTimeSpan(0, 0, 60, 0);
					}


					// get best times of actual cca
					prlFlight = ogCcaDiaFlightData.GetFlightByUrno(prlActualCca->Flnu);							
					pogCcaDiagram->omViewer.GetStartAllocTime(prlActualCca, prlFlight, olActCcaBegin);
					pogCcaDiagram->omViewer.GetEndAllocTime(prlActualCca, prlFlight, olActCcaEnd);
					if(olActCcaEnd < olActCcaBegin)
					{
						olActCcaEnd = olActCcaBegin +  CTimeSpan(0, 0, 60, 0);
					}



					if(IsReallyOverlapped(olPrevCcaBegin ,olPrevCcaEnd, olActCcaBegin, olActCcaEnd) ||	
						IsWithIn(olPrevCcaBegin, olPrevCcaEnd, olActCcaBegin, olActCcaEnd) || 	
						IsWithIn(olActCcaBegin, olActCcaEnd, olPrevCcaBegin, olPrevCcaEnd))
					{
						// Overlapping found, set status flag
						prlPreviousCca->Stat[2] = 'J';
						prlActualCca->Stat[2] = 'J';
					}
				}
			}
		}
	}


	for( i = 0; i < ilCount; i++)
	{
		if(olCcaList[i].Stat[2] != olSaveCcaList[i].Stat[2])
		{
			blChangesFound = true;
		}
		if(olCcaList[i].Stat[2] == '0' && olCcaList[i].Stat[3] != '0')
		{
			olCcaList[i].Stat[3] = '0';
			blChangesFound = true;
		}
		//olCcaList[i].Brush = GetConflictBrushByCcaStatus(&olCcaList[i]);

	}

	olSaveCcaList.DeleteAll();
	olCcaList.RemoveAll();
	return blChangesFound;
}


bool DataSet::CheckDemands( const CCAFLIGHTDATA *prpFlightD , CCSPtrArray<DIACCADATA> &olCcaChangedList)
{
	if(prpFlightD == NULL)
		return false;


	if(ogBCD.GetDataCount("GHP") == 0)
		return false;


	if (!IsDepartureFlight(prpFlightD->Org3, prpFlightD->Des3, prpFlightD->Ftyp[0]))
		return false;

	const int ilUrnoIndex = ogBCD.GetFieldIndex("GHP", "URNO");

	// get rule(s) for flight
	CCSPtrArray<RecordSet> *polGHPs = new CCSPtrArray<RecordSet>;
	ogDataSet.GetGhpDataForFlight(polGHPs, NULL, prpFlightD);
	const int ilFoundRules = polGHPs->GetSize();

	// delete internal demands and demands of old rules
	const RecordSet *prlGHP = NULL;
	long llGhpu = 0;
	if (ilFoundRules == 1)
	{
		prlGHP = &(*polGHPs)[0];
		llGhpu = atol(prlGHP->Values[ilUrnoIndex].GetBuffer(0));
	}
	ogCcaDiaFlightData.omCcaData.DelAllDemByFlnuGhpu(prpFlightD->Urno, llGhpu);

	// get cca recs
	CCSPtrArray<DIACCADATA> olFlightCcaList;				
	ogCcaDiaFlightData.omCcaData.GetCcasByFlnu(olFlightCcaList, prpFlightD->Urno);


	if (ilFoundRules == 0)
	{
		// NO RULES FOUND !!

		DIACCADATA *prlNewCca = new DIACCADATA;

		strcpy(prlNewCca->Stat, "0000000000");
		prlNewCca->Stat[9] = 'N';
	
		prlNewCca->Urno = ogBasicData.GetNextUrno();
		prlNewCca->IsChanged = DATA_NEW_INTERNAL;
		strcpy(prlNewCca->Ctyp, ""); // ' ' == Flight
		prlNewCca->Cdat = CTime::GetCurrentTime();
		prlNewCca->Ckbs = prpFlightD->Stod - CTimeSpan(0, 0, 60, 0);
		prlNewCca->Ckes = prpFlightD->Stod;
		strcpy(prlNewCca->Ckic, "");  //Not Allocated
		prlNewCca->Flnu = prpFlightD->Urno;
		strcpy(prlNewCca->Usec, ogBasicData.omUserID);
		strcpy(prlNewCca->Useu, ogBasicData.omUserID);
		strcpy(prlNewCca->Disp, "");
		strcpy(prlNewCca->Act3, prpFlightD->Act3); 
		strcpy(prlNewCca->Flno, prpFlightD->Flno);
		prlNewCca->Ghpu = 0;//atol(prlGHP->Values[ilUrnoIndex].GetBuffer(0));
		prlNewCca->Gpmu = 0;//atol(prlGPM->Values[ilGpmUrnoIndex]);
		prlNewCca->Ghsu = 0;//atol(prlGPM->Values[ilGhsuIndex].GetBuffer(0));
		strcpy(prlNewCca->Cgru, "");

		olCcaChangedList.Add(prlNewCca);

		for(int  k = olFlightCcaList.GetSize() - 1; k >= 0; k--)
		{
			if (strlen(olFlightCcaList[k].Ckic) == 0)
			{
				//olFlightCcaList[k].IsChanged = DATA_DELETED;
			}
			else
			{
				olFlightCcaList[k].Stat[5] = 'J';
				olFlightCcaList[k].IsChanged = DATA_CHANGED;

				olCcaChangedList.Add(&olFlightCcaList[k]);
			}
		}
		polGHPs->DeleteAll();	
		olFlightCcaList.RemoveAll();
		delete polGHPs;
		polGHPs = NULL;

		return true;
	}

	if (ilFoundRules > 1)
	{
		// MORE THAN ONE RULE FOUND !!
		DIACCADATA *prlNewCca = new DIACCADATA;
		for(int ilLc = 0; ilLc < ilFoundRules; ilLc++)
		{
			prlGHP = &(*polGHPs)[ilLc];
			sprintf(prlNewCca->Disp, "%s;%s;", prlNewCca->Disp, prlGHP->Values[ilUrnoIndex].GetBuffer(0));
		}

		strcpy(prlNewCca->Stat, "0000000000");
		prlNewCca->Stat[9] = 'X';

		prlNewCca->Urno = ogBasicData.GetNextUrno();
		prlNewCca->IsChanged = DATA_NEW_INTERNAL;
		strcpy(prlNewCca->Ctyp, ""); // ' ' == Flight
		prlNewCca->Cdat = CTime::GetCurrentTime();
		prlNewCca->Ckbs = prpFlightD->Stod - CTimeSpan(0, 0, 60, 0);
		prlNewCca->Ckes = prpFlightD->Stod;
		strcpy(prlNewCca->Ckic, "");  //Not Allocated
		prlNewCca->Flnu = prpFlightD->Urno;
		strcpy(prlNewCca->Usec, ogBasicData.omUserID);
		strcpy(prlNewCca->Useu, ogBasicData.omUserID);
		strcpy(prlNewCca->Act3, prpFlightD->Act3); 
		strcpy(prlNewCca->Flno, prpFlightD->Flno);
		prlNewCca->Ghpu = 0;//atol(prlGHP->Values[ilUrnoIndex].GetBuffer(0));
		prlNewCca->Gpmu = 0;//atol(prlGPM->Values[ilGpmUrnoIndex]);
		prlNewCca->Ghsu = 0;//atol(prlGPM->Values[ilGhsuIndex].GetBuffer(0));
		strcpy(prlNewCca->Cgru, "");

		olCcaChangedList.Add(prlNewCca);

		for(int  k = olFlightCcaList.GetSize() - 1; k >= 0; k--)
		{
			if (strlen(olFlightCcaList[k].Ckic) == 0)
			{
				//olFlightCcaList[k].IsChanged = DATA_DELETED;
			}
			else
			{
				olFlightCcaList[k].Stat[5] = 'J';
				olFlightCcaList[k].IsChanged = DATA_CHANGED;

				olCcaChangedList.Add(&olFlightCcaList[k]);
			}
		}
		polGHPs->DeleteAll();	
		olFlightCcaList.RemoveAll();
		delete polGHPs;
		polGHPs = NULL;

		return true;
	}


	// EXACTLY ONE RULE WAS FOUND !!
	prlGHP = &(*polGHPs)[0];


	RecordSet *prlGPM;
	CCSPtrArray<RecordSet> *polGPMs = new CCSPtrArray<RecordSet>;

	// check allocations and demands
	const int ilDemCount = ogDataSet.GetGPMs(prlGHP->Values[ilUrnoIndex], polGPMs);

	if(ilDemCount == 0)
	{
		// RULE HAS NO DEMANDS				
		DIACCADATA *prlNewCca = new DIACCADATA;
		strcpy(prlNewCca->Stat, "0000000000");
		prlNewCca->Stat[9] = 'W';
		prlNewCca->Urno = ogBasicData.GetNextUrno();
		prlNewCca->IsChanged = DATA_NEW_INTERNAL;
		strcpy(prlNewCca->Ctyp, ""); 
		prlNewCca->Cdat = CTime::GetCurrentTime();
		prlNewCca->Ckbs = prpFlightD->Stod - CTimeSpan(0, 0, 60, 0);
		prlNewCca->Ckes = prpFlightD->Stod;
		strcpy(prlNewCca->Ckic, "");  //Not Allocated
		prlNewCca->Flnu = prpFlightD->Urno;
		strcpy(prlNewCca->Usec, ogBasicData.omUserID);
		strcpy(prlNewCca->Useu, ogBasicData.omUserID);
		sprintf(prlNewCca->Disp, "%s",  prlGHP->Values[ilUrnoIndex]);//Keine Bedarfe in der Regel
		strcpy(prlNewCca->Act3, prpFlightD->Act3); 
		strcpy(prlNewCca->Flno, prpFlightD->Flno);
		prlNewCca->Ghpu = atol(prlGHP->Values[ilUrnoIndex].GetBuffer(0));
		prlNewCca->Gpmu = 0;//atol(prlGPM->Values[ilGpmUrnoIndex]);
		prlNewCca->Ghsu = 0;//atol(prlGPM->Values[ilGhsuIndex].GetBuffer(0));
		strcpy(prlNewCca->Cgru, "");

		olCcaChangedList.Add(prlNewCca);

		for(int  k = olFlightCcaList.GetSize() - 1; k >= 0; k--)
		{
			if (strlen(olFlightCcaList[k].Ckic) == 0)
			{
				//olFlightCcaList[k].IsChanged = DATA_DELETED;
			}
			else
			{
				olFlightCcaList[k].Stat[5] = 'J';
				olFlightCcaList[k].IsChanged = DATA_CHANGED;

				olCcaChangedList.Add(&olFlightCcaList[k]);
			}
		}
		polGHPs->DeleteAll();	
		delete polGHPs;
		polGHPs = NULL;

		polGPMs->DeleteAll();	
		delete polGPMs;
		polGPMs = NULL;
		return true;

	}

	int ilDtimIndex = ogBCD.GetFieldIndex("GPM", "DTIM");
	int ilPotaIndex = ogBCD.GetFieldIndex("GPM", "POTA");
	int ilPrtaIndex = ogBCD.GetFieldIndex("GPM", "PRTA");

	int ilGhsuIndex = ogBCD.GetFieldIndex("GPM", "GHSU");
	int ilPermIndex = ogBCD.GetFieldIndex("GPM", "PERM");
	int ilGpmUrnoIndex = ogBCD.GetFieldIndex("GPM", "URNO");

	const long llRuleGHPUrno = atol(prlGHP->Values[ilUrnoIndex].GetBuffer(0));


//#######
	CUIntArray olDem;
	for(int l = ilDemCount - 1; l >= 0; l--)
		olDem.Add(0);



	for(int k = 0; k < olFlightCcaList.GetSize(); k++)
	{							
		//strcpy(olFlightCcaList[k].Stat, "0000000000");
		if (llRuleGHPUrno != olFlightCcaList[k].Ghpu && olFlightCcaList[k].Ghpu != 0)
		{
			// RULE HAS BEEN CHANGED
			olFlightCcaList[k].Stat[5] = '0';  
			olFlightCcaList[k].Stat[6] = 'J';  
			olFlightCcaList[k].IsChanged = DATA_CHANGED;

			olCcaChangedList.Add(&olFlightCcaList[k]);

			continue;
		}

		if (/*olFlightCcaList[k].Stod != TIMENULL && */olFlightCcaList[k].Stod != prpFlightD->Stod)
		{
			// STOD HAS BEEN CHANGED
			if( olFlightCcaList[k].Stat[8] != 'J')
			{
				olFlightCcaList[k].Stat[8] = 'J';  
				olFlightCcaList[k].IsChanged = DATA_CHANGED;
				olCcaChangedList.Add(&olFlightCcaList[k]);
			}
			continue;
		}


		if(strcmp(prpFlightD->Act3, olFlightCcaList[k].Act3 ) != 0)
		{
			// ACT HAS BEEN CHANGED
			if( olFlightCcaList[k].Stat[7] != 'J')
			{
				olFlightCcaList[k].Stat[7] = 'J';  
				olFlightCcaList[k].IsChanged = DATA_CHANGED;
				olCcaChangedList.Add(&olFlightCcaList[k]);
			}
			continue;
		}


		CString olTmpFlightFlno(prpFlightD->Flno);
		CString olTmpFlightFlno2(olFlightCcaList[k].Flno);
		olTmpFlightFlno.TrimRight();
		olTmpFlightFlno2.TrimRight();
		if( olTmpFlightFlno != olTmpFlightFlno2 )
		{
			// FLNO HAS BEEN CHANGED
			if ( olFlightCcaList[k].Stat[4] != 'J')
			{
				olFlightCcaList[k].Stat[4] = 'J';
				olFlightCcaList[k].IsChanged = DATA_CHANGED;
				olCcaChangedList.Add(&olFlightCcaList[k]);
			}
			continue;
		}


		// check if demand exists
		for(int i = ilDemCount - 1; i >= 0; i--)
		{
			prlGPM = &(*polGPMs)[i];



/*
			long gpmu =atol(prlGPM->Values[ilGpmUrnoIndex]);
			long ghsu =atol(prlGPM->Values[ilGhsuIndex].GetBuffer(0));
			CString cgru =prlGPM->Values[ilPermIndex];  
			DIACCADATA *prlCca2 = &olFlightCcaList[k];
*/
			
			if( olFlightCcaList[k].Ghpu == llRuleGHPUrno &&
				olFlightCcaList[k].Gpmu == atol(prlGPM->Values[ilGpmUrnoIndex]) &&
				olFlightCcaList[k].Ghsu == atol(prlGPM->Values[ilGhsuIndex].GetBuffer(0)) &&
				olFlightCcaList[k].Stod == prpFlightD->Stod &&
				strcmp(olFlightCcaList[k].Cgru, prlGPM->Values[ilPermIndex]) == 0)
			{
				if (olDem.GetAt(i) == 0)
					olDem.SetAt(i,k);
				else
					i = -1;
				break;
			}
		}
		if (i < 0)
		{
			// demand not found !!
			if (olFlightCcaList[k].Stat[5] != 'J')
			{
				olFlightCcaList[k].Ghpu = 0;
				olFlightCcaList[k].Gpmu = 0;
				olFlightCcaList[k].Ghsu = 0;
				strcpy(olFlightCcaList[k].Cgru, "");

				olFlightCcaList[k].Stat[5] = 'J';
				olFlightCcaList[k].IsChanged = DATA_CHANGED;

				olCcaChangedList.Add(&olFlightCcaList[k]);
			}
		}
	}

 
	// check if all demands exists
	for(int i = ilDemCount - 1; i >= 0; i--)
	{
		prlGPM = &(*polGPMs)[i];
		for(int k = 0; k < olFlightCcaList.GetSize(); k++)
		{									
			DIACCADATA *prlCca2 = &olFlightCcaList[k];

//			long gpmu =atol(prlGPM->Values[ilGpmUrnoIndex]);
//			long ghsu =atol(prlGPM->Values[ilGhsuIndex].GetBuffer(0));
//			CString cgru =prlGPM->Values[ilPermIndex];  


			if( olFlightCcaList[k].Ghpu == llRuleGHPUrno &&
				olFlightCcaList[k].Gpmu == atol(prlGPM->Values[ilGpmUrnoIndex]) &&
				olFlightCcaList[k].Ghsu == atol(prlGPM->Values[ilGhsuIndex].GetBuffer(0)) &&
				olFlightCcaList[k].Stod == prpFlightD->Stod &&
				strcmp(olFlightCcaList[k].Cgru, prlGPM->Values[ilPermIndex]) == 0)
				 break;
		}

		if (k >= olFlightCcaList.GetSize())
		{
			// create new demand
			CTimeSpan olPota(0,0, atoi(prlGPM->Values[ilPotaIndex]),0);
			CTimeSpan olDtim(0,0, atoi(prlGPM->Values[ilDtimIndex]),0);
			CTimeSpan olPrta(0,0, atoi(prlGPM->Values[ilPrtaIndex]),0);

			DIACCADATA *prlNewCca = new DIACCADATA;
			prlNewCca->Urno = ogBasicData.GetNextUrno();
			prlNewCca->IsChanged = DATA_NEW;
			prlNewCca->Cdat = CTime::GetCurrentTime();
			prlNewCca->Ckbs = prpFlightD->Stod - olDtim - olPrta;
			prlNewCca->Ckes = prpFlightD->Stod + olPota;
			prlNewCca->Stod = prpFlightD->Stod;
			strcpy(prlNewCca->Ckic, "");  //Not Allocated
			strcpy(prlNewCca->Ctyp, "");
			prlNewCca->Flnu = prpFlightD->Urno;
			strcpy(prlNewCca->Usec, ogBasicData.omUserID);
			strcpy(prlNewCca->Useu, ogBasicData.omUserID);
			//prlNewCca->Disp
			strcpy(prlNewCca->Act3, prpFlightD->Act3); 
			strcpy(prlNewCca->Flno, prpFlightD->Flno);
			strcpy(prlNewCca->Stat, "0000000000");
			prlNewCca->Ghpu = llRuleGHPUrno;
			prlNewCca->Gpmu = atol(prlGPM->Values[ilGpmUrnoIndex]);
			prlNewCca->Ghsu = atol(prlGPM->Values[ilGhsuIndex].GetBuffer(0));
			strcpy(prlNewCca->Cgru, prlGPM->Values[ilPermIndex]);

			olCcaChangedList.Add(prlNewCca);
		}
	}


	// unconfirm conflicts if necessary
	for(int ilLc = olFlightCcaList.GetSize() - 1; ilLc >= 0 ; ilLc--)
	{
		if( olFlightCcaList[ilLc].Stat[9] == '0' && olFlightCcaList[ilLc].IsChanged == DATA_CHANGED)
		{
			olFlightCcaList[ilLc].Stat[3] = '0';
		}
	}
	

	if (polGHPs) 
	{
		polGHPs->DeleteAll();	
		delete polGHPs;
	}

	if (polGPMs)
	{
		polGPMs->DeleteAll();	
		delete polGPMs;
	}

	olDem.RemoveAll();
	return true;


}



bool DataSet::CheckDemands( const CCAFLIGHTDATA *prpFlightD, const CCSPtrArray<CCADATA> &ropFlightCcaList, CCSPtrArray<CCADATA> &ropFlightCcaDemList)
{
	if(prpFlightD == NULL)
		return false;


	if(ogBCD.GetDataCount("GHP") == 0)
		return false;


	if (!IsDepartureFlight(prpFlightD->Org3, prpFlightD->Des3, prpFlightD->Ftyp[0]))
		return false;


	// get rule(s) for flight
	CCSPtrArray<RecordSet> *polGHPs = new CCSPtrArray<RecordSet>;
	ogDataSet.GetGhpDataForFlight(polGHPs, NULL, prpFlightD);
	if (polGHPs->GetSize() != 1)
	{
		polGHPs->DeleteAll();	
		delete polGHPs;
		return false; // only generate demands if exact one rule was found 
	}

	RecordSet *prlGHP = &(*polGHPs)[0];
	if (prlGHP == NULL)
	{
		polGHPs->DeleteAll();	
		delete polGHPs;
		return false;
	}




	int ilUrnoIndex = ogBCD.GetFieldIndex("GHP", "URNO");
	int ilDtimIndex = ogBCD.GetFieldIndex("GPM", "DTIM");
	int ilPotaIndex = ogBCD.GetFieldIndex("GPM", "POTA");
	int ilPrtaIndex = ogBCD.GetFieldIndex("GPM", "PRTA");

	int ilGhsuIndex = ogBCD.GetFieldIndex("GPM", "GHSU");
	int ilPermIndex = ogBCD.GetFieldIndex("GPM", "PERM");
	int ilGpmUrnoIndex = ogBCD.GetFieldIndex("GPM", "URNO");
	

	RecordSet *prlGPM;

	CCSPtrArray<RecordSet> *polGPMs = new CCSPtrArray<RecordSet>;

	// check allocations and demands
	int ilCount2 = ogDataSet.GetGPMs(prlGHP->Values[ilUrnoIndex], polGPMs);

	for(int i = ilCount2 - 1; i >= 0; i--)
	{
		prlGPM = &(*polGPMs)[i];
		for(int k = 0; k < ropFlightCcaList.GetSize(); k++)
		{							
			if( ropFlightCcaList[k].Ghpu == atol(prlGHP->Values[ilUrnoIndex].GetBuffer(0)) &&
				ropFlightCcaList[k].Gpmu == atol(prlGPM->Values[ilGpmUrnoIndex]) &&
				ropFlightCcaList[k].Ghsu == atol(prlGPM->Values[ilGhsuIndex].GetBuffer(0)) &&
				ropFlightCcaList[k].Stod == prpFlightD->Stod &&
				strcmp(ropFlightCcaList[k].Cgru, prlGPM->Values[ilPermIndex]) == 0)
				 break;
		}

		if (k >= ropFlightCcaList.GetSize())
		{
			// create new demand
			CTimeSpan olPota(0,0, atoi(prlGPM->Values[ilPotaIndex]),0);
			CTimeSpan olDtim(0,0, atoi(prlGPM->Values[ilDtimIndex]),0);
			CTimeSpan olPrta(0,0, atoi(prlGPM->Values[ilPrtaIndex]),0);

			CCADATA *prlNewCca = new CCADATA;
			prlNewCca->Urno = ogBasicData.GetNextUrno();
			prlNewCca->IsChanged = DATA_NEW;
			prlNewCca->Cdat = CTime::GetCurrentTime();
			prlNewCca->Ckbs = prpFlightD->Stod - olDtim - olPrta;
			prlNewCca->Ckes = prpFlightD->Stod + olPota;
			prlNewCca->Stod = prpFlightD->Stod;
			strcpy(prlNewCca->Ckic, "");  //Not Allocated
			strcpy(prlNewCca->Ctyp, "");
			prlNewCca->Flnu = prpFlightD->Urno;
			strcpy(prlNewCca->Usec, ogBasicData.omUserID);
			strcpy(prlNewCca->Useu, ogBasicData.omUserID);
			//prlNewCca->Disp
			strcpy(prlNewCca->Act3, prpFlightD->Act3); 
			strcpy(prlNewCca->Flno, prpFlightD->Flno);
			strcpy(prlNewCca->Stat, "0000000000");
			prlNewCca->Ghpu = atol(prlGHP->Values[ilUrnoIndex].GetBuffer(0));
			prlNewCca->Gpmu = atol(prlGPM->Values[ilGpmUrnoIndex]);
			prlNewCca->Ghsu = atol(prlGPM->Values[ilGhsuIndex].GetBuffer(0));
			strcpy(prlNewCca->Cgru, prlGPM->Values[ilPermIndex]);

			ropFlightCcaDemList.Add(prlNewCca);
		}
		else
		{
			if (strlen(ropFlightCcaList[k].Ckic) == 0)
			{
				// copy demand !!
				CCADATA *prlNewCca = new CCADATA;
				*prlNewCca = ropFlightCcaList[k];
				ropFlightCcaDemList.Add(prlNewCca);
			}
		}
	}


	polGHPs->DeleteAll();	
	delete polGHPs;

	polGPMs->DeleteAll();	
	delete polGPMs;

	return true;

}





void DataSet::RelHdl( const CCAFLIGHTDATA *prpFlightD, bool bpDdx )
{
	CCSPtrArray<DIACCADATA> olCcaChangedList;
	CheckDemands(prpFlightD, olCcaChangedList);
	ogCcaDiaFlightData.omCcaData.SaveInternal(&olCcaChangedList);
	olCcaChangedList.RemoveAll();
}


void DataSet::CheckDemandsOld( const CCAFLIGHTDATA *prpFlightD , CCSPtrArray<DIACCADATA> &olCcaChangedList)
{
	if(prpFlightD == NULL)
		return;


	if(ogBCD.GetDataCount("GHP") == 0)
		return;


	if(strcmp(prpFlightD->Adid, "D") != 0)
		return;


	DIACCADATA *prlTmpCca;
	const CCAFLIGHTDATA *prlFlight  = prpFlightD;

	CCSPtrArray<RecordSet> *polGHPs;
	CCSPtrArray<RecordSet> *polGPMs;
	RecordSet *prlGHP;
	RecordSet *prlGPM;

	int ilUrnoIndex = ogBCD.GetFieldIndex("GHP", "URNO");
	int ilDtimIndex = ogBCD.GetFieldIndex("GPM", "DTIM");
	int ilPotaIndex = ogBCD.GetFieldIndex("GPM", "POTA");

	int ilGhsuIndex = ogBCD.GetFieldIndex("GPM", "GHSU");
	int ilPermIndex = ogBCD.GetFieldIndex("GPM", "PERM");
	int ilPrioIndex = ogBCD.GetFieldIndex("GPM", "PRIO");
	int ilPrtaIndex = ogBCD.GetFieldIndex("GPM", "PRTA");
	int ilGpmUrnoIndex = ogBCD.GetFieldIndex("GPM", "URNO");
	
	int ilNewGhpu = 0;
	int ilOldGhpu = 0;
	bool blHasAllocated = false;
	int ilDemandCount;

	
	polGHPs = new CCSPtrArray<RecordSet>;
	// get rule(s) for flight
	ogDataSet.GetGhpDataForFlight(polGHPs, NULL, prlFlight);

	const int ilFoundRules = polGHPs->GetSize();
	long llGhpu = 0;
	if (ilFoundRules == 1)
	{
		prlGHP = &(*polGHPs)[0];
		llGhpu = atol(prlGHP->Values[ilUrnoIndex].GetBuffer(0));
	}

	ogCcaDiaFlightData.omCcaData.DelAllDemByFlnuGhpu(prlFlight->Urno, llGhpu);
				
	CCSPtrArray<DIACCADATA> olFlightCcaList;				
	ogCcaDiaFlightData.omCcaData.GetCcasByFlnu(olFlightCcaList, prlFlight->Urno);

	ilDemandCount = 0;
	ilOldGhpu = 0;

	char clTmp;

	for(int  k = olFlightCcaList.GetSize() - 1; k >= 0; k--)
	{

		prlTmpCca = &olFlightCcaList[k];

		olFlightCcaList[k].IsChanged = DATA_UNCHANGED;

		if(olFlightCcaList[k].Ghpu != 0)
		{
			ilOldGhpu = olFlightCcaList[k].Ghpu;
			ilDemandCount++;
		}

		if(	olFlightCcaList[k].Stat[9] != '0')
		{
			olFlightCcaList[k].IsChanged = DATA_DEL_INTERNAL;
		}
		else
		{
			// NO ERROR Flag
			if(  strcmp(olFlightCcaList[k].Ckic, "") == 0) 
			{
				// keine Allokation
				if(CString(olFlightCcaList[k].Act3) != CString(prlFlight->Act3) ) 
				{
					strcpy( olFlightCcaList[k].Act3, prlFlight->Act3);
					olFlightCcaList[k].IsChanged = DATA_CHANGED;
				}

				if(olFlightCcaList[k].Stod != prlFlight->Stod) 
				{
					olFlightCcaList[k].Stod = prlFlight->Stod;
					olFlightCcaList[k].IsChanged = DATA_CHANGED;
				}

				CString olTmpFlightFlno = CString(prlFlight->Flno);
				olTmpFlightFlno.TrimRight();

				if( olTmpFlightFlno != CString(olFlightCcaList[k].Flno) ) 
				{
					strcpy(olFlightCcaList[k].Flno, olTmpFlightFlno);
					olFlightCcaList[k].IsChanged = DATA_CHANGED;
				}
			}

		}


		if(  (strcmp(olFlightCcaList[k].Ckic, "") != 0)  && 
		     (strcmp(olFlightCcaList[k].Ctyp, "") == 0)  && 
		     ( olFlightCcaList[k].Stat[9] == '0') ) 
		   
		{
			blHasAllocated = true;

		/*
			olFlightCcaList[k].Stat[4] = '0';  
			olFlightCcaList[k].Stat[5] = '0';  
			olFlightCcaList[k].Stat[6] = '0';  
			olFlightCcaList[k].Stat[7] = '0';  
			olFlightCcaList[k].Stat[8] = '0';  
			olFlightCcaList[k].Stat[9] = '0';  
		*/
			// Check aircrafttype



				if(CString(olFlightCcaList[k].Act3).IsEmpty() ) 
				{
					strcpy( olFlightCcaList[k].Act3, prlFlight->Act3);
					olFlightCcaList[k].IsChanged = DATA_CHANGED;
				}

				if(olFlightCcaList[k].Stod == TIMENULL) 
				{
					olFlightCcaList[k].Stod = prlFlight->Stod;
					olFlightCcaList[k].IsChanged = DATA_CHANGED;
				}

				if(CString(olFlightCcaList[k].Flno).IsEmpty() ) 
				{
					strcpy( olFlightCcaList[k].Flno, prlFlight->Flno);
					olFlightCcaList[k].IsChanged = DATA_CHANGED;
				}



				if(strcmp(prlFlight->Act3, olFlightCcaList[k].Act3 ) == 0) 
					clTmp = '0';
				else
					clTmp = 'J';

				if( olFlightCcaList[k].Stat[7] != clTmp)
						olFlightCcaList[k].IsChanged = DATA_CHANGED;

				olFlightCcaList[k].Stat[7] = clTmp;  


				if( prlFlight->Stod == olFlightCcaList[k].Stod ) 
					clTmp = '0';
				else
					clTmp = 'J';

				if( olFlightCcaList[k].Stat[8] != clTmp)
					olFlightCcaList[k].IsChanged = DATA_CHANGED;

				olFlightCcaList[k].Stat[8] = clTmp;  



				CString olTmpFlightFlno = CString(prlFlight->Flno);
				CString olTmpFlightFlno2 = CString(olFlightCcaList[k].Flno);
				olTmpFlightFlno.TrimRight();
				olTmpFlightFlno2.TrimRight();

				if( olTmpFlightFlno == olTmpFlightFlno2 ) 
					clTmp = '0';
				else
					clTmp = 'J';

				if( olFlightCcaList[k].Stat[4] != clTmp)
					olFlightCcaList[k].IsChanged = DATA_CHANGED;

				olFlightCcaList[k].Stat[4] = clTmp;  


				// Check if demand exists
				if((olFlightCcaList[k].Ghpu == 0) || (olFlightCcaList[k].Ghsu == 0))
					clTmp = 'J';
				else
					clTmp = '0';

				if( olFlightCcaList[k].Stat[5] != clTmp)
					olFlightCcaList[k].IsChanged = DATA_CHANGED;

				olFlightCcaList[k].Stat[5] = clTmp;  
		}
		olCcaChangedList.Add(&olFlightCcaList[k]);

	}

	
	ilNewGhpu = 0;


	polGPMs = new CCSPtrArray<RecordSet>;

		
	if(ilFoundRules == 0)
	{
		//strcpy(prlFlight->Remark, GetString(IDS_STRING1448)); // Keine regel gefunden
		DIACCADATA *prlNewCca = new DIACCADATA;

		strcpy(prlNewCca->Stat, "0000000000");
		prlNewCca->Stat[9] = 'N';
	
		//strcpy(prlNewCca->Disp, GetString(IDS_STRING1448));
		prlNewCca->Urno = ogBasicData.GetNextUrno();
		prlNewCca->IsChanged = DATA_NEW_INTERNAL;
		strcpy(prlNewCca->Ctyp, ""); // ' ' == Flight
		prlNewCca->Cdat = CTime::GetCurrentTime();
		prlNewCca->Ckbs = prlFlight->Stod - CTimeSpan(0, 0, 60, 0);
		prlNewCca->Ckes = prlFlight->Stod;
		strcpy(prlNewCca->Ckic, "");  //Not Allocated
		prlNewCca->Flnu = prlFlight->Urno;
		strcpy(prlNewCca->Usec, ogBasicData.omUserID);
		strcpy(prlNewCca->Useu, ogBasicData.omUserID);
		strcpy(prlNewCca->Disp, "");
		strcpy(prlNewCca->Act3, prlFlight->Act3); 
		strcpy(prlNewCca->Flno, prlFlight->Flno);
		prlNewCca->Ghpu = 0;//atol(prlGHP->Values[ilUrnoIndex].GetBuffer(0));
		prlNewCca->Gpmu = 0;//atol(prlGPM->Values[ilGpmUrnoIndex]);
		prlNewCca->Ghsu = 0;//atol(prlGPM->Values[ilGhsuIndex].GetBuffer(0));
		strcpy(prlNewCca->Cgru, "");

		//ogCcaDiaFlightData.omCcaData.InsertInternal(prlNewCca);
		olCcaChangedList.Add(prlNewCca);

		for(int  k = olFlightCcaList.GetSize() - 1; k >= 0; k--)
		{

			if(blHasAllocated == true)
			{
				clTmp = 'J';

				if( olFlightCcaList[k].Stat[5] != clTmp && olFlightCcaList[k].Stat[9] == '0')
					olFlightCcaList[k].IsChanged = DATA_CHANGED;

				olFlightCcaList[k].Stat[5] = clTmp;  
			}
			else
			{
				olFlightCcaList[k].IsChanged = DATA_DELETED;
			}

		}

	}

	// Mehrere Regeln gefunden
	if(ilFoundRules > 1)
	{
		//strcpy(prlFlight->Remark, GetString(IDS_STRING1448)); // Keine regel gefunden
		DIACCADATA *prlNewCca = new DIACCADATA;
		for(int ilLc = 0; ilLc < ilFoundRules; ilLc++)
		{
			prlGHP = &(*polGHPs)[ilLc];
			sprintf(prlNewCca->Disp, "%s;%s;", prlNewCca->Disp, prlGHP->Values[ilUrnoIndex].GetBuffer(0));
		}

		strcpy(prlNewCca->Stat, "0000000000");
		prlNewCca->Stat[9] = 'X';

		prlNewCca->Urno = ogBasicData.GetNextUrno();
		prlNewCca->IsChanged = DATA_NEW_INTERNAL;
		strcpy(prlNewCca->Ctyp, ""); // ' ' == Flight
		prlNewCca->Cdat = CTime::GetCurrentTime();
		prlNewCca->Ckbs = prlFlight->Stod - CTimeSpan(0, 0, 60, 0);
		prlNewCca->Ckes = prlFlight->Stod;
		strcpy(prlNewCca->Ckic, "");  //Not Allocated
		prlNewCca->Flnu = prlFlight->Urno;
		strcpy(prlNewCca->Usec, ogBasicData.omUserID);
		strcpy(prlNewCca->Useu, ogBasicData.omUserID);
		strcpy(prlNewCca->Act3, prlFlight->Act3); 
		strcpy(prlNewCca->Flno, prlFlight->Flno);
		prlNewCca->Ghpu = 0;//atol(prlGHP->Values[ilUrnoIndex].GetBuffer(0));
		prlNewCca->Gpmu = 0;//atol(prlGPM->Values[ilGpmUrnoIndex]);
		prlNewCca->Ghsu = 0;//atol(prlGPM->Values[ilGhsuIndex].GetBuffer(0));
		strcpy(prlNewCca->Cgru, "");

		//ogCcaDiaFlightData.omCcaData.InsertInternal(prlNewCca);
		olCcaChangedList.Add(prlNewCca);

		for(int  k = olFlightCcaList.GetSize() - 1; k >= 0; k--)
		{
			if(blHasAllocated == true)
			{
				clTmp = 'J';

				if( olFlightCcaList[k].Stat[5] != clTmp && olFlightCcaList[k].Stat[9] == '0')
					olFlightCcaList[k].IsChanged = DATA_CHANGED;

				olFlightCcaList[k].Stat[5] = clTmp;  
			}						
			else
			{
				olFlightCcaList[k].IsChanged = DATA_DELETED;
			}

		}
	}

	if(ilFoundRules == 1)
	{
		prlGHP = &(*polGHPs)[0];
		if(prlGHP != NULL)
		{
			//strcpy(prlFlight->Remark, ogDataSet.GetRulesName(polGHPs, "GHP"));
			prlGHP = &(*polGHPs)[0];

			//prlFlight->Ghpu = atol(prlGHP->Values[ilUrnoIndex]);

			int ilCount2 = ogDataSet.GetGPMs(prlGHP->Values[ilUrnoIndex], polGPMs);

			
			ilNewGhpu = atol(prlGHP->Values[ilUrnoIndex].GetBuffer(0));


			// Regel ohne Leistung gefunden				
			if(ilCount2 == 0)
			{
				DIACCADATA *prlNewCca = new DIACCADATA;
				strcpy(prlNewCca->Stat, "0000000000");
				prlNewCca->Stat[9] = 'W';
				prlNewCca->Urno = ogBasicData.GetNextUrno();
				prlNewCca->IsChanged = DATA_NEW_INTERNAL;
				strcpy(prlNewCca->Ctyp, ""); 
				prlNewCca->Cdat = CTime::GetCurrentTime();
				prlNewCca->Ckbs = prlFlight->Stod - CTimeSpan(0, 0, 60, 0);
				prlNewCca->Ckes = prlFlight->Stod;
				strcpy(prlNewCca->Ckic, "");  //Not Allocated
				prlNewCca->Flnu = prlFlight->Urno;
				strcpy(prlNewCca->Usec, ogBasicData.omUserID);
				strcpy(prlNewCca->Useu, ogBasicData.omUserID);
				sprintf(prlNewCca->Disp, "%s",  prlGHP->Values[ilUrnoIndex]);//Keine Bedarfe in der Regel
				strcpy(prlNewCca->Act3, prlFlight->Act3); 
				strcpy(prlNewCca->Flno, prlFlight->Flno);
				prlNewCca->Ghpu = atol(prlGHP->Values[ilUrnoIndex].GetBuffer(0));
				prlNewCca->Gpmu = 0;//atol(prlGPM->Values[ilGpmUrnoIndex]);
				prlNewCca->Ghsu = 0;//atol(prlGPM->Values[ilGhsuIndex].GetBuffer(0));
				strcpy(prlNewCca->Cgru, "");
				//ogCcaDiaFlightData.omCcaData.InsertInternal(prlNewCca);
				olCcaChangedList.Add(prlNewCca);
			}



			if(blHasAllocated == true)
			{
				//int ilCount2 = ogDataSet.GetGPMs(prlGHP->Values[ilUrnoIndex], polGPMs);

				//if (ilCount2 != olFlightCcaList.GetSize())
				{
					// not all demands are allocated !!		
					for(int i = ilCount2 - 1; i >= 0; i--)
					{
						prlGPM = &(*polGPMs)[i];
						for(int k = 0; k < olFlightCcaList.GetSize(); k++)
						{							
							if( olFlightCcaList[k].Ghpu == atol(prlGHP->Values[ilUrnoIndex].GetBuffer(0)) &&
								olFlightCcaList[k].Gpmu == atol(prlGPM->Values[ilGpmUrnoIndex]) &&
								olFlightCcaList[k].Ghsu == atol(prlGPM->Values[ilGhsuIndex].GetBuffer(0)) &&
								olFlightCcaList[k].Stod == prlFlight->Stod &&
								CString(olFlightCcaList[k].Cgru) == CString(prlGPM->Values[ilPermIndex]))
								 break;
						}

						if (k >= olFlightCcaList.GetSize())
						{
							//neuer CCA-Datensatz erzeugen
							CTimeSpan olPota(0,0, atoi(prlGPM->Values[ilPotaIndex]),0);
							CTimeSpan olDtim(0,0, atoi(prlGPM->Values[ilDtimIndex]),0);

							DIACCADATA *prlNewCca = new DIACCADATA;
							prlNewCca->Urno = ogBasicData.GetNextUrno();
							prlNewCca->IsChanged = DATA_NEW;
							prlNewCca->Cdat = CTime::GetCurrentTime();
							prlNewCca->Ckbs = prlFlight->Stod - olDtim;
							prlNewCca->Ckes = prlFlight->Stod + olPota;
							prlNewCca->Stod = prlFlight->Stod;
							strcpy(prlNewCca->Ckic, "");  //Not Allocated
							strcpy(prlNewCca->Ctyp, "");
							prlNewCca->Flnu = prlFlight->Urno;
							strcpy(prlNewCca->Usec, ogBasicData.omUserID);
							strcpy(prlNewCca->Useu, ogBasicData.omUserID);
							//prlNewCca->Disp
							strcpy(prlNewCca->Act3, prlFlight->Act3); 
							strcpy(prlNewCca->Flno, prlFlight->Flno);
							strcpy(prlNewCca->Stat, "0000000000");
							prlNewCca->Ghpu = atol(prlGHP->Values[ilUrnoIndex].GetBuffer(0));
							prlNewCca->Gpmu = atol(prlGPM->Values[ilGpmUrnoIndex]);
							prlNewCca->Ghsu = atol(prlGPM->Values[ilGhsuIndex].GetBuffer(0));
							strcpy(prlNewCca->Cgru, prlGPM->Values[ilPermIndex]);

							olCcaChangedList.Add(prlNewCca);
						}
					}
				}
			

				if( ilNewGhpu != ilOldGhpu ) //|| (ilDemandCount != ilCount2)) // Regel hat sich ge�ndert
				{
					for(int k = 0; k < olFlightCcaList.GetSize(); k++)
					{
						if( olFlightCcaList[k].Stat[5] != '0' && olFlightCcaList[k].Stat[9] == '0')
							olFlightCcaList[k].IsChanged = DATA_CHANGED;

						olFlightCcaList[k].Stat[5] = '0';  

						
						if( olFlightCcaList[k].Stat[6] != 'J' && olFlightCcaList[k].Stat[9] == '0')
							olFlightCcaList[k].IsChanged = DATA_CHANGED;

						olFlightCcaList[k].Stat[6] = 'J';  
					}
				}
			}
			else
			{
				int ilTreffer = 0;
				if(ilCount2 > 0)
				{
					for(int i = ilCount2 - 1; i >= 0; i--)
					{
						for(int k = 0; k < olFlightCcaList.GetSize(); k++)
						{
							prlGPM = &(*polGPMs)[i];
							if(	olFlightCcaList[k].Ghpu == atol(prlGHP->Values[ilUrnoIndex].GetBuffer(0)) &&
								olFlightCcaList[k].Gpmu == atol(prlGPM->Values[ilGpmUrnoIndex]) &&
								olFlightCcaList[k].Ghsu == atol(prlGPM->Values[ilGhsuIndex].GetBuffer(0)) &&
								olFlightCcaList[k].Stod == prlFlight->Stod &&
								CString(olFlightCcaList[k].Cgru) == CString(prlGPM->Values[ilPermIndex]))
								ilTreffer++;
						}
					}


					if(ilTreffer != ilCount2)
					{

						/* TVO ???
						for(int ilLc = olFlightCcaList.GetSize() - 1; ilLc >= 0 ; ilLc--)
						{
							if( olFlightCcaList[ilLc].Stat[9] == '0')
							{
								olFlightCcaList[ilLc].IsChanged = DATA_DELETED;
							}
						}
						*/

						for(int i = ilCount2 - 1; i >= ilTreffer; i--)
						{

							//Die neuen CCA-Datens�tze erzeugen
							prlGPM = &(*polGPMs)[i];
							CTimeSpan olPota(0,0, atoi(prlGPM->Values[ilPotaIndex]),0);
							CTimeSpan olDtim(0,0, atoi(prlGPM->Values[ilDtimIndex]),0);

							DIACCADATA *prlNewCca = new DIACCADATA;
							prlNewCca->Urno = ogBasicData.GetNextUrno();
							prlNewCca->IsChanged = DATA_NEW;
							prlNewCca->Cdat = CTime::GetCurrentTime();
							prlNewCca->Ckbs = prlFlight->Stod - olDtim;
							prlNewCca->Ckes = prlFlight->Stod + olPota;
							prlNewCca->Stod = prlFlight->Stod;
							strcpy(prlNewCca->Ckic, "");  //Not Allocated
							strcpy(prlNewCca->Ctyp, "");
							prlNewCca->Flnu = prlFlight->Urno;
							strcpy(prlNewCca->Usec, ogBasicData.omUserID);
							strcpy(prlNewCca->Useu, ogBasicData.omUserID);
							//prlNewCca->Disp
							strcpy(prlNewCca->Act3, prlFlight->Act3); 
							strcpy(prlNewCca->Flno, prlFlight->Flno);
							strcpy(prlNewCca->Stat, "0000000000");
							prlNewCca->Ghpu = atol(prlGHP->Values[ilUrnoIndex].GetBuffer(0));
							prlNewCca->Gpmu = atol(prlGPM->Values[ilGpmUrnoIndex]);
							prlNewCca->Ghsu = atol(prlGPM->Values[ilGhsuIndex].GetBuffer(0));
							strcpy(prlNewCca->Cgru, prlGPM->Values[ilPermIndex]);

							olCcaChangedList.Add(prlNewCca);
						}
					}
				}
			}

		}//if(prlGHP != NULL)

	}//endif(ilFoundRules != 1)



	for(int ilLc = olFlightCcaList.GetSize() - 1; ilLc >= 0 ; ilLc--)
	{
		if( olFlightCcaList[ilLc].Stat[9] == '0' && olFlightCcaList[ilLc].IsChanged == DATA_CHANGED)
		{
			olFlightCcaList[ilLc].Stat[3] = '0';
		}
	}



	polGHPs->DeleteAll();	
	delete polGHPs;

	polGPMs->DeleteAll();	
	delete polGPMs;



}












CString DataSet::CreateFlno(CString opAlc, CString opFltn, CString opFlns)
{

	opAlc.TrimRight();	
	opFltn.TrimRight();	
	
	if(opAlc.IsEmpty() || opFltn.IsEmpty())
		return CString("");


	if(opAlc.GetLength() == 0)
		opAlc = "   ";

	if(opAlc.GetLength() == 1)
		opAlc += "  ";

	if(opAlc.GetLength() == 2)
		opAlc += " ";


	if(opFltn.GetLength() == 0)
		opFltn = "   ";

	if(opFltn.GetLength() == 1)
		opFltn = "00" + opFltn;

	if(opFltn.GetLength() == 2)
		opFltn = "0" + opFltn;

	if(opFltn.GetLength() == 3)
		opFltn += "  ";

	if(opFltn.GetLength() == 4)
		opFltn += " ";

	if(opFlns.GetLength() == 0)
		opFlns = " ";


	CString olFlno = opAlc + opFltn + opFlns;

	olFlno.TrimRight();

	if(olFlno.IsEmpty())
		return "";
	else
		return opAlc + opFltn + opFlns;

}



void DataSet::Init()
{

	ilGhpDopaIdx = ogBCD.GetFieldIndex("GHP","DOPA");
	ilGhpDopdIdx = ogBCD.GetFieldIndex("GHP","DOPD");
	ilGhpMaxtIdx = ogBCD.GetFieldIndex("GHP","MAXT");
	ilGhpRegnIdx = ogBCD.GetFieldIndex("GHP","REGN");
	ilGhpAct5Idx = ogBCD.GetFieldIndex("GHP","ACT5");
	ilGhpActmIdx = ogBCD.GetFieldIndex("GHP","ACTM");
	ilGhpFlcaIdx = ogBCD.GetFieldIndex("GHP","FLCA");
	ilGhpFlnaIdx = ogBCD.GetFieldIndex("GHP","FLNA");
	ilGhpFlsaIdx = ogBCD.GetFieldIndex("GHP","FLSA");
	ilGhpFlcdIdx = ogBCD.GetFieldIndex("GHP","FLCD");
	ilGhpFlndIdx = ogBCD.GetFieldIndex("GHP","FLND");
	ilGhpFlsdIdx = ogBCD.GetFieldIndex("GHP","FLSD");
	ilGhpAlmaIdx = ogBCD.GetFieldIndex("GHP","ALMA");
	ilGhpAlmdIdx = ogBCD.GetFieldIndex("GHP","ALMD");
	ilGhpOrigIdx = ogBCD.GetFieldIndex("GHP","ORIG");
	ilGhpDestIdx = ogBCD.GetFieldIndex("GHP","DEST");
	ilGhpVi1aIdx = ogBCD.GetFieldIndex("GHP","VI1A");
	ilGhpVi1dIdx = ogBCD.GetFieldIndex("GHP","VI1D");
	ilGhpTrraIdx = ogBCD.GetFieldIndex("GHP","TRRA");
	ilGhpTrrdIdx = ogBCD.GetFieldIndex("GHP","TRRD");
	ilGhpSccaIdx = ogBCD.GetFieldIndex("GHP","SCCA");
	ilGhpSccdIdx = ogBCD.GetFieldIndex("GHP","SCCD");
	ilGhpPcaaIdx = ogBCD.GetFieldIndex("GHP","PCAA");
	ilGhpPcadIdx = ogBCD.GetFieldIndex("GHP","PCAD");
	ilGhpTtpaIdx = ogBCD.GetFieldIndex("GHP","TTPA");
	ilGhpTtpdIdx = ogBCD.GetFieldIndex("GHP","TTPD");
	ilGhpTtgaIdx = ogBCD.GetFieldIndex("GHP","TTGA");
	ilGhpTtgdIdx = ogBCD.GetFieldIndex("GHP","TTGD");
	ilGhpHtpaIdx = ogBCD.GetFieldIndex("GHP","HTPA");
	ilGhpHtpdIdx = ogBCD.GetFieldIndex("GHP","HTPD");
	ilGhpHtgaIdx = ogBCD.GetFieldIndex("GHP","HTGA");
	ilGhpHtgdIdx = ogBCD.GetFieldIndex("GHP","HTGD");
	ilGhpPrsnIdx = ogBCD.GetFieldIndex("GHP","PRSN");
	ilGhpFliaIdx = ogBCD.GetFieldIndex("GHP","FLIA");
	ilGhpFlidIdx = ogBCD.GetFieldIndex("GHP","FLID");

	ilGhpVafrIdx = ogBCD.GetFieldIndex("GHP","VAFR");
	ilGhpVatoIdx = ogBCD.GetFieldIndex("GHP","VATO");

	ilGhpPrioIdx = ogBCD.GetFieldIndex("GHP","PRIO");
}
