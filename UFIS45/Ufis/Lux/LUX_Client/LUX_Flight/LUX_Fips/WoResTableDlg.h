// WoResTableDlg.h

// Modification History: 
//	081100	rkr		PRF44(Athen) OnDragOver() und OnDrop zum hinzufügen aus XXXGantt unterstützt


#if !defined(AFX_WORESTABLEDLG_H__39D1ABF1_278C_11D2_8588_0000C04D916B__INCLUDED_)
#define AFX_WORESTABLEDLG_H__39D1ABF1_278C_11D2_8588_0000C04D916B__INCLUDED_



#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// WoResTableDlg.h : header files
//

#include <WoResTableViewer.h>
#include <CCSTable.h>
#include <CCSDragDropCtrl.h>

/////////////////////////////////////////////////////////////////////////////
// WoResTableDlg dialog

class WoResTableDlg : public CDialog
{
// Construction
public:
	WoResTableDlg(CWnd* pParent = NULL);   // standard constructor
	~WoResTableDlg();

	void Activate(FipsModules ipMode, bool bpShow = true);
	void UpdateCaption();
	void Rebuild();

	CCSDragDropCtrl omDragDropObject;

	int GetCount(FipsModules ipMode) const;

	bool Connect();
	bool Disconnect();


// Dialog Data
	//{{AFX_DATA(WoResTableDlg)
	enum { IDD = IDD_WORESTABLE };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(WoResTableDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	FipsModules imMode;

	CCSTable *pomTable;
	WoResTableViewer *pomViewer;

	// Generated message map functions
	//{{AFX_MSG(WoResTableDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnClose();
	afx_msg LONG OnTableDragBegin(UINT wParam, LONG lParam);
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
    afx_msg LONG OnTableLButtonDown(UINT wParam, LONG lParam);
    afx_msg LONG OnDrop(UINT, LONG); 
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnTableReturnPressed(UINT wParam, LONG lParam);
	afx_msg LONG OnTableSelChanged( UINT wParam, LPARAM lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	int imConnectCounter;

	BOOL CheckPostFlight(const WORESTABLE_LINEDATA *prpTableLine);
	BOOL PostFlight(const DIAFLIGHTDATA *prpAFlight, const DIAFLIGHTDATA *prpDFlight);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WORESTABLEDLG_H__39D1ABF1_278C_11D2_8588_0000C04D916B__INCLUDED_)
