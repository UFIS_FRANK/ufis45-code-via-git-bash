// AskBox.cpp : implementation file
//

#include <stdafx.h>
#include <fpms.h>
#include <BasicData.h>
#include <AskBox.h>
#include <resrc1.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// AskBox dialog


AskBox::AskBox(CWnd* pParent , const CString &ropCaption, const CString &ropMessage, const CString &ropButton1Text, CString opButton2Text, CString opButton3Text, CString opCancelText)
	: CDialog(AskBox::IDD, pParent)
{
	omButtonText1 = ropButton1Text;
	omButtonText2 = opButton2Text;
	omButtonText3 = opButton3Text;
	omMessage = ropMessage;
	omCaption = ropCaption;
	omCancelText = opCancelText;
	
	bmButton1Enable = true;
	bmButton2Enable = true;
	bmButton3Enable = true;

	//{{AFX_DATA_INIT(AskBox)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


BOOL AskBox::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// get height of window
	CRect olWndRect;
	GetClientRect(olWndRect);
	int ilWndHeight = olWndRect.bottom;

	// get height of cancel button
	CRect olCancelRect;
	m_CB_Cancel.GetClientRect(olCancelRect);

	//// calculate button widths
	CSize olButton1Size;
	CSize olButton2Size;
	CSize olButton3Size;

	CDC *polDC = GetDC();
	CString olText;
	olText.Format(" %s ", omButtonText1);
	olButton1Size = polDC->GetTextExtent(olText);

	olText.Format(" %s ", omButtonText2);
	olButton2Size = polDC->GetTextExtent(olText);

	if (omButtonText3.GetLength() > 0)
	{
		olText.Format(" %s ", omButtonText3);
		olButton3Size = polDC->GetTextExtent(olText);
	}
	else
	{
		// hide button if no text is set!
		m_CB_Button3.ShowWindow(SW_HIDE);
	}


	//// set button size and pos
	m_CB_Button1.SetWindowPos(NULL, 10, ilWndHeight-10-olCancelRect.Height(), 
		olButton1Size.cx, olCancelRect.Height(), SWP_NOZORDER);
		
	m_CB_Button2.SetWindowPos(NULL, 10+olButton1Size.cx+10, ilWndHeight-10-olCancelRect.Height(), 
		olButton2Size.cx, olCancelRect.Height(), SWP_NOZORDER);

	m_CB_Button3.SetWindowPos(NULL, 10+olButton1Size.cx+10+olButton2Size.cx+10, ilWndHeight-10-olCancelRect.Height(), 
		olButton3Size.cx, olCancelRect.Height(), SWP_NOZORDER);

	// calculate width of the window
	int ilWndWidth = 10+olButton1Size.cx+10+olButton2Size.cx+10+olButton3Size.cx+20+olCancelRect.Width()+10;
	int ilMessWidth = GetMessageWidth(polDC);
	if (ilMessWidth + 10 > ilWndWidth)
		ilWndWidth = ilMessWidth + 10;

	// set cancel button pos
	m_CB_Cancel.SetWindowPos(NULL, ilWndWidth-10-olCancelRect.Width(), ilWndHeight-10-olCancelRect.Height(), 
		0, 0, SWP_NOZORDER | SWP_NOSIZE);


	// set button text
	m_CB_Button1.SetWindowText(omButtonText1);
	m_CB_Button2.SetWindowText(omButtonText2);
	m_CB_Button3.SetWindowText(omButtonText3);
	m_CS_Text.SetWindowText(omMessage);
	m_CB_Cancel.SetWindowText(omCancelText);
	SetWindowText(omCaption);

	// enable/disable buttons
	m_CB_Button1.EnableWindow(bmButton1Enable);
	m_CB_Button2.EnableWindow(bmButton2Enable);
	m_CB_Button3.EnableWindow(bmButton3Enable);


	// set window size and pos
	GetWindowRect(olWndRect);
	SetWindowPos( NULL, 400, 300, ilWndWidth ,olWndRect.Height(), SWP_NOZORDER);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


int AskBox::GetMessageWidth(CDC *popDC) const
{
	if (popDC == NULL)
		return 0;

	int ilMaxWidth = -1;
	int ilCurrWidth;
	CStringArray olStrs;
	int ilStrs = ExtractItemList(omMessage, &olStrs, '\n');

	for (int i = 0; i < ilStrs; i++)
	{
		ilCurrWidth = popDC->GetTextExtent(olStrs[i]).cx;
		ilMaxWidth = max(ilMaxWidth, ilCurrWidth);
	}

	return ilMaxWidth;
}


void AskBox::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(AskBox)
	DDX_Control(pDX, IDCANCEL, m_CB_Cancel);
	DDX_Control(pDX, IDC_BUTTON1, m_CB_Button1);
	DDX_Control(pDX, IDC_BUTTON2, m_CB_Button2);
	DDX_Control(pDX, IDC_BUTTON3, m_CB_Button3);
	DDX_Control(pDX, IDC_TEXT, m_CS_Text);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(AskBox, CDialog)
	//{{AFX_MSG_MAP(AskBox)
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	ON_BN_CLICKED(IDC_BUTTON2, OnButton2)
	ON_BN_CLICKED(IDC_BUTTON3, OnButton3)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// AskBox message handlers

void AskBox::OnButton1() 
{
	 EndDialog( 1 );
}

void AskBox::OnButton2() 
{
	 EndDialog( 2 );
}

void AskBox::OnButton3() 
{
	 EndDialog( 3 );
}

void AskBox::OnCancel() 
{
	
	 EndDialog( 0 );
}


void AskBox::EnableButtons(bool bpButton1 /*= true*/, bool bpButton2 /*= true*/, bool bpButton3 /*= true*/)
{
	bmButton1Enable = bpButton1;
	bmButton2Enable = bpButton2;
	bmButton3Enable = bpButton3;

}


