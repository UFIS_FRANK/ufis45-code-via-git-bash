// ReportArrDepSelctDlg.cpp : implementation file
//

#include <stdafx.h>
#include <fpms.h>
#include <CCSGlobl.h>
#include <ReportArrDepSelctDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ReportArrDepSelctDlg dialog


ReportArrDepSelctDlg::ReportArrDepSelctDlg(CWnd* pParent /*=NULL*/)
	: CDialog(ReportArrDepSelctDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(ReportArrDepSelctDlg)
	m_ListArrDep = -1;
	//}}AFX_DATA_INIT
}


void ReportArrDepSelctDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ReportArrDepSelctDlg)
	DDX_Radio(pDX, IDC_LIST_ARRIVAL, m_ListArrDep);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ReportArrDepSelctDlg, CDialog)
	//{{AFX_MSG_MAP(ReportArrDepSelctDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ReportArrDepSelctDlg message handlers

void ReportArrDepSelctDlg::OnOK() 
{

	// TODO: Add extra validation here
	UpdateData(TRUE);

	if (m_ListArrDep != -1)
	{
		CDialog::OnOK();
	}
	else
	{
		Beep(500, 100) ;
		CString olMessage ;
		olMessage.Format(GetString(IDS_STRING1460), m_ListArrDep);
		MessageBox( olMessage, "ReportSelectDlg", MB_ICONWARNING);
	}

}
