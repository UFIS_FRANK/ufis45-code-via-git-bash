// ReportArrDepLoadPaxTableViewer.cpp : implementation file
// 
// according to arrival/departure load & pax flight each day


#include <stdafx.h>
#include <ReportArrDepLoadPaxTableViewer.h>
#include <CcsGlobl.h>
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include <BasicData.h>
#include <resrc1.h>

#include <ReportXXXCedaData.h>
#include <CedaBasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// ReportArrDepLoadPaxTableViewer
//

int ReportArrDepLoadPaxTableViewer::GetVia(CCSPtrArray<VIADATA> *opVias, ROTATIONDLGFLIGHTDATA *prpFlight)
{
	opVias->DeleteAll();

	if(prpFlight == NULL)
		return 0;
	if(strlen(prpFlight->Vial) == 0)
		return 0;

	CString olVias(prpFlight->Vial);
	CString olFids;
	CString olApc3;
	CString olApc4;

	VIADATA *prlVia;

	while(olVias.IsEmpty() != TRUE)
	{
			prlVia = new VIADATA;
			opVias->Add(prlVia);

			if(olVias.GetLength() < 120)
			{
				olVias += "                                                                                                                             ";
				olVias = olVias.Left(120);
			}


			olApc3 = olVias.Mid(1,3);
			olApc3.TrimLeft();
			sprintf(prlVia->Apc3, olApc3);

			if(olVias.GetLength() >= 120)
				olVias = olVias.Right(olVias.GetLength() - 120);

	}
	return opVias->GetSize();
}



ReportArrDepLoadPaxTableViewer::ReportArrDepLoadPaxTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, 
												 int ipArrDep, char *pcpInfo, char *pcpSelect)
{

	pomData = popData;
	pcmInfo = pcpInfo;
	pcmSelect = pcpSelect;
	
	imArrDep = ipArrDep;

	bmIsFromSearch = false;
    pomTable = NULL;
	imFlightCount = 0;


	// load pax data
	CString olFlightUrnos;
	char buffer[65];

	for (int i=0; i < pomData->GetSize(); i++)
	{
		// collect flight-urnos
		ltoa((*pomData)[i].Urno, buffer, 10);
		olFlightUrnos += CString(buffer) + CString(",");
	}

	if(!olFlightUrnos.IsEmpty())
	{
		olFlightUrnos = olFlightUrnos.Left(olFlightUrnos.GetLength() - 1);
	}
	omApxData.ReadFlnus(olFlightUrnos); 



}

ReportArrDepLoadPaxTableViewer::~ReportArrDepLoadPaxTableViewer()
{
	omPrintHeadHeaderArray.DeleteAll();  // BWi
    DeleteAll();
}


void ReportArrDepLoadPaxTableViewer::SetParentDlg(CDialog* ppParentDlg)
{

	pomParentDlg = ppParentDlg;

}


void ReportArrDepLoadPaxTableViewer::Attach(CCSTable *popTable)
{

    pomTable = popTable;

}


int ReportArrDepLoadPaxTableViewer::GetFlightCount()
{

	return imFlightCount;

}


void ReportArrDepLoadPaxTableViewer::ChangeViewTo(const char *pcpViewName)
{

    pomTable->ResetContent();
    DeleteAll();    
    MakeLines();

	UpdateDisplay();
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

}


/////////////////////////////////////////////////////////////////////////////
// ReportArrDepLoadPaxTableViewer -- code specific to this class

void ReportArrDepLoadPaxTableViewer::MakeLines()
{
	ROTATIONDLGFLIGHTDATA *prlAFlight;
	ROTATIONDLGFLIGHTDATA *prlDFlight;
	ROTATIONDLGFLIGHTDATA *prlNextFlight = NULL;
	ROTATIONDLGFLIGHTDATA *prlFlight;
	int ilLineNo;

	bool blRDeparture = false;

	int ilFlightCount = pomData->GetSize();

	for (int ilLc = 0; ilLc < ilFlightCount; ilLc++)
	{
		prlFlight = &(*pomData)[ilLc];

		if(ilLc + 1 < ilFlightCount)
			prlNextFlight = &(*pomData)[ilLc + 1];	
		else
			prlNextFlight = NULL;	

		//Arrival
		if((strcmp(prlFlight->Org3, pcgHome) != 0) && (strcmp(prlFlight->Des3, pcgHome) == 0))
		{
			prlAFlight = prlFlight;
			prlDFlight = NULL;
			if(prlNextFlight != NULL)
			{
				if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
				{
					prlDFlight = prlNextFlight;

					if((strcmp(prlNextFlight->Org3, pcgHome) == 0) && (strcmp(prlNextFlight->Des3, pcgHome) != 0))
					{
						ilLc++;
					}
					else
						blRDeparture = true;
				}
			}
		}
		else
		{
			// Departure
			if((strcmp(prlFlight->Org3, pcgHome) == 0) && (strcmp(prlFlight->Des3, pcgHome) != 0))
			{
				prlAFlight = NULL;
				prlDFlight = prlFlight;
				blRDeparture = false;
			}
			else
			{
				//Turnaround
				if((strcmp(prlFlight->Org3, pcgHome) == 0) && (strcmp(prlFlight->Des3, pcgHome) == 0))
				{
					if(blRDeparture)
					{
						blRDeparture = false;
						prlAFlight = prlFlight;
						prlDFlight = NULL;
						if(prlNextFlight != NULL)
						{
							if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
							{
								prlDFlight = prlNextFlight;
								if((strcmp(prlNextFlight->Org3, pcgHome) == 0) && (strcmp(prlNextFlight->Des3, pcgHome) != 0))
								{
									ilLc++;
								}
								else
									blRDeparture = true;
							}
						}
					}
					else
					{
						prlAFlight		= prlFlight;
						prlDFlight		= prlFlight;
						if(prlNextFlight != NULL)
						{
							if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
							{
								ilLc--;
								prlAFlight	 = NULL;
								blRDeparture = true;
							}
						}
					}
				}
			}
		}
		ilLineNo = MakeLine(prlAFlight, prlDFlight);

	} // for (int ilLc = 0; ilLc < ilFlightCount; ilLc++)


}

		

int ReportArrDepLoadPaxTableViewer::MakeLine(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight)
{
    SAMEARRDEPLOADPAXTABLE_LINEDATA rlLine;

	if((prpAFlight != NULL) && (prpDFlight != NULL) )
	{
		imFlightCount++;
		if (imArrDep == 0)
		{
			MakeLineData(prpAFlight, NULL, rlLine);
			return CreateLine(rlLine);
		}
		if (imArrDep == 1)
		{
			MakeLineData(NULL, prpDFlight, rlLine);
			return CreateLine(rlLine);
		}
	}
	if (prpAFlight != NULL)
	{
		if (imArrDep == 0)
		{
			imFlightCount++;
			MakeLineData(prpAFlight, NULL, rlLine);
			return CreateLine(rlLine);
		}
	}
	if (prpDFlight != NULL)
	{
		if (imArrDep == 1)
		{
			imFlightCount++;
			MakeLineData(NULL, prpDFlight, rlLine);
			return CreateLine(rlLine);
		}
	}
	return -1;
}




void ReportArrDepLoadPaxTableViewer::MakeLineData(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight, SAMEARRDEPLOADPAXTABLE_LINEDATA &rpLine)
{

	CString olStr;
	if(prpAFlight != NULL)
	{
		rpLine.Adid = "A";
		rpLine.AUrno = prpAFlight->Urno;
		rpLine.ARkey = prpAFlight->Rkey;
		rpLine.AFlno = CString(prpAFlight->Flno);
		rpLine.AStoa = prpAFlight->Stoa; 
		if(bgReportLocal)
			ogBasicData.UtcToLocal(rpLine.AStoa);
		rpLine.ADate = prpAFlight->Stoa.Format("%d.%m.%y");/*DateToHourDivString(prpAFlight->Onbl, prpAFlight->Stoa);*/
		rpLine.AOrg3 = CString(prpAFlight->Org3);
		rpLine.AOrg4 = CString(prpAFlight->Org4);
		rpLine.ATtyp = CString(prpAFlight->Ttyp);
		rpLine.AAct  = CString(prpAFlight->Act3);
		rpLine.ALand = CTime(prpAFlight->Land); 
		if(bgReportLocal) 
			ogBasicData.UtcToLocal(rpLine.ALand);

		rpLine.ABlt1 = CString(prpAFlight->Blt1);
		rpLine.APsta = CString(prpAFlight->Psta);
		rpLine.ARegn = CString(prpAFlight->Regn);
		rpLine.AGta1 = CString(prpAFlight->Gta1);
		
		CCSPtrArray<VIADATA> opVias;
		int ilViaCnt = GetVia(&opVias, prpAFlight);

		if(opVias.GetSize() == 0)
			rpLine.AVia3 = "";
		else
		{
			rpLine.AVia3 =  opVias[ilViaCnt-1].Apc3;	// letzter Via vor Homeairport = letzte Zeile in Vial
		}
		opVias.DeleteAll();

		// load and pax data
		rpLine.Pax1 = CString(prpAFlight->Pax1);
		rpLine.Pax2 = CString(prpAFlight->Pax2);
		rpLine.Pax3 = CString(prpAFlight->Pax3);
		rpLine.Paxt = CString(prpAFlight->Paxt);
		rpLine.Paxf = CString(prpAFlight->Paxf);
		rpLine.Paxi = CString(prpAFlight->Paxi);

		rpLine.Mail = CString(prpAFlight->Mail);
		rpLine.Cgot = CString(prpAFlight->Cgot);
		rpLine.Bagn = CString(prpAFlight->Bagn);
		rpLine.Bagw = CString(prpAFlight->Bagw);

		// get apx data
		APXDATA *prlApxData;
		if ((prlApxData = omApxData.GetApxFlnuTyp(prpAFlight->Urno, "INF")) != NULL)
		{
			rpLine.PaxInf = CString(prlApxData->Paxc);
		}
		if ((prlApxData = omApxData.GetApxFlnuTyp(prpAFlight->Urno, "ID")) != NULL)
		{
			rpLine.PaxId = CString(prlApxData->Paxc);
		}

	}
	else
	{
		rpLine.AUrno =  0;
		rpLine.ARkey =  0;
		rpLine.AFlno = "";
		rpLine.AStoa = TIMENULL; 
		rpLine.ALand = TIMENULL; 
		rpLine.AEtai = TIMENULL; 
		rpLine.ATtyp = "";
		rpLine.ADate = ""; 
		rpLine.AOrg3 = "";
		rpLine.AOrg4 = "";
		rpLine.AVia3 = "";
		rpLine.AAct  = "";
		rpLine.AGta1 = "";
		rpLine.ABlt1 = "";
		rpLine.ARegn = "";
		rpLine.APsta = "";
	}


	if(prpDFlight != NULL)
	{
		rpLine.Adid = "D";

		rpLine.DUrno = prpDFlight->Urno;
		rpLine.DRkey = prpDFlight->Rkey;
		rpLine.DFlno = CString(prpDFlight->Flno); 
		rpLine.DDes3 = CString(prpDFlight->Des3);
		rpLine.DDes4 = CString(prpDFlight->Des4);
		rpLine.DTtyp = CString(prpDFlight->Ttyp);
		rpLine.DAct  = CString(prpDFlight->Act3);

		rpLine.DAirb = CTime(prpDFlight->Airb);
		if(bgReportLocal) 
			ogBasicData.UtcToLocal(rpLine.DAirb);
		rpLine.DEtdi = CTime(prpDFlight->Etdi);
		if(bgReportLocal) 
			ogBasicData.UtcToLocal(rpLine.DEtdi);
		rpLine.DStod = prpDFlight->Stod;
		if(bgReportLocal)
			ogBasicData.UtcToLocal(rpLine.DStod);
		rpLine.DDate = prpDFlight->Stod.Format("%d.%m.%y");
		rpLine.DVian = prpDFlight->Vian;

		if (rpLine.AAct.IsEmpty())
			rpLine.AAct = rpLine.DAct;

		rpLine.DGtd1 = CString(prpDFlight->Gtd1);
		rpLine.DPstd = CString(prpDFlight->Pstd);
		rpLine.DRegn = CString(prpDFlight->Regn);

		char pclUrno[32];
/*		CedaCcaData olCcaData;
		CString olSelection = CString("WHERE FLNU IN (");
		olSelection += CString(ltoa(prpDFlight->Urno, pclUrno, 10));
		olSelection += CString(")");
		olCcaData.Read(olSelection.GetBuffer(0));
		
		int ilCompare;
		int ilLow;
		int ilHigh;
		int ilCcaSize = olCcaData.omData.GetSize() ;
		if (ilCcaSize > 0)
		{
			int ilLow  = atoi(olCcaData.omData[0].Ckic);
			int ilHigh = atoi(olCcaData.omData[0].Ckic);
		}

		for(int i = 0; i < ilCcaSize; i++)
		{
			if( prpDFlight->Urno == olCcaData.omData[i].Flnu )
			{

				ilCompare = atoi(olCcaData.omData[i].Ckic);
				if (ilCompare > ilHigh)
					ilHigh = ilCompare;
				if (ilCompare < ilLow)
					ilLow = ilCompare;
			}

			if (ilHigh > ilLow)
			{
				CString olCkic;
				olCkic.Format("%i-%i", ilLow, ilHigh);
				rpLine.DCkic = olCkic;
			}
			else
			{
				rpLine.DCkic = olCcaData.omData[0].Ckic;
			}

		}

*/
		ReportXXXCedaData olReportXXXCedaData("FLNU", "CKIC", "CCA", true);
		olReportXXXCedaData.ReadCleartext(CString(ltoa(prpDFlight->Urno, pclUrno, 10)));

		// groessten und kleinesten Wert extrahieren
		int ilCompare;
		if (olReportXXXCedaData.omSameFlnu.GetSize() > 0)
		{
			int ilLow  = atoi(olReportXXXCedaData.omSameFlnu[0]);
			int ilHigh = atoi(olReportXXXCedaData.omSameFlnu[0]);

			for (int i = 0; i < olReportXXXCedaData.omSameFlnu.GetSize(); i++)
			{
				ilCompare = atoi(olReportXXXCedaData.omSameFlnu[i]);
				if (ilCompare > ilHigh)
					ilHigh = ilCompare;
				if (ilCompare < ilLow)
					ilLow = ilCompare;
			}

			if (ilHigh > ilLow)
			{
				CString olCkic;
				olCkic.Format("%i-%i", ilLow, ilHigh);
				rpLine.DCkic = olCkic;
			}
			else
			{
				rpLine.DCkic = olReportXXXCedaData.omSameFlnu[0];
			}
		}


		CCSPtrArray<VIADATA> opVias;
		int ilViaCnt = GetVia(&opVias, prpDFlight);

		if(opVias.GetSize() == 0)
			rpLine.DVia3 = "";
		else
			rpLine.DVia3 =  opVias[0].Apc3;	// erster Via nach Homeairport = erste Zeile in Vial
		opVias.DeleteAll();


		// load and pax data
		rpLine.Pax1 = CString(prpDFlight->Pax1);
		rpLine.Pax2 = CString(prpDFlight->Pax2);
		rpLine.Pax3 = CString(prpDFlight->Pax3);
		rpLine.Paxt = CString(prpDFlight->Paxt);
		rpLine.Paxf = CString(prpDFlight->Paxf);
		rpLine.Paxi = CString(prpDFlight->Paxi);

		rpLine.Mail = CString(prpDFlight->Mail);
		rpLine.Cgot = CString(prpDFlight->Cgot);
		rpLine.Bagn = CString(prpDFlight->Bagn);
		rpLine.Bagw = CString(prpDFlight->Bagw);

		// get apx data
		APXDATA *prlApxData;
		if ((prlApxData = omApxData.GetApxFlnuTyp(prpDFlight->Urno, "INF")) != NULL)
		{
			rpLine.PaxInf = CString(prlApxData->Paxc);
		}
		if ((prlApxData = omApxData.GetApxFlnuTyp(prpDFlight->Urno, "ID")) != NULL)
		{
			rpLine.PaxId = CString(prlApxData->Paxc);
		}

	}
	else
	{
		rpLine.DUrno =  0;
		rpLine.DRkey =  0;
		rpLine.DEtdi = TIMENULL; 
		rpLine.DAirb = TIMENULL; 
		rpLine.DStod = TIMENULL; 
		rpLine.DFlno = "";
		rpLine.DDate = ""; 
		rpLine.DDes3 = "";
		rpLine.DDes4 = "";
		rpLine.DTtyp = "";
		rpLine.DVia3 = "";
		rpLine.DAct  = "";
		rpLine.DRegn = "";
		rpLine.DGtd1 = "";
	}


    return;

}



int ReportArrDepLoadPaxTableViewer::CreateLine(SAMEARRDEPLOADPAXTABLE_LINEDATA &rpLine)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareFlight(&rpLine, &omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]

    omLines.NewAt(ilLineno, rpLine);
	return ilLineno;
}


void ReportArrDepLoadPaxTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}



void ReportArrDepLoadPaxTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}


/////////////////////////////////////////////////////////////////////////////
// ReportArrDepLoadPaxTableViewer - display drawing routine



// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void ReportArrDepLoadPaxTableViewer::UpdateDisplay()
{
	pomTable->ResetContent();

	DrawHeader();

	SAMEARRDEPLOADPAXTABLE_LINEDATA *prlLine;
    
	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		int ilColumnNo = 0;
		prlLine = &omLines[ilLc];
		CCSPtrArray<TABLE_COLUMN> olColList;
	
		MakeColList(prlLine, olColList);

		pomTable->AddTextLine(olColList, (void*)(prlLine));
		olColList.DeleteAll();
	}


    pomTable->DisplayTable();
}




void ReportArrDepLoadPaxTableViewer::DrawHeader()
{
	int  ilTotalLines = 0;
	bool blNewLogicLine = false;

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);
	pomTable->ResetContent();
	
	TABLE_HEADER_COLUMN rlHeader;

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 67; 
	rlHeader.Font = &ogCourier_Bold_10;
	rlHeader.Text = GetString(IDS_STRING1078);//FLIGHT
	omHeaderDataArray.New(rlHeader);

 	rlHeader.Length = 25; 
 	if (imArrDep == 0)
		rlHeader.Text = GetString(IDS_STRING298);// ORG
	if (imArrDep == 1)
		rlHeader.Text = GetString(IDS_STRING315);// DES
	omHeaderDataArray.New(rlHeader);

 	rlHeader.Length = 40; 
 	if (imArrDep == 0)
		rlHeader.Text = GetString(IDS_STRING323);//STA
	if (imArrDep == 1)
		rlHeader.Text = GetString(IDS_STRING316);//STD
	omHeaderDataArray.New(rlHeader);

 	rlHeader.Length = 40; 
 	if (imArrDep == 0)
		rlHeader.Text = GetString(IDS_STRING304);//ATA
	if (imArrDep == 1)
		rlHeader.Text = GetString(IDS_STRING321);//ATD
	omHeaderDataArray.New(rlHeader);

 	rlHeader.Length = 25; 
 	rlHeader.Text = GetString(IDS_STRING311);//A/C
	omHeaderDataArray.New(rlHeader);

 	rlHeader.Length = 45; 
 	rlHeader.Text = GetString(IDS_STRING310);//REGN
	omHeaderDataArray.New(rlHeader);

 	if (imArrDep == 0)
	{
		rlHeader.Length = 50;
		rlHeader.Text = GetString(IDS_STRING308);//POS
	}
	if (imArrDep == 1)
	{
		rlHeader.Length = 72;	
		rlHeader.Text = GetString(IDS_STRING1641);//Cki-Counter
	}
 	omHeaderDataArray.New(rlHeader);

 	rlHeader.Length = 45; 
 	rlHeader.Text = GetString(IDS_STRING1406);//Gate
	omHeaderDataArray.New(rlHeader);

 	rlHeader.Length = 45; 
 	if (imArrDep == 0)
		rlHeader.Text = GetString(IDS_STRING1151);//Belt
	if (imArrDep == 1)
		rlHeader.Text = GetString(IDS_STRING308);//Pos
	omHeaderDataArray.New(rlHeader);

	// Load & Pax
 	rlHeader.Length = 20; 
 	rlHeader.Text = GetString(IDS_FIRSTCLASS); //F
	omHeaderDataArray.New(rlHeader); 

	rlHeader.Length = 25; 
 	rlHeader.Text = GetString(IDS_ECONCLASS); //C
	omHeaderDataArray.New(rlHeader); 

	rlHeader.Length = 25; 
 	rlHeader.Text = GetString(IDS_BUSICLASS); //Y
	omHeaderDataArray.New(rlHeader); 

	rlHeader.Length = 20; 
 	rlHeader.Text = GetString(IDS_INFANTS); //INF
	omHeaderDataArray.New(rlHeader); 

	rlHeader.Length = 30; 
 	rlHeader.Text = GetString(IDS_PAXTTL); // P/TTL
	omHeaderDataArray.New(rlHeader); 

	rlHeader.Length = 25; 
 	rlHeader.Text = GetString(IDS_ID); //ID
	omHeaderDataArray.New(rlHeader); 

	rlHeader.Length = 25; 
 	rlHeader.Text = GetString(IDS_TRANSIT); //TR
	omHeaderDataArray.New(rlHeader); 

	rlHeader.Length = 25; 
 	rlHeader.Text = GetString(IDS_TRANSFER); //TRF
	omHeaderDataArray.New(rlHeader); 

	rlHeader.Length = 45; 
 	rlHeader.Text = GetString(IDS_BAGGAGE); //BAG
	omHeaderDataArray.New(rlHeader); 

	rlHeader.Length = 45; 
 	rlHeader.Text = GetString(IDS_CARGO); //CGO
	omHeaderDataArray.New(rlHeader); 

	rlHeader.Length = 45; 
 	rlHeader.Text = GetString(IDS_MAIL); //MAIL
	omHeaderDataArray.New(rlHeader); 

	rlHeader.Length = 45; 
 	rlHeader.Text = GetString(IDS_LOADTTL); // L/TTL
	omHeaderDataArray.New(rlHeader); 


	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();

}



void ReportArrDepLoadPaxTableViewer::MakeColList(SAMEARRDEPLOADPAXTABLE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList)
{

	TABLE_COLUMN rlColumnData;

	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogCourier_Regular_9;

	rlColumnData.Alignment = COLALIGN_LEFT;
	if (imArrDep == 0)
		rlColumnData.Text = prlLine->AFlno;
	if (imArrDep == 1)
		rlColumnData.Text = prlLine->DFlno;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if (imArrDep == 0)
		rlColumnData.Text = prlLine->AOrg3;
	if (imArrDep == 1)
		rlColumnData.Text = prlLine->DDes3;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if (imArrDep == 0)
		rlColumnData.Text = prlLine->AStoa.Format("%H:%M");
	if (imArrDep == 1)
		rlColumnData.Text = prlLine->DStod.Format("%H:%M");
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if (imArrDep == 0)
		rlColumnData.Text = prlLine->ALand.Format("%H:%M");
	if (imArrDep == 1)
		rlColumnData.Text = prlLine->DAirb.Format("%H:%M");
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if (imArrDep == 0)
		rlColumnData.Text = prlLine->AAct;
	if (imArrDep == 1)
		rlColumnData.Text = prlLine->DAct;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if (imArrDep == 0)
		rlColumnData.Text = prlLine->ARegn;
	if (imArrDep == 1)
		rlColumnData.Text = prlLine->DRegn;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if (imArrDep == 0)
		rlColumnData.Text = prlLine->APsta;
	if (imArrDep == 1)
		rlColumnData.Text = prlLine->DCkic;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if (imArrDep == 0)
		rlColumnData.Text = prlLine->AGta1;
	if (imArrDep == 1)
		rlColumnData.Text = prlLine->DGtd1;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	if (imArrDep == 0)
		rlColumnData.Text = prlLine->ABlt1;
	if (imArrDep == 1)
		rlColumnData.Text = prlLine->DPstd;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	// Load & Pax
	rlColumnData.Alignment = COLALIGN_LEFT;
 	rlColumnData.Text = prlLine->Pax1;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
 	rlColumnData.Text = prlLine->Pax2;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
 	rlColumnData.Text = prlLine->Pax3;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
 	rlColumnData.Text = prlLine->PaxInf;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
 	rlColumnData.Text = prlLine->Paxt;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
 	rlColumnData.Text = prlLine->PaxId;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
 	rlColumnData.Text = prlLine->Paxi;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
 	rlColumnData.Text = prlLine->Paxf;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
 	rlColumnData.Text = prlLine->Bagn;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
 	rlColumnData.Text = prlLine->Cgot;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
 	rlColumnData.Text = prlLine->Mail;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
 	rlColumnData.Text = prlLine->Bagw;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

}





int ReportArrDepLoadPaxTableViewer::CompareFlight(SAMEARRDEPLOADPAXTABLE_LINEDATA *prpLine1, SAMEARRDEPLOADPAXTABLE_LINEDATA *prpLine2)
{
	int	ilCompareResult;
	CTime olTime1;
	CTime olTime2;

	if (prpLine1->Adid == "A")
	{
		olTime1 = prpLine1->AStoa;
		olTime2 = prpLine2->AStoa;
	}
	else
	{
		olTime1 = prpLine1->DStod;
		olTime2 = prpLine2->DStod;
	}

	ilCompareResult = (olTime1 == olTime2)? 0:
			(olTime1 > olTime2)? 1: -1;

	return ilCompareResult;
}



//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------


void ReportArrDepLoadPaxTableViewer::GetHeader()
{
	
	omPrintHeadHeaderArray.DeleteAll();  // BWi

	TABLE_HEADER_COLUMN rlHeader;

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 67; 
	rlHeader.Font = &ogCourier_Bold_10;
	rlHeader.Text = GetString(IDS_STRING1078);//FLIGHT
	omPrintHeadHeaderArray.New(rlHeader);

 	rlHeader.Length = 25; 
 	if (imArrDep == 0)
		rlHeader.Text = GetString(IDS_STRING298);// ORG
	if (imArrDep == 1)
		rlHeader.Text = GetString(IDS_STRING315);// DES
	omPrintHeadHeaderArray.New(rlHeader);

 	rlHeader.Length = 40; 
 	if (imArrDep == 0)
		rlHeader.Text = GetString(IDS_STRING323);//STA
	if (imArrDep == 1)
		rlHeader.Text = GetString(IDS_STRING316);//STD
	omPrintHeadHeaderArray.New(rlHeader);

 	rlHeader.Length = 40; 
 	if (imArrDep == 0)
		rlHeader.Text = GetString(IDS_STRING304);//ATA
	if (imArrDep == 1)
		rlHeader.Text = GetString(IDS_STRING321);//ATD
	omPrintHeadHeaderArray.New(rlHeader);

 	rlHeader.Length = 25; 
 	rlHeader.Text = GetString(IDS_STRING311);//A/C
	omPrintHeadHeaderArray.New(rlHeader);

 	rlHeader.Length = 45; 
 	rlHeader.Text = GetString(IDS_STRING310);//REGN
	omPrintHeadHeaderArray.New(rlHeader);

 	if (imArrDep == 0)
	{
		rlHeader.Length = 50;
		rlHeader.Text = GetString(IDS_STRING308);//POS
	}
	if (imArrDep == 1)
	{
		rlHeader.Length = 72;
		rlHeader.Text = GetString(IDS_STRING1641);//Cki-Counter
	}
 	omPrintHeadHeaderArray.New(rlHeader);

 	rlHeader.Length = 45; 
 	rlHeader.Text = GetString(IDS_STRING1406);//Gate
	omPrintHeadHeaderArray.New(rlHeader);

 	rlHeader.Length = 45; 
 	if (imArrDep == 0)
		rlHeader.Text = GetString(IDS_STRING1151);//Belt
	if (imArrDep == 1)
		rlHeader.Text = GetString(IDS_STRING308);//Pos
	omPrintHeadHeaderArray.New(rlHeader);

	// Load & Pax
 	rlHeader.Length = 20; 
 	rlHeader.Text = GetString(IDS_FIRSTCLASS); //F
	omPrintHeadHeaderArray.New(rlHeader); 

	rlHeader.Length = 25; 
 	rlHeader.Text = GetString(IDS_ECONCLASS); //C
	omPrintHeadHeaderArray.New(rlHeader); 

	rlHeader.Length = 25; 
 	rlHeader.Text = GetString(IDS_BUSICLASS); //Y
	omPrintHeadHeaderArray.New(rlHeader); 

	rlHeader.Length = 20; 
 	rlHeader.Text = GetString(IDS_INFANTS); //INF
	omPrintHeadHeaderArray.New(rlHeader); 

	rlHeader.Length = 30; 
 	rlHeader.Text = GetString(IDS_PAXTTL); // P/TTL
	omPrintHeadHeaderArray.New(rlHeader); 

	rlHeader.Length = 25; 
 	rlHeader.Text = GetString(IDS_ID); //ID
	omPrintHeadHeaderArray.New(rlHeader); 

	rlHeader.Length = 25; 
 	rlHeader.Text = GetString(IDS_TRANSIT); //TR
	omPrintHeadHeaderArray.New(rlHeader); 

	rlHeader.Length = 25; 
 	rlHeader.Text = GetString(IDS_TRANSFER); //TRF
	omPrintHeadHeaderArray.New(rlHeader); 

	rlHeader.Length = 45; 
 	rlHeader.Text = GetString(IDS_BAGGAGE); //BAG
	omPrintHeadHeaderArray.New(rlHeader); 

	rlHeader.Length = 45; 
 	rlHeader.Text = GetString(IDS_CARGO); //CGO
	omPrintHeadHeaderArray.New(rlHeader); 

	rlHeader.Length = 45; 
 	rlHeader.Text = GetString(IDS_MAIL); //MAIL
	omPrintHeadHeaderArray.New(rlHeader); 

	rlHeader.Length = 45; 
 	rlHeader.Text = GetString(IDS_LOADTTL); // L/TTL
	omPrintHeadHeaderArray.New(rlHeader); 

 
}




void ReportArrDepLoadPaxTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	char pclFooter[200];

	sprintf(pclFooter, GetString(IDS_STRING1876));

	omFooterName = pclFooter;
	CString olTableName = GetString(IDS_STRING1877);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET
	pomPrint = new CCSPrint(pomParentDlg,ilOrientation,45);

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = olTableName;	
			//rlDocInfo.lpszDocName = TABLENAME;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = imFlightCount;//omLines.GetSize();  
			olFooter1.Format(GetString(IDS_STRING1445), ilLines, omFooterName);

			GetHeader();

			if (ilLines == 0)
				PrintTableHeader(); // Drucken leerer Tabelle
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}

		omPrintHeadHeaderArray.DeleteAll();
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		delete pomPrint;
		pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool ReportArrDepLoadPaxTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;
	double dgCCSPrintFactor = 2.7 ;
	char pclHeader[200];

	sprintf(pclHeader, GetString(IDS_STRING1877), pcmInfo, imFlightCount);
	CString olTableName(pclHeader);

	pomPrint->PrintUIFHeader(omTableName,CString(pclHeader),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omPrintHeadHeaderArray.GetSize();

	for(int i=0;i<ilSize;i++)
	{
		rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Text   = omPrintHeadHeaderArray[i].Text;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool ReportArrDepLoadPaxTableViewer::PrintTableLine(SAMEARRDEPLOADPAXTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	double dgCCSPrintFactor = 2.7 ;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omPrintHeadHeaderArray.GetSize();

	for(int i=0;i<ilSize;i++)
	{
		rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameTop   = PRINT_FRAMETHIN;
		if(bpLastLine == true)
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN;
		}
		else
		{
			rlElement.FrameBottom = PRINT_NOFRAME;
		}
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;


		switch(i)
		{
			case 0:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->AFlno;
				if (imArrDep == 1)
					rlElement.Text = prpLine->DFlno;
			}
			break;
			case 1:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->AOrg3;
				if (imArrDep == 1)
					rlElement.Text = prpLine->DDes3;
			}
			break;
			case 2:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->AStoa.Format("%H:%M");
				if (imArrDep == 1)
					rlElement.Text = prpLine->DStod.Format("%H:%M");
			}
			break;
			case 3:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->ALand.Format("%H:%M");
				if (imArrDep == 1)
					rlElement.Text = prpLine->DAirb.Format("%H:%M");
			}
			break;
			case 4:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->AAct;
				if (imArrDep == 1)
					rlElement.Text = prpLine->DAct;
			}
			break;
			case 5:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->ARegn;
				if (imArrDep == 1)
					rlElement.Text = prpLine->DRegn;
			}
			break;
			case 6:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->APsta;
				if (imArrDep == 1)
					rlElement.Text = prpLine->DCkic;
			}
			break;
			case 7:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->AGta1;
				if (imArrDep == 1)
					rlElement.Text = prpLine->DGtd1;
			}
			break;
			case 8:
			{
				if (imArrDep == 0)
					rlElement.Text = prpLine->ABlt1;
				if (imArrDep == 1)
					rlElement.Text = prpLine->DPstd;
			}
			break;
			case 9:
			{
				rlElement.Text = prpLine->Pax1;
			}
			break;
			case 10:
			{
				rlElement.Text = prpLine->Pax2;
			}
			break;
			case 11:
			{
				rlElement.Text = prpLine->Pax3;
			}
			break;
			case 12:
			{
				rlElement.Text = prpLine->PaxInf;
			}
			break;
			case 13:
			{
				rlElement.Text = prpLine->Paxt;
			}
			break;
			case 14:
			{
				rlElement.Text = prpLine->PaxId;
			}
			break;
			case 15:
			{
				rlElement.Text = prpLine->Paxi;
			}
			break;
			case 16:
			{
				rlElement.Text = prpLine->Paxf;
			}
			break;
			case 17:
			{
				rlElement.Text = prpLine->Bagn;
			}
			break;
			case 18:
			{
				rlElement.Text = prpLine->Cgot;
			}
			break;
			case 19:
			{
				rlElement.Text = prpLine->Mail;
			}
			break;
			case 20:
			{
				rlElement.Text = prpLine->Bagw;
			}
			break;
		}

		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------


// Drucken in Datei
void ReportArrDepLoadPaxTableViewer::PrintPlanToFile(char *pcpDefPath)
{

	CCS_TRY

	char pclHeadlineSelect[200];
	sprintf(pclHeadlineSelect, GetString(IDS_STRING1877), pcmInfo, imFlightCount);

	ofstream of;
	of.open( pcpDefPath, ios::out);
	of << CString(pclHeadlineSelect) << " "
	   << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M") << endl << endl;

	if (imArrDep == 0)
	{
		of << GetString(IDS_STRING1078) << "     "<< GetString(IDS_STRING298) << " " 
		   << GetString(IDS_STRING323)  << "   "  << GetString(IDS_STRING304) << "   " 
		   << GetString(IDS_STRING311) << " " << GetString(IDS_STRING310) << "   "
		   << GetString(IDS_STRING308)  << "  " << GetString(IDS_STRING1406) << "  "
		   << GetString(IDS_STRING1151) << "  " << GetString(IDS_FIRSTCLASS) << "  "
		   << GetString(IDS_ECONCLASS) << "   " << GetString(IDS_BUSICLASS) << "   "
		   << GetString(IDS_INFANTS) << " " << GetString(IDS_PAXTTL) << " "
		   << GetString(IDS_ID) << "  " << GetString(IDS_TRANSIT) << "  " 
		   << GetString(IDS_TRANSFER) << " " << GetString(IDS_BAGGAGE) << "    "
		   << GetString(IDS_CARGO) << "    " << GetString(IDS_MAIL) << "   "
		   << GetString(IDS_LOADTTL) << endl;
		of << "------------------------------------------------------------------------------------------------------------------" << endl;
	}

	if (imArrDep == 1)
	{
		of << GetString(IDS_STRING1078) << "     "<< GetString(IDS_STRING315) << " " 
		   << GetString(IDS_STRING316)  << "   "  << GetString(IDS_STRING321) << "   " 
		   << GetString(IDS_STRING311) << " " << GetString(IDS_STRING310) << "   "
		   << GetString(IDS_STRING1641)  << "   " << GetString(IDS_STRING1406) << "  "
		   << GetString(IDS_STRING308) << "  " << GetString(IDS_FIRSTCLASS) << "  "
		   << GetString(IDS_ECONCLASS) << "   " << GetString(IDS_BUSICLASS) << "   "
		   << GetString(IDS_INFANTS) << " " << GetString(IDS_PAXTTL) << " "
		   << GetString(IDS_ID) << "  " << GetString(IDS_TRANSIT) << "  " 
		   << GetString(IDS_TRANSFER) << " " << GetString(IDS_BAGGAGE) << "    "
		   << GetString(IDS_CARGO) << "    " << GetString(IDS_MAIL) << "   "
		   << GetString(IDS_LOADTTL) << endl;
		of << "--------------------------------------------------------------------------------------------------------------------------" << endl;
	}

	CStringArray olDaten;
	int ilCount = imFlightCount;//omLines.GetSize();


	for(int i = 0; i < ilCount; i++)
	{
		SAMEARRDEPLOADPAXTABLE_LINEDATA rlD = omLines[i];

		if (imArrDep == 0)
		{
			olDaten.Add(rlD.AFlno);
			olDaten.Add(rlD.AOrg3);
			olDaten.Add(rlD.AStoa.Format("%H:%M"));
			olDaten.Add(rlD.ALand.Format("%H:%M"));
			olDaten.Add(rlD.AAct);
			olDaten.Add(rlD.ARegn);
			olDaten.Add(rlD.APsta);
			olDaten.Add(rlD.AGta1);
			olDaten.Add(rlD.ABlt1);
		}
		if (imArrDep == 1)
		{
			olDaten.Add(rlD.DFlno);
			olDaten.Add(rlD.DDes3);
			olDaten.Add(rlD.DStod.Format("%H:%M"));
			olDaten.Add(rlD.DAirb.Format("%H:%M"));
			olDaten.Add(rlD.DAct);
			olDaten.Add(rlD.DRegn);
			olDaten.Add(rlD.DCkic);
			olDaten.Add(rlD.DGtd1);
			olDaten.Add(rlD.DPstd);
		}
		// load & pax
		olDaten.Add(rlD.Pax1);
		olDaten.Add(rlD.Pax2);
		olDaten.Add(rlD.Pax3);
		olDaten.Add(rlD.PaxInf);
		olDaten.Add(rlD.Paxt);
		olDaten.Add(rlD.PaxId);
		olDaten.Add(rlD.Paxi);
		olDaten.Add(rlD.Paxf);
		olDaten.Add(rlD.Bagn);
		olDaten.Add(rlD.Cgot);
		olDaten.Add(rlD.Mail);
		olDaten.Add(rlD.Bagw);

		if (imArrDep == 0)
		{
			of.setf(ios::left, ios::adjustfield);
			of   << setw(11) << olDaten[0].GetBuffer(0)
				 << setw(4) << olDaten[1].GetBuffer(0)
				 << setw(6) << olDaten[2].GetBuffer(0)
				 << setw(6) << olDaten[3].GetBuffer(0)
				 << setw(4) << olDaten[4].GetBuffer(0)
				 << setw(6) << olDaten[5].GetBuffer(0)
				 << setw(5) << olDaten[6].GetBuffer(0)
				 << setw(6) << olDaten[7].GetBuffer(0)
				 << setw(6) << olDaten[8].GetBuffer(0);
		}		
		if (imArrDep == 1)
		{
			of.setf(ios::left, ios::adjustfield);
			of   << setw(11) << olDaten[0].GetBuffer(0)
				 << setw(4) << olDaten[1].GetBuffer(0)
				 << setw(6) << olDaten[2].GetBuffer(0)
				 << setw(6) << olDaten[3].GetBuffer(0)
				 << setw(4) << olDaten[4].GetBuffer(0)
				 << setw(6) << olDaten[5].GetBuffer(0)
				 << setw(14) << olDaten[6].GetBuffer(0)
				 << setw(6) << olDaten[7].GetBuffer(0)
				 << setw(5) << olDaten[8].GetBuffer(0);
		}		
		// load & pax
		of	<< setw(3) << olDaten[9].GetBuffer(0)
			<< setw(4) << olDaten[10].GetBuffer(0)
			<< setw(4) << olDaten[11].GetBuffer(0)
			<< setw(4) << olDaten[12].GetBuffer(0)
			<< setw(6) << olDaten[13].GetBuffer(0)
			<< setw(4) << olDaten[14].GetBuffer(0)
			<< setw(4) << olDaten[15].GetBuffer(0)
			<< setw(4) << olDaten[16].GetBuffer(0)
			<< setw(7) << olDaten[17].GetBuffer(0)
			<< setw(7) << olDaten[18].GetBuffer(0)
			<< setw(7) << olDaten[19].GetBuffer(0)
			<< setw(7) << olDaten[20].GetBuffer(0);
		of << endl;
		
		olDaten.RemoveAll();
	}
	of.close();

	CCS_CATCH_ALL

}
