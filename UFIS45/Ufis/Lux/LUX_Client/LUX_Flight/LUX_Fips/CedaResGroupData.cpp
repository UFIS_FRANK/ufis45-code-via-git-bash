// CedaResGroupData.cpp - Class for handling resource group data
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <basicdata.h>
#include <CedaResGroupData.h>
#include <CedaBasicData.h>
 
#include <algorithm>

// Constructors of the VCDDATA structure
VCDDATA::VCDDATA(void) 
{
	memset(this,'\0',sizeof(*this));
	Urno = 0;
	strcpy(Appn, ogAppName);
	strcpy(Ctyp, "RES_GROUPS");
	strncpy(Pkno, ogBasicData.omUserID, 34);
}
VCDDATA::VCDDATA(const ResGroupData &rrpResGroup)
{
	memset(this,'\0',sizeof(*this));
	Urno = 0;
	strcpy(Appn, ogAppName);
	strcpy(Ctyp, "RES_GROUPS");
	strcpy(Ckey, rrpResGroup.Ckey);
	strncpy(Pkno, ogBasicData.omUserID, 34);
	strcpy(Text, rrpResGroup.Text);
}


// Constructors of the ResGroupData structure
ResGroupData::ResGroupData(void)
{
}

ResGroupData::ResGroupData(const VCDDATA &rrpVcdData)
{
 	Ckey = rrpVcdData.Ckey;
	Text = rrpVcdData.Text;
}



///////////////////////////////////////////////////////////////////////
///// Constructors and Methods of CedaResGroupData - Class

CedaResGroupData::CedaResGroupData()
{                  
    // Create an array of CEDARECINFO for VCDDATA
    BEGIN_CEDARECINFO(VCDDATA, VcdDataRecInfo)
        FIELD_LONG(Urno,"URNO")
        FIELD_CHAR_TRIM(Appn,"APPN")
        FIELD_CHAR_TRIM(Ctyp,"CTYP")
        FIELD_CHAR_TRIM(Ckey,"CKEY")
        FIELD_DATE(Vafr,"VAFR")
        FIELD_DATE(Vato,"VATO")
        FIELD_CHAR_TRIM(Pkno,"PKNO")
        FIELD_CHAR_TRIM(Text,"TEXT")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(VcdDataRecInfo)/sizeof(VcdDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&VcdDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"VCD");
	
	strcpy(pcmListOfFields, "URNO,APPN,CTYP,CKEY,VAFR,VATO,PKNO,TEXT");
	pcmFieldList = pcmListOfFields;
 	 
}


CedaResGroupData::~CedaResGroupData()
{
 	//ogDdx.UnRegister(this,NOTUSED);
	DeleteAllInternal();
	omRecInfo.DeleteAll();
}


bool CedaResGroupData::DeleteAllInternal()
{
	omCkeyMap.RemoveAll();
    omData.DeleteAll();

	return true;
}



bool CedaResGroupData::ReadGroups()
{
	// delete internal structures
	DeleteAllInternal();
	// Read data from database
 	CString olWhere;
	olWhere.Format("WHERE APPN='%s' AND CTYP = 'RES_GROUPS' AND PKNO = '%s'",
		ogAppName, ogBasicData.omUserID);
	if (CedaAction("RT", olWhere.GetBuffer(0)) == false)
        return false;
	// transfer data to internal data structures
	bool ilRc = true;
	VCDDATA rlVcd;
	ResGroupData *prlResGroup;
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		if ((ilRc = GetBufferRecord(ilLc, &rlVcd)) == true)
		{
			if (omCkeyMap.Lookup(rlVcd.Ckey, (void *&) prlResGroup) == 0)
			{
				prlResGroup = new ResGroupData(rlVcd);
				InsertInternal(prlResGroup);
			}
			else
			{
				// collect all data with the same ckey in one internal data record!
				UpdateInternal(prlResGroup, rlVcd);
			}
		}
	}

	return true;
}



bool CedaResGroupData::GetGroupList(const CString &ropResName, CStringArray &ropGroupList) const
{
	ropGroupList.RemoveAll();

	// scan all loaded ResGroupData
	int ilCount = omData.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		if (omData[i].Ckey.Left(3) == ropResName)
		{
			// add groupname to group list
			ropGroupList.Add(omData[i].Ckey.Mid(4));
		}
	}

	return true;
}



bool CedaResGroupData::GetGroupResList(const CString &ropResName, const CString &ropGroupName, CPtrArray &ropUrnoList) const
{
	ropUrnoList.RemoveAll();

	// look for the specific group of the specific resource
 	ResGroupData *prlResGroup = GetGroupData(ropResName, ropGroupName);
	if (!prlResGroup) return false;
 
	// collect the urnos of the resources in the group
	CStringArray olStrArray;
	ExtractItemList(prlResGroup->Text, &olStrArray, ',');
	for (int i=0; i < olStrArray.GetSize(); i++)
	{
		ropUrnoList.Add((void *) atol(olStrArray[i]));
	}

	return true;
}



ResGroupData *CedaResGroupData::GetGroupData(const CString &ropResName, const CString &ropGroupName) const
{
	// look for the specific group of the specific resource
	CString olCkey = ropResName+"_"+ropGroupName;
	ResGroupData *prlResGroup;
	if (omCkeyMap.Lookup(olCkey, (void *&)prlResGroup) == 0)
		return NULL;
	else
		return prlResGroup;
}


bool CedaResGroupData::RemoveNotExistingGroups(const CString &ropResName, CStringArray &ropGroupList) const
{
	CStringArray olAllGroups;
	if (!GetGroupList(ropResName, olAllGroups))
		return false;

	for (int i = ropGroupList.GetSize()-1; i >=0; i--)
	{
 		if (std::find(olAllGroups.GetData(), olAllGroups.GetData() + olAllGroups.GetSize(), ropGroupList[i]) 
				== olAllGroups.GetData() + olAllGroups.GetSize()) 
		{
			ropGroupList.RemoveAt(i);
		}
	}
	return true;
}



bool CedaResGroupData::SaveGroup(const CString &ropResName, const CString &ropGroupName, const CPtrArray &ropUrnoArray)
{

	// build comma-list of urnos
	CString olUrnos;
	CString olUrno;
	for (int i=0; i < ropUrnoArray.GetSize(); i++)
	{
		olUrno.Format("%ld,", (long) ropUrnoArray[i]);
		olUrnos += olUrno;
	}
	if (!olUrnos.IsEmpty())
		olUrnos = olUrnos.Left(olUrnos.GetLength()-1);

	if (olUrnos.IsEmpty())
		return false;

	// look for the specific group of the specific resource
 	ResGroupData *prlResGroup = GetGroupData(ropResName, ropGroupName);

	// NO CHANGING! instead: delete and insert because data may be in many db records 
	if (prlResGroup)
		DeleteGroup(ropResName, ropGroupName);

	// insert new data record into internal data structures
	prlResGroup = new ResGroupData();
	prlResGroup->Ckey = ropResName+"_"+ropGroupName;
	prlResGroup->Text = olUrnos;
	InsertInternal(prlResGroup);


	// insert new data into vcdtab. if necessary use more than one db record
	CString olListOfData;
	VCDDATA rlVcd;
	strcpy(rlVcd.Ckey, ropResName+"_"+ropGroupName);

	bool blDBRes = true;
	CStringArray olUrnosArray;
	// split urno list in VCDTAB.TEXT.MaxLength() pieces
	for (i = SplitItemList(olUrnos, &olUrnosArray, VCD_TEXT_MAXLENGTH / 11) - 1; i >= 0 && blDBRes; i--)
	{
		rlVcd.Urno = ogBasicData.GetNextUrno();
		strcpy(rlVcd.Text, olUrnosArray[i]);

		// insert new data record into vcdtab
		MakeCedaData(olListOfData, &rlVcd);
		blDBRes = CedaAction("IRT","","", olListOfData.GetBuffer(0));
	}

	return blDBRes;
	
}



bool CedaResGroupData::DeleteGroup(const CString &ropResName, const CString &ropGroupName)
{
	// look for the specific group of the specific resource
 	ResGroupData *prlResGroup = GetGroupData(ropResName, ropGroupName);
	if (!prlResGroup)
		return false;

 	// delete the record in internal data structures
	if (!DeleteInternal(prlResGroup))
		return false;

	// delete the record(s) in the database
	CString olSelection;
	olSelection.Format("WHERE APPN='%s' AND CTYP='RES_GROUPS' AND PKNO='%s' AND CKEY='%s'", 
		ogAppName, ogBasicData.omUserID, ropResName+"_"+ropGroupName);
	return CedaAction("DRT", olSelection.GetBuffer(0));
}

bool CedaResGroupData::CheckMembers(ResGroupData *prpResGroup)
{
	CString olText = prpResGroup->Text;
	CString olTab = CString(prpResGroup->Ckey).Left(3);
	prpResGroup->Text = "";

	CStringArray olStrArray;
	bool blFirst = false;
	for(int i = ExtractItemList(olText, &olStrArray) - 1; i >= 0; i--)
	{
		CString olUrno = olStrArray[i];
		RecordSet rlRec;
		if (ogBCD.GetRecord(olTab, "URNO", olUrno, rlRec))
		{
			if (!blFirst)
			{
				blFirst = true;
				prpResGroup->Text = olUrno;
			}
			else
				prpResGroup->Text = olUrno + CString(",") + prpResGroup->Text;
		}
	}

	if (CString(prpResGroup->Text).IsEmpty())
		return false;

	return true;
}

bool CedaResGroupData::InsertInternal(ResGroupData *prpResGroup)
{
	if (!prpResGroup) return false;

	if (!CheckMembers(prpResGroup))
		return false;

 	omData.Add(prpResGroup);
	omCkeyMap.SetAt(prpResGroup->Ckey, prpResGroup);
	//omUrnoMap.SetAt((void *)prpResGroup->Urno, prpResGroup);
	return true;
}


bool CedaResGroupData::UpdateInternal(ResGroupData *prpResGroup, const VCDDATA &rrpVcdData)
{
	if (!prpResGroup) return false;

	prpResGroup->Text += ",";
	prpResGroup->Text += rrpVcdData.Text;
 	return true;
}



bool CedaResGroupData::DeleteInternal(ResGroupData *prpResGroup)
{
	// delete record in other maps
	//omUrnoMap.RemoveKey((void *) prpResGroup->Urno);
	omCkeyMap.RemoveKey(prpResGroup->Ckey);
	
	// scan all loaded ResGroupData
	int ilCount = omData.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		if (omData[i].Ckey == prpResGroup->Ckey)
		{
			// delete record in omData
			omData.DeleteAt(i);
			break;
		}
	}

	return true;
}










