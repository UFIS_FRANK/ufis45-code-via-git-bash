// ReportSeqField3DateTimeDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <fpms.h>
#include <ReportSeqField3DateTimeDlg.h>
#include <CCSTime.h>
#include <CedaBasicData.h>
#include <CcsGlobl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// Dialogfeld ReportSeqField3DateTimeDlg 


ReportSeqField3DateTimeDlg::ReportSeqField3DateTimeDlg(CWnd* popParent, const CString &ropHeadline, CTime* popMinDate, CTime *popMaxDate,
	const CString &ropField1Text, BasicDataType ipField1Type, 
	const CString &ropField2Text, BasicDataType ipField2Type, 
	const CString &ropField3Text, BasicDataType ipField3Type)
 	: CDialog(ReportSeqField3DateTimeDlg::IDD, popParent), omHeadline(ropHeadline), 
	  omField1Text(ropField1Text), imField1Type(ipField1Type), 
	  omField2Text(ropField2Text), imField2Type(ipField2Type), 
	  omField3Text(ropField3Text), imField3Type(ipField3Type)
{
	//{{AFX_DATA_INIT(ReportSeqField3DateTimeDlg)
	m_DateFrom = _T("");
	m_DateTo = _T("");
	m_TimeFrom = _T("");
	m_TimeTo = _T("");
	//}}AFX_DATA_INIT

 	pomMinDate = popMinDate;
	pomMaxDate = popMaxDate;
}


void ReportSeqField3DateTimeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ReportSeqField3DateTimeDlg)
	DDX_Control(pDX, IDC_DATUMVON, m_CE_DateFrom);
	DDX_Control(pDX, IDC_DATUMBIS, m_CE_DateTo);
	DDX_Control(pDX, IDC_ZEITVON, m_CE_TimeFrom);
	DDX_Control(pDX, IDC_ZEITBIS, m_CE_TimeTo);
	DDX_Control(pDX, IDC_EDIT1, m_CE_Field1);
	DDX_Control(pDX, IDC_EDIT2, m_CE_Field2);
	DDX_Control(pDX, IDC_EDIT3, m_CE_Field3);
 	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ReportSeqField3DateTimeDlg, CDialog)
	//{{AFX_MSG_MAP(ReportSeqField3DateTimeDlg)
	ON_MESSAGE(WM_EDIT_KILLFOCUS, OnEditKillfocus)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()




LONG ReportSeqField3DateTimeDlg::OnEditKillfocus( UINT wParam, LPARAM lParam)
{
	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY*)lParam;

	if (prlNotify->Text.IsEmpty()) return 0L;
	prlNotify->UserStatus = true;

	CString olTmp1, olTmp2;
	BasicDataType ilFieldType = REPSEQFIELD3_NONE;
	CCSEdit *polField;

	// get field type
	if (wParam == (UINT)m_CE_Field1.imID)
	{
		ilFieldType = imField1Type;
		polField = &m_CE_Field1;
	}
	else if (wParam == (UINT)m_CE_Field2.imID)
	{
		ilFieldType = imField2Type;
		polField = &m_CE_Field2;
	}
	else if (wParam == (UINT)m_CE_Field3.imID)
	{
		ilFieldType = imField3Type;
		polField = &m_CE_Field3;
	}

	// verify field value
	switch (ilFieldType)
	{
		case REPSEQFIELD3_AIRLINECODE:
			if (!(prlNotify->Status = ogBCD.GetField("ALT", "ALC2", prlNotify->Text, "ALC3", olTmp1 )))
			{
				if ((prlNotify->Status = ogBCD.GetField("ALT", "ALC3", prlNotify->Text, "ALC2", olTmp1 )))
					polField->SetInitText(olTmp1, true);
			}
			break;
		case REPSEQFIELD3_DELAYCODE:
			if (!(prlNotify->Status = ogBCD.GetField("DEN", "DECA", prlNotify->Text, "DECN", olTmp1)))
				prlNotify->Status = ogBCD.GetField("DEN", "DECN", prlNotify->Text, "DECA", olTmp1);
			break;
		case REPSEQFIELD3_DELAYTIME:
			if (polField->GetStatus())
			{
				olTmp1.Format("%04s", prlNotify->Text);
				polField->SetInitText(olTmp1, true);
			}
			break;

	}

	return 0L;
}





/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten ReportSeqField3DateTimeDlg 

void ReportSeqField3DateTimeDlg::OnOK() 
{

	m_CE_Field1.GetWindowText(m_Field1);
	m_CE_Field2.GetWindowText(m_Field2);
	m_CE_Field3.GetWindowText(m_Field3);


	// check input fields
 	if (!m_Field1.IsEmpty() && !m_CE_Field1.GetStatus())
	{
		MessageBox(GetString(IDS_STRING965), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
		return;
	}
 	if (!m_Field2.IsEmpty() && !m_CE_Field2.GetStatus())
	{
		MessageBox(GetString(IDS_STRING965), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
		return;
	}
 	if (!m_Field3.IsEmpty() && !m_CE_Field3.GetStatus())
	{
		MessageBox(GetString(IDS_STRING965), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
		return;
	}
	
	// check date fields
	m_CE_DateFrom.GetWindowText(m_DateFrom);
	m_CE_DateTo.GetWindowText(m_DateTo);
	// if only one date given, it is copied to the other
 	if(m_DateFrom.IsEmpty())
		m_CE_DateTo.GetWindowText(m_DateFrom);
	if(m_DateTo.IsEmpty())
		m_CE_DateFrom.GetWindowText(m_DateTo);
	// set time fields
	m_CE_TimeFrom.GetWindowText(m_TimeFrom);
	if (m_TimeFrom.IsEmpty())
		m_TimeFrom="00:00";
	m_CE_TimeTo.GetWindowText(m_TimeTo);
	if (m_TimeTo.IsEmpty())
		m_TimeTo="23:59";

	
	if( (m_DateTo.IsEmpty()) && (m_DateFrom.IsEmpty()) )
	{
		// no date specified
		MessageBox(GetString(IDS_STRING918), GetString(IMFK_REPORT), MB_OK);
		return;
	}
 
	if(!m_CE_DateFrom.GetStatus() || !m_CE_DateTo.GetStatus() ||
		!m_CE_TimeFrom.GetStatus() || !m_CE_TimeTo.GetStatus())
	{
		// wrong date / time format
		MessageBox(GetString(IDS_STRING934), GetString(IMFK_REPORT), MB_OK);
		return;
	}

	*pomMinDate = DateHourStringToDate(m_DateFrom, m_TimeFrom);
	*pomMaxDate = DateHourStringToDate(m_DateTo, m_TimeTo); 


	if (*pomMinDate > *pomMaxDate)
	{
		// To-Time before From-Time
		MessageBox(GetString(IDS_STRING1642), GetString(IMFK_REPORT), MB_OK);
		return;
	}
 
	CDialog::OnOK(); 
}



void ReportSeqField3DateTimeDlg::OnCancel() 
{
 	
	CDialog::OnCancel();
}


BOOL ReportSeqField3DateTimeDlg::OnInitDialog() 
{

	CDialog::OnInitDialog();
	CDialog::SetWindowText(omHeadline);	

	// set field text
	GetDlgItem(IDC_TEXT1)->SetWindowText(omField1Text);
	GetDlgItem(IDC_TEXT2)->SetWindowText(omField2Text);
	GetDlgItem(IDC_TEXT3)->SetWindowText(omField3Text);

	// set field values
	m_CE_Field1.SetInitText("", true);
	m_CE_Field2.SetInitText("", true);
	m_CE_Field3.SetInitText("", true);

	// set field types
	SetFieldType(imField1Type, m_CE_Field1);
	SetFieldType(imField2Type, m_CE_Field2);
	SetFieldType(imField3Type, m_CE_Field3);

	m_CE_DateFrom.SetTypeToDate();
	m_CE_DateTo.SetTypeToDate();
	m_CE_TimeFrom.SetTypeToTime();
	m_CE_TimeTo.SetTypeToTime();

	m_CE_DateFrom.SetBKColor(YELLOW);
	m_CE_DateTo.SetBKColor(YELLOW);
	m_CE_TimeFrom.SetBKColor(YELLOW);
	m_CE_TimeTo.SetBKColor(YELLOW);

	m_DateFrom = pomMinDate->Format("%d.%m.%Y");
	m_CE_DateFrom.SetWindowText( m_DateFrom );

	m_TimeFrom = pomMinDate->Format("%H:%M");
	m_CE_TimeFrom.SetWindowText( m_TimeFrom );

	m_DateTo = pomMaxDate->Format("%d.%m.%Y" );
	m_CE_DateTo.SetWindowText( m_DateTo );

	m_TimeTo = pomMaxDate->Format("%H:%M");
	m_CE_TimeTo.SetWindowText( m_TimeTo );
 
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}



void ReportSeqField3DateTimeDlg::SetFieldType(BasicDataType ipFieldType, CCSEdit &ropField) 
{

	switch (ipFieldType)
	{
		case REPSEQFIELD3_AIRLINECODE:
			ropField.SetTypeToString("X|#X|#X|#",3,2);
 			break;
		case REPSEQFIELD3_DELAYCODE:
 			ropField.SetTypeToString("XX",2,0);
			break;
		case REPSEQFIELD3_DELAYTIME:
			ropField.SetTypeToString("####",4,0);
			break;
	}


	return;
}




