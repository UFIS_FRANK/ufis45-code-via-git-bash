// LoginDlg.cpp : implementation file
//

#include <stdafx.h>
#include <CCSCedadata.h>
#include <CCSCedacom.h>
#include <LoginDlg.h>
#include <PrivList.h>
#include <BasicData.h>


#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLoginDialog dialog

//extern CCSBasicData ogBasicData;


CLoginDialog::CLoginDialog(const char *pcpHomeAirport, const char *pcpAppl, CWnd* pParent /*=NULL*/)
    : CDialog(CLoginDialog::IDD, pParent)
{
    //{{AFX_DATA_INIT(CLoginDialog)
	//}}AFX_DATA_INIT

	strcpy(pcmHomeAirport,pcpHomeAirport);
	strcpy(pcmAppl,pcpAppl);

	// default text

	INVALID_USERNAME = GetString(IDS_STRING723);
	INVALID_APPLICATION = GetString(IDS_STRING724);
	INVALID_PASSWORD = GetString(IDS_STRING725);
	EXPIRED_USERNAME = GetString(IDS_STRING726);
	EXPIRED_APPLICATION = GetString(IDS_STRING727);
	EXPIRED_WORKSTATION = GetString(IDS_STRING728);
	DISABLED_USERNAME = GetString(IDS_STRING729);
	DISABLED_APPLICATION = GetString(IDS_STRING730);
	DISABLED_WORKSTATION = GetString(IDS_STRING731);
	UNDEFINED_PROFILE = GetString(IDS_STRING732);
	MESSAGE_BOX_CAPTION = GetString(IDS_STRING733);
	USERNAME_CAPTION = GetString(IDS_STRING734);
	PASSWORD_CAPTION = GetString(IDS_STRING735);
	OK_CAPTION = GetString(IDS_STRING736);
	CANCEL_CAPTION = GetString(IDS_STRING737);
	WINDOW_CAPTION = GetString(IDS_STRING738);

}

void CLoginDialog::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CLoginDialog)
	DDX_Control(pDX, IDC_USERNAME, m_UsernameCtrl);
	DDX_Control(pDX, IDC_PASSWORD, m_PasswordCtrl);
	DDX_Control(pDX, IDC_USIDCAPTION, m_UsidCaption);
	DDX_Control(pDX, IDC_PASSCAPTION, m_PassCaption);
	DDX_Control(pDX, IDOK, m_OkCaption);
	DDX_Control(pDX, IDCANCEL, m_CancelCaption);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CLoginDialog, CDialog)
    //{{AFX_MSG_MAP(CLoginDialog)
    ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CLoginDialog message handlers

void CLoginDialog::OnPaint() 
{
    CPaintDC dc(this); // device context for painting
    
    // TODO: Add your message handler code here
    CBrush olBrush( RGB( 192, 192, 192 ) );
    CBrush *polOldBrush = dc.SelectObject( &olBrush );
    CRect olRect;
    GetClientRect( &olRect );
    dc.FillRect( &olRect, &olBrush );
    dc.SelectObject( polOldBrush );
    
    CDC olDC;
    olDC.CreateCompatibleDC( &dc );
    CBitmap olBitmap;
    olBitmap.LoadBitmap( IDB_LOGIN );
    olDC.SelectObject( &olBitmap );
    dc.BitBlt( 10 , 140, 409, 52, &olDC, 0, 0, SRCCOPY );
    olDC.DeleteDC( );
    
    // Do not call CDialog::OnPaint() for painting messages
}

void CLoginDialog::OnOK() 
{
	bool blRc = true;
	CWnd *polFocusCtrl = &m_UsernameCtrl;
	char pclErrTxt[500];

	char pclCaption[300];

	strcpy(pclCaption,MESSAGE_BOX_CAPTION);

	// get the username
	m_UsernameCtrl.GetWindowText(omUsername);
	m_PasswordCtrl.GetWindowText(omPassword);


	// check the username + password
	AfxGetApp()->DoWaitCursor(1);

	
	strcpy(pcgUser, omUsername);
	strcpy(pcgPasswd, omPassword);
	ogBasicData.omUserID = omUsername;
	ogCommHandler.SetUser(omUsername);
	strcpy(CCSCedaData::pcmUser, omUsername);
	strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);

	

	blRc = ogPrivList.Login(pcmHomeAirport,omUsername,omPassword,pcmAppl);
	AfxGetApp()->DoWaitCursor(-1);

	if( blRc )
	{
		CDialog::OnOK();
	}
	else
	{
		if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR INVALID_USER") ) { 
			strcpy(pclErrTxt,INVALID_USERNAME);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR INVALID_APPLICATION") ) {
			strcpy(pclErrTxt,INVALID_APPLICATION);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR INVALID_PASSWORD") ) {
			strcpy(pclErrTxt,INVALID_PASSWORD);
			polFocusCtrl = &m_PasswordCtrl;
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR EXPIRED_USER") ) {
			strcpy(pclErrTxt,EXPIRED_USERNAME);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR EXPIRED_APPLICATION") ) {
			strcpy(pclErrTxt,EXPIRED_APPLICATION);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR EXPIRED_WORKSTATION") ) {
			strcpy(pclErrTxt,EXPIRED_WORKSTATION);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR DISABLED_USER") ) {
			strcpy(pclErrTxt,DISABLED_USERNAME);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR DISABLED_APPLICATION") ) {
			strcpy(pclErrTxt,DISABLED_APPLICATION);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR DISABLED_WORKSTATION") ) {
			strcpy(pclErrTxt,DISABLED_WORKSTATION);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR UNDEFINED_PROFILE") ) {
			strcpy(pclErrTxt,UNDEFINED_PROFILE);
		}
		else 
		{
			if((ogPrivList.omErrorMessage.Find(omPassword) >= 0) && (ogPrivList.omErrorMessage.Find(omUsername) >= 0))
			{

				strcpy(pclErrTxt,GetString(IDS_STRING1427)); // "CEDA down"
				strcpy(pclCaption,GetString(ST_FEHLER));
				
			}
			else
			{
				strcpy(pclErrTxt,ogPrivList.omErrorMessage);
			}
		}

		MessageBox(pclErrTxt,pclCaption,MB_ICONINFORMATION);
	
		imLoginCount++;

		if( imLoginCount >= MAX_LOGIN )
			OnCancel();
		else
		{
			if( polFocusCtrl != NULL )
				polFocusCtrl->SetFocus(); // set the focus to the offending control
		}
	}
}


void CLoginDialog::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}




BOOL CLoginDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CString olLight(" ");
	#ifdef _FIPSLIGHT
	{
		olLight = "-LIGHT ";
	}
	#else
	{
	}
	#endif


//	CString olCaption = GetString(IDS_STRING1207) + " " + ogAppName + " " + GetString(IDS_STRING1208) + " ";
	CString olCaption = GetString(IDS_STRING1207) + " " + ogAppName + olLight + GetString(IDS_STRING1208) + " ";
	olCaption  += ogCommHandler.pcmRealHostName;
	olCaption  += " / ";
	olCaption  += ogCommHandler.pcmRealHostType;
	SetWindowText(olCaption  );
	
	imLoginCount = 0;

	m_UsidCaption.SetWindowText(USERNAME_CAPTION);
	m_PassCaption.SetWindowText(PASSWORD_CAPTION);
	m_OkCaption.SetWindowText(OK_CAPTION);
	m_CancelCaption.SetWindowText(CANCEL_CAPTION);

	m_UsernameCtrl.SetTypeToString("X(32)",32,1);
	m_UsernameCtrl.SetBKColor(YELLOW);
	m_UsernameCtrl.SetTextErrColor(RED);

	m_PasswordCtrl.SetTypeToString("X(32)",32,1);
	m_PasswordCtrl.SetBKColor(YELLOW);
	m_PasswordCtrl.SetTextErrColor(RED);


	m_UsernameCtrl.SetFocus();


	CRect olRect;

	GetWindowRect(olRect);

	int left = (1024 - (olRect.right - olRect.left)) /2;
	int top =  (768  - (olRect.bottom - olRect.top)) /2;

	SetWindowPos(&wndTop,left, top, 0, 0, SWP_SHOWWINDOW | SWP_NOSIZE);

	UpdateWindow();

	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}




