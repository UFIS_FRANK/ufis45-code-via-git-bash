// FlughaefenTableViewer.cpp 
//
//	Liste aller angeflogenen Flugh�fen mit Frequenz
#include <stdafx.h>
#include <Report10TableViewer.h>
#include <CcsGlobl.h>
#include <resrc1.h>

#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// FlughaefenTableViewer
//
FlughaefenTableViewer::FlughaefenTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo, bool bpArr, bool bpDep)
{
	pomData = popData;
	pcmInfo = pcpInfo;

	bmIsFromSearch = FALSE;
    pomFlughaefenTable = NULL;

	bmListStateArr = bpArr;
}

//-----------------------------------------------------------------------------------------------

FlughaefenTableViewer::~FlughaefenTableViewer()
{
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void FlughaefenTableViewer::Attach(CCSTable *popTable)
{
    pomFlughaefenTable = popTable;
}

//-----------------------------------------------------------------------------------------------
void FlughaefenTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void FlughaefenTableViewer::MakeLines()
{
/*	int ilCntr = 0;
	CString olNewstr("");
	CString olOldstr(""); 

	int ilCount = pomData->GetSize();

	for (int ilLc = ilCount - 1; ilLc >= 0 ; ilLc--)
	{
		ilCntr++;
		ROTATIONDLGFLIGHTDATA *prlFlughaefenData = &pomData->GetAt(ilLc);
		olNewstr = prlFlughaefenData->Des3;

		if(ilLc == ilCount - 1)	// der erste
		{
			olOldstr = olNewstr;
		}
		if (olNewstr != olOldstr)	// Des3 wechselt
		{
			MakeLine(olOldstr, ilCntr);
			olOldstr = olNewstr;
			ilCntr = 0;
		}

		if((ilLc == 0) && (ilCntr > 0))	// nach dem letzten
			MakeLine(olNewstr, ilCntr);
	}
*/
    CMapStringToPtr olDestMap;
	RotationDlgCedaFlightData olData;

	int ilData = pomData->GetSize();

	if (!bmListStateArr)
	{
		for (int i = 0; i < ilData; i++)
		{
			ROTATIONDLGFLIGHTDATA *prlFlight = &pomData->GetAt(i);
			ASSERT(prlFlight);

			FLUGHAEFENTABLE_LINEDATA  *prlLine = NULL;
			if (! olDestMap.Lookup((LPCSTR)prlFlight->Des3, (void *&)prlLine) == TRUE)
			{
				prlLine = new FLUGHAEFENTABLE_LINEDATA;
				olDestMap.SetAt((LPCSTR)prlFlight->Des3, prlLine);
			}

			ASSERT(prlLine);

			CCSPtrArray<VIADATA> olVias;
			int ilAnzVias = olData.GetViaArray(&olVias, prlFlight);

			prlLine->Des3 = prlFlight->Des3;
			if (ilAnzVias == 0)
				prlLine->Direct++;
			else
				prlLine->ViaN++;

			if (prlLine->Airlines.Find(CString(prlFlight->Alc2)) == -1)
	//		if( strcmp(prlLine->Airlines, prlFlight->Alc2) != 0)
			{
				if ( prlLine->Airlines.GetLength() == 0)
					prlLine->Airlines += CString(prlFlight->Alc2);
				else
					prlLine->Airlines += CString(",") + CString(prlFlight->Alc2);
			}


//			CCSPtrArray<VIADATA> olVias;
//			int ilAnzVias = olData.GetViaArray(&olVias, prlFlight);
			for( int ilVia=0; ilVia < ilAnzVias; ilVia++ )
			{
				CString	ropDes = olVias[ilVia].Apc3 ;

				prlLine = NULL;
				if (! olDestMap.Lookup((LPCSTR)ropDes, (void *&)prlLine) == TRUE)
				{
					prlLine = new FLUGHAEFENTABLE_LINEDATA;
					olDestMap.SetAt((LPCSTR)ropDes, prlLine);
				}

				ASSERT(prlLine);
				prlLine->Des3 = ropDes;
				if (ilVia == 0)
					prlLine->Via1++;
				else
					prlLine->ViaN++;

				if (prlLine->Airlines.Find(CString(prlFlight->Alc2)) == -1)
	//			if( strcmp(prlLine->Airlines, prlFlight->Alc2) != 0)
				{
					if ( prlLine->Airlines.GetLength() == 0)
						prlLine->Airlines += CString(prlFlight->Alc2);
					else
						prlLine->Airlines += CString(",") + CString(prlFlight->Alc2);
				}

			}
		}
	}
	else
	{
		for (int i = 0; i < ilData; i++)
		{
			ROTATIONDLGFLIGHTDATA *prlFlight = &pomData->GetAt(i);
			ASSERT(prlFlight);

			FLUGHAEFENTABLE_LINEDATA  *prlLine = NULL;
			if (! olDestMap.Lookup((LPCSTR)prlFlight->Org3, (void *&)prlLine) == TRUE)
			{
				prlLine = new FLUGHAEFENTABLE_LINEDATA;
				olDestMap.SetAt((LPCSTR)prlFlight->Org3, prlLine);
			}

			ASSERT(prlLine);

			CCSPtrArray<VIADATA> olVias;
			int ilAnzVias = olData.GetViaArray(&olVias, prlFlight);

			prlLine->Des3 = prlFlight->Org3;
			if (ilAnzVias == 0)
				prlLine->Direct++;
			else
				prlLine->ViaN++;

			if (prlLine->Airlines.Find(CString(prlFlight->Alc2)) == -1)
	//		if( strcmp(prlLine->Airlines, prlFlight->Alc2) != 0)
			{
				if ( prlLine->Airlines.GetLength() == 0)
					prlLine->Airlines += CString(prlFlight->Alc2);
				else
					prlLine->Airlines += CString(",") + CString(prlFlight->Alc2);
			}


//			CCSPtrArray<VIADATA> olVias;
//			int ilAnzVias = olData.GetViaArray(&olVias, prlFlight);
			for( int ilVia=0; ilVia < ilAnzVias; ilVia++ )
			{
				CString	ropDes = olVias[ilVia].Apc3 ;

				prlLine = NULL;
				if (! olDestMap.Lookup((LPCSTR)ropDes, (void *&)prlLine) == TRUE)
				{
					prlLine = new FLUGHAEFENTABLE_LINEDATA;
					olDestMap.SetAt((LPCSTR)ropDes, prlLine);
				}

				ASSERT(prlLine);
				prlLine->Des3 = ropDes;
				if (ilVia == ilAnzVias-1)
					prlLine->Via1++;
				else
					prlLine->ViaN++;

				if (prlLine->Airlines.Find(CString(prlFlight->Alc2)) == -1)
	//			if( strcmp(prlLine->Airlines, prlFlight->Alc2) != 0)
				{
					if ( prlLine->Airlines.GetLength() == 0)
						prlLine->Airlines += CString(prlFlight->Alc2);
					else
						prlLine->Airlines += CString(",") + CString(prlFlight->Alc2);
				}

			}
		}
	}

	CString olTmp;
	POSITION pos;
	for( pos = olDestMap.GetStartPosition(); pos != NULL; )
	{
		FLUGHAEFENTABLE_LINEDATA  *prlLine = NULL;
		olDestMap.GetNextAssoc( pos, olTmp, (void *&)prlLine );

		if (prlLine)
		{
			CreateLine(prlLine);
		}
	}
	olDestMap.RemoveAll();

//generate the headerinformation
	CString olTimeSet = GetString(IDS_STRING1920); //UTC
	if(bgReportLocal)
		olTimeSet = GetString(IDS_STRING1921); //LOCAL

	char pclHeader[256];

	CString olReportName;
	if (bmListStateArr)
		olReportName = GetString(IDS_REPORTS_RB_FlightOrigin);
	else
		olReportName = GetString(IDS_REPORTS_RB_FlightDestination_LONG);

	sprintf(pclHeader, GetString(IDS_R_HEADER10), olReportName, pcmInfo, omLines.GetSize(), olTimeSet);
	omTableName = pclHeader;

	sprintf(pclHeader, GetString(IDS_R_HEADER10), GetString(IDS_REPORTS_RB_FlightDestination), pcmInfo, omLines.GetSize(), olTimeSet);
	omPrintName = pclHeader;


}

//-----------------------------------------------------------------------------------------------

void FlughaefenTableViewer::MakeLine(CString opDes3old, int ipCntr)
{
    // Update viewer data for this shift record
	char clbuffer[10];
    FLUGHAEFENTABLE_LINEDATA rlFlughaefen;
	sprintf(clbuffer,"%d", ipCntr);
	rlFlughaefen.Cntr = clbuffer; 
	rlFlughaefen.Des3 = opDes3old; 

	CreateLine(&rlFlughaefen);
}

//-----------------------------------------------------------------------------------------------

void FlughaefenTableViewer::CreateLine(FLUGHAEFENTABLE_LINEDATA *prpFlughaefen)
{
	FLUGHAEFENTABLE_LINEDATA rlFlughaefen;
	rlFlughaefen = *prpFlughaefen;

    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
	{
//        if (CompareFlight(&rlFlughaefen, &omLines[ilLineno-1]) >= 0)
//            break;  // should be inserted after Lines[ilLineno-1]

		char pclDes1[10];
		char pclDes2[10];
		//sprintf(pclInfo, "Angeflogene Flugh�fen mit Frequenz %s", pcmInfo);
		sprintf(pclDes1, rlFlughaefen.Des3);
		sprintf(pclDes2, omLines[ilLineno-1].Des3);
		if (strcmp(pclDes1,pclDes2) > 0)
			break;
	}

    omLines.NewAt(ilLineno, rlFlughaefen);
	return ;

/*

	int ilLineno = 0;
	FLUGHAEFENTABLE_LINEDATA rlFlughaefen;
	rlFlughaefen = *prpFlughaefen;
    omLines.NewAt(ilLineno, rlFlughaefen);
*/

}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void FlughaefenTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 6;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomFlughaefenTable->SetShowSelection(TRUE);
	pomFlughaefenTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	if(bmListStateArr)
	{
		rlHeader.Length = 90; 
		rlHeader.Text = GetString(IDS_STRING_ORG);
		omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	}
	else
	{
		rlHeader.Length = 90; 
		rlHeader.Text = GetString(IDS_STRING_DEST);
		omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	}

	rlHeader.Length = 60;
	rlHeader.Text = GetString(IDS_STRING_DIRECT);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	if(bmListStateArr)
	{
		rlHeader.Length = 60;
		rlHeader.Text = GetString(IDS_STRING_VIAL);
		omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

		rlHeader.Length = 60;
		rlHeader.Text = GetString(IDS_STRING_VIAS);
		omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	}
	else
	{
		rlHeader.Length = 60;
		rlHeader.Text = GetString(IDS_STRING_VIA1);
		omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

		rlHeader.Length = 60;
		rlHeader.Text = GetString(IDS_STRING_VIAN);
		omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	}

	rlHeader.Length = 60;
	rlHeader.Text = GetString(IDS_STRING_TOTAL);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	rlHeader.Length = 380;
	rlHeader.Text = GetString(IDS_STRING_AIRL);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);


	pomFlughaefenTable->SetHeaderFields(omHeaderDataArray);

	pomFlughaefenTable->SetDefaultSeparator();
	pomFlughaefenTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		char clbuffer[10];
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Des3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		sprintf(clbuffer,"%d", omLines[ilLineNo].Direct);
		rlColumnData.Text = clbuffer;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		sprintf(clbuffer,"%d", omLines[ilLineNo].Via1);
		rlColumnData.Text = clbuffer;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		sprintf(clbuffer,"%d", omLines[ilLineNo].ViaN);
		rlColumnData.Text = clbuffer;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		omLines[ilLineNo].Total = omLines[ilLineNo].Direct + omLines[ilLineNo].Via1 + omLines[ilLineNo].ViaN;
		sprintf(clbuffer,"%d", omLines[ilLineNo].Total);
		rlColumnData.Text = clbuffer;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = omLines[ilLineNo].Airlines;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomFlughaefenTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomFlughaefenTable->DisplayTable();
}


//-----------------------------------------------------------------------------------------------

void FlughaefenTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

void FlughaefenTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void FlughaefenTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	char pclFooter[256];
	//sprintf(pclInfo, "Angeflogene Flugh�fen mit Frequenz %s", pcmInfo);
/*	sprintf(pclFooter, GetString(IDS_STRING1147), (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));
	omTableName = GetString(IDS_STRING1148);
	omFooterName = pclFooter;
*/
	sprintf(pclFooter, omTableName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));

	omFooterName = pclFooter;

	CString olTableName = GetString(IDS_REPORTS_RB_FlightDestination);
	if(bmListStateArr)
		olTableName = GetString(IDS_REPORTS_RB_FlightOrigin);

	int ilOrientation = PRINT_PORTRAET;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			//pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			pomPrint->imLineNo = pomPrint->imMaxLines + 1 - 2;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = olTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();

//			olFooter1.Format(GetString(IDS_STRING1445), pomData->GetSize()/*ilLines*/,omFooterName);
			olFooter1.Format("%s -   %s", omFooterName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );

			for(int i = 0; i < ilLines; i++ ) 
			{
				//if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				if(pomPrint->imLineNo >= pomPrint->imMaxLines-2)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				//if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-3) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;

	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool FlughaefenTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

/*
	char pclHeader[100];
	//sprintf(pclHeader, "Angeflogene Flugh�fen mit Frequenz  vom %s", pcmInfo);
	sprintf(pclHeader, GetString(IDS_STRING1149), pcmInfo);
	CString olTableName(pclHeader);

	//pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);
	pomPrint->PrintUIFHeader("",CString(pclHeader),pomPrint->imFirstLine-10);
*/
//	double dgCCSPrintFactor = 2.7 ;
	pomPrint->PrintUIFHeader("",omPrintName,pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Text   = omHeaderDataArray[i].Text;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool FlughaefenTableViewer::PrintTableLine(FLUGHAEFENTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		
		rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameTop   = PRINT_FRAMETHIN;
		if(bpLastLine == true)
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN;
		}
		else
		{
			rlElement.FrameBottom = PRINT_NOFRAME;
		}
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		char clbuffer[10];
		switch(i)
		{
		case 0:
			{
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Des3;
			}
			break;
		case 1:
			{
				sprintf(clbuffer,"%d", prpLine->Direct);
				rlElement.FrameLeft = PRINT_FRAMETHIN;
				rlElement.Text		= clbuffer;
			}
			break;
		case 2:
			{
				sprintf(clbuffer,"%d", prpLine->Via1);
				rlElement.FrameLeft = PRINT_FRAMETHIN;
				rlElement.Text		= clbuffer;
			}
			break;
		case 3:
			{
				sprintf(clbuffer,"%d", prpLine->ViaN);
				rlElement.FrameLeft = PRINT_FRAMETHIN;
				rlElement.Text		= clbuffer;
			}
			break;
		case 4:
			{
				sprintf(clbuffer,"%d", prpLine->Total);
				rlElement.FrameLeft = PRINT_FRAMETHIN;
				rlElement.Text		= clbuffer;
			}
			break;
		case 5:
			{
				rlElement.FrameLeft = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Airlines;
				rlElement.FrameRight = PRINT_FRAMETHIN;
			}
			break;
		}
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------


// Drucken in Datei
bool FlughaefenTableViewer::PrintPlanToFile(char *opTrenner)
{

	ofstream of;

	char* tmpvar;
	tmpvar = getenv("TMP");
	if (tmpvar == NULL)
		tmpvar = getenv("TEMP");

	if (tmpvar == NULL)
		return false;

	CString olFileName = omTableName;
	olFileName.Remove('*');
	olFileName.Remove('.');
	olFileName.Remove(':');
	olFileName.Remove('/');
	olFileName.Replace(" ", "_");
	
	char pHeader[256];
	strcpy (pHeader, tmpvar);
	CString path = pHeader;
	omFileName =  path + "\\" + olFileName + ".csv";

//	of.open( GetString(IDS_STRING1386), ios::out);
	of.open( omFileName, ios::out);

	int ilwidth = 1;

	CString olstr1;
	CString olstr3;
	CString olstr4;

	if(bmListStateArr)
	{
		olstr1 = GetString(IDS_STRING_ORG);
		olstr3 = GetString(IDS_STRING_VIAL);
		olstr4 = GetString(IDS_STRING_VIAS);
	}
	else
	{
		olstr1 = GetString(IDS_STRING_DEST);
		olstr3 = GetString(IDS_STRING_VIA1);
		olstr4 = GetString(IDS_STRING_VIAN);
	}


	of  << setw(ilwidth) << omTableName << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;

	of	<< setw(ilwidth) << olstr1 
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING_DIRECT) 
		<< setw(1) << opTrenner
		<< setw(ilwidth) << olstr3
		<< setw(1) << opTrenner
		<< setw(ilwidth) << olstr4
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING_TOTAL)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING_AIRL) 
		<< endl;


	int ilCount = omLines.GetSize();

	for(int i = 0; i < ilCount; i++)
	{
		FLUGHAEFENTABLE_LINEDATA rlLine = omLines[i];

		char clbuffer2[10];
		char clbuffer3[10];
		char clbuffer4[10];
		char clbuffer5[10];

		sprintf(clbuffer2,"%d", rlLine.Direct);
		sprintf(clbuffer3,"%d", rlLine.Via1);
		sprintf(clbuffer4,"%d", rlLine.ViaN);
		sprintf(clbuffer5,"%d", rlLine.Total);

		of.setf(ios::left, ios::adjustfield);

		of   << setw(ilwidth) << rlLine.Des3
		     << setw(1) << opTrenner 
		     << setw(ilwidth) << clbuffer2
		     << setw(1) << opTrenner 
		     << setw(ilwidth) << clbuffer3
		     << setw(1) << opTrenner 
			 << setw(ilwidth) << clbuffer4
		     << setw(1) << opTrenner 
			 << setw(ilwidth) << clbuffer5
		     << setw(1) << opTrenner 
			 << setw(ilwidth) << rlLine.Airlines
			 << endl;

	}

	of.close();
	return true;

/*
	ofstream of;
	of.open( pcpDefPath, ios::out);
	of << GetString(IDS_STRING1148) << ": " 
		<< pcmInfo << "    "
		<< CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M") << endl << endl;

	of  << GetString(IDS_STRING990) << "   " 
		<< GetString(IDS_STRING1138) << "    " 
		<< endl;

	of << "---------------------------------------------------------------" << endl;
	int ilCount = omLines.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		FLUGHAEFENTABLE_LINEDATA rlD = omLines[i];
		of.setf(ios::left, ios::adjustfield);
		of   << setw(12) << rlD.Des3  
			 << setw(12) << rlD.Cntr << endl; 
	}
	of.close();
//	MessageBox("Es wurde folgende Datei geschrieben:\nc:\\tmp\\Flughaefen.txt", "Drucken",(MB_ICONINFORMATION|MB_OK));
*/


}
