// PrintInfo.cpp : implementation file
//  

#include <stdafx.h>
#include <PrintInfo.h>
#include <resource.h>
#include <WINSPOOL.H>
#include <CCSGlobl.h>
#include <math.h>

#define INCH 0.284
#define MMX(x) ((int)(MulDiv((x),imLogPixelsX, 72)*INCH))
#define MMY(x) ((int)(MulDiv((x),imLogPixelsY, 72)*INCH))

//-----------------------------------------------------------------------------------------------

PrintInfo::PrintInfo(CString opInfoName,CWnd *opParent)
{
	bmIsInitialized = false;
	pomParent = opParent;
	omInfoName = opInfoName;
	imOrientation = PRINTINFO_PORTRAIT;
	imLineHeight = 50;
	imActualLine = 0;
	
	if (imOrientation == PRINTINFO_LANDSCAPE)
	{
		imFirstPos  = 200;
		imRightEnd = 3000; // nicht getestet
		imLeftOffset = 50;
		imMaxLines = 27;
	}
	else
	{
		imFirstPos  = 200;
		imRightEnd = 1700; // 10 = 1mm
		imLeftOffset = 200;
		imMaxLines = 47;
	}
	imActualPos = imFirstPos;
}

//-----------------------------------------------------------------------------------------------

PrintInfo::~PrintInfo()
{
	if (bmIsInitialized == true)
	{
		omCdc.DeleteDC();
		omThinPen.DeleteObject();
		omMediumPen.DeleteObject();
		omThickPen.DeleteObject();
		omDottedPen.DeleteObject();
		omRgn.DeleteObject();
	}
}

//-----------------------------------------------------------------------------------------------

void PrintInfo::SetBitmaps(CBitmap *popBitmap)
{
/*
	if(popBitmap != NULL)
	{
		pomBitmap = popBitmap;
	}
*/
}

//-----------------------------------------------------------------------------------------------

bool PrintInfo::InitializePrinter()
{
	int ilRc;
	HDC hlHdc;
	char pclDevices[182];
	::GetProfileString("windows", "device","",pclDevices,180);
	CString olDevices = pclDevices;
	CString olDeviceName;
	int ilKomma = olDevices.Find(',');
	if (ilKomma > -1) olDeviceName = olDevices.Left(ilKomma);
	char pclDeviceName[182];
	strcpy(pclDeviceName,olDeviceName);
	HANDLE  hlPrinter = 0;
	static HANDLE hDevMode = 0;
	//if (hDevMode == 0)
	{
		if (OpenPrinter(pclDeviceName,&hlPrinter,NULL) == TRUE)
		{
			//if (GetPrinter(hlPrinter,2,(unsigned char *)&rlPrinterInfo,sizeof(rlPrinterInfo),&llBytesReceived) == TRUE)
			{
				LONG llDevModeLen = DocumentProperties(NULL,hlPrinter,pclDeviceName,NULL,NULL,0);
				if (llDevModeLen > 0)
				{
					hDevMode = GlobalAlloc(GMEM_MOVEABLE,llDevModeLen);
					DEVMODE *prlDevMode = (DEVMODE *) GlobalLock(hDevMode);
					DocumentProperties(NULL,hlPrinter,pclDeviceName,prlDevMode,NULL,DM_OUT_BUFFER);
					DWORD dm_orientation =  DM_ORIENTATION;
					prlDevMode->dmFields = DM_ORIENTATION;
					if(imOrientation == PRINTINFO_LANDSCAPE)
					{
						prlDevMode->dmOrientation = DMORIENT_LANDSCAPE;
					}
					else
					{
						prlDevMode->dmOrientation = DMORIENT_PORTRAIT;
					}
					DocumentProperties(NULL,hlPrinter,pclDeviceName,prlDevMode,prlDevMode,DM_IN_BUFFER|DM_OUT_BUFFER);
					GlobalUnlock(hDevMode);
				}
			}
			ClosePrinter(hlPrinter);
		}
	}
	CPrintDialog *polPrintDialog = new CPrintDialog(FALSE,
		PD_ALLPAGES|PD_NOPAGENUMS|PD_NOSELECTION|PD_USEDEVMODECOPIESANDCOLLATE,pomParent);
	if (hDevMode != 0) polPrintDialog->m_pd.hDevMode = hDevMode;
	LPDEVMODE prlOldDevMode = polPrintDialog->GetDevMode( );
	ilRc = polPrintDialog->DoModal();
	if (ilRc != IDCANCEL )
	{
		LPDEVMODE prlDevMode = polPrintDialog->GetDevMode( );

		hlHdc = polPrintDialog->GetPrinterDC();
		if(hlHdc == NULL)
		{
			MessageBox(NULL, GetString(IDS_STRING904), GetString(IDS_STRING905), MB_OK|MB_ICONEXCLAMATION);
			return false;
		}
		else
		{
			omCdc.Attach(hlHdc);
			omCdc.SetMapMode(MM_TEXT);
			imLogPixelsY = omCdc.GetDeviceCaps(LOGPIXELSY);
			imLogPixelsX = omCdc.GetDeviceCaps(LOGPIXELSX);
			omRgn.CreateRectRgn(0,0,omCdc.GetDeviceCaps(HORZRES),omCdc.GetDeviceCaps(VERTRES));
			omCdc.SelectClipRgn(&omRgn);
			LOGFONT rlLf;
			memset(&rlLf, 0, sizeof(LOGFONT));

			//Courier New 8 
			rlLf.lfHeight = - MulDiv(8, imLogPixelsY, 72);
			rlLf.lfWeight = FW_NORMAL;
			rlLf.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
			lstrcpy(rlLf.lfFaceName, "Courier New");
			ilRc = omCourierNew_Regular_8.CreateFontIndirect(&rlLf);
			//Courier New 8 Bold
			rlLf.lfWeight = FW_BOLD;
			ilRc = omCourierNew_Bold_8.CreateFontIndirect(&rlLf);
			//Arial 8
			rlLf.lfWeight = FW_NORMAL;
			rlLf.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
			lstrcpy(rlLf.lfFaceName, "Arial");
			ilRc = omArial_Regular_8.CreateFontIndirect(&rlLf);
			//Arial 10
			rlLf.lfHeight = - MulDiv(10, imLogPixelsY, 72); 
			ilRc = omArial_Regular_10.CreateFontIndirect(&rlLf);
			//Arial 12
			rlLf.lfHeight = - MulDiv(12, imLogPixelsY, 72); 
			ilRc = omArial_Regular_12.CreateFontIndirect(&rlLf);
			//Arial 18
			rlLf.lfHeight =  - MulDiv(18, imLogPixelsY, 72);   
			ilRc = omArial_Regular_18.CreateFontIndirect(&rlLf);
			//Arial 8 Bold
			rlLf.lfHeight = - MulDiv(8, imLogPixelsY, 72);
			rlLf.lfWeight = FW_BOLD;
			ilRc = omArial_Bold_8.CreateFontIndirect(&rlLf);
			//Arial 10 Bold
			rlLf.lfHeight = - MulDiv(10, imLogPixelsY, 72); 
			ilRc = omArial_Bold_10.CreateFontIndirect(&rlLf);
			//Arial 11 Bold
			rlLf.lfHeight = - MulDiv(11, imLogPixelsY, 72); 
			ilRc = omArial_Bold_11.CreateFontIndirect(&rlLf);
			//Arial 12 Bold
			rlLf.lfHeight = - MulDiv(12, imLogPixelsY, 72); 
			ilRc = omArial_Bold_12.CreateFontIndirect(&rlLf);
			//Arial 18 Bold
			rlLf.lfHeight =  - MulDiv(18, imLogPixelsY, 72);   
			ilRc = omArial_Bold_18.CreateFontIndirect(&rlLf);
			///////////////

			omThinPen.CreatePen(PS_SOLID, MulDiv(2, imLogPixelsX, 254), RGB(0,0,0));
			omMediumPen.CreatePen(PS_SOLID, MulDiv(4, imLogPixelsX, 254), RGB(0,0,0));
			omThickPen.CreatePen(PS_SOLID, MulDiv(8, imLogPixelsX, 254), RGB(0,0,0));
			omDottedPen.CreatePen(PS_DOT,1, RGB(0,0,0));
			delete polPrintDialog;

			// loading the header bitmap

			//omBitmap.LoadBitmap(IDB_HAJLOGO);
			//omMemDc.CreateCompatibleDC(&omCdc);
			//omMemDc.SelectObject(omBitmap);

			//MWO pomCcsBitmap.LoadBitmap(IDB_CCS);
			//omCcsMemDc.CreateCompatibleDC(&omCdc);
			//omCcsMemDc.SelectObject(pomCcsBitmap);

			bmIsInitialized = true;

			return true;
		}
	}
	else
	{
		return false;
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

bool PrintInfo::Print(INFDATA opInfo)
{
	CSize olSize;
	CString olTmpInfoText;
	CString olTmpText;

	omInfo = opInfo;
	//*****************************************************
	// if a sentence longer as the page, cut whith "\r\n", 
	ogInfData.TranslateText(&omInfo, omInfoText, true);
	omInfoText += CString("\r\n");
	int ilInfoLines = 0;
	int ilLength = omInfoText.GetLength();
	int ilStrStartPos = 0;
	int ilStrPos = 0;
	omCdc.SelectObject(&omArial_Regular_10);

	for(; ilStrPos<ilLength; ilStrPos++)
	{
		if(omInfoText.Mid(ilStrPos,2) == "\r\n")
		{
			ilInfoLines++;

			olTmpInfoText = omInfoText.Mid(ilStrStartPos,ilStrPos-ilStrStartPos);
			olSize = omCdc.GetTextExtent(olTmpInfoText);

			if(olSize.cx > MMX(imRightEnd))
			{
				int ilTmpTextPos = 0;
				int ilTmpTextLength = olTmpInfoText.GetLength();
				int ilLastTmpTextPos = 0;
				int ilLastBlankPos = 0;

				for(; ilTmpTextPos<ilTmpTextLength; ilTmpTextPos++)
				{
					olTmpText = olTmpInfoText.Left(ilTmpTextPos);
					olSize = omCdc.GetTextExtent(olTmpText);

					if(olSize.cx > MMX(imRightEnd))
					{
						ilTmpTextPos--;
						if(ilLastBlankPos+10 < ilTmpTextPos)
						{
							omInfoText = omInfoText.Left(ilStrStartPos + ilTmpTextPos) + CString("\r\n") + omInfoText.Right(ilLength-(ilStrStartPos + ilTmpTextPos));
							ilStrPos = ilStrStartPos + ilTmpTextPos;
						}
						else
						{
							omInfoText = omInfoText.Left(ilStrStartPos + ilLastBlankPos) + CString("\r\n") + omInfoText.Right(ilLength-(ilStrStartPos + ilLastBlankPos));
							ilStrPos = ilStrStartPos + ilLastBlankPos;
						}
						ilLength += 2;
						ilInfoLines++;
						break;
					}
					else
					{
						if(olTmpText.Mid(ilTmpTextPos-1,1) == " ")
						{
							ilLastBlankPos = ilTmpTextPos;
						}
					}
				}
			}
			ilStrStartPos = ilStrPos+2;
		}
	}
	omInfoText = omInfoText.Left(omInfoText.GetLength()-2);
	ilInfoLines--;
	//*****************************************************

	// Seitenzahl ermitteln
	dmPages = ceil((double)(ilInfoLines+1)/(double)imMaxLines);

	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//SetBitmaps(&omBitmap,&omBitmap);

	DOCINFO	rlDocInfo;
	memset(&rlDocInfo, 0, sizeof(DOCINFO));
	rlDocInfo.cbSize = sizeof( DOCINFO );
	rlDocInfo.lpszDocName = omInfoName;	
	omCdc.StartDoc( &rlDocInfo );

	imActualPage = 0;
	PrintHeader();
	PrintText();

	omCdc.EndPage();
	omCdc.EndDoc();
	//omBitmap.DeleteObject();
	return true;
}

//-----------------------------------------------------------------------------------------------

void PrintInfo::PrintHeader()
{
	omCdc.StartPage();
	imActualPage++;
	imActualLine = 0;
	CSize olSize;
	CString olTmpText;
	int ilPos = imFirstPos - 10;
	int ilLineHight = imLineHeight;

	int ilHorzRes = omCdc.GetDeviceCaps(HORZRES);
	int ilVertRes = omCdc.GetDeviceCaps(VERTRES);
	omCdc.SelectObject(&omMediumPen);

	//if(imOrientation == PRINTINFO_LANDSCAPE)
	{
		//LOGO=>//omCdc.StretchBlt(ilHorzRes-MMX(385),MMY(ilPos-140),MMX(365),MMY(132),&omMemDc,0,0,365,132, SRCCOPY);
		
		// Print Hader
		//*************
		// Print Frame
		omCdc.MoveTo(MMX(imLeftOffset),MMY(ilPos));
		omCdc.LineTo(ilHorzRes-MMX(20),MMY(ilPos));
		omCdc.LineTo(ilHorzRes-MMX(20),ilVertRes-MMY(50));
		omCdc.LineTo(MMX(imLeftOffset),ilVertRes-MMY(50));
		omCdc.LineTo(MMX(imLeftOffset),MMY(ilPos));

		int ilCol1 = 14;
		int ilCol2 = 1000;	    

		// Print: Tablename
		omCdc.SelectObject(&omArial_Bold_18);
		omCdc.TextOut(MMX(imLeftOffset+ilCol1),MMY(ilPos-80), omInfoName, strlen(omInfoName));

		// Print Info Data
		//*******************
		//############## Beschreibung ##########################################
		omCdc.SelectObject(&omArial_Bold_12);
		ilPos = imFirstPos+10;

		//******* LINE 1 **
		olTmpText = GetString(IDS_STRING1225);//"Flughafen:";
		omCdc.TextOut(MMX(imLeftOffset+ilCol1),MMY(ilPos), olTmpText, strlen(olTmpText));
		olTmpText = GetString(IDS_STRING1226);//"Info vom:";
		omCdc.TextOut(MMX(imLeftOffset+ilCol2),MMY(ilPos), olTmpText, strlen(olTmpText));
		//******* LINE 2 **
		ilPos += ilLineHight;
		olTmpText = GetString(IDS_STRING1227);//"Programm:";
		omCdc.TextOut(MMX(imLeftOffset+ilCol1),MMY(ilPos), olTmpText, strlen(olTmpText));
		olTmpText = GetString(IDS_STRING1228);//"Info von:";
		omCdc.TextOut(MMX(imLeftOffset+ilCol2),MMY(ilPos), olTmpText, strlen(olTmpText));
		//******* LINE 3 **
		ilPos += ilLineHight;
		olTmpText = GetString(IDS_STRING1229);//"Funktionsb.:";
		omCdc.TextOut(MMX(imLeftOffset+ilCol1),MMY(ilPos), olTmpText, strlen(olTmpText));
		//******* LINE 4 **
		ilPos += ilLineHight;
		olTmpText = GetString(IDS_STRING1230);//"Arbeitsb.:";
		omCdc.TextOut(MMX(imLeftOffset+ilCol1),MMY(ilPos), olTmpText, strlen(olTmpText));


		ilPos += ilLineHight;
		omCdc.SelectObject(&omMediumPen);
		omCdc.MoveTo(MMX(imLeftOffset),MMY(ilPos));
		omCdc.LineTo(ilHorzRes-MMX(20),MMY(ilPos));


		//############## Werte ########################################################
		omCdc.SelectObject(&omArial_Regular_12);
		ilPos = imFirstPos+10;
		int ilRight1 = 270;
		int ilRight2 = 200;

		//******* LINE 1 **
		olTmpText = omInfo.Apc3;
		omCdc.TextOut(MMX(imLeftOffset+ilCol1+ilRight1),MMY(ilPos), olTmpText, strlen(olTmpText));
		olTmpText = CString(omInfo.Lstu.Format("%d.%m.%Y/%H:%M"));
		omCdc.TextOut(MMX(imLeftOffset+ilCol2+ilRight2),MMY(ilPos), olTmpText, strlen(olTmpText));
		//******* LINE 2 **
		ilPos += ilLineHight;
		olTmpText = omInfo.Appl;
		omCdc.TextOut(MMX(imLeftOffset+ilCol1+ilRight1),MMY(ilPos), olTmpText, strlen(olTmpText));
		olTmpText = omInfo.Useu;
		omCdc.TextOut(MMX(imLeftOffset+ilCol2+ilRight2),MMY(ilPos), olTmpText, strlen(olTmpText));
		//******* LINE 3 **
		ilPos += ilLineHight;
		olTmpText = omInfo.Funk;
		omCdc.TextOut(MMX(imLeftOffset+ilCol1+ilRight1),MMY(ilPos), olTmpText, strlen(olTmpText));
		//******* LINE 4 **
		ilPos += ilLineHight;
		olTmpText = omInfo.Area;
		omCdc.TextOut(MMX(imLeftOffset+ilCol1+ilRight1),MMY(ilPos), olTmpText, strlen(olTmpText));
	}
	//else
	{
	}
	imActualPos = ilPos;
	PrintFooter();
}

//-----------------------------------------------------------------------------------------------

void PrintInfo::PrintText()
{
	CSize olSize;
	CString olTmpText;
	imActualPos += 100;
	int ilPos = imActualPos;
	int ilLineHight = imLineHeight;

	int ilHorzRes = omCdc.GetDeviceCaps(HORZRES);
	int ilVertRes = omCdc.GetDeviceCaps(VERTRES);

	//if(imOrientation == PRINTINFO_LANDSCAPE)
	{
		int ilCol1 = 14;

		omCdc.SelectObject(&omArial_Regular_10);
		if(omInfoText.GetLength()>0)
		{
			int ilLength = omInfoText.GetLength();
			int ilStrStartPos=0;
			int ilStrPos = 0;

			for(; ilStrPos<ilLength; ilStrPos++)
			{
				if(omInfoText.Mid(ilStrPos,2) == "\r\n")
				{
					if(imActualLine >= imMaxLines)
					{
						omCdc.EndPage();
						PrintHeader();
						ilPos = imActualPos + 95;
						omCdc.SelectObject(&omArial_Regular_10);
					}
					olTmpText = omInfoText.Mid(ilStrStartPos,ilStrPos-ilStrStartPos);
					omCdc.TextOut(MMX(imLeftOffset+ilCol1),MMY(ilPos), olTmpText, strlen(olTmpText));
					imActualLine++;
					ilPos += ilLineHight;
					ilStrStartPos = ilStrPos+2;
				}
			}
			if(ilStrStartPos<ilLength)
			{
				if(imActualLine >= imMaxLines)
				{
					omCdc.EndPage();
					PrintHeader();
					ilPos = imActualPos + 95;
					omCdc.SelectObject(&omArial_Regular_10);
				}
				olTmpText = omInfoText.Mid(ilStrStartPos,ilLength-ilStrStartPos);
				omCdc.TextOut(MMX(imLeftOffset+ilCol1),MMY(ilPos), olTmpText, strlen(olTmpText));
				imActualLine++;
				ilPos += ilLineHight;
			}
		}
	}
	//else
	{
	}
	imActualPos = ilPos-ilLineHight;
}

//-----------------------------------------------------------------------------------------------

void PrintInfo::PrintFooter()
{
	CSize olSize;
	CString olTmpText;
	int ilHorzRes = omCdc.GetDeviceCaps(HORZRES);
	int ilVertRes = omCdc.GetDeviceCaps(VERTRES);

	//if(imOrientation == PRINT_LANDSCAPE)
	{

		omCdc.SelectObject(&omArial_Regular_10);

		// Erstellt von
		olTmpText.Format(GetString(IDS_STRING1223),pcgUser);
		omCdc.TextOut(MMX(imLeftOffset+10),ilVertRes-MMY(40), olTmpText, strlen(olTmpText));

		// Druckdatum
		olTmpText = GetString(IDS_STRING1224) + CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M"));
		olSize = omCdc.GetTextExtent(olTmpText);
		omCdc.TextOut((ilHorzRes/2)-(olSize.cx/2)+MMX(80),ilVertRes-MMY(40), olTmpText, strlen(olTmpText));
		
		// Seitenzahl
		olTmpText.Format(GetString(IDS_STRING1199),imActualPage,dmPages);
		olSize = omCdc.GetTextExtent(olTmpText);
		omCdc.TextOut(ilHorzRes-(MMX(30)+olSize.cx),ilVertRes-MMY(40), olTmpText, strlen(olTmpText));
	}
	//else
	{
	}
}

//-----------------------------------------------------------------------------------------------



