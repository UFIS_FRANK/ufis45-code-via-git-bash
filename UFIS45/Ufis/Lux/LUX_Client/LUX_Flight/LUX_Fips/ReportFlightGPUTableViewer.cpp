// ReportFlightGPUTableViewer.cpp : implementation file
// 
// flights according to GPU-Usage each day


#include <stdafx.h>
#include <ReportFlightGPUTableViewer.h>
#include <CcsGlobl.h>
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include <BasicData.h>
#include <CedaApxData.h>
#include <CedaGpaData.h>
#include <resrc1.h>

#include <ReportXXXCedaData.h>
#include <CedaBasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// ReportFlightGPUTableViewer
//
 


ReportFlightGPUTableViewer::ReportFlightGPUTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, 
												       char *pcpInfo, char *pcpSelect)
{

	pomData = popData;
	pcmInfo = pcpInfo;
	pcmSelect = pcpSelect;
	
	bmIsFromSearch = false;
    pomTable = NULL;
	imFlightCount = 0;


	// load gpa data
	CString olFlightUrnos;
	char buffer[65];

	for(int i = pomData->GetSize() - 1; i >= 0; i --)
	{
		ltoa((*pomData)[i].Urno, buffer, 10);
		olFlightUrnos += CString(buffer) + CString(",");
	}
	if(!olFlightUrnos.IsEmpty())
	{
		olFlightUrnos = olFlightUrnos.Left(olFlightUrnos.GetLength() - 1);
	}
	omGpaData.ReadFlnus(olFlightUrnos); 

}


ReportFlightGPUTableViewer::~ReportFlightGPUTableViewer()
{
	omPrintHeadHeaderArray.DeleteAll();  // BWi
    DeleteAll();
}


void ReportFlightGPUTableViewer::SetParentDlg(CDialog* ppParentDlg)
{

	pomParentDlg = ppParentDlg;

}


void ReportFlightGPUTableViewer::Attach(CCSTable *popTable)
{

    pomTable = popTable;

}


int ReportFlightGPUTableViewer::GetFlightCount()
{

	return imFlightCount;

}


void ReportFlightGPUTableViewer::ChangeViewTo(const char *pcpViewName)
{

    pomTable->ResetContent();
    DeleteAll();    
	//TRACE("ReportFlightGPUTableViewer::ChangeViewTo: Start: %s\n", CTime::GetCurrentTime().Format("%H:%M:%S"));
    MakeLines();
	//TRACE("ReportFlightGPUTableViewer::ChangeViewTo: End: %s\n", CTime::GetCurrentTime().Format("%H:%M:%S"));
   
	UpdateDisplay();
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

}


/////////////////////////////////////////////////////////////////////////////
// ReportFlightGPUTableViewer -- code specific to this class

void ReportFlightGPUTableViewer::MakeLines()
{

	int ilFlightCount = pomData->GetSize();	
	
	
	for (int ilLc = 0; ilLc < ilFlightCount; ilLc++)
	{
 		MakeLine(&(*pomData)[ilLc]);	
	}

 	return;
}

		

void ReportFlightGPUTableViewer::MakeLine(ROTATIONDLGFLIGHTDATA *prpFlight)
{
    REPORTFLIGHTGPUTABLE_LINEDATA rlLine;

	if (strcmp(prpFlight->Des3, pcgHome) == 0) // Arrival
	{
		rlLine.iType = 'A';
		if (MakeLineData(*prpFlight, rlLine))
		{
			CreateLine(rlLine);
			imFlightCount++;
		}
	}

	if (strcmp(prpFlight->Org3, pcgHome) == 0) // Departure
	{
		rlLine.iType = 'D';
		if (MakeLineData(*prpFlight, rlLine))
		{
			CreateLine(rlLine);
			imFlightCount++;
		}
	}

	return;
}




bool ReportFlightGPUTableViewer::MakeLineData(const ROTATIONDLGFLIGHTDATA &rrpFlight, REPORTFLIGHTGPUTABLE_LINEDATA &rpLine)
{

	bool blGpaFind = false;

	// get gpa data
	GPADATA *prlGpaData;
	if ((prlGpaData = omGpaData.GetGpaFlnuAseq(rrpFlight.Urno, 1)) != NULL)
	{
		rpLine.Gpa1Begin = prlGpaData->Gaab;
		rpLine.Gpa1End = prlGpaData->Gaae;

		if(prlGpaData->Gaab != TIMENULL || prlGpaData->Gaae != TIMENULL)	
			blGpaFind = true;
	}
	if ((prlGpaData = omGpaData.GetGpaFlnuAseq(rrpFlight.Urno, 2)) != NULL)
	{
		rpLine.Gpa2Begin = prlGpaData->Gaab;
		rpLine.Gpa2End = prlGpaData->Gaae;
		if(prlGpaData->Gaab != TIMENULL || prlGpaData->Gaae != TIMENULL)	
			blGpaFind = true;
	}
	if ((prlGpaData = omGpaData.GetGpaFlnuAseq(rrpFlight.Urno, 3)) != NULL)
	{
		rpLine.Gpa3Begin = prlGpaData->Gaab;
		rpLine.Gpa3End = prlGpaData->Gaae;
		if(prlGpaData->Gaab != TIMENULL || prlGpaData->Gaae != TIMENULL)	
			blGpaFind = true;
	}

	// Wenn keine GPU-Daten vorhanden sind: Zeile nicht darstellen
	if (!blGpaFind) return false;
	
	rpLine.Flno = rrpFlight.Flno;
	rpLine.Act = rrpFlight.Act3;
	rpLine.Regn = rrpFlight.Regn;

	if (rpLine.iType == 'A')
	{	
		rpLine.Type = "Arrival";
		rpLine.Time = rrpFlight.Stoa;
		rpLine.BestTime = rrpFlight.Land;
		rpLine.Pos = rrpFlight.Psta;
	}
	else if (rpLine.iType == 'D')
	{
		rpLine.Type = "Departure";
		rpLine.Time = rrpFlight.Stod;
		rpLine.BestTime = rrpFlight.Airb;
		rpLine.Pos = rrpFlight.Pstd;
	}
	else
		return false;
	
	if (bgReportLocal)
	{
		ogBasicData.UtcToLocal(rpLine.Time);
		ogBasicData.UtcToLocal(rpLine.BestTime);
	}  
  
    return true;

}



int ReportFlightGPUTableViewer::CreateLine(REPORTFLIGHTGPUTABLE_LINEDATA &rpLine)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareFlight(&rpLine, &omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]

    omLines.NewAt(ilLineno, rpLine);
	return ilLineno;
}


void ReportFlightGPUTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}



void ReportFlightGPUTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}


/////////////////////////////////////////////////////////////////////////////
// ReportFlightGPUTableViewer - display drawing routine



// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void ReportFlightGPUTableViewer::UpdateDisplay()
{
	pomTable->ResetContent();

	DrawHeader();

	REPORTFLIGHTGPUTABLE_LINEDATA *prlLine;
    
	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		int ilColumnNo = 0;
		prlLine = &omLines[ilLc];
		CCSPtrArray<TABLE_COLUMN> olColList;
	
		MakeColList(*prlLine, olColList);

		pomTable->AddTextLine(olColList, (void*)(prlLine));
		olColList.DeleteAll();
	}


    pomTable->DisplayTable();
}




void ReportFlightGPUTableViewer::DrawHeader()
{
	int  ilTotalLines = 0;
	bool blNewLogicLine = false;
	float flCharPixelFactor = 40 / (float) 5;  // 5 Zeichen <--> 40 Pixel

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);
	pomTable->ResetContent();
	
	TABLE_HEADER_COLUMN rlHeader;

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Font = &ogCourier_Bold_10;

	rlHeader.Length = static_cast<int>(9 * flCharPixelFactor); 
	rlHeader.Text = GetString(IDS_STRING337);// Type
	omHeaderDataArray.New(rlHeader);

	rlHeader.Length = static_cast<int>(8 * flCharPixelFactor); 
	rlHeader.Text = GetString(IDS_STRING1078);// Flight
	omHeaderDataArray.New(rlHeader);

	rlHeader.Length = static_cast<int>(8 * flCharPixelFactor); 
 	rlHeader.Text = GetString(IDS_STRING332);// Date
	omHeaderDataArray.New(rlHeader);

 	rlHeader.Length = static_cast<int>(5 * flCharPixelFactor); 
 	rlHeader.Text = GetString(IDS_STRING1881);// STA/STD
	omHeaderDataArray.New(rlHeader);

 	rlHeader.Length = static_cast<int>(5 * flCharPixelFactor); 
 	rlHeader.Text = GetString(IDS_STRING1882);// ATA/ATD
	omHeaderDataArray.New(rlHeader);

 	rlHeader.Length = static_cast<int>(3 * flCharPixelFactor); 
 	rlHeader.Text = GetString(IDS_STRING311);// A/C
	omHeaderDataArray.New(rlHeader);

 	rlHeader.Length = static_cast<int>(12 * flCharPixelFactor); 
 	rlHeader.Text = GetString(IDS_STRING310);// REG
	omHeaderDataArray.New(rlHeader);

  	rlHeader.Length = static_cast<int>(5 * flCharPixelFactor);
	rlHeader.Text = GetString(IDS_STRING308);// POS
 	omHeaderDataArray.New(rlHeader);

	rlHeader.Length = static_cast<int>(13 * flCharPixelFactor); 
 	rlHeader.Text = GetString(IDS_STRING1883);// GPU 1
	omHeaderDataArray.New(rlHeader);

	rlHeader.Length = static_cast<int>(13 * flCharPixelFactor); 
 	rlHeader.Text = GetString(IDS_STRING1884);// GPU 2
	omHeaderDataArray.New(rlHeader);

	rlHeader.Length = static_cast<int>(13 * flCharPixelFactor); 
 	rlHeader.Text = GetString(IDS_STRING1885);// GPU 3
	omHeaderDataArray.New(rlHeader);

  
	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();

}



void ReportFlightGPUTableViewer::MakeColList(const REPORTFLIGHTGPUTABLE_LINEDATA &rrlLine, CCSPtrArray<TABLE_COLUMN> &olColList)
{

	TABLE_COLUMN rlColumnData;

	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogCourier_Regular_9;

	rlColumnData.Alignment = COLALIGN_LEFT;
 	rlColumnData.Text = rrlLine.Type;
 	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
 	rlColumnData.Text = rrlLine.Flno;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
 	rlColumnData.Text = rrlLine.Time.Format("%d.%m.%y");
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
 	rlColumnData.Text = rrlLine.Time.Format("%H:%M");
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrlLine.BestTime.Format("%H:%M");
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
 	rlColumnData.Text = rrlLine.Act;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
 	rlColumnData.Text = rrlLine.Regn;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
 	rlColumnData.Text = rrlLine.Pos;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
 	rlColumnData.Text = rrlLine.Gpa1Begin.Format("%H:%M") + " - " + rrlLine.Gpa1End.Format("%H:%M");
 	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
 	rlColumnData.Text = rrlLine.Gpa2Begin.Format("%H:%M") + " - " + rrlLine.Gpa2End.Format("%H:%M");
 	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
 	rlColumnData.Text = rrlLine.Gpa3Begin.Format("%H:%M") + " - " + rrlLine.Gpa3End.Format("%H:%M");
 	olColList.NewAt(olColList.GetSize(), rlColumnData);

  
}





int ReportFlightGPUTableViewer::CompareFlight(REPORTFLIGHTGPUTABLE_LINEDATA *prpLine1, REPORTFLIGHTGPUTABLE_LINEDATA *prpLine2)
{
 
	// Sortierung nach Type, Date, STA/STD
	if (prpLine1->iType == prpLine2->iType)
	{
		return (prpLine1->Time == prpLine2->Time) ? 0:
			(prpLine1->Time > prpLine2->Time) ? 1 : -1;
	}
	
	if (prpLine1->iType == 'A')
	{
		return -1;
	}
	else
	{
		return 1;
	}
}



//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------


void ReportFlightGPUTableViewer::GetHeader()
{

	float flCharPixelFactor = 40 / (float) 5;  // 5 Zeichen <--> 40 Pixel
	
	omPrintHeadHeaderArray.DeleteAll();  // BWi

	TABLE_HEADER_COLUMN rlHeader;

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Font = &ogCourier_Bold_10;

	rlHeader.Length = static_cast<int>(9 * flCharPixelFactor); 
	rlHeader.Text = GetString(IDS_STRING337);// Type
	omPrintHeadHeaderArray.New(rlHeader);

	rlHeader.Length = static_cast<int>(8 * flCharPixelFactor); 
	rlHeader.Text = GetString(IDS_STRING1078);// Flight
	omPrintHeadHeaderArray.New(rlHeader);

	rlHeader.Length = static_cast<int>(8 * flCharPixelFactor); 
 	rlHeader.Text = GetString(IDS_STRING332);// Date
	omPrintHeadHeaderArray.New(rlHeader);

 	rlHeader.Length = static_cast<int>(7 * flCharPixelFactor); 
 	rlHeader.Text = GetString(IDS_STRING1881);// STA/STD
	omPrintHeadHeaderArray.New(rlHeader);

 	rlHeader.Length = static_cast<int>(7 * flCharPixelFactor); 
 	rlHeader.Text = GetString(IDS_STRING1882);// ATA/ATD
	omPrintHeadHeaderArray.New(rlHeader);

 	rlHeader.Length = static_cast<int>(3 * flCharPixelFactor); 
 	rlHeader.Text = GetString(IDS_STRING311);// A/C
	omPrintHeadHeaderArray.New(rlHeader);

 	rlHeader.Length = static_cast<int>(12 * flCharPixelFactor); 
 	rlHeader.Text = GetString(IDS_STRING310);// REG
	omPrintHeadHeaderArray.New(rlHeader);

  	rlHeader.Length = static_cast<int>(5 * flCharPixelFactor);
	rlHeader.Text = GetString(IDS_STRING308);// POS
 	omPrintHeadHeaderArray.New(rlHeader);

	rlHeader.Length = static_cast<int>(13 * flCharPixelFactor); 
 	rlHeader.Text = GetString(IDS_STRING1883);// GPU 1
	omPrintHeadHeaderArray.New(rlHeader);

	rlHeader.Length = static_cast<int>(13 * flCharPixelFactor); 
 	rlHeader.Text = GetString(IDS_STRING1884);// GPU 2
	omPrintHeadHeaderArray.New(rlHeader);

	rlHeader.Length = static_cast<int>(13 * flCharPixelFactor); 
 	rlHeader.Text = GetString(IDS_STRING1885);// GPU 3
	omPrintHeadHeaderArray.New(rlHeader);
 
}




void ReportFlightGPUTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	char pclFooter[200];
	CString olTableName;

	if (strlen(pcmSelect) == 0)
	{
		sprintf(pclFooter, GetString(IDS_STRING1879));
		olTableName = GetString(IDS_STRING1879);
	}
	else
	{
		sprintf(pclFooter, GetString(IDS_STRING1886), pcmSelect);
		olTableName.Format(GetString(IDS_STRING1886), pcmSelect);
	}

	omFooterName = pclFooter;

	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET
	pomPrint = new CCSPrint(pomParentDlg,ilOrientation,45);

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = olTableName;	
			//rlDocInfo.lpszDocName = TABLENAME;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = imFlightCount;//omLines.GetSize();  
			olFooter1.Format(GetString(IDS_STRING1445), ilLines, omFooterName);

			GetHeader();

			if (ilLines == 0)
				PrintTableHeader(); // Drucken leerer Tabelle
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}

		omPrintHeadHeaderArray.DeleteAll();
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		delete pomPrint;
		pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool ReportFlightGPUTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;
	double dgCCSPrintFactor = 2.7 ;
	char pclHeader[200];

	if (strlen(pcmSelect) == 0)
		sprintf(pclHeader, GetString(IDS_STRING1880), pcmInfo, imFlightCount);
	else
		sprintf(pclHeader, GetString(IDS_STRING1887), pcmSelect, pcmInfo, imFlightCount);
	
	CString olTableName(pclHeader);

	pomPrint->PrintUIFHeader(omTableName,CString(pclHeader),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omPrintHeadHeaderArray.GetSize();

	for(int i=0;i<ilSize;i++)
	{
		rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Text   = omPrintHeadHeaderArray[i].Text;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool ReportFlightGPUTableViewer::PrintTableLine(REPORTFLIGHTGPUTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	double dgCCSPrintFactor = 2.7 ;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omPrintHeadHeaderArray.GetSize();

	for(int i=0;i<ilSize;i++)
	{
		rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameTop   = PRINT_FRAMETHIN;
		if(bpLastLine == true)
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN;
		}
		else
		{
			rlElement.FrameBottom = PRINT_NOFRAME;
		}
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;


		switch(i)
		{
			case 0:
			{
 				rlElement.Text = prpLine->Type;
			}
			break;
			case 1:
			{
 				rlElement.Text = prpLine->Flno;
			}
			break;
			case 2:
			{
				rlElement.Text = prpLine->Time.Format("%d.%m.%y");
			}
			break;
			case 3:
			{
 				rlElement.Text = prpLine->Time.Format("%H:%M");
			}
			break;
			case 4:
			{
 				rlElement.Text = prpLine->BestTime.Format("%H:%M");
			}
			break;
			case 5:
			{
 				rlElement.Text = prpLine->Act;
			}
			break;
			case 6:
			{
 				rlElement.Text = prpLine->Regn;
			}
			break;
			case 7:
			{
 				rlElement.Text = prpLine->Pos;
			}
			break;
			case 8:
			{
 				rlElement.Text = prpLine->Gpa1Begin.Format("%H:%M") + " - " + prpLine->Gpa1End.Format("%H:%M");;
			}
			break;
			case 9:
			{
				rlElement.Text = prpLine->Gpa2Begin.Format("%H:%M") + " - " + prpLine->Gpa2End.Format("%H:%M");;
			}
			break;
			case 10:
			{
				rlElement.Text = prpLine->Gpa3Begin.Format("%H:%M") + " - " + prpLine->Gpa3End.Format("%H:%M");;
			}
			break;
		}

		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------


// Drucken in Datei
void ReportFlightGPUTableViewer::PrintPlanToFile(char *pcpDefPath)
{

	CCS_TRY

	char pclHeadlineSelect[200];
	if (strlen(pcmSelect) == 0)
		sprintf(pclHeadlineSelect, GetString(IDS_STRING1880), pcmInfo, imFlightCount);
	else
		sprintf(pclHeadlineSelect, GetString(IDS_STRING1887), pcmSelect, pcmInfo, imFlightCount);

	ofstream of;
	of.open( pcpDefPath, ios::out);
	of << CString(pclHeadlineSelect) << " "
	   << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M") << endl << endl;

 
 	of << GetString(IDS_STRING337) << "      "<< GetString(IDS_STRING1078) << "   " 
	   << GetString(IDS_STRING332)  << "     "  << GetString(IDS_STRING1881) << " " 
	   << GetString(IDS_STRING1882) << " " << GetString(IDS_STRING311) << " "
	   << GetString(IDS_STRING310)  << "          " << GetString(IDS_STRING308) << "   "
	   << GetString(IDS_STRING1883) << "       " << GetString(IDS_STRING1884) << "       "
	   << GetString(IDS_STRING1885) << endl;
	of << "------------------------------------------------------------------------------------------------------" << endl;
  
	CStringArray olDaten;
 
	for(int i = 0; i < imFlightCount; i++)
	{
		REPORTFLIGHTGPUTABLE_LINEDATA rlD = omLines[i];

		of.setf(ios::left, ios::adjustfield);
		of   << setw(10) << rlD.Type 
			 << setw(9) << rlD.Flno 
			 << setw(9) << rlD.Time.Format("%d.%m.%y")
			 << setw(8) << rlD.Time.Format("%H:%M")
			 << setw(8) << rlD.BestTime.Format("%H:%M")
			 << setw(4) << rlD.Act 
			 << setw(13) << rlD.Regn 
			 << setw(6) << rlD.Pos 
			 << setw(12) << rlD.Gpa1Begin.Format("%H:%M") + "-" + rlD.Gpa1End.Format("%H:%M")
			 << setw(12) << rlD.Gpa2Begin.Format("%H:%M") + "-" + rlD.Gpa2End.Format("%H:%M")
			 << setw(12) << rlD.Gpa3Begin.Format("%H:%M") + "-" + rlD.Gpa3End.Format("%H:%M");
 		of << endl;
		
	}
	of.close();

	CCS_CATCH_ALL

}
