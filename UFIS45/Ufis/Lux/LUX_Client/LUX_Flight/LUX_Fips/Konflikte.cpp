// Konflikte.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <fpms.h>
#include <Konflikte.h>
#include <ButtonListDlg.h>
#include <CCSGlobl.h>
#include <resrc1.h>
#include <SpotAllocation.h>
#include <BltDiagram.h>
#include <Utils.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld Konflikte 

static int CompareByRkey(const DIAFLIGHTDATA **e1, const DIAFLIGHTDATA **e2)
{
	if((**e1).Rkey != (**e2).Rkey)
	{
		return (((**e1).Rkey == (**e2).Rkey) ? 0 : (((**e1).Rkey > (**e2).Rkey) ? 1 : -1));
	}
	else
	{
		int ilRet;

		if(strcmp((**e2).Adid, "B") == 0 && strcmp((**e1).Adid, "B") == 0)
		{
			CTime olTime1;
			CTime olTime2;
				
			if((**e1).Tifa == TIMENULL)
				olTime1 = (**e1).Tifd;
			else
				olTime1 = (**e1).Tifa;

			if((**e2).Tifa == TIMENULL)
				olTime2 = (**e2).Tifd;
			else
				olTime2 = (**e2).Tifa;

			ilRet =  ((olTime1 == olTime2) ? 0 : ((olTime1 > olTime2) ? 1 : -1));
		}
		else
		{
			ilRet = strcmp((**e1).Adid, (**e2).Adid);

		}

		return ilRet;
	}
}

static int Time(const KONFDATA **e1, const KONFDATA **e2)
{
		return ((**e1).TimeOfConflict == (**e2).TimeOfConflict)? 0: 
				((**e1).TimeOfConflict >  (**e2).TimeOfConflict)? 1: -1;
}


static int CompareRotationFlight(const DIAFLIGHTDATA **e1, const DIAFLIGHTDATA **e2)
{
	CTime olTime1;
	CTime olTime2;
	int ilRet = 0;


	if(strcmp((**e2).Adid, "B") == 0 && strcmp((**e1).Adid, "B") == 0)
	{

		if((**e1).Stoa == TIMENULL)
			olTime1 = (**e1).Stod;
		else
			olTime1 = (**e1).Stoa;

		if((**e2).Stoa == TIMENULL)
			olTime2 = (**e2).Stod;
		else
			olTime2 = (**e2).Stoa;

		ilRet =  ((olTime1 == olTime2) ? 0 : ((olTime1 > olTime2) ? 1 : -1));
	}
	else
	{
		ilRet = strcmp((**e1).Adid, (**e2).Adid);

	}


	return ilRet;
}







#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))

Konflikte::Konflikte()
{

	omNoOnblAfterLand	= CTimeSpan(0,0,15,0);
	omCurrEtai			= CTimeSpan(0,0,5,0);
	omCurrDNxti = CTimeSpan(0,0,-5,0);
	omCurrANxti = CTimeSpan(0,0,-5,0);
	omStoaStod = CTimeSpan(0,0,5,0);
	omStodCurrAndNoAirb = CTimeSpan(0,0,5,0);
	omCurrOfbl = CTimeSpan(0,0,15,0);
	omCurrOfblStod = CTimeSpan(0,0,5,0);
	omStodEtdi = CTimeSpan(0,0,15,0);
	omStoaEtai = CTimeSpan(0,0,15,0);
	omCurrStodGd1x = CTimeSpan(0,0,30,0);


	bmNoOnblAfterLand = true;
	bmCurrEtai = true;
	bmCurrANxti = true;
	bmCurrDNxti = true;
	bmStoaStod = true;
	bmStodCurrAndNoAirb = true;
	bmCurrOfbl = true;
	bmCurrOfblStod = true;
	bmStodEtdi = true;
	bmCurrStodGd1x = true;

	bmJoinConf = true;
	bmAderConf = true;
	bmActRegnChangeConf = true;

	bmPstConf = true;
	bmGatConf = true;
	bmWroConf = true;
	bmBltConf = true;

	bmStoaEtai = true;
	bmFlightDataChanged = true;
	bmPosGateRelation = true;

	imBltConflicts = 0;

	bmAttentionButtonUpdate = true;
}


void Konflikte::SetConfiguration()
{

	CString olList = ogCfgData.GetConflictSetup();

	CStringArray olConf;

	/* Conflict No. 1		Start-landing order!  */
	/* Conflict No. 2	15	No Onblock at Touch down + %d  Minutes */
	/* Conflict No. 3	5	Actual Time > ETA + %d Minutes  */
	/* Conflict No. 4	-5	Actual Time  + %d Min > Next Information Time  */
	/* Conflict No. 5		Pos: %s  A/C: %s  error (%s) from parking system !  */
	/* Conflict No. 6		STA/ETA    >=    STD/ETD  */
	/* Conflict No. 7		STD/ETD + 30 Min. < actual time and no DEP telegram received  */
	/* Conflict No. 8		Aircrafttype has been changed */
	/* Conflict No. 9	-5	No Take off time at Offblock + %d Minutes */
	/* Conflict No. 10	5	No Offblock at STD + %d Minutes */
	/* Conflict No. 11	15	ETD > STD + %d Minutes  */
	/* Conflict No. 12	-5	Actual Time + %d Min > Next Information Time  */
	/* Conflict No. 13	30	STD/ETD - 30 Min. < actual time and no boarding information received */
	/* Conflict No. 14	15	ETA > STA + %d Minutes  */

	if( !olList.IsEmpty() )
	{
		ExtractItemList(olList, &olConf, ';');

		if( olConf.GetSize() >= 31)
		{

			if( olConf[0] == "0")
				bmJoinConf = false;
			else
				bmJoinConf = true;

			if( olConf[2] == "0")
				bmNoOnblAfterLand = false;
			else
				bmNoOnblAfterLand = true;
			if( olConf[3] != "-")
				omNoOnblAfterLand	= CTimeSpan(0,0,atoi(olConf[3]),0);

			if( olConf[4] == "0")
				bmCurrEtai = false;
			else
				bmCurrEtai = true;
			if( olConf[5] != "-")
				omCurrEtai			= CTimeSpan(0,0,atoi(olConf[5]),0);

			if( olConf[6] == "0")
				bmCurrANxti = false;
			else
				bmCurrANxti = true;
			if( olConf[7] != "-")
				omCurrANxti			= CTimeSpan(0,0,atoi(olConf[7]),0);

			if( olConf[8] == "0")
				bmAderConf = false;
			else 
				bmAderConf = true;

			if( olConf[10] == "0")
				bmStoaStod = false;
			else
				bmStoaStod = true;
			if( olConf[11] != "-")
				omStoaStod			= CTimeSpan(0,0,atoi(olConf[11]),0);

			if( olConf[12] == "0")
				bmStodCurrAndNoAirb = false;
			else
				bmStodCurrAndNoAirb = true;
			if( olConf[13] != "-")
				omStodCurrAndNoAirb			= CTimeSpan(0,0,atoi(olConf[13]),0);

			if( olConf[14] == "0")
				bmActRegnChangeConf = false;
			else
				bmActRegnChangeConf = true;

			if( olConf[16] == "0")
				bmCurrOfbl = false;
			else
				bmCurrOfbl = true;
			if( olConf[17] != "-")
				omCurrOfbl			= CTimeSpan(0,0,atoi(olConf[17]),0);

			if( olConf[18] == "0")
				bmCurrOfblStod = false;
			else
				bmCurrOfblStod = true;
			if( olConf[19] != "-")
				omCurrOfblStod			= CTimeSpan(0,0,atoi(olConf[19]),0);


			if( olConf[20] == "0")
				bmStodEtdi = false;
			else
				bmStodEtdi = true;
			if( olConf[21] != "-")
				omStodEtdi			= CTimeSpan(0,0,atoi(olConf[21]),0);

			if( olConf[22] == "0")
				bmCurrDNxti = false;
			else
				bmCurrDNxti = true;
			if( olConf[23] != "-")
				omCurrDNxti			= CTimeSpan(0,0,atoi(olConf[23]),0);


			if( olConf[24] == "0")
				bmCurrStodGd1x = false;
			else
				bmCurrStodGd1x = true;
			if( olConf[25] != "-")
				omCurrStodGd1x			= CTimeSpan(0,0,atoi(olConf[25]),0);


			if( olConf[26] == "0")
				bmPstConf = false;
			else
				bmPstConf = true;

			if( olConf[28] == "0")
				bmPstConf = false;
			else
				bmPstConf = true;

			if( olConf[30] == "0")
				bmPstConf = false;
			else
				bmPstConf = true;

			if( olConf[32] == "0")
				bmPstConf = false;
			else
				bmPstConf = true;
		}

		if( olConf.GetSize() >= 36)
		{
			if( olConf[34] == "0")
				bmStoaEtai = false;
			else
				bmStoaEtai = true;
			if( olConf[35] != "-")
				omStoaEtai			= CTimeSpan(0,0,atoi(olConf[35]),0);
		}
		if( olConf.GetSize() >= 37)
		{
			if( olConf[36] == "0")
				bmFlightDataChanged = false;
			else
				bmFlightDataChanged = true;
		}
		if( olConf.GetSize() >= 39)
		{
			if( olConf[38] == "0")
				bmPosGateRelation = false;
			else
				bmPosGateRelation = true;
		}

		//rkr20042001
		// this part is only to perform the values to next version
		// the archivedays now stored in own records.
		// if conflicts must be added you can remove this part
		if( olConf.GetSize() >= 42)
			ogTimeSpanPostFlight = CTimeSpan(atoi(olConf[41]),0,0,0);
		else
			ogTimeSpanPostFlight = CTimeSpan(0,0,0,0);

		if( olConf.GetSize() >= 43)
			ogTimeSpanPostFlightInFuture = CTimeSpan(atoi(olConf[42]),0,0,0);
		else
			ogTimeSpanPostFlightInFuture = CTimeSpan(0,0,0,0);
		// if the conflicts must be added you can remove this part
		//rkr20042001



	}

}





Konflikte::~Konflikte()
{

	ClearAll();

}


void Konflikte::ClearAll()
{
	omUrnoMap.RemoveAll();
	omNotConfirmedMap.RemoveAll();
	imBltConflicts = 0;

	KONFENTRY *prlEntry = NULL;

	for(int i = omData.GetSize() - 1; i >= 0; i--)
	{
		omData[i].Data.DeleteAll();
	}
	omData.DeleteAll();


}


void Konflikte::RemoveAll(DWORD dwOrigin)
{
	KONFENTRY *prlEntry = NULL;
	KONFDATA  *prlData = NULL;
	KonfIdent rlIdNotify;

	for(int j = omData.GetSize() - 1; j >= 0; j--)
	{
		prlEntry = &omData[j];

		for(int i = prlEntry->Data.GetSize() - 1; i >= 0; i--)
		{
			prlData = &prlEntry->Data[i];

			if(prlData->SetBy & dwOrigin)
			{
				prlData->SetBy = prlData->SetBy & ~dwOrigin;
				if (omNotConfirmedMap.RemoveKey((void*) prlData) != 0)
				{
					if (IsBltKonfliktType(prlData->KonfId.Type))
					{
						imBltConflicts--;
					}
				}
				rlIdNotify = prlData->KonfId;
				ogDdx.DataChanged((void *)this, KONF_DELETE,(void *)&rlIdNotify );

				prlEntry->Data.DeleteAt(i);
			}
		}
		if (prlEntry->Data.GetSize() == 0)
		{
			omUrnoMap.RemoveKey((void*) prlEntry->Urno);
			omData.DeleteAt(j);
		}

	}



	/*
	for(int i = omData.GetSize() - 1; i >= 0; i--)
	{
		prlEntry = &omData[i];

		for(int i = prlEntry->Data.GetSize() - 1; i >= 0; i--)
		{
			prlData = &prlEntry->Data[i];
			prlData->SetBy = prlData->SetBy & ~dwOrigin;

			if(prlData->SetBy == 0)
			{
				if (omNotConfirmedMap.RemoveKey((void*) prlData) != 0)
				{
					if (IsBltKonfliktType(prlData->Type))
					{
						imBltConflicts--;
					}
				}

			}
		}
	}
	*/

	CheckAttentionButton();
}


void Konflikte::RemoveAllForFlight(long lpUrno, DWORD dwOrigin)
{
	KONFENTRY *prlEntry = NULL;

	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlEntry) == TRUE)
	{
		KONFDATA  *prlData = NULL;
		KonfIdent rlIdNotify;
		for(int i = prlEntry->Data.GetSize() - 1; i >= 0; i--)
		{
			prlData = &prlEntry->Data[i];
			if(prlData->SetBy & dwOrigin)
			{
				prlData->SetBy = prlData->SetBy & ~dwOrigin;
				if (omNotConfirmedMap.RemoveKey((void*) prlData) != 0)
				{
					if (IsBltKonfliktType(prlData->KonfId.Type))
					{
						imBltConflicts--;
					}
				}
				rlIdNotify = prlData->KonfId;
				ogDdx.DataChanged((void *)this, KONF_DELETE,(void *)&rlIdNotify );

				prlEntry->Data.DeleteAt(i);
			}
		}
		if (prlEntry->Data.GetSize() == 0)
		{
			omUrnoMap.RemoveKey((void*) lpUrno);
			for(int i = omData.GetSize() - 1; i >= 0; i--)
			{
				if(omData[i].Urno == lpUrno)
				{
					omData.DeleteAt(i);
					break;
				}

			}
		}
	}


	CheckAttentionButton();
}




void Konflikte::CleanUp()
{
	CTime olTime = CTime::GetCurrentTime();

	ogBasicData.LocalToUtc(olTime);

	CTimeSpan olSpan(0,3,0,0);

	olTime -= olSpan;

	KONFENTRY *prlEntry = NULL;
	KONFDATA  *prlData = NULL;

	KonfIdent rlIdNotify;

	for(int i = omData.GetSize() - 1; i >= 0; i--)
	{
		prlEntry = &omData[i];

		for(int i = prlEntry->Data.GetSize() - 1; i >= 0; i--)
		{
			if((prlEntry->Data[i].SetBy == 0) && (prlEntry->Data[i].TimeOfConflict < olTime) && (prlEntry->Data[i].Confirmed == true))
			{
				rlIdNotify = prlEntry->Data[i].KonfId;

				ogDdx.DataChanged((void *)this, KONF_DELETE,(void *)&rlIdNotify );

				prlEntry->Data.DeleteAt(i);
			}
		}
		if(prlEntry->Data.GetSize() == 0)
		{
			omUrnoMap.RemoveKey((void *)prlEntry->Urno);

			for(int i = omData.GetSize() - 1; i >= 0; i--)
			{
				if(omData[i].Urno == prlEntry->Urno)
				{
					omData.DeleteAt(i);
					break;
				}

			}

		}
	}
	CheckAttentionButton();
}




KONFDATA *Konflikte::GetKonflikt(const KonfIdent &ropKonfId)
{
	KONFENTRY *prlEntry = NULL;
	KONFDATA  *prlData = NULL;

	if (omUrnoMap.Lookup((void *)ropKonfId.FUrno, (void *& )prlEntry) == TRUE)
	{
		for(int i = prlEntry->Data.GetSize() - 1; i >= 0; i--)
		{
			if(prlEntry->Data[i].KonfId == ropKonfId)
			{
				return &prlEntry->Data[i];
		}
	}
	}
	return prlData;
}




int Konflikte::GetStatus(long lpFUrno, char cpFPart, DWORD dwOrigin, int ipSubOrig)
{
	KONFENTRY *prlEntry = NULL;
	KONFDATA  *prlData = NULL;
	int ilCount = 0;
	int ilConfirmedCount = 0;

	if (omUrnoMap.Lookup((void *)lpFUrno,(void *& )prlEntry) == TRUE)
	{
		for(int i = prlEntry->Data.GetSize() - 1; i >= 0; i--)
		{
			prlData = &prlEntry->Data[i];
			if (prlData->KonfId.FPart != cpFPart)
				continue;

			if(prlData->SetBy & dwOrigin)
			{
				if(prlData->SubId & ipSubOrig || ipSubOrig == SUB_MOD_ALL || prlData->SubId == SUB_MOD_ALL)
				{
					ilCount++;
					if(prlData->Confirmed)
					{
						ilConfirmedCount++;
					}
					else
					{
						return 1;
				}
			}
		}
	}
	}

	if(ilCount == 0)
	{
		return -1;
	}
	else
	{
		if(ilCount == ilConfirmedCount)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
}



void Konflikte::GetKonflikte(long lpFUrno, char cpFPart, DWORD dwOrigin, int ipSubOrig, CCSPtrArray<KonfItem> &opKonfList)
{
	
	KonfItem *prlKonf;
	KONFENTRY *prlEntry = NULL;
	KONFDATA  *prlData = NULL;
	int ilCount = 0;
	int ilConfirmedCount = 0;

	if (omUrnoMap.Lookup((void *)lpFUrno,(void *& )prlEntry) == TRUE)
	{
		for(int i = prlEntry->Data.GetSize() - 1; i >= 0; i--)
		{
			prlData = &prlEntry->Data[i];
			if (prlData->KonfId.FPart != cpFPart)
				continue;

			if(prlData->SetBy & dwOrigin)
			{
				if(prlData->SubId & ipSubOrig || ipSubOrig == SUB_MOD_ALL || prlData->SubId == SUB_MOD_ALL)
				{
					
					prlKonf = new KonfItem;

					prlKonf->Text = prlData->Text;

					prlKonf->KonfId = prlData->KonfId;
					
					opKonfList.Add(prlKonf);

				}
			}
		}
	}

}



void Konflikte::ResetFlight(long lpFUrno, char cpFPart, DWORD dwOrigin )
{
	KONFENTRY *prlEntry = NULL;
	KONFDATA  *prlData = NULL;

	if (omUrnoMap.Lookup((void *)lpFUrno,(void *& )prlEntry) == TRUE)
	{
		for(int i = prlEntry->Data.GetSize() - 1; i >= 0; i--)
		{
			if (prlEntry->Data[i].KonfId.FPart != cpFPart)
				continue;

			if(prlEntry->Data[i].SetBy  & dwOrigin)
			{
				prlEntry->Data[i].Flag = false;
			}
		}
	}
}



void Konflikte::ResetAllNeighborConfFlight(long lpFUrno, char cpFPart)
{
	KONFENTRY *prlEntry = NULL;
	KONFDATA  *prlData = NULL;

	if (omUrnoMap.Lookup((void *)lpFUrno,(void *& )prlEntry) == TRUE)
	{
		for(int i = prlEntry->Data.GetSize() - 1; i >= 0; i--)
		{
			if (prlEntry->Data[i].KonfId.FPart != cpFPart)
				continue;

			if(prlEntry->Data[i].SetBy  & MOD_ID_DIA)
			{
				if (IsNeighborKonfliktType(prlEntry->Data[i].KonfId.Type))
					prlEntry->Data[i].Flag = false;
			}
		}
	}
}



void Konflikte::CheckFlightConf(long lpFUrno, char cpFPart, DWORD dwOrigin)
{
	KONFENTRY *prlEntry = NULL;

	if (omUrnoMap.Lookup((void *)lpFUrno,(void *& )prlEntry) == TRUE)
	{
		KONFDATA  *prlData = NULL;

		KonfIdent rlIdNotify;

		prlEntry->Data.Sort(Time);
		for(int i = 0; i < prlEntry->Data.GetSize(); i++)
		{
			prlData = &prlEntry->Data[i];

			if (prlData->KonfId.FPart != cpFPart)
				continue;

			if(prlData->Flag == false)
			{
				prlData->SetBy = prlData->SetBy & ~dwOrigin;

				if(prlData->SetBy == 0)
				{
					if (omNotConfirmedMap.RemoveKey((void*) prlData) != 0)
					{
						if (IsBltKonfliktType(prlData->KonfId.Type))
						{
							imBltConflicts--;
						}
					}

					rlIdNotify = prlData->KonfId;
					ogDdx.DataChanged((void *)this, KONF_DELETE,(void *)&rlIdNotify );

					prlEntry->Data.DeleteAt(i);
				}
			}
		}
		if(prlEntry->Data.GetSize() == 0)
		{
			omUrnoMap.RemoveKey((void *)lpFUrno);

			for(int i = omData.GetSize() - 1; i >= 0; i--)
			{
				if(omData[i].Urno == lpFUrno)
				{
					omData.DeleteAt(i);
					break;
				}

			}

		}
	}
}


void Konflikte::DisableAttentionButtonsUpdate()
{
	bmAttentionButtonUpdate = false;
}


void Konflikte::EnableAttentionButtonsUpdate()
{
	bmAttentionButtonUpdate = true;
	CheckAttentionButton();
}



void Konflikte::CheckAttentionButton() 
{
	if (!bmAttentionButtonUpdate || bgPosDiaAutoAllocateInWork)
		return;

	if(pogButtonList != NULL)
	{
		if(IsWindow(pogButtonList->m_hWnd) )
		{
//rkr
			int ilCount =	omNotConfirmedMap.GetCount() - imBltConflicts;

		 	CString olText = GetString(IMFK_ATTENTION);
 
			if(ilCount > 0)
			{
 				olText.Format("%s (%d)", GetString(IMFK_ATTENTION), ilCount);
			}
			if(ilCount > 0)
				pogButtonList->m_CB_Achtung.SetColors(RGB(255, 0, 0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			else
				pogButtonList->m_CB_Achtung.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
 
			pogButtonList->m_CB_Achtung.SetWindowText(olText);
		}//end if
	}//end if


	// Update Blt-Attention-Button
	if(pogBltDiagram != NULL)
	{
		if(IsWindow(pogBltDiagram->m_hWnd) )
		{

		 	CString olText = GetString(IMFK_ATTENTION);
 
			if(imBltConflicts > 0)
			{
 				olText.Format("%s (%d)", GetString(IMFK_ATTENTION), imBltConflicts);
			}

			if(imBltConflicts > 0)
				pogBltDiagram->m_CB_BltAttention.SetColors(RGB(255, 0, 0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			else
				pogBltDiagram->m_CB_BltAttention.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
 
			pogBltDiagram->m_CB_BltAttention.SetWindowText(olText);
		}//end if
	}//end if

}




void Konflikte::Confirm(const CPtrArray &ropKonfIds, bool bpRelHdl /* = false */)
{
	KONFENTRY *prlEntry = NULL;
	KONFDATA  *prlData = NULL;

	for (int j = 0; j < ropKonfIds.GetSize(); j++)
	{
		if (omUrnoMap.Lookup((void *)((KonfIdent *)ropKonfIds[j])->FUrno, (void *& )prlEntry) == TRUE)
		{
			for(int i = prlEntry->Data.GetSize() - 1; i >= 0; i--)
			{
				if (prlEntry->Data[i].KonfId != *(KonfIdent *)ropKonfIds[j])
					continue;
				
				// confirm conflict!
				prlEntry->Data[i].NewlyConfirmed = true;
				prlEntry->Data[i].Confirmed = true;
				if (omNotConfirmedMap.RemoveKey((void*) &prlEntry->Data[i]) != 0)
				{
					if (IsBltKonfliktType(prlEntry->Data[i].KonfId.Type))
					{
							imBltConflicts--;
					}
				}
	
				// Do conflict exist in DB?
				// CFLTAB:URNO == ConflictType , when conflict was generated by the flight-hdl!
				if (ogPosDiaFlightData.pomCflData->GetCflByUrno(prlEntry->Data[i].KonfId.Type) == NULL &&
					ogRotationFlights.omCflData.GetCflByUrno(prlEntry->Data[i].KonfId.Type) == NULL)
				{
					// NO! Then create a confirmed record!
					// There are no not confirmed conflicts in the database which was generated from FIPS!!
 					ogPosDiaFlightData.pomCflData->CreateConfirmed(prlEntry->Data[i].KonfId.FUrno, prlEntry->Data[i].KonfId.FPart, prlEntry->Data[i].KonfId.RelFUrno, prlEntry->Data[i].KonfId.RelFPart, prlEntry->Data[i].KonfId.Type, bpRelHdl);
				}
				else
				{
					// only confirm the conflicts
					ogPosDiaFlightData.pomCflData->Confirm(prlEntry->Data[i].KonfId.Type, bpRelHdl);
					ogRotationFlights.omCflData.Confirm(prlEntry->Data[i].KonfId.Type);
				}
			}
		}
	}
	// Release buffered operations
	if (bpRelHdl)
		ogPosDiaFlightData.pomCflData->ReleaseDBUpdate();

	CheckAttentionButton();
}


void Konflikte::ConfirmAll(ConflictGroups ipGroup)
{
	TRACE("Konflikte::ConfirmAll: Start %s\n", CTime::GetCurrentTime().Format("%H:%M:%S"));
	KONFENTRY *prlEntry = NULL;
	KONFDATA  *prlData = NULL;
	//KonfNotify rlNotify;
	bool blConfirm=false;

	CPtrArray olConfirmKonfIds;
	DisableAttentionButtonsUpdate();

	for(int i = omData.GetSize() - 1; i >= 0; i--)
	{
		prlEntry = &omData[i];

		for(int j = prlEntry->Data.GetSize() - 1; j >= 0; j--)
		{
			if(prlEntry->Data[j].SetBy != 0 && prlEntry->Data[j].Confirmed != true)
			{
				// witch conflicts shall be confirmed
				switch (ipGroup)
				{
				case CONF_ALL: 
					blConfirm = true; 
					break;
				case CONF_BLT:
					if (IsBltKonfliktType(prlEntry->Data[j].KonfId.Type))
					{
						blConfirm = true;
					}
					break;
				}
 
				if (blConfirm)
				{
					// collect the conflicts to be confirmed
					blConfirm = false;
					olConfirmKonfIds.Add((void *)&prlEntry->Data[j].KonfId);
				}
			}
		}
	}
	// Confirm all collected conflicts!
	Confirm(olConfirmKonfIds, true);

	EnableAttentionButtonsUpdate();
	TRACE("Konflikte::ConfirmAll: End %s\n", CTime::GetCurrentTime().Format("%H:%M:%S"));
}



void Konflikte::AddKonflikt(const KonfIdent &ropKonfId, CString opText, DWORD dwOrigin, int ipSubOrig, CTime opTimeOfConf, int ipConfirmed)
{
	// Check setup configuration
	if (!IsConflictActivated(ropKonfId.Type))
		return;

	// Check if conflict is allready marked as confirmed in DB
	if (ipConfirmed != 1 &&
		(ogPosDiaFlightData.pomCflData->IsConfirmed("AFT", ropKonfId.FUrno, ropKonfId.FPart, ropKonfId.RelFUrno, ropKonfId.RelFPart, ropKonfId.Type) ||
		 ogRotationFlights.omCflData.IsConfirmed("AFT", ropKonfId.FUrno, ropKonfId.FPart, ropKonfId.RelFUrno, ropKonfId.RelFPart, ropKonfId.Type)))
		ipConfirmed = 1;

	
	KONFENTRY *prlEntry = NULL;
	KONFDATA  *prlData;

	int ilDdxType = -1;

	if((prlData = GetKonflikt(ropKonfId)) != NULL)
	{
		prlData->Flag = true;
		
		prlData->SetBy = prlData->SetBy | dwOrigin;
		prlData->SubId = prlData->SubId | ipSubOrig;
		prlData->Text = opText;

		if(ipConfirmed == 0)
			prlData->Confirmed = false;

		if(ipConfirmed == 1)
			prlData->Confirmed = true;

		
		if(prlData->Confirmed == false)
		{
			void *prlDummy;
			if (!omNotConfirmedMap.Lookup((void *)prlData, (void *&) prlDummy))
			{
				if (IsBltKonfliktType(prlData->KonfId.Type))
					imBltConflicts++;
			}
			omNotConfirmedMap.SetAt((void*) prlData, NULL);
		}
		else
		{
			omNotConfirmedMap.RemoveKey((void*) prlData);

		}
		//nur wenn gerade confirmed wurde(siehe beschreibung in .h)
		if (prlData->NewlyConfirmed)
		{
			ilDdxType = KONF_CHANGE;
			prlData->NewlyConfirmed = false;
		}
	}
	else
	{

		if (omUrnoMap.Lookup((void *)ropKonfId.FUrno, (void *& )prlEntry) == TRUE)
		{
			prlData = new KONFDATA;

			prlData->KonfId = ropKonfId;
			prlData->Text = opText;
			prlData->Confirmed = false;
			prlData->Flag = true;
			prlData->SetBy = dwOrigin;
			prlData->SubId = ipSubOrig;
			prlData->Confirmed = false;
			prlData->NewlyConfirmed = false;
	
			if(ipConfirmed == 1)
				prlData->Confirmed = true;

			
			if(opTimeOfConf	== TIMENULL)
				prlData->TimeOfConflict = CTime::GetCurrentTime();
			else
				prlData->TimeOfConflict = opTimeOfConf;

			ogBasicData.LocalToUtc(prlData->TimeOfConflict);

			prlEntry->Data.Add(prlData);
			if(prlData->Confirmed == false)
			{
				omNotConfirmedMap.SetAt((void*) prlData, NULL);
				if (IsBltKonfliktType(prlData->KonfId.Type))
				{
					imBltConflicts++;
				}
			}
			ilDdxType = KONF_INSERT;
		}
		else
		{
			prlEntry = new KONFENTRY;
			omData.Add(prlEntry);
			prlEntry->Urno = ropKonfId.FUrno;
			prlData = new KONFDATA;

			omUrnoMap.SetAt((void *)prlEntry->Urno,(void *& )prlEntry);
			prlData->KonfId = ropKonfId;
			prlData->Text = opText;
			prlData->Confirmed = false;
			prlData->Flag = true;
			prlData->SetBy = dwOrigin;
			prlData->SubId = ipSubOrig;
			prlData->Confirmed = false;
			prlData->NewlyConfirmed = false;
	
			if(ipConfirmed == 1)
				prlData->Confirmed = true;

			if(opTimeOfConf	== TIMENULL)
				prlData->TimeOfConflict = CTime::GetCurrentTime();
			else
				prlData->TimeOfConflict = opTimeOfConf;

			ogBasicData.LocalToUtc(prlData->TimeOfConflict);
			prlEntry->Data.Add(prlData);
			if(prlData->Confirmed == false)
			{
				omNotConfirmedMap.SetAt((void*) prlData, NULL);
				if (IsBltKonfliktType(prlData->KonfId.Type))
				{
					imBltConflicts++;
				}
			}
			ilDdxType = KONF_INSERT;
		}
	}


	if(ilDdxType != -1)
	{
		KonfIdent rlIdNotify;
		rlIdNotify = ropKonfId;
		ogDdx.DataChanged((void *)this, ilDdxType,(void *)&rlIdNotify );
	}

}



bool Konflikte::IsConflictActivated(int ipKonfType) const
{
	// examine all conflicts in the auto allocation process
	if (bgPosDiaAutoAllocateInWork)
		return true;

	switch (ipKonfType)
	{
	case IDS_STRING2001: // Position - Gate Relation
		{
			if (bmPosGateRelation)
				return true;
			else
				return false;
		}
		break;
	}

	return true;
}




bool Konflikte::IsBltKonfliktType(int ipKonfType) const
{
	return (ipKonfType==IDS_STRING1735 || ipKonfType==IDS_STRING1736);
}


bool Konflikte::IsNeighborKonfliktType(int ipKonfType) const
{
	return (ipKonfType==IDS_STRING1723 || ipKonfType==IDS_STRING2030);
}



void Konflikte::CheckFlight(ROTATIONFLIGHTDATA *prpFlight, bool bpDep /*  = true */)
{
	if (IsArrival(prpFlight->Org3, prpFlight->Des3))
		CheckFlight(prpFlight, 'A', bpDep);
	if (IsDeparture(prpFlight->Org3, prpFlight->Des3))
		CheckFlight(prpFlight, 'D', bpDep);
}



void Konflikte::CheckFlight(ROTATIONFLIGHTDATA *prpFlight, char cpFPart, bool bpDep /*  = true */)
{

	ResetFlight(prpFlight->Urno, cpFPart, MOD_ID_ROTATION);

	if(CString("O;T;G;B;Z;S").Find(prpFlight->Ftyp) < 0)
		return;

	CString olText;
	CString  olBez;
	if(cpFPart == 'A')
	{
		olBez.Format("%-9s (A): ", prpFlight->Flno);
		if (bmFlightDataChanged)
		{
			// check conflicts generated from flighthdl
			CString olExtConfBez;
			olExtConfBez.Format("%sSTA: %s", olBez, prpFlight->Stoa.Format("%H:%M %d.%m.%Y"));				
			CheckFlightExtConf(prpFlight->Urno, cpFPart, olExtConfBez, &ogRotationFlights.omCflData, MOD_ID_ROTATION);
		}

	}
	else
	{
		olBez.Format("%-9s (D): ", prpFlight->Flno);
		if (bmFlightDataChanged)
		{
			// check conflicts generated from flighthdl
			CString olExtConfBez;
			olExtConfBez.Format("%sSTD: %s", olBez, prpFlight->Stod.Format("%H:%M %d.%m.%Y"));				
			CheckFlightExtConf(prpFlight->Urno, cpFPart, olExtConfBez, &ogRotationFlights.omCflData, MOD_ID_ROTATION);
		}
	}

	//if(olBez.IsEmpty())
	//	return;

	CString oTmp;
	CString olPst;

	
	ROTATIONFLIGHTDATA *prlFlightA;
	ROTATIONFLIGHTDATA *prlFlightD;

	CTime olCurr = CTime::GetCurrentTime();

	ogBasicData.LocalToUtc(olCurr);
	char buffer[256];

	CTimeSpan ol30(0,0,30,0);
	CTimeSpan ol15(0,0,15,0);
	CTimeSpan ol5(0,0,5,0);


	/* Conflict No. 1		Start-landing order!  */
	/* Conflict No. 2	15	No Onblock at Touch down + %d  Minutes */
	/* Conflict No. 3	5	Actual Time > ETA + %d Minutes  */
	/* Conflict No. 4	-5	Actual Time  + %d Min > Next Information Time  */
	/* Conflict No. 5		Pos: %s  A/C: %s  error (%s) from parking system !  */
	/* Conflict No. 6		STA/ETA    >=    STD/ETD  */
	/* Conflict No. 7		STD/ETD + 30 Min. < actual time and no DEP telegram received  */
	/* Conflict No. 8		Aircrafttype has been changed */
	/* Conflict No. 9	-5	No Take off time at Offblock + %d Minutes */
	/* Conflict No. 10	5	No Offblock at STD + %d Minutes */
	/* Conflict No. 11	15	ETD > STD + %d Minutes  */
	/* Conflict No. 12	-5	Actual Time + %d Min > Next Information Time  */
	/* Conflict No. 13	30	STD/ETD - 30 Min. < actual time and no boarding information received */
	/* Conflict No. 14	15	ETA > STA + %d Minutes  */


	/*
	omNoOnblAfterLand	= CTimeSpan(0,0,15,0);
	omCurrEtai			= CTimeSpan(0,0,5,0);
	omCurrDNxti = CTimeSpan(0,0,5,0);
	omCurrANxti = CTimeSpan(0,0,5,0);
	omStoaStod = CTimeSpan(0,0,0,0);
	omStodCurrAndNoAirb = CTimeSpan(0,0,5,0);
	omCurrOfbl = CTimeSpan(0,0,15,0);
	omCurrOfblStod = CTimeSpan(0,0,5,0);
	omStodEtdi = CTimeSpan(0,0,15,0);
	omCurrStodGd1x = CTimeSpan(0,0,30,0);
	*/




	/////////////////////////////////////////////
	/////////// NEW FLIGHT
	CTime olSto;

	CTime olStart= CTime(olCurr.GetYear(), olCurr.GetMonth(), olCurr.GetDay(),0,0,0);
	CTime olEnd= CTime(olCurr.GetYear(), olCurr.GetMonth(), olCurr.GetDay(),23,59,59);


	if(IsBetween(prpFlight->Cdat , olStart, olEnd))
	{
	
		if(cpFPart == 'A')
		{
			olText.Format("%sSTA: %s   %s (%s %s)", olBez, prpFlight->Stoa.Format("%H:%M %d.%m.%Y"), GetString(IDS_STRING1805), prpFlight->Usec, prpFlight->Cdat.Format("%H:%M"));
		}
		else			
		{
			olText.Format("%sSTD: %s   %s (%s %s)", olBez, prpFlight->Stod.Format("%H:%M %d.%m.%Y"), GetString(IDS_STRING1805), prpFlight->Usec, prpFlight->Cdat.Format("%H:%M"));
		}
		
		AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0 , ' ', IDS_STRING1805), olText, MOD_ID_ROTATION);

	}
	////////////////////////////


	if(cpFPart == 'A')
	{
		olText.Format("%sSTA: %s     ", olBez, prpFlight->Stoa.Format("%H:%M %d.%m.%Y"));
		
		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 14  |ETA - STA| > %d Minutes  */
		
		if(bmStoaEtai)
		{
			CTimeSpan olEtaStaDiff = prpFlight->Etai - prpFlight->Stoa;
			if((prpFlight->Etai != TIMENULL) && (labs(olEtaStaDiff.GetTotalMinutes()) > omStoaEtai.GetTotalMinutes() )  && (prpFlight->Airb == TIMENULL))
			{
				oTmp.Format(GetString(IDS_STRING1860), omStoaEtai.GetTotalMinutes());
 				AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1860), olText + oTmp, MOD_ID_ROTATION);
			}
		}






		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 14  Return flight */

		if( CString(prpFlight->Stat).GetLength() >= 10)
		{
			if( prpFlight->Stat[8] == '1')
			{
				AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1697), olBez + GetString(IDS_STRING1697), MOD_ID_ROTATION);

			}
		}
		
		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 1  start-landing order!  */
		
		if(bmJoinConf)
		{
			if(strcmp(prpFlight->Rtyp, "E") == 0)
			{
				olText.Format("%sSTA: %s  <%s>   ", olBez, prpFlight->Stoa.Format("%H:%M %d.%m.%Y"), prpFlight->Regn);
	
				olText += GetString(IDS_STRING1430);
			
				AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1430), olText, MOD_ID_ROTATION);
			}
		}
		


		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 2   No Onblock at Touch down + %d  Minutes */

		if(bmNoOnblAfterLand)
		{
			if((prpFlight->Land != TIMENULL) && (prpFlight->Onbl == TIMENULL) && (olCurr > prpFlight->Land + omNoOnblAfterLand))
			{

				oTmp.Format(GetString(IDS_STRING1236), omNoOnblAfterLand.GetTotalMinutes() );
				//CString("Kein Onblock bei Landung + %d Minuten");
				AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1236), olBez + oTmp, MOD_ID_ROTATION);
			}
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 3   Actual Time > ETA + %d Minutes  */

		if(bmCurrEtai)
		{
			if (strcmp(pcgHome, "PVG") == 0)
			{
				// Shanghai need other format and ETOA!
				if((prpFlight->Etoa != TIMENULL) && (olCurr > prpFlight->Etoa + omCurrEtai /*ol5*/) && (prpFlight->Land == TIMENULL))
				{
					CString olTmp2;
					olTmp2.Format(GetString(IDS_STRING2024), omCurrEtai.GetTotalMinutes(), prpFlight->Lstu.Format("%H:%M"));
					oTmp.Format("%s %s %s %s %s", GetString(IDS_STRING323), prpFlight->Stoa.Format("%H:%M"), GetString(IDS_STRING2028), prpFlight->Etoa.Format("%H:%M"), olTmp2);
					AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1237), olBez + oTmp, MOD_ID_ROTATION);
				}
			}
			else
			{
				if((prpFlight->Etai != TIMENULL) && (olCurr > prpFlight->Etai + omCurrEtai /*ol5*/) && (prpFlight->Land == TIMENULL))
				{
					oTmp.Format(GetString(IDS_STRING1237), omCurrEtai.GetTotalMinutes());
					AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1237), olBez + oTmp, MOD_ID_ROTATION);
				}
			}
		}
		

		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 4   Actual Time  + %d Min > Next Information Time  */
		
		if(bmCurrANxti)
		{
			if((prpFlight->Nxti != TIMENULL) && (olCurr + omCurrANxti > prpFlight->Nxti) && (prpFlight->Land == TIMENULL))
			{

				oTmp.Format(GetString(IDS_STRING1238), omCurrANxti.GetTotalMinutes());//CString("Aktuelle Zeit + %d Min > Next Information - Zeit ");
				AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1238), olBez + oTmp, MOD_ID_ROTATION);
			}
		}

		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 5   Pos: %s  A/C: %s  error (%s) from parking system !  */

		if(bmAderConf)
		{
			if(strlen(prpFlight->Ader) > 0)
			{

				sprintf(buffer,GetString(IDS_STRING1428), prpFlight->Psta, prpFlight->Act3, prpFlight->Ader);
				//Andocksystem;
				AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1428), olBez + CString(buffer), MOD_ID_ROTATION);
			}
		}

		prlFlightD = ogRotationFlights.GetDeparture(prpFlight);


		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 6   STA/ETA    >=    STD/ETD  */

		if(bmStoaStod)
		{

			if(prlFlightD != NULL)
			{
				if(prpFlight->Etai != TIMENULL)
				{
					
					if(((prpFlight->Etai + omStoaStod >= prlFlightD->Stod) && (prlFlightD->Etdi == TIMENULL) ) || ( (prpFlight->Etai + omStoaStod >= prlFlightD->Etdi) &&  (prlFlightD->Etdi != TIMENULL)  ))
					{

						oTmp.Format(GetString(IDS_STRING1432), omStoaStod.GetTotalMinutes());//CString("STA/ETA + %d Min ist gr��er als  STD/ETD");
						olText = olBez + oTmp;
						
						AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1432), olText, MOD_ID_ROTATION);
					}

				}
			}
		}

			
		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 7   STD/ETD + %d Min. < actual time and no DEP telegram received  */

		if(bmStodCurrAndNoAirb)
		{
			if (strcmp(pcgHome, "PVG") == 0)
			{
				// Shanghai need other format!
				if(prpFlight->Airb == TIMENULL && prpFlight->Stod != TIMENULL && (prpFlight->Stod + omStodCurrAndNoAirb < olCurr) &&  prpFlight->Aird == TIMENULL)
				{

					// STD+%d minutes still have not received DEP telex (%s)
					oTmp.Format(GetString(IDS_STRING2026), omStodCurrAndNoAirb.GetTotalMinutes(), prpFlight->Lstu.Format("%H:%M"));
					olText = olBez + oTmp;
										
					AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1549), olText, MOD_ID_ROTATION);
				}
			}
			else
			{
				if( ( prpFlight->Airb == TIMENULL && (prpFlight->Stod + omStodCurrAndNoAirb < olCurr) &&  prpFlight->Aird == TIMENULL && prpFlight->Etdi == TIMENULL ) ||
					( (prpFlight->Etdi + omStodCurrAndNoAirb  < olCurr) &&  prpFlight->Aird == TIMENULL && prpFlight->Etdi != TIMENULL ) ) 
				{

					oTmp.Format(GetString(IDS_STRING1549), omStodCurrAndNoAirb.GetTotalMinutes());
					olText = olBez + oTmp;
					
					//CString("STD/ETD + 30 Min. > actual time and No DEP telegram received");
					
					AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1549), olText,  MOD_ID_ROTATION);
				}
			}
		}

		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 8   Aircrafttype has been changed */

		/*
		if( bmActRegnChangeConf)
		{
			// IDS_STRING1551  Registrierung hat sich ge�ndert
			if( CString(prpFlight->Regn) != CString(prpFlight->LastRegn)) 
			{
				olText = olBez + GetString(IDS_STRING1551);
				AddKonflikt(prpFlight->Urno, olText, IDS_STRING1551, MOD_ID_ROTATION);
			}
			else
			{
				// IDS_STRING1550  Flugzeugtyp hat sich ge�ndert
				if( CString(prpFlight->Act5) != CString(prpFlight->LastAct5)) 
				{
					olText = olBez + GetString(IDS_STRING1550);
					AddKonflikt(prpFlight->Urno, olText, IDS_STRING1550, MOD_ID_ROTATION);
				}
			}
		
		}*/


		olPst = prpFlight->Psta;

	}

	if(cpFPart == 'D')
	{
		prlFlightA = ogRotationFlights.GetArrival(prpFlight);


		if(prlFlightA != NULL)
		{
			if(bpDep) CheckFlight(prlFlightA, 'A', false);
		}


		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 1  start-landing order!  */

		if(bmJoinConf)
		{
			if(strcmp(prpFlight->Rtyp, "E") == 0)
			{
				olText = olBez + CString("STA: ") + prpFlight->Stoa.Format("%H:%M %d.%m.%Y  <") + CString(prpFlight->Regn) + CString(">   ");

				olText += GetString(IDS_STRING1430);//CString("Kein Onblock bei Landung + 15 Minuten");
				AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1430), olText, MOD_ID_ROTATION);
			}
		}
		
		


		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 9   No Take off time at Offblock + %d Minutes */

		if(bmCurrOfbl)
		{
			if((prpFlight->Airb == TIMENULL) && (prpFlight->Ofbl != TIMENULL) && (olCurr > prpFlight->Ofbl + omCurrOfbl/*ol15*/))
			{
				oTmp.Format(GetString(IDS_STRING1239), omCurrOfbl.GetTotalMinutes());
				olText = olBez + CString("STD: ") + prpFlight->Stod.Format("%H:%M %d.%m.%Y     ") + oTmp;
				//CString("Keine Startzeit bei Offblock + %d Minuten");
				AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1239), olText, MOD_ID_ROTATION);
			}
		}


		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 10    No Offblock at STD + %d Minutes */
		
		if(bmCurrOfblStod)
		{
			if( prpFlight->Ftyp[0] != 'X' && prpFlight->Ftyp[0] != 'N')
			{
				if((prpFlight->Ofbl == TIMENULL) && (olCurr > prpFlight->Stod + omCurrOfblStod))
				{
					oTmp.Format(GetString(IDS_STRING1240), omCurrOfblStod.GetTotalMinutes());
					olText = olBez + CString("STD: ") + prpFlight->Stod.Format("%H:%M %d.%m.%Y     ") + oTmp;
					//CString("Kein Offblock bei STD + %d Minuten");
					AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1240), olText, MOD_ID_ROTATION);
				}
			}
		}


		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 11  |ETD - STD| > %d Minutes  */
		
		if(bmStodEtdi)
		{
			CTimeSpan olEtdStdDiff = prpFlight->Etdi - prpFlight->Stod;
			if((prpFlight->Etdi != TIMENULL) && (labs(olEtdStdDiff.GetTotalMinutes()) > omStodEtdi.GetTotalMinutes() )  && (prpFlight->Airb == TIMENULL))
			{
				oTmp.Format(GetString(IDS_STRING1241), omStodEtdi.GetTotalMinutes());
				olText = olBez + CString("STD: ") + prpFlight->Stod.Format("%H:%M %d.%m.%Y     ") + oTmp;
				//CString("ETD ist gr��er STD  + 15 Minuten");
				AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1241), olText, MOD_ID_ROTATION);
			}
		}

		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 12   Actual Time + %d Min > Next Information Time  */

		if(bmCurrDNxti)
		{
			if((prpFlight->Nxti != TIMENULL) && (olCurr + omCurrDNxti > prpFlight->Nxti) && (prpFlight->Airb == TIMENULL))
			{

				oTmp.Format(GetString(IDS_STRING1242), omCurrDNxti.GetTotalMinutes());
				olText = olBez + CString("STD: ") + prpFlight->Stod.Format("%H:%M %d.%m.%Y     ") + oTmp;
				
				//CString("Aktuelle Zeit > Next Information - Zeit ");
				AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1242), olText, MOD_ID_ROTATION);
			}
		}

		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 5    Pos: %s  A/C: %s  error (%s) from parking system ! */

		if(bmAderConf)
		{
			if(strlen(prpFlight->Ader) > 0)
			{
				sprintf(buffer,GetString(IDS_STRING1428), prpFlight->Pstd, prpFlight->Act3, prpFlight->Ader);

				olText = olBez + CString("STD: ") + prpFlight->Stod.Format("%H:%M %d.%m.%Y     ") + CString(buffer); //Andocksystem;
				AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1428), olText, MOD_ID_ROTATION);
			}
		}


		olPst = prpFlight->Pstd;
	}  // if(cpFPart == 'D') ...



	if (strcmp(pcgHome, "PVG") != 0)
	{
		if(strcmp(prpFlight->Ftyp, "Z") == 0)
		{
			olText.Format("%sSTD: %s%s%s", olBez, prpFlight->Stod.Format("%H:%M %d.%m.%Y     "), GetString(IDS_STRING1776), CString(prpFlight->Stat).Mid(1,3));
			AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1776), olText, MOD_ID_ROTATION);
		}


		if(strcmp(prpFlight->Ftyp, "B") == 0)
		{
			olText.Format("%sSTD: %s%s%s", olBez, prpFlight->Stod.Format("%H:%M %d.%m.%Y     "), GetString(IDS_STRING1777), CString(prpFlight->Stat).Mid(1,3));
			AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1777), olText, MOD_ID_ROTATION);
		}
	}


	////////////////////////////////////////////////////////////////////////////////////////
	/* Conflict No. 1  start-landing order!  */

	if(strcmp(prpFlight->Adid, "B") == 0)
	{
		if(bmJoinConf)
		{
			if(strcmp(prpFlight->Rtyp, "E") == 0)
			{
				olText.Format("%sSTA/STD: %s%s%s%s", olBez, prpFlight->Stoa.Format("%H:%M %d.%m.%Y / "), prpFlight->Stod.Format("%H:%M %d.%m.%Y  <"), CString(prpFlight->Regn), CString(">   "));

				olText += GetString(IDS_STRING1430);//CString("start-landing order!");
				AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1430), olText, MOD_ID_ROTATION);
			}
		}
	}

	CheckCommonConflicts(prpFlight, NULL, MOD_ID_ROTATION, SUB_MOD_NOTIMP, olCurr, olBez);

	CheckFlightConf(prpFlight->Urno, cpFPart, MOD_ID_ROTATION);
	CheckAttentionButton();
}



void Konflikte::CheckCommonConflicts(const ROTATIONFLIGHTDATA *prpRotFlight, const DIAFLIGHTDATA *prpDiaFlight, 
									 DWORD dwOrigin, int ipSubOrig, const CTime &ropTestTime, const CString &ropFlightText)
{

	if (prpRotFlight == NULL && prpDiaFlight == NULL) return;

	const CTime *polStod;
	const CTime *polEtdi;
	const CTime *polEtod;
	const CTime *polGd1x;
	const CTime *polLstu;
	const char *pclAdid;
	long llUrno;
	char clFPart = ' '; 

	if (prpRotFlight != NULL)
	{
		llUrno = prpRotFlight->Urno;
		polStod = &prpRotFlight->Stod;
		polEtdi = &prpRotFlight->Etdi;
		polEtod = &prpRotFlight->Etod;
		polGd1x = &prpRotFlight->Gd1x;
		polLstu = &prpRotFlight->Lstu;
		pclAdid = prpRotFlight->Adid;
		if (IsArrivalFlight(prpRotFlight->Org3, prpRotFlight->Des3, prpRotFlight->Ftyp[0])) 
			clFPart = 'A';
		if (IsDepartureFlight(prpRotFlight->Org3, prpRotFlight->Des3, prpRotFlight->Ftyp[0])) 
			clFPart = 'D';
	}
	if (prpDiaFlight != NULL)
	{
		llUrno = prpDiaFlight->Urno;
		polStod = &prpDiaFlight->Stod;
		polEtdi = &prpDiaFlight->Etdi;
		polEtod = &prpDiaFlight->Etod;
		polGd1x = &prpDiaFlight->Gd1x;
		polLstu = &prpDiaFlight->Lstu;
		pclAdid = prpDiaFlight->Adid;
		if (IsArrivalFlight(prpDiaFlight->Org3, prpDiaFlight->Des3, prpDiaFlight->Ftyp[0])) 
			clFPart = 'A';
		if (IsDepartureFlight(prpDiaFlight->Org3, prpDiaFlight->Des3, prpDiaFlight->Ftyp[0])) 
			clFPart = 'D';
	}

	if (clFPart == ' ')
		return;

	CString olTmp;
	CString olConfText;

	if(clFPart == 'D')
	{

		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 13    STD/ETD - 30 Min. < actual time and no boarding information received */

		if(bmCurrStodGd1x)
		{
			if (strcmp(pcgHome, "PVG") == 0)
			{
				// Shanghai need other format and ETOD
				bool blConf = false;
				CString olField;
				if( *polGd1x == TIMENULL && *polEtod == TIMENULL && (*polStod - omCurrStodGd1x < ropTestTime))
				{
					blConf = true;
					olField = GetString(IDS_STRING316);
				}
				if ( *polGd1x == TIMENULL && *polEtod != TIMENULL && (*polEtod - omCurrStodGd1x < ropTestTime)) 
				{
					blConf = true;
					olField = GetString(IDS_STRING2029);
				}
				if (blConf)
				{
					// %d minutes before %s but still does not has any Boarding Information (%s)
					olTmp.Format(GetString(IDS_STRING2025), omCurrStodGd1x.GetTotalMinutes(), olField, polLstu->Format("%H:%M"));
					olConfText.Format("%s%s %s  %s", ropFlightText, GetString(IDS_STRING316), polStod->Format("%H:%M"), olTmp);
		
					AddKonflikt(KonfIdent(llUrno, pclAdid[0], 0, ' ', IDS_STRING1548), olConfText, dwOrigin, SUB_MOD_GAT);
				}
			}
			else
			{
				if( ( *polGd1x == TIMENULL && *polEtdi == TIMENULL && (*polStod - omCurrStodGd1x < ropTestTime)) ||
					( *polGd1x == TIMENULL && *polEtdi != TIMENULL && (*polEtdi - omCurrStodGd1x < ropTestTime)) ) 
				{
					olTmp.Format(GetString(IDS_STRING1548), omCurrStodGd1x.GetTotalMinutes());
					olConfText = ropFlightText + olTmp;//CString("STD/ETD - 30 Min. > actual time and no boarding information received");
		
					AddKonflikt(KonfIdent(llUrno, pclAdid[0], 0, ' ', IDS_STRING1548), olConfText, dwOrigin, SUB_MOD_GAT);
				}
			}		
		
		
		}

	}
}




///////////////////////////////////////////////
// SAS


void Konflikte::SASCheckAll()
{
	CTime olTestTime = CTime::GetCurrentTime();

	DisableAttentionButtonsUpdate();
 
	DiaCedaFlightData::RKEYLIST *prlRkey;


	DIAFLIGHTDATA *prlFlight1;
	DIAFLIGHTDATA *prlFlight2;

	
	POSITION pos;
	void *pVoid;
	for( pos = ogPosDiaFlightData.omRkeyMap.GetStartPosition(); pos != NULL; )
	{
		ogPosDiaFlightData.omRkeyMap.GetNextAssoc( pos, pVoid , (void *&)prlRkey );

		int ilCount = prlRkey->Rotation.GetSize();

		prlRkey->Rotation.Sort(CompareByRkey);

		for(int i = 0; i < ilCount; i++)
		{
			prlFlight1 = &(*prlRkey).Rotation[i];

			prlFlight2 = NULL;

			if(i + 1 < ilCount )
			{
				prlFlight2 = &(*prlRkey).Rotation[i+1];

				if( prlFlight2->Adid[0] == 'D')
				{
					i++;
				}
			}

			if(prlFlight2 == NULL)
			{
				if( strcmp(prlFlight1->Org3, pcgHome) == 0)
				{
					prlFlight2 = prlFlight1;
					prlFlight1 = NULL;
				}
			}

			CheckFlightInternal( prlFlight1, prlFlight2);
		}
	}



	/*	
	int ilCount = ogBCD.GetDataCount("PST");

	CString olPst;

	for(int i = 0; i < ilCount; i++)
	{
		olPst = ogBCD.GetField("PST", i, "PNAM");

		CheckPosition(olPst);
	}

	ilCount = ogBCD.GetDataCount("GAT");

	CString olGat;

	for( i = 0; i < ilCount; i++)
	{
		olGat = ogBCD.GetField("GAT", i, "GNAM");

		CheckGate(olGat);
	}


	ilCount = ogBCD.GetDataCount("BLT");

	CString olBlt;

	for( i = 0; i < ilCount; i++)
	{
		olBlt = ogBCD.GetField("BLT", i, "BNAM");

		CheckBlt(olBlt);
	}


	ilCount = ogBCD.GetDataCount("WRO");
	CString olWro;

	for( i = 0; i < ilCount; i++)
	{
		olWro = ogBCD.GetField("WRO", i, "WNAM");

		CheckWro(olWro);
	}
	*/	

	EnableAttentionButtonsUpdate();
 
	CTime olTestTimeEnd =  CTime::GetCurrentTime();
	TRACE("Konflikte::SASCheckAll: Laufzeit: %d\n", (olTestTimeEnd-olTestTime).GetTotalSeconds());
}



void Konflikte::CheckPosition(CString opPst)
{
	DiaCedaFlightData::RKEYLIST *prlRkey;
	
	if(ogPosDiaFlightData.omPosMap.Lookup(opPst,(void *& )prlRkey) == TRUE)
	{
		DIAFLIGHTDATA *prlFlight1;
		DIAFLIGHTDATA *prlFlight2;

		int ilCount = prlRkey->Rotation.GetSize();

		prlRkey->Rotation.Sort(CompareByRkey);

		for(int i = 0; i < ilCount; i++)
		{
			prlFlight1 = &(*prlRkey).Rotation[i];

			prlFlight2 = NULL;

			if(i + 1 < ilCount )
			{
				prlFlight2 = &(*prlRkey).Rotation[i+1];

				if(prlFlight1->Rkey != prlFlight2->Rkey)
				{
					if( strcmp(prlFlight1->Des3, pcgHome) == 0)
					{
						prlFlight2 = NULL;
					}
					else
					{
						prlFlight2 = prlFlight1;
						prlFlight1 = NULL;
					}
				}
				else
				{
					i++;
				}
			}

			if(prlFlight2 == NULL)
			{
				if( strcmp(prlFlight1->Des3, pcgHome) == 0)
				{
					prlFlight2 = NULL;
				}
				else
				{
					prlFlight2 = prlFlight1;
					prlFlight1 = NULL;
				}
			}

			CheckFlightInternal( prlFlight1, prlFlight2);
		}
	}

}


void Konflikte::CheckGate(CString opGat)
{
	DiaCedaFlightData::RKEYLIST *prlRkey;

	
	if(ogPosDiaFlightData.omGatMap.Lookup(opGat,(void *& )prlRkey) == TRUE)
	{
		DIAFLIGHTDATA *prlFlight1;
		int ilCount = prlRkey->Rotation.GetSize();

		prlRkey->Rotation.Sort(CompareByRkey);

		for(int i = 0; i < ilCount; i++)
		{
			prlFlight1 = &(*prlRkey).Rotation[i];

			if( strcmp(prlFlight1->Des3, pcgHome) == 0)
			{
				CheckFlightInternal( prlFlight1, NULL);
			}
			else
			{
				CheckFlightInternal( NULL, prlFlight1);
			}

		}
	}

}



void Konflikte::CheckWro(CString opWro)
{
	DiaCedaFlightData::RKEYLIST *prlRkey;

	
	if(ogPosDiaFlightData.omWroMap.Lookup(opWro,(void *& )prlRkey) == TRUE)
	{
		DIAFLIGHTDATA *prlFlight1;
		int ilCount = prlRkey->Rotation.GetSize();

		prlRkey->Rotation.Sort(CompareByRkey);

		for(int i = 0; i < ilCount; i++)
		{
			prlFlight1 = &(*prlRkey).Rotation[i];

			if( strcmp(prlFlight1->Org3, pcgHome) == 0)
			{
				CheckFlightInternal( NULL, prlFlight1);
			}
		}
	}
}



void Konflikte::CheckBlt(CString opBlt)
{
	DiaCedaFlightData::RKEYLIST *prlRkey;
	
	if(ogPosDiaFlightData.omBltMap.Lookup(opBlt,(void *& )prlRkey) == TRUE)
	{
		DIAFLIGHTDATA *prlFlight1;
		int ilCount = prlRkey->Rotation.GetSize();

		prlRkey->Rotation.Sort(CompareByRkey);

		for(int i = 0; i < ilCount; i++)
		{
			prlFlight1 = &(*prlRkey).Rotation[i];

			if( strcmp(prlFlight1->Des3, pcgHome) == 0)
			{
				CheckFlightInternal( prlFlight1, NULL);
			}
		}
	}
}





void Konflikte::CheckFlight(DIAFLIGHTDATA *prpFlight)
{
	CString olPst;

	if( strcmp(prpFlight->Des3, pcgHome) == 0)
		olPst = prpFlight->Psta;
	else
		olPst = prpFlight->Pstd;
	
	if(!olPst.IsEmpty())
	{
		DiaCedaFlightData::RKEYLIST *prlRkey;

		if(ogPosDiaFlightData.omPosMap.Lookup(olPst,(void *& )prlRkey) == TRUE)
		{
			DIAFLIGHTDATA *prlFlight1;
			DIAFLIGHTDATA *prlFlight2;
			int ilCount = prlRkey->Rotation.GetSize();
	
			prlRkey->Rotation.Sort(CompareByRkey);

			for(int i = 0; i < ilCount; i++)
			{
				prlFlight1 = &(*prlRkey).Rotation[i];

				prlFlight2 = NULL;

				if(i + 1 < ilCount )
				{
					prlFlight2 = &(*prlRkey).Rotation[i+1];

					if(prlFlight1->Rkey != prlFlight2->Rkey)
					{
						if( strcmp(prlFlight1->Des3, pcgHome) == 0)
						{
							prlFlight2 = NULL;
						}
						else
						{
							prlFlight2 = prlFlight1;
							prlFlight1 = NULL;
						}
					}
					else
					{
						/*

						if(CString(prlFlight1->Adid) == "D")
						{
							prlFlight2 = prlFlight1;
							prlFlight1 = NULL;
						}
						else
						{
						
							if(prlFlight2->Tifd <= prlFlight1->Tifa)
							{
								prlFlight2 = NULL;
							}
							else
							{
								if(CString(prlFlight2->Adid) != "B")
									i++;
							}				
						}
						*/
						i++;
					}
				}
				if(prlFlight2 == NULL)
				{
					if( strcmp(prlFlight1->Des3, pcgHome) == 0)
					{
						prlFlight2 = NULL;
					}
					else
					{
						prlFlight2 = prlFlight1;
						prlFlight1 = NULL;
					}
				}

				CheckFlightInternal( prlFlight1, prlFlight2);
			}
		}
	}

}




void Konflikte::CheckFlightExtConf(long lpUrno, char cpFPart, const CString &ropBez, CedaCflData *popCflData, DWORD wpOrigin)
{
	if (!bmFlightDataChanged || lpUrno <= 0 || popCflData == NULL)
		return;

	CCSPtrArray<CFLDATA> olCflList;
	popCflData->GetCflDataByRurnUrno(olCflList, lpUrno);


	CFLDATA *prlCfl;
	CString olText;
	int ilConfirmed;
	CString olUser;

	for(int i = 0; i < olCflList.GetSize(); i++)
	{
		prlCfl = &olCflList[i];

		if (strcmp(prlCfl->Mety, "FI") == 0)
			// record was generated by FIPS! 
			// The conflict will be internal (not in DB) generated once more in later functions! 
			continue;


		CString olConfText = ogBCD.GetField("LBL", "TKEY", prlCfl->Meno, "TEXT"); 
		CString olConfUrno = ogBCD.GetField("LBL", "TKEY", prlCfl->Meno, "URNO"); 

		CString olNewVal;
		CString olOldVal;

		CStringArray olArray;

		if(!olConfUrno.IsEmpty())
		{

			CString olTmp;

			olText.Format("%s   %s", ropBez, olConfText);				


			// get old value
			olOldVal.Empty();
			ExtractItemList(CString(prlCfl->Oval), &olArray, 30);
			for(int k = 0; k < olArray.GetSize() - 1; k++)
			{
				olOldVal += olArray[k] + CString(",");
			}
			// Remove last ','
			if(!olOldVal.IsEmpty())
				olOldVal = olOldVal.Left(olOldVal.GetLength() - 1); 

			olArray.RemoveAll();

			// get new value
			olNewVal.Empty();
			ExtractItemList(CString(prlCfl->Nval), &olArray, 30);
			for(k = 0; k < olArray.GetSize() - 1; k++)
			{
				olNewVal += olArray[k] + CString(",");
			}
			// Remove last ','
			if(!olNewVal.IsEmpty())
				olNewVal = olNewVal.Left(olNewVal.GetLength() - 1); 

			// get user who changed the value
			CString olUserName("");
			if (olArray.GetSize() > 1)
				olUserName = olArray[olArray.GetSize()-1];

			// format conflict string
			olTmp.Format(" %s -> %s (%s %s)", olOldVal, olNewVal, olUserName, olCflList[i].Time.Format("%H:%M"));
			olText += olTmp;

			olUser = prlCfl->Akus;
			olUser.TrimRight();


			if(olUser.IsEmpty())
				ilConfirmed = 0;
			else
				ilConfirmed = 1;


			AddKonflikt(KonfIdent(lpUrno, cpFPart, 0, ' ', olCflList[i].Urno), olText, wpOrigin, SUB_MOD_ALL, olCflList[i].Time, ilConfirmed);
		}

	}

	olCflList.RemoveAll();
}




void Konflikte::CheckFlightInternal(DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD, bool bpOnlyNeighborConf /* = false */)
{


	DiaCedaFlightData::RKEYLIST *prpRkey ;


	long llRkey = 0;

	if(prpFlightA != NULL)
		llRkey = prpFlightA->Rkey;


	if(prpFlightD != NULL)
		llRkey = prpFlightD->Rkey;

	if (llRkey == 0)
		return;


	prpRkey = ogPosDiaFlightData.GetRotationByRkey(llRkey);


	if (prpRkey == NULL)
		return;



	DIAFLIGHTDATA *prlFlightA;
	DIAFLIGHTDATA *prlFlightD;

//	prpRkey->Rotation.Sort(DiaCedaFlightData::CompareByRkey);
	prpRkey->Rotation.Sort(DiaCedaFlightData::CompareRotationFlight);


	int ilCount = prpRkey->Rotation.GetSize();

	for(int i = 0; i < ilCount; i++)
	{
		prlFlightA = &(*prpRkey).Rotation[i];


		if(prpRkey->Rotation.GetSize() == 1 && prlFlightA->Adid[0] == 'D')
		{
			prlFlightD = prlFlightA;
			prlFlightA = NULL;
		}
		else
		{
			if(i+1 <  ilCount)
			{
				prlFlightD = &(*prpRkey).Rotation[i+1];
		
				
				if(prlFlightD->Tifd < prlFlightA->Tifa || strcmp(prlFlightD->Pstd, prlFlightA->Psta) != 0)
				{
					prlFlightD = NULL;
				}
				else
				{
					if(prlFlightD->Adid[0] != 'B')
						i++;
				}				
			
			
			}
			else
			{
				if(prlFlightA->Adid[0] == 'D')
				{
					prlFlightD = prlFlightA;
					prlFlightA = NULL;
				}
				else
				{
					prlFlightD = NULL;
				}
			}
		}


		if (prlFlightA != NULL && IsCircularFlight(prlFlightA->Org3, prlFlightA->Des3, prlFlightA->Ftyp[0]))
		{
			if (bpOnlyNeighborConf)
			{
				CheckFlightOnlyNeighborConf(prlFlightA, NULL);
				CheckFlightOnlyNeighborConf(NULL, prlFlightD);
			}
			else
			{
				CheckFlightInternal2(prlFlightA, NULL);
				CheckFlightInternal2(NULL, prlFlightA);
			}
		}
		else
		{
			if (bpOnlyNeighborConf)
			{
				CheckFlightOnlyNeighborConf(prlFlightA, prlFlightD);				
			}
			else
			{
				CheckFlightInternal2(prlFlightA, prlFlightD);
			}
		}

	}




}




void Konflikte::CheckFlightOnlyNeighborConf(const DIAFLIGHTDATA *prlFlightA, const DIAFLIGHTDATA *prlFlightD)
{

	CString olText;
	CString olPsta;
	CString olPstd;
	CString  olBezA;
	CString  olBezD;

	if(prlFlightA != NULL)
	{
		ResetAllNeighborConfFlight(prlFlightA->Urno, 'A');
		olPsta = prlFlightA->Psta;

		olBezA.Format("%-9s (A): ", prlFlightA->Flno);
		//olBez = CString(prlFlightA->Flno) + CString("  ");
	}


	if(prlFlightD != NULL)
	{
		ResetAllNeighborConfFlight(prlFlightD->Urno, 'D');
		olPstd = prlFlightD->Pstd;

		olBezD.Format("%-9s (D): ", prlFlightD->Flno);
	}

	/////////////////////////////////////////



	///////////////////////////////////////////
	///////////////////////////////////////////
	CCSPtrArray<KonfItem> opKonfList;

	KonfItem *prlKonf;

	int i;
	// get all !!neighbor related!! conflicts of the position
	if (!olPsta.IsEmpty())
	{
		ogSpotAllocation.CheckPstNeighborConf( prlFlightA, prlFlightD, olPsta, &opKonfList);
	 	for(i = 0; i < opKonfList.GetSize(); i++)
		{
 			prlKonf = &opKonfList[i];
			if (prlKonf->KonfId.FUrno > 0)
			{
				CString olBezTmp;
				if (prlKonf->KonfId.FUrno == prlFlightA->Urno)
					olBezTmp = olBezA;
				else
					olBezTmp = olBezD;
				AddKonflikt(prlKonf->KonfId, olBezTmp + CString(prlKonf->Text), MOD_ID_DIA, SUB_MOD_PST);
			}
			else
			{
				AddKonflikt(KonfIdent(prlFlightA->Urno, 'A', prlKonf->KonfId.RelFUrno, prlKonf->KonfId.RelFPart, prlKonf->KonfId.Type), olBezA + CString(prlKonf->Text), MOD_ID_DIA, SUB_MOD_PST);
			}
		}
		opKonfList.DeleteAll();
	}
	else if (!olPstd.IsEmpty())
	{
		ogSpotAllocation.CheckPstNeighborConf( prlFlightA, prlFlightD, olPstd, &opKonfList);
	 	for(i = 0; i < opKonfList.GetSize(); i++)
		{
 			prlKonf = &opKonfList[i];
			if (prlKonf->KonfId.FUrno > 0)
			{
				CString olBezTmp;
				if (prlKonf->KonfId.FUrno == prlFlightD->Urno)
					olBezTmp = olBezD;
				else
					olBezTmp = olBezA;
				AddKonflikt(prlKonf->KonfId, olBezTmp + CString(prlKonf->Text), MOD_ID_DIA, SUB_MOD_PST);
			}
			else
			{
				AddKonflikt(KonfIdent(prlFlightD->Urno, 'D', prlKonf->KonfId.RelFUrno, prlKonf->KonfId.RelFPart, prlKonf->KonfId.Type), 
					olBezD + CString(prlKonf->Text), MOD_ID_DIA, SUB_MOD_PST);
			}
		}
		opKonfList.DeleteAll();
	}


	if(prlFlightA != NULL)
		CheckFlightConf(prlFlightA->Urno, 'A', MOD_ID_DIA);

	if(prlFlightD != NULL)
		CheckFlightConf(prlFlightD->Urno, 'D', MOD_ID_DIA);

	CheckAttentionButton();
}






void Konflikte::CheckFlightInternal2(DIAFLIGHTDATA *prlFlightA, DIAFLIGHTDATA *prlFlightD)
{

	CString olText;
	CString olPsta;
	CString olPstd;
	CString  olBezA;
	CString  olBezD;
	CString  olGta1;
	CString  olGta2;
	CString  olGtd1;
	CString  olGtd2;
	CString  olWro1;
	CString  olWro2;
	CString  olBlt1;
	CString	 olBlt2;

	if(prlFlightA != NULL)
	{
		ResetFlight(prlFlightA->Urno, 'A', MOD_ID_DIA);
		olPsta = prlFlightA->Psta;
		olGta1 = prlFlightA->Gta1;
		olGta2 = prlFlightA->Gta2;
		olBlt1 = prlFlightA->Blt1;
		olBlt2 = prlFlightA->Blt2;

		olBezA.Format("%-9s (A): ", prlFlightA->Flno);
		//olBez = CString(prlFlightA->Flno) + CString("  ");

		if (bmFlightDataChanged)
		{
			CString olExtConfBez;
			olExtConfBez.Format("%sSTA: %s", olBezA, prlFlightA->Stoa.Format("%H:%M %d.%m.%Y"));				
			CheckFlightExtConf(prlFlightA->Urno, 'A', olExtConfBez, ogPosDiaFlightData.pomCflData, MOD_ID_DIA);
		}
	}


	if(prlFlightD != NULL)
	{
		ResetFlight(prlFlightD->Urno, 'D', MOD_ID_DIA);
		olPstd = prlFlightD->Pstd;

		olGtd1 = prlFlightD->Gtd1;
		olGtd2 = prlFlightD->Gtd2;
		olWro1 = prlFlightD->Wro1;
		olWro2 = prlFlightD->Wro2;

		olBezD.Format("%-9s (D): ", prlFlightD->Flno);
		/*
		if(olBez.IsEmpty())
		{
			olBez = CString(prlFlightD->Flno) + CString("  ");
		}
		else
		{
			olBez += " -> ";
			olBez += prlFlightD->Flno;
			olBez += "  ";
		}
		*/

		if (bmFlightDataChanged)
		{
			CString olExtConfBez;
			olExtConfBez.Format("%sSTD: %s", olBezD, prlFlightD->Stod.Format("%H:%M %d.%m.%Y"));				
			CheckFlightExtConf(prlFlightD->Urno, 'D', olExtConfBez, ogPosDiaFlightData.pomCflData, MOD_ID_DIA);
		}
	}

	/////////////////////////////////////////



	///////////////////////////////////////////
	///////////////////////////////////////////
	///////////////////////////////////////////
	///////////////////////////////////////////
	CCSPtrArray<KonfItem> opKonfList;

	KonfItem *prlKonf;

	int i;
	// get all conflicts of the position
	if (!olPsta.IsEmpty())
	{
		ogSpotAllocation.CheckPst( prlFlightA, prlFlightD, olPsta, &opKonfList);
	 	for(i = 0; i < opKonfList.GetSize(); i++)
		{
 			prlKonf = &opKonfList[i];
			if (prlKonf->KonfId.FUrno > 0)
			{
				CString olBezTmp;
				if (prlKonf->KonfId.FUrno == prlFlightA->Urno)
					olBezTmp = olBezA;
				else
					olBezTmp = olBezD;
				AddKonflikt(prlKonf->KonfId, olBezTmp + CString(prlKonf->Text), MOD_ID_DIA, SUB_MOD_PST);
			}
			else
			{
				AddKonflikt(KonfIdent(prlFlightA->Urno, 'A', prlKonf->KonfId.RelFUrno, prlKonf->KonfId.RelFPart, prlKonf->KonfId.Type), olBezA + CString(prlKonf->Text), MOD_ID_DIA, SUB_MOD_PST);
			}
		}
		opKonfList.DeleteAll();
	}
	else if (!olPstd.IsEmpty())
	{
		ogSpotAllocation.CheckPst( prlFlightA, prlFlightD, olPstd, &opKonfList);
	 	for(i = 0; i < opKonfList.GetSize(); i++)
		{
 			prlKonf = &opKonfList[i];
			if (prlKonf->KonfId.FUrno > 0)
			{
				CString olBezTmp;
				if (prlKonf->KonfId.FUrno == prlFlightD->Urno)
					olBezTmp = olBezD;
				else
					olBezTmp = olBezA;
				AddKonflikt(prlKonf->KonfId, olBezTmp + CString(prlKonf->Text), MOD_ID_DIA, SUB_MOD_PST);
			}
			else
			{
				AddKonflikt(KonfIdent(prlFlightD->Urno, 'D', prlKonf->KonfId.RelFUrno, prlKonf->KonfId.RelFPart, prlKonf->KonfId.Type), 
					olBezD + CString(prlKonf->Text), MOD_ID_DIA, SUB_MOD_PST);
			}
		}
		opKonfList.DeleteAll();
	}




	///////////////////////////////////////////
	///////////////////////////////////////////
	///////////////////////////////////////////
	///////////////////////////////////////////

	// get all conflicts of the two gates
	if(!olGta1.IsEmpty())
	{
		ogSpotAllocation.CheckGat( prlFlightA, 'A', olGta1,1, &opKonfList);
	}

	if(!olGta2.IsEmpty())
	{
		ogSpotAllocation.CheckGat( prlFlightA, 'A', olGta2,2, &opKonfList);
	}
	
	for( i = 0; i < opKonfList.GetSize(); i++)
	{
		prlKonf = &opKonfList[i];
		AddKonflikt(prlKonf->KonfId, olBezA + CString(prlKonf->Text), MOD_ID_DIA, SUB_MOD_GAT);
	}
	
	opKonfList.DeleteAll();

	if(!olGtd1.IsEmpty())
	{
		ogSpotAllocation.CheckGat( prlFlightD, 'D', olGtd1,1, &opKonfList);
	}
	if(!olGtd2.IsEmpty())
	{
		ogSpotAllocation.CheckGat( prlFlightD, 'D', olGtd2,2, &opKonfList);
	}

	for( i = 0; i < opKonfList.GetSize(); i++)
	{
		prlKonf = &opKonfList[i];
		AddKonflikt(prlKonf->KonfId, olBezD + CString(prlKonf->Text), MOD_ID_DIA, SUB_MOD_GAT);
	}
	
	opKonfList.DeleteAll();


	// get all conflicts of the waiting room
	if(!olWro1.IsEmpty())
	{
		ogSpotAllocation.CheckWro( prlFlightD, olWro1, 1, &opKonfList);
	}
	if(!olWro2.IsEmpty())
	{
		ogSpotAllocation.CheckWro( prlFlightD, olWro2, 2, &opKonfList);
	}

	for( i = 0; i < opKonfList.GetSize(); i++)
	{
		prlKonf = &opKonfList[i];
		AddKonflikt(prlKonf->KonfId, olBezD + CString(prlKonf->Text), MOD_ID_DIA, SUB_MOD_WRO);
	}
	
	opKonfList.DeleteAll();


	// get all conflicts of the baggage belt
	if(!olBlt1.IsEmpty())
	{
		ogSpotAllocation.CheckBlt( prlFlightA, olBlt1, 1, &opKonfList, 0);
	}
	if(!olBlt2.IsEmpty())
	{
		ogSpotAllocation.CheckBlt( prlFlightA, olBlt2, 2, &opKonfList, 0);
	}

	for( i = 0; i < opKonfList.GetSize(); i++)
	{
		prlKonf = &opKonfList[i];
		AddKonflikt(prlKonf->KonfId, olBezA + CString(prlKonf->Text), MOD_ID_DIA, SUB_MOD_BLT);
	}
	
	opKonfList.DeleteAll();




	CTime olCurr = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olCurr);

	// test additional conflicts that are not related to any resources
	if(prlFlightA != NULL)
	{
		
		CString oTmp;

		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. ??    Departure/Towing before Arrival */

		bool blConf = false;

		DiaCedaFlightData::RKEYLIST *prlRkey;

		prlRkey = ogPosDiaFlightData.GetRotationByRkey(prlFlightA->Rkey);

		if( prlRkey != NULL)
		{
			DIAFLIGHTDATA *prlFlight1;
			DIAFLIGHTDATA *prlFlight2;

			prlRkey->Rotation.Sort(CompareRotationFlight);

			for(int i = 1; i < prlRkey->Rotation.GetSize() ; i++)
			{
				prlFlight1 = &prlRkey->Rotation[i-1];
				prlFlight2 = &prlRkey->Rotation[i];

				if(prlFlight1->Tifa >= prlFlight2->Tifd)
				{
					blConf = true;
					break;
				}

				if( prlFlight1->Adid[0] == 'B' && prlFlight1->Tifa <= prlFlight1->Tifd)
				{
					blConf = true;
					break;
				}

				if(prlFlight2->Adid[0] == 'B' && prlFlight2->Tifa <= prlFlight2->Tifd)
				{
					blConf = true;
					break;
				}


			}


			if(blConf)
			{

				for(int i = 0; i < prlRkey->Rotation.GetSize() ; i++)
				{
					prlFlight1 = &prlRkey->Rotation[i];
					
					//CString("Departure/Towing before Arrival");
					AddKonflikt(KonfIdent(prlFlightA->Urno, 'A', 0, ' ', IDS_STRING1890), olBezA + GetString(IDS_STRING1890), MOD_ID_DIA);
//					AddKonflikt(KonfIdent(prlFlight1->Urno, 'A', 0, ' ', IDS_STRING1890), olBezA + GetString(IDS_STRING1890), MOD_ID_DIA);
//					AddKonflikt(KonfIdent(prlFlight2->Urno, 'A', 0, ' ', IDS_STRING1890), olBezA + GetString(IDS_STRING1890), MOD_ID_DIA);

					CString olBez1;
					olBez1.Format("%-9s (A): ", prlFlight1->Flno);
					AddKonflikt(KonfIdent(prlFlight1->Urno, 'A', 0, ' ', IDS_STRING1890), olBez1 + GetString(IDS_STRING1890), MOD_ID_DIA);

					CString olBez2;
					olBez2.Format("%-9s (A): ", prlFlight2->Flno);
					AddKonflikt(KonfIdent(prlFlight2->Urno, 'A', 0, ' ', IDS_STRING1890), olBez2 + GetString(IDS_STRING1890), MOD_ID_DIA);

				}
			}
		}
 
	}
 	////////////////////////////////////////////////////////////////////////////////////////
	/* Conflict No. ??    Towing can not go off block if the flight is not on block */
	if (prlFlightA != NULL && prlFlightA->Ofbl != TIMENULL && (prlFlightA->Ftyp[0] == 'T' || prlFlightA->Ftyp[0] == 'G'))
	{
 		DiaCedaFlightData::RKEYLIST *prlRkey;

		prlRkey = ogPosDiaFlightData.GetRotationByRkey(prlFlightA->Rkey);

		if( prlRkey != NULL)
		{
			DIAFLIGHTDATA *prlFlight1;
 			for(int i = 0; i < prlRkey->Rotation.GetSize() ; i++)
			{
				prlFlight1 = &prlRkey->Rotation[i];
				if (IsArrivalFlight(prlFlight1->Org3, prlFlight1->Des3, prlFlight1->Ftyp[0]) && 
					prlFlight1->Onbl == TIMENULL)
				{					
					//CString("Towing can not go off block if the flight is not on block!");
					AddKonflikt(KonfIdent(prlFlightA->Urno, 'A', 0, ' ', IDS_STRING1981), olBezA + GetString(IDS_STRING1981), MOD_ID_DIA);
					break;
				}
			}
		}
	}
				

	
	if(prlFlightD != NULL)
	{
		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 10    No Offblock at STD + %d Minutes */
		
		if(bmCurrOfblStod)
		{
			if((prlFlightD->Ofbl == TIMENULL) && (olCurr > prlFlightD->Stod + omCurrOfblStod))
			{
				CString oTmp;
				oTmp.Format(GetString(IDS_STRING1240), omCurrOfblStod.GetTotalMinutes());
				//CString("Kein Offblock bei STD + %d Minuten");
				AddKonflikt(KonfIdent(prlFlightD->Urno, 'D', 0, ' ', IDS_STRING1240), olBezD + oTmp, MOD_ID_DIA);
			}
		}
	}

	if(prlFlightD != NULL)
	{
		CString oTmp;

		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. ??    Departure/Towing before Arrival */

		bool blConf = false;

		DiaCedaFlightData::RKEYLIST *prlRkey;

		prlRkey = ogPosDiaFlightData.GetRotationByRkey(prlFlightD->Rkey);

		if( prlRkey != NULL)
		{
			DIAFLIGHTDATA *prlFlight1;
			DIAFLIGHTDATA *prlFlight2;

			prlRkey->Rotation.Sort(CompareRotationFlight);

			for(int i = 1; i < prlRkey->Rotation.GetSize() ; i++)
			{
				prlFlight1 = &prlRkey->Rotation[i-1];
				prlFlight2 = &prlRkey->Rotation[i];

				if(prlFlight1->Tifa >= prlFlight2->Tifd)
				{
					blConf = true;
					break;
				}

				if( prlFlight1->Adid[0] == 'B' && prlFlight1->Tifa <= prlFlight1->Tifd)
				{
					blConf = true;
					break;
				}

				if(prlFlight2->Adid[0] == 'B' && prlFlight2->Tifa <= prlFlight2->Tifd)
				{
					blConf = true;
					break;
				}


			}


			if(blConf)
			{

				for(int i = 0; i < prlRkey->Rotation.GetSize() ; i++)
				{
					prlFlight1 = &prlRkey->Rotation[i];
					
					//CString("Departure/Towing before Arrival");
					AddKonflikt(KonfIdent(prlFlightD->Urno, 'D', 0, ' ', IDS_STRING1890), olBezD + GetString(IDS_STRING1890), MOD_ID_DIA);
//					AddKonflikt(KonfIdent(prlFlight1->Urno, 'D', 0, ' ', IDS_STRING1890), olBezD + GetString(IDS_STRING1890), MOD_ID_DIA);
//					AddKonflikt(KonfIdent(prlFlight2->Urno, 'D', 0, ' ', IDS_STRING1890), olBezD + GetString(IDS_STRING1890), MOD_ID_DIA);

					CString olBez1;
					olBez1.Format("%-9s (D): ", prlFlight1->Flno);
					AddKonflikt(KonfIdent(prlFlight1->Urno, 'D', 0, ' ', IDS_STRING1890), olBez1 + GetString(IDS_STRING1890), MOD_ID_DIA);

					CString olBez2;
					olBez2.Format("%-9s (D): ", prlFlight2->Flno);
					AddKonflikt(KonfIdent(prlFlight2->Urno, 'D', 0, ' ', IDS_STRING1890), olBez2 + GetString(IDS_STRING1890), MOD_ID_DIA);
				}
			}
		}
	}


	CheckCommonConflicts(NULL, prlFlightA, MOD_ID_DIA, SUB_MOD_NOTIMP, olCurr, olBezA);
	CheckCommonConflicts(NULL, prlFlightD, MOD_ID_DIA, SUB_MOD_NOTIMP, olCurr, olBezD);



	if(prlFlightA != NULL)
		CheckFlightConf(prlFlightA->Urno, 'A', MOD_ID_DIA);

	if(prlFlightD != NULL)
		CheckFlightConf(prlFlightD->Urno, 'D', MOD_ID_DIA);
	
	CheckAttentionButton();

}







