// Report16TableViewer.cpp : implementation file
// 
//	Inventar der Flugzeuge in Hannover

#include <stdafx.h>
#include <ReportSelectDlg.h>
#include <Report16TableViewer.h>
#include <CcsGlobl.h>
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include <CedaBasicData.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif



static int CompareRegn(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2)
{

	if(strcmp( (**e1).Regn, (**e2).Regn) == 0)
	{
		return (((**e1).Tifa == (**e2).Tifa) ? 0 : (((**e1).Tifa > (**e2).Tifa) ? 1 : -1));
	}
	else
	{
		return strcmp( (**e1).Regn, (**e2).Regn);

	}
}





/////////////////////////////////////////////////////////////////////////////
// Report16TableViewer
//

Report16TableViewer::Report16TableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo)
{
	pcmSelect = pcpInfo;
	pomData = popData;
    pomTable = NULL;
}

Report16TableViewer::~Report16TableViewer()
{
    DeleteAll();
}

void Report16TableViewer::Attach(CTable *popTable)
{
    pomTable = popTable;
}



void Report16TableViewer::ChangeViewTo(const char *pcpViewName)
{

    pomTable->ResetContent();
    DeleteAll();    

	UpdateDisplay();
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void Report16TableViewer::UpdateDisplay()
{
	pomTable->ResetContent();

	pomTable->SetHeaderFields(GetString(IDS_STRING1190));
	pomTable->SetFormatList("4|14|4|7|8|5|32"); 

	ROTATIONDLGFLIGHTDATA *prlFlight;
	ROTATIONDLGFLIGHTDATA *prlFlightNext;

	pomData->Sort(CompareRegn);	//Sortierung REGN, TIFA (aufst.)

	bool blLook = true;
  	int ilCount = pomData->GetSize();
	
	imCount = 0;	// wird in ShowLine() inkrementiert

 
	// Schleife �ber alle Regn S�tze, nur der letzte Regn-Satz interessiert ( => Sortierung Tifa aufsteigend! )
	for (int ilLc = 0; ilLc < ilCount ; ilLc++)
	{
		prlFlight = &(*pomData)[ilLc];

		blLook = false;

		if(ilLc < ilCount - 1) 
			prlFlightNext = &pomData->GetAt(ilLc + 1);
		else
			prlFlightNext = NULL;


		if(prlFlightNext == NULL)
		{
			blLook = true;
		}
		else
		{
			if(CString(prlFlightNext->Regn) != CString(prlFlight->Regn))
				blLook = true;
		}

		if(blLook)
		{

//			if( (CString(prlFlight->Adid) == "A") || ((CString(prlFlight->Adid) == "B") && (prlFlight->Ofbl != TIMENULL)))
			// ge�ndert hbe am 16/9/98: ( Onbl != TIMENULL ) => Rundflug ist schon wieder da !
			if( (CString(prlFlight->Adid) == "A") || ((CString(prlFlight->Adid) == "B") && (prlFlight->Onbl != TIMENULL)))
				ShowLine(prlFlight);

		}

	}
	pomTable->DisplayTable();
}


void Report16TableViewer::ShowLine(ROTATIONDLGFLIGHTDATA *prlFlight)
{
	CString olComplete;
	CTime olTifa;
	char buffer[64];

	omData.Add(prlFlight);

	imCount++;

	itoa(imCount, buffer, 10);


	olTifa = prlFlight->Tifa;
	if(bgReportLocal)
		ogBasicData.UtcToLocal(olTifa);


	olComplete = CString(buffer) + "|";
	olComplete += CString(prlFlight->Regn) + "|";
	olComplete += CString(prlFlight->Act5) + "|";
	olComplete += CString(prlFlight->Psta) + "|";
	olComplete += CString(olTifa.Format("%d.%m.%y")) + "|"; 
	olComplete += CString(olTifa.Format("%H:%M")) + "|";

	pomTable->InsertTextLine(-1, olComplete, NULL);	//ilLineno = -1 End


}

//-----------------------------------------------------------------------------------------------

void Report16TableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

void Report16TableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);

	omData.RemoveAll();
}


//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void Report16TableViewer::GetPrintHeader()
{
		
	omPrintHeadHeaderArray.DeleteAll();  
	
	TABLE_HEADER_COLUMN *prlHeader[8];
	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 20; 
	prlHeader[0]->Font = &ogCourier_Bold_10;
	prlHeader[0]->Text = GetString(IDS_STRING1191);//CString("lfd");

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 100; 
	prlHeader[1]->Font = &ogCourier_Bold_10;
	prlHeader[1]->Text = GetString(IDS_STRING996);//CString("Registrierung");

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 45; 
	prlHeader[2]->Font = &ogCourier_Bold_10;
	prlHeader[2]->Text = GetString(IDS_STRING337);//CString("Typ");

	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 70; 
	prlHeader[3]->Font = &ogCourier_Bold_10;
	prlHeader[3]->Text = GetString(IDS_STRING1025);//CString("Position");

	//Abflug
	
	prlHeader[4] = new TABLE_HEADER_COLUMN;
	prlHeader[4]->Alignment = COLALIGN_CENTER;
	prlHeader[4]->Length = 80; 
	prlHeader[4]->Font = &ogCourier_Bold_10;
	prlHeader[4]->Text = GetString(IDS_STRING1192);//CString("Landedatum");

	prlHeader[5] = new TABLE_HEADER_COLUMN;
	prlHeader[5]->Alignment = COLALIGN_CENTER;
	prlHeader[5]->Length = 75; 
	prlHeader[5]->Font = &ogCourier_Bold_10;
	prlHeader[5]->Text = GetString(IDS_STRING1193);//CString("Landezeit");

	prlHeader[6] = new TABLE_HEADER_COLUMN;
	prlHeader[6]->Alignment = COLALIGN_CENTER;
	prlHeader[6]->Length = 180; 
	prlHeader[6]->Font = &ogCourier_Bold_10;


	for(int ili = 0; ili < 7; ili++)
	{
		omPrintHeadHeaderArray.Add( prlHeader[ili]);
	}

}

//-----------------------------------------------------------------------------------------------

bool Report16TableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;
	double dgCCSPrintFactor = 2.7 ;
	char pclHeader[100];

	sprintf(pclHeader, GetString(IDS_STRING1194), pcmSelect);
	CString olTableName(pclHeader);

	GetPrintHeader();
//	pomPrint->PrintUIFHeader(omTableName,CString(pclHeader),pomPrint->imFirstLine-10);
	pomPrint->PrintUIFHeader("",CString(pclHeader),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omPrintHeadHeaderArray.GetSize();

	for(int i=0;i<ilSize;i++)
	{
			rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omPrintHeadHeaderArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}
//-----------------------------------------------------------------------------------------------
void Report16TableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	char pclFooter[100];

	sprintf(pclFooter, GetString(IDS_STRING1194), (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));

	omFooterName = pclFooter;
	CString olTableName = GetString(IDS_STRING1195);
	int ilOrientation = PRINT_PORTRAET;
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	if (pomPrint != NULL)
	{

		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = olTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omData.GetSize();  
  			//int ilLines = pomData->GetSize();
			olFooter1.Format("%s",omFooterName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(i ,true,i);
				}
				else
				{
					PrintTableLine(i ,false,i);
				}
			}
			olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}

		omPrintHeadHeaderArray.DeleteAll();
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		delete pomPrint;
		pomPrint = NULL;
	}

}

//---------------------------------------------------------------------------------------

//bool Report16TableViewer::PrintTableLine(REPORT16TABLE_LINEDATA *prlUrno, bool bpLastLine)
bool Report16TableViewer::PrintTableLine(int ipLine, bool bpLastLine, int ipCnt)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	double dgCCSPrintFactor = 2.7 ;
	rlElement.pFont = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omPrintHeadHeaderArray.GetSize();

	ROTATIONDLGFLIGHTDATA *prlFlight = &omData[ipLine];
	if(prlFlight != NULL)
	{

		for(int i=0;i<ilSize;i++)
		{
			rlElement.Length = (int)(omPrintHeadHeaderArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_FRAMETHIN;
			rlElement.FrameRight = PRINT_NOFRAME;

			//ROTATIONDLGFLIGHTDATA *prlFlight = omCedaFlightData.GetFlightByUrno(prlUrno->Urno);
			switch(i)
			{
				case 0:
				{
					char clbuffer[10];
					ipCnt++;
					sprintf(clbuffer, "%d", ipCnt);
					rlElement.FrameLeft  = PRINT_FRAMETHIN;
					rlElement.Text	= clbuffer;
				}
				break;
				case 1:
				{
						rlElement.Text		= prlFlight->Regn;
				}
				break;
				case 2:
				{
						rlElement.Text		= prlFlight->Act5;
				}
				break;
				case 3:
				{
						rlElement.Text		= prlFlight->Psta;
				}
				break;
				case 4:
				{
					CTime olLocal;
						olLocal = prlFlight->Tifa;
						if(bgReportLocal)
							ogBasicData.UtcToLocal(olLocal);
						rlElement.Text		= olLocal.Format("%d.%m.%y");
				}
				break;
				case 5:
				{
					CTime olLocal;
						olLocal = prlFlight->Tifa;
						if(bgReportLocal)
							ogBasicData.UtcToLocal(olLocal);
						rlElement.Text		= olLocal.Format("%H:%M");
				}
				break;
				case 6:
				{
					rlElement.Text		= "";
					rlElement.FrameRight = PRINT_FRAMETHIN;
				}
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}  // (prlFlight != NULL)
			
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll(); 
	return true;

}

//-----------------------------------------------------------------------------------------------


// Drucken in Datei
bool Report16TableViewer::PrintPlanToFile(char *opTrenner)
{
	ofstream of;

	char* tmpvar;
	tmpvar = getenv("TMP");
	if (tmpvar == NULL)
		tmpvar = getenv("TEMP");

	if (tmpvar == NULL)
		return false;

	CString olFileName = omTableName;
	olFileName.Remove('*');
	olFileName.Remove('.');
	olFileName.Remove(':');
	olFileName.Remove('/');
	olFileName.Replace(" ", "_");
	
	char pHeader[256];
	strcpy (pHeader, tmpvar);
	CString path = pHeader;
	omFileName =  path + "\\" + olFileName + ".csv";

//	of.open( GetString(IDS_STRING1386), ios::out);
	of.open( omFileName, ios::out);

	int ilwidth = 1;

	of  << setw(ilwidth) << omTableName << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;


	of  << setw(ilwidth) << GetString(IDS_STRING1191)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING996)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING337)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING1025)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING1192)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING1193)
		<< endl; 

	char clbuffer[10];
	int ilCount = omData.GetSize();
	
	for(int i = 0; i < ilCount; i++)
	{
		int ilCnt = i + 1;
		sprintf(clbuffer, "%d", ilCnt);

		ROTATIONDLGFLIGHTDATA *prlFlight = &omData[i];
		if(prlFlight != NULL)
		{
			CTime olTifa = prlFlight->Tifa;
			if(bgReportLocal)
				ogBasicData.UtcToLocal(olTifa);
			of.setf(ios::left, ios::adjustfield);
			of   << setw(1) << clbuffer 
				 << setw(1) << opTrenner 
				 << setw(1) << prlFlight->Regn  
				 << setw(1) << opTrenner 
				 << setw(1) << prlFlight->Act5
				 << setw(1) << opTrenner 
				 << setw(1) << prlFlight->Psta
				 << setw(1) << opTrenner 
				 << setw(1) << olTifa.Format("%d.%m.%y")
				 << setw(1) << opTrenner 
				 << setw(1) << olTifa.Format("%H:%M")
				 << endl;
		}

	}
	of.close();
	return true;

/*
		char pclHeader[120];
		ofstream of;
		of.open( pcpDefPath, ios::out);
		sprintf(pclHeader, GetString(IDS_STRING1196) , pcmSelect);
		of << pclHeader 
			<< CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M") << endl << endl;

		of  << GetString(IDS_STRING1191) << ". "
			<< GetString(IDS_STRING996)  << " " 
			<< GetString(IDS_STRING337)  << "    " 
			<< GetString(IDS_STRING1025) << "  " 
			<< GetString(IDS_STRING1192) << "  " 
			<< GetString(IDS_STRING1193) << "  " 
			<< " " << endl; 

		of << "-----------------------------------------------------------------------------------------" << endl;

		char clbuffer[10];
		int ilCount = omData.GetSize();
		
		for(int i = 0; i < ilCount; i++)
		{
			int ilCnt = i + 1;
			sprintf(clbuffer, "%d", ilCnt);

			ROTATIONDLGFLIGHTDATA *prlFlight = &omData[i];
			if(prlFlight != NULL)
			{
				CTime olTifa = prlFlight->Tifa;
				if(bgReportLocal)
					ogBasicData.UtcToLocal(olTifa);
				of.setf(ios::left, ios::adjustfield);
				of   << setw(5) << clbuffer 
					 << setw(16) << prlFlight->Regn  
					 << setw(8) << prlFlight->Act5
					 << setw(10) << prlFlight->Psta
					 << setw(12) << olTifa.Format("%d.%m.%y")
					 << setw(11) << olTifa.Format("%H:%M") << endl;
			}

		}
		of.close();
*/
}

