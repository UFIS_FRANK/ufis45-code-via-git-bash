// CheckInDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <CcaCommonTableDlg.h>
#include <CcaCommonDlg.h>
#include <CcaCommonSearchDlg.h>
#include <CedaBasicData.h>
#include <CedaCcaData.h>
#include <PrivList.h>
#include <CCSGlobl.h>
#include <Utils.h>

// nur TEST !
//#include "CcaCommonPropertySheet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CCcaCommonTableDlg 


CCcaCommonTableDlg::CCcaCommonTableDlg(CWnd* pParent /*=NULL*/, CTime opFrom , CTime opTo)
	: CDialog(CCcaCommonTableDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCcaCommonTableDlg)
	//}}AFX_DATA_INIT
	pomCheckInViewer = NULL;

	omFrom = opFrom;
	omTo = opTo;

    CDialog::Create(CCcaCommonTableDlg::IDD, pParent);


}

void CCcaCommonTableDlg::SetTimeFrame(CTime opFrom , CTime opTo )
{
	omFrom = opFrom;
	omTo = opTo;
	Reload();
}

CCcaCommonTableDlg::~CCcaCommonTableDlg()
{
	if (pomCheckInViewer != NULL)
	{
		delete pomCheckInViewer;
	}
	delete pomCheckInTable ;
    omUrnoMap.RemoveAll();
}


void CCcaCommonTableDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCcaCommonTableDlg)
	DDX_Control(pDX, IDC_KOPIEREN, m_CB_Kopieren);
	DDX_Control(pDX, IDC_ENTF, m_CB_Entf);
	DDX_Control(pDX, IDC_EINFUEGEN, m_CB_Einfuegen);
	DDX_Control(pDX, IDC_COMBO_ANSICHT, m_CC_Ansicht);
	DDX_Control(pDX, IDC_BUTTON_ANSICHT, m_CB_Ansicht);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCcaCommonTableDlg, CDialog)
	//{{AFX_MSG_MAP(CCcaCommonTableDlg)
	ON_BN_CLICKED(IDC_AENDERN, OnAendern)
	ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK,	OnTableLButtonDblclk)
	ON_BN_CLICKED(IDC_EINFUEGEN, OnEinfuegen)
	ON_BN_CLICKED(IDC_ENTF, OnEntf)
	ON_BN_CLICKED(IDC_KOPIEREN, OnKopieren)
	ON_BN_CLICKED(IDC_BUTTON_ANSICHT, OnButtonAnsicht)
	ON_CBN_SELCHANGE(IDC_COMBO_ANSICHT, OnSelchangeComboAnsicht)
	ON_WM_CLOSE()
    ON_MESSAGE(WM_TABLE_LBUTTONDOWN, OnTableLButtonDown)
    ON_MESSAGE(WM_TABLE_RETURN_PRESSED, OnTableReturnPressed)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CCcaCommonTableDlg 

BOOL CCcaCommonTableDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();


    // calculate the window height
    //CRect rectScreen(0, 0, ::GetSystemMetrics(SM_CXSCREEN), 65); //  ::GetSystemMetrics(SM_CYSCREEN));
	//CWnd::MoveWindow(&rectScreen, TRUE);

	CRect rect;
    GetClientRect(&rect);
	//  f�r Viewer
    int ilDialogBarHeight = rect.bottom - rect.top;

	// extend dialog window to current screen width
    rect.left = 0;
    rect.top = 65;
    //rect.right = 1024; ganzer Bildschirm
    rect.right = 900;
    //rect.bottom = 768; ganzer Bildschirm
    rect.bottom = 600;
    MoveWindow(&rect);


	GetClientRect(&rect);

	pomCheckInTable = new CCSTable;
//	pomCheckInTable->imSelectMode = 0;
	pomCheckInTable->SetSelectMode(LBS_MULTIPLESEL | LBS_EXTENDEDSEL);

	rect.top = rect.top + ilDialogBarHeight;

	rect.bottom = rect.bottom - 1;

	rect.left += 2;
	rect.right -= 2;

	//rect.InflateRect(3,3);     // hiding the CTable window border

	pomCheckInTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom);


	pomCheckInViewer = new CcaCommonTableViewer();
	pomCheckInViewer->Attach(pomCheckInTable);

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	//pomCheckInViewer->ChangeViewTo("<Default>");
	pomCheckInViewer->UpdateDisplay();
	
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	bgIsDialogOpen = false;

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

////////////////////////////////////////////////////////////////////////////
// UpdateView
//
void CCcaCommonTableDlg::UpdateView()
{

	AfxGetApp()->DoWaitCursor(1);

    pomCheckInViewer->ChangeViewTo(CString("<Default>"));
	
	AfxGetApp()->DoWaitCursor(-1);

}

void CCcaCommonTableDlg::OnAendern() 
{
	if(!bgIsDialogOpen)
	{
		bgIsDialogOpen = true;
		int ilLineNo = pomCheckInTable->GetCurSel();
		if (ilLineNo == -1)
		{
			MessageBox( GetString(IDS_STRING909), GetString(IDS_WARNING),MB_ICONEXCLAMATION);
		}
		else
		{
			CCADATA *prlCCA = pomCheckInViewer->GetCcaByLineNo(ilLineNo);
			if (prlCCA != NULL)
			{

				pogCommonCcaDlg->NewData(prlCCA);
			}
		}
		bgIsDialogOpen = false;
	}
}

void CCcaCommonTableDlg::OnEinfuegen() // aus Stammdaten.cpp
{

	CCADATA rlCCA;
	
	pogCommonCcaDlg->NewData(&rlCCA);
	
}



void CCcaCommonTableDlg::OnEntf()
{
	if(!bgIsDialogOpen)
	{
		int ilItems[3000];
		int ilAnz = 3000;

		CListBox *polLB = pomCheckInTable->GetCTableListBox();
		if (polLB == NULL)
			return;

		ilAnz = MyGetSelItems(*polLB, ilAnz, ilItems);
		if(ilAnz <= 0)
		{
			return;
		}

		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

		bgIsDialogOpen = true;

		for(int i = ilAnz-1; i >= 0; i--)
		{
			if (i==ilAnz-1 && IDNO == MessageBox( GetString(IDS_STRING1528),GetString(ST_FRAGE),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
			{
				bgIsDialogOpen = false;
				return;
			}

			CCADATA *prlCCA = pomCheckInViewer->GetCcaByLineNo(ilItems[i]);
			if (prlCCA != NULL)
			{
				CCADATA rlCCA = *prlCCA;
				pomCheckInViewer->DeleteCca(rlCCA.Urno);
			}
		}

		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

		Reload();

		bgIsDialogOpen = false;

/*
		bgIsDialogOpen = true;
		int ilLineNo = pomCheckInTable->GetCurSel();
		if (ilLineNo == -1)
		{
			MessageBox( GetString(IDS_STRING909), GetString(IDS_WARNING),MB_ICONEXCLAMATION);
		}
		else
		{
			CCADATA *prlCCA = pomCheckInViewer->GetCcaByLineNo(ilLineNo);
			if (prlCCA != NULL)
			{
				CCADATA rlCCA = *prlCCA;
				if (IDYES == MessageBox( GetString(IDS_STRING1528),GetString(ST_FRAGE),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
				{
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
					pomCheckInViewer->DeleteCca(rlCCA.Urno);
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
				}
			}
		}
		bgIsDialogOpen = false;
*/

	}
}

void CCcaCommonTableDlg::OnKopieren() 
{

	
	
	if(!bgIsDialogOpen)
	{
		bgIsDialogOpen = true;
		int ilLineNo = pomCheckInTable->GetCurSel();
		if (ilLineNo == -1)
		{
			MessageBox( GetString(IDS_STRING909), GetString(IDS_WARNING),MB_ICONEXCLAMATION);
		}
		else
		{
			CCADATA *prlOldCCA = pomCheckInViewer->GetCcaByLineNo(ilLineNo);
			if (prlOldCCA != NULL)
			{
				CCADATA rlCCA = *prlOldCCA;
				rlCCA.Urno = 0;

				pogCommonCcaDlg->NewData(&rlCCA);
			}
		}
		bgIsDialogOpen = false;
	}
}

void CCcaCommonTableDlg::OnButtonAnsicht() 
{
		char pclSelect[1024];

		CCcaCommonSearchDlg *olCcaCommonSearchDlg = new CCcaCommonSearchDlg(this, pclSelect, &omFrom, &omTo);

		if (olCcaCommonSearchDlg->DoModal() == IDOK)
		{
			
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

			omSelection = CString(pclSelect);
			pomCheckInViewer->ReadAllFlights(CString(pclSelect));  

			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

		}
		delete olCcaCommonSearchDlg;
	
}

void CCcaCommonTableDlg::Reload()
{

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	pomCheckInViewer->ReadAllFlights(omSelection);  

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));


}






void CCcaCommonTableDlg::ShowAnsicht() 
{
//"CKIC,CKIT,FLNU,CKBA,CKEA,CTYP"

	//if(ogPrivList.GetStat("CHECKIN_CB_Ansicht") != '1')
	//	return;



	/* vorl�ufig auskommentiert 
	CcaCommonTablePropertySheet olDlg("CCACOMMON", this, pomCheckInViewer);

	if(olDlg.DoModal() != IDCANCEL)
	{
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		UpdateWindow();
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		pomCheckInViewer->ChangeViewTo(pomCheckInViewer->GetViewName());
		UpdateComboAnsicht();
	}
	else
	{
		UpdateWindow();
		UpdateComboAnsicht();
	}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	*/
}



void CCcaCommonTableDlg::OnSelchangeComboAnsicht() 
{
	CComboBox *polView = (CComboBox *)GetDlgItem(IDC_COMBO_ANSICHT);

	int ilItemID;
	CString olView;

	if ((ilItemID = polView->GetCurSel()) != CB_ERR)
	{
		polView->GetLBText(ilItemID, olView);
 
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		pomCheckInViewer->ChangeViewTo(olView);
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}
	
}



void CCcaCommonTableDlg::UpdateComboAnsicht()
{
	CStringArray olArray;
	CStringArray olStrArr;
	CComboBox *polCB = (CComboBox *)GetDlgItem(IDC_COMBO_ANSICHT);
	polCB->ResetContent();
	pomCheckInViewer->GetViews(olStrArr);
	for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
	{
		polCB->AddString(olStrArr[ilIndex]);
	}
	CString olViewName = pomCheckInViewer->GetViewName();

	ilIndex = polCB->FindString(-1,olViewName);
		
	if (ilIndex != CB_ERR)
	{
		polCB->SetCurSel(ilIndex);
	}
} 


//------------------------------------------------------------------------------------------------------

LONG CCcaCommonTableDlg::OnTableLButtonDblclk(UINT wParam, LONG lParam) 
{
	if(!bgIsDialogOpen)
	{
		OnAendern();
	}
	return 0L;
}


void CCcaCommonTableDlg::OnClose() 
{
	pomCheckInViewer->ClearAll();
	ShowWindow(SW_HIDE);
}

LONG CCcaCommonTableDlg::OnTableLButtonDown(UINT wParam, LONG lParam)
{
	return 0L;
}


LONG CCcaCommonTableDlg::OnTableReturnPressed( UINT wParam, LPARAM lParam)
{
	if(!bgIsDialogOpen)
	{
		OnAendern();
	}
	return 0L;
}
