// BackGround.cpp : implementation file
//

#include <stdafx.h>
#include <FPMS.h>
#include <BackGround.h>
#include <CCSGlobl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBackGround dialog


CBackGround::CBackGround(CWnd* pParent /*=NULL*/) 
	: CDialog(CBackGround::IDD, pParent)
{
	//{{AFX_DATA_INIT(CBackGround)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CBackGround::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBackGround)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CBackGround, CDialog)
	//{{AFX_MSG_MAP(CBackGround)
	ON_WM_CREATE()
    ON_WM_PAINT()
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBackGround message handlers

void CBackGround::OnPaint() 
{
    CPaintDC dc(this); // device context for painting
    
    // Load the backgroundbitmap
	
	dc.SetBkMode(TRANSPARENT);

    CBrush olBrush( RGB( 192, 192, 192 ) );
    CBrush *polOldBrush = dc.SelectObject( &olBrush );
    CRect olRect;
    GetClientRect( &olRect );
    dc.FillRect( &olRect, &olBrush );
    dc.SelectObject( polOldBrush );
    


	dc.SetTextColor(RGB(100,100,100));
	
	CDC olDC;
    olDC.CreateCompatibleDC( &dc );
    CBitmap olBitmap;
    //olBitmap.LoadBitmap( IDB_POPS );
    olBitmap.LoadBitmap( IDB_UFISLOGO );
	CBitmap *polOldBitmap = olDC.SelectObject( &olBitmap );
 
    //dc.BitBlt( 287,213 , 460, 365, &olDC, 0, 0, SRCCOPY );

    dc.BitBlt( 287,213 , 460, 365, &olDC, 0, 0, SRCCOPY );

	olDC.SelectObject(polOldBitmap);
    olDC.DeleteDC( );

	CFont *pomOldFont;

	
	/*
	if((ogCustomer == "BVD") && (ogAppName == "POPS"))
	{
		pomOldFont = dc.SelectObject(&ogTimesNewRoman_30);
		
   		dc.TextOut(300,430,"POPS");

		dc.SelectObject(&ogTimesNewRoman_16);

   		dc.TextOut(300,480,"Passenger Service and Operations Planning System");

		dc.SelectObject(&ogTimesNewRoman_12);

   		dc.TextOut(300,505,"Flughafen Frankfurt Main AG - Bodenverkehrsdienste BVD/P");

  		dc.SelectObject(&ogTimesNewRoman_9);

   		dc.TextOut(300,548,"UFIS � ist ein eingetragenes Warenzeichen der ABB Airport Technologies GmbH");
	}
	*/

	pomOldFont = dc.SelectObject(&ogTimesNewRoman_30);
	
   	dc.TextOut(300,430,"FIPS");

	dc.SelectObject(&ogTimesNewRoman_16);

   	dc.TextOut(300,480,"Flight Information Processing System");

	dc.SelectObject(&ogTimesNewRoman_12);

	CString olVers = CString("Version ") + CString(pcgVersion);

   	dc.TextOut(300,505, olVers);

	if (strcmp(pcgHome, "DXB") == 0)
		dc.TextOut(300,525,"Siemens Take Off Vision� based on ABB UFIS�");

  	dc.SelectObject(&ogTimesNewRoman_9);

   	dc.TextOut(300,548,"UFIS � is a registered trademark of ABB Airport Technologies GmbH");

	// Display compiling time!
	CString olBuildTime;
	olBuildTime.Format("Built: %s %s", __DATE__, __TIME__);
	dc.TextOut(0, GetSystemMetrics(SM_CYSCREEN) - 45, olBuildTime);
 
	// Display server name
	CString olServerName(ogCommHandler.pcmRealHostName);
	CSize olTextExtent = dc.GetTextExtent(olServerName, olServerName.GetLength());
	dc.TextOut(GetSystemMetrics(SM_CXSCREEN) - olTextExtent.cx - 10, GetSystemMetrics(SM_CYSCREEN) - 45, olServerName);


	dc.SelectObject(pomOldFont);

    // Do not call CDialog::OnPaint() for painting messages 
} 

int CBackGround::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	
	return 0;
}


BOOL CBackGround::Create(CWnd* pParentWnd, UINT nID) 
{

	return CDialog::Create(IDD, pParentWnd);
}


BOOL CBackGround::OnInitDialog() 
{
	CDialog::OnInitDialog();
	CRect olRect;
	
	olRect.bottom = ::GetSystemMetrics(SM_CYMAXIMIZED);
	olRect.right  = ::GetSystemMetrics(SM_CXMAXIMIZED);
	olRect.top = 0;
	olRect.left = 0;

	MoveWindow(&olRect, TRUE);
	ShowWindow(SW_SHOW);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CBackGround::OnClose() 
{
}


void CBackGround::OnCancel() 
{
}


void CBackGround::OnOK() 
{
}


