#ifndef __REPORT_FLIGHT_GPU_TABLE_VIEWER_H__
#define __REPORT_FLIGHT_GPU_TABLE_VIEWER_H__

#include <stdafx.h>
#include <RotationDlgCedaFlightData.h>
#include <ReportXXXCedaData.h>
#include <CedaGpaData.h>

#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct REPORTFLIGHTGPUTABLE_LINEDATA
{
	long Urno;
 	
	char iType;				// 'A' or 'D' for arrival/departure

	CString		Type;
	CString		Flno;
	CTime		Time;
	CTime		BestTime;
	CString		Act;
	CString		Regn;
	CString		Pos;
	CTime		Gpa1Begin;
	CTime		Gpa1End;
	CTime		Gpa2Begin;
	CTime		Gpa2End;
	CTime		Gpa3Begin;
	CTime		Gpa3End;
	REPORTFLIGHTGPUTABLE_LINEDATA()
	{ 
		Urno = 0;
		iType = ' ';

		Type.Empty() ;		
		Flno.Empty() ;
		Time = TIMENULL;
		BestTime = TIMENULL;
		Act.Empty() ;
		Regn.Empty() ;
		Pos.Empty() ;
		Gpa1Begin = TIMENULL;
		Gpa1End = TIMENULL;
		Gpa2Begin = TIMENULL;
		Gpa2End = TIMENULL;
		Gpa3Begin = TIMENULL;
		Gpa3End = TIMENULL;
	}
 
};

/////////////////////////////////////////////////////////////////////////////
// ReportFlightGPUTableViewer

class ReportFlightGPUTableViewer : public CViewer
{
// Constructions
public:
    ReportFlightGPUTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData,
		                    char *pcpInfo = NULL, char *pcpSelect = NULL);
    ~ReportFlightGPUTableViewer();

    void Attach(CCSTable *popAttachWnd);
	void SetParentDlg(CDialog* ppDlg);
    virtual void ChangeViewTo(const char *pcpViewName);
	void PrintPlanToFile(char *pcpDefPath);
	void PrintTableView(void);

// Internal data processing routines
private:
    void MakeLines();
	void MakeLine(ROTATIONDLGFLIGHTDATA *prpFlight);
	bool MakeLineData(const ROTATIONDLGFLIGHTDATA &rrpFlight, REPORTFLIGHTGPUTABLE_LINEDATA &rpLine);		
	void MakeColList(const REPORTFLIGHTGPUTABLE_LINEDATA &rrlLine, CCSPtrArray<TABLE_COLUMN> &olColList);	
  	int  CompareFlight(REPORTFLIGHTGPUTABLE_LINEDATA *prpFlight1, REPORTFLIGHTGPUTABLE_LINEDATA *prpFlight2);
	int CreateLine(REPORTFLIGHTGPUTABLE_LINEDATA &rpLine);
 
// Operations
public:
	void DeleteAll();
 	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	void DrawHeader();
	int GetFlightCount();


// Attributes used for filtering condition
private:
	bool bmIsFromSearch;

// Attributes
private:
    CCSTable *pomTable;
	CedaGpaData omGpaData;
	CDialog* pomParentDlg;
	CCSPtrArray<ROTATIONDLGFLIGHTDATA> *pomData;
 	int imFlightCount;


private:
    CCSPtrArray<REPORTFLIGHTGPUTABLE_LINEDATA> omLines;
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void GetHeader(void); 
	bool PrintTableLine(REPORTFLIGHTGPUTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader(void);
	CCSPtrArray <TABLE_HEADER_COLUMN> omPrintHeadHeaderArray;
	CCSPrint *pomPrint;
	CString omTableName;
	char *pcmInfo;
	char *pcmSelect;
	CString omFooterName;


};

#endif //__ReportFlightGPUTableViewer_H__
