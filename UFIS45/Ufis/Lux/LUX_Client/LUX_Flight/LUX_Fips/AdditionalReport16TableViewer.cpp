// AdditionalReport16TableViewer.cpp : implementation file
// 
// check-in counter using rate failure rate staistic according to time


#include <stdafx.h>
#include <resrc1.h>
#include <CcsGlobl.h>
#include <CedaBasicData.h>
#include <AdditionalReport16TableViewer.h>
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include <BasicData.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// AdditionalReport16TableViewer
//


AdditionalReport16TableViewer::AdditionalReport16TableViewer(char *pcpInfo, char *pcpSelect, 
															 CTime opMin, CTime opMax)
{

	pcmInfo = pcpInfo;
	pcmSelect = pcpSelect;

	opMax = opMin;

	CString olMinStr = opMin.Format( "%Y%m%d000000" );
	CString olMaxStr = opMax.Format( "%Y%m%d235959" );
	
	if(bgReportLocal)
	{
		CTime olUtcMinDate = DBStringToDateTime(olMinStr);
		CTime olUtcMaxDate = DBStringToDateTime(olMaxStr);

		ogBasicData.LocalToUtc(olUtcMinDate);
		ogBasicData.LocalToUtc(olUtcMaxDate);

		olMinStr = olUtcMinDate.Format( "%Y%m%d%H%M%S" );
		olMaxStr = olUtcMaxDate.Format( "%Y%m%d%H%M%S" );
	}

	pomParentDlg = NULL;

	omMin = DBStringToDateTime(olMinStr);
	omMax = DBStringToDateTime(olMaxStr);


	InitColumn0();
	GetRecordsFromDB();
//	MakeCountListCic();
	MakeCountListCicFailure();
	MakeCountListCca();

}


AdditionalReport16TableViewer::~AdditionalReport16TableViewer()
{

	omColumn0.RemoveAll();
	omCountColumn1.RemoveAll();
	omCountColumn2.RemoveAll();

	omPST.DeleteAll();
	omPSTFailure.DeleteAll();
	omCIC.DeleteAll();

}


void AdditionalReport16TableViewer::InitColumn0()
{

	omColumn0.Add("0");
	omColumn0.Add("1");
	omColumn0.Add("2");
	omColumn0.Add("3");
	omColumn0.Add("4");
	omColumn0.Add("5");
	omColumn0.Add("6");
	omColumn0.Add("7");
	omColumn0.Add("8");
	omColumn0.Add("9");
	omColumn0.Add("10");
	omColumn0.Add("11");
	omColumn0.Add("12");
	omColumn0.Add("13");
	omColumn0.Add("14");
	omColumn0.Add("15");
	omColumn0.Add("16");
	omColumn0.Add("17");
	omColumn0.Add("18");
	omColumn0.Add("19");
	omColumn0.Add("20");
	omColumn0.Add("21");
	omColumn0.Add("22");
	omColumn0.Add("23");
	for (int i = 0; i < 25; i++)
	{
		omCountColumn1.Add(0);
		omCountColumn2.Add(0);
	}

}


void AdditionalReport16TableViewer::GetRecordsFromDB()
{

	CCS_TRY

	AfxGetApp()->DoWaitCursor(1);

	CString olWhere;

	olWhere.Format("(CKBA < '%s' AND (CKEA >= '%s' OR CKEA = ' ')) OR (CKBS < '%s' AND (CKES >= '%s' OR CKES = ' '))", 
 		           CTimeToDBString(omMax, omMax), CTimeToDBString(omMin, omMin), 
				   CTimeToDBString(omMax, omMax), CTimeToDBString(omMin, omMin));
	omCedaCcaData.ReadAllFlights(olWhere);


	// NAFR,NATO aus BLKTAB
	olWhere.Format("WHERE VAFR < '%s' AND (VATO >= '%s' OR VATO = ' ')", 
		           CTimeToDBString(omMax, omMax), CTimeToDBString(omMin, omMin));
	ogBCD.ReadSpecial("CIC", "URNO", olWhere, omCIC);

	CString olUrno;
	CString olAllUrnos;
	CStringArray olUrnoList;
	CString olObject("CIC");
	CString olDay;

	// Liste aller Urnos aufbauen
	for (int i = 0; i < omCIC.GetSize(); i++)
	{
		RecordSet *prlRecord = &omCIC[i];
		olUrno = (*prlRecord)[0];
		if( ! olAllUrnos.IsEmpty() ) 
			olAllUrnos += ",";
		olAllUrnos += olUrno;
	}
	// splitten in 100er
	int ilCount = SplitItemList(olAllUrnos, &olUrnoList, 100);

	olWhere.Empty();
	for (i = 0; i < ilCount; i++)
	{
		olWhere = CString("WHERE ") + CString("BURN IN (");
		olWhere += olUrnoList[i] + CString(")");
		olWhere += " AND TABN = 'CIC'";
		olWhere.GetLength();
		ogBCD.ReadSpecial("BLK", "BURN,TABN,NAFR,NATO,DAYS,TIFR,TITO", olWhere, omPSTFailure);

		for (int j = 0; j < omPSTFailure.GetSize(); j++)
		{
			RecordSet *prlBLKRecord = &omPSTFailure[j];
			CTime olTimeFrom = DBStringToDateTime((*prlBLKRecord)[2]);
			CTime olTimeTo   = DBStringToDateTime((*prlBLKRecord)[3]);
			if ( olTimeFrom < omMax && (olTimeTo >= omMin || olTimeTo == TIMENULL) )
			{
				int ilDay = omMax.GetDayOfWeek();
				// in CTime ist Sonntag=1 und Samstag=7!)
				ilDay -= 1;
				if (ilDay == 0)
					ilDay = 7;
				olDay.Format("%d");
				if ( (*prlBLKRecord)[4].Find(olDay) )
				{
					CString olFrom = (*prlBLKRecord)[5];
					CString olTo   = (*prlBLKRecord)[6];
					olFrom.Insert(2, "00");
					olTo.Insert(4, "00");
					CString olMinStr = omMin.Format( "%Y%m%d" );
					CString olMaxStr = omMax.Format( "%Y%m%d" );
					olFrom.Insert(0, olMinStr);
					olTo.Insert(0, olMaxStr);
					WriteBuffersCicFailure(DBStringToDateTime(olFrom), DBStringToDateTime(olTo));
				}
			}
		}
		omPSTFailure.DeleteAll();
	}

	AfxGetApp()->DoWaitCursor(-1);

	CCS_CATCH_ALL

}


void AdditionalReport16TableViewer::MakeCountListCic()
{

	CTime olTimeFrom;
	CTime olTimeTo;
	CTime olTimeToFailure;
	CTime olTimeFromFailure;


	for (int i = 0; i < omPST.GetSize(); i++)
	{
		RecordSet *prlRecord = &omPST[i];
		olTimeFrom = DBStringToDateTime((*prlRecord)[0]);
		olTimeTo   = DBStringToDateTime((*prlRecord)[1]);

		WriteBuffersCic(olTimeFrom, olTimeTo);
	}

}


void AdditionalReport16TableViewer::MakeCountListCicFailure()
{

	CTime olTimeToFailure;
	CTime olTimeFromFailure;

	for (int i = 0; i < omPSTFailure.GetSize(); i++)
	{
		RecordSet *prlRecord = &omPSTFailure[i];
		olTimeFromFailure = DBStringToDateTime((*prlRecord)[0]);
		olTimeToFailure   = DBStringToDateTime((*prlRecord)[1]);

		WriteBuffersCicFailure(olTimeFromFailure, olTimeToFailure);
	}

}


void AdditionalReport16TableViewer::MakeCountListCca()
{

	int ilCount = omCedaCcaData.omData.GetSize();

	for (int i = 0; i < ilCount; i++)
	{
		if (omCedaCcaData.omData[i].Ckba != TIMENULL && omCedaCcaData.omData[i].Ckea != TIMENULL)
			WriteBuffersCca(omCedaCcaData.omData[i].Ckba, omCedaCcaData.omData[i].Ckea);
		else
			WriteBuffersCca(omCedaCcaData.omData[i].Ckbs, omCedaCcaData.omData[i].Ckes);
	}

}


void AdditionalReport16TableViewer::WriteBuffersCic(CTime olTimeFrom, CTime olTimeTo)
{

	int ilHourCurrent;
	int i;

	CString olFrom = olTimeFrom.Format("%Y%m%d %H%M");
	CString olMin  = omMin.Format("%Y%m%d 0000");
	CString olTo   = olTimeTo.Format("%Y%m%d %H%M");
	CString olMax  = omMax.Format("%Y%m%d %H%M");

	if (olFrom < olMin && olTo < olMax && !olTo.IsEmpty())
		return;

	
	// auf alle 24 Stunden + 1, wenn aktuelle Zeiten auserhalb der angegebenen
	if ( (olTimeTo == TIMENULL && olFrom < olMin) || (olFrom < olMin && olTo > olMax) )
	{
		for (i = 0; i < 24; i++)
		{
			omCountColumn2[i] = omCountColumn2[i] + 1;
		}
		return;
	}

	olFrom = olTimeFrom.Format("%Y%m%d");
	olMin  = omMin.Format("%Y%m%d");
	olTo   = olTimeTo.Format("%Y%m%d");
	olMax  = omMax.Format("%Y%m%d");

	// Datum der Von-Zeit gleich aktuellem Von-Datum bis Ende des Tages
	if (olMin == olFrom && olMax != olTo)
	{
		// Anzahl Stunden des verbliebenen Tages
		ilHourCurrent = atoi(olTimeFrom.Format("%H").GetBuffer(0));
		for (i = ilHourCurrent; i < 24; i++)
			omCountColumn2[i] = omCountColumn2[i] + 1;
	}
	
	// Datum der Bis-Zeit gleich aktuellem Bis-Datum
	if (olMax == olTo && olMin != olFrom)
	{
		// Anzahl Stunden von 00:00 bis Stunde des aktuellen Tages
		ilHourCurrent = atoi(olTimeTo.Format("%H").GetBuffer(0));
		for (int i = 0; i < ilHourCurrent; i++)
			omCountColumn2[i] = omCountColumn2[i] + 1;
	}

	// von und bis des gleichen Tages
	if (olMax == olTo && olMin == olFrom)
	{
		int ilFrom = atoi(olTimeFrom.Format("%H").GetBuffer(0));
		int ilTo   = atoi(olTimeTo.Format("%H").GetBuffer(0));
		for (int i = ilFrom; i <= ilTo; i++)
			omCountColumn2[i] = omCountColumn2[i] + 1;
	}


}


void AdditionalReport16TableViewer::WriteBuffersCicFailure(CTime olTimeFrom, CTime olTimeTo)
{

	int ilHourCurrent;
	int i;

	CString olFrom = olTimeFrom.Format("%Y%m%d %H%M");
	CString olMin  = omMin.Format("%Y%m%d 0000");
	CString olTo   = olTimeTo.Format("%Y%m%d %H%M");
	CString olMax  = omMax.Format("%Y%m%d %H%M");

	if (olFrom < olMin && olTo < olMax && !olTo.IsEmpty())
		return;
	
	// auf alle 24 Stunden + 1, wenn aktuelle Zeiten auserhalb der angegebenen
	if ( (olTimeTo == TIMENULL && (olFrom <= olMin || olTimeFrom == TIMENULL)) || (olFrom <= olMin && olTo >= olMax) )
	{
		for (i = 0; i < 24; i++)
		{
			omCountColumn2[i] = omCountColumn2[i] + 1;
		}
		return;
	}

	olFrom = olTimeFrom.Format("%Y%m%d");
	olMin  = omMin.Format("%Y%m%d");
	olTo   = olTimeTo.Format("%Y%m%d");
	olMax  = omMax.Format("%Y%m%d");

	// Datum der Von-Zeit gleich aktuellem Von-Datum bis Ende des Tages
	if (olMin == olFrom && olMax != olTo)
	{
		// Anzahl Stunden des verbliebenen Tages
		ilHourCurrent = atoi(olTimeFrom.Format("%H").GetBuffer(0));
		for (i = ilHourCurrent; i < 24; i++)
			omCountColumn2[i] = omCountColumn2[i] + 1;
	}
	
	// Datum der Bis-Zeit gleich aktuellem Bis-Datum
	if (olMax == olTo && olMin != olFrom)
	{
		// Anzahl Stunden von 00:00 bis Stunde des aktuellen Tages
		ilHourCurrent = atoi(olTimeTo.Format("%H").GetBuffer(0));
		for (int i = 0; i < ilHourCurrent; i++)
			omCountColumn2[i] = omCountColumn2[i] + 1;
	}

	// von und bis des gleichen Tages
	if (olMax == olTo && olMin == olFrom)
	{
		int ilFrom = atoi(olTimeFrom.Format("%H").GetBuffer(0));
		int ilTo   = atoi(olTimeTo.Format("%H").GetBuffer(0));
		for (int i = ilFrom; i <= ilTo; i++)
			omCountColumn2[i] = omCountColumn2[i] + 1;
	}

}


void AdditionalReport16TableViewer::WriteBuffersCca(CTime olTimeFrom, CTime olTimeTo)
{

	int ilHourCurrent;
	int i;

	CString olFrom = olTimeFrom.Format("%Y%m%d %H%M");
	CString olMin  = omMin.Format("%Y%m%d 0000");
	CString olTo   = olTimeTo.Format("%Y%m%d %H%M");
	CString olMax  = omMax.Format("%Y%m%d %H%M");

	if (olTimeFrom == TIMENULL && olTimeTo == TIMENULL)
		return;
	if (olFrom < olMin && olTo < olMax && !olTo.IsEmpty())
		return;
	
	// auf alle 24 Stunden + 1, wenn aktuelle Zeiten auserhalb der angegebenen
	if ( (olTimeTo == TIMENULL && olFrom <= olMin) || (olFrom <= olMin && olTo >= olMax) )
	{
		for (i = 0; i < 24; i++)
		{
			omCountColumn1[i] = omCountColumn1[i] + 1;
		}
		return;
	}

	olFrom = olTimeFrom.Format("%Y%m%d");
	olMin  = omMin.Format("%Y%m%d");
	olTo   = olTimeTo.Format("%Y%m%d");
	olMax  = omMax.Format("%Y%m%d");

	// Datum der Von-Zeit gleich aktuellem Von-Datum bis Ende des Tages
	if (olMin == olFrom && olMax != olTo)
	{
		// Anzahl Stunden des verbliebenen Tages
		ilHourCurrent = atoi(olTimeFrom.Format("%H").GetBuffer(0));
		for (i = ilHourCurrent; i < 24; i++)
			omCountColumn1[i] = omCountColumn1[i] + 1;
	}
	
	// Datum der Bis-Zeit gleich aktuellem Bis-Datum
	if (olMax == olTo && olMin != olFrom)
	{
		// Anzahl Stunden von 00:00 bis Stunde des aktuellen Tages
		ilHourCurrent = atoi(olTimeTo.Format("%H").GetBuffer(0));
		for (int i = 0; i < ilHourCurrent; i++)
			omCountColumn1[i] = omCountColumn1[i] + 1;
	}

	// von und bis des gleichen Tages
	if (olMax == olTo && olMin == olFrom)
	{
		int ilFrom = atoi(olTimeFrom.Format("%H").GetBuffer(0));
		int ilTo   = atoi(olTimeTo.Format("%H").GetBuffer(0));
		for (int i = ilFrom; i <= ilTo; i++)
			omCountColumn1[i] = omCountColumn1[i] + 1;
	}

}


int AdditionalReport16TableViewer::GetBarCount()
{

	return omColumn0.GetSize();

}



CString AdditionalReport16TableViewer::GetTime(int ipIndex)
{

	if (ipIndex < omColumn0.GetSize())
		return omColumn0[ipIndex];
	else
		return "";

}


int AdditionalReport16TableViewer::GetDataCic(int ipIndex)
{

	if (ipIndex < omCountColumn2.GetSize())
		return omCountColumn2[ipIndex];
	else
		return 0;

}


int AdditionalReport16TableViewer::GetDataCca(int ipIndex)
{

	if (ipIndex < omCountColumn1.GetSize())
		return omCountColumn1[ipIndex];
	else
		return 0;

}


int AdditionalReport16TableViewer::NumberOfCic()
{

	return omCIC.GetSize();
	
}


void AdditionalReport16TableViewer::SetParentDlg(CDialog* ppParentDlg)
{

	pomParentDlg = ppParentDlg;

}


void AdditionalReport16TableViewer::PrintTableView(DGanttChartReportWnd* popDGanttChartReportWnd)
{

	CCS_TRY

	CString olFooter1;
	CString olTableName = GetString(IDS_STRING507);
	olTableName += ":";
	int ilOrientation = PRINT_LANDSCAPE;
	CCSPrint* pomPrint = new CCSPrint(pomParentDlg,ilOrientation,45);
	pomPrint->imMaxLines = 38;

	char pclHeader[256];

	sprintf(pclHeader, GetString(IDS_STRING1750));
	CString olHeader(pclHeader);

	if (pomPrint != NULL)
	{

		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = olTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;

			CString olFooter1;
			olFooter1.Format("%s %d",rlDocInfo.lpszDocName, NumberOfCic());
			popDGanttChartReportWnd->Print(&pomPrint->omCdc, pomPrint, olHeader, olFooter1);
		}

		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		delete pomPrint;
		pomPrint = NULL;
	}

	CCS_CATCH_ALL

}
