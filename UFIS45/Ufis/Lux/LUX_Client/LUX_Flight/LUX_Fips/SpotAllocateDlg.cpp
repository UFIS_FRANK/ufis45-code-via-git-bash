// SpotAllocateDlg.cpp : implementation file
// 09-jan-01	rkr		PRF_ATH:08012001 e-mail_ebe; Dr watson if fileds are empty 
//

#include <stdafx.h>
#include <fpms.h>
#include <SpotAllocateDlg.h>
#include <SpotAllocation.h>
#include <Konflikte.h>
#include <WoResTableDlg.h>
#include <PosDiagram.h>
#include <GatDiagram.h>
#include <BltDiagram.h>
#include <WroDiagram.h>
#include <resrc1.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// SpotAllocateDlg dialog


SpotAllocateDlg::SpotAllocateDlg(CWnd* pParent /*=NULL*/)
	: CDialog(SpotAllocateDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(SpotAllocateDlg)
	m_GatLeftBuffer = 0;
	m_GatRightBuffer = 0;
	m_PstLeftBuffer = 0;
	m_PstRightBuffer = 0;
	//}}AFX_DATA_INIT
}


void SpotAllocateDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(SpotAllocateDlg)
	DDX_Control(pDX, IDC_ALLOCATE_WRO, m_CB_Allocate_Wro);
	DDX_Control(pDX, IDC_ALLOCATE_POS, m_CB_Allocate_Pos);
	DDX_Control(pDX, IDC_ALLOCATE_GAT, m_CB_Allocate_Gat);
	DDX_Control(pDX, IDC_ALLOCATE_BLT, m_CB_Allocate_Blt);
	DDX_Control(pDX, IDC_PROGRESS1, m_Progress);
	DDX_Control(pDX, IDC_GATLEFTBUFFER, m_CE_GatLeftBuffer);
 	DDX_Control(pDX, IDC_GATRIGHTBUFFER, m_CE_GatRightBuffer);
 	DDX_Control(pDX, IDC_PSTLEFTBUFFER, m_CE_PstLeftBuffer);
 	DDX_Control(pDX, IDC_PSTRIGHTBUFFER, m_CE_PstRightBuffer);
	DDX_Text(pDX, IDC_GATLEFTBUFFER, m_GatLeftBuffer);
 	DDX_Text(pDX, IDC_GATRIGHTBUFFER, m_GatRightBuffer);
 	DDX_Text(pDX, IDC_PSTLEFTBUFFER, m_PstLeftBuffer);
 	DDX_Text(pDX, IDC_PSTRIGHTBUFFER, m_PstRightBuffer);
	//DDV_MinMaxInt(pDX, m_PstRightBuffer, 0, 99);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(SpotAllocateDlg, CDialog)
	//{{AFX_MSG_MAP(SpotAllocateDlg)
	ON_BN_CLICKED(IDC_RESET, OnReset)
    ON_MESSAGE(WM_SAS_PROGESS_UPDATE, OnProgressUpdate)
    ON_MESSAGE(WM_SAS_PROGESS_INIT, OnProgressInit)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// SpotAllocateDlg message handlers

BOOL SpotAllocateDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_Progress.SetRange(0, 100);
 
	m_CB_Allocate_Blt.SetCheck(TRUE);
	m_CB_Allocate_Gat.SetCheck(TRUE);
	m_CB_Allocate_Wro.SetCheck(TRUE);
	m_CB_Allocate_Pos.SetCheck(TRUE);

	m_CE_GatLeftBuffer.SetTypeToInt(0, 99);
	m_CE_GatLeftBuffer.SetBKColor(YELLOW);

	m_CE_GatRightBuffer.SetTypeToInt(0, 99);
	m_CE_GatRightBuffer.SetBKColor(YELLOW);

	m_CE_PstLeftBuffer.SetTypeToInt(0, 99);
	m_CE_PstLeftBuffer.SetBKColor(YELLOW);

	m_CE_PstRightBuffer.SetTypeToInt(0, 99);
	m_CE_PstRightBuffer.SetBKColor(YELLOW);

//	m_GatLeftBuffer = 5;
//	m_GatRightBuffer = 5;

	if (ogGateAllocBufferTime.GetTotalMinutes() == 0)
	{
		m_GatLeftBuffer = 5;
		m_GatRightBuffer = 5;
	}
	else
	{
		m_GatLeftBuffer = ogGateAllocBufferTime.GetTotalMinutes();
		m_GatRightBuffer = ogGateAllocBufferTime.GetTotalMinutes();

		m_CE_GatLeftBuffer.EnableWindow(FALSE);
		m_CE_GatRightBuffer.EnableWindow(FALSE);
	}


	if (ogPosAllocBufferTime.GetTotalMinutes() == 0)
	{
		m_PstLeftBuffer = 5;
		m_PstRightBuffer = 5;
	}
	else
	{
		m_PstLeftBuffer = ogPosAllocBufferTime.GetTotalMinutes();
		m_PstRightBuffer = ogPosAllocBufferTime.GetTotalMinutes();

		m_CE_PstLeftBuffer.EnableWindow(FALSE);
		m_CE_PstRightBuffer.EnableWindow(FALSE);
	}

	CString olTmpStr;
	olTmpStr.Format("%d", m_GatLeftBuffer);
	m_CE_GatLeftBuffer.SetWindowText(olTmpStr);
	olTmpStr.Format("%d", m_GatRightBuffer);
	m_CE_GatRightBuffer.SetWindowText(olTmpStr);
	olTmpStr.Format("%d", m_PstLeftBuffer);
	m_CE_PstLeftBuffer.SetWindowText(olTmpStr);
	olTmpStr.Format("%d", m_PstRightBuffer);
	m_CE_PstRightBuffer.SetWindowText(olTmpStr);

	//UpdateData(FALSE);
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void SpotAllocateDlg::GetParameters() 
{
	//UpdateData(TRUE);

	CString olTmpStr;
	m_CE_GatLeftBuffer.GetWindowText(olTmpStr);
	if (olTmpStr.IsEmpty())
	{
		m_CE_GatLeftBuffer.SetWindowText("0");
		m_GatLeftBuffer = 0;
	}
	else
		m_GatLeftBuffer = atoi(olTmpStr);

	m_CE_GatRightBuffer.GetWindowText(olTmpStr);
	if (olTmpStr.IsEmpty())
	{
		m_CE_GatRightBuffer.SetWindowText("0");
		m_GatRightBuffer = 0;
	}
	else
		m_GatRightBuffer = atoi(olTmpStr);

	m_CE_PstLeftBuffer.GetWindowText(olTmpStr);
	if (olTmpStr.IsEmpty())
	{
		m_CE_PstLeftBuffer.SetWindowText("0");
		m_PstLeftBuffer = 0;
	}
	else
		m_PstLeftBuffer = atoi(olTmpStr);

	m_CE_PstRightBuffer.GetWindowText(olTmpStr);
	if (olTmpStr.IsEmpty())
	{
		m_CE_PstRightBuffer.SetWindowText("0");
		m_PstRightBuffer = 0;
	}
	else
		m_PstRightBuffer = atoi(olTmpStr);

	omAllocParameter.omGatLeftBuffer   = CTimeSpan(0,0,m_GatLeftBuffer,0);
	omAllocParameter.omGatRightBuffer  = CTimeSpan(0,0,m_GatRightBuffer,0);
	omAllocParameter.omPstLeftBuffer   = CTimeSpan(0,0,m_PstLeftBuffer,0);
	omAllocParameter.omPstRightBuffer  = CTimeSpan(0,0,m_PstRightBuffer,0);


	if(m_CB_Allocate_Wro.GetCheck() == TRUE)
		omAllocParameter.bmAllocateWro  = true;
	else
		omAllocParameter.bmAllocateWro  = false;

	if(m_CB_Allocate_Pos.GetCheck() == TRUE)
		omAllocParameter.bmAllocatePst  = true;
	else
		omAllocParameter.bmAllocatePst  = false;

	if(m_CB_Allocate_Gat.GetCheck() == TRUE)
		omAllocParameter.bmAllocateGat  = true;
	else
		omAllocParameter.bmAllocateGat  = false;

	if(m_CB_Allocate_Blt.GetCheck() == TRUE)
		omAllocParameter.bmAllocateBlt  = true;
	else
		omAllocParameter.bmAllocateBlt  = false;


}


bool SpotAllocateDlg::CheckAll(void)
{
	CString olErrMes;
	CString olDlgText;
	if (!m_CE_PstLeftBuffer.GetStatus())
	{
		olErrMes.Format(GetString(IDS_STRING1892), GetString(IDS_STRING1893), 0, 99);
		MessageBox(olErrMes, GetString(ST_FEHLER), MB_ICONERROR | MB_OK);
		return false;		
	}
	if (!m_CE_PstRightBuffer.GetStatus())
	{
		olErrMes.Format(GetString(IDS_STRING1892), GetString(IDS_STRING1894), 0, 99);
		MessageBox(olErrMes, GetString(ST_FEHLER), MB_ICONERROR | MB_OK);
		return false;		
	}
	if (!m_CE_GatLeftBuffer.GetStatus())
	{
		olErrMes.Format(GetString(IDS_STRING1892), GetString(IDS_STRING1895), 0, 99);
		MessageBox(olErrMes, GetString(ST_FEHLER), MB_ICONERROR | MB_OK);
		return false;		
	}
	if (!m_CE_GatRightBuffer.GetStatus())
	{
		olErrMes.Format(GetString(IDS_STRING1892), GetString(IDS_STRING1896), 0, 99);
		MessageBox(olErrMes, GetString(ST_FEHLER), MB_ICONERROR | MB_OK);
		return false;		
	}
	return true;
}


void SpotAllocateDlg::OnOK() 
{
/*	test 
	HMODULE hModule = ::GetModuleHandle("fips.exe");
	HMODULE hResHandle = AfxGetResourceHandle();
	AfxMessageBox(61712);
testende */

	if (!CheckAll())
		return;

	if(MessageBox( GetString(IDS_STRING1772), GetString(IDS_STRING1811), MB_YESNO ) != IDYES)
		return;
	
	bgPosDiaAutoAllocateInWork = true;

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));		


	GetParameters();

	ogSpotAllocation.SetParent( this);

	ogSpotAllocation.SetAllocateParameters( &omAllocParameter );

	omSaveData.DeleteAll();

	for(int ilLc = ogPosDiaFlightData.omData.GetSize() - 1; ilLc >= 0; ilLc--)
	{
		omSaveData.New(ogPosDiaFlightData.omData[ilLc]);
	}


	ogSpotAllocation.CreateFlightList();


	if(omAllocParameter.bmAllocatePst)	
		ogSpotAllocation.AutoPstAllocate();

	if(omAllocParameter.bmAllocateGat)	
		ogSpotAllocation.AutoGatAllocate();

	if(omAllocParameter.bmAllocateWro)	
		ogSpotAllocation.AutoWroAllocate();
	
	if(omAllocParameter.bmAllocateBlt)	
		ogSpotAllocation.AutoBltAllocate();

	ogSpotAllocation.ReCheckAllChangedFlights(omAllocParameter.bmAllocatePst, omAllocParameter.bmAllocateGat, omAllocParameter.bmAllocateWro, omAllocParameter.bmAllocateBlt);

	m_Progress.SetRange(0, omSaveData.GetSize());
	m_Progress.SetStep(1);
	m_Progress.SetPos(0);


	DIAFLIGHTDATA *prlFlight; 
	DIAFLIGHTDATA *prlFlightSave; 


 	CString olData;
	CString olField = ogPosDiaFlightData.GetFieldList();

	CString olSelection;
 
 
	for( ilLc = omSaveData.GetSize() - 1; ilLc >= 0; ilLc--)
	{
		prlFlightSave = &omSaveData[ilLc];
		

		prlFlight = ogPosDiaFlightData.GetFlightByUrno(prlFlightSave->Urno);


		if(strlen(prlFlight->Gta1) != 0 && strcmp(prlFlight->Gta1, prlFlightSave->Gta1) != 0)
		{
			prlFlightSave->Ga1b = TIMENULL;
			prlFlightSave->Ga1e = TIMENULL;
		}
		else
		{
			prlFlightSave->Ga1b = prlFlight->Ga1b;
			prlFlightSave->Ga1e = prlFlight->Ga1e;
		}


		if(strlen(prlFlight->Gta2) != 0 && strcmp(prlFlight->Gta2, prlFlightSave->Gta2) != 0)
		{
			prlFlightSave->Ga2b = TIMENULL;
			prlFlightSave->Ga2e = TIMENULL;
		}
		else
		{
			prlFlightSave->Ga2b = prlFlight->Ga2b;
			prlFlightSave->Ga2e = prlFlight->Ga2e;
		}

		if(strlen(prlFlight->Gtd1) != 0 && strcmp(prlFlight->Gtd1, prlFlightSave->Gtd1) != 0)
		{
			prlFlightSave->Gd1b = TIMENULL;
			prlFlightSave->Gd1e = TIMENULL;
		}
		else
		{
			prlFlightSave->Gd1b = prlFlight->Gd1b;
			prlFlightSave->Gd1e = prlFlight->Gd1e;
		}



		if(strlen(prlFlight->Gtd2) != 0 && strcmp(prlFlight->Gtd2, prlFlightSave->Gtd2) != 0)
		{
			prlFlightSave->Gd2b = TIMENULL;
			prlFlightSave->Gd2e = TIMENULL;
		}
		else
		{
			prlFlightSave->Gd2b = prlFlight->Gd2b;
			prlFlightSave->Gd2e = prlFlight->Gd2e;
		}



		if(strlen(prlFlight->Psta) != 0 && strcmp(prlFlight->Psta, prlFlightSave->Psta) != 0)
		{
			prlFlightSave->Pabs = TIMENULL;
			prlFlightSave->Paes = TIMENULL;
		}
		else
		{
			prlFlightSave->Pabs = prlFlight->Pabs;
			prlFlightSave->Paes = prlFlight->Paes;
		}

		if(strlen(prlFlight->Pstd) != 0 && strcmp(prlFlight->Pstd, prlFlightSave->Pstd) != 0)
		{
			prlFlightSave->Pdbs = TIMENULL;
			prlFlightSave->Pdes = TIMENULL;
		}
		else
		{
			prlFlightSave->Pdbs = prlFlight->Pdbs;
			prlFlightSave->Pdes = prlFlight->Pdes;
		}


		if(strlen(prlFlight->Blt1) != 0 && strcmp(prlFlight->Blt1, prlFlightSave->Blt1) != 0)
		{
			prlFlightSave->B1bs = TIMENULL;
			prlFlightSave->B1es = TIMENULL;
		}
		else
		{
			prlFlightSave->B1bs = prlFlight->B1bs;
			prlFlightSave->B1es = prlFlight->B1es;
		}


		if(strlen(prlFlight->Blt2) != 0 && strcmp(prlFlight->Blt2, prlFlightSave->Blt2) != 0)
		{
			prlFlightSave->B2bs = TIMENULL;
			prlFlightSave->B2es = TIMENULL;
		}
		else
		{
			prlFlightSave->B2bs = prlFlight->B2bs;
			prlFlightSave->B2es = prlFlight->B2es;
		}


		if(strlen(prlFlight->Wro1) != 0 && strcmp(prlFlight->Wro1, prlFlightSave->Wro1) != 0)
		{
			prlFlightSave->W1bs = TIMENULL;
			prlFlightSave->W1es = TIMENULL;
		}
		else
		{
			prlFlightSave->W1bs = prlFlight->W1bs;
			prlFlightSave->W1es = prlFlight->W1es;
		}


 		/////////////////////////////////////////////////////
		ogPosDiaFlightData.UpdateFlight(prlFlight, prlFlightSave);

		/*
		olData = "";
		ogPosDiaFlightData.MakeCedaData(olData, olField, prlFlightSave, prlFlight);

  		olSelection.Format("WHERE URNO = %ld", prlFlight->Urno);
  		
		ogPosDiaFlightData.UpdateFlight(prlFlight->Urno, olSelection, olField, olData);
		*/
		/////////////////////////////////////////////////////


		m_Progress.OffsetPos(1);

	} 
	omSaveData.DeleteAll();

	if (pogWoResTableDlg != NULL)
		pogWoResTableDlg->Rebuild();


	UpdateGatPosDiagrams();


	bgPosDiaAutoAllocateInWork = false;

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));		

	CDialog::OnOK();
}


void SpotAllocateDlg::UpdateGatPosDiagrams()
{
	if (pogPosDiagram)
		pogPosDiagram->SendMessage(WM_REPAINT_ALL, 0, 0);

	if (pogGatDiagram)
		pogGatDiagram->SendMessage(WM_REPAINT_ALL, 0, 0);

	if (pogBltDiagram)
		pogBltDiagram->SendMessage(WM_REPAINT_ALL, 0, 0);

	if (pogWroDiagram)
		pogWroDiagram->SendMessage(WM_REPAINT_ALL, 0, 0);
}




void SpotAllocateDlg::OnReset()
{
	if (!CheckAll())
		return;

	if(MessageBox( GetString(IDS_STRING1772), GetString(IDS_STRING1812), MB_YESNO ) != IDYES)
		return;

	
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));		
	
	ogKonflikte.DisableAttentionButtonsUpdate();

	ogSpotAllocation.SetParent( this);

	GetParameters();

	ogSpotAllocation.ResetAllocation(omAllocParameter.bmAllocatePst, omAllocParameter.bmAllocateGat, omAllocParameter.bmAllocateBlt, omAllocParameter.bmAllocateWro);

	if (pogWoResTableDlg != NULL)
		pogWoResTableDlg->Rebuild();

	ogKonflikte.EnableAttentionButtonsUpdate();

	omSaveData.DeleteAll();

	UpdateGatPosDiagrams();

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));		

	EndDialog(IDOK);
}


LONG SpotAllocateDlg::OnProgressUpdate(UINT wParam, LPARAM lParam)
{
	m_Progress.OffsetPos(wParam);



	return 0L;
}


LONG SpotAllocateDlg::OnProgressInit(UINT wParam, LPARAM lParam)
{
	
	m_Progress.SetRange(0,wParam);
	m_Progress.SetStep(1);
	m_Progress.SetPos(0);

	return 0L;
}

