#ifndef __ADDITIONAL_REPORT_16_VIEWER_H__
#define __ADDITIONAL_REPORT_16_VIEWER_H__

#include <stdafx.h>
#include <RotationDlgCedaFlightData.h>
#include <CcaCedaFlightData.h>
#include <DGanttChartReportWnd.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>
#include <CedaCcaData.h>



/////////////////////////////////////////////////////////////////////////////
// AdditionalReport16TableViewer

class AdditionalReport16TableViewer : public CViewer
{
// Constructions
public:
    AdditionalReport16TableViewer(char *pcpInfo = NULL, char *pcpSelect = NULL, 
		                          CTime opMin = TIMENULL, CTime opMax = TIMENULL);
    ~AdditionalReport16TableViewer();

private:
	void InitColumn0();
	void GetRecordsFromDB();

// Operations

// Attributes used for filtering condition
private:
	// 
	CCSPtrArray<RecordSet>omPST;
	CCSPtrArray<RecordSet>omPSTFailure;
	CCSPtrArray<RecordSet>omCIC;
	// Sapltengroese
	CUIntArray omAnzChar;
	CedaCcaData omCedaCcaData;

	// Liste der existierenden Delays
	CStringArray omColumn0;
	// Auftreten der existierenden Delays
	CUIntArray omCountColumn1;
	CUIntArray omCountColumn2;
	char pcmFirstDate[16];	//Time frame beginn (datum)
	char pcmLastDate[16];	//Time frame end
	long lmDays;
	CTime omMin;			// Datum + Zeit
	CTime omMax;
	CDialog* pomParentDlg;

public:
	int GetBarCount();
	int GetDataCic(int ipIndex);
	int GetDataCca(int ipIndex);
	CString GetTime(int ipIndex);
	int NumberOfCic();

	//Print 
	void SetParentDlg(CDialog* ppDlg);
	void PrintTableView(DGanttChartReportWnd* popDGanttChartReportWnd);
	CCSPrint *pomPrint; 
	CString omTableName;
	char *pcmInfo;
	char *pcmSelect;
	CString omFooterName;
	CTime omTimeFirst;

protected:
	void SetFieldLength(CString opCurrentColumns);
	// Listen zum Zaehlen erstellen
	void MakeCountListCic();
	void MakeCountListCicFailure();
	void MakeCountListCca();

	// Schreiben der Zahlen in die Puffer
	void WriteBuffersCic(CTime olTimeFrom, CTime olTimeTo);
	void WriteBuffersCicFailure(CTime olTimeFrom, CTime olTimeTo);
	void WriteBuffersCca(CTime olTimeFrom, CTime olTimeTo);

};

#endif