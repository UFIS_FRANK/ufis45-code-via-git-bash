#if !defined(AFX_KonflikteDlg_H__BBB97141_04EA_11D2_8564_0000C04D916B__INCLUDED_)
#define AFX_KonflikteDlg_H__BBB97141_04EA_11D2_8564_0000C04D916B__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// KonflikteDlg.h : header file
//


#include <CCSTable.h>
#include <Konflikte.h>


struct KONF_LINEDATA
{
	KonfIdent KonfId;
	CTime TimeOfConflict;
	CString Text;
};


 
/////////////////////////////////////////////////////////////////////////////
// KonflikteDlg dialog

class KonflikteDlg : public CDialog
{
// Construction
public:
	KonflikteDlg(CWnd* pParent = NULL);   // standard constructor

	~KonflikteDlg();

// Dialog Data
	//{{AFX_DATA(KonflikteDlg)
	enum { IDD = IDD_KONFLIKT };
	CCSButtonCtrl	m_CB_AllConfirm;
	CCSButtonCtrl	m_CB_Confirm;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(KonflikteDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	//}}AFX_VIRTUAL

public:

	void Conflicts();
	void Attention();

	void ChangeViewTo();

	void MakeLines();
	void MakeLine(const KonfIdent &rrpKonfId, KONFDATA *prpData);

	void DeleteAll();

	int CreateLine(KONF_LINEDATA &rpKonf);

	int CompareKonf(KONF_LINEDATA *prpKonf1, KONF_LINEDATA *prpKonf2);

	void UpdateDisplay();

	int FindLine(const KonfIdent &rrpKonfId);

	void InitTable(); 
	CCSTable *pomTable;

	CCSPtrArray<KONF_LINEDATA> omLines;

	bool bmAttention;
	bool bmIsActiv;


	void DdxChangeKonf(KonfIdent &rrpIdNotify);
	void DdxInsertKonf(KonfIdent &rrpIdNotify);
	void DdxDeleteKonf(KonfIdent &rrpIdNotify);


// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(KonflikteDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnConfirm();
	afx_msg void OnAllconfirm();
	virtual void OnCancel();
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	afx_msg void OnTimer(UINT nIDEvent);
    afx_msg LONG OnTableReturnPressed(UINT wParam, LONG lParam);
	afx_msg LONG OnTableSelChanged( UINT wParam, LPARAM lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_KonflikteDlg_H__BBB97141_04EA_11D2_8564_0000C04D916B__INCLUDED_)
