#include <stdafx.h>

#include <DGanttBarReport.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


DGanttBarReport::DGanttBarReport(DGanttChartReport* popDGanttChartReport, CColor Color,
							     unsigned int ipData, CString opText)
{

	pomDGanttChartReport = popDGanttChartReport;
	imData     = ipData;
	imXText    = opText;
	omColor    = Color;

}


DGanttBarReport::~DGanttBarReport()
{

}


void DGanttBarReport::Draw(CDC* ppDCD, int ipSpace, int ipBarNo, int ipHeight, int ipHeightPrev, CString opBarText)
{

	CSize olSize;
	int ilX;
	int ilY;

	CString olText;
	if (opBarText.IsEmpty())
		olText.Format("%d", imData);
	else
		olText = opBarText;

	ppDCD->SelectObject(pomDGanttChartReport->GetGanttChartFont());
	

	int ilBarWidth = pomDGanttChartReport->GetGanttBarWidth();

	
	int ilBeginnX = ipSpace + ilBarWidth * ipBarNo;

	CPen olPen;
	olPen.CreatePen(PS_SOLID, 1, COLORREF(RGB(omColor.r, omColor.g, omColor.b)));
	ppDCD->SelectObject(olPen);

	if (ipBarNo > 0)
	{
		ppDCD->MoveTo(ilBeginnX, pomDGanttChartReport->GetGanttYBeginn() - ipHeightPrev);
		ppDCD->LineTo(ilBeginnX, pomDGanttChartReport->GetGanttYBeginn() - ipHeight);
	}


	ppDCD->MoveTo(ilBeginnX, pomDGanttChartReport->GetGanttYBeginn() - ipHeight);
	ppDCD->LineTo(ilBeginnX + ilBarWidth, pomDGanttChartReport->GetGanttYBeginn() - ipHeight);
	

	// Text im Balken
/*	olSize = ppDCD->GetTextExtent(olText);
	ilX = ((ilBarWidth - olSize.cx) / 2) + ilBeginnX;
	ilY = pomDGanttChartReport->GetGanttYBeginn() - ((ipHeight + olSize.cy) / 2);
	ppDCD->TextOut(ilX, ilY, olText);*/


	// Text unter der XAchse
	olSize = ppDCD->GetTextExtent(imXText);
	ilX = ((ilBarWidth - olSize.cx) / 2) + ilBeginnX;
	ilY = pomDGanttChartReport->GetGanttYBeginn() + 1;
	ppDCD->TextOut(ilX, ilY, imXText);

}


bool DGanttBarReport::Print(CDC* ppDCD, int ipSpace, int ipBarNo, int ipHeight, int ipHeightPrev, double dpFactorY)
{

	CString olText;
	olText.Format("%d", imData);

    LOGFONT logFont;
    memset(&logFont, 0, sizeof(LOGFONT));

	CFont olArial_7;

    logFont.lfHeight = - MulDiv(7, ppDCD->GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Arial");
    olArial_7.CreateFontIndirect(&logFont);
	ppDCD->SelectObject(&olArial_7);


	CSize olSizeMinX = ppDCD->GetTextExtent("00000");
	int ilBarWidth;
	if (olSizeMinX.cx > pomDGanttChartReport->GetGanttBarWidthPrint())
		ilBarWidth = olSizeMinX.cx;
	else
		ilBarWidth = pomDGanttChartReport->GetGanttBarWidthPrint();

	int ilBeginnX = (int)(ipSpace + ilBarWidth * ipBarNo);

	// neue Seite?
    CRect olClipRect;
    ppDCD->GetClipBox(&olClipRect);
			                     // eine Balkenbreite abziehen, sonst zu dicht am re. Rand
	if (ilBeginnX > (olClipRect.right - ilBarWidth))
	{
		return false;
	}

	CPen olPen;
	if (omColor.r == 255)
		olPen.CreatePen(PS_DOT, 1, COLORREF(RGB(0, 0, 0)));
	else
		olPen.CreatePen(PS_SOLID, 1, COLORREF(RGB(0, 0, 0)));
	ppDCD->SelectObject(olPen);
	   
	if (ipBarNo > 0)
	{
	//              Anfang in x-richtung;         H�he des Balkens                           Ende in x-Richtung                               y-Punkt au x-Achse                           
		ppDCD->MoveTo(ilBeginnX, pomDGanttChartReport->GetGanttYBeginnPrint() - ipHeightPrev);
		ppDCD->LineTo(ilBeginnX, pomDGanttChartReport->GetGanttYBeginnPrint() - ipHeight);
	}

	ppDCD->MoveTo(ilBeginnX, pomDGanttChartReport->GetGanttYBeginnPrint() - ipHeight);
	ppDCD->LineTo(ilBeginnX + ilBarWidth, pomDGanttChartReport->GetGanttYBeginnPrint() - ipHeight);
	
/*
	// Text im Balken
	CSize olSize = ppDCD->GetTextExtent(olText);
	int ilX = ((ilBarWidth - olSize.cx) / 2) + ilBeginnX;
	int ilY = pomDGanttChartReport->GetGanttYBeginnPrint() - ((ipHeight + olSize.cy) / 2);
	ppDCD->TextOut(ilX, ilY, olText);
*/

	// Text unter der XAchse
	CSize olSize = ppDCD->GetTextExtent(imXText);
	int ilX = ((ilBarWidth - olSize.cx) / 2) + ilBeginnX;
	int ilY = pomDGanttChartReport->GetGanttYBeginnPrint() + (int)(1 * dpFactorY);
	ppDCD->TextOut(ilX, ilY, imXText);

	return true;

}