#ifndef __SPOT_ALLOCATION__

#include <CedaBasicData.h>
#include <CCSGlobl.h>
#include <DiaCedaFlightData.h>
#include <CCSTime.h>
#include <Konflikte.h>
//#include "SpotAllocation.h"



struct PST_RESLIST
{
	CString	  Urno;
	CString   Pnam;
	double SpanMin;
	double SpanMax;
	double LengMin;
	double LengMax;
	double HeighMin;
	double HeighMax;
	CTime  Nafr;
	CTime  Nato;
	// Aircrafttype restrictions
	CString ACUserStr;
	CStringArray AssignedAC;
	CStringArray ExcludedAC;
	CStringArray PreferredAC;

	CString Pos1;
	double Mac1; //restricted to
	CString Pos2;
	double Mac2;
	int Mult; // Max A/C
	int Prio; // Priority
	CString Brgs;
	// Nature restrictions
	CString NAUserStr;
	CStringArray AssignedNA;
	CStringArray ExcludedNA;
	CStringArray PreferredNA;
	// Airline restrictions
	CString ALUserStr;
	CStringArray AssignedAL;
	CStringArray ExcludedAL;
	CStringArray PreferredAL;

	CStringArray RelGats; // Related Gates
	PST_RESLIST()
	{
		Urno = "0";SpanMin = 0.;SpanMax = 99999.;LengMin = 0.;LengMax = 99999.;HeighMin= 0.;HeighMax= 99999.;
		Pos1 = "0";Mac1 = 0;Pos2 = "0";Mac2 = 0;Mult = 0;Prio = 0;
		//Exac = "";Natr = "";Pral = "";
		ACUserStr = ""; NAUserStr = ""; ALUserStr = "";
		Nafr = TIMENULL;Nato = TIMENULL;
	}
};



struct GAT_RESLIST
{
	CString Urno;
	CString Gnam;
	CString Busg;
	CString Flti;
	CString RelPos1;
	CString RelPos2;
	int     Maxp;
	int     Mult;
	int     Prio;
	bool	Inbo;
	bool    Outb;
	//CString Natr;
	//CString Pral;
	CTime   Nafr;
	CTime   Nato;

	// Airline restrictions
	CString ALUserStr;
	CStringArray AssignedAL;
	CStringArray ExcludedAL;
	CStringArray PreferredAL;
	// Org/Des restrictions
	CString OrgDesUserStr;
	CStringArray AssignedOrgDes;
	CStringArray ExcludedOrgDes;
	CStringArray PreferredOrgDes;
	// Nature restrictions
	CString NAUserStr;
	CStringArray AssignedNA;
	CStringArray ExcludedNA;
	CStringArray PreferredNA;

	GAT_RESLIST()
	{
		Urno = "0";Flti = "";RelPos1="";RelPos2="";Maxp = 999;Mult = 0;Inbo = false;Outb = false;//Natr = "";Pral = "";
		ALUserStr = ""; OrgDesUserStr = ""; NAUserStr = "";
		Nafr = TIMENULL;Nato = TIMENULL;Busg="";
	}

};

struct GAT_ALLOC
{
	CString Urno;
	CTime Dube;
	CTime Duen;
	DIAFLIGHTDATA *prlFlightA;
	DIAFLIGHTDATA *prlFlightD;
	GAT_ALLOC()
	{
		Dube=-1;Duen=-1;prlFlightA=NULL;prlFlightD=NULL;
	}

};

struct PST_ALLOC
{
	CString Urno;
	CString Pnam;
	CTime Dube;
	CTime Duen;
	double Span;
	DIAFLIGHTDATA *prlFlightA;
	DIAFLIGHTDATA *prlFlightD;
	PST_ALLOC()
	{
		Dube=-1;Duen=-1;Span=0.0;prlFlightA=NULL;prlFlightD=NULL;
	}
};


struct GAT_DEMAND
{
	int				Paxt;
	int				Dura;
	CTime			Dube;
	CTime			Duen;
	DIAFLIGHTDATA	*pFlightA;
	DIAFLIGHTDATA	*pFlightD;
};

struct PST_DEMAND
{
	double			Span;
	int				Dura;
	CTime			Dube;
	CTime			Duen;
	DIAFLIGHTDATA	*pFlightA;
	DIAFLIGHTDATA	*pFlightD;
	GAT_DEMAND		*pGatDem;
	CString Type; //(I)nbound, (O)utbound, (T)owing
	PST_DEMAND()
	{
		Span = 0.0;	Dura = 0; Dube = -1; Duen = -1; pFlightA = NULL; 
		pFlightD = NULL; pGatDem  = NULL; Type = ""; 
	}
};


class SpotAllocation
{
public:

	SpotAllocation();
	
	~SpotAllocation();

	void SetAllocateParameters(AllocateParameters *popParameters);

	void SetParent( CWnd *popParent);

	void Init();

	void ResetAllocation(bool bpPst, bool bpGat, bool bpBlt, bool bpWro);


	void InitNatRestriction();
	void InitPstLinksToTwy();


	void CreatePositionRules();
	void GetAcgrFromAcgrList(PST_RESLIST *prpPstRes, CStringArray &opAcgr);

	void CreateGateRules();

	AllocateParameters omParameters;


	bool CheckPst(DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD, CString opPst, CCSPtrArray<KonfItem> *popKonfList = NULL, CTimeSpan *popRightBuffer = NULL, CTimeSpan *popLeftBuffer = NULL);
	bool CheckPstNeighborConf(const DIAFLIGHTDATA *prpFlightA, const DIAFLIGHTDATA *prpFlightD, const CString &ropPst, CCSPtrArray<KonfItem> *popKonfList = NULL, CTimeSpan *popRightBuffer = NULL, CTimeSpan *popLeftBuffer = NULL);
	bool CheckGat(const DIAFLIGHTDATA *prpFlight, char cpFPart, CString opGat, int ipGatNo , CCSPtrArray<KonfItem> *popKonfList, const CTimeSpan *popRightBuffer = NULL, const CTimeSpan *popLeftBuffer = NULL);
	bool CheckWro(DIAFLIGHTDATA *prpFlightD, CString opWro, int ipWroNo, CCSPtrArray<KonfItem> *popKonfList = NULL, bool bpPreCheck = false);
	bool CheckBlt(DIAFLIGHTDATA *prpFlightA, CString opBlt, int ipBltNo, CCSPtrArray<KonfItem> *popKonfList, int ipMaxCount);

	bool ReCheckAllChangedFlights(bool bpCheckPst, bool bpCheckGat,bool bpCheckWro, bool bpCheckBlt);

	void CreateFlightList();


	bool AllocatePst(DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD, CString opPst);

	bool AllocateGat(DIAFLIGHTDATA *prlFlightA, DIAFLIGHTDATA *prlFlightD, CString opGat, int ipGat = 1);

	bool AllocateWro(DIAFLIGHTDATA *prlFlightD, CString opWro, int ipWro);

	bool AllocateBlt(DIAFLIGHTDATA *prlFlightA, CString opBlt, int ipBelt);


	bool GetPositionsByRule(DIAFLIGHTDATA *prlFlightA, DIAFLIGHTDATA *prlFlightD, CStringArray &opPsts );
	bool GetGatesByRule(DIAFLIGHTDATA *prlFlight, char cpFPart, CStringArray &opGates );
	bool GetBeltsByRule(DIAFLIGHTDATA *prlFlight, CStringArray &opBelts);
	bool GetWrosByRule(DIAFLIGHTDATA *prlFlight, CStringArray &opWros);

 
	bool CheckPosGateRelation(DIAFLIGHTDATA *prpFlight, char cpFPart, const CString &ropPst);

	CString GetGatFlightId(CString opGat);


	PST_RESLIST *GetPstRes(const CString &ropPst);
	GAT_RESLIST *GetGatRes(const CString &ropGat);


	CCSPtrArray<PST_RESLIST> omPstRules;
	CCSPtrArray<GAT_RESLIST> omGatRules;


	CCSPtrArray<DiaCedaFlightData::RKEYLIST> omFlights;

	void SetGatSort(CString opGnam);
  

	bool AutoPstAllocate();
	bool AutoPstAllocateFlights(DIAFLIGHTDATA *prlFlightA, DIAFLIGHTDATA *prlFlightD, const CString &ropRecomPos);

	bool AutoGatAllocate();
	bool AutoGatAllocatePstFlight(DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD);

	//bool AutoGatAllocateWithoutPst();

	bool AutoGatAllocatePst();

	bool AutoWroAllocate();

	bool AutoBltAllocate();
	bool AutoBltAllocateFlight(DIAFLIGHTDATA *prlFlightA, int ipBltNo);

	bool MakeBlkData(CTime &opStartTime, CTime &opEndTime, CString &opTabn, CString &opName, CStringArray &opNafr, CStringArray &opNato, CStringArray &opResn);

	bool bmPstConf;
	bool bmGatConf;
	bool bmWroConf;
	bool bmBltConf;


	CString omGatNatNoAllocList;
	CString omPstNatNoAllocList;

	CStringArray omPstLinksToTwy;

	CWnd *pomParent;

private:
	bool CheckWingoverlap(const DIAFLIGHTDATA *prpFlight, const CTime &ropStartAlloc, const CTime &ropEndAlloc, const CString &ropPos, 
						  double dpPosSpanMax, double dpPosMac, double dpMinDist, double dpACSpan, DIAFLIGHTDATA *&prpFlightTmp, const CString &ropPosFlight);
	void ParsePrefExclAssiStr(const CString &ropStr, CStringArray &ropPreferred, CStringArray &ropExcluded, CStringArray &ropAssigned) const;

	bool CheckResToResLists(const CString &ropTestStr, const CStringArray &ropAssignedStrs, const CStringArray &ropExcludedStrs) const;
	bool CheckResToResLists(const CString &ropTestStr1, const CString &ropTestStr2, const CStringArray &ropAssignedStrs, const CStringArray &ropExcludedStrs) const;

	bool GetMatchCountPst(const DIAFLIGHTDATA *prpFlightA, const DIAFLIGHTDATA *prpFlightD, const CStringArray &ropPreferredAL, const CStringArray &ropPreferredAC, const CStringArray &ropPreferredNA,
							float &rfpGoodCount, int &ripBadCount) const;
	bool GetMatchCountGat(const DIAFLIGHTDATA *prpFlight, const CStringArray &ropPreferredAL, const CStringArray &ropPreferredOrgDes, const CStringArray &ropPreferredNA, 
							float &rfpGoodCount, int &ripBadCount) const;

	bool CheckGatFlightId(const CString &ropGat, const CString &ropFlti); 

	bool GetBestGate(const CString &ropFtliGat, const DIAFLIGHTDATA *prpFlightA, const DIAFLIGHTDATA *prpFlightD, 
					 int ipGateNo, const CTimeSpan *popGatLeftBuffer, const CTimeSpan *popGatRightBuffer, CString &ropBestGate);

	bool GetBestGate(const CString &ropFtliGat, const DIAFLIGHTDATA *prpFlight, char cpFPart,
					 int ipGateNo, const CTimeSpan *popGatLeftBuffer, const CTimeSpan *popGatRightBuffer, CString &ropBestGate);

	void AddToKonfList(const DIAFLIGHTDATA *prpFlightA, const DIAFLIGHTDATA *prpFlightD, long lpRelFUrno, char cpRelFPart, int ipType, const CString &ropConfText, CCSPtrArray<KonfItem> &ropKonfList);
	void AddToKonfList(const DIAFLIGHTDATA *prpFlight, char cpFPart, long lpRelFUrno, char cpRelFPart, int ipType, const CString &ropConfText, CCSPtrArray<KonfItem> &ropKonfList);

	bool GetRelatedBlts(const DIAFLIGHTDATA *prpFlight, int ipBltNo, CStringArray &ropBlts);


	CMapPtrToPtr omChangedFlightsRkeyMap;

};

#endif //__GATPOS_ALLOCATION__