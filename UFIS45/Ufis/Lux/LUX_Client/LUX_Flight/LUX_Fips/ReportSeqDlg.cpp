// ReportSeqDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <fpms.h>
#include <ReportSeqDlg.h>
#include <CCSTime.h>
#include <CcsGlobl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CReportSeqDlg 


CReportSeqDlg::CReportSeqDlg(CWnd* pParent, CString opHeadline, CTime* poMinDate, CTime* poMaxDate)
	: CDialog(CReportSeqDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CReportSeqDlg)
	m_Datumbis = _T("");
	m_Datumvon = _T("");
	//}}AFX_DATA_INIT

	omHeadline = opHeadline;	//Dialogbox-�berschrift
	pomMinDate = poMinDate;
	pomMaxDate = poMaxDate;
}


void CReportSeqDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CReportSeqDlg)
	DDX_Control(pDX, IDC_DATUMVON, m_CE_Datumvon);
	DDX_Control(pDX, IDC_DATUMBIS, m_CE_Datumbis);
	DDX_Text(pDX, IDC_DATUMBIS, m_Datumbis);
	DDX_Text(pDX, IDC_DATUMVON, m_Datumvon);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CReportSeqDlg, CDialog)
	//{{AFX_MSG_MAP(CReportSeqDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CReportSeqDlg 

void CReportSeqDlg::OnOK() 
{
	m_CE_Datumvon.GetWindowText(m_Datumvon);
	m_CE_Datumbis.GetWindowText(m_Datumbis);
	// ist nur ein Time Feld gef�llt, �bernimmt das andere dessen Inhalt
	if(m_Datumvon.IsEmpty())
		m_CE_Datumbis.GetWindowText(m_Datumvon);
	if(m_Datumbis.IsEmpty())
		m_CE_Datumvon.GetWindowText(m_Datumbis);

	*pomMinDate = DateStringToDate(m_Datumvon);
	*pomMaxDate = DateStringToDate(m_Datumbis); 



	if( (m_Datumbis.IsEmpty()) || (m_Datumvon.IsEmpty()) )
		MessageBox(GetString(IDS_STRING918), GetString(IMFK_REPORT), MB_OK);
	else
	{
		if((m_CE_Datumvon.GetStatus()) && (m_CE_Datumbis.GetStatus()))
		{
			if (*pomMinDate > *pomMaxDate)
				MessageBox(GetString(IDS_STRING1642), GetString(IMFK_REPORT), MB_OK);
			else
				CDialog::OnOK();
		}
		else
			MessageBox(GetString(IDS_STRING934), GetString(IMFK_REPORT), MB_OK);
	}
}

void CReportSeqDlg::OnCancel() 
{
	// TODO: Zus�tzlichen Bereinigungscode hier einf�gen
	
	CDialog::OnCancel();
}

BOOL CReportSeqDlg::OnInitDialog() 
{

	CDialog::OnInitDialog();
	CDialog::SetWindowText(omHeadline);	//Dialogbox-�berschrift
	m_CE_Datumvon.SetTypeToDate();
	m_CE_Datumbis.SetTypeToDate();
	m_CE_Datumvon.SetBKColor(YELLOW);
	m_CE_Datumbis.SetBKColor(YELLOW);

	m_Datumvon = pomMinDate->Format("%d.%m.%Y");
	m_CE_Datumvon.SetWindowText( m_Datumvon );

	m_Datumbis = pomMaxDate->Format("%d.%m.%Y" );
	m_CE_Datumbis.SetWindowText( m_Datumbis );

/*rkr
	if (ogCustomer == "SHA" && ogAppName == "FIPS")
	{
		CStatic* polSelectHeadline = (CStatic*)GetDlgItem(IDC_SELECTHEADLINE);
		CStatic* polTill = (CStatic*)GetDlgItem(IDC_ST_INTERVAL_TILL);
		polSelectHeadline->ShowWindow(SW_HIDE);
		polTill->SetWindowText(GetString(IDS_STRING1604));
	}
*/

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}


