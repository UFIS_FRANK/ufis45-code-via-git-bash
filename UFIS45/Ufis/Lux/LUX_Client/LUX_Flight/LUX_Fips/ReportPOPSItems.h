
#ifndef AFX_REPORTPOPSITEMS_H__INCLUDED_
#define AFX_REPORTPOPSITEMS_H__INCLUDED_

// ReportPOPSItems.h : Header-Datei
//

#include <stdafx.h>
#include <CedaCfgData.h>


// etwas eleganter l�sen !?
const int DAILYITEMSItems = 15;		// SUS f�r Dimensionierungen - mu� = NoOfItems sein !!
const int DAILYITEMSCcaItems = 10;

const int imIndexLeer = -1 ;	//  Kennung f�r Leerzeile


struct ITEMS 
{						// Beschreibt eine Report View/Druck Element
	CString Str ;		// Name
	int Size ;			// Anzahl Zeichen im Report
	bool LeftFrame ;	// FRAME links bei Druck ?
	bool RightFrame ;	// FRAME rechts bei Druck ?

	ITEMS()
	{
		Str.Empty() ;
		Size = 3 ;
		LeftFrame = false ;
		RightFrame = false ;
	}
} ;


struct DAILYITEMS	// Positionen auf DAILY OPS-Plan + mit Zellengr��e
{

public:

	// Reihenfolge in Tabellen
	enum { DailyAll, DailyFlight, DailyCca } ;	// Flugplanung, Counterplanung ( Konstruktorargumente )
	enum { ALC,FLTNIN,FLTNOUT,ACT,NOSE,ORG,STA,DES,STD,CHTDES,POS,GP,CTR,PAX,EINTEIL } ;	// Daily/Season
	enum { CALC,CFLTNOUT,CACT,CROUT,CDES,CSTD,CCNO1,CCIFT1,CCKIMIN1,CCKITIME1 } ;	// Counterplanung
//	enum { CALC,CFLTNOUT,CACT,CROUT,CDES,CSTD,CCNO1,CCIFT1,CCKIMIN1,CCKITIME1,CCNO2,CCIFT2,CCKIMIN2,CCKITIME2 } ;	// Counterplanung

	const int ReportArt ; // Wert in der Initialsierungliste des Konstr. zuweisen !!

	ITEMS	Alc ;		// Flight + CCA
	ITEMS	FltnOut ;
	ITEMS	Act ;
	ITEMS	Std ;
	ITEMS	Des ;
	ITEMS	ViaOut ;

	ITEMS	FltnIn ;	// Flight
	ITEMS	Nose ;
	ITEMS	Org ;
	ITEMS	Sta ;
	ITEMS	ViaIn ;
	ITEMS	RemIn ;
	ITEMS	ChtDes ;
	ITEMS	RemOut ;
	ITEMS	Pos ;	
	ITEMS	Gp ;
	ITEMS	Ctr ;
	ITEMS	Pax ;
	ITEMS	Einteil ;
							// CCA
	ITEMS	CAnz[2] ;		// Anzahl Cca
	ITEMS	Ckift[2] ;		// Cca from - to
	ITEMS	CMin[2] ;		// Cca �ffnungsdauer
	ITEMS	CkiftTime[2] ;	// Cca �ffnungszeiten from - to

	int	Folgezeile ;

	int NoOfItems ;		// Anzahl der Items die auf Viewer dargestellt werden !
	int TotalOfItems ;
	bool TwoVias ;		// intern: Routing mit zwei VIAS pro Zeile ?

	
	// Protokoll

	DAILYITEMS() ;
	DAILYITEMS( int ipTab ) ;
	~DAILYITEMS() ;


	void GetDailyFormatList( CString& ) ;				// liefert Argument f�r Viewer::SetFormatList()

	bool IsFagSpecial( CString );						// true <=> Alc2 ist eine FAG Besonderheit
	bool Is2LtrCode( CString ) ;						// true <=> Alc2 eine Ziffer enth�lt

	int GetNoOfItems()	{ return NoOfItems ; }			// Anzahl der Viewer Spalten
	int GetTotalOfItems()	{ return TotalOfItems ; }	// Anzahl Items
	bool DoLeftFrame( int ) ;							// liefert Wert von LeftFrame 
	void GetEmptyFormatString( CString &opFormat );
	int GetRemLength() { return( min( RemIn.Size, RemOut.Size ) ) ; }	// Anzahl Zeichen pro Remark Zeile ( pro In / Out )

	int GetNoOfRem( CString opRem ) ;

} ;



/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio f�gt zus�tzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_REPORTPOPSITEMS_H__INCLUDED_
