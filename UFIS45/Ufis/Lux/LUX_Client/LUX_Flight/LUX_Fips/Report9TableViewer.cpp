// SaisonflugplanTableViewer.cpp 
//
//	Fluggesellschaften eines Saisonflugplanes

#include <stdafx.h>
#include <Report9TableViewer.h>
#include <CcsGlobl.h>
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include <resrc1.h>
#include <CedaBasicData.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// SaisonflugplanTableViewer
//
SaisonflugplanTableViewer::SaisonflugplanTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo)
{
	pomData = popData;
	pcmInfo = pcpInfo;

	bmIsFromSearch = FALSE;
    pomSaisonflugplanTable = NULL;
}

//-----------------------------------------------------------------------------------------------

SaisonflugplanTableViewer::~SaisonflugplanTableViewer()
{
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void SaisonflugplanTableViewer::Attach(CCSTable *popTable)
{
    pomSaisonflugplanTable = popTable;
}

//-----------------------------------------------------------------------------------------------
void SaisonflugplanTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------
void SaisonflugplanTableViewer::MakeLines()
{
	int ilCntrArr = 0;
	int ilCntrDep = 0;
	int ilCntrBoth = 0;
	int ilTotalArr = 0;
	int ilTotalDep = 0;
	int ilTotalBoth = 0;
	CString olNewstr("");
	CString olOldstr(""); 

	int ilCount = pomData->GetSize();

	for (int ilLc = ilCount - 1; ilLc >= 0 ; ilLc--)
	{
//		ilCntrDep++;
		ROTATIONDLGFLIGHTDATA *prlSaisonflugplanData = &pomData->GetAt(ilLc);
		if (strlen(prlSaisonflugplanData->Alc2))
			olNewstr = prlSaisonflugplanData->Alc2;
		else
			olNewstr = prlSaisonflugplanData->Alc3;

		//Arrival
		if((strcmp(prlSaisonflugplanData->Org3, pcgHome) != 0) && (strcmp(prlSaisonflugplanData->Des3, pcgHome) == 0))
		{
			ilCntrArr++;
			ilTotalArr++;
		}
		else
		{
			// Departure
			if((strcmp(prlSaisonflugplanData->Org3, pcgHome) == 0) && (strcmp(prlSaisonflugplanData->Des3, pcgHome) != 0))
			{
				ilCntrDep++;
				ilTotalDep++;
			}
			else
			{
				//Turnaround
				if((strcmp(prlSaisonflugplanData->Org3, pcgHome) == 0) && (strcmp(prlSaisonflugplanData->Des3, pcgHome) == 0))
				{
					ilCntrBoth++;
					ilTotalBoth++;
				}
			}
		}


		if(ilLc == ilCount - 1)	// der erste
		{
			olOldstr = olNewstr;

		}
		if (olNewstr != olOldstr)	// Fluggesellschaft wechselt
		{
			MakeLine(olOldstr, ilCntrArr, ilCntrDep, ilCntrBoth);
			olOldstr = olNewstr;
			ilCntrArr = 0;
			ilCntrDep = 0;
			ilCntrBoth = 0;
		}

		if((ilLc == 0) && (ilCntrDep > 0))	// nach dem letzten
			MakeLine(olNewstr, ilCntrArr, ilCntrDep, ilCntrBoth);
	}

//generate the headerinformation

	CString olTimeSet = GetString(IDS_STRING1920); //UTC
	if(bgReportLocal)
		olTimeSet = GetString(IDS_STRING1921); //LOCAL

	char pclHeader[256];
	sprintf(pclHeader, GetString(IDS_R_HEADER9), GetString(IDS_REPORTS_RB_AirlineInSeason), pcmInfo, omLines.GetSize(), ilTotalArr+ilTotalDep, ilTotalArr, ilTotalDep, olTimeSet);
	omTableName = pclHeader;
}

//-----------------------------------------------------------------------------------------------

void SaisonflugplanTableViewer::MakeLine(CString Alcold, int ipCntrArr, int ipCntrDep, int ipCntrBoth)
{
    // Update viewer data for this shift record
	char clbufferArr[10];
	char clbufferDep[10];
	char clbufferBoth[10];
    SAISONFLUGPLANTABLE_LINEDATA rlSaisonflugplan;
	sprintf(clbufferArr,"%d", ipCntrArr);
	sprintf(clbufferDep,"%d", ipCntrDep);
	sprintf(clbufferBoth,"%d", ipCntrBoth);
	rlSaisonflugplan.CntrArr = clbufferArr; 
	rlSaisonflugplan.CntrDep = clbufferDep; 
	rlSaisonflugplan.CntrBoth = clbufferBoth; 
	rlSaisonflugplan.Alc3 = Alcold; 


	CString olAlfn;

	bool blRet = false;
	if (rlSaisonflugplan.Alc3.GetLength() == 2)
	{
		blRet = ogBCD.GetField("ALT", "ALC2", rlSaisonflugplan.Alc3, "ALFN", olAlfn);
	}
	else if (rlSaisonflugplan.Alc3.GetLength() == 3)
	{
//		if ( strcmp (rlSaisonflugplan.Alc3, "BCS") == 0)
//			bool bl = true;
		blRet = ogBCD.GetField("ALT", "ALC3", rlSaisonflugplan.Alc3, "ALFN", olAlfn);
	}
	else
		olAlfn = "?";

	if (!blRet)
		olAlfn = "?";

/*	bool blRet = ogBCD.GetField("ALT", "ALC2", rlSaisonflugplan.Alc2, "ALFN", olAlfn);
	if(!blRet)
		blRet = ogBCD.GetField("ALT", "ALC3", rlSaisonflugplan.Alc3, "ALFN", olAlfn);
	if(!blRet)
		blRet = ogBCD.GetField("ALT", "ALC3", rlSaisonflugplan.Alc2, "ALFN", olAlfn);
*/	
	
	rlSaisonflugplan.Alfn = olAlfn;

	CreateLine(&rlSaisonflugplan);
}

//-----------------------------------------------------------------------------------------------

void SaisonflugplanTableViewer::CreateLine(SAISONFLUGPLANTABLE_LINEDATA *prpSaisonflugplan)
{
	int ilLineno = 0;
	SAISONFLUGPLANTABLE_LINEDATA rlSaisonflugplan;
	rlSaisonflugplan = *prpSaisonflugplan;
    omLines.NewAt(ilLineno, rlSaisonflugplan);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void SaisonflugplanTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 4;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomSaisonflugplanTable->SetShowSelection(TRUE);
	pomSaisonflugplanTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

//	rlHeader.Length = 160; 
	rlHeader.Length = 80; 
	rlHeader.Text = GetString(IDS_STRING1046);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

//	rlHeader.Length = 160;
	rlHeader.Length = 80;
	rlHeader.Text = GetString(IDS_STRING_ARRIVAL);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	rlHeader.Length = 80;
	rlHeader.Text = GetString(IDS_STRING_DEPARTURE);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	rlHeader.Length = 480;
	rlHeader.Text = GetString(IDS_STRING_AIRLINENAME);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomSaisonflugplanTable->SetHeaderFields(omHeaderDataArray);

	pomSaisonflugplanTable->SetDefaultSeparator();
	pomSaisonflugplanTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		if(omLines[ilLineNo].Alc2.GetLength())
			rlColumnData.Text = omLines[ilLineNo].Alc2;
		else
			rlColumnData.Text = omLines[ilLineNo].Alc3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = omLines[ilLineNo].CntrArr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = omLines[ilLineNo].CntrDep;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = omLines[ilLineNo].Alfn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomSaisonflugplanTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomSaisonflugplanTable->DisplayTable();
}


//-----------------------------------------------------------------------------------------------

void SaisonflugplanTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

void SaisonflugplanTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void SaisonflugplanTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	char pclFooter[256];
	//sprintf(pclInfo, "Fluggesellschaften eines Saisonflugplanes \n%s", pcmInfo);
//	sprintf(pclFooter, GetString(IDS_STRING1144), (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));
	sprintf(pclFooter, omTableName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));

	CString olTableName = GetString(IDS_REPORTS_RB_AirlineInSeason);
	omFooterName = pclFooter;

	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	pomPrint->imMaxLines = 38;  // (P=57,L=38)

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			//pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			pomPrint->imLineNo = pomPrint->imMaxLines + 1 - 2;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = olTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
//rkr			olFooter1.Format(GetString(IDS_STRING1445),ilLines,omFooterName);
//			olFooter1.Format("%s %d / %s %d, %s", GetString(IDS_STRING_AIRLINES), ilLines, GetString(IDS_STRING1256), pomData->GetSize(), omFooterName);
			olFooter1.Format("%s -   %s", omFooterName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );

			for(int i = 0; i < ilLines; i++ ) 
			{
				//if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				if(pomPrint->imLineNo >= pomPrint->imMaxLines-2)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				//if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-3) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;

	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool SaisonflugplanTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;
	double dgCCSPrintFactor = 2.7 ;
/*
	char pclHeader[100];
	sprintf(pclHeader, GetString(IDS_STRING1146), pcmInfo);
	//sprintf(pclHeader, "  vom %s", pcmInfo);
	CString olTableName(pclHeader);
*/
	//pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);
//	pomPrint->PrintUIFHeader(" ",CString(pclHeader),pomPrint->imFirstLine-10);
	pomPrint->PrintUIFHeader(" ",omTableName,pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Text   = omHeaderDataArray[i].Text;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool SaisonflugplanTableViewer::PrintTableLine(SAISONFLUGPLANTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	double dgCCSPrintFactor = 2.7 ;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		
		rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameTop   = PRINT_FRAMETHIN;
		if(bpLastLine == true)
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN;
		}
		else
		{
			rlElement.FrameBottom = PRINT_NOFRAME;
		}
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		switch(i)
		{
		case 0:
			{
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				if(strlen(prpLine->Alc2))
					rlElement.Text = prpLine->Alc2;
				else
					rlElement.Text = prpLine->Alc3;
			}
			break;
		case 1:
			{
				rlElement.FrameLeft = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->CntrArr;
			}
			break;
		case 2:
			{
				rlElement.FrameLeft = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->CntrDep;
			}
			break;
		case 3:
			{
				rlElement.FrameLeft = PRINT_FRAMETHIN;
				rlElement.FrameRight = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Alfn;
			}
			break;
		}
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------

// Drucken in Datei
bool SaisonflugplanTableViewer::PrintPlanToFile(char *opTrenner)
{

	ofstream of;

	char* tmpvar;
	tmpvar = getenv("TMP");
	if (tmpvar == NULL)
		tmpvar = getenv("TEMP");

	if (tmpvar == NULL)
		return false;

	CString olFileName = omTableName;
	olFileName.Remove('*');
	olFileName.Remove('.');
	olFileName.Remove(':');
	olFileName.Remove('/');
	olFileName.Replace(" ", "_");
	
	char pHeader[256];
	strcpy (pHeader, tmpvar);
	CString path = pHeader;
	omFileName =  path + "\\" + olFileName + ".csv";

//	of.open( GetString(IDS_STRING1386), ios::out);
	of.open( omFileName, ios::out);

	int ilwidth = 1;

	of  << setw(ilwidth) << omTableName << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;

	of	<< setw(ilwidth) << GetString(IDS_STRING1046) 
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING_ARRIVAL) 
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING_DEPARTURE)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING_AIRLINENAME)
		<< endl;


	int ilCount = omLines.GetSize();

	for(int i = 0; i < ilCount; i++)
	{
		SAISONFLUGPLANTABLE_LINEDATA rlLine = omLines[i];

		of.setf(ios::left, ios::adjustfield);

		of   << setw(ilwidth) << rlLine.Alc3 
		     << setw(1) << opTrenner 
		     << setw(ilwidth) << rlLine.CntrArr
		     << setw(1) << opTrenner 
		     << setw(ilwidth) << rlLine.CntrDep
		     << setw(1) << opTrenner 
			 << setw(ilwidth) << rlLine.Alfn 
			 << endl;

	}

	of.close();
	return true;



/*
	ofstream of;
	of.open( pcpDefPath, ios::out);
	of << GetString(IDS_STRING928) << " " 
		<< pcmInfo << "    "
		<< CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M") << endl << endl;

	of  << GetString(IDS_STRING1046) << "    "
		<< GetString(IDS_STRING1043) << "       "  
		<< GetString(IDS_STRING1043) << "       "  
		<< endl;
	of << "---------------------------------------------------------------" << endl;
	int ilCount = omLines.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		SAISONFLUGPLANTABLE_LINEDATA rlD = omLines[i];
		of.setf(ios::left, ios::adjustfield);
		of   << setw(20) << rlD.Alc3  
			 << setw(15) << rlD.CntrArr
			 << setw(15) << rlD.CntrDep
			 << setw(50) << rlD.Alfn << endl;
	}
	of.close();
//	MessageBox("Es wurde folgende Datei geschrieben:\nc:\\tmp\\Saisonflug.txt", "Drucken",(MB_ICONINFORMATION|MB_OK));
*/

}