// RotationLoadDlgATHATH.cpp : implementation file
//
// Created 10.12.2000 TVO: Load-Dialog nur fuer hopo ATH
//

#include <stdafx.h>
#include <fpms.h>
#include <RotationLoadDlgATH.h>
#include <CedaApxData.h>
#include <PrivList.h>
#include <resource.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// RotationLoadDlgATH dialog


RotationLoadDlgATH::RotationLoadDlgATH(CWnd* pParent /*=NULL*/, ROTATIONDLGFLIGHTDATA *prpFlight)
	: CDialog(RotationLoadDlgATH::IDD, pParent)
{

	prmFlight = prpFlight;
	
	//{{AFX_DATA_INIT(RotationLoadDlgATH)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void RotationLoadDlgATH::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(RotationLoadDlgATH)
	DDX_Control(pDX, IDC_MAIL, m_CE_MAIL);
	DDX_Control(pDX, IDC_CGOT, m_CE_CGOT);
	DDX_Control(pDX, IDC_BAGW, m_CE_BAGW);
	DDX_Control(pDX, IDC_BAGN, m_CE_BAGN);
	DDX_Control(pDX, IDC_PAXT, m_CE_PAXT);
	DDX_Control(pDX, IDC_PAXI, m_CE_PAXI);
	DDX_Control(pDX, IDC_PAXF, m_CE_PAXF);
	DDX_Control(pDX, IDC_PAX3, m_CE_PAX3);
	DDX_Control(pDX, IDC_PAX2, m_CE_PAX2);
	DDX_Control(pDX, IDC_PAX1, m_CE_PAX1);
	DDX_Control(pDX, IDC_PAX_INF, m_CE_PAX_INF);
	DDX_Control(pDX, IDC_PAX_ID, m_CE_PAX_ID);
	DDX_Control(pDX, IDC_LDM_PAX1, m_CE_LDM_PAX1);
	DDX_Control(pDX, IDC_LDM_PAX2, m_CE_LDM_PAX2);
	DDX_Control(pDX, IDC_LDM_PAX3, m_CE_LDM_PAX3);
	DDX_Control(pDX, IDC_LDM_PAXT, m_CE_LDM_PAXT);
	DDX_Control(pDX, IDC_PTM_PAXF, m_CE_PTM_PAXF);
	DDX_Control(pDX, IDC_MVT_PAXI, m_CE_MVT_PAXI);

	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(RotationLoadDlgATH, CDialog)
	//{{AFX_MSG_MAP(RotationLoadDlgATH)
	ON_MESSAGE(WM_EDIT_KILLFOCUS, OnEditKillfocus)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// RotationLoadDlgATH message handlers

void RotationLoadDlgATH::OnOK() 
{
	if(	!m_CE_PAX1.GetStatus() ||
		!m_CE_PAX2.GetStatus() ||
		!m_CE_PAX3.GetStatus() ||
		!m_CE_PAXT.GetStatus() ||
		!m_CE_PAXI.GetStatus() ||
		!m_CE_PAXF.GetStatus() ||
		!m_CE_MAIL.GetStatus() ||
		!m_CE_CGOT.GetStatus() ||
		!m_CE_BAGW.GetStatus() ||
		!m_CE_BAGN.GetStatus() ||
		!m_CE_PAX_INF.GetStatus() ||
		!m_CE_PAX_ID.GetStatus()  ||
		!m_CE_LDM_PAX1.GetStatus() ||
		!m_CE_LDM_PAX2.GetStatus() ||
		!m_CE_LDM_PAX3.GetStatus() ||
		!m_CE_LDM_PAXT.GetStatus() ||
		!m_CE_PTM_PAXF.GetStatus() ||
		!m_CE_MVT_PAXI.GetStatus())
	{
		MessageBox(GetString(ST_BADFORMAT), GetString(ST_FEHLER));
		return;
	}

	m_CE_MAIL.GetWindowText(prmFlight->Mail,7);
	m_CE_CGOT.GetWindowText(prmFlight->Cgot,7);
	m_CE_BAGW.GetWindowText(prmFlight->Bagw,8);
	m_CE_BAGN.GetWindowText(prmFlight->Bagn,7);

	// save changed data in AFT-tab
 	char pclFieldList[2048];
 	char pclData[3072];

	pclFieldList[0]='\0';
	pclData[0]='\0';
	if (m_CE_MAIL.IsChanged()) 
	{
		strcat(pclFieldList, "MAIL,");
		strcat(pclData, prmFlight->Mail);
		strcat(pclData, ",");
	}
	if (m_CE_CGOT.IsChanged()) 
	{
		strcat(pclFieldList, "CGOT,");
		strcat(pclData, prmFlight->Cgot);
		strcat(pclData, ",");
	}
	if (m_CE_BAGW.IsChanged()) 
	{
		strcat(pclFieldList, "BAGW,");
		strcat(pclData, prmFlight->Bagw);
		strcat(pclData, ",");
	}
	if (m_CE_BAGN.IsChanged()) 
	{
		strcat(pclFieldList, "BAGN,");
		strcat(pclData, prmFlight->Bagn);
		strcat(pclData, ",");
	}

	if (strlen(pclFieldList) > 0)
		pclFieldList[strlen(pclFieldList)-1]='\0';
	if (strlen(pclData) > 0)
		pclData[strlen(pclData)-1]='\0';
 
	//TRACE("RotationDlgCedaFlightData::UpdateFlight: Urno=%ld pclFieldList=%s pclData=%s\n", prmFlight->Urno, pclFieldList, pclData);

	ogRotationDlgFlights.UpdateFlight(prmFlight->Urno, pclFieldList, pclData);
		
		
	// save PAX data
	CString olTmp;

	m_CE_PAX1.GetWindowText(olTmp);
	SaveInApxTab("PA1", olTmp);

	m_CE_PAX2.GetWindowText(olTmp);
	SaveInApxTab("PA2", olTmp);

	m_CE_PAX3.GetWindowText(olTmp);
	SaveInApxTab("PA3", olTmp);

	m_CE_PAXI.GetWindowText(olTmp);
	SaveInApxTab("PAI", olTmp);

	m_CE_PAXF.GetWindowText(olTmp);
	SaveInApxTab("PAF", olTmp);

	m_CE_PAXT.GetWindowText(olTmp);
	SaveInApxTab("PAT", olTmp);

	m_CE_PAX_INF.GetWindowText(olTmp);
	SaveInApxTab("INF", olTmp);

	m_CE_PAX_ID.GetWindowText(olTmp);
	SaveInApxTab("ID", olTmp);


	CDialog::OnOK();
}


BOOL RotationLoadDlgATH::SaveInApxTab(const CString &ropId, const CString &ropData) 
{

	APXDATA *prlApx = ogApxData.GetApx(ropId);

	if(prlApx != NULL)
	{
		strcpy(prlApx->Paxc, ropData);
		
		prlApx->IsChanged = DATA_CHANGED;
		ogApxData.Save(prlApx);
	}
	else
	{
		if(!ropData.IsEmpty())
		{
			prlApx = new APXDATA;
			prlApx->Flnu = prmFlight->Urno;
			strcpy(prlApx->Paxc, ropData);
			strcpy(prlApx->Type, ropId);
			prlApx->IsChanged = DATA_NEW;

			ogApxData.Save(prlApx);
		}
	}

	return true;
}



BOOL RotationLoadDlgATH::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// set editfield types	
	m_CE_PAX1.SetTypeToString( CString("###"), 3, 0);
	m_CE_PAX2.SetTypeToString( CString("###"), 3, 0);
	m_CE_PAX3.SetTypeToString( CString("###"), 3, 0);
	m_CE_PAXT.SetTypeToString( CString("###"), 3, 0);
	m_CE_PAXI.SetTypeToString( CString("###"), 3, 0);
	m_CE_PAXF.SetTypeToString( CString("###"), 3, 0);
	m_CE_PAX_ID.SetTypeToString( CString("###"), 3, 0);
	m_CE_PAX_INF.SetTypeToString( CString("###"), 3, 0);


	m_CE_MAIL.SetTypeToString( CString("######"), 6, 0);
	m_CE_CGOT.SetTypeToString( CString("######"), 6, 0);
	m_CE_BAGW.SetTypeToString( CString("#######"), 7, 0);
	m_CE_BAGN.SetTypeToString( CString("######"), 6, 0);


	m_CE_LDM_PAX1.SetTypeToString( CString("###"), 3, 0);
	m_CE_LDM_PAX2.SetTypeToString( CString("###"), 3, 0);
	m_CE_LDM_PAX3.SetTypeToString( CString("###"), 3, 0);
	m_CE_LDM_PAXT.SetTypeToString( CString("###"), 3, 0);
	m_CE_MVT_PAXI.SetTypeToString( CString("###"), 3, 0);
	m_CE_PTM_PAXF.SetTypeToString( CString("###"), 3, 0);


	// set editfield status
	m_CE_PAX1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Load"));	
	m_CE_PAX2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Load"));	
	m_CE_PAX3.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Load"));	
	m_CE_PAXT.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Load"));	
	m_CE_PAXI.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Load"));	
	m_CE_PAXF.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Load"));	
	m_CE_PAX_ID.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Load"));	
	m_CE_PAX_INF.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Load"));	
	m_CE_MAIL.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Load"));	
	m_CE_CGOT.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Load"));	
	m_CE_BAGW.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Load"));	
	m_CE_BAGN.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Load"));	


	m_CE_LDM_PAX1.EnableWindow(false);
	m_CE_LDM_PAX2.EnableWindow(false);
	m_CE_LDM_PAX3.EnableWindow(false);
	m_CE_LDM_PAXT.EnableWindow(false);
	m_CE_MVT_PAXI.EnableWindow(false);
	m_CE_PTM_PAXF.EnableWindow(false);


	// set sita pax data
	m_CE_LDM_PAX1.SetInitText(prmFlight->Pax1);
	m_CE_LDM_PAX2.SetInitText(prmFlight->Pax2);
	m_CE_LDM_PAX3.SetInitText(prmFlight->Pax3);
	m_CE_LDM_PAXT.SetInitText(prmFlight->Paxt);
	m_CE_MVT_PAXI.SetInitText(prmFlight->Paxi);
	m_CE_PTM_PAXF.SetInitText(prmFlight->Paxf);


	// set user load data
	m_CE_MAIL.SetInitText(prmFlight->Mail);
	m_CE_CGOT.SetInitText(prmFlight->Cgot);
	m_CE_BAGW.SetInitText(prmFlight->Bagw);
	m_CE_BAGN.SetInitText(prmFlight->Bagn);


	// set user pax data
	ogApxData.Read(prmFlight->Urno);
	
	
	APXDATA *prlApx;

	CString olPax1;
	prlApx = ogApxData.GetApx("PA1");
	if(prlApx != NULL)
	{
		m_CE_PAX1.SetInitText(prlApx->Paxc);
		olPax1 = prlApx->Paxc;
	}
	CString olPax2;
	prlApx = ogApxData.GetApx("PA2");
	if(prlApx != NULL)
	{
		m_CE_PAX2.SetInitText(prlApx->Paxc);
		olPax2 = prlApx->Paxc;
	}
	CString olPax3;
	prlApx = ogApxData.GetApx("PA3");
	if(prlApx != NULL)
	{
		m_CE_PAX3.SetInitText(prlApx->Paxc);
		olPax3 = prlApx->Paxc;
	}
	prlApx = ogApxData.GetApx("PAI");
	if(prlApx != NULL)
	{
		m_CE_PAXI.SetInitText(prlApx->Paxc);
	}
	prlApx = ogApxData.GetApx("PAF");
	if(prlApx != NULL)
	{
		m_CE_PAXF.SetInitText(prlApx->Paxc);
	}
	CString olPaxT;
	prlApx = ogApxData.GetApx("PAT");
	if(prlApx != NULL)
	{
		m_CE_PAXT.SetInitText(prlApx->Paxc);
		olPaxT = prlApx->Paxc;
	}
	CString olPaxInf;
	prlApx = ogApxData.GetApx("INF");
	if(prlApx != NULL)
	{
		m_CE_PAX_INF.SetInitText(prlApx->Paxc);
		olPaxInf = CString(prlApx->Paxc);
	}

	prlApx = ogApxData.GetApx("ID");
	if(prlApx != NULL)
	{
		m_CE_PAX_ID.SetInitText(prlApx->Paxc);
	}


	bmAutoCalcPax = true;
	bmAutoCalcLoad = true;



	if( atoi(olPax1) + atoi(olPax2) + atoi(olPax3) + atoi(olPaxInf) != atoi(olPaxT))
	{
		bmAutoCalcPax = false;
		m_CE_PAXT.SetBKColor(RGB(255,0,0));

	}

	if( atoi(prmFlight->Mail) + atoi(prmFlight->Cgot) + atoi(prmFlight->Bagn) != atoi(prmFlight->Bagw))
	{
		bmAutoCalcLoad = false;
		m_CE_BAGW.SetBKColor(RGB(255,0,0));

	}




	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


LONG RotationLoadDlgATH::OnEditKillfocus( UINT wParam, LPARAM lParam)
{
	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY*)lParam;
	CString olTmp;

	// remove leading nulls
	prlNotify->SourceControl->GetWindowText(olTmp);
	olTmp.TrimLeft('0');
	prlNotify->SourceControl->SetWindowText(olTmp);


	if((UINT)m_CE_PAXT.imID == wParam)
	{
		if(m_CE_PAXT.IsChanged())
		{
			bmAutoCalcPax = false;
			m_CE_PAXT.SetBKColor(RGB(255,0,0));

		}
	}

	
	if((UINT)m_CE_BAGW.imID == wParam)
	{
		if(m_CE_BAGW.IsChanged())
		{
			bmAutoCalcLoad = false;
			m_CE_BAGW.SetBKColor(RGB(255,0,0));

		}
	}



	char buffer[64];
	
	long llTotal = 0;

	if(bmAutoCalcPax)
	{
		m_CE_PAX_INF.GetWindowText(olTmp);
		llTotal += atoi(olTmp);

		m_CE_PAX1.GetWindowText(olTmp);
		llTotal += atoi(olTmp);
		
		m_CE_PAX2.GetWindowText(olTmp);
		llTotal += atoi(olTmp);
		
		m_CE_PAX3.GetWindowText(olTmp);
		llTotal += atoi(olTmp);

		ltoa(llTotal, buffer, 10);

		m_CE_PAXT.SetWindowText(buffer);
	}

	llTotal = 0;

	if(bmAutoCalcLoad)
	{

		m_CE_MAIL.GetWindowText(olTmp);
		llTotal += atoi(olTmp);
		
		m_CE_BAGN.GetWindowText(olTmp);
		llTotal += atoi(olTmp);
		
		m_CE_CGOT.GetWindowText(olTmp);
		llTotal += atoi(olTmp);

		ltoa(llTotal, buffer, 10);

		m_CE_BAGW.SetWindowText(buffer);
	}




	return 0L;
}