// AbfluegelvgTableViewer.cpp 
//
//	Statistik Liste aller Abfl�ge pro LVG geordnet nach LVG/Zielflughafen
#include <stdafx.h>
#include <Report8TableViewer.h>
#include <CcsGlobl.h>

#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// AbfluegelvgTableViewer
//
AbfluegelvgTableViewer::AbfluegelvgTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo)
{
	pomData = popData;
	pcmInfo = pcpInfo;

	bmIsFromSearch = FALSE;
    pomAbfluegelvgTable = NULL;
	imLineno = 0;
}

//-----------------------------------------------------------------------------------------------

AbfluegelvgTableViewer::~AbfluegelvgTableViewer()
{
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void AbfluegelvgTableViewer::Attach(CCSTable *popTable)
{
    pomAbfluegelvgTable = popTable;
}

//-----------------------------------------------------------------------------------------------
void AbfluegelvgTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------
void AbfluegelvgTableViewer::MakeLines()//Des3 Alc2
{
	int ilCntr = 0;
	int ilLvgCntr = 0;
	CString olNewstr("");
	CString olOldstr(""); 
	CString olNewLvg("");
	CString olOldLvg("");

	int ilCount = pomData->GetSize();

	for (int ilLc = 0; ilLc <= ilCount - 1 ; ilLc++)
	{
		ilCntr++;
		ilLvgCntr++;
		ROTATIONDLGFLIGHTDATA *prlAbfluegelvgData = &pomData->GetAt(ilLc);
		olNewstr = prlAbfluegelvgData->Des3;
		if (strlen(prlAbfluegelvgData->Alc2))
				olNewLvg = prlAbfluegelvgData->Alc2;
		else
				olNewLvg = prlAbfluegelvgData->Alc3;

		if(ilLc == 0)	// der erste
		{
			olOldstr = olNewstr;
			if (strlen(prlAbfluegelvgData->Alc2))
				olOldLvg = olNewLvg;
		}
		if (olNewstr != olOldstr)	// neue Destination
		{
			MakeLine(olOldstr, olOldLvg, ilCntr);
			olOldstr = olNewstr;
			ilCntr = 0;
		}
		if (olOldLvg != olNewLvg)	// neue LVG
		{
			if(ilCntr > 0)
			{
				MakeLine(olOldstr, olOldLvg, ilCntr);
				olOldstr = olNewstr;
				ilCntr = 0;
			}
			//MakeLine(" ", " ", 0);
			MakeResultLine(GetString(IDS_STRING1200), olOldLvg, ilLvgCntr);
			ilLvgCntr = 0;
			olOldLvg = olNewLvg;
		}
		if((ilLc == ilCount - 1) && (ilCntr > 0))	// nach dem letzten
		{
			MakeLine(olNewstr, olNewLvg, ilCntr);
			MakeResultLine(GetString(IDS_STRING1200), olNewLvg, ilLvgCntr);
		}
	}
}

//-----------------------------------------------------------------------------------------------
// neue Destination
void AbfluegelvgTableViewer::MakeLine(CString opNewstr, CString opNewLvg, int ipCntr)
{
    // Update viewer data for this shift record
	char clbuffer[10];
    ABFLUEGELVGTABLE_LINEDATA rlAbfluegelvg;
		sprintf(clbuffer,"%d", ipCntr);
		rlAbfluegelvg.Cntr = clbuffer;
		rlAbfluegelvg.Alc3 = opNewLvg;
		rlAbfluegelvg.Des3 = opNewstr; 
		
	CreateLine(&rlAbfluegelvg);
}




//-----------------------------------------------------------------------------------------------
// neue LVG
void AbfluegelvgTableViewer::MakeResultLine(CString opNewstr, CString opNewLvg, int ipCntr)
{
    // Update viewer data for this shift record
	char clbuffer[10];
    ABFLUEGELVGTABLE_LINEDATA rlAbfluegelvg;
		sprintf(clbuffer,"%d", ipCntr);
		rlAbfluegelvg.Alc3 = opNewLvg;
		rlAbfluegelvg.Gsmt = clbuffer; 
	CreateLine(&rlAbfluegelvg);

		rlAbfluegelvg.Alc3 = "";
		rlAbfluegelvg.Gsmt = ""; 
	CreateLine(&rlAbfluegelvg);
}


//-----------------------------------------------------------------------------------------------

void AbfluegelvgTableViewer::CreateLine(ABFLUEGELVGTABLE_LINEDATA *prpAbfluegelvg)
{
	int ilLineno = 0;
	ABFLUEGELVGTABLE_LINEDATA rlAbfluegelvg;
	rlAbfluegelvg = *prpAbfluegelvg;
    omLines.NewAt(omLines.GetSize(), rlAbfluegelvg);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void AbfluegelvgTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 2;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomAbfluegelvgTable->SetShowSelection(TRUE);
	pomAbfluegelvgTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;
	rlHeader.Length = 100; 
	rlHeader.Text = GetString(IDS_STRING1136);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 150;
	rlHeader.Text = GetString(IDS_STRING1137);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 100;
	rlHeader.Text = GetString(IDS_STRING1138);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 100;
	rlHeader.Text = GetString(IDS_STRING1139);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomAbfluegelvgTable->SetHeaderFields(omHeaderDataArray);

	//pomAbfluegelvgTable->SetDefaultSeparator();
	pomAbfluegelvgTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
//SEPA_LIKEVERTICAL

    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		// Trennlinie nach Gesamtsumme
		if(!omLines[ilLineNo].Gsmt.IsEmpty())
		{
			rlColumnData.SeparatorType = SEPA_NONE;
			rlColumnData.HorizontalSeparator = SEPA_LIKEVERTICAL;
			rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;
		}

		rlColumnData.Text = omLines[ilLineNo].Alc3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Des3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Cntr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Gsmt;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomAbfluegelvgTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomAbfluegelvgTable->DisplayTable();
}


//-----------------------------------------------------------------------------------------------

void AbfluegelvgTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

void AbfluegelvgTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void AbfluegelvgTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	char pclFooter[150];
	sprintf(pclFooter, GetString(IDS_STRING1140), (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));
	omTableName = GetString(IDS_STRING1055);
	omFooterName = pclFooter;
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format(GetString(IDS_STRING1445),ilLines,omFooterName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;

	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool AbfluegelvgTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	char pclHeader[256];
//	sprintf(pclHeader, GetString(IDS_STRING1141), pcmInfo);
	sprintf(pclHeader, GetString(IDS_STRING1141), pcmInfo);


	//sprintf(pclHeader, "  vom %s", pcmInfo);
//	CString olTableName(pclHeader);
	//mPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);
	pomPrint->PrintUIFHeader(" ",CString(pclHeader),pomPrint->imFirstLine-10);

//	pomPrint->PrintUIFHeader(" "," ",pomPrint->imFirstLine-10); 


	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Text   = omHeaderDataArray[i].Text;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool AbfluegelvgTableViewer::PrintTableLine(ABFLUEGELVGTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(!prpLine->Gsmt.IsEmpty())
			{
				rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
			}
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				{
					rlElement.FrameLeft  = PRINT_FRAMETHIN;
					rlElement.Text = prpLine->Alc3;
				}
				break;
			case 1:
				{
					rlElement.FrameRight = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Des3;
				}
				break;
			case 2:
				{
					rlElement.FrameRight = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Cntr;

				}
				break;
			case 3:
				{
					rlElement.FrameRight = PRINT_FRAMETHIN;
					rlElement.Text = prpLine->Gsmt;
				}
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------

// Drucken in Datei
bool AbfluegelvgTableViewer::PrintPlanToFile(char *opTrenner)
{
	ofstream of;

	char* tmpvar;
	tmpvar = getenv("TMP");
	if (tmpvar == NULL)
		tmpvar = getenv("TEMP");

	if (tmpvar == NULL)
		return false;

	CString olFileName = omTableName;
	olFileName.Remove('*');
	olFileName.Remove('.');
	olFileName.Remove(':');
	olFileName.Remove('/');
	olFileName.Replace(" ", "_");
	
	char pHeader[256];
	strcpy (pHeader, tmpvar);
	CString path = pHeader;
	omFileName =  path + "\\" + olFileName + ".csv";

//	of.open( GetString(IDS_STRING1386), ios::out);
	of.open( omFileName, ios::out);

	int ilwidth = 1;

	of  << setw(ilwidth) << omTableName << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;

	of  << setw(ilwidth) << GetString(IDS_STRING1136)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING1137)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING1138)
		<< setw(1) << opTrenner
		<< setw(ilwidth) << GetString(IDS_STRING1139)
		<< endl;

	int ilCount = omLines.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		ABFLUEGELVGTABLE_LINEDATA rlD = omLines[i];
		of.setf(ios::left, ios::adjustfield);
		of		<< setw(1) << rlD.Alc3  
			 << setw(1) << opTrenner 
				<< setw(1) << rlD.Des3  
			 << setw(1) << opTrenner 
				<< setw(1) << rlD.Cntr  
			 << setw(1) << opTrenner 
				<< setw(1) << rlD.Gsmt
				<< endl;
		if(!rlD.Gsmt.IsEmpty())
			of << "------------------------------------------------------------" << endl;
	}
	of.close();
	return true;

/*
		ofstream of;
		of.open( pcpDefPath, ios::out);
		//of.open( "c:\\tmp\\Abfluegelvg.txt", ios::out);
		of << GetString(IDS_STRING1142) << " " 
			<< pcmInfo << "    "
			<< CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M") << endl << endl;

		of  << GetString(IDS_STRING1136) << "       " 
			<< GetString(IDS_STRING1137) << "   " 
			<< GetString(IDS_STRING1138) << "  " 
			<< GetString(IDS_STRING1139) << "    " 
			<< endl;

		of << "------------------------------------------------------------" << endl;
		int ilCount = omLines.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			ABFLUEGELVGTABLE_LINEDATA rlD = omLines[i];
			of.setf(ios::left, ios::adjustfield);
			of   << setw(10) << rlD.Alc3  
				 << setw(16) << rlD.Des3  
				<< setw(10) << rlD.Cntr  
				<< setw(10) << rlD.Gsmt << endl;
			if(!rlD.Gsmt.IsEmpty())
				of << "------------------------------------------------------------" << endl;
		}
		of.close();
*/
//	MessageBox("Es wurde folgende Datei geschrieben:\nc:\\tmp\\Abfluegelvg.txt", "Drucken",(MB_ICONINFORMATION|MB_OK));
}

