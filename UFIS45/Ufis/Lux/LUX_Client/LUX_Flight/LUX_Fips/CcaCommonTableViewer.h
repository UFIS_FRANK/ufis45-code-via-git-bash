#ifndef __CHECKINTABLEVIEWER_H__
#define __CHECKINTABLEVIEWER_H__

#include <stdafx.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CedaCcaData.h>


struct CHECKINTABLE_LINEDATA
{
	long		Urno; 	// Eindeutige Datensatz-Nr.
	CString		Ckic;	// Schaltername
	CString 	Ckit;	// Terminal
	CString	 	Flnu;	// Datensatznr. des zugeteilten Fluges ->Fluggesellschaft (Althaj.Urno)
	CString	 	Alc;	// Datensatznr. des zugeteilten Fluges ->Fluggesellschaft (Althaj.Urno)
	//CTime		Ckba;	// Schalteröffnungszeit Beginn
	//CTime	 	Ckea;	// Schalteröffnungszeit Ende
	CTime		Ckbs;	// Schalteroeffnungszeit geplanter Begin
	CTime		Ckes;	// Schalteroeffnungszeit geplantes Ende
	CString 	Ctyp;	// Check-In Typ  = 'C'
	CString 	Rem1;	// Bemerkung, wurde in der CCA-Tabelle ergänzt.

};

/////////////////////////////////////////////////////////////////////////////
// CcaCommonTableViewer

class CcaCommonTableViewer : public CViewer
{
// Constructions
public:
    CcaCommonTableViewer();
    ~CcaCommonTableViewer();

	void ClearAll();

	int CompareCCA(CHECKINTABLE_LINEDATA *prpLine1, CHECKINTABLE_LINEDATA *prpLine2);

	void ReadAllFlights(const CString &ropWhere);
	CCADATA *GetCcaByUrno(long lpCcaUrno);
	CCADATA *GetCcaByLineNo(int ipLineNo);
	bool DeleteCca(long lpCcaUrno);

    void Attach(CCSTable *popAttachWnd);
    void UpdateNewSearch();
    virtual void ChangeViewTo(const char *pcpViewName);

	void DeleteAll();
	void CreateLine(CHECKINTABLE_LINEDATA *prpCheckIn);
	void DeleteLine(int ipLineno);
	bool IsPassFilter(CCADATA *prlCheckInData);
	void ProcessCheckInChange(CCADATA *prpCheckInData);
	void ProcessCheckInDelete(CCADATA *prlCheckInData);
	void UpdateDisplay();
	
// Internal data processing routines
private:
    void MakeLines();
	void MakeLine(CCADATA *prlCheckInData);
	bool FindLine(long lpUrno, int &rilLineno);
	int CompareCheckIn(CHECKINTABLE_LINEDATA *prpCheckIn1, CHECKINTABLE_LINEDATA *prpCheckIn2);
	void SetTableSort();

	
	bool bmIsFromSearch;
	int imLineno;
    CCSTable *pomCheckInTable;

	CedaCcaData omCcaData;

//public:
    CCSPtrArray<CHECKINTABLE_LINEDATA> omLines;
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
	CStringArray omSort;


};

#endif //__CHECKINTABLEVIEWER_H__
