#ifndef AFX_REPORTSEQDATETIMEDLG_H__8BBC00F3_8B6C_11D1_8126_0000B43C4B01__INCLUDED_
#define AFX_REPORTSEQDATETIMEDLG_H__8BBC00F3_8B6C_11D1_8126_0000B43C4B01__INCLUDED_

// ReportSeqDateTimeDlg.h : Header-Datei
//
#include <CCSEdit.h>
#include <resrc1.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CReportSeqDateTimeDlg 

class CReportSeqDateTimeDlg : public CDialog
{
// Konstruktion
public:
	CReportSeqDateTimeDlg(CWnd* popParent, const CString &ropHeadline, CTime* popMinDate, CTime *popMaxDate);
 
	CTime *pomMinDate;
	CTime *pomMaxDate;
// Dialogfelddaten
	//{{AFX_DATA(CReportSeqDateTimeDlg)
	enum { IDD = IDD_REPORTSEQDATETIME };
	CCSEdit	m_CE_DateFrom;
	CCSEdit	m_CE_DateTo;
	CCSEdit	m_CE_TimeFrom;
	CCSEdit	m_CE_TimeTo;
	CString	m_DateFrom;
	CString	m_DateTo;
	CString	m_TimeFrom;
	CString	m_TimeTo;
	//}}AFX_DATA
	CString omHeadline;

 	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CReportSeqDateTimeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CReportSeqDateTimeDlg)
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_REPORTSEQDATETIMEDLG_H__8BBC00F3_8B6C_11D1_8126_0000B43C4B01__INCLUDED_
