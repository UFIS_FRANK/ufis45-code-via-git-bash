// ccsunisortpage.cpp : implementation file
//

#include <stdafx.h>
#include <CCSGlobl.h>
#include <PSunisortpage.h>
//#include "CCSItem.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPSUniSortPage property page

IMPLEMENT_DYNCREATE(CPSUniSortPage, CPropertyPage)

CPSUniSortPage::CPSUniSortPage() : CPropertyPage(CPSUniSortPage::IDD)
{
	//{{AFX_DATA_INIT(CPSUniSortPage)
	m_EditSort = _T("");
	//}}AFX_DATA_INIT
	//pomPageBuffer = (CPageBuffer *)pomDataBuffer;
}

CPSUniSortPage::~CPSUniSortPage()
{
	//pomPageBuffer = NULL;
}

void CPSUniSortPage::SetCalledFrom(CString opCalledFrom)
{
	omCalledFrom = opCalledFrom;
}

void CPSUniSortPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPSUniSortPage)
	DDX_Control(pDX, IDC_LIST_FIELDS, m_ListFields);
	//DDX_Text(pDX, IDC_EDIT_SORT, m_EditSort);
	DDX_Control(pDX, IDC_EDIT_SORT, E_SortText);
	//}}AFX_DATA_MAP
	if (pDX->m_bSaveAndValidate == FALSE)
	{
		SetData();
	}
	if (pDX->m_bSaveAndValidate == TRUE)
	{
		GetData();
	}
}


BEGIN_MESSAGE_MAP(CPSUniSortPage, CPropertyPage)
	//{{AFX_MSG_MAP(CPSUniSortPage)
	ON_BN_CLICKED(IDC_BUTTON_ASC, OnButtonAsc)
	ON_BN_CLICKED(IDC_BUTTON_DELETE, OnButtonDelete)
	ON_BN_CLICKED(IDC_BUTTON_DESC, OnButtonDesc)
	ON_BN_CLICKED(IDC_BUTTON_NEW, OnButtonNew)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPSUniSortPage message handlers

void CPSUniSortPage::GetData()
{
	omValues.RemoveAll();
	E_SortText.SetWindowText(m_EditSort);
	CString olSubString = m_EditSort;
	if(!olSubString.IsEmpty())
	{
		int pos;
		int olPos = 0;
		bool blEnd = false;
		CString olText;
		while(blEnd == false)
		{
			pos = olSubString.Find(' ');
			if(pos == -1)
			{
				blEnd = true;
				olText = olSubString;
			}
			else
			{
				olText = olSubString.Mid(0, olSubString.Find(' '));
				olSubString = olSubString.Mid(olSubString.Find(' ')+1, olSubString.GetLength( )-olSubString.Find(' ')+1);
			}
			omValues.Add(olText);
		}
	}
}

void CPSUniSortPage::SetData()
{
	m_ListFields.SetCurSel(-1);
	E_SortText.SetWindowText("");
	m_EditSort = "";
	for(int i = 0; i < omValues.GetSize(); i++)
	{
		m_EditSort += omValues[i] + CString(" ");
	}
	//m_EditSort.TrimRight();
	E_SortText.SetWindowText(m_EditSort);
}

void CPSUniSortPage::InitMask()
{
	SetFieldAndDescriptionArrays();
	int ilCount = omFieldArray.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		char pclText[200]=""; 
		sprintf(pclText, "%s...%s", omFieldArray[i], omDescArray[i]);
		m_ListFields.AddString(pclText);
	}
} 

void CPSUniSortPage::OnButtonAsc() 
{
	E_SortText.GetWindowText(m_EditSort);
	CString olFilter;
	int ilIdx = m_ListFields.GetCurSel();
	if(ilIdx != LB_ERR)
	{
		olFilter = omFieldArray[ilIdx];
	}
	if(m_EditSort.Find(olFilter) == -1)
	{
		m_EditSort+=olFilter + "+ ";
	}
	E_SortText.SetWindowText(m_EditSort);
	E_SortText.LineScroll(E_SortText.GetLineCount(), 0 );
}

void CPSUniSortPage::OnButtonDesc() 
{

	CString olFilter;
	E_SortText.GetWindowText(m_EditSort);
	int ilIdx = m_ListFields.GetCurSel();
	if(ilIdx != LB_ERR)
	{
		olFilter = omFieldArray[ilIdx];
	}
	if(m_EditSort.Find(olFilter) == -1)
	{
		m_EditSort+=olFilter + "- ";
	}
	E_SortText.SetWindowText(m_EditSort);
	E_SortText.SetWindowText(m_EditSort);
	E_SortText.LineScroll(E_SortText.GetLineCount(), 0 );
}

void CPSUniSortPage::OnButtonDelete() 
{
	CString olFilter;
	E_SortText.GetWindowText(m_EditSort);
	int ilIdx = m_ListFields.GetCurSel();
	if(ilIdx != LB_ERR)
	{
		olFilter = omFieldArray[ilIdx];
		int ilPos = m_EditSort.Find(olFilter);
		if(ilPos != -1)
		{
			CString olP1 = m_EditSort.Left(ilPos);
			CString olP2 = m_EditSort.Right((m_EditSort.GetLength()-1) - (ilPos+5));
			m_EditSort = olP1 + olP2;
			E_SortText.SetWindowText(m_EditSort);
		}
	}
	E_SortText.SetWindowText(m_EditSort);
	E_SortText.LineScroll(E_SortText.GetLineCount(), 0 );
}

void CPSUniSortPage::OnButtonNew() 
{
	E_SortText.SetWindowText("");
	m_EditSort = CString("");
}

BOOL CPSUniSortPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	m_ListFields.SetFont(&ogCourier_Regular_8);
	InitMask();	
	E_SortText.SetReadOnly(!bgViewEditSort);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CPSUniSortPage::SetFieldAndDescriptionArrays()
{
	if(omCalledFrom == "SEASON")
	{
		omFieldArray.Add("AFLNO");
		omFieldArray.Add("AJCNT");
		omFieldArray.Add("ASTOA");
		omFieldArray.Add("ADOOA");
		omFieldArray.Add("ATTYP");
		omFieldArray.Add("ASTEV");
		omFieldArray.Add("ASTYP");
		omFieldArray.Add("AORG3");
		omFieldArray.Add("AVIAL");
		omFieldArray.Add("APSTA");
		omFieldArray.Add("AGTA1");
		omFieldArray.Add("AGTA2");
		omFieldArray.Add("ABEMS");
		omFieldArray.Add("ACT3");
		omFieldArray.Add("DFLNO");
		omFieldArray.Add("DJCNT");
		omFieldArray.Add("DSTOD");
		omFieldArray.Add("DDOOD");
		omFieldArray.Add("DTTYP");
		omFieldArray.Add("DSTEV");
		omFieldArray.Add("DSTYP");
		omFieldArray.Add("DDES3");
		omFieldArray.Add("DVIAL");
		omFieldArray.Add("DPSTD");
		omFieldArray.Add("DGTD1");
		omFieldArray.Add("DGTD2");
		omFieldArray.Add("DBEMS");

		omDescArray.Add(GetString(IDS_STRING868));
		omDescArray.Add(GetString(IDS_STRING869));
		omDescArray.Add(GetString(IDS_STRING870));
		omDescArray.Add(GetString(IDS_STRING871));
		omDescArray.Add(GetString(IDS_STRING872));
		omDescArray.Add(GetString(IDS_STRING873));
		omDescArray.Add(GetString(IDS_STRING874));
		omDescArray.Add(GetString(IDS_STRING875));
		omDescArray.Add(GetString(IDS_STRING876));
		omDescArray.Add(GetString(IDS_STRING877));
		omDescArray.Add(GetString(IDS_STRING878));
		omDescArray.Add(GetString(IDS_STRING879));
		omDescArray.Add(GetString(IDS_STRING880));
		omDescArray.Add(GetString(IDS_STRING881));
		omDescArray.Add(GetString(IDS_STRING882));
		omDescArray.Add(GetString(IDS_STRING883));
		omDescArray.Add(GetString(IDS_STRING884));
		omDescArray.Add(GetString(IDS_STRING885));
		omDescArray.Add(GetString(IDS_STRING886));
		omDescArray.Add(GetString(IDS_STRING887));
		omDescArray.Add(GetString(IDS_STRING888));
		omDescArray.Add(GetString(IDS_STRING889));
		omDescArray.Add(GetString(IDS_STRING890));
		omDescArray.Add(GetString(IDS_STRING891));
		omDescArray.Add(GetString(IDS_STRING892));
		omDescArray.Add(GetString(IDS_STRING893));
		omDescArray.Add(GetString(IDS_STRING894));

	}//end if("SAISON")

///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

	if(omCalledFrom == "ROTATION")
	{
		omFieldArray.Add("FLNO");
		omFieldArray.Add("CSGN");
		omFieldArray.Add("ORG4");
		omFieldArray.Add("VIAL");
		omFieldArray.Add("IFRA");
		omFieldArray.Add("TTYP");
		omFieldArray.Add("STOA");
		omFieldArray.Add("ETOA");
		omFieldArray.Add("TMOA");
		omFieldArray.Add("LAND");
		omFieldArray.Add("RWYA");
		omFieldArray.Add("ONBE");
		omFieldArray.Add("ONBL");
		omFieldArray.Add("PSTA");
		omFieldArray.Add("GTA1");
		omFieldArray.Add("REGN");
		omFieldArray.Add("ACT5");
		omFieldArray.Add("RFLN");
		omFieldArray.Add("AEND");
		omFieldArray.Add("ISRE");
		omFieldArray.Add("REMP");
		omFieldArray.Add("DES4");
		omFieldArray.Add("IFRD");
		omFieldArray.Add("STOD");
		omFieldArray.Add("ETOD");
		omFieldArray.Add("OFBL");
		omFieldArray.Add("CTOT");
		omFieldArray.Add("RWYD");
		omFieldArray.Add("AIRB");
		omFieldArray.Add("PSTD");
		omFieldArray.Add("GTD1");
		omFieldArray.Add("GTD2");
		omFieldArray.Add("RONB");
		omFieldArray.Add("ETDC");

		omDescArray.Add(GetString(IDS_STRING895));
		omDescArray.Add(GetString(IDS_STRING515));
		omDescArray.Add(GetString(IDS_STRING896));
		omDescArray.Add(GetString(IDS_STRING897));
		omDescArray.Add(GetString(IDS_STRING518));
		omDescArray.Add(GetString(IDS_STRING814));
		omDescArray.Add(GetString(IDS_STRING870));
		omDescArray.Add(GetString(IDS_STRING842));
		omDescArray.Add(GetString(IDS_STRING535));
		omDescArray.Add(GetString(IDS_STRING536));
		omDescArray.Add(GetString(IDS_STRING521));
		omDescArray.Add(GetString(IDS_STRING537));
		omDescArray.Add(GetString(IDS_STRING538));
		omDescArray.Add(GetString(IDS_STRING675));
		omDescArray.Add(GetString(IDS_STRING689));
		omDescArray.Add(GetString(IDS_STRING341));
		omDescArray.Add(GetString(IDS_STRING524));
		omDescArray.Add(GetString(IDS_STRING898));
		omDescArray.Add(GetString(IDS_STRING525));
		omDescArray.Add(GetString(IDS_STRING526));
		omDescArray.Add(GetString(IDS_STRING527));
		omDescArray.Add(GetString(IDS_STRING544));
		omDescArray.Add(GetString(IDS_STRING545));
		omDescArray.Add(GetString(IDS_STRING546));
		omDescArray.Add(GetString(IDS_STRING760));
		omDescArray.Add(GetString(IDS_STRING548));
		omDescArray.Add(GetString(IDS_STRING549));
		omDescArray.Add(GetString(IDS_STRING550));
		omDescArray.Add(GetString(IDS_STRING551));
		omDescArray.Add(GetString(IDS_STRING791));
		omDescArray.Add(GetString(IDS_STRING892));
		omDescArray.Add(GetString(IDS_STRING444));
		omDescArray.Add(GetString(IDS_STRING899));
		omDescArray.Add(GetString(IDS_STRING547));
	}
}


void CPSUniSortPage::InitFieldAndDescArrays(const CStringArray &opFieldArray,
		                                    const CStringArray &opDescArray)
{

	// nur bei DAILY werden die Arrays dynamisch gesetzt
	if (omCalledFrom == "DAILY")
	{
		// alten Inhalt loeschen
		omDescArray.RemoveAll();
		omFieldArray.RemoveAll();

		// neuen Inhalt setzten
		omFieldArray.Copy(opFieldArray);
		omDescArray.Copy(opDescArray);
	}

}
