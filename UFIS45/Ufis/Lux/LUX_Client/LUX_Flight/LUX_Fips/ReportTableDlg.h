#ifndef AFX_REPORTTABLEDLG_H__CDB257E5_8BF5_11D1_8127_0000B43C4B01__INCLUDED_
#define AFX_REPORTTABLEDLG_H__CDB257E5_8BF5_11D1_8127_0000B43C4B01__INCLUDED_

// ReportTableDlg.h : Header-Datei
//
#include <CCSTable.h>
#include <Table.h>
#include <RotationDlgCedaFlightData.h>
#include <Report1TableViewer.h>
#include <Report2TableViewer.h>
#include <Report3TableViewer.h>
#include <Report4TableViewer.h>
#include <Report5TableViewer.h>
#include <Report6TableViewer.h>
#include <Report7TableViewer.h>
#include <Report8TableViewer.h>
#include <Report9TableViewer.h>
#include <Report10TableViewer.h>
#include <Report11TableViewer.h>
#include <Report12TableViewer.h>
#include <Report13TableViewer.h>
#include <Report14TableViewer.h>
#include <Report15TableViewer.h>
#include <Report16TableViewer.h>

#include <Report17DailyTableViewer.h>
#include <Report19DailyCcaTableViewer.h>

// SHA
#include <ReportSameORGTableViewer.h>
#include <ReportSameDESTableViewer.h>
#include <ReportSameACTTableViewer.h>
#include <ReportSameREGNTableViewer.h>
#include <ReportSameTTYPTableViewer.h>
#include <ReportSameALTableViewer.h>
#include <ReportSameDCDTableViewer.h>
#include <ReportSameDTDTableViewer.h>
#include <ReportSameFTYPXTableViewer.h>
#include <ReportArrDepTableViewer.h>
#include <ReportArrDepLoadPaxTableViewer.h>
#include <ReportSameTimeFrameTableViewer.h>
#include <ReportOvernightTableViewer.h>
#include <ReportFlightDomIntMixTableViewer.h>
#include <ReportSeatDomIntMixTableViewer.h>
#include <ReportFlightGPUTableViewer.h>
#include <ReportBltAllocViewer.h>
#include <ReportDailyFlightLogViewer.h>
#include <ReportFlightDelayViewer.h>


/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CReportTableDlg 

class CReportTableDlg : public CDialog
{
// Konstruktion
public:
	CReportTableDlg(CWnd* pParent = NULL, int igTable = 0, CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData = NULL,
					char *pcpInfo = NULL, char *pcpSelect = NULL, char *pcpInfo2 = NULL, int ipArrDep = 0, RotationDlgCedaFlightData* popRotDlgData = NULL, char *pcpInfoPlus = NULL);

	~CReportTableDlg();

// Dialogfelddaten
	//{{AFX_DATA(CReportTableDlg)
	enum { IDD = IDD_REPORTTABLE };
	CButton	m_CB_Beenden;
	CButton	m_CB_Papier;
	CButton	m_CB_Drucken;
	CButton	m_CB_Datei;
	//}}AFX_DATA
	CString omHeadline;	//Dialogbox-Überschrift


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CReportTableDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

public:

	int imDialogBarHeight;

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CReportTableDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDatei();
	afx_msg void OnDrucken();
	afx_msg void OnPapier();
	afx_msg void OnBeenden();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CCSTable *pomReportTable;
	CTable *pomReportCTable;
	int miTable;
	char *pcmInfo;
	char *pcmInfo2;
	char *pcmInfoPlus;
	char *pcmSelect;
	CCSPtrArray<ROTATIONDLGFLIGHTDATA> *pomData;
	RotationDlgCedaFlightData* pomRotDlgData;

	GefundenefluegeTableViewer	*pomGefundenefluegeViewer;
	AbfluegeTableViewer	*pomAbfluegeViewer;
	FluegezeitraumTableViewer	*pomFluegezeitraumViewer;
	MessedaystopTableViewer	*pomMessedaystopViewer;
	AllemessefluegeTableViewer	*pomAllemessefluegeViewer;
	MessefluegeTableViewer	*pomMessefluegeViewer;
	StatistikMesseTableViewer	*pomStatistikMesseViewer;
	AbfluegelvgTableViewer	*pomAbfluegelvgViewer;
	SaisonflugplanTableViewer	*pomSaisonflugplanlvgViewer;
	FlughaefenTableViewer	*pomFlughaefenViewer;
	ArbeitsflugplanTableViewer	*pomArbeitsflugplanViewer;
	SitzplaetzeTableViewer	*pomSitzplaetzeViewer;
	LvgTableViewer	*pomLvgViewer;
	DesTableViewer	*pomDesViewer;
	Report15TableViewer	*pomReport15Viewer;
	Report16TableViewer	*pomReport16Viewer;
	bool bmCommonDlg;

	// SHA
	ReportSameALTableViewer*    pomReportSameALTableViewer;
	ReportSameORGTableViewer*   pomReportSameORGTableViewer;
	ReportSameDESTableViewer*   pomReportSameDESTableViewer;
	ReportSameACTTableViewer*   pomReportSameACTTableViewer;
	ReportSameREGNTableViewer*  pomReportSameREGNTableViewer;
	ReportSameTTYPTableViewer*  pomReportSameTTYPTableViewer;
	ReportSameDCDTableViewer*   pomReportSameDCDTableViewer;
	ReportSameDTDTableViewer*   pomReportSameDTDTableViewer;
	ReportSameFTYPXTableViewer* pomReportSameFTYPXTableViewer;
	ReportArrDepTableViewer*    pomReportArrDepTableViewer;
	ReportArrDepLoadPaxTableViewer*    pomReportArrDepLoadPaxTableViewer;
	ReportTimeFrameTableViewer*	pomReportTimeFrameTableViewer;
	ReportOvernightTableViewer*	pomReportOvernightTableViewer;
	ReportFlightDomIntMixTableViewer*	pomReportIntDomMixTableViewer;
	ReportSeatDomIntMixTableViewer*	pomReportSeatIntDomMixTableViewer;
	ReportFlightGPUTableViewer*	pomReportFlightGPUTableViewer;
	ReportBltAllocViewer* pomReportBltAllocViewer;
	ReportDailyFlightLogViewer* pomReportDailyFlightLogViewer;
	ReportFlightDelayViewer* pomReportFlightDelayViewer;

	int imArrDep;
	CStatic* omStatic;

	bool bmDailyRep ;	// FAG BVD Daily Reports 
	Report17DailyTableViewer	*pomReport17Viewer;
	Report19DailyCcaTableViewer	*pomReport19Viewer;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_REPORTTABLEDLG_H__CDB257E5_8BF5_11D1_8127_0000B43C4B01__INCLUDED_
