// searchflightpage.cpp : implementation file
//

#include <stdafx.h>
#include <PSsearchflightpage.h>
#include <BasicData.h>
#include <CCSGlobl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSearchFlightPage property page

//IMPLEMENT_DYNCREATE(CSearchFlightPage, CPropertyPage)

CSearchFlightPage::CSearchFlightPage(bool bpLocalTime) : 
	CPropertyPage(CSearchFlightPage::IDD), bmLocalTime(bpLocalTime)
{
	bmChanged = false;
/*	omFieldList.strgSetValue("TIFA From || TIFD From;Date,TIFA From || TIFD From; Time," 
		                "TIFA To || TIFD To; Date,TIFA To || TIFD To; Time," 
						"SearchHours, SearchDay,DOOA || DOOD," 
		                "RelHBefore,RelHAfter,LSTU; Date,LSTU; Time," 
						"FDAT; Date,FDAT; Time," 
						"ALC2,FLTN,FLNS,REGN,ACT3,ORG3 || ORG4 || DES3 || DES4,"
						"TIFA Flugzeit || TIFD Flugzeit; Date,"
						"TIFA Flugzeit || TIFD Flugzeit; Time,TTYP,STEV");
	InitPage();
*/
}

CSearchFlightPage::~CSearchFlightPage()
{
/*	pomPageBuffer = NULL;
	for (int ilIdx = omData.GetSize()-1; ilIdx >=0; ilIdx--)
	{
		DISPStrukt *polField;
		polField = &omData[ilIdx];
		delete polField;
		omData.RemoveAt(ilIdx);
	} // end for
*/
}

void CSearchFlightPage::SetCalledFrom(CString opCalledFrom)
{
	omCalledFrom = opCalledFrom;
}

void CSearchFlightPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSearchFlightPage)
	DDX_Control(pDX, IDC_CHECK1, m_Check1);
	DDX_Control(pDX, IDC_CHECK2, m_Check2);
	DDX_Control(pDX, IDC_CHECK3, m_Check3);
	DDX_Control(pDX, IDC_CHECK4, m_Check4);
	DDX_Control(pDX, IDC_CHECK5, m_Check5);
	DDX_Control(pDX, IDC_CHECK6, m_Check6);
	DDX_Control(pDX, IDC_CHECK7, m_Check7);
	DDX_Control(pDX, IDC_CHECK8, m_Check8);
	DDX_Control(pDX, IDC_CHECK9, m_Check9);
	DDX_Control(pDX, IDC_CHECK10, m_Check10);
	DDX_Control(pDX, IDC_CHECK11, m_Check11);
	DDX_Control(pDX, IDC_CHECK12, m_Check12);
	DDX_Control(pDX, IDC_KOMPL_ROT, m_Rotation);

	DDX_Control(pDX, IDC_EDIT1, m_Edit1);
	DDX_Control(pDX, IDC_EDIT10, m_Edit10);
	DDX_Control(pDX, IDC_EDIT11, m_Edit11);
	DDX_Control(pDX, IDC_EDIT12, m_Edit12);
	DDX_Control(pDX, IDC_EDIT13, m_Edit13);
	DDX_Control(pDX, IDC_EDIT14, m_Edit14);
	DDX_Control(pDX, IDC_EDIT15, m_Edit15);
	DDX_Control(pDX, IDC_EDIT16, m_Edit16);
	DDX_Control(pDX, IDC_EDIT17, m_Edit17);
	DDX_Control(pDX, IDC_EDIT18, m_Edit18);
	DDX_Control(pDX, IDC_EDIT19, m_Edit19);
	DDX_Control(pDX, IDC_EDIT2, m_Edit2);
	DDX_Control(pDX, IDC_EDIT20, m_Edit20);
	DDX_Control(pDX, IDC_EDIT21, m_Edit21);
	DDX_Control(pDX, IDC_EDIT22, m_Edit22);
	DDX_Control(pDX, IDC_EDIT23, m_Edit23);
	DDX_Control(pDX, IDC_EDIT3, m_Edit3);
	DDX_Control(pDX, IDC_EDIT4, m_Edit4);
	DDX_Control(pDX, IDC_EDIT5, m_Edit5);
	DDX_Control(pDX, IDC_EDIT6, m_Edit6);
	DDX_Control(pDX, IDC_EDIT7, m_Edit7);
	DDX_Control(pDX, IDC_EDIT8, m_Edit8);
	DDX_Control(pDX, IDC_EDIT9, m_Edit9);
	//}}AFX_DATA_MAP

 	if (pDX->m_bSaveAndValidate == FALSE)
	{
		SetData();
	}
	if (pDX->m_bSaveAndValidate == TRUE)
	{
		GetData();
	}
	
	/*    
	m_Edit1.GetWindowText(omData[0].omData);// TIFA From || TIFD From; Date
    m_Edit2.GetWindowText(omData[1].omData);// TIFA From || TIFD From; Time
    m_Edit3.GetWindowText(omData[2].omData);// TIFA To || TIFD To; Date
    m_Edit4.GetWindowText(omData[3].omData);// TIFA To || TIFD To; Time
    m_Edit5.GetWindowText(omData[4].omData);// SearchHours
    m_Edit6.GetWindowText(omData[5].omData);// SearchDay
    m_Edit7.GetWindowText(omData[6].omData);// DOOA || DOOD
    m_Edit8.GetWindowText(omData[7].omData);// RelHBefore
    m_Edit9.GetWindowText(omData[8].omData);// RelHAfter
    m_Edit10.GetWindowText(omData[9].omData);// LSTU; Date
    m_Edit11.GetWindowText(omData[10].omData);// LSTU; Time
    m_Edit12.GetWindowText(omData[11].omData);// FDAT; Date
    m_Edit13.GetWindowText(omData[12].omData);// FDAT; Time
    m_Edit14.GetWindowText(omData[13].omData);// ACL2
    m_Edit15.GetWindowText(omData[14].omData);// FLTN
    m_Edit16.GetWindowText(omData[15].omData);// FLNS
    m_Edit17.GetWindowText(omData[16].omData);// REGN
    m_Edit18.GetWindowText(omData[17].omData);// ACT3
    m_Edit19.GetWindowText(omData[18].omData);// ORG3 || ORG4 || DES3 || DES4
    m_Edit20.GetWindowText(omData[19].omData);// TIFA Flugzeit || TIFD Flugzeit; Date
    m_Edit21.GetWindowText(omData[20].omData);// TIFA Flugzeit || TIFD Flugzeit; Time
    m_Edit22.GetWindowText(omData[21].omData);// TTYP
    m_Edit23.GetWindowText(omData[22].omData);// STEV
*/
}


BEGIN_MESSAGE_MAP(CSearchFlightPage, CPropertyPage)
	//{{AFX_MSG_MAP(CSearchFlightPage)
		// NOTE: the ClassWizard will add message map macros here
	ON_BN_CLICKED(IDC_CHECK1, OnClickedAnkunft)
	ON_BN_CLICKED(IDC_CHECK2, OnClickedAbflug)
	ON_BN_CLICKED(IDC_CHECK3, OnChange)
	ON_BN_CLICKED(IDC_CHECK4, OnChange)
	ON_BN_CLICKED(IDC_CHECK5, OnChange)
	ON_BN_CLICKED(IDC_CHECK6, OnChange)
	ON_BN_CLICKED(IDC_CHECK7, OnChange)
	ON_BN_CLICKED(IDC_CHECK8, OnChange)
	ON_BN_CLICKED(IDC_CHECK9, OnChange)
	ON_BN_CLICKED(IDC_CHECK10, OnChange)
	ON_BN_CLICKED(IDC_CHECK11, OnChange)
	ON_BN_CLICKED(IDC_CHECK12, OnChange)
	ON_BN_CLICKED(IDC_KOMPL_ROT, OnChange)
	ON_MESSAGE(WM_EDIT_KILLFOCUS, OnEditKillfocus)
	ON_MESSAGE(WM_EDIT_CHANGED, OnChange)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CSearchFlightPage::GetData()
{
	omValues.RemoveAll();
	CStringArray olTmpValues;
	GetData(olTmpValues);
	for(int i = 0; i < olTmpValues.GetSize(); i++)
	{
		CString olTmp = olTmpValues[i];
		omValues.Add(olTmpValues[i]);
	}
}

void CSearchFlightPage::GetData(CStringArray &ropValues)
{
	CString olT1, olT2, olT3, olT4;
	CString olResult;
	CTime olDate, olTime;
	m_Edit1.GetWindowText(olT1);// TIFA From || TIFD From; Date
	m_Edit2.GetWindowText(olT2);
	m_Edit3.GetWindowText(olT3);
	m_Edit4.GetWindowText(olT4);

	//Erst mal einen precheck und was leer ist wird 
	//entsprechend gesetzt
	if(olT1.IsEmpty() && olT2.IsEmpty() && olT3.IsEmpty() && olT4.IsEmpty())
	{
		;
	}
	else
	{
		if(olT2.IsEmpty())
		{
			olT2 = CString("00:00");
			m_Edit2.SetWindowText(olT2);
		}
		if(olT4.IsEmpty())
		{
			olT4 = CString("23:59");
			m_Edit4.SetWindowText(olT4);
		}
		if(olT1.IsEmpty() && olT3.IsEmpty())
		{
			olT1 = CTime::GetCurrentTime().Format("%d.%m.%Y");
			olT3 = olT1;
			m_Edit1.SetWindowText(olT1);
			m_Edit3.SetWindowText(olT3);
		}
		if(!olT1.IsEmpty() && olT3.IsEmpty())
		{
			olT3 = olT1;
			m_Edit3.SetWindowText(olT3);
		}
		if(olT1.IsEmpty() && !olT3.IsEmpty())
		{
			olT1 = olT3;
			m_Edit1.SetWindowText(olT1);
		}
	}

	// remove seconds if present
	if (olT2.GetLength() > 5)
	{
		if (olT2.GetLength() > 7)
			olT2 = olT2.Left(olT2.GetLength() - 3);
		else
			olT2 = olT2.Left(olT2.GetLength() - 2);
	}

	// Processing From-Date and From-Time
	if(olT1 != "" && olT2 != "")
	{
		olDate = DateHourStringToDate(olT1, olT2);
		if (olDate != TIMENULL)
		{
			if (bmLocalTime) 
			{
				ogBasicData.LocalToUtc(olDate);
			}
			olT1 = olDate.Format("%Y%m%d%H%M%S");
		}
		else
		{
			olT1 = " ";
		}
	}
	else
	{
 		olT1 = " ";
	}
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");

	
	// Processing To-Date and To-Time
    m_Edit3.GetWindowText(olT1);// TIFA To || TIFD To; Date
	m_Edit4.GetWindowText(olT2);
	// remove seconds if present
	if (olT2.GetLength() > 5)
	{
		if (olT2.GetLength() > 7)
			olT2 = olT2.Left(olT2.GetLength() - 3);
		else
			olT2 = olT2.Left(olT2.GetLength() - 2);
	}
	
	if(olT1 != "" && olT2 != "")
	{
		olDate = DateHourStringToDate(olT1, olT2);
		if (olDate != TIMENULL)
		{
			if (bmLocalTime) 
			{
				ogBasicData.LocalToUtc(olDate);
			}
			olT1 = olDate.Format("%Y%m%d%H%M%S");
		}
		else
		{
			olT1 = " ";
		}
	}
	else
	{
 		olT1 = " ";
	}		
 	ropValues.Add(olT1);
	olResult += olT1 + CString("|");

	m_Edit5.GetWindowText(olT1);// SearchHours
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
    m_Edit6.GetWindowText(olT1);// SearchDay
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
    m_Edit7.GetWindowText(olT1);// DOOA || DOOD
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");



    m_Edit8.GetWindowText(olT1);// RelHBefore
	if (olT1 == "")
	{
		olT1 = " ";
	}
	else// if (!bmLocalTime)
	{
		// Include Local-Utc-Hourdiff in RelHBefore
		olDate = CTime::GetCurrentTime();
		CTime olDateU(olDate);
		ogBasicData.LocalToUtc(olDateU);

		CTimeSpan olSpan = olDate - olDateU;
		olT2.Format("%d", atoi(olT1) + olSpan.GetHours());
		olT1 = olT2;
	}
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");

    m_Edit9.GetWindowText(olT1);// RelHAfter
	if (olT1 == "")
	{
		olT1 = " ";
	}
	else// if (!bmLocalTime)
	{
		// Include Local-Utc-Hourdiff in RelHAfter
		olDate = CTime::GetCurrentTime();
		CTime olDateU(olDate);
		ogBasicData.LocalToUtc(olDateU);

		CTimeSpan olSpan = olDate - olDateU; 
		olT2.Format("%d", atoi(olT1) - olSpan.GetHours());
		olT1 = olT2;
	}
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");


    m_Edit10.GetWindowText(olT1);// LSTU
	m_Edit11.GetWindowText(olT2);
	if(olT1 != "" && olT2 != "")
	{
		olDate = DateHourStringToDate(olT1, olT2);
		if (olDate != TIMENULL)
		{
 			olT1 = olDate.Format("%Y%m%d%H%M%S");
		}
		else
		{
			olT1 = " ";
		}
	}
	else
	{
 		olT1 = " ";
	}		
 	ropValues.Add(olT1);
	olResult += olT1 + CString("|");	

    m_Edit12.GetWindowText(olT1);// FDAT
	m_Edit13.GetWindowText(olT2);
	if(olT1 != "" && olT2 != "")
	{
		olDate = DateHourStringToDate(olT1, olT2);
		if (olDate != TIMENULL)
		{
 			olT1 = olDate.Format("%Y%m%d%H%M%S");
		}
		else
		{
			olT1 = " ";
		}
	}
	else
	{
 		olT1 = " ";
	}		
 	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
	
	
    m_Edit14.GetWindowText(olT1);// ACL2
	olT1.TrimLeft();
	olT1.TrimRight();
	if(olT1 == "")
		olT1 = " ";
	olResult += olT1 + CString("|");
	ropValues.Add(olT1);
    m_Edit15.GetWindowText(olT1);// FLTN
	olT1.TrimLeft();
	olT1.TrimRight();
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
    m_Edit16.GetWindowText(olT1);// FLNS
	olT1.TrimLeft();
	olT1.TrimRight();
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
    m_Edit17.GetWindowText(olT1);// REGN
	olT1.TrimLeft();
	olT1.TrimRight();
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
    m_Edit18.GetWindowText(olT1);// ACT3
	olT1.TrimLeft();
	olT1.TrimRight();
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
    m_Edit19.GetWindowText(olT1);// ORG3 || ORG4 || DES3 || DES4
	olT1.TrimLeft();
	olT1.TrimRight();
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");

    m_Edit20.GetWindowText(olT1);// TIFA || TIFD
	m_Edit21.GetWindowText(olT2);
	if(olT1 != "" && olT2 != "")
	{
		olDate = DateHourStringToDate(olT1, olT2);
		if (olDate != TIMENULL)
		{
			if (bmLocalTime) 
			{
				ogBasicData.LocalToUtc(olDate);
			}
			olT1 = olDate.Format("%Y%m%d%H%M%S");
		}
		else
		{
			olT1 = " ";
		}
	}
	else
	{
 		olT1 = " ";
	}		
 	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
	
	
    m_Edit22.GetWindowText(olT1);// TTYP
	olT1.TrimLeft();
	olT1.TrimRight();
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
    m_Edit23.GetWindowText(olT1);// STEV
	olT1.TrimLeft();
	olT1.TrimRight();
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");

	if(m_Check1.GetCheck() == 1)
	{
		olT1 = "J";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	if(m_Check2.GetCheck() == 1)
	{
		olT1 = "J";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	if(m_Check3.GetCheck() == 1)
	{
		olT1 = "J";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	if(m_Check4.GetCheck() == 1)
	{
		olT1 = "J";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	if(m_Check5.GetCheck() == 1)
	{
		olT1 = "J";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	if(m_Check6.GetCheck() == 1)
	{
		olT1 = "J";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	if(m_Check7.GetCheck() == 1)
	{
		olT1 = "J";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	if(m_Check8.GetCheck() == 1)
	{
		olT1 = "J";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	if(m_Rotation.GetCheck() == 1)
	{
		olT1 = "J";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}

	//YYY
	if (m_Rotation.GetCheck() == 1)
	{
		m_Check9.SetCheck(1);
		m_Check10.SetCheck(1);
	}


	if(omCalledFrom == "POSDIA")
	{
		if (bgShowOnGround == 1)
		{
			m_Rotation.SetCheck(1);
			m_Check9.SetCheck(1);
			m_Check10.SetCheck(1);
		}
	}
	//YYY




	if(m_Check9.GetCheck() == 1)
	{
		olT1 = "J";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}

	if(m_Check10.GetCheck() == 1)
	{
		olT1 = "J";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}


	if(m_Check11.GetCheck() == 1)
	{
		olT1 = "J";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}

	if(m_Check12.GetCheck() == 1)
	{
		olT1 = "J";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}

	//ropValues.Add(olResult);

}

void CSearchFlightPage::SetData()
{
	CStringArray olTmpValues;
//	for(int i = omValues.GetSize()-1; i < 26; i++)
//	{
//		omValues.Add(CString(""));
//	}
	SetData(omValues);


}

void CSearchFlightPage::SetData(CStringArray &ropValues)
{
	
	/*
	m_Edit1.SetBKColor(YELLOW);
	m_Edit2.SetBKColor(YELLOW);
	m_Edit3.SetBKColor(YELLOW);
	m_Edit4.SetBKColor(YELLOW);
	m_Edit8.SetBKColor(YELLOW);
	m_Edit9.SetBKColor(YELLOW);
	*/

	bool blDefault = true;

	m_Edit1.SetWindowText("");// TIFA From || TIFD From; Date
    m_Edit2.SetWindowText("");// TIFA From || TIFD From; Time
    m_Edit3.SetWindowText("");// TIFA To || TIFD To; Date
    m_Edit4.SetWindowText("");// TIFA To || TIFD To; Time
    m_Edit5.SetWindowText("");// SearchHours
    m_Edit6.SetWindowText("");// SearchDay
    m_Edit7.SetWindowText("");// DOOA || DOOD
    m_Edit8.SetWindowText("");// RelHBefore
    m_Edit9.SetWindowText("");// RelHAfter
    m_Edit10.SetWindowText("");// LSTU; Date
    m_Edit11.SetWindowText("");// LSTU; Time
    m_Edit12.SetWindowText("");// FDAT; Date
    m_Edit13.SetWindowText("");// FDAT; Time
    m_Edit14.SetWindowText("");// ACL2
    m_Edit15.SetWindowText("");// FLTN
    m_Edit16.SetWindowText("");// FLNS
    m_Edit17.SetWindowText("");// REGN
    m_Edit18.SetWindowText("");// ACT3
    m_Edit19.SetWindowText("");// ORG3 || ORG4 || DES3 || DES4
    m_Edit20.SetWindowText("");// TIFA Flugzeit || TIFD Flugzeit; Date
    m_Edit21.SetWindowText("");// TIFA Flugzeit || TIFD Flugzeit; Time
    m_Edit22.SetWindowText("");// TTYP
    m_Edit23.SetWindowText("");// STEV

	m_Check1.SetCheck(0);
	m_Check2.SetCheck(0);
	m_Check3.SetCheck(0);
	m_Check4.SetCheck(0);
	m_Check5.SetCheck(0);
	m_Check6.SetCheck(0);
	m_Check7.SetCheck(0);
	m_Check8.SetCheck(0);
	m_Check9.SetCheck(0);
	m_Check10.SetCheck(0);
	m_Check11.SetCheck(0);
	m_Check12.SetCheck(0);
	m_Rotation.SetCheck(0);

	for(int i = 0; i < ropValues.GetSize(); i++)
	{
		CTime olDate = TIMENULL;
		CString olV = ropValues[i];
		switch(i)
		{
		case 0:
			olDate = DBStringToDateTime(ropValues[i]);
			if(olDate != TIMENULL)
			{
				if (bmLocalTime) 
				{
					ogBasicData.UtcToLocal(olDate);
				}
 				m_Edit1.SetWindowText(olDate.Format("%d.%m.%Y"));
				m_Edit2.SetWindowText(olDate.Format("%H:%M"));
			}
			else
			{
				m_Edit1.SetWindowText(CString(""));
				m_Edit2.SetWindowText(CString(""));
			}
			break;
		case 1:
			olDate = DBStringToDateTime(ropValues[i]);
			if(olDate != TIMENULL)
			{
				if (bmLocalTime) 
				{
					ogBasicData.UtcToLocal(olDate);
				}
				m_Edit3.SetWindowText(olDate.Format("%d.%m.%Y"));
				m_Edit4.SetWindowText(olDate.Format("%H:%M"));
			}
			else
			{
				m_Edit3.SetWindowText(CString(""));
				m_Edit4.SetWindowText(CString(""));
			}
			break;
		case 2:
			if(ropValues[i] != CString(" "))
			{
				m_Edit5.SetWindowText(ropValues[i]);
			}
			break;
		case 3:
			if(ropValues[i] != CString(" "))
				m_Edit6.SetWindowText(ropValues[i]);
			break;
		case 4:
			if(ropValues[i] != CString(" "))
				m_Edit7.SetWindowText(ropValues[i]);
			break;
		case 5:
			if(ropValues[i] != CString(" "))
			{
				if (true)//!bmLocalTime)
				{
					// Include Local-Utc-Hourdiff in RelHBefore
					olDate = CTime::GetCurrentTime();
					CTime olDateU(olDate);
					ogBasicData.LocalToUtc(olDateU);

					CTimeSpan olSpan = olDate - olDateU;
					CString olNewVal;
					olNewVal.Format("%d", atoi(ropValues[i]) - olSpan.GetHours());
					m_Edit8.SetWindowText(olNewVal);
					
				}
				else
				{
					m_Edit8.SetWindowText(ropValues[i]);
				}
			}
			break;
		case 6:
			if(ropValues[i] != CString(" "))
			{
				if (true)//!bmLocalTime)
				{
					// Include Local-Utc-Hourdiff in RelHAfter
					olDate = CTime::GetCurrentTime();
					CTime olDateU(olDate);
					ogBasicData.LocalToUtc(olDateU);

					CTimeSpan olSpan = olDate - olDateU;
					CString olNewVal;
					olNewVal.Format("%d", atoi(ropValues[i]) + olSpan.GetHours());
					m_Edit9.SetWindowText(olNewVal);
					
				}
				else
				{				
					m_Edit9.SetWindowText(ropValues[i]);
				}
			}
			break;
		case 7:
			olDate = DBStringToDateTime(ropValues[i]);
			if(olDate != TIMENULL)
			{
 				m_Edit10.SetWindowText(olDate.Format("%d.%m.%Y"));
				m_Edit11.SetWindowText(olDate.Format("%H:%M"));
			}
			else
			{
				m_Edit10.SetWindowText(CString(""));
				m_Edit11.SetWindowText(CString(""));
			}
			break;
		case 8:
			olDate = DBStringToDateTime(ropValues[i]);
			if(olDate != TIMENULL)
			{
 				m_Edit12.SetWindowText(olDate.Format("%d.%m.%Y"));
				m_Edit13.SetWindowText(olDate.Format("%H:%M"));
			}
			else
			{
				m_Edit12.SetWindowText(CString(""));
				m_Edit13.SetWindowText(CString(""));
			}
			break;
		case 9:
			if(ropValues[i] != CString(" "))
				m_Edit14.SetWindowText(ropValues[i]);
			break;
		case 10:
			if(ropValues[i] != CString(" "))
				m_Edit15.SetWindowText(ropValues[i]);
			break;
		case 11:
			if(ropValues[i] != CString(" "))
				m_Edit16.SetWindowText(ropValues[i]);
			break;
		case 12:
			if(ropValues[i] != CString(" "))
				m_Edit17.SetWindowText(ropValues[i]);
			break;
		case 13:
			if(ropValues[i] != CString(" "))
				m_Edit18.SetWindowText(ropValues[i]);
			break;
		case 14:
			if(ropValues[i] != CString(" "))
				m_Edit19.SetWindowText(ropValues[i]);
			break;
		case 15:
			olDate = DBStringToDateTime(ropValues[i]);
			if(olDate != TIMENULL)
			{
				if (bmLocalTime) 
				{
					ogBasicData.UtcToLocal(olDate);
				}
				m_Edit20.SetWindowText(olDate.Format("%d.%m.%Y"));
				m_Edit21.SetWindowText(olDate.Format("%H:%M"));
			}
			else
			{
				m_Edit20.SetWindowText(CString(""));
				m_Edit21.SetWindowText(CString(""));
			}
			break;
		case 16:
			if(ropValues[i] != CString(" "))
				m_Edit22.SetWindowText(ropValues[i]);
			break;
		case 17:
			if(ropValues[i] != CString(" "))
				m_Edit23.SetWindowText(ropValues[i]);
			break;
		case 18: //Ab hier CheckBoxen
			if(ropValues[i] == "J")
			{
				m_Check1.SetCheck(1);
				blDefault = false;
			} 
			else
			{
				m_Check1.SetCheck(0);
			}
			break;
		case 19:
			if(ropValues[i] == "J")
			{
				m_Check2.SetCheck(1);
				blDefault = false;
			}
			else
			{
				m_Check2.SetCheck(0);
			}
			break;
		case 20:
			if(ropValues[i] == "J")
			{
				m_Check3.SetCheck(1);
				blDefault = false;
			}
			else
			{
				m_Check3.SetCheck(0);
			}
			break;
		case 21:
			if(ropValues[i] == "J")
			{
				m_Check4.SetCheck(1);
				blDefault = false;
			}
			else
			{
				m_Check4.SetCheck(0);
			}
			break;
		case 22:
			if(ropValues[i] == "J")
			{
				m_Check5.SetCheck(1);
				blDefault = false;
			}
			else
			{
				m_Check5.SetCheck(0);
			}
			break;
		case 23:
			if(ropValues[i] == "J")
			{
				m_Check6.SetCheck(1);
				blDefault = false;
			}
			else
			{
				m_Check6.SetCheck(0);
			}
			break;
		case 24:
			if(ropValues[i] == "J")
			{
				m_Check7.SetCheck(1);
				blDefault = false;
			}
			else
			{
				m_Check7.SetCheck(0);
			}
			break;
		case 25:
			if(ropValues[i] == "J")
			{
				m_Check8.SetCheck(1);
				blDefault = false;
			}
			else
			{
				m_Check8.SetCheck(0);
			}
			break;
		case 26:
			if(omCalledFrom == "POSDIA")
			{
				if (bgShowOnGround == 1)
				{
					ropValues[26] = "J";
				}
			}

			if(ropValues[i] == "J")
			{
				m_Rotation.SetCheck(1);
			}
			else
			{
				m_Rotation.SetCheck(0);
			}
			break;
		case 27:
			if (m_Rotation.GetCheck() == 1)
			{
				ropValues[27] = "J";
				ropValues[28] = "J";
			}


			if(omCalledFrom == "POSDIA")
			{
				if (bgShowOnGround == 1)
				{
					ropValues[27] = "J";
					ropValues[28] = "J";
				}
			}

			if(ropValues[i] == "J")
			{
				m_Check9.SetCheck(1);
			}
			else
			{
				m_Check9.SetCheck(0);
			}
			break;
		case 28:
			if(ropValues[i] == "J")
			{
				m_Check10.SetCheck(1);
			}
			else
			{
				m_Check10.SetCheck(0);
			}
			break;
		case 29:
			if(ropValues[i] == "J")
			{
				m_Check11.SetCheck(1);
			}
			else
			{
				m_Check11.SetCheck(0);
			}
			break;
		case 30:
			if(ropValues[i] == "J")
			{
				m_Check12.SetCheck(1);
			}
			else
			{
				m_Check12.SetCheck(0);
			}
			break;
		}
	}

	if(m_Check1.GetCheck() == 0 && m_Check2.GetCheck() == 0)
	{
		m_Check1.SetCheck(1);
		m_Check2.SetCheck(1);
	}

	if(blDefault)
	{
		if(omCalledFrom == "SEASON")
		{
			m_Check4.SetCheck(1);
			m_Check3.SetCheck(1);
			m_Check6.SetCheck(1);
			m_Rotation.SetCheck(1);
		}
		if(omCalledFrom == "ROTATION")
		{
			m_Check3.SetCheck(1);
			m_Check6.SetCheck(1);
			m_Rotation.SetCheck(1);
		}
		if(omCalledFrom == "DAILY")
		{
			m_Check3.SetCheck(1);
			m_Check6.SetCheck(1);
			m_Rotation.SetCheck(1);
		}
		if(omCalledFrom == "POSDIA" || omCalledFrom == "FLIGHTDIA")
		{
			m_Check4.SetCheck(1);
			m_Check3.SetCheck(1);
			m_Rotation.SetCheck(1);
		}
		if(omCalledFrom == "CCADIA")
		{
			m_Check1.SetCheck(0);
			m_Check4.SetCheck(1);
			m_Check3.SetCheck(1);
		}

		if (m_Rotation.GetCheck() == 1)
		{
			m_Check9.SetCheck(1);
			m_Check10.SetCheck(1);
		}

	}

/*
	if(blDefault)
	{
		if(omCalledFrom == "SEASON")
		{
			m_Check4.SetCheck(1);
			m_Check3.SetCheck(1);
			m_Check6.SetCheck(1);
			m_Check7.SetCheck(1);
		}
		if(omCalledFrom == "ROTATION")
		{
			m_Check3.SetCheck(1);
			m_Check6.SetCheck(1);
			m_Check7.SetCheck(1);
		}
	}
*/
}

/////////////////////////////////////////////////////////////////////////////
// CSearchFlightPage message handlers
void CSearchFlightPage::InitPage()
{

	
} // end InitPage

 
//---------------------------------------------------------------------------------
CString CSearchFlightPage::GetPrivList()
{
	//char aclInitFieldList[] = "APPL,SUBD,FUNC,FUAL,TYPE,STAT";
	CString olInitPageData; 
	/*ogAppName +
 		CString("SearchFlightPage,TIFA From || TIFD From;Date,TimeFrameDateFrom,E,1,") +
		CString("SearchFlightPage,TIFA From || TIFD From;Time,TimeFrameTimeFrom,E,1,") +
		CString("SearchFlightPage,TIFA To || TIFD To;Date,TimeFrameDateTo,E,1,") +
		CString("SearchFlightPage,TIFA To || TIFD To;Time,TimeFrameTimeTo,E,1,") +
		CString("SearchFlightPage,SearchHours,SearchHours,E,1,") +
		CString("SearchFlightPage,SearchDay,SearchDay,E,1,") +
		CString("SearchFlightPage,DOOA || DOOD,DayOfOperation,E,1,") +
		CString("SearchFlightPage,RelHBefore,RelHBefore,E,1,") +
		CString("SearchFlightPage,RelHAfter,RelHAfter,E,1,") +
		CString("SearchFlightPage,LSTU;Date,LSTUDate,E,1,") +
		CString("SearchFlightPage,LSTU;Time,LSTUTime,E,1,") +
		CString("SearchFlightPage,FDAT;Date,FDATDate,E,1,") +
		CString("SearchFlightPage,FDAT;Time,FDATTime,E,1,") +
		CString("SearchFlightPage,ALC2,ALC2,E,1,") +
		CString("SearchFlightPage,FLTN,FLTN,E,1,") +
		CString("SearchFlightPage,FLNS,FLNS,E,1,") +
		CString("SearchFlightPage,REGN,REGN,E,1,") +
		CString("SearchFlightPage,ACT3,ACT3,E,1,") +
		CString("SearchFlightPage,ORG3 || ORG4 || DES3 || DES4,OrgDest,E,1,") +
		CString("SearchFlightPage,TIFA Flugzeit || TIFD Flugzeit;Date,FlugZeitDate,E,1,") +
		CString("SearchFlightPage,TIFA Flugzeit || TIFD Flugzeit;Time,FlugZeitTime,E,1,") +
		CString("SearchFlightPage,TTYP,TTYP,E,1,") +
		CString("SearchFlightPage,STEV,STEV,B,1,") +
		CString("SearchFlightPage,Ankunft,AnkunftButton,B,1,")+
		CString("SearchFlightPage,Abflug,AbflugButton,B,1,")+
		CString("SearchFlightPage,Betrieb,BetriebButton,B,1,")+
		CString("SearchFlightPage,Planung,PlanungButton,B,1,")+
		CString("SearchFlightPage,Prognose,PrognoseButton,B,1,")+
		CString("SearchFlightPage,Cancelled,CancelledButton,B,1,")+
		CString("SearchFlightPage,Noop,NoopButton,B,1,")+
		CString("SearchFlightPage,gelöscht,gelöschtButton,B,1,");
		*/
	return(olInitPageData);
}

void CSearchFlightPage::OnClickedAbflug()
{
	bmChanged = true;
	if(m_Check2.GetCheck() == 0)
	{
		if(m_Check1.GetCheck() == 0)
		{
			m_Check2.SetCheck(1);
		}
	}
}

void CSearchFlightPage::OnClickedAnkunft()
{
	bmChanged = true;
	if(m_Check1.GetCheck() == 0)
	{
		if(m_Check2.GetCheck() == 0)
		{
			m_Check1.SetCheck(1);
		}
	}
}


void CSearchFlightPage::OnChange()
{
	bmChanged = true;

	if (m_Rotation.GetCheck() == 1)
	{
		m_Check9.SetCheck(1);
		m_Check10.SetCheck(1);
	}


	if(omCalledFrom == "POSDIA")
	{
		if (bgShowOnGround == 1)
		{
			m_Rotation.SetCheck(1);
			m_Check9.SetCheck(1);
			m_Check10.SetCheck(1);
		}
	}


	if(	m_Check3.GetCheck() == 0 &&
		m_Check4.GetCheck() == 0 &&
		m_Check5.GetCheck() == 0 &&
		m_Check6.GetCheck() == 0 &&
		m_Check7.GetCheck() == 0 &&
		m_Check9.GetCheck() == 0 &&
		m_Check10.GetCheck() == 0 &&
		m_Check11.GetCheck() == 0 &&
		m_Check12.GetCheck() == 0
	)
		m_Check3.SetCheck(1);
}


LONG CSearchFlightPage::OnEditKillfocus( UINT wParam, LPARAM lParam)
{
 	/*

	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY*)lParam;

	if(((UINT)m_Edit1.imID == wParam) || ((UINT)m_Edit2.imID == wParam)  || ((UINT)m_Edit3.imID == wParam) || ((UINT)m_Edit4.imID == wParam))
	{
		m_Edit1.SetBKColor(YELLOW);
		m_Edit2.SetBKColor(YELLOW);
		m_Edit3.SetBKColor(YELLOW);
		m_Edit4.SetBKColor(YELLOW);
		m_Edit8.SetBKColor(SILVER);
		m_Edit9.SetBKColor(SILVER);
		m_Edit8.SetWindowText("");// RelHBefore
		m_Edit9.SetWindowText("");// RelHAfter
	
	
	}

	if(((UINT)m_Edit8.imID == wParam) || ((UINT)m_Edit9.imID == wParam))
	{
		m_Edit1.SetBKColor(SILVER);
		m_Edit2.SetBKColor(SILVER);
		m_Edit3.SetBKColor(SILVER);
		m_Edit4.SetBKColor(SILVER);
		m_Edit8.SetBKColor(YELLOW);
		m_Edit9.SetBKColor(YELLOW);
		m_Edit1.SetWindowText("");// TIFA From || TIFD From; Date
		m_Edit2.SetWindowText("");// TIFA From || TIFD From; Time
		m_Edit3.SetWindowText("");// TIFA To || TIFD To; Date
		m_Edit4.SetWindowText("");// TIFA To || TIFD To; Time
	}
	
	*/

	return 0L;
}