#if !defined(AFX_SEARCHFLIGHTPAGE_H__E58F1C91_50FA_11D1_9884_000001014864__INCLUDED_)
#define AFX_SEARCHFLIGHTPAGE_H__E58F1C91_50FA_11D1_9884_000001014864__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// searchflightpage.h : header file
//

#include <Ansicht.h>
#include <CCSPtrArray.h>
#include <CCSEdit.h>
/////////////////////////////////////////////////////////////////////////////
// CSearchFlightPage dialog

class CSearchFlightPage : public CPropertyPage
{
	//DECLARE_DYNCREATE(CSearchFlightPage)

// Construction
public:
	CSearchFlightPage(bool bpLocalTime);
	~CSearchFlightPage();

	void SetCalledFrom(CString opCalledFrom);
	CString omCalledFrom;
	bool bmChanged;
// Dialog Data
	//{{AFX_DATA(CSearchFlightPage)
	enum { IDD = IDD_PSSEARCH_FLIGHT_PAGE };
	CButton	m_Check1;
	CButton	m_Check2;
	CButton	m_Check3;
	CButton	m_Check4;
	CButton	m_Check5;
	CButton	m_Check6;
	CButton	m_Check7;
	CButton	m_Check8;
	CButton	m_Check9;
	CButton	m_Check10;
	CButton	m_Check11;
	CButton	m_Check12;
	CButton	m_Rotation;
	CCSEdit	m_Edit1;   // TIFA From || TIFD From; Date
	CCSEdit	m_Edit2;   // TIFA From || TIFD From; Time
	CCSEdit	m_Edit3;   // TIFA To || TIFD To; Date
	CCSEdit	m_Edit4;   // TIFA To || TIFD To; Time
	CCSEdit	m_Edit5;   // SearchHours
	CCSEdit	m_Edit6;   // SearchDay
	CCSEdit	m_Edit7;   // DOOA DOOD
	CCSEdit	m_Edit8;   // RelHBefore
	CCSEdit	m_Edit9;   // RelHAfter
	CCSEdit	m_Edit10;  // LSTU; Date
	CCSEdit	m_Edit11;  // LSTU; Time
	CCSEdit	m_Edit12;  // FDAT; Date
	CCSEdit	m_Edit13;  // FDAT; Time
	CCSEdit	m_Edit14;  // ACL2
	CCSEdit	m_Edit15;  // FLTN
	CCSEdit	m_Edit16;  // FLNS
	CCSEdit	m_Edit17;  // REGN
	CCSEdit	m_Edit18;  // ACT3
	CCSEdit	m_Edit19;  // ORG3 || ORG4 || DES3 || DES4
	CCSEdit	m_Edit20;  // TIFA Flugzeit || TIFD Flugzeit; Date
	CCSEdit	m_Edit21;  // TIFA Flugzeit || TIFD Flugzeit; Time
	CCSEdit	m_Edit22;  // TTYP
	CCSEdit	m_Edit23;  // STEV
	//}}AFX_DATA

	//CCSPtrArray<CCSEdit> omEdits;
	//CCSPtrArray<CButton> omButtons;
	CStringArray omValues;

	void GetData();
	void GetData(CStringArray &ropValues);
	void SetData();
	void SetData(CStringArray &ropValues);
//	CCSItem omFieldList; //("TIFA From || TIFD From;Date,TIFA From || TIFD From; Time," 
		                 //"TIFA To || TIFD To; Date,TIFA To || TIFD To; Time," 
						 //"SearchHours, SearchDay,DOOA || DOOD," 
		                 //"RelHBefore,RelHAfter,LSTU; Date,LSTU; Time," 
						 //"FDAT; Date,FDAT; Time," 
						 //"ALC2,FLTN,FLNS,REGN,ACT3,ORG3 || ORG4 || DES3 || DES4,"
						 //"TIFA Flugzeit || TIFD Flugzeit; Date,"
						 //"TIFA Flugzeit || TIFD Flugzeit; Time,TTYP,STEV");
/*   struct DISPStrukt
   {
	   CString omFieldName;
	   CString omOperator;
	   CString omData;
	   CString omFormatIn;
	   CString omFormatOut;
	   DISPStrukt()
	   {
		   omFieldName = "";
		   omOperator="";
		   omData="";
		   omFormatIn="";
		   omFormatOut="";
		};
   };
   CCSPtrArray<DISPStrukt> omData;
*/
   
// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CSearchFlightPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

//   void ResetBufferPtr(){CBasePropSheetPage::ResetBufferPtr(); pomPageBuffer = NULL;};
//   void *GetData();
//   void InitialScreen();
   CString GetPrivList();
protected:
	bool bmLocalTime;	
//	CPageBuffer *pomPageBuffer;
//	int CreateFilter();
	void InitPage();
	// Generated message map functions
	//{{AFX_MSG(CSearchFlightPage)
		afx_msg void OnClickedAbflug();
		afx_msg void OnClickedAnkunft();
		afx_msg void OnChange();
		afx_msg LONG OnEditKillfocus( UINT wParam, LPARAM lParam);
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SEARCHFLIGHTPAGE_H__E58F1C91_50FA_11D1_9884_000001014864__INCLUDED_)
