// RotationAltDlg .cpp : implementation file
//

#include <stdafx.h>
#include <fpms.h>
#include <RotationAltDlg.h>

#include <CCSGlobl.h>
#include <CedaBasicData.h>
#include <AwBasicDataDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Local function prototype
static void AltCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);


/////////////////////////////////////////////////////////////////////////////
// RotationAltDlg  dialog


RotationAltDlg ::RotationAltDlg (CWnd* pParent /*=NULL*/, CString opAlc)
	: CDialog(RotationAltDlg ::IDD, pParent)
{
	//{{AFX_DATA_INIT(RotationAltDlg )
	m_Alc2 = _T("");
	m_Alc3 = _T("");
	m_Alfn = _T("");
	//}}AFX_DATA_INIT

	if(opAlc.GetLength() == 3)
		m_Alc3 = opAlc;
	else
		m_Alc2 = opAlc;

	ogDdx.Register(this, APP_EXIT, CString("ALTDLG"), CString("Appli. exit"), AltCf);

}


RotationAltDlg::~RotationAltDlg()
{
	ogDdx.UnRegister(this,NOTUSED);
}


static void AltCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    RotationAltDlg *polDlg = (RotationAltDlg *)popInstance;

    if (ipDDXType == APP_EXIT)
        polDlg->AppExit();

}


void RotationAltDlg::AppExit()
{
	EndDialog(IDCANCEL);
}



BOOL RotationAltDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_CE_Alc2.SetTypeToString("X|#X|#",2,0);
	m_CE_Alc3.SetTypeToString("X|#X|#X|#",3,0);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}



void RotationAltDlg ::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(RotationAltDlg )
	DDX_Control(pDX, IDC_ALC3, m_CE_Alc3);
	DDX_Control(pDX, IDC_ALC2, m_CE_Alc2);
	DDX_Control(pDX, IDC_ALFN, m_CE_Alfn);
	DDX_Text(pDX, IDC_ALC2, m_Alc2);
	DDX_Text(pDX, IDC_ALC3, m_Alc3);
	DDX_Text(pDX, IDC_ALFN, m_Alfn);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(RotationAltDlg , CDialog)
	//{{AFX_MSG_MAP(RotationAltDlg )
	ON_MESSAGE(WM_EDIT_KILLFOCUS, OnEditKillfocus)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// RotationAltDlg  message handlers

void RotationAltDlg ::OnOK() 
{

	//killfocus - check for the curent field
	NextDlgCtrl(); 
	PrevDlgCtrl();
	
	m_CE_Alc3.GetWindowText(m_Alc3);
	m_CE_Alc2.GetWindowText(m_Alc2);
	m_CE_Alfn.GetWindowText(m_Alfn);


	if(!m_CE_Alc3.GetStatus())
		m_Alc3 = "";

	if(!m_CE_Alc2.GetStatus())
		m_Alc2 = "";


	if(m_Alc3.IsEmpty() && m_Alc2.IsEmpty())
		return;

	RecordSet *prlRecord = new RecordSet(ogBCD.GetFieldCount("ALT"));
	prlRecord->Values[ogBCD.GetFieldIndex("ALT", "ALFN")] = m_Alfn;
	prlRecord->Values[ogBCD.GetFieldIndex("ALT", "ALC3")] = m_Alc3;
	prlRecord->Values[ogBCD.GetFieldIndex("ALT", "ALC2")] = m_Alc2;
	ogBCD.InsertRecord("ALT", (*prlRecord), true);
	delete prlRecord;

	CDialog::OnOK();
}




LONG RotationAltDlg::OnEditKillfocus( UINT wParam, LPARAM lParam)
{
	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY*)lParam;
	if(prlNotify->Text.IsEmpty())
		return 0L;

	if((UINT)m_CE_Alc2.imID == wParam)
	{
		CString olTmp;
		prlNotify->UserStatus = true;
		prlNotify->Status = !ogBCD.GetField("ALT", "ALC2", prlNotify->Text, "ALC3", olTmp );
		if(prlNotify->Status == false)
			MessageBox(GetString(IDS_STRING938), GetString(ST_FEHLER));

		return 0L;
	}

	if((UINT)m_CE_Alc3.imID == wParam)
	{
		CString olTmp;
		prlNotify->UserStatus = true;
		prlNotify->Status = !ogBCD.GetField("ALT", "ALC3", prlNotify->Text, "ALC2", olTmp );
		if(prlNotify->Status == false)
			MessageBox(GetString(IDS_STRING939), GetString(ST_FEHLER));
		return 0L;
	}


	return 0L;

}
