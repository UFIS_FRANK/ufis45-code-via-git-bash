// ReportFieldDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <fpms.h>
#include <ReportFieldDlg.h>
#include <CcsGlobl.h>
#include <resrc1.h>
#include <cedabasicdata.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CReportFieldDlg 


CReportFieldDlg::CReportFieldDlg(CWnd* pParent, char *opSelect)
	: CDialog(CReportFieldDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CReportFieldDlg)
	m_Select = _T("");
	//}}AFX_DATA_INIT
	pcmSelect = opSelect;
}


void CReportFieldDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CReportFieldDlg)
	DDX_Control(pDX, IDC_SEL, m_CE_Select);
	DDX_Control(pDX, IDCANCEL, m_Cancel);
	DDX_Control(pDX, IDOK, m_Ok);
	DDX_Text(pDX, IDC_SEL, m_Select);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CReportFieldDlg, CDialog)
	//{{AFX_MSG_MAP(CReportFieldDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CReportFieldDlg 

BOOL CReportFieldDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_CE_Select.SetBKColor(YELLOW);
	m_Select = pcmSelect ;
	m_CE_Select.SetWindowText(m_Select);
	
	// TODO: Zus�tzliche Initialisierung hier einf�gen
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CReportFieldDlg::OnOK() 
{
	// TODO: Zus�tzliche Pr�fung hier einf�gen
	m_CE_Select.GetWindowText(m_Select);
	strcpy(pcmSelect, m_Select);
	
	if(!strlen(pcmSelect)  )
	{
		MessageBox(GetString(IDS_STRING918), GetString(IMFK_REPORT), MB_OK);
		return;
	}
	else
	{
		CString olMinStr;
		CString olMaxStr;
		bool blRet = ogBCD.GetField("SEA", "SEAS", pcmSelect, "VPFR", olMinStr);
		if(blRet == TRUE)
			blRet = ogBCD.GetField("SEA", "SEAS", pcmSelect, "VPTO", olMaxStr);
		if (blRet == FALSE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
//					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING928), MB_OK);
			MessageBox(GetString(IDS_STRING_NOSEAS), GetString(IDS_WARNING), MB_ICONWARNING );
			return;
		}
		else
			CDialog::OnOK();
	}
}

void CReportFieldDlg::OnCancel() 
{
	// TODO: Zus�tzlichen Bereinigungscode hier einf�gen
	
	CDialog::OnCancel();
}
