# Microsoft Developer Studio Generated NMAKE File, Based on Fpms.dsp
!IF "$(CFG)" == ""
CFG=FPMS - Win32 Debug
!MESSAGE No configuration specified. Defaulting to FPMS - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "FPMS - Win32 Release" && "$(CFG)" != "FPMS - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Fpms.mak" CFG="FPMS - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "FPMS - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "FPMS - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "FPMS - Win32 Release"

OUTDIR=C:\Ufis_Bin\Release
INTDIR=C:\Ufis_Intermediate\Release
# Begin Custom Macros
OutDir=C:\Ufis_Bin\Release
# End Custom Macros

ALL : "$(OUTDIR)\Fpms.exe"


CLEAN :
	-@erase "$(INTDIR)\AcirrDlg.obj"
	-@erase "$(INTDIR)\AdditionalReport10TableViewer.obj"
	-@erase "$(INTDIR)\AdditionalReport11TableViewer.obj"
	-@erase "$(INTDIR)\AdditionalReport12TableViewer.obj"
	-@erase "$(INTDIR)\AdditionalReport16TableViewer.obj"
	-@erase "$(INTDIR)\AdditionalReport17TableViewer.obj"
	-@erase "$(INTDIR)\AdditionalReport1TableViewer.obj"
	-@erase "$(INTDIR)\AdditionalReport2TableViewer.obj"
	-@erase "$(INTDIR)\AdditionalReport3TableViewer.obj"
	-@erase "$(INTDIR)\AdditionalReport4TableViewer.obj"
	-@erase "$(INTDIR)\AdditionalReport5TableViewer.obj"
	-@erase "$(INTDIR)\AdditionalReport6TableViewer.obj"
	-@erase "$(INTDIR)\AdditionalReport7TableViewer.obj"
	-@erase "$(INTDIR)\AdditionalReport8TableViewer.obj"
	-@erase "$(INTDIR)\AdditionalReport9TableViewer.obj"
	-@erase "$(INTDIR)\AdditionalReportTableDlg.obj"
	-@erase "$(INTDIR)\AirportInformationDlg.obj"
	-@erase "$(INTDIR)\AllocateCca.obj"
	-@erase "$(INTDIR)\AllocateCcaParameter.obj"
	-@erase "$(INTDIR)\AltImportDelDlg.obj"
	-@erase "$(INTDIR)\AskBox.obj"
	-@erase "$(INTDIR)\AwBasicDataDlg.obj"
	-@erase "$(INTDIR)\BackGround.obj"
	-@erase "$(INTDIR)\BasePropertySheet.obj"
	-@erase "$(INTDIR)\BasicData.obj"
	-@erase "$(INTDIR)\BltCcaTableDlg.obj"
	-@erase "$(INTDIR)\BltCcaTableViewer.obj"
	-@erase "$(INTDIR)\BltChart.obj"
	-@erase "$(INTDIR)\BltDiagram.obj"
	-@erase "$(INTDIR)\BltDiaPropertySheet.obj"
	-@erase "$(INTDIR)\BltDiaViewer.obj"
	-@erase "$(INTDIR)\BltGantt.obj"
	-@erase "$(INTDIR)\BltKonflikteDlg.obj"
	-@erase "$(INTDIR)\BltOverviewTableDlg.obj"
	-@erase "$(INTDIR)\BltOverviewTableViewer.obj"
	-@erase "$(INTDIR)\buttonlistdlg.obj"
	-@erase "$(INTDIR)\CcaCedaFlightData.obj"
	-@erase "$(INTDIR)\CCAChangeTimes.obj"
	-@erase "$(INTDIR)\CcaChart.obj"
	-@erase "$(INTDIR)\CcaCommonDlg.obj"
	-@erase "$(INTDIR)\CcaCommonSearchDlg.obj"
	-@erase "$(INTDIR)\CcaCommonTableDlg.obj"
	-@erase "$(INTDIR)\CcaCommonTableViewer.obj"
	-@erase "$(INTDIR)\CcaDiagram.obj"
	-@erase "$(INTDIR)\CcaDiaPropertySheet.obj"
	-@erase "$(INTDIR)\CcaDiaViewer.obj"
	-@erase "$(INTDIR)\CCAExpandDlg.obj"
	-@erase "$(INTDIR)\CcaGantt.obj"
	-@erase "$(INTDIR)\CCSGlobl.obj"
	-@erase "$(INTDIR)\CCSPrint.obj"
	-@erase "$(INTDIR)\CedaApxData.obj"
	-@erase "$(INTDIR)\CedaCcaData.obj"
	-@erase "$(INTDIR)\CedaCfgData.obj"
	-@erase "$(INTDIR)\CedaCflData.obj"
	-@erase "$(INTDIR)\CedaCountData.obj"
	-@erase "$(INTDIR)\CedaDiaCcaData.obj"
	-@erase "$(INTDIR)\CedaGpaData.obj"
	-@erase "$(INTDIR)\CedaGrmData.obj"
	-@erase "$(INTDIR)\CedaHaiData.obj"
	-@erase "$(INTDIR)\CedaImpFlightData.obj"
	-@erase "$(INTDIR)\CedaInfData.obj"
	-@erase "$(INTDIR)\CedaInitModuData.obj"
	-@erase "$(INTDIR)\CedaResGroupData.obj"
	-@erase "$(INTDIR)\CicConfTableDlg.obj"
	-@erase "$(INTDIR)\CicConfTableViewer.obj"
	-@erase "$(INTDIR)\CicDemandTableDlg.obj"
	-@erase "$(INTDIR)\CicDemandTableViewer.obj"
	-@erase "$(INTDIR)\CicNoDemandTableDlg.obj"
	-@erase "$(INTDIR)\CicNoDemandTableViewer.obj"
	-@erase "$(INTDIR)\CollectHAIDlg.obj"
	-@erase "$(INTDIR)\ConnectionAdvisor.obj"
	-@erase "$(INTDIR)\CVIEWER.OBJ"
	-@erase "$(INTDIR)\DailyCcaPrintDlg.obj"
	-@erase "$(INTDIR)\DailyCedaFlightData.obj"
	-@erase "$(INTDIR)\DailyPrintDlg.obj"
	-@erase "$(INTDIR)\DailyScheduleTableDlg.obj"
	-@erase "$(INTDIR)\DailyTableViewer.obj"
	-@erase "$(INTDIR)\DailyVipCedaData.obj"
	-@erase "$(INTDIR)\DataSet.obj"
	-@erase "$(INTDIR)\DelelteCounterDlg.obj"
	-@erase "$(INTDIR)\DGanttBarReport.obj"
	-@erase "$(INTDIR)\DGanttChartReport.obj"
	-@erase "$(INTDIR)\DGanttChartReportWnd.obj"
	-@erase "$(INTDIR)\DiaCedaFlightData.obj"
	-@erase "$(INTDIR)\FlightChart.obj"
	-@erase "$(INTDIR)\FlightDiagram.obj"
	-@erase "$(INTDIR)\FlightDiaPropertySheet.obj"
	-@erase "$(INTDIR)\FlightDiaViewer.obj"
	-@erase "$(INTDIR)\FlightGantt.obj"
	-@erase "$(INTDIR)\FlightSearchPropertySheet.obj"
	-@erase "$(INTDIR)\FlightSearchTableDlg.obj"
	-@erase "$(INTDIR)\FlightSearchTableViewer.obj"
	-@erase "$(INTDIR)\FPMS.obj"
	-@erase "$(INTDIR)\FPMS.res"
	-@erase "$(INTDIR)\GanttBarReport.obj"
	-@erase "$(INTDIR)\GanttChartReport.obj"
	-@erase "$(INTDIR)\GanttChartReportWnd.obj"
	-@erase "$(INTDIR)\GatChart.obj"
	-@erase "$(INTDIR)\GatDiagram.obj"
	-@erase "$(INTDIR)\GatDiaPropertySheet.obj"
	-@erase "$(INTDIR)\GatDiaViewer.obj"
	-@erase "$(INTDIR)\GatGantt.obj"
	-@erase "$(INTDIR)\HAIDlg.obj"
	-@erase "$(INTDIR)\InitialLoadDlg.obj"
	-@erase "$(INTDIR)\Konflikte.obj"
	-@erase "$(INTDIR)\KonflikteDlg.obj"
	-@erase "$(INTDIR)\List.obj"
	-@erase "$(INTDIR)\LoginDlg.obj"
	-@erase "$(INTDIR)\PosChart.obj"
	-@erase "$(INTDIR)\PosDiagram.obj"
	-@erase "$(INTDIR)\PosDiaPropertySheet.obj"
	-@erase "$(INTDIR)\PosDiaViewer.obj"
	-@erase "$(INTDIR)\PosGantt.obj"
	-@erase "$(INTDIR)\PrintInfo.obj"
	-@erase "$(INTDIR)\PrivList.obj"
	-@erase "$(INTDIR)\ProgressDlg.obj"
	-@erase "$(INTDIR)\ProxyDlg.obj"
	-@erase "$(INTDIR)\PSGatGeometry.obj"
	-@erase "$(INTDIR)\PSGeometrie.obj"
	-@erase "$(INTDIR)\PSSearchFlightPage.obj"
	-@erase "$(INTDIR)\PSSortFlightPage.obj"
	-@erase "$(INTDIR)\PSSpecFilterPage.obj"
	-@erase "$(INTDIR)\PSUniFilterPage.obj"
	-@erase "$(INTDIR)\PSUniSortPage.obj"
	-@erase "$(INTDIR)\RegisterDlg.obj"
	-@erase "$(INTDIR)\Report10TableViewer.obj"
	-@erase "$(INTDIR)\Report11TableViewer.obj"
	-@erase "$(INTDIR)\Report12TableViewer.obj"
	-@erase "$(INTDIR)\Report13TableViewer.obj"
	-@erase "$(INTDIR)\Report14TableViewer.obj"
	-@erase "$(INTDIR)\Report15TableViewer.obj"
	-@erase "$(INTDIR)\Report16TableViewer.obj"
	-@erase "$(INTDIR)\Report17DailyTableViewer.obj"
	-@erase "$(INTDIR)\Report19DailyCcaTableViewer.obj"
	-@erase "$(INTDIR)\Report1TableViewer.obj"
	-@erase "$(INTDIR)\Report2TableViewer.obj"
	-@erase "$(INTDIR)\Report3TableViewer.obj"
	-@erase "$(INTDIR)\Report4TableViewer.obj"
	-@erase "$(INTDIR)\Report5TableViewer.obj"
	-@erase "$(INTDIR)\Report6NatCedaData.obj"
	-@erase "$(INTDIR)\Report6TableViewer.obj"
	-@erase "$(INTDIR)\Report7TableViewer.obj"
	-@erase "$(INTDIR)\Report8TableViewer.obj"
	-@erase "$(INTDIR)\Report9TableViewer.obj"
	-@erase "$(INTDIR)\ReportActCedaData.obj"
	-@erase "$(INTDIR)\ReportArrDepLoadPaxTableViewer.obj"
	-@erase "$(INTDIR)\ReportArrDepSelctDlg.obj"
	-@erase "$(INTDIR)\ReportArrDepTableViewer.obj"
	-@erase "$(INTDIR)\ReportBltAllocViewer.obj"
	-@erase "$(INTDIR)\ReportDailyFlightLogViewer.obj"
	-@erase "$(INTDIR)\ReportDomIntTranTableViewer.obj"
	-@erase "$(INTDIR)\ReportFieldDlg.obj"
	-@erase "$(INTDIR)\ReportFlightDelayViewer.obj"
	-@erase "$(INTDIR)\ReportFlightDomIntMixTableViewer.obj"
	-@erase "$(INTDIR)\ReportFlightGPUTableViewer.obj"
	-@erase "$(INTDIR)\ReportFromDlg.obj"
	-@erase "$(INTDIR)\ReportFromFieldDlg.obj"
	-@erase "$(INTDIR)\ReportOvernightTableViewer.obj"
	-@erase "$(INTDIR)\ReportPOPSItems.obj"
	-@erase "$(INTDIR)\ReportSameACTTableViewer.obj"
	-@erase "$(INTDIR)\ReportSameALTableViewer.obj"
	-@erase "$(INTDIR)\ReportSameDCDTableViewer.obj"
	-@erase "$(INTDIR)\ReportSameDESTableViewer.obj"
	-@erase "$(INTDIR)\ReportSameDTDTableViewer.obj"
	-@erase "$(INTDIR)\ReportSameFTYPXTableViewer.obj"
	-@erase "$(INTDIR)\ReportSameORGTableViewer.obj"
	-@erase "$(INTDIR)\ReportSameREGNTableViewer.obj"
	-@erase "$(INTDIR)\ReportSameTimeFrameTableViewer.obj"
	-@erase "$(INTDIR)\ReportSameTTYPTableViewer.obj"
	-@erase "$(INTDIR)\ReportSameTTYPXTableViewer.obj"
	-@erase "$(INTDIR)\ReportSeatDomIntMixTableViewer.obj"
	-@erase "$(INTDIR)\ReportSelectDlg.obj"
	-@erase "$(INTDIR)\ReportSeq1.obj"
	-@erase "$(INTDIR)\ReportSeqChoiceFieldDlg.obj"
	-@erase "$(INTDIR)\ReportSeqDateTimeDlg.obj"
	-@erase "$(INTDIR)\ReportSeqDlg.obj"
	-@erase "$(INTDIR)\ReportSeqField3DateTimeDlg.obj"
	-@erase "$(INTDIR)\ReportSeqFieldDlg.obj"
	-@erase "$(INTDIR)\ReportSortDlg.obj"
	-@erase "$(INTDIR)\ReportTableDlg.obj"
	-@erase "$(INTDIR)\ReportTableSpecialDlg.obj"
	-@erase "$(INTDIR)\ReportTimeFrameSelctDlg.obj"
	-@erase "$(INTDIR)\ReportXXXCedaData.obj"
	-@erase "$(INTDIR)\ResGroupDlg.obj"
	-@erase "$(INTDIR)\RotationActDlg.obj"
	-@erase "$(INTDIR)\RotationAltDlg.obj"
	-@erase "$(INTDIR)\RotationAptDlg.obj"
	-@erase "$(INTDIR)\RotationCedaFlightData.obj"
	-@erase "$(INTDIR)\RotationDlg.obj"
	-@erase "$(INTDIR)\RotationDlgCedaFlightData.obj"
	-@erase "$(INTDIR)\RotationGpuDlg.obj"
	-@erase "$(INTDIR)\RotationHAIDlg.obj"
	-@erase "$(INTDIR)\RotationISFDlg.obj"
	-@erase "$(INTDIR)\RotationJoinDlg.obj"
	-@erase "$(INTDIR)\RotationLoadDlg.obj"
	-@erase "$(INTDIR)\RotationLoadDlgATH.obj"
	-@erase "$(INTDIR)\RotationRegnDlg.obj"
	-@erase "$(INTDIR)\RotationTableChart.obj"
	-@erase "$(INTDIR)\RotationTables.obj"
	-@erase "$(INTDIR)\RotationTableViewer.obj"
	-@erase "$(INTDIR)\RotationVipDlg.obj"
	-@erase "$(INTDIR)\RotGDlgCedaFlightData.obj"
	-@erase "$(INTDIR)\RotGroundDlg.obj"
	-@erase "$(INTDIR)\SeasonAskDlg.obj"
	-@erase "$(INTDIR)\SeasonCedaFlightData.obj"
	-@erase "$(INTDIR)\SeasonCollectADAskDlg.obj"
	-@erase "$(INTDIR)\SeasonCollectAskDlg.obj"
	-@erase "$(INTDIR)\SeasonCollectCedaFlightData.obj"
	-@erase "$(INTDIR)\SeasonCollectDlg.obj"
	-@erase "$(INTDIR)\SeasonDlg.obj"
	-@erase "$(INTDIR)\SeasonDlgCedaFlightData.obj"
	-@erase "$(INTDIR)\SeasonFlightPropertySheet.obj"
	-@erase "$(INTDIR)\SeasonTableDlg.obj"
	-@erase "$(INTDIR)\SeasonTableViewer.obj"
	-@erase "$(INTDIR)\SetupDlg.obj"
	-@erase "$(INTDIR)\SpotAllocateDlg.obj"
	-@erase "$(INTDIR)\SpotAllocation.obj"
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(INTDIR)\TABLE.OBJ"
	-@erase "$(INTDIR)\UFISAmSink.obj"
	-@erase "$(INTDIR)\UniEingabe.obj"
	-@erase "$(INTDIR)\Utils.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\ViaTableCtrl.obj"
	-@erase "$(INTDIR)\VIPDlg.obj"
	-@erase "$(INTDIR)\WoResTableDlg.obj"
	-@erase "$(INTDIR)\WoResTableViewer.obj"
	-@erase "$(INTDIR)\WroChart.obj"
	-@erase "$(INTDIR)\WroDiagram.obj"
	-@erase "$(INTDIR)\WroDiaPropertySheet.obj"
	-@erase "$(INTDIR)\WroDiaViewer.obj"
	-@erase "$(INTDIR)\WroGantt.obj"
	-@erase "$(OUTDIR)\Fpms.exe"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32 
RSC_PROJ=/l 0x407 /fo"$(INTDIR)\FPMS.res" /d "NDEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\Fpms.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=c:\Ufis_Bin\Release\ufis32.lib c:\Ufis_Bin\ClassLib\Release\ccsclass.lib /nologo /subsystem:windows /incremental:no /pdb:"$(OUTDIR)\Fpms.pdb" /machine:I386 /out:"$(OUTDIR)\Fpms.exe" 
LINK32_OBJS= \
	"$(INTDIR)\AcirrDlg.obj" \
	"$(INTDIR)\AdditionalReport10TableViewer.obj" \
	"$(INTDIR)\AdditionalReport11TableViewer.obj" \
	"$(INTDIR)\AdditionalReport12TableViewer.obj" \
	"$(INTDIR)\AdditionalReport16TableViewer.obj" \
	"$(INTDIR)\AdditionalReport17TableViewer.obj" \
	"$(INTDIR)\AdditionalReport1TableViewer.obj" \
	"$(INTDIR)\AdditionalReport2TableViewer.obj" \
	"$(INTDIR)\AdditionalReport3TableViewer.obj" \
	"$(INTDIR)\AdditionalReport4TableViewer.obj" \
	"$(INTDIR)\AdditionalReport5TableViewer.obj" \
	"$(INTDIR)\AdditionalReport6TableViewer.obj" \
	"$(INTDIR)\AdditionalReport7TableViewer.obj" \
	"$(INTDIR)\AdditionalReport8TableViewer.obj" \
	"$(INTDIR)\AdditionalReport9TableViewer.obj" \
	"$(INTDIR)\AdditionalReportTableDlg.obj" \
	"$(INTDIR)\AirportInformationDlg.obj" \
	"$(INTDIR)\AllocateCca.obj" \
	"$(INTDIR)\AllocateCcaParameter.obj" \
	"$(INTDIR)\AltImportDelDlg.obj" \
	"$(INTDIR)\AskBox.obj" \
	"$(INTDIR)\AwBasicDataDlg.obj" \
	"$(INTDIR)\BackGround.obj" \
	"$(INTDIR)\BasePropertySheet.obj" \
	"$(INTDIR)\BasicData.obj" \
	"$(INTDIR)\BltCcaTableDlg.obj" \
	"$(INTDIR)\BltCcaTableViewer.obj" \
	"$(INTDIR)\BltChart.obj" \
	"$(INTDIR)\BltDiagram.obj" \
	"$(INTDIR)\BltDiaPropertySheet.obj" \
	"$(INTDIR)\BltDiaViewer.obj" \
	"$(INTDIR)\BltGantt.obj" \
	"$(INTDIR)\BltKonflikteDlg.obj" \
	"$(INTDIR)\BltOverviewTableDlg.obj" \
	"$(INTDIR)\BltOverviewTableViewer.obj" \
	"$(INTDIR)\buttonlistdlg.obj" \
	"$(INTDIR)\CcaCedaFlightData.obj" \
	"$(INTDIR)\CCAChangeTimes.obj" \
	"$(INTDIR)\CcaChart.obj" \
	"$(INTDIR)\CcaCommonDlg.obj" \
	"$(INTDIR)\CcaCommonSearchDlg.obj" \
	"$(INTDIR)\CcaCommonTableDlg.obj" \
	"$(INTDIR)\CcaCommonTableViewer.obj" \
	"$(INTDIR)\CcaDiagram.obj" \
	"$(INTDIR)\CcaDiaPropertySheet.obj" \
	"$(INTDIR)\CcaDiaViewer.obj" \
	"$(INTDIR)\CCAExpandDlg.obj" \
	"$(INTDIR)\CcaGantt.obj" \
	"$(INTDIR)\CCSGlobl.obj" \
	"$(INTDIR)\CCSPrint.obj" \
	"$(INTDIR)\CedaApxData.obj" \
	"$(INTDIR)\CedaCcaData.obj" \
	"$(INTDIR)\CedaCfgData.obj" \
	"$(INTDIR)\CedaCflData.obj" \
	"$(INTDIR)\CedaCountData.obj" \
	"$(INTDIR)\CedaDiaCcaData.obj" \
	"$(INTDIR)\CedaGpaData.obj" \
	"$(INTDIR)\CedaGrmData.obj" \
	"$(INTDIR)\CedaHaiData.obj" \
	"$(INTDIR)\CedaImpFlightData.obj" \
	"$(INTDIR)\CedaInfData.obj" \
	"$(INTDIR)\CedaInitModuData.obj" \
	"$(INTDIR)\CedaResGroupData.obj" \
	"$(INTDIR)\CicConfTableDlg.obj" \
	"$(INTDIR)\CicConfTableViewer.obj" \
	"$(INTDIR)\CicDemandTableDlg.obj" \
	"$(INTDIR)\CicDemandTableViewer.obj" \
	"$(INTDIR)\CicNoDemandTableDlg.obj" \
	"$(INTDIR)\CicNoDemandTableViewer.obj" \
	"$(INTDIR)\CollectHAIDlg.obj" \
	"$(INTDIR)\ConnectionAdvisor.obj" \
	"$(INTDIR)\CVIEWER.OBJ" \
	"$(INTDIR)\DailyCcaPrintDlg.obj" \
	"$(INTDIR)\DailyCedaFlightData.obj" \
	"$(INTDIR)\DailyPrintDlg.obj" \
	"$(INTDIR)\DailyScheduleTableDlg.obj" \
	"$(INTDIR)\DailyTableViewer.obj" \
	"$(INTDIR)\DailyVipCedaData.obj" \
	"$(INTDIR)\DataSet.obj" \
	"$(INTDIR)\DelelteCounterDlg.obj" \
	"$(INTDIR)\DGanttBarReport.obj" \
	"$(INTDIR)\DGanttChartReport.obj" \
	"$(INTDIR)\DGanttChartReportWnd.obj" \
	"$(INTDIR)\DiaCedaFlightData.obj" \
	"$(INTDIR)\FlightChart.obj" \
	"$(INTDIR)\FlightDiagram.obj" \
	"$(INTDIR)\FlightDiaPropertySheet.obj" \
	"$(INTDIR)\FlightDiaViewer.obj" \
	"$(INTDIR)\FlightGantt.obj" \
	"$(INTDIR)\FlightSearchPropertySheet.obj" \
	"$(INTDIR)\FlightSearchTableDlg.obj" \
	"$(INTDIR)\FlightSearchTableViewer.obj" \
	"$(INTDIR)\FPMS.obj" \
	"$(INTDIR)\GanttBarReport.obj" \
	"$(INTDIR)\GanttChartReport.obj" \
	"$(INTDIR)\GanttChartReportWnd.obj" \
	"$(INTDIR)\GatChart.obj" \
	"$(INTDIR)\GatDiagram.obj" \
	"$(INTDIR)\GatDiaPropertySheet.obj" \
	"$(INTDIR)\GatDiaViewer.obj" \
	"$(INTDIR)\GatGantt.obj" \
	"$(INTDIR)\HAIDlg.obj" \
	"$(INTDIR)\InitialLoadDlg.obj" \
	"$(INTDIR)\Konflikte.obj" \
	"$(INTDIR)\KonflikteDlg.obj" \
	"$(INTDIR)\List.obj" \
	"$(INTDIR)\LoginDlg.obj" \
	"$(INTDIR)\PosChart.obj" \
	"$(INTDIR)\PosDiagram.obj" \
	"$(INTDIR)\PosDiaPropertySheet.obj" \
	"$(INTDIR)\PosDiaViewer.obj" \
	"$(INTDIR)\PosGantt.obj" \
	"$(INTDIR)\PrintInfo.obj" \
	"$(INTDIR)\PrivList.obj" \
	"$(INTDIR)\ProgressDlg.obj" \
	"$(INTDIR)\ProxyDlg.obj" \
	"$(INTDIR)\PSGatGeometry.obj" \
	"$(INTDIR)\PSGeometrie.obj" \
	"$(INTDIR)\PSSearchFlightPage.obj" \
	"$(INTDIR)\PSSortFlightPage.obj" \
	"$(INTDIR)\PSSpecFilterPage.obj" \
	"$(INTDIR)\PSUniFilterPage.obj" \
	"$(INTDIR)\PSUniSortPage.obj" \
	"$(INTDIR)\RegisterDlg.obj" \
	"$(INTDIR)\Report10TableViewer.obj" \
	"$(INTDIR)\Report11TableViewer.obj" \
	"$(INTDIR)\Report12TableViewer.obj" \
	"$(INTDIR)\Report13TableViewer.obj" \
	"$(INTDIR)\Report14TableViewer.obj" \
	"$(INTDIR)\Report15TableViewer.obj" \
	"$(INTDIR)\Report16TableViewer.obj" \
	"$(INTDIR)\Report17DailyTableViewer.obj" \
	"$(INTDIR)\Report19DailyCcaTableViewer.obj" \
	"$(INTDIR)\Report1TableViewer.obj" \
	"$(INTDIR)\Report2TableViewer.obj" \
	"$(INTDIR)\Report3TableViewer.obj" \
	"$(INTDIR)\Report4TableViewer.obj" \
	"$(INTDIR)\Report5TableViewer.obj" \
	"$(INTDIR)\Report6NatCedaData.obj" \
	"$(INTDIR)\Report6TableViewer.obj" \
	"$(INTDIR)\Report7TableViewer.obj" \
	"$(INTDIR)\Report8TableViewer.obj" \
	"$(INTDIR)\Report9TableViewer.obj" \
	"$(INTDIR)\ReportActCedaData.obj" \
	"$(INTDIR)\ReportArrDepLoadPaxTableViewer.obj" \
	"$(INTDIR)\ReportArrDepSelctDlg.obj" \
	"$(INTDIR)\ReportArrDepTableViewer.obj" \
	"$(INTDIR)\ReportBltAllocViewer.obj" \
	"$(INTDIR)\ReportDailyFlightLogViewer.obj" \
	"$(INTDIR)\ReportDomIntTranTableViewer.obj" \
	"$(INTDIR)\ReportFieldDlg.obj" \
	"$(INTDIR)\ReportFlightDelayViewer.obj" \
	"$(INTDIR)\ReportFlightDomIntMixTableViewer.obj" \
	"$(INTDIR)\ReportFlightGPUTableViewer.obj" \
	"$(INTDIR)\ReportFromDlg.obj" \
	"$(INTDIR)\ReportFromFieldDlg.obj" \
	"$(INTDIR)\ReportOvernightTableViewer.obj" \
	"$(INTDIR)\ReportPOPSItems.obj" \
	"$(INTDIR)\ReportSameACTTableViewer.obj" \
	"$(INTDIR)\ReportSameALTableViewer.obj" \
	"$(INTDIR)\ReportSameDCDTableViewer.obj" \
	"$(INTDIR)\ReportSameDESTableViewer.obj" \
	"$(INTDIR)\ReportSameDTDTableViewer.obj" \
	"$(INTDIR)\ReportSameFTYPXTableViewer.obj" \
	"$(INTDIR)\ReportSameORGTableViewer.obj" \
	"$(INTDIR)\ReportSameREGNTableViewer.obj" \
	"$(INTDIR)\ReportSameTimeFrameTableViewer.obj" \
	"$(INTDIR)\ReportSameTTYPTableViewer.obj" \
	"$(INTDIR)\ReportSameTTYPXTableViewer.obj" \
	"$(INTDIR)\ReportSeatDomIntMixTableViewer.obj" \
	"$(INTDIR)\ReportSelectDlg.obj" \
	"$(INTDIR)\ReportSeq1.obj" \
	"$(INTDIR)\ReportSeqChoiceFieldDlg.obj" \
	"$(INTDIR)\ReportSeqDateTimeDlg.obj" \
	"$(INTDIR)\ReportSeqDlg.obj" \
	"$(INTDIR)\ReportSeqField3DateTimeDlg.obj" \
	"$(INTDIR)\ReportSeqFieldDlg.obj" \
	"$(INTDIR)\ReportSortDlg.obj" \
	"$(INTDIR)\ReportTableDlg.obj" \
	"$(INTDIR)\ReportTableSpecialDlg.obj" \
	"$(INTDIR)\ReportTimeFrameSelctDlg.obj" \
	"$(INTDIR)\ReportXXXCedaData.obj" \
	"$(INTDIR)\ResGroupDlg.obj" \
	"$(INTDIR)\RotationActDlg.obj" \
	"$(INTDIR)\RotationAltDlg.obj" \
	"$(INTDIR)\RotationAptDlg.obj" \
	"$(INTDIR)\RotationCedaFlightData.obj" \
	"$(INTDIR)\RotationDlg.obj" \
	"$(INTDIR)\RotationDlgCedaFlightData.obj" \
	"$(INTDIR)\RotationGpuDlg.obj" \
	"$(INTDIR)\RotationHAIDlg.obj" \
	"$(INTDIR)\RotationISFDlg.obj" \
	"$(INTDIR)\RotationJoinDlg.obj" \
	"$(INTDIR)\RotationLoadDlg.obj" \
	"$(INTDIR)\RotationLoadDlgATH.obj" \
	"$(INTDIR)\RotationRegnDlg.obj" \
	"$(INTDIR)\RotationTableChart.obj" \
	"$(INTDIR)\RotationTables.obj" \
	"$(INTDIR)\RotationTableViewer.obj" \
	"$(INTDIR)\RotationVipDlg.obj" \
	"$(INTDIR)\RotGDlgCedaFlightData.obj" \
	"$(INTDIR)\RotGroundDlg.obj" \
	"$(INTDIR)\SeasonAskDlg.obj" \
	"$(INTDIR)\SeasonCedaFlightData.obj" \
	"$(INTDIR)\SeasonCollectADAskDlg.obj" \
	"$(INTDIR)\SeasonCollectAskDlg.obj" \
	"$(INTDIR)\SeasonCollectCedaFlightData.obj" \
	"$(INTDIR)\SeasonCollectDlg.obj" \
	"$(INTDIR)\SeasonDlg.obj" \
	"$(INTDIR)\SeasonDlgCedaFlightData.obj" \
	"$(INTDIR)\SeasonFlightPropertySheet.obj" \
	"$(INTDIR)\SeasonTableDlg.obj" \
	"$(INTDIR)\SeasonTableViewer.obj" \
	"$(INTDIR)\SetupDlg.obj" \
	"$(INTDIR)\SpotAllocateDlg.obj" \
	"$(INTDIR)\SpotAllocation.obj" \
	"$(INTDIR)\StdAfx.obj" \
	"$(INTDIR)\TABLE.OBJ" \
	"$(INTDIR)\UFISAmSink.obj" \
	"$(INTDIR)\UniEingabe.obj" \
	"$(INTDIR)\Utils.obj" \
	"$(INTDIR)\ViaTableCtrl.obj" \
	"$(INTDIR)\VIPDlg.obj" \
	"$(INTDIR)\WoResTableDlg.obj" \
	"$(INTDIR)\WoResTableViewer.obj" \
	"$(INTDIR)\WroChart.obj" \
	"$(INTDIR)\WroDiagram.obj" \
	"$(INTDIR)\WroDiaPropertySheet.obj" \
	"$(INTDIR)\WroDiaViewer.obj" \
	"$(INTDIR)\WroGantt.obj" \
	"$(INTDIR)\FPMS.res"

"$(OUTDIR)\Fpms.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"

OUTDIR=C:\Ufis_Bin\Debug
INTDIR=C:\Ufis_Intermediate\Debug
# Begin Custom Macros
OutDir=C:\Ufis_Bin\Debug
# End Custom Macros

ALL : "..\Debug\FIPS.exe" "$(OUTDIR)\Fpms.bsc" "C:\Ufis_Intermediate\Debug\Fpms.pch"


CLEAN :
	-@erase "$(INTDIR)\AcirrDlg.obj"
	-@erase "$(INTDIR)\AcirrDlg.sbr"
	-@erase "$(INTDIR)\AdditionalReport10TableViewer.obj"
	-@erase "$(INTDIR)\AdditionalReport10TableViewer.sbr"
	-@erase "$(INTDIR)\AdditionalReport11TableViewer.obj"
	-@erase "$(INTDIR)\AdditionalReport11TableViewer.sbr"
	-@erase "$(INTDIR)\AdditionalReport12TableViewer.obj"
	-@erase "$(INTDIR)\AdditionalReport12TableViewer.sbr"
	-@erase "$(INTDIR)\AdditionalReport16TableViewer.obj"
	-@erase "$(INTDIR)\AdditionalReport16TableViewer.sbr"
	-@erase "$(INTDIR)\AdditionalReport17TableViewer.obj"
	-@erase "$(INTDIR)\AdditionalReport17TableViewer.sbr"
	-@erase "$(INTDIR)\AdditionalReport1TableViewer.obj"
	-@erase "$(INTDIR)\AdditionalReport1TableViewer.sbr"
	-@erase "$(INTDIR)\AdditionalReport2TableViewer.obj"
	-@erase "$(INTDIR)\AdditionalReport2TableViewer.sbr"
	-@erase "$(INTDIR)\AdditionalReport3TableViewer.obj"
	-@erase "$(INTDIR)\AdditionalReport3TableViewer.sbr"
	-@erase "$(INTDIR)\AdditionalReport4TableViewer.obj"
	-@erase "$(INTDIR)\AdditionalReport4TableViewer.sbr"
	-@erase "$(INTDIR)\AdditionalReport5TableViewer.obj"
	-@erase "$(INTDIR)\AdditionalReport5TableViewer.sbr"
	-@erase "$(INTDIR)\AdditionalReport6TableViewer.obj"
	-@erase "$(INTDIR)\AdditionalReport6TableViewer.sbr"
	-@erase "$(INTDIR)\AdditionalReport7TableViewer.obj"
	-@erase "$(INTDIR)\AdditionalReport7TableViewer.sbr"
	-@erase "$(INTDIR)\AdditionalReport8TableViewer.obj"
	-@erase "$(INTDIR)\AdditionalReport8TableViewer.sbr"
	-@erase "$(INTDIR)\AdditionalReport9TableViewer.obj"
	-@erase "$(INTDIR)\AdditionalReport9TableViewer.sbr"
	-@erase "$(INTDIR)\AdditionalReportTableDlg.obj"
	-@erase "$(INTDIR)\AdditionalReportTableDlg.sbr"
	-@erase "$(INTDIR)\AirportInformationDlg.obj"
	-@erase "$(INTDIR)\AirportInformationDlg.sbr"
	-@erase "$(INTDIR)\AllocateCca.obj"
	-@erase "$(INTDIR)\AllocateCca.sbr"
	-@erase "$(INTDIR)\AllocateCcaParameter.obj"
	-@erase "$(INTDIR)\AllocateCcaParameter.sbr"
	-@erase "$(INTDIR)\AltImportDelDlg.obj"
	-@erase "$(INTDIR)\AltImportDelDlg.sbr"
	-@erase "$(INTDIR)\AskBox.obj"
	-@erase "$(INTDIR)\AskBox.sbr"
	-@erase "$(INTDIR)\AwBasicDataDlg.obj"
	-@erase "$(INTDIR)\AwBasicDataDlg.sbr"
	-@erase "$(INTDIR)\BackGround.obj"
	-@erase "$(INTDIR)\BackGround.sbr"
	-@erase "$(INTDIR)\BasePropertySheet.obj"
	-@erase "$(INTDIR)\BasePropertySheet.sbr"
	-@erase "$(INTDIR)\BasicData.obj"
	-@erase "$(INTDIR)\BasicData.sbr"
	-@erase "$(INTDIR)\BltCcaTableDlg.obj"
	-@erase "$(INTDIR)\BltCcaTableDlg.sbr"
	-@erase "$(INTDIR)\BltCcaTableViewer.obj"
	-@erase "$(INTDIR)\BltCcaTableViewer.sbr"
	-@erase "$(INTDIR)\BltChart.obj"
	-@erase "$(INTDIR)\BltChart.sbr"
	-@erase "$(INTDIR)\BltDiagram.obj"
	-@erase "$(INTDIR)\BltDiagram.sbr"
	-@erase "$(INTDIR)\BltDiaPropertySheet.obj"
	-@erase "$(INTDIR)\BltDiaPropertySheet.sbr"
	-@erase "$(INTDIR)\BltDiaViewer.obj"
	-@erase "$(INTDIR)\BltDiaViewer.sbr"
	-@erase "$(INTDIR)\BltGantt.obj"
	-@erase "$(INTDIR)\BltGantt.sbr"
	-@erase "$(INTDIR)\BltKonflikteDlg.obj"
	-@erase "$(INTDIR)\BltKonflikteDlg.sbr"
	-@erase "$(INTDIR)\BltOverviewTableDlg.obj"
	-@erase "$(INTDIR)\BltOverviewTableDlg.sbr"
	-@erase "$(INTDIR)\BltOverviewTableViewer.obj"
	-@erase "$(INTDIR)\BltOverviewTableViewer.sbr"
	-@erase "$(INTDIR)\buttonlistdlg.obj"
	-@erase "$(INTDIR)\buttonlistdlg.sbr"
	-@erase "$(INTDIR)\CcaCedaFlightData.obj"
	-@erase "$(INTDIR)\CcaCedaFlightData.sbr"
	-@erase "$(INTDIR)\CCAChangeTimes.obj"
	-@erase "$(INTDIR)\CCAChangeTimes.sbr"
	-@erase "$(INTDIR)\CcaChart.obj"
	-@erase "$(INTDIR)\CcaChart.sbr"
	-@erase "$(INTDIR)\CcaCommonDlg.obj"
	-@erase "$(INTDIR)\CcaCommonDlg.sbr"
	-@erase "$(INTDIR)\CcaCommonSearchDlg.obj"
	-@erase "$(INTDIR)\CcaCommonSearchDlg.sbr"
	-@erase "$(INTDIR)\CcaCommonTableDlg.obj"
	-@erase "$(INTDIR)\CcaCommonTableDlg.sbr"
	-@erase "$(INTDIR)\CcaCommonTableViewer.obj"
	-@erase "$(INTDIR)\CcaCommonTableViewer.sbr"
	-@erase "$(INTDIR)\CcaDiagram.obj"
	-@erase "$(INTDIR)\CcaDiagram.sbr"
	-@erase "$(INTDIR)\CcaDiaPropertySheet.obj"
	-@erase "$(INTDIR)\CcaDiaPropertySheet.sbr"
	-@erase "$(INTDIR)\CcaDiaViewer.obj"
	-@erase "$(INTDIR)\CcaDiaViewer.sbr"
	-@erase "$(INTDIR)\CCAExpandDlg.obj"
	-@erase "$(INTDIR)\CCAExpandDlg.sbr"
	-@erase "$(INTDIR)\CcaGantt.obj"
	-@erase "$(INTDIR)\CcaGantt.sbr"
	-@erase "$(INTDIR)\CCSGlobl.obj"
	-@erase "$(INTDIR)\CCSGlobl.sbr"
	-@erase "$(INTDIR)\CCSPrint.obj"
	-@erase "$(INTDIR)\CCSPrint.sbr"
	-@erase "$(INTDIR)\CedaApxData.obj"
	-@erase "$(INTDIR)\CedaApxData.sbr"
	-@erase "$(INTDIR)\CedaCcaData.obj"
	-@erase "$(INTDIR)\CedaCcaData.sbr"
	-@erase "$(INTDIR)\CedaCfgData.obj"
	-@erase "$(INTDIR)\CedaCfgData.sbr"
	-@erase "$(INTDIR)\CedaCflData.obj"
	-@erase "$(INTDIR)\CedaCflData.sbr"
	-@erase "$(INTDIR)\CedaCountData.obj"
	-@erase "$(INTDIR)\CedaCountData.sbr"
	-@erase "$(INTDIR)\CedaDiaCcaData.obj"
	-@erase "$(INTDIR)\CedaDiaCcaData.sbr"
	-@erase "$(INTDIR)\CedaGpaData.obj"
	-@erase "$(INTDIR)\CedaGpaData.sbr"
	-@erase "$(INTDIR)\CedaGrmData.obj"
	-@erase "$(INTDIR)\CedaGrmData.sbr"
	-@erase "$(INTDIR)\CedaHaiData.obj"
	-@erase "$(INTDIR)\CedaHaiData.sbr"
	-@erase "$(INTDIR)\CedaImpFlightData.obj"
	-@erase "$(INTDIR)\CedaImpFlightData.sbr"
	-@erase "$(INTDIR)\CedaInfData.obj"
	-@erase "$(INTDIR)\CedaInfData.sbr"
	-@erase "$(INTDIR)\CedaInitModuData.obj"
	-@erase "$(INTDIR)\CedaInitModuData.sbr"
	-@erase "$(INTDIR)\CedaResGroupData.obj"
	-@erase "$(INTDIR)\CedaResGroupData.sbr"
	-@erase "$(INTDIR)\CicConfTableDlg.obj"
	-@erase "$(INTDIR)\CicConfTableDlg.sbr"
	-@erase "$(INTDIR)\CicConfTableViewer.obj"
	-@erase "$(INTDIR)\CicConfTableViewer.sbr"
	-@erase "$(INTDIR)\CicDemandTableDlg.obj"
	-@erase "$(INTDIR)\CicDemandTableDlg.sbr"
	-@erase "$(INTDIR)\CicDemandTableViewer.obj"
	-@erase "$(INTDIR)\CicDemandTableViewer.sbr"
	-@erase "$(INTDIR)\CicNoDemandTableDlg.obj"
	-@erase "$(INTDIR)\CicNoDemandTableDlg.sbr"
	-@erase "$(INTDIR)\CicNoDemandTableViewer.obj"
	-@erase "$(INTDIR)\CicNoDemandTableViewer.sbr"
	-@erase "$(INTDIR)\CollectHAIDlg.obj"
	-@erase "$(INTDIR)\CollectHAIDlg.sbr"
	-@erase "$(INTDIR)\ConnectionAdvisor.obj"
	-@erase "$(INTDIR)\ConnectionAdvisor.sbr"
	-@erase "$(INTDIR)\CVIEWER.OBJ"
	-@erase "$(INTDIR)\CVIEWER.SBR"
	-@erase "$(INTDIR)\DailyCcaPrintDlg.obj"
	-@erase "$(INTDIR)\DailyCcaPrintDlg.sbr"
	-@erase "$(INTDIR)\DailyCedaFlightData.obj"
	-@erase "$(INTDIR)\DailyCedaFlightData.sbr"
	-@erase "$(INTDIR)\DailyPrintDlg.obj"
	-@erase "$(INTDIR)\DailyPrintDlg.sbr"
	-@erase "$(INTDIR)\DailyScheduleTableDlg.obj"
	-@erase "$(INTDIR)\DailyScheduleTableDlg.sbr"
	-@erase "$(INTDIR)\DailyTableViewer.obj"
	-@erase "$(INTDIR)\DailyTableViewer.sbr"
	-@erase "$(INTDIR)\DailyVipCedaData.obj"
	-@erase "$(INTDIR)\DailyVipCedaData.sbr"
	-@erase "$(INTDIR)\DataSet.obj"
	-@erase "$(INTDIR)\DataSet.sbr"
	-@erase "$(INTDIR)\DelelteCounterDlg.obj"
	-@erase "$(INTDIR)\DelelteCounterDlg.sbr"
	-@erase "$(INTDIR)\DGanttBarReport.obj"
	-@erase "$(INTDIR)\DGanttBarReport.sbr"
	-@erase "$(INTDIR)\DGanttChartReport.obj"
	-@erase "$(INTDIR)\DGanttChartReport.sbr"
	-@erase "$(INTDIR)\DGanttChartReportWnd.obj"
	-@erase "$(INTDIR)\DGanttChartReportWnd.sbr"
	-@erase "$(INTDIR)\DiaCedaFlightData.obj"
	-@erase "$(INTDIR)\DiaCedaFlightData.sbr"
	-@erase "$(INTDIR)\FlightChart.obj"
	-@erase "$(INTDIR)\FlightChart.sbr"
	-@erase "$(INTDIR)\FlightDiagram.obj"
	-@erase "$(INTDIR)\FlightDiagram.sbr"
	-@erase "$(INTDIR)\FlightDiaPropertySheet.obj"
	-@erase "$(INTDIR)\FlightDiaPropertySheet.sbr"
	-@erase "$(INTDIR)\FlightDiaViewer.obj"
	-@erase "$(INTDIR)\FlightDiaViewer.sbr"
	-@erase "$(INTDIR)\FlightGantt.obj"
	-@erase "$(INTDIR)\FlightGantt.sbr"
	-@erase "$(INTDIR)\FlightSearchPropertySheet.obj"
	-@erase "$(INTDIR)\FlightSearchPropertySheet.sbr"
	-@erase "$(INTDIR)\FlightSearchTableDlg.obj"
	-@erase "$(INTDIR)\FlightSearchTableDlg.sbr"
	-@erase "$(INTDIR)\FlightSearchTableViewer.obj"
	-@erase "$(INTDIR)\FlightSearchTableViewer.sbr"
	-@erase "$(INTDIR)\FPMS.obj"
	-@erase "$(INTDIR)\Fpms.pch"
	-@erase "$(INTDIR)\FPMS.res"
	-@erase "$(INTDIR)\FPMS.sbr"
	-@erase "$(INTDIR)\GanttBarReport.obj"
	-@erase "$(INTDIR)\GanttBarReport.sbr"
	-@erase "$(INTDIR)\GanttChartReport.obj"
	-@erase "$(INTDIR)\GanttChartReport.sbr"
	-@erase "$(INTDIR)\GanttChartReportWnd.obj"
	-@erase "$(INTDIR)\GanttChartReportWnd.sbr"
	-@erase "$(INTDIR)\GatChart.obj"
	-@erase "$(INTDIR)\GatChart.sbr"
	-@erase "$(INTDIR)\GatDiagram.obj"
	-@erase "$(INTDIR)\GatDiagram.sbr"
	-@erase "$(INTDIR)\GatDiaPropertySheet.obj"
	-@erase "$(INTDIR)\GatDiaPropertySheet.sbr"
	-@erase "$(INTDIR)\GatDiaViewer.obj"
	-@erase "$(INTDIR)\GatDiaViewer.sbr"
	-@erase "$(INTDIR)\GatGantt.obj"
	-@erase "$(INTDIR)\GatGantt.sbr"
	-@erase "$(INTDIR)\HAIDlg.obj"
	-@erase "$(INTDIR)\HAIDlg.sbr"
	-@erase "$(INTDIR)\InitialLoadDlg.obj"
	-@erase "$(INTDIR)\InitialLoadDlg.sbr"
	-@erase "$(INTDIR)\Konflikte.obj"
	-@erase "$(INTDIR)\Konflikte.sbr"
	-@erase "$(INTDIR)\KonflikteDlg.obj"
	-@erase "$(INTDIR)\KonflikteDlg.sbr"
	-@erase "$(INTDIR)\List.obj"
	-@erase "$(INTDIR)\List.sbr"
	-@erase "$(INTDIR)\LoginDlg.obj"
	-@erase "$(INTDIR)\LoginDlg.sbr"
	-@erase "$(INTDIR)\PosChart.obj"
	-@erase "$(INTDIR)\PosChart.sbr"
	-@erase "$(INTDIR)\PosDiagram.obj"
	-@erase "$(INTDIR)\PosDiagram.sbr"
	-@erase "$(INTDIR)\PosDiaPropertySheet.obj"
	-@erase "$(INTDIR)\PosDiaPropertySheet.sbr"
	-@erase "$(INTDIR)\PosDiaViewer.obj"
	-@erase "$(INTDIR)\PosDiaViewer.sbr"
	-@erase "$(INTDIR)\PosGantt.obj"
	-@erase "$(INTDIR)\PosGantt.sbr"
	-@erase "$(INTDIR)\PrintInfo.obj"
	-@erase "$(INTDIR)\PrintInfo.sbr"
	-@erase "$(INTDIR)\PrivList.obj"
	-@erase "$(INTDIR)\PrivList.sbr"
	-@erase "$(INTDIR)\ProgressDlg.obj"
	-@erase "$(INTDIR)\ProgressDlg.sbr"
	-@erase "$(INTDIR)\ProxyDlg.obj"
	-@erase "$(INTDIR)\ProxyDlg.sbr"
	-@erase "$(INTDIR)\PSGatGeometry.obj"
	-@erase "$(INTDIR)\PSGatGeometry.sbr"
	-@erase "$(INTDIR)\PSGeometrie.obj"
	-@erase "$(INTDIR)\PSGeometrie.sbr"
	-@erase "$(INTDIR)\PSSearchFlightPage.obj"
	-@erase "$(INTDIR)\PSSearchFlightPage.sbr"
	-@erase "$(INTDIR)\PSSortFlightPage.obj"
	-@erase "$(INTDIR)\PSSortFlightPage.sbr"
	-@erase "$(INTDIR)\PSSpecFilterPage.obj"
	-@erase "$(INTDIR)\PSSpecFilterPage.sbr"
	-@erase "$(INTDIR)\PSUniFilterPage.obj"
	-@erase "$(INTDIR)\PSUniFilterPage.sbr"
	-@erase "$(INTDIR)\PSUniSortPage.obj"
	-@erase "$(INTDIR)\PSUniSortPage.sbr"
	-@erase "$(INTDIR)\RegisterDlg.obj"
	-@erase "$(INTDIR)\RegisterDlg.sbr"
	-@erase "$(INTDIR)\Report10TableViewer.obj"
	-@erase "$(INTDIR)\Report10TableViewer.sbr"
	-@erase "$(INTDIR)\Report11TableViewer.obj"
	-@erase "$(INTDIR)\Report11TableViewer.sbr"
	-@erase "$(INTDIR)\Report12TableViewer.obj"
	-@erase "$(INTDIR)\Report12TableViewer.sbr"
	-@erase "$(INTDIR)\Report13TableViewer.obj"
	-@erase "$(INTDIR)\Report13TableViewer.sbr"
	-@erase "$(INTDIR)\Report14TableViewer.obj"
	-@erase "$(INTDIR)\Report14TableViewer.sbr"
	-@erase "$(INTDIR)\Report15TableViewer.obj"
	-@erase "$(INTDIR)\Report15TableViewer.sbr"
	-@erase "$(INTDIR)\Report16TableViewer.obj"
	-@erase "$(INTDIR)\Report16TableViewer.sbr"
	-@erase "$(INTDIR)\Report17DailyTableViewer.obj"
	-@erase "$(INTDIR)\Report17DailyTableViewer.sbr"
	-@erase "$(INTDIR)\Report19DailyCcaTableViewer.obj"
	-@erase "$(INTDIR)\Report19DailyCcaTableViewer.sbr"
	-@erase "$(INTDIR)\Report1TableViewer.obj"
	-@erase "$(INTDIR)\Report1TableViewer.sbr"
	-@erase "$(INTDIR)\Report2TableViewer.obj"
	-@erase "$(INTDIR)\Report2TableViewer.sbr"
	-@erase "$(INTDIR)\Report3TableViewer.obj"
	-@erase "$(INTDIR)\Report3TableViewer.sbr"
	-@erase "$(INTDIR)\Report4TableViewer.obj"
	-@erase "$(INTDIR)\Report4TableViewer.sbr"
	-@erase "$(INTDIR)\Report5TableViewer.obj"
	-@erase "$(INTDIR)\Report5TableViewer.sbr"
	-@erase "$(INTDIR)\Report6NatCedaData.obj"
	-@erase "$(INTDIR)\Report6NatCedaData.sbr"
	-@erase "$(INTDIR)\Report6TableViewer.obj"
	-@erase "$(INTDIR)\Report6TableViewer.sbr"
	-@erase "$(INTDIR)\Report7TableViewer.obj"
	-@erase "$(INTDIR)\Report7TableViewer.sbr"
	-@erase "$(INTDIR)\Report8TableViewer.obj"
	-@erase "$(INTDIR)\Report8TableViewer.sbr"
	-@erase "$(INTDIR)\Report9TableViewer.obj"
	-@erase "$(INTDIR)\Report9TableViewer.sbr"
	-@erase "$(INTDIR)\ReportActCedaData.obj"
	-@erase "$(INTDIR)\ReportActCedaData.sbr"
	-@erase "$(INTDIR)\ReportArrDepLoadPaxTableViewer.obj"
	-@erase "$(INTDIR)\ReportArrDepLoadPaxTableViewer.sbr"
	-@erase "$(INTDIR)\ReportArrDepSelctDlg.obj"
	-@erase "$(INTDIR)\ReportArrDepSelctDlg.sbr"
	-@erase "$(INTDIR)\ReportArrDepTableViewer.obj"
	-@erase "$(INTDIR)\ReportArrDepTableViewer.sbr"
	-@erase "$(INTDIR)\ReportBltAllocViewer.obj"
	-@erase "$(INTDIR)\ReportBltAllocViewer.sbr"
	-@erase "$(INTDIR)\ReportDailyFlightLogViewer.obj"
	-@erase "$(INTDIR)\ReportDailyFlightLogViewer.sbr"
	-@erase "$(INTDIR)\ReportDomIntTranTableViewer.obj"
	-@erase "$(INTDIR)\ReportDomIntTranTableViewer.sbr"
	-@erase "$(INTDIR)\ReportFieldDlg.obj"
	-@erase "$(INTDIR)\ReportFieldDlg.sbr"
	-@erase "$(INTDIR)\ReportFlightDelayViewer.obj"
	-@erase "$(INTDIR)\ReportFlightDelayViewer.sbr"
	-@erase "$(INTDIR)\ReportFlightDomIntMixTableViewer.obj"
	-@erase "$(INTDIR)\ReportFlightDomIntMixTableViewer.sbr"
	-@erase "$(INTDIR)\ReportFlightGPUTableViewer.obj"
	-@erase "$(INTDIR)\ReportFlightGPUTableViewer.sbr"
	-@erase "$(INTDIR)\ReportFromDlg.obj"
	-@erase "$(INTDIR)\ReportFromDlg.sbr"
	-@erase "$(INTDIR)\ReportFromFieldDlg.obj"
	-@erase "$(INTDIR)\ReportFromFieldDlg.sbr"
	-@erase "$(INTDIR)\ReportOvernightTableViewer.obj"
	-@erase "$(INTDIR)\ReportOvernightTableViewer.sbr"
	-@erase "$(INTDIR)\ReportPOPSItems.obj"
	-@erase "$(INTDIR)\ReportPOPSItems.sbr"
	-@erase "$(INTDIR)\ReportSameACTTableViewer.obj"
	-@erase "$(INTDIR)\ReportSameACTTableViewer.sbr"
	-@erase "$(INTDIR)\ReportSameALTableViewer.obj"
	-@erase "$(INTDIR)\ReportSameALTableViewer.sbr"
	-@erase "$(INTDIR)\ReportSameDCDTableViewer.obj"
	-@erase "$(INTDIR)\ReportSameDCDTableViewer.sbr"
	-@erase "$(INTDIR)\ReportSameDESTableViewer.obj"
	-@erase "$(INTDIR)\ReportSameDESTableViewer.sbr"
	-@erase "$(INTDIR)\ReportSameDTDTableViewer.obj"
	-@erase "$(INTDIR)\ReportSameDTDTableViewer.sbr"
	-@erase "$(INTDIR)\ReportSameFTYPXTableViewer.obj"
	-@erase "$(INTDIR)\ReportSameFTYPXTableViewer.sbr"
	-@erase "$(INTDIR)\ReportSameORGTableViewer.obj"
	-@erase "$(INTDIR)\ReportSameORGTableViewer.sbr"
	-@erase "$(INTDIR)\ReportSameREGNTableViewer.obj"
	-@erase "$(INTDIR)\ReportSameREGNTableViewer.sbr"
	-@erase "$(INTDIR)\ReportSameTimeFrameTableViewer.obj"
	-@erase "$(INTDIR)\ReportSameTimeFrameTableViewer.sbr"
	-@erase "$(INTDIR)\ReportSameTTYPTableViewer.obj"
	-@erase "$(INTDIR)\ReportSameTTYPTableViewer.sbr"
	-@erase "$(INTDIR)\ReportSameTTYPXTableViewer.obj"
	-@erase "$(INTDIR)\ReportSameTTYPXTableViewer.sbr"
	-@erase "$(INTDIR)\ReportSeatDomIntMixTableViewer.obj"
	-@erase "$(INTDIR)\ReportSeatDomIntMixTableViewer.sbr"
	-@erase "$(INTDIR)\ReportSelectDlg.obj"
	-@erase "$(INTDIR)\ReportSelectDlg.sbr"
	-@erase "$(INTDIR)\ReportSeq1.obj"
	-@erase "$(INTDIR)\ReportSeq1.sbr"
	-@erase "$(INTDIR)\ReportSeqChoiceFieldDlg.obj"
	-@erase "$(INTDIR)\ReportSeqChoiceFieldDlg.sbr"
	-@erase "$(INTDIR)\ReportSeqDateTimeDlg.obj"
	-@erase "$(INTDIR)\ReportSeqDateTimeDlg.sbr"
	-@erase "$(INTDIR)\ReportSeqDlg.obj"
	-@erase "$(INTDIR)\ReportSeqDlg.sbr"
	-@erase "$(INTDIR)\ReportSeqField3DateTimeDlg.obj"
	-@erase "$(INTDIR)\ReportSeqField3DateTimeDlg.sbr"
	-@erase "$(INTDIR)\ReportSeqFieldDlg.obj"
	-@erase "$(INTDIR)\ReportSeqFieldDlg.sbr"
	-@erase "$(INTDIR)\ReportSortDlg.obj"
	-@erase "$(INTDIR)\ReportSortDlg.sbr"
	-@erase "$(INTDIR)\ReportTableDlg.obj"
	-@erase "$(INTDIR)\ReportTableDlg.sbr"
	-@erase "$(INTDIR)\ReportTableSpecialDlg.obj"
	-@erase "$(INTDIR)\ReportTableSpecialDlg.sbr"
	-@erase "$(INTDIR)\ReportTimeFrameSelctDlg.obj"
	-@erase "$(INTDIR)\ReportTimeFrameSelctDlg.sbr"
	-@erase "$(INTDIR)\ReportXXXCedaData.obj"
	-@erase "$(INTDIR)\ReportXXXCedaData.sbr"
	-@erase "$(INTDIR)\ResGroupDlg.obj"
	-@erase "$(INTDIR)\ResGroupDlg.sbr"
	-@erase "$(INTDIR)\RotationActDlg.obj"
	-@erase "$(INTDIR)\RotationActDlg.sbr"
	-@erase "$(INTDIR)\RotationAltDlg.obj"
	-@erase "$(INTDIR)\RotationAltDlg.sbr"
	-@erase "$(INTDIR)\RotationAptDlg.obj"
	-@erase "$(INTDIR)\RotationAptDlg.sbr"
	-@erase "$(INTDIR)\RotationCedaFlightData.obj"
	-@erase "$(INTDIR)\RotationCedaFlightData.sbr"
	-@erase "$(INTDIR)\RotationDlg.obj"
	-@erase "$(INTDIR)\RotationDlg.sbr"
	-@erase "$(INTDIR)\RotationDlgCedaFlightData.obj"
	-@erase "$(INTDIR)\RotationDlgCedaFlightData.sbr"
	-@erase "$(INTDIR)\RotationGpuDlg.obj"
	-@erase "$(INTDIR)\RotationGpuDlg.sbr"
	-@erase "$(INTDIR)\RotationHAIDlg.obj"
	-@erase "$(INTDIR)\RotationHAIDlg.sbr"
	-@erase "$(INTDIR)\RotationISFDlg.obj"
	-@erase "$(INTDIR)\RotationISFDlg.sbr"
	-@erase "$(INTDIR)\RotationJoinDlg.obj"
	-@erase "$(INTDIR)\RotationJoinDlg.sbr"
	-@erase "$(INTDIR)\RotationLoadDlg.obj"
	-@erase "$(INTDIR)\RotationLoadDlg.sbr"
	-@erase "$(INTDIR)\RotationLoadDlgATH.obj"
	-@erase "$(INTDIR)\RotationLoadDlgATH.sbr"
	-@erase "$(INTDIR)\RotationRegnDlg.obj"
	-@erase "$(INTDIR)\RotationRegnDlg.sbr"
	-@erase "$(INTDIR)\RotationTableChart.obj"
	-@erase "$(INTDIR)\RotationTableChart.sbr"
	-@erase "$(INTDIR)\RotationTables.obj"
	-@erase "$(INTDIR)\RotationTables.sbr"
	-@erase "$(INTDIR)\RotationTableViewer.obj"
	-@erase "$(INTDIR)\RotationTableViewer.sbr"
	-@erase "$(INTDIR)\RotationVipDlg.obj"
	-@erase "$(INTDIR)\RotationVipDlg.sbr"
	-@erase "$(INTDIR)\RotGDlgCedaFlightData.obj"
	-@erase "$(INTDIR)\RotGDlgCedaFlightData.sbr"
	-@erase "$(INTDIR)\RotGroundDlg.obj"
	-@erase "$(INTDIR)\RotGroundDlg.sbr"
	-@erase "$(INTDIR)\SeasonAskDlg.obj"
	-@erase "$(INTDIR)\SeasonAskDlg.sbr"
	-@erase "$(INTDIR)\SeasonCedaFlightData.obj"
	-@erase "$(INTDIR)\SeasonCedaFlightData.sbr"
	-@erase "$(INTDIR)\SeasonCollectADAskDlg.obj"
	-@erase "$(INTDIR)\SeasonCollectADAskDlg.sbr"
	-@erase "$(INTDIR)\SeasonCollectAskDlg.obj"
	-@erase "$(INTDIR)\SeasonCollectAskDlg.sbr"
	-@erase "$(INTDIR)\SeasonCollectCedaFlightData.obj"
	-@erase "$(INTDIR)\SeasonCollectCedaFlightData.sbr"
	-@erase "$(INTDIR)\SeasonCollectDlg.obj"
	-@erase "$(INTDIR)\SeasonCollectDlg.sbr"
	-@erase "$(INTDIR)\SeasonDlg.obj"
	-@erase "$(INTDIR)\SeasonDlg.sbr"
	-@erase "$(INTDIR)\SeasonDlgCedaFlightData.obj"
	-@erase "$(INTDIR)\SeasonDlgCedaFlightData.sbr"
	-@erase "$(INTDIR)\SeasonFlightPropertySheet.obj"
	-@erase "$(INTDIR)\SeasonFlightPropertySheet.sbr"
	-@erase "$(INTDIR)\SeasonTableDlg.obj"
	-@erase "$(INTDIR)\SeasonTableDlg.sbr"
	-@erase "$(INTDIR)\SeasonTableViewer.obj"
	-@erase "$(INTDIR)\SeasonTableViewer.sbr"
	-@erase "$(INTDIR)\SetupDlg.obj"
	-@erase "$(INTDIR)\SetupDlg.sbr"
	-@erase "$(INTDIR)\SpotAllocateDlg.obj"
	-@erase "$(INTDIR)\SpotAllocateDlg.sbr"
	-@erase "$(INTDIR)\SpotAllocation.obj"
	-@erase "$(INTDIR)\SpotAllocation.sbr"
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(INTDIR)\StdAfx.sbr"
	-@erase "$(INTDIR)\TABLE.OBJ"
	-@erase "$(INTDIR)\TABLE.SBR"
	-@erase "$(INTDIR)\UFISAmSink.obj"
	-@erase "$(INTDIR)\UFISAmSink.sbr"
	-@erase "$(INTDIR)\UniEingabe.obj"
	-@erase "$(INTDIR)\UniEingabe.sbr"
	-@erase "$(INTDIR)\Utils.obj"
	-@erase "$(INTDIR)\Utils.sbr"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(INTDIR)\ViaTableCtrl.obj"
	-@erase "$(INTDIR)\ViaTableCtrl.sbr"
	-@erase "$(INTDIR)\VIPDlg.obj"
	-@erase "$(INTDIR)\VIPDlg.sbr"
	-@erase "$(INTDIR)\WoResTableDlg.obj"
	-@erase "$(INTDIR)\WoResTableDlg.sbr"
	-@erase "$(INTDIR)\WoResTableViewer.obj"
	-@erase "$(INTDIR)\WoResTableViewer.sbr"
	-@erase "$(INTDIR)\WroChart.obj"
	-@erase "$(INTDIR)\WroChart.sbr"
	-@erase "$(INTDIR)\WroDiagram.obj"
	-@erase "$(INTDIR)\WroDiagram.sbr"
	-@erase "$(INTDIR)\WroDiaPropertySheet.obj"
	-@erase "$(INTDIR)\WroDiaPropertySheet.sbr"
	-@erase "$(INTDIR)\WroDiaViewer.obj"
	-@erase "$(INTDIR)\WroDiaViewer.sbr"
	-@erase "$(INTDIR)\WroGantt.obj"
	-@erase "$(INTDIR)\WroGantt.sbr"
	-@erase "$(OUTDIR)\FIPS.pdb"
	-@erase "$(OUTDIR)\Fpms.bsc"
	-@erase "..\Debug\FIPS.exe"
	-@erase "..\Debug\FIPS.ilk"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /FR"$(INTDIR)\\" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32 
RSC_PROJ=/l 0x809 /fo"$(INTDIR)\FPMS.res" /d "_DEBUG" /d "_ENGLISH" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\Fpms.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\AcirrDlg.sbr" \
	"$(INTDIR)\AdditionalReport10TableViewer.sbr" \
	"$(INTDIR)\AdditionalReport11TableViewer.sbr" \
	"$(INTDIR)\AdditionalReport12TableViewer.sbr" \
	"$(INTDIR)\AdditionalReport16TableViewer.sbr" \
	"$(INTDIR)\AdditionalReport17TableViewer.sbr" \
	"$(INTDIR)\AdditionalReport1TableViewer.sbr" \
	"$(INTDIR)\AdditionalReport2TableViewer.sbr" \
	"$(INTDIR)\AdditionalReport3TableViewer.sbr" \
	"$(INTDIR)\AdditionalReport4TableViewer.sbr" \
	"$(INTDIR)\AdditionalReport5TableViewer.sbr" \
	"$(INTDIR)\AdditionalReport6TableViewer.sbr" \
	"$(INTDIR)\AdditionalReport7TableViewer.sbr" \
	"$(INTDIR)\AdditionalReport8TableViewer.sbr" \
	"$(INTDIR)\AdditionalReport9TableViewer.sbr" \
	"$(INTDIR)\AdditionalReportTableDlg.sbr" \
	"$(INTDIR)\AirportInformationDlg.sbr" \
	"$(INTDIR)\AllocateCca.sbr" \
	"$(INTDIR)\AllocateCcaParameter.sbr" \
	"$(INTDIR)\AltImportDelDlg.sbr" \
	"$(INTDIR)\AskBox.sbr" \
	"$(INTDIR)\AwBasicDataDlg.sbr" \
	"$(INTDIR)\BackGround.sbr" \
	"$(INTDIR)\BasePropertySheet.sbr" \
	"$(INTDIR)\BasicData.sbr" \
	"$(INTDIR)\BltCcaTableDlg.sbr" \
	"$(INTDIR)\BltCcaTableViewer.sbr" \
	"$(INTDIR)\BltChart.sbr" \
	"$(INTDIR)\BltDiagram.sbr" \
	"$(INTDIR)\BltDiaPropertySheet.sbr" \
	"$(INTDIR)\BltDiaViewer.sbr" \
	"$(INTDIR)\BltGantt.sbr" \
	"$(INTDIR)\BltKonflikteDlg.sbr" \
	"$(INTDIR)\BltOverviewTableDlg.sbr" \
	"$(INTDIR)\BltOverviewTableViewer.sbr" \
	"$(INTDIR)\buttonlistdlg.sbr" \
	"$(INTDIR)\CcaCedaFlightData.sbr" \
	"$(INTDIR)\CCAChangeTimes.sbr" \
	"$(INTDIR)\CcaChart.sbr" \
	"$(INTDIR)\CcaCommonDlg.sbr" \
	"$(INTDIR)\CcaCommonSearchDlg.sbr" \
	"$(INTDIR)\CcaCommonTableDlg.sbr" \
	"$(INTDIR)\CcaCommonTableViewer.sbr" \
	"$(INTDIR)\CcaDiagram.sbr" \
	"$(INTDIR)\CcaDiaPropertySheet.sbr" \
	"$(INTDIR)\CcaDiaViewer.sbr" \
	"$(INTDIR)\CCAExpandDlg.sbr" \
	"$(INTDIR)\CcaGantt.sbr" \
	"$(INTDIR)\CCSGlobl.sbr" \
	"$(INTDIR)\CCSPrint.sbr" \
	"$(INTDIR)\CedaApxData.sbr" \
	"$(INTDIR)\CedaCcaData.sbr" \
	"$(INTDIR)\CedaCfgData.sbr" \
	"$(INTDIR)\CedaCflData.sbr" \
	"$(INTDIR)\CedaCountData.sbr" \
	"$(INTDIR)\CedaDiaCcaData.sbr" \
	"$(INTDIR)\CedaGpaData.sbr" \
	"$(INTDIR)\CedaGrmData.sbr" \
	"$(INTDIR)\CedaHaiData.sbr" \
	"$(INTDIR)\CedaImpFlightData.sbr" \
	"$(INTDIR)\CedaInfData.sbr" \
	"$(INTDIR)\CedaInitModuData.sbr" \
	"$(INTDIR)\CedaResGroupData.sbr" \
	"$(INTDIR)\CicConfTableDlg.sbr" \
	"$(INTDIR)\CicConfTableViewer.sbr" \
	"$(INTDIR)\CicDemandTableDlg.sbr" \
	"$(INTDIR)\CicDemandTableViewer.sbr" \
	"$(INTDIR)\CicNoDemandTableDlg.sbr" \
	"$(INTDIR)\CicNoDemandTableViewer.sbr" \
	"$(INTDIR)\CollectHAIDlg.sbr" \
	"$(INTDIR)\ConnectionAdvisor.sbr" \
	"$(INTDIR)\CVIEWER.SBR" \
	"$(INTDIR)\DailyCcaPrintDlg.sbr" \
	"$(INTDIR)\DailyCedaFlightData.sbr" \
	"$(INTDIR)\DailyPrintDlg.sbr" \
	"$(INTDIR)\DailyScheduleTableDlg.sbr" \
	"$(INTDIR)\DailyTableViewer.sbr" \
	"$(INTDIR)\DailyVipCedaData.sbr" \
	"$(INTDIR)\DataSet.sbr" \
	"$(INTDIR)\DelelteCounterDlg.sbr" \
	"$(INTDIR)\DGanttBarReport.sbr" \
	"$(INTDIR)\DGanttChartReport.sbr" \
	"$(INTDIR)\DGanttChartReportWnd.sbr" \
	"$(INTDIR)\DiaCedaFlightData.sbr" \
	"$(INTDIR)\FlightChart.sbr" \
	"$(INTDIR)\FlightDiagram.sbr" \
	"$(INTDIR)\FlightDiaPropertySheet.sbr" \
	"$(INTDIR)\FlightDiaViewer.sbr" \
	"$(INTDIR)\FlightGantt.sbr" \
	"$(INTDIR)\FlightSearchPropertySheet.sbr" \
	"$(INTDIR)\FlightSearchTableDlg.sbr" \
	"$(INTDIR)\FlightSearchTableViewer.sbr" \
	"$(INTDIR)\FPMS.sbr" \
	"$(INTDIR)\GanttBarReport.sbr" \
	"$(INTDIR)\GanttChartReport.sbr" \
	"$(INTDIR)\GanttChartReportWnd.sbr" \
	"$(INTDIR)\GatChart.sbr" \
	"$(INTDIR)\GatDiagram.sbr" \
	"$(INTDIR)\GatDiaPropertySheet.sbr" \
	"$(INTDIR)\GatDiaViewer.sbr" \
	"$(INTDIR)\GatGantt.sbr" \
	"$(INTDIR)\HAIDlg.sbr" \
	"$(INTDIR)\InitialLoadDlg.sbr" \
	"$(INTDIR)\Konflikte.sbr" \
	"$(INTDIR)\KonflikteDlg.sbr" \
	"$(INTDIR)\List.sbr" \
	"$(INTDIR)\LoginDlg.sbr" \
	"$(INTDIR)\PosChart.sbr" \
	"$(INTDIR)\PosDiagram.sbr" \
	"$(INTDIR)\PosDiaPropertySheet.sbr" \
	"$(INTDIR)\PosDiaViewer.sbr" \
	"$(INTDIR)\PosGantt.sbr" \
	"$(INTDIR)\PrintInfo.sbr" \
	"$(INTDIR)\PrivList.sbr" \
	"$(INTDIR)\ProgressDlg.sbr" \
	"$(INTDIR)\ProxyDlg.sbr" \
	"$(INTDIR)\PSGatGeometry.sbr" \
	"$(INTDIR)\PSGeometrie.sbr" \
	"$(INTDIR)\PSSearchFlightPage.sbr" \
	"$(INTDIR)\PSSortFlightPage.sbr" \
	"$(INTDIR)\PSSpecFilterPage.sbr" \
	"$(INTDIR)\PSUniFilterPage.sbr" \
	"$(INTDIR)\PSUniSortPage.sbr" \
	"$(INTDIR)\RegisterDlg.sbr" \
	"$(INTDIR)\Report10TableViewer.sbr" \
	"$(INTDIR)\Report11TableViewer.sbr" \
	"$(INTDIR)\Report12TableViewer.sbr" \
	"$(INTDIR)\Report13TableViewer.sbr" \
	"$(INTDIR)\Report14TableViewer.sbr" \
	"$(INTDIR)\Report15TableViewer.sbr" \
	"$(INTDIR)\Report16TableViewer.sbr" \
	"$(INTDIR)\Report17DailyTableViewer.sbr" \
	"$(INTDIR)\Report19DailyCcaTableViewer.sbr" \
	"$(INTDIR)\Report1TableViewer.sbr" \
	"$(INTDIR)\Report2TableViewer.sbr" \
	"$(INTDIR)\Report3TableViewer.sbr" \
	"$(INTDIR)\Report4TableViewer.sbr" \
	"$(INTDIR)\Report5TableViewer.sbr" \
	"$(INTDIR)\Report6NatCedaData.sbr" \
	"$(INTDIR)\Report6TableViewer.sbr" \
	"$(INTDIR)\Report7TableViewer.sbr" \
	"$(INTDIR)\Report8TableViewer.sbr" \
	"$(INTDIR)\Report9TableViewer.sbr" \
	"$(INTDIR)\ReportActCedaData.sbr" \
	"$(INTDIR)\ReportArrDepLoadPaxTableViewer.sbr" \
	"$(INTDIR)\ReportArrDepSelctDlg.sbr" \
	"$(INTDIR)\ReportArrDepTableViewer.sbr" \
	"$(INTDIR)\ReportBltAllocViewer.sbr" \
	"$(INTDIR)\ReportDailyFlightLogViewer.sbr" \
	"$(INTDIR)\ReportDomIntTranTableViewer.sbr" \
	"$(INTDIR)\ReportFieldDlg.sbr" \
	"$(INTDIR)\ReportFlightDelayViewer.sbr" \
	"$(INTDIR)\ReportFlightDomIntMixTableViewer.sbr" \
	"$(INTDIR)\ReportFlightGPUTableViewer.sbr" \
	"$(INTDIR)\ReportFromDlg.sbr" \
	"$(INTDIR)\ReportFromFieldDlg.sbr" \
	"$(INTDIR)\ReportOvernightTableViewer.sbr" \
	"$(INTDIR)\ReportPOPSItems.sbr" \
	"$(INTDIR)\ReportSameACTTableViewer.sbr" \
	"$(INTDIR)\ReportSameALTableViewer.sbr" \
	"$(INTDIR)\ReportSameDCDTableViewer.sbr" \
	"$(INTDIR)\ReportSameDESTableViewer.sbr" \
	"$(INTDIR)\ReportSameDTDTableViewer.sbr" \
	"$(INTDIR)\ReportSameFTYPXTableViewer.sbr" \
	"$(INTDIR)\ReportSameORGTableViewer.sbr" \
	"$(INTDIR)\ReportSameREGNTableViewer.sbr" \
	"$(INTDIR)\ReportSameTimeFrameTableViewer.sbr" \
	"$(INTDIR)\ReportSameTTYPTableViewer.sbr" \
	"$(INTDIR)\ReportSameTTYPXTableViewer.sbr" \
	"$(INTDIR)\ReportSeatDomIntMixTableViewer.sbr" \
	"$(INTDIR)\ReportSelectDlg.sbr" \
	"$(INTDIR)\ReportSeq1.sbr" \
	"$(INTDIR)\ReportSeqChoiceFieldDlg.sbr" \
	"$(INTDIR)\ReportSeqDateTimeDlg.sbr" \
	"$(INTDIR)\ReportSeqDlg.sbr" \
	"$(INTDIR)\ReportSeqField3DateTimeDlg.sbr" \
	"$(INTDIR)\ReportSeqFieldDlg.sbr" \
	"$(INTDIR)\ReportSortDlg.sbr" \
	"$(INTDIR)\ReportTableDlg.sbr" \
	"$(INTDIR)\ReportTableSpecialDlg.sbr" \
	"$(INTDIR)\ReportTimeFrameSelctDlg.sbr" \
	"$(INTDIR)\ReportXXXCedaData.sbr" \
	"$(INTDIR)\ResGroupDlg.sbr" \
	"$(INTDIR)\RotationActDlg.sbr" \
	"$(INTDIR)\RotationAltDlg.sbr" \
	"$(INTDIR)\RotationAptDlg.sbr" \
	"$(INTDIR)\RotationCedaFlightData.sbr" \
	"$(INTDIR)\RotationDlg.sbr" \
	"$(INTDIR)\RotationDlgCedaFlightData.sbr" \
	"$(INTDIR)\RotationGpuDlg.sbr" \
	"$(INTDIR)\RotationHAIDlg.sbr" \
	"$(INTDIR)\RotationISFDlg.sbr" \
	"$(INTDIR)\RotationJoinDlg.sbr" \
	"$(INTDIR)\RotationLoadDlg.sbr" \
	"$(INTDIR)\RotationLoadDlgATH.sbr" \
	"$(INTDIR)\RotationRegnDlg.sbr" \
	"$(INTDIR)\RotationTableChart.sbr" \
	"$(INTDIR)\RotationTables.sbr" \
	"$(INTDIR)\RotationTableViewer.sbr" \
	"$(INTDIR)\RotationVipDlg.sbr" \
	"$(INTDIR)\RotGDlgCedaFlightData.sbr" \
	"$(INTDIR)\RotGroundDlg.sbr" \
	"$(INTDIR)\SeasonAskDlg.sbr" \
	"$(INTDIR)\SeasonCedaFlightData.sbr" \
	"$(INTDIR)\SeasonCollectADAskDlg.sbr" \
	"$(INTDIR)\SeasonCollectAskDlg.sbr" \
	"$(INTDIR)\SeasonCollectCedaFlightData.sbr" \
	"$(INTDIR)\SeasonCollectDlg.sbr" \
	"$(INTDIR)\SeasonDlg.sbr" \
	"$(INTDIR)\SeasonDlgCedaFlightData.sbr" \
	"$(INTDIR)\SeasonFlightPropertySheet.sbr" \
	"$(INTDIR)\SeasonTableDlg.sbr" \
	"$(INTDIR)\SeasonTableViewer.sbr" \
	"$(INTDIR)\SetupDlg.sbr" \
	"$(INTDIR)\SpotAllocateDlg.sbr" \
	"$(INTDIR)\SpotAllocation.sbr" \
	"$(INTDIR)\StdAfx.sbr" \
	"$(INTDIR)\TABLE.SBR" \
	"$(INTDIR)\UFISAmSink.sbr" \
	"$(INTDIR)\UniEingabe.sbr" \
	"$(INTDIR)\Utils.sbr" \
	"$(INTDIR)\ViaTableCtrl.sbr" \
	"$(INTDIR)\VIPDlg.sbr" \
	"$(INTDIR)\WoResTableDlg.sbr" \
	"$(INTDIR)\WoResTableViewer.sbr" \
	"$(INTDIR)\WroChart.sbr" \
	"$(INTDIR)\WroDiagram.sbr" \
	"$(INTDIR)\WroDiaPropertySheet.sbr" \
	"$(INTDIR)\WroDiaViewer.sbr" \
	"$(INTDIR)\WroGantt.sbr"

"$(OUTDIR)\Fpms.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=link.exe
LINK32_FLAGS=c:\Ufis_Bin\Debug\ufis32.lib c:\Ufis_Bin\ClassLib\Debug\ccsclass.lib /nologo /subsystem:windows /incremental:yes /pdb:"$(OUTDIR)\FIPS.pdb" /debug /machine:I386 /out:"../Debug/FIPS.exe" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\AcirrDlg.obj" \
	"$(INTDIR)\AdditionalReport10TableViewer.obj" \
	"$(INTDIR)\AdditionalReport11TableViewer.obj" \
	"$(INTDIR)\AdditionalReport12TableViewer.obj" \
	"$(INTDIR)\AdditionalReport16TableViewer.obj" \
	"$(INTDIR)\AdditionalReport17TableViewer.obj" \
	"$(INTDIR)\AdditionalReport1TableViewer.obj" \
	"$(INTDIR)\AdditionalReport2TableViewer.obj" \
	"$(INTDIR)\AdditionalReport3TableViewer.obj" \
	"$(INTDIR)\AdditionalReport4TableViewer.obj" \
	"$(INTDIR)\AdditionalReport5TableViewer.obj" \
	"$(INTDIR)\AdditionalReport6TableViewer.obj" \
	"$(INTDIR)\AdditionalReport7TableViewer.obj" \
	"$(INTDIR)\AdditionalReport8TableViewer.obj" \
	"$(INTDIR)\AdditionalReport9TableViewer.obj" \
	"$(INTDIR)\AdditionalReportTableDlg.obj" \
	"$(INTDIR)\AirportInformationDlg.obj" \
	"$(INTDIR)\AllocateCca.obj" \
	"$(INTDIR)\AllocateCcaParameter.obj" \
	"$(INTDIR)\AltImportDelDlg.obj" \
	"$(INTDIR)\AskBox.obj" \
	"$(INTDIR)\AwBasicDataDlg.obj" \
	"$(INTDIR)\BackGround.obj" \
	"$(INTDIR)\BasePropertySheet.obj" \
	"$(INTDIR)\BasicData.obj" \
	"$(INTDIR)\BltCcaTableDlg.obj" \
	"$(INTDIR)\BltCcaTableViewer.obj" \
	"$(INTDIR)\BltChart.obj" \
	"$(INTDIR)\BltDiagram.obj" \
	"$(INTDIR)\BltDiaPropertySheet.obj" \
	"$(INTDIR)\BltDiaViewer.obj" \
	"$(INTDIR)\BltGantt.obj" \
	"$(INTDIR)\BltKonflikteDlg.obj" \
	"$(INTDIR)\BltOverviewTableDlg.obj" \
	"$(INTDIR)\BltOverviewTableViewer.obj" \
	"$(INTDIR)\buttonlistdlg.obj" \
	"$(INTDIR)\CcaCedaFlightData.obj" \
	"$(INTDIR)\CCAChangeTimes.obj" \
	"$(INTDIR)\CcaChart.obj" \
	"$(INTDIR)\CcaCommonDlg.obj" \
	"$(INTDIR)\CcaCommonSearchDlg.obj" \
	"$(INTDIR)\CcaCommonTableDlg.obj" \
	"$(INTDIR)\CcaCommonTableViewer.obj" \
	"$(INTDIR)\CcaDiagram.obj" \
	"$(INTDIR)\CcaDiaPropertySheet.obj" \
	"$(INTDIR)\CcaDiaViewer.obj" \
	"$(INTDIR)\CCAExpandDlg.obj" \
	"$(INTDIR)\CcaGantt.obj" \
	"$(INTDIR)\CCSGlobl.obj" \
	"$(INTDIR)\CCSPrint.obj" \
	"$(INTDIR)\CedaApxData.obj" \
	"$(INTDIR)\CedaCcaData.obj" \
	"$(INTDIR)\CedaCfgData.obj" \
	"$(INTDIR)\CedaCflData.obj" \
	"$(INTDIR)\CedaCountData.obj" \
	"$(INTDIR)\CedaDiaCcaData.obj" \
	"$(INTDIR)\CedaGpaData.obj" \
	"$(INTDIR)\CedaGrmData.obj" \
	"$(INTDIR)\CedaHaiData.obj" \
	"$(INTDIR)\CedaImpFlightData.obj" \
	"$(INTDIR)\CedaInfData.obj" \
	"$(INTDIR)\CedaInitModuData.obj" \
	"$(INTDIR)\CedaResGroupData.obj" \
	"$(INTDIR)\CicConfTableDlg.obj" \
	"$(INTDIR)\CicConfTableViewer.obj" \
	"$(INTDIR)\CicDemandTableDlg.obj" \
	"$(INTDIR)\CicDemandTableViewer.obj" \
	"$(INTDIR)\CicNoDemandTableDlg.obj" \
	"$(INTDIR)\CicNoDemandTableViewer.obj" \
	"$(INTDIR)\CollectHAIDlg.obj" \
	"$(INTDIR)\ConnectionAdvisor.obj" \
	"$(INTDIR)\CVIEWER.OBJ" \
	"$(INTDIR)\DailyCcaPrintDlg.obj" \
	"$(INTDIR)\DailyCedaFlightData.obj" \
	"$(INTDIR)\DailyPrintDlg.obj" \
	"$(INTDIR)\DailyScheduleTableDlg.obj" \
	"$(INTDIR)\DailyTableViewer.obj" \
	"$(INTDIR)\DailyVipCedaData.obj" \
	"$(INTDIR)\DataSet.obj" \
	"$(INTDIR)\DelelteCounterDlg.obj" \
	"$(INTDIR)\DGanttBarReport.obj" \
	"$(INTDIR)\DGanttChartReport.obj" \
	"$(INTDIR)\DGanttChartReportWnd.obj" \
	"$(INTDIR)\DiaCedaFlightData.obj" \
	"$(INTDIR)\FlightChart.obj" \
	"$(INTDIR)\FlightDiagram.obj" \
	"$(INTDIR)\FlightDiaPropertySheet.obj" \
	"$(INTDIR)\FlightDiaViewer.obj" \
	"$(INTDIR)\FlightGantt.obj" \
	"$(INTDIR)\FlightSearchPropertySheet.obj" \
	"$(INTDIR)\FlightSearchTableDlg.obj" \
	"$(INTDIR)\FlightSearchTableViewer.obj" \
	"$(INTDIR)\FPMS.obj" \
	"$(INTDIR)\GanttBarReport.obj" \
	"$(INTDIR)\GanttChartReport.obj" \
	"$(INTDIR)\GanttChartReportWnd.obj" \
	"$(INTDIR)\GatChart.obj" \
	"$(INTDIR)\GatDiagram.obj" \
	"$(INTDIR)\GatDiaPropertySheet.obj" \
	"$(INTDIR)\GatDiaViewer.obj" \
	"$(INTDIR)\GatGantt.obj" \
	"$(INTDIR)\HAIDlg.obj" \
	"$(INTDIR)\InitialLoadDlg.obj" \
	"$(INTDIR)\Konflikte.obj" \
	"$(INTDIR)\KonflikteDlg.obj" \
	"$(INTDIR)\List.obj" \
	"$(INTDIR)\LoginDlg.obj" \
	"$(INTDIR)\PosChart.obj" \
	"$(INTDIR)\PosDiagram.obj" \
	"$(INTDIR)\PosDiaPropertySheet.obj" \
	"$(INTDIR)\PosDiaViewer.obj" \
	"$(INTDIR)\PosGantt.obj" \
	"$(INTDIR)\PrintInfo.obj" \
	"$(INTDIR)\PrivList.obj" \
	"$(INTDIR)\ProgressDlg.obj" \
	"$(INTDIR)\ProxyDlg.obj" \
	"$(INTDIR)\PSGatGeometry.obj" \
	"$(INTDIR)\PSGeometrie.obj" \
	"$(INTDIR)\PSSearchFlightPage.obj" \
	"$(INTDIR)\PSSortFlightPage.obj" \
	"$(INTDIR)\PSSpecFilterPage.obj" \
	"$(INTDIR)\PSUniFilterPage.obj" \
	"$(INTDIR)\PSUniSortPage.obj" \
	"$(INTDIR)\RegisterDlg.obj" \
	"$(INTDIR)\Report10TableViewer.obj" \
	"$(INTDIR)\Report11TableViewer.obj" \
	"$(INTDIR)\Report12TableViewer.obj" \
	"$(INTDIR)\Report13TableViewer.obj" \
	"$(INTDIR)\Report14TableViewer.obj" \
	"$(INTDIR)\Report15TableViewer.obj" \
	"$(INTDIR)\Report16TableViewer.obj" \
	"$(INTDIR)\Report17DailyTableViewer.obj" \
	"$(INTDIR)\Report19DailyCcaTableViewer.obj" \
	"$(INTDIR)\Report1TableViewer.obj" \
	"$(INTDIR)\Report2TableViewer.obj" \
	"$(INTDIR)\Report3TableViewer.obj" \
	"$(INTDIR)\Report4TableViewer.obj" \
	"$(INTDIR)\Report5TableViewer.obj" \
	"$(INTDIR)\Report6NatCedaData.obj" \
	"$(INTDIR)\Report6TableViewer.obj" \
	"$(INTDIR)\Report7TableViewer.obj" \
	"$(INTDIR)\Report8TableViewer.obj" \
	"$(INTDIR)\Report9TableViewer.obj" \
	"$(INTDIR)\ReportActCedaData.obj" \
	"$(INTDIR)\ReportArrDepLoadPaxTableViewer.obj" \
	"$(INTDIR)\ReportArrDepSelctDlg.obj" \
	"$(INTDIR)\ReportArrDepTableViewer.obj" \
	"$(INTDIR)\ReportBltAllocViewer.obj" \
	"$(INTDIR)\ReportDailyFlightLogViewer.obj" \
	"$(INTDIR)\ReportDomIntTranTableViewer.obj" \
	"$(INTDIR)\ReportFieldDlg.obj" \
	"$(INTDIR)\ReportFlightDelayViewer.obj" \
	"$(INTDIR)\ReportFlightDomIntMixTableViewer.obj" \
	"$(INTDIR)\ReportFlightGPUTableViewer.obj" \
	"$(INTDIR)\ReportFromDlg.obj" \
	"$(INTDIR)\ReportFromFieldDlg.obj" \
	"$(INTDIR)\ReportOvernightTableViewer.obj" \
	"$(INTDIR)\ReportPOPSItems.obj" \
	"$(INTDIR)\ReportSameACTTableViewer.obj" \
	"$(INTDIR)\ReportSameALTableViewer.obj" \
	"$(INTDIR)\ReportSameDCDTableViewer.obj" \
	"$(INTDIR)\ReportSameDESTableViewer.obj" \
	"$(INTDIR)\ReportSameDTDTableViewer.obj" \
	"$(INTDIR)\ReportSameFTYPXTableViewer.obj" \
	"$(INTDIR)\ReportSameORGTableViewer.obj" \
	"$(INTDIR)\ReportSameREGNTableViewer.obj" \
	"$(INTDIR)\ReportSameTimeFrameTableViewer.obj" \
	"$(INTDIR)\ReportSameTTYPTableViewer.obj" \
	"$(INTDIR)\ReportSameTTYPXTableViewer.obj" \
	"$(INTDIR)\ReportSeatDomIntMixTableViewer.obj" \
	"$(INTDIR)\ReportSelectDlg.obj" \
	"$(INTDIR)\ReportSeq1.obj" \
	"$(INTDIR)\ReportSeqChoiceFieldDlg.obj" \
	"$(INTDIR)\ReportSeqDateTimeDlg.obj" \
	"$(INTDIR)\ReportSeqDlg.obj" \
	"$(INTDIR)\ReportSeqField3DateTimeDlg.obj" \
	"$(INTDIR)\ReportSeqFieldDlg.obj" \
	"$(INTDIR)\ReportSortDlg.obj" \
	"$(INTDIR)\ReportTableDlg.obj" \
	"$(INTDIR)\ReportTableSpecialDlg.obj" \
	"$(INTDIR)\ReportTimeFrameSelctDlg.obj" \
	"$(INTDIR)\ReportXXXCedaData.obj" \
	"$(INTDIR)\ResGroupDlg.obj" \
	"$(INTDIR)\RotationActDlg.obj" \
	"$(INTDIR)\RotationAltDlg.obj" \
	"$(INTDIR)\RotationAptDlg.obj" \
	"$(INTDIR)\RotationCedaFlightData.obj" \
	"$(INTDIR)\RotationDlg.obj" \
	"$(INTDIR)\RotationDlgCedaFlightData.obj" \
	"$(INTDIR)\RotationGpuDlg.obj" \
	"$(INTDIR)\RotationHAIDlg.obj" \
	"$(INTDIR)\RotationISFDlg.obj" \
	"$(INTDIR)\RotationJoinDlg.obj" \
	"$(INTDIR)\RotationLoadDlg.obj" \
	"$(INTDIR)\RotationLoadDlgATH.obj" \
	"$(INTDIR)\RotationRegnDlg.obj" \
	"$(INTDIR)\RotationTableChart.obj" \
	"$(INTDIR)\RotationTables.obj" \
	"$(INTDIR)\RotationTableViewer.obj" \
	"$(INTDIR)\RotationVipDlg.obj" \
	"$(INTDIR)\RotGDlgCedaFlightData.obj" \
	"$(INTDIR)\RotGroundDlg.obj" \
	"$(INTDIR)\SeasonAskDlg.obj" \
	"$(INTDIR)\SeasonCedaFlightData.obj" \
	"$(INTDIR)\SeasonCollectADAskDlg.obj" \
	"$(INTDIR)\SeasonCollectAskDlg.obj" \
	"$(INTDIR)\SeasonCollectCedaFlightData.obj" \
	"$(INTDIR)\SeasonCollectDlg.obj" \
	"$(INTDIR)\SeasonDlg.obj" \
	"$(INTDIR)\SeasonDlgCedaFlightData.obj" \
	"$(INTDIR)\SeasonFlightPropertySheet.obj" \
	"$(INTDIR)\SeasonTableDlg.obj" \
	"$(INTDIR)\SeasonTableViewer.obj" \
	"$(INTDIR)\SetupDlg.obj" \
	"$(INTDIR)\SpotAllocateDlg.obj" \
	"$(INTDIR)\SpotAllocation.obj" \
	"$(INTDIR)\StdAfx.obj" \
	"$(INTDIR)\TABLE.OBJ" \
	"$(INTDIR)\UFISAmSink.obj" \
	"$(INTDIR)\UniEingabe.obj" \
	"$(INTDIR)\Utils.obj" \
	"$(INTDIR)\ViaTableCtrl.obj" \
	"$(INTDIR)\VIPDlg.obj" \
	"$(INTDIR)\WoResTableDlg.obj" \
	"$(INTDIR)\WoResTableViewer.obj" \
	"$(INTDIR)\WroChart.obj" \
	"$(INTDIR)\WroDiagram.obj" \
	"$(INTDIR)\WroDiaPropertySheet.obj" \
	"$(INTDIR)\WroDiaViewer.obj" \
	"$(INTDIR)\WroGantt.obj" \
	"$(INTDIR)\FPMS.res"

"..\Debug\FIPS.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Fpms.dep")
!INCLUDE "Fpms.dep"
!ELSE 
!MESSAGE Warning: cannot find "Fpms.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "FPMS - Win32 Release" || "$(CFG)" == "FPMS - Win32 Debug"
SOURCE=.\AcirrDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\AcirrDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\AcirrDlg.obj"	"$(INTDIR)\AcirrDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\AdditionalReport10TableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\AdditionalReport10TableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\AdditionalReport10TableViewer.obj"	"$(INTDIR)\AdditionalReport10TableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\AdditionalReport11TableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\AdditionalReport11TableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\AdditionalReport11TableViewer.obj"	"$(INTDIR)\AdditionalReport11TableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\AdditionalReport12TableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\AdditionalReport12TableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\AdditionalReport12TableViewer.obj"	"$(INTDIR)\AdditionalReport12TableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\AdditionalReport16TableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\AdditionalReport16TableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\AdditionalReport16TableViewer.obj"	"$(INTDIR)\AdditionalReport16TableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\AdditionalReport17TableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\AdditionalReport17TableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\AdditionalReport17TableViewer.obj"	"$(INTDIR)\AdditionalReport17TableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\AdditionalReport1TableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\AdditionalReport1TableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\AdditionalReport1TableViewer.obj"	"$(INTDIR)\AdditionalReport1TableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\AdditionalReport2TableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\AdditionalReport2TableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\AdditionalReport2TableViewer.obj"	"$(INTDIR)\AdditionalReport2TableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\AdditionalReport3TableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\AdditionalReport3TableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\AdditionalReport3TableViewer.obj"	"$(INTDIR)\AdditionalReport3TableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\AdditionalReport4TableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\AdditionalReport4TableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\AdditionalReport4TableViewer.obj"	"$(INTDIR)\AdditionalReport4TableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\AdditionalReport5TableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\AdditionalReport5TableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\AdditionalReport5TableViewer.obj"	"$(INTDIR)\AdditionalReport5TableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\AdditionalReport6TableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\AdditionalReport6TableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\AdditionalReport6TableViewer.obj"	"$(INTDIR)\AdditionalReport6TableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\AdditionalReport7TableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\AdditionalReport7TableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\AdditionalReport7TableViewer.obj"	"$(INTDIR)\AdditionalReport7TableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\AdditionalReport8TableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\AdditionalReport8TableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\AdditionalReport8TableViewer.obj"	"$(INTDIR)\AdditionalReport8TableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\AdditionalReport9TableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\AdditionalReport9TableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\AdditionalReport9TableViewer.obj"	"$(INTDIR)\AdditionalReport9TableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\AdditionalReportTableDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\AdditionalReportTableDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\AdditionalReportTableDlg.obj"	"$(INTDIR)\AdditionalReportTableDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\AirportInformationDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\AirportInformationDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\AirportInformationDlg.obj"	"$(INTDIR)\AirportInformationDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\AllocateCca.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\AllocateCca.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\AllocateCca.obj"	"$(INTDIR)\AllocateCca.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\AllocateCcaParameter.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\AllocateCcaParameter.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\AllocateCcaParameter.obj"	"$(INTDIR)\AllocateCcaParameter.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\AltImportDelDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\AltImportDelDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\AltImportDelDlg.obj"	"$(INTDIR)\AltImportDelDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\AskBox.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\AskBox.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\AskBox.obj"	"$(INTDIR)\AskBox.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\AwBasicDataDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\AwBasicDataDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\AwBasicDataDlg.obj"	"$(INTDIR)\AwBasicDataDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\BackGround.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\BackGround.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\BackGround.obj"	"$(INTDIR)\BackGround.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\BasePropertySheet.CPP

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\BasePropertySheet.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\BasePropertySheet.obj"	"$(INTDIR)\BasePropertySheet.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\BasicData.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\BasicData.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\BasicData.obj"	"$(INTDIR)\BasicData.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\BltCcaTableDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\BltCcaTableDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\BltCcaTableDlg.obj"	"$(INTDIR)\BltCcaTableDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\BltCcaTableViewer.Cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\BltCcaTableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\BltCcaTableViewer.obj"	"$(INTDIR)\BltCcaTableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\BltChart.CPP

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\BltChart.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\BltChart.obj"	"$(INTDIR)\BltChart.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\BltDiagram.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\BltDiagram.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\BltDiagram.obj"	"$(INTDIR)\BltDiagram.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\BltDiaPropertySheet.CPP

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\BltDiaPropertySheet.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\BltDiaPropertySheet.obj"	"$(INTDIR)\BltDiaPropertySheet.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\BltDiaViewer.CPP

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\BltDiaViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\BltDiaViewer.obj"	"$(INTDIR)\BltDiaViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\BltGantt.CPP

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\BltGantt.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\BltGantt.obj"	"$(INTDIR)\BltGantt.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\BltKonflikteDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\BltKonflikteDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\BltKonflikteDlg.obj"	"$(INTDIR)\BltKonflikteDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\BltOverviewTableDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\BltOverviewTableDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\BltOverviewTableDlg.obj"	"$(INTDIR)\BltOverviewTableDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\BltOverviewTableViewer.Cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\BltOverviewTableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\BltOverviewTableViewer.obj"	"$(INTDIR)\BltOverviewTableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\buttonlistdlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\buttonlistdlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\buttonlistdlg.obj"	"$(INTDIR)\buttonlistdlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\CcaCedaFlightData.Cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\CcaCedaFlightData.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\CcaCedaFlightData.obj"	"$(INTDIR)\CcaCedaFlightData.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\CCAChangeTimes.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\CCAChangeTimes.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\CCAChangeTimes.obj"	"$(INTDIR)\CCAChangeTimes.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\CcaChart.CPP

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\CcaChart.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\CcaChart.obj"	"$(INTDIR)\CcaChart.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\CcaCommonDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\CcaCommonDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\CcaCommonDlg.obj"	"$(INTDIR)\CcaCommonDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\CcaCommonSearchDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\CcaCommonSearchDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\CcaCommonSearchDlg.obj"	"$(INTDIR)\CcaCommonSearchDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\CcaCommonTableDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\CcaCommonTableDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\CcaCommonTableDlg.obj"	"$(INTDIR)\CcaCommonTableDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\CcaCommonTableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\CcaCommonTableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\CcaCommonTableViewer.obj"	"$(INTDIR)\CcaCommonTableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\CcaDiagram.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\CcaDiagram.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\CcaDiagram.obj"	"$(INTDIR)\CcaDiagram.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\CcaDiaPropertySheet.CPP

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\CcaDiaPropertySheet.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\CcaDiaPropertySheet.obj"	"$(INTDIR)\CcaDiaPropertySheet.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\CcaDiaViewer.CPP

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\CcaDiaViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\CcaDiaViewer.obj"	"$(INTDIR)\CcaDiaViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\CCAExpandDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\CCAExpandDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\CCAExpandDlg.obj"	"$(INTDIR)\CCAExpandDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\CcaGantt.CPP

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\CcaGantt.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\CcaGantt.obj"	"$(INTDIR)\CcaGantt.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\CCSGlobl.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\CCSGlobl.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\CCSGlobl.obj"	"$(INTDIR)\CCSGlobl.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\CCSPrint.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\CCSPrint.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\CCSPrint.obj"	"$(INTDIR)\CCSPrint.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\CedaApxData.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\CedaApxData.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\CedaApxData.obj"	"$(INTDIR)\CedaApxData.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\CedaCcaData.Cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\CedaCcaData.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\CedaCcaData.obj"	"$(INTDIR)\CedaCcaData.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\CedaCfgData.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\CedaCfgData.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\CedaCfgData.obj"	"$(INTDIR)\CedaCfgData.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\CedaCflData.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\CedaCflData.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\CedaCflData.obj"	"$(INTDIR)\CedaCflData.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\CedaCountData.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\CedaCountData.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\CedaCountData.obj"	"$(INTDIR)\CedaCountData.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\CedaDiaCcaData.Cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\CedaDiaCcaData.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\CedaDiaCcaData.obj"	"$(INTDIR)\CedaDiaCcaData.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\CedaGpaData.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\CedaGpaData.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\CedaGpaData.obj"	"$(INTDIR)\CedaGpaData.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\CedaGrmData.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\CedaGrmData.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\CedaGrmData.obj"	"$(INTDIR)\CedaGrmData.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\CedaHaiData.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\CedaHaiData.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\CedaHaiData.obj"	"$(INTDIR)\CedaHaiData.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\CedaImpFlightData.Cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\CedaImpFlightData.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\CedaImpFlightData.obj"	"$(INTDIR)\CedaImpFlightData.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\CedaInfData.Cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\CedaInfData.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\CedaInfData.obj"	"$(INTDIR)\CedaInfData.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\CedaInitModuData.Cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\CedaInitModuData.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\CedaInitModuData.obj"	"$(INTDIR)\CedaInitModuData.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\CedaResGroupData.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\CedaResGroupData.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\CedaResGroupData.obj"	"$(INTDIR)\CedaResGroupData.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\CicConfTableDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\CicConfTableDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\CicConfTableDlg.obj"	"$(INTDIR)\CicConfTableDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\CicConfTableViewer.Cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\CicConfTableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\CicConfTableViewer.obj"	"$(INTDIR)\CicConfTableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\CicDemandTableDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\CicDemandTableDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\CicDemandTableDlg.obj"	"$(INTDIR)\CicDemandTableDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\CicDemandTableViewer.Cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\CicDemandTableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\CicDemandTableViewer.obj"	"$(INTDIR)\CicDemandTableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\CicNoDemandTableDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\CicNoDemandTableDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\CicNoDemandTableDlg.obj"	"$(INTDIR)\CicNoDemandTableDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\CicNoDemandTableViewer.Cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\CicNoDemandTableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\CicNoDemandTableViewer.obj"	"$(INTDIR)\CicNoDemandTableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\CollectHAIDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\CollectHAIDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\CollectHAIDlg.obj"	"$(INTDIR)\CollectHAIDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ConnectionAdvisor.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ConnectionAdvisor.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ConnectionAdvisor.obj"	"$(INTDIR)\ConnectionAdvisor.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\CVIEWER.CPP

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\CVIEWER.OBJ" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\CVIEWER.OBJ"	"$(INTDIR)\CVIEWER.SBR" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\DailyCcaPrintDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\DailyCcaPrintDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\DailyCcaPrintDlg.obj"	"$(INTDIR)\DailyCcaPrintDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\DailyCedaFlightData.Cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\DailyCedaFlightData.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\DailyCedaFlightData.obj"	"$(INTDIR)\DailyCedaFlightData.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\DailyPrintDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\DailyPrintDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\DailyPrintDlg.obj"	"$(INTDIR)\DailyPrintDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\DailyScheduleTableDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\DailyScheduleTableDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\DailyScheduleTableDlg.obj"	"$(INTDIR)\DailyScheduleTableDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\DailyTableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\DailyTableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\DailyTableViewer.obj"	"$(INTDIR)\DailyTableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\DailyVipCedaData.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\DailyVipCedaData.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\DailyVipCedaData.obj"	"$(INTDIR)\DailyVipCedaData.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\DataSet.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\DataSet.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\DataSet.obj"	"$(INTDIR)\DataSet.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\DelelteCounterDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\DelelteCounterDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\DelelteCounterDlg.obj"	"$(INTDIR)\DelelteCounterDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\DGanttBarReport.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\DGanttBarReport.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\DGanttBarReport.obj"	"$(INTDIR)\DGanttBarReport.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\DGanttChartReport.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\DGanttChartReport.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\DGanttChartReport.obj"	"$(INTDIR)\DGanttChartReport.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\DGanttChartReportWnd.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\DGanttChartReportWnd.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\DGanttChartReportWnd.obj"	"$(INTDIR)\DGanttChartReportWnd.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\DiaCedaFlightData.Cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\DiaCedaFlightData.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\DiaCedaFlightData.obj"	"$(INTDIR)\DiaCedaFlightData.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\FlightChart.CPP

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\FlightChart.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\FlightChart.obj"	"$(INTDIR)\FlightChart.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\FlightDiagram.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\FlightDiagram.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\FlightDiagram.obj"	"$(INTDIR)\FlightDiagram.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\FlightDiaPropertySheet.CPP

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\FlightDiaPropertySheet.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\FlightDiaPropertySheet.obj"	"$(INTDIR)\FlightDiaPropertySheet.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\FlightDiaViewer.CPP

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\FlightDiaViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\FlightDiaViewer.obj"	"$(INTDIR)\FlightDiaViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\FlightGantt.CPP

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\FlightGantt.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\FlightGantt.obj"	"$(INTDIR)\FlightGantt.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\FlightSearchPropertySheet.CPP

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\FlightSearchPropertySheet.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\FlightSearchPropertySheet.obj"	"$(INTDIR)\FlightSearchPropertySheet.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\FlightSearchTableDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\FlightSearchTableDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\FlightSearchTableDlg.obj"	"$(INTDIR)\FlightSearchTableDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\FlightSearchTableViewer.Cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\FlightSearchTableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\FlightSearchTableViewer.obj"	"$(INTDIR)\FlightSearchTableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\FPMS.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\FPMS.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\FPMS.obj"	"$(INTDIR)\FPMS.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\GanttBarReport.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\GanttBarReport.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\GanttBarReport.obj"	"$(INTDIR)\GanttBarReport.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\GanttChartReport.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\GanttChartReport.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\GanttChartReport.obj"	"$(INTDIR)\GanttChartReport.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\GanttChartReportWnd.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\GanttChartReportWnd.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\GanttChartReportWnd.obj"	"$(INTDIR)\GanttChartReportWnd.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\GatChart.CPP

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\GatChart.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\GatChart.obj"	"$(INTDIR)\GatChart.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\GatDiagram.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\GatDiagram.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\GatDiagram.obj"	"$(INTDIR)\GatDiagram.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\GatDiaPropertySheet.CPP

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\GatDiaPropertySheet.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\GatDiaPropertySheet.obj"	"$(INTDIR)\GatDiaPropertySheet.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\GatDiaViewer.CPP

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\GatDiaViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\GatDiaViewer.obj"	"$(INTDIR)\GatDiaViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\GatGantt.CPP

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\GatGantt.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\GatGantt.obj"	"$(INTDIR)\GatGantt.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\HAIDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\HAIDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\HAIDlg.obj"	"$(INTDIR)\HAIDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\InitialLoadDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\InitialLoadDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\InitialLoadDlg.obj"	"$(INTDIR)\InitialLoadDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\Konflikte.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\Konflikte.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\Konflikte.obj"	"$(INTDIR)\Konflikte.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\KonflikteDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\KonflikteDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\KonflikteDlg.obj"	"$(INTDIR)\KonflikteDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\List.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\List.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\List.obj"	"$(INTDIR)\List.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\LoginDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\LoginDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\LoginDlg.obj"	"$(INTDIR)\LoginDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\PosChart.CPP

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\PosChart.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\PosChart.obj"	"$(INTDIR)\PosChart.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\PosDiagram.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\PosDiagram.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\PosDiagram.obj"	"$(INTDIR)\PosDiagram.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\PosDiaPropertySheet.CPP

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\PosDiaPropertySheet.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\PosDiaPropertySheet.obj"	"$(INTDIR)\PosDiaPropertySheet.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\PosDiaViewer.CPP

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\PosDiaViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\PosDiaViewer.obj"	"$(INTDIR)\PosDiaViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\PosGantt.CPP

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\PosGantt.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\PosGantt.obj"	"$(INTDIR)\PosGantt.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\PrintInfo.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\PrintInfo.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\PrintInfo.obj"	"$(INTDIR)\PrintInfo.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\PrivList.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\PrivList.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\PrivList.obj"	"$(INTDIR)\PrivList.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ProgressDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ProgressDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ProgressDlg.obj"	"$(INTDIR)\ProgressDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ProxyDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ProxyDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ProxyDlg.obj"	"$(INTDIR)\ProxyDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\PSGatGeometry.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\PSGatGeometry.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\PSGatGeometry.obj"	"$(INTDIR)\PSGatGeometry.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\PSGeometrie.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\PSGeometrie.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\PSGeometrie.obj"	"$(INTDIR)\PSGeometrie.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\PSSearchFlightPage.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\PSSearchFlightPage.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\PSSearchFlightPage.obj"	"$(INTDIR)\PSSearchFlightPage.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\PSSortFlightPage.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\PSSortFlightPage.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\PSSortFlightPage.obj"	"$(INTDIR)\PSSortFlightPage.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\PSSpecFilterPage.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\PSSpecFilterPage.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\PSSpecFilterPage.obj"	"$(INTDIR)\PSSpecFilterPage.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\PSUniFilterPage.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\PSUniFilterPage.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\PSUniFilterPage.obj"	"$(INTDIR)\PSUniFilterPage.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\PSUniSortPage.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\PSUniSortPage.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\PSUniSortPage.obj"	"$(INTDIR)\PSUniSortPage.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\RegisterDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\RegisterDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\RegisterDlg.obj"	"$(INTDIR)\RegisterDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\Report10TableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\Report10TableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\Report10TableViewer.obj"	"$(INTDIR)\Report10TableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\Report11TableViewer.Cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\Report11TableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\Report11TableViewer.obj"	"$(INTDIR)\Report11TableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\Report12TableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\Report12TableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\Report12TableViewer.obj"	"$(INTDIR)\Report12TableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\Report13TableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\Report13TableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\Report13TableViewer.obj"	"$(INTDIR)\Report13TableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\Report14TableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\Report14TableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\Report14TableViewer.obj"	"$(INTDIR)\Report14TableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\Report15TableViewer.Cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\Report15TableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\Report15TableViewer.obj"	"$(INTDIR)\Report15TableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\Report16TableViewer.Cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\Report16TableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\Report16TableViewer.obj"	"$(INTDIR)\Report16TableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\Report17DailyTableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\Report17DailyTableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\Report17DailyTableViewer.obj"	"$(INTDIR)\Report17DailyTableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\Report19DailyCcaTableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\Report19DailyCcaTableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\Report19DailyCcaTableViewer.obj"	"$(INTDIR)\Report19DailyCcaTableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\Report1TableViewer.Cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\Report1TableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\Report1TableViewer.obj"	"$(INTDIR)\Report1TableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\Report2TableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\Report2TableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\Report2TableViewer.obj"	"$(INTDIR)\Report2TableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\Report3TableViewer.Cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\Report3TableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\Report3TableViewer.obj"	"$(INTDIR)\Report3TableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\Report4TableViewer.Cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\Report4TableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\Report4TableViewer.obj"	"$(INTDIR)\Report4TableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\Report5TableViewer.Cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\Report5TableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\Report5TableViewer.obj"	"$(INTDIR)\Report5TableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\Report6NatCedaData.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\Report6NatCedaData.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\Report6NatCedaData.obj"	"$(INTDIR)\Report6NatCedaData.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\Report6TableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\Report6TableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\Report6TableViewer.obj"	"$(INTDIR)\Report6TableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\Report7TableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\Report7TableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\Report7TableViewer.obj"	"$(INTDIR)\Report7TableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\Report8TableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\Report8TableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\Report8TableViewer.obj"	"$(INTDIR)\Report8TableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\Report9TableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\Report9TableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\Report9TableViewer.obj"	"$(INTDIR)\Report9TableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportActCedaData.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportActCedaData.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportActCedaData.obj"	"$(INTDIR)\ReportActCedaData.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportArrDepLoadPaxTableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportArrDepLoadPaxTableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportArrDepLoadPaxTableViewer.obj"	"$(INTDIR)\ReportArrDepLoadPaxTableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportArrDepSelctDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportArrDepSelctDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportArrDepSelctDlg.obj"	"$(INTDIR)\ReportArrDepSelctDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportArrDepTableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportArrDepTableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportArrDepTableViewer.obj"	"$(INTDIR)\ReportArrDepTableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportBltAllocViewer.Cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportBltAllocViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportBltAllocViewer.obj"	"$(INTDIR)\ReportBltAllocViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportDailyFlightLogViewer.Cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportDailyFlightLogViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportDailyFlightLogViewer.obj"	"$(INTDIR)\ReportDailyFlightLogViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportDomIntTranTableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportDomIntTranTableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportDomIntTranTableViewer.obj"	"$(INTDIR)\ReportDomIntTranTableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportFieldDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportFieldDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportFieldDlg.obj"	"$(INTDIR)\ReportFieldDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportFlightDelayViewer.Cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportFlightDelayViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportFlightDelayViewer.obj"	"$(INTDIR)\ReportFlightDelayViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportFlightDomIntMixTableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportFlightDomIntMixTableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportFlightDomIntMixTableViewer.obj"	"$(INTDIR)\ReportFlightDomIntMixTableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportFlightGPUTableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportFlightGPUTableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportFlightGPUTableViewer.obj"	"$(INTDIR)\ReportFlightGPUTableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportFromDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportFromDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportFromDlg.obj"	"$(INTDIR)\ReportFromDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportFromFieldDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportFromFieldDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportFromFieldDlg.obj"	"$(INTDIR)\ReportFromFieldDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportOvernightTableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportOvernightTableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportOvernightTableViewer.obj"	"$(INTDIR)\ReportOvernightTableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportPOPSItems.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportPOPSItems.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportPOPSItems.obj"	"$(INTDIR)\ReportPOPSItems.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportSameACTTableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportSameACTTableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportSameACTTableViewer.obj"	"$(INTDIR)\ReportSameACTTableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportSameALTableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportSameALTableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportSameALTableViewer.obj"	"$(INTDIR)\ReportSameALTableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportSameDCDTableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportSameDCDTableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportSameDCDTableViewer.obj"	"$(INTDIR)\ReportSameDCDTableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportSameDESTableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportSameDESTableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportSameDESTableViewer.obj"	"$(INTDIR)\ReportSameDESTableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportSameDTDTableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportSameDTDTableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportSameDTDTableViewer.obj"	"$(INTDIR)\ReportSameDTDTableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportSameFTYPXTableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportSameFTYPXTableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportSameFTYPXTableViewer.obj"	"$(INTDIR)\ReportSameFTYPXTableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportSameORGTableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportSameORGTableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportSameORGTableViewer.obj"	"$(INTDIR)\ReportSameORGTableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportSameREGNTableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportSameREGNTableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportSameREGNTableViewer.obj"	"$(INTDIR)\ReportSameREGNTableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportSameTimeFrameTableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportSameTimeFrameTableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportSameTimeFrameTableViewer.obj"	"$(INTDIR)\ReportSameTimeFrameTableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportSameTTYPTableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportSameTTYPTableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportSameTTYPTableViewer.obj"	"$(INTDIR)\ReportSameTTYPTableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportSameTTYPXTableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportSameTTYPXTableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportSameTTYPXTableViewer.obj"	"$(INTDIR)\ReportSameTTYPXTableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportSeatDomIntMixTableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportSeatDomIntMixTableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportSeatDomIntMixTableViewer.obj"	"$(INTDIR)\ReportSeatDomIntMixTableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportSelectDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportSelectDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportSelectDlg.obj"	"$(INTDIR)\ReportSelectDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportSeq1.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportSeq1.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportSeq1.obj"	"$(INTDIR)\ReportSeq1.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportSeqChoiceFieldDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportSeqChoiceFieldDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportSeqChoiceFieldDlg.obj"	"$(INTDIR)\ReportSeqChoiceFieldDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportSeqDateTimeDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportSeqDateTimeDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportSeqDateTimeDlg.obj"	"$(INTDIR)\ReportSeqDateTimeDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportSeqDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportSeqDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportSeqDlg.obj"	"$(INTDIR)\ReportSeqDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportSeqField3DateTimeDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportSeqField3DateTimeDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportSeqField3DateTimeDlg.obj"	"$(INTDIR)\ReportSeqField3DateTimeDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportSeqFieldDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportSeqFieldDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportSeqFieldDlg.obj"	"$(INTDIR)\ReportSeqFieldDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportSortDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportSortDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportSortDlg.obj"	"$(INTDIR)\ReportSortDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportTableDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportTableDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportTableDlg.obj"	"$(INTDIR)\ReportTableDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportTableSpecialDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportTableSpecialDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportTableSpecialDlg.obj"	"$(INTDIR)\ReportTableSpecialDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportTimeFrameSelctDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportTimeFrameSelctDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportTimeFrameSelctDlg.obj"	"$(INTDIR)\ReportTimeFrameSelctDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReportXXXCedaData.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ReportXXXCedaData.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ReportXXXCedaData.obj"	"$(INTDIR)\ReportXXXCedaData.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ResGroupDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ResGroupDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ResGroupDlg.obj"	"$(INTDIR)\ResGroupDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\RotationActDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\RotationActDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\RotationActDlg.obj"	"$(INTDIR)\RotationActDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\RotationAltDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\RotationAltDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\RotationAltDlg.obj"	"$(INTDIR)\RotationAltDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\RotationAptDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\RotationAptDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\RotationAptDlg.obj"	"$(INTDIR)\RotationAptDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\RotationCedaFlightData.Cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\RotationCedaFlightData.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\RotationCedaFlightData.obj"	"$(INTDIR)\RotationCedaFlightData.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\RotationDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\RotationDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\RotationDlg.obj"	"$(INTDIR)\RotationDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\RotationDlgCedaFlightData.Cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\RotationDlgCedaFlightData.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\RotationDlgCedaFlightData.obj"	"$(INTDIR)\RotationDlgCedaFlightData.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\RotationGpuDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\RotationGpuDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\RotationGpuDlg.obj"	"$(INTDIR)\RotationGpuDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\RotationHAIDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\RotationHAIDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\RotationHAIDlg.obj"	"$(INTDIR)\RotationHAIDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\RotationISFDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\RotationISFDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\RotationISFDlg.obj"	"$(INTDIR)\RotationISFDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\RotationJoinDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\RotationJoinDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\RotationJoinDlg.obj"	"$(INTDIR)\RotationJoinDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\RotationLoadDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\RotationLoadDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\RotationLoadDlg.obj"	"$(INTDIR)\RotationLoadDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\RotationLoadDlgATH.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\RotationLoadDlgATH.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\RotationLoadDlgATH.obj"	"$(INTDIR)\RotationLoadDlgATH.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\RotationRegnDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\RotationRegnDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\RotationRegnDlg.obj"	"$(INTDIR)\RotationRegnDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\RotationTableChart.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\RotationTableChart.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\RotationTableChart.obj"	"$(INTDIR)\RotationTableChart.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\RotationTables.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\RotationTables.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\RotationTables.obj"	"$(INTDIR)\RotationTables.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\RotationTableViewer.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\RotationTableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\RotationTableViewer.obj"	"$(INTDIR)\RotationTableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\RotationVipDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\RotationVipDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\RotationVipDlg.obj"	"$(INTDIR)\RotationVipDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\RotGDlgCedaFlightData.Cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\RotGDlgCedaFlightData.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\RotGDlgCedaFlightData.obj"	"$(INTDIR)\RotGDlgCedaFlightData.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\RotGroundDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\RotGroundDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\RotGroundDlg.obj"	"$(INTDIR)\RotGroundDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\SeasonAskDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\SeasonAskDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\SeasonAskDlg.obj"	"$(INTDIR)\SeasonAskDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\SeasonCedaFlightData.Cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\SeasonCedaFlightData.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\SeasonCedaFlightData.obj"	"$(INTDIR)\SeasonCedaFlightData.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\SeasonCollectADAskDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\SeasonCollectADAskDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\SeasonCollectADAskDlg.obj"	"$(INTDIR)\SeasonCollectADAskDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\SeasonCollectAskDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\SeasonCollectAskDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\SeasonCollectAskDlg.obj"	"$(INTDIR)\SeasonCollectAskDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\SeasonCollectCedaFlightData.Cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\SeasonCollectCedaFlightData.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\SeasonCollectCedaFlightData.obj"	"$(INTDIR)\SeasonCollectCedaFlightData.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\SeasonCollectDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\SeasonCollectDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\SeasonCollectDlg.obj"	"$(INTDIR)\SeasonCollectDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\SeasonDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\SeasonDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\SeasonDlg.obj"	"$(INTDIR)\SeasonDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\SeasonDlgCedaFlightData.Cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\SeasonDlgCedaFlightData.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\SeasonDlgCedaFlightData.obj"	"$(INTDIR)\SeasonDlgCedaFlightData.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\SeasonFlightPropertySheet.CPP

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\SeasonFlightPropertySheet.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\SeasonFlightPropertySheet.obj"	"$(INTDIR)\SeasonFlightPropertySheet.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\SeasonTableDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\SeasonTableDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\SeasonTableDlg.obj"	"$(INTDIR)\SeasonTableDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\SeasonTableViewer.Cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\SeasonTableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\SeasonTableViewer.obj"	"$(INTDIR)\SeasonTableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\SetupDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\SetupDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\SetupDlg.obj"	"$(INTDIR)\SetupDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\SpotAllocateDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\SpotAllocateDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\SpotAllocateDlg.obj"	"$(INTDIR)\SpotAllocateDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\SpotAllocation.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\SpotAllocation.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\SpotAllocation.obj"	"$(INTDIR)\SpotAllocation.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\StdAfx.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"

CPP_SWITCHES=/nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

"$(INTDIR)\StdAfx.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"

CPP_SWITCHES=/nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /FR"$(INTDIR)\\" /Fp"$(INTDIR)\Fpms.pch" /Yc"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

"$(INTDIR)\StdAfx.obj"	"$(INTDIR)\StdAfx.sbr"	"$(INTDIR)\Fpms.pch" : $(SOURCE) "$(INTDIR)"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ENDIF 

SOURCE=.\TABLE.CPP

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\TABLE.OBJ" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\TABLE.OBJ"	"$(INTDIR)\TABLE.SBR" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\UFISAmSink.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\UFISAmSink.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\UFISAmSink.obj"	"$(INTDIR)\UFISAmSink.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\UniEingabe.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\UniEingabe.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\UniEingabe.obj"	"$(INTDIR)\UniEingabe.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\Utils.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\Utils.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\Utils.obj"	"$(INTDIR)\Utils.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\ViaTableCtrl.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\ViaTableCtrl.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\ViaTableCtrl.obj"	"$(INTDIR)\ViaTableCtrl.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\VIPDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\VIPDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\VIPDlg.obj"	"$(INTDIR)\VIPDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\WoResTableDlg.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\WoResTableDlg.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\WoResTableDlg.obj"	"$(INTDIR)\WoResTableDlg.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\WoResTableViewer.Cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\WoResTableViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\WoResTableViewer.obj"	"$(INTDIR)\WoResTableViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\WroChart.CPP

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\WroChart.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\WroChart.obj"	"$(INTDIR)\WroChart.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\WroDiagram.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\WroDiagram.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\WroDiagram.obj"	"$(INTDIR)\WroDiagram.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\WroDiaPropertySheet.CPP

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\WroDiaPropertySheet.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\WroDiaPropertySheet.obj"	"$(INTDIR)\WroDiaPropertySheet.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\WroDiaViewer.CPP

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\WroDiaViewer.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\WroDiaViewer.obj"	"$(INTDIR)\WroDiaViewer.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\WroGantt.CPP

!IF  "$(CFG)" == "FPMS - Win32 Release"


"$(INTDIR)\WroGantt.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"


"$(INTDIR)\WroGantt.obj"	"$(INTDIR)\WroGantt.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\FPMS.rc

"$(INTDIR)\FPMS.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) $(RSC_PROJ) $(SOURCE)



!ENDIF 

