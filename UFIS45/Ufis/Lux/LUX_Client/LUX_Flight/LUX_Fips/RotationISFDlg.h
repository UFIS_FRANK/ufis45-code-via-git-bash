#if !defined(AFX_ROTATIONISFDLG_H__F7B49FB1_6EE9_11D1_8332_0080AD1DC701__INCLUDED_)
#define AFX_ROTATIONISFDLG_H__F7B49FB1_6EE9_11D1_8332_0080AD1DC701__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// RotationISFDlg.h : header file
//

#include <CCSEdit.h> 
#include <resource.h>

/////////////////////////////////////////////////////////////////////////////
// RotationISFDlg dialog

class RotationISFDlg : public CDialog
{
// Construction
public:
	RotationISFDlg(CWnd* pParent = NULL);   // standard constructor

	~RotationISFDlg();

	void AppExit();

	CString omOrgDes4;
	CString omOrgDes3;
	bool bmCsgnUser;

// Dialog Data
	//{{AFX_DATA(RotationISFDlg)
	enum { IDD = IDD_ROTATION_ISF };
	CCSEdit	m_CE_Csgn;
	CCSEdit	m_CE_Ttyp;
	CCSEdit	m_CE_StoDate;
	CCSEdit	m_CE_Act3;
	CButton	m_CR_OrgOrDes;
	CButton	m_CR_OrgOrDes2;
	CCSEdit	m_CE_Fltn;
	CCSEdit	m_CE_Flns;
	CCSEdit	m_CE_Alc;
	CCSEdit	m_CE_Sto;
	CCSEdit	m_CE_Regn;
	CCSEdit	m_CE_OrgDes;
	CCSEdit	m_CE_Act5;
	CCSEdit m_CE_Stev;
	CCSEdit	m_CE_Pos;
	CString m_Stev;
	CString	m_Act5;
	CString	m_OrgDes;
	CString	m_Regn;
	CString	m_Sto;
	CString	m_Alc;
	CString	m_Flns;
	CString	m_Fltn;
	CString	m_Act3;
	CString	m_StoDate;
	CString	m_Ttyp;
	CButton	m_CR_VFR;
	CButton	m_CR_IFR;
	CString	m_Pos;
	CString	m_Csgn;
	BOOL	m_check1;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(RotationISFDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL DestroyWindow();
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(RotationISFDlg)
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnAlc3list();
	afx_msg void OnAct35list();
	afx_msg void OnOrgdeslist();
	afx_msg LONG OnEditKillfocus( UINT wParam, LPARAM lParam);
	afx_msg void OnPosList();
	afx_msg LONG OnEditChanged( UINT wParam, LPARAM lParam);
	afx_msg void OnTtyplist();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ROTATIONISFDLG_H__F7B49FB1_6EE9_11D1_8332_0080AD1DC701__INCLUDED_)
