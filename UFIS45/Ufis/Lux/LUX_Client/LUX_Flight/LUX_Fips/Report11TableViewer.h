// Arbeitsflugplan
#ifndef __ARBEITSFLUGPLANTABLEVIEWER_H__
#define __ARBEITSFLUGPLANTABLEVIEWER_H__

#include <stdafx.h>
#include <RotationDlgCedaFlightData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>
#include <CedaCcaData.h>


struct ARBEITSFLUGPLANTABLE_LINEDATA
{
	int			Cntr;
	CString		DRem1;
	long AUrno;
	long ARkey;
	CString		AFlno;
	CString		ATtyp;
	CString		AOrg3;
	CString		AVia3;
	CString		AAct3;
	CString		AGta1;
	CString		APsta;
	CTime		AStoa;
	CString		ARwya;
	CString		ABlt1;
	CString		AExt1;

	long DUrno;
	long DRkey;
	CString		DFlno;
	CString		DTtyp;
	CString		DDes3;
	CString		DVia3;
	CString		DAct3;
	CString		DGtd1;
	CString		DGtd2;
	CString		DPstd;
	CTime		DStod;

	CString		DRwyd;
	CString		DRegn;
	CString		DWro1;
	CTime		DAirb;
	CTime		DLand;
	CString		DCkic1;// Checkin-Schalter
	CString		DCkic2;// Checkin-Schalter

};

/////////////////////////////////////////////////////////////////////////////
// ArbeitsflugplanTableViewer

class ArbeitsflugplanTableViewer : public CViewer
{
// Constructions
public:
    ArbeitsflugplanTableViewer(CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData, char *pcpInfo = NULL);
    ~ArbeitsflugplanTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
    void MakeLines();
	int  MakeLine(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight);
	void MakeLineData(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight, ARBEITSFLUGPLANTABLE_LINEDATA &rpLine);
	void MakeColList(ARBEITSFLUGPLANTABLE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList, int iCntr);
	int  CompareArbeitsflugplan(ARBEITSFLUGPLANTABLE_LINEDATA *prpArbeitsflugplan1, ARBEITSFLUGPLANTABLE_LINEDATA *prpArbeitsflugplan2);
	int  CompareFlight(ARBEITSFLUGPLANTABLE_LINEDATA *prpFlight1, ARBEITSFLUGPLANTABLE_LINEDATA *prpFlight2);
	bool PrintFrameBottom(void);
	int  GetVia(CCSPtrArray<VIADATA> *opVias, ROTATIONDLGFLIGHTDATA *prpFlight);
// Operations
public:
	void DeleteAll(void);
	int CreateLine(ARBEITSFLUGPLANTABLE_LINEDATA &prpArbeitsflugplan);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay(void);
	void DrawHeader(void);
	bool PrintPlanToFile(char *pcpDefPath);
	void PrintPlanToTxt(char *pcpDefPath);


// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomTable;
	CCSPtrArray<ROTATIONDLGFLIGHTDATA> *pomData;
	CCSPtrArray<CCADATA> omDCins;
	CedaCcaData omCcaData;

public:
    CCSPtrArray<ARBEITSFLUGPLANTABLE_LINEDATA> omLines;
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void GetHeader(void); 
	void PrintTableView(void);
	bool PrintTableLine(ARBEITSFLUGPLANTABLE_LINEDATA *prpLine,bool bpLastLine, int ipCntr);
	bool PrintTableHeader(CTime popStoa);
	CCSPtrArray <TABLE_HEADER_COLUMN> omPrintHeadHeaderArray;
	CCSPrint *pomPrint;
	CString omTableName;
	char *pcmInfo;
	CString omFooterName;
	CString omFileName;
	CBitmap	 omBitmap;
};

#endif //__ARBEITSFLUGPLANTABLEVIEWER_H__
