// DailyPrintDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <fpms.h>
#include <ReportSeqFieldDlg.h>
#include <ReportSelectDlg.h>
#include <DailyPrintDlg.h>
#include <CCSGLOBL.H>
#include <BasicData.h>
#include <resrc1.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDailyPrintDlg 

#define _MIT_TRACE_		FALSE

CDailyPrint::CDailyPrint(CWnd* pParent, char *opSelect, int *plTyp, char *pspVerkehr, bool *pbpSeason, char *opWhere )
	: CDialog(CDailyPrint::IDD, pParent)
{

	// m_Terminal(x) : Selection der Terminals
	// m_VomTag      : Flugdatum ( 1. Tag [Vomtag-Bistag] )
	// m_VonZeit     : Flugtag (Verkehrstag) Anzeige
	// m_Airlines    : gew�nschte Fluggesellschaften
	// m_AirlineEx   : ausgeschlossene Fluggesellschaften
	// m_Verkehrsrechte : anzuzeigende VR
	
	//{{AFX_DATA_INIT(CDailyPrint)
	m_Bistag = _T("");
	m_Biszeit = _T("");
	m_Vomtag = _T("");		 
	m_Vonzeit = _T("");
	m_Terminal1 = FALSE;
	m_Terminal2 = FALSE;
	m_Airlines = _T("");
	m_Verkehrsrechte = _T("");
	m_AirlineEx = _T("");
//	m_List1 = -1;
//	m_ListAirl = TRUE;
//	m_ListAirlEx = FALSE;
	//}}AFX_DATA_INIT


	pomSelectDlg = (CReportSelectDlg*) pParent ;
	pcmSelect = opSelect;		// complete WhereClause
	pcmWhere = opWhere;			// WhereClause f�r PartialRead ( wenn != NULL )
	pmTyp = plTyp;				// Terminals
	pcmVerkehr = pspVerkehr ;	// Verkehrsrechte
	pbmSeason = pbpSeason ;		// Seasonplan ?
	omVerkehrstag = "1" ;		// Vorbelegung Montag

}


void CDailyPrint::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDailyPrint)
	DDX_Control(pDX, IDC_ST_VTAG, m_ST_VTag);
	DDX_Control(pDX, IDC_ST_TAG, m_ST_Tag);
	DDX_Control(pDX, IDC_DAILYSEASON, m_CE_ButSeason);
	DDX_Control(pDX, IDC_CHECKSEASON, m_CE_Season);
	DDX_Control(pDX, IDC_AirlineEx, m_CE_AirlineEx);
	DDX_Control(pDX, IDC_RECHTE, m_CE_Verkehrsrechte);
	DDX_Control(pDX, IDC_EC_Airlines, m_CE_Airlines);
	DDX_Control(pDX, IDC_Terminal2, m_X_Terminal2);
	DDX_Control(pDX, IDC_Terminal1, m_X_Terminal1);
	DDX_Control(pDX, IDC_VONZEIT, m_CE_Vonzeit);
	DDX_Control(pDX, IDC_VOMTAG, m_CE_Vomtag);
	DDX_Text(pDX, IDC_VOMTAG, m_Vomtag);
	DDV_MaxChars(pDX, m_Vomtag, 10);
	DDX_Text(pDX, IDC_VONZEIT, m_Vonzeit);
	DDX_Check(pDX, IDC_Terminal1, m_Terminal1);
	DDX_Check(pDX, IDC_Terminal2, m_Terminal2);
	DDX_Text(pDX, IDC_EC_Airlines, m_Airlines);
	DDV_MaxChars(pDX, m_Airlines, 19);
	DDX_Text(pDX, IDC_RECHTE, m_Verkehrsrechte);
	DDV_MaxChars(pDX, m_Verkehrsrechte, 10);
	DDX_Text(pDX, IDC_AirlineEx, m_AirlineEx);
	DDV_MaxChars(pDX, m_AirlineEx, 19);
//	DDX_Control(pDX, IDC_RADIO_SEL_AIRL, m_CR_List1);
//	DDX_Radio(pDX, IDC_RADIO_SEL_AIRL, m_ListAirl);
//	DDX_Radio(pDX, IDC_RADIO_SEL_AIRLEX, m_ListAirlEx);
	DDX_Control(pDX, IDC_RADIO_SEL_AIRL, m_X_ListAirl);
	DDX_Control(pDX, IDC_RADIO_SEL_AIRLEX, m_X_ListAirlEx);
	//}}AFX_DATA_MAP

}


BEGIN_MESSAGE_MAP(CDailyPrint, CDialog)
	//{{AFX_MSG_MAP(CDailyPrint)
	ON_MESSAGE(WM_EDIT_KILLFOCUS, OnKillfocusVomtag)
	ON_BN_CLICKED(IDC_DAILYSEASON, OnSeason)
	ON_BN_CLICKED(IDC_CHECKSEASON, OnCheckSeason)
	ON_EN_KILLFOCUS(IDC_EC_Airlines, OnKillfocusECAirlines)
	ON_BN_CLICKED(IDC_RADIO_SEL_AIRL, OnAirl)
	ON_BN_CLICKED(IDC_RADIO_SEL_AIRLEX, OnAirlEx)
	//}}AFX_MSG_MAP

END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDailyPrint 

BOOL CDailyPrint::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_CE_Vomtag.SetTypeToDate();
	m_CE_Vomtag.SetBKColor( YELLOW );
	
	// Presets entsprechend vorherigem Aufruf
//	omVerkehrstag = pomSelectDlg->omLastValues.GetSelect() ;
	omVerkehrstag = pomSelectDlg->omLastValues.GetVerkehrTag() ;
	omLastStart = pomSelectDlg->omLastValues.GetMinTime() ;
	omLastEnd = pomSelectDlg->omLastValues.GetMaxTime() ;

	if( omVerkehrstag < "1" || omVerkehrstag > "7" ) omVerkehrstag = "1" ;	

	if( (pomSelectDlg->omLastValues.GetMaxDate()).IsEmpty() )
	{
		CTime olLocalTime = CTime::GetCurrentTime() ;
		m_Vomtag = olLocalTime.Format( "%d.%m.%Y" )  ;
	}
	else
	{
		m_Vomtag = pomSelectDlg->omLastValues.GetMaxDate()  ;
	}

	m_CE_Vomtag.SetWindowText( m_Vomtag ) ;
	m_CE_Vomtag.UpdateWindow() ;

	// Hinweisfenster Vonzeit versorgen
	OnKillfocusVomtag( 0, 0 ) ;	


	// Terminal Controls vorbelegen ?
	if(  *pmTyp == X_TERM1 ) 
	{
		m_X_Terminal1.SetCheck(TRUE);
	}
	else if(  *pmTyp == X_TERM2 ) 
	{
		m_X_Terminal2.SetCheck(TRUE);
	}
	else if(  *pmTyp == X_TERM12 ) 
	{
		m_X_Terminal1.SetCheck(TRUE);
		m_X_Terminal2.SetCheck(TRUE);
	}
	else if(  *pmTyp == X_TERMNO ) 
	{
		m_X_Terminal1.SetCheck(FALSE);
		m_X_Terminal2.SetCheck(FALSE);
	}
	else  
	{
		m_X_Terminal1.SetCheck(FALSE);
		m_X_Terminal2.SetCheck(FALSE);
		*pmTyp = X_TERMDEF ;
	}

//	m_X_ListAirl.SetCheck(TRUE);
//	m_X_ListAirlEx.SetCheck(FALSE);

	if (pomSelectDlg->omLastValues.GetSelAirl())
	{
		m_X_ListAirl.SetCheck(TRUE);
		m_X_ListAirlEx.SetCheck(FALSE);
	}
	else
	{
		m_X_ListAirl.SetCheck(FALSE);
		m_X_ListAirlEx.SetCheck(TRUE);
	}

	if(m_X_ListAirl.GetCheck())
	{
		m_CE_Airlines.EnableWindow(TRUE);
		m_CE_AirlineEx.EnableWindow(FALSE);
	}
	else
	{
		m_CE_Airlines.EnableWindow(FALSE);
		m_CE_AirlineEx.EnableWindow(TRUE);
	}


	m_Airlines = pomSelectDlg->omLastValues.GetAirlines() ;
	m_CE_Airlines.SetWindowText( m_Airlines ) ;
	m_CE_Airlines.UpdateWindow() ;
	m_AirlineEx = pomSelectDlg->omLastValues.GetAirlineEx() ;
	m_CE_AirlineEx.SetWindowText( m_AirlineEx ) ;
	m_CE_AirlineEx.UpdateWindow() ;
	m_Verkehrsrechte = pomSelectDlg->omLastValues.GetVerkehr() ;
	m_CE_Verkehrsrechte.SetWindowText( m_Verkehrsrechte ) ;
	m_CE_Verkehrsrechte.UpdateWindow() ;
	
	UpdateData() ;

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}


void CDailyPrint::OnOK() 
{
	CString olErrorText;
	char pclWhere[512];
	char pslAirlines[256] ;
	char pslAirlineEx[256] ;
	char pslVerkehrstag[256] ;

	bool blNoEx = false ;
	CTime   olMinDate;
	CTime   olMaxDate;
	CString olHStr1( "00:00" ) ;
	CString olHStr2( "23:59" ) ;


	if( m_CE_Season.GetCheck() ) 
	{
		// SEASONAL
		*pbmSeason = true ;
		olMinDate = omLastStart ;
		olMinDate += CTimeSpan( 0,0,0,0 ) ;
		olMaxDate = omLastEnd ;
		olMaxDate += CTimeSpan( 0,23,59,0 ) ;
		// Verkehrstag
//		sprintf( pslVerkehrstag, " AND (DOOA='%s' OR DOOD='%s')", omVerkehrstag, omVerkehrstag );  



				int i = 0;
				bool blInRange = false;
				CTimeSpan olOneDay (1, 0, 0, 0);
				CTime olActTime = olMinDate;
				int ilFirstDay = atoi(omVerkehrstag);
				int ilNextDay = atoi(omVerkehrstag);

				while (olActTime <= olMaxDate)
				{
//					int ilActDay = olActTime.GetDayOfWeek(); don�t call MFC, there is day 1 sunday!!
					int ilActDay = GetDayOfWeek(olActTime);
					if (ilActDay == atoi(omVerkehrstag))
					{
						blInRange = true;
						break;
					}

					olActTime += olOneDay;

					if (i++ > 7)
						break;
				}

				if(blInRange)
				{
					if (bgReportLocal)
					{
						ogBasicData.LocalToUtc(olActTime);
					}

					ilFirstDay = GetDayOfWeek(olActTime);

					if (bgReportLocal)
					{
						ogBasicData.UtcToLocal(olActTime);
					}
				}



/*
		if (bgReportLocal)
		{
			ogBasicData.LocalToUtc(olMinDate);
			ogBasicData.LocalToUtc(olMaxDate);
		}
		int ilFirstDay = GetDayOfWeek(olMinDate);
		int ilNextDay  = GetDayOfWeek(olMaxDate);

		if (bgReportLocal)
		{
			ogBasicData.UtcToLocal(olMinDate);
			ogBasicData.UtcToLocal(olMaxDate);
		}

		sprintf( pslVerkehrstag, " AND ((DOOA='%d' OR DOOD='%d') OR (DOOA='%d' OR DOOD='%d'))", ilFirstDay, ilFirstDay, ilNextDay, ilNextDay );  
*/
		sprintf( pslVerkehrstag, " AND ((DOOA='%d' OR DOOD='%d') OR (DOOA='%d' OR DOOD='%d'))", ilFirstDay, ilFirstDay, ilNextDay, ilNextDay );  

	}
	else
	{
		// DAILY
		strcpy( pslVerkehrstag, "" ) ;
		if(!m_CE_Vomtag.GetStatus())
		{
			olErrorText += GetString( IDS_STRING1399 ) ;
		}

		
		if(!olErrorText.IsEmpty())	
		{

			olErrorText= GetString(IDS_STRING967) + "\n\n" + olErrorText;

//			MessageBox(olErrorText,GetString( ST_FEHLER),MB_ICONEXCLAMATION);
			MessageBox(GetString(IDS_STRING934), GetString(IMFK_REPORT), MB_OK);
			return;
		}
		
		m_CE_Vomtag.GetWindowText(m_Vomtag);

		if( m_Vomtag.IsEmpty())
		{
			// Default: heute
			CTime olTime = CTime::GetCurrentTime();
			m_Vomtag = olTime.Format( "%d.%m.%Y" );
			m_Bistag = olTime.Format( "%d.%m.%Y" );
		}
		else
		{
			m_Bistag = m_Vomtag;
		}

		// Preset festhalten
		pomSelectDlg->omLastValues.SetMaxDate( m_Vomtag ) ;

		olMinDate = DateHourStringToDate( m_Vomtag, olHStr1 );
		olMaxDate = DateHourStringToDate( m_Bistag, olHStr2 );
	}


	// CeDa Zugriff in UTC Zeit
	if( bgReportLocal ) 
	{
		ogBasicData.LocalToUtc(olMinDate);
		ogBasicData.LocalToUtc(olMaxDate);
	}

	CString olMaxStr = olMaxDate.Format( "%Y%m%d%H%M%S" );
	CString olMinStr = olMinDate.Format( "%Y%m%d%H%M%S" );

	// Selektionskriterium basteln
	if((olMaxDate != TIMENULL) && (olMinDate != TIMENULL) )
	{
		if( pcmWhere != NULL ) 
		{
			// strcpy( pcmWhere,"((STOA BETWEEN '%s' AND '%s') OR (STOD  BETWEEN '%s' AND '%s'))" ) ;
			sprintf( pcmWhere,
				"((STOA BETWEEN '%%s' AND '%%s' AND DES3='%s') OR (STOD  BETWEEN '%%s' AND '%%s' AND ORG3='%s'))",
				pcgHome, pcgHome ) ;
		}
		sprintf(pclWhere,"((STOA<='%s' AND STOA>='%s') OR (STOD<='%s' AND STOD>='%s'))",
		olMaxStr, olMinStr, olMaxStr, olMinStr);

		 
		if( m_X_Terminal1.GetCheck() )
		{
			*pmTyp = m_X_Terminal2.GetCheck() ? X_TERM12 : X_TERM1 ;
		}
	    else if( m_X_Terminal2.GetCheck() )
		{
			*pmTyp = X_TERM2;
		}
		else
		{
			*pmTyp = X_TERMNO;
		}
		
	}

	if(!olErrorText.IsEmpty())	
	{
		if( MessageBox( olErrorText,GetString( IDS_WARNING), MB_YESNO ) == IDNO )
		{ 
			return;
		}
	}


	// Verkehrsrechte an Dlg �bergeben !
	m_CE_Verkehrsrechte.GetWindowText(m_Verkehrsrechte);
	m_Verkehrsrechte.TrimLeft() ;
	m_Verkehrsrechte.MakeUpper() ;


	// Airlines
	m_CE_Airlines.GetWindowText(m_Airlines);
	m_Airlines.TrimLeft() ;
	m_Airlines.MakeUpper() ;
	strcpy( pslAirlines, "" ) ;

	if( ! m_Airlines.IsEmpty() )
	{

		if(_MIT_TRACE_) TRACE("Selected : [%s] \n", m_Airlines ) ;
		CString olSeAlc( m_Airlines ) ;
		CString olSeAlcff ;
		char pslAlSe[50] ;
		int ilRest = olSeAlc.GetLength() ;
		int ilAnzAl = 0 ;
		while( ilRest > 1  )	// min. 2 Ltr. Code
		{
			ilRest = CutLeft( olSeAlc, olSeAlcff, " ," ) ;
			if( olSeAlc.GetLength() > 1 )
			{
				if( (CString(olSeAlc).Left(3)).CompareNoCase("ALL") == 0 )
				{
					strcpy( pslAirlines, "" ) ;
					break ;
				}
				else
				{
					if( ++ilAnzAl == 1 )
					{
						sprintf( pslAlSe, " AND ((ALC2='%s' OR ALC3='%s')", olSeAlc, olSeAlc );  
						strcat( pslAirlines, pslAlSe ) ;
					}
					else
					{
						sprintf( pslAlSe, " OR (ALC2='%s' OR ALC3='%s')", olSeAlc, olSeAlc );  
						strcat( pslAirlines, pslAlSe ) ;
					}
				}
			}
			olSeAlc = olSeAlcff ;
		}
	
		if( strlen( pslAirlines ) !=0 ) strcat( pslAirlines,")" ) ;
//		blNoEx = true ;
	}


	// Ausschl��e
	strcpy( pslAirlineEx, "" ) ;
	m_CE_AirlineEx.GetWindowText(m_AirlineEx);
	m_AirlineEx.TrimLeft() ;
	m_AirlineEx.MakeUpper() ;
	if( ! blNoEx ) 
	{
		if( ! m_AirlineEx.IsEmpty() )
		{
			if(_MIT_TRACE_) TRACE("Excluded : [%s] \n", m_AirlineEx ) ;
			CString olExAlc( m_AirlineEx ) ;
			CString olExAlcff ;
			char pslAlEx[50] ;
			int ilRest = olExAlc.GetLength() ;
			while( ilRest > 1  )	// min. 2 Ltr. Code
			{
				ilRest = CutLeft( olExAlc, olExAlcff, " ," ) ;
				if( olExAlc.GetLength() > 1 )
				{
					sprintf( pslAlEx, " AND (ALC2<>'%s' AND ALC3<>'%s')", olExAlc, olExAlc );  
					strcat( pslAirlineEx, pslAlEx ) ;
				}
				olExAlc = olExAlcff ;
			}
		}
	}
	

	// Return Werte
	if(!m_X_ListAirl.GetCheck())
		strcpy( pslAirlines, "" ) ;

	if(!m_X_ListAirlEx.GetCheck())
		strcpy( pslAirlineEx, "" ) ;


	strcpy( pcmVerkehr, m_Verkehrsrechte ) ;
	strcat( pclWhere, pslVerkehrstag ) ;
	strcat( pclWhere, pslAirlines ) ;
	strcat( pclWhere, pslAirlineEx ) ;
	strcpy(pcmSelect, pclWhere);
	if( pcmWhere != NULL ) 
	{
		strcat( pcmWhere, pslVerkehrstag ) ;
		strcat( pcmWhere, pslAirlines ) ;
		strcat( pcmWhere, pslAirlineEx ) ;
	}
	// Presets festhalten
	pomSelectDlg->omLastValues.SetMaxDate( m_Vomtag ) ;
	pomSelectDlg->omLastValues.SetAirlines( m_Airlines ) ;
	pomSelectDlg->omLastValues.SetAirlineEx( m_AirlineEx ) ;
	pomSelectDlg->omLastValues.SetVerkehr( m_Verkehrsrechte ) ;
	pomSelectDlg->omLastValues.SetTerm( *pmTyp ) ;

	pomSelectDlg->omLastValues.SetMaxTime( omLastEnd ) ;
	pomSelectDlg->omLastValues.SetMinTime( omLastStart ) ;
	pomSelectDlg->omLastValues.SetSelAirl( m_X_ListAirl.GetCheck() ) ;

	CDialog::OnOK();
}


void CDailyPrint::OnKillfocusVomtag( UINT wParam, UINT lParam ) 
{

	// Infofenster VonZeit mit Auswahltag versorgen
    CString olLokdum ;

	UpdateData() ;

	if( wParam == 0 ) OnKillfocusECAirlines() ;	// bei Init

	if(	m_CE_Season.GetCheck() ) return ;	// Season aktiv

	if(!m_CE_Vomtag.GetStatus())
	{
		CString olErrorText( GetString( IDS_STRING1399 ) ) ;
		olErrorText= GetString(IDS_STRING967) + "\n\n" + olErrorText;

//		MessageBox(olErrorText,GetString( ST_FEHLER),MB_ICONEXCLAMATION);
		MessageBox(GetString(IDS_STRING934), GetString(IMFK_REPORT), MB_OK);
		return;
	}

	// Wochentag bestimmen
	CTime olLocalTime = DateStringToDate( m_Vomtag ) ;
	int ilWDay = olLocalTime.GetDayOfWeek() ;
	int ilWDStr = imMontagsID - 2 ;	// IDS_STRING1159 = Montag
	int ilSoOffset = 0 ;			// Offset auf Wochentag ( wegen SOnntag )
	if( ilWDay > 1 )
	{
		ilWDStr += ilWDay ;
		ilSoOffset = -1 ;
	}
	else
	{
		// Sonntag
		ilWDStr += ilWDay+7 ;	
		ilSoOffset = 6 ;
	}
//	if( ilWDay > 1 ) ilWDStr += ilWDay ;
//	else ilWDStr += ilWDay+7 ;	// wegen Sonntag
	m_Vonzeit = GetString( ilWDStr )+", "+olLocalTime.Format( "%d.%m.%y" ) ;
	olLokdum.Format(",  %s %d", GetString( IDS_STRING1418 ), ilWDay+ilSoOffset ) ;// ilWDay-1 ) ;
	m_Vonzeit += olLokdum ;
	
	m_CE_Vonzeit.SetWindowText( m_Vonzeit ) ;
	m_CE_Vonzeit.UpdateWindow() ;
	
}



void CDailyPrint::OnKillfocusECAirlines() 
{
	// Exclusions nur bei Nichtdefinition von Airlines !
	UpdateData() ;
	if( ! m_Airlines.IsEmpty() ) m_CE_AirlineEx.EnableWindow( false ) ;
	else m_CE_AirlineEx.EnableWindow( true ) ;
}

void CDailyPrint::OnAirl() 
{
	UpdateData() ;
	m_CE_Airlines.EnableWindow(TRUE);
	m_CE_AirlineEx.EnableWindow(FALSE);
}


void CDailyPrint::OnAirlEx() 
{
	UpdateData() ;
	m_CE_Airlines.EnableWindow(FALSE);
	m_CE_AirlineEx.EnableWindow(TRUE);
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  Saisonmethoden

void CDailyPrint::OnSeason() 
{
	// SeqDialog aufrufen
	CTime olStart( omLastStart ) ;
	CTime olEnd( omLastEnd ) ;
	char pslVT[20] ;	// Verkehrstag

	strcpy( pslVT, omVerkehrstag ) ;
	
	CReportSeqFieldDlg olReportDlg(this, GetString(IDS_STRING1411), GetString(IDS_STRING1416), &olStart, &olEnd, pslVT, "X", 0, 1);

	if(olReportDlg.DoModal() == IDOK)
	{
		// Verkehrstag
		CString olVT( pslVT ) ;
		olVT.TrimLeft();
		strcpy( pslVT, olVT );
		if( atoi(pslVT) < 1 || atoi(pslVT) > 7 )
		{
			CString olError ;
			olError.Format("Tag [%s]",pslVT) ;
			MessageBox( GetString(IDS_STRING1417), olError, MB_ICONWARNING ) ;
			omVerkehrstag = "1" ;	// Default: Montag
		}
		else
		{
			omVerkehrstag = pslVT ;
		}
		// Preset festhalten
//		pomSelectDlg->omLastValues.SetSelect( omVerkehrstag ) ;
		pomSelectDlg->omLastValues.SetVerkehrTag( omVerkehrstag ) ;

		CString omSeasonStart = olStart.Format( "%Y%m%d000000" );
		CString omSeasonEnd = olEnd.Format( "%Y%m%d235959" );

		if(bgReportLocal)
		{
			CTime olUtcMinDate( DBStringToDateTime(omSeasonStart) );
			CTime olUtcMaxDate( DBStringToDateTime(omSeasonEnd) );

			ogBasicData.LocalToUtc(olUtcMinDate);
			ogBasicData.LocalToUtc(olUtcMaxDate);

			omSeasonStart = olUtcMinDate.Format( "%Y%m%d%H%M%S" );
			omSeasonEnd = olUtcMaxDate.Format( "%Y%m%d%H%M%S" );
		}
		if( olStart == TIMENULL || olEnd == TIMENULL )
		{
			m_CE_Season.SetCheck( FALSE ) ;
			OnCheckSeason() ;
		}
		else
		{
			m_CE_Season.SetCheck( TRUE ) ;

			omLastStart = olStart ;
			omLastEnd = olEnd ;
			int ilWTText = imMontagsID - 1 + atoi( omVerkehrstag ) ;
			m_Vonzeit = GetString( ilWTText ) + olStart.Format( " : %d.%m.%Y" );
			m_Vonzeit += " - " + olEnd.Format( "%d.%m.%Y" );
			
			m_CE_Vomtag.SetReadOnly( true ) ;
			m_ST_Tag.SetWindowText( "" ) ;
			m_ST_VTag.SetWindowText( GetString( IDS_STRING1418 )) ;
			m_CE_Vomtag.SetBKColor( GRAY ) ;
			m_CE_Vonzeit.SetWindowText( m_Vonzeit ) ;
			m_CE_Vonzeit.UpdateWindow() ;
		}
	
	}
	else
	{
		m_CE_Season.SetCheck( FALSE ) ;
		OnCheckSeason() ;
		return ;
	}
	
}

void CDailyPrint::OnCheckSeason() 
{

	if( m_CE_Season.GetCheck() )
	{
		// Season bearbeiten
		OnSeason() ;
	}
	else
	{
		// zur�ck zu Daily
		m_CE_Vomtag.SetReadOnly( false ) ;
		m_ST_Tag.SetWindowText( GetString( IDS_STRING1419) ) ;
		m_ST_VTag.SetWindowText( "" ) ;
		m_CE_Vomtag.SetBKColor( YELLOW ) ;
		OnKillfocusVomtag( IDD_REPORTSEQFIELD, 0 ) ;
	}
}

