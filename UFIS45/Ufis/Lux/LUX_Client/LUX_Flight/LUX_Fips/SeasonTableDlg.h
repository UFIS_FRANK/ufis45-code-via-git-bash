#if !defined(AFX_SEASONTABLEDLG_H__C7CD8831_241F_11D1_82D3_0080AD1DC701__INCLUDED_)
#define AFX_SEASONTABLEDLG_H__C7CD8831_241F_11D1_82D3_0080AD1DC701__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// FlightTableDlg.h : header file
//
 
#include <CCSTable.h>
#include <SeasonTableViewer.h>
#include <SeasonDlg.h>
#include <CCSDragDropCtrl.h>

/////////////////////////////////////////////////////////////////////////////
// CSeasonTableDlg dialog

class CSeasonTableDlg : public CDialog
{
// Construction
public:
	CSeasonTableDlg(CWnd* pParent = NULL);   // standard constructor
	~CSeasonTableDlg();

// Dialog Data
	//{{AFX_DATA(CSeasonTableDlg)
	enum { IDD = IDD_SEASONTABLE };
	CComboBox	m_CC_Ansicht;
	CButton	m_CB_Verbinden;
	CButton	m_CB_Teilen;
	CButton	m_CB_Sammelbearbeitung;
 	CButton	m_CB_Drucken;
	CButton	m_CB_Import;
	CButton	m_CB_Freigeben;
	CButton	m_CB_Fluko;
	CButton	m_CB_Excel;
	CButton	m_CB_Entf;
	CButton	m_CB_Einfuegen;
	CButton	m_CB_Cxx;
	CButton	m_CB_Ansicht;
	CButton	m_CB_Analyse;
	CButton	m_CB_Aendern;
	CButton	m_CB_Kopieren;
	//}}AFX_DATA


//attributes
public:

	int imDialogBarHeight;

	CWnd *pomParent;
	CCSTable *pomSeasonTable;

	
	// Definition of drag-and-drop object
	CCSDragDropCtrl omDragDropObject;
	CCSDragDropCtrl omDragDropTarget;
	
	
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSeasonTableDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation

public:
	bool GetNextLine(SEASONFLIGHTDATA *prmAFlight = NULL, SEASONFLIGHTDATA *prmDFlight = NULL);
	bool GetPrevLine(SEASONFLIGHTDATA *prmAFlight = NULL, SEASONFLIGHTDATA *prmDFlight = NULL);
	
	void UpdateView();
	void ShowDetail();
	void ShowAnsicht();

	void SeasonDlgGetPrev(NEW_SEASONDLG_DATA *popData);
	void SeasonDlgGetNext(NEW_SEASONDLG_DATA *popData);
	

	void Verbinden(SEASONFLIGHTDATA *prpAFlight, SEASONFLIGHTDATA *prpDFlight) ;

	void SelectFlight(long lpUrno);
	bool ShowFlight(long lpUrno);
	void DeselectAll(void);



private:
	SeasonTableViewer omViewer;
	void UpdateComboAnsicht();
	BOOL CheckPostFlight(SEASONTABLE_LINEDATA *prlTableLine);
	BOOL PostFlight(SEASONFLIGHTDATA *prpAFlight, SEASONFLIGHTDATA *prpDFlight);
	BOOL HandlePostFlight(BOOL blpPostflight);
	// Generated message map functions
	//{{AFX_MSG(CSeasonTableDlg)
	afx_msg LONG OnTableDragBegin(UINT wParam, LONG lParam);
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
	virtual BOOL OnInitDialog();
	afx_msg void OnSchliessen();
	afx_msg void OnEinfuegen();
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	afx_msg void OnAendern();
	afx_msg void OnKopieren();
	afx_msg void OnTeilen();
	afx_msg void OnVerbinden();
	afx_msg void OnExcel();
	afx_msg void OnCxx();
	afx_msg void OnEntf();
	afx_msg void OnAnalyse();
	afx_msg void OnFluko();
	afx_msg void OnSelchangeComboAnsicht();
	afx_msg void OnButtonAnsicht();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnFreigeben();
	afx_msg void OnPrint();
	afx_msg void OnImport();
	afx_msg void OnClose();
 	afx_msg void OnSammelbearbeitung();
	afx_msg void OnSearch();
	virtual void OnCancel();
    afx_msg LONG OnTableLButtonDown(UINT wParam, LONG lParam);
    afx_msg LONG OnTableReturnPressed(UINT wParam, LONG lParam);
	afx_msg LONG OnTableSelChanged( UINT wParam, LPARAM lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SEASONTABLEDLG_H__C7CD8831_241F_11D1_82D3_0080AD1DC701__INCLUDED_)
