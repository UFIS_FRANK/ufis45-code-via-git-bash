#if !defined(AFX_CicConfTableDlg_H__39D1ABF1_278C_11D2_8588_0000C04D916B__INCLUDED_)
#define AFX_CicConfTableDlg_H__39D1ABF1_278C_11D2_8588_0000C04D916B__INCLUDED_



#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// CicConfTableDlg.h : header file
//

#include <CicConfTableViewer.h>
#include <CCSTable.h>
#include <CCSDragDropCtrl.h>

/////////////////////////////////////////////////////////////////////////////
// CicConfTableDlg dialog

class CicConfTableDlg : public CDialog
{
// Construction
public:
	CicConfTableDlg(CWnd* pParent = NULL);   // standard constructor
	~CicConfTableDlg();

	void Activate();

	void Reset();

	CCSTable *pomTable;

	CicConfTableViewer omViewer;

	CCSDragDropCtrl omDragDropObject;

	bool isCreated;

// Dialog Data
	//{{AFX_DATA(CicConfTableDlg)
	enum { IDD = IDD_CICCONFTABLE };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CicConfTableDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	LONG ProcessDropCcaDuty(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);

	// Generated message map functions
	//{{AFX_MSG(CicConfTableDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnClose();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg LONG OnDragOver(UINT wParam, LONG lParam);
	afx_msg LONG OnDrop(UINT, LONG); 
	afx_msg LONG OnTableDragBegin(UINT wParam, LONG lParam);
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
    afx_msg LONG OnTableLButtonDown(UINT wParam, LONG lParam);
    afx_msg LONG OnTableReturnPressed(UINT wParam, LONG lParam);
	afx_msg LONG OnTableSelChanged( UINT wParam, LPARAM lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CicConfTableDlg_H__39D1ABF1_278C_11D2_8588_0000C04D916B__INCLUDED_)
