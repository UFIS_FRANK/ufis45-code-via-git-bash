// stafchrt.cpp : implementation file
//

#include <stdafx.h>
#include <CCSGlobl.h>
#include <resource.h>
#include <FPMS.h>
#include <CCSButtonCtrl.h>
#include <CCSClientWnd.h>
#include <CCSTimeScale.h>
#include <ccsdragdropctrl.h>
#include <PosDiaViewer.h>
#include <PosGantt.h>
#include <PosDiagram.h>
//----------------------------------------------------------------------

#include <PosChart.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// PosChart
/////////////////////////////////////////////////////////////////////////////

IMPLEMENT_DYNCREATE(PosChart, CFrameWnd)

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

PosChart::PosChart()
{
    pomTimeScale = NULL;
    pomStatusBar = NULL;
    pomViewer = NULL;
    
    pomTopScaleText = NULL;
	pomCountText = NULL;
	imStartVerticalScalePos = 30;    
	imStartTopScaleTextPos = 100;
	lmBkColor = lgBkColor;
}

PosChart::~PosChart()
{
    if (pomTopScaleText != NULL)
        delete pomTopScaleText;
	if (pomCountText != NULL)
		delete pomCountText;
}

BEGIN_MESSAGE_MAP(PosChart, CFrameWnd)
    //{{AFX_MSG_MAP(PosChart)
    ON_WM_CREATE()
	ON_WM_DESTROY()
    ON_WM_SIZE()
    ON_WM_ERASEBKGND()
    ON_WM_PAINT()
    ON_BN_CLICKED(IDC_CHARTBUTTON, OnChartButton)
    ON_MESSAGE(WM_CCSBUTTON_RBUTTONDOWN, OnChartButtonRButtonDown)
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_MESSAGE(WM_DROP, OnDrop)  
	ON_COMMAND(31, OnMenuAssign)
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


int PosChart::GetHeight()
{
    if (imState == Minimized)
        return imStartVerticalScalePos; // height of ChartButtton/TopScaleText
    else                                // (imState == Normal) || (imState == Maximized)
		return imStartVerticalScalePos + omGantt.GetGanttChartHeight() + 2;
        //return imStartVerticalScalePos + imHeight + 2;
}

/////////////////////////////////////////////////////////////////////////////
// PosChart message handlers


int PosChart::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
        return -1;
    
    // TODO: Add your specialized creation code here
     
	//
    pomCountText = new CCS3DStatic(TRUE);
    pomCountText->Create("999", WS_CHILD | WS_VISIBLE,
        CRect(130 + 10, 4 + 1, imStartTopScaleTextPos - 10, (4 + 20) - 1), this);
	//
    
	CRect olRect; GetClientRect(&olRect);

    pomTopScaleText = new CCS3DStatic(TRUE);
    pomTopScaleText->Create("Top Scale Text", WS_CHILD | WS_VISIBLE,
        CRect(imStartTopScaleTextPos + 6, 4 + 1, olRect.right - 6, (4 + 20) - 1), this);

	m_ChartWindowDragDrop.RegisterTarget(this, this);
    m_CountTextDragDrop.RegisterTarget(pomCountText, this);
    m_TopScaleTextDragDrop.RegisterTarget(pomTopScaleText, this);

    ////
    // read all data
    //SetState(Maximized);
    
    omGantt.SetTimeScale(GetTimeScale());
   
	omGantt.SetViewer(GetViewer());
    omGantt.SetStatusBar(GetStatusBar());
    omGantt.SetDisplayWindow(GetStartTime(), GetStartTime() + GetInterval());
    omGantt.SetVerticalScaleWidth(imStartTopScaleTextPos);
	GetViewer()->MakeMasstab();
    omGantt.SetFonts(GetViewer()->GetGeometryFontIndex(), GetViewer()->GetGeometryFontIndex());
    omGantt.SetVerticalScaleColors(/*BLUE*/NAVY, SILVER, YELLOW, SILVER, GRAY);
	omGantt.SetGanttChartColors(NAVY, SILVER, YELLOW, SILVER);
	omGantt.bmMiniBar = GetViewer()->bmGeometryTimeLines;

	omGantt.SetTopScaleText(pomTopScaleText);


    omGantt.Create(0, CRect(olRect.left, imStartVerticalScalePos, olRect.right, olRect.bottom), this);
 
#ifndef	PICHIT_FIXED_THE_COUNTER
	char clBuf[255];
	int ilCount = GetViewer()->GetAllLineCount();
	sprintf(clBuf, "%d", ilCount);
	pomCountText->SetWindowText(clBuf);
#else
	char clBuf[255];
	int ilCount = GetViewer()->GetAllLineCount();
	pomCountText->SetWindowText(clBuf);
#endif
	pomCountText->ShowWindow(SW_HIDE);
    CString olStr("");
    pomTopScaleText->SetWindowText(olStr);
    
    pomTopScaleText->GetWindowText(clBuf, sizeof(clBuf));
	//pomTopScaleText->ShowWindow(SW_HIDE);
    return 0;
}

void PosChart::SetMarkTime(CTime opStatTime, CTime opEndTime)
{
	omGantt.SetMarkTime(opStatTime, opEndTime);
}

void PosChart::OnDestroy() 
{
//	if (bgModal == TRUE)
//		return;
	// Id 30-Sep-96
	// This will remove a lot of warning message when the user change view.
	// If we just delete a staff chart, MFC will produce two warning message.
	// First, Revoke not called before the destructor.
	// Second, calling DestroyWindow() in CWnd::~CWnd.

	m_ChartWindowDragDrop.Revoke();
    m_ChartButtonDragDrop.Revoke();
    m_CountTextDragDrop.Revoke();
    m_TopScaleTextDragDrop.Revoke();

	omGantt.DestroyWindow();

	CFrameWnd::OnDestroy();
}

void PosChart::OnSize(UINT nType, int cx, int cy) 
{
    // TODO: Add your message handler code here
    CFrameWnd::OnSize(nType, cx, cy);
    
    CRect olClientRect; GetClientRect(&olClientRect);
    //TRACE("PosChart OnSize: client rect [top=%d, bottom+%d]\n", olClientRect.top, olClientRect.bottom);
    CRect olRect(imStartTopScaleTextPos + 6, 4 + 1, olClientRect.right - 6, (4 + 20) - 1);
    pomTopScaleText->MoveWindow(&olRect, FALSE);

    olRect.SetRect (olClientRect.left, imStartVerticalScalePos, olClientRect.right, olClientRect.bottom);
    //TRACE("PosChart SetRect: client rect [top=%d, bottom+%d]\n", imStartVerticalScalePos, olClientRect.bottom);
    omGantt.MoveWindow(&olRect, FALSE);
}

BOOL PosChart::OnEraseBkgnd(CDC* pDC) 
{
    // TODO: Add your message handler code here and/or call default
    CRect olClipRect;
    pDC->GetClipBox(&olClipRect);
    //GetClientRect(&olClipRect);

    CBrush olBrush(lmBkColor);
    CBrush *polOldBrush = pDC->SelectObject(&olBrush);
    pDC->PatBlt(olClipRect.left, olClipRect.top,
        olClipRect.Width(), olClipRect.Height(),
        PATCOPY);
    pDC->SelectObject(polOldBrush);
    
    return TRUE;
    
    //return CFrameWnd::OnEraseBkgnd(pDC);
}

void PosChart::OnPaint()
{
    CPaintDC dc(this); // device context for painting
    
    // TODO: Add your message handler code here
    
    // Do not call CFrameWnd::OnPaint() for painting messages
    CPen *polOldPen = (CPen *) dc.SelectStockObject(BLACK_PEN);

    CRect olClientRect;
    GetClientRect(&olClientRect);
    
#define imHorizontalPos imStartVerticalScalePos
#define imVerticalPos imStartTopScaleTextPos

    // draw horizontal seperator
    dc.SelectStockObject(BLACK_PEN);
    dc.MoveTo(olClientRect.left, imHorizontalPos - 2);
    dc.LineTo(olClientRect.right, imHorizontalPos - 2);
    
    dc.SelectStockObject(WHITE_PEN);
    dc.MoveTo(olClientRect.left, imHorizontalPos - 1);
    dc.LineTo(olClientRect.right, imHorizontalPos - 1);
    //

    // draw vertical seperator
    dc.SelectStockObject(BLACK_PEN);
    dc.MoveTo(imVerticalPos - 2, olClientRect.top);
    //dc.LineTo(imVerticalPos - 2, olClientRect.bottom);
    dc.LineTo(imVerticalPos - 2, imHorizontalPos - 2);
    
    dc.SelectStockObject(WHITE_PEN);
    dc.MoveTo(imVerticalPos - 1, olClientRect.top);
    //dc.LineTo(imVerticalPos - 1, olClientRect.bottom);
    dc.LineTo(imVerticalPos - 1, imHorizontalPos - 2);
    //

}

void PosChart::OnChartButton()
{
    if (imState == Minimized)
        SetState(Maximized);
    else
        SetState(Minimized);

	GetParent() -> GetParent() -> SendMessage(WM_POSITIONCHILD, 0, 0L);
}

LONG PosChart::OnChartButtonRButtonDown(UINT wParam, LONG lParam)
{
	CMenu menu;
	menu.CreatePopupMenu();
	for (int i = menu.GetMenuItemCount(); i > 0; i--)
	     menu.RemoveMenu(i, MF_BYPOSITION);
    menu.AppendMenu(MF_STRING,31, "Einteilen");	// Confirm

	CPoint olPoint(LOWORD(lParam),HIWORD(lParam));
	ClientToScreen(&olPoint);
    menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, olPoint.x, olPoint.y, this, NULL);

	return 0L;
}

void PosChart::OnMenuAssign()
{

}

void PosChart::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	GetParent()->SendMessage(WM_KEYDOWN, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
}

void PosChart::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	GetParent()->SendMessage(WM_KEYUP, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
}


LONG PosChart::OnDragOver(UINT wParam, LONG lParam)
{
	return -1L;	// cannot accept this object
//----------------------------------------------------------------------
}

LONG PosChart::OnDrop(UINT wParam, LONG lParam)
{
    return -1L;
//----------------------------------------------------------------------
}

LONG PosChart::ProcessDropShifts(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	return 0L;
}

//-DTT Jul.23-----------------------------------------------------------
LONG PosChart::ProcessDropDutyBar(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	return 0L;
}

LONG PosChart::ProcessDropFlightBar(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	return 0L;
}
//----------------------------------------------------------------------
