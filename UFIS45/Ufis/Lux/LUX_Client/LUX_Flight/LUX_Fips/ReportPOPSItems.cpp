// ReportPOPSItems.cpp: Implementierungsdatei
//
// Listen - Items f�r Daily/Season und Cca  DAILY-Reports ( POPS = FAG BVD )


#include <stdafx.h>
#include <ReportPOPSItems.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


///////////////////////////////////////////////////////////////////////////////////////
// -------- DAILYITEMS ----------------------------------------------------------------


DAILYITEMS::DAILYITEMS() : ReportArt( DailyAll )
{

	NoOfItems = 28 ;
	TotalOfItems = 28 ;
	Folgezeile = 0 ;
	TwoVias = false ;
}


DAILYITEMS::DAILYITEMS( int ipTab ) : ReportArt( ipTab  )
{
	DAILYITEMS() ;	// Default Konstruktor

	if( ipTab == DailyFlight )
	{
		Alc.Size = 6;
		Alc.LeftFrame = true ;
		FltnIn.Size = 5;
		FltnOut.Size = 5;
		Act.Size = 5;
		Act.LeftFrame = true ;
		Nose.Size = 3;
		Nose.LeftFrame = true ;
		Org.Size = 6;
		Org.LeftFrame = true ;
		Sta.Size = 6;
		Des.Size = 6 ;
		Des.LeftFrame = true ;
		Std.Size = 6 ;
		ChtDes.Size = 6 ;
		ChtDes.LeftFrame = true ;
		Pos.Size = 3 ;
		Pos.LeftFrame = true ;
		Gp.Size = 3 ;
		Gp.LeftFrame = true ;
		Ctr.Size = 3 ;
		Ctr.LeftFrame = true ;
		Pax.Size = 3;
		Pax.LeftFrame = true ;

		RemIn.Size = 10 ;			// RemIn bis ViaOut gleiche Gr��e !!
		RemIn.LeftFrame = true ;
		RemIn.RightFrame = true ;
		RemOut.Size = 10 ;
		RemOut.LeftFrame = true ;
		RemOut.RightFrame = true ;
		ViaIn.Size = 10 ;
		ViaIn.LeftFrame = true ;
		ViaIn.RightFrame = true ;
		ViaOut.Size = 10 ;
		ViaOut.LeftFrame = true ;
		ViaOut.RightFrame = true ;

		Einteil.Size = 39 ;
		Einteil.LeftFrame = true ;

		NoOfItems = DAILYITEMSItems ;
	}

	if( ipTab == DailyCca )
	{
		Alc.Size = 6;
		Alc.LeftFrame = true ;
		FltnOut.Size = 6;		// Fltn + s
		Act.Size = 5;
		Act.LeftFrame = true ;
		Des.Size = 4 ;
//		Des.LeftFrame = true ;
		ViaOut.Size = 16 ;		// Routing
		ViaOut.LeftFrame = true ;
		Std.Size = 6 ;

		// ToDo: Length bestimmen
		CAnz[0].LeftFrame = true ;
		CAnz[0].Size = 2 ;		// Anzahl Cca
		Ckift[0].Size = 9 ;		// Cca from - to
		CMin[0].Size = 3 ;		// Cca �ffnungsdauer
		CkiftTime[0].Size = 13 ;	// Cca �ffnungszeiten from - to

		NoOfItems = DAILYITEMSCcaItems ;
	}
}


DAILYITEMS::~DAILYITEMS()
{
	;
}


void DAILYITEMS::GetDailyFormatList( CString& FormatList )
{
	// liefert Argument f�r Viewer::SetFormatList()
	if( ReportArt == DailyFlight )
	{
		FormatList.Format("%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d",
			Alc.Size, FltnIn.Size, FltnOut.Size, Act.Size, Nose.Size, 
			Org.Size, Sta.Size, 
			Des.Size, Std.Size, ChtDes.Size,
			Pos.Size ,Gp.Size , Ctr.Size, Pax.Size ,
			Einteil.Size ) ;
	}
	else if( ReportArt == DailyCca )
	{
		FormatList.Format("%d|%d|%d|%d|%d|%d|%d|%d|%d|%d",	// |%d|%d|%d|%d",
			Alc.Size, FltnOut.Size, Act.Size,ViaOut.Size, Des.Size, Std.Size,
			CAnz[0].Size, Ckift[0].Size, CMin[0].Size, CkiftTime[0].Size );
			// CAnz[1].Size, Ckift[1].Size, CMin[1].Size, CkiftTime[1].Size ) ;
			// bei evt. Saison : CFlag.Size, CVaft.Size ) ;

	}
	else
	{ 
		//  Default anlegen
		// ToDo: Default Length 3 ?
		FormatList.Empty() ;
		for( int index =1; index < TotalOfItems; index ++ )
			FormatList += CString("3|") ;
		FormatList += CString("3") ;

	}

} 


bool DAILYITEMS::IsFagSpecial( CString opAlc2 )
{
	// FAG Besonderheiten
	// ToDo: �ber StringArray (konfigurierbar) !?
	if( opAlc2.IsEmpty() ) return( true ) ;

	if(ogCfgData.GetPopsReportDailyAlcSetup().Find(opAlc2) >= 0) return( true );

	/*	
	
	if( opAlc2.CompareNoCase( "TD" ) == 0 ) return( true ) ;
	if( opAlc2.CompareNoCase( "GW" ) == 0 ) return( true ) ;
	if( opAlc2.CompareNoCase( "JK" ) == 0 ) return( true ) ;
	if( opAlc2.CompareNoCase( "BJ" ) == 0 ) return( true ) ;
	if( opAlc2.CompareNoCase( "AS" ) == 0 ) return( true ) ;
	if( opAlc2.CompareNoCase( "VL" ) == 0 ) return( true ) ;
	// neu am 7/9/98
	if( opAlc2.CompareNoCase( "VG" ) == 0 ) return( true ) ;
	if( opAlc2.CompareNoCase( "WQ" ) == 0 ) return( true ) ;
	if( opAlc2.CompareNoCase( "SS" ) == 0 ) return( true ) ;
	// neu am 14/9/98
	if( opAlc2.CompareNoCase( "AJ" ) == 0 ) return( true ) ;
	// neu am 09/12/98
	if( opAlc2.CompareNoCase( "FH" ) == 0 ) return( true ) ;
	*/

	return( Is2LtrCode( opAlc2 ) ) ;	

}

bool DAILYITEMS::Is2LtrCode( CString opAlc2 )
{
	// true <=> Alc2 eine Ziffer enth�lt
	return( opAlc2.FindOneOf( "0123456789" ) < 0 ? false : true ) ;	
} 

bool DAILYITEMS::DoLeftFrame( int ipItem )
{
	// ipItem aus enum { ALC, etc. ..... }
	// liefert Wert von ipItem::LeftFrame 
	if( ReportArt == DailyFlight )
	{
		switch( ipItem )
		{
		case ALC:
			return( Alc.LeftFrame ) ;
		case FLTNIN:
			return( FltnIn.LeftFrame ) ;
		case FLTNOUT:
			return( FltnOut.LeftFrame ) ;
		case ACT:
			return( Act.LeftFrame ) ;
		case NOSE:
			return( Nose.LeftFrame ) ;
		case ORG:
			return( Org.LeftFrame ) ;
		case STA:
			return( Sta.LeftFrame ) ;
		case DES:
			return( Des.LeftFrame ) ;
		case STD:
			return( Std.LeftFrame ) ;
		case CHTDES:
			return( ChtDes.LeftFrame ) ;
		case POS:
			return( Pos.LeftFrame ) ;
		case GP:
			return( Gp.LeftFrame ) ;
		case CTR:
			return( Ctr.LeftFrame ) ;
		case PAX:
			return( Pax.LeftFrame ) ;
		case EINTEIL:
			return( Einteil.LeftFrame ) ;
		default:
			return( false ) ;
		}
	}
	else if( ReportArt == DailyCca )
	{
		switch( ipItem )
		{
		case CALC:
			return( Alc.LeftFrame ) ;
		case CFLTNOUT:
			return( FltnOut.LeftFrame ) ;
		case CACT:
			return( Act.LeftFrame ) ;
		case CROUT:
			return( ViaOut.LeftFrame ) ;
		case CDES:
			return( Des.LeftFrame ) ;
		case CSTD:
			return( Std.LeftFrame ) ;
		case CCNO1:
			return( CAnz[0].LeftFrame ) ;
		case CCIFT1:
			return( Ckift[0].LeftFrame ) ;
		case CCKIMIN1:
			return( CMin[0].LeftFrame ) ;
		case CCKITIME1:
			return( CkiftTime[0].LeftFrame ) ;
/*
		case CCNO2:
			return( CAnz[1].LeftFrame ) ;
		case CCIFT2:
			return( Ckift[1].LeftFrame ) ;
		case CCKIMIN2:
			return( CMin[1].LeftFrame ) ;
		case CCKITIME2:
			return( CkiftTime[1].LeftFrame ) ;
*/
		default:
			return( false ) ;
		}

	}
	else return false ;
}


void DAILYITEMS::GetEmptyFormatString(CString & opFormat)
{
	// leeren Formatstring f�r Viewertabelle in "opFormat" liefern
	CString olEmpty( '|', GetNoOfItems() - 1 );	// Preset: leere Zeile
	opFormat = olEmpty ;
}


int DAILYITEMS::GetNoOfRem( CString opRem )
{
	// liefert die Anzahl Folgezeilen, die sich aus der Bemerkung opRem ergeben
	return( (int) ( (float) opRem.GetLength() / (float) GetRemLength() + 0.99 ) );
}



//-----------------------------------------------------------------------------------------------

