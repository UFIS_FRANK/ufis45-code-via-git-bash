#ifndef AFX_REPORTSORTDLG_H__CDB257E2_8BF5_11D1_8127_0000B43C4B01__INCLUDED_
#define AFX_REPORTSORTDLG_H__CDB257E2_8BF5_11D1_8127_0000B43C4B01__INCLUDED_

// ReportSortDlg.h : Header-Datei
//
#include <CCSEdit.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CReportSortDlg 

class CReportSortDlg : public CDialog
{
// Konstruktion
public:
	CReportSortDlg(CWnd* pParent = NULL, CString opHeadline = "", CTime* oMinDate = NULL, CTime* oMaxDate = NULL, int *opSelect = 0);

	int *pimSelect;

// Dialogfelddaten
	//{{AFX_DATA(CReportSortDlg)
	enum { IDD = IDD_REPORTSORT };
	CCSEdit	m_CE_Datumvon;
	CCSEdit	m_CE_Datumbis;
	CButton	m_CR_List1;
	int		m_List1;
	CString	m_Datumbis;
	CString	m_Datumvon;
	//}}AFX_DATA
	CString omHeadline;
	CTime *pomMinDate;
	CTime *pomMaxDate;

// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CReportSortDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CReportSortDlg)
	virtual void OnCancel();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_REPORTSORTDLG_H__CDB257E2_8BF5_11D1_8127_0000B43C4B01__INCLUDED_
