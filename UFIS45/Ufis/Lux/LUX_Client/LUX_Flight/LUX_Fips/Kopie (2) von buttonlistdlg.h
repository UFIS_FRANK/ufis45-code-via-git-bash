// ButtonListDlg.h : header file
//

#if !defined(AFX_ButtonListDlg_H__FC3BA6F9_1AED_11D1_82C3_0080AD1DC701__INCLUDED_)
#define AFX_ButtonListDlg_H__FC3BA6F9_1AED_11D1_82C3_0080AD1DC701__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <CCSButtonCtrl.h>
#include <DDEML.h>
#include <resource.h>

class CAutoProxyDlg;

/////////////////////////////////////////////////////////////////////////////
// CButtonListDlg dialog

class CButtonListDlg : public CDialog
{
	DECLARE_DYNAMIC(CButtonListDlg);
	friend class CAutoProxyDlg;

// Construction
public:
	CButtonListDlg(CWnd* pParent = NULL);	// standard constructor
	virtual ~CButtonListDlg();
	void MoveWindow( int x, int y, int nWidth, int nHeight, BOOL bRepaint = TRUE );

// Dialog Data
public:
    int m_nDialogBarHeight;
	CWnd *pomParent;
	LONG OnBcAdd(UINT /*wParam*/, LONG /*lParam*/); 

public:
// Dialog Data
	//{{AFX_DATA(CButtonListDlg)
	enum { IDD = IDD_BUTTONLIST };
	CButton	m_CB_CommonCCA;
	CButton	m_CB_Warteraeume;
	CButton	m_CB_Telexpool;
	CButton	m_CB_Tagesflugplanerfassen;
	CButton	m_CB_Stammdaten;
	CButton	m_CB_Setup;
	CButton	m_CB_Rueckgaengig;
	CButton	m_CB_Regeln;
	CButton	m_CB_Reports;
	CButton	m_CB_LFZPositionen;
	CCSButtonCtrl	m_CB_Konflikte;
	CCSButtonCtrl	m_CB_Info;
	CButton	m_CB_Hilfe;
	CButton	m_CB_Gepaeckbaender;
	CButton	m_CB_Gates;
	CButton	m_CB_Flugdiagramm;
	CButton	m_CB_Coverage;
	CButton	m_CB_Checkin;
	CButton	m_CB_Acin;
	CCSButtonCtrl	m_CB_Achtung;
	CButton	m_CB_Flugplanpflegen;
	CButton	m_CB_Flugplanerfassen;
	CButton	m_CB_Beenden;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CButtonListDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

public:

	bool bmWait;

	void CallTelexpool(long lpUrno, const CString &ropAlc3, const CString &ropFltn,
					   const CString &ropFlns, const CString &ropTifFrom, const CString &ropTifTo,
					   const CString &ropRegn, const CString &ropFlda, const CString &ropAdid, const CTime &ropStoa, const CTime &ropStod);

	bool CallCuteIF(CString opMessage);

	void MessTlxpool(CString opInMessage);
					
	void CleanUp();
	
// Implementation
protected:
	CAutoProxyDlg* m_pAutoProxy;
	HICON m_hIcon;
	bool bmIsCuteIFStarted;

	void RunTelexPool(CString opAlc = "", CString opFltn = "", CString opFlns = "", CString opTifFrom = "", CString opTifTo = "", CString opTtyp = "");

	void CallTelexpoolOld(const CString &ropAlc3, const CString &ropFltn, const CString &ropFlns,
						  const CString &ropTifFrom, const CString &ropTifTo);


	void CallTelexpoolNew(long lpUrno, const CString &ropAlc3, const CString &ropFltn, const CString &ropFlns, 
						  const CString &ropRegn, const CString &ropFlda, const CString &ropAdid, const CTime &ropStoa, const CTime &ropStod);

	bool GetWhereFromTlxpoolMsg(const CString &ropInMessage, CString &ropWhere);


	DWORD HandleDdeError(DWORD opDdeInstance, const char *pcpUserText);

	BOOL CanExit();

public:

	// Generated message map functions
	//{{AFX_MSG(CButtonListDlg)
	afx_msg void OnTimer(UINT nIDEvent);
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnDestroy();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnClose();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnBeenden();
	afx_msg void OnDrucken();
	afx_msg void OnHilfe();
	afx_msg void OnSetup();
	afx_msg void OnInfo();
	afx_msg void OnKonflikte();
	afx_msg void OnCoverage();
	afx_msg void OnFlugplanErfassen();
	afx_msg void OnFlugplanPflegen();
	afx_msg void OnTagesflugplanErfassen();
	afx_msg void OnFlugdiagramm();
	afx_msg void OnCheckin();
	afx_msg void OnGepaeckbaender();
	afx_msg void OnGates();
	afx_msg void OnWarteraeume();
	afx_msg void OnLFZPositionen();
	afx_msg void OnStammdaten();
	afx_msg void OnRegeln();
	afx_msg void OnAchtung();
	afx_msg void OnRueckgaengig();
	afx_msg void OnOnline();
	afx_msg void OnReports();
	afx_msg void OnACIn();
	afx_msg void OnTelexpool();
	afx_msg void OnCommoncca();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ButtonListDlg_H__FC3BA6F9_1AED_11D1_82C3_0080AD1DC701__INCLUDED_)
