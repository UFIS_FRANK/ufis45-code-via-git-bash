static char sccs_flight[] = "@(#) UFIS 4.4 (c) ABB AAT/I flight.c 44.561 / 01/03/23 19:07:46 / TWE";
static char version_nbr[] = "44.561";
static char version_date[] = "01/03/23 19:07:46";
static char cfg_version[] = "@(#)UFIS 4.4 (c) ABB AAT/I [flight.cfg]    98-11-13";

/**********************************************************************/
/*                                                                    */
/* CCS Program Skeleton                                               */
/*                                                                    */
/* Author  :  GSA                                                     */
/*                                                                    */
/* Update history : Redesigned by BST in 12.97 - 03.98                */
/*                                                                    */
/*                  Takeover by TWE version 1.12                      */
/*                  from 99/10/05 16:20:00 (Shanghai 507554 Bytes)    */
/* 20001219         'CHANGE STOA' nur, wenn CEDA-Zeit komplett        */
/* 20001219         CheckRegnHistory: BC mit ADID,TIFA,...  fuer FIPS */
/* 20001223         'TWE 27042000 OUTSOURCED BECAUSE OF SWISS' raus   */
/* 20001224         In SHANGHAI PSTD should be set by FIPS AutoAllocation */
/* 20001225         CheckRegnHistory: RTYP X gibt es nicht! (stoert im FIMS) */
/* 20001225         StoreReceivedData sets .RecHdlCnt to the last used */
/*                  record and so some Rotation Departure didnt update */
/* 20001226         'CF09: Basicdata not found' mit ADDI und Inhalt   */
/* 20001227         RTYP is initialized now in HardCodeFkey           */
/* 20001228         avoid message 'FIELD <CCATAB> <VIAL> not found'   */
/* 20001228         RTYP/RKEY reread and set with SetNewValues        */
/* 20001228         StoreReceivedData now sets .RecHdlCnt only when   */
/*                  ipRec > .RecHdlCnt                                */
/* 20001228         CheckRkeyUpdates: REGN/ACT5 may be evaluated and  */
/*                  not received! Update only when NewLen > 0         */
/* 20010123         InsertFlights: STOD may change, check CCA         */
/* 20010124         HardCodeFkey: added STOD,PDES,BAO1,BAC1 to read list  */
/* 20010124         TWEChangeDepTimeFlds: add pclDepField to dbg output */
/* 20010130         UnFlgIfDbsEqNew: checks if NewFlg is correct      */
/* 20010131         TWEBuildFlightNbr: 2-Char ALC3 nach ALC2 kopieren */
/* 20010131         igTWESQLDebug raus                                */
/* 20010216         Fehlermeldung im SearchFlight bei Oracle-Fehlern  */
/*                                                                    */
/**********************************************************************/
/* This program is a CCS main program */

#define GetFieldIndex(iTab,FName) GetField_C_Index(__LINE__,iTab,FName)

/* use TRACE_FIELD_OF_INTEREST(...) or not: */
#undef USE_TRACE_FIELD
#ifdef USE_TRACE_FIELD
  #define TRACE_FIELD_OF_INTEREST(_FROMLINE_,_FROMEXACT_) \
    if (igJIMNewFlg !=  prgRecFldDat[1][1][igJIMFieldNoOI].NewFlg ) \
    {  /* Fieldname of interest */ \
       dbg (TRACE,"%s: ACT3/1 changed before %d: <%s> ==> <%d>",_FROMEXACT_,_FROMLINE_, \
            prgRecFldDat[1][1][igJIMFieldNoOI].NewDat,\
            prgRecFldDat[1][1][igJIMFieldNoOI].NewFlg); \
       igJIMNewFlg= prgRecFldDat[1][1][igJIMFieldNoOI].NewFlg; \
    } 
  #define Dummy_OF_INTEREST(_FROMLINE_,_FROMEXACT_) \
    /* trace to get the linenumber, where (i.e.) ACT3 changes */ \
    dbg (TRACE,"%s: tracing ACT3/1 changed: Line <%d> ",_FROMEXACT_,_FROMLINE_); \
    if (strcmp(pcgJIMFieldValOI1,prgRecFldDat[1][1][igJIMFieldNoOI].NewDat) != 0 ) \
    {  /* Fieldname of interest */ \
       dbg (TRACE,"%s: ACT3/1 changed before %d: <%s> ==> <%s>",_FROMEXACT_,_FROMLINE_, \
            pcgJIMFieldValOI1,prgRecFldDat[1][1][igJIMFieldNoOI].NewDat); \
       strcpy(pcgJIMFieldValOI1,prgRecFldDat[1][1][igJIMFieldNoOI].NewDat); \
    } \
    if (strcmp(pcgJIMFieldValOI2,prgRecFldDat[1][1][igJIMFieldNoOI].NewDat) != 0 ) \
    {  /* Fieldname of interest */ \
       dbg (TRACE,"%s: ACT3/2 changed before %d: <%s> ==> <%s>",_FROMEXACT_,_FROMLINE_, \
            pcgJIMFieldValOI2,prgRecFldDat[1][1][igJIMFieldNoOI].NewDat); \
       strcpy(pcgJIMFieldValOI2,prgRecFldDat[1][1][igJIMFieldNoOI].NewDat); \
    }\
    if (strcmp(pcgJIMFieldValOI3,prgRecFldDat[1][1][igJIMFieldNoOI2].NewDat) != 0 ) \
    {  /* Fieldname of interest */ \
       dbg (TRACE,"%s: ACT5/1 changed before %d: <%s> ==> <%s>",_FROMEXACT_,_FROMLINE_, \
            pcgJIMFieldValOI3,prgRecFldDat[1][1][igJIMFieldNoOI2].NewDat); \
       strcpy(pcgJIMFieldValOI3,prgRecFldDat[1][1][igJIMFieldNoOI2].NewDat); \
    } \
    if (strcmp(pcgJIMFieldValOI4,prgRecFldDat[1][1][igJIMFieldNoOI2].NewDat) != 0 ) \
    {  /* Fieldname of interest */ \
       dbg (TRACE,"%s: ACT5/2 changed before %d: <%s> ==> <%s>",_FROMEXACT_,_FROMLINE_, \
            pcgJIMFieldValOI4,prgRecFldDat[1][1][igJIMFieldNoOI2].NewDat); \
       strcpy(pcgJIMFieldValOI4,prgRecFldDat[1][1][igJIMFieldNoOI2].NewDat); \
    }
  /* should be called as (i.e.)
     TRACE_FIELD_OF_INTEREST(__LINE__,"Startup");
  */
#else
   /* if this line is active, all occurences of TRACE_FIELD_OF_INTEREST(..) */
   /* are replaced with nothing by the compiler, no code is generated!      */
  #define TRACE_FIELD_OF_INTEREST(_FROMLINE_,_FROMEXACT_)
#endif

#define U_MAIN
#define UGCCS_PRG

#define STH_USE

#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <ctype.h>
#include <string.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"

#include "timdef.h"
#include "sthdef.h"
#include "router.h"
#include <time.h>

/* new includes for TIMESTAMP DEBUG */
#include <sys/types.h>
#include <sys/timeb.h>

#include <stdlib.h>
#include "db_if.h"
#include "tools.h"
#include "loghdl.h"
#include "cli.h"
#include "queutil.h"
#include "action_tools.h"
#include "loghdl.h"
#include "syslib.h"
#include "fditools.h"
#include "cedatime.h"

/* Default values */
#define ACTUAL "AFT"
#define ARCHIV "ARC"
#define CHECK_IN "CCA"
#define DAYS_PRIOR_ACTUAL -4
#define DAYS_POST_ACTUAL 4
#define DAYS_PRIOR_STRG "-4"
#define DAYS_POST_STRG "4"

#define DATABLK_SIZE (256*1024)
#define I_RESULTBUFSTEP (1024*1024*2)
#define RESULTBUFSTEP (1024*1024)
#define MAX_FIELD_LEN 4096      /* Bytes */

/* Flight DataHandling */
#define MAX_TBL_HDL 10
#define MAX_REC_HDL 20
#define MAX_FLD_HDL 400

#define MAX_DFS_HDL 2500

#define MAX_ERROR_HDL 10

#define MAX_SELKEY_SIZE 4096
#define MAX_FLDLST_SIZE 4096
#define MAX_DATBLK_SIZE 4096

#define MAX_REGN_LINES 1024
#define REGN_BUFF_SIZE 512
#define RKEY_BUFF_SIZE (16*1024)

#define MAX_TMPBUF1_SIZE 4096
#define MAX_TMPBUF2_SIZE 4096
#define MAX_TMPBUF3_SIZE 4096
#define MAX_RULBUF_SIZE 4096 * 4
#define MAX_CFLBUF_SIZE 4096

#define FOR_NORMAL 0
#define FOR_INSERT 1
#define FOR_UPDATE 2
#define FOR_DELETE 3
#define FOR_SEARCH 4
#define FOR_TABLES 5

#define LAST_LEFT 1
#define FIRST_RIGHT 2

#define FOR_RULE 0
#define FOR_CNFL 1

#define IS_LESS 1
#define IS_GREATER 2
#define IS_IN_DATA 3

/* ******************************************************************** */
/* Internal used structures						*/
/* ******************************************************************** */

/* Handles (Descriptors and Properties) of USED TABLES Defined in Config*/
/* These Struct Elements are Filled HardCoded. See: InitPojectTables(..) */
/* Used As 'prgPrjTblCfg [TableIndex]'  */
typedef struct
{
  char Project[8];              /* ShortName of Project */
  char TableExt[8];             /* TableExtension of Project */
  char FullName[16];            /* FullName of That Table */
  char ShortName[8];            /* ShortName of That Table */
  char TableType[8];            /* TableType Defined in SysTab */
  int TypeOfTable;              /* 'Switchable' Type of Table */
  int RcvFlg;                   /* Flag of Received Data */
  int DbsFlg;                   /* Flag of Data Read From DataBase */
  int NewFlg;                   /* Flag of New Data */
  int RecHdlMax;                /* Number of RecordBuffers to Allocate */
  int RecHdlCnt;                /* Number of Actually Used RecordBuffers */
  int TblRecLen;                /* Total Length of One Record */
  int TblFldCnt;                /* Number of TableFields */
  int AllFldCnt;                /* Total Number of Fields */
  int TblRulCnt;                /* Number of Assigned Rules */
  int TblCflCnt;                /* Number of Assigned Conflict Rules */
  int DatChgCnt;                /* Counter of Changed Data */
  char *OutSelKey;              /* Pointer to BroadCast Selection */
  int OutSelLen;                /* Length of OutSelection */
  char *AllFldLst;              /* Pointer to List of all Fields */
  char *ActFldLst;              /* Pointer to Actually Read FieldList */
  char *SqlFldLst;              /* Pointer to Update/Insert FieldList */
  char *OutFldLst;              /* Pointer to BroadCast FieldList */
  char *RmsFldLst;              /* Pointer to List of RMS Fields */
  int RmsFldCnt;                /*  */
  int ActFldLen;                /* Length of ActFieldList */
  int SqlFldLen;                /* Length of SqlFieldList */
  int OutFldLen;                /* Length of OutFieldList */
  char *SqlDatBlk;              /* Pointer to Update/Insert DataBlock */
  char *OutDatBlk;              /* Pointer to BroadCast DataBlock */
  char *OldDatBlk;              /* Pointer to BroadCast DataBlock */
  int SqlDatLen;                /* Length of SqlDataBlock */
  int OutDatLen;                /* Length of OutDataBlock */
}
TBL_HANDLES;

/* Handles (Descriptors and Properties) of USED FIELDS Defined in SysTab*/
/* These Struct Elements are Filled HardCoded. See: InitTableFields(..) */
/* Used As 'prgTblFldCfg [TableIndex] [FieldIndex]'  */
typedef struct
{
  char SysTabUrno[16];          /* Urno to Read Missing Descriptors */
  char SysTabFina[32];          /* Name of That Field */
  char SysTabAddi[64];          /* Field Description */
  char SysTabFele[16];          /* FieldLength in ASCII */
  char SysTabType[8];           /* FieldType in ASCII */
  char SysTabDefa[128];         /* Default Value */
  char SysTabSyst[8];           /* Shared Memory Access */
  char SysTabCont[64];          /* Container FieldList */
  char CflWarnTkey[8];          /* Text CFL.MENO of Alarm Messages */
  int FieldLength;              /* Numerical Field Length */
  int FieldType;                /* 'switchable' field type */
  int AutoChange;               /* Can be changed automatically */
  int ReadField;                /* Read Always from DataBase */
  int KeepEntry;                /* Field Accepts Input of Unknown BasicData */
  int WarnEntry;                /* Field Alarms Unknown BasicData */
  int ReadOnly;                 /* ReadOnly Field can't be updated */
  int SendField;                /* Send Always BroadCast, Action and LogHdl */
  int SendOnReop;               /* Send when Flight reset to Operation */
  int SendRulFnd;               /* Send Always to RulFnd (RMS) */
  int ExplodeField;             /* Flag for ExplodeFields */
  int CtrlFlagNbr;              /* Fieldnumber in DSSF as Flag */
  int ChildGroups;              /* Number of Child Groups Exploded */
  int GroupLength;              /* Length of DataRegion per Group */
  int GroupMembers;             /* Number of Child Fields per Group */
  int FirstChild;               /* Index to Related SubField (for ExplodeFields) */
  int LastChild;                /* Index to Related SubField (for ExplodeFields) */
  int MainIndex;                /* Index to Related ParentField (for SubFields) */
  int FirstChar;                /* Data Position in MainField */
  int LastChar;                 /* Data Position in MainField */
  char *RcvDat;                 /* Pointer to Received Data */
  int RcvFlg;                   /* Flag of Received Data */
  int RcvLen;                   /* Length of Received Data */
  int RcvVal;                   /* Converted Value of Received Data */
}
FLD_HANDLES;

/* Pointers to and Values of HANDLED FIELD DATA */
/* Initialized and Filled Dynamically */
/* Used As 'prgRecFldDat [TableIndex] [RecordIndex] [FieldIndex]'  */
typedef struct
{
  char *RcvDat;                 /* Pointer to Received Data */
  char *DbsDat;                 /* Pointer to Data Read from DataBase */
  char *NewDat;                 /* Pointer to New Data to be written to DataBase */
  int RcvFlg;                   /* Flag of Received Data */
  int DbsFlg;                   /* Flag of Data Read From DataBase */
  int NewFlg;                   /* Flag of New Data */
  int RcvLen;                   /* Length of Received Data */
  int DbsLen;                   /* Length of Data Read From DataBase */
  int NewLen;                   /* Length of New Data */
  int RcvVal;                   /* Converted Value of Received Data */
  int DbsVal;                   /* Converted Value of DataBase Data */
  int NewVal;                   /* Converted Value of New Data */
}
DAT_HANDLES;

/* Handles (Descriptors and Properties) of USED RULES Defined in CfgFile*/
/* These Struct Elements are Filled Dynamically. See: AsciiConfig(..) */
/* Used As 'prgTblRulCfg [TableIndex] [RuleIndex]'  */
typedef struct
{
  int RuleType;
  char *RuleTarget;
  char *RuleSource;
}
RUL_HANDLES;

/* Handles (Descriptors and Properties) of CONFLICTS Defined in CfgFile*/
/* These Struct Elements are Filled Dynamically. See: AsciiConfig(..) */
/* Used As 'prgTblCflCfg [TableIndex] [CnflIndex]'  */
typedef struct
{
  int CnflType;
  char *CnflTarget;
  char *CnflSource;
}
CFL_HANDLES;

/* List of Errors */
typedef struct
{
  char ErrorMeno[5];
  char ErrorMety[3];
  char ErrorText[256];
  char ErrorData[256];
}
ERROR_HANDLES;

/* Following Helpfull Structures for Different Use */

/* List of Pointers to DataLines */
/* Used as: prgDataLines[256] in InsertFlights */
typedef struct
{
  int LineFilled;
  char *Fields;
  char *Data;
  char *Selection;
}
DATA_POINTERS;

/* List of RadarData from DFS */
typedef struct
{
  int FreeLine;
  int EventCnt;
  int LastEventStamp;
  int LastTmoaVal;
  char LastTmoaDat[16];
  char LineKeys[16];
  char ItemList[64];
  char DataLine[128];
}
RADAR_DATA;

/* List of TextLines for Multiple Use */
typedef struct
{
  int LineType;
  int ErrorNbr;
  int FldLen;
  int DatLen;
  char Location[8];
  char LineKey[16];
  char SortText[32];
  char TextLine[256];
  char FldLst[128];
  char FldDat[128];
}
TEXT_LINES;

/* Lable Texts (see ReturnFlight) */
typedef struct
{
  char Lkey[3];
  char Tkey[4];
  char Text[64];
}
LABLE_TEXT;

/* Mapping Field Lists */
typedef struct
{
  int Count;
  char Name[33];
  char RcvFldLst[512];
  char UseFldLst[512];
}
MAP_LIST;

/********************************************************************/
/* External variables                                               */
/********************************************************************/
FILE *outp = NULL;
int debug_level = TRACE;

/******************************************************************/
/* Global variables                                               */
/******************************************************************/
static char pcgMainModName[] = "flight";
static char pcgBinFile[256];
static char pcgCfgFile[256];
static char pcgCedaCfg[256];
static char pcgLangCode[16];
static int igMainModId = 7800;
static int igToBcHdl = 1900;
static int igToLogHdl = 0;
static int igToAction = 0;
static int igToRulFnd = 0;
static int igToAdam = 0;
static int igRulFndCallAlways = FALSE;
static char pcgOutRoute[64];

static int igUseShm = TRUE;
static int igTmoOffset = 10;
static int igShowStampDiff = FALSE;
static time_t tgBeginStamp = 0;
static time_t tgEndStamp = 0;
static time_t tgStampDiff = 0;
static time_t tgMaxStampDiff = 0;
static time_t tgQueTime = 0;
static time_t tgNowTime = 0;
static time_t tgQueDiff = 0;

static int igUtcDiff = 0;
static int igUtcToLocal = 0;
static char pcgDbsTimeType[128];
static char pcgDbsTime[16];
static char pcgSystemTime[16];
static char pcgStampType[16];
static int igTifaMinDiff = 720;
static int igReorgTable = FALSE;
static char pcgReorgKey[2048];

static int igStartUpMode = TRACE;
static int igRuntimeMode = TRACE;
static int igAutoMode = TRACE;
static int igCurrentAction = 0;
static int igDebugDetails = FALSE;
static FILE *pfgLogFile = NULL;

static char pcgLogSmmFields[512];

static char pcgCsgnCutFltn[512];

static char pcgClntCharSet[512];
static char pcgServCharSet[512];

/* Patch Character Set */
static char cgPtCR;
static char cgPtLF;

static char pcgTraceModes[1024];
static char pcgDebugModes[1024];
static char pcgOffModes[1024];
static char pcgWksModes[1024];
static char pcgUsrModes[1024];
static char pcgExcoNames[512];
static char pcgRejectNames[512];
static char pcgReadOnlyCmd[512];
static char pcgFsiCmd[16];

static int igConfigError = FALSE;
static int igCheckFlightKey = FALSE;
static int igCheckFkeyFtyp = FALSE;
static int igCheckHistory = TRUE;
static int igRegnHistoryLines = -1;
static int igSitaChecked = FALSE;
static int igIgnoreDoubles = FALSE;
static int igUpdateDoubles = FALSE;
static int igDeleteDoubles = FALSE;
static int igImportAction = FALSE;
static int igClientAction = TRUE;
static int igBroadCast = TRUE;
static int igBcIfr = FALSE;
static int igMyOwnPid = 0;
static int igMyParentPid = 0;
static int igTotalFlights = 0;
static int igChangeInfo = TRUE;
static int igUnlockDssf = FALSE;
static int igReopFlight = FALSE;
static int igChkDblFld = FALSE;

static TBL_HANDLES prgPrjTblCfg[MAX_TBL_HDL + 1];
static FLD_HANDLES prgTblFldCfg[MAX_TBL_HDL + 1][MAX_FLD_HDL + 1];
static DAT_HANDLES prgRecFldDat[MAX_TBL_HDL + 1][MAX_REC_HDL + 1][MAX_FLD_HDL + 1];

static char *pcgGlobalDataBuffer = NULL;
static char *pcgLastDataPos = NULL;
static int igTotalBufferSize;

static int igHandledTables = 0;

static RUL_HANDLES prgTblRulCfg[MAX_TBL_HDL + 1][MAX_FLD_HDL + 1];
static char pcgGlobalRuleBuffer[MAX_RULBUF_SIZE];
static char *pcgLastRulePos = NULL;

static CFL_HANDLES prgTblCflCfg[MAX_TBL_HDL + 1][MAX_FLD_HDL + 1];
static char pcgGlobalCnflBuffer[MAX_CFLBUF_SIZE];
static char *pcgLastCnflPos = NULL;

static ERROR_HANDLES prgErrorList[MAX_ERROR_HDL + 1];
static int igErrorCount = 0;

static DATA_POINTERS prgDataLines[128];
static int igDataLineCount = 0;

static TEXT_LINES prgRegnLines[MAX_REGN_LINES + 1];
static int igRegnLineCount = 0;
static int igRegnLineTotal = 0;

static RADAR_DATA prgRadarData[MAX_DFS_HDL + 1];
static int igRadarDataCount = 0;

static LABLE_TEXT prgLableText[20];
static MAP_LIST prgFldMapLst[50];

static char pcgTmpBuf1[MAX_TMPBUF1_SIZE];
static char pcgTmpBuf2[MAX_TMPBUF2_SIZE];
static char pcgTmpBuf3[MAX_TMPBUF3_SIZE];

static ITEM *prgItem = NULL;    /* The queue item pointer  */
static EVENT *prgEvent = NULL;  /* The event pointer       */

static char pcgRecvName[64];    /* BC-Head used as WKS-Name */
static char pcgDestName[64];    /* BC-Head used as USR-Name */
static char pcgTwStart[64];     /* BC-Head used as Stamp */
static char pcgTwEnd[64];       /* BC-Head used as Stamp */

static int igGlNoAck = FALSE;

static int que_out = 0;

static char pcgErrMsg[512];     /* Error message buffer */

static char *pcgResultData = NULL;
static int igActResultSize = 0;

/* ****************************************** */
/* Define time window for ACTUAL Flight table */
/* ****************************************** */
static int igDaysPrior = 0;
static int igDaysPost = 0;

struct tm *prgCurDate = NULL;   /* Current date */

char pcgCurTime[20];            /* Current Time string */
char pcgCurDate[20];            /* Current Date string */

static int igSysMinStamp = 0;
static int igSysDayStamp = 0;
static int igSysWkdStamp = 0;
static int igRelMinStamp = 0;
static int igRelDayStamp = 0;
static int igRelWkdStamp = 0;

static int igSplitAlways = TRUE;
static int igJoinAlways = TRUE;

static int igAddHopo = TRUE;

static char pcgDefH3LC[8];      /* Default 3-Letter code of Home Port */
static char pcgDefH4LC[8];      /* Default 4-Letter code of Home Port */
static char pcgH3LC[8];         /* Used 3-Letter code of Home Port */
static char pcgH4LC[8];         /* Used 4-Letter code of Home Port */

static char pcgDefTblExt[8];    /* Default TableExtension */
static char pcgDefBasExt[8];    /* Default BasicData TableExtension */
static char pcgTblExt[8];       /* Used TableExtension */
static char pcgBasExt[8];       /* TableExtension for BasicData */

static char pcgAftTab[16];      /* Actual Table */
static char pcgArcTab[16];      /* Archive Table */
static char pcgCcaTab[16];      /* Check-in Table */

static int igAftTab;            /* Actual Table */

static char pcgDataArea[DATABLK_SIZE];
static char pcgSqlBuf[12 * 1024];

static int igDontCallSql = FALSE; /* For special test */

static char pcgRegnList[REGN_BUFF_SIZE + 1];
static char pcgRkeyList[RKEY_BUFF_SIZE + 1];

/* TWE */

static int igPOPS = FALSE;
static int igTWEFromAZFlight = FALSE;
static int igTWECheckDEFC = TRUE;
static int igTWEUrnoIsNumber = FALSE;
static int igTWEForwardAll = FALSE;
static int igTWEImport = FALSE;
static int igTWERoundtrip = FALSE;
static int igTWEJustUpdJfno = FALSE;
static int igTWEUpdate = FALSE;
static int igTWENewFlnoFlight = FALSE;
static int igTWEIgnoreDays = 0;
static int igTWEUpdFlights = 0;
static int igTWEUtc2LocalfromConfig = FALSE;
static int igTWESplit = FALSE;

/* JIM */
static int igJIMCheckRkeys = FALSE;
static int igJIMRotMaxRec = 0; /* During Rotation the .RecCnt is set to 1 
                                  during StoreReceivedData */
#ifdef USE_TRACE_FIELD
static int igJIMFieldNoOI = 0; /* Fieldnumber of interest */
static int igJIMFieldNoOI2 = 0; /* Fieldnumber of interest */
static int igJIMNewFlg = FALSE;
static char pcgJIMFieldValOI1[100] = ""; /* FieldVal of interest */
static char pcgJIMFieldValOI2[100] = ""; /* FieldVal of interest */
static char pcgJIMFieldValOI3[100] = ""; /* FieldVal of interest */
static char pcgJIMFieldValOI4[100] = ""; /* FieldVal of interest */
#endif /* USE_TRACE_FIELD */

static char pcgBorderTime[15] = "";

static char pcgRKeyDat[20] = "";

static char pcgJoinAirl[4] = "";
static char pcgJoinFltn[8] = "";
static char pcgJoinFlts[4] = "";
static char pcgCurrJfno[100] = "";
static char pcgCurrJcnt[10] = "";
static char pcgCurrRkey[20] = ""; /* TWE 01022000 */
static char pcgCurrRTyp[2] = ""; /* TWE 01022000 */
static char pcgTWEFieldsRecv[2048] = "";

static char pcgTWEUpdUrno[11] = "";
static char pcgTWEDepLocTimFlds[512]; /* fieldlist of time dependent fields */


/* FOR DEBUGGING TIMESTAMPS */
struct timeb tgTimeP;
long lgSecSqlBeg;
long lgMilliSecSqlBeg;
long lgSecSqlEnd;
long lgMilliSecSqlEnd;
long lgSqlSec;
long lgSqlMilliSec;

static long lgJoinValidDays = 7;

/********************************************************************/
/* External functions                                               */
/********************************************************************/
extern int init_que ();         /* Attach to CCS queues */
extern int que (int, int, int, int, int, char *); /* CCS queuing routine  */
extern int send_message (int, int, int, int, char *); /* CCS msg-handler i/f  */
extern int init_db ();

extern void str_chg_upc (char *);
extern void str_trm_lft (char *, char *);
extern void str_trm_rgt (char *, char *, int);
extern void str_trm_all (char *, char *, int);
extern int get_item (int, char *, char *, int, char *, char *, char *);
extern int get_real_item (char *, char *, int);
extern int get_item_no (char *, char *, int);
extern int StrgPutStrg (char *, int *, char *, int, int, char *);
extern int GetWord (int, char *, char *);
extern int SendRemoteShutdown (int);
extern int snap (char *, long, FILE *);

/****************************************************************/
/* Function prototypes	                                        */
/****************************************************************/
static int InitSystem (int);
static int GetOutModId (char *pcpProcName);
static int CheckConfigVersion (void);
static int InitProjectTables (char *, char *, char *, char *, int);
static int InitTableFields (int, int);
static int InitExplodeFields (int);
static int ExplodeFieldData (int, int, char *);
static int ClearExplodeGroup (int, int, int, int);
static int ImplodeFieldData (int, int, char *);
static int InitDataBuffer (void);
static int SetInternalFieldType (char *, int *);
static int InitLogFileModes (void);
static int InitDataLogFile (int);
static int CheckSystemInit (int);
static int InitFirstActions (int ipRetVal);
static int GetLogFileMode (int *, char *, char *);

static int init_flight ();
static int InitDbsTime (void);
static int InitGlobalConfig (int ipRc);
static void InitPerformanceCheck (void);

static int CheckTableExtension (char *);

static int Reset (void);        /* Reset program          */
static void Terminate (void);   /* Terminate program      */
static void HandleSignal (int); /* Handles signals        */
static void HandleErr (int);    /* Handles general errors */
static void HandleQueErr (int); /* Handles queuing errors */
static int HandleData (void);   /* Handles event data     */
static void HandleQueues (void); /* Waiting for Sts.-switch */

static int FlightDataInput (char *, char *, char *, char *, int);
static int BuildAfiKeyValues (char *, char *, char *, int, char *, char *, char *, char *);

static int HandleImportData (char *, char *, char *, char *, int);
static int BuildUpdateValues (char *, char *, char *, char *, char *);
static int CheckFlightRotation (char *, char *);
static int HandleExcoData (char *, char *, char *, char *);
static int HandleDfsRadarData (char *, char *, char *, char *);

static int InsertFlights (char *, char *, char *, char *, char *, int);
static int UpdateFlights (char *, char *, char *, char *, int);
static int CheckRkeyUpdates (void);
static int JoinFlights (char *, char *, char *, char *, int);
static int JoinChain (char *, char *, char *, char *);
static int SplitFlights (char *, char *, char *, char *);
static int SplitChain (char *, char *, int);

static int HandleGetFlightRecord (char *, char *, char *, char *, char *);
static int HandleGetDeletedFlights (void);
static int HandleDeleteFlightRecord (void);
static int HandleCreateGroundMovement ();
static int HandleDeleteGroundMovement ();
static int CleanUpFlightTables ();

static int GetCfg (char *, char *, short, char *, char *);
static int GetCedaCfg (char *, char *, char *, char *);
static int BuildItemBuffer (char *, char *, int, char *);
static int GetSysTabVal (char *FldBuff, int ipMaxSize, int ipFldNbr);
static int CheckSysShmField (char *, char *, int);
static int ResetDataBuffer (long ,int, int, int, int, int, int, int);

static void CheckHomePort (void);
static int GetTableIndex (char *pcpTableName);
static int GetField_C_Index (long lpLineNo, int ipTblIdx, char *pcpFieldName);
/* static int GetFieldIndex (int ipTblIdx, char *pcpFieldName); */
static int GetAsciiConfig (void);
static int InitSourceFlagCtrl (void);
static int GetCfgTasks (char *pcpCfgSec, char *pcpCfgRow, int);
static int StoreTableRule (char *, char *, char *, char *);
static int StoreTableCnfl (char *, char *, char *, char *);
static int GetTableType (char *);
static int GetRuleType (char *);
static int GetCnflType (char *);
static int InitRuleFields (void);
static int SetReadAlways (int, char *);
static int SetReadOnly (int, char *);
static int SetSendAlways (int, char *);
static int SetSendOnReop (int, char *);
static int SetSendToRulFnd (int, char *pcpTable, char *pcpFldLst);
static void CheckNewEntry (int, int, char *, char *);
static void CheckChgEntry (int, int, char *, char *);
static void CheckDelEntry (int, int, char *, char *);
static void CheckAnyChange (int, int, char *, char *);
static void CheckMixFields (int, int, char *, char *);
static void CheckCmpValues (int, int, char *, char *);
static int SetBestTimes (int, int, char *, char *);
static int SetCheckTimes (int, int, char *, char *);
static int SetCalcTimes (int, int, char *, char *);
static int SetBasicData (int, int, char *, char *);
static int SetBasicColumn (int, int, char *, char *);
static int SetChangeInfo (int, int, char *, char *);
static int CheckBasicData (int, int, char *, char *);
static int AutoFillField (int, int, char *, char *);
static int BasicLookup (int, int, char *, char *);
static int DayCheckField (int, int, char *, char *);
static int GetDataParts (char *, char *, char *, char *, char *, char *, int *, int *, int *);
static int GetReceivedData (char *, char *, char *);
static int StoreReceivedData (int, int, int, int, char *, char *, char *);
static int CheckReceivedRouting (int, int, int, char *, char *);
static int ShowStoredData (void);
static int ShowReceivedData (void);
static int SearchFlight (char *, char *, char *, int, int);
static int BuildSqlReadFields (int, char *, int);
static int GetRecordData (int, int, int, char *, char *, char *);
static int ShowRecordData (void);
static int PrepNewData (int);
static int SetNewData (void);
static int SetUrnoKeys (char *, long *);
static void ShowNewData (void);
static int SetNewTimeValues (int);
static int BuildTimeValues (int, char *, char *);
static int EvaluateFieldRules (int);
static int EvaluateConflicts (int);
static int CheckFieldValue (int, int, int, char *, int *);
static int CollectChanges (int, int);
static int BuildSqlFldStrg (char *, char *, int);
static int SetNewValues (int, int, int, char *, char *);
static int SetRcvValues (int, int, char *, char *);
static int UnSetRcvValues (int, char *);
static void UnSetNewValues (int, int, int, char *, int);
static int HandleFlightData (int);
static int EvaluateFirstAction (int);
static void CheckRcvdDssfFields (int ipTbl);
static int EvaluateLastAction (int);
static int MakeUpdateAndBroadCast (long, int, int, char *, int, char *, char *);
static int MakeInsertAndBroadCast (int, int, char *, int, char *, char *);
static int GetBasicData (char *, char *, char *, char *, char *, int, int);
static int GetBasicRange (char *, char *, char *, char *, char *, char *, int, int);
static int GetFieldProperties (int);
static int DataTripleCheck (char *, char *, char *, char *, char *, char *, char *, char *, char *);
static int CheckRuleCondition (char *, int *, int, int);

static int ReleaseActionInfo (char *, char *, char *, char *, char *, char *, char *, char *);

static int StoreErrorData (char *, char *, char *, char *);
static int CollectErrorData (char *);
static int FormatDayFreq (char *, char *);
static int SetFlnoItems (int, int);
static int BuildFlightNbr (char *, char *, char *, char *, char *, char *, int);
static int CheckArrDepFields (char *, char *);
static int BuildFkey (char *, char *, char *, char *, char *, char *);
static void BuildProperFlno (int);

static void HardCodeAdid (int, int);
static void HardCodePaid (int, int);
static void HardCodeFkey (int, int, int, char *);
static void HardCodeStat (int, int);
static void HardCodeCsgn (int, int);
static void HardCodeDoop (int, int, char *, char *);
static void HardCodeSeas (int, int);
static void HardCodeJcnt (int ipTbl, int ipRec);
static void HardCodeVias (int, int);

static int DeleteBySelection (char *pcpTable, char *pcpSelKey);
static int ReorgAftTable (char *);
static int SetUtcDiffTime (int, int, int);
static int FlucoPeriodToCeda (char *, char *, char *, char *);
static int FlucoDateToCeda (char *, char *, char *);
static int MoveYear (char *, int);

static int CheckNewRadarData (char *, char *, char *, char *, char *);
static void FreeRadarDataGarbage (void);
static void ModifyRcvValue (int ipTbl, char *pcpField, int ipVal);
static int CheckNewSitaData (int);

static int CheckRulFndCall (int ipTbl, int ipRec);
static int CheckCcaHdlCall (void);
static int SendRecordsToRulFnd (char *, int, char *, char *);
static int MoveCcaTimes (char *, int);

static void CheckWhereClause (int, char *, int, int, char *);

static void CheckRkeyHistory ();
static void CheckRegnHistory (char *pcpListOfRegn);
static void SwapDataLines (int ilRec1, int ilRec2);
static void SendChanges (char *pcpSelKey, char *pcpFldLst, char *pcpFldDat, char *pcpFipsChkFlds, char *pcpFipsChkData,
                         int ipWorkFlag);
static void ReorgRegnHist (void);
static void AddRegnToList (char *pcpRegn);
static void AddRkeyToList (int, int, char *pcpRkey);

static void CheckPerformance (int ipFlag, char *);
static void SendRecordToAdam (void);

static void HandleReturnFlight (int ipBack, char *pcpFtyp, int ipTbl, int ipRec);
static void InitLables (void);
static void GetLabelText (char *, char *, char *, char *);
static void GetPatchValues (void);
static void SendConflict (int ipTbl, int ipRec, int ipFld, char *pcpDest);

static void InitEventMapping (void);
static void InitMappingLists (char *pcpName);
static void ReadFieldMapping (char *pcpName);
static void CheckFieldNameMapping (char *pcpFldNam);
static void GetMappedFieldName (char *, char *, char *);
static int FormatCedaFlno (char *pcpFlno);
static int RejectEvent (char *, char *, char *, char *, char *);
static void CheckAutoBarShift (void);

/* TWE */
static int TWEHandlePOPSData (char *pcpRcvTable, char *pcpFields, char *pcpData, char *pcpSelKey);
static int TWESearchAndDelRoundtrip (char *pcpTable, char *pcpSelKey, int ipTbl, int ipRec);
static int TWEBuildFlightNbr (char *fnr3, char *fnr2, char *alc3, char *alc2, char *flnr, char *flsf, int mth);
static int TWEAddJfnoToRecord (int ipTbl, int ipRec, int ipExists);
static int TWEGetLocTimDepends (void);
static int TWEChangeDepTimeFlds (int ipTbl, int ipRec);

static int TWEHardCodeFlno (void);

/***********************************************************/
/*                                                         */
/* The MAIN program                                        */
/*                                                         */
/***********************************************************/
MAIN
{
  int ilRC = RC_SUCCESS;
  int ilDbgFileCount = 0;
  time_t ilTp;

  debug_level = igStartUpMode;
  INITIALIZE;                   /* General initialization */

  igMyOwnPid = getpid ();
  igMyParentPid = getppid ();

  (void) SetSignals (HandleSignal);
  (void) UnsetSignals ();

  ilRC = InitSystem (ilRC);

  /* Call Terminate if neccessary */
  ilRC = CheckSystemInit (ilRC);

  if ((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
  {
    dbg (TRACE, "MAIN WAITING FOR HSB STATUS SWITCH ...");
    HandleQueues ();
  }                             /* end of if */

  if ((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
  {
    ilRC = init_flight ();
    ilRC = CheckSystemInit (ilRC);
    ilRC = InitFirstActions (ilRC);
    ilRC = CheckSystemInit (ilRC);
  }                             /* end of if */

  /* Prepare Global Functions */
  igUtcDiff = 0;
  pcgReorgKey[0] = 0x00;

  /* to avoid compiler warnings */
  if (ilRC == RC_SUCCESS)
  {
    TRACE_FIELD_OF_INTEREST(__LINE__,"Startup");
    debug_level = igRuntimeMode;
    for (;;)
    {
      dbg (TRACE, "================= START/END =========================");
      ilRC = que (QUE_GETBIG, 0, mod_id, PRIORITY_3, 0, (char *) &prgItem);
      prgEvent = (EVENT *) prgItem->text;
      /* Compute current date */
      time (&ilTp);
      memcpy (prgCurDate, gmtime (&ilTp), sizeof (struct tm));
      prgCurDate->tm_hour = 0;
      prgCurDate->tm_min = 0;
      prgCurDate->tm_sec = 0;
      if (ilRC == RC_SUCCESS)
      {
        /* Acknowledge the item */
        ilRC = que (QUE_ACK, 0, mod_id, 0, 0, NULL);
        if (ilRC != RC_SUCCESS)
        {
          /* handle que_ack error */
          HandleQueErr (ilRC);
        }                       /* fi */
        GetServerTimeStamp (pcgStampType, 1, 0, pcgCurTime);
        (void) GetFullDay (pcgCurTime, &igSysDayStamp, &igSysMinStamp, &igSysWkdStamp);
        (void) GetFullDay (pcgCurTime, &igRelDayStamp, &igRelMinStamp, &igRelWkdStamp);
        (void) ChgDayToDate (pcgCurDate, igSysDayStamp);
        /* DebugPrintItem(DEBUG,prgItem); */
        /* DebugPrintEvent(DEBUG,prgEvent); */
        switch (prgEvent->command)
        {
          case HSB_STANDBY:
            ctrl_sta = prgEvent->command;
            HandleQueues ();
            break;

          case HSB_COMING_UP:
            ctrl_sta = prgEvent->command;
            HandleQueues ();
            break;
          case HSB_ACTIVE:
            ctrl_sta = prgEvent->command;
            break;
          case HSB_STANDALONE:
            ctrl_sta = prgEvent->command;
            ResetDBCounter ();
            break;
          case HSB_ACT_TO_SBY:
            ctrl_sta = prgEvent->command;
            /* close_connection(); */
            HandleQueues ();
            break;
          case HSB_DOWN:
            ctrl_sta = prgEvent->command;
            Terminate ();
            break;
          case REMOTE_DB:
            HandleRemoteDB (prgEvent);
            break;

          case SHUTDOWN:
            Terminate ();
            break;

          case RESET:
            ilRC = Reset ();
            break;

          case EVENT_DATA:
            if ((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
            {
              ilRC = HandleData ();
            }                   /* end if */
            else
            {
              dbg (TRACE, "WRONG HSB STATUS (%d)", ctrl_sta);
              DebugPrintItem (TRACE, prgItem);
              DebugPrintEvent (TRACE, prgEvent);
            }                   /* end else */
            break;

          case TRACE_ON:
            dbg_handle_debug (prgEvent->command);
            igRuntimeMode = debug_level;
            break;

          case TRACE_OFF:
            dbg_handle_debug (prgEvent->command);
            igRuntimeMode = debug_level;
            break;

          case 101:            /* MORE TRACE DETAILS */
            if (igDebugDetails == TRUE)
            {
              igDebugDetails = FALSE;
            }                   /* end if */
            else
            {
              igDebugDetails = TRUE;
            }                   /* end else */
            break;

          case 102:
            if (igUseShm == TRUE)
            {
              dbg (TRACE, "USE OF SHARED MEMORY DISABLED");
              igUseShm = FALSE;
            }
            else
            {
              dbg (TRACE, "USE OF SHARED MEMORY ENABLED");
              igUseShm = TRUE;
            }
            ilRC = RC_SUCCESS;
            break;

          case 201:
            (void) InitDataLogFile (TRUE);
            break;
          case 202:
            if (pfgLogFile != NULL)
            {
              fclose (pfgLogFile);
              pfgLogFile = NULL;
            }                   /* end if */
            break;

          case 203:
            igUtcDiff = 0;
            ilRC = iGetConfigRow (pcgCfgFile, "REORG_AFTTAB", "UTC_DIF", CFG_STRING, pcgReorgKey);
            if (ilRC == RC_SUCCESS)
            {
              igUtcDiff = atoi (pcgReorgKey);
            }                   /* end if */
            pcgReorgKey[0] = 0x00;
            ilRC = iGetConfigRow (pcgCfgFile, "REORG_AFTTAB", "SQL_KEY", CFG_STRING, pcgReorgKey);
            ReorgAftTable (pcgAftTab);
            igUtcDiff = 0;
            pcgReorgKey[0] = 0x00;
            ilRC = RC_SUCCESS;
            break;

          case 204:
            ReorgRegnHist ();
            break;

          case 209:
            SendRecordToAdam ();
            break;

          case 777:
            dbg (TRACE, "---- START TEST ----");
            CheckAutoBarShift ();
            ilRC = RC_SUCCESS;
            break;

          case 778:
            CleanUpFlightTables ();
            ilRC = RC_SUCCESS;
            break;

          case LOG_SWITCH:
            SwitchDebugFile (igRuntimeMode, &ilDbgFileCount);
            break;

          default:
            dbg (TRACE, "MAIN UNKNOWN EVENT");
            DebugPrintItem (TRACE, prgItem);
            DebugPrintEvent (TRACE, prgEvent);
            break;
        }                       /* end switch */

        /* Handle error conditions */

        if (ilRC != RC_SUCCESS)
        {
          HandleErr (ilRC);
        }                       /* end if */
      }
      else
      {
        /* Handle queuing errors */
        HandleQueErr (ilRC);
      }                         /* end else */

      /* Not Neccessary. Said VBL.
         if (prgItem != NULL)
         {
         free(prgItem);
         prgItem = NULL;
         }
       */

    }                           /* end for */

  }                             /* end if */

/*
	exit(0);
*/
  return 0;
}                               /* end of MAIN */

/**************************************************************/
/* The Main Initialization routine (InitSystem)               */
/**************************************************************/
static int InitSystem (int ipRetVal)
{
  int ilRC;
  int ilTryAgain = 10;
  int ilCnt = 0;
  int ilWaitSec;
  int ilWaitTtl;
  char pclActPath[128];
  char *pclPtrPath;
  time_t ilTp;

  ilRC = ipRetVal;

  if (ilRC == RC_SUCCESS)
  {
    dbg (TRACE, "MAIN INITIALIZING ...");
    igMainModId = tool_get_q_id (pcgMainModName);

    dbg (TRACE, "OWN PID=%d / PARENT PID=%d, FLIGHT QUEUE=%d", igMyOwnPid, igMyParentPid, igMainModId);
    dbg (TRACE, "RELEASE: <%s>  DATE/TIME: <%s>\nWHAT VERS: %s\nCONF.FILE: %s", version_nbr, version_date, sccs_flight,
         cfg_version);

    /* Attach Binary File */
    if ((pclPtrPath = getenv ("BIN_PATH")) == NULL)
    {
      strcpy (pclActPath, "/ceda/bin");
    }                           /* end if */
    else
    {
      strcpy (pclActPath, pclPtrPath);
    }                           /* end else */
    sprintf (pcgBinFile, "%s/%s", pclActPath, pcgMainModName);

    /* Attach Config File */
    if ((pclPtrPath = getenv ("CFG_PATH")) == NULL)
    {
      strcpy (pclActPath, "/ceda/conf");
    }                           /* end if */
    else
    {
      strcpy (pclActPath, pclPtrPath);
    }                           /* end else */
    sprintf (pcgCfgFile, "%s/%s.cfg", pclActPath, pcgMainModName);
    sprintf (pcgCedaCfg, "%s/ufis_ceda.cfg", pclActPath);

    ilRC = CheckConfigVersion ();
  }                             /* end if */

  if (ilRC == RC_SUCCESS)
  {
    if (mod_id == igMainModId)
    {
      (void) TransferFile (pcgBinFile);
      (void) TransferFile (pcgCfgFile);
    }                           /* end if */
  }                             /* end if */

  if (ilRC == RC_SUCCESS)
  {
    (void) InitLogFileModes ();
  }                             /* end if */

  /* Init Time Structs for Dates */
  if (ilRC == RC_SUCCESS)
  {
    prgCurDate = (struct tm *) malloc (sizeof (struct tm));
    if (prgCurDate == NULL)
    {
      dbg (TRACE, "MAIN: Malloc error prgCurDate");
      ilRC = RC_FAIL;
    }                           /* end if */
  }                             /* end if */

  /* Init Time Defaults */
  if (ilRC == RC_SUCCESS)
  {
    ilRC = ShowCalendar ();
    time (&ilTp);
    memcpy (prgCurDate, gmtime (&ilTp), sizeof (struct tm));
    prgCurDate->tm_hour = 0;
    prgCurDate->tm_min = 0;
    prgCurDate->tm_sec = 0;
  }                             /* end if */

  /* Init the CEDA queues */
  if (ilRC == RC_SUCCESS)
  {
    ilCnt = ilTryAgain;
    ilWaitSec = 2;
    ilWaitTtl = 0;
    do
    {
      ilRC = init_que ();
      if (ilRC != RC_SUCCESS)
      {
        sleep (ilWaitSec);
        ilCnt--;
        ilWaitTtl += ilWaitSec;
      }                         /* end if */
    }
    while ((ilCnt > 0) && (ilRC != RC_SUCCESS));

    if ((ilRC != RC_SUCCESS) || (ilWaitTtl > 0))
    {
      dbg (TRACE, "WAITED %d SECONDS FOR QCP", ilWaitTtl);
      if (ilRC != RC_SUCCESS)
      {
        dbg (TRACE, "QCP CEDA QUEUES NOT ATTACHED");
      }                         /* end if */
    }                           /* end if */
  }                             /* end if */

  /* Init the DataBase (Login) */
  if (ilRC == RC_SUCCESS)
  {
    ilCnt = ilTryAgain;
    ilWaitSec = 5;
    ilWaitTtl = 0;
    do
    {
      ilRC = init_db ();
      if (ilRC != RC_SUCCESS)
      {
        sleep (ilWaitSec);
        ilCnt--;
        ilWaitTtl += ilWaitSec;
      }                         /* end if */
    }
    while ((ilCnt > 0) && (ilRC != RC_SUCCESS));

    if ((ilRC != RC_SUCCESS) || (ilWaitTtl > 0))
    {
      dbg (TRACE, "WAITED %d SECONDS FOR LOGIN DATABASE", ilWaitTtl);
      if (ilRC != RC_SUCCESS)
      {
        dbg (TRACE, "LOGIN DATABASE FAILED");
      }                         /* end if */
    }                           /* end if */
  }                             /* end if */

  if (ilRC == RC_SUCCESS)
  {
    /* igToBcHdl = tool_get_q_id("bchdl"); */
    igToBcHdl = GetOutModId ("bchdl");

    /* igToLogHdl = tool_get_q_id("loghdl"); */
    igToLogHdl = GetOutModId ("loghdl");

    /* igToAction = tool_get_q_id("action"); */
    igToAction = GetOutModId ("action");

    /* igToRulFnd = tool_get_q_id("rulfnd"); */
    igToRulFnd = GetOutModId ("rulfnd");

    /* igToRulFnd = tool_get_q_id("rulfnd"); */
    igToAdam = GetOutModId ("adam");

    dbg (TRACE, "SPECIAL OUT QUEUES:");
    dbg (TRACE, "LOGHDL=%d RULFND=%d ADAM=%d", igToLogHdl, igToRulFnd, igToAdam);
    sprintf (pcgOutRoute, "%d,%d", igToAction, igToLogHdl);
  }                             /* end if */

  if (ilRC == RC_SUCCESS)
  {
    /* Successfully Started Up */
    /* So Shut Down Remote Flight */
    SendRemoteShutdown (mod_id);
  }                             /* end if */

  return ilRC;
}                               /* end of InitSystem */

/**************************************************************/
/**************************************************************/
static int GetOutModId (char *pcpProcName)
{
  int ilQueId = -1;
  char pclCfgCode[32];
  sprintf (pclCfgCode, "QUE_TO_%s", pcpProcName);
  (void) GetCfg ("QUEUE_DEFINES", pclCfgCode, CFG_STRING, pcgTmpBuf1, "-1");
  ilQueId = atoi (pcgTmpBuf1);
  if (ilQueId < 0)
  {
    ilQueId = tool_get_q_id (pcpProcName);
  }                             /* end if */
  if (ilQueId < 0)
  {
    ilQueId = 0;
  }                             /* end if */
  return ilQueId;
}                               /* end of GetOutModId */

/**************************************************************/
/**************************************************************/
static int CheckConfigVersion (void)
{
  int ilRC = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  char *pclVersPtr = NULL;

  if (strcmp (version_nbr, "%" "I" "%") == 0)
  {
    ilRC = RC_FAIL;
    dbg (TRACE, "=======================================");
    dbg (TRACE, "ERROR: THIS IS NOT A RELEASED VERSION !");
    (void) GetCfg ("VERSION_CHECK", "REL_CONFLICT", CFG_STRING, pcgTmpBuf1, " ");
    if (strcmp (pcgTmpBuf1, "CONTINUE") == 0)
    {
      dbg (TRACE, "ERROR IGNORED DUE TO <CONTINUE> ENTRY");
      ilRC = RC_SUCCESS;
    }                           /* end if */
    dbg (TRACE, "=======================================");
  }                             /* end if */

  ilGetRc = GetCfg ("VERSION_CHECK", "CFG_VERSION_DATE", CFG_STRING, pcgTmpBuf1, " ");
  if (ilGetRc != RC_SUCCESS)
  {
    dbg (TRACE, "=======================================");
    switch (ilGetRc)
    {
      case E_FOPEN:
        dbg (TRACE, "CFG VERSION ERROR: CAN'T OPEN <%s>", pcgCfgFile);
        break;
      case E_SECTION:
        dbg (TRACE, "CFG VERSION ERROR: SECTION <VERSION_CHECK> NOT FOUND");
        break;
      case E_PARAM:
        dbg (TRACE, "CFG VERSION ERROR: ENTRY <CFG_VERSION_DATE> NOT FOUND");
        break;
      default:
        dbg (TRACE, "CFG VERSION: UNDEFINED ERROR %d", ilRC);
        break;
    }                           /* end switch */
    ilRC = RC_FAIL;
  }                             /* end if */
  else
  {
    pclVersPtr = strstr (cfg_version, ".cfg");
    if (pclVersPtr != NULL)
    {
      pclVersPtr += 5;
      pcgTmpBuf2[0] = 0x00;
      (void) GetWord (1, pclVersPtr, pcgTmpBuf2);
      str_trm_rgt (pcgTmpBuf2, " ", TRUE);
      switch (strcmp (pcgTmpBuf1, pcgTmpBuf2))
      {
        case 0:
          ilGetRc = RC_SUCCESS;
          break;
        case -1:
          dbg (TRACE, "VERSION CONFLICT ERROR: CONFIG FILE IS TOO OLD");
          ilGetRc = RC_FAIL;
          break;
        case 1:
          dbg (TRACE, "VERSION CONFLICT ERROR: CONFIG FILE IS TOO NEW");
          ilGetRc = RC_FAIL;
          break;
      }                         /* end switch */
      if (ilGetRc != RC_SUCCESS)
      {
        dbg (TRACE, "CONFIG VERSION: MUST BE <%s> IS <%s>", pcgTmpBuf2, pcgTmpBuf1);
        (void) GetCfg ("VERSION_CHECK", "CFG_CONFLICT", CFG_STRING, pcgTmpBuf1, " ");
        if (strcmp (pcgTmpBuf1, "CONTINUE") == 0)
        {
          dbg (TRACE, "ERROR IGNORED DUE TO <CONTINUE> ENTRY");
          ilGetRc = RC_SUCCESS;
        }                       /* end if */
        dbg (TRACE, "=======================================");
        if (ilGetRc != RC_SUCCESS)
        {
          ilRC = RC_FAIL;
        }                       /* end if */
      }                         /* end if */
    }                           /* end if */
  }                             /* end else */
  return ilRC;
}                               /* end CheckConfigVersion */

/**************************************************************/
/**************************************************************/
static int InitLogFileModes (void)
{
  int ilRC = RC_SUCCESS;
  ilRC = GetLogFileMode (&igStartUpMode, "STARTUP_MODE", "OFF");
  ilRC = GetLogFileMode (&igRuntimeMode, "RUNTIME_MODE", "OFF");
  ilRC = GetCfg ("SYSTEM", "TRACE_MODE", CFG_STRING, pcgTraceModes, ",,");
  ilRC = GetCfg ("SYSTEM", "DEBUG_MODE", CFG_STRING, pcgDebugModes, ",,");
  ilRC = GetCfg ("SYSTEM", "OFF_MODE", CFG_STRING, pcgOffModes, ",,");
  ilRC = GetCfg ("SYSTEM", "WKS_TRACING", CFG_STRING, pcgWksModes, ",,");
  ilRC = GetCfg ("SYSTEM", "USR_TRACING", CFG_STRING, pcgUsrModes, ",,");
  ilRC = RC_SUCCESS;
  debug_level = igStartUpMode;
  igAutoMode = igRuntimeMode;
  ilRC = InitDataLogFile (FALSE);
  return ilRC;
}                               /* end of InitLogFileModes */

/**************************************************************/
/**************************************************************/
static int InitDataLogFile (int ipFlag)
{
  int ilRC = RC_SUCCESS;
  int ilCfgFlg = FALSE;
  char pclCfgRow[128];
  if (mod_id == igMainModId)
  {
    if (pfgLogFile != NULL)
    {
      fclose (pfgLogFile);
      pfgLogFile = NULL;
    }                           /* end if */
    ilRC = GetCfg ("SYSTEM", "EVENT_TRACING", CFG_STRING, pclCfgRow, " ");
    if (ilRC == RC_SUCCESS)
    {
      dbg (TRACE, "DEFAULT EVENT_TRACING <%s>", pclCfgRow);
    }                           /* end if */
    if (strcmp (pclCfgRow, "YES") == 0)
    {
      ilCfgFlg = TRUE;
    }                           /* end if */
    ilRC = GetCfg ("SYSTEM", "EVENT_FILE", CFG_STRING, pclCfgRow, " ");
    if (ilRC == RC_SUCCESS)
    {
      if ((ilCfgFlg == TRUE) || (ipFlag == TRUE))
      {
        pfgLogFile = fopen (pclCfgRow, "w");
        if (pfgLogFile != NULL)
        {
          dbg (TRACE, "OPENED LOG FILE <%s>", pclCfgRow);
        }                       /* end if */
        else
        {
          dbg (TRACE, "OPEN LOG FILE FAILED <%s>", pclCfgRow);
        }                       /* end else */
        dbg (TRACE, "=======================================");
      }                         /* end if */
    }                           /* end if */
    else
    {
      if ((ilCfgFlg == TRUE) || (ipFlag == TRUE))
      {
        dbg (TRACE, "NO LOG FILE CONFIGURED");
        dbg (TRACE, "=======================================");
      }                         /* end if */
    }                           /* end else */
  }                             /* end if */
  ilRC = RC_SUCCESS;
  return ilRC;
}                               /* end of InitDataLogFile */

/**************************************************************/
/* The initialization routine                                 */
/**************************************************************/
static int init_flight (void)
{
  int ilRC = RC_SUCCESS;
  char pclSgsPrj[16];
  char pclSgsExt[16];
  char pclTmp[512];
  char pclTmp2[512];
/****  TWE 26012000  ****/
  char pclInFldLst[20];
  char pclInFldVal[20];
  char pclOutFldLst[20];
  char pclFity[20];
  int ilNumber;
/***********************/

  strcpy (pclSgsExt, "TAB");
  strcpy (pclSgsPrj, "XXX");

  dbg (DEBUG, "INTERNAL INITIALIZING ...");
  igHandledTables = 0;

  ilRC = tool_search_exco_data ("ALL", "TABEND", pclSgsExt);
  ilRC = GetCfg ("DATA_TABLES", "TBL_EXT", CFG_STRING, pcgTblExt, pclSgsExt);
  ilRC = GetCfg ("DATA_TABLES", "BAS_EXT", CFG_STRING, pcgBasExt, pcgTblExt);
  strcpy (pcgDefTblExt, pcgTblExt);
  strcpy (pcgDefBasExt, pcgBasExt);

  ilRC = GetCfg ("FLIGHT_TABLES", "ACTUAL", CFG_STRING, pcgAftTab, ACTUAL);
  ilRC = GetCfg ("FLIGHT_TABLES", "ARCHIV", CFG_STRING, pcgArcTab, ARCHIV);

  ilRC = GetCfg ("CHECK_IN_TABLES", "CHECK_IN", CFG_STRING, pcgCcaTab, CHECK_IN);

  ilRC = tool_search_exco_data ("SYS", "HOMEAP", pclSgsPrj);
  ilRC = GetCfg ("HOME_PORT", "HP3", CFG_STRING, pcgH3LC, pclSgsPrj);
  strcpy (pcgDefH3LC, pcgH3LC);

  ilRC = CheckTableExtension ("SYS");

  ilRC = GetCfg ("TIME_WINDOW", "DAYS_PRIOR_ACTUAL", CFG_STRING, pclTmp, DAYS_PRIOR_STRG);
  igDaysPrior = atoi (pclTmp);

  ilRC = GetCfg ("TIME_WINDOW", "DAYS_POST_ACTUAL", CFG_STRING, pclTmp, DAYS_POST_STRG);
  igDaysPost = atoi (pclTmp);

  ilRC = InitProjectTables (pcgH3LC, pcgAftTab, pcgTblExt, "AFT", 20);
  ilRC = InitProjectTables (pcgH3LC, pcgCcaTab, pcgTblExt, "CKI", 20);

  strcat (pcgAftTab, pcgTblExt);
  strcat (pcgArcTab, pcgTblExt);
  strcat (pcgCcaTab, pcgTblExt);

  ilRC = InitTableFields (0, 0);
  ilRC = InitDataBuffer ();

  ilRC = GetAsciiConfig ();

  sprintf (pclTmp, "APT%s", pcgBasExt);
  ilRC = GetBasicData (pclTmp, "APC3", pcgH3LC, "APC4", pclTmp2, 1, RC_FAIL);
  ilRC = GetCfg ("HOME_PORT", "HP4", CFG_STRING, pcgH4LC, pclTmp2);
  strcpy (pcgDefH4LC, pcgH4LC);

  strcpy (pclInFldLst, "TANA,FINA");
  sprintf (pclInFldVal, "AFT,URNO");
  strcpy (pclOutFldLst, "FITY");
  ilNumber = 1;
  ilRC = syslibSearchSystabData (pcgTblExt, pclInFldLst, pclInFldVal, pclOutFldLst, pclFity, &ilNumber, "");
  if (pclFity[0] == 'N')
  {
    igTWEUrnoIsNumber = TRUE;
    dbg (TRACE, "URNO IS OF TYPE NUMBER");
  }
  else
  {
    igTWEUrnoIsNumber = FALSE;
    dbg (TRACE, "URNO IS OF TYPE CHAR");
  }

  if (mod_id != igMainModId)
  {
    /* It's a Reading Flight (fligro) */
    dbg (TRACE, "=====================================================");
    if (pcgResultData == NULL)
    {
      pcgResultData = (char *) calloc (1, I_RESULTBUFSTEP);
      dbg (TRACE, "INIT: ALLOCATED %d BYTES RESULT BUFFER", I_RESULTBUFSTEP);
      igActResultSize = I_RESULTBUFSTEP;
      if (pcgResultData == NULL)
      {
        igActResultSize = 0;
        dbg (TRACE, "InitFlight: Malloc(pcgResultData) failed");
      }                         /* fi */
    }                           /* end if */
  }                             /* end if */

  dbg (TRACE, "=====================================================");

  ilRC = InitGlobalConfig (ilRC);

  dbg (TRACE, "INITIALIZED HOME <%s> <%s> TABLES <%s> <%s>", pcgH3LC, pcgH4LC, pcgTblExt, pcgBasExt);
  if ((strlen (pcgH3LC) != 3) || (strlen (pcgH4LC) != 4))
  {
    dbg (TRACE, "ERROR: HOME CODES FAILURE <%s> <%s>", pcgH3LC, pcgH4LC);
    igConfigError = TRUE;
  }                             /* end if */
  dbg (TRACE, "=====================================================");
  (void) InitDbsTime ();
  dbg (TRACE, "=====================================================");

  if (ilRC == RC_SUCCESS)
  {
    dbg (TRACE, "=======  NEW LOCATIONTIMEDEPENDS ====================");
    ilRC = TWEGetLocTimDepends ();
    ilRC = RC_SUCCESS;
    dbg (TRACE, "=====================================================");
  }

#ifdef USE_TRACE_FIELD
  igJIMFieldNoOI= GetFieldIndex (1, "ACT3");
  igJIMFieldNoOI2= GetFieldIndex (1, "ACT5");
#endif /* USE_TRACE_FIELD */

  ilRC = RC_SUCCESS;
  return ilRC;
}                               /* end of init_flight */

/*******************************************************/
/*******************************************************/
static int InitDbsTime (void)
{
  char pclTmp[16];
  char pclTmp2[64];
  char pclTmp3[64];
  int ilUtcTimeDiff = 0;

  pcgCurTime[0] = 0x00;
  strcpy (pclTmp3, "UTC");
  GetCedaCfg ("MAIN", "SYSTEM_TIME", pcgSystemTime, "UTC");
  dbg (TRACE, "CEDA SYSTEM TIME IS: <%s>", pcgSystemTime);

  GetCedaCfg ("MAIN", "DBS_TIME_TYPE", pcgDbsTimeType, "UTC,Universal Time Coordinated");
  get_real_item (pcgDbsTime, pcgDbsTimeType, 1);
  if (strcmp (pcgSystemTime, pcgDbsTime) == 0)
  {
    strcpy (pclTmp3, pcgDbsTime);
  }                             /* end if */
  else
  {
  }                             /* end else */
  dbg (TRACE, "DBS TIMES: <%s>", pcgDbsTimeType);

  GetCedaCfg ("MAIN", "TIMESTAMPS", pcgStampType, pclTmp3);
  pcgStampType[3] = 0x00;
  GetServerTimeStamp (pcgStampType, 1, 0, pcgCurTime);
  dbg (TRACE, "GET TIMESTAMPS AS: <%s> <%s>", pcgStampType, pcgCurTime);

  sprintf (pclTmp, "APT%s", pcgBasExt);
  GetBasicData (pclTmp, "APC3", pcgH3LC, "TDI2,TDI1,TICH", pclTmp2, 1, RC_FAIL);
  dbg (TRACE, "UTC TIME INFO: <%s>", pclTmp2);
  ilUtcTimeDiff = atoi (pclTmp2);
  igUtcToLocal = CheckServerTime ();
  if (ilUtcTimeDiff == 0)
  {
    ilUtcTimeDiff = igUtcToLocal;
  }                             /* end if */
  sprintf (pclTmp2, "%d", ilUtcTimeDiff);

  igTWEUtc2LocalfromConfig = FALSE;
  if (GetCedaCfg ("MAIN", "UTC_TO_LOCAL", pcgTmpBuf1, pclTmp2) == RC_SUCCESS)
  {
    igTWEUtc2LocalfromConfig = TRUE;
    dbg (TRACE, "CONFIG: LOCAL - UTC = %s MIN", pcgTmpBuf1);
  }                             /* end if */
  igUtcToLocal = atoi (pcgTmpBuf1);
  dbg (TRACE, "RESULT: LOCAL - UTC = %d MIN", igUtcToLocal);

  return 0;
}                               /* end InitDbsTime */

/*******************************************************/
/*******************************************************/
static void InitPerformanceCheck (void)
{
  (void) GetCfg ("SYSTEM", "TIME_TRACING", CFG_STRING, pcgTmpBuf1, "0");
  tgMaxStampDiff = atoi (pcgTmpBuf1);
  if (tgMaxStampDiff > 0)
  {
    dbg (TRACE, "TIME CHECK SET TO %d SECONDS", tgMaxStampDiff);
  }                             /* end if */
  return;
}                               /* end of InitPerformanceCheck */

/*******************************************************/
/*******************************************************/
static int CheckTableExtension (char *pcpTblNam)
{
  int ilRC = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  short slFkt = 0;
  short slCursor = 0;
  if (igTWEUrnoIsNumber == TRUE)
  {
    sprintf (pcgSqlBuf, "SELECT URNO FROM %s%s WHERE URNO=0", pcpTblNam, pcgTblExt);
  }
  else
  {
    sprintf (pcgSqlBuf, "SELECT URNO FROM %s%s WHERE URNO='0'", pcpTblNam, pcgTblExt);
  }
  slFkt = START;
  slCursor = 0;
  ilGetRc = sql_if (slFkt, &slCursor, pcgSqlBuf, pcgDataArea);
  if (ilGetRc == DB_SUCCESS)
  {
    ilRC = RC_SUCCESS;
  }                             /* end if */
  else
  {
    if (ilGetRc != NOTFOUND)
    {
      dbg (TRACE, "=====================================================");
      dbg (TRACE, "ERROR: WRONG SGS CONFIGURATION !!");
      dbg (TRACE, "TABLE EXT CORRECTED TO: <%s>", pcgH3LC);
      strcpy (pcgDefTblExt, pcgH3LC);
      strcpy (pcgTblExt, pcgH3LC);
      strcpy (pcgBasExt, pcgH3LC);
      dbg (TRACE, "=====================================================");
      ilRC = RC_FAIL;
    }                           /* end if */
  }                             /* end else */
  close_my_cursor (&slCursor);
  if (strcmp (pcgDefTblExt, "TAB") == 0)
  {
    igAddHopo = TRUE;
  }                             /* end if */
  else
  {
    igAddHopo = FALSE;
  }                             /* end else */
  return ilRC;
}                               /* end CheckTableExtension */

/*******************************************************/
/*******************************************************/
static int InitGlobalConfig (int ipRc)
{
  int ilRC = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  ilRC = ipRc;

  GetPatchValues ();

  (void) GetCedaCfg ("MAIN", "LANGUAGE_CODE", pcgLangCode, "ENG");
  dbg (TRACE, "LANGUAGE CODE = <%s>", pcgLangCode);

  InitPerformanceCheck ();

  (void) GetCfg ("GLOBAL_DEFINES", "TMO_OFFSET", CFG_STRING, pcgTmpBuf1, "10");
  igTmoOffset = abs (atoi (pcgTmpBuf1));
  if (igTmoOffset > 0)
  {
    dbg (TRACE, "TMOA CHECK SET TO %d MINUTES", igTmoOffset);
  }                             /* end if */

  ilGetRc = GetCfg ("CALLSIGN_RULES", "CUT_FLTN", CFG_STRING, pcgCsgnCutFltn, "\0");
  if (pcgCsgnCutFltn[0] != '\0')
  {
    dbg (TRACE, "SHORT CSGN FOR <%s>", pcgCsgnCutFltn);
  }                             /* end if */

  ilGetRc = GetCfg ("SMM_LOGGING", "LOG_HEADER", CFG_STRING, pcgLogSmmFields, "\0");
  if (pcgLogSmmFields[0] != '\0')
  {
    dbg (TRACE, "SMM HEADER LIST <%s>", pcgLogSmmFields);
  }                             /* end if */

  InitSourceFlagCtrl ();

  ilGetRc = GetCfg ("GLOBAL_DEFINES", "SBC_IFR", CFG_STRING, pcgTmpBuf1, "NO");
  if (strcmp (pcgTmpBuf1, "YES") == 0)
  {
    dbg (TRACE, "BROADCASTING 'IFR' SET TO 'YES'");
    igBcIfr = TRUE;
  }                             /* end if */

  ilGetRc = GetCfg ("GLOBAL_DEFINES", "SBC_ICR", CFG_STRING, pcgTmpBuf1, "NO");
  if (strcmp (pcgTmpBuf1, "YES") == 0)
  {
    dbg (TRACE, "BROADCASTING 'ICR' SET TO 'YES'");
  }                             /* end if */

  GetCfg ("GLOBAL_DEFINES", "FSI_CMD", CFG_STRING, pcgFsiCmd, "IFR");

  if (igToRulFnd > 0)
  {
    ilGetRc = GetCfg ("GLOBAL_DEFINES", "ALL_TO_RMS", CFG_STRING, pcgTmpBuf1, "NO");
    if (strcmp (pcgTmpBuf1, "YES") == 0)
    {
      dbg (TRACE, "TO RMS: SENDING ALL EVENTS");
      igRulFndCallAlways = TRUE;
    }                           /* end if */
    else
    {
      dbg (TRACE, "TO RMS: SENDING CHANGES ONLY");
      igRulFndCallAlways = FALSE;
    }                           /* end else */
  }                             /* end if */

  ilGetRc = GetCfg ("GLOBAL_DEFINES", "CHECK_HISTORY", CFG_STRING, pcgTmpBuf1, "NO");
  if ((ilGetRc == RC_SUCCESS) && (strcmp (pcgTmpBuf1, "NO") == 0))
  {
    dbg (TRACE, "CHECK HISTORY SET TO 'NO'");
    igCheckHistory = FALSE;
  }                             /* end if */

  /* +++ jim +++ 20001226: configure maximum number of lines for igCheckHistory: */
  ilGetRc = GetCfg ("GLOBAL_DEFINES", "REGN_HISTORY_LINES", CFG_STRING, pcgTmpBuf1, "-1");
  if (ilGetRc != RC_SUCCESS)
  {
    dbg (TRACE, "'REGN_HISTORY_LINES' NO SET (DEFAULT -1)");
  }
  else if (strcmp (pcgTmpBuf1, "-1") != 0)
  {
    igRegnHistoryLines = atoi(pcgTmpBuf1);
  }                             /* end if */
  dbg (TRACE, "REGN HISTORY LINES SET TO %d",igRegnHistoryLines);

  /* +++ jim +++ 20001129: configure maximum days before TIFD to join with: */
  if (GetCfg ("GLOBAL_DEFINES", "JOIN_VALID_DAYS", CFG_STRING, pcgTmpBuf1, "-1") == 0)
  {
    dbg (TRACE, "InitGlobalConfig: JOIN_VALID_DAYS <%s>", pcgTmpBuf1);
    lgJoinValidDays = atol (pcgTmpBuf1);
  }
  else
  {
    dbg (DEBUG, "InitGlobalConfig: JOIN_VALID_DAYS not set...");
  }

  InitEventMapping ();

  InitLables ();

  dbg (TRACE, "=====================================================");
  return ilRC;
}                               /* end InitGlobalConfig */

/*******************************************************/
/*******************************************************/
static void InitEventMapping (void)
{
  int ilGetRc = RC_SUCCESS;
  char pclTmpLine[512];
  ilGetRc = GetCfg ("EVENT_MAPPING", "INTERFACES", CFG_STRING, pclTmpLine, "\0");
  if (ilGetRc == RC_SUCCESS)
  {
    dbg (TRACE, "INTERFACES <%s>", pclTmpLine);
/****  TWE 24012000  ****/
    sprintf (pcgExcoNames, ",%s,", pclTmpLine);
  }                             /* end if */
  else
  {
    pcgExcoNames[0] = 0x00;
  }
/************************/

  ilGetRc = GetCedaCfg ("EVENT_MAPPING", "READONLY_CMD", pclTmpLine, "GFR,GFRC");
  if (ilGetRc == RC_SUCCESS)
  {
    dbg (TRACE, "READONLY_CMD <%s>", pclTmpLine);
/****  TWE 24012000  ****/
    sprintf (pcgReadOnlyCmd, ",%s,", pclTmpLine);
  }                             /* end if */
  else
  {
    pcgReadOnlyCmd[0] = 0x00;
  }
/************************/

  ilGetRc = GetCedaCfg ("EVENT_MAPPING", "READONLY_WKS", pclTmpLine, "\0");
  if (ilGetRc == RC_SUCCESS)
  {
    dbg (TRACE, "READONLY_WKS <%s>", pclTmpLine);
/****  TWE 24012000  ****/
    sprintf (pcgRejectNames, ",%s,", pclTmpLine);
  }                             /* end if */
  else
  {
    pcgRejectNames[0] = 0x00;
  }
/************************/

  prgFldMapLst[0].Count = -1;
  dbg (TRACE, "INIT FIELD NAME MAPPING");
  ReadFieldMapping ("MAP_EXCO");
  if (prgFldMapLst[0].Count < 0)
  {
    prgFldMapLst[0].Count = 0;
    strcpy (prgFldMapLst[0].Name, "MAP_EXCO");
    strcpy (prgFldMapLst[0].RcvFldLst, "\0");
    strcpy (prgFldMapLst[0].UseFldLst, "\0");
    dbg (TRACE, "ALL_EXCO MAPPING NOT ACTIVATED");
  }                             /* end if */

  InitMappingLists ("MAP_WKS_NAME");
  InitMappingLists ("MAP_USR_NAME");

  dbg (TRACE, "FOUND %d MAPPING LISTS", prgFldMapLst[0].Count);

  return;
}                               /* end InitEventMapping */

/*******************************************************/
/*******************************************************/
static void InitMappingLists (char *pcpName)
{
  int ilItmCnt = 0;
  int ilItm = 0;
  int ilLen = 0;
  char pclTmpName[64];
  char pclTmpLine[512];
  GetCfg ("EVENT_MAPPING", pcpName, CFG_STRING, pclTmpLine, "\0");
  ilItmCnt = field_count (pclTmpLine);
  for (ilItm = 1; ilItm <= ilItmCnt; ilItm++)
  {
    ilLen = get_real_item (pclTmpName, pclTmpLine, ilItm);
    if (ilLen > 0)
    {
      ReadFieldMapping (pclTmpName);
    }                           /* end if */
  }                             /* end for */
  return;
}                               /* end InitMappingLists */

/*******************************************************/
/*******************************************************/
static void ReadFieldMapping (char *pcpName)
{
  int ilCnt = 0;
  char pclTmpBuf[512];
  char pclKeyName[64];
  ilCnt = prgFldMapLst[0].Count;
  sprintf (pclKeyName, "%s_RCV", pcpName);
  GetCfg ("EVENT_MAPPING", pclKeyName, CFG_STRING, pclTmpBuf, "\0");
  if (pclTmpBuf[0] != '\0')
  {
    ilCnt++;
    strcpy (prgFldMapLst[ilCnt].Name, pcpName);
    strcpy (prgFldMapLst[ilCnt].RcvFldLst, pclTmpBuf);
    dbg (TRACE, "<%s> <%s>", pclKeyName, prgFldMapLst[ilCnt].RcvFldLst);
    sprintf (pclKeyName, "%s_USE", pcpName);
    GetCfg ("EVENT_MAPPING", pclKeyName, CFG_STRING, pclTmpBuf, "\0");
    strcpy (prgFldMapLst[ilCnt].UseFldLst, pclTmpBuf);
    if (pclTmpBuf[0] != '\0')
    {
      dbg (TRACE, "<%s> <%s>", pclKeyName, prgFldMapLst[ilCnt].UseFldLst);
    }                           /* end if */
  }                             /* end if */
  prgFldMapLst[0].Count = ilCnt;
  return;
}                               /* end ReadFieldMapping */

/*******************************************************/
/*******************************************************/
static void GetPatchValues (void)
{
  int ilClntCnt = 0;
  int ilItem = 0;
  int ilClntValu = 0;
  int ilServValu = 0;
  char pclClntStrg[8];
  char pclServStrg[8];
  /* strcpy(pcgClntCharSet,"34,39,44,10,13"); */
  /* strcpy(pcgServCharSet,"23,24,25,28,29"); */
  /* Older Versions: */
  /* strcpy(pcgServCharSet,"176,177,178,180,179"); */
  GetCedaCfg ("CHAR_PATCH", "CLIENT_CHARS", pcgClntCharSet, "34,39,44,10,13");
  GetCedaCfg ("CHAR_PATCH", "SERVER_CHARS", pcgServCharSet, "176,177,178,180,179");
  dbg (TRACE, "PATCH CHR SET CLIENT <%s>", pcgClntCharSet);
  dbg (TRACE, "PATCH CHR SET SERVER <%s>", pcgServCharSet);
  cgPtCR = 179;
  cgPtLF = 180;
  ilClntCnt = field_count (pcgClntCharSet);
  (void) field_count (pcgServCharSet);
  for (ilItem = 1; ilItem <= ilClntCnt; ilItem++)
  {
    get_real_item (pclClntStrg, pcgClntCharSet, ilItem);
    get_real_item (pclServStrg, pcgServCharSet, ilItem);
    ilClntValu = atoi (pclClntStrg);
    ilServValu = atoi (pclServStrg);
    switch (ilClntValu)
    {
      case 10:
        cgPtLF = ilServValu;
        break;
      case 13:
        cgPtCR = ilServValu;
        break;
      default:
        break;
    }                           /* end switch */
  }                             /* end for */
  return;
}                               /* end GetPatchValues */

/*******************************************************/
/*******************************************************/
static int GetCfg (char *pcpGroup, char *pcpLine, short spNoOfLines, char *pcpDestBuff, char *pcpDefault)
{
  int ilRC = RC_SUCCESS;
  pcpDestBuff[0] = 0x00;
  ilRC = iGetConfigRow (pcgCfgFile, pcpGroup, pcpLine, spNoOfLines, pcpDestBuff);
  if (ilRC != RC_SUCCESS)
  {
    /* use default entry */
    strcpy (pcpDestBuff, pcpDefault);
  }                             /* end if */
  return ilRC;
}                               /* end GetCfg */

/*******************************************************/
/*******************************************************/
static int GetCedaCfg (char *pcpGroup, char *pcpLine, char *pcpDestBuff, char *pcpDefault)
{
  int ilRC = RC_SUCCESS;
  pcpDestBuff[0] = 0x00;
  ilRC = iGetConfigRow (pcgCedaCfg, pcpGroup, pcpLine, CFG_STRING, pcpDestBuff);
  if (ilRC != RC_SUCCESS)
  {
    /* use default entry */
    strcpy (pcpDestBuff, pcpDefault);
  }                             /* end if */
  return ilRC;
}                               /* end GetCedaCfg */

/*******************************************************/
/*******************************************************/
static int GetLogFileMode (int *ipModeLevel, char *pcpLine, char *pcpDefault)
{
  int ilRC;
  char pclTmp[32];
  ilRC = GetCfg ("SYSTEM", pcpLine, CFG_STRING, pclTmp, pcpDefault);
  if (strcmp (pclTmp, "TRACE") == 0)
  {
    *ipModeLevel = TRACE;
  }
  else if (strcmp (pclTmp, "DEBUG") == 0)
  {
    *ipModeLevel = DEBUG;
  }                             /* end else if */
  else
  {
    *ipModeLevel = 0;
  }                             /* end else */

  ilRC = RC_SUCCESS;
  return ilRC;
}                               /* end GetLogFileMode */

/*******************************************************/
/*******************************************************/
static int CheckSystemInit (int ipRetVal)
{
  int ilRC;
  int ilWaitTtl = 90;
  int ilWaitSec = 10;
  ilRC = ipRetVal;
  if (igConfigError == TRUE)
  {
    ilRC = RC_FAIL;
  }                             /* end if */
  if (ilRC != RC_SUCCESS)
  {
    debug_level = TRACE;
    dbg (TRACE, "CONFIGURATION ERROR DETECTED");
    do
    {
      dbg (TRACE, "SHUTDOWN IN %d SECONDS", ilWaitTtl);
      sleep (ilWaitSec);
      ilWaitTtl -= ilWaitSec;
    }
    while (ilWaitTtl > 0);
    Terminate ();
  }                             /* end if */
  return ilRC;
}                               /* end CheckSystemInit */

/*******************************************************/
/*******************************************************/
static int InitFirstActions (int ipRetVal)
{
  int ilRC;
  int ilTbl = 0;
  ilRC = ipRetVal;

  ilTbl = GetTableIndex (pcgAftTab);
  (void) BuildSqlReadFields (ilTbl, "", FOR_TABLES);
  if ((ilRC == RC_SUCCESS) && (igToRulFnd > 0))
  {
    if (mod_id == igMainModId)
    {                           /* I'm the 'flight' */
      if (ilTbl > 0)
      {
        (void) tools_send_info_flag (igToRulFnd, 0, "FLIGHT", "", "CEDA", "", "", "RMS", "HAJ,TAB,INIT", "SFL", "AFT",
                                     "SEL", prgPrjTblCfg[ilTbl].AllFldLst, "DAT", 0);
        dbg (TRACE, "SENT SFL AFT TO RULFND");
        (void) SetSendToRulFnd (0, pcgAftTab, prgPrjTblCfg[ilTbl].AllFldLst);
      }                         /* end if */
      /*
         ilTbl = GetTableIndex(pcgCcaTab);
         (void) BuildSqlReadFields(ilTbl,"", FOR_TABLES);
         if (ilTbl > 0)
         {
         (void) tools_send_info_flag(igToRulFnd,0, "FLIGHT", "", "CEDA",
         "", "", "RMS", "HAJ,TAB,INIT",
         "SFL", "CCA", "SEL",
         prgPrjTblCfg[ilTbl].AllFldLst, "DAT", 0);
         dbg(TRACE,"SENT SFL CCA TO RULFND");
         (void) SetSendToRulFnd(0,pcgCcaTab, prgPrjTblCfg[ilTbl].AllFldLst);
         }
       */
    }                           /* end if */
  }                             /* end if */

  return ilRC;
}                               /* end InitFirstActions */

/********************************************************************/
/* The Reset routine                                                */
/********************************************************************/
static int Reset ()
{
  int ilRC = RC_SUCCESS;        /* Return code */
  Terminate ();
  return ilRC;
}                               /* end of Reset */

/*************************************************************/
/* The termination routine                                   */
/*************************************************************/
static void Terminate ()
{
  logoff ();
  if (pfgLogFile != NULL)
  {
    fclose (pfgLogFile);
    pfgLogFile = NULL;
  }                             /* end if */
  dbg (TRACE, "Terminate: now leaving ...");
  exit (0);
}                               /* end of Terminate */

/*********************************************************/
/* The handle signals routine                            */
/*********************************************************/
static void HandleSignal (int pipSig)
{

  dbg (TRACE, "HandleSignal: signal <%d> received", pipSig);

  switch (pipSig)
  {
    default:
      Terminate ();
      break;
  }                             /* end of switch */

  exit (pipSig);

}                               /* end of HandleSignal */

/******************************************************************/
/* The handle general error routine                               */
/******************************************************************/
static void HandleErr (int pipErr)
{
  dbg (TRACE, "HandleErr: Error no.<%d>", pipErr);
  return;
}                               /* end of HandleErr */

/********************************************************************/
/* The handle queuing error routine                                 */
/********************************************************************/
static void HandleQueErr (int pipErr)
{

  switch (pipErr)
  {

    case QUE_E_FUNC:           /* Unknown function */
      dbg (TRACE, "<%d> : unknown function", pipErr);
      break;

    case QUE_E_MEMORY:         /* Malloc reports no memory */
      dbg (TRACE, "<%d> : malloc failed", pipErr);
      break;

    case QUE_E_SEND:           /* Error using msgsnd */
      dbg (TRACE, "<%d> : msgsnd failed", pipErr);
      break;

    case QUE_E_GET:            /* Error using msgrcv */
      dbg (TRACE, "<%d> : msgrcv failed", pipErr);
      break;

    case QUE_E_EXISTS:
      dbg (TRACE, "<%d> : route/queue already exists ", pipErr);
      break;

    case QUE_E_NOFIND:
      dbg (TRACE, "<%d> : route not found ", pipErr);
      break;

    case QUE_E_ACKUNEX:
      dbg (TRACE, "<%d> : unexpected ack received ", pipErr);
      break;

    case QUE_E_STATUS:
      dbg (TRACE, "<%d> :  unknown queue status ", pipErr);
      break;

    case QUE_E_INACTIVE:
      dbg (TRACE, "<%d> : queue is inaktive ", pipErr);
      break;

    case QUE_E_MISACK:
      dbg (TRACE, "<%d> : missing ack ", pipErr);
      break;

    case QUE_E_NOQUEUES:
      dbg (TRACE, "<%d> : queue does not exist", pipErr);
      break;

    case QUE_E_RESP:           /* No response on CREATE */
      dbg (TRACE, "<%d> : no response on create", pipErr);
      break;

    case QUE_E_FULL:
      dbg (TRACE, "<%d> : too many route destinations", pipErr);
      break;

    case QUE_E_NOMSG:          /* No message on queue */
      dbg (TRACE, "<%d> : no messages on queue", pipErr);
      break;

    case QUE_E_INVORG:         /* Mod id by que call is 0 */
      dbg (TRACE, "<%d> : invalid originator=0", pipErr);
      break;

    case QUE_E_NOINIT:         /* Queues is not initialized */
      dbg (TRACE, "<%d> : queues are not initialized", pipErr);
      break;

    case QUE_E_ITOBIG:
      dbg (TRACE, "<%d> : requestet itemsize to big ", pipErr);
      break;

    case QUE_E_BUFSIZ:
      dbg (TRACE, "<%d> : receive buffer to small ", pipErr);
      break;

    default:                   /* Unknown queue error */
      dbg (TRACE, "<%d> : unknown error", pipErr);
      break;
  }                             /* end switch */

  return;

}                               /* end of HandleQueErr */

/************************************************************/
/* The handle queues routine                                */
/************************************************************/
static void HandleQueues ()
{
  int ilRC = RC_SUCCESS;        /* Return code */
  int ilBreakOut = FALSE;

  do
  {
    ilRC = que (QUE_GETBIG, 0, mod_id, PRIORITY_3, 0, (char *) &prgItem);
    prgEvent = (EVENT *) prgItem->text;
    if (ilRC == RC_SUCCESS)
    {
      /* Acknowledge the item */
      ilRC = que (QUE_ACK, 0, mod_id, 0, 0, NULL);
      if (ilRC != RC_SUCCESS)
      {
        /* handle que_ack error */
        HandleQueErr (ilRC);
      }                         /* fi */

      switch (prgEvent->command)
      {
        case HSB_STANDBY:
          ctrl_sta = prgEvent->command;
          break;
        case HSB_COMING_UP:
          ctrl_sta = prgEvent->command;
          break;
        case HSB_ACTIVE:
          ctrl_sta = prgEvent->command;
          ilBreakOut = TRUE;
          break;
        case HSB_ACT_TO_SBY:
          ctrl_sta = prgEvent->command;
          break;
        case HSB_DOWN:
          ctrl_sta = prgEvent->command;
          Terminate ();
          break;
        case HSB_STANDALONE:
          ctrl_sta = prgEvent->command;
          ResetDBCounter ();
          ilBreakOut = TRUE;
          break;
        case REMOTE_DB:
          HandleRemoteDB (prgEvent);
          break;
        case SHUTDOWN:
          Terminate ();
          break;
        case RESET:
          ilRC = Reset ();
          break;
        case EVENT_DATA:
          dbg (TRACE, "HandleQueues: wrong hsb status <%d>", ctrl_sta);
          break;
        case TRACE_ON:
          dbg_handle_debug (prgEvent->command);
          break;
        case TRACE_OFF:
          dbg_handle_debug (prgEvent->command);
          break;
        default:
          dbg (TRACE, "HandleQueues: unknown event");
          break;
      }                         /* end switch */

      /* Handle error conditions */
      if (ilRC != RC_SUCCESS)
      {
        HandleErr (ilRC);
      }                         /* end if */
    }
    else
    {
      /* Handle queuing errors */
      HandleQueErr (ilRC);
    }                           /* end else */

  }
  while (ilBreakOut == FALSE);
  return;
}                               /* end of HandleQueues */

/*********************************************************************/
/* The handle data routine                                           */
/*********************************************************************/
static int HandleData ()
{
  int ilRC = RC_SUCCESS;
  char pclTable[16];
  char pclActCmd[16];
  char pclChkCmd[64];
  char pclTmpBuf[32];
  char pclChkWks[64];
  char pclChkUsr[64];
  char pclNumDays[10];          /* TWE */
  long llIgnoreSec;             /* TWE */
  int ilBcIfr;                  /* TWE */
  BC_HEAD *prlBchd;
  CMDBLK *prlCmdblk;
  char *pclSelKey;
  char *pclFields;
  char *pclData;
  time_t ilTp;

  igTWEUpdFlights = 0;
  tgBeginStamp = 0;
  tgEndStamp = 0;
  tgStampDiff = 0;
  igShowStampDiff = FALSE;

  igCurrentAction = FOR_NORMAL;

  que_out = prgEvent->originator;
  prlBchd = (BC_HEAD *) ((char *) prgEvent + sizeof (EVENT));
  prlCmdblk = (CMDBLK *) ((char *) prlBchd->data);
  pclSelKey = (char *) prlCmdblk->data;
  pclFields = (char *) pclSelKey + strlen (pclSelKey) + 1;
  pclData = (char *) pclFields + strlen (pclFields) + 1;

  strcpy (pcgTWEFieldsRecv, pclFields);

  if (prlBchd->rc == RC_FAIL)
  {
    return RC_SUCCESS;
  }                             /* end if */

  /* Save Global Info of CMDBLK */
  strcpy (pcgTwStart, prlCmdblk->tw_start);
  strcpy (pcgTwEnd, prlCmdblk->tw_end);
  CheckHomePort ();

  if ((prlCmdblk->obj_name[0] != '\0') && (prlCmdblk->obj_name[0] != ' '))
  {
    if (strlen (prlCmdblk->obj_name) >= 3)
    {
      strcpy (&prlCmdblk->obj_name[3], pcgTblExt);
    }                           /* end if */
  }                             /* end if */
  strcpy (pclTable, prlCmdblk->obj_name);
  strcpy (pclActCmd, prlCmdblk->command);
  str_trm_rgt (pclActCmd, " ", TRUE);

  sprintf (pclChkCmd, ",%s,", pclActCmd);

  igReorgTable = FALSE;
  if (que_out == mod_id)
  {
    dbg (TRACE, "<HandleData> EVENT FROM MYSELF <%s> <%s>", pclTable, pclActCmd);
    /* To Avoid Recursive Answers */
    que_out = 0;
    if ((strstr (",ROT,", pclChkCmd)) == NULL)
    {
      dbg (TRACE, "<HandleData> RECURSION IGNORED");
      return RC_SUCCESS;
    }                           /* end if */
    else
    {
      igReorgTable = TRUE;
      if ((strstr (pcgTwStart, ",UTC,")) != NULL)
      {
        (void) get_real_item (pclTmpBuf, pcgTwStart, 3);
        igUtcDiff = atoi (pclTmpBuf);
        dbg (TRACE, "<HandleData> UTC TIME CORRECTION (%d) ACTIVATED", igUtcDiff);
      }                         /* end if */
    }                           /* end if */
  }                             /* end if */

  /* Save Global Elements of BC_HEAD */
  /* WorkStation */
  memset (pcgRecvName, '\0', (sizeof (prlBchd->recv_name) + 1));
  strncpy (pcgRecvName, prlBchd->recv_name, sizeof (prlBchd->recv_name));

  /* User */
  memset (pcgDestName, '\0', (sizeof (prlBchd->dest_name) + 1));
  strcpy (pcgDestName, prlBchd->dest_name); /* UserLoginName */

  sprintf (pclChkWks, ",%s,", pcgRecvName);
  sprintf (pclChkUsr, ",%s,", pcgDestName);

  /* DebugPrintBchead(TRACE,prlBchd); */
  /* DebugPrintCmdblk(TRACE,prlCmdblk); */

  dbg (TRACE, "CMD <%s> QUE (%d) WKS <%s> USR <%s>", pclActCmd, que_out, pcgRecvName, pcgDestName);
  dbg (TRACE, "SPECIAL INFO TWS <%s> TWE <%s>", pcgTwStart, pcgTwEnd);

  CheckTimeOut (TIMO_INIT, prlBchd->orig_name);

  if (prlBchd->rc == NETOUT_NO_ACK)
  {
    igGlNoAck = TRUE;
    dbg (DEBUG, "NO ANSWER EXPECTED (NO_ACK IS TRUE)");
  }                             /* end if */
  else
  {
    igGlNoAck = FALSE;
    dbg (DEBUG, "SENDER EXPECTS ANSWER (NO_ACK IS FALSE)");
  }                             /* end else */

  if (pfgLogFile != NULL)
  {
    fprintf (pfgLogFile, "FlightTimeStamp:-> %s\n", pcgCurTime);
    fprintf (pfgLogFile, "TBL:-> %s\n", pclTable);
    fprintf (pfgLogFile, "CMD:-> %s\n", pclActCmd);
    fprintf (pfgLogFile, "WKS:-> %s\n", pcgRecvName);
    fprintf (pfgLogFile, "USR:-> %s\n", pcgDestName);
    fprintf (pfgLogFile, "TWS:-> %s\n", pcgTwStart);
    fprintf (pfgLogFile, "TWE:-> %s\n", pcgTwEnd);
    fprintf (pfgLogFile, "FLD:-> %s\n", pclFields);
    fprintf (pfgLogFile, "DAT:-> %s\n", pclData);
    fprintf (pfgLogFile, "KEY:-> %s\n", pclSelKey);
    fprintf (pfgLogFile, "END:-> %d\n\n", mod_id);
    fflush (pfgLogFile);
  }                             /* end if */

  if (igRuntimeMode == igAutoMode)
  {
    if ((strstr (pcgOffModes, pclChkCmd)) != NULL)
    {
      debug_level = 0;
    }                           /* end if */
    if ((strstr (pcgTraceModes, pclChkCmd)) != NULL)
    {
      debug_level = TRACE;
    }                           /* end if */
    if ((strstr (pcgDebugModes, pclChkCmd)) != NULL)
    {
      debug_level = DEBUG;
    }                           /* end if */
    if ((strstr (pcgWksModes, pclChkWks)) != NULL)
    {
      debug_level = TRACE;
    }                           /* end if */
    if ((strstr (pcgUsrModes, pclChkUsr)) != NULL)
    {
      debug_level = TRACE;
    }                           /* end if */
  }                             /* end if */

  ilRC = ResetDataBuffer (__LINE__ ,TRUE, 0, 0, 0, 0, 0, 0);

  /* Reset ErrorCounter */
  igErrorCount = 0;
  igCheckFlightKey = TRUE;
  igCheckFkeyFtyp = FALSE;
  igDeleteDoubles = FALSE;
  igImportAction = FALSE;
  igReopFlight = FALSE;
  igBroadCast = TRUE;
  igChangeInfo = TRUE;
  igUnlockDssf = FALSE;
  igSitaChecked = FALSE;
  igRegnLineTotal = 0;
/* TWE 13012000 */
  igTWEForwardAll = FALSE;
  igTWENewFlnoFlight = FALSE;
  igTotalFlights = 0;
/****************/

  strcpy (pcgRegnList, ",");
  strcpy (pcgRkeyList, ",");

  igClientAction = TRUE;
  if ((strstr (pcgRecvName, "EXCO") != NULL) || (strstr (pcgRecvName, "DEFAULT") != NULL) ||
      (strcmp (pcgRecvName, "SITA") == 0) || (strcmp (pcgDestName, "SITA") == 0) ||
      (strstr (pcgExcoNames, pclChkWks) != NULL) || (strstr (pcgExcoNames, pclChkUsr) != NULL))
  {
    igClientAction = FALSE;
  }                             /* end if */

  if (strstr (pcgReadOnlyCmd, pclChkCmd) == NULL)
  {
    if ((strstr (pcgRejectNames, pclChkWks) != NULL) || (strstr (pcgRejectNames, pclChkUsr) != NULL))
    {
      strcpy (pclActCmd, "REV");
    }                           /* end if */
  }                             /* end if */

  CheckPerformance (TRUE, prlBchd->orig_name);
  (void) CheckTimeOut (TIMO_CHECK, "");

  if (strcmp (pclActCmd, "ISF") == 0)
  {
    ilRC = InsertFlights (pclTable, pclFields, pclData, pclSelKey, "0", TRUE);
  }
  else if (strcmp (pclActCmd, "ISFAZ") == 0)
  {
    igTWEFromAZFlight = TRUE;
    ilRC = InsertFlights (pclTable, pclFields, pclData, pclSelKey, "0", TRUE);
    igTWEFromAZFlight = FALSE;
  }
  else if (strcmp (pclActCmd, "UFR") == 0)
  {
    ilRC = UpdateFlights (pclTable, pclFields, pclData, pclSelKey, TRUE);
  }
  else if (strcmp (pclActCmd, "UFRAZ") == 0)
  {
    igTWEFromAZFlight = TRUE;
    ilRC = UpdateFlights (pclTable, pclFields, pclData, pclSelKey, TRUE);
    igTWEFromAZFlight = FALSE;
  }
  else if (strcmp (pclActCmd, "ROT") == 0)
  {
    ilRC = UpdateFlights (pclTable, pclFields, pclData, pclSelKey, FALSE);
  }
  else if (strcmp (pclActCmd, "GFRC") == 0)
  {
    ilRC = HandleGetFlightRecord ("\r\n", NULL, NULL, NULL, NULL);
  }
  else if (strcmp (pclActCmd, "GFR") == 0)
  {
    ilRC = HandleGetFlightRecord ("\n", NULL, NULL, NULL, NULL);
  }
  else if (strcmp (pclActCmd, "CGFR") == 0)
  {
/* This is special command for coverage application (Swissport) */
    ilRC = HandleGetFlightRecord ("\n", NULL, NULL, NULL, NULL);
  }
  else if (strcmp (pclActCmd, "GDF") == 0)
  {
    ilRC = HandleGetDeletedFlights (); /* not yet implemented */
  }
  else if (strcmp (pclActCmd, "DFR") == 0)
  {
    ilRC = HandleDeleteFlightRecord ();
  }
  else if (strcmp (pclActCmd, "DFRAZ") == 0)
  {
    igTWEFromAZFlight = TRUE;
    ilRC = HandleDeleteFlightRecord ();
    igTWEFromAZFlight = FALSE;
  }
  else if (strcmp (pclActCmd, "SPR") == 0)
  {
    igTWESplit = TRUE;
    ilRC = SplitFlights (pclTable, pclFields, pclData, pclSelKey);
    igTWESplit = FALSE;
  }
  else if (strcmp (pclActCmd, "JOF") == 0)
  {
    igTWESplit = TRUE;
    ilRC = JoinFlights (pclTable, pclFields, pclData, pclSelKey, TRUE);
    igTWESplit = FALSE;
  }
  else if (strcmp (pclActCmd, "CGM") == 0)
  {
    ilRC = HandleCreateGroundMovement ();
  }
  else if (strcmp (pclActCmd, "DGM") == 0)
  {
    ilRC = HandleDeleteGroundMovement ();
  }
  else if (strcmp (pclActCmd, "EXCO") == 0)
  {
    ilRC = HandleExcoData (pclTable, pclFields, pclData, pclSelKey);
  }
  else if (strcmp (pclActCmd, "AFI") == 0)
  {
    ilRC = FlightDataInput (pclTable, pclFields, pclData, pclSelKey, FOR_NORMAL);
  }
  else if (strcmp (pclActCmd, "FSI") == 0)
  {
    ilRC = HandleImportData (pclTable, pclFields, pclData, pclSelKey, FOR_INSERT);
    /* +++ jim +++ 20000827 TEST BEGIN: InsertFlights statt HandleImportData  ! */
  }
  else if (strcmp (pclActCmd, "TWEIM") == 0)
  {
    igJIMCheckRkeys = TRUE;
    igTWEImport = TRUE;
    igTWEUpdate = TRUE;
    dbg (TRACE, "HandleData: TWEIM calling HandleImportData...");
    ilRC = HandleImportData (pclTable, pclFields, pclData, pclSelKey, FOR_INSERT);
    igJIMCheckRkeys = FALSE;
    igTWEImport = FALSE;
    igTWEUpdate = FALSE;
    /* +++ jim +++ 20000827 TEST ENDE : InsertFlights statt HandleImportData  ! */
  }
  else if (strcmp (pclActCmd, "FSU") == 0)
  {
    ilRC = HandleImportData (pclTable, pclFields, pclData, pclSelKey, FOR_UPDATE);
  }
  else if (strcmp (pclActCmd, "CSI") == 0)
  {
    igDontCallSql = TRUE;
    ilRC = HandleImportData (pclTable, pclFields, pclData, pclSelKey, FOR_INSERT);
    igDontCallSql = FALSE;
  }
  else if (strcmp (pclActCmd, "CSU") == 0)
  {
    igDontCallSql = TRUE;
    ilRC = HandleImportData (pclTable, pclFields, pclData, pclSelKey, FOR_UPDATE);
    igDontCallSql = FALSE;
  }
  else if (strcmp (pclActCmd, "SFL") == 0)
  {
    ilRC = SetSendToRulFnd (TRACE, pclTable, pclFields);
  }
  else if (strcmp (pclActCmd, "REV") == 0)
  {
    ilRC = RejectEvent (prlCmdblk->command, pclTable, pclFields, pclData, pclSelKey);
  }
  else if (strcmp (pclActCmd, "TSCH") == 0)
  {
    time (&ilTp);
    dbg (TRACE, "RECVD TSCH FROM %d AT %s", que_out, asctime (gmtime (&ilTp)));
    CleanUpFlightTables ();
    /* Attention: Never Answer New Timsch */
    /* ilRC = new_tools_send_sql(que_out,prlBchd,prlCmdblk,"",""); */
  }
 /********   TWE  211299  ***********/
  else if (strcmp (pclActCmd, "FDI") == 0)
  {
    ilBcIfr = igBcIfr;
    igBcIfr = FALSE;          /* set to no broadcasts  */
    ilRC = iGetConfigRow (pcgCfgFile, "IGNORE_FLIPS_TIME", "IGNORE_DAY_NUM", CFG_STRING, pclNumDays);
    if (ilRC == RC_SUCCESS)
    {
      igTWEIgnoreDays = atoi (pclNumDays); /* 2 = today & tomorrow */
      if (igTWEIgnoreDays < 0)
      {
        pcgBorderTime[0] = 0x00;
        igTWEIgnoreDays = -1;
      }
      else
      {
        GetServerTimeStamp ("UTC", 1, 0, pcgBorderTime);
        llIgnoreSec = igTWEIgnoreDays * 24 * 60 * 60;
        ilRC = AddSecondsToCEDATime2 (pcgBorderTime, llIgnoreSec, pcgStampType, 1);
        if (ilRC == RC_SUCCESS)
        {
          memcpy (pcgBorderTime + 8, "000000", 6);
        }
        else
        {
          dbg (TRACE, "HandleData: ERROR:AddSecondsToCEDATime2 <%s> + %ld", pcgBorderTime, llIgnoreSec);
          igTWEIgnoreDays = -1;
          pcgBorderTime[0] = 0x00;
        }
      }
    }                         /* end if */
    else
    {
      igTWEIgnoreDays = -1;
      pcgBorderTime[0] = 0x00;
    }

    if (strstr (pcgTwStart, "FWD =YES") != NULL)
    {
      dbg (TRACE, "RECEIVED FORWARD TO ALL");
      igTWEForwardAll = TRUE;
      igBcIfr = ilBcIfr;      /* set to configentry */
    }
    igPOPS = TRUE;
    ilRC = TWEHandlePOPSData (pclTable, pclFields, pclData, pclSelKey);
    igPOPS = FALSE;;
    igBcIfr = ilBcIfr;          /* reset Broadcastflag from Config */
  }
 /*******************************/
  else
  {
    dbg (TRACE, "UNKNOWN COMMAND RECEIVED");
    dbg (TRACE, "=== DATA ELEMENTS FROM ORIGINATOR =======");
    dbg (TRACE, "... TABLE  <%s>", pclTable);
    /* dbg(TRACE,"... FIELDS <%s>", pclFields); */
    /* dbg(TRACE,"... DATA   <%s>", pclData); */
    dbg (TRACE, "... SELECT <%s>", pclSelKey);
    dbg (TRACE, "=========================================");
  }                             /* end if else */

  if (ilRC != RC_SUCCESS)
  {
    /* Handle Special Errors */
    /* dbg(TRACE,"FINAL RESULT: NOTHING TO DO"); */
    ilRC = RC_SUCCESS;
  }                             /* end if */

  CheckAutoBarShift ();

  if (strlen (pcgRegnList) > 1)
  {
    CheckRegnHistory (pcgRegnList);
  }                             /* end if */

  /* For RuleFinder */
  if (strlen (pcgRkeyList) > 1)
  {
    CheckRkeyHistory ();
  }                           /* end if */

/*********************************/

  CheckPerformance (FALSE, "\0");
  if (igShowStampDiff == TRUE)
  {
    dbg (TRACE, "CMD <%s> QUE (%d) WKS <%s> USR <%s>", pclActCmd, que_out, pcgRecvName, pcgDestName);
    dbg (TRACE, "SPECIAL INFO TWS <%s> TWE <%s>", pcgTwStart, pcgTwEnd);
    dbg (TRACE, "TABLE  <%s>", pclTable);
    dbg (TRACE, "FIELDS <%s>", pclFields);
    dbg (TRACE, "DATA   <%s>", pclData);
    dbg (TRACE, "SELECT <%s>", pclSelKey);
    dbg (TRACE, "=========================================");
  }                             /* end if */

  if (igActResultSize > I_RESULTBUFSTEP)
  {
    debug_level = TRACE;
    dbg (TRACE, "=========================================");
    dbg (TRACE, "TERMINATING DUE TO SIZE OF MEMORY BLOCKS (%d)", igActResultSize);
    dbg (TRACE, "LAST EVENT:");
    dbg (TRACE, "CMD <%s> QUE (%d) WKS <%s> USR <%s>", pclActCmd, que_out, pcgRecvName, pcgDestName);
    dbg (TRACE, "SPECIAL INFO TWS <%s> TWE <%s>", pcgTwStart, pcgTwEnd);
    dbg (TRACE, "=== DATA ELEMENTS FROM ORIGINATOR =======");
    dbg (TRACE, "... TABLE  <%s>", pclTable);
    dbg (TRACE, "... FIELDS <%s>", pclFields);
    dbg (TRACE, "... DATA   <%s>", pclData);
    dbg (TRACE, "... SELECT <%s>", pclSelKey);
    dbg (TRACE, "=========================================");
    Terminate ();
  }                             /* end if */

  debug_level = igRuntimeMode;
  return ilRC;
}                               /* end of HandleData */

/* *************************************************** */
/* *************************************************** */
static int HandleImportData (char *pcpRcvTable, char *pcpFields, char *pcpData, char *pcpSelKey, int ipImportType)
{
  int ilRC = RC_SUCCESS;
  int ilLinCnt = 0;
  int ilBreakOut = FALSE;
  int ilFldItmCnt = 0;
  int ilDatItmCnt = 0;
  int ilItm = 0;
  int ilArrFldPos = 0;
  int ilDepFldPos = 0;
  int ilArrDatPos = 0;
  int ilDepDatPos = 0;
  int ilRotaType = 0;
  int ilItmLen = 0;
  int ilFltn = 0;
  int ilFld = 0;
  char pclDbgTxt[16];
  char pclTable[16];
  char pclUpdDay[16];
  char pclSelDay[16];
  char pclSelMon[16];
  char pclTmpBuf1[32];
  char pclTmpBuf2[32];
  char pclArrFkey[32];
  char pclDepFkey[32];
  char pclArrFldLst[2048];
  char pclDepFldLst[2048];
  char pclArrFldDat[2048];
  char pclDepFldDat[2048];
  char pclFldNam[16];
  char pclFldDat[2048];
  char pclChkNam[16];
  char pclVpfrDat[16];
  char pclVptoDat[16];
  char pclFredDat[16];
  char pclFreqDat[16];
  char pclDaofDat[16];
  char pclOutSel[128];
  char pclOutFld[2048];
  char pclOutDat[4096];
  char pclSpecFlds[] = "VPFR,VPTO,FRED,FREQ,DAOF";
  char *pclLinBgn = NULL;
  char *pclLinEnd = NULL;

  sprintf (pclTable, "AFT%s", pcgTblExt);
  if (ipImportType == FOR_INSERT)
  {
    strcpy (pclDbgTxt, "FSI");
    igCheckFlightKey = TRUE;
    igCheckFkeyFtyp = TRUE;
    igDeleteDoubles = TRUE;
    igImportAction = TRUE;
    igClientAction = FALSE;
    igBroadCast = FALSE;
    strcpy (pcgTwStart, "14");
  }                             /* end if */
  else
  {
    strcpy (pclDbgTxt, "FSU");
    (void) GetWord (2, pcpSelKey, pclSelDay);
    (void) GetWord (3, pcpSelKey, pclSelMon);
    sprintf (pclTmpBuf1, "%s%s", pclSelDay, pclSelMon);
    (void) FlucoPeriodToCeda (pclUpdDay, pclTmpBuf2, pclTmpBuf1, "");
  }                             /* end else */

   TRACE_FIELD_OF_INTEREST(__LINE__,"40");

  dbg (TRACE, "===== DATA ELEMENTS FROM INTERFACE =====");
  dbg (DEBUG, "%s TABLE  <%s>", pclDbgTxt, pcpRcvTable);
  dbg (TRACE, "%s FIELDS <%s>", pclDbgTxt, pcpFields);
  dbg (DEBUG, "%s DATA   <%s>", pclDbgTxt, pcpData);
  dbg (TRACE, "%s SELECT <%s>", pclDbgTxt, pcpSelKey);
  dbg (TRACE, "========================================");

  igJIMRotMaxRec = 0;
  ilFldItmCnt = field_count (pcpFields);

  strcpy (pclFreqDat, "1");

  ilBreakOut = FALSE;
  ilLinCnt = 0;
  pclLinBgn = pcpData;
  do
  {
    pclLinEnd = strstr (pclLinBgn, "\n");
    if (pclLinEnd != NULL)
    {
      *pclLinEnd = 0x00;
    }                           /* end if */

    ilLinCnt++;
    ilDatItmCnt = field_count (pclLinBgn);
    if (ilDatItmCnt == ilFldItmCnt)
    {
      dbg (TRACE, "==== LINE %d ==== TOTAL FLIGHTS %d ====\n<%s>", ilLinCnt, igTotalFlights, pclLinBgn);
      ilArrFldPos = 0;
      ilDepFldPos = 0;
      ilArrDatPos = 0;
      ilDepDatPos = 0;
      strcpy (pclDaofDat, "0");
      for (ilItm = 1; ilItm <= ilFldItmCnt; ilItm++)
      {
        (void) get_real_item (pclFldNam, pcpFields, ilItm);
        (void) get_real_item (pclFldDat, pclLinBgn, ilItm);
        strcpy (pclChkNam, pclFldNam);
        pclChkNam[4] = 0x00;
        if (strstr (pclSpecFlds, pclChkNam) == NULL)
        {
          switch (pclFldNam[4])
          {
            case 'A':
              StrgPutStrg (pclArrFldLst, &ilArrFldPos, pclFldNam, 0, 3, ",");
              StrgPutStrg (pclArrFldDat, &ilArrDatPos, pclFldDat, 0, -1, ",");
              break;
            case 'B':
              StrgPutStrg (pclArrFldLst, &ilArrFldPos, pclFldNam, 0, 3, ",");
              StrgPutStrg (pclArrFldDat, &ilArrDatPos, pclFldDat, 0, -1, ",");
              StrgPutStrg (pclDepFldLst, &ilDepFldPos, pclFldNam, 0, 3, ",");
              StrgPutStrg (pclDepFldDat, &ilDepDatPos, pclFldDat, 0, -1, ",");
              break;
            case 'D':
              StrgPutStrg (pclDepFldLst, &ilDepFldPos, pclFldNam, 0, 3, ",");
              StrgPutStrg (pclDepFldDat, &ilDepDatPos, pclFldDat, 0, -1, ",");
              break;
            default:
              break;
          }                     /* end switch */
        }                       /* end if */
        else
        {
          ilFld = get_item_no (pclSpecFlds, pclChkNam, 5) + 1;
          if (ilFld > 0)
          {
            switch (ilFld)
            {
              case 1:          /* VPFR */
                strcpy (pclVpfrDat, pclFldDat);
                break;
              case 2:          /* VPTO */
                strcpy (pclVptoDat, pclFldDat);
                break;
              case 3:          /* FRED */
                strcpy (pclFredDat, pclFldDat);
                break;
              case 4:          /* FREQ */
                strcpy (pclFreqDat, pclFldDat);
                break;
              case 5:          /* DAOF */
                strcpy (pclDaofDat, pclFldDat);
                break;
              default:
                break;
            }                   /* end switch */
          }                     /* end if */
        }                       /* end else */
      }                         /* end for */
      pclArrFldLst[ilArrFldPos] = 0x00;
      pclDepFldLst[ilDepFldPos] = 0x00;

      if ((pclFreqDat[0] < '0') || (pclFreqDat[0] > '9'))
      {
        strcpy (pclFreqDat, "1");
      }                         /* end if */

      if (ipImportType == FOR_INSERT)
      {
        if (strstr (pclArrFldLst, "DES3") == NULL)
        {
          StrgPutStrg (pclArrFldLst, &ilArrFldPos, "DES3", 0, -1, ",");
          StrgPutStrg (pclArrFldDat, &ilArrDatPos, pcgH3LC, 0, -1, ",");
          pclArrFldLst[ilArrFldPos] = 0x00;
        }                       /* end if */
        if (strstr (pclArrFldLst, "DES4") == NULL)
        {
          StrgPutStrg (pclArrFldLst, &ilArrFldPos, "DES4", 0, -1, ",");
          StrgPutStrg (pclArrFldDat, &ilArrDatPos, pcgH4LC, 0, -1, ",");
          pclArrFldLst[ilArrFldPos] = 0x00;
        }                       /* end if */

        if (strstr (pclDepFldLst, "ORG3") == NULL)
        {
          StrgPutStrg (pclDepFldLst, &ilDepFldPos, "ORG3", 0, -1, ",");
          StrgPutStrg (pclDepFldDat, &ilDepDatPos, pcgH3LC, 0, -1, ",");
          pclDepFldLst[ilDepFldPos] = 0x00;
        }                       /* end if */
        if (strstr (pclDepFldLst, "ORG4") == NULL)
        {
          StrgPutStrg (pclDepFldLst, &ilDepFldPos, "ORG4", 0, -1, ",");
          StrgPutStrg (pclDepFldDat, &ilDepDatPos, pcgH4LC, 0, -1, ",");
          pclDepFldLst[ilDepFldPos] = 0x00;
        }                       /* end if */
      }                         /* end if */

      if (ipImportType == FOR_INSERT)
      {
        ilItmLen = get_real_item (pclFldNam, pcpSelKey, 1);
        if (strcmp (pclFldNam, "FTYP") == 0)
        {
          ilItmLen = get_real_item (pclFldDat, pcpSelKey, 2);
          if (ilItmLen != 1)
          {
            strcpy (pclFldDat, "S");
          }                     /* end if */
          StrgPutStrg (pclArrFldLst, &ilArrFldPos, pclFldNam, 0, -1, ",");
          StrgPutStrg (pclArrFldDat, &ilArrDatPos, pclFldDat, 0, -1, ",");
          StrgPutStrg (pclDepFldLst, &ilDepFldPos, pclFldNam, 0, -1, ",");
          StrgPutStrg (pclDepFldDat, &ilDepDatPos, pclFldDat, 0, -1, ",");
        }                       /* end if */
        else
        {
          if (strstr (pclArrFldLst, "FTYP") == NULL)
          {
            StrgPutStrg (pclArrFldLst, &ilArrFldPos, "FTYP", 0, -1, ",");
            StrgPutStrg (pclArrFldDat, &ilArrDatPos, "S", 0, -1, ",");
          }                     /* end if */
          if (strstr (pclDepFldLst, "FTYP") == NULL)
          {
            StrgPutStrg (pclDepFldLst, &ilDepFldPos, "FTYP", 0, -1, ",");
            StrgPutStrg (pclDepFldDat, &ilDepDatPos, "S", 0, -1, ",");
          }                     /* end if */
        }                       /* end else */
      }                         /* end if */

      ilArrFldPos--;
      pclArrFldLst[ilArrFldPos] = 0x00;
      ilDepFldPos--;
      pclDepFldLst[ilDepFldPos] = 0x00;
      ilArrDatPos--;
      pclArrFldDat[ilArrDatPos] = 0x00;
      ilDepDatPos--;
      pclDepFldDat[ilDepDatPos] = 0x00;

      /*
         dbg(DEBUG,"----------------------------------------");
         dbg(DEBUG,"ARR FLD <%s>",pclArrFldLst);
         dbg(DEBUG,"ARR DAT <%s>",pclArrFldDat);
         dbg(DEBUG,"----------------------------------------");
         dbg(DEBUG,"DEP FLD <%s>",pclDepFldLst);
         dbg(DEBUG,"DEP DAT <%s>",pclDepFldDat);
         dbg(DEBUG,"----------------------------------------");
       */

      ilRotaType = 0;
      ilFltn = get_item_no (pclArrFldLst, "FLTN", 5) + 1;
      if (ilFltn > 0)
      {
        ilItmLen = get_real_item (pclFldDat, pclArrFldDat, ilFltn);
        if (ilItmLen > 0)
        {
          ilRotaType += 1;
        }                       /* end if */
      }                         /* end if */
      ilFltn = get_item_no (pclDepFldLst, "FLTN", 5) + 1;
      if (ilFltn > 0)
      {
        ilItmLen = get_real_item (pclFldDat, pclDepFldDat, ilFltn);
        if (ilItmLen > 0)
        {
          ilRotaType += 2;
        }                       /* end if */
      }                         /* end if */

      if (ipImportType == FOR_INSERT)
      {
        switch (ilRotaType)
        {
          case 0:
            dbg (TRACE, "NO FLIGHTS FOUND IN DATA LINE");
            break;
          case 1:
            dbg(DEBUG,"Line %ld: IT'S AN INBOUND GROUNDSTOP",__LINE__); 
            sprintf (pclOutSel, "WHERE %s,%s,%s,%s,%s, , ,A", pclVpfrDat, pclVptoDat, pclFredDat, pclFreqDat,
                     pclVpfrDat);
            sprintf (pclOutFld, "%s", pclArrFldLst);
            sprintf (pclOutDat, "%s", pclArrFldDat);
            break;
          case 2:
            dbg(DEBUG,"Line %ld: IT'S AN OUTBOUND GROUNDSTOP",__LINE__);
            sprintf (pclOutFld, "%s", pclDepFldLst);
            sprintf (pclOutDat, "%s", pclDepFldDat);
            sprintf (pclOutSel, "WHERE %s,%s,%s,%s,%s, , ,D", pclVpfrDat, pclVptoDat, pclFredDat, pclFreqDat,
                     pclVpfrDat);
            break;
          case 3:
            dbg(DEBUG,"Line %ld: IT'S A ROTATION",__LINE__);
            sprintf (pclOutFld, "%s\n%s", pclArrFldLst, pclDepFldLst);
            sprintf (pclOutDat, "%s\n%s", pclArrFldDat, pclDepFldDat);
            sprintf (pclOutSel, "WHERE %s,%s,%s,%s,%s,%s,%s,AD", pclVpfrDat, pclVptoDat, pclFredDat, pclFredDat,
                     pclFreqDat, pclVpfrDat, pclVpfrDat);
            break;
          default:
            break;
        }                       /* end switch */
        if (ilRotaType > 0)
        {

   TRACE_FIELD_OF_INTEREST(__LINE__,"41");

          ilRC = ResetDataBuffer (__LINE__ ,TRUE, 0, 0, 0, 0, 0, 0);

   TRACE_FIELD_OF_INTEREST(__LINE__,"42");

          igErrorCount = 0;
          if ((pclDaofDat[0] < '0') || (pclDaofDat[0] > '9'))
          {
            strcpy (pclDaofDat, "0");
          }                     /* end if */
          ilRC = InsertFlights (pclTable, pclOutFld, pclOutDat, pclOutSel, pclDaofDat, FALSE);
        }                       /* end if */
      }                         /* end if */

      else if (ipImportType == FOR_UPDATE)
      {
        if (ilRotaType > 0)
        {
          (void) BuildUpdateValues (pclUpdDay, "A", pclArrFldLst, pclArrFldDat, pclArrFkey);
          (void) BuildUpdateValues (pclUpdDay, "D", pclDepFldLst, pclDepFldDat, pclDepFkey);
        }                       /* end if */

        if (ilRotaType == 3)
        {
          /* dbg(DEBUG,"(ROTATION) CHECK JOINED FLIGHTRECORDS"); */
          (void) CheckFlightRotation (pclArrFkey, pclDepFkey);
        }                       /* end if */

        /* DAOF is a flag for the operation day of the flights */
        /* The meaning here is: */
        /* X = Arrival Flight is on a previous day */
        /* 1 = Departure Flight is on the next day */
        /* 0 = Both Flights on current day */

        if ((ilRotaType == 1) || (ilRotaType == 3))
        {
          /* dbg(DEBUG,"(INBOUND OR ROTATION) UPDATE ARRIVAL FLIGHT"); */
          if ((pclDaofDat[0] == '0') || (pclDaofDat[0] == '1'))
          {
            dbg (TRACE, "NOW UPDATING ARRIVAL");
            ilRC = ResetDataBuffer (__LINE__ ,TRUE, 0, 0, 0, 0, 0, 0);
            igErrorCount = 0;
            sprintf (pclOutSel, "WHERE FKEY='%s'", pclArrFkey);
            ilRC = UpdateFlights (pcgAftTab, pclArrFldLst, pclArrFldDat, pclOutSel, FALSE);
          }                     /* end if */
        }                       /* end if */
        if ((ilRotaType == 2) || (ilRotaType == 3))
        {
          /* dbg(DEBUG,"(OUTBOUND OR ROTATION) UPDATE DEPARTURE FLIGHT"); */
          if ((pclDaofDat[0] == '0') || (pclDaofDat[0] == 'X'))
          {
            dbg (TRACE, "NOW UPDATING DEPARTURE");
            ilRC = ResetDataBuffer (__LINE__ ,TRUE, 0, 0, 0, 0, 0, 0);
            igErrorCount = 0;
            sprintf (pclOutSel, "WHERE FKEY='%s'", pclDepFkey);
            ilRC = UpdateFlights (pcgAftTab, pclDepFldLst, pclDepFldDat, pclOutSel, FALSE);
          }                     /* end if */
        }                       /* end if */

      }                         /* end else if */
    }                           /* end if */
    else
    {
      dbg (TRACE, "ERROR: DATA ITEMS NOT FITTING TO FIELDS");
      dbg (TRACE, "LINE %d\n<%s>", ilLinCnt, pclLinBgn);
    }                           /* end else */

    if (pclLinEnd != NULL)
    {
      pclLinBgn = pclLinEnd + 1;
    }                           /* end if */
    else
    {
      ilBreakOut = TRUE;
    }                           /* end else */
  }
  while (ilBreakOut == FALSE);

  dbg (TRACE, "----------------------------------------");
  dbg (TRACE, "TOTAL LINES = %d / FLIGHTS = %d", ilLinCnt, igTotalFlights);
  dbg (TRACE, "----------------------------------------");

  return ilRC;
}                               /* end HandleImportData */

/* *************************************************** */
/* *************************************************** */
static int FlightDataInput (char *pcpRcvTable, char *pcpFields, char *pcpData, char *pcpSelKey, int ipImportType)
{
  int ilRC = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  int ilTbl = 0;
  int ilLen = 0;
  int ilDataType = 0;
  int ilFoundArr = FALSE;
  int ilFoundDep = FALSE;
  int ilRcvFldCnt = 0;
  int ilRcvFldNbr = 0;
  int ilFldNamLen = 0;
  int ilFldDatLen = 0;
  int ilArrFldPos = 0;
  int ilArrDatPos = 0;
  int ilDepFldPos = 0;
  int ilDepDatPos = 0;
  int ilIntFldPos = 0;
  int ilIntDatPos = 0;
  int ilErrFldPos = 0;
  int ilErrDatPos = 0;
  int ilErrMsgPos = 0;
  int ilCurLstTyp = 0;
  int ilActLstTyp = 0;
  int ilChkFld = 0;
  int ilOrg = 0;                /* +++ jim +++ 20000811 */
  int ilDst = 0;                /* +++ jim +++ 20000811 */
  int ilLenOrg = 0;             /* +++ jim +++ 20000811 */
  int ilLenDst = 0;             /* +++ jim +++ 20000811 */
  int ilRoutType = 0;
  char *pclFldLstBgn = NULL;
  char *pclFldLstEnd = NULL;
  char *pclDatLstBgn = NULL;
  char *pclDatLstEnd = NULL;
  char *pclKeyLstBgn = NULL;
  char *pclKeyLstEnd = NULL;
  char pclRcvFldLst[2048];
  char pclRcvKeyLst[128];
  char pclActCmdKey[16];
  char pclChkFldNam[16];
  char pclRcvFldNam[16];
  char pclRcvFldDat[2048];
  char pclArrFldLst[2048];
  char pclDepFldLst[2048];
  char pclArrDatLst[4096];
  char pclDepDatLst[4096];
  char pclIntFldLst[2048];
  char pclIntDatLst[4096];
  char pclErrFldLst[2048];
  char pclErrDatLst[4096];
  char pclErrMsgLst[4096];
  char pclAftFldLst[4096];
  char pclArrSelKey[256];
  char pclAddKey[256];
  char pclArrUrno[16];
  char pclArrRkey[16];
  char pclDepSelKey[256];
  char pclDepUrno[16];
  char pclDepRkey[16];
  char pclTable[16];
  char pclOrg[16];
  char pclDst[16];
  char pclSpecFlds[] = "ADID";

  dbg (TRACE, "==== AFI DATA =====");
  dbg (TRACE, "TABLE  <%s>", pcpRcvTable);
  dbg (TRACE, "FIELDS <%s>", pcpFields);
  dbg (TRACE, "DATA   <%s>", pcpData);
  dbg (TRACE, "SELECT <%s>", pcpSelKey);
  dbg (TRACE, "===================");

  ilTbl = GetTableIndex (pcgAftTab);
  /* (void) BuildSqlReadFields(ilTbl,"", FOR_TABLES); */
  sprintf (pclAftFldLst, ",%s,", prgPrjTblCfg[ilTbl].AllFldLst);

  pclRcvFldLst[0] = 0x00;
  pclRcvKeyLst[0] = 0x00;
  pclDatLstBgn = pcpData;
  pclDatLstEnd = NULL;
  pclFldLstBgn = pcpFields;
  pclFldLstEnd = NULL;
  pclKeyLstBgn = pcpSelKey;
  pclKeyLstEnd = NULL;

  do
  {
    pclDatLstEnd = strstr (pclDatLstBgn, "\n");
    if (pclDatLstEnd != NULL)
    {
      *pclDatLstEnd = 0x00;
    }                           /* end if */

    pclFldLstEnd = strstr (pclFldLstBgn, "\n");
    if (pclFldLstEnd != NULL)
    {
      *pclFldLstEnd = 0x00;
    }                           /* end if */

    pclKeyLstEnd = strstr (pclKeyLstBgn, "\n");
    if (pclKeyLstEnd != NULL)
    {
      *pclKeyLstEnd = 0x00;
    }                           /* end if */

    if ((strlen (pclFldLstBgn) > 3) && (pclFldLstBgn[0] != ' '))
    {
      strcpy (pclRcvFldLst, pclFldLstBgn);
    }                           /* end if */

    if ((strlen (pclKeyLstBgn) > 2) && (pclKeyLstBgn[0] != ' '))
    {
      strcpy (pclRcvKeyLst, pclKeyLstBgn);
    }                           /* end if */

    ilArrFldPos = 0;
    ilDepFldPos = 0;
    ilIntFldPos = 0;
    ilErrFldPos = 0;
    ilArrDatPos = 0;
    ilDepDatPos = 0;
    ilIntDatPos = 0;
    ilErrDatPos = 0;
    ilErrMsgPos = 0;
    pclArrFldLst[ilArrFldPos] = 0x00;
    pclDepFldLst[ilDepFldPos] = 0x00;
    pclIntFldLst[ilIntFldPos] = 0x00;
    pclAddKey[0] = 0x00;
    ilOrg = 0;                  /* +++ jim +++ 20000811 */
    ilDst = 0;                  /* +++ jim +++ 20000811 */
    ilRoutType = 0;
    ilCurLstTyp = 0;

    dbg (TRACE, "RCV FLD LST <%s>", pclRcvFldLst);
    ilRcvFldCnt = field_count (pclRcvFldLst);
    dbg (TRACE, "RECEIVED %d FIELDS", ilRcvFldCnt);
    for (ilRcvFldNbr = 1; ilRcvFldNbr <= ilRcvFldCnt; ilRcvFldNbr++)
    {
      ilFldNamLen = get_real_item (pclRcvFldNam, pclRcvFldLst, ilRcvFldNbr);
      if (ilFldNamLen > 0)
      {
        ilFldDatLen = get_real_item (pclRcvFldDat, pclDatLstBgn, ilRcvFldNbr);
        if (ilFldDatLen == 0)
        {
          strcpy (pclRcvFldDat, " ");
        }                       /* end if */

        if (ilFldDatLen >= 0)
        {
          ilActLstTyp = 0;

          if (ilFldNamLen > 4)
          {
            /* Check For Internal Field */
            if (pclRcvFldNam[0] == '#')
            {
              ilActLstTyp = 4;
            }                   /* end if */
            else
            {
              /* Check For Dedicated Field Type */
              switch (pclRcvFldNam[4])
              {
                case 'A':
                  ilActLstTyp = 1;
                  break;
                case 'D':
                  ilActLstTyp = 2;
                  break;
                case 'B':
                  ilActLstTyp = 3;
                  break;
                default:
                  ilActLstTyp = -2;
                  break;
              }                 /* end switch */
              pclRcvFldNam[4] = 0x00;
            }                   /* end else */
          }                     /* end if */

          ilChkFld = get_item_no (pclSpecFlds, pclRcvFldNam, 5) + 1;
          if (ilChkFld > 0)
          {
            /* Check Data of Routing Field */
            /* and Set Current List Type */
            switch (ilChkFld)
            {
              case 1:          /* ADID */
                switch (pclRcvFldDat[0])
                {
                  case 'A':
                    ilCurLstTyp = 1;
                    break;
                  case 'D':
                    ilCurLstTyp = 2;
                    break;
                  default:
                    break;
                }               /* end switch */
                ilActLstTyp = -99;
                break;
              case 2:          /*  */
                break;
              default:
                break;
            }                   /* end switch */
          }                     /* end if */

          if (ilActLstTyp == 0)
          {
            /* Set Actual to Current List Type */
            /* and Check For Fitting List */
            ilActLstTyp = ilCurLstTyp;
            switch (ilCurLstTyp)
            {
              case 1:
                if (strstr (pclArrFldLst, pclRcvFldNam) != NULL)
                {
                  ilActLstTyp = 2;
                }               /* end if */
                break;
              case 2:
                if (strstr (pclDepFldLst, pclRcvFldNam) != NULL)
                {
                  ilActLstTyp = 1;
                }               /* end if */
                break;
              default:
                break;
            }                   /* end switch */
          }                     /* end if */

          if (ilActLstTyp == 0)
          {
            /* Check For Automatic Field */
            if (strstr (pclArrFldLst, pclRcvFldNam) == NULL)
            {
              ilActLstTyp = 1;  /* New Arrival Field */
            }                   /* end if */
            else
            {
              ilActLstTyp = 2;  /* New Departure Field */
            }                   /* end else */
          }                     /* end if */

          /* Check For Duplicated Fields */
          switch (ilActLstTyp)
          {
            case 1:
              if (strstr (pclArrFldLst, pclRcvFldNam) != NULL)
              {
                ilActLstTyp = -1;
              }                 /* end if */
              break;
            case 2:
              if (strstr (pclDepFldLst, pclRcvFldNam) != NULL)
              {
                ilActLstTyp = -1;
              }                 /* end if */
              break;
            case 3:
              if (strstr (pclArrFldLst, pclRcvFldNam) != NULL)
              {
                ilActLstTyp = -1;
              }                 /* end if */
              if (strstr (pclDepFldLst, pclRcvFldNam) != NULL)
              {
                ilActLstTyp = -1;
              }                 /* end if */
              break;
            case 4:
              if (strstr (pclIntFldLst, pclRcvFldNam) != NULL)
              {
                ilActLstTyp = -1;
              }                 /* end if */
              break;
            default:
              break;
          }                     /* end switch */

          if ((ilActLstTyp != 4) && (ilActLstTyp != -99))
          {
            sprintf (pclChkFldNam, ",%s,", pclRcvFldNam);
            if (strstr (pclAftFldLst, pclChkFldNam) == NULL)
            {
              ilActLstTyp = -3;
            }                   /* end if */
          }                     /* end if */

          /* Add Fieldname and Data to Lists */
          switch (ilActLstTyp)
          {
            case 1:            /* Arrival */
              StrgPutStrg (pclArrFldLst, &ilArrFldPos, pclRcvFldNam, 0, -1, ",");
              StrgPutStrg (pclArrDatLst, &ilArrDatPos, pclRcvFldDat, 0, -1, ",");
              break;
            case 2:            /* Departure */
              StrgPutStrg (pclDepFldLst, &ilDepFldPos, pclRcvFldNam, 0, -1, ",");
              StrgPutStrg (pclDepDatLst, &ilDepDatPos, pclRcvFldDat, 0, -1, ",");
              break;
            case 3:            /* Both */
              StrgPutStrg (pclArrFldLst, &ilArrFldPos, pclRcvFldNam, 0, -1, ",");
              StrgPutStrg (pclArrDatLst, &ilArrDatPos, pclRcvFldDat, 0, -1, ",");
              StrgPutStrg (pclDepFldLst, &ilDepFldPos, pclRcvFldNam, 0, -1, ",");
              StrgPutStrg (pclDepDatLst, &ilDepDatPos, pclRcvFldDat, 0, -1, ",");
              break;
            case 4:            /* Internal Field */
              StrgPutStrg (pclIntFldLst, &ilIntFldPos, pclRcvFldNam, 0, -1, ",");
              StrgPutStrg (pclIntDatLst, &ilIntDatPos, pclRcvFldDat, 0, -1, ",");
              break;
            case 0:            /* Error Arr/Dep */
              StrgPutStrg (pclErrFldLst, &ilErrFldPos, pclRcvFldNam, 0, -1, ",");
              StrgPutStrg (pclErrDatLst, &ilErrDatPos, pclRcvFldDat, 0, -1, ",");
              StrgPutStrg (pclErrMsgLst, &ilErrMsgPos, "ADFL", 0, -1, ",");
              break;
            case -1:           /* Error duplicated */
              StrgPutStrg (pclErrFldLst, &ilErrFldPos, pclRcvFldNam, 0, -1, ",");
              StrgPutStrg (pclErrDatLst, &ilErrDatPos, pclRcvFldDat, 0, -1, ",");
              StrgPutStrg (pclErrMsgLst, &ilErrMsgPos, "DUPL", 0, -1, ",");
              break;
            case -2:           /* Error Type */
              StrgPutStrg (pclErrFldLst, &ilErrFldPos, pclRcvFldNam, 0, -1, ",");
              StrgPutStrg (pclErrDatLst, &ilErrDatPos, pclRcvFldDat, 0, -1, ",");
              StrgPutStrg (pclErrMsgLst, &ilErrMsgPos, "TYPE", 0, -1, ",");
              break;
            case -3:           /* Error Name */
              StrgPutStrg (pclErrFldLst, &ilErrFldPos, pclRcvFldNam, 0, -1, ",");
              StrgPutStrg (pclErrDatLst, &ilErrDatPos, pclRcvFldDat, 0, -1, ",");
              StrgPutStrg (pclErrMsgLst, &ilErrMsgPos, "NAME", 0, -1, ",");
              break;
            default:
              break;
          }                     /* end switch */

          pclArrFldLst[ilArrFldPos] = 0x00;
          pclDepFldLst[ilDepFldPos] = 0x00;
          pclIntFldLst[ilIntFldPos] = 0x00;

        }                       /* end if */
      }                         /* end if */
    }                           /* end for */

    ilGetRc = RC_SUCCESS;

    if (ilArrFldPos > 0)
    {
      ilArrFldPos--;
      ilArrDatPos--;
    }                           /* end if */
    if (ilDepFldPos > 0)
    {
      ilDepFldPos--;
      ilDepDatPos--;
    }                           /* end if */
    if (ilIntFldPos > 0)
    {
      ilIntFldPos--;
      ilIntDatPos--;
    }                           /* end if */
    if (ilErrFldPos > 0)
    {
      ilGetRc = RC_FAIL;
      ilErrFldPos--;
      ilErrDatPos--;
      ilErrMsgPos--;
    }                           /* end if */

    pclArrFldLst[ilArrFldPos] = 0x00;
    pclDepFldLst[ilDepFldPos] = 0x00;
    pclArrDatLst[ilArrDatPos] = 0x00;
    pclDepDatLst[ilDepDatPos] = 0x00;
    pclIntFldLst[ilIntFldPos] = 0x00;
    pclIntDatLst[ilIntDatPos] = 0x00;
    pclErrFldLst[ilErrFldPos] = 0x00;
    pclErrDatLst[ilErrDatPos] = 0x00;
    pclErrMsgLst[ilErrMsgPos] = 0x00;

    if (ilArrFldPos > 0)
    {
      dbg (TRACE, "ARRIVAL");
      dbg (TRACE, "FLD <%s>", pclArrFldLst);
      dbg (TRACE, "DAT <%s>", pclArrDatLst);
    }                           /* end if */
    if (ilDepFldPos > 0)
    {
      dbg (TRACE, "DEPARTURE");
      dbg (TRACE, "FLD <%s>", pclDepFldLst);
      dbg (TRACE, "DAT <%s>", pclDepDatLst);
    }                           /* end if */
    if (ilIntFldPos > 0)
    {
      dbg (TRACE, "INTERNAL");
      dbg (TRACE, "FLD <%s>", pclIntFldLst);
      dbg (TRACE, "DAT <%s>", pclIntDatLst);
    }                           /* end if */

    dbg (TRACE, "KEYWORDS <%s>", pclRcvKeyLst);
    ilLen = get_real_item (pclActCmdKey, pclRcvKeyLst, 1);
    if (ilLen <= 0)
    {
      strcpy (pclActCmdKey, "AUTO");
    }                           /* end if */

    if (ilGetRc == RC_SUCCESS)
    {
      ilDataType = 0;
      ilFoundArr = FALSE;
      ilFoundDep = FALSE;

      if ((ilGetRc == RC_SUCCESS) && (ilArrFldPos > 0))
      {
        ilRoutType = -1;
        if (strcmp (pclActCmdKey, "FLIGHT") == 0)
        {
          ilRoutType = 0;
          dbg (TRACE, "SINGLE FLIGHT: CHECK ROUTING");
          ilLenOrg = 3;         /* +++ jim +++ 20000811 check for 3 and 4 letter */
          ilLenDst = 3;         /* +++ jim +++ 20000811 airport code in line */
          ilOrg = get_item_no (pclArrFldLst, "ORG3", 5) + 1;
          if (ilOrg <= 0)
          {
            ilOrg = get_item_no (pclArrFldLst, "ORG4", 5) + 1;
            ilLenOrg = 4;
          }
          if (ilOrg > 0)
          {
            ilLen = get_real_item (pclOrg, pclArrDatLst, ilOrg);
          }
          if (ilLen > 0)
          {
            dbg (TRACE, "DEPARTURE FROM ORIGIN <%s>", pclOrg);
            if (((ilLenOrg == 3) && (strcmp (pclOrg, pcgH3LC) == 0)) ||
                ((ilLenOrg == 4) && (strcmp (pclOrg, pcgH4LC) == 0)))
            {
              dbg (TRACE, "THAT'S MY HOME AIRPORT");
              ilRoutType += 2;
            }                   /* end if ilHopoLetters */
          }                     /* end if ilen */
          ilDst = get_item_no (pclArrFldLst, "DES3", 5) + 1;
          if (ilDst <= 0)
          {
            ilDst = get_item_no (pclArrFldLst, "DES4", 5) + 1;
            ilLenDst = 4;
          }
          if (ilDst > 0)
          {
            ilLen = get_real_item (pclDst, pclArrDatLst, ilDst);
          }
          if (ilLen > 0)
          {
            dbg (TRACE, "ARRIVAL AT DESTINATION <%s>", pclDst);
            if (((ilLenDst == 3) && (strcmp (pclDst, pcgH3LC) == 0)) ||
                ((ilLenDst == 4) && (strcmp (pclDst, pcgH4LC) == 0)))
            {
              dbg (TRACE, "THAT'S MY HOME AIRPORT");
              ilRoutType += 1;
            }                   /* end if */
          }                     /* end if */
          dbg (TRACE, "CHECK ROUTING RESULT:");
          switch (ilRoutType)
          {
            case 0:
              dbg (TRACE, "IT'S NOT MY HOME AIRPORT");
              break;
            case 1:
              dbg (TRACE, "ARRIVAL FLIGHT");
              sprintf (pclAddKey, "AND DES%1d='%s'", ilLenDst, pclDst);
              break;
            case 2:
              dbg (TRACE, "DEPARTURE FLIGHT");
              sprintf (pclAddKey, "AND ORG%1d='%s'", ilLenOrg, pclOrg);
              break;
            case 3:
              dbg (TRACE, "CIRCULATION FLIGHT");
              break;
            default:
              dbg (TRACE, "ERROR: ROUTING TYPE %d", ilRoutType);
              break;
          }                     /* end switch */

          if ((ilGetRc == RC_SUCCESS) && (ilRoutType > 0))
          {
            ilGetRc =
              BuildAfiKeyValues ("?", pclArrSelKey, pclAddKey, ilGetRc, pclArrFldLst, pclArrDatLst, pclIntFldLst,
                                 pclIntDatLst);
          }                     /* end if */

          if ((ilGetRc == RC_SUCCESS) && (ilRoutType > 0))
          {
            dbg (TRACE, "FLIGHT SELECTION <%s>", pclArrSelKey);
            strcpy (pclTable, pcgAftTab);
            ilRC = SearchFlight (pclTable, "URNO,RKEY", pclArrSelKey, FOR_SEARCH, TRUE);
            if (ilRC == RC_SUCCESS)
            {
              dbg (TRACE, "FLIGHT URNO,RKEY <%s>", pcgDataArea);
              (void) get_real_item (pclArrUrno, pcgDataArea, 1);
              (void) get_real_item (pclArrRkey, pcgDataArea, 2);
              if (igTWEUrnoIsNumber == TRUE)
              {
                sprintf (pclArrSelKey, "WHERE URNO=%s", pclArrUrno);
              }
              else
              {
                sprintf (pclArrSelKey, "WHERE URNO='%s'", pclArrUrno);
              }
              ilFoundArr = TRUE;
              strcpy (pclActCmdKey, "UFR");
              (void) BuildUpdateValues (" ", "?", pclArrFldLst, pclArrDatLst, " ");
            }                   /* end if */
            else
            {
              dbg (TRACE, "FLIGHT NOT FOUND");
            }                   /* end if */
          }                     /* end if */
        }                       /* end if */
      }                         /* end if */

      if ((ilGetRc == RC_SUCCESS) && (ilRoutType < 0) && (ilArrFldPos > 0))
      {
        dbg (TRACE, "CHECK ARRIVAL FLIGHT DATA");
        ilDataType += 1;
        ilGetRc =
          BuildAfiKeyValues ("A", pclArrSelKey, pclAddKey, ilGetRc, pclArrFldLst, pclArrDatLst, pclIntFldLst,
                             pclIntDatLst);
        if (ilGetRc == RC_SUCCESS)
        {
          dbg (TRACE, "ARR FLIGHT <%s>", pclArrSelKey);
          strcpy (pclTable, pcgAftTab);
          ilRC = SearchFlight (pclTable, "URNO,RKEY", pclArrSelKey, FOR_SEARCH, TRUE);
          if (ilRC == RC_SUCCESS)
          {
            dbg (TRACE, "INBOUND  URNO,RKEY <%s>", pcgDataArea);
            (void) get_real_item (pclArrUrno, pcgDataArea, 1);
            (void) get_real_item (pclArrRkey, pcgDataArea, 2);
            if (igTWEUrnoIsNumber == TRUE)
            {
              sprintf (pclArrSelKey, "WHERE URNO=%s", pclArrUrno);
            }
            else
            {
              sprintf (pclArrSelKey, "WHERE URNO='%s'", pclArrUrno);
            }
            ilFoundArr = TRUE;
          }                     /* end if */
          else
          {
            ilFoundArr = FALSE;
            pclArrUrno[0] = 0x00;
            pclArrRkey[0] = 0x00;
            dbg (TRACE, "FLIGHT NOT FOUND");
          }                     /* end if */
        }                       /* end if */
      }                         /* end if */

      if ((ilGetRc == RC_SUCCESS) && (ilRoutType < 0) && (ilDepFldPos > 0))
      {
        dbg (TRACE, "CHECK DEPARTURE FLIGHT DATA");
        ilDataType += 2;
        ilGetRc =
          BuildAfiKeyValues ("D", pclDepSelKey, pclAddKey, ilGetRc, pclDepFldLst, pclDepDatLst, pclIntFldLst,
                             pclIntDatLst);
        if (ilGetRc == RC_SUCCESS)
        {
          dbg (TRACE, "DEP FLIGHT <%s>", pclDepSelKey);
          strcpy (pclTable, pcgAftTab);
          ilRC = SearchFlight (pclTable, "URNO,RKEY", pclDepSelKey, FOR_SEARCH, TRUE);
          if (ilRC == RC_SUCCESS)
          {
            dbg (TRACE, "OUTBOUND URNO,RKEY <%s>", pcgDataArea);
            (void) get_real_item (pclDepUrno, pcgDataArea, 1);
            (void) get_real_item (pclDepRkey, pcgDataArea, 2);
            if (igTWEUrnoIsNumber == TRUE)
            {
              sprintf (pclDepSelKey, "WHERE URNO=%s", pclDepUrno);
            }
            else
            {
              sprintf (pclDepSelKey, "WHERE URNO='%s'", pclDepUrno);
            }
            ilFoundDep = TRUE;
          }                     /* end if */
          else
          {
            ilFoundDep = FALSE;
            pclDepUrno[0] = 0x00;
            pclDepRkey[0] = 0x00;
            dbg (TRACE, "FLIGHT NOT FOUND");
          }                     /* end if */
        }                       /* end if */
      }                         /* end if */

      if (strstr (",AUTO,UFR,", pclActCmdKey) != NULL)
      {
        if (ilFoundArr == TRUE)
        {
          ilRC = UpdateFlights (pclTable, pclArrFldLst, pclArrDatLst, pclArrSelKey, FALSE);
        }                       /* end if */
        if (ilFoundDep == TRUE)
        {
          ilRC = UpdateFlights (pclTable, pclDepFldLst, pclDepDatLst, pclDepSelKey, FALSE);
        }                       /* end if */
      }                         /* end if */

      if ((ilGetRc == RC_SUCCESS) && (ilDataType == 3))
      {
        dbg (TRACE, "FOUND ROTATION FLIGHT DATA");
      }                         /* end if */

    }                           /* end if */

    if (ilGetRc != RC_SUCCESS)
    {
      dbg (TRACE, "FLIGHT DATA IGNORED DUE TO ERROR");
      if (ilErrFldPos > 0)
      {
        dbg (TRACE, "MSG <%s>", pclErrMsgLst);
        dbg (TRACE, "FLD <%s>", pclErrFldLst);
        dbg (TRACE, "DAT <%s>", pclErrDatLst);
      }                         /* end if */
    }                           /* end if */

    if (pclDatLstEnd != NULL)
    {
      pclDatLstBgn = pclDatLstEnd + 1;
    }                           /* end if */

    if (pclFldLstEnd != NULL)
    {
      pclFldLstBgn = pclFldLstEnd + 1;
    }                           /* end if */

    if (pclKeyLstEnd != NULL)
    {
      pclKeyLstBgn = pclKeyLstEnd + 1;
    }                           /* end if */

  }
  while (pclDatLstEnd != NULL);

  return ilRC;
}                               /* end FlightDataInput */

/* *************************************************** */
/* *************************************************** */
static int BuildAfiKeyValues (char *pcpAdid, char *pcpSelKey, char *pcpAddKey, int ipRetVal, char *pcpActFldLst,
                              char *pcpActDatLst, char *pcpIntFldLst, char *pcpIntDatLst)
{
  int ilRC = RC_SUCCESS;
  int ilGetRc = RC_FAIL;
  int ilUrno = 0;
  int ilFkey = 0;
  int ilFlno = 0;
  int ilAlcd = 0;
  int ilFltn = 0;
  int ilFlns = 0;
  int ilFday = 0;
  int ilLen = 0;
  char pclUrno[32];
  char pclFkey[32];
  char pclFlno[32];
  char pclAlcd[32];
  char pclFltn[32];
  char pclFlns[32];
  char pclFday[32];
  char pclFnr3[16];
  char pclFnr2[16];
  char pclAlc3[16];
  char pclAlc2[16];
  char pclFnbr[16];
  char pclFlsf[16];

  ilRC = ipRetVal;
  pcpSelKey[0] = 0x00;

  if (ilRC == RC_SUCCESS)
  {
    if (ilGetRc != RC_SUCCESS)
    {
      /* Check URNO */
      ilUrno = get_item_no (pcpActFldLst, "URNO", 5) + 1;
      if (ilUrno > 0)
      {
        ilLen = get_real_item (pclUrno, pcpActDatLst, ilUrno);
        if (ilLen > 0)
        {
          if (igTWEUrnoIsNumber == TRUE)
          {
            sprintf (pcpSelKey, "WHERE URNO=%s", pclUrno);
          }
          else
          {
            sprintf (pcpSelKey, "WHERE URNO='%s'", pclUrno);
          }
          ilGetRc = RC_SUCCESS;
        }                       /* end if */
      }                         /* end if */
    }                           /* end if */

    if (ilGetRc != RC_SUCCESS)
    {
      /* Check FKEY */
      ilFkey = get_item_no (pcpActFldLst, "FKEY", 5) + 1;
      if (ilFkey > 0)
      {
        ilLen = get_real_item (pclFkey, pcpActDatLst, ilFkey);
        if (ilLen > 0)
        {
          sprintf (pcpSelKey, "WHERE FKEY='%s'", pclFkey);
          ilGetRc = RC_SUCCESS;
        }                       /* end if */
      }                         /* end if */
    }                           /* end if */

    if (ilGetRc != RC_SUCCESS)
    {
      /* Check FLNO */
      /* or ALC2/ALC3 and FLTN/FLNS */
      /* and FDAY or STOA/STOD */

      /* Search FLNO */
      ilFlno = get_item_no (pcpActFldLst, "FLNO", 5) + 1;
      if (ilFlno > 0)
      {
        ilLen = get_real_item (pclFlno, pcpActDatLst, ilFlno);
        if (ilLen > 0)
        {
          ilGetRc = RC_SUCCESS;
        }                       /* end if */
      }                         /* end if */

      if (ilGetRc != RC_SUCCESS)
      {
        /* Search ALC3 */
        ilAlcd = get_item_no (pcpActFldLst, "ALC3", 5) + 1;
        if (ilAlcd > 0)
        {
          ilLen = get_real_item (pclAlcd, pcpActDatLst, ilAlcd);
          if (ilLen > 0)
          {
            strcpy (pclFlno, pclAlcd);
            ilGetRc = RC_SUCCESS;
          }                     /* end if */
        }                       /* end if */

        if (ilGetRc != RC_SUCCESS)
        {
          /* Search ALC2 */
          ilAlcd = get_item_no (pcpActFldLst, "ALC2", 5) + 1;
          if (ilAlcd > 0)
          {
            ilLen = get_real_item (pclAlcd, pcpActDatLst, ilAlcd);
            if (ilLen > 0)
            {
              strcpy (pclFlno, pclAlcd);
              ilGetRc = RC_SUCCESS;
            }                   /* end if */
          }                     /* end if */
        }                       /* end if */

        if (ilGetRc == RC_SUCCESS)
        {
          ilGetRc = RC_FAIL;
          /* Search FLTN */
          ilFltn = get_item_no (pcpActFldLst, "FLTN", 5) + 1;
          if (ilFltn > 0)
          {
            ilLen = get_real_item (pclFltn, pcpActDatLst, ilFltn);
            if (ilLen > 0)
            {
              strcat (pclFlno, pclFltn);
              ilGetRc = RC_SUCCESS;
            }                   /* end if */
          }                     /* end if */

        }                       /* end if */

        if (ilGetRc == RC_SUCCESS)
        {
          /* Search FLNS */
          ilFlns = get_item_no (pcpActFldLst, "FLNS", 5) + 1;
          if (ilFlns > 0)
          {
            ilLen = get_real_item (pclFlns, pcpActDatLst, ilFlns);
            if (ilLen > 0)
            {
              strcat (pclFlno, pclFlns);
            }                   /* end if */
          }                     /* end if */
        }                       /* end if */

      }                         /* end if */

      if (ilGetRc == RC_SUCCESS)
      {
        /* Here We Have a FLNO */

        dbg (DEBUG, "FLNO <%s>", pclFlno);

        /* Look For Day Of Flight */
        ilGetRc = RC_FAIL;

        /* Search FDAY */
        ilFday = get_item_no (pcpIntFldLst, "FDAY", 5) + 1;
        if (ilFday > 0)
        {
          ilLen = get_real_item (pclFday, pcpIntDatLst, ilFday);
          if (ilLen > 0)
          {
            ilGetRc = RC_SUCCESS;
          }                     /* end if */
        }                       /* end if */

        if (ilGetRc != RC_SUCCESS)
        {
          /* Search STOA or STOD */
          if (pcpAdid[0] == 'A')
          {
            ilFday = get_item_no (pcpActFldLst, "STOA", 5) + 1;
          }                     /* end if */
          else
          {
            ilFday = get_item_no (pcpActFldLst, "STOD", 5) + 1;
          }                     /* end else */

          if (ilFday > 0)
          {
            ilLen = get_real_item (pclFday, pcpActDatLst, ilFday);
            if (ilLen > 0)
            {
              ilGetRc = RC_SUCCESS;
            }                   /* end if */
          }                     /* end if */
        }                       /* end if */
      }                         /* end if */

      if (ilGetRc == RC_SUCCESS)
      {
        /* Here We Have FLNO and FDAY */
        dbg (TRACE, "FDAY <%s>", pclFday);
        strcpy (pclFnr3, pclFlno);

/*****  TWE 21012000  ****/
        if (igTWEImport == TRUE)
        {
          pclAlc2[0] = 0x00;
          pclAlc3[0] = 0x00;
          pclFnr2[0] = 0x00;
          pclFnbr[0] = 0x00;
          pclFlsf[0] = 0x00;
          ilGetRc = TWEBuildFlightNbr (pclFnr3, pclFnr2, pclAlc3, pclAlc2, pclFnbr, pclFlsf, 0);
        }
        else
        {
          ilGetRc = BuildFlightNbr (pclFnr3, pclFnr2, pclAlc3, pclAlc2, pclFnbr, pclFlsf, 0);
        }
/************************/

        dbg (TRACE, "--- FLNO ITEMS OF <%s> ----", pclFlno);
        dbg (TRACE, "FNR3 <%s>", pclFnr3);
        dbg (TRACE, "FNR2 <%s>", pclFnr2);
        dbg (TRACE, "ALC3 <%s>", pclAlc3);
        dbg (TRACE, "ALC2 <%s>", pclAlc2);
        dbg (TRACE, "FLTN <%s>", pclFnbr);
        dbg (TRACE, "FLNS <%s>", pclFlsf);
        dbg (TRACE, "---------------------------------");
        if ((ilGetRc == RC_SUCCESS) && (pcpAdid[0] != '?'))
        {
          (void) BuildFkey (pclFkey, pclFnbr, pclAlc3, pclFlsf, pclFday, pcpAdid);
          dbg (TRACE, "FLIGHT FKEY <%s>", pclFkey);
          sprintf (pcpSelKey, "WHERE FKEY='%s'", pclFkey);
        }                       /* end if */
        else
        {
          if (ilGetRc == RC_SUCCESS)
          {
            if (pclFlsf[0] == 0x00)
            {
              strcpy (pclFlsf, " ");
            }                   /* end if */
            dbg (TRACE, "BUILD GENERAL SELECTION WITH");
            dbg (TRACE, "FLTN <%s>", pclFnbr);
            dbg (TRACE, "ALC3 <%s>", pclAlc3);
            dbg (TRACE, "FLNS <%s>", pclFlsf);
            dbg (TRACE, "FDAY <%s>", pclFday);
            sprintf (pcpSelKey, "WHERE (FLTN='%s' AND ALC3='%s' AND FLNS='%s' " "AND STOD LIKE '%s%%') %s", pclFnbr,
                     pclAlc3, pclFlsf, pclFday, pcpAddKey);
          }                     /* end if */
          else
          {
            dbg (TRACE, "UNABLE TO UNDERSTAND FLNO");
          }                     /* end else */
        }                       /* end else */
      }                         /* end if */

    }                           /* end if */

    if (ilGetRc == RC_SUCCESS)
    {
      ;
    }                           /* end if */
    else
    {
      if (pcpAdid[0] == 'A')
      {
        dbg (TRACE, "UNABLE TO BUILD SELECTION FOR ARR FLIGHT");
      }                         /* end if */
      else if (pcpAdid[0] == 'D')
      {
        dbg (TRACE, "UNABLE TO BUILD SELECTION FOR DEP FLIGHT");
      }                         /* end else if */
      else
      {
        dbg (TRACE, "UNABLE TO BUILD SELECTION FOR THAT FLIGHT");
      }                         /* end else */
    }                           /* end else */

    ilRC = ilGetRc;
  }                             /* end if */

  return ilRC;
}                               /* end BuildAfiKeyValues */

/* *************************************************** */
/* *************************************************** */
static int BuildUpdateValues (char *pcpUpdDay, char *pcpAdid, char *pcpFields, char *pcpData, char *pcpFkey)
{
  int ilRC = RC_SUCCESS;
  int ilTbl = 0;
  int ilFld = 0;
  int ilFldCnt = 0;
  int ilFltn = 0;
  int ilFlns = 0;
  int ilAlc2 = 0;
  int ilAlc3 = 0;
  int ilStoa = 0;
  int ilStod = 0;
  int ilLen = 0;
  int ilFldPos = 0;
  int ilDatPos = 0;
  char pclTblNam[8];
  char pclTmpBuf[32];
  char pclBadFields[] = "ALC2,ALC3,FLNO,FLTN,FLNS,ORG3,DES3";

  dbg (TRACE, "BUILD UPDATE VALUES RCV FLD\n<%s>", pcpFields);
  dbg (TRACE, "BUILD UPDATE VALUES RCV DAT\n<%s>", pcpData);

  ilTbl = GetTableIndex (pcgAftTab);

  (void) ResetDataBuffer (__LINE__ ,TRUE, 0, 0, 0, 0, 0, 0);
  (void) GetReceivedData (pcgAftTab, pcpFields, pcpData);

  if (pcpAdid[0] != '?')
  {
    ilAlc2 = GetFieldIndex (ilTbl, "ALC2");
    ilAlc3 = GetFieldIndex (ilTbl, "ALC3");
    ilFltn = GetFieldIndex (ilTbl, "FLTN");
    ilFlns = GetFieldIndex (ilTbl, "FLNS");

    ilLen = prgTblFldCfg[ilTbl][ilFltn].RcvLen - 1;
    if (ilLen > 0)
    {
      if ((prgTblFldCfg[ilTbl][ilFltn].RcvDat[ilLen] < '0') || (prgTblFldCfg[ilTbl][ilFltn].RcvDat[ilLen] > '9'))
      {
        pclTmpBuf[0] = prgTblFldCfg[ilTbl][ilFltn].RcvDat[ilLen];
        pclTmpBuf[1] = 0x00;
        (void) SetRcvValues (ilTbl, 0, "FLNS", pclTmpBuf);
        prgTblFldCfg[ilTbl][ilFltn].RcvDat[ilLen] = 0x00;
        prgTblFldCfg[ilTbl][ilFltn].RcvLen = ilLen;
      }                         /* end if */
      if (prgTblFldCfg[ilTbl][ilAlc3].RcvLen < 3)
      {
        (void) SetRcvValues (ilTbl, ilAlc2, "", prgTblFldCfg[ilTbl][ilAlc3].RcvDat);
        sprintf (pclTblNam, "ALT%s", pcgBasExt);
        ilRC = GetBasicData (pclTblNam, "ALC2", prgTblFldCfg[ilTbl][ilAlc2].RcvDat, "ALC3", pclTmpBuf, 1, RC_FAIL);
        if (ilRC == RC_SUCCESS)
        {
          (void) SetRcvValues (ilTbl, ilAlc3, "", pclTmpBuf);
        }                       /* end if */
      }                         /* end if */

      ilStoa = GetFieldIndex (ilTbl, "STOA");
      if (prgTblFldCfg[ilTbl][ilStoa].RcvLen > 0)
      {
        sprintf (pclTmpBuf, "%s%s00", pcpUpdDay, prgTblFldCfg[ilTbl][ilStoa].RcvDat);
        (void) SetRcvValues (ilTbl, ilStoa, "", pclTmpBuf);
      }                         /* end if */

      ilStod = GetFieldIndex (ilTbl, "STOD");
      if (prgTblFldCfg[ilTbl][ilStod].RcvLen > 0)
      {
        sprintf (pclTmpBuf, "%s%s00", pcpUpdDay, prgTblFldCfg[ilTbl][ilStod].RcvDat);
        (void) SetRcvValues (ilTbl, ilStod, "", pclTmpBuf);
      }                         /* end if */

      (void) BuildFkey (pcpFkey, prgTblFldCfg[ilTbl][ilFltn].RcvDat, prgTblFldCfg[ilTbl][ilAlc3].RcvDat,
                        prgTblFldCfg[ilTbl][ilFlns].RcvDat, pcpUpdDay, pcpAdid);
      /* dbg(TRACE,"FLIGHT FKEY <%s>",pcpFkey); */

      prgTblFldCfg[ilTbl][ilAlc2].RcvFlg = FALSE;
      prgTblFldCfg[ilTbl][ilAlc3].RcvFlg = FALSE;
      prgTblFldCfg[ilTbl][ilFltn].RcvFlg = FALSE;
      prgTblFldCfg[ilTbl][ilFlns].RcvFlg = FALSE;

    }                           /* end if */
  }                             /* end if */
  else
  {
    dbg (TRACE, "ERASE <%s> FROM FIELDLIST", pclBadFields);
    (void) UnSetRcvValues (ilTbl, pclBadFields);
  }                             /* end else */

  ilFldPos = 0;
  ilDatPos = 0;
  ilFldCnt = prgPrjTblCfg[ilTbl].TblFldCnt;
  for (ilFld = 1; ilFld <= ilFldCnt; ilFld++)
  {
    /* Collect FieldList and Data for Update */
    if (prgTblFldCfg[ilTbl][ilFld].RcvFlg == TRUE)
    {
      StrgPutStrg (pcpFields, &ilFldPos, prgTblFldCfg[ilTbl][ilFld].SysTabFina, 0, -1, ",");
      StrgPutStrg (pcpData, &ilDatPos, prgTblFldCfg[ilTbl][ilFld].RcvDat, 0, -1, ",");
    }                           /* end if */
  }                             /* end for */
  if (ilFldPos > 0)
  {
    ilFldPos--;
    ilDatPos--;
  }                             /* end if */
  pcpFields[ilFldPos] = 0x00;
  pcpData[ilDatPos] = 0x00;

  dbg (TRACE, "BUILD UPDATE VALUES NEW FLD\n<%s>", pcpFields);
  dbg (TRACE, "BUILD UPDATE VALUES NEW DAT\n<%s>", pcpData);

  ilRC = RC_SUCCESS;
  return ilRC;
}                               /* end BuildUpdateValues */

/* *************************************************** */
/* *************************************************** */
static int CheckFlightRotation (char *pcpArrFkey, char *pcpDepFkey)
{
  int ilRC = RC_SUCCESS;
  char pclTable[16];
  char pclFields[32];
  char pclData[32];
  char pclJoinKey[64];

  char pclArrUrno[16];
  char pclArrRkey[16];
  char pclArrWhere[64];
  char pclDepUrno[16];
  char pclDepRkey[16];
  char pclDepWhere[64];

  dbg (TRACE, "CHECK FLIGHT ROTATATION");

  strcpy (pclTable, pcgAftTab);
  sprintf (pclArrWhere, "WHERE FKEY='%s'", pcpArrFkey);
  ilRC = SearchFlight (pclTable, "URNO,RKEY", pclArrWhere, FOR_SEARCH, TRUE);
  if (ilRC == RC_SUCCESS)
  {
    dbg (TRACE, "INBOUND  URNO,RKEY <%s>", pcgDataArea);
    (void) get_real_item (pclArrUrno, pcgDataArea, 1);
    (void) get_real_item (pclArrRkey, pcgDataArea, 2);
  }                             /* end if */
  else
  {
    pclArrUrno[0] = 0x00;
    pclArrRkey[0] = 0x00;
  }                             /* end if */

  strcpy (pclTable, pcgAftTab);
  sprintf (pclDepWhere, "WHERE FKEY='%s'", pcpDepFkey);
  ilRC = SearchFlight (pclTable, "URNO,RKEY", pclDepWhere, FOR_SEARCH, TRUE);
  if (ilRC == RC_SUCCESS)
  {
    dbg (TRACE, "OUTBOUND URNO,RKEY <%s>", pcgDataArea);
    (void) get_real_item (pclDepUrno, pcgDataArea, 1);
    (void) get_real_item (pclDepRkey, pcgDataArea, 2);
  }                             /* end if */
  else
  {
    pclDepUrno[0] = 0x00;
    pclDepRkey[0] = 0x00;
  }                             /* end if */

  if (strcmp (pclArrRkey, pclDepRkey) != 0)
  {
    strcpy (pclTable, pcgAftTab);
    sprintf (pclJoinKey, "%s,%s,%s,%s", pclArrUrno, pclArrRkey, pclDepUrno, pclDepRkey);
    pclFields[0] = 0x00;
    pclData[0] = 0x00;
    (void) JoinFlights (pclTable, pclFields, pclData, pclJoinKey, FALSE);
  }                             /* end if */

  ilRC = RC_SUCCESS;
  return ilRC;
}                               /* end CheckFlightRotation */

/* *************************************************** */
/* *************************************************** */
static int HandleExcoData (char *pcpRcvTable, char *pcpFields, char *pcpData, char *pcpSelKey)
{
  int ilRC = RC_SUCCESS;
  char pclActCmd[16];
  dbg (TRACE, "===== DATA ELEMENTS FROM INTERFACE =====");
  dbg (TRACE, "EXCO TABLE  <%s>", pcpRcvTable);
  dbg (TRACE, "EXCO FIELDS <%s>", pcpFields);
  dbg (TRACE, "EXCO DATA   <%s>", pcpData);
  dbg (TRACE, "EXCO SELECT <%s>", pcpSelKey);
  dbg (TRACE, "========================================");

  (void) get_real_item (pclActCmd, pcpSelKey, 1);
  if (strcmp (pclActCmd, "XFD") == 0)
  {
    ilRC = HandleDfsRadarData (pcpRcvTable, pcpFields, pcpData, pcpSelKey);
  }
  else if (strcmp (pclActCmd, "XXX") == 0)
  {
    dbg (TRACE, "XXX DATA RECEIVED FROM EXCO");
  }
  else if (strcmp (pclActCmd, "YYY") == 0)
  {
    dbg (TRACE, "YYY DATA RECEIVED FROM EXCO");
  }
  else
  {
    dbg (TRACE, "UNKNOWN COMMAND RECEIVED FROM EXCO");
  }                             /* end if else */

  dbg (TRACE, "HANDLE EXCO DATA FINISHED");

  return ilRC;
}                               /* end of HandleExcoData */

/* *************************************************** */
/* *************************************************** */
static int HandleDfsRadarData (char *pcpRcvTable, char *pcpFields, char *pcpData, char *pcpSelKey)
{
  int ilRC = RC_SUCCESS;
  int ilLinIdx = 0;
  int ilTbl = 0;
  int ilFlno = 0;
  int ilRwya = 0;
  int ilEtac = 0;
  int ilTmaa = 0;
  int ilAdid = 0;
  int ilChkFld = 0;
  int ilTistTime = 0;
  char pclTistVafr[16];
  char pclTistVato[16];
  char pclFlightDay[16];
  char pclRelTime[16];
  char pclAftTable[16];
  char pclAftSelKey[128];
  char *pclTistPtr = NULL;
  char pclArrFields[] = "IFRA,LNDA,ETAC";
  char pclDepFields[] = "IFRD,AIRA";

  dbg (TRACE, "===== DATA ELEMENTS FROM INTERFACE =====");
  dbg (TRACE, "DFS TABLE  <%s>", pcpRcvTable);
  dbg (TRACE, "DFS FIELDS <%s>", pcpFields);
  dbg (TRACE, "DFS DATA   <%s>", pcpData);
  dbg (TRACE, "DFS SELECT <%s>", pcpSelKey);
  dbg (TRACE, "========================================");

  ilLinIdx = CheckNewRadarData ("TRKN", "FLNO", "LNDA,AIRA", pcpFields, pcpData);
  if (ilLinIdx > 0)
  {
    dbg (TRACE, "ACTUAL DATA LINE IDX = %d", ilLinIdx);
    /* Get Actual TimeStamp and DayOfEvent from Selection */
    strcpy (pclFlightDay, pcgCurDate);
    strcpy (pclRelTime, pcgCurTime);
    pclTistPtr = strstr (pcpSelKey, "TIST,");
    if (pclTistPtr != NULL)
    {
      pclTistPtr += 5;
      strncpy (pclFlightDay, pclTistPtr, 8);
      pclFlightDay[8] = 0x00;
      strncpy (pclRelTime, pclTistPtr, 14);
      pclRelTime[14] = 0x00;
    }                           /* end if */
    (void) GetFullDay (pclRelTime, &igRelDayStamp, &igRelMinStamp, &igRelWkdStamp);
    ilTistTime = igRelMinStamp - 90;
    (void) FullTimeString (pclTistVafr, ilTistTime, "00");
    ilTistTime = igRelMinStamp + 90;
    (void) FullTimeString (pclTistVato, ilTistTime, "59");

    strcpy (pclAftTable, pcgAftTab);
    ilRC = GetReceivedData (pclAftTable, pcpFields, pcpData);
    ilTbl = GetTableIndex (pclAftTable);
    ilFlno = GetFieldIndex (ilTbl, "FLNO");
    ilRC = CheckArrDepFields (pcpFields, pclArrFields);
    if (ilRC == RC_SUCCESS)
    {
      dbg (TRACE, "RADAR DATA OF ARRIVAL FLIGHT");
      sprintf (pclAftSelKey, "WHERE CSGN='%s' " "AND (TIFA BETWEEN '%s' AND '%s') " "AND ADID='A'",
               prgTblFldCfg[ilTbl][ilFlno].RcvDat, pclTistVafr, pclTistVato);
      if (prgRadarData[ilLinIdx].EventCnt == 1)
      {
        /* RACO: first RadarContact in NM */
        /* What is 'NM' ?? Nautic Miles ? */

        /*
           (void) SetRcvValues(ilTbl, 0, "RACO", "10");
         */

      }                         /* end if */
      ilEtac = GetFieldIndex (ilTbl, "ETAC");
      ilTmaa = GetFieldIndex (ilTbl, "TMAA");
      if (prgTblFldCfg[ilTbl][ilEtac].RcvFlg == TRUE)
      {
        if (prgRadarData[ilLinIdx].LastTmoaVal == 0)
        {
          dbg (DEBUG, "CALCULATE FIRST TMAA: SYS MIN=%d", igSysMinStamp);
          dbg (DEBUG, "TIMESTAMP FROM EVENT: REL MIN=%d", igRelMinStamp);

          dbg (DEBUG, "DFS ETAC <%s> MIN=%d", prgTblFldCfg[ilTbl][ilEtac].RcvDat, prgTblFldCfg[ilTbl][ilEtac].RcvVal);

          if ((prgTblFldCfg[ilTbl][ilEtac].RcvVal - igSysMinStamp) <= igTmoOffset)
          {
            (void) SetRcvValues (ilTbl, 0, "TMAA", prgTblFldCfg[ilTbl][ilEtac].RcvDat);
            (void) ModifyRcvValue (ilTbl, "TMAA", (-igTmoOffset));
            dbg (DEBUG, "DFS TMAA <%s> MIN=%d", prgTblFldCfg[ilTbl][ilTmaa].RcvDat, prgTblFldCfg[ilTbl][ilTmaa].RcvVal);
            strcpy (prgRadarData[ilLinIdx].LastTmoaDat, prgTblFldCfg[ilTbl][ilTmaa].RcvDat);
            prgRadarData[ilLinIdx].LastTmoaVal = prgTblFldCfg[ilTbl][ilTmaa].RcvVal;
          }                     /* end if */
        }                       /* end if */
        else
        {
          dbg (DEBUG, "CHECK LAST TMAA");
          dbg (DEBUG, "DFS ETAC <%s>", prgTblFldCfg[ilTbl][ilEtac].RcvDat);
          dbg (DEBUG, "OLD TMAA <%s>", prgRadarData[ilLinIdx].LastTmoaDat);
        }                       /* end else */
      }                         /* end if */
    }                           /* end if */
    else
    {
      ilRC = CheckArrDepFields (pcpFields, pclDepFields);
      if (ilRC == RC_SUCCESS)
      {
        dbg (TRACE, "RADAR DATA OF DEPARTURE FLIGHT");
        sprintf (pclAftSelKey, "WHERE CSGN='%s' " "AND (TIFD BETWEEN '%s' AND '%s') " "AND ADID='D'",
                 prgTblFldCfg[ilTbl][ilFlno].RcvDat, pclTistVafr, pclTistVato);
        /* Map Field RWYA as RWYD */
        ilRwya = GetFieldIndex (ilTbl, "RWYA");
        (void) SetRcvValues (ilTbl, 0, "RWYD", prgTblFldCfg[ilTbl][ilRwya].RcvDat);
        prgTblFldCfg[ilTbl][ilRwya].RcvFlg = FALSE;
      }                         /* end if */
      else
      {
        dbg (TRACE, "CANNOT DETERMINE DIRECTION OF FLIGHT RADAR DATA");
      }                         /* end else */
    }                           /* end else */

    if (ilRC == RC_SUCCESS)
    {
/*
      (void) ModifyRcvValue(ilTbl,"LNDA",-1);
      (void) ModifyRcvValue(ilTbl,"AIRA",1);
*/
      (void) ShowReceivedData ();
      dbg (TRACE, "SEARCH <%s>", pclAftSelKey);
      ilRC = SearchFlight (pclAftTable, pcpFields, pclAftSelKey, FOR_UPDATE, TRUE);
      if ((ilRC != RC_SUCCESS) || (prgPrjTblCfg[ilTbl].RecHdlCnt < 1))
      {
        dbg (TRACE, "FLIGHT NOT FOUND");
        ilRC = RC_FAIL;
      }                         /* end if */
      if (prgPrjTblCfg[ilTbl].RecHdlCnt > 1)
      {
        dbg (TRACE, "TOO MANY RECORDS (%d)", prgPrjTblCfg[ilTbl].RecHdlCnt);
        ilRC = RC_FAIL;
      }                         /* end if */
    }                           /* end if */

    if (ilRC == RC_SUCCESS)
    {
      /* avoid Update On Field FLNO */
      prgTblFldCfg[ilTbl][ilFlno].RcvFlg = FALSE;

      /* Avoid Bad TMO etc After Landing */
      ilAdid = GetFieldIndex (ilTbl, "ADID");
      switch (prgRecFldDat[ilTbl][1][ilAdid].DbsDat[0])
      {
        case 'A':
          ilChkFld = GetFieldIndex (ilTbl, "LAND");
          if (prgRecFldDat[ilTbl][1][ilChkFld].DbsVal > 0)
          {
            ilRC = RC_FAIL;
            dbg (TRACE, "FLIGHT ALREADY LANDED");
          }                     /* end if */
          break;
        default:
          break;
      }                         /* end switch */
    }                           /* end if */

    if (ilRC == RC_SUCCESS)
    {
      ilRC = ShowRecordData ();
      ilRC = PrepNewData (0);
      dbg (TRACE, "HandleDfsRadarData: UPDATE AND BROADCAST FLIGHT WITH URNO <%s> ", pclAftSelKey);
      ilRC = MakeUpdateAndBroadCast (__LINE__, ilTbl, 0, pclAftSelKey, TRUE, "UFR", "URNO");
    }                           /* end if */

    /* Store last received Radar Data */
    strcpy (prgRadarData[ilLinIdx].ItemList, pcpFields);
    strcpy (prgRadarData[ilLinIdx].DataLine, pcpData);
    prgRadarData[ilLinIdx].LastEventStamp = igSysMinStamp;

    FreeRadarDataGarbage ();

  }                             /* end if */
  else
  {
    ilRC = RC_FAIL;
  }                             /* end if */
  return ilRC;
}                               /* end of HandleDfsRadarData */

/* *************************************************** */
/* *************************************************** */
static void FreeRadarDataGarbage (void)
{
  int ilIdx = 0;
  int ilTimeDiff = 0;
  ilIdx = 1;
  while (ilIdx <= igRadarDataCount)
  {
    if (prgRadarData[ilIdx].FreeLine != TRUE)
    {
      ilTimeDiff = igSysMinStamp - prgRadarData[ilIdx].LastEventStamp;
      if (ilTimeDiff > 180)
      {
        prgRadarData[ilIdx].FreeLine = TRUE;
        dbg (TRACE, "LINE %d SET TO 'FREE'", ilIdx);
      }                         /* end if */
    }                           /* end if */
    ilIdx++;
  }                             /* end while */
  return;
}                               /* end FreeRadarDataGarbage */

/* *************************************************** */
/* *************************************************** */
static void ModifyRcvValue (int ipTbl, char *pcpField, int ipVal)
{
  int ilFld = 0;
  char pclSeconds[] = "00";
  ilFld = GetFieldIndex (ipTbl, pcpField);
  if (ilFld > 0)
  {
    if ((prgTblFldCfg[ipTbl][ilFld].RcvFlg == TRUE) && (prgTblFldCfg[ipTbl][ilFld].RcvVal > 0))
    {
      pclSeconds[0] = prgTblFldCfg[ipTbl][ilFld].RcvDat[12];
      pclSeconds[1] = prgTblFldCfg[ipTbl][ilFld].RcvDat[13];
      pclSeconds[2] = 0x00;
      prgTblFldCfg[ipTbl][ilFld].RcvVal += ipVal;
      (void) FullTimeString (prgTblFldCfg[ipTbl][ilFld].RcvDat, prgTblFldCfg[ipTbl][ilFld].RcvVal, pclSeconds);
    }                           /* end if */
  }                             /* end if */
}                               /* end ModifyRcvValue */

/* *************************************************** */
/* *************************************************** */
static int CheckNewRadarData (char *pcpKeyField, char *pcpChkField, char *pcpEndFields, char *pcpFields, char *pcpData)
{
  int ilLinIdx = 0;
  int ilIdx = 0;
  int ilFreeLine = 0;
  int ilFld = 0;
  int ilLen = 0;
  int ilFldCnt = 0;
  char pclFldNam[8];
  char pclFldDat[32];

  /* Check Key Fields (i.e. FLNO) */
  ilFld = get_item_no (pcpFields, pcpChkField, 5) + 1;
  ilLen = get_real_item (pclFldDat, pcpData, ilFld);
  if (ilLen > 0)
  {
    ilFld = get_item_no (pcpFields, pcpKeyField, 5) + 1;
    ilLen = get_real_item (pclFldDat, pcpData, ilFld);
  }                             /* end if */

  /* Check EventKey Fields (i.e. TRKN) */
  if (ilLen > 0)
  {
    /* Search EventKey in Stored RadarData */
    /* and also look for an empty line to insert new data */
    ilLinIdx = 0;
    ilFreeLine = 0;
    ilIdx = 1;
    while ((ilLinIdx < 1) && (ilIdx <= igRadarDataCount))
    {
      if (strcmp (prgRadarData[ilIdx].LineKeys, pclFldDat) == 0)
      {
        /* Data Line found ! To be sure: reactivate that line */
        prgRadarData[ilIdx].FreeLine = FALSE;
        ilLinIdx = ilIdx;
      }                         /* end if */

      if (ilFreeLine < 1)
      {
        if (prgRadarData[ilIdx].FreeLine == TRUE)
        {
          /* Found a free line */
          ilFreeLine = ilIdx;
        }                       /* end if */
      }                         /* end if */
      ilIdx++;
    }                           /* end while */

    if (ilLinIdx < 1)
    {
      /* New Radar Data */
      if (ilFreeLine < 1)
      {
        if (igRadarDataCount < MAX_DFS_HDL)
        {
          igRadarDataCount++;
        }                       /* end if */
        ilFreeLine = igRadarDataCount;
      }                         /* end if */

      ilLinIdx = ilFreeLine;
      prgRadarData[ilLinIdx].FreeLine = FALSE;
      prgRadarData[ilLinIdx].EventCnt = 0;
      prgRadarData[ilLinIdx].LastTmoaVal = 0;
      strcpy (prgRadarData[ilLinIdx].LineKeys, pclFldDat);
      strcpy (prgRadarData[ilLinIdx].ItemList, pcpFields);
      strcpy (prgRadarData[ilLinIdx].DataLine, pcpData);
    }                           /* end if */

    if (ilLinIdx > 0)
    {
      /* Line just found or stored */
      /* increment EventCounter */
      prgRadarData[ilLinIdx].EventCnt++;

      /* Now check for Last Event */
      ilFldCnt = field_count (pcpEndFields);
      while ((ilFldCnt > 0) && (prgRadarData[ilLinIdx].FreeLine == FALSE))
      {
        (void) get_real_item (pclFldNam, pcpEndFields, ilFldCnt);
        if (strstr (pcpFields, pclFldNam) != NULL)
        {
          prgRadarData[ilLinIdx].FreeLine = TRUE;
        }                       /* end if */
        ilFldCnt--;
      }                         /* end while */

      if (prgRadarData[ilLinIdx].EventCnt > 1)
      {
        if (prgRadarData[ilLinIdx].LastTmoaVal > 0)
        {
          /* Check changed data of the new event */
          /* newer data will be stored by caller */
          /* Normally we get another Fieldlist */
          /* Data will be proved in future prog. */
          if (strcmp (prgRadarData[ilLinIdx].ItemList, pcpFields) == 0)
          {
            dbg (TRACE, "NEW RADAR DATA NOT BETTER");
            ilLinIdx = 0;
          }                     /* end if */
        }                       /* end if */
      }                         /* end if */
    }                           /* end if */
  }                             /* end if */
  else
  {
    dbg (TRACE, "RADAR DATA OF UNDEFINED FLIGHT");
  }                             /* end else */

  return ilLinIdx;
}                               /* end of CheckNewRadarData */

/* *************************************************** */
/* *************************************************** */
static int CheckArrDepFields (char *pcpRcvFields, char *pcpChkFields)
{
  int ilDataType = RC_FAIL;
  int ilFldCnt = 0;
  int ilFldItm = 0;
  char pclFldNam[8];
  ilFldCnt = field_count (pcpRcvFields);
  while ((ilFldItm < ilFldCnt) && (ilDataType != RC_SUCCESS))
  {
    ilFldItm++;
    (void) get_real_item (pclFldNam, pcpRcvFields, ilFldItm);
    if (strstr (pcpChkFields, pclFldNam) != NULL)
    {
      ilDataType = RC_SUCCESS;
    }                           /* end if */
  }                             /* end while */
  return ilDataType;
}                               /* end CheckArrDepFields */

/* *************************************************** */
/* *************************************************** */
static int RejectEvent (char *pcpRcvCmd, char *pcpRcvTable, char *pcpFields, char *pcpData, char *pcpSelKey)
{
  int ilRC = RC_SUCCESS;
  dbg (TRACE, "===== REJECTED EVENT: CMD <%s> =========", pcpRcvCmd);
  dbg (TRACE, "===== DATA ELEMENTS FROM CLIENT =========");
  dbg (TRACE, "REV TABLE  <%s>", pcpRcvTable);
  dbg (TRACE, "REV FIELDS <%s>", pcpFields);
  dbg (TRACE, "REV DATA   <%s>", pcpData);
  dbg (TRACE, "REV SELECT <%s>", pcpSelKey);
  dbg (TRACE, "=========================================");
  (void) tools_send_info_flag (que_out, 0, pcgDestName, "", pcgRecvName, "", "", pcgTwStart, pcgTwEnd, "REV",
                               pcpRcvTable, pcpSelKey, pcpFields, pcpData, 0);
  return ilRC;
}                               /* end RejectEvent */

/* ***************************************************
* Function: 	InsertFlights
* Parameter: 	Table,Fields,Data,Selection
* Return:	RC_SUCCESS, RC_FAIL
* Description:	Inserts flight records.
*******************************************************/
static int InsertFlights (char *pcpRcvTable, char *pcpFields, char *pcpData, char *pcpSelKey, char *pcpDaofDat,
                          int ipAnswFlag)
{
  int ilRC = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  int ilFlightCount = 0;
  int ilRecCnt = 0;
  int ilRecIdx = 0;
  int ilChrPos = 0;
  int ilFtypIdx = 0;
  int ilTifaIdx = 0;
  int ilTifdIdx = 0;
  int ilAdidIdx = 0;
  int ilMinTifaSet = FALSE;
  int ilAftTab = 0;
  int ilCcaTab = 0;
  int ilFld = 0;
  int ilVpfrVal = 0;
  int ilVptoVal = 0;
  int ilVpfrDay = 0;
  int ilVptoDay = 0;
  int ilMinVal = 0;
  int ilStoreIdx = 0;
  int ilLoopDay = 0;
  int ilChckDay = 0;
  int ilWeekDay = 0;
  int ilFreqVal = 0;
  int ilValidWeek = TRUE;
  int ilValidDays = 0;
  int ilFreaCount = 0;
  int ilFredCount = 0;
  int ilWeeks = 0;
  int ilNeededUrnos = 0;
  int ilNeededCcaUrnos = 0;
  int ilDayCount = 0;
  long llActUrno = 0;
  char pclRoutType[16];
  char pclFreaStrg[16];
  char pclFredStrg[16];
  char pclFreaChrs[16];
  char pclFredChrs[16];
  char pclFreqStrg[16];
  char pclVpfrStrg[16];
  char pclVptoStrg[16];
  char pclFtypStrg[16];
  char pclNewSkey[16];
  char pclTmpBuf[128];
  char pclMinTifa[16];
  char pclMaxTifd[16];
  char pclTable[32];
  char pclAutoJof[32];
  char pclRkeyFields[32];
  char pclRkeyData[32];
  char *pclLinBgn = NULL;
  char *pclLinEnd = NULL;
  char *pclJofUrno = NULL;
  char pclTblNam[16];

  if (igDontCallSql != TRUE)
  {
/*
    dbg(TRACE,"===== DATA ELEMENTS FROM CLIENT =========");
    dbg(TRACE,"ISF TABLE  <%s>", pcpRcvTable);
    dbg(TRACE,"ISF FIELDS <%s>", pcpFields);
    dbg(TRACE,"ISF DATA   <%s>", pcpData);
    dbg(TRACE,"ISF SELECT <%s>", pcpSelKey);
    dbg(TRACE,"=========================================");
*/
  }                             /* end if */

  if ((igImportAction == FALSE) && (igClientAction == FALSE))
  {
    /*
       igCheckFkeyFtyp = FALSE;
       igCheckFlightKey = TRUE;
       igDeleteDoubles = TRUE;
     */
  }                             /* end if */

  pclJofUrno = strstr (pcpSelKey, "\n");
  if (pclJofUrno != NULL)
  {
    *pclJofUrno = 0x00;
    pclJofUrno++;
  }                             /* end if */

  strcpy (pclTblNam, pcgAftTab);

  /* Maybe Arrival or Departure Flight */
  igDataLineCount = 1;
  prgDataLines[1].LineFilled = TRUE;
  prgDataLines[1].Fields = pcpFields;
  prgDataLines[1].Data = pcpData;
  prgDataLines[1].Selection = NULL;

  prgDataLines[2].LineFilled = FALSE;
  pclLinEnd = strstr (prgDataLines[1].Fields, "\n");
  if (pclLinEnd != NULL)
  {
    pclLinEnd[0] = 0x00;
    /* Always Departure Flight */
    pclLinBgn = pclLinEnd + 1;
    igDataLineCount = 2;
    prgDataLines[2].LineFilled = TRUE;
    prgDataLines[2].Fields = pclLinBgn;
    pclLinEnd = strstr (prgDataLines[1].Data, "\n");
    if (pclLinEnd != NULL)
    {
      pclLinEnd[0] = 0x00;
      pclLinBgn = pclLinEnd + 1;
      prgDataLines[2].Data = pclLinBgn;
      prgDataLines[2].Selection = NULL;
    }                           /* end if */
  }                             /* end if */

  /* Check for CCA Field and Data (and put it in Line 3) */
  prgDataLines[3].LineFilled = FALSE;
  pclLinEnd = strstr (prgDataLines[igDataLineCount].Data, "\r");
  if (pclLinEnd != NULL)
  {
    pclLinEnd[0] = 0x00;
    prgDataLines[3].LineFilled = TRUE;
    pclLinBgn = pclLinEnd + 1;
    prgDataLines[3].Fields = pclLinBgn;
    pclLinEnd = strstr (prgDataLines[3].Fields, "\n");
    if (pclLinEnd != NULL)
    {
      pclLinEnd[0] = 0x00;
      pclLinBgn = pclLinEnd + 1;
      prgDataLines[3].Data = pclLinBgn;
      prgDataLines[3].Selection = NULL;
    }                           /* end if */
  }                             /* end if */

  dbg (DEBUG, "DATA FOUND FOR %d FLIGHTS", igDataLineCount);

  (void) get_real_item (pclRoutType, pcpSelKey, 8);
  dbg (DEBUG, "ROUTING TYPE <%s>", pclRoutType);

  ilStoreIdx = 1;
  pclFreaStrg[0] = 0x00;
  if (prgDataLines[ilStoreIdx].LineFilled == TRUE)
  {
    /* Maybe Arrival or Departure Flight */
    ilRecIdx = 1;

    dbg (DEBUG, "FIELDS <%s>", prgDataLines[ilStoreIdx].Fields);
    dbg (DEBUG, "DATA   <%s>", prgDataLines[ilStoreIdx].Data);

  TRACE_FIELD_OF_INTEREST(__LINE__,"21");

    ilRC =
      StoreReceivedData (TRUE, 0, ilRecIdx, 0, pcgAftTab, prgDataLines[ilStoreIdx].Fields,
                         prgDataLines[ilStoreIdx].Data);

  TRACE_FIELD_OF_INTEREST(__LINE__,"22");

    /* Hier Routing VIA3/ORG3/DES3 pruefen */
    (void) CheckReceivedRouting (igImportAction, 0, ilRecIdx, pcgAftTab, pclRoutType);

  TRACE_FIELD_OF_INTEREST(__LINE__,"23");


    (void) get_real_item (pclFreaStrg, pcpSelKey, 3);
    (void) get_real_item (pclFreqStrg, pcpSelKey, 4);
  }                             /* end if */

  ilStoreIdx = 2;
  pclFredStrg[0] = 0x00;
  if (prgDataLines[ilStoreIdx].LineFilled == TRUE)
  {
    /* Second Line always is a Departure Flight */
    /* First Line is defined as Arrival Flight */
    ilRecIdx = 2;
    /*
       dbg(DEBUG,"FIELDS <%s>", prgDataLines[ilStoreIdx].Fields);
       dbg(DEBUG,"DATA   <%s>", prgDataLines[ilStoreIdx].Data);
     */

  TRACE_FIELD_OF_INTEREST(__LINE__,"24");

    ilRC =
      StoreReceivedData (TRUE, 0, ilRecIdx, 0, pcgAftTab, prgDataLines[ilStoreIdx].Fields,
                         prgDataLines[ilStoreIdx].Data);

  TRACE_FIELD_OF_INTEREST(__LINE__,"25");

    /* Hier Routing VIA3/ORG3/DES3 pruefen */
    (void) CheckReceivedRouting (igImportAction, 0, ilRecIdx, pcgAftTab, pclRoutType);

  TRACE_FIELD_OF_INTEREST(__LINE__,"26");


    (void) get_real_item (pclFredStrg, pcpSelKey, 4);
    (void) get_real_item (pclFreqStrg, pcpSelKey, 5);
  }                             /* end if */

  ilStoreIdx = 3;
  if (prgDataLines[ilStoreIdx].LineFilled == TRUE)
  {
    ilRecIdx = 1;
    pclLinBgn = prgDataLines[ilStoreIdx].Data;
    ilNeededCcaUrnos = 0;
    do
    {

  TRACE_FIELD_OF_INTEREST(__LINE__,"27");

      pclLinEnd = strstr (pclLinBgn, "\n");
      if (pclLinEnd != NULL)
      {
        pclLinEnd[0] = 0x00;
      }                         /* end if */
      /*
         dbg(DEBUG,"FIELDS <%s>", prgDataLines[ilStoreIdx].Fields);
         dbg(DEBUG,"DATA   <%s>", pclLinBgn);
       */
       
      ilRC = StoreReceivedData (TRUE, 0, ilRecIdx, 0, pcgCcaTab, prgDataLines[ilStoreIdx].Fields, pclLinBgn);
      ilNeededCcaUrnos++;
      if (pclLinEnd != NULL)
      {
        pclLinBgn = pclLinEnd + 1;
        ilRecIdx++;
      }                         /* end if */
    }
    while (pclLinEnd != NULL);
  }                             /* end if */

  TRACE_FIELD_OF_INTEREST(__LINE__,"2");

  if (strstr (pcpSelKey, "WHERE") != NULL)
  {
    (void) get_real_item (pclTmpBuf, pcpSelKey, 1);
    (void) GetWord (2, pclTmpBuf, pclVpfrStrg);
  }                             /* end if */
  else
  {
    (void) get_real_item (pclVpfrStrg, pcpSelKey, 1);
  }                             /* end else */
  (void) get_real_item (pclVptoStrg, pcpSelKey, 2);
  str_trm_all (pclVpfrStrg, " ", TRUE);
  str_trm_all (pclVptoStrg, " ", TRUE);

  ilGetRc = GetFullDay (pclVpfrStrg, &ilVpfrVal, &ilMinVal, &ilVpfrDay);
  if (ilGetRc == RC_SUCCESS)
  {
    ilGetRc = GetFullDay (pclVptoStrg, &ilVptoVal, &ilMinVal, &ilVptoDay);
  }                             /* end if */
  if (ilGetRc == RC_SUCCESS)
  {

    dbg (DEBUG, "PERIOD FROM <%s> DAY (%d) WK DAY (%d)", pclVpfrStrg, ilVpfrVal, ilVpfrDay);
    dbg (DEBUG, "PERIOD TO   <%s> DAY (%d) WK DAY (%d)", pclVptoStrg, ilVptoVal, ilVptoDay);

    ilFreaCount = FormatDayFreq (pclFreaStrg, pclFreaChrs);
    ilFredCount = FormatDayFreq (pclFredStrg, pclFredChrs);

    dbg (DEBUG, "FREA <%s> FRED <%s> WEEKLY FREQ <%s>", pclFreaStrg, pclFredStrg, pclFreqStrg);
    dbg (DEBUG, "FREA <%s> FRED <%s> DAYS <%d> <%d>", pclFreaChrs, pclFredChrs, ilFreaCount, ilFredCount);

    ilFreqVal = atoi (pclFreqStrg);
    if (ilFreqVal < 1)
    {
      ilFreqVal = 1;
    }                           /* end if */
    if (ilFreqVal > 1)
    {
      dbg (TRACE, "WEEKLY FREQUENCY IS %d", ilFreqVal);
    }                           /* end if */
  }                             /* end if */
  else
  {
    dbg (TRACE, "ERROR IN PERIOD FROM <%s> TO <%s>", pclVpfrStrg, pclVptoStrg);
  }                             /* end else */

  if ((ipAnswFlag == TRUE) && (igGlNoAck == FALSE) && (igTWEFromAZFlight == FALSE))
  {
    /* wg. Probleme im Client
       (void) tools_send_info_flag(que_out,0, pcgDestName, "", pcgRecvName,
       "", "", pcgTwStart, pcgTwEnd,
       "ISF",pcgAftTab,"OK","ISF","BOT",0);
     */

    (void) tools_send_info_flag (que_out, 0, pcgDestName, "", pcgRecvName, "", "", pcgTwStart, pcgTwEnd, "ISF",
                                 pcpRcvTable, pcpSelKey, pcpFields, pcpData, 0);

  }                             /* end if */

  if (ilGetRc == RC_SUCCESS)
  {
    ilAftTab = GetTableIndex (pcgAftTab);
    ilCcaTab = GetTableIndex (pcgCcaTab);

    /* +++ jim +++ 20001227: now in HardCodeFkey: 
    if (igDataLineCount > 1)
    {
      StoreReceivedData (FALSE, ilAftTab, 1, 1, "", "RTYP", "J");
      StoreReceivedData (FALSE, ilAftTab, 2, 1, "", "RTYP", "J");
    }
    else
    {
      StoreReceivedData (FALSE, ilAftTab, 1, 1, "", "RTYP", "S");
    }
    +++ jim +++ 20001227: moved to HardCodeFkey  */
  TRACE_FIELD_OF_INTEREST(__LINE__,"3");

    pclVpfrStrg[8] = 0x00;
    ilRC = BuildTimeValues (ilVpfrVal, pclVpfrStrg, pcpDaofDat);
dbg(DEBUG,"%05d:",__LINE__);
    (void) SetNewData ();
dbg(DEBUG,"%05d:",__LINE__);
  TRACE_FIELD_OF_INTEREST(__LINE__,"4");

    /* CALCULATE MAX. OF NEEDED URNOS */
    ilWeeks = ((ilVptoVal - ilVpfrVal + 1) / 7) / ilFreqVal;
    ilWeeks++;                  /* to be sure */
    /* dbg(DEBUG,"WEEKS = %d",ilWeeks); */
    ilValidDays = ilWeeks * ilFreaCount * igDataLineCount;
    /* dbg(DEBUG,"DAYS = %d",ilValidDays); */
    /* dbg(DEBUG,"CCA LINES = %d",ilNeededCcaUrnos); */
    ilNeededUrnos = ilValidDays + (ilWeeks * ilFreaCount * ilNeededCcaUrnos);

    /* Individual RKEY's no longer used */
/* ilNeededUrnos += ilWeeks * ilFreaCount; *//* For RKEY's */

    ilNeededUrnos++;            /* For SKEY */
    pcgDataArea[0] = 0x00;
    /* dbg(TRACE,"NEEDED URNOS = %d", ilNeededUrnos); */
    ilGetRc = GetNextValues (pcgDataArea, ilNeededUrnos);
    /* dbg(TRACE,"URNO FIRST VALUE = <%s>", pcgDataArea); */
  }                             /* end if */

  if (ilGetRc == RC_SUCCESS)
  {
    llActUrno = atol (pcgDataArea);
    sprintf (pclNewSkey, "%ld", llActUrno);
    /* dbg(DEBUG,"SKEY <%s>",pclNewSkey); */
    llActUrno++;

    ilAdidIdx = GetFieldIndex (ilAftTab, "ADID");
    ilTifaIdx = GetFieldIndex (ilAftTab, "TIFA");
    ilTifdIdx = GetFieldIndex (ilAftTab, "TIFD");
    ilFtypIdx = GetFieldIndex (ilAftTab, "FTYP");
    sprintf (pclMinTifa, "%s000000", pclVpfrStrg);
    sprintf (pclMaxTifd, "%s235959", pclVptoStrg);
    ilMinTifaSet = FALSE;

    ilWeekDay = ilVpfrDay;
    ilValidWeek = TRUE;
    ilValidDays = 0;
    ilDayCount = -1;
    ilFlightCount = 0;
  TRACE_FIELD_OF_INTEREST(__LINE__,"5");
    dbg (DEBUG, "LOOP THROUGH PERIOD");
    for (ilLoopDay = ilVpfrVal; ilLoopDay <= ilVptoVal; ilLoopDay++)
    {
      igTWEUpdate = FALSE;
      ilDayCount++;
      if (ilValidWeek == TRUE)
      {
        ilChckDay = ilWeekDay - 1;
        if (pclFreaChrs[ilChckDay] != ' ')
        {
          /* dbg(TRACE,"PREPARE FLIGHT DATA OF DAY %d",ilLoopDay); */
/*  TWE 20012000 */
/* INSERT TIME FRAME */
          ilRC = SetNewTimeValues (ilDayCount);
          if (ilRC != RC_SUCCESS)
          {                     /* flighttime < pcgBorderTime */
            dbg (TRACE, "InsertFlights: NO INSERT/UPDATE FOR FLIGHT");
            dbg (TRACE, "InsertFlights: ITS ALREADY HANDLED BY ACTUAL DATA");
          }
          else
          {
/****************/
            ilFlightCount += igDataLineCount;
            igCurrentAction = FOR_INSERT;

  TRACE_FIELD_OF_INTEREST(__LINE__,"6");

            ilGetRc = HandleFlightData (ilAftTab); /* AFT */

  TRACE_FIELD_OF_INTEREST(__LINE__,"7");

            ilGetRc = HandleFlightData (ilCcaTab); /* CCA */

  TRACE_FIELD_OF_INTEREST(__LINE__,"8");

            ShowNewData ();
            if (igDontCallSql != TRUE)
            {
              if ((igTWEImport == TRUE) && (igTWEUpdate == TRUE))
              {
                dbg (TRACE, "InsertFlights: UPDATE AND BROADCAST FLIGHT WITH URNO <%s> ", pcgTWEUpdUrno);
                ilRC = MakeUpdateAndBroadCast (__LINE__,ilAftTab, 0, "", TRUE, "UFR", "URNO");
                /* +++ jim +++ 20010123: STOD may change, check CCA: */
                if (ilRC == RC_SUCCESS)
                {
                  /* ATTENTION: */
                  /* Never Answer Client before this Call */
                  /* Due to Problems of CCA Class in Clients */
                  (void) CheckCcaHdlCall ();
                }                         /* end if */
              }
              else
              {
                dbg (TRACE, "INSERT AND BROADCAST FLIGHT WITH URNO <%s> ", pcgTWEUpdUrno);

                (void) SetUrnoKeys (pclNewSkey, &llActUrno);
                ilRC = MakeInsertAndBroadCast (ilAftTab, 0, "", igBcIfr, "IFR", "SKEY,RKEY,URNO");
                ilRC = MakeInsertAndBroadCast (ilCcaTab, 0, "", igBcIfr, "ICR", "FLNU,URNO");
              }

              if (ilMinTifaSet == FALSE)
              {
                /* The following loop collects significant data */
                /* to broadcast the timeframe and FTYP's of ISF */
                ilRecCnt = prgPrjTblCfg[ilAftTab].RecHdlCnt;
                for (ilRecIdx = ilRecCnt; ilRecIdx > 0; ilRecIdx--)
                {
                  switch (prgRecFldDat[ilAftTab][ilRecIdx][ilAdidIdx].NewDat[0])
                  {
                    case 'D':
                      strcpy (pclMinTifa, prgRecFldDat[ilAftTab][ilRecIdx][ilTifdIdx].NewDat);
                      break;
                    case 'A':
                      strcpy (pclMinTifa, prgRecFldDat[ilAftTab][ilRecIdx][ilTifaIdx].NewDat);
                      break;
                    default:
                      break;
                  }             /* end switch */
                }               /* end for */
                ilMinTifaSet = TRUE;
              }                 /* end if ilMinTifaSet */
            }                   /* end if sql call yes */
            else
            {
                  dbg (TRACE, "Line %d: sql call skipped", __LINE__);
            }
          }                     /* end if Timeframe ok */
        }                       /* end if */
        ilValidDays++;
        if (ilValidDays == 7)
        {
          ilValidDays = (ilFreqVal - 1) * 7;
          if (ilValidDays > 0)
          {
            ilValidWeek = FALSE;
          }                     /* end if */
        }                       /* end if */
      }                         /* end if */
      else
      {
        /* Skip Over Not Valid Days */
        ilValidDays--;
        if (ilValidDays < 1)
        {
          ilValidWeek = TRUE;
        }                       /* end if */
      }                         /* end else */

      ilWeekDay++;
      if (ilWeekDay > 7)
      {
        ilWeekDay = 1;
      }                         /* end if */
    }                           /* end for */

    dbg (DEBUG, "RESULT %d FLIGHTS (%d RECORDS PER INSERT)", ilFlightCount, igDataLineCount);
    if ((igTWEFromAZFlight == TRUE) && (igGlNoAck == FALSE))
    {
      /* wg. Probleme im Client
         (void) tools_send_info_flag(que_out,0, pcgDestName, "", pcgRecvName,
         "", "", pcgTwStart, pcgTwEnd,
         "ISFAZ",pcgAftTab,"OK","ISFAZ","BOT",0);
       */
      (void) tools_send_info_flag (que_out, 0, pcgDestName, "", pcgRecvName, "", "", pcgTwStart, pcgTwEnd, "ISF",
                                   pcpRcvTable, pcpSelKey, pcpFields, pcpData, 0);
    }                           /* end if */

    /* Already Commited by InsertFunction */
    /* commit_work(); */

    if (igDontCallSql != TRUE)
    {
      if (igBroadCast == TRUE)
      {
        /* The following loop collects significant data */
        /* to broadcast the timeframe and FTYP's of ISF */
        ilRecCnt = prgPrjTblCfg[ilAftTab].RecHdlCnt;
        ilChrPos = 0;
        for (ilRecIdx = 1; ilRecIdx <= ilRecCnt; ilRecIdx++)
        {
          pclFtypStrg[ilChrPos] = prgRecFldDat[ilAftTab][ilRecIdx][ilFtypIdx].NewDat[0];
          ilChrPos++;
          switch (prgRecFldDat[ilAftTab][ilRecIdx][ilAdidIdx].NewDat[0])
          {
            case 'A':
              strcpy (pclMaxTifd, prgRecFldDat[ilAftTab][ilRecIdx][ilTifaIdx].NewDat);
              break;
            case 'D':
              strcpy (pclMaxTifd, prgRecFldDat[ilAftTab][ilRecIdx][ilTifdIdx].NewDat);
              break;
            default:
              break;
          }                     /* end switch */
        }                       /* end for */
        pclFtypStrg[ilChrPos] = 0x00;

        sprintf (prgPrjTblCfg[ilAftTab].OutDatBlk, "%s,%s,%s,%s", pclNewSkey, pclFtypStrg, pclMinTifa, pclMaxTifd);
        (void) tools_send_info_flag (igToBcHdl, 0, pcgDestName, "", pcgRecvName, "", "", pcgTwStart, pcgTwEnd, "ISF",
                                     prgPrjTblCfg[ilAftTab].FullName, pcpSelKey, "SKEY,FTYP,TIFA,TIFD",
                                     prgPrjTblCfg[ilAftTab].OutDatBlk, 0);
        dbg (TRACE, "BROADCASTED 'ISF'\nSEL <%s>\n" "FLD <SKEY,FTYP,TIFA,TIFD>\nDAT <%s>", pcpSelKey,
             prgPrjTblCfg[ilAftTab].OutDatBlk);
      }                         /* end if */

  TRACE_FIELD_OF_INTEREST(__LINE__,"InsertFlights 2");

      if (ilRC == RC_SUCCESS)
        if ((igTWEImport == FALSE) || (igJIMCheckRkeys == TRUE))
        {
          ilRC = CheckRkeyUpdates ();

          if (pclJofUrno != NULL)
          {
            ilFld = GetFieldIndex (ilAftTab, "URNO");
            sprintf (pclAutoJof, "%s,0,%s,0", pclJofUrno, prgRecFldDat[ilAftTab][1][ilFld].NewDat);
            strcpy (pclRkeyFields, "RTYP");
            strcpy (pclRkeyData, "R");
            strcpy (pclTable, pcgAftTab);
            ilRC = ResetDataBuffer (__LINE__ ,TRUE, 0, 0, 0, 0, 0, 0);
            ilRC = JoinFlights (pclTable, pclRkeyFields, pclRkeyData, pclAutoJof, FALSE);
          }                     /* end if */

        }                       /* end if */
    }                           /* end if */
  }                             /* end if */

  return ilRC;
}                               /* end InsertFlights */

/* ****************************************************** */
/* ****************************************************** */
static int SetUrnoKeys (char *pcpNewSkey, long *plpActUrno)
{
  int ilRC = RC_SUCCESS;
  int ilTbl = -1;
  int ilRec = -1;
  int ilRecCnt = -1;
  char pclNewRkey[16];
  char pclNewUrno[16];
  char pclAftUrno[16];
  char pclAftAurn[16];

  /* Sets URNO,RKEY,SKEY,AURN of AFT Tables */
  /* and URNO,FLNU of CKI Tables */
  /* The Value of RKEY is set to the first used URNO */

  sprintf (pclNewRkey, "%ld", *plpActUrno);

  /* (*plpActUrno)++; DO NOT INCREASE */

  for (ilTbl = 1; ilTbl <= igHandledTables; ilTbl++)
  {
    if (prgPrjTblCfg[ilTbl].RcvFlg == TRUE)
    {
      ilRecCnt = prgPrjTblCfg[ilTbl].RecHdlCnt;
      strcpy (pclAftAurn, " ");
      for (ilRec = 1; ilRec <= ilRecCnt; ilRec++)
      {
        sprintf (pclNewUrno, "%ld", *plpActUrno);
        (*plpActUrno)++;
        (void) SetNewValues (ilTbl, ilRec, 0, "URNO", pclNewUrno);
        if (prgPrjTblCfg[ilTbl].TypeOfTable == 1)
        {                       /* AFT */
          (void) SetNewValues (ilTbl, ilRec, 0, "SKEY", pcpNewSkey);
          (void) SetNewValues (ilTbl, ilRec, 0, "RKEY", pclNewRkey);
          (void) SetNewValues (ilTbl, ilRec, 0, "AURN", pclAftAurn);
          UnSetNewValues (ilTbl, ilRec, 0, "CHGI", TRUE);
          strcpy (pclAftUrno, pclNewUrno);
          strcpy (pclAftAurn, pclNewUrno);
        }                       /* end if */
        if (prgPrjTblCfg[ilTbl].TypeOfTable == 2)
        {                       /* CCA */
          (void) SetNewValues (ilTbl, ilRec, 0, "FLNU", pclAftUrno);
        }                       /* end if */
      }                         /* end for */
    }                           /* end if */
  }                             /* end for */
  return ilRC;
}                               /* end SetUrnoKeys */

/**************************************************************/
/**************************************************************/
static int MakeInsertAndBroadCast (int ipTbl, int ipRecIdx, char *pcpSelKey, int ipSendBc, char *pcpBcCmd,
                                   char *pcpBcSelList)
{
  int ilRC = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  int ilRec = -1;
  int ilFirstRec = -1;
  int ilRecCnt = -1;
  int ilFld = 0;
  int ilFldNbr = 0;
  int ilFldCnt = 0;
  int ilChrPos = 0;
  int ilUrnoIdx = 0;
  int ilHopoIdx = 0;
  short slInsCursor;
  char pclFldNam[8];
  char pclInsCmdKey[8];

  if (ilRC == RC_SUCCESS)
  {
    if (igImportAction == TRUE)
    {
      strcpy (pclInsCmdKey, "ISR");
    }                           /* end if */
    else
    {
      strcpy (pclInsCmdKey, "IFR");
    }                           /* end else */
    if (ipRecIdx > 0)
    {
      ilFirstRec = ipRecIdx;
      ilRecCnt = ipRecIdx;
    }                           /* end if */
    else
    {
      ilFirstRec = 1;
      ilRecCnt = prgPrjTblCfg[ipTbl].RecHdlCnt;
    }                           /* end else */
    ilUrnoIdx = GetFieldIndex (ipTbl, "URNO");
    if (strcmp (pcgDefTblExt, "TAB") == 0)
    {
      ilHopoIdx = GetFieldIndex (ipTbl, "HOPO");
    }                           /* end if */
    dbg (DEBUG, "TABLE <%s> HAS %d RECORDS FOR INSERT", prgPrjTblCfg[ipTbl].FullName, ilRecCnt);
    for (ilRec = ilFirstRec; ilRec <= ilRecCnt; ilRec++)
    {
      if (ilHopoIdx > 0)
      {
        (void) SetNewValues (ipTbl, ilRec, ilHopoIdx, "", pcgH3LC);
      }                         /* end if */
      ilFldCnt = CollectChanges (ipTbl, ilRec);
      if (ilFldCnt > 0)
      {
        ilRC = BuildSqlFldStrg (pcgTmpBuf1, prgPrjTblCfg[ipTbl].SqlFldLst, FOR_INSERT);
        sprintf (pcgSqlBuf, "INSERT INTO %s %s", prgPrjTblCfg[ipTbl].FullName, pcgTmpBuf1);

        strcpy (pcgDataArea, prgPrjTblCfg[ipTbl].SqlDatBlk);
        delton (pcgDataArea);
        slInsCursor = 0;
        
        ilGetRc = sql_if (IGNORE, &slInsCursor, pcgSqlBuf, pcgDataArea);
        
        if (ilGetRc == DB_SUCCESS)
        {
          commit_work ();
          igTotalFlights++;
          if ((igTWEImport == FALSE) || (igTWEForwardAll == TRUE))
          {
            AddRkeyToList (ipTbl, ilRec, "");
          }
          (void) ReleaseActionInfo (pcgOutRoute, prgPrjTblCfg[ipTbl].FullName, pclInsCmdKey,
                                    prgRecFldDat[ipTbl][ilRec][ilUrnoIdx].NewDat,
                                    prgRecFldDat[ipTbl][ilRec][ilUrnoIdx].NewDat, prgPrjTblCfg[ipTbl].OutFldLst,
                                    prgPrjTblCfg[ipTbl].OutDatBlk, prgPrjTblCfg[ipTbl].OldDatBlk);
          /*
             dbg(TRACE,"----------------------------------------------");
             dbg(TRACE,"SQL FLD:\n<%s>",prgPrjTblCfg[ipTbl].SqlFldLst);
             dbg(TRACE,"SQL DATA\n<%s>",prgPrjTblCfg[ipTbl].SqlDatBlk);
             dbg(TRACE,"----------------------------------------------");
           */
          ilRC = RC_SUCCESS;
        }                       /* end if */
        else
        {
          dbg (TRACE, "ORA INSERT FAILED");
          dbg (TRACE, "Fields <%s>\nData <%s>", pcgSqlBuf, pcgDataArea);
          rollback ();
          ilRC = RC_FAIL;
        }                       /* end else */
        close_my_cursor (&slInsCursor);
      }                         /* end if */

      if ((ilRC == RC_SUCCESS) && (ipSendBc == TRUE) && (ilFldCnt > 0))
      {
        ilFldCnt = field_count (pcpBcSelList);
        ilChrPos = 0;
        prgPrjTblCfg[ipTbl].OutSelKey[ilChrPos] = 0x00;
        for (ilFldNbr = 1; ilFldNbr <= ilFldCnt; ilFldNbr++)
        {
          (void) get_real_item (pclFldNam, pcpBcSelList, ilFldNbr);
          ilFld = GetFieldIndex (ipTbl, pclFldNam);

          StrgPutStrg (prgPrjTblCfg[ipTbl].OutSelKey, &ilChrPos, prgRecFldDat[ipTbl][ilRec][ilFld].NewDat, 0, -1, ",");
        }                       /* end for */
        if (ilChrPos > 0)
        {
          ilChrPos--;
          prgPrjTblCfg[ipTbl].OutSelKey[ilChrPos] = 0x00;
        }                       /* end if */

        dbg (TRACE, "InsertAndBroadcast: Cmd <%s>\nFields <%s>\nData <%s>", pcpBcCmd, prgPrjTblCfg[ipTbl].OutFldLst,
             prgPrjTblCfg[ipTbl].OutDatBlk);

        (void) tools_send_info_flag (igToBcHdl, 0, pcgDestName, "", pcgRecvName, "", "", pcgTwStart, pcgTwEnd, pcpBcCmd,
                                     prgPrjTblCfg[ipTbl].FullName, prgPrjTblCfg[ipTbl].OutSelKey,
                                     prgPrjTblCfg[ipTbl].OutFldLst, prgPrjTblCfg[ipTbl].OutDatBlk, 0);

        dbg (TRACE, "BROADCASTED '%s' SEL <%s>", pcpBcCmd, prgPrjTblCfg[ipTbl].OutSelKey);

      }                         /* end if */
    }                           /* end for */
  }                             /* end if */

  return ilRC;
}                               /* end MakeInsertAndBroadCast */

/* ***************************************************
* Function: 		UpdateFlights
* Parameter: 		Fields,Data,Selection
* Return:		RC_SUCCESS, RC_FAIL
* Description:	Updates flight records.
*******************************************************/
static int UpdateFlights (char *pcpRcvTable, char *pcpFields, char *pcpData, char *pcpSelKey, int ipAnswFlag)
{
  int ilRC = RC_SUCCESS;
  int ilAnswerClient = TRUE;
  int ilMultiple = FALSE;
  int ilLinCnt = 0;
  int ilTbl = -2;
  char pclTable[32];
  char *pclFldBgn = NULL;
  char *pclFldEnd = NULL;
  char *pclDatBgn = NULL;
  char *pclDatEnd = NULL;
  char *pclSelBgn = NULL;
  char *pclSelEnd = NULL;

  ilAnswerClient = ipAnswFlag;

  dbg (TRACE, "===== DATA ELEMENTS FROM CLIENT =========");
  dbg (TRACE, "UFR TABLE  <%s>", pcpRcvTable);
  dbg (TRACE, "UFR FIELDS <%s>", pcpFields);
  dbg (TRACE, "UFR DATA   <%s>", pcpData);
  dbg (TRACE, "UFR SELECT <%s>", pcpSelKey);
  dbg (TRACE, "=========================================");

  pclFldBgn = pcpFields;
  pclDatBgn = pcpData;
  pclSelBgn = pcpSelKey;

  do
  {
    (void) ResetDataBuffer (__LINE__ ,TRUE, 0, 0, 0, 0, 0, 0);

    ilRC = RC_SUCCESS;
    ilTbl = -2;
    ilLinCnt++;

    pclDatEnd = strstr (pclDatBgn, "\n");
    if (pclDatEnd != NULL)
    {
      *pclDatEnd = 0x00;
    }                           /* end if */

    pclFldEnd = strstr (pclFldBgn, "\n");
    if (pclFldEnd != NULL)
    {
      *pclFldEnd = 0x00;
    }                           /* end if */

    pclSelEnd = strstr (pclSelBgn, "\n");
    if (pclSelEnd != NULL)
    {
      if (ilMultiple != TRUE)
      {
        dbg (TRACE, "===== MULTIPLE UPDATES RECEIVED =========");
        ilMultiple = TRUE;
      }                         /* end if */
      *pclSelEnd = 0x00;
    }                           /* end if */

    if (ilMultiple == TRUE)
    {
      dbg (TRACE, "----- %d. LINE IN MULTIPLE DATA --------", ilLinCnt);
      dbg (TRACE, "UFR FIELDS <%s>", pclFldBgn);
      dbg (TRACE, "UFR DATA   <%s>", pclDatBgn);
      dbg (TRACE, "UFR SELECT <%s>", pclSelBgn);
      dbg (TRACE, "-----------------------------------------");
    }                           /* end if */

    if (strstr (pclSelBgn, "WHERE") == NULL)
    {
      dbg (TRACE, "MISSING WHERE CLAUSE IN SELECTION");
      ilRC = RC_FAIL;
    }                           /* end if */

    if (strstr (pclSelBgn, "()") != NULL)
    {
      /* May Come From CTX (SitaTelex) */
      /* To avoid OraErrors */
      dbg (TRACE, "STATEMENT IGNORED BY SELECTION SYNTAX");
      ilRC = RC_FAIL;
    }                           /* end if */

    if (ilRC == RC_SUCCESS)
    {
      strcpy (pclTable, pcgAftTab);
      ilTbl = GetTableIndex (pclTable);
      ilRC = GetReceivedData (pclTable, pclFldBgn, pclDatBgn);
      ilRC = ShowReceivedData ();
      ilRC = SearchFlight (pclTable, pclFldBgn, pclSelBgn, FOR_UPDATE, TRUE);
      if (ilRC != RC_SUCCESS)
      {
        dbg (TRACE, "FLIGHT NOT FOUND");
        StoreErrorData ("4710", "98", "Flight not found", "");
      }                         /* end if */
    }                           /* end if */

    if (ilRC == RC_SUCCESS)
    {
      (void) ShowRecordData ();
    }                           /* end if */

    if (ilRC == RC_SUCCESS)
    {
      ilRC = PrepNewData (0);
      dbg (TRACE, "UpdateFlights: UPDATE AND BROADCAST FLIGHT WITH URNO <%s> ", pclSelBgn);
      ilRC = MakeUpdateAndBroadCast (__LINE__,ilTbl, 0, pclSelBgn, TRUE, "UFR", "URNO");
    }                           /* end if */

    if (ilRC == RC_SUCCESS)
    {
      /* ATTENTION: */
      /* Never Answer Client before this Call */
      /* Due to Problems of CCA Class in Clients */
      (void) CheckCcaHdlCall ();
    }                         /* end if */

    if ((ilAnswerClient == TRUE) && (igGlNoAck == FALSE))
    {
      if (ilRC == RC_SUCCESS)
      {
        (void) tools_send_info_flag (que_out, 0, pcgDestName, "", pcgRecvName, "", "", pcgTwStart, pcgTwEnd, "UFR",
                                     pclTable, "OK", "UFR", "BOT", 0);
      }                       /* end if */
      else
      {
        (void) CollectErrorData (pcgDataArea);
        (void) tools_send_info_flag (que_out, 0, pcgDestName, "", pcgRecvName, "", "", pcgTwStart, pcgTwEnd, "ERR",
                                     pclTable, "UFR", "UFR", pcgDataArea, 0);
      }                       /* end else */
      ilAnswerClient = FALSE;
    }                         /* end if */

    if (ilRC == RC_SUCCESS)
    {
      (void) CheckRkeyUpdates ();
    }                           /* end if */

    /* dbg(TRACE,"========================================="); */

    if (pclDatEnd != NULL)
    {
      pclDatBgn = pclDatEnd + 1;
    }                           /* end if */

    if (pclFldEnd != NULL)
    {
      pclFldBgn = pclFldEnd + 1;
    }                           /* end if */

    if (pclSelEnd != NULL)
    {
      pclSelBgn = pclSelEnd + 1;
    }                           /* end if */

  }
  while (pclSelEnd != NULL);

  return ilRC;
}                               /* end UpdateFlights */

/**************************************************************/
/**************************************************************/
static int CheckRkeyUpdates (void)
{
  int ilRC = RC_SUCCESS;
  int ilRkeyUpdate = FALSE;
  int ilAutoSplit = FALSE;
  int ilAutoJoin = FALSE;
  int ilStateSplit = FALSE;
  int ilRegnFound = FALSE;
  int ilTbl = -2;
  int ilTblItm = 0;
  int ilTblCnt = 0;
  int ilFldItm = 0;
  int ilFldCnt = 0;
  int ilFld = 0;
  int ilFtyp = 0;
  int ilAdid = 0;
  int ilRegn = 0;
  int ilRkey = 0;
  int ilUrno = 0;
  int ilTifd = 0;
  int ilStat = 0;
  int ilPsta = 0;
  int ilDatPos = 0;
  int ilFldPos = 0;
  char pclTable[32];
  char pclActTbl[32];
  char pclFldNam[32];
  char pclRkeyFields[1024];
  char pclRkeyData[1024];
  char pclRkeyWhere[512];
  char pclArrRkey[16];
  char pclArrUrno[16];
  char pclDepRkey[16];
  char pclArrPsta[16];
  char pclTmpDataArea[1024];

  char pclRkeyCheckFields[] = "REGN,ACT5";
  char pclTifaMin[32];

  dbg (DEBUG, "IN CHECK RKEY UPDATES NOW");
  TRACE_FIELD_OF_INTEREST(__LINE__,"CheckRkeyUpdates 1");

  /* CHECK FOR ADDITIONAL UPDATES ON RKEY */
  /* THIS FUNCTION IS ONLY VALID FOR FLIGHT RECORDS */
  /* NOT FOR HANDLING OF TOWINGS OR GROUNDMOVEMENTS */

  ilTbl = GetTableIndex (pcgAftTab);
  ilFtyp = GetFieldIndex (ilTbl, "FTYP");
  ilAdid = GetFieldIndex (ilTbl, "ADID");
  ilRkey = GetFieldIndex (ilTbl, "RKEY");

  strcpy (pclTmpDataArea, prgRecFldDat[ilTbl][1][GetFieldIndex (ilTbl, "FKEY")].NewDat);
  dbg (TRACE, "CHECK RKEY from: <%s>",pclTmpDataArea);
  /* That is a QuickHack */
  pclArrPsta[0] = 0x00;
  ilPsta = GetFieldIndex (ilTbl, "PSTA");
  if ((prgRecFldDat[ilTbl][1][ilAdid].NewDat[0] == 'A') &&
      /* (prgRecFldDat[ilTbl][1][ilPsta].NewFlg == TRUE) && */
      (prgRecFldDat[ilTbl][1][ilPsta].NewLen > 0))
  {
    strcpy (pclArrPsta, prgRecFldDat[ilTbl][1][ilPsta].NewDat);
    strcpy (pclArrRkey, prgRecFldDat[ilTbl][1][ilRkey].NewDat);
    if (atoi(pclArrRkey) == 0 )
    {
       dbg (TRACE, "RKEY from NewDat is 0!");
       if (atoi(prgRecFldDat[ilTbl][1][ilRkey].DbsDat) > 0)
       {
          strcpy (pclArrRkey, prgRecFldDat[ilTbl][1][ilRkey].DbsDat);
          dbg (TRACE, "using RKEY from DbsDat: <%s> !",pclArrRkey);
       }
    }
    /* dbg(TRACE,"FOUND PSTA <%s>",pclArrPsta); */
  }                             /* end if */
  else
  {
    ilPsta = 0;
  }                             /* end else */
  /* End QuickHack */

  TRACE_FIELD_OF_INTEREST(__LINE__,"CheckRkeyUpdates 2");

  /* RkeyReadFields is a list of Aircraft DataFields. That means: */
  /* the Values of RkeyReadFields and the related Basicdata must */
  /* be identical in all following Records of Same RKEY including */
  /* all Circulation Flights, Towings and GroundMovements.        */
  /* The Only Exception may be The SeatVersion (NOSE) and */
  /* (perhaps) the Annex of that Aircraft (ANNX). So we cannot */
  /* perform a general Update on Rkey. At least we have to evaluate */
  /* all BasicData Rules. So we must generate an Update for each */
  /* record of the Rkey Chain. */

  /* Possible Cases of Reaction: */

  /* Case 1: Departure Flight */
  /* If one of these field values is changed in a DepartureFlight */
  /* we have to Split the Rkey Chain, because an OutGoing Aircraft */
  /* different from the landed Aircraft makes no sense. */
  /* If the Registration has Changed, we can try to Join the Departure */
  /* Flight Record to the last Arrival of that Aircraft. */

  /* Case 2: Arrival Flight */
  /* If one of these field values is changed in an ArrivalFlight */
  /* Record, we transfer all values to all Records of the */
  /* RKEY Chain including the DepartureFlight. */

  /* Other Cases: Towings or GoundMovements */
  /* There is no Possibility in ClientApplications to change */
  /* these Values for Records Type 'T' or 'G' */

  /* Due to the technic of storing data in internal WorkAround */
  /* we always have to look for the first record in the Memory */
  /* DataTable of AftTab: */

  ilRegn = GetFieldIndex (ilTbl, "REGN");
  ilUrno = GetFieldIndex (ilTbl, "URNO");

  ilFldCnt = field_count (pclRkeyCheckFields);
  ilDatPos = 0;
  ilAutoJoin = FALSE;
  ilAutoSplit = FALSE;
  ilRkeyUpdate = FALSE;
  ilRegnFound = FALSE;

  /* But First we look for CXX/DIV or Reactivated Status */

  ilStateSplit = FALSE;

  ilStat = GetFieldIndex (ilTbl, "STAT");
  if (prgRecFldDat[ilTbl][1][ilStat].NewFlg == TRUE)
  {
    dbg (TRACE, "STATUS CHANGED: DBS <%s> NEW <%s>", prgRecFldDat[ilTbl][1][ilStat].DbsDat,
         prgRecFldDat[ilTbl][1][ilStat].NewDat);
    if ((strstr ("DIV,CXX,NOP,RRO,DEL", prgRecFldDat[ilTbl][1][ilStat].NewDat) != NULL) &&
        (strstr ("DIV,CXX,NOP,RRO,DEL", prgRecFldDat[ilTbl][1][ilStat].DbsDat) == NULL))
    {
      dbg (TRACE, "AUTOSPLIT DUE TO CHANGED STATUS");
      ilStateSplit = TRUE;
    }                           /* end if */
    else if ((strstr (" ,ERR", prgRecFldDat[ilTbl][1][ilStat].NewDat) != NULL) &&
             (strstr (" ,ERR", prgRecFldDat[ilTbl][1][ilStat].DbsDat) == NULL))
    {
      dbg (TRACE, "REACTIVATED FLIGHT RECORD");
    }                           /* end else if */
    else
    {
      dbg (TRACE, "NO FURTHER ACTION ON CHANGED STATUS");
    }                           /* end if */
  }                             /* end if */

  TRACE_FIELD_OF_INTEREST(__LINE__,"CheckRkeyUpdates 3");

  ilFldPos = 0;
  ilDatPos = 0;
  pclRkeyFields[ilFldPos] = 0x00;
  pclRkeyData[ilDatPos] = 0x00;

  for (ilFldItm = 1; ilFldItm <= ilFldCnt; ilFldItm++)
  {
    (void) get_real_item (pclFldNam, pclRkeyCheckFields, ilFldItm);
    ilFld = GetFieldIndex (ilTbl, pclFldNam);
    if (ilFld > 0)
    {
      if (ilFld == ilRegn)
      {
        if (prgRecFldDat[ilTbl][1][ilFld].NewLen > 0)
        {
          ilRegnFound = TRUE;
          AddRegnToList (prgRecFldDat[ilTbl][1][ilFld].DbsDat);
          AddRegnToList (prgRecFldDat[ilTbl][1][ilFld].NewDat);
          if (igCurrentAction == FOR_INSERT)
          {
            dbg (TRACE, "FIELD REGN FLAGGED AS 'NEW'");
            prgRecFldDat[ilTbl][1][ilFld].NewFlg = TRUE;
          }                     /* end if */
        }                       /* end if */
      }                         /* end if */
      if ((prgRecFldDat[ilTbl][1][ilFld].NewFlg == TRUE) &&
          (prgRecFldDat[ilTbl][1][ilFld].NewLen > 0 )       )
      {
/*
    dbg (DEBUG, "FIELD <%s> RCV <%s> DBS <%s> NEW <%s> FLG (%d)", prgTblFldCfg[ilTbl][ilFld].SysTabFina,
         prgRecFldDat[ilTbl][1][ilFld].RcvDat, prgRecFldDat[ilTbl][1][ilFld].DbsDat,
         prgRecFldDat[ilTbl][1][ilFld].NewDat, prgRecFldDat[ilTbl][1][ilFld].NewFlg);
*/
        StrgPutStrg (pclRkeyFields, &ilFldPos, pclFldNam, 0, -1, ",");
        /* +++ jim +++ 20001228: Fields may be evaluated and not received! */
/*      StrgPutStrg (pclRkeyData, &ilDatPos, prgTblFldCfg[ilTbl][ilFld].RcvDat, 0, -1, ","); */
        StrgPutStrg (pclRkeyData, &ilDatPos, prgRecFldDat[ilTbl][1][ilFld].NewDat,  0, -1, ",");
        if (prgRecFldDat[ilTbl][1][ilAdid].NewDat[0] == 'D')
        {
          /*
             dbg(TRACE,"FOUND CHANGED CHECK FIELD <%s> FOR OUTBOUND", pclFldNam);
           */
          if (igCurrentAction != FOR_INSERT)
          {
            ilAutoSplit = TRUE;
          }                     /* end if */
          if (ilFld == ilRegn)
          {
            if (ilRegnFound == TRUE)
            {
              ilAutoJoin = TRUE;
            }                   /* end if */
          }                     /* end if */
        }                       /* end if */
        else
        {
          if (igCurrentAction != FOR_INSERT)
          {
            /*
               dbg(TRACE,"FOUND CHANGED CHECK FIELD <%s> FOR INBOUND", pclFldNam);
             */
            ilRkeyUpdate = TRUE;
          }                     /* end if */
        }                       /* end else */
        /*
           dbg(TRACE,"DATA: DBS <%s> NEW <%s>",
           prgRecFldDat[ilTbl][1][ilFld].DbsDat,
           prgRecFldDat[ilTbl][1][ilFld].NewDat);
         */
      }                         /* end if */
    }                           /* end if */
  }                             /* end for */

  TRACE_FIELD_OF_INTEREST(__LINE__,"CheckRkeyUpdates 4");

  if (ilDatPos > 0)
  {
    ilFldPos--;
    pclRkeyFields[ilFldPos] = 0x00;
    ilDatPos--;
    pclRkeyData[ilDatPos] = 0x00;
  }                             /* end if */

  if ((prgRecFldDat[ilTbl][1][ilFtyp].NewDat[0] == 'T') || (prgRecFldDat[ilTbl][1][ilFtyp].NewDat[0] == 'G'))
  {
    dbg (TRACE, "THAT IS NOT A FLIGHT RECORD: FTYP <%s>", prgRecFldDat[ilTbl][1][ilFtyp].NewDat);
    ilAutoJoin = FALSE;
    ilAutoSplit = FALSE;
    ilRkeyUpdate = FALSE;
    ilRegnFound = FALSE;
  }                             /* end if */

  if ((ilAutoJoin == TRUE) && (ilStateSplit == FALSE))
  {
    ilRegnFound = FALSE;
    dbg (TRACE, "AUTOJOIN: TRY TO JOIN FLIGHTS BY REGN");
    ilTifd = GetFieldIndex (ilTbl, "TIFD");
    if (lgJoinValidDays > 1)
    {                           /* +++ jim +++ 20001102: look for arrival within last 'lgJoinValidDays' days */
      dbg (TRACE, "CheckRkeyUpdates: calculate '%s' - %ld days... ", prgRecFldDat[ilTbl][1][ilTifd].NewDat,
           lgJoinValidDays);
      strcpy (pclTifaMin, prgRecFldDat[ilTbl][1][ilTifd].NewDat);
      AddSecondsToCEDATime (pclTifaMin, -lgJoinValidDays * SECONDS_PER_DAY, 1);
      sprintf (pclRkeyWhere, "WHERE REGN='%s' AND TIFA<='%s' AND TIFA>='%s' AND "
               /* "FTYP IN ('O','T','G','Z','B') AND STAT=' ' " */
               "FTYP IN ('O','T','G','Z','B','S') " "AND DES3='%s' " "ORDER BY TIFA DESC",
               prgRecFldDat[ilTbl][1][ilRegn].NewDat, prgRecFldDat[ilTbl][1][ilTifd].NewDat, pclTifaMin, pcgH3LC);
    }
    else
    {                           /* look for arrival before departure */
      sprintf (pclRkeyWhere, "WHERE REGN='%s' AND TIFA<='%s' AND "
               /* "FTYP IN ('O','T','G','Z','B') AND STAT=' ' " */
               "FTYP IN ('O','T','G','Z','B','S') " "AND DES3='%s' " "ORDER BY TIFA DESC",
               prgRecFldDat[ilTbl][1][ilRegn].NewDat, prgRecFldDat[ilTbl][1][ilTifd].NewDat, pcgH3LC);
    }
    dbg (TRACE, "SEARCH ARRIVAL FLIGHT RECORD");
    dbg (TRACE, "<%s>", pclRkeyWhere);

    strcpy (pclTable, pcgAftTab);
    pcgDataArea[0] = 0x00;
    ilRC = SearchFlight (pclTable, "URNO,RKEY", pclRkeyWhere, FOR_SEARCH, TRUE);
    strcpy (pclTmpDataArea, pcgDataArea);
    dbg (TRACE, "INBOUND TABLE <%s> URNO,RKEY <%s>", pclTable, pclTmpDataArea);

    if (ilRC == RC_SUCCESS)
    {
      (void) get_real_item (pclArrRkey, pclTmpDataArea, 2);
      strcpy (pclDepRkey, prgRecFldDat[ilTbl][1][ilRkey].NewDat);
      if (strcmp (pclArrRkey, pclDepRkey) != 0)
      {
        pclTable[0] = 0x00;
        sprintf (pclRkeyWhere, "%s,%s,%s", pclTmpDataArea, prgRecFldDat[ilTbl][1][ilUrno].NewDat, pclDepRkey);
        strcpy (pclRkeyFields, "RTYP");
        strcpy (pclRkeyData, "R");
        ilRC = ResetDataBuffer (__LINE__ ,TRUE, 0, 0, 0, 0, 0, 0);

  TRACE_FIELD_OF_INTEREST(__LINE__,"CheckRkeyUpdates 5");

        ilRC = JoinFlights (pclTable, pclRkeyFields, pclRkeyData, pclRkeyWhere, FALSE);
      }                         /* end if */
      else
      {
        dbg (TRACE, "BOTH FLIGHTS ALREADY IN SAME ROTATION");
      }                         /* end else */
      ilAutoSplit = FALSE;
      ilRkeyUpdate = FALSE;
    }                           /* end if */
  }                             /* end if */

  TRACE_FIELD_OF_INTEREST(__LINE__,"CheckRkeyUpdates 6");

  if (ilAutoSplit == TRUE)
  {
    if (ilRegnFound == TRUE)
    {
      dbg (TRACE, "AUTOSPLIT: DON'T SPLIT DUE TO VALID AC REGN");
      ilAutoSplit = FALSE;
    }                           /* end if */
    if (igReorgTable == TRUE)
    {
      dbg (TRACE, "AUTOSPLIT: DON'T SPLIT DUE TO REORG OF TABLE");
      ilAutoSplit = FALSE;
    }                           /* end if */
    ilRkeyUpdate = FALSE;
  }                             /* end if */

  if ((ilAutoSplit == TRUE) || (ilStateSplit == TRUE))
  {
    strcpy (pclTable, pcgAftTab);
    if (prgRecFldDat[ilTbl][1][ilAdid].NewDat[0] != 'A')
    {
      dbg (TRACE, "AUTOSPLIT: SPLIT CHAIN DUE TO AC CHANGE");
      if (igTWEUrnoIsNumber == TRUE)
      {
        sprintf (pclRkeyWhere, "WHERE RKEY=%s AND URNO<>%s " "AND DES3='%s' " "ORDER BY TIFA DESC",
                 prgRecFldDat[ilTbl][1][ilRkey].NewDat, prgRecFldDat[ilTbl][1][ilUrno].NewDat, pcgH3LC);
      }
      else
      {
        sprintf (pclRkeyWhere, "WHERE RKEY='%s' AND URNO<>'%s' " "AND DES3='%s' " "ORDER BY TIFA DESC",
                 prgRecFldDat[ilTbl][1][ilRkey].NewDat, prgRecFldDat[ilTbl][1][ilUrno].NewDat, pcgH3LC);
      }
      dbg (TRACE, "SEARCH LAST ARRIVAL FLIGHT RECORD");
      dbg (TRACE, "<%s>", pclRkeyWhere);

      pcgDataArea[0] = 0x00;
      ilRC = SearchFlight (pclTable, "URNO,RKEY", pclRkeyWhere, FOR_SEARCH, TRUE);
      strcpy (pclTmpDataArea, pcgDataArea);
      dbg (TRACE, "INBOUND TABLE <%s> URNO,RKEY <%s>", pclTable, pclTmpDataArea);

  TRACE_FIELD_OF_INTEREST(__LINE__,"CheckRkeyUpdates 7");

    }                           /* end if */
    else
    {
      sprintf (pclTmpDataArea, "%s,%s", prgRecFldDat[ilTbl][1][ilUrno].NewDat, prgRecFldDat[ilTbl][1][ilRkey].NewDat);
    }                           /* end else */

    if (ilRC == RC_SUCCESS)
    {
      (void) get_real_item (pclArrUrno, pclTmpDataArea, 1);
      (void) get_real_item (pclArrRkey, pclTmpDataArea, 2);
      ilRC = ResetDataBuffer (__LINE__ ,TRUE, 0, 0, 0, 0, 0, 0);
      ilRC = SplitChain (pclArrUrno, pclArrRkey, LAST_LEFT);

  TRACE_FIELD_OF_INTEREST(__LINE__,"CheckRkeyUpdates 8");

    }                           /* end if */
    else
    {
      dbg (TRACE, "THERE IS NOTHING TO SPLIT");
    }                           /* end else */

    ilRkeyUpdate = FALSE;
  }                             /* end if */

  TRACE_FIELD_OF_INTEREST(__LINE__,"CheckRkeyUpdates 9");

  if (ilRkeyUpdate == TRUE)
  {
    strcpy (pclTable, pcgAftTab);
    AddRegnToList (prgRecFldDat[ilTbl][1][ilRegn].DbsDat);
    AddRegnToList (prgRecFldDat[ilTbl][1][ilRegn].NewDat);

    strcpy (pclArrRkey, prgRecFldDat[ilTbl][1][ilRkey].NewDat);
    if (igTWEUrnoIsNumber == TRUE)
    {
      sprintf (pclRkeyWhere, "WHERE RKEY=%s ORDER BY ADID,TIFA", pclArrRkey);
    }
    else
    {
      sprintf (pclRkeyWhere, "WHERE RKEY='%s' ORDER BY ADID,TIFA", pclArrRkey);
    }
    ilRC = SearchFlight (pclTable, "RKEY", pclRkeyWhere, FOR_TABLES, TRUE);
    /* dbg(TRACE,"RKEY TABLES IDENTIFIED AS <%s> ",pclTable); */
    dbg (TRACE, "RKEY <%s> UPD FIELDS <%s>", pclArrRkey, pclRkeyFields);
    dbg (TRACE, "RKEY <%s> UPD DATA   <%s>", pclArrRkey, pclRkeyData);
    ilTblCnt = field_count (pclTable);

  TRACE_FIELD_OF_INTEREST(__LINE__,"CheckRkeyUpdates 10");

    for (ilTblItm = 1; ilTblItm <= ilTblCnt; ilTblItm++)
    {
      ilRC = ResetDataBuffer (__LINE__ ,TRUE, 0, 0, 0, 0, 0, 0);
      (void) get_real_item (pclActTbl, pclTable, ilTblItm);
      ilTbl = GetTableIndex (pclActTbl);
      if (ilTbl > 0)
      {
        ilRC = GetReceivedData (pclActTbl, pclRkeyFields, pclRkeyData);
        ilRC = ShowReceivedData ();

        ilRC = SearchFlight (pclActTbl, pclRkeyFields, pclRkeyWhere, FOR_UPDATE, TRUE);
        if (ilRC == RC_SUCCESS)
        {
          ilRC = ShowRecordData ();
          ilRC = PrepNewData (0);
          dbg (TRACE, "CheckRkeyUpdates: UPDATE AND BROADCAST FLIGHT WITH URNO <%s> ", pclRkeyWhere);
          ilRC = MakeUpdateAndBroadCast (__LINE__,ilTbl, 0, pclRkeyWhere, TRUE, "UFR", "URNO");
        }                       /* end if */
      }                         /* end if */
    }                           /* end for */

  }                             /* end else */


  TRACE_FIELD_OF_INTEREST(__LINE__,"CheckRkeyUpdates 11");

  /* Rest Of QuickHack */
  if (ilPsta > 0)
  {
    ilRC = ResetDataBuffer (__LINE__ ,TRUE, 0, 0, 0, 0, 0, 0);
    strcpy (pclActTbl, pcgAftTab);
    ilTbl = GetTableIndex (pclActTbl);
    /* and Jibbos New Quick hack: +++ jim +++ 20001224:
       In SHANGHAI PSTD should be set by FIPS AutoAllocation
       ilRC = GetReceivedData (pclActTbl, "PSTD", pclArrPsta);
    */
    ilRC = ShowReceivedData ();
    if (igTWEUrnoIsNumber == TRUE)
    {
      sprintf (pclRkeyWhere, "WHERE RKEY=%s AND ADID='D' AND PSTD=' '", pclArrRkey);
    }
    else
    {
      sprintf (pclRkeyWhere, "WHERE RKEY='%s' AND ADID='D' AND PSTD=' '", pclArrRkey);
    }

  TRACE_FIELD_OF_INTEREST(__LINE__,"CheckRkeyUpdates 12");

    /* dbg(TRACE,"SEARCH PSTD <%s>",pclRkeyWhere); */
    ilRC = SearchFlight (pclActTbl, "PSTD", pclRkeyWhere, FOR_UPDATE, TRUE);
    if (ilRC == RC_SUCCESS)
    {
      ilRC = ShowRecordData ();
      ilRC = PrepNewData (0);
      dbg (TRACE, "CheckRkeyUpdates: UPDATE AND BROADCAST FLIGHT WITH URNO <%s> ", pclRkeyWhere);
      ilRC = MakeUpdateAndBroadCast (__LINE__,ilTbl, 0, pclRkeyWhere, TRUE, "UFR", "URNO");
    }                           /* end if */
  }                             /* end if */

  return ilRC;
}                               /* end CheckRkeyUpdates */

/**************************************************************/
/**************************************************************/
static int MakeUpdateAndBroadCast (long lpLineNo, int ipTbl, int ipRecIdx, char *pcpSelKey, int ipSendBc, 
                                   char *pcpBcCmd,
                                   char *pcpBcSelList)
{
  int ilRC = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  int ilUrnoIdx = 0;
  int ilRec = -1;
  int ilFirstRec = -1;
  int ilRecCnt = -1;
  int ilFld = 0;
  int ilFldNbr = 0;
  int ilFldCnt = 0;
  int ilChrPos = 0;
  short slUpdCursor;
  char pclFldNam[8];
  char pclUrnoWhere[64];
  char pclActionCmd[8];

  dbg (DEBUG, "MakeUpdateAndBroadCast: called from line %ld, ipTbl %d, ipRecIdx %d selection field <%s>", 
             lpLineNo, ipTbl, ipRecIdx, pcpBcSelList);

  TRACE_FIELD_OF_INTEREST(__LINE__,"1");

  if (strcmp (pcpBcCmd, "DFR") != 0)
  {
    strcpy (pclActionCmd, "UFR");
  }                             /* end if */
  else
  {
    strcpy (pclActionCmd, "DFR");
  }                             /* end else */

/****   TWE 31012000  ****/
  if (igTWEImport == TRUE)
  {
    /* HandleFlightData is already done */
    igTWEUpdFlights++;
  }
  else
  {
    if ( prgPrjTblCfg[ipTbl].RecHdlCnt < igJIMRotMaxRec )
    {
       dbg (TRACE, "MakeUpdateAndBroadCast: Rec %d < Max %d !", prgPrjTblCfg[ipTbl].RecHdlCnt, igJIMRotMaxRec);
    }
    ilRC = HandleFlightData (ipTbl);
    igTWEUpdFlights++;
  }
/*************************/
  TRACE_FIELD_OF_INTEREST(__LINE__,"2a");

  if (ilRC == RC_SUCCESS)
  {
    if (ipRecIdx > 0)
    {
      ilFirstRec = ipRecIdx;
      ilRecCnt = ipRecIdx;
    }                           /* end if */
    else
    {
      ilFirstRec = 1;
      ilRecCnt = prgPrjTblCfg[ipTbl].RecHdlCnt;
    }                           /* end else */
    dbg (DEBUG, "MakeUpdateAndBroadCast: Checking from Rec %d to Rec %d", ilFirstRec,ilRecCnt);
    ilUrnoIdx = GetFieldIndex (ipTbl, "URNO");
    for (ilRec = ilFirstRec; ilRec <= ilRecCnt; ilRec++)
    {
      dbg (DEBUG, "MakeUpdateAndBroadCast: Checking now Rec %d ", ilRec);

  TRACE_FIELD_OF_INTEREST(__LINE__,"2");

      (void) TWEChangeDepTimeFlds (ipTbl, ilRec);

  TRACE_FIELD_OF_INTEREST(__LINE__,"3");

      ilFldCnt = CollectChanges (ipTbl, ilRec);

  TRACE_FIELD_OF_INTEREST(__LINE__,"4");

      if (ilFldCnt > 0)
      {
        ilRC = BuildSqlFldStrg (pcgTmpBuf1, prgPrjTblCfg[ipTbl].SqlFldLst, FOR_UPDATE);

  TRACE_FIELD_OF_INTEREST(__LINE__,"5");

/*
TWE 11012000
*/
        if ((igTWEImport == TRUE) && (igTWEUpdate == TRUE))
        {
          if ((pcgTWEUpdUrno[0] == 0) || (strcmp (pcgTWEUpdUrno, " ") == 0))
          {
            strcpy (pcgTWEUpdUrno, prgRecFldDat[ipTbl][ilRec][ilUrnoIdx].NewDat);
            dbg (DEBUG, "MakeUpdateAndBroadCast: Rec %d, pcgTWEUpdUrno set to <%s>", ilRec,pcgTWEUpdUrno);
          }
        }
        if (igTWEUrnoIsNumber == TRUE)
        {
          sprintf (pclUrnoWhere, "WHERE URNO=%s", prgRecFldDat[ipTbl][ilRec][ilUrnoIdx].NewDat);
        }
        else
        {
          sprintf (pclUrnoWhere, "WHERE URNO='%s'", prgRecFldDat[ipTbl][ilRec][ilUrnoIdx].NewDat);
        }
        dbg (DEBUG, "MakeUpdateAndBroadCast: Rec %d, Selection: <%s>", ilRec,pclUrnoWhere);
        sprintf (pcgSqlBuf, "UPDATE %s SET %s %s", prgPrjTblCfg[ipTbl].FullName, pcgTmpBuf1, pclUrnoWhere);
/*
        dbg(TRACE,"----------------------------------------------");
        dbg(TRACE,"SQL FLD:\n<%s>",prgPrjTblCfg[ipTbl].SqlFldLst);
        dbg(TRACE,"SQL DATA\n<%s>",prgPrjTblCfg[ipTbl].SqlDatBlk);
        dbg(TRACE,"----------------------------------------------");
*/
        if (igDebugDetails == TRUE)
        {
          dbg (TRACE, "SQL\n<%s>", pcgSqlBuf);
          dbg (TRACE, "DATA\n<%s>", prgPrjTblCfg[ipTbl].SqlDatBlk);
        }                       /* end if */

        strcpy (pcgDataArea, prgPrjTblCfg[ipTbl].SqlDatBlk);

        dbg (DEBUG, "JIM_UPDATE: <%s>", pcgSqlBuf);
        dbg (DEBUG, "JIM_DATA:   <%s>", prgPrjTblCfg[ipTbl].SqlDatBlk);

        delton (pcgDataArea);
        slUpdCursor = 0;
        
        
        ilGetRc = sql_if (IGNORE, &slUpdCursor, pcgSqlBuf, pcgDataArea);
        
        if (ilGetRc == DB_SUCCESS)
        {
          commit_work ();
          if ((igTWEImport == FALSE) || (igTWEForwardAll == TRUE))
          {
            AddRkeyToList (ipTbl, ilRec, "");
          }

          dbg (DEBUG, "MakeUpdateAndBroadCast: calling ReleaseActionInfo ....");
          (void) ReleaseActionInfo (pcgOutRoute, prgPrjTblCfg[ipTbl].FullName, pclActionCmd,
                                    prgRecFldDat[ipTbl][ilRec][ilUrnoIdx].NewDat, pclUrnoWhere,
                                    prgPrjTblCfg[ipTbl].OutFldLst, prgPrjTblCfg[ipTbl].OutDatBlk,
                                    prgPrjTblCfg[ipTbl].OldDatBlk);
/*
        dbg(TRACE,"----------------------------------------------");
        dbg(TRACE,"SQL FLD:\n<%s>",prgPrjTblCfg[ipTbl].SqlFldLst);
        dbg(TRACE,"SQL DATA\n<%s>",prgPrjTblCfg[ipTbl].SqlDatBlk);
        dbg(TRACE,"----------------------------------------------");
*/
          ilRC = RC_SUCCESS;
        }                       /* end if */
        else
        {
          dbg (TRACE, "ORA UPDATE FAILED");
          rollback ();
          ilRC = RC_FAIL;
        }                       /* end else */
        close_my_cursor (&slUpdCursor);
        dbg (DEBUG, "MakeUpdateAndBroadCast: Rec %d DB Update done", ilRec);
      }                         /* end if */
      else
      {
        dbg (TRACE, "RECORD %d: NOTHING CHANGED NOTHING ELSE TO DO", ilRec);
      }                         /* end else */

      if ((ilRC == RC_SUCCESS) && (ipSendBc == TRUE) && (ilFldCnt > 0))
      {
        dbg (DEBUG, "MakeUpdateAndBroadCast: Rec %d Checking BC", ilRec);
        ilFldCnt = field_count (pcpBcSelList);
        ilChrPos = 0;
        prgPrjTblCfg[ipTbl].OutSelKey[ilChrPos] = 0x00;
        for (ilFldNbr = 1; ilFldNbr <= ilFldCnt; ilFldNbr++)
        {
          (void) get_real_item (pclFldNam, pcpBcSelList, ilFldNbr);
          ilFld = GetFieldIndex (ipTbl, pclFldNam);

          StrgPutStrg (prgPrjTblCfg[ipTbl].OutSelKey, &ilChrPos, prgRecFldDat[ipTbl][ilRec][ilFld].NewDat, 0, -1, ",");
        }                       /* end for */
        if (ilChrPos > 0)
        {
          ilChrPos--;
          prgPrjTblCfg[ipTbl].OutSelKey[ilChrPos] = 0x00;
        }                       /* end if */

        dbg (TRACE, "MakeUpdateAndBroadCast: Cmd <%s>\nFields <%s> \nData <%s>", 
             pcpBcCmd, prgPrjTblCfg[ipTbl].OutFldLst, prgPrjTblCfg[ipTbl].OutDatBlk);
        if (strcmp (prgPrjTblCfg[ipTbl].OutSelKey, "0") != 0)
        {
          (void) tools_send_info_flag (igToBcHdl, 0, pcgDestName, "", pcgRecvName, "", "", pcgTwStart, pcgTwEnd,
                                       pcpBcCmd, prgPrjTblCfg[ipTbl].FullName, prgPrjTblCfg[ipTbl].OutSelKey,
                                       prgPrjTblCfg[ipTbl].OutFldLst, prgPrjTblCfg[ipTbl].OutDatBlk, 0);

          dbg (TRACE, "BROADCASTED '%s' SEL <%s>", pcpBcCmd, prgPrjTblCfg[ipTbl].OutSelKey);
        }
        else
        {
          dbg (TRACE, "BROADCAST '%s' skipped due to SEL <%s>!", pcpBcCmd, prgPrjTblCfg[ipTbl].OutSelKey);
        }
      }                         /* end if */
      dbg (DEBUG, "MakeUpdateAndBroadCast: Rec %d done", ilRec);
    }                           /* end for */
  }                             /* end if */
  return ilRC;
}                               /* end MakeUpdateAndBroadCast */

/**************************************************************/
/**************************************************************/
static int HandleFlightData (int ipTbl)
{
  int ilRC = RC_SUCCESS;

  if (igUtcDiff != 0)
  {
    (void) SetUtcDiffTime (0, igUtcDiff, TRUE);
    igUtcDiff = 0;
  }                             /* end if */

  TRACE_FIELD_OF_INTEREST(__LINE__,"10");

  ilRC = EvaluateFirstAction (ipTbl);

  TRACE_FIELD_OF_INTEREST(__LINE__,"11");

  ilRC = EvaluateFieldRules (ipTbl);

  TRACE_FIELD_OF_INTEREST(__LINE__,"12");

  ilRC = EvaluateLastAction (ipTbl);

  TRACE_FIELD_OF_INTEREST(__LINE__,"13");

  if ((strcmp (pcgRecvName, "SITA") == 0) || (strcmp (pcgDestName, "SITA") == 0))
  {
    ilRC = CheckNewSitaData (ipTbl);

  TRACE_FIELD_OF_INTEREST(__LINE__,"14");

  }
                               /* end if */
  if (prgPrjTblCfg[ipTbl].TypeOfTable == 1)
  {  /* +++ jim +++ 20010130: avoid message 'FIELD <CCATAB> <VIAL> not found'   */
     (void) ImplodeFieldData (ipTbl, 0, "VIAL");
  }

  TRACE_FIELD_OF_INTEREST(__LINE__,"15");

  (void)EvaluateConflicts(ipTbl);

  TRACE_FIELD_OF_INTEREST(__LINE__,"16");


  return ilRC;
}                               /* end HandleFlightData */

/* ***************************************************/
/* ***************************************************/
static int CheckNewSitaData (int ipTbl)
{
  int ilRC = RC_SUCCESS;
  int ilLoop = 0;
  int ilFld = 0;
  int ilFldCnt = 0;
  int ilSked = 0;
  int ilTime = 0;
  int ilTimeDiff = 0;
  int ilFlno = 0;
  int ilStoa = 0;
  int ilStod = 0;
  int ilAdid = 0;
  int ilFld1 = 0;
  int ilFld2 = 0;
  int ilFld3 = 0;
  int ilFld4 = 0;
  char pclArrFld[] = "STOA,ETAE";
  char pclDepFld[] = "STOD,OFBD,AIRD";
  char pclFldLst[128];
  char pclFldNam[8];

  if ((igSitaChecked != TRUE) && (prgPrjTblCfg[ipTbl].TypeOfTable == 1))
  {                             /* AFT */
    ilFlno = GetFieldIndex (ipTbl, "FLNO");
    ilAdid = GetFieldIndex (ipTbl, "ADID");
    ilStod = GetFieldIndex (ipTbl, "STOD");
    ilStoa = GetFieldIndex (ipTbl, "STOA");
    dbg (TRACE, "TELEX <%s> <%s> <%s> <%s>", prgRecFldDat[ipTbl][1][ilFlno].NewDat,
         prgRecFldDat[ipTbl][1][ilAdid].NewDat, prgRecFldDat[ipTbl][1][ilStod].NewDat,
         prgRecFldDat[ipTbl][1][ilStoa].NewDat);

    if (prgPrjTblCfg[ipTbl].RecHdlCnt > 1)
    {
      dbg (TRACE, "FOUND MORE THAN ONE FLIGHT (%d)", prgPrjTblCfg[ipTbl].RecHdlCnt);
      ilRC = RC_FAIL;
    }                           /* end if */

    if (prgRecFldDat[ipTbl][1][ilAdid].NewDat[0] == 'A')
    {
      ilFld1 = GetFieldIndex (ipTbl, "LAND");
      ilFld2 = GetFieldIndex (ipTbl, "ONBL");
      if ((ilRC == RC_SUCCESS) &&
          ((prgRecFldDat[ipTbl][1][ilFld1].NewVal > 0) || (prgRecFldDat[ipTbl][1][ilFld2].NewVal > 0)))
      {
        ilFld3 = GetFieldIndex (ipTbl, "STLD");
        ilFld4 = GetFieldIndex (ipTbl, "STON");
        if (((prgRecFldDat[ipTbl][1][ilFld3].NewDat[0] != ' ') && (prgRecFldDat[ipTbl][1][ilFld3].NewDat[0] != 'D')) ||
            ((prgRecFldDat[ipTbl][1][ilFld4].NewDat[0] != ' ') && (prgRecFldDat[ipTbl][1][ilFld4].NewDat[0] != 'D')))
        {
          dbg (TRACE, "ALREADY LAND/ONBL <%s> <%s>", prgRecFldDat[ipTbl][1][ilFld1].NewDat,
               prgRecFldDat[ipTbl][1][ilFld2].NewDat);
          ilRC = RC_FAIL;
        }                       /* end if */
      }                         /* end if */
    }                           /* end if */

    if (prgRecFldDat[ipTbl][1][ilAdid].NewDat[0] == 'D')
    {
      ilFld1 = GetFieldIndex (ipTbl, "OFBL");
      ilFld2 = GetFieldIndex (ipTbl, "AIRB");
      if ((ilRC == RC_SUCCESS) &&
          ((prgRecFldDat[ipTbl][1][ilFld1].NewVal > 0) || (prgRecFldDat[ipTbl][1][ilFld2].NewVal > 0)))
      {
        ilFld3 = GetFieldIndex (ipTbl, "STOF");
        ilFld4 = GetFieldIndex (ipTbl, "STAB");
        if (((prgRecFldDat[ipTbl][1][ilFld3].NewDat[0] != ' ') && (prgRecFldDat[ipTbl][1][ilFld3].NewDat[0] != 'D')) ||
            ((prgRecFldDat[ipTbl][1][ilFld4].NewDat[0] != ' ') && (prgRecFldDat[ipTbl][1][ilFld4].NewDat[0] != 'D')))
        {
          dbg (TRACE, "ALREADY OFBL/AIRB <%s> <%s>", prgRecFldDat[ipTbl][1][ilFld1].NewDat,
               prgRecFldDat[ipTbl][1][ilFld2].NewDat);
          ilRC = RC_FAIL;
        }                       /* end if */
      }                         /* end if */
    }                           /* end if */

    if (ilRC == RC_SUCCESS)
    {
      strcpy (pclFldLst, pclArrFld);
      for (ilLoop = 1; ((ilLoop <= 2) && (ilRC == RC_SUCCESS)); ilLoop++)
      {
        ilFldCnt = field_count (pclFldLst);
        (void) get_real_item (pclFldNam, pclFldLst, 1);
        ilSked = GetFieldIndex (ipTbl, pclFldNam);
        if (prgRecFldDat[ipTbl][1][ilSked].NewVal > 0)
        {
          for (ilFld = 2; ((ilFld <= ilFldCnt) && (ilRC == RC_SUCCESS)); ilFld++)
          {
            (void) get_real_item (pclFldNam, pclFldLst, ilFld);
            ilTime = GetFieldIndex (ipTbl, pclFldNam);
            if (prgRecFldDat[ipTbl][1][ilTime].NewVal > 0)
            {
              ilTimeDiff = prgRecFldDat[ipTbl][1][ilTime].NewVal - prgRecFldDat[ipTbl][1][ilSked].NewVal;
              if (abs (ilTimeDiff) > igTifaMinDiff)
              {
                dbg (TRACE, "TIME DIFF <%s> <%s> <%s> = %d MIN", prgTblFldCfg[ipTbl][ilSked].SysTabFina,
                     prgTblFldCfg[ipTbl][ilTime].SysTabFina, prgRecFldDat[ipTbl][1][ilTime].NewDat, ilTimeDiff);
                ilRC = RC_FAIL;
              }                 /* end if */
            }                   /* end if */
          }                     /* end for */
        }                       /* end if */
        strcpy (pclFldLst, pclDepFld);
      }                         /* end for */
    }                           /* end if */

    if (ilRC != RC_SUCCESS)
    {
      dbg (TRACE, "DATA FROM (SITA) INTERFACE IGNORED");
    }                           /* end if */
    igSitaChecked = TRUE;
    dbg (TRACE, "-----------------------------------------");
    if (igDebugDetails != TRUE)
    {
      debug_level = igRuntimeMode;
    }                           /* end if */
  }                             /* end if */
  return ilRC;
}                               /* end CheckNewSitaData */

/* ***************************************************/
/* ***************************************************/
static int EvaluateFirstAction (int ipTbl)
{
  int ilRC = RC_SUCCESS;

  CheckRcvdDssfFields (ipTbl);

  return ilRC;
}                               /* end EvaluateFirstAction */

/* ***************************************************/
/* ***************************************************/
static void BuildProperFlno (int ipTbl)
{
	 int ilFlno = 0;
	 int ilRecCnt = 0;
	 int ilRec = 0;
	 int ilFound = FALSE;
	 char pclFlno[16];

	 if (prgPrjTblCfg[ipTbl].TypeOfTable == 1)
	 {                             /* AFT */
			ilFlno = GetFieldIndex (ipTbl, "FLNO");
			ilRecCnt = prgPrjTblCfg[ipTbl].RecHdlCnt;
			for (ilRec = 1; ilRec <= ilRecCnt; ilRec++)
			{
				 if (prgRecFldDat[ipTbl][ilRec][ilFlno].NewLen > 2)
				 {
						strcpy (pclFlno, prgRecFldDat[ipTbl][ilRec][ilFlno].NewDat);
						(void) FormatCedaFlno (pclFlno);
						ilFound = TRUE;
						dbg (TRACE, "BuildProperFlno: NewDat <%s> FormatCedaFlno <%s>", prgRecFldDat[ipTbl][ilRec][ilFlno].NewDat,
							pclFlno);

						if (strcmp (pclFlno, prgRecFldDat[ipTbl][ilRec][ilFlno].NewDat) != 0)
						{
							 dbg (TRACE, "FLNO FORMATTED FROM <%s> TO <%s>", prgRecFldDat[ipTbl][ilRec][ilFlno].NewDat, pclFlno);
						}                       /* end if */
						(void) SetNewValues (ipTbl, ilRec, ilFlno, "FLNO", pclFlno);
						if (ilFound != TRUE)
						{
							 dbg (TRACE, "COULDN'T IDENTIFY FLNO!");
						}                       /* end if */
				 }                         /* end if */
			}                           /* end for */
	 }                             /* end if */
	 return;
}                               /* BuildProperFlno */

/* ***************************************************/
/* ***************************************************/
static void CheckRcvdDssfFields (int ipTbl)
{
  int ilDssf = 0;
  int ilDssfLen = 0;
  int ilDssfChr = 0;
  int ilRecCnt = 0;
  int ilFldCnt = 0;
  int ilRec = 0;
  int ilFld = 0;
  if (prgPrjTblCfg[ipTbl].TypeOfTable == 1)
  {                             /* AFT */
    /* dbg(TRACE,"BEGIN CHECK RECEIVED DSSF FIELDS"); */
    ilDssf = GetFieldIndex (ipTbl, "DSSF");
    ilRecCnt = prgPrjTblCfg[ipTbl].RecHdlCnt;
    ilFldCnt = prgPrjTblCfg[ipTbl].AllFldCnt;
    for (ilFld = 1; ilFld <= ilFldCnt; ilFld++)
    {
      if ((prgTblFldCfg[ipTbl][ilFld].CtrlFlagNbr >= 0) && (prgTblFldCfg[ipTbl][ilFld].RcvFlg == TRUE))
      {
        ilDssfChr = prgTblFldCfg[ipTbl][ilFld].CtrlFlagNbr;
        for (ilRec = 1; ilRec <= ilRecCnt; ilRec++)
        {
          ilDssfLen = prgRecFldDat[ipTbl][ilRec][ilDssf].NewLen;
          if (ilDssfLen < 1)
          {
            ilDssfLen = 1;
          }                     /* end if */
          while (ilDssfChr >= ilDssfLen)
          {
            prgRecFldDat[ipTbl][ilRec][ilDssf].NewDat[ilDssfLen] = ' ';
            ilDssfLen++;
          }                     /* end while */
          prgRecFldDat[ipTbl][ilRec][ilDssf].NewDat[ilDssfLen] = '\0';
          prgRecFldDat[ipTbl][ilRec][ilDssf].NewLen = ilDssfLen;

          /*
             dbg(TRACE,"DSSF CTRL FIELD DETECTED <%s> POS %d IN <%s>",
             prgTblFldCfg[ipTbl][ilFld].SysTabFina, ilDssfChr,
             prgRecFldDat[ipTbl][ilRec][ilDssf].NewDat);
           */

          if (igClientAction == TRUE)
          {
            /*
               dbg(TRACE,"VALUE RECEIVED FROM CLIENT <%s>",
               prgRecFldDat[ipTbl][ilRec][ilFld].RcvDat);
             */
            if (prgRecFldDat[ipTbl][ilRec][ilFld].RcvLen > 0)
            {
              prgRecFldDat[ipTbl][ilRec][ilDssf].NewDat[ilDssfChr] = 'U';
            }                   /* end if */
            else
            {
              prgRecFldDat[ipTbl][ilRec][ilDssf].NewDat[ilDssfChr] = ' ';
            }                   /* end else */
          }                     /* end if */
          else
          {
            if (prgRecFldDat[ipTbl][ilRec][ilDssf].NewDat[ilDssfChr] == 'U')
            {
              dbg (TRACE, "FIELD <%s> IS PROTECTED", prgTblFldCfg[ipTbl][ilFld].SysTabFina);
              strcpy (prgRecFldDat[ipTbl][ilRec][ilFld].NewDat, prgRecFldDat[ipTbl][ilRec][ilFld].DbsDat);
              prgRecFldDat[ipTbl][ilRec][ilFld].NewLen = prgRecFldDat[ipTbl][ilRec][ilFld].DbsLen;
              prgRecFldDat[ipTbl][ilRec][ilFld].NewVal = prgRecFldDat[ipTbl][ilRec][ilFld].DbsVal;
              prgRecFldDat[ipTbl][ilRec][ilFld].NewFlg = FALSE;
              prgRecFldDat[ipTbl][ilRec][ilFld].RcvFlg = FALSE;
              prgRecFldDat[ipTbl][ilRec][ilFld].RcvLen = 0;
              prgTblFldCfg[ipTbl][ilFld].RcvFlg = FALSE;
              prgTblFldCfg[ipTbl][ilFld].RcvLen = 0;
            }                   /* end if */
          }                     /* end else */
          str_trm_rgt (prgRecFldDat[ipTbl][ilRec][ilDssf].NewDat, " ", TRUE);
          prgRecFldDat[ipTbl][ilRec][ilDssf].NewLen = strlen (prgRecFldDat[ipTbl][ilRec][ilDssf].NewDat);
          if (prgRecFldDat[ipTbl][ilRec][ilDssf].NewLen < 1)
          {
            strcpy (prgRecFldDat[ipTbl][ilRec][ilDssf].NewDat, " ");
          }                     /* end if */
          if (strcmp (prgRecFldDat[ipTbl][ilRec][ilDssf].NewDat, prgRecFldDat[ipTbl][ilRec][ilDssf].DbsDat) != 0)
          {
            prgRecFldDat[ipTbl][ilRec][ilDssf].NewFlg = TRUE;
          }                     /* end if */
        }                       /* end for */
      }                         /* end if */
    }                           /* end for */
    /* dbg(TRACE,"END CHECK RECEIVED DSSF FIELDS"); */
  }                             /* end if */
  return;
}                               /* end CheckRcvdDssfFields */

/* ***************************************************/
/* ***************************************************/
static int EvaluateLastAction (int ipTbl)
{
  int ilRC = RC_SUCCESS;
  int ilRec = 0;
  int ilRecCnt = 0;
  int ilFld = 0;
  int ilFldCnt = 0;

  BuildProperFlno (ipTbl);

  ilRecCnt = prgPrjTblCfg[ipTbl].RecHdlCnt;
  for (ilRec = 1; ilRec <= ilRecCnt; ilRec++)
  {
    ilFldCnt = prgPrjTblCfg[ipTbl].AllFldCnt;
    for (ilFld = 1; ilFld <= ilFldCnt; ilFld++)
    {
      switch (prgTblFldCfg[ipTbl][ilFld].FieldType)
      {
        case 14:               /* LSTU */
          if (igCurrentAction != FOR_INSERT)
          {
            (void) SetNewValues (ipTbl, ilRec, ilFld, "", pcgCurTime);
          }                     /* end if */
          break;
        case 15:               /* USER */
          if (igCurrentAction != FOR_INSERT)
          {
            (void) SetNewValues (ipTbl, ilRec, ilFld, "", pcgDestName);
          }                     /* end if */
          break;
        case 16:               /* WORK */
            (void) SetNewValues (ipTbl, ilRec, ilFld, "", pcgRecvName);
          break;
        case 17:               /* CDAT */
          if (igCurrentAction == FOR_INSERT)
          {
            (void) SetNewValues (ipTbl, ilRec, ilFld, "", pcgCurTime);
          }                     /* end if */
          break;
        case 18:               /* CUSR */
          if (igCurrentAction == FOR_INSERT)
          {
            (void) SetNewValues (ipTbl, ilRec, ilFld, "", pcgDestName);
          }                     /* end if */
          break;
        case 19:               /* IMPO */
          if (igImportAction == TRUE)
          {
            (void) SetNewValues (ipTbl, ilRec, ilFld, "", pcgCurTime);
          }                     /* end if */
          break;
        default:
          break;
      }                         /* end switch */
    }                           /* end for */
  }                             /* end for */
  return ilRC;
}                               /* end EvaluateLastAction */

/* ***************************************************
* Function: 		HandleDeleteFlightRecord
* Parameter: 		void
* Return:				RC_SUCCESS, RC_FAIL
* Description:	Deletes a flight record (Update STAT).
*******************************************************/
static int HandleDeleteFlightRecord (void)
{
  int ilRC = RC_SUCCESS;
  int ilFldCnt = 0;
  int ilUrnoIdx = 0;
  int ilOutDatPos = 0;
  char *pclSel = NULL, *pclFields = NULL, *pclData = NULL;
  BC_HEAD *prlBchd;
  CMDBLK *prlCmdblk;
  short ilCursor, ilFkt;

  int ilGetRc = DB_SUCCESS;
  int ilQuickHack = FALSE;
  char pclDelSqlBuf[4098];
  char pclDelSqlDat[128];
  char pclAftSelKey[128];
  char pclSelFldLst[128];
  char pclAftUrno[16];
  char pclAftFields[128];
  /* char pclOutDatLst[130000]; */
  char *pclWhereBegin;

  strcpy (pclAftFields, "STAT");
  strcpy (pclSelFldLst, "URNO,TIFD,TIFA");
  ilUrnoIdx = 1;
  ilFldCnt = field_count (pclSelFldLst);

  prlBchd = (BC_HEAD *) ((char *) prgEvent + sizeof (EVENT));
  prlCmdblk = (CMDBLK *) (char *) prlBchd->data;
  pclSel = (char *) prlCmdblk->data;
  pclFields = (char *) pclSel + strlen (pclSel) + 1;
  pclData = (char *) pclFields + strlen (pclFields) + 1;

  dbg (TRACE, "IN DFR: DELETE FLIGHT RECORDS");
  dbg (TRACE, "--------- FIELDS ----------\n<%s>", pclFields);
  dbg (TRACE, "---------- DATA -----------\n<%s>", pclData);
  dbg (TRACE, "------- SELECTION ---------\n<%s>", pclSel);
  dbg (TRACE, "---------------------------");

  if ((igGlNoAck == FALSE) && (igTWEFromAZFlight == FALSE))
  {
    (void) tools_send_info_flag (que_out, 0, pcgDestName, "", pcgRecvName, "", "", pcgTwStart, pcgTwEnd, "DFR",
                                 pcgAftTab, pclSel, pclFields, pclData, 0);
  }                             /* end if */

  pclWhereBegin = strstr (pclSel, "WHERE ");
  if (pclWhereBegin == NULL)
  {
    pclWhereBegin = pclSel;
  }                             /* end if */
  else
  {
    pclWhereBegin += 6;
  }                             /* end else */
  sprintf (pclDelSqlBuf, "SELECT %s FROM %s WHERE (%s) AND STAT<>'DEL'", pclSelFldLst, pcgAftTab, pclWhereBegin);
  dbg (TRACE, "<%s>", pclDelSqlBuf);

  ilQuickHack = TRUE;
  ilOutDatPos = 0;
  ilCursor = 0;
  ilFkt = START;
  
  do
  {
    ilRC = sql_if (ilFkt, &ilCursor, pclDelSqlBuf, pclDelSqlDat);
    if (ilRC == DB_SUCCESS)
    {
      BuildItemBuffer (pclDelSqlDat, pclSelFldLst, ilFldCnt, ",");
      dbg (TRACE, "DEL FOUND RECORD <%s>", pclDelSqlDat);
      (void) get_real_item (pclAftUrno, pclDelSqlDat, ilUrnoIdx);

      if (igTWEUrnoIsNumber == TRUE)
      {
        sprintf (pclAftSelKey, "WHERE URNO=%s", pclAftUrno);
      }
      else
      {
        sprintf (pclAftSelKey, "WHERE URNO='%s'", pclAftUrno);
      }
      if (ilQuickHack == TRUE)
      {
        ilGetRc = DeleteBySelection (pcgAftTab, pclAftSelKey);
        (void) ReleaseActionInfo (pcgOutRoute, pcgAftTab, "DFR", pclAftUrno, pclAftUrno, pclSelFldLst, pclDelSqlDat,
                                  "\0");
        if (ilOutDatPos < 128000)
        {
          /*
             StrgPutStrg(pclOutDatLst, &ilOutDatPos, pclDelSqlDat, 0, -1, "\n");
           */
        }                       /* end if */
        else
        {
          /*
             ilOutDatPos--;
             pclOutDatLst[ilOutDatPos] = 0x00;
             (void) ReleaseActionInfo("7400",
             pcgAftTab,
             "DFR",
             pclAftUrno,
             pclAftUrno,
             pclSelFldLst,
             pclOutDatLst,"\0");
           */
          ilOutDatPos = 0;
        }                       /* end else */
        ilGetRc =
          tools_send_info_flag (igToBcHdl, 0, pcgDestName, "", pcgRecvName, "", "", pcgTwStart, pcgTwEnd, "DFR",
                                pcgAftTab, pclDelSqlDat, "", pclDelSqlDat, 0);
      }                         /* end if */
      else
      {
        ilGetRc = SearchFlight (pcgAftTab, pclAftFields, pclAftSelKey, FOR_UPDATE, TRUE);
        if (ilGetRc == RC_SUCCESS)
        {
          ilGetRc = ShowRecordData ();
          ilGetRc = PrepNewData (0);
          (void) SetNewValues (1, 1, 0, "STAT", "DEL");
          dbg (TRACE, "HandleDeleteFlightRecord: UPDATE AND BROADCAST FLIGHT WITH URNO <%s> ", pclAftSelKey);
          ilGetRc = MakeUpdateAndBroadCast (__LINE__,1, 1, pclAftSelKey, TRUE, "DFR", "URNO");
        }                       /* end if */
      }                         /* end else */
    }                           /* end if */
    ilFkt = NEXT;
  }
  while (ilRC == DB_SUCCESS);
  close_my_cursor (&ilCursor);
  

  if (ilOutDatPos > 0)
  {
    /*
       ilOutDatPos--;
       pclOutDatLst[ilOutDatPos] = 0x00;
       (void) ReleaseActionInfo("7400",
       pcgAftTab,
       "DFR",
       pclAftUrno,
       pclAftUrno,
       pclSelFldLst,
       pclOutDatLst,"\0");
     */
  }                             /* end if */

/*
  (void) tools_send_info_flag(que_out,0,
              pcgDestName, "", pcgRecvName, "", "", pcgTwStart, pcgTwEnd,
              "DFR", pcgAftTab, pclSel, pclFields, pclData, 0);
*/

  if ((igGlNoAck == FALSE) && (igTWEFromAZFlight == TRUE))
  {
    (void) tools_send_info_flag (que_out, 0, pcgDestName, "", pcgRecvName, "", "", pcgTwStart, pcgTwEnd, "DFRAZ",
                                 pcgAftTab, pclSel, pclFields, pclData, 0);
  }                             /* end if */

  return RC_SUCCESS;
}                               /* end HandleDeleteFlightRecord */

/* ***************************************************
* Function: 		HandleDeleteGroundMovement
* Parameter: 		void
* Return:				RC_SUCCESS, RC_FAIL
* Description:	Deletes tows/ground movements.
*******************************************************/
static int HandleDeleteGroundMovement (void)
{
  int ilRC = RC_SUCCESS;
  dbg (TRACE, "COMMAND 'DGM' NOT YET SUPPORTED");
  return ilRC;
}                               /* HandleDeleteGroundMovement */

/* ***************************************************
* Function: 		HandleCreateGroundMovement
* Parameter: 		void
* Return:				RC_SUCCESS, RC_FAIL
* Description:	Create and inserts tows.
*******************************************************/
static int HandleCreateGroundMovement (void)
{
  int ilRC = RC_SUCCESS;
  dbg (TRACE, "COMMAND 'CGM' NOT YET SUPPORTED");
  return ilRC;
}                               /* end HandleCreateGroundMovement */

/* ***************************************************
* Function: 	SplitFlights
* Parameter: 	
* Return:	RC_SUCCESS, RC_FAIL
* Description:	Splits rotations.
*******************************************************/
static int SplitFlights (char *pcpRcvTable, char *pcpFields, char *pcpData, char *pcpSelKey)
{
  int ilRC = RC_SUCCESS;
  int ilUrnoTbl;
  int ilUrnoLen;
  char pclUrnoTable[32];
  char pclChainTables[32];
  char pclFromUrno[16];
  char pclOldRkey[16];
  char pclSqlWhere[64];
  char pclSplitReadFields[] = "RKEY,RTYP";

  dbg (TRACE, "===== DATA ELEMENTS FROM CLIENT =========");
  dbg (TRACE, "SPR TABLE  <%s>", pcpRcvTable);
  dbg (TRACE, "SPR FIELDS <%s>", pcpFields);
  dbg (TRACE, "SPR DATA   <%s>", pcpData);
  dbg (TRACE, "SPR SELECT <%s>", pcpSelKey);
  dbg (TRACE, "=========================================");

  ilRC = RC_SUCCESS;

  /* Get Special Informations */
  ilUrnoLen = get_real_item (pclFromUrno, pcpSelKey, 1);
  if (ilUrnoLen < 1)
  {
    /* Missing Rkey from Client */
    ilRC = RC_FAIL;
  }                             /* end if */

  if (ilRC == RC_SUCCESS)
  {
    /* DETERMINE LOCATION OF THAT URNO: CAN BE ONLY ONE */
    strcpy (pclUrnoTable, pcgAftTab);
    if (igTWEUrnoIsNumber == TRUE)
    {
      sprintf (pclSqlWhere, "WHERE URNO=%s", pclFromUrno);
    }
    else
    {
      sprintf (pclSqlWhere, "WHERE URNO='%s'", pclFromUrno);
    }
    ilRC = SearchFlight (pclUrnoTable, pclSplitReadFields, pclSqlWhere, FOR_SEARCH, TRUE);
  }                             /* end if */

  if (ilRC == RC_SUCCESS)
  {
    ilUrnoTbl = GetTableIndex (pclUrnoTable);
    dbg (TRACE, "TABLE (OF URNO) IDENTIFIED AS <%s> (%d)", pclUrnoTable, ilUrnoTbl);
    dbg (TRACE, "DATA: <%s>", pcgDataArea);
    dbg (TRACE, "-----------------------------------------");

    if (ilUrnoTbl > 0)
    {
      (void) get_real_item (pclOldRkey, pcgDataArea, 1);
      strcpy (pclChainTables, pclUrnoTable);
    }                           /* end if */
    else
    {
      /* Table unknown or not found */
      ilRC = RC_FAIL;
    }                           /* end else */

  }                             /* end if */

  if (ilRC == RC_SUCCESS)
  {
    ilRC = SplitChain (pclFromUrno, pclOldRkey, LAST_LEFT);
  }                             /* end if */
  else
  {
    /* Collect Error Messages */
  }                             /* end else */

  if (igGlNoAck == FALSE)
  {
    /* wg. PROBLEME IM CLIENT */
    /*
       (void) tools_send_info_flag(que_out,0, pcgDestName, "", pcgRecvName,
       "", "", pcgTwStart, pcgTwEnd,
       "SPR", pclChainTables, "OK", "SPR", "EOT", 0);
     */
    (void) tools_send_info_flag (que_out, 0, pcgDestName, "", pcgRecvName, "", "", pcgTwStart, pcgTwEnd, "SPR",
                                 pcpRcvTable, pcpSelKey, pcpFields, pcpData, 0);

  }                             /* end if */
  dbg (TRACE, "=========================================");
  return ilRC;
}                               /* end SplitFlights */

/* *************************************************** */
/* *************************************************** */
static int SplitChain (char *pcpUrno, char *pcpOldRkey, int ipUrnoType)
{
  int ilRC = RC_SUCCESS;
  int ilTbl = 0;
  int ilTblCnt = 0;
  int ilTblNbr = 0;
  int ilRec = 0;
  int ilFldCnt = 0;
  int ilUrnoIdx = 0;
  int ilTifaIdx = 0;
  int ilUrnoTifaVal = 0;
  int ilRecTifaVal = 0;
  int ilGetRc = 0;
  int ilGetSelRc = 0;
  int ilUrnoFound = FALSE;
  int ilGotRkey = FALSE;
  int ilDoSplit = FALSE;
  int ilNewChain = FALSE;
  int ilChgCnt = 0;
  short slFkt = 0;
  short slSelCursor = 0;
  char pclNewRkey[16];
  char pclTables[16];
  char pclActTable[16];
  char pclSelSqlBuf[2048];
  char pclSelSqlDat[4096];
  char pclUpdSqlWhere[128];
  char pclSelFields[] = "URNO,ADID,FTYP,RTYP,TIFA,TIFD,REGN,AURN";
  strcpy (pclTables, pcgAftTab);

  pclNewRkey[0] = 0x00;
  dbg (TRACE, "SPLIT CHAIN ENTERED");
  ilChgCnt = 0;
  ilTblCnt = field_count (pclTables);
  for (ilTblNbr = 1; ilTblNbr <= ilTblCnt; ilTblNbr++)
  {
    (void) get_real_item (pclActTable, pclTables, ilTblNbr);
    ilTbl = GetTableIndex (pclActTable);
    (void) BuildSqlReadFields (ilTbl, pclSelFields, FOR_NORMAL);
    if (igTWEUrnoIsNumber == TRUE)
    {
      sprintf (pclSelSqlBuf, "SELECT %s FROM %s WHERE RKEY=%s ORDER BY ADID,TIFA", prgPrjTblCfg[ilTbl].ActFldLst,
               pclActTable, pcpOldRkey);
    }
    else
    {
      sprintf (pclSelSqlBuf, "SELECT %s FROM %s WHERE RKEY='%s' ORDER BY ADID,TIFA", prgPrjTblCfg[ilTbl].ActFldLst,
               pclActTable, pcpOldRkey);
    }
    dbg (TRACE, "<%s>", pclSelSqlBuf);

    ilFldCnt = field_count (prgPrjTblCfg[ilTbl].ActFldLst);
    ilUrnoIdx = GetFieldIndex (ilTbl, "URNO");
    ilTifaIdx = GetFieldIndex (ilTbl, "TIFA");
    ilRec = 1;
    slFkt = START;
    slSelCursor = 0;
    
    do
    {
      ilGetSelRc = sql_if (slFkt, &slSelCursor, pclSelSqlBuf, pclSelSqlDat);
      if (ilGetSelRc == DB_SUCCESS)
      {
        BuildItemBuffer (pclSelSqlDat, pclSelFields, ilFldCnt, ",");
        /* dbg(DEBUG,"DATA: <%s>",pclSelSqlDat); */
        (void) GetRecordData (ilTbl, ilRec, ilFldCnt, pclActTable, prgPrjTblCfg[ilTbl].ActFldLst, pclSelSqlDat);
        (void) ShowRecordData ();
        ilRC = PrepNewData (0);
        if (ilUrnoFound == TRUE)
        {
          ilRecTifaVal = prgRecFldDat[ilTbl][ilRec][ilTifaIdx].DbsVal;
          if ((ilRecTifaVal >= igSysMinStamp) || (igSplitAlways == TRUE))
          {
            dbg (TRACE, "SPLITTING THAT RECORD FROM CHAIN");
            if (ilGotRkey == FALSE)
            {
              ilGetRc = GetNextValues (pclNewRkey, 1);
              if (ilGetRc != DB_SUCCESS)
              {
                dbg (TRACE, "GET NEXT URNO FAILED (NEW RKEY)");
                ilRC = RC_FAIL;
              }                 /* end if */
              ilGotRkey = TRUE;
            }                   /* end if */
            if (ilRC == RC_SUCCESS)
            {
              (void) SetNewValues (ilTbl, ilRec, 0, "RKEY", pclNewRkey);
              if (ilNewChain == TRUE)
              {
                (void) SetNewValues (ilTbl, ilRec, 0, "RTYP", "S");
                (void) SetNewValues (ilTbl, ilRec, 0, "AURN", " ");
                ilNewChain = FALSE;
                if ((igTWEImport == FALSE) || (igTWEForwardAll == TRUE))
                {
                  AddRkeyToList (0, 0, pcpOldRkey);
                }
              }                 /* end if */
              ilDoSplit = TRUE;
            }                   /* end if */
          }                     /* end if */
          else
          {
            dbg (TRACE, "SPLIT RECORD IGNORED BY TIMESTAMP");
            ilDoSplit = FALSE;
          }                     /* end else */
        }                       /* end if */
        else
        {
          if (strcmp (prgRecFldDat[ilTbl][ilRec][ilUrnoIdx].DbsDat, pcpUrno) == 0)
          {
            ilUrnoTifaVal = prgRecFldDat[ilTbl][ilRec][ilTifaIdx].DbsVal;
            dbg (TRACE, "URNO FOUND: TIFA <%s> (%d)", prgRecFldDat[ilTbl][ilRec][ilTifaIdx].DbsDat, ilUrnoTifaVal);
            ilUrnoFound = TRUE;
            (void) SetNewValues (ilTbl, ilRec, 0, "RTYP", "S");
            ilDoSplit = TRUE;
            ilNewChain = TRUE;
          }                     /* end if */
          else
          {
            /* dbg(TRACE,"URNO NOT YET FOUND") */
            ilDoSplit = FALSE;
          }                     /* end else */
        }                       /* end else */
        if ((ilRC == RC_SUCCESS) && (ilDoSplit == TRUE))
        {
          if (igTWEUrnoIsNumber == TRUE)
          {
            sprintf (pclUpdSqlWhere, "WHERE URNO=%s", prgRecFldDat[ilTbl][ilRec][ilUrnoIdx].NewDat);
          }
          else
          {
            sprintf (pclUpdSqlWhere, "WHERE URNO='%s'", prgRecFldDat[ilTbl][ilRec][ilUrnoIdx].NewDat);
          }
          dbg (TRACE, "SplitChain: UPDATE AND BROADCAST FLIGHT WITH URNO <%s> ", pclUpdSqlWhere);
          ilRC = MakeUpdateAndBroadCast (__LINE__,ilTbl, ilRec, pclUpdSqlWhere, TRUE, "UPS", "URNO");
          ilChgCnt++;
        }                       /* end if */
      }                         /* end if */
      slFkt = NEXT;
    }
    while (ilGetSelRc == DB_SUCCESS);
    close_my_cursor (&slSelCursor);

    
  }                             /* end for */
  if ((ilRC == RC_SUCCESS) && (ilGotRkey == TRUE) && (ilChgCnt > 0))
  {
    sprintf (pclSelSqlBuf, "%s,%s", pcpUrno, pcpOldRkey);
    sprintf (pclSelSqlDat, "%s,%s", pcpOldRkey, pclNewRkey);
    ilGetRc =
      tools_send_info_flag (igToBcHdl, 0, pcgDestName, "", pcgRecvName, "", "", pcgTwStart, pcgTwEnd, "SPR", "",
                            pclSelSqlBuf, "", pclSelSqlDat, 0);
    dbg (TRACE, "BROADCASTED 'SPR' DAT <%s>", pclSelSqlDat);
  }                             /* end if */
  else
  {
    ilRC = RC_FAIL;
  }                             /* end else */
  dbg (TRACE, "SPLIT CHAIN READY FOR EXIT");
  return ilRC;
}                               /* end SplitChain */

/* ***************************************************
* Function: 	JoinFlights
* Parameter: 	
* Return:	RC_SUCCESS, RC_FAIL
* Description:	Joins rotations.
*******************************************************/
static int JoinFlights (char *pcpRcvTable, char *pcpFields, char *pcpData, char *pcpSelKey, int ipAnswFlag)
{
  int ilRC = RC_SUCCESS;
  char pclTable[32];
  char pclLeftUrno[16];
  char pclLeftRkey[16];
  char pclRightUrno[16];
  char pclRightRkey[16];
  char pclRightFtyp[4];
  char pclSqlWhere[128];
  char pclJoinWriteFields[] = "RKEY,RTYP,REGN,ACT3,ACT5,ACTI,ANNX,MTOW,ENTY";
  char pclJoinCheckFields[] = "RKEY,FTYP";
  char pclJoinWriteData[4096];

  dbg (TRACE, "===== DATA ELEMENTS FROM CLIENT =========");
  dbg (TRACE, "JOF TABLE  <%s>", pcpRcvTable);
  dbg (TRACE, "JOF FIELDS <%s>", pcpFields);
  dbg (TRACE, "JOF DATA   <%s>", pcpData);
  dbg (TRACE, "JOF SELECT <%s>", pcpSelKey);
  dbg (TRACE, "=========================================");

/* ATTENTION: */
/* ALL DATA FROM CLIENT ARE IGNORED (TO DO LATER) */

  ilRC = RC_SUCCESS;
  strcpy (pclTable, pcgAftTab);

  if (ilRC == RC_SUCCESS)
  {
    dbg (TRACE, "FETCH DATA OF LEFT CHAIN");
    (void) get_real_item (pclLeftUrno, pcpSelKey, 1);
    if (igTWEUrnoIsNumber == TRUE)
    {
      sprintf (pclSqlWhere, "WHERE URNO=%s", pclLeftUrno);
    }
    else
    {
      sprintf (pclSqlWhere, "WHERE URNO='%s'", pclLeftUrno);
    }
    ilRC = SearchFlight (pclTable, pclJoinWriteFields, pclSqlWhere, FOR_SEARCH, TRUE);
    dbg (TRACE, "DATA: <%s>", pcgDataArea);
    strcpy (pclJoinWriteData, pcgDataArea);
  }                             /* end if */

  if (ilRC == RC_SUCCESS)
  {
    (void) get_real_item (pclLeftRkey, pclJoinWriteData, 1);

    dbg (TRACE, "FETCH DATA OF RIGHT CHAIN");
    (void) get_real_item (pclRightUrno, pcpSelKey, 3);
    if (igTWEUrnoIsNumber == TRUE)
    {
      sprintf (pclSqlWhere, "WHERE URNO=%s", pclRightUrno);
    }
    else
    {
      sprintf (pclSqlWhere, "WHERE URNO='%s'", pclRightUrno);
    }
    ilRC = SearchFlight (pclTable, pclJoinCheckFields, pclSqlWhere, FOR_SEARCH, TRUE);
    dbg (TRACE, "DATA: <%s>", pcgDataArea);
  }                             /* end if */

  if (ilRC == RC_SUCCESS)
  {
    (void) get_real_item (pclRightRkey, pcgDataArea, 1);
    (void) get_real_item (pclRightFtyp, pcgDataArea, 2);
    if (strcmp (pclLeftRkey, pclRightRkey) == 0)
    {
      dbg (TRACE, "BOTH FLIGHTS ALREADY IN SAME ROTATION");
      ilRC = RC_FAIL;
    }                           /* end if */
  }                             /* end if */

  if (ilRC == RC_SUCCESS)
  {
    if ((pclRightFtyp[0] == 'O') || (pclRightFtyp[0] == 'S'))
    {
      ilRC = SplitChain (pclLeftUrno, pclLeftRkey, LAST_LEFT);
    }                           /* end if */

    ilRC = JoinChain (pclRightUrno, pclRightRkey, pclJoinWriteFields, pclJoinWriteData);
  }                             /* end if */
  else
  {
    /* Collect ErrorMessages */
    ;
  }                             /* end else */

/* wg. PROBLEME IM CLIENT */
/*
  (void) tools_send_info_flag(que_out,0, pcgDestName, "", pcgRecvName,
                                "", "", pcgTwStart, pcgTwEnd,
                                "JOF", pclChainTables, "OK", "SPR", "EOT", 0);
*/

  if ((ipAnswFlag == TRUE) && (igGlNoAck == FALSE))
  {
    (void) tools_send_info_flag (que_out, 0, pcgDestName, "", pcgRecvName, "", "", pcgTwStart, pcgTwEnd, "JOF",
                                 pcpRcvTable, pcpSelKey, pcpFields, pcpData, 0);
  }                             /* end if */

  dbg (TRACE, "=========================================");
  return ilRC;
}                               /* end JoinFlights */

/* *************************************************** */
/* *************************************************** */
static int JoinChain (char *pcpUrno, char *pcpOldRkey, char *pcpWriteFields, char *pcpWriteData)
{
  int ilRC = RC_SUCCESS;
  int ilUrnoFound = FALSE;
  int ilTbl = 0;
  int ilTblCnt = 0;
  int ilTblNbr = 0;
  int ilRec = 0;
  int ilFld = 0;
  int ilFldCnt = 0;
  int ilWriteFldCnt = 0;
  int ilWriteItem = 0;
  int ilAdidIdx = 0;
  int ilUrnoIdx = 0;
  int ilRkeyIdx = 0;
  int ilTifaIdx = 0;
  int ilUrnoTifaVal = 0;
  int ilRecTifaVal = 0;
  int ilGetSelRc = 0;
  int ilChgCnt = 0;
  short slFkt = 0;
  short slSelCursor = 0;
  char pclTables[16];
  char pclActTable[16];
  char pclArrUrno[16];
  char pclCurUrno[16];
  char pclNewRkey[16];
  char pclFldNam[16];
  char pclFldDat[2048];
  char pclSelSqlBuf[2048];
  char pclSelSqlDat[4096];
  char pclUpdSqlWhere[128];
  char pclSelFields[] = "URNO,ADID,FTYP,RTYP,TIFA,TIFD,REGN";

  dbg (TRACE, "JOIN CHAINS ENTERED");
  strcpy (pclArrUrno, "?");
  strcpy (pclCurUrno, "?");
  strcpy (pclTables, pcgAftTab);

  ilWriteFldCnt = field_count (pcpWriteFields);
  ilFld = get_item_no (pcpWriteFields, "RKEY", 5);
  ilFld++;
  (void) get_real_item (pclNewRkey, pcpWriteData, ilFld);

  ilChgCnt = 0;
  ilTblCnt = field_count (pclTables);
  for (ilTblNbr = 1; ilTblNbr <= ilTblCnt; ilTblNbr++)
  {
    (void) get_real_item (pclActTable, pclTables, ilTblNbr);
    ilTbl = GetTableIndex (pclActTable);
    (void) BuildSqlReadFields (ilTbl, pclSelFields, FOR_NORMAL);
    if (igTWEUrnoIsNumber == TRUE)
    {
      sprintf (pclSelSqlBuf, "SELECT %s FROM %s WHERE RKEY=%s OR RKEY=%s" " ORDER BY ADID,TIFA",
               prgPrjTblCfg[ilTbl].ActFldLst, pclActTable, pclNewRkey, pcpOldRkey);
    }
    else
    {
      sprintf (pclSelSqlBuf, "SELECT %s FROM %s WHERE RKEY='%s' OR RKEY='%s'" " ORDER BY ADID,TIFA",
               prgPrjTblCfg[ilTbl].ActFldLst, pclActTable, pclNewRkey, pcpOldRkey);
    }
    dbg (TRACE, "<%s>", pclSelSqlBuf);

    ilFldCnt = field_count (prgPrjTblCfg[ilTbl].ActFldLst);
    ilRkeyIdx = GetFieldIndex (ilTbl, "RKEY");
    ilUrnoIdx = GetFieldIndex (ilTbl, "URNO");
    ilTifaIdx = GetFieldIndex (ilTbl, "TIFA");
    ilAdidIdx = GetFieldIndex (ilTbl, "ADID");
    ilRec = 1;
    slFkt = START;
    slSelCursor = 0;
    
    do
    {
      ilGetSelRc = sql_if (slFkt, &slSelCursor, pclSelSqlBuf, pclSelSqlDat);
      if (ilGetSelRc == DB_SUCCESS)
      {
        BuildItemBuffer (pclSelSqlDat, pclSelFields, ilFldCnt, ",");
        /* dbg(DEBUG,"DATA: <%s>",pclSelSqlDat); */
        (void) GetRecordData (ilTbl, ilRec, ilFldCnt, pclActTable, prgPrjTblCfg[ilTbl].ActFldLst, pclSelSqlDat);
        (void) ShowRecordData ();
        ilRC = PrepNewData (0);
        if (prgRecFldDat[ilTbl][ilRec][ilAdidIdx].DbsDat[0] == 'A')
        {
          strcpy (pclArrUrno, prgRecFldDat[ilTbl][ilRec][ilUrnoIdx].DbsDat);
          strcpy (pclCurUrno, " ");
        }                       /* end if */
        if (prgRecFldDat[ilTbl][ilRec][ilAdidIdx].DbsDat[0] == 'B')
        {
          strcpy (pclArrUrno, prgRecFldDat[ilTbl][ilRec][ilUrnoIdx].DbsDat);
        }                       /* end if */
        if (ilUrnoFound == FALSE)
        {
          if (strcmp (prgRecFldDat[ilTbl][ilRec][ilUrnoIdx].DbsDat, pcpUrno) == 0)
          {
            ilUrnoTifaVal = prgRecFldDat[ilTbl][ilRec][ilTifaIdx].DbsVal;
            dbg (TRACE, "URNO FOUND: TIFA <%s> (%d)", prgRecFldDat[ilTbl][ilRec][ilTifaIdx].DbsDat, ilUrnoTifaVal);
            dbg (TRACE, "TIMESTAMP:  TIME <%s> (%d)", pcgCurTime, igSysMinStamp);
            ilUrnoFound = TRUE;
          }                     /* end if */
        }                       /* end if */
        if ((ilUrnoFound == TRUE) && (strcmp (prgRecFldDat[ilTbl][ilRec][ilRkeyIdx].DbsDat, pcpOldRkey) == 0))
        {
          ilRecTifaVal = prgRecFldDat[ilTbl][ilRec][ilTifaIdx].DbsVal;
          dbg (TRACE, "HANDLED RECORD TIFA <%s> (%d)", prgRecFldDat[ilTbl][ilRec][ilTifaIdx].DbsDat, ilRecTifaVal);
          if ((ilRecTifaVal >= igSysMinStamp) || (igJoinAlways == TRUE))
          {
            dbg (TRACE, "WE CAN JOIN THAT RECORD");
            if (ilRC == RC_SUCCESS)
            {
              for (ilWriteItem = 1; ilWriteItem <= ilWriteFldCnt; ilWriteItem++)
              {
                (void) get_real_item (pclFldNam, pcpWriteFields, ilWriteItem);
                (void) get_real_item (pclFldDat, pcpWriteData, ilWriteItem);
                (void) SetNewValues (ilTbl, ilRec, 0, pclFldNam, pclFldDat);
              }                 /* end for */
              (void) SetNewValues (ilTbl, ilRec, 0, "RTYP", "J");
              (void) SetNewValues (ilTbl, ilRec, 0, "AURN", pclCurUrno);
              if (igTWEUrnoIsNumber == TRUE)
              {
                sprintf (pclUpdSqlWhere, "WHERE URNO=%s", prgRecFldDat[ilTbl][ilRec][ilUrnoIdx].NewDat);
              }
              else
              {
                sprintf (pclUpdSqlWhere, "WHERE URNO='%s'", prgRecFldDat[ilTbl][ilRec][ilUrnoIdx].NewDat);
              }
              dbg (TRACE, "JoinChain: UPDATE AND BROADCAST FLIGHT WITH URNO <%s> ", pclUpdSqlWhere);
              ilRC = MakeUpdateAndBroadCast (__LINE__,ilTbl, ilRec, pclUpdSqlWhere, TRUE, "UPJ", "URNO");
              ilChgCnt++;
            }                   /* end if */
          }                     /* end if */
        }                       /* end if */
        else
        {
          if (ilRC == RC_SUCCESS)
          {
            if (strcmp (prgRecFldDat[ilTbl][ilRec][ilRkeyIdx].DbsDat, pclNewRkey) == 0)
            {
              /* Record of left Chain */
              (void) SetNewValues (ilTbl, ilRec, 0, "RTYP", "J");
              (void) SetNewValues (ilTbl, ilRec, 0, "AURN", pclCurUrno);
              if (igTWEUrnoIsNumber == TRUE)
              {
                sprintf (pclUpdSqlWhere, "WHERE URNO=%s", prgRecFldDat[ilTbl][ilRec][ilUrnoIdx].NewDat);
              }
              else
              {
                sprintf (pclUpdSqlWhere, "WHERE URNO='%s'", prgRecFldDat[ilTbl][ilRec][ilUrnoIdx].NewDat);
              }
              ilRC = MakeUpdateAndBroadCast (__LINE__,ilTbl, ilRec, pclUpdSqlWhere, TRUE, "UPJ", "URNO");
            }                   /* end if */
            else
            {
              /* Record of right Chain */
              (void) SetNewValues (ilTbl, ilRec, 0, "RTYP", "S");
              (void) SetNewValues (ilTbl, ilRec, 0, "AURN", " ");
              if (igTWEUrnoIsNumber == TRUE)
              {
                sprintf (pclUpdSqlWhere, "WHERE URNO=%s", prgRecFldDat[ilTbl][ilRec][ilUrnoIdx].NewDat);
              }
              else
              {
                sprintf (pclUpdSqlWhere, "WHERE URNO='%s'", prgRecFldDat[ilTbl][ilRec][ilUrnoIdx].NewDat);
              }
              ilRC = MakeUpdateAndBroadCast (__LINE__,ilTbl, ilRec, pclUpdSqlWhere, TRUE, "UPS", "URNO");
            }                   /* end else */
          }                     /* end if */
        }                       /* end else */
        strcpy (pclCurUrno, pclArrUrno);
      }                         /* end if */
      slFkt = NEXT;
    }
    while (ilGetSelRc == DB_SUCCESS);
    close_my_cursor (&slSelCursor);
    

  }                             /* end for */
  if ((ilRC == RC_SUCCESS) && (ilChgCnt > 0))
  {
    sprintf (pclSelSqlBuf, "%s,%s,%s", pclNewRkey, pcpOldRkey, pclNewRkey);
    sprintf (pclSelSqlDat, "%s,%s,%s", pclNewRkey, pcpOldRkey, pclNewRkey);
    (void) tools_send_info_flag (igToBcHdl, 0, pcgDestName, "", pcgRecvName, "", "", pcgTwStart, pcgTwEnd, "JOF", "",
                                 pclSelSqlBuf, "", pclSelSqlDat, 0);
    dbg (TRACE, "BROADCASTED 'JOF' DAT <%s>", pclSelSqlDat);
  }                             /* end if */
  else
  {
    ilRC = RC_FAIL;
  }                             /* end else */
  return ilRC;
}                               /* end JoinChain */

/* ***************************************************
* Function: 		HandleGetFlightRecord
* Parameter: 		void
* Return:				RC_SUCCESS, RC_FAIL
* Description:	Retrieves flight record.
*******************************************************/
static int HandleGetFlightRecord (char *pcpLineEnd, char *pcpTable, char *pcpFields, char *pcpData, char *pcpSelKey)
{
  int i, ilRC = RC_SUCCESS;
  int ilFldcnt;
  short ilFkt, ilLocalCursor = 0;
  int ilLen = 0, ilTotalBufLen, ilFound = FALSE;
  int ilNoOfRec = 0;
  int ilTotalRecs = 0;
  int ilLineEndChars = 0;
  char pclRcvSelKey[12 * 1024];
  char pclRcvOrder[1024];
  char pclTmpBuf[16];
  BC_HEAD *prlBchd;
  CMDBLK *prlCmdblk;
  char *pclSelection, *pclFields;
  char *pclFldPtr;
  char pclTable[10];
  char *pclData;
  char *pclTmpPtr;
  int ilGetRotations = FALSE;
  int ilLoopCnt = 0;
  int ilTimeOutCheck = RC_SUCCESS;

  memset (pclTable, '\0', 10);
  pcgDataArea[0] = '\0';

  if (pcpTable == NULL)
  {
    prlBchd = (BC_HEAD *) ((char *) prgEvent + sizeof (EVENT));
    prlCmdblk = (CMDBLK *) ((char *) prlBchd->data);
    pclSelection = (char *) prlCmdblk->data;
    pclFields = (char *) pclSelection + strlen (pclSelection) + 1;
    pclData = (char *) pclFields + strlen (pclFields) + 1;
  }                             /* end if */
  else
  {
    pcgTmpBuf1[0] = 0x00;
    CheckTimeOut (TIMO_INIT, pcgTmpBuf1);
    pclSelection = pcpSelKey;
    pclFields = pcpFields;
    pclData = pcpData;
    que_out = 0;
  }                             /* end else */

  ilFldcnt = field_count (pclFields);

  dbg (TRACE, "--------- FIELDS ----------\n<%s>", pclFields);
  dbg (TRACE, "------- SELECTION ---------\n<%s>", pclSelection);
  dbg (TRACE, "---------------------------");
  dbg (TRACE, "------- DATA --------- <%s>", pclData);

  if (pcgResultData == NULL)
  {
    pcgResultData = (char *) calloc (1, RESULTBUFSTEP);
    if (pcgResultData == NULL)
    {
      dbg (TRACE, "GFR: Malloc error pcgResultData");
      ilRC = RC_FAIL;
      Terminate ();
    }
    else
    {
      igActResultSize = RESULTBUFSTEP;
    }
  }

  pclTmpPtr = strstr (pclSelection, "[CONFIG]");
  if (pclTmpPtr != NULL)
  {
    (void) GetCfg ("SOURCEFLAG_CTRL", "DSSF_FLDLST", CFG_STRING, pcgTmpBuf1, "\0");
    ilLen = 0;
    StrgPutStrg (pcgResultData, &ilLen, "LKEY", 0, -1, ",");
    StrgPutStrg (pcgResultData, &ilLen, pcgLangCode, 0, -1, pcpLineEnd);
    StrgPutStrg (pcgResultData, &ilLen, "HOME", 0, -1, ",");
    StrgPutStrg (pcgResultData, &ilLen, pcgDefH3LC, 0, -1, ",");
    StrgPutStrg (pcgResultData, &ilLen, pcgDefH4LC, 0, -1, pcpLineEnd);
    StrgPutStrg (pcgResultData, &ilLen, "EXTS", 0, -1, ",");
    StrgPutStrg (pcgResultData, &ilLen, pcgDefTblExt, 0, -1, ",");
    StrgPutStrg (pcgResultData, &ilLen, pcgDefBasExt, 0, -1, pcpLineEnd);
    StrgPutStrg (pcgResultData, &ilLen, "DSSF", 0, -1, ",");
    StrgPutStrg (pcgResultData, &ilLen, pcgTmpBuf1, 0, -1, pcpLineEnd);
    StrgPutStrg (pcgResultData, &ilLen, "SCCS", 0, -1, ",");
    StrgPutStrg (pcgResultData, &ilLen, version_nbr, 0, -1, pcpLineEnd);
    StrgPutStrg (pcgResultData, &ilLen, "DATE", 0, -1, ",");
    StrgPutStrg (pcgResultData, &ilLen, version_date, 0, -1, pcpLineEnd);

/* NEW  TWE  080300 */
    /* if UTCDIFF not in config than send zero (client gets difftime itself) */
    if (igTWEUtc2LocalfromConfig == TRUE)
    {
      sprintf (pcgTmpBuf1, "UTCD,%d", igUtcToLocal);
    }
    else
    {
      strcpy (pcgTmpBuf1, "UTCD,0");
    }
    StrgPutStrg (pcgResultData, &ilLen, pcgTmpBuf1, 0, -1, ",");
    StrgPutStrg (pcgResultData, &ilLen, pcgDbsTimeType, 0, -1, pcpLineEnd);
    StrgPutStrg (pcgResultData, &ilLen, "CCHR", 0, -1, ",");
    StrgPutStrg (pcgResultData, &ilLen, pcgClntCharSet, 0, -1, pcpLineEnd);
    StrgPutStrg (pcgResultData, &ilLen, "SCHR", 0, -1, ",");
    StrgPutStrg (pcgResultData, &ilLen, pcgServCharSet, 0, -1, pcpLineEnd);
    StrgPutStrg (pcgResultData, &ilLen, "LOCF", 0, -1, ",");
    StrgPutStrg (pcgResultData, &ilLen, pcgTWEDepLocTimFlds, 0, -1, pcpLineEnd);
    ilRC = RC_SUCCESS;
    ilLoopCnt = 0;
    do
    {
      ilLoopCnt++;
      sprintf (pclTmpBuf, "MODUL%d", ilLoopCnt);
      ilRC = GetCedaCfg ("CLIENT_VERSIONS", pclTmpBuf, pcgTmpBuf1, "NOT_FND");
      if (ilRC == RC_SUCCESS)
      {
        StrgPutStrg (pcgResultData, &ilLen, "APPL", 0, -1, ",");
        StrgPutStrg (pcgResultData, &ilLen, pcgTmpBuf1, 0, -1, pcpLineEnd);
      }                         /* end if */
    }
    while (ilRC == RC_SUCCESS);
    ilLen--;
    pcgResultData[ilLen] = 0x00;
    ilRC = new_tools_send_sql (que_out, prlBchd, prlCmdblk, "CONFIG", pcgResultData);
    dbg (TRACE, "SENT CONFIG:\n%s", pcgResultData);
    return ilRC;
  }                             /* end if */

  pclTmpPtr = strstr (pclSelection, "[ROTATIONS]");
  if (pclTmpPtr != NULL)
  {
    ilGetRotations = TRUE;
    *pclTmpPtr = 0x00;
  }                             /* end if */

  ilLineEndChars = strlen (pcpLineEnd);

  strcpy (pclRcvSelKey, pclSelection);
  CheckWhereClause (FALSE, pclRcvSelKey, TRUE, TRUE, pclRcvOrder);

/* QUICK HACK */
/****************************/
  pclFldPtr = NULL;
  if (pcpTable == NULL)
  {                             /* table unknown */
    dbg (TRACE, "GFR: pcpTable = NULL");
    dbg (TRACE, "GFR: COMPARE <%s> with <%s> OR <%s> with <%s>", prlCmdblk->obj_name, pcgAftTab, prlCmdblk->obj_name,
         pcgArcTab);
    if (pclFldPtr != NULL)
    {
    }
    else if (strcmp (prlCmdblk->obj_name, pcgAftTab) == 0)
    {
      strcpy (pclTable, pcgAftTab);
      ilFound = TRUE;
    }
    else if (strcmp (prlCmdblk->obj_name, pcgArcTab) == 0)
    {
      strcpy (pclTable, pcgArcTab);
      ilFound = TRUE;
    }
  }
  else
  {
    strcpy (pclTable, pcpTable); /* table exists */
  }                             /* end else */
/*****************************/

  memset (pcgDataArea, '\0', 5);
  ilTotalBufLen = 0;
  memset (pcgResultData, '\0', 5);
  ilTotalRecs = 0;

  /* 1. mal immer (AFT)! evtl 2.mal (ARC) */

  for (i = 0; i < 2 && ((ilFound == FALSE) || (i == 0)); i++)
  {                             /* TWE inserted KLAMMERN! */
    /* If Table not yet determined, try table in ff order
       ACTUAL and finally ARCHIV */
    if (ilFound == FALSE)
    {
      switch (i)
      {
        case 0:
          strcpy (pclTable, pcgAftTab);
          break;
        case 1:
          strcpy (pclTable, pcgArcTab);
          break;
        default:
          break;
      }                         /* end switch */
    }                           /* end if */

    if (ilGetRotations != TRUE)
    {
      sprintf (pcgSqlBuf, "SELECT %s FROM %s %s %s", pclFields, pclTable, pclRcvSelKey, pclRcvOrder);
    }                           /* end if */
    else
    {
      sprintf (pcgSqlBuf, "SELECT %s FROM %s WHERE RKEY IN (SELECT RKEY FROM %s %s) %s", pclFields, pclTable, pclTable,
               pclRcvSelKey, pclRcvOrder);
    }                           /* end else */

    ilLoopCnt = 0;
    do
    {
      ilLoopCnt++;
      ilNoOfRec = 0;
      if (ilLoopCnt == 2)
      {
        pclTmpPtr = strstr (pcgSqlBuf, pclTable);
        pclTmpPtr++;
        pclTmpPtr = strstr (pclTmpPtr, pclTable);
        if (strcmp (pclTable, pcgAftTab) == 0)
        {
          strncpy (pclTmpPtr, pcgArcTab, 6);
        }                       /* end if */
        else
        {
          strncpy (pclTmpPtr, pcgAftTab, 6);
        }                       /* end else */
      }                         /* end if */

      dbg (TRACE, "SELECTING FROM TABLE <%s>", pclTable);
      dbg (DEBUG, "SQL STATEMENT\n<%s>", pcgSqlBuf);

      

      ilRC = DB_SUCCESS;
      ilFkt = START;
      ilLocalCursor = 0;
      while ((ilRC == DB_SUCCESS) && (ilTimeOutCheck == RC_SUCCESS))
      {
        memset (pcgDataArea, '\0', 5);
        ilRC = sql_if (ilFkt, &ilLocalCursor, pcgSqlBuf, pcgDataArea);
        if (ilRC == DB_SUCCESS)
        {
          /* search in both tables!! (AFT AND ARC) Total result wished */
          /* SO BEFORE INSERTED IS TAKEN AWAY AGAIN */
/*  ilFound = TRUE;   *//* TWE inserted */
          if (ilFkt == START)
          {
            dbg (TRACE, "GOT FIRST RECORD");
          }                     /* end if */
          else if ((ilTotalRecs % 50) == 0)
          {
            dbg (TRACE, "%d RECORDS (%d BYTES)", ilTotalRecs, ilTotalBufLen);
          }                     /* end if */
          ilNoOfRec++;
          ilTotalRecs++;
          ilLen = BuildItemBuffer (pcgDataArea, "", ilFldcnt, ",");
          strcat (pcgDataArea, pcpLineEnd);
          ilLen += ilLineEndChars;

          if ((12 + ilTotalBufLen + ilLen) >= igActResultSize)
          {
            while ((12 + ilTotalBufLen + ilLen) >= igActResultSize)
            {
              igActResultSize += RESULTBUFSTEP;
            }                   /* end while */
            pcgResultData = (char *) realloc (pcgResultData, igActResultSize);
            if (pcgResultData == NULL)
            {
              ilRC = RC_FAIL;
              dbg (TRACE, "GFR: Malloc error (pcgResultData).");
              break;
            }
            else
            {
              dbg (TRACE, "REALLOCATED BUFFER TO %d BYTES (%d REC'S)", igActResultSize, ilTotalRecs);
            }                   /* end else */
          }                     /* end if */
          memcpy (pcgResultData + ilTotalBufLen, pcgDataArea, ilLen);
          ilTotalBufLen += ilLen;
          pcgResultData[ilTotalBufLen] = '\0';
        }                       /* DB_SUCCESS */
        else if (ilRC == RC_FAIL)
        {
          dbg (DEBUG, "GFR: SQL Buffer<%s>", pcgSqlBuf);
          get_ora_err (-1, pcgErrMsg);
          dbg (TRACE, "GFR: Oracle Error <%s>", pcgErrMsg);
        }
        ilFkt = NEXT;
        ilTimeOutCheck = CheckTimeOut (TIMO_CHECK, "");
      }                         /* while */

      close_my_cursor (&ilLocalCursor);
      dbg (TRACE, "GFR: FETCHED <%d> RECORDS", ilNoOfRec);

    }
    while ((ilGetRotations == TRUE) && (ilLoopCnt < 2) && (ilTimeOutCheck == RC_SUCCESS));

    

  }                             /* end for i (AFT AND ARC) */

  /* tables are read out */

  dbg (TRACE, "GFR: TOTAL <%d> RECORDS FETCHED", ilTotalRecs);

  if (ilTimeOutCheck == RC_SUCCESS)
  {                             /* no timeout */
    if (pcgResultData != NULL)
    {
      if (igGlNoAck == FALSE)
      {
        if (que_out > 0)
        {
          if (ilRC == RC_FAIL)
          {
            prlBchd->rc = RC_FAIL;
            strcpy (pcgResultData, pcgErrMsg);
          }
          else if (ilTotalBufLen == 0)
          {
            prlBchd->rc = RC_SUCCESS;
          }
          else
          {
            pcgResultData[ilTotalBufLen - 1] = '\0';
            prlBchd->rc = RC_SUCCESS;
          }
          ilRC = new_tools_send_sql (que_out, prlBchd, prlCmdblk, pclFields, pcgResultData);
        }                       /* end if que_out */
        dbg (TRACE, "TOTAL READ %d BYTES", ilTotalBufLen);
      }                         /* if acknowledge */
    }
    else
    {                           /* no pcgResultData */

      prlBchd->rc = RC_FAIL;
      dbg (TRACE, "GFR: Failed to alloc pcgResultData");
      ilRC = new_tools_send_sql (que_out, prlBchd, prlCmdblk, pclFields, "");
    }                           /* end else */
  }                             /* end if no timeout */
  else
  {
    dbg (TRACE, "SELECTING OF RECORDS STOPPED");
    dbg (TRACE, "NO ANSWER TO SENDER DUE TO TIMEOUT");
    ilRC = RC_SUCCESS;
  }                             /* end else */

  return ilRC;

}                               /* end HandleGetFlightRecords */

/* ***************************************************
* Function: 		HandleGetDeletedFlights
* Parameter: 		void
* Return:				RC_SUCCESS, RC_FAIL
* Description:	Retrieves flight record.
*******************************************************/
static int HandleGetDeletedFlights (void)
{
  int ilRC = RC_SUCCESS;
  dbg (TRACE, "COMMAND 'GDF' NOT YET SUPPORTED");
  return ilRC;
}                               /* end HandleGetDeletedFlights */

/*********************************************************
* Function: 		CleanUpFlightTable()
* Parameter: 		none
* Return:				RC_SUCCESS,RC_FAIL
* Description:	Updates ACTUAL and ARCHIV Tables
**********************************************************/
static int CleanUpFlightTables ()
{
  time_t ilLocalTime = 0;
  time_t ilTmp;
/**********   TWE  211299  **********/
/*
	short ilFkt,ilLocalCursor=0;
*/
  short ilFkt;
  short slSearchCursor = 0;
  short slDeleteCursor = 0;
/************************************/
  static char pclDate[10];
  int ilRC = DB_SUCCESS;
  int ilTbl = 0;

  (void) GetCfg ("GLOBAL_DEFINES", "ARCHIVE_FLIGHTS", CFG_STRING, pcgTmpBuf1, "YES");
  if (strcmp (pcgTmpBuf1, "NO") == 0)
  {
    debug_level = TRACE;
    dbg (TRACE, "FLIGHTS NOT ARCHIVED DUE TO CONFIG ENTRY 'NO'");
    return RC_SUCCESS;
  }                             /* end if */

  ilTmp = mktime (prgCurDate);
  pcgDataArea[0] = '\0';
  ilFkt = NOACTION | REL_CURSOR;
  slSearchCursor = 0;           /* TWE */

  ilLocalTime = ilTmp + ((igDaysPrior + 1) * 24 * 60 * 60);
  strftime (pclDate, 10, "%" "Y%" "m%" "d", gmtime (&ilLocalTime));

  ilTbl = GetTableIndex (pcgAftTab);
  /* (void) BuildSqlReadFields(ilTbl,"", FOR_TABLES); */

  /* Move obsolete records from ACTUAL to ARCHIV */
  sprintf (pcgSqlBuf,
           "INSERT INTO %s (%s) (SELECT %s FROM %s WHERE " "(TIFD<'%s000000' AND (ORG3='%s' OR ADID='U')) OR "
           "(TIFA<'%s000000' AND DES3='%s' AND (RTYP<>'S' OR FTYP<>'O'))) ", pcgArcTab, prgPrjTblCfg[ilTbl].AllFldLst,
           prgPrjTblCfg[ilTbl].AllFldLst, pcgAftTab, pclDate, pcgH3LC, pclDate, pcgH3LC);
  dbg (DEBUG, "CleanUp: pcgSqlBuf\n<%s>", pcgSqlBuf);
  ilRC = sql_if (ilFkt, &slSearchCursor, pcgSqlBuf, pcgDataArea); /* TWE */
  dbg (DEBUG, "CleanUp: sql_if returned <%d>", ilRC);
  if (ilRC != DB_SUCCESS)
  {
    rollback ();
    dbg (TRACE, "CleanUp (ROLLBACK): (Copy to %s) RC From Sql_If <%d>", pcgArcTab, ilRC);
  }                             /* end if */
  else
  {
    /* Delete out-dated data from ACTUAL */
    slDeleteCursor = 0;         /* TWE */
    sprintf (pcgSqlBuf,
             "DELETE FROM %s WHERE " "(TIFD<'%s000000' AND (ORG3='%s' OR ADID='U')) OR "
             "(TIFA<'%s000000' AND DES3='%s' AND (RTYP<>'S' OR FTYP<>'O')) ", pcgAftTab, pclDate, pcgH3LC, pclDate,
             pcgH3LC);

    dbg (DEBUG, "CleanUp: pcgSqlBuf\n<%s>", pcgSqlBuf);
    ilRC = sql_if (ilFkt, &slDeleteCursor, pcgSqlBuf, pcgDataArea);
    dbg (DEBUG, "CleanUp: sql_if returned <%d>", ilRC);

    if (ilRC != DB_SUCCESS)
    {
      rollback ();
      get_ora_err (ilRC, pcgDataArea);
      dbg (TRACE, "CleanUpFlightTable: (DELETE from %s) Oracle message <%s>", pcgAftTab, pcgDataArea);
    }
    else
    {
      commit_work ();
    }                           /* end if */
    close_my_cursor (&slDeleteCursor); /* TWE */
  }                             /* end if */
  close_my_cursor (&slSearchCursor); /* TWE */

  return ilRC;
}                               /* end CleanUpFlightTables */

/********************************************************/
/********************************************************/
static int InitTableFields (int ipFromTbl, int ipToTbl)
{
  int ilRC = RC_SUCCESS;
  int ilNoOfSysFields;
  int ilFld = 0;
  int ilFldIdx = 0;
  int ilTbl = 0;
  int ilRowCnt = 0;
  int ilFirstTable = 0;
  int ilLastTable = 0;
  int ilItmLen = 0;
  int ilTmpVal = 0;
  int ilLoopCount = 0;
  short ilLocalCursor = 0;
  short ilFkt = START;
  char pclCfgDetCod[16];
  char pclSysTabName[16];
  char pclSysTabWhere[64];
  char pclFldNam[32];
  char pclCfgSec[32];
  char pclCfgDet[32];
  char pclCfgRow[512];
/* We Have To Handle The Selected Fields from SysTab in the Following Order. */
/* Even If We Could Read them By Flags Like 'Used By FlightHandler' */
  char pclSysTabFields[] = "URNO,FINA,ADDI,FELE,TYPE,DEFA,SYST,CONT";

  igChkDblFld = TRUE;

  ilNoOfSysFields = field_count (pclSysTabFields);

  if (ipFromTbl > 0)
  {
    ilFirstTable = ipFromTbl;
  }                             /* end if */
  else
  {
    ilFirstTable = 1;
  }                             /* end else */

  if (ipToTbl > 0)
  {
    ilLastTable = ipToTbl;
  }                             /* end if */
  else
  {
    ilLastTable = igHandledTables;
  }                             /* end else */

  for (ilTbl = ilFirstTable; ilTbl <= ilLastTable; ilTbl++)
  {
    dbg (DEBUG, "=====================================");
    dbg (DEBUG, "CONFIG TABLE <%s> TYPE <%s> (%d)", prgPrjTblCfg[ilTbl].FullName, prgPrjTblCfg[ilTbl].TableType,
         prgPrjTblCfg[ilTbl].TypeOfTable);
    sprintf (pclSysTabName, "SYS%s", prgPrjTblCfg[ilTbl].TableExt);
    if (strcmp (pcgDefTblExt, "TAB") == 0)
    {
      sprintf (pclSysTabWhere, "TANA='%s'", prgPrjTblCfg[ilTbl].ShortName);
    }                           /* end if */
    else
    {
/*
      sprintf(pclSysTabWhere,"TANA='%s' AND PROJ='%s'",
                              prgPrjTblCfg[ilTbl].ShortName,
                              prgPrjTblCfg[ilTbl].Project);
*/
      sprintf (pclSysTabWhere, "TANA='%s'", prgPrjTblCfg[ilTbl].ShortName);
    }                           /* end else */
    sprintf (pcgSqlBuf, "SELECT %s FROM %s WHERE %s ORDER BY FINA", pclSysTabFields, pclSysTabName, pclSysTabWhere);

    prgPrjTblCfg[ilTbl].TblFldCnt = 0;
    prgPrjTblCfg[ilTbl].TblRecLen = 0;
    ilFld = 0;

    ilFkt = START;
    ilLocalCursor = 0;
    
    ilRC = DB_SUCCESS;
    while (ilRC == DB_SUCCESS)
    {
      ilRC = sql_if (ilFkt, &ilLocalCursor, pcgSqlBuf, pcgDataArea);
      if (ilRC == DB_SUCCESS)
      {
        BuildItemBuffer (pcgDataArea, pclSysTabFields, ilNoOfSysFields, ",");
        (void) get_real_item (pclFldNam, pcgDataArea, 2);
        ilFldIdx = GetFieldIndex (ilTbl, pclFldNam);
        if (ilFldIdx < 1)
        {
          ilFld++;
          if (ilFld < MAX_FLD_HDL)
          {
            /* "URNO" */
            ilItmLen = sizeof (prgTblFldCfg[ilTbl][ilFld].SysTabUrno);
            ilRC = GetSysTabVal (prgTblFldCfg[ilTbl][ilFld].SysTabUrno, ilItmLen, 1);

            /* "FINA" */
            ilItmLen = sizeof (prgTblFldCfg[ilTbl][ilFld].SysTabFina);
            ilRC = GetSysTabVal (prgTblFldCfg[ilTbl][ilFld].SysTabFina, ilItmLen, 2);
            /* */
            dbg (DEBUG, "FIELD <%s> <%s>", prgPrjTblCfg[ilTbl].FullName, prgTblFldCfg[ilTbl][ilFld].SysTabFina);
            /* */

            /* "ADDI" */
            ilItmLen = sizeof (prgTblFldCfg[ilTbl][ilFld].SysTabAddi);
            ilRC = GetSysTabVal (prgTblFldCfg[ilTbl][ilFld].SysTabAddi, ilItmLen, 3);

            /* "FELE" */
            ilItmLen = sizeof (prgTblFldCfg[ilTbl][ilFld].SysTabFele);
            ilRC = GetSysTabVal (prgTblFldCfg[ilTbl][ilFld].SysTabFele, ilItmLen, 4);

            /* "TYPE" */
            ilItmLen = sizeof (prgTblFldCfg[ilTbl][ilFld].SysTabType);
            ilRC = GetSysTabVal (prgTblFldCfg[ilTbl][ilFld].SysTabType, ilItmLen, 5);

            /* "DEFA" */
            ilItmLen = sizeof (prgTblFldCfg[ilTbl][ilFld].SysTabDefa);
            ilRC = GetSysTabVal (prgTblFldCfg[ilTbl][ilFld].SysTabDefa, ilItmLen, 6);

            /* "SYST" */
            ilItmLen = sizeof (prgTblFldCfg[ilTbl][ilFld].SysTabSyst);
            ilRC = GetSysTabVal (prgTblFldCfg[ilTbl][ilFld].SysTabSyst, ilItmLen, 7);

            /* "CONT" */
            ilItmLen = sizeof (prgTblFldCfg[ilTbl][ilFld].SysTabCont);
            ilRC = GetSysTabVal (prgTblFldCfg[ilTbl][ilFld].SysTabCont, ilItmLen, 8);

            /* Evaluate Internal Values */

            /* "FELE" -> FieldLength */
            ilTmpVal = atoi (prgTblFldCfg[ilTbl][ilFld].SysTabFele);
            prgTblFldCfg[ilTbl][ilFld].FieldLength = ilTmpVal;
            prgPrjTblCfg[ilTbl].TblRecLen += ilTmpVal;

            /* "TYPE" -> Internal Field Type */
            prgTblFldCfg[ilTbl][ilFld].FieldType = 0;
            ilRC = SetInternalFieldType (prgTblFldCfg[ilTbl][ilFld].SysTabType, &prgTblFldCfg[ilTbl][ilFld].FieldType);

            /* Reset Rest of Properties */
            prgTblFldCfg[ilTbl][ilFld].ReadField = FALSE;
            prgTblFldCfg[ilTbl][ilFld].ReadOnly = FALSE;
            prgTblFldCfg[ilTbl][ilFld].KeepEntry = FALSE;
            prgTblFldCfg[ilTbl][ilFld].WarnEntry = FALSE;
            prgTblFldCfg[ilTbl][ilFld].SendField = FALSE;
            prgTblFldCfg[ilTbl][ilFld].SendOnReop = FALSE;
            prgTblFldCfg[ilTbl][ilFld].AutoChange = TRUE;

            prgTblFldCfg[ilTbl][ilFld].CtrlFlagNbr = -1;
            prgTblFldCfg[ilTbl][ilFld].ExplodeField = FALSE;
            prgTblFldCfg[ilTbl][ilFld].ChildGroups = -1;
            prgTblFldCfg[ilTbl][ilFld].GroupLength = -1;
            prgTblFldCfg[ilTbl][ilFld].GroupMembers = -1;
            prgTblFldCfg[ilTbl][ilFld].FirstChild = -1;
            prgTblFldCfg[ilTbl][ilFld].LastChild = -1;
            prgTblFldCfg[ilTbl][ilFld].MainIndex = -1;
            prgTblFldCfg[ilTbl][ilFld].FirstChar = -1;
            prgTblFldCfg[ilTbl][ilFld].LastChar = -1;

          }                     /* end if */
          else
          {
            debug_level = TRACE;
            dbg (TRACE, "ARRAY OVERFLOW: FIELD %d\n<%s>", ilFld, pcgDataArea);
            igConfigError = TRUE;
          }                     /* end else */
        }                       /* end if */
        else
        {
          dbg (TRACE, "DUPLICATED FIELD <%s.%s> IGNORED", prgPrjTblCfg[ilTbl].ShortName, pclFldNam);
        }                       /* end else */
      }                         /* end if */
      else
      {
        if (ilRC != NOTFOUND)
        {
          igConfigError = TRUE;
          dbg (TRACE, "ERROR READING FROM %s", pclSysTabName);
        }                       /* end if */
      }                         /* end else */
      ilFkt = NEXT;
    }                           /* end while */
    close_my_cursor (&ilLocalCursor);
    

    if (ilFld < 1)
    {
      dbg (TRACE, "ERROR: TABLE <%s> HAS NO FIELDS", prgPrjTblCfg[ilTbl].ShortName);
      igConfigError = TRUE;
    }                           /* end if */

    prgPrjTblCfg[ilTbl].TblFldCnt = ilFld;
    prgPrjTblCfg[ilTbl].AllFldCnt = ilFld;
    dbg (DEBUG, "FIELD COUNT: TBL = %d ALL = %d", prgPrjTblCfg[ilTbl].TblFldCnt, prgPrjTblCfg[ilTbl].AllFldCnt);
    dbg (DEBUG, "RECORD LENGTH = %d", prgPrjTblCfg[ilTbl].TblRecLen);

    /* Look For Fields Not Yet Defined In SysTab */
    sprintf (pclCfgSec, "%s_ADDTABLEFIELDS", prgPrjTblCfg[ilTbl].TableType);

    strcpy (pclCfgDetCod, "ADD");

    for (ilLoopCount = 1; ilLoopCount <= 2; ilLoopCount++)
    {
      dbg (DEBUG, "GET CONFIG SECTION <%s>", pclCfgSec);
      ilRC = RC_SUCCESS;
      ilRowCnt = 0;
      while (ilRC == RC_SUCCESS)
      {
        ilRowCnt++;
        sprintf (pclCfgDet, "%s_%d", pclCfgDetCod, ilRowCnt);

        ilRC = iGetConfigRow (pcgCfgFile, pclCfgSec, pclCfgDet, CFG_STRING, pclCfgRow);
        if (ilRC == RC_SUCCESS)
        {
          prgPrjTblCfg[ilTbl].AllFldCnt = ilFld;
          (void) get_real_item (pclFldNam, pclCfgRow, 1);
          ilFldIdx = GetFieldIndex (ilTbl, pclFldNam);
          if (ilFldIdx < 1)
          {
            ilFld++;
            if (ilFld < MAX_FLD_HDL)
            {
              /* "URNO" */
              strcpy (prgTblFldCfg[ilTbl][ilFld].SysTabUrno, "-1");

              /* "FINA" */
              strcpy (prgTblFldCfg[ilTbl][ilFld].SysTabFina, pclFldNam);
              if (ilLoopCount == 1)
              {
                dbg (TRACE, "ADD TABLE FIELD <%s> <%s>", prgPrjTblCfg[ilTbl].FullName,
                     prgTblFldCfg[ilTbl][ilFld].SysTabFina);
              }                 /* end if */
              else
              {
                dbg (TRACE, "ADD INTERNAL FIELD <%s> <%s>", prgPrjTblCfg[ilTbl].FullName,
                     prgTblFldCfg[ilTbl][ilFld].SysTabFina);
              }                 /* end else */

              /* "ADDI" */
              (void) get_real_item (prgTblFldCfg[ilTbl][ilFld].SysTabAddi, pclCfgRow, 2);

              /* "FELE" */
              (void) get_real_item (prgTblFldCfg[ilTbl][ilFld].SysTabFele, pclCfgRow, 3);

              /* "TYPE" */
              (void) get_real_item (prgTblFldCfg[ilTbl][ilFld].SysTabType, pclCfgRow, 4);

              /* "DEFA" */
              (void) get_real_item (prgTblFldCfg[ilTbl][ilFld].SysTabDefa, pclCfgRow, 5);

              /* "SYST" */
              (void) get_real_item (prgTblFldCfg[ilTbl][ilFld].SysTabSyst, pclCfgRow, 6);

              /* "CONT" */
              (void) get_real_item (prgTblFldCfg[ilTbl][ilFld].SysTabCont, pclCfgRow, 7);

              /* Evaluate Internal Values */

              /* "FELE" -> FieldLength */
              ilTmpVal = atoi (prgTblFldCfg[ilTbl][ilFld].SysTabFele);
              prgTblFldCfg[ilTbl][ilFld].FieldLength = ilTmpVal;
              prgPrjTblCfg[ilTbl].TblRecLen += ilTmpVal;

              /* "TYPE" -> Internal Field Type */
              prgTblFldCfg[ilTbl][ilFld].FieldType = 0;
              ilRC =
                SetInternalFieldType (prgTblFldCfg[ilTbl][ilFld].SysTabType, &prgTblFldCfg[ilTbl][ilFld].FieldType);

              /* Reset Rest of Properties */
              prgTblFldCfg[ilTbl][ilFld].ReadField = FALSE;
              prgTblFldCfg[ilTbl][ilFld].ReadOnly = FALSE;
              prgTblFldCfg[ilTbl][ilFld].KeepEntry = FALSE;
              prgTblFldCfg[ilTbl][ilFld].WarnEntry = FALSE;
              prgTblFldCfg[ilTbl][ilFld].SendField = FALSE;
              prgTblFldCfg[ilTbl][ilFld].SendOnReop = FALSE;
              prgTblFldCfg[ilTbl][ilFld].AutoChange = TRUE;

              prgTblFldCfg[ilTbl][ilFld].CtrlFlagNbr = -1;
              prgTblFldCfg[ilTbl][ilFld].ExplodeField = FALSE;
              prgTblFldCfg[ilTbl][ilFld].ChildGroups = -1;
              prgTblFldCfg[ilTbl][ilFld].GroupLength = -1;
              prgTblFldCfg[ilTbl][ilFld].GroupMembers = -1;
              prgTblFldCfg[ilTbl][ilFld].FirstChild = -1;
              prgTblFldCfg[ilTbl][ilFld].LastChild = -1;
              prgTblFldCfg[ilTbl][ilFld].MainIndex = -1;
              prgTblFldCfg[ilTbl][ilFld].FirstChar = -1;
              prgTblFldCfg[ilTbl][ilFld].LastChar = -1;

            }                   /* end if */
            else
            {
              debug_level = TRACE;
              dbg (TRACE, "ARRAY OVERFLOW: FIELD %d\n<%s>", ilFld, pcgDataArea);
              igConfigError = TRUE;
            }                   /* end else */
          }                     /* end if */
        }                       /* end if */
      }                         /* end while */

      if (ilLoopCount == 1)
      {
        prgPrjTblCfg[ilTbl].TblFldCnt = ilFld;
      }                         /* end if */
      prgPrjTblCfg[ilTbl].AllFldCnt = ilFld;

      /* Look For Internal Fields Not Defined In SysTab */
      sprintf (pclCfgSec, "%s_INTERNALFIELDS", prgPrjTblCfg[ilTbl].TableType);

    }                           /* end for */
    dbg (DEBUG, "FIELD COUNT: TBL = %d ALL = %d", prgPrjTblCfg[ilTbl].TblFldCnt, prgPrjTblCfg[ilTbl].AllFldCnt);
    dbg (DEBUG, "RECORD LENGTH = %d", prgPrjTblCfg[ilTbl].TblRecLen);

    ilRC = InitExplodeFields (ilTbl);
    dbg (DEBUG, "FIELD COUNT: TBL = %d ALL = %d", prgPrjTblCfg[ilTbl].TblFldCnt, prgPrjTblCfg[ilTbl].AllFldCnt);
    dbg (DEBUG, "RECORD LENGTH = %d", prgPrjTblCfg[ilTbl].TblRecLen);

  }                             /* end for */

  if (igConfigError == FALSE)
  {
    ilRC = RC_SUCCESS;
  }                             /* end if */
  else
  {
    ilRC = RC_FAIL;
  }                             /* end else */
  igChkDblFld = FALSE;
  return ilRC;
}                               /* end InitTableFields */

/********************************************************/
/********************************************************/
static int InitExplodeFields (int ipTbl)
{
  int ilRC = RC_SUCCESS;
  int ilFld = 0;
  int ilTbl = 0;
  int ilExpDet = 0;
  int ilExpItm = 0;
  int ilExpCnt = 0;
  int ilExpGrp = 0;
  int ilGrpLen = 0;
  int ilRowCnt = 0;
  int ilFldIdx = 0;
  int ilTblFldIdx = 0;
  int ilCurLen = 0;
  int ilFldLen = 0;
  int ilBgnPos = 0;
  int ilEndPos = 0;
  int ilTmpVal;
  char pclTmpBuf[32];
  char pclFldCod[32];
  char pclFldNam[32];
  char pclTblFldNam[32];
  char pclCfgSec[32];
  char pclCfgDet[32];
  char pclCfgDetCod[32];
  char pclExpCod[32];
  char pclCfgRow[512];
  char pclExpRow[512];

  ilTbl = ipTbl;
  ilFld = prgPrjTblCfg[ilTbl].AllFldCnt;

  /* Look For Explode Fields Not Defined In SysTab */
  sprintf (pclCfgSec, "%s_EXPLODEFIELDS", prgPrjTblCfg[ilTbl].TableType);
  dbg (DEBUG, "GET CONFIG SECTION <%s>", pclCfgSec);
  strcpy (pclCfgDet, "EXPLODE_DETAILS");
  ilRC = iGetConfigRow (pcgCfgFile, pclCfgSec, pclCfgDet, CFG_STRING, pclExpRow);
  if (ilRC == RC_SUCCESS)
  {
    dbg (DEBUG, "FOUND EXPLODE DETAILS <%s>", pclExpRow);
    ilExpDet = field_count (pclExpRow);
    for (ilExpItm = 1; ilExpItm <= ilExpDet; ilExpItm++)
    {
      (void) get_real_item (pclExpCod, pclExpRow, ilExpItm);
      sprintf (pclCfgDet, "EXPLODE_%s", pclExpCod);
      pclCfgRow[0] = 0x00;
      ilRC = iGetConfigRow (pcgCfgFile, pclCfgSec, pclCfgDet, CFG_STRING, pclCfgRow);
      if (ilRC == RC_SUCCESS)
      {
        dbg (DEBUG, "FOUND EXPLODE FIELD <%s>", pclCfgRow);
        (void) get_real_item (pclTblFldNam, pclCfgRow, 1);
        (void) get_real_item (pclTmpBuf, pclCfgRow, 2);
        ilExpCnt = atoi (pclTmpBuf);
        (void) get_real_item (pclTmpBuf, pclCfgRow, 3);
        ilGrpLen = atoi (pclTmpBuf);
        (void) get_real_item (pclCfgDetCod, pclCfgRow, 4);
        ilTblFldIdx = GetFieldIndex (ilTbl, pclTblFldNam);
        if (ilTblFldIdx > 0)
        {
          prgTblFldCfg[ilTbl][ilTblFldIdx].ExplodeField = TRUE;
          prgTblFldCfg[ilTbl][ilTblFldIdx].ChildGroups = ilExpCnt;
          prgTblFldCfg[ilTbl][ilTblFldIdx].GroupLength = ilGrpLen;
          prgTblFldCfg[ilTbl][ilTblFldIdx].FirstChild = ilFld + 1;
          ilRowCnt = 0;
          ilCurLen = 0;
          while (ilRC == RC_SUCCESS)
          {
            ilRowCnt++;
            sprintf (pclCfgDet, "%s_%d", pclCfgDetCod, ilRowCnt);

            ilRC = iGetConfigRow (pcgCfgFile, pclCfgSec, pclCfgDet, CFG_STRING, pclCfgRow);
            if (ilRC == RC_SUCCESS)
            {
              (void) get_real_item (pclFldCod, pclCfgRow, 1);
              (void) get_real_item (pclTmpBuf, pclCfgRow, 3);
              ilBgnPos = ilCurLen;
              ilFldLen = atoi (pclTmpBuf);
              ilCurLen += ilFldLen;
              ilEndPos = ilCurLen - 1;
              for (ilExpGrp = 1; ilExpGrp <= ilExpCnt; ilExpGrp++)
              {
                sprintf (pclFldNam, "%s%d", pclFldCod, ilExpGrp);
                ilFldIdx = GetFieldIndex (ilTbl, pclFldNam);
                if (ilFldIdx < 1)
                {
                  ilFld++;
                  prgPrjTblCfg[ilTbl].AllFldCnt = ilFld;
                  if (ilFld < MAX_FLD_HDL)
                  {
                    /* "URNO" */
                    strcpy (prgTblFldCfg[ilTbl][ilFld].SysTabUrno, "-1");

                    /* "FINA" */
                    strcpy (prgTblFldCfg[ilTbl][ilFld].SysTabFina, pclFldNam);
                    dbg (DEBUG, "ADD SUBFIELD (%d) <%s> <%s> CHAR %d - %d", ilFld, prgPrjTblCfg[ilTbl].FullName,
                         prgTblFldCfg[ilTbl][ilFld].SysTabFina, ilBgnPos, ilEndPos);

                    /* "ADDI" */

                    (void) get_real_item (prgTblFldCfg[ilTbl][ilFld].SysTabAddi, pclCfgRow, 2);

                    /* "FELE" */

                    (void) get_real_item (prgTblFldCfg[ilTbl][ilFld].SysTabFele, pclCfgRow, 3);

                    /* "TYPE" */

                    (void) get_real_item (prgTblFldCfg[ilTbl][ilFld].SysTabType, pclCfgRow, 4);

                    /* "DEFA" */

                    (void) get_real_item (prgTblFldCfg[ilTbl][ilFld].SysTabDefa, pclCfgRow, 5);

                    /* "SYST" */

                    (void) get_real_item (prgTblFldCfg[ilTbl][ilFld].SysTabSyst, pclCfgRow, 6);

                    /* "CONT" */

                    (void) get_real_item (prgTblFldCfg[ilTbl][ilFld].SysTabCont, pclCfgRow, 7);

                    /* Evaluate Internal Values */

                    /* "FELE" -> FieldLength */
                    ilTmpVal = atoi (prgTblFldCfg[ilTbl][ilFld].SysTabFele);
                    prgTblFldCfg[ilTbl][ilFld].FieldLength = ilTmpVal;
                    prgPrjTblCfg[ilTbl].TblRecLen += ilTmpVal;

                    /* "TYPE" -> Internal Field Type */
                    prgTblFldCfg[ilTbl][ilFld].FieldType = 0;
                    ilRC =
                      SetInternalFieldType (prgTblFldCfg[ilTbl][ilFld].SysTabType,
                                            &prgTblFldCfg[ilTbl][ilFld].FieldType);

                    /* Reset Rest of Properties */
                    prgTblFldCfg[ilTbl][ilFld].ReadField = FALSE;
                    prgTblFldCfg[ilTbl][ilFld].ReadOnly = FALSE;
                    prgTblFldCfg[ilTbl][ilFld].KeepEntry = FALSE;
                    prgTblFldCfg[ilTbl][ilFld].WarnEntry = FALSE;
                    prgTblFldCfg[ilTbl][ilFld].SendField = FALSE;
                    prgTblFldCfg[ilTbl][ilFld].SendOnReop = FALSE;
                    prgTblFldCfg[ilTbl][ilFld].AutoChange = TRUE;

                    prgTblFldCfg[ilTbl][ilFld].MainIndex = ilTblFldIdx;

                    prgTblFldCfg[ilTbl][ilFld].CtrlFlagNbr = -1;
                    prgTblFldCfg[ilTbl][ilFld].ExplodeField = FALSE;
                    prgTblFldCfg[ilTbl][ilFld].ChildGroups = ilExpGrp;
                    prgTblFldCfg[ilTbl][ilFld].GroupLength = ilGrpLen;
                    prgTblFldCfg[ilTbl][ilFld].GroupMembers = ilRowCnt;
                    prgTblFldCfg[ilTbl][ilFld].FirstChild = -1;
                    prgTblFldCfg[ilTbl][ilFld].LastChild = -1;
                    prgTblFldCfg[ilTbl][ilFld].FirstChar = ilBgnPos;
                    prgTblFldCfg[ilTbl][ilFld].LastChar = ilEndPos;

                    ilBgnPos += ilGrpLen;
                    ilEndPos += ilGrpLen;

                  }             /* end if */
                  else
                  {
                    debug_level = TRACE;
                    dbg (TRACE, "ARRAY OVERFLOW: FIELD %d\n<%s>", ilFld, pcgDataArea);
                    igConfigError = TRUE;
                  }             /* end else */
                }               /* end if */
                else
                {
                  dbg (TRACE, "DUPLICATE FIELD NAME <%s>", pclFldNam);
                }               /* end else */
              }                 /* end for */
            }                   /* end if */
          }                     /* end while */
          prgTblFldCfg[ilTbl][ilTblFldIdx].LastChild = ilFld;
          prgTblFldCfg[ilTbl][ilTblFldIdx].GroupMembers = ilRowCnt - 1;
          dbg (DEBUG, "EXPLODE GRP=%d MMB=%d LEN=%d FST=%d LST=%d", prgTblFldCfg[ilTbl][ilTblFldIdx].ChildGroups,
               prgTblFldCfg[ilTbl][ilTblFldIdx].GroupMembers, prgTblFldCfg[ilTbl][ilTblFldIdx].GroupLength,
               prgTblFldCfg[ilTbl][ilTblFldIdx].FirstChild, prgTblFldCfg[ilTbl][ilTblFldIdx].LastChild);
        }                       /* end if */
      }                         /* end if */
    }                           /* end for */
  }                             /* end if */
  return ilRC;
}                               /* end InitExplodeFields */

/* ******************************************************************** */
/* ******************************************************************** */
static int ExplodeFieldData (int ipTbl, int ipMainIdx, char *pcpMainFld)
{
  int ilRC = RC_SUCCESS;
  int ilMainIdx = 0;
  int ilFirstField = 0;
  int ilLastField = 0;
  int ilFld = 0;
  int ilRec = 0;
  int ilRecCnt = 0;
  int ilChrPos = 0;
  int ilBgnPos = 0;
  int ilEndPos = 0;
  ilMainIdx = ipMainIdx;
  if (ilMainIdx < 1)
  {
    ilMainIdx = GetFieldIndex (ipTbl, pcpMainFld);
  }                             /* end if */
  if (ilMainIdx > 0)
  {
    ilRecCnt = prgPrjTblCfg[ipTbl].RecHdlCnt;
    ilFirstField = prgTblFldCfg[ipTbl][ilMainIdx].FirstChild;
    ilLastField = prgTblFldCfg[ipTbl][ilMainIdx].LastChild;
    for (ilRec = 1; ilRec <= ilRecCnt; ilRec++)
    {
      /* if (prgRecFldDat[ipTbl][ilRec][ilMainIdx].RcvFlg == TRUE) */
      if (prgRecFldDat[ipTbl][ilRec][ilMainIdx].NewLen > 0)
      {
        for (ilFld = ilFirstField; ilFld <= ilLastField; ilFld++)
        {
          ilBgnPos = prgTblFldCfg[ipTbl][ilFld].FirstChar;
          ilEndPos = prgTblFldCfg[ipTbl][ilFld].LastChar;
          if (ilEndPos <= prgRecFldDat[ipTbl][ilRec][ilMainIdx].NewLen)
          {
            ilChrPos = 0;
            StrgPutStrg (prgRecFldDat[ipTbl][ilRec][ilFld].NewDat, &ilChrPos,
                         prgRecFldDat[ipTbl][ilRec][ilMainIdx].NewDat, ilBgnPos, ilEndPos, "\0");
            prgRecFldDat[ipTbl][ilRec][ilFld].NewDat[ilChrPos] = 0x00;
            (void) CheckFieldValue (prgTblFldCfg[ipTbl][ilFld].FieldType, prgTblFldCfg[ipTbl][ilFld].FieldLength, FALSE,
                                    prgRecFldDat[ipTbl][ilRec][ilFld].NewDat,
                                    &prgRecFldDat[ipTbl][ilRec][ilFld].NewVal);
            prgRecFldDat[ipTbl][ilRec][ilFld].NewFlg = TRUE;
            strcpy (prgRecFldDat[ipTbl][ilRec][ilFld].RcvDat, prgRecFldDat[ipTbl][ilRec][ilFld].NewDat);
            prgRecFldDat[ipTbl][ilRec][ilFld].RcvVal = prgRecFldDat[ipTbl][ilRec][ilFld].NewVal;
            prgRecFldDat[ipTbl][ilRec][ilFld].RcvFlg = TRUE;
            if (prgRecFldDat[ipTbl][ilRec][ilFld].NewDat[0] != ' ')
            {
              prgRecFldDat[ipTbl][ilRec][ilFld].NewLen = strlen (prgRecFldDat[ipTbl][ilRec][ilFld].NewDat);
            }                   /* end if */

          }                     /* end if */
        }                       /* end for */
      }                         /* end if */
    }                           /* end for */
  }                             /* end if */
  return ilRC;
}                               /* end ExplodeFieldData */

/* ******************************************************************** */
/* ******************************************************************** */
static int ImplodeFieldData (int ipTbl, int ipMainIdx, char *pcpMainFld)
{
  int ilRC = RC_SUCCESS;
  int ilMainIdx = 0;
  int ilFirstField = 0;
  int ilLastField = 0;
  int ilFld = 0;
  int ilRec = 0;
  int ilRecCnt = 0;
  int ilBgnPos = 0;
  int ilEndPos = 0;
  int ilMaxPos = 0;
  ilMainIdx = ipMainIdx;
  if (ilMainIdx < 1)
  {
    ilMainIdx = GetFieldIndex (ipTbl, pcpMainFld);
  }                             /* end if */
  if (ilMainIdx > 0)
  {
    /* dbg(TRACE,"NOW IMPLODING <%s>", prgPrjTblCfg[ipTbl].FullName); */
    ilRecCnt = prgPrjTblCfg[ipTbl].RecHdlCnt;
    ilFirstField = prgTblFldCfg[ipTbl][ilMainIdx].FirstChild;
    ilLastField = prgTblFldCfg[ipTbl][ilMainIdx].LastChild;
    for (ilRec = 1; ilRec <= ilRecCnt; ilRec++)
    {
      if (prgRecFldDat[ipTbl][ilRec][ilMainIdx].NewFlg == TRUE)
      {
        ilMaxPos = prgTblFldCfg[ipTbl][ilMainIdx].FieldLength;
        memset (pcgTmpBuf1, ' ', ilMaxPos);
        pcgTmpBuf1[ilMaxPos] = 0x00;
        for (ilFld = ilFirstField; ilFld <= ilLastField; ilFld++)
        {
          ilBgnPos = prgTblFldCfg[ipTbl][ilFld].FirstChar;
          ilEndPos = prgTblFldCfg[ipTbl][ilFld].LastChar;
          if (ilEndPos <= ilMaxPos)
          {
            StrgPutStrg (pcgTmpBuf1, &ilBgnPos, prgRecFldDat[ipTbl][ilRec][ilFld].NewDat, 0, -1, "\0");
          }                     /* end if */
        }                       /* end for */
        str_trm_rgt (pcgTmpBuf1, " ", TRUE);
        (void) SetNewValues (ipTbl, ilRec, ilMainIdx, "", pcgTmpBuf1);
      }                         /* end if */
    }                           /* end for */
  }                             /* end if */
  return ilRC;
}                               /* end ImplodeFieldData */

/* ******************************************************************** */
/* ******************************************************************** */
static int GetSysTabVal (char *pcpFldBuf, int ipMaxSize, int ipFldNbr)
{
  char pclTmpBuf[4096];
  (void) get_real_item (pclTmpBuf, pcgDataArea, ipFldNbr);
  pclTmpBuf[ipMaxSize] = 0x00;
  strcpy (pcpFldBuf, pclTmpBuf);
  return RC_SUCCESS;
}                               /* end GetSysTabVal */

/* ******************************************************************** */
/* change NUL-Separator in an databuffer (form an Item-List)   */
/* ******************************************************************** */
static int BuildItemBuffer (char *pcpData, char *pcpFieldList, int ipNoOfFields, char *pcpSepChr)
{
  int ilChrPos = 0;
  if (ipNoOfFields < 1)
  {
    ipNoOfFields = field_count (pcpFieldList);
  }                             /* end if */
  ipNoOfFields--;
  while (ipNoOfFields > 0)
  {
    if (pcpData[ilChrPos] == 0x00)
    {
      pcpData[ilChrPos] = pcpSepChr[0];
      ipNoOfFields--;
    }                           /* end if */
    ilChrPos++;
  }                             /* end while */
  while (pcpData[ilChrPos] != 0x00)
  {
    ilChrPos++;
  }                             /* end while */
  return ilChrPos;
}                               /* end of BuildItemBuffer */

/********************************************************/
/********************************************************/
static int InitProjectTables (char *pcpActPrj, char *pcpActTab, char *pcpActExt, char *pcpTblTyp, int ipMaxRecNbr)
{
  int ilRC = RC_SUCCESS;
  int ilMaxRecCnt = 0;
  igHandledTables++;
  ilMaxRecCnt = ipMaxRecNbr;
  if (ilMaxRecCnt > MAX_REC_HDL)
  {
    ilMaxRecCnt = MAX_REC_HDL;
    dbg (TRACE, "MAX REC OVERFLOW (%d) TBL <%s> REDUCED TO (%d)", ipMaxRecNbr, pcpActTab, ilMaxRecCnt);
  }                             /* end if */
  dbg (DEBUG, "INIT TABLE %d NAM <%s> EXT <%s> TYP <%s> REC (%d)", igHandledTables, pcpActTab, pcpActExt, pcpTblTyp,
       ilMaxRecCnt);
  if (igHandledTables <= MAX_TBL_HDL)
  {
    strcpy (prgPrjTblCfg[igHandledTables].Project, pcpActPrj);
    strcpy (prgPrjTblCfg[igHandledTables].TableExt, pcpActExt);
    sprintf (prgPrjTblCfg[igHandledTables].FullName, "%s%s", pcpActTab, pcpActExt);
    strcpy (prgPrjTblCfg[igHandledTables].ShortName, pcpActTab);
    strcpy (prgPrjTblCfg[igHandledTables].TableType, pcpTblTyp);
    prgPrjTblCfg[igHandledTables].TypeOfTable = GetTableType (pcpTblTyp);
    prgPrjTblCfg[igHandledTables].RecHdlMax = ilMaxRecCnt;
    prgPrjTblCfg[igHandledTables].RecHdlCnt = 0;
    prgPrjTblCfg[igHandledTables].TblRecLen = 0;
    prgPrjTblCfg[igHandledTables].TblFldCnt = 0;
    prgPrjTblCfg[igHandledTables].AllFldCnt = 0;
    prgPrjTblCfg[igHandledTables].TblRulCnt = 0;
    prgPrjTblCfg[igHandledTables].TblCflCnt = 0;
  }                             /* end if */
  else
  {
    debug_level = TRACE;
    dbg (TRACE, "ARRAY OVERFLOW: TABLE %d <%s>", igHandledTables, pcpActTab);
    igConfigError = TRUE;
    ilRC = RC_FAIL;
  }                             /* end else */
  return ilRC;
}                               /* end InitProjectTables */

/********************************************************/
/********************************************************/
static int SetInternalFieldType (char *pcpAsciiType, int *ipInternType)
{
  int ilRC = RC_SUCCESS;
  int ilTypNbr = 0;
  char pclValidTypes[] =
    "TRIM,CHAR,DATE,TIME,NMBR,LONG,REAL,CURR,LIST,URNO," "FKEY,FURN,FREL,LSTU,USER,WORK,CDAT,CUSR,IMPO,GRPU";
  if (strlen (pcpAsciiType) == 4)
  {
    ilTypNbr = get_item_no (pclValidTypes, pcpAsciiType, 5);
    ilTypNbr++;
  }                             /* end if */
  if (ilTypNbr > 0)
  {
    *ipInternType = ilTypNbr;
  }                             /* end if */
  return ilRC;
}                               /* end SetInternalFieldType */

/********************************************************/
/********************************************************/
static int InitDataBuffer (void)
{
  int ilRC = RC_SUCCESS;
  int ilTblRecLen = 0;
  int ilLenInKilo;
  int ilTbl;
  int ilRec;
  int ilFld;
  int ilMaxRecords;
  int ilLastField;
  int ilFldLen;
  if (igConfigError == FALSE)
  {
    igTotalBufferSize = 0;

    for (ilTbl = 1; ilTbl <= igHandledTables; ilTbl++)
    {
      if ((prgPrjTblCfg[ilTbl].TypeOfTable > 0) && (prgPrjTblCfg[ilTbl].TypeOfTable < 90))
      {
        ilTblRecLen = 0;

        ilTblRecLen += prgPrjTblCfg[ilTbl].TblRecLen; /* Record Length */
        ilTblRecLen += prgPrjTblCfg[ilTbl].AllFldCnt; /* Delimiters */
        ilTblRecLen *= 3;       /* RCV/DBS/NEW DataHandle */
        /* ilTblRecLen *= MAX_REC_HDL; */
        ilTblRecLen *= prgPrjTblCfg[ilTbl].RecHdlMax;

        /* Add One Record For RCV Buffers in FieldTable */
        ilTblRecLen += prgPrjTblCfg[ilTbl].TblRecLen; /* Record Length */
        ilTblRecLen += prgPrjTblCfg[ilTbl].AllFldCnt; /* Delimiters */

        /* Add 5 FieldList Buffers Per Table */
        ilTblRecLen += MAX_FLDLST_SIZE + 1; /* All Fields */
        ilTblRecLen += MAX_FLDLST_SIZE + 1; /* Act Fields */
        ilTblRecLen += MAX_FLDLST_SIZE + 1; /* Sql Fields */
        ilTblRecLen += MAX_FLDLST_SIZE + 1; /* Out Fields */
        ilTblRecLen += MAX_FLDLST_SIZE + 1; /* Rms Fields */

        /* Add 1 Selection Buffer Per Table (OutSelKey) */
        ilTblRecLen += MAX_SELKEY_SIZE + 1;

        /* Add three DataBlock Buffers Per Table */
        ilTblRecLen += MAX_DATBLK_SIZE + 1;
        ilTblRecLen += MAX_DATBLK_SIZE + 1;
        ilTblRecLen += MAX_DATBLK_SIZE + 1;

        igTotalBufferSize += ilTblRecLen;
      }                         /* end if */
    }                           /* end for */

    dbg (TRACE, "=====================================");
    dbg (TRACE, "TOTAL BUFFER SIZE NEEDED = %d", igTotalBufferSize);
    ilLenInKilo = igTotalBufferSize / 1024;
    ilLenInKilo++;
    igTotalBufferSize = ilLenInKilo * 1024;
    pcgGlobalDataBuffer = (char *) malloc (igTotalBufferSize);
    if (pcgGlobalDataBuffer == NULL)
    {
      dbg (TRACE, "ERROR: ALLOCATING GLOBAL DATA BUFFER OF %d BYTES.", igTotalBufferSize);
      igConfigError = TRUE;
      ilRC = RC_FAIL;
    }                           /* end if */
    else
    {
      dbg (TRACE, "TOTAL BYTES (ROUNDED) ALLOCATED: %d (%d KB)", igTotalBufferSize, ilLenInKilo);
    }                           /* end else */

    if (igConfigError == FALSE)
    {
      pcgLastDataPos = pcgGlobalDataBuffer;
      for (ilTbl = 1; ilTbl <= igHandledTables; ilTbl++)
      {
        if ((prgPrjTblCfg[ilTbl].TypeOfTable > 0) && (prgPrjTblCfg[ilTbl].TypeOfTable < 90))
        {
          ilLastField = prgPrjTblCfg[ilTbl].AllFldCnt;
          for (ilFld = 1; ilFld <= ilLastField; ilFld++)
          {
            ilFldLen = prgTblFldCfg[ilTbl][ilFld].FieldLength;

            prgTblFldCfg[ilTbl][ilFld].RcvDat = pcgLastDataPos;
            pcgLastDataPos += ilFldLen + 1;

            /* ilMaxRecords = MAX_REC_HDL; */
            ilMaxRecords = prgPrjTblCfg[ilTbl].RecHdlMax;
            for (ilRec = 1; ilRec <= ilMaxRecords; ilRec++)
            {
              prgRecFldDat[ilTbl][ilRec][ilFld].RcvDat = pcgLastDataPos;
              pcgLastDataPos += ilFldLen + 1;

              prgRecFldDat[ilTbl][ilRec][ilFld].DbsDat = pcgLastDataPos;
              pcgLastDataPos += ilFldLen + 1;

              prgRecFldDat[ilTbl][ilRec][ilFld].NewDat = pcgLastDataPos;
              pcgLastDataPos += ilFldLen + 1;

            }                   /* end for */
          }                     /* end for */

          prgPrjTblCfg[ilTbl].AllFldLst = pcgLastDataPos;
          pcgLastDataPos += MAX_FLDLST_SIZE + 1;
          prgPrjTblCfg[ilTbl].ActFldLst = pcgLastDataPos;
          pcgLastDataPos += MAX_FLDLST_SIZE + 1;
          prgPrjTblCfg[ilTbl].SqlFldLst = pcgLastDataPos;
          pcgLastDataPos += MAX_FLDLST_SIZE + 1;
          prgPrjTblCfg[ilTbl].OutFldLst = pcgLastDataPos;
          pcgLastDataPos += MAX_FLDLST_SIZE + 1;
          prgPrjTblCfg[ilTbl].RmsFldLst = pcgLastDataPos;
          pcgLastDataPos += MAX_FLDLST_SIZE + 1;

          prgPrjTblCfg[ilTbl].OutSelKey = pcgLastDataPos;
          pcgLastDataPos += MAX_SELKEY_SIZE + 1;

          prgPrjTblCfg[ilTbl].SqlDatBlk = pcgLastDataPos;
          pcgLastDataPos += MAX_DATBLK_SIZE + 1;
          prgPrjTblCfg[ilTbl].OutDatBlk = pcgLastDataPos;
          pcgLastDataPos += MAX_DATBLK_SIZE + 1;
          prgPrjTblCfg[ilTbl].OldDatBlk = pcgLastDataPos;
          pcgLastDataPos += MAX_DATBLK_SIZE + 1;

          ilRC = ResetDataBuffer (__LINE__ ,TRUE, ilTbl, ilTbl, 1, ilMaxRecords, 1, ilLastField);
        }                       /* end if */
      }                         /* end for */

    }                           /* end if */

    ilFldLen = pcgLastDataPos - pcgGlobalDataBuffer;
    dbg (DEBUG, "LAST DATA POS = %d", ilFldLen);
    dbg (DEBUG, "=====================================");
  }                             /* end if */
  return ilRC;
}                               /* end InitDataBuffer */

/********************************************************/
/********************************************************/
static int ResetDataBuffer (long lpLineNo ,int ipResetAll, int ipFirstTbl, int ipLastTbl, 
                            int ipFirstRec, int ipLastRec,
                            int ipFirstFld, int ipLastFld)
{
  int ilRC = RC_SUCCESS;
  int ilTbl;
  int ilFstTbl;
  int ilLstTbl;
  int ilFld;
  int ilFstFld;
  int ilLstFld;
  int ilRec;
  int ilFstRec;
  int ilLstRec;

  TRACE_FIELD_OF_INTEREST(lpLineNo,"ResetDataBuffer A");

  /* Checking Parameters */
  ilFstTbl = ipFirstTbl;
  ilLstTbl = ipLastTbl;
  if (ilFstTbl < 1)
  {
    ilFstTbl = 1;
  }                             /* end if */
  if (ilLstTbl < 1)
  {
    ilLstTbl = igHandledTables;
  }                             /* end if */

  for (ilTbl = ilFstTbl; ilTbl <= ilLstTbl; ilTbl++)
  {
    if ((prgPrjTblCfg[ilTbl].TypeOfTable > 0) && (prgPrjTblCfg[ilTbl].TypeOfTable < 90))
    {
      if (ipResetAll == TRUE)
      {
        prgPrjTblCfg[ilTbl].RcvFlg = FALSE;
        prgPrjTblCfg[ilTbl].DbsFlg = FALSE;
        prgPrjTblCfg[ilTbl].NewFlg = FALSE;

        prgPrjTblCfg[ilTbl].ActFldLst[0] = 0x00;
        prgPrjTblCfg[ilTbl].SqlFldLst[0] = 0x00;
        prgPrjTblCfg[ilTbl].OutFldLst[0] = 0x00;
        prgPrjTblCfg[ilTbl].ActFldLen = 0;
        prgPrjTblCfg[ilTbl].SqlFldLen = 0;
        prgPrjTblCfg[ilTbl].OutFldLen = 0;

        prgPrjTblCfg[ilTbl].OutSelKey[0] = 0x00;
        prgPrjTblCfg[ilTbl].OutSelLen = 0;

        prgPrjTblCfg[ilTbl].SqlDatBlk[0] = 0x00;
        prgPrjTblCfg[ilTbl].OutDatBlk[0] = 0x00;
        prgPrjTblCfg[ilTbl].OldDatBlk[0] = 0x00;
        prgPrjTblCfg[ilTbl].SqlDatLen = 0;
        prgPrjTblCfg[ilTbl].OutDatLen = 0;
      }                         /* end if */

      /* Checking Parameters */
      ilFstFld = ipFirstFld;
      ilLstFld = ipLastFld;
      if (ilFstFld < 1)
      {
        ilFstFld = 1;
      }                         /* end if */
      if (ilLstFld < 1)
      {
        ilLstFld = prgPrjTblCfg[ilTbl].AllFldCnt;
      }                         /* end if */

      for (ilFld = ilFstFld; ilFld <= ilLstFld; ilFld++)
      {
        if (ipResetAll == TRUE)
        {
          strcpy (prgTblFldCfg[ilTbl][ilFld].RcvDat, " ");
          prgTblFldCfg[ilTbl][ilFld].RcvLen = 0;
          prgTblFldCfg[ilTbl][ilFld].RcvVal = 0;
          prgTblFldCfg[ilTbl][ilFld].RcvFlg = FALSE;
        }                       /* end if */

        /* Checking Parameters */
        ilFstRec = ipFirstRec;
        ilLstRec = ipLastRec;
        if (ilFstRec < 1)
        {
          ilFstRec = 1;
        }                       /* end if */
        if (ilLstRec < 1)
        {
          ilLstRec = prgPrjTblCfg[ilTbl].RecHdlCnt;
        }                       /* end if */

        for (ilRec = ilFstRec; ilRec <= ilLstRec; ilRec++)
        {
          strcpy (prgRecFldDat[ilTbl][ilRec][ilFld].RcvDat, " ");
          prgRecFldDat[ilTbl][ilRec][ilFld].RcvLen = 0;
          prgRecFldDat[ilTbl][ilRec][ilFld].RcvVal = 0;
          prgRecFldDat[ilTbl][ilRec][ilFld].RcvFlg = FALSE;

          strcpy (prgRecFldDat[ilTbl][ilRec][ilFld].DbsDat, " ");
          prgRecFldDat[ilTbl][ilRec][ilFld].DbsLen = 0;
          prgRecFldDat[ilTbl][ilRec][ilFld].DbsVal = 0;
          prgRecFldDat[ilTbl][ilRec][ilFld].DbsFlg = FALSE;

          strcpy (prgRecFldDat[ilTbl][ilRec][ilFld].NewDat, " ");
          prgRecFldDat[ilTbl][ilRec][ilFld].NewLen = 0;
          prgRecFldDat[ilTbl][ilRec][ilFld].NewVal = 0;
          prgRecFldDat[ilTbl][ilRec][ilFld].NewFlg = FALSE;
        }                       /* end for */
      }                         /* end for */

      if (ipResetAll == TRUE)
      {
        prgPrjTblCfg[ilTbl].RecHdlCnt = 0;
      }                         /* end if */
    }                           /* end if */

  }                             /* end for */

  TRACE_FIELD_OF_INTEREST(lpLineNo,"ResetDataBuffer E");

  return ilRC;
}                               /* end ResetDataBuffer */

/********************************************************/
/********************************************************/
static int GetTableIndex (char *pcpTableName)
{
  int ilTblNbr = -1;
  int ilTbl = -1;
  for (ilTblNbr = 1; ((ilTblNbr <= igHandledTables) && (ilTbl < 1)); ilTblNbr++)
  {
    if (strcmp (prgPrjTblCfg[ilTblNbr].FullName, pcpTableName) == 0)
    {
      ilTbl = ilTblNbr;
    }                           /* end if */
  }                             /* end for */
  return ilTbl;
}                               /* end GetTableIndex */

/********************************************************/
/********************************************************/
static void CheckHomePort (void)
{
  int ilTblNbr = -1;
  int ilHomLen = 0;
  int ilExtLen = 0;
  char pclHome[8];
  char pclExts[8];
  char pclTmp[8];

  if (strcmp (pcgDefTblExt, "TAB") == 0)
  {
    /* First Check Table Extension */
    /* Because We Need It Later */
    ilExtLen = get_real_item (pclExts, pcgTwEnd, 2);
    if (ilExtLen != 3)
    {
      strcpy (pclExts, pcgDefTblExt);
    }                           /* end if */

/* QuickHack */
    strcpy (pclExts, pcgDefTblExt);

    if (strcmp (pclExts, pcgTblExt) != 0)
    {
      strcpy (pcgTblExt, pclExts);
      strcpy (pcgBasExt, pclExts);
      for (ilTblNbr = 1; ilTblNbr <= igHandledTables; ilTblNbr++)
      {
        strcpy (prgPrjTblCfg[ilTblNbr].TableExt, pcgTblExt);
        sprintf (prgPrjTblCfg[ilTblNbr].FullName, "%s%s", prgPrjTblCfg[ilTblNbr].ShortName,
                 prgPrjTblCfg[ilTblNbr].TableExt);
      }                         /* end for */
      strcpy (&pcgAftTab[3], pcgTblExt);
      strcpy (&pcgArcTab[3], pcgTblExt);
      strcpy (&pcgCcaTab[3], pcgTblExt);
      dbg (TRACE, "EXTENSIONS SET TO <%s> <%s>", pcgTblExt, pcgBasExt);
      dbg (TRACE, "TABLES <%s> <%s> <%s>", pcgAftTab, pcgArcTab, pcgCcaTab);
    }                           /* end if */

    /* Now Check HomePort */
    ilHomLen = get_real_item (pclHome, pcgTwEnd, 1);
    if (ilHomLen != 3)
    {
      strcpy (pclHome, pcgDefH3LC);
    }                           /* end if */
    if (strcmp (pclHome, pcgH3LC) != 0)
    {
      strcpy (pcgH3LC, pclHome);
      sprintf (pclTmp, "APT%s", pcgBasExt);
      (void) GetBasicData (pclTmp, "APC3", pcgH3LC, "APC4", pcgH4LC, 1, RC_FAIL);
      dbg (TRACE, "HOMEPORT SET TO <%s> <%s>", pcgH3LC, pcgH4LC);
    }                           /* end if */
  }                             /* end if */

  return;
}                               /* end CheckHomePort */

/********************************************************/
/********************************************************/
static int GetField_C_Index (long lpLineNo, int ipTblIdx, char *pcpFieldName)
/* static int GetFieldIndex (int ipTblIdx, char *pcpFieldName) */
{
  int ilFldNbr = -1;
  int ilMaxNbr = -1;
  int ilFld = -1;

  ilMaxNbr = prgPrjTblCfg[ipTblIdx].AllFldCnt;
  for (ilFldNbr = 1; ((ilFldNbr <= ilMaxNbr) && (ilFld < 1)); ilFldNbr++)
  {
    if (strncmp (prgTblFldCfg[ipTblIdx][ilFldNbr].SysTabFina, pcpFieldName, 4) == 0)
    {
      ilFld = ilFldNbr;
    }                           /* end if */
  }                             /* end for */
  if ((igChkDblFld == FALSE) && (ilFld < 1))
  {
    dbg (DEBUG, "Line %ld, WARNING: FIELD <%s> <%s> NOT FOUND", lpLineNo, 
         prgPrjTblCfg[ipTblIdx].FullName, pcpFieldName);
/*    dbg (DEBUG, "WARNING: FIELD <%s> <%s> NOT FOUND",prgPrjTblCfg[ipTblIdx].FullName, pcpFieldName);
*/
  }                             /* end if */

  return ilFld;
}                               /* end GetFieldIndex */

/********************************************************/
/********************************************************/
static int GetAsciiConfig (void)
{
  int ilRC = RC_SUCCESS;
  int ilCfgItmCnt;
  int ilCfgItm;
  int ilItmLen;
  char pclCfgSec[64];
  char pclCfgDet[64];
  char pclCfgRow[64];
  char pclCfgLst[1024];

  /* USED TO READ RULES ONLY !!! */

  strcpy (pclCfgSec, "PROCESSCONFIG");
  strcpy (pclCfgDet, "CONFIGDEFINES");

  /* Point to First RuleBuffer */
  pcgLastRulePos = pcgGlobalRuleBuffer;

  (void) iGetConfigRow (pcgCfgFile, pclCfgSec, pclCfgDet, CFG_STRING, pclCfgLst);
  ilCfgItmCnt = field_count (pclCfgLst);
  dbg (DEBUG, "CFG DETAILS= <%s> ITEMS: (%d)", pclCfgLst, ilCfgItmCnt);

  for (ilCfgItm = 1; ilCfgItm <= ilCfgItmCnt; ilCfgItm++)
  {
    ilItmLen = get_real_item (pclCfgRow, pclCfgLst, ilCfgItm);
    str_trm_all (pclCfgRow, " \t", TRUE);
    ilItmLen = strlen (pclCfgRow);
    if (ilItmLen > 0)
    {
      (void) GetCfgTasks (pclCfgSec, pclCfgRow, FOR_RULE);
    }                           /* end if */
  }                             /* end for */

  ilRC = InitRuleFields ();

  dbg (DEBUG, "=====================================================");

  strcpy(pclCfgSec,"CONFLICT_HANDLING");
  strcpy(pclCfgDet,"CONFLICTDEFINES");

  pcgLastCnflPos = pcgGlobalCnflBuffer;
  pclCfgLst[0] = 0x00;
  iGetConfigRow(pcgCfgFile,pclCfgSec,pclCfgDet,CFG_STRING,pclCfgLst);
  ilCfgItmCnt = field_count(pclCfgLst);
  dbg(DEBUG,"CFL DETAILS= <%s> ITEMS: (%d)",pclCfgLst,ilCfgItmCnt);

  for (ilCfgItm = 1; ilCfgItm <= ilCfgItmCnt; ilCfgItm++)
  {
    ilItmLen = get_real_item(pclCfgRow,pclCfgLst,ilCfgItm);
    str_trm_all(pclCfgRow," \t",TRUE);
    ilItmLen = strlen(pclCfgRow);
    if (ilItmLen > 0)
    {
      (void)GetCfgTasks(pclCfgSec,pclCfgRow, FOR_CNFL);
    }
  }

  /* ilRC = InitRuleFields(); */
  igAftTab = GetTableIndex (pcgAftTab);
  SetReadAlways (igAftTab, pcgLogSmmFields);

  return ilRC;
}                               /* end GetAsciiConfig */

/********************************************************/
/********************************************************/
static int InitSourceFlagCtrl (void)
{
  int ilRC = RC_SUCCESS;
  int ilTbl = 0;
  int ilFldCnt = 0;
  int ilFldIdx = 0;
  int ilFld = 0;
  char pclFldNam[8];
  char pclDssfFldLst[1024];
  pclDssfFldLst[0] = 0x00;
  ilRC = GetCfg ("SOURCEFLAG_CTRL", "DSSF_FLDLST", CFG_STRING, pclDssfFldLst, "\0");
  if (ilRC == RC_SUCCESS)
  {
    ilFldCnt = field_count (pclDssfFldLst);
    dbg (DEBUG, "DSSF CTRL %d FIELDS <%s>", ilFldCnt, pclDssfFldLst);
    if (ilFldCnt > 0)
    {
      for (ilTbl = 1; ilTbl <= igHandledTables; ilTbl++)
      {
        if (prgPrjTblCfg[ilTbl].TypeOfTable == 1)
        {                       /* AFT */
          ilRC = SetReadAlways (ilTbl, "DSSF");
          for (ilFld = 1; ilFld <= ilFldCnt; ilFld++)
          {
            (void) get_real_item (pclFldNam, pclDssfFldLst, ilFld);
            ilFldIdx = GetFieldIndex (ilTbl, pclFldNam);
            if (ilFldIdx > 0)
            {
              prgTblFldCfg[ilTbl][ilFldIdx].CtrlFlagNbr = ilFld - 1;
              /*
                 dbg(TRACE,"DSSF IDX %d <%s> SET TO %d",ilFldIdx,pclFldNam,
                 prgTblFldCfg[ilTbl][ilFldIdx].CtrlFlagNbr);
               */
            }                   /* end if */
          }                     /* end for */
        }                       /* end if */
      }                         /* end for */
    }                           /* end if */
  }                             /* end if */
  ilRC = RC_SUCCESS;
  return ilRC;
}                               /* InitSourceFlagCtrl */

/********************************************************/
/********************************************************/
static int GetCfgTasks (char *pcpCfgSec, char *pcpCfgRow, int ipForWhat)
{
  int ilRC = RC_SUCCESS;
  int ilTskItmCnt;
  int ilTskItm;
  int ilItmLen;
  int ilLen1;
  int ilLen2;
  int ilLen3;
  char pclTskLst[1024];
  char pclTskTbl[64];
  char pclTskSec[64];
  char pclNamLst[1024];
  char pclTskNam[64];
  char pclRulDef[1024];
  char pclRulTyp[64];
  char pclRulDst[512];
  char pclRulSrc[512];
  /*
     dbg(DEBUG,"CFG SECTION <%s>",pcpCfgSec);
     dbg(DEBUG,"CFG ROW     <%s>",pcpCfgRow);
   */
  pclTskLst[0] = 0x00;
  (void) iGetConfigRow (pcgCfgFile, pcpCfgSec, pcpCfgRow, CFG_STRING, pclTskLst);
  (void) GetDataParts (pclTskTbl, pclTskSec, pclNamLst, pclTskLst, ":", "=", &ilLen1, &ilLen2, &ilLen3);
  /*
     dbg(TRACE,"TASK TABLES  <%s>",pclTskTbl);
     dbg(TRACE,"TASK SECTION <%s>",pclTskSec);
     dbg(TRACE,"TASK NAMES   <%s>",pclNamLst);
   */
  if (ilLen2 == 0)
  {
    strcpy (pclTskSec, pcpCfgSec);
  }                             /* end if */
  /* dbg(DEBUG,"USED SECTION <%s>",pclTskSec); */
  ilTskItmCnt = field_count (pclNamLst);

  for (ilTskItm = 1; ilTskItm <= ilTskItmCnt; ilTskItm++)
  {
    ilItmLen = get_real_item (pclTskNam, pclNamLst, ilTskItm);
    str_trm_all (pclTskNam, " \t", TRUE);
    ilItmLen = strlen (pclTskNam);
    if (ilItmLen > 0)
    {
      pclRulDef[0] = 0x00;
      (void) iGetConfigRow (pcgCfgFile, pclTskSec, pclTskNam, CFG_STRING, pclRulDef);
      /* dbg(TRACE,"RULE <%s>",pclRulDef); */
      (void) GetDataParts (pclRulTyp, pclRulDst, pclRulSrc, pclRulDef, ":", "=", &ilLen1, &ilLen2, &ilLen3);
      switch (ipForWhat)
      {
        case FOR_RULE:
          dbg (DEBUG, "RULE TYPE <%s>", pclRulTyp);
          dbg (DEBUG, "RULE DEST <%s>", pclRulDst);
          dbg (DEBUG, "RULE SRC  <%s>", pclRulSrc);
          (void) StoreTableRule (pclTskTbl, pclRulTyp, pclRulDst, pclRulSrc);
          break;
        case FOR_CNFL:
          dbg (DEBUG, "CONFLICT TYPE <%s>", pclRulTyp);
          dbg (DEBUG, "CONFLICT DEST <%s>", pclRulDst);
          dbg (DEBUG, "CONFLICT SRC  <%s>", pclRulSrc);
          (void) StoreTableCnfl (pclTskTbl, pclRulTyp, pclRulDst, pclRulSrc);
          break;
        default:
          break;
      }                         /* end switch */
    }                           /* end if */
  }                             /* end for */
  return ilRC;
}                               /* end GetCfgTasks */

/********************************************************/
/********************************************************/
static int GetFieldProperties (int ipTbl)
{
  int ilRC = RC_SUCCESS;
  int ilFldTyp;
  int ilFldCnt;
  int ilFld;
  int ilItmCnt;
  int ilItm;
  int ilItmLen;
  char pclTmpBuf[2048];
  char pclFldCfg[2048];
  char pclFldRow[8];
  char pclCfgSec[64];
  sprintf (pclCfgSec, "%s_FIELDCONFIG", prgPrjTblCfg[ipTbl].TableType);
  ilFldCnt = prgPrjTblCfg[ipTbl].AllFldCnt;
  dbg (DEBUG, "GET PROPERTIES <%s> OF (%d) FIELDS", pclCfgSec, ilFldCnt);
  for (ilFld = 1; ilFld <= ilFldCnt; ilFld++)
  {
    sprintf (pclFldRow, "<%s>", prgTblFldCfg[ipTbl][ilFld].SysTabFina);
    pclFldCfg[0] = 0x00;
    (void) iGetConfigRow (pcgCfgFile, pclCfgSec, pclFldRow, CFG_STRING, pclFldCfg);
    /* dbg(TRACE,"FLD_CFG %s = <%s>",pclFldRow,pclFldCfg); */
    ilItmCnt = field_count (pclFldCfg);
    ilItm = 1;
    while (ilItm <= ilItmCnt)
    {
      ilItmLen = get_real_item (pclTmpBuf, pclFldCfg, ilItm);
      if (ilItmLen > 0)
      {
        if (strcmp (pclTmpBuf, "READ") == 0)
        {
          ilRC = SetReadAlways (ipTbl, prgTblFldCfg[ipTbl][ilFld].SysTabFina);
        }
        else if (strcmp (pclTmpBuf, "NOIN") == 0)
        {
          dbg (DEBUG, "NOIN FIELD <%s> <%s>", prgPrjTblCfg[ipTbl].ShortName, prgTblFldCfg[ipTbl][ilFld].SysTabFina);
          prgTblFldCfg[ipTbl][ilFld].ReadOnly = TRUE;
        }
        else if (strcmp (pclTmpBuf, "WARN") == 0)
        {
          prgTblFldCfg[ipTbl][ilFld].WarnEntry = TRUE;
          ilItm++;
          ilItmLen = get_real_item (prgTblFldCfg[ipTbl][ilFld].CflWarnTkey, pclFldCfg, ilItm);
          if (ilItmLen <= 0)
          {
            strcpy (prgTblFldCfg[ipTbl][ilFld].CflWarnTkey, "CF09");
          }                     /* end if */
          dbg (DEBUG, "WARN FIELD <%s> <%s> <%s>", prgPrjTblCfg[ipTbl].ShortName,                prgTblFldCfg[ipTbl][ilFld].SysTabFina,
               prgTblFldCfg[ipTbl][ilFld].CflWarnTkey);
        }
        else if (strcmp (pclTmpBuf, "KEEP") == 0)
        {
          dbg (DEBUG, "KEEP FIELD <%s> <%s>", prgPrjTblCfg[ipTbl].ShortName, prgTblFldCfg[ipTbl][ilFld].SysTabFina);
          prgTblFldCfg[ipTbl][ilFld].KeepEntry = TRUE;
        }
        else if (strcmp (pclTmpBuf, "SEND") == 0)
        {
          ilRC = SetSendAlways (ipTbl, prgTblFldCfg[ipTbl][ilFld].SysTabFina);
        }
        else if (strcmp (pclTmpBuf, "REOP") == 0)
        {
          dbg (DEBUG, "REOP FIELD <%s> <%s>", prgPrjTblCfg[ipTbl].ShortName, prgTblFldCfg[ipTbl][ilFld].SysTabFina);
          ilRC = SetSendOnReop (ipTbl, prgTblFldCfg[ipTbl][ilFld].SysTabFina);
        }
        else if (strcmp (pclTmpBuf, "FIXT") == 0)
        {
          prgTblFldCfg[ipTbl][ilFld].AutoChange = FALSE;
          dbg (DEBUG, "FIXT FIELD <%s> <%s>", prgPrjTblCfg[ipTbl].ShortName, prgTblFldCfg[ipTbl][ilFld].SysTabFina);
        }
        else
        {
          ilFldTyp = 0;
          ilRC = SetInternalFieldType (pclTmpBuf, &ilFldTyp);
          if (ilFldTyp > 0)
          {
            prgTblFldCfg[ipTbl][ilFld].FieldType = ilFldTyp;
          }                     /* end if */
        }                       /* end else */
      }                         /* end if */
      ilItm++;
    }                           /* end while */
  }                             /* end if */
  ilRC = RC_SUCCESS;
  return ilRC;
}                               /* end GetFieldProperties */

/********************************************************/
/********************************************************/
static int StoreTableRule (char *pcpTskTbl, char *pcpRulTyp, char *pcpRulDst, char *pcpRulSrc)
{
  int ilRC = RC_SUCCESS;
  int ilTblNbr;
  int ilTblIdx;
  int ilTblCnt;
  int ilRulIdx;
  int ilNamLen;
  int ilRuleTable = 0;
  int ilStoreIdx = 0;
  char pclTblNam[16];
  char pclRulTbl[16];
  ilTblCnt = field_count (pcpTskTbl);
  for (ilTblNbr = 1; ilTblNbr <= ilTblCnt; ilTblNbr++)
  {
    ilNamLen = get_real_item (pclTblNam, pcpTskTbl, ilTblNbr);
    if (ilNamLen > 0)
    {
      pclTblNam[3] = 0x00;
      sprintf (pclRulTbl, "%s%s", pclTblNam, pcgTblExt);
      dbg (DEBUG, "STORING RULE <%s> <%s>", pclRulTbl, pcpRulTyp);
      ilTblIdx = GetTableIndex (pclRulTbl);
      if (ilTblIdx > 0)
      {
        if (ilStoreIdx == 0)
        {
          ilRulIdx = prgPrjTblCfg[ilTblIdx].TblRulCnt + 1;
          prgTblRulCfg[ilTblIdx][ilRulIdx].RuleType = GetRuleType (pcpRulTyp);
          prgTblRulCfg[ilTblIdx][ilRulIdx].RuleTarget = pcgLastRulePos;
          strcpy (prgTblRulCfg[ilTblIdx][ilRulIdx].RuleTarget, pcpRulDst);
          pcgLastRulePos += strlen (pcpRulDst) + 1;
          prgTblRulCfg[ilTblIdx][ilRulIdx].RuleSource = pcgLastRulePos;
          strcpy (prgTblRulCfg[ilTblIdx][ilRulIdx].RuleSource, pcpRulSrc);
          pcgLastRulePos += strlen (pcpRulSrc) + 1;
          prgPrjTblCfg[ilTblIdx].TblRulCnt = ilRulIdx;
          ilRuleTable = ilTblIdx;
          ilStoreIdx = ilRulIdx;
          if (prgTblRulCfg[ilTblIdx][ilRulIdx].RuleType == 1)
          {                     /*FIRST_VALUE */
            SetReadOnly (ilTblIdx, pcpRulDst);
          }                     /* end if */
        }                       /* end if */
        else
        {
          prgPrjTblCfg[ilTblIdx].TblRulCnt = ilStoreIdx;
          prgTblRulCfg[ilTblIdx][ilStoreIdx].RuleType = prgTblRulCfg[ilRuleTable][ilStoreIdx].RuleType;
          prgTblRulCfg[ilTblIdx][ilStoreIdx].RuleTarget = prgTblRulCfg[ilRuleTable][ilStoreIdx].RuleTarget;
          prgTblRulCfg[ilTblIdx][ilStoreIdx].RuleSource = prgTblRulCfg[ilRuleTable][ilStoreIdx].RuleSource;
          if (prgTblRulCfg[ilTblIdx][ilStoreIdx].RuleType == 1)
          {                     /*FIRST_VALUE */
            SetReadOnly (ilTblIdx, pcpRulDst);
          }                     /* end if */
        }                       /* end else */
      }                         /* end if */
    }                           /* end if */
  }                             /* end for */
  return ilRC;
}                               /* end StoreTableRule */

/********************************************************/
/********************************************************/
static int StoreTableCnfl (char *pcpTskTbl, char *pcpCflTyp, char *pcpCflDst, char *pcpCflSrc)
{
  int ilRC = RC_SUCCESS;
  int ilTblNbr;
  int ilTblIdx;
  int ilTblCnt;
  int ilCflIdx;
  int ilNamLen;
  int ilCnflTable = 0;
  int ilStoreIdx = 0;
  char pclTblNam[16];
  char pclCflTbl[16];

  ilTblCnt = field_count (pcpTskTbl);
  for (ilTblNbr = 1; ilTblNbr <= ilTblCnt; ilTblNbr++)
  {
    ilNamLen = get_real_item (pclTblNam, pcpTskTbl, ilTblNbr);
    if (ilNamLen > 0)
    {
      pclTblNam[3] = 0x00;
      sprintf (pclCflTbl, "%s%s", pclTblNam, pcgTblExt);

      ilTblIdx = GetTableIndex (pclCflTbl);
      if (ilTblIdx > 0)
      {
        if (ilStoreIdx == 0)
        {
          ilCflIdx = prgPrjTblCfg[ilTblIdx].TblCflCnt + 1;
          prgTblCflCfg[ilTblIdx][ilCflIdx].CnflType = GetCnflType (pcpCflTyp);
          prgTblCflCfg[ilTblIdx][ilCflIdx].CnflTarget = pcgLastCnflPos;
          strcpy (prgTblCflCfg[ilTblIdx][ilCflIdx].CnflTarget, pcpCflDst);
          pcgLastCnflPos += strlen (pcpCflDst) + 1;
          prgTblCflCfg[ilTblIdx][ilCflIdx].CnflSource = pcgLastCnflPos;
          strcpy (prgTblCflCfg[ilTblIdx][ilCflIdx].CnflSource, pcpCflSrc);
          pcgLastCnflPos += strlen (pcpCflSrc) + 1;
          prgPrjTblCfg[ilTblIdx].TblCflCnt = ilCflIdx;
          ilCnflTable = ilTblIdx;
          ilStoreIdx = ilCflIdx;
        }                       /* end if */
        else
        {
          prgPrjTblCfg[ilTblIdx].TblCflCnt = ilStoreIdx;
          prgTblCflCfg[ilTblIdx][ilStoreIdx].CnflType = prgTblCflCfg[ilCnflTable][ilStoreIdx].CnflType;
          prgTblCflCfg[ilTblIdx][ilStoreIdx].CnflTarget = prgTblCflCfg[ilCnflTable][ilStoreIdx].CnflTarget;
          prgTblCflCfg[ilTblIdx][ilStoreIdx].CnflSource = prgTblCflCfg[ilCnflTable][ilStoreIdx].CnflSource;
        }                       /* end else */
        SetReadAlways (ilTblIdx, prgTblCflCfg[ilTblIdx][ilStoreIdx].CnflTarget);
        SetReadAlways (ilTblIdx, prgTblCflCfg[ilTblIdx][ilStoreIdx].CnflSource);
      }                         /* end if */
    }                           /* end if */
  }                             /* end for */
  return ilRC;
}                               /* end StoreTableCnfl */

/********************************************************/
/********************************************************/
static int GetTableType (char *pcpTypeStrg)
{
  int ilTblTyp = 0;
  if (strcmp (pcpTypeStrg, "AFT") == 0)
  {
    ilTblTyp = 1;
  }
  else if (strcmp (pcpTypeStrg, "CKI") == 0)
  {
    ilTblTyp = 2;
  }
  else if (strcmp (pcpTypeStrg, "BAS") == 0)
  {
    ilTblTyp = 99;
  }                             /* end if else if */
  else
  {
    ilTblTyp = -1;
  }                             /* end else */
  return ilTblTyp;
}                               /* end GetTableType */

/********************************************************/
/********************************************************/
static int GetRuleType (char *pcpTypeStrg)
{
  int ilRulTyp = 0;
  if (strcmp (pcpTypeStrg, "FIRST_VALUE") == 0)
  {
    ilRulTyp = 1;
  }
  else if (strcmp (pcpTypeStrg, "BASIC_DATA") == 0)
  {
    ilRulTyp = 2;
  }
  else if (strcmp (pcpTypeStrg, "CHECK_VALUE") == 0)
  {
    ilRulTyp = 3;
  }
  else if (strcmp (pcpTypeStrg, "CALCULATE") == 0)
  {
    ilRulTyp = 4;
  }
  else if (strcmp (pcpTypeStrg, "CHANGE_INFO") == 0)
  {
    ilRulTyp = 5;
  }
  else if (strcmp (pcpTypeStrg, "CROSS_CHECK") == 0)
  {
    ilRulTyp = 6;
  }
  else if (strcmp (pcpTypeStrg, "MULTI_INPUT") == 0)
  {
    ilRulTyp = 7;
  }
  else if (strcmp (pcpTypeStrg, "AUTO_FILL") == 0)
  {
    ilRulTyp = 8;
  }
  else if (strcmp (pcpTypeStrg, "DAY_CHECK") == 0)
  {
    ilRulTyp = 9;
  }
  else if (strcmp (pcpTypeStrg, "BASIC_LOOKUP") == 0)
  {
    ilRulTyp = 10;
  }
  else if (strcmp (pcpTypeStrg, "BASIC_VALUE") == 0)
  {
    ilRulTyp = 11;
  }                             /* end else if */
  else
  {
    ilRulTyp = -1;
    dbg (TRACE, "UNKNOWN RULE TYPE <%s> DETECTED", pcpTypeStrg);
  }                             /* end else */
  return ilRulTyp;
}                               /* end GetRuleType */

/********************************************************/
/********************************************************/
static int GetCnflType (char *pcpTypeStrg)
{
  int ilCflTyp = 0;
  if (strcmp (pcpTypeStrg, "NEW_VALUES") == 0)
  {
    ilCflTyp = 1;
  }
  else if (strcmp (pcpTypeStrg, "CHG_VALUES") == 0)
  {
    ilCflTyp = 2;
  }
  else if (strcmp (pcpTypeStrg, "DEL_VALUES") == 0)
  {
    ilCflTyp = 3;
  }
  else if (strcmp (pcpTypeStrg, "ANY_CHANGE") == 0)
  {
    ilCflTyp = 4;
  }
  else if (strcmp (pcpTypeStrg, "MIX_FIELDS") == 0)
  {
    ilCflTyp = 5;
  }
  else if (strcmp (pcpTypeStrg, "CMP_VALUES") == 0)
  {
    ilCflTyp = 6;
         /*** MCU 20.11.2000 new conflict type inserted **/
  }
  else if (strcmp (pcpTypeStrg, "CF") == 0)
  {
    ilCflTyp = 7;
  }
  else if (strcmp (pcpTypeStrg, "CF") == 0)
  {
    ilCflTyp = 8;
  }
  else if (strcmp (pcpTypeStrg, "CF") == 0)
  {
    ilCflTyp = 9;
  }
  else if (strcmp (pcpTypeStrg, "CF") == 0)
  {
    ilCflTyp = 10;
  }                             /* end else if */
  else
  {
    ilCflTyp = -1;
    dbg (TRACE, "UNKNOWN CONFLICT TYPE <%s> DETECTED", pcpTypeStrg);
  }                             /* end else */
  return ilCflTyp;
}                               /* end GetCnflType */

/********************************************************/
/********************************************************/
static int InitRuleFields (void)
{
  int ilRC = RC_SUCCESS;
  int ilTbl = -1;
  int ilRul = -1;
  int ilRulCnt = -1;
  for (ilTbl = 1; ilTbl <= igHandledTables; ilTbl++)
  {
    if ((prgPrjTblCfg[ilTbl].TypeOfTable > 0) && (prgPrjTblCfg[ilTbl].TypeOfTable < 90))
    {
      dbg (DEBUG, "STORE RULES OF TABLE <%s>", prgPrjTblCfg[ilTbl].FullName);
      ilRulCnt = prgPrjTblCfg[ilTbl].TblRulCnt;
      for (ilRul = 1; ilRul <= ilRulCnt; ilRul++)
      {
        /*
           dbg(DEBUG,"TYP (%d) RULE <%s> = <%s>",
           prgTblRulCfg[ilTbl][ilRul].RuleType,
           prgTblRulCfg[ilTbl][ilRul].RuleTarget,
           prgTblRulCfg[ilTbl][ilRul].RuleSource);
         */
        ilRC = SetReadAlways (ilTbl, prgTblRulCfg[ilTbl][ilRul].RuleTarget);
        ilRC = SetReadAlways (ilTbl, prgTblRulCfg[ilTbl][ilRul].RuleSource);
      }                         /* end for */
      ilRC = GetFieldProperties (ilTbl);
    }                           /* end if */
  }                             /* end for */
  return ilRC;
}                               /* end InitRuleFields */

/********************************************************/
/********************************************************/
static int SetSendToRulFnd (int ipLogFlg, char *pcpTable, char *pcpFldLst)
{
  int ilRC = RC_SUCCESS;
  int ilTbl = 0;
  int ilFldIdx = 0;
  int ilFldCnt = 0;
  int ilFldNbr = 0;
  int ilNamLen = 0;
  char pclFldNam[8];
  ilFldCnt = field_count (pcpFldLst);
  dbg (TRACE, "GOT %d RMS FIELDS FOR <%s>", ilFldCnt, pcpTable);
  if (ipLogFlg != 0)
  {
    dbg (TRACE, "\n%s", pcpFldLst);
  }                             /* end if */
  ilTbl = GetTableIndex (pcpTable);
  if (ilTbl > 0)
  {
    ilFldCnt = prgPrjTblCfg[ilTbl].TblFldCnt;
    for (ilFldIdx = 1; ilFldIdx <= ilFldCnt; ilFldIdx++)
    {
      prgTblFldCfg[ilTbl][ilFldIdx].SendRulFnd = FALSE;
    }                           /* end for */
    ilFldCnt = field_count (pcpFldLst);
    for (ilFldNbr = 1; ilFldNbr <= ilFldCnt; ilFldNbr++)
    {
      ilNamLen = get_real_item (pclFldNam, pcpFldLst, ilFldNbr);
      if (ilNamLen == 4)
      {
        ilFldIdx = GetFieldIndex (ilTbl, pclFldNam);
        if (ilFldIdx > 0)
        {
          prgTblFldCfg[ilTbl][ilFldIdx].SendRulFnd = TRUE;
          dbg (DEBUG, "<%s.%s> FLAGGED AS RMS FIELD", prgPrjTblCfg[ilTbl].ShortName, pclFldNam);
        }                       /* end if */
        else
        {
          dbg (TRACE, "UNKNOWN FIELD <%s>", pclFldNam);
        }                       /* end else */
      }                         /* end if */
      else
      {
        dbg (TRACE, "WRONG FIELDNAME <%s>", pclFldNam);
      }                         /* end else */
    }                           /* end for */
    prgPrjTblCfg[ilTbl].RmsFldCnt = ilFldCnt;
    strcpy (prgPrjTblCfg[ilTbl].RmsFldLst, pcpFldLst);
  }                             /* end if */
  else
  {
    dbg (TRACE, "UNKNOWN TABLE <%s>", pcpTable);
  }                             /* end else */
  return ilRC;
}                               /* end SetSendToRulFnd */

/********************************************************/
/********************************************************/
static int SetReadAlways (int ipTbl, char *pcpFldLst)
{
  int ilRC = RC_SUCCESS;
  int ilFldIdx = 0;
  int ilFldCnt = 0;
  int ilFldNbr = 0;
  int ilNamLen = 0;
/*  TWE 14012000  (Configfile error with fieldname "Registration" */
/*
  char pclFldNam[8];
*/
  char pclFldNam[24];

  ilFldCnt = field_count (pcpFldLst);
  for (ilFldNbr = 1; ilFldNbr <= ilFldCnt; ilFldNbr++)
  {
    ilNamLen = get_real_item (pclFldNam, pcpFldLst, ilFldNbr);
    if (ilNamLen == 5)
    {
      pclFldNam[4] = 0x00;
      ilNamLen = 4;
    }                           /* end if */

    if (ilNamLen == 4)
    {
      ilFldIdx = GetFieldIndex (ipTbl, pclFldNam);
      if ((ilFldIdx > 0) && (ilFldIdx <= prgPrjTblCfg[ipTbl].TblFldCnt) &&
          (prgTblFldCfg[ipTbl][ilFldIdx].ReadField != TRUE))
      {
        prgTblFldCfg[ipTbl][ilFldIdx].ReadField = TRUE;
        dbg (DEBUG, "READ ALWAYS FIELD <%s> <%s>", prgPrjTblCfg[ipTbl].FullName, pclFldNam);
      }                         /* end if */
    }                           /* end if */
  }                             /* end for */
  return ilRC;
}                               /* end SetReadAlways */

/********************************************************/
/********************************************************/
static int SetReadOnly (int ipTbl, char *pcpFldLst)
{
  int ilRC = RC_SUCCESS;
  int ilFldIdx = 0;
  int ilFldCnt = 0;
  int ilFldNbr = 0;
  int ilNamLen = 0;
  char pclFldNam[8];
  ilFldCnt = field_count (pcpFldLst);
  for (ilFldNbr = 1; ilFldNbr <= ilFldCnt; ilFldNbr++)
  {
    ilNamLen = get_real_item (pclFldNam, pcpFldLst, ilFldNbr);
    if (ilNamLen == 4)
    {
      ilFldIdx = GetFieldIndex (ipTbl, pclFldNam);
      if (ilFldIdx > 0)
      {
        prgTblFldCfg[ipTbl][ilFldIdx].ReadOnly = TRUE;
      }                         /* end if */
    }                           /* end if */
  }                             /* end for */
  return ilRC;
}                               /* end SetReadOnly */

/********************************************************/
/********************************************************/
static int SetSendAlways (int ipTbl, char *pcpFldLst)
{
  int ilRC = RC_SUCCESS;
  int ilFldIdx = 0;
  int ilFldCnt = 0;
  int ilFldNbr = 0;
  int ilNamLen = 0;
  char pclFldNam[8];
  ilFldCnt = field_count (pcpFldLst);
  for (ilFldNbr = 1; ilFldNbr <= ilFldCnt; ilFldNbr++)
  {
    ilNamLen = get_real_item (pclFldNam, pcpFldLst, ilFldNbr);
    if (ilNamLen == 4)
    {
      ilFldIdx = GetFieldIndex (ipTbl, pclFldNam);
      if (ilFldIdx > 0)
      {
        prgTblFldCfg[ipTbl][ilFldIdx].SendField = TRUE;
      }                         /* end if */
    }                           /* end if */
  }                             /* end for */
  return ilRC;
}                               /* end SetSendAlways */

/********************************************************/
/********************************************************/
static int SetSendOnReop (int ipTbl, char *pcpFldLst)
{
  int ilRC = RC_SUCCESS;
  int ilFldIdx = 0;
  int ilFldCnt = 0;
  int ilFldNbr = 0;
  int ilNamLen = 0;
  char pclFldNam[8];
  ilFldCnt = field_count (pcpFldLst);
  for (ilFldNbr = 1; ilFldNbr <= ilFldCnt; ilFldNbr++)
  {
    ilNamLen = get_real_item (pclFldNam, pcpFldLst, ilFldNbr);
    if (ilNamLen == 4)
    {
      ilFldIdx = GetFieldIndex (ipTbl, pclFldNam);
      if (ilFldIdx > 0)
      {
        prgTblFldCfg[ipTbl][ilFldIdx].SendOnReop = TRUE;
      }                         /* end if */
    }                           /* end if */
  }                             /* end for */
  return ilRC;
}                               /* end SetSendOnReop */

/********************************************************/
/********************************************************/
static int GetDataParts (char *pcpPart1, char *pcpPart2, char *pcpPart3, char *pcpSource, char *pcpSep1, char *pcpSep2,
                         int *ipLen1, int *ipLen2, int *ipLen3)
{
  int ilRC = RC_SUCCESS;
  char *pclSepPtr1;
  char *pclSepPtr2;
  pcpPart2[0] = 0x00;
  pcpPart3[0] = 0x00;

  pclSepPtr1 = strstr (pcpSource, pcpSep1);
  if (pclSepPtr1 != NULL)
  {
    *pclSepPtr1 = 0x00;
    strcpy (pcpPart1, pcpSource);
    pclSepPtr1++;
  }                             /* end if */
  else
  {
    pcpPart1[0] = 0x00;
    pclSepPtr1 = pcpSource;
  }                             /* end else */

  pclSepPtr2 = strstr (pclSepPtr1, pcpSep2);
  if (pclSepPtr2 != NULL)
  {
    *pclSepPtr2 = 0x00;
    pclSepPtr2++;
    strcpy (pcpPart3, pclSepPtr2);
  }                             /* end if */

  strcpy (pcpPart2, pclSepPtr1);

  str_trm_all (pcpPart1, " ", TRUE);
  str_trm_all (pcpPart2, " ", TRUE);
  str_trm_all (pcpPart3, " ", TRUE);

  *ipLen1 = strlen (pcpPart1);
  *ipLen2 = strlen (pcpPart2);
  *ipLen3 = strlen (pcpPart3);

  return ilRC;
}                               /* end of GetDataParts */

/********************************************************/
/********************************************************/
static int GetReceivedData (char *pcpTable, char *pcpFields, char *pcpData)
{
  int ilRC = RC_SUCCESS;
  int ilTbl;
  int ilFldCnt;
  int ilFld;
  int ilItmNbr;
  int ilItmLen;
  int ilFtyp = 0;
  char pclFldNam[16];
  igReopFlight = FALSE;
  ilTbl = GetTableIndex (pcpTable);
  if (ilTbl > 0)
  {
    prgPrjTblCfg[ilTbl].RcvFlg = TRUE;
    ilFldCnt = field_count (pcpFields);
    for (ilItmNbr = 1; ilItmNbr <= ilFldCnt; ilItmNbr++)
    {
      ilItmLen = get_real_item (pclFldNam, pcpFields, ilItmNbr);
      if (ilItmLen > 0)
      {
        if (igClientAction != TRUE)
        {
          CheckFieldNameMapping (pclFldNam);
        }                       /* end if */
        ilFld = GetFieldIndex (ilTbl, pclFldNam);
        if (ilFld > 0)
        {
          if (prgTblFldCfg[ilTbl][ilFld].ReadOnly != TRUE)
          {
            ilItmLen = get_real_item (pcgTmpBuf1, pcpData, ilItmNbr);
            if (ilItmLen <= prgTblFldCfg[ilTbl][ilFld].FieldLength)
            {
              prgTblFldCfg[ilTbl][ilFld].RcvLen = ilItmLen;
              strcpy (prgTblFldCfg[ilTbl][ilFld].RcvDat, pcgTmpBuf1);
              if (prgTblFldCfg[ilTbl][ilFld].RcvLen < 1)
              {
                strcpy (prgTblFldCfg[ilTbl][ilFld].RcvDat, prgTblFldCfg[ilTbl][ilFld].SysTabDefa);
                prgTblFldCfg[ilTbl][ilFld].RcvLen = strlen (prgTblFldCfg[ilTbl][ilFld].RcvDat);
                if (prgTblFldCfg[ilTbl][ilFld].RcvLen < 1)
                {
                  strcpy (prgTblFldCfg[ilTbl][ilFld].RcvDat, " ");
                }               /* end if */
              }                 /* end if */
              (void) CheckFieldValue (prgTblFldCfg[ilTbl][ilFld].FieldType, prgTblFldCfg[ilTbl][ilFld].FieldLength,
                                      TRUE, prgTblFldCfg[ilTbl][ilFld].RcvDat, &prgTblFldCfg[ilTbl][ilFld].RcvVal);
              prgTblFldCfg[ilTbl][ilFld].RcvFlg = TRUE;
            }                   /* end if */
            else
            {
              dbg (TRACE, "RECEIVED VALUE <%s> TOO LARGE", pclFldNam);
            }                   /* end else */
          }                     /* end if */
          else
          {
            get_real_item (pcgTmpBuf1, pcpData, ilItmNbr);
            dbg (DEBUG, "READONLY <%s> REJECTED <%s>", pclFldNam, pcgTmpBuf1);
          }                     /* end else */
        }                       /* end if */
      }                         /* end if */
    }                           /* end for */
  }                             /* end if */
  else
  {
    dbg (TRACE, "UNKNOWN TABLE <%s>", pcpTable);
  }                             /* end else */

  ilFtyp = GetFieldIndex (ilTbl, "FTYP");
  if (ilFtyp > 0)
  {
    if ((prgTblFldCfg[ilTbl][ilFtyp].RcvFlg == TRUE) && (prgTblFldCfg[ilTbl][ilFtyp].RcvDat[0] == 'O'))
    {
      igReopFlight = TRUE;
    }                           /* end if */
  }                             /* end if */

  return ilRC;
}                               /* end GetReceivedData */

/********************************************************/
/********************************************************/
static int ShowReceivedData (void)
{
  int ilRC = RC_SUCCESS;
  int ilTbl = -1;
  int ilFld = -1;
  int ilFldCnt = -1;
  if (igDebugDetails == TRUE)
  {
    for (ilTbl = 1; ilTbl <= igHandledTables; ilTbl++)
    {
      if (prgPrjTblCfg[ilTbl].RcvFlg == TRUE)
      {
        dbg (TRACE, "============= RECEIVED TABLE DATA ==========");
        dbg (TRACE, "TABLE NAME <%s>", prgPrjTblCfg[ilTbl].FullName);
        ilFldCnt = prgPrjTblCfg[ilTbl].AllFldCnt;
        for (ilFld = 1; ilFld <= ilFldCnt; ilFld++)
        {
          if (prgTblFldCfg[ilTbl][ilFld].RcvFlg == TRUE)
          {
            dbg (TRACE, "FIELD <%s> <%s>", prgTblFldCfg[ilTbl][ilFld].SysTabFina, prgTblFldCfg[ilTbl][ilFld].RcvDat);
          }                     /* end if */
        }                       /* end for */
        dbg (TRACE, "============================================");
      }                         /* end if */
    }                           /* end for */
  }                             /* end if */
  return ilRC;
}                               /* end ShowReceivedData */

/********************************************************/
/********************************************************/
static int StoreReceivedData (int ipFlag, int ipTbl, int ipRec, int ipFldCnt, char *pcpTable, char *pcpFields,
                              char *pcpData)
{
  int ilRC = RC_SUCCESS;
  int ilTbl;
  int ilFldCnt;
  int ilFld;
  int ilItmNbr;
  int ilItmLen;
  char pclFldNam[16];

   TRACE_FIELD_OF_INTEREST(__LINE__,"SRDA");

  dbg(DEBUG,"StoreReceivedData: Fields=%d pcpData=<%s>",ipFldCnt,pcpData);
  ilTbl = ipTbl;
  if (ilTbl < 1)
  {
    ilTbl = GetTableIndex (pcpTable);
  }                             /* end if */
  if (ilTbl > 0)
  {
    /* +++ jim +++ 20001228: in case of Rotation do not set .RecHdlCnt to 1!: */
    if (prgPrjTblCfg[ilTbl].RecHdlCnt < ipRec)
    {
       prgPrjTblCfg[ilTbl].RecHdlCnt = ipRec;
    }
    
    prgPrjTblCfg[ilTbl].RcvFlg = TRUE;
    ilFldCnt = ipFldCnt;
    if (ilFldCnt < 1)
    {
      ilFldCnt = field_count (pcpFields);
    }                           /* end if */
    for (ilItmNbr = 1; ilItmNbr <= ilFldCnt; ilItmNbr++)
    {
      ilItmLen = get_real_item (pclFldNam, pcpFields, ilItmNbr);
      if (ilItmLen > 0)
      {
        /* +++ jim +++ 20000825 */
        dbg (DEBUG, "StoreReceivedData: ilTbl: %d, ipRec: %d Evaluating <%s>",ilTbl ,ipRec ,pclFldNam); 
        ilFld = GetFieldIndex (ilTbl, pclFldNam);
        if (ilFld > 0)
        {
          dbg (DEBUG, "StoreReceivedData: old: <%s>",prgRecFldDat[ilTbl][ipRec][ilFld].RcvDat); 
          if ((prgTblFldCfg[ilTbl][ilFld].ReadOnly != TRUE) || (ipFlag != TRUE))
          {
            prgRecFldDat[ilTbl][ipRec][ilFld].RcvLen =
              get_real_item (prgRecFldDat[ilTbl][ipRec][ilFld].RcvDat, pcpData, ilItmNbr);
            if (prgRecFldDat[ilTbl][ipRec][ilFld].RcvLen < 1)
            {
              strcpy (prgRecFldDat[ilTbl][ipRec][ilFld].RcvDat, " ");
            }                   /* end if */
            (void) CheckFieldValue (prgTblFldCfg[ilTbl][ilFld].FieldType, prgTblFldCfg[ilTbl][ilFld].FieldLength, FALSE,
                                    prgRecFldDat[ilTbl][ipRec][ilFld].RcvDat,
                                    &prgRecFldDat[ilTbl][ipRec][ilFld].RcvVal);
            prgRecFldDat[ilTbl][ipRec][ilFld].RcvFlg = TRUE;
            if (ipFlag != TRUE)
            {
              dbg (DEBUG, "<%s> SET TO <%s>", pclFldNam, prgRecFldDat[ilTbl][ipRec][ilFld].RcvDat);
            }                   /* end if */
          }                     /* end if */
          else
          {
            dbg (DEBUG, "READONLY <%s> REJECTED", pclFldNam);
          }                     /* end else */
          dbg (DEBUG, "StoreReceivedData: new: <%s>",prgRecFldDat[ilTbl][ipRec][ilFld].RcvDat); 
        }                       /* end if */
        else
        {
          dbg (TRACE, "UNKNOWN FIELD <%s>", pclFldNam);
        }                       /* end else */
      }                         /* end if */
    }                           /* end for */
    dbg(DEBUG,"StoreReceivedData: after loop");
  }                             /* end if */
  else
  {
    dbg (TRACE, "UNKNOWN TABLE <%s>", pcpTable);
  }                             /* end else */
  /* ShowReceivedData();  * +++ jim +++ 20000825 */
  /* ShowStoredData();  * +++ jim +++ 20000825 */

   TRACE_FIELD_OF_INTEREST(__LINE__,"SRDE");

   dbg(DEBUG,"StoreReceivedData: end");
  return ilRC;
}                               /* end StoreReceivedData */

/********************************************************/
/********************************************************/
static int CheckReceivedRouting (int ipActionFlag, int ipTbl, int ipRec, char *pcpTable, char *pcpRoutType)
{
  int ilRC = RC_SUCCESS;
  int ilTbl;
  int ilRoutChar = 0;
  int ilOrg3 = 0;
  int ilDes3 = 0;
  int ilVia3 = 0;
  int ilVial = 0;
  /* Function normally called from FSI Data Import */
  if (ipActionFlag == TRUE)
  {
    ilTbl = ipTbl;
    if (ilTbl < 1)
    {
      ilTbl = GetTableIndex (pcpTable);
    }                           /* end if */
    ilVial = GetFieldIndex (ilTbl, "VIAL");
    ilVia3 = GetFieldIndex (ilTbl, "VIA3");
    ilOrg3 = GetFieldIndex (ilTbl, "ORG3");
    ilDes3 = GetFieldIndex (ilTbl, "DES3");
    ilRoutChar = ipRec - 1;

    switch (pcpRoutType[ilRoutChar])
    {
      case 'A':                /* Arrival */
        if (strcmp (prgRecFldDat[ilTbl][ipRec][ilOrg3].RcvDat, pcgH3LC) == 0)
        {
          /* It's an illegal RoundTrip */
          strcpy (prgRecFldDat[ilTbl][ipRec][ilOrg3].RcvDat, prgRecFldDat[ilTbl][ipRec][ilVia3].RcvDat);
          prgRecFldDat[ilTbl][ipRec][ilOrg3].RcvLen = prgRecFldDat[ilTbl][ipRec][ilVia3].RcvLen;

          strcpy (prgRecFldDat[ilTbl][ipRec][ilVia3].RcvDat, " ");
          prgRecFldDat[ilTbl][ipRec][ilVia3].RcvLen = 0;
          prgRecFldDat[ilTbl][ipRec][ilVia3].RcvFlg = FALSE;
        }                       /* end if */
        break;
      case 'D':                /* Departure */
        if (strcmp (prgRecFldDat[ilTbl][ipRec][ilDes3].RcvDat, pcgH3LC) == 0)
        {
          /* It's an illegal RoundTrip */
          strcpy (prgRecFldDat[ilTbl][ipRec][ilDes3].RcvDat, prgRecFldDat[ilTbl][ipRec][ilVia3].RcvDat);
          prgRecFldDat[ilTbl][ipRec][ilDes3].RcvLen = prgRecFldDat[ilTbl][ipRec][ilVia3].RcvLen;

          strcpy (prgRecFldDat[ilTbl][ipRec][ilVia3].RcvDat, " ");
          prgRecFldDat[ilTbl][ipRec][ilVia3].RcvLen = 0;
          prgRecFldDat[ilTbl][ipRec][ilVia3].RcvFlg = FALSE;
        }                       /* end if */
        break;
      default:
        break;
    }                           /* end switch */

    dbg (DEBUG, "VIA3 <%s> LEN (%d)", prgRecFldDat[ilTbl][ipRec][ilVia3].RcvDat,
         prgRecFldDat[ilTbl][ipRec][ilVia3].RcvLen);
    dbg (DEBUG, "VIAL <%s> LEN (%d)", prgRecFldDat[ilTbl][ipRec][ilVial].RcvDat,
         prgRecFldDat[ilTbl][ipRec][ilVial].RcvLen);

    if ((prgRecFldDat[ilTbl][ipRec][ilVia3].RcvLen > 0) && (prgRecFldDat[ilTbl][ipRec][ilVial].RcvLen < 1))
    {
      dbg (DEBUG, "MOVE VIA3 TO VIAL");
      sprintf (prgRecFldDat[ilTbl][ipRec][ilVial].RcvDat, " %s", prgRecFldDat[ilTbl][ipRec][ilVia3].RcvDat);
      prgRecFldDat[ilTbl][ipRec][ilVial].RcvLen = prgRecFldDat[ilTbl][ipRec][ilVia3].RcvLen + 1;
      prgRecFldDat[ilTbl][ipRec][ilVial].RcvFlg = TRUE;
    }                           /* end if */

  }                             /* end if */

  return ilRC;
}                               /* end CheckReceivedRouting */

/********************************************************/
/********************************************************/
static int ShowStoredData (void)
{
  int ilRC = RC_SUCCESS;
  int ilTbl = -1;
  int ilRec = -1;
  int ilRecCnt = -1;
  int ilFld = -1;
  int ilFldCnt = -1;
  for (ilTbl = 1; ilTbl <= igHandledTables; ilTbl++)
  {
    if (prgPrjTblCfg[ilTbl].RcvFlg == TRUE)
    {
      dbg (TRACE, "============= RECEIVED DATA ==========");
      dbg (TRACE, "TABLE NAME <%s>", prgPrjTblCfg[ilTbl].FullName);
      ilRecCnt = prgPrjTblCfg[ilTbl].RecHdlCnt;
      for (ilRec = 1; ilRec <= ilRecCnt; ilRec++)
      {
        dbg (TRACE, "%d. RECORD:", ilRec);
        ilFldCnt = prgPrjTblCfg[ilTbl].AllFldCnt;
        for (ilFld = 1; ilFld <= ilFldCnt; ilFld++)
        {
          if (prgRecFldDat[ilTbl][ilRec][ilFld].RcvFlg == TRUE)
          {
            dbg (TRACE, "FIELD <%s> <%s>", prgTblFldCfg[ilTbl][ilFld].SysTabFina,
                 prgRecFldDat[ilTbl][ilRec][ilFld].RcvDat);
          }                     /* end if */
        }                       /* end for */
      }                         /* end for */
      dbg (TRACE, "============================================");
    }                           /* end if */
  }                             /* end for */
  return ilRC;
}                               /* end ShowStoredData */

/********************************************************/
/********************************************************/
static int SearchFlight (char *pcpTable, char *pcpFields, char *pcpWhere, int ipActionType, int ipFirstCall)
{
  int ilRC = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  int ilChrPos = 0;
  int ilNoOfRec = 0;
  int ilFldCnt = 0;
  int ilTbl = 0;
  int ilTblCnt = 0;
  char pclTmpTable[16];
  char pclTblLst[128];
  char pclRcvSelKey[1024];
  short slFkt = 0;
  short slLocalCursor = 0;

  strcpy (pclRcvSelKey, pcpWhere);
  CheckWhereClause (TRUE, pclRcvSelKey, FALSE, TRUE, "\0");

  if ((ipActionType == FOR_SEARCH) || (ipActionType == FOR_TABLES))
  {
    strcpy (pclTblLst, pcpTable);
    ilTblCnt = field_count (pclTblLst);
    ilTbl = 0;
    ilChrPos = 0;
    while (ilTbl < ilTblCnt)
    {
      ilTbl++;
      (void) get_real_item (pclTmpTable, pclTblLst, ilTbl);
      sprintf (pcgSqlBuf, "SELECT %s FROM %s %s", pcpFields, pclTmpTable, pclRcvSelKey);
      /* dbg(TRACE,"<%s>",pcgSqlBuf); */
      slFkt = START;
      slLocalCursor = 0;
      
      dbg (DEBUG, "SearchFlight: pcgSqlBuf: <%s>",pcgSqlBuf);
      ilGetRc = sql_if (slFkt, &slLocalCursor, pcgSqlBuf, pcgDataArea);
      dbg (DEBUG, "SearchFlight: table <%s>: sql_if returns <%d>",pclTmpTable,ilGetRc);
      

      if (ilGetRc == DB_SUCCESS)
      {
        StrgPutStrg (pcpTable, &ilChrPos, pclTmpTable, 0, -1, ",");
        if (ipActionType == FOR_SEARCH)
        {
          ilTbl = ilTblCnt + 1;
        }                       /* end if */
      }                         /* end if */
      else if (ilGetRc == RC_FAIL)
      {
          dbg (TRACE, "SearchFlight: SQL Buffer<%s>", pcgSqlBuf);
          get_ora_err (ilGetRc, pcgErrMsg);
          dbg (TRACE, "SearchFlight: Oracle Error <%s>", pcgErrMsg);
      }                         /* end if */
      close_my_cursor (&slLocalCursor);
    }                           /* end while */

    if (ilChrPos > 0)
    {
      ilChrPos--;
      pcpTable[ilChrPos] = 0x00;
      ilFldCnt = 0;
      BuildItemBuffer (pcgDataArea, pcpFields, ilFldCnt, ",");
      ilRC = RC_SUCCESS;
    }                           /* end if */
    else
    {
      strcpy (pcpTable, "NOT_FD");
      ilRC = RC_FAIL;
    }                           /* end else */

  }                             /* end if */
  else
  {
    /* FOR_UPDATE */
    ilTbl = GetTableIndex (pcpTable);
    if (ilTbl > 0)
    {
      /*  TWE 17012000  */
      if ((strncmp (pcpTable, "AFT", 3) == 0) &&
          ((igPOPS == TRUE) && (igTWENewFlnoFlight == TRUE)) )
      {
        strcpy (prgPrjTblCfg[ilTbl].ActFldLst, prgPrjTblCfg[ilTbl].AllFldLst);
      }
      else if (ipFirstCall == TRUE)
      {
        ilGetRc = BuildSqlReadFields (ilTbl, "", FOR_NORMAL);
      }                         /* end if */
/*****************/

      sprintf (pcgSqlBuf, "SELECT %s FROM %s %s", prgPrjTblCfg[ilTbl].ActFldLst, pcpTable, pclRcvSelKey);
      ilNoOfRec = 0;
      ilFldCnt = field_count (prgPrjTblCfg[ilTbl].ActFldLst);
      slFkt = START;
      slLocalCursor = 0;
      ilGetRc = DB_SUCCESS;
      
      while (ilGetRc == DB_SUCCESS)
      {
        ilGetRc = sql_if (slFkt, &slLocalCursor, pcgSqlBuf, pcgDataArea);
        if (ilGetRc == DB_SUCCESS)
        {
          ilNoOfRec++;
/*  TWE 17012000  */
          BuildItemBuffer (pcgDataArea, prgPrjTblCfg[ilTbl].ActFldLst, ilFldCnt, ",");
/*********************/
          /* dbg(DEBUG,"%d.: DATA <%s>",ilNoOfRec,pcgDataArea); */
          ilRC = GetRecordData (ilTbl, ilNoOfRec, ilFldCnt, pcpTable, prgPrjTblCfg[ilTbl].ActFldLst, pcgDataArea);
        }                       /* end if */
        else
        {
          if (ilGetRc == RC_FAIL)
          {
            dbg (TRACE, "SearchFlight: SQL Buffer<%s>", pcgSqlBuf);
            get_ora_err (ilGetRc, pcgErrMsg);
            dbg (TRACE, "SEARCH FLIGHT: ORACLE ERROR !!");
            ilRC = RC_FAIL;
          }                     /* end if */
        }                       /* end else */
        slFkt = NEXT;
      }                         /* while */
      close_my_cursor (&slLocalCursor);

      

      if (ilNoOfRec > 1)
      {
        dbg (TRACE, "SEARCH FLIGHT: FETCHED <%d> RECORDS", ilNoOfRec);
        dbg (TRACE, "============================================");
      }                         /* end if */
      if (ilNoOfRec < 1)
      {
        /* dbg(TRACE,"FLIGHT NOT FOUND"); */
        ilRC = RC_FAIL;
      }                         /* end if */
    }                           /* end if */
    else
    {
      dbg (TRACE, "SEARCH FLIGHT: UNKNOWN TABLE <%s>", pcpTable);
    }                           /* end else */
  }                             /* end else */
  return ilRC;
}                               /* end SearchFlight */

/********************************************************/
/********************************************************/
static void CheckWhereClause (int ipUseOrder, char *pcpSelKey, int ipChkStat, int ipChkHopo, char *pcpOrderKey)
{
  char pclTmpBuf[32];
  char pclAddKey[128];
  char pclRcvSqlBuf[12 * 1024];
  char *pclWhereBgn = NULL;
  char *pclWhereEnd = NULL;
  char *pclOrderBgn = NULL;
  char *pclChrPtr = NULL;
  int ilKeyPos = 0;
  int blFldDat = FALSE;
  pclAddKey[0] = 0x00;

  /* First Copy Selection Text To WorkBuffer */
  strcpy (pclRcvSqlBuf, pcpSelKey);
  /* Set All Chars Of WhereClause To Capitals */
  str_chg_upc (pclRcvSqlBuf);

  pclWhereBgn = strstr (pclRcvSqlBuf, "WHERE");
  if (pclWhereBgn == NULL)
  {
    pclWhereBgn = pclRcvSqlBuf;
  }                             /* end if */
  else
  {
    /* Skip Over KeyWord */
    pclWhereBgn += 5;
  }                             /* end else */
  /* Search First Non Blank Character of WhereClause */
  while ((*pclWhereBgn == ' ') && (*pclWhereBgn != '\0'))
  {
    pclWhereBgn++;
  }                             /* end while */
  pclWhereEnd = pclWhereBgn + strlen (pclWhereBgn);

  /* Set All Data To Blank, So Fields And 'Order By' Are Remaining */
  pclChrPtr = pclWhereBgn;
  blFldDat = FALSE;
  while (*pclChrPtr != '\0')
  {
    if (*pclChrPtr == '\'')
    {
      if (blFldDat == FALSE)
      {
        blFldDat = TRUE;
      }                         /* end if */
      else
      {
        blFldDat = FALSE;
      }                         /* end else */
    }                           /* end if */
    else
    {
      if (blFldDat == TRUE)
      {
        *pclChrPtr = ' ';
      }                         /* end if */
    }                           /* end else */
    pclChrPtr++;
  }                             /* end while */

  /* Search 'Order By' Statement */
  pclOrderBgn = strstr (pclWhereBgn, "ORDER BY");
  if (pclOrderBgn != NULL)
  {
    pclWhereEnd = pclOrderBgn - 1;
  }                             /* end if */
  /* Search Last Non Blank Character of WhereClause */
  pclWhereEnd--;
  while ((pclWhereEnd >= pclWhereBgn) && (*pclWhereEnd == ' '))
  {
    pclWhereEnd--;
  }                             /* end while */
  pclWhereEnd++;
  *pclWhereEnd = '\0';

  if (ipChkStat == TRUE)
  {
    if (strstr (pclWhereBgn, "STAT") == NULL)
    {
      strcpy (pclAddKey, "STAT<>'DEL'");
    }                           /* end if */
  }                             /* end if */
  ilKeyPos = strlen (pclAddKey);

  if ((igAddHopo == TRUE) && (ipChkHopo == TRUE) && (strcmp (pcgDefTblExt, "TAB") == 0))
  {
    if (strstr (pclWhereBgn, "HOPO") == NULL)
    {
      if (ilKeyPos > 0)
      {
        StrgPutStrg (pclAddKey, &ilKeyPos, " AND ", 0, -1, "\0");
      }
      sprintf (pclTmpBuf, "HOPO='%s'", pcgH3LC);
      StrgPutStrg (pclAddKey, &ilKeyPos, pclTmpBuf, 0, -1, "\0");
    }                           /* end if */
  }                             /* end if */
  pclAddKey[ilKeyPos] = 0x00;

  if ((ilKeyPos > 0) || (ipUseOrder == FALSE))
  {
    /* Restore Original WhereClause */
    strcpy (pclRcvSqlBuf, pcpSelKey);
    *pclWhereEnd = '\0';
    dbg (DEBUG, "MODIFY WHERECLAUSE:");
    dbg (DEBUG, "KEY: <%s>", pclWhereBgn);
    dbg (DEBUG, "ADD: <%s>", pclAddKey);
    if (ilKeyPos > 0)
    {
      if (pclWhereEnd > pclWhereBgn)
      {
        sprintf (pcpSelKey, "WHERE (%s)", pclWhereBgn);
        ilKeyPos = strlen (pcpSelKey);
        StrgPutStrg (pcpSelKey, &ilKeyPos, " AND ", 0, -1, "\0");
      }                         /* end if */
      else
      {
        sprintf (pcpSelKey, "WHERE ", pclWhereBgn);
        ilKeyPos = strlen (pcpSelKey);
      }                         /* end else */
      StrgPutStrg (pcpSelKey, &ilKeyPos, pclAddKey, 0, -1, "\0");
      pcpSelKey[ilKeyPos] = 0x00;
    }                           /* end if */
    else
    {
      if (pclWhereEnd > pclWhereBgn)
      {
        sprintf (pcpSelKey, "WHERE %s", pclWhereBgn);
      }                         /* end if */
    }                           /* end else */
    if (ipUseOrder == FALSE)
    {
      pcpOrderKey[0] = 0x00;
    }                           /* end if */
    if (pclOrderBgn != NULL)
    {
      if (ipUseOrder == TRUE)
      {
        ilKeyPos = strlen (pcpSelKey);
        StrgPutStrg (pcpSelKey, &ilKeyPos, " ", 0, -1, pclOrderBgn);
        pcpSelKey[ilKeyPos] = 0x00;
      }                         /* end if */
      else
      {
        strcpy (pcpOrderKey, pclOrderBgn);
      }                         /* end else */
    }                           /* end if */
    dbg (DEBUG, "KEY: <%s>", pcpSelKey);
    if (ipUseOrder == FALSE)
    {
      dbg (DEBUG, "ORD: <%s>", pcpOrderKey);
    }                           /* end if */
  }                             /* end if */

  return;
}                               /* end CheckWhereClause */

/********************************************************/
/********************************************************/
static int BuildSqlReadFields (int ipTbl, char *pcpAddFields, int ipForWhat)
{
  int ilRC = RC_SUCCESS;
  int ilFld = 0;
  int ilFldCnt = 0;
  int ilFldPos = 0;
  char pclFldNam[8];
  ilFldCnt = prgPrjTblCfg[ipTbl].TblFldCnt;
  for (ilFld = 1; ilFld <= ilFldCnt; ilFld++)
  {
    if ((prgTblFldCfg[ipTbl][ilFld].RcvFlg == TRUE) || (prgTblFldCfg[ipTbl][ilFld].ReadField == TRUE) ||
        (prgTblFldCfg[ipTbl][ilFld].SendField == TRUE) || ((igReopFlight == TRUE) &&
                                                           (prgTblFldCfg[ipTbl][ilFld].SendOnReop == TRUE)) ||
        (ipForWhat == FOR_TABLES))
    {
      StrgPutStrg (prgPrjTblCfg[ipTbl].ActFldLst, &ilFldPos, prgTblFldCfg[ipTbl][ilFld].SysTabFina, 0, -1, ",");

    }                           /* end if */
  }                             /* end for */
  ilFldCnt = field_count (pcpAddFields);
  for (ilFld = 1; ilFld <= ilFldCnt; ilFld++)
  {
    prgPrjTblCfg[ipTbl].ActFldLst[ilFldPos] = 0x00;
    (void) get_real_item (pclFldNam, pcpAddFields, ilFld);
    if (strstr (prgPrjTblCfg[ipTbl].ActFldLst, pclFldNam) == NULL)
    {
      StrgPutStrg (prgPrjTblCfg[ipTbl].ActFldLst, &ilFldPos, pclFldNam, 0, -1, ",");
    }                           /* end if */
  }                             /* end for */
  if (ilFldPos > 0)
  {
    ilFldPos--;
    prgPrjTblCfg[ipTbl].ActFldLst[ilFldPos] = 0x00;
    prgPrjTblCfg[ipTbl].ActFldLen = ilFldPos;
  }                             /* end if */
  if (ipForWhat == FOR_TABLES)
  {
    strcpy (prgPrjTblCfg[ipTbl].AllFldLst, prgPrjTblCfg[ipTbl].ActFldLst);
  }                             /* end if */
  return ilRC;
}                               /* end BuildSqlReadFields */

/********************************************************/
/********************************************************/
static int GetRecordData (int ipTbl, int ipRec, int ipFldCnt, char *pcpTable, char *pcpFields, char *pcpData)
{
  int ilRC = RC_SUCCESS;
  int ilTbl;
  int ilFldCnt;
  int ilFld;
  int ilItmNbr;
  int ilItmLen;
  char pclFldNam[16];
  ilTbl = ipTbl;
  if (ilTbl < 1)
  {
    ilTbl = GetTableIndex (pcpTable);
  }                             /* end if */
  if ((ilTbl > 0) && (ipRec <= prgPrjTblCfg[ilTbl].RecHdlMax))
  {
#ifdef jim_make_quickhacks
    prgPrjTblCfg[ilTbl].RecHdlCnt = ipRec;
#else
    /* +++ jim +++ 20001228: this should be more reliable: */
    if (prgPrjTblCfg[ilTbl].RecHdlCnt < ipRec)
    {
       prgPrjTblCfg[ilTbl].RecHdlCnt = ipRec;
    }
#endif /* else of jim_make_quickhacks */

    prgPrjTblCfg[ilTbl].RcvFlg = TRUE;
    ilFldCnt = ipFldCnt;
    if (ilFldCnt < 1)
    {
      ilFldCnt = field_count (pcpFields);
    }                           /* end if */
    for (ilItmNbr = 1; ilItmNbr <= ilFldCnt; ilItmNbr++)
    {
      ilItmLen = get_real_item (pclFldNam, pcpFields, ilItmNbr);
      if (ilItmLen > 0)
      {
        ilFld = GetFieldIndex (ilTbl, pclFldNam);
        if (ilFld > 0)
        {
          prgRecFldDat[ilTbl][ipRec][ilFld].DbsLen =
            get_real_item (prgRecFldDat[ilTbl][ipRec][ilFld].DbsDat, pcpData, ilItmNbr);
          if (prgRecFldDat[ilTbl][ipRec][ilFld].DbsLen < 1)
          {
            strcpy (prgRecFldDat[ilTbl][ipRec][ilFld].DbsDat, " ");
          }                     /* end if */
          (void) CheckFieldValue (prgTblFldCfg[ilTbl][ilFld].FieldType, prgTblFldCfg[ilTbl][ilFld].FieldLength, FALSE,
                                  prgRecFldDat[ilTbl][ipRec][ilFld].DbsDat, &prgRecFldDat[ilTbl][ipRec][ilFld].DbsVal);
          prgRecFldDat[ilTbl][ipRec][ilFld].DbsFlg = TRUE;
        }                       /* end if */
        else
        {
          dbg (TRACE, "UNKNOWN FIELD <%s>", pclFldNam);
        }                       /* end else */
      }                         /* end if */
    }                           /* end for */
  }                             /* end if */
  else
  {
    if (ilTbl < 1)
    {
      dbg (TRACE, "UNKNOWN TABLE <%s> RECORDS NOT STORED", pcpTable);
    }                           /* end if */
    else
    {
      dbg (TRACE, "RECORD OVERFLOW TABLE <%s> REC NBR:%d MAX:%d", pcpTable, ipRec, prgPrjTblCfg[ilTbl].RecHdlMax);
    }                           /* end else */
  }                             /* end else */

  return ilRC;
}                               /* end GetRecordData */

/********************************************************/
/********************************************************/
static int ShowRecordData (void)
{
  int ilRC = RC_SUCCESS;
  int ilTbl = -1;
  int ilRec = -1;
  int ilRecCnt = -1;
  int ilFld = -1;
  int ilFldCnt = -1;
  if (igDebugDetails == TRUE)
  {
    for (ilTbl = 1; ilTbl <= igHandledTables; ilTbl++)
    {
      if (prgPrjTblCfg[ilTbl].RcvFlg == TRUE)
      {
        dbg (TRACE, "============= FETCHED TABLE DATA ==========");
        dbg (TRACE, "TABLE NAME <%s>", prgPrjTblCfg[ilTbl].FullName);
        ilRecCnt = prgPrjTblCfg[ilTbl].RecHdlCnt;
        for (ilRec = 1; ilRec <= ilRecCnt; ilRec++)
        {
          dbg (TRACE, "%d. RECORD:", ilRec);
          ilFldCnt = prgPrjTblCfg[ilTbl].TblFldCnt;
          for (ilFld = 1; ilFld <= ilFldCnt; ilFld++)
          {
            if (prgRecFldDat[ilTbl][ilRec][ilFld].DbsFlg == TRUE)
            {
              dbg (TRACE, "FIELD <%s> <%s>", prgTblFldCfg[ilTbl][ilFld].SysTabFina,
                   prgRecFldDat[ilTbl][ilRec][ilFld].DbsDat);
            }                   /* end if */
          }                     /* end for */
        }                       /* end for */
        dbg (TRACE, "============================================");
      }                         /* end if */
    }                           /* end for */
  }                             /* end if */
  return ilRC;
}                               /* end ShowRecordData */

/********************************************************/
/********************************************************/
static void ShowNewData (void)
{
  int ilTbl = -1;
  int ilRec = -1;
  int ilRecCnt = -1;
  int ilFld = -1;
  int ilFldCnt = -1;
  if (igDebugDetails == TRUE)
  {
    for (ilTbl = 1; ilTbl <= igHandledTables; ilTbl++)
    {
      if (prgPrjTblCfg[ilTbl].RcvFlg == TRUE)
      {
        dbg (TRACE, "=========== RCV/DBS/NEW TABLE DATA ========");
        dbg (TRACE, "TABLE NAME <%s>", prgPrjTblCfg[ilTbl].FullName);
        ilRecCnt = prgPrjTblCfg[ilTbl].RecHdlCnt;
        for (ilRec = 1; ilRec <= ilRecCnt; ilRec++)
        {
          dbg (TRACE, "%d. RECORD:", ilRec);
          ilFldCnt = prgPrjTblCfg[ilTbl].AllFldCnt;
          for (ilFld = 1; ilFld <= ilFldCnt; ilFld++)
          {
            if (prgRecFldDat[ilTbl][ilRec][ilFld].NewFlg == TRUE)
            {
              dbg (TRACE, "<%s> <%s> <%s> <%s>", prgTblFldCfg[ilTbl][ilFld].SysTabFina,
                   prgRecFldDat[ilTbl][ilRec][ilFld].RcvDat, prgRecFldDat[ilTbl][ilRec][ilFld].DbsDat,
                   prgRecFldDat[ilTbl][ilRec][ilFld].NewDat);
            }                   /* end if */
          }                     /* end for */
        }                       /* end for */
        dbg (TRACE, "============================================");
      }                         /* end if */
    }                           /* end for */
  }                             /* end if */
  return;
}                               /* end ShowNewData */

/********************************************************/
/********************************************************/
static int PrepNewData (int ipTblIdx)
{
  int ilRC = RC_SUCCESS;
  int ilTbl = -1;
  int ilFstTbl = -1;
  int ilLstTbl = -1;
  int ilRec = -1;
  int ilRecCnt = -1;
  int ilFld = -1;
  int ilFldCnt = -1;
  if (ipTblIdx > 0)
  {
    ilFstTbl = ipTblIdx;
    ilLstTbl = ipTblIdx;
  }                             /* end if */
  else
  {
    ilFstTbl = 1;
    ilLstTbl = igHandledTables;
  }                             /* end else */
  for (ilTbl = ilFstTbl; ilTbl <= ilLstTbl; ilTbl++)
  {
    if (prgPrjTblCfg[ilTbl].RcvFlg == TRUE)
    {
      /*
         dbg(DEBUG,"============= PREPARE NEW TABLE DATA ==========");
         dbg(DEBUG,"TABLE NAME <%s>",prgPrjTblCfg[ilTbl].FullName);
       */
      ilRecCnt = prgPrjTblCfg[ilTbl].RecHdlCnt;
      for (ilRec = 1; ilRec <= ilRecCnt; ilRec++)
      {
        /* dbg(DEBUG,"%d. RECORD:",ilRec); */
        ilFldCnt = prgPrjTblCfg[ilTbl].AllFldCnt;
        for (ilFld = 1; ilFld <= ilFldCnt; ilFld++)
        {
          if (prgTblFldCfg[ilTbl][ilFld].RcvFlg == TRUE)
          {
            strcpy (prgRecFldDat[ilTbl][ilRec][ilFld].RcvDat, prgTblFldCfg[ilTbl][ilFld].RcvDat);
            prgRecFldDat[ilTbl][ilRec][ilFld].RcvLen = prgTblFldCfg[ilTbl][ilFld].RcvLen;
            prgRecFldDat[ilTbl][ilRec][ilFld].RcvVal = prgTblFldCfg[ilTbl][ilFld].RcvVal;
            prgRecFldDat[ilTbl][ilRec][ilFld].RcvFlg = TRUE;

            strcpy (prgRecFldDat[ilTbl][ilRec][ilFld].NewDat, prgRecFldDat[ilTbl][ilRec][ilFld].RcvDat);
            prgRecFldDat[ilTbl][ilRec][ilFld].NewLen = prgRecFldDat[ilTbl][ilRec][ilFld].RcvLen;
            prgRecFldDat[ilTbl][ilRec][ilFld].NewVal = prgRecFldDat[ilTbl][ilRec][ilFld].RcvVal;
          }                     /* end if */
          else
          {
            if (prgRecFldDat[ilTbl][ilRec][ilFld].DbsFlg == TRUE)
            {
              strcpy (prgRecFldDat[ilTbl][ilRec][ilFld].NewDat, prgRecFldDat[ilTbl][ilRec][ilFld].DbsDat);
              prgRecFldDat[ilTbl][ilRec][ilFld].NewLen = prgRecFldDat[ilTbl][ilRec][ilFld].DbsLen;
              prgRecFldDat[ilTbl][ilRec][ilFld].NewVal = prgRecFldDat[ilTbl][ilRec][ilFld].DbsVal;
            }                   /* end if */
          }                     /* end else */
          if (strcmp (prgRecFldDat[ilTbl][ilRec][ilFld].NewDat, prgRecFldDat[ilTbl][ilRec][ilFld].DbsDat) != 0)
          {
            prgRecFldDat[ilTbl][ilRec][ilFld].NewFlg = TRUE;
          }                     /* end if */
          /*
             dbg(DEBUG,"FIELD <%s> RCV <%s> DBS <%s> NEW <%s> VAL (%d) FLG (%d)",
             prgTblFldCfg[ilTbl][ilFld].SysTabFina,
             prgRecFldDat[ilTbl][ilRec][ilFld].RcvDat,
             prgRecFldDat[ilTbl][ilRec][ilFld].DbsDat,
             prgRecFldDat[ilTbl][ilRec][ilFld].NewDat,
             prgRecFldDat[ilTbl][ilRec][ilFld].NewVal,
             prgRecFldDat[ilTbl][ilRec][ilFld].NewFlg);
           */
        }                       /* end for */
      }                         /* end for */
      if (prgPrjTblCfg[ilTbl].TypeOfTable == 1)
      {  /* +++ jim +++ 20010130: avoid message 'FIELD <CCATAB> <VIAL> not found'   */
         ilRC = ExplodeFieldData (ilTbl, 0, "VIAL");
      }
    }                           /* end if */
  }                             /* end for */
  return ilRC;
}                               /* end PrepNewData */

/********************************************************/
/********************************************************/
static int SetNewData (void)
{
  int ilRC = RC_SUCCESS;
  int ilTbl = -1;
  int ilRec = -1;
  int ilRecCnt = -1;
  int ilFld = -1;
  int ilFldCnt = -1;
  for (ilTbl = 1; ilTbl <= igHandledTables; ilTbl++)
  {
    if (prgPrjTblCfg[ilTbl].RcvFlg == TRUE)
    {
      /*
         dbg(DEBUG,"============= SET NEW TABLE DATA ==========");
         dbg(DEBUG,"TABLE NAME <%s>",prgPrjTblCfg[ilTbl].FullName);
       */
      ilRecCnt = prgPrjTblCfg[ilTbl].RecHdlCnt;
      for (ilRec = 1; ilRec <= ilRecCnt; ilRec++)
      {
        dbg(DEBUG,"%05d: Start outer loop ilRec=%d",__LINE__,ilRec);
        /* dbg(DEBUG,"%d. RECORD:",ilRec); */
        ilFldCnt = prgPrjTblCfg[ilTbl].AllFldCnt;
        for (ilFld = 1; ilFld <= ilFldCnt; ilFld++)
        {
          dbg(DEBUG,"%05d: Start inner loop ilFld=%d",__LINE__,ilFld);
          dbg(DEBUG,"%05d: Start inner loop FIELD <%s> ",__LINE__,
	      prgTblFldCfg[ilTbl][ilFld].SysTabFina);
	  if (prgRecFldDat[ilTbl][ilRec][ilFld].RcvFlg == TRUE)
          {
            strcpy (prgRecFldDat[ilTbl][ilRec][ilFld].NewDat, prgRecFldDat[ilTbl][ilRec][ilFld].RcvDat);
            prgRecFldDat[ilTbl][ilRec][ilFld].NewLen = prgRecFldDat[ilTbl][ilRec][ilFld].RcvLen;
            prgRecFldDat[ilTbl][ilRec][ilFld].NewVal = prgRecFldDat[ilTbl][ilRec][ilFld].RcvVal;
            prgRecFldDat[ilTbl][ilRec][ilFld].NewFlg = TRUE;

               dbg(DEBUG,"FIELD <%s> RCV <%s> NEW <%s> VAL (%d) ",
               prgTblFldCfg[ilTbl][ilFld].SysTabFina,
               prgRecFldDat[ilTbl][ilRec][ilFld].RcvDat,
               prgRecFldDat[ilTbl][ilRec][ilFld].NewDat,
               prgRecFldDat[ilTbl][ilRec][ilFld].NewVal);

dbg(DEBUG,"%05d",__LINE__);
          }                     /* end if */
          else
          {
	    dbg(DEBUG,"SysTabDefa: FIELD <%s> Default <%s>",
	       prgTblFldCfg[ilTbl][ilFld].SysTabFina,
 	       prgTblFldCfg[ilTbl][ilFld].SysTabDefa); 
            strcpy (prgRecFldDat[ilTbl][ilRec][ilFld].NewDat,prgTblFldCfg[ilTbl][ilFld].SysTabDefa);
            prgRecFldDat[ilTbl][ilRec][ilFld].NewLen = 0;
            prgRecFldDat[ilTbl][ilRec][ilFld].NewVal = 0;
            prgRecFldDat[ilTbl][ilRec][ilFld].NewFlg = FALSE;
dbg(DEBUG,"%05d",__LINE__);
          }                     /* end else */
dbg(DEBUG,"%05d: End inner loop ilFld=%d",__LINE__,ilFld);
        }                       /* end for */
dbg(DEBUG,"%05d: End outer loop ilRec=%d",__LINE__,ilRec);
      }                         /* end for */
dbg(DEBUG,"%05d",__LINE__);
      if (prgPrjTblCfg[ilTbl].TypeOfTable == 1)
      {  /* +++ jim +++ 20010130: avoid message 'FIELD <CCATAB> <VIAL> not found'   */
dbg(DEBUG,"%05d",__LINE__);
         ilRC = ExplodeFieldData (ilTbl, 0, "VIAL");
dbg(DEBUG,"%05d",__LINE__);
    }                           /* end if */
    }                           /* end if */
  }                             /* end for */
  dbg(DEBUG,"SetNewData end");
  return ilRC;
}                               /* end SetNewData */

/********************************************************/
/********************************************************/
static int SetNewTimeValues (int ipDayOffset)
{
  int ilRC = RC_SUCCESS;
  int ilTbl = -1;
  int ilRec = -1;
  int ilRecCnt = -1;
  int ilFld = -1;
  int ilFldCnt = -1;
  int ilMinuteOffset = 0;
  char pclSeconds[4];
/*********   TWE 211299  **********/
  int ilFld1 = -1;
  char pclNewStod[20];
  char pclNewStoa[20];
/**********************************/

  ilMinuteOffset = ipDayOffset * 1440;

  for (ilTbl = 1; ilTbl <= igHandledTables; ilTbl++)
  {
    if (prgPrjTblCfg[ilTbl].RcvFlg == TRUE)
    {
      ilRecCnt = prgPrjTblCfg[ilTbl].RecHdlCnt;
   /*********   TWE 211299  **********/
      if (igTWEImport == TRUE)
      {
        strcpy (pclNewStod, "");
        strcpy (pclNewStoa, "");
        if (prgPrjTblCfg[ilTbl].TypeOfTable == 1)
        {                       /* AFT */
          for (ilRec = 1; ilRec <= ilRecCnt; ilRec++)
          {
            /* First we Need ADID */
            HardCodeAdid (ilTbl, ilRec);
          }                     /* end for */
        }
      }
   /**********************************/

      for (ilRec = 1; ilRec <= ilRecCnt; ilRec++)
      {
        ilFldCnt = prgPrjTblCfg[ilTbl].AllFldCnt;
        for (ilFld = 1; ilFld <= ilFldCnt; ilFld++)
        {
          if (prgRecFldDat[ilTbl][ilRec][ilFld].RcvFlg == TRUE)
          {
            if (prgTblFldCfg[ilTbl][ilFld].FieldType == 3)
            {                   /* DATE FIELD */
              if (prgRecFldDat[ilTbl][ilRec][ilFld].NewVal > 0)
              {
         /*********   TWE 211299  **********/
                /* get pclNewStod / pclNewStoa only from first record */

                if ((pclNewStod[0] != '\0') && (igTWEImport == TRUE) &&
                    (strcmp (prgTblFldCfg[ilTbl][ilFld].SysTabFina, "STOD") == 0) )
                {
                }
                else if ((pclNewStoa[0] != '\0') && (igTWEImport == TRUE) &&
                         (strcmp (prgTblFldCfg[ilTbl][ilFld].SysTabFina, "STOA") == 0) )
                {
                }
                else
                {
/**********************************/
                  pclSeconds[0] = prgRecFldDat[ilTbl][ilRec][ilFld].RcvDat[12];
                  pclSeconds[1] = prgRecFldDat[ilTbl][ilRec][ilFld].RcvDat[13];
                  pclSeconds[2] = 0x00;
                  prgRecFldDat[ilTbl][ilRec][ilFld].NewVal = prgRecFldDat[ilTbl][ilRec][ilFld].RcvVal + ilMinuteOffset;

                  (void) FullTimeString (prgRecFldDat[ilTbl][ilRec][ilFld].NewDat,
                                         prgRecFldDat[ilTbl][ilRec][ilFld].NewVal, pclSeconds);

                  dbg (DEBUG, "TIMEFIELD <%s> DBS <%s> RCV <%s> NEW <%s>", prgTblFldCfg[ilTbl][ilFld].SysTabFina,
                       prgRecFldDat[ilTbl][ilRec][ilFld].DbsDat, prgRecFldDat[ilTbl][ilRec][ilFld].RcvDat,
                       prgRecFldDat[ilTbl][ilRec][ilFld].NewDat);

                }               /* TWE */

/*********   TWE 211299  **********/
                if (
                    ((strcmp (prgTblFldCfg[ilTbl][ilFld].SysTabFina, "STOD") == 0) ||
                     (strcmp (prgTblFldCfg[ilTbl][ilFld].SysTabFina, "STOA") == 0)) && 
                    (igTWEImport == TRUE) 
                   )
                {
                  ilFld1 = GetFieldIndex (ilTbl, "ADID");
                  if (ilFld1 > 0)
                  {
                    if (prgRecFldDat[ilTbl][ilRec][ilFld1].NewDat[0] == 'A')
                    {
                      if (strcmp (prgTblFldCfg[ilTbl][ilFld].SysTabFina, "STOA") == 0)
                      {
                        strcpy (pcgRKeyDat, prgRecFldDat[ilTbl][ilRec][ilFld].NewDat);
                        ilFld1 = -1;
                        ilFld1 = GetFieldIndex (ilTbl, "STOD");
                        if (ilFld1 > 0)
                        {
                          strcpy (pclNewStod, prgRecFldDat[ilTbl][ilRec][ilFld1].NewDat);
/* und wenn NewDat = " " ist?    if ((long)(strlen(pclNewStod)) > 0)  +++ jim +++ 20001219 */
                          if ((long) (strlen (pclNewStod)) == 14)
                          {
                            strncpy (pclNewStod, pcgRKeyDat, 8);
                            pclNewStod[14] = 0x00;
                            if (strcmp (pclNewStod, pcgRKeyDat) > 0)
                            {
                              dbg (DEBUG, "SetNewTimeValues: CHANGE STOD <%s> TO DAY BEFORE", pclNewStod);
                              ilRC = AddSecondsToCEDATime2 (pclNewStod, -86400, pcgStampType, 1);
                              if (ilRC != RC_SUCCESS)
                              {
                                dbg (TRACE, "SetNewTimeValues: AddSecondsToCEDATime2 to <%s> failed (-86400)",
                                     pclNewStod);
                              }
                            }

                            if (ilRC == RC_SUCCESS)
                            {

                              strcpy (prgRecFldDat[ilTbl][ilRec][ilFld1].NewDat, pclNewStod);
                            }
                            else
                            {
                              ilRC = RC_SUCCESS;
                            }
                            ilFld1 = GetFieldIndex (ilTbl, "TIFD");
                            if (ilFld1 > 0)
                            {
                              strcpy (prgRecFldDat[ilTbl][ilRec][ilFld1].NewDat, pclNewStod);
                            }
                            ilFld1 = GetFieldIndex (ilTbl, "TISD");
                            if (ilFld1 > 0)
                            {
                              strcpy (prgRecFldDat[ilTbl][ilRec][ilFld1].NewDat, "S");
                            }
                          }     /* if STOD > 0 */
                        }       /* if STOD */
                        if (strcmp (prgRecFldDat[ilTbl][ilRec][ilFld].NewDat, pcgBorderTime) < 0)
                        {
                          ilRC = RC_FAIL;
                        }
                        ilFld1 = GetFieldIndex (ilTbl, "TIFA");
                        if (ilFld1 > 0)
                        {
                          strcpy (prgRecFldDat[ilTbl][ilRec][ilFld1].NewDat, prgRecFldDat[ilTbl][ilRec][ilFld].NewDat);
                        }
                        ilFld1 = GetFieldIndex (ilTbl, "TISA");
                        if (ilFld1 > 0)
                        {
                          strcpy (prgRecFldDat[ilTbl][ilRec][ilFld1].NewDat, "S");
                        }
                      }         /* STOA found */
                    }           /* ADID == A */
                    else if (prgRecFldDat[ilTbl][ilRec][ilFld1].NewDat[0] == 'D')
                    {
                      if (strcmp (prgTblFldCfg[ilTbl][ilFld].SysTabFina, "STOD") == 0)
                      {
                        strcpy (pcgRKeyDat, prgRecFldDat[ilTbl][ilRec][ilFld].NewDat);
                        ilFld1 = GetFieldIndex (ilTbl, "STOA");
                        if (ilFld1 > 0)
                        {
                          strcpy (pclNewStoa, prgRecFldDat[ilTbl][ilRec][ilFld1].NewDat);
/* und wenn NewDat = " " ist?    if ((long)(strlen(pclNewStoa)) > 0)  +++ jim +++ 20001219 */
                          if ((long) (strlen (pclNewStoa)) == 14)
                          {
                            memcpy (pclNewStoa, pcgRKeyDat, 8);
                            pclNewStoa[14] = 0x00;
                            if (strcmp (pclNewStoa, pcgRKeyDat) < 0)
                            {
                              dbg (DEBUG, "SetNewTimeValues: CHANGE STOA <%s> TO DAY AFTER", pclNewStoa);
                              ilRC = AddSecondsToCEDATime2 (pclNewStoa, 86400, pcgStampType, 1);
                              if (ilRC != RC_SUCCESS)
                              {
                                dbg (TRACE, "SetNewTimeValues: AddSecondsToCEDATime2 to <%s> failed (+86400)",
                                     pclNewStoa);
                              }
                            }
                            if (ilRC == RC_SUCCESS)
                            {
                              strcpy (prgRecFldDat[ilTbl][ilRec][ilFld1].NewDat, pclNewStoa);
                            }
                            else
                            {
                              ilRC = RC_SUCCESS;
                            }
                            ilFld1 = GetFieldIndex (ilTbl, "TIFA");
                            if (ilFld1 > 0)
                            {
                              strcpy (prgRecFldDat[ilTbl][ilRec][ilFld1].NewDat, pclNewStoa);
                            }
                            ilFld1 = GetFieldIndex (ilTbl, "TISA");
                            if (ilFld1 > 0)
                            {
                              strcpy (prgRecFldDat[ilTbl][ilRec][ilFld1].NewDat, "S");
                            }
                          }     /* Stoa > 0 */
                        }       /* STOA found */

                        if (strcmp (prgRecFldDat[ilTbl][ilRec][ilFld].NewDat, pcgBorderTime) < 0)
                        {
                          ilRC = RC_FAIL;
                        }
                        ilFld1 = GetFieldIndex (ilTbl, "TIFD");
                        if (ilFld1 > 0)
                        {
                          strcpy (prgRecFldDat[ilTbl][ilRec][ilFld1].NewDat, prgRecFldDat[ilTbl][ilRec][ilFld].NewDat);
                        }
                        ilFld1 = GetFieldIndex (ilTbl, "TISD");
                        if (ilFld1 > 0)
                        {
                          strcpy (prgRecFldDat[ilTbl][ilRec][ilFld1].NewDat, "S");
                        }
                      }         /* STOD found */
                    }           /* ADID == D */
                    else
                    {
                      strcpy (pcgRKeyDat, prgRecFldDat[ilTbl][ilRec][ilFld].NewDat);
                      if (strcmp (prgRecFldDat[ilTbl][ilRec][ilFld].NewDat, pcgBorderTime) < 0)
                      {
                        ilRC = RC_FAIL;
                      }
                    }           /* if adid unknown */
                  }             /* if no adid */
                  else
                  {
                    strcpy (pcgRKeyDat, prgRecFldDat[ilTbl][ilRec][ilFld].NewDat);
                    if (strcmp (prgRecFldDat[ilTbl][ilRec][ilFld].NewDat, pcgBorderTime) < 0)
                    {
                      ilRC = RC_FAIL;
                    }
                  }
                }
              }                 /* end if */
            }                   /* end if */
          }                     /* end if */
        }                       /* end for */
      }                         /* end for */
    }                           /* end if */
  }                             /* end for */
  return ilRC;
}                               /* end SetNewTimeValues */

/********************************************************/
/********************************************************/
static int BuildTimeValues (int ipDayOffset, char *pcpDayDate, char *pcpDaofDat)
{
  int ilRC = RC_SUCCESS;
  int ilTbl = -1;
  int ilRec = -1;
  int ilRecCnt = -1;
  int ilFld = -1;
  int ilFldCnt = -1;
  int ilMinuteOffset = 0;
  int ilDepOffset = 0;
  char pclTmpDate[32];
  ilMinuteOffset = ipDayOffset * 1440;
  ilDepOffset = atoi (pcpDaofDat) * 1440;

  dbg (DEBUG, "BUILD TIMES <%s> (%d)", pcpDayDate, ilMinuteOffset);

  for (ilTbl = 1; ilTbl <= igHandledTables; ilTbl++)
  {
    if (prgPrjTblCfg[ilTbl].RcvFlg == TRUE)
    {
      ilRecCnt = prgPrjTblCfg[ilTbl].RecHdlCnt;
      for (ilRec = 1; ilRec <= ilRecCnt; ilRec++)
      {
        ilFldCnt = prgPrjTblCfg[ilTbl].AllFldCnt;
        for (ilFld = 1; ilFld <= ilFldCnt; ilFld++)
        {
          if (prgTblFldCfg[ilTbl][ilFld].FieldType == 3)
          {                     /* DATE */
            if (prgRecFldDat[ilTbl][ilRec][ilFld].RcvFlg == TRUE)
            {
              if (prgRecFldDat[ilTbl][ilRec][ilFld].RcvLen == 4)
              {
                sprintf (pclTmpDate, "%s%s00", pcpDayDate, prgRecFldDat[ilTbl][ilRec][ilFld].RcvDat);
                dbg (DEBUG, "FIELD <%s> TIME <%s> (%d) <%s>", prgTblFldCfg[ilTbl][ilFld].SysTabFina,
                     prgRecFldDat[ilTbl][ilRec][ilFld].RcvDat, prgRecFldDat[ilTbl][ilRec][ilFld].RcvVal, pclTmpDate);

                (void) StoreReceivedData (TRUE, ilTbl, ilRec, 0, pcgAftTab, prgTblFldCfg[ilTbl][ilFld].SysTabFina,
                                          pclTmpDate);
                if ((ilRec == 2) && (ilDepOffset > 0))
                {
                  prgRecFldDat[ilTbl][ilRec][ilFld].RcvVal += ilDepOffset;

                  (void) FullTimeString (prgRecFldDat[ilTbl][ilRec][ilFld].RcvDat,
                                         prgRecFldDat[ilTbl][ilRec][ilFld].RcvVal, "00");
                  dbg (DEBUG, "FIELD <%s> TIME SHIFTED TO <%s>", prgTblFldCfg[ilTbl][ilFld].SysTabFina,
                       prgRecFldDat[ilTbl][ilRec][ilFld].RcvDat);
                }               /* end if */
              }
              else if (prgRecFldDat[ilTbl][ilRec][ilFld].RcvLen != 14)
              {
                dbg (DEBUG, "FIELD <%s> TIME LENGTH INVALID: <%s>, dropped!", prgTblFldCfg[ilTbl][ilFld].SysTabFina,
                     prgRecFldDat[ilTbl][ilRec][ilFld].RcvDat);
                prgRecFldDat[ilTbl][ilRec][ilFld].RcvFlg = FALSE;
                prgRecFldDat[ilTbl][ilRec][ilFld].RcvLen = 0;
                prgRecFldDat[ilTbl][ilRec][ilFld].RcvDat[0] = 0;
              }                 /* end if */
            }                   /* end if */
          }                     /* end if */
        }                       /* end for */
        if (igImportAction == TRUE)
        {
          (void) SetFlnoItems (ilTbl, ilRec);
dbg(DEBUG,"%05d:",__LINE__);
        }                       /* end if */
      }                         /* end for */
    }                           /* end if */
  }                             /* end for */
  return ilRC;
}                               /* end BuildTimeValues */

/********************************************************/
/********************************************************/
static int EvaluateConflicts (int ipTbl)
{
  int ilRC = RC_SUCCESS;
  int ilCfl = -1;
  int ilCflCnt = -1;
  int ilRec = -1;
  int ilRecCnt = -1;

  ilRecCnt = prgPrjTblCfg[ipTbl].RecHdlCnt;

  ilCflCnt = prgPrjTblCfg[ipTbl].TblCflCnt;
  for (ilCfl = 1; ilCfl <= ilCflCnt; ilCfl++)
  {
    for (ilRec = 1; ilRec <= ilRecCnt; ilRec++)
    {
      /*
         dbg(TRACE,"CHECK CFL <%s> <%s>",
         prgTblCflCfg[ipTbl][ilCfl].CnflTarget,
         prgTblCflCfg[ipTbl][ilCfl].CnflSource);
       */
      switch (prgTblCflCfg[ipTbl][ilCfl].CnflType)
      {
        case 1:                /* NEW_VALUES */
          CheckNewEntry (ipTbl, ilRec, prgTblCflCfg[ipTbl][ilCfl].CnflTarget, prgTblCflCfg[ipTbl][ilCfl].CnflSource);
          break;
        case 2:                /* CHG_VALUES */
          CheckChgEntry (ipTbl, ilRec, prgTblCflCfg[ipTbl][ilCfl].CnflTarget, prgTblCflCfg[ipTbl][ilCfl].CnflSource);
          break;
        case 3:                /* DEL_VALUES */
          CheckDelEntry (ipTbl, ilRec, prgTblCflCfg[ipTbl][ilCfl].CnflTarget, prgTblCflCfg[ipTbl][ilCfl].CnflSource);
          break;
        case 4:                /* ANY_CHANGE */
          CheckAnyChange (ipTbl, ilRec, prgTblCflCfg[ipTbl][ilCfl].CnflTarget, prgTblCflCfg[ipTbl][ilCfl].CnflSource);
          break;
        case 5:                /* MIX_FIELDS */
          CheckMixFields (ipTbl, ilRec, prgTblCflCfg[ipTbl][ilCfl].CnflTarget, prgTblCflCfg[ipTbl][ilCfl].CnflSource);
          break;
        case 6:                /* CMP_VALUES */
          CheckCmpValues (ipTbl, ilRec, prgTblCflCfg[ipTbl][ilCfl].CnflTarget, prgTblCflCfg[ipTbl][ilCfl].CnflSource);
          break;
        default:
          break;
      }                         /* end switch */
    }                           /* end for */
  }                             /* end for */

  return ilRC;

}                               /* end EvaluateConflicts */

/********************************************************/
/********************************************************/
static int EvaluateFieldRules (int ipTbl)
{
  int ilRC = RC_SUCCESS;
  int ilRul = -1;
  int ilRulCnt = -1;
  int ilRec = -1;
  int ilRecCnt = -1;
  int ilActLoop;
  int ilActChgCnt;
  int ilMaxLoop;

  ilRecCnt = prgPrjTblCfg[ipTbl].RecHdlCnt;

  /* FOLLOWING HARD CODED PARTS */
  /* Some of them are Used in Rules */
  /* So we Evaluate them Now */

   TRACE_FIELD_OF_INTEREST(__LINE__,"EVRA");

  if (prgPrjTblCfg[ipTbl].TypeOfTable == 1)
  {                             /* AFT */
    for (ilRec = 1; ilRec <= ilRecCnt; ilRec++)
    {
      /* First we Need STAT (Including Check ReturnFlights) */
      /* and ADID */
      HardCodeStat (ipTbl, ilRec);

  TRACE_FIELD_OF_INTEREST(__LINE__,"34");

      HardCodeAdid (ipTbl, ilRec);
    }                           /* end for */
  }                             /* end if */


  TRACE_FIELD_OF_INTEREST(__LINE__,"35");

  ilRulCnt = prgPrjTblCfg[ipTbl].TblRulCnt;
  ilMaxLoop = ilRulCnt + 1;
  ilActLoop = 0;
  do
  {
    ilActLoop++;
    prgPrjTblCfg[ipTbl].DatChgCnt = 0;
    for (ilRul = 1; ilRul <= ilRulCnt; ilRul++)
    {
      for (ilRec = 1; ilRec <= ilRecCnt; ilRec++)
      {
        ilActChgCnt = prgPrjTblCfg[ipTbl].DatChgCnt;
        switch (prgTblRulCfg[ipTbl][ilRul].RuleType)
        {
          case 1:              /* FIRST_VALUE */
            ilRC =
              SetBestTimes (ipTbl, ilRec, prgTblRulCfg[ipTbl][ilRul].RuleTarget, prgTblRulCfg[ipTbl][ilRul].RuleSource);
            break;
          case 2:              /* BASIC_DATA */
            if (ilActLoop == 1)
            {

  TRACE_FIELD_OF_INTEREST(__LINE__,"36");

              ilRC =
                SetBasicData (ipTbl, ilRec, prgTblRulCfg[ipTbl][ilRul].RuleTarget,
                              prgTblRulCfg[ipTbl][ilRul].RuleSource);

  TRACE_FIELD_OF_INTEREST(__LINE__,"37");

            }                   /* end if */
            break;
          case 3:              /* CHECK_VALUE */
            ilRC =
              SetCheckTimes (ipTbl, ilRec, prgTblRulCfg[ipTbl][ilRul].RuleTarget,
                             prgTblRulCfg[ipTbl][ilRul].RuleSource);
            break;
          case 4:              /* CALCULATE */
            ilRC =
              SetCalcTimes (ipTbl, ilRec, prgTblRulCfg[ipTbl][ilRul].RuleTarget, prgTblRulCfg[ipTbl][ilRul].RuleSource);
            break;
          case 5:              /* CHANGE_INFO */
            ilRC =
              SetChangeInfo (ipTbl, ilRec, prgTblRulCfg[ipTbl][ilRul].RuleTarget,
                             prgTblRulCfg[ipTbl][ilRul].RuleSource);
            break;
          case 6:              /* CROSS_CHECK */
            if (ilActLoop == 1)
            {
              ilRC =
                CheckBasicData (ipTbl, ilRec, prgTblRulCfg[ipTbl][ilRul].RuleTarget,
                                prgTblRulCfg[ipTbl][ilRul].RuleSource);
            }                   /* end if */
            break;
          case 7:              /* MULTI_INPUT *//* What Ever It Means */
            break;
          case 8:              /* AUTO_FILL */
            ilRC =
              AutoFillField (ipTbl, ilRec, prgTblRulCfg[ipTbl][ilRul].RuleTarget,
                             prgTblRulCfg[ipTbl][ilRul].RuleSource);
            break;
          case 9:              /* DAY_CHECK */
            ilRC =
              DayCheckField (ipTbl, ilRec, prgTblRulCfg[ipTbl][ilRul].RuleTarget,
                             prgTblRulCfg[ipTbl][ilRul].RuleSource);
            break;
          case 10:             /* BASIC_LOOKUP */
            ilRC =
              BasicLookup (ipTbl, ilRec, prgTblRulCfg[ipTbl][ilRul].RuleTarget, prgTblRulCfg[ipTbl][ilRul].RuleSource);
            break;
          case 11:             /* BASIC_VALUE */
            if (ilActLoop == 1)
            {
              ilRC =
                SetBasicColumn (ipTbl, ilRec, prgTblRulCfg[ipTbl][ilRul].RuleTarget,
                                prgTblRulCfg[ipTbl][ilRul].RuleSource);
            }                   /* end if */
            break;
          default:
            break;
        }                       /* end switch */
        if (ilActChgCnt != prgPrjTblCfg[ipTbl].DatChgCnt)
        {
          if (igDebugDetails == TRUE)
          {
            dbg (TRACE, "RULE USED <%s>", prgTblRulCfg[ipTbl][ilRul].RuleTarget);
          }                     /* end if */
        }                       /* end if */
      }                         /* end for */
    }                           /* end for */
    ilMaxLoop--;
  }
  while ((prgPrjTblCfg[ipTbl].DatChgCnt > 0) && (ilMaxLoop > 0));


  TRACE_FIELD_OF_INTEREST(__LINE__,"38");

  if ((prgPrjTblCfg[ipTbl].DatChgCnt > 0) && (ilMaxLoop <= 0))
  {
    dbg (TRACE, "STOPPED ENDLESS LOOP IN FIELD RULES !");
  }                             /* end if */

  /* FOLLOWING HARD CODED PARTS */
  /* Some of them Use Result of Rules */
  /* So we Evaluate them Here */
  if (prgPrjTblCfg[ipTbl].TypeOfTable == 1)
  {                             /* AFT */
    for (ilRec = 1; ilRec <= ilRecCnt; ilRec++)
    {
      /* Now we Need the Rest of All */
      HardCodePaid (ipTbl, ilRec);
      HardCodeFkey (ipTbl, ilRec, TRUE, "");
      HardCodeCsgn (ipTbl, ilRec);
      HardCodeDoop (ipTbl, ilRec, "DOOA", "STOA");
      HardCodeDoop (ipTbl, ilRec, "DOOD", "STOD");
      HardCodeSeas (ipTbl, ilRec);
      HardCodeJcnt (ipTbl, ilRec); /* +++ jim +++ 20000826 */

      (void) TWEHardCodeFlno ();
      HardCodeVias (ipTbl, ilRec);

    }                           /* end for */
  }                             /* end if */

  /* Retry Rules again (FLTI needs it) */
  ilRulCnt = prgPrjTblCfg[ipTbl].TblRulCnt;
  ilMaxLoop = ilRulCnt + 1;
  ilActLoop = 0;
  do
  {
    ilActLoop++;
    for (ilRul = 1; ilRul <= ilRulCnt; ilRul++)
    {
      for (ilRec = 1; ilRec <= ilRecCnt; ilRec++)
      {
        ilActChgCnt = prgPrjTblCfg[ipTbl].DatChgCnt;
        switch (prgTblRulCfg[ipTbl][ilRul].RuleType)
        {

          case 10:             /* BASIC_LOOKUP */
            ilRC =
              BasicLookup (ipTbl, ilRec, prgTblRulCfg[ipTbl][ilRul].RuleTarget, prgTblRulCfg[ipTbl][ilRul].RuleSource);
            break;
          default:
            break;
        }                       /* end switch */
        if (ilActChgCnt != prgPrjTblCfg[ipTbl].DatChgCnt)
        {
          if (igDebugDetails == TRUE)
          {
            dbg (TRACE, "RULE USED <%s>", prgTblRulCfg[ipTbl][ilRul].RuleTarget);
          }                     /* end if */
        }                       /* end if */
      }                         /* end for */
    }                           /* end for */
    ilMaxLoop--;
  }
  while ((prgPrjTblCfg[ipTbl].DatChgCnt > 0) && (ilMaxLoop > 0));

   TRACE_FIELD_OF_INTEREST(__LINE__,"EVRE");

  return ilRC;
}                               /* end EvaluateFieldRules */

/********************************************************/
/********************************************************/
static void CheckNewEntry (int ipTbl, int ipRec, char *pcpTgtFldLst, char *pcpSrcFldLst)
{
  int ilLen = 0;
  int ilSrcFldCnt = 0;
  int ilSrcFld = 0;
  int ilSrcFldIdx = 0;
  char pclFldNam[16];
  if (igCurrentAction == FOR_INSERT)
  {
    return;
  }                             /* end if */
  ilSrcFldCnt = field_count (pcpSrcFldLst);
  for (ilSrcFld = 1; ilSrcFld <= ilSrcFldCnt; ilSrcFld++)
  {
    ilLen = get_real_item (pclFldNam, pcpSrcFldLst, ilSrcFld);
    if (ilLen > 0)
    {
      ilSrcFldIdx = GetFieldIndex (ipTbl, pclFldNam);
      if (ilSrcFldIdx > 0)
      {
        if ((prgRecFldDat[ipTbl][ipRec][ilSrcFldIdx].NewFlg == TRUE) &&
            (prgRecFldDat[ipTbl][ipRec][ilSrcFldIdx].DbsLen == 0))
        {
          dbg (TRACE, "CONFLICT: NEW VALUE INSERTED !");
          SendConflict (ipTbl, ipRec, ilSrcFldIdx, pcpTgtFldLst);
        }                       /* end if */
      }                         /* end if */
    }                           /* end if */
  }                             /* end for */
  return;
}                               /* end CheckNewEntry */

/********************************************************/
/********************************************************/
static void CheckChgEntry (int ipTbl, int ipRec, char *pcpTgtFldLst, char *pcpSrcFldLst)
{
  int ilLen = 0;
  int ilSrcFldCnt = 0;
  int ilSrcFld = 0;
  int ilSrcFldIdx = 0;
  char pclFldNam[16];
  if (igCurrentAction == FOR_INSERT)
  {
    return;
  }                             /* end if */
  ilSrcFldCnt = field_count (pcpSrcFldLst);
  for (ilSrcFld = 1; ilSrcFld <= ilSrcFldCnt; ilSrcFld++)
  {
    ilLen = get_real_item (pclFldNam, pcpSrcFldLst, ilSrcFld);
    if (ilLen > 0)
    {
      ilSrcFldIdx = GetFieldIndex (ipTbl, pclFldNam);
      if (ilSrcFldIdx > 0)
      {
        if ((prgRecFldDat[ipTbl][ipRec][ilSrcFldIdx].NewFlg == TRUE) &&
            (prgRecFldDat[ipTbl][ipRec][ilSrcFldIdx].NewLen > 0) &&
            (prgRecFldDat[ipTbl][ipRec][ilSrcFldIdx].DbsLen > 0))
        {
          dbg (TRACE, "CONFLICT: FIELD VALUE CHANGED !");
          SendConflict (ipTbl, ipRec, ilSrcFldIdx, pcpTgtFldLst);
        }                       /* end if */
      }                         /* end if */
    }                           /* end if */
  }                             /* end for */
  return;
}                               /* end CheckChgEntry */

/********************************************************/
/********************************************************/
static void CheckDelEntry (int ipTbl, int ipRec, char *pcpTgtFldLst, char *pcpSrcFldLst)
{
  int ilLen = 0;
  int ilSrcFldCnt = 0;
  int ilSrcFld = 0;
  int ilSrcFldIdx = 0;
  char pclFldNam[16];
  if (igCurrentAction == FOR_INSERT)
  {
    return;
  }                             /* end if */
  ilSrcFldCnt = field_count (pcpSrcFldLst);
  for (ilSrcFld = 1; ilSrcFld <= ilSrcFldCnt; ilSrcFld++)
  {
    ilLen = get_real_item (pclFldNam, pcpSrcFldLst, ilSrcFld);
    if (ilLen > 0)
    {
      ilSrcFldIdx = GetFieldIndex (ipTbl, pclFldNam);
      if (ilSrcFldIdx > 0)
      {
        if ((prgRecFldDat[ipTbl][ipRec][ilSrcFldIdx].NewFlg == TRUE) &&
            (prgRecFldDat[ipTbl][ipRec][ilSrcFldIdx].NewLen == 0) &&
            (prgRecFldDat[ipTbl][ipRec][ilSrcFldIdx].DbsLen > 0))
        {
          dbg (TRACE, "CONFLICT: FIELD VALUE ERASED !");
          SendConflict (ipTbl, ipRec, ilSrcFldIdx, pcpTgtFldLst);
        }                       /* end if */
      }                         /* end if */
    }                           /* end if */
  }                             /* end for */
  return;
}                               /* end CheckDelEntry */

/********************************************************/
/********************************************************/
static void CheckAnyChange (int ipTbl, int ipRec, char *pcpTgtFldLst, char *pcpSrcFldLst)
{
  int ilLen = 0;
  int ilSrcFldCnt = 0;
  int ilSrcFld = 0;
  int ilSrcFldIdx = 0;
  char pclFldNam[16];
  if (igCurrentAction == FOR_INSERT)
  {
    return;
  }                             /* end if */
  ilSrcFldCnt = field_count (pcpSrcFldLst);
  for (ilSrcFld = 1; ilSrcFld <= ilSrcFldCnt; ilSrcFld++)
  {
    ilLen = get_real_item (pclFldNam, pcpSrcFldLst, ilSrcFld);
    if (ilLen > 0)
    {
      ilSrcFldIdx = GetFieldIndex (ipTbl, pclFldNam);
      if (ilSrcFldIdx > 0)
      {
        if (prgRecFldDat[ipTbl][ipRec][ilSrcFldIdx].NewFlg == TRUE)
        {
          dbg (TRACE, "ANY CONFLICT: FIELD VALUE <%s> CHANGED:RCV <%s> DBS <%s> NEW <%s> !", 
               pclFldNam, prgRecFldDat[ipTbl][ipRec][ilSrcFldIdx].RcvDat, 
                          prgRecFldDat[ipTbl][ipRec][ilSrcFldIdx].DbsDat,
                          prgRecFldDat[ipTbl][ipRec][ilSrcFldIdx].NewDat);
          SendConflict (ipTbl, ipRec, ilSrcFldIdx, pcpTgtFldLst);
        }                       /* end if */
      }                         /* end if */
    }                           /* end if */
  }                             /* end for */
  return;
}                               /* end CheckAnyChange */

/********************************************************/
/********************************************************/
static void CheckMixFields (int ipTbl, int ipRec, char *pcpTgtFldLst, char *pcpSrcFldLst)
{
  int ilLen = 0;
  int ilChkFld = 0;
  int ilSrcFldCnt = 0;
  int ilSrcFld = 0;
  int ilSrcFldIdx = 0;
  char pclFldNam[16];
  if (igCurrentAction == FOR_INSERT)
  {
    return;
  }                             /* end if */
  ilSrcFldCnt = field_count (pcpSrcFldLst);
  for (ilSrcFld = 1; ((ilSrcFld <= ilSrcFldCnt) && (ilChkFld == 0)); ilSrcFld++)
  {
    ilLen = get_real_item (pclFldNam, pcpSrcFldLst, ilSrcFld);
    if (ilLen > 0)
    {
      ilSrcFldIdx = GetFieldIndex (ipTbl, pclFldNam);
      if (ilSrcFldIdx > 0)
      {
        if (prgRecFldDat[ipTbl][ipRec][ilSrcFldIdx].NewFlg == TRUE)
        {
          ilChkFld = ilSrcFldIdx;
        }                       /* end if */
      }                         /* end if */
    }                           /* end if */
  }                             /* end for */

  if (ilChkFld > 0)
  {
    for (ilSrcFld = 1; ilSrcFld <= ilSrcFldCnt; ilSrcFld++)
    {
      ilLen = get_real_item (pclFldNam, pcpSrcFldLst, ilSrcFld);
      if (ilLen > 0)
      {
        ilSrcFldIdx = GetFieldIndex (ipTbl, pclFldNam);
        if ((ilSrcFldIdx > 0) && (ilSrcFldIdx != ilChkFld))
        {
          if ((prgRecFldDat[ipTbl][ipRec][ilSrcFldIdx].DbsLen > 0) &&
              (strcmp (prgRecFldDat[ipTbl][ipRec][ilSrcFldIdx].NewDat, prgRecFldDat[ipTbl][ipRec][ilChkFld].NewDat) !=
               0))
          {
            dbg (TRACE, "ANY CONFLICT: MIX FIELD VALUE CHANGED !");
            SendConflict (ipTbl, ipRec, ilSrcFldIdx, pcpTgtFldLst);
          }                     /* end if */
        }                       /* end if */
      }                         /* end if */
    }                           /* end for */
  }                             /* end if */
  return;
}                               /* end CheckMixFields */

static void CheckCmpValues (int ipTbl, int ipRec, char *pcpTgtFldLst, char *pcpSrcFldLst)
{
  int ilLen = 0;
  int ilValueCount = 0;
  int ilValueItem = 0;
  int ilSrcFldIdx = 0;
  char pclFldNam[16];
  char clValue[1024];

  dbg (DEBUG, "CheckCmpValues Action %d Tbl %d Rec %d \nTarget <%s> \nSource <%s>", igCurrentAction, ipTbl, ipRec,
       pcpTgtFldLst, pcpSrcFldLst);
  ilValueCount = field_count (pcpSrcFldLst);
  ilLen = get_real_item (pclFldNam, pcpSrcFldLst, 1);
  dbg (DEBUG, "ValueCount %d Lend FieldName %d FieldName <%s>", ilValueCount, ilLen, pclFldNam);
  if (ilLen > 0)
  {
    ilSrcFldIdx = GetFieldIndex (ipTbl, pclFldNam);
    dbg (DEBUG, "Field Index %d", ilSrcFldIdx);
    if (ilSrcFldIdx > 0)
    {
      for (ilValueItem = 2; ilValueItem <= ilValueCount; ilValueItem++)
      {
        *clValue = '\0';
        ilLen = get_real_item (clValue, pcpSrcFldLst, ilValueItem);
        dbg (DEBUG, "Value %d got <%s> len %d", ilValueItem, clValue, ilLen);
        dbg (DEBUG, "DBS %d <%s>", prgRecFldDat[ipTbl][ipRec][ilSrcFldIdx].DbsFlg,
             prgRecFldDat[ipTbl][ipRec][ilSrcFldIdx].DbsDat);
        dbg (DEBUG, "RCV %d <%s>", prgRecFldDat[ipTbl][ipRec][ilSrcFldIdx].RcvFlg,
             prgRecFldDat[ipTbl][ipRec][ilSrcFldIdx].RcvDat);
        dbg (DEBUG, "NEW %d <%s>", prgRecFldDat[ipTbl][ipRec][ilSrcFldIdx].NewFlg,
             prgRecFldDat[ipTbl][ipRec][ilSrcFldIdx].NewDat);

        if (ilLen > 0 && prgRecFldDat[ipTbl][ipRec][ilSrcFldIdx].NewFlg == TRUE)
        {
          dbg (DEBUG, "CMP VALUE CONFLICT: FIELD <%s> VALUE <%s/%s>", pclFldNam, clValue,
               prgRecFldDat[ipTbl][ipRec][ilSrcFldIdx].NewDat);
          if (!strcmp (prgRecFldDat[ipTbl][ipRec][ilSrcFldIdx].NewDat, clValue))
          {
            dbg (DEBUG, "CMP VALUE CONFLICT: FIELD <%s> VALUE <%s> FOUND !", pclFldNam, clValue);
            SendConflict (ipTbl, ipRec, ilSrcFldIdx, pcpTgtFldLst);
          }
        }                       /* end if */
      }                         /* end if */
    }                           /* end if */
  }                             /* end for */
  return;
}

/********************************************************/
/********************************************************/
static int SetBestTimes (int ipTbl, int ipRec, char *pcpTgtFldLst, char *pcpSrcFldLst)
{
  int ilRC = RC_SUCCESS;
  int ilDestFld = 0;
  int ilStatFld = 0;
  int ilFldIdx = 0;
  int ilFldCnt = 0;
  int ilFldNbr = 0;
  int ilNamLen = 0;
  int ilMinVal = 0;
  int ilNewTime = 0;
  int ilChgFlg = FALSE;
  char pclFldNam[16];
  char pclStatVal[16];
  char pclTimeVal[32];
  char pclSeconds[4];
  ilNamLen = get_real_item (pclFldNam, pcpTgtFldLst, 1);
  ilDestFld = GetFieldIndex (ipTbl, pclFldNam);
  ilNamLen = get_real_item (pclFldNam, pcpTgtFldLst, 2);
  ilStatFld = GetFieldIndex (ipTbl, pclFldNam);
  if ((ilDestFld > 0) && (ilStatFld > 0))
  {
    ilFldCnt = field_count (pcpSrcFldLst);
    do
    {
      ilFldNbr++;
      ilNamLen = get_real_item (pclFldNam, pcpSrcFldLst, ilFldNbr);
      ilFldNbr++;
      if (pclFldNam[0] == '|')
      {
        /* dbg(TRACE,"ELSE RULE DETECTED"); */
        ilNamLen = get_real_item (pclFldNam, pcpSrcFldLst, ilFldNbr);
        ilFldNbr++;
        (void) get_real_item (pclTimeVal, pcpSrcFldLst, ilFldNbr);
        ilFldNbr++;
        ilFldIdx = GetFieldIndex (ipTbl, pclFldNam);
        if (ilFldIdx > 0)
        {
          if (prgRecFldDat[ipTbl][ipRec][ilFldIdx].NewLen > 0)
          {
            ilMinVal = 0;
            ilMinVal = atoi (pclTimeVal);
            ilNewTime = prgRecFldDat[ipTbl][ipRec][ilFldIdx].NewVal + ilMinVal;
            pclSeconds[0] = prgRecFldDat[ipTbl][ipRec][ilFldIdx].NewDat[12];
            pclSeconds[1] = prgRecFldDat[ipTbl][ipRec][ilFldIdx].NewDat[13];
            pclSeconds[2] = 0x00;
            (void) FullTimeString (pclTimeVal, ilNewTime, pclSeconds);
            (void) SetNewValues (ipTbl, ipRec, ilDestFld, "", pclTimeVal);
            (void) get_real_item (pclStatVal, pcpSrcFldLst, ilFldNbr);
            (void) SetNewValues (ipTbl, ipRec, ilStatFld, "", pclStatVal);
            ilChgFlg = TRUE;
          }                     /* end if */
        }                       /* end if */
        ilNamLen = 0;
      }                         /* end if */
      if (ilNamLen == 4)
      {
        ilFldIdx = GetFieldIndex (ipTbl, pclFldNam);
        if (ilFldIdx > 0)
        {
          (void) get_real_item (pclStatVal, pcpSrcFldLst, ilFldNbr);
          /* to be sure */
          /* pclStatVal[1] = 0x00; */
          if (prgRecFldDat[ipTbl][ipRec][ilFldIdx].NewLen > 0)
          {
            (void) SetNewValues (ipTbl, ipRec, ilDestFld, "", prgRecFldDat[ipTbl][ipRec][ilFldIdx].NewDat);
            (void) SetNewValues (ipTbl, ipRec, ilStatFld, "", pclStatVal);
            ilChgFlg = TRUE;
          }                     /* end if */
        }                       /* end if */
      }                         /* end if */
    }
    while ((ilFldNbr < ilFldCnt) && (ilChgFlg != TRUE));
    if (ilChgFlg != TRUE)
    {
      /* All Source Fields Are Empty */
      (void) SetNewValues (ipTbl, ipRec, ilDestFld, "", " ");
      (void) SetNewValues (ipTbl, ipRec, ilStatFld, "", " ");
      /* dbg(DEBUG,"NOTHING FOUND, SO SET TO BLANK"); */
    }                           /* end if */
  }                             /* end if */
  return ilRC;
}                               /* end SetBestTimes */

/********************************************************/
/********************************************************/
static int SetCheckTimes (int ipTbl, int ipRec, char *pcpTgtFldLst, char *pcpSrcFldLst)
{
  int ilRC = RC_SUCCESS;
  int ilDestFld = 0;
  int ilStatFld = 0;
  int ilFldIdx = 0;
  int ilFldCnt = 0;
  int ilFldNbr = 0;
  int ilNamLen = 0;
  int ilChgFlg = FALSE;
  char pclChkNam[16];
  char pclFldNam[16];
  char pclStatVal[16];
  ilNamLen = get_real_item (pclFldNam, pcpTgtFldLst, 1);
  ilDestFld = GetFieldIndex (ipTbl, pclFldNam);
  ilNamLen = get_real_item (pclFldNam, pcpTgtFldLst, 2);
  ilStatFld = GetFieldIndex (ipTbl, pclFldNam);
  if ((ilDestFld > 0) && (ilStatFld > 0))
  {
    ilFldCnt = field_count (pcpSrcFldLst);
    do
    {
      ilFldNbr++;
      ilNamLen = get_real_item (pclChkNam, pcpSrcFldLst, ilFldNbr);
      ilFldNbr++;
      (void) get_real_item (pclFldNam, pcpSrcFldLst, ilFldNbr);
      ilFldNbr++;
      (void) get_real_item (pclStatVal, pcpSrcFldLst, ilFldNbr);
      if (ilNamLen == 4)
      {
        ilFldIdx = GetFieldIndex (ipTbl, pclChkNam);
        if (ilFldIdx > 0)
        {
          /* to be sure */
          pclStatVal[1] = 0x00;
          if (prgRecFldDat[ipTbl][ipRec][ilFldIdx].NewLen > 0)
          {
            ilFldIdx = GetFieldIndex (ipTbl, pclFldNam);
            (void) SetNewValues (ipTbl, ipRec, ilDestFld, "", prgRecFldDat[ipTbl][ipRec][ilFldIdx].NewDat);
            (void) SetNewValues (ipTbl, ipRec, ilStatFld, "", pclStatVal);
            ilChgFlg = TRUE;
          }                     /* end if */
        }                       /* end if */
      }                         /* end if */
    }
    while ((ilFldNbr < ilFldCnt) && (ilChgFlg != TRUE));
    if (ilChgFlg != TRUE)
    {
      /* All Source Fields Are Empty */
      /* Check For ElseRule Not Yet Implemented */
      /* SO: HARD CODED */
      (void) SetNewValues (ipTbl, ipRec, ilDestFld, "", " ");
      (void) SetNewValues (ipTbl, ipRec, ilStatFld, "", " ");
      /* dbg(DEBUG,"NOTHING FOUND, SO SET TO BLANK"); */
    }                           /* end if */
  }                             /* end if */
  return ilRC;
}                               /* end SetCheckTimes */

/********************************************************/
/********************************************************/
static int SetCalcTimes (int ipTbl, int ipRec, char *pcpTgtFldLst, char *pcpSrcFldLst)
{
  int ilRC = RC_SUCCESS;
  int ilDestFld = 0;
  int ilFldIdx = 0;
  int ilFldCnt = 0;
  int ilFldNbr = 0;
  int ilNamLen = 0;
  int ilMinVal = 0;
  int ilNewTime = 0;
  int ilChgFlg = FALSE;
  int ilChkRcv = FALSE;
  char pclRulFlg[32];
  char pclFldNam[32];
  char pclTimeVal[32];
  char pclSeconds[4];
  ilFldCnt = field_count (pcpTgtFldLst);
  if (ilFldCnt == 1)
  {
    strcpy (pclRulFlg, "ALL");
    ilNamLen = get_real_item (pclFldNam, pcpTgtFldLst, 1);
  }                             /* end if */
  else
  {
    ilNamLen = get_real_item (pclRulFlg, pcpTgtFldLst, 1);
    ilNamLen = get_real_item (pclFldNam, pcpTgtFldLst, 2);
  }                             /* end else */
  if (strcmp (pclRulFlg, "RCV") == 0)
  {
    ilChkRcv = TRUE;
  }                             /* end if */
  else
  {
    ilChkRcv = FALSE;
  }                             /* end else */

  ilDestFld = GetFieldIndex (ipTbl, pclFldNam);
  if (ilDestFld > 0)
  {
    ilFldCnt = field_count (pcpSrcFldLst);
    do
    {
      ilFldNbr++;
      ilNamLen = get_real_item (pclFldNam, pcpSrcFldLst, ilFldNbr);
      ilFldNbr++;
      if (ilNamLen == 4)
      {
        ilFldIdx = GetFieldIndex (ipTbl, pclFldNam);
        if (ilFldIdx > 0)
        {
          if ((ilChkRcv == FALSE) || (prgRecFldDat[ipTbl][ipRec][ilFldIdx].RcvFlg == TRUE))
          {
            (void) get_real_item (pclTimeVal, pcpSrcFldLst, ilFldNbr);
            ilMinVal = 0;
            ilMinVal = atoi (pclTimeVal);
            if (prgRecFldDat[ipTbl][ipRec][ilFldIdx].NewLen > 0)
            {
              ilNewTime = prgRecFldDat[ipTbl][ipRec][ilFldIdx].NewVal + ilMinVal;
              pclSeconds[0] = prgRecFldDat[ipTbl][ipRec][ilFldIdx].NewDat[12];
              pclSeconds[1] = prgRecFldDat[ipTbl][ipRec][ilFldIdx].NewDat[13];
              pclSeconds[2] = 0x00;
              (void) FullTimeString (pclTimeVal, ilNewTime, pclSeconds);
              (void) SetNewValues (ipTbl, ipRec, ilDestFld, "", pclTimeVal);
              ilChgFlg = TRUE;
            }                   /* end if */
          }                     /* end if */
        }                       /* end if */
      }                         /* end if */
    }
    while ((ilFldNbr < ilFldCnt) && (ilChgFlg != TRUE));
    if (ilChgFlg != TRUE)
    {
      if (ilChkRcv == FALSE)
      {
        /* All Source Fields Are Empty */
        /* Check For ElseRule Not Yet Implemented */
        /* SO: HARD CODED */
        (void) SetNewValues (ipTbl, ipRec, ilDestFld, "", " ");
        /* dbg(DEBUG,"NOTHING FOUND, SO SET TO BLANK"); */
      }                         /* end if */
    }                           /* end if */
  }                             /* end if */
  return ilRC;
}                               /* end SetCalcTimes */

/********************************************************/
/********************************************************/
static int SetBasicData (int ipTbl, int ipRec, char *pcpTgtFldLst, char *pcpSrcFldLst)
{
  int ilRC = RC_SUCCESS;
  int ilGetRc;
  int ilTgtFld = 0;
  int ilRelFld = 0;
  int ilFldCnt = 0;
  int ilFldNbr = 0;
  int ilLstLen = 0;
  int ilTgtLen = 0;
  int ilCondRcvBlank = FALSE;
  char pclBasFld[8];
  char pclTblNam[16];
  char pclBasTbl[16];
  char pclFldNam[8];
  char pclFldLst[128];
  char pclTgtLst[128];

   TRACE_FIELD_OF_INTEREST(__LINE__,"SBD0");

  ilGetRc = CheckRuleCondition (pcpTgtFldLst, &ilCondRcvBlank, ipTbl, ipRec);

  (void) get_real_item (pclFldNam, pcpTgtFldLst, 2);
  ilRelFld = GetFieldIndex (ipTbl, pclFldNam);
  if ((ilGetRc == RC_SUCCESS) && (ilRelFld > 0))
  {
    if (prgRecFldDat[ipTbl][ipRec][ilRelFld].NewLen > 0)
    {
      (void) get_real_item (pclTblNam, pcpSrcFldLst, 1);
      pclTblNam[3] = 0x00;
      sprintf (pclBasTbl, "%s%s", pclTblNam, pcgBasExt);
      (void) get_real_item (pclBasFld, pcpSrcFldLst, 2);
      ilLstLen = 0;
      ilTgtLen = 0;
      ilFldCnt = field_count (pcpSrcFldLst);
      for (ilFldNbr = 3; ilFldNbr <= ilFldCnt; ilFldNbr++)
      {
        (void) get_real_item (pclFldNam, pcpSrcFldLst, ilFldNbr);
        StrgPutStrg (pclFldLst, &ilLstLen, pclFldNam, 0, -1, ",");
        (void) get_real_item (pclFldNam, pcpTgtFldLst, ilFldNbr);
        StrgPutStrg (pclTgtLst, &ilTgtLen, pclFldNam, 0, -1, ",");
      }                         /* end for */
      if (ilLstLen > 0)
      {
        ilLstLen--;
        pclFldLst[ilLstLen] = 0x00;
        ilTgtLen--;
        pclTgtLst[ilTgtLen] = 0x00;

   TRACE_FIELD_OF_INTEREST(__LINE__,"SBD1");

        ilGetRc =
          GetBasicData (pclBasTbl, pclBasFld, prgRecFldDat[ipTbl][ipRec][ilRelFld].NewDat, pclFldLst, pcgTmpBuf1, 1,
                        RC_NOT_FOUND);

   TRACE_FIELD_OF_INTEREST(__LINE__,"SBD2");

        if (ilGetRc != RC_FAIL)
        {
          ilFldCnt = field_count (pclTgtLst);
          for (ilFldNbr = 1; ilFldNbr <= ilFldCnt; ilFldNbr++)
          {
            (void) get_real_item (pclFldNam, pclTgtLst, ilFldNbr);
            ilTgtFld = GetFieldIndex (ipTbl, pclFldNam);
            if (ilTgtFld > 0)
            {
              (void) get_real_item (pcgTmpBuf3, pcgTmpBuf1, ilFldNbr);
              if ((ilGetRc == RC_SUCCESS) || (prgTblFldCfg[ipTbl][ilTgtFld].KeepEntry == FALSE))
              {
                SetNewValues (ipTbl, ipRec, ilTgtFld, "", pcgTmpBuf3);
              }                 /* end if */
            }                   /* end if */
          }                     /* end for */
          if (ilGetRc == RC_NOT_FOUND)
          {
            if (prgTblFldCfg[ipTbl][ilRelFld].WarnEntry == TRUE)
            {
              sprintf (pcgTmpBuf3, "%s,01,01,I,#", prgTblFldCfg[ipTbl][ilRelFld].CflWarnTkey);
              if (strcmp(prgTblFldCfg[ipTbl][ilRelFld].CflWarnTkey,"CF09") == 0)
              { /* +++ jim +++ 20001224 */
                sprintf (pcgTmpBuf3, "%s,01,01,CF09, ,USEU", prgTblFldCfg[ipTbl][ilRelFld].CflWarnTkey);
                dbg(DEBUG,"SetBasicData: pcgTmpBuf3:   <%s>",pcgTmpBuf3);
                dbg(DEBUG,"SetBasicData: pcpTgtFldLst: <%s>",pcpTgtFldLst);
                dbg(DEBUG,"SetBasicData: pcpSrcFldLst: <%s>",pcpSrcFldLst);
              }
              SendConflict (ipTbl, ipRec, ilRelFld, pcgTmpBuf3);
            }                   /* end if */
          }                     /* end if */
        }                       /* end if */
      }                         /* end if */
    }                           /* end if */
    else
    {
      /* BasicField is Empty */
      ;
    }                           /* end else */
  }                             /* end if */

   TRACE_FIELD_OF_INTEREST(__LINE__,"SBDE");

  return ilRC;
}                               /* end SetBasicData */

/********************************************************/
/********************************************************/
static int SetBasicColumn (int ipTbl, int ipRec, char *pcpTgtFldLst, char *pcpSrcFldLst)
{
  int ilRC = RC_SUCCESS;
  int ilGetRc;
  int ilRelFld = 0;
  int ilCondRcvBlank = FALSE;
  char pclBasFld[8];
  char pclSrcFld[8];
  char pclTgtFld[8];
  char pclTblNam[16];
  char pclBasTbl[16];

   TRACE_FIELD_OF_INTEREST(__LINE__,"SBCA");

  ilGetRc = CheckRuleCondition (pcpTgtFldLst, &ilCondRcvBlank, ipTbl, ipRec);

  (void) get_real_item (pclTgtFld, pcpTgtFldLst, 2);
  ilRelFld = GetFieldIndex (ipTbl, pclTgtFld);
  if ((ilGetRc == RC_SUCCESS) && (ilRelFld > 0))
  {
    if (prgRecFldDat[ipTbl][ipRec][ilRelFld].NewLen > 0)
    {
      (void) get_real_item (pclTblNam, pcpSrcFldLst, 1);
      pclTblNam[3] = 0x00;
      sprintf (pclBasTbl, "%s%s", pclTblNam, pcgBasExt);
      (void) get_real_item (pclBasFld, pcpSrcFldLst, 2);
      (void) get_real_item (pclSrcFld, pcpSrcFldLst, 3);

      ilGetRc =
        GetBasicData (pclBasTbl, pclBasFld, prgRecFldDat[ipTbl][ipRec][ilRelFld].NewDat, pclBasFld, pcgTmpBuf1, 1,
                      RC_NOT_FOUND);
      if (ilGetRc == RC_NOT_FOUND)
      {
        ilGetRc =
          GetBasicData (pclBasTbl, pclSrcFld, prgRecFldDat[ipTbl][ipRec][ilRelFld].NewDat, pclBasFld, pcgTmpBuf1, 1,
                        RC_NOT_FOUND);
      }                         /* end if */
      if (ilGetRc != RC_FAIL)
      {
        (void) get_real_item (pcgTmpBuf3, pcgTmpBuf1, 1);
        if ((ilGetRc == RC_SUCCESS) || (prgTblFldCfg[ipTbl][ilRelFld].KeepEntry == FALSE))
        {
          SetNewValues (ipTbl, ipRec, ilRelFld, "", pcgTmpBuf3);
        }                       /* end if */
        if (ilGetRc == RC_NOT_FOUND)
        {
          if (prgTblFldCfg[ipTbl][ilRelFld].WarnEntry == TRUE)
          {
            sprintf (pcgTmpBuf3, "%s,01,01,I,#", prgTblFldCfg[ipTbl][ilRelFld].CflWarnTkey);
            if (strcmp(prgTblFldCfg[ipTbl][ilRelFld].CflWarnTkey,"CF09") == 0)
            { /* +++ jim +++ 20001224 */
              sprintf (pcgTmpBuf3, "%s,01,01,CF09, ,USEU", prgTblFldCfg[ipTbl][ilRelFld].CflWarnTkey);
              dbg(DEBUG,"SetBasicData: pcgTmpBuf3:   <%s>",pcgTmpBuf3);
              dbg(DEBUG,"SetBasicData: pcpTgtFldLst: <%s>",pcpTgtFldLst);
              dbg(DEBUG,"SetBasicData: pcpSrcFldLst: <%s>",pcpSrcFldLst);
            }
            SendConflict (ipTbl, ipRec, ilRelFld, pcgTmpBuf3);
          }                     /* end if */
        }                       /* end if */
      }                         /* end if */
    }                           /* end if */
    else
    {
      /* BasicField is Empty */
      ;
    }                           /* end else */
  }                             /* end if */

   TRACE_FIELD_OF_INTEREST(__LINE__,"SBCE");

  return ilRC;
}                               /* end SetBasicColumn */

/********************************************************/
/********************************************************/
static int SetChangeInfo (int ipTbl, int ipRec, char *pcpTgtFldLst, char *pcpSrcFldLst)
{
  int ilRC = RC_SUCCESS;
  int ilDestFld = 0;
  int ilFldIdx = 0;
  int ilFldCnt = 0;
  int ilFldNbr = 0;
  int ilNamLen = 0;
  int ilChrPos = 0;
  int ilChgFlg = FALSE;
  char pclChgInf[32];
  char pclChgTxt[32];
  char pclFldNam[16];
  char pclTimeVal[16];
  strcpy (pclTimeVal, "00:00");

  if ((igReorgTable != TRUE) && (igChangeInfo == TRUE))
  {
    ilNamLen = get_real_item (pclFldNam, pcpTgtFldLst, 1);
    ilDestFld = GetFieldIndex (ipTbl, pclFldNam);
    if (ilDestFld > 0)
    {
      ilFldCnt = field_count (pcpSrcFldLst);
      do
      {
        ilFldNbr++;
        ilNamLen = get_real_item (pclFldNam, pcpSrcFldLst, ilFldNbr);
        ilFldNbr++;
        (void) get_real_item (pclChgTxt, pcpSrcFldLst, ilFldNbr);
        if (ilNamLen == 4)
        {
          ilFldIdx = GetFieldIndex (ipTbl, pclFldNam);
          if (prgRecFldDat[ipTbl][ipRec][ilFldIdx].NewFlg == TRUE)
          {
            if (prgRecFldDat[ipTbl][ipRec][ilFldIdx].NewLen > 0)
            {
              ilChrPos = 0;
              StrgPutStrg (pclTimeVal, &ilChrPos, prgRecFldDat[ipTbl][ipRec][ilFldIdx].NewDat, 8, 9, ":");
              StrgPutStrg (pclTimeVal, &ilChrPos, prgRecFldDat[ipTbl][ipRec][ilFldIdx].NewDat, 10, 11, "\0");
              pclTimeVal[5] = 0x00;
            }                   /* end if */
            else
            {
              strcpy (pclTimeVal, ".....");
            }                   /* end else */
            sprintf (pclChgInf, "%s=%s", pclChgTxt, pclTimeVal);
            (void) SetNewValues (ipTbl, ipRec, ilDestFld, "", pclChgInf);
            ilChgFlg = TRUE;
          }                     /* end if */
        }                       /* end if */
      }
      while ((ilFldNbr < ilFldCnt) && (ilChgFlg != TRUE));
      if (ilChgFlg != TRUE)
      {
        /* All Source Fields Are Empty */
        /* dbg(DEBUG,"CHGI: NOTHING CHANGED"); */
        /* (void)SetNewValues(ipTbl,ipRec,ilDestFld,"", " "); */
        ;
      }                         /* end if */
    }                           /* end if */
  }                             /* end if */
  return ilRC;
}                               /* end SetChangeInfo */

/********************************************************/
/********************************************************/
static int AutoFillField (int ipTbl, int ipRec, char *pcpTgtFldLst, char *pcpSrcFldLst)
{
  int ilRC = RC_SUCCESS;
  int ilValidRule = FALSE;
  int ilLen = 0;
  int ilKeyFld = 0;
  int ilTgtFld = 0;
  int ilRelFld = 0;
  int ilSrcFld = 0;
  int ilSrcVal = 0;
  char pclSeconds[8];
  char pclTgtFldNam[8];
  char pclFldNam[8];
  char pclChkKey[8];
  char pclActTyp[16];
  char pclActCmd[16];
  char pclActRel[16];
  char pclTmpBuf[128];
  char pclBasTbl[16];
  char pclBasFld[16];
  char pclBasVal[16];
  char pclFldLst[16];
  char pclTmpItm[16];
  /*
     dbg(TRACE,"AUTO_FILL: <%s> <%s>", pcpTgtFldLst, pcpSrcFldLst);
   */
  (void) get_real_item (pclFldNam, pcpTgtFldLst, 1);
  pclChkKey[0] = pclFldNam[4];
  pclFldNam[4] = 0x00;
  ilLen = get_real_item (pclTmpBuf, pcpTgtFldLst, 2);
  if (ilLen <= 0)
  {
    strcpy (pclTmpBuf, " ");
  }                             /* end if */
  ilKeyFld = GetFieldIndex (ipTbl, pclFldNam);
  if (ilKeyFld > 0)
  {
    switch (pclChkKey[0])
    {
      case '?':
        if (prgRecFldDat[ipTbl][ipRec][ilKeyFld].NewLen > 0)
        {
          ilValidRule = TRUE;
        }                       /* end if */
        break;
      case 'E':
        if (prgRecFldDat[ipTbl][ipRec][ilKeyFld].NewLen == 0)
        {
          ilValidRule = TRUE;
        }                       /* end if */
        break;
      default:
        if (strcmp (pclTmpBuf, prgRecFldDat[ipTbl][ipRec][ilKeyFld].NewDat) == 0)
        {
          ilValidRule = TRUE;
        }                       /* end if */
        break;
    }                           /* end switch */
    if (ilValidRule == TRUE)
    {
      /*
         dbg(TRACE,"CONDITION IS VALID");
       */
      (void) get_real_item (pclTgtFldNam, pcpTgtFldLst, 3);
      ilTgtFld = GetFieldIndex (ipTbl, pclTgtFldNam);
      (void) get_real_item (pclFldNam, pcpSrcFldLst, 1);
      ilSrcFld = GetFieldIndex (ipTbl, pclFldNam);
      (void) get_real_item (pclActCmd, pcpSrcFldLst, 2);
      (void) get_real_item (pclActRel, pcpSrcFldLst, 3);

      if (ilTgtFld > 0)
      {
        if ((strcmp (pclActCmd, "CLR") == 0) || (strcmp (pclFldNam, "CLR") == 0))
        {
          (void) SetNewValues (ipTbl, ipRec, ilTgtFld, pclTgtFldNam, " ");
          ilSrcFld = 0;
          ilTgtFld = 0;
        }                       /* end if */
      }                         /* end if */

      /* HardCoded */
      strcpy (pclActTyp, "TIME");

      if ((ilSrcFld > 0) && (ilTgtFld > 0))
      {
        if (strcmp (pclActTyp, "TIME") == 0)
        {
          /* dbg(TRACE,"ACTION TYPE IS <%s>", pclActTyp); */
          if (prgRecFldDat[ipTbl][ipRec][ilSrcFld].NewVal > 0)
          {
            if (strcmp (pclActRel, "VAL") == 0)
            {
              (void) get_real_item (pclTmpBuf, pcpSrcFldLst, 4);
              ilSrcVal = abs (atoi (pclTmpBuf));
            }                   /* end if */
            else if (strcmp (pclActRel, "BAS") == 0)
            {
              (void) get_real_item (pclTmpItm, pcpSrcFldLst, 4);
              pclTmpItm[3] = 0x00;
              sprintf (pclBasTbl, "%s%s", pclTmpItm, pcgBasExt);
              (void) get_real_item (pclBasFld, pcpSrcFldLst, 5);
              (void) get_real_item (pclFldNam, pcpSrcFldLst, 6);
              ilRelFld = GetFieldIndex (ipTbl, pclFldNam);
              strcpy (pclBasVal, prgRecFldDat[ipTbl][ipRec][ilRelFld].NewDat);
              (void) get_real_item (pclFldLst, pcpSrcFldLst, 7);
              (void) GetBasicData (pclBasTbl, pclBasFld, pclBasVal, pclFldLst, pcgTmpBuf1, 1, RC_FAIL);
              /*
                 dbg(TRACE,"FOUND VALUE <%s> IS <%s>",pclFldLst,pcgTmpBuf1);
               */
              ilSrcVal = abs (atoi (pcgTmpBuf1));
              if (ilSrcVal < 15)
              {
                ilSrcVal = -1;
              }                 /* end if */
            }                   /* end else if */
            else
            {
              /*
                 dbg(TRACE,"UNKNOWN OPERATOR <%s>", pclActRel);
               */
              ilSrcVal = -1;
            }                   /* end else */

            if (ilSrcVal >= 0)
            {
              if (strcmp (pclActCmd, "ADD") == 0)
              {
                ilSrcVal += prgRecFldDat[ipTbl][ipRec][ilSrcFld].NewVal;
              }
              else if (strcmp (pclActCmd, "SUB") == 0)
              {
                ilSrcVal = prgRecFldDat[ipTbl][ipRec][ilSrcFld].NewVal - ilSrcVal;
              }                 /* end else if */
              pclSeconds[0] = prgRecFldDat[ipTbl][ipRec][ilSrcFld].NewDat[12];
              pclSeconds[1] = prgRecFldDat[ipTbl][ipRec][ilSrcFld].NewDat[13];
              pclSeconds[2] = 0x00;
              (void) FullTimeString (pclTmpBuf, ilSrcVal, pclSeconds);
              (void) SetNewValues (ipTbl, ipRec, ilTgtFld, "", pclTmpBuf);
            }                   /* end if */
          }                     /* end if */
        }                       /* end if */
      }                         /* end if */
    }                           /* end if */
  }                             /* end if */
  return ilRC;
}                               /* end AutoFillField */

/********************************************************/
/********************************************************/
static int BasicLookup (int ipTbl, int ipRec, char *pcpTgtFldLst, char *pcpSrcFldLst)
{
  int ilRC = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  int ilSrcCnt = 0;
  int ilSrcItm = 0;
  int ilMapCnt = 0;
  int ilMapItm = 0;
  int ilSetCnt = 0;
  int ilSetItm = 0;
  int ilKeyFld = 0;
  int ilTgtFld = 0;
  int ilSrcFld = 0;
  int ilLen = 0;
  char pclSrcCpy[512];
  char pclChkNam[8];
  char pclTgtNam[8];
  char pclSrcNam[8];
  char pclTmpBuf[128];
  char pclTmpVal[16];
  char pclResult[16];
  char pclBasTbl[16];
  char pclBasFld[16];
  char pclBasVal[16];
  char pclFldLst[16];
  char *pclBasPtr = NULL;
  char *pclMapPtr = NULL;
  char *pclSetPtr = NULL;

  dbg (DEBUG, "BASIC_LOOKUP: <%s> <%s>", pcpTgtFldLst, pcpSrcFldLst);
  strcpy (pclSrcCpy, pcpSrcFldLst);
  pclBasPtr = strstr (pclSrcCpy, ",BAS,");
  pclMapPtr = strstr (pclSrcCpy, ",MAP,");
  pclSetPtr = strstr (pclSrcCpy, ",SET,");
  if (pclBasPtr != NULL)
  {
    *pclBasPtr = 0x00;
    pclBasPtr += 5;
  }                             /* end if */
  if (pclMapPtr != NULL)
  {
    *pclMapPtr = 0x00;
    pclMapPtr += 5;
  }                             /* end if */
  if (pclSetPtr != NULL)
  {
    *pclSetPtr = 0x00;
    pclSetPtr += 5;
  }                             /* end if */

  /*
     dbg(TRACE,"SRC <%s>",pclSrcCpy);
     if (pclBasPtr != NULL)
     {
     dbg(TRACE,"BAS <%s>",pclBasPtr);
     }
   */

  if (pclMapPtr != NULL)
  {
    /* dbg(TRACE,"MAP <%s>",pclMapPtr); */
    ilMapCnt = field_count (pclMapPtr);
  }                             /* end if */

  if (pclSetPtr != NULL)
  {
    /* dbg(TRACE,"SET <%s>",pclSetPtr); */
    ilSetCnt = field_count (pclSetPtr);
  }                             /* end if */

  (void) get_real_item (pclChkNam, pcpTgtFldLst, 1);
  (void) get_real_item (pclTmpBuf, pcpTgtFldLst, 2);
  ilKeyFld = GetFieldIndex (ipTbl, pclChkNam);
  if (ilKeyFld > 0)
  {
    if (strcmp (pclTmpBuf, prgRecFldDat[ipTbl][ipRec][ilKeyFld].NewDat) == 0)
    {
      /* dbg(TRACE,"CONDITION IS VALID"); */
      (void) get_real_item (pclTgtNam, pcpTgtFldLst, 3);
      ilTgtFld = GetFieldIndex (ipTbl, pclTgtNam);
      if (ilTgtFld > 0)
      {
        (void) get_real_item (pclBasTbl, pclBasPtr, 1);
        strcpy (&pclBasTbl[3], pcgBasExt);
        (void) get_real_item (pclBasFld, pclBasPtr, 2);
        (void) get_real_item (pclFldLst, pclBasPtr, 3);
        /* dbg(TRACE,"<%s> <%s> <%s>",pclBasTbl,pclBasFld,pclFldLst); */

        ilSrcCnt = field_count (pclSrcCpy);
        /* dbg(TRACE,"%d SRC FIELDS",ilSrcCnt); */
        pclResult[0] = 0x00;
        for (ilSrcItm = 1; ilSrcItm <= ilSrcCnt; ilSrcItm++)
        {
          (void) get_real_item (pclSrcNam, pclSrcCpy, ilSrcItm);
          ilSrcFld = GetFieldIndex (ipTbl, pclSrcNam);
          if (ilSrcFld > 0)
          {
            if (prgRecFldDat[ipTbl][ipRec][ilSrcFld].NewLen > 0)
            {
              strcpy (pclBasVal, prgRecFldDat[ipTbl][ipRec][ilSrcFld].NewDat);
              /* dbg(TRACE,"SRC FLD: <%s> <%s>",pclSrcNam,pclBasVal); */

              ilGetRc = GetBasicData (pclBasTbl, pclBasFld, pclBasVal, pclFldLst, pclTmpVal, 1, RC_FAIL);
              if (ilGetRc == RC_SUCCESS)
              {
                /* dbg(TRACE,"BASIC VALUE <%s>",pclTmpVal); */
                if (pclMapPtr != NULL)
                {
                  ilMapItm = 1;
                  while (ilMapItm < ilMapCnt)
                  {

                    (void) get_real_item (pclTmpBuf, pclMapPtr, ilMapItm);
                    ilMapItm++;
                    if (strstr (pclTmpBuf, pclTmpVal) != NULL)
                    {

                      (void) get_real_item (pclTmpVal, pclMapPtr, ilMapItm);
                      /* dbg(TRACE,"MAPPED VALU <%s>",pclTmpVal); */
                      ilMapItm += ilMapCnt;
                    }           /* end if */
                    ilMapItm++;
                  }             /* end while */
                  strcat (pclResult, pclTmpVal);
                }               /* end if */
              }                 /* end if */
            }                   /* end if */
          }                     /* end if */
          else
          {
            dbg (TRACE, "BASIC_LOOKUP ERROR: FIELD <%s> DOESN'T EXIST", pclSrcNam);
          }                     /* end else */
        }                       /* end for */
        /* dbg(TRACE,"RESULT BEFORE SET <%s>",pclResult); */
        if (pclSetPtr != NULL)
        {
          ilSetItm = 1;
          while (ilSetItm < ilSetCnt)
          {
            (void) get_real_item (pclTmpBuf, pclSetPtr, ilSetItm);
            ilSetItm++;
            if (strcmp (pclTmpBuf, pclResult) == 0)
            {
              (void) get_real_item (pclResult, pclSetPtr, ilSetItm);
              /* dbg(TRACE,"SET TO VALU <%s>",pclResult); */
              ilSetItm += ilSetCnt;
            }                   /* end if */
            ilSetItm++;
          }                     /* end while */
        }                       /* end if */
        /* dbg(TRACE,"FINAL RESULT <%s>",pclResult); */
        ilLen = strlen (pclResult);
        if (prgTblFldCfg[ipTbl][ilTgtFld].FieldLength < ilLen)
        {
          ilLen = prgTblFldCfg[ipTbl][ilTgtFld].FieldLength;
          pclResult[ilLen] = 0x00;
          /* dbg(TRACE,"RESULT DATA CUT <%s>",pclResult); */
        }                       /* end if */
        (void) SetNewValues (ipTbl, ipRec, ilTgtFld, "", pclResult);
      }                         /* end if */
      else
      {
        dbg (TRACE, "BASIC_LOOKUP ERROR: FIELD <%s> DOESN'T EXIST", pclTgtNam);
      }                         /* end else */
    }                           /* end if */
  }                             /* end if */
  return ilRC;
}                               /* end BasicLookup */

/********************************************************/
/********************************************************/
static int DayCheckField (int ipTbl, int ipRec, char *pcpTgtFldLst, char *pcpSrcFldLst)
{
  int ilRC = RC_SUCCESS;
  int ilFld = 0;
  int ilFldCnt = 0;
  int ilKeyFld = 0;
  int ilActFld = 0;
  int ilLimVal = 0;
  int ilNewVal = 0;
  int ilMinDif = 0;
  char pclSeconds[8];
  char pclFldNam[8];
  char pclTmpBuf[32];
  if ((strcmp (pcgRecvName, "SITA") == 0) || (strcmp (pcgRecvName, "EXCO") == 0))
  {
    /* dbg(TRACE,"DAY_CHECK: <%s> <%s>", pcpTgtFldLst, pcpSrcFldLst); */
    (void) get_real_item (pclFldNam, pcpSrcFldLst, 1);
    /* dbg(TRACE,"CHECKING AGAINST <%s>",pclFldNam); */
    ilKeyFld = GetFieldIndex (ipTbl, pclFldNam);
    if (ilKeyFld > 0)
    {
      if (prgRecFldDat[ipTbl][ipRec][ilKeyFld].NewVal > 0)
      {
        (void) get_real_item (pclTmpBuf, pcpSrcFldLst, 2);
        ilLimVal = abs (atoi (pclTmpBuf));
        if (ilLimVal <= 0)
        {
          ilLimVal = 60;
        }                       /* end if */
        ilFldCnt = field_count (pcpTgtFldLst);
        for (ilFld = 1; ilFld <= ilFldCnt; ilFld++)
        {
          (void) get_real_item (pclFldNam, pcpTgtFldLst, ilFld);
          /* dbg(TRACE,"CHECKING FIELD <%s>",pclFldNam); */
          ilActFld = GetFieldIndex (ipTbl, pclFldNam);
          if (ilActFld > 0)
          {
            if (prgRecFldDat[ipTbl][ipRec][ilActFld].NewVal > 0)
            {
              ilMinDif = prgRecFldDat[ipTbl][ipRec][ilKeyFld].NewVal - prgRecFldDat[ipTbl][ipRec][ilActFld].NewVal;
              if (ilMinDif > ilLimVal)
              {
                /* dbg(TRACE,"TIME TOO EARLY: SHIFT TO NEXT DAY"); */
                ilNewVal = prgRecFldDat[ipTbl][ipRec][ilActFld].NewVal + 1440;
                pclSeconds[0] = prgRecFldDat[ipTbl][ipRec][ilActFld].NewDat[12];
                pclSeconds[1] = prgRecFldDat[ipTbl][ipRec][ilActFld].NewDat[13];
                pclSeconds[2] = 0x00;
                (void) FullTimeString (pclTmpBuf, ilNewVal, pclSeconds);
                (void) SetNewValues (ipTbl, ipRec, ilActFld, "", pclTmpBuf);
              }                 /* end if */
            }                   /* end if */
          }                     /* end if */
        }                       /* end for */
      }                         /* end if */
    }                           /* end if */
  }                             /* end if */
  return ilRC;
}                               /* end DayCheckField */

/********************************************************/
/********************************************************/
static int CheckBasicData (int ipTbl, int ipRec, char *pcpTgtFldLst, char *pcpSrcFldLst)
{
  int ilRC = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  int ilTgtFld = 0;
  int ilRelFld = 0;
  int ilFstFld = 0;
  int ilFldCnt = 0;
  int ilFldNbr = 0;
  int ilLstLen = 0;
  int ilTgtLen = 0;
  int ilNamLen = 0;
  int ilRcvOneFld = FALSE;
  int ilRcvOneVal = FALSE;
  int ilCondRcvBlank = FALSE;
  char pclBasFld[8];
  char pclBasVal[128];
  char pclBasTbl[16];
  char pclTblNam[16];
  char pclFldNam[8];
  char pclChkTyp[8];
  char pclFldLst[256];
  char pclTgtLst[256];

  strcpy (pclBasFld, "");
  strcpy (pclBasVal, "");
  strcpy (pclChkTyp, " ");

  ilGetRc = CheckRuleCondition (pcpTgtFldLst, &ilCondRcvBlank, ipTbl, ipRec);

  if (ilGetRc == RC_SUCCESS)
  {
    ilLstLen = 0;
    ilTgtLen = 0;
    (void) get_real_item (pclTblNam, pcpSrcFldLst, 1);
    pclTblNam[3] = 0x00;
    sprintf (pclBasTbl, "%s%s", pclTblNam, pcgBasExt);
    ilFstFld = 2;
    ilFldCnt = field_count (pcpTgtFldLst);
    ilRelFld = 0;
    ilRcvOneFld = FALSE;
    ilRcvOneVal = FALSE;
    for (ilFldNbr = ilFstFld; ilFldNbr <= ilFldCnt; ilFldNbr++)
    {
      ilNamLen = get_real_item (pclFldNam, pcpTgtFldLst, ilFldNbr);
      if (ilNamLen > 4)
      {
        pclChkTyp[0] = pclFldNam[4];
        pclFldNam[4] = 0x00;
        if (ilRelFld == 0)
        {
          ilTgtFld = GetFieldIndex (ipTbl, pclFldNam);
          if (ilTgtFld > 0)
          {
            if (prgRecFldDat[ipTbl][ipRec][ilTgtFld].RcvFlg == TRUE)
            {
              ilRcvOneFld = TRUE;
              if (prgRecFldDat[ipTbl][ipRec][ilTgtFld].RcvLen > 0)
              {
                ilRcvOneVal = TRUE;
              }                 /* end if */
            }                   /* end if */
            switch (pclChkTyp[0])
            {
              case 'R':
                if (prgRecFldDat[ipTbl][ipRec][ilTgtFld].RcvLen > 0)
                {
                  strcpy (pclBasVal, prgRecFldDat[ipTbl][ipRec][ilTgtFld].RcvDat);
                  ilRelFld = ilTgtFld;
                }               /* end if */
                break;
              case 'N':
                if ((prgRecFldDat[ipTbl][ipRec][ilTgtFld].NewFlg == TRUE) &&
                    (prgRecFldDat[ipTbl][ipRec][ilTgtFld].NewLen > 0))
                {
                  strcpy (pclBasVal, prgRecFldDat[ipTbl][ipRec][ilTgtFld].NewDat);
                  ilRelFld = ilTgtFld;
                }               /* end if */
                break;
              case '?':
                if (prgRecFldDat[ipTbl][ipRec][ilTgtFld].NewLen > 0)
                {
                  strcpy (pclBasVal, prgRecFldDat[ipTbl][ipRec][ilTgtFld].NewDat);
                  ilRelFld = ilTgtFld;
                }               /* end if */
                break;
              default:
                break;
            }                   /* end switch */
            if ((ilRelFld == 0) && (ilCondRcvBlank == TRUE) && (prgRecFldDat[ipTbl][ipRec][ilTgtFld].NewLen > 0))
            {
              strcpy (pclBasVal, prgRecFldDat[ipTbl][ipRec][ilTgtFld].NewDat);
              ilRelFld = ilTgtFld;
            }                   /* end if */
            if (ilRelFld > 0)
            {
              (void) get_real_item (pclBasFld, pcpSrcFldLst, ilFldNbr);
            }                   /* end if */
          }                     /* end if */
        }                       /* end if */
      }                         /* end if */
      StrgPutStrg (pclTgtLst, &ilTgtLen, pclFldNam, 0, -1, ",");
      ilNamLen = get_real_item (pclFldNam, pcpSrcFldLst, ilFldNbr);
      if (ilNamLen == 4)
      {
        StrgPutStrg (pclFldLst, &ilLstLen, pclFldNam, 0, -1, ",");
      }                         /* end if */
    }                           /* end for */

    if (ilLstLen > 0)
    {
      ilLstLen--;
    }                           /* end if */
    if (ilTgtLen > 0)
    {
      ilTgtLen--;
    }                           /* end if */
    pclFldLst[ilLstLen] = 0x00;
    pclTgtLst[ilTgtLen] = 0x00;

    if (ilRelFld > 0)
    {
      /* dbg(TRACE,"CROSS_CHECK: BASIC <%s> <%s>",pclBasFld,pclBasVal); */
      ilGetRc = GetBasicData (pclBasTbl, pclBasFld, pclBasVal, pclFldLst, pcgTmpBuf1, 1, RC_SUCCESS);
    }                           /* end if */
    else
    {
      /* BasicFields are empty or unchanged */
      ilGetRc = RC_FAIL;
      if (ilRcvOneFld == TRUE)
      {
        /* We received at least one of the BasicFields */
        if (ilRcvOneVal == FALSE)
        {
          /* All received data are empty */
          /* So we build an empty buffer for TargetValues */
          pcgTmpBuf1[0] = 0x00;
          ilGetRc = RC_SUCCESS;
          /* dbg(TRACE,"CROSS_CHECK: CLEAR ALL TARGET VALUES"); */
        }                       /* end if */
      }                         /* end if */
    }                           /* end else */

    if (ilGetRc == RC_SUCCESS)
    {
      ilFldCnt = field_count (pclTgtLst);
      for (ilFldNbr = 1; ilFldNbr <= ilFldCnt; ilFldNbr++)
      {
        (void) get_real_item (pclFldNam, pclTgtLst, ilFldNbr);
        ilTgtFld = GetFieldIndex (ipTbl, pclFldNam);
        if (ilTgtFld > 0)
        {
          (void) get_real_item (pcgTmpBuf3, pcgTmpBuf1, ilFldNbr);
          ilGetRc = SetNewValues (ipTbl, ipRec, ilTgtFld, "", pcgTmpBuf3);
        }                       /* end if */
      }                         /* end for */
    }                           /* end if */
    else
    {
      /* dbg(TRACE,"CROSS_CHECK: NOTHING CHANGED, NOTHING TO DO"); */
      ;
    }                           /* end else */

  }                             /* end if */
  return ilRC;
}                               /* end CheckBasicData */

/********************************************************/
/********************************************************/
static int CheckRuleCondition (char *pcpTgtFldLst, int *ipCondRcvBlank, int ipTbl, int ipRec)
{
  int ilRC = RC_FAIL;
  int ilCndFldIdx = -1;
  int ilRelFldLen;
  int ilChrPos = 0;
  char pclCndFldNam[8];
  char pclCondLine[512];
  char pclCondTyp[8];

  ilRC = RC_FAIL;
  *ipCondRcvBlank = FALSE;
  ilRelFldLen = get_real_item (pclCondLine, pcpTgtFldLst, 1);
  if (ilRelFldLen > 4)
  {
    /* There is a Condition To Check First */
    ilChrPos = 0;
    StrgPutStrg (pclCndFldNam, &ilChrPos, pclCondLine, 0, 3, "");
    pclCndFldNam[4] = 0x00;
    ilCndFldIdx = GetFieldIndex (ipTbl, pclCndFldNam);
    if (ilCndFldIdx > 0)
    {
      ilChrPos = 0;
      StrgPutStrg (pclCondTyp, &ilChrPos, pclCondLine, 4, 5, "");
      pclCondTyp[2] = 0x00;
      switch (pclCondTyp[0])
      {
        case 'R':
          if (prgRecFldDat[ipTbl][ipRec][ilCndFldIdx].RcvFlg == TRUE)
          {
            ilRC = RC_SUCCESS;
          }                     /* end if */
          break;
        case 'N':
          if (prgRecFldDat[ipTbl][ipRec][ilCndFldIdx].NewFlg == TRUE)
          {
            ilRC = RC_SUCCESS;
          }                     /* end if */
          break;
        case '?':
          if (prgRecFldDat[ipTbl][ipRec][ilCndFldIdx].NewLen > 0)
          {
            ilRC = RC_SUCCESS;
          }                     /* end if */
          break;
        case '!':
          if (prgRecFldDat[ipTbl][ipRec][ilCndFldIdx].NewLen < 1)
          {
            ilRC = RC_SUCCESS;
          }                     /* end if */
          break;
        case 'E':
          if (prgRecFldDat[ipTbl][ipRec][ilCndFldIdx].NewLen < 1)
          {
            ilRC = RC_SUCCESS;
            *ipCondRcvBlank = prgRecFldDat[ipTbl][ipRec][ilCndFldIdx].NewFlg;
          }                     /* end if */
          break;
        default:
          break;
      }                         /* end switch */
      /*
         if (ilRC == RC_SUCCESS)
         {
         dbg(DEBUG,"CONDITION FLD <%s> TYP <%s> IS TRUE",pclCndFldNam,pclCondTyp);
         }
         else
         {
         dbg(DEBUG,"CONDITION FLD <%s> TYP <%s> IS FALSE",pclCndFldNam,pclCondTyp);
         }
       */
    }                           /* end if */
  }                             /* end if */
  else
  {
    if (ilRelFldLen == 4)
    {
      if (strcmp (pclCondLine, "ARR?") == 0)
      {
        ilCndFldIdx = GetFieldIndex (ipTbl, "DES3");
        if (strcmp (prgRecFldDat[ipTbl][ipRec][ilCndFldIdx].NewDat, pcgH3LC) == 0)
        {
          ilRC = RC_SUCCESS;
        }                       /* end if */
      }                         /* end if */
      if (strcmp (pclCondLine, "DEP?") == 0)
      {
        ilCndFldIdx = GetFieldIndex (ipTbl, "ORG3");
        if (strcmp (prgRecFldDat[ipTbl][ipRec][ilCndFldIdx].NewDat, pcgH3LC) == 0)
        {
          ilRC = RC_SUCCESS;
        }                       /* end if */
      }                         /* end if */
      if (strcmp (pclCondLine, "TOW?") == 0)
      {
        ilCndFldIdx = GetFieldIndex (ipTbl, "FTYP");
        if (strcmp (prgRecFldDat[ipTbl][ipRec][ilCndFldIdx].NewDat, "T") == 0)
        {
          ilRC = RC_SUCCESS;
        }                       /* end if */
      }                         /* end if */
      if (strcmp (pclCondLine, "GRM?") == 0)
      {
        ilCndFldIdx = GetFieldIndex (ipTbl, "FTYP");
        if (strcmp (prgRecFldDat[ipTbl][ipRec][ilCndFldIdx].NewDat, "G") == 0)
        {
          ilRC = RC_SUCCESS;
        }                       /* end if */
      }                         /* end if */
    }                           /* end if */
    else
    {
      ilRC = RC_SUCCESS;
    }                           /* end else */
  }                             /* end else */
  return ilRC;
}                               /* end CheckRuleCondition */

/********************************************************/
/********************************************************/
static int SetNewValues (int ipTbl, int ipRec, int ipFldIdx, char *pcpFldNam, char *pcpNewVal)
{
  int ilRC = RC_SUCCESS;
  int ilFld = 0;
  int ilActFld = 0;
  int ilDssf = 0;
  int ilDssfLen = 0;
  int ilDssfChr = 0;
  int ilCheckDssf = TRUE;
  ilFld = ipFldIdx;
  if (ilFld < 1)
  {
    ilFld = GetFieldIndex (ipTbl, pcpFldNam);
  }                             /* end if */

  if (pcpFldNam != NULL)
  {
    if (strlen (pcpFldNam) > 4)
    {
      if (pcpFldNam[4] == '!')
      {
        ilCheckDssf = FALSE;
      }                         /* end if */
    }                           /* end if */
  }                             /* end if */

  if (ilFld > 0)
  {
    ilActFld = ilFld;
    /* CHECK DATA SOURCE PRIORITY */
    if (prgTblFldCfg[ipTbl][ilFld].CtrlFlagNbr >= 0)
    {
      ilDssf = GetFieldIndex (ipTbl, "DSSF");
      ilDssfChr = prgTblFldCfg[ipTbl][ilFld].CtrlFlagNbr;
      ilDssfLen = prgRecFldDat[ipTbl][ipRec][ilDssf].NewLen;
      if (ilDssfLen < 1)
      {
        ilDssfLen = 1;
      }                         /* end if */
      while (ilDssfChr >= ilDssfLen)
      {
        prgRecFldDat[ipTbl][ipRec][ilDssf].NewDat[ilDssfLen] = ' ';
        ilDssfLen++;
      }                         /* end while */
      prgRecFldDat[ipTbl][ipRec][ilDssf].NewDat[ilDssfLen] = '\0';
      prgRecFldDat[ipTbl][ipRec][ilDssf].NewLen = ilDssfLen;
      /*
         dbg(TRACE,"DSSF CTRL FIELD DETECTED <%s> POS %d IN <%s>",
         prgTblFldCfg[ipTbl][ilFld].SysTabFina, ilDssfChr,
         prgRecFldDat[ipTbl][ipRec][ilDssf].NewDat);
       */

      if (((prgRecFldDat[ipTbl][ipRec][ilFld].RcvFlg == TRUE) && (igClientAction == TRUE)) || (igUnlockDssf == TRUE) ||
          (ilCheckDssf == FALSE))
      {
        /*
           dbg(TRACE,"VALUE RECEIVED FROM CLIENT <%s>",
           prgRecFldDat[ipTbl][ipRec][ilFld].RcvDat);
         */
        if ((prgRecFldDat[ipTbl][ipRec][ilFld].RcvLen > 0) && (ilCheckDssf == TRUE))
        {
          prgRecFldDat[ipTbl][ipRec][ilDssf].NewDat[ilDssfChr] = 'U';
          ilFld = 0;
        }                       /* end if */
        else
        {
          prgRecFldDat[ipTbl][ipRec][ilDssf].NewDat[ilDssfChr] = ' ';
        }                       /* end else */
      }                         /* end if */
      else
      {
        if (prgRecFldDat[ipTbl][ipRec][ilDssf].NewDat[ilDssfChr] == 'U')
        {
          dbg (DEBUG, "FIELD <%s> IS PROTECTED", prgTblFldCfg[ipTbl][ilActFld].SysTabFina);
          ilFld = 0;
        }                       /* end if */
      }                         /* end else */
      str_trm_rgt (prgRecFldDat[ipTbl][ipRec][ilDssf].NewDat, " ", TRUE);
      prgRecFldDat[ipTbl][ipRec][ilDssf].NewLen = strlen (prgRecFldDat[ipTbl][ipRec][ilDssf].NewDat);
      if (prgRecFldDat[ipTbl][ipRec][ilDssf].NewLen < 1)
      {
        strcpy (prgRecFldDat[ipTbl][ipRec][ilDssf].NewDat, " ");
      }                         /* end if */
      if (strcmp (prgRecFldDat[ipTbl][ipRec][ilDssf].NewDat, prgRecFldDat[ipTbl][ipRec][ilDssf].DbsDat) != 0)
      {
        prgRecFldDat[ipTbl][ipRec][ilDssf].NewFlg = TRUE;
      }                         /* end if */
    }                           /* end if */
  }                             /* end if */

  if (ilFld > 0)
  {
    prgRecFldDat[ipTbl][ipRec][ilFld].NewLen = get_real_item (prgRecFldDat[ipTbl][ipRec][ilFld].NewDat, pcpNewVal, 1);
    if (prgRecFldDat[ipTbl][ipRec][ilFld].NewLen < 1)
    {
      strcpy (prgRecFldDat[ipTbl][ipRec][ilFld].NewDat, prgTblFldCfg[ipTbl][ilFld].SysTabDefa);
      prgRecFldDat[ipTbl][ipRec][ilFld].NewLen = strlen (prgRecFldDat[ipTbl][ipRec][ilFld].NewDat);
      if (prgRecFldDat[ipTbl][ipRec][ilFld].NewLen < 1)
      {
        strcpy (prgRecFldDat[ipTbl][ipRec][ilFld].NewDat, " ");
      }                         /* end if */
    }                           /* end if */
    (void) CheckFieldValue (prgTblFldCfg[ipTbl][ilFld].FieldType, prgTblFldCfg[ipTbl][ilFld].FieldLength, FALSE,
                            prgRecFldDat[ipTbl][ipRec][ilFld].NewDat, &prgRecFldDat[ipTbl][ipRec][ilFld].NewVal);

    if (strcmp (prgRecFldDat[ipTbl][ipRec][ilFld].NewDat, prgRecFldDat[ipTbl][ipRec][ilFld].DbsDat) != 0)
    {
      if (prgRecFldDat[ipTbl][ipRec][ilFld].NewFlg != TRUE)
      {
        prgRecFldDat[ipTbl][ipRec][ilFld].NewFlg = TRUE;
        prgPrjTblCfg[ipTbl].DatChgCnt++;
      }                         /* end if */
    }                           /* end if */
    else
    {
      prgRecFldDat[ipTbl][ipRec][ilFld].NewFlg = FALSE;
    }                           /* end else */

  }                             /* end if */
  return ilRC;
}                               /* end SetNewValues */

/********************************************************/
/********************************************************/
static int SetRcvValues (int ipTbl, int ipFldIdx, char *pcpFldNam, char *pcpRcvVal)
{
  int ilRC = RC_SUCCESS;
  int ilFld = 0;
  ilFld = ipFldIdx;
  if (ilFld < 1)
  {
    ilFld = GetFieldIndex (ipTbl, pcpFldNam);
  }                             /* end if */
  if (ilFld > 0)
  {
    prgTblFldCfg[ipTbl][ilFld].RcvLen = get_real_item (prgTblFldCfg[ipTbl][ilFld].RcvDat, pcpRcvVal, 1);
    if (prgTblFldCfg[ipTbl][ilFld].RcvLen < 1)
    {
      strcpy (prgTblFldCfg[ipTbl][ilFld].RcvDat, prgTblFldCfg[ipTbl][ilFld].SysTabDefa);
      prgTblFldCfg[ipTbl][ilFld].RcvLen = strlen (prgTblFldCfg[ipTbl][ilFld].RcvDat);
      if (prgTblFldCfg[ipTbl][ilFld].RcvLen < 1)
      {
        strcpy (prgTblFldCfg[ipTbl][ilFld].RcvDat, " ");
      }                         /* end if */
    }                           /* end if */
    (void) CheckFieldValue (prgTblFldCfg[ipTbl][ilFld].FieldType, prgTblFldCfg[ipTbl][ilFld].FieldLength, FALSE,
                            prgTblFldCfg[ipTbl][ilFld].RcvDat, &prgTblFldCfg[ipTbl][ilFld].RcvVal);
    prgTblFldCfg[ipTbl][ilFld].RcvFlg = TRUE;
  }                             /* end if */
  return ilRC;
}                               /* end SetRcvValues */

/********************************************************/
/********************************************************/
static int UnSetRcvValues (int ipTbl, char *pcpFldLst)
{
  int ilRC = RC_SUCCESS;
  int ilLen = 0;
  int ilFldCnt = 0;
  int ilFldNbr = 0;
  int ilFldIdx = 0;
  char pclFldNam[8];
  ilFldCnt = field_count (pcpFldLst);
  for (ilFldNbr = 1; ilFldNbr <= ilFldCnt; ilFldNbr++)
  {
    ilLen = get_real_item (pclFldNam, pcpFldLst, ilFldNbr);
    if (ilLen > 0)
    {
      ilFldIdx = GetFieldIndex (ipTbl, pclFldNam);
      if (ilFldIdx > 0)
      {
        prgTblFldCfg[ipTbl][ilFldIdx].RcvFlg = FALSE;
      }                         /* end if */
    }                           /* end if */
  }                             /* end for */
  return ilRC;
}                               /* end UnSetRcvValues */

/********************************************************/
/********************************************************/
static void UnSetNewValues (int ipTbl, int ipRec, int ipCount, char *pcpFldLst, int ipWhenFilled)
{
  int ilFld = 0;
  int ilFldCnt = 0;
  int ilFldIdx = 0;
  char pclFldNam[8];
  ilFldCnt = ipCount;
  if (ilFldCnt < 1)
  {
    ilFldCnt = field_count (pcpFldLst);
  }                             /* end if */
  for (ilFld = 1; ilFld <= ilFldCnt; ilFld++)
  {
    get_real_item (pclFldNam, pcpFldLst, ilFld);
    ilFldIdx = GetFieldIndex (ipTbl, pclFldNam);
    if (ilFldIdx > 0)
    {
      if (prgRecFldDat[ipTbl][ipRec][ilFldIdx].NewFlg == TRUE)
      {
        if ((ipWhenFilled == TRUE) || ((ipWhenFilled == FALSE) && (prgRecFldDat[ipTbl][ipRec][ilFldIdx].NewLen == 0)))
        {
          if (ipWhenFilled == TRUE)
          {
            dbg (DEBUG, "FIELD <%s> NEW VALUE REJECTED <%s>", pclFldNam, prgRecFldDat[ipTbl][ipRec][ilFldIdx].NewDat);
          }                     /* end if */
          else
          {
            dbg (TRACE, "EMPTY FIELD <%s> IGNORED", pclFldNam);
          }                     /* end else */
          strcpy (prgRecFldDat[ipTbl][ipRec][ilFldIdx].NewDat, prgRecFldDat[ipTbl][ipRec][ilFldIdx].DbsDat);
          prgRecFldDat[ipTbl][ipRec][ilFldIdx].NewLen = prgRecFldDat[ipTbl][ipRec][ilFldIdx].DbsLen;
          prgRecFldDat[ipTbl][ipRec][ilFldIdx].NewVal = prgRecFldDat[ipTbl][ipRec][ilFldIdx].DbsVal;
          prgRecFldDat[ipTbl][ipRec][ilFldIdx].NewFlg = FALSE;
        }                       /* end if */
      }                         /* end if */
    }                           /* end if */
  }                             /* end for */
  return;
}                               /* end UnSetNewValues */

/********************************************************/
/********************************************************/
static int CollectChanges (int ipTbl, int ipRec)
{
  int ilRC = RC_SUCCESS;
  int ilSqlFldLen = 0;
  int ilSqlDatLen = 0;
  int ilOutFldLen = 0;
  int ilOutDatLen = 0;
  int ilOldDatLen = 0;
  int ilFld = -1;
  int ilFldCnt = -1;
  int ilUpdFldCnt = 0;
  char pclFldData[2048];
/*********  TWE 211299  *********/
  int ilOrg3 = -1;
  int ilDes3 = -1;
  int ilJcnt = -1;
  int ilJfno = -1;

  /* Rundflüge in Swissport Interface Files erlaubt !!!  */
  if (igTWERoundtrip == TRUE)
  {
    ilOrg3 = GetFieldIndex (ipTbl, "ORG3");
    ilDes3 = GetFieldIndex (ipTbl, "DES3");
    strcpy (prgRecFldDat[ipTbl][ipRec][ilOrg3].RcvDat, pcgH3LC);
    strcpy (prgRecFldDat[ipTbl][ipRec][ilOrg3].NewDat, pcgH3LC);
    prgRecFldDat[ipTbl][ipRec][ilOrg3].RcvLen = 3;
    prgRecFldDat[ipTbl][ipRec][ilOrg3].RcvFlg = TRUE;
    prgRecFldDat[ipTbl][ipRec][ilOrg3].NewFlg = TRUE;
    strcpy (prgRecFldDat[ipTbl][ipRec][ilDes3].RcvDat, pcgH3LC);
    strcpy (prgRecFldDat[ipTbl][ipRec][ilDes3].NewDat, pcgH3LC);
    prgRecFldDat[ipTbl][ipRec][ilDes3].RcvLen = 3;
    prgRecFldDat[ipTbl][ipRec][ilDes3].RcvFlg = TRUE;
    prgRecFldDat[ipTbl][ipRec][ilDes3].NewFlg = TRUE;
  }
/**********************************/

  prgPrjTblCfg[ipTbl].SqlFldLen = ilSqlFldLen;
  prgPrjTblCfg[ipTbl].SqlDatLen = ilSqlDatLen;
  prgPrjTblCfg[ipTbl].OutFldLen = ilOutFldLen;
  prgPrjTblCfg[ipTbl].OutDatLen = ilOutDatLen;

  if (igTWEJustUpdJfno == TRUE)
  {
    ilJcnt = GetFieldIndex (ipTbl, "JCNT");
    ilJfno = GetFieldIndex (ipTbl, "JFNO");
  }

  ilUpdFldCnt = 0;
  ilFldCnt = prgPrjTblCfg[ipTbl].TblFldCnt;

/* define  MORE_DEBUG  +++ jim +++ 20000825 */
#ifdef MORE_DEBUG
    dbg (DEBUG, "CollectChanges: ipTbl %d, ipRec: %d ",ipTbl, ipRec);
#endif
  for (ilFld = 1; ilFld <= ilFldCnt; ilFld++)
  {
/******  TWE 17012000  ******/
    if (igTWEJustUpdJfno == TRUE)
    {
      dbg (DEBUG, "CollectChanges: JFNO just updated"); /* +++ jim +++ 20000825 */
      if ((ilFld == ilJcnt) || (ilFld == ilJfno))
      {
      }
      else 
      {
        prgRecFldDat[ipTbl][ipRec][ilFld].NewFlg = FALSE;
      }
    }
    else if ((igTWENewFlnoFlight == TRUE) && (igPOPS == TRUE))
    {
      dbg (DEBUG, "CollectChanges: POPS"); /* +++ jim +++ 20000825 */
      if (prgRecFldDat[ipTbl][ipRec][ilFld].NewFlg == FALSE)
      {
        strcpy (pclFldData, prgRecFldDat[ipTbl][ipRec][ilFld].DbsDat);
        tool_filter_spaces (pclFldData);
        if ((long) (strlen (pclFldData)) > 0)
        {
          strcpy (prgRecFldDat[ipTbl][ipRec][ilFld].NewDat, prgRecFldDat[ipTbl][ipRec][ilFld].DbsDat);
          prgRecFldDat[ipTbl][ipRec][ilFld].NewFlg = TRUE;
        }
      }
    }
/****************************/

    /* Collect FieldList and Data for Update */
#ifdef MORE_DEBUG
    dbg (DEBUG, "FIELD <%s> RCV <%s> DBS <%s> NEW <%s> FLG (%d)", prgTblFldCfg[ipTbl][ilFld].SysTabFina,
         prgRecFldDat[ipTbl][ipRec][ilFld].RcvDat, prgRecFldDat[ipTbl][ipRec][ilFld].DbsDat,
         prgRecFldDat[ipTbl][ipRec][ilFld].NewDat, prgRecFldDat[ipTbl][ipRec][ilFld].NewFlg);
#endif /* MORE_DEBUG */

    if (prgRecFldDat[ipTbl][ipRec][ilFld].NewFlg == TRUE)
    {
      StrgPutStrg (prgPrjTblCfg[ipTbl].SqlFldLst, &ilSqlFldLen, prgTblFldCfg[ipTbl][ilFld].SysTabFina, 0, -1, ",");
      StrgPutStrg (prgPrjTblCfg[ipTbl].SqlDatBlk, &ilSqlDatLen, prgRecFldDat[ipTbl][ipRec][ilFld].NewDat, 0, -1, ",");
      if ((prgTblFldCfg[ipTbl][ilFld].FieldType != 14) && (prgTblFldCfg[ipTbl][ilFld].FieldType != 15))
      {
        /* Field is not USER (15) and not LSTU (14) */
        /* So tell the Caller to do that Update */
        ilUpdFldCnt++;
      }                         /* end if */
    }                           /* end if */

    /* Collect FieldList and Data for OutEvent */
    if ((prgRecFldDat[ipTbl][ipRec][ilFld].NewFlg == TRUE) || (prgTblFldCfg[ipTbl][ilFld].SendField == TRUE) ||
        ((igReopFlight == TRUE) && (prgTblFldCfg[ipTbl][ilFld].SendOnReop == TRUE)))
    {
      StrgPutStrg (prgPrjTblCfg[ipTbl].OutFldLst, &ilOutFldLen, prgTblFldCfg[ipTbl][ilFld].SysTabFina, 0, -1, ",");
      StrgPutStrg (prgPrjTblCfg[ipTbl].OutDatBlk, &ilOutDatLen, prgRecFldDat[ipTbl][ipRec][ilFld].NewDat, 0, -1, ",");
      StrgPutStrg (prgPrjTblCfg[ipTbl].OldDatBlk, &ilOldDatLen, prgRecFldDat[ipTbl][ipRec][ilFld].DbsDat, 0, -1, ",");
    }                           /* end if */
  }                             /* end for */

  if (ilSqlFldLen > 0)
  {
    ilSqlFldLen--;
  }                             /* end if */
  prgPrjTblCfg[ipTbl].SqlFldLst[ilSqlFldLen] = 0x00;
  prgPrjTblCfg[ipTbl].SqlFldLen = ilSqlFldLen;

  if (ilSqlDatLen > 0)
  {
    ilSqlDatLen--;
  }                             /* end if */
  prgPrjTblCfg[ipTbl].SqlDatBlk[ilSqlDatLen] = 0x00;
  prgPrjTblCfg[ipTbl].SqlDatLen = ilSqlDatLen;

  if (ilOutFldLen > 0)
  {
    ilOutFldLen--;
  }                             /* end if */
  prgPrjTblCfg[ipTbl].OutFldLst[ilOutFldLen] = 0x00;
  prgPrjTblCfg[ipTbl].OutFldLen = ilOutFldLen;

  if (ilOutDatLen > 0)
  {
    ilOutDatLen--;
  }                             /* end if */
  prgPrjTblCfg[ipTbl].OutDatBlk[ilOutDatLen] = 0x00;
  prgPrjTblCfg[ipTbl].OutDatLen = ilOutDatLen;

  if (ilOldDatLen > 0)
  {
    ilOldDatLen--;
  }                             /* end if */
  prgPrjTblCfg[ipTbl].OldDatBlk[ilOldDatLen] = 0x00;

#ifdef MORE_DEBUG
  dbg (DEBUG, "SQL FIELDS <%s>", prgPrjTblCfg[ipTbl].SqlFldLst);
  dbg (DEBUG, "SQL DATA   <%s>", prgPrjTblCfg[ipTbl].SqlDatBlk);
  dbg (DEBUG, "OUT FIELDS <%s>", prgPrjTblCfg[ipTbl].OutFldLst);
  dbg (DEBUG, "OUT DATA   <%s>", prgPrjTblCfg[ipTbl].OutDatBlk);
#endif /* MORE_DEBUG */

  ilRC = ilUpdFldCnt;
  return ilRC;
}                               /* end CollectChanges */

/********************************************************/
/* Function: UnFlgIfDbsEqNew (20010130)                 */
/*           Unset .NewFlg, if .NewDat == .DbsDat       */
/*           In case of InsertFlight all .RcvDat are    */
/*           copied to .NewDat. If flight 'EXITS' and   */
/*           update should be done, the .DbsDat should  */
/*           be checked to avoid traffic and            */
/*           'ANY CONFLICT' (happens with i.e. TWEIM)   */
/********************************************************/
static int UnFlgIfDbsEqNew (int ipTbl, int ipRec)
{
  int ilRC = RC_SUCCESS;
  int ilFld = -1;
  int ilFldCnt = -1;

  ilFldCnt = prgPrjTblCfg[ipTbl].TblFldCnt;

  for (ilFld = 1; ilFld <= ilFldCnt; ilFld++)
  {
    if (prgRecFldDat[ipTbl][ipRec][ilFld].NewFlg == TRUE)
    {
       if (strcmp(prgRecFldDat[ipTbl][ipRec][ilFld].NewDat,
                  prgRecFldDat[ipTbl][ipRec][ilFld].DbsDat) == 0)
       {
          prgRecFldDat[ipTbl][ipRec][ilFld].NewFlg = FALSE;
       }
    }                           /* end if */
  }                             /* end for */

  return ilRC;
}                               /* end UnFlgIfDbsEqNew */


/********************************************************/
/********************************************************/
static int BuildSqlFldStrg (char *pcpSqlStrg, char *pcpFldLst, int ipTyp)
{
  int ilRC = RC_SUCCESS;
  int ilFldCnt = 0;
  int ilFld = 0;
  int ilStrLen = 0;
  char pclFldNam[8];
  char pclFldVal[16];
  pcpSqlStrg[0] = 0x00;
  pcgTmpBuf3[0] = 0x00;
  ilFldCnt = field_count (pcpFldLst);
  switch (ipTyp)
  {
    case FOR_UPDATE:
      ilStrLen = 0;
      for (ilFld = 1; ilFld <= ilFldCnt; ilFld++)
      {
        (void) get_real_item (pclFldNam, pcpFldLst, ilFld);
        sprintf (pclFldVal, "%s=:V%s", pclFldNam, pclFldNam);
        StrgPutStrg (pcpSqlStrg, &ilStrLen, pclFldVal, 0, -1, ",");
      }                         /* end for */
      if (ilStrLen > 0)
      {
        ilStrLen--;
        pcpSqlStrg[ilStrLen] = 0x00;
      }                         /* end if */
      break;
    case FOR_INSERT:
      ilStrLen = 0;
      for (ilFld = 1; ilFld <= ilFldCnt; ilFld++)
      {
        (void) get_real_item (pclFldNam, pcpFldLst, ilFld);
        sprintf (pclFldVal, ":V%s", pclFldNam);
        StrgPutStrg (pcgTmpBuf3, &ilStrLen, pclFldVal, 0, -1, ",");
      }                         /* end for */
      if (ilStrLen > 0)
      {
        ilStrLen--;
        pcgTmpBuf3[ilStrLen] = 0x00;
        sprintf (pcpSqlStrg, "(%s) VALUES (%s)", pcpFldLst, pcgTmpBuf3);
      }                         /* end if */
      break;
    default:
      break;
  }                             /* end switch */
  /* dbg(TRACE,"SQL STRING <%s>",pcpSqlStrg); */
  return ilRC;
}                               /* end BuildSqlFldStrg */

/********************************************************/
/********************************************************/
static int CheckFieldValue (int ipTyp, int ipLen, int ipChgFlg, char *pcpData, int *pipValue)
{
  int ilRC = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  int ilVal1 = 0;
  int ilVal2 = 0;
  int ilVal3 = 0;
  int ilRetVal = 0;
  switch (ipTyp)
  {
    case 1:                    /* TRIM */
      if (ipChgFlg == TRUE)
      {
        ;                       /* StrgTrimBoth(???); */
      }                         /* end if */
      break;
    case 2:                    /* CHAR */
      break;
    case 3:                    /* DATE                  DAYS    MINUTES   WK_DAY */
      ilGetRc = GetFullDay (pcpData, &ilVal1, &ilRetVal, &ilVal3);
      break;
    case 4:                    /* TIME */
      break;
    case 5:                    /* NMBR */
      ilRetVal = atoi (pcpData);
      if (ilRetVal != 0)
      {
        sprintf (pcpData, "%0*d", ipLen, ilRetVal);
      }                         /* end if */
      else
      {
        if (strchr (pcpData, '0') != NULL)
        {
          sprintf (pcpData, "%0*d", ipLen, ilRetVal);
        }                       /* end if */
      }                         /* end else */
      break;
    case 6:                    /* LONG */
      /* ilRetVal = atoi(pcpData); */
      break;
    case 7:                    /* REAL */
      break;
    case 8:                    /* CURR */
      break;
    case 9:                    /* LIST */
      break;
    case 10:                   /* URNO */
      break;
    case 11:                   /* FKEY */
      break;
    case 12:                   /* FURN */
      break;
    case 13:                   /* FREL */
      break;
    case 14:                   /* LSTU */
      if (ipChgFlg == TRUE)
      {
        strcpy (pcpData, pcgCurTime);
      }                         /* end if */
      break;
    case 15:                   /* USER */
      if (ipChgFlg == TRUE)
      {
        strcpy (pcpData, pcgDestName);
      }                         /* end if */
      break;
    case 16:                   /* WORK */
      if (ipChgFlg == TRUE)
      {
        strcpy (pcpData, pcgRecvName);
      }                         /* end if */
      break;
    case 17:                   /* WDAY */
      ilGetRc = GetFullDay (pcpData, &ilVal1, &ilVal2, &ilRetVal);
      break;
    case 18:                   /* DAYS */
      ilGetRc = GetFullDay (pcpData, &ilRetVal, &ilVal2, &ilVal3);
      break;
    default:
      break;
  }                             /* end switch */

  /* QuickHack Warum ?? */
  ilGetRc = RC_SUCCESS;

  if (ilGetRc != RC_SUCCESS)
  {
    dbg (TRACE, "ERASED ILLEGAL DATA <%s> TYP (%d)", pcpData, ipTyp);
    dbg (TRACE, "INTERNALS: VAL1=%d VAL2=%d VAL3=%d RET=%d", ilVal1, ilVal2, ilVal3, ilRetVal);
    strcpy (pcpData, " ");
    ilRetVal = 0;
  }                             /* end if */
  *pipValue = ilRetVal;
  return ilRC;
}                               /* end CheckFieldValue */

/********************************************************/
/********************************************************/
static int CheckSysShmField (char *pcpTable, char *pcpFields, int ipRetVal)
{
  int ilRC = RC_FAIL;
  int ilGetRc;
  int ilTbl;
  int ilFldCnt;
  int ilFld;
  int ilItmNbr;
  int ilItmLen;
  char pclFldNam[16];
  char pclTblFull[16];
  char pclTblShort[16];
  strcpy (pclTblFull, pcpTable);
  strcpy (pclTblShort, pcpTable);
  pclTblShort[3] = 0x00;
  ilTbl = GetTableIndex (pclTblFull);
  if (ilTbl < 1)
  {
    sprintf (pclTblFull, "%s%s", pclTblShort, pcgBasExt);
    ilTbl = GetTableIndex (pclTblFull);
  }                             /* end if */
  if (ilTbl < 1)
  {
    ilGetRc = InitProjectTables (pcgH3LC, pclTblShort, pcgBasExt, "BAS", 0);
    if (ilGetRc == RC_SUCCESS)
    {
      ilGetRc = InitTableFields (igHandledTables, igHandledTables);
    }                           /* end if */
  }                             /* end if */
  ilTbl = GetTableIndex (pclTblFull);
  if (ilTbl > 0)
  {
    ilRC = ipRetVal;
    ilFldCnt = field_count (pcpFields);
    for (ilItmNbr = 1; ilItmNbr <= ilFldCnt; ilItmNbr++)
    {
      ilItmLen = get_real_item (pclFldNam, pcpFields, ilItmNbr);
      if (ilItmLen > 0)
      {
        ilFld = GetFieldIndex (ilTbl, pclFldNam);
        if (ilFld > 0)
        {
          if (prgTblFldCfg[ilTbl][ilFld].SysTabSyst[0] != 'Y')
          {
            dbg (TRACE, "ATTENTION: <%s> <%s> NOT IN SHM", pclTblFull, pclFldNam);
            ilRC = RC_FAIL;
          }                     /* end if */
        }                       /* end if */
        else
        {
          ilRC = RC_FAIL;
        }                       /* end else */
      }                         /* end if */
    }                           /* end for */
  }                             /* end if */
  return ilRC;
}                               /* end CheckSysShmField */

/********************************************************/
/********************************************************/
static int GetBasicData (char *pcpTable, char *pcpFields, char *pcpData, char *pcpResultFields, char *pcpResultData,
                         int ipMaxLines, int ipDefRc)
{
  int ilRC = RC_FAIL;
  int ilGetRc = RC_SUCCESS;
  int ilCount = 0;
  int ilUseOra = FALSE;
  short slCursor = 0;
  short slFkt = START;
  char *pclNlPos;
  char pclSqlBuf[512];
  char pclSqlKey[128];
  pcpResultData[0] = 0x00;
  pcgTmpBuf3[0] = 0x00;

  if (igUseShm == TRUE)
  {
    ilRC = CheckSysShmField (pcpTable, pcpFields, RC_SUCCESS);
    ilRC = CheckSysShmField (pcpTable, pcpResultFields, ilRC);
  }                             /* end if */

  if (ilRC == RC_SUCCESS)
  {
    ilCount = ipMaxLines;
    if (strcmp (pcgDefTblExt, "TAB") == 0)
    {
      sprintf (pclSqlBuf, "%s,HOPO", pcpFields);
      sprintf (pclSqlKey, "%s,%s", pcpData, pcgH3LC);
    }                           /* end if */
    else
    {
      strcpy (pclSqlBuf, pcpFields);
      strcpy (pclSqlKey, pcpData);
    }                           /* end else */
    dbg (DEBUG, "SYSLIB: TBL <%s> FLD <%s> DAT <%s>", pcpTable, pclSqlBuf, pclSqlKey);
    ilRC = syslibSearchDbData (pcpTable, pclSqlBuf, pclSqlKey, pcpResultFields, pcgTmpBuf3, &ilCount, "\n");
    if (ilRC == RC_SUCCESS)
    {
      if ((ipMaxLines < 1) || (ipMaxLines > ilCount))
      {
        ipMaxLines = ilCount;
      }                         /* end if */
      pclNlPos = pcgTmpBuf3;
      pclNlPos++;
      while (ipMaxLines > 0)
      {
        pclNlPos = strstr (pclNlPos, "\n");
        if (pclNlPos != NULL)
        {
          ipMaxLines--;
          pclNlPos++;
        }                       /* end if */
        else
        {
          ipMaxLines = 0;
        }                       /* end else */
      }                         /* end while */
      if (pclNlPos != NULL)
      {
        pclNlPos--;
        if (*pclNlPos == '\n')
        {
          *pclNlPos = 0x00;
          dbg (TRACE, "DATA CUT! \n<%s>", pcgTmpBuf3);
        }                       /* end if */
      }                         /* end if */
      strcpy (pcpResultData, pcgTmpBuf3);
    }                           /* end if */
    else
    {
      if (ilRC == RC_FAIL)
      {
        ilUseOra = TRUE;
        igUseShm = FALSE;
        dbg (TRACE, "=====================================================");
        dbg (TRACE, "ERROR: SHARED MEMORY FAILURE !!");
        dbg (TRACE, "USE TBL <%s>", pcpTable);
        dbg (TRACE, "GET FLD <%s>", pcpResultFields);
        dbg (TRACE, "KEY FLD <%s>", pcpFields);
        dbg (TRACE, "KEY DAT <%s>", pcpData);
        dbg (TRACE, "GET BASIC DATA SWITCHED TO USE OF ORACLE");
        dbg (TRACE, "=====================================================");
      }                         /* end if */
      if (ilRC == RC_NOT_FOUND)
      {
        ilRC = ipDefRc;
      }                         /* end if */
    }                           /* end else */
  }                             /* end if */
  else
  {
    ilUseOra = TRUE;
  }                             /* end else */

  if (ilUseOra == TRUE)
  {
    sprintf (pclSqlKey, "WHERE %s='%s'", pcpFields, pcpData);
    CheckWhereClause (TRUE, pclSqlKey, FALSE, TRUE, "\0");
    sprintf (pclSqlBuf, "SELECT %s FROM %s %s", pcpResultFields, pcpTable, pclSqlKey);
    dbg (DEBUG, "<%s>", pclSqlBuf);
    slFkt = START;
    slCursor = 0;
    
    ilGetRc = sql_if (slFkt, &slCursor, pclSqlBuf, pcpResultData);
    
    if (ilGetRc == DB_SUCCESS)
    {
      BuildItemBuffer (pcpResultData, pcpResultFields, 0, ",");
      ilRC = RC_SUCCESS;
    }                           /* end if */
    else
    {
      ilRC = ipDefRc;
    }                           /* end else */
    close_my_cursor (&slCursor);
  }                             /* end else */
  dbg (DEBUG, "RESULT <%s> <%s>", pcpResultFields, pcpResultData);

  return ilRC;
}                               /* end GetBasicData */

/********************************************************/
/********************************************************/
static int GetBasicRange (char *pcpTable, char *pcpFields, char *pcpData, char *pcpResultFields, char *pcpResultData,
                          char *pcpResultLine, int ipMaxLines, int ipDefRc)
{
  int ilRC = RC_SUCCESS;
  int ilCount = 0;
  int ilItmCnt = 0;
  int ilItm = 0;
  int ilSkedDay = 0;
  int ilVpfrDay = 0;
  int ilVptoDay = 0;
  int ilMin = 0;
  int ilWkd = 0;
  int ilFound = FALSE;
  char pclSeas[16];
  char pclVpfr[16];
  char pclVpto[16];
  char pclSqlBuf[32];
  char pclSqlKey[64];
  pcgTmpBuf3[0] = 0x00;
  if (strcmp (pcgDefTblExt, "TAB") == 0)
  {
    sprintf (pclSqlBuf, "%s,HOPO", pcpFields);
    sprintf (pclSqlKey, ",%s", pcgH3LC);
  }                             /* end if */
  else
  {
    strcpy (pclSqlBuf, pcpFields);
    strcpy (pclSqlKey, "\0");
  }                             /* end else */
  ilRC = syslibSearchDbData (pcpTable, pclSqlBuf, pclSqlKey, pcpResultFields, pcgTmpBuf3, &ilCount, ",");
  if (ilRC == RC_SUCCESS)
  {
    (void) GetFullDay (pcpData, &ilSkedDay, &ilMin, &ilWkd);
    ilItmCnt = field_count (pcgTmpBuf3);
    ilFound = FALSE;
    for (ilItm = 1; ((ilItm < ilItmCnt) && (ilFound != TRUE)); ilItm += 3)
    {
      (void) get_real_item (pclSeas, pcgTmpBuf3, ilItm);
      (void) get_real_item (pclVpfr, pcgTmpBuf3, ilItm + 1);
      (void) get_real_item (pclVpto, pcgTmpBuf3, ilItm + 2);
      (void) GetFullDay (pclVpfr, &ilVpfrDay, &ilMin, &ilWkd);
      (void) GetFullDay (pclVpto, &ilVptoDay, &ilMin, &ilWkd);
      if ((ilVpfrDay <= ilSkedDay) && (ilVptoDay >= ilSkedDay))
      {
        strcpy (pcpResultData, pclSeas);
        sprintf (pcpResultLine, "%s,%s,%s", pclSeas, pclVpfr, pclVpto);
        ilFound = TRUE;
      }                         /* end if */
    }                           /* end for */
  }                             /* end if */
  return ilRC;
}                               /* end GetBasicRange */

/********************************************************/
/********************************************************/
static int DataTripleCheck (char *pcpOutData, char *pcpData1, char *pcpCheck1, char *pcpData2, char *pcpCheck2,
                            char *pcpResult1, char *pcpResult2, char *pcpResult3, char *pcpElseResult)
{
  int ilRetVal;
  if ((strcmp (pcpData1, pcpCheck1) == 0) && (strcmp (pcpData2, pcpCheck2) == 0))
  {
    strcpy (pcpOutData, pcpResult3);
    ilRetVal = 3;
  }
  else if (strcmp (pcpData2, pcpCheck2) == 0)
  {
    strcpy (pcpOutData, pcpResult2);
    ilRetVal = 2;
  }
  else if (strcmp (pcpData1, pcpCheck1) == 0)
  {
    strcpy (pcpOutData, pcpResult1);
    ilRetVal = 1;
  }
  else
  {
    strcpy (pcpOutData, pcpElseResult);
    ilRetVal = 4;
  }                             /* end if / else if / else */
  return ilRetVal;
}                               /* end DataTripleCheck */

/********************************************************/
/********************************************************/
static int StoreErrorData (char *pcpMeno, char *pcpMety, char *pcpText, char *pcpData)
{
  int ilRC = RC_SUCCESS;
  igErrorCount++;
  if (igErrorCount <= MAX_ERROR_HDL)
  {
    strcpy (prgErrorList[igErrorCount].ErrorMeno, pcpMeno);
    strcpy (prgErrorList[igErrorCount].ErrorMety, pcpMety);
    strcpy (prgErrorList[igErrorCount].ErrorText, pcpText);
    strcpy (prgErrorList[igErrorCount].ErrorData, pcpData);
  }                             /* end if */
  return ilRC;
}                               /* end StoreErrorData() */

/********************************************************/
/********************************************************/
static int CollectErrorData (char *pcpResult)
{
  int ilRC = RC_SUCCESS;
  int ilChrPos = 0;
  int ilErrIdx;

  if (igErrorCount > MAX_ERROR_HDL)
  {
    igErrorCount = MAX_ERROR_HDL;
  }                             /* end if */
  for (ilErrIdx = 1; ilErrIdx <= igErrorCount; ilErrIdx++)
  {
    StrgPutStrg (pcpResult, &ilChrPos, prgErrorList[ilErrIdx].ErrorMeno, 0, -1, ",");
    StrgPutStrg (pcpResult, &ilChrPos, prgErrorList[ilErrIdx].ErrorMety, 0, -1, ",");
    StrgPutStrg (pcpResult, &ilChrPos, prgErrorList[ilErrIdx].ErrorText, 0, -1, ",");
    StrgPutStrg (pcpResult, &ilChrPos, prgErrorList[ilErrIdx].ErrorData, 0, -1, "\n");
  }                             /* end for */
  if (ilChrPos > 0)
  {
    ilChrPos--;
    pcpResult[ilChrPos] = 0x00;
  }                             /* end if */
  /* dbg(DEBUG,"ERRORS <%s>",pcpResult); */
  return ilRC;
}                               /* end CollectErrorData() */

/******************************************************************************/
static int FormatDayFreq (char *pcpFreq, char *pcpFreqChr)
{
  int ilIdx;
  int ilChrPos;
  int ilStrLen;
  int ilDayCnt = 0;
  char pclTmpChr[] = "X";
  strcpy (pcpFreqChr, "       ");
  ilStrLen = strlen (pcpFreq);
  for (ilChrPos = 0; ilChrPos < ilStrLen; ilChrPos++)
  {
    if (((pcpFreq[ilChrPos] >= '1') && (pcpFreq[ilChrPos] <= '7')) || (pcpFreq[ilChrPos] == 'X'))
    {
      if (pcpFreq[ilChrPos] == 'X')
      {
        pcpFreqChr[ilChrPos] = ilChrPos + '1';
      }                         /* end if */
      else
      {
        pclTmpChr[0] = pcpFreq[ilChrPos];
        ilIdx = atoi (pclTmpChr) - 1;
        pcpFreqChr[ilIdx] = pcpFreq[ilChrPos];
      }                         /* end else */
      ilDayCnt++;
    }                           /* end if */
  }                             /* end for */
  return ilDayCnt;
}                               /* end FormatDayFreq */


/* ******************************************************************** */
/* ******************************************************************** */
static int SetFlnoItems (int ipTbl, int ipRec)
{
  int ilRC = RC_SUCCESS;
  int ilFltn = 0;
  int ilFlns = 0;
  int ilAlc3 = 0;
  int ilFlno = 0;
/****   TWE 20000124  *****/
  int ilAlc2 = 0;
/**************************/
  char pclFlno[16];
  char pclFnr3[16];
  char pclFnr2[16];
  char pclAlc3[16];
  char pclAlc2[16];
  char pclAlcRcv[16];
  char pclFltn[16];
  char pclFlns[16];
  char pclFields[128];
  char pclData[128];
  char pclTmp1[30];
  char pclTmp2[30];
  int ilReceive;

  ilReceive = FALSE;

  strcpy (pclFnr3, "");
  strcpy (pclFnr2, "");
  strcpy (pclFlns, " ");
  pclAlc2[0] = 0x00;
  pclAlc3[0] = 0x00;


   TRACE_FIELD_OF_INTEREST(__LINE__,"SFNA");

  ilFltn = GetFieldIndex (ipTbl, "FLTN");
  if (prgRecFldDat[ipTbl][ipRec][ilFltn].RcvFlg == TRUE)
  {
    strcpy (pclFltn, prgRecFldDat[ipTbl][ipRec][ilFltn].RcvDat);
    ilReceive = TRUE;
  }
  else
  {
    strcpy (pclFltn, prgRecFldDat[ipTbl][ipRec][ilFltn].DbsDat);
  }
  dbg (DEBUG, "pclFltn <%s>", pclFltn);
  ilFlns = GetFieldIndex (ipTbl, "FLNS");
  if (prgRecFldDat[ipTbl][ipRec][ilFlns].RcvFlg == TRUE)
  {
    strcpy (pclFlns, prgRecFldDat[ipTbl][ipRec][ilFlns].RcvDat);
    ilReceive = TRUE;
  }
  else
  {
    strcpy (pclFlns, prgRecFldDat[ipTbl][ipRec][ilFlns].DbsDat);
  }
  dbg (DEBUG, "pclFlns <%s>", pclFlns);
  ilAlc3 = GetFieldIndex (ipTbl, "ALC3");
  if (prgRecFldDat[ipTbl][ipRec][ilAlc3].RcvFlg == TRUE)
  {
    strcpy (pclAlc3, prgRecFldDat[ipTbl][ipRec][ilAlc3].RcvDat);
    ilReceive = TRUE;
  }
  else
  {
    strcpy (pclAlc3, prgRecFldDat[ipTbl][ipRec][ilAlc3].DbsDat);
  }
  dbg (DEBUG, " pclAlc3 <%s>", pclAlc3);
  ilAlc2 = GetFieldIndex (ipTbl, "ALC2");
  if (prgRecFldDat[ipTbl][ipRec][ilAlc2].RcvFlg == TRUE)
  {
    strcpy (pclAlc2, prgRecFldDat[ipTbl][ipRec][ilAlc2].RcvDat);
    ilReceive = TRUE;
  }
  else
  {
    strcpy (pclAlc2, prgRecFldDat[ipTbl][ipRec][ilAlc2].DbsDat);
  }
  dbg (DEBUG, " pclAlc2 <%s>", pclAlc2);

  tool_filter_spaces (pclAlc2);
  if ((long) (strlen (pclAlc2)) == 2)
  {
    strcpy (pclAlcRcv, pclAlc2);
  }
  else
  {
    dbg (DEBUG, "SetFlnoItems: ALC2 <%s> not valid (take ALC3)", pclAlc2);
    strcpy (pclAlc3, prgRecFldDat[ipTbl][ipRec][ilAlc3].RcvDat);
    tool_filter_spaces (pclAlc3);
    if ((long) (strlen (pclAlc3)) == 3)
    {
      strcpy (pclAlcRcv, pclAlc3);
    }
    else if ((long) (strlen (pclAlc3)) == 2)
    {  /* +++ jim +++ 20010123: perhaps in ALC3 is ALC2 */
      dbg (TRACE, "SetFlnoItems: using 2 chars of ALC3 <%s> ", pclAlc3);
      strcpy (pclAlc2, pclAlc3);
      strcpy (pclAlcRcv, pclAlc3);
      pclAlc3[0] = 0;
    }
    else
    {
      dbg (TRACE, "SetFlnoItems: ALC3 <%s> and ALC2 <%s> not valid", pclAlc3, pclAlc2);
    }
  }

  /* LOOK AT FIELD DEFC OF ALTTAB FOR TAKE 2 OR 3 LETTERCODE */

  if (igTWECheckDEFC == TRUE)
  {
    ilRC = RC_FAIL;
    pclTmp2[0] = 0x00;
    sprintf (pclTmp1, "ALT%s", pcgBasExt);
    if ((long) (strlen (pclAlc3)) == 3)
    {
      ilRC = GetBasicData (pclTmp1, "ALC3", pclAlc3, "DEFC", pclTmp2, 1, RC_FAIL);
    }
    if (ilRC != RC_SUCCESS)
    {
      ilRC = GetBasicData (pclTmp1, "ALC2", pclAlc2, "DEFC", pclTmp2, 1, RC_FAIL);
      if (ilRC != RC_SUCCESS)
      {
        dbg (TRACE, "SetFlnoItems:ERROR: FIELD DEFC NOT FOUND IN <%s> for <%s> or <%s>", pclTmp1, pclAlc3, pclAlc2);
        dbg (TRACE, "SetFlnoItems: TAKE ALC2 <%s> FOR FLNO", pclAlc2);
      }
    }
    if (ilRC == RC_SUCCESS)
    {
      if (pclTmp2[0] == '3')
      {
        dbg (DEBUG, "SetFlnoItems: TAKE ALC3 <%s> FOR FLNO", pclAlc3);
        strcpy (pclAlcRcv, pclAlc3);
      }
      else
      {
        if ((long) (strlen (pclAlc2)) == 2)
        {
          dbg (DEBUG, "SetFlnoItems: TAKE ALC2 <%s> FOR FLNO", pclAlc2);
        }
        else
        {
          dbg (TRACE, "SetFlnoItems:ERROR: DEFAULT ALC2 <%s> NOT VALID FOR FLNO", pclAlc2);
          dbg (TRACE, "SetFlnoItems: TAKE ALC3 <%s> FOR FLNO", pclAlcRcv);
        }
      }
    }
    else
    {
      igTWECheckDEFC = FALSE;
    }
    ilRC = RC_SUCCESS;
  }                             /*  igTWECheckDEFC  */

  if (igTWEImport == TRUE)
  {
    pclFnr2[0] = 0x00;
    pclFnr3[0] = 0x00;
    (void) TWEBuildFlightNbr (pclFnr3, pclFnr2, pclAlc3, pclAlc2, pclFltn, pclFlns, 0);
  }
  else
  {
    (void) BuildFlightNbr (pclFnr3, pclFnr2, pclAlc3, pclAlc2, pclFltn, pclFlns, 0);
  }

  if (strlen (pclAlcRcv) == 2)
  {
    pclAlcRcv[2] = ' ';
    pclAlcRcv[3] = 0x00;
  }                             /* end if */
  sprintf (pclFlno, "%s%-5s%s", pclAlcRcv, pclFltn, pclFlns);

  dbg (DEBUG, "SetFlnoItems: FLNO <%s>", pclFlno);

  if (ilReceive == TRUE)
  {
    strcpy (pclFields, "FLNO,FLTN,FLNS,ALC2,ALC3");
    sprintf (pclData, "%s,%s,%s,%s,%s", pclFlno, pclFltn, pclFlns, pclAlc2, pclAlc3);
    (void) StoreReceivedData (TRUE, ipTbl, ipRec, 0, pcgAftTab, pclFields, pclData);
dbg(DEBUG,"%05d:",__LINE__);

    ilFlno = GetFieldIndex (ipTbl, "FLNO");
    (void) SetNewValues (ipTbl, ipRec, ilFlno, "FLNO", pclFlno);
dbg(DEBUG,"%05d:",__LINE__);
  }


   TRACE_FIELD_OF_INTEREST(__LINE__,"SFNE");

dbg(DEBUG,"%05d:",__LINE__);
  return ilRC;

}                               /* end SetFlnoItems */

/* ******************************************************************** */
/* The bd_buildFlightNbr routine					*/
/* ******************************************************************** */
static int FormatCedaFlno (char *pcpFlno)
{
  int ilRC = RC_FAIL;
  int ilLen = 0;
  int ilPos = 0;
  char pclTmpFlno[16];
  char pclAlc[4];
  char pclFnr[8];
  char pclSuf[4];
  strcpy (pclTmpFlno, pcpFlno);
  str_trm_all (pclTmpFlno, " ", TRUE);
  ilLen = strlen (pclTmpFlno);
  if (ilLen > 2)
  {
    pclSuf[0] = 0x00;
    ilLen--;
    if ((pclTmpFlno[ilLen] > '9') || (pclTmpFlno[ilLen] < '0') || (pclTmpFlno[ilLen - 1] == ' '))
    {
      pclSuf[0] = pclTmpFlno[ilLen];
      pclSuf[1] = 0x00;
      pclTmpFlno[ilLen] = 0x00;
    }                           /* end if */
    if ((pclTmpFlno[2] >= '0') && (pclTmpFlno[2] <= '9'))
    {
      strncpy (pclAlc, pclTmpFlno, 2);
      pclAlc[2] = 0x00;
      strcpy (pclFnr, &pclTmpFlno[2]);
    }                           /* end if */
    else
    {
      strncpy (pclAlc, pclTmpFlno, 3);
      pclAlc[3] = 0x00;
      strcpy (pclFnr, &pclTmpFlno[3]);
    }                           /* end else */
    str_trm_all (pclAlc, " ", TRUE);
    str_trm_all (pclFnr, " ", TRUE);
    strcpy (pclTmpFlno, "00000");
    ilPos = 3 - strlen (pclFnr);
    if (ilPos < 0)
    {
      ilPos = 0;
    }                           /* end if */
    strcpy (&pclTmpFlno[ilPos], pclFnr);
    strcpy (pcpFlno, "         ");
    ilPos = 0;
    StrgPutStrg (pcpFlno, &ilPos, pclAlc, 0, -1, "\0");
    ilPos = 3;
    StrgPutStrg (pcpFlno, &ilPos, pclTmpFlno, 0, -1, "\0");
    pcpFlno[8] = pclSuf[0];
    str_trm_all (pcpFlno, " ", TRUE);
    ilRC = RC_SUCCESS;
  }                             /* end if */
  return ilRC;
}                               /* end FormatCedaFlno */

static int BuildFlightNbr (char *fnr3, char *fnr2, char *alc3, char *alc2, char *flnr, char *flsf, int mth)
{
  int rc = RC_SUCCESS;
  int fnrLen;
  int i;
  int cp;
  int dp;
  char zeros[] = "0000";
  char tmpFlnr[16];
  char pclTmpTable[16];

  sprintf (pclTmpTable, "ALT%s", pcgBasExt);
  fnrLen = strlen (fnr3);
  if (fnrLen == 0)
  {
    sprintf (fnr3, "%s%s%s", alc3, flnr, flsf);
  }                             /* end if */
  /* dbg(DEBUG,"BUILD FLNO <%s>",fnr3); */
  fnrLen = strlen (fnr3);
  fnr2[0] = 0x00;
  alc3[0] = 0x00;
  alc2[0] = 0x00;
  flnr[0] = 0x00;
  flsf[0] = 0x00;

  if (fnrLen > 2)
  {
    cp = 2;
    if ((fnr3[cp] >= 'A') && (fnr3[cp] <= 'Z'))
    {
      cp = 3;
    }                           /* end if */
    for (i = 0; i < cp; i++)
    {
      alc3[i] = fnr3[i];
    }                           /* end for */
    alc3[cp] = 0x00;

    while ((cp < fnrLen) && ((fnr3[cp] == ' ') || (fnr3[cp] == '0')))
    {
      cp++;
    }                           /* end while */
    dp = 0;
    while ((cp < fnrLen) && ((fnr3[cp] >= '0') && (fnr3[cp] <= '9')))
    {
      flnr[dp] = fnr3[cp];
      dp++;
      cp++;
      if (dp > 4)
      {
        dp = 4;
        rc = RC_FAIL;
      }                         /* end if */
    }                           /* end while */
    flnr[dp] = 0x00;

    while ((cp < fnrLen) && (fnr3[cp] == ' '))
    {
      cp++;
    }                           /* end while */

    if (cp < fnrLen)
    {
      flsf[0] = fnr3[cp];
      flsf[1] = 0x00;
    }                           /* end if */

    if (strlen (flnr) == 0)
    {
      rc = RC_FAIL;
    }                           /* end if */
    /*
       dbg(DEBUG,"1. ALC3<%s> ALC2<%s> FLNR<%s> FLSF<%s>",alc3,alc2,flnr,flsf);
     */
    if (rc == RC_SUCCESS)
    {
      if (strlen (alc3) == 2)
      {
        if (mth != 1)
        {
          strcpy (alc2, alc3);
          if (strcmp (alc2, "YY") != 0)
          {
            rc = GetBasicData (pclTmpTable, "ALC2", alc2, "ALC3", alc3, 1, RC_FAIL);
          }                     /* end if */
        }                       /* end if */
        else
        {
          alc3[2] = 0x20;
          alc3[3] = 0x00;
        }                       /* end else */
      }                         /* end if */
      else
      {
        if (mth != 1)
        {
          rc = GetBasicData (pclTmpTable, "ALC3", alc3, "ALC2", alc2, 1, RC_FAIL);
          if (rc != RC_SUCCESS)
          {
            strcpy (alc2, alc3);
            rc = RC_SUCCESS;
          }                     /* end if */
        }                       /* end if */
      }                         /* end else */
    }                           /* end if */
    /*
       dbg(DEBUG,"2. ALC3<%s> ALC2<%s> FLNR<%s> FLSF<%s>",alc3,alc2,flnr,flsf);
     */

    if (rc == RC_SUCCESS)
    {
      if (strlen (alc2) == 2)
      {
        alc2[2] = 0x20;
        alc2[3] = 0x00;
      }                         /* end if */
      if (strlen (alc3) == 2)
      {
        alc3[2] = 0x20;
        alc3[3] = 0x00;
      }                         /* end if */
      cp = 3 - strlen (flnr);
      if (cp > 0)
      {
        zeros[cp] = 0x00;
        sprintf (tmpFlnr, "%s%s", zeros, flnr);
        strcpy (flnr, tmpFlnr);
      }                         /* end if */
      /*
         dbg(DEBUG,"3. ALC3<%s> ALC2<%s> FLNR<%s> FLSF<%s>",alc3,alc2,flnr,flsf);
       */
      if (strlen (flnr) > 3)
      {
        sprintf (fnr3, "%s%s%s", alc3, flnr, flsf);
        if (mth != 1)
        {
          sprintf (fnr2, "%s%s%s", alc2, flnr, flsf);
        }                       /* end if */
      }                         /* end if */
      else
      {
        if (strlen (flsf) > 0)
        {
          sprintf (fnr3, "%s%s %s", alc3, flnr, flsf);
          if (mth != 1)
          {
            sprintf (fnr2, "%s%s %s", alc2, flnr, flsf);
          }                     /* end if */
        }                       /* end if */
        else
        {
          sprintf (fnr3, "%s%s", alc3, flnr);
          if (mth != 1)
          {
            sprintf (fnr2, "%s%s", alc2, flnr);
          }                     /* end if */
        }                       /* end else */
      }                         /* end else */

      alc2[2] = 0x00;

    }                           /* end if */
  }                             /* end if */
  else
  {
    rc = RC_FAIL;
  }                             /* end else */
  return rc;
}                               /* end BuildFlightNbr */

/* *******************************************************/
/* *******************************************************/
static void CheckPerformance (int ipFlag, char *pcpRtTime)
{
  if (tgMaxStampDiff > 0)
  {
    if (ipFlag == TRUE)
    {
      tgBeginStamp = time (0L);
      tgQueDiff = 0;
      if ((pcpRtTime[0] >= '0') && (pcpRtTime[0] <= '9'))
      {
        tgNowTime = tgBeginStamp % 10000;
        tgQueTime = atol (pcpRtTime);
        tgQueDiff = tgNowTime - tgQueTime;
        if (tgQueDiff > tgMaxStampDiff)
        {
          dbg (TRACE, "PERF_CHECK: EVENT QUEUE_TIME (%d SEC)", tgQueDiff);
        }                       /* end if */
      }                         /* end if */
      dbg (TRACE, "----- SQL ACTION BEGIN");
    }                           /* end if */
    else
    {
      tgEndStamp = time (0L);
      tgStampDiff = tgEndStamp - tgBeginStamp;
      dbg (TRACE, "----- SQL ACTION READY (%d SEC)", tgStampDiff);
      if (tgStampDiff > tgMaxStampDiff)
      {
        if (debug_level == 0)
        {
          debug_level = TRACE;
          igShowStampDiff = TRUE;
        }                       /* end if */
        dbg (TRACE, "PERF_CHECK: TRANSACTION TIME (%d SEC)", tgStampDiff);
      }                         /* end if */
      if ((tgQueDiff > 0) && (tgStampDiff > 0))
      {
        tgStampDiff += tgQueDiff;
        if (tgStampDiff > tgMaxStampDiff)
        {
          if (debug_level == 0)
          {
            debug_level = TRACE;
            igShowStampDiff = TRUE;
          }                     /* end if */
          dbg (TRACE, "PERF_CHECK: TOTAL QUEUE_TIME (%d SEC)", tgStampDiff);
        }                       /* end if */
      }                         /* end if */
    }                           /* end else */
  }                             /* end if */
  return;
}                               /* end CheckPerformance */

/********************************************************/
/* Space for Appending (Hard Coded Parts)		*/
/* *****************************************************/

/* *****************************************************/
/* *****************************************************/
/* HardCodeJcnt: Calculate number of CodeShareFlights (JointFlights) */
/*               The FLTN of the CodeShareFlights are stored in JFNO */
/*               9 characters in widht without any delimiter         */

static void HardCodeJcnt (int ipTbl, int ipRec)
{
  int ilCnt = 0;
  int ilJcnt = -1;
  int ilJfno = -1;
  char *pclDummy;
  char pclJfno[200];
  char pclJcnt[10];

  if (prgPrjTblCfg[ipTbl].TypeOfTable == 1)
  {                             /* AFT */
    dbg (DEBUG, "in 'HardCodeJcnt' ...");
    ilJcnt = GetFieldIndex (ipTbl, "JCNT");
    ilJfno = GetFieldIndex (ipTbl, "JFNO");
    if (ilJfno > 0)
    {
      if (prgRecFldDat[ipTbl][ipRec][ilJfno].NewFlg == TRUE)
      {
        /* pointer to data: */
        pclDummy = prgRecFldDat[ipTbl][ipRec][ilJfno].NewDat;
        /* get trimmed data, assume no ':' is in data */
        ilCnt = GetDataItem (pclJfno, pclDummy, 1, ':', "", "  ");
        dbg (DEBUG, "HardCodeJcnt: JFNO: <%s>, len: <%d>", pclJfno, ilCnt);
        if ((ilCnt % 9) > 0)
        {                       /* last JFNO has less than 9 char */
          ilCnt = (ilCnt / 9) + 1;
        }
        else
        {                       /* last JFNO has 9 char or new value is empty */
          ilCnt = (ilCnt / 9);
        }
        dbg (DEBUG, "HardCodeJcnt: JFNO: <%s>, #CodeShares: <%d>", pclJfno, ilCnt);
        sprintf (pclJcnt, "%d", ilCnt);
        (void) SetNewValues (ipTbl, ipRec, ilJcnt, "", pclJcnt);
      }                         /* new JFNO */
    }                           /* JFNO in AFT */
  }                             /* AFT */
}                               /* end HardCodeJcnt */

/* *****************************************************/
static void HardCodeAdid (int ipTbl, int ipRec)
{
  int ilGetRc;
  int ilFld1 = 0;
  int ilFld2 = 0;
  int ilFld3 = 0;

  if (prgPrjTblCfg[ipTbl].TypeOfTable == 1)
  {                             /* AFT */
    /* ------------- FILL ADID ------------- */
    ilFld1 = GetFieldIndex (ipTbl, "ADID");
    if (ilFld1 > 0)
    {
      ilFld2 = GetFieldIndex (ipTbl, "ORG3");
      ilFld3 = GetFieldIndex (ipTbl, "DES3");
      ilGetRc =
        DataTripleCheck (pcgTmpBuf2, prgRecFldDat[ipTbl][ipRec][ilFld2].NewDat, pcgH3LC,
                         prgRecFldDat[ipTbl][ipRec][ilFld3].NewDat, pcgH3LC, "D", "A", "B", "U");
      if (ilGetRc == 4)
      {
        ilFld2 = GetFieldIndex (ipTbl, "ORG4");
        ilFld3 = GetFieldIndex (ipTbl, "DES4");
        ilGetRc =
          DataTripleCheck (pcgTmpBuf2, prgRecFldDat[ipTbl][ipRec][ilFld2].NewDat, pcgH4LC,
                           prgRecFldDat[ipTbl][ipRec][ilFld3].NewDat, pcgH4LC, "D", "A", "B", "U");
        if (ilGetRc != 4)
        {
          dbg (TRACE, "HardCodeAdid: ORIG OR DEST 3/4-LC NOT CONSISTENT");
        }                       /* end if */
      }                         /* end if */
      /* QuickHack */
      switch (pcgTmpBuf2[0])
      {
        case 'A':
          (void) SetNewValues (ipTbl, ipRec, 0, "DES3", pcgH3LC);
          (void) SetNewValues (ipTbl, ipRec, 0, "DES4", pcgH4LC);
          break;
        case 'D':
          (void) SetNewValues (ipTbl, ipRec, 0, "ORG3", pcgH3LC);
          (void) SetNewValues (ipTbl, ipRec, 0, "ORG4", pcgH4LC);
          break;
        default:
          break;
      }                         /* end switch */
      ilGetRc = SetNewValues (ipTbl, ipRec, ilFld1, "", pcgTmpBuf2);
    }                           /* end if */
  }                             /* end if */
  return;
}                               /* HardCodeAdid */

/* **************************************************** */
/* **************************************************** */
static void HardCodePaid (int ipTbl, int ipRec)
{
  int ilGetRc = RC_NOT_FOUND;
  int ilFld1;
  int ilFld2;
  int ilFld3;
  char pclAirlTable[16];
  char pclAircTable[16];
  char pclBasicField[16];
  char pclResultField[16];
  char pclTmpBuf[512];

  sprintf (pclAirlTable, "ALT%s", pcgBasExt);
  sprintf (pclAircTable, "ACR%s", pcgBasExt);

  /* ------------- FILL PAID ------------- */
  ilFld1 = GetFieldIndex (ipTbl, "PAID");
  if (prgRecFldDat[ipTbl][ipRec][ilFld1].NewDat[0] != 'P')
  {
    strcpy (pclResultField, "CASH");
    pcgTmpBuf1[0] = 0x00;
    strcpy (pclBasicField, "ALC3");
    ilFld3 = GetFieldIndex (ipTbl, pclBasicField);
    if (prgRecFldDat[ipTbl][ipRec][ilFld3].NewLen >= 3)
    {
      ilGetRc =
        GetBasicData (pclAirlTable, pclBasicField, prgRecFldDat[ipTbl][ipRec][ilFld3].NewDat, pclResultField,
                      pcgTmpBuf1, 1, RC_NOT_FOUND);
    }                           /* end if */
    if (ilGetRc == RC_NOT_FOUND)
    {
      strcpy (pclBasicField, "ALC2");
      ilFld2 = GetFieldIndex (ipTbl, pclBasicField);
      if (prgRecFldDat[ipTbl][ipRec][ilFld2].NewLen >= 2)
      {
        ilGetRc =
          GetBasicData (pclAirlTable, pclBasicField, prgRecFldDat[ipTbl][ipRec][ilFld2].NewDat, pclResultField,
                        pcgTmpBuf1, 1, RC_NOT_FOUND);
      }                         /* end if */
    }                           /* end if */
    if ((ilGetRc == RC_NOT_FOUND) || (pcgTmpBuf1[0] == ' '))
    {
      strcpy (pcgTmpBuf1, "B");
      pclTmpBuf[0] = 0x00;
      ilGetRc = RC_NOT_FOUND;
      strcpy (pclResultField, "OWNE");
      strcpy (pclBasicField, "REGN");
      ilFld2 = GetFieldIndex (ipTbl, pclBasicField);
      if (prgRecFldDat[ipTbl][ipRec][ilFld2].NewLen >= 4)
      {
        ilGetRc =
          GetBasicData (pclAircTable, pclBasicField, prgRecFldDat[ipTbl][ipRec][ilFld2].NewDat, pclResultField,
                        pclTmpBuf, 1, RC_NOT_FOUND);
      }                         /* end if */
      if (ilGetRc == RC_SUCCESS)
      {
        str_trm_all (pclTmpBuf, " ", TRUE);
        /*
           dbg(DEBUG,"OWNER OF <%s> IS <%s>",
           prgRecFldDat[ipTbl][ipRec][ilFld2].NewDat, pclTmpBuf);
         */
        if ((strlen (pclTmpBuf) > 0) && (strcmp (pclTmpBuf, "XX88") != 0))
        {
          strcpy (pcgTmpBuf1, "K");
        }                       /* end if */
      }                         /* end if */
    }                           /* end if */
    if (pcgTmpBuf1[0] == 'X')
    {
      pcgTmpBuf1[0] = 'B';
    }                           /* end if */
    ilGetRc = SetNewValues (ipTbl, ipRec, ilFld1, "", pcgTmpBuf1);
  }                             /* end if */
  return;
}                               /* HardCodePaid */

/* **************************************************** */
/* **************************************************** */
static int BuildFkey (char *pcpResult, char *pcpFltn, char *pcpAlc3, char *pcpFlns, char *pcpFday, char *pcpAdid)
{
  int ilRC = RC_SUCCESS;
  int ilLen;
  int ilPos;
  char pclTmpBuf[128];
  strcpy (pclTmpBuf, "00000####YYYYMMDD#");

  ilLen = strlen (pcpFltn);
  ilPos = 5 - ilLen;
  StrgPutStrg (pclTmpBuf, &ilPos, pcpFltn, 0, -1, "\0");

  ilLen = strlen (pcpAlc3);
  ilPos = 8 - ilLen;
  StrgPutStrg (pclTmpBuf, &ilPos, pcpAlc3, 0, -1, "\0");
  if (pcpFlns[0] != ' ')
  {
    ilPos = 8;
    StrgPutStrg (pclTmpBuf, &ilPos, pcpFlns, 0, -1, "\0");
  }                             /* end if */

  ilPos = 9;
  StrgPutStrg (pclTmpBuf, &ilPos, pcpFday, 0, 7, "\0");

  ilPos = 17;
  StrgPutStrg (pclTmpBuf, &ilPos, pcpAdid, 0, -1, "\0");

  strcpy (pcpResult, pclTmpBuf);
  return ilRC;
}                               /* end BuildFkey */

/* **************************************************** */
/* **************************************************** */
static void HardCodeFkey (int ipTbl, int ipRec, int ipSetVal, char *pcpResult)
{
  int ilGetRc = RC_SUCCESS;
  int ilFld = 0;
  int ilFtyp = 0;
  int ilRkey = 0;
  int ilLen = 0;
  int ilFltnLen = 0;
  int ilPos = 0;
  int ilFldCnt = 0;
  char pclFldVal[16];
  char pclTmpBuf[128];
  char pclSqlWhere[128];
  char pclTables[32];
  char pclCurrAdid[8];
  char *pclFields = NULL;

  (void) BuildSqlReadFields (ipTbl, "URNO,RKEY,RTYP,JFNO,JCNT,ADID,ACT3,ACT5,STOD", FOR_NORMAL);
  pclFields = prgPrjTblCfg[ipTbl].ActFldLst;
  
  ilRkey = GetFieldIndex (ipTbl, "RKEY");

  strcpy (pclTmpBuf, "00000####YYYYMMDD#");
  ilFld = GetFieldIndex (ipTbl, "FLTN");
  ilFltnLen = get_real_item (pclFldVal, prgRecFldDat[ipTbl][ipRec][ilFld].NewDat, 1);
  tool_filter_spaces (pclFldVal);
  ilPos = 5 - ilFltnLen;
  StrgPutStrg (pclTmpBuf, &ilPos, pclFldVal, 0, -1, "\0");

  ilFld = GetFieldIndex (ipTbl, "ALC3");
  ilLen = get_real_item (pclFldVal, prgRecFldDat[ipTbl][ipRec][ilFld].NewDat, 1);
  tool_filter_spaces (pclFldVal);

/**********   TWE 211299  **********/
  if ((ilLen != 3) && (igTWEImport == TRUE))
  {
    dbg (TRACE, "HardCodeFkey: TWEREM: ALC3 <%s> is not valid", pclFldVal);
    ilFld = GetFieldIndex (ipTbl, "ALC2");
    ilLen = get_real_item (pclFldVal, prgRecFldDat[ipTbl][ipRec][ilFld].NewDat, 1);
    if (ilLen != 2)
    {
      ilLen = get_real_item (pclFldVal, prgRecFldDat[ipTbl][ipRec][ilFld].RcvDat, 1);
    }
    dbg (TRACE, "HardCodeFkey: Take ALC2 <%s> for FKEY", pclFldVal);
  }
/***********************************/

  ilPos = 8 - ilLen;
  StrgPutStrg (pclTmpBuf, &ilPos, pclFldVal, 0, -1, "\0");

  ilFld = GetFieldIndex (ipTbl, "FLNS");
  ilLen = get_real_item (pclFldVal, prgRecFldDat[ipTbl][ipRec][ilFld].NewDat, 1);
  ilPos = 8;
  StrgPutStrg (pclTmpBuf, &ilPos, pclFldVal, 0, -1, "\0");

  ilFld = GetFieldIndex (ipTbl, "ADID");
  ilLen = get_real_item (pclFldVal, prgRecFldDat[ipTbl][ipRec][ilFld].NewDat, 1);
  ilPos = 17;
  ilFtyp = GetFieldIndex (ipTbl, "FTYP");
  switch (prgRecFldDat[ipTbl][ipRec][ilFtyp].NewDat[0])
  {
    case 'T':
      pclFldVal[0] = 'T';
      break;
    case 'G':
      pclFldVal[0] = 'G';
      break;
    default:
      break;
  }                             /* end switch */
  StrgPutStrg (pclTmpBuf, &ilPos, pclFldVal, 0, -1, "\0");

  if (pclFldVal[0] == 'A')
  {
    ilFld = GetFieldIndex (ipTbl, "STOA");
  }                             /* end if */
  else
  {
    ilFld = GetFieldIndex (ipTbl, "STOD");
  }                             /* end else */
  ilLen = get_real_item (pclFldVal, prgRecFldDat[ipTbl][ipRec][ilFld].NewDat, 1);

/**********   TWE 211299  **********/
  if ((ilLen <= 0) && (igTWEImport == TRUE))
  {
    dbg (TRACE, "HardCodeFkey: TWEREM: ADID <%c> has no STOA/STOD", pclFldVal[0]);
    dbg (TRACE, "HardCodeFkey: TAKE FLDA FOR SEARCH");
    ilFld = GetFieldIndex (ipTbl, "FLDA");
    ilLen = get_real_item (pclFldVal, prgRecFldDat[ipTbl][ipRec][ilFld].NewDat, 1);
    if ((ilLen <= 0) && (igTWEImport == TRUE))
    {
      dbg (TRACE, "HardCodeFkey: TWEREM: ADID <%c> has no FLDA", pclFldVal[0]);
    }
  }
/***********************************/

  ilPos = 9;
  StrgPutStrg (pclTmpBuf, &ilPos, pclFldVal, 0, 7, "\0");

  if (ipSetVal == TRUE)
  {
    ilFld = GetFieldIndex (ipTbl, "FKEY");
    (void) SetNewValues (ipTbl, ipRec, ilFld, "", pclTmpBuf);

    if ((igCheckFlightKey == TRUE) && (ilFltnLen > 0) && (pclTmpBuf[17] != 'B'))
    {
      if (prgRecFldDat[ipTbl][ipRec][ilFld].NewFlg == TRUE)
      {
/*****   TWE  20012000  ****/
        if ((igTWERoundtrip == TRUE) && (igDontCallSql != TRUE))
        {
          pcgDataArea[0] = 0x00;
          strcpy (pclTables, pcgAftTab);
          (void) TWESearchAndDelRoundtrip (pcgAftTab, pclTmpBuf, ipTbl, ipRec);
        }
        else if (igDontCallSql != TRUE)
        {
          strcpy (pclTables, pcgAftTab);

          if (igTWEImport == TRUE) 
          {
            igCheckFkeyFtyp = FALSE;
          }

          if (igCheckFkeyFtyp == TRUE)
          {
            sprintf (pclSqlWhere, "WHERE FKEY='%s' AND FTYP='%s'", pclTmpBuf,
                     prgRecFldDat[ipTbl][ipRec][ilFtyp].NewDat);
          }                     /* end if */
          else
          {
            sprintf (pclSqlWhere, "WHERE FKEY='%s'", pclTmpBuf);
          }                     /* end else */

          dbg (TRACE, "ACT ON <%s>", pclTmpBuf);

          pcgDataArea[0] = 0x00;
          if (igDontCallSql != TRUE)
          {
            ilGetRc = SearchFlight (pclTables, pclFields,
                                    pclSqlWhere, FOR_SEARCH, TRUE);
            if (ilGetRc == RC_SUCCESS)
            {
              dbg (DEBUG, "FLIGHT <%s> EXITS !", pclTmpBuf);
              StoreErrorData ("4711", "99", "Flight already exists", pclTmpBuf);

/**********   TWE 11012000  **********/
              if (igTWEImport == TRUE)
              {
                /* +++ jim +++ 20010123: get data from record to DBS-fields */
                ilFldCnt = field_count (pclFields);
                BuildItemBuffer (pcgDataArea, "", ilFldCnt , ",");
                /* dbg(DEBUG,"%d.: DATA <%s>",ilNoOfRec,pcgDataArea); */
                (void) GetRecordData (ipTbl, ipRec, ilFldCnt, pclTables, 
                                      pclFields, pcgDataArea);
                /* +++ jim +++ 20010123: END get data from record to DBS-fields */
                /* +++ jim +++ 20010130: check New-Fields against DBS-fields: */
                UnFlgIfDbsEqNew(ipTbl, ipRec);
                
                igTWEUpdate = TRUE;
                igCurrentAction = FOR_UPDATE;

                ilFld = get_item_no (pclFields, "URNO", 5) + 1;
                (void) get_real_item (pcgTWEUpdUrno, pcgDataArea, ilFld);
                ilFld = get_item_no (pclFields, "RKEY", 5) + 1;
                (void) get_real_item (pcgCurrRkey, pcgDataArea, ilFld);
                ilFld = get_item_no (pclFields, "RTYP", 5) + 1;
                (void) get_real_item (pcgCurrRTyp, pcgDataArea, ilFld);
                ilFld = get_item_no (pclFields, "JFNO", 5) + 1;
                (void) GetDataItem (pcgCurrJfno, pcgDataArea, ilFld, ',', "\0", "\0\0");
                ilFld = get_item_no (pclFields, "JCNT", 5) + 1;
                (void) get_real_item (pcgCurrJcnt, pcgDataArea, ilFld);
                ilFld = get_item_no (pclFields, "ADID", 5) + 1;
                (void) get_real_item (pclCurrAdid, pcgDataArea, ilFld);
                dbg (DEBUG, "Recovering RCV URNO with DB data <%s>", pcgTWEUpdUrno);
                StoreReceivedData (FALSE, ipTbl, ipRec, 1, "", "URNO", pcgTWEUpdUrno);
                (void) SetNewValues (ipTbl, ipRec, 0, "URNO", pcgTWEUpdUrno);

                if (igDataLineCount == 1)
                { /* there can not be any new rotation info in the import record */
                  dbg (DEBUG, "Overwriting Import RKEY/RTYP with DB data <%s,%s>", pcgCurrRkey,pcgCurrRTyp);
                  StoreReceivedData (FALSE, ipTbl, ipRec, 1, "", "RTYP", pcgCurrRTyp);
                  StoreReceivedData (FALSE, ipTbl, ipRec, 1, "", "RKEY", pcgCurrRkey);
                  (void) SetNewValues (ipTbl, ipRec, 0, "RTYP", pcgCurrRTyp);
                  (void) SetNewValues (ipTbl, ipRec, 0, "RKEY", pcgCurrRkey);
                }                           /* end if */
                else
                { /* there can be new rotation info in the import record */
                  if (pclCurrAdid[0] == 'A')
                  {
                    if ((prgRecFldDat[ipTbl][ipRec][ilRkey].NewFlg != TRUE) &&
                        (prgRecFldDat[ipTbl][ipRec][ilRkey].RcvFlg != TRUE) )
                    { /* no new data, recover old data */
                      dbg (DEBUG, "Overwriting Import RKEY/RTYP with URNO/J <%s>", pcgTWEUpdUrno);
                      StoreReceivedData (FALSE, ipTbl, ipRec, 1, "", "RTYP", "J");
                      StoreReceivedData (FALSE, ipTbl, ipRec, 1, "", "RKEY", pcgTWEUpdUrno);
                      (void) SetNewValues (ipTbl, ipRec, 0, "RTYP", "J");
                      (void) SetNewValues (ipTbl, ipRec, 0, "RKEY", pcgTWEUpdUrno);
                    }
                    else
                    {
                      dbg (DEBUG, "RKEY/RTYP not touched, Flags NEW/RCV: <%d,%d> ", 
                           prgRecFldDat[ipTbl][ipRec][ilRkey].NewFlg,
                           prgRecFldDat[ipTbl][ipRec][ilRkey].RcvFlg);
                    }
                  }
                  else
                  { /* departure: rotation data must now be in ilRec-1 */
                    dbg (DEBUG, "Overwriting Import RKEY/RTYP with Arrival data <%s,J> (%d)", 
                                       prgRecFldDat[ipTbl][ipRec-1][ilRkey].RcvDat,ipRec);
                    StoreReceivedData (FALSE, ipTbl, ipRec, 1, "", "RTYP", "J");
                    StoreReceivedData (FALSE, ipTbl, ipRec, 1, "", "RKEY", 
                                       prgRecFldDat[ipTbl][ipRec-1][ilRkey].RcvDat);
                    (void) SetNewValues (ipTbl, ipRec, 0, "RTYP", "J");
                    (void) SetNewValues (ipTbl, ipRec, 0, "RKEY", 
                                       prgRecFldDat[ipTbl][ipRec-1][ilRkey].NewDat);
                  }
                }                           /* end if */

                /* get old value JFNO add eventuell
                   new values (case of slave flight
                   and store in internal structures */
                if ((pcgCurrJcnt[0] == '\0') || (pcgCurrJcnt[0] == '0'))
                {
                  strcpy (pcgCurrJfno, "");
                }
                (void) TWEAddJfnoToRecord (ipTbl, ipRec, TRUE);
              }                 /* TWE Import */
/*************************************/
              if ((igTWEUpdate == TRUE) && (igTWEImport == TRUE))
              {
                dbg (DEBUG, "UPDATE FLIGHT RECORD BY TWE");
              }
              else if (igIgnoreDoubles == TRUE)
              {
                dbg (TRACE, "IGNORE FLIGHT RECORD");
              }                 /* end if */
              else if (igUpdateDoubles == TRUE)
              {
                dbg (TRACE, "UPDATE FLIGHT RECORD");
              }                 /* end else if */
              else if (igDeleteDoubles == TRUE)
              {
                if (igTWEImport == TRUE)
                {
                  if (igTWEUrnoIsNumber == TRUE)
                  {
                    sprintf (pclSqlWhere, "WHERE URNO = %s", pcgTWEUpdUrno);
                  }
                  else
                  {
                    sprintf (pclSqlWhere, "WHERE URNO = '%s'", pcgTWEUpdUrno);
                  }
                }
                dbg (TRACE, "DELETE AND INSERT FLIGHT RECORD <%s>", pclSqlWhere);
                DeleteBySelection (pclTables, pclSqlWhere);
              }                 /* end else if */
              else
              {
                dbg (TRACE, "CONTINUE TRANSACTION");
              }                 /* end else */
            }                   /* end if flight exists */
            else
            {
              dbg (DEBUG, "FLIGHT <%s> DOES NOT EXITS !", pclTmpBuf);
              /* flight not found */
              /* +++ jim +++ 20001227: moved from InsertFlights to here: */
              if (igDataLineCount > 1)
              {
                StoreReceivedData (FALSE, ipTbl, 1, 1, "", "RTYP", "J");
                StoreReceivedData (FALSE, ipTbl, 2, 1, "", "RTYP", "J");
                (void) SetNewValues (ipTbl, 1, 0, "RTYP", "J");
                (void) SetNewValues (ipTbl, 2, 0, "RTYP", "J");
              }
              else
              {
                StoreReceivedData (FALSE, ipTbl, 1, 1, "", "RTYP", "S");
                (void) SetNewValues (ipTbl, 1, 0, "RTYP", "S");
              }
              /* +++ jim +++ 20001227: moved from InsertFlights to here / END */
              pcgCurrJcnt[0] = 0x00;
              pcgCurrJfno[0] = 0x00;
              (void) TWEAddJfnoToRecord (ipTbl, ipRec, FALSE);
            }
          }                     /* end if */
        }                       /* if call sql */
      }                         /* end if */
    }                           /* end if */
  }                             /* end if */
  else
  {
    strcpy (pcpResult, pclTmpBuf);
  }                             /* end else */

  return;
}                               /* HardCodeFkey */

/* **************************************************** */
/* **************************************************** */
static void HardCodeStat (int ipTbl, int ipRec)
{
  int ilFtyp = 0;
  int ilStat = 0;
  ilStat = GetFieldIndex (ipTbl, "STAT");
  ilFtyp = GetFieldIndex (ipTbl, "FTYP");
  if ((prgRecFldDat[ipTbl][ipRec][ilFtyp].DbsDat[0] == 'Z') || (prgRecFldDat[ipTbl][ipRec][ilFtyp].DbsDat[0] == 'B'))
  {
    if (prgRecFldDat[ipTbl][ipRec][ilFtyp].NewDat[0] != 'O')
    {
      dbg (TRACE, "UPDATE ON RETURNING FLIGHT");
      if (prgRecFldDat[ipTbl][ipRec][ilFtyp].NewFlg == TRUE)
      {
        dbg (TRACE, "UPDATE FTYP FROM <%s> TO <%s> REJECTED", prgRecFldDat[ipTbl][ipRec][ilFtyp].DbsDat,
             prgRecFldDat[ipTbl][ipRec][ilFtyp].NewDat);
        dbg (TRACE, "ALL RECEIVED DATA FIELDS IGNORED");
        UnSetNewValues (ipTbl, ipRec, 0, prgPrjTblCfg[ipTbl].AllFldLst, TRUE);
        return;
      }                         /* end if */

      /* HERE WE ARE SURE TO UPDATE THE RECORD */
      /* But ignore all dangerous fields */
      UnSetNewValues (ipTbl, ipRec, 0, "VIAL,VIA3,VIA4,VIAN,REM1,DES3,DES4,STAT,OFBD,OFBS,AIRA,AIRD", TRUE);
      if (igClientAction != TRUE)
      {

  TRACE_FIELD_OF_INTEREST(__LINE__,"31");

        UnSetNewValues (ipTbl, ipRec, 0, "FLNO,FLTN,FLNS,ACT3,ACT5,ALC2,ALC3,REGN,STOD,PSTD", TRUE);

  TRACE_FIELD_OF_INTEREST(__LINE__,"32");

      }                         /* end if */
      UnSetNewValues (ipTbl, ipRec, 0, "STOA", FALSE);
    }                           /* end if */
    else
    {
      dbg (TRACE, "RESET RETURNED FLIGHT TO OPERATIONAL");
      HandleReturnFlight (FALSE, prgRecFldDat[ipTbl][ipRec][ilFtyp].DbsDat, ipTbl, ipRec);
    }                           /* end else */
  }                             /* end if */
  else
  {
    switch (prgRecFldDat[ipTbl][ipRec][ilFtyp].NewDat[0])
    {
      case ' ':                /* New Status: ask RST */
        (void) SetNewValues (ipTbl, ipRec, ilStat, "STAT", " ");
        break;
      case 'S':
        (void) SetNewValues (ipTbl, ipRec, ilStat, "STAT", " ");
        break;
      case 'O':
        (void) SetNewValues (ipTbl, ipRec, ilStat, "STAT", " ");
        break;
      case 'D':
        (void) SetNewValues (ipTbl, ipRec, ilStat, "STAT", "DIV");
        break;
      case 'X':
        (void) SetNewValues (ipTbl, ipRec, ilStat, "STAT", "CXX");
        break;
      case 'R':
        (void) SetNewValues (ipTbl, ipRec, ilStat, "STAT", "RRO");
        break;
      case 'N':
        (void) SetNewValues (ipTbl, ipRec, ilStat, "STAT", "NOP");
        break;
      case 'T':
        (void) SetNewValues (ipTbl, ipRec, ilStat, "STAT", " ");
        break;
      case 'G':
        (void) SetNewValues (ipTbl, ipRec, ilStat, "STAT", " ");
        break;
      case 'Z':                /* see below */
        break;
      case 'B':                /* see below */
        break;
      default:
        dbg (TRACE, "UNKNOWN FTYP <%s> STAT SET TO <ERR>", prgRecFldDat[ipTbl][ipRec][ilFtyp].NewDat);
        (void) SetNewValues (ipTbl, ipRec, ilStat, "STAT", "ERR");
        break;
    }                           /* end switch */

    switch (prgRecFldDat[ipTbl][ipRec][ilFtyp].NewDat[0])
    {
      case 'Z':
        dbg (TRACE, "FLIGHT RETURNS FROM AIR");
        HandleReturnFlight (TRUE, "Z", ipTbl, ipRec);
        break;
      case 'B':
        dbg (TRACE, "FLIGHT RETURNS FROM TAXI");
        HandleReturnFlight (TRUE, "B", ipTbl, ipRec);
        break;
      default:
        break;
    }                           /* end switch */
  }                             /* end else */
  return;
}                               /* HardCodeStat */

/* **************************************************** */
/* **************************************************** */
static void HandleReturnFlight (int ipBack, char *pcpFtyp, int ipTbl, int ipRec)
{
  int ilFtyp = 0;
  int ilStat = 0;
  int ilDes3 = 0;
  int ilDes4 = 0;
  int ilRem1 = 0;
  int ilLndu = 0;
  int ilAiru = 0;
  int ilAdid = 0;
  int ilFlno = 0;
  int ilFlns = 0;
  int ilOfbl = 0;
  int ilAirb = 0;
  int ilVia3 = 0;
  int ilVia4 = 0;
  int ilStoa = 0;
  int ilTimeDiff = 0;
  char pclNxtLeg[16];
  char pclFlno[16];
  char pclStoa[32];
  char pclOldDest[256];
  char pclTmpBuf[256];
  char pclDbgCmt[64];
  char *pclRemStoa = NULL;

  if (pcpFtyp[0] == 'Z')
  {
    strcpy (pclDbgCmt, "RETURN FLIGHT");
  }                             /* end if */
  else
  {
    strcpy (pclDbgCmt, "RETURN TAXI");
  }                             /* end else */

  ilStat = GetFieldIndex (ipTbl, "STAT");
  ilFtyp = GetFieldIndex (ipTbl, "FTYP");
  ilDes3 = GetFieldIndex (ipTbl, "DES3");
  ilDes4 = GetFieldIndex (ipTbl, "DES4");
  ilRem1 = GetFieldIndex (ipTbl, "REM1");
  ilLndu = GetFieldIndex (ipTbl, "LNDU");
  ilAiru = GetFieldIndex (ipTbl, "AIRU");
  ilAdid = GetFieldIndex (ipTbl, "ADID");
  ilFlno = GetFieldIndex (ipTbl, "FLNO");
  ilFlns = GetFieldIndex (ipTbl, "FLNS");
  ilOfbl = GetFieldIndex (ipTbl, "OFBL");
  ilAirb = GetFieldIndex (ipTbl, "AIRB");
  ilVia3 = GetFieldIndex (ipTbl, "VIA3");
  ilVia4 = GetFieldIndex (ipTbl, "VIA4");
  ilStoa = GetFieldIndex (ipTbl, "STOA");

  /* in any case we have to make sure */
  /* that we leave all fields unchanged */
  dbg (TRACE, "ALL RECEIVED DATA FIELDS IGNORED");
  UnSetNewValues (ipTbl, ipRec, 0, prgPrjTblCfg[ipTbl].AllFldLst, TRUE);

  if (ipBack == TRUE)
  {
    /* RETURNING FLIGHT */
    /* Allowed only for Operational Flights */
    if (prgRecFldDat[ipTbl][ipRec][ilFtyp].DbsDat[0] != 'O')
    {
      dbg (TRACE, "%s: NOT ALLOWED FOR FTYP <%s>", pclDbgCmt, prgRecFldDat[ipTbl][ipRec][ilFtyp].DbsDat);
      return;
    }                           /* end if */

    /* Allowed only for Departure Flights */
    if (prgRecFldDat[ipTbl][ipRec][ilAdid].DbsDat[0] != 'D')
    {
      dbg (TRACE, "%s: NOT ALLOWED FOR ADID <%s>", pclDbgCmt, prgRecFldDat[ipTbl][ipRec][ilAdid].DbsDat);
      return;
    }                           /* end if */

    if (pcpFtyp[0] == 'Z')
    {
      /* ReturnFlight must be AirBorne */
      if (prgRecFldDat[ipTbl][ipRec][ilAirb].DbsDat[0] == ' ')
      {
        dbg (TRACE, "%s: NOT YET AIRBORNE", pclDbgCmt);
        return;
      }                         /* end if */
    }                           /* end if */
    else
    {
      /* ReturnTaxi must be Offblock */
      if (prgRecFldDat[ipTbl][ipRec][ilOfbl].DbsDat[0] == ' ')
      {
        dbg (TRACE, "%s: NOT YET OFFBLOCK", pclDbgCmt);
        return;
      }                         /* end if */
      /* ReturnTaxi must not be AirBorne */
      if (prgRecFldDat[ipTbl][ipRec][ilAirb].DbsDat[0] != ' ')
      {
        dbg (TRACE, "%s: ALREADY AIRBORNE <%s>", pclDbgCmt, prgRecFldDat[ipTbl][ipRec][ilAirb].DbsDat);
        return;
      }                         /* end if */
    }                           /* end else */

    /* HE WE ARE SURE TO CONTINUE */
    /* The only changed FieldValue should be FTYP */
    (void) SetNewValues (ipTbl, ipRec, ilFtyp, "FTYP", pcpFtyp);

    igUnlockDssf = TRUE;
    strcpy (pclStoa, prgRecFldDat[ipTbl][ipRec][ilStoa].NewDat);
    if (prgRecFldDat[ipTbl][ipRec][ilStoa].NewLen > 0)
    {
      GetServerTimeStamp ("", 101, 0, pclStoa);
    }                           /* end if */

    if (pcpFtyp[0] == 'Z')
    {
      /* Return Flight */
      sprintf (pclOldDest, "%s <%s/%s> --> %s <%s>", prgLableText[9].Text, prgRecFldDat[ipTbl][ipRec][ilDes3].NewDat,
               prgRecFldDat[ipTbl][ipRec][ilDes4].NewDat, prgLableText[10].Text, pclStoa);
      sprintf (pclTmpBuf, "[%-3.3s%-1.1s]", prgRecFldDat[ipTbl][ipRec][ilDes3].NewDat,
               prgRecFldDat[ipTbl][ipRec][ilFlns].NewDat);
      ilTimeDiff = igSysMinStamp - prgRecFldDat[ipTbl][ipRec][ilAirb].NewVal;
      if (ilTimeDiff < 0)
      {
        ilTimeDiff = abs (ilTimeDiff) + 15;
      }                         /* end if */
      else if (ilTimeDiff < 15)
      {
        ilTimeDiff = 15;
      }                         /* end else if */
      else if (ilTimeDiff > 60)
      {
        ilTimeDiff = 60;
      }                         /* end else if */
    }                           /* end if */
    else
    {
      /* Return Taxi */
      sprintf (pclOldDest, "%s <%s/%s> --> %s <%s>", prgLableText[4].Text, prgRecFldDat[ipTbl][ipRec][ilDes3].NewDat,
               prgRecFldDat[ipTbl][ipRec][ilDes4].NewDat, prgLableText[5].Text, pclStoa);
      sprintf (pclTmpBuf, "<%-3.3s%-1.1s>", prgRecFldDat[ipTbl][ipRec][ilDes3].NewDat,
               prgRecFldDat[ipTbl][ipRec][ilFlns].NewDat);
      ilTimeDiff = igSysMinStamp - prgRecFldDat[ipTbl][ipRec][ilOfbl].NewVal;
      if (ilTimeDiff < 0)
      {
        ilTimeDiff = abs (ilTimeDiff) + 5;
      }                         /* end if */
      else if (ilTimeDiff < 5)
      {
        ilTimeDiff = 5;
      }                         /* end else if */
      else if (ilTimeDiff > 15)
      {
        ilTimeDiff = 15;
      }                         /* end else if */
    }                           /* end else */

    if (prgRecFldDat[ipTbl][ipRec][ilVia3].NewLen > 0)
    {
      sprintf (pclNxtLeg, "%-3.3s/%-4.4s", prgRecFldDat[ipTbl][ipRec][ilVia3].NewDat,
               prgRecFldDat[ipTbl][ipRec][ilVia4].NewDat);
    }                           /* end if */
    else
    {
      sprintf (pclNxtLeg, "%-3.3s/%-4.4s", prgRecFldDat[ipTbl][ipRec][ilDes3].NewDat,
               prgRecFldDat[ipTbl][ipRec][ilDes4].NewDat);
    }                           /* end else */
    (void) SetNewValues (ipTbl, ipRec, ilStat, "STAT", pclTmpBuf);

    if (pcpFtyp[0] == 'Z')
    {
      /* Return Flight */
      sprintf (pclTmpBuf, "%s <%s> %s <%s> %s %s %c%c%s", prgLableText[6].Text,
               prgRecFldDat[ipTbl][ipRec][ilFlno].NewDat, prgLableText[7].Text, pclNxtLeg, prgLableText[8].Text,
               pcgH3LC, cgPtCR, cgPtLF, pclOldDest);
      (void) SetNewValues (ipTbl, ipRec, ilFlns, "FLNS", "D");
      sprintf (pclFlno, "%-9.9s", prgRecFldDat[ipTbl][ipRec][ilFlno].NewDat);
      pclFlno[8] = 'D';
      (void) SetNewValues (ipTbl, ipRec, ilFlno, "FLNO", pclFlno);
      (void) SetNewValues (ipTbl, ipRec, 0, "CHGI", "RETURN FLIGHT");
    }                           /* end if */
    else
    {
      /* Return Taxi */
      sprintf (pclTmpBuf, "%s <%s> %s <%s> %s %c%c%s", prgLableText[1].Text, prgRecFldDat[ipTbl][ipRec][ilFlno].NewDat,
               prgLableText[2].Text, pclNxtLeg, prgLableText[3].Text, cgPtCR, cgPtLF, pclOldDest);
      (void) SetNewValues (ipTbl, ipRec, ilAiru, "AIRU", prgRecFldDat[ipTbl][ipRec][ilOfbl].NewDat);
      (void) SetNewValues (ipTbl, ipRec, ilLndu, "LNDU", prgRecFldDat[ipTbl][ipRec][ilOfbl].NewDat);
      (void) SetNewValues (ipTbl, ipRec, 0, "CHGI", "RETURN TAXI");
    }                           /* end else */
    (void) SetNewValues (ipTbl, ipRec, ilRem1, "REM1", pclTmpBuf);
    (void) SetNewValues (ipTbl, ipRec, 0, "ISRE", "+");
    (void) SetNewValues (ipTbl, ipRec, ilAdid, "ADID", "B");
    (void) SetNewValues (ipTbl, ipRec, ilDes3, "Des3", pcgH3LC);
    (void) SetNewValues (ipTbl, ipRec, ilDes4, "Des4", pcgH4LC);
    ilTimeDiff *= 60;
    GetServerTimeStamp (pcgStampType, 1, ilTimeDiff, pclStoa);
    dbg (TRACE, "USED TIME DIFF = %d <%s>", ilTimeDiff, pclStoa);
    (void) SetNewValues (ipTbl, ipRec, ilStoa, "STOA", pclStoa);
    igChangeInfo = FALSE;
  }                             /* end if */
  else
  {
    /* RESET TO OPERATION */
    /* The only changed FieldValue should be FTYP */
    (void) SetNewValues (ipTbl, ipRec, ilFtyp, "FTYP", "O");

    igUnlockDssf = TRUE;
    strcpy (pclTmpBuf, &prgRecFldDat[ipTbl][ipRec][ilStat].DbsDat[1]);
    pclTmpBuf[3] = 0x00;        /* Original Destination */
    (void) SetNewValues (ipTbl, ipRec, ilDes3, "DES3", pclTmpBuf);
    (void) SetNewValues (ipTbl, ipRec, ilDes4, "DES4", " ");
    if (pcpFtyp[0] == 'Z')
    {
      /* Return Flight */
      strcpy (pclTmpBuf, &prgRecFldDat[ipTbl][ipRec][ilStat].DbsDat[4]);
      pclTmpBuf[1] = 0x00;      /* Original Suffix */
      (void) SetNewValues (ipTbl, ipRec, ilFlns, "FLNS", pclTmpBuf);
      sprintf (pclFlno, "%-9.9s", prgRecFldDat[ipTbl][ipRec][ilFlno].DbsDat);
      pclFlno[8] = pclTmpBuf[0];
      (void) SetNewValues (ipTbl, ipRec, ilFlno, "FLNO", pclFlno);
    }                           /* end if */
    else
    {
      /* Return Taxi */
      (void) SetNewValues (ipTbl, ipRec, ilAiru, "AIRU", " ");
      (void) SetNewValues (ipTbl, ipRec, 0, "AIRA", " ");
      (void) SetNewValues (ipTbl, ipRec, 0, "AIRD", " ");
    }                           /* end else */
    (void) SetNewValues (ipTbl, ipRec, ilLndu, "LNDU", " ");
    (void) SetNewValues (ipTbl, ipRec, 0, "LNDA", " ");
    (void) SetNewValues (ipTbl, ipRec, 0, "LNDD", " ");
    (void) SetNewValues (ipTbl, ipRec, 0, "ONBU", " ");
    (void) SetNewValues (ipTbl, ipRec, 0, "ONBD", " ");
    (void) SetNewValues (ipTbl, ipRec, 0, "ONBS", " ");
    (void) SetNewValues (ipTbl, ipRec, 0, "CHGI", "SET OPERATIONAL");
    (void) SetNewValues (ipTbl, ipRec, ilRem1, "REM1", " ");
    (void) SetNewValues (ipTbl, ipRec, 0, "ISRE", " ");
    (void) SetNewValues (ipTbl, ipRec, ilStat, "STAT", " ");
    /* Original STOA */
    strcpy (pclStoa, " ");
    pclRemStoa = strstr (prgRecFldDat[ipTbl][ipRec][ilRem1].DbsDat, "-->");
    dbg (TRACE, "<%s>", prgRecFldDat[ipTbl][ipRec][ilRem1].DbsDat);
    if (pclRemStoa != NULL)
    {
      if (pclRemStoa != NULL)
      {
        pclRemStoa = strstr (pclRemStoa, "<");
        pclRemStoa++;
        strcpy (pclStoa, pclRemStoa);
        GetServerTimeStamp ("", 102, 0, pclStoa);
      }                         /* end if */
    }                           /* end if */
    (void) SetNewValues (ipTbl, ipRec, ilStoa, "STOA", pclStoa);
    igChangeInfo = FALSE;
  }                             /* end else */
  return;
}                               /* end HandleReturnFlight */

/* **************************************************** */
/* **************************************************** */
static void HardCodeCsgn (int ipTbl, int ipRec)
{
  int ilFtyp;
  int ilFld;
  int ilLen;
  int ilPos;
  int ilSrcPos = 0;
  char pclFltn[16];
  char pclAlc3[16];
  char pclTmpBuf[16];
  char pclChkAlc[16];

  strcpy (pclTmpBuf, "          ");

  ilFtyp = GetFieldIndex (ipTbl, "FTYP");
  if (prgRecFldDat[ipTbl][ipRec][ilFtyp].NewDat[0] == 'T')
  {
    strcpy (pclTmpBuf, "TOWING");
  }                             /* end if */
  if (prgRecFldDat[ipTbl][ipRec][ilFtyp].NewDat[0] == 'G')
  {
    strcpy (pclTmpBuf, "GRDMOV");
  }                             /* end if */

  if (pclTmpBuf[0] == ' ')
  {
    ilFld = GetFieldIndex (ipTbl, "FLTN");
    ilLen = get_real_item (pclFltn, prgRecFldDat[ipTbl][ipRec][ilFld].NewDat, 1);
    if (ilLen > 0)
    {
      ilFld = GetFieldIndex (ipTbl, "ALC3");
      ilLen = get_real_item (pclAlc3, prgRecFldDat[ipTbl][ipRec][ilFld].NewDat, 1);
      ilPos = 0;
      StrgPutStrg (pclTmpBuf, &ilPos, pclAlc3, 0, -1, "\0");

      sprintf (pclChkAlc, ",%s,", pclAlc3);
      ilSrcPos = 0;
      if (strstr (pcgCsgnCutFltn, pclChkAlc) != NULL)
      {
        /*
           if ((pclFltn[0] >= '1') && (pclFltn[0] <='7'))
           {
           }
         */
        ilSrcPos = 1;
      }                         /* end if */

      ilPos = 3;
      StrgPutStrg (pclTmpBuf, &ilPos, pclFltn, ilSrcPos, -1, "\0");

      str_trm_rgt (pclTmpBuf, " ", TRUE);
    }                           /* end if */
    else
    {
      ilFld = GetFieldIndex (ipTbl, "REGN");
      ilLen = get_real_item (pclTmpBuf, prgRecFldDat[ipTbl][ipRec][ilFld].NewDat, 1);
    }                           /* end else */
  }                             /* end if */

  ilFld = GetFieldIndex (ipTbl, "CSGN");
  (void) SetNewValues (ipTbl, ipRec, ilFld, "", pclTmpBuf);

  return;
}                               /* HardCodeCsgn */

/* **************************************************** */
/* **************************************************** */
static void HardCodeDoop (int ipTbl, int ipRec, char *pcpFldNam, char *pcpRelFld)
{
  int ilGetRc = RC_SUCCESS;
  int ilTgtFld;
  int ilSrcFld;
  int ilDay = 0;
  int ilMin = 0;
  int ilWkd = 0;
  char pclTmpBuf[8];

  ilTgtFld = GetFieldIndex (ipTbl, pcpFldNam);
  ilSrcFld = GetFieldIndex (ipTbl, pcpRelFld);
  if ((ilTgtFld > 0) && (ilSrcFld > 0))
  {
    strcpy (pclTmpBuf, " ");
    if (prgRecFldDat[ipTbl][ipRec][ilSrcFld].NewLen > 0)
    {
      ilGetRc = GetFullDay (prgRecFldDat[ipTbl][ipRec][ilSrcFld].NewDat, &ilDay, &ilMin, &ilWkd);
      if (ilGetRc == RC_SUCCESS)
      {
        sprintf (pclTmpBuf, "%d", ilWkd);
      }                         /* end if */
    }                           /* end if */
    (void) SetNewValues (ipTbl, ipRec, ilTgtFld, "", pclTmpBuf);
  }                             /* end if */

  return;
}                               /* HardCodeDoop */

/* **************************************************** */
/* **************************************************** */
static void HardCodeSeas (int ipTbl, int ipRec)
{
  int ilRC = RC_SUCCESS;
  int ilTgtFld;
  int ilRelFld;
  int ilSrcFld;
  char pclSeaTab[16];
  char pclSrcFld[8];
  char pclTmpBuf[32];
  char pclTmpLine[64];
  sprintf (pclSeaTab, "SEA%s", pcgBasExt);

  ilTgtFld = GetFieldIndex (ipTbl, "SEAS");
  ilRelFld = GetFieldIndex (ipTbl, "ADID");
  if ((ilTgtFld > 0) && (ilRelFld > 0))
  {
    if (prgRecFldDat[ipTbl][ipRec][ilRelFld].NewDat[0] == 'D')
    {
      strcpy (pclSrcFld, "STOD");
    }                           /* end if */
    else
    {
      strcpy (pclSrcFld, "STOA");
    }                           /* end else */
    ilSrcFld = GetFieldIndex (ipTbl, pclSrcFld);
    if (ilSrcFld > 0)
    {
      if (prgRecFldDat[ipTbl][ipRec][ilSrcFld].NewLen > 0)
      {
        ilRC =
          GetBasicRange (pclSeaTab, "SEAS", prgRecFldDat[ipTbl][ipRec][ilSrcFld].NewDat, "SEAS,VPFR,VPTO", pclTmpBuf,
                         pclTmpLine, 0, RC_FAIL);
        if (ilRC == RC_SUCCESS)
        {
          (void) SetNewValues (ipTbl, ipRec, ilTgtFld, "", pclTmpBuf);
        }                       /* end if */
      }                         /* end if */
    }                           /* end if */
  }                             /* end if */

  return;
}                               /* HardCodeSeas */

/* **************************************************** */
/* **************************************************** */
static void HardCodeVias (int ipTbl, int ipRec)
{
  int ilVial = 0;
  int ilAdid = 0;
  int ilOrg3 = 0;
  int ilDes3 = 0;
  int ilApc3 = 0;
  int ilApc4 = 0;
  int ilVian = 0;
  int ilStoa = 0;
  int ilStod = 0;
  int ilTime = 0;
  int ilGrpCnt = 0;
  int ilGrpNbr = 0;
  int ilNxtGrp = 0;
  int ilNxtApc = 0;
  int ilTimeSet = TRUE;
  int ilNearVia3Idx = 0;
  int ilTimeVia3Idx = 0;
  int ilNearVia4Idx = 0;
  int ilTimeVia4Idx = 0;
  char pclTmpBuf[32];

  ilVial = GetFieldIndex (ipTbl, "VIAL");
  ilVian = 0;

  if (prgRecFldDat[ipTbl][ipRec][ilVial].NewLen > 0)
  {
    ilAdid = GetFieldIndex (ipTbl, "ADID");
    ilOrg3 = GetFieldIndex (ipTbl, "ORG3");
    ilDes3 = GetFieldIndex (ipTbl, "DES3");
    ilApc3 = GetFieldIndex (ipTbl, "AP31");
    ilApc4 = GetFieldIndex (ipTbl, "AP41");
    ilGrpCnt = prgTblFldCfg[ipTbl][ilVial].ChildGroups;
    for (ilGrpNbr = 1; ilGrpNbr <= ilGrpCnt; ilGrpNbr++)
    {
      if ((prgRecFldDat[ipTbl][ipRec][ilApc3].NewLen > 0) || (prgRecFldDat[ipTbl][ipRec][ilApc4].NewLen > 0))
      {
        if ((strcmp (prgRecFldDat[ipTbl][ipRec][ilApc3].NewDat, prgRecFldDat[ipTbl][ipRec][ilOrg3].NewDat) == 0) ||
            (strcmp (prgRecFldDat[ipTbl][ipRec][ilApc3].NewDat, prgRecFldDat[ipTbl][ipRec][ilDes3].NewDat) == 0))
        {
          dbg (TRACE, "CLEAR DATA OF ORIG/DEST <%s> IN VIA LIST", prgRecFldDat[ipTbl][ipRec][ilApc3].NewDat);
          (void) ClearExplodeGroup (ipTbl, ipRec, ilVial, ilGrpNbr);
        }                       /* end if */
        else
        {
          ilNxtApc = ilApc3;
          for (ilNxtGrp = ilGrpNbr + 1; ilNxtGrp <= ilGrpCnt; ilNxtGrp++)
          {
            ilNxtApc++;
            if (strcmp (prgRecFldDat[ipTbl][ipRec][ilApc3].NewDat, prgRecFldDat[ipTbl][ipRec][ilNxtApc].NewDat) == 0)
            {
              dbg (TRACE, "CLEAR DATA OF DUPL APC3 <%s> IN VIA LIST", prgRecFldDat[ipTbl][ipRec][ilApc3].NewDat);
              if (prgRecFldDat[ipTbl][ipRec][ilAdid].NewDat[0] == 'D')
              {
                (void) ClearExplodeGroup (ipTbl, ipRec, ilVial, ilNxtGrp);
              }                 /* end if */
              else
              {
                (void) ClearExplodeGroup (ipTbl, ipRec, ilVial, ilGrpNbr);
              }                 /* end else */
            }                   /* end if */
          }                     /* end for */
        }                       /* end else */
      }                         /* end if */
      ilApc3++;
      ilApc4++;
    }                           /* end for */

    ilApc4 = GetFieldIndex (ipTbl, "AP41");
    ilApc3 = GetFieldIndex (ipTbl, "AP31");
    ilStoa = GetFieldIndex (ipTbl, "STA1");
    ilStod = GetFieldIndex (ipTbl, "STD1");
    ilGrpCnt = prgTblFldCfg[ipTbl][ilVial].ChildGroups;
    ilTimeSet = TRUE;
    ilTime = -1;
    while (ilGrpCnt > 0)
    {
      if ((prgRecFldDat[ipTbl][ipRec][ilApc3].NewLen > 0) || (prgRecFldDat[ipTbl][ipRec][ilApc4].NewLen > 0))
      {
        ilVian++;
        switch (prgRecFldDat[ipTbl][ipRec][ilAdid].NewDat[0])
        {
          case 'D':
            /* Departure Flight */
            /* The 'near' Via is the first entry in VIAL == NearVia3 */
            /* or the Via with min(STOA) in that list    == Via3 */
            if (ilTime < 0)
            {
              /* Here we get the first entry */
              ilNearVia3Idx = ilApc3;
              ilNearVia4Idx = ilApc4;
              ilTimeVia3Idx = ilApc3;
              ilTimeVia4Idx = ilApc4;
              ilTime = prgRecFldDat[ipTbl][ipRec][ilStoa].NewVal;
            }                   /* end if */
            if (prgRecFldDat[ipTbl][ipRec][ilStoa].NewVal <= 0)
            {
              ilTimeSet = FALSE;
            }                   /* end if */
            if ((ilTimeSet == TRUE) && (prgRecFldDat[ipTbl][ipRec][ilStoa].NewVal > 0) &&
                (prgRecFldDat[ipTbl][ipRec][ilStoa].NewVal < ilTime))
            {
              ilTimeVia3Idx = ilApc3;
              ilTimeVia4Idx = ilApc4;
              ilTime = prgRecFldDat[ipTbl][ipRec][ilStoa].NewVal;
            }                   /* end if */
            break;

          default:
            /* Arrival or Circulation Flight */
            /* We need the last entry in VIAL */
            /* or the max(STOD) of that List */
            ilNearVia3Idx = ilApc3;
            ilNearVia4Idx = ilApc4;
            if (prgRecFldDat[ipTbl][ipRec][ilStod].NewVal <= 0)
            {
              ilTimeSet = FALSE;
            }                   /* end if */
            if ((ilTimeSet == TRUE) && (prgRecFldDat[ipTbl][ipRec][ilStod].NewVal > ilTime))
            {
              ilTimeVia3Idx = ilApc3;
              ilTimeVia4Idx = ilApc4;
              ilTime = prgRecFldDat[ipTbl][ipRec][ilStod].NewVal;
            }                   /* end if */
            break;
        }                       /* end switch */
      }                         /* end if */
      ilApc3++;
      ilApc4++;
      ilStoa++;
      ilStod++;
      ilGrpCnt--;
    }                           /* end while */
  }                             /* end if */

  if (ilVian > 0)
  {
    sprintf (pclTmpBuf, "%d", ilVian);
    if (ilTimeSet == TRUE)
    {
      ilNearVia3Idx = ilTimeVia3Idx;
      ilNearVia4Idx = ilTimeVia4Idx;
    }                           /* end if */
    (void) SetNewValues (ipTbl, ipRec, 0, "VIAN", pclTmpBuf);
    (void) SetNewValues (ipTbl, ipRec, 0, "VIA3", prgRecFldDat[ipTbl][ipRec][ilNearVia3Idx].NewDat);
    (void) SetNewValues (ipTbl, ipRec, 0, "VIA4", prgRecFldDat[ipTbl][ipRec][ilNearVia4Idx].NewDat);
  }                             /* end if */
  else
  {
    (void) SetNewValues (ipTbl, ipRec, 0, "VIAN", "0");
    (void) SetNewValues (ipTbl, ipRec, 0, "VIA3", " ");
    (void) SetNewValues (ipTbl, ipRec, 0, "VIA4", " ");
  }                             /* end else */

  return;
}                               /* HardCodeVias */

/* **************************************************** */
/* **************************************************** */
static int ClearExplodeGroup (int ipTbl, int ipRec, int ipFld, int ipGrp)
{
  int ilRC = RC_SUCCESS;
  int ilFldCnt = 0;
  int ilFldNbr = 0;
  int ilGrpCnt = 0;
  int ilFldIdx = 0;
  dbg (TRACE, "MAIN  FIELD <%s>", prgTblFldCfg[ipTbl][ipFld].SysTabFina);
  dbg (TRACE, "MAIN  FIRST   (%d)", prgTblFldCfg[ipTbl][ipFld].FirstChild);
  dbg (TRACE, "MAIN  GROUPS  (%d)", prgTblFldCfg[ipTbl][ipFld].ChildGroups);
  dbg (TRACE, "MAIN  MEMBERS (%d)", prgTblFldCfg[ipTbl][ipFld].GroupMembers);
  dbg (TRACE, "CLEAR LINE  (%d)", ipGrp);
  ilGrpCnt = prgTblFldCfg[ipTbl][ipFld].ChildGroups;
  ilFldCnt = prgTblFldCfg[ipTbl][ipFld].GroupMembers;
  ilFldIdx = prgTblFldCfg[ipTbl][ipFld].FirstChild + ipGrp - 1;
  for (ilFldNbr = 1; ilFldNbr <= ilFldCnt; ilFldNbr++)
  {
    dbg (TRACE, "CLEAR FIELD <%s>", prgTblFldCfg[ipTbl][ilFldIdx].SysTabFina);
    (void) SetNewValues (ipTbl, ipRec, ilFldIdx, "", " ");
    ilFldIdx += ilGrpCnt;
  }                             /* end for */
  prgRecFldDat[ipTbl][ipRec][ipFld].NewFlg = TRUE;

  return ilRC;
}                               /* ClearExplodeGroup */

/* **************************************************** */
/* **************************************************** */
static int DeleteBySelection (char *pcpTable, char *pcpSelKey)
{
  int ilRC = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  short slCursor;
  char pclSqlBuf[1024];
  char pclSqlDat[1024];

  sprintf (pclSqlBuf, "DELETE FROM %s %s", pcpTable, pcpSelKey);
  pclSqlDat[0] = 0x00;
  slCursor = 0;
  dbg (DEBUG, "%s", pclSqlBuf);
  
  ilGetRc = sql_if (IGNORE, &slCursor, pclSqlBuf, pclSqlDat);
  
  if (ilGetRc == DB_SUCCESS)
  {
    dbg (DEBUG, "RECORD(S) DELETED %s", pclSqlDat);
    ilRC = RC_SUCCESS;
  }                             /* end if */
  else
  {
    ilRC = RC_FAIL;
  }                             /* end else */
  commit_work ();
  close_my_cursor (&slCursor);
  return ilRC;
}                               /* end DeleteBySelection */

/* **************************************************** */
/* **************************************************** */
static int ReorgAftTable (char *pcpTbl)
{
  int ilRC = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  int ilRecCnt = 0;
  int ilUtcDiff = 0;
  short slCursor;
  short slFkt;
  char pclSqlBuf[2048];
  char pclSqlDat[128];
  char pclUpdSel[128];
  char pclUpdFld[128];
  char pclUpdDat[128];

  dbg (TRACE, "REORG TABLE <%s> START", pcpTbl);
  strcpy (pcgTwStart, "REORG");
  sprintf (pcgTwEnd, "%s,%s", pcgH3LC, pcgTblExt);
  ilUtcDiff = igUtcDiff;
  if (igUtcDiff != 0)
  {
    dbg (TRACE, "SHIFTING TIMES BY %d MINUTES", igUtcDiff);
    sprintf (pcgTwStart, "REORG,UTC,%d", ilUtcDiff);
  }                             /* end if */
  if (strlen (pcgReorgKey) > 0)
  {
    sprintf (pclSqlBuf, "SELECT URNO FROM %s WHERE %s", pcpTbl, pcgReorgKey);
  }                             /* end if */
  else
  {
    sprintf (pclSqlBuf, "SELECT URNO FROM %s ", pcpTbl);
  }                             /* end else */
  dbg (TRACE, "<%s>", pclSqlBuf);
  strcpy (pcgRecvName, "CEDA");
  strcpy (pcgDestName, "AutoCheck");
  strcpy (pclUpdFld, "URNO");
  pclSqlDat[0] = 0x00;
  slCursor = 0;
  slFkt = START;
  ilRecCnt = 0;
  ilGetRc = DB_SUCCESS;
  
  while (ilGetRc == DB_SUCCESS)
  {
    ilGetRc = sql_if (slFkt, &slCursor, pclSqlBuf, pclSqlDat);
    if (ilGetRc == DB_SUCCESS)
    {
      ilRecCnt++;
      (void) get_real_item (pclUpdDat, pclSqlDat, 1);
      if (igTWEUrnoIsNumber == TRUE)
      {
        sprintf (pclUpdSel, "WHERE URNO=%s", pclUpdDat);
      }
      else
      {
        sprintf (pclUpdSel, "WHERE URNO='%s'", pclUpdDat);
      }

      (void) tools_send_info_flag (7800, 0, pcgDestName, "", pcgRecvName, "", "", pcgTwStart, pcgTwEnd, "ROT", pcpTbl,
                                   pclUpdSel, "URNO", pclUpdDat, 5);
    }                           /* end if */
    slFkt = NEXT;
  }                             /* end while */
  close_my_cursor (&slCursor);
  
  dbg (TRACE, "REORG TABLE <%s> READY %d RECORDS CHECKED", pcpTbl, ilRecCnt);
  return ilRC;
}                               /* end ReorgAftTable */

/********************************************************/
/********************************************************/
static int SetUtcDiffTime (int ipTblIdx, int ipUtcOffset, int ipAllFields)
{
  int ilRC = RC_SUCCESS;
  int ilTbl = -1;
  int ilFstTbl = -1;
  int ilLstTbl = -1;
  int ilRec = -1;
  int ilRecCnt = -1;
  int ilFld = -1;
  int ilFldCnt = -1;
  int ilMinuteOffset = 0;
  char pclSeconds[4];
  char pclNewUtcTime[32];

  ilMinuteOffset = ipUtcOffset;
  if (ipTblIdx > 0)
  {
    ilFstTbl = ipTblIdx;
    ilLstTbl = ipTblIdx;
  }                             /* end if */
  else
  {
    ilFstTbl = 1;
    ilLstTbl = igHandledTables;
  }                             /* end else */

  for (ilTbl = ilFstTbl; ilTbl <= ilLstTbl; ilTbl++)
  {
    if (prgPrjTblCfg[ilTbl].RcvFlg == TRUE)
    {
      ilRecCnt = prgPrjTblCfg[ilTbl].RecHdlCnt;
      for (ilRec = 1; ilRec <= ilRecCnt; ilRec++)
      {
        ilFldCnt = prgPrjTblCfg[ilTbl].AllFldCnt;
        for (ilFld = 1; ilFld <= ilFldCnt; ilFld++)
        {
          /* DATE */
          if ((prgTblFldCfg[ilTbl][ilFld].FieldType == 3) &&
              ((prgTblFldCfg[ilTbl][ilFld].AutoChange == TRUE) || (ipAllFields == TRUE)))
          {
            if (prgRecFldDat[ilTbl][ilRec][ilFld].NewVal > 0)
            {
              pclSeconds[0] = prgRecFldDat[ilTbl][ilRec][ilFld].NewDat[12];
              pclSeconds[1] = prgRecFldDat[ilTbl][ilRec][ilFld].NewDat[13];
              pclSeconds[2] = 0x00;
              prgRecFldDat[ilTbl][ilRec][ilFld].NewVal = prgRecFldDat[ilTbl][ilRec][ilFld].NewVal + ilMinuteOffset;
              dbg (TRACE, "NewVal = %ld", prgRecFldDat[ilTbl][ilRec][ilFld].NewVal);
              (void) FullTimeString (pclNewUtcTime, prgRecFldDat[ilTbl][ilRec][ilFld].NewVal, pclSeconds);
              if (igDebugDetails == TRUE)
              {
                dbg (TRACE, "TIMES <%s> OLD <%s> NEW <%s>", prgTblFldCfg[ilTbl][ilFld].SysTabFina,
                     prgRecFldDat[ilTbl][ilRec][ilFld].NewDat, pclNewUtcTime);
              }                 /* end if */

              (void) SetNewValues (ilTbl, ilRec, ilFld, "", pclNewUtcTime);
            }                   /* end if */
          }                     /* end if */
        }                       /* end for */
      }                         /* end for */
    }                           /* end if */
  }                             /* end for */
  return ilRC;
}                               /* end SetUtcDiffTime */

/********************************************************/
/********************************************************/
static int FlucoPeriodToCeda (char *pcpVpfrCeda, char *pcpVptoCeda, char *pcpVpfrFluco, char *pcpVptoFluco)
{
  int ilRC = RC_SUCCESS;
  char pclSeaTab[16];
  char pclCurrSeas[16];
  char pclSeasLine[64];
  char pclSeasVpfr[16];
  char pclSeasVpto[16];

  char pclVpfrDate[16];
  char pclVpfrTime[16];
  char pclVpfrSeas[16];
  char pclVpfrVpfr[16];
  char pclVpfrVpto[16];

  char pclVptoDate[16];
  char pclVptoTime[16];
  char pclVptoSeas[16];
  char pclVptoVpfr[16];
  char pclVptoVpto[16];

  int ilDatCnt = 0;

  pcpVpfrCeda[0] = 0x00;
  pcpVptoCeda[0] = 0x00;
  sprintf (pclSeaTab, "SEA%s", pcgBasExt);
  ilRC = GetBasicRange (pclSeaTab, "SEAS", pcgCurTime, "SEAS,VPFR,VPTO", pclCurrSeas, pclSeasLine, 0, RC_FAIL);
  if (ilRC == RC_SUCCESS)
  {
    /* dbg(TRACE,"CURR.SEAS <%s> <%s>", pclCurrSeas, pclSeasLine); */
    (void) get_real_item (pclSeasVpfr, pclSeasLine, 2);
    (void) get_real_item (pclSeasVpto, pclSeasLine, 3);

    if (strlen (pcpVpfrFluco) > 0)
    {
      DeleteCharacterInString (pcpVpfrFluco, ' ');
      (void) FlucoDateToCeda ("", pclVpfrDate, pcpVpfrFluco);
      sprintf (pclVpfrTime, "%s000000", pclVpfrDate);
      ilRC = GetBasicRange (pclSeaTab, "SEAS", pclVpfrTime, "SEAS,VPFR,VPTO", pclVpfrSeas, pclSeasLine, 0, RC_FAIL);
      if (ilRC == RC_SUCCESS)
      {
        /* dbg(TRACE,"VPFR.SEAS <%s> <%s>", pclVpfrSeas, pclSeasLine); */
        (void) get_real_item (pclVpfrVpfr, pclSeasLine, 2);
        (void) get_real_item (pclVpfrVpto, pclSeasLine, 3);
        if (strcmp (pclVpfrVpto, pclSeasVpfr) < 0)
        {
          /* dbg(TRACE,"DATE OUT OF RANGE. SHIFT IT TO NEXT YEAR"); */
          (void) MoveYear (pclVpfrDate, 1);
        }                       /* end if */
        ilDatCnt++;
        strcpy (pcpVpfrCeda, pclVpfrDate);
      }                         /* end if */
    }                           /* end if */

    if (strlen (pcpVptoFluco) > 0)
    {
      DeleteCharacterInString (pcpVptoFluco, ' ');
      (void) FlucoDateToCeda ("", pclVptoDate, pcpVptoFluco);
      sprintf (pclVptoTime, "%s000000", pclVptoDate);
      ilRC = GetBasicRange (pclSeaTab, "SEAS", pclVptoTime, "SEAS,VPFR,VPTO", pclVptoSeas, pclSeasLine, 0, RC_FAIL);
      if (ilRC == RC_SUCCESS)
      {
        /* dbg(TRACE,"VPTO.SEAS <%s> <%s>", pclVptoSeas, pclSeasLine); */
        (void) get_real_item (pclVptoVpfr, pclSeasLine, 2);
        (void) get_real_item (pclVptoVpto, pclSeasLine, 3);
        if (strcmp (pclVptoVpto, pclSeasVpfr) < 0)
        {
          /* dbg(TRACE,"DATE OUT OF RANGE. SHIFT IT TO NEXT YEAR"); */
          (void) MoveYear (pclVptoDate, 1);
        }                       /* end if */
        ilDatCnt++;
        strcpy (pcpVptoCeda, pclVptoDate);
      }                         /* end if */
    }                           /* end if */

    if (ilDatCnt == 2)
    {
      /* To Be Sure */
      if (strcmp (pclVptoDate, pclVpfrDate) < 0)
      {
        /* dbg(TRACE,"DATE OUT OF RANGE. SHIFT IT TO NEXT YEAR"); */
        (void) MoveYear (pclVptoDate, 1);
        strcpy (pcpVptoCeda, pclVptoDate);
      }                         /* end if */
    }                           /* end if */
  }                             /* end if */
  /*
     dbg(TRACE,"CONVERT <%s> <%s> PERIOD <%s> <%s>",
     pcpVpfrFluco, pcpVptoFluco, pclVpfrDate, pclVptoDate);
   */
  return ilRC;
}                               /* end FlucoPeriodToCeda */

/********************************************************/
/********************************************************/
static int MoveYear (char *pcpDate, int ipOffset)
{
  int ilRC = RC_SUCCESS;
  int ilYearVal = 0;
  char pclTmpBuf[8];
  strncpy (pclTmpBuf, pcpDate, 4);
  pclTmpBuf[4] = 0x00;
  ilYearVal = atoi (pclTmpBuf) + ipOffset;
  sprintf (pclTmpBuf, "%4d", ilYearVal);
  strncpy (pcpDate, pclTmpBuf, 4);
  return ilRC;
}                               /* end MoveYear */

/********************************************************/
/********************************************************/
static int FlucoDateToCeda (char *pcpYearStrg, char *pcpCedaDate, char *pcpFlucoDate)
{
  int ilRC = RC_SUCCESS;
  int ilMonVal = 0;
  char pclYear[8];
  char pclDay[4];
  char pclMonthEng[] = "JAN,FEB,MAR,APR,MAY,JUN,JUL,AUG,SEP,OCT,NOV,DEC";

  if (strlen (pcpYearStrg) == 4)
  {
    strcpy (pclYear, pcpYearStrg);
  }                             /* end if */
  else
  {
    strncpy (pclYear, pcgCurTime, 4);
    pclYear[4] = 0x00;
  }                             /* end else */
  strncpy (pclDay, pcpFlucoDate, 2);
  pclDay[2] = 0x00;
  ilMonVal = get_item_no (pclMonthEng, &pcpFlucoDate[2], 4) + 1;

  sprintf (pcpCedaDate, "%s%02d%s", pclYear, ilMonVal, pclDay);
  if (strlen (pcpYearStrg) == 4)
  {
    dbg (TRACE, "YEAR <%s> FLUCO <%s> CEDA <%s>", pcpYearStrg, pcpFlucoDate, pcpCedaDate);
  }                             /* end if */
  return ilRC;
}                               /* end FlucoDateToCeda */

/********************************************************/
/********************************************************/
static int CheckRulFndCall (int ipTbl, int ipRec)
{
  int ilRC = RC_FAIL;
  int ilFld = 0;
  int ilFldCnt = 0;
  if (igToRulFnd > 0)
  {
    dbg (DEBUG, "CHECK RMS CALL");

    if (igRulFndCallAlways == TRUE)
    {
      dbg (TRACE, "SEND ALWAYS IS ACTIVE");
    }                           /* end if */
    ilFldCnt = prgPrjTblCfg[ipTbl].TblFldCnt;
    ilFld = 1;
    while (ilFld <= ilFldCnt)
    {
      if (((prgRecFldDat[ipTbl][ipRec][ilFld].NewFlg == TRUE) && (prgTblFldCfg[ipTbl][ilFld].SendRulFnd == TRUE)) ||
          (igRulFndCallAlways == TRUE))
      {
        dbg (DEBUG, "CHANGED RMS FIELD <%s>", prgTblFldCfg[ipTbl][ilFld].SysTabFina);

        ilRC = RC_SUCCESS;
        ilFld += ilFldCnt;
      }                         /* end if */
      ilFld++;
    }                           /* end while */
  }                             /* end if */
  return ilRC;
}                               /* end CheckRulFndCall */

/********************************************************/
/********************************************************/
static int CheckCcaHdlCall (void)
{
  int ilRC = RC_SUCCESS;
  int ilTbl = 0;
  int ilRec = 0;
  int ilRecCnt = 0;
  int ilAdid = 0;
  int ilStod = 0;
  int ilUrno = 0;
  int ilMinOff = 0;
  ilTbl = GetTableIndex (pcgAftTab);

  ilAdid = GetFieldIndex (ilTbl, "ADID");
  ilStod = GetFieldIndex (ilTbl, "STOD");
  ilUrno = GetFieldIndex (ilTbl, "URNO");
  ilRecCnt = prgPrjTblCfg[ilTbl].RecHdlCnt;
  for (ilRec = 1; ilRec <= ilRecCnt; ilRec++)
  {
    if ((prgRecFldDat[ilTbl][ilRec][ilAdid].NewDat[0] != 'A') && (prgRecFldDat[ilTbl][ilRec][ilStod].NewFlg == TRUE))
    {
      ilMinOff = prgRecFldDat[ilTbl][ilRec][ilStod].NewVal - prgRecFldDat[ilTbl][ilRec][ilStod].DbsVal;
      dbg (TRACE, "STOD <%s> <%s> DIFF (%d) MIN", prgRecFldDat[ilTbl][ilRec][ilStod].DbsDat,
           prgRecFldDat[ilTbl][ilRec][ilStod].NewDat, ilMinOff);
      if (ilMinOff != 0)
      {
        (void) MoveCcaTimes (prgRecFldDat[ilTbl][ilRec][ilUrno].NewDat, ilMinOff);
/*
        if ((strcmp(prgRecFldDat[ilTbl][ilRec][ilBaz1].DbsDat," ") != 0) || 
            (strcmp(prgRecFldDat[ilTbl][ilRec][ilBaz1].NewDat," ") != 0) || 
            (strcmp(prgRecFldDat[ilTbl][ilRec][ilBaz1].RcvDat," ") != 0))
        {
        }
*/
      }                         /* end if */
    }                           /* end if */
  }                             /* end for */
  return ilRC;
}                               /* end CheckCcaHdlCall */

/********************************************************/
/********************************************************/
static int SendRecordsToRulFnd (char *pcpCmd, int ipTbl, char *pcpSelKey, char *pcpKeyValues)
{
  int ilRC = RC_SUCCESS;
  int ilDatPos = 0;
  int ilRecCnt = 0;
  int ilAdid = 0;
  short slCursor = 0;
  short slFkt = 0;
  char pclAdid[4];
  char pclSqlBuf[4096];
  char pclSqlDat[4096];
  char pclSqlKey[4096];
  char pclRecDat[4096 * 4];

  if (igToRulFnd < 1)
  {
    return ilRC;
  }                             /* end if */

  strcpy (pclSqlKey, pcpSelKey);
  CheckWhereClause (TRUE, pclSqlKey, FALSE, TRUE, "\0");

  strcpy (pclSqlDat, pcpKeyValues);
  delton (pclSqlDat);

  sprintf (pclSqlBuf, "SELECT %s FROM %s %s", prgPrjTblCfg[ipTbl].RmsFldLst, prgPrjTblCfg[ipTbl].FullName, pclSqlKey);
  dbg (TRACE, "SEND RECORDS TO RULFND\n<%s>", pclSqlBuf);
  ilAdid = get_item_no (prgPrjTblCfg[ipTbl].RmsFldLst, "ADID", 5) + 1;

  slCursor = 0;
  slFkt = START;
  
  while (sql_if (slFkt, &slCursor, pclSqlBuf, pclSqlDat) == DB_SUCCESS)
  {
    BuildItemBuffer (pclSqlDat, "", prgPrjTblCfg[ipTbl].RmsFldCnt, ",");
    ilRecCnt++;
    if (ilAdid > 0)
    {
      get_real_item (pclAdid, pclSqlDat, ilAdid);
      switch (pclAdid[0])
      {
        case 'A':
          StrgPutStrg (pclRecDat, &ilDatPos, pclSqlDat, 0, -1, "\n");
          break;
        case 'B':
          StrgPutStrg (pclRecDat, &ilDatPos, pclSqlDat, 0, -1, "\n");
          break;
        case 'D':
          if (ilRecCnt == 1)
          {
            pclRecDat[ilDatPos] = '\n';
            ilDatPos++;
          }                     /* end if */
          StrgPutStrg (pclRecDat, &ilDatPos, pclSqlDat, 0, -1, "\0");
          break;
        default:
          StrgPutStrg (pclRecDat, &ilDatPos, pclSqlDat, 0, -1, "\n");
          break;
      }                         /* end switch */
    }                           /* end if */
    else
    {
      StrgPutStrg (pclRecDat, &ilDatPos, pclSqlDat, 0, -1, "\n");
    }                           /* end else */
    slFkt = NEXT;
  }                             /* end while */
  close_my_cursor (&slCursor);
  

  if (ilDatPos > 0)
  {
    pclRecDat[ilDatPos] = 0x00;
    dbg (TRACE, "<%s>", pclRecDat);

    (void) tools_send_info_flag (igToRulFnd, 0, pcgDestName, "", pcgRecvName, "", "", "RMS", pcgTwEnd, pcpCmd,
                                 prgPrjTblCfg[ipTbl].ShortName, "SEL", prgPrjTblCfg[ipTbl].RmsFldLst, pclRecDat, 0);
  }                             /* end if */
  return ilRC;
}                               /* end SendRecordsToRulFnd */

/********************************************************/
/********************************************************/
static int MoveCcaTimes (char *pcpAftUrno, int ipMinOff)
{
  int ilRC = RC_SUCCESS;
  int ilTbl = 0;
  char pclCcaSelKey[128];
  char pclFields[128];
  strcpy (pclFields, "URNO,CKIC,CKBS,CKES");
  ilTbl = GetTableIndex (pcgCcaTab);
  ilRC = SetReadAlways (ilTbl, pclFields);
  dbg (TRACE, "SEARCH CCA RECORDS OF URNO <%s>", pcpAftUrno);
  sprintf (pclCcaSelKey, "WHERE FLNU=%s", pcpAftUrno);
  dbg (TRACE, "<%s>", pclCcaSelKey);
  ilRC = SearchFlight (pcgCcaTab, pclFields, pclCcaSelKey, FOR_UPDATE, TRUE);
  if (ilRC == RC_SUCCESS)
  {
    prgPrjTblCfg[ilTbl].RcvFlg = TRUE;
    ilRC = PrepNewData (ilTbl);
    (void) SetUtcDiffTime (ilTbl, ipMinOff, FALSE);
    ilRC = ShowRecordData ();
    dbg (TRACE, "MoveCcaTimes: UPDATE AND BROADCAST FLIGHT WITH URNO <%s> ", pclCcaSelKey);
    ilRC = MakeUpdateAndBroadCast (__LINE__,ilTbl, 0, pclCcaSelKey, TRUE, "URT", "URNO");
  }                             /* end if */
  return ilRC;
}                               /* end MoveCcaTimes */


/* ******************************************************************** */
/* ******************************************************************** */
static void CheckRkeyHistory ()
{
  int ilTbl = 0;
  int ilItmCnt = 0;
  int ilItmNbr = 0;
  int ilLen = 0;
  char pclRkey[16];
  dbg (TRACE, "RKEY LIST <%s>", pcgRkeyList);
  ilTbl = GetTableIndex (pcgAftTab);
  ilItmCnt = field_count (pcgRkeyList);
  for (ilItmNbr = 1; ilItmNbr <= ilItmCnt; ilItmNbr++)
  {
    ilLen = get_real_item (pclRkey, pcgRkeyList, ilItmNbr);
    if (ilLen > 0)
    {
      SendRecordsToRulFnd ("UFR", ilTbl, "WHERE RKEY=:VRKEY AND (ADID='A' OR ADID='D') ORDER BY ADID", pclRkey);
    }                           /* end if */
  }                             /* end for */
  return;
}                               /* end CheckRkeyHistory */

/* ******************************************************************** */
/* ******************************************************************** */
static void CheckRegnHistory (char *pcpListOfRegn)
{
  int ilOverFlow = FALSE;
  int ilTbl = 0;
  int ilTblCnt = 0;
  int ilRec = 0;
  int ilLastRec = 0;
  int ilRec1 = 0;
  int ilRec2 = 0;
  int ilMinRec = 0;
  int ilDataCnt = 0;
  int ilArrCnt = 0; /* +++ jim +++ 20001226: end loop at ilArrCnt > igRegnMaxLines */
  int ilDepCnt = 0; /* +++ jim +++                   and ilDepCnt > igRegnMaxLines */
  int ilRegnCnt = 0;
  int ilRegn = 0;
  int ilRegnLen = 0;
  int ilFldCnt = 0;
  int ilChkRec = TRUE;
  short slLocalCursor;
  short slFkt;
  char pclRegnName[64];
  char pclAllTables[32];
  char pclActTable[16];
  char pclAftFields[512];
  char pclSqlKey[512];

  char pclUrno[16];
  char pclAdid[16];
  char pclRkey[16];
  char pclNewRkey[16];
  char pclRtyp[16];
  char pclAurn[16];
  char pclNewAurn[16];
  char pclFtyp[16];
  char pclTifa[16];
  char pclTifd[16];
  char pclFlno[16];             /* +++ jim +++ 20001219 */

  char pclArrUrno[16];
  char pclArrRkey[16];

  char pclLastUrno[16];
  char pclLastAdid[16];
  char pclLastRtyp[16];
  char pclLastFipsChkData[60] = ""; /* +++ jim +++ 20001219 */
  char pclFipsChkData[60] = ""; /* +++ jim +++ 20001219 */
  char pclFipsChkFlds[] = "ADID,FTYP,TIFA,TIFD,FLNO"; /* +++ jim +++ 20001219 */

  int ilUrnoItm = 0;
  int ilAdidItm = 0;
  int ilRkeyItm = 0;
  int ilRtypItm = 0;
  int ilAurnItm = 0;
  int ilTifdItm = 0;
  int ilTifaItm = 0;
  int ilFtypItm = 0;
  int ilFlnoItm = 0;            /* +++ jim +++ 20001218 */

  int ilErrorSet = FALSE;
  int ilRkeyError = FALSE;
  int ilAurnError = FALSE;

  if (igCheckHistory != TRUE)
  {
    dbg (TRACE, "CHECK HISTORY NOT ACTIVATED");
    return;
  }                             /* end if */

  strcpy (pclAftFields, "ADID,FTYP,FLNO,URNO,RKEY,RTYP,AURN,TIFD,TIFA");

  ilUrnoItm = get_item_no (pclAftFields, "URNO", 5) + 1;
  ilAdidItm = get_item_no (pclAftFields, "ADID", 5) + 1;
  ilRkeyItm = get_item_no (pclAftFields, "RKEY", 5) + 1;
  ilRtypItm = get_item_no (pclAftFields, "RTYP", 5) + 1;
  ilAurnItm = get_item_no (pclAftFields, "AURN", 5) + 1;
  ilTifaItm = get_item_no (pclAftFields, "TIFA", 5) + 1;
  ilTifdItm = get_item_no (pclAftFields, "TIFD", 5) + 1;
  ilFtypItm = get_item_no (pclAftFields, "FTYP", 5) + 1;
  ilFlnoItm = get_item_no (pclAftFields, "FLNO", 5) + 1; /* +++ jim +++ 20001218 */

  strcpy (pclAllTables, pcgAftTab);

  ilRegnCnt = 0;
  ilRegnCnt = field_count (pcpListOfRegn);
  ilFldCnt = field_count (pclAftFields);
  for (ilRegn = 1; ilRegn <= ilRegnCnt; ilRegn++)
  {
    ilRegnLen = get_real_item (pclRegnName, pcpListOfRegn, ilRegn);
    if (ilRegnLen > 0)
    {
      dbg (TRACE, "-----------------------------------------------------");
      dbg (DEBUG, "--- REGN: <%s> READING  HISTORY ---", pclRegnName);
      igRegnLineCount = 2;
      ilDataCnt = 0;
      ilTblCnt = field_count (pclAllTables);
      for (ilTbl = 1; ilTbl <= ilTblCnt; ilTbl++)
      {
        (void) get_real_item (pclActTable, pclAllTables, ilTbl);
        sprintf (pclSqlKey, "WHERE REGN='%s' ORDER BY TIFA DESC", pclRegnName);
        CheckWhereClause (TRUE, pclSqlKey, TRUE, TRUE, "\0");
        sprintf (pcgSqlBuf, "SELECT %s FROM %s %s", pclAftFields, pclActTable, pclSqlKey);
        pcgDataArea[0] = 0x00;
        slLocalCursor = 0;
        slFkt = START;
        
        while (sql_if (slFkt, &slLocalCursor, pcgSqlBuf, pcgDataArea) == DB_SUCCESS)
        { /*  this loop also has a break! */
          BuildItemBuffer (pcgDataArea, "", ilFldCnt, ",");
          if (igRegnLineCount < MAX_REGN_LINES)
          {
            (void) get_real_item (pclUrno, pcgDataArea, ilUrnoItm);
            (void) get_real_item (pclAdid, pcgDataArea, ilAdidItm);
            (void) get_real_item (pclTifa, pcgDataArea, ilTifaItm);
            (void) get_real_item (pclTifd, pcgDataArea, ilTifdItm);
            dbg(DEBUG,"CheckRegnHistory: URNO,ADID,TIFA,TIFD: <%11s> <%1s> <%14s> <%14s>",
                pclUrno, pclAdid, pclTifa, pclTifd);
            switch (pclAdid[0])
            {
              case 'A':
                ilArrCnt++; /* +++ jim +++ 20001226: end loop at ilArrCnt > igRegnMaxLines */
                ilDataCnt++;
                strcpy (prgRegnLines[ilDataCnt].Location, pclActTable);
                strcpy (prgRegnLines[ilDataCnt].LineKey, pclUrno);
                strcpy (prgRegnLines[ilDataCnt].TextLine, pcgDataArea);
                prgRegnLines[ilDataCnt].LineType = 1;
                prgRegnLines[ilDataCnt].ErrorNbr = 0;
                prgRegnLines[ilDataCnt].FldLen = 0;
                prgRegnLines[ilDataCnt].DatLen = 0;
                sprintf (prgRegnLines[ilDataCnt].SortText, "%s%s", pclTifa, pclAdid);
                break;
              case 'B':
                ilArrCnt++; /* +++ jim +++ 20001226: end loop at ilArrCnt > igRegnMaxLines */
                ilDataCnt++;
                ilDepCnt++; /* +++ jim +++ 20001226: end loop at ilArrCnt > igRegnMaxLines */
                pcgDataArea[0] = 'D';
                strcpy (prgRegnLines[ilDataCnt].Location, pclActTable);
                strcpy (prgRegnLines[ilDataCnt].LineKey, pclUrno);
                strcpy (prgRegnLines[ilDataCnt].TextLine, pcgDataArea);
                prgRegnLines[ilDataCnt].LineType = 2;
                prgRegnLines[ilDataCnt].ErrorNbr = 0;
                prgRegnLines[ilDataCnt].FldLen = 0;
                prgRegnLines[ilDataCnt].DatLen = 0;
                sprintf (prgRegnLines[ilDataCnt].SortText, "%s%s", pclTifd, pclAdid);
                ilDataCnt++;
                pcgDataArea[0] = 'A';
                strcpy (prgRegnLines[ilDataCnt].Location, pclActTable);
                strcpy (prgRegnLines[ilDataCnt].TextLine, pcgDataArea);
                prgRegnLines[ilDataCnt].LineType = 2;
                prgRegnLines[ilDataCnt].ErrorNbr = 0;
                prgRegnLines[ilDataCnt].FldLen = 0;
                prgRegnLines[ilDataCnt].DatLen = 0;
                sprintf (prgRegnLines[ilDataCnt].SortText, "%s%s", pclTifa, pclAdid);
                break;
              case 'D':
                ilDepCnt++; /* +++ jim +++ 20001226: end loop at ilArrCnt > igRegnMaxLines */
                ilDataCnt++;
                strcpy (prgRegnLines[ilDataCnt].Location, pclActTable);
                strcpy (prgRegnLines[ilDataCnt].LineKey, pclUrno);
                strcpy (prgRegnLines[ilDataCnt].TextLine, pcgDataArea);
                prgRegnLines[ilDataCnt].LineType = 3;
                prgRegnLines[ilDataCnt].ErrorNbr = 0;
                prgRegnLines[ilDataCnt].FldLen = 0;
                prgRegnLines[ilDataCnt].DatLen = 0;
                sprintf (prgRegnLines[ilDataCnt].SortText, "%s%s", pclTifd, pclAdid);
                break;
              default:
                ilDataCnt++;
                strcpy (prgRegnLines[ilDataCnt].Location, pclActTable);
                strcpy (prgRegnLines[ilDataCnt].LineKey, pclUrno);
                strcpy (prgRegnLines[ilDataCnt].TextLine, pcgDataArea);
                prgRegnLines[ilDataCnt].LineType = 0;
                prgRegnLines[ilDataCnt].ErrorNbr = 0;
                prgRegnLines[ilDataCnt].FldLen = 0;
                prgRegnLines[ilDataCnt].DatLen = 0;
                sprintf (prgRegnLines[ilDataCnt].SortText, " %s", pclAdid);
                break;
            }                   /* end switch */
            igRegnLineCount = ilDataCnt + 2;
          }                     /* end if */
          else
          {
            igRegnLineCount++;
            if (ilOverFlow != TRUE)
            {
              dbg (TRACE, "CHECK HISTORY: BUFFER OVERFLOW");
              ilOverFlow = TRUE;
            }                   /* end if */
          }                     /* end else */
          slFkt = NEXT;
           /* +++ jim +++ 20001226: end loop at ilArrCnt > igRegnMaxLines */
          if ((igRegnHistoryLines > 0) && (ilArrCnt > igRegnHistoryLines)
                                       && (ilDepCnt > igRegnHistoryLines))
          {
             dbg(DEBUG,"CheckRegnHistory: %d Arrivals and %d Departures read, break...",ilArrCnt,ilDepCnt);
             break;  /* <<< === */
          }
        }                       /* end while */
        close_my_cursor (&slLocalCursor);
        
        /* +++ jim +++ 20001226: end loop at ilArrCnt > igRegnMaxLines */
        dbg(DEBUG,"CheckRegnHistory: read %d lines with %d Arrivals and %d Departures",ilDataCnt,ilArrCnt,ilDepCnt);
      }                         /* end for */

      if (igRegnLineCount >= MAX_REGN_LINES)
      {
        dbg (TRACE, "OVERFLOW: FOUND %d RECORDS", igRegnLineCount);
      }                         /* end for */
      dbg (TRACE, "--- REGN: <%s> CHECKING HISTORY ---", pclRegnName);
      igRegnLineCount = ilDataCnt;

      /* Sorting Records */
      for (ilRec1 = 1; ilRec1 < ilDataCnt; ilRec1++)
      {
        ilMinRec = ilRec1;
        for (ilRec2 = ilRec1 + 1; ilRec2 <= ilDataCnt; ilRec2++)
        {
          if (strcmp (prgRegnLines[ilRec2].SortText, prgRegnLines[ilMinRec].SortText) < 0)
          {
            ilMinRec = ilRec2;
          }                     /* end if */
        }                       /* end for */
        if (ilMinRec > ilRec1)
        {
          SwapDataLines (ilRec1, ilMinRec);
        }                       /* end if */
      }                         /* end for */

      /* Checking History and Errors */
      pclLastAdid[0] = 0x00;
      pclLastRtyp[0] = 0x00;
      pclLastUrno[0] = 0x00;
      pclArrUrno[0] = 0x00;
      pclArrRkey[0] = 0x00;
      ilLastRec = 0;
      prgRegnLines[ilLastRec].LineType = -1;

      for (ilRec = 1; ilRec <= ilDataCnt; ilRec++)
      {
        (void) get_real_item (pclUrno, prgRegnLines[ilRec].TextLine, ilUrnoItm);
        (void) get_real_item (pclAdid, prgRegnLines[ilRec].TextLine, ilAdidItm);
        (void) get_real_item (pclRkey, prgRegnLines[ilRec].TextLine, ilRkeyItm);
        (void) get_real_item (pclRtyp, prgRegnLines[ilRec].TextLine, ilRtypItm);
        (void) get_real_item (pclAurn, prgRegnLines[ilRec].TextLine, ilAurnItm);
        (void) get_real_item (pclFtyp, prgRegnLines[ilRec].TextLine, ilFtypItm);
        (void) get_real_item (pclTifa, prgRegnLines[ilRec].TextLine, ilTifaItm); /* +++ jim +++ 20001218 */
        (void) get_real_item (pclTifd, prgRegnLines[ilRec].TextLine, ilTifdItm); /* +++ jim +++ 20001218 */
        (void) get_real_item (pclFlno, prgRegnLines[ilRec].TextLine, ilFlnoItm); /* +++ jim +++ 20001218 */
        sprintf (pclFipsChkData, "%s,%s,%s,%s,%s", pclAdid, pclFtyp, pclTifa, pclTifd, pclFlno);
        ilChkRec = FALSE;
        switch (pclFtyp[0])
        {
          case 'T':
            dbg (DEBUG, "    ---- TOW ----");
            ilChkRec = TRUE;
            break;
          case 'G':
            dbg (DEBUG, "    ---- MVT ----");
            ilChkRec = TRUE;
            break;
          case 'X':
            dbg (DEBUG, "    ---- CXX ----");
            break;
          case 'N':
            dbg (DEBUG, "    ---- NOP ----");
            break;
          case 'D':
            dbg (DEBUG, "    ---- DIV ----");
            break;
          case 'R':
            dbg (DEBUG, "    ---- ROU ----");
            break;
          default:
            ilChkRec = TRUE;
            break;
        }                       /* end switch */

        if (ilChkRec != TRUE)
        {
          /* INACTIVE FLIGHT RECORD */

          fprintf (outp, "%c %d %s\n", prgRegnLines[ilRec].Location[1], prgRegnLines[ilRec].LineType,
                   prgRegnLines[ilRec].TextLine);
          fflush (outp);
/*        if ((strcmp (pclUrno, pclRkey) != 0) || (pclAurn[0] != '\0') || (pclRtyp[0] != 'X')) */
/* +++ jim +++ 20001225: RTYP X gibt es nicht! (stoert im FIMS) */
          if ((strcmp (pclUrno, pclRkey) != 0) || (pclAurn[0] != '\0') || (pclRtyp[0] != 'S'))
          {
            if (igTWEUrnoIsNumber == TRUE)
            {
              sprintf (pcgSqlBuf, "UPDATE %s SET RKEY=%s,AURN=' ',RTYP='S' WHERE URNO=%s", prgRegnLines[ilRec].Location,
                       pclUrno, pclUrno);
            }
            else
            {
              sprintf (pcgSqlBuf, "UPDATE %s SET RKEY='%s',AURN=' ',RTYP='S' WHERE URNO='%s'",
                       prgRegnLines[ilRec].Location, pclUrno, pclUrno);
            }
            dbg (DEBUG, "    <%s>", pcgSqlBuf);
            pcgDataArea[0] = 0x00;
            slLocalCursor = 0;
            
            (void) sql_if (IGNORE, &slLocalCursor, pcgSqlBuf, pcgDataArea);
            
            commit_work ();
            close_my_cursor (&slLocalCursor);
            sprintf (pcgTmpBuf1, "%s, ,X", pclUrno);
            SendChanges (pclUrno, "RKEY,AURN,RTYP", pcgTmpBuf1, pclFipsChkFlds, pclFipsChkData, FALSE);
          }                     /* end if */
        }                       /* end if */

        if (ilChkRec == TRUE)
        {
          /* ACTIVE FLIGHT RECORD */
          /* Check valid Arr/Dep Sequence */

          ilErrorSet = FALSE;
          if (ilLastRec > 0)
          {
            switch (prgRegnLines[ilLastRec].LineType)
            {
                /* ==================== */
              case 1:          /* LAST EVENT = ARRIVAL */
                /* ==================== */
                switch (prgRegnLines[ilRec].LineType)
                {
                  case 1:      /* Actual Event = Arrival */
                    dbg (TRACE, "    ERROR URNO <%s> DUP ARRIVAL A", pclLastUrno);
                    ilErrorSet = TRUE;
                    break;
                  case 2:      /* Actual Event = Roundtrip */
                    if (pclAdid[0] == 'A')
                    {
                      dbg (TRACE, "    ERROR URNO <%s> DUP ARRIVAL B", pclLastUrno);
                      ilErrorSet = TRUE;
                    }           /* end if */
                    break;
                  case 3:      /* Actual Event = Departure */
                    /* Normal Sequence ARR - DEP */
                    break;
                  default:
                    ilErrorSet = FALSE;
                    break;
                }               /* end switch */
                break;

                /* ====================== */
              case 2:          /* LAST EVENT = ROUNDTRIP */
                /* ====================== */
                switch (prgRegnLines[ilRec].LineType)
                {
                  case 1:      /* Actual Event = Arrival */
                    dbg (TRACE, "    ERROR URNO <%s> ARRIVAL FOLLOWS ROUNDTRIP", pclLastUrno);
                    ilErrorSet = TRUE;
                    break;
                  case 2:      /* Actual Event = Roundtrip */
                    break;
                  case 3:      /* Actual Event = Departure */
                    /* Normal Sequence ARR - DEP */
                    break;
                  default:
                    break;
                }               /* end switch */
                break;

                /* ====================== */
              case 3:          /* LAST EVENT = DEPARTURE */
                /* ====================== */
                switch (prgRegnLines[ilRec].LineType)
                {
                    /* ---------------------- */
                  case 1:      /* Actual Event = Arrival */
                    /* ---------------------- */
                    break;
                    /* ------------------------ */
                  case 2:      /* Actual Event = Roundtrip */
                    /* ------------------------ */
                    dbg (TRACE, "    ERROR URNO <%s> DUP DEPARTURE B", pclUrno);
                    ilErrorSet = TRUE;
                    break;
                    /* ------------------------ */
                  case 3:      /* Actual Event = Departure */
                    /* ------------------------ */
                    dbg (TRACE, "    ERROR URNO <%s> DUP DEPARTURE D", pclUrno);
                    ilErrorSet = TRUE;
                    break;
                  default:
                    break;
                }               /* end switch */
                break;

              case -1:         /* To Be Sure */
                break;

              default:         /* Illegal Last Event */
                dbg (TRACE, "    ERROR URNO ROUTING <%s> ADID <%s>", pclLastUrno, pclAdid);
                ilErrorSet = TRUE;
                break;
            }                   /* end switch */
          }                     /* end if */

          /* ==================================== */
          /* Check RKEY and AURN of Actual Record */
          /* ==================================== */
          ilRkeyError = FALSE;
          ilAurnError = FALSE;
          switch (prgRegnLines[ilRec].LineType)
          {
            case 1:            /*  ARRIVAL RECORD */
              if (pclAurn[0] != '\0')
              {
                dbg (DEBUG, "    ERROR AURN <%s> IN ARRIVAL RECORD", pclAurn);
                ilAurnError = TRUE;
                strcpy (pclNewAurn, " ");
              }                 /* end if */
              (void) get_real_item (pclArrRkey, prgRegnLines[ilRec].TextLine, ilUrnoItm);
              (void) get_real_item (pclArrUrno, prgRegnLines[ilRec].TextLine, ilUrnoItm);
              if (strcmp (pclArrRkey, pclRkey) != 0)
              {
                dbg (DEBUG, "    ERROR RKEY SHOULD BE <%s>", pclArrRkey);
                ilRkeyError = TRUE;
                strcpy (pclNewRkey, pclArrRkey);
              }                 /* end if */
              break;

            case 2:            /* ROUNDTRIP RECORD */
              if (pclAdid[0] == 'A')
              {
                if (pclArrRkey[0] != '\0')
                {
                  if (strcmp (pclArrRkey, pclRkey) != 0)
                  {
                    dbg (DEBUG, "    ERROR RKEY SHOULD BE <%s>", pclArrRkey);
                    ilRkeyError = TRUE;
                    strcpy (pclNewRkey, pclArrRkey);
                  }             /* end if */
                }               /* end if */
                else
                {

                  (void) get_real_item (pclArrRkey, prgRegnLines[ilRec].TextLine, ilUrnoItm);
                }               /* end else */
              }                 /* end if */
              if (pclAdid[0] == 'D')
              {
                if ((pclFtyp[0] != 'T') && (pclFtyp[0] != 'G'))
                {
                  if (pclArrUrno[0] != '\0')
                  {
                    if (strcmp (pclArrUrno, pclAurn) != 0)
                    {
                      dbg (DEBUG, "    ERROR AURN SHOULD BE <%s>", pclArrUrno);
                      ilAurnError = TRUE;
                      strcpy (pclNewAurn, pclArrUrno);
                    }           /* end if */
                  }             /* end if */

                  (void) get_real_item (pclArrUrno, prgRegnLines[ilRec].TextLine, ilUrnoItm);
                }               /* end if */
                else
                {
                  if (pclAurn[0] != '\0')
                  {
                    dbg (DEBUG, "    ERROR AURN SHOULD BE <BLANK>");
                    ilAurnError = TRUE;
                    strcpy (pclNewAurn, " ");
                  }             /* end if */
                }               /* end else */
              }                 /* end if */
              break;
            case 3:            /* DEPARTURE ADID == D */
              if (pclArrRkey[0] != '\0')
              {
                if (strcmp (pclArrRkey, pclRkey) != 0)
                {
                  dbg (DEBUG, "    ERROR RKEY SHOULD BE <%s>", pclArrRkey);
                  ilRkeyError = TRUE;
                  strcpy (pclNewRkey, pclArrRkey);
                }               /* end if */
              }                 /* end if */
              if (pclArrUrno[0] != 0x00)
              {
                if (strcmp (pclArrUrno, pclAurn) != 0)
                {
                  dbg (DEBUG, "    ERROR AURN SHOULD BE <%s>", pclArrUrno);
                  ilAurnError = TRUE;
                  strcpy (pclNewAurn, pclArrUrno);
                }               /* end if */
              }                 /* end if */
              pclArrUrno[0] = 0x00;
              pclArrRkey[0] = 0x00;
              break;
            default:
              break;
          }                     /* end switch */

          if (ilErrorSet == FALSE)
          {
            if (ilRkeyError == TRUE)
            {
              if (igTWEUrnoIsNumber == TRUE)
              {
                sprintf (pcgSqlBuf, "UPDATE %s SET RKEY=%s WHERE URNO=%s", prgRegnLines[ilRec].Location, pclNewRkey,
                         pclUrno);
              }
              else
              {
                sprintf (pcgSqlBuf, "UPDATE %s SET RKEY='%s' WHERE URNO='%s'", prgRegnLines[ilRec].Location, pclNewRkey,
                         pclUrno);
              }
              dbg (DEBUG, "    <%s>", pcgSqlBuf);
              pcgDataArea[0] = 0x00;
              slLocalCursor = 0;
              
              (void) sql_if (IGNORE, &slLocalCursor, pcgSqlBuf, pcgDataArea);
              
              commit_work ();
              close_my_cursor (&slLocalCursor);
              sprintf (pcgTmpBuf1, "%s", pclNewRkey);
              SendChanges (pclUrno, "RKEY", pcgTmpBuf1, pclFipsChkFlds, pclFipsChkData, FALSE);
            }                   /* end if */
            if (ilAurnError == TRUE)
            {
              if (igTWEUrnoIsNumber == TRUE)
              {
                sprintf (pcgSqlBuf, "UPDATE %s SET AURN='%s' WHERE URNO=%s", prgRegnLines[ilRec].Location, pclNewAurn,
                         pclUrno);
              }
              else
              {
                sprintf (pcgSqlBuf, "UPDATE %s SET AURN='%s' WHERE URNO='%s'", prgRegnLines[ilRec].Location, pclNewAurn,
                         pclUrno);
              }
              dbg (DEBUG, "    <%s>", pcgSqlBuf);
              pcgDataArea[0] = 0x00;
              slLocalCursor = 0;
              
              (void) sql_if (IGNORE, &slLocalCursor, pcgSqlBuf, pcgDataArea);
              
              commit_work ();
              close_my_cursor (&slLocalCursor);
              sprintf (pcgTmpBuf1, "%s", pclNewAurn);
              SendChanges (pclUrno, "AURN", pcgTmpBuf1, pclFipsChkFlds, pclFipsChkData, FALSE);
            }                   /* end if */

            if (pclLastUrno[0] != 0x00)
            {
              if ((pclLastRtyp[0] != 'J') && (pclLastAdid[0] != 'D'))
              {
                if (igTWEUrnoIsNumber == TRUE)
                {
                  sprintf (pcgSqlBuf, "UPDATE %s SET RTYP='J' WHERE URNO=%s", prgRegnLines[ilLastRec].Location,
                           pclLastUrno);
                }
                else
                {
                  sprintf (pcgSqlBuf, "UPDATE %s SET RTYP='J' WHERE URNO='%s'", prgRegnLines[ilLastRec].Location,
                           pclLastUrno);
                }
                dbg (DEBUG, "    <%s>", pcgSqlBuf);
                pcgDataArea[0] = 0x00;
                slLocalCursor = 0;
                
                (void) sql_if (IGNORE, &slLocalCursor, pcgSqlBuf, pcgDataArea);
                
                commit_work ();
                close_my_cursor (&slLocalCursor);
                sprintf (pcgTmpBuf1, "J");
                SendChanges (pclLastUrno, "RTYP", pcgTmpBuf1, pclFipsChkFlds, pclLastFipsChkData, FALSE);
              }                 /* end if */
              if ((pclRtyp[0] != 'J') && (pclAdid[0] == 'D'))
              {
                if (igTWEUrnoIsNumber == TRUE)
                {
                  sprintf (pcgSqlBuf, "UPDATE %s SET RTYP='J' WHERE URNO=%s", prgRegnLines[ilRec].Location, pclUrno);
                }
                else
                {
                  sprintf (pcgSqlBuf, "UPDATE %s SET RTYP='J' WHERE URNO='%s'", prgRegnLines[ilRec].Location, pclUrno);
                }
                dbg (DEBUG, "    <%s>", pcgSqlBuf);
                pcgDataArea[0] = 0x00;
                slLocalCursor = 0;
                
                (void) sql_if (IGNORE, &slLocalCursor, pcgSqlBuf, pcgDataArea);
                
                commit_work ();
                close_my_cursor (&slLocalCursor);
                sprintf (pcgTmpBuf1, "J");
                SendChanges (pclUrno, "RTYP", pcgTmpBuf1, pclFipsChkFlds, pclFipsChkData, FALSE);
                pclRtyp[0] = 'J';
                pclRtyp[1] = 0x00;
              }                 /* end if */
            }
          }                     /* end if */
          else
          {
            dbg (TRACE, "ERROR SET = TRUE");
            dbg (TRACE, "CURR ADID <%s> URNO <%s>", pclAdid, pclUrno);
            dbg (TRACE, "LAST ADID <%s> URNO <%s>", pclLastAdid, pclLastUrno);
            if (pclAdid[0] == 'D')
            {
              if ((pclRtyp[0] != 'E') || ((pclAurn[0] != '\0') && (pclAurn[0] != ' ')))
              {
                if (igTWEUrnoIsNumber == TRUE)
                {
                  sprintf (pcgSqlBuf, "UPDATE %s SET RKEY=%s,RTYP='E',AURN=' ' WHERE URNO=%s",
                           prgRegnLines[ilRec].Location, pclUrno, pclUrno);
                }
                else
                {
                  sprintf (pcgSqlBuf, "UPDATE %s SET RKEY='%s',RTYP='E',AURN=' ' WHERE URNO='%s'",
                           prgRegnLines[ilRec].Location, pclUrno, pclUrno);
                }
                dbg (DEBUG, "    <%s>", pcgSqlBuf);
                pcgDataArea[0] = 0x00;
                slLocalCursor = 0;
                
                (void) sql_if (IGNORE, &slLocalCursor, pcgSqlBuf, pcgDataArea);
                
                commit_work ();
                close_my_cursor (&slLocalCursor);
                sprintf (pcgTmpBuf1, "%s,E, ", pclUrno);
                SendChanges (pclUrno, "RKEY,RTYP,AURN", pcgTmpBuf1, pclFipsChkFlds, pclFipsChkData, FALSE);
              }                 /* end if */
            }                   /* end if */
            else
            {
              if ((pclLastAdid[0] != 'D') || ((pclLastAdid[0] == 'D') && (pclAdid[0] == 'A')))
              {
                if (pclLastRtyp[0] != 'E')
                {
                  if (igTWEUrnoIsNumber == TRUE)
                  {
                    sprintf (pcgSqlBuf, "UPDATE %s SET RKEY=%s,RTYP='E',AURN=' ' WHERE URNO=%s",
                             prgRegnLines[ilLastRec].Location, pclLastUrno, pclLastUrno);
                  }
                  else
                  {
                    sprintf (pcgSqlBuf, "UPDATE %s SET RKEY='%s',RTYP='E',AURN=' ' WHERE URNO='%s'",
                             prgRegnLines[ilLastRec].Location, pclLastUrno, pclLastUrno);
                  }
                  dbg (DEBUG, "    <%s>", pcgSqlBuf);
                  pcgDataArea[0] = 0x00;
                  slLocalCursor = 0;
                  

                  (void) sql_if (IGNORE, &slLocalCursor, pcgSqlBuf, pcgDataArea);
                  
                  commit_work ();
                  close_my_cursor (&slLocalCursor);
                  sprintf (pcgTmpBuf1, "%s,E, ", pclLastUrno);
                  SendChanges (pclLastUrno, "RKEY,RTYP,AURN", pcgTmpBuf1, pclFipsChkFlds, pclLastFipsChkData, FALSE);
                }               /* end if */
              }                 /* end if */
            }                   /* end else */
            if ((pclAdid[0] == 'B') && (pclLastAdid[0] == 'D'))
            {
              if (pclRtyp[0] != 'E')
              {
                if (igTWEUrnoIsNumber == TRUE)
                {
                  sprintf (pcgSqlBuf, "UPDATE %s SET RKEY=%s,RTYP='E',AURN=' ' WHERE URNO=%s",
                           prgRegnLines[ilRec].Location, pclUrno, pclUrno);
                }
                else
                {
                  sprintf (pcgSqlBuf, "UPDATE %s SET RKEY='%s',RTYP='E',AURN=' ' WHERE URNO='%s'",
                           prgRegnLines[ilRec].Location, pclUrno, pclUrno);
                }
                dbg (DEBUG, "    <%s>", pcgSqlBuf);
                pcgDataArea[0] = 0x00;
                slLocalCursor = 0;
                
                (void) sql_if (IGNORE, &slLocalCursor, pcgSqlBuf, pcgDataArea);
                
                commit_work ();
                close_my_cursor (&slLocalCursor);
                sprintf (pcgTmpBuf1, "%s,E, ", pclUrno);
                SendChanges (pclUrno, "RKEY,RTYP,AURN", pcgTmpBuf1, pclFipsChkFlds, pclFipsChkData, FALSE);
              }                 /* end if */
            }                   /* end if */
          }                     /* end else */

          /*  fprintf(outp,"%c %d %s\n",
             prgRegnLines[ilRec].Location[1],
             prgRegnLines[ilRec].LineType,
             prgRegnLines[ilRec].TextLine);
           */
          fflush (outp);

          strcpy (pclLastAdid, pclAdid);
          strcpy (pclLastUrno, pclUrno);
          strcpy (pclLastRtyp, pclRtyp);
          sprintf (pclLastFipsChkData, "%s,%s,%s,%s,%s", pclAdid, pclFtyp, pclTifa, pclTifd, pclFlno);
          ilLastRec = ilRec;

        }                       /* end if */
      }                         /* end for */

      if ((pclLastAdid[0] == 'A') && (pclLastRtyp[0] == 'E'))
      {
        dbg (TRACE, "%.3s %s ACTIVATE LAST ARRIVAL: ADID <%s> RTYP <%s>", prgRegnLines[ilLastRec].Location, pclLastUrno,
             pclLastAdid, pclLastRtyp);
        if (igTWEUrnoIsNumber == TRUE)
        {
          sprintf (pcgSqlBuf, "UPDATE %s SET RTYP='S',AURN=' ' WHERE URNO=%s", prgRegnLines[ilLastRec].Location,
                   pclLastUrno);
        }
        else
        {
          sprintf (pcgSqlBuf, "UPDATE %s SET RTYP='S',AURN=' ' WHERE URNO='%s'", prgRegnLines[ilLastRec].Location,
                   pclLastUrno);
        }
        dbg (DEBUG, "    <%s>", pcgSqlBuf);
        pcgDataArea[0] = 0x00;
        slLocalCursor = 0;
        
        (void) sql_if (IGNORE, &slLocalCursor, pcgSqlBuf, pcgDataArea);
        
        commit_work ();
        close_my_cursor (&slLocalCursor);
        sprintf (pcgTmpBuf1, "S, ");
        SendChanges (pclLastUrno, "RTYP,AURN", pcgTmpBuf1, pclFipsChkFlds, pclLastFipsChkData, FALSE);
      }                         /* end if */

      dbg (TRACE, "--- REGN: <%s> %d RECORDS CHECKED ---", pclRegnName, ilDataCnt);
      SendChanges ("0", pclAftFields, "0", pclFipsChkFlds, pclLastFipsChkData, TRUE);
    }                           /* end if */
    igRegnLineTotal += ilDataCnt;
  }                             /* end for */

  return;
}                               /* end CheckRegnHistory */

/* ******************************************************************** */
/* ******************************************************************** */
static void SwapDataLines (int ilRec1, int ilRec2)
{
  strcpy (prgRegnLines[0].Location, prgRegnLines[ilRec1].Location);
  strcpy (prgRegnLines[ilRec1].Location, prgRegnLines[ilRec2].Location);
  strcpy (prgRegnLines[ilRec2].Location, prgRegnLines[0].Location);

  strcpy (prgRegnLines[0].TextLine, prgRegnLines[ilRec1].TextLine);
  strcpy (prgRegnLines[ilRec1].TextLine, prgRegnLines[ilRec2].TextLine);
  strcpy (prgRegnLines[ilRec2].TextLine, prgRegnLines[0].TextLine);

  strcpy (prgRegnLines[0].LineKey, prgRegnLines[ilRec1].LineKey);
  strcpy (prgRegnLines[ilRec1].LineKey, prgRegnLines[ilRec2].LineKey);
  strcpy (prgRegnLines[ilRec2].LineKey, prgRegnLines[0].LineKey);

  strcpy (prgRegnLines[0].SortText, prgRegnLines[ilRec1].SortText);
  strcpy (prgRegnLines[ilRec1].SortText, prgRegnLines[ilRec2].SortText);
  strcpy (prgRegnLines[ilRec2].SortText, prgRegnLines[0].SortText);

  prgRegnLines[0].LineType = prgRegnLines[ilRec1].LineType;
  prgRegnLines[ilRec1].LineType = prgRegnLines[ilRec2].LineType;
  prgRegnLines[ilRec2].LineType = prgRegnLines[0].LineType;

  return;
}                               /* end SwapDataLines */

/* ******************************************************************** */
/* ******************************************************************** */
static void SendChanges (char *pcpSelKey, char *pcpFldLst, char *pcpFldDat, char *pcpFipsChkFlds, char *pcpFipsChkData,
                         int ipWorkFlag)
{
  int ilRecIdx = 0;
  int ilLen = 0;
  int ilFldItm = 0;
  int ilChgItm = 0;
  int ilChgCnt = 0;
  char pclFldNam[8];
  char pclOldVal[32];
  char pclNewVal[32];
  char pclSelBuf[128];
  char pclDatBuf[128];

  char pclSendFld[128] = "";
  char pclSendDat[128] = "";

  sprintf (pclSendFld, "%s,%s", pcpFldLst, pcpFipsChkFlds);
  sprintf (pclSendDat, "%s,%s", pcpFldDat, pcpFipsChkData);

  if (ipWorkFlag == FALSE)
  {
    (void) tools_send_info_flag (igToBcHdl, 0, pcgDestName, "", pcgRecvName, "", "", pcgTwStart, pcgTwEnd, "UPJ",
                                 pcgAftTab, pcpSelKey, pclSendFld, pclSendDat, 0);

    ilRecIdx = 1;
    while (ilRecIdx <= igRegnLineCount)
    {
      if (strcmp (prgRegnLines[ilRecIdx].LineKey, pcpSelKey) == 0)
      {
        StrgPutStrg (prgRegnLines[ilRecIdx].FldLst, &prgRegnLines[ilRecIdx].FldLen, pcpFldLst, 0, -1, ",");
        StrgPutStrg (prgRegnLines[ilRecIdx].FldDat, &prgRegnLines[ilRecIdx].DatLen, pcpFldDat, 0, -1, ",");
        ilRecIdx = igRegnLineCount;
      }                         /* end if */
      ilRecIdx++;
    }                           /* end while */
  }                             /* end if */
  else
  {
    for (ilRecIdx = 1; ilRecIdx <= igRegnLineCount; ilRecIdx++)
    {
      if (prgRegnLines[ilRecIdx].FldLen > 0)
      {
        prgRegnLines[ilRecIdx].FldLst[prgRegnLines[ilRecIdx].FldLen - 1] = 0x00;
        prgRegnLines[ilRecIdx].FldDat[prgRegnLines[ilRecIdx].DatLen - 1] = 0x00;

        dbg (TRACE, "SendChanges: URNO <%s> <%s> <%s>", prgRegnLines[ilRecIdx].LineKey, prgRegnLines[ilRecIdx].FldLst,
             prgRegnLines[ilRecIdx].FldDat);

        ilChgCnt = field_count (prgRegnLines[ilRecIdx].FldLst);
        for (ilChgItm = 1; ilChgItm <= ilChgCnt; ilChgItm++)
        {
          ilLen = get_real_item (pclFldNam, prgRegnLines[ilRecIdx].FldLst, ilChgItm);
          if (ilLen > 0)
          {
            ilFldItm = get_item_no (pcpFldLst, pclFldNam, 5) + 1;
            if (ilFldItm > 0)
            {
              ilLen = get_real_item (pclNewVal, prgRegnLines[ilRecIdx].FldDat, ilChgItm);
              ilLen = get_real_item (pclOldVal, prgRegnLines[ilRecIdx].TextLine, ilFldItm);
              if (strcmp (pclOldVal, pclNewVal) != 0)
              {
                dbg (TRACE, "      CHANGED: FLD <%s> <%s> NEW <%s>", pclFldNam, pclOldVal, pclNewVal);

                if (strcmp (pclFldNam, "RKEY") == 0)
                {
                  sprintf (pclSelBuf, "%s,%s", prgRegnLines[ilRecIdx].LineKey, pclOldVal);
                  sprintf (pclDatBuf, "%s,%s", pclOldVal, pclNewVal);

                  (void) tools_send_info_flag (igToBcHdl, 0, pcgDestName, "", pcgRecvName, "", "", pcgTwStart, pcgTwEnd,
                                               "SPR", "", pclSelBuf, "", pclDatBuf, 0);
                  dbg (TRACE, "      SBC: <SPR> <%s> <%s>", pclSelBuf, pclDatBuf);

                  sprintf (pclSelBuf, "%s,%s,%s", pclNewVal, pclOldVal, pclNewVal);
                  sprintf (pclDatBuf, "%s,%s,%s", pclNewVal, pclOldVal, pclNewVal);

                  (void) tools_send_info_flag (igToBcHdl, 0, pcgDestName, "", pcgRecvName, "", "", pcgTwStart, pcgTwEnd,
                                               "JOF", "", pclSelBuf, "", pclDatBuf, 0);
                  dbg (TRACE, "      SBC: <JOF> <%s>", pclSelBuf);
                }               /* end if */
              }                 /* end if */
            }                   /* end if */
          }                     /* end if */
        }                       /* end for */
      }                         /* end if */
    }                           /* end for */
  }                             /* end else */

  return;

/*

UPS / UPJ

    (void) tools_send_info_flag(igToBcHdl,0, pcgDestName, "", pcgRecvName,
                                   "", "", pcgTwStart, pcgTwEnd,
                                   pcpBcCmd,
                                   prgPrjTblCfg[ipTbl].FullName,
                                   prgPrjTblCfg[ipTbl].OutSelKey,
                                   prgPrjTblCfg[ipTbl].OutFldLst,
                                   prgPrjTblCfg[ipTbl].OutDatBlk, 0);

*/

}                               /* end SendChanges */

/* ******************************************************************** */
/* ******************************************************************** */
static void ReorgRegnHist (void)
{
  int ilGetRc = RC_SUCCESS;
  short slLocalCursor = 0;
  short slFkt = 0;
  char pclSqlBuf[512];
  char pclSqlKey[512];
  igRegnLineTotal = 0;

  ilGetRc = iGetConfigRow (pcgCfgFile, "REORG_AFTTAB", "HISTORY", CFG_STRING, pcgRegnList);
  if (ilGetRc == RC_SUCCESS)
  {
    if (strstr (pcgRegnList, "-ALL-") != NULL)
    {

      dbg (TRACE, "SELECTING REGN's FROM <%s>", pcgAftTab);
      sprintf (pclSqlKey, "WHERE REGN>' ' ORDER BY REGN");
      CheckWhereClause (TRUE, pclSqlKey, TRUE, TRUE, "\0");

      sprintf (pclSqlBuf, "SELECT DISTINCT(REGN) FROM %s %s", pcgAftTab, pclSqlKey);
      pcgRegnList[0] = 0x00;
      slLocalCursor = 0;
      slFkt = START;
      
      while ((ilGetRc = sql_if (slFkt, &slLocalCursor, pclSqlBuf, pcgRegnList)) == DB_SUCCESS)
      {
        CheckRegnHistory (pcgRegnList);
        slFkt = NEXT;
      }                         /* end while */
      close_my_cursor (&slLocalCursor);
      

    }                           /* end if */
    else
    {
      CheckRegnHistory (pcgRegnList);
    }                           /* end else */

  }                             /* end if */
  dbg (TRACE, "HISTORY: TOTAL %d RECORDS CHECKED", igRegnLineTotal);
  igRegnLineTotal = 0;
  return;
}                               /* end ReorgRegnHist */

/* ******************************************************************** */
/* ******************************************************************** */
static void CheckAutoBarShift (void)
{
  int ilGetRc = RC_SUCCESS;
  int ilTotal = 0;
  short slLocalCursor = 0;
  short slFkt = 0;
  char pclSqlBuf[512];
  char pclSqlKey[512];
  char pclSqlDat[512];
  igRegnLineTotal = 0;

  return;

/*
  ilGetRc = iGetConfigRow(pcgCfgFile,
          "REORG_AFTTAB","HISTORY",CFG_STRING,pcgRegnList);
*/
  if (ilGetRc == RC_SUCCESS)
  {
    dbg (TRACE, "SELECTING URNO's FROM <%s>", pcgAftTab);
    sprintf (pclSqlKey, "WHERE " "TIFA BETWEEN '19991004140000' AND '19991004150000' " "AND DES3='PVG' AND RTYP<>'J'");
    CheckWhereClause (TRUE, pclSqlKey, TRUE, TRUE, "\0");

    sprintf (pclSqlBuf, "SELECT URNO FROM %s %s", pcgAftTab, pclSqlKey);
    dbg (TRACE, "\n<%s>", pclSqlBuf);
    pclSqlDat[0] = 0x00;
    slLocalCursor = 0;
    slFkt = START;
    
    while ((ilGetRc = sql_if (slFkt, &slLocalCursor, pclSqlBuf, pclSqlDat)) == DB_SUCCESS)
    {

      ilTotal++;
      slFkt = NEXT;
    }                           /* end while */
    close_my_cursor (&slLocalCursor);
    

  }                             /* end if */
  dbg (TRACE, "BARSHIFT: TOTAL %d RECORDS CHECKED", ilTotal);
  ilTotal = 0;
  return;
}                               /* end CheckAutoBarShift */

/* ******************************************************************** */
/* ******************************************************************** */
static void AddRegnToList (char *pcpRegn)
{
  char pclChkRegn[32];
  int ilLstLen = 0;
  int ilRegnLen = 0;
  if ((pcpRegn[0] != '\0') && (pcpRegn[0] != ' '))
  {
    sprintf (pclChkRegn, ",%s,", pcpRegn);
    if (strstr (pcgRegnList, pclChkRegn) == NULL)
    {
      ilRegnLen = strlen (pcpRegn);
      ilLstLen = strlen (pcgRegnList);
      if ((ilLstLen + ilRegnLen + 1) < REGN_BUFF_SIZE)
      {
        StrgPutStrg (pcgRegnList, &ilLstLen, pcpRegn, 0, -1, ",");
        pcgRegnList[ilLstLen] = 0x00;
      }                         /* end if */
      else
      {
        dbg (TRACE, "ERROR BUFFER OVERFLOW: REGN LIST (%s)", pcpRegn);
      }                         /* end else */
    }                           /* end if */
  }                             /* end if */
  return;
}                               /* end AddRegnToList */

/* ******************************************************************** */
/* ******************************************************************** */
static void AddRkeyToList (int ipTbl, int ipRec, char *pcpRcvRkey)
{
  int ilGetRc = RC_SUCCESS;
  int ilRkey = 0;
  char pclRkey[32];
  char pclChkRkey[32];
  int ilLstLen = 0;
  int ilRkeyLen = 0;
  if (ipTbl < 1)
  {
    strcpy (pclRkey, pcpRcvRkey);
    ilRkey = 1;
  }                             /* end if */
  else
  {
    ilGetRc = CheckRulFndCall (ipTbl, ipRec);
    if (ilGetRc == RC_SUCCESS)
    {
      ilRkey = GetFieldIndex (ipTbl, "RKEY");
      if (ilRkey > 0)
      {
        strcpy (pclRkey, prgRecFldDat[ipTbl][ipRec][ilRkey].NewDat);
      }                         /* end if */
    }                           /* end if */
  }                             /* end else */
  if (ilRkey > 0)
  {
    if ((pclRkey[0] != '\0') && (pclRkey[0] != ' ')
        /*  TWE */
        && (strcmp (pclRkey, "0") != 0))
    {
      sprintf (pclChkRkey, ",%s,", pclRkey);
      if (strstr (pcgRkeyList, pclChkRkey) == NULL)
      {
        ilRkeyLen = strlen (pclRkey);
        ilLstLen = strlen (pcgRkeyList);
        if ((ilLstLen + ilRkeyLen + 1) < RKEY_BUFF_SIZE)
        {
          StrgPutStrg (pcgRkeyList, &ilLstLen, pclRkey, 0, -1, ",");
          pcgRkeyList[ilLstLen] = 0x00;
        }                       /* end if */
        else
        {
          dbg (TRACE, "ERROR BUFFER OVERFLOW: RKEY LIST (%s)", pclRkey);
        }                       /* end else */
      }                         /* end if */
    }                           /* end if */
  }                             /* end if */
  return;
}                               /* end AddRkeyToList */

/* ******************************************************************** */
/* ******************************************************************** */
static void SendRecordToAdam (void)
{
  int ilGetRc = RC_SUCCESS;
  int ilTbl = 0;
  int ilFldCnt = 0;
  int ilRecCnt = 0;
  short slLocalCursor = 0;
  short slFkt = 0;
  char pclSqlBuf[4096];
  char pclSqlKey[512];
  dbg (TRACE, "CONVERT: START FETCHING RECORDS");
  if (igToAdam > 0)
  {
    ilTbl = GetTableIndex (pcgAftTab);

    pclSqlKey[0] = 0x00;
    ilGetRc = iGetConfigRow (pcgCfgFile, "REORG_AFTTAB", "CONVERT_AFT", CFG_STRING, pclSqlKey);
    if (ilGetRc == RC_SUCCESS)
    {
      dbg (TRACE, "USED SELECTION <%s>", pclSqlKey);
      if (strcmp (pclSqlKey, "-ALL-") == 0)
      {
        pclSqlKey[0] = 0x00;
        sprintf (pclSqlBuf, "SELECT %s FROM %s ", prgPrjTblCfg[ilTbl].AllFldLst, pcgAftTab);
      }                         /* end if */
      else
      {
        sprintf (pclSqlBuf, "SELECT %s FROM %s WHERE %s", prgPrjTblCfg[ilTbl].AllFldLst, pcgAftTab, pclSqlKey);
      }                         /* end else */
      ilFldCnt = field_count (prgPrjTblCfg[ilTbl].AllFldLst);
      pcgDataArea[0] = 0x00;
      slLocalCursor = 0;
      slFkt = START;
      
      while ((ilGetRc = sql_if (slFkt, &slLocalCursor, pclSqlBuf, pcgDataArea)) == DB_SUCCESS)
      {
        ilRecCnt++;
        BuildItemBuffer (pcgDataArea, "", ilFldCnt, ",");
        (void) tools_send_info_flag (igToAdam, 0, "FLIGHT", "", "CEDA", "", "", "AFS", "ALL,TAB,CONVERT", "CNV", "AFT",
                                     pclSqlKey, prgPrjTblCfg[ilTbl].AllFldLst, pcgDataArea, 0);
        slFkt = NEXT;
      }                         /* end while */
      close_my_cursor (&slLocalCursor);
      

    }                           /* end if */
    else
    {
      dbg (TRACE, "MISSING: REORG_AFTTAB / CONVERT_AFT");
    }                           /* end else */
  }                             /* end if */
  else
  {
    dbg (TRACE, "MISSING: PROCESS <adam> NOT FOUND");
  }                             /* end else */
  dbg (TRACE, "CONVERT: TOTAL %d RECORDS FETCHED", ilRecCnt);
  return;
}                               /* end SendRecordToAdam */

/********************************************************/
/********************************************************/
static int ReleaseActionInfo (char *pcpRoute, char *pcpTbl, char *pcpCmd, char *pcpUrnoList, char *pcpSel, char *pcpFld,
                              char *pcpDat, char *pcpOldDat)
{
  int ilRC = RC_SUCCESS;
  int ilSelLen = 0;
  int ilSelPos = 0;
  int ilDatLen = 0;
  int ilDatPos = 0;
  int ilRouteItm = 0;
  int ilItmCnt = 0;
  int ilActRoute = 0;
  char pclRouteNbr[8];
  char *pclOutSel = NULL;
  char *pclOutDat = NULL;

  if (igDebugDetails == TRUE)
  {
    dbg (TRACE, "ACTION TBL <%s>", pcpTbl);
    dbg (TRACE, "ACTION CMD <%s>", pcpCmd);
    dbg (TRACE, "ACTION SEL <%s>", pcpSel);
    dbg (TRACE, "ACTION URN <%s>", pcpUrnoList);
    dbg (TRACE, "ACTION USR <%s>", pcgDestName);
  }                             /* end if */
  dbg (DEBUG, "ACTION FLD/DAT/OLD\n<%s>\n<%s>\n<%s>", pcpFld, pcpDat, pcpOldDat);

  ilSelLen = strlen (pcpUrnoList) + strlen (pcpSel) + 32;
  pclOutSel = (char *) malloc (ilSelLen);
  ilDatLen = strlen (pcpDat) + strlen (pcpOldDat) + 32;
  pclOutDat = (char *) malloc (ilDatLen);
  if ((pclOutSel != NULL) && (pclOutDat != NULL))
  {
    StrgPutStrg (pclOutSel, &ilSelPos, pcpSel, 0, -1, " \n");
    StrgPutStrg (pclOutSel, &ilSelPos, pcpUrnoList, 0, -1, "\n");
    if (ilSelPos > 0)
    {
      ilSelPos--;
    }                           /* end if */
    pclOutSel[ilSelPos] = 0x00;
    StrgPutStrg (pclOutDat, &ilDatPos, pcpDat, 0, -1, " \n");
    StrgPutStrg (pclOutDat, &ilDatPos, pcpOldDat, 0, -1, "\n");
    if (ilDatPos > 0)
    {
      ilDatPos--;
    }                           /* end if */
    pclOutDat[ilDatPos] = 0x00;
    ilItmCnt = field_count (pcpRoute);
    for (ilRouteItm = 1; ilRouteItm <= ilItmCnt; ilRouteItm++)
    {
      (void) get_real_item (pclRouteNbr, pcpRoute, ilRouteItm);
      ilActRoute = atoi (pclRouteNbr);
      if (ilActRoute > 0)
      {
        (void) tools_send_info_flag (ilActRoute, 0, pcgDestName, "", pcgRecvName, "", "", pcgTwStart, pcgTwEnd, pcpCmd,
                                     pcpTbl, pclOutSel, pcpFld, pclOutDat, 0);
      }                         /* end if */
    }                           /* end for */
    free (pclOutSel);
    free (pclOutDat);
  }                             /* end if */
  else
  {
    debug_level = TRACE;
    dbg (TRACE, "ERROR: ALLOC %d/%d BYTES FOR ACTION INFO", ilSelLen, ilDatLen);
    exit (0);
  }                             /* end else */
  pclOutSel = NULL;
  pclOutDat = NULL;
  return ilRC;
}                               /* end ReleaseActionInfo() */

/********************************************************/
/********************************************************/
static void InitLables (void)
{
  GetLabelText ("RT01", pcgLangCode, prgLableText[1].Text, "Flight");
  GetLabelText ("RT02", pcgLangCode, prgLableText[2].Text, "to");
  GetLabelText ("RT03", pcgLangCode, prgLableText[3].Text, "returned back from taxiway");
  GetLabelText ("RT04", pcgLangCode, prgLableText[4].Text, "Original final destination");
  GetLabelText ("RT05", pcgLangCode, prgLableText[5].Text, "STA");
  GetLabelText ("RF01", pcgLangCode, prgLableText[6].Text, "Flight");
  GetLabelText ("RF02", pcgLangCode, prgLableText[7].Text, "diverted from");
  GetLabelText ("RF03", pcgLangCode, prgLableText[8].Text, "back to");
  GetLabelText ("RF04", pcgLangCode, prgLableText[9].Text, "Original final destination");
  GetLabelText ("RF05", pcgLangCode, prgLableText[10].Text, "STA");
  return;
}                               /* end InitLables */

/********************************************************/
/********************************************************/
static void GetLabelText (char *pcpTkey, char *pcpLkey, char *pcpResult, char *pcpDefault)
{
  short slLocalCursor = 0;
  sprintf (pcgSqlBuf, "SELECT TEXT FROM LBL%s WHERE TKEY='%s' AND LKEY='%s'", pcgDefTblExt, pcpTkey, pcpLkey);
  pcgDataArea[0] = 0x00;
  
  if (sql_if (START, &slLocalCursor, pcgSqlBuf, pcgDataArea) == DB_SUCCESS)
  {
    strcpy (pcpResult, pcgDataArea);
  }                             /* end if */
  else
  {
    strcpy (pcpResult, pcpDefault);
  }                             /* end else */
  close_my_cursor (&slLocalCursor);
  
  return;
}                               /* end GetLabelText */

/********************************************************/
/********************************************************/
static void SendConflict (int ipTbl, int ipRec, int ipFld, char *pcpDest)
{
  int ilUrno = 0;
  int ilLen = 0;
  int ilItmCnt = 0;
  int ilItm = 0;
  int ilFld = 0;
  int ilFldPos = 0;
  int ilNewPos = 0;
  int ilOldPos = 0;
  char pclFldNam[16];
 /*** MCU 21.11.2000 added fields AKTI + AKUS ***/
  char *pclOutFields = "TIME,MENO,METY,PRIO,ADDI,RTAB,RURN,FLST,NVAL,OVAL,AKTI,AKUS";
  char pclOutData[1024];
  char pclOutSel[64];
  char pclFlst[255];
  char pclNval[255];
  char pclOval[255];
  char pclCflTxt[255];
  char pclCflTab[16];
  char pclMeno[16];
  char pclMety[16];
  char pclPrio[16];
  char pclActCmd[16];
  char pclDelim[4];
  int ilCf09 = FALSE;
  int ilUseu = -4712;

  pclDelim[0] = 30;
  pclDelim[1] = 0x00;

  sprintf (pclCflTab, "CFL%s", pcgTblExt);

  get_real_item (pclMeno, pcpDest, 1);
  get_real_item (pclMety, pcpDest, 2);
  get_real_item (pclPrio, pcpDest, 3);
  get_real_item (pclActCmd, pcpDest, 4);
  get_real_item (pclCflTxt, pcpDest, 5);

  if ((pclCflTxt[0] == '\0') || (pclCflTxt[0] == '?'))
  {
    strcpy (pclCflTxt, prgTblFldCfg[ipTbl][ipFld].SysTabAddi);
  }                             /* end if */
  if ((pclCflTxt[0] == '\0') || (pclCflTxt[0] == '#') || (pclCflTxt[0] == ' '))
  {
    strcpy (pclCflTxt, prgTblFldCfg[ipTbl][ipFld].SysTabFina);
  }                             /* end if */
  pclCflTxt[64] = 0x00;

  if (strcmp(pclActCmd,"CF09") == 0 )
  {
    ilCf09 = TRUE;
    dbg(DEBUG, "SendConflict: CF09 detected. Setting CMD to WRD");
    dbg(DEBUG, "SendConflict: ipTbl: %d ipFld: %d pcpDest: <%s>",ipTbl,ipFld,pcpDest);
    strcpy (pclActCmd, "WRD");
    strcpy (pclOutSel, "RURN,RTAB,MENO");
  }
  else if (pclActCmd[0] == 'I')
  {
    strcpy (pclActCmd, "IBT");
    strcpy (pclOutSel, "              ");
  }                             /* end if */
  else
  {
    strcpy (pclActCmd, "WRD");
    strcpy (pclOutSel, "RURN,RTAB,MENO");
  }                             /* end else */

  if (ilCf09 == FALSE)
  {
      if ((strstr (pcpDest, prgTblFldCfg[ipTbl][ipFld].SysTabFina) == NULL) && (strstr (pcpDest, "----") == NULL))
      {
        StrgPutStrg (pclFlst, &ilFldPos, prgTblFldCfg[ipTbl][ipFld].SysTabFina, 0, -1, pclDelim);
        StrgPutStrg (pclNval, &ilNewPos, prgRecFldDat[ipTbl][ipRec][ipFld].NewDat, 0, -1, pclDelim);
        StrgPutStrg (pclOval, &ilOldPos, prgRecFldDat[ipTbl][ipRec][ipFld].DbsDat, 0, -1, pclDelim);
      }                             /* end if */
  }
  else
  { /* Value of field [ipFld] was not found in BasicData, patch message to show this: */
    dbg(DEBUG, "SendConflict: CF09 patching %s ",prgTblFldCfg[ipTbl][ipFld].SysTabFina);
    if ((strstr (pcpDest, prgTblFldCfg[ipTbl][ipFld].SysTabFina) == NULL) && (strstr (pcpDest, "----") == NULL))
    {
      StrgPutStrg (pclFlst, &ilFldPos, prgTblFldCfg[ipTbl][ipFld].SysTabFina, 0, -1, pclDelim);
/*    dbg(DEBUG, "SendConflict: RCV <%s> DBS <%s> NEW <%s> ",
                 prgRecFldDat[ipTbl][ipRec][ipFld].RcvDat,
                 prgRecFldDat[ipTbl][ipRec][ipFld].DbsDat,
                 prgRecFldDat[ipTbl][ipRec][ipFld].NewDat);
*/
      StrgPutStrg (pclOval, &ilOldPos, prgTblFldCfg[ipTbl][ipFld].SysTabAddi, 0, -1, pclDelim);
      if (igTWEImport == TRUE)
      {
         StrgPutStrg (pclNval, &ilNewPos, prgRecFldDat[ipTbl][ipRec][ipFld].NewDat, 0, -1, pclDelim);
      }
      else
      {
         StrgPutStrg (pclNval, &ilNewPos, prgRecFldDat[ipTbl][ipRec][ipFld].DbsDat, 0, -1, pclDelim);
      }
    }                             /* end if */
  }
    
  ilItmCnt = field_count (pcpDest);
  for (ilItm = 6; ilItm <= ilItmCnt; ilItm++)
  {
    ilUseu = GetFieldIndex (ipTbl, "USEU");
    ilLen = get_real_item (pclFldNam, pcpDest, ilItm);
    if (ilLen > 0)
    {
      ilFld = GetFieldIndex (ipTbl, pclFldNam);
      if (ilFld > 0)
      {
        pclFlst[ilFldPos] = 0x00;
        if (strstr (pclFlst, pclFldNam) == NULL)
        {
          StrgPutStrg (pclFlst, &ilFldPos, prgTblFldCfg[ipTbl][ilFld].SysTabFina, 0, -1, pclDelim);
          if ((igTWEImport == TRUE) && (ilUseu == ilFld) )
          { /* in Case of import, USEU perhaps is not filled already ... */
/*          StrgPutStrg (pclNval, &ilNewPos, "stxhdl", 0, -1, pclDelim); */
            StrgPutStrg (pclNval, &ilNewPos, pcgDestName, 0, -1, pclDelim);
            StrgPutStrg (pclOval, &ilOldPos, " ? ", 0, -1, pclDelim);
          }
          else
          {
            StrgPutStrg (pclNval, &ilNewPos, prgRecFldDat[ipTbl][ipRec][ilFld].NewDat, 0, -1, pclDelim);
            StrgPutStrg (pclOval, &ilOldPos, prgRecFldDat[ipTbl][ipRec][ilFld].DbsDat, 0, -1, pclDelim);
          }
        }                       /* end if */
      }                         /* end if */
      else
      {
        if (strcmp (pclFldNam, "----") != 0)
        {
          dbg (TRACE, "CONFLICT: UNKNOWN FIELD <%s>", pclFldNam);
        }                       /* end if */
      }                         /* end else */
    }                           /* end if */
  }                             /* end for */
  if (ilFldPos > 0)
  {
    ilFldPos--;
    ilNewPos--;
    ilOldPos--;
  }                             /* end if */

  pclFlst[ilFldPos] = 0x00;
  pclNval[ilNewPos] = 0x00;
  pclOval[ilOldPos] = 0x00;

  ilUrno = GetFieldIndex (ipTbl, "URNO");
 /*** MCU 21.11.2000 added empty fields AKTI + AKUS ***/
  sprintf (pclOutData, "%s,%s,%s,%s,%s,AFT,%s,%s,%s,%s, , ", pcgCurTime, pclMeno, pclMety, pclPrio, pclCflTxt,
           prgRecFldDat[ipTbl][ipRec][ilUrno].NewDat, pclFlst, pclNval, pclOval);
 /***********
  sprintf(pclOutData,"%s,%s,%s,%s,%s,AFT,%s,%s,%s,%s",
                      pcgCurTime,
		      pclMeno,pclMety,pclPrio,pclCflTxt,
                      prgRecFldDat[ipTbl][ipRec][ilUrno].NewDat,
		      pclFlst,pclNval,pclOval);
				********************/
/* */
dbg(TRACE, "SendConflict: tools_send_info_flag to router:\npcgTwStart <%s>\npcgTwEnd <%s>\npclActCmd <%s>\npclCflTab <%s>\npclOutSel <%s>\npclOutFields <%s>\npclOutData <%s>",
pcgTwStart,pcgTwEnd, pclActCmd, pclCflTab, pclOutSel, pclOutFields, pclOutData);
  dbg (TRACE, "line %ld: Broadcasting to %ld ... ",__LINE__,1200);
/* */
  (void) tools_send_info_flag (1200, 0, pcgDestName, "", pcgRecvName, "", "", pcgTwStart, pcgTwEnd, pclActCmd,
                               pclCflTab, pclOutSel, pclOutFields, pclOutData, NETOUT_NO_ACK);
  return;
}                               /* end SendConflict */

/********************************************************/
/********************************************************/
static void CheckFieldNameMapping (char *pcpFldNam)
{
  int ilFound = FALSE;
  int ilIdx = 0;
  GetMappedFieldName (pcpFldNam, prgFldMapLst[0].RcvFldLst, prgFldMapLst[0].UseFldLst);
  ilFound = FALSE;
  for (ilIdx = 1; ((ilIdx <= prgFldMapLst[0].Count) && (ilFound == FALSE)); ilIdx++)
  {
    if (strcmp (prgFldMapLst[ilIdx].Name, pcgRecvName) == 0)
    {
      GetMappedFieldName (pcpFldNam, prgFldMapLst[ilIdx].RcvFldLst, prgFldMapLst[ilIdx].UseFldLst);
      ilFound = TRUE;
    }                           /* end if */
  }                             /* end for */
  ilFound = FALSE;
  for (ilIdx = 1; ((ilIdx <= prgFldMapLst[0].Count) && (ilFound == FALSE)); ilIdx++)
  {
    if (strcmp (prgFldMapLst[ilIdx].Name, pcgDestName) == 0)
    {
      GetMappedFieldName (pcpFldNam, prgFldMapLst[ilIdx].RcvFldLst, prgFldMapLst[ilIdx].UseFldLst);
      ilFound = TRUE;
    }                           /* end if */
  }                             /* end for */
  return;
}                               /* end CheckFieldNameMapping */

/********************************************************/
/********************************************************/
static void GetMappedFieldName (char *pcpFldNam, char *pcpMapRcv, char *pcpMapUse)
{
  int ilFldItm = 0;
  char pclTmpBuf[16];
  /* dbg(TRACE,"MAPPING FIELD <%s> IN <%s>",pcpFldNam,pcpMapRcv); */
  ilFldItm = get_item_no (pcpMapRcv, pcpFldNam, 5) + 1;
  if (ilFldItm > 0)
  {
    get_real_item (pclTmpBuf, pcpMapUse, ilFldItm);
    dbg (TRACE, "FIELD <%s> MAPPED TO <%s>", pcpFldNam, pclTmpBuf);
    strcpy (pcpFldNam, pclTmpBuf);
  }                             /* end if */
  return;
}                               /* end GetMappedFieldName */

 /*************************************************************
									TWE FUNCTIONS
	**************************************************************/

static int TWEHandlePOPSData (char *pcpRcvTable, char *pcpFields, char *pcpData, char *pcpSelKey)
{
  int ilRC = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  int ilAdid;
  int ilUrno;
  int ilFld;
  int ilTbl;
  int ilItmLen;
  int ilRotaType;
  short slUpdCursor;

  char pclArrFldLst[2048];
  char pclDepFldLst[2048];
  char pclOutFld[2048];
  char pclArrFldDat[4096];
  char pclDepFldDat[4096];
  char pclFldDat[2048];
  char pclOutDat[4096];

  char pclTable[20];
  char pclVpfrDat[20];
  char pclVptoDat[20];
  char pclOutSel[1024];
  char pclFredDat[10];
  char pclFreqDat[10];
  char pclDaofDat[10];
  char pclUrno[15];

  igTWENewFlnoFlight = FALSE;

  if (pcpSelKey[0] != '\0')
  {
    igTWENewFlnoFlight = TRUE;
    strcpy (pclTable, pcgAftTab);
    ilTbl = GetTableIndex (pclTable);
    strcpy (pclArrFldLst, prgPrjTblCfg[ilTbl].AllFldLst);
    ilRC = SearchFlight (pclTable, pclArrFldLst, pcpSelKey, FOR_UPDATE, TRUE);

    /* NOT FOUND OLD FLIGHT  ==>>>  NO INSERT OF NEW FLNO!!!!!!!!!!!!! */
    if (ilRC != RC_SUCCESS)
    {
      dbg (TRACE, "TWEHandlePOPSData: FLIGHT %s NOT FOUND => NO INSERT OF NEW FLNO", pcpSelKey);
      ilRC = RC_SUCCESS;
      return (ilRC);
    }

    /* SET OLD FLIGHT TO CANCELLATION */
    ilUrno = get_item_no (pclArrFldLst, "URNO", 5) + 1;
    if (ilUrno > 0)
    {
      ilItmLen = get_real_item (pclUrno, pcgDataArea, ilUrno);
    }
    if (ilItmLen > 0)
    {
      if (igTWEUrnoIsNumber == TRUE)
      {
        sprintf (pcgSqlBuf, "UPDATE %s SET FTYP='X' WHERE URNO=%s", prgPrjTblCfg[ilTbl].FullName, pclUrno);
      }
      else
      {
        sprintf (pcgSqlBuf, "UPDATE %s SET FTYP='X' WHERE URNO='%s'", prgPrjTblCfg[ilTbl].FullName, pclUrno);
      }
      strcpy (pcgDataArea, "");
      slUpdCursor = 0;
      
      ilGetRc = sql_if (IGNORE, &slUpdCursor, pcgSqlBuf, pcgDataArea);
      if (ilGetRc == DB_SUCCESS)
      {
        commit_work ();
      }
      else
      {
        rollback ();
        dbg (TRACE, "ORA-ERROR appeared for <%s>", pcgSqlBuf);
      }
      close_my_cursor (&slUpdCursor);
      
    }
    strcpy (pclArrFldLst, "");
  }                             /* Select exists */

  /* init */
  strcpy (pclVpfrDat, "");
  strcpy (pclVptoDat, "");
  strcpy (pclDaofDat, "0");
  strcpy (pclFredDat, "1234567");
  strcpy (pclFreqDat, "1");
  sprintf (pclTable, "AFT%s", pcgTblExt);

  /* fill local lists */
  strcpy (pclArrFldLst, pcpFields);
  strcpy (pclDepFldLst, pcpFields);
  strcpy (pclArrFldDat, pcpData);
  strcpy (pclDepFldDat, pcpData);

  /* find out: inbound, outbound or rotation */
  ilRotaType = 0;
  ilAdid = get_item_no (pcpFields, "ADID", 5) + 1;
  if (ilAdid > 0)
  {
    ilItmLen = get_real_item (pclFldDat, pcpFields, ilAdid);
    if (ilItmLen > 0)
    {
      if (pclFldDat[0] == 'A')
      {
        ilRotaType = 1;
      }
      else if (pclFldDat[0] == 'D')
      {
        ilRotaType = 2;
      }
      else if (pclFldDat[0] == 'B')
      {
        ilRotaType = 3;
      }
      else
      {
        dbg (TRACE, "TWEHandlePOPSData: ERROR: No valid ADID <%s>", pclFldDat);
      }
    }
    else
    {
      dbg (TRACE, "TWEHandlePOPSData: ERROR: Field ADID has no data");
    }
  }
  else
  {
    dbg (TRACE, "TWEHandlePOPSData: ERROR: No Field ADID send");
  }

  /* build InsertFlights parameter */
  switch (ilRotaType)
  {
    case 0:
      dbg (TRACE, "TWEHandlePOPSData: ERROR: No flight found");
      break;
    case 1:
      /* GET VALIDITY */
      ilFld = get_item_no (pcpFields, "STOA", 5) + 1;
      ilItmLen = get_real_item (pclVpfrDat, pcpData, ilFld);
      pclVpfrDat[8] = 0x00;
      strcpy (pclVptoDat, pclVpfrDat);
      sprintf (pclOutSel, "WHERE %s,%s,%s,%s,%s, , ,A", pclVpfrDat, pclVptoDat, pclFredDat, pclFreqDat, pclVpfrDat);
      sprintf (pclOutFld, "%s", pclArrFldLst);
      sprintf (pclOutDat, "%s", pclArrFldDat);
      break;
    case 2:
      /* GET VALIDITY */
      ilFld = get_item_no (pcpFields, "STOD", 5) + 1;
      ilItmLen = get_real_item (pclVpfrDat, pcpData, ilFld);
      pclVpfrDat[8] = 0x00;
      strcpy (pclVptoDat, pclVpfrDat);
      sprintf (pclOutFld, "%s", pclDepFldLst);
      sprintf (pclOutDat, "%s", pclDepFldDat);
      sprintf (pclOutSel, "WHERE %s,%s,%s,%s,%s, , ,D", pclVpfrDat, pclVptoDat, pclFredDat, pclFreqDat, pclVpfrDat);
      break;
    case 3:
      /* slType = 3 */
      /* GET VALIDITY */
      ilFld = get_item_no (pcpFields, "STOD", 5) + 1;
      ilItmLen = get_real_item (pclVpfrDat, pcpData, ilFld);
      pclVpfrDat[8] = 0x00;
      strcpy (pclVptoDat, pclVpfrDat);
      sprintf (pclOutFld, "%s\n%s", pclArrFldLst, pclDepFldLst);
      sprintf (pclOutDat, "%s\n%s", pclArrFldDat, pclDepFldDat);
      sprintf (pclOutSel, "WHERE %s,%s,%s,%s,%s,%s,%s,AD", pclVpfrDat, pclVptoDat, pclFredDat, pclFredDat, pclFreqDat,
               pclVpfrDat, pclVpfrDat);
      igTWERoundtrip = TRUE;
      break;
    default:
      break;
  }                             /* end switch */

  /* Call InsertFlights */
  if (ilRotaType > 0)
  {
    /* ACTION */
    if (igTWENewFlnoFlight == FALSE)
    {
      ilRC = ResetDataBuffer (__LINE__ ,TRUE, 0, 0, 0, 0, 0, 0);
    }
    igErrorCount = 0;
    if ((pclDaofDat[0] < '0') || (pclDaofDat[0] > '9'))
    {
      strcpy (pclDaofDat, "0");
    }
    igTWEImport = TRUE;
    ilRC = InsertFlights (pclTable, pclOutFld, pclOutDat, pclOutSel, pclDaofDat, FALSE);
    ilRC = RC_SUCCESS;
  }                             /* end if Rotatype */

  igTWEImport = FALSE;
  igTWERoundtrip = FALSE;

  return ilRC;

}                               /* end of TWEHandlePOPSData */


static int TWESearchAndDelRoundtrip (char *pcpTable, char *pcpFkey, int ipTbl, int ipRec)
{
  int ilRC = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  int ilStod;
  int ilStoa;
  int ilFtyp;
  int ilJcnt;
  int ilJfno;
  short slFkt;
  char pclSqlBuf[1024];
  char pclSqlDat[1024];
  char pclDBStod[20];
  char pclStod[20];
  char pclDBStoa[20];
  char pclStoa[20];
  char pclAdid[8];
  char pclFtyp[8];
  char pclJcnt[8];
  char pclJfno[2048];
  char pclDummy[2048];
  char *pclPtr;
  short slCursor = 0;

  ilStoa = GetFieldIndex (ipTbl, "STOA");
  (void) get_real_item (pclStoa, prgRecFldDat[ipTbl][ipRec][ilStoa].NewDat, 1);
  ilStod = GetFieldIndex (ipTbl, "STOD");
  (void) get_real_item (pclStod, prgRecFldDat[ipTbl][ipRec][ilStod].NewDat, 1);
  ilFtyp = GetFieldIndex (ipTbl, "FTYP");
  (void) get_real_item (pclFtyp, prgRecFldDat[ipTbl][ipRec][ilFtyp].NewDat, 1);
  ilJcnt = GetFieldIndex (ipTbl, "JCNT");
  (void) get_real_item (pclJcnt, prgRecFldDat[ipTbl][ipRec][ilJcnt].NewDat, 1);
  ilJfno = GetFieldIndex (ipTbl, "JFNO");
  (void) get_real_item (pclJfno, prgRecFldDat[ipTbl][ipRec][ilJfno].NewDat, 1);

  /* SEARCH OLD RECORD */
  if (strcmp (pclFtyp, "X") == 0)
  {
    sprintf (pclSqlBuf, "SELECT STOD,STOA,ADID FROM %s WHERE FKEY='%s' AND (STOD = '%s' OR STOA = '%s')", pcpTable,
             pcpFkey, pclStod, pclStoa);
  }
  else
  {
    sprintf (pclSqlBuf, "SELECT STOD,STOA,ADID,JCNT,JFNO FROM %s WHERE FKEY='%s' AND FTYP = '%s'", pcpTable, pcpFkey,
             pclFtyp);
  }
  dbg (DEBUG, "TWESearchAndDelRoundtrip: <%s>", pclSqlBuf);
  slCursor = 0;
  strcpy (pclSqlDat, "");
  slFkt = START;
  ilGetRc = DB_SUCCESS;
  
  /* get the one with valid JFNO */
  while (ilGetRc == DB_SUCCESS)
  {
    ilGetRc = sql_if (slFkt, &slCursor, pclSqlBuf, pclSqlDat);
    if (ilGetRc != DB_SUCCESS)
    {
      if (slFkt == START)
      {
        ilRC = RC_FAIL;
      }
      else
      {
        ilRC = RC_SUCCESS;
      }
    }
    else
    {
      strcpy (pclDBStod, pclSqlDat);
      pclPtr = pclSqlDat;
      while (*pclPtr != '\0')
      {
        pclPtr++;
      }
      pclPtr++;
      strcpy (pclDBStoa, pclPtr);
      while (*pclPtr != '\0')
      {
        pclPtr++;
      }
      pclPtr++;
      strcpy (pclAdid, pclPtr);
      while (*pclPtr != '\0')
      {
        pclPtr++;
      }
      pclPtr++;
      strcpy (pclJcnt, pclPtr);
      while (*pclPtr != '\0')
      {
        pclPtr++;
      }
      pclPtr++;
      strcpy (pclJfno, pclPtr);
      strcpy (pclDummy, pclJfno);

      str_trm_all (pclDummy, " ", TRUE);
      if ((long) (strlen (pclDummy)) > 0)
      {
        ilGetRc = RC_FAIL;
      }
    }                           /* found record */
    slFkt = NEXT;
  }                             /* end while */
  commit_work ();
  close_my_cursor (&slCursor);
  

  /* DELETE  OLD RECORD */
  if ((ilRC == RC_SUCCESS) && (strcmp (pclFtyp, "X") == 0))
  {
    sprintf (pclSqlBuf, "DELETE FROM %s WHERE FKEY='%s' AND (STOD = '%s' OR STOA = '%s')", pcpTable, pcpFkey, pclStod,
             pclStoa);
  }
  else if (ilRC == RC_SUCCESS)
  {
    sprintf (pclSqlBuf, "DELETE FROM %s WHERE FKEY='%s' AND FTYP = '%s'", pcpTable, pcpFkey, pclFtyp);
  }
  if (ilRC == RC_SUCCESS)
  {
    strcpy (pclSqlDat, "");
    slCursor = 0;
    dbg (DEBUG, "TWESearchAndDelRoundtrip: <%s>", pclSqlBuf);
    
    ilGetRc = sql_if (IGNORE, &slCursor, pclSqlBuf, pclSqlDat);
    
    if (ilGetRc != DB_SUCCESS)
    {
      dbg (TRACE, "ERROR:TWESearchAndDelRoundtrip: Delete old by <%s> failed", pclSqlBuf);
      ilRC = RC_FAIL;
    }
    commit_work ();
    close_my_cursor (&slCursor);
  }

  /* MANIPULATE NEW RECORD WITH DATA OF OLD RECORD IF OLD RECORD FOUND */
  if ((ilRC == RC_SUCCESS) && (strcmp (pclFtyp, "X") == 0))
  {
    /*
       change this cancelled record to STOD and STOA from Database
       because they where validated before and valid for this
     */
    strcpy (prgRecFldDat[ipTbl][ipRec][ilStoa].NewDat, pclDBStoa);
    strcpy (prgRecFldDat[ipTbl][ipRec][ilStod].NewDat, pclDBStod);
  }
  else if (ilRC == RC_SUCCESS)
  {
    if (pclAdid[0] == 'D')
    {
      if (strcmp (pclDBStoa, pclStoa) > 0)
      {
        strcpy (prgRecFldDat[ipTbl][ipRec][ilStoa].NewDat, pclDBStoa);
      }
      else if (strcmp (pclDBStod, pclStod) < 0)
      {
        strcpy (prgRecFldDat[ipTbl][ipRec][ilStod].NewDat, pclDBStod);
      }
    }
    if (pclAdid[0] == 'A')
    {
      if (strcmp (pclDBStoa, pclStoa) > 0)
      {
        strcpy (prgRecFldDat[ipTbl][ipRec][ilStoa].NewDat, pclDBStoa);
      }
      else if (strcmp (pclDBStod, pclStod) < 0)
      {
        strcpy (prgRecFldDat[ipTbl][ipRec][ilStod].NewDat, pclDBStod);
      }
    }
    strcpy (prgRecFldDat[ipTbl][ipRec][ilJcnt].NewDat, pclJcnt);
    strcpy (prgRecFldDat[ipTbl][ipRec][ilJfno].NewDat, pclJfno);
    prgRecFldDat[ipTbl][ipRec][ilJcnt].NewFlg = TRUE;
    prgRecFldDat[ipTbl][ipRec][ilJfno].NewFlg = TRUE;
  }

  return ilRC;

}                               /* end TWESearchAndDelRoundtrip */

static int TWEBuildFlightNbr (char *fnr3, char *fnr2, char *alc3, char *alc2, char *flnr, char *flsf, int mth)
{
  int rc = RC_SUCCESS;
  int fnrLen;
  int i;
  int cp;
  int dp;
  char zeros[] = "0000";
  char tmpFlnr[16];
  char pclTmpTable[16];

  dbg (DEBUG, "TWEBuildFlightNbr: \nfnr3, fnr2, alc3, alc2, flnr, flsf\n<%s> <%s> <%s> <%s> <%s> <%s>", fnr3, fnr2,
       alc3, alc2, flnr, flsf);

  if (alc3[0] == '\0')
  {
    strcpy (alc3, alc2);
  }

  sprintf (pclTmpTable, "ALT%s", pcgBasExt);
  fnrLen = strlen (fnr3);
  if (fnrLen == 0)
  {
    /* TWE */
    sprintf (fnr3, "%s%s%s", alc3, flnr, flsf);
  }                             /* end if */

  /* fnr3 unformatierte FLNO (airline (min 2), flugnummer, suffix ) */

  dbg (DEBUG, "BUILD FLNO <%s>", fnr3);

  /* reset all fields that came */

  fnr2[0] = 0x00;
  alc3[0] = 0x00;
  alc2[0] = 0x00;
  flnr[0] = 0x00;
  flsf[0] = 0x00;

  fnrLen = strlen (fnr3);
  if (fnrLen > 2)
  {
    /* 3 or 2 Letter Airline code */
    cp = 2;
    if ((fnr3[cp] >= 'A') && (fnr3[cp] <= 'Z'))
    {
      cp = 3;
    }                           /* end if */

    /* Copy first signs to alc3 */
    for (i = 0; i < cp; i++)
    {
      alc3[i] = fnr3[i];
    }                           /* end for */
    alc3[cp] = 0x00;

    /* take not blanc or 0 from fnr3 */
    while ((cp < fnrLen) && ((fnr3[cp] == ' ') || (fnr3[cp] == '0')))
    {
      cp++;
    }                           /* end while */

    /* copy real numbers to flnr */
    dp = 0;
    while ((cp < fnrLen) && ((fnr3[cp] >= '0') && (fnr3[cp] <= '9')))
    {
      flnr[dp] = fnr3[cp];
      dp++;
      cp++;
      if (dp > 4)
      {                         /* maximal 5 zahlen */
        dp = 4;
        rc = RC_FAIL;
      }                         /* end if */
    }                           /* end while */
    flnr[dp] = 0x00;

    /* dont take blancs from fnr3 for suffix */
    while ((cp < fnrLen) && (fnr3[cp] == ' '))
    {
      cp++;
    }                           /* end while */

    /*  take sign from fnr3 for suffix */
    if (cp < fnrLen)
    {
      flsf[0] = fnr3[cp];
      flsf[1] = 0x00;
    }                           /* end if */
    else
    {
      /*  take blanc for suffix */
      flsf[0] = 0x20;
      flsf[1] = 0x00;
    }

    if (strlen (flnr) == 0)
    {
      rc = RC_FAIL;
    }                           /* end if */

    dbg (DEBUG, "1. ALC3<%s> ALC2<%s> FLNR<%s> FLSF<%s>", alc3, alc2, flnr, flsf);

    /* Check alc3 field for length */
    if (rc == RC_SUCCESS)
    {
      if (strlen (alc3) == 2)
      {
        if (mth != 1)
        {
          strcpy (alc2, alc3);
          if (strcmp (alc2, "YY") != 0)
          {
            rc = GetBasicData (pclTmpTable, "ALC2", alc2, "ALC3", alc3, 1, RC_FAIL);
          }                     /* end if */
        }                       /* end if */
        else
        {
          alc3[2] = 0x20;
          alc3[3] = 0x00;
        }                       /* end else */
      }                         /* end if */
      else
      {                         /* 3 Letter ok, get 2 Letter Airline */

        if (mth != 1)
        {
          rc = GetBasicData (pclTmpTable, "ALC3", alc3, "ALC2", alc2, 1, RC_FAIL);
          if (rc != RC_SUCCESS)
          {
            strcpy (alc2, "");  /* Dont take alc3 code, its written to global!  */
            rc = RC_SUCCESS;
          }                     /* end if */
        }                       /* end if */
      }                         /* end else */
    }                           /* end if */

    dbg (DEBUG, "2. ALC3<%s> ALC2<%s> FLNR<%s> FLSF<%s>", alc3, alc2, flnr, flsf);

    if (rc == RC_SUCCESS)
    {
      /* Add blanc to airlines for right format in flno */
      if (strlen (alc2) == 2)
      {
        alc2[2] = 0x20;
        alc2[3] = 0x00;
      }                         /* end if */
      if (strlen (alc3) == 2)
      {
        alc3[2] = 0x20;
        alc3[3] = 0x00;
      }                         /* end if */

      /* Add 0 in front of flnr (bis 3 stellig)  */
      cp = 3 - strlen (flnr);
      if (cp > 0)
      {
        zeros[cp] = 0x00;
        sprintf (tmpFlnr, "%s%s", zeros, flnr);
        strcpy (flnr, tmpFlnr);
      }                         /* end if */

      dbg (DEBUG, "3. ALC3<%s> ALC2<%s> FLNR<%s> FLSF<%s>", alc3, alc2, flnr, flsf);

      /* mit 0 aufgefüllt auf min 3 Stellen */
      if (strlen (flnr) > 3)
      {
        sprintf (fnr3, "%s%s%s", alc3, flnr, flsf);
        if (mth != 1)
        {
          sprintf (fnr2, "%s%s%s", alc2, flnr, flsf);
        }                       /* end if */
      }                         /* end if */
      else
      {                         /* 3 stellige flnr */

        if (strlen (flsf) > 0)
        {
          sprintf (fnr3, "%s%s %s", alc3, flnr, flsf);
          if (mth != 1)
          {
            sprintf (fnr2, "%s%s %s", alc2, flnr, flsf);
          }                     /* end if */
        }                       /* end if */
        else
        {
          sprintf (fnr3, "%s%s", alc3, flnr);
          if (mth != 1)
          {
            sprintf (fnr2, "%s%s", alc2, flnr);
          }                     /* end if */
        }                       /* end else */
      }                         /* end else */

      alc2[2] = 0x00;           /* delete blancs in airline */

    }                           /* end if */
  }                             /* end if */
  else
  {
    rc = RC_FAIL;
  }                             /* end else */
  dbg (DEBUG, "4. ALC3<%s> ALC2<%s> FLNR<%s> FLSF<%s>", alc3, alc2, flnr, flsf);
  return rc;
}                               /* end TWEBuildFlightNbr */

static int TWEAddJfnoToRecord (int ipTbl, int ipRec, int ipExists)
{
  int ilRC = RC_SUCCESS;
  int ilJcnt = 0;
  int ilLen = 0;
  int ilBlancs = 0;
  char pclJfno[200];
  char pclJcnt[10];
  char pclHelp[10];
  char pclDummy[20];

  /* don't touch JFNO when no new data available:  +++ jim +++ 20000825 */
  if ((pcgJoinAirl[0] != '\0') && /* if slave data complete */
      (pcgJoinFltn[0] != '\0') && /* flight is slave */
      (pcgJoinFlts[0] != '\0'))
  {
    dbg (DEBUG, "TWEAddJfnoToRecord: pcgCurrJfno: %s", pcgCurrJfno);
    strcpy (pclJcnt, pcgCurrJcnt);
    strcpy (pclJfno, pcgCurrJfno);

    /* fill up DB JFNO to a multiple of 9 chars: */
    ilLen = strlen (pclJfno);
    if ((ilBlancs = ilLen % 9) > 0)
    {
      ilBlancs = 9 - ilBlancs;
      while (ilBlancs > 0)
      {
        strcat (pclJfno, " ");  /* trim of ORACLE away and fill up to 9 signs */
        ilBlancs--;
      }
    }

    /* add to current JFNO and JCNT++  */
    /* reformat flight number */
    ilLen = strlen (pcgJoinFltn);
    pclHelp[0] = 0x00;
    while (ilLen < 3)
    {
      strcat (pclHelp, "0");
      ilLen++;
    }
    strcat (pclHelp, pcgJoinFltn);

    sprintf (pclDummy, "%-3s%-5s%s", pcgJoinAirl, pclHelp, pcgJoinFlts);

    /* DON'T insert a cancelled Slave!! */
    /* delete if exists already */

    if (strstr (pclJfno, pclDummy) == NULL)
    {
      strcat (pclJfno, pclDummy); /* add to JFNO */
      ilJcnt = atoi (pclJcnt);
      ilJcnt++;
      sprintf (pclJcnt, "%d", ilJcnt);
    }                         /* if Join flight not exist in JFNO */

    if (ipExists == TRUE)
    {
      igTWEJustUpdJfno = TRUE;
      dbg (DEBUG, "FLIGHT IS AN SLAVE SO JUST UPDATE JFNO");
    }
    else
    {
      dbg (DEBUG, "SLAVE BEFORE EXISTING MASTER / INSERT MASTER WITH JFNO <%s>", pclJfno);
    }

    /* don't touch JFNO when no new data available:  +++ jim +++ 20000825 */
    (void) SetNewValues (ipTbl, ipRec, 0, "JFNO", pclJfno);
    (void) SetNewValues (ipTbl, ipRec, 0, "JCNT", pclJcnt);
  }                             /* if new slavedata exists */

  return ilRC;

}                               /* end of TWEAddJfnoToRecord */


static int TWEGetLocTimDepends (void)
{
  int ilGetRc = RC_SUCCESS;

  ilGetRc = GetCfg ("LOCATIONTIMEDEPENDS", "DEPEND_LOC_TIME_FIELDS", CFG_STRING, pcgTWEDepLocTimFlds, "\0");

  if (ilGetRc == RC_SUCCESS)
  {
    dbg (TRACE, "DEPEND_LOC_TIME_FIELDS <%s>", pcgTWEDepLocTimFlds);
    SetReadAlways (igAftTab, pcgTWEDepLocTimFlds);
  }
  else
  {
    dbg (TRACE, "config: DEPEND_LOC_TIME_FIELDS not found");
    ilGetRc = RC_SUCCESS;
  }

  return ilGetRc;

}                               /* end of TWEGetLocTimDepends */

static int TWEChangeDepTimeFlds (int ipTbl, int ipRec)
{
  int ilRC = RC_SUCCESS;
  int ilFld;
  int ilDepFld;
  int ilCmpFld;
  int ilItmLen;
  int ilNext;
  long llDiff;
  char *pclConfPtr;
  char *pclRowPtr;
  char *pclCmpFldPtr;

  char pclDepField[20];         /* depended field */
  char pclDepDbsDat[20];        /* time in depended field in DB */
  char pclSendDat[20];          /* time send in compare field */
  char pclDbsDat[20];           /* time of main field in DB */
  char pclNewData[20];          /* changed time in depended field */
  char pclDbFld[20];            /* main field */
  char pclCompField[20];        /* compare field */

  char pclConfRow[512];         /* list of rules (|) */
  char pclRelFields[100];       /* list of fields */

  int ilHourDiff;
  int ilMinDiff;
  char pclMin1[3];
  char pclMin2[3];
  char pclHour1[3];
  char pclHour2[3];

  if (ipTbl != igAftTab)
  {
    return ilRC;
  }

  /* for all depending fields */
  pclConfPtr = pcgTWEDepLocTimFlds;
  while (GetNextDataItem (pclDepField, &pclConfPtr, ",", "\0", "  ") > 0)
  {
    (void) GetCfg ("LOCATIONTIMEDEPENDS", pclDepField, CFG_STRING, pclConfRow, "\0");
    /* +++ jim +++ 20010124: add pclDepField to dbg output */
    dbg (DEBUG, "FOUND RULE: <%s>= <%s>", pclDepField, pclConfRow);
    ilDepFld = GetFieldIndex (ipTbl, pclDepField);

/* TODO ALSO CHECK WETHER TIMEFIELD */

    if (ilDepFld <= 0)
    {
      /* field not in AFTTAB */
      dbg (DEBUG, "field not in AFTTAB => no auto update");
    }
    else if (prgRecFldDat[ipTbl][ipRec][ilDepFld].RcvFlg == TRUE)
    {
      /* field was send by client */
      dbg (DEBUG, "field was send by client => no auto update");
    }
    else
    {

      strcpy (pclDepDbsDat, prgRecFldDat[ipTbl][ipRec][ilDepFld].DbsDat);
      tool_filter_spaces (pclDepDbsDat);
      if (pclDepDbsDat[0] == '\0')
      {
/*      dbg (TRACE, "field not in DbsDat[]");  nearly every rule ...*/
        /* no data for field till now */
      }
      else
      {
        /* fields data updaten with diff */
        /* get first related field */
        pclRowPtr = strstr (pclConfRow, ":");
        pclRowPtr++;
        ilNext = TRUE;
        while ((ilNext == TRUE) && (GetNextDataItem (pclRelFields, &pclRowPtr, "|", "\0", "  ") > 0))
        {
          /* dbg (DEBUG, "pclRelFields <%s>", pclRelFields); */
          (void) GetDataItem (pclDbFld, pclRelFields, 1, ',', "\0", "  ");

          /* dbg (DEBUG, "Check if %s is in DB", pclDbFld); */
          ilFld = GetFieldIndex (ipTbl, pclDbFld);
          if (ilFld <= 0)
          {
          }                     /* not in AFTTAB */
          else
          {
            strcpy (pclDbsDat, prgRecFldDat[ipTbl][ipRec][ilFld].DbsDat);
            tool_filter_spaces (pclDbsDat);
            if (pclDbsDat[0] != '\0')
            {
              dbg (DEBUG, "main field %s/DBS is <%s> from DB", pclDbFld, pclDbsDat);
              pclCmpFldPtr = strstr (pclRelFields, ",");
              if (pclCmpFldPtr != NULL)
              {
                pclCmpFldPtr++;
                while ((ilNext == TRUE) &&
                       (GetNextDataItem (pclCompField, &pclCmpFldPtr, ",", "\0", "  ") > 0))
                {
                  dbg (DEBUG, "Search %s in New list (not received)", pclCompField);
                  ilCmpFld = GetFieldIndex (ipTbl, pclCompField);
                  strcpy (pclSendDat, prgRecFldDat[ipTbl][ipRec][ilCmpFld].NewDat);
                  tool_filter_spaces (pclSendDat);
                  if (pclSendDat[0] != '\0')
                  {
                    dbg (DEBUG, "Compfield %s contents <%s> in NewDat", pclCompField, pclSendDat);
                    memcpy (pclHour1, &(pclSendDat[8]), 2);
                    pclHour1[2] = 0x00;
                    memcpy (pclMin1, &(pclSendDat[10]), 2);
                    pclMin1[2] = 0x00;
                    memcpy (pclHour2, &(pclDbsDat[8]), 2);
                    pclHour2[2] = 0x00;
                    memcpy (pclMin2, &(pclDbsDat[10]), 2);
                    pclMin2[2] = 0x00;

                    /* Send Minus DB = Diff */
                    /* Diff could be negativ or positiv */
                    /* Maximum of Diff is 12 Hours!!! */
                    ilHourDiff = atoi (pclHour1) - atoi (pclHour2);
                    if (ilHourDiff < -12)
                    {
                      ilHourDiff += 24;
                    }

                    ilMinDiff = atoi (pclMin1) - atoi (pclMin2);
                    if ((ilMinDiff < 0) && (ilHourDiff > 0))
                    {
                      ilMinDiff += 60;
                      ilHourDiff--; /* reduce diff */
                    }
                    else if ((ilMinDiff > 0) && (ilHourDiff < 0))
                    {
                      ilMinDiff -= 60;
                      ilHourDiff++; /* reduce diff */
                    }

                    ilHourDiff *= 60;
                    ilMinDiff += ilHourDiff;
                    llDiff = ilMinDiff * 60; /* diff in Seconds */
                    strcpy (pclNewData, pclDepDbsDat);
                    ilRC = AddSecondsToCEDATime (pclNewData, llDiff, 1);
                    if (ilRC == RC_SUCCESS)
                    {
                      dbg (DEBUG, "Update field <%s> from <%s> to <%s>", pclDepField, pclDepDbsDat, pclNewData);
                      /* UPDATE */
                      strcpy (prgRecFldDat[ipTbl][ipRec][ilDepFld].NewDat, pclNewData);
                      prgRecFldDat[ipTbl][ipRec][ilDepFld].NewFlg = TRUE;
                      ilNext = FALSE;
                    }
                  }             /* CmpFlds contents data */
                }               /* while CmpFlds */
              }                 /* Komma found behind main fld */
            }                   /* main fld contains data */
          }                     /* 1.field found in aft (main) */
        }                       /* while related fields exists in rule (|) */
      }
    }
  }

  return ilRC;

}                               /* end of TWEChangeDepTimeFlds */


static int TWEHardCodeFlno (void)
{
  int ilRC = RC_SUCCESS;
  int ilTbl;
  int ilRecCnt;
  int ilRec;

  if (igTWESplit == TRUE)
  {
    return ilRC;
  }

  if (strstr (pcgTWEFieldsRecv, "FLNO") != NULL)
  {
    return ilRC;
  }

  for (ilTbl = 1; ilTbl <= igHandledTables; ilTbl++)
  {
    if (prgPrjTblCfg[ilTbl].RcvFlg == TRUE)
    {
      ilRecCnt = prgPrjTblCfg[ilTbl].RecHdlCnt;
      for (ilRec = 1; ilRec <= ilRecCnt; ilRec++)
      {
        (void) SetFlnoItems (ilTbl, ilRec);
dbg(DEBUG,"%05d:",__LINE__);
      }
    }
  }

  return ilRC;
}                               /* end of TWEHardCodeFlno */
