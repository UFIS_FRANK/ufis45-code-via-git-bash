DROP VIEW CEDA.SHIFT_CHANGE;

/* Formatted on 2012/06/18 10:16 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW ceda.shift_change (avfr,
                                                avto,
                                                acti,
                                                fcol,
                                                drrn,
                                                fctc,
                                                scod,
                                                sday,
                                                stfu,
                                                peno,
                                                finm,
                                                lanm,
                                                cano,
                                                spfcode,
                                                sorcode,
                                                shnm,
                                                shiftdiff,
                                                shiftdiffh,
                                                qu
                                               )
AS
   SELECT DISTINCT avfr, avto, sp.acti, sp.fcol, drrn, fctc, scod, dr.sday,
                   dr.stfu, stftab.peno, stftab.finm, stftab.lanm,
                   stftab.cano, spftab.code spfcode, sortab.code sorcode,
                   stftab.shnm,
                   FLOOR (  (  TO_DATE (avfr, 'YYYYMMDDHH24MISS')
                             - TO_DATE (sp.acti, 'YYYYMMDDHH24MISS')
                            )
                          * 24
                          * 60
                         ) shiftdiff,
                   FLOOR (  (  TO_DATE (avfr, 'YYYYMMDDHH24MISS')
                             - TO_DATE (sp.acti, 'YYYYMMDDHH24MISS')
                            )
                          * 24
                         ) shiftdiffh,
                   'Q1' qu
              FROM drrtab dr, sortab, spftab, stftab, sprtab sp
             WHERE dr.rosl = '3'
               -- AND dr.ross = 'A'
               AND dr.stfu = sp.ustf
               AND (    sp.acti
                           BETWEEN TO_CHAR
                                      (  TO_TIMESTAMP (avfr,
                                                       'YYYY-MM-DD HH24:MI:SS'
                                                      )
                                       - 2600 / 3600,
                                       'YYYYMMDDHH24MISS'
                                      )
                               AND TO_CHAR
                                      (  TO_TIMESTAMP (avfr,
                                                       'YYYY-MM-DD HH24:MI:SS'
                                                      )
                                       + 2600 / 3600,
                                       'YYYYMMDDHH24MISS'
                                      )
                    AND sp.acti BETWEEN sday || '000000' AND sday || '235959'
                   --TO_CHAR
                   --             (  TO_TIMESTAMP (sday || '235959',
                   --                              'YYYY-MM-DD HH24:MI:SS'
                   --                             )
                    --             + 24 / 24,
                    --             'YYYYMMDD'
                     --           )
                     --  || '235959'
                   )
               AND sp.fcol = 1
               --and drrn = 1

               -- AND scod NOT IN (SELECT sdac
               --                    FROM odatab)
               AND (    (spftab.surn = stftab.urno)
                    AND (sortab.surn = stftab.urno)
                   )
               AND dr.stfu = stftab.urno
               AND (    (   (dr.sday BETWEEN spftab.vpfr AND spftab.vpto)
                         OR     (spftab.vpfr <= dr.sday AND spftab.vpto = ' '
                                )
                            AND (spftab.prio = '1')
                        )
                    AND (   (dr.sday BETWEEN sortab.vpfr AND sortab.vpto)
                         OR (sortab.vpfr <= dr.sday AND sortab.vpto = ' ')
                        )
                   )
               -- and dr.stfu not in (select ustf from sprtab where acti BETWEEN sday || '000000'
                --      AND sday || '235959' and drrn=2   )
               AND dr.stfu NOT IN (
                                   SELECT adrr.stfu
                                     FROM drrtab adrr
                                    WHERE adrr.sday = dr.sday
                                          AND adrr.drrn = 2)
   UNION
   SELECT DISTINCT avfr, avto, sp.acti, sp.fcol, drrn, fctc, scod, dr.sday,
                   dr.stfu, stftab.peno, stftab.finm, stftab.lanm,
                   stftab.cano, spftab.code spfcode, sortab.code sorcode,
                   stftab.shnm,
                   FLOOR (  (  TO_DATE (avfr, 'YYYYMMDDHH24MISS')
                             - TO_DATE (sp.acti, 'YYYYMMDDHH24MISS')
                            )
                          * 24
                          * 60
                         ) shiftdiff,
                   FLOOR (  (  TO_DATE (avfr, 'YYYYMMDDHH24MISS')
                             - TO_DATE (sp.acti, 'YYYYMMDDHH24MISS')
                            )
                          * 24
                         ) shiftdiffh,
                   'Q2' qu
              FROM drrtab dr, sortab, spftab, stftab, sprtab sp
             WHERE dr.rosl = '3'
               -- AND dr.ross = 'A'
               AND dr.stfu = sp.ustf
               AND (sp.acti
                       BETWEEN TO_CHAR
                                      (  TO_TIMESTAMP (avfr,
                                                       'YYYY-MM-DD HH24:MI:SS'
                                                      )
                                       - 2100 / 3600,
                                       'YYYYMMDDHH24MISS'
                                      )
                           AND TO_CHAR
                                      (  TO_TIMESTAMP (avfr,
                                                       'YYYY-MM-DD HH24:MI:SS'
                                                      )
                                       + 2100 / 3600,
                                       'YYYYMMDDHH24MISS'
                                      )
                   -- sp.acti BETWEEN sday || '000000'
                   --           AND sday || '235959'

                   --TO_CHAR
                   --             (  TO_TIMESTAMP (sday || '235959',
                   --                              'YYYY-MM-DD HH24:MI:SS'
                   --                             )
                    --             + 24 / 24,
                    --             'YYYYMMDD'
                     --           )
                     --  || '235959'
                   )
               AND sp.fcol = 1
                --and drrn = 2
               -- AND scod NOT IN (SELECT sdac
                --                   FROM odatab)
               AND (    (spftab.surn = stftab.urno)
                    AND (sortab.surn = stftab.urno)
                   )
               AND dr.stfu = stftab.urno
               AND (    (   (dr.sday BETWEEN spftab.vpfr AND spftab.vpto)
                         OR     (spftab.vpfr <= dr.sday AND spftab.vpto = ' '
                                )
                            AND (spftab.prio = '1')
                        )
                    AND (   (dr.sday BETWEEN sortab.vpfr AND sortab.vpto)
                         OR (sortab.vpfr <= dr.sday AND sortab.vpto = ' ')
                        )
                   )
               AND dr.stfu IN (SELECT adrr.stfu
                                 FROM drrtab adrr
                                WHERE adrr.sday = dr.sday AND adrr.drrn = 2)
--   ORDER BY stftab.peno;;;;;;;;;;;;;;;;;;;
