DROP VIEW CEDA.REPORT_NOHIT;

/* Formatted on 2010/12/22 12:45 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW ceda.report_nohit (avfr,
                                                avto,
                                                drrn,
                                                fctc,
                                                scod,
                                                sday,
                                                stfu,
                                                peno,
                                                finm,
                                                lanm,
                                                cano,
                                                spfcode,
                                                sorcode,
                                                shnm,
                                                acti
                                               )
AS
   SELECT DISTINCT avfr, avto, drrn, fctc, scod, dr.sday, dr.stfu,
                   stftab.peno, stftab.finm, stftab.lanm, stftab.cano,
                   spftab.code spfcode, sortab.code sorcode, stftab.shnm,
                   ' ' acti
              FROM drrtab dr, sortab, spftab, stftab
             WHERE dr.rosl = '3'
               AND dr.ross = 'A'
               AND scod NOT IN (SELECT sdac
                                  FROM odatab)
               AND (    (spftab.surn = stftab.urno)
                    AND (sortab.surn = stftab.urno)
                   )
               AND dr.stfu = stftab.urno
               AND (    (   (dr.sday BETWEEN spftab.vpfr AND spftab.vpto)
                         OR     (spftab.vpfr <= dr.sday AND spftab.vpto = ' '
                                )
                            AND (spftab.prio = '1')
                        )
                    AND (   (dr.sday BETWEEN sortab.vpfr AND sortab.vpto)
                         OR (sortab.vpfr <= dr.sday AND sortab.vpto = ' ')
                        )
                   )
          GROUP BY avfr,
                   avto,
                   drrn,
                   fctc,
                   scod,
                   dr.sday,
                   dr.stfu,
                   stftab.peno,
                   stftab.finm,
                   stftab.lanm,
                   stftab.cano,
                   spftab.code,
                   sortab.code,
                   stftab.shnm
          ORDER BY stftab.peno;

