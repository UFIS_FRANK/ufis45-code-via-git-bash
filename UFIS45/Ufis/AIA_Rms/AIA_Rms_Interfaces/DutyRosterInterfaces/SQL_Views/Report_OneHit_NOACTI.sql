DROP VIEW CEDA.REPORT_ONEHIT_NOACTI;

/* Formatted on 2010/12/22 12:45 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW ceda.report_onehit_noacti (scod, sday, stfu, cn)
AS
   SELECT   /*+ ALL_ROWS */
            dr.scod, dr.sday, dr.stfu, COUNT (sp.fcol) cn
       FROM drrtab dr, sprtab sp
      WHERE dr.rosl = '3'
        AND dr.ross = 'A'
        AND scod NOT IN (SELECT sdac
                           FROM odatab)
        AND dr.stfu = sp.ustf
        AND (sp.acti BETWEEN sday || '000000'
                         AND    TO_CHAR
                                      (  TO_TIMESTAMP (sday || '235959',
                                                       'YYYY-MM-DD HH24:MI:SS'
                                                      )
                                       + 24 / 24,
                                       'YYYYMMDD'
                                      )
                             || '235959'
            )
   GROUP BY dr.scod, dr.sday, dr.stfu
     HAVING COUNT (sp.fcol) < 2;

