DROP VIEW CEDA.REPORT_ONEHIT;

/* Formatted on 2010/12/22 12:45 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW ceda.report_onehit (avfr,
                                                 avto,
                                                 drrn,
                                                 fctc,
                                                 scod,
                                                 sday,
                                                 stfu,
                                                 cn,
                                                 peno,
                                                 finm,
                                                 lanm,
                                                 cano,
                                                 spfcode,
                                                 sorcode,
                                                 shnm,
                                                 fcol,
                                                 acti
                                                )
AS
   SELECT   avfr, avto, drrn, fctc, scod, dr.sday, dr.stfu, COUNT (sp.fcol)
                                                                           cn,
            stftab.peno, stftab.finm, stftab.lanm, stftab.cano,
            spftab.code spfcode, sortab.code sorcode, stftab.shnm,
            MAX (sp.fcol) fcol, MAX (sp.acti)
       FROM drrtab dr, sprtab sp, sortab, spftab, stftab
      WHERE dr.rosl = '3'
        AND dr.ross = 'A'
        AND scod NOT IN (SELECT sdac
                           FROM odatab)
        AND dr.stfu = sp.ustf
        AND (sp.acti BETWEEN sday || '000000'
                         AND    TO_CHAR
                                      (  TO_TIMESTAMP (sday || '235959',
                                                       'YYYY-MM-DD HH24:MI:SS'
                                                      )
                                       + 24 / 24,
                                       'YYYYMMDD'
                                      )
                             || '235959'
            )
        AND ((spftab.surn = stftab.urno) AND (sortab.surn = stftab.urno))
        AND dr.stfu = stftab.urno
        AND (    (   (dr.sday BETWEEN spftab.vpfr AND spftab.vpto)
                  OR     (spftab.vpfr <= dr.sday AND spftab.vpto = ' ')
                     AND (spftab.prio = '1')
                 )
             AND (   (dr.sday BETWEEN sortab.vpfr AND sortab.vpto)
                  OR (sortab.vpfr <= dr.sday AND sortab.vpto = ' ')
                 )
            )
   GROUP BY avfr,
            avto,
            drrn,
            fctc,
            scod,
            dr.sday,
            dr.stfu,
            stftab.peno,
            stftab.finm,
            stftab.lanm,
            stftab.cano,
            spftab.code,
            sortab.code,
            stftab.shnm,
            sp.fcol,
            sp.acti
--   TO_CHAR (TO_TIMESTAMP(sp.acti,'YYYY-MM-DD'),'YYYYMMDDHH24MISS')
   HAVING   COUNT (sp.fcol) < 2;

