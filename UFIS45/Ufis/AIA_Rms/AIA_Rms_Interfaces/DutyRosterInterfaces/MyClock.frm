VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.ocx"
Begin VB.Form MyClock 
   Caption         =   "Collection of Clock Icons"
   ClientHeight    =   1545
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4290
   LinkTopic       =   "Form1"
   ScaleHeight     =   1545
   ScaleWidth      =   4290
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox Text1 
      Height          =   345
      Left            =   90
      TabIndex        =   0
      Text            =   "Text1"
      Top             =   960
      Width           =   3045
   End
   Begin VB.Timer Timer1 
      Interval        =   7500
      Left            =   2640
      Top             =   300
   End
   Begin MSComctlLib.ImageList ClockIcons 
      Left            =   1920
      Top             =   270
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   8
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyClock.frx":0000
            Key             =   "hms0300000"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyClock.frx":031A
            Key             =   "hms0300075"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyClock.frx":0634
            Key             =   "hms0300150"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyClock.frx":094E
            Key             =   "hms0300225"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyClock.frx":0C68
            Key             =   "hms0300300"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyClock.frx":0F82
            Key             =   "hms0300375"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyClock.frx":129C
            Key             =   "hms0300450"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyClock.frx":15B6
            Key             =   "hms0300525"
         EndProperty
      EndProperty
   End
   Begin VB.Image Image1 
      Height          =   480
      Left            =   120
      Picture         =   "MyClock.frx":18D0
      Top             =   210
      Width           =   480
   End
End
Attribute VB_Name = "MyClock"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Timer1_Timer()
    Dim nowKey As String
    Dim IconKey As String
    Dim ilHH As Integer
    Dim ilMM As Integer
    Dim ilSS As Integer
    Dim IconIndex As Integer
    nowKey = Format(Now, "hhmmss")
    ilHH = Val(Left(nowKey, 2))
    ilMM = Val(Mid(nowKey, 3, 2))
    ilSS = Val(Right(nowKey, 2)) * 10
    ilHH = 3
    ilMM = 0
    ilSS = (Int(ilSS / 75)) * 75
    IconKey = "hms"
    IconKey = IconKey & Right("00" & CStr(ilHH), 2)
    IconKey = IconKey & Right("00" & CStr(ilMM), 2)
    IconKey = IconKey & Right("000" & CStr(ilSS), 3)
    Text1.Text = IconKey
    IconIndex = GetIconIndex(IconKey)
    If IconIndex > 0 Then Image1.Picture = ClockIcons.ListImages(IconIndex).Picture
End Sub
Private Function GetIconIndex(IconKey As String) As Integer
    On Error GoTo ErrorHandler
    GetIconIndex = ClockIcons.ListImages(IconKey).Index
    Exit Function
ErrorHandler:
    GetIconIndex = -1
    Resume Next
End Function

