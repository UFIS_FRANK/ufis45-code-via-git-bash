VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form frmEmployees 
   Caption         =   "Missing Shiftroster Level"
   ClientHeight    =   6150
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   10590
   Icon            =   "frmEmployees.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   6150
   ScaleWidth      =   10590
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdClose 
      Caption         =   "&Close"
      Height          =   375
      Left            =   4598
      TabIndex        =   3
      Top             =   5625
      Width           =   1395
   End
   Begin VB.ListBox lstEmployees 
      Height          =   4935
      Left            =   195
      TabIndex        =   2
      Top             =   450
      Width           =   10080
   End
   Begin TABLib.TAB TabStf 
      Height          =   645
      Left            =   9435
      TabIndex        =   0
      Tag             =   "{=TABLE=}STFTAB{=FIELDS=}URNO,PENO,LANM,FINM,SHNM,PERC"
      Top             =   5340
      Visible         =   0   'False
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1138
      _StockProps     =   64
   End
   Begin VB.Label lblResult 
      Height          =   285
      Left            =   180
      TabIndex        =   4
      Top             =   5655
      Width           =   3345
   End
   Begin VB.Label Label1 
      Caption         =   "Employees without Level 1-shift, which have a Level 2-shift:"
      Height          =   255
      Left            =   210
      TabIndex        =   1
      Top             =   105
      Width           =   5655
   End
End
Attribute VB_Name = "frmEmployees"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim bmLoaded As Boolean

Private Sub cmdClose_Click()
    Me.Hide
End Sub

Private Sub Form_Activate()
    Dim l As Long
    Dim llLineCount As Long
    Dim llCountCases As Long
    Dim strTmp As String
    Dim strLineNo As String
    Dim llStfLine As Long
    Dim usec As String

    lstEmployees.Clear
    llCountCases = 0
    llLineCount = frmMainCleaner.TabDRR.GetLineCount - 2
    For l = 0 To llLineCount Step 1
        If frmMainCleaner.TabDRR.GetFieldValue(l, "STFU") <> frmMainCleaner.TabDRR.GetFieldValue(l + 1, "STFU") Then
            'new employee in next line, if ROSL <> 1 then level 1 is missing!
            If frmMainCleaner.TabDRR.GetFieldValue(l + 1, "ROSL") <> 1 Then
                
                strLineNo = TabStf.GetLinesByColumnValue(0, frmMainCleaner.TabDRR.GetFieldValue(l + 1, "STFU"), 0)
                usec = frmMainCleaner.TabDRR.GetFieldValue(l + 1, "USEC")
                If IsNumeric(strLineNo) And usec <> "SAP-HR" = True Then
                    llStfLine = CLng(strLineNo)
                    strTmp = TabStf.GetFieldValue(llStfLine, "PENO") & " "
                    strTmp = strTmp & frmMainCleaner.TabDRR.GetFieldValue(l + 1, "SDAY") & " "
                    strTmp = strTmp & TabStf.GetFieldValue(llStfLine, "FINM") & " "
                    strTmp = strTmp & TabStf.GetFieldValue(llStfLine, "LANM") & " "
                    strTmp = strTmp & TabStf.GetFieldValue(llStfLine, "SHNM")
                    lstEmployees.AddItem strTmp
                    frmMainCleaner.TabDRR.SetLineColor l + 1, vbBlack, vbGreen
                    llCountCases = llCountCases + 1
                End If
            End If
        End If
    Next l
    lblResult = "Found " & CStr(llCountCases) & " cases in the loaded data."
    frmMainCleaner.TabDRR.RedrawTab
End Sub

Private Sub Form_Load()
    If bmLoaded = False Then
        bmLoaded = True
        frmMainCleaner.InitTabGeneral TabStf
        frmMainCleaner.InitTabForCedaConnection TabStf
        frmMainCleaner.StatusBar1.Panels(1).Text = "Please wait while loading STF data ..."
        frmMainCleaner.LoadData TabStf, "WHERE URNO > 0"
        frmMainCleaner.StatusBar1.Panels(1).Text = "Ready."
    End If
End Sub
