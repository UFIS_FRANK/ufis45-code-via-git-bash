VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form frmHiddenData 
   Caption         =   "Hidden Data Grids"
   ClientHeight    =   10785
   ClientLeft      =   165
   ClientTop       =   555
   ClientWidth     =   15240
   LinkTopic       =   "Form1"
   ScaleHeight     =   10785
   ScaleWidth      =   15240
   StartUpPosition =   3  'Windows Default
   Begin TABLib.TAB rpt 
      Height          =   1845
      Left            =   5760
      TabIndex        =   15
      Top             =   8610
      Width           =   3795
      _Version        =   65536
      _ExtentX        =   6694
      _ExtentY        =   3254
      _StockProps     =   64
   End
   Begin TABLib.TAB TabLanguage 
      Height          =   1695
      Left            =   270
      TabIndex        =   13
      Top             =   8580
      Width           =   3825
      _Version        =   65536
      _ExtentX        =   6747
      _ExtentY        =   2990
      _StockProps     =   64
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Command2"
      Height          =   225
      Left            =   12960
      TabIndex        =   12
      Top             =   60
      Width           =   855
   End
   Begin TABLib.TAB helper 
      Height          =   4755
      Left            =   2580
      TabIndex        =   1
      Top             =   30
      Visible         =   0   'False
      Width           =   2235
      _Version        =   65536
      _ExtentX        =   3942
      _ExtentY        =   8387
      _StockProps     =   64
   End
   Begin TABLib.TAB TAB1 
      Height          =   6765
      Left            =   4710
      TabIndex        =   6
      Top             =   600
      Width           =   5175
      _Version        =   65536
      _ExtentX        =   9128
      _ExtentY        =   11933
      _StockProps     =   64
   End
   Begin TABLib.TAB BSDTAB 
      Height          =   6765
      Left            =   9960
      TabIndex        =   3
      Top             =   600
      Width           =   3045
      _Version        =   65536
      _ExtentX        =   5371
      _ExtentY        =   11933
      _StockProps     =   64
   End
   Begin TABLib.TAB ODATAB 
      Height          =   2505
      Left            =   60
      TabIndex        =   2
      Top             =   600
      Width           =   4575
      _Version        =   65536
      _ExtentX        =   8070
      _ExtentY        =   4419
      _StockProps     =   64
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Close"
      Height          =   315
      Left            =   30
      TabIndex        =   0
      Top             =   30
      Width           =   1035
   End
   Begin TABLib.TAB SPFTAB 
      Height          =   3825
      Left            =   60
      TabIndex        =   8
      Top             =   3540
      Width           =   4575
      _Version        =   65536
      _ExtentX        =   8070
      _ExtentY        =   6747
      _StockProps     =   64
   End
   Begin TABLib.TAB SPRTAB 
      Height          =   6765
      Left            =   13080
      TabIndex        =   10
      Top             =   600
      Width           =   4125
      _Version        =   65536
      _ExtentX        =   7276
      _ExtentY        =   11933
      _StockProps     =   64
   End
   Begin VB.Label Label7 
      Caption         =   "Report"
      Height          =   375
      Left            =   7350
      TabIndex        =   16
      Top             =   8280
      Width           =   945
   End
   Begin VB.Label Label6 
      Caption         =   "TabLanguage"
      Height          =   255
      Left            =   990
      TabIndex        =   14
      Top             =   8310
      Width           =   1485
   End
   Begin VB.Label Label5 
      Caption         =   "SPRTAB"
      Height          =   255
      Left            =   13110
      TabIndex        =   11
      Top             =   270
      Width           =   1005
   End
   Begin VB.Label Label4 
      Caption         =   "SPFTAB"
      Height          =   255
      Left            =   1320
      TabIndex        =   9
      Top             =   3240
      Width           =   1005
   End
   Begin VB.Label Label3 
      Caption         =   "Absence Codes Lookup"
      Height          =   315
      Left            =   4860
      TabIndex        =   7
      Top             =   300
      Width           =   1995
   End
   Begin VB.Label Label2 
      Caption         =   "BSDTAB"
      Height          =   255
      Left            =   9990
      TabIndex        =   5
      Top             =   270
      Width           =   1005
   End
   Begin VB.Label Label1 
      Caption         =   "ODATAB"
      Height          =   255
      Left            =   1260
      TabIndex        =   4
      Top             =   300
      Width           =   1005
   End
End
Attribute VB_Name = "frmHiddenData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Public omFile As String
'Public strReport As String
Private strLanguage As String
Private strFieldSeparator As String

Private Sub Command1_Click()
    Me.Hide
End Sub

Private Sub Command2_Click()
    Dim tmpSelKey As String
    tmpSelKey = "WHERE ACTI>'20060401000000'"
    LoadSprTabData SPRTAB, tmpSelKey
End Sub

Public Sub LoadSprTabData(SPRTAB As TABLib.TAB, UseSelKey As String)
    Dim CdrAnsw As Boolean
    Dim tmpGridFields As String
    Dim tmpCmd As String
    Dim tmpTable As String
    Dim tmpFields As String
    Dim tmpSqlKey As String
    Dim tmpHeader As String
    Dim tmpLength As String
    Dim tmpData As String
    Dim tmpKeys As String
    Dim tmpUstf As String
    Dim tmpSday As String
    Dim tmpFcol As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim tmpLen As Integer
    
    tmpGridFields = "LINE,STAT,CMDS,PKNO,FCOL,ACTI,USTF,URNO,KEYS,' ',' ',' '"
    tmpHeader = "L,S,A,PKNO,FCOL,ACTI,USTF,URNO,KEYS,1,2,3"
    tmpLength = "10,10,10,10,10,10,10,10,10,10,10,10"
    
    SPRTAB.ResetContent
    SPRTAB.ShowVertScroller False
    SPRTAB.ShowHorzScroller False
    SPRTAB.HeaderString = tmpHeader
    SPRTAB.HeaderLengthString = tmpLength
    SPRTAB.LogicalFieldList = tmpGridFields
    SPRTAB.SetMainHeaderValues "3,1,2,6", "Line,EMPL,Clocking,System", ",,,"
    SPRTAB.SetMainHeaderFont 17, False, False, True, 0, "Arial"
    SPRTAB.AutoSizeByHeader = True
    SPRTAB.AutoSizeColumns
    SPRTAB.Refresh
    
    tmpCmd = "RTA"
    tmpTable = "SPRTAB"
    tmpFields = "'N',' ',' ',PKNO,FCOL,ACTI,USTF,URNO,' ',' ',' ',' '"

    SPRTAB.CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
    SPRTAB.CedaHopo = UfisServer.HOPO
    SPRTAB.CedaIdentifier = ""
    SPRTAB.CedaPort = "3357"
    SPRTAB.CedaReceiveTimeout = "250"
    SPRTAB.CedaRecordSeparator = vbLf
    SPRTAB.CedaSendTimeout = "250"
    SPRTAB.CedaServerName = UfisServer.HostName
    SPRTAB.CedaTabext = UfisServer.TblExt
    SPRTAB.CedaUser = UfisServer.ModName
    SPRTAB.CedaWorkstation = UfisServer.GetMyWorkStationName

    If CedaIsConnected Then
        CdrAnsw = SPRTAB.CedaAction(tmpCmd, tmpTable, tmpFields, "", UseSelKey)
    ElseIf UfisServer.HostName = "LOCAL" Then
        'SPRTAB.ReadFromFile "c:\tmp\RmsImportTab2_3.txt"
    End If
    
    MaxLine = SPRTAB.GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpUstf = Trim(SPRTAB.GetFieldValue(CurLine, "USTF"))
        tmpSday = Trim(SPRTAB.GetFieldValue(CurLine, "ACTI"))
        tmpFcol = Trim(SPRTAB.GetFieldValue(CurLine, "FCOL"))
        tmpUstf = Right("0000000000" & tmpUstf, 10)
        tmpSday = Left(tmpSday, 8)
        tmpKeys = tmpUstf & "-" & tmpSday & "-" & tmpFcol
        SPRTAB.SetFieldValues CurLine, "KEYS", tmpKeys
    Next
    
    SPRTAB.ShowVertScroller True
    SPRTAB.ShowHorzScroller True
    
    SPRTAB.AutoSizeByHeader = True
    SetTabSortCols SPRTAB, "ACTI,PKNO"
    SetTabIndexes SPRTAB, "KEYS", "KEYS", True
    
    SPRTAB.AutoSizeColumns
    SPRTAB.Refresh
    
    Screen.MousePointer = 0
End Sub

Public Sub LoadBasicData(CurTab As TABLib.TAB, TANA As String, LoadFile As String, SqlKey As String)
    Dim CdrAnsw As Boolean
    Dim tmpCmd As String
    Dim tmpTable As String
    Dim tmpFields As String
    Dim tmpSqlKey As String
    Dim tmpHeader As String
    Dim tmpLength As String
    Dim tmpData As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim tmpLen As Integer
    
    LoadSystabInfo helper, TANA
    tmpFields = helper.SelectDistinct("0", "", "", ",", False)
    tmpLength = helper.SelectDistinct("1", "", "", ",", False)
    
    CurTab.ResetContent
    
    CurTab.ShowVertScroller False
    CurTab.ShowHorzScroller False
    CurTab.HeaderString = tmpFields
    CurTab.HeaderLengthString = tmpLength
    CurTab.LogicalFieldList = tmpFields
    CurTab.AutoSizeByHeader = True
    CurTab.AutoSizeColumns
    CurTab.Refresh
    Screen.MousePointer = 11
    
    CurTab.CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
    CurTab.CedaHopo = UfisServer.HOPO
    CurTab.CedaIdentifier = ""
    CurTab.CedaPort = "3357"
    CurTab.CedaReceiveTimeout = "250"
    CurTab.CedaRecordSeparator = vbLf
    CurTab.CedaSendTimeout = "250"
    CurTab.CedaServerName = UfisServer.HostName
    CurTab.CedaTabext = UfisServer.TblExt
    CurTab.CedaUser = UfisServer.ModName
    CurTab.CedaWorkstation = UfisServer.GetMyWorkStationName
    
    
    tmpCmd = "RTA"
    tmpTable = TANA & "TAB"
    ' Changed By GFO 1/12/2006
    tmpSqlKey = SqlKey
    
    CdrAnsw = CurTab.CedaAction(tmpCmd, tmpTable, tmpFields, "", tmpSqlKey)
    
    CurTab.ShowVertScroller True
    CurTab.ShowHorzScroller True
    CurTab.CursorLifeStyle = True
    CurTab.AutoSizeByHeader = True
    CurTab.AutoSizeColumns
    CurTab.Refresh
    Screen.MousePointer = 0
End Sub

Private Sub LoadSystabInfo(CurTab As TABLib.TAB, TANA As String)
    Dim CdrAnsw As Boolean
    Dim tmpCmd As String
    Dim tmpTable As String
    Dim tmpFields As String
    Dim tmpSqlKey As String
    Dim tmpHeader As String
    Dim tmpLength As String
    Dim tmpData As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim tmpLen As Integer
    
    tmpCmd = "RTA"
    tmpTable = "SYSTAB"
    tmpFields = "FINA,'10'"
    
    tmpSqlKey = "WHERE TANA='" & TANA & "'"
    
    tmpHeader = "FINA,LEN"
    tmpLength = "10,10"
    
    CurTab.ResetContent
    CurTab.CursorLifeStyle = False
    CurTab.ShowVertScroller False
    CurTab.ShowHorzScroller False
    CurTab.HeaderString = tmpHeader
    CurTab.HeaderLengthString = tmpLength
    CurTab.AutoSizeByHeader = True
    CurTab.AutoSizeColumns
    CurTab.Refresh
    Screen.MousePointer = 11
    
    CurTab.CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
    CurTab.CedaHopo = UfisServer.HOPO
    CurTab.CedaIdentifier = ""
    CurTab.CedaPort = "3357"
    CurTab.CedaReceiveTimeout = "250"
    CurTab.CedaRecordSeparator = vbLf
    CurTab.CedaSendTimeout = "250"
    CurTab.CedaServerName = UfisServer.HostName
    CurTab.CedaTabext = UfisServer.TblExt
    CurTab.CedaUser = UfisServer.ModName
    CurTab.CedaWorkstation = UfisServer.GetMyWorkStationName
    
    CdrAnsw = CurTab.CedaAction(tmpCmd, tmpTable, tmpFields, "", tmpSqlKey)
    
    CurTab.ShowVertScroller True
    CurTab.ShowHorzScroller True
    CurTab.CursorLifeStyle = True
    CurTab.AutoSizeByHeader = True
    CurTab.AutoSizeColumns
    CurTab.Sort "0", True, True
    CurTab.Refresh
    Screen.MousePointer = 0
    

End Sub

Private Sub LoadAbsenceCodes(CurTab As TABLib.TAB, LoadFile As String)
    Dim CdrAnsw As Boolean
    Dim tmpCmd As String
    Dim tmpTable As String
    Dim tmpFields As String
    Dim tmpSqlKey As String
    Dim tmpHeader As String
    Dim tmpLength As String
    Dim tmpData As String
    Dim tmpSdac As String
    Dim tmpUrno As String
    Dim tmpSdacList As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim LineNo As Long
    Dim tmpLen As Integer
    Dim fName As String
    Dim fNo As Integer
    Dim fLine As String
    
    tmpHeader = "SHR,RMS,MEANING,ODAU"
    tmpLength = "10,10,10,10"
    CurTab.ResetContent
    CurTab.CursorLifeStyle = False
    CurTab.ShowVertScroller False
    CurTab.ShowHorzScroller False
    CurTab.HeaderString = tmpHeader
    CurTab.HeaderLengthString = tmpLength
    CurTab.LogicalFieldList = "SHRC,RMSC,MEAN,ODAU"
    CurTab.AutoSizeByHeader = True
    CurTab.AutoSizeColumns
    CurTab.Refresh
    Screen.MousePointer = 11
    fNo = FreeFile
    fName = "c:\ufis\system\SHRAbsenceCodes.txt"
    tmpSdacList = ""
    Open fName For Input As #fNo
        Line Input #fNo, fLine
        While Not EOF(fNo)
            Line Input #fNo, fLine
            tmpData = Replace(fLine, ";", ",", 1, -1, vbBinaryCompare) & ","
            CurTab.InsertTextLine tmpData, False
            tmpSdac = GetItem(tmpData, 2, ",")
            If tmpSdac <> "" Then tmpSdacList = tmpSdacList & ",'" & tmpSdac & "'"
        Wend
    Close #fNo
    If tmpSdacList <> "" Then
        tmpSdacList = Mid(tmpSdacList, 2)
        tmpCmd = "RTA"
        tmpTable = "ODATAB"
        tmpFields = "SDAC,URNO"
        tmpSqlKey = "WHERE SDAC IN (" & tmpSdacList & ")"
        helper.ResetContent
        helper.HeaderString = tmpFields
        helper.HeaderLengthString = "10,10"
        helper.CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
        helper.CedaHopo = UfisServer.HOPO
        helper.CedaIdentifier = ""
        helper.CedaPort = "3357"
        helper.CedaReceiveTimeout = "250"
        helper.CedaRecordSeparator = vbLf
        helper.CedaSendTimeout = "250"
        helper.CedaServerName = UfisServer.HostName
        helper.CedaTabext = UfisServer.TblExt
        helper.CedaUser = UfisServer.ModName
        helper.CedaWorkstation = UfisServer.GetMyWorkStationName
        
        If CedaIsConnected Then
            CdrAnsw = helper.CedaAction(tmpCmd, tmpTable, tmpFields, "", tmpSqlKey)
            helper.AutoSizeByHeader = True
            helper.AutoSizeColumns
            MaxLine = CurTab.GetLineCount - 1
            For CurLine = 0 To MaxLine
                tmpSdac = Trim(CurTab.GetColumnValue(CurLine, 1))
                If tmpSdac <> "" Then
                    tmpSdacList = helper.GetLinesByColumnValue(0, tmpSdac, 0)
                    If tmpSdacList <> "" Then
                        LineNo = Val(tmpSdacList)
                        tmpUrno = helper.GetColumnValue(LineNo, 1)
                        CurTab.SetColumnValue CurLine, 3, tmpUrno
                    End If
                End If
            Next
        ElseIf UfisServer.HostName = "LOCAL" Then
            CurTab.ResetContent
            CurTab.ReadFromFileWithProperties LoadFile
        End If
    End If
    
    CurTab.ShowVertScroller True
    CurTab.ShowHorzScroller True
    CurTab.CursorLifeStyle = True
    CurTab.AutoSizeByHeader = True
    CurTab.AutoSizeColumns
    CurTab.Refresh
    Screen.MousePointer = 0
End Sub

Private Sub Form_Load()
    If CedaIsConnected Then
        LoadBasicData ODATAB, "ODA", "", ""
        LoadBasicData BSDTAB, "BSD", "", ""
        ' Changed By GFO 1/12/2006
        ' Load Functions For Specific Date
        ' LoadBasicData SPFTAB, "SPF", "", ""
        LoadAbsenceCodes TAB1, ""
        
        
    ElseIf UfisServer.HostName = "LOCAL" Then
        LoadAbsenceCodes TAB1, "c:\tmp\RmsImportOdaLookUp.txt"
        BSDTAB.ResetContent
        BSDTAB.ReadFromFileWithProperties "c:\tmp\RmsImportBsdTab.txt"
        BSDTAB.ShowHorzScroller True
        SPFTAB.ResetContent
        SPFTAB.ReadFromFileWithProperties "c:\tmp\RmsImportSpfTab.txt"
        SPFTAB.LogicalFieldList = SPFTAB.HeaderString
        SPFTAB.ShowHorzScroller True
    End If
    ' Added By GFO for the Report needs
        ReadLanguageFile
End Sub

Public Function LookupHrAbsenceCode(LCode As String, GetFields As String) As String
    Dim tmpResult As String
    Dim tmpList As String
    Dim LineNo As Long
    tmpResult = ""
    tmpList = TAB1.GetLinesByColumnValue(0, LCode, 0)
    If tmpList <> "" Then
        LineNo = Val(tmpList)
        tmpResult = TAB1.GetFieldValues(LineNo, GetFields)
    End If
    LookupHrAbsenceCode = tmpResult
End Function

Public Function LookupStaffBasicFunction(LCode As String, GetFields As String) As String
    Dim tmpResult As String
    Dim tmpList As String
    Dim LineNo As Long
    Dim ColNo As Long
    tmpResult = ""
    ColNo = GetRealItemNo(SPFTAB.LogicalFieldList, "SURN")
    tmpList = SPFTAB.GetLinesByColumnValue(ColNo, LCode, 0)
    If tmpList <> "" Then
        LineNo = Val(tmpList)
        tmpResult = SPFTAB.GetFieldValues(LineNo, GetFields)
    End If
    LookupStaffBasicFunction = tmpResult
End Function


' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' reads the language-file and puts it into a TAB.
' the file is built like
' 'ID' <Tabulator> 'Language' <Tabulator> 'String'
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Private Function ReadLanguageFile()
    Dim strLanguageFile As String

    'init the TAB
    TabLanguage.ResetContent
    TabLanguage.ShowHorzScroller True
    TabLanguage.EnableHeaderSizing True
    TabLanguage.HeaderLengthString = "50,50,150"
    TabLanguage.ColumnWidthString = "10,2,20"
    TabLanguage.ColumnAlignmentString = "R,L,L"
    strFieldSeparator = Chr(9)
    TabLanguage.SetFieldSeparator strFieldSeparator
    TabLanguage.HeaderString = "STID" & strFieldSeparator & "COCO" & strFieldSeparator & "STRG"

    'read the strings
    'strLanguageFile = "c:\ufis\system\RosteringPrint.ulf"
    strLanguageFile = GetIniEntry(myIniFile, "UFIS_SYSTEM ", "", "", "") & "\RosteringPrint.ulf"
    If ExistFile(strLanguageFile) = True Then
        TabLanguage.ReadFromFile strLanguageFile
    End If

    'init the language-parameter (default is US => English)
    strLanguage = GetIniEntry("", "ROSTERING", "GLOBAL", "LANGUAGE", "US")
End Function

' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' delivers the string fitting to the ID
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Public Function LoadStg(ByRef sSTID As String) As String
    Dim strRet As String
    Dim strLineNo As String

    strLineNo = TabLanguage.GetLinesByMultipleColumnValue("0,1", sSTID & strFieldSeparator & strLanguage, 0)

    If IsNumeric(strLineNo) = True And strLineNo <> "" Then
        strRet = TabLanguage.GetColumnValue(CLng(strLineNo), 2)
    Else
        strRet = ""
    End If

    LoadStg = strRet
End Function


Public Sub LoadReportData(CurTab As TABLib.TAB, TANA As String, TableName As String, SqlKey As String)
    Dim CdrAnsw As Boolean
    Dim tmpCmd As String
    Dim tmpTable As String
    Dim tmpFields As String
    Dim tmpSqlKey As String
    Dim tmpHeader As String
    Dim tmpLength As String
    Dim tmpData As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim tmpLen As Integer
    
    If (TableName = "L2L3DIFF") Then
    tmpFields = "SORCODE,SPFCODE,CANO,LANM,FINM,SDAY,L2SCOD,L3SCOD,USEU,PENO,LSTU"
    Else
    tmpFields = "SORCODE,SPFCODE,CANO,LANM,FINM,SDAY,AVFR,AVTO,SCOD,ACTI,PENO,ACTO"
    End If
    tmpLength = "6,9,20,20,6,12,12,2"
    
    CurTab.ResetContent
    
    CurTab.ShowVertScroller False
    CurTab.ShowHorzScroller False
    CurTab.HeaderString = tmpFields
    CurTab.HeaderLengthString = tmpLength
    CurTab.LogicalFieldList = tmpFields
    CurTab.AutoSizeByHeader = True
    CurTab.AutoSizeColumns
    CurTab.Refresh
    Screen.MousePointer = 11
    
    CurTab.CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
    CurTab.CedaHopo = UfisServer.HOPO
    CurTab.CedaIdentifier = ""
    CurTab.CedaPort = "3357"
    CurTab.CedaReceiveTimeout = "250"
    CurTab.CedaRecordSeparator = vbLf
    CurTab.CedaSendTimeout = "250"
    CurTab.CedaServerName = UfisServer.HostName
    CurTab.CedaTabext = UfisServer.TblExt
    CurTab.CedaUser = UfisServer.ModName
    CurTab.CedaWorkstation = UfisServer.GetMyWorkStationName
    
    
    tmpCmd = "RTA"
    tmpTable = TableName
    ' Changed By GFO 1/12/2006
    tmpSqlKey = SqlKey
    
    CdrAnsw = CurTab.CedaAction(tmpCmd, tmpTable, tmpFields, "", tmpSqlKey)
    
    CurTab.ShowVertScroller True
    CurTab.ShowHorzScroller True
    CurTab.CursorLifeStyle = True
    CurTab.AutoSizeByHeader = True
    CurTab.AutoSizeColumns
    CurTab.Refresh
    Screen.MousePointer = 0
End Sub

