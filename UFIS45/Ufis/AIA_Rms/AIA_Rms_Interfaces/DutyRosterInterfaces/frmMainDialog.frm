VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmMainDialog 
   AutoRedraw      =   -1  'True
   Caption         =   "RMS Data Import Tool"
   ClientHeight    =   11535
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   18090
   Icon            =   "frmMainDialog.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   11535
   ScaleWidth      =   18090
   Tag             =   "8"
   WindowState     =   2  'Maximized
   Begin VB.Timer Interface_TIMER 
      Left            =   12210
      Top             =   8220
   End
   Begin VB.Frame fraWorkPanel 
      BackColor       =   &H80000010&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Index           =   8
      Left            =   30
      TabIndex        =   401
      Top             =   8730
      Visible         =   0   'False
      Width           =   12030
      Begin VB.Frame fraTopSplit 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   150
         Index           =   8
         Left            =   10890
         MousePointer    =   9  'Size W E
         TabIndex        =   402
         Top             =   0
         Visible         =   0   'False
         Width           =   1110
      End
      Begin VB.Frame fraSplitter 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   675
         Index           =   8
         Left            =   2010
         MousePointer    =   9  'Size W E
         TabIndex        =   422
         Top             =   1020
         Width           =   60
      End
      Begin VB.Frame fraRightData 
         Caption         =   "RMS Server DRRTAB Data"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Index           =   8
         Left            =   2100
         TabIndex        =   420
         Top             =   1020
         Visible         =   0   'False
         Width           =   2235
         Begin TABLib.TAB TAB2 
            Height          =   435
            Index           =   8
            Left            =   90
            TabIndex        =   421
            Top             =   210
            Width           =   2025
            _Version        =   65536
            _ExtentX        =   3572
            _ExtentY        =   767
            _StockProps     =   64
         End
      End
      Begin VB.Frame fraLeftData 
         Caption         =   "SHR Absence Data"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Index           =   8
         Left            =   0
         TabIndex        =   418
         Top             =   1020
         Visible         =   0   'False
         Width           =   1995
         Begin TABLib.TAB TAB1 
            Height          =   435
            Index           =   8
            Left            =   60
            TabIndex        =   419
            Top             =   210
            Width           =   1815
            _Version        =   65536
            _ExtentX        =   3201
            _ExtentY        =   767
            _StockProps     =   64
         End
      End
      Begin VB.Frame fraDialog 
         Caption         =   "Report Data"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   8
         Left            =   0
         TabIndex        =   405
         Top             =   0
         Width           =   11655
         Begin VB.CheckBox chkTask 
            Caption         =   "Report Employees with Different L2,L3 Shift"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   600
            Index           =   47
            Left            =   7920
            Style           =   1  'Graphical
            TabIndex        =   429
            Tag             =   "RPT.L2L3DIFF"
            ToolTipText     =   "Report Employees with Different L2,L3 Shift"
            Top             =   270
            Width           =   1785
         End
         Begin VB.CheckBox chkTask 
            Caption         =   "Report No Click (Night ,Holiday)"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   600
            Index           =   46
            Left            =   6150
            Style           =   1  'Graphical
            TabIndex        =   428
            Tag             =   "RPT.NOHIT_N"
            ToolTipText     =   "Report Employees With No CLOCK HIT  and are Night or Holiday"
            Top             =   270
            Width           =   1785
         End
         Begin VB.TextBox txtHourDiff 
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1032
               SubFormatType   =   1
            EndProperty
            Height          =   315
            Left            =   2850
            TabIndex        =   424
            Text            =   "2"
            Top             =   600
            Width           =   495
         End
         Begin VB.CheckBox chkTask 
            Caption         =   "Shift Change"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   55
            Left            =   4380
            Style           =   1  'Graphical
            TabIndex        =   417
            Tag             =   "RPT.ONE_CLICK"
            ToolTipText     =   "Report Emp with One Click "
            Top             =   270
            Width           =   1785
         End
         Begin VB.CheckBox chkTask 
            Caption         =   "Report No Click"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   54
            Left            =   4380
            Style           =   1  'Graphical
            TabIndex        =   416
            Tag             =   "RPT.NOHIT"
            ToolTipText     =   "Report Employees with no clock Hit"
            Top             =   570
            Width           =   1785
         End
         Begin VB.CheckBox chkPanelIcon 
            Height          =   615
            Index           =   8
            Left            =   90
            Picture         =   "frmMainDialog.frx":08CA
            Style           =   1  'Graphical
            TabIndex        =   415
            Top             =   240
            Width           =   615
         End
         Begin VB.CheckBox chkTask 
            Caption         =   "To"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   51
            Left            =   720
            Style           =   1  'Graphical
            TabIndex        =   414
            Tag             =   "VPTO"
            Top             =   570
            Width           =   615
         End
         Begin VB.CheckBox chkTask 
            Caption         =   "From"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   50
            Left            =   720
            Style           =   1  'Graphical
            TabIndex        =   413
            Tag             =   "VPFR"
            Top             =   270
            Width           =   615
         End
         Begin VB.TextBox txtVpfrYY 
            Alignment       =   2  'Center
            BackColor       =   &H0000FFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   8
            Left            =   1350
            MaxLength       =   4
            TabIndex        =   412
            Top             =   270
            Width           =   555
         End
         Begin VB.TextBox txtVpfrMM 
            Alignment       =   2  'Center
            BackColor       =   &H0000FFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   8
            Left            =   1920
            MaxLength       =   2
            TabIndex        =   411
            Top             =   270
            Width           =   345
         End
         Begin VB.TextBox txtVpfrDD 
            Alignment       =   2  'Center
            BackColor       =   &H0000FFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   8
            Left            =   2280
            MaxLength       =   2
            TabIndex        =   410
            Top             =   270
            Width           =   345
         End
         Begin VB.TextBox txtVptoYY 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   8
            Left            =   1350
            MaxLength       =   4
            TabIndex        =   409
            Top             =   585
            Width           =   555
         End
         Begin VB.TextBox txtVptoMM 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   8
            Left            =   1920
            MaxLength       =   2
            TabIndex        =   408
            Top             =   585
            Width           =   345
         End
         Begin VB.TextBox txtVptoDD 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   8
            Left            =   2280
            MaxLength       =   2
            TabIndex        =   407
            Top             =   585
            Width           =   345
         End
         Begin VB.CheckBox chkTask 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Index           =   45
            Left            =   3720
            Picture         =   "frmMainDialog.frx":0BD4
            Style           =   1  'Graphical
            TabIndex        =   406
            Tag             =   "ABS.ORGA"
            ToolTipText     =   "Open staff filter"
            Top             =   270
            Width           =   615
         End
         Begin VB.Label Label1 
            Caption         =   "HourDiff"
            Height          =   285
            Left            =   2760
            TabIndex        =   423
            Top             =   270
            Width           =   945
         End
         Begin VB.Image imgPointer 
            Height          =   240
            Index           =   8
            Left            =   3900
            Picture         =   "frmMainDialog.frx":149E
            ToolTipText     =   "Step 1"
            Top             =   60
            Width           =   240
         End
      End
      Begin VB.Frame fraTopData 
         Caption         =   "RMS Staff Data"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Index           =   8
         Left            =   4440
         TabIndex        =   403
         Top             =   1020
         Visible         =   0   'False
         Width           =   2235
         Begin TABLib.TAB TAB3 
            Height          =   435
            Index           =   8
            Left            =   90
            TabIndex        =   404
            Tag             =   "SHOW"
            Top             =   210
            Visible         =   0   'False
            Width           =   2025
            _Version        =   65536
            _ExtentX        =   3572
            _ExtentY        =   767
            _StockProps     =   64
         End
      End
   End
   Begin VB.Frame fraButtonPanel 
      BorderStyle     =   0  'None
      Height          =   735
      Index           =   0
      Left            =   0
      TabIndex        =   12
      Top             =   0
      Width           =   17025
      Begin VB.CheckBox Ckb_Interface 
         Caption         =   "Enable Interface"
         Height          =   255
         Left            =   15240
         TabIndex        =   430
         Top             =   360
         Width           =   1575
      End
      Begin VB.CheckBox chkMain 
         Caption         =   "Setup"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   13
         Left            =   10860
         Style           =   1  'Graphical
         TabIndex        =   399
         Tag             =   " ,,,EXIT"
         Top             =   375
         Width           =   1065
      End
      Begin VB.PictureBox picBall 
         BackColor       =   &H0000FFFF&
         BorderStyle     =   0  'None
         Height          =   90
         Index           =   12
         Left            =   2250
         Picture         =   "frmMainDialog.frx":1A28
         ScaleHeight     =   90
         ScaleWidth      =   90
         TabIndex        =   398
         Top             =   405
         Visible         =   0   'False
         Width           =   90
      End
      Begin VB.PictureBox picBall 
         BackColor       =   &H0000FFFF&
         BorderStyle     =   0  'None
         Height          =   90
         Index           =   7
         Left            =   8730
         Picture         =   "frmMainDialog.frx":1A7C
         ScaleHeight     =   90
         ScaleWidth      =   90
         TabIndex        =   366
         Top             =   405
         Visible         =   0   'False
         Width           =   90
      End
      Begin VB.PictureBox picBall 
         BackColor       =   &H0000FFFF&
         BorderStyle     =   0  'None
         Height          =   90
         Index           =   6
         Left            =   7650
         Picture         =   "frmMainDialog.frx":1AD0
         ScaleHeight     =   90
         ScaleWidth      =   90
         TabIndex        =   365
         Top             =   405
         Visible         =   0   'False
         Width           =   90
      End
      Begin VB.PictureBox picBall 
         BackColor       =   &H0000FFFF&
         BorderStyle     =   0  'None
         Height          =   90
         Index           =   5
         Left            =   6570
         Picture         =   "frmMainDialog.frx":1B24
         ScaleHeight     =   90
         ScaleWidth      =   90
         TabIndex        =   364
         Top             =   405
         Visible         =   0   'False
         Width           =   90
      End
      Begin VB.PictureBox picBall 
         BackColor       =   &H0000FFFF&
         BorderStyle     =   0  'None
         Height          =   90
         Index           =   4
         Left            =   5490
         Picture         =   "frmMainDialog.frx":1B78
         ScaleHeight     =   90
         ScaleWidth      =   90
         TabIndex        =   363
         Top             =   405
         Visible         =   0   'False
         Width           =   90
      End
      Begin VB.PictureBox picBall 
         BackColor       =   &H0000FFFF&
         BorderStyle     =   0  'None
         Height          =   90
         Index           =   3
         Left            =   4410
         Picture         =   "frmMainDialog.frx":1BCC
         ScaleHeight     =   90
         ScaleWidth      =   90
         TabIndex        =   362
         Top             =   405
         Visible         =   0   'False
         Width           =   90
      End
      Begin VB.PictureBox picBall 
         BackColor       =   &H0000FFFF&
         BorderStyle     =   0  'None
         Height          =   90
         Index           =   2
         Left            =   3330
         Picture         =   "frmMainDialog.frx":1C20
         ScaleHeight     =   90
         ScaleWidth      =   90
         TabIndex        =   361
         Top             =   405
         Visible         =   0   'False
         Width           =   90
      End
      Begin VB.PictureBox picBall 
         BackColor       =   &H0000FFFF&
         BorderStyle     =   0  'None
         Height          =   90
         Index           =   0
         Left            =   1170
         Picture         =   "frmMainDialog.frx":1C74
         ScaleHeight     =   90
         ScaleWidth      =   90
         TabIndex        =   360
         Top             =   405
         Visible         =   0   'False
         Width           =   90
      End
      Begin VB.PictureBox picBall 
         BackColor       =   &H0000FFFF&
         BorderStyle     =   0  'None
         Height          =   90
         Index           =   1
         Left            =   90
         Picture         =   "frmMainDialog.frx":1CC8
         ScaleHeight     =   90
         ScaleWidth      =   90
         TabIndex        =   359
         Top             =   405
         Visible         =   0   'False
         Width           =   90
      End
      Begin VB.Timer MainTimer 
         Interval        =   1000
         Left            =   0
         Top             =   -120
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Show Data"
         Height          =   285
         Left            =   11130
         TabIndex        =   329
         Top             =   30
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Save to File"
         Height          =   285
         Left            =   12570
         TabIndex        =   328
         Top             =   30
         Visible         =   0   'False
         Width           =   1245
      End
      Begin VB.CheckBox chkMain 
         Caption         =   "Staff Filter"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   12
         Left            =   2220
         Style           =   1  'Graphical
         TabIndex        =   219
         Tag             =   ",,,ORGA"
         Top             =   375
         Width           =   1065
      End
      Begin VB.CheckBox chkMain 
         Caption         =   "Scenario"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   11
         Left            =   9780
         Style           =   1  'Graphical
         TabIndex        =   7
         Tag             =   "2,-1,,SCENARIO"
         Top             =   375
         Width           =   1065
      End
      Begin VB.CheckBox chkMain 
         Caption         =   "About"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   10
         Left            =   11940
         Style           =   1  'Graphical
         TabIndex        =   9
         Tag             =   " ,,,ABOUT"
         Top             =   375
         Width           =   1065
      End
      Begin VB.CheckBox chkMain 
         Caption         =   "Help"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   9
         Left            =   13020
         Style           =   1  'Graphical
         TabIndex        =   10
         Tag             =   " ,,,HELP"
         Top             =   375
         Width           =   1065
      End
      Begin VB.CheckBox chkMain 
         Caption         =   "Exit"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   8
         Left            =   14100
         Style           =   1  'Graphical
         TabIndex        =   11
         Tag             =   " ,,,EXIT"
         Top             =   375
         Width           =   1065
      End
      Begin VB.CheckBox chkMain 
         Caption         =   "Tool"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   7
         Left            =   8700
         Style           =   1  'Graphical
         TabIndex        =   13
         Tag             =   "7,-1,,TOOLS"
         Top             =   375
         Width           =   1065
      End
      Begin VB.CheckBox chkMain 
         Caption         =   "Import"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   2
         Left            =   3300
         Style           =   1  'Graphical
         TabIndex        =   3
         Tag             =   "1,-1,,IMPORT"
         Top             =   375
         Width           =   1065
      End
      Begin VB.CheckBox chkMain 
         Caption         =   "Statistic"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   6
         Left            =   7620
         Style           =   1  'Graphical
         TabIndex        =   8
         Tag             =   "8,-1,,STATISTIC"
         Top             =   375
         Width           =   1065
      End
      Begin VB.CheckBox chkMain 
         Caption         =   "Report"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   5
         Left            =   6540
         Style           =   1  'Graphical
         TabIndex        =   6
         Tag             =   "6,-1,,REPORTS"
         Top             =   375
         Width           =   1065
      End
      Begin VB.CheckBox chkMain 
         Caption         =   "Module"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   4
         Left            =   5460
         Style           =   1  'Graphical
         TabIndex        =   5
         Tag             =   "5,-1,,MODULES"
         Top             =   375
         Width           =   1065
      End
      Begin VB.CheckBox chkMain 
         Caption         =   "Interface"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   3
         Left            =   4380
         Style           =   1  'Graphical
         TabIndex        =   4
         Tag             =   "4,-1,,INTERFACE"
         Top             =   375
         Width           =   1065
      End
      Begin VB.CheckBox chkMain 
         Caption         =   "Restore"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   1
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   2
         Tag             =   " ,,,RESTORE"
         Top             =   375
         Width           =   1065
      End
      Begin VB.CheckBox chkMain 
         Caption         =   "Backup"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   0
         Left            =   1140
         Style           =   1  'Graphical
         TabIndex        =   1
         Tag             =   " ,,,BACKUP"
         Top             =   375
         Width           =   1065
      End
      Begin VB.Label lblTime 
         Alignment       =   2  'Center
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   2
         Left            =   4935
         TabIndex        =   349
         ToolTipText     =   "Server Reference Time"
         Top             =   60
         Width           =   2655
      End
      Begin VB.Label lblTime 
         Alignment       =   2  'Center
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   1
         Left            =   2505
         TabIndex        =   348
         ToolTipText     =   "Server Reference Time (UTC)"
         Top             =   60
         Width           =   2175
      End
      Begin VB.Label lblTime 
         Alignment       =   2  'Center
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Index           =   0
         Left            =   75
         TabIndex        =   347
         ToolTipText     =   "Local Time (This Workstation)"
         Top             =   60
         Width           =   2325
      End
      Begin VB.Label lblTime 
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Label3"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Index           =   3
         Left            =   7860
         TabIndex        =   346
         Top             =   60
         Width           =   645
      End
   End
   Begin VB.FileListBox MyFileList 
      Height          =   1065
      Index           =   0
      Left            =   12180
      TabIndex        =   344
      Top             =   6750
      Visible         =   0   'False
      Width           =   1965
   End
   Begin VB.Frame fraWorkArea 
      Caption         =   "Integrated Quick Help"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   525
      Index           =   1
      Left            =   3030
      TabIndex        =   311
      Top             =   780
      Visible         =   0   'False
      Width           =   6825
      Begin VB.Frame fraHelp 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   8625
         Index           =   7
         Left            =   5580
         TabIndex        =   326
         Top             =   270
         Width           =   705
         Begin VB.Label lblHelp 
            AutoSize        =   -1  'True
            Caption         =   "Help 7"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   7
            Left            =   0
            TabIndex        =   327
            Top             =   0
            Width           =   495
         End
      End
      Begin VB.Frame fraHelp 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   8625
         Index           =   6
         Left            =   4800
         TabIndex        =   324
         Top             =   270
         Width           =   705
         Begin VB.Label lblHelp 
            AutoSize        =   -1  'True
            Caption         =   "Help 6"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   6
            Left            =   0
            TabIndex        =   325
            Top             =   0
            Width           =   495
         End
      End
      Begin VB.Frame fraHelp 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   8625
         Index           =   5
         Left            =   4020
         TabIndex        =   322
         Top             =   270
         Width           =   705
         Begin VB.Label lblHelp 
            AutoSize        =   -1  'True
            Caption         =   "Help 5"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   5
            Left            =   0
            TabIndex        =   323
            Top             =   0
            Width           =   495
         End
      End
      Begin VB.Frame fraHelp 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   8625
         Index           =   4
         Left            =   3240
         TabIndex        =   320
         Top             =   270
         Width           =   705
         Begin VB.Label lblHelp 
            AutoSize        =   -1  'True
            Caption         =   "Help 4"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   4
            Left            =   0
            TabIndex        =   321
            Top             =   0
            Width           =   495
         End
      End
      Begin VB.Frame fraHelp 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   8625
         Index           =   3
         Left            =   2460
         TabIndex        =   318
         Top             =   270
         Width           =   705
         Begin VB.Label lblHelp 
            AutoSize        =   -1  'True
            Caption         =   "Help 3"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   3
            Left            =   0
            TabIndex        =   319
            Top             =   0
            Width           =   495
         End
      End
      Begin VB.Frame fraHelp 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   8625
         Index           =   2
         Left            =   1680
         TabIndex        =   316
         Top             =   270
         Width           =   705
         Begin VB.Label lblHelp 
            AutoSize        =   -1  'True
            Caption         =   "Help 2"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   2
            Left            =   0
            TabIndex        =   317
            Top             =   0
            Width           =   495
         End
      End
      Begin VB.Frame fraHelp 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   8625
         Index           =   1
         Left            =   900
         TabIndex        =   314
         Top             =   270
         Width           =   705
         Begin VB.Label lblHelp 
            AutoSize        =   -1  'True
            Caption         =   "Help 1"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   1
            Left            =   0
            TabIndex        =   315
            Top             =   0
            Width           =   495
         End
      End
      Begin VB.Frame fraHelp 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   8625
         Index           =   0
         Left            =   120
         TabIndex        =   312
         Top             =   270
         Width           =   705
         Begin VB.Label lblHelp 
            AutoSize        =   -1  'True
            Caption         =   "Help 0"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   0
            Left            =   0
            TabIndex        =   313
            Top             =   0
            Width           =   495
         End
      End
   End
   Begin VB.Frame fraWorkArea 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Index           =   0
      Left            =   60
      TabIndex        =   14
      Top             =   780
      Width           =   3075
      Begin TABLib.TAB tabSlides 
         Height          =   435
         Left            =   2100
         TabIndex        =   387
         Top             =   240
         Visible         =   0   'False
         Width           =   915
         _Version        =   65536
         _ExtentX        =   1614
         _ExtentY        =   767
         _StockProps     =   64
      End
      Begin VB.PictureBox UfisIntroFrame 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         ForeColor       =   &H80000008&
         Height          =   585
         Index           =   2
         Left            =   660
         ScaleHeight     =   555
         ScaleWidth      =   1290
         TabIndex        =   382
         TabStop         =   0   'False
         Top             =   480
         Visible         =   0   'False
         Width           =   1320
         Begin VB.Image UfisIntroPicture 
            Height          =   285
            Index           =   2
            Left            =   0
            Top             =   0
            Width           =   915
         End
      End
      Begin VB.PictureBox UfisIntroFrame 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         ForeColor       =   &H80000008&
         Height          =   585
         Index           =   1
         Left            =   240
         ScaleHeight     =   555
         ScaleWidth      =   1290
         TabIndex        =   381
         TabStop         =   0   'False
         Top             =   300
         Width           =   1320
         Begin VB.Image UfisIntroPicture 
            Height          =   285
            Index           =   1
            Left            =   0
            Top             =   0
            Width           =   915
         End
      End
      Begin VB.Timer UfisIntroTimer 
         Enabled         =   0   'False
         Index           =   2
         Interval        =   1000
         Left            =   2490
         Top             =   -270
      End
      Begin VB.Timer UfisIntroTimer 
         Enabled         =   0   'False
         Index           =   1
         Interval        =   100
         Left            =   2040
         Top             =   -270
      End
      Begin VB.Timer UfisIntroTimer 
         Index           =   0
         Left            =   1590
         Top             =   -270
      End
      Begin VB.PictureBox UfisIntroFrame 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         ForeColor       =   &H80000008&
         Height          =   585
         Index           =   0
         Left            =   45
         ScaleHeight     =   555
         ScaleWidth      =   1290
         TabIndex        =   343
         TabStop         =   0   'False
         Top             =   150
         Visible         =   0   'False
         Width           =   1320
         Begin VB.Image UfisIntroPicture 
            Height          =   285
            Index           =   0
            Left            =   0
            Top             =   0
            Width           =   915
         End
      End
      Begin VB.Label lblTitle 
         AutoSize        =   -1  'True
         Caption         =   "Main Scenario"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   2
         Left            =   510
         TabIndex        =   397
         Top             =   2100
         Visible         =   0   'False
         Width           =   1230
      End
      Begin VB.Label lblTitle 
         AutoSize        =   -1  'True
         Caption         =   "Last Step"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   510
         TabIndex        =   396
         Top             =   1860
         Visible         =   0   'False
         Width           =   825
      End
      Begin VB.Label lblTitle 
         AutoSize        =   -1  'True
         Caption         =   "Current Step"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   510
         TabIndex        =   395
         Top             =   1620
         Visible         =   0   'False
         Width           =   1080
      End
      Begin VB.Label lblLoading 
         Caption         =   "Loading Basic Data ..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   510
         TabIndex        =   357
         Top             =   1290
         Visible         =   0   'False
         Width           =   2115
      End
      Begin VB.Label UfisIntroLabel 
         AutoSize        =   -1  'True
         Caption         =   "120"
         Height          =   195
         Left            =   60
         TabIndex        =   345
         Top             =   150
         Visible         =   0   'False
         Width           =   270
      End
   End
   Begin VB.Frame fraWorkPanel 
      BackColor       =   &H80000010&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Index           =   0
      Left            =   8130
      TabIndex        =   174
      Top             =   780
      Visible         =   0   'False
      Width           =   3945
      Begin VB.Frame fraTopSplit 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   120
         Index           =   0
         Left            =   2970
         MousePointer    =   9  'Size W E
         TabIndex        =   303
         Top             =   0
         Width           =   1200
      End
      Begin VB.Frame fraTopData 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Index           =   0
         Left            =   4380
         TabIndex        =   287
         Top             =   600
         Width           =   2235
         Begin TABLib.TAB TAB3 
            Height          =   435
            Index           =   0
            Left            =   90
            TabIndex        =   288
            Top             =   270
            Visible         =   0   'False
            Width           =   2025
            _Version        =   65536
            _ExtentX        =   3572
            _ExtentY        =   767
            _StockProps     =   64
         End
      End
      Begin VB.Frame fraDialog 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   585
         Index           =   0
         Left            =   0
         TabIndex        =   180
         Top             =   0
         Width           =   3825
      End
      Begin VB.Frame fraLeftData 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Index           =   0
         Left            =   0
         TabIndex        =   178
         Top             =   600
         Width           =   1995
         Begin TABLib.TAB TAB1 
            Height          =   435
            Index           =   0
            Left            =   60
            TabIndex        =   179
            Top             =   270
            Visible         =   0   'False
            Width           =   1815
            _Version        =   65536
            _ExtentX        =   3201
            _ExtentY        =   767
            _StockProps     =   64
         End
      End
      Begin VB.Frame fraRightData 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Index           =   0
         Left            =   2070
         TabIndex        =   176
         Top             =   600
         Width           =   2235
         Begin TABLib.TAB TAB2 
            Height          =   435
            Index           =   0
            Left            =   90
            TabIndex        =   177
            Top             =   270
            Visible         =   0   'False
            Width           =   2025
            _Version        =   65536
            _ExtentX        =   3572
            _ExtentY        =   767
            _StockProps     =   64
         End
      End
      Begin VB.Frame fraSplitter 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   675
         Index           =   0
         Left            =   2010
         MousePointer    =   9  'Size W E
         TabIndex        =   175
         Top             =   720
         Width           =   60
      End
   End
   Begin VB.PictureBox pbBottomPanel 
      Align           =   2  'Align Bottom
      BorderStyle     =   0  'None
      Height          =   915
      Index           =   0
      Left            =   0
      ScaleHeight     =   915
      ScaleWidth      =   18090
      TabIndex        =   243
      Top             =   10320
      Width           =   18090
      Begin VB.Frame fraBottomLabels 
         Height          =   915
         Index           =   1
         Left            =   9270
         TabIndex        =   260
         Top             =   -30
         Width           =   6015
         Begin TABLib.TAB tabValues 
            Height          =   645
            Index           =   1
            Left            =   5310
            TabIndex        =   351
            Top             =   180
            Visible         =   0   'False
            Width           =   615
            _Version        =   65536
            _ExtentX        =   1085
            _ExtentY        =   1138
            _StockProps     =   64
         End
         Begin VB.CheckBox chkRightImp 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   6
            Left            =   4620
            Style           =   1  'Graphical
            TabIndex        =   284
            Top             =   525
            Width           =   645
         End
         Begin VB.CheckBox chkRightImp 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   5
            Left            =   3960
            Style           =   1  'Graphical
            TabIndex        =   283
            Top             =   525
            Width           =   645
         End
         Begin VB.CheckBox chkRightImp 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   4
            Left            =   3300
            Style           =   1  'Graphical
            TabIndex        =   282
            Top             =   525
            Width           =   645
         End
         Begin VB.CheckBox chkRightImp 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   3
            Left            =   2640
            Style           =   1  'Graphical
            TabIndex        =   281
            Top             =   525
            Width           =   645
         End
         Begin VB.CheckBox chkRightImp 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   2
            Left            =   1980
            Style           =   1  'Graphical
            TabIndex        =   280
            Top             =   525
            Width           =   645
         End
         Begin VB.CheckBox chkRightImp 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   1
            Left            =   1320
            Style           =   1  'Graphical
            TabIndex        =   279
            Top             =   525
            Width           =   645
         End
         Begin VB.CheckBox chkSearch 
            Caption         =   "Find"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   3
            Left            =   705
            Style           =   1  'Graphical
            TabIndex        =   263
            Top             =   525
            Width           =   600
         End
         Begin VB.CheckBox chkSearch 
            Caption         =   "Look"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   1
            Left            =   90
            Style           =   1  'Graphical
            TabIndex        =   262
            Top             =   525
            Width           =   600
         End
         Begin VB.TextBox txtSearch 
            Alignment       =   2  'Center
            BackColor       =   &H8000000F&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   1
            Left            =   90
            MaxLength       =   9
            TabIndex        =   261
            Top             =   195
            Width           =   1215
         End
         Begin VB.CheckBox chkRightImp 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   0
            Left            =   1320
            Style           =   1  'Graphical
            TabIndex        =   278
            Top             =   510
            Visible         =   0   'False
            Width           =   645
         End
         Begin VB.Image imgColorPointer 
            Height          =   240
            Index           =   1
            Left            =   0
            Picture         =   "frmMainDialog.frx":1D1C
            Top             =   0
            Visible         =   0   'False
            Width           =   240
         End
         Begin VB.Label SrvFkeyCnt 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0FF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   1
            Left            =   1320
            TabIndex        =   270
            Tag             =   "Warn"
            Top             =   195
            Width           =   645
         End
         Begin VB.Label SrvFkeyCnt 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FF00&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   2
            Left            =   1980
            TabIndex        =   268
            Tag             =   "INS"
            Top             =   195
            Width           =   645
         End
         Begin VB.Label SrvFkeyCnt 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00E0E0E0&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   300
            Index           =   6
            Left            =   4620
            TabIndex        =   267
            Tag             =   "Equal"
            Top             =   195
            Width           =   645
         End
         Begin VB.Label SrvFkeyCnt 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   300
            Index           =   5
            Left            =   3960
            TabIndex        =   266
            Tag             =   "REM"
            Top             =   195
            Width           =   645
         End
         Begin VB.Label SrvFkeyCnt 
            Alignment       =   1  'Right Justify
            BackColor       =   &H000000FF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   300
            Index           =   4
            Left            =   3300
            TabIndex        =   265
            Tag             =   "DEL"
            Top             =   195
            Width           =   645
         End
         Begin VB.Label SrvFkeyCnt 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   300
            Index           =   3
            Left            =   2640
            TabIndex        =   264
            Tag             =   "UPD"
            Top             =   195
            Width           =   645
         End
         Begin VB.Label SrvFkeyCnt 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   0
            Left            =   1320
            TabIndex        =   269
            Tag             =   "NONE"
            ToolTipText     =   "Loaded from AODB"
            Top             =   195
            Visible         =   0   'False
            Width           =   645
         End
      End
      Begin VB.Frame fraBottomLabels 
         Height          =   915
         Index           =   2
         Left            =   6150
         TabIndex        =   255
         Top             =   -30
         Width           =   3045
         Begin VB.Timer timSrvLed 
            Interval        =   50000
            Left            =   0
            Top             =   30
         End
         Begin VB.Frame fraMidImp 
            BorderStyle     =   0  'None
            Height          =   300
            Index           =   0
            Left            =   90
            TabIndex        =   352
            Top             =   525
            Visible         =   0   'False
            Width           =   2865
            Begin VB.CheckBox chkMidImp 
               BackColor       =   &H00FFFFFF&
               Caption         =   "0"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   300
               Index           =   3
               Left            =   2160
               Style           =   1  'Graphical
               TabIndex        =   356
               Top             =   0
               Width           =   705
            End
            Begin VB.CheckBox chkMidImp 
               BackColor       =   &H0000FF00&
               Caption         =   "0"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   300
               Index           =   2
               Left            =   1440
               Style           =   1  'Graphical
               TabIndex        =   355
               Top             =   0
               Width           =   705
            End
            Begin VB.CheckBox chkMidImp 
               BackColor       =   &H0000FFFF&
               Caption         =   "0"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   300
               Index           =   1
               Left            =   720
               Style           =   1  'Graphical
               TabIndex        =   354
               Top             =   0
               Width           =   705
            End
            Begin VB.CheckBox chkMidImp 
               BackColor       =   &H000000FF&
               Caption         =   "0"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00FFFFFF&
               Height          =   300
               Index           =   0
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   353
               Top             =   0
               Width           =   705
            End
         End
         Begin VB.Shape shpSrvLed 
            FillColor       =   &H0000FF00&
            FillStyle       =   0  'Solid
            Height          =   135
            Index           =   2
            Left            =   1440
            Shape           =   3  'Circle
            Top             =   285
            Width           =   165
         End
         Begin VB.Shape shpSrvLed 
            FillColor       =   &H8000000F&
            FillStyle       =   0  'Solid
            Height          =   135
            Index           =   1
            Left            =   1605
            Shape           =   3  'Circle
            Top             =   285
            Width           =   165
         End
         Begin VB.Shape shpSrvLed 
            FillColor       =   &H8000000F&
            FillStyle       =   0  'Solid
            Height          =   135
            Index           =   0
            Left            =   1275
            Shape           =   3  'Circle
            Top             =   285
            Width           =   165
         End
         Begin VB.Shape Shape1 
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00000000&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H0000FFFF&
            FillStyle       =   0  'Solid
            Height          =   255
            Left            =   195
            Top             =   225
            Visible         =   0   'False
            Width           =   630
         End
         Begin VB.Label lblCurPos 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   3
            Left            =   90
            TabIndex        =   259
            Top             =   525
            Width           =   2865
         End
         Begin VB.Label lblCurPos 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   1
            Left            =   1890
            TabIndex        =   258
            Top             =   195
            Width           =   1065
         End
         Begin VB.Label lblCurPos 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   2
            Left            =   1185
            TabIndex        =   257
            Top             =   195
            Width           =   675
         End
         Begin VB.Label lblCurPos 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   0
            Left            =   90
            TabIndex        =   256
            Top             =   195
            Width           =   1065
         End
      End
      Begin VB.Frame fraBottomLabels 
         Height          =   915
         Index           =   0
         Left            =   15
         TabIndex        =   244
         Top             =   -30
         Width           =   6015
         Begin TABLib.TAB tabValues 
            Height          =   645
            Index           =   0
            Left            =   5310
            TabIndex        =   350
            Top             =   180
            Visible         =   0   'False
            Width           =   615
            _Version        =   65536
            _ExtentX        =   1085
            _ExtentY        =   1138
            _StockProps     =   64
         End
         Begin VB.CheckBox chkLeftImp 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   6
            Left            =   4620
            Style           =   1  'Graphical
            TabIndex        =   277
            Top             =   525
            Width           =   645
         End
         Begin VB.CheckBox chkLeftImp 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   5
            Left            =   3960
            Style           =   1  'Graphical
            TabIndex        =   276
            Top             =   525
            Width           =   645
         End
         Begin VB.CheckBox chkLeftImp 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   4
            Left            =   3300
            Style           =   1  'Graphical
            TabIndex        =   275
            Top             =   525
            Width           =   645
         End
         Begin VB.CheckBox chkLeftImp 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   3
            Left            =   2640
            Style           =   1  'Graphical
            TabIndex        =   274
            Top             =   525
            Width           =   645
         End
         Begin VB.CheckBox chkLeftImp 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   2
            Left            =   1980
            Style           =   1  'Graphical
            TabIndex        =   273
            Top             =   525
            Width           =   645
         End
         Begin VB.CheckBox chkLeftImp 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   1
            Left            =   1320
            Style           =   1  'Graphical
            TabIndex        =   272
            Top             =   525
            Width           =   645
         End
         Begin VB.CheckBox chkSearch 
            Caption         =   "Find"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   2
            Left            =   705
            Style           =   1  'Graphical
            TabIndex        =   247
            Top             =   525
            Width           =   600
         End
         Begin VB.TextBox txtSearch 
            Alignment       =   2  'Center
            BackColor       =   &H8000000F&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   0
            Left            =   90
            MaxLength       =   9
            TabIndex        =   246
            Top             =   195
            Width           =   1215
         End
         Begin VB.CheckBox chkSearch 
            Caption         =   "Look"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   0
            Left            =   90
            Style           =   1  'Graphical
            TabIndex        =   245
            Top             =   525
            Width           =   600
         End
         Begin VB.CheckBox chkLeftImp 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   0
            Left            =   1320
            Style           =   1  'Graphical
            TabIndex        =   271
            Top             =   510
            Visible         =   0   'False
            Width           =   645
         End
         Begin VB.Image imgColorPointer 
            Height          =   240
            Index           =   0
            Left            =   0
            Picture         =   "frmMainDialog.frx":22A6
            Top             =   0
            Visible         =   0   'False
            Width           =   240
         End
         Begin VB.Label CliFkeyCnt 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0FF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   1
            Left            =   1320
            TabIndex        =   254
            Tag             =   "Warn"
            Top             =   195
            Width           =   645
         End
         Begin VB.Label CliFkeyCnt 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FF00&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   2
            Left            =   1980
            TabIndex        =   253
            Tag             =   "New"
            Top             =   195
            Width           =   645
         End
         Begin VB.Label CliFkeyCnt 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00E0E0E0&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   300
            Index           =   6
            Left            =   4620
            TabIndex        =   252
            Tag             =   "Ignore"
            Top             =   195
            Width           =   645
         End
         Begin VB.Label CliFkeyCnt 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   300
            Index           =   5
            Left            =   3960
            TabIndex        =   251
            Tag             =   "Double"
            Top             =   195
            Width           =   645
         End
         Begin VB.Label CliFkeyCnt 
            Alignment       =   1  'Right Justify
            BackColor       =   &H000000FF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   300
            Index           =   4
            Left            =   3300
            TabIndex        =   250
            Tag             =   "Error"
            Top             =   195
            Width           =   645
         End
         Begin VB.Label CliFkeyCnt 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   300
            Index           =   3
            Left            =   2640
            TabIndex        =   249
            Tag             =   "OK"
            Top             =   195
            Width           =   645
         End
         Begin VB.Label CliFkeyCnt 
            Alignment       =   1  'Right Justify
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   0
            Left            =   1320
            TabIndex        =   248
            Tag             =   "NONE"
            ToolTipText     =   "Loaded from File"
            Top             =   195
            Visible         =   0   'False
            Width           =   645
         End
      End
   End
   Begin VB.Frame fraTopPanel 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3975
      Index           =   0
      Left            =   12150
      TabIndex        =   15
      Top             =   780
      Width           =   14385
      Begin VB.Frame fraFuncPanel 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   300
         Index           =   0
         Left            =   0
         TabIndex        =   173
         Top             =   0
         Visible         =   0   'False
         Width           =   675
         Begin VB.CheckBox chkWork 
            Caption         =   "?"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   192
            Tag             =   "0"
            Top             =   0
            Width           =   1050
         End
      End
      Begin VB.Frame fraFuncPanel 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   300
         Index           =   11
         Left            =   90
         TabIndex        =   26
         Top             =   3540
         Width           =   11685
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   110
            Left            =   9720
            Style           =   1  'Graphical
            TabIndex        =   191
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   108
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   129
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   107
            Left            =   1080
            Style           =   1  'Graphical
            TabIndex        =   128
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   106
            Left            =   2160
            Style           =   1  'Graphical
            TabIndex        =   127
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   105
            Left            =   3240
            Style           =   1  'Graphical
            TabIndex        =   126
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   104
            Left            =   4320
            Style           =   1  'Graphical
            TabIndex        =   125
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   103
            Left            =   5400
            Style           =   1  'Graphical
            TabIndex        =   124
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   102
            Left            =   6480
            Style           =   1  'Graphical
            TabIndex        =   123
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   101
            Left            =   7560
            Style           =   1  'Graphical
            TabIndex        =   122
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   100
            Left            =   8640
            Style           =   1  'Graphical
            TabIndex        =   121
            Top             =   0
            Width           =   1065
         End
      End
      Begin VB.Frame fraFuncPanel 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   300
         Index           =   10
         Left            =   90
         TabIndex        =   25
         Top             =   3210
         Width           =   11685
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   109
            Left            =   9720
            Style           =   1  'Graphical
            TabIndex        =   190
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   98
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   120
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   97
            Left            =   1080
            Style           =   1  'Graphical
            TabIndex        =   119
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   96
            Left            =   2160
            Style           =   1  'Graphical
            TabIndex        =   118
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   95
            Left            =   3240
            Style           =   1  'Graphical
            TabIndex        =   117
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   94
            Left            =   4320
            Style           =   1  'Graphical
            TabIndex        =   116
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   93
            Left            =   5400
            Style           =   1  'Graphical
            TabIndex        =   115
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   92
            Left            =   6480
            Style           =   1  'Graphical
            TabIndex        =   114
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   91
            Left            =   7560
            Style           =   1  'Graphical
            TabIndex        =   113
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   90
            Left            =   8640
            Style           =   1  'Graphical
            TabIndex        =   112
            Top             =   0
            Width           =   1065
         End
      End
      Begin VB.Frame fraFuncPanel 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   300
         Index           =   9
         Left            =   90
         TabIndex        =   24
         Top             =   2880
         Width           =   11685
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   99
            Left            =   9720
            Style           =   1  'Graphical
            TabIndex        =   189
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   88
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   111
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   87
            Left            =   1080
            Style           =   1  'Graphical
            TabIndex        =   110
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   86
            Left            =   2160
            Style           =   1  'Graphical
            TabIndex        =   109
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   85
            Left            =   3240
            Style           =   1  'Graphical
            TabIndex        =   108
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   84
            Left            =   4320
            Style           =   1  'Graphical
            TabIndex        =   107
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   83
            Left            =   5400
            Style           =   1  'Graphical
            TabIndex        =   106
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   82
            Left            =   6480
            Style           =   1  'Graphical
            TabIndex        =   105
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   81
            Left            =   7560
            Style           =   1  'Graphical
            TabIndex        =   104
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   80
            Left            =   8640
            Style           =   1  'Graphical
            TabIndex        =   103
            Top             =   0
            Width           =   1065
         End
      End
      Begin VB.Frame fraFuncPanel 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   300
         Index           =   8
         Left            =   90
         TabIndex        =   23
         Top             =   2550
         Width           =   11685
         Begin VB.CheckBox chkTool 
            BackColor       =   &H0000FFFF&
            Caption         =   "Statistic"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   4
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   358
            Tag             =   "REPTST"
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   89
            Left            =   9720
            Style           =   1  'Graphical
            TabIndex        =   188
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   78
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   102
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   77
            Left            =   1080
            Style           =   1  'Graphical
            TabIndex        =   101
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   76
            Left            =   2160
            Style           =   1  'Graphical
            TabIndex        =   100
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   75
            Left            =   3240
            Style           =   1  'Graphical
            TabIndex        =   99
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   74
            Left            =   4320
            Style           =   1  'Graphical
            TabIndex        =   98
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   73
            Left            =   5400
            Style           =   1  'Graphical
            TabIndex        =   97
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   72
            Left            =   6480
            Style           =   1  'Graphical
            TabIndex        =   96
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   71
            Left            =   7560
            Style           =   1  'Graphical
            TabIndex        =   95
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   70
            Left            =   8640
            Style           =   1  'Graphical
            TabIndex        =   94
            Top             =   0
            Width           =   1065
         End
      End
      Begin VB.Frame fraFuncPanel 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   300
         Index           =   7
         Left            =   90
         TabIndex        =   22
         Top             =   2220
         Width           =   11685
         Begin VB.CheckBox chkTool 
            BackColor       =   &H0000FFFF&
            Caption         =   " Debug"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   3
            Left            =   1080
            Style           =   1  'Graphical
            TabIndex        =   218
            Tag             =   "DEBUG"
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkTool 
            BackColor       =   &H0000FFFF&
            Caption         =   " Check"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   216
            Tag             =   "CHECK"
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   79
            Left            =   9720
            Style           =   1  'Graphical
            TabIndex        =   187
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   68
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   93
            Top             =   0
            Width           =   1050
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   67
            Left            =   1080
            Style           =   1  'Graphical
            TabIndex        =   92
            Top             =   0
            Width           =   1050
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   66
            Left            =   2160
            Style           =   1  'Graphical
            TabIndex        =   91
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   65
            Left            =   3240
            Style           =   1  'Graphical
            TabIndex        =   90
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   64
            Left            =   4320
            Style           =   1  'Graphical
            TabIndex        =   89
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   63
            Left            =   5400
            Style           =   1  'Graphical
            TabIndex        =   88
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   62
            Left            =   6480
            Style           =   1  'Graphical
            TabIndex        =   87
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   61
            Left            =   7560
            Style           =   1  'Graphical
            TabIndex        =   86
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   60
            Left            =   8640
            Style           =   1  'Graphical
            TabIndex        =   85
            Top             =   0
            Width           =   1065
         End
      End
      Begin VB.Frame fraFuncPanel 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   300
         Index           =   6
         Left            =   90
         TabIndex        =   21
         Top             =   1890
         Width           =   11685
         Begin VB.CheckBox chkTool 
            BackColor       =   &H0000FFFF&
            Caption         =   "Report"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   2
            Left            =   1080
            Style           =   1  'Graphical
            TabIndex        =   215
            Tag             =   "REPTST"
            Top             =   0
            Visible         =   0   'False
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   69
            Left            =   9720
            Style           =   1  'Graphical
            TabIndex        =   186
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " Report"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   58
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   84
            Tag             =   "8"
            Top             =   0
            Width           =   1050
         End
         Begin VB.CheckBox chkWork 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   57
            Left            =   1080
            Style           =   1  'Graphical
            TabIndex        =   83
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   56
            Left            =   2160
            Style           =   1  'Graphical
            TabIndex        =   82
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   55
            Left            =   3240
            Style           =   1  'Graphical
            TabIndex        =   81
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   54
            Left            =   4320
            Style           =   1  'Graphical
            TabIndex        =   80
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   53
            Left            =   5400
            Style           =   1  'Graphical
            TabIndex        =   79
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   52
            Left            =   6480
            Style           =   1  'Graphical
            TabIndex        =   78
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   51
            Left            =   7560
            Style           =   1  'Graphical
            TabIndex        =   77
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   50
            Left            =   8640
            Style           =   1  'Graphical
            TabIndex        =   76
            Top             =   0
            Width           =   1065
         End
      End
      Begin VB.Frame fraFuncPanel 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   300
         Index           =   5
         Left            =   90
         TabIndex        =   20
         Top             =   1560
         Width           =   11685
         Begin VB.CheckBox chkTool 
            BackColor       =   &H0000FFFF&
            Caption         =   "Export"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   1
            Left            =   2160
            Style           =   1  'Graphical
            TabIndex        =   217
            Tag             =   "EXPORT"
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   59
            Left            =   9720
            Style           =   1  'Graphical
            TabIndex        =   185
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   "Web Site"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   48
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   75
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   "Test"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   47
            Left            =   1080
            Style           =   1  'Graphical
            TabIndex        =   74
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   46
            Left            =   2160
            Style           =   1  'Graphical
            TabIndex        =   73
            Top             =   0
            Width           =   1050
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   45
            Left            =   3240
            Style           =   1  'Graphical
            TabIndex        =   72
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   44
            Left            =   4320
            Style           =   1  'Graphical
            TabIndex        =   71
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   43
            Left            =   5400
            Style           =   1  'Graphical
            TabIndex        =   70
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   42
            Left            =   6480
            Style           =   1  'Graphical
            TabIndex        =   69
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   41
            Left            =   7560
            Style           =   1  'Graphical
            TabIndex        =   68
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   40
            Left            =   8640
            Style           =   1  'Graphical
            TabIndex        =   67
            Top             =   0
            Width           =   1065
         End
      End
      Begin VB.Frame fraFuncPanel 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   300
         Index           =   4
         Left            =   90
         TabIndex        =   19
         Top             =   1230
         Width           =   11685
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   49
            Left            =   9720
            Style           =   1  'Graphical
            TabIndex        =   184
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   "Clock I/O"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   38
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   66
            Tag             =   "5"
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   "Absences"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   37
            Left            =   1080
            Style           =   1  'Graphical
            TabIndex        =   65
            Tag             =   "6"
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   36
            Left            =   2160
            Style           =   1  'Graphical
            TabIndex        =   64
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   "  "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   35
            Left            =   3240
            Style           =   1  'Graphical
            TabIndex        =   63
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   34
            Left            =   4320
            Style           =   1  'Graphical
            TabIndex        =   62
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   33
            Left            =   5400
            Style           =   1  'Graphical
            TabIndex        =   61
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   32
            Left            =   6480
            Style           =   1  'Graphical
            TabIndex        =   60
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   31
            Left            =   7560
            Style           =   1  'Graphical
            TabIndex        =   59
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   30
            Left            =   8640
            Style           =   1  'Graphical
            TabIndex        =   58
            Top             =   0
            Width           =   1065
         End
      End
      Begin VB.Frame fraFuncPanel 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   300
         Index           =   3
         Left            =   90
         TabIndex        =   18
         Top             =   900
         Width           =   13245
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   39
            Left            =   9720
            Style           =   1  'Graphical
            TabIndex        =   183
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   28
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   57
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   27
            Left            =   1080
            Style           =   1  'Graphical
            TabIndex        =   56
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   26
            Left            =   2160
            Style           =   1  'Graphical
            TabIndex        =   55
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   25
            Left            =   3240
            Style           =   1  'Graphical
            TabIndex        =   54
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   24
            Left            =   4320
            Style           =   1  'Graphical
            TabIndex        =   53
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   23
            Left            =   5400
            Style           =   1  'Graphical
            TabIndex        =   52
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   22
            Left            =   6480
            Style           =   1  'Graphical
            TabIndex        =   51
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   "  "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   21
            Left            =   7560
            Style           =   1  'Graphical
            TabIndex        =   50
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   20
            Left            =   10800
            Style           =   1  'Graphical
            TabIndex        =   49
            Top             =   0
            Width           =   1065
         End
      End
      Begin VB.Frame fraFuncPanel 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   300
         Index           =   2
         Left            =   90
         TabIndex        =   17
         Top             =   570
         Width           =   14235
         Begin VB.CheckBox chkTool 
            BackColor       =   &H0000FFFF&
            Caption         =   "Stop All"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   13
            Left            =   2160
            Style           =   1  'Graphical
            TabIndex        =   394
            Tag             =   "SLIDE.STOP"
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkTool 
            BackColor       =   &H0000FFFF&
            Caption         =   "Maximize"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   10
            Left            =   11880
            Style           =   1  'Graphical
            TabIndex        =   390
            Tag             =   "SLIDE.MAXI"
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkTool 
            BackColor       =   &H0000FFFF&
            Caption         =   "Collected"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   12
            Left            =   6480
            Style           =   1  'Graphical
            TabIndex        =   393
            Tag             =   "SLIDE.COLLECTED"
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkTool 
            BackColor       =   &H0000FFFF&
            Caption         =   "Hold All"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   11
            Left            =   5400
            Style           =   1  'Graphical
            TabIndex        =   392
            Tag             =   "SLIDE.HOLD.ALL"
            Top             =   0
            Width           =   1065
         End
         Begin MSComctlLib.Slider Slider1 
            Height          =   345
            Index           =   1
            Left            =   10740
            TabIndex        =   391
            ToolTipText     =   "Steps Per Time Unit"
            Top             =   -30
            Width           =   1185
            _ExtentX        =   2090
            _ExtentY        =   609
            _Version        =   393216
            LargeChange     =   1
            Max             =   5
            SelStart        =   1
            TickStyle       =   3
            Value           =   1
         End
         Begin VB.CheckBox chkTool 
            BackColor       =   &H0000FFFF&
            Caption         =   "Synchron"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   9
            Left            =   7560
            Style           =   1  'Graphical
            TabIndex        =   389
            Tag             =   "SLIDE.DOUBLE"
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkTool 
            BackColor       =   &H0000FFFF&
            Caption         =   "Show List"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   8
            Left            =   1080
            Style           =   1  'Graphical
            TabIndex        =   388
            Tag             =   "SLIDE.SELECT"
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkTool 
            BackColor       =   &H0000FFFF&
            Caption         =   "Hold Right"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   7
            Left            =   4320
            Style           =   1  'Graphical
            TabIndex        =   386
            Tag             =   "SLIDE.HOLD.RIGHT"
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkTool 
            BackColor       =   &H0000FFFF&
            Caption         =   "Hold Left"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   6
            Left            =   3240
            Style           =   1  'Graphical
            TabIndex        =   385
            Tag             =   "SLIDE.HOLD.LEFT"
            Top             =   0
            Width           =   1065
         End
         Begin MSComctlLib.Slider Slider1 
            Height          =   345
            Index           =   0
            Left            =   8580
            TabIndex        =   384
            ToolTipText     =   "Scenario Unit Timer"
            Top             =   -30
            Width           =   2265
            _ExtentX        =   3995
            _ExtentY        =   609
            _Version        =   393216
            SelStart        =   5
            TickStyle       =   3
            Value           =   5
         End
         Begin VB.CheckBox chkTool 
            BackColor       =   &H0000FFFF&
            Caption         =   "Open Files"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   5
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   383
            Tag             =   "SLIDES"
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   29
            Left            =   11310
            Style           =   1  'Graphical
            TabIndex        =   182
            Tag             =   " "
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   18
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   48
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   17
            Left            =   12180
            Style           =   1  'Graphical
            TabIndex        =   47
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   16
            Left            =   12030
            Style           =   1  'Graphical
            TabIndex        =   46
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   15
            Left            =   11640
            Style           =   1  'Graphical
            TabIndex        =   45
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   14
            Left            =   11250
            Style           =   1  'Graphical
            TabIndex        =   44
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   13
            Left            =   10980
            Style           =   1  'Graphical
            TabIndex        =   43
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   12
            Left            =   11040
            Style           =   1  'Graphical
            TabIndex        =   42
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   11
            Left            =   10710
            Style           =   1  'Graphical
            TabIndex        =   41
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   10
            Left            =   10470
            Style           =   1  'Graphical
            TabIndex        =   40
            Top             =   0
            Width           =   1065
         End
      End
      Begin VB.Frame fraFuncPanel 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   300
         Index           =   1
         Left            =   90
         TabIndex        =   16
         Top             =   240
         Width           =   11685
         Begin VB.CheckBox chkTool 
            BackColor       =   &H0000FFFF&
            Caption         =   "Basic Data"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   16
            Left            =   5400
            Style           =   1  'Graphical
            TabIndex        =   427
            Tag             =   "SLIDE.SELECT"
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkTool 
            BackColor       =   &H0000FFFF&
            Caption         =   "Shifts"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   15
            Left            =   4320
            Style           =   1  'Graphical
            TabIndex        =   426
            Tag             =   "SLIDE.SELECT"
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkTool 
            BackColor       =   &H0000FFFF&
            Caption         =   "Staff"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   14
            Left            =   3240
            Style           =   1  'Graphical
            TabIndex        =   425
            Tag             =   "SLIDE.SELECT"
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   19
            Left            =   9720
            Style           =   1  'Graphical
            TabIndex        =   181
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   9
            Left            =   8640
            Style           =   1  'Graphical
            TabIndex        =   39
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   8
            Left            =   7560
            Style           =   1  'Graphical
            TabIndex        =   38
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   7
            Left            =   6480
            Style           =   1  'Graphical
            TabIndex        =   37
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   "Basic Data"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   6
            Left            =   5400
            Style           =   1  'Graphical
            TabIndex        =   36
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   " Shifts"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   5
            Left            =   4350
            Style           =   1  'Graphical
            TabIndex        =   35
            Tag             =   "7"
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   "Staff"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   4
            Left            =   3240
            Style           =   1  'Graphical
            TabIndex        =   34
            Tag             =   "2"
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   "Absences"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   3
            Left            =   2160
            Style           =   1  'Graphical
            TabIndex        =   33
            Tag             =   "4"
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   "Clock I/O"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   2
            Left            =   1080
            Style           =   1  'Graphical
            TabIndex        =   32
            Tag             =   "3"
            Top             =   0
            Width           =   1065
         End
         Begin VB.CheckBox chkWork 
            Caption         =   "ID Cards"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   1
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   31
            Tag             =   "1"
            Top             =   0
            Width           =   1065
         End
      End
   End
   Begin VB.Frame fraWorkPanel 
      BackColor       =   &H80000010&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1005
      Index           =   1
      Left            =   60
      TabIndex        =   27
      Top             =   1440
      Visible         =   0   'False
      Width           =   12000
      Begin VB.Frame fraTopSplit 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   120
         Index           =   1
         Left            =   10800
         MousePointer    =   9  'Size W E
         TabIndex        =   304
         Top             =   0
         Width           =   1200
      End
      Begin VB.Frame fraTopData 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Index           =   1
         Left            =   4410
         TabIndex        =   289
         Top             =   1020
         Width           =   2235
         Begin TABLib.TAB TAB3 
            Height          =   435
            Index           =   1
            Left            =   90
            TabIndex        =   290
            Top             =   270
            Visible         =   0   'False
            Width           =   2025
            _Version        =   65536
            _ExtentX        =   3572
            _ExtentY        =   767
            _StockProps     =   64
         End
      End
      Begin VB.Frame fraSplitter 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   675
         Index           =   1
         Left            =   2010
         MousePointer    =   9  'Size W E
         TabIndex        =   130
         Top             =   1020
         Width           =   60
      End
      Begin VB.Frame fraRightData 
         Caption         =   "Server Data Evaluation"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Index           =   1
         Left            =   2100
         TabIndex        =   30
         Top             =   1020
         Width           =   2235
         Begin TABLib.TAB TAB2 
            Height          =   435
            Index           =   1
            Left            =   90
            TabIndex        =   132
            Top             =   270
            Width           =   2025
            _Version        =   65536
            _ExtentX        =   3572
            _ExtentY        =   767
            _StockProps     =   64
         End
      End
      Begin VB.Frame fraLeftData 
         Caption         =   "File Data Evaluation"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Index           =   1
         Left            =   0
         TabIndex        =   29
         Top             =   1020
         Width           =   1995
         Begin TABLib.TAB TAB1 
            Height          =   435
            Index           =   1
            Left            =   60
            TabIndex        =   131
            Top             =   270
            Width           =   1815
            _Version        =   65536
            _ExtentX        =   3201
            _ExtentY        =   767
            _StockProps     =   64
         End
      End
      Begin VB.Frame fraDialog 
         Caption         =   "Staff ID Card Import"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   1
         Left            =   0
         TabIndex        =   28
         Top             =   0
         Width           =   11595
         Begin VB.CheckBox chkTask 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Index           =   18
            Left            =   2430
            Picture         =   "frmMainDialog.frx":2830
            Style           =   1  'Graphical
            TabIndex        =   379
            Tag             =   "CARD.CLEAR:0,35"
            Top             =   270
            Width           =   615
         End
         Begin VB.CheckBox chkTask 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Index           =   35
            Left            =   1800
            Picture         =   "frmMainDialog.frx":2B3A
            Style           =   1  'Graphical
            TabIndex        =   285
            Tag             =   "CARD.CHECK:0"
            Top             =   270
            Width           =   615
         End
         Begin VB.CheckBox chkPanelIcon 
            Height          =   615
            Index           =   1
            Left            =   90
            Picture         =   "frmMainDialog.frx":3404
            Style           =   1  'Graphical
            TabIndex        =   222
            Top             =   270
            Width           =   615
         End
         Begin VB.CheckBox chkImport 
            Caption         =   "Import"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   1
            Left            =   3060
            Style           =   1  'Graphical
            TabIndex        =   210
            Tag             =   "0,35,18,17"
            Top             =   585
            Width           =   1065
         End
         Begin VB.CheckBox chkTask 
            Caption         =   "Compare"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   17
            Left            =   3060
            Style           =   1  'Graphical
            TabIndex        =   209
            Tag             =   "CARD.COMP:0,35,18"
            Top             =   270
            Width           =   1065
         End
         Begin VB.CheckBox chkTask 
            Caption         =   "Load Staff"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   14
            Left            =   720
            Style           =   1  'Graphical
            TabIndex        =   206
            Top             =   585
            Width           =   1065
         End
         Begin VB.CheckBox chkTask 
            Caption         =   " Open File"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   0
            Left            =   720
            Style           =   1  'Graphical
            TabIndex        =   133
            Tag             =   "CARD.FILE"
            Top             =   270
            Width           =   1065
         End
         Begin VB.Image imgPointer 
            Height          =   240
            Index           =   1
            Left            =   1110
            Picture         =   "frmMainDialog.frx":3CCE
            ToolTipText     =   "Step 1"
            Top             =   30
            Visible         =   0   'False
            Width           =   240
         End
      End
   End
   Begin VB.Frame fraWorkPanel 
      BackColor       =   &H80000010&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1005
      Index           =   2
      Left            =   60
      TabIndex        =   134
      Top             =   2520
      Visible         =   0   'False
      Width           =   12000
      Begin VB.Frame fraTopSplit 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   120
         Index           =   2
         Left            =   10800
         MousePointer    =   9  'Size W E
         TabIndex        =   305
         Top             =   0
         Width           =   1200
      End
      Begin VB.Frame fraTopData 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Index           =   2
         Left            =   4440
         TabIndex        =   291
         Top             =   1020
         Width           =   2235
         Begin TABLib.TAB TAB3 
            Height          =   435
            Index           =   2
            Left            =   90
            TabIndex        =   292
            Top             =   270
            Visible         =   0   'False
            Width           =   2025
            _Version        =   65536
            _ExtentX        =   3572
            _ExtentY        =   767
            _StockProps     =   64
         End
      End
      Begin VB.Frame fraDialog 
         Caption         =   "Employee Data Import"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   2
         Left            =   0
         TabIndex        =   140
         Top             =   0
         Width           =   11595
         Begin VB.CheckBox chkTask 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Index           =   34
            Left            =   1800
            Picture         =   "frmMainDialog.frx":4258
            Style           =   1  'Graphical
            TabIndex        =   242
            Top             =   270
            Width           =   615
         End
         Begin VB.CheckBox chkPanelIcon 
            Height          =   615
            Index           =   2
            Left            =   90
            Picture         =   "frmMainDialog.frx":4B22
            Style           =   1  'Graphical
            TabIndex        =   221
            Top             =   270
            Width           =   615
         End
         Begin VB.CheckBox chkTask 
            Caption         =   "Import"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   20
            Left            =   2430
            Style           =   1  'Graphical
            TabIndex        =   212
            Tag             =   "IMP"
            Top             =   585
            Width           =   1065
         End
         Begin VB.CheckBox chkTask 
            Caption         =   "Compare"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   19
            Left            =   2430
            Style           =   1  'Graphical
            TabIndex        =   211
            Tag             =   "COMP"
            Top             =   270
            Width           =   1065
         End
         Begin VB.CheckBox chkTask 
            Caption         =   "Load Staff"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   15
            Left            =   720
            Style           =   1  'Graphical
            TabIndex        =   207
            Tag             =   "STFDATA"
            Top             =   585
            Width           =   1065
         End
         Begin VB.CheckBox chkTask 
            Caption         =   " Open File"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   1
            Left            =   720
            Style           =   1  'Graphical
            TabIndex        =   141
            Top             =   270
            Width           =   1065
         End
      End
      Begin VB.Frame fraLeftData 
         Caption         =   "File Data Evaluation"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Index           =   2
         Left            =   0
         TabIndex        =   138
         Top             =   1020
         Width           =   1995
         Begin TABLib.TAB TAB1 
            Height          =   435
            Index           =   2
            Left            =   60
            TabIndex        =   139
            Top             =   270
            Width           =   1815
            _Version        =   65536
            _ExtentX        =   3201
            _ExtentY        =   767
            _StockProps     =   64
         End
      End
      Begin VB.Frame fraRightData 
         Caption         =   "Server Data Evaluation"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Index           =   2
         Left            =   2100
         TabIndex        =   136
         Top             =   1020
         Width           =   2235
         Begin TABLib.TAB TAB2 
            Height          =   435
            Index           =   2
            Left            =   90
            TabIndex        =   137
            Top             =   270
            Width           =   2025
            _Version        =   65536
            _ExtentX        =   3572
            _ExtentY        =   767
            _StockProps     =   64
         End
      End
      Begin VB.Frame fraSplitter 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   675
         Index           =   2
         Left            =   2010
         MousePointer    =   9  'Size W E
         TabIndex        =   135
         Top             =   1020
         Width           =   60
      End
   End
   Begin VB.Frame fraWorkPanel 
      BackColor       =   &H80000010&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1005
      Index           =   3
      Left            =   60
      TabIndex        =   142
      Top             =   3600
      Visible         =   0   'False
      Width           =   12000
      Begin VB.Frame fraTopSplit 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   120
         Index           =   3
         Left            =   10800
         MousePointer    =   9  'Size W E
         TabIndex        =   306
         Top             =   0
         Width           =   1200
      End
      Begin VB.Frame fraTopData 
         Caption         =   "Staff Data"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Index           =   3
         Left            =   4470
         TabIndex        =   293
         Top             =   1020
         Width           =   2235
         Begin TABLib.TAB TAB3 
            Height          =   435
            Index           =   3
            Left            =   90
            TabIndex        =   294
            Tag             =   "SHOW"
            Top             =   270
            Visible         =   0   'False
            Width           =   2025
            _Version        =   65536
            _ExtentX        =   3572
            _ExtentY        =   767
            _StockProps     =   64
         End
      End
      Begin VB.Frame fraSplitter 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   675
         Index           =   3
         Left            =   2010
         MousePointer    =   9  'Size W E
         TabIndex        =   149
         Top             =   1080
         Width           =   60
      End
      Begin VB.Frame fraRightData 
         Caption         =   "Clocking Server Data (SPRTAB)"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Index           =   3
         Left            =   2100
         TabIndex        =   147
         Top             =   1020
         Width           =   2235
         Begin TABLib.TAB TAB2 
            Height          =   435
            Index           =   3
            Left            =   90
            TabIndex        =   148
            Top             =   270
            Width           =   2025
            _Version        =   65536
            _ExtentX        =   3572
            _ExtentY        =   767
            _StockProps     =   64
         End
      End
      Begin VB.Frame fraLeftData 
         Caption         =   "Clocking File Data"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Index           =   3
         Left            =   0
         TabIndex        =   145
         Top             =   1020
         Width           =   1995
         Begin TABLib.TAB TAB1 
            Height          =   435
            Index           =   3
            Left            =   60
            TabIndex        =   146
            Top             =   270
            Width           =   1815
            _Version        =   65536
            _ExtentX        =   3201
            _ExtentY        =   767
            _StockProps     =   64
         End
      End
      Begin VB.Frame fraDialog 
         Caption         =   "Clock In/Out Import"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   3
         Left            =   0
         TabIndex        =   143
         Top             =   0
         Width           =   11595
         Begin VB.CheckBox chkTask 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Index           =   44
            Left            =   2430
            Picture         =   "frmMainDialog.frx":53EC
            Style           =   1  'Graphical
            TabIndex        =   380
            Tag             =   "CLOCK.CLEAR:2,36"
            Top             =   270
            Width           =   615
         End
         Begin VB.CheckBox chkTask 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Index           =   36
            Left            =   1800
            Picture         =   "frmMainDialog.frx":56F6
            Style           =   1  'Graphical
            TabIndex        =   286
            Tag             =   "CLOCK.CHECK:2"
            Top             =   270
            Width           =   615
         End
         Begin VB.CheckBox chkPanelIcon 
            Height          =   615
            Index           =   3
            Left            =   90
            Picture         =   "frmMainDialog.frx":5FC0
            Style           =   1  'Graphical
            TabIndex        =   220
            Top             =   270
            Width           =   615
         End
         Begin VB.CheckBox chkImport 
            Caption         =   "Import"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   3
            Left            =   3060
            Style           =   1  'Graphical
            TabIndex        =   214
            Tag             =   "2,36,44,21"
            Top             =   585
            Width           =   1065
         End
         Begin VB.CheckBox chkTask 
            Caption         =   "Compare"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   21
            Left            =   3060
            Style           =   1  'Graphical
            TabIndex        =   213
            Tag             =   "CLOCK.COMP:2,36,44"
            Top             =   270
            Width           =   1065
         End
         Begin VB.CheckBox chkTask 
            Caption         =   "Load Staff"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   16
            Left            =   720
            Style           =   1  'Graphical
            TabIndex        =   208
            Top             =   585
            Width           =   1065
         End
         Begin VB.CheckBox chkTask 
            Caption         =   " Open File"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   2
            Left            =   720
            Style           =   1  'Graphical
            TabIndex        =   144
            Tag             =   "CLOCK.FILE"
            Top             =   270
            Width           =   1065
         End
         Begin VB.Image imgPointer 
            Height          =   240
            Index           =   3
            Left            =   1140
            Picture         =   "frmMainDialog.frx":62CA
            ToolTipText     =   "Step 1"
            Top             =   0
            Width           =   240
         End
      End
   End
   Begin VB.Frame fraWorkPanel 
      BackColor       =   &H80000010&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Index           =   4
      Left            =   60
      TabIndex        =   150
      Top             =   4680
      Visible         =   0   'False
      Width           =   17970
      Begin VB.Frame fraTopSplit 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   120
         Index           =   4
         Left            =   10800
         MousePointer    =   9  'Size W E
         TabIndex        =   307
         Top             =   0
         Width           =   1200
      End
      Begin VB.Frame fraTopData 
         Caption         =   "RMS Staff Data"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Index           =   4
         Left            =   4440
         TabIndex        =   295
         Top             =   1020
         Width           =   2235
         Begin TABLib.TAB TAB3 
            Height          =   435
            Index           =   4
            Left            =   90
            TabIndex        =   296
            Tag             =   "SHOW"
            Top             =   210
            Visible         =   0   'False
            Width           =   2025
            _Version        =   65536
            _ExtentX        =   3572
            _ExtentY        =   767
            _StockProps     =   64
         End
      End
      Begin VB.Frame fraDialog 
         Caption         =   "Absence Data Import"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   4
         Left            =   30
         TabIndex        =   156
         Top             =   0
         Width           =   17325
         Begin VB.CheckBox chkTask 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Index           =   22
            Left            =   2640
            Picture         =   "frmMainDialog.frx":6854
            Style           =   1  'Graphical
            TabIndex        =   400
            Tag             =   "ABS.ORGA"
            ToolTipText     =   "Open staff filter"
            Top             =   270
            Width           =   615
         End
         Begin VB.CheckBox chkTask 
            Caption         =   "Validation"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   42
            Left            =   4980
            Style           =   1  'Graphical
            TabIndex        =   342
            Tag             =   "ABS.VALID:3,40"
            ToolTipText     =   "Clear out obselete data."
            Top             =   270
            Width           =   1065
         End
         Begin VB.CheckBox chkTask 
            Caption         =   "Load Shift"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   41
            Left            =   4980
            Style           =   1  'Graphical
            TabIndex        =   341
            Tag             =   "ABS.DRRTAB:3,40"
            ToolTipText     =   "Load related shifts and absence records from RMS."
            Top             =   585
            Width           =   1065
         End
         Begin VB.CheckBox chkTask 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Index           =   40
            Left            =   4350
            Picture         =   "frmMainDialog.frx":711E
            Style           =   1  'Graphical
            TabIndex        =   340
            Tag             =   "ABS.FUNC:3"
            ToolTipText     =   "Check matching functions and periods."
            Top             =   270
            Width           =   615
         End
         Begin VB.CheckBox chkTask 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Index           =   39
            Left            =   6690
            Picture         =   "frmMainDialog.frx":79E8
            Style           =   1  'Graphical
            TabIndex        =   338
            Tag             =   "ABS.CLEAR:3,40,42,33"
            ToolTipText     =   "Clear out obsolete data."
            Top             =   270
            Width           =   615
         End
         Begin VB.TextBox txtVptoDD 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   4
            Left            =   2280
            MaxLength       =   2
            TabIndex        =   337
            Top             =   585
            Width           =   345
         End
         Begin VB.TextBox txtVptoMM 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   4
            Left            =   1920
            MaxLength       =   2
            TabIndex        =   336
            Top             =   585
            Width           =   345
         End
         Begin VB.TextBox txtVptoYY 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   4
            Left            =   1350
            MaxLength       =   4
            TabIndex        =   335
            Top             =   585
            Width           =   555
         End
         Begin VB.TextBox txtVpfrDD 
            Alignment       =   2  'Center
            BackColor       =   &H0000FFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   4
            Left            =   2280
            MaxLength       =   2
            TabIndex        =   334
            Top             =   270
            Width           =   345
         End
         Begin VB.TextBox txtVpfrMM 
            Alignment       =   2  'Center
            BackColor       =   &H0000FFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   4
            Left            =   1920
            MaxLength       =   2
            TabIndex        =   333
            Top             =   270
            Width           =   345
         End
         Begin VB.TextBox txtVpfrYY 
            Alignment       =   2  'Center
            BackColor       =   &H0000FFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   4
            Left            =   1350
            MaxLength       =   4
            TabIndex        =   332
            Top             =   270
            Width           =   555
         End
         Begin VB.CheckBox chkTask 
            Caption         =   "From"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   38
            Left            =   720
            Style           =   1  'Graphical
            TabIndex        =   331
            Tag             =   "VPFR"
            Top             =   270
            Width           =   615
         End
         Begin VB.CheckBox chkTask 
            Caption         =   "To"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   37
            Left            =   720
            Style           =   1  'Graphical
            TabIndex        =   330
            Tag             =   "VPTO"
            Top             =   585
            Width           =   615
         End
         Begin VB.CheckBox chkTask 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Index           =   33
            Left            =   6060
            Picture         =   "frmMainDialog.frx":7CF2
            Style           =   1  'Graphical
            TabIndex        =   241
            Tag             =   "ABS.SHRCHECK:3,40,42"
            ToolTipText     =   "Cross-check the whole data set."
            Top             =   270
            Width           =   615
         End
         Begin VB.CheckBox chkPanelIcon 
            Height          =   615
            Index           =   4
            Left            =   90
            Picture         =   "frmMainDialog.frx":85BC
            Style           =   1  'Graphical
            TabIndex        =   223
            Top             =   270
            Width           =   615
         End
         Begin VB.CheckBox chkImport 
            Caption         =   "Import"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   4
            Left            =   7320
            Style           =   1  'Graphical
            TabIndex        =   205
            Tag             =   "3,40,42,33,39,12"
            ToolTipText     =   "Enable the import function buttons."
            Top             =   585
            Width           =   1065
         End
         Begin VB.CheckBox chkTask 
            Caption         =   "Compare"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   12
            Left            =   7320
            Style           =   1  'Graphical
            TabIndex        =   204
            Tag             =   "ABS.COMP:3,40,42,33,39"
            ToolTipText     =   "Compare uploaded absences with RMS information."
            Top             =   270
            Width           =   1065
         End
         Begin VB.CheckBox chkTask 
            Caption         =   "Load Staff"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   11
            Left            =   3270
            Style           =   1  'Graphical
            TabIndex        =   203
            Tag             =   "ABS.STFTAB"
            ToolTipText     =   "Load related staff records from RMS."
            Top             =   585
            Width           =   1065
         End
         Begin VB.CheckBox chkTask 
            Caption         =   "Load Abs."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   3
            Left            =   3270
            Style           =   1  'Graphical
            TabIndex        =   157
            Tag             =   "ABS.HRVIEW"
            ToolTipText     =   "Load absence data from remote system."
            Top             =   270
            Width           =   1065
         End
         Begin VB.Frame Frame1 
            Caption         =   "Differences"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   975
            Left            =   8460
            TabIndex        =   372
            Top             =   0
            Width           =   1425
            Begin VB.CheckBox chkTask 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   615
               Index           =   43
               Left            =   720
               Style           =   1  'Graphical
               TabIndex        =   374
               Tag             =   "SPARE"
               ToolTipText     =   "Spare Button (Don't touch)"
               Top             =   270
               Width           =   615
            End
            Begin VB.CheckBox chkTask 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   615
               Index           =   13
               Left            =   90
               Picture         =   "frmMainDialog.frx":88C6
               Style           =   1  'Graphical
               TabIndex        =   373
               Tag             =   "ABS.DETAILS"
               Top             =   270
               Width           =   615
            End
         End
         Begin VB.Frame fraPanelRules 
            Caption         =   "Validation Rules"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   975
            Index           =   4
            Left            =   9855
            TabIndex        =   367
            Top             =   0
            Width           =   5895
            Begin VB.Frame fraPanelRules2 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   975
               Index           =   4
               Left            =   3240
               TabIndex        =   375
               Top             =   0
               Visible         =   0   'False
               Width           =   2655
               Begin VB.CheckBox chkRule 
                  Caption         =   "... ..."
                  Enabled         =   0   'False
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   210
                  Index           =   5
                  Left            =   90
                  TabIndex        =   378
                  Tag             =   "ABS."
                  Top             =   690
                  Width           =   2475
               End
               Begin VB.CheckBox chkRule 
                  Caption         =   "... ..."
                  Enabled         =   0   'False
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   210
                  Index           =   4
                  Left            =   90
                  TabIndex        =   377
                  Top             =   480
                  Width           =   2475
               End
               Begin VB.CheckBox chkRule 
                  Caption         =   "... ..."
                  Enabled         =   0   'False
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   210
                  Index           =   3
                  Left            =   90
                  TabIndex        =   376
                  Top             =   270
                  Width           =   2475
               End
            End
            Begin VB.CheckBox chkRule 
               Caption         =   "Check Clock-In Times"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   2
               Left            =   720
               TabIndex        =   371
               Tag             =   "ABS.DRRCODE_CLOCK:,R,"
               ToolTipText     =   "Don't import Absence"
               Top             =   300
               Width           =   2475
            End
            Begin VB.CheckBox chkRule 
               Caption         =   "Protect Old Shift Codes"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   1
               Left            =   720
               TabIndex        =   370
               Top             =   480
               Value           =   1  'Checked
               Width           =   2475
            End
            Begin VB.CheckBox chkRule 
               Caption         =   "Protect Absence Codes"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   0
               Left            =   720
               TabIndex        =   369
               Tag             =   "ABS.DRRCODE:,R,"
               ToolTipText     =   "Don't update: 'R'"
               Top             =   690
               Width           =   2475
            End
            Begin VB.CheckBox chkPanelRules 
               Height          =   615
               Index           =   4
               Left            =   90
               Picture         =   "frmMainDialog.frx":8BD0
               Style           =   1  'Graphical
               TabIndex        =   368
               Tag             =   "0,2"
               Top             =   270
               Width           =   615
            End
         End
         Begin VB.Image imgPointer 
            Height          =   240
            Index           =   4
            Left            =   3450
            Picture         =   "frmMainDialog.frx":9012
            ToolTipText     =   "Step 1"
            Top             =   30
            Width           =   240
         End
      End
      Begin VB.Frame fraLeftData 
         Caption         =   "SHR Absence Data"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Index           =   4
         Left            =   0
         TabIndex        =   154
         Top             =   1020
         Width           =   1995
         Begin TABLib.TAB TAB1 
            Height          =   435
            Index           =   4
            Left            =   60
            TabIndex        =   155
            Top             =   210
            Width           =   1815
            _Version        =   65536
            _ExtentX        =   3201
            _ExtentY        =   767
            _StockProps     =   64
         End
      End
      Begin VB.Frame fraRightData 
         Caption         =   "RMS Server DRRTAB Data"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Index           =   4
         Left            =   2100
         TabIndex        =   152
         Top             =   1020
         Width           =   2235
         Begin TABLib.TAB TAB2 
            Height          =   435
            Index           =   4
            Left            =   90
            TabIndex        =   153
            Top             =   210
            Width           =   2025
            _Version        =   65536
            _ExtentX        =   3572
            _ExtentY        =   767
            _StockProps     =   64
         End
      End
      Begin VB.Frame fraSplitter 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   675
         Index           =   4
         Left            =   2010
         MousePointer    =   9  'Size W E
         TabIndex        =   151
         Top             =   1020
         Width           =   60
      End
   End
   Begin VB.Frame fraWorkPanel 
      BackColor       =   &H80000010&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1005
      Index           =   5
      Left            =   60
      TabIndex        =   158
      Top             =   5760
      Visible         =   0   'False
      Width           =   12000
      Begin VB.Frame fraTopSplit 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   120
         Index           =   5
         Left            =   10800
         MousePointer    =   9  'Size W E
         TabIndex        =   308
         Top             =   0
         Width           =   1200
      End
      Begin VB.Frame fraTopData 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Index           =   5
         Left            =   4440
         TabIndex        =   297
         Top             =   1020
         Width           =   2235
         Begin TABLib.TAB TAB3 
            Height          =   435
            Index           =   5
            Left            =   90
            TabIndex        =   298
            Top             =   270
            Visible         =   0   'False
            Width           =   2025
            _Version        =   65536
            _ExtentX        =   3572
            _ExtentY        =   767
            _StockProps     =   64
         End
      End
      Begin VB.Frame fraSplitter 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   675
         Index           =   5
         Left            =   2010
         MousePointer    =   9  'Size W E
         TabIndex        =   165
         Top             =   1020
         Width           =   60
      End
      Begin VB.Frame fraRightData 
         Caption         =   "Interface Protocol History"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Index           =   5
         Left            =   2100
         TabIndex        =   163
         Top             =   1020
         Width           =   2235
         Begin TABLib.TAB TAB2 
            Height          =   435
            Index           =   5
            Left            =   90
            TabIndex        =   164
            Top             =   270
            Width           =   2025
            _Version        =   65536
            _ExtentX        =   3572
            _ExtentY        =   767
            _StockProps     =   64
         End
      End
      Begin VB.Frame fraLeftData 
         Caption         =   "Last Processed File"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Index           =   5
         Left            =   0
         TabIndex        =   161
         Top             =   1020
         Width           =   1995
         Begin TABLib.TAB TAB1 
            Height          =   435
            Index           =   5
            Left            =   60
            TabIndex        =   162
            Top             =   270
            Width           =   1815
            _Version        =   65536
            _ExtentX        =   3201
            _ExtentY        =   767
            _StockProps     =   64
         End
      End
      Begin VB.Frame fraDialog 
         Caption         =   "Clock In/Out Interface Status"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   5
         Left            =   0
         TabIndex        =   159
         Top             =   0
         Width           =   11595
         Begin VB.Timer AmpelTimer 
            Enabled         =   0   'False
            Index           =   5
            Interval        =   1000
            Left            =   0
            Top             =   0
         End
         Begin VB.CheckBox chkTask 
            Caption         =   "Get"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   27
            Left            =   1980
            Style           =   1  'Graphical
            TabIndex        =   230
            Tag             =   "TRLR"
            Top             =   270
            Width           =   615
         End
         Begin VB.CheckBox chkTask 
            Caption         =   "Set"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   26
            Left            =   1980
            Style           =   1  'Graphical
            TabIndex        =   229
            Tag             =   "TRLG"
            Top             =   585
            Width           =   615
         End
         Begin VB.CheckBox chkTask 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   25
            Left            =   5850
            Style           =   1  'Graphical
            TabIndex        =   228
            Top             =   270
            Width           =   1065
         End
         Begin VB.CheckBox chkTask 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   24
            Left            =   4770
            Style           =   1  'Graphical
            TabIndex        =   227
            Top             =   270
            Width           =   1065
         End
         Begin VB.CheckBox chkTask 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   23
            Left            =   3690
            Style           =   1  'Graphical
            TabIndex        =   226
            Top             =   270
            Width           =   1065
         End
         Begin VB.CheckBox chkTask 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   7
            Left            =   2610
            Style           =   1  'Graphical
            TabIndex        =   225
            Top             =   270
            Width           =   1065
         End
         Begin VB.CheckBox chkPanelIcon 
            Height          =   615
            Index           =   5
            Left            =   90
            Picture         =   "frmMainDialog.frx":959C
            Style           =   1  'Graphical
            TabIndex        =   224
            Top             =   270
            Width           =   615
         End
         Begin VB.CheckBox chkAmpel 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Index           =   5
            Left            =   720
            Picture         =   "frmMainDialog.frx":98A6
            Style           =   1  'Graphical
            TabIndex        =   194
            Tag             =   "TRLY"
            Top             =   270
            Width           =   615
         End
         Begin VB.CheckBox chkTask 
            Caption         =   "Start"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   6
            Left            =   1350
            Style           =   1  'Graphical
            TabIndex        =   193
            Tag             =   "TRLG"
            Top             =   585
            Width           =   615
         End
         Begin VB.CheckBox chkTask 
            Caption         =   "Stop"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   4
            Left            =   1350
            Style           =   1  'Graphical
            TabIndex        =   160
            Tag             =   "TRLR"
            Top             =   270
            Width           =   615
         End
      End
   End
   Begin VB.Frame fraWorkPanel 
      BackColor       =   &H80000010&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1005
      Index           =   6
      Left            =   60
      TabIndex        =   166
      Top             =   6840
      Visible         =   0   'False
      Width           =   12000
      Begin VB.Frame fraTopSplit 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   120
         Index           =   6
         Left            =   10800
         MousePointer    =   9  'Size W E
         TabIndex        =   309
         Top             =   0
         Width           =   1200
      End
      Begin VB.Frame fraTopData 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Index           =   6
         Left            =   4470
         TabIndex        =   299
         Top             =   1020
         Width           =   2235
         Begin TABLib.TAB TAB3 
            Height          =   435
            Index           =   6
            Left            =   90
            TabIndex        =   300
            Top             =   270
            Visible         =   0   'False
            Width           =   2025
            _Version        =   65536
            _ExtentX        =   3572
            _ExtentY        =   767
            _StockProps     =   64
         End
      End
      Begin VB.Frame fraDialog 
         Caption         =   "HR Absences Interface Status"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   6
         Left            =   0
         TabIndex        =   172
         Top             =   0
         Width           =   11595
         Begin VB.CheckBox chkTask 
            Caption         =   "Start"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   32
            Left            =   1350
            Style           =   1  'Graphical
            TabIndex        =   240
            Tag             =   "TRLG"
            Top             =   585
            Width           =   615
         End
         Begin VB.CheckBox chkTask 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   31
            Left            =   2610
            Style           =   1  'Graphical
            TabIndex        =   239
            Top             =   270
            Width           =   1065
         End
         Begin VB.CheckBox chkTask 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   30
            Left            =   3690
            Style           =   1  'Graphical
            TabIndex        =   238
            Top             =   270
            Width           =   1065
         End
         Begin VB.CheckBox chkTask 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   29
            Left            =   4770
            Style           =   1  'Graphical
            TabIndex        =   237
            Top             =   270
            Width           =   1065
         End
         Begin VB.CheckBox chkTask 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   28
            Left            =   5850
            Style           =   1  'Graphical
            TabIndex        =   236
            Top             =   270
            Width           =   1065
         End
         Begin VB.CheckBox chkTask 
            Caption         =   "Set"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   9
            Left            =   1980
            Style           =   1  'Graphical
            TabIndex        =   235
            Tag             =   "TRLG"
            Top             =   585
            Width           =   615
         End
         Begin VB.Timer AmpelTimer 
            Enabled         =   0   'False
            Index           =   6
            Interval        =   1000
            Left            =   0
            Top             =   0
         End
         Begin VB.CheckBox chkTask 
            Caption         =   "Stop"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   8
            Left            =   1350
            Style           =   1  'Graphical
            TabIndex        =   234
            Tag             =   "TRLR"
            Top             =   270
            Width           =   615
         End
         Begin VB.CheckBox chkAmpel 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Index           =   6
            Left            =   720
            Picture         =   "frmMainDialog.frx":9CE8
            Style           =   1  'Graphical
            TabIndex        =   233
            Tag             =   "TRLY"
            Top             =   270
            Width           =   615
         End
         Begin VB.CheckBox chkPanelIcon 
            Height          =   615
            Index           =   6
            Left            =   90
            Picture         =   "frmMainDialog.frx":A12A
            Style           =   1  'Graphical
            TabIndex        =   232
            Top             =   270
            Width           =   615
         End
         Begin VB.CheckBox chkTask 
            Caption         =   "Get"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   5
            Left            =   1980
            Style           =   1  'Graphical
            TabIndex        =   231
            Tag             =   "TRLR"
            Top             =   270
            Width           =   615
         End
      End
      Begin VB.Frame fraLeftData 
         Caption         =   "Last Processed File"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Index           =   6
         Left            =   0
         TabIndex        =   170
         Top             =   1020
         Width           =   1995
         Begin TABLib.TAB TAB1 
            Height          =   435
            Index           =   6
            Left            =   60
            TabIndex        =   171
            Top             =   270
            Width           =   1815
            _Version        =   65536
            _ExtentX        =   3201
            _ExtentY        =   767
            _StockProps     =   64
         End
      End
      Begin VB.Frame fraRightData 
         Caption         =   "Interface Protocol History"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Index           =   6
         Left            =   2100
         TabIndex        =   168
         Top             =   1020
         Width           =   2235
         Begin TABLib.TAB TAB2 
            Height          =   435
            Index           =   6
            Left            =   90
            TabIndex        =   169
            Top             =   270
            Width           =   2025
            _Version        =   65536
            _ExtentX        =   3572
            _ExtentY        =   767
            _StockProps     =   64
         End
      End
      Begin VB.Frame fraSplitter 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   675
         Index           =   6
         Left            =   2010
         MousePointer    =   9  'Size W E
         TabIndex        =   167
         Top             =   1020
         Width           =   60
      End
   End
   Begin VB.Frame fraWorkPanel 
      BackColor       =   &H80000010&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1005
      Index           =   7
      Left            =   60
      TabIndex        =   195
      Top             =   7920
      Visible         =   0   'False
      Width           =   12000
      Begin VB.Frame fraTopSplit 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   120
         Index           =   7
         Left            =   10800
         MousePointer    =   9  'Size W E
         TabIndex        =   310
         Top             =   0
         Width           =   1200
      End
      Begin VB.Frame fraTopData 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Index           =   7
         Left            =   4440
         TabIndex        =   301
         Top             =   1020
         Width           =   2235
         Begin TABLib.TAB TAB3 
            Height          =   435
            Index           =   7
            Left            =   90
            TabIndex        =   302
            Top             =   270
            Visible         =   0   'False
            Width           =   2025
            _Version        =   65536
            _ExtentX        =   3572
            _ExtentY        =   767
            _StockProps     =   64
         End
      End
      Begin VB.Frame fraSplitter 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   675
         Index           =   7
         Left            =   2010
         MousePointer    =   9  'Size W E
         TabIndex        =   202
         Top             =   1020
         Visible         =   0   'False
         Width           =   60
      End
      Begin VB.Frame fraRightData 
         Caption         =   "Server Data Evaluation"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Index           =   7
         Left            =   2100
         TabIndex        =   200
         Top             =   1020
         Visible         =   0   'False
         Width           =   2235
         Begin TABLib.TAB TAB2 
            Height          =   435
            Index           =   7
            Left            =   90
            TabIndex        =   201
            Top             =   270
            Width           =   2025
            _Version        =   65536
            _ExtentX        =   3572
            _ExtentY        =   767
            _StockProps     =   64
         End
      End
      Begin VB.Frame fraLeftData 
         Caption         =   "DRRTAB Data Evaluation"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Index           =   7
         Left            =   0
         TabIndex        =   198
         Top             =   1020
         Width           =   1995
         Begin TABLib.TAB TAB1 
            Height          =   435
            Index           =   7
            Left            =   60
            TabIndex        =   199
            Top             =   270
            Width           =   1815
            _Version        =   65536
            _ExtentX        =   3201
            _ExtentY        =   767
            _StockProps     =   64
         End
      End
      Begin VB.Frame fraDialog 
         Caption         =   "All DRRTAB Data (Performance Test)"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   7
         Left            =   0
         TabIndex        =   196
         Top             =   0
         Width           =   11595
         Begin VB.CheckBox chkTask 
            Caption         =   " Load All"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   10
            Left            =   90
            Style           =   1  'Graphical
            TabIndex        =   197
            Tag             =   "DRRTEST.X"
            Top             =   270
            Width           =   1065
         End
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   300
      Left            =   0
      TabIndex        =   0
      Top             =   11235
      Width           =   18090
      _ExtentX        =   31909
      _ExtentY        =   529
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   28813
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin TABLib.TAB TAB0 
      Height          =   1035
      Index           =   0
      Left            =   14430
      TabIndex        =   339
      Top             =   6750
      Visible         =   0   'False
      Width           =   1935
      _Version        =   65536
      _ExtentX        =   3413
      _ExtentY        =   1826
      _StockProps     =   64
   End
   Begin VB.Image Ampel 
      Height          =   480
      Index           =   3
      Left            =   13590
      Picture         =   "frmMainDialog.frx":A434
      Top             =   6150
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image Ampel 
      Height          =   480
      Index           =   2
      Left            =   13140
      Picture         =   "frmMainDialog.frx":A876
      Top             =   6150
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image Ampel 
      Height          =   480
      Index           =   1
      Left            =   12690
      Picture         =   "frmMainDialog.frx":ACB8
      Top             =   6150
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image Ampel 
      Height          =   480
      Index           =   0
      Left            =   12240
      Picture         =   "frmMainDialog.frx":B0FA
      Top             =   6150
      Visible         =   0   'False
      Width           =   480
   End
End
Attribute VB_Name = "frmMainDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim myLeft As Integer
Dim curMainButton As Integer
Dim curFuncPanel As Integer
Dim curWorkButton As Integer
Dim curWorkPanel As Integer
Dim curMainFunction As String
Dim minAbsSday As String
Dim maxAbsSday As String
Dim AbsStfPeno As Long
Dim AbsStfUrno As Long
Dim AbsHrvEmpId As Long
Dim AbsHrvVpfr  As Long
Dim AbsDrrStfu As Long
Dim AbsDrrAvfr As Long
Dim AbsHrvVpto As Long
Dim ActTab As Integer
Dim StaffOnly As Boolean
Dim TaskCheckBusy As Boolean
Dim CalledFromInternal As Boolean
Dim SlideShow As Boolean
Dim SlideHoldLeft As Boolean
Dim SlideHoldMid As Boolean
Dim SlideHoldRight As Boolean
Dim SlideHoldAll As Boolean
Dim SlideDouble As Boolean
Dim SlideMaximized As Boolean
Dim SlideIndex As Integer
Dim minCount As Integer
Dim Clock_Interval As String
Dim Keycards_Interval As String
Dim Absences_Interval As String
Dim InterfaceError As Boolean
Dim InterfaceEnabled As Boolean
Dim LogFile As Boolean






Public Sub EvaluateBroadCast(BcCmd As String, BcTbl As String, BcSel As String, BcFld As String, BcDat As String, BcTws As String, BcTwe As String, BcUsr As String, BcWks As String)
    Dim tmpData As String
    Dim tmpTime
    Select Case BcTbl
        Case "DRRTAB"
        Case "TIMERS"
            Select Case BcCmd
                Case "TIME"
                    If shpSrvLed(0).FillColor <> NormalGreen Then
                        shpSrvLed(0).FillColor = NormalGreen
                        shpSrvLed(0).BorderColor = vbBlack
                    Else
                        shpSrvLed(0).FillColor = LightGreen
                        shpSrvLed(0).BorderColor = vbBlack
                    End If
                    shpSrvLed(0).Refresh
                    tmpData = GetItem(BcTws, 1, ",")
                    tmpTime = CedaFullDateToVb(tmpData)
                    tmpData = UCase(Format(tmpTime, "dd.mmm.yyyy / hh:mm ")) & "z"
                    lblTime(1).Caption = tmpData
                    tmpData = GetItem(BcTws, 2, ",")
                    tmpTime = CedaFullDateToVb(tmpData)
                    tmpData = UCase(Format(tmpTime, "dd.mmm.yyyy / hh:mm LT ")) & HomeAirport
                    lblTime(2).Caption = tmpData
                    timSrvLed.Enabled = False
                    shpSrvLed(2).FillColor = vbButtonFace
                    If lblTime(3).Caption = "" Then lblTime(3).Caption = " Server: " & UfisServer.HostName
                    lblCurPos(2).ToolTipText = ""
                    timSrvLed.Enabled = True
                Case Else
            End Select
        Case Else
    End Select
End Sub

Private Sub chkAmpel_Click(Index As Integer)
    If chkAmpel(Index).Value = 1 Then
        AmpelTimer(Index).Enabled = True
    Else
        AmpelTimer(Index).Enabled = False
    End If
End Sub

Private Sub chkImport_Click(Index As Integer)
    On Error Resume Next
    If chkImport(Index).Value = 1 Then
        chkImport(Index).BackColor = LightGreen
        CheckWorkflow chkImport(Index).Tag
        ToggleBottomPanel curWorkPanel, True, True, True, True
        imgPointer(Index).Visible = True
    Else
        chkImport(Index).BackColor = vbButtonFace
        ToggleBottomPanel curWorkPanel, True, True, False, True
        imgPointer(Index).Visible = False
    End If
End Sub

Private Sub CheckWorkflow(tmpTag As String)
    Dim NextStep As Integer
    Dim taskIdx As Integer
    Dim taskItem As String
    Dim i As Integer
    If TaskCheckBusy = False Then
        TaskCheckBusy = True
        taskItem = imgPointer(curWorkPanel).Tag
        NextStep = Val(taskItem)
        i = 0
        taskItem = "START"
        While taskItem <> ""
            i = i + 1
            taskItem = GetItem(tmpTag, i, ",")
            If taskItem <> "" Then
                taskIdx = Val(taskItem)
                If i >= NextStep Then chkTask(taskIdx).Value = 1
                imgPointer(curWorkPanel).Visible = True
            End If
        Wend
        TaskCheckBusy = False
    End If
End Sub

Private Sub chkLeftImp_Click(Index As Integer)
    If chkLeftImp(Index).Value = 1 Then
        chkLeftImp(Index).Value = 0
    Else
    End If
End Sub

Private Sub chkMain_Click(Index As Integer)
    Dim tmpMainTag As String
    Dim tmpMainFunc As String
    Dim tmpFuncTag As String
    Dim tmpIdx As Integer
    tmpMainTag = chkMain(Index).Tag
    tmpMainFunc = GetItem(tmpMainTag, 4, ",")
    If chkMain(Index).Value = 1 Then
        chkMain(Index).BackColor = LightGreen
        Select Case Index
            Case 8  'Exit
                ShutDownApplication
                chkMain(Index).Value = 0
            Case 9  'Help
                MyMsgBox.UnderConstruction "(Coming soon ...)"
                'fraWorkArea(1).Visible = True
                'Form_Resize
                chkMain(Index).Value = 0
            Case 10 'About
                MyAboutBox.SetLifeStyle = True
                MyAboutBox.VersionTab.LifeStyle = True
                MyAboutBox.Show vbModal, Me
                chkMain(Index).Value = 0
            Case 12 'Filter
                frmFilterDialog.Show
            Case Else
                If curMainButton >= 0 Then
                    CalledFromInternal = True
                    chkMain(curMainButton).Value = 0
                    CalledFromInternal = False
                End If
                curMainButton = Index
                curMainFunction = tmpMainFunc
                ReorgWorkPanel 0
                tmpFuncTag = Trim(fraFuncPanel(curFuncPanel).Tag)
                If tmpFuncTag <> "" Then
                    If Val(tmpFuncTag) < 0 Then
                        tmpMainTag = Trim(chkMain(curMainButton).Tag)
                        tmpIdx = Val(GetItem(tmpMainTag, 2, ","))
                        If tmpIdx >= 0 Then chkWork(tmpIdx).Value = 1
                    End If
                    If curWorkPanel > 0 Then
                        If fraWorkPanel(curWorkPanel).Visible Then
                            fraWorkArea(0).Visible = False
                            AdjustBottomLabels curWorkPanel, TAB1(curWorkPanel), TAB2(curWorkPanel), True
                        Else
                            fraWorkArea(0).Visible = True
                        End If
                    End If
                End If
                Select Case curMainFunction
                    Case "SCENARIO"
                        ReorgSlideShow True
                        If SlideShow = False Then
                            CheckUfisIntroPicture 3, False, 0, "", "c:\ufis\RmsScenarios"
                        End If
                    Case Else
                End Select
        End Select
    Else
        chkMain(Index).BackColor = vbButtonFace
        Select Case Index
            Case 8  'Exit
            Case 9  'Help
                fraWorkArea(1).Visible = False
            Case 10 'About
            Case 12 'Filter
                frmFilterDialog.Hide
            Case Else
                Select Case tmpMainFunc
                    Case "SCENARIO"
                        ReorgSlideShow False
                    Case Else
                End Select
                If curWorkPanel >= 0 Then
                    fraWorkPanel(curWorkPanel).Visible = False
                    ToggleBottomPanel curWorkPanel, False, False, False, False
                End If
                If curFuncPanel >= 0 Then fraFuncPanel(curFuncPanel).Visible = False
                If curMainButton >= 0 Then
                    If curMainButton = Index Then
                        fraTopPanel(0).Caption = ""
                        curMainButton = -1
                        If Not CalledFromInternal Then
                            fraWorkArea(0).Visible = True
                            CheckUfisIntroPicture -1, False, 0, "RmsImpTool_INTRO.bmp", "c:\ufis\system"
                            Form_Resize
                            
                        End If
                    End If
                End If
        End Select
    End If
End Sub

Private Sub chkMidImp_Click(Index As Integer)
    If chkMidImp(Index).Value = 1 Then
        chkMidImp(Index).Value = 0
        TAB2(curWorkPanel).SetFocus
    Else
    End If
End Sub

Private Sub chkPanelIcon_Click(Index As Integer)
    Dim MaxLine As Long
    On Error Resume Next
    If chkPanelIcon(Index).Value = 1 Then
        chkPanelIcon(Index).BackColor = LightGreen
        chkPanelIcon(Index).Refresh
        MaxLine = 0
        MaxLine = MaxLine + TAB1(Index).GetLineCount
        MaxLine = MaxLine + TAB2(Index).GetLineCount
        MaxLine = MaxLine + TAB3(Index).GetLineCount
        If MaxLine > 0 Then
            If MyMsgBox.CallAskUser(0, 0, 0, "Function Control", "Do you want to reset the grids?", "ask", "Yes,No;F", UserAnswer) = 1 Then
                chkImport(Index).Value = 0
                TAB1(Index).ResetContent
                TAB1(Index).Refresh
                TAB2(Index).ResetContent
                TAB2(Index).Refresh
                TAB3(Index).ResetContent
                TAB3(Index).Refresh
                imgPointer(Index).Visible = True
                imgPointer(Index).Top = 30
                imgPointer(Index).Left = 3060
                imgPointer(Index).ToolTipText = "Step 1:"
                imgPointer(Index).Tag = "1"
                ToggleBottomPanel curWorkPanel, False, False, False, True
                chkPanelIcon(Index).Tag = ""
                Me.Refresh
            End If
        End If
        chkPanelIcon(Index).BackColor = vbButtonFace
        chkPanelIcon(Index).Value = 0
    Else
        SetVpfrTagValue Index
    End If
End Sub

Private Sub chkPanelRules_Click(Index As Integer)
    Dim tmpTag As String
    Dim tmpVal As String
    Dim chkIdx As Integer
    Dim i As Integer
    Dim SetEnable As Boolean
    If chkPanelRules(Index).Value = 1 Then
        chkPanelRules(Index).BackColor = LightYellow
        SetEnable = True
    Else
        chkPanelRules(Index).BackColor = vbButtonFace
        SetEnable = False
    End If
    chkPanelRules(Index).Refresh
    tmpTag = chkPanelRules(Index).Tag
    tmpVal = "START"
    i = 0
    While tmpVal <> ""
        i = i + 1
        tmpVal = GetItem(tmpTag, i, ",")
        If tmpVal <> "" Then
            chkIdx = Val(tmpVal)
            chkRule(chkIdx).Enabled = SetEnable
        End If
    Wend
End Sub

Private Sub chkRightImp_Click(Index As Integer)
    Dim tmpTag As String
    Dim tmpMsg As String
    Dim HitCount As Long
    If chkRightImp(Index).Value = 1 Then
        chkRightImp(Index).BackColor = LightGreen
        HitCount = Val(SrvFkeyCnt(Index).Caption)
        If HitCount > 0 Then
            tmpTag = chkRightImp(Index).Tag
            Select Case tmpTag
                Case "INS"
                    tmpMsg = "Do you want to insert" & vbNewLine & HitCount & " records?"
                    If MyMsgBox.CallAskUser(0, 0, 0, "Import Action", tmpMsg, "ask", "Yes,No;F", UserAnswer) = 1 Then
                        Select Case curWorkPanel
                            Case 3
                                HandleSprtabInsert TAB2(curWorkPanel), SrvFkeyCnt(Index).BackColor
                            Case 4
                                HandleDrrtabInsert TAB2(curWorkPanel), SrvFkeyCnt(Index).BackColor
                            Case Else
                        End Select
                    End If
                Case "UPD"
                    tmpMsg = "Do you want to update" & vbNewLine & HitCount & " records?"
                    If MyMsgBox.CallAskUser(0, 0, 0, "Import Action", tmpMsg, "ask", "Yes,No;F", UserAnswer) = 1 Then
                        Select Case curWorkPanel
                            Case 1
                                HandleKeyCardUpdate TAB2(curWorkPanel), SrvFkeyCnt(Index).BackColor
                            Case 4
                                HandleDrrtabUpdate TAB2(curWorkPanel), SrvFkeyCnt(Index).BackColor
                            Case Else
                        End Select
                    End If
                Case "DEL"
                Case "REM"
                Case Else
            End Select
            AdjustBottomLabels curWorkPanel, TAB1(curWorkPanel), TAB2(curWorkPanel), False
        End If
        chkRightImp(Index).Value = 0
    Else
        chkRightImp(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub HandleDrrtabUpdate(CurTab As TABLib.TAB, CurColor As Long)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim LineNo As Long
    Dim LineCount As Long
    Dim newLineNo As Long
    Dim HitCount As String
    Dim tmpBool As String
    Dim lineTag As String
    Dim newFields As String
    Dim newData As String
    Dim newUrno As String
    Dim newTimeStamp As String
    Dim outFields As String
    Dim outData As String
    TAB0(0).ResetContent
    newFields = ""
    outFields = ""
    Dim tmpSTFU As String
    
    CurTab.SetInternalLineBuffer True
    HitCount = CurTab.GetLinesByBackColor(CurColor)
    If Val(HitCount) > 0 Then
        LineNo = CurTab.GetNextResultLine
        While LineNo >= 0
            tmpBool = CurTab.GetFieldValue(LineNo, "LINE")
            If tmpBool = "Y" Then
                lineTag = CurTab.GetLineTag(LineNo)
                tmpSTFU = CurTab.GetFieldValue(LineNo, "SBFR")
                GetKeyItem newFields, lineTag, "{=FIELDS=}", "{="
                outFields = CreateUniqueFieldList(outFields, newFields)
            End If
            LineNo = CurTab.GetNextResultLine
        Wend
    End If
    CurTab.SetInternalLineBuffer False
    If outFields <> "" Then
        outFields = "URNO," & outFields
        CurTab.SetInternalLineBuffer True
        HitCount = CurTab.GetLinesByBackColor(CurColor)
        If Val(HitCount) > 0 Then
            Screen.MousePointer = 11
            LineNo = CurTab.GetNextResultLine
            While LineNo >= 0
                tmpBool = CurTab.GetFieldValue(LineNo, "LINE")
                If tmpBool = "Y" Then
                    newLineNo = InsertEmptyTabLine(TAB0(0), -1)
                    newData = CurTab.GetFieldValues(LineNo, outFields)
                     tmpSTFU = CurTab.GetFieldValue(LineNo, "AVFR")
                    TAB0(0).SetFieldValues newLineNo, outFields, newData
                    ' Bug returns wrong values
                    ' Inserts Wrong Hours
                    lineTag = CurTab.GetLineValues(LineNo)
                    lineTag = CurTab.GetLineTagKeyItem(LineNo, newFields, "{=FIELDS=}", "{=")

                    lineTag = CurTab.GetLineTag(LineNo)
                    GetKeyItem newFields, lineTag, "{=FIELDS=}", "{="
                    GetKeyItem newData, lineTag, "{=DATA=}", "{="
                    TAB0(0).SetFieldValues newLineNo, newFields, newData
                    CurTab.SetFieldValues LineNo, "STAT", "I"
                    CurTab.SetLineColor LineNo, vbBlack, LightYellow
                End If
                LineNo = CurTab.GetNextResultLine
            Wend
            newTimeStamp = GetTimeStamp(0)
            outFields = Mid(outFields, 6)
            newFields = outFields
            outFields = newFields & ",USEU,LSTU"
            outData = "*CMD*,DRRTAB,URT," & CStr(ItemCount(outFields, ",")) & "," & outFields & ",[URNO=:VURNO]" & vbLf
            LineCount = 0
            MaxLine = TAB0(0).GetLineCount - 1
            For CurLine = 0 To MaxLine
                LineCount = LineCount + 1
                If LineCount > 200 Then
                    UfisServer.CallCeda CedaDataAnswer, "REL", "DRRTAB", outFields, outData, "LATE", "", 0, False, False
                    outData = GetItem(outData, 1, vbLf) & vbLf
                    LineCount = 1
                End If
                newData = TAB0(0).GetFieldValues(CurLine, newFields)
                newUrno = TAB0(0).GetFieldValue(CurLine, "URNO")
                newData = CleanNullValues(newData)
                outData = outData & newData & ","
                outData = outData & "RmsImport" & ","
                outData = outData & newTimeStamp & ","
                outData = outData & newUrno & vbLf
            Next
            If LineCount > 0 Then
                UfisServer.CallCeda CedaDataAnswer, "REL", "DRRTAB", outFields, outData, "LATE", "", 0, False, False
            End If
            'UfisServer.CallCeda CedaDataAnswer, "SBC", tmpSqlTab & "/REFR", tmpSqlFld, tmpSqlDat, tmpSqlKey, "", 0, False, False
            CurTab.AutoSizeColumns
            CurTab.Refresh
            Screen.MousePointer = 0
        End If
        CurTab.SetInternalLineBuffer False
    End If
    CurTab.Refresh
    CurTab.SetCurrentSelection CurTab.GetCurrentSelected
    CurTab.SetFocus
    TAB0(0).ResetContent
End Sub

Private Function CreateUniqueFieldList(MainList As String, CheckList As String) As String
    Dim Result As String
    Dim tmpField As String
    Dim i As Integer
    Result = MainList
    i = 0
    tmpField = "START"
    While tmpField <> ""
        i = i + 1
        tmpField = GetItem(CheckList, i, ",")
        If tmpField <> "" Then
            If InStr(Result, tmpField) <= 0 Then Result = Result & "," & tmpField
        End If
    Wend
    If Left(Result, 1) = "," Then Result = Mid(Result, 2)
    CreateUniqueFieldList = Result
End Function

Private Sub HandleDrrtabInsert(CurTab As TABLib.TAB, CurColor As Long)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim LineNo As Long
    Dim newLineNo As Long
    Dim LineCount As Long
    Dim HitCount As String
    Dim tmpBool As String
    Dim newFields As String
    Dim newData As String
    Dim newUrno As String
    Dim newTimeStamp As String
    Dim outFields As String
    Dim outData As String
    TAB0(0).ResetContent
    newFields = newDrrTabFields
    CurTab.SetInternalLineBuffer True
    HitCount = CurTab.GetLinesByBackColor(CurColor)
    If Val(HitCount) > 0 Then
        Screen.MousePointer = 11
        UfisServer.UrnoPoolInit 100
        UfisServer.UrnoPoolPrepare Val(HitCount)
        LineNo = CurTab.GetNextResultLine
        While LineNo >= 0
            tmpBool = CurTab.GetFieldValue(LineNo, "LINE")
            If tmpBool = "Y" Then
                newData = CurTab.GetFieldValues(LineNo, newFields)
                newLineNo = InsertEmptyTabLine(TAB0(0), -1)
                TAB0(0).SetFieldValues newLineNo, newFields, newData
                newUrno = UfisServer.UrnoPoolGetNext
                CurTab.SetFieldValues LineNo, "URNO,STAT", newUrno & ",I"
                TAB0(0).SetFieldValues newLineNo, "URNO", newUrno
                CurTab.SetLineColor LineNo, vbBlack, LightYellow
            End If
            LineNo = CurTab.GetNextResultLine
        Wend
        CurTab.SetInternalLineBuffer False
        newTimeStamp = GetTimeStamp(0)
        outFields = newFields & ",USEC,CDAT,HOPO"
        outData = "*CMD*,DRRTAB,IRT," & CStr(ItemCount(outFields, ",")) & "," & outFields & vbLf
        MaxLine = TAB0(0).GetLineCount - 1
        For CurLine = 0 To MaxLine
            LineCount = LineCount + 1
            If LineCount > 200 Then
                UfisServer.CallCeda CedaDataAnswer, "REL", "DRRTAB", outFields, outData, "LATE", "", 0, False, False
                outData = GetItem(outData, 1, vbLf) & vbLf
                LineCount = 1
            End If
            newData = TAB0(0).GetFieldValues(CurLine, newFields)
            newData = CleanNullValues(newData)
            outData = outData & newData & ","
            outData = outData & "RmsImport" & ","
            outData = outData & newTimeStamp & ","
            outData = outData & HomeAirport & vbLf
        Next
        If LineCount > 0 Then
            UfisServer.CallCeda CedaDataAnswer, "REL", "DRRTAB", outFields, outData, "LATE", "", 0, False, False
        End If
        'UfisServer.CallCeda CedaDataAnswer, "SBC", tmpSqlTab & "/REFR", tmpSqlFld, tmpSqlDat, tmpSqlKey, "", 0, False, False
        CurTab.AutoSizeColumns
        CurTab.Refresh
        Screen.MousePointer = 0
    End If
    CurTab.Refresh
    CurTab.SetCurrentSelection CurTab.GetCurrentSelected
    CurTab.SetFocus
    TAB0(0).ResetContent
End Sub

Private Sub chkRule_Click(Index As Integer)
    Dim tmpTag As String
    Dim tmpTask As String
    Dim tmpData As String
    Dim tmpTabBool As String
    If fraPanelRules(curWorkPanel).Enabled = True Then
        tmpTag = chkRule(Index).Tag
        tmpTask = GetItem(tmpTag, 1, ":")
        tmpData = GetItem(tmpTag, 2, ":")
        Select Case tmpTask
            Case "ABS.DRRCODE"
                If chkRule(Index).Value = 1 Then tmpTabBool = "N" Else tmpTabBool = "Y"
                ToggleDrrtabUpdates TAB2(curWorkPanel), tmpTabBool, tmpData
            Case "ABS.DRRCODE_CLOCK"
                If chkRule(Index).Value = 1 Then tmpTabBool = "N" Else tmpTabBool = "Y"
                ToggleDrrtabUpdatesClock TAB2(curWorkPanel), tmpTabBool
            Case Else
        End Select
    End If
End Sub

Private Sub chkRule_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    chkRule(Index).SetFocus
End Sub

Private Sub CheckValidationRules(SetEnable As Boolean)
    Dim tmpTag As String
    Dim tmpVal As String
    Dim chkIdx As Integer
    Dim i As Integer
    Dim SetVal As Integer
    If Ckb_Interface = Unchecked Then
    
    
    If SetEnable = True Then SetVal = 1 Else SetVal = 0
    fraPanelRules(curWorkPanel).Enabled = SetEnable
    chkPanelRules(curWorkPanel).Value = 1
    tmpTag = chkPanelRules(curWorkPanel).Tag
    tmpVal = "START"
    i = 0
    While tmpVal <> ""
        i = i + 1
        tmpVal = GetItem(tmpTag, i, ",")
        If tmpVal <> "" Then
            chkIdx = Val(tmpVal)
            chkRule(chkIdx).Value = SetVal
        End If
    Wend
    If SetEnable = True Then chkPanelRules(curWorkPanel).Value = 0
    End If
End Sub

Private Sub ToggleDrrtabUpdates(drrTab As TABLib.TAB, SetTabBool As String, CheckList As String)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim tmpAction As String
    Dim tmpType As String
    Dim tmpCode As String
    Dim ForeColor As Long
    Dim BackColor As Long
    ForeColor = vbBlack
    If SetTabBool = "Y" Then BackColor = vbWhite Else BackColor = LightGrey
    MaxLine = drrTab.GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpType = drrTab.GetFieldValue(CurLine, "BSDX")
        If tmpType = "A" Then
            tmpAction = drrTab.GetFieldValue(CurLine, "CMDS")
            If tmpAction = "U" Then
                tmpCode = "," & drrTab.GetFieldValue(CurLine, "SCOD") & ","
                If InStr(CheckList, tmpCode) > 0 Then
                    drrTab.SetFieldValues CurLine, "LINE", SetTabBool
                    drrTab.SetLineColor CurLine, ForeColor, BackColor
                End If
            End If
        End If
    Next
    AdjustBottomLabels curWorkPanel, TAB1(curWorkPanel), drrTab, False
    drrTab.Refresh
    drrTab.SetCurrentSelection drrTab.GetCurrentSelected
End Sub

Private Sub ToggleDrrtabUpdatesClock(drrTab As TABLib.TAB, SetTabBool As String)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim tmpAction As String
    Dim tmpType As String
    Dim tmpCode As String
    Dim ForeColor As Long
    Dim BackColor As Long
    Dim ClockIN As String
    Dim ClockOUT As String
    Dim PENO As String
    
    ForeColor = vbBlack
   
    
    If SetTabBool = "Y" Then BackColor = vbWhite Else BackColor = LightGrey
    MaxLine = drrTab.GetLineCount - 1
    For CurLine = 0 To MaxLine
        ClockIN = ""
        ClockOUT = ""
        tmpType = drrTab.GetFieldValue(CurLine, "BSDX")
        If tmpType = "A" Then
            tmpAction = drrTab.GetFieldValue(CurLine, "CMDS")
            If tmpAction = "U" Then
                tmpCode = "," & drrTab.GetFieldValue(CurLine, "SCOD") & ","
                'Get Clock Times
                
                ClockIN = drrTab.GetFieldValue(CurLine, "CLKI")
                ClockOUT = drrTab.GetFieldValue(CurLine, "CLKO")
                PENO = drrTab.GetFieldValue(CurLine, "PENO")
                
                If (Len(ClockIN) > 0 Or Len(ClockOUT) > 0) Then
                'If InStr(CheckList, tmpCode) > 0 Then
                    drrTab.SetFieldValues CurLine, "LINE", SetTabBool
                    drrTab.SetLineColor CurLine, ForeColor, BackColor
                End If
            End If
        End If
    Next
    AdjustBottomLabels curWorkPanel, TAB1(curWorkPanel), drrTab, False
    drrTab.Refresh
    drrTab.SetCurrentSelection drrTab.GetCurrentSelected
End Sub

Private Sub chkTask_Click(Index As Integer)
    Dim tmpTag As String
    Dim tmpChkList As String
    Dim tmpTask As String
    Dim tmpData As String
    Dim addFields As String
    On Error Resume Next
    tmpTag = chkTask(Index).Tag
    tmpTask = GetItem(tmpTag, 1, ":")
    tmpChkList = GetItem(tmpTag, 2, ":")
    If chkTask(Index).Value = 1 Then
        Screen.MousePointer = 11
        chkTask(Index).BackColor = LightGreen
        chkTask(Index).Refresh
        If tmpChkList <> "" Then chkImport(curWorkPanel).Value = 0
        CheckWorkflow tmpChkList
        Select Case tmpTask
            Case "DUMMY"
            Case "CLOCK.FILE"
                
                'Interface = "DISENABLED"
                'Ckb_Interface.Value = False
                
                
                
                HandleClockImport TAB1(curWorkPanel), TAB3(curWorkPanel), TAB2(curWorkPanel)
                SetWorkflowPointer chkTask(36), "Step 2:", "2"
                ToggleBottomPanel curWorkPanel, True, True, False, True
                AdjustBottomLabels curWorkPanel, TAB1(curWorkPanel), TAB2(curWorkPanel), False
                chkTask(Index).Value = 0
            Case "CLOCK.CHECK"
                HandleClockCheck TAB1(curWorkPanel), TAB3(curWorkPanel), TAB2(curWorkPanel)
                SetWorkflowPointer chkTask(44), "Step 3:", "3"
                AdjustBottomLabels curWorkPanel, TAB1(curWorkPanel), TAB2(curWorkPanel), False
                chkTask(Index).Value = 0
            Case "CLOCK.CLEAR"
                ClearClockingData TAB1(curWorkPanel), TAB2(curWorkPanel)
                SetWorkflowPointer chkTask(21), "Step 4:", "4"
                AdjustBottomLabels curWorkPanel, TAB1(curWorkPanel), TAB2(curWorkPanel), False
                chkTask(Index).Value = 0
            Case "CLOCK.COMP"
                If CheckPanelIcon(curWorkPanel, False) = True Then
                    CheckValidationRules False
                    CompareClockingData TAB1(curWorkPanel), TAB3(curWorkPanel), TAB2(curWorkPanel)
                    imgPointer(curWorkPanel).Visible = True
                    imgPointer(curWorkPanel).Top = 750
                    imgPointer(curWorkPanel).ToolTipText = "Step 5: Import"
                    imgPointer(curWorkPanel).Tag = "5"
                    chkImport(curWorkPanel).Value = 1
                    AdjustBottomLabels curWorkPanel, TAB1(curWorkPanel), TAB2(curWorkPanel), False
                    CheckValidationRules True
                End If
                chkTask(Index).Value = 0
            Case "CARD.FILE"
                'Ckb_Interface.Value = False
                HandleCardImport TAB1(curWorkPanel), TAB2(curWorkPanel)
                SetWorkflowPointer chkTask(35), "Step 2:", "2"
                ToggleBottomPanel curWorkPanel, True, True, False, True
                AdjustBottomLabels curWorkPanel, TAB1(curWorkPanel), TAB2(curWorkPanel), False
                chkTask(Index).Value = 0
            Case "CARD.CHECK"
                HandleCardCheck TAB1(curWorkPanel), TAB2(curWorkPanel)
                SetWorkflowPointer chkTask(18), "Step 3:", "3"
                chkTask(Index).Value = 0
            Case "CARD.CLEAR"
                ClearKeyCardData TAB1(curWorkPanel), TAB2(curWorkPanel)
                SetWorkflowPointer chkTask(17), "Step 4:", "4"
                chkTask(Index).Value = 0
            Case "CARD.COMP"
                If CheckPanelIcon(curWorkPanel, False) = True Then
                    CheckValidationRules False
                    CompareKeyCardData TAB1(curWorkPanel), TAB2(curWorkPanel)
                    imgPointer(curWorkPanel).Visible = True
                    imgPointer(curWorkPanel).Top = 750
                    imgPointer(curWorkPanel).ToolTipText = "Step 7: Import"
                    imgPointer(curWorkPanel).Tag = "7"
                    chkImport(curWorkPanel).Value = 1
                    AdjustBottomLabels curWorkPanel, TAB1(curWorkPanel), TAB2(curWorkPanel), False
                    CheckValidationRules True
                End If
                chkTask(Index).Value = 0
            Case "ABS.ORGA"
                chkMain(12).Value = 1
            Case "ABS.SHRCHECK"
                'Ckb_Interface.Value = False
                If CheckPanelIcon(curWorkPanel, False) = True Then
                    CheckHrAbsData TAB1(curWorkPanel), TAB3(curWorkPanel), TAB2(curWorkPanel)
                    SetWorkflowPointer chkTask(39), "Step 5:", "5"
                    AdjustBottomLabels curWorkPanel, TAB1(curWorkPanel), TAB2(curWorkPanel), False
                End If
                chkTask(Index).Value = 0
            Case "ABS.COMP"
                If CheckPanelIcon(curWorkPanel, False) = True Then
                    CheckValidationRules False
                    CompareHrAbsData TAB1(curWorkPanel), TAB2(curWorkPanel), TAB3(curWorkPanel)
                    imgPointer(curWorkPanel).Visible = True
                    imgPointer(curWorkPanel).Top = 750
                    imgPointer(curWorkPanel).ToolTipText = "Step 7: Import"
                    imgPointer(curWorkPanel).Tag = "7"
                    chkImport(curWorkPanel).Value = 1
                    AdjustBottomLabels curWorkPanel, TAB1(curWorkPanel), TAB2(curWorkPanel), False
                    CheckValidationRules True
                End If
                chkTask(Index).Value = 0
            Case "ABS.CLEAR"
                If CheckPanelIcon(curWorkPanel, False) = True Then
                    ClearHrAbsData TAB1(curWorkPanel), TAB3(curWorkPanel), TAB2(curWorkPanel)
                    SetWorkflowPointer chkTask(12), "Step 6:", "6"
                    AdjustBottomLabels curWorkPanel, TAB1(curWorkPanel), TAB2(curWorkPanel), False
                End If
                chkTask(Index).Value = 0
            Case "ABS.VALID"
                If CheckPanelIcon(curWorkPanel, False) = True Then
                    ValidateHrAbsData TAB1(curWorkPanel), TAB3(curWorkPanel)
                    chkTask(41).Value = 1
                    SetWorkflowPointer chkTask(33), "Step 4:", "4"
                    AdjustBottomLabels curWorkPanel, TAB1(curWorkPanel), TAB2(curWorkPanel), False
                End If
                chkTask(Index).Value = 0
            Case "VPFR"
                CheckVpfrDefault True, ""
                SetWorkflowPointer chkTask(3), "Step 1:", "1"
                chkTask(Index).Value = 0
            Case "VPTO"
                CheckVptoDefault True, ""
                SetWorkflowPointer chkTask(3), "Step 1:", "1"
                chkTask(Index).Value = 0
            Case "TRLR"
                chkAmpel(curWorkPanel).Picture = Ampel(2).Picture
                chkTask(Index).Value = 0
            Case "TRLY"
                chkAmpel(curWorkPanel).Picture = Ampel(1).Picture
                chkTask(Index).Value = 0
            Case "TRLG"
                chkAmpel(curWorkPanel).Picture = Ampel(0).Picture
                chkTask(Index).Value = 0
            Case "DRRTEST"
                TAB1(curWorkPanel).ResetContent
                TAB1(curWorkPanel).ShowVertScroller False
                TAB1(curWorkPanel).ShowHorzScroller False
                TAB1(curWorkPanel).Refresh
                Screen.MousePointer = 11
                TAB1(curWorkPanel).ReadFromFileWithProperties "c:\tmp\HrAbsData.txt"
                TAB1(curWorkPanel).ShowVertScroller True
                TAB1(curWorkPanel).ShowHorzScroller True
                TAB1(curWorkPanel).Refresh
                TAB1(curWorkPanel).SetCurrentSelection 0
                Screen.MousePointer = 0
                chkTask(Index).Value = 0
            Case "ABS.HRVIEW"
                If CheckPanelIcon(curWorkPanel, True) = True Then
                    LoadHrAbsenceView TAB1(curWorkPanel)
                    TAB3(curWorkPanel).SetCurrentSelection 0
                    chkTask(Index).Value = 0
                    chkTask(11).Value = 1
                    SetWorkflowPointer chkTask(40), "Step 2:", "2"
                    TAB3(curWorkPanel).SetFocus
                End If
                chkTask(Index).Value = 0
            Case "ABS.STFTAB"
                If CheckPanelIcon(curWorkPanel, False) = True Then
                    LoadHrStaff TAB3(curWorkPanel), TAB1(curWorkPanel)
                    ArrangeHrAbsSortIndexes TAB1(curWorkPanel), TAB3(curWorkPanel), TAB2(curWorkPanel)
                    TAB3(curWorkPanel).SetCurrentSelection 0
                End If
                chkTask(Index).Value = 0
            Case "ABS.DRRTAB"
                If CheckPanelIcon(curWorkPanel, False) = True Then
                    LoadHrAbsDrrtab TAB2(curWorkPanel), TAB3(curWorkPanel)
                    ArrangeHrAbsSortIndexes TAB1(curWorkPanel), TAB3(curWorkPanel), TAB2(curWorkPanel)
                    TAB3(curWorkPanel).SetCurrentSelection 0
                    SetWorkflowPointer chkTask(33), "Step 4:", "4"
                    End If
                chkTask(Index).Value = 0
            Case "ABS.FUNC"
                If CheckPanelIcon(curWorkPanel, False) = True Then
                    CheckShrFunctions TAB1(curWorkPanel), TAB3(curWorkPanel)
                    SetWorkflowPointer chkTask(42), "Step 3:", "3"
                    ToggleBottomPanel curWorkPanel, True, True, False, True
                    AdjustBottomLabels curWorkPanel, TAB1(curWorkPanel), TAB2(curWorkPanel), False
                End If
                chkTask(Index).Value = 0
            Case "ABS.DETAILS"
                If FormIsVisible("frmDifferences") = False Then
                    If TAB2(curWorkPanel).GetLineCount > 0 Then
                        addFields = "PENO,SDAY"
                        frmDifferences.Show , Me
                        frmDifferences.ShowData TAB2(curWorkPanel), TAB2(curWorkPanel).GetCurrentSelected, addFields
                    Else
                        chkTask(Index).Value = 0
                    End If
                End If
            Case "SPARE"
                DontPlay
                chkTask(Index).Value = 0
            Case "RPT.ONE_CLICK"
                ReportClick tmpTask
                'chkTask(Index).Value = 0
            Case "RPT.NOHIT"
                ReportClick tmpTask
                'chkTask(Index).Value = 0
            Case "RPT.NOHIT_N"
                ReportClick tmpTask
                'chkTask(Index).Value = 0
                Case "RPT.L2L3DIFF"
                ReportClick tmpTask
                'chkTask(Index).Value = 0
            Case Else
                chkTask(Index).Value = 0
        End Select
        Screen.MousePointer = 0
    Else
        chkTask(Index).BackColor = vbButtonFace
        chkTask(Index).Refresh
        Select Case tmpTask
            Case "ABS.ORGA"
                chkMain(12).Value = 0
            Case "ABS.DETAILS"
                If FormIsLoaded("frmDifferences") = True Then
                    Unload frmDifferences
                End If
            Case Else
        End Select
    End If
End Sub

Private Sub ReportClick(ReportName As String)
    Dim rpt As New rptWorkgroups
    Dim rptL2L3 As New rptWorkgroupsL2L3
    Dim preview As New frmPreview
    
    Dim tmpFuncFilter As String
    Dim tmpOrgFilter As String
    
    Dim tmpHeader As String
    Dim tmpLength As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim tmpSqlKey As String
    Dim tmpTableName As String
    
    Dim tmpHourDiff As String
    
    tmpHourDiff = txtHourDiff.Text
    
    
    tmpVpfr = Trim(txtVpfrYY(curWorkPanel).Tag)
    tmpVpto = Trim(txtVptoYY(curWorkPanel).Tag)
    chkPanelIcon(curWorkPanel).Tag = tmpVpfr & "," & tmpVpto
    
    tmpSqlKey = ""
    
    ' From - To Date
    
    If ReportName = "RPT.L2L3DIFF" Then
    If (tmpVpfr <> "") And (tmpVpto <> "") Then
        tmpSqlKey = " lstu between '" & tmpVpfr & "0000'  and '" & tmpVpto & "2359'"
    ElseIf (tmpVpfr <> "") And (tmpVpto = "") Then
        tmpSqlKey = " lstu like '" & tmpVpfr & "%' "
    Else
    End If
    Else
    
    
    If (tmpVpfr <> "") And (tmpVpto <> "") Then
        tmpSqlKey = " sday between '" & tmpVpfr & "'  and '" & tmpVpto & "'"
    ElseIf (tmpVpfr <> "") And (tmpVpto = "") Then
        tmpSqlKey = " sday = '" & tmpVpfr & "' "
    Else
    End If
    End If
    
    
    ' Function , Org Filter
    If chkMain(12).Value = 1 Then
        tmpFuncFilter = frmFilterDialog.tabRightFilter(1).SelectDistinct("0", "'", "'", ",", True)
        tmpOrgFilter = frmFilterDialog.tabRightFilter(0).SelectDistinct("0", "'", "'", ",", True)
         If tmpOrgFilter <> "" Then
            tmpSqlKey = tmpSqlKey & " and sorcode in (" & tmpOrgFilter & ")"
            rpt.txtShowFilter.Text = tmpFuncFilter
            rptL2L3.txtShowFilter.Text = tmpFuncFilter
         End If
         
         If tmpFuncFilter <> "" Then
            tmpSqlKey = tmpSqlKey & " and spfcode in (" & tmpFuncFilter & ")"
            rpt.txtShowFilter.Text = rpt.txtShowFilter.Text & " -- " & tmpFuncFilter
            rptL2L3.txtShowFilter.Text = rptL2L3.txtShowFilter.Text & " -- " & tmpFuncFilter
         End If
         
         
     End If
     
     Select Case ReportName
            Case "RPT.ONE_CLICK"
               ' tmpTableName = "REPORT_ONEHIT"
               tmpTableName = "SHIFT_CHANGE"
                rpt.lblReportName.Caption = "Shift Change"
                
                'tmpSqlKey = tmpSqlKey & " AND "
               
               ' tmpSqlKey = tmpSqlKey & " AND (shiftdiff <= -" & tmpHourDiff & " or  shiftdiff >= " & tmpHourDiff & ") "
               'ATH-RMS
                tmpSqlKey = tmpSqlKey & " AND ( (shiftdiff <= -" & tmpHourDiff & " or  shiftdiff >= " & tmpHourDiff & ") or scod='R' )  "
                
               ' tmpSqlKey = tmpSqlKey & " AND (acti BETWEEN TO_CHAR(to_timestamp(avfr,'YYYY-MM-DD HH24:MI:SS')-45/3600,'YYYYMMDDHH24MISS')"
               ' tmpSqlKey = tmpSqlKey & " AND TO_CHAR(to_timestamp(avto,'YYYY-MM-DD HH24:MI:SS')+45/3600,'YYYYMMDDHH24MISS') )"
               
                
'                tmpSqlKey = tmpSqlKey & " ( "
'                tmpSqlKey = tmpSqlKey & " ( acti  between TO_CHAR(to_timestamp(avfr,'YYYY-MM-DD HH24:MI:SS')-" & tmpHourDiff & "/24,'YYYYMMDDHH24MISS') "
'                tmpSqlKey = tmpSqlKey & "   and TO_CHAR(to_timestamp(avfr,'YYYY-MM-DD HH24:MI:SS')+" & tmpHourDiff & "/24,'YYYYMMDDHH24MISS') "
'                tmpSqlKey = tmpSqlKey & "   and fcol =1 "
'                tmpSqlKey = tmpSqlKey & "        ) "
'                tmpSqlKey = tmpSqlKey & "       or "
'                tmpSqlKey = tmpSqlKey & "       ( acti  between TO_CHAR(to_timestamp(avto,'YYYY-MM-DD HH24:MI:SS')-" & tmpHourDiff & "/24,'YYYYMMDDHH24MISS') "
'                tmpSqlKey = tmpSqlKey & "         and TO_CHAR(to_timestamp(avto,'YYYY-MM-DD HH24:MI:SS')+" & tmpHourDiff & "/24,'YYYYMMDDHH24MISS') "
'                tmpSqlKey = tmpSqlKey & "         and fcol =0 "
'                tmpSqlKey = tmpSqlKey & "        ) "
'                tmpSqlKey = tmpSqlKey & "    ) "
'                tmpSqlKey = tmpSqlKey & "  and stfu || sday  IN (SELECT DISTINCT report_onehit_noacti.stfu || report_onehit_noacti.sday"
'                tmpSqlKey = tmpSqlKey & " From report_onehit_noacti where "
'
'                If (tmpVpfr <> "") And (tmpVpto <> "") Then
'                    tmpSqlKey = tmpSqlKey & " report_onehit_noacti.sday between '" & tmpVpfr & "'  and '" & tmpVpto & "'"
'                ElseIf (tmpVpfr <> "") And (tmpVpto = "") Then
'                    tmpSqlKey = tmpSqlKey & " report_onehit_noacti.sday = '" & tmpVpfr & "' "
'
'                End If
                'tmpSqlKey = tmpSqlKey & " ) "
                'tmpSqlKey = tmpSqlKey & " order by sday,peno "
            Case "RPT.NOHIT"
                tmpTableName = "REPORT_NOHIT"
                rpt.lblReportName.Caption = "Report NO HIT"
                tmpSqlKey = tmpSqlKey & " AND stfu not in ( "
                tmpSqlKey = tmpSqlKey & " select sp.ustf from sprtab sp where  "
                tmpSqlKey = tmpSqlKey & " acti like sday || '%' "
                tmpSqlKey = tmpSqlKey & " or ( "
                tmpSqlKey = tmpSqlKey & " sp.acti "
                tmpSqlKey = tmpSqlKey & " Between TO_CHAR"
                tmpSqlKey = tmpSqlKey & " (TO_TIMESTAMP (avfr,'YYYY-MM-DD HH24:MI:SS') -" & tmpHourDiff & "/24,'YYYYMMDDHH24MISS')"
                tmpSqlKey = tmpSqlKey & " AND TO_CHAR"
                tmpSqlKey = tmpSqlKey & " (TO_TIMESTAMP (avto,'YYYY-MM-DD HH24:MI:SS') +" & tmpHourDiff & "/24,'YYYYMMDDHH24MISS')"
                tmpSqlKey = tmpSqlKey & " ) "
                tmpSqlKey = tmpSqlKey & " ) "
                If (tmpVpto <> "") Then
                'tmpSqlKey = tmpSqlKey & " and ( '" & tmpVpto & Hour(Now) & Minute(Now) & "' between avfr and avto or ( '" & tmpVpto & Hour(Now) & Minute(Now) & "' <= avfr and '" & tmpVpto & Hour(Now) & Minute(Now) & "' <= avto  ))"
                tmpSqlKey = tmpSqlKey & " and ( '" & tmpVpto & Format(Now, "hhmm") & "' between avfr and avto  )"
                Else
                'tmpSqlKey = tmpSqlKey & " and ( '" & tmpVpfr & Hour(Now) & Minute(Now) & "' between avfr and avto or ( '" & tmpVpfr & Hour(Now) & Minute(Now) & "' <= avfr and '" & tmpVpfr & Hour(Now) & Minute(Now) & "' <= avto ))"
                tmpSqlKey = tmpSqlKey & " and ( '" & tmpVpfr & Format(Now, "hhmm") & "' between avfr and avto )"
                End If
                tmpSqlKey = tmpSqlKey & " order by sday,lanm "
            Case "RPT.NOHIT_N"
                tmpTableName = "REPORT_NOHIT"
                rpt.lblReportName.Caption = "Report NO HIT Nights or Holidays"
                tmpSqlKey = tmpSqlKey & " AND (   scod IN ('N', 'N1', 'N2', 'N3', 'N4', 'N5', 'N6', 'N9') "
                tmpSqlKey = tmpSqlKey & " OR sday || '000000' IN (SELECT hday FROM holtab) "
                ' Add Sunday / Saturday in the Holidays
                '1 = Sunday
                '7 = Saturday
                tmpSqlKey = tmpSqlKey & " OR '1' in ( Select to_char(to_date(sday, 'YYYYMMDD'), 'D') from DUAL )   "
                tmpSqlKey = tmpSqlKey & " OR '7' in ( Select to_char(to_date(sday, 'YYYYMMDD'), 'D') from DUAL )   "
                
                tmpSqlKey = tmpSqlKey & " ) "
                tmpSqlKey = tmpSqlKey & " AND stfu not in ( "
                tmpSqlKey = tmpSqlKey & " select sp.ustf from sprtab sp where  "
                tmpSqlKey = tmpSqlKey & " acti like sday || '%' "
                tmpSqlKey = tmpSqlKey & " or ( "
                tmpSqlKey = tmpSqlKey & " sp.acti "
                tmpSqlKey = tmpSqlKey & " Between TO_CHAR"
                tmpSqlKey = tmpSqlKey & " (TO_TIMESTAMP (avfr,'YYYY-MM-DD HH24:MI:SS') -" & tmpHourDiff & "/24,'YYYYMMDDHH24MISS')"
                tmpSqlKey = tmpSqlKey & " AND TO_CHAR"
                tmpSqlKey = tmpSqlKey & " (TO_TIMESTAMP (avto,'YYYY-MM-DD HH24:MI:SS') +" & tmpHourDiff & "/24,'YYYYMMDDHH24MISS')"
                tmpSqlKey = tmpSqlKey & " ) "
                tmpSqlKey = tmpSqlKey & " ) "
                tmpSqlKey = tmpSqlKey & " order by sday,lanm "
                
             Case "RPT.L2L3DIFF"
                tmpTableName = "L2L3DIFF"
                rpt.lblReportName.Caption = "Report L2,L3 Differences "
                rptL2L3.lblReportName.Caption = "Report L2,L3 Differences "
                tmpSqlKey = tmpSqlKey & " order by sday,lanm "
             
     
     End Select
     
    rpt.txtPeriod.Text = " From " & tmpVpfr & " to " & tmpVpto
    rptL2L3.txtPeriod.Text = " From " & tmpVpfr & " to " & tmpVpto
    tmpSqlKey = "where " & tmpSqlKey
    If ReportName = "RPT.L2L3DIFF" Then
        frmHiddenData.LoadReportData frmHiddenData.rpt, "RPT", tmpTableName, tmpSqlKey
        preview.Caption = rptL2L3.lblReportName.Caption
        Set preview.ARViewer.ReportSource = rptL2L3

    Else
        frmHiddenData.LoadReportData frmHiddenData.rpt, "RPT", tmpTableName, tmpSqlKey
        preview.Caption = rpt.lblReportName.Caption
        Set preview.ARViewer.ReportSource = rpt

    End If
    Debug.Print tmpSqlKey
    preview.Show
    'SetFormOnTop preview, True
End Sub

Private Sub chkTask_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim tmpTag As String
    Dim tmpTask As String
    If Button = 2 Then
        tmpTag = chkTask(Index).Tag
        tmpTask = GetItem(tmpTag, 1, ":")
        If chkTask(Index).Value = 1 Then
            Select Case tmpTask
                Case "ABS.ORGA"
                    frmFilterDialog.Show
                Case Else
            End Select
        Else
        End If
    End If
End Sub

Private Function CheckPanelIcon(Index As Integer, FirstCall As Boolean) As Boolean
    Dim Result As Boolean
    Result = False
    If FirstCall = True Then chkPanelIcon(Index).Value = 1
    If chkPanelIcon(Index).Tag = "" Then Result = True
    If chkPanelIcon(Index).BackColor = vbButtonFace Then Result = True
    CheckPanelIcon = Result
End Function

Private Sub SetWorkflowPointer(chkBox As CheckBox, MyToolTip As String, StepNumber As String)
    On Error Resume Next
    imgPointer(curWorkPanel).Visible = True
    imgPointer(curWorkPanel).Top = 30
    imgPointer(curWorkPanel).Left = chkBox.Left + ((chkBox.Width - imgPointer(curWorkPanel).Width) / 2) - 30
    imgPointer(curWorkPanel).ToolTipText = MyToolTip
    imgPointer(curWorkPanel).Tag = StepNumber
End Sub
Private Sub CheckShrFunctions(shrTab As TABLib.TAB, stfTab As TABLib.TAB)
    CheckShrAbsences shrTab, stfTab
End Sub

Private Sub CompareHrAbsData(shrTab As TABLib.TAB, drrTab As TABLib.TAB, stfTab As TABLib.TAB)
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim LineNo As Long
    Dim HitCount As Long
    Dim HitLine As Long
    Dim stfHitCount As Long
    Dim stfHitLine As Long
    Dim ColNo As Long
    Dim newLineNo As Long
    Dim tmpBool As String
    Dim tmpImfr As String
    Dim tmpImto As String
    Dim tmpSTFU As String
    Dim tmpPeno As String
    Dim tmpStfPeno As String
    Dim tmpHrsd As String
    Dim tmpSday As String
    Dim tmpShrBsdu As String
    Dim tmpDrrBsdu As String
    Dim tmpSdac As String
    Dim tmpSbfr As String
    Dim tmpCmds As String
    Dim tmpAction As String
    Dim tmpBsdx As String
    Dim tmpLine As String
    Dim newFields As String
    Dim newData As String
    Dim updFields As String
    Dim updData As String
    Dim tmpName As String
    Dim shrUpdate As Boolean
    Dim i As Integer
    Dim fromDay
    Dim toDay
    Dim curDay
    TAB0(0).ResetContent
    drrTab.SetInternalLineBuffer True
    MaxLine = shrTab.GetLineCount - 1
    For CurLine = 0 To MaxLine
        shrUpdate = False
        tmpBool = shrTab.GetColumnValue(CurLine, 0)
        If tmpBool = "Y" Then
            tmpSTFU = shrTab.GetFieldValue(CurLine, "STFU")
            tmpPeno = shrTab.GetFieldValue(CurLine, "PENO")
            tmpSdac = shrTab.GetFieldValue(CurLine, "SDAC")
            tmpImfr = shrTab.GetFieldValue(CurLine, "IMFR")
            tmpImto = shrTab.GetFieldValue(CurLine, "IMTO")
            tmpShrBsdu = shrTab.GetFieldValue(CurLine, "ODAU")
            tmpStfPeno = GetLookupFieldValues(stfTab, "URNO", tmpSTFU, "PENO", stfHitCount, stfHitLine)
            fromDay = CedaDateToVb(tmpImfr)
            toDay = CedaDateToVb(tmpImto)
            curDay = fromDay
            While curDay <= toDay
                tmpSday = Format(curDay, "yyyymmdd")
                tmpHrsd = tmpSTFU & "-" & tmpSday
                tmpLine = drrTab.GetLinesByIndexValue("HRSD", tmpHrsd, 0)
                HitCount = Val(tmpLine)
                If HitCount > 0 Then
                    LineNo = 1
                    While LineNo >= 0
                        LineNo = drrTab.GetNextResultLine
                        If LineNo >= 0 Then
                            tmpAction = drrTab.GetFieldValue(LineNo, "CMDS")
                            If tmpAction <> "I" Then
                                tmpCmds = "-"
                                tmpBsdx = "?"
                                'Compare existing record
                                tmpSbfr = drrTab.GetFieldValue(LineNo, "SBFR")
                                If Left(tmpSbfr, 4) = "1899" Then
                                    'Found absence
                                    tmpBsdx = "A"
                                    tmpDrrBsdu = drrTab.GetFieldValue(LineNo, "BSDU")
                                    updFields = CheckAbsRecUpdates(drrTab, LineNo, tmpSdac, tmpShrBsdu, drrTab.LogicalFieldList)
                                    If updFields <> "" Then tmpCmds = "U"
                                Else
                                    'found regular shift
                                    tmpBsdx = "S"
                                    updFields = CheckShiftRecUpdates(drrTab, LineNo, tmpSday, tmpSdac, tmpShrBsdu, drrTab.LogicalFieldList)
                                    tmpCmds = "U"
                                End If
                                SetTabCellObjects drrTab, LineNo, "DECO", "CellDiffGry", updFields, True
                                Select Case tmpCmds
                                    Case "U"
                                        updData = "Y,A,U"
                                        drrTab.SetLineColor LineNo, vbBlack, vbWhite
                                        shrUpdate = True
                                    Case "-"
                                        updData = "N,A,-"
                                        drrTab.SetLineColor LineNo, vbBlack, LightGrey
                                    Case Else
                                End Select
                                updFields = "LINE,STAT,CMDS,BSDX"
                                updData = updData & "," & tmpBsdx
                                drrTab.SetFieldValues LineNo, updFields, updData
                            End If
                        End If
                    Wend
                Else
                    'Insert new absence
                    newLineNo = InsertEmptyTabLine(TAB0(0), -1)
                    newData = "Y,A,I," & tmpSday & "," & tmpSTFU & ","
                    newData = newData & tmpSday & "083000" & ","
                    newData = newData & tmpSday & "083000" & ","
                    newData = newData & tmpHrsd & ","
                    newData = newData & tmpShrBsdu & ","
                    newData = newData & tmpSdac & ","
                    newData = newData & " " & ","
                    newData = newData & "18991230000000" & ","
                    newData = newData & "18991230000000" & ","
                    newData = newData & "1" & ","
                    newData = newData & "0000" & ","
                    newData = newData & "A" & ","
                    'newData = newData & "2" & ","
                    newData = newData & AbsencesLevel & ","
                    'newData = newData & "1" & ","
                    newData = newData & "1" & ","
                    newData = newData & tmpPeno & ","
                    newFields = "LINE,STAT,CMDS,SDAY,STFU,AVFR,AVTO,HRSD,BSDU,SCOD,SCOO,SBFR,SBTO,DRRN,SBLU,ROSS,ROSL,DRS2,PENO"
                    'newDrrTabFields = "SDAY,STFU,AVFR,AVTO,BSDU,SCOD,SCOO,SBFR,SBTO,DRRN,SBLU,ROSS,ROSL,DRS2,URNO"
                    TAB0(0).SetFieldValues newLineNo, newFields, newData
                    shrUpdate = True
                End If
                curDay = DateAdd("d", 1, curDay)
            Wend
            If shrUpdate = True Then
                updFields = "FUNC,SDAC"
                SetTabCellObjects shrTab, CurLine, "DECO", "CellDiffGry", updFields, True
                SetTabCellObjects stfTab, stfHitLine, "DECO", "CellDiffGry", "PENO,FUNC", True
            End If
        End If
    Next
    drrTab.SetInternalLineBuffer False
    
    TAB0(0).AutoSizeColumns
    TAB0(0).Refresh
    SetTabIndexes drrTab, "STFU,HRSD", "STFU,HRSD", False
    MaxLine = TAB0(0).GetLineCount - 1
    For CurLine = 0 To MaxLine
        newData = TAB0(0).GetFieldValues(CurLine, TAB0(0).LogicalFieldList)
        newLineNo = InsertEmptyTabLine(drrTab, -1)
        drrTab.SetFieldValues newLineNo, drrTab.LogicalFieldList, newData
        drrTab.SetLineColor newLineNo, vbBlack, vbGreen
    Next
    TAB0(0).ResetContent
    TAB0(0).Refresh
    
    SetTabSortCols drrTab, "AVFR,PENO"
    SetTabIndexes drrTab, "STFU,HRSD", "STFU,HRSD", True
    drrTab.Refresh
    
    ToggleBottomPanel curWorkPanel, True, True, False, True
    AdjustBottomLabels curWorkPanel, shrTab, drrTab, False
    stfTab.Refresh
    drrTab.Refresh
    shrTab.Refresh
End Sub

Private Function CheckAbsRecUpdates(drrTab As TABLib.TAB, LineNo As Long, AbsCode As String, AbsUrno As String, CheckFields As String) As String
    Dim i As Integer
    Dim ColNo As Long
    Dim tmpField As String
    Dim tmpValue As String
    Dim tmpUpdFields As String
    Dim tmpUpdValues As String
    Dim tmpLineTag As String
    
    tmpUpdFields = ""
    tmpUpdValues = ""
    tmpLineTag = ""
    i = 0
    tmpField = "START"
    While tmpField <> ""
        i = i + 1
        tmpField = GetItem(CheckFields, i, ",")
        If tmpField <> "" Then
            ColNo = GetRealItemNo(drrTab.LogicalFieldList, tmpField)
            If ColNo >= 0 Then
                Select Case tmpField
                    Case "SCOD"
                        tmpValue = drrTab.GetFieldValue(LineNo, tmpField)
                        If tmpValue <> AbsCode Then
                            tmpUpdFields = tmpUpdFields & "," & tmpField
                            tmpUpdValues = tmpUpdValues & "," & AbsCode
                        End If
                    Case "BSDU"
                        tmpValue = drrTab.GetFieldValue(LineNo, tmpField)
                        If tmpValue <> AbsUrno Then
                            tmpUpdFields = tmpUpdFields & "," & tmpField
                            tmpUpdValues = tmpUpdValues & "," & AbsUrno
                        End If
                    Case Else
                End Select
            End If
        End If
    Wend
    tmpUpdFields = Mid(tmpUpdFields, 2)
    tmpUpdValues = Mid(tmpUpdValues, 2)
    If tmpUpdFields <> "" Then tmpLineTag = "{=FIELDS=}" & tmpUpdFields & "{=DATA=}" & tmpUpdValues
    drrTab.SetLineTag LineNo, tmpLineTag
    CheckAbsRecUpdates = tmpUpdFields
End Function

Private Function CheckShiftRecUpdates(drrTab As TABLib.TAB, LineNo As Long, CurSday As String, AbsCode As String, AbsUrno As String, CheckFields As String) As String
    Dim i As Integer
    Dim ColNo As Long
    Dim tmpField As String
    Dim tmpOldValue As String
    Dim tmpNewValue As String
    Dim tmpUpdFields As String
    Dim tmpUpdValues As String
    Dim tmpLineTag As String
    
    ' Updated from GFO to solve the Hour Problem
'    tmpUpdFields = ",SBFR,SBTO,SBLU"
'    tmpUpdValues = ",18991230000000,18991230000000,15"
'    tmpUpdFields = ",SBFR,SBTO"
    'tmpUpdValues = ",18991230000000,18991230000000"
    
    tmpLineTag = ""
    i = 0
    tmpField = "START"
    While tmpField <> ""
        i = i + 1
        tmpField = GetItem(CheckFields, i, ",")
        If tmpField <> "" Then
            ColNo = GetRealItemNo(drrTab.LogicalFieldList, tmpField)
            If ColNo >= 0 Then
                tmpNewValue = ""
                tmpOldValue = drrTab.GetFieldValue(LineNo, tmpField)
                Select Case tmpField
                    Case "SBFR"
                    ' Updated from GFO to solve the Hour Problem
'                        tmpNewValue = "1899123" & Right(tmpOldValue, 6)
                        tmpNewValue = tmpOldValue
                        tmpUpdFields = tmpUpdFields & "," & "SBFR"
                        tmpUpdValues = tmpUpdValues & "," & tmpOldValue
                    Case "SBTO"
                    ' Updated from GFO to solve the Hour Problem
'                         tmpNewValue = "1899123" & Right(tmpOldValue, 6)
                        tmpNewValue = tmpOldValue
                        tmpUpdFields = tmpUpdFields & "," & "SBTO"
                        tmpUpdValues = tmpUpdValues & "," & tmpOldValue
                    Case "SBLU"
                    ' Updated from GFO to solve the Hour Problem
                        tmpNewValue = tmpOldValue
                        tmpUpdFields = tmpUpdFields & "," & "SBLU"
                        tmpUpdValues = tmpUpdValues & "," & tmpOldValue
                    Case "AVFR"
                    ' Updated from GFO to solve the Hour Problem
                    '    tmpNewValue = CurSday & "083000"
                    '    tmpNewValue = CurSday & Right(tmpOldValue, 6)
                    tmpNewValue = tmpOldValue
                        If tmpOldValue = tmpNewValue Then tmpNewValue = ""
                    Case "AVTO"
                    ' Updated from GFO to solve the Hour Problem
                     '   tmpNewValue = CurSday & "083000"
                        'tmpNewValue = CurSday & Right(tmpOldValue, 6)
                        tmpNewValue = tmpOldValue
                        If tmpOldValue = tmpNewValue Then tmpNewValue = ""
                    Case "SCOD"
                        If tmpOldValue <> AbsCode Then
                            tmpUpdFields = tmpUpdFields & "," & "SCOO"
                            tmpUpdValues = tmpUpdValues & "," & tmpOldValue
                            tmpNewValue = AbsCode
                        End If
                    Case "BSDU"
                        If tmpOldValue <> AbsUrno Then
                            tmpNewValue = AbsUrno
                        End If
                    Case "ROSS"
                        If tmpOldValue <> "A" Then
                            tmpNewValue = "A"
                        End If
                    Case "ROSL"
                        If tmpOldValue <> "2" Then
                        ' GFO Added
                        'The Absense should go to the
                        'Request Level
                        ' To be check what is the Request Level
                        ' tmpNewValue = "2"
                        
                            tmpNewValue = AbsencesLevel
                        End If
                    Case "FCTC"
                        If tmpOldValue <> "" Then
                            tmpNewValue = " "
                        End If
                        Case "DRRN"
                        If tmpOldValue <> "1" Then
                            'tmpNewValue = "1"
                        End If
                    Case "DRS2"
                        If tmpOldValue <> "1" Then
                            tmpNewValue = "1"
                        End If
                    Case Else
                End Select
                If tmpNewValue <> "" Then
                    tmpUpdFields = tmpUpdFields & "," & tmpField
                    tmpUpdValues = tmpUpdValues & "," & Trim(tmpNewValue)
                End If
            End If
        End If
    Wend
    tmpUpdFields = Mid(tmpUpdFields, 2)
    tmpUpdValues = Mid(tmpUpdValues, 2)
    If tmpUpdFields <> "" Then tmpLineTag = "{=FIELDS=}" & tmpUpdFields & "{=DATA=}" & tmpUpdValues
    drrTab.SetLineTag LineNo, tmpLineTag
    CheckShiftRecUpdates = tmpUpdFields
End Function

Private Sub LoadHrAbsenceView(CurTab As TABLib.TAB)
    Static SHRAbsviewServer As String
    Dim CdrAnsw As Boolean
    Dim tmpCmd As String
    Dim tmpTable As String
    Dim tmpFields As String
    Dim tmpGridFields As String
    Dim tmpSqlKey As String
    Dim tmpFuncFilter As String
    Dim tmpHeader As String
    Dim tmpLength As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim tmpData As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim tmpLen As Integer
    Dim AbsHrvMemo As String
    
    tmpCmd = "RTA"
    tmpTable = "rms.emp_leaves_rms@rms_to_shr"
    tmpFields = "'Y',' ',' ',emp_id,' ',func,to_char(from_date,'yyyymmdd'),to_char(to_date,'yyyymmdd'),leave_id,' ',period,leave_memo,' ',' ',' ',' ',' '"
   '  tmpFields = "'Y',' ',' ',emp_id,' ',func,to_char(from_date,'yyyymmdd'),to_char(to_date,'yyyymmdd'),leave_id,' ',leave_memo,' ',' ',' ',' ',' '"
   tmpGridFields = "LINE,STAT,CMDS,PENO,DEPT,FUNC,VPFR,VPTO,LEAV,SDAC,PERIOD,MEMO,IMFR,IMTO,STFU,ODAU,RMRK"
   ' tmpGridFields = "LINE,STAT,CMDS,PENO,DEPT,FUNC,VPFR,VPTO,LEAV,SDAC,MEMO,IMFR,IMTO,STFU,ODAU,RMRK"
    AbsHrvEmpId = GetRealItemNo(tmpGridFields, "PENO")
    AbsHrvVpfr = GetRealItemNo(tmpGridFields, "VPFR")
    AbsHrvVpto = GetRealItemNo(tmpGridFields, "VPTO")
    AbsHrvMemo = GetRealItemNo(tmpGridFields, "MEMO")
    
    tmpVpfr = Trim(txtVpfrYY(curWorkPanel).Tag)
    tmpVpto = Trim(txtVptoYY(curWorkPanel).Tag)
    chkPanelIcon(curWorkPanel).Tag = tmpVpfr & "," & tmpVpto
    
    tmpSqlKey = ""
    If (tmpVpfr <> "") And (tmpVpto <> "") Then
        tmpSqlKey = "((to_date >= to_date('" & tmpVpfr & "','yyyymmdd')) "
        tmpSqlKey = tmpSqlKey & "and (from_date <= to_date('" & tmpVpto & "','yyyymmdd')))"
    ElseIf tmpVpfr <> "" Then
        tmpSqlKey = "(to_date >= to_date('" & tmpVpfr & "','yyyymmdd'))"
        'tmpSqlKey = "where (from_date >= to_date('" & tmpVpfr & "','yyyymmdd')) "
        'tmpSqlKey = tmpSqlKey & "or (to_date >= to_date('" & tmpVpfr & "','yyyymmdd'))"
    Else
    End If
    
    If tmpSqlKey <> "" Then
        If chkMain(12).Value = 1 Then
            tmpFuncFilter = frmFilterDialog.tabRightFilter(1).SelectDistinct("0", "'", "'", ",", True)
            If tmpFuncFilter <> "" Then
                tmpSqlKey = tmpSqlKey & " and func in (" & tmpFuncFilter & ")"
            End If
        End If
        tmpSqlKey = "where " & tmpSqlKey
        tmpHeader = "L,S,A,PENO,DEPT,FUNC,FROM,TO DATE,LEAVE,COD,PERIOD,MEMO,IMFR,IMTO,STFU,ODAU,RMRK"
        'tmpHeader = "L,S,A,PENO,DEPT,FUNC,FROM,TO DATE,LEAVE,COD,MEMO,IMFR,IMTO,STFU,ODAU,RMRK"
        tmpLength = "10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10"
        
        CurTab.ResetContent
        CurTab.ShowVertScroller False
        CurTab.ShowHorzScroller False
        CurTab.HeaderString = tmpHeader
        CurTab.HeaderLengthString = tmpLength
        CurTab.LogicalFieldList = tmpGridFields
        CurTab.SetMainHeaderValues "3,8,5", "Line,SHR Absence Records,RMS Import Data", ",,"
        CurTab.SetMainHeaderFont 17, False, False, True, 0, "Arial"
        
        
        CurTab.AutoSizeByHeader = True
        CurTab.AutoSizeColumns
        CurTab.Refresh
        Screen.MousePointer = 11
        
        CurTab.CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
        CurTab.CedaHopo = UfisServer.HOPO
        CurTab.CedaIdentifier = ""
        CurTab.CedaPort = "3357"
        CurTab.CedaReceiveTimeout = "250"
        CurTab.CedaRecordSeparator = vbLf
        CurTab.CedaSendTimeout = "250"
        If SHRAbsviewServer = "" Then SHRAbsviewServer = GetIniEntry(myIniFile, "SHR_ABSENCES", "", "READ_FROM_SERVER", UfisServer.HostName)
        CurTab.CedaServerName = SHRAbsviewServer
        CurTab.CedaTabext = UfisServer.TblExt
        CurTab.CedaUser = UfisServer.ModName
        CurTab.CedaWorkstation = UfisServer.GetMyWorkStationName
        
        If CedaIsConnected Then
            CdrAnsw = CurTab.CedaAction(tmpCmd, tmpTable, tmpFields, "", tmpSqlKey)
        ElseIf UfisServer.HostName = "LOCAL" Then
            CurTab.ReadFromFile "c:\tmp\RmsImportTab1_4.txt"
        End If
        
        'The remark field (MEMO) of SHR Sstem
        'sometimes contains linefeed characters,
        'which create empty lines in CurTab.
        'Thus we must clear out all those lines.
        '(Otherwise TAB will crash after a while.)
        ClearTabData CurTab, "PENO", "", "", ""
                
        SetDateTimeFormat CurTab, "VPFR,VPTO,IMFR,IMTO", "YYYYMMDD", MyDateFormat
        
        CurTab.ShowVertScroller True
        CurTab.ShowHorzScroller True
        CurTab.AutoSizeByHeader = True
        CurTab.AutoSizeColumns
        CurTab.Refresh
       
        minAbsSday = "99999999"
        maxAbsSday = "00000000"
        MaxLine = CurTab.GetLineCount - 1
        Dim tmpMemo As String
        
        For CurLine = 0 To MaxLine
            'tmpMemo = StrConv(CurTab.GetColumnValue(CurLine, AbsHrvMemo), vbFromUnicode)
            
            'CurTab.SetColumnValue CurLine, AbsHrvMemo, tmpMemo
            
            tmpData = CurTab.GetColumnValue(CurLine, AbsHrvEmpId)
            tmpLen = Len(tmpData)
            If tmpLen < 4 Then
                tmpData = Right("0000" & tmpData, 4)
                CurTab.SetColumnValue CurLine, AbsHrvEmpId, tmpData
            Else
            End If
            tmpData = CurTab.GetColumnValue(CurLine, AbsHrvVpfr)
            If tmpData < minAbsSday Then minAbsSday = tmpData
            tmpData = CurTab.GetColumnValue(CurLine, AbsHrvVpto)
            If tmpData > maxAbsSday Then maxAbsSday = tmpData
        Next
        If minAbsSday < tmpVpfr Then minAbsSday = tmpVpfr
        If (tmpVpto <> "") And (maxAbsSday > tmpVpto) Then maxAbsSday = tmpVpto
        CurTab.AutoSizeColumns
        CurTab.Refresh
        Screen.MousePointer = 0
    Else
    End If
End Sub

Private Sub LoadHrStaff(CurTab As TABLib.TAB, absTab As TABLib.TAB)
    Dim CdrAnsw As Boolean
    Dim tmpCmd As String
    Dim tmpTable As String
    Dim tmpFields As String
    Dim tmpGridFields As String
    Dim tmpSqlKey As String
    Dim tmpHeader As String
    Dim tmpLength As String
    Dim tmpData As String
    Dim tmpFunc As String
    Dim tmpUrno As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim tmpLen As Integer
    
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim SPFTAB_tmpsql As String
    
    tmpVpfr = Trim(txtVpfrYY(curWorkPanel).Tag) & "000000"
    tmpVpto = Trim(txtVptoYY(curWorkPanel).Tag)
    SPFTAB_tmpsql = "(('" & tmpVpfr & "' between spftab.vpfr and  spftab.vpto) or (spftab.vpfr<= '" & tmpVpfr & "' and  spftab.vpto=' '  )) and (spftab.prio='1') "
    ' Changed By GFO 1/12/2006
    ' Load Functions For Specific Date
    frmHiddenData.LoadBasicData frmHiddenData.SPFTAB, "SPF", "", " WHERE " & SPFTAB_tmpsql
    
    tmpCmd = "RTA"
    tmpTable = "STFTAB"
    tmpFields = "'Y',' ',' ',PENO,PERC,' ',' ',LANM,FINM,KIND,SHNM,DOEM,DODM,URNO,' ',' ',' '"
    tmpGridFields = "LINE,STAT,CMDS,PENO,PERC,DEPT,FUNC,LANM,FINM,KIND,SHNM,DOEM,DODM,URNO,' ',' ',' '"
    AbsStfPeno = GetRealItemNo(tmpGridFields, "PENO")
    AbsStfUrno = GetRealItemNo(tmpGridFields, "URNO")
    
    tmpData = absTab.SelectDistinct(CStr(AbsHrvEmpId), "'", "'", ",", True)
    If tmpData <> "" Then
        tmpSqlKey = "WHERE PENO IN (" & tmpData & ")"
        StaffOnly = False
    Else
        'If Trim(txtFuncFilter(4).Text) = "" Then
        '    tmpSqlKey = ""
        '    StaffOnly = True
        'Else
            tmpSqlKey = "WHERE PENO IN ('....')"
            StaffOnly = False
        'End If
    End If
    
    tmpHeader = "L,S,A,PENO,PERC,DEPT,FUNC,LANM,FINM,G,SHNM,DOEM,DODM,URNO,1,2,3"
    tmpLength = "10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10"
    
    CurTab.ResetContent
    CurTab.ShowVertScroller False
    CurTab.ShowHorzScroller False
    CurTab.HeaderString = tmpHeader
    CurTab.HeaderLengthString = tmpLength
    CurTab.LogicalFieldList = tmpGridFields
    CurTab.SetMainHeaderValues "3,4,4,2,3", "Line,Identifier,Personal Data,Employed,System", ",,,,"
    CurTab.SetMainHeaderFont 17, False, False, True, 0, "Arial"
    CurTab.AutoSizeByHeader = True
    CurTab.AutoSizeColumns
    CurTab.Refresh
    Screen.MousePointer = 11
    
    CurTab.CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
    CurTab.CedaHopo = UfisServer.HOPO
    CurTab.CedaIdentifier = ""
    CurTab.CedaPort = "3357"
    CurTab.CedaReceiveTimeout = "250"
    CurTab.CedaRecordSeparator = vbLf
    CurTab.CedaSendTimeout = "250"
    CurTab.CedaServerName = UfisServer.HostName
    CurTab.CedaTabext = UfisServer.TblExt
    CurTab.CedaUser = UfisServer.ModName
    CurTab.CedaWorkstation = UfisServer.GetMyWorkStationName
    
    If CedaIsConnected Then
    
        CdrAnsw = CurTab.CedaAction(tmpCmd, tmpTable, tmpFields, "", tmpSqlKey)
    ElseIf UfisServer.HostName = "LOCAL" Then
        CurTab.ReadFromFile "c:\tmp\RmsImportTab3_4.txt"
    End If
    
    SetDateTimeFormat CurTab, "DOEM,DODM", "YYYYMMDDhhmmss", MyDateFormat
    
    CurTab.ShowVertScroller True
    CurTab.ShowHorzScroller True
    CurTab.AutoSizeByHeader = True
    CurTab.AutoSizeColumns
    CurTab.Refresh
    
    If Not StaffOnly Then
        MaxLine = CurTab.GetLineCount - 1
        For CurLine = 0 To MaxLine
            tmpData = CurTab.GetColumnValue(CurLine, AbsStfPeno)
            tmpLen = Len(tmpData)
            If tmpLen < 4 Then
                tmpData = Right("0000" & tmpData, 4)
                CurTab.SetColumnValue CurLine, AbsStfPeno, tmpData
            Else
            End If
            tmpUrno = CurTab.GetFieldValue(CurLine, "URNO")
            tmpFunc = frmHiddenData.LookupStaffBasicFunction(tmpUrno, "CODE")
            CurTab.SetFieldValues CurLine, "FUNC", tmpFunc
        Next
        CurTab.AutoSizeColumns
        CurTab.Refresh
    End If
    
    Screen.MousePointer = 0
End Sub

Private Sub LoadHrAbsDrrtab(CurTab As TABLib.TAB, stfTab As TABLib.TAB)
    Dim CdrAnsw As Boolean
    Dim tmpCmd As String
    Dim tmpTable As String
    Dim tmpFields As String
    Dim tmpGridFields As String
    Dim tmpSqlKey As String
    Dim tmpSprKey As String
    Dim tmpHeader As String
    Dim tmpLength As String
    Dim tmpData As String
    Dim tmpSTFU As String
    Dim tmpSday As String
    Dim tmpHrsd As String
    Dim tmpSbfr As String
    Dim tmpBsdx As String
    Dim tmpPeno As String
    Dim tmpPerc As String
    Dim tmpClki As String
    Dim tmpClko As String
    Dim tmpSprKeys As String
    Dim tmpKeys As String
    Dim HitCount As Long
    Dim HitLine As Long
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim tmpLen As Integer
    
    tmpCmd = "RTA"
    tmpTable = "DRRTAB"
    tmpFields = "'N',' ',' ',' ',' ',FCTC,' ',SDAY,SCOD,SCOO,DRRN,ROSS,ROSL,' ',' ',AVFR,AVTO,SBFR,SBTO,SBLU,DRS1,DRS2,DRS3,DRS4,STFU,BSDU,URNO,' ',' ',' '"
    tmpGridFields = "LINE,STAT,CMDS,PENO,PERC,FCTC,BSDX,SDAY,SCOD,SCOO,DRRN,ROSS,ROSL,CLKI,CLKO,AVFR,AVTO,SBFR,SBTO,SBLU,DRS1,DRS2,DRS3,DRS4,STFU,BSDU,URNO,HRSD,....,...."
    
    AbsDrrStfu = GetRealItemNo(tmpGridFields, "STFU")
    AbsDrrAvfr = GetRealItemNo(tmpGridFields, "AVFR")
    
    tmpData = stfTab.SelectDistinct(CStr(AbsStfUrno), "'", "'", ",", True)
    tmpSqlKey = "WHERE STFU IN (" & tmpData & ") AND SDAY BETWEEN '" & minAbsSday & "' AND '" & maxAbsSday & "' AND ROSS='A'"
    tmpSprKey = "WHERE USTF IN (" & tmpData & ") AND ACTI BETWEEN '" & minAbsSday & "000000' AND '" & maxAbsSday & "235959'"
    
    tmpHeader = "L,S,A,PENO,PERC,FCTC,T,SDAY,SC,OC,D,S,L,CLKI,CLKO,AVFR,AVTO,SBFR,SBTO,SBLU,DRS1,DRS2,DRS3,DRS4,STFU,BSDU,URNO,STFU-SDAY,2,3"
    tmpLength = "10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10"
    
    CurTab.ResetContent
    CurTab.ShowVertScroller False
    CurTab.ShowHorzScroller False
    CurTab.HeaderString = tmpHeader
    CurTab.HeaderLengthString = tmpLength
    CurTab.LogicalFieldList = tmpGridFields
    CurTab.SetMainHeaderValues "3,3,7,2,2,3,4,3,3", "Line,Identifier,Shift Data,Clock I/O,Time Period,Internal Break Times,DRS Values,Relation Keys,System", ",,,,,,,,"
    CurTab.SetMainHeaderFont 17, False, False, True, 0, "Arial"
    CurTab.AutoSizeByHeader = True
    CurTab.AutoSizeColumns
    CurTab.Refresh
    
    TAB0(0).ResetContent
    TAB0(0).ShowVertScroller True
    TAB0(0).ShowHorzScroller True
    TAB0(0).HeaderString = tmpHeader
    TAB0(0).HeaderLengthString = tmpLength
    TAB0(0).LogicalFieldList = tmpGridFields
    TAB0(0).AutoSizeByHeader = True
    TAB0(0).AutoSizeColumns
    TAB0(0).Refresh
    
    Screen.MousePointer = 11
    
    CurTab.CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
    CurTab.CedaHopo = UfisServer.HOPO
    CurTab.CedaIdentifier = ""
    CurTab.CedaPort = "3357"
    CurTab.CedaReceiveTimeout = "250"
    CurTab.CedaRecordSeparator = vbLf
    CurTab.CedaSendTimeout = "250"
    CurTab.CedaServerName = UfisServer.HostName
    CurTab.CedaTabext = UfisServer.TblExt
    CurTab.CedaUser = UfisServer.ModName
    CurTab.CedaWorkstation = UfisServer.GetMyWorkStationName
    Dim mytmp As String
    If Not StaffOnly Then
        If CedaIsConnected Then
            'mytmp = " select " & tmpFields & " from " & tmpTable & " where " & tmpSqlKey
            CdrAnsw = CurTab.CedaAction(tmpCmd, tmpTable, tmpFields, "", tmpSqlKey)
        ElseIf UfisServer.HostName = "LOCAL" Then
            CurTab.ReadFromFile "c:\tmp\RmsImportTab2_4.txt"
        End If
    End If
    
    frmHiddenData.LoadSprTabData frmHiddenData.SPRTAB, tmpSprKey
    Dim tmpUrno As String
    
    MaxLine = CurTab.GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpUrno = CurTab.GetFieldValue(CurLine, "URNO")
        tmpSTFU = CurTab.GetFieldValue(CurLine, "STFU")
        tmpSday = CurTab.GetFieldValue(CurLine, "SDAY")
        tmpSbfr = CurTab.GetFieldValue(CurLine, "SBFR")
        
        If Left(tmpSbfr, 4) = "1899" Then tmpBsdx = "A" Else tmpBsdx = "S"
        tmpPeno = GetLookupFieldValues(stfTab, "URNO", tmpSTFU, "PENO,PERC", HitCount, HitLine)
        tmpHrsd = tmpSTFU & "-" & tmpSday
        tmpKeys = Right("0000000000" & Trim(tmpSTFU), 10) & "-" & tmpSday
        
        tmpSprKey = tmpKeys & "-1"
        tmpClki = GetLookupFieldValues(frmHiddenData.SPRTAB, "KEYS", tmpSprKey, "ACTI", HitCount, HitLine)
        tmpSprKey = tmpKeys & "-0"
        tmpClko = GetLookupFieldValues(frmHiddenData.SPRTAB, "KEYS", tmpSprKey, "ACTI", HitCount, HitLine)
        
        tmpClki = Mid(tmpClki, 9, 4)
        tmpClko = Mid(tmpClko, 9, 4)
        
        
        
        tmpData = "N" & "," & tmpHrsd & "," & tmpBsdx & "," & tmpPeno & "," & tmpClki & "," & tmpClko
        CurTab.SetFieldValues CurLine, "LINE,HRSD,BSDX,PENO,PERC,CLKI,CLKO", tmpData
        
    Next
    SetDateTimeFormat CurTab, "SDAY", "YYYYMMDD", MyDateFormat
    SetDateTimeFormat CurTab, "AVFR,AVTO,SBFR,SBTO", "YYYYMMDDhhmmss", MyDateTimeFormat
    
    CurTab.ShowVertScroller True
    CurTab.ShowHorzScroller True
    CurTab.AutoSizeByHeader = True
    CurTab.AutoSizeColumns
    CurTab.Refresh
    
    Screen.MousePointer = 0
    
End Sub

Private Sub CheckHrAbsData(shrTab As TABLib.TAB, stfTab As TABLib.TAB, drrTab As TABLib.TAB)
    SetTabIndexes drrTab, "STFU,HRSD", "STFU,HRSD", False
    ClearTabData drrTab, "CMDS", "I", "", ""
    SetTabIndexes drrTab, "STFU,HRSD", "STFU,HRSD", True
    CheckDrrtabData shrTab, stfTab, drrTab
    CheckStfAbsences stfTab, shrTab, drrTab, "STAT", "C"
    stfTab.OnVScrollTo 0
    stfTab.SetCurrentSelection 0
    stfTab.SetFocus
End Sub

Private Sub ValidateHrAbsData(shrTab As TABLib.TAB, stfTab As TABLib.TAB)
    SetTabIndexes shrTab, "PENO", "PENO", False
    ClearTabData shrTab, "LINE", "N", "", ""
    SetTabIndexes shrTab, "PENO", "PENO", True
    
    SetTabIndexes stfTab, "PENO,URNO", "PENO,URNO", False
    ClearTabData stfTab, "LINE", "N", "", ""
    SetTabIndexes stfTab, "PENO,URNO", "PENO,URNO", True
    
    stfTab.OnVScrollTo 0
    stfTab.SetCurrentSelection 0
    stfTab.SetFocus
End Sub

Private Sub ClearHrAbsData(shrTab As TABLib.TAB, stfTab As TABLib.TAB, drrTab As TABLib.TAB)
    SetTabIndexes shrTab, "PENO", "PENO", False
    ClearTabData shrTab, "LINE", "N", "", ""
    SetTabIndexes shrTab, "PENO", "PENO", True
    
    CheckStfAbsences stfTab, shrTab, drrTab, "STAT", "V"
    
    SetTabIndexes drrTab, "STFU,HRSD", "STFU,HRSD", False
    ClearTabData drrTab, "LINE", "N", "CMDS", ".I.U.D."
    SetTabIndexes drrTab, "STFU,HRSD", "STFU,HRSD", True
    
    stfTab.OnVScrollTo 0
    stfTab.SetCurrentSelection 0
    stfTab.SetFocus
End Sub

Private Sub ArrangeHrAbsSortIndexes(shrTab As TABLib.TAB, stfTab As TABLib.TAB, drrTab As TABLib.TAB)
    SetTabIndexes shrTab, "PENO", "PENO", False
    SetTabIndexes stfTab, "PENO,URNO", "PENO,URNO", False
    SetTabIndexes drrTab, "STFU,HRSD", "STFU,HRSD", False
    SetTabSortCols shrTab, "FUNC,VPFR,PENO"
    SetTabSortCols stfTab, "PENO"
    SetTabSortCols drrTab, "AVFR,PENO"
    SetTabIndexes shrTab, "PENO", "PENO", True
    SetTabIndexes stfTab, "PENO,URNO", "PENO,URNO", True
    SetTabIndexes drrTab, "STFU,HRSD", "STFU,HRSD", True
End Sub

Private Sub ClearTabData(CurTab As TABLib.TAB, LookField As String, ClearValue As String, CheckField As String, CheckValue As String)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim DoIt As Boolean
    Dim tmpData As String
    Dim tmpCheck As String
    MaxLine = CurTab.GetLineCount - 1
    CurLine = MaxLine
    While CurLine >= 0
        DoIt = True
        tmpData = CurTab.GetFieldValues(CurLine, LookField)
        If CheckField <> "" Then
            tmpCheck = "." & CurTab.GetFieldValue(CurLine, CheckField) & "."
            'If tmpCheck = CheckValue Then DoIt = False
            If InStr(CheckValue, tmpCheck) > 0 Then DoIt = False
        End If
        If (tmpData = ClearValue) And (DoIt = True) Then
            CurTab.DeleteLine CurLine
        Else
            CurTab.SetColumnValue CurLine, 1, "V"
        End If
        CurLine = CurLine - 1
    Wend
    CurTab.Refresh
End Sub

Private Sub CheckShrAbsences(shrTab As TABLib.TAB, stfTab As TABLib.TAB)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim tmpLookup As String
    Dim tmpLeave As String
    Dim tmpSdac As String
    Dim tmpOdau As String
    Dim tmpStat As String
    Dim tmpRmrk As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim tmpImfr As String
    Dim tmpImto As String
    Dim tmpPeno As String
    Dim tmpBool As String
    Dim tmpCmds As String
    Dim tmpSTFU As String
    Dim tmpStfFunc As String
    Dim tmpShrFunc As String
    Dim tmpData As String
    Dim tmpResult As String
    Dim tmpKeys As String
    Dim tmpLastKeys As String
    Dim HitCount As Long
    Dim HitLine As Long
    
    'tmpGridFields = "LINE,STAT,CMDS,PENO,FUNC,VPFR,VPTO,LEAV,SDAC,IMFR,IMTO,ODAU,RMRK"
    tmpVpfr = Trim(txtVpfrYY(curWorkPanel).Tag)
    tmpVpto = Trim(txtVptoYY(curWorkPanel).Tag)
    If tmpVpto = "" Then tmpVpto = maxAbsSday
    
    SetTabSortCols shrTab, "FUNC,VPFR,PENO"
        
    MaxLine = shrTab.GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpLeave = shrTab.GetFieldValue(CurLine, "LEAV")
        tmpLookup = frmHiddenData.LookupHrAbsenceCode(tmpLeave, "RMSC,ODAU")
        tmpStat = "C"
        tmpOdau = ""
        tmpRmrk = ""
        tmpBool = "Y"
        tmpCmds = ""
        If tmpLookup <> "" Then
            tmpSdac = GetItem(tmpLookup, 1, ",")
            tmpOdau = GetItem(tmpLookup, 2, ",")
            If tmpSdac = "" Then
                tmpSdac = "--"
                tmpStat = "L"
                tmpRmrk = "SHR Leave: Missing lookup value"
            ElseIf tmpOdau = "" Then
                tmpStat = "B"
                tmpRmrk = "Abs.Code missing in RMS"
            End If
        Else
            tmpSdac = "??"
            tmpStat = "N"
            tmpRmrk = "SHR Leave: Not found"
        End If
        tmpImfr = shrTab.GetFieldValue(CurLine, "VPFR")
        tmpImto = shrTab.GetFieldValue(CurLine, "VPTO")
        If tmpImfr < tmpVpfr Then tmpImfr = tmpVpfr
        If tmpImto > tmpVpto Then tmpImto = tmpVpto
        
        If tmpImfr > tmpImto Then tmpStat = "P"
        
        tmpPeno = shrTab.GetFieldValue(CurLine, "PENO")
        
        tmpData = GetLookupFieldValues(stfTab, "PENO", tmpPeno, "URNO,FUNC", HitCount, HitLine)
        If HitCount <= 0 Then tmpStat = "E"
        
        tmpSTFU = GetItem(tmpData, 1, ",")
        
        tmpShrFunc = shrTab.GetFieldValue(CurLine, "FUNC")
        tmpStfFunc = GetItem(tmpData, 2, ",")
        If tmpShrFunc <> tmpStfFunc Then tmpStat = "F"
        
        Select Case tmpStat
            Case "D"
                shrTab.SetLineColor CurLine, vbBlack, LightGrey
                tmpBool = "N"
                tmpCmds = "-"
            Case "F"
                shrTab.SetLineColor CurLine, vbBlack, LightGrey
                tmpBool = "N"
                tmpCmds = "-"
            Case "P", "L", "N", "E"
                shrTab.SetLineColor CurLine, vbWhite, vbRed
                tmpBool = "N"
                tmpCmds = "-"
                SetTabCellObjects shrTab, CurLine, "DECO", "CellDiffRed", "FUNC", True
            Case "B"
                shrTab.SetLineColor CurLine, vbWhite, vbRed
                tmpBool = "N"
                tmpCmds = "-"
                SetTabCellObjects shrTab, CurLine, "DECO", "CellDiffRed", "SDAC", True
            Case "C"
                shrTab.SetLineColor CurLine, vbBlack, vbWhite
                tmpBool = "Y"
                tmpCmds = ""
            Case Else
                shrTab.SetLineColor CurLine, vbBlack, vbWhite
                tmpBool = "N"
                tmpCmds = "?"
        End Select
        
        tmpResult = ""
        tmpResult = tmpResult & tmpBool & ","
        tmpResult = tmpResult & tmpStat & ","
        tmpResult = tmpResult & tmpCmds & ","
        tmpResult = tmpResult & tmpSdac & ","
        tmpResult = tmpResult & tmpImfr & ","
        tmpResult = tmpResult & tmpImto & ","
        tmpResult = tmpResult & tmpOdau & ","
        tmpResult = tmpResult & tmpRmrk & ","
        tmpResult = tmpResult & tmpSTFU & ","
        
        shrTab.SetFieldValues CurLine, "LINE,STAT,CMDS,SDAC,IMFR,IMTO,ODAU,RMRK,STFU", tmpResult
    Next
    
    tmpLastKeys = ""
    MaxLine = shrTab.GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpBool = shrTab.GetFieldValue(CurLine, "LINE")
        If tmpBool = "Y" Then
            tmpKeys = shrTab.GetFieldValues(CurLine, "IMFR,IMTO,STFU")
            If tmpKeys = tmpLastKeys Then
                tmpResult = "N,D,-"
                shrTab.SetFieldValues CurLine, "LINE,STAT,CMDS", tmpResult
                shrTab.SetLineColor CurLine, vbBlack, vbYellow
            End If
            tmpLastKeys = tmpKeys
        End If
    Next
    
    MaxLine = stfTab.GetLineCount - 1
    For CurLine = 0 To MaxLine
        stfTab.SetFieldValues CurLine, "LINE,STAT,CMDS", "N,V,-"
        stfTab.SetLineColor CurLine, vbBlack, LightGrey
    Next
    
    MaxLine = shrTab.GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpBool = shrTab.GetFieldValue(CurLine, "LINE")
        If tmpBool = "Y" Then
            tmpPeno = shrTab.GetFieldValue(CurLine, "PENO")
            tmpData = GetLookupFieldValues(stfTab, "PENO", tmpPeno, "URNO", HitCount, HitLine)
            If HitCount > 0 Then
                stfTab.SetFieldValues HitLine, "LINE,STAT,CMDS", "Y,V,"
                stfTab.SetLineColor HitLine, vbBlack, vbWhite
                SetTabCellObjects stfTab, HitLine, "DECO", "CellDiffGry", "FUNC", True
                SetTabCellObjects shrTab, CurLine, "DECO", "CellDiffGry", "FUNC", True
            End If
        End If
    Next
    
    shrTab.AutoSizeColumns
    stfTab.Refresh
    shrTab.Refresh
End Sub

Private Sub CheckStfAbsences(stfTab As TABLib.TAB, shrTab As TABLib.TAB, drrTab As TABLib.TAB, SetField As String, SetValue As String)
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim HitCount As Long
    Dim HitLine As Long
    Dim tmpPeno As String
    Dim tmpUrno As String
    Dim tmpData As String
    MaxLine = stfTab.GetLineCount - 1
    For CurLine = 0 To MaxLine
        stfTab.SetFieldValues CurLine, SetField, SetValue
        tmpPeno = stfTab.GetFieldValue(CurLine, "PENO")
        tmpUrno = stfTab.GetFieldValue(CurLine, "URNO")
        tmpData = GetLookupFieldValues(shrTab, "PENO", tmpPeno, "URNO", HitCount, HitLine)
        If HitCount > 0 Then
            stfTab.SetColumnValue CurLine, 0, "Y"
            stfTab.SetColumnValue CurLine, 2, ""
            stfTab.SetLineColor CurLine, vbBlack, vbWhite
        Else
            stfTab.SetColumnValue CurLine, 0, "N"
            stfTab.SetColumnValue CurLine, 2, "-"
            stfTab.SetLineColor CurLine, vbBlack, LightGrey
            SynchronizeTabValues drrTab, "STFU", tmpUrno, "LINE", "N", "", ""
        End If
    Next
End Sub
Private Sub SynchronizeTabValues(CurTab As TABLib.TAB, IndexName As String, LookValue As String, SetFields As String, SetValues As String, CheckField As String, CheckValue As String)
    Dim tmpLine As String
    Dim tmpData As String
    Dim LineNo As Long
    Dim HitCount As Long
    Dim SetVal As Boolean
    CurTab.SetInternalLineBuffer True
    tmpLine = CurTab.GetLinesByIndexValue(IndexName, LookValue, 0)
    HitCount = Val(tmpLine)
    If HitCount > 0 Then
        LineNo = CurTab.GetNextResultLine
        While LineNo >= 0
            SetVal = True
            If CheckField <> "" Then
                tmpData = CurTab.GetFieldValue(LineNo, CheckField)
                If tmpData = CheckValue Then SetVal = False
            End If
            If SetVal = True Then CurTab.SetFieldValues LineNo, SetFields, SetValues
            LineNo = CurTab.GetNextResultLine
        Wend
    End If
    CurTab.SetInternalLineBuffer False
End Sub

Private Sub CheckDrrtabData(shrTab As TABLib.TAB, stfTab As TABLib.TAB, drrTab As TABLib.TAB)
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim tmpBool As String
    Dim tmpImfr As String
    Dim tmpImto As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim tmpSTFU As String
    Dim tmpLastStfu As String
    Dim tmpHrsd As String
    Dim tmpSday As String
    Dim fromDay
    Dim toDay
    Dim curDay
    ' Step 1: Check DRRTAB of valid SHR users
    MaxLine = shrTab.GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpBool = shrTab.GetColumnValue(CurLine, 0)
        If tmpBool = "Y" Then
            tmpSTFU = shrTab.GetFieldValue(CurLine, "STFU")
            tmpImfr = shrTab.GetFieldValue(CurLine, "IMFR")
            tmpImto = shrTab.GetFieldValue(CurLine, "IMTO")
            fromDay = CedaDateToVb(tmpImfr)
            toDay = CedaDateToVb(tmpImto)
            curDay = fromDay
            While curDay <= toDay
                tmpSday = Format(curDay, "yyyymmdd")
                tmpHrsd = tmpSTFU & "-" & tmpSday
                SynchronizeTabValues drrTab, "HRSD", tmpHrsd, "LINE,STAT,CMDS", "Y,C,", "", ""
                curDay = DateAdd("d", 1, curDay)
            Wend
        End If
    Next
    ' Step 2: Search invalid DRRTAB absences
    ' Check TimeFrame of each user
    MaxLine = shrTab.GetLineCount - 1
    tmpLastStfu = shrTab.GetFieldValue(0, "STFU")
    For CurLine = 0 To MaxLine
        tmpBool = shrTab.GetColumnValue(CurLine, 0)
        If tmpBool = "Y" Then
            tmpSTFU = shrTab.GetFieldValue(CurLine, "STFU")
            If tmpSTFU <> tmpLastStfu Then
                fromDay = CedaDateToVb(minAbsSday)
                toDay = CedaDateToVb(maxAbsSday)
                curDay = fromDay
                While curDay <= toDay
                    tmpSday = Format(curDay, "yyyymmdd")
                    tmpHrsd = tmpLastStfu & "-" & tmpSday
                    'SynchronizeTabValues drrTab, "HRSD", tmpHrsd, "LINE,STAT,CMDS", "Y,C,?", "LINE", "Y"
                    SynchronizeTabValues drrTab, "HRSD", tmpHrsd, "LINE,STAT,CMDS", "N,C,?", "LINE", "Y"
                    curDay = DateAdd("d", 1, curDay)
                Wend
            End If
            tmpLastStfu = tmpSTFU
        End If
    Next
End Sub

Private Sub SetDateTimeFormat(CurTab As TABLib.TAB, FieldList As String, DataFormat As String, ShowFormat As String)
    Dim tmpField As String
    Dim ColNo As Integer
    Dim i As Integer
    i = 0
    tmpField = "START"
    While tmpField <> ""
        i = i + 1
        tmpField = GetItem(FieldList, i, ",")
        If tmpField <> "" Then
            ColNo = GetRealItemNo(CurTab.LogicalFieldList, tmpField)
            CurTab.DateTimeSetColumnFormat CLng(ColNo), DataFormat, ShowFormat
            
        End If
    Wend
End Sub

Private Sub chkTool_Click(Index As Integer)
    Dim tmpValue As Long
    Dim tmpTag As String
    tmpTag = chkTool(Index).Tag
    If chkTool(Index).Value = 1 Then
        chkTool(Index).BackColor = LightGreen
        Select Case tmpTag
            Case "CHECK"
                MyMsgBox.UnderConstruction "(Future use only)"
                'frmMainCleaner.Show
                chkTool(Index).Value = 0
            Case "EXPORT"
                MyMsgBox.UnderConstruction "(Future use only)"
                'frmMainExport.Show
                chkTool(Index).Value = 0
            Case "REPTST"
                MyMsgBox.UnderConstruction "(Future use only)"
                'frmPreview.Show
                chkTool(Index).Value = 0
            Case "DEBUG"
                MyMsgBox.UnderConstruction "(Future use only)"
                'frmDebug.Show
                chkTool(Index).Value = 0
            Case "SLIDES"
                HandleSlides
                chkTool(Index).Value = 0
            Case "SLIDE.COLLECTED"
                SlideIndex = 1
                If SlideShow = False Then chkTool(Index).Value = 0
            Case "SLIDE.HOLD.LEFT"
                SlideHoldLeft = True
            Case "SLIDE.HOLD.MID"
                SlideHoldMid = True
            Case "SLIDE.HOLD.RIGHT"
                SlideHoldRight = True
            Case "SLIDE.HOLD.ALL"
                SlideHoldAll = True
                If SlideShow = True Then
                    UfisIntroTimer(2).Enabled = False
                Else
                    UfisIntroTimer(0).Enabled = False
                End If
            Case "SLIDE.SELECT"
                SlideShow = True
                frmFileDialog.SelectSlides True
                frmFileDialog.Show , Me
            Case "SLIDE.DOUBLE"
                If SlideShow = False Then
                    UfisIntroFrame(0).Visible = True
                    UfisIntroFrame(1).Visible = True
                    UfisIntroFrame(2).Visible = False
                    lblTitle(2).Visible = False
                    lblTitle(0).Visible = False
                    lblTitle(1).Visible = False
                    lblTitle(0).Caption = "Left Scenario"
                    lblTitle(1).Caption = "Right Scenario"
                    fraWorkArea(0).Refresh
                End If
                SlideDouble = True
            Case "SLIDE.MAXI"
                fraWorkArea(0).Top = 0
                fraWorkArea(0).height = Me.ScaleHeight
                SlideMaximized = True
                'Slider1(0).SetFocus
                chkTool(Index).Value = 0
            Case "SLIDE.STOP"
                ReorgSlideShow False
                ReorgSlideShow False    'Call twice!
                InitSlideShowTimer
                chkTool(Index).Value = 0
            Case Else
                chkTool(Index).Value = 0
        End Select
    Else
        chkTool(Index).BackColor = vbButtonFace
        Select Case tmpTag
            Case "SLIDE.COLLECTED"
                SlideIndex = 0
            Case "SLIDE.HOLD.LEFT"
                SlideHoldLeft = False
            Case "SLIDE.HOLD.MID"
                SlideHoldMid = False
            Case "SLIDE.HOLD.RIGHT"
                SlideHoldRight = False
            Case "SLIDE.HOLD.ALL"
                If SlideShow = True Then
                    UfisIntroTimer(2).Enabled = True
                Else
                    UfisIntroTimer(0).Enabled = True
                End If
                SlideHoldAll = False
            Case "SLIDE.SELECT"
                frmFileDialog.SelectSlides False
                tmpValue = frmFileDialog.tabFoundFiles(0).GetLineCount + frmFileDialog.tabFoundFiles(1).GetLineCount
                If tmpValue > 0 Then
                    InitSlideShowTimer
                Else
                    SlideShow = False
                End If
            Case "SLIDE.DOUBLE"
                If SlideShow = False Then
                    UfisIntroFrame(0).Visible = False
                    UfisIntroFrame(1).Visible = False
                    UfisIntroFrame(2).Visible = True
                    lblTitle(0).Caption = "Current Step"
                    lblTitle(1).Caption = "Last Step"
                    lblTitle(2).Visible = True
                    lblTitle(0).Visible = False
                    lblTitle(1).Visible = False
                    fraWorkArea(0).Refresh
                End If
                SlideDouble = False
            Case Else
        End Select
    End If
End Sub

Private Sub chkWork_Click(Index As Integer)
    Dim tmpTag As String
    
     Ckb_Interface.Value = Unchecked
     
    If chkWork(Index).Value = 1 Then
        chkWork(Index).BackColor = LightGreen
        If curWorkButton >= 0 Then
            CalledFromInternal = True
            chkWork(curWorkButton).Value = 0
            CalledFromInternal = False
        End If
        curWorkButton = Index
        fraFuncPanel(curFuncPanel).Tag = CStr(curWorkButton)
        ReorgWorkPanel 0
        If curWorkPanel > 0 Then
            AdjustBottomLabels curWorkPanel, TAB1(curWorkPanel), TAB2(curWorkPanel), True
        End If
    Else
        If curWorkPanel >= 0 Then
            ToggleBottomPanel curWorkPanel, False, False, False, False
            fraWorkPanel(curWorkPanel).Visible = False
        End If
        chkWork(Index).BackColor = vbButtonFace
        If curWorkButton = Index Then
            curWorkButton = -1
            fraFuncPanel(curFuncPanel).Tag = CStr(curWorkButton)
        End If
        Form_Resize
    End If
    If curWorkButton >= 0 Then
        fraWorkArea(0).Visible = False
    ElseIf Not CalledFromInternal Then
        fraWorkArea(0).Visible = True
    End If
End Sub

Private Sub Ckb_Interface_Click()


    If Ckb_Interface = Checked And Interface_TIMER.Enabled = False Then
   lblTime(1).Caption = " Interface Enabled"
   Interface_TIMER.Enabled = True
   Interface_TIMER.Interval = 60000 ' one second then we change the minutes
   
    Else
    Interface_TIMER.Enabled = False
    lblTime(1).Caption = " Interface Disabled"
    End If
End Sub

Private Sub CliFkeyCnt_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    JumpToColoredLine TAB1(curWorkPanel), CliFkeyCnt(Index).BackColor
End Sub

Private Sub Command1_Click()
    Dim i As Integer
    For i = 0 To TAB1.UBound
        TAB1(i).WriteToFile "c:\tmp\RmsImportTab1_" & CStr(i) & ".txt", False
        TAB2(i).WriteToFile "c:\tmp\RmsImportTab2_" & CStr(i) & ".txt", False
        TAB3(i).WriteToFile "c:\tmp\RmsImportTab3_" & CStr(i) & ".txt", False
    Next
    frmHiddenData.ODATAB.WriteToFileWithProperties "c:\tmp\RmsImportOdaTab.txt"
    frmHiddenData.BSDTAB.WriteToFileWithProperties "c:\tmp\RmsImportBsdTab.txt"
    frmHiddenData.SPFTAB.WriteToFileWithProperties "c:\tmp\RmsImportSpfTab.txt"
    frmHiddenData.TAB1.WriteToFileWithProperties "c:\tmp\RmsImportOdaLookUp.txt"
End Sub

Private Sub Command2_Click()
    frmHiddenData.Show
End Sub

Private Sub Form_Activate()
    Static IsActivated As Boolean
    If Not IsActivated Then
        lblLoading.Visible = True
        Me.Refresh
        Load frmHiddenData
        CheckUfisIntroPicture -1, False, 0, "RmsImpTool_INTRO.bmp", "c:\ufis\system"
        lblLoading.Visible = False
        ApplicationIsStarted = True
        IsActivated = True
    End If
End Sub

Private Sub Form_Load()
    Dim i As Integer
    Dim j As Integer
    Dim NewWidth As Long
    Dim NewHeight As Long
    Dim NewValue As Long
    Dim tmpCap As String
    Dim tmpTxt As String
    Dim tmpTag As String
    Dim tmpColorGry As String
    Dim tmpColorYel As String
    Dim tmpColorRed As String
    Dim tmpFields As String
    On Error Resume Next
    myLeft = 60
    curMainButton = -1
    curWorkButton = -1
    curWorkPanel = -1
    curFuncPanel = -1
    
    fraWorkArea(0).Left = myLeft
    fraWorkArea(1).Left = myLeft
    fraTopPanel(0).Left = myLeft
    For i = 0 To fraFuncPanel.UBound
        fraFuncPanel(i).Top = 330
        fraFuncPanel(i).Left = 90
        fraFuncPanel(i).BackColor = fraTopPanel(0).BackColor
        fraFuncPanel(i).Tag = "-1"
        fraFuncPanel(i).Visible = False
    Next
    For i = 0 To chkWork.UBound
        tmpCap = Trim(chkWork(i).Caption)
        If tmpCap <> "" Then
            chkWork(i).Visible = True
            chkWork(i).Enabled = True
        Else
            chkWork(i).Visible = False
        End If
    Next
    For i = 0 To fraSplitter.UBound
        fraSplitter(i).BackColor = vbButtonFace
        fraSplitter(i).Width = 90
        fraSplitter(i).MousePointer = 0
        fraTopSplit(i).BackColor = vbButtonFace
        fraTopSplit(i).Left = 0
        fraTopSplit(i).height = 90
        fraTopSplit(i).MousePointer = 0
    Next
    For i = 0 To fraWorkPanel.UBound
        fraWorkPanel(i).Left = myLeft
        fraWorkPanel(i).BackColor = vbButtonFace
        fraTopData(i).Left = 0
        fraTopData(i).height = 2400
        fraTopData(i).Top = fraDialog(i).Top + fraDialog(i).height + 90
        If Left(TAB3(i).Tag, 4) = "SHOW" Then
            fraTopSplit(i).Top = fraTopData(i).Top + fraTopData(i).height
            fraLeftData(i).Top = fraTopSplit(i).Top + fraTopSplit(i).height
            fraRightData(i).Top = fraLeftData(i).Top
            fraSplitter(i).Top = fraLeftData(i).Top
            fraTopData(i).Visible = True
            TAB3(i).Visible = True
        Else
            fraTopData(i).Visible = False
            fraTopSplit(i).Enabled = False
            fraTopSplit(i).Top = fraDialog(i).Top + fraDialog(i).height
            fraLeftData(i).Top = fraTopSplit(i).Top + fraTopSplit(i).height
            fraRightData(i).Top = fraLeftData(i).Top
            fraSplitter(i).Top = fraLeftData(i).Top
        End If
    Next
    tmpColorGry = CStr(vbBlack) & "," & CStr(LightGray)
    tmpColorYel = CStr(vbBlack) & "," & CStr(vbYellow)
    tmpColorRed = CStr(vbBlack) & "," & CStr(LightestRed)
    For i = 0 To TAB1.UBound
        TAB1(i).ResetContent
        TAB1(i).FontName = "Courier New"
        TAB1(i).HeaderFontSize = 17
        TAB1(i).FontSize = 17
        TAB1(i).LineHeight = 19
        TAB1(i).LeftTextOffset = 0
        TAB1(i).SetTabFontBold True
        TAB1(i).HeaderString = " "
        TAB1(i).HeaderLengthString = "1000"
        TAB1(i).MainHeader = True
        TAB1(i).LifeStyle = True
        TAB1(i).GridLineColor = DarkGray
        TAB1(i).CreateCellObj "Marker", vbBlue, vbWhite, 17, False, False, True, 0, "Courier New"
        TAB1(i).SetColumnBoolProperty 0, "Y", "N"
        TAB1(i).CreateDecorationObject "CellDiffGry", "R,T,B,L", "2,2,2,2", tmpColorGry & "," & tmpColorGry
        TAB1(i).CreateDecorationObject "CellDiffYel", "R,T,B,L", "2,2,2,2", tmpColorYel & "," & tmpColorYel
        TAB1(i).CreateDecorationObject "CellDiffRed", "R,T,B,L", "2,2,2,2", tmpColorRed & "," & tmpColorRed
        TAB2(i).ResetContent
        TAB2(i).FontName = "Courier New"
        TAB2(i).HeaderFontSize = 17
        TAB2(i).FontSize = 17
        TAB2(i).LineHeight = 19
        TAB2(i).LeftTextOffset = 0
        TAB2(i).SetTabFontBold True
        TAB2(i).HeaderString = " "
        TAB2(i).HeaderLengthString = "1000"
        TAB2(i).MainHeader = True
        TAB2(i).LifeStyle = True
        TAB2(i).GridLineColor = DarkGray
        TAB2(i).CreateCellObj "Marker", vbBlue, vbWhite, 17, False, False, True, 0, "Courier New"
        TAB2(i).SetColumnBoolProperty 0, "Y", "N"
        TAB2(i).CreateDecorationObject "CellDiffGry", "R,T,B,L", "2,2,2,2", tmpColorGry & "," & tmpColorGry
        TAB2(i).CreateDecorationObject "CellDiffYel", "R,T,B,L", "2,2,2,2", tmpColorYel & "," & tmpColorYel
        TAB2(i).CreateDecorationObject "CellDiffRed", "R,T,B,L", "2,2,2,2", tmpColorYel & "," & tmpColorYel
        TAB3(i).Top = 210
        TAB3(i).ResetContent
        TAB3(i).FontName = "Arial"
        TAB3(i).HeaderFontSize = 17
        TAB3(i).FontSize = 17
        TAB3(i).LineHeight = 19
        TAB3(i).LeftTextOffset = 0
        TAB3(i).SetTabFontBold True
        TAB3(i).HeaderString = " "
        TAB3(i).HeaderLengthString = "2000"
        TAB3(i).MainHeader = True
        TAB3(i).LifeStyle = True
        TAB3(i).GridLineColor = DarkGray
        TAB3(i).CreateCellObj "Marker", vbBlue, vbWhite, 17, False, False, True, 0, "Arial"
        TAB3(i).SetColumnBoolProperty 0, "Y", "N"
        TAB3(i).CreateDecorationObject "CellDiffGry", "R,T,B,L", "2,2,2,2", tmpColorGry & "," & tmpColorGry
        TAB3(i).CreateDecorationObject "CellDiffYel", "R,T,B,L", "2,2,2,2", tmpColorYel & "," & tmpColorYel
        TAB3(i).CreateDecorationObject "CellDiffRed", "R,T,B,L", "2,2,2,2", tmpColorYel & "," & tmpColorYel
        Select Case i
            Case 4
                TAB1(i).myName = "PENO"
                TAB2(i).myName = "PENO"
                TAB3(i).myName = "PENO"
            Case Else
        End Select
    Next
    For i = 0 To chkTool.UBound
        chkTool(i).BackColor = vbButtonFace
    Next
    tmpFields = "BL01,BL02"
    For i = 0 To CliFkeyCnt.UBound
        chkLeftImp(i).Tag = CliFkeyCnt(i).Tag
        chkRightImp(i).Tag = SrvFkeyCnt(i).Tag
        CliFkeyCnt(i).Tag = CStr(CliFkeyCnt(i).BackColor)
        CliFkeyCnt(i).BackColor = vbButtonFace
        SrvFkeyCnt(i).Tag = CStr(SrvFkeyCnt(i).BackColor)
        SrvFkeyCnt(i).BackColor = vbButtonFace
        CliFkeyCnt(i).Enabled = False
        SrvFkeyCnt(i).Enabled = False
        chkLeftImp(i).Enabled = False
        chkRightImp(i).Enabled = False
        tmpFields = tmpFields & "," & Right(("0000" & CStr(i)), 4)
    Next
    For i = 0 To tabValues.UBound
        tabValues(i).ResetContent
        tabValues(i).HeaderString = tmpFields
        tabValues(i).LogicalFieldList = tmpFields
        For j = 0 To CliFkeyCnt.UBound
            InsertEmptyTabLine tabValues(i), -1
        Next
    Next
    For i = 0 To fraHelp.UBound
        fraHelp(i).BackColor = vbButtonFace
        fraHelp(i).Left = fraHelp(0).Left
        fraHelp(i).Visible = False
    Next
    For i = 0 To chkMain.UBound
        If GetItem(chkMain(i).Tag, 3, ",") = "D" Then chkMain(i).Enabled = False
    Next
    For i = 0 To picBall.UBound
        picBall(i).BackColor = vbButtonFace
        picBall(i).height = 75
        picBall(i).Width = 75
        picBall(i).Top = chkMain(i).Top + 15
        picBall(i).Left = chkMain(i).Left + 15
        picBall(i).Tag = "N"
        picBall(i).Visible = False
    Next
    
    fraPanelRules(4).Width = 3270
    
    fraTopPanel(0).height = fraButtonPanel(0).height
    lblTime(0).Left = 60
    lblTime(0).BackColor = LightestYellow
    For i = 1 To lblTime.UBound
        lblTime(i).Top = lblTime(0).Top
        lblTime(i).Left = lblTime(i - 1).Left + lblTime(i - 1).Width + 30
        lblTime(i).Caption = ""
        lblTime(i).BackColor = LightestYellow
    Next
    MainTimer_Timer
    
    fraTopPanel(0).Top = fraButtonPanel(0).Top + fraButtonPanel(0).height
    fraTopPanel(0).Visible = True
    StatusBar1.ZOrder
    UfisIntroPicture(0).BorderStyle = 0
    UfisIntroPicture(0).Visible = False
    lblTime(3).Caption = " Server: " & UfisServer.HostName
    
    tabSlides.Width = 1500
    tabSlides.Top = 150
    
    'Check Screen Settings
    NewWidth = Screen.Width / 15
    NewHeight = Screen.height / 15
    NewValue = NewWidth / NewHeight
    If NewValue > 1 Then NewWidth = NewWidth / 2
    Me.Top = ((NewHeight - 768) / 2) * 15
    Me.Left = ((NewWidth - 1024) / 2) * 15
    If NewWidth > 1024 Then NewWidth = 1024
    If NewHeight > 768 Then NewHeight = 768
    Me.Width = NewWidth * 15
    Me.height = NewHeight * 15
End Sub

Private Sub Form_Resize()
    Dim NewSize As Long
    Dim leftSize As Long
    Dim rightSize As Long
    Dim i As Integer
    Dim NewTop As Long
    Dim NewLeft As Long
    UfisIntroTimer(1).Enabled = False
    lblTitle(0).Visible = False
    lblTitle(1).Visible = False
    
    NewSize = Me.ScaleWidth - myLeft
    fraButtonPanel(0).Width = NewSize - fraButtonPanel(0).Left + myLeft
    fraTopPanel(0).Width = NewSize - fraTopPanel(0).Left - myLeft + 60
    fraWorkArea(0).Width = NewSize - fraWorkArea(0).Left - myLeft + 60
    fraWorkArea(1).Width = NewSize - fraWorkArea(1).Left - myLeft + 60
    tabSlides.Left = fraWorkArea(0).Width - tabSlides.Width - 60
    If curFuncPanel >= 0 Then fraFuncPanel(curFuncPanel).Width = fraTopPanel(0).Width - 150
    NewSize = Me.ScaleWidth - lblTime(3).Left - lblTime(0).Left
    If NewSize > 300 Then lblTime(3).Width = NewSize
    If fraTopPanel(0).Visible Then
        fraWorkArea(0).Top = fraTopPanel(0).Top + fraTopPanel(0).height
    Else
        fraWorkArea(0).Top = fraButtonPanel(0).Top + fraButtonPanel(0).height - 90
    End If
    NewSize = Me.ScaleHeight - fraWorkArea(0).Top - pbBottomPanel(0).height - StatusBar1.height
    If NewSize > 300 Then fraWorkArea(0).height = NewSize
    tabSlides.height = fraWorkArea(0).height - 210
    If curWorkPanel >= 0 Then
        fraWorkPanel(curWorkPanel).Top = fraWorkArea(0).Top + 120
        fraWorkPanel(curWorkPanel).height = fraWorkArea(0).height
        fraWorkPanel(curWorkPanel).Width = fraWorkArea(0).Width
        fraDialog(curWorkPanel).Width = fraWorkArea(0).Width - (fraDialog(curWorkPanel).Left * 2)
        fraTopData(curWorkPanel).Width = fraDialog(curWorkPanel).Width
        TAB3(curWorkPanel).Width = fraTopData(curWorkPanel).Width - 210
        TAB3(curWorkPanel).height = fraTopData(curWorkPanel).height - TAB3(curWorkPanel).Top - 120
        fraTopSplit(curWorkPanel).Width = fraDialog(curWorkPanel).Width
        NewSize = fraWorkArea(0).height - fraLeftData(curWorkPanel).Top - fraLeftData(curWorkPanel).Left - 120
        If NewSize > 300 Then
            fraLeftData(curWorkPanel).height = NewSize
            fraRightData(curWorkPanel).height = NewSize
            fraSplitter(curWorkPanel).height = NewSize
            NewSize = NewSize - TAB1(curWorkPanel).Top - 105
            If NewSize > 300 Then
                TAB1(curWorkPanel).height = NewSize
                TAB2(curWorkPanel).height = NewSize
            End If
        End If
        If fraSplitter(curWorkPanel).Visible Then
            NewSize = fraDialog(curWorkPanel).Width - fraSplitter(curWorkPanel).Width
            leftSize = NewSize / 2
            rightSize = NewSize - leftSize
            If leftSize > 300 Then
                fraLeftData(curWorkPanel).Width = leftSize
                TAB1(curWorkPanel).Left = 90
                TAB1(curWorkPanel).Width = leftSize - 195
                leftSize = leftSize + fraLeftData(curWorkPanel).Left
                fraSplitter(curWorkPanel).Left = leftSize
                leftSize = leftSize + fraSplitter(curWorkPanel).Width
                fraRightData(curWorkPanel).Left = leftSize
                fraRightData(curWorkPanel).Width = rightSize
                TAB2(curWorkPanel).Left = 90
                TAB2(curWorkPanel).Width = rightSize - 195
            End If
        Else
            leftSize = fraDialog(curWorkPanel).Width
            fraLeftData(curWorkPanel).Width = leftSize
            TAB1(curWorkPanel).Left = 90
            TAB1(curWorkPanel).Width = leftSize - 180
        End If
    End If
    fraBottomLabels(0).Left = myLeft
    NewSize = fraWorkArea(0).Width - fraBottomLabels(2).Width - 120
    leftSize = NewSize / 2
    rightSize = NewSize - leftSize
    If leftSize > 120 Then
        fraBottomLabels(0).Width = leftSize
        leftSize = leftSize + fraBottomLabels(0).Left + 60
        fraBottomLabels(2).Left = leftSize
        leftSize = leftSize + fraBottomLabels(2).Width + 60
        fraBottomLabels(1).Left = leftSize
        fraBottomLabels(1).Width = rightSize
    End If
    ArrangeBottomPanel
    ArrangeHelpPanel
    If fraWorkArea(0).Visible Then
        For i = 0 To UfisIntroFrame.UBound
            UfisIntroFrame(i).Top = ((fraWorkArea(0).height - UfisIntroPicture(i).height) \ 2) + 60
            UfisIntroFrame(i).Left = (fraWorkArea(0).Width - UfisIntroPicture(i).Width) \ 2
        Next
    End If
    If ApplicationIsStarted = False Then Me.Refresh
    UfisIntroTimer(1).Enabled = True
End Sub
Private Sub ArrangeHelpPanel()
    Dim NewSize As Long
    If chkMain(9).Value = 1 Then
        NewSize = 0
        If curWorkPanel >= 0 Then
            fraHelp(Val(fraHelp(0).Tag)).Visible = False
            If fraWorkPanel(curWorkPanel).Visible Then
                fraWorkArea(1).Top = fraWorkPanel(curWorkPanel).Top + fraDialog(curWorkPanel).height + 60
                NewSize = fraWorkPanel(curWorkPanel).height - fraDialog(curWorkPanel).height - 60
                lblHelp(curWorkPanel).Caption = fraDialog(curWorkPanel).Caption & ": Sorry! Help is not yet available."
                fraHelp(curWorkPanel).Visible = True
            Else
                fraWorkArea(1).Top = fraWorkArea(0).Top + 90
                NewSize = fraWorkArea(0).height - 90
            End If
            fraHelp(0).Tag = CStr(curWorkPanel)
        Else
            fraWorkArea(1).Top = fraWorkArea(0).Top + 90
            NewSize = fraWorkArea(0).height - 90
        End If
        If NewSize > 450 Then
            fraWorkArea(1).height = NewSize
            If curWorkPanel >= 0 Then
                fraHelp(curWorkPanel).height = NewSize - fraHelp(curWorkPanel).Top - 90
                fraHelp(curWorkPanel).Width = fraWorkArea(1).Width - fraHelp(curWorkPanel).Left - 90
            End If
            fraWorkArea(1).Visible = True
        Else
            fraWorkArea(1).Visible = False
        End If
    Else
        fraWorkArea(1).Visible = False
    End If
End Sub
Private Sub ArrangeBottomPanel()
    Dim newLeftSize As Long
    Dim newRightSize As Long
    Dim newLeftLeft As Long
    Dim newRightLeft As Long
    Dim newLeftWidth As Long
    Dim newRightWidth As Long
    Dim i As Integer
    newLeftSize = fraBottomLabels(0).Width - chkLeftImp(1).Left - 90 - (5 * 15)
    newRightSize = fraBottomLabels(1).Width - chkRightImp(1).Left - 90 - (5 * 15)
    newLeftWidth = ((newLeftSize \ 6) \ 15) * 15
    newRightWidth = ((newRightSize \ 6) \ 15) * 15
    
    If (newLeftWidth > 90) And (newRightWidth > 90) Then
        newLeftLeft = chkLeftImp(1).Left
        newRightLeft = chkRightImp(1).Left
        For i = 1 To 5
            chkLeftImp(i).Left = newLeftLeft
            chkLeftImp(i).Width = newLeftWidth
            CliFkeyCnt(i).Left = newLeftLeft
            CliFkeyCnt(i).Width = newLeftWidth
            chkRightImp(i).Left = newRightLeft
            chkRightImp(i).Width = newRightWidth
            SrvFkeyCnt(i).Left = newRightLeft
            SrvFkeyCnt(i).Width = newRightWidth
            newLeftLeft = newLeftLeft + newLeftWidth + 15
            newRightLeft = newRightLeft + newRightWidth + 15
        Next
        chkLeftImp(6).Left = newLeftLeft
        chkLeftImp(6).Width = fraBottomLabels(0).Width - newLeftLeft - 90
        CliFkeyCnt(6).Left = newLeftLeft
        CliFkeyCnt(6).Width = chkLeftImp(6).Width
        chkRightImp(6).Left = newRightLeft
        chkRightImp(6).Width = fraBottomLabels(1).Width - newRightLeft - 90
        SrvFkeyCnt(6).Left = newRightLeft
        SrvFkeyCnt(6).Width = chkRightImp(6).Width
        i = Val(GetItem(imgColorPointer(0).Tag, 2, ","))
        imgColorPointer(0).Left = CliFkeyCnt(i).Left
        i = Val(GetItem(imgColorPointer(1).Tag, 2, ","))
        imgColorPointer(1).Left = SrvFkeyCnt(i).Left
    End If
End Sub
Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Cancel = True
        chkMain(8).Value = 1
    End If
End Sub

Private Sub ReorgWorkPanel(curWp As Integer)
    Dim tmpMainTag As String
    Dim tmpWorkTag As String
    Dim tmpPicture As String
    If curMainButton >= 0 Then
        fraTopPanel(0).Caption = chkMain(curMainButton).Caption & ": Main Functions"
        fraTopPanel(0).Visible = True
        tmpMainTag = Trim(chkMain(curMainButton).Tag)
        curFuncPanel = Val(GetItem(tmpMainTag, 1, ","))
        fraFuncPanel(curFuncPanel).Visible = True
        If curWorkPanel >= 0 Then fraWorkPanel(curWorkPanel).Visible = False
        curWorkButton = Val(fraFuncPanel(curFuncPanel).Tag)
        If curWorkButton >= 0 Then
            tmpWorkTag = chkWork(curWorkButton).Tag
            curWorkPanel = Val(GetItem(tmpWorkTag, 1, ","))
            fraWorkPanel(curWorkPanel).Visible = True
        End If
        Form_Resize
        CheckVpfrDefault False, ""
        tmpPicture = "RmsImpTool_" & curMainFunction & ".bmp"
        CheckUfisIntroPicture -1, False, 0, tmpPicture, "c:\ufis\system"
    Else
        If curWorkPanel >= 0 Then fraWorkPanel(curWorkPanel).Visible = False
        fraWorkArea(0).Visible = True
    End If
End Sub
Private Sub CheckVpfrDefault(SetValues As Boolean, UseValue As String)
    Dim tmpTag As String
    Dim tmpData As String
    On Error Resume Next
    tmpTag = Trim(txtVpfrYY(curWorkPanel).Tag)
    If (tmpTag = "") Or (SetValues = True) Then
        If UseValue <> "" Then
            tmpData = UseValue
        Else
            tmpData = Left(GetTimeStamp(0), 8)
        End If
        txtVpfrYY(curWorkPanel).Tag = tmpData
        txtVpfrYY(curWorkPanel).Text = Mid(tmpData, 1, 4)
        txtVpfrMM(curWorkPanel).Text = Mid(tmpData, 5, 2)
        txtVpfrDD(curWorkPanel).Text = Mid(tmpData, 7, 2)
    End If
End Sub
Private Sub CheckVptoDefault(SetValues As Boolean, UseValue As String)
    Dim tmpTag As String
    Dim tmpData As String
    On Error Resume Next
    tmpTag = Trim(txtVptoYY(curWorkPanel).Tag)
    If (tmpTag = "") Or (SetValues = True) Then
        If UseValue <> "" Then tmpData = UseValue Else tmpData = Left(GetTimeStamp(0), 8)
        If tmpTag <> "" Then tmpData = ""
        txtVptoYY(curWorkPanel).Tag = tmpData
        txtVptoYY(curWorkPanel).Text = Mid(tmpData, 1, 4)
        txtVptoMM(curWorkPanel).Text = Mid(tmpData, 5, 2)
        txtVptoDD(curWorkPanel).Text = Mid(tmpData, 7, 2)
    End If
End Sub

Public Sub CheckUfisIntroPicture(CalledFrom As Integer, RefreshPicture As Boolean, IncIdx As Integer, MyPicture As String, MyPath As String)
    Static PicFileIndex As Integer
    Static PicFileCount As Integer
    Static PicCountDownLimit As Long
    Static PicCountDownValue As Long
    Static LastPicFileName As String
    Static PicturePath As String
    Static FuncFileIndex As Integer
    Static FuncFileCount As Integer
    Static FuncCountDownLimit As Long
    Static FuncCountDownValue As Long
    Static FuncScenarioPath As String
    Static LastPathName As String
    Static LastFileName As String
    Static FuncScenario As Boolean
    Dim FileIndex As Integer
    Dim FileCount As Integer
    Dim CountDownLimit As Long
    Dim CountDownValue As Long
    Dim filename As String
    Dim PathName As String
    Dim FilePattern As String
    Dim newEntry As String
    Dim NewSize As Long
    Dim TimeStep As Long
    Dim i As Integer
    Dim LoadMyPicture As Boolean
    Dim TryAgain As Boolean
    Dim ActPicX
    Dim ActPicY
    Dim ActZoom
    On Error GoTo ErrorHandle
    If (fraWorkArea(0).Visible) Or (RefreshPicture = True) Then
        If PicturePath = "" Then
            PicCountDownValue = 1
            PicCountDownLimit = 1
            FuncCountDownValue = 1
            FuncCountDownLimit = 1
            PicturePath = GetIniEntry(myIniFile, "MAIN", "", "PICTURE_PATH", "c:\ufis\system")
            TimeStep = Val(GetIniEntry(myIniFile, "MAIN", "", "SLIDE_INTERVAL", "5"))
            If TimeStep < 1 Then TimeStep = 5
            If TimeStep > 60 Then
                PicCountDownLimit = TimeStep
                UfisIntroLabel.Visible = True
                TimeStep = 1
            End If
            TimeStep = TimeStep * 1000
            UfisIntroTimer(0).Interval = TimeStep
            UfisIntroTimer(0).Enabled = False
            If UCase(PicturePath) <> UCase("c:\ufis\system") Then UfisIntroTimer(0).Enabled = True
        End If
        
        LoadMyPicture = False
        PathName = MyPath
        filename = MyPicture
        LoadMyPicture = RefreshPicture
        If LoadMyPicture = False Then
            Select Case CalledFrom
                Case -3 'Restore Slide Show
                    LoadMyPicture = True
                Case -2 'FileDialog
                    LoadMyPicture = True
                Case -1 'MainDialog Static Picture
                    If FuncScenario = True Then
                        'FileIndex = 0
                        'CountDownValue = 0
                        FuncScenario = False
                    End If
                    LoadMyPicture = True
                Case 0  'MainTimer (Slide Show)
                    If SlideShow = False Then
                        UfisIntroTimer(0).Enabled = False
                        If FuncScenario = True Then
                            PathName = FuncScenarioPath
                            CountDownValue = FuncCountDownValue
                            CountDownLimit = FuncCountDownLimit
                            FileIndex = FuncFileIndex
                            FileCount = FuncFileCount
                        Else
                            PathName = PicturePath
                            CountDownValue = PicCountDownValue
                            CountDownLimit = PicCountDownLimit
                            FileIndex = PicFileIndex
                            FileCount = PicFileCount
                        End If
                        If UCase(PathName) <> UCase("c:\ufis\system") Then
                            CountDownValue = CountDownValue - 1
                            If CountDownValue <= 0 Then
                                FileIndex = FileIndex + IncIdx
                                FilePattern = "*.jpg;*.bmp"
                                filename = GetNextFile(PathName, FilePattern, FileIndex, FileCount, True)
                                If filename <> "" Then LoadMyPicture = True
                            Else
                                If LastFileName <> LastPicFileName Then
                                    filename = LastPicFileName
                                    LoadMyPicture = True
                                End If
                            End If
                            If FuncScenario = True Then
                                FuncCountDownValue = CountDownValue
                                FuncCountDownLimit = CountDownLimit
                                FuncFileIndex = FileIndex
                                FuncFileCount = FileCount
                            Else
                                PicCountDownValue = CountDownValue
                                PicCountDownLimit = CountDownLimit
                                PicFileIndex = FileIndex
                                PicFileCount = FileCount
                                If filename <> "" Then LastPicFileName = filename
                            End If
                        End If
                    End If
                Case 1  'RefreshTimer (Cannot happen here)
                    'LoadMyPicture = True
                Case 2  'SlideTimer
                    LoadMyPicture = True
                Case 3  'MainFunctionScenario SlideShow
                    If SlideShow = False Then
                        UfisIntroTimer(0).Enabled = False
                        FuncScenario = True
                        If MyPath <> FuncScenarioPath Then
                            FuncFileIndex = 0
                            FuncFileCount = 0
                            FuncScenarioPath = MyPath
                        End If
                        If UCase(FuncScenarioPath) <> UCase("c:\ufis\system") Then
                            PathName = FuncScenarioPath
                            FilePattern = "*.jpg;*.bmp"
                            filename = GetNextFile(PathName, FilePattern, FuncFileIndex, FuncFileCount, True)
                            If filename <> "" Then LoadMyPicture = True
                        End If
                        FileCount = FuncFileCount
                    End If
                Case Else
            End Select
        End If
        
        If LoadMyPicture = True Then
            TryAgain = False
            If (filename <> "") Or (RefreshPicture = True) Then
                For i = 0 To 1
                    UfisIntroPicture(i).Visible = False
                    UfisIntroPicture(i).Stretch = False
                Next
                If RefreshPicture = False Then
                    LastFileName = filename
                    newEntry = "," & filename & "," & PathName & ","
                    If (SlideHoldRight = False) And (SlideDouble = False) Then
                        UfisIntroPicture(1).Picture = UfisIntroPicture(0).Picture
                        If SlideShow = True Then UfisIntroPicture(1).Tag = UfisIntroPicture(0).Tag
                    End If
                    If SlideHoldLeft = False Then
                        UfisIntroPicture(0).Picture = LoadPicture(PathName & "\" & filename)
                        If TryAgain = True Then
                            TryAgain = False
                            UfisIntroPicture(0).Picture = LoadPicture(PathName & "\" & filename)
                        End If
                        If (SlideShow = True) And (TryAgain = False) Then UfisIntroPicture(0).Tag = newEntry
                        If TryAgain = True Then UfisIntroPicture(0).Tag = "ERROR"
                        If (SlideDouble = True) And (SlideHoldRight = False) Then
                            UfisIntroPicture(1).Picture = UfisIntroPicture(0).Picture
                            If SlideShow = True Then UfisIntroPicture(1).Tag = UfisIntroPicture(0).Tag
                            If TryAgain = True Then UfisIntroPicture(1).Tag = "ERROR"
                        End If
                    ElseIf SlideHoldRight = False Then
                        UfisIntroPicture(1).Picture = LoadPicture(PathName & "\" & filename)
                        If (SlideShow = True) And (TryAgain = False) Then UfisIntroPicture(1).Tag = newEntry
                        If TryAgain = True Then UfisIntroPicture(1).Tag = "ERROR"
                    End If
                End If
                For i = 0 To 1
                    ActPicX = UfisIntroPicture(i).Width
                    ActPicY = UfisIntroPicture(i).height
                    NewSize = (fraWorkArea(0).Width - 150) / 2
                    If ActPicX > NewSize Then
                        UfisIntroPicture(i).Stretch = True
                        ActZoom = ActPicX / NewSize
                        UfisIntroPicture(i).Width = ActPicX / ActZoom
                        UfisIntroPicture(i).height = ActPicY / ActZoom
                    End If
                    NewSize = fraWorkArea(0).height - 300
                    ActPicX = UfisIntroPicture(i).Width
                    ActPicY = UfisIntroPicture(i).height
                    If ActPicY > NewSize Then
                        UfisIntroPicture(i).Stretch = True
                        ActZoom = ActPicY / NewSize
                        UfisIntroPicture(i).Width = ActPicX / ActZoom
                        UfisIntroPicture(i).height = ActPicY / ActZoom
                    End If
                    UfisIntroFrame(i).Width = UfisIntroPicture(i).Width
                    UfisIntroFrame(i).height = UfisIntroPicture(i).height
                    UfisIntroPicture(i).Visible = True
                    UfisIntroFrame(i).Visible = (SlideShow Or FuncScenario)
                Next
                If (SlideShow = False) And ((CalledFrom = 0) Or (CalledFrom = 3)) Then
                    If FileCount > 1 Then UfisIntroTimer(0).Enabled = True Else UfisIntroTimer(0).Enabled = False
                End If
            End If
            UfisIntroFrame(0).Top = ((fraWorkArea(0).height - UfisIntroPicture(0).height) \ 2) + 60
            UfisIntroFrame(0).Left = ((fraWorkArea(0).Width \ 2) - UfisIntroPicture(0).Width) \ 2
            UfisIntroFrame(1).Top = ((fraWorkArea(0).height - UfisIntroPicture(1).height) \ 2) + 60
            UfisIntroFrame(1).Left = (((fraWorkArea(0).Width \ 2) - UfisIntroPicture(1).Width) \ 2) + (fraWorkArea(0).Width \ 2)
            lblTitle(0).Left = UfisIntroFrame(0).Left + 15
            lblTitle(0).Top = UfisIntroFrame(0).Top - lblTitle(0).height - 30
            lblTitle(1).Left = UfisIntroFrame(1).Left + UfisIntroFrame(1).Width - lblTitle(1).Width - 15
            lblTitle(1).Top = UfisIntroFrame(1).Top - lblTitle(1).height - 30
            If lblTitle(0).Top > 120 Then
                lblTitle(0).Visible = UfisIntroFrame(0).Visible
                lblTitle(1).Visible = UfisIntroFrame(1).Visible
            Else
                lblTitle(0).Visible = False
                lblTitle(1).Visible = False
            End If
            LoadMyPicture = False
            If (TryAgain = False) And (SlideDouble = False) And (UfisIntroPicture(0).Width > UfisIntroPicture(0).height) Then LoadMyPicture = True
            If (UfisIntroFrame(0).Visible = False) And (SlideDouble = False) Then LoadMyPicture = True
            If LoadMyPicture = True Then
                UfisIntroPicture(2).Stretch = False
                UfisIntroPicture(2).Visible = False
                UfisIntroPicture(2).Picture = UfisIntroPicture(0).Picture
                UfisIntroPicture(2).Tag = UfisIntroPicture(0).Tag
                ActPicX = UfisIntroPicture(2).Width
                ActPicY = UfisIntroPicture(2).height
                NewSize = fraWorkArea(0).Width - 150
                If ActPicX > NewSize Then
                    UfisIntroPicture(2).Stretch = True
                    ActZoom = ActPicX / NewSize
                    UfisIntroPicture(2).Width = ActPicX / ActZoom
                    UfisIntroPicture(2).height = ActPicY / ActZoom
                End If
                NewSize = fraWorkArea(0).height - 300
                ActPicX = UfisIntroPicture(2).Width
                ActPicY = UfisIntroPicture(2).height
                If ActPicY > NewSize Then
                    UfisIntroPicture(2).Stretch = True
                    ActZoom = ActPicY / NewSize
                    UfisIntroPicture(2).Width = ActPicX / ActZoom
                    UfisIntroPicture(2).height = ActPicY / ActZoom
                End If
                UfisIntroFrame(2).Width = UfisIntroPicture(2).Width
                UfisIntroFrame(2).height = UfisIntroPicture(2).height
                UfisIntroFrame(2).Top = ((fraWorkArea(0).height - UfisIntroPicture(2).height) \ 2) + 60
                UfisIntroFrame(2).Left = (fraWorkArea(0).Width - UfisIntroPicture(2).Width) \ 2
                lblTitle(2).Left = UfisIntroFrame(2).Left + 15
                lblTitle(2).Top = UfisIntroFrame(2).Top - lblTitle(2).height - 30
                If UfisIntroPicture(2).Tag <> "ERROR" Then
                    UfisIntroPicture(2).Visible = True
                    UfisIntroFrame(2).Visible = True
                End If
            Else
                UfisIntroFrame(2).Visible = False
            End If
            If (lblTitle(2).Top > 120) And ((SlideShow Or FuncScenario) = True) Then
                lblTitle(2).Visible = UfisIntroFrame(2).Visible
            Else
                lblTitle(2).Visible = False
            End If
            fraWorkArea(0).Refresh
        End If
        If (SlideShow = False) And (FuncScenario = False) Then
            If (RefreshPicture = False) And (PicCountDownValue <= 0) Then PicCountDownValue = PicCountDownLimit
            UfisIntroLabel.Caption = CStr(PicCountDownValue)
            If PicCountDownValue > 0 Then UfisIntroTimer(0).Enabled = True
            If CountDownLimit > 60 Then
                UfisIntroLabel.Visible = True
            Else
                UfisIntroLabel.Visible = False
            End If
        Else
            UfisIntroLabel.Visible = False
        End If
    End If
    Exit Sub
ErrorHandle:
    PathName = "c:\ufis\system"
    filename = "RmsImpTool_INTRO.bmp"
    TryAgain = True
    Resume Next
End Sub





Private Sub MainTimer_Timer()
    Dim CurTime
    CurTime = Now
    lblTime(0).Caption = UCase(Format(CurTime, "dd.mmm.yyyy / hh:mm:ss"))
    If UfisServer.HostName = "LOCAL" Then
        lblTime(2).Caption = "( " & UCase(Format(CurTime, "dd.mmm.yyyy / hh:mm")) & " LT " & HomeAirport & " )"
        CurTime = DateAdd("n", -(UtcTimeDiff), CurTime)
        lblTime(1).Caption = "( " & UCase(Format(CurTime, "dd.mmm.yyyy / hh:mm")) & " z )"
    End If
    If SlideShow = True Then
        If FormIsVisible("frmFileDialog") = False Then chkTool(8).Value = 0
    End If
End Sub

Private Sub Slider1_Change(Index As Integer)
    Static OldValue(2) As Integer
    If Slider1(Index).Value <> OldValue(Index) Then
        InitSlideShowTimer
        OldValue(Index) = Slider1(Index).Value
    End If
End Sub

Private Sub InitSlideShowTimer()
    Dim Index As Integer
    If SlideShow = True Then Index = 2 Else Index = 0
    If Slider1(1).Value > 0 Then
        UfisIntroTimer(Index).Interval = Slider1(0).Value * 1000 \ Slider1(1).Value
    Else
        UfisIntroTimer(Index).Interval = Slider1(0).Value * 1000
    End If
    UfisIntroTimer(2).Enabled = False
    If ((SlideShow = True) And (Slider1(0).Value > 0) And (SlideHoldAll = False)) Or (Index = 0) Then
        UfisIntroTimer_Timer Index
    End If
End Sub

Private Sub SrvFkeyCnt_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    JumpToColoredLine TAB2(curWorkPanel), SrvFkeyCnt(Index).BackColor
End Sub

Private Sub TAB1_GotFocus(Index As Integer)
    fraLeftData(Index).ForeColor = vbBlue
End Sub

Private Sub TAB1_LostFocus(Index As Integer)
    fraLeftData(Index).ForeColor = vbButtonText
End Sub

Private Sub TAB2_GotFocus(Index As Integer)
    fraRightData(Index).ForeColor = vbBlue
End Sub

Private Sub TAB2_LostFocus(Index As Integer)
    fraRightData(Index).ForeColor = vbButtonText
End Sub

Private Sub TAB3_GotFocus(Index As Integer)
    fraTopData(Index).ForeColor = vbBlue
End Sub

Private Sub TAB3_LostFocus(Index As Integer)
    fraTopData(Index).ForeColor = vbButtonText
End Sub

Private Sub TAB1_RowSelectionChanged(Index As Integer, ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim ForeColor As Long
    Dim BackColor As Long
    If Selected Then
        TAB1(Index).GetLineColor LineNo, ForeColor, BackColor
        SetColorPointer 0, BackColor
    End If
    HandleTabRowSelectionChanged fraLeftData(Index), TAB1(Index), 1, Index, LineNo, Selected
End Sub

Private Sub TAB2_RowSelectionChanged(Index As Integer, ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim ForeColor As Long
    Dim BackColor As Long
    Dim addFields As String
    If Selected Then
        TAB2(Index).GetLineColor LineNo, ForeColor, BackColor
        SetColorPointer 1, BackColor
        If FormIsVisible("frmDifferences") = True Then
            addFields = "PENO,SDAY"
            frmDifferences.ShowData TAB2(Index), LineNo, addFields
        End If
    End If
    HandleTabRowSelectionChanged fraRightData(Index), TAB2(Index), 2, Index, LineNo, Selected
End Sub

Private Sub TAB3_RowSelectionChanged(Index As Integer, ByVal LineNo As Long, ByVal Selected As Boolean)
    HandleTabRowSelectionChanged fraTopData(Index), TAB3(Index), 3, Index, LineNo, Selected
End Sub
Private Sub HandleTabRowSelectionChanged(CurFrame As Frame, CurTab As TABLib.TAB, TabNo As Integer, Index As Integer, LineNo As Long, Selected As Boolean)
    Dim MaxLine As Long
    Dim SelLine As Long
    Dim tmpCap As String
    If LineNo >= 0 Then
        If Selected = True Then
            tmpCap = CurFrame.Caption
            tmpCap = GetItem(tmpCap, 1, "(")
            MaxLine = CurTab.GetLineCount
            SelLine = LineNo + 1
            tmpCap = tmpCap & " (" & CStr(SelLine) & "/" & CStr(MaxLine) & ")"
            CurFrame.Caption = tmpCap
            If ActTab = 0 Then
                Select Case Index
                    Case 1
                        SynchronizeKeyCardGrids TabNo, LineNo, Selected
                    Case 3
                        SynchronizeClockingGrids TabNo, LineNo, Selected
                    Case 4
                        SynchronizeHrAbsGrids TabNo, LineNo, Selected
                    Case Else
                End Select
            End If
        End If
    End If
End Sub

Private Sub TAB1_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    HandleTabDblClick TAB1(Index), Index, LineNo, ColNo
End Sub

Private Sub TAB2_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    HandleTabDblClick TAB2(Index), Index, LineNo, ColNo
End Sub

Private Sub TAB3_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    HandleTabDblClick TAB3(Index), Index, LineNo, ColNo
End Sub

Private Sub HandleTabDblClick(CurTab As TABLib.TAB, Index As Integer, LineNo As Long, ColNo As Long)
    Dim CurLine As Long
    If LineNo >= 0 Then
    ElseIf LineNo = -1 Then
        Screen.MousePointer = 11
        CurLine = CurTab.GetCurrentSelected
        If CurTab.CurrentSortColumn = ColNo Then
            If CurTab.SortOrderASC Then
                CurTab.Sort CStr(ColNo), False, True
            Else
                CurTab.Sort CStr(ColNo), True, True
            End If
        Else
            CurTab.Sort CStr(ColNo), True, True
        End If
        CurTab.AutoSizeByHeader = True
        CurTab.AutoSizeColumns
        CurTab.SetCurrentSelection CurLine
        'curtab.Refresh
        Screen.MousePointer = 0
    Else
    End If
End Sub
Private Sub AmpelTimer_Timer(Index As Integer)
    Dim tmpTag As String
    tmpTag = Trim(AmpelTimer(Index).Tag)
    If tmpTag = "" Then tmpTag = "Y"
    Select Case tmpTag
        Case "Y"
            chkAmpel(Index).Picture = Ampel(1).Picture
            AmpelTimer(Index).Tag = "D"
        Case "D"
            AmpelTimer(Index).Tag = "Y"
            chkAmpel(Index).Picture = Ampel(3).Picture
        Case Else
    End Select
End Sub



Private Sub timSrvLed_Timer()
    If shpSrvLed(2).FillColor <> vbYellow Then
        shpSrvLed(2).FillColor = vbYellow
        lblCurPos(2).ToolTipText = "Expecting the timer event from server " & UfisServer.HostName
        If lblTime(3).BackColor <> LightestYellow Then
            lblTime(3).Caption = " Server: " & UfisServer.HostName
            lblTime(3).BackColor = LightestYellow
            lblTime(3).ForeColor = vbBlack
        End If
    ElseIf UfisServer.HostName <> "LOCAL" Then
        shpSrvLed(2).FillColor = vbRed
        lblTime(3).Caption = " Missing Timer Event from server " & UfisServer.HostName
        lblTime(3).BackColor = vbRed
        lblTime(3).ForeColor = vbWhite
    Else
        shpSrvLed(0).FillColor = vbYellow
        shpSrvLed(1).FillColor = vbYellow
    End If
End Sub

Private Sub txtFuncFilter_GotFocus(Index As Integer)
    SetTextSelected
End Sub

Private Sub txtFuncFilter_KeyPress(Index As Integer, KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtVpfrDD_GotFocus(Index As Integer)
    SetTextSelected
End Sub
Private Sub txtVpfrMM_GotFocus(Index As Integer)
    SetTextSelected
End Sub
Private Sub txtVpfrYY_GotFocus(Index As Integer)
    SetTextSelected
End Sub

Private Sub txtVpfrYY_Change(Index As Integer)
    SetVpfrTagValue Index
End Sub
Private Sub txtVpfrMM_Change(Index As Integer)
    SetVpfrTagValue Index
End Sub
Private Sub txtVpfrDD_Change(Index As Integer)
    SetVpfrTagValue Index
End Sub
Private Sub SetVpfrTagValue(Index As Integer)
    Dim tmpTag As String
    Select Case Index
        Case 4
            tmpTag = txtVpfrYY(Index).Text
            tmpTag = tmpTag & txtVpfrMM(Index).Text
            tmpTag = tmpTag & txtVpfrDD(Index).Text
            txtVpfrYY(Index).Tag = tmpTag
            tmpTag = tmpTag & "," & Trim(txtVptoYY(curWorkPanel).Tag)
            If chkPanelIcon(curWorkPanel).Tag <> "" Then
                If tmpTag <> chkPanelIcon(curWorkPanel).Tag Then chkPanelIcon(curWorkPanel).BackColor = vbRed Else chkPanelIcon(curWorkPanel).BackColor = vbButtonFace
            End If
            SetWorkflowPointer chkTask(3), "Step 1:", "1"
            
        Case 8
            tmpTag = txtVpfrYY(Index).Text
            tmpTag = tmpTag & txtVpfrMM(Index).Text
            tmpTag = tmpTag & txtVpfrDD(Index).Text
            txtVpfrYY(Index).Tag = tmpTag
            tmpTag = tmpTag & "," & Trim(txtVptoYY(curWorkPanel).Tag)
            If chkPanelIcon(curWorkPanel).Tag <> "" Then
                If tmpTag <> chkPanelIcon(curWorkPanel).Tag Then chkPanelIcon(curWorkPanel).BackColor = vbRed Else chkPanelIcon(curWorkPanel).BackColor = vbButtonFace
            End If
            'SetWorkflowPointer chkTask(3), "Step 1:", "1"
        Case Else
    End Select
End Sub

Private Sub txtVptoDD_GotFocus(Index As Integer)
    SetTextSelected
End Sub
Private Sub txtVptoMM_GotFocus(Index As Integer)
    SetTextSelected
End Sub
Private Sub txtVptoYY_GotFocus(Index As Integer)
    SetTextSelected
End Sub

Private Sub txtVptoYY_Change(Index As Integer)
    SetVptoTagValue Index
End Sub
Private Sub txtVptoMM_Change(Index As Integer)
    SetVptoTagValue Index
End Sub
Private Sub txtVptoDD_Change(Index As Integer)
    SetVptoTagValue Index
End Sub
Private Sub SetVptoTagValue(Index As Integer)
    Dim tmpTag As String
    tmpTag = txtVptoYY(Index).Text
    tmpTag = tmpTag & txtVptoMM(Index).Text
    tmpTag = tmpTag & txtVptoDD(Index).Text
    txtVptoYY(Index).Tag = tmpTag
    tmpTag = Trim(txtVpfrYY(curWorkPanel).Tag) & "," & tmpTag
    If chkPanelIcon(curWorkPanel).Tag <> "" Then
        If tmpTag <> chkPanelIcon(curWorkPanel).Tag Then chkPanelIcon(curWorkPanel).BackColor = vbRed Else chkPanelIcon(curWorkPanel).BackColor = vbButtonFace
    End If
    SetWorkflowPointer chkTask(3), "Step 1:", "1"
End Sub

Private Sub SynchronizeHrAbsGrids(TabNo As Integer, LineNo As Long, Selected As Boolean)
    Dim tmpPeno As String
    Dim tmpSTFU As String
    Dim SelLineNo As Long
    Dim i As Integer
    If ActTab = 0 Then
        i = curWorkPanel
        ActTab = TabNo
        If Selected Then
            Select Case TabNo
                Case 1  'HrView
                    tmpPeno = TAB1(i).GetFieldValue(LineNo, "PENO")
                    LineNo = SelectLookupLineNo(TAB1(i), "PENO", tmpPeno, Selected, "", False, LineNo, 0)
                    LineNo = SelectLookupLineNo(TAB3(i), "PENO", tmpPeno, Selected, "", True, LineNo, -1)
                    tmpSTFU = TAB3(i).GetFieldValue(LineNo, "URNO")
                    LineNo = SelectLookupLineNo(TAB2(i), "STFU", tmpSTFU, Selected, "", True, LineNo, 1)
                Case 2  'DRRTAB
                    tmpSTFU = TAB2(i).GetFieldValue(LineNo, "STFU")
                    LineNo = SelectLookupLineNo(TAB2(i), "STFU", tmpSTFU, Selected, "", False, LineNo, 1)
                    LineNo = SelectLookupLineNo(TAB3(i), "URNO", tmpSTFU, Selected, "", True, LineNo, -1)
                    tmpPeno = TAB3(i).GetFieldValue(LineNo, "PENO")
                    LineNo = SelectLookupLineNo(TAB1(i), "PENO", tmpPeno, Selected, "", True, LineNo, 0)
                Case 3  'STFTAB
                    tmpPeno = TAB3(i).GetFieldValue(LineNo, "PENO")
                    tmpSTFU = TAB3(i).GetFieldValue(LineNo, "URNO")
                    LineNo = SelectLookupLineNo(TAB3(i), "PENO", tmpPeno, Selected, "", False, LineNo, -1)
                    LineNo = SelectLookupLineNo(TAB1(i), "PENO", tmpPeno, Selected, "", True, LineNo, 0)
                    LineNo = SelectLookupLineNo(TAB2(i), "STFU", tmpSTFU, Selected, "", True, LineNo, 1)
                Case Else
            End Select
            ActTab = 0
        End If
    End If
End Sub

Private Sub SynchronizeKeyCardGrids(TabNo As Integer, LineNo As Long, Selected As Boolean)
    Dim tmpPeno As String
    Dim tmpSTFU As String
    Dim SelLineNo As Long
    Dim i As Integer
    If ActTab = 0 Then
        i = curWorkPanel
        ActTab = TabNo
        If Selected Then
            Select Case TabNo
                Case 1  'Key Cards
                    tmpPeno = TAB1(i).GetFieldValue(LineNo, "PENO")
                    LineNo = SelectLookupLineNo(TAB1(i), "PENO", tmpPeno, Selected, "PENO", False, LineNo, 0)
                    LineNo = SelectLookupLineNo(TAB2(i), "PENO", tmpPeno, Selected, "PENO", True, LineNo, 1)
                Case 2  'STFTAB
                    tmpPeno = TAB2(i).GetFieldValue(LineNo, "PENO")
                    LineNo = SelectLookupLineNo(TAB2(i), "PENO", tmpPeno, Selected, "PENO", False, LineNo, 1)
                    LineNo = SelectLookupLineNo(TAB1(i), "PENO", tmpPeno, Selected, "PENO", True, LineNo, 0)
                Case Else
            End Select
            ActTab = 0
        End If
    End If
End Sub

Private Sub SynchronizeClockingGrids(TabNo As Integer, LineNo As Long, Selected As Boolean)
    Dim tmpPeno As String
    Dim tmpSTFU As String
    Dim SelLineNo As Long
    Dim i As Integer
    If ActTab = 0 Then
        i = curWorkPanel
        ActTab = TabNo
        If Selected Then
            Select Case TabNo
                Case 1  'Clocking File
                    tmpPeno = TAB1(i).GetFieldValue(LineNo, "CANO")
                    LineNo = SelectLookupLineNo(TAB1(i), "CANO", tmpPeno, Selected, "CANO", False, LineNo, 0)
                    LineNo = SelectLookupLineNo(TAB3(i), "CANO", tmpPeno, Selected, "CANO", True, LineNo, -1)
                    tmpSTFU = TAB3(i).GetFieldValue(LineNo, "URNO")
                    LineNo = SelectLookupLineNo(TAB2(i), "USTF", tmpSTFU, Selected, "PKNO", True, LineNo, 1)
                Case 2  'SPRTAB
                    tmpSTFU = TAB2(i).GetFieldValue(LineNo, "USTF")
                    LineNo = SelectLookupLineNo(TAB2(i), "USTF", tmpSTFU, Selected, "PKNO", False, LineNo, 1)
                    LineNo = SelectLookupLineNo(TAB3(i), "URNO", tmpSTFU, Selected, "CANO", True, LineNo, -1)
                    tmpPeno = TAB3(i).GetFieldValue(LineNo, "CANO")
                    LineNo = SelectLookupLineNo(TAB1(i), "CANO", tmpPeno, Selected, "CANO", True, LineNo, 0)
                Case 3  'STFTAB
                    tmpPeno = TAB3(i).GetFieldValue(LineNo, "CANO")
                    tmpSTFU = TAB3(i).GetFieldValue(LineNo, "URNO")
                    LineNo = SelectLookupLineNo(TAB3(i), "CANO", tmpPeno, Selected, "CANO", False, LineNo, -1)
                    LineNo = SelectLookupLineNo(TAB1(i), "CANO", tmpPeno, Selected, "CANO", True, LineNo, 0)
                    LineNo = SelectLookupLineNo(TAB2(i), "USTF", tmpSTFU, Selected, "PKNO", True, LineNo, 1)
                Case Else
            End Select
            ActTab = 0
        End If
    End If
End Sub


Private Function SelectLookupLineNo(CurTab As TABLib.TAB, IndexName As String, LookValue As String, Selected As Boolean, MarkFields As String, ScrollTo As Boolean, RefLineNo As Long, CounterIndex As Integer) As Long
    Dim tmpLine As String
    Dim tmpFieldList As String
    Dim LineNo As Long
    Dim FirstLine As Long
    Dim LineCount As Long
    Dim CurLinePos As Long
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim ForeColor As Long
    Dim BackColor As Long
    Dim CntColor(0 To 8) As Long
    Dim i As Integer
    MaxLine = CurTab.GetLineCount - 1
    For CurLine = 0 To MaxLine
        CurTab.ResetCellProperties CurLine
    Next
    FirstLine = -1
    If Selected Then
        If CounterIndex = 1 Then
            For i = 0 To chkMidImp.UBound
                CntColor(i) = 0
            Next
        End If
        LineCount = 0
        CurTab.SetInternalLineBuffer True
        tmpLine = CurTab.GetLinesByIndexValue(IndexName, LookValue, 0)
        If Val(tmpLine) > 0 Then
            tmpFieldList = MarkFields
            If tmpFieldList = "" Then tmpFieldList = CurTab.myName
            LineNo = CurTab.GetNextResultLine
            FirstLine = LineNo
            If LineNo >= 0 Then
                If ScrollTo = True Then
                    CurTab.OnVScrollTo LineNo - 2
                    CurTab.SetCurrentSelection LineNo
                End If
                While LineNo >= 0
                    If CounterIndex = 1 Then
                        CurTab.GetLineColor LineNo, ForeColor, BackColor
                        For i = 0 To chkMidImp.UBound
                            If BackColor = chkMidImp(i).BackColor Then
                                CntColor(i) = CntColor(i) + 1
                                Exit For
                            End If
                        Next
                    End If
                    SetTabCellObjects CurTab, LineNo, "MARKER", "Marker", tmpFieldList, True
                    LineCount = LineCount + 1
                    If LineNo = RefLineNo Then CurLinePos = LineCount
                    LineNo = CurTab.GetNextResultLine
                Wend
            End If
            If CounterIndex = 1 Then
                For i = 0 To chkMidImp.UBound
                    chkMidImp(i).Caption = CStr(CntColor(i))
                    chkMidImp(i).Tag = CStr(RefLineNo)
                Next
                fraMidImp(0).Tag = IndexName & "," & LookValue
                fraMidImp(0).Visible = True
            End If
        Else
            If CounterIndex = 1 Then fraMidImp(0).Visible = False
            CurTab.SetCurrentSelection -1
            CurTab.OnVScrollTo 0
        End If
        CurTab.SetInternalLineBuffer False
        If CounterIndex >= 0 Then lblCurPos(CounterIndex).Caption = CStr(CurLinePos) & "/" & CStr(LineCount)
    End If
    CurTab.Refresh
    SelectLookupLineNo = FirstLine
End Function
Private Function GetLookupFieldValues(CurTab As TABLib.TAB, IndexName As String, LookValue As String, GetFields As String, HitCount As Long, FirstLine As Long) As String
    Dim Result As String
    Dim tmpLine As String
    Dim LineNo As Long
    Dim CurLine As Long
    Dim MaxLine As Long
    Result = ""
    FirstLine = -1
    CurTab.SetInternalLineBuffer True
    tmpLine = CurTab.GetLinesByIndexValue(IndexName, LookValue, 0)
    HitCount = Val(tmpLine)
    If HitCount > 0 Then
        LineNo = CurTab.GetNextResultLine
        If LineNo >= 0 Then
            FirstLine = LineNo
            Result = CurTab.GetFieldValues(LineNo, GetFields)
            While LineNo >= 0
                LineNo = CurTab.GetNextResultLine
                If LineNo >= 0 Then
                    'Might be we need all lines
                End If
            Wend
        End If
    End If
    CurTab.SetInternalLineBuffer False
    GetLookupFieldValues = Result
End Function

Private Function GetNextFile(PathName As String, filename As String, FileIndex As Integer, FileCount As Integer, SwapAround As Boolean) As String
    Dim Result As String
    On Error Resume Next
    Result = ""
    MyFileList(0).Path = PathName
    MyFileList(0).filename = filename
    If FileIndex >= MyFileList(0).ListCount Then
        If SwapAround = True Then FileIndex = 0 Else FileIndex = -1
    End If
    If FileIndex >= 0 Then Result = MyFileList(0).List(FileIndex)
    FileIndex = FileIndex + 1
    FileCount = MyFileList(0).ListCount
    GetNextFile = Result
End Function

Private Sub UfisIntroPicture_DblClick(Index As Integer)
    Dim tmpTag As String
    If SlideShow = True Then
        tmpTag = UfisIntroPicture(Index).Tag
        frmFileDialog.chkCollect.Value = 1
        frmFileDialog.tabFoundFiles(1).InsertTextLine tmpTag, False
        frmFileDialog.tabFoundFiles(1).AutoSizeColumns
    End If
End Sub

Private Sub UfisIntroTimer_Timer(Index As Integer)
    Dim tmpCount As Long
    Dim tmpTime As Long
    Dim tmpRest As Long
    Dim tmpHour As Long
    Dim tmpMinute   As Long
    Dim tmpSec As Long
    Dim tmpSlide As String
    Dim tmpPath As String
    Dim tmpText As String
    Select Case Index
        Case 0
            CheckUfisIntroPicture 0, False, 0, "", ""
        Case 1  'Timer for Picture Resizing
            CheckUfisIntroPicture 1, True, 0, "", ""
            UfisIntroTimer(Index).Enabled = False
        Case 2  'Timer for Slides
            UfisIntroTimer(0).Enabled = False
            UfisIntroTimer(Index).Enabled = False
            tmpCount = frmFileDialog.GetNextResultLine(SlideIndex, tmpPath, tmpSlide)
            chkTool(12).Value = SlideIndex
            If tmpCount > 0 Then
                CheckUfisIntroPicture 2, False, 0, tmpSlide, tmpPath
                If Slider1(1).Value > 0 Then
                    tmpTime = tmpCount * CLng(Slider1(0).Value) \ CLng(Slider1(1).Value)
                Else
                    tmpTime = tmpCount * CLng(Slider1(0).Value)
                End If
                tmpHour = tmpTime \ 3600
                tmpRest = tmpTime - tmpHour * 3600
                tmpMinute = tmpRest \ 60
                tmpSec = tmpRest - tmpMinute * 60
                tmpText = CStr(tmpHour) & "h:" & CStr(tmpMinute) & "m:" & CStr(tmpSec) & "s"
                tmpText = CStr(tmpCount) & " / " & tmpText
                StatusBar1.Panels(1).Text = tmpText
                StatusBar1.Panels(2).Text = tmpPath & "\" & tmpSlide
            Else
                StatusBar1.Panels(1).Text = ""
                StatusBar1.Panels(2).Text = ""
            End If
            UfisIntroTimer(Index).Enabled = SlideShow
        Case Else
    End Select
End Sub

Private Sub ToggleBottomPanel(Index As Integer, SetActive As Boolean, EnableLeft As Boolean, EnableRight As Boolean, SaveStatus As Boolean)
    Dim i As Integer
    Dim newColor As Long
    Dim LineNo As Long
    Dim tmpTag As String
    If Index >= 0 Then
        LineNo = CLng(Index)
        If SaveStatus = True Then
            tabValues(0).SetColumnValue LineNo, 0, CStr(Int(SetActive))
            tabValues(0).SetColumnValue LineNo, 1, CStr(Int(EnableLeft))
            tabValues(1).SetColumnValue LineNo, 0, CStr(Int(SetActive))
            tabValues(1).SetColumnValue LineNo, 1, CStr(Int(EnableRight))
            tabValues(0).Refresh
            tabValues(1).Refresh
        Else
            fraMidImp(0).Visible = False
        End If
        If SetActive = True Then
            For i = 0 To CliFkeyCnt.UBound
                tmpTag = Trim(chkLeftImp(i).Tag)
                If tmpTag <> "" Then
                    chkLeftImp(i).Caption = tmpTag
                    tmpTag = CliFkeyCnt(i).Tag
                    newColor = Val(tmpTag)
                    CliFkeyCnt(i).BackColor = newColor
                    chkLeftImp(i).Enabled = EnableLeft
                    CliFkeyCnt(i).Enabled = True
                End If
                tmpTag = Trim(chkRightImp(i).Tag)
                If tmpTag <> "" Then
                    chkRightImp(i).Caption = tmpTag
                    tmpTag = SrvFkeyCnt(i).Tag
                    newColor = Val(tmpTag)
                    SrvFkeyCnt(i).BackColor = newColor
                    chkRightImp(i).Enabled = EnableRight
                    SrvFkeyCnt(i).Enabled = True
                End If
            Next
            For i = 0 To chkSearch.UBound
                'chkSearch(i).Enabled = True
            Next
            
            imgColorPointer(0).Tag = ""
            imgColorPointer(1).Tag = ""
            
            For i = 0 To chkMidImp.UBound
                chkMidImp(i).Enabled = EnableRight
            Next
        Else
            For i = 0 To CliFkeyCnt.UBound
                CliFkeyCnt(i).BackColor = vbButtonFace
                CliFkeyCnt(i).Caption = ""
                SrvFkeyCnt(i).BackColor = vbButtonFace
                SrvFkeyCnt(i).Caption = ""
                chkLeftImp(i).Caption = ""
                chkRightImp(i).Caption = ""
                chkLeftImp(i).Enabled = False
                chkRightImp(i).Enabled = False
                CliFkeyCnt(i).Enabled = False
                SrvFkeyCnt(i).Enabled = False
            Next
            For i = 0 To chkSearch.UBound
                chkSearch(i).Enabled = False
            Next
            For i = 0 To imgColorPointer.UBound
                imgColorPointer(i).Visible = False
            Next
            For i = 0 To chkMidImp.UBound
                chkMidImp(i).Enabled = False
            Next
        End If
    End If
End Sub

Private Sub AdjustBottomLabels(Index As Integer, LeftTab As TABLib.TAB, RightTab As TABLib.TAB, Restore As Boolean)
    Dim i As Integer
    Dim CurColor As Long
    Dim HitCount As String
    Dim LineNo As Long
    Dim ColNo As Long
    Dim SetEnable As Boolean
    Dim EnableLeft As Boolean
    Dim EnableRight As Boolean
    If Index >= 0 Then
        LineNo = CLng(Index)
        If Restore = True Then
            SetEnable = Val(tabValues(0).GetColumnValue(LineNo, 0))
            EnableLeft = Val(tabValues(0).GetColumnValue(LineNo, 1))
            EnableRight = Val(tabValues(1).GetColumnValue(LineNo, 1))
            ToggleBottomPanel Index, SetEnable, EnableLeft, EnableRight, True
        End If
        For i = 0 To CliFkeyCnt.UBound
            ColNo = i + 2
            If CliFkeyCnt(i).Enabled = True Then
                LeftTab.SetInternalLineBuffer True
                CurColor = CliFkeyCnt(i).BackColor
                HitCount = LeftTab.GetLinesByBackColor(CurColor)
                CliFkeyCnt(i).Caption = HitCount
                tabValues(0).SetColumnValue LineNo, ColNo, HitCount
                LeftTab.SetInternalLineBuffer False
            End If
            If SrvFkeyCnt(i).Enabled = True Then
                RightTab.SetInternalLineBuffer True
                CurColor = SrvFkeyCnt(i).BackColor
                HitCount = RightTab.GetLinesByBackColor(CurColor)
                SrvFkeyCnt(i).Caption = HitCount
                tabValues(1).SetColumnValue LineNo, ColNo, HitCount
                RightTab.SetInternalLineBuffer False
            End If
        Next
    End If
End Sub

Private Sub JumpToColoredLine(CurTab As TABLib.TAB, LookColor As Long)
    Dim CurScroll As Long
    Dim CurLine As Long
    Dim UseLine As Long
    Dim MaxLine As Long
    Dim ForeColor As Long
    Dim BackColor As Long
    Dim LineFound As Boolean
    Dim LoopCount As Integer
    Dim VisLines As Long
    CurLine = CurTab.GetCurrentSelected
    If CurLine < 0 Then CurLine = -1
    MaxLine = CurTab.GetLineCount - 1
    LineFound = False
    LoopCount = 0
    Do
        While CurLine <= MaxLine
            CurLine = CurLine + 1
            CurTab.GetLineColor CurLine, ForeColor, BackColor
            If BackColor = LookColor Then
                UseLine = CurLine
                LineFound = True
                CurLine = MaxLine + 1
            End If
        Wend
        LoopCount = LoopCount + 1
        CurLine = -1
    Loop While (LineFound = False) And (LoopCount < 2)
    If LineFound Then
        CurScroll = UseLine - 2
        CurTab.OnVScrollTo CurScroll
        CurTab.SetCurrentSelection UseLine
    End If
    CurTab.SetFocus
End Sub

Private Sub SetColorPointer(Index As Integer, LookColor As Long)
    Dim i As Integer
    Dim j As Integer
    Dim CurColor As Long
    Dim NewLeft As Long
    CurColor = Val(GetItem(imgColorPointer(Index).Tag, 1, ","))
    If LookColor <> CurColor Then
        imgColorPointer(Index).Visible = False
        If Index = 0 Then
            i = 0
            While i <= CliFkeyCnt.UBound
                If CliFkeyCnt(i).BackColor = LookColor Then
                    NewLeft = CliFkeyCnt(i).Left
                    imgColorPointer(Index).Visible = True
                    j = i
                    i = CliFkeyCnt.UBound
                End If
                i = i + 1
            Wend
        Else
            i = 0
            While i <= SrvFkeyCnt.UBound
                If SrvFkeyCnt(i).BackColor = LookColor Then
                    NewLeft = SrvFkeyCnt(i).Left
                    imgColorPointer(Index).Visible = True
                    j = i
                    i = SrvFkeyCnt.UBound
                End If
                i = i + 1
            Wend
        End If
        imgColorPointer(Index).Left = NewLeft
        imgColorPointer(Index).Tag = CStr(LookColor) & "," & CStr(j)
    End If
End Sub

Private Sub DontPlay()
    If MyMsgBox.CallAskUser(0, 0, 0, "Function Button Control", "Don't play with" & vbNewLine & "my spare parts!", "grobi", "", UserAnswer) = 1 Then DoNothing
End Sub

Private Sub HandleCardImport(keyTab As TABLib.TAB, stfTab As TABLib.TAB)
    Dim tmpFileResult As String
    Dim tmpFilePath As String
    Dim tmpFileFilter As String
    Dim CurPath As String
    Dim CurName As String
    Dim CurFile As String
    Dim UseFile As String
    Dim tmpHeader As String
    Dim tmpLength As String
    Dim tmpGridFields As String
    Dim tmpSortData As String
    Dim tmpTimeStamp As String
    Dim tmpLine As String
    Dim tmpData As String
    Dim tmpCmd As String
    Dim tmpTable As String
    Dim tmpFields As String
    Dim tmpSqlKey As String
    Dim fNo As Integer
    Dim ItemNo As Long
    Dim i As Integer
    Dim CdrAnsw As Boolean
    
    Screen.MousePointer = 0
    
    tmpGridFields = "LINE,STAT,CMDS,CANO,TYPE,TIME,PENO,NAME,SORT,' ',' ',' '"
    tmpHeader = "L,S,A,CANO,T,TIME,PENO,NAME,SORT,1,2,3"
    tmpLength = "10,10,10,10,10,10,10,10,10,10,10,10"
    
    keyTab.ResetContent
    stfTab.ResetContent
    keyTab.Refresh
    stfTab.Refresh
    
    keyTab.ShowVertScroller False
    keyTab.ShowHorzScroller False
    keyTab.HeaderString = tmpHeader
    keyTab.HeaderLengthString = tmpLength
    keyTab.LogicalFieldList = tmpGridFields
    keyTab.SetMainHeaderValues "3,3,2,4", "Line,Card Info,Personal Data,System", ",,,"
    keyTab.SetMainHeaderFont 17, False, False, True, 0, "Arial"
    keyTab.AutoSizeByHeader = True
    keyTab.AutoSizeColumns
    keyTab.Refresh
    
    frmFileDialog.SetTopPanelValues chkPanelIcon(curWorkPanel)
    tmpFileFilter = "{Key Card Files}{*.txt}{M}|{All Files}{*.*}{M}"
    tmpFilePath = GetIniEntry(myIniFile, "KEYCARDS", "", "PATH_TO_FILES", "c:\ufis\data")
    tmpFileResult = frmFileDialog.GetFileOpenDialog(0, tmpFilePath, tmpFileFilter, 0, 0, "")
    If tmpFileResult <> "" Then
        ItemNo = 0
        CurFile = GetRealItem(tmpFileResult, ItemNo, vbLf)
        While CurFile <> ""
            UseFile = Replace(CurFile, " ", "-", 1, -1, vbBinaryCompare)
            GetFileAndPath UseFile, CurName, CurPath
            tmpTimeStamp = GetItem(CurName, 1, ".")
            fNo = FreeFile()
            Open CurFile For Input As #fNo
            While Not EOF(fNo)
                Line Input #fNo, tmpLine
                'tmpGridFields = "LINE,STAT,CMDS,CANO,TYPE,TIME,PENO,NAME,SORT,' ',' ',' '"
                tmpData = "Y,," & ","
                tmpData = tmpData & Right(GetItem(tmpLine, 2, ","), 6) & ","
                tmpData = tmpData & GetItem(tmpLine, 1, ",") & ","
                tmpData = tmpData & tmpTimeStamp & ","
                tmpData = tmpData & Right("0000" & GetItem(tmpLine, 4, ","), 4) & ","
                tmpData = tmpData & GetItem(tmpLine, 3, ",") & ","
                tmpSortData = Right("0000" & GetItem(tmpLine, 4, ","), 4) & "-"
                tmpSortData = tmpSortData & tmpTimeStamp & "-" & GetItem(tmpLine, 1, ",") & ","
                tmpData = tmpData & tmpSortData & ","
                tmpData = tmpData & ",,,"
                keyTab.InsertTextLine tmpData, False
            Wend
            Close #fNo
            ItemNo = ItemNo + 1
            CurFile = GetRealItem(tmpFileResult, ItemNo, vbLf)
        Wend
        keyTab.ShowHorzScroller True
        keyTab.ShowVertScroller True
        SetTabSortCols keyTab, "PENO"
        SetTabIndexes keyTab, "PENO", "PENO", True
        
        keyTab.AutoSizeColumns
        keyTab.Refresh
    
        tmpGridFields = "LINE,STAT,CMDS,CANO,PENO,LANM,FINM,URNO,' ',' ',' '"
        tmpHeader = "L,S,A,CANO,PENO,LANM,FINM,URNO,1,2,3"
        tmpLength = "10,10,10,10,10,10,10,10,10,10,10"
        
        stfTab.ResetContent
        stfTab.ShowVertScroller False
        stfTab.ShowHorzScroller False
        stfTab.HeaderString = tmpHeader
        stfTab.HeaderLengthString = tmpLength
        stfTab.LogicalFieldList = tmpGridFields
        stfTab.SetMainHeaderValues "3,1,3,4", "Line,Card,Personal Data,System", ",,,"
        stfTab.SetMainHeaderFont 17, False, False, True, 0, "Arial"
        stfTab.AutoSizeByHeader = True
        stfTab.AutoSizeColumns
        stfTab.Refresh
        
        chkTask(14).Tag = "DUMMY"
        chkTask(14).Value = 1
        chkTask(14).Refresh
        
        tmpData = keyTab.SelectDistinct(CStr(6), "'", "'", ",", True)
        If tmpData <> "" Then
            tmpSqlKey = "WHERE PENO IN (" & tmpData & ")"
        End If
        
        tmpCmd = "RTA"
        tmpTable = "STFTAB"
        tmpFields = "'Y',' ',' ',CANO,PENO,LANM,FINM,URNO,' ',' ',' '"
    
        Screen.MousePointer = 11
        
        stfTab.CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
        stfTab.CedaHopo = UfisServer.HOPO
        stfTab.CedaIdentifier = ""
        stfTab.CedaPort = "3357"
        stfTab.CedaReceiveTimeout = "250"
        stfTab.CedaRecordSeparator = vbLf
        stfTab.CedaSendTimeout = "250"
        stfTab.CedaServerName = UfisServer.HostName
        stfTab.CedaTabext = UfisServer.TblExt
        stfTab.CedaUser = UfisServer.ModName
        stfTab.CedaWorkstation = UfisServer.GetMyWorkStationName
        
        If CedaIsConnected Then
            CdrAnsw = stfTab.CedaAction(tmpCmd, tmpTable, tmpFields, "", tmpSqlKey)
        ElseIf UfisServer.HostName = "LOCAL" Then
            stfTab.ReadFromFile "c:\tmp\RmsImportTab2_1.txt"
        End If
        
        stfTab.ShowVertScroller True
        stfTab.ShowHorzScroller True
        stfTab.AutoSizeByHeader = True
        SetTabSortCols stfTab, "PENO"
        SetTabIndexes stfTab, "PENO", "PENO", True
        stfTab.AutoSizeColumns
        stfTab.Refresh
        
        chkTask(14).Tag = ""
        chkTask(14).Value = 0
    End If
    Screen.MousePointer = 0
    
End Sub

Private Sub HandleCardCheck(keyTab As TABLib.TAB, stfTab As TABLib.TAB)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim NxtLine As Long
    Dim CurPeno As String
    Dim NxtPeno As String
    SetTabSortCols keyTab, "SORT"
    keyTab.AutoSizeColumns
    keyTab.Refresh
    MaxLine = keyTab.GetLineCount - 2
    For CurLine = 0 To MaxLine
        NxtLine = CurLine + 1
        CurPeno = keyTab.GetFieldValue(CurLine, "PENO")
        NxtPeno = keyTab.GetFieldValue(NxtLine, "PENO")
        If CurPeno = NxtPeno Then
            keyTab.SetLineColor CurLine, vbBlack, LightGray
            keyTab.SetColumnValue CurLine, 0, "N"
        End If
    Next
    SetTabSortCols keyTab, "PENO"
    keyTab.AutoSizeColumns
    keyTab.Refresh
End Sub

Private Sub ClearKeyCardData(keyTab As TABLib.TAB, stfTab As TABLib.TAB)
    SetTabIndexes keyTab, "PENO", "PENO", False
    ClearTabData keyTab, "LINE", "N", "", ""
    SetTabIndexes keyTab, "PENO", "PENO", True
End Sub

Private Sub CompareKeyCardData(keyTab As TABLib.TAB, stfTab As TABLib.TAB)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim stfHitLine As Long
    Dim stfHitCount As Long
    Dim tmpKeyPeno As String
    Dim tmpStfPeno As String
    Dim tmpKeyCano As String
    Dim tmpStfCano As String
    Dim tmpType As String
    MaxLine = keyTab.GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpType = keyTab.GetFieldValue(CurLine, "TYPE")
        tmpKeyPeno = keyTab.GetFieldValue(CurLine, "PENO")
        tmpStfPeno = GetLookupFieldValues(stfTab, "PENO", tmpKeyPeno, "PENO", stfHitCount, stfHitLine)
        If stfHitCount > 0 Then
            tmpKeyCano = Trim(keyTab.GetFieldValue(CurLine, "CANO"))
            tmpStfCano = Trim(stfTab.GetFieldValue(stfHitLine, "CANO"))
            If tmpType = "I" Then
                If tmpKeyCano <> tmpStfCano Then
                    SetTabCellObjects stfTab, stfHitLine, "DECO", "CellDiffGry", "CANO", True
                    stfTab.SetColumnValue stfHitLine, 1, "C"
                    stfTab.SetColumnValue stfHitLine, 2, "U"
                    stfTab.SetLineTag stfHitLine, tmpKeyCano
                    SetTabCellObjects keyTab, CurLine, "DECO", "CellDiffGry", "TYPE,CANO", True
                Else
                    stfTab.SetLineColor stfHitLine, vbBlack, LightGray
                    stfTab.SetColumnValue stfHitLine, 1, "C"
                    stfTab.SetColumnValue stfHitLine, 2, "-"
                    stfTab.SetColumnValue stfHitLine, 0, "N"
                    keyTab.SetLineColor CurLine, vbBlack, LightGray
                    keyTab.SetColumnValue CurLine, 0, "N"
                End If
            Else
                If (tmpStfCano <> "") And (tmpStfCano <> "------") Then
                    SetTabCellObjects stfTab, stfHitLine, "DECO", "CellDiffGry", "CANO", True
                    stfTab.SetColumnValue stfHitLine, 1, "C"
                    stfTab.SetColumnValue stfHitLine, 2, "U"
                    keyTab.SetLineColor CurLine, vbWhite, vbRed
                    SetTabCellObjects keyTab, CurLine, "DECO", "CellDiffGry", "TYPE,CANO", True
                    stfTab.SetLineTag stfHitLine, "------"
                Else
                    stfTab.SetLineColor stfHitLine, vbBlack, LightGray
                    stfTab.SetColumnValue stfHitLine, 0, "N"
                    stfTab.SetColumnValue stfHitLine, 1, "C"
                    stfTab.SetColumnValue stfHitLine, 2, "-"
                    keyTab.SetLineColor CurLine, vbBlack, LightGray
                    keyTab.SetColumnValue CurLine, 0, "N"
                End If
            End If
        Else
            If tmpType = "I" Then
                keyTab.SetLineColor CurLine, vbBlack, vbGreen
                keyTab.SetColumnValue CurLine, 0, "N"
            Else
                keyTab.SetLineColor CurLine, vbBlack, LightGray
                keyTab.SetColumnValue CurLine, 0, "N"
            End If
        End If
    Next
    keyTab.Refresh
    stfTab.Refresh
End Sub

Private Sub HandleKeyCardUpdate(stfTab As TABLib.TAB, CurColor As Long)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim linCount As Long
    Dim tmpBool As String
    Dim tmpCano As String
    Dim tmpUrno As String
    Dim tmpData As String
    Dim outData As String
    Dim outFields As String
    outFields = "CANO"
    outData = "*CMD*,STFTAB,URT," & CStr(ItemCount(outFields, ",")) & "," & outFields & ",[URNO=:VURNO]" & vbLf
    MaxLine = stfTab.GetLineCount - 1
    linCount = 0
    For CurLine = 0 To MaxLine
        tmpBool = stfTab.GetFieldValue(CurLine, "LINE")
        If tmpBool = "Y" Then
            linCount = linCount + 1
            tmpCano = stfTab.GetLineTag(CurLine)
            tmpUrno = stfTab.GetFieldValue(CurLine, "URNO")
            tmpData = tmpCano & "," & tmpUrno
            outData = outData & tmpData & vbLf
        End If
    Next
    If linCount > 0 Then
        UfisServer.CallCeda CedaDataAnswer, "REL", "STFTAB", outFields, outData, "LATE", "", 0, False, False
    End If
End Sub

Private Sub HandleClockImport(keyTab As TABLib.TAB, stfTab As TABLib.TAB, SPRTAB As TABLib.TAB)
    Dim tmpFileResult As String
    Dim tmpFilePath As String
    Dim tmpFileFilter As String
    Dim tmpFileName As String
    Dim CurPath As String
    Dim CurName As String
    Dim CurFile As String
    Dim UseFile As String
    Dim tmpHeader As String
    Dim tmpLength As String
    Dim tmpGridFields As String
    Dim tmpSortData As String
    Dim tmpTimeStamp As String
    Dim tmpLine As String
    Dim tmpCano As String
    Dim tmpType As String
    Dim tmpDate As String
    Dim tmpMinDate As String
    Dim tmpMaxDate As String
    Dim tmpTime As String
    Dim tmpInOut As String
    Dim tmpData As String
    Dim tmpCmd As String
    Dim tmpTable As String
    Dim tmpFields As String
    Dim tmpSqlKey As String
    Dim fNo As Integer
    Dim ItemNo As Long
    Dim i As Integer
    Dim CdrAnsw As Boolean
    
    Screen.MousePointer = 0
    
    tmpGridFields = "LINE,STAT,CMDS,PENO,CANO,TYPE,DATE,TIME,STFU,SORT,' ',' ',' '"
    tmpHeader = "L,S,A,PENO,CANO,TYPE,DATE,TIME,STFU,SORT,1,2,3"
    tmpLength = "10,10,10,10,10,10,10,10,10,10,10,10,10"
    
    keyTab.ResetContent
    stfTab.ResetContent
    SPRTAB.ResetContent
    keyTab.Refresh
    stfTab.Refresh
    SPRTAB.Refresh
    
    keyTab.ShowVertScroller False
    keyTab.ShowHorzScroller False
    keyTab.HeaderString = tmpHeader
    keyTab.HeaderLengthString = tmpLength
    keyTab.LogicalFieldList = tmpGridFields
    keyTab.SetMainHeaderValues "3,1,4,5", "Line,EMPL,Clocking Info,System", ",,,,"
    keyTab.SetMainHeaderFont 17, False, False, True, 0, "Arial"
    keyTab.AutoSizeByHeader = True
    keyTab.AutoSizeColumns
    keyTab.Refresh
    
    frmFileDialog.SetTopPanelValues chkPanelIcon(curWorkPanel)
    
    tmpFilePath = GetIniEntry(myIniFile, "CLOCKING", "", "PATH_TO_FILES", "c:\ufis\data")
    If Ckb_Interface.Value = Unchecked Then
        tmpFileFilter = "{Clocking Files}{*.txt}{M}|{All Files}{*.*}{M}"
        tmpFileResult = frmFileDialog.GetFileOpenDialog(0, tmpFilePath, tmpFileFilter, 0, 0, "")
    Else
    
        tmpFileName = GetIniEntry(myIniFile, "CLOCKING", "", "CLOCK_FILE_NAME", "")
        If (Len(Trim(tmpFileName)) = 0) Then
            tmpFileName = Format$(DateAdd("n", -1, Now), "yyyy-dd-mm hmm") & ".txt"
        End If
        
        
        
        tmpFileResult = tmpFilePath + "\" + tmpFileName
        If LogFile Then WriteToLogFile "Checking for file " & tmpFileResult
    End If
      
    
    
    If tmpFileResult <> "" Then
        tmpMinDate = "99999999"
        tmpMaxDate = "00000000"
        ItemNo = 0
        CurFile = GetRealItem(tmpFileResult, ItemNo, vbLf)
        While CurFile <> ""
            UseFile = Replace(CurFile, " ", "-", 1, -1, vbBinaryCompare)
            GetFileAndPath UseFile, CurName, CurPath
            tmpTimeStamp = GetItem(CurName, 1, ".")
            fNo = FreeFile()
            ' Check if the FIle Exists
            
    If Ckb_Interface.Value = Checked Then
    If FileExists(tmpFileResult) Then
    
    Else
    tmpFileResult = ""
    InterfaceError = True
    Exit Sub
    End If
    
    End If
            Open CurFile For Input As #fNo
            
            
            While Not EOF(fNo)
                Line Input #fNo, tmpLine
                tmpCano = Right("000000" & GetItem(tmpLine, 1, ","), 6)
                tmpDate = GetItem(tmpLine, 2, ",")
                tmpDate = tmpDate & GetItem(tmpLine, 3, ",")
                tmpDate = tmpDate & GetItem(tmpLine, 4, ",")
                tmpTime = GetItem(tmpLine, 5, ",")
                tmpTime = tmpTime & GetItem(tmpLine, 6, ",")
                tmpType = GetItem(tmpLine, 7, ",")
                Select Case tmpType
                    Case "CLIN"
                        tmpInOut = "I"
                    Case "COUT"
                        tmpInOut = "O"
                    Case Else
                        tmpInOut = "?"
                End Select
                tmpSortData = tmpCano & "-" & tmpDate & "-" & tmpTime & "-" & tmpInOut
                'tmpGridFields = "LINE,STAT,CMDS,PENO,CANO,TYPE,DATE,TIME,STFU,SORT,' ',' ',' '"
                tmpData = "Y,," & ","
                tmpData = tmpData & "" & ","
                tmpData = tmpData & tmpCano & ","
                tmpData = tmpData & tmpType & ","
                tmpData = tmpData & tmpDate & ","
                tmpData = tmpData & tmpTime & ","
                tmpData = tmpData & "" & ","
                tmpData = tmpData & tmpSortData & ","
                tmpData = tmpData & ",,,"
                keyTab.InsertTextLine tmpData, False
                If tmpDate < tmpMinDate Then tmpMinDate = tmpDate
                If tmpDate > tmpMaxDate Then tmpMaxDate = tmpDate
            Wend
           
            
            Close #fNo
            ItemNo = ItemNo + 1
            CurFile = GetRealItem(tmpFileResult, ItemNo, vbLf)
        Wend
        keyTab.ShowHorzScroller True
        keyTab.ShowVertScroller True
        SetTabSortCols keyTab, "SORT"
        SetTabIndexes keyTab, "CANO", "CANO", True
        
        keyTab.AutoSizeColumns
        keyTab.Refresh
    
        tmpGridFields = "LINE,STAT,CMDS,CANO,PENO,LANM,FINM,URNO,' ',' ',' '"
        tmpHeader = "L,S,A,CANO,PENO,LANM,FINM,URNO,1,2,3"
        tmpLength = "10,10,10,10,10,10,10,10,10,10,10"
        
        stfTab.ResetContent
        stfTab.ShowVertScroller False
        stfTab.ShowHorzScroller False
        stfTab.HeaderString = tmpHeader
        stfTab.HeaderLengthString = tmpLength
        stfTab.LogicalFieldList = tmpGridFields
        stfTab.SetMainHeaderValues "3,1,3,4", "Line,Card,Personal Data,System", ",,,"
        stfTab.SetMainHeaderFont 17, False, False, True, 0, "Arial"
        stfTab.AutoSizeByHeader = True
        stfTab.AutoSizeColumns
        stfTab.Refresh
        
        chkTask(16).Tag = "DUMMY"
        chkTask(16).Value = 1
        chkTask(16).Refresh
        
        tmpData = keyTab.SelectDistinct(CStr(4), "'", "'", ",", True)
        If tmpData <> "" Then
            tmpSqlKey = "WHERE CANO IN (" & tmpData & ")"
        End If
        
        tmpCmd = "RTA"
        tmpTable = "STFTAB"
        tmpFields = "'Y',' ',' ',CANO,PENO,LANM,FINM,URNO,' ',' ',' '"
    
        Screen.MousePointer = 11
        
        stfTab.CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
        stfTab.CedaHopo = UfisServer.HOPO
        stfTab.CedaIdentifier = ""
        stfTab.CedaPort = "3357"
        stfTab.CedaReceiveTimeout = "250"
        stfTab.CedaRecordSeparator = vbLf
        stfTab.CedaSendTimeout = "250"
        stfTab.CedaServerName = UfisServer.HostName
        stfTab.CedaTabext = UfisServer.TblExt
        stfTab.CedaUser = UfisServer.ModName
        stfTab.CedaWorkstation = UfisServer.GetMyWorkStationName
        
        If CedaIsConnected Then
            CdrAnsw = stfTab.CedaAction(tmpCmd, tmpTable, tmpFields, "", tmpSqlKey)
        ElseIf UfisServer.HostName = "LOCAL" Then
            stfTab.ReadFromFile "c:\tmp\RmsImportTab3_3.txt"
        End If
        
        stfTab.ShowVertScroller True
        stfTab.ShowHorzScroller True
        stfTab.AutoSizeByHeader = True
        SetTabSortCols stfTab, "PENO"
        SetTabIndexes stfTab, "URNO,CANO", "URNO,CANO", True
        
        stfTab.AutoSizeColumns
        stfTab.Refresh
        
        
        tmpGridFields = "LINE,STAT,CMDS,PKNO,FCOL,ACTI,USTF,URNO,KEYS,' ',' ',' '"
        tmpHeader = "L,S,A,PKNO,FCOL,ACTI,USTF,URNO,KEYS,1,2,3"
        tmpLength = "10,10,10,10,10,10,10,10,10,10,10,10"
        
        SPRTAB.ResetContent
        SPRTAB.ShowVertScroller False
        SPRTAB.ShowHorzScroller False
        SPRTAB.HeaderString = tmpHeader
        SPRTAB.HeaderLengthString = tmpLength
        SPRTAB.LogicalFieldList = tmpGridFields
        SPRTAB.SetMainHeaderValues "3,1,2,6", "Line,EMPL,Clocking,System", ",,,"
        SPRTAB.SetMainHeaderFont 17, False, False, True, 0, "Arial"
        SPRTAB.AutoSizeByHeader = True
        SPRTAB.AutoSizeColumns
        SPRTAB.Refresh
        
        tmpMinDate = tmpMinDate & "000000"
        tmpMaxDate = tmpMaxDate & "235959"
        
        tmpData = stfTab.SelectDistinct(CStr(7), "'", "'", ",", True)
        If tmpData <> "" Then
            tmpSqlKey = "WHERE (USTF IN (" & tmpData & "))"
            tmpSqlKey = tmpSqlKey & " AND (ACTI BETWEEN '" & tmpMinDate & "' AND '" & tmpMaxDate & "')"
        End If
        
        tmpCmd = "RTA"
        tmpTable = "SPRTAB"
        tmpFields = "'N',' ',' ',PKNO,FCOL,ACTI,USTF,URNO,' ',' ',' ',' '"
    
        SPRTAB.CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
        SPRTAB.CedaHopo = UfisServer.HOPO
        SPRTAB.CedaIdentifier = ""
        SPRTAB.CedaPort = "3357"
        SPRTAB.CedaReceiveTimeout = "250"
        SPRTAB.CedaRecordSeparator = vbLf
        SPRTAB.CedaSendTimeout = "250"
        SPRTAB.CedaServerName = UfisServer.HostName
        SPRTAB.CedaTabext = UfisServer.TblExt
        SPRTAB.CedaUser = UfisServer.ModName
        SPRTAB.CedaWorkstation = UfisServer.GetMyWorkStationName
        
        If CedaIsConnected Then
            CdrAnsw = SPRTAB.CedaAction(tmpCmd, tmpTable, tmpFields, "", tmpSqlKey)
        ElseIf UfisServer.HostName = "LOCAL" Then
            SPRTAB.ReadFromFile "c:\tmp\RmsImportTab2_3.txt"
        End If
        
        SPRTAB.ShowVertScroller True
        SPRTAB.ShowHorzScroller True
        SPRTAB.AutoSizeByHeader = True
        SetTabSortCols SPRTAB, "PKNO"
        SetTabIndexes SPRTAB, "PKNO,USTF", "PKNO,USTF", True
        
        SPRTAB.AutoSizeColumns
        SPRTAB.Refresh
        
        chkTask(16).Tag = ""
        chkTask(16).Value = 0
    End If
    Screen.MousePointer = 0
    
End Sub

Private Sub HandleClockCheck(keyTab As TABLib.TAB, stfTab As TABLib.TAB, SPRTAB As TABLib.TAB)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim stfHitCount As Long
    Dim stfHitLine As Long
    Dim tmpKeyCano As String
    Dim tmpStfData As String
    Dim tmpSprKeys As String
    Dim tmpKeySort As String
    Dim tmpOldSort As String
    SetTabSortCols keyTab, "SORT"
    keyTab.AutoSizeColumns
    keyTab.Refresh
    MaxLine = keyTab.GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpKeyCano = keyTab.GetFieldValue(CurLine, "CANO")
        tmpStfData = GetLookupFieldValues(stfTab, "CANO", tmpKeyCano, "PENO,URNO", stfHitCount, stfHitLine)
        If stfHitCount > 0 Then
            keyTab.SetFieldValues CurLine, "PENO,STFU", tmpStfData
        Else
            keyTab.SetLineColor CurLine, vbBlack, LightestRed
            keyTab.SetColumnValue CurLine, 0, "N"
        End If
    Next
    
    MaxLine = keyTab.GetLineCount - 1
    tmpOldSort = keyTab.GetFieldValue(0, "SORT")
    For CurLine = 1 To MaxLine
        tmpKeySort = keyTab.GetFieldValue(CurLine, "SORT")
        If tmpKeySort = tmpOldSort Then
            keyTab.SetLineColor CurLine, vbBlack, vbYellow
            keyTab.SetFieldValues CurLine, "LINE", "N"
            
        End If
        tmpOldSort = tmpKeySort
    Next
    keyTab.AutoSizeColumns
    keyTab.Refresh
    
    MaxLine = SPRTAB.GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpSprKeys = SPRTAB.GetFieldValues(CurLine, "PKNO,ACTI,FCOL")
        tmpSprKeys = Replace(tmpSprKeys, ",", "-", 1, -1, vbBinaryCompare)
        SPRTAB.SetFieldValues CurLine, "KEYS", tmpSprKeys
        If LogFile Then WriteToLogFile "In Clock Check curLine:" & CurLine & " tmpSprKeys:" & tmpSprKeys
    Next
    SPRTAB.AutoSizeColumns
    SPRTAB.Refresh
End Sub

Private Sub ClearClockingData(keyTab As TABLib.TAB, stfTab As TABLib.TAB)
    SetTabIndexes keyTab, "CANO", "CANO", False
    ClearTabData keyTab, "LINE", "N", "", ""
    SetTabIndexes keyTab, "CANO", "CANO", True
End Sub

Private Sub CompareClockingData(keyTab As TABLib.TAB, stfTab As TABLib.TAB, SPRTAB As TABLib.TAB)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim LineNo As Long
    Dim sprHitCount As Long
    Dim sprHitLine As Long
    Dim tmpData As String
    Dim tmpType As String
    Dim tmpDate As String
    Dim tmpTime As String
    Dim tmpActi As String
    Dim tmpFcol As String
    Dim tmpPkno As String
    Dim tmpKeys As String
    Dim tmpUrno As String
    Dim tmpCmds As String
    Dim tmpBool As String
    
    'Step 1: Check uploaded identical records
    SetTabIndexes SPRTAB, "PKNO,USTF,KEYS", "PKNO,USTF,KEYS", False
    ClearTabData SPRTAB, "CMDS", "I", "", ""
    SetTabIndexes SPRTAB, "KEYS", "KEYS", True
    MaxLine = keyTab.GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpBool = keyTab.GetFieldValue(CurLine, "LINE")
        If tmpBool = "Y" Then
            tmpPkno = keyTab.GetFieldValue(CurLine, "PENO")
            tmpDate = keyTab.GetFieldValue(CurLine, "DATE")
            tmpTime = keyTab.GetFieldValue(CurLine, "TIME")
            tmpActi = tmpDate & tmpTime & "00"
            tmpType = keyTab.GetFieldValue(CurLine, "TYPE")
            Select Case tmpType
                Case "CLIN"
                    tmpFcol = "1"
                Case "COUT"
                    tmpFcol = "0"
                Case Else
                    tmpFcol = "?"
            End Select
            tmpKeys = tmpPkno & "-" & tmpActi & "-" & tmpFcol
            tmpUrno = Trim(GetLookupFieldValues(SPRTAB, "KEYS", tmpKeys, "URNO", sprHitCount, sprHitLine))
            If sprHitCount > 0 Then
                If tmpUrno <> "" Then
                    tmpCmds = Trim(SPRTAB.GetFieldValue(sprHitLine, "CMDS"))
                    If tmpCmds = "" Then
                        keyTab.SetLineColor CurLine, vbBlack, LightGray
                        keyTab.SetFieldValues CurLine, "LINE", "N"
                        SPRTAB.SetLineColor sprHitLine, vbBlack, LightGray
                        SPRTAB.SetFieldValues sprHitLine, "LINE", "N"
                    End If
                    If tmpCmds = "-" Then
                        keyTab.SetLineColor CurLine, vbBlack, LightYellow
                        keyTab.SetFieldValues CurLine, "LINE", "N"
                    End If
                End If
            End If
            If LogFile Then WriteToLogFile "In CompareClockingData (identical) curLine:" & CurLine & " tmpPkno:" & tmpPkno
        End If
    Next
    
    'Step 2: Insert new records
    SetTabIndexes SPRTAB, "KEYS", "KEYS", False
    LineNo = SPRTAB.GetLineCount - 1
    MaxLine = keyTab.GetLineCount - 1
    For CurLine = 0 To MaxLine
        'KeyGridFields = "LINE,STAT,CMDS,PENO,CANO,TYPE,DATE,TIME,STFU,SORT,' ',' ',' '"
        'SprGridFields = "LINE,STAT,CMDS,PKNO,FCOL,ACTI,USTF,URNO,' ',' ',' '"
        tmpBool = keyTab.GetFieldValue(CurLine, "LINE")
        If tmpBool = "Y" Then
            tmpPkno = keyTab.GetFieldValue(CurLine, "PENO")
            tmpDate = keyTab.GetFieldValue(CurLine, "DATE")
            tmpTime = keyTab.GetFieldValue(CurLine, "TIME")
            tmpActi = tmpDate & tmpTime & "00"
            tmpType = keyTab.GetFieldValue(CurLine, "TYPE")
            Select Case tmpType
                Case "CLIN"
                    tmpFcol = "1"
                Case "COUT"
                    tmpFcol = "0"
                Case Else
                    tmpFcol = "?"
            End Select
            tmpKeys = tmpPkno & "-" & tmpActi & "-" & tmpFcol
            tmpData = "Y,,I" & ","
            tmpData = tmpData & tmpPkno & ","
            tmpData = tmpData & tmpFcol & ","
            tmpData = tmpData & tmpActi & ","
            tmpData = tmpData & keyTab.GetFieldValue(CurLine, "STFU") & ","
            tmpData = tmpData & "" & ","
            tmpData = tmpData & tmpKeys & ","
            tmpData = tmpData & ",,,"
            SPRTAB.InsertTextLine tmpData, False
            LineNo = LineNo + 1
            SPRTAB.SetLineColor LineNo, vbBlack, vbGreen
            If LogFile Then WriteToLogFile "In CompareClockingData (Insert) curLine:" & CurLine & " tmpSprKeys:" & tmpData
        End If
    Next
    
    'Step 3: Check double records
    SetTabSortCols SPRTAB, "KEYS"
    MaxLine = SPRTAB.GetLineCount - 1
    tmpData = SPRTAB.GetFieldValue(0, "KEYS")
    For CurLine = 1 To MaxLine
        tmpKeys = SPRTAB.GetFieldValue(CurLine, "KEYS")
        If tmpKeys = tmpData Then
            SPRTAB.SetLineColor CurLine, vbBlack, vbYellow
        End If
        tmpData = tmpKeys
    Next
    
    SetTabSortCols SPRTAB, "PKNO"
    SetTabIndexes SPRTAB, "PKNO,USTF,KEYS", "PKNO,USTF,KEYS", True
    SPRTAB.AutoSizeColumns
    SPRTAB.Refresh
    keyTab.Refresh
    
End Sub

Private Sub HandleSprtabInsert(CurTab As TABLib.TAB, CurColor As Long)
    Dim LineNo As Long
    Dim LineCount As Long
    Dim HitCount As String
    Dim tmpBool As String
    Dim newFields As String
    Dim newData As String
    Dim newUrno As String
    Dim newTimeStamp As String
    Dim outFields As String
    Dim outData As String
    'SprGridFields = "LINE,STAT,CMDS,PKNO,FCOL,ACTI,USTF,URNO,' ',' ',' '"
    newFields = "PKNO,FCOL,ACTI,USTF,URNO"
    CurTab.SetInternalLineBuffer True
    
    HitCount = CurTab.GetLinesByBackColor(CurColor)
    
    If Val(HitCount) > 0 Then
        Screen.MousePointer = 11
        UfisServer.UrnoPoolInit 100
        UfisServer.UrnoPoolPrepare Val(HitCount)
        newTimeStamp = GetTimeStamp(0)
        outFields = newFields & ",USEC,CDAT,HOPO"
        outData = "*CMD*,SPRTAB,IRT," & CStr(ItemCount(outFields, ",")) & "," & outFields & vbLf
        LineNo = CurTab.GetNextResultLine
        While LineNo >= 0
            tmpBool = CurTab.GetFieldValue(LineNo, "LINE")
            If tmpBool = "Y" Then
                LineCount = LineCount + 1
                If LineCount > 200 Then
                    UfisServer.CallCeda CedaDataAnswer, "REL", "SPRTAB", outFields, outData, "LATE", "", 0, False, False
                    outData = GetItem(outData, 1, vbLf) & vbLf
                    LineCount = 1
                End If
                newUrno = UfisServer.UrnoPoolGetNext
                CurTab.SetFieldValues LineNo, "URNO,STAT,CMDS", newUrno & ",I,-"
                CurTab.SetLineColor LineNo, vbBlack, LightYellow
                newData = CurTab.GetFieldValues(LineNo, newFields)
                newData = CleanNullValues(newData)
                outData = outData & newData & ","
                outData = outData & "RmsImport" & ","
                outData = outData & newTimeStamp & ","
                outData = outData & HomeAirport & vbLf
                
                If LogFile Then WriteToLogFile outData
            Else
            
                
                
            End If
            
            
            LineNo = CurTab.GetNextResultLine
        Wend
        CurTab.SetInternalLineBuffer False
        If LineCount > 0 Then
            UfisServer.CallCeda CedaDataAnswer, "REL", "SPRTAB", outFields, outData, "LATE", "", 0, False, False
        End If
        'UfisServer.CallCeda CedaDataAnswer, "SBC", tmpSqlTab & "/REFR", tmpSqlFld, tmpSqlDat, tmpSqlKey, "", 0, False, False
        CurTab.AutoSizeColumns
        CurTab.Refresh
        Screen.MousePointer = 0
    End If
    CurTab.Refresh
    CurTab.SetCurrentSelection CurTab.GetCurrentSelected
    If Ckb_Interface.Value = Unchecked Then
    CurTab.SetFocus
    End If
    
    Interface = GetIniEntry(myIniFile, "MAIN", "", "INTERFACE", "DISABLED")
    
End Sub

Private Sub HandleSlides()
    Static tmpFilePath As String
    Dim tmpFileFilter As String
    Dim tmpFileResult As String
    SlideShow = True
    tmpFileFilter = "{Slide Files}{*.jpg;*.bmp}{M}|{All Files}{*.*}{M}"
    If tmpFilePath = "" Then tmpFilePath = "c:\ufis\data"
    tmpFileResult = frmFileDialog.GetFileOpenDialog(0, tmpFilePath, tmpFileFilter, 0, 0, "SLIDE.SHOW")
    If tmpFileResult = "OK" Then
        InitSlideShowTimer
    Else
        UfisIntroTimer(2).Enabled = False
        SlideShow = False
    End If
End Sub

Private Sub ReorgSlideShow(SetEnable As Boolean)
    Dim tmpTag As String
    Dim tmpSlideTag As String
    Dim tmpFileName As String
    Dim tmpPathName As String
    Dim i As Integer
    On Error Resume Next
    If SetEnable = True Then
        tmpTag = UfisIntroFrame(2).Tag
        If tmpTag <> "" Then
            SlideShow = Val(GetItem(tmpTag, 1, ","))
            If SlideShow = True Then
                For i = 0 To 2
                    UfisIntroFrame(i).Visible = True
                    tmpSlideTag = GetItem(tmpTag, i + 8, ",")
                    tmpFileName = GetItem(tmpSlideTag, 2, ";")
                    tmpPathName = GetItem(tmpSlideTag, 3, ";")
                    UfisIntroPicture(i).Picture = LoadPicture(tmpPathName & "\" & tmpFileName)
                Next
            End If
            chkTool(8).Value = Val(GetItem(tmpTag, 2, ","))
            chkTool(12).Value = Val(GetItem(tmpTag, 3, ","))
            chkTool(9).Value = Val(GetItem(tmpTag, 4, ","))
            chkTool(6).Value = Val(GetItem(tmpTag, 5, ","))
            chkTool(7).Value = Val(GetItem(tmpTag, 6, ","))
            chkTool(11).Value = Val(GetItem(tmpTag, 7, ","))
            If SlideShow = True Then
                CheckUfisIntroPicture 1, True, 0, "", ""
                fraWorkArea(0).Refresh
                If SlideHoldAll = False Then UfisIntroTimer(2).Enabled = True
            End If
        End If
    Else
        tmpTag = CStr(CInt(SlideShow)) & ","
        tmpTag = tmpTag & CStr(chkTool(8).Value) & ","
        tmpTag = tmpTag & CStr(chkTool(12).Value) & ","
        tmpTag = tmpTag & CStr(chkTool(9).Value) & ","
        tmpTag = tmpTag & CStr(chkTool(6).Value) & ","
        tmpTag = tmpTag & CStr(chkTool(7).Value) & ","
        tmpTag = tmpTag & CStr(chkTool(11).Value) & ","
        For i = 0 To 2
            tmpSlideTag = UfisIntroPicture(i).Tag
            tmpSlideTag = Replace(tmpSlideTag, ",", ";", 1, -1, vbBinaryCompare)
            tmpTag = tmpTag & tmpSlideTag & ","
        Next
        UfisIntroFrame(2).Tag = tmpTag
        SlideShow = False
        UfisIntroTimer(2).Enabled = False
        chkTool(8).Value = 0
        chkTool(12).Value = 0
        chkTool(9).Value = 0
        chkTool(6).Value = 0
        chkTool(7).Value = 0
        chkTool(11).Value = 0
        UfisIntroFrame(0).Visible = False
        UfisIntroFrame(1).Visible = False
        StatusBar1.Panels(1).Text = ""
        StatusBar1.Panels(2).Text = ""
        UfisIntroTimer(0).Enabled = True
    End If
   End Sub
   
   Public Sub EnableInterfaceTimer(SetEnable As Boolean, Clock_Interval_in As String, Keycards_Interval_in As String, Absences_Interval_in As String)
   
   'set the Intervals
   Clock_Interval = Clock_Interval_in
   Keycards_Interval = Keycards_Interval_in
   Absences_Interval = Absences_Interval_in
   
   InterfaceEnabled = SetEnable
  
   
   
   Ckb_Interface.Value = Unchecked
   
   
   
   If SetEnable Then
   ' First time
   ' It will be ebaled from the Ckb_Interface_Click
   Interface_TIMER.Enabled = False
   
   Ckb_Interface.Value = Checked
   
   
   'Logging
   LogFile = True
   If LogFile Then WriteToLogFile "Started"
   End If
   
  
     
   End Sub
   Private Sub Interface_TIMER_Timer()
' This is called every Minute so
   minCount = minCount + 1
   If minCount = Clock_Interval Then
   If LogFile Then WriteToLogFile "Interval Started"
   InterfaceError = False
   InterfaceClockHandle
   If LogFile Then WriteToLogFile "Interval Finished"
   End If
   End Sub
   
   Private Sub InterfaceClockHandle()
   'Dim curWorkPanel As Integer
   curWorkPanel = 3
       
    If LogFile Then WriteToLogFile "After Clock Import InterfaceError=" & InterfaceError
    If (Ckb_Interface.Value = Checked) Then
       
    HandleClockImport TAB1(curWorkPanel), TAB3(curWorkPanel), TAB2(curWorkPanel)
    If LogFile Then WriteToLogFile "After Clock Import InterfaceError=" & InterfaceError
    If InterfaceError = False Then
    
    'SetWorkflowPointer chkTask(36), "Step 2:", "2"
    'ToggleBottomPanel curWorkPanel, True, True, False, True
    'AdjustBottomLabels curWorkPanel, TAB1(curWorkPanel), TAB2(curWorkPanel), False
    'chkTask(Index).Value = 0
    
    
    HandleClockCheck TAB1(curWorkPanel), TAB3(curWorkPanel), TAB2(curWorkPanel)
    If LogFile Then WriteToLogFile "After Clock check "
    'SetWorkflowPointer chkTask(44), "Step 3:", "3"
    'AdjustBottomLabels curWorkPanel, TAB1(curWorkPanel), TAB2(curWorkPanel), False
    'chkTask(Index).Value = 0
    
    ClearClockingData TAB1(curWorkPanel), TAB2(curWorkPanel)
    If LogFile Then WriteToLogFile "After Clock Data "
    'SetWorkflowPointer chkTask(21), "Step 4:", "4"
    'AdjustBottomLabels curWorkPanel, TAB1(curWorkPanel), TAB2(curWorkPanel), False
    'chkTask(Index).Value = 0
    
    'CheckValidationRules False
    CompareClockingData TAB1(curWorkPanel), TAB3(curWorkPanel), TAB2(curWorkPanel)
    imgPointer(curWorkPanel).Visible = True
    imgPointer(curWorkPanel).Top = 750
    imgPointer(curWorkPanel).ToolTipText = "Step 5: Import"
    imgPointer(curWorkPanel).Tag = "5"
    chkImport(curWorkPanel).Value = 1
    AdjustBottomLabels curWorkPanel, TAB1(curWorkPanel), TAB2(curWorkPanel), False
    'CheckValidationRules True
    'chkTask(Index).Value = 0
    
    Dim Index As Integer
    'Insert
    Index = 2
    ' Update
    'Index = 1
    
    HandleSprtabInsert TAB2(curWorkPanel), SrvFkeyCnt(Index).BackColor
    If LogFile Then WriteToLogFile "After Insert  Data "
    End If
    
    End If
    
    'Reset the timer
    minCount = 0
    
   End Sub
   
   Public Function FileExists(fName As String) As Boolean
   If fName = "" Or Right(fName, 1) = "\" Then
   FileExists = False: Exit Function
   End If
   
   FileExists = (Dir(fName) <> "")
   
   End Function


   
   
