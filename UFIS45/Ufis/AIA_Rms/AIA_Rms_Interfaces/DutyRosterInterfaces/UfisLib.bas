Attribute VB_Name = "UfisLib"
Option Explicit

Public MyMainForm As Form
Public MyTcpIpAdr As String

Public UfisSystemPath As String
Public UfisApplPath As String
Public UfisTmpPath As String
Public UfisHelpPath As String

'=======================================
'Defines for internal ReturnCodes
'=======================================
Public Const RC_NOT_FOUND = -2       'There was no Result
Public Const RC_SUCCESS = True       'All OK
Public Const RC_FAIL = False         'Not OK

'=======================================
'Defines for CC = CallCeda() ReturnCodes
'=======================================
'CEDA Communication
Public Const CC_RC_ORA_ERROR = -12      'DB Transaction failed
Public Const CC_RC_PROC_ERROR = -11     'Ceda Process has Problems
Public Const CC_RC_COM_ERROR = -10      'Something wrong with the Communication
Public Const CC_RC_ACCESS_DENIED = -9   'Transaction or Login rejected
Public Const CC_RC_WRITE_DENIED = -8    'Transaction not permitted
Public Const CC_RC_LOST_CONNECTION = -7 'The Server died
Public Const CC_RC_PROC_CRASH = -6      'The Process died
Public Const CC_RC_NOT_CONNECTED = -5   'Could not connect to the Server
Public Const CC_RC_TIMEOUT = -4         'Got no answer from the Server
Public Const CC_RC_UNEXPECTED = -3      'Got a unexpected answer from the server
Public Const CC_RC_NOT_FOUND = -2       'There was no Result
Public Const CC_RC_QUE_WAIT = -1        'The transaction is put on the Queue
'Internal Errors
Public Const CC_RC_WRONG_BUFFER = -100

'=======================================
'Defines for general purposes
'=======================================
'CleanString()
Public Const INIT_FOR_CLIENT = -1
Public Const INIT_FOR_SERVER = -2
Public Const FOR_CLIENT = 1
Public Const FOR_SERVER = 2
Public Const SERVER_TO_CLIENT = 3
Public Const CLIENT_TO_SERVER = 4

Public Const FOR_INSERT = 1
Public Const FOR_UPDATE = 2
Public Const FOR_DELETE = 3
Public Const FOR_SELECT = 4

Public Const FOR_ALL = 0
Public Const FOR_SEND = -1
Public Const FOR_RECV = -2

'=======================================
'Defines for Windows NT Features
'=======================================
Public Const VER_PLATFORM_WIN32s = 0
Public Const VER_PLATFORM_WIN32_WINDOWS = 1
Public Const VER_PLATFORM_WIN32_NT = 2

Public Const WF_CPU286 = &H2&
Public Const WF_CPU386 = &H4&
Public Const WF_CPU486 = &H8&
Public Const WF_STANDARD = &H10&
Public Const WF_ENHANCED = &H20&
Public Const WF_80x87 = &H400&

Public Const SM_MOUSEPRESENT = 19

Public Const GFSR_SYSTEMRESOURCES = &H0
Public Const GFSR_GDIRESOURCES = &H1
Public Const GFSR_USERRESOURCES = &H2

Public Const MF_POPUP = &H10
Public Const MF_BYPOSITION = &H400
Public Const MF_SEPARATOR = &H800

Public Const SRCCOPY = &HCC0020
Public Const SRCERASE = &H440328
Public Const SRCINVERT = &H660046
Public Const SRCAND = &H8800C6

Public Const HWND_TOPMOST = -1
Public Const HWND_NOTOPMOST = -2
Public Const SWP_NOACTIVATE = &H10
Public Const SWP_SHOWWINDOW = &H40
 
Public Const TWIPS = 1
Public Const PIXELS = 3
Public Const RES_INFO = 2
Public Const MINIMIZED = 1

Const MAX_COMPUTERNAME_LENGTH = 15

'=======================================
'Windows NT Structures
'=======================================

Type MYVERSION
    lMajorVersion As Long
    lMinorVersion As Long
    lExtraInfo As Long
End Type

Type OSVERSIONINFO
        dwOSVersionInfoSize As Long
        dwMajorVersion As Long
        dwMinorVersion As Long
        dwBuildNumber As Long
        dwPlatformId As Long
        szCSDVersion As String * 128      '  Maintenance string for PSS usage
End Type

Type Rect
    Left As Integer
    Top As Integer
    Right As Integer
    Bottom As Integer
End Type

Public Type SystemInfo
    dwOemId As Long
    dwPageSize As Long
    lpMinimumApplicationAddress As Long
    lpMaximumApplicationAddress As Long
    dwActiveProcessorMask As Long
    dwNumberOfProcessors As Long
    dwProcessorType As Long
    dwAllocationGranularity As Long
    dwReserved As Long
End Type

Public Type MEMORYSTATUS
    dwLength As Long
    dwMemoryLoad As Long
    dwTotalPhys As Long
    dwAvailPhys As Long
    dwTotalPageFile As Long
    dwAvailPageFile As Long
    dwTotalVirtual As Long
    dwAvailVirtual As Long
End Type

' This code was adapted from original system tray module published by Ben Baird.
' Tray Icon add/remove functions implemented within this module.
' Created by E.Spencer (elliot@spnc.demon.co.uk) - This code is public domain.
' Added call back to handle mouse events correctly

Public Declare Function SetForegroundWindow Lib "user32" (ByVal hwnd As Long) As Long
Declare Function Shell_NotifyIcon Lib "shell32.dll" Alias "Shell_NotifyIconA" (ByVal dwMessage As Long, lpData As NOTIFYICONDATA) As Long
Declare Function CallWindowProc Lib "user32" Alias "CallWindowProcA" (ByVal lpPrevWndFunc As Long, _
   ByVal hwnd As Long, ByVal Msg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" (ByVal hwnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long
Public Const WM_RBUTTONDOWN = &H204
Public Const WM_RBUTTONUP = &H205
Public Const WM_ACTIVATEAPP = &H1C
Public Const NIF_ICON = &H2
Public Const NIF_MESSAGE = &H1
Public Const NIF_TIP = &H4
Public Const NIM_ADD = &H0
Public Const NIM_DELETE = &H2
Public Const MAX_TOOLTIP As Integer = 64
Public Const GWL_WNDPROC = (-4)

Type NOTIFYICONDATA
   cbSize As Long
   hwnd As Long
   uID As Long
   uFlags As Long
   uCallbackMessage As Long
   hIcon As Long
   szTip As String * MAX_TOOLTIP
End Type
Public nfIconData As NOTIFYICONDATA
Private FHandle As Long     ' Storage for form handle
Private WndProc As Long     ' Address of our handler
Private Hooking As Boolean  ' Hooking indicator

'=======================================
'Application Defines
'=======================================
Public Const DEFAULT_CEDA_INI = "C:\UFIS\SYSTEM\CEDA.INI"
Public myIniPath As String
Public myIniFile As String
Public myIniFullName As String
Public myIniSection As String
'=======================================
'Keyboard Status Flags
'=======================================
Public KeybdFlags As Integer
Public KeybdState As String
Public KeybdIsCtrl As Boolean
Public KeybdIsShift As Boolean
Public KeybdIsAlt As Boolean

'=======================================
'Application Structures
'=======================================
Public Type MaxFrame        'Using Twips
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

'=======================================
'Coordination of Pending Events
'=======================================
Public Const RC_NONE = 0
Public Const RC_PENDING = 1
Public Const RC_IGNORED = 2
Public Const RC_NEWFOCUS = 3
Public Const RELEASE = 0

Public Type PendEvent
    IsActive As Boolean
    Type As Integer
    Form As Form
    Object As Control
End Type
Public PendingEvent As PendEvent


'=======================================
'Windows NT Function Prototypes
'=======================================
Public Const HELP_CONTEXT = &H1
Public Const HELP_QUIT = &H2
Public Const HELP_INDEX = &H3
Public Const HELP_HELPONHELP = &H4
Public Const HELP_SETINDEX = &H5
Public Const HELP_KEY = &H101
Public Const HELP_MULTIKEY = &H201
'Public Const HELP_FILE = "GanttHelp.hlp"

Declare Function WinHelp Lib "user32" Alias "WinHelpA" (ByVal hwnd As Long, ByVal lpHelpFile As String, ByVal wCommand As Long, ByVal dwData As Long) As Long
Declare Function GetWindowsDirectory Lib "kernel32" Alias "GetWindowsDirectoryA" (ByVal lpBuffer As String, ByVal nSize As Long) As Long
Declare Sub GetSystemInfo Lib "kernel32" (lpSystemInfo As SystemInfo)
Declare Sub GlobalMemoryStatus Lib "kernel32" (lpBuffer As MEMORYSTATUS)
Declare Function GetVersionEx Lib "kernel32" Alias "GetVersionExA" (ByRef lpVersionInformation As OSVERSIONINFO) As Long
Declare Function GetSystemMetrics Lib "user32" (ByVal nIndex As Long) As Long
Declare Function GetDeviceCaps Lib "gdi32" (ByVal hDC As Long, ByVal nIndex As Long) As Long
Declare Function TrackPopupMenu Lib "user32" (ByVal hMenu As Long, ByVal wFlags As Long, ByVal X As Long, ByVal Y As Long, ByVal nReserved As Long, ByVal hwnd As Long, lpReserved As Any) As Long
Declare Function GetMenu Lib "user32" (ByVal hwnd As Long) As Long
Declare Function GetSubMenu Lib "user32" (ByVal hMenu As Long, ByVal nPos As Long) As Long
Declare Function GetDesktopWindow Lib "user32" () As Long
Declare Function GetDC Lib "user32" (ByVal hwnd As Long) As Long
Declare Function ReleaseDC Lib "user32" (ByVal hwnd As Long, ByVal hDC As Long) As Long
Declare Function BitBlt Lib "gdi32" (ByVal hDestDC As Long, ByVal X As Long, ByVal Y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal hSrcDC As Long, ByVal XSrc As Long, ByVal YSrc As Long, ByVal dwRop As Long) As Long
Declare Sub SetWindowPos Lib "user32" (ByVal hwnd As Long, ByVal hWndInsertAfter As Long, ByVal X As Long, ByVal Y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long)
'Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationname As String, lpKeyName As Any, ByVal lpDefault As String, ByVal lpRetunedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Declare Function GetProfileString Lib "kernel32" Alias "GetProfileStringA" (ByVal lpAppName As String, lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long) As Long
Declare Function waveOutGetNumDevs Lib "winmm" () As Long
Declare Function GetSystemDirectory Lib "kernel32" Alias "GetSystemDirectoryA" (ByVal lpBuffer As String, ByVal nSize As Long) As Long
Declare Function sndPlaySound Lib "winmm" Alias "sndPlaySoundA" (ByVal lpszSoundName As String, ByVal uFlags As Long) As Long
Declare Function FlashWindow Lib "user32" (ByVal hwnd As Long, ByVal bInvert As Long) As Long
Public Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationname As String, ByVal lpKeyName As Any, ByVal lsString As Any, ByVal lplFilename As String) As Long
Public Declare Function GetPrivateProfileInt Lib "kernel32" Alias "GetPriviteProfileIntA" (ByVal lpApplicationname As String, ByVal lpKeyName As String, ByVal nDefault As Long, ByVal lpFileName As String) As Long
Public Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationname As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Private Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" (ByVal lpBuffer As String, nSize As Long) As Long
Private Declare Function GetComputerName Lib "kernel32" Alias "GetComputerNameA" (ByVal lpBuffer As String, nSize As Long) As Long
Private Declare Function HtmlHelpTopic Lib "hhctrl.ocx" Alias _
    "HtmlHelpA" (ByVal hwnd As Long, ByVal lpHelpFile As String, _
    ByVal wCommand As Long, ByVal dwData As Long) As Long

Public VisibleFrame As Frame
Public CedaIniFile As String
Public HomeAirport As String
Public UtcTimeDiff As Integer

Public ApplicationIsStarted As Boolean
Public ApplicationIsBusy As Boolean
Public CedaIsConnected As Boolean
Public ServerIsAvailable As Boolean
Public ModalMsgIsOpen As Boolean
Public ShutDownRequested As Boolean
Public AutoArrange As Boolean
Public ErrorIgnored As Boolean
Public UserAnswer As String
Public CedaDataAnswer As String
Public CurMem As Integer
Public SuspendErrMsg As Boolean
Public StopNestedCalls As Boolean
Public InitialMaskDisplay As Boolean
Public HtmHelpIsOpen As Boolean
Public HtmHelpHwnd As Long

Dim WinVersion As Integer
Dim SoundAvailable As Integer

Public Const MonthList = "JAN,FEB,MAR,APR,MAY,JUN,JUL,AUG,SEP,OCT,NOV,DEC"
'=======================================
'Special Color Values
'=======================================
'-------------------------------------
'YELLOW
'-------------------------------------
Public Const LightestYellow = &HC0FFFF
Public Const LightYellow = &H80FFFF
Public Const NormalYellow = &HFFFF&
Public Const DarkYellow = &HC0C0&
Public Const DarkestYellow = &H8080&
'-------------------------------------
'GREEN
'-------------------------------------
Public Const NearlyGreen = &HC0FFC0
Public Const LightestGreen = &HC0FFC0
Public Const LightGreen = &H80FF80
Public Const NormalGreen = &HC000&
Public Const DarkGreen = &H8000&
Public Const DarkestGreen = &H4000&
'-------------------------------------
'RED
'-------------------------------------
Public Const LightestRed = &HC0C0FF
Public Const LightRed = &H8080FF

Public Const LightOcre = &HC0E0FF
'-------------------------------------
'GRAY (GREY)
'-------------------------------------
Public Const LightGray = &HE0E0E0
Public Const NormalGray = &HC0C0C0
Public Const DarkGray = &H808080
Public Const LightGrey = &HE0E0E0
Public Const NormalGrey = &HC0C0C0
Public Const DarkGrey = &H808080
'-------------------------------------
'BLUE
'-------------------------------------
Public Const LightestBlue = &HFFC0C0
Public Const LightBlue = &HFF8080
Public Const NormalBlue = &HFF0000
Public Const DarkBlue = &HC00000
Public Const DarkestBlue = &H800000

Public Function GetIniEntry(cpFileName As String, cpSection1 As String, cpSection2 As String, cpKeyWord As String, cpDefault As String) As String
Dim IniFileName As String
Dim TextLine As String
Dim clHeader As String
Dim clResult As String
Dim blLoopEnd As Boolean
Dim blSectionFound As Boolean
Dim blBlockFound As Boolean
Dim ilPos As Integer
Dim ilCount As Integer
    On Error GoTo HandleError
    IniFileName = cpFileName
    If IniFileName = "" Then IniFileName = DEFAULT_CEDA_INI
    clResult = ""
    blLoopEnd = False
    blBlockFound = False
    ilCount = 0
    Do While (Not blLoopEnd) And (ilCount < 2)
        ilCount = ilCount + 1
        If ilCount = 1 Then
            clHeader = "[" & cpSection1 & "]"
        Else
            clHeader = "[" & cpSection2 & "]"
        End If
        If clHeader <> "[]" Then
            blSectionFound = False
            Open IniFileName For Input As #1
            Do While (Not EOF(1)) And (Not blLoopEnd)
                Line Input #1, TextLine
                TextLine = LTrim(TextLine)
                If InStr(TextLine, "[") = 1 Then 'New Section
                    If Not blSectionFound Then
                        If InStr(TextLine, clHeader) = 1 Then
                            blSectionFound = True 'Given Section found
                        End If
                    Else
                        blLoopEnd = True
                    End If
                Else
                    If blSectionFound Then
                        If blBlockFound = True Then
                            If InStr(TextLine, "BLOCK_END") = 1 Then
                                blLoopEnd = True
                            Else
                                clResult = clResult & TextLine & vbNewLine
                            End If
                        Else
                            If InStr(TextLine, cpKeyWord) = 1 Then
                                'KeyWord found
                                ilPos = InStr(TextLine, "=")
                                If ilPos > 0 Then
                                    clResult = Trim(Mid$(TextLine, ilPos + 1))
                                    If clResult = "BLOCK_BEGIN" Then
                                        blBlockFound = True
                                        clResult = ""
                                    Else
                                        blLoopEnd = True
                                    End If
                                Else
                                    'Nothing, we search until the end of section
                                End If
                            End If
                        End If
                    End If
                End If
            Loop
            Close #1
        End If
        If clResult = "" Then
            blLoopEnd = False
        End If
    Loop
    If clResult = "" Then
        clResult = cpDefault
    End If
    If cpKeyWord = "?EXIST?" Then
        If blSectionFound Then
            clResult = "YES"
        Else
            clResult = "NO"
        End If
    End If
    GetIniEntry = clResult
    Exit Function
HandleError:
    Select Case Err.Number
        Case 53
            If SuspendErrMsg = False Then
                MsgBox "File not found:" & vbNewLine & IniFileName & vbNewLine & "For: " & cpSection1 & "/" & cpKeyWord
            End If
            GetIniEntry = cpDefault
        Case Else
            Resume Next
    End Select
End Function

Public Function GetItem(cpTxt As String, ipNbr As Integer, cpSep As String) As String
 Dim Result
 Dim ilSepLen As Integer
 Dim ilFirstPos As Integer
 Dim ilLastPos As Integer
 Dim ilItmLen As Integer
 Dim ilItmNbr As Integer
    Result = ""
    If ipNbr > 0 Then
        ilSepLen = Len(cpSep)
        ilFirstPos = 1
        ilLastPos = 1
        ilItmNbr = 0
        While (ilItmNbr < ipNbr) And (ilLastPos > 0)
            ilItmNbr = ilItmNbr + 1
            ilLastPos = InStr(ilFirstPos, cpTxt, cpSep)
            If (ilItmNbr < ipNbr) And (ilLastPos > 0) Then ilFirstPos = ilLastPos + ilSepLen
        Wend
        If ilLastPos < ilFirstPos Then ilLastPos = Len(cpTxt) + 1
        If (ilItmNbr = ipNbr) And (ilLastPos > ilFirstPos) Then
            ilItmLen = ilLastPos - ilFirstPos
            Result = Mid(cpTxt, ilFirstPos, ilItmLen)
        End If
    End If
    GetItem = RTrim(Result)
End Function
'------------------------------------------------------------------------------------
' Same as GetItem but zero based item no and using long values
'------------------------------------------------------------------------------------
Function GetRealItem(cpTxt As String, lpNbr As Long, cpSep As String) As String
    Dim Result
    Dim llSepLen As Long
    Dim llFirstPos As Long
    Dim llLastPos As Long
    Dim llItmLen As Long
    Dim llItmNbr As Long
    
    Result = ""
    If lpNbr >= 0 Then
        llSepLen = Len(cpSep)
        llFirstPos = 1
        llLastPos = 1
        llItmNbr = -1
        While (llItmNbr < lpNbr) And (llLastPos > 0)
            llItmNbr = llItmNbr + 1
            llLastPos = InStr(llFirstPos, cpTxt, cpSep)
            If (llItmNbr < lpNbr) And (llLastPos > 0) Then llFirstPos = llLastPos + llSepLen
        Wend
        If llLastPos < llFirstPos Then llLastPos = Len(cpTxt) + 1
        If (llItmNbr = lpNbr) And (llLastPos > llFirstPos) Then
            llItmLen = llLastPos - llFirstPos
            Result = Mid(cpTxt, llFirstPos, llItmLen)
        End If
    End If
    GetRealItem = RTrim(Result)
End Function
'-------------------------------------------------------------------------
' Returns the item number but also for an itemlist with variable field len
' For the first item "0" will be returned
'-------------------------------------------------------------------------
Function GetRealItemNo(ItemList As String, ItemValue As String) As Integer
    Dim cnt As Integer
    Dim i As Integer
    Dim blFound As Boolean
    Dim strItemList As String
    Dim strVal As String
    Dim pos As Integer
    
    strItemList = "," + ItemList + ","
    strVal = "," + ItemValue + ","
    
    pos = InStr(strItemList, strVal)
    If pos > 0 Then
        If pos = 1 Then
            GetRealItemNo = 0
        Else
            GetRealItemNo = ItemCount(Left(strItemList, pos), ",") - 2
        End If
    Else
        GetRealItemNo = -1
    End If
End Function

Public Function GetItemNo(ItemList As String, ItemValue As String) As Integer
    Dim ItmLen As Integer
    Dim ItmPos As Integer
    ItmLen = Len(ItemValue) + 1
    ItmPos = InStr(ItemList, ItemValue)
    If ItmPos > 0 Then
        GetItemNo = ((ItmPos - 1) \ ItmLen) + 1
    Else
        GetItemNo = -1
    End If
End Function

Public Function GetFieldValue(FieldName As String, DataList As String, FieldList As String) As String
    Dim ItmNbr As Integer
    ItmNbr = GetItemNo(FieldList, FieldName)
    GetFieldValue = GetItem(DataList, ItmNbr, ",")
End Function
Public Function CedaDateAdd(cpDate As String, ipDays As Integer) As String
    Dim tmpDate
        tmpDate = CedaDateToVb(cpDate)
        tmpDate = DateAdd("d", ipDays, tmpDate)
        CedaDateAdd = Format(tmpDate, "yyyymmdd")
End Function
Public Function CedaDateDiff(CurDate1 As String, CurDate2 As String) As Long
    Dim tmpDate1
    Dim tmpDate2
    If (CurDate1 <> "") And (CurDate2 <> "") Then
        tmpDate1 = CedaDateToVb(CurDate1)
        tmpDate2 = CedaDateToVb(CurDate2)
        CedaDateDiff = DateDiff("d", tmpDate1, tmpDate2)
    Else
        CedaDateDiff = 0
    End If
End Function
Public Function CedaDateToVb(cpDate As String) 'as Variant
    If Trim(cpDate) <> "" Then
        CedaDateToVb = DateSerial(Val(Mid(cpDate, 1, 4)), Val(Mid(cpDate, 5, 2)), Val(Mid(cpDate, 7, 2)))
    Else
        CedaDateToVb = ""
    End If
End Function

Public Function CedaTimeToVb(cpDate As String) 'as Variant
Dim ilHH As Integer
Dim ilMM As Integer
Dim ilSS As Integer
    If Trim(cpDate) <> "" Then
        If Len(cpDate) > 8 Then
            CedaTimeToVb = TimeSerial(Val(Mid(cpDate, 9, 2)), Val(Mid(cpDate, 11, 2)), Val(Mid(cpDate, 13, 2)))
        Else
            If InStr(cpDate, ":") > 0 Then
                ilHH = Val(GetItem(cpDate, 1, ":"))
                ilMM = Val(GetItem(cpDate, 2, ":"))
                ilSS = Val(GetItem(cpDate, 3, ":"))
                CedaTimeToVb = TimeSerial(ilHH, ilMM, ilSS)
            Else
                CedaTimeToVb = TimeSerial(Val(Mid(cpDate, 1, 2)), Val(Mid(cpDate, 3, 2)), Val(Mid(cpDate, 5, 2)))
            End If
        End If
    Else
        CedaTimeToVb = ""
    End If
End Function

Public Function CedaFullDateToVb(cpDate As String) 'as Variant
Dim MyDate
Dim myTime
    MyDate = CedaDateToVb(cpDate)
    myTime = CedaTimeToVb(cpDate)
    If (MyDate <> "") And (myTime <> "") Then
        CedaFullDateToVb = MyDate + myTime
    Else
        CedaFullDateToVb = ""
    End If
End Function

Public Function VbDateStrgToCeda(cpDate As String, cpInForm As String) As String
Dim clResult As String
Dim UsedFormat As String
Dim clYear As String
Dim clMon As String
Dim clDay As String
Dim clHour As String
Dim clMin As String
Dim clSec As String
Dim ilInLen As Integer
Dim i As Integer
Dim clCod As String
Dim clChr As String
    clResult = ""
    clYear = ""
    clMon = ""
    clDay = ""
    clHour = ""
    clMin = ""
    clSec = ""
    ilInLen = Len(cpInForm)
    UsedFormat = cpInForm
    For i = 1 To ilInLen
        clCod = Mid(UsedFormat, i, 1)
        clChr = Mid(cpDate, i, 1)
        Select Case clCod
            Case "y"
                clYear = clYear & clChr
            Case "m"
                clMon = clMon & clChr
            Case "d"
                clDay = clDay & clChr
            Case Else
        End Select
    Next
    clResult = clYear & clMon & clDay
    VbDateStrgToCeda = clResult
End Function

Public Function DateInputFormatToCeda(cpDate As String, cpInForm As String) As String
Dim myCedaDate As String
Dim myChkDate As String
    myCedaDate = VbDateStrgToCeda(cpDate, cpInForm)
    myChkDate = Format(CedaDateToVb(myCedaDate), cpInForm)
    If myChkDate <> cpDate Then
        myCedaDate = ""
    End If
    DateInputFormatToCeda = myCedaDate
End Function

Function DeviceColors(hDC As Long) As Single
Const PLANES = 14
Const BITSPIXEL = 12
    DeviceColors = 2 ^ (GetDeviceCaps(hDC, PLANES) * GetDeviceCaps(hDC, BITSPIXEL))
End Function

Function GetSysIni(section, Key)
Dim RetVal As String, appName As String, worked As Integer
    RetVal = String$(255, 0)
    worked = GetPrivateProfileString(section, Key, "", RetVal, Len(RetVal), "System.ini")
    If worked = 0 Then
        GetSysIni = "unknown"
    Else
        GetSysIni = Left(RetVal, InStr(RetVal, Chr(0)) - 1)
    End If
End Function

Function GetWinIni(section, Key)
Dim RetVal As String, appName As String, worked As Integer
    RetVal = String$(255, 0)
    worked = GetProfileString(section, Key, "", RetVal, Len(RetVal))
    If worked = 0 Then
        GetWinIni = "unknown"
    Else
        GetWinIni = Left(RetVal, InStr(RetVal, Chr(0)) - 1)
    End If
End Function

Function SystemDirectory() As String
Dim WinPath As String
    WinPath = String(145, Chr(0))
    SystemDirectory = Left(WinPath, GetSystemDirectory(WinPath, 145))
End Function

Function WindowsDirectory() As String
Dim WinPath As String
    WinPath = String(145, Chr(0))
    WindowsDirectory = Left(WinPath, GetWindowsDirectory(WinPath, 145))
End Function

Function WindowsVersion() As MYVERSION
Dim myOS As OSVERSIONINFO, WinVer As MYVERSION
Dim lResult As Long

    myOS.dwOSVersionInfoSize = Len(myOS)    'should be 148
    
    lResult = GetVersionEx(myOS)
        
    'Fill user type with pertinent info
    WinVer.lMajorVersion = myOS.dwMajorVersion
    WinVer.lMinorVersion = myOS.dwMinorVersion
    WinVer.lExtraInfo = myOS.dwPlatformId
    
    WindowsVersion = WinVer

End Function

Public Sub SetFormOnTop(UsedForm As Object, OnTop As Boolean)
    If OnTop = True Then
       SetWindowPos UsedForm.hwnd, HWND_TOPMOST, UsedForm.Left / 15, _
                    UsedForm.Top / 15, UsedForm.Width / 15, _
                    UsedForm.height / 15, SWP_NOACTIVATE Or SWP_SHOWWINDOW
    Else
       SetWindowPos UsedForm.hwnd, HWND_NOTOPMOST, UsedForm.Left / 15, _
                    UsedForm.Top / 15, UsedForm.Width / 15, _
                    UsedForm.height / 15, SWP_NOACTIVATE Or SWP_SHOWWINDOW
    End If
End Sub

Public Function CountVisibleForms() As Integer
Dim i As Integer
Dim cnt As Integer
    cnt = 0
    For i = 0 To Forms.count - 1
        If Forms(i).Visible = True Then cnt = cnt + 1
    Next i
    CountVisibleForms = cnt
End Function
Public Function FormIsVisible(FormName As String) As Boolean
Dim i As Integer
Dim Answ As Boolean
    Answ = False
    For i = 0 To Forms.count - 1
        If (Forms(i).Name = FormName) And (Forms(i).Visible = True) Then
            Answ = True
            Exit For
        End If
    Next i
    FormIsVisible = Answ
End Function
Public Function FormIsLoaded(FormName As String) As Boolean
Dim i As Integer
Dim Answ As Boolean
    Answ = False
    For i = 0 To Forms.count - 1
        If Forms(i).Name = FormName Then
            Answ = True
            Exit For
        End If
    Next i
    FormIsLoaded = Answ
End Function



Public Function CheckDateField(UseObject As Control, UseFormat As String, SetError As Boolean)
    With UseObject
        .Tag = DateInputFormatToCeda(.Text, UseFormat)
        If .Tag = "" Then
            .BackColor = vbRed
            .ForeColor = vbWhite
            CheckDateField = False
            If SetError Then
                PendingEvent.IsActive = True
                Set PendingEvent.Form = Screen.ActiveForm
                Set PendingEvent.Object = UseObject
                PendingEvent.Type = 1
            End If
        Else
            .BackColor = vbWindowBackground
            .ForeColor = vbWindowText
            CheckDateField = True
            If SetError Then PendingEvent.IsActive = False
        End If
    End With
End Function

Public Sub DoNothing()
    'did nothing
End Sub

Public Function GetUrnoFromSqlKey(KeyText As String) As String
Dim Result As String
Dim tmpTxt As String
Dim tmpOffs As Long
    tmpTxt = Trim(KeyText)
    tmpOffs = InStr(tmpTxt, "URNO")
    If tmpOffs > 0 Then
        tmpOffs = tmpOffs + 4
        tmpTxt = LTrim(Mid(tmpTxt, tmpOffs))
        If Left(tmpTxt, 1) = "=" Then
            tmpTxt = LTrim(Mid(tmpTxt, 2))
        Else
            'may be like, <>, o.s.o.
        End If
        If Left(tmpTxt, 1) = "'" Then
            tmpTxt = GetItem(tmpTxt, 2, "'")
        End If
    Else
        'normally the URNO itself
    End If
    Result = GetItem(tmpTxt, 1, " ")
    GetUrnoFromSqlKey = Result
End Function
Public Function CleanNullValues(myString As String)
Dim Result As String
Dim tmpStrg As String
    Result = myString
    If Result = "" Then Result = " "
    Do
        tmpStrg = Result
        Result = Replace(tmpStrg, ",,", ", ,", 1, -1, vbBinaryCompare)
    Loop While Result <> tmpStrg
    If Left(Result, 1) = "," Then Result = " " & Result
    If Right(Result, 1) = "," Then Result = Result & " "
    CleanNullValues = Result
End Function
Public Function CleanString(myText As String, myMethod As Integer, CheckOldVersion As Boolean) As String
Dim Result As String
Static FunctionIsInitialized As Boolean
Static ClientPatchList As String
Static NewServerPatchList As String
Static OldServerPatchList As String
'Until SEP'99 we used ServerPatchValues up from ASCII(176),
'but because these values matched chinese (and greek) characters
'we now use values below ASCII(31).
'But because not all applications (FDIHDL) or installations are changed
'and using the new version of patch values, we here check both.
'This can be forced or suppressed by setting the CheckOldVersion parameter
    If Not FunctionIsInitialized Then
        ClientPatchList = BuildBinaryString("34,39,44,10,13,40,41")
        NewServerPatchList = BuildBinaryString("23,24,25,28,29,40,41")
        OldServerPatchList = BuildBinaryString("176,177,178,179,180,155,156")
        FunctionIsInitialized = True
    End If
    Select Case myMethod
        Case INIT_FOR_CLIENT
            ClientPatchList = BuildBinaryString(myText)
        Case INIT_FOR_SERVER
            If CheckOldVersion Then
                OldServerPatchList = BuildBinaryString(myText)
            Else
                NewServerPatchList = BuildBinaryString(myText)
            End If
        Case FOR_CLIENT, SERVER_TO_CLIENT
            Result = ReplaceChars(myText, NewServerPatchList, ClientPatchList)
            If (Result = myText) And CheckOldVersion Then
                'Nothing changed, so we try the old version
                Result = ReplaceChars(myText, OldServerPatchList, ClientPatchList)
            End If
            'VB needs CR/LF
            If myMethod = FOR_CLIENT Then Result = Replace(Result, vbLf, vbNewLine, 1, -1, vbBinaryCompare)
        Case FOR_SERVER, CLIENT_TO_SERVER
            'the server needs patched LF only
            Result = myText
            If myMethod = FOR_SERVER Then Result = Replace(myText, vbNewLine, vbLf, 1, -1, vbBinaryCompare)
            If CheckOldVersion Then
                Result = ReplaceChars(Result, ClientPatchList, OldServerPatchList)
            Else
                Result = ReplaceChars(Result, ClientPatchList, NewServerPatchList)
            End If
        Case Else
            Result = myText
    End Select
    CleanString = Result
End Function

Public Function BuildBinaryString(myAscList As String) As String
Dim Result As String
Dim ItmNbr As Integer
Dim ItmVal 'as variant
    Result = ""
    ItmNbr = 0
    Do
        ItmNbr = ItmNbr + 1
        ItmVal = GetItem(myAscList, ItmNbr, ",")
        If ItmVal <> "" Then
            Result = Result & Chr(ItmVal)
        End If
    Loop While ItmVal <> ""
    BuildBinaryString = Result
End Function
Public Function ReplaceChars(myText As String, LookFor As String, ReplaceWith As String) As String
Dim Result As String
Dim ilLen As Integer
Dim clLook As String
Dim clPatch As String
Dim i As Integer
    Result = myText
    ilLen = Len(LookFor)
    For i = 1 To ilLen
        clLook = Mid(LookFor, i, 1)
        clPatch = Mid(ReplaceWith, i, 1)
        Result = Replace(Result, clLook, clPatch, 1, -1, vbBinaryCompare)
    Next
    ReplaceChars = Result
End Function
Public Sub CheckFormOnTop(ActForm As Form, SetValue As Boolean)
    If ActForm.Visible = True Then
        If ActForm.OnTop.Value = 1 Then SetFormOnTop ActForm, SetValue
    End If
End Sub
Public Function GetCedaWeekDay(CedaDate As String) As String
    Dim strWkdy As String
    Dim varFday
    If CedaDate <> "" Then
        varFday = CedaDateToVb(Left(CedaDate, 8))
        strWkdy = Trim(Str(Weekday(varFday, vbMonday)))
    End If
    GetCedaWeekDay = strWkdy
End Function
Public Function GetDatePart(CedaDate As String) As String
Dim tmpVal As String
    If Trim(CedaDate) <> "" Then tmpVal = Mid(CedaDate, 1, 9) Else tmpVal = ""
    GetDatePart = tmpVal
End Function

Public Function GetTimePart(CedaDate As String) As String
Dim tmpVal As String
    If Trim(CedaDate) <> "" Then tmpVal = Mid(CedaDate, 9, 2) & ":" & Mid(CedaDate, 11, 2) Else tmpVal = ""
    GetTimePart = tmpVal
End Function

Public Function GetTimeStamp(MinuteOffset As Integer) As String
Dim tmpTime 'as variant
    tmpTime = DateAdd("n", MinuteOffset, now)
    GetTimeStamp = Format(tmpTime, "yyyymmddhhmmss")
End Function

Public Function CheckValidTime(tmpData As String, tmpAllow As String) As Boolean
    Dim Result As Boolean
    Dim tmpChk As String
    Dim tmpVal As Integer
    tmpVal = Val(tmpData)
    tmpChk = Right("0000" & Trim(Str(tmpVal)), 4)
    If tmpChk = tmpData Then
        Result = True
        If tmpVal < 0 Then Result = False
        tmpVal = Val(Left(tmpChk, 2))
        If tmpVal < 0 Then Result = False
        If tmpVal > 23 Then
            If (tmpVal = 24) And (tmpAllow <> "2400") Then Result = False
            If tmpVal > 24 Then Result = False
        End If
        tmpVal = Val(Right(tmpChk, 2))
        If tmpVal < 0 Then Result = False
        If tmpVal > 59 Then Result = False
    Else
        Result = False
    End If
    CheckValidTime = Result
End Function

Public Function BuildAftFkey(tmpAlc3 As String, tmpFltn As String, tmpFlns As String, tmpDate As String, tmpAdid As String) As String
Dim Result As String
Dim tmpData As String
    Result = "00000XXX#00000000U"
    tmpData = Trim(tmpAlc3)
    If tmpData <> "" Then
        Mid(Result, 6, 3) = tmpData
    End If
    tmpData = Trim(tmpFltn)
    If tmpData <> "" Then
        tmpData = Right("00000" & tmpData, 5)
        Mid(Result, 1, 5) = tmpData
    End If
    tmpData = Trim(tmpFlns)
    If tmpData <> "" Then
        Mid(Result, 9, 1) = tmpData
    End If
    tmpData = Trim(tmpDate)
    If tmpData <> "" Then
        tmpData = Left(tmpData, 8)
        Mid(Result, 10, 8) = tmpData
    End If
    tmpData = Trim(tmpAdid)
    If tmpData <> "" Then
        Mid(Result, 18, 1) = tmpData
    End If
    BuildAftFkey = Result
End Function

Public Sub SetItem(ItemList As String, ItemNbr As Integer, ItemSep As String, NewValue As String)
Dim Result As String
Dim CurItm As Integer
Dim ReqItm As Integer
Dim MaxItm As Integer
    Result = ""
    ReqItm = ItemNbr - 1
    For CurItm = 1 To ReqItm
        Result = Result & GetItem(ItemList, CurItm, ItemSep) & ItemSep
    Next
    Result = Result & NewValue & ItemSep
    MaxItm = ItemCount(ItemList, ItemSep)
    ReqItm = ReqItm + 2
    For CurItm = ReqItm To MaxItm
        Result = Result & GetItem(ItemList, CurItm, ItemSep) & ItemSep
    Next
    ItemList = Left(Result, Len(Result) - Len(ItemSep))
End Sub
Public Function ItemCount(ItemList As String, ItemSep As String) As Long
Dim Result As Long
Dim SepPos As Long
Dim SepLen As Long
    If ItemList <> "" Then
        Result = 1
        SepLen = Len(ItemSep)
        SepPos = 1
        Do
            SepPos = InStr(SepPos, ItemList, ItemSep, vbBinaryCompare)
            If SepPos > 0 Then
                Result = Result + 1
                SepPos = SepPos + SepLen
            End If
        Loop While SepPos > 0
    Else
        Result = 0
    End If
    ItemCount = Result
End Function

Public Function DecodeSsimDayFormat(tmpDate As String, InFormat As String, OutFormat As String) As String
    Dim Result As String
    Dim tmpDay As String
    Dim tmpMonth As String
    Dim tmpYear As String
    Dim tmpMonVal As Integer
    Dim ErrValue As Boolean
    Result = tmpDate
    ErrValue = False
    Select Case InFormat
        Case "SSIM2"
            If tmpDate <> "00XXX00" Then
                tmpDay = Left(tmpDate, 2)
                tmpMonth = Mid(tmpDate, 3, 3)
                tmpMonVal = GetItemNo(MonthList, tmpMonth)
                tmpMonth = Right("00" & Trim(Str(tmpMonVal)), 2)
                tmpYear = Right(tmpDate, 2)
                If tmpYear = "99" Then tmpYear = "19" & tmpYear Else tmpYear = "20" & tmpYear
            Else
                ErrValue = True
            End If
        Case "SSIM4"
            tmpDay = Left(tmpDate, 2)
            tmpMonth = Mid(tmpDate, 3, 3)
            tmpMonVal = GetItemNo(MonthList, tmpMonth)
            tmpMonth = Right("00" & Trim(Str(tmpMonVal)), 2)
            tmpYear = Right(tmpDate, 4)
        Case "CEDA"
            tmpYear = Left(tmpDate, 4)
            tmpMonth = Mid(tmpDate, 5, 2)
            tmpDay = Right(tmpDate, 2)
        Case Else
    End Select
    If Not ErrValue Then
        'here we always have a 4 digit year, 2 digit month and 2 digit day
        Select Case OutFormat
            Case "CEDA"
                Result = tmpYear & tmpMonth & tmpDay
            Case "SSIM2"
                tmpMonVal = Val(tmpMonth)
                tmpMonth = GetItem(MonthList, tmpMonVal, ",")
                tmpYear = Right(tmpYear, 2)
                Result = tmpDay & tmpMonth & tmpYear
            Case Else
        End Select
    End If
    DecodeSsimDayFormat = Result
End Function

Public Function StripAftFlno(AftFlno, AftAirl As String, AftFltn As String, AftFlns As String) As String
    Dim Result As String
    Dim tmpDat As String
    AftFltn = Trim(AftFlno)
    AftAirl = Left(AftFltn, 2)
    AftFltn = Mid(AftFltn, 3)
    tmpDat = Left(AftFltn, 1)
    If tmpDat <> "" Then
        If tmpDat < "0" Or tmpDat > "9" Then
            AftAirl = AftAirl & tmpDat
            AftFltn = Mid(AftFltn, 2)
        End If
    End If
    Result = BuildProperAftFlno(AftAirl, AftFltn, AftFlns)
    StripAftFlno = Result
End Function

Public Function BuildProperAftFlno(AftAirl As String, AftFltn As String, AftFlns As String) As String
Dim Result As String
Dim tmpAirl As String
Dim tmpFltn As String
Dim tmpFlns As String
Dim tmpSffx As String
Dim ilLen As Integer
    tmpAirl = Trim(AftAirl)
    tmpFltn = Trim(AftFltn)
    tmpFlns = Trim(AftFlns)
    If tmpAirl <> "" And tmpFltn <> "" Then
        While Left(tmpFltn, 1) = "0"
            tmpFltn = Mid(tmpFltn, 2)
        Wend
        tmpSffx = Right(tmpFltn, 1)
        If tmpSffx <> "" Then
            If tmpSffx < "0" Or tmpSffx > "9" Then
                tmpFlns = tmpSffx
                tmpFltn = Left(tmpFltn, Len(tmpFltn) - 1)
            End If
        End If
        tmpFltn = RTrim(tmpFltn)
        ilLen = Len(tmpFltn)
        If (ilLen >= 0) And (ilLen < 3) Then tmpFltn = Right("000" & tmpFltn, 3)
        If tmpFlns = "" Then tmpFlns = " "
        Result = Left(tmpAirl & "   ", 3)
        Result = Result & Left(tmpFltn & "     ", 5)
        Result = Result & tmpFlns
        AftAirl = tmpAirl
        AftFltn = tmpFltn
        AftFlns = tmpFlns
    Else
        Result = ""
        AftAirl = ""
        AftFltn = ""
        AftFlns = ""
    End If
    BuildProperAftFlno = RTrim(Result)
End Function

Public Sub SleepSomeSeconds(SecondValue As Long)
Dim StartTime
    StartTime = DateAdd("s", SecondValue, now)
    While StartTime > now
        'Sleep
    Wend
End Sub

Public Function CheckValidCedaTime(ChkDateTime As String) As Boolean
    Dim IsValidDate As Boolean
    Dim IsValidTime As Boolean
    Dim Result As Boolean
    Dim tmpDate As String
    Dim tmpTime As String
    Dim tmpSeconds As String
    Dim tmpLen As Integer
    Result = True
    tmpLen = Len(ChkDateTime)
    If (tmpLen = 12) Or (tmpLen = 14) Then
        tmpDate = Left(ChkDateTime, 8)
        IsValidDate = CheckValidDate(tmpDate)
        tmpTime = Mid(ChkDateTime, 9, 4)
        IsValidTime = CheckValidTime(tmpTime, "")
        If tmpLen = 14 Then
            tmpSeconds = Right(ChkDateTime, 2)
        End If
        If Not IsValidDate Then Result = False
        If Not IsValidTime Then Result = False
    Else
        Result = False
    End If
    CheckValidCedaTime = Result
End Function


Public Function CheckValidDate(ChkDate As String) As Boolean
    Dim VbDate 'As Variant
    Dim CedaDate As String
    VbDate = CedaDateToVb(ChkDate)
    CedaDate = Format(VbDate, "yyyymmdd")
    If CedaDate = ChkDate Then
        CheckValidDate = True
    Else
        CheckValidDate = False
    End If
End Function

Public Function GetFileAndPath(UseFileName As String, ActFile As String, ActPath As String) As Integer
    Dim Result As Boolean
    Dim tmpDat As String
    Dim ItmCnt As Integer
    Dim CurItm As Integer
    ItmCnt = ItemCount(UseFileName, "\")
    ActFile = GetItem(UseFileName, ItmCnt, "\")
    ActPath = ""
    ItmCnt = ItmCnt - 1
    For CurItm = 1 To ItmCnt
        ActPath = ActPath & GetItem(UseFileName, CurItm, "\") & "\"
    Next
    CurItm = InStr(ActFile, " ")
    If CurItm > 0 Then
        If CurItm > 1 Then ActPath = ActPath & Left(ActFile, CurItm - 1) & "\"
        ActFile = Mid(ActFile, CurItm + 1)
        ActFile = Trim(ActFile)
    End If
    If Len(ActPath) > 0 Then ActPath = Left(ActPath, Len(ActPath) - 1)
    GetFileAndPath = ItemCount(ActFile, " ")
End Function

Public Sub Sleep(Sekunden%)
    Dim AktuelleZeit As Variant
    AktuelleZeit = Timer
    Do
        'DoEvents
    Loop Until Timer - AktuelleZeit > Sekunden%
End Sub

Public Sub SetTextSelected()
    Dim i As Integer
    Dim MyTextBox As Object
    Set MyTextBox = Screen.ActiveControl
    If TypeName(MyTextBox) = "TextBox" Then
        i = Len(MyTextBox.Text)
        MyTextBox.SelStart = 0
        MyTextBox.SelLength = i
    End If
End Sub


' Add your application to the system tray.
' Param 1 = Handle of form (which deals with sys tray events)
' Param 2 = Icon (form icon - any icon)
' Param 3 = Handle of icon (form icon - any icon)
' Param 4 = Tip for sys tray icon.
'
' Example - AddIconToTray Me.Hwnd, Me.Icon, Me.Icon.Handle, "This is a test tip"
'
Public Sub AddIconToTray(MeHwnd As Long, MeIcon As Long, MeIconHandle As Long, Tip As String)
    With nfIconData
       .hwnd = MeHwnd
       .uID = MeIcon
       .uFlags = NIF_ICON Or NIF_MESSAGE Or NIF_TIP
       .uCallbackMessage = WM_RBUTTONUP
       .hIcon = MeIconHandle
       .szTip = Tip & Chr$(0)
       .cbSize = Len(nfIconData)
    End With
    Shell_NotifyIcon NIM_ADD, nfIconData
End Sub

' Remove your application from the system tray.
' Call when you quit your application.
'
Public Sub RemoveIconFromTray()
    Shell_NotifyIcon NIM_DELETE, nfIconData
End Sub

' Call this routine to ensure my app gets notified of all events
' Example - Hook Me.hWnd
'
Public Sub Hook(Lwnd As Long)
If Hooking = False Then
   FHandle = Lwnd
   WndProc = SetWindowLong(Lwnd, GWL_WNDPROC, AddressOf WindowProc)
   Hooking = True
End If
End Sub

' Call this routine to transfer event notification back to standard handler
' Example - Unhook
'
Public Sub Unhook()
If Hooking = True Then
   SetWindowLong FHandle, GWL_WNDPROC, WndProc
   Hooking = False
End If
End Sub

' Detect a right click event on our system tray icon - pass control to a handler routine
' in the main form (change as required)
Public Function WindowProc(ByVal hw As Long, ByVal uMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
' Ensure that its our app thats affected and that its the right event
If Hooking = True Then
   If uMsg = WM_RBUTTONUP And lParam = WM_RBUTTONDOWN Then
      MyMainForm.SysTrayMouseEventHandler  ' Pass the event back to the form handler
      WindowProc = True               ' Let windows know we handled it
      Exit Function
   End If
   WindowProc = CallWindowProc(WndProc, hw, uMsg, wParam, lParam) ' Pass it along
End If
End Function


Public Function GetKeyItem(ByRef rResult, ByRef rString, ByVal vStartSep, ByVal vEndSep) As Boolean
    Dim llPosStart As Long
    Dim llPosEnd As Long
    
    GetKeyItem = False
    rResult = ""
    llPosStart = InStr(1, rString, vStartSep, vbTextCompare)
    If llPosStart > 0 Then
        GetKeyItem = True
        llPosStart = llPosStart + Len(vStartSep)
        llPosEnd = InStr(llPosStart, rString, vEndSep, vbTextCompare)
        If llPosEnd > 0 Then
            rResult = Mid(rString, llPosStart, (llPosEnd - llPosStart))
        Else
            rResult = Right(rString, Len(rString) - llPosStart + 1)
        End If
    End If
End Function

'Public Sub SetControlFonts(MyForm As Form, MyName As String, MySize As Double, SetBold As Boolean)
'    Dim UseFont As StdFont
'    Dim CurCtrl As Control
'    Set UseFont = New StdFont
'    With UseFont
'        If MyName <> "" Then .Name = MyName
'        If MySize > 0 Then .Size = MySize
'        .Bold = SetBold
'    End With
'    Set MyForm.Font = UseFont
'    On Error Resume Next
'    For Each CurCtrl In MyForm.Controls
'        Set CurCtrl.Font = UseFont
'    Next
'End Sub

Public Sub CheckKeyboard(Flags As Integer)
    If Flags <> KeybdFlags Then
        Select Case Flags
            Case 0
                KeybdState = "None"
                KeybdIsShift = False
                KeybdIsCtrl = False
                KeybdIsAlt = False
            Case 1
                KeybdState = "Shift"
                KeybdIsShift = True
                KeybdIsCtrl = False
                KeybdIsAlt = False
            Case 2
                KeybdState = "Ctrl"
                KeybdIsShift = False
                KeybdIsCtrl = True
                KeybdIsAlt = False
            Case 3
                KeybdState = "Shift/Ctrl"
                KeybdIsShift = True
                KeybdIsCtrl = True
                KeybdIsAlt = False
            Case 4
                KeybdState = "Alt"
                KeybdIsShift = False
                KeybdIsCtrl = False
                KeybdIsAlt = True
            Case 5
                KeybdState = "Shift/Alt"
                KeybdIsShift = True
                KeybdIsCtrl = False
                KeybdIsAlt = True
            Case 6
                KeybdState = "Ctrl/Alt"
                KeybdIsShift = False
                KeybdIsCtrl = True
                KeybdIsAlt = True
            Case 7
                KeybdState = "Shift/Ctrl/Alt"
                KeybdIsShift = True
                KeybdIsCtrl = True
                KeybdIsAlt = True
        End Select
        KeybdFlags = Flags
    End If
End Sub

Public Sub DisplayWinHelpFile(X As Form)
    Dim Foo As Long
    Dim HelpFilePath As String
    Dim HelpFileName As String

    On Local Error GoTo Error_Handler
    HelpFileName = App.EXEName & ".chm"
    HelpFilePath = App.Path & "\" & HelpFileName
    Foo = WinHelp(X.hwnd, HelpFilePath, HELP_INDEX, CLng(0))
Error_End:
    Exit Sub
Error_Handler:
    MsgBox Err.Description
    Resume Error_End
End Sub

Public Sub DisplayHtmHelpFile(UseFileName As String, ContextId As String, UseKeyWord As String)
    Static myHelpPath As String
    Static myHelpAppId 'As Variant
    Dim HelpFilePath As String
    Dim HelpFileName As String
    'Context ID prepared for future use. Actually ignored
    If myHelpAppId > 0 Then
        On Error Resume Next
        Err.Description = ""
        AppActivate myHelpAppId, False
        If Err.Description = "" Then
            SendKeys "%{F4}", True
        End If
    End If
    HelpFileName = App.EXEName & ".chm"
    If myHelpPath = "" Then myHelpPath = GetIniEntry(DEFAULT_CEDA_INI, "GLOBAL", "", "HelpDirectory", "c:\ufis\help")
    HelpFileName = GetIniEntry(DEFAULT_CEDA_INI, App.EXEName, "", "HelpFile", HelpFileName)
    HelpFilePath = myHelpPath & "\" & HelpFileName
    myHelpAppId = Shell("HH " & HelpFilePath, vbNormalFocus)
End Sub

Public Sub ShowHtmlHelp(CurForm As Form, ByVal tHelpFile As String, ByVal tHelpPage As String)
    Static myHelpPath As String
    Dim myHelpAppId 'As Variant
    Dim UseHelpFile As String
    Const HH_DISPLAY_TOPIC = &H0
    On Error Resume Next
    If myHelpPath = "" Then myHelpPath = GetIniEntry(DEFAULT_CEDA_INI, "GLOBAL", "", "HelpDirectory", "c:\ufis\help")
    UseHelpFile = tHelpFile
    If UseHelpFile = "" Then UseHelpFile = App.EXEName & ".chm"
    UseHelpFile = myHelpPath & "\" & UseHelpFile
    ' open the help page in a modeless window
    myHelpAppId = HtmlHelpTopic(CurForm.hwnd, UseHelpFile, HH_DISPLAY_TOPIC, 0)
    If myHelpAppId > 0 Then
        HtmHelpHwnd = CurForm.hwnd
        HtmHelpIsOpen = True
    Else
        HtmHelpHwnd = 0
        HtmHelpIsOpen = False
    End If
End Sub

Public Sub CloseHtmlHelp()
    Dim myHelpAppId 'As Variant
    Const HHH_CLOSE_ALL = &H12
    On Error Resume Next
    If HtmHelpHwnd > 0 Then
        myHelpAppId = HtmlHelpTopic(HtmHelpHwnd, "", HHH_CLOSE_ALL, 0)
        HtmHelpHwnd = 0
    End If
    HtmHelpIsOpen = False
End Sub

Public Function ExistFile(sSpec As String) As Boolean
    On Error Resume Next
    Call FileLen(sSpec)
    ExistFile = (Err = 0)
End Function

Public Function GetWindowsUserName() As String
    Dim l&, Ergebnis&, Fehler&
    Dim User$, Puffer$

    'Benutzernamen ermitteln
    User = Space(255)
    l = 255
    Ergebnis = GetUserName(User, l)

    If Ergebnis <> 0 Then
        GetWindowsUserName = Left$(User, l - 1)
    Else
        GetWindowsUserName = ""
    End If
End Function

Public Function GetWorkStationName() As String
    Dim buffer$, Result&, l&, CName$
    l = MAX_COMPUTERNAME_LENGTH + 1
    buffer = Space$(l)
    Result = GetComputerName(buffer, l)

    If Result = 1 Then
      CName = Left$(buffer, InStr(1, buffer, Chr$(0)) - 1)
      GetWorkStationName = CName
    End If
End Function

' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' -     Loads all strings belonging to all controls of a form.
' -     Depends on the value of the "TAG"
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Public Sub LoadResStrings(frm As Form)
    On Error Resume Next

    Dim ctl As Control
    Dim Obj As Object

    Dim sCtlType As String
    Dim nVal As Integer

    'set the form's caption
    frm.Caption = LoadResString(CInt(frm.Tag))

    For Each ctl In frm.Controls
        sCtlType = TypeName(ctl)
        If sCtlType = "Label" Then
            ctl.Caption = LoadResString(CInt(ctl.Tag))
        ElseIf sCtlType = "Menu" Then
            ctl.Caption = LoadResString(CInt(ctl.Caption))
        ElseIf sCtlType = "TabStrip" Then
            For Each Obj In ctl.Tabs
                Obj.Caption = LoadResString(CInt(Obj.Tag))
                Obj.ToolTipText = LoadResString(CInt(Obj.ToolTipText))
            Next
        ElseIf sCtlType = "Toolbar" Then
            For Each Obj In ctl.Buttons
                Obj.ToolTipText = LoadResString(CInt(Obj.ToolTipText))
            Next
        ElseIf sCtlType = "ListView" Then
            For Each Obj In ctl.ColumnHeaders
                Obj.Text = LoadResString(CInt(Obj.Tag))
            Next
        Else
            nVal = 0
            nVal = Val(ctl.Tag)
            If nVal > 0 Then ctl.Caption = LoadResString(nVal)
            nVal = 0
            nVal = Val(ctl.ToolTipText)
            If nVal > 0 Then ctl.ToolTipText = LoadResString(nVal)
        End If
    Next
End Sub

' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' -     connects to CEDA.
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Public Function ConnectToCeda(ByRef rUfisCom As Object, ByRef sAppl As String, ByRef sHopo As String, ByRef sTableExt As String, ByRef sServer As String, ByRef sConnectType As String) As Boolean
    Dim ret As Integer

    rUfisCom.CleanupCom

    rUfisCom.SetCedaPerameters sAppl, sHopo, sTableExt

    ret = rUfisCom.InitCom(sServer, sConnectType)

    If ret = 0 Then
        MsgBox "Connection to CEDA failed!", vbCritical, "No connection!"
        End
    End If
End Function

' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' -     Delivers the content of a file in a string
' -     only for small files
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Public Function GetFileContent(ByRef sFileName As String) As String
    Dim filelines As Long
    Dim tmpCount As Long
    Dim InputLine As String
    Dim buffer As String

    If sFileName <> "" Then
        Open sFileName For Input As #1   ' Open file for input.
        Do While Not EOF(1)   ' Check for end of file.
           filelines = filelines + 1
           tmpCount = tmpCount + 1
           Line Input #1, InputLine   ' Read line of data.
           buffer = buffer + InputLine
        Loop
        Close #1   ' Close file.
    End If
    GetFileContent = buffer
End Function

Public Function MakeCedaDate(ByVal myDat As Date) As String
    Dim s As String
    Dim strDay As String
    Dim strMonth As String
    Dim strYear As String
    Dim strHour As String
    Dim strMinute As String
    Dim strSeconds As String
    
    Dim strDat As String
    
    strDat = myDat
    strYear = Year(myDat) 'Mid(strDat, 7, 4)
    strMonth = Month(myDat) 'Mid(strDat, 4, 2)
    If Len(strMonth) = 1 Then
        strMonth = "0" + strMonth
    End If
    strDay = Day(myDat) 'Mid(strDat, 1, 2)
    If Len(strDay) = 1 Then
        strDay = "0" + strDay
    End If
    strHour = Hour(myDat) 'Mid(strDat, 12, 2)
    If Len(strHour) = 1 Then
        strHour = "0" + strHour
    End If
    strMinute = Minute(myDat) 'Mid(strDat, 15, 2)
    If Len(strMinute) = 1 Then
        strMinute = "0" + strMinute
    End If
    strSeconds = Second(myDat) 'Mid(strDat, 18, 2)
    s = strYear + strMonth + strDay + strHour + strMinute + strSeconds
    MakeCedaDate = s
End Function
Public Sub WriteToLogFile(logentry As String)
Dim fn As Integer
fn = FreeFile
Open "c:\ufis\If_clock.log" For Append As #fn
Write #fn, Format$(now, "yyyy-mm-dd hh:mm") & ": " & logentry
Close #fn

End Sub

