VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmExtras 
   Caption         =   "Extras ..."
   ClientHeight    =   2535
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   3840
   Icon            =   "frmExtras.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   2535
   ScaleWidth      =   3840
   StartUpPosition =   3  'Windows Default
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   1665
      Top             =   945
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame Frame2 
      Caption         =   "Saving"
      Height          =   1005
      Left            =   135
      TabIndex        =   1
      Top             =   1305
      Width           =   3525
      Begin VB.CommandButton btnLoad 
         Caption         =   "Load"
         Height          =   375
         Left            =   1875
         TabIndex        =   4
         Top             =   360
         Width           =   1410
      End
      Begin VB.CommandButton btnSave 
         Caption         =   "Save"
         Height          =   375
         Left            =   210
         TabIndex        =   3
         Top             =   360
         Width           =   1410
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Printing"
      Height          =   915
      Left            =   135
      TabIndex        =   0
      Top             =   135
      Width           =   3525
      Begin VB.CommandButton btnPrint 
         Caption         =   "Print"
         Height          =   375
         Left            =   1080
         TabIndex        =   2
         Top             =   360
         Width           =   1410
      End
   End
End
Attribute VB_Name = "frmExtras"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const INVALID_HANDLE_VALUE As Long = -1
Private Const MAX_PATH As Long = 260

Private Type SECURITY_ATTRIBUTES
   nLength As Long
   lpSecurityDescriptor As Long
   bInheritHandle As Long
End Type

Private Declare Function CreateDirectory Lib "kernel32" _
    Alias "CreateDirectoryA" _
   (ByVal lpPathName As String, _
    lpSecurityAttributes As SECURITY_ATTRIBUTES) As Long

Private Sub btnPrint_Click()
    On Error GoTo ErrHdl

    Dim strPrintDate As String
    Dim strTitle As String

    strPrintDate = Now
    strTitle = frmMainExport.cbTabtab.Text
    CommonDialog1.CancelError = True
    CommonDialog1.Flags = cdlPDReturnDC Or cdlPDHidePrintToFile Or cdlPDNoSelection 'or cdlPDPrintSetup
    CommonDialog1.ShowPrinter

    frmMainExport.TAB1.PrintTab CommonDialog1.hDC, strTitle, strPrintDate

    Exit Sub
ErrHdl:
    Exit Sub
End Sub

Private Sub btnLoad_Click()
    With frmMainExport
        If Len(.txtServer.Text) = 0 Then MsgBox "Please enter a server name!", vbInformation, "No server name"
        If Len(.txtHopo.Text) = 0 Then MsgBox "Please enter a HOPO!", vbInformation, "No HOPO"
        If Len(.txtTableExt.Text) = 0 Then MsgBox "Please enter a table extension!", vbInformation, "No table extension"
    End With
    Dim strFileNameSystab As String
    Dim strFileNameTabtab As String

    strFileNameSystab = GetFileName & "Systab.udt"
    strFileNameTabtab = GetFileName & "Tabtab.udt"

    If ExistFile(strFileNameSystab) = False Then
        MsgBox "The file " & strFileNameSystab & " was not found. Please connect at least one time an press the save button.", vbInformation, "File not found"
        Exit Sub
    Else
        frmDebug.tabData(0).ResetContent
        frmDebug.tabData(0).ReadFromFileWithProperties strFileNameSystab
    End If

    If ExistFile(strFileNameTabtab) = False Then
        MsgBox "The file " & strFileNameTabtab & " was not found. Please connect at least one time an press the save button.", vbInformation, "File not found"
        Exit Sub
    Else
        frmDebug.tabData(1).ResetContent
        frmDebug.tabData(1).ReadFromFileWithProperties strFileNameTabtab
    End If
    frmMainExport.AfterConnect
End Sub

Private Sub btnSave_Click()
    Dim strFileNameSystab As String
    Dim strFileNameTabtab As String

    If frmMainExport.bmIsConnected = True Then
        CreateNestedFoldersByPath "c:\ufis\system\UfisDatabaseTool"
        strFileNameSystab = GetFileName & "Systab.udt"
        strFileNameTabtab = GetFileName & "Tabtab.udt"

        frmDebug.tabData(0).WriteToFileWithProperties strFileNameSystab
        frmDebug.tabData(1).WriteToFileWithProperties strFileNameTabtab
    Else
        MsgBox "Please connect to a server first!", vbInformation, "Not connected"
    End If
End Sub

Private Function GetFileName() As String
    With frmMainExport
        Dim strFileName As String
        strFileName = "c:\ufis\system\UfisDatabaseTool\"
        strFileName = strFileName & .txtServer.Text & "_"
        strFileName = strFileName & .txtHopo.Text & "_"
        strFileName = strFileName & .txtTableExt.Text & "_"
        GetFileName = strFileName
    End With
End Function

Public Function ExistFile(sSpec As String) As Boolean
    On Error Resume Next
    Call FileLen(sSpec)
    ExistFile = (Err = 0)
End Function

Private Function CreateNestedFoldersByPath(ByVal _
                        completeDirectory As String) As Integer

  'creates nested directories on the drive
  'included in the path by parsing the final
  'directory string into a directory array,
  'and looping through each to create the final path.
  
  'The path could be passed to this method as a
  'pre-filled array, reducing the code.
  
   Dim r As Long
   Dim SA As SECURITY_ATTRIBUTES
   Dim drivePart As String
   Dim newDirectory  As String
   Dim item As String
   Dim sfolders() As String
   Dim pos As Integer
   Dim X As Integer
   
  'must have a trailing slash for
  'the GetPart routine below
   If Right$(completeDirectory, 1) <> "\" Then
      completeDirectory = completeDirectory & "\"
   End If
  
  'if there is a drive in the string, get it
  'else, just use nothing - assumes current drive
   pos = InStr(completeDirectory, ":")

   If pos Then
         drivePart = GetPart(completeDirectory, "\")
   Else: drivePart = ""
   End If

  'now get the rest of the items that
  'make up the string
   Do Until completeDirectory = ""

    'strip off one item (i.e. "Files\")
     item = GetPart(completeDirectory, "\")

    'add it to an array for later use, and
    'if this is the first item (x=0),
    'append the drivepart
     ReDim Preserve sfolders(0 To X) As String

     If X = 0 Then item = drivePart & item
     sfolders(X) = item

    'increment the array counter
     X = X + 1

   Loop

  'Now create the directories.
  'Because the first directory is
  '0 in the array, reinitialize x to -1
   X = -1
   
   Do
   
      X = X + 1
     'just keep appending the folders in the
     'array to newDirectory.  When x=0 ,
     'newDirectory is "", so the
     'newDirectory gets assigned drive:\firstfolder.
     
     'Subsequent loops adds the next member of the
     'array to the path, forming a fully qualified
     'path to the new directory.
      newDirectory = newDirectory & sfolders(X)
      
     'the only member of the SA type needed (on
     'a win95/98 system at least)
      SA.nLength = LenB(SA)
      
      Call CreateDirectory(newDirectory, SA)

   Loop Until X = UBound(sfolders)

  'done. Return x, but add 1 for the 0-based array.
   CreateNestedFoldersByPath = X + 1

End Function

'takes a string separated by "delimiter",
'splits off 1 item, and shortens the string
'so that the next item is ready for removal.
Function GetPart(startStrg As String, delimiter As String) As String
  Dim C As Integer
  Dim item As String

  C = 1
  Do
    If Mid$(startStrg, C, 1) = delimiter Then
      item = Mid$(startStrg, 1, C)
      startStrg = Mid$(startStrg, C + 1, Len(startStrg))
      GetPart = item
      Exit Function
    End If
    C = C + 1
  Loop
End Function
