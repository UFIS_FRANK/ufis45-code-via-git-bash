// CCParam.h: implementation of the CCSParam class
//
//////////////////////////////////////////////////////////////////////
//
// CCSParam declares a class to be used as parameter interface between applications 
//  and the database.
// It was implemented (and not yet tested) as part of the CoCo project
//
//
// It is important tuo use this source in connection to global definitions in CCSGlobl:
//	* ogBCD is the basicdata object, used here as data access layer in BufferPrams() and GetParam()
//	* ofLog is used to log problems when inserting record into the database
//
//
//
//	Version 1.0 on 22.11.1999 by WES
//
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// this is a global !!
CCSParam ogCCSParam;
void CCSParamMakeCedaString(CString &ropText);
void CCSParamMakeClientString(CString &ropText);
CCSParam *pomCCSParam;


// ---------------------------------------------------------
//
// I use my own character mapping functions, taken from CoCo project
//

void CCSParamGrid2Internal(CString &s)
{
	// substitute some strings (decimal values)
	//	\n	->	#10
	//	\r	->	#13
	//	\t	->	#9
	int i, found;
	CString l,r;

	do
	{
		found=0;

		i=s.Find("\\n");
		if (i>=0) {
			l=s.Left(i);
			r=s.Right(s.GetLength()-i-2);
			s=l+"\012"+r;
			found=1; }

		i=s.Find("\\r");
		if (i>=0) {
			l=s.Left(i);
			r=s.Right(s.GetLength()-i-2);
			s=l+"\015"+r;
			found=1; }

		i=s.Find("\\t");
		if (i>=0) {
			l=s.Left(i);
			r=s.Right(s.GetLength()-i-2);
			s=l+"\011"+r;
			found=1; }
	}
	while (found);
}

void CCSParamInternal2Grid(CString &s)
{
	// substitute some strings (decimal values)
	//	\n	<-	#13#10
	//	\r	<-	<space>#13
	//	\t	<-	<space>#7
	int i,found;
	CString l,r;

	do
	{
		found=0;

		i=s.Find("Flug&");
		if (i>=0)
		{
			found=0;
			//int mynum=s[15];
		}

		i=s.Find('\012');
		if (i>=0) 
		{
			l=s.Left(i);
			r=s.Right(s.GetLength()-i-1);
			s=l+"\\n"+r;
			found=1; }

		i=s.Find('\015');
		if (i>=0) 
		{
			l=s.Left(i);
			r=s.Right(s.GetLength()-i-1);
			s=l+"\\r"+r;
			found=1; }

		i=s.Find('\011');
		if (i>=0) 
		{ 
			l=s.Left(i);
			r=s.Right(s.GetLength()-i-1);
			s=l+"\\t"+r;
			found=1; }
	}
	while (found);
}

// ATTENTION!!! adapt if character-mapping changes!
CString omCCSParamClientChars("\042\047\54\012\015\072\173\175\133\135\174\136\77\50\51\73\100");
CString omCCSParamServerChars("\260\261\262\264\263\277\223\224\225\226\227\230\231\233\234\235\236");

void CCSParamMakeCedaString(CString &ropText)
{
	int ilIndex;

	for (int ilLc = 0; ilLc < ropText.GetLength(); ilLc++)
	{
		if ((ilIndex = omCCSParamClientChars.Find(ropText.GetAt(ilLc))) > -1)
		{
			ropText.SetAt(ilLc,omCCSParamServerChars[ilIndex]);
		}
	}
}
void CCSParamMakeClientString(CString &ropText)
{
	for(int i = 0; i < omCCSParamServerChars.GetLength(); i++)
	{
		ropText.Replace(omCCSParamServerChars[i], omCCSParamClientChars[i]);
	}
}


// ---------------------------------------------------------
//  other helper functions by wes
//
int GetYear(CString y){return atoi(y.Left(4));}
int GetMonth(CString y){return atoi(y.Mid(4,2));}
int GetDay(CString y){return atoi(y.Mid(6,2));}
int GetHour(CString y){return atoi(y.Mid(8,2));}
int GetMinute(CString y){return atoi(y.Mid(10,2));}
int GetSecond(CString y){return atoi(y.Mid(12,2));}
bool IsDayOfWeekSet(CString y, int dow){ return (y.Mid(dow,1) == "1");}	// 1=MON, 7=SUN


// ---------------------------------------------------------


// constructor and destructor
CCSParam::CCSParam()
{
	pomCCSParam = this;
	itemcnt=0;
	items=(CCSParamEntry *)NULL;
	// Messages l�schen
	omMessageList.RemoveAll();
}

CCSParam::~CCSParam()
{
	if (itemcnt>0) free(items);
}

BOOL ProcessParCf(BcStruct *popBC)
{
	CCSParamEntry* olMyParam;
	CString *ptimestamp = NULL;
	CString olPAID;
	CString olAPPL;
	CString olVALU;
	CString olStringNow;
	CString olFields = popBC->Fields;
	CString olData = popBC->Data;

	CStringArray olFieldArray;
	CStringArray olDataArray;

	if (ptimestamp == 0)
	{
		CTime olTimeNow = CTime::GetCurrentTime();
		olStringNow = olTimeNow.Format("%Y%m%d%H%M%S");
		ptimestamp = &olStringNow;
	}

	ExtractItemList(olFields, &olFieldArray, ',');
	ExtractItemList(olData, &olDataArray, ',');

	for (int i = 0; i < olFieldArray.GetSize(); i++)
	{
		if (olFieldArray[i] == "PAID")
		{
			olPAID = olDataArray[i];
		}
		else if (olFieldArray[i] == "APPL")
		{
			olAPPL = olDataArray[i];
		}
		else if (olFieldArray[i] == "VALU")
		{
			olVALU = olDataArray[i];
		}
	}

	olMyParam = pomCCSParam->GetParam(olAPPL,olPAID,*ptimestamp,"","","","","","","","","","");
	if (olMyParam != NULL)
	{
		CCSParamMakeClientString(olVALU);
		strcpy(olMyParam->value, olVALU);
	}
	return TRUE;
}

// access functions
int CCSParam::BufferParams(CString appname)
{
	// read all VALTAB and PARTAB entries, belonging to this application
	ogBCD.SetObject("VAL","URNO,APPL,FREQ,TIMF,TIMT,UVAL,VAFR,VATO,TABN");
	POS_VURNO=ogBCD.GetFieldIndex("VAL","URNO");
	POS_VAPPL=ogBCD.GetFieldIndex("VAL","APPL");
	POS_VFREQ=ogBCD.GetFieldIndex("VAL","FREQ");
	POS_VTIMF=ogBCD.GetFieldIndex("VAL","TIMF");
	POS_VTIMT=ogBCD.GetFieldIndex("VAL","TIMT");
	POS_VUVAL=ogBCD.GetFieldIndex("VAL","UVAL");
	POS_VVAFR=ogBCD.GetFieldIndex("VAL","VAFR");
	POS_VVATO=ogBCD.GetFieldIndex("VAL","VATO");
	POS_VTABN=ogBCD.GetFieldIndex("VAL","TABN");

	ogBCD.SetObject("PAR","URNO,APPL,NAME,PAID,PTYP,TXID,TYPE,VALU", true, ProcessParCf);

	//ogDdx.Register((void *)this,S_FLIGHT_CHANGE,CString("FLIGHTDATA"), CString("Flight changed"),ProcessFlightCf);
	POS_PURNO=ogBCD.GetFieldIndex("PAR","URNO");
	POS_PAPPL=ogBCD.GetFieldIndex("PAR","APPL");
	POS_PNAME=ogBCD.GetFieldIndex("PAR","NAME");
	POS_PPAID=ogBCD.GetFieldIndex("PAR","PAID");
	POS_PPTYP=ogBCD.GetFieldIndex("PAR","PTYP");
	POS_PTXID=ogBCD.GetFieldIndex("PAR","TXID");
	POS_PTYPE=ogBCD.GetFieldIndex("PAR","TYPE");
	POS_PVALU=ogBCD.GetFieldIndex("PAR","VALU");
	
	ogBCD.Read("VAL",appname);
	ogBCD.Read("PAR",appname);
	int j=ogBCD.GetDataCount("VAL");		// count entries in VALTAB
	items=(CCSParamEntry *)realloc(items,(itemcnt+j)*sizeof(CCSParamEntry));	// allocate new entries or shrink
	
	// prepare record buffers for reading
	int cnt=ogBCD.GetFieldCount("VAL");
	RecordSet olVRecord(cnt);
	cnt=ogBCD.GetFieldCount("PAR");
	RecordSet olPRecord(cnt);

	CString s;

	// process every record in VALTAB and fill the struct-array
	for (int i = 0; i < j; i++)
	{
		ogBCD.GetRecord("VAL", i, olVRecord);
		s=olVRecord.Values[POS_VUVAL];				// get urno of PARTAB entry
		CString olParValue;
		if (ogBCD.GetRecord("PAR", "URNO", s, olPRecord))
		{
			olParValue = olPRecord.Values[POS_PVALU];
			CCSParamMakeClientString( olParValue);
			CCSParamMakeClientString(olVRecord.Values[POS_VAPPL]); CCSParamInternal2Grid(olVRecord.Values[POS_VAPPL]); strcpy(items[itemcnt].appl,(olVRecord.Values[POS_VAPPL]).Left(8));
			CCSParamMakeClientString(olVRecord.Values[POS_VFREQ]); CCSParamInternal2Grid(olVRecord.Values[POS_VFREQ]); strcpy(items[itemcnt].freq,(olVRecord.Values[POS_VFREQ]).Left(7));
			CCSParamMakeClientString(olVRecord.Values[POS_VTIMF]); CCSParamInternal2Grid(olVRecord.Values[POS_VTIMF]); strcpy(items[itemcnt].timf,(olVRecord.Values[POS_VTIMF]).Left(4));
			CCSParamMakeClientString(olVRecord.Values[POS_VTIMT]); CCSParamInternal2Grid(olVRecord.Values[POS_VTIMT]); strcpy(items[itemcnt].timt,(olVRecord.Values[POS_VTIMT]).Left(4));
			CCSParamMakeClientString(olVRecord.Values[POS_VVAFR]); CCSParamInternal2Grid(olVRecord.Values[POS_VVAFR]); strcpy(items[itemcnt].vafr,(olVRecord.Values[POS_VVAFR]).Left(14));
			CCSParamMakeClientString(olVRecord.Values[POS_VVATO]); CCSParamInternal2Grid(olVRecord.Values[POS_VVATO]); strcpy(items[itemcnt].vato,(olVRecord.Values[POS_VVATO]).Left(14));

			CCSParamMakeClientString(s); CCSParamInternal2Grid(s); strcpy(items[itemcnt].parurno,s.Left(10));
			CCSParamMakeClientString(olVRecord.Values[POS_PPAID]); CCSParamInternal2Grid(olVRecord.Values[POS_PPAID]); strcpy(items[itemcnt].pid,(olPRecord.Values[POS_PPAID]).Left(16));
			CCSParamMakeClientString(olVRecord.Values[POS_PPTYP]); CCSParamInternal2Grid(olVRecord.Values[POS_PPTYP]); strcpy(items[itemcnt].ptyp,(olPRecord.Values[POS_PPTYP]).Left(16));
			CCSParamMakeClientString(olVRecord.Values[POS_PTXID]); CCSParamInternal2Grid(olVRecord.Values[POS_PTXID]); strcpy(items[itemcnt].txid,(olPRecord.Values[POS_PTXID]).Left(32));
			CCSParamMakeClientString(olVRecord.Values[POS_PTYPE]); CCSParamInternal2Grid(olVRecord.Values[POS_PTYPE]); strcpy(items[itemcnt].type,(olPRecord.Values[POS_PTYPE]).Left(4));
			CCSParamMakeClientString(olVRecord.Values[POS_PVALU]); CCSParamInternal2Grid(olVRecord.Values[POS_PVALU]); strcpy(items[itemcnt].value,olParValue.Left(64));
			CCSParamMakeClientString(olVRecord.Values[POS_PNAME]); CCSParamInternal2Grid(olVRecord.Values[POS_PNAME]); strcpy(items[itemcnt].name,(olPRecord.Values[POS_PNAME]).Left(100));
			itemcnt++;
		}
	}

	return itemcnt;
}

//
// check record 
//
bool CCSParam::IsValidRecord(int idx, CString timestamp)
{
	int i,j;
	
	COleDateTime mytime(GetYear(timestamp),GetMonth(timestamp),GetDay(timestamp),GetHour(timestamp),GetMinute(timestamp),GetSecond(timestamp));
	
	bool isvalid = true;
	CString mydate;
	
	// pass 1: date from
	if (strcmp(items[idx].vafr,"") !=0)
	{
		mydate = items[idx].vafr; 
		COleDateTime frtime(GetYear(mydate),GetMonth(mydate),GetDay(mydate),GetHour(mydate),GetMinute(mydate),GetSecond(mydate));
		if (frtime <= mytime)
		{
			isvalid = true;
		}
		else
		{
			isvalid = false;
		}
	}
	
	// pass 2: date until
	if (isvalid)
	{
		if (strcmp(items[idx].vato,"") != 0)
		{
			mydate = items[idx].vato; 
			COleDateTime totime(GetYear(mydate),GetMonth(mydate),GetDay(mydate),GetHour(mydate),GetMinute(mydate),GetSecond(mydate));
			if (totime>=mytime)
			{
				isvalid = true;
			}
			else
			{
				isvalid = false;
			}
		}
		// pass 3: day of week
		if (isvalid)
		{
			mydate=items[idx].freq;
			if ((isvalid) && (mydate != ""))
			{
				i = mytime.GetDayOfWeek()-1;
				if (i==0)
				{
					i=7;		// wrap around for ufis dayofweek format
				}
				isvalid = IsDayOfWeekSet(mydate,i-1);
			}
			// pass 4: daily time frame
			if (isvalid)
			{
				j=atoi(timestamp.Mid(10,4));
				if (isvalid && (strcmp(items[idx].timf,"") != 0))
				{
					isvalid = (atoi(items[idx].timf) <= j);
				}
				if (isvalid && (strcmp(items[idx].timt,"") != 0))
				{
					isvalid=(atoi(items[idx].timt) >= j);
				}
			}
		}
	}
	return isvalid;
}

//*************************************************************************************
// 
//*************************************************************************************
CString CCSParam::GetParamValue(CString appl, CString paramid, CString* ptimestamp/*=0*/)
{
	CCSParamEntry* olMyParam;
	CString olValue;
	olValue.Empty();

	CString olStringNow;
	if(ptimestamp == 0)
	{
		CTime	olTimeNow = CTime::GetCurrentTime();
		olStringNow = olTimeNow.Format("%Y%m%d%H%M%S");
		ptimestamp = &olStringNow;
	}

	olMyParam = GetParam(appl,paramid,*ptimestamp,"","","","","","","","","","");

	if (olMyParam == NULL) 
	{
		// Fehlermeldung anzeigen, wenn gew�nscht.
		CString olDate = "Date: " + ptimestamp->Mid(6,2) + "." + ptimestamp->Mid(4,2) + "." + ptimestamp->Left(4);
		// Zeit formatieren
		CString olTime = "Time: " + ptimestamp->Mid(8,2) + ":" + ptimestamp->Mid(10,2);
		AfxMessageBox(LoadStg(IDS_STRING1845) + "\n" + "Parameter: " + paramid + "\n" + olDate + "\n" + olTime, MB_OK|MB_ICONEXCLAMATION);
	}
	else
	{
		olValue = olMyParam->value;
	}
	return olValue;
}

//*************************************************************************************
//
//*************************************************************************************

CCSParamEntry* CCSParam::GetParam(CString appl, CString paramid, CString timestamp,
								CString defaultvalue,CString name,CString paramtyp , CString txid, 
								CString type, CString datefrom , CString dateto,
								CString timefrom,CString timeto,CString freq, bool bpMakeNewDBEntry)
{
	CString olErrorMsg;

	// does key(appname, paramid) exist?
	int i = itemcnt - 1;
	bool blCanCreate = true;
	bool blIsValid = false;

	// Gibt an, ob ein Parameter neu in die DB geschrieben wird, falls noch nicht gefunden.
	blCanCreate = bpMakeNewDBEntry;

	// application name can only be 8 Chars
	appl = appl.Left(8);
	
	// search a the parameter and check if it is valid
	while ((i>=0) && (!blIsValid))
	{
		if ((strcmp(appl,items[i].appl) == 0) && (strcmp(paramid,items[i].pid) == 0))
		{
			if (IsValidRecord(i,timestamp))
			{
				blIsValid = true;
			}
			else 
			{
				blCanCreate = false;
				i--;
			}
		}
		else
		{
			i--;
		}
	}
	if (i >= 0) 
	{	// yes -> return address
		return &(items[i]);
	}
	else
	{	// no -> was it invalid or not existing -> create new entries with default values in PARTAB and VALTAB
		if (!blCanCreate) 
		{
			ogRosteringLogText.Format("CCSParam: Item not found or invalid (%i) 1=valid\nItem not created:%s\n",blIsValid,paramid);
			WriteInRosteringLog(LOGFILE_OFF);
			return NULL;
		}

		CString myurno,s;

		// prepare record buffers for reading
		int cnt=ogBCD.GetFieldCount("VAL");
		RecordSet olVRecord(cnt);
		cnt=ogBCD.GetFieldCount("PAR");
		RecordSet olPRecord(cnt);

		myurno.Format("%i",ogBCD.GetNextUrno());
		items=(CCSParamEntry *)realloc(items,(itemcnt+1)*sizeof(CCSParamEntry));	// allocate new entries or shrink
		s=appl.Left(8);         strcpy(items[itemcnt].appl,s);
		s=paramid.Left(16);     strcpy(items[itemcnt].pid,s);
		s=defaultvalue.Left(14);strcpy(items[itemcnt].name,s);
		s=defaultvalue.Left(64);strcpy(items[itemcnt].value,s);
		s=myurno.Left(10);      strcpy(items[itemcnt].parurno,s);
		s=paramtyp.Left(16);    strcpy(items[itemcnt].ptyp,s);
		s=txid.Left(32);        strcpy(items[itemcnt].txid,s);
		s=datefrom.Left(14);    strcpy(items[itemcnt].vafr,s);
		s=dateto.Left(14);      strcpy(items[itemcnt].vato,s);
		s=timefrom.Left(4);     strcpy(items[itemcnt].timf,s);
		s=timeto.Left(4);       strcpy(items[itemcnt].timt,s);
		s=freq.Left(7);         strcpy(items[itemcnt].freq,s);
		s=type.Left(4);			strcpy(items[itemcnt].type,s);
		s=name.Left(100);		strcpy(items[itemcnt].name,s);

		s=items[itemcnt].appl;    CCSParamGrid2Internal(s); CCSParamMakeCedaString(s); olPRecord.Values[POS_PAPPL]=s;
		s=items[itemcnt].parurno; CCSParamGrid2Internal(s); CCSParamMakeCedaString(s); olPRecord.Values[POS_PURNO]=s;
		s=items[itemcnt].pid;     CCSParamGrid2Internal(s); CCSParamMakeCedaString(s); olPRecord.Values[POS_PPAID]=s;
		s=items[itemcnt].ptyp;    CCSParamGrid2Internal(s); CCSParamMakeCedaString(s); olPRecord.Values[POS_PPTYP]=s;
		s=items[itemcnt].txid;    CCSParamGrid2Internal(s); CCSParamMakeCedaString(s); olPRecord.Values[POS_PTXID]=s;
		s=items[itemcnt].type;    CCSParamGrid2Internal(s); CCSParamMakeCedaString(s); olPRecord.Values[POS_PTYPE]=s;
		s=items[itemcnt].value;   CCSParamGrid2Internal(s); CCSParamMakeCedaString(s); olPRecord.Values[POS_PVALU]=s;
		s=items[itemcnt].name;    CCSParamGrid2Internal(s); CCSParamMakeCedaString(s); olPRecord.Values[POS_PNAME]=s;

		if (ogBCD.InsertRecord("PAR",olPRecord, true) == false)
		{
			// warning: problem when inserting parameter record into database
			ogLog.Trace("DEBUG", "[CCSParam::InsertRecord] problem when inserting parameter record into database PARTAB.");
			return NULL;
		}
		else
		{
			s=items[itemcnt].appl;    CCSParamGrid2Internal(s); CCSParamMakeCedaString(s); olVRecord.Values[POS_VAPPL]=s;
			s=items[itemcnt].freq;    CCSParamGrid2Internal(s); CCSParamMakeCedaString(s); olVRecord.Values[POS_VFREQ]=s;
			s=items[itemcnt].timf;    CCSParamGrid2Internal(s); CCSParamMakeCedaString(s); olVRecord.Values[POS_VTIMF]=s;
			s=items[itemcnt].timt;    CCSParamGrid2Internal(s); CCSParamMakeCedaString(s); olVRecord.Values[POS_VTIMT]=s;
			s=items[itemcnt].vafr;    CCSParamGrid2Internal(s); CCSParamMakeCedaString(s); olVRecord.Values[POS_VVAFR]=s;
			s=items[itemcnt].vato;    CCSParamGrid2Internal(s); CCSParamMakeCedaString(s); olVRecord.Values[POS_VVATO]=s;
			s=items[itemcnt].parurno; CCSParamGrid2Internal(s); CCSParamMakeCedaString(s); olVRecord.Values[POS_VUVAL]=s;
			s="PAR";				  CCSParamGrid2Internal(s); CCSParamMakeCedaString(s); olVRecord.Values[POS_VTABN]=s;

			if (ogBCD.InsertRecord("VAL",olVRecord, true) == false)
			{
				
				// warning: problem when inserting validation record into database
				ogLog.Trace("DEBUG", "[CCSParam::InsertRecord] problem when inserting parameter record into database VALTAB.");
				return NULL;
			}
			else
			{	
				// Meldung in Stringliste eintragen
				olErrorMsg = "Parameter: "+paramid+"; Application: "+appl+"; Type: "+paramtyp +";";
				omMessageList.AddTail(olErrorMsg);
			}
		}
		itemcnt++;
		if (IsValidRecord(itemcnt-1, timestamp)) 
		{
			return &(items[itemcnt-1]);
		}
		else
		{
			return NULL;
		}
	}
}