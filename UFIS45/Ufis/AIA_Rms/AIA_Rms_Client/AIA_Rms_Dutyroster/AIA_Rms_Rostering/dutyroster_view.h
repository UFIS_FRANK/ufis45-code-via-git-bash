#if !defined(AFX_DUTYROSTER_VIEW_H__978497A4_5C56_11D3_8EF7_00001C034EA0__INCLUDED_)
#define AFX_DUTYROSTER_VIEW_H__978497A4_5C56_11D3_8EF7_00001C034EA0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DutyRoster_View.h : header file

// Dokument (nicht benutzt)
#include <stdafx.h>
#include <RosteringDoc.h>
#include <basicdata.h>
#include <Table.h>
#include <CViewer.h>
#include <InfoBoxDlg.h>
// Mitarbeiterstammdaten
#include <CedaStfData.h>
#include <CedaSdtData.h>
// Klasse zur Zeitkontenberechnung
#include <Accounts.h>
// Gruppen-Klasse
#include <Groups.h>
// Tabellencontrols
#include <CCSDynTable.h>
// Tagesschichtdaten
#include <CedaDrrData.h>
#include <CedaDrgData.h>
#include <ExcelExportDlg.h>

// Externe class-defs
class DutySolidTabViewer;
class DutyIsTabViewer;
class DutyDebitTabViewer;
class DutyStatTabViewer;
class DutyBsdDlg;
class DutyReleaseDlg;
class DutyAttentionDlg;
class DutyPlanPs;
class CSendToSapDlg;
class CTageslisteDlg;
class DutyDragDropDlg;
class DutyAccountDlg;
class CedaDrrData;
class CedaDrgData;
class ExcelExportDlg;

//****************************************************************************
// struct VIEWINFO
//****************************************************************************

#define STAT_PLAN			1
#define STAT_LONGTIME		2
#define STAT_DUTY			3
#define STAT_ALL			4
#define STAT_CURRENT		5

struct VIEWINFO
{
	bool bCorrectViewInfo;
									//PPage
	COleDateTime oDateFrom;			// 1	User select beginn
	COleDateTime oDateTo;			// 1	User select end
	COleDateTime oShowDateFrom;		// -	System/Show select beginn
	COleDateTime oShowDateTo;		// -	System/Show select end
	COleDateTime oLoadDateFrom;		// -	Load select beginn
	COleDateTime oLoadDateTo;		// -	Load select end

	CString oPfcUrnos;				// 1	Function Urnos (show in DutyIsTabViewer)
	bool bSelectAllFunc;			// 1	Alle Funktionen filtern/anzeigen
	CString oOrgUrnos;				// 1	Organisation Urnos
	CString oACT_ICAOCode;			// 1	ACT ICAO code - Aircraft Type
	CString oALT_ICAOCode;			// 1	ALT Airline 3-Lettercode - Airline
	CString oPfcStatUrnos;			// 3	Function Urnos (show in DutyDebitTabViewer)
	CString oOrgStatUrnos;			// 3	Organisations Urnos (Shifts show in DutyDebitTabViewer and DragDropListe)
	CString oPfcFctcs;				// -	Function Codes (show in DutyIsTabViewer)
	CString oOrgDpt1s;				// -	Organisation Codes
	
	CString oPfcStatFctcs;			// -	Function Codes (show in DutyDebitTabViewer)
	CString oOrgStatDpt1s;			// -	Function Codes (show in DutyDebitTabViewer)
	CStringArray oStfUrnos;			// -	Staff Urnos    (show in DutyIsTabViewer)
	CStringArray oStfStatUrnos;		// -	Staff Urnos		(shows in DutyDebitTabViewer)
	    
	CString	oPType;					// 2	Planning Types => 1= Shift roster, 2= Duty roster, 
									//						  3= Employee daily list, 4= Final processing,
									//						  U=  Vacation/Free/Ill, W= Wishes
	int	iPresent;					// 2	1=All     2=Present Staffs     3=not Present Staffs
	bool bGroupStat;				// 3	J/N (mit GruppenStatistik = true)
	bool bDetailStat;				// 3	J/N (mit DetailStatistik = true)
	bool bExtraShifts;				// 3	J/N (mit Sonderschichten anzeigen = true)
	int	iDCoverage;					// 3	1=Actual/Planned     2=Differences
	bool bAdditiv;					// 3	J/N (Schichten nach Funktionen trennen = false)
	bool bAllCodes;					// 3	J/N (Alle Codes einlesen = true)
	int iGetStatFrom;				// 3	woraus sollen die Ist-Werte der Statistik ermittelt werden?
									//		 'PLAN'/1->exklusiv Schichtplan-DRRs, 
									//		 'LANGZEIT'/2->exklusiv Langzeitdienstplan,
									//		 'DIENSTPLAN'/3->aktuelle DRRs ab Dienstplan,
									//		 'ALLE'/4->DRRs mit h�chster Stufe ab Schichtplan, 
									//		 'CURRENT'/5->alt, ROS/DSR
	int	iEName;						// 4	1=Initiales     2=First and last Name 3=Shortname 4=NameConfiguration
	CString oNameConfigString;		// 4	Configuration for displaying the names
	int iSortFVN;					// 4	Sortierung nach Funktion/Vertrag/Name(=1) oder nur nach Name (=0)
	int	iGroup;						// 4	1=No     2=Groups
	long lPlanGroupUrno;			// 4	Urno der PlanungsGruppe, die gezeigt werden soll
	CStringArray oWorkGroupUrnos;	// -	ArbeitsGruppen Urnos die der PlanungsGruppe zugeteilt sind
	CStringArray oWorkGroupCodes;	// -	ArbeitsGruppen Codes die der PlanungsGruppe zugeteilt sind
	CStringArray oPlanGroupCodes;	// -	Member Codes in Pgp
	CStringArray oPlanGroupUrnos;	// -	Member Urnos in Pgp
	CStringArray oPlanGroupMin;		// -	Min f�r Member in Pgp
	CStringArray oPlanGroupMax;		// -	Max f�r Member in Pgp
	bool bSortDayly;				// 4	J/N (Tageweise Sortieren = true)
	COleDateTime oSelectDate;		// -	From User select Date

	CUIntArray oAccounts;			// 5	Accounts from enum ACCOUNT_TYPES in CAccounts  (schow in DutySattTabVier)
	COleDateTime oAccountDateFrom;	// -	Account beginn (for Accounts)
	COleDateTime oAccountDateTo;	// 5	Tag zu dem die Konten anzuzeigen sind (default = oDateTo)
	int iDayCount;				    // 5	Anzahl der Anzeigetage
	bool bDoConflictCheck;			// 5	true - Konfliktpr�fung durchf�hren
	bool bCheckMainFuncOnly;		// 5	true - Schichtzuordnung nur f�r Stammfunktionen pr�fen, sonst f�r alle

	VIEWINFO(void)
	{ 
		bCorrectViewInfo = false;
		oDateFrom.SetStatus(COleDateTime::invalid);
		oDateTo.SetStatus(COleDateTime::invalid);		
		oShowDateFrom.SetStatus(COleDateTime::invalid);		
		oShowDateTo.SetStatus(COleDateTime::invalid);		
		oLoadDateFrom.SetStatus(COleDateTime::invalid);		
		oLoadDateTo.SetStatus(COleDateTime::invalid);		
		oPfcUrnos		= "";
		bSelectAllFunc	= false;
		oOrgUrnos		= "";
		oACT_ICAOCode		= "";
		oALT_ICAOCode		= "";	
		oPfcFctcs		= "";
		oOrgDpt1s		= "";
		oPfcStatUrnos	= "";
		oOrgStatUrnos	= "";
		oPfcStatFctcs	= "";
		oOrgStatDpt1s	= "";
		oPType			= "";
		bGroupStat		= false;
		bDetailStat		= true;
		bExtraShifts	= false;
		bAdditiv		= true,
		bAllCodes		= false,
		iGetStatFrom	= STAT_PLAN,	
		iDCoverage		= 1;
		iEName			= 2;
		oNameConfigString	= "";
		iSortFVN		= 0;
		iPresent		= 1;
		iGroup			= 1;
		oSelectDate.SetStatus(COleDateTime::invalid);	
		lPlanGroupUrno = 0;
		bSortDayly		= false;
		oAccountDateFrom.SetStatus(COleDateTime::invalid);	
		oAccountDateTo.SetStatus(COleDateTime::invalid);	
		iDayCount		= 7;
		bDoConflictCheck = true;
		bCheckMainFuncOnly = true;
	}
};

//****************************************************************************
// struct ATTENTIONINFO
//****************************************************************************

struct ATTENTIONINFO
{
	CString Text;
	COLORREF Color;
	ATTENTIONINFO(void)
	{ 
		Color = GetSysColor(COLOR_WINDOWTEXT);
	}
};

//****************************************************************************
// enum RButtonDownStatus
//****************************************************************************

enum RButtonDownStatus
{
	RBDS_NOTHING = 0,
	RBDS_ALL,
	RBDS_NODSR
};

//****************************************************************************
// enum Diverses
//****************************************************************************

enum Diverses
{
	MENUE_PLAN					= 77888889,
	MENUE_CONFLICTCHECK,
	MENUE_CONFLICTCHECKALL,
	MENUE_OPENDRR,
	MENUE_OPENACCOUNT1, 
	MENUE_OPENACCOUNT2,
	// ADO
	MENUE_WISH					= 103,
	MENUE_DELETE_WISH			= 104,
	MENUE_ADD_WISH				= 105,
	MENUE_SHOW_WISH				= 106,
	MENUE_CREATE_DOUBLETOUR		= 107,
	MENUE_CANCEL_DOUBLETOUR		= 108,
	// APO
	MENUE_ADD_DRR				= 109,
	MENUE_EDIT_DRR				= 110,
	// Dummys f�r Popup-Men�
	MENUE_DUMMY_111				= 111,
	MENUE_DUMMY_112				= 112,
	MENUE_DUMMY_113				= 113,
	MENUE_DUMMY_114				= 114,
	MENUE_DUMMY_115				= 115,
	MENUE_DUMMY_116				= 116,
	MENUE_DUMMY_117				= 117,
	MENUE_DUMMY_118				= 118,
	MENUE_DUMMY_119				= 119,
	MENUE_DELETE_DRR			= 120,
	// Dummys f�r Popup-Men�
	MENUE_DUMMY_121				= 121,
	MENUE_DUMMY_122				= 122,
	MENUE_DUMMY_123				= 123,
	MENUE_DUMMY_124				= 124,
	MENUE_DUMMY_125				= 125,
	MENUE_DUMMY_126				= 126,
	MENUE_DUMMY_127				= 127,
	MENUE_DUMMY_128				= 128,
	MENUE_DUMMY_129				= 129,
	// 
	MENUE_DELETE_ABSENCE		= 130,		// DRR "U"
	MENUE_CLEAR_DRR				= 131,
	MENUE_CHANGE_SHIFT			= 132,
	MENUE_PRINTDAILYROSTER		= 133

};

//****************************************************************************
// struct STATISTICSTRUCT
//****************************************************************************

struct STATISTICSTRUCT
{
	long lStfUrno;				//Mitarbeiter-Urno

	double dWorkingHour;		//Arbeitszeitkonto
	bool bWorkingHourOK;
	double dVacation;			//Urlaubskonto
	bool bVacationOK;

	STATISTICSTRUCT(void)
	{
		lStfUrno		= 0;
		dWorkingHour	= 0;
		bWorkingHourOK = false;
		dVacation		= 0;
		bVacationOK		= false;
	}
};

//****************************************************************************
// struct RETURNSTRUCT
//****************************************************************************

struct RETURNSTRUCT
{
	int iInt;
	long lLong;
	double dDouble;
	bool bBool;
	CString oString;

	RETURNSTRUCT(void)
	{
		iInt	= 0;
		lLong	= 0;
		dDouble	= 0;
		bBool	= false;
	}
};

//****************************************************************************
// DutyRoster_View form view
//****************************************************************************

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

class DutyRoster_View : public CFormView
{
protected:
	DutyRoster_View();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(DutyRoster_View)

// Implementierung
public:
// Funktionen
	// Wird bei Gr��en�nderung des Fensters eingesetzt.
	void SizeWindow(UINT nSide, LPRECT lpRect );
	// Gr��e des Fensters
	CRect GetStartWindowRect(void) {return omWindowRect;}

	// gibt den Status des aktualisierungs Buttons zur�ck
	bool GetActualButtonNeed(void) {return bmNeedActualButton;}
	// Maximale und minimale Gr��e des Views.
	CPoint GetMaxTrackSize();
    CPoint GetMinTrackSize();

	// Mitarbeiter aus Ansicht l�schen
	void DeleteStaffFromData(long lpStfUrno);
	// Systemmeldung oder Ereignis ins Journal
	void InsertAttention(CString opText,COLORREF opColor = BLACK);
	// Mitarbeiter in Ansicht laden
	void LoadStaffInData(long lpStfUrno, CTime opFrom = -1, CTime opTo = -1);

	// Ausgabe aller Warnungen an Attention List
	void ForwardWarnings();

	// Broadcasthandler f�r DRR-�nderung (Daily Roster Record)
	void ProcessDrrChange(DRRDATA *prpDrr, int ipDDXType);
	// Broadcasthandler f�r DRG-�nderung (Daily Roster Groups)
	void ProcessDrgChange(DRGDATA *prpDrg, int ipDDXType);

	void UpdateComboBox(bool bpChangeView=true);
	bool MustStaffInPeriode(long lpStfUrno);
	void FillGroupStruct(bool bpSortGroupsNew = true);
	void ChangeGroupMembers(INLINEDATA *prpInlineUpdate);
	void FillAccountStruct();

	void OpenAccount(int ipNr);
	// Gr��e und Position des Windows beim ersten Aufruf berechnen
	CRect CalcInitialWindowSizeAndPos();
	// ComboBox Setup
	void FillComboBox();
	// Errechnet die benutzbare Gr��e des Fensters
	CRect CalcUsableClientRect();
	// Mitarbeiter-Auswahlliste verf�gbar?
	bool GetEnableSelectEmployee(void) {return rmViewInfo.bCorrectViewInfo;}
	// Dienstplanfreigabe verf�gbar?
	bool GetEnableRelease(void);
	// Dialog mit Kennzahlen verf�gbar?
	bool GetEnableCharacteristicValues(void) {return rmViewInfo.bCorrectViewInfo;}
	// Daten laden
	void LoadData();
	// Tagesdialog aufrufen
	int CallTageslisteDlg(DRRDATA* popDrr);

	/*****************************************************************************
	Erstellt neuen DRR nach Vorgaben, checkt alle Regeln und f�gt den neuen Datensatz ein
	*****************************************************************************/
	bool InsertShiftEx(DRRDATA* popDrr, int ipCodeType, long lpCodeUrno, CString opNewFctc);
	
	// Checkt die Arbeitsregeln, wenn durchfallen - fragt, ob als Teilzeit+ eingef�gt 
	// werden soll, wenn ja, f�gt ein und �ffnet Tagesdialog
	bool InsertShift(DRRDATA* popDrr, bool bpEnableTeilzeitplusQuestion);

	
	/*****************************************************************************
	1. wenn neu einzuf�gende Schicht ODA: alle anderen Schichten des Tages sollen TZ+ sein
	2. wenn neu einzuf�gende Schicht BSD: 
			- wenn eine der Tagesschichten ODA ist, soll diese als TZ+ eingef�gt sein
	*****************************************************************************/
	bool CheckODAandTeilzeitplus(int ipCodeType, DRRDATA* popNewDrr, CCSPtrArray<DRRDATA> opDrrList, bool* pbpShiftInserted);

	/***********************************************************************************
	setzt popDrr zu TZ+ und ruft Tagesdialog auf
	***********************************************************************************/
	bool SetAndUpdateTeilzeitPlusShift(DRRDATA* popDrr, bool bpInsert);
	// Ermittelt Namen der aktuell in der Combobox ausgew�hlten View.
	CString GetSelectedView();

	// laden der Schichtdatens�tze
	bool LoadSdtData(void);

protected:
// Funktionen
	void InitDefaultView();
	// kopiert die momentane Ansicht
	void CopyViewInfo(VIEWINFO &rpViewInfo);
	// initialisiert die Ansicht
	void InitViewInfo(void);
	// Parameter der Ansicht laden und pr�fen
	void GetPropPageParams(VIEWINFO &rpSafetyCopy);
	// Exception-sichere Ermittlung von Strings aus einem StringArray
	bool GetSafeStringFromArray(CStringArray &opValues, int ipIndex, CString &opStrParam);
	// Liste der in der Statistik anzuzeigenden Funktionen generieren
	void GetStatPfcList(void);
	// Liste der selektierten Funktionen generieren
	void GetPfcList(void);
	// Liste der in der Statistik selektierten Organisationen generieren
	void GetStatOrgList(void);
	// Liste der selektierten Organisationen generieren
	void GetOrgList(CString& opOrgUrnos, CString& opOrgDpt1s);
	// Liste der anzuzeigenden Mitarbeiter mit den gew�nschten Funktionen oder Gruppen filtern
	void GetStaffByFunctionsOrGroups(CString& opPfcFctcs, CStringArray& opStfUrnos, CString& opPfcUrnos);
	void GetPfcMap(CMapStringToPtr &opPfcCodeMap, CString& opPfcUrnos);
	// Liste der anzuzeigenden Mitarbeiter mit den gew�nschten Organisationen filtern
	void GetStaffByOrganisations(CStringArray& opStfUrnos, CString& opOrgUrnos, CString& opOrgDpt1s);
	// Liste der anzuzeigenden Mitarbeiter Nach Ein- u.Austrittsdatum filtern und sortieren
	void GetValidSortedStaff();
	// laden der Kontodatens�tze aller Mitarbeiter der Ansicht
	bool LoadAccData(CMapPtrToPtr *popLoadStfUrnoMap, CString opUrnoString = "");
	// laden der Spr
	bool LoadSprData(CMapPtrToPtr *popLoadStfUrnoMap);
	// laden der Tagesschicht-Datens�tze DRR
	bool LoadDrrData(CMapPtrToPtr *popLoadStfUrnoMap);
	// laden der Arbeitsgruppen-Datens�te DRG
	bool LoadDrgData(CMapPtrToPtr *popLoadStfUrnoMap);
	// laden der Tagesschicht-Datens�tze DRS
	bool LoadDrsData(CMapPtrToPtr *popLoadStfUrnoMap);
	// laden der Tagesschicht-Abweichungs-Datens�tze DRD
	bool LoadDrdData(CMapPtrToPtr *popLoadStfUrnoMap);
	// laden der Tagesschicht-Abwesenheits-Datens�tze DRA
	bool LoadDraData(CMapPtrToPtr *popLoadStfUrnoMap);
	// laden der Wunsch-Datens�tze DRW
	bool LoadDrwData(CMapPtrToPtr *popLoadStfUrnoMap, CString opUrnoString = "");
	// die Ansicht des globalen Datenhaltungsobjektes f�r DSRs einstellen
	void SetDsrView(void);
	// Mitarbeiter-Urno aus rechtem Tabellenteil (SolidTabViewer) ermitteln
	long GetStaffUrnoFromSolidTab(void);
	// Dialog zur Mitarbeiter-Ansicht anzeigen
	void DoSelectStaffDialog(CString opStfUrno = "");
	// Erzeugt eine leere default VIEW.
	void GenerateEmptyView();
	// �ffnet den Dialog zum ver�ndern der "Wunsch" Daten
	void OnMenuWish();
	// l�scht W�nsche
	void OnMenuDeleteWish();
	// Anzeigen von W�nschen
	void OnMenuShowWish();
	// // l�scht Abwesenheiten (DRRs)
	void OnMenuDeleteAbsence();
	// Reine Arbeitsfunktion f�r ein paar OnMenuDeleteAbsence()-Aufrufe
	void DeleteDrrOfFieldIfURosl(SELECTEDFIELD* popSelField);
	// Men�eintr�ge zum Bearbeiten zus�tzlicher Schichten anzeigen
	bool ShowMultiDrrMenu(DRRDATA *popDrr, CMenu *popMenu);
	/****************************************************************
	Abh�ngig ob ODA, BSD, schreibgesch�tzt usw, setzt entsprechende Strings rein
	ilType = MENUE_EDIT_DRR || MENUE_CLEAR_DRR oder MENUE_DELETE_DRR
	****************************************************************/
	CString GetDrrMenuString(DRRDATA *popDrr, int ilType);
	// Pr�ft, ob Kontextmenu Punkt "doppeltour anlegen" angezeigt werden kann.
	bool ShowCreateDoubleTourMenu(DRRDATA *popDrr, CMenu *popMenu);
	// Pr�ft, ob Kontextmenu Punkt "doppeltour trennen" angezeigt werden kann.
	bool ShowCancelDoubleTourMenu(DRRDATA *popDrr, CMenu *popMenu);
	// Pr�ft, ob Kontextmenu Punkt "Dienstplan bearbeiten" angezeigt werden kann.
	bool ShowPlanMenu(DRRDATA *popDrr, CMenu *popMenu);
	// Gibt an, ob im Kontextmenu der Menupunkt "Tagesdaten bearbeiten" angezeigt wird
	bool ShowEditDrrMenu(DRRDATA *popDrr, CMenu *popMenu);
	// Gibt an, ob im Kontextmenu der Menupunkt "Konflikpr�fung" angezeigt wird
	bool ShowConflictMenu(DRRDATA *popDrr, CMenu *popMenu);
	// Gibt an, ob im Kontextmenu der Menupunkt "Wunsch anzeigen" angezeigt wird
	bool ShowWishMenu(CString opSday, CString opStfUrno, CMenu *popMenu);
	// Gibt an, ob im Kontextmenu der Menupunkt "Schicht/Abwesenheit l�schen" angezeigt wird
	bool ShowClearDrr(DRRDATA *popDrr, CMenu *popMenu);
	// Zur�cksetzen eines DRRs
	void OnMenuClearDrr() ;
	// Gibt an, ob im Kontextmenu der Menupunkt "Schicht tauschen" angezeigt wird
	bool ShowChangeShift(DRRDATA *popDrr, CMenu *popMenu);
	// Ruft Dialog zum Tauschen der Schicht auf
	void OnMenuChangeShift();

	void SendViewStfuToScbhdl(VIEWINFO *ropOldView, VIEWINFO *ropNewView);
	void SendViewStfuToScbhdlCommand(CString olStfuString, CString olCommand, CString olYear);
	void SendCommand(CString olCommand, CString olAction);

	// Aktuelle DRR finden.
	DRRDATA* GetActualDrr();

	// wenn mindestens ein DRR vorhanden ist, DRR mit der Drrn==1 wird zur�ckgegeben, sonst 0
	DRRDATA* GetFirstDrrOfSelectedField(SELECTEDFIELD* popSelField);

	bool SaveUrnoListInView(CStringArray& opList);
	bool GetUrnoListFromView(CStringArray& opList);

	void SetRet(int ipRet) {imRet = ipRet;};
	int GetRet() {return imRet;}

// Daten
	int imRet;	
	// liest und schreibt die Ansichtseinstellungen in die / aus der Datenbank
	CViewer *pomViewer;
	// ben�tigt f�r die Berechnung der maximalen Gr��e des Fensters
	CPoint omMaxTrackSize;
	// ben�tigt f�r die Berechnung der minimalen Gr��e des Fensters
    CPoint omMinTrackSize;
	// Startfenster f�r DutyDragDropDlg
	CRect omWindowRect;
	// Zeiger auf den nicht-modalen Info-Dialog
	DutyAttentionDlg *pomAttentionDlg;

	DutyDragDropDlg	*pomDutyDragDropDlg;

	// Mitarbeiterliste links
	UINT imSolidTableID;
	CCSDynTable *pomSolidTable;
	CCSEDIT_ATTRIB *prmSolidIPEAttrib;
	
	CViewer *pomCurrentSolidViewer;

	// Der Hauptbereich in der Mitte (Ansicht der Schichten)
	UINT imIsTableID;
	CCSDynTable *pomIsTable;
	CCSEDIT_ATTRIB *prmIsIPEAttrib;
public:
	DutyIsTabViewer *pomIsTabViewer;
	DutyDebitTabViewer *pomDebitTabViewer;
	DutySolidTabViewer *pomSolidTabViewer;
	DutyStatTabViewer *pomStatTabViewer;
protected:	
	CViewer *pomCurrentIsViewer;

	// Die Soll Tabelle ganz unten
	UINT imDebitTableID;
	CCSDebitDynTable *pomDebitTable;
	CCSEDIT_ATTRIB *prmDebitIPEAttrib;
	
	CViewer *pomCurrentDebitViewer;

	// Arbeitszeitkontentabelle rechts
	UINT imStatTableID;
	CCSDynTable *pomStatTable;
	CCSEDIT_ATTRIB *prmStatIPEAttrib;
	
	CViewer *pomCurrentStatViewer;

	InfoBoxDlg *pomInfoBoxDlg;
	INLINEDATA rmPlanInlineData;
	bool bmLastSortDayly;
	bool bmChangeDay;

	// Gibt an, ob der Aktualisierungs Button angezeigt werden soll
	bool bmNeedActualButton;

	// D'n'D Control
	//CCSDragDropCtrl omDragDrop;

	// Zeitraum f�r bereits geladene Daten ACCTAB
	CString omAccLoadedDateYFrom;
	CString omAccLoadedDateYTo;
public:
	// Zeiger auf Objekt zur Berechnung der Mitarbeiter-Arbeitszeitkonten (AZK)
	CAccounts *pomAccounts;

	// Enth�lt alle Mitarbeiter URNO die manuell in die View aufgenommen wurden
	// es sind nicht alle aus der in View gespeicherten omExtraUrnoListInView da,
	// dijenige, die durch Display gekommen sind, sind hier gel�scht
	CStringArray omExtraUrnoListInView;
	CMapStringToOb omExtraUrnoMap;	// schnelles Suchen durch omExtraUrnoListInView

	CString omOldViewName;			// alte View vor dem Aufruf der Dialogmaske
	bool bmReadUrnoList;
	CMapStringToOb omDispExtraUrnoMap;	// Map mit allen MAs, die in View �ber Display oder manuell aufgenommen wurden

public:
	//{{AFX_DATA(DutyRoster_View)
	enum { IDD = IDD_DUTYROSTER_DLG };
	//}}AFX_DATA

// Attributes
public:

	// Sollte im Prinzip noch gekapselt werden.
	VIEWINFO rmViewInfo;
	CCSPtrArray<ATTENTIONINFO> omAttentionInfo;
	CCSPtrArray<STFDATA> omStfData;
	//CCSPtrArray<GROUPSTRUCT> omGroups;			// for Viewers
	CGroups omGroups;			// for Viewers
	//uhi 8.9.00
	//CedaSdtData	omSdtData;
	bool bmDragDopIsOK;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DutyRoster_View)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnDraw(CDC* pDC);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~DutyRoster_View();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(DutyRoster_View)
	afx_msg void OnBActual();
	afx_msg void OnBAttention();
	afx_msg void OnBPrint();
	afx_msg void OnBExcel();
	afx_msg void OnBRelease();
	afx_msg void OnBView();
	afx_msg void OnSelchange_CB_View();
    afx_msg LONG HorzTableScroll(UINT wParam, LONG lParam);
    afx_msg LONG VertTableScroll(UINT wParam, LONG lParam);
    afx_msg LONG HMoveTable(UINT wParam, LONG lParam);
    afx_msg LONG VMoveTable(UINT wParam, LONG lParam);
    afx_msg LONG InlineUpdate(UINT wParam, LONG lParam);
    afx_msg LONG TableRButtonDown(UINT wParam, LONG lParam);
    afx_msg LONG TableLButtonDown(UINT wParam, LONG lParam);
    afx_msg LONG TableLButtonDblClk(UINT wParam, LONG lParam);
    afx_msg LONG TableLButtonUp(UINT wParam, LONG lParam);
    afx_msg LONG TableMouseMove(UINT wParam, LONG lParam);
	afx_msg LONG OnDragOver(UINT wParam, LONG lParam);
	afx_msg void OnMenuPlan();
	afx_msg void OnMenuCreateDoubleTour();
	afx_msg void OnMenuCancelDoubleTour();
	afx_msg void OnMenuConflictCheck();
	afx_msg void OnMenuConflictCheckAll();
	afx_msg void OnMenuOpenDRR();
	afx_msg void OnMenuOpenAccount1();
	afx_msg void OnMenuOpenAccount2();
	afx_msg void OnCancel();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSelectMa();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnClose();
	afx_msg void OnDestroy();
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	//}}AFX_MSG
	afx_msg void OnMenuAddDrr();
	afx_msg void OnMenuEditDrr(UINT ipID);
	afx_msg void OnMenuDeleteDrr(UINT ipID);
	afx_msg void OnMenuPrintDailyRoster();
	DECLARE_MESSAGE_MAP()
};



//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DUTYROSTER_VIEW_H__978497A4_5C56_11D3_8EF7_00001C034EA0__INCLUDED_)
