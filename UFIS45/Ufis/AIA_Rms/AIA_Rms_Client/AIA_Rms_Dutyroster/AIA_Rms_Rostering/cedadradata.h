#ifndef _CEDADRADATA_H_
#define _CEDADRADATA_H_
 
#include <stdafx.h>

///////////////////////////////////////////////////////////////////////////// 

// COMMANDS FOR MEMBER-FUNCTIONS
#define DRA_SEND_DDX	(true)
#define DRA_NO_SEND_DDX	(false)

// Felddimensionen
#define DRA_ABSC_LEN	(5)

// Struktur eines DRA-Datensatzes
struct DRADATA {
	COleDateTime	Abfr;					// abwesend von
	COleDateTime	Abto;					// abwesend bis
	char			Drrn[DRRN_LEN+2]; 		// Schichtnummer (1-n pro Tag)
	char 			Rema[REMA_LEN+2]; 		// Bemerkung
	char 			Sdac[SDAC_LEN+2]; 		// Abwesenheitscode (Code aus ODA)
	char 			Absc[DRA_ABSC_LEN+2]; 	// besondere Abwesenheitsarten
	char 			Sday[SDAY_LEN+2]; 		// Tages-Schl�ssel YYYYMMDD
	long 			Stfu;					// Mitarbeiter-Urno
	long 			Urno;					// Datensatz-Urno
	long 			Bsdu;					// Urno of ODA-record

	//DataCreated by this class
	int			IsChanged;	// Check whether Data has Changed f�r Relaese

	// Initialisation
	DRADATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged = DATA_UNCHANGED;
		Abfr.SetStatus(COleDateTime::invalid);		// Abweichung von
		Abto.SetStatus(COleDateTime::invalid);		// Abweichung bis
		Bsdu = 0;
	}
};	

// the broadcast CallBack function, has to be outside the CedaDraData class
void ProcessDraCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//************************************************************************************************************************
//************************************************************************************************************************
// Deklaration CedaDraData: kapselt den Zugriff auf die Tabelle DRA (Daily Roster
//	Absences - untert�gige Abwesenheit)
//************************************************************************************************************************
//************************************************************************************************************************

class CedaDraData: public CedaData
{
// Funktionen
public:
    // Konstruktor/Destruktor
	CedaDraData(CString opTableName = "DRA", CString opExtName = "TAB");
	~CedaDraData();

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}

// allgemeine Funktionen
	// Feldliste lesen
	CString GetFieldList(void) {return CString(pcmListOfFields);}

// Broadcasts empfangen und bearbeiten
	// behandelt die Broadcasts BC_DRA_CHANGE,BC_DRA_DELETE und BC_DRA_NEW
	void ProcessDraBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

// Lesen/Schreiben von Datens�tzen
	// Datens�tze nach Datum gefiltert lesen
	bool ReadFilteredByDateAndStaff(COleDateTime opStart, COleDateTime opEnd,
									CMapPtrToPtr *popLoadStfUrnoMap = NULL,
									bool bpRegisterBC = true, bool bpClearData = true);
	// Datens�tze einlesen
	bool Read(char *pspWhere = NULL, CMapPtrToPtr *popLoadStfUrnoMap = NULL,
			  bool bpRegisterBC = true, bool bpClearData = true);
	// Datensatz mit bestimmter Urno einlesen
	bool ReadDraByUrno(long lpUrno, DRADATA *prpDra);
	
	// alle DRAs eines Mitarbeiters aus der internen Datenhaltung entfernen (ohne aus der DB zu l�schen!!!)
	bool RemoveInternalByStaffUrno(long lpStfUrno, bool bpSendBC = false);
	// einen Datensatz aus der Datenbank l�schen
	bool Delete(DRADATA *prpDra, bool bpWithSave = true);
	// einen neuen Datensatz in der Datenbank speichern
	bool Insert(DRADATA *prpDra, bool bpSave = true);
	// einen ge�nderten Datensatz speichern
	bool Update(DRADATA *prpDra, bool bpSave = true);

// Datens�tze pr�fen
	// pr�fen, ob es einen DRA zu einem DRR gibt
	bool HasDraOfDrr(DRRDATA *popDrr);
	//uhi 8.3.01 Pr�fen, ob es mindestens einen DRA ohne DEL zu einem DRR gibt
	bool HasDraOfDrrWithoutDel(DRRDATA *popDrr);
	// pr�ft, ob es konkurrierende untert�gige Abwesenheiten zu einer Abwesenheit gibt
	bool IsValidDra(COleDateTime opDateFrom, COleDateTime opDateTo, long lpUrno, 
					CMapPtrToPtr *popDraMap, DRRDATA *popDrr = NULL);

// Datens�tze suchen
	// DRA nach Urno suchen
	DRADATA* GetDraByUrno(long lpUrno);
	// einen Array vom Typ CMapPtrToPtr mit den Zeigern auf die Datens�tze f�llen, die dem DRR <popDrr> zugeordnet sind
	int GetDraMapOfDrr(CMapPtrToPtr *popDraMap, DRRDATA *popDrr);

// Manipulation von Datens�tzen
	// DRADATA erzeugen, initialisieren und in die Datenhaltung mit aufnehmen
	DRADATA* CreateDraData(DRRDATA *popDrr);
	// kopiert die Feldwerte eines DRA-Datensatzes ohne die Schl�sselfelder (Urno, etc.)
	void CopyDra(DRADATA* popDraDataSource,DRADATA* popDraDataTarget);
	// die DRR-Nummer (Drrn) des Datensatzes anpassen
	int DecreaseDrrn(DRRDATA *popDrr);
	// vertauscht die DRR-Nummern
	bool SwapDrrn(DRRDATA *popDrr1, DRRDATA *popDrr2);

protected:	
// Funktionen
// allgemeine Funktionen
	// Tabellenname einstellen
	bool SetTableNameAndExt(CString opTableAndExtName);
	// Feldliste schreiben
	void SetFieldList(CString opFieldList) {strcpy(pcmListOfFields,opFieldList.GetBuffer(0));}
	// alle internen Arrays zur Datenhaltung leeren und beim BC-Handler abmelden
	bool ClearAll(bool bpUnregister = true);

// Broadcasts empfangen und bearbeiten
	// beim Broadcast-Handler anmelden
	void Register(void);

// Kommunikation mit CEDA
	// Kommandos an CEDA senden (die Funktionen der Basisklasse werden �berschrieben
	// wegen des Feldlisten Bugs)
	bool CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
					char *pcpSelection, char *pcpSort, char *pcpData, 
					char *pcpDest = "BUF1",bool bpIsWriteAction = false, 
					long lpID = 0, CCSPtrArray<RecordSet> *pomData = NULL, 
					int ipFieldCount = 0);
    bool CedaAction(char *pcpAction, char *pcpSelection = NULL, char *pcpSort = NULL,
					char *pcpData = pcgDataBuf, char *pcpDest = "BUF1", 
					bool bpIsWriteAction = false, long lpID = 0);
	
// Datens�tze bearbeiten/speichern
	// speichert einen einzelnen Datensatz
	bool Save(DRADATA *prpDra);
	// einen Broadcast DRA_NEW abschicken und den Datensatz in die interne Datenhaltung aufnehmen
	bool InsertInternal(DRADATA *prpDra, bool bpSendDdx);
	// einen Broadcast DRA_CHANGE versenden
	bool UpdateInternal(DRADATA *prpDra, bool bpSendDdx = true);
	// einen Datensatz aus der internen Datenhaltung l�schen und einen Broadcast senden
	void DeleteInternal(DRADATA *prpDra, bool bpSendDdx = true);

// Daten
    // die geladenen Datens�tze
	CCSPtrArray<DRADATA> omData;
    // Map mit den Datensatz-Urnos der geladenen Datens�tze
	CMapPtrToPtr omUrnoMap;
	// beim Broadcast-Handler angemeldet?
	bool bmIsBCRegistered;

	// ToDo: Minimale und maximales G�ltigkeitsdatum einbauen, damit nicht
	// unn�tig viele Datens�tze intern gehalten werden. Die min.-max.-Daten
	// m�ssen bei jeder Leseaktion (ReadXXX) gepr�ft und wenn n�tig erweitert
	// werden. Wenn dann BCs einlaufen (REL,NEW), kann gepr�ft werden ob
	// die Datens�tze in den BCs �berhaupt relevant sind.

	// min. und max. Tag der Ansichts-relevanten DRAs
	COleDateTime omMinDay;
	COleDateTime omMaxDay;
};

#endif	// _CEDADRADATA_H_
