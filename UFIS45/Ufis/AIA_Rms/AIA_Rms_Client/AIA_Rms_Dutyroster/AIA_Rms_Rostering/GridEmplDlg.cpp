// GridEmplDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CGridEmplDlg 

CGridEmplDlg::CGridEmplDlg(CStringList* popUrnoList, CString* opSelectUrno,CMapStringToOb* popExtraUrnoMap,CWnd* pParent)
	: CDialog(CGridEmplDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CGridEmplDlg)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT

	pomEmplList		= 0;
	omSelectUrno	= opSelectUrno;
	pomUrnoList		= popUrnoList;
	pomExtraUrnoMap	= popExtraUrnoMap;
	pomParent		= (DutyRoster_View*)pParent;

	// lokale Kopie der URNOListe herstellen
	POSITION pos;
	pomLocalUrnoList = new CStringList();

	for (pos = pomUrnoList->GetHeadPosition(); pos != NULL; )
	{
		pomLocalUrnoList->AddTail(pomUrnoList->GetNext(pos));
	}
}

CGridEmplDlg::~CGridEmplDlg()
{
	// lokale Urnoliste l�schen
	if ( pomLocalUrnoList )	delete pomLocalUrnoList;
	pomLocalUrnoList = 0;

	// Grid l�schen
	if (pomEmplList) delete pomEmplList;
	pomEmplList = 0;
}

void CGridEmplDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGridEmplDlg)
	DDX_Control(pDX, IDC_B_MOVETO, m_B_MoveTo);
	DDX_Control(pDX, IDCANCEL, m_B_Cancel);
	DDX_Control(pDX, IDC_B_INSERT, m_B_Insert);
	DDX_Control(pDX, IDC_B_DELETE, m_B_Delete);
	DDX_Control(pDX, IDC_B_FIND, m_B_Find);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CGridEmplDlg, CDialog)
	//{{AFX_MSG_MAP(CGridEmplDlg)
	ON_BN_CLICKED(IDC_B_DELETE, OnBDelete)
	ON_BN_CLICKED(IDC_B_INSERT, OnBInsert)
	ON_BN_CLICKED(IDC_B_FIND, OnBFind)
	ON_MESSAGE(WM_GRID_LBUTTONDOWN, OnGridLButton)
	ON_BN_CLICKED(IDC_B_MOVETO, OnBMoveto)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CGridEmplDlg 

BOOL CGridEmplDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// Wartedialog anzeigen
	CWaitDlg olWaitDlg(this,LoadStg(WAIT_DATA_PREPARE));

	// Titel setzen.
	SetWindowText(LoadStg(IDS_STRING61191));

	// Buttontexte setzen.
	m_B_Cancel.SetWindowText(LoadStg(SHIFT_STF_B_CANCEL));
	m_B_Delete.SetWindowText(LoadStg(IDS_STRING1812));
	m_B_Insert.SetWindowText(LoadStg(IDS_STRING1813));
	m_B_Find.SetWindowText(LoadStg(IDS_STRING1817));
	m_B_MoveTo.SetWindowText(LoadStg(IDS_STRING1818));
	
	// Grid initialisieren
	IniGrid();
	SortGrid(2);

	CString csCode;
	csCode = ogPrivList.GetStat("DUTYIDDGRID_IDC_INSERT");

	if(csCode=='1') 
		m_B_Insert.EnableWindow(true);

	if(csCode=='0') 
		m_B_Insert.EnableWindow(false);

	if(csCode=='-') 
		m_B_Insert.ShowWindow(SW_HIDE);

	csCode = ogPrivList.GetStat("DUTYIDDGRID_DELETE");

	if(csCode=='1') 
		m_B_Delete.EnableWindow(true);

	if(csCode=='0') 
		m_B_Delete.EnableWindow(false);

	if(csCode=='-') 
		m_B_Delete.ShowWindow(SW_HIDE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

//**********************************************************************************
// Grid initialisieren
//**********************************************************************************

void CGridEmplDlg::IniGrid ()
{
	// Grid erzeugen
	pomEmplList = new CGridControl ( this, IDC_EMPL_GRID,EMPLCOLCOUNT,ogStfData.omData.GetSize());

	// �berschriften setzen
	pomEmplList->SetValueRange(CGXRange(0,1), LoadStg(SHIFT_STF_PENO));
	pomEmplList->SetValueRange(CGXRange(0,2), LoadStg(SHIFT_S_STAFF));
	pomEmplList->SetValueRange(CGXRange(0,3), LoadStg(SHIFT_STF_FUNCTION));
	pomEmplList->SetValueRange(CGXRange(0,4), LoadStg(SHIFT_STF_WGRP));
	pomEmplList->SetValueRange(CGXRange(0,5), LoadStg(IDS_STRING139));
	pomEmplList->SetValueRange(CGXRange(0,6), LoadStg(IDS_STRING140));

	// Breite der Spalten einstellen
	IniColWidths ();

	// Grid mit Daten f�llen.
	FillGrid();

	pomEmplList->Initialize();
	pomEmplList->GetParam()->EnableUndo(FALSE);
	pomEmplList->LockUpdate(TRUE);
	pomEmplList->LockUpdate(FALSE);
	pomEmplList->GetParam()->EnableUndo(TRUE);

	// Verhindert, das die Spaltenbreite ver�ndert wird
	pomEmplList->GetParam()->EnableTrackColWidth(FALSE);

	// Es k�nnen nur ganze Zeilen markiert werden.
	pomEmplList->GetParam()->EnableSelection(GX_SELROW | GX_SELMULTIPLE | GX_SELSHIFT);

	pomEmplList->SetStyleRange( CGXRange().SetTable(), CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				.SetReadOnly(TRUE));
	pomEmplList->EnableAutoGrow (FALSE);
	pomEmplList->SetLbDblClickAction (WM_COMMAND, IDC_B_MOVETO);
}

//**********************************************************************************
// Grid initialisieren, Breite richtet sich nach Anzahl der Buchstaben im Titel
//**********************************************************************************

void CGridEmplDlg::IniColWidths ()
{
	CString csTest;
	int ilFactor = 7;

	pomEmplList->SetColWidth ( 0, 0, 27);

	csTest = LoadStg(SHIFT_STF_PENO);
	pomEmplList->SetColWidth ( 1, 1, max(100,csTest.GetLength()*ilFactor));

	csTest = LoadStg(SHIFT_STF_NAME);
	pomEmplList->SetColWidth ( 2, 2, max(250,csTest.GetLength()*ilFactor));

	csTest = LoadStg(SHIFT_STF_FUNCTION);
	pomEmplList->SetColWidth ( 3, 3, max(150,csTest.GetLength()*ilFactor));

	csTest = LoadStg(SHIFT_STF_WGRP);
	pomEmplList->SetColWidth ( 4, 4, max(20,csTest.GetLength()*ilFactor));

	csTest = LoadStg(IDS_STRING139); //entry date
	pomEmplList->SetColWidth ( 5, 5, max(70,csTest.GetLength()*ilFactor));

	csTest = LoadStg(IDS_STRING140); //leaving date
	pomEmplList->SetColWidth ( 6, 6, max(70,csTest.GetLength()*ilFactor));
}

//**********************************************************************************
// Grid mit Daten f�llen
//**********************************************************************************

void CGridEmplDlg::FillGrid()
{
	CString		csFullName,csUrno;
	long		ilUrno;
	
	CString *prlSurn = NULL;
	CString *prlResult;
	CString olTmp;
	CString olSurn;
	bool blValid;

	// Get all Functions **************************************************
	CMapStringToPtr olSpfMap;

	// Anzahl ermitteln.
	int ilSpfCount =  ogSpfData.omData.GetSize();
	COleDateTime olReferenceDate = pomParent->rmViewInfo.oDateFrom;
	COleDateTime olVpto;
	
	for (int iSpf = 0; iSpf < ilSpfCount; iSpf++)
	{
		blValid = false;
		
		// wenn VPTO leer ist, dann ist der record noch g�ltig...
		olVpto = ogSpfData.omData[iSpf].Vpto;
		if ((olVpto.GetStatus() == COleDateTime::invalid) || (olVpto.GetStatus() == COleDateTime::null))
		{
			blValid = true;
		}
		if (blValid == false)
		{
			if (olReferenceDate <= olVpto)
			{
				blValid = true;
			}
		}
		if (blValid == true)
		{
			olSurn.Format("%ld",ogSpfData.omData[iSpf].Surn);
			if (olSpfMap.Lookup(olSurn,(void *&)prlSurn) == TRUE)
			{
				if (ogSpfData.omData[iSpf].Prio == "1")
				{
					olTmp = *prlSurn;
					*prlSurn = (CString)ogSpfData.omData[iSpf].Fctc + "," + olTmp;
				}
				else
				{
					*prlSurn +=  "," + (CString)ogSpfData.omData[iSpf].Fctc;
				}
			}
			else
			{
				CString *polCode = new CString;
				*polCode = (CString)ogSpfData.omData[iSpf].Fctc;
				olSpfMap.SetAt(olSurn,polCode);
			}
		}
	}
	// END of Get all Functions *******************************************

	// Get all Work Groups ************************************************
	CMapStringToPtr olSwgMap;
	// Anzahl ermitteln.
	int ilSwgCount =  ogSwgData.omData.GetSize();
	for(int iSwg=0; iSwg<ilSwgCount;iSwg++)
	{
		olSurn.Format("%ld",ogSwgData.omData[iSwg].Surn);
		if(olSwgMap.Lookup(olSurn,(void *&)prlSurn) == TRUE)
		{
			*prlSurn +=  "," + (CString)ogSwgData.omData[iSwg].Code;
		}
		else
		{
			CString *polCode = new CString;
			*polCode = (CString)ogSwgData.omData[iSwg].Code;
			olSwgMap.SetAt(olSurn,polCode);
		}
	}
	// END of Get all Work Groups ******************************************

	// Anzeige austellen
	pomEmplList->LockUpdate(TRUE);
	CObject* polDummy;

	for (int i = 0; i < ogStfData.omData.GetSize(); i++ )
	{
		// Urno auslesen
		ilUrno = ogStfData.omData[i].Urno;
		csUrno.Format("%ld",ogStfData.omData[i].Urno);

		// Urno mit erstem Feld koppeln.						
		pomEmplList->SetStyleRange (CGXRange(i + 1, 1), CGXStyle().SetItemDataPtr( (void*)ilUrno ) );

		// personnel no.
		pomEmplList->SetValueRange(CGXRange(i + 1, 1), ogStfData.omData[i].Peno);

		// Name und Vorname
		VIEWINFO *prmViewInfo = &pomParent->rmViewInfo;
		csFullName = CBasicData::GetFormatedEmployeeName(prmViewInfo->iEName, &ogStfData.omData[i], "", "");
		pomEmplList->SetValueRange(CGXRange(i + 1, 2), csFullName);

		// Wenn die Urno des aktuellen Mitarbeiters in der FUNCTIONMAP gefunden wurde 
		if (olSpfMap.Lookup(csUrno,(void *&)prlResult) == TRUE)
		{
			// Liste der Functions eintragen;
			pomEmplList->SetValueRange(CGXRange(i + 1, 3), *prlResult);
		}

		// Wenn die Urno des aktuellen Mitarbeiters in der WORKGROUPMAP gefunden wurde 
		if(olSwgMap.Lookup(csUrno,(void *&)prlResult) == TRUE)
		{
			// Liste der Arbeitsgruppen eintragen
			pomEmplList->SetValueRange(CGXRange(i + 1, 4), *prlResult);
		}

		// entry and leavin date
		pomEmplList->SetValueRange(CGXRange(i + 1, 5), ogStfData.omData[i].Doem.Format("%d.%m.%Y"));
		pomEmplList->SetValueRange(CGXRange(i + 1, 6), ogStfData.omData[i].Dodm.Format("%d.%m.%Y"));

		// Sollt ein Eintrag mit dieser URNO �bergeben worden sein,
		// Eintrag rot darstellen.
		if(pomLocalUrnoList->Find(csUrno) == NULL)
		{
			pomEmplList->SetStyleRange( CGXRange(i + 1, 1, i + 1, EMPLCOLCOUNT), CGXStyle().SetTextColor(COLORREF(GRAY)));
		}

		// Sollte ein Eintrag mit dieser URNO �bergeben worden sein, Eintrag kursiv darstellen
		if (pomExtraUrnoMap->Lookup(csUrno,polDummy))
		{
			pomEmplList->SetStyleRange( CGXRange(i + 1, 1, i + 1, EMPLCOLCOUNT), CGXStyle().SetFont(CGXFont().SetItalic(TRUE)));
		}
	}

	// Anzeige einstellen
	pomEmplList->LockUpdate(false);
	pomEmplList->Redraw();

	//Delete Maps
	CString *prlString;
	CString olText;
	POSITION olPos;
	for (olPos = olSpfMap.GetStartPosition(); olPos != NULL; )
	{
		olSpfMap.GetNextAssoc(olPos,olText,(void*&)prlString);
		delete prlString;
	}
	olSpfMap.RemoveAll();

	for (olPos = olSwgMap.GetStartPosition(); olPos != NULL; )
	{
		olSwgMap.GetNextAssoc(olPos,olText,(void*&)prlString);
		delete prlString;
	}
	olSwgMap.RemoveAll();
}

//**********************************************************************************
// 
//**********************************************************************************

void CGridEmplDlg::OnBDelete() 
{
	CGXStyle		olStyle;
	long			llUrno;
	CString			olUrno;
	CRowColArray	olRowColArray;
	POSITION		olPos;

	// Selektion ausw�hlen
	pomEmplList->GetSelectedRows (olRowColArray);

	// Wurden Zeilen ausgew�hlt
	if (olRowColArray.GetSize() == 0)
	{
		TRACE ("Keine Zeilen ausgew�hlt.\n");
		return;
	}
	
	// F�r jede ausgew�hlte Zeile abarbeiten
	for (int i=0;i<olRowColArray.GetSize();i++)
	{
		// Style und damit Datenpointer einer Zelle erhalten
		pomEmplList->ComposeStyleRowCol(olRowColArray[i],1, &olStyle );
		// Urno erhalten
		llUrno = (long)olStyle.GetItemDataPtr();
		if (llUrno == 0)
		{
			TRACE ("Urno=0 EmplDlg f�r Zeile%d\n", i );
		}
		else
		{
			olUrno.Format("%ld", llUrno);

			// Urno in der Liste suchen
			olPos = pomLocalUrnoList->Find(olUrno);
			
			// Falls gefunden, Urno l�schen.
			if (olPos != NULL)
			{
				pomLocalUrnoList->RemoveAt(olPos);
				// Aus der Auswahlliste l�schen.
				// Schwarz markieren
				pomEmplList->GetParam()->SetLockReadOnly(FALSE);
				pomEmplList->SetStyleRange( CGXRange(olRowColArray[i],1,olRowColArray[i],EMPLCOLCOUNT), CGXStyle().SetTextColor(BLACK));
				pomEmplList->GetParam()->SetLockReadOnly(TRUE);
			}
		}
	}
	// Dialog beenden
	OnOK();
}

//**********************************************************************************
// Suchen Dialog aufrufen
//**********************************************************************************

void CGridEmplDlg::OnBFind() 
{
	if (pomEmplList)
	{
		pomEmplList->OnShowFindReplaceDialog(TRUE);
	}
}

//**********************************************************************************
// 
//**********************************************************************************

void CGridEmplDlg::OnBInsert() 
{
	CGXStyle		olStyle;
	long			llUrno;
	CString			olUrno;
	CRowColArray	olRowColArray;
		
	// Selektion ausw�hlen
	pomEmplList->GetSelectedRows (olRowColArray);

	// Wurden Zeilen ausgew�hlt
	if (olRowColArray.GetSize() == 0)
	{
		TRACE ("Keine Zeilen ausgew�hlt.\n");
		return;
	}
	
	// F�r jede ausgew�hlte Zeile abarbeiten
	for (int i = 0; i < olRowColArray.GetSize(); i++)
	{
		// Style und damit Datenpointer einer Zelle erhalten
		pomEmplList->ComposeStyleRowCol(olRowColArray[i],1, &olStyle );

		// Urno erhalten
		llUrno = (long)olStyle.GetItemDataPtr();
		if (llUrno == 0)
		{
			TRACE ("Urno=0 in GridEmplDlg f�r Zeile%d\n",olRowColArray[i]);
		}
		else
		{
			olUrno.Format( "%ld", llUrno );
			// Sollte der die URNO noch nicht vorhanden sein, Urno zu TEMP Liste hinzuf�gen
			if(pomLocalUrnoList->Find(olUrno) == NULL)
			{
				// In die Auswahlliste eintragen
				pomLocalUrnoList->AddTail(olUrno);

				// Rot markieren
				pomEmplList->GetParam()->SetLockReadOnly(FALSE);
				pomEmplList->SetStyleRange( CGXRange(olRowColArray[i],1,olRowColArray[i],EMPLCOLCOUNT), CGXStyle().SetTextColor(COLORREF(RED)));
				pomEmplList->GetParam()->SetLockReadOnly(TRUE);
			}
		}
	}
	// Dialog beenden

	OnOK();
}

//**********************************************************************************
// 
//**********************************************************************************

void CGridEmplDlg::OnOK() 
{
	POSITION		pos;
	long			ilUrno;
	CString			olUrno;
	CRowColArray	olRowColArray;
	CGXStyle		olStyle;

	// Lokale URNOListe in �bergebende Liste kopieren
	pomUrnoList->RemoveAll();

	if (!pomLocalUrnoList->IsEmpty())
	{
		for( pos = pomLocalUrnoList->GetHeadPosition(); pos != NULL; )
		{
			pomUrnoList->AddTail(pomLocalUrnoList->GetNext(pos));
		}
	}

	// Aktuelle Auswahl ermitteln und zur�ckgeben
	// Selektion ausw�hlen
	pomEmplList->GetSelectedRows (olRowColArray);

	// Wurden Zeilen ausgew�hlt
	if (olRowColArray.GetSize() == 0)
	{
		TRACE ("Keine Zeilen ausgew�hlt.\n");
	}

	// Style und damit Datenpointer einer Zelle erhalten
	pomEmplList->ComposeStyleRowCol(olRowColArray[0],1, &olStyle );
	// Urno erhalten
	ilUrno = (long)olStyle.GetItemDataPtr();
	if ( !ilUrno )
		TRACE ("Urno=0 in GridEmplDlg f�r Zeile%d\n",olRowColArray[0]);
	else
	{
		olUrno.Format( "%ld", ilUrno );
	// Den Inhalt des Zeigers f�llen
		*omSelectUrno = olUrno;
	}

	// Grid beenden
	pomEmplList->DestroyWindow();

	CDialog::OnOK();
}

//**********************************************************************************
// Left Button Click in das Grid
//**********************************************************************************

LONG CGridEmplDlg::OnGridLButton(WPARAM wParam, LPARAM lParam)
{
	GRIDNOTIFY* rlNotify = (GRIDNOTIFY*) lParam;

	POSITION area = pomEmplList->GetParam( )->GetRangeList( )->AddTail(new CGXRange);
	pomEmplList->SetSelection(area,rlNotify->row,0,rlNotify->row,EMPLCOLCOUNT);

	return 0L;
}

//**********************************************************************************
// l�schen des Grid Objekts
//**********************************************************************************

void CGridEmplDlg::OnCancel() 
{
	// Grid beenden
	pomEmplList->DestroyWindow() ;
	
	CDialog::OnCancel();
}

//**********************************************************************************
// "Gehe zu" Button wurde gew�hlt. Funktion wie OnOK.
//**********************************************************************************

void CGridEmplDlg::OnBMoveto() 
{
	long			ilUrno;
	CString			olUrno;
	CRowColArray	olRowColArray;
	CGXStyle		olStyle;

	// Aktuelle Auswahl ermitteln und zr�ckgeben
	// Selektion ausw�hlen
	pomEmplList->GetSelectedRows (olRowColArray);

	// Wurden Zeilen ausgew�hlt
	if (olRowColArray.GetSize() == 0)
	{
		TRACE ("Keine Zeilen ausgew�hlt.\n");
	}

	// Style und damit Datenpointer einer Zelle erhalten
	pomEmplList->ComposeStyleRowCol(olRowColArray[0],1, &olStyle );
	// Urno erhalten
	ilUrno = (long)olStyle.GetItemDataPtr();
	if ( !ilUrno )
		TRACE ("Urno=0 in GridEmplDlg f�r Zeile%d\n",olRowColArray[0]);
	else
	{
		olUrno.Format( "%ld", ilUrno );
	// Den Inhalt des Zeigers f�llen
		*omSelectUrno = olUrno;
	}

	// Grid beenden
	pomEmplList->DestroyWindow();

	CDialog::OnOK();
}

//**********************************************************************************
// Sortierung des Grids "per Hand"
//**********************************************************************************

bool CGridEmplDlg::SortGrid(int ipRow)
{
	CGXSortInfoArray  sortInfo;
	sortInfo.SetSize( 1 );
	sortInfo[0].sortOrder = CGXSortInfo::ascending;
	sortInfo[0].nRC = ipRow;                       
	sortInfo[0].sortType = CGXSortInfo::autodetect;  
	pomEmplList->SortRows( CGXRange().SetRows(1, ogStfData.omData.GetSize()), sortInfo); 

	return (true);
}
