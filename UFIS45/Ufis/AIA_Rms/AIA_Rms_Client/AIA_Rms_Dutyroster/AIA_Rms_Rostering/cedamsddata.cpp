// cedamasddata.cpp - Class for handling MSD data
//

#include <stdafx.h>

// Das globale Objekt
CedaMsdData ogMsdData;

//************************************************************************************
//
//************************************************************************************

CedaMsdData::CedaMsdData()
{                  
    // Create an array of CEDARECINFO for STAFFDATA
    BEGIN_CEDARECINFO(MSDDATA, MsdDataRecInfo)
		FIELD_CHAR_TRIM(Bsdc, "BSDC")	
		FIELD_CHAR_TRIM(Days, "DAYS")	
		FIELD_CHAR_TRIM(Dnam, "DNAM")	
		FIELD_CHAR_TRIM(Rema, "REMA")	
		FIELD_CHAR_TRIM(Type, "TYPE")	
		FIELD_CHAR_TRIM(Usec, "USEC")	
		FIELD_CHAR_TRIM(Useu, "USEU")	
		FIELD_INT	   (Sdu1, "SDU1")	
		FIELD_LONG	   (Urno, "URNO")	
		FIELD_DATE	   (Bkf1, "BKF1")	
		FIELD_DATE	   (Bkt1, "BKT1")	
		FIELD_DATE	   (Sbgi, "SBGI")	
		FIELD_DATE	   (Seni, "SENI")	
		FIELD_DATE	   (Cdat, "CDAT")	
		FIELD_DATE	   (Esbg, "ESBG")	
		FIELD_DATE	   (Lsen, "LSEN")	
		FIELD_DATE	   (Lstu, "LSTU")	
		FIELD_DATE	   (Brkf, "BRKF")
		FIELD_DATE	   (Brkt, "BRKT")	
		FIELD_LONG	   (Bsdu, "BSDU")	
		FIELD_LONG	   (Sdgu, "SDGU")	
		FIELD_CHAR_TRIM(Fctc, "FCTC")	
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(MsdDataRecInfo)/sizeof(MsdDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&MsdDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
	strcpy(pcmTableName,"MSD");
	strcat(pcmTableName,pcgTableExt);

	strcpy(pcmListOfFields,"DAYS,BSDC,"
						   "DNAM,REMA,TYPE,USEC,USEU,SDU1,"
						   "URNO,BKF1,BKT1,SBGI,SENI,CDAT,ESBG,LSEN,"
						   "LSTU,BRKF,BRKT,BSDU,SDGU,FCTC");

	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	ClearAll();
}

//************************************************************************************
//
//************************************************************************************

void CedaMsdData::Register(void)
{
	CCS_TRY;

	//BC_MSD_CHANGE,BC_MSD_DELETE,BC_MSD_NEW,BC_RELMSD,MSD_CHANGE,MSD_SELCHANGE,MSD_DELETE,MSD_NEW,RELMSD,
	DdxRegister((void *)this,BC_MSD_CHANGE,	"CedaMsdData", "BC_MSD_CHANGE", ProcessMsdCf);
	DdxRegister((void *)this,BC_MSD_DELETE,	"CedaMsdData", "BC_MSD_DELETE",	ProcessMsdCf);
	DdxRegister((void *)this,BC_MSD_NEW,	"CedaMsdData", "BC_MSD_NEW",	ProcessMsdCf);
	DdxRegister((void *)this,BC_RELMSD,		"CedaMsdData", "BC_RELMSD",		ProcessMsdCf);

	CCS_CATCH_ALL;
}

//************************************************************************************
//
//************************************************************************************

CedaMsdData::~CedaMsdData()
{
	ogDdx.UnRegister(this,NOTUSED);
	ClearAll();
	omRecInfo.DeleteAll();
}	

//******************************************************************************************
// Wird in ShiftRoster_View benutzt, es werden offensichtlich nur bestimmte Daten eingelesen
//******************************************************************************************

bool CedaMsdData::ReadSpecialData(CCSPtrArray<MSDDATA> *popMsd, char *pspWhere, char *pspFieldList, bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[2048] = " ";

	// Ist die �bergebende Feldliste g�ltig
	if(strlen(pspFieldList) > 0)
	{
		// Hier wird angegeben welche Felder gelesen werden sollen
		// Bei * werden alle genommen
		if( pspFieldList[0] == '*' )
		{
			strcpy(pclFieldList, pcmFieldList);
		}
		else
		{
			// �bergebene Feldliste wird benutzt
			strcpy(pclFieldList, pspFieldList);
		}
	}

	// Hier finded offensichtlich das Lesen der Daten aus der Datenbank statt..
	// Wohin gehen die Daten ?
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	ogDdx.UnRegister(this,NOTUSED);
	Register();

	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction2("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}

	// Solange wie der R�ckgabewert TRUE ist weitermachen
	for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
	{
		// Einen MSD Datensatz erzeugen.
		MSDDATA *prpMsd = new MSDDATA;
		// Datensatz f�llen
		if ((ilRc = GetBufferRecord2(ilCountRecord,prpMsd,CString(pclFieldList))) == true)
		{
			// Datensatz ins Array einf�gen
			AddMsdInternal(prpMsd);
			//popMsd->Add(prpMsd);
		}
		else
		{
			// letzten Datensatz l�schen, falls ung�ltig
			delete prpMsd;
		}
	}

	// Wenn nichts eingetragen
	if(omData.GetSize() == 0)
		return false;
	else
		return true;
}

//************************************************************************************
// Einlesen der gesamten Informationen (???) Wird bisher im Rostering nicht benutzt.
//************************************************************************************

bool CedaMsdData::Read(char *pspWhere /*NULL*/)
{
	bool blRc = true;
	
	// alle Daten l�schen
	ClearAll();


    // Select data from the database
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	ogDdx.UnRegister(this,NOTUSED);
	Register();

	if(pspWhere == NULL)
	{	
		if (CedaAction2("RTA", "") == false)
			return false;
	}
	else
	{
		if (CedaAction2("RT", pspWhere) == false)
			return false;
	}

	for (int ilCountRecord = 0; blRc == true; ilCountRecord++)
    {
		// Datenhaltungs Struct
		MSDDATA *prlMsd = new MSDDATA;

		if ((blRc = GetFirstBufferRecord2(prlMsd))==true) 
		{
			prlMsd->IsChanged = DATA_UNCHANGED;
			if(AddMsdInternal(prlMsd)==false)
			{
				if(IsTraceLoggingEnabled())
				{
					GetDefectDataString(ogRosteringLogText, (void*)prlMsd);
					WriteInRosteringLog(LOGFILE_TRACE);
				}
				delete prlMsd;
			}
#ifdef TRACE_FULL_DATA
			else
			{
				// Datensatz OK, loggen if FULL
				ogRosteringLogText.Format(" read %8.8lu %s OK:  ", ilCountRecord, pcmTableName);
				GetDataFormatted(ogRosteringLogText, prlMsd);
				WriteLogFull("");
			}
#endif TRACE_FULL_DATA
		}
		else
		{
			delete prlMsd;
			TRACE("Deleted extra prlMsd\n");
		}
	}

    return true;
}

//************************************************************************************
// l�schen aller Daten diese Klasse
//************************************************************************************

bool CedaMsdData::ClearAll()
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
    return true;
}

//************************************************************************************
//
//************************************************************************************

bool CedaMsdData::AddMsdInternal(MSDDATA *prpMsd, bool bpWithDDX /*= false*/)
{
	MSDDATA *prlMsd = NULL;
	if (omUrnoMap.Lookup((void *)prpMsd->Urno,(void *&)prlMsd) == FALSE)
	{
		omData.Add(prpMsd);
		omUrnoMap.SetAt((void *)prpMsd->Urno,prpMsd);

		if (bpWithDDX == true)
		{
			ogDdx.DataChanged((void *)this,MSD_NEW,(void *)prpMsd);
		}
		return true;
	}
	else
	{
		return false;
	}
}

//************************************************************************************
// Update data methods (called from PrePlanTable class)
//************************************************************************************

bool CedaMsdData::DeleteMsd(MSDDATA *prpMSDDATA, BOOL bpWithSave)
{

	bool olRc = true;
	if(prpMSDDATA->IsChanged == DATA_UNCHANGED)
	{
		prpMSDDATA->IsChanged = DATA_DELETED;
	}
	ogDdx.DataChanged((void *)this,MSD_DELETE,(void *)prpMSDDATA);
	
	if(bpWithSave == TRUE)
	{
		CWaitCursor olDummy;
		olRc = SaveMsd(prpMSDDATA);
	}
	return olRc;
}


//**********************************************************************************
//
//**********************************************************************************

bool CedaMsdData::InsertMsd(MSDDATA *prpMSDDATA, BOOL bpWithSave)
{
	bool olRc = true;

	//prpMSDDATA->IsChanged = DATA_NEW;
	if(prpMSDDATA->IsChanged == DATA_UNCHANGED)
	{
		prpMSDDATA->IsChanged = DATA_CHANGED;
	}
	if(bpWithSave == TRUE)
	{
		CWaitCursor olDummy;
		SaveMsd(prpMSDDATA);
	}
	ogDdx.DataChanged((void *)this, MSD_NEW, (void *)prpMSDDATA);
	return olRc;
}

//**********************************************************************************
//
//**********************************************************************************

bool CedaMsdData::UpdateMsd(MSDDATA *prpMSDDATA, BOOL bpWithSave)
{
	bool olRc = true;

	ogDdx.DataChanged((void *)this,MSD_CHANGE,(void *)prpMSDDATA);

	if (prpMSDDATA->IsChanged == DATA_UNCHANGED)
	{
		prpMSDDATA->IsChanged = DATA_CHANGED;
	}

	if (bpWithSave == TRUE)
	{
		CWaitCursor olDummy;
		//SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		SaveMsd(prpMSDDATA);
		//SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}
	return olRc;
}

//**********************************************************************************
//
//**********************************************************************************

bool CedaMsdData::MsdExist(long Urno)
{
	MSDDATA *prlData;
	bool olRc = true;

	if(omUrnoMap.Lookup((void *)Urno,(void *&)prlData)  == TRUE)
	{
		;//*prpData = prlData;
	}
	else
	{
		olRc = false;
	}
	return olRc;
}


//**********************************************************************************
// the callback function for storing broadcasted fligthdata in FLIGHTDATA array
//**********************************************************************************

void ProcessMsdCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogMsdData.ProcessMsdBc(ipDDXType,vpDataPointer,ropInstanceName);
}


//**********************************************************************************
// 
//**********************************************************************************
void CedaMsdData::ProcessMsdBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	//BC_MSD_CHANGE,BC_MSD_DELETE,BC_MSD_NEW,MSD_CHANGE,MSD_SELCHANGE,MSD_DELETE,MSD_NEW
	struct BcStruct *polBcStruct;
	polBcStruct = (struct BcStruct *) vpDataPointer;

	if (ipDDXType == BC_MSD_CHANGE)
	{
		MSDDATA *polMsd;
		MSDDATA olMsd;

		// getting the old MSD-record
		long llUrno = GetUrnoFromSelectionString(CString(polBcStruct->Selection));
		polMsd = GetMsdByUrno (llUrno);

		if (polMsd == NULL)
		{
			MSDDATA *polNewMsd = new MSDDATA;
			GetRecordFromItemList(&polNewMsd, polBcStruct->Fields, polBcStruct->Data);
			if (AddMsdInternal(polNewMsd, true) == false)
			{
				if (IsTraceLoggingEnabled())
				{
					GetDefectDataString(ogRosteringLogText, (void*)polNewMsd);
					WriteInRosteringLog(LOGFILE_TRACE);
				}
				delete polNewMsd;
			}
		}
		else
		{
			GetRecordFromItemList(&olMsd, polBcStruct->Fields, polBcStruct->Data);
			CopyMsdValues(&olMsd, polMsd);
			ogDdx.DataChanged((void *)this, MSD_CHANGE, (void *)polMsd);
		}
	}
	else if (ipDDXType == BC_MSD_DELETE)
	{
		long llUrno = GetUrnoFromSelectionString(CString(polBcStruct->Selection));
		MSDDATA *polMsd = GetMsdByUrno (llUrno);

		if (polMsd)
		{
			DeleteMsdInternal(polMsd, true);
		}
	}
	else if (ipDDXType == BC_MSD_NEW)
	{
		MSDDATA *polNewMsd = new MSDDATA;
		GetRecordFromItemList(polNewMsd, polBcStruct->Fields, polBcStruct->Data);
		if (AddMsdInternal(polNewMsd, true) == false)
		{
			if (IsTraceLoggingEnabled())
			{
				GetDefectDataString(ogRosteringLogText, (void*)polNewMsd);
				WriteInRosteringLog(LOGFILE_TRACE);
			}
			delete polNewMsd;
		}
	}
	else if (ipDDXType == BC_RELMSD)
	{
		SDGDATA *prlSdg = ogSdgData.GetSdgByUrno(atol(polBcStruct->Data));					
		if (prlSdg)
		{
			ogDdx.DataChanged(this,RELMSD,(void *)prlSdg);
		}
	}
}

//**********************************************************************************
// 
//**********************************************************************************

MSDDATA * CedaMsdData::GetMsdByUrno(long pcpUrno)
{
	MSDDATA *prpMsd;

	if (omUrnoMap.Lookup((void*)pcpUrno,(void *& )prpMsd) == TRUE)
	{
		return prpMsd;
	}
	return NULL;
}

//**********************************************************************************
// 
//**********************************************************************************

bool CedaMsdData::SaveMsd(MSDDATA *prpMsd)
{
	bool olRc = true;
	CString olListOfData;
	char pclSelection[512];
	char pclData[2048];

	if (prpMsd->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpMsd->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpMsd);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpMsd->IsChanged = DATA_UNCHANGED;
		if(!omLastErrorMessage.IsEmpty())
		{
			::MessageBox(NULL, omLastErrorMessage.GetBuffer(0), "DB-Zugriffsfehler-Insert", MB_OK);
		}
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpMsd->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpMsd);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpMsd->IsChanged = DATA_UNCHANGED;
		if(!omLastErrorMessage.IsEmpty())
		{
			::MessageBox(NULL, omLastErrorMessage.GetBuffer(0), "DB-Zugriffsfehler-Update", MB_OK);
		}
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpMsd->Urno);
		olRc = CedaAction("DRT",pclSelection);
		if(!omLastErrorMessage.IsEmpty())
		{
			::MessageBox(NULL, omLastErrorMessage.GetBuffer(0), "DB-Zugriffsfehler-Delete", MB_OK);
		}
		break;
	}
   return olRc;
}

//**********************************************************************************
// 
//**********************************************************************************

void CedaMsdData::GetMsdBySdgu(long lpSdgu,CCSPtrArray<MSDDATA> *popMSDDATA)
{
	POSITION rlPos;
	for (rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		MSDDATA *prlMsd;
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlMsd);
		if(prlMsd->Sdgu == lpSdgu)
		{
			popMSDDATA->Add(prlMsd);
		}
	}
}



//**********************************************************************************
// 
//**********************************************************************************

void CedaMsdData::DeleteBySdgu(long lpSdgUrno)
{
	for (long l = omData.GetSize() -1; l > -1; l--)
	{
		if (omData[l].Sdgu == lpSdgUrno)
		{
			DeleteMsdInternal(&omData[l]);
		}
	}
}

//**********************************************************************************
// 
//**********************************************************************************

bool CedaMsdData::UpdateMsdInternal(MSDDATA *prpMsd, bool bpWithDDX /*= false*/)
{
	bool blRet = true;
	if (prpMsd!=NULL)
	{
		MSDDATA* prlMsd = GetMsdByUrno(prpMsd->Urno);
		if(prlMsd==NULL)
		{
			blRet = false;
		}
		else
		{
			*prlMsd = *prpMsd;
		}
	}
	else
	{
		blRet = false;
	}
    return blRet;

/*	if( bpWithDDX == true )
	{
		// ein Broadcast kann u.u. zum L�schen des MAs f�hren, dann knallts, wenn man
		// keine lokale Kopie des Datensatzes angelegt hat
		DRRDATA olDrrData;
		CopyDrr(&olDrrData,prpDrr);
		
		if(pomLastInternChangedDrr == prpDrr)
		{
			pomLastInternChangedDrr = &olDrrData;
		}
		
		ogDdx.DataChanged((void *)this,DRR_CHANGE,(void *)&olDrrData);
	}*/

}

//**********************************************************************************
// 
//**********************************************************************************

bool CedaMsdData::DeleteMsdInternal(MSDDATA *prpMsd, bool bpWithDDX /*= false*/)
{
	bool blRet = true;
	omUrnoMap.RemoveKey((void *)prpMsd->Urno);

	int ilMsdCount = omData.GetSize();
	for (int ilCountRecord = 0; ilCountRecord < ilMsdCount; ilCountRecord++)
	{
		if (omData[ilCountRecord].Urno == prpMsd->Urno)
		{
			omData.DeleteAt(ilCountRecord);
			if (bpWithDDX)
			{
				ogDdx.DataChanged((void *)this, MSD_DELETE, NULL);
			}
			break;
		}
	}

	return blRet;
}

//**********************************************************************************
// 
//**********************************************************************************
long CedaMsdData::GetUrnoFromSelectionString(CString opSelection)
{
	long llUrno;

	if (opSelection.Find('\'') != -1)
	{
		llUrno = GetUrnoFromSelection(opSelection);
	}
	else
	{
		int ilFirst;
		int ilLast;

		if(strstr(opSelection,"'") != NULL)
		{
			ilFirst = opSelection.Find("'") + 1;
			ilLast	= opSelection.Find("'",ilFirst);
		}
		else
		{
			ilFirst = opSelection.Find("=") + 1;
			ilLast	= opSelection.GetLength();
		}

		llUrno = atol(opSelection.Mid(ilFirst,ilLast-ilFirst));
	}

	return llUrno;
}

//**********************************************************************************
// 
//**********************************************************************************
void CedaMsdData::CopyMsdValues(MSDDATA* popMsdDataSource, MSDDATA* popMsdDataTarget)
{
	CCS_TRY;
	strcpy(popMsdDataTarget->Bsdc,popMsdDataSource->Bsdc);
	strcpy(popMsdDataTarget->Days,popMsdDataSource->Days);
	strcpy(popMsdDataTarget->Dnam,popMsdDataSource->Dnam);
	strcpy(popMsdDataTarget->Rema,popMsdDataSource->Rema);
	strcpy(popMsdDataTarget->Type,popMsdDataSource->Type);
	strcpy(popMsdDataTarget->Usec,popMsdDataSource->Usec);
	strcpy(popMsdDataTarget->Useu,popMsdDataSource->Useu);
	strcpy(popMsdDataTarget->Fctc,popMsdDataSource->Fctc);
	popMsdDataTarget->Sdu1 = popMsdDataSource->Sdu1;
	popMsdDataTarget->Urno = popMsdDataSource->Urno;
	popMsdDataTarget->Bkf1 = popMsdDataSource->Bkf1;
	popMsdDataTarget->Bkt1 = popMsdDataSource->Bkt1;
	popMsdDataTarget->Sbgi = popMsdDataSource->Sbgi;
	popMsdDataTarget->Seni = popMsdDataSource->Seni;
	popMsdDataTarget->Cdat = popMsdDataSource->Cdat;
	popMsdDataTarget->Esbg = popMsdDataSource->Esbg;
	popMsdDataTarget->Lsen = popMsdDataSource->Lsen;
	popMsdDataTarget->Lstu = popMsdDataSource->Lstu;
	popMsdDataTarget->Brkf = popMsdDataSource->Brkf;
	popMsdDataTarget->Brkt = popMsdDataSource->Brkt;
	popMsdDataTarget->Bsdu = popMsdDataSource->Bsdu;
	popMsdDataTarget->Sdgu = popMsdDataSource->Sdgu;

	CCS_CATCH_ALL;
}