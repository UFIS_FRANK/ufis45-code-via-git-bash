// CedaTeaData.cpp
 
#include <stdafx.h>


void ProcessTeaCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

CedaTeaData ogTeaData;

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaTeaData::CedaTeaData() : CedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for TEADataStruct
	BEGIN_CEDARECINFO(TEADATA,TEADataRecInfo)
		FIELD_DATE(Cdat,"CDAT")
		FIELD_CHAR_TRIM(Fgmc,"FGMC")
		FIELD_CHAR_TRIM(Fgmn,"FGMN")
		FIELD_DATE(Lstu,"LSTU")
		FIELD_CHAR_TRIM(Prfl,"PRFL")
		FIELD_CHAR_TRIM(Rema,"REMA")
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Usec,"USEC")
		FIELD_CHAR_TRIM(Useu,"USEU")

	END_CEDARECINFO //(TEADataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(TEADataRecInfo)/sizeof(TEADataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&TEADataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"TEA");
	strcat(pcmTableName,pcgTableExt);
	strcpy(pcmListOfFields,"CDAT,FGMC,FGMN,LSTU,PRFL,REMA,URNO,USEC,USEU");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaTeaData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaTeaData::Register(void)
{
	DdxRegister((void *)this,BC_TEA_CHANGE,	"TEADATA", "Tea-changed",	ProcessTeaCf);
	DdxRegister((void *)this,BC_TEA_NEW,	"TEADATA", "Tea-new",		ProcessTeaCf);
	DdxRegister((void *)this,BC_TEA_DELETE,	"TEADATA", "Tea-deleted",	ProcessTeaCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaTeaData::~CedaTeaData(void)
{
	TRACE("CedaTeaData::~CedaTeaData called\n");
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaTeaData::ClearAll(bool bpWithRegistration)
{
	TRACE("CedaTeaData::ClearAll called\n");
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaTeaData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(pspWhere == NULL)
	{	
		ilRc = CedaAction2("RT");
	}
	else
	{
		ilRc = CedaAction2("RT", pspWhere);
	}
	if (ilRc != true)
	{
		TRACE("Read-Tea: Ceda-Error %d \n",ilRc);
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
	{
		TEADATA *prlTea = new TEADATA;
		if ((ilRc = GetFirstBufferRecord2(prlTea)) == true)
		{
			omData.Add(prlTea);//Update omData
			omUrnoMap.SetAt((void *)prlTea->Urno,prlTea);
#ifdef TRACE_FULL_DATA
			// Datensatz OK, loggen if FULL
			ogRosteringLogText.Format(" read %8.8lu %s OK:  ", ilCountRecord, pcmTableName);
			GetDataFormatted(ogRosteringLogText, prlTea);
			WriteLogFull("");
#endif TRACE_FULL_DATA
		}
		else
		{
			delete prlTea;
		}
	}
	TRACE("Read-Tea: %d gelesen\n",ilCountRecord-1);
        		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  read %d records from table %s",ilCountRecord-1, pcmTableName);
		WriteInRosteringLog();
	}

	return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaTeaData::Insert(TEADATA *prpTea)
{
	prpTea->IsChanged = DATA_NEW;
	if(Save(prpTea) == false) return false; //Update Database
	InsertInternal(prpTea);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaTeaData::InsertInternal(TEADATA *prpTea)
{
	ogDdx.DataChanged((void *)this, TEA_NEW,(void *)prpTea ); //Update Viewer
	omData.Add(prpTea);//Update omData
	omUrnoMap.SetAt((void *)prpTea->Urno,prpTea);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaTeaData::Delete(long lpUrno)
{
	TEADATA *prlTea = GetTeaByUrno(lpUrno);
	if (prlTea != NULL)
	{
		prlTea->IsChanged = DATA_DELETED;
		if(Save(prlTea) == false) return false; //Update Database
		DeleteInternal(prlTea);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaTeaData::DeleteInternal(TEADATA *prpTea)
{
	ogDdx.DataChanged((void *)this,TEA_DELETE,(void *)prpTea); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpTea->Urno);
	int ilTeaCount = omData.GetSize();
	for (int ilCountRecord = 0; ilCountRecord < ilTeaCount; ilCountRecord++)
	{
		if (omData[ilCountRecord].Urno == prpTea->Urno)
		{
			omData.DeleteAt(ilCountRecord);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaTeaData::Update(TEADATA *prpTea)
{
	if (GetTeaByUrno(prpTea->Urno) != NULL)
	{
		if (prpTea->IsChanged == DATA_UNCHANGED)
		{
			prpTea->IsChanged = DATA_CHANGED;
		}
		if(Save(prpTea) == false) return false; //Update Database
		UpdateInternal(prpTea);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaTeaData::UpdateInternal(TEADATA *prpTea)
{
	TEADATA *prlTea = GetTeaByUrno(prpTea->Urno);
	if (prlTea != NULL)
	{
		*prlTea = *prpTea; //Update omData
		ogDdx.DataChanged((void *)this,TEA_CHANGE,(void *)prlTea); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

TEADATA *CedaTeaData::GetTeaByUrno(long lpUrno)
{
	TEADATA  *prlTea;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlTea) == TRUE)
	{
		return prlTea;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaTeaData::ReadSpecialData(CCSPtrArray<TEADATA> *popTea,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction2("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popTea != NULL)
	{
		for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
		{
			TEADATA *prpTea = new TEADATA;
			if ((ilRc = GetBufferRecord2(ilCountRecord,prpTea,CString(pclFieldList))) == true)
			{
				popTea->Add(prpTea);
			}
			else
			{
				delete prpTea;
			}
		}
		if(popTea->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaTeaData::Save(TEADATA *prpTea)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpTea->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpTea->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpTea);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpTea->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpTea->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpTea);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpTea->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpTea->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
	TRACE("Tea-IRT/URT/DRT: Ceda-Return %d \n",ilRc);
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessTeaCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogTeaData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaTeaData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlTeaData;
	prlTeaData = (struct BcStruct *) vpDataPointer;
	TEADATA *prlTea;
	
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("ProcessBC:\n  Cmd <%s>\n  Tbl <%s>\n  Twe <%s>\n  Sel <%s>\n  Fld <%s>\n  Dat <%s>\n  Bcn <%s>",prlTeaData->Cmd, prlTeaData->Object, prlTeaData->Twe, prlTeaData->Selection, prlTeaData->Fields, prlTeaData->Data,prlTeaData->BcNum);
		WriteInRosteringLog();
	}
	
	switch(ipDDXType)
	{
	default:
		break;
	case BC_TEA_CHANGE:
		prlTea = GetTeaByUrno(GetUrnoFromSelection(prlTeaData->Selection));
		if(prlTea != NULL)
		{
			GetRecordFromItemList(prlTea,prlTeaData->Fields,prlTeaData->Data);
			UpdateInternal(prlTea);
			break;
		}
	case BC_TEA_NEW:
		prlTea = new TEADATA;
		GetRecordFromItemList(prlTea,prlTeaData->Fields,prlTeaData->Data);
		InsertInternal(prlTea);
		break;
	case BC_TEA_DELETE:
		{
			long llUrno;
			CString olSelection = (CString)prlTeaData->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlTeaData->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			prlTea = GetTeaByUrno(llUrno);
			if (prlTea != NULL)
			{
				DeleteInternal(prlTea);
			}
		}
		break;
	}
}

//---------------------------------------------------------------------------------------------------------
