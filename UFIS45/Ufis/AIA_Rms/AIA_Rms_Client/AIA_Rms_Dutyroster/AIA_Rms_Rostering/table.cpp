// table.cpp - Displays data as a list of textlines
//
// Notes: I decided to derive the class CTable from the standard MFC CWnd.
// This window will draw the upper of its client-area as table header bar, and
// its lower will be the standard MFC CListBox.
//
// Most routines in this class will assume that the table window is exist if
// the derived data member "m_hWnd" is not NULL.
//
//
// Written by:
// Damkerng Thammathakerngkit   April 16, 1996
//
// Modification History:
// May 1, 1996      Damkerng    Add new member functions required to be used in PrePlanTable.
//                              These member functions are ResetContent(), InsertTextLine().
//
// May 1, 1996      Damkerng    Change omListBox data member to be CTableListBox which is
//                              derived from CListBox. This is required because PrePlanTable
//                              need a WM_TABLETEXTSELECT message.
//
// May 1, 1996      Damkerng    Change comma-separated list parameters "opText" passed to
//                              AddTextLine(), InsertTextLine(), and ChangeTextLine() to be a
//                              vertbar-separated list parameters. This is necessary because
//                              in the spec, we need to display "LNAM, FNAM" which surely
//                              have a comma in the field content. To avoid mistaken field,
//                              I change the separator to be the vertical bar. This change
//                              effects member functions GetTextLineValue(), GetTextFieldValue()
//                              and the DrawItem() routine.
//
// May 2, 1996      Damkerng    Fix bug that the CTable did not display the table content
//                              (ridges and separator lines) if there is no data in the table.
//                              The omListBox changed to be "pomListBox".
//
// May 2, 1996      Damkerng    Change parameters passed into SetHeaderFields() and
//                              SetFormatList() to be consistent with parameter "opText"
//                              in AddTextLine(), InsertTextLine(), and ChangeTextLine().
//
// May 3, 1996      Damkerng    Add parameter "pvpData" to methods AddTextLine(),
//                              InsertTextLine(), and ChangeTextLine().

#include <stdafx.h>

// default font (in stock object) used when no font specified
#define DEFAULT_FONT    ANSI_FIXED_FONT

// spacing between the font and the dotted-line
#define EXTRASPACE	2

/////////////////////////////////////////////////////////////////////////////
// CTableListBox

// In order to implement the CTable correctly, we need CTableListBox to be subclassed
// from the MFC's CListBox.
//
// The first reason is we need to handle mouse events. For example, when the user
// double-click in the table content, CTableListBox will detect the WM_LBUTTONDBLCLK
// message. It will generate a message WM_TABLETEXTSELECT to the surrounding window,
// in turn.
//
// The second reason is we need to update the table content correctly. This need
// CTableListBox as a separated class since, we need to override the OnPaint() member
// functions to handle the WM_PAINT message. When the CTableListBox, which is an
// owner-draw control receive this message, it need to redraw the entire table content
// as an empty table. By this requirement, I need to declare the CTableListBox as
//"friend" of the CTable.
//
class CTableListBox: public CListBox
{
public:
    // Specify the CTable object which this list box associated with.
    // Since CTableListBox is a separated object from CTable, it need this data member.
    CTable *pomTableWindow;

	// For remember the clip-box which detected in OnEraseBkgnd and used in OnPaint
	CRect omClipBox;

	// Return the item ID of the list box based on the given "point"
	int GetItemFromPoint(CPoint point);

	// Update the member "imCurrentColumn" of the parent CTable
	void DetectCurrentColumn(CPoint point);

protected:
    //{{AFX_MSG(CTableListBox)
    afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
    afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
    afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnDestroy();
	afx_msg void OnSelchange();
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};

BEGIN_MESSAGE_MAP(CTableListBox, CListBox)
    //{{AFX_MSG_MAP(CTableListBox)
    ON_WM_LBUTTONDBLCLK()
    ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_MESSAGE(WM_DROP, OnDrop)  
    ON_WM_PAINT()
	ON_WM_ERASEBKGND()
	ON_WM_DESTROY()
	ON_CONTROL_REFLECT(LBN_SELCHANGE, OnSelchange)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CTableListBox::OnDestroy()
{
	CListBox::OnDestroy();
}

// GetItemFromPoint: Return the item ID of the list box based on the given "point"
int CTableListBox::GetItemFromPoint(CPoint point)
{
    CRect rcItem;

	BOOL blResult;
	/*UINT nItem = */ItemFromPoint(point,blResult);
	for (int itemID = GetTopIndex(); itemID < GetCount(); itemID++)
    {
        GetItemRect(itemID, &rcItem);
        if (rcItem.PtInRect(point))
            return itemID;
    }
    return -1;
}

// DetectCurrentColumn: Update the member "imCurrentColumn" of the parent CTable
void CTableListBox::DetectCurrentColumn(CPoint point)
{
	unsigned int x = point.x + pomTableWindow->imTableContentOffset;
	for (int ilLc = 0; ilLc < pomTableWindow->omLBorder.GetSize(); ilLc++)
	{
		if (pomTableWindow->omLBorder[ilLc] < x && x < pomTableWindow->omRBorder[ilLc])
		{
			pomTableWindow->imCurrentColumn = ilLc;
			return;
		}
	}

	// the user click on outside
	pomTableWindow->imCurrentColumn = -1;
}

// OnLButtonDblClk: Notify the parent window of CTable "There's a double-click down here".
void CTableListBox::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	DetectCurrentColumn(point);

    int itemID = GetCaretIndex();
	CWnd *pWnd = pomTableWindow->pomParentWindow;	// surrounding window
    pWnd->SendMessage(WM_TABLE_LBUTTONDBLCLK, itemID, (LPARAM)this);
}

// OnLButtonDblClk: Notify the parent window of CTable "There's a dragging/click down here".
void CTableListBox::OnLButtonDown(UINT nFlags, CPoint point)
{
	DetectCurrentColumn(point);

	int itemID = GetItemFromPoint(point);
	CWnd *pWnd = pomTableWindow->pomParentWindow;	// surrounding window

	if (pomTableWindow->imSelectMode & (LBS_MULTIPLESEL | LBS_EXTENDEDSEL))	// multiple selection?
	{
		if (itemID != LB_ERR && pomTableWindow->omDragEnabled[itemID] && GetSel(itemID))
		{												// user start dragging?
			pWnd->SendMessage(WM_TABLE_DRAGBEGIN, itemID, (LPARAM)this);
			return;	// don't call the default action of CListBox if some dragging performed
		}
	}
	else
	{
		if (itemID != LB_ERR && pomTableWindow->omDragEnabled[itemID])
		{
			SetCurSel(itemID);
			pWnd->SendMessage(WM_TABLE_DRAGBEGIN, itemID, (LPARAM)this);
			return;	// don't call the default action of CListBox if some dragging performed
		}
	}

	pWnd->SendMessage(WM_TABLE_LBUTTONDOWN, itemID, (LPARAM)this);
    CListBox::OnLButtonDown(nFlags, point);
}

void CTableListBox::OnRButtonDown(UINT nFlags, CPoint point)
{
	DetectCurrentColumn(point);

	int itemID = GetItemFromPoint(point);
	CWnd *pWnd = pomTableWindow->pomParentWindow;	// surrounding window
	LPARAM llLParam = MAKELPARAM(point.x, point.y);
    pWnd->SendMessage(WM_TABLE_RBUTTONDOWN, itemID, llLParam);
}

LONG CTableListBox::OnDragOver(UINT wParam, LONG lParam)
{
	long llRc;

	CPoint olPoint;
    ::GetCursorPos(&olPoint);
	DetectCurrentColumn(olPoint);

	CWnd *pWnd = pomTableWindow->pomParentWindow;	// surrounding window
    llRc = pWnd->SendMessage(WM_DRAGOVER, wParam, lParam);
	return llRc;
}

LONG CTableListBox::OnDrop(UINT wParam, LONG lParam)
{
	long llRc;

	CPoint olPoint;
    ::GetCursorPos(&olPoint);
	DetectCurrentColumn(olPoint);

	CWnd *pWnd = pomTableWindow->pomParentWindow;	// surrounding window
    llRc = pWnd->SendMessage(WM_DROP, wParam, lParam);
	return llRc;
}

// CTableListBox::OnEraseBkgnd: Remember the clip-box for the paint routine
BOOL CTableListBox::OnEraseBkgnd(CDC* pDC) 
{
	pDC->GetClipBox(omClipBox);
	return TRUE;
}

// CTableListBox::OnPaint Redraw the entire table content if it's empty.
void CTableListBox::OnPaint()
{
    CListBox::OnPaint();

    if (GetCount() == 0)	// table is empty?
	{
        // reset table content beginning offset (in pixels)
        pomTableWindow->imTableContentOffset = 0;

        // calculate size and position of each line
        CRect rect;
        GetClientRect(&rect);
        int bottom = rect.bottom;

        // draw the empty entire table content line-by-line
        CClientDC dc(this);
		int height = pomTableWindow->imTextFontHeight + EXTRASPACE;
        while (rect.top <= bottom)
        {
            rect.bottom = min(rect.top + height, bottom);
            pomTableWindow->DrawItem(dc, 0, rect, FALSE, FALSE, FALSE);
            rect.top += height;
        }
	}
	// Want to know that if the painting region is intersect with one of the string.
	// This will fix bug which table not update the lower region when there is no data there.
	else if (GetCount() > 0)
	{
		CRect rcItem;
		GetItemRect(GetCount()-1, rcItem);
		if (rcItem.bottom < omClipBox.top)	// need some fixing?
		{
			InvalidateRect(NULL);
		}
    }
};

/////////////////////////////////////////////////////////////////////////////
// CTable

BEGIN_MESSAGE_MAP(CTable, CWnd)
    //{{AFX_MSG_MAP(CTable)
    ON_WM_PAINT()
	ON_WM_DESTROY()
    ON_WM_MEASUREITEM()
    ON_WM_DRAWITEM()
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_MESSAGE(WM_DROP, OnDrop)  
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CTable::SetSelectMode(int ipSelectMode)
{
	imSelectMode = ipSelectMode;
}

// create an empty CTable object
CTable::CTable()
{
    pomListBox = new CTableListBox;
    
    // indicates no initialization yet
    bmInited = FALSE;
	
	imSelectMode = LBS_MULTIPLESEL | LBS_EXTENDEDSEL;

    // initialize data for searching routine
    imNextLineToSearch = 0;
    omLastSearchText = "";

    // prepare helper drawing objects
    FacePen.CreatePen(PS_SOLID, 1, ::GetSysColor(COLOR_BTNFACE));
    ShadowPen.CreatePen(PS_SOLID, 1, ::GetSysColor(COLOR_BTNSHADOW));
    WhitePen.CreatePen(PS_SOLID, 1, ::GetSysColor(COLOR_WINDOW));
    BlackPen.CreatePen(PS_SOLID, 1, ::GetSysColor(COLOR_WINDOWFRAME));

    FaceBrush.CreateSolidBrush(::GetSysColor(COLOR_BTNFACE));

    // prepare the horizontal separator brush pattern
    CBitmap Bitmap;
    static WORD separator[] = { 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa };

    Bitmap.CreateBitmap(8, 8, 1, 1, separator);
    SeparatorBrush.CreatePatternBrush(&Bitmap);
    Bitmap.DeleteObject();
}

// create a CTable object and initialize data members
CTable::CTable(CWnd *popParentWindow,
    int ipXStart, int ipXEnd, int ipYStart, int ipYEnd,
    COLORREF lpTextColor, COLORREF lpTextBkColor,
    COLORREF lpHighlightColor, COLORREF lpHighlightBkColor,
    CFont *popTextFont, CFont *popHeaderFont)
{
    CTable();

    SetTableData(popParentWindow,
        ipXStart, ipXEnd, ipYStart, ipYEnd,
        lpTextColor, lpTextBkColor,
        lpHighlightColor, lpHighlightBkColor,
        popTextFont, popHeaderFont);
}
 
 // destructor for a CTable object
CTable::~CTable()
{
	pomListBox->DestroyWindow();
    delete pomListBox;

    // destroy helper drawing objects of this instances
    FacePen.DeleteObject();
    ShadowPen.DeleteObject();
    WhitePen.DeleteObject();
    BlackPen.DeleteObject();
    FaceBrush.DeleteObject();
    SeparatorBrush.DeleteObject();
	DestroyWindow();
}

void CTable::OnDestroy()
{
	CWnd::OnDestroy();
}
// change parameters of the created CTable instance
bool CTable::SetTableData(CWnd *popParentWindow,
    int ipXStart, int ipXEnd, int ipYStart, int ipYEnd,
    COLORREF lpTextColor, COLORREF lpTextBkColor,
    COLORREF lpHighlightColor, COLORREF lpHighlightBkColor,
    CFont *popTextFont, CFont *popHeaderFont)
{
    // destroy the window if parent window changed
    if (popParentWindow != pomParentWindow)
        DestroyWindow();    // this works even if m_hWnd == NULL.

    // initialize all data members
    pomParentWindow = popParentWindow;
    imXStart = min(ipXStart, ipXEnd);   // ensure positive height and width
    imXEnd = max(ipXStart, ipXEnd);
    imYStart = min(ipYStart, ipYEnd);
    imYEnd = max(ipYStart, ipYEnd);
    lmTextColor = lpTextColor;
    lmTextBkColor = lpTextBkColor;
    lmHighlightColor = lpHighlightColor;
    lmHighlightBkColor = lpHighlightBkColor;
    pomTextFont = popTextFont;
    pomHeaderFont = popHeaderFont;

    // indicate that window information is initialized
    bmInited = TRUE;

    // update object positions.
    // Warning: CalculateWidgets() have to be done before SetPosition() in case if header
    // font size changed.
    //
    CalculateWidgets();
    SetPosition(imXStart, imXEnd, imYStart, imYEnd);
    if (m_hWnd != NULL)
    {
		pomListBox->SetItemHeight(0, imTextFontHeight + EXTRASPACE);
        pomListBox->SetHorizontalExtent(imHorizontalExtent);
		imItemHeight = imTextFontHeight + EXTRASPACE;
		//pomListBox->SetScrollRange(SB_HORZ,0,imHorizontalExtent); 
    }

    ////RC(true);
    return true;
}

// display the table (also create window if necessary)
bool CTable::DisplayTable()
{
    if (!bmInited)  // uninitialized?
    {
        //RC(false);
        return false;
    }

    if (m_hWnd == NULL) // window was not created yet?
    {
        // create the table window
        //DWORD lWndStyle = WS_CHILD | WS_BORDER | WS_VISIBLE;
		DWORD lWndStyle = WS_CHILD | WS_VISIBLE;
        CRect rectWnd(imXStart, imYStart, imXEnd-imXStart+1, imYEnd-imYStart+1);

        if (Create(NULL, NULL, lWndStyle, rectWnd, pomParentWindow, 1) == 0)
        {
            //RC(false);
            return false;
        }

        // create header bar (CRect only, not real window) for OnPaint routine
        GetClientRect(&omHeaderBarRect);
        omHeaderBarRect.bottom = omHeaderBarRect.top + imHeaderFontHeight + 7;
        imTableContentOffset = 0;

        // let the list box know which window it must send a message to
        pomListBox->pomTableWindow = this;

        // create an owner-drawn list box control
        CRect rect;
        DWORD lListBoxStyle = WS_HSCROLL | WS_VSCROLL | WS_VISIBLE | WS_BORDER
            | imSelectMode | LBS_NOINTEGRALHEIGHT | LBS_OWNERDRAWFIXED | LBS_NOTIFY ;

        GetClientRect(&rect);
		rect.OffsetRect(-1, -1);
        rect.top += imHeaderFontHeight + 7;
        if (pomListBox->Create(lListBoxStyle, rect, this, 1) == 0)
        {
            //RC(false);
            return false;
        }

        // Register Target for Dragdrop.
        //pomListBox->RegisterDropTarget();

        // Fix Windows 3.x's bug on list box co-ordinate calculation.
        // Windows 3.x will inflate window position given when created by -1,-1.
        // However, this problem could be fixed by artfully moving window just one time.
        //
		rect.bottom--, rect.right--;
        pomListBox->MoveWindow(&rect, FALSE);

        // insert data from the omData into the owner-drawn list box control
        for (int i = omData.GetSize(); i > 0; i--)
            pomListBox->AddString("");

        // set up the list box horizontal horizontal controls
        pomListBox->SetHorizontalExtent(imHorizontalExtent);
  		//pomListBox->SetScrollRange(SB_HORZ,0,imHorizontalExtent); 
  }

    // force itself to refresh the window
    InvalidateRect(NULL, FALSE);

    //RC(true);
    return true;
}

// reposition the table window
bool CTable::SetPosition(int ipXStart, int ipXEnd, int ipYStart, int ipYEnd)
{
    BOOL blRepaint = TRUE;

    if (abs(ipXEnd-ipXStart+1) == imXEnd-imXStart+1 &&
        abs(ipYEnd-ipYStart+1) == imYEnd-imYStart+1)
        blRepaint = FALSE;

    imXStart = min(ipXStart, ipXEnd);   // ensure positive height and width
    imXEnd = max(ipXStart, ipXEnd);
    imYStart = min(ipYStart, ipYEnd);
    imYEnd = max(ipYStart, ipYEnd);

    // recalculate object positions
    if (blRepaint)
        CalculateWidgets();

    // reposition the window, including the owner-drawn list box controls
    if (m_hWnd != NULL)
    {
        CRect rect;
        MoveWindow(imXStart, imYStart, imXEnd-imXStart+1, imYEnd-imYStart+1, blRepaint);
        GetClientRect(&rect);
		rect.OffsetRect(-1, -1);
        rect.top += imHeaderFontHeight + 7;
        pomListBox->MoveWindow(&rect, blRepaint);
    }

    //RC(true);
    return true;
}

CListBox *CTable::GetCTableListBox()
{
	return pomListBox;
}

int CTable::GetLinenoFromPoint(CPoint opPoint)
{
	pomListBox->ScreenToClient(&opPoint);    
	return pomListBox->GetItemFromPoint(opPoint);
}

bool CTable::SetFieldNames(CString opFieldNames)
{
    omFieldNames.RemoveAll();

    int i = 0;
    while (i < opFieldNames.GetLength())
    {
        for (int n = i; n < opFieldNames.GetLength() && opFieldNames[n] != ','; n++)
            ;
        omFieldNames.Add(opFieldNames.Mid(i, n-i));
        i = n + 1;
    }

    //RC(true);
    return true;
}

bool CTable::SetHeaderFields(CString opHeaderFields)
{
    omHeaderFields.RemoveAll();

    int i = 0;
    while (i < opHeaderFields.GetLength())
    {
        for (int n = i; n < opHeaderFields.GetLength() && opHeaderFields[n] != '|'; n++)
            ;
        omHeaderFields.Add(opHeaderFields.Mid(i, n-i));
        i = n + 1;
    }
    CalculateWidgets();

    //RC(true);
    return true;
}

bool CTable::SetFormatList(CString opFormatList)
{
    omFormatList.RemoveAll();

    int i = 0;
    while (i < opFormatList.GetLength())
    {
        for (int n = i; n < opFormatList.GetLength() && opFormatList[n] != '|'; n++)
            ;
        omFormatList.Add(opFormatList.Mid(i, n-i));
        i = n + 1;
    }
    CalculateWidgets();

    //RC(true);
    return true;
}

bool CTable::SetTypeList(CString opTypeList)
{
    omTypeList.RemoveAll();

    int i = 0;
    while (i < opTypeList.GetLength())
    {
        for (int n = i; n < opTypeList.GetLength() && opTypeList[n] != '|'; n++)
            ;
        omTypeList.Add(opTypeList.Mid(i, n-i));
        i = n + 1;
    }

    //RC(true);
    return true;
}

bool CTable::ResetContent()
{
    omData.RemoveAll();
	omDataPtr.RemoveAll();
	omColor.RemoveAll();
	omBkColor.RemoveAll();
	omDragEnabled.RemoveAll();
    omSeparatorType.RemoveAll();
	omFontPtr.RemoveAll();
    if (m_hWnd != NULL)     // list box is exist?
        pomListBox->ResetContent();

    //RC(true);
    return true;
}

int CTable::GetCurrentLine()
{
    return (m_hWnd != NULL)? pomListBox->GetCaretIndex() : -1 ;
}

bool CTable::AddTextLine(CString opText, void *pvpData)
{
    omData.Add(opText);
    omDataPtr.Add(pvpData);
	omColor.Add(lmTextColor);
	omBkColor.Add(lmTextBkColor);
	omDragEnabled.Add(TRUE);
	omSeparatorType.Add(ST_NORMAL);
	omFontPtr.Add(pomTextFont);
    if (m_hWnd != NULL)     // list box is exist?
    {
        if (pomListBox->AddString("") == LB_ERR)
        {
            //RC(false);
            return false;
        }
    }
                   
    //RC(true);
    return true;
}

bool CTable::InsertTextLine(int ipLineNo, CString opText, void *pvpData)
{
    if ((ipLineNo == -1) || (ipLineNo >= omData.GetSize()))    // add string to the end of the list?
        return AddTextLine(opText,pvpData);


    omData.InsertAt(ipLineNo, opText);
    omDataPtr.InsertAt(ipLineNo, pvpData);
	omColor.InsertAt(ipLineNo, lmTextColor);
	omBkColor.InsertAt(ipLineNo, lmTextBkColor);
	omDragEnabled.InsertAt(ipLineNo, TRUE);
	omSeparatorType.InsertAt(ipLineNo, (WORD)ST_NORMAL);
	omFontPtr.InsertAt(ipLineNo, pomTextFont);
    if (m_hWnd != NULL)     // list box is exist?
    {
		if (pomListBox->InsertString(ipLineNo, "") == LB_ERR)
		{
			CRect rcItem;
			if (pomListBox->GetItemRect(ipLineNo, rcItem) == LB_ERR 
				// ||  pomListBox->SetCaretIndex(ipLineNo) == LB_ERR
				)
			  {
					//RC(false);
					return false;
			  }
			pomListBox->InvalidateRect(rcItem);
			return false;
		}
    }

    //RC(true);
    return true;
}

bool CTable::ChangeTextLine(int ipLineNo, CString opText, void *pvpData)
{
    if (ipLineNo == -1 && m_hWnd != NULL)   // use default line number?
        ipLineNo = pomListBox->GetCaretIndex();

    if (!(0 <= ipLineNo && ipLineNo < omData.GetSize()))    // valid line number?
    {
        //RC(false);
        return false;
    }

    omData.SetAt(ipLineNo, opText);
    omDataPtr.SetAt(ipLineNo, pvpData);
    if (m_hWnd != NULL)     // list box is exist?
    {
		CRect rcItem;
		if (pomListBox->GetItemRect(ipLineNo, rcItem) == LB_ERR 
			// ||  pomListBox->SetCaretIndex(ipLineNo) == LB_ERR
			)
        {
            //RC(false);
            return false;
        }
		pomListBox->InvalidateRect(rcItem);
    }

    //RC(true);
    return true;
}

bool CTable::DeleteTextLine(int ipLineNo)
{
    if (ipLineNo == -1 && m_hWnd != NULL)   // use default line number?
        ipLineNo = pomListBox->GetCaretIndex();

    if (!(0 <= ipLineNo && ipLineNo < omData.GetSize()))    // valid line number?
    {
        //RC(false);
        return false;
    }

    omData.RemoveAt(ipLineNo);
	omDataPtr.RemoveAt(ipLineNo);
	omColor.RemoveAt(ipLineNo);
	omBkColor.RemoveAt(ipLineNo);
	omDragEnabled.RemoveAt(ipLineNo);
	omSeparatorType.RemoveAt(ipLineNo);
	omFontPtr.RemoveAt(ipLineNo);
    if (m_hWnd != NULL)     // list box is exist?
    {
        if (pomListBox->DeleteString(ipLineNo) == LB_ERR)
        {
            //RC(false);
            return false;
        }
    }

    //RC(true);
    return true;
}

bool CTable::SetTextLineColor(int ipLineNo, COLORREF lpTextColor, COLORREF lpTextBkColor)
{
    if (ipLineNo == -1 && m_hWnd != NULL)   // use default line number?
        ipLineNo = pomListBox->GetCaretIndex();

    if (!(0 <= ipLineNo && ipLineNo < omData.GetSize()))    // valid line number?
    {
        //RC(false);
        return false;
    }

    // not implemented yet, waiting to be sure about the color
	if (lpTextColor != -1L)
		omColor[ipLineNo] = lpTextColor;
	if (lpTextBkColor != -1L)
		omBkColor[ipLineNo] = lpTextBkColor;

    //RC(true);
    return true;
}

bool CTable::SetTextLineSeparator(int ipLineNo, int ipSeparatorType)
{
    if (ipLineNo == -1 && m_hWnd != NULL)   // use default line number?
        ipLineNo = pomListBox->GetCaretIndex();

    if (!(0 <= ipLineNo && ipLineNo < omData.GetSize()))    // valid line number?
    {
        //RC(false);
        return false;
    }

	omSeparatorType[ipLineNo] = (unsigned short)ipSeparatorType;

    //RC(true);
    return true;
}

bool CTable::SetTextLineFont(int ipLineNo, CFont *popFont)
{
    if (ipLineNo == -1 && m_hWnd != NULL)   // use default line number?
        ipLineNo = pomListBox->GetCaretIndex();

    if (!(0 <= ipLineNo && ipLineNo < omData.GetSize()))    // valid line number?
    {
        //RC(false);
        return false;
    }

	omFontPtr[ipLineNo] = popFont;

    //RC(true);
    return true;
}

bool CTable::SetTextLineDragEnable(int ipLineNo, BOOL bpEnable)
{
    if (ipLineNo == -1 && m_hWnd != NULL)   // use default line number?
        ipLineNo = pomListBox->GetCaretIndex();

	omDragEnabled[ipLineNo] = (unsigned short)bpEnable;

	//RC(true);
	return true;
}

bool CTable::GetTextLineValue(int ipLineNo, CString &opText)
{
    if (ipLineNo == -1 && m_hWnd != NULL)   // use default line number?
        ipLineNo = pomListBox->GetCaretIndex();

    if (!(0 <= ipLineNo && ipLineNo < omData.GetSize()))    // valid line number?
    {
        //RC(false);
        return false;
    }

    opText = omData[ipLineNo];

    //RC(true);
    return true;
}

void *CTable::GetTextLineData(int ipLineNo)
{
    if (ipLineNo == -1 && m_hWnd != NULL)   // use default line number?
        ipLineNo = pomListBox->GetCaretIndex();

    if (!(0 <= ipLineNo && ipLineNo < omData.GetSize()))    // valid line number?
        return NULL;

    return omDataPtr[ipLineNo];
}

BOOL CTable::GetTextLineDragEnable(int ipLineNo)
{
    if (ipLineNo == -1 && m_hWnd != NULL)   // use default line number?
        ipLineNo = pomListBox->GetCaretIndex();

    if (!(0 <= ipLineNo && ipLineNo < omData.GetSize()))    // valid line number?
        return FALSE;

	return omDragEnabled[ipLineNo];
}

bool CTable::GetTextFieldValue(int ipLineNo, int ipFieldNo, CString &opText)
{
    CString s;

    // get the entire line, and checking for errors
    if (GetTextLineValue(ipLineNo, s) == false)
    {
        //RC(false);
        return false;
    }

    // skip over the leading fields
    int n = s.GetLength();
    for (int i = 0, fieldno = ipFieldNo; i < n && fieldno > 0; i++)
        if (s[i] == '|')
            fieldno--;
    if (fieldno != 0)
    {
        //RC(false);
        return false;
    }

    // extract text field value
    for (int pos = i; pos < n && s[pos] != '|'; pos++)
            ;
    opText = s.Mid(i, pos-i);

    //RC(true);
    return true;
}

bool CTable::GetTextFieldValue(int ipLineNo, CString opFieldName, CString &opText)
{
    // searching for the given "omFieldName"
    for (int i = 0; i < omFieldNames.GetSize(); i++)
        if (opFieldName == omFieldNames[i])
            return GetTextFieldValue(ipLineNo, i, opText);

    //RC(false);
    return false;
}

int CTable::SearchTextLine(int ipFieldNo, CString opText)
{
    if (opText != omLastSearchText) // new search string?
    {
        imNextLineToSearch = 0;
        omLastSearchText = opText;  // remember text for next searching
    }

    // scanning the "omData" line-by-line
    for (int n = omData.GetSize(); imNextLineToSearch < n; imNextLineToSearch++)
    {
        CString field;

        if (GetTextFieldValue(imNextLineToSearch, ipFieldNo, field) == false)   // error?
            return -1;
        if (field == opText)    // found?
            return imNextLineToSearch++;
    }

    return -1;  // not found
}


/////////////////////////////////////////////////////////////////////////////
// CalculateWidgets:
//
// This private member function will recompute the correct font height, font width,
// and everything that need to display the table.
//
void CTable::CalculateWidgets()
{
    CDC dc;
    TEXTMETRIC tm;
    int ilTextFontWidth;

    if (!bmInited)
        return;     // cannot calculate anything if not initialized yet

    dc.CreateCompatibleDC(NULL);

    // calculate the text font height and width
    if (dc.SelectObject(pomTextFont) == NULL)   // it works with both NULL and invalid font
        dc.SelectStockObject(DEFAULT_FONT);
    dc.GetTextMetrics(&tm);
    ilTextFontWidth = tm.tmAveCharWidth;
    imTextFontHeight = tm.tmHeight + tm.tmExternalLeading;
	imItemHeight = imTextFontHeight + EXTRASPACE;

    // calculate the header font height and width
    if (dc.SelectObject(pomHeaderFont) == NULL) // it works with both NULL and invalid font
        dc.SelectStockObject(DEFAULT_FONT);
    dc.GetTextMetrics(&tm);
    imHeaderFontHeight = tm.tmHeight + tm.tmExternalLeading;

    // Calculate column width and positions.
    // Notes: expression "pos += width+5" means lframe, rframe, white, gray, and shadow
    // vertical bars (ridges separating columns).
    //
    omLBorder.RemoveAll();
    omRBorder.RemoveAll();
    int n = max(omHeaderFields.GetSize(), omFormatList.GetSize());
    for (int i = 0, pos = -1, width = 0; i < n; i++, pos += width+5)
    {
        int w1 = 0; // width calculated from the header bar
        int w2 = 0; // width calculated from format descriptors

        if (i < omHeaderFields.GetSize())
            w1 = dc.GetOutputTextExtent(omHeaderFields[i], omHeaderFields[i].GetLength()).cx;
        if (i < omFormatList.GetSize())
            w2 = atoi((const char *)omFormatList[i]) * ilTextFontWidth;
        width = max(w1+2, w2) + 3;  // extra +2 pixels for header text, +3 pixels for spacing

        omLBorder.Add(pos);
        omRBorder.Add(pos + width + 1);
    }

    dc.DeleteDC();

    // determine width of the list box (for hiding/displaying horizontal scroll bar)
    imHorizontalExtent = pos - 4;
    if (m_hWnd != NULL)
	{
        pomListBox->SetHorizontalExtent(imHorizontalExtent);
		//pomListBox->SetScrollRange(SB_HORZ,0,imHorizontalExtent); 
	}

    // Checking for the last column (must provide a filler column on the right).
    // We use the client-area width to allow calculation while the window was not created yet.
    //
    if (pos < (imXEnd - imXStart - 1))
    {
        omLBorder.Add(pos);
        omRBorder.Add(imXEnd - imXStart);
    }
}


/////////////////////////////////////////////////////////////////////////////
// Message Handlings

// OnPaint: redraw the header and the table content
afx_msg void CTable::OnPaint()
{
    // update header bar if necessary
    CRect rect, rectTest;
    GetUpdateRect(&rect);
	// Ge�ndert ADO 18.10.99
    //if (rectTest.IntersectRect(&rect, &omHeaderBarRect))    // "rect" overlaps the header bar?
        DrawHeaderBar();
    ValidateRect(NULL);

    // update the table content (which is an owner-drawn list box)
    MapWindowPoints(pomListBox, &rect);
    pomListBox->InvalidateRect(&rect, TRUE);
}

// OnMeasureItem: set up the height of each item in list box
afx_msg void CTable::OnMeasureItem(int, LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	lpMeasureItemStruct->itemHeight = imTextFontHeight + EXTRASPACE;
}

// OnDrawItem: draw each item of the owner-drawn list box
afx_msg void CTable::OnDrawItem(int, LPDRAWITEMSTRUCT lpDrawItemStruct)
{
    CDC dc;
    CRect rect;
    BOOL blToggleFocusOnly;

    dc.Attach(lpDrawItemStruct->hDC);

    // change the visible position if necessary (redraw the header bar too)
    if (-dc.GetWindowOrg().x != imTableContentOffset)
    {
        imTableContentOffset = -dc.GetWindowOrg().x;
        DrawHeaderBar();
    }

    // draw the given item 
    blToggleFocusOnly = !(lpDrawItemStruct->itemAction & (ODA_DRAWENTIRE | ODA_SELECT));
    DrawItem(dc, lpDrawItemStruct->itemID,
        lpDrawItemStruct->rcItem,
        blToggleFocusOnly,
        lpDrawItemStruct->itemState & ODS_SELECTED,
        lpDrawItemStruct->itemState & ODS_FOCUS);

    // fill blank items at the end of the table, if necessary
    if ((lpDrawItemStruct->itemID == (UINT)pomListBox->GetCount() - 1) &&   // last line?
        (lpDrawItemStruct->itemAction & ODA_DRAWENTIRE))
    {
        int bottom, height;
        int id = lpDrawItemStruct->itemID;

        // calculate size and position of each line
        pomListBox->GetClientRect(&rect);
        bottom = rect.bottom;
        rect = lpDrawItemStruct->rcItem;
        height = rect.bottom - rect.top;

        // draw lower empty part of table content line-by-line
        for (rect.top += height; rect.top <= bottom; rect.top += height)
        {
            rect.bottom = min(rect.top + height, bottom);
            DrawItem(dc, ++id, rect, FALSE, FALSE, FALSE);
        }
    }

    dc.Detach();
}


/////////////////////////////////////////////////////////////////////////////
// Drawing Routines

// DrawHeaderBar: draw the header bar of the table
void CTable::DrawHeaderBar()
{
    CDC *pDC;
    CRect rect, rectTextClip;

    // prepare device context and font
    pDC = GetDC();
    pDC->SetBkMode(TRANSPARENT);
    pDC->SetTextColor(::GetSysColor(COLOR_WINDOWTEXT));
    if (pDC->SelectObject(pomHeaderFont) == NULL)   // it works with both NULL and invalid font
        pDC->SelectStockObject(DEFAULT_FONT);

    // paint background of the header bar
    GetClientRect(&rect);
    rect.bottom = rect.top + imHeaderFontHeight + 7;
    pDC->FillRect(&rect, &FaceBrush);

    // draw header of each field, one-by-one
    for (int i = 0; i < omLBorder.GetSize(); i++)
    {
        // draw recessed-style container box
        pDC->SelectObject(&ShadowPen);
        pDC->MoveTo(imTableContentOffset + omLBorder[i] - 1, rect.bottom - 1);
        pDC->LineTo(imTableContentOffset + omLBorder[i] - 1, rect.top + 1);
        pDC->LineTo(imTableContentOffset + omRBorder[i] + 1, rect.top + 1);
        pDC->SelectObject(&WhitePen);
        pDC->LineTo(imTableContentOffset + omRBorder[i] + 1, rect.bottom - 3);
        pDC->LineTo(imTableContentOffset + omLBorder[i] - 1, rect.bottom - 3);

        // draw the border line between the header bar and the table content
        pDC->SelectObject(&BlackPen);
        pDC->MoveTo(imTableContentOffset + omLBorder[i], rect.bottom - 1);
        pDC->LineTo(imTableContentOffset + omRBorder[i] + 1, rect.bottom - 1);

        // draw header text in the given clip-region
        if (i < omHeaderFields.GetSize())
        {
            const char *s = omHeaderFields[i];
            int n = omHeaderFields[i].GetLength();

            rectTextClip.left = imTableContentOffset + omLBorder[i] + 1;
            rectTextClip.top = rect.top + 2;
            rectTextClip.right = imTableContentOffset + omRBorder[i] + 1;
            rectTextClip.bottom = rect.bottom - 4;
            pDC->DrawText(s, n, &rectTextClip, DT_TOP | DT_LEFT);
        }
    }
    ReleaseDC(pDC);
}


#if 0
// DrawItem: draw an item in the table content area
void CTable::DrawItem(CDC &dc, UINT itemID, const CRect &rectClip,
    BOOL bpIsToggleFocusOnly, BOOL bpIsSelected, BOOL bpIsFocus)
{
    CString s;                  // comma-list represent record to be displayed
    int n;                      // length of string "s"
    int left, right;            // position to extract field from the comma-list

    CBrush TextBkBrush;         // helper brush (derived from lmTextBkColor)
    CBrush HighlightBkBrush;    // helper brush (derived from lmHighlightBkColor)
    CPen *pOldPen;
    CBrush *pOldBrush;
    CFont *pOldFont;
    int OldBkMode;
    COLORREF OldBkColor;
    COLORREF OldTextColor;

    // prepare helper drawing objects
	BOOL blIsValidItemID = (itemID < (UINT)omData.GetSize());
	COLORREF llTextColor = (blIsValidItemID)? omColor[itemID]: lmTextColor;
	COLORREF llTextBkColor = (blIsValidItemID)? omBkColor[itemID]: lmTextBkColor;
	BOOL blIsThickSeparateLine = blIsValidItemID && (omSeparatorType[itemID] == ST_THICK);
    TextBkBrush.CreateSolidBrush(llTextBkColor);
    HighlightBkBrush.CreateSolidBrush(lmHighlightBkColor);

    // change selected objects in current device context
    pOldPen = dc.SelectObject(&BlackPen);
    SeparatorBrush.UnrealizeObject();       // reset brush origin (for consistent pattern)
    dc.SetBrushOrg(dc.GetWindowOrg().x % 8, 0);
    pOldBrush = dc.SelectObject(&SeparatorBrush);
    if ((pOldFont = dc.SelectObject(pomTextFont)) == NULL)  // it works with both NULL and invalid font
        pOldFont = (CFont *)dc.SelectStockObject(DEFAULT_FONT);
    OldBkMode = dc.SetBkMode(TRANSPARENT);
	BOOL blIsDraggable = (blIsValidItemID)? omDragEnabled[itemID]: FALSE;
    OldBkColor = dc.SetBkColor(bpIsSelected && blIsDraggable? lmHighlightBkColor: llTextBkColor);
    OldTextColor = dc.SetTextColor(bpIsSelected && blIsDraggable? lmHighlightColor: llTextColor);

    // prepare data for scanning
    s = (itemID < (UINT)omData.GetSize())? omData[itemID]: "";
    n = s.GetLength();

    // paint each field of the given item
    int i;
    for (i = 0, left = 0, right = 0; i < omLBorder.GetSize(); i++, left = right + 1)
    {
        CRect rect, rectTextClip;

        // check the position of the text to be displayed
        if (left < n)   // more data?
            for (right = left; right < n && s[right] != '|'; right++)
                ;

        // do nothing if it is unnecessary to redraw this column
        rect = rectClip;
        rect.left = omLBorder[i] - 1;
        rect.right = omRBorder[i] + 3;
/////////
		if (rect.left > rect.right)
			rect.left = 1;

        if (!rect.IntersectRect(&rect, &rectClip))  // this column is out off drawing rectangle?
            continue;

        // drawing interior of the given field
        rect.left = omLBorder[i] + 1;
        rect.top = rectClip.top;
        rect.right = omRBorder[i] - 1;
        rect.bottom = rectClip.bottom - 1;

        if (!bpIsToggleFocusOnly)
        {
            // Draw the item background (using helper brushes).
            // The function FillRect() just won't fill the most right pixels.
            // So, I fixed it here before and after FillRect().
            rect.right++, rect.bottom++;
            dc.FillRect(&rect, bpIsSelected && blIsDraggable? &HighlightBkBrush: &TextBkBrush);
            rect.right--, rect.bottom--;

            // draw the separator line between items
			DWORD llRop = (blIsThickSeparateLine)? BLACKNESS: PATCOPY;
            if (rect.bottom == rect.top + imTextFontHeight + EXTRASPACE - 1)
			{
                dc.PatBlt(rect.left, rect.bottom, rect.right - rect.left + 1, 1, llRop);
				if (blIsThickSeparateLine)
				{
					dc.PatBlt(rect.left, rect.bottom-1, rect.right - rect.left + 1, 1, llRop);
				}
			}

            // draw text (content of the table) in the given clip-region
            if (left < n)
            {
                rectTextClip = rect;
                rectTextClip.right++;
                dc.DrawText(s.Mid(left, right-left), right-left, &rectTextClip, DT_TOP | DT_LEFT);
            }

            // draw the ridge (vertical separator)
            dc.SelectObject(&ShadowPen);
            dc.MoveTo(omLBorder[i] - 1, rectClip.top);
            dc.LineTo(omLBorder[i] - 1, rectClip.bottom);
            dc.SelectObject(&BlackPen);
            dc.MoveTo(omLBorder[i], rectClip.top);
            dc.LineTo(omLBorder[i], rectClip.bottom);
            dc.MoveTo(omRBorder[i], rectClip.top);
            dc.LineTo(omRBorder[i], rectClip.bottom);
            dc.SelectObject(&WhitePen);
            dc.MoveTo(omRBorder[i] + 1, rectClip.top);
            dc.LineTo(omRBorder[i] + 1, rectClip.bottom);
            dc.SelectObject(&FacePen);
            dc.MoveTo(omRBorder[i] + 2, rectClip.top);
            dc.LineTo(omRBorder[i] + 2, rectClip.bottom);
        }

        // need to redraw focus rectangle?
        if (bpIsFocus | bpIsToggleFocusOnly)
            dc.DrawFocusRect(&rect);
    }

    // delete helper drawing objects
    TextBkBrush.DeleteObject();
    HighlightBkBrush.DeleteObject();

    // restore the old device context (necessary for owner-drawn controls in MFC)
    dc.SelectObject(pOldPen);
    dc.SelectObject(pOldBrush);              
    dc.SetBkMode(OldBkMode);
    dc.SetBkColor(OldBkColor);
    dc.SetTextColor(OldTextColor);
}

#endif

// DrawItem: draw an item in the table content area
void CTable::DrawItem(CDC &dc, UINT itemID, const CRect &rectClip,
    BOOL bpIsToggleFocusOnly, BOOL bpIsSelected, BOOL bpIsFocus)
{
    CString s;                  // comma-list represent record to be displayed
    int n;                      // length of string "s"
    int left=0, right=0;            // position to extract field from the comma-list

    CBrush TextBkBrush;         // helper brush (derived from lmTextBkColor)
    CBrush HighlightBkBrush;    // helper brush (derived from lmHighlightBkColor)
    CPen *pOldPen;
    CBrush *pOldBrush;
    CFont *pOldFont;
    int OldBkMode;
    COLORREF OldBkColor;
    COLORREF OldTextColor;

    // prepare helper drawing objects
	BOOL blIsValidItemID = (itemID < (UINT)omData.GetSize());
	COLORREF llTextColor = (blIsValidItemID)? omColor[itemID]: lmTextColor;
	COLORREF llTextBkColor = (blIsValidItemID)? omBkColor[itemID]: lmTextBkColor;
	BOOL blIsThickSeparateLine = blIsValidItemID && (omSeparatorType[itemID] == ST_THICK);
	CFont *polFont = (blIsValidItemID)? (CFont *)omFontPtr[itemID]: NULL;
    TextBkBrush.CreateSolidBrush(llTextBkColor);
    HighlightBkBrush.CreateSolidBrush(lmHighlightBkColor);

    // change selected objects in current device context
    pOldPen = dc.SelectObject(&BlackPen);
    SeparatorBrush.UnrealizeObject();       // reset brush origin (for consistent pattern)
    dc.SetBrushOrg(dc.GetWindowOrg().x % 8, 0);
    pOldBrush = dc.SelectObject(&SeparatorBrush);
    if ((pOldFont = dc.SelectObject(polFont)) == NULL)  // it works with both NULL and invalid font
        pOldFont = (CFont *)dc.SelectStockObject(DEFAULT_FONT);
    OldBkMode = dc.SetBkMode(TRANSPARENT);
	BOOL blIsDraggable = (blIsValidItemID)? omDragEnabled[itemID]: FALSE;
    OldBkColor = dc.SetBkColor(bpIsSelected && blIsDraggable? lmHighlightBkColor: llTextBkColor);
    OldTextColor = dc.SetTextColor(bpIsSelected && blIsDraggable? lmHighlightColor: llTextColor);

    // prepare data for scanning
    s = (itemID < (UINT)omData.GetSize())? omData[itemID]: "";
    n = s.GetLength();
    // paint each field of the given item
    int i;
    for (i = 0, left = 0; i < omLBorder.GetSize(); i++, left = right + 1)
    {
        CRect rect, rectTextClip;
		
		CString olType = (i < omTypeList.GetSize())? omTypeList[i]: CString("TEXT");
			
        // check the position of the text to be displayed
        if (left < n)   // more data?
            for (right = left; right < n && s[right] != '|'; right++)
                ;

        // do nothing if it is unnecessary to redraw this column
        rect = rectClip;
        rect.left = omLBorder[i] - 1;
        rect.right = omRBorder[i] + 3;
		if (rect.left > rect.right)
			rect.left = 1;
        if (!rect.IntersectRect(&rect, &rectClip))  // this column is out off drawing rectangle?
            continue;

        // drawing interior of the given field
        rect.left = omLBorder[i] + 1;
        rect.top = rectClip.top;
        rect.right = omRBorder[i] - 1;
        rect.bottom = rectClip.bottom - 1;

        if (!bpIsToggleFocusOnly)
        {
			if (olType == CString("TEXT"))
			{
				// Draw the item background (using helper brushes).
				// The function FillRect() just won't fill the most right pixels.
				// So, I fixed it here before and after FillRect().
				rect.right++, rect.bottom++;
				dc.FillRect(&rect, bpIsSelected && blIsDraggable? &HighlightBkBrush: &TextBkBrush);
				rect.right--, rect.bottom--;

				// draw the separator line between items
				DWORD llRop = (blIsThickSeparateLine)? BLACKNESS: PATCOPY;
				if (rect.bottom == rect.top + imTextFontHeight + EXTRASPACE - 1)
				{
					COLORREF oldColor = dc.SetTextColor(BLACK);	// make sure that it will be black dotted
	                dc.PatBlt(rect.left, rect.bottom, rect.right - rect.left + 1, 1, llRop);
					if (blIsThickSeparateLine)
					{
						dc.PatBlt(rect.left, rect.bottom-1, rect.right - rect.left + 1, 1, llRop);
					}
					dc.SetTextColor(oldColor);
				}

				// draw text (content of the table) in the given clip-region
				if (left < n)
				{
					rectTextClip = rect;
					rectTextClip.left++;	// for lefting one pixel before the text
					rectTextClip.right++;
					dc.DrawText(s.Mid(left, right-left), right-left, &rectTextClip, DT_TOP | DT_LEFT);
				}
			}
			else
			{
				if (s != CString(""))
				{
					COLORREF llColor = atol(s.Mid(left, right-left));
					CBrush ColorBkBrush;    //  brush to fill background of color field
					ColorBkBrush.CreateSolidBrush(llColor);
					// Draw the item background (using helper brushes).
					// The function FillRect() just won't fill the most right pixels.
					// So, I fixed it here before and after FillRect().
					rect.right++, rect.bottom++;
					dc.FillRect(&rect, &ColorBkBrush);
					rect.right--, rect.bottom--;
				}
				else
				{
					rect.right++, rect.bottom++;
					dc.FillRect(&rect, bpIsSelected? &HighlightBkBrush: &TextBkBrush);
					rect.right--, rect.bottom--;
				}
				// draw the separator line between items
				DWORD llRop = (blIsThickSeparateLine)? BLACKNESS: PATCOPY;
				if (rect.bottom == rect.top + imTextFontHeight + EXTRASPACE - 1)
				{
					COLORREF oldColor = dc.SetTextColor(BLACK);	// make sure that it will be black dotted
	                dc.PatBlt(rect.left, rect.bottom, rect.right - rect.left + 1, 1, llRop);
					if (blIsThickSeparateLine)
					{
						dc.PatBlt(rect.left, rect.bottom-1, rect.right - rect.left + 1, 1, llRop);
					}
					dc.SetTextColor(oldColor);
				}

				// no draw text for color fields
			}
            // draw the ridge (vertical separator)
            dc.SelectObject(&ShadowPen);
            dc.MoveTo(omLBorder[i] - 1, rectClip.top);
            dc.LineTo(omLBorder[i] - 1, rectClip.bottom);
            dc.SelectObject(&BlackPen);
            dc.MoveTo(omLBorder[i], rectClip.top);
            dc.LineTo(omLBorder[i], rectClip.bottom);
            dc.MoveTo(omRBorder[i], rectClip.top);
            dc.LineTo(omRBorder[i], rectClip.bottom);
            dc.SelectObject(&WhitePen);
			dc.MoveTo(omRBorder[i] + 1, rectClip.top);
            dc.LineTo(omRBorder[i] + 1, rectClip.bottom);
            dc.SelectObject(&FacePen);
			dc.MoveTo(omRBorder[i] + 2, rectClip.top);
            dc.LineTo(omRBorder[i] + 2, rectClip.bottom);
        }

        // need to redraw focus rectangle?
        if (bpIsFocus | bpIsToggleFocusOnly)
            dc.DrawFocusRect(&rect);
    }

    // delete helper drawing objects
    TextBkBrush.DeleteObject();
    HighlightBkBrush.DeleteObject();

    // restore the old device context (necessary for owner-drawn controls in MFC)
    dc.SelectObject(pOldPen);
    dc.SelectObject(pOldBrush);
    dc.SelectObject(pOldFont);
    dc.SetBkMode(OldBkMode);
    dc.SetBkColor(OldBkColor);
    dc.SetTextColor(OldTextColor);
}

LONG CTable::OnDragOver(UINT wParam, LONG lParam)
{
	long llRc;
	if(ogPrivList.GetStat("TABLE_DRAG")!='1')
	{
		return -1L;
	}
	
 	CWnd *pWnd = pomParentWindow;	// surrounding window
    llRc = pWnd->SendMessage(WM_DRAGOVER, wParam, lParam);
	return llRc;
}

LONG CTable::OnDrop(UINT wParam, LONG lParam)
{
	long llRc;

 	CWnd *pWnd = pomParentWindow;	// surrounding window
    llRc = pWnd->SendMessage(WM_DROP, wParam, lParam);
	return llRc;
}

void CTableListBox::OnSelchange() 
{
	// TODO: Add your control notification handler code here
	int ilItemID = -1;
	CWnd *pWnd = pomTableWindow->pomParentWindow;	// surrounding window
	if (pomTableWindow->imSelectMode & (LBS_MULTIPLESEL | LBS_EXTENDEDSEL))	
	{
		// multiple selection?
		return;
	}
	else
	{
		ilItemID=GetCurSel();
		pWnd->SendMessage(WM_TABLE_SELCHANGE, ilItemID, (LPARAM)this);
	}
}
  
