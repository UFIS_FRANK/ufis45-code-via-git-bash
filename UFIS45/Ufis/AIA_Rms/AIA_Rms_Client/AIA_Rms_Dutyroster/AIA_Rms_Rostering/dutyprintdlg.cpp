// dutyprintdlg.cpp : implementation file
//

#include <stdafx.h>
#include <rostering.h>
#include <dutyprintdlg.h>
#include <PrintDailyRoster.h>
#include <PrintDailyRosterB.h>
#include <ReportCompensation.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DutyPrintDlg dialog


DutyPrintDlg::DutyPrintDlg(CWnd* pParent /*=NULL*/, COleDateTime pDate /*=NULL*/)
	: CDialog(DutyPrintDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(DutyPrintDlg)
	//}}AFX_DATA_INIT
	pomParent	= (DutyRoster_View*)pParent;
	pomStfData	= &pomParent->omStfData;
	omDate		= pDate;
	if (omDate.GetStatus() == COleDateTime::valid)
	{
		if (omDate.GetYear() < 1900)
		{
			omDate.SetStatus (COleDateTime::invalid);
		}
	}
}

void DutyPrintDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DutyPrintDlg)
	DDX_Control(pDX, IDC_E_DUTYPRINT_DAY, m_DutyprintDay);
	DDX_Control(pDX, IDC_PLAN1, m_Print1);
	DDX_Control(pDX, IDC_PLAN2, m_Print2);
	DDX_Control(pDX, IDC_PLAN3, m_Print3);
	DDX_Control(pDX, IDC_PLAN4, m_Print4);
	DDX_Control(pDX, IDC_PLAN5, m_Print5);
	DDX_Control(pDX, IDC_PLANL, m_PrintL);
	DDX_Control(pDX, IDC_R_DAILY_A, m_PrintDailyTypA);
	DDX_Control(pDX, IDC_R_DAILY_B, m_PrintDailyTypB);
	DDX_Control(pDX, IDC_R_PRINT_DUTYOVERVIEW, m_PrintDutyOverview);
	DDX_Control(pDX, IDC_R_COMPENSATION, m_CompensationReport);
	DDX_Control(pDX, IDC_S_L, m_SL);
	DDX_Control(pDX, IDC_S_5, m_S5);
	DDX_Control(pDX, IDC_S_4, m_S4);
	DDX_Control(pDX, IDC_S_3, m_S3);
	DDX_Control(pDX, IDC_S_2, m_S2);
	DDX_Control(pDX, IDC_S_1, m_S1);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DutyPrintDlg, CDialog)
	//{{AFX_MSG_MAP(DutyPrintDlg)
	ON_BN_CLICKED(IDC_R_DAILY_A, OnRDailyA)
	ON_BN_CLICKED(IDC_R_DAILY_B, OnRDailyB)
	ON_BN_CLICKED(IDC_R_PRINT_DUTYOVERVIEW, OnRPrintDutyoverview)
	ON_BN_CLICKED(IDC_R_COMPENSATION, OnRCompensationReport)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DutyPrintDlg message handlers

BOOL DutyPrintDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// setting the text
	SetWindowText(LoadStg(IDS_STRING449));
	SetDlgItemText(IDC_R_PRINT_DUTYOVERVIEW,LoadStg(IDS_STRING450));
	SetDlgItemText(IDC_PLAN1,LoadStg(IDS_STRING405));
	SetDlgItemText(IDC_PLAN2,LoadStg(IDS_STRING412));
	SetDlgItemText(IDC_PLAN3,LoadStg(IDS_STRING421));
	SetDlgItemText(IDC_PLAN4,LoadStg(IDS_STRING422));
	SetDlgItemText(IDC_PLAN5,LoadStg(IDS_STRING423));
	SetDlgItemText(IDC_PLANL,LoadStg(IDC_PLANLU));
	SetDlgItemText(IDC_S_DUTYPRINT_DAY,LoadStg(IDS_DAY));
	SetDlgItemText(IDC_R_DAILY_A,LoadStg(IDS_STRING447));
	SetDlgItemText(IDC_R_DAILY_B,LoadStg(IDS_STRING448));

	m_DutyprintDay.SetTypeToDate(TRUE);
	m_DutyprintDay.SetTextErrColor(RED);
	m_DutyprintDay.SetBKColor(LTYELLOW);
	if (omDate.GetStatus() == COleDateTime::valid)
	{
		CString olDate = omDate.Format ("%d.%m.%Y");
		m_DutyprintDay.SetInitText(olDate);
		m_DutyprintDay.EnableWindow(TRUE);
		m_PrintDailyTypA.SetCheck(1);
		OnRDailyA();
	}
	else
	{
		// setting the default radio-buttons
		m_PrintDutyOverview.SetCheck(1);
		m_Print2.SetCheck(1);
		m_DutyprintDay.EnableWindow(FALSE);
	}

	// looking for which levels to display
	InitLevels();

	// hiding the color-rects (we only need the rectangle in OnPaint)
	m_S1.ShowWindow(SW_HIDE);
	m_S2.ShowWindow(SW_HIDE);
	m_S3.ShowWindow(SW_HIDE);
	m_S4.ShowWindow(SW_HIDE);
	m_S5.ShowWindow(SW_HIDE);
	m_SL.ShowWindow(SW_HIDE);

	// reports only for ATH
	if (ogCCSParam.GetParamValue(ogAppl,"ID_PROD_CUSTOMER") != "ATH")
	{
		CRect olRect;
		int ilMoveUp = 45;

		// --- hiding the report(s)
		GetDlgItem (IDC_FRAME_REPORTS)->ShowWindow(SW_HIDE);
		GetDlgItem (IDC_R_COMPENSATION)->ShowWindow(SW_HIDE);

		// --- resizing the form
		GetWindowRect (olRect);
		ClientToScreen (olRect);
		MoveWindow(olRect.left, olRect.top, olRect.Width(), olRect.Height() - ilMoveUp);
		CenterWindow();

		// --- moving the OK and CANCEL button
		GetDlgItem (IDOK)->GetWindowRect(olRect);
		ScreenToClient (olRect);
		GetDlgItem (IDOK)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

		GetDlgItem (IDCANCEL)->GetWindowRect(olRect);
		ScreenToClient (olRect);
		GetDlgItem (IDCANCEL)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void DutyPrintDlg::OnRDailyA()
{
	m_Print1.EnableWindow(FALSE);
	m_Print2.EnableWindow(FALSE);
	m_Print3.EnableWindow(FALSE);
	m_Print4.EnableWindow(FALSE);
	m_Print5.EnableWindow(FALSE);
	m_PrintL.EnableWindow(FALSE);
	m_DutyprintDay.EnableWindow(TRUE);
	m_DutyprintDay.SetBKColor(LTYELLOW);
}

void DutyPrintDlg::OnRDailyB() 
{
	m_Print1.EnableWindow(FALSE);
	m_Print2.EnableWindow(FALSE);
	m_Print3.EnableWindow(FALSE);
	m_Print4.EnableWindow(FALSE);
	m_Print5.EnableWindow(FALSE);
	m_PrintL.EnableWindow(FALSE);
	m_DutyprintDay.EnableWindow(TRUE);
	m_DutyprintDay.SetBKColor(LTYELLOW);
}

void DutyPrintDlg::OnRPrintDutyoverview() 
{
	m_Print1.EnableWindow();
	m_Print2.EnableWindow();
	m_Print3.EnableWindow();
	m_Print4.EnableWindow();
	m_Print5.EnableWindow();
	m_PrintL.EnableWindow();
	if (m_Print1.GetCheck() == 0 &&
		m_Print2.GetCheck() == 0 &&
		m_Print3.GetCheck() == 0 &&
		m_Print4.GetCheck() == 0 &&
		m_Print5.GetCheck() == 0 &&
		m_PrintL.GetCheck() == 0)
	{
		// setting default
		m_Print2.SetCheck(1);
	}
	m_DutyprintDay.EnableWindow(FALSE);

}

void DutyPrintDlg::OnRCompensationReport()
{
	m_Print1.EnableWindow(FALSE);
	m_Print2.EnableWindow(FALSE);
	m_Print3.EnableWindow(FALSE);
	m_Print4.EnableWindow(FALSE);
	m_Print5.EnableWindow(FALSE);
	m_PrintL.EnableWindow(FALSE);
	m_DutyprintDay.EnableWindow(FALSE);
}

void DutyPrintDlg::OnOK() 
{
	CString olStartApp = pcgRosteringPrintPath;

	if (m_PrintDutyOverview.GetCheck() == 1)
	{
		PrintDutyOverview();
		CDialog::OnOK();
	}
	else if (m_CompensationReport.GetCheck() == 1)
	{
		CString olFileName = GetUniqueFileName("RPT_");
		if (olFileName.GetLength())
		{
			ReportCompensation olPrint(*pomParent);
			if (olPrint.GeneratePrintFile(olFileName))
			{
				SendFileName(olStartApp,olFileName);
			}
		}
		CDialog::OnOK();
	}
	else
	{
		if (m_DutyprintDay.GetStatus() == true)
		{
			CString olDate;
			m_DutyprintDay.GetWindowText(olDate);
			omDate = OleDateStringToDate(olDate);
			if (m_PrintDailyTypA.GetCheck() == 1)
			{
				CString olFileName = GetUniqueFileName("DRP_");
				if (olFileName.GetLength())
				{
					CPrintDailyRoster olPrint(omDate, pomParent->GetSelectedView(),pomParent->rmViewInfo.oStfUrnos,"");
					if (olPrint.GeneratePrintFile(olFileName))
					{
						SendFileName(olStartApp,olFileName);
					}
				}
			}
			else if (m_PrintDailyTypB.GetCheck() == 1)
			{
				CString olFileName = GetUniqueFileName("DRP_");
				if (olFileName.GetLength())
				{
					CPrintDailyRosterB olPrint(omDate, pomParent->GetSelectedView(), pomParent->rmViewInfo.oStfUrnos,"");
					if (olPrint.GeneratePrintFile(olFileName))
					{
						SendFileName(olStartApp,olFileName);
					}
				}
			}

			CDialog::OnOK();
		}
		else
		{
			Beep(440,70);
			MessageBox(LoadStg(IDS_NO_DATE), LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
			m_DutyprintDay.SetFocus();
		}
	}
}

void DutyPrintDlg::OnCancel() 
{
	CDialog::OnCancel();
}

void DutyPrintDlg::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	CBrush olBrush;
	CRect	olRect;
	CPen	olPen;
	olPen.CreatePen(PS_SOLID, 1, COLORREF(BLACK)); 
	olBrush.CreateSolidBrush(COLORREF(BLACK));
	dc.SelectObject(&olPen);
	bool blRect; //we draw only the rect if level is visible

	for (int i = 0; i < 6; i++)
	{
		blRect = false;
		switch (i)
		{
		case 0:
			{
				if (bmShow1)
				{
					m_S1.GetWindowRect(&olRect);
					olBrush.DeleteObject();
					olBrush.CreateSolidBrush(COLORREF(GRAY));
					blRect = true;
				}
				break;
			}
		case 1:
			{
				if (bmShow2)
				{
					m_S2.GetWindowRect(&olRect);
					olBrush.DeleteObject();
					olBrush.CreateSolidBrush(COLORREF(BLACK));
					blRect = true;
				}
				break;
			}
		case 2:
			{
				if (bmShow3)
				{
					m_S3.GetWindowRect(&olRect);
					olBrush.DeleteObject();
					olBrush.CreateSolidBrush(COLORREF(BLUE));
					blRect = true;
				}
				break;
			}
		case 3:
			{
				if (bmShow4)
				{
					m_S4.GetWindowRect(&olRect);
					olBrush.DeleteObject();
					olBrush.CreateSolidBrush(COLORREF(PURPLE));
					blRect = true;
				}
				break;
			}
		case 4:
			{
				if (bmShow5)
				{
					m_S5.GetWindowRect(&olRect);
					olBrush.DeleteObject();
					olBrush.CreateSolidBrush(COLORREF(ORANGE));
					blRect = true;
				}
				break;
			}
		case 5:
			{
				if (bmShowL)
				{
					m_SL.GetWindowRect(&olRect);
					olBrush.DeleteObject();
					olBrush.CreateSolidBrush(COLORREF(FUCHSIA));
					blRect = true;
				}
				break;
			}
		}
		if (blRect)
		{
			ScreenToClient(&olRect);
			dc.SelectObject(&olBrush);
			dc.Rectangle(olRect);
		}
	}
	olBrush.DeleteObject();
	olPen.DeleteObject();
}

CString DutyPrintDlg::GetUniqueFileName(CString opPrefix)
{
	CString olResult;
	COleDateTime olDate;
	olDate = COleDateTime::GetCurrentTime();
	CString olFilePath = pcgLogFilePath;
	int ilPos = olFilePath.Find("ROSTERING");
	if (ilPos > -1)
	{
		olResult = olFilePath.Left(ilPos) + opPrefix + olDate.Format("%Y%m%d_%H%M%S") + ".UPF";
	}
	return olResult;
}

BOOL DutyPrintDlg::SendFileName(CString opApp, CString opFileName)
{
	bool blStarted = false;

	
	//getting the uppercase-string
	char tCommandLine[512];

	strcpy (tCommandLine, pcgRosteringPrintPath);
	if (opFileName.GetLength())
	{
		strcat (tCommandLine, " ");
		strcat (tCommandLine, opFileName.GetBuffer(0));
	}

	//starting the process
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	ZeroMemory(&si,sizeof(si));
	si.cb = sizeof(si);
	si.wShowWindow = SW_SHOWNORMAL;
	si.dwFlags = STARTF_USESHOWWINDOW;
	blStarted = CreateProcess (NULL,tCommandLine, NULL,NULL,FALSE,NORMAL_PRIORITY_CLASS,NULL,NULL,&si,&pi);

	if (blStarted == false)
	{
		AfxMessageBox(IP_UFISAPPMNG_STARTAPP_FAILED, MB_ICONINFORMATION); 
		return FALSE;
	}
	else
	{
		return TRUE;
	}
}

BOOL DutyPrintDlg::CheckStartApp(CString opApp)
{
	return TRUE;
}

CString DutyPrintDlg::GetEmployeeText(long lpStfUrno, CString opSday)
{
	CString olLeftText;
	CString olRightText;
	CString olTmp;

//--- getting the left text of DutySolidTabViewer (e.g. "LA / VZTAG / 38:50")	
	CString olActualCot		= "";
	CString olFunctionCode	= "";
	CString olHoursPerWeek	= "";

	CCSPtrArray<SPFDATA> olSpfData;
	int ilSpfCnt = ogSpfData.GetSpfArrayBySurnWithTime(olSpfData, lpStfUrno, pomParent->rmViewInfo.oSelectDate, pomParent->rmViewInfo.bCheckMainFuncOnly);
	if (ilSpfCnt > 0)
	{
		olFunctionCode = olSpfData[0].Fctc;
	}
	olActualCot = ogScoData.GetCotAndCWEHBySurnWithTime(lpStfUrno, pomParent->rmViewInfo.oSelectDate, &olHoursPerWeek);

	if (olHoursPerWeek.GetLength() == 0)
	{
		COTDATA *prlCotData = ogCotData.GetCotByCtrc(olActualCot);
		if (prlCotData != NULL)
		{
			olHoursPerWeek = prlCotData->Whpw;
		}
	}

	if (olHoursPerWeek.GetLength() > 2)
	{
		olHoursPerWeek.Insert(2,":");
		olLeftText = olFunctionCode + " / " + olActualCot + " / " + olHoursPerWeek; 
	}
	else
	{
		olLeftText = olFunctionCode + " / " + olActualCot + " / "; 
	}

//--- getting the right text of DutySolidTabViewer
	STFDATA *polStfData;
	polStfData = ogStfData.GetStfByUrno (lpStfUrno);
	if (polStfData != NULL)
	{
		olRightText = CBasicData::GetFormatedEmployeeName(pomParent->rmViewInfo.iEName, polStfData, "", "");
	}
	return olRightText + "|" + olLeftText;
}

CString DutyPrintDlg::GetShiftText(DRRDATA *popDrrData)
{
	CString olResult;
	CString olTmp;
	CString olAvfr;
	CString olScod;
	olScod = CString (popDrrData->Scod);
	ODADATA *polOdaData;
	polOdaData = ogOdaData.GetOdaBySdac (olScod);
	if (polOdaData != NULL)
	{
		return olScod;
	}
	BSDDATA *polBsdData = ogBsdData.GetBsdByBsdc (olScod);
	if (polBsdData != NULL)
	{
		if (CString(polBsdData->Bewc) == "J" || bgPrintDutyrosterShiftcode)
		{
			return olScod;
		}
	}

	if (popDrrData->Avfr.GetStatus() == COleDateTime::valid)
	{
		olAvfr = popDrrData->Avfr.Format("%H:%M");
	}
	CString olAvto;
	if (popDrrData->Avto.GetStatus() == COleDateTime::valid)
	{
		olAvto = popDrrData->Avto.Format("%H:%M");
	}

	if (olAvfr.GetLength() > 4 && olAvto.GetLength() > 4)
	{
		olResult = olAvfr + "-"; //"19:30-"

		olResult += olAvto;
	}
	else
	{
		olResult = CString(popDrrData->Scod);
	}
	return olResult;
}


void DutyPrintDlg::PrintDutyOverview() 
{
	//--- look after RosteringPrint.exe: start it, if not started yet or pending
	CString olStartApp = pcgRosteringPrintPath;

	//--- look after the file
	CString	olTmpLine;
	CString olTmp;
	CString olFileName = GetUniqueFileName("DP_");
	ofstream of;

	//--- open the file
	of.open(olFileName);

	//--- writing the name of the report to print
	olTmp = "DUTYOVERVIEW";
	of << "<=NAME=>" << olTmp << "<=\\=>" << endl;

	//--- getting the title (name of the view), e.g. "Dienstplan Mai 2002"
	olTmp = pomParent->GetSelectedView();
	of << "<=TITLE=>" << olTmp << "<=\\=>" << endl;

	//--- getting the user, e.g. "UserXYZ"
	olTmp = pcgUser;
	of << "<=USER=>" << olTmp << "<=\\=>" << endl;

	//--- getting valid from, e.g. "01.04.2002"
	olTmp = pomParent->rmViewInfo.oDateFrom.Format("%d.%m.%Y");
	of << "<=FROM=>" << olTmp << "<=\\=>" << endl;

	//--- getting valid to, e.g. "28.04.2002"
	olTmp = pomParent->rmViewInfo.oDateTo.Format("%d.%m.%Y");
	of << "<=TO=>" << olTmp << "<=\\=>" << endl;

	//--- getting the customer ID
	olTmp = ogCCSParam.GetParamValue(ogAppl,"ID_PROD_CUSTOMER");
	of << "<=CUSTOMER=>" << olTmp << "<=\\=>" << endl;

	//--- getting only the holidays, not weekend
	COleDateTime olLookupDate = pomParent->rmViewInfo.oDateFrom;
	CString olLookupDateString;
	void  *prlVoid = NULL;
	olTmp = "";
	while (olLookupDate <= pomParent->rmViewInfo.oDateTo)
	{
		olLookupDateString = olLookupDate.Format("%Y%m%d000000");
		if (pomParent->pomIsTabViewer->omHolidayKeyMap.Lookup(olLookupDateString, (void *&)prlVoid) == TRUE)
		{
			olTmp += "|" + olLookupDate.Format("%d.%m.%Y");
		}
		olLookupDate += COleDateTimeSpan(1, 0, 0, 0);
	}
	if (olTmp.GetLength() > 0)
	{
		olTmp = olTmp.Mid(1);
	}
	of << "<=HOLDAYS=>" << olTmp << "<=\\=>" << endl;

	//--- getting the shiftplan-lines and writing out line by line
	CString olRosl;
	if (m_Print1.GetCheck() == 1)
		olRosl = "1";
	else if (m_Print2.GetCheck() == 1)
		olRosl = "2";
	else if (m_Print3.GetCheck() == 1)
		olRosl = "3";
	else if (m_Print4.GetCheck() == 1)
		olRosl = "4";
	else if (m_Print5.GetCheck() == 1)
		olRosl = "5";
	else if (m_PrintL.GetCheck() == 1)
		olRosl = "L";

	CString olYearMonth = pomParent->rmViewInfo.oDateFrom.Format("%Y%m");
	COleDateTime olFrom = pomParent->rmViewInfo.oDateFrom;
	COleDateTime olTo = pomParent->rmViewInfo.oDateTo;

	long llStfUrno;

	CString	olSday;
	CString olRgbHexColor;
	ODADATA *prlOdaData = NULL;
	BSDDATA *prlBsdData = NULL;
	DRRDATA *polDrrData = NULL;
	for (int ilEmployee = 0; ilEmployee < pomStfData->GetSize(); ilEmployee++)
	{
		llStfUrno = (*pomStfData)[ilEmployee].Urno;
		olTmp.Format("%d", ilEmployee + 1);
		olTmpLine = "<=" + olTmp + "=>" + GetEmployeeText(llStfUrno, "");
		olTmp = "";

		for (COleDateTime olDay = olFrom; olDay <= olTo; olDay += COleDateTimeSpan(1,0,0,0))
		{
			olTmp += "|";
			olSday = olDay.Format("%Y%m%d");

			polDrrData = ogDrrData.GetDrrByKey(olSday, llStfUrno, "1", olRosl);
			if (polDrrData != NULL)
			{
				olTmp += GetShiftText(polDrrData);
				prlOdaData = ogOdaData.GetOdaBySdac(polDrrData->Scod);
				if (prlOdaData != NULL)
				{
					olRgbHexColor = prlOdaData->Rgbc;
				}
				else
				{
					prlBsdData = ogBsdData.GetBsdByBsdc (polDrrData->Scod);
					if (prlBsdData != NULL)
					{
						olRgbHexColor = prlBsdData->Rgbc;
					}
					else
					{
						olRgbHexColor = "";
					}
				}
			}
			polDrrData = ogDrrData.GetDrrByKey(olSday, llStfUrno, "2", olRosl);
			if (polDrrData != NULL)
			{
				olTmp += "/" + GetShiftText(polDrrData);
			}
			olTmp += CString("@") + olRgbHexColor;
		}
		olTmpLine += "|" + olTmp.Mid(1) + "<=\\=>";

		of << olTmpLine << endl;
	}

	//--- close the file
	of.close();

	//--- look after RosteringPrint.exe: send filename to it, if started.
	SendFileName (olStartApp, olFileName);
}

void DutyPrintDlg::InitLevels()
{
	bmShow1=bmShow2=bmShow3=bmShow4=bmShow5=bmShowL=false;

	CString olStepList = ogCCSParam.GetParamValue(ogAppl,"ID_SHOW_STEPS");

	// In Array überführen
	CStringArray olStepListStrArray;
	int ilArray = ExtractItemList(olStepList,&olStepListStrArray, '|');

	for (int i = 0; i < ilArray; i++)
	{
		CString olTmp = olStepListStrArray.GetAt(i);

		if (olTmp == "L")
		{
			bmShowL = true;
		}
		else if (olTmp == "1")
		{
			bmShow1 = true;
		}
		else if (olTmp == "2")
		{
			bmShow2 = true;
		}
		else if (olTmp == "3")
		{
			bmShow3 = true;
		}
		else if (olTmp == "4")
		{
			bmShow4 = true;
		}
		else if (olTmp == "5")
		{
			bmShow5 = true;
		}
	}

	// Oberflächen Elemente entsprechend der Auswertung setzen
	if (!bmShowL) m_PrintL.ShowWindow(SW_HIDE);
	if (!bmShow1) m_Print1.ShowWindow(SW_HIDE);
	if (!bmShow2) m_Print2.ShowWindow(SW_HIDE);
	if (!bmShow3) m_Print3.ShowWindow(SW_HIDE);
	if (!bmShow4) m_Print4.ShowWindow(SW_HIDE);
	if (!bmShow5) m_Print5.ShowWindow(SW_HIDE);
}
