// DutyAttentionDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld DutyAttentionDlg 
//---------------------------------------------------------------------------

DutyAttentionDlg::DutyAttentionDlg(CWnd* pParent /*=NULL*/)	: CDialog(DutyAttentionDlg::IDD, pParent)
{
	 pomParent = (DutyRoster_View*)pParent;

	//{{AFX_DATA_INIT(DutyAttentionDlg)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT

	// nicht modaler Dialog
//	CDialog::Create(DutyAttentionDlg::IDD, pParent);
}

DutyAttentionDlg::~DutyAttentionDlg()
{

}

bool DutyAttentionDlg::Create()
{
	return CDialog::Create(DutyAttentionDlg::IDD,pomParent) != 0;
}

void DutyAttentionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DutyAttentionDlg)
	DDX_Control(pDX, IDC_LB_ATTENTION, m_LB_Attention);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DutyAttentionDlg, CDialog)
	//{{AFX_MSG_MAP(DutyAttentionDlg)
	ON_BN_CLICKED(IDC_B_DELETE, On_B_Delete)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten DutyAttentionDlg 
//---------------------------------------------------------------------------

BOOL DutyAttentionDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	HICON m_hIcon = AfxGetApp()->LoadIcon(IDR_ROSTERTYPE);
	SetIcon(m_hIcon, FALSE);

	SetWindowText(LoadStg(IDS_INFO));
	
	SetDlgItemText(IDC_B_DELETE, LoadStg(IDS_STRING966));
	SetDlgItemText(IDCANCEL, LoadStg(ID_CANCEL));
	
	int ilInfoSize = pomParent->omAttentionInfo.GetSize();
	for(int i=0; i<ilInfoSize; i++)
	{
		m_LB_Attention.AddString(pomParent->omAttentionInfo[i].Text,pomParent->omAttentionInfo[i].Color);
		m_LB_Attention.SetCurSel(m_LB_Attention.GetCount()-1);
	}
	ShowWindow(SW_SHOWNORMAL);

	return TRUE;
}

//*******************************************************************************************************************
// AddAttention: f�gt der ListBox den Eintrag <opAttention> in der Textfarbe
//	<opAttentionColor> hinzu.
// R�ckgabe:	keine
//*******************************************************************************************************************

void DutyAttentionDlg::AddAttention(CString opAttention, COLORREF opAttentionColor)
{
CCS_TRY;
	m_LB_Attention.AddString(opAttention,opAttentionColor);
	m_LB_Attention.SetCurSel(m_LB_Attention.GetCount()-1);
CCS_CATCH_ALL;
}

//---------------------------------------------------------------------------

void DutyAttentionDlg::UpdateListBox() 
{
CCS_TRY;
	int ilInfoSize = pomParent->omAttentionInfo.GetSize();

	int ilListBolxSize = m_LB_Attention.GetCount();
	if(ilListBolxSize == LB_ERR) ilListBolxSize = 0;

	if(ilInfoSize>ilListBolxSize)
	{
		for(;ilListBolxSize<ilInfoSize;ilListBolxSize++)
		{
			m_LB_Attention.AddString(pomParent->omAttentionInfo[ilListBolxSize].Text,pomParent->omAttentionInfo[ilListBolxSize].Color);
			m_LB_Attention.SetCurSel(m_LB_Attention.GetCount()-1);
		}
	}
CCS_CATCH_ALL;
}

//---------------------------------------------------------------------------

void DutyAttentionDlg::On_B_Delete() 
{
	m_LB_Attention.ResetContent();
	pomParent->omAttentionInfo.DeleteAll();
}

//---------------------------------------------------------------------------

void DutyAttentionDlg::OnCancel() 
{
	CDialog::OnCancel();
//	CDialog::DestroyWindow();
//	delete this;
//	pogDutyAttentionDlg = NULL;
}

//---------------------------------------------------------------------------

void DutyAttentionDlg::OnOK() 
{
	//do nothing
}

//---------------------------------------------------------------------------

void DutyAttentionDlg::InternalCancel() 
{
	OnCancel();
}

//---------------------------------------------------------------------------


//uhi 23.4.01
void DutyAttentionDlg::ResetAttentionDlg()
{
	m_LB_Attention.ResetContent();
}
