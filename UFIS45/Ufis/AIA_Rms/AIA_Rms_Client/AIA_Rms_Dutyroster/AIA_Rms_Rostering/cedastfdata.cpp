// CedaStfData.cpp
 
#include <stdafx.h>
 

void ProcessStfCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaStfData ogStfData;

CedaStfData::CedaStfData() : CedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for STFDataStruct
	BEGIN_CEDARECINFO(STFDATA,STFDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_OLEDATE(Dodm,"DODM")
		FIELD_OLEDATE(Doem,"DOEM")
		FIELD_OLEDATE(Dobk,"DOBK")
		FIELD_CHAR_TRIM(Finm,"FINM")
		FIELD_CHAR_TRIM(Lanm,"LANM")
		FIELD_CHAR_TRIM(Makr,"MAKR")
		FIELD_CHAR_TRIM(Peno,"PENO")
		FIELD_CHAR_TRIM(Perc,"PERC")
		FIELD_CHAR_TRIM(Prfl,"PRFL")
		FIELD_CHAR_TRIM(Regi,"REGI")
		FIELD_CHAR_TRIM(Rema,"REMA")
		FIELD_CHAR_TRIM(Shnm,"SHNM")
		FIELD_CHAR_TRIM(Teld,"TELD")
		FIELD_CHAR_TRIM(Telh,"TELH")
		FIELD_CHAR_TRIM(Telp,"TELP")
		FIELD_CHAR_TRIM(Tohr,"TOHR")
		FIELD_CHAR_TRIM(Usec,"USEC")
		FIELD_CHAR_TRIM(Useu,"USEU")
		FIELD_DATE(Cdat,"CDAT")
		FIELD_DATE(Lstu,"LSTU")

	END_CEDARECINFO //(STFDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i = 0; i < sizeof(STFDataRecInfo)/sizeof(STFDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&STFDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"STF");
	strcat(pcmTableName,pcgTableExt);
	strcpy(pcmListOfFields,"URNO,DODM,DOEM,DOBK,FINM,LANM,MAKR,PENO,PERC,PRFL,REGI,REMA,SHNM,TELD,TELH,TELP,TOHR,USEC,USEU,CDAT,LSTU");

	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	Register();
	omData.RemoveAll();
}


//--REGISTER----------------------------------------------------------------------------------------------

void CedaStfData::Register(void)
{
	DdxRegister((void *)this,BC_STF_CHANGE,	"STFDATA", "Stf-changed",	ProcessStfCf);
	DdxRegister((void *)this,BC_STF_NEW,	"STFDATA", "Stf-new",		ProcessStfCf);
	DdxRegister((void *)this,BC_STF_DELETE,	"STFDATA", "Stf-deleted",	ProcessStfCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaStfData::~CedaStfData(void)
{
	TRACE("CedaStfData::~CedaStfData called\n");
	ogDdx.UnRegister(this,NOTUSED);
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaStfData::ClearAll(bool bpWithRegistration)
{
	TRACE("CedaStfData::ClearAll called\n");
	omPenoMap.RemoveAll();
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaStfData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(pspWhere == NULL)
	{	
		ilRc = CedaAction2("RT");
	}
	else
	{
		ilRc = CedaAction2("RT", pspWhere);
	}
	if (ilRc != true)
	{
		TRACE("Read-Stf: Ceda-Error %d \n",ilRc);
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
	{
		STFDATA *prlStf = new STFDATA;
		if ((ilRc = GetFirstBufferRecord2(prlStf)) == true)
		{
			omData.Add(prlStf);//Update omData
			omUrnoMap.SetAt((void *)prlStf->Urno,prlStf);
			CString olTemp;
			olTemp.Format("%s",prlStf->Peno);
			omPenoMap.SetAt(olTemp,prlStf);
#ifdef TRACE_FULL_DATA
			// Datensatz OK, loggen if FULL
			ogRosteringLogText.Format(" read %8.8lu %s OK:  ", ilCountRecord, pcmTableName);
			GetDataFormatted(ogRosteringLogText, prlStf);
			WriteLogFull("");
#endif TRACE_FULL_DATA
		}
		else
		{
			delete prlStf;
		}
	}
	TRACE("Read-Stf: %d gelesen\n",ilCountRecord-1);
    		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  read %d records from table %s",ilCountRecord-1, pcmTableName);
		WriteInRosteringLog();
	}

	ClearFastSocketBuffer();	
	return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaStfData::Insert(STFDATA *prpStf)
{
	prpStf->IsChanged = DATA_NEW;
	if(Save(prpStf) == false) return false; //Update Database
	InsertInternal(prpStf);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaStfData::InsertInternal(STFDATA *prpStf)
{
	ogDdx.DataChanged((void *)this, STF_NEW,(void *)prpStf ); //Update Viewer
	omData.Add(prpStf);//Update omData
	omUrnoMap.SetAt((void *)prpStf->Urno,prpStf);

	CString olTemp;
	olTemp.Format("%s",prpStf->Peno);
	omPenoMap.SetAt(olTemp,prpStf);

    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaStfData::Delete(long lpUrno)
{
	STFDATA *prlStf = GetStfByUrno(lpUrno);
	if (prlStf != NULL)
	{
		prlStf->IsChanged = DATA_DELETED;
		if(Save(prlStf) == false) return false; //Update Database
		DeleteInternal(prlStf);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaStfData::DeleteInternal(STFDATA *prpStf)
{
	ogDdx.DataChanged((void *)this,STF_DELETE,(void *)prpStf); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpStf->Urno);

	CString olTemp;
	olTemp.Format("%s",prpStf->Peno);
	omPenoMap.RemoveKey(olTemp);

	int ilStfCount = omData.GetSize();
	for (int ilCountRecord = 0; ilCountRecord < ilStfCount; ilCountRecord++)
	{
		if (omData[ilCountRecord].Urno == prpStf->Urno)
		{
			omData.DeleteAt(ilCountRecord);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaStfData::Update(STFDATA *prpStf)
{
	if (GetStfByUrno(prpStf->Urno) != NULL)
	{
		if (prpStf->IsChanged == DATA_UNCHANGED)
		{
			prpStf->IsChanged = DATA_CHANGED;
		}
		if(Save(prpStf) == false) return false; //Update Database
		UpdateInternal(prpStf);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaStfData::UpdateInternal(STFDATA *prpStf)
{
	STFDATA *prlStf = GetStfByUrno(prpStf->Urno);
	if (prlStf != NULL)
	{
		*prlStf = *prpStf; //Update omData
		ogDdx.DataChanged((void *)this,STF_CHANGE,(void *)prlStf); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

STFDATA *CedaStfData::GetStfByUrno(long lpUrno)
{
	STFDATA  *prlStf;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlStf) == TRUE)
	{
		return prlStf;
	}
	return NULL;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

STFDATA *CedaStfData::GetStfByPeno(CString opPeno)
{

	STFDATA  *prlStf;
	if (omPenoMap.Lookup(opPeno,(void *& )prlStf) == TRUE)
	{
		return prlStf;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaStfData::ReadSpecialData(CCSPtrArray<STFDATA> *popStf,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[2048] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	else
	{
		strcpy(pclFieldList, pcmListOfFields);
	}
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction2("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popStf != NULL)
	{
		for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
		{
			STFDATA *prpStf = new STFDATA;
			if ((ilRc = GetBufferRecord2(ilCountRecord,prpStf,CString(pclFieldList))) == true)
			{
				popStf->Add(prpStf);
			}
			else
			{
				delete prpStf;
			}
		}
		ClearFastSocketBuffer();	
		if(popStf->GetSize() == 0) return false;
	}
	ClearFastSocketBuffer();	
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaStfData::Save(STFDATA *prpStf)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpStf->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpStf->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpStf);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpStf->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpStf->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpStf);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpStf->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpStf->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
	TRACE("Stf-IRT/URT/DRT: Ceda-Return %d \n",ilRc);
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessStfCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogStfData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaStfData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlStfData;
	prlStfData = (struct BcStruct *) vpDataPointer;
	CString olSelection = (CString)prlStfData->Selection;
	STFDATA *prlStf;
	
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("ProcessBC:\n  Cmd <%s>\n  Tbl <%s>\n  Twe <%s>\n  Sel <%s>\n  Fld <%s>\n  Dat <%s>\n  Bcn <%s>",prlStfData->Cmd, prlStfData->Object, prlStfData->Twe, prlStfData->Selection, prlStfData->Fields, prlStfData->Data,prlStfData->BcNum);
		WriteInRosteringLog();
	}
	
	switch(ipDDXType)
	{
	default:
		break;
	case BC_STF_CHANGE:
		{
			long llUrno;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlStfData->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			//		long llUrno = GetUrnoFromSelection(prlStfData->Selection);
			prlStf = GetStfByUrno(llUrno);
			if(prlStf != NULL)
			{
				GetRecordFromItemList(prlStf,prlStfData->Fields,prlStfData->Data);
				UpdateInternal(prlStf);
				break;
			}
		}
	case BC_STF_NEW:
		prlStf = new STFDATA;
		GetRecordFromItemList(prlStf,prlStfData->Fields,prlStfData->Data);
		InsertInternal(prlStf);
		break;
	case BC_STF_DELETE:
		{
			long llUrno;
			CString olSelection = (CString)prlStfData->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlStfData->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			prlStf = GetStfByUrno(llUrno);
			if (prlStf != NULL)
			{
				DeleteInternal(prlStf);
			}
		}
		break;
	}
}

//***************************************************************************************
// Pr�fen ob der Mitarbeiter zu diesem Zeitpunkt noch im Unternehmen ist.
//***************************************************************************************

bool CedaStfData::IsStfNowValid(long lpUrno, COleDateTime opDay)	
{
	bool blResult;

	STFDATA *polStf = GetStfByUrno(lpUrno);
	if (polStf != NULL)
	{
		COleDateTime olDodm;
		olDodm.SetStatus(COleDateTime::null);
		if(polStf->Dodm.GetStatus() == COleDateTime::valid)
		{
			// DODM ist der letzte Arbeitstag!!
			olDodm = polStf->Dodm + COleDateTimeSpan(1,0,0,0);
		}

		CString olVpto = olDodm.Format("%Y%m%d");
		CString olVpfr = polStf->Doem.Format("%Y%m%d");
		if(olVpto == "") olVpto = "99999999";
		if(olVpfr == "") olVpfr = "00000000";

		CString olActuallDay = opDay.Format("%Y%m%d");

		if(olActuallDay < olVpfr || olActuallDay >= olVpto)
			// nicht im Unternehmen
			blResult = false;
		else
			blResult = true;
	}
	else
		blResult = false;

	return blResult;
}

/********************************************************************************
liefert Namen in der Form gem. der View-Einstellung zur�ck
lpUrno - Mitarbeiter URNO
ipEName - Einstellung der View
********************************************************************************/
CString CedaStfData::GetName(long lpUrno, int ipEName /*= 1*/)
{
	CString olName("");
	STFDATA* polStf = GetStfByUrno(lpUrno);
	if(!polStf) return olName;

	olName = CBasicData::GetFormatedEmployeeName(ipEName, polStf, "", "");

	return olName;
}