// ExcelExport.cpp: implementation of the ExcelExport class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

ExcelExport::ExcelExport(CWnd* pParent /*=NULL*/)
{
	pomParent = (DutyRoster_View*)pParent;
	prmViewInfo = &pomParent->rmViewInfo;
	pomGroups = &pomParent->omGroups;
	pomStfData  = &pomParent->omStfData;
}

ExcelExport::~ExcelExport()
{

}

bool ExcelExport::CreateExportViewData()
{
	COleDateTime olDayTmp;
	ofstream of;

	of.open(omExcelFileName, ios::out);
	of  << setw(0) << omViewName << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;

	int i;
	int j;
	int k;

	//
	// writing header captions into file
	//
	int ilEmployeeCols = 2;
	int ilAccountCols = prmViewInfo->oAccounts.GetSize();
	for (i = 0; i < ilEmployeeCols; i++)
	{
		of	<< setw(0) << GetEmployeeCaption(i) 
			<< setw(0) << omTrenner;
	}
	for (olDayTmp = prmViewInfo->oDateFrom; olDayTmp <= prmViewInfo->oDateTo; olDayTmp += COleDateTimeSpan(1,0,0,0))
	{
		of	<< setw(0) << GetDayCaption(olDayTmp)
			<< setw(0) << omTrenner;
	}
	for (i = 0; i < ilAccountCols; i++)
	{
		of	<< setw(0) << GetAccountCaption(i)
			<< setw(0) << omTrenner;
	}
	of << endl;

	//
	// writing cell text into file
	//
	CStringArray olLevels;
	int ilEmployeeCount = pomParent->rmViewInfo.oStfUrnos.GetSize();
	int ilLevelCount = ExtractItemList(prmViewInfo->oPType, &olLevels, ',');
	for (i = 0; i < ilEmployeeCount; i++)
	{
		for (j = 0; j < ilLevelCount; j++)
		{
			for (k = 0; k < ilEmployeeCols; k++)
			{
				of	<< setw(0) << GetEmployeeCellText(k,i) 
					<< setw(0) << omTrenner;
			}
			for (olDayTmp = prmViewInfo->oDateFrom; olDayTmp <= prmViewInfo->oDateTo; olDayTmp += COleDateTimeSpan(1,0,0,0))
			{
				of	<< setw(0) << GetDayCellText(olDayTmp, i, olLevels[j])
					<< setw(0) << omTrenner;
			}
			if (j == 0) //writing account info only in the first line of the employee
			{
				for (k = 0; k < ilAccountCols; k++)
				{
					of	<< setw(0) << GetAccountCellText(prmViewInfo->oDateFrom, prmViewInfo->oDateTo, i, k)
						<< setw(0) << omTrenner;
				}
			}
			of << endl;
		}
	}

	//
	// writing statistic-section (debit) into file
	//
	int ilDebitRows = pomParent->pomDebitTabViewer->omDDTV_Shifts.GetSize();
	int ilDebitCols = ((COleDateTimeSpan)(prmViewInfo->oDateTo - prmViewInfo->oDateFrom)).GetDays() + 1;

	for (i = 0; i < ilDebitRows; i++)
	{
		for (j = 0; j < ilDebitCols; j++)
		{
			if (j < pomParent->pomDebitTabViewer->omDDTV_Shifts[i].omDayDebitShift.GetSize())
			{
				of	<< setw(0) << GetDebitCellText(i, j)
					<< setw(0) << omTrenner;
			}
		}
		of << endl;
	}

	of.close();
	return true;
}

CString ExcelExport::GetDayCaption(COleDateTime &opDay)
{
	CString olRet = "";

	if (opDay.GetStatus() != COleDateTime::valid)
	{
		return olRet;
	}

	int ilDayOfWeek = opDay.GetDayOfWeek();

	switch (ilDayOfWeek)
	{
		case 1:
			olRet = LoadStg(DUTY_SUN);
			break;
		case 2:
			olRet = LoadStg(DUTY_MON);
			break;
		case 3:
			olRet = LoadStg(DUTY_TUE);
			break;
		case 4:
			olRet = LoadStg(DUTY_WED);
			break;
		case 5:
			olRet = LoadStg(DUTY_THU);
			break;
		case 6:
			olRet = LoadStg(DUTY_FRI);
			break;
		case 7:
			olRet = LoadStg(DUTY_SAT);
			break;
	}
	olRet += opDay.Format(" %d.%m.");

	return olRet;
}

CString ExcelExport::GetEmployeeCaption(int ipColNo)
{
	CString olRet;

	switch(ipColNo)
	{
		case 0:
			if(prmViewInfo->iGroup  == 2)
			{
				olRet = CString(" ") + LoadStg(IDS_STRING1692); // Group/Function/Prio.
			}
			else if(prmViewInfo->iGroup  == 3)
			{
				olRet = CString(" ") + LoadStg(PR_DSR_CARPOOL); // CarPool.
			}
			else
			{
				olRet.Format(LoadStg(IDS_STRING197), pomParent->rmViewInfo.oStfUrnos.GetSize()); // "Total: %d*INTXT*"
			}
			break;
		case 1:
			if(prmViewInfo->iEName == 1) //Code: "Initials*INTXT*"
			{
				olRet = CString(" ") + LoadStg(SHIFT_SHEET_C_CODE);
			}
			else if(prmViewInfo->iEName == 3) //Shortname: "Nickname*INTXT*"
			{
				olRet = CString(" ") + LoadStg(IDS_STRING1807);
			}
			else if(prmViewInfo->iEName == 2 || prmViewInfo->iEName == 4) // "Employees*INTXT*"
			{
				olRet = CString(" ") + LoadStg(SHIFT_S_STAFF);
			}
			else
			{
				olRet = CString(""); //No Text >> No Group or CarPool.
			}
			break;
		default:
			olRet =  CString("");
			break;
	}
	return olRet;
}

CString ExcelExport::GetAccountCaption(int ipAccountType)
{
	CString olRet = "";

	ACCOUNTTYPEINFO *prlAccountType = pomParent->pomAccounts->GetAccountTypeInfoByAccountType(prmViewInfo->oAccounts[ipAccountType]);

	if (prlAccountType != NULL)
	{
		olRet = prlAccountType->oShortName;
	}
	return olRet;
}

CString ExcelExport::GetEmployeeCellText(int ipColNo, int ipStfUrnoNo)
{
	CString olRet;
	CString olTmp;

	switch (ipColNo)
	{
		case 0:
			if (prmViewInfo->iGroup == 2)
			{
				if (ipStfUrnoNo < pomGroups->GetSize())
				{
					olRet = " ";
					olRet += (*pomGroups)[ipStfUrnoNo].oWgpCode;

					if(!(*pomGroups)[ipStfUrnoNo].oWgpCode.IsEmpty() && !(*pomGroups)[ipStfUrnoNo].oPfcCode.IsEmpty())
					{
						olRet += "/"; // wenn Gruppe und Funktion nicht leer sind, ist ein Separator notwendig
					}
					olRet += (*pomGroups)[ipStfUrnoNo].oPfcCode;
				}
			}
			else
			{
				if (ipStfUrnoNo < pomStfData->GetSize())
				{
					long llStfu = (*pomStfData)[ipStfUrnoNo].Urno;
					// Funktionscode und Vertragscode und Wochenarbeitszeit
					CString olActualCot = "";
					CString olFunctionCode = "";
					CString olHoursPerWeek = "";

					CCSPtrArray<SPFDATA> olSpfData;
					int ilSpfCnt = ogSpfData.GetSpfArrayBySurnWithTime(olSpfData, llStfu, prmViewInfo->oSelectDate, prmViewInfo->bCheckMainFuncOnly);
					if (ilSpfCnt > 0)
					{
						olFunctionCode = olSpfData[0].Fctc;
					}

					// Vertragscode & Wochenarbeitszeit
					olActualCot = ogScoData.GetCotAndCWEHBySurnWithTime(llStfu, prmViewInfo->oSelectDate, &olHoursPerWeek);
					if (strlen(olHoursPerWeek) == 0)
					{
						COTDATA *prlCotData = ogCotData.GetCotByCtrc(olActualCot);
						if (prlCotData != NULL)
						{
							olHoursPerWeek = prlCotData->Whpw;
						}
					}
					if (strlen(olHoursPerWeek) > 2)
					{
						olHoursPerWeek.Insert(2,":");
					}
					olRet = olFunctionCode + " / " + olActualCot + " / " + olHoursPerWeek; 
				}
			}
			break;
		case 1:
			if (prmViewInfo->iGroup == 2)
			{
				if(ipStfUrnoNo < pomGroups->GetSize())
				{
					if ((*pomGroups)[ipStfUrnoNo].lStfUrno != 0)
					{
						if (prmViewInfo->iEName == 1) //Code/Initials
						{
							olRet.Format(" %s",(*pomGroups)[ipStfUrnoNo].oStfPerc);
						}
						else if (prmViewInfo->iEName == 2) //Name
						{
							CString olVName,olNName;
							olVName = (*pomGroups)[ipStfUrnoNo].oStfFinm;
							olNName = (*pomGroups)[ipStfUrnoNo].oStfLanm;
							if (olVName.IsEmpty() == FALSE || olNName.IsEmpty() == FALSE)
							{
								olRet.Format(" %s.%s",olVName.Left(1),olNName);
							}
						}
						else if (prmViewInfo->iEName == 3) //Shortname/Nickname
						{
							olRet.Format(" %s",(*pomGroups)[ipStfUrnoNo].oStfShnm);
						}
						else if (prmViewInfo->iEName == 4) //free configurated
						{
							olRet = " " + pogNameConfigurationDlg->GetNameString(
								(*pomGroups)[ipStfUrnoNo].oStfFinm.GetBuffer(0),
								(*pomGroups)[ipStfUrnoNo].oStfLanm.GetBuffer(0),
								(*pomGroups)[ipStfUrnoNo].oStfShnm.GetBuffer(0),
								(*pomGroups)[ipStfUrnoNo].oStfPerc.GetBuffer(0),
								(*pomGroups)[ipStfUrnoNo].oStfPeno.GetBuffer(0));
						}
					}
				}
			}
			else
			{
				if (ipStfUrnoNo < pomStfData->GetSize())
				{
					olRet = CBasicData::GetFormatedEmployeeName(prmViewInfo->iEName, &(*pomStfData)[ipStfUrnoNo], " ", "");
				}
			}

			break;
		default:
			olRet = "";
			break;
	}
	return olRet;
}

CString ExcelExport::GetDayCellText(COleDateTime &opDay, int ipStfUrnoNo, CString opRosl)
{
	CString olRet = "";

	if (opDay.GetStatus() != COleDateTime::valid)
	{
		return olRet;
	}

	long llStfUrno;
	CString olSday = opDay.Format("%Y%m%d");

	if (prmViewInfo->iGroup == 2)
	{
		llStfUrno = (*pomGroups)[ipStfUrnoNo].lStfUrno;
	}
	else
	{
		llStfUrno = (*pomStfData)[ipStfUrnoNo].Urno;
	}

	DRRDATA *prlDrr1 = ogDrrData.GetDrrByKey (olSday, llStfUrno, "1", opRosl);
	DRRDATA *prlDrr2 = ogDrrData.GetDrrByKey (olSday, llStfUrno, "2", opRosl);

	if (prlDrr1 != NULL)
	{
		if (prlDrr2 != NULL)
		{
			olRet = CString(prlDrr1->Scod) + "/" + CString(prlDrr2->Scod);
		}
		else
		{
			olRet = prlDrr1->Scod;
		}
	}
	else
	{
		olRet = ogCCSParam.GetParamValue(ogAppl,"ID_BLANK_CHAR");
	}
	return olRet;
}

CString ExcelExport::GetAccountCellText(COleDateTime &opFrom, COleDateTime &opTo, int ipStfUrnoNo, int ipAccColNo)
{
	CString olRet = "";
	long llStfUrno;

	if (prmViewInfo->iGroup == 2)
	{
		llStfUrno = (*pomGroups)[ipStfUrnoNo].lStfUrno;
	}
	else
	{
		llStfUrno = (*pomStfData)[ipStfUrnoNo].Urno;
	}

	ACCOUNTRETURNSTRUCT rlAccountReturnStruct = pomParent->pomAccounts->GetAccountByStaffAndMonth(prmViewInfo->oAccounts[ipAccColNo],llStfUrno,prmViewInfo->oDateFrom,prmViewInfo->oDateTo);

	CString olFormat;
	int ilFormat = atoi(rlAccountReturnStruct.oFormat);

	if (ilFormat < 0 || ilFormat > 10)
	{
		olFormat = "2";
	}
	else
	{
		olFormat.Format("%d",ilFormat);
	}

	if (rlAccountReturnStruct.oValueDefinition == "STRINGorSINGEL")
	{
		olRet = rlAccountReturnStruct.oString.Left(6);
	}
	else
	{
		olRet.Format("%0."+olFormat+"f", rlAccountReturnStruct.dDouble);
	}	
	return olRet;
}

CString ExcelExport::GetDebitCellText(int ilLineNo, int ilDayNo)
{
	CString olRet = "";
	CString olTmp = "";

	DayDebitShift *prlDayDebitshift = (DayDebitShift*)pomParent->pomDebitTabViewer->omDDTV_Shifts[ilLineNo].omDayDebitShift[ilDayNo];

	if (ilDayNo == 0)
	{
		if (ilLineNo == 0)
		{
			olRet = CString(" ") + LoadStg(IDS_STRING1553); //only first line of first column
		}
		else
		{
			olRet = pomParent->pomDebitTabViewer->omDDTV_Shifts[ilLineNo].ShiftText;
		}
		olRet += omTrenner + omTrenner;
	}

	int ilShift = prlDayDebitshift->ShiftCount;
	int ilDebit = prlDayDebitshift->DebitCount;

	CString olType = pomParent->pomDebitTabViewer->omDDTV_Shifts[ilLineNo].DataType;

	if (olType == "1")
	{
		int ilValue = pomParent->pomDebitTabViewer->GetFreeShiftValue(ilShift,ilDayNo);
		ilValue = ilValue - ilDebit;
		olTmp.Format("%d", ilValue);
	}
	else
	{
		if (ilShift == 0 && ilDebit == 0)
		{
			if (ilLineNo != 0)
			{
				olTmp = "-";
			}
		}
		else
		{
			if (prmViewInfo->iDCoverage == 1)
			{
				olTmp.Format("%d/%d", ilShift, ilDebit);
			}
			else
			{
				olTmp.Format("%+d", ilShift - ilDebit);
			}
		}
	}

	return olRet + olTmp;
}


void ExcelExport::Export(CString opExportName)
{
	char pclConfigPath[256];
	char pclExcelPath[256];
	char pclTrenner[64];

	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "Excel", "DEFAULT",pclExcelPath, sizeof pclExcelPath, pclConfigPath);

	GetPrivateProfileString(ogAppName, "ExcelSeparator", ";",pclTrenner, sizeof pclTrenner, pclConfigPath);

	if (!strcmp(pclExcelPath, "DEFAULT"))
	{
		AfxMessageBox(LoadStg(IDS_STRING154), MB_ICONERROR);
		return;
	}

	CWaitCursor olDummy;
	omTrenner = pclTrenner;
	omExcelFileName = CreateFileName();

	if (opExportName == "VIEW_COMPLETE")
	{
		CreateExportViewData();
	}
	else if (opExportName == "VIEW_BLANK")
	{
		CreateExportBlankShifts();
	}
	else if (opExportName == "VIEW_ABSENCE_X")
	{
		CreateExportXAbsences();
	}

	// testing error
	if (omExcelFileName.IsEmpty() || omExcelFileName.GetLength() > 255)
	{
		CString olMsg;
		if (omExcelFileName.IsEmpty())
		{
			CString olMsg = "Filename is empty !" + omExcelFileName;
			AfxMessageBox(olMsg, MB_ICONERROR);
			return;
		}
		if (omExcelFileName.GetLength() > 255 )
		{
			CString olMsg = "Length of the filename > 255: " + omExcelFileName;
			AfxMessageBox(olMsg,  MB_ICONERROR);
			return;
		}

		olMsg = "Filename invalid: " + omExcelFileName;
		AfxMessageBox(olMsg, MB_ICONERROR);
		return;
	}

	char pclTmp[256];
	strcpy(pclTmp, omExcelFileName); 

	// Set up parameters to be sent:
	char *args[4];
	args[0] = "child";
	args[1] = pclTmp;
	args[2] = NULL;
	args[3] = NULL;

	_spawnv( _P_NOWAIT , pclExcelPath, args );
}

CString ExcelExport::CreateFileName()
{
	CString olRet;
	char* tmpvar;
	tmpvar = getenv("TMP");
	if (tmpvar == NULL)
		tmpvar = getenv("TEMP");

	if (tmpvar == NULL)
		return "";

	CString olExcelFileName;

	omViewName.Format ("DutyRoster %s", pomParent->GetSelectedView()); // getting the title (name of the view)
	olExcelFileName.Format("%s %s", omViewName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );

	olExcelFileName.Remove('*');
	olExcelFileName.Remove('.');
	olExcelFileName.Remove(':');
	olExcelFileName.Remove('/');
	olExcelFileName.Replace(" ", "_");

	char pHeader[256];
	strcpy (pHeader, tmpvar);
	CString path = pHeader;
	olRet =  path + "\\" + olExcelFileName + ".csv";
	return olRet;
}

bool ExcelExport::CreateExportBlankShifts()
{
	COleDateTime olDayTmp;
	CStringArray olArr;

	CString olTmp;
	CString olSday;
	CString olName;
	CString olFunction;

	ofstream of;
	of.open(omExcelFileName, ios::out);

	//
	// writing header captions into file
	//
	of << setw(0) << "View/Date/Time of Creation" << setw(0) << omTrenner << omTrenner
	   << setw(0) << "Report Name" << setw(0) << omTrenner << omTrenner
	   << setw(0) << "System/Program" << setw(0)
	   << endl;

	of << setw(0) << omViewName << CTime::GetCurrentTime().Format("/%d.%m.%Y/%H:%M") << setw(0) << omTrenner << omTrenner
	   << setw(0) << "Staff not assigned to shifts" << setw(0) << omTrenner << omTrenner
	   << setw(0) << "SAMS/Rostering" << setw(0) 
	   << endl
	   << endl;

	of << setw(0) << "Period requested: " << setw(0) << prmViewInfo->oDateFrom.Format("%d-%m-%Y") << setw(0)
	   << setw(0) << " to " << setw(0) << prmViewInfo->oDateTo.Format("%d-%m-%Y") << setw(0)
	   << omTrenner << omTrenner << omTrenner << omTrenner
	   << setw(0) << "UserID : " << setw(0) << pcgUser << setw(0)
	   << endl
	   << endl;

	of << setw(0) << "Employee ID" << setw(0) << omTrenner  << setw(0)
	   << setw(0) << "Employee Name" << setw(0) << omTrenner  << setw(0)
	   << setw(0) << "Employee function code" << setw(0) << omTrenner  << setw(0)
	   << setw(0) << "Date" << setw(0) << omTrenner  << setw(0)
	   << endl
	   << endl;

	//
	// look for empty, active DRR-records and export them
	//
	STFDATA *prlStfData = NULL;
	DRRDATA *prlDrrData = NULL;

	int ilEmployeeCount = prmViewInfo->oStfUrnos.GetSize();
	for (int i = 0; i < ilEmployeeCount; i++)
	{
		prlStfData = ogStfData.GetStfByUrno(atol(prmViewInfo->oStfUrnos[i]));
		if (prlStfData != NULL)
		{
			for (olDayTmp = prmViewInfo->oDateFrom; olDayTmp <= prmViewInfo->oDateTo; olDayTmp += COleDateTimeSpan(1,0,0,0))
			{
				olSday = olDayTmp.Format("%Y%m%d");
				prlDrrData = ogDrrData.GetActiveDrr(olSday, prlStfData->Urno);
				if (prlDrrData != NULL && strlen(prlDrrData->Scod) == 0)
				{
					olSday = olDayTmp.Format("%d-%m-%Y");
					olName = CBasicData::GetFormatedEmployeeName(prmViewInfo->iEName, prlStfData, " ", "");
					olFunction = ogSpfData.GetMainPfcBySurnWithTime (prlStfData->Urno, olDayTmp);

					of << setw(0) << prlStfData->Peno << setw(0) << omTrenner  << setw(0)
					   << setw(0) << olName << setw(0) << omTrenner  << setw(0)
					   << setw(0) << olFunction << setw(0) << omTrenner  << setw(0)
					   << setw(0) << olSday << setw(0) << omTrenner  << setw(0)
					   << endl;
				}
			}
		}
	}

	of.close();
	return true;
}

bool ExcelExport::CreateExportXAbsences()
{
	COleDateTime olDayTmp;
	CStringArray olArr;
	int ilEmployeeCount;

	CString olTmp;
	CString olSday;
	CString olScod;
	CString olName;
	CString olFunction;

	ofstream of;
	of.open(omExcelFileName, ios::out);

	//
	// writing header captions into file
	//
	of << setw(0) << "View/Date/Time of Creation" << setw(0) << omTrenner << omTrenner
	   << setw(0) << "Report Name" << setw(0) << omTrenner << omTrenner
	   << setw(0) << "System/Program" << setw(0)
	   << endl;

	of << setw(0) << omViewName << CTime::GetCurrentTime().Format("/%d.%m.%Y/%H:%M") << setw(0) << omTrenner << omTrenner
	   << setw(0) << "Staff absences entered via SAMS" << setw(0) << omTrenner << omTrenner
	   << setw(0) << "SAMS/Rostering" << setw(0) 
	   << endl
	   << endl;

	of << setw(0) << "Period requested: " << setw(0) << prmViewInfo->oDateFrom.Format("%d-%m-%Y") << setw(0)
	   << setw(0) << " to " << setw(0) << prmViewInfo->oDateTo.Format("%d-%m-%Y") << setw(0)
	   << omTrenner << omTrenner << omTrenner << omTrenner
	   << setw(0) << "UserID : " << setw(0) << pcgUser << setw(0)
	   << endl
	   << endl;

	of << setw(0) << "Employee ID" << setw(0) << omTrenner  << setw(0)
	   << setw(0) << "Employee Name" << setw(0) << omTrenner  << setw(0)
	   << setw(0) << "Employee function code" << setw(0) << omTrenner  << setw(0)
	   << setw(0) << "Absence code" << setw(0) << omTrenner  << setw(0)
	   << setw(0) << "Date" << setw(0) << omTrenner  << setw(0)
	   << endl
	   << endl;

	//
	// look for empty, active DRR-records and export them
	//
	STFDATA *prlStfData = NULL;
	DRRDATA *prlDrrData = NULL;
	ODADATA *prlOdaData = NULL;

	ilEmployeeCount = prmViewInfo->oStfUrnos.GetSize();
	for (int i = 0; i < ilEmployeeCount; i++)
	{
		prlStfData = ogStfData.GetStfByUrno(atol(prmViewInfo->oStfUrnos[i]));
		if (prlStfData != NULL)
		{
			for (olDayTmp = prmViewInfo->oDateFrom; olDayTmp <= prmViewInfo->oDateTo; olDayTmp += COleDateTimeSpan(1,0,0,0))
			{
				olSday = olDayTmp.Format("%Y%m%d");
				prlDrrData = ogDrrData.GetActiveDrr(olSday, prlStfData->Urno);
				if (prlDrrData != NULL)
				{
					prlOdaData = ogOdaData.GetOdaBySdac(prlDrrData->Scod);
					olScod = prlDrrData->Scod;
					if (prlOdaData != NULL && olScod.GetAt(olScod.GetLength()-1) == 'X')
					{
						olSday = olDayTmp.Format("%d-%m-%Y");
						olName = CBasicData::GetFormatedEmployeeName(prmViewInfo->iEName, prlStfData, " ", "");
						olFunction = ogSpfData.GetMainPfcBySurnWithTime (prlStfData->Urno, olDayTmp);

						of << setw(0) << prlStfData->Peno << setw(0) << omTrenner  << setw(0)
						   << setw(0) << olName << setw(0) << omTrenner  << setw(0)
						   << setw(0) << olFunction << setw(0) << omTrenner  << setw(0)
						   << setw(0) << prlDrrData->Scod << setw(0) << omTrenner  << setw(0)
						   << setw(0) << olSday << setw(0) << omTrenner  << setw(0)
						   << endl;
					}
				}
			}
		}
	}

	of.close();
	return true;
}
