// DutyRoster_View.cpp : implementation file
//

#include <stdafx.h>
#include <ufis.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//ARR_MAXSELECTLEN from aatarry.h / (additional_size + sizeof(urno) + sizeof(') * 2 + sizeof(,))							 
const int ARR_MAXSELECTLEN = (50*1024)/(128+10+2+1);

//****************************************************************************
//	Globale Funktionen zum Sortieren von Datens�tzen
//****************************************************************************

#ifdef _DEBUG
int giCallCompareStfByFctConName = 0;
int giCallSpf = 0;
int giCallSco = 0;
int giTimeSpf = 0;
int giTimeSco = 0;
#endif _DEBUG

static int CompareStfByFctConName( const STFDATA **e1, const STFDATA **e2);
static int CompareStfByName( const STFDATA **e1, const STFDATA **e2);
static int CompareTeaData( const TEADATA **e1, const TEADATA **e2);

DutyRoster_View *pcgView = NULL;

//****************************************************************************
//	Globale Callback-Funktion f�r den Broadcast-Handler
//****************************************************************************

void ProcessDutyRoster_ViewCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//****************************************************************************
//****************************************************************************
// DutyRoster_View
//****************************************************************************
//****************************************************************************

IMPLEMENT_DYNCREATE(DutyRoster_View, CFormView)

DutyRoster_View::DutyRoster_View(): CFormView(DutyRoster_View::IDD)
{
	//{{AFX_DATA_INIT(DutyRoster_View)
	// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	
	// Init Views
	InitDefaultView();
	
	pomDutyDragDropDlg	= NULL;
	pomAttentionDlg		= NULL;
	pomInfoBoxDlg		= NULL;
	
	pomSolidTable		= NULL;
	prmSolidIPEAttrib	= NULL;
	pomSolidTabViewer	= NULL;
	
	pomIsTable			= NULL;
	prmIsIPEAttrib		= NULL;
	pomIsTabViewer		= NULL;
	
	pomDebitTable		= NULL;
	prmDebitIPEAttrib	= NULL;
	pomDebitTabViewer	= NULL;
	
	pomStatTable		= NULL;
	prmStatIPEAttrib	= NULL;
	pomStatTabViewer	= NULL;
	
	bmLastSortDayly	= false;
	bmChangeDay     = false;
	bmNeedActualButton = false;
	bmReadUrnoList	= false;
	
	omMinTrackSize = (0,0);
	omMaxTrackSize = (SHRT_MAX,SHRT_MAX);
	
	CString omAccLoadedDateYFrom("");
	CString omAccLoadedDateYTo("");
	
	// Objekt zur Berechnung der Mitarbeiter Arbeitszeitkonten (AZK) initialisieren
	pomAccounts = new CAccounts(&ogDrrData,&ogAccData,&ogStfData,&ogBsdData,&ogOdaData,&ogScoData,&ogCotData);
}

//****************************************************************************
// DutyRoster_View destuctor
//****************************************************************************

DutyRoster_View::~DutyRoster_View()
{
	// Info-Dialog schliessen
	if (pomAttentionDlg != NULL)
		delete pomAttentionDlg;
	
	// vom Broadcasthandler abmelden
	ogDdx.UnRegister(this,NOTUSED);
	delete pomViewer;
	
	pomSolidTable->DragDropRevoke();
	pomSolidTable->DeleteTable();
	delete prmSolidIPEAttrib;
	delete pomSolidTabViewer;
	
	pomIsTable->DragDropRevoke();
	pomIsTable->DeleteTable();
	delete prmIsIPEAttrib;
	delete pomIsTabViewer;
	
	pomDebitTable->DeleteTable();
	delete prmDebitIPEAttrib;
	delete pomDebitTabViewer;
	
	pomStatTable->DeleteTable();
	delete prmStatIPEAttrib;
	delete pomStatTabViewer;
	
	omStfData.DeleteAll();
	omAttentionInfo.DeleteAll();
	
	omGroups.DeleteAll();
	
	ogAccData.ClearAll(true);
	ogSdtData.ClearAll();
	
	if (pomDutyDragDropDlg != NULL)
	{
		pomDutyDragDropDlg->InternalCancel();
		delete pomDutyDragDropDlg;
		pomDutyDragDropDlg = NULL;
	}

	// AZK-Objekt l�schen
	if (pomAccounts != NULL)
	{
		delete pomAccounts;
	}

	//view-registrierung l�schen
	if (rmViewInfo.bCorrectViewInfo == 1)
	{
		if (ogCCSParam.GetParamValue("GLOBAL","ID_ENABLE_ACC") == "Y")
		{
			CString olDELSTF_Urnos;
			CString olDELSTF_Year = rmViewInfo.oDateFrom.Format("%Y");

			//adding and deleting for the same year
			for (int i = 0; i < rmViewInfo.oStfUrnos.GetSize(); i++)
			{
				olDELSTF_Urnos += rmViewInfo.oStfUrnos[i] + ',';
			}

			if (ogCCSParam.GetParamValue("GLOBAL","ID_ENABLE_ACC") == "Y")
			{
				SendViewStfuToScbhdlCommand(olDELSTF_Urnos, CString ("DELSTF"), olDELSTF_Year);
			}

			/*
			CString olTmp = "ZRH,BSL,GVA";
			if (olTmp.Find (ogCCSParam.GetParamValue(ogAppl,"ID_PROD_CUSTOMER")) > -1)
			{
				// send the event
				SendViewStfuToScbhdlCommand(olDELSTF_Urnos, CString ("DELSTF"), olDELSTF_Year);
			}

			//closing the view, so let's refill the array
			SendCommand(CString("REFILL"), CString("XBS2"));
			*/
		}
	}
}

//****************************************************************************
// DXX
//****************************************************************************

void DutyRoster_View::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DutyRoster_View)
	//}}AFX_DATA_MAP
}

//****************************************************************************
// MessageMap
//****************************************************************************

BEGIN_MESSAGE_MAP(DutyRoster_View, CFormView)
//{{AFX_MSG_MAP(DutyRoster_View)
ON_BN_CLICKED(IDC_B_ACTUAL,				OnBActual)
ON_BN_CLICKED(IDC_B_ATTENTION,			OnBAttention)
ON_BN_CLICKED(IDC_B_DUTYRELEASE2,		OnBRelease)
ON_BN_CLICKED(IDC_B_VIEW,				OnBView)
ON_CBN_SELCHANGE(IDC_CB_VIEW,			OnSelchange_CB_View)
ON_MESSAGE(WM_DYNTABLE_HSCROLL,			HorzTableScroll)
ON_MESSAGE(WM_DYNTABLE_VSCROLL,			VertTableScroll)
ON_MESSAGE(WM_DYNTABLE_HMOVE,			HMoveTable)
ON_MESSAGE(WM_DYNTABLE_VMOVE,			VMoveTable)
ON_MESSAGE(WM_DYNTABLE_INLINE_UPDATE,	InlineUpdate)
ON_MESSAGE(WM_DYNTABLE_RBUTTONDOWN,		TableRButtonDown)
ON_MESSAGE(WM_DYNTABLE_LBUTTONDOWN,		TableLButtonDown)
ON_MESSAGE(WM_DYNTABLE_LBUTTONDBLCLK,	TableLButtonDblClk)
ON_MESSAGE(WM_DYNTABLE_LBUTTONUP,		TableLButtonUp)
ON_MESSAGE(WM_DYNTABLE_MOUSEMOVE,		TableMouseMove)
ON_MESSAGE(WM_DRAGOVER,					OnDragOver)
ON_COMMAND(MENUE_PLAN,					OnMenuPlan)
ON_COMMAND(MENUE_CREATE_DOUBLETOUR,		OnMenuCreateDoubleTour)
ON_COMMAND(MENUE_CANCEL_DOUBLETOUR,		OnMenuCancelDoubleTour)
ON_COMMAND(MENUE_CONFLICTCHECK,			OnMenuConflictCheck)
ON_COMMAND(MENUE_CONFLICTCHECKALL,		OnMenuConflictCheckAll)
ON_COMMAND(MENUE_OPENDRR,				OnMenuOpenDRR)
ON_COMMAND(MENUE_OPENACCOUNT1,			OnMenuOpenAccount1)
ON_COMMAND(MENUE_OPENACCOUNT2,			OnMenuOpenAccount2)
ON_WM_CREATE()
ON_COMMAND(ID_SELECT_MA,				OnSelectMa)
ON_WM_SIZE()
ON_WM_CLOSE()
ON_WM_DESTROY()
ON_COMMAND(MENUE_WISH,					OnMenuWish)
ON_COMMAND(MENUE_DELETE_WISH,			OnMenuDeleteWish)
ON_COMMAND(MENUE_ADD_WISH,				OnMenuWish)
ON_COMMAND(MENUE_SHOW_WISH,				OnMenuShowWish)
ON_COMMAND(MENUE_DELETE_ABSENCE,		OnMenuDeleteAbsence)
ON_COMMAND(MENUE_CLEAR_DRR,				OnMenuClearDrr)
ON_COMMAND(MENUE_CHANGE_SHIFT,			OnMenuChangeShift)
ON_BN_CLICKED(ID_DUTY_PRINT,			OnBPrint)
ON_BN_CLICKED(ID_EXCEL,					OnBExcel)
ON_BN_CLICKED(ID_DUTY_CANCEL,			OnCancel)
ON_CBN_SELCHANGE(IDW_COMBO,				OnSelchange_CB_View)
ON_WM_GETMINMAXINFO()
ON_WM_MOUSEMOVE()
ON_WM_LBUTTONDBLCLK()
//}}AFX_MSG_MAP
ON_COMMAND(MENUE_ADD_DRR,				OnMenuAddDrr)
ON_COMMAND_RANGE(MENUE_EDIT_DRR, MENUE_EDIT_DRR+MAX_DRR_DRRN, OnMenuEditDrr)
ON_COMMAND_RANGE(MENUE_DELETE_DRR, MENUE_DELETE_DRR+MAX_DRR_DRRN, OnMenuDeleteDrr)
ON_COMMAND(MENUE_PRINTDAILYROSTER,		OnMenuPrintDailyRoster)
END_MESSAGE_MAP()

//****************************************************************************
// _DEBUG

#ifdef _DEBUG
void DutyRoster_View::AssertValid() const
{
	CFormView::AssertValid();
}

void DutyRoster_View::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

//****************************************************************************
// PreCreateWindow(CREATESTRUCT& cs) 
//****************************************************************************

BOOL DutyRoster_View::PreCreateWindow(CREATESTRUCT& cs) 
{
	return CFormView::PreCreateWindow(cs);
}

//****************************************************************************
// FillComboBox: f�llt die ComboBox in der Toolbar mit den zur Auswahl
//	stehenden Ansichten (Ansicht = gespeicherter Satz von Filtern zur
//	Anzeige bestimmter Mitarbeiter).
// R�ckgabe: keine
//****************************************************************************

void DutyRoster_View::FillComboBox() 
{
	CCS_TRY;
	// ComboBox-Objekt generieren
	CComboBox* polComboBox = ((DutyRoster_Frm*) GetParentFrame())->GetComboBox();
	
	// Inhalt der Combobox l�schen.
	polComboBox->ResetContent();
	
	// Ansichten aus Viewer laden...
	CStringArray olStrArr;
	pomViewer->GetViews(olStrArr);
	// ...und der Combobox hinzuf�gen
	for(int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
	{
		polComboBox->AddString(olStrArr[ilIndex]);
	}
	// per Default die Ansicht <Default> suchen...
	ilIndex = polComboBox->FindString(-1,LoadStg(IDS_STRING562));
	if(ilIndex != CB_ERR)
	{
		// ...und ausw�hlen (wenn gefunden)
		polComboBox->SetCurSel(ilIndex);
		pomViewer->SelectView(LoadStg(IDS_STRING562));
		omOldViewName = LoadStg(IDS_STRING562);
	}
	
	CCS_CATCH_ALL;
}

//****************************************************************************
// OnCreate: erzeugt die View.
// R�ckgabe:	-1	->	Fehler
//				0	->	Erfolg
//****************************************************************************

int DutyRoster_View::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	return 0;
}

//********************************************************************************
// CalcInitialWindowSizeAndPos: Gr��e und Position des Windows beim ersten 
//	Aufruf berechnen.
//	R�ckgabe:	CRect	-> das Fenster-Rechteck ohne Rahmenlinien
//********************************************************************************

CRect DutyRoster_View::CalcInitialWindowSizeAndPos() 
{
	CCS_TRY;
	// Fenster Rect
	CRect	olWindowRect;
	CRect	olMainFrameRect;
	
	int ilCXMonitor = 0, ilWindowWidth = 0, ilLeft = 0, ilRight = 0;
	
	// Einlesen des Client Bereiches des Hauptframes.(Mainframe)
	GetParentFrame()->GetParentFrame()->GetClientRect(olMainFrameRect);
	
	olWindowRect = olMainFrameRect;
	
	// Auf welchem Monitor soll das Fenster dargestellt werden, relevant falls Anzahl > 1.
	int ilWhichMonitor	= ogCfgData.GetMonitorForWindow(CString(MON_DUTYROSTER_STRING));
	// Anzahl der Monitore im System
	int ilMonitors		= ogCfgData.GetMonitorForWindow(CString(MON_COUNT_STRING));
	
	// Breite eines Monitors.
	ilWindowWidth = (int)((olMainFrameRect.right - olMainFrameRect.left)/ilMonitors);
	
	// ben�tig f�r die Berechnung der maximalen Gr��e des Fensters
	omMaxTrackSize = CPoint(olMainFrameRect.right+10, olMainFrameRect.bottom+10);
	
	// Fenster auf Monitor 1 darstellen
	if(ilWhichMonitor == 1)
	{
		// Anzahl der Monitore im System
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = olMainFrameRect.right;
			ilLeft = (int)((ilCXMonitor-ilWindowWidth)/2);
			ilRight = ilLeft + ilWindowWidth;
			break;
		case 2:
			ilCXMonitor = (int)(olMainFrameRect.right/2);
			ilLeft = (int)((ilCXMonitor-ilWindowWidth)/2);
			ilRight = ilLeft + ilWindowWidth;
			break;
		case 3:
			ilCXMonitor = (int)(olMainFrameRect.right/3);
			ilLeft = (int)((ilCXMonitor-ilWindowWidth)/2);
			ilRight = ilLeft + ilWindowWidth;
			break;
		default:
			ilCXMonitor = olMainFrameRect.right;
			ilLeft = (int)((ilCXMonitor-ilWindowWidth)/2);
			ilRight = ilLeft + ilWindowWidth;
			break;
		}
	}
	
	// Fenster auf Monitor 2 darstellen
	else if(ilWhichMonitor == 2)
	{
		// Anzahl der Monitore im System
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = olMainFrameRect.right;
			ilLeft = (int)((ilCXMonitor-ilWindowWidth)/2);
			ilRight = ilLeft + ilWindowWidth;
			break;
		case 2:
			ilCXMonitor = (int)(olMainFrameRect.right/2);
			ilLeft = (int)((ilCXMonitor-ilWindowWidth)/2)+(ilCXMonitor);
			ilRight = ilLeft + ilWindowWidth;
			break;
		case 3:
			ilCXMonitor = (int)(olMainFrameRect.right/3);
			ilLeft = (int)((ilCXMonitor-ilWindowWidth)/2)+(ilCXMonitor);
			ilRight = ilLeft + ilWindowWidth;
			break;
		default:
			// Hier OK ?
			ilRight = GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}
	// Fenster auf Monitor 3 darstellen
	else if(ilWhichMonitor == 3)
	{
		// Anzahl der Monitore im System
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = olMainFrameRect.right;
			ilLeft = (int)((ilCXMonitor-ilWindowWidth)/2);
			ilRight = ilLeft + ilWindowWidth;
			break;
		case 2:
			ilCXMonitor = (int)(olMainFrameRect.right/2);
			ilLeft = (int)((ilCXMonitor-ilWindowWidth)/2);
			ilRight = ilLeft + ilWindowWidth;
			break;
		case 3:
			ilCXMonitor = (int)(olMainFrameRect.right/3);
			ilLeft = (int)((ilCXMonitor-ilWindowWidth)/2)+(2*ilCXMonitor);
			ilRight = ilLeft + ilWindowWidth;
			break;
		default:
			// Hier OK ?
			ilRight = GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}
	
	// Werte werde angepa�t auf Anzahl der Monitore und welchem Monitor das Fenster
	// dargestellt werden soll.
	olWindowRect.left  = ilLeft;
	olWindowRect.right = ilRight;
	
	// Sollte eine Statusbar vorhanden sein, diese vom Client Rect abziehen
	CRect	olStatusBarRect;
	// Berechnete H�he der Statusbar
	int		ilStatusBarHight;	
	
	if (GetParentFrame()->GetParentFrame()->GetMessageBar() != NULL)
	{
		(GetParentFrame()->GetParentFrame()->GetMessageBar())->GetWindowRect(olStatusBarRect);
		ilStatusBarHight = olStatusBarRect.bottom - olStatusBarRect.top;
		
		// Client Rect f�r dieses Fenster verkleinern.
		olWindowRect.bottom -= ilStatusBarHight;
	}
	
	// Die Rahmen Abziehen
	olWindowRect.right -= GetSystemMetrics(SM_CXSIZEFRAME);
	olWindowRect.bottom -= GetSystemMetrics(SM_CYSIZEFRAME);
	
	return (olWindowRect);
	CCS_CATCH_ALL;
	return CRect(0,0,0,0);
}

//****************************************************************************
// DutyRoster_View message handlers
//****************************************************************************

void DutyRoster_View::OnInitialUpdate() 
{
	CCS_TRY;
	CFormView::OnInitialUpdate();
	
	// Einen neuen View herstellen
	pomViewer = new CViewer;
	pomViewer->SetViewerKey("ID_SHEET_DUTY_VIEW");
	
	// Titel des Docs setzen.
	CRosteringDoc *polDoc = (CRosteringDoc *)GetDocument();
	if(polDoc != NULL)
	{
		polDoc->SetTitle(LoadStg(DUTY_CAPTION));
	}
	
	// ComboBox Setup
	FillComboBox();
	
	// Frame auf die richtige Position und Gr��e schieben
	GetParentFrame()->MoveWindow(CalcInitialWindowSizeAndPos());
	
	// unn�tig ?
	//	ResizeParentToFit();
	GetParentFrame()->RecalcLayout();
	
	//Falls nach Fahrgemeinschaften sortiert wird
	ogTeaData.omData.Sort(CompareTeaData);
	
	//**************************************************************************************
	//Rahmendicke ermitteln 
	//int ilCYCaption = GetSystemMetrics(SM_CYCAPTION);
	int ilCYEdge	= GetSystemMetrics(SM_CYSIZEFRAME);
	int ilCXEdge	= GetSystemMetrics(SM_CXSIZEFRAME);

	//<<<<<< Show Views
	//>>>>>> Create Tables
	int ilScrollBar				= 16; //is alway fix in CCSDYNTABLE
	int ilTopDistance			= 1; // Platz nach oben
	int ilLeftDistance			= 1; // Platz nach links

	// grid containing the shifts (the big one in the middle)
	int ilIsTabWidth			= ogCfgData.GetMonitorForWindow(MON_DUTYROSTER_DAYWIDTH);
	if (ilIsTabWidth < 20)
	{
		ilIsTabWidth = 20;
	}
	int ilIsTabSolidWidth		= 0;

	// grid containing employee names, function, contract, ... (in the left)
	int ilSolidTabColumns		= 1;
	int ilTotalWidth = ogCfgData.GetMonitorForWindow(MON_DUTYROSTER_TOTWIDTH);
	if (ilTotalWidth == 1) ilTotalWidth = 222;
	int ilRelation = ogCfgData.GetMonitorForWindow(MON_DUTYROSTER_RELATION);
	//int ilSolidTabWidth			= 93+18;
	if (ilRelation == 1) ilRelation = 50;
	int ilSolidTabWidth			= (100 - ilRelation) * ilTotalWidth / 100;
	//int ilSolidTabSolidWidth	= 50;
	int ilSolidTabSolidWidth	= ilTotalWidth - ilSolidTabWidth;

	// grid containing account information (in the right)
	int ilColumns	= ogCfgData.GetMonitorForWindow(MON_DUTYROSTER_STATICCOLUMNS);
	if (ilColumns < 1)
	{
		ilColumns = 1;
	}
	int ilStatTabColumns		= ilColumns;
	int ilStatTabWidth			= 58;
	int ilStatTabSolidWidth		= 0;

	// grid displaying actual/target number of shifts (in the bottom)
	int ilDebitTabWidth			= ilIsTabWidth;
	int ilDebitTabSolidWidth	= ilTotalWidth;//(ilSolidTabColumns + 1) * ilSolidTabWidth;

	// Gr��e des benutzbaren Client Rects berechnen.
	omWindowRect = CalcUsableClientRect();

	// Breite des Client Windows
	int ilWndWidth = omWindowRect.right - omWindowRect.left;
	int ilTabFixWidth = ilIsTabSolidWidth + (ilSolidTabWidth*ilSolidTabColumns+ilSolidTabSolidWidth) + (ilStatTabWidth*ilStatTabColumns+ilStatTabSolidWidth) + (ilScrollBar+ilCXEdge);//+2*ilCXEdge

	// Anzahl der Spalten der IS Tabelle
	int ilIsTabColumns = (int)((ilWndWidth - ilTabFixWidth) / ilIsTabWidth);
	// mindestens eine Spalte darstellen
	if(ilIsTabColumns<1) ilIsTabColumns = 1;

	// Anzahl der Debit Spalten entspricht den IS Spalten
	int ilDebitTabColumns		= ilIsTabColumns;

	//////////////////////////////
	// H�he der einzelnen Tabelleneinheiten
	int ilIsTabHight			= 18;
	int ilIsTabHeaderHight		= 18;
	int ilSolidTabHight			= ilIsTabHight;
	int ilSolidTabHeaderHight	= ilIsTabHeaderHight;
	int ilStatTabHight			= ilIsTabHight;
	int ilStatTabHeaderHight	= ilIsTabHeaderHight;
	int ilDebitTabHight			= ilIsTabHight;
	int ilDebitTabHeaderHight	= ilIsTabHight;
	
	// Anzahl Reihen in der Debit Tabelle
	int ilRows	= ogCfgData.GetMonitorForWindow(MON_DUTYROSTER_DEBITROWS);
	//if (ilRows < 1) ilRows = 1;
	if (ilRows < 2) ilRows = 2;
	int ilDebitTabRows	= ilRows; //ehemals fest 5;
	
	// H�he des Client Fenster wird berechnet. (Tool mit ein gerechnet ?)
	int ilWndHight = omWindowRect.bottom - omWindowRect.top;
	
	// CY Edge
	int ilTabFixHight = ilIsTabHeaderHight + (ilDebitTabRows*ilDebitTabHight+ilDebitTabHeaderHight) + (ilScrollBar+ilCYEdge+ilTopDistance);//+2*ilCYEdge+ilCYCaption
	
	int ilIsTabRows = (int)((ilWndHight - ilTabFixHight) / ilIsTabHight);
	if(ilIsTabRows<1) ilIsTabRows = 1;
	
	int ilSolidTabRows			= ilIsTabRows;
	int ilStatTabRows			= ilIsTabRows;
	
	// Minimale Gr��e des Views festlegen
	omMinTrackSize = CPoint(ilTabFixWidth+ilIsTabWidth+ilLeftDistance+11,ilTabFixHight+ilIsTabHight+ilTopDistance+67);
	
	// Solid Table wird erzeugt *****************************************************************
	CRect olSolidTableRect = CRect(ilLeftDistance,ilTopDistance,0,0);
	
	imSolidTableID = 464559;
	pomSolidTable	= new CCSDynTable(this,imSolidTableID);
	
	pomSolidTable->SetTableData(olSolidTableRect,ilSolidTabColumns,ilSolidTabWidth,ilSolidTabHeaderHight,ilSolidTabRows,
							 ilSolidTabHight,ilSolidTabSolidWidth,false,false,false,false,false,true);
	pomSolidTable->SetDefaultColors(COLORREF(DT_LIGHTSILVER2), COLORREF(DT_BLACK), COLORREF(DT_LIGHTSILVER2),
		COLORREF(DT_BLACK), COLORREF(DT_LIGHTSILVER2), COLORREF(DT_BLACK));
	prmSolidIPEAttrib = new CCSEDIT_ATTRIB;
	pomSolidTable->SetDefaultTextForm(&ogMSSansSerif_Regular_8, &ogMSSansSerif_Italic_8, &ogMSSansSerif_Regular_8, &ogMSSansSerif_Regular_8,
		DT_SINGLELINE|DT_VCENTER, DT_SINGLELINE|DT_VCENTER,
		DT_SINGLELINE|DT_VCENTER, prmSolidIPEAttrib);
	pomSolidTable->Create();
	pomSolidTabViewer  =  new DutySolidTabViewer(this);
	pomCurrentSolidViewer = (CViewer *)pomSolidTabViewer;
	pomCurrentSolidViewer->SetViewerKey(CString("SOLID_TABLE"));
	pomSolidTabViewer->Attach(pomSolidTable);
	
	
	
	
	// IS Table wird erzeugt *****************************************************************
	CRect olIsTableRect = CRect(olSolidTableRect.right-1,olSolidTableRect.top,0,0);
	imIsTableID = 464600;
	pomIsTable	= new CCSDynTable(this,imIsTableID);
	
	pomIsTable->SetTableData(olIsTableRect,ilIsTabColumns,ilIsTabWidth,ilIsTabHeaderHight,ilIsTabRows,
							 ilIsTabHight,ilIsTabSolidWidth,true,false,true,true,true,true);
	pomIsTable->SetDefaultColors(COLORREF(DT_LIGHTSILVER1), COLORREF(DT_BLACK), COLORREF(DT_LIGHTSILVER2),
		COLORREF(DT_BLACK), COLORREF(DT_LIGHTSILVER2), COLORREF(DT_BLACK));
	pomIsTable->SetDefaultColors(9,COLORREF(DT_LIGHTYELLOW1));
	prmIsIPEAttrib = new CCSEDIT_ATTRIB;
	prmIsIPEAttrib->Format = CString("x|#x|#x|#x|#x|#x|#x|#x|#");
	prmIsIPEAttrib->TextMaxLenght = 8;
	prmIsIPEAttrib->TextMinLenght = 0;
	pomIsTable->SetDefaultTextForm(&ogMSSansSerif_Regular_8, &ogMSSansSerif_Italic_8, &ogMSSansSerif_Regular_8, &ogMSSansSerif_Regular_8,
		DT_SINGLELINE|DT_CENTER|DT_VCENTER, DT_SINGLELINE|DT_CENTER|DT_VCENTER,
		DT_SINGLELINE|DT_VCENTER, prmIsIPEAttrib);
	pomIsTable->Create();
	// die Tabelle wird f�r D'n'D regristriert und will nur drops f�r DIT_DUTYROSTER_BSD_TABLE erhalten
	pomIsTable->DropRegister(DIT_DUTYROSTER_BSD_TABLE);
	
	// Viewer wird erzeugt ***************************************************************
	
	pomIsTabViewer  =  new DutyIsTabViewer(this);
	pomCurrentIsViewer = (CViewer *)pomIsTabViewer;
	pomCurrentIsViewer->SetViewerKey(CString("IS_TABLE"));
	// Die Table wird dem Viewer zugeordnet
	pomIsTabViewer->Attach(pomIsTable);
	//<< Is Tab
	//>> Debit Table *****
	CRect olDebitTableRect = CRect(olIsTableRect.left-ilDebitTabSolidWidth,olIsTableRect.bottom,0,0);
	imDebitTableID = 464601;
	
	pomDebitTable = new CCSDebitDynTable(this,imDebitTableID);
	pomDebitTable->SetTableData(olDebitTableRect,ilDebitTabColumns,ilDebitTabWidth,ilDebitTabHeaderHight,ilDebitTabRows,
								ilDebitTabHight,ilDebitTabSolidWidth,false,false,false,true,true,false);
	pomDebitTable->SetDefaultColors(COLORREF(DT_LIGHTSILVER1), COLORREF(DT_BLACK), COLORREF(DT_LIGHTSILVER2),
		COLORREF(DT_BLACK), COLORREF(DT_LIGHTSILVER2), COLORREF(DT_BLACK));
	prmDebitIPEAttrib = new CCSEDIT_ATTRIB;
	pomDebitTable->SetDefaultTextForm(&ogMSSansSerif_Regular_8, &ogMSSansSerif_Italic_8, &ogMSSansSerif_Regular_8, &ogMSSansSerif_Regular_8,
		DT_SINGLELINE|DT_CENTER|DT_VCENTER, DT_SINGLELINE|DT_CENTER|DT_VCENTER,
		DT_SINGLELINE|DT_VCENTER, prmDebitIPEAttrib);
	pomDebitTable->Create();
	
	// Von hier aus darf DnD gestartet werden.
	pomDebitTable->DragRegister(DIT_DUTYROSTER_BSD_TABLE);
	
	pomDebitTabViewer  =  new DutyDebitTabViewer(this);
	pomCurrentDebitViewer = (CViewer *)pomDebitTabViewer;
	pomCurrentDebitViewer->SetViewerKey(CString("DEBIT_TABLE"));
	pomDebitTabViewer->Attach(pomDebitTable);
	//<< Debit Table
	//>> Statistic Table *****
	CRect olStatTableRect = CRect(olIsTableRect.right,olIsTableRect.top,0,0);
	imStatTableID = 464602;
	
	pomStatTable = new CCSDynTable(this,imStatTableID);
	pomStatTable->SetTableData(olStatTableRect,ilStatTabColumns,ilStatTabWidth,ilStatTabHeaderHight,ilStatTabRows,
								ilStatTabHight,ilStatTabSolidWidth,false,false,true,false,false,true);
	pomStatTable->SetDefaultColors(COLORREF(DT_LIGHTSILVER1), COLORREF(DT_BLACK), COLORREF(DT_LIGHTSILVER2),
		COLORREF(DT_BLACK), COLORREF(DT_LIGHTSILVER2), COLORREF(DT_BLACK));
	prmStatIPEAttrib = new CCSEDIT_ATTRIB;
	pomStatTable->SetDefaultTextForm(&ogMSSansSerif_Regular_8, &ogMSSansSerif_Italic_8, &ogMSSansSerif_Regular_8, &ogMSSansSerif_Regular_8,
		DT_SINGLELINE|DT_CENTER|DT_VCENTER, DT_SINGLELINE|DT_CENTER|DT_VCENTER,
		DT_SINGLELINE|DT_VCENTER, prmStatIPEAttrib);
	pomStatTable->Create();
	
	pomStatTabViewer  =  new DutyStatTabViewer(this,pomAccounts);
	pomCurrentStatViewer = (CViewer *)pomStatTabViewer;
	pomCurrentStatViewer->SetViewerKey(CString("STAT_TABLE"));
	pomStatTabViewer->Attach(pomStatTable);
	//<< Statistic Table
	
	// SetDifferenceToParentWndRect setzt den verbleibenden Raum zwischen Tabelle und Dialog
	int ilHDiff = (omWindowRect.right - ilCXEdge) - olStatTableRect.right;
	int ilVDiff = (omWindowRect.bottom - ilCYEdge) - olDebitTableRect.bottom;
	pomSolidTable->SetDifferenceToParentWndRect(ilHDiff, ilVDiff);
	pomIsTable->SetDifferenceToParentWndRect(ilHDiff, ilVDiff);
	pomDebitTable->SetDifferenceToParentWndRect(ilHDiff, ilVDiff);
	pomStatTable->SetDifferenceToParentWndRect(ilHDiff, ilVDiff);
	
	// Hiermit registriert sich das Window f�r Broadcasts
	// Parameter: Zeiger auf Fenster, Command, Trace Info, Trace Info, Callback Function
    DdxRegister((void *)this,DRR_CHANGE,		"DutyRoster_View", "DRR_CHANGE",		ProcessDutyRoster_ViewCf);
    DdxRegister((void *)this,DRR_NEW,			"DutyRoster_View", "DRR_NEW",			ProcessDutyRoster_ViewCf);
	DdxRegister((void *)this, DRG_CHANGE,		"DutyRoster_View", "DRG_CHANGE",	    ProcessDutyRoster_ViewCf);
	DdxRegister((void *)this, DRG_NEW,			"DutyRoster_View", "DRG_NEW",		    ProcessDutyRoster_ViewCf);
	DdxRegister((void *)this, DRG_DELETE,		"DutyRoster_View", "DRG_DELETE",		ProcessDutyRoster_ViewCf);
	
	CCS_CATCH_ALL;
}

//****************************************************************************
// OnDraw
//****************************************************************************

void DutyRoster_View::OnDraw(CDC* pDC) 
{
}

/****************************************************************************
UpdateComboBox()
bool bpChangeView - if false, then don't read pomViewer.ViewName, but if possible
keep the own actual one
****************************************************************************/

void DutyRoster_View::UpdateComboBox(bool bpChangeView/*=true*/)
{
	CCS_TRY;
	
	CComboBox* polComboBox;
	polComboBox = ((DutyRoster_Frm*) GetParentFrame())->GetComboBox();
	if(!polComboBox) return;
	
	CString olViewName("");
	
	if(!bpChangeView)
	{
		// read activ view entry
		int ilSelectedView = polComboBox->GetCurSel();
		if(ilSelectedView != CB_ERR)
		{
			polComboBox->GetLBText(ilSelectedView, olViewName);
		}
	}
	
	polComboBox->ResetContent();
	
	// Alle Views in die Combobox eintragen.
	CStringArray olStrArr;
	pomViewer->GetViews(olStrArr);
	for(int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
	{
		polComboBox->AddString(olStrArr[ilIndex]);
	}
	
	// Aktuellen View in die Liste ausw�hlen
	if(bpChangeView)
	{
		olViewName = pomViewer->GetViewName();
	}
	
	if(olViewName.IsEmpty() == TRUE)
	{
		olViewName = pomViewer->SelectView();
		omOldViewName = olViewName;
	}
	
	ilIndex = polComboBox->FindString(-1,olViewName);
	if(ilIndex == CB_ERR)
		ilIndex = polComboBox->FindString(-1,"<Default>");
	
	
	if(ilIndex != CB_ERR)
	{
		polComboBox->SetCurSel(ilIndex);
	}
	
	CCS_CATCH_ALL;
}

//****************************************************************************
// CopyViewInfo: kopiert die aktuelle Ansicht in <rpViewInfo>.
//	R�ckgabe:	keine
//****************************************************************************

void DutyRoster_View::CopyViewInfo(VIEWINFO &rpViewInfo)
{
	CCS_TRY;
	// momentane Ansicht kopieren
	rpViewInfo.oDateFrom		= rmViewInfo.oDateFrom;
	rpViewInfo.oDateTo			= rmViewInfo.oDateTo;
	rpViewInfo.oShowDateFrom	= rmViewInfo.oShowDateFrom;
	rpViewInfo.oShowDateTo		= rmViewInfo.oShowDateTo;
	rpViewInfo.oLoadDateFrom	= rmViewInfo.oLoadDateFrom;
	rpViewInfo.oLoadDateTo		= rmViewInfo.oLoadDateTo;
	rpViewInfo.oPfcUrnos		= rmViewInfo.oPfcUrnos;
	rpViewInfo.bSelectAllFunc	= rmViewInfo.bSelectAllFunc;
	rpViewInfo.oOrgUrnos		= rmViewInfo.oOrgUrnos;
	rpViewInfo.oACT_ICAOCode		= rmViewInfo.oACT_ICAOCode;
	rpViewInfo.oALT_ICAOCode		= rmViewInfo.oALT_ICAOCode;
	rpViewInfo.oPfcStatUrnos	= rmViewInfo.oPfcStatUrnos;
	rpViewInfo.oOrgStatUrnos	= rmViewInfo.oOrgStatUrnos;
	rpViewInfo.oPfcFctcs		= rmViewInfo.oPfcFctcs;
	rpViewInfo.oOrgDpt1s		= rmViewInfo.oOrgDpt1s;
	rpViewInfo.oPfcStatFctcs	= rmViewInfo.oPfcStatFctcs;
	rpViewInfo.oOrgStatDpt1s	= rmViewInfo.oOrgStatDpt1s;
	rpViewInfo.oPType			= rmViewInfo.oPType;
	rpViewInfo.bGroupStat		= rmViewInfo.bGroupStat;
	rpViewInfo.bDetailStat		= rmViewInfo.bDetailStat;
	rpViewInfo.bExtraShifts		= rmViewInfo.bExtraShifts;
	rpViewInfo.bAdditiv			= rmViewInfo.bAdditiv;
	rpViewInfo.bAllCodes		= rmViewInfo.bAllCodes;
	rpViewInfo.iGetStatFrom		= rmViewInfo.iGetStatFrom;
	rpViewInfo.iDCoverage		= rmViewInfo.iDCoverage;
	rpViewInfo.iEName			= rmViewInfo.iEName;
	rpViewInfo.iSortFVN			= rmViewInfo.iSortFVN;
	rpViewInfo.oNameConfigString= rmViewInfo.oNameConfigString;
	rpViewInfo.iPresent			= rmViewInfo.iPresent;
	rpViewInfo.iGroup			= rmViewInfo.iGroup;
	rpViewInfo.oSelectDate		= rmViewInfo.oSelectDate;
	rpViewInfo.oStfUrnos.RemoveAll();
	rpViewInfo.oStfUrnos.Append(rmViewInfo.oStfUrnos);
	rpViewInfo.lPlanGroupUrno	= rmViewInfo.lPlanGroupUrno;
	rpViewInfo.oWorkGroupUrnos.RemoveAll();
	rpViewInfo.oWorkGroupUrnos.Append(rmViewInfo.oWorkGroupUrnos);
	rpViewInfo.oWorkGroupCodes.RemoveAll();
	rpViewInfo.oWorkGroupCodes.Append(rmViewInfo.oWorkGroupCodes);
	rpViewInfo.oPlanGroupCodes.RemoveAll();
	rpViewInfo.oPlanGroupCodes.Append(rmViewInfo.oPlanGroupCodes);
	rpViewInfo.oPlanGroupUrnos.RemoveAll();
	rpViewInfo.oPlanGroupUrnos.Append(rmViewInfo.oPlanGroupUrnos);
	rpViewInfo.oPlanGroupMin.RemoveAll();
	rpViewInfo.oPlanGroupMin.Append(rmViewInfo.oPlanGroupMin);
	rpViewInfo.oPlanGroupMax.RemoveAll();
	rpViewInfo.oPlanGroupMax.Append(rmViewInfo.oPlanGroupMax);
	rpViewInfo.bSortDayly		= rmViewInfo.bSortDayly;
	rpViewInfo.oAccounts.RemoveAll();
	rpViewInfo.oAccounts.Append(rmViewInfo.oAccounts);
	rpViewInfo.oAccountDateFrom	= rmViewInfo.oAccountDateFrom;
	rpViewInfo.oAccountDateTo	= rmViewInfo.oAccountDateTo;
	CCS_CATCH_ALL;
}

//****************************************************************************
// InitViewInfo: initialisiert die Ansicht in <rpViewInfo>.
//	R�ckgabe:	keine
//****************************************************************************

void DutyRoster_View::InitViewInfo()
{
	CCS_TRY;
	// Ansicht auf Defaultwerte zur�cksetzen
	rmViewInfo.bCorrectViewInfo	= false;
	rmViewInfo.oDateFrom.SetStatus(COleDateTime::invalid);
	rmViewInfo.oDateTo.SetStatus(COleDateTime::invalid);
	rmViewInfo.oShowDateFrom.SetStatus(COleDateTime::invalid);
	rmViewInfo.oShowDateTo.SetStatus(COleDateTime::invalid);
	rmViewInfo.oLoadDateFrom.SetStatus(COleDateTime::invalid);
	rmViewInfo.oLoadDateTo.SetStatus(COleDateTime::invalid);
	rmViewInfo.oPfcUrnos		= "";
	rmViewInfo.bSelectAllFunc	= false;
	rmViewInfo.oOrgUrnos		= "";
	rmViewInfo.oACT_ICAOCode		= "";
	rmViewInfo.oALT_ICAOCode		= "";
	rmViewInfo.oPfcFctcs		= "";
	rmViewInfo.oOrgDpt1s		= "";
	rmViewInfo.oPfcStatUrnos	= "";
	rmViewInfo.oOrgStatUrnos	= "";
	rmViewInfo.oPfcStatFctcs	= "";
	rmViewInfo.oOrgStatDpt1s	= "";
	rmViewInfo.oPType			= "";
	rmViewInfo.bGroupStat		= false;
	rmViewInfo.bDetailStat		= true;
	rmViewInfo.bExtraShifts		= false;
	rmViewInfo.bAdditiv			= true;
	rmViewInfo.bAllCodes		= false;
	rmViewInfo.iGetStatFrom		= STAT_PLAN;
	rmViewInfo.iDCoverage		= 1;
	if (strlen(pcgDefaultNameConfig_Duty))
	{
		rmViewInfo.iEName = 4;
		rmViewInfo.oNameConfigString = CString(pcgDefaultNameConfig_Duty);
	}
	else
	{
		rmViewInfo.iEName = 2;
	}
	
	rmViewInfo.iSortFVN			= 0;
	rmViewInfo.iPresent			= 1;
	rmViewInfo.iGroup			= 1;
	rmViewInfo.oSelectDate.SetStatus(COleDateTime::invalid);
	rmViewInfo.oStfUrnos.RemoveAll();
	rmViewInfo.lPlanGroupUrno	= 0;
	rmViewInfo.oWorkGroupUrnos.RemoveAll();
	rmViewInfo.oWorkGroupCodes.RemoveAll();
	rmViewInfo.oPlanGroupCodes.RemoveAll();
	rmViewInfo.oPlanGroupUrnos.RemoveAll();
	rmViewInfo.oPlanGroupMin.RemoveAll();
	rmViewInfo.oPlanGroupMax.RemoveAll();
	rmViewInfo.bSortDayly		= false;
	rmViewInfo.oAccounts.RemoveAll();
	rmViewInfo.oAccountDateFrom.SetStatus(COleDateTime::invalid);
	rmViewInfo.oAccountDateTo.SetStatus(COleDateTime::invalid);
	rmViewInfo.bDoConflictCheck = true;
	rmViewInfo.bCheckMainFuncOnly = true;
	CCS_CATCH_ALL;
}

//****************************************************************************
// GetSafeStringFromArray: ermittelt den Wert <opValues[<ipIndex>]> und
//	speichert ihn in <opStrParam>. Der Index wird auf G�ltigkeit �berpr�ft.
//	wenn der Index ung�ltig ist, wird <opStrParam> = "" gesetzt.
//	R�ckgabe:	true	-> g�ltiger Index
//				false	-> ung�ltiger Index, Wert ist Leerstring
//****************************************************************************

bool DutyRoster_View::GetSafeStringFromArray(CStringArray &opValues,
											 int ipIndex, CString &opStrParam)
{
	CCS_TRY;
	// Index ung�ltig?
	if (opValues.GetSize() <= ipIndex)
	{
		// ja -> String ist leer
		opStrParam = "";
		return false;
	}
	
	// String ermitteln
	opStrParam = opValues[ipIndex];
	return true;
	CCS_CATCH_ALL;
	return false;
}

//****************************************************************************
// GetPropPageParams: ermittelt alle in einer Ansicht zusammengefassten
//	Filter- und Anzeigeparameter und speichert die Werte in der 
//	Struktur VIEWINFO <rmViewInfo>. Die Ansicht muss vorher aus der 
//	Datenbank geladen worden sein (siehe OnSelchange_CB_View). Die Funktion 
//	CViewer::GetFilter(<PropPage>) liefert die Werte je einer PropPage in 
//	Form von einer String-Liste zur�ck.
//	R�ckgabe:	keine
//****************************************************************************

void DutyRoster_View::GetPropPageParams(VIEWINFO &rpSafetyCopy) 
{
	CCS_TRY;
	// String-Array, der die Ansichts-Parameter (aus den PropertyPages) in 
	// String-Form speichert
	CStringArray olValues;
	// Puffer f�r Parameter
	CString olPeriode;
	// der Parameter
	CString olStrParam;
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// erste PropPage: Laden
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// die Parameter der ersten Propage 'Laden' lesen: Start- und Enddatum und 
	// ausgew�hlte Funktionen
	// Erwartet werden 8 Strings
	pomViewer->GetFilter("DUTYVIEW1", olValues);
	
	if (GetSafeStringFromArray(olValues,0,olPeriode))
	{
		// Datum angegeben?
		if (olPeriode == "FIX")
		{
			// ja -> Start- und Enddatum vorhanden?
			if(GetSafeStringFromArray(olValues,1,olStrParam) && (olStrParam != ""))
			{
				// L�nge / Format OK?
				if(olStrParam == "0-0" || olStrParam.GetLength() < 17)
				{	
					// ToDo: Fehlerbehandlung
					return;
				}
				// ja -> Start- und Enddatum des Bearbeitungszeitraums einstellen
				rmViewInfo.oDateFrom.SetDate(atoi(olStrParam.Mid(0,4)),atoi(olStrParam.Mid(4,2)),atoi(olStrParam.Mid(6,2)));
				rmViewInfo.oDateTo.SetDate(atoi(olStrParam.Mid(9,4)),atoi(olStrParam.Mid(13,2)),atoi(olStrParam.Mid(15,2)));
				// aktuell angezeigten Tag beibehalten?
				if(bmChangeDay == true)
				{
					// ja
					rmViewInfo.oSelectDate   = rpSafetyCopy.oSelectDate;
				}
				else
				{
					// nein
					rmViewInfo.oSelectDate   = rmViewInfo.oDateFrom;
				}
			}
		}
		else if (olPeriode == "VARIABLE")
		{
			// nein -> Datum variabel, d.h. relativ zu heute
			// Offset-Werte vorhanden?
			if(GetSafeStringFromArray(olValues,2,olStrParam) && (olStrParam != ""))
			{
				// ja
				int ilPlus  = 0;
				int ilMinus = 0;
				// Werte = 0 oder L�nge/Format nicht OK?
				if(olStrParam == "00-00" || olStrParam.GetLength() < 5)
				{
					ilPlus  = 0;
					ilMinus = 0;
				}
				else
				{
					// Offset lesen
					ilPlus  = atoi(olStrParam.Mid(0,2));
					ilMinus = atoi(olStrParam.Mid(3,2));
				} 
				// momentanes Datum ermitteln
				COleDateTime olOleTime = COleDateTime::GetCurrentTime();
				// GetCurrentTime liefert auch die Zeit als Bruchteil des Datums dazu, 
				// das f�hrt sp�ter zu Rundungsfehler wir m�ssen die Zeit l�schen, so dass nur das Datum bleibt.
				olOleTime.SetDateTime(olOleTime.GetYear(),olOleTime.GetMonth(),olOleTime.GetDay(),0,0,0);
				// aktuell angezeigten Tag beibehalten?
				if(bmChangeDay == true)
				{
					// ja
					rmViewInfo.oSelectDate = rpSafetyCopy.oSelectDate;
				}
				else
				{
					// nein
					rmViewInfo.oSelectDate = olOleTime;
				}
				// Offset einstellen
				rmViewInfo.oDateFrom = olOleTime - COleDateTimeSpan(ilMinus,0,0,0);
				rmViewInfo.oDateTo   = olOleTime + COleDateTimeSpan(ilPlus,0,0,0);
				// Feststellen, ob �ber eine Monatsgrenze hinaus geladen werden soll
				if (olOleTime.GetMonth() != rmViewInfo.oDateFrom.GetMonth() && !bgEnableMonthlyAllowance)
				{
					// Falls JA, oDateFrom auf Menatsanfang abschneiden
					rmViewInfo.oDateFrom.SetDate(olOleTime.GetYear(), olOleTime.GetMonth(), 1);
				}
				if (olOleTime.GetMonth() != rmViewInfo.oDateTo.GetMonth() && !bgEnableMonthlyAllowance)
				{
					// Falls JA, oDateTo auf Menatsende abschneiden
					int ilDays = CedaDataHelper::GetDaysOfMonth(olOleTime);
					rmViewInfo.oDateTo.SetDate(olOleTime.GetYear(), olOleTime.GetMonth(), ilDays);
				}
			}
		}
		// Start- und Enddatum der Anzeige einstellen (max +/- eine Woche
		// vom Bearbeitungszeitraum)
		int ilViewPl	= ogCfgData.GetMonitorForWindow(MON_DUTYROSTER_VIEWPL);
		int ilViewMi	= ogCfgData.GetMonitorForWindow(MON_DUTYROSTER_VIEWMI);
		
		// std Konfliktpr�fungsgr��e (z.Z. 7 Tage) 
		int ilKonfl = atoi(ogCCSParam.GetParamValue(ogAppl,"ID_WOR_MAWORD"));
		
		// ist Konfliktpr�fung gew�nscht?
		pomViewer->GetFilter("DUTYVIEW5", olValues);
		GetSafeStringFromArray(olValues,1,olStrParam);
		
		// zur�ck zu DUTYVIEW1
		pomViewer->GetFilter("DUTYVIEW1", olValues);
		
		if (olStrParam=="1")
		{
			// Konfliktpr�fung off,	Ladazeitraum = eingabe!
			rmViewInfo.oLoadDateFrom =	rmViewInfo.oDateFrom;
			rmViewInfo.oLoadDateTo	 =	rmViewInfo.oDateTo;
		}
		else
		{
			// Konfliktpr�fung on,	Ladezeitraum = eingabe + std gr��e
			rmViewInfo.oLoadDateFrom =	rmViewInfo.oDateFrom - COleDateTimeSpan(ilKonfl,0,0,0);
			rmViewInfo.oLoadDateTo	 =	rmViewInfo.oDateTo   + COleDateTimeSpan(ilKonfl,0,0,0);
		}
		
		//Anzeigespanne = eingabe + setupspanne
		rmViewInfo.oShowDateFrom = rmViewInfo.oDateFrom - COleDateTimeSpan(ilViewMi,0,0,0);
		rmViewInfo.oShowDateTo	 = rmViewInfo.oDateTo   + COleDateTimeSpan(ilViewPl,0,0,0);
		
		// sollen fr�here Daten angezeigt werden als geladen werden? 
		// Ladedatum anpassen
		//liegt anzeigespanne vor ladezeitraum?
		if (rmViewInfo.oShowDateFrom < rmViewInfo.oLoadDateFrom)
			rmViewInfo.oLoadDateFrom = rmViewInfo.oShowDateFrom;
		//liegt anzeigespanne nach ladezeitraum?
		if (rmViewInfo.oShowDateTo > rmViewInfo.oLoadDateTo)
			rmViewInfo.oLoadDateTo = rmViewInfo.oShowDateTo;
		
		// Setzt das Kontenenddatum auf das Leseenddatum
		rmViewInfo.oAccountDateTo = rmViewInfo.oDateTo;
		rmViewInfo.oAccountDateFrom = rmViewInfo.oDateFrom;
		
		TRACE("DutyRoster_View::GetPropPageParams(): Zeit von %s bis %s\n",rmViewInfo.oDateFrom.Format("%d.%m.%Y"),rmViewInfo.oDateTo.Format("%d.%m.%Y"));
		TRACE("DutyRoster_View::GetPropPageParams(): Anzeigezeit von %s bis %s\n",rmViewInfo.oShowDateFrom.Format("%d.%m.%Y"),rmViewInfo.oShowDateTo.Format("%d.%m.%Y"));
		TRACE("DutyRoster_View::GetPropPageParams(): Ladezeitraum von %s bis %s\n",rmViewInfo.oLoadDateFrom.Format("%d.%m.%Y"),rmViewInfo.oLoadDateTo.Format("%d.%m.%Y"));
		
	}
	
	// die ausgew�hlten Funktionen ermitteln (Lader, Gruppenf�hrer, etc.)
	if(GetSafeStringFromArray(olValues,3,olStrParam) && !olStrParam.IsEmpty() && olStrParam != "NULL")
	{
		// String mit den Funktions-Urnos in Ansicht kopieren
		rmViewInfo.oPfcUrnos = olStrParam;
		// das Trennzeichen auf Komma setzen
		int ilFind = 0;
		while((ilFind = rmViewInfo.oPfcUrnos.Find("|")) != -1)
		{
			rmViewInfo.oPfcUrnos.SetAt(ilFind,',');
		}
		// den Funktions-ID-String mit Komma beginnen und abschliessen
		if(rmViewInfo.oPfcUrnos.IsEmpty() == FALSE)
		{	
			CString olTmp;
			olTmp.Format(",%s,",rmViewInfo.oPfcUrnos);
			rmViewInfo.oPfcUrnos = olTmp;
		}
		// eine Liste mit asugew�hlten Funktionen f�r CodeDlg/CodeBigDlg ausf�llen
	}
	
	// die ausgew�hlten Organisationen ermitteln
	if(GetSafeStringFromArray(olValues,4,olStrParam) && !olStrParam.IsEmpty() && olStrParam != "NULL")
	{
		// String mit den Organisations-Urnos in Ansicht kopieren
		rmViewInfo.oOrgUrnos = olStrParam;
		// das Trennzeichen auf Komma setzen
		int ilFind = 0;
		while((ilFind = rmViewInfo.oOrgUrnos.Find("|")) != -1)
		{
			rmViewInfo.oOrgUrnos.SetAt(ilFind,',');
		}
		// den Organsisations-ID-String mit Komma beginnen und abschliessen
		if(rmViewInfo.oOrgUrnos.IsEmpty() == FALSE)
		{	
			CString olTmp;
			olTmp.Format(",%s,",rmViewInfo.oOrgUrnos);
			rmViewInfo.oOrgUrnos = olTmp;
		}
	}
	
	rmViewInfo.oACT_ICAOCode = "";
	// die ausgew�hlten ACT ermitteln
	if(GetSafeStringFromArray(olValues,5,olStrParam) && !olStrParam.IsEmpty() && olStrParam != "NULL" && olStrParam != "0" && olStrParam != "1")
	{
		if ((!olStrParam.Find('|')) == (!olStrParam.Find("NULL")))
		{
			rmViewInfo.oACT_ICAOCode = olStrParam;
			//		int z = olValues.GetSize();
		}
	}
	
	rmViewInfo.oALT_ICAOCode = "";
	// die ausgew�hlten ALT ermitteln
	if(GetSafeStringFromArray(olValues,6,olStrParam) && !olStrParam.IsEmpty() && olStrParam != "NULL" && olStrParam != "0" && olStrParam != "1")
	{
		if ((!olStrParam.Find('|')) == (!olStrParam.Find("NULL")))
		{
			rmViewInfo.oALT_ICAOCode = olStrParam;
		}
	}
	
	rmViewInfo.bSelectAllFunc = false;
	// Nur nach Stammfunktion filtern oder nach allen Funktionen des MAs?
	if(GetSafeStringFromArray(olValues,7,olStrParam) && !olStrParam.IsEmpty())
	{
		if (olStrParam == "1")
		{
			// nach allen Funktionen des MAs
			rmViewInfo.bSelectAllFunc = true;
		}
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// zweite PropPage: Filter
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// die Parameter der zweiten Propage 'Filter' lesen: welche Planungsstufen sollen angezeigt
	// werden, sollen an- und/oder abwesende Mitarbeiter angezeigt werden
	// Erwartet werden 2 Strings
	pomViewer->GetFilter("DUTYVIEW2", olValues);
	
	// anzuzeigende Planungsstufen ermitteln
	if(GetSafeStringFromArray(olValues,0,olStrParam) && !olStrParam.IsEmpty() && (olStrParam.GetLength() >= 8))
	{
		CString olTmp = ogCCSParam.GetParamValue(ogAppl,"ID_SHOW_STEPS");
		if(olStrParam.Mid(0,1) == "1" && ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN1") == '1' && strstr(olTmp,"1")!=NULL)
			rmViewInfo.oPType  = ",1";	// Schichtplan
		if(olStrParam.Mid(6,1) == "1" && ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLANU") == '1' && strstr(olTmp,"U")!=NULL)
			rmViewInfo.oPType += ",U";	// Urlaub
		if(olStrParam.Mid(7,1) == "1" && ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLANW") == '1' && strstr(olTmp,"W")!=NULL)
			rmViewInfo.oPType += ",W";	// W�nsche
		if(olStrParam.Mid(5,1) == "1" && ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLANL") == '1' && strstr(olTmp,"L")!=NULL)
			rmViewInfo.oPType += ",L";	// LangzeitDienstplan
		if(olStrParam.Mid(1,1) == "1" && ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN2") == '1' && strstr(olTmp,"2")!=NULL)
			rmViewInfo.oPType += ",2";	// Dienstplan
		if(olStrParam.Mid(2,1) == "1" && ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN3") == '1' && strstr(olTmp,"3")!=NULL)
			rmViewInfo.oPType += ",3";	// Mitarbeiter - Tagesliste
		if(olStrParam.Mid(3,1) == "1" && ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN4") == '1' && strstr(olTmp,"4")!=NULL)
			rmViewInfo.oPType += ",4";	// Tagesendbearbeitung
		if(olStrParam.Mid(4,1) == "1" && ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN5") == '1' && strstr(olTmp,"5")!=NULL)
			rmViewInfo.oPType += ",5";	// Nachbearbeitung
		
		// f�hrendes Komma eleminieren, wenn Eintr�ge vorhanden
		if(rmViewInfo.oPType.IsEmpty() == FALSE && rmViewInfo.oPType.GetLength() > 1)
			rmViewInfo.oPType = rmViewInfo.oPType.Mid(1);
		else
			rmViewInfo.oPType.Empty();
	}
	
	// welche Mitarbeiter sollen angezeigt werden?
	if (GetSafeStringFromArray(olValues,1,olStrParam) && (olStrParam == "PRESENT"))
		rmViewInfo.iPresent  = 2;	// alle anwesenden
	else if(GetSafeStringFromArray(olValues,1,olStrParam) && (olStrParam == "NOTPRESENT"))
		rmViewInfo.iPresent  = 3;	// alle abwesenden
	else
		rmViewInfo.iPresent  = 1;	// alle
	
	TRACE("DutyRoster_View::GetPropPageParams(): Planungsstufen %s\n",rmViewInfo.oPType);
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// dritte PropPage: Statistik
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// die Parameter der dritten Propage 'Statistik' lesen: Einstellungen der Statistik
	// Erwartet werden 8 Strings
	pomViewer->GetFilter("DUTYVIEW3", olValues);
	
	// Gruppenstatistik anzeigen?
	GetSafeStringFromArray(olValues,0,olStrParam);
	rmViewInfo.bGroupStat = (olStrParam == "J");
	// Detailstatistik anzeigen?
	GetSafeStringFromArray(olValues,1,olStrParam);
	rmViewInfo.bDetailStat = (olStrParam == "J");
	// Sonderschichten anzeigen?
	GetSafeStringFromArray(olValues,2,olStrParam);
	rmViewInfo.bExtraShifts = (olStrParam == "J");
	// �ber- und Unterdeckung: Ist/Soll-Anzeige oder Differenz aus Ist und Soll anzeigen?
	GetSafeStringFromArray(olValues,3,olStrParam);
	rmViewInfo.iDCoverage = (olStrParam == "ISDEBIT") ? 1 : 2;
	// Funktionen, die in der Statistik angezeigt werden sollen, kopieren
	if(GetSafeStringFromArray(olValues,4,olStrParam) && !olStrParam.IsEmpty())
	{
		// kopieren
		rmViewInfo.oPfcStatUrnos = olStrParam;
		// Trennzeichen durch Komma ersetzen
		int ilFind = 0;
		
		rmViewInfo.oPfcStatUrnos.Replace('|', ',');
		
		// Fuktionen-Urno-String mit Komma beginnen und abschlie�en
		if(rmViewInfo.oPfcStatUrnos.GetLength() != 0)
		{	
			rmViewInfo.oPfcStatUrnos = "," + rmViewInfo.oPfcStatUrnos + ",";
		}
	}
	// Schichten der Organisationen, die in der Statistik angezeigt werden sollen, kopieren
	if(GetSafeStringFromArray(olValues,5,olStrParam) && !olStrParam.IsEmpty())
	{
		// kopieren
		rmViewInfo.oOrgStatUrnos = olStrParam;
		// Trennzeichen durch Komma ersetzen
		int ilFind = 0;
		while((ilFind = rmViewInfo.oOrgStatUrnos.Find("|")) != -1)
		{
			rmViewInfo.oOrgStatUrnos.SetAt(ilFind,',');
		}
		// Organisationen-Urno-String mit Komma beginnen und abschlie�en
		if(rmViewInfo.oOrgStatUrnos.IsEmpty() == FALSE)
		{	
			CString olTmp;
			olTmp.Format(",%s,",rmViewInfo.oOrgStatUrnos);
			rmViewInfo.oOrgStatUrnos = olTmp;
		}
		
		// Organisationscodes ermitteln und speichern
		ORGDATA *prlOrg;
		CString olOrgUrno;
		//		long llOrgUrno;
		// erstes Komma (sollte Zeichen Nr. 0 sein)
		int ilFrom = rmViewInfo.oOrgStatUrnos.Find(",");
		int ilTo;
		
		//CString olExpand = "";
		// String-Liste mit ORG-Codes initialisieren
		rmViewInfo.oOrgStatDpt1s = "";
		// alle Urnos extrahieren
		do
		{
			// Position des n�chsten Kommas
			if ((ilTo = rmViewInfo.oOrgStatUrnos.Find(",",ilFrom+1)) != -1)
			{
				// n�chste Urno extrahieren
				olOrgUrno = rmViewInfo.oOrgStatUrnos.Mid(ilFrom+1,ilTo-1-ilFrom);
				// Code ermitteln
				if (olOrgUrno == "NULL")
				{
					// tu nichts, NULL zeigt an, dass keine Organisationen
					// ausgew�hlt sind
				}
				else
				{
					if (olOrgUrno == "NO_ORGCODE")
					{
						rmViewInfo.oOrgStatDpt1s += ",'NO_ORGCODE'";
					}
					else
					{
						// Org-Datensatz ermitteln
						if ((prlOrg = ogOrgData.GetOrgByUrno(atol(LPCTSTR(olOrgUrno)))) != NULL)
						{
							// Code ermitteln und speichern
							rmViewInfo.oOrgStatDpt1s += ",'";
							rmViewInfo.oOrgStatDpt1s += CString(prlOrg->Dpt1) + "'";
						}
					}
				}
				ilFrom = ilTo;
			}
		} while (ilTo != -1);
		
		// f�hrende Trennzeichen eleminieren, wenn Elemente enthalten
		if(rmViewInfo.oOrgStatDpt1s.GetLength() > 1)
		{
			rmViewInfo.oOrgStatDpt1s    = rmViewInfo.oOrgStatDpt1s.Mid(1);
		}
	}
	// woher kommen die Ist-Werte aus der unteren Schicht-Statistik?
	if (GetSafeStringFromArray(olValues,6,olStrParam) && !olStrParam.IsEmpty())
	{
		if(olStrParam == "CURRENT")
			rmViewInfo.iGetStatFrom = STAT_CURRENT;
		else if(olStrParam == "PLAN")
			rmViewInfo.iGetStatFrom = STAT_PLAN;
		else if(olStrParam == "LANGZEIT")
			rmViewInfo.iGetStatFrom = STAT_LONGTIME;
		else if(olStrParam == "DIENSTPLAN")
			rmViewInfo.iGetStatFrom = STAT_DUTY;
		else // alle Stufen
			rmViewInfo.iGetStatFrom = STAT_ALL;
	}
	// Funktionsstatistiken addieren?
	GetSafeStringFromArray(olValues,7,olStrParam);
	rmViewInfo.bAdditiv	= (olStrParam != "N");
	
	// Alle Codes lesen?
	GetSafeStringFromArray(olValues,8,olStrParam);
	rmViewInfo.bAllCodes = (olStrParam != "N");
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// vierte PropPage: Sortierung/Gruppierung
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// die Parameter der vierten Propage 'Sortierung/Gruppierung' lesen
	// Erwartet werden 5 Strings
	pomViewer->GetFilter("DUTYVIEW4", olValues);
	
	// Namensfeld als:
	GetSafeStringFromArray(olValues,0,olStrParam);
	if(olStrParam == "CODE")
	{
		// ...Vor- und Nachname
		rmViewInfo.iEName = 1;	
	}
	else if(olStrParam == "FLNAME")
	{
		// ...K�rzel
		rmViewInfo.iEName = 2;
	}
	else if(olStrParam == "SHNM")
	{
		// ...Kurzname
		rmViewInfo.iEName = 3;
	}
	else if(olStrParam == "FREE")
	{
		// ...Kurzname
		rmViewInfo.iEName = 4;
	}
	
	// Gruppieren nach...
	if(GetSafeStringFromArray(olValues,1,olStrParam) && (olStrParam == "GRUPP"))
	{
		// ...Gruppen
		rmViewInfo.iGroup  = 2;
		// Urno der Planungsgruppe aus n�chstenm Parameter lesen
		GetSafeStringFromArray(olValues,2,olStrParam);
		rmViewInfo.lPlanGroupUrno = atol(olStrParam);
		// lade alle Arbeitspruppen die zur Planungspruppe geh�ren
		int ilWgpSize = ogWgpData.omData.GetSize();
		CString olTmp, olWgpItemList;
		// alle Arbeitsgruppen untersuchen
		for(int i=0; i<ilWgpSize; i++)
		{
			// geh�rt die Arbeitsgruppe zur Planungsgruppe?
			if(rmViewInfo.lPlanGroupUrno == ogWgpData.omData[i].Pgpu)
			{
				// ja -> Urno der Arbeitsgruppe in Ansichts-Arbeitsgruppenliste 
				// speichern (die Liste ist ein String mit Komma als Trennzeichen
				olTmp.Format("%ld",ogWgpData.omData[i].Urno);
				rmViewInfo.oWorkGroupUrnos.Add(olTmp);
				// den Bezeichner (Code) der Arbeitsgruppe in einer tempor�ren Liste speichern
				olTmp.Format("%s",ogWgpData.omData[i].Wgpc);
				olWgpItemList += CString(",") + olTmp;
			}
		}
		// tempor�re Liste mit Arbeitsgruppencodes nachberarbeiten
		if(olWgpItemList.IsEmpty() == FALSE && olWgpItemList.GetLength() > 1)
		{
			// f�hrendes Komma eleminieren
			olWgpItemList = olWgpItemList.Mid(1);
			// sortieren nach Name (???)
			olWgpItemList = SortItemList(olWgpItemList, ',');
			// Arbeitsgruppencodes aus String in Ansichts-Liste kopieren
			ExtractItemList(olWgpItemList, &rmViewInfo.oWorkGroupCodes, ',');
		}
		
		// lade alle Funktionen, die die Planungsgruppe enth�lt 
		// (Zur Zeit nur Funktionen (PFC), sp�ter evtl. �ndern!)
		// Datensatz der Planungsgruppe lesen
		PGPDATA *prlPgp = ogPgpData.GetPgpByUrno(rmViewInfo.lPlanGroupUrno);
		// Datensatz gefunden?
		if(prlPgp != NULL)
		{
			//Zur Zeit nur Funktionen (PFC), sp�ter evtl. �ndern!!!!!!!!!!!
			// ja -> Typ 'Funktionen'?
			if(CString(prlPgp->Type) == CString("PFC"))
			{
				// ja -> 
				CString olTmp;
				// Anzahl der verschiedenen Funktionen in dieser Planungsgruppe
				int ilMembers = ExtractItemList(prlPgp->Pgpm, &rmViewInfo.oPlanGroupCodes, '|');
				// Anzahl der Mitarbeiter, die mind. pro Funktion ben�tigt werden
				ExtractItemList(prlPgp->Minm, &rmViewInfo.oPlanGroupMin, '|');
				// Anzahl der Mitarbeiter, die h�chstens pro Funktion ben�tigt werden
				ExtractItemList(prlPgp->Maxm, &rmViewInfo.oPlanGroupMax, '|');
				// Puffer f�r Funktions-Datensatz
				PFCDATA *prlPfc = NULL;
				// alle ben�tigten Funktionen durchgehen
				for(int i=0; i<ilMembers; i++)
				{
					// Funktion mit Hilfe der Bezeichnung (Code) ermitteln
					prlPfc = ogPfcData.GetPfcByFctc(rmViewInfo.oPlanGroupCodes[i]);
					// gefunden?
					if(prlPfc != NULL)
					{
						// ja -> extrahieren, formatieren und...
						olTmp.Format("%ld",prlPfc->Urno);
						// ...der Urno-Liste f�r ben�tigte Funktionen der 
						// Planungsgruppe hinzuf�gen
						rmViewInfo.oPlanGroupUrnos.Add(olTmp);
					}
					else
					{
						// Funktionsdatensatz nicht gefunden
						rmViewInfo.oPlanGroupUrnos.Add("");
					}
				}
				// Anzahl der ben�tigten Funktionen eventuell korrigieren
				ilMembers = rmViewInfo.oPlanGroupUrnos.GetSize();
				// alle Funktionen durchgehen
				for( i=0; i<ilMembers; i++)
				{
					// Funktion in der Liste (String-Sequenz mit Trennzeichen Komma)
					// suchen, die alle in der Statistik-Ist-Zeile angezeigten
					// Funktionen beinhaltet
					olTmp.Format(",%s,",rmViewInfo.oPlanGroupUrnos[i]);
					if(rmViewInfo.oPfcUrnos.Find(olTmp) == -1)
					{
						// nicht gefunden
						// Liste leer?
						if(rmViewInfo.oPfcUrnos.GetLength() == 0)
						{
							// ja -> mit Komma anfangen
							rmViewInfo.oPfcUrnos += CString(",");
						}
						// Funktions-Urno hinzuf�gen
						rmViewInfo.oPfcUrnos += rmViewInfo.oPlanGroupUrnos[i] + CString(",");
					}
				}
			}
		}
		// angezeigte Planungsstufe ermitteln (Dienstplan, Mitarbeiter-Tagesliste oder
		// Tagesendbearbeitung)
		GetSafeStringFromArray(olValues,3,olStrParam);
		rmViewInfo.oPType = olStrParam;
		rmViewInfo.oPType.TrimLeft();
	}
	else if(GetSafeStringFromArray(olValues,1,olStrParam) && (olStrParam == "CARPOOL"))
	{
		// ...Fahrgemeinschaften
		rmViewInfo.iGroup  = 3;
	}
	else
	{
		// ...keine Gruppierung
		rmViewInfo.iGroup  = 1;
	}
	// Sortierung und Anzeige �ber den angegebenen Zeitraum (SORT_PERIODE) oder
	// nach ausgew�hltem Tag?
	GetSafeStringFromArray(olValues,4,olStrParam);
	rmViewInfo.bSortDayly = (olStrParam != "SORT_PERIODE");

	// Sortierung nach Funktion/Vertrag/Name
	GetSafeStringFromArray(olValues,5,olStrParam);
	if(olStrParam == "SORT_FVN")
	{
		rmViewInfo.iSortFVN = 1;
	}
	else
	{
		rmViewInfo.iSortFVN = 0;
	}

	// getting the saved NameConfigurationString of the view
	GetSafeStringFromArray(olValues,6,olStrParam);
	rmViewInfo.oNameConfigString = olStrParam;
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// f�nfte PropPage: Konten
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// den Parameter der f�nften Propage 'Konten' lesen
	pomViewer->GetFilter("DUTYVIEW5", olValues);
	//ToDo: Durch Auswahllistbox ersetzen
	// Kontoarten hinzuf�gen
	
	GetSafeStringFromArray(olValues,0,olStrParam);
	
	int l = GetItemCount(olStrParam,'|');
	for (int j = 1; j<l; j++)
	{
		CString olSelExtKey = GetListItem(olStrParam,j,FALSE,'|');
		ACCOUNTTYPEINFO* olAccTypInfo;
		olAccTypInfo = pomAccounts->GetAccountTypeInfoByExternKey(olSelExtKey);
		if (olAccTypInfo)
		{
			int olKto = olAccTypInfo->iInternNumber;
			rmViewInfo.oAccounts.Add(olKto);
		}
	}
	// Konfliktpr�fung ein/aus
	GetSafeStringFromArray(olValues,1,olStrParam);
	
	rmViewInfo.bDoConflictCheck = olStrParam == "0";			// true - Konfliktpr�fung durchf�hren
	
	// Schichtzuordnung nur f�r Stammfunktionen pr�fen (oder f�r alle)
	GetSafeStringFromArray(olValues,2,olStrParam);
	
	rmViewInfo.bCheckMainFuncOnly = olStrParam == "1";
	
	CCS_CATCH_ALL;
}

//****************************************************************************
// GetStatPfcList: erzeugt die Liste der Funktionen, die in der
//	Statistik angezeigt werden. l�schen, die nicht mehr 
//	in der Ansicht sind. Der Parameter <rmViewInfo.oPfcStatUrnos>
//	beinhaltet alle Funktions-Urnos, die in der Statistik angezeigt
//	werden sollen.
//	R�ckgabe:	keine
//****************************************************************************

void DutyRoster_View::GetStatPfcList() 
{
	CCS_TRY;
	
	// Rettung der �bergebenen Werte
	CString olOldPfcStatUrnos;
	olOldPfcStatUrnos = rmViewInfo.oPfcStatUrnos;
	
	// Anzahl der anzuzeigenden Funktionen
	int ilCount =  ogPfcData.omData.GetSize();
	CString olTmpUrno, olTmpUrnoII, olTmpPfcStatUrnos;
	// alle Funktionen durchgehen
	for(int i = 0; i < ilCount; i++)
	{	
		// Funktions-Urno ohne Komma
		olTmpUrno.Format("%ld",ogPfcData.omData[i].Urno);
		// Funktions-Urno in Kommata
		olTmpUrnoII.Format(",%ld,",ogPfcData.omData[i].Urno);
		// muss die Funktion hinzugef�gt werden?
		if(rmViewInfo.oPfcStatUrnos.Find(olTmpUrnoII) != -1)
		{
			// ja
			olTmpPfcStatUrnos += CString(",") + olTmpUrno;
		}
	}
	// String-Liste mit Funktions-Urnos kopieren
	rmViewInfo.oPfcStatUrnos = olTmpPfcStatUrnos;
	// abschliessendes Trennzeichen hinzuf�gen, wenn Elemente enthalten
	if(rmViewInfo.oPfcStatUrnos.IsEmpty() == FALSE)
	{
		rmViewInfo.oPfcStatUrnos += CString(",");
	}
	
	// mit den ermittelten Funktions-Urnos werden die Bezeichner
	// (Codes) ermittelt, die in der Statistik ausgegeben werden
	// Code-Liste leeren
	rmViewInfo.oPfcStatFctcs.Empty();
	// Anzahl der global geladenen Funktionen
	ilCount =  ogPfcData.omData.GetSize();
	// alle Funktionen im globalen Datenhaltungsobjekt durchgehen
	for(i = 0; i < ilCount; i++)
	{	
		// Funktions-Urno ermitteln
		olTmpUrno.Format(",%ld,",ogPfcData.omData[i].Urno);
		// in der Statistik-Liste suchen
		if(rmViewInfo.oPfcStatUrnos.Find(olTmpUrno) != -1)
		{
			// gefunden -> Code ermitteln und in Statistik-Code-Liste speichern
			rmViewInfo.oPfcStatFctcs += CString(",'") + ogPfcData.omData[i].Fctc + CString("'");
		}
	}
	
	// ADO (6.6.00): Auswahl alle Schichten ohne Code einf�gen
	if(olOldPfcStatUrnos.Find("NO_PFCCODE") != -1)
	{
		// wenn gefunden -> in Liste speichern, da� alle ohne Funktion-Code angezeigt werden sollen
		rmViewInfo.oPfcStatFctcs += CString(",'NO_PFCCODE'");
	}
	
	// f�hrendes Komma eleminieren, wenn n�tig
	if(rmViewInfo.oPfcStatFctcs.IsEmpty() == FALSE && rmViewInfo.oPfcStatFctcs.GetLength() > 1)
	{
		rmViewInfo.oPfcStatFctcs = rmViewInfo.oPfcStatFctcs.Mid(1);
	}
	CCS_CATCH_ALL;
}

//****************************************************************************
// GetPfcList: generiert Liste der selektierten Funktionen
//	R�ckgabe:	keine
//****************************************************************************

void DutyRoster_View::GetPfcList() 
{
	CCS_TRY;
	CString olTmpUrno;
	
	// Anzahl aller global geladener Funktionen
	int ilCount =  ogPfcData.omData.GetSize();
	// Setze PFC-Urno-List um in PFC-Code-Liste (und Stat-Liste)
	for(int i = 0; i < ilCount; i++)
	{	
		// Funktions-Urno lesen und in Trennzeichen verpacken
		olTmpUrno.Format(",%ld,",ogPfcData.omData[i].Urno);
		// Funktions-Urno in der Liste der Ansichts-Funktions-Urnos suchen
		if(rmViewInfo.oPfcUrnos.Find(olTmpUrno) != -1)
		{
			// wenn gefunden -> Funktion-Code in Liste speichern
			rmViewInfo.oPfcFctcs += CString(",'") + ogPfcData.omData[i].Fctc + CString("'");
		}
	}
	if(rmViewInfo.oPfcUrnos.Find("NO_PFCCODE") != -1)
	{
		// wenn gefunden -> in Liste speichern, da� alle ohne Funktion-Code angezeigt werden sollen
		rmViewInfo.oPfcFctcs += CString(",'NO_PFCCODE'");
	}
	
	// f�hrende Trennzeichen eleminieren, wenn Elemente enthalten
	if(rmViewInfo.oPfcFctcs.GetLength() > 1)
	{
		rmViewInfo.oPfcFctcs    = rmViewInfo.oPfcFctcs.Mid(1);
	}
	
	CCS_CATCH_ALL;
}

//****************************************************************************
// GetStaffByOrganisations: ermittelt unter Ber�cksichtigung der eingestellten 
// Organisationseinheiten die anzuzeigenden Mitarbeiter.
//****************************************************************************

void DutyRoster_View::GetStaffByOrganisations(CStringArray& opStfUrnos, CString& opOrgUrnos, CString& opOrgDpt1s) 
{
	CCS_TRY;
	// URNOs aus opStfUrnos kopieren, falls diese schon Daten enth�lt
	// um die Schnittmenge zu bilden
	CMapStringToPtr olTmpStfUrnoMap;
	CStringArray olTmpStfUrnos;
	int ilCount =  opStfUrnos.GetSize();
	for(int i = 0; i < ilCount; i++)
	{	
		olTmpStfUrnoMap.SetAt(opStfUrnos[i],NULL);
	}
	opStfUrnos.RemoveAll();
	
	CString olTmpUrno;
	CStringArray olOrgCodes;		
	CMapStringToPtr olOrgCodeMap;	
	CMapStringToPtr olStfUrnoMap;	
	POSITION rlPos;					
	void  *prlVoid = NULL;			
	
	/////////// Organisations-Codes der anzuzeigenden Organisationen ermitteln /////////////////
	
	// Anzahl aller global geladener Organisationen
	ilCount =  ogOrgData.omData.GetSize();
	
	// Setze ORG-Urno-List um in ORG-Code-Liste (und Stat-Liste)
	for(i = 0; i < ilCount; i++)
	{	
		// Organisations-Urno lesen und in Trennzeichen verpacken
		olTmpUrno.Format(",%ld,",ogOrgData.omData[i].Urno);
		// Organisations-Urno in der Liste der Ansichts-Organisations-Urnos oder 
		// in der Liste der Statistik-Organisations-Urnos suchen
		if(	opOrgUrnos.Find(olTmpUrno) != -1)
		{
			// Bezeichnung zur Liste hinzuf�gen
			olOrgCodes.Add(ogOrgData.omData[i].Dpt1);
			// neuen Eintrag mappen
			olOrgCodeMap.SetAt(ogOrgData.omData[i].Dpt1,NULL);
		}
	}
	
	if(	opOrgUrnos.Find("NO_ORGCODE") != -1)
	{
		// Bezeichnung zur Liste hinzuf�gen
		olOrgCodes.Add("NO_ORGCODE");
		// neuen Eintrag mappen
		olOrgCodeMap.SetAt("NO_ORGCODE",NULL);
	}
	
	/////// Select Staff-Urnos with Organisation-Codes and Periode
	
	CCSPtrArray<SORDATA> olSorData;
	if(rmViewInfo.bSortDayly == false)
	{
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Evaluierung der Daten �ber den angebenen Zeitraum: es werden 
		// alle Mitarbeiter angezeigt, die an mindestens einem Tag die
		// passende Organisation haben.
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		bmLastSortDayly = false;
		void  *prlVoid = NULL;
		// alle Mitarbeiter mit der passenden Organisation im angegebenen Zeitraum ermitteln
		ogSorData.GetSorArrayByOrgArrayWithTime(&olOrgCodes, rmViewInfo.oDateFrom, rmViewInfo.oDateTo, &olSorData);
		// Anzahl der Datens�tze
		int ilSorSize = olSorData.GetSize();
		CString olTmp;
		// alle Datens�tze durchgehen
		for(int i=0; i<ilSorSize; i++)
		{
			// Mitarbeiter-Urno ermitteln...
			olTmp.Format("%ld",olSorData[i].Surn);
			// ...und in Array speichern
			olStfUrnoMap.SetAt(olTmp,NULL); //Alle MA's mit der richtigen Organisation aus den Stammdaten SOR
		}
		
		// �berpr�fen ob auch MA ohne Organisation geladen werden sollen
		if(opOrgDpt1s.Find("NO_ORGCODE") != -1)
		{
			CCSPtrArray<STFDATA> olStfData;  // Alle MA's ohne ORG - Code
			// alle Mitarbeiter ohne Organisation im angegebenen Zeitraum ermitteln
			ogSorData.GetStfWithoutOrgWithTime(rmViewInfo.oDateFrom, rmViewInfo.oDateTo, &olStfData);
			// Anzahl der Datens�tze
			int ilStfSize = olStfData.GetSize();
			// alle Datens�tze durchgehen
			for(int i=0; i<ilStfSize; i++)
			{
				// Mitarbeiter-Urno ermitteln...
				olTmp.Format("%ld",olStfData[i].Urno);
				// ...und in Array speichern
				olStfUrnoMap.SetAt(olTmp,NULL); //Alle MA's ohne Organisation aus den Stammdaten STF
			}
			olStfData.RemoveAll();
		}
		
		// Gruppierung nach Planungsgruppe?
		if(rmViewInfo.iGroup  == 2)
		{	
			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			// zus�tzlich zu den bis jetzt ermittelten Mitarbeitern alle 
			// ausw�hlen, die am Tag <rmViewInfo.oSelectDate> zu den
			// ausgew�hlten Arbeitspruppen geh�ren
			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			// Array, der die Datens�tze Zuordnung MA - Arbeitsgruppen h�lt
			CCSPtrArray<SWGDATA> olSwgData;
			// alle Mitarbeiter ausw�hlen, die in einer der Gruppen der
			// Ansicht sind 
			ogSwgData.GetSwgByWgpWithTime(&rmViewInfo.oWorkGroupCodes, rmViewInfo.oSelectDate, rmViewInfo.oSelectDate, &olSwgData);
			// Anzahl der Arbeitsgruppen
			int	ilSwgSize = olSwgData.GetSize();
			// alle gefundenen Datens�tze durchgehen
			for(i=0; i<ilSwgSize; i++)
			{
				// die Mitarbeiter-Urno ermitteln...
				olTmp.Format("%ld",olSwgData[i].Surn);
				// ...und im Array speichern
				olStfUrnoMap.SetAt(olTmp,NULL);
			}
			// aufr�umen
			olSwgData.RemoveAll();
		}
		// alle ermittelten Mitarbeiter-Urnos in die Liste
		// der Ansicht kopieren
		POSITION rlPos;
		for(rlPos = olStfUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			olStfUrnoMap.GetNextAssoc(rlPos, olTmp, (void *&)prlVoid);
			olTmpStfUrnos.Add(olTmp);  //Alle MA's mit der richtigen Organisation und evtl. Gruppe
		}
	}
	else
	{
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// tageweise Evaluierung der Daten: es werden nur diejenigen Mitarbeiter 
		// angezeigt, am ausgew�hlten Tag die passende Organisation haben
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		bmLastSortDayly = true;
		CString olWhere;	// Puffer f�r WHERE-Clause
		CCSPtrArray<DRDDATA> olDrdData;	// speichert alle DRDs an diesem Tag mit den gew�nschten Organisationen
		
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// 1. Schritt: alle Mitarbeiter ermitteln, die nach Stammdaten die 
		// passende Organisation haben am zu evaluierenden Tag.
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// alle Mitarbeiter ermitteln, die die passenden Organisationen haben
		ogSorData.GetSorArrayByOrgArrayWithTime(&olOrgCodes, rmViewInfo.oSelectDate, rmViewInfo.oSelectDate, &olSorData);
		// Anzahl der gefundenen Mitarbeiter-Organisationen-Verkn�pfungen
		int ilSorSize = olSorData.GetSize();
		CString olTmp;
		// alle Datens�tze durchgehen
		for(int i=0; i<ilSorSize; i++)
		{
			// Mitarbeiter-Urno ermitteln
			olTmp.Format("%ld",olSorData[i].Surn);
			// im Mitarbeiter-Urno-Array speichern
			olStfUrnoMap.SetAt(olTmp,NULL);
		}
		// �berpr�fen ob auch MA ohne Organisation geladen werden sollen
		if(opOrgDpt1s.Find("NO_ORGCODE") != -1)
		{
			CCSPtrArray<STFDATA> olStfData;  // Alle MA's ohne ORG - Code
			// alle Mitarbeiter ohne Organisation im angegebenen Zeitraum ermitteln
			ogSorData.GetStfWithoutOrgWithTime(rmViewInfo.oSelectDate, rmViewInfo.oSelectDate, &olStfData);
			// Anzahl der Datens�tze
			int ilStfSize = olStfData.GetSize();
			// alle Datens�tze durchgehen
			for(int i=0; i<ilStfSize; i++)
			{
				// Mitarbeiter-Urno ermitteln...
				olTmp.Format("%ld",olStfData[i].Urno);
				// ...und in Array speichern
				olStfUrnoMap.SetAt(olTmp,NULL); //Alle MA's ohne Organisation aus den Stammdaten STF
			}
			olStfData.RemoveAll();
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// 2. Schritt: alle Mitarbeiter ermitteln, die einen DRD mit der
		// passenden Organisation am zu evaluierenden Tag haben.
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Where-Statement f�r DRD-Abfrage: alle DRDs, die f�r den 
		// ausgew�hlten Tag gelten sortiert nach Mitarbeiter-Organisation, 
		// Gruppe (z.B. Ladegruppe) und Schl�ssel
		olWhere.Format("WHERE SDAY = '%s'", rmViewInfo.oSelectDate.Format("%Y%m%d"));
		//ogDrdData.ReadSpecialData(&olDrdData,(char*)(LPCTSTR)olWhere,"FCTC,DPT1,STFU",false);
		ogDrdData.ReadSpecialData(&olDrdData,(char*)(LPCTSTR)olWhere,"DPT1,DRDF,DRDT,DRRN,FCTC,PRMC,SDAY,STFU,URNO",false);
		// Anzahl der gefundenen Datens�tze
		int ilDrdSize = olDrdData.GetSize();
		// alle gefundenen DRDs dieses Tages durchgehen
		for (i = 0; i < ilDrdSize; i++)
		{
			// Mitarbeiter-Organisation dieses DRDs ermitteln
			olTmp.Format("%s",olDrdData[i].Dpt1);
			// passende Organisation?
			if(olOrgCodeMap.Lookup(olTmp,(void *&)prlVoid) == TRUE)
			{
				// Mitarbeiter-Urno ermitteln
				olTmp.Format("%ld",olDrdData[i].Stfu);
				// Mitarbeiter schon vorhanden?
				if (!(olStfUrnoMap.Lookup(olTmp,(void *&)prlVoid)))
				{
					// nein -> hinzuf�gen
					_ASSERT(prlVoid == NULL);
					olStfUrnoMap.SetAt(olTmp,NULL);
				}
			}
			else
			{
				// Eintrag untersucht -> entfernen
				olTmp.Format("%ld",olDrdData[i].Stfu);
				// Mitarbeiter vorhanden?
				if (olStfUrnoMap.Lookup(olTmp,(void *&)prlVoid))
				{
					// ja -> entfernen, da die Funktion nicht passt
					_ASSERT(prlVoid == NULL);
					olStfUrnoMap.RemoveKey(olTmp);
				}
			}
		}
		
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// 3. Schritt: zus�tzlich alle Mitarbeiter ermitteln, die an diesem
		// Tag zur ausgew�hlten Planungsgruppe geh�ren. Evaluiert werden
		// wie bei 1.) und 2.) zuerst die Stammdaten und dann die DRDs.
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		// ToDo: Auswahl der Planungsgruppen f�hrt zum zus�tzlichen Laden
		// von Mitarbeitern, demnach sind die Planungsgruppen eigentlich
		// ein zus�tzlicher Filter und geh�ren in die PropPage laden?!
		
		// Planungsgruppen evaluieren?
		if(rmViewInfo.iGroup  == 2)
		{
			// ja
			CMapStringToPtr olStfUrnoByWgpMap;	// Mitarbeiter-Urnos aus Arbeitsgruppen-Stammdaten
			CMapStringToPtr olWgpCodeMap;	// Arbeitsgruppen-Bezeichner (Codes)
			CCSPtrArray<SWGDATA> olSwgData;	// Zuordnungen Mitarbeiter-Arbeitsgruppen
			
			// Arbeitsgruppen-Bezeichner-Array initialisieren mit den zu evaluierenden
			// Arbeitsgruppen, die in der Ansicht gespeichert sind
			for(i=0; i<rmViewInfo.oWorkGroupCodes.GetSize(); i++)
			{
				olWgpCodeMap.SetAt(rmViewInfo.oWorkGroupCodes[i], NULL);
			}
			
			// alle Mitarbeiter-Arbeitsgruppen-Zuordnungen (SWG) ermitteln, die am 
			// ausgew�hlten Tag f�r die ausgew�hlten Arbeitsgruppen gelten
			ogSwgData.GetSwgByWgpWithTime(&rmViewInfo.oWorkGroupCodes, rmViewInfo.oSelectDate, rmViewInfo.oSelectDate, &olSwgData);
			// Anzahl der gefundenen Zuordnungen
			int ilSwgSize = olSwgData.GetSize();
			CString olTmp;
			// alle Zurodnungen durchgehen
			for(i=0; i<ilSwgSize; i++)
			{
				// die Mitarbeiter-Urno ermitteln und speichern
				olTmp.Format("%ld",olSwgData[i].Surn);
				olStfUrnoByWgpMap.SetAt(olTmp,NULL);
			}
			
			// alle unter 2.) ermittelten DRDs durchgehen
			for(i=0; i<ilDrdSize; i++)
			{
				// ge�ndert ADO (Feld WGPC aud DRD entfernt)
				// Arbeitsgruppe ermitteln
				//olTmp.Format("%s",olDrdData[i].Wgpc);
				// passt die Arbeitsgruppe?
				//if(olWgpCodeMap.Lookup(olTmp,(void *&)prlVoid) == TRUE)
				//{
				// ja -> in Mitarbeiter-Urno-Liste speichern
				olTmp.Format("%ld",olDrdData[i].Stfu);
				olStfUrnoMap.SetAt(olTmp,NULL); 
				// Mitarbeiter ist drin -> entfernen, damit er nicht nochmal hinzugef�gt wird (siehe unten)
				olStfUrnoByWgpMap.RemoveKey(olTmp);
				//}
				//else
				//{
				// Mitarbeiter entfernen, damit er sp�ter nicht hinzugef�gt wird (siehe unten)
				//	olTmp.Format("%ld",olDrdData[i].Stfu);
				//	olStfUrnoByWgpMap.RemoveKey(olTmp);
				//}
			}
			// alle �brigen Mitarbeiter, die die passende Arbeitsgruppenzugeh�rigkeit
			// haben, hinzuf�gen
			for(rlPos = olStfUrnoByWgpMap.GetStartPosition(); rlPos != NULL; )
			{
				olStfUrnoByWgpMap.GetNextAssoc(rlPos, olTmp, (void *&)prlVoid);
				olStfUrnoMap.SetAt(olTmp,NULL); //Alle MA's mit der richtigen Gruppe aus den Stammdaten, die im DRS nicht definiert sind 
			}
			// aufr�umen
			olStfUrnoByWgpMap.RemoveAll();
			olWgpCodeMap.RemoveAll();
			olSwgData.RemoveAll();
			olDrdData.DeleteAll();
		}
		// alle ermittelten Mitarbeiter im StringArray der Ansicht speichern
		for(rlPos = olStfUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			olStfUrnoMap.GetNextAssoc(rlPos, olTmp, (void *&)prlVoid);
			olTmpStfUrnos.Add(olTmp);  //Alle MA's mit der richtigen Organisation und evtl. Gruppe
		}
	}
	
	ilCount =  olTmpStfUrnos.GetSize();
	for(i = 0; i < ilCount; i++)
	{	
		if (olTmpStfUrnoMap.Lookup(olTmpStfUrnos[i],(void *&)prlVoid) == TRUE)
			opStfUrnos.Add(olTmpStfUrnos[i]);
	}
	// aufr�umen
	olSorData.RemoveAll();
	olOrgCodeMap.RemoveAll();
	olStfUrnoMap.RemoveAll();
	olTmpStfUrnoMap.RemoveAll();
	olTmpStfUrnos.RemoveAll();
	
	CCS_CATCH_ALL;
}

/*************************************************************************************
Liste der selektierten Organisationen generieren
*************************************************************************************/
void DutyRoster_View::GetOrgList(CString& opOrgUrnos, CString& opOrgDpt1s)
{
	CCS_TRY;
	CString olTmpUrno;
	
	// Anzahl aller global geladener Organisationen
	int ilCount =  ogOrgData.omData.GetSize();
	// Setze ORG-Urno-List um in ORG-Code-Liste (und Stat-Liste)
	for(int i = 0; i < ilCount; i++)
	{	
		// Organisations-Urno lesen und in Trennzeichen verpacken
		olTmpUrno.Format(",%ld,",ogOrgData.omData[i].Urno);
		// Organisations-Urno in der Liste der Ansichts-Organisations-Urnos suchen
		if(opOrgUrnos.Find(olTmpUrno) != -1)
		{
			// wenn gefunden -> Organisation-Code in Liste speichern
			opOrgDpt1s += CString(",'") + ogOrgData.omData[i].Dpt1 + CString("'");
		}
	}
	if(opOrgUrnos.Find("NO_ORGCODE") != -1)
	{
		// wenn gefunden -> in Liste speichern, da� alle ohne Organisation-Code angezeigt werden sollen
		opOrgDpt1s += CString(",'NO_ORGCODE'");
	}
	// f�hrende Trennzeichen eleminieren, wenn Elemente enthalten
	if(opOrgDpt1s.GetLength() > 1)
	{
		opOrgDpt1s    = opOrgDpt1s.Mid(1);
	}
	CCS_CATCH_ALL;
}

//****************************************************************************
// GetValidSortedStaff: ermittelt unter Ber�cksichtigung der eingestellten 
// Parameter (Zeiten) die anzuzeigenden Mitarbeiter und sortiert diese
// je nach Einstellung
//****************************************************************************

void DutyRoster_View::GetValidSortedStaff()
{
	CCS_TRY;
	
	// F�r die Sortierfunktion
	pcgView = this;
	
	CMapPtrToPtr olStfMap;
	void *polKey;
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Alle Mitarbeiter in das Datenhaltungsobjekt f�r Mitarbeiter <omStfData> laden.
	// <omStfData> ist Klassenmember und h�lt alle Mitarbeiterstammdaten der aktuellen
	// Ansicht.
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Anzahl der zu ladenden Mitarbeiter
	if(rmViewInfo.oStfUrnos.GetSize() > 0)
	{
		// Map der evtl. zu ladenden MA bilden
		int ilStfUrnoSize = rmViewInfo.oStfUrnos.GetSize();
		CString olTmpStfUrno;
		CObject* polDummy;
		// Alle MA aus STF durchsuchen, ob sie im Zeitraum �berhaupt Angestellt (und in der MAP) sind
		STFDATA* polStf = 0;
		for (int ilStfNr = 0; ilStfNr<ilStfUrnoSize; ilStfNr++)
		{
			olTmpStfUrno = rmViewInfo.oStfUrnos[ilStfNr];
			polStf = ogStfData.GetStfByUrno(atol(olTmpStfUrno));
			if(polStf == 0) continue;
			if(	polStf->Doem.GetStatus() == COleDateTime::valid &&
				polStf->Doem <= rmViewInfo.oDateTo &&
				(polStf->Dodm.GetStatus() != COleDateTime::valid ||
				polStf->Dodm >= rmViewInfo.oDateFrom))
			{
				if (!olStfMap.Lookup((void *)(polStf->Urno),polKey))
				{
					olStfMap.SetAt((void *)(polStf->Urno),polStf);
					omStfData.NewAt(omStfData.GetSize(), *polStf);
				}
			}
			else
			{
				// in der manuell addierten ExtraListe konnten welche stehen, die auch zeitlich nicht passen, 
				// wir sollen sie jedoch weiter behalten
				if (omExtraUrnoMap.Lookup(olTmpStfUrno,polDummy))
				{
					if (!olStfMap.Lookup((void *)(polStf->Urno),polKey))
					{
						olStfMap.SetAt((void *)(polStf->Urno),polStf);
						omStfData.NewAt(omStfData.GetSize(), *polStf);
					}
				}
			}
		}
	}
	
	if(rmViewInfo.oStfStatUrnos.GetSize() > 0)
	{
		int ilStfUrnoSize = rmViewInfo.oStfStatUrnos.GetSize();
		CString olTmpStfUrno;
		CObject* polDummy;
		STFDATA* polStf = 0;
		for (int ilStfNr = ilStfUrnoSize - 1; ilStfNr >= 0; ilStfNr--)
		{
			olTmpStfUrno = rmViewInfo.oStfStatUrnos[ilStfNr];
			polStf = ogStfData.GetStfByUrno(atol(olTmpStfUrno));
			
			if (polStf->Doem.GetStatus() == COleDateTime::valid &&
				polStf->Doem <= rmViewInfo.oDateTo &&
				(polStf->Dodm.GetStatus() != COleDateTime::valid ||
				polStf->Dodm >= rmViewInfo.oDateFrom))
			{
			}
			else
			{
				// in der manuell addierten ExtraListe konnten welche stehen, die auch zeitlich nicht passen, 
				// wir sollen sie jedoch weiter behalten
				if (!omExtraUrnoMap.Lookup(olTmpStfUrno,polDummy))
				{
					rmViewInfo.oStfStatUrnos.RemoveAt(ilStfNr);
				}
			}
		}
	}
	
	if (rmViewInfo.iSortFVN == 1)
	{
		// Mitarbeiter sortieren nach Funktion,Vertrag,Name
		omStfData.Sort(CompareStfByFctConName);
	}
	else
	{
		omStfData.Sort(CompareStfByName);
	}
	
#ifdef _DEBUG
	TRACE("CompareStfByFctConName Performance: Number of total Calls: %i, Number of Spf calls: %i, Time Spf: %i, Number of Sco calls: %i, Time Sco: %i\n", 
		giCallCompareStfByFctConName, giCallSpf, giTimeSpf, giCallSco, giTimeSco);
#endif _DEBUG
	
	/////// sortierte Mitarbeiter-Urno-Liste in View speichern
	
	// alte Liste l�schen
	rmViewInfo.oStfUrnos.RemoveAll();
	// Anzahl der Mitarbeiter
	int ilSize = omStfData.GetSize();
	// alle Mitarbeiter durchgehen
	CString olTmpUrno;
	for(int i=0; i<ilSize; i++)
	{
		// Urno formatieren...
		olTmpUrno.Format("%ld",omStfData[i].Urno);
		// ...und im Urno-Array der Ansicht speichern
		rmViewInfo.oStfUrnos.Add(olTmpUrno);
	}
	
	CCS_CATCH_ALL;
}

//****************************************************************************
// LoadAccData: l�dt die Kontodatens�tze aller Mitarbeiter, die
//	in die Ansicht geladen werden sollen.
//	R�ckgabe:	false	->	Fehler beim Laden der Daten
//				true	->	Daten geladen oder keine Daten vorhanden
//****************************************************************************

bool DutyRoster_View::LoadAccData(CMapPtrToPtr *popLoadStfUrnoMap,CString opUrnoString) 
{
	CCS_TRY;
	WriteLogFull("enter LoadAccData()");
	
	CString olAccWhere	= "";		// Puffer f�r WHERE-Clause
	
	//CString olDateYFrom = rmViewInfo.oLoadDateFrom.Format("%Y");	// Start- und Stopjahr aus Ansichtsparameter
	//CString olDateYTo   = rmViewInfo.oLoadDateTo.Format("%Y");
	CString olDateYFrom = rmViewInfo.oDateFrom.Format("%Y");	// Start- und Stopjahr aus Ansichtsparameter
	CString olDateYTo   = rmViewInfo.oDateTo.Format("%Y");
	
	// erstmal schauen, welche Mitarbeiter geladen werden sollen
	CStringArray olLoadStfUrnoArray;
	if (popLoadStfUrnoMap != NULL)
	{
		long	llStfUrno;			// die Urno als long
		void*	plDummy;			// das Objekt, immer NULL und daher obsolet
		CString olStfUrno;
		
		for(POSITION pos = popLoadStfUrnoMap->GetStartPosition(); pos != NULL;)   
		{
			popLoadStfUrnoMap->GetNextAssoc(pos,(void*&)llStfUrno,plDummy);
			olStfUrno.Format("'%ld'",llStfUrno);
			olLoadStfUrnoArray.Add(olStfUrno);
		}
	}
	else	// die urno Zeichenkette wurde benutzt
	{
		ExtractItemList(opUrnoString,&olLoadStfUrnoArray,',');
	}
	
	// mit bereits geladenem Zeitraum und Urnos vergleichen
	// einfachhalber checken wir nur, wenn ein Jahr geladen wird oder zwei nacheinandervolgende Jahre (100% aller F�lle), 
	// ansonsten einfach wieder alles laden
	if (olLoadStfUrnoArray.GetSize() != 0)
	{
		long		llStfUrno;			// die Urno als long
		CString		olStfUrno;
		
		CString olYear = olDateYFrom;
		for (;;)
		{
			for (int ilPos = olLoadStfUrnoArray.GetSize() - 1; ilPos >= 0;ilPos--)   
			{
				olStfUrno = olLoadStfUrnoArray[ilPos];
				llStfUrno = atol(olStfUrno.Mid(1,olStfUrno.GetLength()-1));								
				// Pr�fen, ob Datensatz bereits geladen wurde
				if(ogAccData.IsAccExisted(llStfUrno, olYear))
				{
					olLoadStfUrnoArray.RemoveAt(ilPos);
				}
			}
			
			if (olDateYFrom == olDateYTo || olYear == olDateYTo)
			{
				break;	// eine oder zwei Runden
			}
			else
			{
				olYear = olDateYTo;
			}
		}
	}
	
	// irgendetwas zu tun ?
	if (olLoadStfUrnoArray.GetSize() == 0)
		return true;
	
	// keine Abpr�fung erforderlich beim Laden
	ogAccData.SetLoadStfuUrnoMap(NULL);
	
	//wenn das Start und das Ende-Datum gleich sind, brauchen wir kein "BETWEEN"
	int ilMaxUrnos = olDateYFrom == olDateYTo ? 100 : 50;
	
	while (olLoadStfUrnoArray.GetSize() > 0)
	{
		int ilUrnos = 0;
		CString olTmpUrnos;
		while (ilUrnos < ilMaxUrnos && olLoadStfUrnoArray.GetSize() > 0)
		{
			ilUrnos++;
			olTmpUrnos += ',' + olLoadStfUrnoArray[0];
			olLoadStfUrnoArray.RemoveAt(0);
		}
		
		olTmpUrnos = olTmpUrnos.Mid(1);
		
		// 1 Jahr ?
		if (olDateYFrom == olDateYTo)
		{
			olAccWhere.Format("WHERE STFU IN (%s) AND YEAR = '%s'", olTmpUrnos, olDateYFrom);
		}
		else
		{
			olAccWhere.Format("WHERE STFU IN (%s) AND YEAR BETWEEN '%s' AND '%s'", olTmpUrnos,olDateYFrom, olDateYTo);
		}
		
		// Datens�tze abfragen
		if (ogAccData.Read((char*)(LPCTSTR)olAccWhere, false) == false)
		{
			// Fehler -> echter Fehler (anderer als 'Keine Datens�tze gefunden')?
			ogAccData.omLastErrorMessage.MakeLower();
			if(ogAccData.omLastErrorMessage.Find("no data found") == -1)
			{
				// ja -> ausgeben
				CString olErrorTxt;
				olErrorTxt.Format("%s %d\n%s",LoadStg(ST_READERR), ogAccData.imLastReturnCode, ogAccData.omLastErrorMessage);
				MessageBox(olErrorTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
				return false;
			}
		}
	}
	
	
	// f�r Aktualisierungen beim Brodcast-Handler anmelden
	ogAccData.Register();
	omAccLoadedDateYFrom	= olDateYFrom;
	omAccLoadedDateYTo		= olDateYTo;
	CCS_CATCH_ALL;
	return true;
}


//****************************************************************************
// LoadSdtData: l�dt die Schichtdatens�tze.
//	R�ckgabe:	false	->	Fehler beim Laden der Daten
//				true	->	Daten geladen oder keine Daten vorhanden
//****************************************************************************

bool DutyRoster_View::LoadSdtData() 
{
	CCS_TRY;
	WriteLogFull("enter LoadSdtData()");
	//----------------------------------------
	// Zugriff �ber DAO
	//omSdtData.SetAccessMethod("DAO");
	//----------------------------------------
	
	
	// R�ckgabe-Wert
	bool blReturn = true;
	// Puffer f�r WHERE-Clause
	CString	olSdtWhere;
	
	// globales Objekt zur Haltung der Datens�tze zur�cksetzen
	//uhi 8.9.00
	ogSdtData.ClearAll();
	
	// Funktionen ausgew�hlt?
	if(rmViewInfo.oPfcStatFctcs.IsEmpty() == FALSE)
	{
		// Filterkriterium Start- und Enddatum f�r Datens�tze
		CString olDateHFrom  = rmViewInfo.oDateFrom.Format("%Y%m%d000000");
		CString olDateHTo    = rmViewInfo.oDateTo.Format("%Y%m%d235959");
		// Filterkriterium ausgew�hlte Funtionen
		CString omTmpFkts	 = rmViewInfo.oPfcStatFctcs;
		// WHERE-Clause generieren
		// nur die operativen SDTs laden:
		//	- mit SDGU=0 (in "current" bzw. "oparative" in der Coverage manuell eingef�gt
		//	- oder das zweite Byte im Feld EXPD des zugeh�rigen SDG-records muss "O" (f�r operativ) oder Blank sein
		olSdtWhere.Format("WHERE FCTC IN (%s) AND ESBG BETWEEN '%s' AND '%s' AND (SDGU = 0 or (select EXPD from sdgtab where SDGTAB.URNO = SDTTAB.SDGU) in ('1 ','0 ','1O','0O'))", omTmpFkts, olDateHFrom, olDateHTo);
		
		// Datens�tze abfragen
		//if(omSdtData.Read(olSdtWhere) == false)
		//uhi 8.9.00
		if(ogSdtData.Read((char*)(LPCTSTR)olSdtWhere) == false)
		{
			// Fehler -> echter Fehler (anderer als 'Keine Datens�tze gefunden')?
			//omSdtData.omLastErrorMessage.MakeLower();
			ogSdtData.omLastErrorMessage.MakeLower();
			//if(omSdtData.omLastErrorMessage.Find("no data found") == -1)
			if(ogSdtData.omLastErrorMessage.Find("no data found") == -1)
			{
				// ja -> ausgeben
				CString olErrorTxt;
				//olErrorTxt.Format("%s %d\n%s",LoadStg(ST_READERR), omSdtData.imLastReturnCode, omSdtData.omLastErrorMessage);
				olErrorTxt.Format("%s %d\n%s",LoadStg(ST_READERR), ogSdtData.imLastReturnCode, ogSdtData.omLastErrorMessage);
				MessageBox(olErrorTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
				blReturn = false;
			}
		}	
	}
	return blReturn;
	CCS_CATCH_ALL;
	return false;
}

//****************************************************************************
// LoadDraData: l�dt die TagesAbwesenheitsdatens�tze f�r die Ansicht.
//	R�ckgabe:	false	->	Fehler beim Laden der Daten
//				true	->	Daten geladen oder keine Daten vorhanden
//****************************************************************************

bool DutyRoster_View::LoadDraData(CMapPtrToPtr *popLoadStfUrnoMap) 
{
	CCS_TRY;
	WriteLogFull("enter LoadDraData()");
	// Datens�tze ermitteln
	if(ogDraData.ReadFilteredByDateAndStaff(rmViewInfo.oLoadDateFrom,rmViewInfo.oLoadDateTo,popLoadStfUrnoMap) == false)
	{
		// Fehler -> echter Fehler (anderer als 'Keine Datens�tze gefunden')?
		ogDraData.omLastErrorMessage.MakeLower();
		if(ogDraData.omLastErrorMessage.Find("no data found") == -1)
		{
			// ja -> ausgeben
			CString olErrorTxt;
			olErrorTxt.Format("%s %d\n%s",LoadStg(ST_READERR), ogDraData.imLastReturnCode, ogDraData.omLastErrorMessage);
			MessageBox(olErrorTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
			return false;
		}
	}
	return true;
	CCS_CATCH_ALL;
	return false;
}

//****************************************************************************
// LoadDrdData: l�dt die TagesAbweichungsschichtdatens�tze f�r die Ansicht.
//	R�ckgabe:	false	->	Fehler beim Laden der Daten
//				true	->	Daten geladen oder keine Daten vorhanden
//****************************************************************************

bool DutyRoster_View::LoadDrdData(CMapPtrToPtr *popLoadStfUrnoMap) 
{
	CCS_TRY;
	WriteLogFull("enter LoadDrdData()");
	// Datens�tze ermitteln
	if(ogDrdData.ReadFilteredByDateAndStaff(rmViewInfo.oLoadDateFrom,rmViewInfo.oLoadDateTo,popLoadStfUrnoMap) == false)
	{
		// Fehler -> echter Fehler (anderer als 'Keine Datens�tze gefunden')?
		ogDrdData.omLastErrorMessage.MakeLower();
		if(ogDrdData.omLastErrorMessage.Find("no data found") == -1)
		{
			// ja -> ausgeben
			CString olErrorTxt;
			olErrorTxt.Format("%s %d\n%s",LoadStg(ST_READERR), ogDrdData.imLastReturnCode, ogDrdData.omLastErrorMessage);
			MessageBox(olErrorTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
			return false;
		}
	}
	return true;
	CCS_CATCH_ALL;
	return false;
}

//****************************************************************************
// LoadDrsData: l�dt die Datens�tze 't�gliche Erg�nzungen' f�r die Ansicht.
//	R�ckgabe:	false	->	Fehler beim Laden der Daten
//				true	->	Daten geladen oder keine Daten vorhanden
//****************************************************************************

bool DutyRoster_View::LoadDrsData(CMapPtrToPtr *popLoadStfUrnoMap) 
{
	CCS_TRY;
	WriteLogFull("enter LoadDrsData()");
	// Datens�tze ermitteln
	if(ogDrsData.ReadFilteredByDateAndStaff(rmViewInfo.oLoadDateFrom,rmViewInfo.oLoadDateTo,popLoadStfUrnoMap) == false)
	{
		// Fehler -> echter Fehler (anderer als 'Keine Datens�tze gefunden')?
		ogDrsData.omLastErrorMessage.MakeLower();
		if(ogDrsData.omLastErrorMessage.Find("no data found") == -1)
		{
			// ja -> ausgeben
			CString olErrorTxt;
			olErrorTxt.Format("%s %d\n%s",LoadStg(ST_READERR), ogDrsData.imLastReturnCode, ogDrsData.omLastErrorMessage);
			MessageBox(olErrorTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
			return false;
		}
	}
	return true;
	CCS_CATCH_ALL;
	return false;
}

//****************************************************************************
//****************************************************************************

bool DutyRoster_View::LoadSprData(CMapPtrToPtr *popLoadStfUrnoMap) 
{
	CCS_TRY;
	CString olCustomer = ogCCSParam.GetParamValue(ogAppl,"ID_PROD_CUSTOMER");
	CString olNoLoad = CString("ZRH,GVA,BSL");
	if (olNoLoad.Find (olCustomer) > -1)
		return true;	// in ZRH,GVA und BSL nicht laden
	
	// Datens�tze ermitteln
	if(ogSprData.ReadFilteredByDateAndStaff(rmViewInfo.oLoadDateFrom,rmViewInfo.oLoadDateTo,popLoadStfUrnoMap) == false)
	{
		// Fehler -> echter Fehler (anderer als 'Keine Datens�tze gefunden')?
		ogSprData.omLastErrorMessage.MakeLower();
		if(ogSprData.omLastErrorMessage.Find("no data found") == -1)
		{
			// ja -> ausgeben
			CString olErrorTxt;
			olErrorTxt.Format("%s %d\n%s",LoadStg(ST_READERR), ogSprData.imLastReturnCode, ogSprData.omLastErrorMessage);
			MessageBox(olErrorTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
			return false;
		}
	}
	return true;
	CCS_CATCH_ALL;
	return false;
}

//****************************************************************************
// LoadDrrData: l�dt die Tagesschichtdatens�tze f�r die Ansicht.
//	R�ckgabe:	false	->	Fehler beim Laden der Daten
//				true	->	Daten geladen oder keine Daten vorhanden
//****************************************************************************

bool DutyRoster_View::LoadDrrData(CMapPtrToPtr *popLoadStfUrnoMap) 
{
	CCS_TRY;
	// Datens�tze ermitteln
	if(ogDrrData.ReadFilteredByDateAndStaff(rmViewInfo.oLoadDateFrom,rmViewInfo.oLoadDateTo,popLoadStfUrnoMap) == false)
	{
		// Fehler -> echter Fehler (anderer als 'Keine Datens�tze gefunden')?
		ogDrrData.omLastErrorMessage.MakeLower();
		if(ogDrrData.omLastErrorMessage.Find("no data found") == -1)
		{
			// ja -> ausgeben
			CString olErrorTxt;
			olErrorTxt.Format("%s %d\n%s",LoadStg(ST_READERR), ogDrrData.imLastReturnCode, ogDrrData.omLastErrorMessage);
			MessageBox(olErrorTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
			return false;
		}
	}
	return true;
	CCS_CATCH_ALL;
	return false;
}

//****************************************************************************
// LoadDrgData: l�dt die Arbeitsgruppendatens�tze f�r die Ansicht.
//	R�ckgabe:	false	->	Fehler beim Laden der Daten
//				true	->	Daten geladen oder keine Daten vorhanden
//****************************************************************************

bool DutyRoster_View::LoadDrgData(CMapPtrToPtr *popLoadStfUrnoMap) 
{
	CCS_TRY;
	WriteLogFull("enter LoadDrgData()");
	// Datens�tze ermitteln
	if(ogDrgData.ReadFilteredByDateAndStaff(rmViewInfo.oLoadDateFrom,rmViewInfo.oLoadDateTo,popLoadStfUrnoMap) == false)
	{
		// Fehler -> echter Fehler (anderer als 'Keine Datens�tze gefunden')?
		ogDrgData.omLastErrorMessage.MakeLower();
		if(ogDrgData.omLastErrorMessage.Find("no data found") == -1)
		{
			// ja -> ausgeben
			CString olErrorTxt;
			olErrorTxt.Format("%s %d\n%s",LoadStg(ST_READERR), ogDrgData.imLastReturnCode, ogDrgData.omLastErrorMessage);
			MessageBox(olErrorTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
			return false;
		}
	}
	return true;
	CCS_CATCH_ALL;
	return false;
}

//****************************************************************************
// LoadData: l�dt unter Ber�cksichtigung aller relevanten Mitarbeiter die 
//	Daten zur Dienstplanung aus der Datenbank.
// R�ckgabe:	keine
//****************************************************************************

void DutyRoster_View::LoadData()
{
	WriteLogFull("enter LoadData()");
	// Map mit den zu ladenden MA-Urnos
	CMapPtrToPtr olLoadStfUrnoMap;
	// tempor�res Array mit allen Urnos erzeugen
	CStringArray olTmpStfUrnoArray;
	// Array mit URNOS wird zugef�gt.
	olTmpStfUrnoArray.Append(rmViewInfo.oStfUrnos);
	// alte URNO Liste wird gel�scht
	rmViewInfo.oStfUrnos.RemoveAll();
	if(olTmpStfUrnoArray.GetSize() > 0)
	{
		CMapStringToPtr olTempStfUrnoMap;
		// Anzahl der Urnos ermiteln
		int ilUrnos = olTmpStfUrnoArray.GetSize();
		//Lade Daten //////////////////////////////////////////////////////
		if(ilUrnos>90) //max. ilUrnos>99
		{
			///////////////////////////////////////////////////////////////
			//Gro�e Urno-Liste = Lade alle Daten OHNE Urno-Liste im Zeitraum.
			// Map wird an LoadRosData �bergeben,
			// in die Map werden als Key alle Mitarbeiter URNOS eingetragen
			for(int ili=0; ili<ilUrnos ;ili++)
			{
				olLoadStfUrnoMap.SetAt((void *)atol(olTmpStfUrnoArray[ili]), NULL);
				olTempStfUrnoMap.SetAt(olTmpStfUrnoArray[ili], NULL);
			}
			// Tagesschichtdaten
			LoadDrrData(&olLoadStfUrnoMap);
			// Erg�nzungen
			LoadDrsData(&olLoadStfUrnoMap);
			// Abweichungen
			LoadDrdData(&olLoadStfUrnoMap);
			// Abwesenheiten
			LoadDraData(&olLoadStfUrnoMap);
			// Arbeitsgruppen
			LoadDrgData(&olLoadStfUrnoMap);
			// Arbeiitszeiterfassung
			LoadSprData(&olLoadStfUrnoMap);
			
			///////////////////////////////////////////////////
			// An- oder Abwesenheit �berpr�fen
			// rmViewInfo.iPresent (1 = ALL, 2 = PRESENT, 3 = NOTPRESENT)
			CString olTempStf;
			POSITION rlTempPos;
			void  *prlVoid = NULL;
			
			// Schleife f�r alle Mitarbeiter
			for(rlTempPos = olTempStfUrnoMap.GetStartPosition(); rlTempPos != NULL; )
			{
				olTempStfUrnoMap.GetNextAssoc(rlTempPos, olTempStf, (void *&)prlVoid);
				bool blAdd = true;
				
				// Nur ausf�hren wenn eh nicht alles angezeigt werden soll.
				if(rmViewInfo.iPresent != 1)
				{
					long llStfUrno = atol(olTempStf);
					blAdd = MustStaffInPeriode(llStfUrno);
				}
				// Mitarbeiter mu� angezeigt werden
				if(blAdd)
				{
					// In die URNO Liste des VIEWS eintragen, die vorher gel�scht wurde
					rmViewInfo.oStfUrnos.Add(olTempStf);
				}
				// Mitarbeiter mu� nicht angezeigt werden
				else
				{	// Alle Daten die mit diesem Mitarbeiter zu tun haben m�ssen
					// gel�scht werden
					DeleteStaffFromData(atol(olTempStf));
				}
			}
			
			// Maps l�schen
			olLoadStfUrnoMap.RemoveAll();
			olTempStfUrnoMap.RemoveAll();
			
			// Warum wird denn jetzt die Map wieder gef�llt ?
			// Anzahl der Mitarbeiter URNOS in der View		
			ilUrnos = rmViewInfo.oStfUrnos.GetSize();
			for(ili=0; ili<ilUrnos ;ili++)
			{
				olLoadStfUrnoMap.SetAt((void *)atol(rmViewInfo.oStfUrnos[ili]), NULL);
			}
			/////////////////////////////////////////////////////////////////////////////////////////////////
			if(olLoadStfUrnoMap.GetCount() > 0)
			{
				// Arbeitskontodaten laden
				LoadAccData(&olLoadStfUrnoMap);
				// Schichtdatens�tze laden
				LoadSdtData();
				// Wunschdatens�tze laden
				LoadDrwData(&olLoadStfUrnoMap);
			}
		}
		else
		{
			////////////////////////////////////////////////////////////////
			//Kleine Urno-Liste (max.99) = Lade alle Daten MIT Urno-Liste im Zeitraum
			CString olTmpCharUrnos;
			CString olTmp;
			// f�r jeden Mitarbeiter in dieser View
			for(int ili=0; ili<ilUrnos ;ili++)
			{
				olTmp = olTmpStfUrnoArray[ili];
				olTmpCharUrnos += CString(",'") + olTmp + CString("'");
				olTempStfUrnoMap.SetAt(olTmp,NULL);
				// MA-Urno-Map f�r DRR laden
				olLoadStfUrnoMap.SetAt((void *)atol(olTmp), NULL);
			}
			if(olTmpCharUrnos.GetLength() > 1)
				olTmpCharUrnos = olTmpCharUrnos.Mid(1);
			else
				olTmpCharUrnos = "0";
			// Wunschdatens�tze laden
			LoadDrwData(NULL,olTmpCharUrnos);
			// Tagesschichtdaten
			LoadDrrData(&olLoadStfUrnoMap);
			// Erg�nzungen
			LoadDrsData(&olLoadStfUrnoMap);
			// Abweichungen
			LoadDrdData(&olLoadStfUrnoMap);
			// Abwesenheiten
			LoadDraData(&olLoadStfUrnoMap);
			// Arbeitsgruppen
			LoadDrgData(&olLoadStfUrnoMap);
			// Arbeiitszeiterfassung
			LoadSprData(&olLoadStfUrnoMap);
			
			///////////////////////////////////////////////////
			// An- oder Abwesenheit �berpr�fen
			// rmViewInfo.iPresent (1 = ALL, 2 = PRESENT, 3 = NOTPRESENT)
			olTmpCharUrnos.Empty();
			CString olTmpNumberUrnos;
			CString olTempStf;
			POSITION rlTempPos;
			void  *prlVoid = NULL;
			for(rlTempPos = olTempStfUrnoMap.GetStartPosition(); rlTempPos != NULL; )
			{
				olTempStfUrnoMap.GetNextAssoc(rlTempPos, olTempStf, (void *&)prlVoid);
				bool blAdd = true;
				if(rmViewInfo.iPresent != 1)
				{
					long llStfUrno = atol(olTempStf);
					blAdd = MustStaffInPeriode(llStfUrno);
				}
				
				if(blAdd)
				{
					// In die URNO Liste des VIEWS eintragen, die vorher gel�scht wurde
					rmViewInfo.oStfUrnos.Add(olTempStf);
					olTmpCharUrnos   += CString(",'") + olTempStf + CString("'");
					olTmpNumberUrnos += CString(",")  + olTempStf + CString("");
				}
				else
				{	
					// L�sche MA wenn An- oder Abwesenheit nicht dem Kriterium endspricht
					// Alle Daten die mit diesem Mitarbeiter zu tun haben m�ssen
					// gel�scht werden
					DeleteStaffFromData(atol(olTempStf));
				}
			}
			if(olTmpCharUrnos.GetLength() > 1 && olTmpNumberUrnos.GetLength() > 1)
			{
				olTmpCharUrnos = olTmpCharUrnos.Mid(1);
				olTmpNumberUrnos = olTmpNumberUrnos.Mid(1);
			}
			else
			{
				olTmpCharUrnos = "0";
				olTmpNumberUrnos = "0";
			}
			olTempStfUrnoMap.RemoveAll();
			/////////////////////////////////////////////////////////////////////////////////////////////////
			if(olTmpCharUrnos.IsEmpty() == FALSE && olTmpNumberUrnos.IsEmpty() == FALSE)
			{
				// Arbeitskontodaten laden
				LoadAccData(NULL,olTmpCharUrnos);

				// Schichtdatens�tze laden
				LoadSdtData();
			}
		}
		
		//////////////////////////////////////////////////////////////////////
		// ShiftCheck-Sektion
		
		ogShiftCheck.SetCheckDate(rmViewInfo.oShowDateFrom, rmViewInfo.oShowDateTo);
		ogShiftCheck.SetViewEName(rmViewInfo.iEName);
		ogShiftCheck.FillWorkTimeData();
		if(ogShiftCheck.IsWarning())
		{
			// wenn Schichten kollidieren, werden sie nicht gespeichert und in Warnung angegeben
			ForwardWarnings();
		}
		// nach dem FillWorkTimeData() m�ssen und k�nnen wir Check durchf�hren
		// jedoch zu Ausgabe erlauben wir nur die AttentionBox, keine MessageBox
		/*	BDA Das Pr�fen der Arbeitsregeln nach dem Daten geladen werden:
		if(rmViewInfo.bDoConflictCheck)
		{
		int ilOutputOld = ogShiftCheck.imOutput;
		ogShiftCheck.imOutput = OUT_WARNING_AS_WARNING | OUT_ERROR_AS_WARNING;	// Fehler sollen nur als Warnungen ausgegeben werden
		
		  ogShiftCheck.DutyRosterCheckIt(0,0,true);
		  
			ogShiftCheck.imOutput = ilOutputOld;
			
			  // Nach dem ogShiftCheck.DutyRosterCheckIt() m�ssen wir pr�fen, ob Warnungen vorliegen, wenn ja, ausgeben
			  if(ogShiftCheck.IsWarning())
			  ForwardWarnings();
			  }
		*/
	}
	if(ogErrlog.GetLength())
	{
		InsertAttention(ogErrlog);
		
		WriteInErrlogFile();
	}
}

//****************************************************************************
// Ermittelt die Auswahl in der Combobox
//****************************************************************************

CString DutyRoster_View::GetSelectedView()
{
	// die ComboBox mit den Ansichten ermitteln
	CComboBox* polComboBox = ((DutyRoster_Frm*) GetParentFrame())->GetComboBox();
	
	// Name der Ansicht in der Combobox (wird zum Laden der Parameter ben�tigt)
	CString olViewName;
	
	// g�ltige Ansicht ausgew�hlt?
	int ilIndex = polComboBox->GetCurSel();
	if(ilIndex == CB_ERR)	
		return ("CB_ERR"); 
	
	// Namen der Ansicht lesen
	polComboBox->GetLBText(ilIndex, olViewName);
	
	return (olViewName);
}


/****************************************************************************
OnSelchange_CB_View: behandelt die �nderung der Einstellungen der
Ansicht. Alle Parameter (die in den PSDutyRosterXYViewPages eingegeben
werden) werden neu geladen und die angezeigten Daten entsprechend
gefiltert.
R�ckgabe:	keine

****************************************************************************/

void DutyRoster_View::OnSelchange_CB_View() 
{
	CCS_TRY;
	CString olViewName;
	bool blReloadView = false;
	
	// momentane Ansicht speichern zur eventuellen sp�teren Wiederherstellung
	VIEWINFO rmOldViewInfo;
	CopyViewInfo(rmOldViewInfo);
	
	// momentane Ansicht auf Default-Werte zur�cksetzen
	InitViewInfo();
	
	// Titel des Frames setzen
	CRosteringDoc *polDoc = (CRosteringDoc *)GetDocument();
	if(polDoc != NULL)
	{
		polDoc->SetTitle(LoadStg(IDS_STRING1587));
	}
	
	// Ermittlungs des Names der ausgew�hlten View
	// Name der Ansicht in der Combobox (wird zum Laden der Parameter ben�tigt)
	olViewName = GetSelectedView();
	
	if (olViewName == "CB_ERR") 
	{
		TRACE ("Viewname konnte nicht ermittelt werden\n");
		WriteInRosteringLog(LOGFILE_FULL, "Viewname konnte nicht ermittelt werden");
		
		return;
	}
	
	/*
	omExtraUrnoListInView:
	
	  --view------view------------alte Liste---------neue Liste
	  
		Actions beim Aufruf der View-Dialog und bei ComboBox Selektion
		
		  default	->	old1 (existed)	-					aus View lesen			pomViewer->bmNewView = false
		  default	->	default			-					-						pomViewer->bmNewView = false
		  old1	->	default			l�schen				-						pomViewer->bmNewView = false
		  old1	->	new				l�schen				-						pomViewer->bmNewView = true
		  old1	->	old1			-					-						pomViewer->bmNewView = false
		  old1	->	old2 (existed)	l�schen				aus View lesen			pomViewer->bmNewView = false
		  
	*/
	// was soll mit omExtraUrnoListInView gemacht werden?
	if (omOldViewName == LoadStg(IDS_STRING562) && omOldViewName != olViewName ||
		omOldViewName != LoadStg(IDS_STRING562) && 
		olViewName != LoadStg(IDS_STRING562) &&
		omOldViewName != olViewName && 
		pomViewer->bmNewView == false)
	{
		if (!bmReadUrnoList)
		{
			blReloadView = true;
		}
		bmReadUrnoList = true;
	}
	else
	{
		if (bmReadUrnoList)
		{
			blReloadView = true;
		}
		bmReadUrnoList = false;
	}
	pomViewer->bmNewView = false;
	
	// Ansicht im Viewer ausw�hlen
	pomViewer->SelectView(olViewName);
	omOldViewName = olViewName;
	
	// Filter-Parameter aus PropertyPages ermitteln
	GetPropPageParams(rmOldViewInfo);

	// Liste aller selektierten Funktionen ausf�llen
	GetPfcList();
	
	// Liste der Funktionen erzeugen, die in der Statistik (untere Tabelle) 
	// angezeigt werden
	GetStatPfcList();
	
	// Liste der selektierten Organisationen generieren
	GetOrgList(rmViewInfo.oOrgUrnos, rmViewInfo.oOrgDpt1s);
	GetOrgList(rmViewInfo.oOrgStatUrnos, rmViewInfo.oOrgStatDpt1s);
	
	// WarteDialog und -Cursor einblenden.
	CWaitDlg plWaitDlg(this,LoadStg(WAIT_DATA_PREPARE));
	
	//Anzeige im Info Fenster bezogen auf aktuelle View -> alter Schrott raus
	omAttentionInfo.DeleteAll();
	if (pomAttentionDlg != NULL)
	{
		pomAttentionDlg->ResetAttentionDlg();
	}

	// setting the name-configstring
	pogNameConfigurationDlg->SetConfigString(rmViewInfo.oNameConfigString);
	
	////////////////////////////////////////////////////////////////////////////////////////////////
	/////// Select Data with Filter ////////////////////////////////////////////////////////////////
	
	// Start- und Enddatum g�ltig und Funktionen ausgew�hlt und Planungsstufen ausgew�hlt?
	if ((rmViewInfo.oDateFrom.GetStatus()	== COleDateTime::valid) &&
		(rmViewInfo.oDateTo.GetStatus()		== COleDateTime::valid) &&
		(rmViewInfo.oPfcUrnos.IsEmpty()		== FALSE || rmViewInfo.oPfcStatUrnos.IsEmpty() == FALSE) &&
		(rmViewInfo.oOrgUrnos.IsEmpty()		== FALSE || rmViewInfo.oOrgStatUrnos.IsEmpty() == FALSE) &&
		(rmViewInfo.oPType.IsEmpty()		== FALSE))
	{
		
		// ja -> neue Ansicht generieren
		// Schichtdaten neu laden, wenn sich die Ansicht ge�ndert hat,
		// d.h. eine neue Mitarbeiterliste generiert wurde
		// haben sich die allgemeinen Filterkriterien ge�ndert?
		if (bmReadUrnoList	||
			blReloadView	||
			rmOldViewInfo.oDateFrom			!= rmViewInfo.oDateFrom			||
			rmOldViewInfo.oDateTo			!= rmViewInfo.oDateTo			||
			rmOldViewInfo.iPresent			!= rmViewInfo.iPresent			||
			rmOldViewInfo.oPfcUrnos			!= rmViewInfo.oPfcUrnos			||
			rmOldViewInfo.oPfcStatUrnos		!= rmViewInfo.oPfcStatUrnos		||
			rmOldViewInfo.oOrgUrnos			!= rmViewInfo.oOrgUrnos			||
			rmOldViewInfo.oOrgStatUrnos		!= rmViewInfo.oOrgStatUrnos		||
			rmOldViewInfo.oACT_ICAOCode		!= rmViewInfo.oACT_ICAOCode		||
			rmOldViewInfo.oALT_ICAOCode		!= rmViewInfo.oALT_ICAOCode		||
			rmOldViewInfo.bSelectAllFunc	!= rmViewInfo.bSelectAllFunc	||
			rmOldViewInfo.iSortFVN			!= rmViewInfo.iSortFVN			||
			rmViewInfo.bSortDayly			!= bmLastSortDayly				||
			(rmOldViewInfo.oPType			!= rmViewInfo.oPType && (rmViewInfo.iPresent != 1 || rmOldViewInfo.iPresent != rmViewInfo.iPresent)) ||
			bmChangeDay == true)
		{
			// ja -> Daten m�ssen geladen werden
			// vorher alte view l�schen
			omExtraUrnoListInView.RemoveAll();
			omExtraUrnoMap.RemoveAll();
			omDispExtraUrnoMap.RemoveAll();
			blReloadView = true;
			
			// manuell hinzugef�gte Mitarbeiter holen
			GetUrnoListFromView(omExtraUrnoListInView);
			for(int ilStfNr = 0; ilStfNr < omExtraUrnoListInView.GetSize(); ilStfNr++)
			{
				omExtraUrnoMap.SetAt(omExtraUrnoListInView.GetAt(ilStfNr),(CObject*)0);
			}
			
			// Filter auf die MA anwenden
			// omStfData u. rmViewInfo.oStfUrnos f�llen
			// Mitarbeiter-Liste (Klassenmember) l�schen, wird neu erstellt in GetValidSortedStaff()
			omStfData.DeleteAll();
			// rmViewInfo.oStfUrnos wird bis in GetValidSortedStaff() weitergereicht um
			// die Schnittmenge zu ermitteln
			// mu� initial gef�llt sein mit allen MA
			rmViewInfo.oStfUrnos.RemoveAll();
			rmViewInfo.oStfStatUrnos.RemoveAll();
			int ilStfDataSize = ogStfData.omData.GetSize();
			CString olTmpUrno;
			for (int ilx = 0; ilx < ilStfDataSize; ilx++)
			{	
				olTmpUrno.Format("%ld", ogStfData.omData[ilx].Urno);
				// ...und im Urno-Array der Ansicht speichern
				rmViewInfo.oStfUrnos.Add(olTmpUrno);
				rmViewInfo.oStfStatUrnos.Add(olTmpUrno);
			}
			
			// 1. MA nach Funktionen oder Gruppen filtern
			GetStaffByFunctionsOrGroups(rmViewInfo.oPfcFctcs, rmViewInfo.oStfUrnos, rmViewInfo.oPfcUrnos);
			GetStaffByFunctionsOrGroups(rmViewInfo.oPfcStatFctcs, rmViewInfo.oStfStatUrnos, rmViewInfo.oPfcStatUrnos);
			
			// 2. MA nach Organisationseinheit filtern
			GetStaffByOrganisations(rmViewInfo.oStfUrnos, rmViewInfo.oOrgUrnos, rmViewInfo.oOrgDpt1s);
			GetStaffByOrganisations(rmViewInfo.oStfStatUrnos, rmViewInfo.oOrgStatUrnos, rmViewInfo.oOrgStatDpt1s);
			
			// 3. durch gespeicherte Daten die Liste erweitern
			MergeStringArrays(rmViewInfo.oStfUrnos,rmViewInfo.oStfStatUrnos);	// alle Stat- anzeigen
			if (omExtraUrnoListInView.GetSize())
			{
				SubstStringArrays(omExtraUrnoListInView, rmViewInfo.oStfUrnos, &omExtraUrnoMap);
				if(omExtraUrnoListInView.GetSize())
				{
					MergeStringArrays(rmViewInfo.oStfUrnos,omExtraUrnoListInView);	// alle Manual- anzeigen
				}
			}
			
			// 4. (immer letzter Filter) MA nach Ein- und Austrittsdatum filtern und Sortieren
			GetValidSortedStaff();
			
			// ab hier sind alle neu zu ladende MAs im rmViewInfo.oStfUrnos
			if (ogCCSParam.GetParamValue("GLOBAL","ID_ENABLE_ACC") == "Y")
			{
				SendViewStfuToScbhdl(&rmOldViewInfo, &rmViewInfo);
			}
			//---------------------------------------------
			// Ermitteln aller relevanten Mitarbeiter Urnos
			// und l�dt enstprechend DRR.
			LoadData();
			//---------------------------------------------
		}
		else
		{
			// nein -> tu nichts
			// MA-Liste wiederherstellen, wurde weiter oben in InitViewInfo gel�scht
			rmViewInfo.oStfUrnos.Append(rmOldViewInfo.oStfUrnos);
			rmViewInfo.oStfStatUrnos.Append(rmOldViewInfo.oStfStatUrnos);
			
			if(rmOldViewInfo.oPfcStatFctcs != rmViewInfo.oPfcStatFctcs) 
			{
				// es m�ssen lediglich die Schichtdaten neu geladen werden,
				// weil andere Funktionen angezeigt werden sollen
				LoadSdtData();
			}
		}
		
		// 	omDispExtraUrnoMap f�llen (	// Map mit allen MAs, die in View �ber Display oder manuell aufgenommen wurden)
		CStringArray olStfDispExtUrnos;
		olStfDispExtUrnos.Copy(rmViewInfo.oStfUrnos);
		SubstStringArrays(olStfDispExtUrnos, rmViewInfo.oStfStatUrnos);
		
		for(int ilStfNr = 0; ilStfNr < olStfDispExtUrnos.GetSize(); ilStfNr++)
		{
			omDispExtraUrnoMap.SetAt(olStfDispExtUrnos.GetAt(ilStfNr),(CObject*)0);
		}
		
		// Sortierung t�glich und nach Arbeitsgruppen?
		if(rmViewInfo.iGroup == 2)
		{
			FillGroupStruct();
			if(rmViewInfo.bSortDayly == true)
			{
				// ja -> Arbeitsgruppen laden
				pomSolidTable->DragDropRevoke();
				// Arbeitsgruppen-Dialog anzeigen...
				if(pomDutyDragDropDlg == NULL)
				{
					pomDutyDragDropDlg = new DutyDragDropDlg(this);
				}
				else
				{
					// ...oder anpassen
					pomDutyDragDropDlg->ChangeView();
				}
				pomSolidTable->DropRegister(DIT_FROMGROUP_TODUTYROSTER);//DIT_FROMDUTYROSTER_TOGROUP
			}
			else
			{
				if(pomDutyDragDropDlg != NULL)
				{
					pomDutyDragDropDlg->InternalCancel();
					delete pomDutyDragDropDlg;
					pomDutyDragDropDlg = 0;
					
					pomSolidTable->DragDropRevoke();
				}
			}
		}
		else
		{
			if(pomDutyDragDropDlg != NULL)
			{
				pomDutyDragDropDlg->InternalCancel();
				delete pomDutyDragDropDlg;
				pomDutyDragDropDlg = 0;
				
				pomSolidTable->DragDropRevoke();
			}
		}
		
		CString olWindowText;
		olWindowText.Format(LoadStg(IDS_STRING1607),rmViewInfo.oDateFrom.Format("%d.%m.%Y"),rmViewInfo.oDateTo.Format("%d.%m.%Y"));
		CRosteringDoc *polDoc = (CRosteringDoc *)GetDocument();
		if(polDoc != NULL)
		{
			polDoc->SetTitle(olWindowText);
		}
		
		rmViewInfo.bCorrectViewInfo	= true;
		
		// Konten aktualisieren
		FillAccountStruct();
		
		// Info und Warnungen lesen...
		CStringArray olAttentionTextArray;
		pomAccounts->GetAttentionText(&olAttentionTextArray);
		for(int iAT=0; iAT<olAttentionTextArray.GetSize(); iAT++)
		{
			// ...im Warnungs-Array speichern...
			InsertAttention(olAttentionTextArray[iAT],RED);
		}
		// ...und aus dem AZK-Objekt l�schen
		pomAccounts->DeleteAttentionText();
		pomSolidTabViewer->ChangeView(!bmChangeDay);
		pomIsTabViewer->ChangeView(!bmChangeDay);
		pomStatTabViewer->ChangeView(!bmChangeDay);
		pomDebitTabViewer->ChangeView(!bmChangeDay);
		
		// Grids in Kennzahl-Dialog f�llen
		WriteLogFull("Grids in Kennzahl-Dialog f�llen");
		CCharacteristicValueDlg *polDlg = ((DutyRoster_Frm*)GetParentFrame())->GetCValDlg();
		
		if (polDlg != NULL)
		{
			polDlg->pomStfData = &omStfData;
			polDlg->pomViewInfo = &rmViewInfo;
			polDlg->pomAccounts = pomAccounts;
			polDlg->FillGrids();
		}
	}
	else
	{
		// nein -> mit den vorhandenen Parametern kann keine
		// Ansicht generiert werden -> leere Ansicht erzeugen
		GenerateEmptyView();
		// ab hier sind alle neu zu ladende MAs im rmViewInfo.oStfUrnos
		if (ogCCSParam.GetParamValue("GLOBAL","ID_ENABLE_ACC") == "Y")
		{
			SendViewStfuToScbhdl(&rmOldViewInfo, &rmViewInfo);
		}

		// Grids in Kennzahl-Dialog f�llen
		CCharacteristicValueDlg *polDlg = ((DutyRoster_Frm*)GetParentFrame())->GetCValDlg();
		if (polDlg != NULL)
		{
			polDlg->ClearGrids();
		}
	}
	
	bmChangeDay = false;
	
	// Dem CodeDialog (Schwebedialog der alle Codes enth�lt) m�ssen die neuen 
	// Zeiten mitgeteilt werden, damit er die Schichten neu anzeigen kann.
	// Zugriff auf die Codedialoge per Mainframe
	CMainFrame* polMainFrame = (CMainFrame*) GetParentFrame()->GetParentFrame();
	
	//	polMainFrame->ChangeValidDatesFromDuty(	true,rmViewInfo.oDateFrom,rmViewInfo.oDateTo,rmViewInfo.oOrgDpt1s,rmViewInfo.oPfcFctcs, rmViewInfo.bAllCodes);
	polMainFrame->ChangeValidDatesFromDuty(	true,rmViewInfo.oDateFrom,rmViewInfo.oDateTo,rmViewInfo.oOrgStatDpt1s,rmViewInfo.oPfcStatFctcs, rmViewInfo.bAllCodes);
	WriteLogFull("end of OnSelchange_CB_View()");	
	CCS_CATCH_ALL;
}

//****************************************************************************
// leere Ansicht erzeugen 
//****************************************************************************

void DutyRoster_View::GenerateEmptyView()
{
	rmViewInfo.bCorrectViewInfo	= false;
	
	// Schicht- und Mitarbeiterdaten zur�cksetzen
	omStfData.DeleteAll();
	// Tagesschichtdaten zur�cksetzen
	ogDrrData.ClearAll(true);
	
	// falls der ???-Dialog noch aktiv ist...
	if(pomDutyDragDropDlg != NULL)
	{
		// ...beenden und D'n'D-Aktionen abbrechen
		pomDutyDragDropDlg->InternalCancel();
		delete pomDutyDragDropDlg;
		pomDutyDragDropDlg = 0;
		
		pomSolidTable->DragDropRevoke();
		
	}
	
	// Konten l�schen
	pomAccounts->DeleteRunningAccounts();
	
	// Tabellen neu zeichnen
	pomSolidTabViewer->ChangeView();
	pomIsTabViewer->ChangeView();
	pomStatTabViewer->ChangeView();
	pomDebitTabViewer->ChangeView();
}

//****************************************************************************
// OnCancel
//****************************************************************************

void DutyRoster_View::OnCancel() 
{
	// Window schlie�en. (Es wird auf das MDI Frame Window zugegriffen)
	((CMDIChildWnd*) GetParentFrame())->MDIDestroy();
}

//****************************************************************************
// HorzTableScroll
//****************************************************************************

LONG DutyRoster_View::HorzTableScroll(UINT wParam, LONG lParam) 
{
	CCS_TRY;
	MODIFYTAB *prlModiTab = (MODIFYTAB *)lParam;
	
	if(wParam == imIsTableID)
	{
		pomIsTabViewer->HorzTableScroll(prlModiTab);
		pomDebitTabViewer->HorzTableScroll(prlModiTab);
	}
	if(wParam == imStatTableID)
	{
		pomStatTabViewer->HorzTableScroll(prlModiTab);
	}
	return 0L;
	CCS_CATCH_ALL;
	return 0L;
}

//****************************************************************************
// VertTableScroll
//****************************************************************************

LONG DutyRoster_View::VertTableScroll(UINT wParam, LONG lParam) 
{
	CCS_TRY;
	MODIFYTAB *prlModiTab = (MODIFYTAB *)lParam;
	
	if(wParam == imIsTableID)
	{
		pomSolidTabViewer->VertTableScroll(prlModiTab);
		pomIsTabViewer->VertTableScroll(prlModiTab);
		pomStatTabViewer->VertTableScroll(prlModiTab);
	}
	if(wParam == imDebitTableID)
	{
		pomDebitTabViewer->VertTableScroll(prlModiTab);
	}
	return 0L;
	CCS_CATCH_ALL;
	return 0L;
}

//****************************************************************************
// Maximale und minimale Gr��e des Views.
//****************************************************************************

CPoint DutyRoster_View::GetMaxTrackSize()
{
	return (omMaxTrackSize);
}

CPoint DutyRoster_View::GetMinTrackSize()
{
	// Sicherheitsabfrage falls Werte noch nicht initialisiert sind.
	if (omMinTrackSize.x < 0) omMinTrackSize.x = 0;
	if (omMinTrackSize.y < 0) omMinTrackSize.y = 0;
	return (omMinTrackSize);
}

//****************************************************************************
// HMoveTable
//****************************************************************************

LONG DutyRoster_View::HMoveTable(UINT wParam, LONG lParam) 
{
	CCS_TRY;
	MODIFYTAB *prlModiTab = (MODIFYTAB *)lParam;
	
	if(!prlModiTab) return 0;
	
	if(wParam == imIsTableID)
	{
		if(pomIsTabViewer != 0)
			pomIsTabViewer->MoveTable(prlModiTab);
		if(pomStatTabViewer != 0)
			pomStatTabViewer->MoveTable(prlModiTab);
	}
	if(wParam == imDebitTableID)
	{
		if(pomDebitTabViewer)
			pomDebitTabViewer->MoveTable(prlModiTab);
	}
	return 0L;
	CCS_CATCH_ALL;
	return 0L;
}

//****************************************************************************
// VMoveTable
//****************************************************************************

LONG DutyRoster_View::VMoveTable(UINT wParam, LONG lParam) 
{
	CCS_TRY;
	MODIFYTAB *prlModiTab = (MODIFYTAB *)lParam;
	
	if(wParam == imIsTableID)
	{
		pomIsTabViewer->MoveTable(prlModiTab);
		pomDebitTabViewer->MoveTable(prlModiTab);
	}
	if(wParam == imStatTableID)
	{
		pomStatTabViewer->MoveTable(prlModiTab);
	}
	if(wParam == imSolidTableID)
	{
		pomSolidTabViewer->MoveTable(prlModiTab);
	}
	
	return 0L;
	CCS_CATCH_ALL;
	return 0L;
}

//****************************************************************************
// InlineUpdate
//****************************************************************************

LONG DutyRoster_View::InlineUpdate(UINT wParam, LONG lParam) 
{
	CCS_TRY;
	INLINEDATA *prlInlineUpdate = (INLINEDATA *)lParam;
	
	if(wParam == imIsTableID)
	{
		if(ogPrivList.GetStat("DUTYROSTER_CHANGEFIELD") == '1')
			pomIsTabViewer->InlineUpdate(prlInlineUpdate);
	}
	if(wParam == imSolidTableID)
	{
		ChangeGroupMembers(prlInlineUpdate);
	}
	return 0L;
	CCS_CATCH_ALL;
	return 0L;
}

//****************************************************************************
// TableMouseMove
//****************************************************************************

LONG DutyRoster_View::TableMouseMove(UINT wParam, LONG lParam) 
{
	CCS_TRY;
	lParam;
	//INLINEDATA *prlInlineData = (INLINEDATA *)lParam;
	
	if(wParam == imIsTableID || /*wParam == imDebitTableID ||*/ wParam == imSolidTableID || wParam == imStatTableID)
	{
		//if(lParam > -1) //Table or Solid
		{
			if(pomSolidTabViewer != 0)
				pomSolidTabViewer->HighlightRow(lParam, true);
		}
	}
	return 0L;
	CCS_CATCH_ALL;
	return 0L;
}

//**********************************************************************************
// Eine Drag Aktion wird ausgef�hrt
//**********************************************************************************

LONG DutyRoster_View::OnDragOver(UINT wParam, LONG lParam)
{
	CCS_TRY;
	// gleich wie MouseMove
	lParam;
	//INLINEDATA *prlInlineData = (INLINEDATA *)lParam;
	
	if(wParam == imIsTableID || /*wParam == imDebitTableID ||*/ wParam == imSolidTableID || wParam == imStatTableID)
	{
		//if(lParam > -1) //Table or Solid
		{
			if(pomSolidTabViewer != 0)
				pomSolidTabViewer->HighlightRow(lParam, true);
		}
	}
	return -1L;
	CCS_CATCH_ALL;
	return -1L;
}

//****************************************************************************
// TableLButtonDown
//****************************************************************************

LONG DutyRoster_View::TableLButtonDown(UINT wParam, LONG lParam) 
{
	CCS_TRY;
	/*
	INLINEDATA *prlInlineData = (INLINEDATA *)lParam;
	int ilRow	 = prlInlineData->iRow;
	int ilColumn = prlInlineData->iColumn;
	
	  if(wParam == imIsTableID)
	  {
	  if(ilRow > -1 && ilColumn > -1) //Table
	  {
	  }
	  else if(ilRow == -1 && ilColumn > -1) //Header
	  {
	  }
	  else if(ilRow > -1 && ilColumn == -1) //Solid
	  {
	  }
	  }
	*/
	return 0L;
	CCS_CATCH_ALL;
	return 0L;
}

//****************************************************************************
// TableLButtonUp
//****************************************************************************

LONG DutyRoster_View::TableLButtonUp(UINT wParam, LONG lParam) 
{
	CCS_TRY;
	INLINEDATA *prlInlineData = (INLINEDATA *)lParam;
	int ilRow	 = prlInlineData->iRow;
	int ilColumn = prlInlineData->iColumn;
	
	// Herkunft ist die IS Table
	if(wParam == imIsTableID)
	{
		//Table
		if(ilRow > -1 && ilColumn > -1) 
		{
			if(ogPrivList.GetStat("DUTYROSTER_CHANGEFIELD") == '1' && (prlInlineData->iFlag == SELECT || prlInlineData->iFlag == UNSELECT || prlInlineData->iFlag == ONLYSELECT))
			{
				pomIsTabViewer->SelectUpdateField(prlInlineData);
			}
		}
		//Header
		else if(ilRow == -1 && ilColumn > -1) 
		{
			if(rmViewInfo.oShowDateFrom.GetStatus() == COleDateTime::valid && rmViewInfo.oDateFrom.GetStatus() == COleDateTime::valid && rmViewInfo.oDateTo.GetStatus() == COleDateTime::valid)
			{
				COleDateTime olDay = rmViewInfo.oShowDateFrom;
				olDay += COleDateTimeSpan(prlInlineData->iColumnOffset+ilColumn,0,0,0);
				if(olDay >= rmViewInfo.oDateFrom && olDay <= rmViewInfo.oDateTo && rmViewInfo.bSortDayly == true)
				{
					if(olDay != rmViewInfo.oSelectDate)
					{
						// Tageswechsel bei Tagesansicht
						rmViewInfo.oSelectDate = olDay;
						bmChangeDay = true;
						OnSelchange_CB_View();
					}
				}
			}
		}
		//Solid
		else if(ilRow > -1 && ilColumn == -1) 
		{
		}
	}
	return 0L;
	CCS_CATCH_ALL;
	return 0L;
}

//****************************************************************************
// TableRButtonDown
//****************************************************************************

LONG DutyRoster_View::TableRButtonDown(UINT wParam, LONG lParam) 
{
	CCS_TRY;
	INLINEDATA *prlInlineData = (INLINEDATA *)lParam;
	
	int ilRow			= prlInlineData->iRow;
	int ilColumn		= prlInlineData->iColumn;
	int ilRowOffset		= prlInlineData->iRowOffset;
	int ilColumnOffset	= prlInlineData->iColumnOffset;
	CString olOldText	= prlInlineData->oOldText;
	CString olNewText	= prlInlineData->oNewText;
	CPoint olPoint      = CPoint(prlInlineData->oPoint.x, prlInlineData->oPoint.y);
	int ilRButtonDownStatus	= prlInlineData->iRButtonDownStatus;
	
	rmPlanInlineData.iRow			= ilRow;
	rmPlanInlineData.iColumn		= ilColumn;
	rmPlanInlineData.iRowOffset		= ilRowOffset;
	rmPlanInlineData.iColumnOffset	= ilColumnOffset;
	rmPlanInlineData.oOldText		= olOldText;
	rmPlanInlineData.oNewText		= olNewText;
	rmPlanInlineData.oPoint			= olPoint;
	rmPlanInlineData.iRButtonDownStatus = ilRButtonDownStatus;
	
	if(wParam == imIsTableID)
	{
		if(ilRow > -1 && ilColumn > -1) //Table
		{
			int ilStfRow = 0;
			CString olPType;
			int ilPTypeNr = 0;
			bool blTmp = pomIsTabViewer->GetElementAndPType(ilRow+ilRowOffset, ilStfRow, olPType ,ilPTypeNr);
			if(blTmp)
			{
				if(((ilStfRow < omStfData.GetSize() && rmViewInfo.iGroup == 1)||
					(ilStfRow < omGroups.GetSize() && rmViewInfo.iGroup == 2)) && 
					ilRButtonDownStatus != RBDS_NOTHING)
				{
					long llStfUrno = 0;
					CString olStfUrno;
					
					if(rmViewInfo.iGroup == 2)
						llStfUrno = omGroups[ilStfRow].lStfUrno;
					else
						llStfUrno = omStfData[ilStfRow].Urno;
					olStfUrno.Format("%ld",llStfUrno);
					
					COleDateTime olDay = rmViewInfo.oShowDateFrom;
					olDay += COleDateTimeSpan(ilColumn+ilColumnOffset,0,0,0);
					CTime olTime(olDay.GetYear(), olDay.GetMonth(), olDay.GetDay(), 0, 0, 0);
					CString olSday = olDay.Format("%Y%m%d");
					
					if(olDay>=rmViewInfo.oDateFrom && olDay<=rmViewInfo.oDateTo)
					{
						//Make PopUp-Menue
						CMenu menu;
						menu.CreatePopupMenu();
						for (int i = menu.GetMenuItemCount(); i > 0; i--)
							menu.RemoveMenu(i, MF_BYPOSITION);
						
						// ADO
						// Sollte ein "Wunsch" Feld gew�hlt worden sein
						if (olPType == "W")
						{
							// Sollte f�r diesen Mitarbeiter an diesem Tag ein Wunschdatensatz
							// vorhanden sein, Menu aktivieren.
							CString olResult = ogBCD.GetFieldExt("DRW", "SDAY","STFU", olSday,olStfUrno,"URNO");
							
							//if (ogDrwData.GetDrwByKey(olSday,llStfUrno) != NULL)
							if (!olResult.IsEmpty())
							{
								
								CString csCode;
								csCode = ogPrivList.GetStat("DUTYROSTER_CONFLICTCHECK");
								
								if(csCode=='1') 
								{
									// Wunsch bearbeiten
									menu.AppendMenu(MF_STRING, MENUE_WISH, LoadStg(IDS_STRING1821));
									// Wunsch l�schen
									menu.AppendMenu(MF_STRING, MENUE_DELETE_WISH, LoadStg(IDS_STRING1841));
								}
								
							}
							else
							{
								// Wunsch neu anlegen
								menu.AppendMenu(MF_STRING, MENUE_ADD_WISH, LoadStg(IDS_STRING1842));
								
							}
						}
						else if(olPType == "U")
						{
							if(0 != ogDrrData.GetDrrByKey(olSday, llStfUrno, CString("1"), CString("U")))
							{
								
								
								CString csCode;
								csCode = ogPrivList.GetStat("DUTYROSTER_CONFLICTCHECK");
								
								if(csCode=='1') 
								{
									// Abwesenheit l�schen*INTXT*
									menu.AppendMenu(MF_STRING, MENUE_DELETE_ABSENCE, LoadStg(IDS_STRING1846));
									// Tagesdaten anzeigen*INTXT*
									menu.AppendMenu(MF_STRING, MENUE_OPENDRR, LoadStg(IDS_STRING1926));
								}
								
								
								
								
								
								
								
								
								
							}
						}
						else
						{
							// ausgew�hlten DRR ermitteln
							DRRDATA *polDrr = GetActualDrr();
							// Men�punkte nach Pr�fung anzeigen:
							// 'Wunsch anzeigen'
							ShowWishMenu(olSday,olStfUrno,&menu);
							// 'Tagesdaten bearbeiten'
							ShowEditDrrMenu(polDrr,&menu);
							// 'Dienstplan bearbeiten'
							ShowPlanMenu(polDrr,&menu);
							// 'Erzeugen von Doppeltouren'
							ShowCreateDoubleTourMenu(polDrr,&menu);
							// 'Trennen von Doppeltouren'
							ShowCancelDoubleTourMenu(polDrr,&menu);
							// DRR l�schen
							ShowClearDrr(polDrr,&menu);
							// Schichten tauschen
							ShowChangeShift(polDrr,&menu);
							// alle Men�punkte zum Bearbeiten mehrerer Schichten
							ShowMultiDrrMenu(polDrr,&menu);
							// 'Konflikt Pr�fung'
							ShowConflictMenu(polDrr,&menu);
						}
						menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, olPoint.x, olPoint.y, this, NULL);
						//END - Make PopUp-Menue
					}
				}
			}
		}
		else if(ilRow == -1 && ilColumn > -1) //Header
		{
			//Make PopUp-Menue
			CMenu menu;
			menu.CreatePopupMenu();
			for (int i = menu.GetMenuItemCount(); i > 0; i--)
				menu.RemoveMenu(i, MF_BYPOSITION);
			
			UINT nFlags = MF_STRING;
			
			// graue Felder k�nnen keinen Konfliktcheck durchf�hren
			COleDateTime olDay = rmViewInfo.oShowDateFrom;
			olDay += COleDateTimeSpan(ilColumn+ilColumnOffset,0,0,0);
			
			if (GetSelectedView() != "<Default>")
			{
				bool blCheckEnabled = (olDay>=rmViewInfo.oDateFrom && olDay<=rmViewInfo.oDateTo);
				
				if(!rmViewInfo.bDoConflictCheck || !blCheckEnabled)
				{
					// Konfliktcheck ist ausgeschaltet, mu� gegrayt werden
					nFlags |= MF_GRAYED;
				}
				
				CString csCode;
				csCode = ogPrivList.GetStat("DUTYROSTER_CONFLICTCHECK");
				
				if(csCode=='1') 
				{
					menu.AppendMenu(nFlags, MENUE_CONFLICTCHECKALL, LoadStg(IDS_STRING1811));
					menu.AppendMenu(MF_STRING,MENUE_PRINTDAILYROSTER,"Print daily roster");
				}
				
				// Menu Darstellen
				menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, olPoint.x, olPoint.y, this, NULL);
			}
		}
		else if(ilRow > -1 && ilColumn == -1) //Solid
		{
			
		}
	}
	if(wParam == imSolidTableID)
	{
		if(ilRow > -1 && ilColumn >= -1) //Table
		{
			int ilStfRow = 0;
			CString olPType;
			int ilPTypeNr = 0;
			bool blTmp = pomSolidTabViewer->GetElementAndPType(ilRow+ilRowOffset, ilStfRow, olPType, ilPTypeNr);
			if(blTmp)
			{
				if(((ilStfRow < omStfData.GetSize() && rmViewInfo.iGroup == 1)||
					(ilStfRow < omGroups.GetSize() && rmViewInfo.iGroup == 2)) && 
					ilRButtonDownStatus != RBDS_NOTHING)
				{
					long llStfUrno = 0;
					
					if(rmViewInfo.iGroup == 2)
						llStfUrno = omGroups[ilStfRow].lStfUrno;
					else
						llStfUrno = omStfData[ilStfRow].Urno;

					CString csCode;
					csCode = ogPrivList.GetStat("DUTYROSTER_OPENAZK");
				
					
					if(llStfUrno>0 && csCode== "1")
					{
						//Make PopUp-Menue
						CMenu menu;
						menu.CreatePopupMenu();
						for (int i = menu.GetMenuItemCount(); i > 0; i--)
							menu.RemoveMenu(i, MF_BYPOSITION);
						CString olTmp, olYear1, olYear2;
						olYear1 = rmViewInfo.oDateFrom.Format("%Y");
						olYear2 = rmViewInfo.oDateTo.Format("%Y");
						
						olTmp.Format("%s %s",LoadStg(IDS_STRING1705), olYear1);
						menu.AppendMenu(MF_STRING, MENUE_OPENACCOUNT1, olTmp);
						if(ogPrivList.GetStat("DUTYROSTER_OPENAZK") != '1')
							menu.EnableMenuItem(MF_BYCOMMAND|MENUE_OPENACCOUNT1,MF_GRAYED);
						
						
						
						if(olYear1 != olYear2)
						{
							olTmp.Format("%s %s",LoadStg(IDS_STRING1705), olYear2);
							menu.AppendMenu(MF_STRING, MENUE_OPENACCOUNT2, olTmp);
							if(ogPrivList.GetStat("DUTYROSTER_OPENAZK") != '1')
								menu.EnableMenuItem(MF_BYCOMMAND|MENUE_OPENACCOUNT2,MF_GRAYED);
						}
						
						menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, olPoint.x, olPoint.y, this, NULL);
						//END - Make PopUp-Menue
					}
				}
			}
		}
		else if(ilRow == -1 && ilColumn > -1) //Header
		{
			
		}
		else if(ilRow > -1 && ilColumn == -1) //Solid
		{
			
		}
	}
	
	
	
	
	
	return 0L;
	CCS_CATCH_ALL;
	return 0L;
}

//****************************************************************************
// TableLButtonDblClk
//****************************************************************************

LONG DutyRoster_View::TableLButtonDblClk(UINT wParam, LONG lParam) 
{
	CCS_TRY;
	// Info �ber angeklicktes Feld lokal kopieren
	INLINEDATA *prlInlineData = (INLINEDATA *)lParam;
	rmPlanInlineData.iRow			= prlInlineData->iRow;
	rmPlanInlineData.iColumn		= prlInlineData->iColumn;
	rmPlanInlineData.iRowOffset		= prlInlineData->iRowOffset;
	rmPlanInlineData.iColumnOffset	= prlInlineData->iColumnOffset;
	rmPlanInlineData.oOldText		= prlInlineData->oOldText;
	rmPlanInlineData.oNewText		= prlInlineData->oNewText;
	rmPlanInlineData.oPoint			= CPoint(prlInlineData->oPoint.x, prlInlineData->oPoint.y);
	rmPlanInlineData.iRButtonDownStatus = prlInlineData->iRButtonDownStatus;
	
	// welcher Mitarbeiter wurde doppelgeklickkt
	long llStfUrno = -1;
	
	// kommt die Nachricht vom Tabellen-Teil mit Mitarbeitern?
	if(wParam == imSolidTableID)
	{
		// ja -> stimmt die Spalte?
		if(rmPlanInlineData.iRow > -1 && rmPlanInlineData.iColumn == 1)
		{
			// ja -> Urno ermitteln
			if ((llStfUrno = GetStaffUrnoFromSolidTab()) >0)
			{
				// Urno als CString
				CString olUrno;
				olUrno.Format("%ld",llStfUrno);
				// Dialog mit Mitarbeiter-Liste anzeigen
				DoSelectStaffDialog(olUrno);
			}
		}
	}
	else if(wParam == imIsTableID)
	{
		OnMenuOpenDRR();
	}
	return 0L;
	CCS_CATCH_ALL;
	return 0L;
}

//****************************************************************************
// OnMenuAddDrr: Eventhandler zum Hinzuf�gen von Anwesenheiten und 
//	Abwesenheiten �ber das Kontextmen�.
// R�ckgabe:	keine
//****************************************************************************

void DutyRoster_View::OnMenuAddDrr()
{
	CCS_TRY;
	//	TRACE("DutyRoster_View::OnMenuAddDrr()\n");
	// die aktuelle Schicht ermitteln 
	DRRDATA *polDrr = GetActualDrr();
	// g�ltiger DRR?
	if (polDrr == NULL)	return;	// nein -> abbrechen
	
	// Dialog zur Auswahl einer Basisschicht anzeigen
	CSelectShiftDlg olSelectShiftDlg(this, polDrr);
	if (olSelectShiftDlg.DoModal() == IDCANCEL)	return;	// Dialog wurde mit abbrechen beendet
	
	CString olNewFctcDummy = "";	// nur als Platzhalter
	InsertShiftEx(polDrr, olSelectShiftDlg.GetCodeType(), olSelectShiftDlg.GetCodeUrno(), olNewFctcDummy);
	
	// Warnungen ausgeben, wenn vorhanden
	if(ogShiftCheck.IsWarning())
	{
		ForwardWarnings();
	}
	
	CCS_CATCH_ALL;
}

/*****************************************************************************
Erstellt neuen DRR nach Vorgaben, checkt alle Regeln und f�gt den neuen Datensatz ein
CString opNewFctc - wenn nicht leer, dann mu� diese Funktion �bernommen werden 
(kommt aus Coverage/Statistik per DragDrop)
*****************************************************************************/
bool DutyRoster_View::InsertShiftEx(DRRDATA* popDrr, int ipCodeType, long lpCodeUrno, CString opNewFctc)
{
	CCS_TRY;
	// n�chste DRR-Nummer und alle DRR mit Nummer > 1 ermitteln
	CString olNextDrrn;
	CCSPtrArray<DRRDATA> olDrrList;
	olNextDrrn.Format("%d",(ogDrrData.GetDrrListByKeyWithoutDrrn(CString(popDrr->Sday), popDrr->Stfu, CString(popDrr->Rosl),	&olDrrList) +1));
	
	// neuen DRR anlegen
	DRRDATA *polNewDrr = ogDrrData.CreateDrrData(popDrr->Stfu, CString(popDrr->Sday),CString(popDrr->Rosl),CString(popDrr->Ross),olNextDrrn,false/*no InsertInternal()*/);
	
	if (polNewDrr == NULL)
	{
		// Datensatz immer noch null -> Fehler
		return false;
	}
	
	// Schichtcode und davon abh�ngige Info einstellen
	if(!ogDrrData.SetDrrDataScod(polNewDrr,ipCodeType == CODE_IS_BSD,lpCodeUrno,opNewFctc,false,rmViewInfo.bCheckMainFuncOnly))
		return false;
	
	// looking after the function of the second shift
	BSDDATA* polBsd = ogBsdData.GetBsdByUrno(polNewDrr->Bsdu);
	if (polBsd != NULL)
	{
		CString olNewFctc;
		if (strlen(polBsd->Fctc) > 0)
		{
			olNewFctc = polBsd->Fctc;
		}
		else
		{
			// if the basic-shift hasn't got a function, we have to choose one
			CStringArray	olStrArr;
			CUIntArray		olPfcUrnos;
			CString olTmp = this->rmViewInfo.oPfcStatFctcs;
			CedaData::ExtractTextLineFast (olStrArr, olTmp.GetBuffer (0), ",");
			PFCDATA *polPfcData;
			for (int ilSize = 0; ilSize < olStrArr.GetSize(); ilSize++)
			{
				olTmp = olStrArr[ilSize];
				if (olTmp.GetLength () > 2)
				{
					// the codes are like 'LA', so we have to cut the last and the first char
					olTmp = olTmp.Mid (1, olTmp.GetLength () - 2);
					polPfcData = ogPfcData.GetPfcByFctc (olTmp);
					if (polPfcData != NULL)
					{
						olPfcUrnos.Add (polPfcData->Urno);
					}
				}
			}
			
			PfcSelectDlg olDlg(this, &olPfcUrnos);
			if (olPfcUrnos.GetSize() > 1)
			{
				if (olDlg.DoModal() == IDOK)
				{
					PFCDATA* polPfcData = ogPfcData.GetPfcByFctc(olDlg.omSelPfcCode);
					if (polPfcData != 0)
					{
						olNewFctc = polPfcData->Fctc;
					}
				}
				else
				{
					// abgelehnt
					return false;
				}
			}
			else if (olPfcUrnos.GetSize() == 1)
			{
				PFCDATA* polPfcData = ogPfcData.GetPfcByUrno(olPfcUrnos[0]);
				if (polPfcData != 0)
				{
					olNewFctc = polPfcData->Fctc;
				}
			}
		}
		if (olNewFctc.GetLength() && olNewFctc.GetLength() < (FCTC_LEN+2))	//width of DRRTAB.FCTC
		{
			strcpy(polNewDrr->Fctc, olNewFctc.GetBuffer(0));
		}
		//UpdateMultiDrrOfFields(ilCodeType,llNewUrno,olNewFctc,blAddDrr);
	}
	
	bool blShiftInserted = false;	// wenn die Schicht eingef�gt wird, kommt ein true zur�ck, um 2. mal nicht einzuf�gen
	bool blRet = CheckODAandTeilzeitplus(ipCodeType, polNewDrr, olDrrList, &blShiftInserted);
	
	if(blRet && !blShiftInserted)
	{
		blRet = InsertShift(polNewDrr, ipCodeType == CODE_IS_BSD);
	}
	
	return blRet;
	CCS_CATCH_ALL;
	return false;
}

/*****************************************************************************
Checkt die Arbeitsregeln, wenn durchgefallen - fragt, ob als Teilzeit+ eingef�gt 
werden soll, wenn ja, f�gt ein und �ffnet Tagesdialog
*****************************************************************************/
bool DutyRoster_View::InsertShift(DRRDATA* popDrr, bool bpEnableTeilzeitplusQuestion)
{
	// Schicht nach Einhaltung der Arbeitsregeln testen
	bool blShiftEnabled = true;
	
	// ShiftCheck durchf�hren
	blShiftEnabled = ogShiftCheck.DutyRosterCheckIt(popDrr, rmViewInfo.bDoConflictCheck, DRR_NEW, bpEnableTeilzeitplusQuestion);
	
	if(blShiftEnabled)
	{
		// neuen Datensatz speichern und Broadcast absenden (die Aktualisierung
		// der Anzeige wird im Broadcast-Handler von DutyIsTabViewer erledigt)
		ogDrrData.Insert(popDrr);
		ogDrrData.CheckAndSetDraIfDrrBsdDelConnected(popDrr, popDrr->Bsdu);
	}
	else
	{
		if(bpEnableTeilzeitplusQuestion)
		{
			if(ogShiftCheck.IsTeilzeitplusConfirmed())
			{
				// Diese Schicht ist durchgefallen und wurde nach Teilzeit+ gefragt, Antwort positiv
				// Teilzeit+ setzen, checken und Tagesdialog aufrufen
				if(!SetAndUpdateTeilzeitPlusShift(popDrr, true))
				{
					return false;
				}
				else
				{
					return true;
				}
				
			}
			else
			{
				return false;
			}
		}
	}
	return blShiftEnabled;
}

/*****************************************************************************
1. wenn neu einzuf�gende Schicht ODA: alle anderen Schichten des Tages sollen TZ+ sein
2. wenn neu einzuf�gende Schicht BSD: 
- wenn eine der Tagesschichten ODA ist, soll diese als TZ+ eingef�gt sein
bool* pbpShiftInserted - return true, wenn die neue Schicht schon eingef�gt ist
*****************************************************************************/
bool DutyRoster_View::CheckODAandTeilzeitplus(int ipCodeType, DRRDATA* popNewDrr, CCSPtrArray<DRRDATA> opDrrList, bool* pbpShiftInserted)
{
	*pbpShiftInserted = false;
	DRRDATA* polTmpDrr = 0;
	if(ipCodeType == CODE_IS_ODA)
	{
		// alle anderen Schichten des Tages sollen Teilzeit+ sein, pr�fen, setzen
		for(int ilCount=0; ilCount<opDrrList.GetSize(); ilCount++)
		{
			polTmpDrr = &opDrrList.GetAt(ilCount);
			if(!polTmpDrr || polTmpDrr->Avfr.GetStatus() != COleDateTime::valid) continue;	// Avfr zeigt uns, dass es ein gel�schter Satz ist
			if(!ogDrrData.IsExtraShift(CString(polTmpDrr->Drs4)))
			{
				// diese Schicht ist keine Extraschicht! mu� zu TZ+ oder �berzeit ge�ndert werden
				CString olMsg;
				if("ZRH" == ogCCSParam.GetParamValue(ogAppl,"ID_PROD_CUSTOMER"))
				{
					// Soll die vorhandene Schicht %s am Tag %s zu Teilzeit Plus ge�ndert werden?*INTXT*
					olMsg.Format(LoadStg(IDS_STRING1653), polTmpDrr->Scod, polTmpDrr->Avfr.Format("%d.%m.%Y"));
				}
				else
				{
					// Soll die vorhandene Schicht %s am Tag %s zu �berzeit ge�ndert werden?*INTXT*
					olMsg.Format(LoadStg(IDS_STRING9162), polTmpDrr->Scod, polTmpDrr->Avfr.Format("%d.%m.%Y"));
				}
				
				int ilRet = AfxMessageBox((LPCTSTR)olMsg, MB_YESNO | MB_ICONQUESTION);
				if(ilRet == IDYES)
				{
					// Diese Schicht wurde nach Teilzeit+ gefragt, Antwort positiv
					// Teilzeit+ setzen, checken und Tagesdialog aufrufen
					// polTmpDrr ist in lokalem Speicher
					polTmpDrr = ogDrrData.GetDrrByUrno(polTmpDrr->Urno);
					if(!polTmpDrr) return false;
					if(!SetAndUpdateTeilzeitPlusShift(polTmpDrr, false))
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
		}
	}
	else if(ipCodeType == CODE_IS_BSD)
	{
		int ilThisCodeType = 0;
		// alle anderen Schichten des Tages sollen Teilzeit+ sein, pr�fen, setzen
		for(int ilCount=0; ilCount<opDrrList.GetSize(); ilCount++)
		{
			polTmpDrr = &opDrrList.GetAt(ilCount);
			if(	!polTmpDrr || 
				polTmpDrr->Avfr.GetStatus() != COleDateTime::valid) continue;	// Avfr zeigt uns, dass es ein gel�schter Satz ist
			
			ilThisCodeType = ogOdaData.IsODAAndIsRegularFree(CString(polTmpDrr->Scod));
			if(ilThisCodeType & (CODE_IS_ODA_REGULARFREE | CODE_IS_ODA_FREE | CODE_IS_ODA_TIMEBASED | CODE_IS_ODA_NOT_TIMEBASED))
			{
				// ein von ODA-s
				// die �berschneidung der Zeit pr�fen
				COleDateTime olTmpDrrTimeTo = polTmpDrr->Avto;
				if(	polTmpDrr->Avto.GetStatus() == COleDateTime::valid && 
					polTmpDrr->Avfr == polTmpDrr->Avto && 
					polTmpDrr->Avfr.GetHour() == 0 && 
					polTmpDrr->Avfr.GetMinute() == 0)
				{
					olTmpDrrTimeTo += COleDateTimeSpan(1,0,0,0);
				}
				
				if(	polTmpDrr->Avto.GetStatus() == COleDateTime::valid &&
					popNewDrr->Avfr.GetStatus() == COleDateTime::valid &&
					popNewDrr->Avto.GetStatus() == COleDateTime::valid &&
					(polTmpDrr->Avfr > popNewDrr->Avfr &&	// tmp-fr zwischen new-fr und new-to
					polTmpDrr->Avfr < popNewDrr->Avto) ||
					(olTmpDrrTimeTo > popNewDrr->Avfr &&	// tmp-to zwischen new-fr und new-to
					olTmpDrrTimeTo < popNewDrr->Avto) ||
					(popNewDrr->Avfr > polTmpDrr->Avfr &&	// new-fr zwischen tmp-fr und tmp-to
					popNewDrr->Avfr < olTmpDrrTimeTo) ||
					(popNewDrr->Avto > polTmpDrr->Avfr &&	// new-to zwischen tmp-fr und tmp-to
					popNewDrr->Avto < olTmpDrrTimeTo))
				{
					// die neue Schicht ist keine Extraschicht! mu� zu TZ+ ge�ndert werden
					// Frage:
					// Soll die neue Schicht %s am Tag %s als Teilzeit Plus eingef�gt werden?*INTXT*
					CString olMsg;
					olMsg.Format(LoadStg(IDS_STRING1625), popNewDrr->Scod, popNewDrr->Avfr.Format("%d.%m.%Y"));
					int ilRet = AfxMessageBox((LPCTSTR)olMsg, MB_YESNO | MB_ICONQUESTION);
					if(ilRet == IDYES)
					{
						// Diese Schicht wurde nach Teilzeit+ gefragt, Antwort positiv
						// Teilzeit+ setzen, checken und Tagesdialog aufrufen
						
						// polTmpDrr ist in lokalem Speicher
						polTmpDrr = ogDrrData.GetDrrByUrno(polTmpDrr->Urno);
						if(!polTmpDrr) return false;
						
						if(!SetAndUpdateTeilzeitPlusShift(popNewDrr, true))
						{
							return false;
						}
						else
						{
							*pbpShiftInserted = true;
							return true;		// fertig
						}
					}
					else
					{
						return false;
					}
				}
			}
		}		
	}
	
	return true;
}

/***********************************************************************************
setzt popDrr zu TZ+ und ruft Tagesdialog auf
bpInsert = true - insert, false - update
***********************************************************************************/
bool DutyRoster_View::SetAndUpdateTeilzeitPlusShift(DRRDATA* popDrr, bool bpInsert)
{
	if(!ogBsdData.GetBsdByUrno(popDrr->Bsdu)) 
		return false;
	DRRDATA rlDrrUndo;
	ogDrrData.CopyDrr(&rlDrrUndo, popDrr);
	
	// in Genf ist nur �berzeit erlaubt !
	if("ZRH" == ogCCSParam.GetParamValue(ogAppl,"ID_PROD_CUSTOMER"))
	{
		strcpy(popDrr->Drs4, "8");
	}
	else
	{
		strcpy(popDrr->Drs4, "1");
	}
	
	bool blShiftEnabled = ogShiftCheck.DutyRosterCheckIt(popDrr, rmViewInfo.bDoConflictCheck, bpInsert ? DRR_NEW : DRR_CHANGE, false);
	if(blShiftEnabled)
	{
		if(bpInsert)
		{
			ogDrrData.Insert(popDrr);
		}
		else
		{
			ogDrrData.Update(popDrr);
		}
		
		ogDrrData.CheckAndSetDraIfDrrBsdDelConnected(popDrr, popDrr->Bsdu);
		
		if(CallTageslisteDlg(popDrr) != IDOK)
		{
			if(bpInsert)
			{
				// Abbrechen gedruckt oder ein Fehler passierte
				if(strcmp(popDrr->Drrn, "1"))
				{
					// nicht die erste Schicht des Tages
					ogDrrData.Delete(popDrr, true);
				}
				
				else
				{
					ogDrrData.ClearDrr(popDrr, true, true);
				}
			}
			else
			{
				// Abbrechen gedruckt oder ein Fehler passierte
				ogDrrData.ClearDrr(popDrr, true, true);
				//ogDrrData.Update(&rlDrrUndo);
				//ogDrrData.CheckAndSetDraIfDrrBsdDelConnected(popDrr, popDrr->Bsdu);
			}
			return false;
		}
	}
	return true;
}

/*****************************************************************************
Tagesdialog aufrufen
R�ckgabe: ein von CDialog-R�ckgaben - IDOK, IDCANCEL, IDABORT (Fehler), -1 wenn kein Dialog kreiert werden konnte
*****************************************************************************/
int DutyRoster_View::CallTageslisteDlg(DRRDATA* popDrr)
{
	// Die Schichtplanstufe darf nie ver�ndert werden
	bool blReadOnly = false;
	
	if (CString(popDrr->Rosl) == "1")
		blReadOnly = true;
	
	int ilRet;
	
	CTageslisteDlg olTageslistepDlg(blReadOnly,popDrr,&omStfData, this);
	ilRet = olTageslistepDlg.DoModal();	
	
	return ilRet;
}

//****************************************************************************
// OnMenuEditDrr: Eventhandler zum �ndern von Anwesenheiten und 
//	Abwesenheiten �ber das Kontextmen�. �ber den Parameter <ipID>, der die
//	ID des Men�eintrages ist, wird die zu bearbeitenden DRR-Nummer
//	DRRN ermittelt.
// R�ckgabe:	keine
//****************************************************************************

void DutyRoster_View::OnMenuEditDrr(UINT ipID)
{
	CCS_TRY;
	// DRR-Nummer ermitteln
	int ilDrrn = ipID - MENUE_EDIT_DRR;
	CString olDrrn;
	olDrrn.Format("%d",ilDrrn);
	TRACE("DutyRoster_View::OnMenuEditDrr(): DRR-Nummer ist %d\n",ilDrrn);
	
	// die aktuelle Schicht ermitteln 
	DRRDATA *polActiveDrr = GetActualDrr();
	// g�ltiger DRR?
	if (polActiveDrr == NULL)	return;	// nein -> abbrechen
	// mit Hilfe des aktiven DRR den zu bearbeitenden DRR ermitteln
	DRRDATA *polEditDrr = ogDrrData.GetDrrByKey(CString(polActiveDrr->Sday),polActiveDrr->Stfu,olDrrn,CString(polActiveDrr->Rosl));
	// g�ltiger DRR?
	if (polEditDrr == NULL) return; // nein -> abbrechen
	// Dialog aufrufen
	CallTageslisteDlg(polEditDrr);
	CCS_CATCH_ALL;
}

//****************************************************************************
// OnMenuDeleteDrr: Eventhandler zum L�schen von Anwesenheiten und 
//	Abwesenheiten �ber das Kontextmen�. �ber den Parameter <ipID>, der die
//	ID des Men�eintrages ist, wird die zu bearbeitenden DRR-Nummer
//	DRRN ermittelt. 
// R�ckgabe:	keine
//****************************************************************************

void DutyRoster_View::OnMenuDeleteDrr(UINT ipID)
{
	CCS_TRY;
	// DRR-Nummer ermitteln
	int ilDrrn = ipID - MENUE_DELETE_DRR;	//  = {1...9} (sieh MAX_DRR_DRRN == 9)
	//	TRACE("DutyRoster_View::OnMenuDeleteDrr(): DRR-Nummer ist %d\n",ilDrrn);
	
	// die aktuelle Schicht ermitteln 
	CString olDrrn;
	olDrrn.Format("%d",ilDrrn);
	DRRDATA *polActiveDrr = GetActualDrr();
	
	// g�ltiger DRR?
	if (polActiveDrr == NULL)	return;	// nein -> abbrechen
	
	// mit Hilfe des aktiven DRR den zu bearbeitenden DRR ermitteln
	DRRDATA *polEditDrr = ogDrrData.GetDrrByKey(CString(polActiveDrr->Sday),polActiveDrr->Stfu,olDrrn,CString(polActiveDrr->Rosl));
	
	// g�ltiger DRR?
	if (polEditDrr == NULL) return; // nein -> abbrechen
	
	// DRR und alle anh�ngigen DRDs, DRAs und DRS l�schen. Datens�tze mit h�heren
	// DRR-Nummern (Drrn) werden angepasst
	ogDrrData.Delete(polEditDrr,true);
	CCS_CATCH_ALL;
}

//****************************************************************************
// OnMenuPlan
//****************************************************************************

void DutyRoster_View::OnMenuPlan() 
{
	CCS_TRY;
	int ilRow			= rmPlanInlineData.iRow;	
	int ilColumn		= rmPlanInlineData.iColumn;
	int ilRowOffset		= rmPlanInlineData.iRowOffset;
	int ilColumnOffset	= rmPlanInlineData.iColumnOffset;
	CString olOldText	= rmPlanInlineData.oOldText;
	CString olNewText	= rmPlanInlineData.oNewText;
	CPoint olPoint      = rmPlanInlineData.oPoint;
	//int ilRButtonDownStatus	= rmPlanInlineData.iRButtonDownStatus;
	
	int ilStfRow = 0;
	CString olPType;
	int ilPTypeNr = 0;
	bool blTmp = pomIsTabViewer->GetElementAndPType(ilRow+ilRowOffset, ilStfRow, olPType, ilPTypeNr);
	if(blTmp)
	{
		if(((ilStfRow < omStfData.GetSize() && rmViewInfo.iGroup == 1)||
			(ilStfRow < omGroups.GetSize() && rmViewInfo.iGroup == 2)) && 
			(olPType == "1" || olPType == "2" || olPType == "3" || olPType == "4" || olPType == "5" || olPType == "L"))
		{
			long llStfUrno = 0;
			
			if(rmViewInfo.iGroup == 2)
				llStfUrno = omGroups[ilStfRow].lStfUrno;
			else
				llStfUrno = omStfData[ilStfRow].Urno;
			
			CString olCode;
			COleDateTime olDay = rmViewInfo.oShowDateFrom;
			olDay += COleDateTimeSpan(ilColumn+ilColumnOffset,0,0,0);
			
			//int ilDay = olDay.GetDay()-1;
			DRRDATA* prlDrr = ogDrrData.GetDrrByRoss(olDay.Format("%Y%m%d"),llStfUrno);		
			if (prlDrr != NULL)
			{
				olCode = CString(prlDrr->Scod);
			}

			DutyPlanPs olDutyPlanPs(LoadStg(IDS_STRING1606), llStfUrno, olCode,olDay, olPType,this,0);
			olDutyPlanPs.m_psh.dwFlags &= ~(PSP_HASHELP);
			olDutyPlanPs.m_psh.dwFlags |= PSH_NOAPPLYNOW;
			olDutyPlanPs.DoModal();
		}
	}
	CCS_CATCH_ALL;
}

//****************************************************************************
// OnMenuConflictCheck
//****************************************************************************

void DutyRoster_View::OnMenuConflictCheck() 
{
	CCS_TRY;
	if(!rmViewInfo.bDoConflictCheck) return;
	
	int ilRow			= rmPlanInlineData.iRow;	
	int ilColumn		= rmPlanInlineData.iColumn;
	int ilRowOffset		= rmPlanInlineData.iRowOffset;
	int ilColumnOffset	= rmPlanInlineData.iColumnOffset;
	CString olOldText	= rmPlanInlineData.oOldText;
	CString olNewText	= rmPlanInlineData.oNewText;
	CPoint olPoint      = rmPlanInlineData.oPoint;
	//int ilRButtonDownStatus	= rmPlanInlineData.iRButtonDownStatus;
	
	int ilStfRow = 0;
	CString olPType;
	int ilPTypeNr = 0;
	bool blTmp = pomIsTabViewer->GetElementAndPType(ilRow+ilRowOffset, ilStfRow, olPType, ilPTypeNr);
	if(blTmp)
	{
		if(((ilStfRow < omStfData.GetSize() && rmViewInfo.iGroup == 1)||
			(ilStfRow < omGroups.GetSize() && rmViewInfo.iGroup == 2)) && 
			(olPType == "1" || olPType == "2" || olPType == "3" || olPType == "4"))
		{
			long llStfUrno = 0;
			
			if(rmViewInfo.iGroup == 2)
				llStfUrno = omGroups[ilStfRow].lStfUrno;
			else
				llStfUrno = omStfData[ilStfRow].Urno;
			
			CString olCode;
			COleDateTime olDay = rmViewInfo.oShowDateFrom;
			olDay += COleDateTimeSpan(ilColumn+ilColumnOffset,0,0,0);
			CString olSday = olDay.Format("%Y%m%d");// volles Datum f�r DRR-Abfrage
			
			// DRR-Datensatz suchen
			DRRDATA *prlDrrData = ogDrrData.GetDrrByKey(olSday,llStfUrno,"1"/*Schichtnummer*/,olPType);
			
			//CONFLICT CHECK
			if(prlDrrData != 0)
			{
				ogShiftCheck.DutyRosterCheckIt(prlDrrData, true, DRR_CHANGE, false);
				if(ogShiftCheck.IsWarning())
				{
					ForwardWarnings();
				}
			}
		}
	}
	CCS_CATCH_ALL;
}

//****************************************************************************
// OnMenuConflictCheckAll: Konfliktpr�fung f�r alle Mitarbeiter.
//****************************************************************************

void DutyRoster_View::OnMenuConflictCheckAll() 
{
	CCS_TRY;
	if(!rmViewInfo.bDoConflictCheck) return;
	
	// dauert in der Regel etwas l�nger...
	CWaitDlg	plWaitDlg(this,LoadStg(WAIT_DATA_CHECK));
	
	// Spalte ermitteln, wird zur Betsimmung des Tages ben�tigt
	int ilColumn		= rmPlanInlineData.iColumn;
	int ilColumnOffset	= rmPlanInlineData.iColumnOffset;
	// ausgew�hlten Tag ermitteln
	COleDateTime olDay = rmViewInfo.oShowDateFrom;
	olDay += COleDateTimeSpan(ilColumn+ilColumnOffset,0,0,0);
	//CString olSday = olDay.Format("%Y%m%d");// volles Datum f�r DRR-Abfrage
	//int ilPTypeNr = 0;	// ...als Int
	
	// Mitarbeiter- oder Gruppen- oder Fahrgemeinschafts-Urno
	//long llStfUrno = 0;
	
	int ilOutputOld = ogShiftCheck.imOutput;
	ogShiftCheck.imOutput = OUT_WARNING_AS_WARNING | OUT_ERROR_AS_WARNING;	// Fehler sollen nur als Warnungen ausgegeben werden
	
	ogShiftCheck.DutyRosterCheckIt(olDay);
	
	ogShiftCheck.imOutput = ilOutputOld;
	
	// Nach dem ogShiftCheck.DutyRosterCheckIt() m�ssen wir pr�fen, ob Warnungen vorliegen, wenn ja, ausgeben
	if(ogShiftCheck.IsWarning())
		ForwardWarnings();
	
	// Warte Dialog beenden.
	plWaitDlg.DestroyWindow();
	
	CCS_CATCH_ALL;
}

//****************************************************************************
// OnMenuOpenDSR()
//****************************************************************************

void DutyRoster_View::OnMenuOpenDRR() 
{
	CCS_TRY;
	
	int ilRow			= rmPlanInlineData.iRow;	
	int ilColumn		= rmPlanInlineData.iColumn;
	int ilRowOffset		= rmPlanInlineData.iRowOffset;
	int ilColumnOffset	= rmPlanInlineData.iColumnOffset;
	CString olOldText	= rmPlanInlineData.oOldText;
	CString olNewText	= rmPlanInlineData.oNewText;
	CPoint olPoint      = rmPlanInlineData.oPoint;
	
	bool blReadOnly = false;
	
	int ilStfRow = 0;
	CString olPType;
	int ilPTypeNr = 0;
	
	bool blTmp = pomIsTabViewer->GetElementAndPType(ilRow+ilRowOffset, ilStfRow, olPType, ilPTypeNr);
	if(!blTmp)	return;
	
	if(((ilStfRow < omStfData.GetSize() && rmViewInfo.iGroup == 1)||
		(ilStfRow < omGroups.GetSize() && rmViewInfo.iGroup == 2)) && 
		(olPType == "1" || olPType == "2" || olPType == "3" || olPType == "4" || olPType == "L" || olPType == "5" || olPType == "U"))
	{
		//---------------------------------
		// Mitarbeiter URNO erhalten
		long llStfUrno = 0;
		if(rmViewInfo.iGroup == 2)
		{
			llStfUrno = omGroups[ilStfRow].lStfUrno;
		}
		else
		{
			llStfUrno = omStfData[ilStfRow].Urno;
		}
		//---------------------------------
		// Tag berechnen
		CString olCode;
		COleDateTime olDay = rmViewInfo.oShowDateFrom;
		olDay += COleDateTimeSpan(ilColumn+ilColumnOffset,0,0,0);
		CTime olTime(olDay.GetYear(), olDay.GetMonth(), olDay.GetDay(), 0, 0, 0);
		CString olSday = olDay.Format("%Y%m%d");
		//---------------------------------
		//  DRR Datensatz laden.
		DRRDATA *prlDrr = ogDrrData.GetDrrByKey(olSday,llStfUrno,"1",olPType);
		
		// Testen ob g�ltiger DRR Datensatz gefunden wurde.
		if(prlDrr != NULL)
		{
			// Pr�fen, ob es sich um den aktiven oder "leeren" DRR handelt
			if (strcmp(prlDrr->Ross,"A") ==  0)
			{
				if (CString(prlDrr->Rosl) == "1")
				{
					blReadOnly = true;
				}
				else
				{
					blReadOnly = false;
				}
			}
			else if (CString(prlDrr->Rosl) == "U" && prlDrr->Bsdu != 0)
			{
				blReadOnly = false;
			}
			else
			{
				blReadOnly = true;
			}
			
			// Dialog aufrufen
			int ilRet;
			CTageslisteDlg olTageslistepDlg (blReadOnly,prlDrr,&omStfData, this);
			ilRet = olTageslistepDlg.DoModal();	
		}
		else
		{
			TRACE ("DRR nicht gefunden.\n");
		}
	}
	CCS_CATCH_ALL;
}

//****************************************************************************
// OnMenuOpenAccount1
//****************************************************************************

void DutyRoster_View::OnMenuOpenAccount1() 
{
	CCS_TRY;
	OpenAccount(1);
	CCS_CATCH_ALL;
}

//****************************************************************************
// OnMenuOpenAccount2
//****************************************************************************

void DutyRoster_View::OnMenuOpenAccount2() 
{
	CCS_TRY;
	OpenAccount(2);
	CCS_CATCH_ALL;
}

//****************************************************************************
// l�scht W�nsche
//****************************************************************************

void DutyRoster_View::OnMenuDeleteWish() 
{
	CCS_TRY;
	
	int ilRow			= rmPlanInlineData.iRow;	
	int ilColumn		= rmPlanInlineData.iColumn;
	int ilRowOffset		= rmPlanInlineData.iRowOffset;
	int ilColumnOffset	= rmPlanInlineData.iColumnOffset;
	int ilStfRow		= 0;
	int ilPTypeNr		= 0;
	CString olPType;
	CString olStfUrno,olUrno;
	
	bool blTmp = pomIsTabViewer->GetElementAndPType(ilRow+ilRowOffset, ilStfRow, olPType, ilPTypeNr);
	
	if(blTmp)
	{
		long llStfUrno = 0;
		
		// Mitarbeiter URNO erhalten
		if(rmViewInfo.iGroup == 2)
			llStfUrno = omGroups[ilStfRow].lStfUrno;
		else
			llStfUrno = omStfData[ilStfRow].Urno;
		olStfUrno.Format("%ld",llStfUrno);
		
		// Tag berechnen
		CString olCode;
		COleDateTime olDay = rmViewInfo.oShowDateFrom;
		olDay += COleDateTimeSpan(ilColumn+ilColumnOffset,0,0,0);
		
		CTime olTime(olDay.GetYear(), olDay.GetMonth(), olDay.GetDay(), 0, 0, 0);
		CString olSday = olDay.Format("%Y%m%d");
		
		
		// Exitiert ein Datensatz mit diesen Daten ?
		olUrno = ogBCD.GetFieldExt("DRW", "SDAY","STFU", olSday,olStfUrno,"URNO");
		if (!olUrno.IsEmpty())
		{
			// Datens�tze l�schen
			//----------------------------------------------------------------------
			// Unterschiedliche Behandlung wenn mehrere Felder selektiert wurden
			//----------------------------------------------------------------------
			if (pomIsTabViewer->IsMultiSelection())
				// Alle Markierten W�nsche l�schen.
				pomIsTabViewer->DeleteDRWData();
			else
			{
				// Ein einzigen Wunsch l�schen
				ogBCD.DeleteRecord("DRW","URNO",olUrno, true);
			}
		}
		pomIsTabViewer->UnselectFields();
	}
	CCS_CATCH_ALL;
}

//****************************************************************************
// �ffnet einen Dialog zum ver�ndern der W�nsche
//****************************************************************************

void DutyRoster_View::OnMenuWish() 
{
	CCS_TRY;
	
	int ilRow			= rmPlanInlineData.iRow;	
	int ilColumn		= rmPlanInlineData.iColumn;
	int ilRowOffset		= rmPlanInlineData.iRowOffset;
	int ilColumnOffset	= rmPlanInlineData.iColumnOffset;
	int ilStfRow		= 0;
	int ilPTypeNr		= 0;
	CString olPType,olUrno,olStfUrno;
	
	bool blTmp = pomIsTabViewer->GetElementAndPType(ilRow+ilRowOffset, ilStfRow, olPType, ilPTypeNr);
	if(blTmp)
	{
		long llStfUrno = 0;
		
		// Mitarbeiter URNO erhalten
		if(rmViewInfo.iGroup == 2)
			llStfUrno = omGroups[ilStfRow].lStfUrno;
		else
			llStfUrno = omStfData[ilStfRow].Urno;
		olStfUrno.Format("%ld",llStfUrno);
		
		// Tag berechnen
		CString olCode;
		COleDateTime olDay = rmViewInfo.oShowDateFrom;
		olDay += COleDateTimeSpan(ilColumn+ilColumnOffset,0,0,0);
		
		CTime olTime(olDay.GetYear(), olDay.GetMonth(), olDay.GetDay(), 0, 0, 0);
		CString olSday = olDay.Format("%Y%m%d");
		
		// Exitiert ein Datensatz mit diesen Daten ?
		// Bemerkung: Im Prinzip ist else hier unn�tig. Wird noch bearbeitet
		olUrno = ogBCD.GetFieldExt("DRW", "SDAY","STFU", olSday,olStfUrno,"URNO");
		if (!olUrno.IsEmpty())
		{
			// "Wunschdialog" ausf�hren
			CWishDialog olWishDialog(this,olUrno,llStfUrno,olSday);
			olWishDialog.DoModal(); 
		}
		else
		{
			// "Wunschdialog" ausf�hren
			CWishDialog olWishDialog(this,"",llStfUrno,olSday);
			olWishDialog.DoModal(); 
		}
	}
	CCS_CATCH_ALL;
}

//****************************************************************************
// �ffnet einen Dialog zum Trennen einer Doppeltour
//****************************************************************************

void DutyRoster_View::OnMenuCancelDoubleTour()
{
	// Soll die selektierte Doppeltour getrennt werden?*INTXT*
	
	DRRDATA* polDrr;
	
	// Aktuellen DRR suchen
	polDrr = GetActualDrr();
	
	// G�ltigkeitstest
	if (polDrr == NULL) return;
	
	// Doppeltour trennen
	ogDrrData.CancelDoubleTour(polDrr);
}

//****************************************************************************
// �ffnet einen Dialog zum Erzeugen einer Doppeltour
//****************************************************************************

void DutyRoster_View::OnMenuCreateDoubleTour()
{
	DRRDATA* polDrr;
	DRSDATA* polDrs;
	
	// Aktuellen DRR suchen
	polDrr = GetActualDrr();
	
	// G�ltigkeitstest
	if (polDrr == NULL) return;
	
	// DRS suchen, der �ber die DRR-Urno <lpDrru> mit dem entsprechenden DRR verkn�pft ist
	polDrs = ogDrsData.GetDrsByDrru(polDrr->Urno);
	
	// G�ltigkeitstest, falls Drs nicht vorhanden, neuen Drs erstellen
	if (polDrs == NULL)
	{
		polDrs = ogDrsData.CreateDrsData(polDrr->Stfu, polDrr->Sday, polDrr->Urno);
		if (polDrs == NULL) return;
	}
	
	CDoubleTourDlg	plDlg(this,polDrr,polDrs,&omStfData,this);
	if (plDlg.DoModal() == IDOK)
	{
	/*
	// DRS abspeichern
	if (blIsNewDrs)
	{
	blResult = ogDrsData.Insert(polDrs);
	}
	else
	{
	blResult = ogDrsData.Update(polDrs);
	}
	
	  if (!blResult)
	  {
	  // Fehler ausgeben
	  CString olErrorTxt;
	  olErrorTxt.Format("%s %d\n%s",LoadStg(ST_WRITEERR), ogDrsData.imLastReturnCode, ogDrsData.omLastErrorMessage);
	  MessageBox(olErrorTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
	}*/
	}
}

//****************************************************************************
// �ffnet einen Dialog zum anzeigen der W�nsche
//****************************************************************************

void DutyRoster_View::OnMenuShowWish() 
{
	CCS_TRY;
	
	int ilRow			= rmPlanInlineData.iRow;	
	int ilColumn		= rmPlanInlineData.iColumn;
	int ilRowOffset		= rmPlanInlineData.iRowOffset;
	int ilColumnOffset	= rmPlanInlineData.iColumnOffset;
	int ilStfRow		= 0;
	int ilPTypeNr		= 0;
	CString olPType,olUrno,olStfUrno;
	
	bool blTmp = pomIsTabViewer->GetElementAndPType(ilRow+ilRowOffset, ilStfRow, olPType, ilPTypeNr);
	if(blTmp)
	{
		long llStfUrno = 0;
		
		// Mitarbeiter URNO erhalten
		if(rmViewInfo.iGroup == 2)
			llStfUrno = omGroups[ilStfRow].lStfUrno;
		else
			llStfUrno = omStfData[ilStfRow].Urno;
		olStfUrno.Format("%ld",llStfUrno);
		
		// Tag berechnen
		CString olCode;
		COleDateTime olDay = rmViewInfo.oShowDateFrom;
		olDay += COleDateTimeSpan(ilColumn+ilColumnOffset,0,0,0);
		
		CTime olTime(olDay.GetYear(), olDay.GetMonth(), olDay.GetDay(), 0, 0, 0);
		CString olSday = olDay.Format("%Y%m%d");
		
		// Exitiert ein Datensatz mit diesen Daten ?
		olUrno = ogBCD.GetFieldExt("DRW", "SDAY","STFU", olSday,olStfUrno,"URNO");
		if (!olUrno.IsEmpty())
		{
			// "Wunschdialog" ausf�hren
			CWishDialog olWishDialog(this,olUrno,llStfUrno,olSday,true);
			olWishDialog.DoModal(); 
		}
	}
	CCS_CATCH_ALL;
}

//****************************************************************************
// l�scht Abwesenheiten (DRRs) in allen selektierten Felder
//****************************************************************************

void DutyRoster_View::OnMenuDeleteAbsence() 
{
	CCS_TRY;
	// wir scannen alle selektierten Felder nacheinander
	SELECTEDFIELD* polSelField = pomIsTabViewer->GetFirstSelectedField();
	if(!polSelField)
	{
		// wenn kein Feld selektiert ist (einzeln l�schen)
		SELECTEDFIELD olSelField;
		olSelField.iRow		= rmPlanInlineData.iRow;
		olSelField.iColumn	= rmPlanInlineData.iColumn;
		olSelField.iRowOffset	= rmPlanInlineData.iRowOffset;
		olSelField.iColumnOffset	= rmPlanInlineData.iColumnOffset;
		olSelField.oOldText = "";
		olSelField.iOldBkColor = BPN_WHITE;
		DeleteDrrOfFieldIfURosl(&olSelField);
	}
	else
	{
		for(; polSelField != 0; polSelField = pomIsTabViewer->GetNextSelectedField())
		{
			DeleteDrrOfFieldIfURosl(polSelField);
		}
		pomIsTabViewer->UnselectFields();
	}
	
	CCS_CATCH_ALL;
}

/*************************************************************************************
Reine Arbeitsfunktion f�r ein paar OnMenuDeleteAbsence()-Aufrufe
*************************************************************************************/
void DutyRoster_View::DeleteDrrOfFieldIfURosl(SELECTEDFIELD* popSelField)
{
	DRRDATA* polDrr;
	polDrr = GetFirstDrrOfSelectedField(popSelField);
	if(polDrr != 0 && !strcmp(polDrr->Rosl, "U"))
	{
		ogDrrData.Delete(polDrr, true);
	}
}

/**************************************************************************************
wenn mindestens ein DRR vorhanden ist, DRR mit der Drrn==1 wird zur�ckgegeben, sonst 0
**************************************************************************************/
DRRDATA* DutyRoster_View::GetFirstDrrOfSelectedField(SELECTEDFIELD* popSelField)
{
	if(!popSelField) return 0;
	
	int ilRow			= popSelField->iRow;	
	int ilColumn		= popSelField->iColumn;
	int ilRowOffset		= popSelField->iRowOffset;
	int ilColumnOffset	= popSelField->iColumnOffset;
	int ilStfRow		= 0;
	int ilPTypeNr		= 0;
	CString olPType;
	//	CString olStfUrno,olUrno;
	
	if(pomIsTabViewer->GetElementAndPType(ilRow+ilRowOffset, ilStfRow, olPType, ilPTypeNr))
	{
		long llStfUrno = 0;
		
		// Mitarbeiter URNO erhalten
		if(rmViewInfo.iGroup == 2)
			llStfUrno = omGroups[ilStfRow].lStfUrno;
		else
			llStfUrno = omStfData[ilStfRow].Urno;
		//		olStfUrno.Format("%ld",llStfUrno);
		
		// Tag berechnen
		//		CString olCode;
		COleDateTime olDay = rmViewInfo.oShowDateFrom;
		olDay += COleDateTimeSpan(ilColumn+ilColumnOffset,0,0,0);
		
		CString olSday = olDay.Format("%Y%m%d");
		
		DRRDATA* polDrr = ogDrrData.GetDrrByKey(olSday, llStfUrno, "1", olPType);
		return polDrr;
	}
	return 0;
}

//****************************************************************************
// OpenAccount: ruft den Dialog f�r die Anzeige des Arbeitszeitkontos
//	auf.
// R�ckgabe:	keine
//****************************************************************************

void DutyRoster_View::OpenAccount(int ipNr) 
{
	CCS_TRY;
	// der Mitarbeiter
	long llStfUrno;
	
	// welches Jahr soll untersucht werden
	CString  olYear;
	if(ipNr == 2)
		olYear = rmViewInfo.oDateTo.Format("%Y");
	else
		olYear = rmViewInfo.oDateFrom.Format("%Y");
	
	if((llStfUrno = GetStaffUrnoFromSolidTab()) > 0)
	{
		DutyAccountDlg olDutyAccountDlg(llStfUrno, olYear, this);
		olDutyAccountDlg.DoModal();
		// ADO, Konten neu Anzeigen
		pomStatTabViewer->ChangeView(false,false);
	}
	CCS_CATCH_ALL;
}

//****************************************************************************
// GetStaffUrnoFromSolidTab: ermittelt die Urno des Mitarbeiters,
//	der in der Tabelle rechts (SolidTabViewer) angeklickt wurde.
// R�ckgabe:	long Mitarbeiter-Urno oder -1 bei Fehler.
//****************************************************************************

long DutyRoster_View::GetStaffUrnoFromSolidTab() 
{
	CCS_TRY;
	// Spalte und Zeile der angeklickten Zelle
	int ilRow			= rmPlanInlineData.iRow;	
	//int ilColumn		= rmPlanInlineData.iColumn;
	int ilRowOffset		= rmPlanInlineData.iRowOffset;
	
	// auf die Anzahl der Mitarbeiter bezogene Zeilennummer
	// (bei Ansicht mehrerer Planungsstufen ungleich <ilRow>)
	int ilStfRow = 0;
	// Planungsstufe (???)
	CString olPType;
	int ilPTypeNr = 0;
	
	// Zellenelement ermitteln
	if(pomSolidTabViewer->GetElementAndPType(ilRow+ilRowOffset, ilStfRow, olPType, ilPTypeNr))
	{
		// Zeilennummer und Ansichtstyp OK?
		if((ilStfRow < omStfData.GetSize() && rmViewInfo.iGroup == 1)||
			(ilStfRow < omGroups.GetSize() && rmViewInfo.iGroup == 2))
		{
			// Mitarbeiter-Urno aus dem entsprechenden Array ermitteln
			if(rmViewInfo.iGroup == 2)
				return omGroups[ilStfRow].lStfUrno;
			else
				return omStfData[ilStfRow].Urno;
		}
	}
	return -1;
	CCS_CATCH_ALL;
	return -1;
}


//****************************************************************************
// OnBActual: Eventhandler f�r den Button 'Ansicht aktualisieren.
// Neu Sortierung der Gruppen
//	R�ckgabe:	keine
//****************************************************************************

void DutyRoster_View::OnBActual() 
{
	CCS_TRY;
	// Warte Dialog
	CWaitDlg	plWaitDlg(this,LoadStg(WAIT_DATA_PREPARE));
	
	FillGroupStruct();
	pomSolidTabViewer->ChangeView(false,false);
	pomIsTabViewer->ChangeView(false,false);
	pomStatTabViewer->ChangeView(false,false);
	if(pomDutyDragDropDlg != NULL)
	{
		pomDutyDragDropDlg->ChangeView(false,false);
	}
	
	// Button wird nicht mehr gebraucht
	bmNeedActualButton = false;
	
	CCS_CATCH_ALL;
}

//****************************************************************************
// OnBAttention
//****************************************************************************

void DutyRoster_View::OnBAttention() 
{
	CCS_TRY;
	
	// Durch den �bergeordneten Frame eine neue Toolbar laden
	// ohne attention mark
	((DutyRoster_Frm*)GetParentFrame())->LoadToolBar(FALSE);
	
	// Info-Dialog aktivieren oder erzeugen
	if (pomAttentionDlg == NULL)
	{		
		// Dialog erzeugen
		pomAttentionDlg = new DutyAttentionDlg(this);
		if (!pomAttentionDlg->Create())
		{
			// Fehler -> aufr�umen
			delete pomAttentionDlg;
			pomAttentionDlg = NULL;
		}
	}
	else
	{
		// Dialog anzeigen
		pomAttentionDlg->ShowWindow(SW_SHOWNORMAL);
		pomAttentionDlg->SetActiveWindow();
	}
	
	//	DutyAttentionDlg olDutyAttentionDlg(this);
	//	olDutyAttentionDlg.DoModal();
	CCS_CATCH_ALL;
}

//****************************************************************************
// InsertAttention
//****************************************************************************

void DutyRoster_View::InsertAttention(CString opText,COLORREF opColor /*=BLACK*/)
{
	CCS_TRY;
	
	// Neue Toolbar laden, mit attention mark
	DutyRoster_Frm* prlFrame = (DutyRoster_Frm*)GetParentFrame();
	_ASSERTE(prlFrame!=0);
	prlFrame->LoadToolBar(true);
	ATTENTIONINFO *polAInfo = new ATTENTIONINFO;
	polAInfo->Color = opColor;
	polAInfo->Text  = opText;
	omAttentionInfo.Add(polAInfo);
	// Eintrag in Info-Dialog, wenn aktiviert
	if (pomAttentionDlg != NULL)
	{
		// Eintrag hinzuf�gen und Dialog anzeigen
		pomAttentionDlg->AddAttention(opText,opColor);
	}
	CCS_CATCH_ALL;
}

//****************************************************************************
// OnBPrint
//****************************************************************************

void DutyRoster_View::OnBPrint() 
{
	CCS_TRY;
	DutyPrintDlg olDutyPrintDlg(this);
	olDutyPrintDlg.DoModal();
	CCS_CATCH_ALL;
}

//****************************************************************************
// OnBRelease
//****************************************************************************

void DutyRoster_View::OnBRelease() 
{
	CCS_TRY;
	if(GetEnableRelease())
	{
		DutyReleaseDlg olDutyReleaseDlg(this);
		olDutyReleaseDlg.DoModal();
	}
	else
	{
		MessageBeep((UINT)-1);
	}
	CCS_CATCH_ALL;
}

//****************************************************************************
// OnBToSAP() 
//****************************************************************************
//uhi 15.9.00
/*void DutyRoster_View::OnBToSAP() 
{
CCS_TRY;
CSendToSapDlg olSapDlg(this);
olSapDlg.DoModal();
CCS_CATCH_ALL;
}*/

/****************************************************************************
OnBView
****************************************************************************/

void DutyRoster_View::OnBView() 
{
	CCS_TRY;
	
	DutyRosterPropertySheet olDlg(LoadStg(DUTY_SHEET_BASE_CAPTION),this,pomViewer, pomAccounts);
	
	int iRet = olDlg.DoModal();
	
	if(iRet != IDCANCEL)
	{
		UpdateComboBox(true);
		SetRet(iRet);
		OnSelchange_CB_View();	
		SetRet(IDOK);
	}	
	else
	{
		if(olDlg.bmUpdateViewOnCancel)
		{
			// one of views was deleted, wie must update the combobox
			UpdateComboBox(false);
			OnSelchange_CB_View();
		}
	}
	WriteLogFull("end of OnBView()");	
	CCS_CATCH_ALL;
}

//****************************************************************************
// GetEnableRelease: pr�ft, ob der Dienstplan freigegeben werden kann.
// R�ckgabe:	true	->	Freigabe darf erfolgen
//				false	->	Freigabe darf nicht erfolgen
//****************************************************************************
bool DutyRoster_View::GetEnableRelease()
{
	CCS_TRY;
	// Start- und Enddatum g�ltig und Funktionen und Planungstyp OK?
	if (rmViewInfo.oDateFrom.GetStatus() == COleDateTime::valid && 
		rmViewInfo.oDateTo.GetStatus() == COleDateTime::valid &&
	   (rmViewInfo.oPfcUrnos.IsEmpty() == FALSE || rmViewInfo.oPfcStatUrnos.IsEmpty() == FALSE) && 
		rmViewInfo.oPType.IsEmpty() == FALSE)
	{
		// Freigabe verf�gbar
		return true;
	}
	CCS_CATCH_ALL;
	// Freigabe nicht verf�gbar
	return false;
}

//****************************************************************************
// 
//****************************************************************************

void DutyRoster_View::ChangeGroupMembers(INLINEDATA *prpInlineUpdate) 
{
	CCS_TRY;
	if(!bmDragDopIsOK)
	{
		MessageBeep((UINT)-1);
		return;
	}
	
	INLINEDATA rlInlineUpdate1 = *prpInlineUpdate; // 1 = Drop ( to Staff ), 2 = Drag ( from Staff )
	INLINEDATA rlInlineUpdate2;
	
	CStringArray olStrArray;
	if(ExtractItemList(rlInlineUpdate1.oNewText, &olStrArray, '|') > 8)
	{
		rlInlineUpdate2.iFlag			= atoi(olStrArray[0]);
		rlInlineUpdate2.iRow			= atoi(olStrArray[1]);
		rlInlineUpdate2.iColumn			= atoi(olStrArray[2]);
		rlInlineUpdate2.iRowOffset		= atoi(olStrArray[3]);
		rlInlineUpdate2.iColumnOffset	= atoi(olStrArray[4]);
		rlInlineUpdate2.oOldText		= olStrArray[5];
		rlInlineUpdate2.oNewText		= olStrArray[6];
		rlInlineUpdate2.oPoint.x		= atoi(olStrArray[7]);
		rlInlineUpdate2.oPoint.y		= atoi(olStrArray[8]);
	}
	rlInlineUpdate1.oNewText = "";
	
	int ilGroupLine1 = rlInlineUpdate1.iRow + rlInlineUpdate1.iRowOffset;
	int ilGroupLine2 = rlInlineUpdate2.iRow + rlInlineUpdate2.iRowOffset;
	
	if(ilGroupLine1 < omGroups.GetSize() && ilGroupLine2 < omGroups.GetSize())
	{
		if((omGroups[ilGroupLine1].oWgpCode != "" || omGroups[ilGroupLine2].oWgpCode != "") && ilGroupLine1 != ilGroupLine2)
		{
			DutyChangeGroupDlg olDutyChangeGroupDlg(&rlInlineUpdate1,&rlInlineUpdate2, this);
			if (olDutyChangeGroupDlg.DoModal() == IDOK) 
			{
				// Aktuelisierungsbutton mu� gesetzt werden
				bmNeedActualButton = true;
			}
			
		}
		else
		{
			MessageBeep((UINT)-1);
		}
	}
	CCS_CATCH_ALL;
}

//****************************************************************************
// DeleteStaffFromData: l�scht alle auf den Mitarbeiter mit der
//	Urno <lpStfUrno> bezogenen Datens�tze aus allen globalen und
//	lokalen Datenhaltungsobjekten. Die Datens�tze werden nur aus
//	den Objekten gel�scht, nicht in der Datenbank.
// R�ckgabe:	keine
//****************************************************************************

void DutyRoster_View::DeleteStaffFromData(long lpStfUrno)
{
	CCS_TRY;
	// DRR-Daten aus interner Datenhaltung entfernen
	ogDrrData.RemoveInternalByStaffUrno(lpStfUrno,true/*Remove DRS*/,false/*don't send BC*/);
	// DRA-Daten aus interner Datenhaltung entfernen
	ogDraData.RemoveInternalByStaffUrno(lpStfUrno,false/*don't send BC*/);
	// DRD-Daten aus interner Datenhaltung entfernen
	ogDrdData.RemoveInternalByStaffUrno(lpStfUrno,false/*don't send BC*/);
	// DRG-Daten aus interner Datenhaltung entfernen
	ogDrgData.RemoveInternalByStaffUrno(lpStfUrno,false/*don't send BC*/);
	// SPR-Daten aus interner Datenhaltung entfernen
	ogSprData.RemoveInternalByStaffUrno(lpStfUrno,false/*don't send BC*/);
	
	// ogACC ////
	for(int i=0; i<ogAccData.omData.GetSize(); i++)
	{
		if(lpStfUrno == ogAccData.omData[i].Stfu)
		{
			ACCDATA *prlAcc =  ogAccData.GetAccByUrno(ogAccData.omData[i].Urno);
			ogAccData.DeleteInternal(prlAcc, false);
			i--;
		}
	}
	
	// omSTF ////
	int ilSize = omStfData.GetSize();
	for(i=0; i<ilSize; i++)
	{
		if(lpStfUrno == omStfData[i].Urno)
		{
			omStfData.DeleteAt(i);
			break;
		}
	}
	
	// rmViewInfo.oStfUrnos ////
	ilSize = rmViewInfo.oStfUrnos.GetSize();
	CString olStfUrno;
	olStfUrno.Format("%ld",lpStfUrno);
	for(i=0; i<ilSize; i++)
	{
		if(olStfUrno == rmViewInfo.oStfUrnos[i])
		{
			rmViewInfo.oStfUrnos.RemoveAt(i);
			break;
		}
	}
	
	CCS_CATCH_ALL;
}

//****************************************************************************
// 
//****************************************************************************


void DutyRoster_View::LoadStaffInData(long lpStfUrno, CTime opFrom /*=-1*/, CTime opTo/*=-1*/)
{
	CCS_TRY;
	CString olAccWhere;
	CString olStfWhere;
	CString olSQL;	
	bool blLoadFailed = false;
	
	COleDateTime olLoadDateFrom;
	COleDateTime olLoadDateTo;
	if(opFrom != -1 && opTo != -1)
	{
		olLoadDateFrom = CTimeToCOleDateTime(opFrom);
		olLoadDateTo   = CTimeToCOleDateTime(opTo);
	}
	else
	{
		olLoadDateFrom = rmViewInfo.oLoadDateFrom;
		olLoadDateTo   = rmViewInfo.oLoadDateTo;
	}
	
	// SQL-Statement f�r DRR/DRS/DRA/DRD (benutzen alle dasselbe)
	olSQL.Format("WHERE STFU='%ld' AND SDAY BETWEEN '%s' AND '%s'",lpStfUrno,olLoadDateFrom.Format("%Y%m%d"),olLoadDateTo.Format("%Y%m%d"));
	
	// DRR-Daten nachladen
	if(ogDrrData.Read(olSQL.GetBuffer(0),NULL,true,false) == false)
	{
		ogDrrData.omLastErrorMessage.MakeLower();
		if(ogDrrData.omLastErrorMessage.Find("no data found") == -1)
		{
			CString olErrorTxt;
			olErrorTxt.Format("%s %d\n%s",LoadStg(ST_READERR), ogDrrData.imLastReturnCode, ogDrrData.omLastErrorMessage);
			MessageBox(olErrorTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
			blLoadFailed = true;
		}
	}
	olSQL.ReleaseBuffer();
	
	// DRS-Daten nachladen
	if(ogDrsData.Read(olSQL.GetBuffer(0),NULL,true,false) == false)
	{
		ogDrsData.omLastErrorMessage.MakeLower();
		if(ogDrsData.omLastErrorMessage.Find("no data found") == -1)
		{
			CString olErrorTxt;
			olErrorTxt.Format("%s %d\n%s",LoadStg(ST_READERR), ogDrsData.imLastReturnCode, ogDrsData.omLastErrorMessage);
			MessageBox(olErrorTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
			blLoadFailed = true;
		}
	}
	olSQL.ReleaseBuffer();
	
	// DRD-Daten nachladen
	if(ogDrdData.Read(olSQL.GetBuffer(0),NULL,true,false) == false)
	{
		ogDrdData.omLastErrorMessage.MakeLower();
		if(ogDrdData.omLastErrorMessage.Find("no data found") == -1)
		{
			CString olErrorTxt;
			olErrorTxt.Format("%s %d\n%s",LoadStg(ST_READERR),ogDrdData.imLastReturnCode,ogDrdData.omLastErrorMessage);
			MessageBox(olErrorTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
			blLoadFailed = true;
		}
	}
	olSQL.ReleaseBuffer();
	
	// DRA-Daten nachladen
	if(ogDraData.Read(olSQL.GetBuffer(0),NULL,true,false) == false)
	{
		ogDraData.omLastErrorMessage.MakeLower();
		if(ogDraData.omLastErrorMessage.Find("no data found") == -1)
		{
			CString olErrorTxt;
			olErrorTxt.Format("%s %d\n%s",LoadStg(ST_READERR),ogDraData.imLastReturnCode,ogDraData.omLastErrorMessage);
			MessageBox(olErrorTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
			blLoadFailed = true;
		}
	}
	olSQL.ReleaseBuffer();

	// Load Acc-Data ////
	CString olDateYFrom = olLoadDateFrom.Format("%Y");	//the FROM-Year we need
	CString olDateYTo   = olLoadDateTo.Format("%Y");	//the TO-Year we need
	bool	blLoadAcc	= false;						//indicator, if we have already loaded the data
	
	if (olDateYFrom == olDateYTo)
	{
		if (!ogAccData.IsAccExisted(lpStfUrno, olDateYFrom))
		{
			olAccWhere.Format("WHERE STFU = '%ld' AND YEAR = '%s'",lpStfUrno, olDateYFrom);
			blLoadAcc = true;
		}
	}
	else
	{
		//look which years to load
		bool blLoadYearFrom = false;
		bool blLoadYearTo = false;
		if (!ogAccData.IsAccExisted(lpStfUrno, olDateYFrom))
		{
			blLoadYearFrom = true;
		}
		if (!ogAccData.IsAccExisted(lpStfUrno, olDateYTo))
		{
			blLoadYearTo = true;
		}
		
		if (blLoadYearFrom != true && blLoadYearTo != true)
		{
			blLoadAcc = false;
		}
		else	//look after the SQL-statement
		{
			blLoadAcc = true;
			if (blLoadYearFrom == true && blLoadYearTo == true)
			{
				olAccWhere.Format("WHERE STFU = '%ld' AND YEAR BETWEEN '%s' AND '%s'",lpStfUrno, olDateYFrom, olDateYTo);
			}
			else if (blLoadYearFrom == true && blLoadYearTo == false)
			{
				olAccWhere.Format("WHERE STFU = '%ld' AND YEAR = '%s'",lpStfUrno, olDateYFrom);
			}
			else
			{
				olAccWhere.Format("WHERE STFU = '%ld' AND YEAR = '%s'",lpStfUrno, olDateYTo);
			}
		}
	}
	
	if (blLoadAcc == true)
	{
		if(ogAccData.Read((char*)(LPCTSTR)olAccWhere,false) == false)
		{
			ogAccData.omLastErrorMessage.MakeLower();
			if(ogAccData.omLastErrorMessage.Find("no data found") == -1)
			{
				CString olErrorTxt;
				olErrorTxt.Format("%s %d\n%s",LoadStg(ST_READERR), ogAccData.imLastReturnCode, ogAccData.omLastErrorMessage);
				MessageBox(olErrorTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
				blLoadFailed = true;
			}
		}
	}
	
	// loading STF-Data
	
	//extracting the employee-urnos of the view
	CMapStringToPtr olMap;
	for (int ilC = rmViewInfo.oStfUrnos.GetSize () - 1; ilC > -1; ilC--)
	{
		olMap.SetAt(rmViewInfo.oStfUrnos.GetAt (ilC),NULL);
	}
	
	//is the employee already in the view?
	void* polDummy;
	CString olKey;
	olKey.Format("%ld", lpStfUrno);
	if (!olMap.Lookup (olKey, polDummy))
	{
		olStfWhere.Format("WHERE URNO = '%ld'" ,lpStfUrno);
		ogStfData.ReadSpecialData(&omStfData, (char*)(LPCTSTR)olStfWhere, "", false);
		
		if (rmViewInfo.iSortFVN == 1)	// Mitarbeiter sortieren nach Funktion,Vertrag,Name
		{
			omStfData.Sort(CompareStfByFctConName);
		}
		else
		{
			omStfData.Sort(CompareStfByName);
		}
	}
	
#ifdef _DEBUG
	TRACE("CompareStfByFctConName Performance: Number of total Calls: %i, Number of Spf calls: %i, Time Spf: %i, Number of Sco calls: %i, Time Sco: %i\n", 
		giCallCompareStfByFctConName, giCallSpf, giTimeSpf, giCallSco, giTimeSco);
#endif _DEBUG
	
	// Make actual and sorted Urnolist
	CString olTmpUrno;
	rmViewInfo.oStfUrnos.RemoveAll();
	int ilSize = omStfData.GetSize();
	
	//look after unique URNOs
	for (int i = 0; i < ilSize; i++)
	{
		olTmpUrno.Format("%ld", omStfData[i].Urno);
		rmViewInfo.oStfUrnos.Add(olTmpUrno);
	}
	
	CCS_CATCH_ALL;
}

//****************************************************************************
// Pr�ft, ob ein Mitarbeiter in einem Zeitraum anwesend ist - alle/nur anwesend/nur abwesend - Filter
//****************************************************************************

bool DutyRoster_View::MustStaffInPeriode(long lpStfUrno)
{
	CCS_TRY;
	
	COleDateTime olFrom;
	COleDateTime olTo;
	if(rmViewInfo.bSortDayly)
	{
		// Tages bezogen
		olFrom	= rmViewInfo.oSelectDate;
		olTo	= rmViewInfo.oSelectDate;
	}
	else
	{
		// Zeitraum bezogen
		olFrom	= rmViewInfo.oDateFrom;
		olTo	= rmViewInfo.oDateTo;
	}
	
	// Anzahl der Tage ermitteln
	COleDateTimeSpan olPeriode = olTo - olFrom;
	int ilDaysInPeriode = (int)olPeriode.GetDays() + 1;
	
	CStringArray opRoslArray;
	ExtractItemList(rmViewInfo.oPType, &opRoslArray, ',');
	
	DRRDATA* prlDrr;
	int ilRoslCount = 0;
	// F�r jeden Tag innerhalb der Periode durchgehen
	for(int i=0; i<ilDaysInPeriode; i++)
	{
		COleDateTime olActualDay = olFrom + COleDateTimeSpan(i,0,0,0);
		
		// aktiven DRR ermitteln
		// wir checken alle Planstufen, die im rmViewInfo.oPType gespeichert sind
		prlDrr = 0;
		for(ilRoslCount=opRoslArray.GetSize()-1; ilRoslCount>=0; ilRoslCount--)
		{
			prlDrr = ogDrrData.GetDrrByKey(olActualDay.Format("%Y%m%d"),lpStfUrno,"1", opRoslArray[ilRoslCount]);
			if (prlDrr != 0)
				break;
		}
		
		if(prlDrr == 0)
		{
			if(rmViewInfo.iPresent == 2 && ilRoslCount == -1)
			{
				// for-Schleifen-�berlauf, d.h. kein Eintrag
				return true;
			}
			else
				continue;
		}
		
		if(rmViewInfo.iPresent == 2)
		{
			// nur anwesenden + alle leeren, die kein Eintrag haben
			
			// Schichtcode pr�fen: handelt es sich um eine Anwesenheit?
			if(ogBsdData.GetBsdByUrno(prlDrr->Bsdu))
				return true;
		}
		else // if(rmViewInfo.iPresent == 3)
		{
			// nur abwesenden
			// Schichtcode pr�fen: handelt es sich um eine Abwesenheit?
			if(ogOdaData.GetOdaByUrno(prlDrr->Bsdu))
				return true;
		}
	}
	return false;
	CCS_CATCH_ALL;
	return false;
}

//****************************************************************************
// FillAccountStruct: aktualisiert die Anzeige der Arbeitszeitkonten
//	der Mitarbeiter.
//	R�ckgabe: keine
//****************************************************************************

void DutyRoster_View::FillAccountStruct()
{
	CCS_TRY;
	CCS_CATCH_ALL;
}

//****************************************************************************
// Gr��e des benutzbaren Client Rects berechnen.
//****************************************************************************

CRect DutyRoster_View::CalcUsableClientRect()
{
	CRect olClientRect;
	//GetParentFrame()->GetClientRect(olClientRect);
	GetClientRect(olClientRect);
	return (olClientRect);
}

//****************************************************************************
//  Ver�nderung der Gr��e an alle Clients weitergeben.
//****************************************************************************

void DutyRoster_View::SizeWindow(UINT nSide, LPRECT lpRect ) 
{
	CCS_TRY;
	CCS_CATCH_ALL;
}

//****************************************************************************
// 
//****************************************************************************

void DutyRoster_View::OnSize(UINT nType, int cx, int cy) 
{
	CCS_TRY;
	CFormView::OnSize(nType, cx, cy);
	// Ver�nderung der Tabellen durch die Bewegung
	// Tabellen d�rfen nicht bewegt werden, bevor sie initialisiert sind.
	if (pomSolidTabViewer != NULL && pomIsTabViewer != NULL && pomDebitTabViewer != NULL && pomStatTabViewer != NULL)
	{
		// Gr��e des benutzbaren Client Rects berechnen.
		CRect olClientRect;
		olClientRect = CalcUsableClientRect();
		
		pomSolidTabViewer->TableSizing(/*8,*/&olClientRect);
		pomIsTabViewer->TableSizing(/*8,*/&olClientRect);
		pomDebitTabViewer->TableSizing(/*8,*/&olClientRect);
		pomStatTabViewer->TableSizing(/*8,*/&olClientRect);
	}
	CCS_CATCH_ALL;
}

//****************************************************************************
// OnSelectMa: EventHandler zum Aufruf des Mitarbeiter-Dialoges.
// R�ckgabe:	keine
//****************************************************************************

void DutyRoster_View::OnSelectMa() 
{
	CCS_TRY;
	// Mitarbeiter-Auswahl-Dialog aufrufen
	CString olDummy = "";
	DoSelectStaffDialog(olDummy);
	CCS_CATCH_ALL;
}

//****************************************************************************
// DoSelectStaffDialog: ruft den Dialog zur Ansicht aller Mitarbeiter 
//	auf. In diesem Dialog lassen sich Mitarbeiter zur Ansicht hinzuf�gen
//	oder l�schen, etc.. <opStfUrno> gibt den zu selektierenden MA an.
// R�ckgabe:	keine
//****************************************************************************

void DutyRoster_View::DoSelectStaffDialog(CString opStfUrno /* = ""*/) 
{
	CCS_TRY;
	// nur ausf�hrbar, wenn eine g�ltige Ansicht ausgew�hlt wurde
	if(rmViewInfo.bCorrectViewInfo == false)
	{
		// Meldung ausgeben und terminieren
		MessageBeep((UINT)-1);
		return;
	}

	//////////////////////////////////////////////////////////////////////////////////
	///////////// Liste der Mitarbeiter in der Ansicht f�r Dialog erzeugen
	//////////////////////////////////////////////////////////////////////////////////
	// Puffer f�r Urno-Liste
	CStringList olUrnoList;
	CString		olTmpUrno;
	int i;

	// alle Mitarbeiter durchgehen
	for (i = 0; i < omStfData.GetSize(); i++)
	{
		// Urno formatieren...
		olTmpUrno.Format("%ld",omStfData[i].Urno);
		// ...und im Urno-Array der Ansicht speichern
		olUrnoList.AddTail(olTmpUrno);
	}
	//////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////
	///////////// Dialog aufrufen
	//////////////////////////////////////////////////////////////////////////////////
	CGridEmplDlg olGridEmplDlg(&olUrnoList,&opStfUrno,&omExtraUrnoMap,this);
	if (olGridEmplDlg.DoModal() == IDCANCEL) return; // �nderungen ignorieren
	//////////////////////////////////////////////////////////////////////////////////
	
	//////////////////////////////////////////////////////////////////////////////////
	///////////// Mitarbeiter hinzuf�gen
	//////////////////////////////////////////////////////////////////////////////////
	// Nachrichtenverarbeitung anstossen, damit das Fenster an der Stelle neu 
	// gezeichnet wird, wo vorher der Dialog war
	LPMSG olMsg = NULL;
	while (PeekMessage(olMsg, NULL, 0, 0, PM_REMOVE))
	{
		if (olMsg->message == WM_QUIT)
			break;
		TranslateMessage(olMsg);         
		DispatchMessage(olMsg);      
	}	

	// WarteDialog und -Cursor einblenden.
	CWaitDlg plWaitDlg(this,LoadStg(WAIT_DATA_PREPARE));

	// Position im String Array
	POSITION olPos = olUrnoList.GetHeadPosition();
	// die zu �berpr�fende Urno aus der Dialog-Liste
	long llCheckUrno;
	// dito als String
	CString olCheckUrno;
	// muss die Urno hinzugef�gt werden?
	bool blAdd, blRemove, blUpdateView = false;

	// Sollte die Anzahl gleich geblieben sein, Funktion beenden.
	if (omStfData.GetSize() == olUrnoList.GetCount())
	{
		pomIsTabViewer->ScrollToVisible(atol(opStfUrno.GetBuffer(0)));
		return;
	}

	if (omStfData.GetSize() < olUrnoList.GetCount())
	{
		//////////////////////////////////////////////////////////////////////////////////
		///////////// Mitarbeiter hinzuf�gen
		//////////////////////////////////////////////////////////////////////////////////

		// alle hinzuzuf�genden MAs ermitteln
		while (olPos != NULL)
		{
			// n�chste MA-Urno aus der im Dialog erstellten Liste lesen

			olCheckUrno = olUrnoList.GetNext(olPos);
			llCheckUrno = atol(olCheckUrno.GetBuffer(0));
			blAdd = true;

			// Urno in der Liste der MA-Urnos in der Ansicht suchen
			for (i = 0; i < omStfData.GetSize(); i++)
			{
				// Urno gefunden?
				if (llCheckUrno == omStfData[i].Urno)
				{
					// ja -> Suche abbrechen, dieser MA ist bereits in der Ansicht
					blAdd = false;
					break;
				}
			}
			// Urno hinzuf�gen?
			if (blAdd)
			{
				//ja
				LoadStaffInData(llCheckUrno);

				// Urno in die Liste der Eingef�gten Urnos aufnehmen
				omExtraUrnoListInView.Add(olCheckUrno);
				omExtraUrnoMap.SetAt(olCheckUrno,(CObject*)0);
				omDispExtraUrnoMap.SetAt(olCheckUrno,(CObject*)0);

				if (ogCCSParam.GetParamValue("GLOBAL","ID_ENABLE_ACC") == "Y")
				{
					SendViewStfuToScbhdlCommand(olCheckUrno, CString ("ADDSTF"), rmViewInfo.oDateFrom.Format("%Y"));
				}
			}
		}
	}
	else
	{
		//////////////////////////////////////////////////////////////////////////////////
		///////////// Mitarbeiter entfernen
		//////////////////////////////////////////////////////////////////////////////////
		// Urno in der Liste der MA-Urnos in der Ansicht suchen
		CObject* polDummy;
		for (i = omStfData.GetSize()-1; i > -1; i--)
		{
			// Urno ermitteln
			llCheckUrno = omStfData[i].Urno;
			olCheckUrno.Format("%ld",omStfData[i].Urno);
			
			// Urno nur entfernen, wenn sie in der Extraliste steht. 
			// Es d�rfen keine Urnos gel�scht werden die urspr�nglich im View schon vorhanden waren
			if (omExtraUrnoMap.Lookup(olCheckUrno,polDummy))
			{
				blRemove = true;
				// diese Urno in der Liste aus dem Dialog suchen
				POSITION olPos = olUrnoList.GetHeadPosition();

				while (blRemove && (olPos != NULL))
				{
					if(atol(olUrnoList.GetNext(olPos).GetBuffer(0)) == llCheckUrno)
					{
						blRemove = false; // ja -> Suche abbrechen, Mitarbeiter bleibt in der Ansicht
					}
				};

				if (blRemove) //MA aus view entfernen
				{
					DeleteStaffFromData(llCheckUrno);

					for (int j = 0; j < omExtraUrnoListInView.GetSize(); j++)
					{
						if (omExtraUrnoListInView[j] == olCheckUrno)
						{
							omExtraUrnoListInView.RemoveAt(j);
							break;
						}
					}
					omExtraUrnoMap.RemoveKey(olCheckUrno);
					omDispExtraUrnoMap.RemoveKey(olCheckUrno);

					if (ogCCSParam.GetParamValue("GLOBAL","ID_ENABLE_ACC") == "Y")
					{
						SendViewStfuToScbhdlCommand(olCheckUrno, CString ("DELSTF"), rmViewInfo.oDateFrom.Format("%Y"));
					}
				}
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////
	///////////// Ansicht neu zeichnen
	//////////////////////////////////////////////////////////////////////////////////
	// muss die Ansicht �berarbeitet werden?
	if (blAdd || blRemove)
	{
		// Ansicht aktualisieren
		pomStatTabViewer->ChangeView(false,false);
		if(rmViewInfo.iGroup == 2)
		{
			FillGroupStruct(false);
		}

		// Tabellen neu zeichnen
		pomSolidTabViewer->ChangeView();
		pomIsTabViewer->ChangeView();
		pomStatTabViewer->ChangeView();
		pomDebitTabViewer->ChangeView();

		// Auch die View Liste aktualisieren
		SaveUrnoListInView(omExtraUrnoListInView);
	}

	// WarteDialog l�schen
	plWaitDlg.DestroyWindow();

	// normalen Mauszeiger wiederherstellen
	RemoveWaitCursor();
	//////////////////////////////////////////////////////////////////////////////////

	CCS_CATCH_ALL;
}

//****************************************************************************
// Beenden des Dialog
//****************************************************************************

void DutyRoster_View::OnClose() 
{
	CFormView::OnClose();
}

//****************************************************************************
// Beenden des Dialog
//****************************************************************************

void DutyRoster_View::OnDestroy() 
{
	CFormView::OnDestroy();
}

//****************************************************************************
// LoadDrwData: l�dt die Datens�tze der W�nsche, die
//	zur Generierung der Ansicht ben�tigt werden.
//	R�ckgabe:	false	->	Fehler beim Laden der Daten
//				true	->	Daten geladen oder keine Daten vorhanden
//****************************************************************************

bool DutyRoster_View::LoadDrwData(CMapPtrToPtr *popLoadStfUrnoMap,
								  CString opUrnoString /*=""*/) 
{
	CCS_TRY;
	WriteLogFull("enter LoadDrwData()");
	// R�ckgabe-Wert
	bool blReturn = true;
	// Puffer f�r WHERE-Clause
	CString olDrwWhere,olDrwUrnos;
	// Filterkriterium Start- und Enddatum f�r Datens�tze
	CString olDateDFrom  = rmViewInfo.oLoadDateFrom.Format("%Y%m%d");
	CString olDateDTo    = rmViewInfo.oLoadDateTo.Format("%Y%m%d");
	
	// TODO: L�schen des alten Inhalts ?
	// globales Objekt zur Haltung der Datens�tze zur�cksetzen
	//ogDrwData.ClearAll(true);
	
	// Mitarbeiter-String f�r WHERE-Clause benutzen?
	if (popLoadStfUrnoMap == NULL){
		// ja -> der String <opUrnoString> enth�lt die Urnos aller in 
		// Betracht kommender Mitarbeiter
		olDrwWhere.Format("WHERE STFU IN (%s) AND SDAY BETWEEN '%s' AND '%s'", opUrnoString, olDateDFrom, olDateDTo);
	}
	else{
		// nein -> benutze Pointer-Map, um die g�ltigen Mitarbeiter
		// auszufiltern
		//ogDrwData.SetLoadStfuUrnoMap(popLoadStfUrnoMap);
		olDrwWhere.Format("WHERE SDAY BETWEEN '%s' AND '%s'", olDateDFrom, olDateDTo);
	}
	//strcpy(pclDrwWhere, olDrwWhere);
	
	// Datens�tze abfragen
	if (ogBCD.Read("DRW",olDrwWhere) == false)
		//if(ogDrwData.Read(pclDrwWhere) == false)
	{
		// Fehler -> echter Fehler (anderer als 'Keine Datens�tze gefunden')?
		//ogDrwData.omLastErrorMessage.MakeLower();
		//if(ogDrwData.omLastErrorMessage.Find("no data found") == -1)
		//{
		// ja -> ausgeben
		//	CString olErrorTxt;
		//	olErrorTxt.Format("%s %d\n%s",LoadStg(ST_READERR), ogDrwData.imLastReturnCode, ogDrwData.omLastErrorMessage);
		//	MessageBox(olErrorTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
		//	blReturn = false;
		//}
	}
	
	//-------------------------------------------------------------------------------
	// Alle Mitarbeiter aus den internen Daten l�schen, falls diese nicht 
	// in der der View vorhanden sind.
	//-------------------------------------------------------------------------------
	
	// Mitarbeiter-String f�r WHERE-Clause benutzen?
	if (popLoadStfUrnoMap != NULL){
		
		CStringArray	olDeleteUrnos;
		CString			olDatabaseStfu;
		long			ilDatabaseStfu;
		void			*prlVoid = NULL;
		
		// Alle internen Daten durchgehen
		for (int i=0;i<ogBCD.GetDataCount("DRW");i++)
		{
			olDatabaseStfu = ogBCD.GetField("DRW", i, "STFU");
			ilDatabaseStfu = atol(olDatabaseStfu);
			// Ist diese Urno in der Liste der anzuzeigenen Urnos ?
			if(popLoadStfUrnoMap->Lookup((void *)ilDatabaseStfu, (void *&)prlVoid) == FALSE)
				// Nicht gefunden, zum L�schen markieren.
			{
				olDeleteUrnos.Add(ogBCD.GetField("DRW", i, "URNO"));
			}
		}
		// manuelles L�schen aus den internen Daten
		for (i=0;i<olDeleteUrnos.GetSize();i++)
		{
			ogBCD.DeleteRecord("DRW","URNO",olDeleteUrnos.GetAt(i));
		}
	}
	return blReturn;
	CCS_CATCH_ALL;
	return false;
}

//****************************************************************************
// Gibt den DRR Datensatz des aktuell ausgew�hlten Feldes zur�ck. Null falls 
// Fehler.
//****************************************************************************

DRRDATA* DutyRoster_View::GetActualDrr()
{
	// Der R�ckgabe DRR
	DRRDATA *prlDrr = NULL;
	
	int ilRow			= rmPlanInlineData.iRow;	
	int ilColumn		= rmPlanInlineData.iColumn;
	int ilRowOffset		= rmPlanInlineData.iRowOffset;
	int ilColumnOffset	= rmPlanInlineData.iColumnOffset;
	CString olOldText	= rmPlanInlineData.oOldText;
	CString olNewText	= rmPlanInlineData.oNewText;
	CPoint olPoint      = rmPlanInlineData.oPoint;
	//int ilRButtonDownStatus	= rmPlanInlineData.iRButtonDownStatus;
	
	int ilStfRow = 0;
	CString olPType;
	int ilPTypeNr = 0;
	
	bool blTmp = pomIsTabViewer->GetElementAndPType(ilRow+ilRowOffset, ilStfRow, olPType, ilPTypeNr);
	if(!blTmp)	return NULL;
	
	if(((ilStfRow < omStfData.GetSize() && rmViewInfo.iGroup == 1)||
		(ilStfRow < omGroups.GetSize() && rmViewInfo.iGroup == 2)) && 
		(olPType == "1" || olPType == "2" || olPType == "3" || olPType == "4" || olPType == "L" || olPType == "5" || olPType == "U"))
	{
		//---------------------------------
		// Mitarbeiter URNO erhalten
		long llStfUrno = 0;
		if(rmViewInfo.iGroup == 2)
			llStfUrno = omGroups[ilStfRow].lStfUrno;
		else
			llStfUrno = omStfData[ilStfRow].Urno;
		//---------------------------------
		// Tag berechnen
		CString olCode;
		COleDateTime olDay = rmViewInfo.oShowDateFrom;
		olDay += COleDateTimeSpan(ilColumn+ilColumnOffset,0,0,0);
		CTime olTime(olDay.GetYear(), olDay.GetMonth(), olDay.GetDay(), 0, 0, 0);
		CString olSday = olDay.Format("%Y%m%d");
		//---------------------------------
		//  DRR Datensatz laden.
		prlDrr = ogDrrData.GetDrrByKey(olSday,llStfUrno,"1",olPType);
	}
	return (prlDrr);
}

//****************************************************************************
// ShowMultiDrrMenu: f�gt alle Men�punkte zum Bearbeiten von zus�tzlichen
//	DRRs hinzu.
// R�ckgabe:	BOOL Men�punkt(e) hinzugef�gt?
//****************************************************************************

bool DutyRoster_View::ShowMultiDrrMenu(DRRDATA *popDrr, CMenu *popMenu)
{
	CString csCode;
	csCode = ogPrivList.GetStat("DUTYROSTER_SHOWMULTIDRR");
	if (csCode == '0' || csCode == '-') 
	{
		return false;
	}

	// Besonderer Fall in der Schichtplanstufe
	bool blIsRosl1 = false;
	if ((CString(popDrr->Rosl) == "1") || (CString(popDrr->Ross) != "A"))
	{
		blIsRosl1 = true;
	}
	
	// g�ltiger DRR?
	if (popDrr == NULL || popDrr->Bsdu == 0)
		return false;
	
	// Es mu� eine aktive Stufe sein
	if (CString(popDrr->Ross ) != "A" && !blIsRosl1)
		return false;
	
	// keine Men�punkte, wenn es noch keinen Schichtcode gibt
	if (CString(popDrr->Scod) == "")
		return false;
	
	if (!blIsRosl1)
		// Men�punkt 'Anwesenheit / Abwesenheit eintragen' hinzuf�gen
		popMenu->AppendMenu(MF_STRING, MENUE_ADD_DRR, LoadStg(IDS_STRING968));
	
	int ilDrrn;				// DRR-Nummer
	CString olDrrScod;		// Schichtcode
	CString olMenuString;	// Eintrag im Men�
	
	// alle zus�tzlichen Schichten ermitteln
	CCSPtrArray<DRRDATA> olDrrList;
	
	// mehr als einen DRR gefunden?
	if (ogDrrData.GetDrrListByKeyWithoutDrrn(CString(popDrr->Sday),popDrr->Stfu,CString(popDrr->Rosl),&olDrrList) > 1)
	{
		// ja -> Men�punkte generieren
		for (int ilCount=1; ilCount<olDrrList.GetSize(); ilCount++)
		{
			DRRDATA* polDrr = (DRRDATA*)olDrrList.CPtrArray::GetAt(ilCount);
			// DRR-Nummer und Schichtcode ermitteln
			olDrrScod = polDrr->Scod;
			ilDrrn = atoi(polDrr->Drrn);
			
			// g�ltiger DRR?
			if ((ilDrrn > 1) && (ilDrrn <= MAX_DRR_DRRN) && (olDrrScod != ""))
			{
				if (ilCount == 1)
				{
					// Men�punkt SEPARATOR hinzuf�gen f�r bessere �bersichtlichkeit
					popMenu->AppendMenu(MF_SEPARATOR);
				}
				
				olMenuString = GetDrrMenuString(polDrr, MENUE_EDIT_DRR);
				popMenu->AppendMenu(MF_STRING, MENUE_EDIT_DRR + ilDrrn, olMenuString);
				
				if (!blIsRosl1)
				{
					olMenuString = GetDrrMenuString(polDrr, MENUE_DELETE_DRR);
					popMenu->AppendMenu(MF_STRING, MENUE_DELETE_DRR + ilDrrn, olMenuString);
				}
			}
		}
	}
	return true;
}

//****************************************************************************
// ShowCreateDoubleTourMenu: pr�ft, ob im Kontextmenu der Menupunkt 
//	"Doppeltour erzeugen" angezeigt werden muss und f�gt ihn ggf. hinzu
// R�ckgabe:	BOOL Men�punkt hinzugef�gt?
//****************************************************************************

bool DutyRoster_View::ShowCreateDoubleTourMenu(DRRDATA *popDrr, CMenu *popMenu)
{
	CString csCode;
	csCode = ogPrivList.GetStat("DUTYROSTER_CREATEDOUBLETOUR");
	if (csCode == '0' || csCode == '-') 
	{
		return false;
	}

	// g�ltiger DRR?
	if (popDrr == NULL || popDrr->Bsdu == 0)
		return false;
	
	// nicht anzeigen bei Planugstufe "Langzeitdienstplan"
	if (CString(popDrr->Rosl ) == "L" || CString(popDrr->Rosl) == "1")
		return false;
	
	// Es mu� eine aktive Stufe sein
	if (CString(popDrr->Ross ) != "A")
		return false;
	
	// Es darf noch keiner Doppeltour zugeordnet sein
	if (ogDrrData.HasDoubleTour(popDrr))
		return false;
	
	// Nur Anwesenheitschichten sind erlaubt.
	if (!ogBsdData.IsBsdCode(popDrr->Scod))
		return false;
	
	// Men�punkt hinzuf�gen
	popMenu->AppendMenu(MF_STRING, MENUE_CREATE_DOUBLETOUR, LoadStg(IDS_STRING967));
	return true;
}

//****************************************************************************
// ShowCancelDoubleTourMenu: pr�ft, ob im Kontextmenu der Menupunkt 
//	"Doppeltour trennen" angezeigt werden muss und f�gt ihn ggf. hinzu
// R�ckgabe:	BOOL Men�punkt hinzugef�gt?
//****************************************************************************

bool DutyRoster_View::ShowCancelDoubleTourMenu(DRRDATA *popDrr, CMenu *popMenu)
{
	CString csCode;
	csCode = ogPrivList.GetStat("DUTYROSTER_SHOWCANCELDOUBLETOUR");
	if (csCode == '0' || csCode == '-') 
	{
		return false;
	}

	// g�ltiger DRR?
	if (popDrr == NULL || popDrr->Bsdu == 0)
		return false;
	
	// nicht anzeigen bei Planugstufe "Langzeitdienstplan"
	if (CString(popDrr->Rosl ) == "L" || CString(popDrr->Rosl) == "1")
		return false;
	
	// Es mu� eine aktive Stufe sein
	if (CString(popDrr->Ross ) != "A")
		return false;
	
	// Es darf mu� einer Doppeltour zugeordnet sein
	if (!ogDrrData.HasDoubleTour(popDrr))
		return false;
	
	// Men�punkt anzeigen
	popMenu->AppendMenu(MF_STRING, MENUE_CANCEL_DOUBLETOUR, LoadStg(IDS_STRING125));
	return true;
}

//****************************************************************************
// ShowPlanMenu: pr�ft, ob im Kontextmenu der Menupunkt 
//	"Dienstplan bearbeiten" angezeigt werden muss und f�gt ihn ggf. hinzu
// R�ckgabe:	BOOL Men�punkt hinzugef�gt?
//****************************************************************************

bool DutyRoster_View::ShowPlanMenu(DRRDATA *popDrr, CMenu *popMenu)
{
	CString csCode;
	// Dienstplan darf bearbeitet werden
	if (popDrr == NULL)
	{
		// Men�punkt anzeigen

		csCode = ogPrivList.GetStat("DUTYROSTER_PROCESSING");
	
		if(csCode=='1') 
		{
				popMenu->AppendMenu(MF_STRING, MENUE_PLAN, LoadStg(IDS_STRING1606));
				return true;
		}
	
		if(csCode=='0' || csCode=='-')
		return false;


	}
	
	// Men�punkt nicht anzeigen f�r Planugstufe "Langzeitdienstplan"
	if (CString(popDrr->Rosl ) == "L" || CString(popDrr->Rosl) == "1")
		return false;
	
	// Es mu� eine aktive Stufe sein
	if (CString(popDrr->Ross ) != "A")
		return false;
	
	// Men�punkt anzeigen
		csCode = ogPrivList.GetStat("DUTYROSTER_PROCESSING");
	
		if(csCode=='1') 
		{
				popMenu->AppendMenu(MF_STRING, MENUE_PLAN, LoadStg(IDS_STRING1606));
				return true;
		}
	
		if(csCode=='0' || csCode=='-')
		return false;

	return true;


}

/****************************************************************
Abh�ngig ob ODA, BSD, schreibgesch�tzt usw, setzt entsprechende Strings rein
ilType = MENUE_EDIT_DRR || MENUE_CLEAR_DRR oder MENUE_DELETE_DRR
****************************************************************/
CString DutyRoster_View::GetDrrMenuString(DRRDATA *popDrr, int ilType)
{
	CString olMenuString = "";
	
	// DRR g�ltig?
	if (popDrr == NULL || popDrr->Bsdu == 0)
		return olMenuString;
	
	
	if(ilType == MENUE_EDIT_DRR)
	{
		if (ogOdaData.GetOdaByUrno(popDrr->Bsdu) == NULL)
		{
			// BSD
			// In der Schichtplanstufe einen anderen Text zeigen.
			if (CString(popDrr->Rosl) == "1" || strcmp(popDrr->Ross, "A"))
			{
				if(strlen(popDrr->Fctc) > 0)
				{
					olMenuString.Format(LPCTSTR(LoadStg(IDS_STRING175)),popDrr->Scod,popDrr->Fctc);	// "Schicht %s / % ansehen*INTXT*"
				}
				else
				{
					olMenuString.Format(LPCTSTR(LoadStg(IDS_STRING182)),popDrr->Scod);	// "Schicht %s ansehen*INTXT*"
				}
			}
			else
			{
				if(strlen(popDrr->Fctc) > 0)
				{
					olMenuString.Format(LPCTSTR(LoadStg(IDS_STRING177)),popDrr->Scod,popDrr->Fctc);		// "Schicht %s / %s �ndern*INTXT*"
				}
				else
				{
					olMenuString.Format(LPCTSTR(LoadStg(IDS_STRING183)),popDrr->Scod);		// "Schicht %s �ndern*INTXT*"
				}
			}
		}
		else
		{
			// ODA
			// In der Schichtplanstufe einen anderen Text zeigen.
			if (CString(popDrr->Rosl) == "1" || strcmp(popDrr->Ross, "A"))
			{
				olMenuString.Format(LPCTSTR(LoadStg(IDS_STRING191)),popDrr->Scod);	// "Abwesenheit %s ansehen*INTXT*"
			}
			else
			{
				olMenuString.Format(LPCTSTR(LoadStg(IDS_STRING192)),popDrr->Scod);		// "Abwesenheit %s �ndern*INTXT*"
			}
		}
	}
	else if(ilType == MENUE_CLEAR_DRR || ilType == MENUE_DELETE_DRR)
	{
		if (ogOdaData.GetOdaByUrno(popDrr->Bsdu) == NULL)
		{
			if(strlen(popDrr->Fctc) > 0)
			{
				// "Schicht %s / %s l�schen*INTXT*"
				olMenuString.Format(LPCTSTR(LoadStg(IDS_STRING980)),popDrr->Scod,popDrr->Fctc);
			}
			else
			{
				// "Schicht %s l�schen*INTXT*"
				olMenuString.Format(LPCTSTR(LoadStg(IDS_STRING181)),popDrr->Scod);
			}
		}
		else
		{
			// -> "Abwesenheit %s l�schen*INTXT*"
			olMenuString.Format(LPCTSTR(LoadStg(IDS_STRING981)),popDrr->Scod);
		}
	}
	
	return olMenuString;
}

//****************************************************************************
// ShowEditDrrMenu: pr�ft, ob im Kontextmenu der Menupunkt 
//	"Tagesdaten bearbeiten" angezeigt werden muss und f�gt ihn ggf. hinzu
// R�ckgabe:	BOOL Men�punkt hinzugef�gt?
//****************************************************************************

bool DutyRoster_View::ShowEditDrrMenu(DRRDATA *popDrr, CMenu *popMenu)
{
	CString csCode;
	csCode = ogPrivList.GetStat("DUTYROSTER_SHOWEDITDRRMENU");
	if (csCode == '0' || csCode == '-') 
	{
		return false;
	}

	// DRR g�ltig?
	if (popDrr == NULL || popDrr->Bsdu == 0)
		return false;
	
	// Keine Bearbeitung bei Planugstufe "Langzeitdienstplan" oder "Schichtplan" zulassen
	// Men�punkt nicht anzeigen?
	CString olDontShow = "";
	if (olDontShow.Find(CString(popDrr->Rosl)) != -1)
	{
		return false;
	}
	
	popMenu->AppendMenu(MF_STRING, MENUE_OPENDRR, GetDrrMenuString(popDrr, MENUE_EDIT_DRR));
	return true;
}

//****************************************************************************
// ShowConflictMenu: pr�ft, ob im Kontextmenu der Menupunkt 
//	"Konflikpr�fung" angezeigt werden muss und f�gt ihn ggf. hinzu
// R�ckgabe:	BOOL Men�punkt hinzugef�gt?
//****************************************************************************

bool DutyRoster_View::ShowConflictMenu(DRRDATA *popDrr, CMenu *popMenu)
{
	CString csCode;
	csCode = ogPrivList.GetStat("DUTYROSTER_CONFLICTCHECK");
	if (csCode == '0' || csCode == '-') 
	{
		return false;
	}

	// Men�punkt nicht anzeigen?
	CString olDontShow = "A,U,L,1";
	if (popDrr == 0)
	{
		return false;
	}
	else if (olDontShow.Find(CString(popDrr->Rosl)) != -1)
	{
		return false;
	}
	
	UINT nFlags = MF_STRING;
	if (!rmViewInfo.bDoConflictCheck)
	{
		// Konfliktcheck ist ausgeschaltet, mu� gegrayt werden
		nFlags |= MF_GRAYED;
	}
	popMenu->AppendMenu(nFlags, MENUE_CONFLICTCHECK, LoadStg(IDS_STRING1677));
	return true;
}

//****************************************************************************
// ShowWishMenu: pr�ft, ob im Kontextmenu der Menupunkt "Wunsch anzeigen" 
//	angezeigt werden muss und f�gt ihn ggf. hinzu.
// R�ckgabe:	Men�punkt anzeigen?
//****************************************************************************

bool DutyRoster_View::ShowWishMenu(CString opSday, CString opStfUrno, CMenu *popMenu)
{
	CString csCode;
	csCode = ogPrivList.GetStat("DUTYROSTER_SHOWWISHMENU");
	if (csCode == '0' || csCode == '-') 
	{
		return false;
	}

	
	// Sollte f�r diesen Mitarbeiter an diesem Tag ein Wunschdatensatz
	// vorhanden sein, Menu aktivieren.
	CString olResult = ogBCD.GetFieldExt("DRW", "SDAY","STFU", opSday,opStfUrno,"URNO");
	//if (ogDrwData.GetDrwByKey(olSday,llStfUrno) != NULL)
	if (olResult.IsEmpty())
		return false;
	
	// Men�punkt hinzuf�gen
	popMenu->AppendMenu(MF_STRING, MENUE_SHOW_WISH, LoadStg(IDS_STRING1843));
	return true;
}

//****************************************************************************
// Setzt einen DRR zur�ck ("l�schen" einer DRR Eingabe)
//****************************************************************************

void DutyRoster_View::OnMenuClearDrr() 
{
	CCS_TRY;
	
	// Datens�tze l�schen
	//----------------------------------------------------------------------
	// Unterschiedliche Behandlung wenn mehrere Felder selektiert wurden
	//----------------------------------------------------------------------
	if (pomIsTabViewer->IsMultiSelection())
		// Alle markierten Drrs zur�cksetzen.
		pomIsTabViewer->ClearSelectedDrrData();
	else
	{
		// Einen einzigen DRR zr�cksetzen
		DRRDATA* popDrr = NULL;
		popDrr = GetActualDrr();
		
		if (popDrr == NULL)
			return;
		
		if (!ogDrrData.ClearDrr(popDrr))
			MessageBox(LoadStg(IDS_STRING1158),LoadStg(ST_FEHLER),MB_ICONWARNING);
	}
	
	// Alle Felder deselektieren
	pomIsTabViewer->UnselectFields();
	
	CCS_CATCH_ALL;
}

//****************************************************************************
// ShowClearDrr: pr�ft, ob im Kontextmenu der Menupunkt 
//	"Schicht/Abwesenheit l�schen" angezeigt werden muss und f�gt ihn ggf. hinzu
// R�ckgabe:	BOOL Men�punkt hinzugef�gt?
//****************************************************************************

bool DutyRoster_View::ShowClearDrr(DRRDATA *popDrr, CMenu *popMenu)
{
	CString csCode;
	csCode = ogPrivList.GetStat("DUTYROSTER_SHOWCLEARDRR");
	if (csCode == '0' || csCode == '-') 
	{
		return false;
	}

	// DRR mu� gef�llt sein
	if (popDrr == NULL || popDrr->Bsdu == 0)
		return false;
	
	// Es mu� eine aktive Stufe sein
	if (CString(popDrr->Ross ) != "A" || CString(popDrr->Rosl) == "1")
		return false;
	
	// Men�punkt 'Schicht/Abwesenheit XY l�schen' hinzuf�gen
	popMenu->AppendMenu(MF_STRING, MENUE_CLEAR_DRR, GetDrrMenuString(popDrr, MENUE_CLEAR_DRR));
	
	return true;
}

//****************************************************************************
// ShowChangeShift: pr�ft, ob im Kontextmenu der Menupunkt 
//	"Schichten tauschen" angezeigt werden muss und f�gt ihn ggf. hinzu
// R�ckgabe:	BOOL Men�punkt hinzugef�gt?
//****************************************************************************

bool DutyRoster_View::ShowChangeShift(DRRDATA *popDrr, CMenu *popMenu)
{
	CString csCode;
	csCode = ogPrivList.GetStat("DUTYROSTER_SHOWCHANGESHIFT");
	if (csCode == '0' || csCode == '-') 
	{
		return false;
	}

	// DRR mu� gef�llt sein
	if (popDrr == NULL || popDrr->Bsdu == 0)
		return false;
	
	// Es mu� eine aktive Stufe sein
	if (CString(popDrr->Ross) != "A" || CString(popDrr->Rosl) == "1")
		return false;
	
	// Nur in der Tagesliste-Stufe kann Schichttausch stattfinden
	if ((CString(popDrr->Rosl) != "3") && (CString(popDrr->Rosl) != "2"))
		return false;
	
	// Men�punkt 'Schicht tauschen' hinzuf�gen
	popMenu->AppendMenu(MF_STRING, MENUE_CHANGE_SHIFT, LoadStg(IDS_STRING84));
	
	return true;
}

//****************************************************************************
// Schichten tauschen
//****************************************************************************

void DutyRoster_View::OnMenuChangeShift() 
{
	CCS_TRY;
	
	
	int ilRow			= rmPlanInlineData.iRow;	
	int ilColumn		= rmPlanInlineData.iColumn;
	int ilRowOffset		= rmPlanInlineData.iRowOffset;
	int ilColumnOffset	= rmPlanInlineData.iColumnOffset;
	
	
	int ilStfRow = 0;
	CString olPType;
	int ilPTypeNr = 0;
	
	bool blTmp = pomIsTabViewer->GetElementAndPType(ilRow+ilRowOffset, ilStfRow, olPType, ilPTypeNr);
	if(!blTmp)	return ;
	
	if(((ilStfRow < omStfData.GetSize() && rmViewInfo.iGroup == 1)||
		(ilStfRow < omGroups.GetSize() && rmViewInfo.iGroup == 2)) && 
		(olPType == "1" || olPType == "2" || olPType == "3" || olPType == "4" || olPType == "L" || olPType == "5" || olPType == "U"))
	{
		// Mitarbeiter URNO erhalten
		long llStfUrno = 0;
		if (rmViewInfo.iGroup == 2)
		{
			llStfUrno = omGroups[ilStfRow].lStfUrno;
		}
		else
		{
			llStfUrno = omStfData[ilStfRow].Urno;
		}
		
		// Tag berechnen
		COleDateTime olDay = rmViewInfo.oShowDateFrom;
		olDay += COleDateTimeSpan (ilColumn+ilColumnOffset, 0, 0, 0);
		COleDateTime olDate (olDay.GetYear(), olDay.GetMonth(), olDay.GetDay(), 0, 0, 0);
		
		// Dialog zum Tauschen von Schichten aufrufen
		CChangeShiftDlg Dlg (this, olDate, llStfUrno, olPType);
		Dlg.DoModal();
		
		// Alle Felder deselektieren
		pomIsTabViewer->UnselectFields();
	}
	
	CCS_CATCH_ALL;
}

/**********************************************************************************************************************
Ausgabe aller Warnungen aus ogShiftCheck an Attention List
R�ckgabe:	keine
**********************************************************************************************************************/
void DutyRoster_View::ForwardWarnings()
{
	CCS_TRY;
	WriteLogFull("enter FillWorkTimeData()");
	CStringArray* polWarningsArray = ogShiftCheck.GetWarningList();
	if(!polWarningsArray) return;
	
	for(int ilCount=0; ilCount<polWarningsArray->GetSize(); ilCount++)
	{
		InsertAttention(polWarningsArray->GetAt(ilCount), RED);
	}
	
	// aufr�umen
	delete polWarningsArray;
	CCS_CATCH_ALL;
}



//****************************************************************************
//****************************************************************************
// Globale Funktionen zum Sortieren von Datens�tzen
//****************************************************************************
//****************************************************************************

//****************************************************************************
// CompareStfByName: vergleicht zwei Mitarbeiter nach Nachname und bei 
//	Gleichheit des Nachnamens nach Vorname.
// R�ckgabe:	<0	->	e1 < e2
//				==0	->	e1 == e2
//				>0	->	e1 > e2
//****************************************************************************

static int CompareStfByName( const STFDATA **e1, const STFDATA **e2)
{
	if (pcgView == 0)
		return 0;

	CString ole1 = CBasicData::GetFormatedEmployeeName(pcgView->rmViewInfo.iEName, &(**e1), "", "");
	CString ole2 = CBasicData::GetFormatedEmployeeName(pcgView->rmViewInfo.iEName, &(**e2), "", "");

	return ole1.Collate(ole2);
}

//****************************************************************************
// CompareTeaData: vergleicht zwei Fahrgemeinschaften nach Code.
// R�ckgabe:	<0	->	e1 < e2
//				==0	->	e1 == e2
//				>0	->	e1 > e2
//****************************************************************************

static int CompareTeaData( const TEADATA **e1, const TEADATA **e2)
{
	CString ole1,ole2;
	ole1 = (**e1).Fgmc;
	ole2 = (**e2).Fgmc;
	return (ole1.Collate(ole2));
}

//****************************************************************************
// CompareStfByFctConName: vergleicht zwei Mitarbeiter nach Stammfunktion/Vertragscode/Name
// R�ckgabe:	<0	->	e1 < e2
//				==0	->	e1 == e2
//				>0	->	e1 > e2
//****************************************************************************
static int CompareStfByFctConName( const STFDATA **e1, const STFDATA **e2)
{
	// Zugriff auf Dutyrosterview
	if (pcgView == 0)
		return 0;

	int ilCompareResult = 0;

	long Stf1 = (**e1).Urno;
	long Stf2 = (**e2).Urno;

	// Funktionscode und Vertragscode
	CString olFunctionCode1 =  ogSpfData.GetMainPfcBySurnWithTime(Stf1,pcgView->rmViewInfo.oSelectDate);
	CString olFunctionCode2 =  ogSpfData.GetMainPfcBySurnWithTime(Stf2,pcgView->rmViewInfo.oSelectDate);

	ilCompareResult = olFunctionCode1.Collate(olFunctionCode2);
	if (ilCompareResult)
	{
		return ilCompareResult;
	}

	// Gleich ! Vertrag �berpr�fen
	CString olActualCot1 = ogScoData.GetCotAndCWEHBySurnWithTime(Stf1,pcgView->rmViewInfo.oSelectDate);
	CString olActualCot2 = ogScoData.GetCotAndCWEHBySurnWithTime(Stf2,pcgView->rmViewInfo.oSelectDate);

	ilCompareResult = olActualCot1.Collate(olActualCot2);
	if (ilCompareResult)
	{
		return ilCompareResult;
	}

	// Gleich ! Namen �berpr�fen
	CString ole1;
	CString ole2;
	ole1 = CBasicData::GetFormatedEmployeeName(pcgView->rmViewInfo.iEName, &(**e1), "", "");
	ole2 = CBasicData::GetFormatedEmployeeName(pcgView->rmViewInfo.iEName, &(**e2), "", "");
	ilCompareResult = ole1.Collate(ole2);
	return ilCompareResult;
}

/********************************************************************
speichert aktuelle rmViewInfo.oStfUrnos in View
********************************************************************/
bool DutyRoster_View::SaveUrnoListInView(CStringArray& opList)
{
	CString olViewName = GetSelectedView();
	
	if (olViewName == "CB_ERR") 
	{
		TRACE ("Viewname konnte nicht ermittelt werden\n");
		return false;
	}
	
	if(pomViewer == 0)
		return false;
	
	// remove duplicated urnos
	CMapStringToPtr olMap;
	void *polKey;
	for (int ilC = opList.GetSize() - 1; ilC >= 0; ilC--)
	{
		if (olMap.Lookup(opList[ilC],polKey))
		{
			opList.RemoveAt(ilC);
		}
		else
		{
			olMap.SetAt(opList[ilC],NULL);
		}
	}
	
	// Ansicht im Viewer ausw�hlen
	pomViewer->SelectView(olViewName);
	pomViewer->SetFilter("DUTYVIEW_URNOLIST", opList);
	pomViewer->SafeDataToDB(olViewName);
	return true;
}

/********************************************************************
liest gespeicherte Urno-Liste aus aktuellen View
********************************************************************/
bool DutyRoster_View::GetUrnoListFromView(CStringArray& opList)
{
	CString olViewName = GetSelectedView();
	
	if (olViewName == "CB_ERR") 
	{
		TRACE ("Viewname konnte nicht ermittelt werden\n");
		return false;
	}
	if(pomViewer == 0) return false;
	
	pomViewer->GetFilter("DUTYVIEW_URNOLIST", opList);
	return true;
}




/****************************************************************************
FillGroupStruct
****************************************************************************/

void DutyRoster_View::FillGroupStruct(bool bpSortGroupsNew /*=true*/)
{
	CCS_TRY;
	WriteLogFull("enter FillGroupStruct()");
	pomIsTable->EndIPEditandDragDropAction();
	bmDragDopIsOK = false;
	
	//DRRDATA *prlDrr;
	DRGDATA *prlDrg;
	CString olPfcCode;
	CString olStfShnm;
	//CString olShiftCode;
	CString olWgpCode;
	CTime olShiftTime;
	long llStfUrno = 0;
	CTime olDay = COleDateTimeToCTime(rmViewInfo.oSelectDate);
	int ilDay = rmViewInfo.oSelectDate.GetDay()-1;
	if(ilDay<0) return;
	CString olSday = rmViewInfo.oSelectDate.Format("%Y%m%d");
	int ilStfSize = omStfData.GetSize();
	//WGPDATA *prlWgp;
	GROUPSTRUCT *polGroupStruct;
	
	bool blSortDayly = rmViewInfo.bSortDayly == true && rmViewInfo.lPlanGroupUrno != 0;
	
	//Create vacant Default Groups
	if(bpSortGroupsNew)
	{
		// Neue Gruppenreihenfolge ermitteln
		int ilPlanGroupSize = rmViewInfo.oPlanGroupCodes.GetSize();
		int ilWorkGroupSize = rmViewInfo.oWorkGroupCodes.GetSize();
		
		int ilBColor1 = 13;
		int ilBColor2 = 14;
		int ilBColor = ilBColor2;
		
		omGroups.DeleteAll();
		
		// keine Sortierung
		for(int ilWG=0; ilWG<ilWorkGroupSize; ilWG++)
		{
			if(ilBColor == ilBColor2)
				ilBColor = ilBColor1;
			else
				ilBColor = ilBColor2;
			//ilBColor = ilBColor1;
			CString olWGCode = rmViewInfo.oWorkGroupCodes[ilWG];
			for(int ilPG=0; ilPG<ilPlanGroupSize; ilPG++)
			{
				CString olPfcCode = rmViewInfo.oPlanGroupCodes[ilPG];
				int ilSize = atoi(rmViewInfo.oPlanGroupMax[ilPG]);
				if(ilSize <=0)
					ilSize = atoi(rmViewInfo.oPlanGroupMin[ilPG]);
				for(int i=0; i<ilSize; i++)
				{
					polGroupStruct = new GROUPSTRUCT;
					polGroupStruct->oWgpCode		= olWGCode;
					polGroupStruct->lStfUrno		= 0;
					polGroupStruct->oStfPeno		= "";
					polGroupStruct->oStfFinm		= "";
					polGroupStruct->oStfLanm		= "";
					polGroupStruct->oStfPerc		= "";
					polGroupStruct->oPfcCode		= olPfcCode;
					polGroupStruct->iColor1Nr		= ilBColor;
					polGroupStruct->oStfShnm		= "";
					omGroups.Add(polGroupStruct);
					//ilBColor = ilBColor2;
				}			
			}
		}
	}	
	
	///////////////////////////////////////////////////////
	//Insert staffs in groups or add staffs without groups
	// 1. Alle MA's aus omStfData mit DRD-Datenatz aus ogDrdData
	// 2. Rest der MA's aus omStfData �ber die Stammdaten
	
	CString olStfPeno;
	CString olStfFinm;
	CString olStfLanm;
	CString olStfPerc;
	CString olShiftCode;
	
	for (int ilStf = 0; ilStf < ilStfSize; ilStf++)
	{
		olShiftCode = "";
		olWgpCode = "";
		olPfcCode = "";
		olShiftTime = -1;
		llStfUrno = omStfData[ilStf].Urno;
		olStfPeno = omStfData[ilStf].Peno;
		olStfFinm = omStfData[ilStf].Finm;
		olStfLanm = omStfData[ilStf].Lanm;
		olStfPerc = omStfData[ilStf].Perc;
		olStfShnm = omStfData[ilStf].Shnm;
		// Die Tagesdaten aus DRG/Stammdaten
		if(blSortDayly)
		{
			// Parameter aus Tagesdaten, wenn vorhanden
			// DRRN ist immer =="1", d.h. es wird bei der Gruppensortierung nur 
			// die Schicht Nr.1 gezeigt und bearbeitet
			prlDrg = ogDrgData.GetDrgByKey(olSday, llStfUrno, "1");
			if(prlDrg != NULL)
			{
				olWgpCode	= prlDrg->Wgpc;
				olPfcCode	= prlDrg->Fctc;
			}
			else
			{
				// alle Parameter, die nicht in den Tagesdaten definiert sind, 
				// aus den Stammdaten ermitteln
				olWgpCode = ogSwgData.GetWgpBySurnWithTime(llStfUrno, rmViewInfo.oSelectDate);
				olPfcCode = ogSpfData.GetMainPfcBySurnWithTime(llStfUrno, rmViewInfo.oSelectDate);
			}
			
		}
		else
		{
			// Wenn sortieren �ber Zeitraum DRGs werden grunds�tzlich ignoriert (es ist eben
			// kein Tag selektiert)
			olWgpCode = ogSwgData.GetWgpBySurnWithTime(llStfUrno, rmViewInfo.oSelectDate);
			olPfcCode = ogSpfData.GetMainPfcBySurnWithTime(llStfUrno, rmViewInfo.oSelectDate);
		}
		////////////////////////////
		// Find position in omGroups
		bool blInGroup		 = false;
		bool blInGroupBefor  = false;
		bool blInThisStruct	 = false;
		int ilStructNr = -1;
		int ilTmpStructNr = -1;
		int ilGroupSize = omGroups.GetSize();
		GROUPSTRUCT *pGroupStruct;
		for (int ilStruct=0; (ilStruct < ilGroupSize && !blInThisStruct && !blInGroupBefor); ilStruct++)
		{
			pGroupStruct = &omGroups[ilStruct];
			if(olWgpCode == pGroupStruct->oWgpCode)
			{
				blInGroup = true;
				if (olPfcCode == pGroupStruct->oPfcCode)
				{
					if (pGroupStruct->lStfUrno == llStfUrno)
					{
						blInThisStruct = true;
						ilStructNr = ilStruct;	// �berschreiben an dieser Position !
					}
					else if (pGroupStruct->lStfUrno == 0)	// die Leerzeile / Funktion der Gruppe
					{
						ilStructNr = ilStruct;
					}
				}
			}
			else	// Gruppenwechsel	!
			{
				if(blInGroup)	// geh�rt der MA zur Gruppe ?
				{
					blInGroupBefor = true;
					if (ilStructNr == -1)			// keine Leerzeile gefunden ?
					{
						ilStructNr = ilStruct - 1;	// einf�gen hinter den letzten MA der Gruppe
					}
				}
			}
			
			ilTmpStructNr = ilStruct;
		}
		
		if (blInGroup && !blInThisStruct && !blInGroupBefor)	// geh�rt zu letzter Gruppe -> muss angeh�ngt werden
		{
			blInGroupBefor = true;
			if (ilStructNr == -1)			// keine Leerzeile gefunden ?
			{
				ilStructNr = ilTmpStructNr;	// einf�gen hinter den letzten MA der Gruppe
			}
		}
		
		if (blInThisStruct) // geh�rt auf diese Position in der Gruppe -> �berschreiben des MA
		{
			omGroups[ilStructNr].lStfUrno   = llStfUrno;
			omGroups[ilStructNr].oStfPeno   = olStfPeno;
			omGroups[ilStructNr].oStfFinm   = olStfFinm;
			omGroups[ilStructNr].oStfLanm   = olStfLanm;
			omGroups[ilStructNr].oStfPerc   = olStfPerc;
			omGroups[ilStructNr].oDrrn		= "1";
			omGroups[ilStructNr].oWgpCode   = olWgpCode;
			omGroups[ilStructNr].oPfcCode   = olPfcCode;
			omGroups[ilStructNr].oShiftTime = olShiftTime;
			//omGroups[ilStructNr].oShiftCode = olShiftCode;
			omGroups[ilStructNr].oStfShnm	= olStfShnm;
		}
		else if (blInGroupBefor) // in existierende Gruppe hinzuf�gen -> Anh�ngen an diese Gruppe
		{
			polGroupStruct = new GROUPSTRUCT;
			polGroupStruct->lStfUrno	= llStfUrno;
			polGroupStruct->oStfPeno	= olStfPeno;
			polGroupStruct->oStfFinm	= olStfFinm;
			polGroupStruct->oStfLanm	= olStfLanm;
			polGroupStruct->oStfPerc	= olStfPerc;
			polGroupStruct->oDrrn		= "1";
			polGroupStruct->oWgpCode	= olWgpCode;
			polGroupStruct->oPfcCode	= olPfcCode;
			polGroupStruct->oShiftTime  = olShiftTime;
			//polGroupStruct->oShiftCode  = olShiftCode;
			polGroupStruct->oStfShnm	= olStfShnm;
			
			if (ilStructNr < omGroups.GetSize() && ilStructNr >= 0)
			{
				if (omGroups[ilStructNr].oPfcCode == olPfcCode && omGroups[ilStructNr].lStfUrno == 0)	// �berschreiben der Leerzeile 
				{
					omGroups[ilStructNr].lStfUrno   = llStfUrno;
					omGroups[ilStructNr].oStfPeno   = olStfPeno;
					omGroups[ilStructNr].oStfFinm   = olStfFinm;
					omGroups[ilStructNr].oStfLanm   = olStfLanm;
					omGroups[ilStructNr].oStfPerc   = olStfPerc;
					omGroups[ilStructNr].oDrrn		= "1";
					omGroups[ilStructNr].oWgpCode   = olWgpCode;
					omGroups[ilStructNr].oPfcCode   = olPfcCode;
					omGroups[ilStructNr].oShiftTime = olShiftTime;
					//omGroups[ilStructNr].oShiftCode = olShiftCode;
					omGroups[ilStructNr].oStfShnm	= olStfShnm;
					delete polGroupStruct;
				}
				else	// einf�gen eines neuen MA !
				{
					polGroupStruct->iColor1Nr	= omGroups[ilStructNr].iColor1Nr;
					omGroups.InsertAt(ilStructNr, polGroupStruct);	// einf�gen hei�t, vorhandenen Eintrag nach oben schieben und neuen Eintrag dort einf�gen !
				}
			}
			else
			{
				polGroupStruct->iColor1Nr	= -1;
				omGroups.Add(polGroupStruct);
			}
		}
		else // unten Anh�ngen
		{
			polGroupStruct = new GROUPSTRUCT;
			polGroupStruct->lStfUrno		= llStfUrno;
			polGroupStruct->oStfPeno		= olStfPeno;
			polGroupStruct->oStfFinm		= olStfFinm;
			polGroupStruct->oStfLanm		= olStfLanm;
			polGroupStruct->oStfPerc		= olStfPerc;
			polGroupStruct->oDrrn			= "1";
			polGroupStruct->oWgpCode		= olWgpCode;
			polGroupStruct->oPfcCode		= olPfcCode;
			polGroupStruct->iColor1Nr		= -1;
			polGroupStruct->oShiftTime      = olShiftTime;
			//polGroupStruct->oShiftCode		= olShiftCode;
			polGroupStruct->oStfShnm		= olStfShnm;
			omGroups.Add(polGroupStruct);
		}
	}
	CCS_CATCH_ALL;
}

/****************************************************************************
Alle Aufrufe durch die BC Handler gehen hier rein und werden weiter verteilt
****************************************************************************/

void ProcessDutyRoster_ViewCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	CCS_TRY;
	DutyRoster_View *prlDutyRoster_View = (DutyRoster_View *)vpInstance;

	switch(ipDDXType)
	{
		default:
			break;
		case DRR_CHANGE:
		case DRR_NEW:
			prlDutyRoster_View->ProcessDrrChange((DRRDATA *)vpDataPointer, ipDDXType);
			break;

		case DRG_CHANGE:
		case DRG_NEW:
		case DRG_DELETE:
			prlDutyRoster_View->ProcessDrgChange((DRGDATA *)vpDataPointer, ipDDXType);
			break;
	}
	CCS_CATCH_ALL;
}

//****************************************************************************
// ProcessDrrChange: bearbeitet den Broadcast DRR_NEW, DRR_CHANGE, DRR_DELETE
// R�ckgabe:	keine
//****************************************************************************

void DutyRoster_View::ProcessDrrChange(DRRDATA *prpDrr, int ipDDXType)
{
	return;
}

/****************************************************************************
ProcessDrgChange: bearbeitet die Broadcasts DRG_NEW, DRG_CHANGE und DRG_DELETE:
F�r die STFU & SDAY & DRRN pr�fen, ob WGPC & FCTC neu sind. Wenn ja, in SWG und SPF
speichern und betroffene Views neu zeichnen.
FillGroupStruct() macht diese Arbeit am besten.
R�ckgabe:	keine
****************************************************************************/

void DutyRoster_View::ProcessDrgChange(DRGDATA *prpDrg, int ipDDXType)
{
	FillGroupStruct();
	pomSolidTabViewer->ChangeView(false,false);
	pomIsTabViewer->ChangeView(false,false);
	pomStatTabViewer->ChangeView(false,false);
	if(pomDutyDragDropDlg != NULL)
	{
		pomDutyDragDropDlg->ChangeView(false,false);
	}
}

/***************************************************************************
rmViewInfo.oStfUrnos bekommt zus�tzlich noch alle MAs, die passende Funktion oder Organisation haben
****************************************************************************/

void DutyRoster_View::GetStaffByFunctionsOrGroups(CString& opPfcFctcs, CStringArray& opStfUrnos, CString& opPfcUrnos) 
{
	CCS_TRY;
	
	POSITION rlPos;					// f�r Array-Loops
	void  *prlVoid = NULL;			// pointer f�r Lookups in PtrMapArrays
	CString olTmp;
	int ilSize, ilNum;
	
	/////////// Funktions-Codes der anzuzeigenden Funktionen ermitteln /////////////////
	
	CMapStringToPtr olPfcCodeMap;	// Bezeichner der anzuzeigenden Funktionen
	GetPfcMap(olPfcCodeMap, opPfcUrnos);
	
	COleDateTime olStart, olEnd;
	
	bmLastSortDayly = rmViewInfo.bSortDayly;
	
	// Ausgew�hlte Start- und Enddatum setzen
	if(rmViewInfo.bSortDayly)
	{
		olStart = olEnd = rmViewInfo.oSelectDate;
	}
	else
	{
		olStart = rmViewInfo.oDateFrom;
		olEnd = rmViewInfo.oDateTo;
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 1. Schritt: alle Mitarbeiter ermitteln, die nach Stammdaten  im ausgew�hlten Zeitraum
	// die passende Funktion haben
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	bool blGetAllMAWithoutFunc = opPfcFctcs.Find("NO_PFCCODE") != -1;
	
	CMapPtrToPtr olStfUrnoMap;	// anzuzeigende Mitarbeiter
	// alle Mitarbeiter mit der passenden Funktion im angegebenen Zeitraum aus Stammdaten ermitteln
	ogSpfData.GetStfuByPfcListWithTime(olPfcCodeMap, olStart, olEnd, olStfUrnoMap, rmViewInfo.bSelectAllFunc, blGetAllMAWithoutFunc);
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 2. Schritt: alle Mitarbeiter ermitteln, die einen DRG mit der
	// passenden Funktion oder Gruppe am zu evaluierenden Tag haben.
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// Planungsgruppen evaluieren
	CMapStringToPtr olWgpCodeMap;	// Arbeitsgruppen-Bezeichner (Codes)
	// Arbeitsgruppen-Bezeichner-Array initialisieren mit den zu evaluierenden
	// Arbeitsgruppen, die in der Ansicht gespeichert sind
	for(ilNum=0; ilNum<rmViewInfo.oWorkGroupCodes.GetSize(); ilNum++)
	{
		olWgpCodeMap.SetAt(rmViewInfo.oWorkGroupCodes[ilNum], NULL);
	}
	
	CCSPtrArray<DRGDATA> olDrgData;
	
	// ilDrgSize - Anzahl der gefundenen Datens�tze
	int ilDrgSize = ogDrgData.GetDrgArrayByTime(olStart, olEnd,"STFU,SDAY", &olDrgData);
	
	// DRGs korrigieren die Stammdaten
	// Wir m�ssen zwei Abweichungen von Stammdaten ber�cksichtigen:
	// 1. Wenn ein MA nicht in der Liste ist UND
	// an allen ausgew�hlten Tagen keine der gesuchten Funktionen hat ODER geh�rt zu keiner der
	// anzuzeigenden Gruppen, mu� er aus der Liste entfernt werden
	// Die #1. kommt dann, wenn ein MA in seinen Stammdaten die gesuchte Funktion oder Gruppe hat, 
	// so kommt er in die Liste rein, doch man hat den MA aus der Gruppe und Funktion zumindes f�r 
	// diese Zeit versetzt, - so geh�rt er nicht mehr in die Ansicht
	// 2. Wenn ein MA bereits in der Liste ist UND an einem der ausgew�hlten Tage eine der 
	// gesuchten Funktionen ODER gesuchten Gruppen hat, mu� er eingef�gt werden
	
	// olDrgData ist bereits nach Stfu (MA-Urno) & Sday sortiert (sieh oben).
	// Daf�r suchen wir alle DRGs durch, und f�gen alle neue Mitarbeiter der Liste ein und 
	// entfernen alle MAs, die an keinem der Tage eine der richtigen Funktionen haben
	long llStfu;
	for(int ilDrgCount=0; ilDrgCount<ilDrgSize;)
	{
		llStfu = olDrgData[ilDrgCount].Stfu;
		
		// Vorab: hat er die richtige Funktion oder die richtige Gruppe?
		BOOL bTrueFuncOrGroup = 
			olPfcCodeMap.Lookup(olDrgData[ilDrgCount].Fctc,(void *&)prlVoid) ||
			olWgpCodeMap.Lookup(olDrgData[ilDrgCount].Wgpc,(void *&)prlVoid);
		
		
		// Ist dieser MA in der Liste?
		if (!(olStfUrnoMap.Lookup((void*)llStfu,(void *&)prlVoid)))
		{
			// nein, Fall 1.
			// hat er die richtige Funktion oder die richtige Gruppe?
			if(bTrueFuncOrGroup)
			{
				// ja, mu� eingef�gt werden
				_ASSERT(prlVoid == NULL);
				olStfUrnoMap.SetAt((void*)llStfu,NULL);
				
				// wir 'bl�ttern' alle weiteren DRGs dieses MAs durch
				for(; ilDrgCount<ilDrgSize; ilDrgCount++)
				{	
					if(llStfu != olDrgData[ilDrgCount].Stfu)
						break;
				}
			}
			else
			{
				ilDrgCount++;	// Inkrement
			}
		}
		else
		{
			// ja, MA ist in der Liste, Fall 2.
			// hat er die richtige Funktion oder die richtige Gruppe?
			if(bTrueFuncOrGroup)
			{
				// ja, bleibt da
				// wir 'bl�ttern' alle weiteren DRGs dieses MAs durch
				for(; ilDrgCount<ilDrgSize; ilDrgCount++)
				{	
					if(llStfu != olDrgData[ilDrgCount].Stfu)
						break;
				}
			}
			else
			{
				// nein
				bool bDeleteMAfromList = true;		// Voreinstellung
				// haben wir mehr als ein Tag?
				if(olStart != olEnd)
				{
					// ja, wenn er an einem der �brigen Tage die richtige Funktion oder Gruppe hat, bleibt er da,
					// sonst mu� gel�scht werden
					COleDateTimeSpan const oneDay(1,0,0,0);		// Konstante: ein Tag f�r Iteration
					
					// F�r jeden Tag nacheinanderfolgende DRGs pr�fen
					// olDrgData ist bereits nach Stfu (MA-Urno) & Sday sortiert (sieh oben).
					ilDrgCount++;
					for(COleDateTime olDateCount = olStart+oneDay; olDateCount <= olEnd && ilDrgCount<ilDrgSize; ilDrgCount++, olDateCount+=oneDay)
					{
						if(llStfu == olDrgData[ilDrgCount].Stfu)
						{
							// es ist immer noch der gleiche Mensch
							// stimmen nach der Iteration die Tagangaben �berein?
							if(olDrgData[ilDrgCount].Sday == olDateCount.Format("%Y%m%d"))
							{
								// diese DRG ist f�r aktuellen MA & aktuellen Tag
								// stimmt die Funktion?
								if(	olPfcCodeMap.Lookup(olDrgData[ilDrgCount].Fctc,(void *&)prlVoid) ||
									olWgpCodeMap.Lookup(olDrgData[ilDrgCount].Wgpc,(void *&)prlVoid))
								{
									// ja, alles stimmt - dieser Mitarbeiter bleibt in der Liste,
									// weil er an mindensens einem der Tage eine passende Funktion oder Gruppe hat
									bDeleteMAfromList = false;
									
									// alle DRGs dieses MAs durchlaufen, wir wissen schon genug �ber ihn
									for(; ilDrgCount<ilDrgSize; ilDrgCount++)
									{	
										if(llStfu != olDrgData[ilDrgCount].Stfu)
											break;
									}
									break;
								}
							}
						}
						else
						{
							// das ist schon ein anderer Mensch
							// und da wir noch nicht alle Tage gecheckt haben, hei�t es,
							// da� der MA, den wir pr�fen, nicht an allen Tagen 'falsche'
							// Funktion hat, so mu� er bleiben
							bDeleteMAfromList = false;
							break;
						}
					}
					// wenn hier immer noch bDeleteMAfromList == true, hei�t es, da� an allen Tagen dem MA durch 
					// entsprechende DRGs nicht passende Funktionen und Gruppen zugewiesen sind, er soll gel�scht werden
				}
				else
				{
					// wir haben nur ein Tag, der MA fliegt raus
					ilDrgCount++;	// Iteration nicht vergessen
				}
				
				if(bDeleteMAfromList)
				{
					// die Date-Loop ist durchgelaufen und an keinem Tag war eine passende Funktion gefunden,
					// so mu� dieser MA gel�scht werden - gelitten
					olStfUrnoMap.RemoveKey((void*)llStfu);
				}
			}
		}
	}

	// alle ermittelten Mitarbeiter-Urnos in die Liste der Ansicht speichern
	CString olStfu;
	CStringArray olTmpStfUrnos;
	for(rlPos = olStfUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		olStfUrnoMap.GetNextAssoc(rlPos, (void*&)llStfu, (void *&)prlVoid);
		olStfu.Format("%ld",llStfu);
		olTmpStfUrnos.Add(olStfu);  //Alle MA's mit der richtigen Funktion und evtl. Gruppe
	}

	// URNOs aus opStfUrnos kopieren, falls diese schon Daten enth�lt
	// um die Schnittmenge zu bilden
	CMapStringToPtr olTmpStfUrnoMap;
	ilSize =  opStfUrnos.GetSize();
	for(ilNum = 0; ilNum < ilSize; ilNum++)
	{	
#ifdef _DEBUG
		
		void  *prlVoid = NULL;
		if(olTmpStfUrnoMap.Lookup(opStfUrnos[ilNum],prlVoid))
		{
			CString olStr = opStfUrnos[ilNum];
			if(olStr.IsEmpty())
				TRACE("olTmpStfUrnoMap found: %d empty\n", ilNum);
			else
				TRACE("olTmpStfUrnoMap found: %d %s\n", ilNum, olStr);
		}
#endif _DEBUG
		olTmpStfUrnoMap.SetAt(opStfUrnos[ilNum],NULL);
	}
	opStfUrnos.RemoveAll();

	ilSize =  olTmpStfUrnos.GetSize();

	for(ilNum = 0; ilNum < ilSize; ilNum++)
	{	
		if (olTmpStfUrnoMap.Lookup(olTmpStfUrnos[ilNum],(void *&)prlVoid) == TRUE)
			opStfUrnos.Add(olTmpStfUrnos[ilNum]);
	}
	// aufr�umen
	olDrgData.DeleteAll();
	olPfcCodeMap.RemoveAll();
	olStfUrnoMap.RemoveAll();
	olWgpCodeMap.RemoveAll();
	olTmpStfUrnoMap.RemoveAll();
	olTmpStfUrnos.RemoveAll();
	
	CCS_CATCH_ALL;
}

/********************************************************************
Funktions-Codes der anzuzeigenden Funktionen ermitteln
********************************************************************/
void DutyRoster_View::GetPfcMap(CMapStringToPtr &opPfcCodeMap, CString& opPfcUrnos)
{
	CString olTmpUrno;
	// Setze PFC-Urno-List um in PFC-Code-und Stat-Liste
	for(int ilNum = 0; ilNum < ogPfcData.omData.GetSize(); ilNum++)
	{	
		// Funktions-Urno lesen und in Trennzeichen verpacken
		olTmpUrno.Format(",%ld,",ogPfcData.omData[ilNum].Urno);
		// Funktions-Urno in der Liste der Ansichts-Funktions-Urnos suchen
		if(	opPfcUrnos.Find(olTmpUrno) != -1)
		{
			// wenn gefunden -> Funktion-Code in Liste speichern
			// neuen Eintrag mappen
			opPfcCodeMap.SetAt(ogPfcData.omData[ilNum].Fctc,NULL);
		}
	}
	if(	opPfcUrnos.Find("NO_PFCCODE") != -1)
	{
		// wenn gefunden -> in Liste speichern, da� alle ohne Funktion-Code angezeigt werden sollen
		// neuen Eintrag mappen
		opPfcCodeMap.SetAt("NO_PFCCODE",NULL);
	}
}

/**********************************************************
create default views
**********************************************************/
void DutyRoster_View::InitDefaultView()
{
	CViewer olViewer;
	CStringArray olPossibleFilters;
	CString olLastView;
	
	//DutyRoster Viewer -----------------------------
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("DUTYVIEW1");
	olPossibleFilters.Add("DUTYVIEW2");
	olPossibleFilters.Add("DUTYVIEW3");
	olPossibleFilters.Add("DUTYVIEW4");
	olPossibleFilters.Add("DUTYVIEW5");
	olPossibleFilters.Add("DUTYVIEW_URNOLIST");
	olViewer.SetViewerKey("ID_SHEET_DUTY_VIEW");
	olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");
	if (olLastView != "") olViewer.SelectView(olLastView);	// restore the previously selected view
	//-------------------------------------------------
	
	olPossibleFilters.RemoveAll();
}



void DutyRoster_View::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	
	CFormView::OnLButtonDblClk(nFlags, point);
}

//
// sends a complete STF.URNOs of a view to scbhdl for prefetching
//
void DutyRoster_View::SendViewStfuToScbhdl(VIEWINFO *ropOldView, VIEWINFO *ropNewView)
{
	CCS_TRY;
	CString olCustomer = ogCCSParam.GetParamValue(ogAppl,"ID_PROD_CUSTOMER");

	// putting old STF-URNOs into Map
	CMapStringToPtr olMap;

	for (int i = 0; i < ropOldView->oStfUrnos.GetSize(); i++)
	{
		olMap.SetAt(ropOldView->oStfUrnos[i], NULL);
	}

	void *polKey;
	CString olADDSTF_Urnos;											// the STF-URNOs to add
	CString olDELSTF_Urnos;											// the STF-URNOs to remove
	CString olADDSTF_Year = ropNewView->oDateFrom.Format("%Y");		// new year
	CString olDELSTF_Year = ropOldView->oDateFrom.Format("%Y");		// old year

	if (olADDSTF_Year == olDELSTF_Year)
	{
		//adding and deleting for the same year
		for (i = 0; i < ropNewView->oStfUrnos.GetSize(); i++)
		{
			if (olMap.Lookup(ropNewView->oStfUrnos[i], (void *&)polKey))
			{
				olMap.RemoveKey (ropNewView->oStfUrnos[i].GetBuffer(0));
			}
			else
			{
				olADDSTF_Urnos += ropNewView->oStfUrnos[i] + ',';
			}
		}
		CString		olStfu;
		POSITION	rlPos;
		for (rlPos = olMap.GetStartPosition(); rlPos != NULL; )
		{
			olMap.GetNextAssoc (rlPos, olStfu, (void *&)polKey);
			olDELSTF_Urnos += olStfu + ',';
		}
	}
	else
	{
		//delete registration all employees of the old year
		//add employees for the new year
		CString		olStfu;
		POSITION	rlPos;
		for (rlPos = olMap.GetStartPosition(); rlPos != NULL; )
		{
			olMap.GetNextAssoc (rlPos, olStfu, (void *&)polKey);
			olDELSTF_Urnos += olStfu + ',';
		}
		for (i = 0; i < ropNewView->oStfUrnos.GetSize(); i++)
		{
			olStfu = ropNewView->oStfUrnos[i];
			olADDSTF_Urnos += ropNewView->oStfUrnos[i] + ',';
		}
	}

	// send the events
	SendViewStfuToScbhdlCommand(olDELSTF_Urnos, CString ("DELSTF"), olDELSTF_Year);
	SendViewStfuToScbhdlCommand(olADDSTF_Urnos, CString ("ADDSTF"), olADDSTF_Year);

	CCS_CATCH_ALL;
}


void DutyRoster_View::SendViewStfuToScbhdlCommand(CString olStfuString, CString olCommand, CString olYear)
{
	CCS_TRY;
	static char cBlank[2]	= " ";			// for non-important information
	static char cUser[12];					// the name of the actual user
	static char cAction[6];					// the command for the scbhdl
	static char cTws[33];					// containing 'ADDSTF' or 'DELSTF' (for prefetching ACC-data)
	static char cTwe[33]	= "Rostering";	// name of the application
	strcpy(cUser,pcgUser);
	strcpy(cTws,olCommand.GetBuffer(0));
	strcpy(cAction,"XBS2");
	
	// send the events
	if (olYear.GetLength () < 5)
	{
		if (olStfuString.GetLength () > 0)
		{
			CString olTmp = olStfuString;
			CStringArray olArr;
			CedaData::ExtractTextLineFast (olArr, olTmp.GetBuffer(0), ",");
			
			//if there are less than ARR_MAXSELECTLEN URNOs, we can send them at once
			if (olArr.GetSize() <= ARR_MAXSELECTLEN)
			{
				::CallCeda	(cBlank, cUser, cBlank, cAction, cBlank, cBlank, cTws, cTwe, olYear.GetBuffer(0), cBlank, olStfuString.GetBuffer(512), "RETURN");
			}
			else //split it into 'ARR_MAXSELECTLEN URNO'-packets
			{
				olTmp = "";
				for (int i = 0; i < olArr.GetSize(); i++)
				{
					olTmp += olArr[i] + ',';
					if ((i % ARR_MAXSELECTLEN == 0) && (i > 0))
					{
						::CallCeda	(cBlank, cUser, cBlank, cAction, cBlank, cBlank, cTws, cTwe, olYear.GetBuffer(0), cBlank, olTmp.GetBuffer(512), "RETURN");
						olTmp = "";
					}
				}
				if (olTmp.GetLength() > 0)
				{
					::CallCeda	(cBlank, cUser, cBlank, cAction, cBlank, cBlank, cTws, cTwe, olYear.GetBuffer(0), cBlank, olTmp.GetBuffer(512), "RETURN");
				}
			}
		}
	}
	CCS_CATCH_ALL;
}

void DutyRoster_View::SendCommand(CString olCommand, CString olAction)
{
	CCS_TRY;
	static char cBlank[2]	= " ";			// for non-important information
	static char cUser[12];					// the name of the actual user
	static char cAction[128];				// the command for the scbhdl (e.g. "XBS2")
	static char cTws[33];					// containing 'real' command (e.g. "REFILL")
	static char cTwe[33]	= "Rostering";	// name of the application
	strcpy(cUser,pcgUser);
	strcpy(cTws,olCommand.GetBuffer(0));
	strcpy(cAction,olAction.GetBuffer(0));
	::CallCeda	(cBlank, cUser, cBlank, cAction, cBlank, cBlank, cTws, cTwe, cBlank, cBlank, cBlank, "RETURN");
	
	CCS_CATCH_ALL;
}

//****************************************************************************
// OnMenuPrintDailyRoster: Drucken der Tagesdienstplanung
//****************************************************************************

void DutyRoster_View::OnMenuPrintDailyRoster() 
{
	CCS_TRY;
	
	// Spalte ermitteln, wird zur Betsimmung des Tages ben�tigt
	int ilColumn		= rmPlanInlineData.iColumn;
	int ilColumnOffset	= rmPlanInlineData.iColumnOffset;
	
	// ausgew�hlten Tag ermitteln
	COleDateTime olDay = rmViewInfo.oShowDateFrom;
	olDay += COleDateTimeSpan(ilColumn + ilColumnOffset, 0, 0, 0);
	
	// show the dialog
	DutyPrintDlg olDutyPrintDlg(this, olDay);
	olDutyPrintDlg.DoModal();
	CCS_CATCH_ALL;
}

void DutyRoster_View::OnBExcel() 
{
	ExcelExportDlg olDlg(this);
	olDlg.DoModal();
}
