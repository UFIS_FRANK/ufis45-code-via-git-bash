// InfoDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CInfoDlg 


CInfoDlg::CInfoDlg(CWnd* pParent, CStringArray* popStringArray)
	: CDialog(CInfoDlg::IDD, pParent)
{
//	omStringArray = opStringArray;
	//{{AFX_DATA_INIT(CInfoDlg)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT


	pomStringArray = popStringArray;
}


void CInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CInfoDlg)
		// HINWEIS: Der Klassen-Assistent f�gt hier DDX- und DDV-Aufrufe ein
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CInfoDlg, CDialog)
	//{{AFX_MSG_MAP(CInfoDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CInfoDlg 

BOOL CInfoDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetWindowText(LoadStg(IDS_INFO));
	SetDlgItemText(IDC_ERSTELLT_AM,LoadStg(IDS_STRING1823));
	SetDlgItemText(IDC_GEAENDERT_AM,LoadStg(IDS_STRING433));
	SetDlgItemText(IDC_ERSTELLT_VON,LoadStg(IDS_STRING1824));
	SetDlgItemText(IDC_GEAENDERT_VON,LoadStg(IDS_STRING431));

	// Es mussen genau 6 Elemente sein 
	if (pomStringArray->GetSize() == 6)
	{
		SetDlgItemText(IDC_STATIC_1,pomStringArray->GetAt(0));
		SetDlgItemText(IDC_STATIC_2,pomStringArray->GetAt(1));
		SetDlgItemText(IDC_STATIC_4,pomStringArray->GetAt(2));
		SetDlgItemText(IDC_STATIC_5,pomStringArray->GetAt(3));
		SetDlgItemText(IDC_STATIC_3,pomStringArray->GetAt(4));
		SetDlgItemText(IDC_STATIC_6,pomStringArray->GetAt(5));
	}

	return TRUE;  
}
