#if !defined(AFX_TAGESLISTEDLG_H__EC839161_9636_11D1_940B_0000B44A2C90__INCLUDED_)
#define AFX_TAGESLISTEDLG_H__EC839161_9636_11D1_940B_0000B44A2C90__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// TageslisteDlg.h : header file
//
#include <stdafx.h>
#include <resource.h>
#include <CedaDrrData.h>
#include <CedaDrdData.h>
#include <CedaDraData.h>
#include <CedaDrsData.h>
#include <Groups.h>


///////////////////////////////////////////////////////////////////////////// 
// CTageslisteDlg dialog

#define  DEVCOLCOUNT 4
#define  ABSCOLCOUNT 4

class CGridControl;

class CTageslisteDlg : public CDialog
{ 
// Construction
public:
	CTageslisteDlg(bool bpReadOnly,DRRDATA *popDsr, CCSPtrArray<STFDATA>* popStfData,CWnd* pParent = NULL);   // standard constructor
	~CTageslisteDlg();

// Dialog Data
	//{{AFX_DATA(CTageslisteDlg)
	enum { IDD = IDD_DRR_DLG };
	CListCtrl	m_Perc_Listbox;
	CComboBox	m_CB_ShiftNumber;
	CComboBox	m_C_ActualShiftFunc;
	CCSEdit		m_Bufu;
	CCSEdit		m_Scod;
	CCSEdit		m_Avfr_Date;
	CCSEdit		m_Avfr_Time;
	CCSEdit		m_Avto_Date;
	CCSEdit		m_Avto_Time;
	CCSEdit		m_Sbfr_Time;
	CCSEdit		m_Sbto_Time;
	CCSEdit		m_Sblu;
	CCSEdit		m_Remark;
	CCSEdit		m_E_ZCode1;
	CCSEdit		m_E_ZCode3;
	CCSEdit		m_E_ZCode4;
	CButton		m_B_Info;
	CButton		m_C_SAP;
	CComboBox   m_C_SUB1SUB2;
	CComboBox	m_C_Group;
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTageslisteDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
public:
	// BC Bearbeitung f�r DRD
	void ProcessDrdChange(DRDDATA *prpDrd,int ipDDXType);
	// BC Bearbeitung f�r DRA
	void ProcessDraChange(DRADATA *prpDra,int ipDDXType);
	// BC Bearbeitung f�r DRS
	void ProcessDrsChange(DRSDATA *prpDrs,int ipDDXType);
	// BC Bearbeitung f�r DRG
	void ProcessDrgChange(DRGDATA *prpDrg,int ipDDXType);


protected:
	/****************************************************************
	Pr�fft ob in der ersten ComboBox leerer String selektiert ist,
	wenn ja, selektiert leeren String in der zweiten,
	wenn in der ersten ComboBox ein nicht leerer String selektiert ist,
	pr�fft, ob die zweite leer ist, wenn ja - selektiert n�chsten nichtleeren 
	Eintrag in der ListBox und mach DropDown auf.
	****************************************************************/
	void CheckEqualSelChangeInComboBoxes(CComboBox& opBox1, CComboBox& opBox2);

//	void OnDblclkEinsatzLb();
	// Generated message map functions
	//{{AFX_MSG(CTageslisteDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnSetShiftIs();
	afx_msg void On_Insert_Dev();
	afx_msg void On_Delete_Dev();
	afx_msg void On_Edit_Dev();
	afx_msg void On_Absence_Delete();
	afx_msg void On_Absence_Edit();
	afx_msg void On_Absence_Insert();
	afx_msg void OnButtoninfo();
	afx_msg void OnInsertDoubleTour();
	afx_msg void OnCancelDoubleTour();
	afx_msg void OnSelchangeShiftCombobox();
	afx_msg void OnUpdateEZcode1();
	afx_msg void OnUpdateEZcode3();
	afx_msg void OnUpdateEZcode4();
	afx_msg LONG OnGridLButton(WPARAM wParam, LPARAM lParam);
	afx_msg void OnSelchangeSub1Sub2();
	//}}AFX_MSG
	LONG OnKillfocus(UINT lParam, LONG wParam);
	long OnEditChanged(UINT lParam, LONG wParam);
	DECLARE_MESSAGE_MAP()

private:

// Data
	// Das Grid
	CGridControl *pomDevGrid;
	CGridControl *pomAbsenceGrid;
	// Zeiger auf DRR Datenstruktur
	DRRDATA *pomDrr;
	// Liste mit allen DRR mit Schl�sselwerten wie <pomDrr>
	CCSPtrArray<DRRDATA> omDrrList;
	// Map mit allen relevanten DRDs
	CMapPtrToPtr omRefDrdMap;
	// Map mit allen tempor�ren DRD`s
	CMapPtrToPtr omTempDrdMap;

	// Map mit allen relevanten DRDs
	CMapPtrToPtr omRefDraMap;
	// Map mit allen relevanten DRDs
	CMapPtrToPtr omTempDraMap;

	// Readonly Flag
	bool bmReadOnly;
	// Schicht Code vor der �nderung
	long lmOldShiftUrno;
	// Funktion vor der �nderung
	CString omOldFctc;
	// M�ssen die Drd Daten gespeichert werden
	bool bmDrdIsDirty;
	// M�ssen die Dra Daten gespeichert werden
	bool bmDraIsDirty;
	// Anzeigenlogik synchronisieren
	bool bmSetDrsActive;

	bool bmChangedSub1Sub2;

	VIEWINFO* pomViewInfo;	// aus DutyRoster_View

	// ein von CODE_IS_ODA_FREE, CODE_IS_BSD, CODE_IS_ODA_REGULARFREE - Typ des Drr->Scode
	int imScodType; 

	// ein von CODE_UNDEFINED, CODE_IS_ODA_TIMEBASED oder CODE_IS_ODA_NOT_TIMEBASED
	int imTbsdType;
	bool bmWork;		// wenn "1" - diese Abwesenheit soll wie eine Schicht behandelt werden

	// Zeiger auf Array mit allen Mitarbeitern
	CCSPtrArray<STFDATA>* pomStfData;
		
	// Urno der ausgew�hlten Schicht
	long lmShiftUrno;
	// Aktuelle Funktion der ausgew�hlten Schicht aus DRR
	CString omFctc;
	// Drr Urno
	long lmUrno;
	CString omErrorTxt;
	CString	omReadErrTxt;
	CString	omInsertErrTxt;
	CString	omUpdateErrTxt;
	CString	omDeleteErrTxt;
	CString omHinweis;	
	CString omFehler;	
	CString	omCaption;
	CBrush omBrush;

// functions
	// Grid initialisieren
	void InitDevGrid ();
	void InitDevColWidths();
	void FillDevGrid();
	bool SortDevGrid(int ipRow);

	// Grid initialisieren
	void InitAbsenceGrid ();
	void InitAbsenceColWidths();
	void FillAbsenceGrid();
	bool SortAbsenceGrid(int ipRow);
	// DRR-Nummer ComboBox initialisieren
	void InitDrrnComboBox();
	void InitFctcComboBox();
	void InitSub1Sub2ComboBox();

	// Arbeitsgruppenliste und Funktionenliste f�llen
	// DRG pr�fen, wenn vorhanden, die Arbeitsgruppe und Funktion selektieren, sonst leer
	void FillGroupAndFunctionLists();

	// Eingabe auf G�ltigkeit pr�fen.
	bool IsInputValid();

	// Statische Elemente setzen
	void SetStatic();
	// Editfelder setzen
	void SetEdit();
	// Buttontexte setzen
	void SetButtonText();
	// Funktion und deren Textfeld initialisieren
	void SetFctcAndFields();
	// Zeitfelder f�r die Schichten setzen
	//void SetTimeFieldsByBsd(long lpUrno, int ipShift);
	// Initialisiert Schichtcode und Zeitangaben aus BSD oder ODA, abh�ngig vom ipShift
	void SetShiftFields(int ipShift, CString opScode, bool bpDurationOnly=false);
	// F�llt 7 CString-s aus popText mit Datum und Zeitangaben
	void InitTimesByBsd(CString* popText, BSDDATA* popBsd);
	void InitTimesByOda(CString* popText, CString opScode);
	void InitTimesByDrr(CString* popText, DRRDATA* popDrr);
	void InitTimesBySpr(CString* popText);
	// Controls auf grau setzen
	void ShowControls(bool bpShow);
	// Controls selektiv auf grau setzen abh�ngig vom imScodType
	void ShowControlsSelective();
	// Editfeld enablen/disablen, bkColor auf yellow/gray
	void SetReadOnlyColored(int ipItem, bool bpReadOnly=true);
	// Alle Items die sich auf DRSdaten 
	void SetDrsItems();
	// beim BC-Handler an- oder abmelden
	void Register(bool bpRegister=true);
	// Sichert die tempor�r gehaltenen DRD Infos in die Datenbank
	void SaveDrdInformation();
	// alle relevanten DRDs ermitteln die bisher in der Datenbank sind.
	// F�r jeden bisher vorhandenen DRD wird ein neuer DRD erzeugt
	void CreateTempDrdMap();
	// Schichtzeiten von der Oberfl�che einlesen und in COleDateTime umwandeln
	void ReadShiftTimes(int ipShift, COleDateTime& opTimeFrom,COleDateTime& opTimeTo);
	// Sichert die tempor�r gehaltenen DRA Infos in die Datenbank
	void SaveDraInformation();
	// alle relevanten DRAs ermitteln die bisher in der Datenbank sind.
	// F�r jeden bisher vorhandenen DRA wird ein neuer DRA erzeugt
	void CreateTempDraMap(bool bpCheckDel, long lpBsdUrno=0);
	// Liest alles und speichert
	bool ReadAndSaveData(bool bpCancelDoubleTour = false);
	// hiding fields of the SPR-data, removing other controls dynamically
	void HideSpr();
	void ChangeSub1Sub2ToDefault();
	

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TAGESLISTEDLG_H__EC839161_9636_11D1_940B_0000B44A2C90__INCLUDED_)
