// ExcelExportDlg.cpp : implementation file
//

#include "stdafx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ExcelExportDlg dialog


ExcelExportDlg::ExcelExportDlg(CWnd* pParent /*=NULL*/)
	: CDialog(ExcelExportDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(ExcelExportDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	pomParent = (DutyRoster_View*)pParent;
}


void ExcelExportDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ExcelExportDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ExcelExportDlg, CDialog)
	//{{AFX_MSG_MAP(ExcelExportDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ExcelExportDlg message handlers

BOOL ExcelExportDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// set title of form
	SetWindowText(LoadStg(IDS_STRING135));

	// set text of the radio buttons
	SetDlgItemText(IDC_R_EXCEL_VIEW,LoadStg(IDS_STRING136));
	SetDlgItemText(IDC_R_EXCEL_BLANK,LoadStg(IDS_STRING142));
	SetDlgItemText(IDC_R_EXCEL_ABSX,LoadStg(IDS_STRING143));

	// set text of OK and CANCEL buttons
	SetDlgItemText(IDOK,LoadStg(IDS_STRING998));
	SetDlgItemText(IDCANCEL,LoadStg(IDS_STRING999));

	HideRadios();

	return TRUE;
}

void ExcelExportDlg::OnOK() 
{
	CWaitCursor olDummy;
	bool blIsAnythingChecked = false;

	ExcelExport olExport(pomParent);

	// get the selected radio button and trigger the export
	if (((CButton*) GetDlgItem(IDC_R_EXCEL_VIEW))->GetCheck() == 1)
	{
		olExport.Export("VIEW_COMPLETE");
		blIsAnythingChecked = true;
	}
	else if (((CButton*) GetDlgItem(IDC_R_EXCEL_BLANK))->GetCheck() == 1)
	{
		olExport.Export("VIEW_BLANK");
		blIsAnythingChecked = true;
	}
	else if (((CButton*) GetDlgItem(IDC_R_EXCEL_ABSX))->GetCheck() == 1)
	{
		olExport.Export("VIEW_ABSENCE_X");
		blIsAnythingChecked = true;
	}

	// was anything checked?
	if (blIsAnythingChecked == false)
	{
		MessageBox(LoadStg(IDS_STRING144), LoadStg(IDS_STRING135),MB_OK | MB_ICONINFORMATION);
	}
	else
	{
		CDialog::OnOK();
	}
}

void ExcelExportDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}

// hide reports/exports in dependancy of customer
void ExcelExportDlg::HideRadios()
{
	CRect olRect;
	int ilMoveUp = 27;

	// resize the form
	GetWindowRect (olRect);
	ClientToScreen (olRect);
	MoveWindow(olRect.left, olRect.top, olRect.Width(), olRect.Height() - ilMoveUp);
	CenterWindow();

	// resize the frame containing the radios
	GetDlgItem(IDC_EXCEL_FRAME)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem(IDC_EXCEL_FRAME)->MoveWindow(olRect.left, olRect.top, olRect.Width(), olRect.Height() - ilMoveUp);

	if (ogCCSParam.GetParamValue(ogAppl,"ID_PROD_CUSTOMER") == "SIN")
	{
		// SIN paid only report of blank shifts and absences with 'X', so hide export of complete view
		// see PRF 5290 (vs. SIN 0199) and 5291 (vs. SIN 0200)
		GetDlgItem(IDC_R_EXCEL_VIEW)->ShowWindow(SW_HIDE);

		// move the radios
		GetDlgItem(IDC_R_EXCEL_BLANK)->GetWindowRect(olRect);
		ScreenToClient (olRect);
		GetDlgItem(IDC_R_EXCEL_BLANK)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

		GetDlgItem(IDC_R_EXCEL_ABSX)->GetWindowRect(olRect);
		ScreenToClient (olRect);
		GetDlgItem(IDC_R_EXCEL_ABSX)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());
	}
	else
	{
		// default is export of complete view and blank shifts, so we hide absences with 'X'
		GetDlgItem(IDC_R_EXCEL_ABSX)->ShowWindow(SW_HIDE);

		// no radios to move, because the hidden radio is the last one
	}

	// move the buttons
	GetDlgItem(IDOK)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem(IDOK)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

	GetDlgItem(IDCANCEL)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem(IDCANCEL)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());
}
