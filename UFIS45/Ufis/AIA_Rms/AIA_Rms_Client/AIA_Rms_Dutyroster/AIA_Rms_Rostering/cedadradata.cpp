#include <stdafx.h>

//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Globale Funktionen
//************************************************************************************************************************************************
//************************************************************************************************************************************************
 
//************************************************************************************************************************************************
// ProcessDraCf: Callback-Funktion f�r den Broadcast-Handler und die Broadcasts
//	BC_DRA_CHANGE, BC_DRA_NEW und BC_DRA_DELETE. Ruft zur eingentlichen Bearbeitung
//	der Nachrichten die Funktion CedaDraData::ProcessDraBc() der entsprechenden 
//	Instanz von CedaDraData auf.	
// R�ckgabe: keine
//************************************************************************************************************************************************

void  ProcessDraCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	// Empf�nger ermitteln
	CedaDraData *polDraData = (CedaDraData *)vpInstance;
	// Bearbeitungsfunktion des Empf�ngers aufrufen
	polDraData->ProcessDraBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Implementation der Klasse CedaDraData (Daily Roster Absences - untert�gige Abwesenheit)
//************************************************************************************************************************************************
//************************************************************************************************************************************************

//************************************************************************************************************************************************
// Konstruktor
//************************************************************************************************************************************************

CedaDraData::CedaDraData(CString opTableName, CString opExtName) : CedaData(&ogCommHandler)
{
    // Infostruktur vom Typ CEDARECINFO f�r DRA-Datens�tze
	// Wird von Vaterklasse CCSCedaData f�r verschiedene Funktionen
	// (CCSCedaData::GetBufferRecord(), CCSCedaData::MakeCedaData())
	// benutzt. Die Datenstruktur beschreibt die Tabellenstruktur.
    BEGIN_CEDARECINFO(DRADATA, DraDataRecInfo)
		FIELD_OLEDATE	(Abfr,"ABFR")	// Abwesenheit von
		FIELD_CHAR_TRIM	(Drrn,"DRRN")	// Schichtnummer (1-n pro Tag)
		FIELD_OLEDATE	(Abto,"ABTO")	// Abwesenheit bis
		FIELD_CHAR_TRIM	(Rema,"REMA")	// Bemerkung
		FIELD_CHAR_TRIM	(Sdac,"SDAC")	// Abwesenheitscode aus ODA
		FIELD_CHAR_TRIM	(Absc,"ABSC")	// besondere Abwesenheitsarten
		FIELD_CHAR_TRIM	(Sday,"SDAY")	// Schichttag
		FIELD_LONG		(Stfu,"STFU")	// MA-Urno
		FIELD_LONG		(Urno,"URNO")	// DS-Urno
		FIELD_LONG		(Bsdu,"BSDU")	// ODA-Urno
    END_CEDARECINFO

	// Infostruktur kopieren
    for (int i = 0; i < sizeof(DraDataRecInfo)/sizeof(DraDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&DraDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

	// Tabellenname per Default auf DRA setzen
	opExtName = pcgTableExt;
	SetTableNameAndExt(opTableName+opExtName);

    // Feldnamen initialisieren
	strcpy(pcmListOfFields,"ABFR,DRRN,ABTO,REMA,SDAC,ABSC,SDAY,STFU,URNO,BSDU");

	// Zeiger der Vaterklasse auf Tabellenstruktur setzen
	CCSCedaData::pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

    // Datenarrays initialisieren
	omUrnoMap.InitHashTable(256);
}

//************************************************************************************************************************************************
// Destruktor
//************************************************************************************************************************************************

CedaDraData::~CedaDraData()
{
	ClearAll(true);
	omRecInfo.DeleteAll();
}

//************************************************************************************************************************************************
// SetTableNameAndExt: stellt den Namen und die Extension ein.
// R�ckgabe:	false	->	Tabellenname mit Extension gr��er als
//							Klassenmember
//				true	->	alles OK
//************************************************************************************************************************************************

bool CedaDraData::SetTableNameAndExt(CString opTableAndExtName)
{
CCS_TRY;
	if(opTableAndExtName.GetLength() >= sizeof(pcmTableName)) return false;
	strcpy(pcmTableName,opTableAndExtName.GetBuffer(0));
	return true;
CCS_CATCH_ALL;
return false;
}

//************************************************************************************************************************************************
// Register: beim Broadcasthandler anmelden. Um Broadcasts (Meldungen vom 
//	Datenbankhandler) zu empfangen, muss diese Funktion ausgef�hrt werden.
//	Der letzte Parameter ist ein Zeiger auf eine Callback-Funktion, welche
//	dann die entsprechende Nachricht bearbeitet. �ber den nach void 
//	konvertierten Zeiger auf dieses Objekt ((void *)this) kann das 
//	Objekt ermittelt werden, das der eigentliche Empf�nger der Nachricht ist
//	und an das die Callback-Funktion die Nachricht weiterleitet (per Aufruf einer
//	entsprechenden Member-Funktion).
// R�ckgabe: keine
//************************************************************************************************************************************************

void CedaDraData::Register(void)
{
CCS_TRY;
	// bereits angemeldet?
	if (bmIsBCRegistered) return;	// ja -> gibt nichts zu tun, terminieren

	// DRA-Datensatz hat sich ge�ndert
	DdxRegister((void *)this,BC_DRA_CHANGE,"CedaDraData", "BC_DRA_CHANGE",	ProcessDraCf);
	DdxRegister((void *)this,BC_DRA_NEW,   "CedaDraData", "BC_DRA_NEW",		ProcessDraCf);
	DdxRegister((void *)this,BC_DRA_DELETE,"CedaDraData", "BC_DRA_DELETE",  ProcessDraCf);
	// jetzt ist die Instanz angemeldet
	bmIsBCRegistered = true;
CCS_CATCH_ALL;
}


//************************************************************************************************************************************************
// ClearAll: aufr�umen. Alle Arrays und PointerMaps werden geleert.
// R�ckgabe:	immer true
//************************************************************************************************************************************************

bool CedaDraData::ClearAll(bool bpUnregister)
{
CCS_TRY;
    // alle Arrays leeren
	omUrnoMap.RemoveAll();
	omData.DeleteAll();
	
	// beim BC-Handler abmelden
	if(bpUnregister && bmIsBCRegistered)
	{
		ogDdx.UnRegister(this,NOTUSED);
		// jetzt ist die Instanz angemeldet
		bmIsBCRegistered = false;
	}
    return true;
CCS_CATCH_ALL;
return false;
}

//************************************************************************************************************************************************
// ReadDraByUrno: liest den DRA-Datensatz mit der  Urno <lpUrno> aus der 
//	Datenbank.
// R�ckgabe:	boolean Datensatz erfolgreich gelesen?
//************************************************************************************************************************************************

bool CedaDraData::ReadDraByUrno(long lpUrno, DRADATA *prpDra)
{
CCS_TRY;
	CString olWhere;
	olWhere.Format("%ld", lpUrno);
	// Datensatz aus Datenbank lesen

	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, olWhere);
		WriteInRosteringLog();
	}

	if (CedaAction2("RT", (char*)(LPCTSTR)olWhere) == false)
	{
		return false;	// Fehler -> abbrechen
	}
	// Datensatz-Objekt instanziieren...
	DRADATA *prlDra = new DRADATA;
	// und initialisieren
	if (GetBufferRecord2(0,prpDra) == true)
	{  
		ClearFastSocketBuffer();	
		return true;	// Aktion erfolgreich
	}
	else
	{
		// Fehler -> aufr�umen und terminieren
		delete prlDra;
		return false;
	}

	return false;
CCS_CATCH_ALL;
return false;
}

//************************************************************************************************************************************************
// ReadFilteredByDateAndStaff: liest Datens�tze ein, deren Feld SDAY (g�ltig am)
//	innerhalb des Zeitraums <opStart> bis <opEnd> liegen. Die Parameter werden auf 
//	G�ltigkeit gepr�ft. Wenn <popLoadStfUrnoMap> nicht NULL ist, werden nur die 
//	Datens�tze geladen, deren Feld STFU (Mitarbeiter-Urno) im Array enthalten ist.
// R�ckgabe:	true	->	Aktion erfolgreich
//				false	->	Fehler beim Einlesen der Datens�tze
//************************************************************************************************************************************************

bool CedaDraData::ReadFilteredByDateAndStaff(COleDateTime opStart, COleDateTime opEnd,
											 CMapPtrToPtr *popLoadStfUrnoMap /*= NULL*/,
											 bool bpRegisterBC /*= true*/,
											 bool bpClearData /*= true*/)
{
CCS_TRY;
	// G�ltigkeit der Datumsangaben pr�fen
	if ((opStart.GetStatus() != COleDateTime::valid) ||
		(opEnd.GetStatus() != COleDateTime::valid) || 
		(opStart >= opEnd))
	{
		// Fehler -> terminieren
		return false;
	}

	// Puffer f�r WHERE-Statement
	CString olWhere;

	// Anzahl der Urnos
	int ilCountStfUrnos = -1;
	if ((popLoadStfUrnoMap != NULL) && ((ilCountStfUrnos = popLoadStfUrnoMap->GetCount()) == 0))
	{
		// es gibt nichts zu tun, kein Fehler
		return true;	
	}
	
	// keine Urno-Liste oder Urno-Liste �bergeben aber Anzahl gr��er als max. in WHERE-Clause erlaubte MA-Urnos?
	if ((popLoadStfUrnoMap == NULL) || (ilCountStfUrnos > MAX_WHERE_STAFFURNO_IN))
	{
		// mehr Urnos als in WHERE-Clause passen -> alles laden und lokal filtern
		olWhere.Format("WHERE SDAY BETWEEN '%s' AND '%s'",opStart.Format("%Y%m%d"), opEnd.Format("%Y%m%d"),opStart.Format("%Y%m%d"), opEnd.Format("%Y%m%d"));
		// Datens�tze einlesen und Ergebnis zur�ckgeben
		return Read(olWhere.GetBuffer(0),popLoadStfUrnoMap,bpRegisterBC,bpClearData);
	}
	else
	{
		// MA-Urnos mit in WHERE-Clause aufnehmen
		POSITION pos;			// zum Iterieren
		void *plDummy;			// das Objekt, immer NULL und daher obsolet
		long llStfUrno;			// die Urno als long
		CString	olBuf;			// um die Urno in einen String zu konvertieren
		CString olStfUrnoList;	// die Urno-Liste
		// alle Urnos ermitteln
		for(pos = popLoadStfUrnoMap->GetStartPosition(); pos != NULL;)   
		{
			// n�chste Urno
			popLoadStfUrnoMap->GetNextAssoc(pos,(void*&)llStfUrno,plDummy);
			// umwandeln in String
			olBuf.Format("%ld",llStfUrno);
			// ab in die Liste
			olStfUrnoList += ",'" + olBuf + "'";
		}
		// f�hrendes Komma aus Urno-Liste entfernen, WHERE-Statement generieren
		olWhere.Format("WHERE STFU IN (%s) AND SDAY BETWEEN '%s' AND '%s'",olStfUrnoList.Mid(1),opStart.Format("%Y%m%d"), opEnd.Format("%Y%m%d"),opStart.Format("%Y%m%d"), opEnd.Format("%Y%m%d"));
		// Datens�tze einlesen und Ergebnis zur�ckgeben
		return Read(olWhere.GetBuffer(0),NULL,bpRegisterBC,bpClearData);
	}
CCS_CATCH_ALL;
return false;
}

//************************************************************************************************************************************************
// Read: liest Datens�tze ein.
// R�ckgabe:	true	->	Aktion erfolgreich
//				false	->	Fehler beim Einlesen der Datens�tze
//************************************************************************************************************************************************

bool CedaDraData::Read(char *pspWhere /*=NULL*/, CMapPtrToPtr *popLoadStfUrnoMap /*= NULL*/,
					   bool bpRegisterBC /*= true*/, bool bpClearData /*= true*/)
{
CCS_TRY;

	// wenn gew�nscht, aufr�umen (und beim Broadcast-Handler abmelden, wenn
	// nach Ende angemeldet werden soll)
	if (bpClearData)
		ClearAll(bpRegisterBC);
	
    // Datens�tze lesen
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(pspWhere == NULL)
	{
		if (!CedaData::CedaAction2("RT", "")) return false;
	}
	else
	{
		if (!CedaData::CedaAction2("RT", pspWhere)) return false;
	}

	// ToDo: vereinfachen/umwandeln in do/while, zwei branches f�r
	// if(pomLoadStfuUrnoMap != NULL) �berfl�ssig
    bool blMoreRecords = false;
	// Datensatzz�hler f�r TRACE
	int ilCountRecord = 0;
	// void-Pointer f�r MA-Urno-Lookup
	void  *prlVoid = NULL;
	// solange es Datens�tze gibt
	do
	{
		// neuer Datensatz
		DRADATA *prlDra = new DRADATA;
		// Datensatz lesen
		blMoreRecords = GetBufferRecord2(ilCountRecord,prlDra);
		// Datensatz gelesen, Z�hler inkrementieren
		ilCountRecord++;
		// wurde eine Datensatz gelesen und ist (wenn vorhanden) die
		// Mitarbeiter-Urno dieses Datensatzes im MA-Urno-Array enthalten? 
		if(blMoreRecords && 
		   ((popLoadStfUrnoMap == NULL) || 
			(popLoadStfUrnoMap->Lookup((void *)prlDra->Stfu, (void *&)prlVoid) == TRUE)))
		{ 
			// Datensatz hinzuf�gen
			if (!InsertInternal(prlDra, DRA_NO_SEND_DDX))
			{
				// Fehler -> lokalen Datensatz l�schen
				if(IsTraceLoggingEnabled())
				{
					GetDefectDataString(ogRosteringLogText, (void*)prlDra);
					WriteInRosteringLog(LOGFILE_TRACE);
				}
				delete prlDra;
			}
#ifdef TRACE_FULL_DATA
			else
			{
				// Datensatz OK, loggen if FULL
				ogRosteringLogText.Format(" read %8.8lu %s OK:  ", ilCountRecord, pcmTableName);
				GetDataFormatted(ogRosteringLogText, prlDra);
				WriteLogFull("");
			}
#endif TRACE_FULL_DATA
		}
		else
		{
			// kein weiterer Datensatz
			delete prlDra;
		}
	} while (blMoreRecords);
	ClearFastSocketBuffer();	
	
	// beim Broadcast-Handler anmelden, wenn gew�nscht
	if (bpRegisterBC) Register();

	// Test: Anzahl der gelesenen DRA
	TRACE("Read-Dra: %d gelesen\n",ilCountRecord);
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  read %d records from table %s",ilCountRecord, pcmTableName);
		WriteInRosteringLog();
	}



    return true;
CCS_CATCH_ALL;
return false;
}

//*********************************************************************************************
// Insert: speichert den Datensatz <prpDra> in der Datenbank und schickt einen
//	Broadcast ab.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaDraData::Insert(DRADATA *prpDra, bool bpSave /*= true*/)
{
CCS_TRY;
	// �nderungs-Flag setzen
	prpDra->IsChanged = DATA_NEW;
	// in Datenbank speichern, wenn erw�nscht
	if(bpSave && Save(prpDra) == false) return false;
	// Broadcast DRA_NEW abschicken
	InsertInternal(prpDra,true);
    return true;
CCS_CATCH_ALL;
return false;
}

//*********************************************************************************************
// InsertInternal: f�gt den internen Arrays einen Datensatz hinzu. Der Datensatz
//	muss sp�ter oder vorher explizit durch Aufruf von Save() oder Release() in der Datenbank 
//	gespeichert werden. Wenn <bpSendDdx> true ist, wird ein Broadcast gesendet.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaDraData::InsertInternal(DRADATA *prpDra, bool bpSendDdx)
{
CCS_TRY;

	DRADATA *prlData;
	// �bergebener Datensatz NULL oder Datensatz mit diesem Schl�ssel
	// schon vorhanden?
	if(omUrnoMap.Lookup((void *)prpDra->Urno,(void *&)prlData)  == TRUE){
		return false;
	}

	// Datensatz intern anf�gen
	omData.Add(prpDra);
	// Datensatz der Urno-Map hinzuf�gen
	omUrnoMap.SetAt((void *)prpDra->Urno,prpDra);
		
	// Broadcast senden?
	if( bpSendDdx == true )
	{
		// ja -> go!
		ogDdx.DataChanged((void *)this,DRA_NEW,(void *)prpDra);
	}
	return true;
CCS_CATCH_ALL;
return false;
}

//*********************************************************************************************
// Update: sucht den Datensatz mit der Urno <prpDra->Urno> und speichert ihn
//	in der Datenbank.
// R�ckgabe:	false	->	Datensatz wurde nicht gefunden
//				true	->	alles OK
//*********************************************************************************************

bool CedaDraData::Update(DRADATA *prpDra,bool bpSave /*= true*/)
{
CCS_TRY;
	// Datensatz raussuchen
	if (GetDraByUrno(prpDra->Urno) != NULL)
	{
		// �nderungsflag setzen
		if (prpDra->IsChanged == DATA_UNCHANGED)
		{
			prpDra->IsChanged = DATA_CHANGED;
		}
		// in die Datenbank speichern
		if(bpSave && Save(prpDra) == false) return false; 
		// Broadcast DRA_CHANGE versenden
		UpdateInternal(prpDra);
	}
    return true;
CCS_CATCH_ALL;
return false;
}

//*********************************************************************************************
// UpdateInternal: �ndert einen Datensatz, der in der internen Datenhaltung
//	enthalten ist.
// R�ckgabe:	false	->	Datensatz wurde nicht gefunden
//				true	->	alles OK
//*********************************************************************************************

bool CedaDraData::UpdateInternal(DRADATA *prpDra, bool bpSendDdx /*true*/)
{
CCS_TRY;
// APO 9.12.99 Updates passieren immer auf Kopie eines Datensatzes der internen
// Datenhaltung, kopieren daher unn�tig.
/*	// DRA in lokaler Datenhaltung suchen
	DRADATA *prlDra = GetDraByKey(prpDra->Sday,prpDra->Stfu,prpDra->Dran,prpDra->Rosl);
	// DRA gefunden?
	if (prlDra != NULL)
	{
		// ja -> durch Zeiger ersetzen
		*prlDra = *prpDra;
	}
*/
	// Broadcast senden, wenn gew�nscht
	if( bpSendDdx == true )
	{
//		TRACE("Sending %d\n",DRA_CHANGE);
		ogDdx.DataChanged((void *)this,DRA_CHANGE,(void *)prpDra);
	}
	return true;
CCS_CATCH_ALL;
return false;
}

//*********************************************************************************************
// DeleteInternal: l�scht einen Datensatz aus den internen Arrays.
// R�ckgabe:	keine
//*********************************************************************************************

void CedaDraData::DeleteInternal(DRADATA *prpDra, bool bpSendDdx /*true*/)
{
CCS_TRY;
	// zuerst Broadcast senden, damit alle Module reagieren k�nnen BEVOR
	// der Datensatz gel�scht wird (synchroner Mechanismus)
	if(bpSendDdx == true)
	{
		ogDdx.DataChanged((void *)this,DRA_DELETE,(void *)prpDra);
	}

	// Urno-Schl�ssel l�schen
	omUrnoMap.RemoveKey((void *)prpDra->Urno);

	// Datensatz l�schen
	int ilCount = omData.GetSize();
	for (int i = 0; i < ilCount; i++)
	{
		if (omData[i].Urno == prpDra->Urno)
		{
			omData.DeleteAt(i);//Update omData
			break;
		}
	}
CCS_CATCH_ALL;
}

//*********************************************************************************************
// RemoveInternalByStaffUrno: entfernt alle DRAs aus der internen Datenhaltung,
//	die dem Mitarbeiter <lpStfUrno> zugeordnet sind. Die Datens�tze werden NICHT 
//	aus der Datenbank gel�scht.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaDraData::RemoveInternalByStaffUrno(long lpStfUrno, bool bpSendBC /*= false*/)
{
CCS_TRY;
	DRADATA *prlDra;
	// alle DRAs durchgehen
	for(int i=0; i<omData.GetSize(); i++)
	{
		// DRA dieses MAs entfernen?
		if(lpStfUrno == omData[i].Stfu)
		{
			// ja
			if ((prlDra =  GetDraByUrno(omData[i].Urno)) == NULL) return false;
			// DRA entfernen
			DeleteInternal(prlDra, bpSendBC);
			// neue Datensatz-Anzahl anpassen
			i--;
		}
	}
	return true;
CCS_CATCH_ALL;
return false;
}

//*********************************************************************************************
// Delete: markiert den Datensatz <prpDra> als gel�scht und l�scht den Datensatz
//	aus der internen Datenhaltung und der Datenbank. 
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaDraData::Delete(DRADATA *prpDra, bool bpWithSave /* true*/)
{
CCS_TRY;
	// Flag setzen
	prpDra->IsChanged = DATA_DELETED;
	
	// speichern
	if(bpWithSave == TRUE)
	{
		if (!Save(prpDra)) return false;
	}

	// aus der internen Datenhaltung l�schen
	DeleteInternal(prpDra,true);

	return true;
CCS_CATCH_ALL;
return false;
}

//*********************************************************************************************
// ProcessDraBc: behandelt die einlaufenden Broadcasts.
// R�ckgabe:	keine
//*********************************************************************************************

void  CedaDraData::ProcessDraBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
CCS_TRY;
	// Performance-Trace
	SYSTEMTIME rlPerf;
	GetSystemTime(&rlPerf);
	TRACE("CedaDraData::ProcessDraBc: START %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);

	// Datensatz-Urno
	long llUrno;
	// Daten und Info
	struct BcStruct *prlBcStruct = NULL;
	prlBcStruct = (struct BcStruct *) vpDataPointer;
	DRADATA *prlDra = NULL;

		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("ProcessBC:\n  Cmd <%s>\n  Tbl <%s>\n  Twe <%s>\n  Sel <%s>\n  Fld <%s>\n  Dat <%s>\n  Bcn <%s>",prlBcStruct->Cmd, prlBcStruct->Object, prlBcStruct->Twe, prlBcStruct->Selection, prlBcStruct->Fields, prlBcStruct->Data,prlBcStruct->BcNum);
		WriteInRosteringLog();
	}

	switch(ipDDXType)
	{
	case BC_DRA_CHANGE:	// Datensatz �ndern
		{
			// Datenatz-Urno ermittlen
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			prlDra = GetDraByUrno(llUrno);
			if(prlDra != NULL)
			{
				// ja -> Datensatz aktualisieren
				GetRecordFromItemList(prlDra,prlBcStruct->Fields,prlBcStruct->Data);
				UpdateInternal(prlDra);
				break;
			}
			// nein -> gehe zum Insert, !!! weil es kann sein, dass diese �nderung den Mitarbeiter in View laden soll
		}
	case BC_DRA_NEW: // neuen Datensatz einf�gen
		{
			// einzuf�gender Datensatz
			prlDra = new DRADATA;
			GetRecordFromItemList(prlDra,prlBcStruct->Fields,prlBcStruct->Data);
			InsertInternal(prlDra, DRA_SEND_DDX);
		}
		break;
	case BC_DRA_DELETE:	// Datensatz l�schen
		{
			// Datenatz-Urno ermittlen
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			prlDra = GetDraByUrno(llUrno);
			if (prlDra != NULL)
			{
				// ja -> Datensatz l�schen
				DeleteInternal(prlDra);
			}
		}
		break;
	default:
		break;
	}
	// Performance-Test
	GetSystemTime(&rlPerf);
	TRACE("CedaDraData::ProcessDraBc: END %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
CCS_CATCH_ALL;
}

//*********************************************************************************************
// GetDraByUrno: sucht den Datensatz mit der Urno <lpUrno>.
// R�ckgabe:	ein Zeiger auf den Datensatz oder NULL, wenn der
//				Datensatz nicht gefunden wurde
//*********************************************************************************************

DRADATA *CedaDraData::GetDraByUrno(long lpUrno)
{
CCS_TRY;
	// der Datensatz
	DRADATA *prpDra;
	// Datensatz in Urno-Map suchen
	if (omUrnoMap.Lookup((void*)lpUrno,(void *& )prpDra) == TRUE)
	{
		return prpDra;	// gefunden -> Zeiger zur�ckgeben
	}
	// nicht gefunden -> R�ckgabe NULL
	return NULL;
CCS_CATCH_ALL;
return NULL;
}

//*******************************************************************************
// Save: speichert den Datensatz <prpDra>.
// R�ckgabe:	bool Erfolg?
//*******************************************************************************

bool CedaDraData::Save(DRADATA *prpDra)
{
CCS_TRY;
	bool olRc = true;
	CString olListOfData;
	char pclSelection[1024];
	char pclData[2048];

	if (prpDra->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpDra->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpDra);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpDra->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpDra->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpDra);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpDra->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpDra->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpDra);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("DRT",pclSelection,"",pclData);
		break;
	}
	// R�ckgabe: Ergebnis der CedaAction
	return olRc;
CCS_CATCH_ALL;
return false;
}

//*************************************************************************************
// CedaAction: ruft die Funktion CedaAction der Basisklasse CCSCedaData auf.
//	Alle Parameter werden durchgereicht. Aufgrund eines internen CEDA-Bugs,
//	der daf�r sorgen kann, dass die Tabellenbeschreibung <pcmListOfFields>
//	zerst�rt wird, muss sicherheitshalber <pcmListOfFields> vor jedem Aufruf
//	von CedaData::CedaAction() gerettet und danach wiederhergestellt werden.
// R�ckgabe:	der R�ckgabewert von CedaData::CedaAction()
//*************************************************************************************

bool CedaDraData::CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
							 char *pcpSelection, char *pcpSort, char *pcpData, 
							 char *pcpDest /*= "BUF1"*/,bool bpIsWriteAction /*= false*/, 
							 long lpID /*= 0*/, CCSPtrArray<RecordSet> *pomData /*= NULL*/, 
							 int ipFieldCount /*= 0*/)
{
CCS_TRY;
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CedaData::CedaAction(pcpAction,pcpSelection,pcpSort,pcpData,pcpDest,bpIsWriteAction,lpID);
	// Feldliste wiederherstellen
	ClearFastSocketBuffer();	
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
CCS_CATCH_ALL;
return false;
}

//*************************************************************************************
// CedaAction: kurze Version, Beschreibung siehe oben.
// R�ckgabe:	der R�ckgabewert von CedaData::CedaAction()
//*************************************************************************************

bool CedaDraData::CedaAction(char *pcpAction, char *pcpSelection, char *pcpSort,
							 char *pcpData, char *pcpDest /*"BUF1"*/, 
							 bool bpIsWriteAction/* false */, long lpID /* = imBaseID*/)
{
CCS_TRY;
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CedaData::CedaAction(pcpAction,CCSCedaData::pcmTableName, CCSCedaData::pcmFieldList,(pcpSelection != NULL)? pcpSelection: CCSCedaData::pcmSelection,(pcpSort != NULL)? pcpSort: CCSCedaData::pcmSort,pcpData,pcpDest,bpIsWriteAction, lpID);
	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
CCS_CATCH_ALL;
return false;
}

//*********************************************************************************************
// GetDraMapOfDrr: einen Array mit den Zeigern auf die Datens�tze f�llen, die 
//	dem DRR <popDrr> zugeordnet sind.
// R�ckgabe:	die Anzahl der zugeordneten DRAs oder -1, wenn einer der 
//				Parameter ung�ltig ist
//*********************************************************************************************

int CedaDraData::GetDraMapOfDrr(CMapPtrToPtr *popDraMap, DRRDATA *popDrr)
{
CCS_TRY;
	// pr�fen, ob die Map g�ltig ist
	if ((popDraMap == NULL) || (popDrr == NULL)) return -1;

	// Array leeren
	popDraMap->RemoveAll();

	// alle internen Datens�tze untersuchen
	for (int ilCount=0; ilCount<omData.GetSize(); ilCount++)
	{
		// jeden DRR untersuchen
		if ((strcmp(omData[ilCount].Sday,popDrr->Sday) == 0) && 
			(omData[ilCount].Stfu == popDrr->Stfu) && 
			(strcmp(omData[ilCount].Drrn,popDrr->Drrn) == 0))
		{
			// Zeiger auf Datensatz in die Map
			popDraMap->SetAt((void *)omData[ilCount].Urno,&omData[ilCount]);
		}
	}
	// Anzahl der Datens�tze zur�ck
	return popDraMap->GetCount();
CCS_CATCH_ALL;
return -1;
}

//*********************************************************************************************
// DecreaseDrrn: sucht alle DRAs, die zum DRR <popDrr> geh�ren und verringert
//	die DRR-Nummer (DRRN) um eins. Achtung!!! Diese Funktion muss VOR dem �ndern
//	des zugeh�rigen DRR aufgerufen werden. Vor dem Aufruf dieser Funktion
//	m�ssen jedoch alle alten DRAs mit der neuen DRR-Nummer gel�scht worden sein.
// R�ckgabe:	> 0	->	Aktion erfolgreich, Anzahl der ge�nderten DRA
//				-1	->	Fehler
//*********************************************************************************************

int CedaDraData::DecreaseDrrn(DRRDATA *popDrr)
{
CCS_TRY;
	// neue DRR-Nummer generieren
	int ilDrrn = atoi(popDrr->Drrn) - 1;
	CString olDrrn;
	olDrrn.Format("%d",ilDrrn);
	// Z�hler als R�ckgabe
	int ilCountDra = 0;
	// alle internen Datens�tze untersuchen
	for(int ilCount=0; ilCount<omData.GetSize(); ilCount++)
	{
		// jeden DRR untersuchen
		if ((strcmp(omData[ilCount].Sday,popDrr->Sday) == 0) && 
			(omData[ilCount].Stfu == popDrr->Stfu) && 
			(strcmp(omData[ilCount].Drrn,popDrr->Drrn) == 0))
		{
			TRACE("CedaDraData::DecreaseDrrn(): DRA gefunden!!!\n");
			// neue DRR_Nummer einstellen
			strcpy(omData[ilCount].Drrn,olDrrn.GetBuffer(0));
			// Datensatz speichern
			if (!Update(&omData[ilCount])) return -1;
			ilCountDra++;
		}
	}
	return ilCountDra;
CCS_CATCH_ALL;
return -1;
}

//*********************************************************************************************
// SwapDrrn: sucht alle DRAs, die zu den DRRs <popDrr1> und <popDrr2> geh�ren und 
//	vertauscht die DRR-Nummern.
// R�ckgabe:	true	->	Aktion erfolgreich, Anzahl der ge�nderten DRA
//				false	->	Fehler
//*********************************************************************************************

bool CedaDraData::SwapDrrn(DRRDATA *popDrr1, DRRDATA *popDrr2)
{
CCS_TRY;
	// alle internen Datens�tze untersuchen
	for(int ilCount=0; ilCount<omData.GetSize(); ilCount++)
	{
		// jeden DRR untersuchen: geh�rt dieser DRA zum DRR1?
		if ((strcmp(omData[ilCount].Sday,popDrr1->Sday) == 0) && 
			(omData[ilCount].Stfu == popDrr1->Stfu) && 
			(strcmp(omData[ilCount].Drrn,popDrr1->Drrn) == 0))
		{
			// ja -> DRR-Nummer �ndern
			strcpy(omData[ilCount].Drrn,popDrr2->Drrn);
			// Datensatz speichern
			if (!Update(&omData[ilCount])) return false;
		}
		else if ((strcmp(omData[ilCount].Sday,popDrr2->Sday) == 0) && 
				 (omData[ilCount].Stfu == popDrr2->Stfu) && 
				 (strcmp(omData[ilCount].Drrn,popDrr2->Drrn) == 0))
		{
			// ja -> DRR-Nummer �ndern
			strcpy(omData[ilCount].Drrn,popDrr1->Drrn);
			// Datensatz speichern
			if (!Update(&omData[ilCount])) return false;
		}
	}
	return true;
CCS_CATCH_ALL;
return false;
}

//*********************************************************************************************
// HasDraOfDrr: pr�fen, ob dem DRR <popDrr> DRAs zugeordnet sind.
// R�ckgabe:	true	->	DRA gefunden
//				false	->	kein DRA gefunden
//*********************************************************************************************

bool CedaDraData::HasDraOfDrr(DRRDATA *popDrr)
{
CCS_TRY;
	// pr�fen, ob der Zeiger g�ltig ist
	if (popDrr == NULL) return false;	// ung�ltig -> terminieren

	// alle internen Datens�tze untersuchen
	for(int ilCount=0; ilCount<omData.GetSize(); ilCount++)
	{
		// jeden DRR untersuchen
		if ((strcmp(omData[ilCount].Sday,popDrr->Sday) == 0) && 
			(omData[ilCount].Stfu == popDrr->Stfu) && 
			(strcmp(omData[ilCount].Drrn,popDrr->Drrn) == 0))
		{
			// R�ckgabe: DRA gefunden!
			return true;
		}
	}
	// R�ckgabe: keinen DRA gefunden!
	return false;
CCS_CATCH_ALL;
return false;
}

//*********************************************************************************************
// HasDraOfDrr: pr�fen, ob dem DRR <popDrr> DRAs ohne DEL zugeordnet sind.
// R�ckgabe:	true	->	mindestens eine DRA ohne DEL gefunden
//				false	->	kein DRA gefunden bzw. alle DRA mit DEL
//*********************************************************************************************
//uhi 8.3.01
bool CedaDraData::HasDraOfDrrWithoutDel(DRRDATA *popDrr)
{
CCS_TRY;
	CString olAbfr, olAbto;
	// pr�fen, ob der Zeiger g�ltig ist
	if (popDrr == NULL) return false;	// ung�ltig -> terminieren

	// alle internen Datens�tze untersuchen
	for(int ilCount=0; ilCount<omData.GetSize(); ilCount++)
	{
		// jeden DRR untersuchen
		if ((strcmp(omData[ilCount].Sday,popDrr->Sday) == 0) && 
			(omData[ilCount].Stfu == popDrr->Stfu) && 
			(strcmp(omData[ilCount].Drrn,popDrr->Drrn) == 0))
		{
			olAbfr = omData[ilCount].Abfr.Format("%H%M");
			olAbto =  omData[ilCount].Abto.Format("%H%M");
			TRACE("CedaDraData::HasDraData(): DRA gefunden!!!\n");
			if (!ogDelData.HasDel(popDrr->Bsdu, omData[ilCount].Sdac, olAbfr, olAbto))
				return true;
		}
	}
	// R�ckgabe: keinen DRA gefunden!
	return false;
CCS_CATCH_ALL;
return false;
}

//*********************************************************************************************
// IsValidDra: pr�ft, ob der �bergebene Zeitraum zu einem der 
//	Zeitr�ume der DRAs in <popDraMap> in Konflikt steht.
// R�ckgabe:	true	->	Zeitraum ist OK
//				false	->	Zeitraum ist nicht OK
//*********************************************************************************************

bool CedaDraData::IsValidDra(COleDateTime opDateFrom, COleDateTime opDateTo, long lpUrno,
							 CMapPtrToPtr *popDraMap, DRRDATA *popDrr /*= NULL*/)
{
CCS_TRY;
	// g�ltiger DRR?
	if (popDrr != NULL)
	{
		// ja -> Zeitbereich pr�fen
		if(	opDateFrom.GetStatus() != COleDateTime::valid || 
			opDateTo.GetStatus() != COleDateTime::valid || 
			opDateFrom <= popDrr->Avfr	|| 
			opDateFrom >= popDrr->Avto	|| 
			opDateTo <= popDrr->Avfr	||
			opDateTo >= popDrr->Avto)
		{
			// einer der Zeitwerte liegt ausserhalb des g�ltigen Bereichs ->
			// DRA ist ung�ltig, abbrechen
			return false;
		}
	}

	POSITION rlPos; 
	long llUrno; 
	DRADATA *prlDra; 
	// alle DRAs in <popDraMap> pr�fen und die Zeitspanne mit der von <popDra> pr�fen
	for(rlPos = popDraMap->GetStartPosition(); rlPos != NULL; )
	{
		// DRA ermitteln
		popDraMap->GetNextAssoc(rlPos,(void *&)llUrno, (void *&)prlDra);
		// DRA nicht mit sich selbst vergleichen
		if (lpUrno != prlDra->Urno)
		{
			// liegt der Start oder das Ende der Abwesenheit innerhalb des Zeitraums des 
			// vorhandenen DRAs?
			if (((opDateFrom >= prlDra->Abfr) && (opDateFrom < prlDra->Abto)) || // Start liegt innerhalb
				((opDateTo > prlDra->Abfr) && (opDateTo <= prlDra->Abto)))	// Ende liegt innerhalb
			{
				// ja -> DRA ist ung�ltig
				return false;
			}
		}	
	}
	// DRA ist g�ltig
	return true;
CCS_CATCH_ALL;
return false;
}

//************************************************************************************************************************************************
// CopyDraValues: Kopiert die "Nutzdaten" (keine urnos usw.)
// R�ckgabe:	keine
//************************************************************************************************************************************************

void CedaDraData::CopyDra(DRADATA* popDraDataSource,DRADATA* popDraDataTarget)
{
	// Daten kopieren
	strcpy(popDraDataTarget->Rema,popDraDataSource->Rema);
	strcpy(popDraDataTarget->Sdac,popDraDataSource->Sdac);
	popDraDataTarget->Bsdu = popDraDataSource->Bsdu;
	popDraDataTarget->Abfr = popDraDataSource->Abfr;
	popDraDataTarget->Abto = popDraDataSource->Abto;
}

//************************************************************************************************************************************************
// CreateDraData: initialisiert eine Struktur vom Typ DRADATA.
// R�ckgabe:	ein Zeiger auf den ge�nderten / erzeugten Datensatz oder
//				NULL, wenn ein Fehler auftrat.
//************************************************************************************************************************************************

DRADATA* CedaDraData::CreateDraData(DRRDATA *popDrr)
{
CCS_TRY;
	// Objekt erzeugen
	DRADATA *prlDraData = new DRADATA;
	// �nderungsflag setzen
	prlDraData->IsChanged = DATA_NEW;
	// Schichttag einstellen
	strcpy(prlDraData->Sday,popDrr->Sday);
	// Mitarbeiter-Urno
	prlDraData->Stfu = popDrr->Stfu;
	// n�chste freie Datensatz-Urno ermitteln und speichern
	prlDraData->Urno = ogBasicData.GetNextUrno();
	// DRRN setzen (Nummer des DRA bei mehreren Schichten an einem Tag)
	strcpy(prlDraData->Drrn,popDrr->Drrn);
	// Zeiger auf Datensatz zur�ck
	return prlDraData;
CCS_CATCH_ALL;
return NULL;
}

