// EmplDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static int Compare_SNAME_U( const AWSTFVIEWDATA_EMPL **e1, const AWSTFVIEWDATA_EMPL **e2);
static int Compare_LNAME_U( const AWSTFVIEWDATA_EMPL **e1, const AWSTFVIEWDATA_EMPL **e2);
static int Compare_FUNC_U( const AWSTFVIEWDATA_EMPL **e1, const AWSTFVIEWDATA_EMPL **e2);
static int Compare_WGRP_U( const AWSTFVIEWDATA_EMPL **e1, const AWSTFVIEWDATA_EMPL **e2);
static int Compare_CPOOL_U( const AWSTFVIEWDATA_EMPL **e1, const AWSTFVIEWDATA_EMPL **e2);
static int Compare_PNUM_U( const AWSTFVIEWDATA_EMPL **e1, const AWSTFVIEWDATA_EMPL **e2);
static int Compare_BEGINN_U( const AWSTFVIEWDATA_EMPL **e1, const AWSTFVIEWDATA_EMPL **e2);
static int Compare_END_U( const AWSTFVIEWDATA_EMPL **e1, const AWSTFVIEWDATA_EMPL **e2);

static int Compare_SNAME_D( const AWSTFVIEWDATA_EMPL **e1, const AWSTFVIEWDATA_EMPL **e2);
static int Compare_LNAME_D( const AWSTFVIEWDATA_EMPL **e1, const AWSTFVIEWDATA_EMPL **e2);
static int Compare_FUNC_D( const AWSTFVIEWDATA_EMPL **e1, const AWSTFVIEWDATA_EMPL **e2);
static int Compare_WGRP_D( const AWSTFVIEWDATA_EMPL **e1, const AWSTFVIEWDATA_EMPL **e2);
static int Compare_CPOOL_D( const AWSTFVIEWDATA_EMPL **e1, const AWSTFVIEWDATA_EMPL **e2);
static int Compare_PNUM_D( const AWSTFVIEWDATA_EMPL **e1, const AWSTFVIEWDATA_EMPL **e2);
static int Compare_BEGINN_D( const AWSTFVIEWDATA_EMPL **e1, const AWSTFVIEWDATA_EMPL **e2);
static int Compare_END_D( const AWSTFVIEWDATA_EMPL **e1, const AWSTFVIEWDATA_EMPL **e2);


//static int Compare_GRUPP_U( const AWDBOVIEWDATA_EMPL **e1, const AWDBOVIEWDATA_EMPL **e2);
//static int Compare_MEMBERS_U( const AWDBOVIEWDATA_EMPL **e1, const AWDBOVIEWDATA_EMPL **e2);

//static int Compare_GRUPP_D( const AWDBOVIEWDATA_EMPL **e1, const AWDBOVIEWDATA_EMPL **e2);
//static int Compare_MEMBERS_D( const AWDBOVIEWDATA_EMPL **e1, const AWDBOVIEWDATA_EMPL **e2);

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld EmplDlg 
//---------------------------------------------------------------------------



EmplDlg::EmplDlg(CStringList* popUrnoList, CString opSelectUrno,CWnd* pParent ): CDialog(EmplDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(EmplDlg)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT
	pomParent = (ShiftRoster_View*)pParent;
	omUp = " /\\";
	omDown = " \\/";
	omSelectUrno = opSelectUrno;
	pomUrnoList = popUrnoList;

	// lokale Kopie der URNOListe herstellen
	POSITION pos;
	pomLocalUrnoList = new CStringList();

	for( pos = pomUrnoList->GetHeadPosition(); pos != NULL; )
	{
		pomLocalUrnoList->AddTail(pomUrnoList->GetNext(pos));
	}
}

//**********************************************************************************
//
//**********************************************************************************

EmplDlg::~EmplDlg(void)
{
	omStfLines.DeleteAll();
	omDboLines.DeleteAll();
	delete pomLocalUrnoList;
}

//**********************************************************************************
//
//**********************************************************************************

void EmplDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(EmplDlg)
	DDX_Control(pDX, IDOK,				m_B_Ok);
	DDX_Control(pDX, IDCANCEL,			m_B_Cancel);
	DDX_Control(pDX, IDC_BUTTON1,		m_B_1);
	DDX_Control(pDX, IDC_BUTTON2,		m_B_2);
	DDX_Control(pDX, IDC_BUTTON3,		m_B_3);
	DDX_Control(pDX, IDC_BUTTON4,		m_B_4);
	DDX_Control(pDX, IDC_BUTTON5,		m_B_5);
	DDX_Control(pDX, IDC_BUTTON6,		m_B_6);
	DDX_Control(pDX, IDC_BUTTON7,		m_B_7);
	DDX_Control(pDX, IDC_BUTTON8,		m_B_8);
	DDX_Control(pDX, IDC_BUTTON9,		m_B_9);
	DDX_Control(pDX, IDC_B_DELETE,		m_B_Delete);
	DDX_Control(pDX, IDC_B_INSERT,		m_B_Insert);
	DDX_Control(pDX, IDC_LB_LIST,		m_LB_List);
	//}}AFX_DATA_MAP
}

//**********************************************************************************
//
//**********************************************************************************

BEGIN_MESSAGE_MAP(EmplDlg, CDialog)
	//{{AFX_MSG_MAP(EmplDlg)
	//ON_BN_CLICKED(IDC_B_DELETEALL,	OnBDeleteall)
	ON_BN_CLICKED(IDC_B_INSERT,		OnBInsert)
	ON_BN_CLICKED(IDC_B_DELETE,		OnBDelete)
	ON_BN_CLICKED(IDC_BUTTON1,		OnButton1)
	ON_BN_CLICKED(IDC_BUTTON2,		OnButton2)
	ON_BN_CLICKED(IDC_BUTTON3,		OnButton3)
	ON_BN_CLICKED(IDC_BUTTON4,		OnButton4)
	ON_BN_CLICKED(IDC_BUTTON5,		OnButton5)
	ON_BN_CLICKED(IDC_BUTTON6,		OnButton6)
	ON_BN_CLICKED(IDC_BUTTON7,		OnButton7)
	ON_BN_CLICKED(IDC_BUTTON8,		OnButton8)
	ON_BN_CLICKED(IDC_BUTTON9,		OnButton9)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

//**********************************************************************************
// Behandlungsroutinen f�r Nachrichten EmplDlg 
//**********************************************************************************

BOOL EmplDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	// Pointer Array mit View Daten ?!?
	omStfLines.DeleteAll();
	CString olLine, olTmpTxt;

	// Font der Listbox setzen.
	m_LB_List.SetFont(&ogCourier_Regular_10);

	// Buttontexte setzen.
	m_B_Ok.SetWindowText(LoadStg(SHIFT_STF_B_OK));
	m_B_Cancel.SetWindowText(LoadStg(SHIFT_STF_B_CANCEL));
	m_B_Delete.SetWindowText(LoadStg(IDS_STRING1812));
	m_B_Insert.SetWindowText(LoadStg(IDS_STRING1813));

	m_B_1.SetWindowText(LoadStg(SHIFT_STF_PERC));
	m_B_2.SetWindowText(LoadStg(SHIFT_STF_NAME));
	m_B_3.SetWindowText(LoadStg(SHIFT_STF_FUNCTION));
	m_B_4.SetWindowText(LoadStg(SHIFT_STF_WGRP));
	m_B_5.SetWindowText(LoadStg(SHIFT_STF_CPOOL));
	m_B_6.SetWindowText(LoadStg(IDS_STRING139));
	m_B_7.SetWindowText(LoadStg(IDS_STRING140));
	m_B_8.SetWindowText(LoadStg(SHIFT_STF_PENO));
	
	// ???
	m_LB_List.SetDefaultColorBySelection(false);

	// Gr��enberechnungen
	CRect olRectList;
	m_LB_List.GetWindowRect(&olRectList);
	ScreenToClient(&olRectList);

	int ilListLeft   = olRectList.left;
	//int ilListTop    = olRectList.top;
	//int ilListRight  = olRectList.right;
	//int ilListBottom = olRectList.bottom;

	int ilTop		= olRectList.top - 27;
	int ilBottom	= olRectList.top - 1;
	int ilFirstLeft = ilListLeft;
	int ilLastRight = olRectList.right;
	int ilFaktor	= 8;

	int ilLeft  = ilFirstLeft;
	int ilRight = 0;
	//Gesamt-Textl�nge ca. 112 Char.

	// Sortierungen voreinstellen
	imB1 = SRT_DOWN;
	imB2 = SRT_DOWN;
	imB3 = SRT_UP;
	imB4 = SRT_DOWN;
	imB5 = SRT_DOWN;
	imB6 = SRT_DOWN;
	imB7 = SRT_DOWN;
	imB8 = SRT_DOWN;
	imB9 = SRT_NOTSORT;

	m_B_9.ShowWindow(SW_HIDE);

	// Titel setzen.
	SetWindowText(LoadStg(IDS_STRING61191));
	
	// Buttons anordnen.
	ilRight = ilLeft + (5+2)*ilFaktor - 1;
	m_B_1.MoveWindow(CRect(ilLeft,ilTop,ilRight,ilBottom));
	ilLeft = ilRight+1;
	ilRight = ilLeft + (36+1)*ilFaktor - 1;
	m_B_2.MoveWindow(CRect(ilLeft,ilTop,ilRight,ilBottom));
	ilLeft = ilRight+1;
	ilRight = ilLeft + (15+1)*ilFaktor - 1;
	m_B_3.MoveWindow(CRect(ilLeft,ilTop,ilRight,ilBottom));
	ilLeft = ilRight+1;
	ilRight = ilLeft + (12+1)*ilFaktor - 1;
	m_B_4.MoveWindow(CRect(ilLeft,ilTop,ilRight,ilBottom));
	ilLeft = ilRight+1;
	ilRight = ilLeft + (9+1)*ilFaktor - 1;
	m_B_5.MoveWindow(CRect(ilLeft,ilTop,ilRight,ilBottom));
	ilLeft = ilRight+1;
	ilRight = ilLeft + (10+1)*ilFaktor - 1;
	m_B_6.MoveWindow(CRect(ilLeft,ilTop,ilRight,ilBottom));
	ilLeft = ilRight+1;
	ilRight = ilLeft + (10+1)*ilFaktor - 1;
	m_B_7.MoveWindow(CRect(ilLeft,ilTop,ilRight,ilBottom));
	ilLeft = ilRight+1;
	ilRight = ilLastRight;
	m_B_8.MoveWindow(CRect(ilLeft,ilTop,ilRight,ilBottom));
	
	CString *prlSurn = NULL;
	CString olSurn;


	CMapStringToPtr olSwgMap;
	int ilSwgCount =  ogSwgData.omData.GetSize();
	CMapStringToPtr olSteMap;
	int ilSteCount =  ogSteData.omData.GetSize();


	//Get all Functions **************************************************
	CMapStringToPtr olSpfMap;
	// Anzahl der Funktion ect. ermitteln.
	int ilSpfCount =  ogSpfData.omData.GetSize();
	
	for(int iSpf=0; iSpf<ilSpfCount;iSpf++)
	{
		olSurn.Format("%ld",ogSpfData.omData[iSpf].Surn);
		//CString olVpto = ogSpfData.omData[iSpf].Vpto.Format("%Y%m%d");
		//CString olVpfr = ogSpfData.omData[iSpf].Vpfr.Format("%Y%m%d");

		if(olSpfMap.Lookup(olSurn,(void *&)prlSurn) == TRUE)
		{
			*prlSurn +=  "," + (CString)ogSpfData.omData[iSpf].Fctc;
		}
		else
		{
			CString *polCode = new CString;
			*polCode = (CString)ogSpfData.omData[iSpf].Fctc;
			olSpfMap.SetAt(olSurn,polCode);
		}
	}
	//END of Get all Functions *******************************************

	
	//Get all Work Groups
	for(int iSwg=0; iSwg<ilSwgCount;iSwg++)
	{
		olSurn.Format("%ld",ogSwgData.omData[iSwg].Surn);
		CString olVpto = ogSwgData.omData[iSwg].Vpto.Format("%Y%m%d");
		CString olVpfr = ogSwgData.omData[iSwg].Vpfr.Format("%Y%m%d");
		if(olSwgMap.Lookup(olSurn,(void *&)prlSurn) == TRUE)
		{
			*prlSurn +=  "," + (CString)ogSwgData.omData[iSwg].Code;
		}
		else
		{
			CString *polCode = new CString;
			*polCode = (CString)ogSwgData.omData[iSwg].Code;
			olSwgMap.SetAt(olSurn,polCode);
		}
	}
	//END of Get all Work Groups
	//Get all Car Pools
	for(int iSte=0; iSte<ilSteCount;iSte++)
	{
		olSurn.Format("%ld",ogSteData.omData[iSte].Surn);
		CString olVpto = ogSteData.omData[iSte].Vpto.Format("%Y%m%d");
		CString olVpfr = ogSteData.omData[iSte].Vpfr.Format("%Y%m%d");
		if(olSteMap.Lookup(olSurn,(void *&)prlSurn) == TRUE)
		{
			*prlSurn +=  "," + (CString)ogSteData.omData[iSte].Code;
		}
		else
		{
			CString *polCode = new CString;
			*polCode = (CString)ogSteData.omData[iSte].Code;
			olSteMap.SetAt(olSurn,polCode);
		}
	}
	//END of Get all Car Pools
	int ilCount =  ogStfData.omData.GetSize();
	AWSTFVIEWDATA_EMPL *prlStfView = NULL;
	for(int i = 0; i < ilCount; i++)
	{
		prlStfView = new AWSTFVIEWDATA_EMPL;
		prlStfView->Peno = ogStfData.omData[i].Peno;
		prlStfView->Perc = ogStfData.omData[i].Perc;
		prlStfView->Finm = ogStfData.omData[i].Finm;
		prlStfView->Lanm = ogStfData.omData[i].Lanm;
		prlStfView->Makr = ogStfData.omData[i].Makr;
		prlStfView->Dodm = ogStfData.omData[i].Dodm;
		prlStfView->Doem = ogStfData.omData[i].Doem;
		CString olUrno;
		olUrno.Format("%ld",ogStfData.omData[i].Urno);
		prlStfView->Urno = olUrno;

		CString *prlSurn = NULL;
		CString olSurn = olUrno;
		// Wenn die Urno des aktuellen mitarbeiters in der MAP gefunden wurde 
		if(olSpfMap.Lookup(olSurn,(void *&)prlSurn) == TRUE)
		{
			// in prlSurn sind die Ergebnisse eingetragen
			prlStfView->Prmc = SortItemList(*prlSurn,',');
		}
		if(olSwgMap.Lookup(olSurn,(void *&)prlSurn) == TRUE)
		{
			prlStfView->Wgpc = SortItemList(*prlSurn,',');
		}
		if(olSteMap.Lookup(olSurn,(void *&)prlSurn) == TRUE)
		{
			prlStfView->Fgmc = SortItemList(*prlSurn,',');
		}

		omStfLines.Add(prlStfView);
	}

	//Delete Maps
	CString *prlString;
	CString olText;
	for(POSITION olPos = olSpfMap.GetStartPosition(); olPos != NULL; )
	{
		olSpfMap.GetNextAssoc(olPos,olText,(void*&)prlString);
		delete prlString;
	}
	olSpfMap.RemoveAll();
	for(olPos = olSwgMap.GetStartPosition(); olPos != NULL; )
	{
		olSwgMap.GetNextAssoc(olPos,olText,(void*&)prlString);
		delete prlString;
	}
	olSwgMap.RemoveAll();
	for(olPos = olSteMap.GetStartPosition(); olPos != NULL; )
	{
		olSteMap.GetNextAssoc(olPos,olText,(void*&)prlString);
		delete prlString;
	}
	olSteMap.RemoveAll();
	//END - Delete Maps

	// Und alles sortieren und damit auch darstellen
	Sort(SRT_LNAME_U);

	// Sollte eine URNO �bergeben worden sein, wird diese selektiert.
	if (omSelectUrno != "")
	{
		// Urno suchen und selektieren
		for(int i = 0; i < omStfLines.GetSize(); i++)
		{
			if (omStfLines[i].Urno == omSelectUrno)
			{
				// Selektieren und sichbar machen
				m_LB_List.SetSel(i);
				m_LB_List.SetTopIndex(i);
				break;
			}
		}
	}

	return TRUE; 
}

//**********************************************************************************
// 
//**********************************************************************************

void EmplDlg::Sort(int ipSort) 
{
	CString olLine,olTmpTxt,olUrno;

	// Alle "Pfeile" l�schen.
	m_B_1.SetWindowText(LoadStg(SHIFT_STF_PERC));
	m_B_2.SetWindowText(LoadStg(SHIFT_STF_NAME));
	m_B_3.SetWindowText(LoadStg(SHIFT_STF_FUNCTION));
	m_B_4.SetWindowText(LoadStg(SHIFT_STF_WGRP));
	m_B_5.SetWindowText(LoadStg(SHIFT_STF_CPOOL));
	m_B_6.SetWindowText(LoadStg(IDS_STRING139));
	m_B_7.SetWindowText(LoadStg(IDS_STRING140));
	m_B_8.SetWindowText(LoadStg(SHIFT_STF_PENO));
	
	// Daten sortieren.
	switch(ipSort)
	{
		case SRT_SNAME_U:
			m_B_1.SetWindowText(LoadStg(SHIFT_STF_PERC) + omUp);
			omStfLines.Sort(Compare_SNAME_U);
			break;
		case SRT_LNAME_U:
			m_B_2.SetWindowText(LoadStg(SHIFT_STF_NAME) + omUp);
			omStfLines.Sort(Compare_LNAME_U);
			break;
		case SRT_FUNC_U:
			m_B_3.SetWindowText(LoadStg(SHIFT_STF_FUNCTION) + omUp);
			omStfLines.Sort(Compare_FUNC_U);
			break;
		case SRT_WGRP_U:
			m_B_4.SetWindowText(LoadStg(SHIFT_STF_WGRP) + omUp);
			omStfLines.Sort(Compare_WGRP_U);
			break;
		case SRT_CPOOL_U:
			m_B_5.SetWindowText(LoadStg(SHIFT_STF_CPOOL) + omUp);
			omStfLines.Sort(Compare_CPOOL_U);
			break;
		case SRT_BEGINN_U:
			m_B_6.SetWindowText(LoadStg(IDS_STRING139) + omUp);
			omStfLines.Sort(Compare_BEGINN_U);
			break;
		case SRT_END_U:
			m_B_7.SetWindowText(LoadStg(IDS_STRING140) + omUp);
			omStfLines.Sort(Compare_END_U);
			break;
		case SRT_PNUM_U:
			m_B_8.SetWindowText(LoadStg(SHIFT_STF_PENO) + omUp);
			omStfLines.Sort(Compare_PNUM_U);
			break;
		case SRT_SNAME_D:
			m_B_1.SetWindowText(LoadStg(SHIFT_STF_PERC) + omDown);
			omStfLines.Sort(Compare_SNAME_D);
			break;
		case SRT_LNAME_D:
			m_B_2.SetWindowText(LoadStg(SHIFT_STF_NAME) + omDown);
			omStfLines.Sort(Compare_LNAME_D);
			break;
		case SRT_FUNC_D:
			m_B_3.SetWindowText(LoadStg(SHIFT_STF_FUNCTION) + omDown);
			omStfLines.Sort(Compare_FUNC_D);
			break;
		case SRT_WGRP_D:
			m_B_4.SetWindowText(LoadStg(SHIFT_STF_WGRP) + omDown);
			omStfLines.Sort(Compare_WGRP_D);
			break;
		case SRT_CPOOL_D:
			m_B_5.SetWindowText(LoadStg(SHIFT_STF_CPOOL) + omDown);
			omStfLines.Sort(Compare_CPOOL_D);
			break;
		case SRT_BEGINN_D:
			m_B_6.SetWindowText(LoadStg(IDS_STRING139) + omDown);
			omStfLines.Sort(Compare_BEGINN_D);
			break;
		case SRT_END_D:
			m_B_7.SetWindowText(LoadStg(IDS_STRING140) + omDown);
			omStfLines.Sort(Compare_END_D);
			break;
		case SRT_PNUM_D:
			m_B_8.SetWindowText(LoadStg(SHIFT_STF_PENO) + omDown);
			omStfLines.Sort(Compare_PNUM_D);
			break;
	}
	// Daten darstellen
	DisplayData();
}

//**********************************************************************************
// 
//**********************************************************************************

void EmplDlg::DisplayData() 
{
	int			ilCount;
	CString		olTmpTxt;
	CString		olLine;

	ilCount = omStfLines.GetSize();
	m_LB_List.LockWindowUpdate();
	m_LB_List.ResetContent();

	COLORREF olColor;
	olColor = RED;

	// Warte Dialog einblenden.
	CWaitDlg	olWaitDlg(this,LoadStg(WAIT_DATA_PREPARE));

	// Datenreihen in Listbox einf�gen.
	for (int i = 0; i < ilCount; i++)
	{
		olTmpTxt.Format("%s, %s",omStfLines[i].Lanm,omStfLines[i].Finm);
		olLine.Format("%s  %-36s %-15s %-12s %-9s %-10s-%-10s %16s       URNO=%s",omStfLines[i].Peno.Left(16), omStfLines[i].Perc.Left(5), olTmpTxt.Left(36), omStfLines[i].Prmc.Left(15), omStfLines[i].Wgpc.Left(12), omStfLines[i].Fgmc.Left(9), omStfLines[i].Doem.Format("%d.%m.%Y"), omStfLines[i].Dodm.Format("%d.%m.%Y"), omStfLines[i].Urno);

		// Sollt ein Eintrag mit dieser URNO �bergeben worden sein,
		// Eintrag rot darstellen.
		if(pomLocalUrnoList->Find(omStfLines[i].Urno) != NULL)
			m_LB_List.AddString(olLine,olColor); //RED
		else
			m_LB_List.AddString(olLine);
	}

	m_LB_List.UnlockWindowUpdate();
	m_LB_List.SetCaretIndex(0);
}

//**********************************************************************************
// 
//**********************************************************************************

void EmplDlg::OnOK() 
{
	POSITION pos;

	// Lokale URNOListe in �bergebende Liste kopieren
	pomUrnoList->RemoveAll();
	for( pos = pomLocalUrnoList->GetHeadPosition(); pos != NULL; )
	{
		pomUrnoList->AddTail(pomLocalUrnoList->GetNext(pos));
	}
	
	CDialog::OnOK();
}

//**********************************************************************************
// 
//**********************************************************************************

void EmplDlg::OnBInsert() 
{
	CString olUrno;
	int ilMaxItems;
	// Wieveile Eintr�ge wurden Ausgew�hlt
	ilMaxItems = m_LB_List.GetSelCount();
	// Speicherplatz bereitstellen
	int *pilIndex = new int [ilMaxItems];

	// Anzahl der ausgew�hlten Objekte bestimmen
	m_LB_List.GetSelItems(ilMaxItems, pilIndex );

	// Sollten mehr als 90 Eintr�ge ausgw�hlt worden sein wird eine Fehlermeldung
	// ausgegeben
	if(ilMaxItems<90)
	{
		CString olTmpTxt,olUrno;
		for(int i=0; i<ilMaxItems; i++)
		{
			m_LB_List.GetText(pilIndex[i],olTmpTxt);
			olUrno = olTmpTxt.Mid(olTmpTxt.Find("URNO=")+5);
			//MessageBox("SelUrno:"+olUrno);
			// Sollte der die URNO noch nicht vorhanden sein, Urno zu TEMP
			// Liste hinzuf�gen
			if(pomLocalUrnoList->Find(olUrno) == NULL)
				pomLocalUrnoList->AddTail(olUrno);
		}
	// Anzeige aktualisieren.
	DisplayData();
	}
	else
	{
		CString olTxt;
		olTxt.Format(LoadStg(IDS_STRING1773), 90);
		Beep(440,70);
		MessageBox(olTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
	}
	delete pilIndex;
}

//**********************************************************************************
// 
//**********************************************************************************

void EmplDlg::OnBDelete() 
{
	POSITION olPos;

	// Wieveile Eintr�ge wurden Ausgew�hlt
	int ilMaxItems = m_LB_List.GetSelCount();

	// Speicherplatz bereitstellen
	int *pilIndex = new int [ilMaxItems];

	// Anzahl der ausgew�hlten Objekte bestimmen
	m_LB_List.GetSelItems(ilMaxItems, pilIndex );

	// Sollten mehr als 90 Eintr�ge ausgw�hlt worden sein wird eine Fehlermeldung ausgegeben
	if (ilMaxItems < 90)
	{
		CString olTmpTxt,olUrno;
		for (int i = 0; i < ilMaxItems; i++)
		{
			m_LB_List.GetText(pilIndex[i],olTmpTxt);
			olUrno = olTmpTxt.Mid(olTmpTxt.Find("URNO=")+5);

			// Sollte die URNO vorhanden sein, Urno aus der TEMP-Liste l�schen
			olPos = pomLocalUrnoList->Find(olUrno);

			// Urno l�schen 
			if(olPos!= NULL)
			{
				pomLocalUrnoList->RemoveAt(olPos);
			}
		}
		// Anzeige aktualisieren.
		DisplayData();
	}
	else
	{
		CString olTxt;
		olTxt.Format(LoadStg(IDS_STRING1773), 90);
		Beep(440,70);
		MessageBox(olTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
	}
	delete pilIndex;
}

//**********************************************************************************
// 
//**********************************************************************************

void EmplDlg::OnButton1() 
{
	if(imB1 == SRT_UP)
	{
		imB1 = SRT_DOWN;
		Sort(SRT_SNAME_D);
	}
	else
	{
		imB1 = SRT_UP;
		Sort(SRT_SNAME_U);
	}
}

//**********************************************************************************
// 
//**********************************************************************************

void EmplDlg::OnButton2() 
{
	if(imB2 == SRT_UP)
	{
		imB2 = SRT_DOWN;
		Sort(SRT_LNAME_D);
	}
	else
	{
		imB2 = SRT_UP;
		Sort(SRT_LNAME_U);
	}
}

//**********************************************************************************
// 
//**********************************************************************************

void EmplDlg::OnButton3() 
{
	if(imB3 == SRT_UP)
	{
		imB3 = SRT_DOWN;
		Sort(SRT_FUNC_D);
	}
	else
	{
		imB3 = SRT_UP;
		Sort(SRT_FUNC_U);
	}
}

//**********************************************************************************
// 
//**********************************************************************************

void EmplDlg::OnButton4() 
{
	if(imB4 == SRT_UP)
	{
		imB4 = SRT_DOWN;
		Sort(SRT_WGRP_D);
	}
	else
	{
		imB4 = SRT_UP;
		Sort(SRT_WGRP_U);
	}
}

//**********************************************************************************
// 
//**********************************************************************************

void EmplDlg::OnButton5() 
{
	if(imB5 == SRT_UP)
	{
		imB5 = SRT_DOWN;
		Sort(SRT_CPOOL_D);
	}
	else
	{
		imB5 = SRT_UP;
		Sort(SRT_CPOOL_U);
	}
}

//**********************************************************************************
// 
//**********************************************************************************

void EmplDlg::OnButton6() 
{
	if(imB7 == SRT_UP)
	{
		imB7 = SRT_DOWN;
		Sort(SRT_BEGINN_D);
	}
	else
	{
		imB7 = SRT_UP;
		Sort(SRT_BEGINN_U);
	}
}

//**********************************************************************************
// 
//**********************************************************************************

void EmplDlg::OnButton7() 
{
	if(imB8 == SRT_UP)
	{
		imB8 = SRT_DOWN;
		Sort(SRT_END_D);
	}
	else
	{
		imB8 = SRT_UP;
		Sort(SRT_END_U);
	}
}

//**********************************************************************************
// 
//**********************************************************************************

void EmplDlg::OnButton8() 
{
	if(imB6 == SRT_UP)
	{
		imB6 = SRT_DOWN;
		Sort(SRT_PNUM_D);
	}
	else
	{
		imB6 = SRT_UP;
		Sort(SRT_PNUM_U);
	}
}

//**********************************************************************************
// 
//**********************************************************************************//---------------------------------------------------------------------------

void EmplDlg::OnButton9() 
{

}


//**********************************************************************************
// COMPARE ROUTINES
//**********************************************************************************

static int Compare_SNAME_U( const AWSTFVIEWDATA_EMPL **e1, const AWSTFVIEWDATA_EMPL **e2)
{
	CString ole1,ole2;
	ole1 = (**e1).Perc;
	ole2 = (**e2).Perc;
	return (ole1.Collate(ole2));
}
static int Compare_LNAME_U( const AWSTFVIEWDATA_EMPL **e1, const AWSTFVIEWDATA_EMPL **e2)
{
	CString ole1,ole2;
	ole1 = (**e1).Lanm;
	ole2 = (**e2).Lanm;
	int ilCompareResult;
	ilCompareResult = ole1.Collate(ole2);
	if(ilCompareResult == 0)
	{
		ole1 = (**e1).Finm;
		ole2 = (**e2).Finm;
		ilCompareResult = ole1.Collate(ole2);
	}
	return ilCompareResult;
}
static int Compare_FUNC_U( const AWSTFVIEWDATA_EMPL **e1, const AWSTFVIEWDATA_EMPL **e2)
{
	CString ole1,ole2;
	int ilCompareResult;
	ole1 = (**e1).Prmc;
	ole2 = (**e2).Prmc;
	ilCompareResult = ole1.Collate(ole2);
	if(ilCompareResult == 0)
	{
		ole1 = (**e1).Lanm;
		ole2 = (**e2).Lanm;
		ilCompareResult = ole1.Collate(ole2);
	}
	if(ilCompareResult == 0)
	{
		ole1 = (**e1).Finm;
		ole2 = (**e2).Finm;
		ilCompareResult = ole1.Collate(ole2);
	}
	return ilCompareResult;

}
static int Compare_WGRP_U( const AWSTFVIEWDATA_EMPL **e1, const AWSTFVIEWDATA_EMPL **e2)
{
	CString ole1,ole2;
	int ilCompareResult;
	ole1 = (**e1).Wgpc;
	ole2 = (**e2).Wgpc;
	ilCompareResult = ole1.Collate(ole2);
	if(ilCompareResult == 0)
	{
		ole1 = (**e1).Lanm;
		ole2 = (**e2).Lanm;
		ilCompareResult = ole1.Collate(ole2);
	}
	if(ilCompareResult == 0)
	{
		ole1 = (**e1).Finm;
		ole2 = (**e2).Finm;
		ilCompareResult = ole1.Collate(ole2);
	}
	return ilCompareResult;
}
static int Compare_CPOOL_U( const AWSTFVIEWDATA_EMPL **e1, const AWSTFVIEWDATA_EMPL **e2)
{
	CString ole1,ole2;
	int ilCompareResult;
	ole1 = (**e1).Fgmc;
	ole2 = (**e2).Fgmc;
	ilCompareResult = ole1.Collate(ole2);
	if(ilCompareResult == 0)
	{
		ole1 = (**e1).Lanm;
		ole2 = (**e2).Lanm;
		ilCompareResult = ole1.Collate(ole2);
	}
	if(ilCompareResult == 0)
	{
		ole1 = (**e1).Finm;
		ole2 = (**e2).Finm;
		ilCompareResult = ole1.Collate(ole2);
	}
	return ilCompareResult;
}
static int Compare_BEGINN_U( const AWSTFVIEWDATA_EMPL **e1, const AWSTFVIEWDATA_EMPL **e2)
{
	CString ole1,ole2;
	int ilCompareResult;
	ole1 = (**e1).Doem.Format("%Y%m%d");
	ole2 = (**e2).Doem.Format("%Y%m%d");
	ilCompareResult = ole1.Collate(ole2);
	if(ilCompareResult == 0)
	{
		ole1 = (**e1).Lanm;
		ole2 = (**e2).Lanm;
		ilCompareResult = ole1.Collate(ole2);
	}
	if(ilCompareResult == 0)
	{
		ole1 = (**e1).Finm;
		ole2 = (**e2).Finm;
		ilCompareResult = ole1.Collate(ole2);
	}
	return ilCompareResult;
}
static int Compare_END_U( const AWSTFVIEWDATA_EMPL **e1, const AWSTFVIEWDATA_EMPL **e2)
{
	CString ole1,ole2;
	int ilCompareResult;
	ole1 = (**e1).Dodm.Format("%Y%m%d");
	ole2 = (**e2).Dodm.Format("%Y%m%d");
	ilCompareResult = ole1.Collate(ole2);
	if(ilCompareResult == 0)
	{
		ole1 = (**e1).Lanm;
		ole2 = (**e2).Lanm;
		ilCompareResult = ole1.Collate(ole2);
	}
	if(ilCompareResult == 0)
	{
		ole1 = (**e1).Finm;
		ole2 = (**e2).Finm;
		ilCompareResult = ole1.Collate(ole2);
	}
	return ilCompareResult;
}
static int Compare_PNUM_U( const AWSTFVIEWDATA_EMPL **e1, const AWSTFVIEWDATA_EMPL **e2)
{
	CString ole1,ole2;
	ole1 = (**e1).Peno;
	ole2 = (**e2).Peno;
	return (ole1.Collate(ole2));
}
static int Compare_SNAME_D( const AWSTFVIEWDATA_EMPL **e1, const AWSTFVIEWDATA_EMPL **e2)
{
	CString ole1,ole2;
	ole1 = (**e1).Perc;
	ole2 = (**e2).Perc;
	return (ole2.Collate(ole1));
}
static int Compare_LNAME_D( const AWSTFVIEWDATA_EMPL **e1, const AWSTFVIEWDATA_EMPL **e2)
{
	CString ole1,ole2;
	ole1 = (**e1).Lanm;
	ole2 = (**e2).Lanm;
	int ilCompareResult;
	ilCompareResult = ole2.Collate(ole1);
	if(ilCompareResult == 0)
	{
		ole1 = (**e1).Finm;
		ole2 = (**e2).Finm;
		ilCompareResult = ole2.Collate(ole1);
	}
	return ilCompareResult;
}
static int Compare_FUNC_D( const AWSTFVIEWDATA_EMPL **e1, const AWSTFVIEWDATA_EMPL **e2)
{
	CString ole1,ole2;
	int ilCompareResult;
	ole1 = (**e1).Prmc;
	ole2 = (**e2).Prmc;
	ilCompareResult = ole2.Collate(ole1);
	if(ilCompareResult == 0)
	{
		ole1 = (**e1).Lanm;
		ole2 = (**e2).Lanm;
		ilCompareResult = ole2.Collate(ole1);
	}
	if(ilCompareResult == 0)
	{
		ole1 = (**e1).Finm;
		ole2 = (**e2).Finm;
		ilCompareResult = ole2.Collate(ole1);
	}
	return ilCompareResult;
}
static int Compare_WGRP_D( const AWSTFVIEWDATA_EMPL **e1, const AWSTFVIEWDATA_EMPL **e2)
{
	CString ole1,ole2;
	int ilCompareResult;
	ole1 = (**e1).Wgpc;
	ole2 = (**e2).Wgpc;
	ilCompareResult = ole2.Collate(ole1);
	if(ilCompareResult == 0)
	{
		ole1 = (**e1).Lanm;
		ole2 = (**e2).Lanm;
		ilCompareResult = ole2.Collate(ole1);
	}
	if(ilCompareResult == 0)
	{
		ole1 = (**e1).Finm;
		ole2 = (**e2).Finm;
		ilCompareResult = ole2.Collate(ole1);
	}
	return ilCompareResult;
}
static int Compare_CPOOL_D( const AWSTFVIEWDATA_EMPL **e1, const AWSTFVIEWDATA_EMPL **e2)
{
	CString ole1,ole2;
	int ilCompareResult;
	ole1 = (**e1).Fgmc;
	ole2 = (**e2).Fgmc;
	ilCompareResult = ole2.Collate(ole1);
	if(ilCompareResult == 0)
	{
		ole1 = (**e1).Lanm;
		ole2 = (**e2).Lanm;
		ilCompareResult = ole2.Collate(ole1);
	}
	if(ilCompareResult == 0)
	{
		ole1 = (**e1).Finm;
		ole2 = (**e2).Finm;
		ilCompareResult = ole2.Collate(ole1);
	}
	return ilCompareResult;
}
static int Compare_BEGINN_D( const AWSTFVIEWDATA_EMPL **e1, const AWSTFVIEWDATA_EMPL **e2)
{
	CString ole1,ole2;
	int ilCompareResult;
	ole1 = (**e1).Doem.Format("%Y%m%d");
	ole2 = (**e2).Doem.Format("%Y%m%d");
	ilCompareResult = ole2.Collate(ole1);
	if(ilCompareResult == 0)
	{
		ole1 = (**e1).Lanm;
		ole2 = (**e2).Lanm;
		ilCompareResult = ole2.Collate(ole1);
	}
	if(ilCompareResult == 0)
	{
		ole1 = (**e1).Finm;
		ole2 = (**e2).Finm;
		ilCompareResult = ole2.Collate(ole1);
	}
	return ilCompareResult;
}
static int Compare_END_D( const AWSTFVIEWDATA_EMPL **e1, const AWSTFVIEWDATA_EMPL **e2)
{
	CString ole1,ole2;
	int ilCompareResult;
	ole1 = (**e1).Dodm.Format("%Y%m%d");
	ole2 = (**e2).Dodm.Format("%Y%m%d");
	ilCompareResult = ole2.Collate(ole1);
	if(ilCompareResult == 0)
	{
		ole1 = (**e1).Lanm;
		ole2 = (**e2).Lanm;
		ilCompareResult = ole2.Collate(ole1);
	}
	if(ilCompareResult == 0)
	{
		ole1 = (**e1).Finm;
		ole2 = (**e2).Finm;
		ilCompareResult = ole2.Collate(ole1);
	}
	return ilCompareResult;
}
static int Compare_PNUM_D( const AWSTFVIEWDATA_EMPL **e1, const AWSTFVIEWDATA_EMPL **e2)
{
	CString ole1,ole2;
	ole1 = (**e1).Peno;
	ole2 = (**e2).Peno;
	return (ole2.Collate(ole1));
}

/*static int Compare_GRUPP_U( const AWDBOVIEWDATA_EMPL **e1, const AWDBOVIEWDATA_EMPL **e2)
{
	CString ole1,ole2;
	ole1 = (**e1).Grpn;
	ole2 = (**e2).Grpn;
	return (ole1.Collate(ole2));
}*/
/*static int Compare_MEMBERS_U( const AWDBOVIEWDATA_EMPL **e1, const AWDBOVIEWDATA_EMPL **e2)
{
	CString ole1,ole2;
	ole1 = (**e1).Valu;
	ole2 = (**e2).Valu;
	return (ole1.Collate(ole2));
}*/
/*static int Compare_GRUPP_D( const AWDBOVIEWDATA_EMPL **e1, const AWDBOVIEWDATA_EMPL **e2)
{
	CString ole1,ole2;
	ole1 = (**e1).Grpn;
	ole2 = (**e2).Grpn;
	return (ole2.Collate(ole1));
}*/
/*static int Compare_MEMBERS_D( const AWDBOVIEWDATA_EMPL **e1, const AWDBOVIEWDATA_EMPL **e2)
{
	CString ole1,ole2;
	ole1 = (**e1).Valu;
	ole2 = (**e2).Valu;
	return (ole2.Collate(ole1));
}*/

