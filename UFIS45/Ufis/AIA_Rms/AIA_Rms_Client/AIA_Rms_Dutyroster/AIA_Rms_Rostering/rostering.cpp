// Rostering.cpp : Defines the class behaviors for the application.
//
//////////////////////////////////////////////////////////////////////////////////
//
//		HISTORY 
//
//		rdr		Jan.2k	GUI-Multilanguagesupport added
//
//////////////////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <AatHelp.h>
#include <AatLogin.h>
#include <PrivList.h>
#include <CedaInitModuData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//****************************************************************************
// CRosteringApp
//****************************************************************************

BEGIN_MESSAGE_MAP(CRosteringApp, CWinApp)
	//{{AFX_MSG_MAP(CRosteringApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	// NOTE - the ClassWizard will add and remove mapping macros here.
	//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

//const CLSID CLSID_UfisAppMgr = {0x7DC4A830,0x47BB,0x11D6,{0x80,0x52,0x00,0x01,0x02,0x22,0x05,0xE4}};

//****************************************************************************
// CRosteringApp construction
//****************************************************************************

CRosteringApp::CRosteringApp()
{
	
}

//****************************************************************************
// CRosteringApp destruction
//****************************************************************************

CRosteringApp::~CRosteringApp()
{
	//ogUfisAppMng.DetachDispatch();
	delete CGUILng::TheOne();
	delete pogNameConfigurationDlg;
	AatHelp::ExitApp();
}

//****************************************************************************
// The one and only CRosteringApp object
//****************************************************************************

CRosteringApp theApp;

//****************************************************************************
// CRosteringApp initialization
//****************************************************************************

BOOL CRosteringApp::InitInstance()
{
	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}

	AfxEnableControlContainer();

	// Initialize StingrayGrid
	GXInit();
	if (!AfxSocketInit())
	{
		AfxMessageBox("IDP_SOCKETS_INIT_FAILED");
		return FALSE;
	}

	// Standard initialization
	SetDialogBkColor();        // Set dialog background color to gray

	// Initialize help
	if (!AatHelp::Initialize(ogAppName))
	{
		CString olErrorMsg;
		AatHelp::GetLastError(olErrorMsg);
		MessageBox(NULL,olErrorMsg,"Error",MB_OK);
	}

	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need.
#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

//How to turn off messages sent by MFC to Debug window?
//see "http://www.pocketpcdn.com/qa/debug_messages.html"
#ifdef _DEBUG

   afxTraceFlags = 0;

   // *** UNCOMMENT NEXT LINE if you want multi-app debugging ***
   // afxTraceFlags |= traceMultiApp;

   // *** UNCOMMENT NEXT LINE if you want main message pump trace (includes DDE) ***
   // afxTraceFlags |= traceAppMsg;

   // *** UNCOMMENT NEXT LINE if you want Windows message tracing ***
   // afxTraceFlags |= traceWinMsg;

   // *** UNCOMMENT NEXT LINE if you want Windows command routing trace ***
   // afxTraceFlags |= traceCmdRouting;

   // *** UNCOMMENT NEXT LINE if you want special OLE callback trace ***
   // afxTraceFlags |= traceOle;

   // *** UNCOMMENT NEXT LINE if you want special database trace ***
   // afxTraceFlags |= traceDatabase;

   // *** UNCOMMENT NEXT LINE if you want special Internet client trace ***
   // afxTraceFlags |= traceInternet;

#endif /* DEBUG */

   if (getenv("CEDA") == NULL)
	{
        strcpy(pcgConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI");
	}
    else
	{
        strcpy(pcgConfigPath, getenv("CEDA"));
	}

    GetPrivateProfileString("GLOBAL", "HOMEAIRPORT", "TAB", pcgHome, sizeof pcgHome, pcgConfigPath);
    GetPrivateProfileString("GLOBAL", "TABLEEXTENSION", "TAB", pcgTableExt, sizeof pcgTableExt, pcgConfigPath);

	GetPrivateProfileString(pcgAppName, "GROUPING", "DEFAULT",pcgGroupingPath, sizeof pcgGroupingPath, pcgConfigPath);
	GetPrivateProfileString(pcgAppName, "BDPS-UIF", "DEFAULT",pcgStammdatenPath, sizeof pcgStammdatenPath, pcgConfigPath);
	GetPrivateProfileString(pcgAppName, "STAFFBALANCE", "DEFAULT",pcgStaffBalancePath, sizeof pcgStaffBalancePath, pcgConfigPath);
	GetPrivateProfileString(pcgAppName, "INITACCOUNT", "DEFAULT",pcgInitAccountPath, sizeof pcgInitAccountPath, pcgConfigPath);
	GetPrivateProfileString(pcgAppName, "ABSPLAN", "DEFAULT",pcgAbsencePlanningPath, sizeof pcgAbsencePlanningPath, pcgConfigPath);
	GetPrivateProfileString(pcgAppName, "LOGFILE", "DEFAULT",pcgLogFilePath, sizeof pcgLogFilePath, pcgConfigPath);
	GetPrivateProfileString(pcgAppName, "ROSTERINGPRINT", "DEFAULT",pcgRosteringPrintPath, sizeof pcgRosteringPrintPath, pcgConfigPath);
	GetPrivateProfileString(pcgAppName, "NAME_CONFIGURATION_DUTYROSTER", "",pcgDefaultNameConfig_Duty, sizeof pcgDefaultNameConfig_Duty, pcgConfigPath);
	GetPrivateProfileString(pcgAppName, "NAME_CONFIGURATION_SHIFTROSTER", "",pcgDefaultNameConfig_Shift, sizeof pcgDefaultNameConfig_Shift, pcgConfigPath);

	char pcgEnableMonthlyAllowance[10];
	char *cpCopy;
	GetPrivateProfileString(pcgAppName, "ENABLE_MONTHLY_ALLOWANCE", "FALSE", pcgEnableMonthlyAllowance, sizeof pcgEnableMonthlyAllowance, pcgConfigPath);
	cpCopy = _strupr (_strdup (pcgEnableMonthlyAllowance));
	if (strcmp(cpCopy, "TRUE") == 0)
	{
		bgEnableMonthlyAllowance = true;
	}
	else
	{
		bgEnableMonthlyAllowance = false;
	}
	free (cpCopy);

	char pcgScrollToNewShift[10];
	GetPrivateProfileString(pcgAppName, "SCROLL_TO_NEW_SHIFT", "FALSE", pcgScrollToNewShift, sizeof pcgScrollToNewShift, pcgConfigPath);
	cpCopy = _strupr (_strdup (pcgScrollToNewShift));
	if (strcmp(cpCopy, "TRUE") == 0)
	{
		bgScrollToNewShiftLine = true;
	}
	else
	{
		bgScrollToNewShiftLine = false;
	}
	free (cpCopy);

	char pcgPrintDutyrosterShiftcode[10];
	GetPrivateProfileString(pcgAppName, "PRINT_DUTYROSTER_SHIFTCODE", "FALSE", pcgPrintDutyrosterShiftcode, sizeof pcgPrintDutyrosterShiftcode, pcgConfigPath);
	cpCopy = _strupr (_strdup (pcgPrintDutyrosterShiftcode));
	if (strcmp(cpCopy, "TRUE") == 0)
	{
		bgPrintDutyrosterShiftcode = true;
	}
	else
	{
		bgPrintDutyrosterShiftcode = false;
	}
	free (cpCopy);
	char pcgPrintShiftrosterShiftcode[10];
	GetPrivateProfileString(pcgAppName, "PRINT_SHIFTROSTER_SHIFTCODE", "FALSE", pcgPrintShiftrosterShiftcode, sizeof pcgPrintShiftrosterShiftcode, pcgConfigPath);
	cpCopy = _strupr (_strdup (pcgPrintShiftrosterShiftcode));
	if (strcmp(cpCopy, "TRUE") == 0)
	{
		bgPrintShiftrosterShiftcode = true;
	}
	else
	{
		bgPrintShiftrosterShiftcode = false;
	}
	free (cpCopy);
	

	// Errlog Path vorbereiten
	CString olLog(pcgLogFilePath);
	int ilDot = olLog.ReverseFind('.');
	if(ilDot == -1) ilDot = olLog.GetLength()-1;
	olLog.Insert(ilDot, "_ERRORS");
	strcpy(pcgErrlogFilePath, (LPCTSTR)olLog);

#ifdef _DEBUG
	if(strcmp(pcgLogFilePath, "DEFAULT"))
	{
		// in Debug-Version erstellen wir Logfile mit Datumsteil
		COleDateTime olDate;
		olDate = COleDateTime::GetCurrentTime();
		CString olNewLog = pcgLogFilePath;
		olNewLog.Insert(olNewLog.Find('.'),olDate.Format("%Y%m%d_%H%M"));

		strcpy(pcgLogFilePath, (LPCTSTR)olNewLog);
	}
#endif _DEBUG
	
	// LOGSTATUS =  "FULL" / "TRACE" / "OFF"
	char pclLogFileStatus[256];
	GetPrivateProfileString(pcgAppName, "LOGSTATUS", "OFF",pclLogFileStatus, sizeof(pclLogFileStatus), pcgConfigPath);
	if(!strcmp(pclLogFileStatus, "TRACE"))
	{
		lgLogFileStatus = LOGFILE_TRACE;
	}
	else if(!strcmp(pclLogFileStatus, "FULL"))
	{
		lgLogFileStatus = LOGFILE_FULL;
	}
	else
	{
		lgLogFileStatus = LOGFILE_OFF;
	}

	// CedaBasicData Objekt initialisieren
	ogBCD.SetTableExtension(CString(pcgTableExt));
	ogBCD.SetHomeAirport(CString(pcgHome));

	strcpy(CCSCedaData::pcmTableExt, pcgTableExt);
	strcpy(CCSCedaData::pcmApplName, pcgAppName);
	strcpy(CCSCedaData::pcmHomeAirport, pcgHome);

	// MDI: Akivierung der DocTemplates und Zuordung der Views und Child+mes ShiftRoster
	pomShiftRosterTemplate = new CMultiDocTemplate(
		IDR_ROSTERTYPE,
		RUNTIME_CLASS(CRosteringDoc),		// Dokument f�r beide Views
		RUNTIME_CLASS(ShiftRoster_Frm),		// custom MDI child frame
		RUNTIME_CLASS(ShiftRoster_View));	// View
	AddDocTemplate(pomShiftRosterTemplate);

	// DutyRoster
	pomDutyRosterTemplate = new CMultiDocTemplate(
		IDR_ROSTERTYPE,
		RUNTIME_CLASS(CRosteringDoc),		// Dokument f�r beide Views
		RUNTIME_CLASS(DutyRoster_Frm),		// custom MDI child frame
		RUNTIME_CLASS(DutyRoster_View));	// View
	AddDocTemplate(pomDutyRosterTemplate);

	// Standard Fonts and Brushes initialization
    InitFont();
	CreateBrushes();

	// INIT Tablenames and Homeairport
	strcpy(CCSCedaData::pcmTableExt, pcgTableExt);
	strcpy(CCSCedaData::pcmApplName, ogAppName);

	VersionInfo rlInfo;
	VersionInfo::GetVersionInfo(NULL,rlInfo);	// must use NULL (and it works), because AFX is not initialized yet and will cause an ASSERT
	CCSCedaData::bmVersCheck = true;
	strcpy(CCSCedaData::pcmVersion, rlInfo.omFileVersion);
	strcpy(CCSCedaData::pcmInternalBuild, rlInfo.omPrivateBuild);

	ogCommHandler.SetAppName(ogAppName);

    if(ogCommHandler.Initialize() != true)  // connection error?
    {
        AfxMessageBox(CString("CedaCom ") + ogCommHandler.LastError());
        return FALSE;
    }

	// die globalen Klassen initialisieren
	ogDrdData.Initialize(ogCommHandler.pcmRealHostName); 
	ogOacData.Initialize(ogCommHandler.pcmRealHostName);    
	ogOrgData.Initialize(ogCommHandler.pcmRealHostName); 
	ogSgmData.Initialize(ogCommHandler.pcmRealHostName);  
	ogSgrData.Initialize(ogCommHandler.pcmRealHostName);  
	ogBasicData.Initialize(ogCommHandler.pcmRealHostName);  
	ogAccData.Initialize(ogCommHandler.pcmRealHostName); 
	ogBsdData.Initialize(ogCommHandler.pcmRealHostName); 
	ogCfgData.Initialize(ogCommHandler.pcmRealHostName); 
	ogCotData.Initialize(ogCommHandler.pcmRealHostName); 
	ogDelData.Initialize(ogCommHandler.pcmRealHostName); 
	ogDraData.Initialize(ogCommHandler.pcmRealHostName); 
	ogDrgData.Initialize(ogCommHandler.pcmRealHostName);    
	ogDrrData.Initialize(ogCommHandler.pcmRealHostName); 
	ogDrsData.Initialize(ogCommHandler.pcmRealHostName); 
	ogInitModuData.Initialize(ogCommHandler.pcmRealHostName); 
	ogMsdData.Initialize(ogCommHandler.pcmRealHostName); 
	ogOdaData.Initialize(ogCommHandler.pcmRealHostName); 
	ogPerData.Initialize(ogCommHandler.pcmRealHostName); 
	ogPfcData.Initialize(ogCommHandler.pcmRealHostName); 
	ogPgpData.Initialize(ogCommHandler.pcmRealHostName); 
	ogScoData.Initialize(ogCommHandler.pcmRealHostName); 
	ogSdgData.Initialize(ogCommHandler.pcmRealHostName); 
	ogSdtData.Initialize(ogCommHandler.pcmRealHostName); 
	ogSorData.Initialize(ogCommHandler.pcmRealHostName); 
	ogSpeData.Initialize(ogCommHandler.pcmRealHostName); 
	ogSpfData.Initialize(ogCommHandler.pcmRealHostName); 
	ogSteData.Initialize(ogCommHandler.pcmRealHostName); 
	ogStfData.Initialize(ogCommHandler.pcmRealHostName); 
	ogSwgData.Initialize(ogCommHandler.pcmRealHostName); 
	ogTeaData.Initialize(ogCommHandler.pcmRealHostName); 
	ogWgpData.Initialize(ogCommHandler.pcmRealHostName); 
	ogWisData.Initialize(ogCommHandler.pcmRealHostName);
	ogSprData.Initialize(ogCommHandler.pcmRealHostName);
	ogPrivList.Initialize(ogCommHandler.pcmRealHostName); 
	
	//***********************************************************************************************
	// Initialisierung der GUI-Sprache // RDR 03.01.2000
	//***********************************************************************************************
	// in ceda.ini einf�gen !!!!!!!!
	// ;'DE','US','IT' oder 'DE,Test' etc.
	// LANGUAGE=DE
	char lng[128];
	char dbparam[128];

	CGUILng* ogGUILng = CGUILng::TheOne();

    GetPrivateProfileString("GLOBAL", "LANGUAGE", "", lng, sizeof lng, pcgConfigPath);
    GetPrivateProfileString(ogAppName, "DBParam", "", dbparam, sizeof dbparam, pcgConfigPath);
	CString Param = dbparam;
	if (Param.IsEmpty())
	{
		Param = "0";
	}

	CString Error = "";
	CStringArray olApplArray;
	olApplArray.Add(ogAppl);
	bool rdr = true;
	rdr = ogGUILng->MemberInit(&Error, CString(pcgUser), &olApplArray, CString(pcgHome), CString(lng), Param);

	if (!rdr)
	{
		CString tmp = "Languagesupport failed!\n"+Error+"parameter empty!\n";
        AfxMessageBox(tmp);
		TRACE("MultiLng-Support failed!\n %sparameter empty!!!\n",Error);
	}
	//***********************************************************************************************
	// ENDE der Initialisierung der GUI-Sprache // RDR 03.01.2000
	//***********************************************************************************************
	
	ogBasicData.SetUtcDifferenceFromApttab();

	//--------------------------------------------------------------------------------------
	// Register your broadcasts here
	CString olTableName;
	CString olTableExt = CString(pcgTableExt);
	// RT : Read Table
	// IRT: Insert Record Table
	// URT: Update Record Table
	// DRT: Delete Record Table

	olTableName = CString("DRR") + olTableExt;
	ogBcHandle.AddTableCommand( olTableName, CString("IRT"),BC_DRR_NEW,    false,pcgAppName);
	ogBcHandle.AddTableCommand( olTableName, CString("URT"),BC_DRR_CHANGE, false,pcgAppName);
	ogBcHandle.AddTableCommand( olTableName, CString("DRT"),BC_DRR_DELETE, false,pcgAppName);

	ogBcHandle.AddTableCommand(CString("SNGDRR"), CString("SBC"), BC_RELOAD_SINGLE_DRR, false,pcgAppName);
	ogBcHandle.AddTableCommand(CString("MULDRR"), CString("SBC"), BC_RELOAD_MULTI_DRR, false,pcgAppName);
	ogBcHandle.AddTableCommand(CString("RELDRR"), CString("SBC"), BC_RELDRR, true);
	ogBcHandle.AddTableCommand(CString("ENDRLR"), CString("SBC"), BC_ENDRLR, false,pcgAppName);

	olTableName = CString("REL") + olTableExt;
	ogBcHandle.AddTableCommand( olTableName, CString("IRT"),BC_REL_NEW,    false,pcgAppName);
	ogBcHandle.AddTableCommand( olTableName, CString("URT"),BC_REL_CHANGE, false,pcgAppName);
	ogBcHandle.AddTableCommand( olTableName, CString("DRT"),BC_REL_DELETE, false,pcgAppName);

	olTableName = CString("DRA") + olTableExt;
	ogBcHandle.AddTableCommand( olTableName, CString("IRT"),BC_DRA_NEW,    false,pcgAppName);
	ogBcHandle.AddTableCommand( olTableName, CString("URT"),BC_DRA_CHANGE, false,pcgAppName);
	ogBcHandle.AddTableCommand( olTableName, CString("DRT"),BC_DRA_DELETE, false,pcgAppName);

	olTableName = CString("DRD") + olTableExt;
	ogBcHandle.AddTableCommand( olTableName, CString("IRT"),BC_DRD_NEW,    false,pcgAppName);
	ogBcHandle.AddTableCommand( olTableName, CString("URT"),BC_DRD_CHANGE, false,pcgAppName);
	ogBcHandle.AddTableCommand( olTableName, CString("DRT"),BC_DRD_DELETE, false,pcgAppName);

	olTableName = CString("DRS") + olTableExt;
	ogBcHandle.AddTableCommand( olTableName, CString("IRT"),BC_DRS_NEW,    false,pcgAppName);
	ogBcHandle.AddTableCommand( olTableName, CString("URT"),BC_DRS_CHANGE, false,pcgAppName);
	ogBcHandle.AddTableCommand( olTableName, CString("DRT"),BC_DRS_DELETE, false,pcgAppName);

	olTableName = CString("DRG") + olTableExt;
	ogBcHandle.AddTableCommand( olTableName, CString("IRT"),BC_DRG_NEW,    false,pcgAppName);
	ogBcHandle.AddTableCommand( olTableName, CString("URT"),BC_DRG_CHANGE, false,pcgAppName);
	ogBcHandle.AddTableCommand( olTableName, CString("DRT"),BC_DRG_DELETE, false,pcgAppName);

	olTableName = CString("WGP") + olTableExt;
	ogBcHandle.AddTableCommand(olTableName, CString("IRT"), BC_WGP_NEW,		 false,pcgAppName);
	ogBcHandle.AddTableCommand(olTableName, CString("URT"), BC_WGP_CHANGE,	 false,pcgAppName);
	ogBcHandle.AddTableCommand(olTableName, CString("DRT"), BC_WGP_DELETE,	 false,pcgAppName);

	olTableName = CString("ACC") + olTableExt;
	ogBcHandle.AddTableCommand(olTableName, CString("IRT"), BC_ACC_NEW,		 true);
	ogBcHandle.AddTableCommand(olTableName, CString("URT"), BC_ACC_CHANGE,	 true);
	ogBcHandle.AddTableCommand(olTableName, CString("DRT"), BC_ACC_DELETE,	 true);
	ogBcHandle.AddTableCommand(CString("RELACC"), CString("SBC"), BC_RELACC, true);

	olTableName = CString("PFC") + olTableExt;
	ogBcHandle.AddTableCommand( olTableName, CString("IRT"),BC_PFC_NEW,    true);
	ogBcHandle.AddTableCommand( olTableName, CString("URT"),BC_PFC_CHANGE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("DRT"),BC_PFC_DELETE, true);

	olTableName = CString("SPF") + olTableExt;
	ogBcHandle.AddTableCommand( olTableName, CString("IRT"),BC_SPF_NEW,    true);
	ogBcHandle.AddTableCommand( olTableName, CString("URT"),BC_SPF_CHANGE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("DRT"),BC_SPF_DELETE, true);

	olTableName = CString("ORG") + olTableExt;
	ogBcHandle.AddTableCommand( olTableName, CString("IRT"),BC_ORG_NEW,    true);
	ogBcHandle.AddTableCommand( olTableName, CString("URT"),BC_ORG_CHANGE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("DRT"),BC_ORG_DELETE, true);

	olTableName = CString("SOR") + olTableExt;
	ogBcHandle.AddTableCommand( olTableName, CString("IRT"),BC_SOR_NEW,    true);
	ogBcHandle.AddTableCommand( olTableName, CString("URT"),BC_SOR_CHANGE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("DRT"),BC_SOR_DELETE, true);

	olTableName = CString("PER") + olTableExt;
	ogBcHandle.AddTableCommand( olTableName, CString("PER"),BC_ORG_NEW,    true);
	ogBcHandle.AddTableCommand( olTableName, CString("PER"),BC_ORG_CHANGE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("PER"),BC_ORG_DELETE, true);

	olTableName = CString("SPE") + olTableExt;
	ogBcHandle.AddTableCommand( olTableName, CString("IRT"),BC_SPE_NEW,    true);
	ogBcHandle.AddTableCommand( olTableName, CString("URT"),BC_SPE_CHANGE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("DRT"),BC_SPE_DELETE, true);

	olTableName = CString("STF") + olTableExt;
	ogBcHandle.AddTableCommand( olTableName, CString("IRT"),BC_STF_NEW,    true);
	ogBcHandle.AddTableCommand( olTableName, CString("URT"),BC_STF_CHANGE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("DRT"),BC_STF_DELETE, true);

	olTableName = CString("PGP") + olTableExt;
	ogBcHandle.AddTableCommand(olTableName, CString("IRT"), BC_PGP_NEW,		 false,pcgAppName);
	ogBcHandle.AddTableCommand(olTableName, CString("URT"), BC_PGP_CHANGE,	 false,pcgAppName);
	ogBcHandle.AddTableCommand(olTableName, CString("DRT"), BC_PGP_DELETE,	 false,pcgAppName);

	olTableName = CString("SDT") + olTableExt;
	ogBcHandle.AddTableCommand(olTableName, CString("IRT"), BC_SDT_NEW,		 true);
	ogBcHandle.AddTableCommand(olTableName, CString("URT"), BC_SDT_CHANGE,	 true);
	ogBcHandle.AddTableCommand(olTableName, CString("DRT"), BC_SDT_DELETE,	 true);

	olTableName = CString("SDG") + olTableExt;
	ogBcHandle.AddTableCommand( olTableName, CString("IRT"),BC_SDG_NEW,    true);
	ogBcHandle.AddTableCommand( olTableName, CString("URT"),BC_SDG_CHANGE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("DRT"),BC_SDG_DELETE, true);
	ogBcHandle.AddTableCommand (CString("RELSDG"),  CString("SBC"), BC_RELSDG, true);

	olTableName = CString("MSD") + olTableExt;
	ogBcHandle.AddTableCommand( olTableName, CString("IRT"),BC_MSD_NEW,    true);
	ogBcHandle.AddTableCommand( olTableName, CString("URT"),BC_MSD_CHANGE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("DRT"),BC_MSD_DELETE, true);
	ogBcHandle.AddTableCommand (CString("RELMSD"),  CString("SBC"), BC_RELMSD, true);

	olTableName = CString("SGR") + olTableExt;
	ogBcHandle.AddTableCommand( olTableName, CString("IRT"),BC_SGR_NEW,    true);
	ogBcHandle.AddTableCommand( olTableName, CString("URT"),BC_SGR_CHANGE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("DRT"),BC_SGR_DELETE, true);

	olTableName = CString("SGM") + olTableExt;
	ogBcHandle.AddTableCommand( olTableName, CString("IRT"),BC_SGM_NEW,    true);
	ogBcHandle.AddTableCommand( olTableName, CString("URT"),BC_SGM_CHANGE, true);
	ogBcHandle.AddTableCommand( olTableName, CString("DRT"),BC_SGM_DELETE, true);

	// we have to load the ORGTAB before the login, so that we can register it
	CString olSizeText;
	ogOrgData.SetTableName(CString(CString("ORG") + CString(pcgTableExt)));
	ogOrgData.Read();
	olSizeText.Format(" ........ %d",ogOrgData.omData.GetSize());
	WriteLogFull("ORGTAB loaded %s", olSizeText);

	// we need also the parameters to check the customer
	int ilCntParameter = 0;
	ilCntParameter += ogCCSParam.BufferParams(ogAppl);
	ilCntParameter += ogCCSParam.BufferParams("GLOBAL");
	ilCntParameter += ogCCSParam.BufferParams("INITACC");

	char pclAddParameters[10];
	char *cpCopyParameter;
	bool blAddParameters = false;
	GetPrivateProfileString(pcgAppName, "ADD_PARAMETERS", "FALSE", pclAddParameters, sizeof pclAddParameters, pcgConfigPath);
	cpCopyParameter = _strupr (_strdup (pclAddParameters));
	if (strcmp(cpCopyParameter, "TRUE") == 0)
	{
		blAddParameters = true;
	}
	free (cpCopyParameter);

	if (ilCntParameter == 0 && blAddParameters == false)
	{
        AfxMessageBox(CString("Could not load the parameters!\nNot allowed to add parameters to DB (ADD_PARAMETERS=FALSE).\nApplication will terminate!"));
		return FALSE;
	}
	else
	{
		LoadParameters();
	}

	// Login Dialog aufrufen
	// pcgHome,ogAppName und ogAppl sind global definiert und hardcodiert.
#if	0
	CLoginDialog olLoginDlg(pcgHome,ogAppName,NULL);
	if( olLoginDlg.DoModal() != IDCANCEL )
	{
		int ilStartApp = IDOK; 
		if(ogPrivList.GetStat("InitModu") == '1')
		{
			RegisterDlg olRegisterDlg;
			ilStartApp = olRegisterDlg.DoModal();
		}
		if(ilStartApp != IDOK)
		{
			return FALSE;
		}
	}
	else
	{
		return FALSE;
	}
#else
	CDialog olDummyDlg;	
	olDummyDlg.Create(IDD_DUMMY_DLG,NULL);

	CAatLogin olLoginCtrl;
	olLoginCtrl.Create(NULL,WS_CHILD|WS_DISABLED,CRect(0,0,100,100),&olDummyDlg,4711);

	olLoginCtrl.SetInfoButtonVisible(TRUE);
	olLoginCtrl.SetRegisterApplicationString(ogInitModuData.GetInitModuTxt());
	if (olLoginCtrl.ShowLoginDialog() != "OK")
	{
		olDummyDlg.DestroyWindow();
		return FALSE;
	}

	strcpy(pcgUser,olLoginCtrl.GetUserName_());
	strcpy(pcgPasswd,olLoginCtrl.GetUserPassword());
	ogBasicData.omUserID = pcgUser;

	ogCommHandler.SetUser(pcgUser);
	strcpy(CCSCedaData::pcmUser, pcgUser);
	strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);

	ogPrivList.Add(olLoginCtrl.GetPrivilegList());

	olDummyDlg.DestroyWindow();

#endif
	
	
	// Login Zeit setzen
	ogLoginTime = CTime::GetCurrentTime();
	
	//LogFile schreiben
	WriteInRosteringLog(LOGFILE_OFF, " ");
	WriteInRosteringLog(LOGFILE_OFF, "----------  Starting Rostering   ----------");

	ogRosteringLogText.Format("Server: %s / %s", ogCommHandler.pcmRealHostName, ogCommHandler.pcmRealHostType);
	WriteInRosteringLog(LOGFILE_OFF);

	ogRosteringLogText.Format("User: %s", pcgUser);
	WriteInRosteringLog(LOGFILE_OFF);
	
	ogRosteringLogText.Format("Login time: %s", ogLoginTime.Format("%d.%m.%Y  %H:%M"));
	WriteInRosteringLog(LOGFILE_OFF);
	
	CString olLogStat;
	switch(lgLogFileStatus)
	{
		default:
		case LOGFILE_OFF:
			olLogStat = "OFF";
			break;
		case LOGFILE_TRACE:
			olLogStat = "TRACE";
			break;
		case LOGFILE_FULL:
			olLogStat = "FULL";
			break;
	}
	ogRosteringLogText.Format("LOGSTATUS=%s   (possible is FULL, TRACE or OFF)",olLogStat);
	WriteInRosteringLog(LOGFILE_OFF, ogRosteringLogText);

	// Ruft offensichtlich den "hochlade" Dialog auf
    InitialLoad();

	// Pr�ft die geladenen Parameter
	CheckParameters();

	/*	if (!ogUfisAppMng.CreateDispatch(CLSID_UfisAppMgr))
	{
		ogPrivList.SetStat("DUTYROSTER_IDC_B_PRINT", "0");
		ogPrivList.SetStat("SR_PRINT", "0");
	}
	else
	{
		CString strFullExePath;
		strFullExePath = _pgmptr;
		ogUfisAppMng.Register(strFullExePath);
		// Alternative? Besser?
		// TCHAR szFileName[MAX_PATH];
		// DWORD dwSize = MAX_PATH;
		// GetModuleFileName(0, szFileName, dwSize);
	}*/

	// initializing the NameConfigurationDlg
	CStringArray olArr;
	olArr.Add(LoadStg(IDS_STRING1796));
	olArr.Add(LoadStg(IDS_STRING1797));
	olArr.Add(LoadStg(IDS_STRING1798));
	olArr.Add(LoadStg(IDS_STRING1799));
	olArr.Add(LoadStg(IDS_STRING1800));
	olArr.Add(LoadStg(IDS_STRING1801));
	olArr.Add(LoadStg(IDS_STRING1802));
	olArr.Add(LoadStg(IDS_STRING1803));
	olArr.Add(LoadStg(IDS_STRING1804));
	olArr.Add(LoadStg(IDS_STRING1805));

	pogNameConfigurationDlg = new NameConfigurationDlg(&olArr, "");

	// create main MDI Frame window
	CMainFrame* pMainFrame = new CMainFrame;
	if (!pMainFrame->LoadFrame(IDR_MAINFRAME))
		return FALSE;
	m_pMainWnd = pMainFrame;
	// Men� �bersetzen
	TranslateMainMenu(pMainFrame->m_hMenuDefault);
	// Men� �bersetzen
	TranslateDocMenu(pomShiftRosterTemplate->m_hMenuShared);
	// Men� �bersetzen
	TranslateDocMenu(pomDutyRosterTemplate->m_hMenuShared);
	
	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	// The main window has been initialized, so show and update it.
	// Hauptfenster maximiert zeigen.
	// set Windowstitle at runtime!!! rdr
	pMainFrame->SetWindowText(ogAppName);
	pMainFrame->ShowWindow(m_nCmdShow | SW_SHOWMAXIMIZED);
	pMainFrame->UpdateWindow();

	return TRUE;
}

//****************************************************************************
// Hier werden die Daten von der Datenbank in die Application geladen
//****************************************************************************

void CRosteringApp::InitialLoad()
{
	ogErrlog.Empty();
	pogInitialLoad = new CInitialLoadDlg();
	if (pogInitialLoad != NULL)
	{
		pogInitialLoad->SetMessage(LoadStg(IDS_STRING1362));
	}
	CWaitCursor olDummy;

	bgIsInitialized = false;

	/////////////////////////////////////////////////////////////////////////////////////
	// add here your cedaXXXdata read methods
	/////////////////////////////////////////////////////////////////////////////////////
	//------------------------------------------------------------------------------------
	ogCfgData.SetTableName(CString(CString("VCD") + CString(pcgTableExt)));
	ogBsdData.SetTableName(CString(CString("BSD") + CString(pcgTableExt)));
	ogOdaData.SetTableName(CString(CString("ODA") + CString(pcgTableExt)));
	ogOacData.SetTableName(CString(CString("OAC") + CString(pcgTableExt)));
	ogCotData.SetTableName(CString(CString("COT") + CString(pcgTableExt)));
	ogPerData.SetTableName(CString(CString("PER") + CString(pcgTableExt)));
	ogPfcData.SetTableName(CString(CString("PFC") + CString(pcgTableExt)));
	ogSdgData.SetTableName(CString(CString("SDG") + CString(pcgTableExt)));
	ogSpeData.SetTableName(CString(CString("SPE") + CString(pcgTableExt)));
	ogSpfData.SetTableName(CString(CString("SPF") + CString(pcgTableExt)));
	ogScoData.SetTableName(CString(CString("SCO") + CString(pcgTableExt)));
	ogSorData.SetTableName(CString(CString("SOR") + CString(pcgTableExt)));
	ogSteData.SetTableName(CString(CString("STE") + CString(pcgTableExt)));
	ogSwgData.SetTableName(CString(CString("SWG") + CString(pcgTableExt)));
	ogStfData.SetTableName(CString(CString("STF") + CString(pcgTableExt)));
	ogTeaData.SetTableName(CString(CString("TEA") + CString(pcgTableExt)));
	ogPgpData.SetTableName(CString(CString("PGP") + CString(pcgTableExt)));
	ogWgpData.SetTableName(CString(CString("WGP") + CString(pcgTableExt)));
	ogAccData.SetTableName(CString(CString("ACC") + CString(pcgTableExt)));
	ogDelData.SetTableName(CString(CString("DEL") + CString(pcgTableExt)));
	ogDraData.SetTableName(CString(CString("DRA") + CString(pcgTableExt)));
	ogDrdData.SetTableName(CString(CString("DRD") + CString(pcgTableExt)));
	ogDrgData.SetTableName(CString(CString("DRG") + CString(pcgTableExt)));
	ogDrrData.SetTableName(CString(CString("DRR") + CString(pcgTableExt)));
	ogDrsData.SetTableName(CString(CString("DRS") + CString(pcgTableExt)));
	ogMsdData.SetTableName(CString(CString("MSD") + CString(pcgTableExt)));
	ogSdtData.SetTableName(CString(CString("SDT") + CString(pcgTableExt)));
	ogWisData.SetTableName(CString(CString("WIS") + CString(pcgTableExt)));
	ogSprData.SetTableName(CString(CString("SPR") + CString(pcgTableExt)));
	ogSgrData.SetTableName(CString(CString("SGR") + CString(pcgTableExt)));
	ogSgmData.SetTableName(CString(CString("SGM") + CString(pcgTableExt)));
	
	//--------------------------------------------------------------------------------------
	//Update Sync because a crashed apllication could have set this state
	CTime olCurrentTime; 
	CTime olToTime;
	olCurrentTime = CTime::GetCurrentTime();
	olToTime = olCurrentTime + CTimeSpan(0, 0, 2, 0);
	CString olStrFrom = CTimeToDBString(olCurrentTime,olCurrentTime);
	CString olStrTo = CTimeToDBString(olToTime,olToTime);
	//char pclData[1024]="";
	/*
	//==> ROS
	CString olOrigFieldList;
	char pclRel[]="REL";
	char pclLate[]="LATE";
	
	  sprintf(pclData, "*CMD*,SYN%s,SYNC,DUTYROSTER, ,%s,%s, ", 
	  pcgTableExt, 
	  olStrFrom.GetBuffer(0), 
	  olStrTo.GetBuffer(0));
	  olOrigFieldList = CString(ogRosData.pcmListOfFields);
	  ogRosData.CedaAction(pclRel,pclLate,"",pclData);
	  strcpy(ogRosData.pcmListOfFields, olOrigFieldList);
	*/
	//SYNC END------------------------------------------------------------------------
	// CFG
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING102));
	ogCfgData.ReadCfgData();
	ogCfgData.ReadMonitorSetup();

	//---------------------------------------------------------------------------------
	// Start Reading
	// Initialisiert die DRW Tabelle / W�nsche
	ogBCD.SetObject("DRW");
	// Auf Broadcast reagieren
	ogBCD.SetDdxType("DRW", "IRT", BC_DRW_NEW);
	ogBCD.SetDdxType("DRW", "URT", BC_DRW_CHANGE);
	ogBCD.SetDdxType("DRW", "DRT", BC_DRW_DELETE);

	CString olSizeText;
	int ilTmpLoadSize = 0;
	int ilPercent = 100 / 22; //Teiler ist gleich Anzahl der zu lesenden Tabellen
	int i;

	/*
	// Nur wegen Server Denver drau�en
	// Parameter mittels CCSParam Klasse einlesen
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1844));

	ilResult += ogCCSParam.BufferParams(ogAppl);
	ilResult += ogCCSParam.BufferParams("GLOBAL");
	olSizeText.Format(" ........ %d", ilResult);
	// Parameter testen
	LoadParameters();

	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);*/

	for (i = 0; i < ogCfgData.omViews.GetSize(); i++)
	{
		ilTmpLoadSize += ogCfgData.omViews[i].omNameData.GetSize();
	}

	olSizeText.Format(" ........ %d",ilTmpLoadSize);
	WriteLogFull("Parameters loaded %s", olSizeText);
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);
	WriteLogFull("CFGTAB loaded %s", olSizeText);
	
	// PFCTAB
	pogInitialLoad->SetMessage(LoadStg(SHIFT_STF_FUNCTION));
	ogPfcData.Read();
	olSizeText.Format(" ........ %d",ogPfcData.omData.GetSize());
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);
	WriteLogFull("PFCTAB loaded %s", olSizeText);
	
	//SGRTAB
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING117));
	CString	olWhere;
	olWhere.Format("WHERE APPL = '%s'", ogAppl);
	ogSgrData.Read((char*)(LPCTSTR)olWhere);
	olSizeText.Format(" ........ %d",ogSgrData.omData.GetSize());
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);
	ilTmpLoadSize = ogSgrData.omData.GetSize();
	WriteLogFull("SGRTAB loaded %s", olSizeText);
	
	if(ilTmpLoadSize > 0)
	{
		// SGMTAB
		CString olTmpUrnoList, olTmp;
		for(int ilElement=0; ilElement<ilTmpLoadSize; ilElement++)
		{
			olTmp.Format(",'%ld'", ogSgrData.omData[ilElement].Urno);
			olTmpUrnoList += olTmp;
		}
		if(olTmpUrnoList.GetLength() > 1)
			olTmpUrnoList = olTmpUrnoList.Mid(1);
		olWhere.Format("WHERE USGR IN (%s)", olTmpUrnoList);
		pogInitialLoad->SetMessage(LoadStg(IDS_STRING117)+" "+LoadStg(SHIFT_S_STAFF));
		ogSgmData.Read((char*)(LPCTSTR)olWhere);
		olSizeText.Format(" ........ %d",ogSgmData.omData.GetSize());
		pogInitialLoad->SetMessage(olSizeText, false);
		pogInitialLoad->SetProgress(ilPercent);
		WriteLogFull("SGMTAB loaded %s", olSizeText);
	}
	
	// PERTAB
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1380));
	ogPerData.Read();
	olSizeText.Format(" ........ %d",ogPerData.omData.GetSize());
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);
	WriteLogFull("PERTAB loaded %s", olSizeText);
	
	// STFTAB
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1381));
	ogStfData.Read();
	olSizeText.Format(" ........ %d",ogStfData.omData.GetSize());
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);
	WriteLogFull("STFTAB loaded %s", olSizeText);
	
	// WISTAB
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1820));
	ogWisData.Read();
	olSizeText.Format(" ........ %d",ogWisData.GetSize());
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);
	WriteLogFull("WISTAB loaded %s", olSizeText);
	
	// BSDTAB
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1383));
	ogBsdData.Read();
	olSizeText.Format(" ........ %d",ogBsdData.omData.GetSize());
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);
	WriteLogFull("BSDTAB loaded %s", olSizeText);
	
	// ODATAB
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1616));
	ogOdaData.Read();
	olSizeText.Format(" ........ %d",ogOdaData.omData.GetSize());
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);
	WriteLogFull("ODATAB loaded %s", olSizeText);
	
	// PGPTAB
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1809));
	ogPgpData.Read();
	olSizeText.Format(" ........ %d",ogPgpData.omData.GetSize());
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);
	WriteLogFull("PGPTAB loaded %s", olSizeText);
	
	// WGPTAB
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1691));
	ogWgpData.Read();
	olSizeText.Format(" ........ %d",ogWgpData.omData.GetSize());
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);
	WriteLogFull("WGPTAB loaded %s", olSizeText);
	
	// COTTAB
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING121));
	ogCotData.Read();
	olSizeText.Format(" ........ %d",ogCotData.omData.GetSize());
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);
	WriteLogFull("COTTAB loaded %s", olSizeText);
	
	// SPFTAB
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1673));//Zuordnungen
	ogSpfData.Read();
	ilTmpLoadSize = ogSpfData.omData.GetSize();
	pogInitialLoad->SetProgress(ilPercent);
	WriteLogFull("SPFTAB loaded %s", olSizeText);
	
	// SCOTAB
	ogScoData.Read();
	ilTmpLoadSize += ogScoData.omData.GetSize();
	pogInitialLoad->SetProgress(ilPercent);
	WriteLogFull("SCOTAB loaded %s", olSizeText);
	
	// SPETAB
	ogSpeData.Read();
	ilTmpLoadSize += ogSpeData.omData.GetSize();
	pogInitialLoad->SetProgress(ilPercent);
	WriteLogFull("SPETAB loaded %s", olSizeText);
	
	// Zuordnungen zu Fahrgemeinschaften
	//ogSteData.Read();
	//ilTmpLoadSize += ogSteData.omData.GetSize();
	//pogInitialLoad->SetProgress(ilPercent);
	
	// SWGTAB
	ogSwgData.Read();
	ilTmpLoadSize += ogSwgData.omData.GetSize();
	pogInitialLoad->SetProgress(ilPercent);
	WriteLogFull("SWGTAB loaded %s", olSizeText);
	
	// SORTAB
	ogSorData.Read();
	ilTmpLoadSize += ogSorData.omDataSur.GetSize();
	olSizeText.Format(" ........ %d",ilTmpLoadSize);
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);
	WriteLogFull("SORTAB loaded %s", olSizeText);
	
	// BCDTAB (HOLTAB)
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING148));
	ogBCD.SetObject("HOL");
	ogBCD.SetObjectDesc("HOL", LoadStg(IDS_STRING1392));
	ogBCD.Read(CString("HOL"));
	olSizeText.Format(" ........ %d",ogBCD.GetDataCount("HOL"));
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);
	WriteLogFull("BCDTAB (HOLTAB) loaded %s", olSizeText);
	
	// BCDTAB (DBOTAB)
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1395));
	ogBCD.SetObject("DBO");
	ogBCD.SetObjectDesc("DBO", LoadStg(IDS_STRING1396));
	ogBCD.Read(CString("DBO"));
	olSizeText.Format(" ........ %d",ogBCD.GetDataCount("DBO"));
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);
	WriteLogFull("BCDTAB (DBOTAB) loaded %s", olSizeText);
	
	// SDGTAB
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1397));
	ogSdgData.Read();
	olSizeText.Format(" ........ %d",ogSdgData.omData.GetSize());
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);
	WriteLogFull("SDGTAB loaded %s", olSizeText);
	
	// OACTAB
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1659));
	ogOacData.Read(0);
	olSizeText.Format(" ........ %d",ogOacData.omData.GetSize());
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);
	WriteLogFull("OACTAB loaded %s", olSizeText);
	
	// DELTAB
	ogDelData.Read();
	olSizeText.Format(" ........ %d",ogDelData.omData.GetSize());
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1810)+olSizeText);
	pogInitialLoad->SetProgress(ilPercent);
	WriteLogFull("DELTAB loaded %s", olSizeText);

	// Arbeitszeitkonten
	ogBCD.SetObject("ADE");
	ogBCD.SetObjectDesc("ADE", LoadStg(IDS_STRING1667));
	ogBCD.AddKeyMap("ADE", "URNO");
	ogBCD.AddKeyMap("ADE", "NAME");
	ogBCD.AddKeyMap("ADE", "CODE");
	ogBCD.AddKeyMap("ADE", "TYPE");
	ogBCD.Read(CString("ADE"));
	
	// BCDTAB (ADSTAB)
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1667));
	ogBCD.SetObject("ADS");
	ogBCD.SetObjectDesc("ADS", LoadStg(IDS_STRING1667));
	ogBCD.AddKeyMap("ADS", "URNO");
	ogBCD.AddKeyMap("ADS", "ADEU");
	ogBCD.AddKeyMap("ADS", "LNUM");
	ogBCD.Read(CString("ADS"));
	olSizeText.Format(" ........ %d",ogBCD.GetDataCount("ADS") + ogBCD.GetDataCount("ADE"));
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);
	WriteLogFull("BCDTAB (ADSTAB) loaded %s", olSizeText);
	
	ogShiftCheck.Initialize();

	//-- End Reading
	pogInitialLoad->SetProgress(100);

	// Hochlade Dialog beenden
	if (pogInitialLoad != NULL)
	{
		pogInitialLoad->DestroyWindow();
		pogInitialLoad = NULL;
	}

	// sind Fehlermeldungen vorhanden? Errlog-File erstellen und User informieren wenn gew�nscht
	if(ogErrlog.GetLength())
	{
		WriteInErrlogFile();

		// Message Box wenn gew�nscht
		char pclWarningDisplay[256];
		GetPrivateProfileString(pcgAppName, "INCONSISTENCY_WARNING_DISPLAY", "YES",pclWarningDisplay, sizeof pclWarningDisplay, pcgConfigPath);
		if (stricmp(pclWarningDisplay,"YES") == 0)
		{
			// Inkonsistente Daten in der Datenbank gefunden.\nDiese Daten werden in Rostering nicht verwendet.\nBitte informieren Sie Ihren Systemadministrator.\nWeitere Angaben in %s*INTXT*
			CString olMsg;
			olMsg.Format(LoadStg(IDS_STRING196), pcgErrlogFilePath);
			AfxMessageBox(olMsg,MB_OK|MB_ICONEXCLAMATION);
		}
	}
}

//****************************************************************************
// Parameter Werte pr�fen und ggf. Defaultwerte setzen
//****************************************************************************

void CRosteringApp::LoadParameters()
{
	// aktuelle Zeit erhalten
	COleDateTime olTimeNow = COleDateTime::GetCurrentTime();
	CString olStringNow = olTimeNow.Format("%Y%m%d%H%M%S");
	CString olDefaultValidFrom	= ("19901010101010");
	CString olDefaultValidTo	= "";

	// Falls Parameter nicht exitiert. anlegen und Defaultwert setzen.
	// General
	ogCCSParam.GetParam(ogAppl,"ID_BLANK_CHAR",olStringNow,"-",LoadStg(737),"General","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","1111111",true);
	ogCCSParam.GetParam(ogAppl,"ID_DRR_DELETED",olStringNow," ",LoadStg(738),"General","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","1111111",true);
	ogCCSParam.GetParam(ogAppl,"ID_PROD_CUSTOMER",olStringNow,"UNDEF",LoadStg(739),"General","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","1111111",true);
	ogCCSParam.GetParam(ogAppl,"ID_REGULAR_FREE",olStringNow,"FREE",LoadStg(740),"General","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","1111111",true);
	ogCCSParam.GetParam(ogAppl,"ID_SHOW_STEPS",olStringNow,"1|U|W|L|2|3|4|5",LoadStg(741),"General","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","1111111",true);

	// Workingrules
	ogCCSParam.GetParam(ogAppl,"ID_WOR_AZE_DIFF",olStringNow,"10",LoadStg(744),"Workingrules","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","1111111",true);

	ogCCSParam.GetParam(ogAppl,"ID_WOR_BREAK_PAY",olStringNow,"0000001",LoadStg(745),"Workingrules","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","1111111",true);	

	ogCCSParam.GetParam(ogAppl,"ID_WOR_FREEH",olStringNow,"480",LoadStg(746),"Workingrules","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","1111111",true);
	ogCCSParam.GetParam(ogAppl,"ID_WOR_FREEHT",olStringNow,"600",LoadStg(747),"Workingrules","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","1111111",true);

	ogCCSParam.GetParam(ogAppl,"ID_WOR_MAFREED",olStringNow,"4",LoadStg(748),"Workingrules","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","1111111",true);
	ogCCSParam.GetParam(ogAppl,"ID_WOR_MAFREEDT",olStringNow,"2",LoadStg(749),"Workingrules","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","1111111",true);

	ogCCSParam.GetParam(ogAppl,"ID_WOR_MAKM",olStringNow,"7",LoadStg(750),"Workingrules","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","1111111",true);

	ogCCSParam.GetParam(ogAppl,"ID_WOR_MAWORD",olStringNow,"7",LoadStg(751),"Workingrules","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","1111111",true);
	ogCCSParam.GetParam(ogAppl,"ID_WOR_MAWORDT",olStringNow,"6",LoadStg(752),"Workingrules","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","1111111",true);

	ogCCSParam.GetParam(ogAppl,"ID_WOR_MAWORH",olStringNow,"600",LoadStg(753),"Workingrules","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","1111111",true);
	ogCCSParam.GetParam(ogAppl,"ID_WOR_MAWORHT",olStringNow,"600",LoadStg(754),"Workingrules","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","1111111",true);	

	ogCCSParam.GetParam(ogAppl,"ID_WOR_MIWORH",olStringNow,"0",LoadStg(755),"Workingrules","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","1111111",true);
	ogCCSParam.GetParam(ogAppl,"ID_WOR_MIWORHT",olStringNow,"0",LoadStg(756),"Workingrules","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","1111111",true);

	ogCCSParam.GetParam(ogAppl,"ID_WOR_RESTDW",olStringNow,"1",LoadStg(757),"Workingrules","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","1111111",true);
	ogCCSParam.GetParam(ogAppl,"ID_WOR_RESTH",olStringNow,"1980",LoadStg(758),"Workingrules","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","1111111",true);

	ogCCSParam.GetParam(ogAppl,"ID_WOR_WORHWT",olStringNow,"3600",LoadStg(762),"Workingrules","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","1111111",true);

	ogCCSParam.GetParam(ogAppl,"ID_NUM_WOR_DAYS",olStringNow,"5",LoadStg(766),"Workingrules","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","1111111",true);

	// Sollen die Mitarbeiter u. Schicht Funktionen verglichen werden Y/N
	ogCCSParam.GetParam(ogAppl,"ID_TEST_FUNCTION",olStringNow,"N",LoadStg(763),"General","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","1111111",true);

	// Soll die Abwesenheit nach dem Vertragscode kontrolliert werden? Y/N
	ogCCSParam.GetParam(ogAppl,"ID_TEST_ABSENCE",olStringNow,"Y",LoadStg(764),"General","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","1111111",true);

	// Soll die Funktionalit�t "Operational Flag" eingeschaltet werden? Y/N
	ogCCSParam.GetParam("GLOBAL","ID_OPERATION_FL",olStringNow,"N",LoadStg(765),"General","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","1111111",true);

	// Shift roster general
	ogCCSParam.GetParam("GLOBAL","ID_ORG_SECURITY",olStringNow,"N",LoadStg(IDS_STRING107),"General","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","1111111",true);

	// enable the account prefetching
	ogCCSParam.GetParam("GLOBAL","ID_ENABLE_ACC",olStringNow,"N",LoadStg(IDS_STRING116),"Accounting","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","1111111",true);
}

//****************************************************************************
// Pr�fung der geladenen Parameter
//****************************************************************************

void CRosteringApp::CheckParameters()
{
	//--------------------------------------------------------------------------------
	// Sollten neue Parameter eingef�gt worden sein, Meldung ausgeben.
	CStringList* polMessageList = ogCCSParam.GetMessageList();
	if (polMessageList->GetCount() != 0)
	{
		CListBoxDlg olDlg(polMessageList, LoadStg(IDS_STRING1847),LoadStg(IDS_STRING1850));   
		olDlg.DoModal();
	}
	//--------------------------------------------------------------------------------
	// Parameter "regul�r Arbeitsfrei" wird gepr�ft
	
	//1.) Pr�fen ob der Code vorhanden ist.
	//2.) Hat die Schicht das Flag "regul�r Arbeitsfrei"
	// aktuelle Zeit erhalten
	CString olOdaCode = ogCCSParam.GetParamValue(ogAppl,"ID_REGULAR_FREE");	

	ODADATA* polOda = ogOdaData.GetOdaBySdac(olOdaCode);
	
	if (polOda != NULL)
	{
		if (CString(polOda->Free) == "x")
		{
			// Alles OK !
		}
		else
		{
			// Code hat nicht "regul�r Arbeitsfrei" Flag.
			if (AfxMessageBox(LoadStg(IDS_STRING398), MB_ICONSTOP|MB_YESNO|MB_DEFBUTTON2) == IDNO)
			{
				// Anwendung beenden.
				PostQuitMessage(NULL);
			}
		}
	}
	else
	{
		if (AfxMessageBox(LoadStg(IDS_STRING399), MB_ICONSTOP|MB_YESNO|MB_DEFBUTTON2) == IDNO)
		{
			// Anwendung beenden.
			PostQuitMessage(NULL);
		}
	}
	//--------------------------------------------------------------------------------
	// Parameter "operational flag" wird gepr�ft
	if ("Y" == ogCCSParam.GetParamValue("GLOBAL","ID_OPERATION_FL"))
	{
		bgEnableScenario = true;
	}
	else
	{
		bgEnableScenario = false;
	}

}

//****************************************************************************
// TranslateDocMenu: �bersetzt das Men�, das bei aktiviertem Dienst-
// oder Schichtplan angezeigt wird.
// R�ckgabe: Erfolg?
//****************************************************************************

BOOL CRosteringApp::TranslateDocMenu(HMENU hMenu)
{
	CCS_TRY;

	bool bSchichtplan, bDienstplan, bStammdaten, bGrouping;
	bool bStaffBalance, bInitAccount, bAbsencePlanning;
	bSchichtplan = bDienstplan = bStammdaten = bGrouping = true;	
	bStaffBalance = bInitAccount = bAbsencePlanning = true;
	bool bImportShifts = false;
	bool bSendToSap = false;
	bool blReloadBasicData = false;
	
	// g�ltiges Handle?
	if (!hMenu) return false;

	CString csCode;

	// display shiftroster in menu?
	csCode = ogPrivList.GetStat("SR_VIEW");
	if (csCode == '0' || csCode == '-')
	{
		bSchichtplan = false;
	}

	// display dutyroster in menu?
	csCode = ogPrivList.GetStat("MENUDUTYROSTER");
	if (csCode == '0' || csCode == '-')
	{
		bDienstplan = false;
	}

	// display "Send To SAP" dialog?
	csCode = ogPrivList.GetStat("MENUSENDTOSAP");
	if (csCode == '1')
	{
		bSendToSap = true;
	}

	// display "Reload Basic Data" dialog?
	csCode = ogPrivList.GetStat("MENURELOADBASICDATA");
	if (csCode == '1')
	{
		blReloadBasicData = true;
	}

	if(strcmp(pcgStammdatenPath, "DEFAULT") == 0)
		bStammdaten = false;
	
	if(strcmp(pcgStaffBalancePath, "DEFAULT") == 0)
		bStaffBalance = false;
	
	if(strcmp(pcgInitAccountPath, "DEFAULT") == 0)
		bInitAccount = false;
	
	if(strcmp(pcgAbsencePlanningPath, "DEFAULT") == 0)
		bAbsencePlanning = false;
	
	if(strcmp(pcgGroupingPath, "DEFAULT") == 0)
		bGrouping = false;
	
	// Men� erzeugen
	CMenu olMenu;
	olMenu.Attach(hMenu);
	
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	// Men� 'Datei' �ndern
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	olMenu.ModifyMenu(0,MF_BYPOSITION|MF_STRING,0,LoadStg(ID_M_DATEI));
	CMenu* pSubMenu0 = olMenu.GetSubMenu(0);
	if (pSubMenu0 != NULL)
	{
		pSubMenu0->ModifyMenu(0,MF_BYPOSITION|MF_STRING,ID_FILE_SETUP,LoadStg(ID_M_SETUP));
		pSubMenu0->ModifyMenu(2,MF_BYPOSITION|MF_STRING,ID_SENDTOSAP,LoadStg(IDS_S_ToSAP));
		pSubMenu0->ModifyMenu(3,MF_BYPOSITION|MF_STRING,IDM_IMPORTSHIFTS,LoadStg(IDM_IMPORTSHIFTS));
		pSubMenu0->ModifyMenu(4,MF_BYPOSITION|MF_STRING,IDM_RELOADBASICDATA,LoadStg(ID_RELOADBASICDATA));
		pSubMenu0->ModifyMenu(6,MF_BYPOSITION|MF_STRING,ID_APP_EXIT,LoadStg(ID_M_BEENDEN));

		if (bSendToSap == false)
			pSubMenu0->RemoveMenu(ID_SENDTOSAP, MF_BYCOMMAND);

		if (bImportShifts == false)
			pSubMenu0->RemoveMenu(IDM_IMPORTSHIFTS, MF_BYCOMMAND);

		if (blReloadBasicData == false)
			pSubMenu0->RemoveMenu(IDM_RELOADBASICDATA, MF_BYCOMMAND);

		if (bSendToSap == false && bImportShifts == false && blReloadBasicData == false)
			pSubMenu0->RemoveMenu(1, MF_BYPOSITION);

	}
	
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	// Men� 'Programm' �ndern
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	olMenu.ModifyMenu(1,MF_BYPOSITION|MF_STRING,0,LoadStg(ID_M_PROGRAMM));
	CMenu *pSubMenu1 = olMenu.GetSubMenu(1);
	if (pSubMenu1 != NULL)
	{
		pSubMenu1->ModifyMenu(0,MF_BYPOSITION|MF_STRING,ID_VIEW_DUTYROSTER,LoadStg(ID_M_DIENSTPLAN));
		pSubMenu1->ModifyMenu(1,MF_BYPOSITION|MF_STRING,ID_VIEW_SHIFTROSTER,LoadStg(ID_M_SCHICHTPLAN));
		pSubMenu1->ModifyMenu(3,MF_BYPOSITION|MF_STRING,ID_PROGRAMM_STAMMDATEN,LoadStg(ID_M_STAMMDATEN));
		pSubMenu1->ModifyMenu(4,MF_BYPOSITION|MF_STRING,ID_PROGRAMM_GRUPPIERUNG,LoadStg(ID_M_GRUPPIERUNGEN));
		pSubMenu1->ModifyMenu(5,MF_BYPOSITION|MF_STRING,ID_PROGRAMM_STAFFBALANCE,LoadStg(ID_M_STAFFBALANCE));
		pSubMenu1->ModifyMenu(6,MF_BYPOSITION|MF_STRING,ID_PROGRAMM_INITACCOUNT,LoadStg(ID_M_INITACCOUNT));
		pSubMenu1->ModifyMenu(7,MF_BYPOSITION|MF_STRING,ID_PROGRAMM_ABSENCEPLANNING,LoadStg(ID_M_ABSENCEPLANNING));

		if (bSchichtplan == false)
			pSubMenu1->RemoveMenu (ID_VIEW_SHIFTROSTER, MF_BYCOMMAND);

		if (bDienstplan == false)
			pSubMenu1->RemoveMenu (ID_VIEW_DUTYROSTER, MF_BYCOMMAND);

		if(bGrouping == false)
			pSubMenu1->RemoveMenu(ID_PROGRAMM_GRUPPIERUNG, MF_BYCOMMAND);

		if(bStammdaten == false)
			pSubMenu1->RemoveMenu(ID_PROGRAMM_STAMMDATEN, MF_BYCOMMAND);

		if(bStaffBalance == false)
			pSubMenu1->RemoveMenu(ID_PROGRAMM_STAFFBALANCE, MF_BYCOMMAND);

		if(bInitAccount == false)
			pSubMenu1->RemoveMenu(ID_PROGRAMM_INITACCOUNT, MF_BYCOMMAND);

		if(bAbsencePlanning == false)
			pSubMenu1->RemoveMenu(ID_PROGRAMM_ABSENCEPLANNING, MF_BYCOMMAND);

		if(bStammdaten == false && bGrouping == false && bStaffBalance == false && bInitAccount == false && bAbsencePlanning == false)
			pSubMenu1->RemoveMenu(2, MF_BYPOSITION);
	}
	
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	// Men� 'Ansicht' �ndern
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	olMenu.ModifyMenu(2,MF_BYPOSITION|MF_STRING,0,LoadStg(ID_M_ANSICHT));
	CMenu *pSubMenu2 = olMenu.GetSubMenu(2);
	if (pSubMenu2 != NULL)
	{
		pSubMenu2->ModifyMenu(0,MF_BYPOSITION|MF_STRING,ID_VIEW_STATUS_BAR,LoadStg(ID_M_STATUSLEISTE));
	}
	
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	// Men� 'Fenster' �ndern
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	olMenu.ModifyMenu(3,MF_BYPOSITION|MF_STRING,0,LoadStg(ID_M_FENSTER));
	CMenu *pSubMenu3 = olMenu.GetSubMenu(3);
	if (pSubMenu3 != NULL)
	{
		pSubMenu3->ModifyMenu(0,MF_BYPOSITION|MF_STRING,ID_WINDOW_CASCADE,LoadStg(ID_M_WINDOW_CASCADE));
		pSubMenu3->ModifyMenu(1,MF_BYPOSITION|MF_STRING,ID_WINDOW_TILE_HORZ,LoadStg(ID_M_TILE_HORZ));
		pSubMenu3->ModifyMenu(2,MF_BYPOSITION|MF_STRING,ID_WINDOW_ARRANGE,LoadStg(ID_M_ARRANGE));
	}
	
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	// Men� '?' �ndern
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	olMenu.ModifyMenu(4,MF_BYPOSITION|MF_STRING,0,LoadStg(ID_M_HILFE));
	CMenu *pSubMenu4 = olMenu.GetSubMenu(4);
	if (pSubMenu4 != NULL)
	{
		pSubMenu4->ModifyMenu(0,MF_BYPOSITION|MF_STRING,ID_HELP_FINDER,LoadStg(ID_M_HILFETHEMEN));
		pSubMenu4->ModifyMenu(1,MF_BYPOSITION|MF_STRING,ID_APP_ABOUT,LoadStg(ID_M_INFO));
	}

	csCode = ogPrivList.GetStat("MENUDUTYROSTER");
	if(csCode=='0' || csCode=='-')
	{
		pSubMenu0->RemoveMenu (ID_VIEW_DUTYROSTER, MF_BYCOMMAND);
		pSubMenu1->RemoveMenu (ID_VIEW_DUTYROSTER, MF_BYCOMMAND);
	}

	csCode = ogPrivList.GetStat("MENUSHIFTROSTER");
	if(csCode=='0' || csCode=='-')
	{
		pSubMenu0->RemoveMenu (ID_VIEW_SHIFTROSTER, MF_BYCOMMAND);
		pSubMenu1->RemoveMenu (ID_VIEW_SHIFTROSTER, MF_BYCOMMAND);
	}
	
	csCode = ogPrivList.GetStat("MENUSTAMMDATEN");
	if(csCode=='0' || csCode=='-')
	{
		pSubMenu0->RemoveMenu (ID_PROGRAMM_STAMMDATEN, MF_BYCOMMAND);
		pSubMenu1->RemoveMenu (ID_PROGRAMM_STAMMDATEN, MF_BYCOMMAND);
	}

	csCode = ogPrivList.GetStat("MENUGRUPPIERUNG");
	if(csCode=='0' || csCode=='-')
	{
		pSubMenu0->RemoveMenu (ID_PROGRAMM_GRUPPIERUNG, MF_BYCOMMAND);
		pSubMenu1->RemoveMenu (ID_PROGRAMM_GRUPPIERUNG, MF_BYCOMMAND);
	}

	csCode = ogPrivList.GetStat("MENUSTAFFBALANCE");
	if(csCode=='0' || csCode=='-')
	{
		pSubMenu0->RemoveMenu (ID_PROGRAMM_STAFFBALANCE, MF_BYCOMMAND);
		pSubMenu1->RemoveMenu (ID_PROGRAMM_STAFFBALANCE, MF_BYCOMMAND);
	}

	csCode = ogPrivList.GetStat("MENUINITACCOUNT");
	if(csCode=='0' || csCode=='-')
	{
		pSubMenu0->RemoveMenu (ID_PROGRAMM_INITACCOUNT, MF_BYCOMMAND);
		pSubMenu1->RemoveMenu (ID_PROGRAMM_INITACCOUNT, MF_BYCOMMAND);
	}

	csCode = ogPrivList.GetStat("MENUABSENCEPLANNING");
	if(csCode=='0' || csCode=='-')
	{
		pSubMenu0->RemoveMenu (ID_PROGRAMM_ABSENCEPLANNING, MF_BYCOMMAND);
		pSubMenu1->RemoveMenu (ID_PROGRAMM_ABSENCEPLANNING, MF_BYCOMMAND);
	}

	olMenu.Detach();	
	return true;
	CCS_CATCH_ALL;
	return false;
}

//****************************************************************************
// TranslateMainMenu: �bersetzt das normale Men� (IDR_MAINFRAME).
// R�ckgabe:	Erfolg?
//****************************************************************************

BOOL CRosteringApp::TranslateMainMenu(HMENU hMenu)
{
	CCS_TRY;

	bool bSchichtplan, bDienstplan, bStammdaten, bGrouping;
	bool bStaffBalance, bInitAccount, bAbsencePlanning;
	bSchichtplan = bStammdaten = bGrouping = true;	
	bStaffBalance = bDienstplan = bInitAccount = bAbsencePlanning = true;
	bool bImportShifts = false;
	bool bSendToSap = false;
	bool blReloadBasicData = false;

	// g�ltiges Handle?
	if (!hMenu) return false;

	CString csCode;

	// display shiftroster in menu?
	csCode = ogPrivList.GetStat("SR_VIEW");
	if (csCode == '0' || csCode == '-')
	{
		bSchichtplan = false;
	}

	// display dutyroster in menu?
	csCode = ogPrivList.GetStat("MENUDUTYROSTER");
	if (csCode == '0' || csCode == '-')
	{
		bDienstplan = false;
	}

	// display "Send To SAP" dialog?
	csCode = ogPrivList.GetStat("MENUSENDTOSAP");
	if (csCode == '1')
	{
		bSendToSap = true;
	}

	// display "Reload Basic Data" dialog?
	csCode = ogPrivList.GetStat("MENURELOADBASICDATA");
	if (csCode == '1')
	{
		blReloadBasicData = true;
	}

	if(strcmp(pcgStammdatenPath, "DEFAULT") == 0)
		bStammdaten = false;

	if(strcmp(pcgStaffBalancePath, "DEFAULT") == 0)
		bStaffBalance = false;

	if(strcmp(pcgInitAccountPath, "DEFAULT") == 0)
		bInitAccount = false;

	if(strcmp(pcgAbsencePlanningPath, "DEFAULT") == 0)
		bAbsencePlanning = false;

	if(strcmp(pcgGroupingPath, "DEFAULT") == 0)
		bGrouping = false;

	// Men� erzeugen
	CMenu olMenu;
	olMenu.Attach(hMenu);

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	// Men� 'Datei' �ndern
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	olMenu.ModifyMenu(0,MF_BYPOSITION|MF_STRING,0,LoadStg(ID_M_DATEI));
	CMenu *pSubMenu0 = olMenu.GetSubMenu(0);
	if (pSubMenu0 != NULL)
	{
		pSubMenu0->ModifyMenu(0,MF_BYPOSITION|MF_STRING,ID_FILE_SETUP,LoadStg(ID_M_SETUP));
		pSubMenu0->ModifyMenu(2,MF_BYPOSITION|MF_STRING,ID_SENDTOSAP,LoadStg(IDS_S_ToSAP));
		pSubMenu0->ModifyMenu(3,MF_BYPOSITION|MF_STRING,IDM_IMPORTSHIFTS,LoadStg(IDM_IMPORTSHIFTS));
		pSubMenu0->ModifyMenu(4,MF_BYPOSITION|MF_STRING,IDM_RELOADBASICDATA,LoadStg(ID_RELOADBASICDATA));
		pSubMenu0->ModifyMenu(6,MF_BYPOSITION|MF_STRING,ID_APP_EXIT,LoadStg(ID_M_BEENDEN));

		if (bSendToSap == false)
			pSubMenu0->RemoveMenu(ID_SENDTOSAP, MF_BYCOMMAND);

		if (bImportShifts == false)
			pSubMenu0->RemoveMenu(IDM_IMPORTSHIFTS, MF_BYCOMMAND);

		if (blReloadBasicData == false)
			pSubMenu0->RemoveMenu(IDM_RELOADBASICDATA, MF_BYCOMMAND);

		if (bSendToSap == false && bImportShifts == false && blReloadBasicData == false)
			pSubMenu0->RemoveMenu(1, MF_BYPOSITION);
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	// Men� 'Programm' �ndern
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	olMenu.ModifyMenu(1,MF_BYPOSITION|MF_STRING,0,LoadStg(ID_M_PROGRAMM));
	CMenu *pSubMenu1 = olMenu.GetSubMenu(1);
	if (pSubMenu1 != NULL)
	{
		pSubMenu1->ModifyMenu(0,MF_BYPOSITION|MF_STRING,ID_VIEW_DUTYROSTER,LoadStg(ID_M_DIENSTPLAN));
		pSubMenu1->ModifyMenu(1,MF_BYPOSITION|MF_STRING,ID_VIEW_SHIFTROSTER,LoadStg(ID_M_SCHICHTPLAN));
		pSubMenu1->ModifyMenu(3,MF_BYPOSITION|MF_STRING,ID_PROGRAMM_STAMMDATEN,LoadStg(ID_M_STAMMDATEN));
		pSubMenu1->ModifyMenu(4,MF_BYPOSITION|MF_STRING,ID_PROGRAMM_GRUPPIERUNG,LoadStg(ID_M_GRUPPIERUNGEN));
		pSubMenu1->ModifyMenu(5,MF_BYPOSITION|MF_STRING,ID_PROGRAMM_STAFFBALANCE,LoadStg(ID_M_STAFFBALANCE));
		pSubMenu1->ModifyMenu(6,MF_BYPOSITION|MF_STRING,ID_PROGRAMM_INITACCOUNT,LoadStg(ID_M_INITACCOUNT));
		pSubMenu1->ModifyMenu(7,MF_BYPOSITION|MF_STRING,ID_PROGRAMM_ABSENCEPLANNING,LoadStg(ID_M_ABSENCEPLANNING));
		
		if(bSchichtplan == false)
			pSubMenu1->RemoveMenu(ID_VIEW_SHIFTROSTER, MF_BYCOMMAND);

		if(bDienstplan == false)
			pSubMenu1->RemoveMenu (ID_VIEW_DUTYROSTER, MF_BYCOMMAND);

		if(bGrouping == false)
			pSubMenu1->RemoveMenu(ID_PROGRAMM_GRUPPIERUNG, MF_BYCOMMAND);

		if(bStammdaten == false)
			pSubMenu1->RemoveMenu(ID_PROGRAMM_STAMMDATEN, MF_BYCOMMAND);

		if(bStaffBalance == false)
			pSubMenu1->RemoveMenu(ID_PROGRAMM_STAFFBALANCE, MF_BYCOMMAND);

		if(bInitAccount == false)
			pSubMenu1->RemoveMenu(ID_PROGRAMM_INITACCOUNT, MF_BYCOMMAND);

		if(bAbsencePlanning == false)
			pSubMenu1->RemoveMenu(ID_PROGRAMM_ABSENCEPLANNING, MF_BYCOMMAND);

		if(bStammdaten == false && bGrouping == false && bStaffBalance == false && bInitAccount == false && bAbsencePlanning == false)
			pSubMenu1->RemoveMenu(2, MF_BYPOSITION);
		
		csCode = ogPrivList.GetStat("MENUDUTYROSTER");
		if(csCode=='0' || csCode=='-')
		{
			pSubMenu1->RemoveMenu (ID_VIEW_DUTYROSTER, MF_BYCOMMAND);
		}

		csCode = ogPrivList.GetStat("MENUSHIFTROSTER");
		if(csCode=='0'|| csCode=='-')
		{
			pSubMenu1->RemoveMenu (ID_VIEW_SHIFTROSTER, MF_BYCOMMAND);
		}

		csCode = ogPrivList.GetStat("MENUSTAMMDATEN");
		if(csCode=='0' || csCode=='-')
		{
			pSubMenu1->RemoveMenu (ID_PROGRAMM_STAMMDATEN, MF_BYCOMMAND);
		}

		csCode = ogPrivList.GetStat("MENUGRUPPIERUNG");
		if(csCode=='0' || csCode=='-')
		{
			pSubMenu1->RemoveMenu (ID_PROGRAMM_GRUPPIERUNG, MF_BYCOMMAND);
		}

		csCode = ogPrivList.GetStat("MENUSTAFFBALANCE");
		if(csCode=='0' || csCode=='-')
		{
			pSubMenu1->RemoveMenu (ID_PROGRAMM_STAFFBALANCE, MF_BYCOMMAND);
		}

		csCode = ogPrivList.GetStat("MENUINITACCOUNT");
		if(csCode=='0' || csCode=='-')
		{
			pSubMenu1->RemoveMenu (ID_PROGRAMM_INITACCOUNT, MF_BYCOMMAND);
		}

		csCode = ogPrivList.GetStat("MENUABSENCEPLANNING");
		if(csCode=='0' || csCode=='-')
		{
			pSubMenu1->RemoveMenu (ID_PROGRAMM_ABSENCEPLANNING, MF_BYCOMMAND);
		}
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	// Men� 'Ansicht' �ndern
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	olMenu.ModifyMenu(2,MF_BYPOSITION|MF_STRING,0,LoadStg(ID_M_ANSICHT));
	CMenu *pSubMenu2 = olMenu.GetSubMenu(2);
	if (pSubMenu2 != NULL)
	{
		pSubMenu2->ModifyMenu(0,MF_BYPOSITION|MF_STRING,ID_VIEW_STATUS_BAR,LoadStg(ID_M_STATUSLEISTE));
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	// Men� '?' �ndern
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	olMenu.ModifyMenu(3,MF_BYPOSITION|MF_STRING,0,LoadStg(ID_M_HILFE));
	CMenu *pSubMenu3 = olMenu.GetSubMenu(3);
	if (pSubMenu3 != NULL)
	{
		pSubMenu3->ModifyMenu(0,MF_BYPOSITION|MF_STRING,ID_HELP_FINDER,LoadStg(ID_M_HILFETHEMEN));
		pSubMenu3->ModifyMenu(1,MF_BYPOSITION|MF_STRING,ID_APP_ABOUT,LoadStg(ID_M_INFO));
	}
	olMenu.Detach();	
	return true;
	CCS_CATCH_ALL;
	return false;
}

//****************************************************************************
// CAboutDlg dialog used for App About
//****************************************************************************

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	DDX_Control(pDX, IDC_STATIC_USER, m_StcUser);
	DDX_Control(pDX, IDC_STATIC_SERVER, m_StcServer);
	DDX_Control(pDX, IDC_STATIC_LOGINTIME, m_StcLogintime);
	DDX_Control(pDX, IDC_COPYRIGHT4, m_Copyright4);
	DDX_Control(pDX, IDC_COPYRIGHT3, m_Copyright3);
	DDX_Control(pDX, IDC_COPYRIGHT2, m_Copyright2);
	DDX_Control(pDX, IDC_COPYRIGHT1, m_Copyright1);
	DDX_Control(pDX, IDC_SERVER,	 m_Server);
	DDX_Control(pDX, IDC_USER,		 m_User);
	DDX_Control(pDX, IDC_LOGINTIME,  m_Logintime);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
//{{AFX_MSG_MAP(CAboutDlg)
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void CRosteringApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

//****************************************************************************
// Daten aufbereiten und darstellen
//****************************************************************************

BOOL CAboutDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	VersionInfo rlInfo;

	VersionInfo::GetVersionInfo(NULL,rlInfo);	// must use NULL (and it works), because AFX is not initialized yet and will cause an ASSERT
	CString olVersionString;
	//Rostering  4.4.4.11 / 06.05.2002 => Rostering  4.4.4.11 / May 06 2002
	olVersionString.Format("Rostering %s / %s", rlInfo.omFileVersion, __DATE__);

	// Statics einstellen
	m_Copyright1.SetWindowText(rlInfo.omProductName/*LoadStg(IDS_COPYRIGHT1)*/);
	m_Copyright2.SetWindowText(olVersionString);
	m_Copyright3.SetWindowText(rlInfo.omLegalCopyright/*LoadStg(IDS_COPYRIGHT3)*/);
	m_Copyright4.SetWindowText(LoadStg(IDS_COPYRIGHT4));
	m_StcServer.SetWindowText(LoadStg(IDS_STATIC_SERVER));
	m_StcUser.SetWindowText(LoadStg(IDS_STATIC_USER));
	m_StcLogintime.SetWindowText(LoadStg(IDS_STATIC_LOGINTIME));
	
	CString olServer = ogCommHandler.pcmRealHostName;
	olServer  += " / ";
	olServer  += ogCommHandler.pcmRealHostType;
	m_Server.SetWindowText(olServer);
	
	m_User.SetWindowText(CString(pcgUser));
	m_Logintime.SetWindowText(ogLoginTime.Format("%d.%m.%Y  %H:%M"));
	
	SetWindowText(LoadStg(IDS_ROSTERING_ABOUTBOX));
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}



void CRosteringApp::WinHelp(DWORD dwData, UINT nCmd) 
{
	// TODO: Add your specialized code here and/or call the base class

	AatHelp::WinHelp(dwData, nCmd);
	//CWinApp::WinHelp(dwData, nCmd);
}
