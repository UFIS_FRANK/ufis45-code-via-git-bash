// CedaSorData.cpp
 
#include <stdafx.h>

CedaSorData ogSorData;

void ProcessSorCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

static int CompareTimes(const SORDATA **e1, const SORDATA **e2);
static int CompareSurnAndTimes(const SORDATA **e1, const SORDATA **e2);
static int CompareCodeAndTimes(const SORDATA **e1, const SORDATA **e2);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaSorData::CedaSorData() : CedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for SORDataStruct
	BEGIN_CEDARECINFO(SORDATA,SORDataRecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_LONG		(Surn,"SURN")
		FIELD_CHAR_TRIM	(Code,"CODE")
		FIELD_OLEDATE	(Vpfr,"VPFR")
		FIELD_OLEDATE	(Vpto,"VPTO")
	END_CEDARECINFO //(SORDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(SORDataRecInfo)/sizeof(SORDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&SORDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"SOR");
	strcat(pcmTableName,pcgTableExt);
	strcpy(pcmListOfFields,"URNO,SURN,CODE,VPFR,VPTO");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omDataSur.RemoveAll();
	Register();
}

//---------------------------------------------------------------------------------------------------------

void CedaSorData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaSorData::Register(void)
{
	DdxRegister((void *)this,BC_SOR_CHANGE,	"SORDATA", "Sor-changed",	ProcessSorCf);
	DdxRegister((void *)this,BC_SOR_NEW,	"SORDATA", "Sor-new",		ProcessSorCf);
	DdxRegister((void *)this,BC_SOR_DELETE,	"SORDATA", "Sor-deleted",	ProcessSorCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaSorData::~CedaSorData(void)
{
	TRACE("CedaSorData::~CedaSorData called\n");
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaSorData::ClearAll(bool bpWithRegistration)
{
	TRACE("CedaSorData::ClearAll called\n");
    omUrnoMap.RemoveAll();
    omDataSur.DeleteAll();
    //omDataOrg.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaSorData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omDataSur.RemoveAll();
    omDataOrg.DeleteAll();
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(pspWhere == NULL)
	{	
		ilRc = CedaAction2("RT");
	}
	else
	{
		ilRc = CedaAction2("RT", pspWhere);
	}
	if (ilRc != true)
	{
		TRACE("Read-Sor: Ceda-Error %d \n",ilRc);
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
	{
		SORDATA *prlSor = new SORDATA;
		if ((ilRc = GetFirstBufferRecord2(prlSor)) == true)
		{
			if(IsValidSor(prlSor))
			{
				omDataSur.Add(prlSor);//Update omDataSur
				omDataOrg.Add(prlSor);//Update omDataOrg
				omUrnoMap.SetAt((void *)prlSor->Urno,prlSor);
#ifdef TRACE_FULL_DATA
				// Datensatz OK, loggen if FULL
				ogRosteringLogText.Format(" read %8.8lu %s OK:  ", ilCountRecord, pcmTableName);
				GetDataFormatted(ogRosteringLogText, prlSor);
				WriteLogFull("");
#endif TRACE_FULL_DATA
			}
			else
			{
				delete prlSor;
			}
		}
		else
		{
			delete prlSor;
		}
	}
	TRACE("Read-Sor: %d gelesen\n",ilCountRecord-1);

	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  read %d records from table %s",ilCountRecord-1, pcmTableName);
		WriteInRosteringLog();
	}
	
	// wir sortieren alles nach Surn & Time - das beschleunigt das Lesen zigfach
	omDataSur.Sort(CompareSurnAndTimes);
	omDataOrg.Sort(CompareCodeAndTimes);

	ClearFastSocketBuffer();	
	return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaSorData::Insert(SORDATA *prpSor)
{
	prpSor->IsChanged = DATA_NEW;
	if(Save(prpSor) == false) return false; //Update Database
	InsertInternal(prpSor);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaSorData::InsertInternal(SORDATA *prpSor)
{
	ogDdx.DataChanged((void *)this, SOR_NEW,(void *)prpSor ); //Update Viewer
	omDataSur.Add(prpSor);//Update omDataSur
	omDataOrg.Add(prpSor);//Update omDataOrg
	omUrnoMap.SetAt((void *)prpSor->Urno,prpSor);
	
	// wir sortieren alles nach Surn/Org & Time - das beschleunigt das Lesen zigfach
	omDataSur.Sort(CompareSurnAndTimes);
	omDataOrg.Sort(CompareCodeAndTimes);

    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaSorData::Delete(long lpUrno)
{
	SORDATA *prlSor = GetSorByUrno(lpUrno);
	if (prlSor != NULL)
	{
		prlSor->IsChanged = DATA_DELETED;
		if(Save(prlSor) == false) return false; //Update Database
		DeleteInternal(prlSor);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaSorData::DeleteInternal(SORDATA *prpSor)
{
	ogDdx.DataChanged((void *)this,SOR_DELETE,(void *)prpSor); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpSor->Urno);

	int ilCountRecord = FindFirstOfSurn(prpSor->Surn);
	int ilSorCount = omDataSur.GetSize();
	if(ilCountRecord >= 0)
	{
		for (; ilCountRecord < ilSorCount; ilCountRecord++)
		{
			if (omDataSur[ilCountRecord].Urno == prpSor->Urno)
			{
				omDataSur.DeleteAt(ilCountRecord);//Update omDataSur
				break;
			}
		}
	}

	ilCountRecord = FindFirstOfCode(prpSor->Code);
	if(ilCountRecord >= 0)
	{
		// ilSorCount = omDataSur.GetSize(); - beide Arrays sind gleich
		for (; ilCountRecord < ilSorCount; ilCountRecord++)
		{
			if (omDataOrg[ilCountRecord].Urno == prpSor->Urno)
			{
				omDataOrg.RemoveAt(ilCountRecord);//Update omDataOrg
				break;
			}
		}
	}

    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaSorData::Update(SORDATA *prpSor)
{
	if (GetSorByUrno(prpSor->Urno) != NULL)
	{
		if (prpSor->IsChanged == DATA_UNCHANGED)
		{
			prpSor->IsChanged = DATA_CHANGED;
		}
		if(Save(prpSor) == false) return false; //Update Database
		UpdateInternal(prpSor);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaSorData::UpdateInternal(SORDATA *prpSor)
{
	SORDATA *prlSor = GetSorByUrno(prpSor->Urno);
	if (prlSor != NULL)
	{
		*prlSor = *prpSor; //Update omDataSur
		ogDdx.DataChanged((void *)this,SOR_CHANGE,(void *)prlSor); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

SORDATA *CedaSorData::GetSorByUrno(long lpUrno)
{
	SORDATA  *prlSor;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlSor) == TRUE)
	{
		return prlSor;
	}
	return NULL;
}

//---------------------------------------------------------------------------------------------------------

void CedaSorData::GetSorBySurnWithTime(long lpSurn,CTime opStart,CTime opEnd,CCSPtrArray<SORDATA> *popSorData)
{
	SORDATA  *prlSor;
	COleDateTime olStart,olEnd;

	popSorData->DeleteAll();
	
	CCSPtrArray<SORDATA> olSorData;
	POSITION rlPos;
	for ( rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSor);
		if((prlSor->Surn == lpSurn))
		{
			olSorData.Add(prlSor); 
		}
	}
	int ilSize = olSorData.GetSize();
	if(ilSize > 0)
	{
		olSorData.Sort(CompareTimes);
		
		olStart.SetDateTime(opStart.GetYear(),opStart.GetMonth(),opStart.GetDay(),opStart.GetHour(),opStart.GetMinute(),opStart.GetSecond());
		olEnd.SetDateTime(opEnd.GetYear(),opEnd.GetMonth(),opEnd.GetDay(),opEnd.GetHour(),opEnd.GetMinute(),opEnd.GetSecond());
		SORDATA *prlPrevSor=NULL;
		for(int i=ilSize;--i>=0;)
		{
			prlSor = &olSorData[i];
			// ARE: <prlSor->Vpfr> hat Status NULL -> COleDateTime-Exception.
			// Status NULL OK? Oder korrupte Datensatz-Leichen? Oder hat Wert = NULL
			// bestimmte Bedeutung (z.B. immer g�ltig)?
			if((prlSor->Vpfr.GetStatus() == COleDateTime::valid) && (prlSor->Vpfr<=olEnd))
			{
				switch(prlSor->Vpto.GetStatus())
				{
				case COleDateTime::valid:
					if(prlSor->Vpto>=olStart)
					{
						popSorData->Add(prlSor);
					}
					else
					{
						i=0;
					}
					break;
				case COleDateTime::null:
					if(prlPrevSor!=NULL)
					{
						if(prlPrevSor->Vpfr>=olStart)
						{
							popSorData->Add(prlSor);
						}
						else
						{
							i=0;
						}
					}
					else
					{
						popSorData->Add(prlSor);
					}
					break;
				default:
					break;
				}
			}
			prlPrevSor = prlSor;
		}
	}
}

//---------------------------------------------------------------------------------------

CString CedaSorData::GetOrgBySurnWithTime(long lpSurn, COleDateTime opDate)
{
	CString olOrg("");

	if(opDate.GetStatus() != COleDateTime::valid) return olOrg;

	SORDATA  *prlSor = 0;

	int ilCount = FindFirstOfSurn(lpSurn);
	if(ilCount == -1) return olOrg;	// lpSurn nicht gefunden

	for(; ilCount < omDataSur.GetSize(); ilCount++)
	{
		prlSor = (SORDATA*)omDataSur.CPtrArray::GetAt(ilCount);

		if(prlSor->Surn > lpSurn) 
			break;		// omDataSur ist ja nach Surn-Vpfr sortiert
		
		if(prlSor->Vpfr.GetStatus() != COleDateTime::valid) 
			continue;	// 1. Defekter Datensatz
		
		if(prlSor->Vpfr <= opDate)
		{
			switch(prlSor->Vpto.GetStatus())
			{
			default:
				// case COleDateTime::invalid:
				continue;	// gibts nicht
			case COleDateTime::valid:
				if(prlSor->Vpto < opDate) 
					continue;	// 3. Ende der G�ltigkeitszeit liegt vor dem opStart
				break;
			case COleDateTime::null:
				break;	// keine Endzeit, keine Beschr�nkung
			}
			olOrg = prlSor->Code;	// gefunden!
			// wir pr�fen weiter, ob da noch passende Datens�tze vorliegen
		}
		else
			break;	// 2. Anfang der G�ltigkeitszeit liegt nach dem opDate, kein Datensatz mehr kann uns interessieren
	}
	return olOrg;
}

//---------------------------------------------------------------------------------------------------------

void CedaSorData::GetSorArrayByOrgArrayWithTime(CStringArray *popOrg, COleDateTime opStart, COleDateTime opEnd, CCSPtrArray<SORDATA> *popSorData)
{
	if(!popSorData) return;
	popSorData->DeleteAll();
	
	if(popOrg->GetSize() > 0 && opStart.GetStatus() == COleDateTime::valid && opEnd.GetStatus() == COleDateTime::valid && opStart <= opEnd)
	{
		SORDATA* prlSor;
		SORDATA* prlSorFound = 0;
		int ilCodeCount;
		CString olThisOrg;
		long llUrno = 0;
		for(int ilOrg = 0; ilOrg < popOrg->GetSize(); ilOrg++)
		{
			olThisOrg = popOrg->GetAt(ilOrg);
			ilCodeCount = FindFirstOfCode(olThisOrg);
			if(ilCodeCount == -1) continue;	// Code nicht gefunden
			for(; ilCodeCount < omDataOrg.GetSize(); ilCodeCount++)
			{
				prlSor = (SORDATA*)omDataOrg.CPtrArray::GetAt(ilCodeCount);
				if(!prlSor) continue;	// waaaas?
				if(prlSor->Code != olThisOrg)
				{
					llUrno = 0;
					continue;	// Ende dieser Org-Auflistung
				}
				if(prlSor->Surn == llUrno)
					continue;	// schon gecheckt

				llUrno = prlSor->Surn;
				int ilUrnoCount = FindFirstOfSurn(llUrno);
				if(ilCodeCount == -1) continue;	// Code nicht gefunden
				
				// letzten (aktuellen) Datensatz suchen und vergleichen
				for(; ilUrnoCount < omDataSur.GetSize(); ilUrnoCount++)
				{
					prlSor = (SORDATA*)omDataSur.CPtrArray::GetAt(ilUrnoCount);
					
					if(prlSor->Surn != llUrno)
						break;
					if(prlSor->Vpfr.GetStatus() != COleDateTime::valid) 
						continue;	// 1. Defekter Datensatz
					
					if(prlSor->Vpfr <= opEnd)
					{
						switch(prlSor->Vpto.GetStatus())
						{
						default:
							// case COleDateTime::invalid:
							continue;	// gibts nicht
						case COleDateTime::valid:
							if(prlSor->Vpto < opStart) 
								continue;	// 3. Ende der G�ltigkeitszeit liegt vor dem opStart
							break;
						case COleDateTime::null:
							break;	// keine Endzeit, keine Beschr�nkung
						}
						prlSorFound = prlSor;	// gefunden!
						// wir pr�fen weiter, ob da noch passende Datens�tze vorliegen
					}
					else
						break;	// 2. Anfang der G�ltigkeitszeit liegt nach dem opEnd, kein Datensatz mehr kann uns interessieren
				}
				
				if(prlSorFound != 0)
				{
					// wir haben jetzt den g�ltigen Datensatz dieses MAs geholt,
					// checken, ob es der gesuchte ist
					if(olThisOrg == prlSorFound->Code)
					{
						// ja, speichern
						popSorData->Add(prlSorFound);
					}
					prlSorFound = 0;
				}
			}
		}
	}
}

//---------------------------------------------------------------------------------------------------------
// GetSorWithoutOrgWithTime:
// Gibt eine List aller MA's aus, die in einem bestimmten Zeitraum die keiner Organisation angeh�ren
//---------------------------------------------------------------------------------------------------------

CString CedaSorData::GetStfWithoutOrgWithTime(COleDateTime opStart, COleDateTime opEnd, CCSPtrArray<STFDATA> *popStfData)
{
	int ilStfSize = ogStfData.omUrnoMap.GetCount();
	CMapStringToPtr olSorMap;
	popStfData->DeleteAll();
	CString olUrnos = "";

	if(ilStfSize > 0 && opStart.GetStatus() == COleDateTime::valid && opEnd.GetStatus() == COleDateTime::valid && opStart <= opEnd)
	{
		// 1. Schritt: Alle MA's suchen die im Zeitraum eine Organisation haben
		COleDateTime olTmpStart,olTmpEnd;
		SORDATA *prlSor = NULL;
		CString olTmpUrno;

		CCSPtrArray<SORDATA> olSorData;
		POSITION rlPos;
		for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSor);
			olSorData.Add(prlSor); 
		}

		olSorData.Sort(CompareSurnAndTimes);
		int ilSize = olSorData.GetSize();
		SORDATA *prlNextSor = NULL;
		void  *prlVoid = NULL;

		for(int i = 0; i < ilSize; i++)
		{
			prlSor = &olSorData[i];
			prlNextSor = NULL;
			if((i+1) < ilSize)
			{
				if(prlSor->Surn == olSorData[i+1].Surn)
					prlNextSor = &olSorData[i+1];
			}

			if(prlSor->Vpfr.GetStatus() == COleDateTime::valid)
			{
				if(prlSor->Vpfr<=opEnd)
				{
					switch(prlSor->Vpto.GetStatus())
					{
					case COleDateTime::valid:
						if(prlSor->Vpto>=opStart)
						{
							olTmpUrno.Format("%ld",prlSor->Surn);
							olSorMap.SetAt(olTmpUrno,NULL);
						}
						break;
					case COleDateTime::null:
						if(prlNextSor != NULL)
						{
							if(prlNextSor->Vpfr>=opStart)
							{
								olTmpUrno.Format("%ld",prlSor->Surn);
								olSorMap.SetAt(olTmpUrno,NULL);
							}
						}
						else
						{
							olTmpUrno.Format("%ld",prlSor->Surn);
							olSorMap.SetAt(olTmpUrno,NULL);
						}
						break;
					default:
						break;
					}
				}
			}
		}
		olSorData.RemoveAll();
		// 2. Schritt: Liste aller MA's erstellen, die nicht in der Liste der MA's mit Organisation sind
		STFDATA *prlStf = NULL;
		for(rlPos = ogStfData.omUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			ogStfData.omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlStf);
			olTmpUrno.Format("%ld", prlStf->Urno);

			if(olSorMap.Lookup(olTmpUrno,(void *&)prlVoid) == FALSE)
			{
				popStfData->Add(prlStf);
			}
		}
		// Create Stf-Urno-List for return value
		ilSize = popStfData->GetSize();
		olUrnos = ",";
		for(i = 0; i < ilSize; i++)
		{
			CString olTmpUrno;
			olTmpUrno.Format(",%ld,",(*popStfData)[i].Urno);
			if(olUrnos.Find(olTmpUrno) == -1)
			{
				olUrnos += olTmpUrno.Mid(1);
			}
		}
		if(olUrnos.IsEmpty() == FALSE)
		{
			olUrnos = olUrnos.Mid(1,olUrnos.GetLength()-2);
		}
		olSorMap.RemoveAll();
	}
	return olUrnos;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaSorData::ReadSpecialData(CCSPtrArray<SORDATA> *popSor,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}

	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction2("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popSor != NULL)
	{
		for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
		{
			SORDATA *prpSor = new SORDATA;
			if ((ilRc = GetBufferRecord2(ilCountRecord,prpSor,CString(pclFieldList))) == true)
			{
				// BDA: Vpfr & Vpto k�nnen auch mal null sein, es ist falsch!
				// ARE: Vpto kann auch mal null sein, es ist OK! aber auf keinen Fall ung�ltig
				if(prpSor->Vpfr.GetStatus() != COleDateTime::valid || prpSor->Vpto.GetStatus() == COleDateTime::invalid)
				{
					TRACE("Read-Sor: %d gelesen, FEHLER IM DATENSATZ, Surn=%ld\n",ilCountRecord-1,prpSor->Surn);
					delete prpSor;
					continue;
				}
				//else
					//TRACE("Read-Sor: %d gelesen, KEIN FEHLER\n",ilCountRecord-1);
				popSor->Add(prpSor);
			}
			else
			{
				delete prpSor;
			}
		}
		if(popSor->GetSize() == 0) return false;
	}
	ClearFastSocketBuffer();	
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaSorData::Save(SORDATA *prpSor)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpSor->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpSor->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpSor);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpSor->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSor->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpSor);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpSor->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSor->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
	TRACE("Sor-IRT/URT/DRT: Ceda-Return %d \n",ilRc);
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessSorCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogSorData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaSorData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlSorData;
	prlSorData = (struct BcStruct *) vpDataPointer;
	SORDATA *prlSor;
	
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("ProcessBC:\n  Cmd <%s>\n  Tbl <%s>\n  Twe <%s>\n  Sel <%s>\n  Fld <%s>\n  Dat <%s>\n  Bcn <%s>",prlSorData->Cmd, prlSorData->Object, prlSorData->Twe, prlSorData->Selection, prlSorData->Fields, prlSorData->Data,prlSorData->BcNum);
		WriteInRosteringLog();
	}
	
	switch(ipDDXType)
	{
	default:
		break;
	case BC_SOR_CHANGE:
		prlSor = GetSorByUrno(GetUrnoFromSelection(prlSorData->Selection));
		if(prlSor != NULL)
		{
			GetRecordFromItemList(prlSor,prlSorData->Fields,prlSorData->Data);
			UpdateInternal(prlSor);
			break;
		}
	case BC_SOR_NEW:
		prlSor = new SORDATA;
		GetRecordFromItemList(prlSor,prlSorData->Fields,prlSorData->Data);
		InsertInternal(prlSor);
		break;
	case BC_SOR_DELETE:
		{
			long llUrno;
			CString olSelection = (CString)prlSorData->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlSorData->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			prlSor = GetSorByUrno(llUrno);
			if (prlSor != NULL)
			{
				DeleteInternal(prlSor);
			}
		}
		break;
	}
}

//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------

static int CompareTimes(const SORDATA **e1, const SORDATA **e2)
{
	if((**e1).Vpfr>(**e2).Vpfr)
		return 1;
	else
		return -1;
}

//---------------------------------------------------------------------------------------------------------

static int CompareSurnAndTimes(const SORDATA **e1, const SORDATA **e2)
{
	if((**e1).Surn>(**e2).Surn) 
		return 1;
	else if((**e1).Surn<(**e2).Surn) 
		return -1;

	if((**e1).Vpfr>(**e2).Vpfr) 
		return 1;
	else if((**e1).Vpfr<(**e2).Vpfr) 
		return -1;

	return 0;
}

//---------------------------------------------------------------------------------------------------------

static int CompareCodeAndTimes(const SORDATA **e1, const SORDATA **e2)
{
	int il = strcmp((**e1).Code,(**e2).Code);
	if(il != 0)
		return il;

	if((**e1).Vpfr>(**e2).Vpfr) 
		return 1;
	else if((**e1).Vpfr<(**e2).Vpfr) 
		return -1;

	return 0;
}

/*****************************************************************************
Sehr schnelles Suchen vom ersten Datensatz mit der angegebenen Surn
! wir nutzen die Tatsache, das omDataSur nach Surn & Time sortiert ist !
*****************************************************************************/
int CedaSorData::FindFirstOfSurn(long lpSurn)
{
	int ilSize = omDataSur.GetSize();
	if(!ilSize) 
		return -1;
	
	int ilSearch;

	if(omDataSur[ilSize-1].Surn != omDataSur[0].Surn)
	{
		if(ilSize > 16)
		{
			// rekursives Suchen
			ilSearch = ilSize >> 1;
			int ilUp, ilDown;
			
			for(ilUp = ilSize-1, ilDown = 0;;)
			{
				if(lpSurn == omDataSur[ilSearch].Surn)
				{
					if(!ilSearch || lpSurn != omDataSur[ilSearch-1].Surn)
					{
						// gefunden!
						return ilSearch;
					}

					// lineares Suchen nach unten
					for(ilSearch--; ilSearch>=0;ilSearch--)
					{
						if(!ilSearch || lpSurn != omDataSur[ilSearch-1].Surn)
						{
							// gefunden!
							return ilSearch;
						}
					}
				}
				
				if(ilUp-ilDown <= 1)
				{
					if(ilUp == ilDown)
					{
						// kein Surn in der Liste vorhanden!
						return -1;
					}

					if(lpSurn == omDataSur[ilUp].Surn)
						return ilUp;
					else if(lpSurn == omDataSur[ilDown].Surn)
						return ilDown;
					else 
						return -1;
				}

				if(lpSurn < omDataSur[ilSearch].Surn)
				{
					ilUp = ilSearch;
					ilSearch = (ilUp + ilDown) >> 1;	// == (ilUp + ilDown) / 2
				}
				else //  if(lpSurn > omDataSur[ilSearch].Surn)
				{
					ilDown = ilSearch;
					ilSearch = (ilUp + ilDown) >> 1;	// == (ilUp + ilDown) / 2
				}
			}
		}
		else
		{
			// lineares Suchen
			for(int ilSearch=0; ilSearch<ilSize; ilSearch++)
			{
				if(omDataSur[ilSearch].Surn == lpSurn)
					return ilSearch;
			}
		}
	}
	else
	{
		return 0;	// alle gleich
	}
	
	return -1;	// kein gefunden!
}

/*****************************************************************************
Sehr schnelles Suchen vom ersten Datensatz mit dem angegebenen Code
! wir nutzen die Tatsache, das omDataOrg nach Code & Time sortiert ist !
*****************************************************************************/
int CedaSorData::FindFirstOfCode(LPCTSTR popCode)
{
	int ilSize = omDataOrg.GetSize();
	if(!ilSize) 
		return -1;
	
	int ilSearch;

	if(strcmp(omDataOrg[ilSize-1].Code,omDataOrg[0].Code))
	{
		if(ilSize > 16)
		{
			// rekursives Suchen
			ilSearch = ilSize >> 1;
			int ilUp, ilDown;
			
			for(ilUp = ilSize-1, ilDown = 0;;)
			{
				if(!strcmp(popCode,omDataOrg[ilSearch].Code))
				{
					if(!ilSearch || strcmp(popCode,omDataOrg[ilSearch-1].Code))
					{
						// gefunden!
						return ilSearch;
					}

					// lineares Suchen nach unten
					for(ilSearch--; ilSearch>=ilDown;ilSearch--)
					{
						if(!ilSearch || strcmp(popCode,omDataOrg[ilSearch-1].Code))
						{
							// gefunden!
							return ilSearch;
						}
					}
				}
				
				if(ilUp-ilDown <= 1)
				{
					if(ilUp == ilDown)
					{
						// kein Surn in der Liste vorhanden!
						return -1;
					}

					if(!strcmp(popCode,omDataOrg[ilUp].Code))
						return ilUp;
					else if(!strcmp(popCode,omDataOrg[ilDown].Code))
						return ilDown;
					else 
						return -1;
				}

				if(strcmp(popCode,omDataOrg[ilSearch].Code) < 0)
				{
					ilUp = ilSearch;
					ilSearch = (ilUp + ilDown) >> 1;	// == (ilUp + ilDown) / 2
				}
				else //  if(strcmp(popCode,omDataOrg[ilSearch].Code) > 0)
				{
					ilDown = ilSearch;
					ilSearch = (ilUp + ilDown) >> 1;	// == (ilUp + ilDown) / 2
				}
			}
		}
		else
		{
			// lineares Suchen
			for(int ilSearch=0; ilSearch<ilSize; ilSearch++)
			{
				if(!strcmp(popCode,omDataOrg[ilSearch].Code))
					return ilSearch;
			}
		}
	}
	else
	{
		return 0;	// alle gleich
	}
	
	return -1;	// kein gefunden!
}

/**********************************************************************
**********************************************************************/
bool CedaSorData::IsValidSor(SORDATA *prpSor)
{
	// long			Surn;			// Mitarbeiter-Urno
	if(!ogStfData.GetStfByUrno(prpSor->Surn))
	{
		CString olErr;
		olErr.Format("Organisation Connection Table (SORTAB) URNO '%d' is defect, Employee with URNO '%d' in STFTAB not found\n",
			prpSor->Urno, prpSor->Surn);
		ogErrlog += olErr;
		
		if(IsTraceLoggingEnabled())
		{
			GetDefectDataString(ogRosteringLogText, (void*)prpSor, "SORTAB.SURN in STFTAB not found");
			WriteInRosteringLog(LOGFILE_TRACE);
		}
		return false;	// 1. Defekter Datensatz
	}
	
	// char			Code[7];		// Organisation Code
	if(!ogOrgData.GetOrgByDpt1(CString(prpSor->Code)))
	{
		STFDATA* prlStf = ogStfData.GetStfByUrno(prpSor->Surn);

		CString olErr;
		olErr.Format("Organisation code '%s' for %s,%s in Organisational Units Table not found\n",
			prpSor->Code, prlStf->Lanm, prlStf->Finm);
		ogErrlog += olErr;
		
		if(IsTraceLoggingEnabled())
		{
			GetDefectDataString(ogRosteringLogText, (void*)prpSor, "SORTAB.CODE in ORGTAB not found");
			WriteInRosteringLog(LOGFILE_TRACE);
		}
		return false;	// 1. Defekter Datensatz
	}

	// COleDateTime	Vpfr;
	// COleDateTime	Vpto;
	if(prpSor->Vpfr.GetStatus() != COleDateTime::valid || prpSor->Vpto.GetStatus() == COleDateTime::invalid)
	{
		STFDATA* prlStf = ogStfData.GetStfByUrno(prpSor->Surn);

		CString olErr;
		olErr.Format("Organisation time for %s,%s is not valid\n",
			prlStf->Lanm, prlStf->Finm);
		ogErrlog += olErr;
		
		if(IsTraceLoggingEnabled())
		{
			GetDefectDataString(ogRosteringLogText, (void*)prpSor, "SORTAB.VPFR or SORTAB.VPTO is not valid");
			WriteInRosteringLog(LOGFILE_TRACE);
		}
		return false;	// 1. Defekter Datensatz
	}
	
	return true;
}