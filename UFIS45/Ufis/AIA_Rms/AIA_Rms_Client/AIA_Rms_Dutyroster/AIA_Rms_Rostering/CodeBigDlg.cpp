// CodeBigDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

///////////////////////////////////////////////////////////////////////////// 
// Dialogfeld CCodeBigDlg 


CCodeBigDlg::CCodeBigDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCodeBigDlg::IDD, pParent)
{
	pomBsdGrid = NULL;
	pomOdaGrid = NULL;
	pomWisGrid = NULL;

	// Von/Bis Werte auf null setzen.
	omFromTime.SetStatus(COleDateTime::null);
	omToTime.SetStatus(COleDateTime::null);

	// Grid ist nicht sauber !
	bmIsDirty = true;

	bmAllCodes = false;


	// W�nsche unbedingt laden:
	bmShowWish = true;

	// Nicht-modaler Dialog mu� per CDialog::Create() erzeugt werden
	CDialog::Create(CCodeBigDlg::IDD, pParent);
		
	//{{AFX_DATA_INIT(CCodeBigDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

//************************************************************************************
//
//************************************************************************************

CCodeBigDlg::~CCodeBigDlg()
{
	if (pomBsdGrid != NULL)
		delete pomBsdGrid;

	if (pomOdaGrid != NULL)
		delete pomOdaGrid;

	if (pomWisGrid != NULL)
		delete pomWisGrid;
}

//************************************************************************************
// �bergabe des g�ltigen Organisations Codes
//************************************************************************************

void CCodeBigDlg::SetOrgCodes(CString opOrgList) 
{
	if (omOrgList != opOrgList)
	{
		omOrgList = opOrgList;
		// Grid mu� neu gezeichnet
		bmIsDirty = true;
	}
}

//************************************************************************************
// �bergabe des g�ltigen Funktions Codes
//************************************************************************************

void CCodeBigDlg::SetPfcCodes(CString opPfcList)
{
	if (omPfcList != opPfcList)
	{
		omPfcList = opPfcList;
		// Grid mu� neu gezeichnet
		bmIsDirty = true;
	}
}

//************************************************************************************
// �bergabe des G�ltigkeitsbereiches f�r Schichten
//************************************************************************************

void CCodeBigDlg::ChangeValidDates(COleDateTime opFrom,COleDateTime opTo)
{
	// Pr�fen, ob eine Ver�nderung vorliegt.
	if (omFromTime == opFrom && omToTime == opTo)
		return;

	// Grid mu� neu gezeichnet
	bmIsDirty = true;

	omFromTime = opFrom;
	omToTime = opTo;
}

//************************************************************************************
// Grid neu laden
//************************************************************************************

void CCodeBigDlg::ReloadAndShow(bool bpAllCodes)
{
	if (bmIsDirty || bpAllCodes != bmAllCodes)
	{

		
		//-----------------------------------------------------------------------------------
		// Grid l�schen
		// Readonly ausschalten
		pomBsdGrid->GetParam()->SetLockReadOnly(false);

		// Anzahl der Rows ermitteln
		int ilRowCount = pomBsdGrid->GetRowCount();
		if (ilRowCount > 0)
			// Grid l�schen			
			pomBsdGrid->RemoveRows(1,ilRowCount);
		//-----------------------------------------------------------------------------------
		// Grid f�llen
		// Sollte die Liste der anzuzeigenen OrgCodes leer sein, mu� nix angezeigt werden
		//if (!omOrgList.IsEmpty()) 
		FillBsdGrid(bpAllCodes);

		bmIsDirty = false;
		bmAllCodes = bpAllCodes;
	}

	FillOdaGrid();
	FillWishGrid();
}

//************************************************************************************
//
//************************************************************************************

void CCodeBigDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCodeBigDlg)
		// HINWEIS: Der Klassen-Assistent f�gt hier DDX- und DDV-Aufrufe ein
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCodeBigDlg, CDialog)
	//{{AFX_MSG_MAP(CCodeBigDlg)
	ON_MESSAGE(WM_GRID_DRAGBEGIN, OnGridDragBegin)
	ON_BN_CLICKED(IDC_BUTTONFINDBSD, On_B_FindBSD)
	ON_BN_CLICKED(IDC_BUTTONFINDODA, On_B_FindODA)
	ON_BN_CLICKED(IDC_BUTTONFINDWIS, On_B_FindWIS)
	ON_WM_TIMER()
	ON_WM_NCLBUTTONDOWN()
	ON_WM_NCPAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

//************************************************************************************
//
//************************************************************************************

void CCodeBigDlg::OnOK() 
{
	// Grid beenden
	/*
	pomBsdGrid->DestroyWindow();
	pomOdaGrid->DestroyWindow();
	pomWisGrid->DestroyWindow();
	
	CDialog::OnOK();*/
}

//****************************************************************************************************
// OnCancel: macht nichts. Wird �berschrieben, um unerw�nschtes Verhalten 
//  zu unterdr�cken.
//  R�ckgabe:	keine
//****************************************************************************************************

void CCodeBigDlg::OnCancel() 
{
	//do nothing
}

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CCodeBigDlg 

BOOL CCodeBigDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// Grid initialisieren
	IniGrid();
	
	// Titel setzen
	SetWindowText(LoadStg(IDS_STRING397));

	// Close "x" in der Titelleiste l�schen
	CMenu* pmenuSystem = this->GetSystemMenu(FALSE);
	if (pmenuSystem != NULL)
	{
		 pmenuSystem->DeleteMenu(SC_CLOSE, MF_BYCOMMAND);
	}

	// Fenster anzeigen
//	ShowWindow(SW_SHOWNORMAL);

	// Darstellung eines Buttons in der Titelleiste, Time f�r die aktualisierung anstossen.
	SetTimer(TIMER_REFRESH, 180, NULL);
	
	return TRUE;  
}

//************************************************************************************
// Ver�ndert den Fenstertitel
//************************************************************************************

void CCodeBigDlg::SetCaptionInfo(CString opInfo)
{ 
	// Titel setzen
	SetWindowText(LoadStg(IDS_STRING397) + opInfo);
}

//**********************************************************************************
// Grid initialisieren
//**********************************************************************************

void CCodeBigDlg::IniGrid ()
{
CCS_TRY;
	//-----------------------------------------------------------------------------
	// BSD Grid erzeugen
	pomBsdGrid = new CGridControl ( this,IDC_BSDGRID,COLCOUNT_BSD,0);
	// Header setzen
	pomBsdGrid->SetValueRange(CGXRange(0,1), LoadStg(IDS_STRING1619));
	pomBsdGrid->SetValueRange(CGXRange(0,2), LoadStg(SHIFT_BSD_ESBG));
	pomBsdGrid->SetValueRange(CGXRange(0,3), LoadStg(SHIFT_BSD_LSEN));
	pomBsdGrid->SetValueRange(CGXRange(0,4), LoadStg(IDS_STRING615));
	pomBsdGrid->SetValueRange(CGXRange(0,5), LoadStg(SHIFT_BSD_BKF1));
	pomBsdGrid->SetValueRange(CGXRange(0,6), LoadStg(SHIFT_BSD_BKT1));
	pomBsdGrid->SetValueRange(CGXRange(0,7), LoadStg(SHIFT_BSD_BKD1));
	pomBsdGrid->SetValueRange(CGXRange(0,8), LoadStg(SHIFT_BSD_TYPE));
	pomBsdGrid->SetValueRange(CGXRange(0,9), LoadStg(IDS_STRING318));
	pomBsdGrid->SetValueRange(CGXRange(0,10), LoadStg(IDS_STRING323));

	//-----------------------------------------------------------------------------
	// ODA Grid erzeugen
	pomOdaGrid = new CGridControl ( this,IDC_ODAGRID,COLCOUNT_ODA,ogOdaData.omData.GetSize());
	// Header setzen
	pomOdaGrid->SetValueRange(CGXRange(0,1), LoadStg(IDS_STRING1620));
	pomOdaGrid->SetValueRange(CGXRange(0,2), LoadStg(IDS_STRING461));
	//-----------------------------------------------------------------------------
	// Wis Grid erzeugen
	pomWisGrid = new CGridControl ( this,IDC_WISGRID,COLCOUNT_WIS,ogWisData.omData.GetSize());
	// Header setzen
	pomWisGrid->SetValueRange(CGXRange(0,1), LoadStg(IDS_STRING325));
	pomWisGrid->SetValueRange(CGXRange(0,2), LoadStg(IDS_STRING461));
	//-----------------------------------------------------------------------------

	// Breite der Spalten einstellen
	IniColWidths ();
	// Grid mit Daten f�llen.
	FillWishGrid();

	// Tooltips aktivieren
	pomBsdGrid->EnableGridToolTips();
	CGXStylesMap *olStylesmap1 = pomBsdGrid->GetParam()->GetStylesMap();
	olStylesmap1->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));
	
	// Tooltip-Texte der Spalten-Header einstellen
	pomBsdGrid->SetStyleRange(CGXRange(0,1), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, LoadStg(IDS_STRING350)));	
	pomBsdGrid->SetStyleRange(CGXRange(0,2), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, LoadStg(IDS_STRING351)));	
	pomBsdGrid->SetStyleRange(CGXRange(0,3), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, LoadStg(IDS_STRING355)));	
	pomBsdGrid->SetStyleRange(CGXRange(0,4), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, LoadStg(IDS_STRING1935)));	
	pomBsdGrid->SetStyleRange(CGXRange(0,5), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, LoadStg(IDS_STRING356)));	
	pomBsdGrid->SetStyleRange(CGXRange(0,6), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, LoadStg(IDS_STRING379)));	
	pomBsdGrid->SetStyleRange(CGXRange(0,7), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, LoadStg(IDS_STRING380)));	
	pomBsdGrid->SetStyleRange(CGXRange(0,8), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, LoadStg(IDS_STRING383)));	
	pomBsdGrid->SetStyleRange(CGXRange(0,9), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, LoadStg(IDS_STRING384)));	
	pomBsdGrid->SetStyleRange(CGXRange(0,10), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, LoadStg(IDS_STRING389)));	

	pomBsdGrid->Initialize();
	// Scrollbar immer anzeigen
	pomBsdGrid->SetScrollBarMode(SB_VERT,gxnEnabled);

	pomBsdGrid->GetParam()->EnableUndo(FALSE);
	/*pomBsdGrid->LockUpdate(TRUE);
	pomBsdGrid->LockUpdate(FALSE);*/
	pomBsdGrid->GetParam()->EnableUndo(TRUE);

	// Verhindert, das die Spaltenbreite ver�ndert wird
	pomBsdGrid->GetParam()->EnableTrackColWidth(FALSE);
	// Es k�nnen nur ganze Zeilen markiert werden.
	//pomBsdGrid->GetParam()->EnableSelection(GX_SELROW);
	pomBsdGrid->GetParam()->EnableSelection(GX_SELNONE);
	
	pomBsdGrid->SetStyleRange( CGXRange().SetTable(), CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				.SetReadOnly(TRUE)
				//.SetEnabled(FALSE)
				.SetControl(GX_IDS_CTRL_STATIC)
				);
	pomBsdGrid->EnableAutoGrow ( true );
	// DoppelClick Acktion festlegen
	pomBsdGrid->SetLbDblClickAction ( WM_COMMAND, IDC_BUTTONFINDBSD);
	// DnD aktivieren
	pomBsdGrid->SetEnableDnD(true);

	
	//---------------------------------------------------------------------------------	
	// Tooltips aktivieren
	pomOdaGrid->EnableGridToolTips();
	CGXStylesMap *olStylesmap2 = pomOdaGrid->GetParam()->GetStylesMap();
	olStylesmap2->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));

	// Tooltip-Texte der Spalten-Header einstellen
	pomOdaGrid->SetStyleRange(CGXRange(0,1), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, LoadStg(IDS_STRING390)));	
	pomOdaGrid->SetStyleRange(CGXRange(0,2), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, LoadStg(IDS_STRING395)));	

	pomOdaGrid->Initialize();
	pomOdaGrid->GetParam()->EnableUndo(FALSE);
	/*pomOdaGrid->LockUpdate(TRUE);
	pomOdaGrid->LockUpdate(FALSE);*/
	pomOdaGrid->GetParam()->EnableUndo(TRUE);

	// Scrollbar immer anzeigen
	pomOdaGrid->SetScrollBarMode(SB_VERT,gxnEnabled);

	// Verhindert, das die Spaltenbreite ver�ndert wird
	pomOdaGrid->GetParam()->EnableTrackColWidth(FALSE);
	// Es k�nnen nur ganze Zeilen markiert werden.
	//pomBsdGrid->GetParam()->EnableSelection(GX_SELROW);
	pomOdaGrid->GetParam()->EnableSelection(GX_SELNONE);
	
	pomOdaGrid->SetStyleRange( CGXRange().SetTable(), CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				.SetReadOnly(TRUE)
				//.SetEnabled(FALSE)
				.SetControl(GX_IDS_CTRL_STATIC)
				);
	pomOdaGrid->EnableAutoGrow ( FALSE );
	// DoppelClick Acktion festlegen
	pomOdaGrid->SetLbDblClickAction ( WM_COMMAND, IDC_BUTTONFINDODA);
	// DnD aktivieren
	pomOdaGrid->SetEnableDnD(true);
		
	//-----------------------------------------------------------------------------
	// Tooltips aktivieren
	pomWisGrid->EnableGridToolTips();
	CGXStylesMap *olStylesmap3 = pomWisGrid->GetParam()->GetStylesMap();
	olStylesmap3->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));

	// Tooltip-Texte der Spalten-Header einstellen
	pomWisGrid->SetStyleRange(CGXRange(0,1), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, LoadStg(IDS_STRING396)));	
	pomWisGrid->SetStyleRange(CGXRange(0,2), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, LoadStg(IDS_STRING395)));	

	pomWisGrid->Initialize();
	pomWisGrid->GetParam()->EnableUndo(FALSE);
	/*pomWisGrid->LockUpdate(TRUE);
	pomWisGrid->LockUpdate(FALSE);*/
	pomWisGrid->GetParam()->EnableUndo(TRUE);

	// Scrollbar immer anzeigen
	pomWisGrid->SetScrollBarMode(SB_VERT,gxnEnabled);

	// Verhindert, das die Spaltenbreite ver�ndert wird
	pomWisGrid->GetParam()->EnableTrackColWidth(FALSE);
	// Es k�nnen nur ganze Zeilen markiert werden.
	//pomBsdGrid->GetParam()->EnableSelection(GX_SELROW);
	pomWisGrid->GetParam()->EnableSelection(GX_SELNONE);
	
	pomWisGrid->SetStyleRange( CGXRange().SetTable(), CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				.SetReadOnly(TRUE)
				//.SetEnabled(FALSE)
				.SetControl(GX_IDS_CTRL_STATIC)
				);
	pomWisGrid->EnableAutoGrow ( FALSE );
	// DoppelClick Acktion festlegen
	pomWisGrid->SetLbDblClickAction ( WM_COMMAND, IDC_BUTTONFINDWIS);
	// DnD aktivieren
	pomWisGrid->SetEnableDnD(true);
	
CCS_CATCH_ALL;
}

//**********************************************************************************
// Soll das Grid mit den W�nschen angezeigt werden (TRUE/FALSE)
//**********************************************************************************

void CCodeBigDlg::SetShowWish(bool bpShowWish)
{
	bmShowWish = bpShowWish;
	// Grid mit den W�nschen l�schen
	if (!bpShowWish)
		pomWisGrid->HideRows(0,pomWisGrid->GetRowCount(), true);
	else
		pomWisGrid->HideRows(0,pomWisGrid->GetRowCount(), false);
}

//**********************************************************************************
// Breite der Columns setzen
//**********************************************************************************

void CCodeBigDlg::IniColWidths()
{
	pomBsdGrid->SetColWidth ( 0, 0, 0);
	pomBsdGrid->SetColWidth ( 1, 1, 52);
	pomBsdGrid->SetColWidth ( 2, 2, 35);
	pomBsdGrid->SetColWidth ( 3, 3, 35);
	pomBsdGrid->SetColWidth ( 4, 4, 35);
	pomBsdGrid->SetColWidth ( 5, 5, 35);
	pomBsdGrid->SetColWidth ( 6, 6, 35);
	pomBsdGrid->SetColWidth ( 7, 7, 35);
	pomBsdGrid->SetColWidth ( 8, 8, 12);
	pomBsdGrid->SetColWidth ( 9, 9, 45);
	pomBsdGrid->SetColWidth ( 10, 10, 45);

	pomOdaGrid->SetColWidth ( 0, 0, 0);
	pomOdaGrid->SetColWidth ( 1, 1, 52);
	pomOdaGrid->SetColWidth ( 2, 2, 115);

	pomWisGrid->SetColWidth ( 0, 0, 0);
	pomWisGrid->SetColWidth ( 1, 1, 52);
	pomWisGrid->SetColWidth ( 2, 2, 115);
}

//**********************************************************************************
// BSD Grid f�llen
//**********************************************************************************

void CCodeBigDlg::FillBsdGrid(bool bpAllCodes)
{
CCS_TRY;
	// Anzeige austellen
	pomBsdGrid->LockUpdate(TRUE);
	
	// Warte Dialog einblenden.
	CWaitDlg	olWaitDlg(this,LoadStg(IDS_STRING1819));


	int ilRowCount;
	BSDDATA* polBsd;

	// alle BSD-Datens�tze durchgehen
	for(int ilCount=0; ilCount<ogBsdData.omData.GetSize(); ilCount++)
	{
		// Datensatz ermitteln
		polBsd = &ogBsdData.omData[ilCount];
		// Werte pr�fen
		if (CheckBsd(polBsd,polBsd->Vafr,polBsd->Vato,bpAllCodes))
		{
			// Neue Row einf�gen
			ilRowCount = pomBsdGrid->GetRowCount();
			pomBsdGrid->InsertRows(ilRowCount+1,1);
			// Urno mit erstem Feld koppeln.						
			pomBsdGrid->SetStyleRange (CGXRange(ilRowCount+1,1), CGXStyle().SetItemDataPtr((void*)polBsd->Urno));
			// 1.) Code
			pomBsdGrid->SetValueRange(CGXRange(ilRowCount+1,1),polBsd->Bsdc);
			// 2.) fr�hester Schichtbeginn
			CString olEsbg(polBsd->Esbg);
			olEsbg = olEsbg.Left(2) + ":" + olEsbg.Right(2);
			pomBsdGrid->SetValueRange(CGXRange(ilRowCount+1,2),olEsbg);
			// 3.) sp�testes Schichtende
			CString olLsen(polBsd->Lsen);
			olLsen = olLsen.Left(2) + ":" + olLsen.Right(2);
			pomBsdGrid->SetValueRange(CGXRange(ilRowCount+1,3),olLsen);
			// 4.) regul�re Dauer
			COleDateTimeSpan olMyTime(0,0, atoi(polBsd->Sdu1),0 );
			pomBsdGrid->SetValueRange(CGXRange(ilRowCount+1,4),olMyTime.Format("%H:%M"));
			// 5.) Pause von
			CString olBkf1(polBsd->Bkf1);
			if (!olBkf1.IsEmpty())
			{
				olBkf1 = olBkf1.Left(2) + ":" + olBkf1.Right(2);
				pomBsdGrid->SetValueRange(CGXRange(ilRowCount+1,5),olBkf1);
			}
			// 6.) Pause bis
			CString olBkt1(polBsd->Bkt1);
			if (!olBkt1.IsEmpty())
			{
				olBkt1 = olBkt1.Left(2) + ":" + olBkt1.Right(2);
				pomBsdGrid->SetValueRange(CGXRange(ilRowCount+1,6),olBkt1);
			}
			// 7.) Pausenl�nge
			pomBsdGrid->SetValueRange(CGXRange(ilRowCount+1,7),polBsd->Bkd1);
			// 8.) Typ
			pomBsdGrid->SetValueRange(CGXRange(ilRowCount+1,8),polBsd->Type);
			// 9.) G�ltig von
			pomBsdGrid->SetValueRange(CGXRange(ilRowCount+1,9),(polBsd->Vafr).Format("%d.%m.%y"));
			// 10.) G�ltig bis
			pomBsdGrid->SetValueRange(CGXRange(ilRowCount+1,10),(polBsd->Vato).Format("%d.%m.%y"));
	
		}
	}

	// Grid neu sortieren
	SortGrid(pomBsdGrid,1);
	// Anzeige wieder anstellen
	pomBsdGrid->LockUpdate(FALSE);
	// Neu zeichnen.
	pomBsdGrid->Redraw();

CCS_CATCH_ALL;
}

//**********************************************************************************
// Grid mit Daten f�llen
//**********************************************************************************

void CCodeBigDlg::FillWishGrid(bool bpAllCodes)
{
CCS_TRY;
	// W�nsche
	if (bmShowWish)
	{
		pomWisGrid->GetParam()->SetLockReadOnly(false);
		int ilRowCount = pomWisGrid->GetRowCount();
		if (ilRowCount > 0)
			pomWisGrid->RemoveRows(1,ilRowCount);

		//-------------------------------------------------------------------------------
		// W�nsche
		CCSPtrArray<WISDATA> olWisData;
		int ilWisSize = ogWisData.GetWisArrayByOrgList(omOrgList, &olWisData, bpAllCodes);

		WISDATA* prlWis;
		for (int ilCount=0; ilCount<ilWisSize; ilCount++ )
		{
			prlWis = (WISDATA*)olWisData.CPtrArray::GetAt(ilCount);
	
			ilRowCount = pomWisGrid->GetRowCount ();
			pomWisGrid->InsertRows ( ilRowCount+1,1);

			// Urno mit erstem Feld koppeln.						
			pomWisGrid->SetStyleRange (CGXRange(ilCount+1,1), CGXStyle().SetItemDataPtr((void*)prlWis->Urno));

			// Datenfelder f�llen
			pomWisGrid->SetValueRange(CGXRange(ilCount+1,1),	prlWis->Wisc);
			pomWisGrid->SetValueRange(CGXRange(ilCount+1,2),	prlWis->Wisd);
		}

		// Grid sortieren
		SortGrid(pomWisGrid,1);
		pomWisGrid->GetParam()->SetLockReadOnly(true);
	}

CCS_CATCH_ALL;
}

//**********************************************************************************
// Sortierung des Grids "per Hand"
//**********************************************************************************

bool CCodeBigDlg::SortGrid(CGridControl* popGrid,int ipRow)
{
	CCS_TRY;
	CGXSortInfoArray  sortInfo;
	sortInfo.SetSize( 1 );
	sortInfo[0].sortOrder = CGXSortInfo::ascending;
	sortInfo[0].nRC = ipRow;                       
	sortInfo[0].sortType = CGXSortInfo::autodetect;  
	popGrid->SortRows( CGXRange().SetRows(1, popGrid->GetRowCount()), sortInfo); 
	
	return true;
	CCS_CATCH_ALL;
	return false;
}

//**********************************************************************************
// Left Button Click in das Grid
//**********************************************************************************

LONG CCodeBigDlg::OnGridDragBegin(WPARAM wParam, LPARAM lParam)
{
	GRIDNOTIFY* rlNotify = (GRIDNOTIFY*) lParam;

	CString olUrno;
	olUrno.Format("%ld",(long)rlNotify->value2);

	if (rlNotify->source == pomBsdGrid)
	  	OnDragBegin(olUrno,rlNotify->value1,"BSD");

	if (rlNotify->source == pomOdaGrid)
	  	OnDragBegin(olUrno,rlNotify->value1,"ODA");

	if (rlNotify->source == pomWisGrid)
	  	OnDragBegin(olUrno,rlNotify->value1,"WIS");


	return 0L;
}

//****************************************************************************************************
// OnDragBegin(): Messagehandler f�r Drag and Drop-Mechanismus
//  R�ckgabe: long -> immer 0L
//****************************************************************************************************

void CCodeBigDlg::OnDragBegin(CString opUrno,CString opCode, CString opSource)
{
CCS_TRY;
	// Daten vorhanden?
	// ACHTUNG: Ziel k�nnen beide Views sein !
	// D'n'D-Objekt initialisieren
	omDragDropObject.CreateDWordData(DIT_DUTYROSTER_BSD_TABLE, 3);
	//omDragDropObject.CreateDWordData(DIT_SHIFTROSTER_BSD_TABLE, 1);
	// Basisschichten-Datenobjekt hinzuf�gen
	omDragDropObject.AddString(opCode);
	// "Absenderkennung" mitgeben
	omDragDropObject.AddString(opSource);
	// Urno mitgeben
	omDragDropObject.AddString(opUrno);
	// Aktion fortsetzen
	omDragDropObject.BeginDrag();
CCS_CATCH_ALL;
}

//****************************************************************************************************
// Suchen in den Grids, hier BSD Grid
//****************************************************************************************************

void CCodeBigDlg::On_B_FindBSD()
{
	if (pomBsdGrid)
		pomBsdGrid->OnShowFindReplaceDialog(TRUE);
}

//****************************************************************************************************
// Suchen in den Grids, hier ODA Grid
//****************************************************************************************************

void CCodeBigDlg::On_B_FindODA()
{
	if (pomOdaGrid)
		pomOdaGrid->OnShowFindReplaceDialog(TRUE);
}

//****************************************************************************************************
// Suchen in den Grids, hier WIS Grid
//****************************************************************************************************

void CCodeBigDlg::On_B_FindWIS()
{
	if (pomWisGrid)
		pomWisGrid->OnShowFindReplaceDialog(TRUE);
}

/****************************************************************************************************
Pr�ft ob die Schicht im vorgegebenen Zeitraum ist und ob sie angzeigt werden soll
bool bpAllCodes - true - alle Codes anzeigen, unabh�ngig vom Pfc/Org
****************************************************************************************************/

bool CCodeBigDlg::CheckBsd(BSDDATA* popBsd ,COleDateTime opShiftFromTime,COleDateTime opShiftToTime, bool bpAllCodes)
{
	bool blTimeCheckFromOk = false;
	bool blTimeCheckToOk = false;

	COleDateTime olNullTime;
	CString olCodeWithSeparator;

	if(!bpAllCodes)
	{
		// Pr�fung ob diese Schicht in der Liste der Anzuzeigenen Schichten enthalten ist.
		if(!omPfcList.IsEmpty())
		{
			if(!strlen(popBsd->Fctc))
			{
				if (omPfcList.Find("NO_PFCCODE") == -1)
				{
					//				TRACE("NO_PFCCODE\n");
					return false;
				}
			}
			else
			{
				olCodeWithSeparator.Format("'%s'",LPCTSTR(popBsd->Fctc));
				if (omPfcList.Find(olCodeWithSeparator) == -1)
				{
					//				TRACE("PFCCODE %s NOT FOUND\n",olCodeWithSeparator);
					return false;
				}
			}
		}

		// Pr�fung ob diese Organisation in der Liste der Anzuzeigenden Organisationen enthalten ist.
		if (!omOrgList.IsEmpty())
		{
			if(!strlen(popBsd->Dpt1))
			{
				if (omOrgList.Find("NO_ORGCODE") == -1)
				{
					//				TRACE("NO_ORGCODE\n");
					return false;
				}
			}
			else
			{
				olCodeWithSeparator.Format("'%s'",LPCTSTR(popBsd->Dpt1));
				if (omOrgList.Find(olCodeWithSeparator) == -1)
				{
					//				TRACE("ORGCODE %s NOT FOUND\n",olCodeWithSeparator);
					return false;
				}
			}
		}
		else
		{
			return false;		// leere OrgList & !bpAllCodes - keine Funktionen anzeigen
		}
	}
	// TODO: Sollte die Liste leer sein nix anzeigen lassen. ATM wird die Liste nicht ber�cksichtig, sollte 
	// sie leer sein.
	/*
	else
		// -> Liste leer
		return false;*/

	/*
	TRACE("------------------------------------------------------------------------\n");
	CString olTime = "\nCheckBsd zur Pr�fung:" + opShiftFromTime.Format("%d.%m.%Y") + "-" + opShiftToTime.Format("%d.%m.%Y");
	TRACE("%s\n",(LPCTSTR)olTime);
	olTime = "\nCheckBsd g�ltiger Bereich:" + omFromTime.Format("%d.%m.%Y") + "-" + omToTime.Format("%d.%m.%Y");
	TRACE("%s\n",(LPCTSTR)olTime);*/

	// Nur Schichten, die im G�ltigkeisbereich liegen zulassen
	//----------------------------------------------------------------------------------
	// Ist ein Vorgabe Wert vorhanden f�r diese Schicht ?
	// Ist der Wert der Schicht Null ? Dann keine Pr�fung.
	if (opShiftToTime.GetStatus() != COleDateTime::valid || omFromTime.GetStatus() != COleDateTime::valid  
		|| opShiftToTime == olNullTime || omFromTime == olNullTime)  
	{
		// -> keine Pr�fung vornehmen.
		blTimeCheckFromOk = true;
	}
	else
	{
		// Pr�fung vornehmen
		// Schichtg�ltigkeit Ende mu� gr��er als Anfang Ladedatum sein
		if (opShiftToTime >= omFromTime)
			blTimeCheckFromOk = true;
		else
			blTimeCheckFromOk = false;
			
	}

	// Nur Schichten, die im G�ltigkeisbereich liegen zulassen
	//----------------------------------------------------------------------------------
	// Endwert pr�fen
	// Ist ein vorgabe Wert vorhanden f�r diese Schicht ?
	// Ist der Wert der Schicht Null ? Dann keine Pr�fung.
	if (opShiftFromTime.GetStatus() != COleDateTime::valid || omToTime.GetStatus() != COleDateTime::valid 
		|| opShiftFromTime == olNullTime || omToTime == olNullTime)
	{
		// -> keine Pr�fung vornehmen.
		blTimeCheckToOk = true;
	}
	else
	{
		// Pr�fung vornehmen
		// Schichtg�ltigkeit mu� kleiner als der Ende Ladedatum sein
		if (opShiftFromTime <= omToTime)
			blTimeCheckToOk = true;
		else
			blTimeCheckToOk = false;
	}

//	TRACE("CheckBsd Result: from %i to %i\n",blTimeCheckFromOk,blTimeCheckToOk);

	//---------------------------------------------------------------------------------
	return (blTimeCheckFromOk && blTimeCheckToOk);
}


//********************************************************************************************************************
// Description: Here we handle the most important thing, drawing in the non client area
//********************************************************************************************************************

void CCodeBigDlg::OnNcPaint()
{
	Default();
}

//********************************************************************************************************************
// testet ob der Button getroffen wurde
// (hardcodiert)
//********************************************************************************************************************

bool CCodeBigDlg::IsButtonHit(CPoint point)
{
	CRect rc; 
	point.y = -point.y;

	if (point.x <=294+33 && point.x >= 282+33 && point.y <= 16 && point.y >= 6)
		return true;
	else
		return false;
}

//********************************************************************************************************************
// Klick in die NonClient Area
//********************************************************************************************************************

void CCodeBigDlg::OnNcLButtonDown(UINT nHitTest, CPoint point)
{
	CRect rc; 
	GetClientRect(rc); 
	ScreenToClient(&point);
	
    if (nHitTest == HTMINBUTTON)
	{
		// sendet Nachricht zum Wechseln der Dialoggr��e
		GetParent()->SendMessage(WM_USER_CHANGECODEDLG, 2, NULL);
	}
	else
	{
		CDialog::OnNcLButtonDown(nHitTest, point);
	}
}


//****************************************************************************************************
// Buttons in der Titelleiste: neuzeichnen wird angestossen.
//****************************************************************************************************

void CCodeBigDlg::OnTimer(UINT nIDEvent) 
{
	SendMessage(WM_NCPAINT, MAKEWPARAM(NULL, NULL), NULL);
	
	CDialog::OnTimer(nIDEvent);
}


//********************************************************************************************************************
// ODA Grid f�llen
//********************************************************************************************************************

void CCodeBigDlg::FillOdaGrid()
{
	pomOdaGrid->GetParam()->SetLockReadOnly(false);
	int ilRowCount = pomOdaGrid->GetRowCount();
	if (ilRowCount > 0)
		pomOdaGrid->RemoveRows(1,ilRowCount);

	// Abwesenheiten
	//uhi 18.04.00
	int ilCount = 0;

	pomOdaGrid->LockUpdate(TRUE);

	for (int i=0; i<ogOdaData.omData.GetSize(); i++ )
	{
		ODADATA *prlOda = &ogOdaData.omData[i];
		if (bmShowWish)
		{
			ilRowCount = pomOdaGrid->GetRowCount ();
			pomOdaGrid->InsertRows( ilRowCount+1,1);
			// Urno mit erstem Feld koppeln.						
			pomOdaGrid->SetStyleRange (CGXRange(ilRowCount+1,1), CGXStyle().SetItemDataPtr((void*)prlOda->Urno));
			// Datenfelder f�llen
			pomOdaGrid->SetValueRange(CGXRange(ilCount+1,1),	prlOda->Sdac);
			// Tooltip-Texte einstellen
			pomOdaGrid->SetValueRange(CGXRange(ilCount+1,2),	prlOda->Sdan);
			ilCount++;
		}
		else
		{
			if (!strcmp(prlOda->Free, "x") || !strcmp(prlOda->Type, "S"))
			{
				int ilRowCount = pomOdaGrid->GetRowCount ();
				pomOdaGrid->InsertRows( ilRowCount+1,1);
				// Urno mit erstem Feld koppeln.						
				pomOdaGrid->SetStyleRange (CGXRange(ilRowCount+1,1), CGXStyle().SetItemDataPtr((void*)prlOda->Urno));
				// Datenfelder f�llen
				pomOdaGrid->SetValueRange(CGXRange(ilCount+1,1),	prlOda->Sdac );
				// Tooltip-Texte einstellen
				pomOdaGrid->SetValueRange(CGXRange(ilCount+1,2),	prlOda->Sdan);
				ilCount++;
			}
		}
	}

	// Grid neu sortieren
	SortGrid(pomOdaGrid,1);
	// Tooltip Array �bergeben
	//pomOdaGrid->SetEnableExtraToolTip(true);
	// Anzeige wieder anstellen
	pomOdaGrid->LockUpdate(false);
	pomOdaGrid->GetParam()->SetLockReadOnly(true);
	// Neu zeichnen.
	pomOdaGrid->Redraw();
}