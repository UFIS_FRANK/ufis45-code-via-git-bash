// Plan_Info.cpp: Implementierungsdatei
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CPlan_Info 

CPlan_Info::CPlan_Info(CWnd* pParent /*=NULL*/)
	: CDialog(CPlan_Info::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPlan_Info)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT

	// Nicht-modaler Dialog mu� per CDialog::Create() erzeugt werden
	CDialog::Create(CPlan_Info::IDD, pParent);
}

void CPlan_Info::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPlan_Info)
		// HINWEIS: Der Klassen-Assistent f�gt hier DDX- und DDV-Aufrufe ein
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CPlan_Info, CDialog)
	//{{AFX_MSG_MAP(CPlan_Info)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CPlan_Info 

BOOL CPlan_Info::OnInitDialog() 
{
	CDialog::OnInitDialog();
	// Titel setzen:
	SetWindowText(LoadStg(IDS_STRING1596));
	// Benutzerhinweis laden
	GetDlgItem(IDC_STATIC_USAGE)->SetWindowText(LoadStg(IDS_INFO_USAGE));
	// ToolTips aktivieren
	SetToolTip();

	return TRUE;  
}

//****************************************************************************************************
// Blendet das Fenster aus
//****************************************************************************************************

void CPlan_Info::OnCancel() 
{
	// Fenster ausblenden
	ShowWindow(SW_HIDE);
}

//****************************************************************************************************
// Aktiviert die ToolTips
//****************************************************************************************************

void CPlan_Info::SetToolTip()
{
CCS_TRY;
	m_ToolTip.Create(this);
	// Schichtplan
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_STRING405), (GetDlgItemRect(IDC_STATIC_SHIFTPLAN)),IDC_STATIC_SHIFTPLAN);
	// Urlaub
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(SHIFT_S_HOLIDAY), (GetDlgItemRect(IDC_STATIC_VACANCIES)),IDC_STATIC_VACANCIES);
	// W�nsche
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_STRING1363), (GetDlgItemRect(IDC_STATIC_WISHES)),IDC_STATIC_WISHES);
	// Langzeitdienstplan
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDC_PLANLU), (GetDlgItemRect(IDC_STATIC_LONGTIMEPLAN)),IDC_STATIC_LONGTIMEPLAN);
	// Dienstplan
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_STRING412), (GetDlgItemRect(IDC_STATIC_DUTYPLAN)),IDC_STATIC_DUTYPLAN);
	// Mitarbeiter Tagesliste
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_STRING421), (GetDlgItemRect(IDC_STATIC_DAILYLIST)),IDC_STATIC_DAILYLIST);
	// Tagesendbearbeitung
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_STRING422), (GetDlgItemRect(IDC_STATIC_DAYEND)),IDC_STATIC_DAYEND);
	// Nachbearbeitung
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_STRING423), (GetDlgItemRect(IDC_STATIC_WORKOVER)),IDC_STATIC_WORKOVER);
	// Name
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_STRING60), (GetDlgItemRect(IDC_STATIC_NAME)),IDC_STATIC_NAME);
	// Datum ausgew�hlter Tag
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_INFO_GREENDATE), (GetDlgItemRect(IDC_STATIC_GREENDATE)),IDC_STATIC_GREENDATE);
	// Datum Wochentag
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_INFO_GREYDATE), (GetDlgItemRect(IDC_STATIC_GREYDATE1)),IDC_STATIC_GREYDATE1);
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_INFO_GREYDATE), (GetDlgItemRect(IDC_STATIC_GREYDATE2)),IDC_STATIC_GREYDATE2);
	// Datum Wochenende
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_INFO_REDDATE), (GetDlgItemRect(IDC_STATIC_REDDATE)),IDC_STATIC_REDDATE);
	// Sortierung
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_INFO_SORT), (GetDlgItemRect(IDC_STATIC_SORT)),IDC_STATIC_SORT);
	// Schichtplan-Codes
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_INFO_NONEDITABLEFIELDS), (GetDlgItemRect(IDC_STATIC_NONEDITABLEFIELDS)),IDC_STATIC_NONEDITABLEFIELDS);
	// Urlaubs- und Wunschfelder
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_INFO_EDITABLEFIELDS), (GetDlgItemRect(IDC_STATIC_EDITABLEFIELDS)),IDC_STATIC_EDITABLEFIELDS);
	// kein Vertrag
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_INFO_NOCONTRACT), (GetDlgItemRect(IDC_STATIC_NOCONTRACT)),IDC_STATIC_NOCONTRACT);
	// mehrere Schichten
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_INFO_MULTIPLESHIFT), (GetDlgItemRect(IDC_STATIC_MULTIPLESHIFT)),IDC_STATIC_MULTIPLESHIFT);
	// freigegebene Felder
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_INFO_RELEASEDFIELDS), (GetDlgItemRect(IDC_STATIC_RELEASEDFIELDS1)),IDC_STATIC_RELEASEDFIELDS1);
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_INFO_RELEASEDFIELDS), (GetDlgItemRect(IDC_STATIC_RELEASEDFIELDS2)),IDC_STATIC_RELEASEDFIELDS2);
	// aktive Felder
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_INFO_ACTIVEFIELD), (GetDlgItemRect(IDC_STATIC_ACTIVEFIELD1)),IDC_STATIC_ACTIVEFIELD1);
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_INFO_ACTIVEFIELD), (GetDlgItemRect(IDC_STATIC_ACTIVEFIELD2)),IDC_STATIC_ACTIVEFIELD2);
	// gr�ne Ecke
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_INFO_GREENTRIANGLE), (GetDlgItemRect(IDC_STATIC_GREENTRIANGLE)),IDC_STATIC_GREENTRIANGLE);
	// Abweichung von Basisischicht (roter Schichtcode)
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_INFO_DIFFERFROMBSD), (GetDlgItemRect(IDC_STATIC_DIFFERFROMBSD)),IDC_STATIC_DIFFERFROMBSD);
	// rote Ecke
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_INFO_REDTRIANGLE), (GetDlgItemRect(IDC_STATIC_REDTRIANGLE)),IDC_STATIC_REDTRIANGLE);
	// schwarze Ecke
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_INFO_BLACKTRIANGLE), (GetDlgItemRect(IDC_STATIC_BLACKTRIANGLE)),IDC_STATIC_BLACKTRIANGLE);
	// graue Ecke
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_INFO_GREYTRIANGLE), (GetDlgItemRect(IDC_STATIC_GREYTRIANGLE)),IDC_STATIC_GREYTRIANGLE);

	// noch nicht freigegebene Felder
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_INFO_NEXTLEVEL), (GetDlgItemRect(IDC_STATIC_NEXTLEVEL1)),IDC_STATIC_NEXTLEVEL1);
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_INFO_NEXTLEVEL), (GetDlgItemRect(IDC_STATIC_NEXTLEVEL2)),IDC_STATIC_NEXTLEVEL2);
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_INFO_NEXTLEVEL), (GetDlgItemRect(IDC_STATIC_NEXTLEVEL3)),IDC_STATIC_NEXTLEVEL3);
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_INFO_NEXTLEVEL), (GetDlgItemRect(IDC_STATIC_NEXTLEVEL4)),IDC_STATIC_NEXTLEVEL4);
	// Statistik-Header
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_INFO_STATHEADER), (GetDlgItemRect(IDC_STATIC_STATHEADER)),IDC_STATIC_STATHEADER);
	// Schichtcode in Statistik
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_INFO_STATSHIFT), (GetDlgItemRect(IDC_STATIC_STATSHIFT)),IDC_STATIC_STATSHIFT);
	// Summen-Zeile in Statistik (1.Zeile)
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_INFO_STATSUM), (GetDlgItemRect(IDC_STATIC_STATSUM)),IDC_STATIC_STATSUM);
	// Unterdeckung in Statistik
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_INFO_UNDERDEMAND), (GetDlgItemRect(IDC_STATIC_UNDERDEMAND)),IDC_STATIC_UNDERDEMAND);
	// ausgeglichene Statistik
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_INFO_DEMANDOK), (GetDlgItemRect(IDC_STATIC_DEMANDOK1)),IDC_STATIC_DEMANDOK1);
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_INFO_DEMANDOK), (GetDlgItemRect(IDC_STATIC_DEMANDOK2)),IDC_STATIC_DEMANDOK2);
	// kein Bedarf
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_INFO_NODEMAND), (GetDlgItemRect(IDC_STATIC_NODEMAND)),IDC_STATIC_NODEMAND);
	// �berdeckung in Statistik
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_INFO_OVERDEMAND), (GetDlgItemRect(IDC_STATIC_OVERDEMAND)),IDC_STATIC_OVERDEMAND);
	// Kontobezeichnung
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_INFO_ACCOUNTHEADER), (GetDlgItemRect(IDC_STATIC_ACCOUNTHEADER)),IDC_STATIC_ACCOUNTHEADER);
	// Kontostand
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_INFO_ACCOUNT), (GetDlgItemRect(IDC_STATIC_ACCOUNT)),IDC_STATIC_ACCOUNT);
	// nicht benutzte Felder in Kontotabelle
	m_ToolTip.AddTool(this, (LPCTSTR)LoadStg(IDS_INFO_UNUSEDFIELDS), (GetDlgItemRect(IDC_STATIC_UNUSEDFIELDS)),IDC_STATIC_UNUSEDFIELDS);

	// Ansprechzeit
	m_ToolTip.SetDelayTime(TTDT_INITIAL,50);
	// Anzeigedauer
	m_ToolTip.SetDelayTime(TTDT_AUTOPOP,5000);
	m_ToolTip.Activate(TRUE);
CCS_CATCH_ALL;
}

//****************************************************************************************************
// Ermittelt die Gr��e eines Controls
//****************************************************************************************************

CRect CPlan_Info::GetDlgItemRect(int ipID)
{
	CWnd *pWnd;
	CRect olRect;

	pWnd = GetDlgItem(ipID);
	VERIFY(pWnd != 0);
	pWnd->GetWindowRect(&olRect);
	ScreenToClient(&olRect);

	return olRect;
}

//****************************************************************************************************
// 
//****************************************************************************************************

LRESULT CPlan_Info::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	// We need to pass these messages to the tooltip for it to determine
	// the position of the mouse

	switch (message)
	{
	case WM_LBUTTONDOWN:
	case WM_RBUTTONDOWN:
	case WM_MBUTTONDOWN:
	case WM_LBUTTONUP:
	case WM_MBUTTONUP:
	case WM_RBUTTONUP:
	case WM_MOUSEMOVE:
		{
			MSG msg;
			msg.hwnd = m_hWnd;
			msg.message = message;
			msg.wParam = wParam;
			msg.lParam = lParam;

			m_ToolTip.RelayEvent(&msg);
		}
	}

	return CDialog::WindowProc(message,wParam,lParam);
}

