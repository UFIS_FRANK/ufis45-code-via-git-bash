// CedaSreData.cpp
 
#include <stdafx.h>

CedaSreData ogSreData;

void ProcessSreCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

static int CompareTimes(const SREDATA **e1, const SREDATA **e2);
static int CompareSurnAndTimes(const SREDATA **e1, const SREDATA **e2);

//--CEDADATA-----------------------------------------------------------------------------------------------
CedaSreData::CedaSreData() : CedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for SREDataStruct
	BEGIN_CEDARECINFO(SREDATA,SREDataRecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_LONG		(Surn,"SURN")
		FIELD_INT		(Regi,"REGI")
		FIELD_OLEDATE	(Vpfr,"VPFR")
		FIELD_OLEDATE	(Vpto,"VPTO")
	END_CEDARECINFO //(SREDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(SREDataRecInfo)/sizeof(SREDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&SREDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	}

	// initialize field names
	strcpy(pcmTableName,"SRE");
	strcat(pcmTableName,pcgTableExt);
	strcpy(pcmListOfFields,"URNO,SURN,REGI,VPFR,VPTO");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omDataSre.RemoveAll();
}


//--REGISTER----------------------------------------------------------------------------------------------
void CedaSreData::Register(void)
{
	DdxRegister((void *)this,BC_SRE_CHANGE,	"SREDATA", "Sre-changed",	ProcessSreCf);
	DdxRegister((void *)this,BC_SRE_NEW,	"SREDATA", "Sre-new",		ProcessSreCf);
	DdxRegister((void *)this,BC_SRE_DELETE,	"SREDATA", "Sre-deleted",	ProcessSreCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------
CedaSreData::~CedaSreData(void)
{
	TRACE("CedaSreData::~CedaSreData called\n");
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------
void CedaSreData::ClearAll(bool bpWithRegistration)
{
	TRACE("CedaSreData::ClearAll called\n");
    omUrnoMap.RemoveAll();
    omDataSre.DeleteAll();

	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------
bool CedaSreData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omDataSre.DeleteAll();
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(pspWhere == NULL)
	{	
		ilRc = CedaAction2("RT");
	}
	else
	{
		ilRc = CedaAction2("RT", pspWhere);
	}

	if (ilRc != true)
	{
		TRACE("Read-Sre: Ceda-Error %d \n",ilRc);
		return ilRc;
	}

	// Load data from CedaData into the dynamic array of record
	for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
	{
		SREDATA *prlSre = new SREDATA;
		if ((ilRc = GetFirstBufferRecord2(prlSre)) == true)
		{
			if(IsValidSre(prlSre))
			{
				omDataSre.Add(prlSre);//Update omDataSre
				omUrnoMap.SetAt((void *)prlSre->Urno,prlSre);
#ifdef TRACE_FULL_DATA
				// Datensatz OK, loggen if FULL
				ogRosteringLogText.Format(" read %8.8lu %s OK:  ", ilCountRecord, pcmTableName);
				GetDataFormatted(ogRosteringLogText, prlSre);
				WriteLogFull("");
#endif TRACE_FULL_DATA
			}
			else
			{
				delete prlSre;
			}
		}
		else
		{
			delete prlSre;
		}
	}
	TRACE("Read-Sre: %d gelesen\n",ilCountRecord-1);

	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  read %d records from table %s",ilCountRecord-1, pcmTableName);
		WriteInRosteringLog();
	}

	// wir sortieren alles nach Surn & Time - das beschleunigt das Lesen zigfach
	omDataSre.Sort(CompareSurnAndTimes);

	ClearFastSocketBuffer();	
	return true;
}

//--INSERT-------------------------------------------------------------------------------------------------
bool CedaSreData::Insert(SREDATA *prpSre)
{
	prpSre->IsChanged = DATA_NEW;
	if(Save(prpSre) == false) return false; //Update Database
	InsertInternal(prpSre);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------
bool CedaSreData::InsertInternal(SREDATA *prpSre)
{
	ogDdx.DataChanged((void *)this, SRE_NEW,(void *)prpSre ); //Update Viewer
	omDataSre.Add(prpSre);//Update omDataSre
	omUrnoMap.SetAt((void *)prpSre->Urno,prpSre);
	
	// wir sortieren alles nach Surn/Org & Time - das beschleunigt das Lesen zigfach
	omDataSre.Sort(CompareSurnAndTimes);

    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------
bool CedaSreData::Delete(long lpUrno)
{
	SREDATA *prlSre = GetSreByUrno(lpUrno);
	if (prlSre != NULL)
	{
		prlSre->IsChanged = DATA_DELETED;
		if(Save(prlSre) == false) return false; //Update Database
		DeleteInternal(prlSre);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------
bool CedaSreData::DeleteInternal(SREDATA *prpSre)
{
	ogDdx.DataChanged((void *)this,SRE_DELETE,(void *)prpSre); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpSre->Urno);

	int ilCountRecord = FindFirstOfSurn(prpSre->Surn);
	int ilSreCount = omDataSre.GetSize();
	if(ilCountRecord >= 0)
	{
		for (; ilCountRecord < ilSreCount; ilCountRecord++)
		{
			if (omDataSre[ilCountRecord].Urno == prpSre->Urno)
			{
				omDataSre.DeleteAt(ilCountRecord);//Update omDataSre
				break;
			}
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------
bool CedaSreData::Update(SREDATA *prpSre)
{
	if (GetSreByUrno(prpSre->Urno) != NULL)
	{
		if (prpSre->IsChanged == DATA_UNCHANGED)
		{
			prpSre->IsChanged = DATA_CHANGED;
		}
		if(Save(prpSre) == false) return false; //Update Database
		UpdateInternal(prpSre);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------
bool CedaSreData::UpdateInternal(SREDATA *prpSre)
{
	SREDATA *prlSre = GetSreByUrno(prpSre->Urno);
	if (prlSre != NULL)
	{
		*prlSre = *prpSre; //Update omDataSre
		ogDdx.DataChanged((void *)this,SRE_CHANGE,(void *)prlSre); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------
SREDATA *CedaSreData::GetSreByUrno(long lpUrno)
{
	SREDATA  *prlSre;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlSre) == TRUE)
	{
		return prlSre;
	}
	return NULL;
}

int CedaSreData::GetRegiBySurnWithTime(long lpSurn, COleDateTime opDate)
{
	int ilRet = -1;

	if (opDate.GetStatus() != COleDateTime::valid)	//invalid date
	{
		return ilRet;
	}

	int ilCount = FindFirstOfSurn(lpSurn);
	if (ilCount == -1)								// Surn not found
	{
		return ilRet;
	}

	SREDATA  *prlSre = 0;
	for(; ilCount < omDataSre.GetSize(); ilCount++)
	{
		prlSre = (SREDATA*)omDataSre.CPtrArray::GetAt(ilCount);

		if (prlSre->Surn > lpSurn)
		{
			break;		// omDataSre is sorted in Surn-Vpfr
		}

		if (prlSre->Vpfr.GetStatus() != COleDateTime::valid)
		{
			continue;	// Defect record
		}

		if (prlSre->Vpfr <= opDate)
		{
			switch(prlSre->Vpto.GetStatus())
			{
				case COleDateTime::valid:
					if(prlSre->Vpto < opDate)
						continue;	// 3. Ende der G�ltigkeitszeit liegt vor dem opStart

				case COleDateTime::null:
					break;			// no VPTO => no limitation

				default:			// case COleDateTime::invalid:
					continue;

			}
			ilRet = prlSre->Regi;
			// let's have a look if there are more records fitting
		}
		else
		{
			break;	// VPFR > opDate, so there can't be another record because of the sorting
		}
	}
	return ilRet;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------
bool CedaSreData::ReadSpecialData(CCSPtrArray<SREDATA> *popSre,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}

	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction2("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popSre != NULL)
	{
		for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
		{
			SREDATA *prpSre = new SREDATA;
			if ((ilRc = GetBufferRecord2(ilCountRecord,prpSre,CString(pclFieldList))) == true)
			{
				// BDA: Vpfr & Vpto k�nnen auch mal null sein, es ist falsch!
				// ARE: Vpto kann auch mal null sein, es ist OK! aber auf keinen Fall ung�ltig
				if(prpSre->Vpfr.GetStatus() != COleDateTime::valid || prpSre->Vpto.GetStatus() == COleDateTime::invalid)
				{
					TRACE("Read-Sre: %d gelesen, FEHLER IM DATENSATZ, Surn=%ld\n",ilCountRecord-1,prpSre->Surn);
					delete prpSre;
					continue;
				}
				popSre->Add(prpSre);
			}
			else
			{
				delete prpSre;
			}
		}
		if(popSre->GetSize() == 0) return false;
	}
	ClearFastSocketBuffer();	
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------
bool CedaSreData::Save(SREDATA *prpSre)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpSre->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpSre->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpSre);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpSre->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSre->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpSre);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpSre->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSre->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
	TRACE("Sre-IRT/URT/DRT: Ceda-Return %d \n",ilRc);
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------
void  ProcessSreCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogSreData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------
void  CedaSreData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlSreData;
	prlSreData = (struct BcStruct *) vpDataPointer;
	SREDATA *prlSre;

	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("ProcessBC:\n  Cmd <%s>\n  Tbl <%s>\n  Twe <%s>\n  Sel <%s>\n  Fld <%s>\n  Dat <%s>\n  Bcn <%s>",prlSreData->Cmd, prlSreData->Object, prlSreData->Twe, prlSreData->Selection, prlSreData->Fields, prlSreData->Data,prlSreData->BcNum);
		WriteInRosteringLog();
	}

	switch(ipDDXType)
	{
	default:
		break;
	case BC_SRE_CHANGE:
		prlSre = GetSreByUrno(GetUrnoFromSelection(prlSreData->Selection));
		if(prlSre != NULL)
		{
			GetRecordFromItemList(prlSre,prlSreData->Fields,prlSreData->Data);
			UpdateInternal(prlSre);
			break;
		}
	case BC_SRE_NEW:
		prlSre = new SREDATA;
		GetRecordFromItemList(prlSre,prlSreData->Fields,prlSreData->Data);
		InsertInternal(prlSre);
		break;
	case BC_SRE_DELETE:
		{
			long llUrno;
			CString olSelection = (CString)prlSreData->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlSreData->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			prlSre = GetSreByUrno(llUrno);
			if (prlSre != NULL)
			{
				DeleteInternal(prlSre);
			}
		}
		break;
	}
}

//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------
static int CompareTimes(const SREDATA **e1, const SREDATA **e2)
{
	if((**e1).Vpfr>(**e2).Vpfr)
		return 1;
	else
		return -1;
}

//---------------------------------------------------------------------------------------------------------
static int CompareSurnAndTimes(const SREDATA **e1, const SREDATA **e2)
{
	if((**e1).Surn>(**e2).Surn) 
		return 1;
	else if((**e1).Surn<(**e2).Surn) 
		return -1;

	if((**e1).Vpfr>(**e2).Vpfr) 
		return 1;
	else if((**e1).Vpfr<(**e2).Vpfr) 
		return -1;

	return 0;
}

/*****************************************************************************
Sehr schnelles Suchen vom ersten Datensatz mit der angegebenen Surn
! wir nutzen die Tatsache, das omDataSre nach Surn & Time sortiert ist !
*****************************************************************************/
int CedaSreData::FindFirstOfSurn(long lpSurn)
{
	int ilSize = omDataSre.GetSize();
	if(!ilSize) 
		return -1;

	int ilSearch;

	if(omDataSre[ilSize-1].Surn != omDataSre[0].Surn)
	{
		if(ilSize > 16)
		{
			// rekursives Suchen
			ilSearch = ilSize >> 1;
			int ilUp, ilDown;

			for(ilUp = ilSize-1, ilDown = 0;;)
			{
				if(lpSurn == omDataSre[ilSearch].Surn)
				{
					if(!ilSearch || lpSurn != omDataSre[ilSearch-1].Surn)
					{
						// gefunden!
						return ilSearch;
					}

					// lineares Suchen nach unten
					for(ilSearch--; ilSearch>=0;ilSearch--)
					{
						if(!ilSearch || lpSurn != omDataSre[ilSearch-1].Surn)
						{
							// gefunden!
							return ilSearch;
						}
					}
				}

				if(ilUp-ilDown <= 1)
				{
					if(ilUp == ilDown)
					{
						// kein Surn in der Liste vorhanden!
						return -1;
					}

					if(lpSurn == omDataSre[ilUp].Surn)
						return ilUp;
					else if(lpSurn == omDataSre[ilDown].Surn)
						return ilDown;
					else 
						return -1;
				}

				if(lpSurn < omDataSre[ilSearch].Surn)
				{
					ilUp = ilSearch;
					ilSearch = (ilUp + ilDown) >> 1;	// == (ilUp + ilDown) / 2
				}
				else //  if(lpSurn > omDataSre[ilSearch].Surn)
				{
					ilDown = ilSearch;
					ilSearch = (ilUp + ilDown) >> 1;	// == (ilUp + ilDown) / 2
				}
			}
		}
		else
		{
			// lineares Suchen
			for(int ilSearch=0; ilSearch<ilSize; ilSearch++)
			{
				if(omDataSre[ilSearch].Surn == lpSurn)
					return ilSearch;
			}
		}
	}
	else
	{
		return 0;	// alle gleich
	}

	return -1;	// kein gefunden!
}

/**********************************************************************
**********************************************************************/
bool CedaSreData::IsValidSre(SREDATA *prpSre)
{
	if(!ogStfData.GetStfByUrno(prpSre->Surn))
	{
		CString olErr;
		olErr.Format("Organisation Connection Table (SRETAB) URNO '%d' is defect, Employee with URNO '%d' in STFTAB not found\n",
			prpSre->Urno, prpSre->Surn);
		ogErrlog += olErr;

		if(IsTraceLoggingEnabled())
		{
			GetDefectDataString(ogRosteringLogText, (void*)prpSre, "SRETAB.SURN in STFTAB not found");
			WriteInRosteringLog(LOGFILE_TRACE);
		}
		return false;
	}

	if(prpSre->Vpfr.GetStatus() != COleDateTime::valid || prpSre->Vpto.GetStatus() == COleDateTime::invalid)
	{
		STFDATA* prlStf = ogStfData.GetStfByUrno(prpSre->Surn);

		CString olErr;
		olErr.Format("Regulary-Indicator time for %s,%s is not valid\n", prlStf->Lanm, prlStf->Finm);
		ogErrlog += olErr;

		if(IsTraceLoggingEnabled())
		{
			GetDefectDataString(ogRosteringLogText, (void*)prpSre, "SRETAB.VPFR or SRETAB.VPTO is not valid");
			WriteInRosteringLog(LOGFILE_TRACE);
		}
		return false;
	}

	return true;
}

/*************************************************************************************
pr�ft Regi bei STF und wenn leer, dann pruft Regi aus SRETAB
ja - return true
*************************************************************************************/
bool CedaSreData::IsRegularEmployee(long lpSurn, COleDateTime opDate)
{
	STFDATA* polStf = ogStfData.GetStfByUrno (lpSurn);

	if(!polStf)
	{
		return false;
	}

	if(!strcmp(polStf->Regi,"I"))
	{
		return false;					// Irregular
	}
	else if(!strcmp(polStf->Regi,"R"))
	{
		return true;					// Regular
	}
	else if(!strlen(polStf->Regi))		// there's no entry, so let's look in the SRETAB
	{
		int ilRegi = GetRegiBySurnWithTime(lpSurn, opDate);
		if (ilRegi > 0)
		{
			return true;				// Regular
		}
		else
		{
			return false;				// Irregular
		}
	}
	else								// wrong entry in STFTAB.REGI
	{
		CString olSDate = opDate.Format("%d.%m.%Y");
		ogRosteringLogText.Format("wrong Regular entry %s for employee %s,%s on %s", polStf->Regi, polStf->Lanm, polStf->Finm, olSDate);
		WriteInRosteringLog(LOGFILE_TRACE);
	}

	return false;
}