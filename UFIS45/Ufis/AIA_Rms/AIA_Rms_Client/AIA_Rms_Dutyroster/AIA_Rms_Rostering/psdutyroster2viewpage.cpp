// PSDutyRoster2ViewPage.cpp: Implementierungsdatei
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Eigenschaftenseite PSDutyRoster2ViewPage 

IMPLEMENT_DYNCREATE(PSDutyRoster2ViewPage, RosterViewPage)

PSDutyRoster2ViewPage::PSDutyRoster2ViewPage() : RosterViewPage(PSDutyRoster2ViewPage::IDD)
{
	//{{AFX_DATA_INIT(PSDutyRoster2ViewPage)
	//}}AFX_DATA_INIT
	omTitleStrg = LoadStg(IDS_W_Filter);
	m_psp.pszTitle = omTitleStrg;
	m_psp.dwFlags |= PSP_USETITLE;
}

PSDutyRoster2ViewPage::~PSDutyRoster2ViewPage()
{
}

void PSDutyRoster2ViewPage::DoDataExchange(CDataExchange* pDX)
{
	RosterViewPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PSDutyRoster2ViewPage)
	DDX_Control(pDX, IDC_S_W, m_SW);
	DDX_Control(pDX, IDC_S_U, m_SU);
	DDX_Control(pDX, IDC_S_L, m_SL);
	DDX_Control(pDX, IDC_S_5, m_S5);
	DDX_Control(pDX, IDC_S_4, m_S4);
	DDX_Control(pDX, IDC_S_3, m_S3);
	DDX_Control(pDX, IDC_S_2, m_S2);
	DDX_Control(pDX, IDC_S_1, m_S1);
	DDX_Control(pDX, IDC_MA_ALL,		m_MAAll);
	DDX_Control(pDX, IDC_MA_PRESENT,	m_MAPresent);
	DDX_Control(pDX, IDC_MA_NOTPRESENT,	m_MANotPresent);
	DDX_Control(pDX, IDC_PLANW,		m_PlanW);
	DDX_Control(pDX, IDC_PLANU,		m_PlanU);
	DDX_Control(pDX, IDC_PLANL,		m_PlanL);
	DDX_Control(pDX, IDC_PLAN5b,	m_Plan5);
	DDX_Control(pDX, IDC_PLAN4,		m_Plan4);
	DDX_Control(pDX, IDC_PLAN3,		m_Plan3);
	DDX_Control(pDX, IDC_PLAN2,		m_Plan2);
	DDX_Control(pDX, IDC_PLAN1,		m_Plan1);
	//}}AFX_DATA_MAP

	if (pDX->m_bSaveAndValidate == FALSE)
	{
		SetData();
	}
	if (pDX->m_bSaveAndValidate == TRUE)
	{
		GetData();
	}
}


BEGIN_MESSAGE_MAP(PSDutyRoster2ViewPage, RosterViewPage)
	//{{AFX_MSG_MAP(PSDutyRoster2ViewPage)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten PSDutyRoster2ViewPage 

void PSDutyRoster2ViewPage::SetCalledFrom(CString opCalledFrom)
{
	omCalledFrom = opCalledFrom;
}

//---------------------------------------------------------------------------

void PSDutyRoster2ViewPage::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	CRect olRect;
	CPen olPen;
	olPen.CreatePen(PS_SOLID, 1, COLORREF(BLACK)); 
	dc.SelectObject(&olPen);
	CBrush olBrush;
	CString csCode;
	olBrush.CreateSolidBrush(COLORREF(BLACK));
	for(int i=0; i<8; i++ )
	{
		switch (i)
		{
			case 0:
			{
				csCode = ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN1");
				if ((csCode=='1' || csCode=='0') && bmShow1) 
				{
					m_S1.GetWindowRect(&olRect);
					olBrush.DeleteObject();
					olBrush.CreateSolidBrush(COLORREF(GRAY));
				}
				break;
			}
			case 1:
			{
				csCode = ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN2");
				if ((csCode=='1' || csCode=='0') && bmShow2)
				{
					m_S2.GetWindowRect(&olRect);
					olBrush.DeleteObject();
					olBrush.CreateSolidBrush(COLORREF(BLACK));
				}
				break;
			}
			case 2:
			{
				csCode = ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN3");
				if ((csCode=='1' || csCode=='0') && bmShow3)
				{
					m_S3.GetWindowRect(&olRect);
					olBrush.DeleteObject();
					olBrush.CreateSolidBrush(COLORREF(BLUE));
				}
				break;
			}
			case 3:
			{
				csCode = ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN4");
				if ((csCode=='1' || csCode=='0') && bmShow4)
				{
					m_S4.GetWindowRect(&olRect);
					olBrush.DeleteObject();
					olBrush.CreateSolidBrush(COLORREF(PURPLE));
				}
				break;
			}
			case 4:
			{
				csCode = ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN5");
				if ((csCode=='1' || csCode=='0') && bmShow5)
				{
					m_S5.GetWindowRect(&olRect);
					olBrush.DeleteObject();
					olBrush.CreateSolidBrush(COLORREF(ORANGE));
					
				}
				break;
			}
			case 5:
			{
				csCode = ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLANL");
				if ((csCode=='1' || csCode=='0') && bmShowL)
				{
					m_SL.GetWindowRect(&olRect);
					olBrush.DeleteObject();
					olBrush.CreateSolidBrush(COLORREF(FUCHSIA));
				}
				break;
			}
			case 6:
			{
				csCode = ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLANU");
				if ((csCode=='1' || csCode=='0') && bmShowU)
				{
					m_SU.GetWindowRect(&olRect);
					olBrush.DeleteObject();
					olBrush.CreateSolidBrush(COLORREF(RED));
				}
				break;
			}
			case 7:
			{
				csCode = ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLANW");
				if ((csCode=='1' || csCode=='0') && bmShowW)
				{
					m_SW.GetWindowRect(&olRect);
					olBrush.DeleteObject();
					olBrush.CreateSolidBrush(COLORREF(GREEN));
				}
				break;
			}
		}
		ScreenToClient(&olRect);
		dc.SelectObject(&olBrush);
		dc.Rectangle(olRect); //Ellipse,Rectangle
	}
}
//---------------------------------------------------------------------------

BOOL PSDutyRoster2ViewPage::OnInitDialog() 
{
	//Nur auf der Ersten Seite wird OnInitDialog vor SetData aufgerufen !!!!!!!!!
	RosterViewPage::OnInitDialog();

	m_S1.ShowWindow(SW_HIDE);
	m_S2.ShowWindow(SW_HIDE);
	m_S3.ShowWindow(SW_HIDE);
	m_S4.ShowWindow(SW_HIDE);
	m_S5.ShowWindow(SW_HIDE);
	m_SL.ShowWindow(SW_HIDE);
	m_SU.ShowWindow(SW_HIDE);
	m_SW.ShowWindow(SW_HIDE);

	SetWndStatAll(ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLANW"), m_PlanW);
	SetWndStatAll(ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLANU"), m_PlanU);
	SetWndStatAll(ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLANL"), m_PlanL);
	SetWndStatAll(ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN5"), m_Plan5);
	SetWndStatAll(ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN4"), m_Plan4);
	SetWndStatAll(ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN3"), m_Plan3);
	SetWndStatAll(ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN2"), m_Plan2);
	SetWndStatAll(ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN1"), m_Plan1);

	SetStatic();
	SetButtonText();

	// Zeigt nur die Planungstufen an, die in der Parametern festgelegt wurden
	ShowSteps();

	return TRUE;
}

//*******************************************************************************
// Zeigt nur die Planungstufen an, die in der Parametern festgelegt wurden
//*******************************************************************************

void PSDutyRoster2ViewPage::ShowSteps()
{
	bmShow1=bmShow2=bmShow3=bmShow4=bmShow5=bmShowU=bmShowL=bmShowW=false;

	//
	// Werte aus der Parameterklasse einlesen
	//
	CString olStepList= ogCCSParam.GetParamValue(ogAppl,"ID_SHOW_STEPS");
	CStringArray olStrArray;
	int ilArray = ExtractItemList(olStepList,&olStrArray, '|');

	for (int i=0;i<ilArray;i++)
	{
		CString olTmp = olStrArray.GetAt(i);

		if (olTmp == "1")
			bmShow1 = true;
		else if (olTmp == "2")
			bmShow2 = true;
		else if (olTmp == "3")
			bmShow3 = true;
		else if (olTmp == "4")
			bmShow4 = true;
		else if (olTmp == "5")
			bmShow5 = true;
		else if (olTmp == "U")
			bmShowU = true;
		else if (olTmp == "L")
			bmShowL = true;
		else if (olTmp == "W")
			bmShowW = true;
	}
	olStrArray.RemoveAll();
	
	// Oberfl�chen Elemente entsprechend der Auswertung setzen
	if (!bmShow1) m_Plan1.ShowWindow(SW_HIDE);
	if (!bmShow2) m_Plan2.ShowWindow(SW_HIDE);
	if (!bmShow3) m_Plan3.ShowWindow(SW_HIDE);
	if (!bmShow4) m_Plan4.ShowWindow(SW_HIDE);
	if (!bmShow5) m_Plan5.ShowWindow(SW_HIDE);
	if (!bmShowL) m_PlanL.ShowWindow(SW_HIDE);
	if (!bmShowU) m_PlanU.ShowWindow(SW_HIDE);
	if (!bmShowW) m_PlanW.ShowWindow(SW_HIDE);

	// Alle durch die Parameter nicht sichtbaren Stufen unchecken.
	if (!bmShow1)	m_Plan1.SetCheck(0);
	if (!bmShow2)	m_Plan2.SetCheck(0);
	if (!bmShow3)	m_Plan3.SetCheck(0);
	if (!bmShow4)	m_Plan4.SetCheck(0);
	if (!bmShow5)	m_Plan5.SetCheck(0);
	if (!bmShowL)	m_PlanL.SetCheck(0);
	if (!bmShowU)	m_PlanU.SetCheck(0);
	if (!bmShowW)	m_PlanW.SetCheck(0);

	
	//
	// Werte aus der security
	//
	CString csCode;
	csCode = ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLANW");

	if(csCode=='1' && bmShowW) 
		m_PlanW.EnableWindow(true);
	else if(csCode=='0' && bmShowW)
		m_PlanW.EnableWindow(false);
	else if(csCode=='-')
		m_PlanW.ShowWindow(SW_HIDE);

	csCode = ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLANL");
	
	if(csCode=='1' && bmShowL) 
		m_PlanL.EnableWindow(true);
	else if(csCode=='0' && bmShowL)
		m_PlanL.EnableWindow(false);
	else if(csCode=='-')
		m_PlanL.ShowWindow(SW_HIDE);

	csCode = ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLANU");
	
	if(csCode=='1' && bmShowU)
		m_PlanU.EnableWindow(true);
	else if(csCode=='0' && bmShowU)
		m_PlanU.EnableWindow(false);
	else if(csCode=='-')
		m_PlanU.ShowWindow(SW_HIDE);

	csCode = ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN5b");
	
	if(csCode=='1' && bmShow5) 
		m_Plan5.EnableWindow(true);
	else if(csCode=='0' && bmShow5)
		m_Plan5.EnableWindow(false);
	else if(csCode=='-')
		m_Plan5.ShowWindow(SW_HIDE);
	
	csCode = ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN4");
	
	if(csCode=='1' && bmShow4) 
		m_Plan4.EnableWindow(true);
	else if(csCode=='0' && bmShow4)
		m_Plan4.EnableWindow(false);
	else if(csCode=='-')
		m_Plan4.ShowWindow(SW_HIDE);

	csCode = ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN3");
	
	if(csCode=='1' && bmShow3) 
		m_Plan3.EnableWindow(true);
	else if(csCode=='0' && bmShow3)
		m_Plan3.EnableWindow(false);
	else if(csCode=='-')
		m_Plan3.ShowWindow(SW_HIDE);

	csCode = ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN2");
	
	if(csCode=='1' && bmShow2) 
		m_Plan2.EnableWindow(true);
	else if(csCode=='0' && bmShow2)
		m_Plan2.EnableWindow(false);
	else if(csCode=='-')
		m_Plan2.ShowWindow(SW_HIDE);
	
	csCode = ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN1");
	
	if(csCode=='1' && bmShow1) 
		m_Plan1.EnableWindow(true);
	else if(csCode=='0' && bmShow1)
		m_Plan1.EnableWindow(false);
	else if(csCode=='-')
		m_Plan1.ShowWindow(SW_HIDE);	
}

//---------------------------------------------------------------------------

BOOL PSDutyRoster2ViewPage::GetData()
{
	omValues.RemoveAll();
	CStringArray olTmpValues;
	if(GetData(olTmpValues) == FALSE)
	{
		return FALSE;
	}
	for(int i = 0; i < olTmpValues.GetSize(); i++)
	{
		CString olTmp = olTmpValues[i];
		omValues.Add(olTmpValues[i]);
	}
	return TRUE;
}

//---------------------------------------------------------------------------

BOOL PSDutyRoster2ViewPage::GetData(CStringArray &opValues)
{
	if(ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLANW") != '1') m_PlanW.SetCheck(0);
	if(ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLANU") != '1') m_PlanU.SetCheck(0);
	if(ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLANL") != '1') m_PlanL.SetCheck(0);
	if(ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN5") != '1') m_Plan5.SetCheck(0);
	if(ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN4") != '1') m_Plan4.SetCheck(0);
	if(ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN3") != '1') m_Plan3.SetCheck(0);
	if(ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN2") != '1') m_Plan2.SetCheck(0);
	if(ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN1") != '1') m_Plan1.SetCheck(0);

	CString olPlan = "000000";
	olPlan.Format("%d%d%d%d%d%d%d%d",m_Plan1.GetCheck(),
									 m_Plan2.GetCheck(),
									 m_Plan3.GetCheck(),
									 m_Plan4.GetCheck(),
									 m_Plan5.GetCheck(),
									 m_PlanL.GetCheck(),
									 m_PlanU.GetCheck(),
									 m_PlanW.GetCheck());
	opValues.Add(olPlan);

	if(m_MAPresent.GetCheck() == 1)
		opValues.Add("PRESENT");
	else if(m_MANotPresent.GetCheck() == 1)
		opValues.Add("NOTPRESENT");
	else
		opValues.Add("ALL");

	return TRUE;
}

//---------------------------------------------------------------------------

void PSDutyRoster2ViewPage::SetData()
{
	SetData(omValues);
}

//---------------------------------------------------------------------------

void PSDutyRoster2ViewPage::SetData(CStringArray &opValues)
{
	m_Plan1.SetCheck(1);
	m_Plan2.SetCheck(1);
	m_Plan3.SetCheck(1);
	m_Plan4.SetCheck(1);
	m_Plan5.SetCheck(1);
	m_PlanL.SetCheck(0);
	m_PlanU.SetCheck(0);
	m_PlanW.SetCheck(0);

	m_MAAll.SetCheck(1);
	m_MAPresent.SetCheck(0);
	m_MANotPresent.SetCheck(0);
	//*** end> Set all adjustments to default

	for(int ilValue = 0; ilValue < opValues.GetSize(); ilValue++)
	{
		switch(ilValue)
		{
		default:
			break;
		case 0:
			{
				if(opValues[ilValue].IsEmpty() == FALSE)
				{
					//*** Set Plan types
					CString olPlan1,olPlan2,olPlan3,olPlan4,olPlan5,olPlanL,olPlanU,olPlanW;
					
					m_Plan1.SetCheck(0);
					m_Plan2.SetCheck(0);
					m_Plan3.SetCheck(0);
					m_Plan4.SetCheck(0);
					m_Plan5.SetCheck(0);
					m_PlanL.SetCheck(0);
					m_PlanU.SetCheck(0);
					m_PlanW.SetCheck(0);
					
					if(opValues[ilValue].GetLength() == 8)
					{
						olPlan1 = opValues[ilValue].Mid(0,1);
						olPlan2 = opValues[ilValue].Mid(1,1);
						olPlan3 = opValues[ilValue].Mid(2,1);
						olPlan4 = opValues[ilValue].Mid(3,1);
						olPlan5 = opValues[ilValue].Mid(4,1);
						olPlanL = opValues[ilValue].Mid(5,1);
						olPlanU = opValues[ilValue].Mid(6,1);
						olPlanW = opValues[ilValue].Mid(7,1);
					}
					
					if(olPlan1 == "1" && ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN1") == '1')
						m_Plan1.SetCheck(1);
					if(olPlan2 == "1" && ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN2") == '1')
						m_Plan2.SetCheck(1);
					if(olPlan3 == "1" && ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN3") == '1')
						m_Plan3.SetCheck(1);
					if(olPlan4 == "1" && ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN4") == '1')
						m_Plan4.SetCheck(1);
					if(olPlan5 == "1" && ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN5") == '1')
						m_Plan5.SetCheck(1);
					if(olPlanL == "1" && ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLANL") == '1')
						m_PlanL.SetCheck(1);
					if(olPlanU == "1" && ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLANU") == '1')
						m_PlanU.SetCheck(1);
					if(olPlanW == "1" && ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLANW") == '1')
						m_PlanW.SetCheck(1);
					//*** end> Set Plan types
				}
			}
			break;
		case 1:
			{
				if(opValues[ilValue] == "PRESENT")
				{
					m_MAAll.SetCheck(0);
					m_MAPresent.SetCheck(1);
					m_MANotPresent.SetCheck(0);
				}
				else if(opValues[ilValue] == "NOTPRESENT")
				{
					m_MAAll.SetCheck(0);
					m_MAPresent.SetCheck(0);
					m_MANotPresent.SetCheck(1);
				}
				else
				{
					m_MAAll.SetCheck(1);
					m_MAPresent.SetCheck(0);
					m_MANotPresent.SetCheck(0);
				}
			}
			break;
		}
	}
}

//---------------------------------------------------------------------------


void PSDutyRoster2ViewPage::SetButtonText()
{
	SetDlgItemText(IDC_PLAN1,LoadStg(IDS_STRING405));
	SetDlgItemText(IDC_PLAN2,LoadStg(IDS_STRING412));
	SetDlgItemText(IDC_PLAN3,LoadStg(IDS_STRING421));
	SetDlgItemText(IDC_PLAN4,LoadStg(IDS_STRING422));
	SetDlgItemText(IDC_PLAN5b,LoadStg(IDS_STRING423));
	SetDlgItemText(IDC_PLANU,LoadStg(IDS_STRING1680));
	SetDlgItemText(IDC_PLANW,LoadStg(IDS_STRING1681));
	SetDlgItemText(IDC_PLANL,LoadStg(IDC_PLANLU));
	SetDlgItemText(IDC_MA_NOTPRESENT,LoadStg(IDC_MA_NOTPRESENT));
	SetDlgItemText(IDC_MA_PRESENT,LoadStg(IDC_MA_PRESENT));
	SetDlgItemText(IDC_MA_ALL,LoadStg(IDC_MA_ALL));
}

void PSDutyRoster2ViewPage::SetStatic()
{
	SetDlgItemText(IDC_S_MAAnzeigen,LoadStg(IDC_S_MAAnzeigen));
	SetDlgItemText(IDC_S_PS,LoadStg(IDC_S_PS));
}
