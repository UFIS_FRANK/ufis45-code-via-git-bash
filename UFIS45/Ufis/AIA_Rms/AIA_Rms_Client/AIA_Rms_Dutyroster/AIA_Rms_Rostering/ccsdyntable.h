#if !defined(_CCSDYNTABLE_H_INCLUDED_)
#define _CCSDYNTABLE_H_INCLUDED_

#include <CCSGlobl.h>
#include <CCSDefines.h>
#include <CCSPtrArray.h>
#include <CCSEdit.h>
#include <ccsdragdropctrl.h>

/////////////////////////////////////////////////////////////////////////////
// CCSDynTable

/*
#define DT_MAROON			RGB(128,   0,   0)	// dark red
#define DT_GREEN			RGB(  0, 128,   0)	// dark green
#define DT_DKGREEN			RGB(  0, 200,   0)	// dark green
#define DT_OLIVE			RGB(128, 128,   0)	// dark yellow
#define DT_NAVY				RGB(  0,   0, 128)	// dark blue
#define DT_PURPLE			RGB(128,   0, 128)	// dark magenta
#define DT_TEAL				RGB(  0, 128, 128)	// dark cyan
#define DT_GRAY				RGB(128, 128, 128)	// dark gray
#define DT_RED				RGB(255,   0,   0)	
#define DT_ORANGE			RGB(255, 132,   0)	
#define DT_LIME				RGB(  0, 255,   0)	// green
#define DT_BLUE				RGB(  0,   0, 255)	
#define DT_FUCHSIA			RGB(255,   0, 255)	// magenta
#define DT_AQUA				RGB(  0, 255, 255)	// cyan
#define DT_MEDIUMDGRAY		RGB(164, 164, 164)
#define DT_LTGRAY			RGB(170, 170, 170)	
*/
#define DT_YELLOW				RGB(255, 255,   0)	
#define DT_WHITE				RGB(255, 255, 255)
#define DT_BLACK		 		RGB(  0,   0,   0)
#define DT_SILVER				RGB(192, 192, 192)
#define DT_LIGHTSILVER1			RGB(235, 235, 235)
#define DT_LIGHTSILVER2			RGB(217, 217, 217)
#define DT_SELECTBLUE			RGB(143, 239, 255)
#define DT_SILVERRED			RGB(236, 174, 174)
#define DT_SILVERGREEN			RGB(100, 174, 174)
#define DT_LIGHTYELLOW1			RGB(249, 249, 200)

#define DT_SILVERGREENLIGHT		RGB(100, 208, 208)
#define DT_SILVERREDLIGHT		RGB(236, 203, 203)
#define DT_SILVERREDLIGHT2		RGB(233, 195, 195)
#define DT_SILVERREDLIGHT3		RGB(255, 230, 230)
#define DT_SILVERBLUELIGHT		RGB(194, 232, 232)
#define DT_SILVERORANGELIGHT	RGB(244, 224, 197)
#define DT_LIGHTBLUE			RGB(125, 170, 215)

// kopiert aus 'OLEIDL.H'
#define	DROPEFFECT_NONE	( 0 )
#define	DROPEFFECT_COPY	( 1 )
#define	DROPEFFECT_MOVE	( 2 )
#define	DROPEFFECT_LINK	( 4 )

enum
{
	TTYPE_FIX,TTYPE_VAR,							//Table Type
	CONTROL,ALT,SHIFT,								//LButtonDown or RButtonDown (Send with INLINEDATA)
	SELECT,UNSELECT,ONLYSELECT,								//DragDrop LButtonDown or RButtonDown (Send with INLINEDATA)
	DRAGDROP_UPDATE,IPEDIT_UPDATE,					//Update Type (Send with INLINEDATA)
	MARKER_LT_T,MARKER_RT_T,MARKER_RB_T,MARKER_LB_T,//Trianlge Marker
	MARKER_L_B,MARKER_R_B,							//Beam Marker
	_TABLE,_HEADER,_SOLID,_SOLIDHEADER,				//Field Type
	NO_LINE,NORMAL_LINE,FAT_LINE,					//Line width, but not at the edge
	NORMAL_LINE_ALWAYS,FAT_LINE_ALWAYS				//Line width, draw always
};

// font style constants
enum
{
	FONT_STYLE_REGULAR = 0,
	FONT_STYLE_BOLD,
	FONT_STYLE_ITALIC
};

struct FIELDDATA
{
	CString			oText;
	COLORREF		oTextColor;
	int				oBkColorNr;
	int				iFontStyle;	// one of font style constants
	CCSEDIT_ATTRIB	rIPEAttrib;
	bool			bInplaceEdit;
	bool			bDrop;
	bool			bDrag;
	int				iRButtonDownStatus;
	CString			oToolTipText;
	CString			oRegEx;

	//Field Marker
	int				iLBeamColorNr;			// Left
	int				iRBeamColorNr;			// Right
	int				iLTTriangleColorNr;		// Left Top
	int				iRTTriangleColorNr;		// Right Top
	int				iRBTriangleColorNr;		// Right Bottom
	int				iLBTriangleColorNr;		// Left Bottom
	//fette Linien
	UINT			iLLineWidth;
	UINT			iTLineWidth;
	UINT			iRLineWidth;
	UINT			iBLineWidth;
	int				iLLineColorNr;
	int				iTLineColorNr;
	int				iRLineColorNr;
	int				iBLineColorNr;


	FIELDDATA(void)
	{
		bInplaceEdit		= false;
		bDrop				= false;
		bDrag				= false;
		iRButtonDownStatus	= 0;
		oText				= "";
		oToolTipText		= "";
		oTextColor			= BPN_NOCOLOR;
		oBkColorNr			= -1;
		iFontStyle			= FONT_STYLE_REGULAR;
		iLBeamColorNr		= -1;
		iRBeamColorNr		= -1;
		iLTTriangleColorNr	= -1;
		iRTTriangleColorNr	= -1;
		iRBTriangleColorNr	= -1;
		iLBTriangleColorNr	= -1;
		iLLineWidth			= NO_LINE;
		iTLineWidth			= NO_LINE;
		iRLineWidth			= NO_LINE;
		iBLineWidth			= NO_LINE;
		iLLineColorNr		= -1;
		iTLineColorNr		= -1;
		iRLineColorNr		= -1;
		iBLineColorNr		= -1;
	}
};

struct TABLEDATA
{
	FIELDDATA	oSolidHeader;
	CPtrArray	oHeader;		//Include *FIELDDATA's
	CPtrArray	oSolid;			//Include *FIELDDATA's
	CPtrArray	oTable;			//Include CPtrArray's with *FIELDDATA's
};

struct MODIFYTAB
{
	UINT iID;

	int iOldHorzPos;
	int iNewHorzPos;
	int iOldVertPos;
	int iNewVertPos;

	int iOldColumns;
	int iNewColumns;
	int iOldRows;
	int iNewRows;

	CRect oOldRect;
	CRect oNewRect;

	MODIFYTAB(void)
	{
		iID = 0;
		iOldHorzPos = 0;
		iNewHorzPos = 0;
		iOldVertPos = 0;
		iNewVertPos = 0;
		iOldColumns = 0;
		iNewColumns = 0;
		iOldRows    = 0;
		iNewRows    = 0;
		oOldRect	= CRect(0,0,0,0);
		oNewRect    = CRect(0,0,0,0);
	}
};

struct TABLEINFO
{
	UINT iID;
	int  iColumns;
	int  iRows;
	int  iColumnWidth;
	int  iRowHight;
	bool bSolid;
	bool bHeader;
	int iMaxDataHorz;
	int iMinDataHorz;
	int iMaxDataVert;
	int iMinDataVert;
	int iColumnOffset;
	int iRowOffset;

	TABLEINFO(void)
	{
		iID = 0;
		iColumns = 0;
		iRows    = 0;
		iColumnWidth = 0;
		iRowHight    = 0;
		bSolid   = false;
		bHeader  = false;
		iMaxDataHorz = 0;
		iMinDataHorz = 0;
		iMaxDataVert = 0;
		iMinDataVert = 0;
		iColumnOffset = 0;
		iRowOffset    = 0;
	}
};

struct IPEDITPAINTINFO
{
	bool bExist;
	int iOldRow;
	int iOldColumn;
	int iNewRow;
	int iNewColumn;
	IPEDITPAINTINFO(void)
	{
		bExist = false;
		iOldRow = -1;
		iOldColumn = -1;
		iNewRow = -1;
		iNewColumn = -1;
	}

};

struct INLINEDATA
{
	CString oOldText;
	CString oNewText;
	CString oFctc;
	long lNewUrno;
	int iRow;
	int iColumn;
	int iRowOffset;
	int iColumnOffset;
	int	iRButtonDownStatus;
	CPoint oPoint;
	UINT iDropeffect;	// bei Drag'n Drop-Aktionen: 
						//	DROPEFFECT_COPY (1) = Control-Taste gedrückt
						//	DROPEFFECT_MOVE (2) = Alt-Taste gedrückt
						//  DROPEFFECT_NONE (0) = keine Taste gedrückt
	UINT iFlag;

	INLINEDATA(void)
	{
		lNewUrno			= 0;
		iRow				= 0;
		iColumn				= 0;
		iRowOffset			= 0;
		iColumnOffset		= 0;
		iRButtonDownStatus	= 0;
		oPoint				= CPoint(0,0);
		iFlag				= 0;
		iDropeffect			= 0; // DROPEFFECT_NONE
	}
};


/////////////////////////////////////////////////////////////////////////////
// Fenster CCSDynTable 

class CCSDynTable : public CWnd
{
// Konstruktion
public:
	CCSDynTable(CWnd* pParent = NULL, UINT ipID = 0);

// Attribute
public:

protected:
//private:
	CWnd  *pomParent;
	int imOldParentWndWidth;
	int imOldParentWndHeight;
	UINT imID;

    CPen   omPen;
    CPen   omWhitePen, omBlackPen, omBlackPenFat, omSilverPen;
    CPen   omDefTablePen, omDefSolidPen, omDefHeaderPen;

    CPen omDefTablePen0, omDefTablePen1, omDefTablePen2, omDefTablePen3,
		 omDefTablePen4, omDefTablePen5, omDefTablePen6, omDefTablePen7,
		 omDefTablePen8, omDefTablePen9, omDefTablePen10, omDefTablePen11,
		 omDefTablePen12, omDefTablePen13, omDefTablePen14, omDefTablePen15,
		 omDefTablePen16, omDefTablePen17;
	CPtrArray omDefPens;






    CBrush omBrush;
    CBrush omWhiteBrush, omBlackBrush, omSilverBrush;

    CBrush omDefTableBrush, omDefSolidBrush, omDefHeaderBrush;
    CBrush omDefTableBrush0, omDefTableBrush1, omDefTableBrush2, omDefTableBrush3,
		   omDefTableBrush4, omDefTableBrush5, omDefTableBrush6, omDefTableBrush7,
		   omDefTableBrush8, omDefTableBrush9, omDefTableBrush10, omDefTableBrush11,
		   omDefTableBrush12, omDefTableBrush13, omDefTableBrush14, omDefTableBrush15,
		   omDefTableBrush16, omDefTableBrush17, omDefTableBrush18;
	CPtrArray omDefBrushes;

	COLORREF omDefTableColor, omDefHeaderColor, omDefSolidColor;
	COLORREF omDefTableTextColor, omDefHeaderTextColor, omDefSolidTextColor;

	CFont omDefTableFont;
	CFont omItalicTableFont;
	CFont omDefHeaderFont;
	CFont omDefSolidFont;

	CFont *pomDefTableFont;
	CFont *pomItalicTableFont;
	CFont *pomDefHeaderFont;
	CFont *pomDefSolidFont;

	UINT imDefTableFormat;
	UINT imDefHeaderFormat;
	UINT imDefSolidFormat;

	CRect omEraseBkgndRect;
	int imEBkRegion;

	int imColumns, imRows;
	int imColumnWidth, imHeaderHight, imRowHight, imSolidWidth;
	bool bmInplaceEdit;
	CRect omTableRect;
	UINT imTType;

	CScrollBar *pomHScroll;
	CScrollBar *pomVScroll;
	bool bmVScrollBar;
	bool bmHScrollBar;
	bool bmVSizing;
	bool bmHSizing;
	int	imScrollBarWidth;
	int	imScrollBarHight;
	int imMaxDataHorz;
	int imMinDataHorz;
	int imMaxDataVert;
	int imMinDataVert;
	int imColumnStartPos;	
	int imRowStartPos;
	int imHorzDiffToParentWnd;	//Important for HSizing
	int imVertDiffToParentWnd;	//Important for VSizing

	int imEditColumn;		//X-Position of the CCSEdit field in the table
	int imEditRow;			//Y-Position of the CCSEdit field in the table
	bool bmInplaceEditExist;//Inline CCSEdit field exist Y(=true)/N
	bool bmIPEditCRLF;		//true = CRLF at the horz. end of the table
	CCSEdit *pomIPEdit;
	CCSEDIT_ATTRIB *prmDefIEditAttrib;
	CCSEDIT_ATTRIB rmDefIEditAttrib;
	CCSEDIT_ATTRIB rmNoIEditAttrib;
	bool bmDestroyIPEditAction;
	INLINEDATA rmInlineData;

	TABLEDATA rmTableData;

	MODIFYTAB *prmModifyTab;
	IPEDITPAINTINFO rmIPEditPaintInfo;

	int imDropWord;
	int imDragWord;
	CCSDragDropCtrl omDragDrop;
	bool bmIsDropFieldSelected;
	
	// Test
	int m_id;

	// Der letzte Punkt ! (Mulitselect per Mausmove)
	CPoint omLastPoint;


	// ToolTipInfo
	CRect omOldToolTipRect; // in welschem Rect war das letzte ToolTip
	CToolTipCtrl  omToolTip;



// Operationen
public:

// Überschreibungen
	//{{AFX_VIRTUAL(CCSDynTable)
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	//}}AFX_VIRTUAL

// Implementierung
public:
	BOOL Create();
	void SetTableData(CRect opRect,bool bpInplaceEdit = false, bool bpHSB = true, bool bpVSB = true);
	void SetTableData(CRect &opRect, UINT ipColumns, UINT ipColumnWidth, UINT ipHeaderHight, UINT ipRows,
						UINT ipRowHight, UINT ipSolidWidth, bool bpInplaceEdit = false, bool bpIPEditCRLF = false,
						bool bpHSB = true, bool bpVSB = true, bool bpHSizing = true, bool bpVSizing = true);
	void SetDefaultColors(COLORREF opTableColor,COLORREF opTableTextColor, COLORREF opHeaderColor,
							COLORREF opHeaderTextColor, COLORREF opSolidColor, COLORREF opSolidTextColor);
	void SetDefaultColors(UINT ipIdx ,COLORREF opTableColor);
	void SetDefaultTextForm(CFont *popTableFont, CFont *popItalicTableFont, CFont *popHeaderFont, CFont *popSolidFont, UINT ipTableFormat,
								UINT ipHeaderFormat, UINT ipSolidFormat, CCSEDIT_ATTRIB * prpIEditAttrib);
	void SetDifferenceToParentWndRect(int ipHorizontal = 0,int ipVertical = 0);
	void ResetContent();
	TABLEDATA* GetTableData();
	void GetTableInfo(TABLEINFO &rpTableInfo);
	void SetVScrollData(UINT ipMin, UINT ipMax, UINT ipPos);
	void SetHScrollData(UINT ipMin, UINT ipMax, UINT ipPos);
	void ChangedTableData(int ipNewColumnOffset, int ipNewRowOffset);
	void ChangedFieldData(int ipRow ,int ipColumn);
	void SizeTable(/*UINT nSide, */LPRECT lpRect);
	void MoveTable(int ipX, int ipY);
	bool DeleteTable();
	bool GetIPEditStatus();
	void DragDropRevoke();
	void DropRegister(UINT ipDataClassWord);
	void DragRegister(UINT ipDataClassWord);
	void EndIPEditandDragDropAction();
	// Table auf bestimmte Position scrollen.
	void ScrollToPosition(int ipPosition);

	bool GetTableFieldByPoint(CPoint point,int &ipRow,int &ipColumn,CRect &opRect);
//private:
protected:
	virtual ~CCSDynTable();
	void PaintTable(CDC *popDC, int ipFromColumn = -1, int ipFromRow = -1,int ipToColumn = -1, int ipToRow = -1);
	void PaintText(CDC *popDC, int ipFromColumn = -1, int ipFromRow = -1,int ipToColumn = -1, int ipToRow = -1);
	void PaintFieldText(CDC *popDC, int ipColumn, int ipRow);
	void PaintExtraLines(CDC *popDC, CRect opRect, FIELDDATA *popFieldData, int ipCol, int ipTableCols, int ipRow, int ipTableRows);
	void PaintMarker(CDC *popDC, int ipMarker, CRect opRect, CBrush *popBrush);
	bool MakeInplaceEdit(int ipRow, int ipColumn);
	void EndInplaceEditing();
	void CreateIPEdit(int ipRow, int ipColumn);
	bool CheckInlineUpdate();
	bool IsIPEdit(int ipRow,int ipColumn);
	bool IsDrop(int ipRow,int ipColumn);
	bool IsDrag(int ipRow,int ipColumn);
	CString GetToolTipText(int ipRow,int ipColumn);

	bool GetDropStatus();
	// Begin einer Drag Aktion
	void OnDrag(CString opText); 

protected:
	//{{AFX_MSG(CCSDynTable)
	afx_msg void OnPaint();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
    afx_msg LONG OnMoveInplaceLeft(UINT, LONG); 
    afx_msg LONG OnMoveInplaceRight(UINT, LONG); 
    afx_msg LONG OnMoveInplaceUp(UINT, LONG); 
    afx_msg LONG OnMoveInplaceDown(UINT, LONG); 
    afx_msg LONG OnInplaceReturn(UINT, LONG); 
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
    afx_msg LONG OnDragOver(UINT wParam, LONG lParam); 
    afx_msg LONG OnDrop(UINT wParam, LONG lParam); 
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSizing(UINT fwSide, LPRECT pRect);
	afx_msg void OnClose();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
// Fenster CCSDebitDynTable 

class CCSDebitDynTable : public CCSDynTable
{
	// Konstruktion
public:
	CCSDebitDynTable(CWnd* pParent = NULL, UINT ipID = 0);

protected:
	//{{AFX_MSG(CCSDebitDynTable)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}

#endif // !defined(_CCSDYNTABLE_H_INCLUDED_)
