// CedaSwgData.cpp
 
#include <stdafx.h>

CedaSwgData ogSwgData;
	
void ProcessSwgCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

static int CompareTimes(const SWGDATA **e1, const SWGDATA **e2);
static int CompareSurnAndTimes(const SWGDATA **e1, const SWGDATA **e2);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaSwgData::CedaSwgData() : CedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for SWGDataStruct
	BEGIN_CEDARECINFO(SWGDATA,SWGDataRecInfo)
		FIELD_CHAR_TRIM	(Code,"CODE")
		FIELD_LONG		(Surn,"SURN")
		FIELD_LONG		(Urno,"URNO")
		FIELD_OLEDATE	(Vpfr,"VPFR")
		FIELD_OLEDATE	(Vpto,"VPTO")
	END_CEDARECINFO //(SWGDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(SWGDataRecInfo)/sizeof(SWGDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&SWGDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"SWG");
	strcat(pcmTableName,pcgTableExt);
	strcpy(pcmListOfFields,"CODE,SURN,URNO,VPFR,VPTO");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaSwgData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaSwgData::Register(void)
{
	DdxRegister((void *)this,BC_SWG_CHANGE,	"SWGDATA", "Swg-changed",	ProcessSwgCf);
	DdxRegister((void *)this,BC_SWG_NEW,	"SWGDATA", "Swg-new",		ProcessSwgCf);
	DdxRegister((void *)this,BC_SWG_DELETE,	"SWGDATA", "Swg-deleted",	ProcessSwgCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaSwgData::~CedaSwgData(void)
{
	TRACE("CedaSwgData::~CedaSwgData called\n");
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaSwgData::ClearAll(bool bpWithRegistration)
{
	TRACE("CedaSwgData::ClearAll called\n");
	omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaSwgData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(pspWhere == NULL)
	{	
		ilRc = CedaAction2("RT");
	}
	else
	{
		ilRc = CedaAction2("RT", pspWhere);
	}
	if (ilRc != true)
	{
		TRACE("Read-Swg: Ceda-Error %d \n",ilRc);
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
	{
		SWGDATA *prlSwg = new SWGDATA;
		if ((ilRc = GetFirstBufferRecord2(prlSwg)) == true)
		{
			if(IsValidSwg(prlSwg))
			{
				omData.Add(prlSwg);//Update omData
				omUrnoMap.SetAt((void *)prlSwg->Urno,prlSwg);
#ifdef TRACE_FULL_DATA
				// Datensatz OK, loggen if FULL
				ogRosteringLogText.Format(" read %8.8lu %s OK:  ", ilCountRecord, pcmTableName);
				GetDataFormatted(ogRosteringLogText, prlSwg);
				WriteLogFull("");
#endif TRACE_FULL_DATA
			}
			else
			{
				delete prlSwg;
			}
		}
		else
		{
			delete prlSwg;
		}
	}
	TRACE("Read-Swg: %d gelesen\n",ilCountRecord-1);
        		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  read %d records from table %s",ilCountRecord-1, pcmTableName);
		WriteInRosteringLog();
	}

	return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaSwgData::Insert(SWGDATA *prpSwg)
{
	prpSwg->IsChanged = DATA_NEW;
	if(Save(prpSwg) == false) return false; //Update Database
	InsertInternal(prpSwg);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaSwgData::InsertInternal(SWGDATA *prpSwg)
{
	ogDdx.DataChanged((void *)this, SWG_NEW,(void *)prpSwg ); //Update Viewer
	omData.Add(prpSwg);//Update omData
	omUrnoMap.SetAt((void *)prpSwg->Urno,prpSwg);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaSwgData::Delete(long lpUrno)
{
	SWGDATA *prlSwg = GetSwgByUrno(lpUrno);
	if (prlSwg != NULL)
	{
		prlSwg->IsChanged = DATA_DELETED;
		if(Save(prlSwg) == false) return false; //Update Database
		DeleteInternal(prlSwg);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaSwgData::DeleteInternal(SWGDATA *prpSwg)
{
	ogDdx.DataChanged((void *)this,SWG_DELETE,(void *)prpSwg); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpSwg->Urno);
	int ilSwgCount = omData.GetSize();
	for (int ilCountRecord = 0; ilCountRecord < ilSwgCount; ilCountRecord++)
	{
		if (omData[ilCountRecord].Urno == prpSwg->Urno)
		{
			omData.DeleteAt(ilCountRecord);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaSwgData::Update(SWGDATA *prpSwg)
{
	if (GetSwgByUrno(prpSwg->Urno) != NULL)
	{
		if (prpSwg->IsChanged == DATA_UNCHANGED)
		{
			prpSwg->IsChanged = DATA_CHANGED;
		}
		if(Save(prpSwg) == false) return false; //Update Database
		UpdateInternal(prpSwg);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaSwgData::UpdateInternal(SWGDATA *prpSwg)
{
	SWGDATA *prlSwg = GetSwgByUrno(prpSwg->Urno);
	if (prlSwg != NULL)
	{
		*prlSwg = *prpSwg; //Update omData
		ogDdx.DataChanged((void *)this,SWG_CHANGE,(void *)prlSwg); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

SWGDATA *CedaSwgData::GetSwgByUrno(long lpUrno)
{
	SWGDATA  *prlSwg;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlSwg) == TRUE)
	{
		return prlSwg;
	}
	return NULL;
}

//---------------------------------------------------------------------------------------------------------

void CedaSwgData::GetSwgBySurnWithTime(long lpSurn,CTime opStart,CTime opEnd,CCSPtrArray<SWGDATA> *popSwgData)
{
	SWGDATA  *prlSwg;
	COleDateTime olStart,olEnd;

	popSwgData->DeleteAll();
	
	CCSPtrArray<SWGDATA> olSwgData;
	POSITION rlPos;
	for ( rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSwg);
		if((prlSwg->Surn == lpSurn))
		{
			olSwgData.Add(prlSwg); 
		}
	}
	int ilSize = olSwgData.GetSize();
	if(ilSize > 0)
	{
		olSwgData.Sort(CompareTimes);
		
		olStart.SetDateTime(opStart.GetYear(),opStart.GetMonth(),opStart.GetDay(),opStart.GetHour(),opStart.GetMinute(),opStart.GetSecond());
		olEnd.SetDateTime(opEnd.GetYear(),opEnd.GetMonth(),opEnd.GetDay(),opEnd.GetHour(),opEnd.GetMinute(),opEnd.GetSecond());
		SWGDATA *prlPrevSwg=NULL;
		for(int i=ilSize;--i>=0;)
		{
			prlSwg = &olSwgData[i];
			
			if(prlSwg->Vpfr<=olEnd)
			{
				switch(prlSwg->Vpto.GetStatus())
				{
				case COleDateTime::valid:
					if(prlSwg->Vpto>=olStart)
					{
						popSwgData->Add(prlSwg);
					}
					else
					{
						i=0;
					}
					break;
				case COleDateTime::null:
					if(prlPrevSwg!=NULL)
					{
						if(prlPrevSwg->Vpfr>=olStart)
						{
							popSwgData->Add(prlSwg);
						}
						else
						{
							i=0;
						}
					}
					else
					{
						popSwgData->Add(prlSwg);
					}
					break;
				default:
					break;
				}
			}
			prlPrevSwg = prlSwg;
		}
	}
}

//---------------------------------------------------------------------------------------

CString CedaSwgData::GetWgpBySurnWithTime(long lpSurn, COleDateTime opDate)
{
	CString olWgpCode;

	CTime olDay(opDate.GetYear(),opDate.GetMonth(),opDate.GetDay(),0,0,0);
	CCSPtrArray<SWGDATA> olSwgData;

	GetSwgBySurnWithTime(lpSurn, olDay, olDay, &olSwgData);
	int ilSwgSize = olSwgData.GetSize();

	for(int i=0; i<ilSwgSize; i++)
	{
		if(olSwgData[i].Vpfr.GetStatus() == COleDateTime::valid)
		{
			if(olSwgData[i].Vpfr <= opDate)
			{
				switch(olSwgData[i].Vpto.GetStatus())
				{
				case COleDateTime::valid:
					if(olSwgData[i].Vpto >= opDate)
					{
						olWgpCode = olSwgData[i].Code;
					}
					else
					{
						olWgpCode = "";

					}
					break;
				default:
						olWgpCode = olSwgData[i].Code;
					break;
				}
			}
		}
	}
	olSwgData.RemoveAll();
	return olWgpCode;
}

//---------------------------------------------------------------------------------------

SWGDATA* CedaSwgData::GetSwgBySurnWithTime(long lpSurn, COleDateTime opDate)
{
	SWGDATA *polSwg = NULL;

	CTime olDay(opDate.GetYear(),opDate.GetMonth(),opDate.GetDay(),0,0,0);
	CCSPtrArray<SWGDATA> olSwgData;

	GetSwgBySurnWithTime(lpSurn, olDay, olDay, &olSwgData);
	int ilSwgSize = olSwgData.GetSize();

	for(int i=0; i<ilSwgSize; i++)
	{
		if(olSwgData[i].Vpfr.GetStatus() == COleDateTime::valid)
		{
			if(olSwgData[i].Vpfr <= opDate)
			{
				switch(olSwgData[i].Vpto.GetStatus())
				{
				case COleDateTime::valid:
					if(olSwgData[i].Vpto >= opDate)
					{
						polSwg = &olSwgData[i];
					}
					else
					{
						polSwg = NULL;

					}
					break;
				default:
						polSwg = &olSwgData[i];
					break;
				}
			}
		}
	}
	olSwgData.RemoveAll();
	return polSwg;
}

/************************************************************************************
BDA 01.03.2000 Speichert WGPC in den Stammdaten, - DRG-Broadcast-Bearbeitung
ACHTUNG! Es wird nicht gepr�fft, ob WGPC korrekt ist, nur die L�nge!
return false, a) kein Satz mit angegebenen Parametern gefunden wurde oder 
b) *popWgpc zu lang oder
c) der Inhalt ist gleich - es gibt nichts 'upzudaten'
************************************************************************************/

bool CedaSwgData::SetWgpBySurnWithTime(long lpSurn, COleDateTime opDate, LPCTSTR popWgpc)
{
	if(_tcslen(popWgpc) > SWG_CODE_LEN) return false;	// WGPC zu lang
	
	CTime olDay(opDate.GetYear(),opDate.GetMonth(),opDate.GetDay(),0,0,0);
	CCSPtrArray<SWGDATA> olSwgData;
	
	GetSwgBySurnWithTime(lpSurn, olDay, olDay, &olSwgData);
	int ilSwgSize = olSwgData.GetSize();
	
	bool bRet = false;
	for(int i=0; i<ilSwgSize; i++)
	{
		if(olSwgData[i].Vpfr.GetStatus() == COleDateTime::valid)
		{
			if(olSwgData[i].Vpfr <= opDate)
			{
				switch(olSwgData[i].Vpto.GetStatus())
				{
				case COleDateTime::valid:
					if(olSwgData[i].Vpto >= opDate)
					{
						// Datensatz gefunden, kann erneuert werden
						if(_tcscmp(olSwgData[i].Code, popWgpc))
						{
							_tcscpy(olSwgData[i].Code, popWgpc);
							bRet = true;
						}
					}
					break;
				default:
					// Datensatz gefunden, kann erneuert werden
					if(_tcscmp(olSwgData[i].Code, popWgpc))
					{
						_tcscpy(olSwgData[i].Code, popWgpc);
						bRet = true;
					}
					break;
				}
				break;
			}
		}
	}
	olSwgData.RemoveAll();
	return bRet;
}

//---------------------------------------------------------------------------------------------------------

CString CedaSwgData::GetSwgByWgpWithTime(CStringArray *popWgp, COleDateTime opStart, COleDateTime opEnd, CCSPtrArray<SWGDATA> *popSwgData)
{
	popSwgData->DeleteAll();
	CString olUrnos = "";

	if(popWgp->GetSize() > 0 && opStart.GetStatus() == COleDateTime::valid && opEnd.GetStatus() == COleDateTime::valid && opStart <= opEnd)
	{
		CString olTmpCode;
		COleDateTime olTmpStart,olTmpEnd;
		CMapStringToPtr olWgpMap;
		SWGDATA *prlSwg = NULL;


		for(int i=0; i<popWgp->GetSize(); i++)
		{
			olWgpMap.SetAt(popWgp->GetAt(i),NULL);
		}

		CCSPtrArray<SWGDATA> olSwgData;
		POSITION rlPos;
		for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSwg);
			olSwgData.Add(prlSwg); 
		}

		olSwgData.Sort(CompareSurnAndTimes);
		int ilSize = olSwgData.GetSize();
		SWGDATA *prlNextSwg = NULL;
		void  *prlVoid = NULL;

		for(i = 0; i < ilSize; i++)
		{
			prlSwg = &olSwgData[i];
			prlNextSwg = NULL;
			if((i+1) < ilSize)
			{
				if(prlSwg->Surn == olSwgData[i+1].Surn)
					prlNextSwg = &olSwgData[i+1];
			}

			if(prlSwg->Vpfr.GetStatus() == COleDateTime::valid)
			{
				if(prlSwg->Vpfr<=opEnd)
				{
					switch(prlSwg->Vpto.GetStatus())
					{
					case COleDateTime::valid:
						if(prlSwg->Vpto>=opStart)
						{
							olTmpCode.Format("%s",prlSwg->Code);
							if(olWgpMap.Lookup(olTmpCode,(void *&)prlVoid) == TRUE)
							{
								popSwgData->Add(prlSwg);
							}
						}
						break;
					case COleDateTime::null:
						if(prlNextSwg != NULL)
						{
							if(prlNextSwg->Vpfr>=opStart)
							{
								olTmpCode.Format("%s",prlSwg->Code);
								if(olWgpMap.Lookup(olTmpCode,(void *&)prlVoid) == TRUE)
								{
									popSwgData->Add(prlSwg);
								}
							}
						}
						else
						{
							olTmpCode.Format("%s",prlSwg->Code);
							if(olWgpMap.Lookup(olTmpCode,(void *&)prlVoid) == TRUE)
							{
								popSwgData->Add(prlSwg);
							}
						}
						break;
					default:
						break;
					}
				}
			}
		}
		// Create Stf-Urno-List for return value
		ilSize = popSwgData->GetSize();
		olUrnos = ",";
		for(i = 0; i < ilSize; i++)
		{
			CString olTmpUrno;
			olTmpUrno.Format(",%ld,",(*popSwgData)[i].Surn);
			if(olUrnos.Find(olTmpUrno) == -1)
			{
				olUrnos += olTmpUrno.Mid(1);
			}
		}
		if(olUrnos.IsEmpty() == FALSE)
		{
			olUrnos = olUrnos.Mid(1,olUrnos.GetLength()-2);
		}
		olWgpMap.RemoveAll();
	}
	return olUrnos;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaSwgData::ReadSpecialData(CCSPtrArray<SWGDATA> *popSwg,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction2("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popSwg != NULL)
	{
		for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
		{
			SWGDATA *prpSwg = new SWGDATA;
			if ((ilRc = GetBufferRecord2(ilCountRecord,prpSwg,CString(pclFieldList))) == true)
			{
				popSwg->Add(prpSwg);
			}
			else
			{
				delete prpSwg;
			}
		}
		if(popSwg->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaSwgData::Save(SWGDATA *prpSwg)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpSwg->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpSwg->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpSwg);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpSwg->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSwg->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpSwg);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpSwg->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSwg->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
	TRACE("Swg-IRT/URT/DRT: Ceda-Return %d \n",ilRc);
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessSwgCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogSwgData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void CedaSwgData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlSwgData;
	prlSwgData = (struct BcStruct *) vpDataPointer;
	SWGDATA *prlSwg;
	
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("ProcessBC:\n  Cmd <%s>\n  Tbl <%s>\n  Twe <%s>\n  Sel <%s>\n  Fld <%s>\n  Dat <%s>\n  Bcn <%s>",prlSwgData->Cmd, prlSwgData->Object, prlSwgData->Twe, prlSwgData->Selection, prlSwgData->Fields, prlSwgData->Data,prlSwgData->BcNum);
		WriteInRosteringLog();
	}
	
	switch(ipDDXType)
	{
	default:
		break;
	case BC_SWG_CHANGE:
		prlSwg = GetSwgByUrno(GetUrnoFromSelection(prlSwgData->Selection));
		if(prlSwg != NULL)
		{
			GetRecordFromItemList(prlSwg,prlSwgData->Fields,prlSwgData->Data);
			UpdateInternal(prlSwg);
			break;
		}
	case BC_SWG_NEW:
		prlSwg = new SWGDATA;
		GetRecordFromItemList(prlSwg,prlSwgData->Fields,prlSwgData->Data);
		InsertInternal(prlSwg);
		break;
	case BC_SWG_DELETE:
		{
			long llUrno;
			CString olSelection = (CString)prlSwgData->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlSwgData->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			prlSwg = GetSwgByUrno(llUrno);
			if (prlSwg != NULL)
			{
				DeleteInternal(prlSwg);
			}
		}
		break;
	}
}

/***********************************************************
***********************************************************/
bool CedaSwgData::IsValidSwg(SWGDATA *prpSwg)
{
	// long 			Surn;
	if(!ogStfData.GetStfByUrno(prpSwg->Surn))
	{
		CString olErr;
		olErr.Format("Work Group Connection Table (SWGTAB) URNO '%d' is defect, Employee with URNO '%d' in STFTAB not found\n",
			prpSwg->Urno, prpSwg->Surn);
		ogErrlog += olErr;
		
		if(IsTraceLoggingEnabled())
		{
			GetDefectDataString(ogRosteringLogText, (void*)prpSwg, "SWGTAB.SURN in STFTAB not found");
			WriteInRosteringLog(LOGFILE_TRACE);
		}
		return false;	// 1. Defekter Datensatz
	}

	// char 			Code[SWG_CODE_LEN];	// = Wgpc
	if(!ogWgpData.GetWgpByWgpc(CString(prpSwg->Code)))
	{
		STFDATA* prlStf = ogStfData.GetStfByUrno(prpSwg->Surn);

		CString olErr;
		olErr.Format("Work group '%s' for %s,%s in Work Groups Table not found\n",
			prpSwg->Code, prlStf->Lanm, prlStf->Finm);
		ogErrlog += olErr;
		
		if(IsTraceLoggingEnabled())
		{
			GetDefectDataString(ogRosteringLogText, (void*)prpSwg, "SWGTAB.CODE in WGPTAB not found");
			WriteInRosteringLog(LOGFILE_TRACE);
		}
		return false;	// 1. Defekter Datensatz
	}

	// COleDateTime	Vpfr;
	// COleDateTime	Vpto;
	if(prpSwg->Vpfr.GetStatus() == COleDateTime::invalid || prpSwg->Vpto.GetStatus() == COleDateTime::invalid)
	{
		STFDATA* prlStf = ogStfData.GetStfByUrno(prpSwg->Surn);

		CString olErr;
		olErr.Format("Work group time for %s,%s is not valid\n",
			prlStf->Lanm, prlStf->Finm);
		ogErrlog += olErr;
		
		if(IsTraceLoggingEnabled())
		{
			GetDefectDataString(ogRosteringLogText, (void*)prpSwg, "SWGTAB.VPFR or SWGTAB.VPTO is not valid");
			WriteInRosteringLog(LOGFILE_TRACE);
		}
		return false;	// 1. Defekter Datensatz
	}
	
	return true;
}

//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------

static int CompareTimes(const SWGDATA **e1, const SWGDATA **e2)
{
	if((**e1).Vpfr>(**e2).Vpfr)
		return 1;
	else
		return -1;
}

//---------------------------------------------------------------------------------------------------------

static int CompareSurnAndTimes(const SWGDATA **e1, const SWGDATA **e2)
{
	if((**e1).Surn>(**e2).Surn) 
		return 1;
	else if((**e1).Surn<(**e2).Surn) 
		return -1;

    if((**e1).Vpfr>(**e2).Vpfr) 
		return 1;
	else if((**e1).Vpfr<(**e2).Vpfr) 
		return -1;

	return 0;
}

//---------------------------------------------------------------------------------------------------------

