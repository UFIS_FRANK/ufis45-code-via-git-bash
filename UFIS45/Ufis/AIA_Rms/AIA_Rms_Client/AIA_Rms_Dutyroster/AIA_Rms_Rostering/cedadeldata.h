#ifndef _CDELSD_H_
#define _CDELSD_H_
 
//#include <afxwin.h>
//#include "ccsobj.h"

#include <stdafx.h>
/*#include "ccsglobl.h"
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
//#include "CedaDsrData.h"
#include <ccsddx.h>*/


///////////////////////////////////////////////////////////////////////////// 

struct DELDATA {
	long	Urno; 					// Eindeutige Datensatz ID
	long	Bsdu;		 	 	 	// Datensatznummer des BSD
	char	Dpt1[8+2]; 				// Code der Organisationseinheit aus ORGTAB
	char	Fctc[FCTC_LEN+2]; 		// Code der Funktion aus PFCTAB
	char	Delf[4+2]; 				// Abordnung von	HHMM
	char	Delt[4+2]; 				// Abordung bis		HHMM
	char	Sdac[SDAC_LEN+2]; 	// Abwesenheitscode (Code aus ODA)
	
	int  IsChanged;			// check whether data has changed for release

	DELDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged = FALSE;
	}
 };	

// the broadcast CallBack function, has to be outside the CedaDelData class
void ProcessDelCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

class CedaDelData: public CedaData
{
// Attributes
public:
    CCSPtrArray<DELDATA> omData;
	//CUIntArray omSelectedData;
	
    CMapPtrToPtr omUrnoMap;

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}
	//bool ChangeTime(long lpUrno,CTime opStartTime,CTime opEndTime,GanttDynType bpDynType,BOOL bpSave = TRUE);
// Operations
public:
    CedaDelData();
	~CedaDelData();
	void Register(void);
	bool Read(char *pspWhere = NULL);
	//bool ReadSpecialData(CCSPtrArray<MSDDATA> *popSdt, char *pspWhere, char *pspFieldList, bool ipSYS);
	//bool DeleteMsdWithRos(long lpRosu,CTime opStart,CTime opEnd,bool bpWithDdx);
 	void ProcessDelBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	bool InsertInternal(DELDATA *prpDel, bool bpWithDDX = false);
	bool UpdateDelInternal(DELDATA *prpDel);
	bool ClearAll();
	bool ReadSpecialData(CCSPtrArray<DELDATA> *popDel, char *pspWhere, char *pspFieldList, bool ipSYS);
	DELDATA * GetDelByUrno(char pcpUrno);
	bool InsertDel(DELDATA *prpDELDATA, BOOL bpWithSave = FALSE);
	bool SaveDel(DELDATA *prpDel);
	bool DelExist(long Urno);
	bool IsValidDel(DELDATA *prpDel);

	/*********************************************************************************************
	GetDelListByBsdu: sucht alle Datens�tze mit Bsdu
	wenn popDrrList == 0, wird keine Liste ausgef�llt, nur die Anzahl Datens�tze zur�ckgegeben
	R�ckgabe:	Anzahl der gefundenen Datens�tze
	*********************************************************************************************/
	int GetDelListByBsdu(long lpBsdu, bool bpGetAbscenses, CCSPtrArray<DELDATA> *popDelList=0);
	
	//uhi 8.3.01
	bool HasDel(long lpBsdu, CString opSdac, CString opAbfr, CString opAbto);
	bool HasDel(long lpBsdu, const CString& ropOrg,const CString& ropFctc,const CString& ropAbfr,const CString& ropAbto);

	/*bool GetMsdsBetweenTimes(CTime opStart,CTime opEnd,CCSPtrArray<MSDDATA> &opSdtArray);
	bool GetMsdsBetweenTimes(CTime opStart,CTime opEnd,CMapPtrToPtr &opSdtUrnoMap);
	bool ChangeMsdTime(long lpMsdUrno,CTime opNewStart,CTime opNewEnd, bool bpWithSave = false);
	bool MsdExist(long Urno);
	bool DnamExist(CString opDnam);

	bool UpdateMsd(MSDDATA *prpMSDDATA, BOOL bpWithSave = FALSE);
	bool DeleteMsd(MSDDATA *prpMSDDATA, BOOL bpWithSave = FALSE);

	bool DeleteMsdInternal(MSDDATA *prpMsd);
	void MakeMsdSelected(MSDDATA *prlMsd, BOOL bpSelect);
	void DeSelectAll();
	*/
	void Release(CCSPtrArray<DELDATA> *popDelList = NULL);
	//void GetAllDnam(CStringArray &ropList);
	//bool GetSelectedMsd(CCSPtrArray<MSDDATA> *popSelectedMsd);
	//void DeleteByMsdu(long lpMsdUrno);
	/*void DeleteBySdgu(long lpSdgUrno);
	void GetMsdByBsdu(long lpBsdu,CCSPtrArray<MSDDATA> *popMSDDATA);
	void GetMsdByRosu(long lpSdgu,CCSPtrArray<MSDDATA> *popMSDDATA);
	bool DeleteMsdWithSdgu(MSDDATA *prpMSDDATA,bool bpWithSave = true);*/

	//bool DeleteAllSdtsWithRosu(long lpRosu,bool bpWithDdx = true);
	//bool DeleteSdtWithRosuBetweenTimes(long lpRosu,CTime opStart,CTime opEnd,bool bpWithDdx = true);
	//void PrepareMSDDATA(MSDDATA *prpSdt);
    //bool InsertSdt(const MSDDATA *prpMSDDATA);    // used in PrePlanTable only
    //bool UpdateDel(const DELDATA *prpDELDATA);    

private:
	// Schnelles Suchen
	int FindFirstOfBsdu(long lpBsdu);

};

extern CedaDelData ogDelData;

#endif
