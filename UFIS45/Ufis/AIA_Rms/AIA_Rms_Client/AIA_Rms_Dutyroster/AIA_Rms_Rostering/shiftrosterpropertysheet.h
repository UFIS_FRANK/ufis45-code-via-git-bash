// ShiftRosterPropertySheet.h : header file
//
#ifndef _SHIFTROSTERPROPERTYSHEET_H_
#define _SHIFTROSTERPROPERTYSHEET_H_


#include <BasePropertySheet.h>
#include <PSShiftRoster1ViewPage.h>
#include <PSShiftRoster2ViewPage.h>
//uhi 25.4.01
#include <PSShiftRoster3ViewPage.h>

/////////////////////////////////////////////////////////////////////////////
// ShiftRosterPropertySheet

class ShiftRosterPropertySheet : public BasePropertySheet
{
// Construction
public:
	ShiftRosterPropertySheet(CString opCalledFrom, CWnd* pParentWnd = NULL,	CViewer *popViewer = NULL, UINT iSelectPage = 0);

// Attributes
public:
	PSShiftRoster1ViewPage omShiftRosterView;
	PSShiftRoster2ViewPage omShiftRoster2View;
	//uhi 25.4.01
	PSShiftRoster3ViewPage omShiftRoster3View;
	CString omCalledFrom;

// Operations
public:
	virtual void LoadDataFromViewer();
	virtual void SaveDataToViewer(CString opViewName,BOOL bpSaveToDb = TRUE);
	virtual int QueryForDiscardChanges();

};

/////////////////////////////////////////////////////////////////////////////

#endif // _SHIFTROSTERPROPERTYSHEET_H_
