// cSdtd.cpp - Class for handling Sdt data
//

#include <stdafx.h>

// Das globale Objekt
CedaSdgData ogSdgData;

//**********************************************************************************
//
//**********************************************************************************

void  ProcessSdgCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	// Sanduhr einblenden, falls es l�nger dauert
	AfxGetApp()->DoWaitCursor(1);
	// Empf�nger ermitteln
	CedaSdgData *polSdgData = (CedaSdgData *)vpInstance;
	// Bearbeitungsfunktion des Empf�ngers aufrufen
	polSdgData->ProcessSdgBc(ipDDXType,vpDataPointer,ropInstanceName);
	// Sanduhr ausblenden
	AfxGetApp()->DoWaitCursor(-1);
}

CedaSdgData::CedaSdgData()
{                  
    // Create an array of CEDARECINFO for STAFFDATA
    BEGIN_CEDARECINFO(SDGDATA, SdtDataRecInfo)
		FIELD_DATE	   (Begi, "BEGI")
		FIELD_CHAR_TRIM(Days, "DAYS")	
		FIELD_CHAR_TRIM(Dnam, "DNAM")	
		FIELD_CHAR_TRIM(Dura, "DURA")	
		FIELD_CHAR_TRIM(Peri, "PERI")	
		FIELD_CHAR_TRIM(Repi, "REPI")	
		FIELD_LONG	   (Urno, "URNO")
		FIELD_CHAR_TRIM(Expd, "EXPD")
    END_CEDARECINFO
    // Copy the record structure
    for (int i = 0; i < sizeof(SdtDataRecInfo)/sizeof(SdtDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&SdtDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
    // Initialize table names and field names
	strcpy(pcmTableName,"SDG");
	strcat(pcmTableName,pcgTableExt);

	strcpy(pcmListOfFields,"BEGI,DAYS,DNAM,DURA,PERI,REPI,URNO,EXPD");

	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	ClearAll();
}

//**********************************************************************************
//
//**********************************************************************************

CedaSdgData::~CedaSdgData()
{
	ogDdx.UnRegister(this,NOTUSED);
	
	ClearAll();
	omRecInfo.DeleteAll();
}	

//**********************************************************************************
//
//**********************************************************************************

bool CedaSdgData::ReadSpecialData(CCSPtrArray<SDGDATA> *popSdt, char *pspWhere, char *pspFieldList, bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[2048] = " ";

	if(strlen(pspFieldList) > 0)
	{
		if( pspFieldList[0] == '*' )
		{
			strcpy(pclFieldList, pcmFieldList);
		}
		else
		{
			strcpy(pclFieldList, pspFieldList);
		}
	}
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction2("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	int ilCountRecord = 0;
	if(popSdt != NULL)
	{
		for (ilCountRecord = 0; ilRc == true; ilCountRecord++)
		{
			SDGDATA *prpSdg = new SDGDATA;
			if ((ilRc = GetBufferRecord2(ilCountRecord,prpSdg,CString(pclFieldList))) == true)
			{
				popSdt->Add(prpSdg);
				omUrnoMap.SetAt((void *)prpSdg->Urno,prpSdg);
			}
			else
			{
				delete prpSdg;
			}
		}
		if(popSdt->GetSize() == 0) return false;
	}
    
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  read %d records from table %s",ilCountRecord-1, pcmTableName);
		WriteInRosteringLog();
	}

	return true;
}

//**********************************************************************************
//
//**********************************************************************************

bool CedaSdgData::Read(char *pspWhere /*NULL*/)
{
	bool ilRc = true;

	ClearAll();

	CTime omTime;

    // Select data from the database
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(pspWhere == NULL)
	{	
		if (CedaAction2("RTA", "") == false)
		{
			return false;
		}
	}
	else
	{
		if (CedaAction2("RT", pspWhere) == false)
		{
			return false;
		}
	}

    // Load data from CedaData into the dynamic array of record


    for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
    {
		SDGDATA *prlSdg = new SDGDATA;
		if ((ilRc = GetBufferRecord2(ilCountRecord,prlSdg)) == true)
		{
			prlSdg->IsChanged = DATA_UNCHANGED;
			AddSdgInternal(prlSdg);
#ifdef TRACE_FULL_DATA
			// Datensatz OK, loggen if FULL
			ogRosteringLogText.Format(" read %8.8lu %s OK:  ", ilCountRecord, pcmTableName);
			GetDataFormatted(ogRosteringLogText, prlSdg);
			WriteLogFull("");
#endif TRACE_FULL_DATA
		}
		else
		{
			delete prlSdg;
		}
	}
    
	// beim Broadcast-Handler anmelden, wenn gew�nscht
	Register();

	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  read %d records from table %s",ilCountRecord-1, pcmTableName);
		WriteInRosteringLog();
	}

    return true;
}

//**********************************************************************************
//
//**********************************************************************************

bool CedaSdgData::ClearAll()
{
	omUrnoMap.RemoveAll();
    omData.DeleteAll();

	// beim BC-Handler abmelden
	if(bmIsBCRegistered)
	{
		ogDdx.UnRegister(this,NOTUSED);
		// jetzt ist die Instanz angemeldet
		bmIsBCRegistered = false;
	}

	return true;
}

//**********************************************************************************
//
//**********************************************************************************

bool CedaSdgData::AddSdgInternal(SDGDATA *prpSdg)
{
	omData.Add(prpSdg);
	omUrnoMap.SetAt((void *)prpSdg->Urno,prpSdg);
	ogDdx.DataChanged((void *)this,SDG_NEW,(void *)prpSdg); //Update Viewer    
	return true;
}

//**********************************************************************************
// Update data methods (called from PrePlanTable class)
//**********************************************************************************

bool CedaSdgData::DeleteSdg(SDGDATA *prpSdgData, BOOL bpWithSave)
{

	bool olRc = true;
	ogDdx.DataChanged((void *)this,SDG_DELETE,(void *)prpSdgData);
	if(prpSdgData->IsChanged == DATA_UNCHANGED)
	{
		prpSdgData->IsChanged = DATA_DELETED;
	}
	if(bpWithSave == TRUE)
	{
		CWaitCursor olDummy;
		olRc = (bool) SaveSdg(prpSdgData);
	}
	return olRc;
}

//**********************************************************************************
//
//**********************************************************************************

bool CedaSdgData::InsertSdg(SDGDATA *prpSdgData, BOOL bpWithSave)
{
	bool olRc = true;

	SDGDATA *prlSdgData = new SDGDATA;

	*prlSdgData = *prpSdgData;

	AddSdgInternal(prlSdgData);

	prlSdgData->IsChanged = DATA_NEW;

	if(bpWithSave == TRUE)
	{
		CWaitCursor olDummy;
		SaveSdg(prlSdgData);
	}

	return olRc;
}

//**********************************************************************************
//
//**********************************************************************************

bool CedaSdgData::UpdateSdg(SDGDATA *prpSdgData, BOOL bpWithSave)
{
	bool olRc = true;

	ogDdx.DataChanged((void *)this,SDG_CHANGE,(void *)prpSdgData);

	if(prpSdgData->IsChanged == DATA_UNCHANGED)
	{
		prpSdgData->IsChanged = DATA_CHANGED;
	}
	if(bpWithSave == TRUE)
	{
		CWaitCursor olDummy;
		SaveSdg(prpSdgData);
	}
	return olRc;
}

//**********************************************************************************
//
//**********************************************************************************

bool CedaSdgData::SdtExist(long Urno)
{
	// don't read from database anymore, just check internal array
	SDGDATA *prlData;
	bool olRc = true;

	if (omUrnoMap.Lookup((void *)Urno,(void *&)prlData)  == FALSE)
	{
		olRc = false;
	}
	return olRc;
}


//**********************************************************************************
//
//**********************************************************************************

void  CedaSdgData::ProcessSdgBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	// Datensatz-Urno
	long llUrno;
	// Daten und Info
	struct BcStruct *prlBcStruct = NULL;
	prlBcStruct = (struct BcStruct *) vpDataPointer;
	SDGDATA *prlSdg = NULL;

CCS_TRY

	switch(ipDDXType)
	{
	case BC_SDG_NEW: // neuen Datensatz einf�gen
		{
			// einzuf�gender Datensatz
			prlSdg = new SDGDATA;
			GetRecordFromItemList(prlSdg,prlBcStruct->Fields,prlBcStruct->Data);
			AddSdgInternal(prlSdg);
		}
		break;
	case BC_SDG_CHANGE:	// Datensatz �ndern
		{
			// Datenatz-Urno ermittlen
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			prlSdg = GetSdgByUrno(llUrno);
			if(prlSdg != NULL)
			{
				bool blOldSdgWasOperative;
				bool blNewSdgIsOperative;

				blOldSdgWasOperative = IsSdgOparative(prlSdg);

				// ja -> Datensatz aktualisieren
				GetRecordFromItemList(prlSdg,prlBcStruct->Fields,prlBcStruct->Data);
				UpdateSdg(prlSdg);

				blNewSdgIsOperative = IsSdgOparative(prlSdg);
				
				// evtl. SDTs nachladen
				if (blOldSdgWasOperative || blNewSdgIsOperative)
				{
					CheckReloadSDTs();
				}
			}
			// nein -> Datensatz interessiert uns nicht
		}
		break;
	case BC_SDG_DELETE:	// Datensatz l�schen
		{
			// Datenatz-Urno ermittlen
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			prlSdg = GetSdgByUrno(llUrno);
			if (prlSdg != NULL)
			{
				// ja -> Datensatz l�schen
				DeleteSdgInternal(prlSdg);
			}

			// auch die zugeh�rigen MSDs und SDTs l�schen
			ogMsdData.DeleteBySdgu (llUrno);
			ogSdtData.DeleteSdtBySdgu (llUrno);
		}
		break;
	case BC_RELSDG:
		{
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			llUrno = atol(prlBcStruct->Data);
			prlSdg = GetSdgByUrno(llUrno);
			if (prlSdg != NULL)
			{
				if (!ogSdtData.ReloadSdtBySdgu (prlSdg->Urno))
				{
					CString olMsg;
					olMsg.Format("Problems occurred while processing BC:\nCommand:%s\nObject:%s\nFields:%s\nData:%s\nSelection:%s\nTws:%s\nTwe:%s",
						prlBcStruct->Cmd,
						prlBcStruct->Object,
						prlBcStruct->Fields,
						prlBcStruct->Data,
						prlBcStruct->Selection,
						prlBcStruct->Tws,
						prlBcStruct->Twe);

					::MessageBox(NULL, olMsg.GetBuffer(0), "CedaSdgData::ProcessSdgBc()", MB_OK);
				}
			}
		}
		break;
	default:
		break;
	}
	return;
CCS_CATCH_ALL
	CString olMsg;
	olMsg.Format("Problems occurred while processing BC:\nCommand:%s\nObject:%s\nFields:%s\nData:%s\nSelection:%s\nTws:%s\nTwe:%s",
		prlBcStruct->Cmd,
		prlBcStruct->Object,
		prlBcStruct->Fields,
		prlBcStruct->Data,
		prlBcStruct->Selection,
		prlBcStruct->Tws,
		prlBcStruct->Twe);

	::MessageBox(NULL, olMsg.GetBuffer(0), "CedaSdgData::ProcessSdgBc()", MB_OK);

}


//**********************************************************************************
//
//**********************************************************************************

SDGDATA * CedaSdgData::GetSdgByUrno(long pcpUrno)
{
	SDGDATA *prlSdg;

	if (omUrnoMap.Lookup((void*)pcpUrno,(void *& )prlSdg) == TRUE)
	{
		return prlSdg;
	}
	return NULL;
}

//**********************************************************************************
//
//**********************************************************************************

bool CedaSdgData::DeleteSdgInternal(SDGDATA *prpSdg)
{
	prpSdg->IsChanged = DATA_DELETED;
	ogDdx.DataChanged((void *)this,SDG_DELETE,(void *)prpSdg); //Update Viewer

	omUrnoMap.RemoveKey((void *)prpSdg->Urno);

	int ilSdgCount = omData.GetSize();
	for (int ilCountRecord = 0; ilCountRecord < ilSdgCount; ilCountRecord++)
	{
		if (omData[ilCountRecord].Urno == prpSdg->Urno)
		{
			omData.DeleteAt(ilCountRecord);
			break;
		}
	}
    return true;
}

//**********************************************************************************
//
//**********************************************************************************

bool CedaSdgData::SaveSdg(SDGDATA *prpSdg)
{
	bool olRc = true;
	CString olListOfData;
	char pclSelection[512];
	char pclData[2048];

	if (prpSdg->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpSdg->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpSdg);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpSdg->IsChanged = DATA_UNCHANGED;
		if(!omLastErrorMessage.IsEmpty())
		{
			::MessageBox(NULL, omLastErrorMessage.GetBuffer(0), "DB-Zugriffsfehler-Insert", MB_OK);
		}
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSdg->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpSdg);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpSdg->IsChanged = DATA_UNCHANGED;
		if(!omLastErrorMessage.IsEmpty())
		{
			::MessageBox(NULL, omLastErrorMessage.GetBuffer(0), "DB-Zugriffsfehler-Update", MB_OK);
		}
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpSdg->Urno);
		olRc = CedaAction("DRT",pclSelection);
		if(!omLastErrorMessage.IsEmpty())
		{
			::MessageBox(NULL, omLastErrorMessage.GetBuffer(0), "DB-Zugriffsfehler-Delete", MB_OK);
		}
		break;
	}

   return olRc;
}

void CedaSdgData::GetTimeFrames(long lpUrno, CCSPtrArray<TIMEFRAMEDATA> &opTimes)
{
	SDGDATA *prlSdg;
	CStringArray olDates;

	opTimes.DeleteAll();

	CTime olLastDate;
	CCSPtrArray<TIMEFRAMEDATA> olTimes;

	TIMEFRAMEDATA *prlTimeFrame;

	CTimeSpan olOneDay(1,0,0,0);
	CTimeSpan olSevenDay(7,0,0,0);

	prlSdg = GetSdgByUrno(lpUrno);

	if(prlSdg != NULL)
	{
		prlTimeFrame = new TIMEFRAMEDATA;

		prlTimeFrame->StartTime = CTime(prlSdg->Begi.GetYear(), prlSdg->Begi.GetMonth(), prlSdg->Begi.GetDay(), 12,0,0);
		olLastDate = prlTimeFrame->StartTime;		

		prlTimeFrame->EndTime = prlTimeFrame->StartTime;

		for(int j = 1; j < atoi(prlSdg->Days); j++)
		{
			prlTimeFrame->EndTime = prlTimeFrame->EndTime + olOneDay;
		}

		olTimes.Add(prlTimeFrame);


		for(j = 1; j < atoi(prlSdg->Dura); j++)
		{
			prlTimeFrame = new TIMEFRAMEDATA;

			for(int j = 0; j < atoi(prlSdg->Peri); j++)
			{
				olLastDate += olSevenDay;
			}
			prlTimeFrame->StartTime = olLastDate;

			prlTimeFrame->EndTime = prlTimeFrame->StartTime;

			for(int k = 1; k < atoi(prlSdg->Days); k++)
			{
				prlTimeFrame->EndTime = prlTimeFrame->EndTime + olOneDay;
			}
			olTimes.Add(prlTimeFrame);
		}


		CTime olDate;
		CTimeSpan olSpan;
		ExtractItemList( CString(prlSdg->Repi), &olDates, ';');

		for(int i = 0; i < olDates.GetSize(); i++)
		{
			olDate = DateStringToDate(olDates[i]);
			
			olSpan = olDate - prlSdg->Begi;


			for(j = 0; j < olTimes.GetSize(); j++)
			{
				prlTimeFrame = new TIMEFRAMEDATA;

				prlTimeFrame->StartTime = olTimes[j].StartTime + olSpan;
				prlTimeFrame->EndTime   = olTimes[j].EndTime + olSpan;
				prlTimeFrame->StartTime = CTime(prlTimeFrame->StartTime.GetYear(), prlTimeFrame->StartTime.GetMonth(), prlTimeFrame->StartTime.GetDay(), 0,0,0);
				prlTimeFrame->EndTime   = CTime(prlTimeFrame->EndTime.GetYear(), prlTimeFrame->EndTime.GetMonth(), prlTimeFrame->EndTime.GetDay(), 23,59,59);

				opTimes.Add(prlTimeFrame);
			}
		}

		for( i = olTimes.GetSize() - 1; i >=0 ; i--)
		{
			prlTimeFrame = &olTimes[i];
			prlTimeFrame->StartTime = CTime(prlTimeFrame->StartTime.GetYear(), prlTimeFrame->StartTime.GetMonth(), prlTimeFrame->StartTime.GetDay(), 0,0,0);
			prlTimeFrame->EndTime   = CTime(prlTimeFrame->EndTime.GetYear(), prlTimeFrame->EndTime.GetMonth(), prlTimeFrame->EndTime.GetDay(), 23,59,59);
			opTimes.InsertAt(0, prlTimeFrame);
		}
		olTimes.RemoveAll();
	}
}

void CedaSdgData::Register()
{
	// bereits angemeldet?
	if (bmIsBCRegistered) return;	// ja -> gibt nichts zu tun, terminieren

	// Drr-Datensatz hat sich ge�ndert
	ogDdx.Register((void *)this,BC_SDG_CHANGE, CString("CedaSdgData"), CString("BC_SDG_CHANGE"),        ProcessSdgCf);
	ogDdx.Register((void *)this,BC_SDG_NEW, CString("CedaSdgData"), CString("BC_SDG_NEW"),		   ProcessSdgCf);
	ogDdx.Register((void *)this,BC_SDG_DELETE, CString("CedaSdgData"), CString("BC_SDG_DELETE"),        ProcessSdgCf);
	ogDdx.Register((void *)this,BC_RELSDG, CString("CedaSdgData"), CString("BC_RELSDG"),        ProcessSdgCf);

	// jetzt ist die Instanz angemeldet
	bmIsBCRegistered = true;
}

DutyRoster_View *CedaSdgData::GetViewInfo()
{
	DutyRoster_View *pView = NULL;

	CWinApp *pApp = AfxGetApp();
	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL && pView == NULL)
	{
		CDocTemplate *pDocTempl = pApp->GetNextDocTemplate(pos);
		POSITION p1 = pDocTempl->GetFirstDocPosition();
		while(p1 != NULL && pView == NULL)
		{
			CDocument *pDoc = pDocTempl->GetNextDoc(p1);
			POSITION p2 = pDoc->GetFirstViewPosition();
			while (p2 != NULL && pView == NULL)
			{
				pView = DYNAMIC_DOWNCAST(DutyRoster_View,pDoc->GetNextView(p2));
								
			}
		}
	}

	return pView;
}

bool CedaSdgData::IsSdgOparative(SDGDATA *prpSdg)
{
	bool blRet = false;
	
	if (strcmp (&prpSdg->Expd[1], "O") == NULL || strlen(prpSdg->Expd) == 1)
	{
		blRet = true;
	}

	return blRet;
}

void CedaSdgData::CheckReloadSDTs()
{
	DutyRoster_View *polParent = this->GetViewInfo();
	if (polParent)
	{
		polParent->LoadSdtData();
		polParent->pomDebitTabViewer->ChangeView();
	}
}