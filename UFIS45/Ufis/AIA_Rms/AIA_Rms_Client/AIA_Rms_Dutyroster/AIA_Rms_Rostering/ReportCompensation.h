// ReportCompensation.h: interface for the ReportCompensation class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_REPORTCOMPENSATION_H__02F037D9_2134_4B23_BF2C_ED13F887184B__INCLUDED_)
#define AFX_REPORTCOMPENSATION_H__02F037D9_2134_4B23_BF2C_ED13F887184B__INCLUDED_

#include <stdafx.h>
#include <fstream>

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class ReportCompensation  
{

public:
	ReportCompensation();
	ReportCompensation(DutyRoster_View &ropDutyrosterView);
	BOOL GeneratePrintFile(const CString& ropFileName);
	virtual ~ReportCompensation();
private:
	VIEWINFO* pomView;
	DutyRoster_View* pomDutyrosterView;
	int imNightHoursStart;
	int imNightHoursEnd;

	void GenerateCommon(std::ofstream& of);
	void GenerateData(std::ofstream& of);
	double GetNightWorkingHours(COleDateTime *popFrom,COleDateTime *popTo);
	double GetSundayHolidayWorkingHours(COleDateTime *popFrom,COleDateTime *popTo);
	bool UseShift(DRRDATA *popDrrData);

};

#endif // !defined(AFX_REPORTCOMPENSATION_H__02F037D9_2134_4B23_BF2C_ED13F887184B__INCLUDED_)
