// cedadrddata.cpp - Klasse f�r die Handhabung von DRD-Daten (Daily Roster Deviation - 
//  t�gliche Abweichungen von Stammdaten)
//

#include <stdafx.h>


//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Globale Funktionen
//************************************************************************************************************************************************
//************************************************************************************************************************************************

//************************************************************************************************************************************************
// ProcessDrdCf: Callback-Funktion f�r den Broadcast-Handler und die Broadcasts
//	BC_DRD_CHANGE, BC_DRD_NEW und BC_DRD_DELETE. Ruft zur eingentlichen Bearbeitung
//	der Nachrichten die Funktion CedaDrdData::ProcessDrdBc() der entsprechenden 
//	Instanz von CedaDrdData auf.	
// R�ckgabe: keine
//************************************************************************************************************************************************

void  ProcessDrdCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	// Empf�nger ermitteln
	CedaDrdData *polDrdData = (CedaDrdData *)vpInstance;
	// Bearbeitungsfunktion des Empf�ngers aufrufen
	polDrdData->ProcessDrdBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Implementation der Klasse CedaDrdData
//************************************************************************************************************************************************
//************************************************************************************************************************************************

//************************************************************************************************************************************************
// Konstruktor
//************************************************************************************************************************************************

CedaDrdData::CedaDrdData(CString opTableName, CString opExtName) : CedaData(&ogCommHandler)
{
    // Infostruktur vom Typ CEDARECINFO f�r DRD-Datens�tze
	// Wird von Vaterklasse CCSCedaData f�r verschiedene Funktionen
	// (CCSCedaData::GetBufferRecord(), CCSCedaData::MakeCedaData())
	// benutzt. Die Datenstruktur beschreibt die Tabellenstruktur.
    BEGIN_CEDARECINFO(DRDDATA, DrdDataRecInfo)
		FIELD_CHAR_TRIM	(Dpt1,"DPT1")	// Organisationseinheit (Code aus ORG)
		FIELD_OLEDATE	(Drdf,"DRDF")	// Abweichung von
		FIELD_OLEDATE	(Drdt,"DRDT")	// Abweichung bis
		FIELD_CHAR_TRIM	(Drrn,"DRRN")	// Schichtnummer (1-n pro Tag)
		FIELD_CHAR_TRIM	(Fctc,"FCTC")	// Funktion des Mitarbeiters (Code aus PFC)
		FIELD_CHAR_TRIM	(Prmc,"PRMC")	// Qualifikationen (max. 15 Codes aus PER, getrennt mit '|')
		FIELD_CHAR_TRIM	(Sday,"SDAY")	// Schichttag
		FIELD_LONG		(Stfu,"STFU")	// MA-Urno
		FIELD_LONG		(Urno,"URNO")	// DS-Urno
		END_CEDARECINFO
		
		// Infostruktur kopieren
		for (int i = 0; i < sizeof(DrdDataRecInfo)/sizeof(DrdDataRecInfo[0]); i++)
		{
			CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
			memcpy(prpCedaRecInfo,&DrdDataRecInfo[i],sizeof(CEDARECINFO));
			omRecInfo.Add(prpCedaRecInfo);
		}
		
		// Tabellenname per Default auf DRD setzen
		opExtName = pcgTableExt;
		SetTableNameAndExt(opTableName+opExtName);
		
		// Feldnamen initialisieren
		strcpy(pcmListOfFields,	"DPT1,DRDF,DRDT,DRRN,FCTC,PRMC,SDAY,STFU,URNO");
		// Zeiger der Vaterklasse auf Tabellenstruktur setzen
		CCSCedaData::pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer
		
		// Datenarrays initialisieren
		omUrnoMap.InitHashTable(256);
		
		// ToDo: min. / max. Datum f�r DRDs initialisieren
		//	omMinDay = omMaxDay = TIMENULL;
}

//************************************************************************************************************************************************
// Destruktor
//************************************************************************************************************************************************

CedaDrdData::~CedaDrdData()
{
	ClearAll(true);
	omRecInfo.DeleteAll();
}

//************************************************************************************************************************************************
// SetTableNameAndExt: stellt den Namen und die Extension ein.
// R�ckgabe:	false	->	Tabellenname mit Extension gr��er als
//							Klassenmember
//				true	->	alles OK
//************************************************************************************************************************************************

bool CedaDrdData::SetTableNameAndExt(CString opTableAndExtName)
{
	CCS_TRY;
	if(opTableAndExtName.GetLength() >= sizeof(pcmTableName)) return false;
	strcpy(pcmTableName,opTableAndExtName.GetBuffer(0));
	return true;
	CCS_CATCH_ALL;
	return false;
}

//************************************************************************************************************************************************
// Register: beim Broadcasthandler anmelden. Um Broadcasts (Meldungen vom 
//	Datenbankhandler) zu empfangen, muss diese Funktion ausgef�hrt werden.
//	Der letzte Parameter ist ein Zeiger auf eine Callback-Funktion, welche
//	dann die entsprechende Nachricht bearbeitet. �ber den nach void 
//	konvertierten Zeiger auf dieses Objekt ((void *)this) kann das 
//	Objekt ermittelt werden, das der eigentliche Empf�nger der Nachricht ist
//	und an das die Callback-Funktion die Nachricht weiterleitet (per Aufruf einer
//	entsprechenden Member-Funktion).
// R�ckgabe: keine
//************************************************************************************************************************************************

void CedaDrdData::Register(void)
{
	CCS_TRY;
	// bereits angemeldet?
	if (bmIsBCRegistered) return;	// ja -> gibt nichts zu tun, terminieren
	
	// DRD-Datensatz hat sich ge�ndert
	DdxRegister((void *)this,BC_DRD_CHANGE, "CedaDrdData", "BC_DRD_CHANGE",        ProcessDrdCf);
	DdxRegister((void *)this,BC_DRD_NEW,    "CedaDrdData", "BC_DRD_NEW",		   ProcessDrdCf);
	DdxRegister((void *)this,BC_DRD_DELETE, "CedaDrdData", "BC_DRD_DELETE",        ProcessDrdCf);
	// jetzt ist die Instanz angemeldet
	bmIsBCRegistered = true;
	CCS_CATCH_ALL;
}


//************************************************************************************************************************************************
// ClearAll: aufr�umen. Alle Arrays und PointerMaps werden geleert.
// R�ckgabe:	immer true
//************************************************************************************************************************************************

bool CedaDrdData::ClearAll(bool bpUnregister)
{
	CCS_TRY;
    // alle Arrays leeren
	omUrnoMap.RemoveAll();
	omData.DeleteAll();
	
	// beim BC-Handler abmelden
	if(bpUnregister && bmIsBCRegistered)
	{
		ogDdx.UnRegister(this,NOTUSED);
		// jetzt ist die Instanz angemeldet
		bmIsBCRegistered = false;
	}
    return true;
	CCS_CATCH_ALL;
	return false;
}

//************************************************************************************************************************************************
// ReadDrdByUrno: liest den DRD-Datensatz mit der  Urno <lpUrno> aus der 
//	Datenbank.
// R�ckgabe:	boolean Datensatz erfolgreich gelesen?
//************************************************************************************************************************************************

bool CedaDrdData::ReadDrdByUrno(long lpUrno, DRDDATA *prpDrd)
{
	CCS_TRY;
	CString olWhere;
	olWhere.Format("%ld", lpUrno);
	// Datensatz aus Datenbank lesen
	
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, olWhere);
		WriteInRosteringLog();
	}
	
	if (CedaAction2("RT", (char*)(LPCTSTR)olWhere) == false)
	{
		return false;	// Fehler -> abbrechen
	}
	// Datensatz-Objekt instanziieren...
	DRDDATA *prlDrd = new DRDDATA;
	// und initialisieren
	if (GetBufferRecord2(0,prpDrd) == true)
	{  
		return true;	// Aktion erfolgreich
	}
	else
	{
		// Fehler -> aufr�umen und terminieren
		delete prlDrd;
		return false;
	}
	ClearFastSocketBuffer();	
	return false;
	CCS_CATCH_ALL;
	return false;
}

//************************************************************************************************************************************************
// ReadFilteredByDateAndStaff: liest Datens�tze ein, deren Feld SDAY (g�ltig am)
//	innerhalb des Zeitraums <opStart> bis <opEnd> liegen. Die Parameter werden auf 
//	G�ltigkeit gepr�ft. Wenn <popLoadStfUrnoMap> nicht NULL ist, werden nur die 
//	Datens�tze geladen, deren Feld STFU (Mitarbeiter-Urno) im Array enthalten ist.
// R�ckgabe:	true	->	Aktion erfolgreich
//				false	->	Fehler beim Einlesen der Datens�tze
//************************************************************************************************************************************************

bool CedaDrdData::ReadFilteredByDateAndStaff(COleDateTime opStart, COleDateTime opEnd,
											 CMapPtrToPtr *popLoadStfUrnoMap /*= NULL*/,
											 bool bpRegisterBC /*= true*/,
											 bool bpClearData /*= true*/)
{
	CCS_TRY;
	// G�ltigkeit der Datumsangaben pr�fen
	if ((opStart.GetStatus() != COleDateTime::valid) ||
		(opEnd.GetStatus() != COleDateTime::valid) || 
		(opStart >= opEnd))
	{
		// Fehler -> terminieren
		return false;
	}
	
	// Puffer f�r WHERE-Statement
	CString olWhere;
	
	// Anzahl der Urnos
	if (popLoadStfUrnoMap != NULL && popLoadStfUrnoMap->GetCount() == 0)
	{
		// es gibt nichts zu tun, kein Fehler
		return true;	
	}
	
	// keine Urno-Liste oder Urno-Liste �bergeben aber Anzahl gr��er als max. in WHERE-Clause erlaubte MA-Urnos?
	if ((popLoadStfUrnoMap == NULL) || (popLoadStfUrnoMap->GetCount() > MAX_WHERE_STAFFURNO_IN))
	{
		// mehr Urnos als in WHERE-Clause passen -> alles laden und lokal filtern
		olWhere.Format("WHERE SDAY BETWEEN '%s' AND '%s'",opStart.Format("%Y%m%d"), opEnd.Format("%Y%m%d"),opStart.Format("%Y%m%d"), opEnd.Format("%Y%m%d"));
		// Datens�tze einlesen und Ergebnis zur�ckgeben
		return Read(olWhere.GetBuffer(0),popLoadStfUrnoMap,bpRegisterBC,bpClearData);
	}
	else
	{
		// MA-Urnos mit in WHERE-Clause aufnehmen
		POSITION pos;			// zum Iterieren
		void *plDummy;			// das Objekt, immer NULL und daher obsolet
		long llStfUrno;			// die Urno als long
		CString	olBuf;			// um die Urno in einen String zu konvertieren
		CString olStfUrnoList;	// die Urno-Liste
		// alle Urnos ermitteln
		for(pos = popLoadStfUrnoMap->GetStartPosition(); pos != NULL;)   
		{
			// n�chste Urno
			popLoadStfUrnoMap->GetNextAssoc(pos,(void*&)llStfUrno,plDummy);
			// umwandeln in String
			olBuf.Format("%ld",llStfUrno);
			// ab in die Liste
			olStfUrnoList += ",'" + olBuf + "'";
		}
		// f�hrendes Komma aus Urno-Liste entfernen, WHERE-Statement generieren
		olWhere.Format("WHERE STFU IN (%s) AND SDAY BETWEEN '%s' AND '%s'",olStfUrnoList.Mid(1),opStart.Format("%Y%m%d"), opEnd.Format("%Y%m%d"),opStart.Format("%Y%m%d"), opEnd.Format("%Y%m%d"));
		// Datens�tze einlesen und Ergebnis zur�ckgeben
		return Read(olWhere.GetBuffer(0),NULL,bpRegisterBC,bpClearData);
	}
	CCS_CATCH_ALL;
	return false;
}

//************************************************************************************************************************************************
// Read: liest Datens�tze ein.
// R�ckgabe:	true	->	Aktion erfolgreich
//				false	->	Fehler beim Einlesen der Datens�tze
//************************************************************************************************************************************************

bool CedaDrdData::Read(char *pspWhere /*=NULL*/, CMapPtrToPtr *popLoadStfUrnoMap /*= NULL*/,
					   bool bpRegisterBC /*= true*/, bool bpClearData /*= true*/)
{
	CCS_TRY;
	// Return-Code f�r Funktionsaufrufe
	//bool ilRc = true;
	
	// wenn gew�nscht, aufr�umen (und beim Broadcast-Handler abmelden, wenn
	// nach Ende angemeldet werden soll)
	if (bpClearData) ClearAll(bpRegisterBC);
	
    // Datens�tze lesen
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}
	
	if(pspWhere == NULL)
	{
		if (!CedaAction2("RT", "")) return false;
	}
	else
	{
		if (!CedaAction2("RT", pspWhere)) return false;
	}
	
    bool blMoreRecords = false;
	// Datensatzz�hler f�r TRACE
	int ilCountRecord = 0;
	// void-Pointer f�r MA-Urno-Lookup
	void  *prlVoid = NULL;
	// solange es Datens�tze gibt
	do
	{
		// neuer Datensatz
		DRDDATA *prlDrd = new DRDDATA;
		// Datensatz lesen
		blMoreRecords = GetBufferRecord2(ilCountRecord,prlDrd);
		// Datensatz gelesen, Z�hler inkrementieren
		ilCountRecord++;
		// wurde eine Datensatz gelesen und ist (wenn vorhanden) die
		// Mitarbeiter-Urno dieses Datensatzes im MA-Urno-Array enthalten? 
		if(blMoreRecords && 
			((popLoadStfUrnoMap == NULL) || 
			(popLoadStfUrnoMap->Lookup((void *)prlDrd->Stfu, (void *&)prlVoid) == TRUE)))
		{ 
			// Datensatz hinzuf�gen
			if (!InsertInternal(prlDrd, DRD_NO_SEND_DDX))
			{
				// Fehler -> lokalen Datensatz l�schen
				if(IsTraceLoggingEnabled())
				{
					GetDefectDataString(ogRosteringLogText, (void*)prlDrd);
					WriteInRosteringLog(LOGFILE_TRACE);
				}
				delete prlDrd;
			}
#ifdef TRACE_FULL_DATA
			else
			{
				// Datensatz OK, loggen if FULL
				ogRosteringLogText.Format(" read %8.8lu %s OK:  ", ilCountRecord, pcmTableName);
				GetDataFormatted(ogRosteringLogText, prlDrd);
				WriteLogFull("");
			}
#endif TRACE_FULL_DATA
		}
		else
		{
			// kein weiterer Datensatz
			delete prlDrd;
		}
	} while (blMoreRecords);
	
	ClearFastSocketBuffer();	
	// beim Broadcast-Handler anmelden, wenn gew�nscht
	if (bpRegisterBC) Register();
	
	// Test: Anzahl der gelesenen DRD
	TRACE("Read-Drd: %d gelesen\n",ilCountRecord);
	
	ClearFastSocketBuffer();	
	
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  read %d records from table %s",ilCountRecord, pcmTableName);
		WriteInRosteringLog();
	}
    return true;
	CCS_CATCH_ALL;
	return false;
}

//*******************************************************************************************************************
// ReadSpecialData: liest Datens�tze mit Hilfe eines �bergebenen SQL-Strings ein.
// R�ckgabe:	bool Erfolg?
//*******************************************************************************************************************

bool CedaDrdData::ReadSpecialData(CCSPtrArray<DRDDATA> *popDrdArray, char *pspWhere,
								  char *pspFieldList, bool ipSYS/*=true*/)
{
	CCS_TRY;
	bool ilRc = true;
	char pclFieldList[256] = " ";
	
	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}
	
	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction2("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	int ilCountRecord = 0;
	if(popDrdArray != NULL)	// Datens�tze in den �bergebenen Array
	{
		for (ilCountRecord = 0; ilRc == true; ilCountRecord++)
		{
			DRDDATA *prpDrd = new DRDDATA;
			if ((ilRc = GetBufferRecord2(ilCountRecord,prpDrd,CString(pclFieldList))) == true)
			{
				popDrdArray->Add(prpDrd);
			}
			else
			{
				delete prpDrd;
			}
		}
		if(popDrdArray->GetSize() == 0) return false;
	}
	
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  read %d records from table %s",ilCountRecord-1, pcmTableName);
		WriteInRosteringLog();
	}
	ClearFastSocketBuffer();	
	
	return true;
	CCS_CATCH_ALL;
	return false;
}

//*********************************************************************************************
// Insert: speichert den Datensatz <prpDrd> in der Datenbank und schickt einen
//	Broadcast ab.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrdData::Insert(DRDDATA *prpDrd, bool bpSave /*= true*/)
{
	CCS_TRY;
	// �nderungs-Flag setzen
	prpDrd->IsChanged = DATA_NEW;
	// in Datenbank speichern, wenn erw�nscht
	if(bpSave && Save(prpDrd) == false) return false;
	// Broadcast DRD_NEW abschicken
	InsertInternal(prpDrd,true);
    return true;
	CCS_CATCH_ALL;
	return false;
}

//*********************************************************************************************
// InsertInternal: f�gt den internen Arrays einen Datensatz hinzu. Der Datensatz
//	muss sp�ter oder vorher explizit durch Aufruf von Save() oder Release() in der Datenbank 
//	gespeichert werden. Wenn <bpSendDdx> true ist, wird ein Broadcast gesendet.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrdData::InsertInternal(DRDDATA *prpDrd, bool bpSendDdx)
{
	CCS_TRY;
	
	DRDDATA *prlData;
	// �bergebener Datensatz NULL oder Datensatz mit diesem Schl�ssel
	// schon vorhanden?
	if(omUrnoMap.Lookup((void *)prpDrd->Urno,(void *&)prlData)  == TRUE)
	{
		return false;
	}
	
	// Datensatz intern anf�gen
	omData.Add(prpDrd);
	// Datensatz der Urno-Map hinzuf�gen
	omUrnoMap.SetAt((void *)prpDrd->Urno,prpDrd);
	
	// Broadcast senden?
	if( bpSendDdx == true )
	{
		// ja -> go!
		ogDdx.DataChanged((void *)this,DRD_NEW,(void *)prpDrd);
	}
	return true;
	CCS_CATCH_ALL;
	return false;
}

//*********************************************************************************************
// Update: sucht den Datensatz mit der Urno <prpDrd->Urno> und speichert ihn
//	in der Datenbank.
// R�ckgabe:	false	->	Datensatz wurde nicht gefunden
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrdData::Update(DRDDATA *prpDrd, bool bpSave /*= true*/)
{
	CCS_TRY;
	// Datensatz raussuchen
	if (GetDrdByUrno(prpDrd->Urno) != NULL)
	{
		// �nderungsflag setzen
		if (prpDrd->IsChanged == DATA_UNCHANGED)
		{
			prpDrd->IsChanged = DATA_CHANGED;
		}
		// in die Datenbank speichern
		if(bpSave && Save(prpDrd) == false) return false; 
		// Broadcast DRD_CHANGE versenden
		UpdateInternal(prpDrd);
	}
    return true;
	CCS_CATCH_ALL;
	return false;
}

//*********************************************************************************************
// UpdateInternal: �ndert einen Datensatz, der in der internen Datenhaltung
//	enthalten ist.
// R�ckgabe:	false	->	Datensatz wurde nicht gefunden
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrdData::UpdateInternal(DRDDATA *prpDrd, bool bpSendDdx /*true*/)
{
	CCS_TRY;
	// Broadcast senden, wenn gew�nscht
	if( bpSendDdx == true )
	{
		ogDdx.DataChanged((void *)this,DRD_CHANGE,(void *)prpDrd);
	}
	return true;
	CCS_CATCH_ALL;
	return false;
}

//*********************************************************************************************
// DeleteInternal: l�scht einen Datensatz aus den internen Arrays.
// R�ckgabe:	keine
//*********************************************************************************************

void CedaDrdData::DeleteInternal(DRDDATA *prpDrd, bool bpSendDdx /*true*/)
{
	CCS_TRY;
	// Broadcast senden BEVOR der Datensatz gel�scht wird
	if(bpSendDdx == true)
	{
		ogDdx.DataChanged((void *)this,DRD_DELETE,(void *)prpDrd);
	}
	
	// Urno-Schl�ssel l�schen
	omUrnoMap.RemoveKey((void *)prpDrd->Urno);
	
	// Datensatz l�schen
	int ilCount = omData.GetSize();
	for (int i = 0; i < ilCount; i++)
	{
		if (omData[i].Urno == prpDrd->Urno)
		{
			omData.DeleteAt(i);//Update omData
			break;
		}
	}
	
	CCS_CATCH_ALL;
}

//*********************************************************************************************
// RemoveInternalByStaffUrno: entfernt alle DRDs aus der internen Datenhaltung,
//	die dem Mitarbeiter <lpStfUrno> zugeordnet sind. Die Datens�tze werden NICHT 
//	aus der Datenbank gel�scht.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrdData::RemoveInternalByStaffUrno(long lpStfUrno, bool bpSendBC /*= false*/)
{
	CCS_TRY;
	DRDDATA *prlDrd;
	// alle DRDs durchgehen
	for(int i=0; i<omData.GetSize(); i++)
	{
		// DRD dieses MAs entfernen?
		if(lpStfUrno == omData[i].Stfu)
		{
			// ja
			if ((prlDrd =  GetDrdByUrno(omData[i].Urno)) == NULL) return false;
			// DRD entfernen
			DeleteInternal(prlDrd, bpSendBC);
			// neue Datensatz-Anzahl anpassen
			i--;
		}
	}
	return true;
	CCS_CATCH_ALL;
	return false;
}

//*********************************************************************************************
// Delete: markiert den Datensatz <prpDrd> als gel�scht und l�scht den Datensatz
//	aus der internen Datenhaltung und der Datenbank. 
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrdData::Delete(DRDDATA *prpDrd, bool bpWithSave /* true*/)
{
	CCS_TRY;
	// Flag setzen
	prpDrd->IsChanged = DATA_DELETED;
	
	// speichern
	if(bpWithSave == TRUE)
	{
		if (!Save(prpDrd)) return false;
	}
	
	// aus der internen Datenhaltung l�schen
	DeleteInternal(prpDrd,true);
	
	return true;
	CCS_CATCH_ALL;
	return false;
}

//*********************************************************************************************
// ProcessDrdBc: behandelt die einlaufenden Broadcasts.
// R�ckgabe:	keine
//*********************************************************************************************

void  CedaDrdData::ProcessDrdBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	CCS_TRY;
#ifdef _DEBUG	
	// Performance-Trace
	SYSTEMTIME rlPerf;
	GetSystemTime(&rlPerf);
	TRACE("CedaDrdData::ProcessDrdBc: START %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
#endif
	
	// Datensatz-Urno
	long llUrno;
	// Daten und Info
	struct BcStruct *prlBcStruct = NULL;
	prlBcStruct = (struct BcStruct *) vpDataPointer;
	DRDDATA *prlDrd = NULL;
	
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("ProcessBC:\n  Cmd <%s>\n  Tbl <%s>\n  Twe <%s>\n  Sel <%s>\n  Fld <%s>\n  Dat <%s>\n  Bcn <%s>",prlBcStruct->Cmd, prlBcStruct->Object, prlBcStruct->Twe, prlBcStruct->Selection, prlBcStruct->Fields, prlBcStruct->Data,prlBcStruct->BcNum);
		WriteInRosteringLog();
	}
	
	switch(ipDDXType)
	{
	case BC_DRD_CHANGE:	// Datensatz �ndern
		{
			// Datenatz-Urno ermittlen
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			prlDrd = GetDrdByUrno(llUrno);
			if(prlDrd != NULL)
			{
				// ja -> Datensatz aktualisieren
				GetRecordFromItemList(prlDrd,prlBcStruct->Fields,prlBcStruct->Data);
				UpdateInternal(prlDrd);
				break;
			}
			// nein -> gehe zum Insert, !!! weil es kann sein, dass diese �nderung den Mitarbeiter in View laden soll
		}
	case BC_DRD_NEW: // neuen Datensatz einf�gen
		{
			// einzuf�gender Datensatz
			prlDrd = new DRDDATA;
			GetRecordFromItemList(prlDrd,prlBcStruct->Fields,prlBcStruct->Data);
			InsertInternal(prlDrd, DRD_SEND_DDX);
		}
		break;
	case BC_DRD_DELETE:	// Datensatz l�schen
		{
			// Datenatz-Urno ermittlen
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			prlDrd = GetDrdByUrno(llUrno);
			if (prlDrd != NULL)
			{
				// ja -> Datensatz l�schen
				DeleteInternal(prlDrd);
			}
		}
		break;
	default:
		break;
	}
#ifdef _DEBUG
	// Performance-Test
	GetSystemTime(&rlPerf);
	TRACE("CedaDrdData::ProcessDrdBc: END %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
#endif
	CCS_CATCH_ALL;
}

//*********************************************************************************************
// GetDrdByUrno: sucht den Datensatz mit der Urno <lpUrno>.
// R�ckgabe:	ein Zeiger auf den Datensatz oder NULL, wenn der
//				Datensatz nicht gefunden wurde
//*********************************************************************************************

DRDDATA *CedaDrdData::GetDrdByUrno(long lpUrno)
{
	CCS_TRY;
	// der Datensatz
	DRDDATA *prpDrd;
	// Datensatz in Urno-Map suchen
	if (omUrnoMap.Lookup((void*)lpUrno,(void *& )prpDrd) == TRUE)
	{
		return prpDrd;	// gefunden -> Zeiger zur�ckgeben
	}
	// nicht gefunden -> R�ckgabe NULL
	return NULL;
	CCS_CATCH_ALL;
	return NULL;
}

//*********************************************************************************************
// GetDrdMapOfDrr: einen Arraymit den Zeigern auf die Datens�tze f�llen, die 
//	dem DRR <popDrr> zugeordnet sind.
// R�ckgabe:	die Anzahl der zugeordneten DRDs oder -1, wenn einer der 
//				Parameter ung�ltig ist
//*********************************************************************************************

int CedaDrdData::GetDrdMapOfDrr(CMapPtrToPtr *popDrdMap, DRRDATA *popDrr)
{
	CCS_TRY;
	// pr�fen, ob die Map g�ltig ist
	if ((popDrdMap == NULL) || (popDrr == NULL)) return -1;
	
	// Array leeren
	popDrdMap->RemoveAll();
	
	// alle internen Datens�tze untersuchen
	for(int ilCount=0; ilCount<omData.GetSize(); ilCount++)
	{
		//		TRACE("CedaDrdData::GetDrdMapOfDrr():\n");
		//		TRACE("\tDRR: %s *** %s *** %ld)\n",popDrr->Sday,popDrr->Drrn,popDrr->Stfu);
		//		TRACE("\tDRD: %s *** %s *** %ld)\n",omData[ilCount].Sday,omData[ilCount].Drrn,omData[ilCount].Stfu);
		// jeden DRR untersuchen
		if ((strcmp(omData[ilCount].Sday,popDrr->Sday) == 0) && 
			(omData[ilCount].Stfu == popDrr->Stfu) && 
			(strcmp(omData[ilCount].Drrn,popDrr->Drrn) == 0))
		{
			//			TRACE("\tDRD gefunden!!!\n");
			// Zeiger auf Datensatz in die Map
			popDrdMap->SetAt((void *)omData[ilCount].Urno,&omData[ilCount]);
		}
	}
	// Anzahl der Datens�tze zur�ck
	return popDrdMap->GetCount();
	CCS_CATCH_ALL;
	return -1;
}

//*********************************************************************************************
// DecreaseDrrn: sucht alle DRDs, die zum DRR <popDrr> geh�ren und verringert
//	die DRR-Nummer (DRRN) um eins. Achtung!!! Diese Funktion muss VOR dem �ndern
//	des zugeh�rigen DRR aufgerufen werden. Vor dem Aufruf dieser Funktion
//	m�ssen jedoch alle alten DRDs mit der neuen DRR-Nummer gel�scht worden sein.
// R�ckgabe:	> 0	->	Aktion erfolgreich, Anzahl der ge�nderten DRA
//				-1	->	Fehler
//*********************************************************************************************

int CedaDrdData::DecreaseDrrn(DRRDATA *popDrr)
{
	CCS_TRY;
	// neue DRR-Nummer generieren
	int ilDrrn = atoi(popDrr->Drrn) - 1;
	CString olDrrn;
	olDrrn.Format("%d",ilDrrn);
	// Z�hler als R�ckgabe
	int ilCountDrd = 0;
	// alle internen Datens�tze untersuchen
	for(int ilCount=0; ilCount<omData.GetSize(); ilCount++)
	{
		// jeden DRR untersuchen
		if ((strcmp(omData[ilCount].Sday,popDrr->Sday) == 0) && 
			(omData[ilCount].Stfu == popDrr->Stfu) && 
			(strcmp(omData[ilCount].Drrn,popDrr->Drrn) == 0))
		{
			TRACE("CedaDraData::DecreaseDrrn(): DRA gefunden!!!\n");
			// neue DRR-Nummer eintragen
			strcpy(omData[ilCount].Drrn,olDrrn.GetBuffer(0));
			// Datensatz speichern
			if (!Update(&omData[ilCount])) return -1;
			ilCountDrd++;
		}
	}
	return ilCountDrd;
	CCS_CATCH_ALL;
	return -1;
}

//*********************************************************************************************
// SwapDrrn: sucht alle DRDs, die zu den DRRs <popDrr1> und <popDrr2> geh�ren und 
//	vertauscht die DRR-Nummern.
// R�ckgabe:	true	->	Aktion erfolgreich, Anzahl der ge�nderten DRA
//				false	->	Fehler
//*********************************************************************************************

bool CedaDrdData::SwapDrrn(DRRDATA *popDrr1, DRRDATA *popDrr2)
{
	CCS_TRY;
	// alle internen Datens�tze untersuchen
	for(int ilCount=0; ilCount<omData.GetSize(); ilCount++)
	{
		// jeden DRR untersuchen: geh�rt dieser DRD zum DRR1?
		if ((strcmp(omData[ilCount].Sday,popDrr1->Sday) == 0) && 
			(omData[ilCount].Stfu == popDrr1->Stfu) && 
			(strcmp(omData[ilCount].Drrn,popDrr1->Drrn) == 0))
		{
			// ja -> DRR-Nummer �ndern
			strcpy(omData[ilCount].Drrn,popDrr2->Drrn);
			// Datensatz speichern
			if (!Update(&omData[ilCount])) return false;
		}
		else if ((strcmp(omData[ilCount].Sday,popDrr2->Sday) == 0) && 
			(omData[ilCount].Stfu == popDrr2->Stfu) && 
			(strcmp(omData[ilCount].Drrn,popDrr2->Drrn) == 0))
		{
			// ja -> DRR-Nummer �ndern
			strcpy(omData[ilCount].Drrn,popDrr1->Drrn);
			// Datensatz speichern
			if (!Update(&omData[ilCount])) return false;
		}
	}
	return true;
	CCS_CATCH_ALL;
	return false;
}

//*********************************************************************************************
// HasDrdOfDrr: pr�fen, ob dem DRR <popDrr> DRDs zugeordnet sind.
// R�ckgabe:	true	->	DRD gefunden
//				false	->	kein DRD gefunden
//*********************************************************************************************

bool CedaDrdData::HasDrdOfDrr(DRRDATA *popDrr)
{
	CCS_TRY;
	// pr�fen, ob der Zeiger g�ltig ist
	if (popDrr == NULL) return false;	// ung�ltig -> terminieren
	
	// alle internen Datens�tze untersuchen
	for(int ilCount=0; ilCount<omData.GetSize(); ilCount++)
	{
		// jeden DRR untersuchen
		if ((strcmp(omData[ilCount].Sday,popDrr->Sday) == 0) && 
			(omData[ilCount].Stfu == popDrr->Stfu) && 
			(strcmp(omData[ilCount].Drrn,popDrr->Drrn) == 0))
		{
			//			TRACE("CedaDrdData::HasDrdData(): DRD gefunden!!!\n");
			// R�ckgabe: DRD gefunden!
			return true;
		}
	}
	// R�ckgabe: keinen DRD gefunden!
	return false;
	CCS_CATCH_ALL;
	return false;
}

//*********************************************************************************************
// HasDrdOfDrr: pr�fen, ob dem DRR <popDrr> DRDs ohne DEL zugeordnet sind.
// R�ckgabe:	true	->	mindestens eine DRD ohne DEL gefunden
//				false	->	kein DRD gefunden bzw. alle DRD mit DEL
//*********************************************************************************************

bool CedaDrdData::HasDrdOfDrrWithoutDel(DRRDATA *popDrr)
{
	CCS_TRY;
		CString olAbfr, olAbto;

	// pr�fen, ob der Zeiger g�ltig ist
	if (popDrr == NULL) return false;	// ung�ltig -> terminieren
	
	// alle internen Datens�tze untersuchen
	for(int ilCount=0; ilCount<omData.GetSize(); ilCount++)
	{
		// jeden DRR untersuchen
		if ((strcmp(omData[ilCount].Sday,popDrr->Sday) == 0) && 
			(omData[ilCount].Stfu == popDrr->Stfu) && 
			(strcmp(omData[ilCount].Drrn,popDrr->Drrn) == 0))
		{
			olAbfr = omData[ilCount].Drdf.Format("%H%M");
			olAbto =  omData[ilCount].Drdt.Format("%H%M");
			TRACE("CedaDraData::HasDraData(): DRA gefunden!!!\n");
			if (!ogDelData.HasDel(popDrr->Bsdu, omData[ilCount].Dpt1,omData[ilCount].Fctc, olAbfr, olAbto))
				return true;
		}
	}
	// R�ckgabe: keinen DRD gefunden!
	return false;
	CCS_CATCH_ALL;
	return false;
}

//*********************************************************************************************
// IsValidDrd: pr�ft, ob es konkurrierende Abweichungen zu einer Abweichung gibt.
//	Ein DRD ist ung�ltig, wenn es entweder andere DRDs gibt, die demselben DRR
//	zugeordnet sind und sich zeitlich mit dem DRD <popDrd> �berschneiden. Der DRD
//	ist auch ung�ltig, wenn der Zeitraum seiner G�ltigkeit ausserhalb des
//	Zeitraums der zugrundeliegenden Schicht (<popDrr->Avfr> bis <popDrr->Avto>) liegt.
// R�ckgabe:	true	->	DRD ist OK
//				false	->	DRD ist nicht OK
//*********************************************************************************************

bool CedaDrdData::IsValidDrd(DRDDATA *popDrd, CMapPtrToPtr *popDrdMap, 
							 DRRDATA *popDrr /*= NULL*/)
{
	CCS_TRY;
	// g�ltiger DRR?
	if (popDrr != NULL)
	{
		// ja -> Zeitbereich pr�fen
		if ((popDrd->Drdf < popDrr->Avfr) || (popDrd->Drdf > popDrr->Avto) || 
			(popDrd->Drdt < popDrr->Avfr) || (popDrd->Drdt > popDrr->Avto))
		{
			// einer der Zeitwerte liegt ausserhalb des g�ltigen Bereichs ->
			// DRD ist ung�ltig, abbrechen
			return false;
		}
	}
	
	POSITION rlPos; 
	long llUrno; 
	DRDDATA *prlDrd; 
	// alle DRDs in <popDrdMap> pr�fen und die Zeitspanne mit der von <popDrd> pr�fen
	for(rlPos = popDrdMap->GetStartPosition(); rlPos != NULL; )
	{
		// DRD ermitteln
		popDrdMap->GetNextAssoc(rlPos,(void *&)llUrno, (void *&)prlDrd);
		// DRD nicht mit sich selbst vergleichen
		if (popDrd->Urno != prlDrd->Urno)
		{
			// liegt der Start oder das Ende der Abwesenheit innerhalb des Zeitraums des 
			// vorhandenen DRDs?
			if (((popDrd->Drdf >= prlDrd->Drdf) && (popDrd->Drdf <= prlDrd->Drdt)) || // Start liegt innerhalb
				((popDrd->Drdt >= prlDrd->Drdf) && (popDrd->Drdt <= prlDrd->Drdt)))	// Ende liegt innerhalb
			{
				// ja -> DRD ist ung�ltig
				return false;
			}
		}
	}
	// DRD ist g�ltig
	return true;
	CCS_CATCH_ALL;
	return false;
}

//*********************************************************************************************
// IsValidDrd: pr�ft, ob der �bergebene Zeitraum in Konflikt steht zu einem der 
//	Zeitr�ume der DRDs in <popDrdMap>.
// R�ckgabe:	true	->	Zeitraum ist OK
//				false	->	Zeitraum ist nicht OK
//*********************************************************************************************

bool CedaDrdData::IsValidDrd(COleDateTime opDateFrom, COleDateTime opDateTo, long lpUrno,
							 CMapPtrToPtr *popDrdMap, DRRDATA *popDrr /*= NULL*/)
{
	CCS_TRY;
	// g�ltiger DRR?
	if (popDrr != NULL)
	{
		// ja -> Zeitbereich pr�fen
		if ((opDateFrom < popDrr->Avfr) || (opDateFrom > popDrr->Avto) || 
			(opDateTo < popDrr->Avfr) || (opDateTo > popDrr->Avto))
		{
			// einer der Zeitwerte liegt ausserhalb des g�ltigen Bereichs ->
			// DRD ist ung�ltig, abbrechen
			return false;
		}
	}
	
	POSITION rlPos; 
	long llUrno; 
	DRDDATA *prlDrd; 
	// alle DRDs in <popDrdMap> pr�fen und die Zeitspanne mit der von <popDrd> pr�fen
	for(rlPos = popDrdMap->GetStartPosition(); rlPos != NULL; )
	{
		// DRD ermitteln
		popDrdMap->GetNextAssoc(rlPos,(void *&)llUrno, (void *&)prlDrd);
		// DRD nicht mit sich selbst vergleichen
		if (lpUrno != prlDrd->Urno)
		{
			// liegt der Start oder das Ende der Abwesenheit innerhalb des Zeitraums des 
			// vorhandenen DRDs?
			if (((opDateFrom >= prlDrd->Drdf) && (opDateFrom <= prlDrd->Drdt)) || // Start liegt innerhalb
				((opDateTo >= prlDrd->Drdf) && (opDateTo <= prlDrd->Drdt)))	// Ende liegt innerhalb
			{
				// ja -> DRD ist ung�ltig
				return false;
			}
		}	
	}
	// DRD ist g�ltig
	return true;
	CCS_CATCH_ALL;
	return false;
}

//*******************************************************************************
// Save: speichert den Datensatz <prpDrd>.
// R�ckgabe:	bool Erfolg?
//*******************************************************************************

bool CedaDrdData::Save(DRDDATA *prpDrd)
{
	CCS_TRY;
	bool olRc = true;
	CString olListOfData;
	char pclSelection[1024];
	char pclData[2048];
	
	if (prpDrd->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	
	switch(prpDrd->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpDrd);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpDrd->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpDrd->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpDrd);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpDrd->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpDrd->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpDrd);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("DRT",pclSelection,"",pclData);
		break;
	}
	// R�ckgabe: Ergebnis der CedaAction
	return olRc;
	CCS_CATCH_ALL;
	return false;
}

//*************************************************************************************
// CedaAction: ruft die Funktion CedaAction der Basisklasse CCSCedaData auf.
//	Alle Parameter werden durchgereicht. Aufgrund eines internen CEDA-Bugs,
//	der daf�r sorgen kann, dass die Tabellenbeschreibung <pcmListOfFields>
//	zerst�rt wird, muss sicherheitshalber <pcmListOfFields> vor jedem Aufruf
//	von CedaData::CedaAction() gerettet und danach wiederhergestellt werden.
// R�ckgabe:	der R�ckgabewert von CedaData::CedaAction()
//*************************************************************************************

bool CedaDrdData::CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
							 char *pcpSelection, char *pcpSort, char *pcpData, 
							 char *pcpDest /*= "BUF1"*/,bool bpIsWriteAction /*= false*/, 
							 long lpID /*= 0*/, CCSPtrArray<RecordSet> *pomData /*= NULL*/, 
							 int ipFieldCount /*= 0*/)
{
	CCS_TRY;
	// R�ckgabepuffer

	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CedaData::CedaAction(pcpAction,pcpSelection,pcpSort,pcpData,pcpDest,bpIsWriteAction,lpID);
	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	ClearFastSocketBuffer();	
	return blRet;
	CCS_CATCH_ALL;
	return false;
}

//*************************************************************************************
// CedaAction: kurze Version, Beschreibung siehe oben.
// R�ckgabe:	der R�ckgabewert von CedaData::CedaAction()
//*************************************************************************************

bool CedaDrdData::CedaAction(char *pcpAction, char *pcpSelection, char *pcpSort,
							 char *pcpData, char *pcpDest /*"BUF1"*/, 
							 bool bpIsWriteAction/* false */, long lpID /* = imBaseID*/)
{
	CCS_TRY;
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CedaData::CedaAction(pcpAction,CCSCedaData::pcmTableName, CCSCedaData::pcmFieldList,(pcpSelection != NULL)? pcpSelection: CCSCedaData::pcmSelection,(pcpSort != NULL)? pcpSort: CCSCedaData::pcmSort,pcpData,pcpDest,bpIsWriteAction, lpID);
	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
	CCS_CATCH_ALL;
	return false;
}

//************************************************************************************************************************
//************************************************************************************************************************
// die folgenden Funktionen sind Hilfsfunktionen zum Manipulieren von 
// Objekten des Typs DRDDATA. Die Funktionen wurden als Member der Klasse 
// CedaDrdData angelegt, um den Zugriff und die Manipulation von DRD-Datens�tzen
// zu zentralisieren und in einer Klasse zu kapseln.
//************************************************************************************************************************
//************************************************************************************************************************

//************************************************************************************************************************************************
// CreateDrdData: initialisiert eine Struktur vom Typ DRDDATA.
// R�ckgabe:	ein Zeiger auf den ge�nderten / erzeugten Datensatz oder
//				NULL, wenn ein Fehler auftrat.
//************************************************************************************************************************************************

DRDDATA* CedaDrdData::CreateDrdData(long lpStfUrno, CString opDay, CString opDrrn)
{
	CCS_TRY;
	// Objekt erzeugen
	DRDDATA *prlDrdData = new DRDDATA;
	// �nderungsflag setzen
	prlDrdData->IsChanged = DATA_NEW;
	// Schichttag einstellen
	strcpy(prlDrdData->Sday,opDay.GetBuffer(0));
	// Mitarbeiter-Urno
	prlDrdData->Stfu = lpStfUrno;
	// n�chste freie Datensatz-Urno ermitteln und speichern
	prlDrdData->Urno = ogBasicData.GetNextUrno();
	// DRRN setzen (Nummer des DRD bei mehreren Schichten an einem Tag)
	strcpy(prlDrdData->Drrn,opDrrn.GetBuffer(0));
	// Zeiger auf Datensatz zur�ck
	return prlDrdData;
	CCS_CATCH_ALL;
	return NULL;
}


/************************************************************************************************************************************************
vergleicht Drd-Zeiten mit Del-Zeiten, bei Kollision return false
************************************************************************************************************************************************/
bool CedaDrdData::CompareDrdToDelList(DRDDATA *popDrd, CCSPtrArray<DELDATA>& olDelList)
{
	CCS_TRY;

	COleDateTimeSpan olTimeSpan;
	COleDateTime olTimeDelf,olTimeDelt;
	for(int ilCount=0; ilCount<olDelList.GetSize(); ilCount++)
	{
		DELDATA* polDel = (DELDATA*)&olDelList[ilCount];
		// Datum pr�fen
		// Zeit von
		CedaDataHelper::DateStringToOleDateTime(popDrd->Sday,olTimeDelf);
		if(olTimeDelf.GetStatus() != COleDateTime::valid)
			continue;

		olTimeDelt = olTimeDelf;	// nur der Tag, keine HHMM
		CedaDataHelper::HourMinStringToOleDateTimeSpan(CString(polDel->Delf), olTimeSpan);
		if(olTimeSpan.GetStatus() != COleDateTimeSpan::valid)
			continue;

		olTimeDelf += olTimeSpan;

		// Zeit bis
		CedaDataHelper::HourMinStringToOleDateTimeSpan(CString(polDel->Delt), olTimeSpan);
		if(olTimeSpan.GetStatus() != COleDateTimeSpan::valid)
			continue;

		olTimeDelt += olTimeSpan;

		if(	olTimeDelf < popDrd->Drdf && olTimeDelt <= popDrd->Drdf ||
			olTimeDelf >= popDrd->Drdt && olTimeDelt > popDrd->Drdt)
			continue;
		else 
			return false;
	}
	return true;

	CCS_CATCH_ALL;
	return false;
}

//************************************************************************************************************************************************
// CompareDrdToDrd: zwei DRDs miteinander vergleichen. Wenn <bpCompareKey> gesetzt ist,
//	werden auch die Felder, die den Schl�ssel ergeben verglichen. 
// R�ckgabe:	false	->	die Werte der Felder beider DRDs sind gleich
//				true	->	die Werte der Felder beider DRDs sind unterschiedlich
//************************************************************************************************************************************************

bool CedaDrdData::CompareDrdToDrd(DRDDATA *popDrd1, DRDDATA *popDrd2,bool bpCompareKey /*= false*/)
{
	CCS_TRY;
	// Schl�sselfelder vergleichen, wenn gew�nscht
	if (bpCompareKey && ((popDrd1->Stfu != popDrd2->Stfu) || 
		(strcmp(popDrd1->Sday,popDrd2->Sday) != 0) || (strcmp(popDrd1->Drrn,popDrd2->Drrn) != 0))) 
	{
		return true;
	}
	// andere Felder vergleichen
	if ((strcmp(popDrd1->Dpt1,popDrd2->Dpt1) != 0) || 
		(popDrd1->Drdf != popDrd2->Drdf) ||
		(popDrd1->Drdt != popDrd2->Drdt) ||
		(strcmp(popDrd1->Fctc,popDrd2->Fctc) != 0) 
		)
	{
		return true;
	}
	// zum Schluss die Qualifikationslisten vergleichen (weil's am aufwendigsten ist)
	return ComparePrmcList(popDrd1->Prmc,popDrd2->Prmc);
	CCS_CATCH_ALL;
	return false;
}

//************************************************************************************************************************************************
// ComparePrmcList: vergleicht zwei Listen mit Qualifikationscodes.
// R�ckgabe:	false	->	die Listen weichen nicht ab
//				true	->	die Listen sind unterschiedlich
//************************************************************************************************************************************************

bool CedaDrdData::ComparePrmcList(CString opList1, CString opList2)
{
	CCS_TRY;
	// Arrays f�r die Extraktion
	CStringArray olStringArray1, olStringArray2;
	// Anzahlen der Items
	int ilCount1, ilCount2;
	// Listen extrahieren
	ilCount1 = ExtractItemList(opList1,&olStringArray1,'|');
	ilCount2 = ExtractItemList(opList2,&olStringArray2,'|');
	// Schalter f�r Suchabbruch
	bool blItemFound = false;
	// wenn unterschiedlich viele Items in den Listen sind...
	if (ilCount1 != ilCount2)	return true;	// ...sind die Listen unterschiedlich
	// sonst den Inhalt vergleichen
	for (int ilLoop1 = 0; ilLoop1 < ilCount1; ilLoop1++)
	{
		// Schalter zur�cksetzen
		blItemFound = false;
		// Element aus Liste 1 in Liste 2 suchen
		for (int ilLoop2 = 0; (ilLoop2 < ilCount2) && !blItemFound; ilLoop2++)
		{
			if (olStringArray1[ilLoop1] == olStringArray2[ilLoop2]) 
			{
				// Item gefunden -> Suche abbrechen
				blItemFound = true;
				// Element aus Liste 2 l�schen
				olStringArray2.RemoveAt(ilLoop2);
				// Gr�sse der Liste 2 verkleinern
				ilCount2--;
			}
		}
		// beim ersten nicht gefundenen Element abbrechen
		if (!blItemFound) return true;
	}
	// alle Elemente wurden gefunden, die Listen sind gleich
	return false;
	CCS_CATCH_ALL;
	return false;
}

/************************************************************************************************************************************************
CompareDrdToBasicData: pr�ft, ob die Angaben im DRD tats�chlich von den Werten der
Mitarbeiterstammdaten abweichen.
R�ckgabe:	false	->	die Werte weichen nicht ab
true	->	die Werte sind unterschiedlich
bool bpCheckMainFuncOnly - true - Schichtzuordnung nur f�r Stammfunktionen pr�fen, sonst f�r alle
************************************************************************************************************************************************/

bool CedaDrdData::CompareDrdToBasicData(DRDDATA *popDrd, bool bpCheckMainFuncOnly)
{
	CCS_TRY;
	// Tag, der untersucht wird
	COleDateTime olDate;
	CedaDataHelper::DateStringToOleDateTime(popDrd->Sday,olDate);
	
	// Abweichung von Funktion laut Stammdaten pr�fen
	if (!ogSpfData.ComparePfcBySurnWithTime(CString(popDrd->Fctc),popDrd->Stfu,olDate,bpCheckMainFuncOnly))
	{
		// Funktion weicht ab
		return true;
	}
	/*
	// Abweichung von Arbeitsgruppe laut Stammdaten pr�fen
	if (ogSwgData.GetWgpBySurnWithTime(popDrd->Stfu,olDate) != CString(popDrd->Wgpc))
	{
	// Arbeitsgruppe weicht ab
	return true;
}*/
	// Abweichung von Organisationseinheit laut Stammdaten pr�fen
	if (ogSorData.GetOrgBySurnWithTime(popDrd->Stfu,olDate) != CString(popDrd->Dpt1))
	{
		// Organisationseinheit weicht ab
		return true;
	}
	// keine Abweichung gefunden
	return false;
	CCS_CATCH_ALL;
	return false;
}

//************************************************************************************************************************************************
// CopyDrdValues: Kopiert die "Nutzdaten" (keine urnos usw.)
// R�ckgabe:	keine
//************************************************************************************************************************************************

void CedaDrdData::CopyDrd(DRDDATA* popDrdDataSource,DRDDATA* popDrdDataTarget)
{
	CCS_TRY;
	// Daten kopieren
	strcpy(popDrdDataTarget->Dpt1,popDrdDataSource->Dpt1);
	strcpy(popDrdDataTarget->Fctc,popDrdDataSource->Fctc);
	strcpy(popDrdDataTarget->Prmc,popDrdDataSource->Prmc);
	//strcpy(popDrdDataTarget->Wgpc,popDrdDataSource->Wgpc);
	popDrdDataTarget->Drdf = popDrdDataSource->Drdf;
	popDrdDataTarget->Drdt = popDrdDataSource->Drdt;
	CCS_CATCH_ALL;
}

//************************************************************************************************************************************************
// 
//************************************************************************************************************************************************

