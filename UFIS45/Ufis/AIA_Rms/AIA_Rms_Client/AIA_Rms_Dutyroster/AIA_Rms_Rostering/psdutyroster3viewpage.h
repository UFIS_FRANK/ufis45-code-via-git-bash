#ifndef AFX_PSDUTYROSTER3VIEWPAGE_H__DF496402_6D7D_11D2_8054_004095434A85__INCLUDED_
#define AFX_PSDUTYROSTER3VIEWPAGE_H__DF496402_6D7D_11D2_8054_004095434A85__INCLUDED_

// PSDutyRoster3ViewPage.h : Header-Datei
//
#include <stdafx.h>
#include <Ansicht.h>
#include <RosterViewPage.h>
#include <DutyRoster_View.h>
//#include "CCSEdit.h"

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld PSDutyRoster3ViewPage 

class PSDutyRoster3ViewPage : public RosterViewPage
{
	DECLARE_DYNCREATE(PSDutyRoster3ViewPage)

// Konstruktion
public:
	PSDutyRoster3ViewPage();
	~PSDutyRoster3ViewPage();

// Dialogfelddaten
	//{{AFX_DATA(PSDutyRoster3ViewPage)
	enum { IDD = IDD_PSDUTYROSTER3 };
	CButton	m_C_Extra;
	CButton	m_C_Grupp;
	CButton	m_C_Detail;
	CButton	m_R_Diff;
	CButton	m_R_IstSoll;
	CButton	m_C_Additiv;
	CButton	m_C_AllCodes;
	CButton	m_R_Plan;
	CButton	m_R_Langzeit;
	CButton	m_R_Dienstplan;
	CButton	m_R_Alle_Stufen;
	CListBox	m_LB_Functions;
	CListBox	m_LB_Organisations;
	//}}AFX_DATA

	BOOL GetData();
	BOOL GetData(CStringArray &opValues);
	void SetData();
	void SetData(CStringArray &opValues);

// Überschreibungen
	// Der Klassen-Assistent generiert virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(PSDutyRoster3ViewPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:
	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(PSDutyRoster3ViewPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnAllFuncs();
	afx_msg void OnNoFuncs();
	afx_msg void OnAllOrgs();
	afx_msg void OnNoOrgs();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	CString omCalledFrom;
	bool bmPgpLoaded;
	void SetCalledFrom(CString opCalledFrom);
private:
	CString omTitleStrg;
	void SetButtonText();
	void SetStatic();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_PSDUTYROSTER3VIEWPAGE_H__DF496402_6D7D_11D2_8054_004095434A85__INCLUDED_
