#if !defined(AFX_EXCELEXPORTDLG_H__619A3F02_A534_4B03_A3F8_B5E922127A0B__INCLUDED_)
#define AFX_EXCELEXPORTDLG_H__619A3F02_A534_4B03_A3F8_B5E922127A0B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ExcelExportDlg.h : header file
//
#include <DutyRoster_view.h>
/////////////////////////////////////////////////////////////////////////////
// ExcelExportDlg dialog
class DutyRoster_View;
class ExcelExportDlg : public CDialog
{
// Construction
public:
	ExcelExportDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(ExcelExportDlg)
	enum { IDD = IDD_EXCEL_EXPORT };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ExcelExportDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ExcelExportDlg)
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void HideRadios();
	DutyRoster_View* pomParent;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EXCELEXPORTDLG_H__619A3F02_A534_4B03_A3F8_B5E922127A0B__INCLUDED_)
