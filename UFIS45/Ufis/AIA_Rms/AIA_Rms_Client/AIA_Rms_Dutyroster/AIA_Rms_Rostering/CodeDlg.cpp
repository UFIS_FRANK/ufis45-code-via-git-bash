// CCodeDlg.cpp : implementation file
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

///////////////////////////////////////////////////////////////////////////// 

CCodeDlg::CCodeDlg( UINT nIDTemplate,  CWnd* pParent )
			: CDialog(nIDTemplate, pParent)
{
	m_IDD = nIDTemplate;
	pomBsdGrid = NULL;
	pomOdaGrid = NULL;
	pomWisGrid = NULL;

	if(m_IDD == IDD_CODE_DLG)
	{
		// Grid ist sauber !
		bmIsDirty = false;
	}
	else
	{
		// Grid ist nicht sauber !		???????????????
		bmIsDirty = true;			
	}

	bmAllCodes = false;

	// W�nsche unbedingt laden:
	bmShowWish = true;

	// Von/Bis Werte auf null setzen.
	omFromTime.SetStatus(COleDateTime::null);
	omToTime.SetStatus(COleDateTime::null);

	// Nicht-modaler Dialog mu� per CDialog::Create() erzeugt werden
	CDialog::Create(m_IDD, pParent);
		
	//{{AFX_DATA_INIT(CCodeDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

//************************************************************************************
//
//************************************************************************************

CCodeDlg::~CCodeDlg()
{
	if (pomBsdGrid != NULL)
		delete pomBsdGrid;

	if (pomOdaGrid != NULL)
		delete pomOdaGrid;

	if (pomWisGrid != NULL)
		delete pomWisGrid;
}

//************************************************************************************
// �bergabe des g�ltigen Organisations Codes
//************************************************************************************

void CCodeDlg::SetOrgCodes(CString opOrgList) 
{
	if (omOrgList != opOrgList)
	{
		omOrgList = opOrgList;
		// opOrgList = rmViewInfo.oPfcFctcs, alle FCTSs sind mit ,'...', getrennt
		// Grid mu� neu gezeichnet
		bmIsDirty = true;
	}
}

//************************************************************************************
// �bergabe des g�ltigen Funktions Codes
//************************************************************************************

void CCodeDlg::SetPfcCodes(CString opPfcList)
{
	if (omPfcList != opPfcList)
	{
		omPfcList = opPfcList;
		// Grid mu� neu gezeichnet
		bmIsDirty = true;
	}
}

//************************************************************************************
// �bergabe des G�ltigkeitsbereiches f�r Schichten
//************************************************************************************

void CCodeDlg::ChangeValidDates(COleDateTime opFrom,COleDateTime opTo)
{
	// Pr�fen, ob eine Ver�nderung vorliegt.
	if (omFromTime == opFrom && omToTime == opTo)
		return;

	// Grid mu� neu gezeichnet
	bmIsDirty = true;
	
	omFromTime = opFrom;
	omToTime = opTo;
}

//************************************************************************************
// Grid neu laden
//************************************************************************************

void CCodeDlg::ReloadAndShow(bool bpAllCodes)
{
	if (bmIsDirty || bpAllCodes != bmAllCodes)
	{
		// Daten vorbereiten
		PrepareBsdCodes(bpAllCodes);
		//-----------------------------------------------------------------------------------
		// Grid l�schen
		// Readonly ausschalten
		pomBsdGrid->GetParam()->SetLockReadOnly(false);

		// Anzahl der Rows ermitteln
		int ilRowCount = pomBsdGrid->GetRowCount();
		if (ilRowCount > 0)
			// Grid l�schen			
			pomBsdGrid->RemoveRows(1,ilRowCount);
		//-----------------------------------------------------------------------------------
		// Grid f�llen
		// Sollte die Liste der anzuzeigenen OrgCodes leer sein, mu� nix angezeigt werden
		//if (!omOrgList.IsEmpty()) 
		FillBsdGrid(bpAllCodes);
	
		bmIsDirty = false;
		bmAllCodes = bpAllCodes;
	}

	FillOdaGrid();
	FillWishGrid();

}

//************************************************************************************
//
//************************************************************************************

void CCodeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCodeDlg)
	//}}AFX_DATA_MAP
}

//************************************************************************************
//
//************************************************************************************

BEGIN_MESSAGE_MAP(CCodeDlg, CDialog)
	//{{AFX_MSG_MAP(CCodeDlg)
	ON_MESSAGE(WM_GRID_DRAGBEGIN, OnGridDragBegin)
	ON_BN_CLICKED(IDC_BUTTONFINDBSD, On_B_FindBSD)
	ON_BN_CLICKED(IDC_BUTTONFINDODA, On_B_FindODA)
	ON_BN_CLICKED(IDC_BUTTONFINDWIS, On_B_FindWIS)
	ON_WM_TIMER()
	ON_WM_NCLBUTTONDOWN()
	ON_WM_NCPAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

//************************************************************************************
//
//************************************************************************************

void CCodeDlg::OnOK() 
{
	// Grid beenden
	/*
	pomBsdGrid->DestroyWindow();
	pomOdaGrid->DestroyWindow();
	pomWisGrid->DestroyWindow();
	
	CDialog::OnOK();*/
}


//****************************************************************************************************
// OnCancel: macht nichts. Wird �berschrieben, um unerw�nschtes Verhalten 
//  zu unterdr�cken.
//  R�ckgabe:	keine
//****************************************************************************************************

void CCodeDlg::OnCancel() 
{
	//do nothing
}

//************************************************************************************
//
//************************************************************************************

BOOL CCodeDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// Grid initialisieren
	IniGrid();
	
	// Titel setzen
	SetWindowText(LoadStg(IDS_STRING397));

	// Close "x" in der Titelleiste l�schen

	CMenu* pmenuSystem = this->GetSystemMenu(FALSE);
	if (pmenuSystem != NULL)
	{
		pmenuSystem->DeleteMenu(SC_CLOSE, MF_BYCOMMAND);
	}

	// Fenster anzeigen
	//ShowWindow(SW_SHOWNORMAL);

	// Darstellung eines Buttons in der Titelleiste, Time f�r die aktualisierung anstossen.
	SetTimer(TIMER_REFRESH, 180, NULL);

	return (true);  
}

//************************************************************************************
// Ver�ndert den Fenstertitel
//************************************************************************************

void CCodeDlg::SetCaptionInfo(CString opInfo)
{ 
	// Titel setzen
	SetWindowText(LoadStg(IDS_STRING397) + opInfo);
}

//**********************************************************************************
// Grid initialisieren
//**********************************************************************************

void CCodeDlg::IniGrid ()
{
CCS_TRY;

	//-----------------------------------------------------------------------------
	// BSD Grid erzeugen
	pomBsdGrid = new CGridControl ( this,IDC_BSDGRID,COLCOUNT_CODEDLG,0);
	// Header setzen
	pomBsdGrid->SetValueRange(CGXRange(0,1), LoadStg(IDS_STRING1619));
	//-----------------------------------------------------------------------------
	// ODA Grid erzeugen
	pomOdaGrid = new CGridControl ( this,IDC_ODAGRID,COLCOUNT_CODEDLG,ogOdaData.omData.GetSize());
	// Header setzen
	pomOdaGrid->SetValueRange(CGXRange(0,1), LoadStg(IDS_STRING1620));
	//-----------------------------------------------------------------------------
	// Wis Grid erzeugen
	pomWisGrid = new CGridControl ( this,IDC_WISGRID,COLCOUNT_CODEDLG,ogWisData.omData.GetSize());
	// Header setzen
	pomWisGrid->SetValueRange(CGXRange(0,1), LoadStg(IDS_STRING325));
	//-----------------------------------------------------------------------------

	// Breite der Spalten einstellen
	IniColWidths ();
	// Grid mit Daten f�llen.
	FillWishGrid();

	pomBsdGrid->Initialize();
	
	// Scrollbar immer anzeigen
	pomBsdGrid->SetScrollBarMode(SB_VERT,gxnEnabled);
	
	pomBsdGrid->GetParam()->EnableUndo(FALSE);
	/*pomBsdGrid->LockUpdate(TRUE);
	pomBsdGrid->LockUpdate(FALSE);*/
	pomBsdGrid->GetParam()->EnableUndo(TRUE);

	// Verhindert, das die Spaltenbreite ver�ndert wird
	pomBsdGrid->GetParam()->EnableTrackColWidth(FALSE);
	// Es k�nnen nur ganze Zeilen markiert werden.
	//pomBsdGrid->GetParam()->EnableSelection(GX_SELROW);
	pomBsdGrid->GetParam()->EnableSelection(GX_SELNONE);
	
	pomBsdGrid->SetStyleRange( CGXRange().SetTable(), CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				.SetReadOnly(TRUE)
				//.SetEnabled(FALSE)
				.SetControl(GX_IDS_CTRL_STATIC)
				);
	pomBsdGrid->EnableAutoGrow (true);
	pomBsdGrid->EnableSorting (true);
	// DoppelClick Acktion festlegen
	pomBsdGrid->SetLbDblClickAction ( WM_COMMAND, IDC_BUTTONFINDBSD);
	// DnD aktivieren
	pomBsdGrid->SetEnableDnD(true);

	// Tooltips aktivieren
	pomBsdGrid->EnableGridToolTips();
	CGXStylesMap *olStylesmap1 = pomBsdGrid->GetParam()->GetStylesMap();
	olStylesmap1->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));

	//---------------------------------------------------------------------------------	
	pomOdaGrid->Initialize();
	pomOdaGrid->GetParam()->EnableUndo(FALSE);
	/*pomOdaGrid->LockUpdate(TRUE);
	pomOdaGrid->LockUpdate(FALSE);*/
	pomOdaGrid->GetParam()->EnableUndo(TRUE);
	
	// Scrollbar immer anzeigen
	pomOdaGrid->SetScrollBarMode(SB_VERT,gxnEnabled);
	pomOdaGrid->EnableSorting (true);

	// Verhindert, das die Spaltenbreite ver�ndert wird
	pomOdaGrid->GetParam()->EnableTrackColWidth(FALSE);
	// Es k�nnen nur ganze Zeilen markiert werden.
	//pomBsdGrid->GetParam()->EnableSelection(GX_SELROW);
	pomOdaGrid->GetParam()->EnableSelection(GX_SELNONE);
	
	pomOdaGrid->SetStyleRange( CGXRange().SetTable(), CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				.SetReadOnly(TRUE)
				//.SetEnabled(FALSE)
				.SetControl(GX_IDS_CTRL_STATIC)
				);
	pomOdaGrid->EnableAutoGrow ( FALSE );
	// DoppelClick Acktion festlegen
	pomOdaGrid->SetLbDblClickAction ( WM_COMMAND, IDC_BUTTONFINDODA);
	// DnD aktivieren
	pomOdaGrid->SetEnableDnD(true);
	// Tooltips aktivieren
	pomOdaGrid->EnableGridToolTips();
	CGXStylesMap *olStylesmap2 = pomOdaGrid->GetParam()->GetStylesMap();
	olStylesmap2->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));
		
	//-----------------------------------------------------------------------------
	pomWisGrid->Initialize();
	pomWisGrid->GetParam()->EnableUndo(FALSE);
	/*pomWisGrid->LockUpdate(TRUE);
	pomWisGrid->LockUpdate(FALSE);*/
	pomWisGrid->GetParam()->EnableUndo(TRUE);

	// Scrollbar immer anzeigen
	pomWisGrid->SetScrollBarMode(SB_VERT,gxnEnabled);
	pomWisGrid->EnableSorting (true);

	// Verhindert, das die Spaltenbreite ver�ndert wird
	pomWisGrid->GetParam()->EnableTrackColWidth(FALSE);
	// Es k�nnen nur ganze Zeilen markiert werden.
	//pomBsdGrid->GetParam()->EnableSelection(GX_SELROW);
	pomWisGrid->GetParam()->EnableSelection(GX_SELNONE);
	
	pomWisGrid->SetStyleRange( CGXRange().SetTable(), CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				.SetReadOnly(TRUE)
				//.SetEnabled(FALSE)
				.SetControl(GX_IDS_CTRL_STATIC)
				);
	pomWisGrid->EnableAutoGrow ( FALSE );
	// DoppelClick Acktion festlegen
	pomWisGrid->SetLbDblClickAction ( WM_COMMAND, IDC_BUTTONFINDWIS);
	// DnD aktivieren
	pomWisGrid->SetEnableDnD(true);
	// Tooltips aktivieren
	pomWisGrid->EnableGridToolTips();
	CGXStylesMap *olStylesmap3 = pomWisGrid->GetParam()->GetStylesMap();
	olStylesmap3->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));

CCS_CATCH_ALL;
}

//**********************************************************************************
// Breite der Columns setzen
//**********************************************************************************

void CCodeDlg::IniColWidths()
{
	pomBsdGrid->SetColWidth ( 0, 0, 0);
	pomBsdGrid->SetColWidth ( 1, 1, 52);
	pomBsdGrid->SetColWidth ( 2, 2, 0);

	pomOdaGrid->SetColWidth ( 0, 0, 0);
	pomOdaGrid->SetColWidth ( 1, 1, 52);
	pomOdaGrid->SetColWidth ( 2, 2, 0);

	pomWisGrid->SetColWidth ( 0, 0, 0);
	pomWisGrid->SetColWidth ( 1, 1, 52);
	pomWisGrid->SetColWidth ( 2, 2, 0);
}

//**********************************************************************************
// Grid mit Daten f�llen
//**********************************************************************************

void CCodeDlg::FillWishGrid(bool bpAllCodes)
{
CCS_TRY;
	// W�nsche
	if (bmShowWish)
	{
		pomWisGrid->GetParam()->SetLockReadOnly(false);
		int ilRowCount = pomWisGrid->GetRowCount();
		if (ilRowCount > 0)
			pomWisGrid->RemoveRows(1,ilRowCount);

		//-------------------------------------------------------------------------------
		// W�nsche
		CCSPtrArray<WISDATA> olWisData;
		int ilWisSize = ogWisData.GetWisArrayByOrgList(omOrgList, &olWisData, bpAllCodes);

		WISDATA* prlWis;
		for (int ilCount=0; ilCount<ilWisSize; ilCount++ )
		{
			prlWis = (WISDATA*)olWisData.CPtrArray::GetAt(ilCount);

			ilRowCount = pomWisGrid->GetRowCount ();
			pomWisGrid->InsertRows ( ilRowCount+1,1);

			// Urno mit erstem Feld koppeln.						
			pomWisGrid->SetStyleRange (CGXRange(ilCount+1,1), CGXStyle().SetItemDataPtr((void*)prlWis->Urno));

			// Datenfelder f�llen
			pomWisGrid->SetValueRange(CGXRange(ilCount+1,1),	prlWis->Wisc);
			pomWisGrid->SetValueRange(CGXRange(ilCount+1,2),	prlWis->Wisd);
		}

		// Grid sortieren
		SortGrid(pomWisGrid,1);
		pomWisGrid->GetParam()->SetLockReadOnly(true);
	}
	// Extra Tooltip anschalten
	pomWisGrid->SetEnableExtraToolTip(true);

CCS_CATCH_ALL;
}

//**********************************************************************************
// Sortierung des Grids "per Hand"
//**********************************************************************************

bool CCodeDlg::SortGrid(CGridControl* popGrid,int ipRow)
{
	CCS_TRY;
	CGXSortInfoArray  sortInfo;
	sortInfo.SetSize( 1 );
	sortInfo[0].sortOrder = CGXSortInfo::ascending;
	sortInfo[0].nRC = ipRow;                       
	sortInfo[0].sortType = CGXSortInfo::autodetect;  
	popGrid->SortRows( CGXRange().SetRows(1, popGrid->GetRowCount()), sortInfo); 
	
	return (true);
	CCS_CATCH_ALL;
	return false;
}

//**********************************************************************************
// Left Button Click in das Grid
//**********************************************************************************

LONG CCodeDlg::OnGridDragBegin(WPARAM wParam, LPARAM lParam)
{
	GRIDNOTIFY* rlNotify = (GRIDNOTIFY*) lParam;

	CString olUrno;
	olUrno.Format("%ld",(long)rlNotify->value2);

	if (rlNotify->source == pomBsdGrid)
	  	OnDragBegin(olUrno,rlNotify->value1,"BSD");

	if (rlNotify->source == pomOdaGrid)
	  	OnDragBegin(olUrno,rlNotify->value1,"ODA");

	if (rlNotify->source == pomWisGrid)
	  	OnDragBegin(olUrno,rlNotify->value1,"WIS");


	return 0L;
}

//****************************************************************************************************
// OnDragBegin(): Messagehandler f�r Drag and Drop-Mechanismus
//  R�ckgabe: long -> immer 0L
//****************************************************************************************************

void CCodeDlg::OnDragBegin(CString opUrno,CString opCode, CString opSource)
{
CCS_TRY;
	// Daten vorhanden?
	// ACHTUNG: Ziel k�nnen beide Views sein !
	// D'n'D-Objekt initialisieren
	omDragDropObject.CreateDWordData(DIT_DUTYROSTER_BSD_TABLE, 3);
	//omDragDropObject.CreateDWordData(DIT_SHIFTROSTER_BSD_TABLE, 1);
	// Basisschichten-Datenobjekt hinzuf�gen
	omDragDropObject.AddString(opCode);
	// "Absenderkennung" mitgeben
	omDragDropObject.AddString(opSource);
	// Urno mitgeben
	omDragDropObject.AddString(opUrno);
	// Aktion fortsetzen
	omDragDropObject.BeginDrag();
CCS_CATCH_ALL;
}

//****************************************************************************************************
// Suchen in den Grids, hier BSD Grid
//****************************************************************************************************

void CCodeDlg::On_B_FindBSD()
{
	if (pomBsdGrid)
		pomBsdGrid->OnShowFindReplaceDialog(TRUE);
}

//****************************************************************************************************
// Suchen in den Grids, hier ODA Grid
//****************************************************************************************************

void CCodeDlg::On_B_FindODA()
{
	if (pomOdaGrid)
		pomOdaGrid->OnShowFindReplaceDialog(TRUE);
}

//****************************************************************************************************
// Suchen in den Grids, hier WIS Grid
//****************************************************************************************************

void CCodeDlg::On_B_FindWIS()
{
	if (pomWisGrid)
		pomWisGrid->OnShowFindReplaceDialog(TRUE);
}


/****************************************************************************************************
Pr�ft ob die Schicht im vorgegebenen Zeitraum ist und ob sie angzeigt werden soll
bool bpAllCodes - true - alle Codes anzeigen, unabh�ngig vom Pfc/Org
****************************************************************************************************/

bool CCodeDlg::CheckBsd(BSDDATA* popBsd,COleDateTime opShiftFromTime,COleDateTime opShiftToTime, bool bpAllCodes)
{
	bool blTimeCheckFromOk = false;
	bool blTimeCheckToOk = false;

	COleDateTime olNullTime;
	CString olCodeWithSeparator;

	if(!bpAllCodes)
	{
		// Pr�fung ob diese Schicht in der Liste der Anzuzeigenen Schichten enthalten ist.
		if(!omPfcList.IsEmpty())
		{
			if(!strlen(popBsd->Fctc))
			{
				if (omPfcList.Find("NO_PFCCODE") == -1)
				{
					//				TRACE("NO_PFCCODE\n");
					return false;
				}
			}
			else
			{
				olCodeWithSeparator.Format("'%s'",LPCTSTR(popBsd->Fctc));
				if (omPfcList.Find(olCodeWithSeparator) == -1)
				{
					//				TRACE("PFCCODE %s NOT FOUND\n",olCodeWithSeparator);
					return false;
				}
			}
		}
		
		// Pr�fung ob diese Organisation in der Liste der Anzuzeigenen Organisationen enthalten ist.
		if (!omOrgList.IsEmpty())
		{
			if(!strlen(popBsd->Dpt1))
			{
				if (omOrgList.Find("NO_ORGCODE") == -1)
				{
					//				TRACE("NO_ORGCODE\n");
					return false;
				}
			}
			else
			{
				olCodeWithSeparator.Format("'%s'",LPCTSTR(popBsd->Dpt1));
				if (omOrgList.Find(olCodeWithSeparator) == -1)
				{
					//				TRACE("ORGCODE %s NOT FOUND\n",olCodeWithSeparator);
					return false;
				}
			}
		}
		else
		{
			return false;		// leere OrgList & !bpAllCodes - keine Funktionen anzeigen
		}
	}
	// TODO: Sollte die Liste leer sein nix anzeigen lassen. ATM wird die Liste nicht ber�cksichtig, sollte 
	// sie leer sein.
//	else
		// -> Liste leer, 
//		return false;

	// Nur Schichten, die im G�ltigkeisbereich liegen zulassen
	//----------------------------------------------------------------------------------
	// Ist ein Vorgabe Wert vorhanden f�r diese Schicht ?
	// Ist der Wert der Schicht Null ? Dann keine Pr�fung.
	if (opShiftToTime.GetStatus() != COleDateTime::valid || omFromTime.GetStatus() != COleDateTime::valid  
		|| opShiftToTime == olNullTime || omFromTime == olNullTime)  
	{
		// -> keine Pr�fung vornehmen.
		blTimeCheckFromOk = true;
	}
	else
	{
		// Pr�fung vornehmen
		// Schichtg�ltigkeit Ende mu� gr��er als Anfang Ladedatum sein
		if (opShiftToTime >= omFromTime)
			blTimeCheckFromOk = true;
		else
			blTimeCheckFromOk = false;
			
	}
	// Nur Schichten, die im G�ltigkeisbereich liegen zulassen
	//----------------------------------------------------------------------------------
	// Endwert pr�fen
	// Ist ein vorgabe Wert vorhanden f�r diese Schicht ?
	// Ist der Wert der Schicht Null ? Dann keine Pr�fung.
	if (opShiftFromTime.GetStatus() != COleDateTime::valid || omToTime.GetStatus() != COleDateTime::valid 
		|| opShiftFromTime == olNullTime || omToTime == olNullTime)
	{
		// -> keine Pr�fung vornehmen.
		blTimeCheckToOk = true;
	}
	else
	{
		// Pr�fung vornehmen
		// Schichtg�ltigkeit mu� kleiner als der Ende Ladedatum sein
		if (opShiftFromTime <= omToTime)
			blTimeCheckToOk = true;
		else
			blTimeCheckToOk = false;
	}

	//---------------------------------------------------------------------------------
	return (blTimeCheckFromOk && blTimeCheckToOk);
}

//****************************************************************************************************
// Buttons in der Titelleiste: Neuzeichnen wird angestossen.
//****************************************************************************************************

void CCodeDlg::OnTimer(UINT nIDEvent) 
{
	SendMessage(WM_NCPAINT, MAKEWPARAM(NULL, NULL), NULL);
	
	CDialog::OnTimer(nIDEvent);
}

//********************************************************************************************************************
// Description: Here we handle the most important thing, drawing in the non client area
//********************************************************************************************************************

void CCodeDlg::OnNcPaint()
{
	Default();
}

//********************************************************************************************************************
// Klick in die NonClient Area
//********************************************************************************************************************

void CCodeDlg::OnNcLButtonDown(UINT nHitTest, CPoint point)
{
	CRect rc; 
	GetClientRect(rc); 
	ScreenToClient(&point);
	
    if (nHitTest == HTMAXBUTTON)
	{
		// sendet Nachricht zum Wechseln der Dialoggr��e
		GetParent()->SendMessage(WM_USER_CHANGECODEDLG, 1, NULL);
	}
	else
	{
		CDialog::OnNcLButtonDown(nHitTest, point);
	}
}

//********************************************************************************************************************
// testet ob der Button getroffen wurde
// (hardcodiert)
//********************************************************************************************************************

bool CCodeDlg::IsButtonHit(CPoint point)
{
	CRect rc; 
	point.y = -point.y;

	if (point.x <=167 && point.x >= 154 && point.y <= 16 && point.y >= 6)
		return true;
	else
		return false;
}

//********************************************************************************************************************
// ODA Grid f�llen
//********************************************************************************************************************

void CCodeDlg::FillOdaGrid()
{
	pomOdaGrid->GetParam()->SetLockReadOnly(false);
	int ilRowCount = pomOdaGrid->GetRowCount();
	if (ilRowCount > 0)
		pomOdaGrid->RemoveRows(1,ilRowCount);
	// Abwesenheiten
	//uhi 17.04.00
	int ilCount = 0;

	pomOdaGrid->LockUpdate(TRUE);

	for (int i=0; i<ogOdaData.omData.GetSize(); i++ )
	{
		ODADATA *prlOda = &ogOdaData.omData[i];
		if (bmShowWish)
		{
			ilRowCount = pomOdaGrid->GetRowCount ();
			pomOdaGrid->InsertRows( ilRowCount+1,1);
			// Urno mit erstem Feld koppeln.						
			pomOdaGrid->SetStyleRange (CGXRange(ilRowCount+1,1), CGXStyle().SetItemDataPtr((void*)prlOda->Urno));
			// Datenfelder f�llen
			pomOdaGrid->SetValueRange(CGXRange(ilCount+1,1),	prlOda->Sdac );
			// Tooltip-Texte einstellen
			pomOdaGrid->SetValueRange(CGXRange(ilCount+1,2),prlOda->Sdan);

			ilCount++;
		}
		else
		{
			if (!strcmp(prlOda->Free, "x") || !strcmp(prlOda->Type, "S"))
			{
				int ilRowCount = pomOdaGrid->GetRowCount ();
				pomOdaGrid->InsertRows(ilRowCount+1,1);
				// Urno mit erstem Feld koppeln.						
				pomOdaGrid->SetStyleRange (CGXRange(ilRowCount+1,1), CGXStyle().SetItemDataPtr((void*)prlOda->Urno));
				// Datenfelder f�llen
				pomOdaGrid->SetValueRange(CGXRange(ilCount+1,1),	prlOda->Sdac );
				// Tooltip-Texte einstellen
				pomOdaGrid->SetValueRange(CGXRange(ilCount+1,2),prlOda->Sdan);
				ilCount++;
			}
		}
	}

	// Extra Tooltip anschalten
	pomOdaGrid->SetEnableExtraToolTip(true);

	// Grid sortieren
	SortGrid(pomOdaGrid,1);

	// Anzeige wieder anstellen
	pomOdaGrid->LockUpdate(false);
	pomOdaGrid->GetParam()->SetLockReadOnly(true);
	// Neu zeichnen.
	pomOdaGrid->Redraw();
}
//**********************************************************************************
// Initialisiert eine Map mit allen BSD Codes, jeder Code 
//**********************************************************************************

void CCodeDlg::PrepareBsdCodes(bool bpAllCodes)
{
CCS_TRY;
	CString olUrno;
	COleDateTime olShiftFromTime,olShiftToTime,olNullTime;
	//bool blTimeCheckFromOk = false;
	//bool blTimeCheckToOk = false;

	// Map l�schen
	omBsdMap.RemoveAll();

	// Sollte die Liste der anzuzeigenen OrgCodes leer sein, mu� nix angezeigt werden
//	if (omOrgList.IsEmpty())
//		return;
	
	// Schichten
	for ( int i=0; i<ogBsdData.omData.GetSize(); i++ )
	{
		BSDDATA *prlBsd = &ogBsdData.omData[i];

		if (CheckBsd(prlBsd,prlBsd->Vafr,prlBsd->Vato,bpAllCodes))
		{
			// -> Daten in die Map eintragen
			olUrno.Format("%ld",prlBsd->Urno);
			omBsdMap.SetAt(prlBsd->Bsdc,olUrno);
		}
	}
CCS_CATCH_ALL;
}

//**********************************************************************************
// Soll das Grid mit den W�nschen angezeigt werden (TRUE/FALSE)
//**********************************************************************************

void CCodeDlg::SetShowWish(bool bpShowWish)
{
	bmShowWish = bpShowWish;
	// Grid mit den W�nschen l�schen
	if (!bpShowWish)
		pomWisGrid->HideRows(0,pomWisGrid->GetRowCount(), true);
	else
		pomWisGrid->HideRows(0,pomWisGrid->GetRowCount(), false);
}

//**********************************************************************************
// BSD Grid f�llen
//**********************************************************************************

void CCodeDlg::FillBsdGrid(bool bpAllCodes)
{
CCS_TRY;
	// Anzeige austellen
	pomBsdGrid->LockUpdate(TRUE);

	// Warte Dialog einblenden.
	CWaitDlg	olWaitDlg(this,LoadStg(IDS_STRING1819));
  

	int ilRowCount;
	BSDDATA *prlBsd;
	POSITION pos;   
	CString key;
	CString olUrno;   
	long llUrno;
	int ilCount = 0;
	
	// Iterate through the entire map
	for( pos = omBsdMap.GetStartPosition(); pos != NULL; )
	{
		omBsdMap.GetNextAssoc( pos, key,olUrno);
		llUrno = atol(olUrno);

		prlBsd = ogBsdData.GetBsdByUrno(llUrno);

		if (prlBsd != NULL)
		{
			// Neue Row einf�gen
			ilRowCount = pomBsdGrid->GetRowCount ();
			pomBsdGrid->InsertRows ( ilRowCount+1,1);

			// Urno mit erstem Feld koppeln.						
			pomBsdGrid->SetStyleRange (CGXRange(ilRowCount+1,1), CGXStyle().SetItemDataPtr((void*)prlBsd->Urno));
	
			// Datenfelder f�llen
			pomBsdGrid->SetValueRange(CGXRange(ilCount+1,1),prlBsd->Bsdc );

			// Von Bis herstellen.
			CString olEsbg(prlBsd->Esbg);
			olEsbg = olEsbg.Left(2) + ":" + olEsbg.Right(2);
			CString olLsen(prlBsd->Lsen);
			olLsen = olLsen.Left(2) + ":" + olLsen.Right(2);

			// Datenfelder f�r Extra Tooltip f�llen
			pomBsdGrid->SetValueRange(CGXRange(ilCount+1,2),olEsbg+" - "+olLsen);
	
			ilCount++;
		}
	}
	// Extra Tooltip anschalten
	pomBsdGrid->SetEnableExtraToolTip(true);

	// Grid sortieren
	SortGrid(pomBsdGrid,1);

	// Anzeige wieder anstellen
	pomBsdGrid->LockUpdate(false);
	// Neu zeichnen.
	pomBsdGrid->Redraw();

CCS_CATCH_ALL;
}

