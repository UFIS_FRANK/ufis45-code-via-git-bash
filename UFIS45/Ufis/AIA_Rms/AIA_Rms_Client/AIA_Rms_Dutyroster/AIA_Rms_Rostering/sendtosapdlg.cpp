// SendToSapDlg.cpp : implementation file
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSendToSapDlg dialog


CSendToSapDlg::CSendToSapDlg(CWnd* pParent /*=NULL*/) : CDialog(CSendToSapDlg::IDD, pParent)
{
CCS_TRY;
	//{{AFX_DATA_INIT(CSendToSapDlg)
	m_VI_LangzeitDienstplan = -1;
	//}}AFX_DATA_INIT
CCS_CATCH_ALL;
}


void CSendToSapDlg::DoDataExchange(CDataExchange* pDX)
{
CCS_TRY;
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSendToSapDlg)
	DDX_Control(pDX, IDC_COMBO_ORG_UNIT, m_ComboOrgUnit);
	DDX_Control(pDX, IDOK,						m_OK);
	DDX_Control(pDX, IDCANCEL,					m_Cancel);
	DDX_Control(pDX, IDC_LANGZEIT_DIENSTPLAN,	m_CR_LangzeitDienstplan);
	DDX_Control(pDX, IDC_DATE_FROM,				m_CE_DateFrom);
	DDX_Control(pDX, IDC_DATE_TO,				m_CE_DateTo);
	DDX_Control(pDX, IDC_TIME_FROM,				m_CE_TimeFrom);
	DDX_Control(pDX, IDC_TIME_TO,				m_CE_TimeTo);
	DDX_Radio(pDX, IDC_LANGZEIT_DIENSTPLAN,		m_VI_LangzeitDienstplan);
	//}}AFX_DATA_MAP
CCS_CATCH_ALL;
}


BEGIN_MESSAGE_MAP(CSendToSapDlg, CDialog)
	//{{AFX_MSG_MAP(CSendToSapDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSendToSapDlg message handlers

BOOL CSendToSapDlg::OnInitDialog() 
{
CCS_TRY;
	CDialog::OnInitDialog();

	HICON m_hIcon = AfxGetApp()->LoadIcon(IDR_ROSTERTYPE);
	SetIcon(m_hIcon, FALSE);

	SetDlgItemText(IDC_ZEITRAUM_VON,LoadStg(PR_SAP_DATEFROM));
	SetDlgItemText(IDC_ZEITRAUM_BIS,LoadStg(PR_SAP_DATETO));
	SetDlgItemText(IDC_LANGZEIT_DIENSTPLAN,LoadStg(IDS_STRING341/*IDC_PLANLU*/));
	//SetDlgItemText(IDC_KURZZEIT_DIENSTPLAN2,LoadStg(IDS_STRING342/*DUTY_CAPTION*/));
	//SetDlgItemText(IDC_ABWESENHEITEN,LoadStg(IDS_STRING407));
	SetDlgItemText(IDC_ORG_UNIT, LoadStg(IDS_STRING452));
	SetDlgItemText(IDOK,LoadStg(ID_OK));
	SetDlgItemText(IDCANCEL,LoadStg(ID_CANCEL));

	SetWindowText(LoadStg(IDS_STRING451));

	CTime olCurrentTime = CTime::GetCurrentTime();
	//------------------------------------
	m_CE_DateFrom.SetTypeToDate( true);
	m_CE_DateFrom.SetBKColor(LTYELLOW);
	m_CE_DateFrom.SetTextErrColor(RED);
//	m_CE_DateFrom.SetSecState(ogPrivList.GetStat(".m_CE_DateFrom"));
	// - - - - - - - - - - - - - - - - - -
	m_CE_TimeFrom.SetTypeToTime( true);
	m_CE_TimeFrom.SetBKColor(LTYELLOW);
	m_CE_TimeFrom.SetTextErrColor(RED);
	m_CE_TimeFrom.SetInitText(olCurrentTime.Format("00:00"));
//	m_CE_TimeFrom.SetSecState(ogPrivList.GetStat(".m_CE_TimeFrom"));
	//------------------------------------
	m_CE_DateTo.SetTypeToDate( true);
	m_CE_DateTo.SetBKColor(LTYELLOW);
	m_CE_DateTo.SetTextErrColor(RED);
//	m_CE_DateTo.SetSecState(ogPrivList.GetStat(".m_CE_DateTo"));
	// - - - - - - - - - - - - - - - - - -
	m_CE_TimeTo.SetTypeToTime( true);
	m_CE_TimeTo.SetBKColor(LTYELLOW);
	m_CE_TimeTo.SetTextErrColor(RED);
	m_CE_TimeTo.SetInitText(olCurrentTime.Format("23:59"));
//	m_CE_TimeTo.SetSecState(ogPrivList.GetStat(".m_CE_TimeTo"));
	//------------------------------------

	// set RadioButtons 
	m_VI_LangzeitDienstplan = 0;

	FillComboOrgUnit();

	UpdateData( FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
CCS_CATCH_ALL;
return FALSE;
}

void CSendToSapDlg::OnOK() 
{
CCS_TRY;
//CDialog::OnOK();
	bool blStatus = true;

	CString olErrorText;

	UpdateData( TRUE);

	if (m_CE_DateFrom.GetStatus() == false)
	{
		blStatus = false;
		if (m_CE_DateFrom.GetWindowTextLength() == 0)
		{
			// Eingabefeld Zeitraum von (Datum) enth�lt keine Daten.\n*INTXT*
			olErrorText = LoadStg (IDS_STRING63464);
		}
		else
		{
			//Eingabefeld Zeitraum von (Datum) entspricht nicht dem Eingabeformat.\n*INTXT*
			olErrorText = LoadStg (IDS_STRING63465);
		}
	}
	if (m_CE_TimeFrom.GetStatus() == false)
	{
		blStatus = false;
		if (m_CE_TimeFrom.GetWindowTextLength() == 0)
		{
			// Eingabefeld Zeitraum von (Zeit) enth�lt keine Daten.\n*INTXT*
			olErrorText = LoadStg (IDS_STRING63466);
		}
		else
		{
			//Eingabefeld Zeitraum von (Zeit) entspricht nicht dem Eingabeformat.\n*INTXT*
			olErrorText = LoadStg (IDS_STRING63467);
		}
	}

	if (m_CE_DateTo.GetStatus() == false)
	{
		blStatus = false;
		if (m_CE_DateTo.GetWindowTextLength() == 0)
		{
			// Eingabefeld Zeitraum bis (Datum) enth�lt keine Daten.\n*INTXT*
			olErrorText = LoadStg (IDS_STRING63468);
		}
		else
		{
			//Eingabefeld Zeitraum bis (Datum) entspricht nicht dem Eingabeformat.\n*INTXT*
			olErrorText = LoadStg (IDS_STRING63469);
		}
	}
	if (m_CE_TimeTo.GetStatus() == false)
	{
		blStatus = false;
		if (m_CE_TimeTo.GetWindowTextLength() == 0)
		{
			// Eingabefeld Zeitraum bis (Zeit) enth�lt keine Daten.\n*INTXT*
		 	olErrorText = LoadStg (IDS_STRING63470);
		}
		else
		{
			// Eingabefeld Zeitraum bis (Zeit) enth�lt keine Daten.\n*INTXT*
			olErrorText = LoadStg (IDS_STRING63471);
		}
	}

	CString olOrgUnit;
	m_ComboOrgUnit.GetWindowText(olOrgUnit);
	if (olOrgUnit.GetLength() == 0)
	{
		blStatus = false;
		olErrorText = LoadStg(IDS_STRING59142);
	}
	else
	{
		olOrgUnit = olOrgUnit.Left(8);
		olOrgUnit.TrimRight();
		ORGDATA *prlOrgData = ogOrgData.GetOrgByDpt1(olOrgUnit);
		if (prlOrgData == NULL)
		{
			blStatus = false;
			olErrorText = LoadStg(IDS_STRING59143);
		}
	}

	if (blStatus == true) // ... input is OK
	{
		CWaitCursor olDummy;

		CString olDate;
		CString olTime;
		CTime olFrom;
		CTime olTo;

		m_CE_DateFrom.GetWindowText (olDate);
		m_CE_TimeFrom.GetWindowText (olTime);
		olFrom = DateHourStringToDate (olDate, olTime);

		m_CE_DateTo.GetWindowText (olDate);
		m_CE_TimeTo.GetWindowText (olTime);
		olTo = DateHourStringToDate (olDate, olTime);

		CString olDateFrom = olFrom.Format("%Y%m%d%H%M00");
		CString olDateTo   = olTo.Format("%Y%m%d%H%M59");

		bool blRet = true;
		static char clSelection[64];
		static char cBlank[2] = " ";
		static char cUser[12];
		static char cAction[6];
		static char cTwe[33] = "Rostering";
		sprintf(clSelection, "[LTR],%s,%s,%s", olOrgUnit, olDateFrom, olDateTo);
		strcpy(cUser,pcgUser);
		strcpy(cAction,"SAP");

		//CallCeda (req_id, dest1, dest2,  cmd,     object, seq,    tws,    twe,  selection,   fields, data,   data_dest);
		::CallCeda (cBlank, cUser, cBlank, cAction, cBlank, cBlank, cBlank, cTwe, clSelection, cBlank, cBlank, "RETURN");

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox (olErrorText, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
	}
	
CCS_CATCH_ALL;
}

void CSendToSapDlg::OnCancel() 
{
CCS_TRY;
	CDialog::OnCancel();
CCS_CATCH_ALL;
}

void CSendToSapDlg::InternalCancel()
{
CCS_TRY;
	CDialog::OnCancel();
CCS_CATCH_ALL;
}

void CSendToSapDlg::FillComboOrgUnit()
{
	CString olLine;

	m_ComboOrgUnit.ResetContent();
	m_ComboOrgUnit.SetFont(&ogCourier_Regular_8);

	int ilOrgCount =  ogOrgData.omData.GetSize();

	// add an empty entry
	m_ComboOrgUnit.AddString(olLine);

	for (int i = 0; i < ilOrgCount; i++)
	{
		olLine.Format("%-8s   %-40s",ogOrgData.omData[i].Dpt1, ogOrgData.omData[i].Dptn);
		m_ComboOrgUnit.AddString(olLine);
	}
}
