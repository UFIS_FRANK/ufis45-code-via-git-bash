// DutyDebitTabViewer.cpp 
//
#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

// Local function prototype
static void DutyDebitTabViewerCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//*******************************************************************************************************
// DutyDebitTabViewer
//*******************************************************************************************************

DutyDebitTabViewer::DutyDebitTabViewer(CWnd* pParent /*=NULL*/)
{
	//----------------------------------------------------------------
	// Anzahl der feststeheneden Lines festlegen
	imFixLines = 1;

	pomParent = (DutyRoster_View*)pParent;

    // daily roster records
	DdxRegister(this, DRR_CHANGE,	   "DutyDebitTabViewer", "DRR_CHANGE",	    DutyDebitTabViewerCf);
    DdxRegister(this, DRR_NEW,		   "DutyDebitTabViewer", "DRR_NEW",			DutyDebitTabViewerCf);
	DdxRegister(this, DRR_DELETE,	   "DutyDebitTabViewer", "DRR_DELETE",		DutyDebitTabViewerCf);
	
    // shift demand table
	DdxRegister(this, SDT_NEW,		   "DutyDebitTabViewer", "SDT_NEW",			DutyDebitTabViewerCf);
	DdxRegister(this, SDT_DELETE,	   "DutyDebitTabViewer", "SDT_DELETE",		DutyDebitTabViewerCf);

	// Start und Ende des Releases
	DdxRegister(this, ENDRLR,			"DutyDebitTabViewer", "ENDRLR",			DutyDebitTabViewerCf);
	DdxRegister(this, STARTRLR,			"DutyDebitTabViewer", "STARTRLR",		DutyDebitTabViewerCf);

	// account table
	DdxRegister(this, ACC_NEW,          "DutyDebitTabViewer", "ACC_NEW",        DutyDebitTabViewerCf);
    DdxRegister(this, ACC_CHANGE,       "DutyDebitTabViewer", "ACC_CHANGE",     DutyDebitTabViewerCf);
    DdxRegister(this, ACC_DELETE,       "DutyDebitTabViewer", "ACC_CHANGE",     DutyDebitTabViewerCf);

    pomDynTab = NULL;

	// Es ist zur Zeit kein Release aktiv
	bmReleaseInProgress = false;
}

//*******************************************************************************************************
//
//*******************************************************************************************************

DutyDebitTabViewer::~DutyDebitTabViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
	
	DeleteAllShifts();
}

//*******************************************************************************************************
//
//*******************************************************************************************************

void DutyDebitTabViewer::Attach(CCSDebitDynTable *popTable)
{
    pomDynTab = popTable;
	prmTableData = pomDynTab->GetTableData();
}

//*******************************************************************************************************
//
//*******************************************************************************************************

void DutyDebitTabViewer::ChangeView(bool bpSetHNull /*=true*/,bool bpSetVNull /*=true*/)
{
	CCS_TRY;
	WriteLogFull("enter DutyDebitTabViewer::ChangeView()");

	prmViewInfo = &pomParent->rmViewInfo;
	pomParent->GetParentFrame()->GetMessageBar()->SetWindowText(LoadStg(IDS_STRING63498));
	DeleteAllShifts();
	
	int ilDays = 0;
	if(prmViewInfo->oDateFrom.GetStatus() == COleDateTime::valid && prmViewInfo->oDateTo.GetStatus() == COleDateTime::valid &&
		prmViewInfo->oShowDateFrom.GetStatus() == COleDateTime::valid && prmViewInfo->oShowDateTo.GetStatus() == COleDateTime::valid)
	{
		omShowFrom = prmViewInfo->oShowDateFrom;
		omShowTo   = prmViewInfo->oShowDateTo;
		omFrom	   = prmViewInfo->oDateFrom;
		omTo       = prmViewInfo->oDateTo;
		
		COleDateTimeSpan olDays = omShowTo - omShowFrom;
		imDays = olDays.GetDays()+1;
		ilDays = imDays - 1;
		
		//~~~~~ 05.07.99 SHA : 
		COleDateTimeSpan olTmpDays = omTo - omFrom;
		imDebitDays = olTmpDays.GetDays()+1;
		
		omDateFrom.Format("%s",omFrom.Format("%Y%m%d"));
		omDateTo.Format("%s",omTo.Format("%Y%m%d"));
		
		//--------------------------------------------------
		// Extra Tage, werden zum eigentliche Zeitraum addiert
		imBonusLoadDays = (imDays -imDebitDays) /2;
		
		// 14.3.00 ADO : Ladezeitraum mu� nicht symmetrisch sein
		olDays = omFrom - omShowFrom;
		imBonusLoadDaysFrom = olDays.GetDays();
		
		olDays = omShowTo - omTo;
		imBonusLoadDaysTo = olDays.GetDays();
	}
	else
	{
		imDays = 0;
		ilDays = 0;
		imBonusLoadDays = 0;
		imBonusLoadDaysFrom = 0;
		imBonusLoadDaysTo = 0;
	}
	
	//--------------------------------------------------
	// ADD IN GROUPS
	if (prmViewInfo->bGroupStat==true)
		mGroupMode=TRUE;
	else
		mGroupMode=FALSE;
	
	//--------------------------------------------------
	// COLLECT SHIFT WITH FUNCTION SEPERATLY ?
	if (prmViewInfo->bAdditiv == true)
		mStatMode = SHIFTONLY;
	else
		mStatMode = SHIFTWITHFCT;
	
	//--------------------------------------------------
	// Herkunft der Ist-Werte
	// woraus sollen die Ist-Werte der Statistik ermittelt werden?
	// 1 - 'PLAN'			exklusiv Schichtplan-DRRs, 
	// 2 - 'LANGZEIT'		exklusiv Langzeitdienstplan,
	// 3 - 'DIENSTPLAN'		aktuelle DRRs ab Dienstplan,
	// 4 - 'ALLE'			DRRs mit h�chster Stufe ab Schichtplan, 
	// 5 - 'CURRENT'		alt, ROS/DSR
	
	imStatView = prmViewInfo->iGetStatFrom;
	
	//--------------------------------------------------
	// VIEW DIFFERENCES OR ACTUAL/PLANNED
	if (prmViewInfo->iDCoverage == 1)
		mCalcMode = ISTSOLL;
	else
		mCalcMode = DIFFERENZ;
	
	//--------------------------------------------------
	// IF THERE IS NO FUNCTION OR NO ORGANISATIONS (NO GROUPS AND NO DETAILS) THEN DONT CALC ANYTHING
	if ((prmViewInfo->bGroupStat != true) && (prmViewInfo->bDetailStat != true)
		|| prmViewInfo->oPfcStatFctcs.IsEmpty() /*|| prmViewInfo->oOrgStatDpt1s.IsEmpty()*/ )
		mStatistic = FALSE;
	else
		mStatistic = TRUE;
	
	
	//--------------------------------------------------
	if (mStatistic)
	{
		//*** SHIFT LINE 0 = TOTAL AMOUNT FOR EVERY DAY ***
		GenerateTotalShift();	
		// Verplante Tage/Schichten einlesen
		PreparePlanedDays();
		//Bedarfe werden eingelesen
		ReadDebitData();		
		// Schichten einlesen.
		ReadAllShiftFromDrr();
		
		SortShifts();			
		
		int ilColumnStartPos = 0;
		TABLEINFO rlTableInfo;
		pomDynTab->GetTableInfo(rlTableInfo);
		if(!bpSetHNull)
		{
			ilColumnStartPos = rlTableInfo.iColumnOffset;
		}
		
		pomDynTab->SetHScrollData(0,ilDays,ilColumnStartPos);
		
		int ilRowStartPos = imFixLines;
		if(!bpSetVNull)
		{
			ilRowStartPos = rlTableInfo.iRowOffset;
		}
		
		int ilLines = omDDTV_Shifts.GetSize()-1;	// inklusive FixLines
		pomDynTab->SetVScrollData(imFixLines, ilLines, ilRowStartPos);
		
		InitTableArrays(ilColumnStartPos);
		MakeTableData(ilColumnStartPos,ilRowStartPos);
		pomDynTab->InvalidateRect(NULL);
	}
	else
		RefreshView();
	//END STATISTICS YES NO --------------------------------------------------
	CCS_CATCH_ALL;
}

//*******************************************************************************************************
// Broadcast Behandlung
//*******************************************************************************************************

static void DutyDebitTabViewerCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	DutyDebitTabViewer *polViewer = (DutyDebitTabViewer *)popInstance;
	
	// Weitergabe an Viewer
	polViewer->HandleBc(ipDDXType,vpDataPointer);
}

/********************************************************************************************
uhi 7.9.00 Neue Schicht im Current-Mode (Coverage) anzeigen
********************************************************************************************/
void DutyDebitTabViewer::ProcessSdtNew(SDTDATA* popSdt)
{
	CCS_TRY;

	// Sollte der Viewer ausgeschaltet sein, auf keine BC`s reagieren
	if (!mStatistic)
		return;
	
	CString				olDate,olFct,olKey,olBsdc;
	DDTV_SHIFT			rlTmpShift;
	
	olFct = popSdt->Fctc;
	olBsdc = popSdt->Bsdc;
	
	// Funktionsvergleich
	CString olCheck = popSdt->Fctc;
	if(olCheck == "")
		olCheck = "NO_PFCCODE";
	olCheck ="'" + olCheck +"'";
	
	if((prmViewInfo->oPfcStatFctcs.Find(olCheck)==-1) /* geh�rt es hierher? ich denke ja...: && mStatMode == SHIFTWITHFCT*/)
	{
		// diese Funktion wird nicht angezeigt
		return;
	}

	int ilLineNo = LookForSdt(popSdt, olKey);
	if (ilLineNo == -1)
	{
		rlTmpShift.Fctc = olFct;
		rlTmpShift.Bsdc = olBsdc;
		//-----------------------------------------------------
		//Shift Text herstellen
		// Von Bis herstellen.
		CString olText = "";
		CString olEsbg = popSdt->Esbg.Format("%H:%M");
		CString olLsen = popSdt->Lsen.Format("%H:%M");
		rlTmpShift.omShiftTime = popSdt->Esbg.Format("%H%M") + popSdt->Lsen.Format("%H%M");
		
		// Funktionen mit Anzeigen
		if (mStatMode == SHIFTWITHFCT && !olFct.IsEmpty())
			olText = " ("+olFct+", " + olEsbg+" - "+olLsen + ")";
		else
			olText = " ("+ olEsbg+" - "+olLsen + ")";
		
		rlTmpShift.ShiftText = olBsdc + olText;
		//-------------------------------------------------------
		
		rlTmpShift.DataType = "2";
		ilLineNo = GenerateShift(rlTmpShift,olKey);
		SortShifts();
		ilLineNo = LookForSdt(popSdt, olKey);
	}

	//*** ADD NOW ***
	olDate.Format("%s",popSdt->Esbg.Format("%Y%m%d"));
	int ilDay=-1;
	omDates.Lookup(olDate,(void*&)ilDay);

	if (ilLineNo!=-1 && ilDay!=-1)
	{
		AddShiftDebit(ilLineNo,ilDay,true);	
		
		if (mGroupMode)
		{
			ilLineNo=0;
			CString olKey="";
			if (omGroupMember.Lookup(olBsdc,(void*&)ilLineNo))
			{
				ilLineNo = LookForGroup(ilLineNo);
				AddShiftDebit(ilLineNo,ilDay,false);
			}
		}

		pomParent->GetParentFrame()->GetMessageBar()->SetWindowText(LoadStg(IDS_STRING63499));
	}

	RefreshView();

	CCS_CATCH_ALL;;
}

void DutyDebitTabViewer::ProcessSdtDelete(SDTDATA* popSdt)
{
	CCS_TRY;
	
	CString				olDate,olKey;
	
	//*** SUBSTRACT NOW ***
	int ilLineNo = LookForSdt(popSdt, olKey);
	if(ilLineNo == -1)
		return;

	olDate.Format("%s",popSdt->Esbg.Format("%Y%m%d"));
	
	int ilDay = -1;
	omDates.Lookup(olDate,(void*&)ilDay);
	
	if (ilDay!=-1)
	{
		bool blFuncIsTrue = true;
		if(prmViewInfo->bExtraShifts)
		{
			// nur in diesem Fall m�ssen wir pr�fen
			CString olCheck(popSdt->Fctc);
			if(olCheck == "")
				olCheck = "NO_PFCCODE";
			olCheck ="'" + olCheck +"'";
			if(prmViewInfo->oPfcStatFctcs.Find(olCheck)==-1)
				blFuncIsTrue = false;
		}
		
		DecreaseShiftDebit(ilLineNo,ilDay,blFuncIsTrue);

		if (mGroupMode)
		{
			ilLineNo=0;
			CString olKey="";
			if (omGroupMember.Lookup(popSdt->Bsdc,(void*&)ilLineNo))
			{
				ilLineNo = LookForGroup(ilLineNo);
				DecreaseShiftDebit(ilLineNo,ilDay,FALSE);
			}
		}

		// look, if we have to delete the line, because there are nomore demands
		int ilMax = 0;
		for (int ilTmpDay = 0; ilTmpDay < imDebitDays; ilTmpDay++)
		{
			ilMax = __max (((DayDebitShift*)omDDTV_Shifts[ilLineNo].omDayDebitShift[ilTmpDay])->DebitCount, ilMax);
			ilMax = __max (((DayDebitShift*)omDDTV_Shifts[ilLineNo].omDayDebitShift[ilTmpDay])->ShiftCount, ilMax);
		}
		if (ilMax == 0)
		{
			for (int i = 0; i < omDDTV_Shifts[ilLineNo].omDayDebitShift.GetSize(); i++)
			{
				delete omDDTV_Shifts[ilLineNo].omDayDebitShift[i];
			}
			omDDTV_Shifts[ilLineNo].omDayDebitShift.RemoveAll();
			DDTV_SHIFT *polTmp = &omDDTV_Shifts[ilLineNo];
			delete polTmp;//(DDTV_SHIFT)omDDTV_Shifts[ilLineNo];
			omDDTV_Shifts.RemoveAt (ilLineNo);
			CString olKey;
			int ilLineNo;
			if (mStatMode == SHIFTONLY || !strlen(popSdt->Fctc))
			{
				olKey.Format("%s",popSdt->Bsdc);
			}
			else
			{
				olKey.Format("%s-%s",popSdt->Bsdc,popSdt->Fctc);
			}
			
			if(omShiftMap.Lookup(olKey,(void*&)ilLineNo))
			{
				omShiftMap.RemoveKey (olKey);
			}
			int ilLookupLine;
			for (POSITION olPos = omShiftMap.GetStartPosition(); olPos != NULL;)
			{
				omShiftMap.GetNextAssoc(olPos, olKey, (void*&)ilLookupLine);
				if (ilLookupLine > ilLineNo)
				{
					ilLookupLine--;
					omShiftMap.SetAt (olKey, (void*)ilLookupLine);
				}
			}
		}
	}

	RefreshView();
	
	CCS_CATCH_ALL;
}

//*****************************************************************************************
// Broadcast Behandlung: Ein DRR wurde ver�ndert
//*****************************************************************************************

void DutyDebitTabViewer::ProcessDrrDelete(DRRDATA* popDrr)
{
	HandleOffDays(popDrr);

	// Daten der alten Schicht l�schen
	DeleteSingleShiftData(popDrr);

	RefreshView();
}

//*****************************************************************************************
// // Broadcast Behandlung: Ein DRR wurde ver�ndert
//*****************************************************************************************

void DutyDebitTabViewer::ProcessDrrChange(DRRDATA* popDrr)
{
	// Daten der alten Schicht l�schen
	CString olKeyDrrChanged;	// zu l�schende DRR kommt mit Scod leer, daher brauchen wir den Key aus interner 
								// Datenhaltung (was durch L�schen zerst�rt wird)

	// erst um Abwesenheiten k�mmern
	HandleOffDays(popDrr);

	DeleteSingleShiftData(popDrr, &olKeyDrrChanged);

	// jetzt neue Schicht hinzuf�gen
	if (strlen(popDrr->Scod))
	{
		// Nur diese eine Schicht zuf�gen
		ReadSingleShiftFromDrr(popDrr);
	}

	if (ogDrrData.pomLastInternChangedDrr == popDrr)
	{
		if (bgScrollToNewShiftLine)
		{
			CString olNewLineKey;
			olNewLineKey.Format("%s-%s", popDrr->Scod, popDrr->Fctc);
			Scroll2ActLine(popDrr, &olNewLineKey);
		}
		else
		{
			Scroll2ActLine(popDrr, &olKeyDrrChanged);
		}

		ogDrrData.pomLastInternChangedDrr = NULL;
	}

	RefreshView();
}


//*****************************************************************************************
// Ein neuer Drr wurde erzeugt.
//*****************************************************************************************

void DutyDebitTabViewer::ProcessDrrNew(DRRDATA* popDrr)
{
	HandleOffDays(popDrr);

	// Schicht f�r einen DRR einlesen.
	ReadSingleShiftFromDrr(popDrr);
	
	if (ogDrrData.pomLastInternChangedDrr == popDrr)
	{
		Scroll2ActLine(popDrr);
		ogDrrData.pomLastInternChangedDrr = NULL;
	}

	RefreshView();
}

/******************************************************************************************
bearbeitet Broadcastst ACC_NEW ACC_CHANGE ACC_DELETE
******************************************************************************************/
void DutyDebitTabViewer::ProcessAccBc()
{
	// die Statdaten nach CCharacteristicValueDlg
	SendOffDayChangedBC();
}


//*****************************************************************************************
// Behandelt die BCs
//*****************************************************************************************

void DutyDebitTabViewer::HandleBc(int ipDDXType, void *vpDataPointer)
{
	// Sollte der Viewer ausgeschalten sein, auf keine BC`s reagieren
	if (!mStatistic)
		return;

	switch(ipDDXType)
	{
	case SDT_NEW:
		ProcessSdtNew((SDTDATA*)vpDataPointer);
		break;
	case SDT_DELETE:
		ProcessSdtDelete((SDTDATA*)vpDataPointer);
		break;
	case ACC_NEW:
	case ACC_CHANGE:
	case ACC_DELETE:
		ProcessAccBc();
		break;
	case STARTRLR:
		// Synchronisation Release
		bmReleaseInProgress = true;
		break;
	case ENDRLR:
		bmReleaseInProgress = false;
		// Release wurde beendet, vollst�ndig neu berechnen
		ChangeView(FALSE,FALSE);
		break;
	case DRR_NEW:
		if (!bmReleaseInProgress)
		{
			DRRDATA* polDrr = (DRRDATA*) vpDataPointer;
			// Nur dieser eine DRR wird eingearbeitet
			ProcessDrrNew(polDrr);
		}
		break;
	case DRR_DELETE:
		if (!bmReleaseInProgress)
		{
			// Vollst�ndig neu berechnen
			DRRDATA* polDrr = (DRRDATA*) vpDataPointer;
			ProcessDrrDelete(polDrr);
		}
		break;
	case DRR_CHANGE:
		if (!bmReleaseInProgress)
		{
			DRRDATA* polDrr = (DRRDATA*) vpDataPointer;
			ProcessDrrChange(polDrr);
		}
		break;
	}
} 

//*******************************************************************************************************
//
//*******************************************************************************************************

void DutyDebitTabViewer::HorzTableScroll(MODIFYTAB *prpModiTab)
{
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);
	if (prpModiTab->iID == rlTableInfo.iID)
	{
		MakeTableData(prpModiTab->iNewHorzPos,prpModiTab->iNewVertPos/*-imFixLines*/);
		pomDynTab->ChangedTableData(prpModiTab->iNewHorzPos,prpModiTab->iNewVertPos);
	}
	else
	{
		MakeTableData(prpModiTab->iNewHorzPos,rlTableInfo.iRowOffset/*-imFixLines*/);
		pomDynTab->ChangedTableData(prpModiTab->iNewHorzPos,rlTableInfo.iRowOffset);
	}
}

//*******************************************************************************************************
//
//*******************************************************************************************************

void DutyDebitTabViewer::VertTableScroll(MODIFYTAB *prpModiTab)
{
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);
	
	if(prpModiTab->iID == rlTableInfo.iID)
	{
		MakeTableData(prpModiTab->iNewHorzPos,prpModiTab->iNewVertPos);
		pomDynTab->ChangedTableData(prpModiTab->iNewHorzPos,prpModiTab->iNewVertPos);
	}
	else
	{
		MakeTableData(rlTableInfo.iColumnOffset,prpModiTab->iNewVertPos);
		pomDynTab->ChangedTableData(rlTableInfo.iColumnOffset,prpModiTab->iNewVertPos);
	}
}

//*******************************************************************************************************
//
//*******************************************************************************************************

void DutyDebitTabViewer::TableSizing(/*UINT nSide, */LPRECT lpRect)
{
	pomDynTab->SizeTable(/*nSide, */lpRect);
}

//*******************************************************************************************************
//
//*******************************************************************************************************

void DutyDebitTabViewer::MoveTable(MODIFYTAB *prpModiTab)
{
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);
	if(prpModiTab->iID == rlTableInfo.iID)
	{
		if(prmTableData->oSolid.GetSize() > 0 && prmTableData->oHeader.GetSize() > 0)
		{
			MakeTableData(prpModiTab->iNewHorzPos,prpModiTab->iNewVertPos);
		}
	}
	else
	{
		int ilX = 0;
		int ilY = 0;
		ilY = prpModiTab->oNewRect.bottom - prpModiTab->oOldRect.bottom;
		pomDynTab->MoveTable(ilX,ilY);
	}
}

//*******************************************************************************************************
//
//*******************************************************************************************************

void DutyDebitTabViewer::InitTableArrays(int ipColumnOffset /*=0*/)
{
	CString olText; 
	
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);
	pomDynTab->ResetContent();
		
	prmTableData->oSolidHeader.oText		= CString(" ") + LoadStg(IDS_STRING1553);
	prmTableData->oSolidHeader.oTextColor	= BPN_NOCOLOR;
	prmTableData->oSolidHeader.oBkColorNr	= BPN_NOCOLOR;
}

/*******************************************************************************************************
ipRowOffset startet vor FixLines
*******************************************************************************************************/

void DutyDebitTabViewer::MakeTableData(int ipColumnOffset,int ipRowOffset)
{
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);

	//** Add rows by SIZING
	for(int ilActualRows = prmTableData->oSolid.GetSize(); ilActualRows < rlTableInfo.iRows; ilActualRows++)
	{
		// Damit die ersten Reihen immer fest bleiben, ohne Offset berechnen

		if (ilActualRows < imFixLines)
			AddRow(ilActualRows, rlTableInfo.iColumns,ipColumnOffset, ilActualRows+1);
		else
			AddRow(ilActualRows, rlTableInfo.iColumns,ipColumnOffset, ipRowOffset+ilActualRows);
	}

	//** Delete rows by SIZING
	while(rlTableInfo.iRows < ilActualRows)
	{
		DelRow(ilActualRows-1);
		ilActualRows = prmTableData->oSolid.GetSize();
	}
	
	int ilActualColumns = prmTableData->oHeader.GetSize();
	//*** Add columns by SIZING
	while(ilActualRows>0 && rlTableInfo.iColumns > ilActualColumns)
	{
		AddColumn(ilActualColumns,ipColumnOffset+ilActualColumns,ipRowOffset);
		ilActualColumns = prmTableData->oHeader.GetSize();
	}
	//** Delete columns by SIZING
	while(ilActualRows>0 && rlTableInfo.iColumns < ilActualColumns)
	{
		DelColumn(ilActualColumns-1);
		ilActualColumns = prmTableData->oHeader.GetSize();
	}
	//****************************************************************
	int ilColumnDiv = ipColumnOffset-rlTableInfo.iColumnOffset;
	if(ilColumnDiv != 0)
	{
		//** SCROLL right
		if(ilColumnDiv > 0)
		{
			if(ilColumnDiv > prmTableData->oHeader.GetSize())
				ilColumnDiv = prmTableData->oHeader.GetSize();
			for(int i=0; i < ilColumnDiv;i++)
			{
				DelColumn(0);
				AddColumn(prmTableData->oHeader.GetSize(),ipColumnOffset+rlTableInfo.iColumns-ilColumnDiv+i,ipRowOffset);
			}
		}
		else //** SCROLL left if(ilColumnDiv < 0)
		{
			ilColumnDiv = -(ilColumnDiv);
			if(ilColumnDiv > prmTableData->oHeader.GetSize())
				ilColumnDiv = prmTableData->oHeader.GetSize();
			for(int i=0; i < ilColumnDiv; i++)
				
			{
				DelColumn(prmTableData->oHeader.GetSize()-1);
				AddColumn(0,ipColumnOffset+ilColumnDiv-i-1,ipRowOffset);
			}
		}
	}
	int ilRowDiv = ipRowOffset - rlTableInfo.iRowOffset; // rlTableInfo.iRowOffset - absolut ab Header

	if(ilRowDiv != 0)
	{
		//** SCROLL DOWN
		if(ilRowDiv > 0)
		{
			ilActualRows = prmTableData->oSolid.GetSize();
			
			for(int i=0; i < ilRowDiv; i++)
			{
				DelRow(imFixLines);

				AddRow(ilActualRows-1,rlTableInfo.iColumns,ipColumnOffset,ilActualRows + i + 1 + rlTableInfo.iRowOffset-imFixLines); //org
				blLastActionIsScrollUpOrDown = S_SCROLL_DOWN;
			}

		} 
		else	//** SCROLL UP	if(ilRowDiv < 0)
		{
			ilRowDiv = -(ilRowDiv);
			for(int i=0; i < ilRowDiv; i++)
			{
				DelRow(prmTableData->oSolid.GetSize()-1);
				AddRow(imFixLines,rlTableInfo.iColumns,ipColumnOffset,rlTableInfo.iRowOffset - imFixLines - i + 1);
				blLastActionIsScrollUpOrDown = S_SCROLL_UP;
			}
		}
	}
}

//*******************************************************************************************************
// Verkn�pfung der Oberfl�che mit den Daten
// ipRowNr - Nummer relativ zu Header, d.h. erste nach dem Header == 0
//*******************************************************************************************************

void DutyDebitTabViewer::AddRow(int ipRow, int ipColumns, int ipColumnNr, int ipRowNr)
{
	CString olText;
	COLORREF olCol;
	
	//-------------------------------------------------------------
	//** Solid
	FIELDDATA *polFieldData = new FIELDDATA;
	
	if (mStatistic)
	{
		polFieldData->oText		= GetShiftDescription(ipRowNr);
	}
	else
	{
		polFieldData->oText		= "";
	}
	
	polFieldData->oTextColor	= BPN_NOCOLOR;
	polFieldData->oBkColorNr	= BPN_NOCOLOR;
	// Es darf DnD gestartet werden.
	polFieldData->bDrag = TRUE;
	
	prmTableData->oSolid.InsertAt(ipRow,(void*)polFieldData);

	//** Table
	CPtrArray *polTableValue = new CPtrArray;
	// Alle Spalten durchgehen
	for(int ilColumn=0;ilColumn<ipColumns;ilColumn++)
	{
		olText.Empty();
		FIELDDATA *polFieldData = new FIELDDATA;
		polFieldData->oTextColor	= BPN_NOCOLOR;
		polFieldData->oBkColorNr	= BPN_NOCOLOR;
		polFieldData->rIPEAttrib.TextMaxLenght = 8;
		polFieldData->rIPEAttrib.TextMinLenght = 1;
		polFieldData->bInplaceEdit = false;
		
		olText="";
		if(ipColumnNr+ilColumn<imDays)
		{
			olText="";
			olText = ReadShiftFormated(ipRowNr, ipColumnNr+ilColumn - imBonusLoadDaysFrom, olCol);
			polFieldData->oTextColor = olCol;
		}
		polFieldData->oText	= olText;
		
		// DnD erlauben ---------------------------
		polFieldData->bDrag = TRUE;
		//---------------------------------
		polTableValue->Add((void*)polFieldData);
	}
	prmTableData->oTable.InsertAt(ipRow,(void*)polTableValue);
	
}

//*******************************************************************************************************
//
//*******************************************************************************************************

void DutyDebitTabViewer::DelRow(int ipRow)
{
	//** Table Text Color
	
	CPtrArray *polTableValue = (CPtrArray*)prmTableData->oTable[ipRow];
	for(int i = polTableValue->GetSize()-1;i>=0;i--)
	{
		FIELDDATA *polFieldData = (FIELDDATA*)(*polTableValue)[i];
		delete polFieldData;
	}
	polTableValue->RemoveAll();
	delete polTableValue;
	prmTableData->oTable.RemoveAt(ipRow);
	
	//** Solid Text Color
	FIELDDATA *polFieldData = (FIELDDATA*)prmTableData->oSolid[ipRow];
	delete polFieldData;
	prmTableData->oSolid.RemoveAt(ipRow);
}

//*******************************************************************************************************
// ipRowNr startet vor FixLines
//*******************************************************************************************************

void DutyDebitTabViewer::AddColumn(int ipColumn, int ipColumnNr, int ipRowNr)
{
	CString olText; 
	COLORREF olCol = 0;
	
	// Header (ilRow=0) ---------------------------------------- 
	olText.Empty();
	FIELDDATA *polFieldData = new FIELDDATA;
	polFieldData->oTextColor = BPN_NOCOLOR;
	polFieldData->oBkColorNr = BPN_NOCOLOR;
	
	if(ipColumnNr<imDays)
	{
		if (mStatistic)
			olText = ReadShiftFormated(0 ,ipColumnNr - imBonusLoadDaysFrom, olCol);
	}
	
	polFieldData->oText = olText;
	prmTableData->oHeader.InsertAt(ipColumn,(void*)polFieldData);

	// Table ---------------------------------------------------------
	int ilActualRows = prmTableData->oTable.GetSize();
	for(int ilRow = 0; ilRow<ilActualRows; ilRow++)
	{
		olText.Empty();
		// Wir holen uns immer ganze Reihen nach und nach aus der Oberfl�che ab
		CPtrArray *polTableValue = (CPtrArray*)prmTableData->oTable[ilRow];
		
		FIELDDATA *polFieldData = new FIELDDATA;
		polFieldData->oTextColor = BPN_NOCOLOR;
		polFieldData->oBkColorNr = BPN_NOCOLOR;
		polFieldData->rIPEAttrib.TextMaxLenght = 8;
		polFieldData->rIPEAttrib.TextMinLenght = 1;
		polFieldData->bInplaceEdit = false;
		// DnD erlauben ---------------------------
		polFieldData->bDrag = TRUE;
		//---------------------------------
		
		olText="";
		if(ipColumnNr<imDays)
		{
			if (ipColumnNr<imBonusLoadDaysFrom)
			{
				olText = "";
			}
			else if (mStatistic)
			{
				// ADO: Fixed Lines. Sollte die Row kleiner als die Anzahl der festen Lines sein,
				// wird der Wert OHNE Rowoffset berechnet-
				if (ilRow < imFixLines)
					olText = ReadShiftFormated(ilRow+1,ipColumnNr - imBonusLoadDaysFrom, olCol);
				else
					olText = ReadShiftFormated(ipRowNr+ilRow,ipColumnNr - imBonusLoadDaysFrom, olCol);
			}
			
			polFieldData->oTextColor = olCol;
		}
		// Und f�gen in die abgeholten Reihen neue Spalten ein
		polFieldData->oText	= olText;
		polTableValue->InsertAt(ipColumn,(void*)polFieldData);
	}
	
}

//*******************************************************************************************************
//
//*******************************************************************************************************

void DutyDebitTabViewer::DelColumn(int ipColumn)
{
	//** Header Text Color
	FIELDDATA *polFieldData = (FIELDDATA*)prmTableData->oHeader[ipColumn];
	delete polFieldData;
	prmTableData->oHeader.RemoveAt(ipColumn);

	//** Table Text Color
	int ilActualRows = prmTableData->oTable.GetSize();
	for(int ilRow = 0; ilRow<ilActualRows; ilRow++)
	{
		CPtrArray *polTableValue = (CPtrArray*)prmTableData->oTable[ilRow];
		FIELDDATA *polFieldData = (FIELDDATA*)(*polTableValue)[ipColumn];
		delete polFieldData;
		polTableValue->RemoveAt(ipColumn);
	}
	
}

//*******************************************************************************************************
// Bedarfe einlesen
//*******************************************************************************************************

int DutyDebitTabViewer::ReadDebitData()
{
	CCS_TRY;
	
	int					ilSdt,ilCount,ilDay;
	COleDateTimeSpan	olDays;
	COleDateTime		olTmpDay;
	CString				olDate,olFct,olKey,olBsdc;
	DDTV_SHIFT			rlTmpShift;
	
	omDatesString.RemoveAll();
	
	//*** map day to index ***
	//e.g. 19990610 -> index = 3
	for (ilDay=0;ilDay<imDebitDays;ilDay++)
	{
		olDays.SetDateTimeSpan(ilDay,0,0,0);
		olTmpDay = omFrom + olDays;
		omDates.SetAt(olTmpDay.Format("%Y%m%d"),(void*)ilDay);
		omDatesString.Add(olTmpDay.Format("%Y%m%d"));
	}

	//***LOOP OVER ALL SDTs***
	int ilLineNo;
	for (ilSdt=0,ilCount = ogSdtData.omData.GetSize();ilSdt<ilCount;ilSdt++)
	{
		olKey.Empty();
		SDTDATA* polSdt = &ogSdtData.omData[ilSdt];

		olFct = polSdt->Fctc;
		olBsdc = polSdt->Bsdc;
		
		ilLineNo = LookForSdt(polSdt, olKey);
		if (ilLineNo == -1)
		{
			//*** NOT FOUND -> PRODUCE NEW ONE***
			// nur wenn das unser Bedarf ist
			bool blFuncIsTrue = true;
			// nur in diesem Fall m�ssen wir pr�fen
			CString olCheck(polSdt->Fctc);
			if(olCheck == "")
				olCheck = "NO_PFCCODE";
			olCheck ="'" + olCheck +"'";
			if(prmViewInfo->oPfcStatFctcs.Find(olCheck)==-1)
				continue;	// fremder Bedarf

			rlTmpShift.Fctc = olFct;
			rlTmpShift.Bsdc = olBsdc;
			
			//-----------------------------------------------------
			//Shift Text herstellen
			// Von Bis herstellen.
			CString olText = "";
			CString olEsbg = polSdt->Esbg.Format("%H:%M");
			CString olLsen = polSdt->Lsen.Format("%H:%M");
			rlTmpShift.omShiftTime = polSdt->Esbg.Format("%H%M") + polSdt->Lsen.Format("%H%M");
			
			// Funktionen mit Anzeigen
			if (mStatMode == SHIFTWITHFCT && !olFct.IsEmpty())
				olText = " ("+olFct+", " + olEsbg+" - "+olLsen + ")";
			else
				olText = " ("+ olEsbg+" - "+olLsen + ")";
			
			rlTmpShift.ShiftText = olBsdc + olText;
			//-------------------------------------------------------
			
			rlTmpShift.DataType = "2";
			ilLineNo = GenerateShift(rlTmpShift,olKey);
		}
		
		olDate.Format("%s",polSdt->Esbg.Format("%Y%m%d"));
		ilDay=-1;
		omDates.Lookup(olDate,(void*&)ilDay);
		
		//*** ADD NOW ***
		if (ilLineNo!=-1 && ilDay!=-1)
		{
			AddShiftDebit(ilLineNo,ilDay,true);	
			
			if (mGroupMode)
			{
				ilLineNo=0;
				CString olKey="";
				if (omGroupMember.Lookup(olBsdc,(void*&)ilLineNo))
				{
					ilLineNo = LookForGroup(ilLineNo);
					AddShiftDebit(ilLineNo,ilDay,false);
				}
				
			}
		}
	}//***END LOOP OVER ALL SDTs ***
	SortShifts();

	CCS_CATCH_ALL;
	return 0;
}

//*******************************************************************************************************
//
//*******************************************************************************************************

int DutyDebitTabViewer::GenerateShift(DDTV_SHIFT &rlShiftData, CString olKey)
{
	//*** GENERATE A NEW LINE FOR A SHIFT ***
	DDTV_SHIFT		*prlShift;
	DayDebitShift	*prlDayDebitShift;
	
	prlShift = new DDTV_SHIFT;
	
	prlShift->Bsdc = rlShiftData.Bsdc;
	prlShift->Fctc = rlShiftData.Fctc;
	prlShift->omShiftTime = rlShiftData.omShiftTime;
	//----------------------------------------------------------------
	// ADO 30.5.00
	prlShift->ShiftText = CString(" ") + rlShiftData.ShiftText;
	//----------------------------------------------------------------
	prlShift->Key = olKey;
	prlShift->DataType = rlShiftData.DataType;
	
	for (int ilDay=0;ilDay<imDebitDays;ilDay++)
	{
		prlDayDebitShift = new DayDebitShift;
		prlDayDebitShift->DebitCount=0;
		prlDayDebitShift->ShiftCount=0;
		
		prlShift->omDayDebitShift.Add(prlDayDebitShift);
	}
	
	omDDTV_Shifts.Add(prlShift);
	
	int	ilLineNo = omDDTV_Shifts.GetSize()-1;
	
	omShiftMap.SetAt(olKey,(void*)ilLineNo);
	
	return ilLineNo;
}

//*******************************************************************************************************
//
//*******************************************************************************************************

int DutyDebitTabViewer::GenerateTotalShift()
{
	//*** THE 0-LINE FOR THE TOTALS ***
	DDTV_SHIFT		rlTmpShift;
	
	rlTmpShift.Fctc = "-";
	rlTmpShift.Bsdc = "0";
	rlTmpShift.omShiftTime = "";
	rlTmpShift.ShiftText = "Total";
	
	rlTmpShift.DataType = "0";
	GenerateShift(rlTmpShift,"TOTAL");
	return 0;
}

//*******************************************************************************************************
//
//*******************************************************************************************************

int DutyDebitTabViewer::AddShiftDebit(int ilLineNo, int ilDayNo, BOOL bAddTotal)
{
	//*** INCREASE DEBIT FOR SHIFT AT DAY ***
	DayDebitShift *prlDayDebitshift;
	DayDebitShift *prlDayDebitshiftTotal;
	
	prlDayDebitshift=(DayDebitShift*)omDDTV_Shifts[ilLineNo].omDayDebitShift[ilDayNo];
	
	prlDayDebitshift->DebitCount++;
	
	if (bAddTotal)
	{
		//*** ADD TO TOTAL LINE 0 ! ***
		prlDayDebitshiftTotal=(DayDebitShift*)omDDTV_Shifts[0].omDayDebitShift[ilDayNo];
		prlDayDebitshiftTotal->DebitCount++;
	}
	
	return 0;
}

//uhi 7.9.00
int DutyDebitTabViewer::DecreaseShiftDebit(int ilLineNo, int ilDayNo, BOOL bAddTotal)
{
	//*** DECREASE DEBIT FOR SHIFT AT DAY ***
	DayDebitShift *prlDayDebitshift;
	DayDebitShift *prlDayDebitshiftTotal;
	
	if (ilLineNo < omDDTV_Shifts.GetSize())
	{
		if (ilDayNo < omDDTV_Shifts[ilLineNo].omDayDebitShift.GetSize())
		{
			prlDayDebitshift=(DayDebitShift*)omDDTV_Shifts[ilLineNo].omDayDebitShift[ilDayNo];
			
			prlDayDebitshift->DebitCount--;
			
			if (bAddTotal)
			{
				//*** SUBTRACT FROM TOTAL LINE 0 ! ***
				prlDayDebitshiftTotal=(DayDebitShift*)omDDTV_Shifts[0].omDayDebitShift[ilDayNo];
				prlDayDebitshiftTotal->DebitCount--;
			}
		}
	}
	
	return 0;
}

//*******************************************************************************************************
// Anzahl der Schicht um 1 verringern
//*******************************************************************************************************

int DutyDebitTabViewer::DecreaseShiftCount(int ilLineNo, int ilDayNo, CString opKey, BOOL bAddTotal)
{
	//*** INCREASE SHIFT FOR SHIFT AT DAY ***
	DayDebitShift *prlDayDebitshift;
	DayDebitShift *prlDayDebitshiftTotal;
	
	ASSERT(ilLineNo >= 0);
	ASSERT(ilDayNo >= 0);
	
	// Schicht Anzahl an diesem Tag um 1 verringern
	prlDayDebitshift=(DayDebitShift*)omDDTV_Shifts[ilLineNo].omDayDebitShift[ilDayNo];
	prlDayDebitshift->ShiftCount--;
	
	// Titelzeile aktualisierem
	if (bAddTotal)
	{
		//*** ADD TO TOTAL LINE 0 ! ***
		prlDayDebitshiftTotal=(DayDebitShift*)omDDTV_Shifts[0].omDayDebitShift[ilDayNo];
		prlDayDebitshiftTotal->ShiftCount--;
	}
	
	// Alle Tage f�r diese Schicht pr�fen.
	// Pr�fen ob die ganze Zeile gel�scht werden soll
	// Keine Schicht und auch kein Schicht Soll mehr vorhanden
	bool blLineEmpty = true;
	
	// Achtung nur wenn bAddTotal TRUE ist werden leere Zeilen gel�scht.
	for (int i=0; (i < imDebitDays) && blLineEmpty /*&& bAddTotal*/; i++)
	{
		prlDayDebitshift=(DayDebitShift*)omDDTV_Shifts[ilLineNo].omDayDebitShift[i];
		
		if (prlDayDebitshift->ShiftCount != 0 || prlDayDebitshift->DebitCount != 0)
			blLineEmpty = false;
	}
	
	// Auswerten
	if (blLineEmpty)// && bAddTotal)
	{
		// Leere Zeile l�schen
		TRACE("Delete Line\n");
		DeleteShiftByLineNo(ilLineNo);
		omDDTV_Shifts.DeleteAt(ilLineNo);
		// Maps aktualisieren
		omShiftMap.RemoveKey(opKey);
		//*** SORT SHIFTS(LINES)  FOR OUTPUT IN GRID ***
		// Wichtig um Indizies wieder herzustellen.
		SortShifts();			
	}
	return 0;
}

//*******************************************************************************************************
// Anzahl der Schicht um 1 erh�hen.
//*******************************************************************************************************

int DutyDebitTabViewer::AddShiftCount(int ilLineNo, int ilDayNo, BOOL bAddTotal)
{
	//*** INCREASE SHIFT FOR SHIFT AT DAY ***
	DayDebitShift *prlDayDebitshift;
	DayDebitShift *prlDayDebitshiftTotal;
	
	ASSERT (ilLineNo >= 0);
	ASSERT (ilDayNo >= 0);
	
	prlDayDebitshift=(DayDebitShift*)omDDTV_Shifts[ilLineNo].omDayDebitShift[ilDayNo];
	
	prlDayDebitshift->ShiftCount++;
	
	if (bAddTotal)
	{
		//*** ADD TO TOTAL LINE 0 ! ***
		prlDayDebitshiftTotal=(DayDebitShift*)omDDTV_Shifts[0].omDayDebitShift[ilDayNo];
		prlDayDebitshiftTotal->ShiftCount++;
	}
	
	return 0;
}

//*******************************************************************************************************
//
//*******************************************************************************************************

CString DutyDebitTabViewer::ReadShiftFormated(int ilLineNo, int ilDayNo, COLORREF &ShiftCol)
{
	CString		cDmy = "";
	int			ilShift = 0;
	int			ilDebit = 0;
	
	DayDebitShift *prlDayDebitshift;
	
	if ((ilLineNo>=0 && ilLineNo<omDDTV_Shifts.GetSize()) && (ilDayNo>=0 && ilDayNo <omDDTV_Shifts[ilLineNo].omDayDebitShift.GetSize()))
	{
		prlDayDebitshift=(DayDebitShift*)omDDTV_Shifts[ilLineNo].omDayDebitShift[ilDayNo];
		
		ilShift = prlDayDebitshift->ShiftCount;
		ilDebit = prlDayDebitshift->DebitCount;
		
		// Schichttyp ermitteln
		CString olType;
		olType = omDDTV_Shifts[ilLineNo].DataType;
		
		// F�r geplante/nicht geplante Tage andere Darstellung
		if (olType == "1" )
		{
			int ilValue = GetFreeShiftValue(ilShift,ilDayNo);
			ilValue = ilValue - ilDebit;
			// String Format festlegen
			cDmy.Format("%d", ilValue);
			
			// Farbe bestimmen
			if (ilValue >= 0)
				ShiftCol = GREEN;
			else
				ShiftCol = RED;
		}
		else
		{
			if (mCalcMode == ISTSOLL)
				cDmy.Format("%d/%d", ilShift, ilDebit);
			else
				cDmy.Format("%+d", ilShift - ilDebit);
			
			if (ilDebit == ilShift)
				ShiftCol = BLACK;
			
			if (ilDebit < ilShift)
				ShiftCol = BLUE;
			
			if (ilDebit > ilShift)
				ShiftCol = RED;
			
			if (ilShift == 0 && ilDebit == 0)
				cDmy = "-";
		}
	}
	
	return cDmy;
}

//*******************************************************************************************************
// Alle DRRs durchgehen und die Schichten bewerten, wird ein opSday �bnergeben, werden nur DRR
// die diesem SDay entsprechen ausgewertet
//*******************************************************************************************************

int DutyDebitTabViewer::ReadAllShiftFromDrr()
{
	DRRDATA* polDrr;

	// L�schen der DRR->Schichtcode Map
	omDrrToCodeMap.RemoveAll();
	omDrrToStatusMap.RemoveAll();
	omStatShiftsWithoutDemand.RemoveAll();
	omSecondShifts.RemoveAll();

	int ilDrrSize = ogDrrData.GetInternalSize();

	for (int ilDrr = 0; ilDrr < ilDrrSize; ilDrr++)
	{
		polDrr = ogDrrData.GetInternalData(ilDrr);

		ODADATA* polOda = ogOdaData.GetOdaByUrno(polDrr->Bsdu);
		if (polOda != NULL)
		{
			if (UseDrrRecord(polDrr->Rosl, polDrr->Ross, false))
			{
				if (IsStfuInStatUrnos(polDrr->Stfu))
				{
					IncreaseAbsenceCount(polDrr->Sday);
					CString olKey;
					olKey.Format ("%ld", polDrr->Urno);
					omDrrOdaUrnos.SetAt(olKey, NULL);
				}
			}
		}
		else
		{
			HandleOffDays(polDrr);
			// Daten des DRR auswerten, keine einzelnen BC senden
			ReadSingleShiftFromDrr(polDrr, false);
		}
	}
	// BC mit Gesamtsumme senden
	SendOffDayChangedBC();

	return 0;
}

//*******************************************************************************************************
// Liest einen einzigen DRR ein und bearbeitet die Schicht.
// R�ckgabe: wurde DRR eingef�gt
//*******************************************************************************************************

bool DutyDebitTabViewer::ReadSingleShiftFromDrr(DRRDATA* popDrr,bool bpSendBC/*=true*/,CString* popKeyDrrChanged/*=0*/)
{
	bool blReturn = false;

	CString olKey;
	int ilDayMap;
	int ilDayNo = -1;
	int ilLineNo = -1;
	//DayDebitShift *prlDayDebitshift;
	bool blIsInStat;

	// ---------- Pr�fung des DRR---------------------
	// Pr�fen ob dieser DRR bearbeitet werden soll
	if (!IsDrrValid(popDrr, false, &blIsInStat))
		return false;

	// Summen-Anzeige anpassen, wenn die Funktion der Schicht stimmt (d.h. das ist eine der Bedarfs-Schichten)
	bool blFuncIsTrue = false;
	if (!prmViewInfo->bExtraShifts) // nur in diesem Fall m�ssen wir pr�fen
	{
		if (strlen(popDrr->Fctc) > 0)
		{
			if (HasShiftDemand(popDrr))
			{
				blFuncIsTrue = true;
			}
		}
		else
		{
			CString olCheck;
			olCheck = "'NO_PFCCODE'";
			if (prmViewInfo->oPfcStatFctcs.Find(olCheck) > -1)
			{
				blFuncIsTrue = true;
			}
		}
		/*if (blFuncIsTrue && IsStfuInExtraUrnos(popDrr->Stfu) && ogDrrData.HasDoubleTour(popDrr))
		{
			DRSDATA *polDrs = ogDrsData.GetDrsByDrru(popDrr->Urno);
			if (polDrs != NULL)
			{
				if (strcmp(polDrs->Stat, "S") == 0)
			}
		}*/
	}
	else
	{
		blFuncIsTrue = true;
	}
	BSDDATA* polBsd = ogBsdData.GetBsdByUrno(popDrr->Bsdu);

	// Pr�fen ob dieser Schichtcode bereits vorkommt
	ilLineNo = LookForDrr(popDrr, olKey);
	if (ilLineNo != -1)
	{
		// Summen-Anzeige anpassen
		omDates.Lookup (CString(popDrr->Sday), (void*&)ilDayMap);
		AddShiftCount (ilLineNo, ilDayMap, blFuncIsTrue);
		blReturn = true;
		if (popKeyDrrChanged != 0)
		{
			*popKeyDrrChanged = olKey;
		}
	}
	else if (blIsInStat && strlen(popDrr->Scod) != 0)
	{
		// nur die inStat-MAs werden ber�cksichtigt
		DDTV_SHIFT rlTmpShift;
		// Daten vorbereiten
		rlTmpShift.Fctc = popDrr->Fctc;
		rlTmpShift.Bsdc = popDrr->Scod;
		
		//-----------------------------------------------------
		//Shift Text herstellen
		// Von Bis herstellen.
		CString olText = "";
		
		CString olEsbg(polBsd->Esbg);
		CString olLsen(polBsd->Lsen);
		rlTmpShift.omShiftTime = olEsbg + olLsen;
		olEsbg = olEsbg.Left(2) + ":" + olEsbg.Right(2);
		olLsen = olLsen.Left(2) + ":" + olLsen.Right(2);
		
		if (mStatMode == SHIFTWITHFCT && strlen(popDrr->Fctc)>0)
		{
			olText = " (";
			olText += popDrr->Fctc;
			olText += ", " + olEsbg+" - "+olLsen + ")";
		}
		else
		{
			olText = " ("+ olEsbg+" - "+olLsen + ")";
		}
		
		rlTmpShift.ShiftText = popDrr->Scod + olText;
		//-----------------------------------------------------
		rlTmpShift.DataType = "2";
		// Schicht in die Datenhaltung und in die Detailanzeige einf�gen

		ilLineNo = GenerateShift(rlTmpShift,olKey);
		SortShifts();
		
		// Summen-Anzeige anpassen
		omShiftMap.Lookup(olKey,(void*&)ilLineNo);
		omDates.Lookup(CString(popDrr->Sday),(void*&)ilDayMap);						
		AddShiftCount(ilLineNo,ilDayMap,blFuncIsTrue);
		blReturn = true;
	}
	
	//-------------------------------------------------------------------------
	// Gruppen
	if (mGroupMode && (strlen(popDrr->Scod) != 0))
	{
		//*** ADD TO THE GROUPS ***
		ilLineNo=0,ilDayMap=0;
		olKey="";
		if (omGroupMember.Lookup(popDrr->Scod,(void*&)ilLineNo))
		{
			olKey = omGroupName[ilLineNo];
			omShiftMap.Lookup(olKey,(void*&)ilLineNo);
			omDates.Lookup(CString(popDrr->Sday),(void*&)ilDayMap);			
			
			AddShiftCount(ilLineNo,ilDayMap,false);
		}
	}
	//-------------------------------------------------------------------------
	
	// Sollte die Schicht eingetragen worden sein, DRR->Schichtcode Map aktualisieren
	if (blReturn)
	{
		// Drr in die Code-Func-Map einf�gen
		InsertDrrToCodeMap(popDrr);
		// Status Map aktualisieren
		SetStatMap(popDrr);
	}
	
	return blReturn;
}

//*******************************************************************************************************
//
//*******************************************************************************************************

CString DutyDebitTabViewer::GetShiftDescription(int ilNo)
{
	//*** GIVE THE DESCRIPTION FOR THE ROW-HEADER ***
	if (ilNo >= 0 && ilNo<omDDTV_Shifts.GetSize())
	{
		return omDDTV_Shifts[ilNo].ShiftText;
	}
	else
		return CString(" ");
}

//*******************************************************************************************************
// Schichtbeginn vergleichen
//*******************************************************************************************************

static int CompareShiftbeginn( const DDTV_SHIFT **e1, const DDTV_SHIFT **e2)
{
	//*** REQUIRED END-ORDER: TOTAL - SHIFTS - GROUPS ***
	//*** TOTAL MUST BE LINE 0 !! ***
	
	int ilVgl=strcmp ( (**e1).DataType , (**e2).DataType);
	
	if ( ilVgl == 0)
	{
		int ilEsbgCmp = strcmp ( (**e1).omShiftTime , (**e2).omShiftTime);
		if(!ilEsbgCmp)
		{
			return ( strcmp ( (**e1).Bsdc , (**e2).Bsdc) );		// wenn die Zeiten gleich sind, vergleichen wir die Bsdc
		}
		else
		{
			return ilEsbgCmp;
		}
	}
	else
	{
		return ilVgl;
	}
}

//*******************************************************************************************************
// Schichten vergleichen
//*******************************************************************************************************

static int CompareShifts( const DDTV_SHIFT **e1, const DDTV_SHIFT **e2)
{
	//*** REQUIRED END-ORDER: TOTAL - SHIFTS - GROUPS ***
	//*** TOTAL MUST BE LINE 0 !! ***
	
	int ilVgl=strcmp ( (**e1).DataType , (**e2).DataType);
	
	if ( ilVgl == 0)
		return ( strcmp ( (**e1).Bsdc , (**e2).Bsdc) );
	else
		return ilVgl;
}

//*******************************************************************************************************
// Schichten sortieren
//*******************************************************************************************************

int DutyDebitTabViewer::SortShifts()
{
	// die Sortierung bestimmen
	int ilSortstatistic = ogCfgData.GetMonitorForWindow(MON_SORTSTATISTIC);

	if(ilSortstatistic == 0)
	{
		// 0 - nach Schichtcode
		omDDTV_Shifts.Sort(CompareShifts);
	}
	else
	{
		// 1 - nach Schichtbeginn
		omDDTV_Shifts.Sort(CompareShiftbeginn);
	}
	
	omShiftMap.RemoveAll();
	int ilCount=omDDTV_Shifts.GetSize();
	for (int ilD=0;ilD<ilCount;ilD++)
	{
		omShiftMap.SetAt(omDDTV_Shifts[ilD].Key,(void*)ilD);		
	}
	
	return 0;
}

//*******************************************************************************************************
//
//*******************************************************************************************************

int DutyDebitTabViewer::DeleteAllShifts()
{
	//*** DELETE THE COMPLETE DATA SET FOR STATISTICS ***
	int iCount=omDDTV_Shifts.GetSize();
	
	if (iCount == 0)
		return 0;
	
	for (int i=0;i<iCount;i++)
	{
		DeleteShiftByLineNo(i);
	}
	
	omDDTV_Shifts.DeleteAll();
	omDDTV_Shifts.RemoveAll();
	
	omShiftMap.RemoveAll();
	omDates.RemoveAll();
	omDatesString.RemoveAll();
	
	omGroupMember.RemoveAll();
	omGroupMap.RemoveAll();
	omGroupName.RemoveAll();
	omDrrOdaUrnos.RemoveAll();

	return 0;
}

//*******************************************************************************************************
//
//*******************************************************************************************************

int DutyDebitTabViewer::DeleteShiftByLineNo(int ilNo)
{
	//*** DELETE A SHIFT LINE BY LINE-NO ***
	
	for (int i=0;i<omDDTV_Shifts[ilNo].omDayDebitShift.GetSize();i++)
	{
		delete omDDTV_Shifts[ilNo].omDayDebitShift[i];
	}
	
	return 0;
}

//*****************************************************************************************
// Die Daten dieser Schicht aus der Ansicht entfernen
//*****************************************************************************************

void DutyDebitTabViewer::DeleteSingleShiftData(DRRDATA* popDrr, CString* popKeyDrrChanged/*=0*/)
{
	// Alten Schichtcode ermitteln. Diese Schicht mu� entfernt werden
	CString olUrno,olOldCode, olKey;

	olUrno.Format("%ld",popDrr->Urno);
	omDrrToCodeMap.Lookup(olUrno,olOldCode);		

	// Testen des Codes
	if (olOldCode.IsEmpty())
	{
		omDrrToStatusMap.RemoveKey(olUrno);
		omDrrToCodeMap.RemoveKey(olUrno);
		return;
	}

	bool blIsInStat;
	// Pr�fen ob der eingegangene DRR bearbeitet werden soll. Keine Pr�fung des Schichttyps
	if (!IsDrrValid(popDrr,true, &blIsInStat))
		return;

	olKey = olOldCode;
	if(popKeyDrrChanged != 0)
		*popKeyDrrChanged = olKey;

	int ilLineNo = -1;
	int ilDayNo = -1;			
	// Positionen ermitteln um Summen-Anzeige anzupassen
	omShiftMap.Lookup(olKey,(void*&)ilLineNo);
	omDates.Lookup(CString(popDrr->Sday),(void*&)ilDayNo);						
	
	// Anzahl der Schicht um 1 verringern
	// eventuell Zeilen l�schen
	// Wurde die Position gefunden ?

	bool blFuncIsTrue = false;
	if (!prmViewInfo->bExtraShifts)
	{
		if (strlen(popDrr->Fctc) > 0)
		{
			if (HadShiftDemand(popDrr))
			{
				blFuncIsTrue = true;
			}
		}
		else
		{
			CString olCheck;
			olCheck = "'NO_PFCCODE'";
			if (prmViewInfo->oPfcStatFctcs.Find(olCheck) > -1)
			{
				blFuncIsTrue = true;
			}
		}
	}
	else
	{
		blFuncIsTrue = true;
	}
	
	if (ilLineNo != -1  && ilDayNo != -1)
	{
		DecreaseShiftCount(ilLineNo,ilDayNo,olKey,blFuncIsTrue);						
	}
	
	//-------------------------------------------------------------------------
	// Gruppen
	if (mGroupMode )
	{
		ilLineNo=-1,ilDayNo=-1;
		int ilGroupNumber = -1;
		olKey="";
		if (omGroupMember.Lookup(olOldCode,(void*&)ilGroupNumber))
		{
			ilLineNo = LookForGroup(ilGroupNumber);
			olKey = omGroupName[ilGroupNumber];

			omDates.Lookup(CString(popDrr->Sday),(void*&)ilDayNo);
			
			if (ilLineNo != -1 && ilDayNo != -1)
				DecreaseShiftCount(ilLineNo,ilDayNo,olKey,false);						
		}
	}
	omDrrToCodeMap.RemoveKey(olUrno);
}

//*****************************************************************************************
// Pr�ft ob eine Schicht bearbeitet werden soll
// bpIsDelete -> Pr�fung zum L�schen einer Schicht
//*****************************************************************************************

bool DutyDebitTabViewer::IsDrrValid(DRRDATA* popDrr, bool bpIsDelete, bool* pbpIsInStat/*=0*/)
{
	CString	olCheck;
	CString	olKey;
	CString	olDateString;

	if (pbpIsInStat!=0)
		*pbpIsInStat = false;

	// Testen des Zeitraums
	olCheck = popDrr->Sday;
	olDateString = olCheck;
	if (olCheck < omDateFrom || olCheck > omDateTo)
		return false;

	// Planungsstufe u. -status pr�fen
	if (!UseDrrRecord(popDrr->Rosl, popDrr->Ross, bpIsDelete))
		return false;

	// L�schen eines DRR der sich vormals in einer Doppeltour als Sch�ler befunden hat nicht durchf�hren
	if (!bpIsDelete)// && !ogDrrData.HasDoubleTour(popDrr))
	{
		SetStatMap(popDrr);
	}
	else
	{
		CString olOldStat;
		CString olUrno; 
		olUrno.Format("%ld",popDrr->Urno);
		omDrrToStatusMap.Lookup(olUrno,olOldStat);

		if (olOldStat == "S") 
		{
			if (!IsStfuInStatUrnos(popDrr->Stfu))
				omDrrToStatusMap.RemoveKey(olUrno);
			// Nur wenn er vorher auch Sch�ler war nicht weiter bearbeiten
			return false;
		}
	}

	// die Schichten merken, die nicht zu den Bedarfen z�hlen
	if (bpIsDelete && !HadShiftDemand(popDrr))
	{
		CString olKey;
		void* prlVoid;
		olKey.Format ("%ld", popDrr->Urno);
		if (omStatShiftsWithoutDemand.Lookup(olKey, prlVoid))
		{
			omStatShiftsWithoutDemand.RemoveKey(olKey);
		}
	}
	else if (!bpIsDelete && !HasShiftDemand(popDrr))
	{
		if (IsStfuInStatUrnos(popDrr->Stfu))
		{
			BSDDATA *polBsdData;
			polBsdData = ogBsdData.GetBsdByUrno (popDrr->Bsdu);
			if (polBsdData != NULL)
			{
				CString olKey;
				void* prlVoid;
				olKey.Format ("%ld", popDrr->Urno);
				if (!omStatShiftsWithoutDemand.Lookup(olKey, prlVoid))
				{
					omStatShiftsWithoutDemand.SetAt(olKey, NULL);
				}
			}
		}
	}

	// ----------------------------------------------------------------------------------------
	// -- "zweite Schichten" (DRRN != '1') merken, damit sie "possible days OFF" nicht beeinflussen
	// ----------------------------------------------------------------------------------------
	if (bpIsDelete)
	{
		CString olKey;
		olKey.Format ("%ld", popDrr->Urno);
		omSecondShifts.RemoveKey(olKey);
	}
	else
	{
		if (strcmp(popDrr->Drrn, "1") != 0 /*|| ogOdaData.GetOdaBySdac((ogDrrData.GetDrrByKey(popDrr->Sday, popDrr->Stfu, "1", popDrr->Rosl))->Scod) != NULL*/)
		{
			CString olKey;
			olKey.Format ("%ld", popDrr->Urno);
			omSecondShifts.SetAt(olKey, NULL);
		}
	}

	//-------------------------------------------------------------------------
	// Handelt es sich um den Sch�ler in einer Doppeltour
	//-------------------------------------------------------------------------
	if (ogDrrData.HasDoubleTour(popDrr))
	{
		// Achtung : Sollte ein Wechsel von Lehrer nach Sch�ler stattfinden, mu� der betrefende DRRCODE gel�scht
		// werden, also mu� diese Funktion True zur�ckgeben. Aber nur in dieser Konstellation
		// Bei einem Wechsel von garnichts zu Sch�ler mu� der Drr ebenfalls gel�scht werden.

		//Status in der Doppeltour pr�fen
		DRSDATA* polDrs = ogDrsData.GetDrsByDrru(popDrr->Urno);
		if (polDrs != NULL)
		{
			if (strcmp(polDrs->Stat, "S") == 0)
			{
				if (!bpIsDelete)
				{
					/*// Ein Wechsel von Lehrer zu Sch�ler
					CString olOldStat,olUrno; 
					olUrno.Format("%ld",popDrr->Urno);
					omDrrToStatusMap.Lookup(olUrno,olOldStat);
					
					// Nur wenn er vorher auch Sch�ler war nicht weiter bearbeiten
					if (olOldStat == "S") 
					{*/
					return false;
					//}
				}
				/*else
				{
					// Eintragen das es ein Sch�ler ist
					SetStatMap(popDrr);
					return false;
				}*/
			}
		}
	}
	//-------------------------------------------------------------------------
	// Test der Mitarbeiter Urno: ist der MA �berhaupt in der Liste? Und in welcher?
	olCheck.Format("%d",popDrr->Stfu);
	bool blFound = false;
	int ilStfCount = prmViewInfo->oStfUrnos.GetSize();
	for (int ilStf=0; ilStf < ilStfCount; ilStf++)
	{
		if(olCheck == prmViewInfo->oStfUrnos[ilStf])
		{
			blFound = true;
			break;
		}
	}
	
	if(!blFound)
		return false;	// dieser MA ist gar nicht angezeigt

	// ist die Funktion der Schicht f�r den Statistik-Teil relevant?
	bool blStatFound = false;
	PFCDATA *polPfcData = ogPfcData.GetPfcByFctc(CString(popDrr->Fctc));
	if (polPfcData != NULL)
	{
		CString olSearchFor;
		olSearchFor.Format("'%s'", popDrr->Fctc);
		if (strstr(pomParent->rmViewInfo.oPfcStatFctcs, olSearchFor.GetBuffer(0)) != NULL)
		{
			blStatFound = true;
			if (pbpIsInStat!=0)
				*pbpIsInStat = true;
		}
	}
	else
	{
		if (!strlen(popDrr->Fctc))
		{
			if (strstr(pomParent->rmViewInfo.oPfcStatFctcs,"NO_PFCCODE") != NULL)
			{
				blStatFound = true;
				if (pbpIsInStat!=0)
					*pbpIsInStat = true;
			}
		}
	}

	if (!blStatFound)
	{
		// kein Stat-MA
		// - er geh�rt nicht zu selektierten, testen ob diese seine Schicht in Bedarfen gespeichert ist
		CString olKey;
		if(bpIsDelete)
		{
			// Alten Schichtcode ermitteln. Diese Schicht mu� entfernt werden und hat kein Scode mehr
			CString olUrno;
			olUrno.Format("%ld",popDrr->Urno);
			omDrrToCodeMap.Lookup(olUrno,olKey);		
		}
		if(LookForDrr(popDrr, olKey) == -1)
			return false;	
	}
	//-------------------------------------------------------------------------
	
	//-------------------------------------------------------------------------
	// Testen ob die Funktionen stimmen, wenn nicht alles gezeigt werden soll
	if(!bpIsDelete && mStatMode == SHIFTWITHFCT && !prmViewInfo->bExtraShifts)
	{
		BSDDATA* polBsd = ogBsdData.GetBsdByUrno(popDrr->Bsdu);
		if(polBsd != 0)
		{
			olCheck = popDrr->Fctc;
			if (olCheck == "")
			{
				olCheck = "NO_PFCCODE";
			}
			olCheck ="'" + olCheck +"'";
			if (prmViewInfo->oPfcStatFctcs.Find(olCheck) == -1)
			{
				return false;
			}
		}
	}

	return true;
}

//*****************************************************************************************
// Zeile "geplante Schichten" einf�gen
//*****************************************************************************************

void DutyDebitTabViewer::PreparePlanedDays()
{
	// the line "1" for offdays
	DDTV_SHIFT	rlTmpShift;
	
	rlTmpShift.Fctc = "-";
	rlTmpShift.Bsdc = "1";
	rlTmpShift.omShiftTime = "";
	rlTmpShift.ShiftText = LoadStg(IDS_STRING1921);
	rlTmpShift.DataType = "1";
	// Zeile einf�gen 
	GenerateShift(rlTmpShift,"OFFDAYS");	
}

//*****************************************************************************************
// Schichten neu sortieren und neu anzeigen lassen
//*****************************************************************************************

void DutyDebitTabViewer::RefreshView()
{
	// Alte Position merken
	int ilColumnOffset = 0;
	int ilRowStartPos = 0;
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);
	ilColumnOffset = rlTableInfo.iColumnOffset;
	ilRowStartPos = rlTableInfo.iRowOffset;
	
	//*** ROWS = NUMBER OF SHIFTLINES ***
	int ilLines = omDDTV_Shifts.GetSize()-1;	// inklusive FixLines
	pomDynTab->SetVScrollData(imFixLines, ilLines, ilRowStartPos);

	// Tabelle neu zeichnen
	// Mu� gemacht werden, weil durch scroll nicht alles erzeugt werden kann, wenn zu wenige Lines
	// vorhanden sind
	InitTableArrays(ilColumnOffset);
	MakeTableData(ilColumnOffset,ilRowStartPos);
	pomDynTab->InvalidateRect(NULL);
}

//*****************************************************************************************
// Pr�ft ob ein g�ltiger Abwesenheitscode �bergeben wurde
//*****************************************************************************************

bool DutyDebitTabViewer::IsCodeOda(long lpUrno)
{
	// Pr�fen ob es sich um eine Schicht oder Abwesenheit handelt
	ODADATA*	prlOda;
	prlOda = ogOdaData.GetOdaByUrno(lpUrno);
	
	if (prlOda == NULL)
		return false;
	else
		return true;
}

//*****************************************************************************************
// Pr�ft ob ein g�ltiger Abwesenheitscode �bergeben wurde
//*****************************************************************************************

bool DutyDebitTabViewer::IsCodeOda(CString opCode)
{
	// Pr�fen ob es sich um eine Schicht oder Abwesenheit handelt
	ODADATA*	prlOda;
	prlOda = ogOdaData.GetOdaBySdac(opCode);
	
	if (prlOda == NULL)
		return false;
	else
		return true;
}

//*****************************************************************************************
// Berechnet die Anzahl der noch zu vergebenen Abwesenheitsschichten
//*****************************************************************************************

int DutyDebitTabViewer::GetFreeShiftValue(int ipOdaCount, int ipDayNo)
{
	// Variablen/Konstanten bestimmen
	int ilValue = 0;
	int ilLineNo = 0;
	int ilStfCount = prmViewInfo->oStfStatUrnos.GetSize();

	DayDebitShift* prlDayDebitshiftTotal=(DayDebitShift*)omDDTV_Shifts[0].omDayDebitShift[ipDayNo];
	int ilDebitSum = prlDayDebitshiftTotal->DebitCount;

	omShiftMap.Lookup("OFFDAYS",(void*&)ilLineNo);

	// Schicht, die im Bedarf nicht verlangt wird, verringert Anzahl "M�glicher Freier Tage"
	int ilDiff = 0;
	for (int i = 1; i < omDDTV_Shifts.GetSize(); i++)
	{
		if (i != ilLineNo)
		{
			DayDebitShift* prlDayDebitshift=(DayDebitShift*)omDDTV_Shifts[i].omDayDebitShift[ipDayNo];
			int ilDebitCount = prlDayDebitshift->DebitCount;
			int ilShiftCount = prlDayDebitshift->ShiftCount;
			if (ilShiftCount > ilDebitCount && ilDebitCount > 0)
			{
				ilDiff  = ilDiff + (ilShiftCount - ilDebitCount);
			}
		}
	}

	POSITION olPos;
	CString olUrno;
	DRRDATA *popDrr;
	long llUrno;
	void* prlVoid;
	int ilDayNo = 0;

	for (olPos = omStatShiftsWithoutDemand.GetStartPosition(); olPos != NULL; )
	{
		omStatShiftsWithoutDemand.GetNextAssoc(olPos, olUrno, (void*&)prlVoid);
		llUrno = atol(olUrno);
		popDrr = ogDrrData.GetDrrByUrno(llUrno);
		if (popDrr != NULL)
		{
			omDates.Lookup(CString(popDrr->Sday),(void*&)ilDayNo);
			if (ilDayNo == ipDayNo)
			{
				ilDiff++;
			}
		}
	}

	int ilSecondShifts = 0;
	for (olPos = omSecondShifts.GetStartPosition(); olPos != NULL; )
	{
		omSecondShifts.GetNextAssoc(olPos, olUrno, (void*&)prlVoid);
		llUrno = atol(olUrno);
		popDrr = ogDrrData.GetDrrByUrno(llUrno);
		if (popDrr != NULL)
		{
			omDates.Lookup(CString(popDrr->Sday),(void*&)ilDayNo);
			if (ilDayNo == ipDayNo)
			{
				ilSecondShifts++;
			}
		}
	}

	// Sch�ler einer Doppeltour verringern immer Anzahl "M�glicher Freier Tage"
	CString olStat;
	int ilDoppel = 0;
	for (olPos = omDrrToStatusMap.GetStartPosition(); olPos != NULL; )
	{
		omDrrToStatusMap.GetNextAssoc(olPos, olUrno, olStat);
		if (olStat == CString("S"))
		{
			llUrno = atol(olUrno);
			popDrr = ogDrrData.GetDrrByUrno(llUrno);
			if (popDrr != NULL)
			{
				omDates.Lookup(CString(popDrr->Sday),(void*&)ilDayNo);
				if (ilDayNo == ipDayNo)
				{
					ilDoppel++;
				}
			}
		}
	}

	// Wert berechnen
	ilValue = ilStfCount - ilDebitSum - ipOdaCount - ilDiff - ilDoppel + ilSecondShifts;

	return ilValue;
}

//*****************************************************************************************
// Sendet einen BC mit neuen Daten f�r der OFF Days
//*****************************************************************************************

void DutyDebitTabViewer::SendOffDayChangedBC()
{
	// Neuen Wert berechnen
	int ilNewValue;
	
	// Summe berechnen
	ilNewValue = GetFreeShiftValueSum();
	
	// Information zusammenstellen
	omOffDayData.imValue = ilNewValue;

	// Broadcast versenden
	ogDdx.DataChanged((void *)this,OFFDAY_CHANGE, (void *)&omOffDayData);
	
}

//*****************************************************************************************
// Berechnet die Summe der noch verteilbaren Abwesenheiten
//*****************************************************************************************

int DutyDebitTabViewer::GetFreeShiftValueSum()
{
	int ilSum = 0;
	int ilLineNo = 0;
	int ilShift = 0;
	
	DayDebitShift	*prlDayDebitShift;
	
	// Zeile f�r die Offdays bestimmen
	omShiftMap.Lookup("OFFDAYS",(void*&)ilLineNo);
	
	// Alle Tage durchgehen
	for (int i=0; i < imDebitDays; i++)
	{
		prlDayDebitShift=(DayDebitShift*)omDDTV_Shifts[ilLineNo].omDayDebitShift[i];
		ilShift = prlDayDebitShift->ShiftCount;
		// Wert f�r diesen Tag berechnen
		ilSum += GetFreeShiftValue(ilShift,i);
	}
	return ilSum;
}

//*****************************************************************************************
// RDR
//*****************************************************************************************

bool DutyDebitTabViewer::Scroll2ActLine(DRRDATA* popDrr, CString* popKeyDrrChanged/*=0*/)
{
	if (ogCfgData.GetMonitorForWindow(MON_DUTYROSTER_SCROLL) == 1)
	{
		CString olKey("");
		if(popKeyDrrChanged != 0)
		{
			olKey = *popKeyDrrChanged;
		}
		
		int ilLineNo = LookForDrr(popDrr, olKey)/* - 1*/;
		
		// Fixed Lines beachten
		ilLineNo = ilLineNo - imFixLines;
		if (ilLineNo < 0)
			ilLineNo = 0;
		
		pomDynTab->ScrollToPosition(ilLineNo);
	}
	return true; 
}

//*****************************************************************************************
// Aktualisiert die Map mit den Stati eines DRR in der Doppeltour
//*****************************************************************************************

void DutyDebitTabViewer::SetStatMap(DRRDATA* popDrr)
{
	if (IsStfuInStatUrnos(popDrr->Stfu))
	{
		if (HasShiftDemand(popDrr))
		{
			CString olUrno;
			olUrno.Format("%ld",popDrr->Urno);

			if (ogDrrData.HasDoubleTour(popDrr))
			{
				DRSDATA* polDrs = ogDrsData.GetDrsByDrru(popDrr->Urno);
				if (polDrs != NULL)
				{
					if (strcmp(polDrs->Stat, "S") == 0)
					{
						omDrrToStatusMap.SetAt(olUrno,polDrs->Stat);
					}
				}
			}
			/*else
			{
				// Keine Doppeltour, kein Status
				omDrrToStatusMap.SetAt(olUrno,"x");		
			}*/
		}
	}
}

/********************************************************************************************
R�ckgabe: Line Number oder -1 und opKey
********************************************************************************************/
int DutyDebitTabViewer::LookForSdt(SDTDATA* popSdt, CString& opKey)
{
	CString olKey;

	if(mStatMode == SHIFTONLY || !strlen(popSdt->Fctc))
	{
		opKey.Format("%s",popSdt->Bsdc);
	}
	else
	{
		opKey.Format("%s-%s",popSdt->Bsdc,popSdt->Fctc);
	}
	
	int ilLineNo;

	if(omShiftMap.Lookup(opKey,(void*&)ilLineNo))
		return ilLineNo;
	else
		return -1;
}

/********************************************************************************************
R�ckgabe: Line Number oder -1 und opKey
********************************************************************************************/
int DutyDebitTabViewer::LookForDrr(DRRDATA* popDrr, CString& opKey)
{
	int ilLineNo;

	if (opKey.IsEmpty())
	{
		if(mStatMode == SHIFTONLY || !strlen(popDrr->Fctc))
		{
			opKey.Format("%s",popDrr->Scod);
		}
		else 
		{
			opKey.Format("%s-%s",popDrr->Scod,popDrr->Fctc);
		}
	}

	if(omShiftMap.Lookup(opKey,(void*&)ilLineNo))
		return ilLineNo;
	else
		return -1;
}

/*******************************************************************************************

*******************************************************************************************/
void DutyDebitTabViewer::InsertDrrToCodeMap(DRRDATA* popDrr)
{
	CString olKey;
	olKey.Format("%ld",popDrr->Urno);

	CString olMember;

	if(mStatMode == SHIFTONLY || !strlen(popDrr->Fctc))
	{
		olMember.Format("%s",popDrr->Scod);
	}
	else
	{
		olMember.Format("%s-%s", popDrr->Scod, popDrr->Fctc);
	}

	omDrrToCodeMap.SetAt(olKey,olMember);		
}


/********************************************************************************************
R�ckgabe: Line Number
********************************************************************************************/
int DutyDebitTabViewer::LookForGroup(int ipGroupLineNo)
{
	if(ipGroupLineNo < 0)
		return -1;

	int ilLineNo;
	CString olKey = omGroupName[ipGroupLineNo];
	omShiftMap.Lookup(olKey,(void*&)ilLineNo);
	return ilLineNo;
}


void DutyDebitTabViewer::IncreaseAbsenceCount(CString opSday)
{
	int ilDayMap = -1;
	int ilLineNo = -1;
	omDates.Lookup (opSday, (void*&)ilDayMap);
	omShiftMap.Lookup ("OFFDAYS",(void*&)ilLineNo);
	if (ilLineNo > -1 && ilDayMap > -1)
	{
		if (ilLineNo < omDDTV_Shifts.GetSize())
		{
			if (ilDayMap < omDDTV_Shifts[ilLineNo].omDayDebitShift.GetSize())
			{
				((DayDebitShift*)omDDTV_Shifts[ilLineNo].omDayDebitShift[ilDayMap])->ShiftCount++;
			}
		}
	}
}

void DutyDebitTabViewer::DecreaseAbsenceCount(CString opSday)
{
	int ilDayMap = -1;
	int ilLineNo = -1;
	omDates.Lookup (opSday, (void*&)ilDayMap);
	omShiftMap.Lookup ("OFFDAYS",(void*&)ilLineNo);
	if (ilLineNo > -1 && ilDayMap > -1)
	{
		if (ilLineNo < omDDTV_Shifts.GetSize())
		{
			if (ilDayMap < omDDTV_Shifts[ilLineNo].omDayDebitShift.GetSize())
			{
				((DayDebitShift*)omDDTV_Shifts[ilLineNo].omDayDebitShift[ilDayMap])->ShiftCount--;
			}
		}
	}
}

bool DutyDebitTabViewer::IsStfuInStatUrnos(long lpStfu)
{
	bool blRet = false;
	if (prmViewInfo != NULL)
	{
		int ilStfCount = prmViewInfo->oStfStatUrnos.GetSize();
		for (int i = 0; i < ilStfCount; i++)
		{
			if (atol(prmViewInfo->oStfStatUrnos[i]) == lpStfu)
			{
				blRet = true;
				break;
			}
		}
	}
	return blRet;
}

bool DutyDebitTabViewer::IsStfuInExtraUrnos(long lpStfu)
{
	bool blRet = false;
	if (pomParent != NULL)
	{
		CString olStfu;
		olStfu.Format("%ld", lpStfu);
		CObject *polDummy;
		if (pomParent->omExtraUrnoMap.Lookup(olStfu, polDummy))
		{
			blRet = true;
		}
	}
	return blRet;
}

bool DutyDebitTabViewer::UseDrrRecord(CString opRosl, CString opRoss, bool bpIsDelete)
{
	bool blRet = false;

	switch (this->imStatView)
	{
	case STAT_PLAN: // Ist-Werte laut Schichtplan
		if (opRosl == "1")
			blRet = true;
		break;
	case STAT_LONGTIME: // Ist-Werte laut Langzeitdienstplan
		if (opRosl == "L")
			blRet = true;
		break;
	case STAT_DUTY: // Ist-Werte laut Dienstplan 
		if (opRosl == "2")
			blRet = true;
		break;
	case STAT_ALL: // aktive Stufe
		if (bpIsDelete || opRoss == "A")
			blRet = true;
		break;
	default:	// unbekannt oder STAT_CURRENT (alt, aus der ROS/DRS-Zeit)
		break;
	}
	return blRet;
}

void DutyDebitTabViewer::HandleOffDays(DRRDATA *prpDrrData)
{
	CString olKey;
	CString olTmp;

	olKey.Format ("%ld", prpDrrData->Urno);
	if (omDrrToStatusMap.Lookup(olKey, olTmp))
	{
		omDrrToStatusMap.RemoveKey(olKey);
	}

	if (IsStfuInStatUrnos(prpDrrData->Stfu))
	{
		bool blChanged = false;
		ODADATA* polOda = ogOdaData.GetOdaBySdac (prpDrrData->Scod);
		void* prlVoid;

		if (UseDrrRecord(prpDrrData->Rosl, prpDrrData->Ross, false))
		{
			if ((polOda != NULL) && (!omDrrOdaUrnos.Lookup(olKey, prlVoid)))
			{

				IncreaseAbsenceCount(prpDrrData->Sday);
				omDrrOdaUrnos.SetAt(olKey, NULL);
				SendOffDayChangedBC();
				blChanged = true;
			}

			else if ((polOda == NULL) && (omDrrOdaUrnos.Lookup(olKey, prlVoid)))
			{
				DecreaseAbsenceCount(prpDrrData->Sday);
				omDrrOdaUrnos.RemoveKey(olKey);
				SendOffDayChangedBC();
				blChanged = true;
			}
		}
	}
	else if (IsStfuInExtraUrnos(prpDrrData->Stfu))
	{
		if (UseDrrRecord(prpDrrData->Rosl, prpDrrData->Ross, false))
		{
			bool blIsPupil = false;
			bool blWasPupil = false;
			void* prlVoid;
			if (omDrrToStatusMapExtra.Lookup(olKey, prlVoid))
			{
				blWasPupil = true;
				omDrrToStatusMapExtra.RemoveKey(olKey);
			}
			DRSDATA* polDrs = ogDrsData.GetDrsByDrru(prpDrrData->Urno);
			if (polDrs != NULL)
			{
				if (strcmp (polDrs->Stat, "S") == 0)
				{
					blIsPupil = true;
					omDrrToStatusMapExtra.SetAt(olKey, NULL);
				}
			}

			bool blOldDrrCoveredDemand = HadShiftDemand(prpDrrData);
			bool blNewDrrCoversDemand  = HasShiftDemand(prpDrrData);

			if (blWasPupil == true)
				blOldDrrCoveredDemand = false;
			if (blIsPupil == true)
				blNewDrrCoversDemand = false;

			if (blOldDrrCoveredDemand == true && blNewDrrCoversDemand == false)
			{
				IncreaseAbsenceCount(prpDrrData->Sday);
				SendOffDayChangedBC();
			}
			else if (blOldDrrCoveredDemand == false && blNewDrrCoversDemand == true)
			{
				DecreaseAbsenceCount(prpDrrData->Sday);
				SendOffDayChangedBC();
			}
		}
	}
}

bool DutyDebitTabViewer::HasShiftDemand(DRRDATA *prpDrrData)
{
	bool blRet = false;

	CString olKey;

	int ilLineNo = -1;
	int ilDayNo  = -1;

	ilLineNo = LookForDrr(prpDrrData, olKey);
	if (ilLineNo != -1)
	{
		omDates.Lookup (CString(prpDrrData->Sday), (void*&)ilDayNo);
		if (ilDayNo != -1)
		{
			DayDebitShift *prlDayDebitshift;
			prlDayDebitshift=(DayDebitShift*)omDDTV_Shifts[ilLineNo].omDayDebitShift[ilDayNo];
			if (prlDayDebitshift->DebitCount > 0)
			{
				blRet = true;
			}
		}
	}
	return blRet;
}

bool DutyDebitTabViewer::HadShiftDemand(DRRDATA *prpDrrData)
{
	bool blRet = false;

	CString olKey;
	CString olUrno;
	int ilLineNo = -1;
	int ilDayNo  = -1;

	// look for the old record
	olUrno.Format("%ld",prpDrrData->Urno);
	omDrrToCodeMap.Lookup (olUrno, olKey);
	if (olKey.GetLength() > 0)
	{
		ilLineNo = LookForDrr(prpDrrData, olKey);
		if (ilLineNo != -1)
		{
			omDates.Lookup (CString(prpDrrData->Sday), (void*&)ilDayNo);
			if (ilDayNo != -1)
			{
				DayDebitShift *prlDayDebitshift;
				prlDayDebitshift=(DayDebitShift*)omDDTV_Shifts[ilLineNo].omDayDebitShift[ilDayNo];
				if (prlDayDebitshift->DebitCount > 0)
				{
					blRet = true;
				}
			}
		}
	}
	return blRet;
}