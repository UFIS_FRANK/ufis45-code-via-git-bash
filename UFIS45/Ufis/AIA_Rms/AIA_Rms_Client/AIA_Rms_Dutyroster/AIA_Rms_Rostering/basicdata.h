// basicdat.h CCSBasicData class for providing general used methods

#ifndef _BASICDATA
#define _BASICDATA

#include <InitialLoadDlg.h>
#include <ccsglobl.h>
#include <ccsptrarray.h>
#include <ccscedaData.h>
#include <CcsLog.h>
#include <CedaCfgData.h>
#include <CCSTime.h>
#include <CCSDdx.h>
#include <CCSCedaCom.h>
#include <CCSBcHandle.h>
#include <CCSMessageBox.h>
#include <CedaStfData.h>

void DdxRegister(void *vpInstance, int ipDDXType, LPCTSTR DDXName, LPCTSTR InstanceName, DDXCALLBACK pfpCallBack);
CString LoadStg(UINT nID);
void WriteLogFull(LPTSTR fmt,...);
void WriteInRosteringLog(long lpStatus=LOGFILE_FULL, LPCTSTR opLogText=0 );
void WriteInErrlogFile();
bool IsFullLoggingEnabled();
bool IsTraceLoggingEnabled();
bool IsOffLoggingEnabled();
void MergeStringArrays(CStringArray& opDst, CStringArray& opSrc);
void SubstStringArrays(CStringArray& opDst, CStringArray& opSrc, CMapStringToOb* popExtraUrnoMap=0);

CTime COleDateTimeToCTime(COleDateTime opTime);
COleDateTime CTimeToCOleDateTime(CTime opTime);
// mit einem Datums-String ein COleDateTime-Objekt initialisieren
bool YYYYMMDDToOleDateTime(CString opTimeString, COleDateTime &opTime);

int GetItemCount(CString olList, char cpTrenner  = ',' );
int ExtractItemList(CString opSubString, CStringArray *popStrArray, char cpTrenner = ',');
CString GetListItem(CString &opList, int ipPos, bool bpCut, char cpTrenner  = ',');
CString DeleteListItem(CString &opList, CString olItem);
bool GetListItemByField(CString &opDataList, CString &opFieldList, CString &opField, CString &opData);
bool GetListItemByField(CString &opDataList, CString &opFieldList, CString &opField, CTime &opData);
CString SortItemList(CString opSubString, char cpTrenner);
int SplitItemList(CString opString, CStringArray *popStrArray, int ipMaxItem, char cpTrenner = ',');
int Read(CString opObject, CString opWhere = "", CCSPtrArray<RecordSet> *pomBuffer = NULL);

/////////////////////////////////////////////////////////////////////////////
// class BasicData

class CBasicData : public CedaData
{

public:

	CBasicData(void);
	~CBasicData(void);
	
	int imScreenResolutionX;
	int imScreenResolutionY;
	int imMonitorCount;
	long ConfirmTime;
	CString omUserID;

	long GetNextUrno(void);

	bool GetNurnos(int ipNrOfUrnos);

	void SetWorkstationName(CString opWsName);
	CString GetWorkstationName();

	//handling of UTC-Times
	void SetUtcDifferenceFromApttab(void);
	void ConvertDateToLocal(COleDateTime &ropDate);
	void ConvertDateToUtc(COleDateTime &ropDate);
	COleDateTime GetLocalFromUtc(COleDateTime opDate);
	COleDateTime GetUtcFromLocal(COleDateTime opDate);

public:
	static CString GetFormatedEmployeeName(int ipEName, STFDATA *popStfData, char* pcpPrefix, char* pcpSuffix, NameConfigurationDlg* prpNameConfigDlg = NULL);
	static CString GetFormatedEmployeeName(int ipEName, const STFDATA *popStfData, char* pcpPrefix, char* pcpSuffix);
	static void TrimLeft(char *s);
	static void TrimRight(char *s);

private:
	COleDateTimeSpan	omLocalDiff1;
	COleDateTimeSpan	omLocalDiff2;
	COleDateTime		omTich;

	CString omWorkstationName; 

	CDWordArray omUrnos;
	int imUtcDifference;
};


#endif
