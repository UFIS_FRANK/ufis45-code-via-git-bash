#ifndef AFX_DUTYBSDDLG_H__972380B1_2AC9_11D2_8014_004095434A85__INCLUDED_
#define AFX_DUTYBSDDLG_H__972380B1_2AC9_11D2_8014_004095434A85__INCLUDED_

// DutyBsdDlg.h : Header-Datei
//
#include <stdafx.h>
/*#include "resource.h"
#include <DutyRoster_View.h>
#include <ShiftRoster_View.h>
#include <Table.h>
//#include "CedaBsdData.h"
#include <CedaOdaData.h>
#include <basicdata.h>
#include <CCSDragDropCtrl.h>*/

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld DutyBsdDlg 
// Der Dialog zeigt alle Schichten und Abwesenheiten an. Die Schicht-
// bezeichner lassen sich per Drag and Drop in die Fenster der aufrufenden
// Klassen ziehen (sofern diese Drag and Drop unterst�tzen).

class DutyBsdDlg : public CDialog
{
// Konstruktion
public:
	DutyBsdDlg(CWnd* pParent = NULL);   // Standardkonstruktor
	~DutyBsdDlg();

	// Parent Window 
	CWnd *pomParent;

private:

public:

// Dialogfelddaten
	//{{AFX_DATA(DutyBsdDlg)
	enum { IDD = IDD_DUTY_BSD_DLG };
		// HINWEIS: Der Klassen-Assistent f�gt hier Datenelemente ein
	//}}AFX_DATA


// �berschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktions�berschreibungen
	//{{AFX_VIRTUAL(DutyBsdDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(DutyBsdDlg)
	virtual BOOL OnInitDialog();
	afx_msg LONG OnDragBegin(UINT wParam, LONG lParam);
	virtual void OnOK();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	CString Format_Table(CString opTable,void *prpData);
	void InternalCancel();
	
	// Arrays, die die Strings halten. Die Strings repr�sentieren
	// die verf�gbaren Datens�tze f�r Basisschichten und Abwesenheiten
	CCSPtrArray<BSDDATA>  omBsdLines;
	CCSPtrArray<ODADATA>  omOdaLines;
	// mit Hilfe der CTable-Objekte werden die Inhalte der Arrays f�r
	// Basisschichten und Abwesenheiten dargestellt und Events wie 
	// Mausklicks behandelt (CTable h�lt ein Datenobjekt CTableListBox).
	CTable *pomBsdTable;
	CTable *pomOdaTable;
	// Objekt f�r D'n'D-Funktionalit�t
	CCSDragDropCtrl omDragDropObject;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio f�gt zus�tzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_DUTYBSDDLG_H__972380B1_2AC9_11D2_8014_004095434A85__INCLUDED_
