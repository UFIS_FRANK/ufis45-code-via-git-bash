// cedadatahelper.cpp - Klasse zum Konvertieren von Datenformaten
//
 
#include <stdafx.h>

//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Implementation der Klasse CedaDataHelper
//************************************************************************************************************************************************
//************************************************************************************************************************************************

//************************************************************************************************************************************************
// DateStringToOleDateTime: erzeugt aus einem String im Format YYYYMMDD ein 
//	COleDateTime-Obkjekt.
// R�ckgabe:	true	-> <opDate> g�ltig, <opTime> initialisiert
//				false	-> Fehler
//************************************************************************************************************************************************

bool CedaDataHelper::DateStringToOleDateTime(CString opDate, COleDateTime &opTime)
{
CCS_TRY;
	if ((opDate == "") || (opDate.GetLength() < 8) || (opDate.SpanExcluding("0123456789") != "")){
		// <opDate> ung�ltig -> terminieren
		return false;
	}
	
	// COleDateTime-Objekt aus <opDate> erzeugen...
	COleDateTime olTime(atoi(opDate.Left(4).GetBuffer(0)), // Jahr (YYYY)
						atoi(opDate.Mid(4,2).GetBuffer(0)), // Monat (MM)
						atoi(opDate.Mid(6,2).GetBuffer(0)), // Tag (DD)
						0,0,0);	// Stunde, Minute, Sekunde
	// ...und kopieren
	opTime = olTime;
	return true;
CCS_CATCH_ALL;
return false;
}

/************************************************************************************************************************************************
bda 2001.08.30 expanded with opHolidayKeyMap
IsWeekendOrHoliday: // pr�fen, ob der Tag in <opDate> ein Wochenendtag oder Feiertag ist
************************************************************************************************************************************************/

bool CedaDataHelper::IsWeekendOrHoliday(COleDateTime opDate, CMapStringToPtr &opHolidayKeyMap /*Map for Holidays*/, long lpSurn)
{
CCS_TRY;
	// Wochentag pr�fen
	switch(opDate.GetDayOfWeek())
	{
	case 1: // Sonntag
	case 7: // Sonnabend
		return true;
	default: // anderer Tag
		// es kann ein Feiertag sein
		{
			//Is the day a holiday? (defined in HOLTAB)
			if(ogSreData.IsRegularEmployee(lpSurn, opDate))
			{
				// is true only for regular employee
				CString olTmp;
				void  *prlVoid = NULL;
				olTmp = opDate.Format("%Y%m%d000000");
				if(opHolidayKeyMap.Lookup(olTmp,(void *&)prlVoid) == TRUE)
				{
					return true;
				}
			}
		}
		return false;
	}
CCS_CATCH_ALL;
return false;
}

//************************************************************************************************************************************************
// HourMinStringToOleDateTimeSpan: erzeugt aus einem String im Format 'HHMM' oder
//	'HHMMSS' ein COleDateTimeSpan-Obkjekt.
// R�ckgabe:	true	-> String g�ltig, <opTimeSpan> wurde initialisiert
//				false	-> Fehler
//************************************************************************************************************************************************

bool CedaDataHelper::HourMinStringToOleDateTimeSpan(CString opHourMin, COleDateTimeSpan &opTimeSpan)
{
CCS_TRY;
	// String pr�fen
	if ((opHourMin == "") || (opHourMin.SpanExcluding("0123456789") != "") ||
		((opHourMin.GetLength() != 4) && (opHourMin.GetLength() != 6))){
		// <opHourMin> ung�ltig -> terminieren
		return false;
	}
	
	if (atoi(opHourMin.Left(2).GetBuffer(0)) == 24)
	{
		//die Zeit "24 Uhr" bzw. "2400" muss in 235959 gewandelt werden
		COleDateTimeSpan olTimeSpan(0,		// Tage = 0
									23,		// Stunde (HH)
									59,		// Minuten (MM)
									59);	// Sekunden
		// ...und kopieren
		opTimeSpan = olTimeSpan;
	}
	else
	{
		// gibt es Sekunden?
		int ilSec = 0;
		if (opHourMin.GetLength() == 6) atoi(opHourMin.Right(2).GetBuffer(0)); // ja -> ermitteln

		// COleDateTimeSpan-Objekt aus String erzeugen...
		COleDateTimeSpan olTimeSpan(0, // Tage = 0
									atoi(opHourMin.Left(2).GetBuffer(0)), // Stunde (HH)
									atoi(opHourMin.Mid(2,2).GetBuffer(0)), // Minuten (MM)
									ilSec);	// Sekunden
		// ...und kopieren
		opTimeSpan = olTimeSpan;
	}
	return true;
CCS_CATCH_ALL;
return false;
}

//************************************************************************************************************************************************
// DateTimeStringToOleDateTime: erzeugt aus einem Zeit-String im Format 'YYYYMMDDHHMMSS' 
//	ein Objekt vom Typ COleDateTime.
// R�ckgabe:	true	-> String g�ltig, <opTime> wurde initialisiert
//				false	-> Fehler
//************************************************************************************************************************************************

bool CedaDataHelper::DateTimeStringToOleDateTime(CString opDateTime, COleDateTime &opTime)
{

CCS_TRY;
	// pr�fen, ob die L�nge stimmt
	if (opDateTime.GetLength() < 12) return false; // falsche L�nge

	// Datum und Zeit ermitteln
	COleDateTime olDate;
	COleDateTimeSpan olTimeSpan;
	if (!DateStringToOleDateTime(opDateTime.Left(8),olDate) ||
		!HourMinStringToOleDateTimeSpan(opDateTime.Mid(8),olTimeSpan))
	{
		// Fehler beim Konvertieren -> terminieren
		return false;
	}

	// Datum und Zeit addieren und kopieren
	opTime = olDate + olTimeSpan;
	return true;
CCS_CATCH_ALL;
return false;

}

//************************************************************************************************************************************************
// DeleteExtraChars: L�scht alle ":" und "." aus einen String
// R�ckgabe:	gereinigter String
//************************************************************************************************************************************************

void CedaDataHelper::DeleteExtraChars(CString& opString)
{
CCS_TRY;
	opString.Replace(":","");
	opString.Replace(".","");
CCS_CATCH_ALL;
}

//*******************************************************************************************************
// GetDaysOfMonth(): ermittelt, wieviel Tage der Monat im Datum <opDays> hat
//	(28 min. z.B. Februar bis 31 max.).
// R�ckgabe:	die Anzahl der Tage des Monats.
//*******************************************************************************************************

int CedaDataHelper::GetDaysOfMonth(COleDateTime opDay)
{
CCS_TRY;
	if(opDay.GetStatus() != COleDateTime::valid)
		return 0;

	COleDateTime olMonth = COleDateTime(opDay.GetYear(), opDay.GetMonth(),1,0,0,0);
	bool blNextMonth = false;
	int ilDays = 28;
	while(!blNextMonth)
	{
		COleDateTime olTmpDay = olMonth + COleDateTimeSpan(ilDays,0,0,0);
		if(olTmpDay.GetMonth() != opDay.GetMonth())
			blNextMonth = true;
		else
			ilDays++;
	}
	if(ilDays < 0 || ilDays > 31)
		ilDays = 0;
	return ilDays;
CCS_CATCH_ALL;
return 0;
}

//*******************************************************************************************************
// GetMaxPeriod(): vergleicht zwei Zetr�ume miteinander und ermittelt 
//	die maximal resultierende Zeitspanne, wenn gew�nscht (<opMaxPeriodFrom> und
//	<opMaxPeriodTo> ungleich NULL).
// R�ckgabe:	
//	CHECK_P2_INNER_P1				->	die zweite Zeitspanne liegt innerhalb der ersten 
//										Zeitspanne
//	CHECK_P2_OUTER_P1				->	die erste Zeitspanne liegt innerhalb der zweiten 
//										Zeitspanne
//	CHECK_P2_INNER_ABOVE_P1			->	die zweite Zeitspanne beginnt innerhalb der
//										der ersten Zeitspanne, �berschreitet diese aber
//	CHECK_P2_ABOVE_P1				->	die zweite Zeitspanne liegt ausserhalb und �ber
//										der ersten Zeitspanne										
//	CHECK_P2_INNER_UNDERNEATH_P1	->	die zweite Zeitspanne beginnt innerhalb der
//										der ersten Zeitspanne, unterschreitet diese aber
//	CHECK_P2_UNDERNEATH_P1			->	die zweite Zeitspanne liegt ausserhalb und unter
//										der ersten Zeitspanne	
//	CHECK_INVALID_DATE				->	einer der Parameter ist ung�ltig oder Exception									
//*******************************************************************************************************

int CedaDataHelper::GetMaxPeriod(COleDateTime opP1From, COleDateTime opP1To, 
								 COleDateTime opP2From, COleDateTime opP2To, 
								 COleDateTime *popMaxPeriodFrom /*= NULL*/, 
								 COleDateTime *popMaxPeriodTo /*= NULL*/)
{
CCS_TRY;
	if((opP1From.GetStatus() != COleDateTime::valid) || (opP1To.GetStatus() != COleDateTime::valid) || 
	   (opP2From.GetStatus() != COleDateTime::valid) || (opP2To.GetStatus() != COleDateTime::valid) ||
	   (opP1From > opP1To) || (opP2From > opP2To))
		return CHECK_INVALID_DATE;
	
	// max. Zeitraum
	COleDateTime olMaxFrom = opP2From;
	COleDateTime olMaxTo = opP2To;

	// R�ckgabewert
	int ilReturnCode = 0;

	// Zeitr�ume miteinander vergleichen, Ergebnisse bitweise kodieren,
	// max. Ladezeitraum ermitteln, wenn m�glich
	// Startzeitpunkt
	if (opP1From <= opP2From)
	{
		ilReturnCode += 1;
		olMaxFrom = opP1From;
		if (opP2From >= opP1To) 
			ilReturnCode += 4;
	}

	if (opP1To >= opP2To)
	{
		ilReturnCode += 2;
		olMaxTo = opP1To;
		if (opP2To <= opP1From)
			ilReturnCode += 8;
	}

	// max. Zeitraum speichern, wenn m�glich
	if (popMaxPeriodFrom != NULL) *popMaxPeriodFrom = olMaxFrom;
	if (popMaxPeriodTo != NULL) *popMaxPeriodTo = olMaxTo;

	// jetzt kann <ilReturnCode> einen der in CedaDataHelper.h definierten Werte haben
	return ilReturnCode;
CCS_CATCH_ALL;
return CHECK_INVALID_DATE;
}

//*************************************************************************************
// Pr�fen ob der Text ein 1. Schicht 2. Abwesenheit 3. Wunschcode ist
// Pr�ft ob es sich um einen "freien" Typ handelt.
// Falls Code nicht gefunden wurde wird FALSE zur�ckgegeben.
//
// in "ipCodeType" steht der Code-Typ
//*************************************************************************************

bool CedaDataHelper::GetCodeType(CString opText, int& ipCodeType, bool& bpIsFreeType)
{
	bool blRet		= false;
	bpIsFreeType	= false;

	//Pr�fen, ob Schicht-Code
	if (ogBsdData.IsBsdCode (opText))
	{
		ipCodeType	= CODE_IS_BSD;
		blRet		= true;
	}
	else
	{
		//Pr�fen, ob Abwesenheits-Code
		ODADATA* polOdaData = ogOdaData.GetOdaBySdac (opText);
		if (polOdaData != NULL)
		{
			ipCodeType	= CODE_IS_ODA;
			blRet		= true;
			if (strcmp(polOdaData->Free,"x") == 0)
			{
				bpIsFreeType = true;
			}
		}
		else
		{
			//Pr�fen, ob Wunsch-Code
			if (ogWisData.CheckCode (opText))
			{
				ipCodeType = CODE_IS_WIS;
				return true;
			}
		}
	}
	return blRet;
}

//*************************************************************************************
// Pr�fen ob der Text ein 1. Schicht 2. Abwesenheit 3. Wunschcode ist
//*************************************************************************************

CString CedaDataHelper::GetShiftCodeByUrno(long lpUrno)
{
	CString olRet = "";

	//Pr�fen, ob "normaler" Schicht-Code
	BSDDATA* polBsdData = ogBsdData.GetBsdByUrno (lpUrno);
	if (polBsdData != NULL)
	{
		olRet = CString (polBsdData->Bsdc);
	}
	else
	{
		//Pr�fen, ob Abwesenheits-Code
		ODADATA* polOdaData = ogOdaData.GetOdaByUrno (lpUrno);
		if (polOdaData != NULL)
		{
			olRet = CString (polOdaData->Sdac);
		}
		else
		{
			//Pr�fen, ob Wunsch-Code
			WISDATA* polWisData = ogWisData.GetWisByUrno (lpUrno);
			if (polWisData != NULL)
			{
				olRet = CString (polWisData->Wisc);
			}
		}
	}
	return olRet;
}

/**************************************************************************************
f�r BSD wird gepr�ft ob die Funktionen von Schicht und Mitarbeiter zusammenpassen
f�r ODA - ob diese Abwesenheit f�r den Vertragscode des Mitarbeiters zul�ssig ist
bpCheckMainFuncOnly - true - Schichtzuordnung nur f�r Stammfunktionen pr�fen, sonst f�r alle
- CString opNewFctc - wenn nicht leer, dann mu� diese Funktion �bernommen werden 
(kommt aus Coverage/Statistik per DragDrop)
**************************************************************************************/
bool CedaDataHelper::CheckFuncOrgContract(long lpStfu, CString opSday,long lpNewShiftUrno, CString opNewFctc, bool bpCheckMainFuncOnly)
{
	CTime	olTimeNow = CTime::GetCurrentTime();
	CString olStringNow = olTimeNow.Format("%Y%m%d%H%M%S");

	COleDateTime olSday;
	CedaDataHelper::DateStringToOleDateTime (opSday, olSday);
	
	BSDDATA* polBsd = ogBsdData.GetBsdByUrno(lpNewShiftUrno);
	if (polBsd != NULL)
	{
		CString olTestFunction = ogCCSParam.GetParamValue(ogAppl,"ID_TEST_FUNCTION",&olStringNow);
		if(olTestFunction == "Y")
		{
			if(opNewFctc.IsEmpty())
			{
				// nur wenn kein Funktionscode mitgegeben ist, setzen wir einen BSD-Code ein
				opNewFctc = polBsd->Fctc;
			}
			// Bei keinem Code ist es immer OK !
			if (!opNewFctc.IsEmpty())
			{
				// Codes vergleichen
				if (!ogSpfData.ComparePfcBySurnWithTime(opNewFctc,lpStfu,olSday,bpCheckMainFuncOnly))
				{
					// Funktionen sind nicht gleich: keine der Funktionen des Mitarbeiters stimmt mit opNewFctc �berein
					CString olName;
					CString olTime;
					STFDATA *prlStf;
					
					prlStf = ogStfData.GetStfByUrno(lpStfu);
					
					if (prlStf > NULL)
					{
						CString olVName(prlStf->Finm);
						olName.Format("%s.%s",olVName.Left(1),prlStf->Lanm);
					}
					olTime = olSday.Format("%d.%m.%y");
					
					CCSPtrArray<SPFDATA> olSpfData;
					int ilFuncNum = ogSpfData.GetSpfArrayBySurnWithTime(olSpfData,lpStfu,olSday,bpCheckMainFuncOnly);
					CString olFunctions = ogSpfData.FormatFuncs(olSpfData,1);
					
					CString olMessage;
					
					if(!ilFuncNum)
					{
						// Abfrage ob gew�nscht
						// %s  hat am  %s  keine regul�re Funktion.\n
						// Die Schicht  %s  hat die Funktion  %s\n
						// \n
						// Soll die Schicht dennoch zugewiesen werden?*INTXT*
						olMessage.Format(LoadStg(IDS_STRING1657),olName,olTime,polBsd->Bsdc,opNewFctc);
						if (AfxMessageBox(olMessage,MB_ICONQUESTION | MB_YESNO) != IDYES)
							// -> Nein, keine �nderung durchf�hren
							return false;
					}
					else if(ilFuncNum == 1)
					{
						// Abfrage ob gew�nscht
						// %s  hat am  %s  regul�re Funktion  %s.\n
						// Die Schicht  %s  hat die Funktion  %s\n
						// \n
						// Soll die Schicht dennoch zugewiesen werden?*INTXT*
						olMessage.Format(LoadStg(IDS_STRING1655),olName,olTime,olFunctions,polBsd->Bsdc,opNewFctc);
						if (AfxMessageBox(olMessage,MB_ICONQUESTION | MB_YESNO) != IDYES)
							// -> Nein, keine �nderung durchf�hren
							return false;
					}
					else
					{
						// Abfrage ob gew�nscht
						// %s  hat am  %s  regul�re Funktionen  %s.\n
						// Die Schicht  %s  hat die Funktion  %s\n
						// \n
						// Soll die Schicht dennoch zugewiesen werden?*INTXT*
						olMessage.Format(LoadStg(IDS_STRING1660),olName,olTime,olFunctions,polBsd->Bsdc,opNewFctc);
						if (AfxMessageBox(olMessage,MB_ICONQUESTION | MB_YESNO) != IDYES)
							// -> Nein, keine �nderung durchf�hren
							return false;
					}
				}
			}
		}
	}
	else
	{
		// Soll die Abwesenheit nach dem Vertragscode kontrolliert werden? Y/N
		CString olTestFunction = ogCCSParam.GetParamValue(ogAppl,"ID_TEST_ABSENCE",&olStringNow);
		if(olTestFunction == "Y")
		{
			// ODA?
			ODADATA* polOda = ogOdaData.GetOdaByUrno(lpNewShiftUrno);
			if(polOda != 0)
			{
				// wir pr�fen, ob dieser MA diese Abwesenheit bekommen darf
				
				// Vertragsliste SCODATA holen
				CCSPtrArray<SCODATA> olScoData;
				ogScoData.GetScoListBySurnWithTime(lpStfu, olSday, &olScoData);
				
				bool blEnabled = false;
				OACDATA* polOac;
				SCODATA* polSco = 0;
				CString olCode;
				CString olSdac(polOda->Sdac);
				for(int index=0; index<olScoData.GetSize(); index++)
				{
					// alle Vertr�ge durchgehen
					polSco = &olScoData[index];
					if(!polSco) continue;
					
					olCode = polSco->Code;
					polOac = ogOacData.GetOacByKey(olCode, olSdac);
					if(polOac != 0)
					{
						// gefunden, weiter brauchen wir nicht zu suchen
						blEnabled = true;
						break;
					}
				}
				
				if(blEnabled == false)
				{
					// kein OAC gefunden oder der OAC-Code stimmt mit der neuen Abwesenheit nicht �berein
					// wir m�ssen den Benutzer informieren
					CString olName;
					CString olTimeString;
					STFDATA *prlStf;
					
					prlStf = ogStfData.GetStfByUrno(lpStfu);
					
					if (prlStf > NULL)
					{
						CString olVName(prlStf->Finm);
						olName.Format("%s.%s",olVName.Left(1),prlStf->Lanm);
					}
					olTimeString = olSday.Format("%d.%m.%y");
					
					CString olMessage;
					
					// %s am %s:\n\n
					// Die Abwesenheit %s ist nach dem aktuellen Vertragscode %s nicht zul�ssig!*INTXT*
					olMessage.Format(LoadStg(IDS_STRING1658),olName,olTimeString,polOda->Sdac,(polSco == 0 ? "" : polSco->Code));
					AfxMessageBox(olMessage,MB_ICONSTOP | MB_OK);
					return false;
				}
			}
		}
	}
	return true;
}