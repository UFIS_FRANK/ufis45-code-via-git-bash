// PfcSelectDlg.cpp : implementation file
//

#include <stdafx.h>
#include <CCSEdit.h>
#include <PfcSelectDlg.h>
#include <CedaBasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// PfcSelectDlg dialog


PfcSelectDlg::PfcSelectDlg(CWnd* pParent /*=NULL*/, CUIntArray *popPfcUrnos)
	: CDialog(PfcSelectDlg::IDD, pParent)
{

	for (int i = 0; i < popPfcUrnos->GetSize(); i++)
	{
		omPfcUrnos.Add((*popPfcUrnos)[i]);
	}
	//{{AFX_DATA_INIT(PfcSelectDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

}

PfcSelectDlg::~PfcSelectDlg()
{
	omPfcData.DeleteAll();
	omPfcUrnos.RemoveAll();
}


void PfcSelectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PfcSelectDlg)
	DDX_Control(pDX, IDC_PFC_LIST, m_CL_List);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(PfcSelectDlg, CDialog)
	//{{AFX_MSG_MAP(PfcSelectDlg)
	ON_LBN_DBLCLK(IDC_PFC_LIST, OnBDblClickPfc)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// PfcSelectDlg message handlers

void PfcSelectDlg::OnOK() 
{
	int ilSel = m_CL_List.GetCurSel();

	if (ilSel ==  LB_ERR)
	{
		MessageBox(LoadStg(IDS_STRING63447), LoadStg(ST_HINWEIS));	//Keine Funktion selektiert!*INTXT*
		return;
	}

	PFCDATA *prlPfc = (PFCDATA *)m_CL_List.GetItemDataPtr(ilSel);
	if (prlPfc != NULL)
	{
		omSelPfcCode = CString(prlPfc->Fctc);
	}

	CDialog::OnOK();
}

BOOL PfcSelectDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetWindowText(LoadStg(IDS_STRING63484)); //Auswahl Funktion*INTXT*
	
	m_CL_List.SetFont(&ogCourier_Regular_8);	

	PFCDATA *prlPfc;

	CWnd *polWnd = GetDlgItem(IDCANCEL);

	if (polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING999));
	}

	polWnd = GetDlgItem(IDOK);

	if (polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING998));
	}

	for (int  i = 0; i < omPfcUrnos.GetSize(); i++)
	{
		prlPfc = ogPfcData.GetPfcByUrno(omPfcUrnos[i]);

		if (prlPfc != NULL)
		{
			omPfcData.New(*prlPfc);

			char pclLine[250] = "";

			sprintf(pclLine, "%-9s  %s", prlPfc->Fctc, prlPfc->Fctn);
			int ilIndex = m_CL_List.AddString(pclLine);

			if (ilIndex != LB_ERR)
			{
				m_CL_List.SetItemDataPtr(ilIndex, (void *)prlPfc);
			}	
		}
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void PfcSelectDlg::OnBDblClickPfc()
{
	OnOK();
}