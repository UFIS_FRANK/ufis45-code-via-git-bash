// RosteringView.h : interface of the CRosteringView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_ROSTERINGVIEW_H__F66E6B6E_5BB3_11D3_8EF6_00001C034EA0__INCLUDED_)
#define AFX_ROSTERINGVIEW_H__F66E6B6E_5BB3_11D3_8EF6_00001C034EA0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


//****************************************************************************
// Klasse CRosteringView
//****************************************************************************

class CRosteringView : public CView
{
protected: // create from serialization only
	CRosteringView();
	DECLARE_DYNCREATE(CRosteringView)

// Attributes
public:
	CRosteringDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRosteringView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CRosteringView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CRosteringView)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in RosteringView.cpp
inline CRosteringDoc* CRosteringView::GetDocument()
   { return (CRosteringDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ROSTERINGVIEW_H__F66E6B6E_5BB3_11D3_8EF6_00001C034EA0__INCLUDED_)
