#if !defined(AFX_PLAN_INFO_H__97731695_DD42_11D3_8FEF_00500454BF3F__INCLUDED_)
#define AFX_PLAN_INFO_H__97731695_DD42_11D3_8FEF_00500454BF3F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Plan_Info.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CPlan_Info 

class CPlan_Info : public CDialog
{
// Konstruktion
public:
	CPlan_Info(CWnd* pParent = NULL);   // Standardkonstruktor

// Dialogfelddaten
	//{{AFX_DATA(CPlan_Info)
	enum { IDD = IDD_PLAN_INFO };
		// HINWEIS: Der Klassen-Assistent fügt hier Datenelemente ein
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CPlan_Info)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:
	
	// ToolTips
	CToolTipCtrl  m_ToolTip;

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CPlan_Info)
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	// Bereitet Tooltipps vor
	void SetToolTip();
	// Position und Ausmasse eine controls
	CRect GetDlgItemRect(int ipID);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_PLAN_INFO_H__97731695_DD42_11D3_8FEF_00500454BF3F__INCLUDED_
