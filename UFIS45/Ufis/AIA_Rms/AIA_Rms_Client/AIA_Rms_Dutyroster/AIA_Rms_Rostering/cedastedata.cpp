// CedaSteData.cpp
 
#include <stdafx.h>

CedaSteData ogSteData;

void ProcessSteCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

static int CompareTimes(const STEDATA **e1, const STEDATA **e2);
static int CompareSurnAndTimes(const STEDATA **e1, const STEDATA **e2);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaSteData::CedaSteData() : CedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for STEDataStruct
	BEGIN_CEDARECINFO(STEDATA,STEDataRecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_LONG		(Surn,"SURN")
		FIELD_CHAR_TRIM	(Code,"CODE")
		FIELD_OLEDATE	(Vpfr,"VPFR")
		FIELD_OLEDATE	(Vpto,"VPTO")
	END_CEDARECINFO //(STEDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(STEDataRecInfo)/sizeof(STEDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&STEDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"STE");
	strcat(pcmTableName,pcgTableExt);
	strcpy(pcmListOfFields,"URNO,SURN,CODE,VPFR,VPTO");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaSteData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaSteData::Register(void)
{
	DdxRegister((void *)this,BC_STE_CHANGE,	"STEDATA", "Ste-changed",	ProcessSteCf);
	DdxRegister((void *)this,BC_STE_NEW,	"STEDATA", "Ste-new",		ProcessSteCf);
	DdxRegister((void *)this,BC_STE_DELETE,	"STEDATA", "Ste-deleted",	ProcessSteCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaSteData::~CedaSteData(void)
{
	TRACE("CedaSteData::~CedaSteData called\n");
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaSteData::ClearAll(bool bpWithRegistration)
{
	TRACE("CedaSteData::ClearAll called\n");
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaSteData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(pspWhere == NULL)
	{	
		ilRc = CedaAction2("RT");
	}
	else
	{
		ilRc = CedaAction2("RT", pspWhere);
	}
	if (ilRc != true)
	{
		TRACE("Read-Ste: Ceda-Error %d \n",ilRc);
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
	{
		STEDATA *prlSte = new STEDATA;
		if ((ilRc = GetFirstBufferRecord2(prlSte)) == true)
		{
			// BDA: Vpfr & Vpto k�nnen auch mal null sein, es ist falsch!
			// ARE: Vpto kann auch mal null sein, es ist OK! aber auf keinen Fall ung�ltig
			if(prlSte->Vpfr.GetStatus() != COleDateTime::valid || prlSte->Vpto.GetStatus() == COleDateTime::invalid)
			{
				if(IsTraceLoggingEnabled())
				{
					GetDefectDataString(ogRosteringLogText, (void*)prlSte);
					WriteInRosteringLog(LOGFILE_TRACE);
				}
				delete prlSte;
				continue;
			}
			
			omData.Add(prlSte);//Update omData
			omUrnoMap.SetAt((void *)prlSte->Urno,prlSte);
#ifdef TRACE_FULL_DATA
			// Datensatz OK, loggen if FULL
			ogRosteringLogText.Format(" read %8.8lu %s OK:  ", ilCountRecord, pcmTableName);
			GetDataFormatted(ogRosteringLogText, prlSte);
			WriteLogFull("");
#endif TRACE_FULL_DATA
		}
		else
		{
			delete prlSte;
		}
	}
	TRACE("Read-Ste: %d gelesen\n",ilCountRecord-1);

	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  read %d records from table %s",ilCountRecord-1, pcmTableName);
		WriteInRosteringLog();
	}
	

    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaSteData::Insert(STEDATA *prpSte)
{
	prpSte->IsChanged = DATA_NEW;
	if(Save(prpSte) == false) return false; //Update Database
	InsertInternal(prpSte);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaSteData::InsertInternal(STEDATA *prpSte)
{
	ogDdx.DataChanged((void *)this, STE_NEW,(void *)prpSte ); //Update Viewer
	omData.Add(prpSte);//Update omData
	omUrnoMap.SetAt((void *)prpSte->Urno,prpSte);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaSteData::Delete(long lpUrno)
{
	STEDATA *prlSte = GetSteByUrno(lpUrno);
	if (prlSte != NULL)
	{
		prlSte->IsChanged = DATA_DELETED;
		if(Save(prlSte) == false) return false; //Update Database
		DeleteInternal(prlSte);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaSteData::DeleteInternal(STEDATA *prpSte)
{
	ogDdx.DataChanged((void *)this,STE_DELETE,(void *)prpSte); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpSte->Urno);
	int ilSteCount = omData.GetSize();
	for (int ilCountRecord = 0; ilCountRecord < ilSteCount; ilCountRecord++)
	{
		if (omData[ilCountRecord].Urno == prpSte->Urno)
		{
			omData.DeleteAt(ilCountRecord);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaSteData::Update(STEDATA *prpSte)
{
	if (GetSteByUrno(prpSte->Urno) != NULL)
	{
		if (prpSte->IsChanged == DATA_UNCHANGED)
		{
			prpSte->IsChanged = DATA_CHANGED;
		}
		if(Save(prpSte) == false) return false; //Update Database
		UpdateInternal(prpSte);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaSteData::UpdateInternal(STEDATA *prpSte)
{
	STEDATA *prlSte = GetSteByUrno(prpSte->Urno);
	if (prlSte != NULL)
	{
		*prlSte = *prpSte; //Update omData
		ogDdx.DataChanged((void *)this,STE_CHANGE,(void *)prlSte); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

STEDATA *CedaSteData::GetSteByUrno(long lpUrno)
{
	STEDATA  *prlSte;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlSte) == TRUE)
	{
		return prlSte;
	}
	return NULL;
}

//---------------------------------------------------------------------------------------------------------

void CedaSteData::GetSteBySurnWithTime(long lpSurn,CTime opStart,CTime opEnd,CCSPtrArray<STEDATA> *popSteData)
{
	STEDATA  *prlSte;
	COleDateTime olStart,olEnd;

	popSteData->DeleteAll();
	
	CCSPtrArray<STEDATA> olSteData;
	POSITION rlPos;
	for ( rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSte);
		if((prlSte->Surn == lpSurn))
		{
			olSteData.Add(prlSte); 
		}
	}
	int ilSize = olSteData.GetSize();
	if(ilSize > 0)
	{
		olSteData.Sort(CompareTimes);
		
		olStart.SetDateTime(opStart.GetYear(),opStart.GetMonth(),opStart.GetDay(),opStart.GetHour(),opStart.GetMinute(),opStart.GetSecond());
		olEnd.SetDateTime(opEnd.GetYear(),opEnd.GetMonth(),opEnd.GetDay(),opEnd.GetHour(),opEnd.GetMinute(),opEnd.GetSecond());
		STEDATA *prlPrevSte=NULL;
		for(int i=ilSize;--i>=0;)
		{
			prlSte = &olSteData[i];
			
			if(prlSte->Vpfr<olEnd)
			{
				switch(prlSte->Vpto.GetStatus())
				{
				case COleDateTime::valid:
					if(prlSte->Vpto>olStart)
					{
						popSteData->Add(prlSte);
					}
					else
					{
						i=0;
					}
					break;
				case COleDateTime::null:
					if(prlPrevSte!=NULL)
					{
						if(prlPrevSte->Vpfr>olStart)
						{
							popSteData->Add(prlSte);
						}
						else
						{
							i=0;
						}
					}
					else
					{
						popSteData->Add(prlSte);
					}
					break;
				default:
					break;
				}
			}
			prlPrevSte = prlSte;
		}
	}
}

//---------------------------------------------------------------------------------------

CString CedaSteData::GetTeaBySurnWithTime(long lpSurn, COleDateTime opDate)
{
	CString olTeaCode;

	CTime olDay(opDate.GetYear(),opDate.GetMonth(),opDate.GetDay(),0,0,0);
	CCSPtrArray<STEDATA> olSteData;

	GetSteBySurnWithTime(lpSurn, olDay, olDay, &olSteData);
	int ilSteSize = olSteData.GetSize();

	for(int i=0; i<ilSteSize; i++)
	{
		if(olSteData[i].Vpfr.GetStatus() == COleDateTime::valid)
		{
			if(olSteData[i].Vpfr <= opDate)
			{
				switch(olSteData[i].Vpto.GetStatus())
				{
				case COleDateTime::valid:
					if(olSteData[i].Vpto > opDate)
					{
						olTeaCode = olSteData[i].Code;
					}
					else
					{
						olTeaCode = "";

					}
					break;
				default:
						olTeaCode = olSteData[i].Code;
					break;
				}
			}
		}
	}
	olSteData.RemoveAll();
	return olTeaCode;
}

//---------------------------------------------------------------------------------------------------------

CString CedaSteData::GetSteByTeaWithTime(CStringArray *popTea, COleDateTime opStart, COleDateTime opEnd, CCSPtrArray<STEDATA> *popSteData)
{
	popSteData->DeleteAll();
	CString olUrnos = "";

	if(popTea->GetSize() > 0 && opStart.GetStatus() == COleDateTime::valid && opEnd.GetStatus() == COleDateTime::valid && opStart <= opEnd)
	{
		CString olTmpCode;
		COleDateTime olTmpStart,olTmpEnd;
		CMapStringToPtr olTeaMap;
		STEDATA *prlSte = NULL;


		for(int i=0; i<popTea->GetSize(); i++)
		{
			olTeaMap.SetAt(popTea->GetAt(i),NULL);
		}

		CCSPtrArray<STEDATA> olSteData;
		POSITION rlPos;
		for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSte);
			olSteData.Add(prlSte); 
		}

		olSteData.Sort(CompareSurnAndTimes);
		int ilSize = olSteData.GetSize();
		STEDATA *prlNextSte = NULL;
		void  *prlVoid = NULL;

		for(i = 0; i < ilSize; i++)
		{
			prlSte = &olSteData[i];
			prlNextSte = NULL;
			if((i+1) < ilSize)
			{
				if(prlSte->Surn == olSteData[i+1].Surn)
					prlNextSte = &olSteData[i+1];
			}

			if(prlSte->Vpfr.GetStatus() == COleDateTime::valid)
			{
				if(prlSte->Vpfr<=opEnd)
				{
					switch(prlSte->Vpto.GetStatus())
					{
					case COleDateTime::valid:
						if(prlSte->Vpto>=opStart)
						{
							olTmpCode.Format("%s",prlSte->Code);
							if(olTeaMap.Lookup(olTmpCode,(void *&)prlVoid) == TRUE)
							{
								popSteData->Add(prlSte);
							}
						}
						break;
					case COleDateTime::null:
						if(prlNextSte != NULL)
						{
							if(prlNextSte->Vpfr>=opStart)
							{
								olTmpCode.Format("%s",prlSte->Code);
								if(olTeaMap.Lookup(olTmpCode,(void *&)prlVoid) == TRUE)
								{
									popSteData->Add(prlSte);
								}
							}
						}
						else
						{
							olTmpCode.Format("%s",prlSte->Code);
							if(olTeaMap.Lookup(olTmpCode,(void *&)prlVoid) == TRUE)
							{
								popSteData->Add(prlSte);
							}
						}
						break;
					default:
						break;
					}
				}
			}
		}
		// Create Stf-Urno-List for return value
		ilSize = popSteData->GetSize();
		olUrnos = ",";
		for(i = 0; i < ilSize; i++)
		{
			CString olTmpUrno;
			olTmpUrno.Format(",%ld,",(*popSteData)[i].Surn);
			if(olUrnos.Find(olTmpUrno) == -1)
			{
				olUrnos += olTmpUrno.Mid(1);
			}
		}
		if(olUrnos.IsEmpty() == FALSE)
		{
			olUrnos = olUrnos.Mid(1,olUrnos.GetLength()-2);
		}
		olTeaMap.RemoveAll();
	}
	return olUrnos;
}

//--READSTECIALDATA-------------------------------------------------------------------------------------

bool CedaSteData::ReadStecialData(CCSPtrArray<STEDATA> *popSte,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction2("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popSte != NULL)
	{
		for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
		{
			STEDATA *prpSte = new STEDATA;
			if ((ilRc = GetBufferRecord2(ilCountRecord,prpSte,CString(pclFieldList))) == true)
			{
				// BDA: Vpfr & Vpto k�nnen auch mal null sein, es ist falsch!
				// ARE: Vpto kann auch mal null sein, es ist OK! aber auf keinen Fall ung�ltig
				if(prpSte->Vpfr.GetStatus() != COleDateTime::valid || prpSte->Vpto.GetStatus() == COleDateTime::invalid)
				{
					TRACE("Read-Ste: %d gelesen, FEHLER IM DATENSATZ, Surn=%ld\n",ilCountRecord-1,prpSte->Surn);
					delete prpSte;
					continue;
				}
				//else
					//TRACE("Read-Ste: %d gelesen, KEIN FEHLER\n",ilCountRecord-1);

				
				popSte->Add(prpSte);
			}
			else
			{
				delete prpSte;
			}
		}
		if(popSte->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaSteData::Save(STEDATA *prpSte)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpSte->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpSte->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpSte);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpSte->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSte->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpSte);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpSte->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSte->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
	TRACE("Ste-IRT/URT/DRT: Ceda-Return %d \n",ilRc);
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessSteCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogSteData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaSteData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlSteData;
	prlSteData = (struct BcStruct *) vpDataPointer;
	STEDATA *prlSte;
	
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("ProcessBC:\n  Cmd <%s>\n  Tbl <%s>\n  Twe <%s>\n  Sel <%s>\n  Fld <%s>\n  Dat <%s>\n  Bcn <%s>",prlSteData->Cmd, prlSteData->Object, prlSteData->Twe, prlSteData->Selection, prlSteData->Fields, prlSteData->Data,prlSteData->BcNum);
		WriteInRosteringLog();
	}
	
	switch(ipDDXType)
	{
	default:
		break;
	case BC_STE_CHANGE:
		prlSte = GetSteByUrno(GetUrnoFromSelection(prlSteData->Selection));
		if(prlSte != NULL)
		{
			GetRecordFromItemList(prlSte,prlSteData->Fields,prlSteData->Data);
			UpdateInternal(prlSte);
			break;
		}
	case BC_STE_NEW:
		prlSte = new STEDATA;
		GetRecordFromItemList(prlSte,prlSteData->Fields,prlSteData->Data);
		InsertInternal(prlSte);
		break;
	case BC_STE_DELETE:
		{
			long llUrno;
			CString olSelection = (CString)prlSteData->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlSteData->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			prlSte = GetSteByUrno(llUrno);
			if (prlSte != NULL)
			{
				DeleteInternal(prlSte);
			}
		}
		break;
	}
}

//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------

static int CompareTimes(const STEDATA **e1, const STEDATA **e2)
{
	if((**e1).Vpfr>(**e2).Vpfr)
		return 1;
	else
		return -1;
}

//---------------------------------------------------------------------------------------------------------

static int CompareSurnAndTimes(const STEDATA **e1, const STEDATA **e2)
{
	if((**e1).Surn>(**e2).Surn) return 1;
	else if((**e1).Surn<(**e2).Surn) return -1;

	if((**e1).Vpfr>(**e2).Vpfr) return 1;
	else if((**e1).Vpfr<(**e2).Vpfr) return -1;
	return 0;
}

//---------------------------------------------------------------------------------------------------------
