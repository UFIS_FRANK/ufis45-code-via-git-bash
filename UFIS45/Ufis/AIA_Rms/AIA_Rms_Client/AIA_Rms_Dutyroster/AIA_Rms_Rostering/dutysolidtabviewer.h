#if !defined(_DUTYSOLIDTABVIEWER_H_INCLUDED_)
#define _DUTYSOLIDTABVIEWER_H_INCLUDED_

#include <stdafx.h>
#include <CViewer.h>
//#include "CCSDynTable.h"
//#include "CCSPtrArray.h"
#include <DutyRoster_View.h>
#include <Groups.h>

#define SCROLL_TO_VISIBLE 10236

//****************************************************************************
// DutySolidTabViewer
//****************************************************************************


/////////////////////////////////////////////////////////////////////////////
class DutySolidTabViewer : public CViewer
{
// Constructions
public:
    DutySolidTabViewer(CWnd* pParent = NULL);
    ~DutySolidTabViewer();
	DutyRoster_View *pomParent;

    void Attach(CCSDynTable *popAttachWnd);
    virtual void ChangeView(bool bpSetHNull = true, bool bpSetVNull = true);

// Internal data processing routines
private:
	void InitTableArrays(int ipColumnOffset = 0);
	void MakeTableData(int ipColumnOffset,int ipRowOffset);
	void AddRow(int ipRow, int ipColumns, int ipColumnOffset, int ipRowOffset);
	void DelRow(int ipRow);
	void AddColumn(int ipColumn, int ipColumnOffset, int ipRowOffset);
	void DelColumn(int ipColumn);
public:
	bool GetElementAndPType(int ipRowNr, int &ipRow, CString &opPType, int &ipPTypNr);
	int	 GetRowForUrno(long lpUrno);
	bool ScrollToVisible(long lpUrno);
	void HighlightRow(int ipRow, bool bpHighlightOn);
	void RemoveHighlight();
	// Operations
public:

// Window refreshing routines
public:
	void HorzTableScroll(MODIFYTAB *prpModiTab);
	void VertTableScroll(MODIFYTAB *prpModiTab);
	void TableSizing(/*UINT nSide, */LPRECT lpRect);
	void MoveTable(MODIFYTAB *prpModiTab);
	void InlineUpdate(INLINEDATA *prpInlineUpdate);


// Attributes used for filtering condition
private:

// Attributes
private:
    CCSDynTable *pomDynTab;
	TABLEDATA *prmTableData;
	
	VIEWINFO *prmViewInfo;
	CCSPtrArray<STFDATA> *pomStfData;
	//CCSPtrArray<GROUPSTRUCT> *pomGroups;
	CGroups *pomGroups;

	CStringArray omPTypes;
	int imHighlightedRow;	// eine Zeile ist highlighted (orange), wenn die Maus irgendwo in DutyRoster �ber die Felder 
							// dieses MAs f�hrt, reset == 0
	int imOldColorNotHighlightedRow; // die Farbe vor dem Highlight

// Methods which handle changes (from Data Distributor)
public:

};

#endif //_DUTYSOLIDTABVIEWER_H_INCLUDED_
