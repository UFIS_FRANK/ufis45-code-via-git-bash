// CedaBsdData.cpp
  
#include <stdafx.h>


void ProcessBsdCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------
CedaBsdData ogBsdData;

CedaBsdData::CedaBsdData() : CedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for BSDDataStruct
	BEGIN_CEDARECINFO(BSDDATA,BSDDataRecInfo)
		FIELD_CHAR_TRIM	(Bkd1,"BKD1")
		FIELD_CHAR_TRIM	(Bkf1,"BKF1")
		//bda 29.09.00: not used FIELD_CHAR_TRIM	(Bkr1,"BKR1")
		FIELD_CHAR_TRIM	(Bkt1,"BKT1")
		FIELD_CHAR_TRIM	(Bsdc,"BSDC")
		//bda 29.09.00: not used FIELD_CHAR_TRIM	(Bsdk,"BSDK")
		//bda 29.09.00: not used FIELD_CHAR_TRIM	(Bsdn,"BSDN")
		//bda 29.09.00: not used FIELD_CHAR_TRIM	(Bsds,"BSDS")
		FIELD_DATE	   	(Cdat,"CDAT")
		//bda 29.09.00: not used FIELD_CHAR_TRIM	(Ctrc,"CTRC")
		FIELD_CHAR_TRIM	(Esbg,"ESBG")
		FIELD_CHAR_TRIM	(Lsen,"LSEN")
		FIELD_DATE	   	(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Rema,"REMA")
		FIELD_CHAR_TRIM	(Sdu1,"SDU1")
		//bda 29.09.00: not used FIELD_CHAR_TRIM	(Sex1,"SEX1")
		//bda 29.09.00: not used FIELD_CHAR_TRIM	(Ssh1,"SSH1")
		FIELD_CHAR_TRIM	(Type,"TYPE")
		FIELD_LONG	   	(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Rgsb,"RGSB")
		FIELD_OLEDATE	(Vafr,"VAFR")
		FIELD_OLEDATE	(Vato,"VATO")
		FIELD_CHAR_TRIM	(Fctc,"FCTC")
		FIELD_CHAR_TRIM	(Dpt1,"DPT1")
		FIELD_CHAR_TRIM	(Fdel,"FDEL")
		FIELD_CHAR_TRIM	(Bewc,"BEWC")
		FIELD_CHAR_TRIM	(Rgbc,"RGBC")
	END_CEDARECINFO //(	BSDDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(BSDDataRecInfo)/sizeof(BSDDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&BSDDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"BSD");
	strcat(pcmTableName,pcgTableExt);
	/*bda 29.09.00: einige Felder sind nicht benutzt und desw. ausgeschaltet
	strcpy(pcmListOfFields,	"BEWC,BKD1,BKF1,BKR1,BKT1,BSDC,BSDK,BSDN,BSDS,CDAT,CTRC,"
							"ESBG,LSEN,LSTU,PRFL,REMA,SDU1,SEX1,SSH1,TYPE,URNO,USEC,"
							"USEU,RGSB,VAFR,VATO,FCTC,DPT1,FDEL");*/
	// ATTENTION - fieldlist in Read()!
	strcpy(pcmListOfFields,	"BKD1,BKF1,BKT1,BSDC,CDAT,"
							"ESBG,LSEN,LSTU,PRFL,REMA,SDU1,TYPE,URNO,USEC,"
							"USEU,RGSB,VAFR,VATO,FCTC,DPT1,FDEL,BEWC,RGBC");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaBsdData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaBsdData::Register(void)
{
	DdxRegister((void *)this,BC_BSD_CHANGE,	"BSDDATA", "Bsd-changed",	ProcessBsdCf);
	DdxRegister((void *)this,BC_BSD_NEW,	"BSDDATA", "Bsd-new",		ProcessBsdCf);
	DdxRegister((void *)this,BC_BSD_DELETE,	"BSDDATA", "Bsd-deleted",	ProcessBsdCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaBsdData::~CedaBsdData(void)
{
	TRACE("CedaBsdData::~CedaBsdData called\n");
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaBsdData::ClearAll(bool bpWithRegistration)
{
	TRACE("CedaBsdData::ClearAll called\n");
    omUrnoMap.RemoveAll();
    omBsdcMap.RemoveAll();
    omData.DeleteAll();
	omSelectedData.RemoveAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaBsdData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omBsdcMap.RemoveAll();
    omData.DeleteAll();

	if (!CedaObject::IsFieldAvailable(CString("BSD"),CString("RGBC")))
	{
		strcpy(pcmListOfFields,	"BKD1,BKF1,BKT1,BSDC,CDAT,"
								"ESBG,LSEN,LSTU,PRFL,REMA,SDU1,TYPE,URNO,USEC,"
								"USEU,RGSB,VAFR,VATO,FCTC,DPT1,FDEL,BEWC");
		pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer
	}
	
	
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(pspWhere == NULL)
	{	
		ilRc = CedaAction2("RT");
	}
	else
	{
		ilRc = CedaAction2("RT", pspWhere);
	}
	if (ilRc != true)
	{
		TRACE("Read-Bsd: Ceda-Error %d \n",ilRc);
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
	{
		BSDDATA *prlBsd = new BSDDATA;
		if ((ilRc = GetFirstBufferRecord2(prlBsd)) == true)
		{
			if(IsValidBsd(prlBsd))
			{
				omData.Add(prlBsd);//Update omData
				omUrnoMap.SetAt((void *)prlBsd->Urno,prlBsd);
				omBsdcMap.SetAt((CString)prlBsd->Bsdc,prlBsd);
#ifdef TRACE_FULL_DATA
				// Datensatz OK, loggen if FULL
				ogRosteringLogText.Format(" read %8.8lu %s OK:  ", ilCountRecord, pcmTableName);
				GetDataFormatted(ogRosteringLogText, prlBsd);
				WriteLogFull("");
#endif TRACE_FULL_DATA
			}
			else
			{
				delete prlBsd;
			}
		}
		else
		{
			delete prlBsd;
		}
	}
	TRACE("Read-Bsd: %d gelesen\n",ilCountRecord-1);
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  read %d records from table %s",ilCountRecord-1, pcmTableName);
		WriteInRosteringLog();
	}
	ClearFastSocketBuffer();	

    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaBsdData::Insert(BSDDATA *prpBsd)
{
	prpBsd->IsChanged = DATA_NEW;
	if(Save(prpBsd) == false) return false; //Update Database
	InsertInternal(prpBsd);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaBsdData::InsertInternal(BSDDATA *prpBsd)
{
	bool blRet = false;
	BSDDATA *prlBsd = NULL;
	if (omUrnoMap.Lookup((void *)prpBsd->Urno,(void *& )prlBsd) == FALSE)
	{
		ogDdx.DataChanged((void *)this, BSD_NEW,(void *)prpBsd ); //Update Viewer
		omData.Add(prpBsd);//Update omData
		omUrnoMap.SetAt((void *)prpBsd->Urno,prpBsd);
		omBsdcMap.SetAt((CString)prpBsd->Bsdc,prpBsd);
		blRet =true;
	}
    return blRet;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaBsdData::Delete(long lpUrno)
{
	BSDDATA *prlBsd = GetBsdByUrno(lpUrno);
	if (prlBsd != NULL)
	{
		prlBsd->IsChanged = DATA_DELETED;
		if(Save(prlBsd) == false) return false; //Update Database
		DeleteInternal(prlBsd);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaBsdData::DeleteInternal(BSDDATA *prpBsd)
{
	ogDdx.DataChanged((void *)this,BSD_DELETE,(void *)prpBsd); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpBsd->Urno);
	omBsdcMap.RemoveKey((CString)prpBsd->Bsdc);
	int ilBsdCount = omData.GetSize();
	for (int ilCountRecord = 0; ilCountRecord < ilBsdCount; ilCountRecord++)
	{
		if (omData[ilCountRecord].Urno == prpBsd->Urno)
		{
			omData.DeleteAt(ilCountRecord);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaBsdData::Update(BSDDATA *prpBsd)
{
	if (GetBsdByUrno(prpBsd->Urno) != NULL)
	{
		if (prpBsd->IsChanged == DATA_UNCHANGED)
		{
			prpBsd->IsChanged = DATA_CHANGED;
		}
		if(Save(prpBsd) == false) return false; //Update Database
		UpdateInternal(prpBsd);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaBsdData::UpdateInternal(BSDDATA *prpBsd)
{
	BSDDATA *prlBsd = GetBsdByUrno(prpBsd->Urno);
	if (prlBsd != NULL)
	{
		omBsdcMap.RemoveKey((CString)prlBsd->Bsdc);
		*prlBsd = *prpBsd; //Update omData
		omBsdcMap.SetAt((CString)prlBsd->Bsdc,prlBsd);
		ogDdx.DataChanged((void *)this,BSD_CHANGE,(void *)prlBsd); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

BSDDATA *CedaBsdData::GetBsdByUrno(long lpUrno)
{
	BSDDATA  *prlBsd;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlBsd) == TRUE)
	{
		return prlBsd;
	}
	return NULL;
}


//*****************************************************************************************
// Pr�ft, ob dieser Code als Schichtcode vorhanden ist 
//*****************************************************************************************

bool CedaBsdData::IsBsdCode(CString opCode)
{
	BSDDATA  *prlBsd;
	if (omBsdcMap.Lookup(opCode,(void *& )prlBsd) == TRUE)
	{
		return true;
	}
	return false;
}

//*****************************************************************************************
// Sucht einen Code 
//*****************************************************************************************

BSDDATA *CedaBsdData::GetBsdByBsdc(CString opBsdc)
{
	BSDDATA  *prlBsd;
	if (omBsdcMap.Lookup(opBsdc,(void *& )prlBsd) == TRUE)
	{
		return prlBsd;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaBsdData::ReadSpecialData(CCSPtrArray<BSDDATA> *popBsd,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction2("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popBsd != NULL)
	{
		for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
		{
			BSDDATA *prpBsd = new BSDDATA;
			if ((ilRc = GetBufferRecord2(ilCountRecord,prpBsd,CString(pclFieldList))) == true)
			{
				popBsd->Add(prpBsd);
			}
			else
			{
				delete prpBsd;
			}
		}
		if(popBsd->GetSize() == 0) return false;
	}
	ClearFastSocketBuffer();	
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaBsdData::Save(BSDDATA *prpBsd)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpBsd->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpBsd->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpBsd);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpBsd->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpBsd->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpBsd);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpBsd->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpBsd->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
	TRACE("Bsd-IRT/URT/DRT: Ceda-Return %d \n",ilRc);
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessBsdCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogBsdData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaBsdData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlBsdData;
	prlBsdData = (struct BcStruct *) vpDataPointer;
	BSDDATA *prlBsd;
	long llUrno;
	CString olSelection;

		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("ProcessBC:\n  Cmd <%s>\n  Tbl <%s>\n  Twe <%s>\n  Sel <%s>\n  Fld <%s>\n  Dat <%s>\n  Bcn <%s>",prlBsdData->Cmd, prlBsdData->Object, prlBsdData->Twe, prlBsdData->Selection, prlBsdData->Fields, prlBsdData->Data,prlBsdData->BcNum);
		WriteInRosteringLog();
	}

	switch(ipDDXType)
	{
	default:
		break;
	case BC_BSD_CHANGE:
		llUrno = GetUrnoFromSelection(prlBsdData->Selection);
		prlBsd = GetBsdByUrno(llUrno);
		if(prlBsd != NULL)
		{
			GetRecordFromItemList(prlBsd,prlBsdData->Fields,prlBsdData->Data);
			UpdateInternal(prlBsd);
			break;
		}
	case BC_BSD_NEW:
		prlBsd = new BSDDATA;
		GetRecordFromItemList(prlBsd,prlBsdData->Fields,prlBsdData->Data);
		InsertInternal(prlBsd);
		break;
	case BC_BSD_DELETE:
		olSelection = (CString)prlBsdData->Selection;
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlBsdData->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+2;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		prlBsd = GetBsdByUrno(llUrno);
		if (prlBsd != NULL)
		{
			DeleteInternal(prlBsd);
		}
		break;
	}
}

void CedaBsdData::MakeBsdSelected(BSDDATA *prpBsd, BOOL bpSelect)
{
	if(prpBsd != NULL)
	{
		int ilCount = omSelectedData.GetSize();
		long lBsdu = prpBsd->Urno;
		UINT *loc;
		prpBsd->IsSelected = bpSelect;

		if(bpSelect == TRUE)
		{			
			if(ilCount>0)
			{
				loc = (UINT*)std::find(&omSelectedData[0],&omSelectedData[0] + ilCount,lBsdu);
				if(loc == (&omSelectedData[0] + ilCount)) // not found
				{
					omSelectedData.Add(prpBsd->Urno);
					ogDdx.DataChanged((void *)this,BSD_CHANGE,(void *)prpBsd);
				}
			}
			else
			{
				omSelectedData.Add(prpBsd->Urno);
				ogDdx.DataChanged((void *)this,BSD_CHANGE,(void *)prpBsd);
			}
		}
		else
		{
			if(ilCount>0)
			{
				loc = std::find(&omSelectedData[0],&omSelectedData[0] + ilCount,lBsdu);
				if(loc!=(&omSelectedData[0] + ilCount)) // found
				{
					ogDdx.DataChanged((void *)this,BSD_CHANGE,(void *)prpBsd);
					omSelectedData.RemoveAt(loc-&omSelectedData[0]);
				}
			}
		}
	}
}

//***********************************************************************************************
// 
//***********************************************************************************************

void CedaBsdData::DeSelectAll()
{
	int ilCount = omSelectedData.GetUpperBound();
	for(int k = 0; k <= ilCount; k++)
	{
		long ilUrno = omSelectedData.GetAt(k);
		BSDDATA *prlBsd = GetBsdByUrno(ilUrno);
		if(prlBsd!=NULL)
		{
			prlBsd->IsSelected = FALSE;
			ogDdx.DataChanged((void *)this,BSD_CHANGE,(void *)prlBsd);
		}
	}
	omSelectedData.RemoveAll();
}

//***********************************************************************************************
// 
//***********************************************************************************************

bool CedaBsdData::GetSelectedBsd(CCSPtrArray<BSDDATA> *popSelectedBsd)
{
	bool blRet = false;
	int ilCount = omSelectedData.GetUpperBound();
	for(int k=0;k<= ilCount;k++)
	{
		long llUrno = omSelectedData.GetAt(k);
		BSDDATA *prlBsd = GetBsdByUrno(llUrno);
		if(prlBsd!=NULL)
		{
			blRet = true;
			popSelectedBsd->Add(prlBsd);
		}
	}
	return blRet;
}

//***********************************************************************************************
// Sucht eine Schicht mit vorgegebenen Code und G�ltigkeitszeitpunkt.
//***********************************************************************************************

BSDDATA *CedaBsdData::GetBsdByBsdcAndDate(CString opBsdc,CString opDay)
{
	COleDateTime olDay;

	if (CedaDataHelper::DateStringToOleDateTime(opDay, olDay))
		return (GetBsdByBsdcAndDate(opBsdc,olDay));
	else
		return NULL;
}

//***********************************************************************************************
// Sucht eine Schicht mit vorgegebenen Code und G�ltigkeitszeitpunkt.
//***********************************************************************************************

BSDDATA *CedaBsdData::GetBsdByBsdcAndDate(CString opBsdc,COleDateTime opDay)
{
	// Eingangspr�fung
	if (opBsdc.IsEmpty() || opDay.GetStatus() != COleDateTime::valid)
		return NULL;

	COleDateTime olShiftFromTime,olShiftToTime, olNullTime;
	bool blFoundValid = false;
	bool blTimeCheckFromOk = false;
	bool blTimeCheckToOk = false;

	BSDDATA *prlBsd = 0;

	// Alle Schichten durchgehen, solange kein Treffer gefunden wurde
	for ( int i=0; i<omData.GetSize() && !blFoundValid; i++ )
	{
		prlBsd = &omData[i];

		if (opBsdc == CString(prlBsd->Bsdc))
		{
			olShiftFromTime = prlBsd->Vafr;
			olShiftToTime = prlBsd->Vato;

			//-------------------------TEST
			/*CString oltest;
			oltest = (prlBsd->Vafr).Format("Schicht :(%d.%m.%Y) -") + (prlBsd->Vato).Format(" (%d.%m.%Y)");
			oltest += opDay.Format(" Tag: (%d.%m.%Y)");
			TRACE("ogBSD.GetBsdByBsdcAndDate ->%s\n",oltest);*/
			//-------------------------TESTENDE
		
			//----------------------------------------------------------------------------------
			// Begin pr�fen
			// Ist ein vorgabe Wert vorhanden f�r diese Schicht ?
			if (olShiftFromTime.GetStatus() != COleDateTime::valid  || olShiftFromTime == olNullTime)
			{
				// -> keine Pr�fung vornehmen.
				blTimeCheckFromOk = true;
			}
			else
			{
				// Pr�fung vornehmen
				if (opDay >= olShiftFromTime)
					blTimeCheckFromOk = true;
				else
					blTimeCheckFromOk = false;
			}
			//----------------------------------------------------------------------------------
			// Ende pr�fen
			// Ist ein vorgabe Wert vorhanden f�r diese Schicht ?
			if (olShiftToTime.GetStatus() != COleDateTime::valid || olShiftToTime == olNullTime)
			{
				// -> keine Pr�fung vornehmen.
				blTimeCheckToOk = true;
			}
			else
			{
				// Pr�fung vornehmen
				if (opDay <= olShiftToTime)
					blTimeCheckToOk = true;
				else
					blTimeCheckToOk = false;
			}
			//------------------------------------------------------------------------------------
			// G�ltige Schicht gefunden: (Start und Ende g�ltig, Schichtcode ist g�ltig)
			if (blTimeCheckToOk && blTimeCheckFromOk)
				blFoundValid = true;
		}
	}
	if (blFoundValid)
	{
		TRACE("ogBSD.GetBsdByBsdcAndDate result: valid\n");
		return prlBsd;
	}
	else
	{
		TRACE("ogBSDGetBsdByBsdcAndDate result: invalid\n");
		return NULL;
	}
}

//**************************************************************************************
// Pr�ft ob Schichcode an diesem Tag g�ltig ist.
// -> Sucht eine Schicht die an diesem Tag g�ltig ist.
// gibt g�ltige URNO f�r diesen Tag zur�ck.
//**************************************************************************************

long CedaBsdData::CheckAndGetValidBsdCode(long ilUrno,CString olNewText,int ilCodeType,COleDateTime olDay)
{
	bool blIsValid;
	long llReturnUrno = ilUrno;

	// Mehrdeutigkeit bez�glich der Zeit is NUR (!) bei Schichten vorhanden. Juhu.
	if (ilCodeType != CODE_IS_BSD)
		return llReturnUrno;

	// Pr�fung ob diese Schicht g�ltig ist
	bool blTimeCheckFromOk = false;
	bool blTimeCheckToOk = false;
	COleDateTime olNullTime;

	//---------------------------------------------------------------------------------
	// Wurde die Urno der Schicht mitgeliefert ?
	// Dann Schicht anhand der Urno pr�fen.
	if (ilUrno != 0)
	{
		BSDDATA* polBsd = ogBsdData.GetBsdByUrno(ilUrno);
		if (polBsd == NULL)
			blIsValid = false;
		else
		{
			//-------------------------TEST
			CString oltest;
			oltest = (polBsd->Vafr).Format("Schicht :(%d.%m.%Y) -") + (polBsd->Vato).Format(" (%d.%m.%Y)");
			oltest += olDay.Format(" Tag: (%d.%m.%Y)");
			TRACE("%s\n",(LPCTSTR)oltest);
			//-------------------------TESTENDE
			//---------------------------------------------------------------------------------
			// Startwert pr�fen
			// Ist ein Vorgabe Wert vorhanden f�r diese Schicht ?
			// Sollte der Wert Null sein, ebenfalls nicht pr�fen
			if (polBsd->Vafr.GetStatus() != COleDateTime::valid || polBsd->Vafr == olNullTime)
				// -> keine Pr�fung vornehmen.
				blTimeCheckFromOk = true;
			else
			{
				// Pr�fung vornehmen
				if (polBsd->Vafr <= olDay)
					blTimeCheckFromOk = true;
			}
			//----------------------------------------------------------------------------------
			// Endwert pr�fen
			// Sollte der Wert Null sein, ebenfalls nicht pr�fen
			if (polBsd->Vato.GetStatus() != COleDateTime::valid || polBsd->Vato == olNullTime)
				// -> keine Pr�fung vornehmen.
				blTimeCheckToOk = true;
			else
			{
				// Pr�fung vornehmen
				if (polBsd->Vato >= olDay)
					blTimeCheckToOk = true;
			}
		}
	}
	//-----------------------------------------------------------------------------------------
	// Sollte keine Urno �bergeben worden sein, oder die �bergebene ung�ltig sein.
	if (ilUrno == 0 || !(blTimeCheckToOk && blTimeCheckFromOk))
	{
		// Versuche eine g�ltige Schicht zu finden
		BSDDATA* polBsd = GetBsdByBsdcAndDate(olNewText,olDay);
		
		if (polBsd != NULL)
			// Schicht gefunden, URNO bereitstellen.
			llReturnUrno = polBsd->Urno;
		else
			// Schicht nicht gefunden.
			llReturnUrno = 0;
	}
	// Urno zur�ckgeben
	return llReturnUrno;
}

/****************************************************************************
R�ckgabe: Pausenanfang, bei �bernacht- dann auch mit einem Tag addiert
z.B. wenn die Schicht von 23:30 bis 8:00 geht, und die Pause von 2:00 bis 3:00
wird die Pause als 1Tag 2:00 zur�ckgegeben
Return false, wenn die Anfangzeit == Endzeit ist, d.h. keine Pause vorgesehen
****************************************************************************/
bool CedaBsdData::GetBkf1Time(BSDDATA* popBsd, COleDateTimeSpan& opBkf1Time)
{
	if(strcmp(popBsd->Bkf1, popBsd->Bkt1) == 0)
		return false;	// Anfang == Ende
	if (!CedaDataHelper::HourMinStringToOleDateTimeSpan(popBsd->Bkf1,opBkf1Time))
		return false;
	
	// beginnt die Pausenlage am n�chsten Tag?
	// da beide Zeiten als Zahlenstrings vorliegen (z.B. 0230 und 1545) kann man sie direkt als Strings vergleichen
	if (strcmp(popBsd->Bkf1,popBsd->Esbg)<0) 
	{
		// noch einen Tag drauf
		opBkf1Time += COleDateTimeSpan(1,0,0,0);
	}
	return true;
}

/****************************************************************************
R�ckgabe: Pausenende, bei �bernacht- dann auch mit einem Tag addiert
z.B. wenn die Schicht von 23:30 bis 8:00 geht, und die Pause von 2:00 bis 3:00
wird die Pause als 1Tag 3:00 zur�ckgegeben
Return false, wenn die Anfangzeit == Endzeit ist, d.h. keine Pause vorgesehen
****************************************************************************/
bool CedaBsdData::GetBkt1Time(BSDDATA* popBsd, COleDateTimeSpan& opBkt1Time)
{
	if(strcmp(popBsd->Bkf1, popBsd->Bkt1) == 0)
		return false;	// Anfang == Ende
	if (!CedaDataHelper::HourMinStringToOleDateTimeSpan(popBsd->Bkt1,opBkt1Time))
		return false;
	
	// beginnt die Pausenlage am n�chsten Tag?
	// da beide Zeiten als Zahlenstrings vorliegen (z.B. 0230 und 1545) kann man sie direkt als Strings vergleichen
	if (strcmp(popBsd->Bkt1,popBsd->Bkf1)<0 || strcmp(popBsd->Bkt1,popBsd->Esbg)<0) 
	{
		// noch einen Tag drauf
		opBkt1Time += COleDateTimeSpan(1,0,0,0);
	}
	return true;
}

/****************************************************************************
testet, ob die Daten korrupt sind: 
FCTC 
DPT1
****************************************************************************/
bool CedaBsdData::IsValidBsd(BSDDATA* popBsd)
{
	CCS_TRY;

	//	char 	 	 	 Fctc[7]; 	// Reg.Funktions.code				
	if(strlen(popBsd->Fctc) > 0 && !ogPfcData.GetPfcByFctc(popBsd->Fctc))
	{
		CString olErr;
		olErr.Format("Basic Shift Data Table entry '%s' is defect: Function code '%s' is not found in Functions Table\n",
			popBsd->Bsdc, popBsd->Fctc);
		ogErrlog += olErr;
		
		if(IsTraceLoggingEnabled())
		{
			GetDefectDataString(ogRosteringLogText, (void*)popBsd, "BSDTAB.FCTC in PFCTAB not found");
			WriteInRosteringLog(LOGFILE_TRACE);
		}
		return false;	// 1. Defekter Datensatz
	}

	
	if(strlen(popBsd->Dpt1) > 0 && !ogOrgData.GetOrgByDpt1(CString(popBsd->Dpt1)))
	{
		// ORGCODE nicht gefunden!
		CString olErr;
		olErr.Format("Basic Shift Data Table entry '%s' is defect: code '%s' is not found in Organisational Unit Table\n",
			popBsd->Bsdc, popBsd->Dpt1);
		ogErrlog += olErr;

		if(GetDefectDataString(ogRosteringLogText, (void *)popBsd), "BSD.DPT1 not found in ORGTAB")
		{
			WriteInRosteringLog(LOGFILE_TRACE);
		}
		return false;
	}
	return true;
	CCS_CATCH_ALL;
	return false;
}

	





