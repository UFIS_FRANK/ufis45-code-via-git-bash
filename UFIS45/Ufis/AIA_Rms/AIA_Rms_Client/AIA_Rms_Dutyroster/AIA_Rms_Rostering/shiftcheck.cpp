#include <stdafx.h>

extern CedaStfData ogStfData;
CedaShiftCheck* gpShiftCheck;

/*****************************************************************************
Constructor
*****************************************************************************/
CedaShiftCheck::CedaShiftCheck()
{
	// die Initialisierung ist sp�ter in Initialize()
	gpShiftCheck = this;
	imEName = 2;
}

/******************************************************************************	
Destructor
******************************************************************************/
CedaShiftCheck::~CedaShiftCheck()
{
	ClearAll(true);
	
	return;
}

/*******************************************************************************
*******************************************************************************/
void CedaShiftCheck::Initialize(void)
{
	CCS_TRY; // ';' ist nur f�r Alt+F8=Automatisches Formatieren n�tig
	imOutput = OUT_WARNING_AS_WARNING | OUT_ERROR_AS_ERROR;
	
	// bei ShiftCheck ist keine Initialisierung der Zeiten, so setzen wir im Vorfeld auf +-10 Jahre (wer macht mehr?)
	omCheckDateFrom = COleDateTime::GetCurrentTime() - COleDateTimeSpan(10,0,0,0);
	omCheckDateTo = COleDateTime::GetCurrentTime() + COleDateTimeSpan(10,0,0,0);
	
	omMapStaffDutyData.InitHashTable(256);
	
	// beim Broadcast-Handler anmelden
	Register();
	
	// Regeln-Array f�llen
	// alle Volumeangaben, als Pointer ist ID_WOR_SPACE bis ID_WOR_MAXIMUMVALUE
	omRules.SetSize(ID_WOR_MAXIMUMVALUE);

	// der erste Parameter ID_WOR_SPACE ist immer gleich default auf aktiv gesetzt
	if(ogCCSParam.GetParamValue(ogAppl,"ID_PROD_CUSTOMER") != "FRA")
	{
		omRules[ID_WOR_FREEH]		= atoi(ogCCSParam.GetParamValue(ogAppl,"ID_WOR_FREEH"));  // min
		omRules[ID_WOR_FREEHT]		= atoi(ogCCSParam.GetParamValue(ogAppl,"ID_WOR_FREEHT"));  // min
		omRules[ID_WOR_RESTH]		= atoi(ogCCSParam.GetParamValue(ogAppl,"ID_WOR_RESTH")); 
		omRules[ID_WOR_RESTDW]		= atoi(ogCCSParam.GetParamValue(ogAppl,"ID_WOR_RESTDW"));
		omRules[ID_WOR_MAWORH]		= atoi(ogCCSParam.GetParamValue(ogAppl,"ID_WOR_MAWORH"));
		omRules[ID_WOR_MAWORHT]		= atoi(ogCCSParam.GetParamValue(ogAppl,"ID_WOR_MAWORHT"));
		omRules[ID_WOR_WORHWT]		= atoi(ogCCSParam.GetParamValue(ogAppl,"ID_WOR_WORHWT"));
		omRules[ID_WOR_MIWORH]		= atoi(ogCCSParam.GetParamValue(ogAppl,"ID_WOR_MIWORH"));
		omRules[ID_WOR_MIWORHT]		= atoi(ogCCSParam.GetParamValue(ogAppl,"ID_WOR_MIWORHT"));
		omRules[ID_WOR_MAWORD]		= atoi(ogCCSParam.GetParamValue(ogAppl,"ID_WOR_MAWORD"));
		omRules[ID_WOR_MAWORDT]		= atoi(ogCCSParam.GetParamValue(ogAppl,"ID_WOR_MAWORDT"));
		omRules[ID_WOR_MAFREED]		= atoi(ogCCSParam.GetParamValue(ogAppl,"ID_WOR_MAFREED"));
		omRules[ID_WOR_MAFREEDT]	= atoi(ogCCSParam.GetParamValue(ogAppl,"ID_WOR_MAFREEDT"));
	}
	else
	{
		// kein ShiftCheck in Frankfurt!
		omRules[ID_WOR_FREEH]		= atoi(ogCCSParam.GetParamValue(ogAppl,"ID_WOR_FREEH"));;
		omRules[ID_WOR_FREEHT]		= atoi(ogCCSParam.GetParamValue(ogAppl,"ID_WOR_FREEHT"));;
		omRules[ID_WOR_RESTH]		= (DWORD)-1;
		omRules[ID_WOR_RESTDW]		= (DWORD)-1;
		
		// bda ID_WOR_MAWORH ID_WOR_MAWORHT zzt f�r die Schweiz abgeschaltet
		omRules[ID_WOR_MAWORH]		= (DWORD)-1;//atoi(ogCCSParam.GetParamValue(ogAppl,"ID_WOR_MAWORH"));
		omRules[ID_WOR_MAWORHT]		= (DWORD)-1;//atoi(ogCCSParam.GetParamValue(ogAppl,"ID_WOR_MAWORHT"));
		
		omRules[ID_WOR_WORHWT]		= (DWORD)-1;
		omRules[ID_WOR_MIWORH]		= (DWORD)-1;
		omRules[ID_WOR_MIWORHT]		= (DWORD)-1;
		omRules[ID_WOR_MAWORD]		= (DWORD)-1;
		omRules[ID_WOR_MAWORDT]		= (DWORD)-1;
		omRules[ID_WOR_MAFREED]		= (DWORD)-1;
		omRules[ID_WOR_MAFREEDT]	= (DWORD)-1;
	}
	
#ifdef _DEBUG
	TRACE("\nFehlertext-Kontrolle - auf Deutsch und Originalsprache:\n\n");
	CString olStr;
	long llTest = 12345;
	olStr = LoadStg(IDS_STRING1654);
	TRACE("ID_WOR_SPACE\tArbeitszeiten �berschneiden sich!\t ________ %s\n", olStr);
	
	olStr = LoadStg(IDS_STRING1626);
	TRACE("ID_WOR_FREEH\tT�gliche Ruhezeit unterschritten!\t ________ %s\n", olStr);
	
	olStr = LoadStg(IDS_STRING1627);
	TRACE("ID_WOR_FREEHT\tt�gliche Ruhezeit < %d h\t ________ %s\n", llTest, olStr);
	
	olStr = LoadStg(IDS_STRING1628);
	TRACE("ID_WOR_RESTH\tFreier Tag: min. Ruhezeit unterschritten! (%d h)\t ________ %s\n", llTest, olStr);
	
	olStr = LoadStg(IDS_STRING1629);
	TRACE("ID_WOR_RESTDW\tMin. Freie Tage unterschritten! (%d pro Woche)\t ________ %s\n", llTest, olStr);
	
	olStr = LoadStg(IDS_STRING1640);
	TRACE("ID_WOR_MAWORH\tMax. t�gliche Arbeitszeit �berschritten! (%d h)\t ________ %s\n", llTest, olStr);
	
	olStr = LoadStg(IDS_STRING1645);
	TRACE("ID_WOR_MAWORHT\tmax. t�gliche Arbeitszeit > %d h\t ________ %s\n", llTest, olStr);
	
	olStr = LoadStg(IDS_STRING1646);
	TRACE("ID_WOR_WORHWT\tWochenarbeitszeit > %d h\t ________ %s\n", llTest, olStr);
	
	olStr = LoadStg(IDS_STRING1647);
	TRACE("ID_WOR_MIWORH\tMin. t�gliche Arbeitszeit unterschritten! (%d h)\t ________ %s\n", llTest, olStr);
	
	olStr = LoadStg(IDS_STRING1648);
	TRACE("ID_WOR_MIWORHT\tmin. t�gliche Arbeitszeit < %d h\t ________ %s\n", llTest, olStr);
	
	olStr = LoadStg(IDS_STRING1649);
	TRACE("ID_WOR_MAWORD\tMax. Arbeitstage in Folge �berschritten! (%d)\t ________ %s\n", llTest, olStr);
	
	olStr = LoadStg(IDS_STRING1650);
	TRACE("ID_WOR_MAWORDT\tArbeitstage in Folge > %d\t ________ %s\n", llTest, olStr);
	
	olStr = LoadStg(IDS_STRING1651);
	TRACE("ID_WOR_MAFREED\tFreie Tage in Folge �berschritten! (%d)\t ________ %s\n", llTest, olStr);
	
	olStr = LoadStg(IDS_STRING1652);
	TRACE("ID_WOR_MAFREEDT\tfreie Tage in Folge > %d\t ________ %s\n", llTest, olStr);
	
	
#endif _DEBUG
	
	bmEnableTeilzeitplusQuestion = false;
	bmSaveAsTeilzeitplus = false;
	bmDoConfictCheck = true;
	
	return;
	CCS_CATCH_ALL;
}

/**********************************************************************************
// SetCheckDate f�llt omCheckDateFrom und omCheckDateTo aus
**********************************************************************************/
void CedaShiftCheck::SetCheckDate(COleDateTime opCheckDateFrom, COleDateTime opCheckDateTo)
{
	if(opCheckDateFrom > opCheckDateTo) return;
	omCheckDateFrom = opCheckDateFrom;
	omCheckDateTo = opCheckDateTo;
}

/**********************************************************************************
Register: beim Broadcasthandler anmelden. Um Broadcasts (Meldungen vom 
Datenbankhandler) zu empfangen, muss diese Funktion ausgef�hrt werden.
Der letzte Parameter ist ein Zeiger auf eine Callback-Funktion, welche
dann die entsprechende Nachricht bearbeitet. �ber den nach void 
konvertierten Zeiger auf dieses Objekt ((void *)this) kann das 
Objekt ermittelt werden, das der eigentliche Empf�nger der Nachricht ist
und an das die Callback-Funktion die Nachricht weiterleitet (per Aufruf einer
entsprechenden Member-Funktion).
R�ckgabe: keine
**********************************************************************************/
void CedaShiftCheck::Register(void)
{
CCS_TRY;
	// DRR-Datensatz hat sich ge�ndert
	DdxRegister((void *)this,DRR_CHANGE,        "ShiftCheck", "DRR_CHANGE",				ProcessShiftCheckCf);
	DdxRegister((void *)this,DRR_MULTI_CHANGE,  "ShiftCheck", "DRR_MULTI_CHANGE",			ProcessShiftCheckCf);
	DdxRegister((void *)this,DRR_NEW,           "ShiftCheck", "DRR_NEW",					ProcessShiftCheckCf);
	DdxRegister((void *)this,DRR_DELETE,        "ShiftCheck", "DRR_DELETE",				ProcessShiftCheckCf);
	// DRA-Datensatz hat sich ge�ndert
	DdxRegister((void *)this,DRA_CHANGE,        "ShiftCheck", "DRA_CHANGE",				ProcessShiftCheckCf);
	DdxRegister((void *)this,DRA_NEW,           "ShiftCheck", "DRA_NEW",					ProcessShiftCheckCf);
	DdxRegister((void *)this,DRA_DELETE,        "ShiftCheck", "DRA_DELETE",				ProcessShiftCheckCf);
CCS_CATCH_ALL;
}

/******************************************************************
ClearAll: aufr�umen. Alle Arrays und PointerMaps werden geleert.
R�ckgabe:	immer true
******************************************************************/

bool CedaShiftCheck::ClearAll(bool bpUnregister)
{
	CCS_TRY;
	
	// beim BC-Handler abmelden
	ogDdx.UnRegister(this,NOTUSED);

	ClearWorkTimeMap();

    return true;
	CCS_CATCH_ALL;
	return false;
}


/*******************************************************************
L�scht alle Eintr�ge in omMapStaffDutyData
*******************************************************************/
void CedaShiftCheck::ClearWorkTimeMap()
{
	CCS_TRY;
	long llStfu;
	CedaWorkTime* polWorkTime = 0;
	for(POSITION pos = omMapStaffDutyData.GetStartPosition(); pos!=0; )
	{
		omMapStaffDutyData.GetNextAssoc(pos, llStfu, polWorkTime);
		if(polWorkTime != 0)
			delete polWorkTime;
	}
	omMapStaffDutyData.RemoveAll();
	CCS_CATCH_ALL;
}

/************************************************************************************************************************************************
ProcessShiftCheckCf: Callback-Funktion f�r den Broadcast-Handler und die Broadcasts.
***********************************************************************************************************************************************/

void ProcessShiftCheckCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	CCS_TRY; // ';' ist nur f�r Alt+F8=Automatisches Formatieren n�tig
	// Sanduhr einblenden, falls es l�nger dauert
	AfxGetApp()->DoWaitCursor(1);

	// Meldungen an den Benutzer ausschalten, die gehen ihn nicht an
	int ilOutputOld = gpShiftCheck->imOutput;
	gpShiftCheck->imOutput = OUT_NONE;

	switch (ipDDXType)
	{
	default:
		break;
	case DRR_CHANGE:
		gpShiftCheck->UpdateData((DRRDATA *)vpDataPointer, DRR_CHANGE);
		//TRACE("ShiftCheck DRR_CHANGE\n");
		break;
	case DRR_MULTI_CHANGE:
		gpShiftCheck->ProcessDrrMultiChange(vpDataPointer, ropInstanceName);
		//TRACE("ShiftCheck DRR_MULTI_CHANGE\n");
		break;
	case DRR_NEW:
		gpShiftCheck->UpdateData((DRRDATA *)vpDataPointer, DRR_NEW);
		//TRACE("ShiftCheck DRR_NEW\n");
		break;
	case DRR_DELETE:
		gpShiftCheck->UpdateData((DRRDATA *)vpDataPointer, DRR_DELETE);
		//TRACE("ShiftCheck DRR_DELETE\n");
		break;
	case DRA_CHANGE:
		gpShiftCheck->UpdateData((DRADATA *)vpDataPointer, DRA_CHANGE);
		//TRACE("ShiftCheck DRA_CHANGE\n");
		break;
	case DRA_NEW:
		gpShiftCheck->UpdateData((DRADATA *)vpDataPointer, DRA_NEW);
		//TRACE("ShiftCheck DRA_NEW\n");
		break;
	case DRA_DELETE:
		gpShiftCheck->UpdateData((DRADATA *)vpDataPointer, DRA_DELETE);
		//TRACE("ShiftCheck DRA_DELETE\n");
		break;
	}// end switch

	// Meldungen zur�ckschalten
	gpShiftCheck->imOutput = ilOutputOld;

  // Sanduhr ausblenden
	RemoveWaitCursor();
	CCS_CATCH_ALL;
}

/****************************************************************************
ProcessDrrMultiChange: bearbeitet den Broadcast DRR_MULTI_CHANGE 
(�nderung mehrerer DRR-Datens�tze).
****************************************************************************/
void CedaShiftCheck::ProcessDrrMultiChange(void *vpDataPointer, CString &ropInstanceName)
{
CCS_TRY;

	CCSPtrArray<DRRDATA> *polDrrList = (CCSPtrArray<DRRDATA> *)vpDataPointer;
	int ilSize = polDrrList->GetSize();
	DRRDATA *prlDrr = NULL;
	int ilDDXType;
	for(int index=0; index<ilSize; index++)
	{
		prlDrr = &(*polDrrList)[index];

		switch(prlDrr->IsChanged)
		{
		default:
			continue;	// DATA_UNCHANGED vor allem

		case DATA_CHANGED:
			ilDDXType = DRR_CHANGE;
			break;
		case DATA_NEW:
			ilDDXType = DRR_NEW;
			break;
		case DATA_DELETED:
			ilDDXType = DRR_DELETE;
			break;
		}
		UpdateData(prlDrr, ilDDXType);
	}
CCS_CATCH_ALL;
}

/********************************************************************************************
FillWorkTimeData() scannt:
alle DRRs und evtl. dazugeh�rige DRAs und gibt die an eine der beiden
UpdateData(), um letztendlich den Datenarray mit Arbeitszeiten auszuf�llen
********************************************************************************************/
void CedaShiftCheck::FillWorkTimeData(void)
{
	CCS_TRY; // ';' ist nur f�r Alt+F8=Automatisches Formatieren n�tig
	WriteLogFull("enter FillWorkTimeData()");

	// Drr-s und Dra-s
	DRRDATA* prlDrr;
	int ilDrrSize = ogDrrData.GetInternalSize();
	TRACE("ShiftCheck::FillWorkTimeData - %lu DRRs gefunden\n", ilDrrSize);

	ClearWorkTimeMap();
	SetOutputAsWarning();

	for (int ilDrr=0;ilDrr<ilDrrSize;ilDrr++)
	{
		// diesen DRR lesen
		prlDrr = ogDrrData.GetInternalData(ilDrr);
			
		// DRR speichern
		UpdateData(prlDrr, DRR_NEW);
	}
	SetOutputAsWarning(false);

#ifdef TRACE_ALL_CHECKFUNC
	for (ilDrr=0;ilDrr<ilDrrSize;ilDrr++)
	{
		// diesen DRR lesen
		prlDrr = ogDrrData.GetInternalData(ilDrr);

		if(SHOW_MA_URNO && prlDrr->Stfu == SHOW_MA_URNO)
			ogDrrData.TraceDrr(prlDrr, (LPCTSTR)"DRR: ");
		//Sleep(5);
	}
#endif TRACE_ALL_CHECKFUNC
	CCS_CATCH_ALL;
}

/*******************************************************************************************
true - alles als Warnung,
false - Warnung als Warnung, Fehler als Fehler
*******************************************************************************************/
void CedaShiftCheck::SetOutputAsWarning(bool bpAsWarning/*=true*/)
{
	if(bpAsWarning)
	{
		imOutput = OUT_WARNING_AS_WARNING | OUT_ERROR_AS_WARNING;
	}
	else
	{
		imOutput = OUT_WARNING_AS_WARNING | OUT_ERROR_AS_ERROR;
	}
}

/********************************************************************************************
UpdateData() speichert �nderungen der Arbeitszeit im Datenarray
********************************************************************************************/
void CedaShiftCheck::UpdateData(DRRDATA* prpDrr,int ipDDXType)
{
	if(!prpDrr || !prpDrr->Stfu)
		return;
	// WORKAROUND: wir blockieren Datenm�ll mit valid times and 000000 time
	/*if(	prpDrr->Avfr.GetStatus() != COleDateTime::valid || 
		prpDrr->Avto.GetStatus() != COleDateTime::valid	||
		prpDrr->Avfr <=  
*/

	CedaWorkTime* polWorkTime;
	if(!omMapStaffDutyData.Lookup(prpDrr->Stfu, polWorkTime))
	{
		// kein Element gefunden
		polWorkTime = new CedaWorkTime(prpDrr->Stfu, this, imEName);
		if(!polWorkTime) return;

		omMapStaffDutyData.SetAt(prpDrr->Stfu, polWorkTime);
	}
	polWorkTime->UpdateData(prpDrr, ipDDXType);
	//TRACE("ShiftCheck: Drr updated\n");
}

/********************************************************************************************
UpdateData() speichert �nderungen der Arbeitszeit im Datenarray
********************************************************************************************/
void CedaShiftCheck::UpdateData(DRADATA* prpDra,int ipDDXType)
{
	if(!prpDra || !prpDra->Stfu) return;

	CedaWorkTime* polWorkTime;
	if(!omMapStaffDutyData.Lookup(prpDra->Stfu, polWorkTime))
	{
		// kein Element gefunden
		polWorkTime = new CedaWorkTime(prpDra->Stfu, this, imEName);
		if(!polWorkTime) return;

		omMapStaffDutyData.SetAt(prpDra->Stfu, polWorkTime);
	}

	polWorkTime->UpdateData(prpDra, ipDDXType);
	//TRACE("ShiftCheck: Dra updated\n");
}

/****************************************************************************
die eingentliche Testfunktion
prpDrr - neue Datensatz zum Testen,
ipDDXType - ein von Broadcasts:
DRR_CHANGE, DRR_MULTI_CHANGE, DRR_NEW, DRR_DELETE, DRA_CHANGE, DRA_NEW, DRA_DELETE
DRR_MULTI_CHANGE ist gleich, wie DRR_CHANGE
wenn prpDrr == 0, dann ipDDXType wird ingnoriert und ein Check von allen MAs wird durchgef�hrt
in diesem Fall return true, wenn kein MA eine Fehlermeldung bekam
!!!!!!!!!!!!
wenn bpEnableTeilzeitplusQuestion = true, beim Nichterf�llen der Regeln und wenn eine Teilzeitplus m�glich w�re,
fragt man den Benutzer wegen TZ+, statt einfach eine Fehlermeldung auszugeben
Die Antwort ist unter IsTeilzeitPlusConfirmed() abzufragen
Diese Frage ist nur f�r EIN DRR m�glich
R�ckgabe 'false' trotzdem
****************************************************************************/
bool CedaShiftCheck::DutyRosterCheckIt(DRRDATA* prpDrr, bool bpDoConflictCheck, int ipDDXType, bool bpEnableTeilzeitplusQuestion /*=false*/)
{
	// Feature erlaubt?
	//if(ogPrivList.GetStat("DUTYROSTER_CONFLICTCHECK") != '1')	return true;	// nein -> terminieren

	bmDoConfictCheck = bpDoConflictCheck;
	bmShiftRosterCheck = false;
	bmSaveAsTeilzeitplus = false;

	_ASSERTE(omCheckDateFrom.GetStatus() == COleDateTime::valid && omCheckDateTo.GetStatus() == COleDateTime::valid);

	bool blRet = true;
	CedaWorkTime* polWorkTime = 0;
	if(prpDrr != 0)
	{
		if(prpDrr->Avfr.GetStatus() != COleDateTime::valid || prpDrr->Avto.GetStatus() != COleDateTime::valid || strcmp(prpDrr->Ross, "A") != 0)
		{
			bmDoConfictCheck = true;
			return true;
		}
		// einzelnen MA checken
		// den Mensch finden
		if(omMapStaffDutyData.Lookup(prpDrr->Stfu, polWorkTime))
		{
			bool blEnableTeilzeitplusQuestionOld = bmEnableTeilzeitplusQuestion;
			bmEnableTeilzeitplusQuestion = bpEnableTeilzeitplusQuestion;
			bmSaveAsTeilzeitplus = false;

			// weitergeben
			blRet = polWorkTime->CheckIt((void*)prpDrr, ipDDXType);
			
			bmEnableTeilzeitplusQuestion = blEnableTeilzeitplusQuestionOld;
		}
		// else : wenn kein Datensatz gefunden - d.h. dieser MA hat gar keine Schichten - alles kann eingef�gt werden - return true
	}
	else
	{
		_ASSERTE(!bpEnableTeilzeitplusQuestion);	// kein bpEnableTeilzeitplusQuestion bei Blindpr�fen mehrerer MAs
		// alle MAs checken
		long llStfu;
		for(POSITION pos = omMapStaffDutyData.GetStartPosition(); pos!=0; )
		{
			omMapStaffDutyData.GetNextAssoc(pos, llStfu, polWorkTime);
			if(!polWorkTime) continue;	// was auch immer
			polWorkTime->CheckIt(0, 0);
		}
	}

	bmDoConfictCheck = true;
	return blRet;
}

/****************************************************************************
opTestDay - Tag, an dem getestet werden soll
Die Funktion l�uft alle MAs an dem angegeben Tag durch
****************************************************************************/
void CedaShiftCheck::DutyRosterCheckIt(COleDateTime &opTestDay)
{
	// Feature erlaubt?
	//if(ogPrivList.GetStat("DUTYROSTER_CONFLICTCHECK") != '1') return;
	
	if(opTestDay.GetStatus() != COleDateTime::valid)
		return;

	bmShiftRosterCheck = false;
	bmSaveAsTeilzeitplus = false;

	_ASSERTE(omCheckDateFrom.GetStatus() == COleDateTime::valid && omCheckDateTo.GetStatus() == COleDateTime::valid);
	
	CedaWorkTime* polWorkTime = 0;

	// alle MAs checken
	long llStfu;
	
	for(POSITION pos = omMapStaffDutyData.GetStartPosition(); pos!=0; )
	{
		omMapStaffDutyData.GetNextAssoc(pos, llStfu, polWorkTime);
		if(!polWorkTime) continue;	// was auch immer
		polWorkTime->CheckIt(&opTestDay, DRR_CHECK);
	}
}

/****************************************************************************
true, wenn pomWarnings nicht leer ist (d.h. Warnungen vorliegen)
****************************************************************************/
bool CedaShiftCheck::IsWarning()
{
	long llStfu;
	CedaWorkTime* polWorkTime = 0;

	for(POSITION pos = omMapStaffDutyData.GetStartPosition(); pos!=0; )
	{
		omMapStaffDutyData.GetNextAssoc(pos, llStfu, polWorkTime);
		if(	polWorkTime != 0 && 
			polWorkTime->pomWarnings != 0 && 
			polWorkTime->pomWarnings->GetSize() > 0)
			return true;
	}
	
	return false;
}

/****************************************************************************
Alle Warnungen werden zu einer Liste (Array) gesammelt und aus der Quelle gel�scht
(sie sind in der Quelle nicht mehr n�tig, weil sie nur f�r diese Ausgabe gespeichert waren)
****************************************************************************/
CStringArray* CedaShiftCheck::GetWarningList()
{
	CCS_TRY;
	CStringArray* polWarningsArray = new CStringArray;
	if(!polWarningsArray) return 0;

	long llStfu;
	CedaWorkTime* polWorkTime = 0;
	for(POSITION pos = omMapStaffDutyData.GetStartPosition(); pos!=0; )
	{
		omMapStaffDutyData.GetNextAssoc(pos, llStfu, polWorkTime);
		if(	polWorkTime != 0 && 
			polWorkTime->pomWarnings != 0 && 
			polWorkTime->pomWarnings->GetSize() > 0)
		{
			polWarningsArray->Append(*polWorkTime->pomWarnings);
			polWorkTime->pomWarnings->RemoveAll();
		}
			
	}
	
	if(!polWarningsArray->GetSize())
	{
		// keine Warnungen vorhanden
		delete polWarningsArray;
		return 0;
	}
	return polWarningsArray;
	CCS_CATCH_ALL;
	return 0;
}

/*********************************************************************************************	
ShiftRosterCheckIt checkt die Arbeitsregeln bei Schichtplanung
*********************************************************************************************/

int CedaShiftCheck::ShiftRosterCheckIt(int ipGPLPeri,CCSPtrArray<GSPTABLEDATA> &opGSPTableData)
{
	CCS_TRY;

	bmShiftRosterCheck = true;
	
	//	Check der wichtigsten Parameter:
	
	if (opGSPTableData.GetSize() < 0)
	{
		return RET_NO_ERROR;
	}

	// Datenarray f�llen
	// Wir legen f�r jeden MA (indexiert durch ipGPLPeri) 3 Wochen, alle drei sind gleich. Getestet wird nur die 2. Woche
	if (RET_NO_ERROR != ReadShifts(ipGPLPeri,opGSPTableData))
	{
		return RET_SOFTWARE_ERROR;//	Error Message
	}

	// alle MAs checken
	CedaWorkTime* polWorkTime = 0;
	long llStfu;

	pomMe->GetParentFrame()->GetMessageBar()->SetWindowText(LoadStg(IDS_STRING63504));
	// Test
	bool blContinue = false;
	COleDateTime olTestDay;
	for(POSITION pos = omMapStaffDutyData.GetStartPosition(); pos!=0; )
	{
		blContinue = false;
		omMapStaffDutyData.GetNextAssoc(pos, llStfu, polWorkTime);
		if(!polWorkTime) continue;	// was auch immer

		int ilAppendWeeks = max(omRules.GetAt(ID_WOR_MAWORD), omRules.GetAt(ID_WOR_MAWORDT));
		if (ilAppendWeeks == -1)
		{
			//parameters are turned off - we don't need to check
			ilAppendWeeks = 0;
		}
		else
		{
			//we have to append at least the first week at the end of the shiftroster pattern
			ilAppendWeeks = (int)(ilAppendWeeks / 7) + 1;
		}
		omCheckDateTo += COleDateTimeSpan(ilAppendWeeks*7,0,0,0);

		for (olTestDay = omCheckDateFrom; olTestDay < omCheckDateTo; olTestDay+=COleDateTimeSpan(1,0,0,0))
		{
			// Kalenderwoche feststellen: Wendepunkt ist 00:00 Montag
			int ilDayOfWeek = olTestDay.GetDayOfWeek();
			bool blCheckWeek = false;
			if (ilDayOfWeek == 2) // == Monday
			{
				blCheckWeek = true;
			}

			if(!polWorkTime->CheckIt(&olTestDay, DRR_CHECK, blCheckWeek))
			{
				blContinue = true;
				break;
			}
		}
		if(blContinue)
		{
			// dieser MA wird nicht weiter getestet, seine Schichten haben einen Fehler
			continue;
		}
	}
	
	return RET_NO_ERROR;
	CCS_CATCH_ALL;
	return RET_SOFTWARE_ERROR;
}

/*************************************************************************************
Liest alle Schichten und speichert in dem Datenarray
*************************************************************************************/
int	CedaShiftCheck::ReadShifts(int ipGPLPeri,CCSPtrArray<GSPTABLEDATA> &opGSPTableData)
{
	CCS_TRY;

	//	return, if there are no records to check
	if (opGSPTableData.GetSize() <= 0)
	{
		return RET_SOFTWARE_ERROR;
	}

	//	Initialisierung: Setz Lesedatum auf Untergrenze
	COleDateTime olBegTime = COleDateTime::GetCurrentTime();
	olBegTime.SetDateTime (olBegTime.GetYear(),olBegTime.GetMonth(),olBegTime.GetDay(),0,0,0);
	while (olBegTime.GetDayOfWeek() != 2) //we have to start with Monday!
	{
		olBegTime += COleDateTimeSpan (1, 0, 0, 0);
	}

	COleDateTime olBegin;
	COleDateTime olTmpBegin;
	COleDateTime olEnd = olBegTime + COleDateTimeSpan ((ipGPLPeri * 7) - 1, 0, 0, 0);

	//	Lege entsprechende Shift-Records an, versehen mit den Zeiteintr�gen zu BeginShift
	CedaWorkTime* polWorkTime;
	lmUrno = 1; // assigning "virtual" URNOs to the "virtual" DRR-records

	omCheckDateFrom = olBegTime;// + COleDateTimeSpan(7,0,0,0);
	omCheckDateTo = omCheckDateFrom + COleDateTimeSpan(ipGPLPeri * 7,0,0,0);

	DRRDATA* polDrr = new DRRDATA;
	polDrr->Rosl[0] = '1';
	polDrr->Ross[0] = 'A';

	int ilRotateWeek = 1; //determining the start-week of the current employee
	int ilTmpRotateWeek;
	olBegin = olBegTime;

	// to detect violations of "max. working days in series" at the break of the shiftplan
	// we have to take care to append the first week(s) at the end again.
	int ilAppendWeeks = max(omRules.GetAt(ID_WOR_MAWORD), omRules.GetAt(ID_WOR_MAWORDT));
	if (ilAppendWeeks == -1)
	{
		//parameters are turned off - we don't need to check
		ilAppendWeeks = 0;
	}
	else
	{
		//we have to append at least the first week at the end of the shiftroster pattern
		ilAppendWeeks = (int)(ilAppendWeeks / 7) + 1;
	}

	// "virtual" employee-Urno, representative for the employee
	polDrr->Stfu = 1;

	// important - for the "virtual" employee (stfu = 1) a new entry
	polWorkTime = new CedaWorkTime(1, this, imEName);
	if(!polWorkTime)
	{
		return RET_SOFTWARE_ERROR;
	}
	omMapStaffDutyData.RemoveKey(1);
	omMapStaffDutyData.SetAt(1, polWorkTime);

	olTmpBegin = olBegTime;
	ilTmpRotateWeek = 1;

	CString olStatusbarText;

	for (int ilWeek = 0; ilWeek < (ipGPLPeri + ilAppendWeeks); ilWeek++)
	{
		if (ilWeek + ilTmpRotateWeek > ipGPLPeri)
		{
			// if we expire ipGPLPeri, we have to start with the first
			// week: ilWeek + ilTmpRotateWeek MUST be 1
			ilTmpRotateWeek = 1 - ilWeek;	
		}

		olStatusbarText.Format (LoadStg(IDS_STRING63503), ilWeek);
		pomMe->GetParentFrame()->GetMessageBar()->SetWindowText(olStatusbarText);

		if (FillAWeek(polDrr, polWorkTime,(ilWeek + ilTmpRotateWeek),olTmpBegin,opGSPTableData) == RET_NO_ERROR)
		{
			// ok-no error, so let's do the next week
			olTmpBegin += COleDateTimeSpan(7,0,0,0);
		}
		else
		{
			break;
		}
	}

	delete polDrr;
	return RET_NO_ERROR;

	CCS_CATCH_ALL;
	return RET_SOFTWARE_ERROR;
}

/***********************************************************************************************************************
Liest alle Schichten einer Woche und speichert im Datenarray
***********************************************************************************************************************/

int	CedaShiftCheck::FillAWeek(DRRDATA* prpDrr, CedaWorkTime* popWorkTime, int ipPeri,COleDateTime opBegin,CCSPtrArray<GSPTABLEDATA> &opGSPTableData)
{
	CCS_TRY;
	int					index;
	CString				olWeek, olKey;
	GSPTABLEDATA		rlData;
	BSDDATA				*prlBsd;
	DRRDATA				olDrr;
	CString				*polScod;		// Schicht

	olWeek.Format("%03d",ipPeri);

	for (index = 0; index < opGSPTableData.GetSize(); index++)
	{
		if (opGSPTableData[index].Week == olWeek)
		{
			rlData = opGSPTableData[index];
			break;
		}
	}
	if (index < opGSPTableData.GetSize())
	{
		long	llActUrno;
		bool	blHasFirstShift;

		for (int ilDay = 0; ilDay < 7; ilDay++, opBegin += COleDateTimeSpan(1,0,0,0))
		{
			blHasFirstShift = false;

			for (int ilShiftNo = 1; ilShiftNo <= 2; ilShiftNo++)
			{
				// we have to process the second shift only if there is a first shift.
				// so we keep in mind if there was a valid first shift (DayUrno > 0).
				// Anyway second shifts without a first shift aren't released in the shiftroster.
				if (ilShiftNo == 1) // 1 Schicht
				{
					llActUrno = atol(rlData.DayUrno[ilDay]);
					polScod = &rlData.Day[ilDay];
					if (llActUrno > 0)
					{
						blHasFirstShift = true;
					}
				}
				else if (ilShiftNo == 2) // 2 Schicht
				{
					if (blHasFirstShift != true)
					{
						break;
					}

					llActUrno = atol(rlData.Day2Urno[ilDay]);
					polScod = &rlData.Day2[ilDay];
				}

				// setting the shift-code
				strcpy(prpDrr->Scod, (LPCTSTR)*polScod);

				// setting the shift-number
				itoa(ilShiftNo, &prpDrr->Drrn[0], 10);

				// lokale Urno
				prpDrr->Urno = lmUrno;
				lmUrno++;

				prlBsd = ogBsdData.GetBsdByUrno(llActUrno);
				if (prlBsd > NULL)
				{
					// das ist eine Schicht
					
					//---------------------------------------------------------------------------------------------------------------------
					// AVFR einstellen aus opBegin und BSD->ESBG (fr�hester Schichtbeginn)
					//---------------------------------------------------------------------------------------------------------------------
					
					// ESBG umwandeln in Zeit
					COleDateTimeSpan olEsbgSpan;
					if (!CedaDataHelper::HourMinStringToOleDateTimeSpan(prlBsd->Esbg,olEsbgSpan)) return RET_SOFTWARE_ERROR;	// bei Fehler abbrechen
					prpDrr->Avfr = opBegin + olEsbgSpan;
					
					//---------------------------------------------------------------------------------------------------------------------
					// AVTO einstellen aus DRR->und BSD->LSEN (sp�testes Schichtande)
					//---------------------------------------------------------------------------------------------------------------------
					// LSEN umwandeln in Zeit
					COleDateTimeSpan olLsenSpan;
					if (!CedaDataHelper::HourMinStringToOleDateTimeSpan(prlBsd->Lsen,olLsenSpan)) return RET_SOFTWARE_ERROR;	// bei Fehler abbrechen
					// Sp�testes Schichtende fr�her als fr�hester Schichtbeginn?
					if (olLsenSpan <= olEsbgSpan)
					{
						// ja -> einen Tag zus�tzlich addieren
						olLsenSpan += COleDateTimeSpan(1,0,0,0);
					}
					// Anwesend bis einstellen: SDAY + LSEN
					prpDrr->Avto = opBegin + olLsenSpan;
					
					//---------------------------------------------------------------------------------------------------------------------
					// SBFR einstellen aus opBegin und BSD->BKF1 (Pausenbeginn)
					//---------------------------------------------------------------------------------------------------------------------
					// BKF1 umwandeln in Zeit
					COleDateTimeSpan olBkf1Span;
					if(ogBsdData.GetBkf1Time(prlBsd, olBkf1Span))
					{
						// Pause von einstellen: SDAY + BKF1
						prpDrr->Sbfr = opBegin + olBkf1Span;
					}
					else prpDrr->Sbfr.SetStatus(COleDateTime::null);
					
					//---------------------------------------------------------------------------------------------------------------------
					// SBTO einstellen aus DRR->und BSD->BKT1 (Pausenende)
					//---------------------------------------------------------------------------------------------------------------------
					// BKT1 umwandeln in Zeit
					COleDateTimeSpan olBkt1Span;
					if(ogBsdData.GetBkt1Time(prlBsd, olBkt1Span))
					{
						// Pause bis einstellen: SDAY + BKT1
						prpDrr->Sbto = opBegin + olBkt1Span;
					}
					else prpDrr->Sbto.SetStatus(COleDateTime::null);
					
					//---------------------------------------------------------------------------------------------------------------------
					// Pausenl�nge einstellen
					//---------------------------------------------------------------------------------------------------------------------
					strcpy(prpDrr->Sblu,prlBsd->Bkd1);
				}
				else
				{
					// FREE / R_FREE
					prpDrr->Avfr = opBegin;
					prpDrr->Avto = opBegin;
					prpDrr->Sbfr = opBegin;
					prpDrr->Sbto = opBegin;
					
				}
				popWorkTime->UpdateData(prpDrr, DRR_NEW);
			}
		}
	}
	else
	{
		return RET_SOFTWARE_ERROR;
	}
	
	return RET_NO_ERROR;
	CCS_CATCH_ALL;
	return RET_SOFTWARE_ERROR;
}



void CedaShiftCheck::SetViewEName(int ipEName)
{
	imEName = ipEName;
}
