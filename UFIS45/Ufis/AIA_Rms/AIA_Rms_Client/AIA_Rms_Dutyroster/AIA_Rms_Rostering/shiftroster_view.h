#if !defined(AFX_SHIFTROSTER_VIEW_H__978497A1_5C56_11D3_8EF7_00001C034EA0__INCLUDED_)
#define AFX_SHIFTROSTER_VIEW_H__978497A1_5C56_11D3_8EF7_00001C034EA0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ShiftRoster_View.h : header file
//

#include <stdafx.h>
#include <basicdata.h>
#include <CViewer.h>
#include <CedaBsdData.h>
#include <CedaOdaData.h>
#include <CedaSdtData.h>
#include <CedaMsdData.h>
#include <CedaStfData.h>
#include <CedaSdgData.h>
#include <CedaPfcData.h>
#include <CedaScoData.h>
#include <CedaDrrData.h>
#include <ShiftAWDlgBonus.h>

// spezial ToolBar
#include <combobar.h>

class ShiftCodeNumberDlg;
class ShiftKonsistDlg;

// Maximale H�he und Breite des Fensters
#define MAX_SHIFT_WIDTH		1024
#define MAX_SHIFT_HEIGHT	786

//***************************************************************************
//
//***************************************************************************

enum							
{
	SHIFTDEMAND=100,SHIFTPLAN,
	VIEW_DIFF,VIEW_ISDEBIT,							//Ansicht
	VIEW_ALLGPL,VIEW_NOTALLGPL,						//Ansicht
	VIEW_NOTHING,VIEW_GRUPP,VIEW_DETAIL,VIEW_BONUS, //Ansicht
	VIEW_FLNAME,VIEW_CODE,VIEW_FREE,				//Ansicht
	RECORDCHANGED,
	DETAIL,SUM,GRUPP,								//+DETAIL,BONUS //NSRTABLEDATA_Type 
	BONUS,EMPLOYEE,									//Differentiation for ShiftAWDlg
	WEEK,DAY,NUMBER,
	SPL,GPL,ALL,									//CodeNumbe
	MENU_ADD_LINE,
	MENU_DEL_LINE,
	MENU_CANCEL_ACTION
};

//***************************************************************************
// Farben f�r Oberfl�chenelemente (Tabellenzellen)
//***************************************************************************
#define SELECTBLUE	  RGB(143, 239, 255)  
#define TESTCOLOR	  RGB(100, 100, 100)  

//***************************************************************************
// Verkn�pfung vom CCSEdit Feld aus GSPTABLE
// zum CString aus GSPTABLEDATA
// �ber CMapPtrToPtr omGSPTablePtrMap
//***************************************************************************
struct GSPTABLEPTR
{
	bool bSetBk;
	int Record;
	int Day;
	CString BsdCode;
	CString	BsdUrno;
	CString PfcCode;
	CString PfcUrno;

	GSPTABLEPTR()
	{
		Record	= 0;
		Day		= 0;
		bSetBk	= false;
	}
};

//***************************************************************************
// ShiftRoster
//***************************************************************************

struct GSPTABLE 
{
	CCSEdit *Week;
	CCSEdit *Number;
	CCSEdit *Houers;		// das heisst hours 
	CCSEdit *Day[7];		// 0 - Mon, 1 - Tue, 2 - Wnd, 3 - Thu, 4 - Fri, 5 - Sat, 6 - Sun
	CCSButtonCtrl *Staff;
	CCSDragDropCtrl ButtonsDragDrop;

	GSPTABLE()
	{
		Week	= NULL;
		Number	= NULL;
		Houers	= NULL;
		Staff	= NULL;
		for (int i = 0; i < 7; i++)
		{
			Day[i] = NULL;
		}
	}
};

//***************************************************************************
// ShiftRoster
//***************************************************************************

struct GSPTABLEDATA 
{
	CString GSPUrno;
	CString Week;
	CString	Number;
	CString StaffFLName;
	CString StaffCode;
	CString StaffFree;
	CString Day[7];
	CString Day2[7];
	CString Houers;
	CString StaffUrno;
	CString DayUrno[7];
	CString Day2Urno[7];
	CString PfcCode[7];
	CString PfcCode2[7];
	CString PfcUrno[7];
	CString PfcUrno2[7];

	int IsChanged;

	GSPTABLEDATA()
	{	
		GSPUrno		= "0";
		Number		= "0";
		Houers		= "0.00";
		DayUrno[0]	= "0";
		DayUrno[1]	= "0";
		DayUrno[2]	= "0";
		DayUrno[3]	= "0";
		DayUrno[4]	= "0";
		DayUrno[5]	= "0";
		DayUrno[6]	= "0";
		IsChanged	= 0;
	}
};

//***************************************************************************
// Change GSP Data
//***************************************************************************

struct CHANGEGSPDATA 
{
	int Day;
	int Record;
	int	NewNumber;
	int	OldNumber;

	CString NewBSDUrno;
	CString NewBSDCode;
	CString New2BSDUrno;
	CString New2BSDCode;
	CString NewPfcUrno;
	CString NewPfcCode;
	CString NewPfc2Urno;
	CString NewPfc2Code;

	CString OldBSDUrno;
	CString OldBSDCode;
	CString Old2BSDUrno;
	CString Old2BSDCode;
	CString OldPfcUrno;
	CString OldPfcCode;
	CString OldPfc2Urno;
	CString OldPfc2Code;

	CHANGEGSPDATA()
	{	
		Day			= 0;
		Record		=-1;
		NewNumber	= 0;
		OldNumber	= 0;
	}
};

//***************************************************************************
// NettoShiftRequirement
//***************************************************************************

struct NSRTABLE
{
	CCSEdit *Grupp;
	CCSEdit *Day[7];	// 0 - Mon, 1 - Tue, 2 - Wnd, 3 - Thu, 4 - Fri, 5 - Sat, 6 - Sun

	NSRTABLE()
	{
		Grupp = NULL;
		for (int i = 0; i < 7; i++)
		{
			Day[i] = NULL;
		}
	}
};

//***************************************************************************
// // NettoShiftRequirementData
//***************************************************************************

struct NSRTABLEDATA
{
	int Type;					// to diffrent between Bedarf(DETAIL), Beaufschlagung (BONUS), Sum/first line (SUM), Grupps(GRUPP)
	CString Grupp;
	CString omShiftTime;		// Schichtbeginn+Schichtende - f�rs Sortieren, z.B. 05301545
	CString GruppUrno;
	CString Fctc;
	int MonIs;
	int TueIs;
	int WndIs;
	int ThuIs;
	int FriIs;
	int SatIs;
	int SunIs;
	int MonDebit;
	int TueDebit;
	int WndDebit;
	int ThuDebit;
	int FriDebit;
	int SatDebit;
	int SunDebit;

	NSRTABLEDATA(void)
	{	
		Type		= DETAIL;
		GruppUrno	= "0000000000";
		MonIs		= 0;
		TueIs		= 0;
		WndIs		= 0;
		ThuIs		= 0;
		FriIs		= 0;
		SatIs		= 0;
		SunIs		= 0;
		MonDebit	= 0;
		TueDebit	= 0;
		WndDebit	= 0;
		ThuDebit	= 0;
		FriDebit	= 0;
		SatDebit	= 0;
		SunDebit	= 0;
	}
};

//***************************************************************************
// Bonus Data
//***************************************************************************

struct BONUSDATA
{
	CString DBOurno;	//Beaufschlagungs Urno
	CString DBOdbab;	//Aufschlag Abwesenheit
	CString DBOdbho;	//Aufschlag Urlaub
	CString DBOroup;	//Aufrunden ab
	CString DBObgru;	//Beaufschlagungs-Gruppe URNO
	CString BsdList;	//Basisschichten Liste |-Getrennt
	CString BsdUrnoList;//Basisschichten-Urno Liste |-Getrennt
	CString SGRgrpn;	//Gruppenname
	int IsChanged;

	BONUSDATA()
	{	
		DBOurno			= "0000000000";
		DBOdbab			= "0";
		DBOdbho			= "0";
		DBOroup			= "0.5";
		DBObgru			= "0";
		IsChanged		= 0;
	}
};

//***************************************************************************
// Code Number Data
//***************************************************************************

struct CODENUMBERDATA
{
	int NrOfWeeks;
	int Work;
	int Free;
	double YearDebit;
	double YearIs;
	double PerWeek;
	double PerDay;
	int HouersGSP;
	int HouersAll;
	int MaSchedule;
	int MaAssign;
	int MaScheduleAll;
	int MaAssignAll;

	CODENUMBERDATA(void)
	{	
		NrOfWeeks		= 0;
		Work			= 0;
		Free			= 0;
		YearDebit		= 0.0;
		YearIs			= 0.0;
		PerWeek			= 0.0;
		PerDay			= 0.0;
		HouersGSP		= 0;
		HouersAll		= 0;
		MaSchedule		= 0;
		MaAssign		= 0;
		MaScheduleAll	= 0;
		MaAssignAll		= 0;
	}
};

//***************************************************************************
// Selected fields in GSP Table
//***************************************************************************

struct SELECTGSPTABLEDATA
{
	CCSEdit *SourceControl;
	CString	Text;

	SELECTGSPTABLEDATA()
	{
		SourceControl = NULL;
	}
};

//***************************************************************************
// Auszug aus GRN
//***************************************************************************

struct GRUPPDATA
{
	CString Grpn; 	// Beaufschlagungs-Gruppenname (aus GRN)
	CString Valu; 	// Werte der Grupierung (BSD-Urnos aus GRM)
	CString	Urno;  	// Eindeutige Datensatz-Nr. (GRN)
};

//***************************************************************************
// Mitarbeiter, ihre Schichten und G�ltigkeit
//***************************************************************************

struct REL_STAFF_DATA
{
	long	lStfUrno;
	int		iBeginWeek;
	CStringArray oShifts;
	CStringArray oShifts2;
	CStringArray oFunctions;
	CStringArray oFunctions2;

	COleDateTime oGPLVafr;
	COleDateTime oGPLVato;

	COleDateTime oRelFrom;
	COleDateTime oRelTo;

	COleDateTime oDODM; //leaving date
	COleDateTime oDOEM; //entry date
	COleDateTime oDOBK; //re-entrance date

	REL_STAFF_DATA()
	{
		lStfUrno	= 0;
		iBeginWeek	= 0;
		oGPLVafr.SetStatus(COleDateTime::invalid);
		oGPLVato.SetStatus(COleDateTime::invalid);
		oRelFrom.SetStatus(COleDateTime::invalid);
		oRelTo.SetStatus(COleDateTime::invalid);
		oDODM.SetStatus(COleDateTime::invalid);
		oDOEM.SetStatus(COleDateTime::invalid);
		oDOBK.SetStatus(COleDateTime::invalid);
	}
};

//***************************************************************************
// ShiftRoster_View form view
//***************************************************************************

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

class ShiftRoster_View : public CFormView
{
protected:
	ShiftRoster_View();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(ShiftRoster_View)

// Form Data
public:
	//{{AFX_DATA(ShiftRoster_View)
	enum { IDD = IDD_SHIFTROSTER_DLG };
	CButton			m_B_SecondShift;	// wenn highlighted - alle Doppelschicht-Zellen sollen statt erster - zweite Schicht anzeigen und bearbeiten
	CButton			m_ButDummy;
	CStatic			m_S_Null;
	CButton			m_B_Stfs_P;
	CButton			m_B_Stfs_M;
	CButton			m_B_Konsist;
	CButton			m_B_Info;
	CCSEdit			m_E_Null;
	CComboBox		m_CB_Bonus;
	CButton			m_B_Bonus;
	CSpinButtonCtrl	m_SBC_Periode;
	CCSEdit			m_E_GPL_Peri;
	CCSEdit			m_E_GPL_Caption;
	CListBox		m_LB_GPLList;
	CStatic			m_s_GSPField;
	CScrollBar		m_SB_NSR;
	CComboBox		m_CB_Contract;
	CScrollBar		m_SB_GSP;
	CStatic			m_S_Week;
	CStatic			m_S_We;
	CStatic			m_S_Tu;
	CStatic			m_S_Th;
	CStatic			m_S_Su;
	CStatic			m_S_Staff;
	CStatic			m_S_Sa;
	CStatic			m_S_Number;
	CStatic			m_S_Mo;
	CStatic			m_S_Houers;
	CStatic			m_S_Fr;
	CButton			m_B_New;
	CButton			m_B_Copy;
	CButton			m_B_Assign;
	CButton			m_B_Delete;
	CStatic			m_S_Valid;
	CStatic			m_S_Contract;
	CStatic			m_S_Holiday;
	CStatic			m_S_Absence;
	CStatic			m_S_Roundup;
	CCSEdit			m_E_GPL_Vafr;
	CCSEdit			m_E_GPL_Vato;
	CCSEdit			m_E_Absence;
	CCSEdit			m_E_Holiday;
	CCSEdit			m_E_Roundup;
	CCSEdit			m_E_ReleaseNote;
	CComboBox		m_CB_OrgUnit;
	CStatic			m_S_OrgUnit;
	//}}AFX_DATA

// Attributes
public:
	//uhi 25.4.01
	CString omViewQualificationUrnos;
	CString omViewFunctionUrnos;

// Operations
public:
	CString GetFormatedEmployeeName(STFDATA* prpStfData);
	NameConfigurationDlg* GetNamesConfigurationDlg();
	int GetStaffUrnosBySplUrno(CString opSplu, CMapStringToPtr *ropStfuMap);
	CString GetSelectedShiftplanUrno();
	//BC-processing
	void ProcessBCs(int ipDDXType, void *vpDataPointer);
	void ProcessSdgBc(int ipDDXType, void *vpDataPointer);

	// wird von ShiftAWDlg gecalled, wenn MAs aus Zuweisung gel�scht werden sollen
	void OnDeleteEmployees(CString pDeleteUrnos);

	// wird von ShiftAWDlg gecalled, wenn MAs aus zugewiesen werden sollen
	void OnAddEmployees(CString pNewUrnos, CCSButtonCtrl* pButton);

	// Abspeichern Sortierung
	CGXSortInfoArray omSortInfo;
	
	// Merker, ob alle MAs in ShiftAWDlg gezeigt werden sollen oder nur die noch nicht zugewiesenen
	bool bmShowAll;

	// Merker, ShiftAWDlg ge�ffnet
	ShiftAWDlg *pomShiftAWDlg;
	bool bmShiftAWDlgVisible;

	CString	omStaffUrnosOfLine;
	CString	omStaffUrnosAll;
	CString omSelSPLurno;
	CString omSelGPLurno;

	// Abfrage ob gesichert werden soll und ob der Dialog beendet werden kann
	bool OkToExit();

	void OnChangeView();

	// MA D'n'D darf nur in aktivierte GPLs funktionierten, da die Konsistenz-Pr�fung nur f�r solche funktioniert
	bool IsVisibleGplActive();

	//min & max-times of the GPLs to initzialize release-dialog's time-fields
	bool GetMinMaxGplTimesBySplu(CString opSPLurno, COleDateTime *ropFrom, COleDateTime *ropTo);

	void Release(CString opSplUrno, COleDateTime opFrom, COleDateTime opTo, CMapStringToPtr *popStaffMap, bool bpDelete = false);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ShiftRoster_View)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
		
// Implementation
protected:
	void Register();
	void InitDefaultView();
	void InitComboBox();
	virtual ~ShiftRoster_View();

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// l�dt Schichtdaten aus der Datenbank
	void LoadData();
	// das Soll eines Grundschichtplans anpassen
	void AddDebitDay(NSRTABLEDATA *prpData,CTime opEsbg,int ipDay=0,bool bpIsDay=false);
	// ermitteln eines Nettoschichtbedarfs anhand des Codes
	NSRTABLEDATA* FindNSRTableGrupp(CString opBsdc, CString opFctc = "");
	// f�llt die Grundschichtplan-Listbox
	void Build_LB_GPL(CString opFilter="");
	// Eintr�ge in Grundschichtplan-Listbox einf�gen
	void InsertIn_LB_GPL(RecordSet *popRecord, CString opFilter = "");
	// Comboboxen aktualisieren
	void UpdateComboBox();
	// ???
	void SaveBonus();
	// ???
	void Build_Bonus(CString opSPLurno);
	// ???
	void SaveGPL();
	// ???
	void GetShiftRequirementData(int ipTab, int ipScrollPos = 3);
	// ???
	void DisplayShiftRosterData(int ilSet = 0);
	// ???
	void DisplayShiftRequirementData(int ilVariant);
	// ???
	void SetCodeNumberNull(UINT ipTyp);
	// ???
	void AddOrSubIsDay(NSRTABLEDATA *prpData,int ipDay,int ipFaktor = 1,int ipNr = 1);
	// ???
	void DeleteNullFromShiftRequirementData();
	// ???
	void ChangeShiftRequirementBonusData();
	// ???
	void ChangeShiftRequirementGruppData();
	// ???
	//void FillGSPTABLEPTR(void *pvpField,CString *popValid,CString *popUrno,int ipRecord,bool bpSetBk);
	void FillGSPTABLEPTR(void *pvpField, CString opBsdCode, CString opBsdUrno, CString opPfcCode,
									   CString opPfcUrno, int ipRecord, bool bpSetBk);
	// ???
	void SetShiftRequirementText(CCSEdit *popField,int ipIs, int ipDebit,int ipView = VIEW_ISDEBIT,bool bpNull = false);
	// ???
	GSPTABLEPTR* FindGSPTablePtr(void *pvpField);
	// ???
	void GetShiftRosterData(CString opGPLUrno);
	// ???
	void DeleteSelectGSPTableFields();
	// ???
	bool GetStaffListByUrnoList(CString opUrnoList,CString *popStaffListFLName,CString *popStaffListCode,CString* popStaffFree);
	void CheckRemoveStfus(RecordSet *popGSPRecord);
	// ???
	void AddWeekToGPL();
	// ???
	void DeleteWeekfromGPL();
	// ???
	void ChangeShiftRequirementData(int ipTyp);
	
// *************** Initialisierungsfunktionen *************************
	// Eigenschaften der Oberfl�chenelemente wiederherstellen
	void RestoreWindowSettings(void);
	// sprachabh�ngigen Strings laden
	void LoadStrings(void);
	// Farben und Formate des CCSEdits einstellen
	void InitCCSEditControls(void);
	// Position und Gr��e des Fensters herstellen
	CRect CalcInitialWindowSizeAndPos() ;
		// Buttons initialisieren
	void SetInitialButtonStates();

	/****************************************************************************
	DayButtons erstellen
	****************************************************************************/
	void CreateDayButtons(int ipDay, GSPTABLE *prpGSPTABLE, int* pipTmpLeftPos, DWORD opEditStyle_Edit, int ipTopPos, int ipWidth3, int ipHight, int ipFirstEditID, int ipGSPLine, int ipAddPix);

	/****************************************************************************
	NSR Table erstellen
	****************************************************************************/
	void CreateNSRTable(NSRTABLE *prpNSRTABLE, int ipDay, DWORD opEditStyle_noEdit_Center, CRect opRect);

	/***************************************************************
	Holt Schichtdaten eines Tages
	***************************************************************/
	void GetDayStuff(CString opUrno, CString op2Urno, CString opPfc, CString op2Pfc, int ipDay, GSPTABLEDATA *prpGplData, double* dpMinutes);

	// Testet ob der Code Dialog sichtbar ist.
	bool IsCodeDlgActive();

	// Testet ob der Konsist Dialog sichtbar ist.
	bool IsKonsistDlgActive();

	// Objekt f�r D'n'D-Funktionalit�t
	CCSDragDropCtrl omDragDropObject;

	// Zeiger auf den Dialog mit Liste der Schicht-Codes
	ShiftCodeNumberDlg *pomShiftCodeNumberDlg;

	// Zeiger auf den Dialog zur Konsistenzpr�fung 
	ShiftKonsistDlg    *pomShiftKonsistDlg;

	// die Bedarfs-Ansicht 
	CViewer *pomViewer;

	CCSPtrArray<GSPTABLEPTR>	omGSPTablePtr;
	CCSPtrArray<GSPTABLE>		omGSPTable;
	CCSPtrArray<GSPTABLEDATA>	omGSPTableData;
	CCSPtrArray<GSPTABLEDATA>	omDeletedGSPs;
	CCSPtrArray<CHANGEGSPDATA>	omChangedGSPs;
	CMapPtrToPtr				omGSPTablePtrMap;

    CCSPtrArray<NSRTABLE>		omNSRTable;
    CCSPtrArray<NSRTABLEDATA>	omNSRTableData;
	CMapStringToPtr				omNSRTableDataMap;

	// Array mit Zeigern auf die Schichtbedarfs Gruppen (Aus der Combobox ausw�hlbar)
    CCSPtrArray<BONUSDATA>	omBonusData;
    CCSPtrArray<GRUPPDATA>	omGruppData;
    CCSPtrArray<SELECTGSPTABLEDATA> omSelectGSPTableData;

	CPoint omDropPosition;
	bool bmIsDragDropAction;

	NSRTABLEDATA rmActualShifData;
	CODENUMBERDATA rmCodeNumberData;

	int imActualLine;
	int imFirstButtonID;
	int imFirstEditID;
	CString omOldFieldText;
	CString omActualSPLurnoByGPL;

	// Register changes
	bool bmIsChangedGPL;
	bool bmIsChangedBonus;

	// ScrollBar ranges
	int imGSPRangeMin;
	int imGSPRangeMax;
	int imNSRRangeMin;
	int imNSRRangeMax;
	// end

	int imGSPLines;	//the most important GSP Lines
	int imNSRLines;	//the most important NSR Lines

	CRect omGSPTableRect;
	CRect omNSRTableRect;

	CString omObject;
	CString omFields;
	CString omSortFields;
	CString omAktiv;

/* ToDo: pomRecord �berall durch lokale Objekt ersetzen (wird jedesmal bisher unsinnigerweise neu erzeugt und gel�scht) */
	RecordSet *pomRecord;

	//Sort NSRTable
	CString omSortCode;
	CString omSortGrupp;
	CString omSortBonus;

	// ViewerParameter (f�r Ansichten)
	int	imViewStattGrupp;
	int imViewStattDetail;
	int imViewStattBonus;
	int imViewShowGPL;
	int imViewShowCover;
	int imViewShowEmployee;
	CString omViewFunctionFctcs;
	// end

	// Zeiger auf die Comboboxen im Frame
	CComboBox* pomComboBoxView;
	CComboBox* pomComboBoxCoverage;
	CComboBox* pomComboBoxShiftplan;

	// remember whether the first or the second shifts are shown
	int imShowShiftNumber;

	// Events f�r die Editfelder
	void OnGSPTable_EditLButtonDown(UINT ipID, LONG rpNotify);
	void OnGSPTable_EditRButtonDown(UINT ipID, LONG rpNotify);
	void OnGSPTable_EditKillfocus(UINT ipID, LONG rpNotify);
	void OnGSPTable_R_ButtonKlick(UINT ipID, LONG rpNotify);
	void OnGSPTable_L_ButtonKlick(UINT nID, LONG rpNotify);

	// Test ob DnD m�glich und dann durchf�hren (Schichten von Tabelle unten nach oben ziehen
	void BeginNSRDragAndDrop(CCSEDITNOTIFY *prlNotify);
	
	// Generated message map functions
	//{{AFX_MSG(ShiftRoster_View)
	afx_msg void OnBView();
	afx_msg void OnBRequirement();
	afx_msg void OnBPlan();
	afx_msg void OnBSaveplan();
	afx_msg void OnBRelease();
	afx_msg void OnBCodenumber();
	afx_msg void OnSelchangeCbView();
	afx_msg void OnSelchangeCbRequirement();
	afx_msg void OnSelchangeCbBonus();
	afx_msg void OnSelchangeLbGpllist();
	afx_msg void OnDblclkLbGpllist();
	afx_msg void OnBInfo();
	afx_msg void OnBCheck();
	afx_msg void OnBNew();
	afx_msg void OnBCopy();
	afx_msg void OnBAssignGPL();
	afx_msg void OnBDelete();
	afx_msg void OnBKonsist();
	afx_msg void OnBBonus();
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnSelchange_CB_SPL();
	afx_msg void OnSelchange_CB_Contract();
	afx_msg LONG OnDrop(UINT wParam, LONG lParam);
	afx_msg LONG OnDragOver(UINT wParam, LONG lParam);
	afx_msg LONG OnInplaceReturn(UINT, LONG); 
	afx_msg void OnBStfsP();
	afx_msg void OnBStfsM();
	afx_msg void OnBSecondshift();
	afx_msg void OnDestroy();
	afx_msg void OnCancel();
	afx_msg void OnShiftPrint();
	afx_msg BOOL OnTtnNeedText(UINT id,NMHDR *pTTTStruct,LRESULT *pResult);
	afx_msg void OnSelchange_CB_OrgUnit();
	afx_msg void OnChangeEGplFilter();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	void HandleNSRTable(CString opBsdUrno, CString opPfcUrno, int ipNumber, int ipDay);

	// called when an employee was droped from the ShiftAWDlg to a week
	void OnDropStaff(CString pNewUrnos, CWnd *pWnd, GSPTABLEDATA *pGspTableData, GSPTABLE *pGspTable);
	
	// displaying a message-box if an overcapacity has been produced
	void OverCapacityMsgBox(CString opWeek);

	void OnMenuAddLineToGSP();
	void OnMenuDeleteLineFromGSP();
	void OnMenuCancelAction();
	int imClickedLine;
	
	// holding URNOs of the already selected employees
	CString om_SelAWDlgUrnos;

	// Tooltips giving information about shift-code and function-code
	CToolTipCtrl omToolTipCtrl;

	//remember the customer to adapt shiftrostering-behavior
	CString omCustomer;

	// period of the spin-button
	int im_SBC_Periode;

	// index-numbers of the GSPTAB
	// general stuff
	int imGSPurnoIdx;
	int imGSPgpluIdx;
	int imGSPweekIdx;
	int imGSPnstfIdx;
	int imGSPstfuIdx;
	int imGSPstfnIdx;

	// erste Schicht-URNOs
	int imGSPbumoIdx;
	int imGSPbutuIdx;
	int imGSPbuweIdx;
	int imGSPbuthIdx;
	int imGSPbufrIdx;
	int imGSPbusaIdx;
	int imGSPbusuIdx;

	// zweite Schicht-URNOs
	int imGSPsumoIdx;
	int imGSPsutuIdx;
	int imGSPsuweIdx;
	int imGSPsuthIdx;
	int imGSPsufrIdx;
	int imGSPsusaIdx;
	int imGSPsusuIdx;

	// erste Funktion-URNOs
	int imGSPp1moIdx;
	int imGSPp1tuIdx;
	int imGSPp1weIdx;
	int imGSPp1thIdx;
	int imGSPp1frIdx;
	int imGSPp1saIdx;
	int imGSPp1suIdx;

	// zweite Funktion-URNOs
	int imGSPp2moIdx;
	int imGSPp2tuIdx;
	int imGSPp2weIdx;
	int imGSPp2thIdx;
	int imGSPp2frIdx;
	int imGSPp2saIdx;
	int imGSPp2suIdx;

	NameConfigurationDlg *pomNameConfigDlg;
};
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SHIFTROSTER_VIEW_H__978497A1_5C56_11D3_8EF7_00001C034EA0__INCLUDED_)
extern ShiftRoster_View *pomMe;
