#ifndef _CEDADRDDATA_H_
#define _CEDADRDDATA_H_

#include <stdafx.h>
/*#include "CCSPtrArray.h"
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <ccsglobl.h>
#include <CedaDrrData.h> // f�r IsValidDrd()
#include <CedaDelData.h>*/


///////////////////////////////////////////////////////////////////////////// 

// COMMANDS FOR MEMBER-FUNCTIONS
#define DRD_SEND_DDX	(true)
#define DRD_NO_SEND_DDX	(false)

// Felddimensionen
#define DRD_DPT1_LEN	(8)
#define DRD_PRMC_LEN	(90)

// Struktur eines DRD-Datensatzes
struct DRDDATA 
{
	char			Dpt1[DRD_DPT1_LEN+2];	// Organisationseinheit (Code aus ORG)
	COleDateTime	Drdf;					// Abweichung von HHMM
	COleDateTime	Drdt;					// Abweichung bis HHMM
	char			Drrn[DRRN_LEN+2]; 	// Schichtnummer (1-n pro Tag)
	char 			Fctc[FCTC_LEN+2]; 	// Funktion des Mitarbeiters (Code aus PFC)
	char 			Prmc[DRD_PRMC_LEN+2]; 	// Qualifikationen (max. 15 Codes aus PER, getrennt mit '|')
	char 			Sday[SDAY_LEN+2]; 	// Tages-Schl�ssel YYYYMMDD
	long 			Stfu;					// Mitarbeiter-Urno
	long 			Urno;					// Datensatz-Urno

	//DataCreated by this class
	int			IsChanged;	// Check whether Data has Changed f�r Relaese

	// Initialisation
	DRDDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged = DATA_UNCHANGED;
		Drdf.SetStatus(COleDateTime::invalid);		// Abweichung von
		Drdt.SetStatus(COleDateTime::invalid);		// Abweichung bis
	}
};	

// the broadcast CallBack function, has to be outside the CedaDrdData class
void ProcessDrdCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//************************************************************************************************************************
//************************************************************************************************************************
// Deklaration CedaDrdData: kapselt den Zugriff auf die Tabelle DRD
//************************************************************************************************************************
//************************************************************************************************************************

class CedaDrdData: public CedaData
{
// Funktionen
public:
    // Konstruktor/Destruktor
	CedaDrdData(CString opTableName = "DRD", CString opExtName = "TAB");
	~CedaDrdData();

// allgemeine Funktionen
	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}
	// Feldliste lesen
	CString GetFieldList(void) {return CString(pcmListOfFields);}

// Broadcasts empfangen und bearbeiten
	// behandelt die Broadcasts BC_DRD_CHANGE,BC_DRD_DELETE und BC_DRD_NEW
	void ProcessDrdBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

// Lesen/Schreiben von Datens�tzen
	// Datens�tze nach Datum gefiltert lesen
	bool ReadFilteredByDateAndStaff(COleDateTime opStart, COleDateTime opEnd,
									CMapPtrToPtr *popLoadStfUrnoMap = NULL,
									bool bpRegisterBC = true, bool bpClearData = true);
	// Datens�tze einlesen
	bool Read(char *pspWhere = NULL, CMapPtrToPtr *popLoadStfUrnoMap = NULL,
			  bool bpRegisterBC = true, bool bpClearData = true);
	// Datens�tze mit Hilfe selbst-formatierter SQL-String einlesen
	bool ReadSpecialData(CCSPtrArray<DRDDATA> *popDrdArray,char *pspWhere,char *pspFieldList,bool ipSYS);
	// Datensatz mit bestimmter Urno einlesen
	bool ReadDrdByUrno(long lpUrno, DRDDATA *prpDrd);
	
	// alle DRDs eines Mitarbeiters aus der internen Datenhaltung entfernen (ohne aus der DB zu l�schen!!!)
	bool RemoveInternalByStaffUrno(long lpStfUrno, bool bpSendBC = false);
	// einen Datensatz aus der Datenbank l�schen
	bool Delete(DRDDATA *prpDrd, bool bpSave = true);
	// einen neuen Datensatz in der Datenbank speichern
	bool Insert(DRDDATA *prpDrd, bool bpSave = true);
	// einen ge�nderten Datensatz speichern
	bool Update(DRDDATA *prpDrd,bool bpSave = true);

// Datens�tze suchen
	// DRD mit Urno <lpUrno> suchen
	DRDDATA*	GetDrdByUrno(long llUrno);
	// einen Array vom Typ CMapPtrToPtr mit den Zeigern auf die Datens�tze f�llen, die dem DRR <popDrr> zugeordnet sind
	int GetDrdMapOfDrr(CMapPtrToPtr *popDrdMap, DRRDATA *popDrr);

// Zugriff auf interne Daten
	// liefert den Zeiger auf die interne Datenhaltung <omData>
	DRDDATA		GetInternalData (int ipIndex) {return omData[ipIndex];}
	// liefert die Anzahl der Datens�tze in der internen Datenhaltung <omData>
	int			GetInternalSize() {return omData.GetSize();}
	// pr�fen, ob es einen DRD zu einem DRR gibt
	bool HasDrdOfDrr(DRRDATA *popDrr);
	// pr�fen ob es einen zu einem DRR gibt, zu dem es keinen DEL gibt 
	bool HasDrdOfDrrWithoutDel(DRRDATA *popDrr);
	// pr�ft, ob es konkurrierende Abweichungen zu einer Abweichung gibt
	bool IsValidDrd(DRDDATA *popDrd, CMapPtrToPtr *popDrdMap, DRRDATA *popDrr = NULL);
	// pr�ft, ob der Zeitraum <opDateFrom> bis <opDateTo> sich nicht mit der G�ltigkeit einer der DRDs in <popDrdMap> �berschneidet
	bool IsValidDrd(COleDateTime opDateFrom, COleDateTime opDateTo, long lpUrno,
					CMapPtrToPtr *popDrdMap, DRRDATA *popDrr = NULL);
	bool CompareDrdToDelList(DRDDATA *popDrd, CCSPtrArray<DELDATA>& olDelList);
	// zwei DRDs miteinander vergleichen: Ergebnis false -> DRDs sind gleich
	bool CompareDrdToDrd(DRDDATA *popDrd1, DRDDATA *popDrd2,bool bpCompareKey = false);
	// pr�fen, ob die Info im DRD von den Mitarbeiterstammdaten abweichen (false -> keine Abweichung)
	bool CompareDrdToBasicData(DRDDATA *popDrd, bool bpCheckMainFuncOnly);
	// pr�ft, ob die Codes in zwei Qualifikationslisten �bereinstimmen
	bool ComparePrmcList(CString opList1, CString opList2);
	// Info kopieren (keine Schl�sselfelder)
	void CopyDrd(DRDDATA* popDrdDataSource,DRDDATA* popDrdDataTarget);

// Manipulation von Datens�tzen
	// DRDDATA erzeugen, initialisieren und in die Datenhaltung mit aufnehmen
	DRDDATA* CreateDrdData(long lpStfUrno, CString opDay, CString opDrrn);
	// die DRR-Nummer (Drrn) des Datensatzes anpassen
	int DecreaseDrrn(DRRDATA *popDrr);
	// vertuscht die DRR-Nummern
	bool SwapDrrn(DRRDATA *popDrr1, DRRDATA *popDrr2);

// Daten
 
protected:	
// Funktionen
// Broadcasts empfangen und bearbeiten
	// beim Broadcast-Handler anmelden
	void Register(void);

// allgemeine Funktionen
	// Tabellenname einstellen
	bool SetTableNameAndExt(CString opTableAndExtName);
	// Feldliste schreiben
	void SetFieldList(CString opFieldList) {strcpy(pcmListOfFields,opFieldList.GetBuffer(0));}
	// alle internen Arrays zur Datenhaltung leeren und beim BC-Handler abmelden
	bool ClearAll(bool bpUnregister = true);

// Datens�tze speichern/bearbeiten
	// speichert einen einzelnen Datensatz
	bool Save(DRDDATA *prpDrd);
	// einen Broadcast DRD_NEW abschicken und den Datensatz in die interne Datenhaltung aufnehmen
	bool InsertInternal(DRDDATA *prpDrd, bool bpSendDdx);
	// einen Broadcast DRD_CHANGE versenden
	bool UpdateInternal(DRDDATA *prpDrd, bool bpSendDdx = true);
	// einen Datensatz aus der internen Datenhaltung l�schen und einen Broadcast senden
	void DeleteInternal(DRDDATA *prpDrd, bool bpSendDdx = true);

// Kommunikation mit CEDA
	// Kommandos an CEDA senden (die Funktionen der Basisklasse werden �berschrieben
	// wegen des Feldlisten Bugs)
	bool CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
					char *pcpSelection, char *pcpSort, char *pcpData, 
					char *pcpDest = "BUF1",bool bpIsWriteAction = false, 
					long lpID = 0, CCSPtrArray<RecordSet> *pomData = NULL, 
					int ipFieldCount = 0);
    bool CedaAction(char *pcpAction, char *pcpSelection = NULL, char *pcpSort = NULL,
					char *pcpData = pcgDataBuf, char *pcpDest = "BUF1", 
					bool bpIsWriteAction = false, long lpID = 0);

// Daten
	// die geladenen Datens�tze
	CCSPtrArray<DRDDATA> omData;
    // Map mit den Datensatz-Urnos der geladenen Datens�tze
	CMapPtrToPtr omUrnoMap;
	// beim Broadcast-Handler angemeldet?
	bool bmIsBCRegistered;

	// ToDo: Minimale und maximales G�ltigkeitsdatum einbauen, damit nicht
	// unn�tig viele Datens�tze intern gehalten werden. Die min.-max.-Daten
	// m�ssen bei jeder Leseaktion (ReadXXX) gepr�ft und wenn n�tig erweitert
	// werden. Wenn dann BCs einlaufen (REL,NEW), kann gepr�ft werden ob
	// die Datens�tze in den BCs �berhaupt relevant sind.

	// min. und max. Tag der Ansichts-relevanten DRDs
	COleDateTime omMinDay;
	COleDateTime omMaxDay;
};

#endif	// _CEDADRDDATA_H_
