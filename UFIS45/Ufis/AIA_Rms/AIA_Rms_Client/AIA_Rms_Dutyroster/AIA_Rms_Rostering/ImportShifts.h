#if !defined(AFX_IMPORTSHIFT_H__DC92BB23_C452_11D5_ACE1_0050DA1CABCB__INCLUDED_)
#define AFX_IMPORTSHIFT_H__DC92BB23_C452_11D5_ACE1_0050DA1CABCB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ImportShifts.h : header file
//
#include <stdafx.h>
#include <resource.h>

/////////////////////////////////////////////////////////////////////////////
// CImportShifts dialog

class CImportShifts : public CDialog
{
// Construction
public:
	CImportShifts(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CImportShifts)
	enum { IDD = IDD_IMPORTSHIFTS };
	CCSEdit	m_From;
	CCSEdit	m_To;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CImportShifts)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CImportShifts)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IMPORTSHIFT_H__DC92BB23_C452_11D5_ACE1_0050DA1CABCB__INCLUDED_)
