#if !defined(AFX_PRINTDAILYROSTER1_H__0F378153_409F_11D6_81F4_00010215BFDE__INCLUDED_)
#define AFX_PRINTDAILYROSTER1_H__0F378153_409F_11D6_81F4_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PrintDailyRoster1.h : header file
//
#include <CedaDrrData.h>
#include <CedaSgrData.h>
#include <CedaBsdData.h>
#include <fstream>

/////////////////////////////////////////////////////////////////////////////
// CPrintDailyRoster command target

class CPrintDailyRoster : public CCmdTarget
{
	DECLARE_DYNCREATE(CPrintDailyRoster)

	CPrintDailyRoster();									// protected constructor used by dynamic creation

// Attributes
public:
	virtual	BOOL GeneratePrintFile(const CString& ropFileName);
	DutyRoster_View*	pomDutyRoster_View;

// Operations
public:
	CPrintDailyRoster(const COleDateTime& ropDate,const CString& ropViewName,const CStringArray& ropStfUrnos,const CString& ropLevel);
	virtual ~CPrintDailyRoster();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPrintDailyRoster)
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual void	DeleteAll();
	virtual BOOL	BuildGroups();
	virtual	void	GenerateCommon(std::ofstream& of);
	virtual	void	GenerateSpecific(std::ofstream& of);	
	virtual void	GenerateHeader(std::ofstream& of,const CString& ropOrgUnit);
	virtual void	GenerateCrLf(std::ofstream& of);
	virtual	CString GetShiftFunction(const DRRDATA *popDrr);
	virtual void	GenerateEof(std::ofstream& of);
	virtual DutyRoster_View *GetViewInfo();
		
	// Generated message map functions
	//{{AFX_MSG(CPrintDailyRoster)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
protected:
	struct	Absence		// absences 
	{
		long			Stfu;
		COleDateTime	Abfr;
		COleDateTime	Abto;
		CString			Sdac;
		CString			Sdan;
	};

	virtual	int		BuildAbsences(DRRDATA *popDrr,CCSPtrArray<Absence>& ropAbsences);	
	static	int		CompareAbsenceBySdac(const Absence **e1, const Absence **e2);

private:
	struct	Group		// shift groups
	{
		CString					omName;
		CMapStringToPtr			omBasicShiftMap;
		CCSPtrArray<DRRDATA>	omDailyRosterRecords;
		CCSPtrArray<Absence>	omDailyRosterAbsences;
		~Group()
		{
			omDailyRosterAbsences.DeleteAll();			
		}
	};

	struct	OrgUnit		// organisational units
	{
		CCSPtrArray<Group>		omGroups;
		~OrgUnit()
		{
			omGroups.DeleteAll();
		}
	};

	BOOL BuildOrgUnit(OrgUnit *popOrgUnit,const CString& ropOrgUnit);
	BOOL BuildGroup(Group *popGroup,SGRDATA& ropSgr,const CString& ropOrgUnit);

	void GenerateOrgUnit(std::ofstream& of,const CString& ropOrgUnit,OrgUnit *popOrgUnit);
	void GenerateGroup(std::ofstream& of,const CString& ropOrgUnit,Group *polGroup);
	

protected:
	COleDateTime		omDate;
	CStringArray		omStfUrnos;

	CString				omType;
	CString				omTitle;
	CString				omCaption;
	CString				omView;
	int					imLine;
	CString				omLine;
	CString				omLevel;
private:
	CMapStringToPtr		omOrgUnits;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PRINTDAILYROSTER1_H__0F378153_409F_11D6_81F4_00010215BFDE__INCLUDED_)
