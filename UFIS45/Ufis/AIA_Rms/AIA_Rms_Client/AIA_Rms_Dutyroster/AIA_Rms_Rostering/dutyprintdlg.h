#if !defined(AFX_DUTYPRINTDLG_H__ED1E3C25_40D1_11D6_804E_0001022205E4__INCLUDED_)
#define AFX_DUTYPRINTDLG_H__ED1E3C25_40D1_11D6_804E_0001022205E4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// dutyprintdlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DutyPrintDlg dialog

class DutyPrintDlg : public CDialog
{
// Construction
public:
	DutyPrintDlg(CWnd* pParent = NULL, COleDateTime pDate = COleDateTime());   // standard constructor
	static CString GetUniqueFileName(CString opPrefix);
	static BOOL SendFileName(CString opApp, CString opFileName);
	static BOOL CheckStartApp(CString opApp);

// Dialog Data
	//{{AFX_DATA(DutyPrintDlg)
	enum { IDD = IDD_DUTY_PRINT };
	CCSEdit	m_DutyprintDay;
	CButton m_Print1;
	CButton m_Print2;
	CButton m_Print3;
	CButton m_Print4;
	CButton m_Print5;
	CButton m_PrintL;
	CButton m_PrintDailyTypA;
	CButton m_PrintDailyTypB;
	CButton	m_PrintDutyOverview;
	CButton	m_CompensationReport;
	CStatic	m_SL;
	CStatic	m_S5;
	CStatic	m_S4;
	CStatic	m_S3;
	CStatic	m_S2;
	CStatic	m_S1;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DutyPrintDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DutyPrintDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnRDailyA();
	afx_msg void OnRDailyB();
	afx_msg void OnRPrintDutyoverview();
	afx_msg void OnRCompensationReport();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	void PrintDutyOverview();
	void InitLevels();
	DutyRoster_View* pomParent;
	COleDateTime omDate;
	CString GetEmployeeText(long lpStfUrno, CString opSday);
	CString GetShiftText(DRRDATA *popDrrData);
	CCSPtrArray<STFDATA> *pomStfData;
	bool bmShow1,bmShow2,bmShow3,bmShow4,bmShow5,bmShowL;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DUTYPRINTDLG_H__ED1E3C25_40D1_11D6_804E_0001022205E4__INCLUDED_)
