// PrintDailyRoster1.cpp : implementation file
//

#include <stdafx.h>
#include <rostering.h>
#include <PrintDailyRoster.h>
#include <CedaSgrData.h>
#include <CedaSgmData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


static	const char	*DayOfWeek[]	=	{
	"Sunday",
	"Monday",
	"Tuesday",
	"Wednesday",
	"Thursday",
	"Friday",
	"Saturday",
};

/////////////////////////////////////////////////////////////////////////////
// CPrintDailyRoster

IMPLEMENT_DYNCREATE(CPrintDailyRoster, CCmdTarget)

CPrintDailyRoster* pomCPrintDailyRoster;


//****************************************************************************
// CompareDrrByTimeFunc: vergleicht zwei Drr's nach Zeit, bei 
//	Gleichheit der Zeit nach Code.
// R�ckgabe:	<0	->	e1 < e2
//				==0	->	e1 == e2
//				>0	->	e1 > e2
//****************************************************************************

static int CompareDrrByStfuAndTime( const DRRDATA **e1, const DRRDATA **e2)
{
	if ((**e1).Avfr > (**e2).Avfr)
		return 1;
	else if ((**e1).Avfr < (**e2).Avfr)
		return -1;
	else
	{
		if ((**e1).Avto > (**e2).Avto)
			return 1;
		else if ((**e1).Avto < (**e2).Avto)
			return -1;
		else
			return strcmp(ogStfData.GetName((**e1).Stfu, pomCPrintDailyRoster->pomDutyRoster_View->rmViewInfo.iEName),
						  ogStfData.GetName((**e2).Stfu, pomCPrintDailyRoster->pomDutyRoster_View->rmViewInfo.iEName));
	}
}

int CPrintDailyRoster::CompareAbsenceBySdac(const Absence **e1, const Absence **e2)
{
	int ilCmp = strcmp((**e1).Sdac,(**e2).Sdac);
	if (ilCmp < 0)
		return -1;
	else if (ilCmp > 0)
		return 1;
	else if ((**e1).Abfr < (**e2).Abfr)
		return -1;
	else if ((**e1).Abfr > (**e2).Abfr)
		return 1;
	else
		return 0;
}

CPrintDailyRoster::CPrintDailyRoster()
{
	omType	  = "DAILYROSTERPLAN_A";
	omTitle   = LoadStg (IDS_STRING63463);  //"Tagesdienstplan Disponenten"
	omCaption = omTitle;
	omView	  = "Default";
	imLine	  = 1;
	omLevel	  = "3";
	pomCPrintDailyRoster = this;
	pomDutyRoster_View = this->GetViewInfo();
}


CPrintDailyRoster::CPrintDailyRoster(const COleDateTime& ropDate,const CString& ropViewName,const CStringArray& ropStfUrnos,const CString& ropLevel)
{
	omDate	  = ropDate;
	omStfUrnos.Append(ropStfUrnos);
	omType	  = "DAILYROSTERPLAN_A";
	omTitle   = LoadStg (IDS_STRING63463);  //"Tagesdienstplan Disponenten"
	omCaption = omTitle;
	omView	  =	ropViewName;
	imLine	  = 1;	
	omLevel	  = ropLevel;
	pomCPrintDailyRoster = this;
	pomDutyRoster_View = this->GetViewInfo();
}

CPrintDailyRoster::~CPrintDailyRoster()
{
	DeleteAll();
}


BEGIN_MESSAGE_MAP(CPrintDailyRoster, CCmdTarget)
	//{{AFX_MSG_MAP(CPrintDailyRoster)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CPrintDailyRoster::DeleteAll()
{
	CString olName;
	OrgUnit*polOrgUnit;

	POSITION pos = omOrgUnits.GetStartPosition();
	while(pos != NULL)
	{
		omOrgUnits.GetNextAssoc(pos,olName,(void *&)polOrgUnit);
		delete polOrgUnit;
	}
}

BOOL CPrintDailyRoster::BuildGroups()
{
	CString olOrgUnit;
	void*	polValue;
	for (int i = 0; i < omStfUrnos.GetSize(); i++)
	{
		olOrgUnit = ogSorData.GetOrgBySurnWithTime(atol(omStfUrnos[i]),omDate);	
		if (!omOrgUnits.Lookup(olOrgUnit,polValue))
		{
			OrgUnit *polOrgUnit  = new OrgUnit;
			if (BuildOrgUnit(polOrgUnit,olOrgUnit))
			{
				omOrgUnits.SetAt(olOrgUnit,polOrgUnit);
			}
			else
			{
				delete polOrgUnit;
			}
		}
	}

	return TRUE;
}

BOOL CPrintDailyRoster::BuildOrgUnit(OrgUnit *popOrgUnit,const CString& ropOrgUnit)
{
	BOOL blRet = FALSE;
	CCSPtrArray<SGRDATA> olSgrList;
	ogSgrData.GetSgrsByTabn(olSgrList,"BSD","ROSTER");
	for (int i = 0; i < olSgrList.GetSize(); i++)
	{
		Group *polGroup  = new Group;
		polGroup->omName = olSgrList[i].Grpn;
		if (BuildGroup(polGroup,olSgrList[i],ropOrgUnit))
		{
			blRet = TRUE;
			if (polGroup->omDailyRosterRecords.GetSize() || polGroup->omDailyRosterAbsences.GetSize())
			{
				popOrgUnit->omGroups.Add(polGroup);
			}
			else
			{
				delete polGroup;
			}
		}
		else
		{
			delete polGroup;
		}
	}

	olSgrList.DeleteAll();
	return blRet;
}

BOOL CPrintDailyRoster::BuildGroup(Group *popGroup,SGRDATA& ropSgr,const CString& ropOrgUnit)
{
	CCSPtrArray<SGMDATA> olSgmList;
	ogSgmData.GetSgmDataByGrnUrno(olSgmList,ropSgr.Urno);
	for (int i = 0; i < olSgmList.GetSize(); i++)
	{
		BSDDATA *polBSD = ogBsdData.GetBsdByUrno(atoi(olSgmList[i].Uval));
		if (polBSD)
		{
			popGroup->omBasicShiftMap.SetAt(polBSD->Bsdc,polBSD);
		}
	}
	
	// prepare the allowed functions
	DutyRoster_View *polView = GetViewInfo();
	VIEWINFO *polViewInfo = &polView->rmViewInfo;
	CMapStringToPtr	omFunctionMap;
	CString olTmp = polViewInfo->oPfcStatUrnos;
	CStringArray olArr;
	CedaData::ExtractTextLineFast(olArr, olTmp.GetBuffer(0), ",");
	for (i = 0; i < olArr.GetSize(); i++)
	{
		PFCDATA *polPfc = ogPfcData.GetPfcByUrno (atol(olArr[i]));
		if (polPfc != NULL)
		{
			omFunctionMap.SetAt	(polPfc->Fctc, polPfc);
		}
	}

	// loop over all employees visible in this view
	for (i = 0; i < omStfUrnos.GetSize(); i++)
	{
		if (ogSorData.GetOrgBySurnWithTime(atol(omStfUrnos[i]),omDate) == ropOrgUnit)
		{
			CCSPtrArray<DRRDATA> olDrrList;
			ogDrrData.GetDrrListByKeyWithoutDrrn(omDate.Format("%Y%m%d"),atoi(omStfUrnos[i]),omLevel,&olDrrList);				
			for (int j = 0; j < olDrrList.GetSize(); j++)
			{
				void *ptr;
				if (popGroup->omBasicShiftMap.Lookup(olDrrList[j].Scod,ptr))
				{
					if (omFunctionMap.Lookup(olDrrList[j].Fctc,ptr))
					{
						popGroup->omDailyRosterRecords.Add(&olDrrList[j]);
					}

					// check for absences
					BuildAbsences(&olDrrList[j],popGroup->omDailyRosterAbsences);
					
				}
			}
		}
	}

	olSgmList.DeleteAll();

	return TRUE;
}


void CPrintDailyRoster::GenerateCommon(std::ofstream& of)
{
	of << "<=NAME=>"	<<	(const char *)omType					<< "<=\\=>" << '\n';
	of << "<=TITLE=>"	<<	(const char *)omCaption					<< "<=\\=>" << '\n';
	of << "<=USER=>"	<<	(const char *)pcgUser					<< "<=\\=>" << '\n';
	of << "<=AT=>"		<<	(const char *)omDate.Format("%d.%m.%Y") << "<=\\=>" << '\n';
	of << "<=VIEW=>"	<<	(const char *)omView					<< "<=\\=>" << '\n';
}

void CPrintDailyRoster::GenerateHeader(std::ofstream& of,const CString& ropOrgUnit)
{
	of	<<	"<=ORGA=>"
		<<	(const char *)ropOrgUnit
		<<	"<=\\=>" 
		//<<	'\n'
		;
}

void CPrintDailyRoster::GenerateSpecific(std::ofstream& of)
{
	CString olName;
	OrgUnit *polOrgUnit;

	for (POSITION pos = omOrgUnits.GetStartPosition(); pos;)
	{
		omOrgUnits.GetNextAssoc(pos,olName,(void *&)polOrgUnit);
		GenerateOrgUnit(of,olName,polOrgUnit);
	}
}

void CPrintDailyRoster::GenerateOrgUnit(std::ofstream& of,const CString& ropOrgUnit,OrgUnit *popOrgUnit)
{
	GenerateHeader(of,ropOrgUnit);

	for (int ilC = 0; ilC < popOrgUnit->omGroups.GetSize(); ilC++)
	{
		Group *polGroup = &popOrgUnit->omGroups[ilC];		
		GenerateGroup(of,ropOrgUnit,polGroup);
	}
}

void CPrintDailyRoster::GenerateGroup(std::ofstream& of,const CString& ropOrgUnit,Group *polGroup)
{
	polGroup->omDailyRosterRecords.Sort(CompareDrrByStfuAndTime);

	omLine.Format("<=SEPA=>");
	of	<<	(const char *)omLine 
		<<	(const char *)polGroup->omName 
		<<	"<=\\=>" 
		<<	'\n'
		;

	for (int i = 0; i < polGroup->omDailyRosterRecords.GetSize(); i++)
	{ 
		DRRDATA *polDrr = &polGroup->omDailyRosterRecords[i];
		STFDATA *polStf = ogStfData.GetStfByUrno(polDrr->Stfu);
		COleDateTimeSpan olDuration = polDrr->Avto - polDrr->Avfr;
		// Schichtdauer berechnen
		if(polDrr->Sblu != 0 && strlen(polDrr->Sblu))
		{
			int ilSblu = atoi(polDrr->Sblu);
			if (ilSblu > 0)
				olDuration -= COleDateTimeSpan(0,0,ilSblu,0);	// Pause aus DRR einbezihen
		}
		omLine.Format("");
		of	<<	(const char *)omLine
			<<	(const char *)polDrr->Avfr.Format("%H.%M") 
			<<	'|' 
			<<	'-' 
			<<	'|' 
			<<	(const char *)polDrr->Avto.Format("%H.%M") 
			<<	'|'
			<<	polDrr->Scod 
			<<	'|' 
			<<	(const char *)GetShiftFunction(polDrr) 
			<<	'|'
			<<	(const char *)ogStfData.GetName(polStf->Urno, pomCPrintDailyRoster->pomDutyRoster_View->rmViewInfo.iEName)
			<<	'|'
			<<	(const char *)olDuration.Format("%H:%M")
			<< '\n'
			;
	}

	//GenerateCrLf(of);

	polGroup->omDailyRosterAbsences.Sort(CompareAbsenceBySdac);

	CString olSdac;
	for (i = 0; i < polGroup->omDailyRosterAbsences.GetSize(); i++)
	{
		Absence *polAbs = &polGroup->omDailyRosterAbsences[i];
		if (polAbs->Sdac != olSdac)
		{
			omLine.Format("",imLine++);
			of	<<	(const char *)omLine
				<<	(const char *)polAbs->Sdan
				<<	'\n'
				;

		}

		STFDATA *polStf = ogStfData.GetStfByUrno(polAbs->Stfu);
		COleDateTimeSpan olDuration = polAbs->Abto - polAbs->Abfr;

		omLine.Format("");
		of	<<	(const char *)omLine
			<<	(const char *)polAbs->Abfr.Format("%H.%M") 
			<<	'|' 
			<<	'-' 
			<<	'|' 
			<<	(const char *)polAbs->Abto.Format("%H.%M") 
			<<	'|'
			<<	(const char *)polAbs->Sdac
			<<	'|' 
			<<	'|'
			<<	(const char *)ogStfData.GetName(polStf->Urno, pomCPrintDailyRoster->pomDutyRoster_View->rmViewInfo.iEName)
			<<	'|'
			<<	(const char *)olDuration.Format("%H:%M")
			<<	'\n'
			;
	}

	//GenerateCrLf(of);
}

void CPrintDailyRoster::GenerateCrLf(std::ofstream& of)
{
	omLine.Format("");

	of	<<	(const char *)omLine
		<<	'\n'
		;
}

void CPrintDailyRoster::GenerateEof(std::ofstream& of)
{
	of	<<	"<=\\=>"
		<<	'\n'
		;
}

BOOL CPrintDailyRoster::GeneratePrintFile(const CString& ropFileName)
{
	if (omDate.GetStatus() != COleDateTime::valid)
		return FALSE;

	DeleteAll();

	if (!BuildGroups())
		return FALSE;

	std::ofstream of(ropFileName);

	GenerateCommon(of);

	GenerateSpecific(of);

	of.close();

	return TRUE;
}

CString CPrintDailyRoster::GetShiftFunction(const DRRDATA *popDrr)
{
	CString olFct = popDrr->Fctc;
	if (!olFct.GetLength())
	{
		olFct = ogSpfData.GetMainPfcBySurnWithTime(popDrr->Stfu,omDate);
	}

	return olFct;
}

int CPrintDailyRoster::BuildAbsences(DRRDATA *popDrr,CCSPtrArray<Absence>& ropAbsences)
{
	// check for daily roster absences
	CMapPtrToPtr olDraMap;
	if (ogDraData.GetDraMapOfDrr(&olDraMap,popDrr) > 0)
	{
		int ilUrno;
		DRADATA *polDra;
		for (POSITION pos = olDraMap.GetStartPosition(); pos != NULL;)
		{
			olDraMap.GetNextAssoc(pos,(void *&)ilUrno,(void *&)polDra);
			ODADATA *polOda = ogOdaData.GetOdaBySdac(polDra->Sdac);
			if (polOda && strcmp(polOda->Type,"T") == 0)
			{
				Absence *polAbs = new Absence;
				polAbs->Abfr = polDra->Abfr;
				polAbs->Abto = polDra->Abto;
				polAbs->Sdac = polDra->Sdac;
				polAbs->Sdan = polOda->Sdan;
				polAbs->Stfu = polDra->Stfu;
				ropAbsences.Add(polAbs);																		
			}
		}
	}

	// check for absences of the basic shift (DELTAB)
	CCSPtrArray<DELDATA> olDelList;
	if (ogDelData.GetDelListByBsdu(popDrr->Bsdu,true,&olDelList) > 0)
	{
		for (int j = 0; j < olDelList.GetSize(); j++)
		{
			ODADATA *polOda = ogOdaData.GetOdaBySdac(olDelList[j].Sdac);
			if (polOda && strcmp(polOda->Type,"T") == 0)
			{
				Absence *polAbs = new Absence;
				CString olTime(olDelList[j].Delf);
				int ilHour   = atoi(olTime.Left(2));
				int ilMinute = atoi(olTime.Mid(2)); 
				polAbs->Abfr = COleDateTime(omDate.GetYear(),omDate.GetMonth(),omDate.GetDay(),ilHour,ilMinute,0);

				olTime = olDelList[j].Delt;
				ilHour   = atoi(olTime.Left(2));
				ilMinute = atoi(olTime.Mid(2)); 
				polAbs->Abto = COleDateTime(omDate.GetYear(),omDate.GetMonth(),omDate.GetDay(),ilHour,ilMinute,0);
				polAbs->Sdac = polOda->Sdac;
				polAbs->Sdan = polOda->Sdan;
				polAbs->Stfu = popDrr->Stfu;
				ropAbsences.Add(polAbs);																		
			}
		}
	}

	return ropAbsences.GetSize();
}

DutyRoster_View *CPrintDailyRoster::GetViewInfo()
{
	DutyRoster_View *pView = NULL;

	CWinApp *pApp = AfxGetApp();
	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL && pView == NULL)
	{
		CDocTemplate *pDocTempl = pApp->GetNextDocTemplate(pos);
		POSITION p1 = pDocTempl->GetFirstDocPosition();
		while(p1 != NULL && pView == NULL)
		{
			CDocument *pDoc = pDocTempl->GetNextDoc(p1);
			POSITION p2 = pDoc->GetFirstViewPosition();
			while (p2 != NULL && pView == NULL)
			{
				pView = DYNAMIC_DOWNCAST(DutyRoster_View,pDoc->GetNextView(p2));
								
			}
		}
	}

	return pView;
}

/////////////////////////////////////////////////////////////////////////////
// CPrintDailyRoster message handlers
