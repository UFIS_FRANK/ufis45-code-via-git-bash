rem Copies all generated files from the SCM-Structue into the Install-Shield directory.

rem set drive to J: (IS 6.2) or K: (IS 8.0) 
set drive=k:

rem Copy ini- and config-files
copy c:\ufis_bin\ceda.ini %drive%\aia_rms_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\grp.ini %drive%\aia_rms_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\bdps_sec.cfg %drive%\aia_rms_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\*.bmp %drive%\aia_rms_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\RosteringPrint.ulf %drive%\aia_rms_build\ufis_client_appl\system\*.*

rem copy applicationss
copy c:\ufis_bin\debug\bdps_sec.exe %drive%\aia_rms_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpspass.exe %drive%\aia_rms_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpsuif.exe %drive%\aia_rms_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\autocoverage.exe %drive%\aia_rms_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\fips.exe %drive%\aia_rms_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\grp.exe %drive%\aia_rms_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\initializeaccount.exe %drive%\aia_rms_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\opsspm.exe %drive%\aia_rms_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\poolalloc.exe %drive%\aia_rms_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\profileeditor.exe %drive%\aia_rms_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\regelwerk.exe %drive%\aia_rms_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\servicecatalog.exe %drive%\aia_rms_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\rostering.exe %drive%\aia_rms_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\rosteringprint.exe %drive%\aia_rms_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\absenceplanning.exe %drive%\aia_rms_build\ufis_client_appl\applications\*.*


REM Copy system-files
copy c:\ufis_bin\release\bcproxy.exe %drive%\aia_rms_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcproxy.tlb %drive%\aia_rms_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcserv32.exe %drive%\aia_rms_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufis32.dll %drive%\aia_rms_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.exe %drive%\aia_rms_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.tlb %drive%\aia_rms_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisapplmgr.exe %drive%\aia_rms_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisapplmgr.tlb %drive%\aia_rms_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bccomserver.exe %drive%\aia_rms_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bccomserver.tlb %drive%\aia_rms_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.ocx %drive%\aia_rms_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.tlb %drive%\aia_rms_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.ocx %drive%\aia_rms_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.tlb %drive%\aia_rms_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucom.ocx %drive%\aia_rms_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucom.tlb %drive%\aia_rms_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.ocx %drive%\aia_rms_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.tlb %drive%\aia_rms_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\aatlogin.ocx %drive%\aia_rms_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\aatlogin.tlb %drive%\aia_rms_build\ufis_client_appl\system\*.*


rem Copy Help-Files
rem copy c:\ufis_bin\Help\*.chm %drive%\aia_rms_build\ufis_client_appl\help\*.*
