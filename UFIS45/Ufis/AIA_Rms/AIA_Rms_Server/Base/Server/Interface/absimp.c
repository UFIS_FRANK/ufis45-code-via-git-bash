#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/AIA_Rms/AIA_Rms_Server/Base/Server/Interface/absimp.c 1.1 2006/02/24 16:28:31SGT mcu Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :  MCU                                                      */
/* Date           :  16.02.2006                                               */
/* Description    :  ABSIMP absences import                                   */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/

#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <dirent.h>
#include <fcntl.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "syslib.h"
#include "AATArray.h"
#include "db_if.h"
#include <time.h>
#include <cedatime.h>


/*#define TEST*/


#define BSDFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgBsdArray.plrArrayFieldOfs[ipFieldNo]])
#define ODAFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgOdaArray.plrArrayFieldOfs[ipFieldNo]])
#define FIELDVAL(rrArray,pcpBuf,ipFieldNo) (&pcpBuf[rrArray.plrArrayFieldOfs[ipFieldNo]])
#define PFIELDVAL(prArray,pcpBuf,ipFieldNo) (&pcpBuf[prArray->plrArrayFieldOfs[ipFieldNo]])

#define CLIENTTODB 0
#define DBTOCLIENT 1
#define NOCONVERT 2

#define CMD_ABS     (0)
#define CMD_URT     (1)
#define CMD_IRT     (2)
#define CMD_DRT	  	(3)

#define OVERLAP(start1,end1,start2,end2) strcmp(start1,end2) <= 0 && strcmp(start2,end1) <= 0
#define ISBETWEEN(start1,end1,time) strcmp(start1,time) <= 0 && strcmp(end1,time) >= 0

static char *prgCmdTxt[] = {"ABS","URT","IRT","DRT",""};
static int    rgCmdDef[] = {CMD_ABS,CMD_URT,CMD_IRT,CMD_DRT,0};

static char *cgWeekDays[8] = {
	"SO","MO","DI","MI","DO","FR","SA","SO" };
static char *cgWeekDaysEng[8] = {
	"SU","MO","TU","WE","TH","FR","SA","SU" };
static int igTestOutput = 9;

/******************************************************************************/
/***   Fields                                                             *****/
/******************************************************************************/


#define DRR_FIELDS "AVFR,AVTO,BKDP,BSDU,BUFU,CDAT,DRRN,DRS1,DRS2,DRS3,DRS4,"\
		   "DRSF,EXPF,FCTC,HOPO,LSTU,REMA,ROSL,ROSS,SBFR,SBLP,SBLU,"\
		   "SBTO,SCOD,SCOO,SDAY,STFU,URNO,USEC,USEU"

#define EMP_LEAVES_FIELDS "EMP_ID,FUNC,LEAVE_ID,FROM_DATE,TO_DATE"

#define DRRPLANNED_FIELDS "URNO,AVFR,AVTO,SCOD,STFU"
static char cgDrrPlanFields[256] = DRRPLANNED_FIELDS;

#define BSD_FIELDS "URNO,BSDC,ESBG,LSEN,BKD1"


#define DRRSTF_FIELDS "AVFR,AVTO,DRRN,SBFR,SBLU,SBTO,SCOD,SDAY,STFU,PENO,DRRTAB.URNO,DRS1,DRS3,DRS4,DRS2,ROSL"

static char cgDrrFields[512] = "";
static char cgStfFields[256] = "";

static char cgBsdFields[256] = BSD_FIELDS;
static char cgDrrStfFields[256] = DRRSTF_FIELDS;

static char cgDrrReloadHintRosl[256];
static char cgDrrReloadHintStfu[256];

#define ADD_DRR_FIELDS "USED,DUMY"

#define STF_FIELDS "PENO,FINM,LANM,SHNM,GEBU,KIND,NATI,DOBK,DOEM,DODM,REMA,TOHR,RELS"


#define DRR_INSERT_FIELDS "BSDU,SCOD,SCOO,SDAY,AVFR,AVTO,STFU,DRRN,ROSS,ROSL,SBFR,SBTO,SBLU,DRS2,FCTC"
#define DRR_INSERT_HOST ":VBSDU,:VSCOD,:VSCOO,:VSDAY,:VAVFR,:VAVTO,:VSTFU,:VDRRN,:VROSS,:VROSL,:VSBFR,:VSBTO,:VSBLU,:VDRS2,:VFCTC"
#define DRR_UPDATE_FIELDS "BSDU=:VBSDU,SCOD=:VSCOD,AVFR=:VAVFR,AVTO=:VAVTO,SBLU=:VSBLU,SBFR=:VSBFR,SBTO=:VSBTO,FCTC=:VFCTC,DRS2=:VDRS2,SCOO=:VSCOO,DRS1=:VDRS1,DRS3=:VDRS3,DRS4=:VDRS4"
#define DRR_SEND_FIELDS "BSDU,SCOD,AVFR,AVTO,SBLU,SBFR,SBTO,FCTC,DRS2,SCOO,DRS1,DRS3,DRS4"
#define DRR_DELETE_FIELDS "BSDU=:VBSDU,SCOD=:VSCOD,SCOO=:VSCOO,SBFR=:VSBFR,SBTO=:VSBTO,SBLU=:VSBLU,AVFR=:VAVFR,AVTO=:VAVTO"
#define DRR_DEL_FIELDS "BSDU,SCOD,SCOO,SBFR,SBTO,SBLU,AVFR,AVTO"

#define COT_FIELDS "URNO,CTRC"
#define ORG_FIELDS "URNO,DPT1"
#define ODA_FIELDS "URNO,SDAC,FREE,SDAS,TYPE,ABFR,ABTO"

static char cgOdaFields[256] = ODA_FIELDS;

static char cgAbsCodeAppl[24] = "HRABS";
static char cgFuncAppl[24] = "HRABS";
static char cgAbsCode[24] = "ODA";
static char cgFunc[24] = "FUNC";

static char cgEmpLeavesView[124] = "emp_leaves_rms";
static long glOffset = 0;
static long glDuration = 0;

/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */

extern FILE *outp;
int  debug_level = TRACE;

extern errno;

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static int   igLineNo     = 0;                   /* length of incoming item */
static EVENT *prgOutEvent  = NULL;
static int   igInitOK      = FALSE;
static long  lgEvtCnt      = 0;
static char  cgConfigFile[512];
static char  cgHopo[8];                         /* default home airport    */
static char  cgBrkUjty[24] = "";                         /* default home airport    */
static char  cgTabEnd[8];                       /* default table extension */
static	int  igStartUpMode = TRACE;
static	int  igRuntimeMode = 0;
static char  cgProcessName[80];                  /* name of executable */
static char  cgErrorString[1024];
static char cgUpdEmpSequence[12] = "Y";
static char cgUpdAbsSequence[12] = "Y";
static int igExportOffset = -1;
static int igSubChangesOffset = -1;
static char cgAlertName[124] = "SAP HR";
static char cgTohr[4] = "Y";
static char cgSetTohr[4] = "Y";

static char cgCmdBuf[240];
static char cgSelection[50*1024];
static char cgFldBuf[8196];
static char cgSqlBuf[5*1024];
static char cgTwStart[128];
static char cgTwEnd[128];
static char cgRecvName[128];
static char cgDestName[128];
static int igQueOut;

static int igBchdl = 1900;
static int igAction = 7400;
static int igLoghdl = 7700;

static char *pcgStfuList = NULL;
static long lgStfuListLength = 50000;
static long lgStfuListActualLength = 0;

static char cgDrrBuf[2048];
static char cgErrMsg[1024];
static char cgCfgBuffer[256];
static char cgDataArea[5*1024];

static BOOL bgBcActiv = TRUE;
static BOOL bgBreak = FALSE;
static BOOL bgSendBroadcast = TRUE;

static char cgNewRegi[6] = "N";
static char *pcgRegiDeleted = NULL;
static int  igRegiDeletedLength = 0;
static int  igRegiDeletedUsed = 0;

static char cgSapSubDef[6] = "2";
static char cgAbsSubDef[6] = "1";
static char cgShiftSubDef[6] = "1";

static char cgRegi[36];
static char cgLastLstu[16];

static long lgSuccessCount;
static long lgErrorCount;

static FILE *pfgInput = NULL;
static FILE *pfgError = NULL;
static FILE *pfgLog = NULL;
static FILE *pfgTmpFile = NULL;
static char cgTmpFilePath[512] = "";

struct _AbsenceStruct
{
	char Peno[32];
	char Code[18];
	char Func[18];
	char FullDayIndicator[12];
	char StartDate[24];
	char EndDate[24];
	char StartTime[24];
	char EndTime[24];
	char UpdateIndicator[4];
	char Start[32];
	char End[32];
} rgAbsenceData,rgAttendanceData;

typedef struct _AbsenceStruct ABSENCE;


/* static array informations */

#define ARR_NAME_LEN   (18)
#define ARR_FLDLST_LEN (256)

struct _arrayinfo {
	HANDLE   rrArrayHandle;
	char     crArrayTableName[ARR_NAME_LEN+1];
	char     crArrayName[ARR_NAME_LEN+1];
	char     crArrayFieldList[ARR_FLDLST_LEN+1];
	long     lrArrayFieldCnt;
	char   *pcrArrayRowBuf;
	long     lrArrayRowLen;
	long   *plrArrayFieldOfs;
	long   *plrArrayFieldLen;
	HANDLE   rrIdx01Handle;
	char     crIdx01Name[ARR_NAME_LEN+1];
	char     crIdx01FieldList[ARR_FLDLST_LEN+1];
	long     lrIdx01FieldCnt;
	char   *pcrIdx01RowBuf;
	long     lrIdx01RowLen;
	long   *plrIdx01FieldPos;
	long   *plrIdx01FieldOrd;

/*****************/
	HANDLE   rrIdx02Handle;
	char     crIdx02Name[ARR_NAME_LEN+1];
	char     crIdx02FieldList[ARR_FLDLST_LEN+1];
	long     lrIdx02FieldCnt;
	char   *pcrIdx02RowBuf;
	long     lrIdx02RowLen;
	long   *plrIdx02FieldPos;
	long   *plrIdx02FieldOrd;
	/****************************/
	HANDLE   rrIdx03Handle;
	char     crIdx03Name[ARR_NAME_LEN+1];
	char     crIdx03FieldList[ARR_FLDLST_LEN+1];
	long     lrIdx03FieldCnt;
	char   *pcrIdx03RowBuf;
	long     lrIdx03RowLen;
	long   *plrIdx03FieldPos;
	long   *plrIdx03FieldOrd;
};
typedef struct _arrayinfo ARRAYINFO;

static ARRAYINFO rgOdaArray;


static int igDrrAVFR;
static int igDrrAVTO;
static int igDrrBKDP;
static int igDrrBSDU;
static int igDrrBUFU;
static int igDrrCDAT;
static int igDrrDRRN;
static int igDrrDRS1;
static int igDrrDRS2;
static int igDrrDRS3;
static int igDrrDRS4;
static int igDrrDRSF;
static int igDrrEXPF;
static int igDrrFCTC;
static int igDrrHOPO;
static int igDrrLSTU;
static int igDrrREMA;
static int igDrrROSL;
static int igDrrROSS;
static int igDrrSBFR;
static int igDrrSBLU;
static int igDrrSBLP;
static int igDrrSBTO;
static int igDrrSCOD;
static int igDrrSCOO;
static int igDrrSDAY;
static int igDrrSTFU;
static int igDrrURNO;
static int igDrrUSEC;
static int igDrrUSEU;
static int igDrrUSED;

static int igCotURNO;
static int igCotCTRC;

static int igOrgURNO;
static int igOrgDPT1;

static int igStfURNO;
static int igStfPENO;
static int igStfTOHR;

static int igOdaURNO;
static int igOdaABFR;
static int igOdaABTO;
static int igOdaSDAC;
static int igOdaFREE;
static int igOdaTYPE;
static int igOdaSDAS;


static int igDrrPlanURNO;
static int igDrrPlanAVFR;
static int igDrrPlanAVTO;
static int igDrrPlanSCOD;
static int igDrrPlanSTFU;


static int igJobACFR;
static int igJobACTO;
static int igJobUDSR;

static int igSreSURN;
static int igSreVPFR;
static int igSreVPTO;


static int igBsdBSDC;
static int igBsdESBG;
static int igBsdLSEN;
static int igBsdURNO;
static int igBsdBKD1;

/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
int check_ret1(int ipRc,int ipLine);
static int  InitAbsimp();
static long GetLengtOfMonth(int ipMonth,int ipYear);

static int SetIndexInfo(ARRAYINFO *prpArrayInfo,char *pcpFieldList,int ilIndex);
static int SetArrayInfo(char *pcpArrayName, char *pcpTableName, 
		char *pcpArrayFieldList,char *pcpAddFields,long *pcpAddFieldLens,
			ARRAYINFO *prpArrayInfo,char *pcpSelection,BOOL bpFill);
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,
	char *pcpCfgBuffer);

static int GetItemNbr(char *Item, char *Data, char Delim, long Nbr);
static int ItemCnt(char *pcgData, char cpDelim);
static void StringPut(char *pcpDest,char *pcpSource,long lpLen);
static long GetSequence(char *pcpKey,char *pcpMonth);
static int GetFileNames(short spFunc,char *pcpFileId,char *pcpFileDir,char *pcpFileName,BOOL bpForExport);

static BOOL RelevantChange(char *pcpData,char *pcpOldData,char *pcpFieldList,char *pcpRelevantFields);
static BOOL RelevantFields(char *pcpFieldList,char *pcpRelevantFields);
static BOOL RelevantChangeJob(char *pcpData,char *pcpOldData,char *pcpFieldList);

static int SendAnswer(int ipDest, char *pcpAnswer);

extern int get_no_of_items(char *s);
extern int itemCount(char *);

static int	Reset(void);                       /* Reset program          */
static void	Terminate(int ipSleep);            /* Terminate program      */
/*static void	HandleSignal(int); */                /* Handles signals        */
static void	HandleErr(int);                    /* Handles general errors */
static void	HandleQueErr(int);                 /* Handles queuing errors */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/

static void TrimRight(char *pcpBuffer);
static int  GetFieldLength(char *pcpHopo, char *pcpTana, char *pcpFina,
							long *pipLen);
static int  GetRowLength(char *pcpHopo, char *pcpTana, char *pcpFieldList,
							long *pipLen);

static int  GetCommand(char *pcpCommand, int *pipCmd);

static int HandleData(EVENT *prpEvent);       /* Handles event data     */
static int SaveIndexInfo (ARRAYINFO *prpArrayInfo);
static int MySaveIndexInfo (ARRAYINFO *prpArrayInfo);

static void IncrementCount(int ipRc);
static void MoveToErrorFile(char *pcpLineBuf);
static int StrToTime(char *pcpTime,time_t *pclTime);
static void  StrAddTime(char *,char*,char *);
static int MyNewToolsSendSql(int ipRouteID,
		BC_HEAD *prpBchd,
		CMDBLK *prpCmdblk,
		char *pcpSelection,
		char *pcpFields,
		char *pcpData);
static int TriggerAction(char *pcpTableName,char *pcpFields);
static int  WaitAndCheckQueue(int ipTimeout, int ipModId, ITEM **prpItem);
static int  CheckQueue(int ipModId, ITEM **prpItem);
static int FindItemInArrayList(char *pcpInput, char *pcpPattern,char cpDel,int *ipLine,int *ipCol,int *ipPos);

static int GetDebugLevel(char *, int *);
static int GetBreakTimeDiff (char *pcpLocal1, char *pcpLocal2, time_t *plpDiff );
static int DumpArray(ARRAYINFO *prpArray,char *pcpFields);
static int DoSelect(short spFunction,short *pspCursor,char *pcpTable,char *pcpSelection,char *pcpFields,char *pcpDataArea);
static int DoSingleSelect(char *pcpTable,char *pcpSelection,char *pcpFields,char *pcpDataArea);

static int WriteTrailer(FILE *pflFile);
static int WriteHeader(FILE *pflFile,char *pcpFileId,char *pcpFileName,long lpLineCount);
static int MakeFileName(char *pcpFileId,long plSequence,char *pcpFileName);
static int ExportDailyRoster(FILE *pfpFile,char *pcpSday, char *pcpStaffUrno);
static int AddToUrnoList(char *pcpUrno,char *pcpList,long lpListLength,long *plpActualLength);

static void StringTrimRight(char *pcpBuffer);
static int CheckDateFormat(char *pcpDate);
static void PutSequence(char *pcpKey,long lpSequence,char *pcpMonth);

static void StringMid(char *pcpDest,char *pcpSource,long lpFrom,long lpLen,int ipConvert);
static int IsAbsenceCode(char *pcpCode, BOOL *bpOffRd);
static int CheckSre(char *pcpSurn,char *pcpSday);
static int CheckRegi(char *pcpSurn,char *pcpSday);
static BOOL CheckTohr(char *pcpSurn);
static BOOL DeleteStfRow(char *pcpSurn);
static int GetOvertime(char *pcpBsdc,char *pcpSday,char *pcpFrom,char *pcpTo,char *pcpDrs1,char *pcpDrs3,char *pcpDrs4);
static int GetSbluFromBSDTAB(char *pcpBsdc,char *pcpSblu);
static int DeleteDra(ABSENCE *prpAbsence,char *pcpStfUrno,char *pcpDrrn);
static int DeleteDrr(ABSENCE *prpAbsence,char *pcpStfUrno);
static int	DoUpdate(char *pcpTable,char *pcpSelection,char *pcpUpdateFields,
	char *pcpSendFields,char *pcpUrno,char *pcpDataArea,BOOL bpBroadcast);
static int DoInsert(char *pcpTable,char *pcpFields,
					char *pcpInsertFields,char *pcpDataArea,BOOL bpBroadcast);
static int DoInsert2(char *pcpTable,char *pcpFields,
					char *pcpInsertFields,char *pcpDataArea,BOOL bpBroadcast);
static int	DoUpdate2(char *pcpTable,char *pcpSelection,char *pcpUpdateFields,
	char *pcpSendFields,char *pcpUrno,char *pcpDataArea,BOOL bpBroadcast);
static int WriteSubsequentChanges(FILE *pfpFile,char *pcpDrrUrno);
static int ExportSubsequentChanges(char *pcpDrrUrno);
static int OpenTmpFile(BOOL bpRead);
static int GetItem(char* pcpTarget,char* pcpData,char* pcpFieldlist,char* pcpField,int piDeleteBlanks);

static int TimeToStr(char *pcpTime,time_t lpTime)
{
  struct tm *_tm;
 
  _tm = (struct tm *)localtime(&lpTime);
  sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
      _tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
      _tm->tm_min,_tm->tm_sec);
  return (_tm->tm_wday == 0 ? 7 : _tm->tm_wday) + '0';
}                                                                                

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
	int	ilRc = RC_SUCCESS;			/* Return code			*/
	int	ilCnt = 0;
	int ilOldDebugLevel;

	/************************
	FILE *prlErrFile = NULL;
	char clErrFile[512];
	************************/

	INITIALIZE;			/* General initialization	*/

	/****************************************************************************
	sprintf(&clErrFile[0],"%s/%s%5.5d.err",getenv("DBG_PATH"),mod_name,getpid());
	prlErrFile = freopen(&clErrFile[0],"w",stderr);
	fprintf(stderr,"HoHoHo\n");fflush(stderr);
	dbg(TRACE,"MAIN: error file <%s>",&clErrFile[0]);
	****************************************************************************/


	strcpy(cgProcessName,argv[0]);

	dbg(TRACE,"MAIN: version <%s>",mks_version);

	/* Attach to the MIKE queues */
	do
	{
		ilRc = init_que();
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		}/* end of if */
	}while((ilCnt < 10) && (ilRc != RC_SUCCESS));

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
		sleep(60);
		exit(1);
	}else{
		dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
	}/* end of if */

	do
	{
		ilRc = init_db();
		if (ilRc != RC_SUCCESS)
		{
			check_ret(ilRc);
			dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		} /* end of if */
	} while((ilCnt < 10) && (ilRc != RC_SUCCESS));

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
		sleep(60);
		exit(2);
	}else{
		dbg(TRACE,"MAIN: init_db() OK!");
	} /* end of if */

	/* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */

	sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
	dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
	ilRc = TransferFile(cgConfigFile);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */

	sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
	dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
	ilRc = TransferFile(cgConfigFile);
	if(ilRc != RC_SUCCESS)
	{ 
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */

	ilRc = SendRemoteShutdown(mod_id);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
	} /* end of if */

	if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
	{
		dbg(DEBUG,"MAIN: waiting for status switch ...");
		HandleQueues();
	}/* end of if */

	if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
	{
		dbg(TRACE,"MAIN: initializing ...");
		if(igInitOK == FALSE)
		{
			ilRc = InitAbsimp();
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitAbsimp: init failed!");
			} /* end of if */
		}/* end of if */
	} else {
		Terminate(30);
	}/* end of if */

/***
	dbg(TRACE,"MAIN: initializing OK sleep(60) ...");
	sleep(60);
***/

	dbg(TRACE,"=====================");
	dbg(TRACE,"MAIN: initializing OK");
	dbg(TRACE,"=====================");
	
	debug_level = igRuntimeMode;

	for(;;)
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;
				
		if( ilRc == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRc != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRc);
			} /* fi */
			
			lgEvtCnt++;

			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				break;	
			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				/* CloseConnection(); */
				HandleQueues();
				break;	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(1);
				break;	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				break;	
			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;
			case	SHUTDOWN	:
				/* process shutdown - maybe from uutil */
				Terminate(1);
				break;
					
			case	RESET		:
				ilRc = Reset();
				break;
					
			case	EVENT_DATA	:
				if((ctrl_sta == HSB_STANDALONE) || 
					(ctrl_sta == HSB_ACTIVE) || 
					(ctrl_sta == HSB_ACT_TO_SBY))
				{
					ilRc = HandleData(prgEvent);
					if(ilRc != RC_SUCCESS)
					{
						HandleErr(ilRc);
					}/* end of if */
				}else{
					dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
				}/* end of if */
				break;
				
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;
			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;
			default			:
				dbg(TRACE,"MAIN: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
		
	} /* end for */
	
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/

static int FindItemInArrayList(char *pcpInput, char *pcpPattern,char cpDel,int *ipLine,int *ipCol,int *ipPos)
{
	int ilRc = RC_SUCCESS;

	ilRc = FindItemInList(pcpInput,pcpPattern,cpDel,ipLine,ipCol,ipPos);
	*ipLine = (*ipLine) -1;
	
	return ilRc;
}



static int InitFieldIndex()
{
	int ilRc = RC_SUCCESS;
	int ilCol,ilPos;
	int ilDBFields;

	/*FindItemInArrayList(DEM_FIELDS,"ALID",',',&igDemALID,&ilCol,&ilPos);*/


	FindItemInArrayList(cgDrrFields,"AVFR",',',&igDrrAVFR,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"AVTO",',',&igDrrAVTO,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"BKDP",',',&igDrrBKDP,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"BSDU",',',&igDrrBSDU,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"BUFU",',',&igDrrBUFU,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"CDAT",',',&igDrrCDAT,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"DRRN",',',&igDrrDRRN,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"DRS1",',',&igDrrDRS1,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"DRS2",',',&igDrrDRS2,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"DRS3",',',&igDrrDRS3,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"DRS4",',',&igDrrDRS4,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"DRSF",',',&igDrrDRSF,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"EXPF",',',&igDrrEXPF,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"FCTC",',',&igDrrFCTC,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"HOPO",',',&igDrrHOPO,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"LSTU",',',&igDrrLSTU,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"REMA",',',&igDrrREMA,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"ROSL",',',&igDrrROSL,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"ROSS",',',&igDrrROSS,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"SBFR",',',&igDrrSBFR,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"SBLP",',',&igDrrSBLP,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"SBLU",',',&igDrrSBLU,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"SBTO",',',&igDrrSBTO,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"SCOD",',',&igDrrSCOD,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"SCOO",',',&igDrrSCOO,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"SDAY",',',&igDrrSDAY,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"STFU",',',&igDrrSTFU,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"URNO",',',&igDrrURNO,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"USEC",',',&igDrrUSEC,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"USEU",',',&igDrrUSEU,&ilCol,&ilPos);



	FindItemInArrayList(cgStfFields,"URNO",',',&igStfURNO,&ilCol,&ilPos);
	FindItemInArrayList(cgStfFields,"PENO",',',&igStfPENO,&ilCol,&ilPos);
	FindItemInArrayList(cgStfFields,"TOHR",',',&igStfTOHR,&ilCol,&ilPos);

	FindItemInArrayList(cgOdaFields,"URNO",',',&igOdaURNO,&ilCol,&ilPos);
	FindItemInArrayList(cgOdaFields,"SDAC",',',&igOdaSDAC,&ilCol,&ilPos);
	FindItemInArrayList(cgOdaFields,"FREE",',',&igOdaFREE,&ilCol,&ilPos);
	FindItemInArrayList(cgOdaFields,"SDAS",',',&igOdaSDAS,&ilCol,&ilPos);
	FindItemInArrayList(cgOdaFields,"TYPE",',',&igOdaTYPE,&ilCol,&ilPos);
	FindItemInArrayList(cgOdaFields,"ABFR",',',&igOdaABFR,&ilCol,&ilPos);
	FindItemInArrayList(cgOdaFields,"ABTO",',',&igOdaABTO,&ilCol,&ilPos);

	FindItemInArrayList(cgDrrPlanFields,"URNO",',',&igDrrPlanURNO,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrPlanFields,"AVFR",',',&igDrrPlanAVFR,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrPlanFields,"AVTO",',',&igDrrPlanAVTO,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrPlanFields,"SCOD",',',&igDrrPlanSCOD,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrPlanFields,"STFU",',',&igDrrPlanSTFU,&ilCol,&ilPos);


	FindItemInArrayList(cgBsdFields,"URNO",',',&igBsdURNO,&ilCol,&ilPos);
	FindItemInArrayList(cgBsdFields,"BSDC",',',&igBsdBSDC,&ilCol,&ilPos);
	FindItemInArrayList(cgBsdFields,"ESBG",',',&igBsdESBG,&ilCol,&ilPos);
	FindItemInArrayList(cgBsdFields,"LSEN",',',&igBsdLSEN,&ilCol,&ilPos);
	FindItemInArrayList(cgBsdFields,"BKD1",',',&igBsdBKD1,&ilCol,&ilPos);

	return ilRc;

}


static int ReadConfigLine(char *pcpSection,char *pcpKeyword,
	char *pcpCfgBuffer)
{
	int ilRc = RC_SUCCESS;

	char clSection[124];
	char clKeyword[124];
	char clBuffer[256];
	char *pclEquals;

	strcpy(clSection,pcpSection);
	strcpy(clKeyword,pcpKeyword);

	ilRc = iGetConfigLine(cgConfigFile,clSection,clKeyword,clBuffer);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
	} 
	else
	{
			pclEquals = strchr(clBuffer,'=');
			if (pclEquals != NULL)
			{
				strcpy(pcpCfgBuffer,&pclEquals[2]);
			}
			else
			{
				strcpy(pcpCfgBuffer,clBuffer);
			}
			StringTrimRight(pcpCfgBuffer);
			dbg(DEBUG,"Config Line <%s>,<%s>:<%s> found in %s",
					&clSection[0],&clKeyword[0],pcpCfgBuffer,cgConfigFile);
	}/* end of if */
	return ilRc;
}

static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,
	char *pcpCfgBuffer)
{
	int ilRc = RC_SUCCESS;

	char clSection[124];
	char clKeyword[124];

	strcpy(clSection,pcpSection);
	strcpy(clKeyword,pcpKeyword);

	ilRc = iGetConfigEntry(cgConfigFile,clSection,clKeyword,
			CFG_STRING,pcpCfgBuffer);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
	} 
	else
	{
			dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s",
					&clSection[0],&clKeyword[0],pcpCfgBuffer,cgConfigFile);
	}/* end of if */
	return ilRc;
}


static int InitAbsimp()
{
	int  ilRc = RC_SUCCESS;			/* Return code */
  char clSection[64];
  char clKeyword[64];
  char clNow[64];
	time_t tlNow;
	int ilOldDebugLevel;
	long pclAddFieldLens[12];
	char pclAddFields[256];
	char pclSqlBuf[2560];
	char pclSelection[1024];
 	short slCursor;
	short slSqlFunc;
	char  clBreak[24];
	char  clTmpStr[24];
	int ilLc;
	int ilTmpModId;

	tlNow = time(NULL);
	tlNow -= tlNow % 86400;

	TimeToStr(clNow,tlNow);

	dbg(TRACE,"Now = <%s>",clNow);

	/* now reading from configfile or from database */


	if ((ilTmpModId = tool_get_q_id("bchdl")) == RC_SUCCESS)
	{
		igBchdl = ilTmpModId;
		dbg(TRACE,"InitAbsimp: Mod-Id for <bchdl> set to %d",igBchdl);
	}
	else
	{
		dbg(TRACE,"InitAbsimp: Mod-Id for <bchdl> not found, set to default %d",igBchdl);
	}

	if ((ilTmpModId = tool_get_q_id("action")) == RC_SUCCESS)
	{
		igAction = ilTmpModId;
		dbg(TRACE,"InitAbsimp: Mod-Id for <action> set to %d",igAction);
	}
	else
	{
		dbg(TRACE,"InitAbsimp: Mod-Id for <action> not found, set to default %d",igAction);
	}


	if ((ilTmpModId = tool_get_q_id("loghdl")) == RC_SUCCESS)
	{
		igLoghdl = ilTmpModId;
		dbg(TRACE,"InitAbsimp: Mod-Id for <loghdl> set to %d",igLoghdl);
	}
	else
	{
		dbg(TRACE,"InitAbsimp: Mod-Id for <loghdl> not found, set to default %d",igLoghdl);
	}


	if(ilRc == RC_SUCCESS)
	{
		/* read HomeAirPort from SGS.TAB */
		memset((void*)&cgHopo[0], 0x00, 8);

		sprintf(&clSection[0],"SYS");
		sprintf(&clKeyword[0],"HOMEAP");

		ilRc = tool_search_exco_data(&clSection[0], &clKeyword[0], &cgHopo[0]);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"<InitAbsimp> EXTAB,%s,%s not found in SGS.TAB",&clSection[0], &clKeyword[0]);
			Terminate(30);
		} else {
			dbg(TRACE,"<InitAbsimp> home airport    <%s>",&cgHopo[0]);
		}/* end of if */
	}/* end of if */

	
	if(ilRc == RC_SUCCESS)
	{
		sprintf(&clSection[0],"ALL");
		sprintf(&clKeyword[0],"TABEND");

		ilRc = tool_search_exco_data(&clSection[0], &clKeyword[0], &cgTabEnd[0]);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"<InitAbsimp> EXTAB,%s,%s not found in SGS.TAB",&clSection[0], &clKeyword[0]);
			Terminate(30);
		} else {
			dbg(TRACE,"<InitAbsimp> table extension <%s>",&cgTabEnd[0]);
		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		 GetDebugLevel("STARTUP_MODE", &igStartUpMode);
		 GetDebugLevel("RUNTIME_MODE", &igRuntimeMode);
		 
		 debug_level = igStartUpMode;

		ilRc = ReadConfigEntry("SYSTEM","OFFSET",clTmpStr);
		if(ilRc == RC_SUCCESS)
		{
			glOffset = atol(clTmpStr)*86400;
			dbg(TRACE,"OFFSET found set to <%d>",(int)(glOffset/86400));
		}
		else
		{
			dbg(TRACE,"OFFSET not found set to <%d>",(int)(glOffset/86400));
			ilRc = RC_SUCCESS;
		}

		ilRc = ReadConfigEntry("TEST","DURATION",clTmpStr);
		if(ilRc == RC_SUCCESS)
		{
			glDuration = atol(clTmpStr)*86400;
			dbg(TRACE,"DURATION found set to <%d>",(int)(glDuration/86400));
		}
		else
		{
			dbg(TRACE,"DURATION not found set to <%d>",(int)(glDuration/86400));
			ilRc = RC_SUCCESS;
		}

		ilRc = ReadConfigEntry("SYSTEM","EMPLEAVESVIEW",cgEmpLeavesView);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"View to read from set to <%s>",cgEmpLeavesView);
		}
		else
		{
			dbg(TRACE,"View to read from not found, set to <%s>",cgEmpLeavesView);
			ilRc = RC_SUCCESS;
		}


		ilRc = ReadConfigEntry("FUNCTIONLOOKUP","INTERFACE",cgFuncAppl);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"INTERFACE for Function set to <%s>",cgFuncAppl);
		}
		else
		{
			dbg(TRACE,"INTERFACE for Function not found, set to <%s>",cgFuncAppl);
			ilRc = RC_SUCCESS;
		}


		ilRc = ReadConfigEntry("FUNCTIONLOOKUP","LKEY",cgFunc);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"LKEY for Function set to <%s>",cgFunc);
		}
		else
		{
			dbg(TRACE,"LKEY for Function not found, set to <%s>",cgFunc);
			ilRc = RC_SUCCESS;
		}

		ilRc = ReadConfigEntry("ABSCODELOOKUP","INTERFACE",cgAbsCodeAppl);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"INTERFACE for Absence Code set to <%s>",cgAbsCodeAppl);
		}
		else
		{
			dbg(TRACE,"INTERFACE for Absence Code not found, set to <%s>",cgAbsCodeAppl);
			ilRc = RC_SUCCESS;
		}

		ilRc = ReadConfigEntry("ABSCODELOOKUP","LKEY",cgAbsCode);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"LKEY for AbsenceCode set to <%s>",cgAbsCode);
		}
		else
		{
			dbg(TRACE,"LKEY for AbsenceCode not found, set to <%s>",cgAbsCode);
			ilRc = RC_SUCCESS;
		}

	if(ilRc == RC_SUCCESS)
	{
		ilRc = CEDAArrayInitialize(20,20);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitAbsimp: CEDAArrayInitialize failed <%d>",ilRc);
		}/* end of if */
	}/* end of if */



	if(ilRc == RC_SUCCESS)
	{

		sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);
		dbg(TRACE,"InitAbsimp: SetArrayInfo ODATAB array \n<%s>",pclSelection);
		ilRc = SetArrayInfo("ODATAB","ODATAB",ODA_FIELDS,NULL,NULL,&rgOdaArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitAbsimp: SetArrayInfo ODATAB failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"InitAbsimp: SetArrayInfo ODATAB OK");
			dbg(DEBUG,"<%s> %d",rgOdaArray.crArrayName,rgOdaArray.rrArrayHandle);
			strcpy(rgOdaArray.crIdx01Name,"ODA_URNO");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgOdaArray.rrArrayHandle,
				rgOdaArray.crArrayName,
				&rgOdaArray.rrIdx01Handle,
				rgOdaArray.crIdx01Name,"URNO");
				strcpy(rgOdaArray.crIdx01FieldList,"URNO");
			SetIndexInfo(&rgOdaArray,"URNO",1);

			strcpy(rgOdaArray.crIdx02Name,"ODA_SDAC");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgOdaArray.rrArrayHandle,
				rgOdaArray.crArrayName,
				&rgOdaArray.rrIdx02Handle,
				rgOdaArray.crIdx02Name,"SDAC");
				strcpy(rgOdaArray.crIdx02FieldList,"SDAC");
			SetIndexInfo(&rgOdaArray,"SDAC",2);
		}
	}


	if(ilRc == RC_SUCCESS)
	{
		ilRc = CEDAArrayReadAllHeader(outp);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitAbsimp: CEDAArrayReadAllHeader failed <%d>",ilRc);
		}else{
			dbg(TRACE,"InitAbsimp: CEDAArrayReadAllHeader OK");
		}/* end of if */
	}/* end of if */

	
	pcgStfuList = malloc(lgStfuListLength);
	if(pcgStfuList == NULL)
	{
		dbg(TRACE,"unable to allocate %ld Bytes for StfuList",lgStfuListLength);
		ilRc = RC_FAIL;
	}
	else
	{
		memset(pcgStfuList,0,lgStfuListLength);
	}

	if(ilRc == RC_SUCCESS)
	{
		ilRc = InitFieldIndex();
	}


	TriggerAction("STFTAB","URNO!,PENO!,TOHR!");
	TriggerAction("GDLTAB","URNO!,LKEY!,LVAL!,RVAL!,TYPE!");
	/*TriggerAction("DRRTAB","URNO!,SDAY!,DRS2!");*/
	/*TriggerAction("DRRTAB","URNO!,STFU,AVFR,AVTO,DRS2,SCOD!,SBLU,SDAY!");*/

	if(ilRc == RC_SUCCESS)
	{
		igInitOK = TRUE;
		dbg(TRACE,"InitAbsimp: InitFieldIndex OK");
	}
	else
	{
		dbg(TRACE,"InitAbsimp: InitFieldIndex failed");
	}
}

	dbg(TRACE,"Debug Level is %d",debug_level);
	return (ilRc);	
} /* end of initialize */



static int SetIndexInfo(ARRAYINFO *prpArrayInfo,char *pcpFieldList,int ilIndex)
{
	int	ilRc = RC_SUCCESS;				/* Return code */
  int ilLc;
	char **ppclIdxRowBuf;
	long *pllIdxRowLen;

	dbg(DEBUG,"%05d:SetIndexInfo:",__LINE__);
	switch(ilIndex)
	{
		case 1:
			ppclIdxRowBuf = &prpArrayInfo->pcrIdx01RowBuf;
			pllIdxRowLen = &prpArrayInfo->lrIdx01RowLen;
			break;
		case 2:
			ppclIdxRowBuf = &prpArrayInfo->pcrIdx02RowBuf;
			pllIdxRowLen = &prpArrayInfo->lrIdx02RowLen;
			break;
		case 3:
			ppclIdxRowBuf = &prpArrayInfo->pcrIdx03RowBuf;
			pllIdxRowLen = &prpArrayInfo->lrIdx03RowLen;
			break;
		default:
			ilRc = RC_FAIL;
			dbg(DEBUG,"SetIndexInfo:ivalid index %d",ilIndex);
	}

	if (ilRc == RC_SUCCESS)
	{
  ilRc = GetRowLength(cgHopo,prpArrayInfo->crArrayTableName,pcpFieldList,
							pllIdxRowLen);
	*ppclIdxRowBuf = (char *) calloc(1,(*pllIdxRowLen+10));
	if(*ppclIdxRowBuf == NULL)
  {
    ilRc = RC_FAIL;
    dbg(TRACE,"SetArrayInfo: calloc failed");
   }/* end of if */
	}

	return ilRc;
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
static int SetArrayInfo(char *pcpArrayName, char *pcpTableName, char *pcpArrayFieldList,char *pcpAddFields,long *pcpAddFieldLens,ARRAYINFO *prpArrayInfo,
	char *pcpSelection,BOOL bpFill)
{
	int	ilRc = RC_SUCCESS;				/* Return code */
  int ilLc;

	if(prpArrayInfo == NULL)
	{
		dbg(TRACE,"SetArrayInfo: invalid last parameter : null pointer not valid");
		ilRc = RC_FAIL;
	}else
	{
		memset(prpArrayInfo,0x00,sizeof(prpArrayInfo));

		prpArrayInfo->rrArrayHandle = -1;
		prpArrayInfo->rrIdx01Handle = -1;

		if(pcpArrayName != NULL)
		{
			if(strlen(pcpArrayName) <= ARR_NAME_LEN)
			{
				strcpy(prpArrayInfo->crArrayName,pcpArrayName);
			}/* end of if */
		}/* end of if */
	}/* end of if */

	strcpy(prpArrayInfo->crArrayTableName,pcpTableName);
	if(ilRc == RC_SUCCESS)
	{
		prpArrayInfo->lrArrayFieldCnt = get_no_of_items(pcpArrayFieldList);
		if (pcpAddFields != NULL)
		{
			prpArrayInfo->lrArrayFieldCnt += get_no_of_items(pcpAddFields);
		}

		prpArrayInfo->plrArrayFieldOfs = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
		if(prpArrayInfo->plrArrayFieldOfs == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrArrayFieldOfs calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		prpArrayInfo->plrArrayFieldLen = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
		if(prpArrayInfo->plrArrayFieldLen == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrArrayFieldLen calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}else{
			*(prpArrayInfo->plrArrayFieldLen) = -1;
		}/* end of if */
	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
       	ilRc = GetRowLength(&cgHopo[0],pcpTableName,pcpArrayFieldList,
							&(prpArrayInfo->lrArrayRowLen));
       	if(ilRc != RC_SUCCESS)
       	{
           	dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpArrayName,pcpArrayFieldList);
      	}/* end of if */
   	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
		prpArrayInfo->lrArrayRowLen+=100;
		prpArrayInfo->pcrArrayRowBuf = (char *) calloc(1,(prpArrayInfo->lrArrayRowLen+10));
   		if(prpArrayInfo->pcrArrayRowBuf == NULL)
   		{
      			ilRc = RC_FAIL;
      			dbg(TRACE,"SetArrayInfo: calloc failed");
   		}/* end of if */
   	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
		prpArrayInfo->pcrIdx01RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx01RowLen+1));
   		if(prpArrayInfo->pcrIdx01RowBuf == NULL)
   		{
      			ilRc = RC_FAIL;
      			dbg(TRACE,"SetArrayInfo: calloc failed");
   		}/* end of if */
   	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		dbg(DEBUG,"CEDAArrayCreate follows Array <%s> Table <%s>",pcpArrayName,pcpTableName);
		ilRc = CEDAArrayCreate(&(prpArrayInfo->rrArrayHandle),
				pcpArrayName,pcpTableName,pcpSelection,pcpAddFields,
				pcpAddFieldLens,pcpArrayFieldList,
				&(prpArrayInfo->plrArrayFieldLen[0]),
				&(prpArrayInfo->plrArrayFieldOfs[0]));

		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"SetArrayInfo: CEDAArrayInitialize failed <%d>",ilRc);
		}/* end of if */
	}/* end of if */

		if(pcpArrayFieldList != NULL)
		{
			if(strlen(pcpArrayFieldList) <= ARR_FLDLST_LEN)
			{
				strcpy(prpArrayInfo->crArrayFieldList,pcpArrayFieldList);
			}/* end of if */
		}/* end of if */
		if(pcpAddFields != NULL)
		{
			if(strlen(pcpAddFields) + 
				strlen(prpArrayInfo->crArrayFieldList) <= (size_t)ARR_FLDLST_LEN)
			{
				strcat(prpArrayInfo->crArrayFieldList,",");
				strcat(prpArrayInfo->crArrayFieldList,pcpAddFields);
				dbg(DEBUG,"<%s>",prpArrayInfo->crArrayFieldList);
			}/* end of if */
		}/* end of if */


	if(ilRc == RC_SUCCESS && bpFill)
	{
		ilRc = CEDAArrayFill(&(prpArrayInfo->rrArrayHandle),pcpArrayName,NULL);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"SetArrayInfo: CEDAArrayFill failed <%d>",ilRc);
		}/* end of if */
	}/* end of if */

   for(ilLc = 0; ilLc < prpArrayInfo->lrArrayFieldCnt; ilLc++)
   {
			char	pclFieldName[25];

			*pclFieldName  = '\0';

			GetDataItem(pclFieldName,prpArrayInfo->crArrayFieldList,
						ilLc+1,',',"","\0\0");
     dbg(DEBUG,"%02d %-10s Offset: %d",
				ilLc,pclFieldName,prpArrayInfo->plrArrayFieldOfs[ilLc]);
   }   

	return(ilRc);
}



/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
	int	ilRc = RC_SUCCESS;				/* Return code */
	
	dbg(TRACE,"Reset: now resetting");
	
	return ilRc;
	
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
	/* unset SIGCHLD ! DB-Child will terminate ! */

	dbg(TRACE,"Terminate: now DB logoff ...");

	signal(SIGCHLD,SIG_IGN);

	logoff();

	dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


	dbg(TRACE,"Terminate: now leaving ...");

	fclose(outp);

	sleep(ipSleep < 1 ? 1 : ipSleep);
	
	exit(0);
	
} /* end of Terminate */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
	dbg(TRACE,"HandleErr: <%d> : unknown ERROR",pipErr);

	return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
	switch(pipErr) {
	case	QUE_E_FUNC	:	/* Unknown function */
		dbg(TRACE,"<%d> : unknown function",pipErr);
		break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
		dbg(TRACE,"<%d> : malloc failed",pipErr);
		break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
		dbg(TRACE,"<%d> : msgsnd failed",pipErr);
		break;
	case	QUE_E_GET	:	/* Error using msgrcv */
		dbg(TRACE,"<%d> : msgrcv failed",pipErr);
		break;
	case	QUE_E_EXISTS	:
		dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
		break;
	case	QUE_E_NOFIND	:
		dbg(TRACE,"<%d> : route not found ",pipErr);
		break;
	case	QUE_E_ACKUNEX	:
		dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
		break;
	case	QUE_E_STATUS	:
		dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
		break;
	case	QUE_E_INACTIVE	:
		dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
		break;
	case	QUE_E_MISACK	:
		dbg(TRACE,"<%d> : missing ack ",pipErr);
		break;
	case	QUE_E_NOQUEUES	:
		dbg(TRACE,"<%d> : queue does not exist",pipErr);
		break;
	case	QUE_E_RESP	:	/* No response on CREATE */
		dbg(TRACE,"<%d> : no response on create",pipErr);
		break;
	case	QUE_E_FULL	:
		dbg(TRACE,"<%d> : too many route destinations",pipErr);
		break;
	case	QUE_E_NOMSG	:	/* No message on queue */
		dbg(TRACE,"<%d> : no messages on queue",pipErr);
		break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
		dbg(TRACE,"<%d> : invalid originator=0",pipErr);
		break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
		dbg(TRACE,"<%d> : queues are not initialized",pipErr);
		break;
	case	QUE_E_ITOBIG	:
		dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
		break;
	case	QUE_E_BUFSIZ	:
		dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
		break;
	default			:	/* Unknown queue error */
		dbg(TRACE,"<%d> : unknown error",pipErr);
		break;
	} /* end switch */
         
	return;

} /* end of HandleQueErr */


/******************************************************************************/
/******************************************************************************/
static int GetRowLength(char *pcpHopo, char *pcpTana, char *pcpFieldList,
						long *plpLen)
{
	int	 ilRc = RC_SUCCESS;			/* Return code */
	int  ilNoOfItems = 0;
	int  ilLoop = 0;
	long llFldLen = 0;
	long llRowLen = 0;
	char clFina[8];
	
	ilNoOfItems = get_no_of_items(pcpFieldList);
	ilLoop = 1;
	do
	{
		ilRc = get_real_item(&clFina[0],pcpFieldList,ilLoop);
		if(ilRc > 0)
		{
			ilRc = GetFieldLength(pcpHopo,pcpTana,&clFina[0],&llFldLen);
			if(ilRc == RC_SUCCESS)
			{
				llRowLen++;
				llRowLen += llFldLen;
			}/* end of if */
		}/* end of if */
		ilLoop++;
	}while((ilRc == RC_SUCCESS) && (ilLoop <= ilNoOfItems));

	if(ilRc == RC_SUCCESS)
	{
		*plpLen = llRowLen;
	}/* end of if */

	return(ilRc);
	
} /* end of GetRowLength */


/******************************************************************************/
/******************************************************************************/
static int GetFieldLength(char *pcpHopo, char *pcpTana, char *pcpFina,
							long *plpLen)
{
	int  ilRc = RC_SUCCESS;			/* Return code */
	int  ilCnt = 0;
	char clTaFi[32];
	char clFele[16];
	char clFldLst[16];

	ilCnt = 1;
	sprintf(&clTaFi[0],"%s,%s",pcpTana,pcpFina);
	sprintf(&clFldLst[0],"TANA,FINA"); /* wird von syslib zerst�rt */


	ilRc = syslibSearchSystabData(&cgTabEnd[0],&clFldLst[0],&clTaFi[0],"FELE",&clFele[0],&ilCnt,"");
	switch (ilRc)
	{
	case RC_SUCCESS :
		*plpLen = atoi(&clFele[0]);
		/* dbg(DEBUG,"GetFieldLength: <%s,%s,%s> <%d>",pcpHopo,pcpTana,pcpFina,*plpLen);*/
		break;

	case RC_NOT_FOUND :
		dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_NOT_FOUND <%s>",ilRc,&clTaFi[0]);
		break;

	case RC_FAIL :
		dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_FAIL",ilRc);
		break;

	default :
		dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> unexpected",ilRc);
		break;
	}/* end of switch */

	return(ilRc);
	
} /* end of GetFieldLength */


/******************************************************************************/
/******************************************************************************/
static int GetCommand(char *pcpCommand, int *pipCmd)
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	int ilLoop = 0;
	char clCommand[48];
	
	memset(&clCommand[0],0x00,8);

	ilRc = get_real_item(&clCommand[0],pcpCommand,1);
	if(ilRc > 0)
	{
		ilRc = RC_FAIL;
	}/* end of if */

	while((ilRc != 0) && (prgCmdTxt[ilLoop] != NULL))
	{
		ilRc = strcmp(&clCommand[0],prgCmdTxt[ilLoop]);
		ilLoop++;
	}/* end of while */

	if(ilRc == 0)
	{
		ilLoop--;
		dbg(DEBUG,"GetCommand: <%s> <%d>",prgCmdTxt[ilLoop],rgCmdDef[ilLoop]);
		*pipCmd = rgCmdDef[ilLoop];
	}else{
		dbg(TRACE,"GetCommand: <%s> is not valid",&clCommand[0]);
		ilRc = RC_FAIL;
	}/* end of if */

	return(ilRc);
	
} /* end of GetCommand */




int compare(const void * pcpStr1,const void * pcpStr2)
{
	char *pclStr1 = (char *)pcpStr1;
	char *pclStr2 = (char *)pcpStr2;
	return(strcmp(&pclStr1[10],&pclStr2[10]));
}



static void StringTrimRightLineEnd(char *pcpBuffer)
{
	if (*pcpBuffer != '\0')
	{
		char *pclBlank = &pcpBuffer[strlen(pcpBuffer)-1];
		while((*pclBlank == '\r' || *pclBlank == '\n') && pclBlank != pcpBuffer)
		{
			*pclBlank = '\0';
			pclBlank--;
		}
	}
}

static void StringTrimRight(char *pcpBuffer)
{
	if (*pcpBuffer != '\0')
	{
		char *pclBlank = &pcpBuffer[strlen(pcpBuffer)-1];
		while(isspace(*pclBlank) && pclBlank != pcpBuffer)
		{
			*pclBlank = '\0';
			pclBlank--;
		}
	}
}

static void StringPut(char *pcpDest,char *pcpSource,long lpLen)
{

	int i,ilSourceLen;

	ilSourceLen = strlen(pcpSource);

	for(i = 0; i < lpLen; i++)
	{
		if (i < ilSourceLen)
		{
			pcpDest[i] = pcpSource[i];
		}
		else
		{
			pcpDest[i] = ' ';
		}
	}
}


static int DaysOfMonth[12] = {31,28,31,30,31,30,31,31,30,31,30,31};

static int CheckDateOld(char *pcpDate)
{
	int ilRc = RC_SUCCESS;
	int ilLc;
	char clYear[6];
	char clMonth[6];
	char clDay[6];
	char clHour[6];
	char clMinute[6];
	char clSecond[6];
	int ilYear,ilMonth,ilDay,ilHour,ilMinute,ilSecond;

	/*dbg(DEBUG,"CheckDate14: <%s>",pcpDate);*/
	for (ilLc = 0; pcpDate[ilLc] != '\0'; ilLc++)
	{
			if(! (pcpDate[ilLc] >= '0' && pcpDate[ilLc] <= '9'))
			{
				strcpy(pcpDate," ");
				return (RC_FAIL);
			}
	}
	if (ilLc < 12 || ilLc > 14)
	{
		dbg(TRACE,"CheckDate: invalid length %d\n",ilLc);
		strcpy(pcpDate," ");
		return (RC_FAIL);
	}
	else
	{
		dbg(TRACE,"CheckDate: valid length %d\n",ilLc);
	}
		
	for (; ilLc < 14 ; ilLc++)
	{
			pcpDate[ilLc] = '0';
	}
	pcpDate[ilLc] = '\0';
	/*dbg(DEBUG,"CheckDate14: <%s>",pcpDate);*/

	strncpy(clYear,pcpDate,4);
	clYear[4] = '\0';
	strncpy(clMonth,&pcpDate[4],2);
	clMonth[2] = '\0';
	strncpy(clDay,&pcpDate[6],2);
	clDay[2] = '\0';
	strncpy(clHour,&pcpDate[8],2);
	clHour[2] = '\0';
	strncpy(clMinute,&pcpDate[10],2);
	clMinute[2] = '\0';
	strncpy(clSecond,&pcpDate[12],2);
	clSecond[2] = '\0';

	ilYear = atoi(clYear);
	ilMonth = atoi(clMonth);
	ilDay = atoi(clDay);
	ilHour = atoi(clHour);
	ilMinute = atoi(clMinute);
	ilSecond = atoi(clSecond);

	if (ilYear < 1900 || ilYear > 2099)
	{
		dbg(TRACE,"CheckDate: invalid year <%s>\n",clYear);
		strcpy(pcpDate," ");
		return (RC_FAIL);
	}

	if (ilMonth < 1 || ilMonth > 12)
	{
		dbg(TRACE,"CheckDate: invalid month <%s>\n",clMonth);
		strcpy(pcpDate," ");
		return (RC_FAIL);
	}

	if (ilMonth == 2)
	{
		if (!(ilYear % 4))
		{
			if(ilDay > 29)
			{
				dbg(TRACE,"CheckDate: invalid day in feb of leap year <%s>\n",clDay);
				strcpy(pcpDate," ");
				return (RC_FAIL);
			}
		}
		else
		{
			if (ilDay > DaysOfMonth[ilMonth-1])
			{
				dbg(TRACE,"CheckDate: invalid day <%s> for %d\n",clDay,ilMonth);
				strcpy(pcpDate," ");
				return (RC_FAIL);
			}
		}
	}
	else
	{
			if (ilDay > DaysOfMonth[ilMonth-1])
			{
				dbg(TRACE,"CheckDate: invalid day <%s> for %d\n",clDay,ilMonth);
				strcpy(pcpDate," ");
				return (RC_FAIL);
			}
		}
	if (ilHour > 24)
	{
		dbg(TRACE,"CheckDate: invalid hour <%s>\n",clHour);
		strcpy(pcpDate," ");
		return (RC_FAIL);
	}
	if (ilMinute > 60)
	{
		dbg(TRACE,"CheckDate: invalid minute <%s>\n",clMinute);
		strcpy(pcpDate," ");
		return (RC_FAIL);
	}
	if (ilSecond > 60)
	{
		dbg(TRACE,"CheckDate: invalid second <%s>\n",clSecond);
		strcpy(pcpDate," ");
		return (RC_FAIL);
	}
	return ilRc;
}	

static void StringMid(char *pcpDest,char *pcpSource,long lpFrom,long lpLen,int ipConvert)
{
	strncpy(pcpDest,&pcpSource[lpFrom],lpLen);
	pcpDest[lpLen] = '\0';
	switch(ipConvert)
	{
	case CLIENTTODB:
		ConvertClientStringToDb(pcpDest);
		break;
	case DBTOCLIENT:
		ConvertDbStringToClient(pcpDest);
		break;
	default:
			/* do nothing */
			;
	}
}

static char *ItemPut(char *pcpDest,char *pcpSource,char cpDelimiter)
{
	strcpy(pcpDest,pcpSource);
	pcpDest += strlen(pcpDest);
	if (cpDelimiter != '\0')
	{
		*pcpDest = cpDelimiter;
		pcpDest++;
	}
	return (pcpDest);
}


static int DoSelect(short spFunction,short *pspCursor,char *pcpTable,char *pcpSelection,char *pcpFields,char *pcpDataArea)
{
	int ilRc = RC_SUCCESS;
	static char clSqlBuf[4096];

	char *pclDataArea;
	int ilLc,ilItemCount;

	sprintf(clSqlBuf,"SELECT %s FROM %s %s",pcpFields,pcpTable,pcpSelection);

	dbg(DEBUG,"DoSelect: clSqlBuf <%s>",clSqlBuf);
  *pcpDataArea = '\0';
	ilRc = sql_if(spFunction,pspCursor,clSqlBuf,pcpDataArea);
	if(ilRc != RC_SUCCESS && ilRc != NOTFOUND)
	{
		/* there must be a database error */
		get_ora_err(ilRc, cgErrMsg);
		dbg(TRACE,"DoSelect failed RC=%d <%s>",ilRc,cgErrMsg);
		dbg(TRACE,"SQL: <%s>",clSqlBuf);
	}
	if (ilRc != RC_SUCCESS)
	{
		close_my_cursor(pspCursor);
		*pspCursor = 0;
	}

	ilItemCount = get_no_of_items(pcpFields); 
	pclDataArea = pcpDataArea;
	for(ilLc =  1; ilLc < ilItemCount; ilLc++)
	{
		while(*pclDataArea != '\0')
		{
			pclDataArea++;
		}
			*pclDataArea = ',';
	}
  if (ilRc == RC_SUCCESS)
	{
		dbg(DEBUG,"DoSelect: RC=%d  items=%d <%s>",ilRc,ilItemCount,pcpDataArea);
	}
	else
	{
		dbg(DEBUG,"DoSelect: RC=%d",ilRc,pcpDataArea);
	}
	return(ilRc);
}

static int DoSingleSelect(char *pcpTable,char *pcpSelection,char *pcpFields,char *pcpDataArea)
{
	int ilRc = RC_SUCCESS;
	static char clSqlBuf[4096];

	short slCursor = 0;
	short slFunction;
	char *pclDataArea;
	int ilLc,ilItemCount;

	sprintf(clSqlBuf,"SELECT %s FROM %s %s",pcpFields,pcpTable,pcpSelection);

  *pcpDataArea = '\0';
	ilRc = sql_if(START,&slCursor,clSqlBuf,pcpDataArea);
	if(ilRc != RC_SUCCESS && ilRc != NOTFOUND)
	{
		/* there must be a database error */
		get_ora_err(ilRc, cgErrMsg);
		dbg(TRACE,"DoSingleSelect failed RC=%d <%s>",ilRc,cgErrMsg);
		dbg(TRACE,"SQL: <%s>",clSqlBuf);
	}
	close_my_cursor(&slCursor);
	slCursor = 0;

	ilItemCount = get_no_of_items(pcpFields); 
	pclDataArea = pcpDataArea;
	for(ilLc =  1; ilLc < ilItemCount; ilLc++)
	{
		while(*pclDataArea != '\0')
		{
			pclDataArea++;
		}
			*pclDataArea = ',';
		}
  if (ilRc == RC_SUCCESS)
	{
		dbg(DEBUG,"DoSingleSelect: RC=%d <%s>",ilRc,pcpDataArea);
	}
	else
	{
		dbg(DEBUG,"DoSingleSelect: RC=%d",ilRc);
	}
	return(ilRc);
}

static int	DoDelete(char *pcpTable,char *pcpSelection)
{
	int ilRc = RC_SUCCESS;
	static char clSqlBuf[4096];
	char clTimeStamp[24];

	short slCursor = 0;
	short slFunction;

	sprintf(clSqlBuf,"DELETE FROM %s %s",pcpTable,pcpSelection);

	if (debug_level == DEBUG)
	{
		dbg(DEBUG,"DoDelete: <%s>",clSqlBuf);
	}
	ilRc = sql_if(START,&slCursor,clSqlBuf,cgDataArea);
	if(ilRc != RC_SUCCESS)
	{
		if(ilRc != RC_NOTFOUND)
		{ 
			/* there must be a database error */
			get_ora_err(ilRc, cgErrMsg);
			dbg(TRACE,"DoDelete failed RC=%d <%s>",ilRc,cgErrMsg);
			dbg(TRACE,"SQL: <%s>",clSqlBuf);
		}
	}
	else
	{
		commit_work();
		/* send updates to loghdl and bchdl */
    SendCedaEvent(igLoghdl,0,cgDestName,cgAlertName,cgTwStart, cgTwEnd,
          "DRT",pcpTable,pcpSelection," "," ","",5,0);          
    SendCedaEvent(igBchdl,0,cgDestName,cgAlertName,cgTwStart, cgTwEnd,
          "DRT",pcpTable,pcpSelection," "," ","",3,0);          
    SendCedaEvent(igAction,0,cgDestName,cgAlertName,cgTwStart, cgTwEnd,
          "DRT",pcpTable,pcpSelection," "," ","",3,0);          
	}
	commit_work();
	close_my_cursor(&slCursor);
	slCursor = 0;
	return(ilRc);
}

static int	DoUpdate2(char *pcpTable,char *pcpSelection,char *pcpUpdateFields,
	char *pcpSendFields,char *pcpUrno,char *pcpDataArea,BOOL bpBroadcast)
{
	int ilRc = RC_SUCCESS;
	static char clSqlBuf[4096];
	static char clDataArea[40000];
	static char clFieldList[2000];
	static char clSendFieldList[2000];
	char clTimeStamp[24];
	char clSelection[204];

	short slCursor = 0;
	short slFunction;
	char *pclDataArea;
	char *pclData;

	sprintf(clSelection,"WHERE URNO = '%s'",pcpUrno);
	dbg(DEBUG,"DoUpdate: Selection <%s>",clSelection);
	strcpy(clDataArea,pcpDataArea);
	strcpy(clFieldList,pcpUpdateFields);
	sprintf(clSqlBuf,"UPDATE %s SET %s %s",
		pcpTable,clFieldList,pcpSelection);

	if (clDataArea[strlen(clDataArea)-1] != ',')
	{
		strcat(clDataArea,",");
	}

	if (debug_level == DEBUG)
	{
		dbg(DEBUG,"DoUpdate: <%s>",clSqlBuf);
		dbg(DEBUG,"DoUpdate: <%s>",clDataArea);
	}
	pclData = strdup(clDataArea);
	delton(clDataArea);
	ilRc = sql_if(START,&slCursor,clSqlBuf,clDataArea);
	if(ilRc != RC_SUCCESS)
	{
		if(ilRc != RC_NOTFOUND)
		{ 
			/* there must be a database error */
			get_ora_err(ilRc, cgErrMsg);
			dbg(TRACE,"DoUpdate failed RC=%d <%s>",ilRc,cgErrMsg);
			dbg(TRACE,"SQL: <%s>",clSqlBuf);
			rollback();
		}
	}
	else
	{
		commit_work();
		/* don't send updates to loghdl and bchdl */
	}
	free(pclData);
	close_my_cursor(&slCursor);
	slCursor = 0;
	return(ilRc);
}

static int	DoUpdate(char *pcpTable,char *pcpSelection,char *pcpUpdateFields,
	char *pcpSendFields,char *pcpUrno,char *pcpDataArea,BOOL bpBroadcast)
{
	int ilRc = RC_SUCCESS;
	static char clSqlBuf[4096];
	static char clDataArea[40000];
	static char clFieldList[2000];
	static char clSendFieldList[2000];
	char clTimeStamp[24];
	char clSelection[204];

	short slCursor = 0;
	short slFunction;
	char *pclDataArea;
	char *pclData;

	sprintf(clSelection,"WHERE URNO = '%s'",pcpUrno);
	dbg(DEBUG,"DoUpdate: Selection <%s>",clSelection);
	strcpy(clDataArea,pcpDataArea);
	sprintf(clFieldList,"%s,LSTU=:VLSTU,USEU=:VUSEU",pcpUpdateFields);
	sprintf(clSendFieldList,"%s,LSTU,USEU",pcpSendFields);
	sprintf(clSqlBuf,"UPDATE %s SET %s %s",
		pcpTable,clFieldList,pcpSelection);

	if (clDataArea[strlen(clDataArea)-1] != ',')
	{
		strcat(clDataArea,",");
	}
	pclDataArea = clDataArea + strlen(clDataArea);
	GetServerTimeStamp("UTC", 1,0,clTimeStamp);
	pclDataArea = ItemPut(pclDataArea,clTimeStamp,',');
	pclDataArea = ItemPut(pclDataArea,cgAlertName,'\0');

	if (debug_level == DEBUG)
	{
		dbg(DEBUG,"DoUpdate: <%s>",clSqlBuf);
		dbg(DEBUG,"DoUpdate: <%s>",clDataArea);
	}
	pclData = strdup(clDataArea);
	delton(clDataArea);
	ilRc = sql_if(START,&slCursor,clSqlBuf,clDataArea);
	if(ilRc != RC_SUCCESS)
	{
		if(ilRc != RC_NOTFOUND)
		{ 
			/* there must be a database error */
			get_ora_err(ilRc, cgErrMsg);
			dbg(TRACE,"DoUpdate failed RC=%d <%s>",ilRc,cgErrMsg);
			dbg(TRACE,"SQL: <%s>",clSqlBuf);
			rollback();
		}
	}
	else
	{
		commit_work();
		/* send updates to loghdl and bchdl */
		if ((pclData != NULL) && (bpBroadcast))
		{
    	SendCedaEvent(igLoghdl,0,cgDestName,cgAlertName,cgTwStart, cgTwEnd,
          "URT",pcpTable,clSelection,clSendFieldList,pclData,"",5,0);          
    	SendCedaEvent(igBchdl,0,cgDestName,cgAlertName,cgTwStart, cgTwEnd,
          "URT",pcpTable,clSelection,clSendFieldList,pclData,"",3,0);          
    	SendCedaEvent(igAction,0,cgDestName,cgAlertName,cgTwStart, cgTwEnd,
          "URT",pcpTable,clSelection,clSendFieldList,pclData,"",3,0);          
		}
		else
		{
			dbg(TRACE,"DoUpdate: not enough memory to send data to loghdl");
			Terminate(20);
		}
	}
	free(pclData);
	close_my_cursor(&slCursor);
	slCursor = 0;
	return(ilRc);
}

static int DoInsert2(char *pcpTable,char *pcpFields,
					char *pcpInsertFields,char *pcpDataArea,BOOL bpBroadcast)
{
	int ilRc = RC_SUCCESS;
	static char clSqlBuf[4096];
	static char clDataArea[40000];
	char clTimeStamp[24];
	static char clFieldList[2000];
	char clUrno[24];

	short slCursor = 0;
	short slFunction;
	char *pclDataArea;
	char *pclData;

	strcpy(clDataArea,pcpDataArea);
	dbg(DEBUG,"<%s>",clDataArea);
	sprintf(clFieldList,"%s",pcpFields);
	sprintf(clSqlBuf,"INSERT INTO %s (%s) VALUES(%s)",
		pcpTable,pcpFields,pcpInsertFields);

	if (clDataArea[strlen(clDataArea)-1] != ',')
	{
		strcat(clDataArea,",");
	}
	if (debug_level == DEBUG)
	{
		dbg(DEBUG,"DoInsert: <%s>",clSqlBuf);
		dbg(DEBUG,"DoInsert: <%s>",clDataArea);
	}
	pclData = strdup(clDataArea);
	delton(clDataArea);
	ilRc = sql_if(START,&slCursor,clSqlBuf,clDataArea);
	if(ilRc != RC_SUCCESS)
	{
		/* there must be a database error */
		get_ora_err(ilRc, cgErrMsg);
		dbg(TRACE,"DoInsert failed RC=%d <%s>",ilRc,cgErrMsg);
		dbg(TRACE,"SQL: <%s>",clSqlBuf);
		rollback();
	}
	else
	{
		commit_work();
		/* send inserts to loghdl */
		if ((pclData != NULL) && (bpBroadcast))
		{
    	SendCedaEvent(igLoghdl,0,cgDestName,cgAlertName,cgTwStart, cgTwEnd,
          "IRT",pcpTable," ",clFieldList,pclData,"",5,0);          
    	SendCedaEvent(igBchdl,0,cgDestName,cgAlertName,cgTwStart, cgTwEnd,
          "IRT",pcpTable," ",clFieldList,pclData,"",3,0);          
    	SendCedaEvent(igAction,0,cgDestName,cgAlertName,cgTwStart, cgTwEnd,
          "IRT",pcpTable," ",clFieldList,pclData,"",3,0);          
		}
		else
		{
			dbg(TRACE,"DoInsert: not enough memory to send data to loghdl");
			Terminate(20);
		}
	}

	free(pclData);
	strcpy(pcpDataArea,clUrno);
	close_my_cursor(&slCursor);
	slCursor = 0;
	return(ilRc);
}


static int DoInsert(char *pcpTable,char *pcpFields,
					char *pcpInsertFields,char *pcpDataArea,BOOL bpBroadcast)
{
	int ilRc = RC_SUCCESS;
	static char clSqlBuf[4096];
	static char clDataArea[40000];
	char clTimeStamp[24];
	static char clFieldList[2000];
	char clUrno[24];

	short slCursor = 0;
	short slFunction;
	char *pclDataArea;
	char *pclData;

	strcpy(clDataArea,pcpDataArea);
	dbg(DEBUG,"<%s>",clDataArea);
	sprintf(clFieldList,"%s,CDAT,USEC,URNO,HOPO",pcpFields);
	sprintf(clSqlBuf,"INSERT INTO %s (%s,CDAT,USEC,URNO,HOPO) VALUES(%s,"
	":VCDAT,:VUSEC,:VURNO,:VHOPO)",
		pcpTable,pcpFields,pcpInsertFields);

	if (clDataArea[strlen(clDataArea)-1] != ',')
	{
		strcat(clDataArea,",");
	}
	pclDataArea = clDataArea + strlen(clDataArea);
	GetServerTimeStamp("UTC", 1,0,clTimeStamp);
	pclDataArea = ItemPut(pclDataArea,clTimeStamp,',');
	pclDataArea = ItemPut(pclDataArea,cgAlertName,',');
	GetNextValues(clUrno,1);
	pclDataArea = ItemPut(pclDataArea,clUrno,',');
	pclDataArea = ItemPut(pclDataArea,cgHopo,'\0');
	if (debug_level == DEBUG)
	{
		dbg(DEBUG,"DoInsert: <%s>",clSqlBuf);
		dbg(DEBUG,"DoInsert: <%s>",clDataArea);
	}
	pclData = strdup(clDataArea);
	delton(clDataArea);
	ilRc = sql_if(START,&slCursor,clSqlBuf,clDataArea);
	if(ilRc != RC_SUCCESS)
	{
		/* there must be a database error */
		get_ora_err(ilRc, cgErrMsg);
		dbg(TRACE,"DoInsert failed RC=%d <%s>",ilRc,cgErrMsg);
		dbg(TRACE,"SQL: <%s>",clSqlBuf);
		rollback();
	}
	else
	{
		commit_work();
		/* send inserts to loghdl */
		if ((pclData != NULL) && (bpBroadcast))
		{
    	SendCedaEvent(igLoghdl,0,cgDestName,cgAlertName,cgTwStart, cgTwEnd,
          "IRT",pcpTable," ",clFieldList,pclData,"",5,0);          
    	SendCedaEvent(igBchdl,0,cgDestName,cgAlertName,cgTwStart, cgTwEnd,
          "IRT",pcpTable," ",clFieldList,pclData,"",3,0);          
    	SendCedaEvent(igAction,0,cgDestName,cgAlertName,cgTwStart, cgTwEnd,
          "IRT",pcpTable," ",clFieldList,pclData,"",3,0);          
		}
		else
		{
			dbg(TRACE,"DoInsert: not enough memory to send data to loghdl");
			Terminate(20);
		}
	}

	free(pclData);
	strcpy(pcpDataArea,clUrno);
	close_my_cursor(&slCursor);
	slCursor = 0;
	return(ilRc);
}


static int IsAbsenceCode(char *pcpCode,BOOL *bpOffRd)
{
	int ilRc = RC_SUCCESS;
	char clKey[124];
	long llRow = ARR_FIRST;

	*bpOffRd = FALSE;
	strcpy(clKey,pcpCode);

	ilRc = CEDAArrayFindRowPointer(&rgOdaArray.rrArrayHandle,rgOdaArray.crArrayName,
		&rgOdaArray.rrIdx02Handle,rgOdaArray.crIdx02Name,clKey,
		&llRow,(void **)&rgOdaArray.pcrArrayRowBuf);
	if (ilRc >= 0)
	{
		char clOdaFree[3];
		char clOdaType[6];

		strcpy(clOdaFree,PFIELDVAL((&rgOdaArray),rgOdaArray.pcrArrayRowBuf,igOdaFREE));
		strcpy(clOdaType,PFIELDVAL((&rgOdaArray),rgOdaArray.pcrArrayRowBuf,igOdaTYPE));

		if (strstr(clOdaFree,"x") || strstr(clOdaType,"S"))
		{
			*bpOffRd = TRUE;
			dbg(DEBUG,"Absence <%s> but will be exported. FREE <%s> TYPE <%s>",
				pcpCode,clOdaFree,clOdaType);
		}
	}

	dbg(DEBUG,"IsAbsenceCode for <%s> will return %d",pcpCode,ilRc);
	return ilRc;
}

static int CheckAbsenceCode(char *pcpCode,char *pcpAbfr,char *pcpAbto,char *pcpOdaUrno)
{
	int ilRc = RC_SUCCESS;
	char clKey[124];
	long llRow = ARR_FIRST;

	strcpy(clKey,pcpCode);

	ilRc = CEDAArrayFindRowPointer(&rgOdaArray.rrArrayHandle,rgOdaArray.crArrayName,
	&rgOdaArray.rrIdx02Handle,rgOdaArray.crIdx02Name,clKey,
		&llRow,(void **)&rgOdaArray.pcrArrayRowBuf);
	if (ilRc == RC_SUCCESS)
	{
		strcpy(pcpAbfr,PFIELDVAL((&rgOdaArray),rgOdaArray.pcrArrayRowBuf,igOdaABFR));
		strcpy(pcpAbto,PFIELDVAL((&rgOdaArray),rgOdaArray.pcrArrayRowBuf,igOdaABTO));
		strcpy(pcpOdaUrno,PFIELDVAL((&rgOdaArray),rgOdaArray.pcrArrayRowBuf,igOdaURNO));
	  dbg(DEBUG,"CheckAbsenceCode: OdaURNO <%s> %d ABFR <%s> ABTO <%s>",
					pcpOdaUrno,igOdaURNO,pcpAbfr,pcpAbto);
	}
	else
	{
		dbg(TRACE,"Absence Code <%s> in ODATAB not found",pcpCode);
		strcpy(pcpOdaUrno,"0");
	}

	return ilRc;
}


static int AddToUrnoList(char *pcpUrno,char *pcpList,long lpListLength,long *plpActualLength)
{
	int ilRc = RC_SUCCESS;
	
  int ilNewLen;

	ilNewLen = strlen(pcpUrno) + 1;
	if (((*plpActualLength) + ilNewLen) > (lpListLength+50))
	{
		char *pclTmp;
		pclTmp = realloc(pcpList,lpListLength + 10000);
		if (pclTmp != NULL)
		{
			pcpList = pclTmp;
			lpListLength += 10000;
		}
		else
		{
			dbg(TRACE,"unable to reallocate %ld bytes for Urnolist",
					lpListLength+50000);
			Terminate(30);
		}
	}

	if (*pcpList != '\0')
	{
		sprintf(&pcpList[*plpActualLength],",'%s'",pcpUrno);
		*plpActualLength += ilNewLen;
	}
	else
	{
		sprintf(&pcpList[*plpActualLength],"'%s'",pcpUrno);
		*plpActualLength += ilNewLen-1;
	}

	/*dbg(DEBUG,"AddToUrnoList: <%s>",pcpList);*/

	return ilRc;
}

/* DRR_INSERT_FIELDS "BSDU,SCOD,SCOO,SDAY,AVFR,AVTO,STFU,DRRN,ROSS,ROSL,SBFR,SBTO,SBLU"*/
static int InsertDrr(ABSENCE *prpAbsence,char *pcpBsdu,char *pcpStfUrno,char *pcpAbfr,char *pcpAbto)
{
	int ilRc = RC_SUCCESS;
	char *pclDataArea;
	char clTmpDataArea[124];
	char clSelection[128];
	char clDrrUrno[24];
	char clDrrn[4];
	char clRosl[4];
	char clScod[24];
	BOOL blIsUpdated = FALSE;

	dbg(DEBUG,"InsertDrr: CODE <%s> BSDU <%s>, STFURNO <%s>",prpAbsence->Code,pcpBsdu,pcpStfUrno);

	memset(clTmpDataArea,0,sizeof(clTmpDataArea));
	sprintf(clSelection,"WHERE STFU='%s' AND SDAY='%s' AND ROSS='A'",
		pcpStfUrno,prpAbsence->StartDate);
	ilRc = DoSingleSelect("DRRTAB",clSelection,"URNO,DRRN,ROSL,SCOD",clTmpDataArea);

	if(ilRc == RC_SUCCESS)
	{
		/* set old DRR Ross to 'L' */
		GetDataItem(clDrrUrno,clTmpDataArea,1,',',"","\0\0");
		GetDataItem(clDrrn,clTmpDataArea,2,',',"","\0\0");
		GetDataItem(clRosl,clTmpDataArea,3,',',"","\0\0");
		GetDataItem(clScod,clTmpDataArea,4,',',"","\0\0");
		/*if (*clRosl == '2' && *clDrrn == '1')*/
		{
			dbg(DEBUG,"Old active DRR found, overwrite");
			memset(cgDataArea,0,sizeof(cgDataArea));
			pclDataArea = cgDataArea;

			pclDataArea = ItemPut(pclDataArea,pcpBsdu,','); 
			pclDataArea = ItemPut(pclDataArea,prpAbsence->Code,',');
			pclDataArea = ItemPut(pclDataArea,prpAbsence->Start,',');
			pclDataArea = ItemPut(pclDataArea,prpAbsence->End,',');
			pclDataArea = ItemPut(pclDataArea,"0000",',');
			pclDataArea = ItemPut(pclDataArea,"18991230000000",',');
			pclDataArea = ItemPut(pclDataArea,"18991230000000",',');
			pclDataArea = ItemPut(pclDataArea,prpAbsence->Func,',');
			pclDataArea = ItemPut(pclDataArea,cgSapSubDef,','); /* DRS2 set to SAPSUBDEF  */
			pclDataArea = ItemPut(pclDataArea,clScod,',');

dbg(DEBUG,"Update DRR follows <%s> Start <%s> End <%s>",cgDataArea,prpAbsence->Start,prpAbsence->End);

	pclDataArea = ItemPut(pclDataArea," ",','); /* DRS1 set to " "  */
	pclDataArea = ItemPut(pclDataArea," ",','); /* DRS3 set to " "  */
	pclDataArea = ItemPut(pclDataArea," ",','); /* DRS4 set to " "  */
			sprintf(clSelection,"WHERE URNO = '%s'",clDrrUrno);
			ilRc = DoUpdate("DRRTAB",clSelection,DRR_UPDATE_FIELDS,
					DRR_SEND_FIELDS,clDrrUrno,cgDataArea,TRUE);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InsertDrr: DRR record <%s> to update not found",
					clSelection);
			}
			else
			{
				blIsUpdated = TRUE;
			}
		}
	}

	if (!blIsUpdated)
	{
		memset(cgDataArea,0,sizeof(cgDataArea));
		pclDataArea = cgDataArea;

		pclDataArea = ItemPut(pclDataArea,pcpBsdu,','); 
		pclDataArea = ItemPut(pclDataArea,prpAbsence->Code,',');
		/* copy shift code also in SCOO */
		pclDataArea = ItemPut(pclDataArea,prpAbsence->Code,',');
		pclDataArea = ItemPut(pclDataArea,prpAbsence->StartDate,',');
		pclDataArea = ItemPut(pclDataArea,prpAbsence->Start,',');
		pclDataArea = ItemPut(pclDataArea,prpAbsence->End,',');
		pclDataArea = ItemPut(pclDataArea,pcpStfUrno,',');
		pclDataArea = ItemPut(pclDataArea,"1",','); /* DRRN always '1' */
		pclDataArea = ItemPut(pclDataArea,"A",','); /* ROSS always 'A' */
		pclDataArea = ItemPut(pclDataArea,"2",','); /* ROSL always '2' */
		pclDataArea = ItemPut(pclDataArea,"18991230000000",','); /* SBFR always  */
		pclDataArea = ItemPut(pclDataArea,"18991230000000",','); /* SBTO always  */
		pclDataArea = ItemPut(pclDataArea,"0000",','); /* SBLU always  */
		pclDataArea = ItemPut(pclDataArea,cgSapSubDef,','); /* DRS2 set to SAPSUBDEF  */
		pclDataArea = ItemPut(pclDataArea,prpAbsence->Func,',');

		ilRc = DoInsert("DRRTAB",DRR_INSERT_FIELDS,DRR_INSERT_HOST,cgDataArea,TRUE);
	}

	if (ilRc == RC_SUCCESS)
	{
		/*AddToUrnoList(pcpStfUrno,pcgStfuList,lgStfuListLength,&lgStfuListActualLength);*/
	}
	return ilRc;
}

/*DRR_DELETE_FIELDS "BSDU=:VBSDU,SCOD=:VSCOD,SCOO=:VSCOO,SBFR=:VSBFR,SBTO=:VSBTO,SBLU=:VSBLU,AVFR=:VAVFR,AVTO=:VAVTO"*/


/* DRR_UPDATE_FIELDS "BSDU=:VBSDU,SCOD=:VSCOD,AVFR=:VAVFR,AVTO=:VAVTO,SBLU=:VSBLU,SBFR=:VSBFR,SBTO=:VSBTO"*/
static int UpdateDrr(ABSENCE *prpAbsence,char *pcpBsdu,char *pcpStfUrno,char *pcpAbfr,char *pcpAbto)
{
	int ilRc = RC_SUCCESS;
	char *pclDataArea;
	char clDrrUrno[124];
	char clSelection[124];
	char clTmpDataArea[124];
	char clScod[24];

	dbg(DEBUG,"UpdateDrr: CODE <%s> BSDU <%s>, STFURNO <%s>",prpAbsence->Code,pcpBsdu,pcpStfUrno);
	memset(cgDataArea,0,sizeof(cgDataArea));
	pclDataArea = cgDataArea;

	pclDataArea = ItemPut(pclDataArea,pcpBsdu,','); 
	pclDataArea = ItemPut(pclDataArea,prpAbsence->Code,',');
	pclDataArea = ItemPut(pclDataArea,prpAbsence->Start,',');
	pclDataArea = ItemPut(pclDataArea,prpAbsence->End,',');
	pclDataArea = ItemPut(pclDataArea,"0000",',');
	pclDataArea = ItemPut(pclDataArea,"18991230000000",',');
	pclDataArea = ItemPut(pclDataArea,"18991230000000",',');
	pclDataArea = ItemPut(pclDataArea," ",',');
	pclDataArea = ItemPut(pclDataArea,cgSapSubDef,','); /* DRS2 set to SAPSUBDEF  */

	sprintf(clSelection,"WHERE STFU='%s' AND SDAY='%s' AND ROSS='A'",
		pcpStfUrno,prpAbsence->StartDate);
	ilRc = DoSingleSelect("DRRTAB",clSelection,"URNO,SCOD",clTmpDataArea);
	if (ilRc == RC_SUCCESS)
	{
		GetDataItem(clDrrUrno,clTmpDataArea,1,',',"","\0\0");
		GetDataItem(clScod,clTmpDataArea,2,',',"","\0\0");
		pclDataArea = ItemPut(pclDataArea,clScod,',');
	pclDataArea = ItemPut(pclDataArea," ",','); /* DRS1 set to " "  */
	pclDataArea = ItemPut(pclDataArea," ",','); /* DRS3 set to " "  */
	pclDataArea = ItemPut(pclDataArea," ",','); /* DRS4 set to " "  */
		ilRc = DoUpdate("DRRTAB",clSelection,DRR_UPDATE_FIELDS,DRR_SEND_FIELDS,
				clDrrUrno,cgDataArea,TRUE);
		if (ilRc == RC_SUCCESS)
		{
			/*AddToUrnoList(pcpStfUrno,pcgStfuList,lgStfuListLength,&lgStfuListActualLength);*/
		}
	}
	else
	{
		dbg(TRACE,"UpdateDrr: DRR record <%s> to update not found",
			clSelection);
		InsertDrr(prpAbsence,pcpBsdu,pcpStfUrno,pcpAbfr,pcpAbto);
		/*** delete the old invalid DRA-records **/
		sprintf(clSelection,"WHERE STFU='%s' AND SDAY='%s' ",
			pcpStfUrno,prpAbsence->StartDate);
		ilRc = DoDelete("DRATAB",clSelection);
	}

	/*** delete matching DRA record if it exist **/
	sprintf(clSelection,"WHERE STFU='%s' AND SDAY='%s' AND SDAC='%s'",
			pcpStfUrno,prpAbsence->StartDate,prpAbsence->Code);
			ilRc = DoDelete("DRATAB",clSelection);
	return ilRc;
}

static int DoDataLookup(char *pcpAppl,char *pcpLKey,char *pcpDest,char *pcpSrc)
{
	int ilRc = RC_SUCCESS;
	char clTmpDataArea[512];
	char clSelection[512];


	sprintf(clSelection,"WHERE TYPE='%s' AND LKEY='%s' and LVAL='%s'",
			pcpAppl,pcpLKey,pcpSrc);
	ilRc = DoSingleSelect("GDLTAB",clSelection,"RVAL",clTmpDataArea);
	if (ilRc == RC_SUCCESS)
	{
		strcpy(pcpDest,clTmpDataArea);
	  StringTrimRight(pcpDest);
		dbg(DEBUG,"DataLookup: <%s> found for <%s/%s>",pcpDest,pcpLKey,pcpSrc);
	}

	return ilRc;
}



static int DrrImport(ABSENCE *prpAbsence)
{
	int ilRc = RC_SUCCESS;
	char clSelection[1024];
	char clStfUrno[24];
	char clDrrUrno[24];
	char clDrrn[24] = "";
	char clAbfr[24] = "";
	char clAbto[24] = "";
	char clOdaUrno[24] = "";

	sprintf(clSelection,"WHERE TO_NUMBER(PENO)='%d'",atoi(prpAbsence->Peno));
	ilRc = DoSingleSelect("STFTAB",clSelection,"URNO",clStfUrno);
	if(ilRc == RC_SUCCESS)
	{
	if (ilRc == RC_SUCCESS)
	{
		ilRc = DoDataLookup(cgAbsCodeAppl,cgAbsCode,prpAbsence->Code,prpAbsence->Code);
	}
		/* search for absence code in ODATAB */
		ilRc = CheckAbsenceCode(prpAbsence->Code,clAbfr,clAbto,clOdaUrno);
		dbg(DEBUG,"DrrImport: RC from CheckAbsenceCode %d ODA-URNO <%s> ABFR <%s> ABTO <%s>",ilRc,clOdaUrno,clAbfr,clAbto);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"DrrImport: invalid absence code <%s>",prpAbsence->Code);
		}
	}
	else
	{
		dbg(TRACE,"DrrImport: invalid Peno <%s>",prpAbsence->Peno);
	}
	
	if (ilRc == RC_SUCCESS)
	{
		DoDataLookup(cgFuncAppl,cgFunc,prpAbsence->Func,prpAbsence->Func);
		/*** continue work if Func not found **/
	}

	if(ilRc == RC_SUCCESS)
	{
		if (strcmp(prpAbsence->End,"20301231") > 0)
		{
			dbg(TRACE,"ImportDrr:invalid absence end date <%s>",prpAbsence->End);
		}
		else
		{
				dbg(DEBUG,"Start <%s> End <%s>",prpAbsence->Start,prpAbsence->End);
				if (strncmp(prpAbsence->Start,prpAbsence->End,8) < 0)
				{
					/*** absence is more than one day **/
				  char clDay[24];
				  char clEnd[24];

					strcpy(clEnd,prpAbsence->End);
					strcpy(clDay,prpAbsence->Start);
					while (strncmp(clDay,clEnd,8) <= 0)
					{
						strncpy(prpAbsence->StartDate,clDay,8);
						prpAbsence->StartDate[8] = '\0';
						strcpy(prpAbsence->Start,clDay);
						strcpy(prpAbsence->End,clDay);
						dbg(DEBUG,"Start <%s> End <%s>",prpAbsence->Start,prpAbsence->End);
						InsertDrr(prpAbsence,clOdaUrno,clStfUrno,clDay,clDay);
						AddSecondsToCEDATime(clDay,(time_t)86400,1);
					}
				}
				else
				{
					InsertDrr(prpAbsence,clOdaUrno,clStfUrno,clAbfr,clAbto);
				}
		}
	}
	return ilRc;
}


static int FillAttendanceStruct(char *pcpBuf,ABSENCE *prpAbsence)
{
	int ilRc = RC_SUCCESS;
	int ilActualPos = 0;
	char *pclBuf;

	pclBuf = pcpBuf;

	/* Employee Number */
	StringMid(prpAbsence->Peno,pclBuf,ilActualPos,8,CLIENTTODB);
	ilActualPos += 8;
	/* Absence Code */
	StringMid(prpAbsence->Code,pclBuf,ilActualPos,4,CLIENTTODB);
	ilActualPos += 4;
	/* Full day indicator */
	StringMid(prpAbsence->FullDayIndicator,pclBuf,ilActualPos,1,CLIENTTODB);
	ilActualPos += 1;
	/* Start Date */
	StringMid(prpAbsence->StartDate,pclBuf,ilActualPos,8,CLIENTTODB);
	ilActualPos += 8;
	/* End Date */
	StringMid(prpAbsence->EndDate,pclBuf,ilActualPos,8,CLIENTTODB);
	ilActualPos += 8;
	/* Start Time */
	StringMid(prpAbsence->StartTime,pclBuf,ilActualPos,4,CLIENTTODB);
	ilActualPos += 4;
	if (strcmp(prpAbsence->StartTime,"    ") == 0)
	{
		strcpy(prpAbsence->StartTime,"0830");
	}
	/* End Time */
	StringMid(prpAbsence->EndTime,pclBuf,ilActualPos,4,CLIENTTODB);
	ilActualPos += 4;
	if (strcmp(prpAbsence->EndTime,"    ") == 0)
	{
		strcpy(prpAbsence->EndTime,"0830");
	}
	/* Update Indicator */
	StringMid(prpAbsence->UpdateIndicator,pclBuf,ilActualPos,1,CLIENTTODB);
	ilActualPos += 1;

	sprintf(prpAbsence->Start,"%s%s00",prpAbsence->StartDate,prpAbsence->StartTime);
	sprintf(prpAbsence->End,"%s%s00",prpAbsence->EndDate,prpAbsence->EndTime);

	if(CheckDateFormat(prpAbsence->Start) != RC_SUCCESS)
	{
			dbg(TRACE,"invalid date format in absence Start <%s>",prpAbsence->Start);
			return(RC_FAIL);
	}

	if(CheckDateFormat(prpAbsence->End) != RC_SUCCESS)
	{
			dbg(TRACE,"invalid date format in absence End <%s>",prpAbsence->End);
			return(RC_FAIL);
	}

	if(debug_level == DEBUG)
	{
		dbg(TRACE,"FillAttendanceStruct: PENO     <%s>",prpAbsence->Peno);
		dbg(TRACE,"FillAttendanceStruct: CODE     <%s>",prpAbsence->Code);
		dbg(TRACE,"FillAttendanceStruct: FULLDAY  <%s>",prpAbsence->FullDayIndicator);
		dbg(TRACE,"FillAttendanceStruct: STARTDATE<%s>",prpAbsence->StartDate);
		dbg(TRACE,"FillAttendanceStruct: ENDDATE  <%s>",prpAbsence->EndDate);
		dbg(TRACE,"FillAttendanceStruct: STARTTIME<%s>",prpAbsence->StartTime);
		dbg(TRACE,"FillAttendanceStruct: ENDTIME  <%s>",prpAbsence->EndTime);
		dbg(TRACE,"FillAttendanceStruct: UPDATE-I <%s>",prpAbsence->UpdateIndicator);
		dbg(TRACE,"FillAttendanceStruct: START    <%s>",prpAbsence->Start);
		dbg(TRACE,"FillAttendanceStruct: END      <%s>",prpAbsence->End);
	}


	return ilRc;
}

static int FillAbsenceStruct(char *pcpBuf,ABSENCE *prpAbsence)
{
	int ilRc = RC_SUCCESS;
	int ilActualPos = 0;
	char *pclBuf;

	pclBuf = pcpBuf;

	/* Employee Number */
	get_real_item(prpAbsence->Peno, pclBuf, 1);
	StringTrimRight(prpAbsence->Peno);
	/* Function Code */
	get_real_item(prpAbsence->Func, pclBuf, 2);
	StringTrimRight(prpAbsence->Func);
	/* Absence Code */
	get_real_item(prpAbsence->Code, pclBuf, 3);
	StringTrimRight(prpAbsence->Code);
	/* Start Date */
	get_real_item(prpAbsence->StartDate, pclBuf, 4);
	/* End Date */
	get_real_item(prpAbsence->EndDate, pclBuf, 5);

	strcpy(prpAbsence->StartTime,"0830");
	strcpy(prpAbsence->EndTime,"0830");
	
	sprintf(prpAbsence->Start,"%s%s00",prpAbsence->StartDate,prpAbsence->StartTime);
	sprintf(prpAbsence->End,"%s%s00",prpAbsence->EndDate,prpAbsence->EndTime);

	if(CheckDateFormat(prpAbsence->Start) != RC_SUCCESS)
	{
			dbg(TRACE,"invalid date format in absence Start <%s>",prpAbsence->Start);
			ilRc = RC_FAIL;
	}

	if(CheckDateFormat(prpAbsence->End) != RC_SUCCESS)
	{
			dbg(TRACE,"invalid date format in absence End <%s>",prpAbsence->End);
			ilRc = RC_FAIL;
	}

	if(debug_level == DEBUG)
	{
		dbg(TRACE,"FillAbsenceStruct: PENO     <%s>",prpAbsence->Peno);
		dbg(TRACE,"FillAbsenceStruct: CODE     <%s>",prpAbsence->Code);
		dbg(TRACE,"FillAbsenceStruct: FUNC     <%s>",prpAbsence->Func);
		dbg(TRACE,"FillAbsenceStruct: FULLDAY  <%s>",prpAbsence->FullDayIndicator);
		dbg(TRACE,"FillAbsenceStruct: STARTDATE<%s>",prpAbsence->StartDate);
		dbg(TRACE,"FillAbsenceStruct: ENDDATE  <%s>",prpAbsence->EndDate);
		dbg(TRACE,"FillAbsenceStruct: STARTTIME<%s>",prpAbsence->StartTime);
		dbg(TRACE,"FillAbsenceStruct: ENDTIME  <%s>",prpAbsence->EndTime);
		dbg(TRACE,"FillAbsenceStruct: UPDATE-I <%s>",prpAbsence->UpdateIndicator);
		dbg(TRACE,"FillAbsenceStruct: START    <%s>",prpAbsence->Start);
		dbg(TRACE,"FillAbsenceStruct: END      <%s>",prpAbsence->End);
	}
	return ilRc;
}


static int AbsencesImport()
{
	int ilRc = RC_SUCCESS;
	char clSqlBuf[1024];
	char clSequence[12];
	char clDataArea[1024];
	char *pclDataArea;
	short slFunc = START;
	short slCursor;
	char clNow[24];
	char clTo[24];
	int ilLc;
	int ilItemCount;

	GetServerTimeStamp("LOC", 1,0,clNow);
	if (glOffset != 0L)
	{
		AddSecondsToCEDATime(clNow,glOffset,1);
	}
	if (glDuration > 0)
	{
		strcpy(clTo,clNow);
		AddSecondsToCEDATime(clTo,glDuration,1);
		clNow[8] = '\0';
		clTo[8] = '\0';
		sprintf(clSqlBuf,"SELECT EMP_ID,FUNC,LEAVE_ID,to_char(FROM_DATE,'yyyymmdd'),to_char(TO_DATE,'yyyymmdd') FROM %s WHERE FROM_DATE BETWEEN  to_date(%s,'yyyymmdd') AND to_date(%s,'yyyymmdd')",cgEmpLeavesView,clNow,clTo);
		dbg(TRACE,"AbsenceImport from %s to %s",clNow,clTo);
	}
	else
	{
		clNow[8] = '\0';
	sprintf(clSqlBuf,"SELECT EMP_ID,FUNC,LEAVE_ID,to_char(FROM_DATE,'yyyymmdd'),to_char(TO_DATE,'yyyymmdd') FROM %s WHERE FROM_DATE >= to_date(%s,'yyyymmdd')",cgEmpLeavesView,clNow);
		dbg(TRACE,"AbsenceImport from %s ",clNow);
	}

	slCursor = 0;
	ilItemCount = 5; 
	while((ilRc = sql_if(slFunc,&slCursor,clSqlBuf,clDataArea)) == RC_SUCCESS)
	{
		slFunc = NEXT; 
		pclDataArea = clDataArea;
		for(ilLc =  1; ilLc < ilItemCount; ilLc++)
		{
			while(*pclDataArea != '\0')
			{
				pclDataArea++;
			}
				*pclDataArea = ',';
		}
		if (FillAbsenceStruct((char *)clDataArea,&rgAbsenceData) == RC_SUCCESS)
		{
			dbg(TRACE,"AbsenceStruct: Start <%s> End <%s>", rgAbsenceData.Start,rgAbsenceData.End);
			/* Always Full Day Absence, insert into DRRTAB */
			DrrImport(&rgAbsenceData);
		}
	}
	if (ilRc == RC_FAIL)
	{
		/* there must be a database error */
		get_ora_err(ilRc, cgErrMsg);
		dbg(TRACE,"Select from %d failed RC=%d <%s>",cgEmpLeavesView,ilRc,cgErrMsg);
		dbg(TRACE,"SQL: <%s>",clSqlBuf);
	}
  return ilRc;
}


static int GetItem(char* pcpTarget,char* pcpData,char* pcpFieldlist,char* pcpField,int piDeleteBlanks)
{
  int ilRc = RC_FAIL;
  int ilItemNo=0;

  ilItemNo=get_item_no(pcpFieldlist,pcpField,5);
  ilItemNo++;
  ilRc=GetDataItem(pcpTarget,pcpData,ilItemNo,',',"","");
	dbg(TRACE,"GetItem: found <%s> itemno %d RC %d",pcpTarget,ilItemNo,ilRc);
  if(piDeleteBlanks==TRUE)
    {
      DeleteCharacterInString(pcpTarget,cBLANK);
    }
  return ilRc;
}
/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{
	int	   ilRc           = RC_SUCCESS;			/* Return code */
	int      ilCmd          = 0;

	BC_HEAD *prlBchd       = NULL;
	CMDBLK  *prlCmdblk       = NULL;
	char    *pclSelection    = NULL;
	char    *pclFields       = NULL;
	char    *pclData         = NULL;
	char    *pclRow          = NULL;
	char 	clTable[34];
	char    clTmpJob[12];
	char    clUrnoList[50000];
	char	*pclKeyPtr		= NULL;
	char	clPeno[20]	= "\0";
	char	clStaffUrno[20]	= "\0";
	char	clTime[20]	= "\0";
	long	llLen = 0;


	prlBchd    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
	prlCmdblk    = (CMDBLK *)  ((char *)prlBchd->data);
	pclSelection = prlCmdblk->data;
	pclFields    = pclSelection + strlen(pclSelection) + 1;
	pclData      = pclFields + strlen(pclFields) + 1;

	strcpy(clTable,prlCmdblk->obj_name);

	dbg(TRACE,"==========START <%10.10d> ==========",lgEvtCnt);

  igQueOut = prgEvent->originator;

  /* Save Global Elements of CMDBLK */
  strcpy(cgTwStart,prlCmdblk->tw_start);
  strcpy(cgTwEnd,prlCmdblk->tw_end);
  /* Save Global Elements of BC_HEAD */
  strcpy(cgRecvName,prlBchd->recv_name);
  strcpy(cgDestName,prlBchd->dest_name);
 
	/****************************************/
	DebugPrintBchead(DEBUG,prlBchd);
	DebugPrintCmdblk(DEBUG,prlCmdblk);
  dbg(DEBUG,"TwStart   <%s>",cgTwStart);
  dbg(DEBUG,"TwEnd     <%s>",cgTwEnd);
	dbg(DEBUG,"originator<%d>",prpEvent->originator);
	dbg(DEBUG,"selection <%s>",pclSelection);
	dbg(DEBUG,"fields    <%s>",pclFields);
	dbg(DEBUG,"data      <%s>",pclData);
	/****************************************/

  
	memset(pcgStfuList,0,lgStfuListLength);
	lgStfuListActualLength = 0;

	ilRc = GetCommand(&prlCmdblk->command[0],&ilCmd);
	dbg(DEBUG,"RC from GetCommand(%s) is %d",&ilCmd,ilRc);

	if (ilCmd != CMD_URT && ilCmd != CMD_DRT)
	{ /* delete old data from pclData */
		char *pclNewline = NULL;

		if ((pclNewline = strchr(pclData,'\n')) != NULL)
		{
			*pclNewline = '\0';
		}
	}

	if(ilRc == RC_SUCCESS)
	{
		switch (ilCmd)
		{
		case CMD_URT:
				dbg(TRACE,"Command is URT");
		case CMD_IRT:
					ilRc = CEDAArrayEventUpdate2(prgEvent,clUrnoList,clTable);
				break;
	case CMD_DRT :
					ilRc = CEDAArrayEventUpdate2(prgEvent,clUrnoList,clTable);
				break;
		case CMD_ABS:
				dbg(TRACE,"Command is ABS");
			  AbsencesImport();
				break;
		default:
			  dbg(TRACE,"unknown command <%s>",prlCmdblk->command);
		}
	}

	dbg(TRACE,"==========END  <%10.10d> ==========",lgEvtCnt);

	return(RC_SUCCESS);
	
} /* end of HandleData */


static int StrToTime(char *pcpTime,time_t *plpTime)
{
	struct tm *_tm;
	time_t now;
	char   _tmpc[6];
	long llUtcDiff;

	dbg(0,"StrToTime, <%s>",pcpTime);
	if (strlen(pcpTime) < 12 )
	{
		*plpTime = time(0L);
		return RC_FAIL;
	} /* end if */




	now = time(0L);
	_tm = (struct tm *)gmtime(&now);

	_tmpc[2] = '\0';
/*
	strncpy(_tmpc,pcpTime+12,2);
	_tm -> tm_sec = atoi(_tmpc);
*/
	_tm -> tm_sec = 0;
	
	strncpy(_tmpc,pcpTime+10,2);
	_tm -> tm_min = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+8,2);
	_tm -> tm_hour = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+6,2);
	_tm -> tm_mday = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+4,2);
	_tm -> tm_mon = atoi(_tmpc)-1;
	strncpy(_tmpc,pcpTime,4);
	_tmpc[4] = '\0';
	_tm ->tm_year = atoi(_tmpc)-1900;
	_tm ->tm_wday = 0;
	_tm ->tm_yday = 0;
	_tm->tm_isdst = -1; /* no adjustment of daylight saving time */
	now = mktime(_tm);
	dbg(0,"StrToTime: %02d.%02d.%04d %02d:%02d Now: %ld",_tm->tm_mday,
_tm->tm_mon+1,_tm->tm_year+1900,_tm->tm_hour,_tm->tm_min,now);
	if (now != (time_t) -1)
	{
	{
		time_t utc,local;

	_tm = (struct tm *)gmtime(&now);
	utc = mktime(_tm);
	_tm = (struct tm *)localtime(&now);
	local = mktime(_tm);
	dbg(TRACE,"utc %ld, local %ld, utc-diff %ld",
		utc,local,local-utc);
		llUtcDiff = local-utc;
	}
		now +=  + llUtcDiff;
		*plpTime = now;
		return RC_SUCCESS;
	}
	*plpTime = time(NULL);
	return RC_FAIL;
}


/**
dbg(TRACE,"%05d:",__LINE__);
***/



/*
 *     struct  tm {
 *             int     tm_sec;    seconds after the minute -- [0, 59]  
 *             int     tm_min;    minutes after the hour -- [0, 59] 
 *             int     tm_hour;   hour since midnight -- [0, 23] 
 *             int     tm_mday;   day of the month -- [1, 31] 
 *             int     tm_mon;    months since January -- [0, 11] 
 *             int     tm_year;   years since 1900 
 *             int     tm_wday;   days since Sunday -- [0, 6] 
 *             int     tm_yday;   days since January 1 -- [0, 365] 
 *             int     tm_isdst;  flag for daylight savings time 
 *             long    tm_tzadj;  seconds from GMT (east < 0) 
 *             char    tm_name[LTZNMAX];   name of timezone 
 *     };
 */




void	StrAddTime(char *pcpBase,char *pcpOffs,char *pcpDest)
{

	if (pcpBase != pcpDest)
	{
		strcpy(pcpDest,pcpBase);
	}
	AddSecondsToCEDATime(pcpDest,(time_t)atol(pcpOffs),1);

} /* end of StrAddTime */



int check_ret1(int ipRc,int ipLine)
{
	int ilOldDebugLevel;

	ilOldDebugLevel = debug_level;

	debug_level = TRACE;

	dbg(TRACE,"DB-ERROR in Line: %d",ipLine);
	debug_level  = ilOldDebugLevel;
	return check_ret(ipRc);
}

static void HandleQueues()
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	int	ilBreakOut = FALSE;
	
	do
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;	

		if( ilRc == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRc != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRc);
			} /* fi */
		
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				ilBreakOut = TRUE;
				break;	

			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(1);
				break;	
	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				ilBreakOut = TRUE;
				break;	

			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;

			case	SHUTDOWN	:
				Terminate(1);
				break;
						
			case	RESET		:
				ilRc = Reset();
				break;
						
			case	EVENT_DATA	:
				dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
					
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;

			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;

			default			:
				dbg(TRACE,"HandleQueues: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
	} while (ilBreakOut == FALSE);

	if(igInitOK == FALSE)
	{
			ilRc = InitAbsimp();
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitAbsimp failed!");
			} /* end of if */
	}/* end of if */

}

static int TriggerAction(char *pcpTableName,char *pcpFields)
{
  int           ilRc          = RC_SUCCESS ;    /* Return code */
  EVENT        *prlEvent      = NULL ;
  BC_HEAD      *prlBchead     = NULL ;
  CMDBLK       *prlCmdblk     = NULL ;
  /*ACTIONConfig *prlAction     = NULL ;*/
	ACTIONConfig rlAction;
  char         *pclSelection  = NULL ;
  char         *pclFields     = NULL ;
  char         *pclData       = NULL ;
  long          llActSize     = 0 ;
  int           ilAnswerQueue = 0 ;
  char          clQueueName[16] ;
	
  if(ilRc == RC_SUCCESS)
  {

  }/* end of if */
	

  if (ilRc == RC_SUCCESS)
  {
   sprintf (&clQueueName[0],"%s2", mod_name) ;

   ilRc = GetDynamicQueue(&ilAnswerQueue,&clQueueName[0]);
   if (ilRc != RC_SUCCESS)
   {
    dbg(TRACE,"TriggerAction: GetDynamicQueue failed <%d>",ilRc);
   }
   else
   {
    dbg(DEBUG,"TriggerAction: got <%d> for <%s>",ilAnswerQueue, &clQueueName[0]);
   }/* end of if */
  }/* end of if */
	
  if (ilRc == RC_SUCCESS)
  {
   llActSize = sizeof(EVENT) + sizeof(BC_HEAD) + 
	 		sizeof(CMDBLK) + sizeof(ACTIONConfig) + strlen(pcpFields) + 666;
   prgOutEvent = realloc(prgOutEvent,llActSize);
   if(prgOutEvent == NULL)
   {
    dbg(TRACE,"TriggerAction: realloc out event <%d> bytes failed",llActSize);
    ilRc = RC_FAIL ;
   } /* end of if */
  } /* end of if */

  if(ilRc == RC_SUCCESS)
  {
   prlBchead     = (BC_HEAD *) ((char *) prgOutEvent + sizeof(EVENT)) ;
   prlCmdblk     = (CMDBLK *)  ((char *) prlBchead->data) ;
   pclSelection  = prlCmdblk->data ;
   pclFields     = pclSelection + strlen (pclSelection) + 1 ;
	 strcpy(pclFields,pcpFields);
   pclData       = pclFields + strlen (pclFields) + 1 ;
   strcpy (pclData, "DYN") ;
	memset(&rlAction,0,sizeof(ACTIONConfig));
   /*prlAction     = (ACTIONConfig *) (pclData + strlen(pclData) + 1) ;*/
	
	prlBchead->rc = RC_SUCCESS;
	
   prgOutEvent->type        = USR_EVENT ;
   prgOutEvent->command     = EVENT_DATA ;
   prgOutEvent->originator  = ilAnswerQueue ;
   prgOutEvent->retry_count = 0 ;
   prgOutEvent->data_offset = sizeof(EVENT) ;
   prgOutEvent->data_length = llActSize-sizeof(EVENT) ;

	rlAction.iEmptyFieldHandling = iDELETE_ALL_BLANKS ; 
	rlAction.iIgnoreEmptyFields = 0 ; 
	strcpy(rlAction.pcSndCmd, "") ; 
	sprintf(rlAction.pcSectionName,"%s_%s", mod_name,pcpTableName) ; 
	strcpy(rlAction.pcTableName, pcpTableName) ; 
	strcpy(rlAction.pcFields, pcpFields) ; 
  strcpy(rlAction.pcSectionCommands, "IRT,URT,DRT") ;
	rlAction.iModID = mod_id ;   

	 /***
   prlAction->iEmptyFieldHandling = iDELETE_ALL_BLANKS ;
   prlAction->iIgnoreEmptyFields = 0 ;
   strcpy(prlAction->pcSndCmd, "") ;
   sprintf(prlAction->pcSectionName,"%s_%s", mod_name,pcpTableName) ;
   strcpy(prlAction->pcTableName, pcpTableName) ;
   strcpy(prlAction->pcFields, pcpFields) ;
   strcpy(prlAction->pcSectionCommands, "IRT,URT,DRT") ;
   prlAction->iModID = mod_id ;
	 ***/
	 /*********************** MCU TEST **************
   strcpy(prlAction->pcFields, "-") ;
   strcpy(prlAction->pcFields, "") ;
	 *********************** MCU TEST ***************/
	 /***********
	 if (!strcmp(pcpTableName,"SPRTAB"))
		{
   	strcpy(prlAction->pcFields, "PKNO,USTF,ACTI,FCOL") ;
	 }
	 ************/

   /****************************************
     DebugPrintEvent(DEBUG,prgOutEvent) ;
     DebugPrintBchead(DEBUG,prlBchead) ;
     DebugPrintCmdblk(DEBUG,prlCmdblk) ;
     dbg (DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
     dbg (DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
     dbg (DEBUG,"TriggerAction: data      <%s>",pclData) ;
     DebugPrintACTIONConfig(DEBUG,prlAction) ;
   ****************************************/

   if(ilRc == RC_SUCCESS)
   {
    rlAction.iADFlag = iDELETE_SECTION ;
	 memcpy(pclData + strlen(pclData) + 1,&rlAction,sizeof(ACTIONConfig));

    ilRc = que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_2, llActSize, (char *) prgOutEvent) ; 
    if(ilRc != RC_SUCCESS)
    {
     dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRc) ;
    }
    else
    {
     ilRc = WaitAndCheckQueue(100, ilAnswerQueue,&prgItem) ;
     if(ilRc != RC_SUCCESS)
     {
      dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRc) ;
     }
     else
     {
      /* return value for delete does not matter !!! */
		
      /****************************************
      prlEvent     = (EVENT *)   ((char *)prgItem->text) ;
      prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT)) ;
      prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data) ;
      pclSelection = (char *)    prlCmdblk->data ;
      pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1;
      pclData      = (char *)    pclFields + strlen(pclFields) + 1;
      DebugPrintItem(DEBUG, prgItem) ;
      DebugPrintEvent(DEBUG, prlEvent) ;
      DebugPrintBchead(DEBUG, prlBchead) ;
      DebugPrintCmdblk(DEBUG, prlCmdblk) ;
      dbg(DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
      dbg(DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
      dbg(DEBUG,"TriggerAction: data      <%s>",pclData) ;
      ****************************************/
    } /* end of if */
   }/* end of if */
  }/* end of if */

  if(ilRc == RC_SUCCESS)
  {
   rlAction.iADFlag = iADD_SECTION ;
	 memcpy(pclData + strlen(pclData) + 1,&rlAction,sizeof(ACTIONConfig));

   ilRc = que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_3, llActSize,
                                              (char *) prgOutEvent ) ;
   if(ilRc != RC_SUCCESS)
   {
    dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRc) ;
   }
   else
   {
    ilRc = WaitAndCheckQueue(100,ilAnswerQueue,&prgItem) ;
    if(ilRc != RC_SUCCESS)
    {
     dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRc) ;
    }
    else
    {
     prlEvent     = (EVENT *)   ((char *)prgItem->text) ;
     prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT)) ;
     prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data) ;
     pclSelection = (char *)    prlCmdblk->data ;
     pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1 ;
     pclData      = (char *)    pclFields + strlen(pclFields) + 1 ;

     if(strcmp(pclData,"SUCCESS") != 0)
     {
      dbg(TRACE,"TriggerAction: add dynamic action config failed") ;
      DebugPrintItem(DEBUG,prgItem) ;
      DebugPrintEvent(DEBUG,prlEvent) ;
      DebugPrintBchead(DEBUG,prlBchead) ;
      DebugPrintCmdblk(DEBUG,prlCmdblk) ;
      dbg (DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
      dbg (DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
      dbg(DEBUG,"TriggerAction: data      <%s>",pclData) ;
      ilRc = RC_FAIL ;
     }
     else
     {
      dbg(DEBUG,"TriggerAction: add dynamic action config OK") ;
     }/* end of if */
    }/* end of if */
   }/* end of if */
  }/* end of if */

  ilRc = que(QUE_DELETE,ilAnswerQueue,ilAnswerQueue,0,0,0) ;
  if(ilRc != RC_SUCCESS)
  {
   dbg(TRACE,"que QUE_DELETE <%d> failed <%d>",ilAnswerQueue, ilRc) ;
  }
  else
  {
   dbg(DEBUG,"TriggerAction: queue <%d> <%s> deleted", ilAnswerQueue, &clQueueName[0]) ;
  }/* end of if */
 }/* end of if */

 return (ilRc) ;
	
}


static int TriggerActionSAP(char *pcpTableName)
{
  int           ilRc          = RC_SUCCESS ;    /* Return code */
  EVENT        *prlEvent      = NULL ;
  BC_HEAD      *prlBchead     = NULL ;
  CMDBLK       *prlCmdblk     = NULL ;
  ACTIONConfig *prlAction     = NULL ;
  char         *pclSelection  = NULL ;
  char         *pclFields     = NULL ;
  char         *pclData       = NULL ;
  long          llActSize     = 0 ;
  int           ilAnswerQueue = 0 ;
  char          clQueueName[16] ;
	
  if(ilRc == RC_SUCCESS)
  {

  }/* end of if */
	

  if (ilRc == RC_SUCCESS)
  {
   sprintf (&clQueueName[0],"%s2", mod_name) ;

   ilRc = GetDynamicQueue(&ilAnswerQueue,&clQueueName[0]);
   if (ilRc != RC_SUCCESS)
   {
    dbg(TRACE,"TriggerAction: GetDynamicQueue failed <%d>",ilRc);
   }
   else
   {
    dbg(DEBUG,"TriggerAction: got <%d> for <%s>",ilAnswerQueue, &clQueueName[0]);
   }/* end of if */
  }/* end of if */
	
  if (ilRc == RC_SUCCESS)
  {
   llActSize = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + sizeof(ACTIONConfig) + 666;
   prgOutEvent = realloc(prgOutEvent,llActSize);
   if(prgOutEvent == NULL)
   {
    dbg(TRACE,"TriggerAction: realloc out event <%d> bytes failed",llActSize);
    ilRc = RC_FAIL ;
   } /* end of if */
  } /* end of if */

  if(ilRc == RC_SUCCESS)
  {
   prlBchead     = (BC_HEAD *) ((char *) prgOutEvent + sizeof(EVENT)) ;
   prlCmdblk     = (CMDBLK *)  ((char *) prlBchead->data) ;
   pclSelection  = prlCmdblk->data ;
   pclFields     = pclSelection + strlen (pclSelection) + 1 ;
   pclData       = pclFields + strlen (pclFields) + 1 ;
   strcpy (pclData, "DYN") ;
   prlAction     = (ACTIONConfig *) (pclData + strlen(pclData) + 1) ;
	
	prlBchead->rc = RC_SUCCESS;
	
   prgOutEvent->type        = USR_EVENT ;
   prgOutEvent->command     = EVENT_DATA ;
   prgOutEvent->originator  = ilAnswerQueue ;
   prgOutEvent->retry_count = 0 ;
   prgOutEvent->data_offset = sizeof(EVENT) ;
   prgOutEvent->data_length = llActSize-sizeof(EVENT) ;

   prlAction->iEmptyFieldHandling = iDELETE_ALL_BLANKS ;
   prlAction->iIgnoreEmptyFields = 0 ;
   strcpy(prlAction->pcSndCmd, "") ;
   sprintf(prlAction->pcSectionName,"%s_%s", mod_name,pcpTableName) ;
   strcpy(prlAction->pcTableName, pcpTableName) ;
   strcpy(prlAction->pcFields, "-") ;
	 /*********************** MCU TEST ***************/
   strcpy(prlAction->pcFields, "") ;
	 /*********************** MCU TEST ***************/
	 /************/
	 if (!strcmp(pcpTableName,"SPRTAB"))
		{
   	strcpy(prlAction->pcFields, "PKNO,USTF,ACTI,FCOL") ;
	 }
	 /************/
   strcpy(prlAction->pcSectionCommands, "IRT,URT,DRT") ;
   prlAction->iModID = mod_id ;

   /****************************************
     DebugPrintEvent(DEBUG,prgOutEvent) ;
     DebugPrintBchead(DEBUG,prlBchead) ;
     DebugPrintCmdblk(DEBUG,prlCmdblk) ;
     dbg (DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
     dbg (DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
     dbg (DEBUG,"TriggerAction: data      <%s>",pclData) ;
     DebugPrintACTIONConfig(DEBUG,prlAction) ;
   ****************************************/

   if(ilRc == RC_SUCCESS)
   {
    prlAction->iADFlag = iDELETE_SECTION ;

    ilRc = que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_3, llActSize, (char *) prgOutEvent) ; 
    if(ilRc != RC_SUCCESS)
    {
     dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRc) ;
    }
    else
    {
     ilRc = WaitAndCheckQueue(10, ilAnswerQueue,&prgItem) ;
     if(ilRc != RC_SUCCESS)
     {
      dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRc) ;
     }
     else
     {
      /* return value for delete does not matter !!! */
		
      /****************************************
      prlEvent     = (EVENT *)   ((char *)prgItem->text) ;
      prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT)) ;
      prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data) ;
      pclSelection = (char *)    prlCmdblk->data ;
      pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1;
      pclData      = (char *)    pclFields + strlen(pclFields) + 1;
      DebugPrintItem(DEBUG, prgItem) ;
      DebugPrintEvent(DEBUG, prlEvent) ;
      DebugPrintBchead(DEBUG, prlBchead) ;
      DebugPrintCmdblk(DEBUG, prlCmdblk) ;
      dbg(DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
      dbg(DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
      dbg(DEBUG,"TriggerAction: data      <%s>",pclData) ;
      ****************************************/
    } /* end of if */
   }/* end of if */
  }/* end of if */

  if(ilRc == RC_SUCCESS)
  {
   prlAction->iADFlag = iADD_SECTION ;

   ilRc = que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_3, llActSize,
                                              (char *) prgOutEvent ) ;
   if(ilRc != RC_SUCCESS)
   {
    dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRc) ;
   }
   else
   {
    ilRc = WaitAndCheckQueue(10,ilAnswerQueue,&prgItem) ;
    if(ilRc != RC_SUCCESS)
    {
     dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRc) ;
    }
    else
    {
     prlEvent     = (EVENT *)   ((char *)prgItem->text) ;
     prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT)) ;
     prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data) ;
     pclSelection = (char *)    prlCmdblk->data ;
     pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1 ;
     pclData      = (char *)    pclFields + strlen(pclFields) + 1 ;

     if(strcmp(pclData,"SUCCESS") != 0)
     {
      dbg(TRACE,"TriggerAction: add dynamic action config failed") ;
      DebugPrintItem(DEBUG,prgItem) ;
      DebugPrintEvent(DEBUG,prlEvent) ;
      DebugPrintBchead(DEBUG,prlBchead) ;
      DebugPrintCmdblk(DEBUG,prlCmdblk) ;
      dbg (DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
      dbg (DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
      dbg(DEBUG,"TriggerAction: data      <%s>",pclData) ;
      ilRc = RC_FAIL ;
     }
     else
     {
      dbg(DEBUG,"TriggerAction: add dynamic action config OK") ;
     }/* end of if */
    }/* end of if */
   }/* end of if */
  }/* end of if */

  ilRc = que(QUE_DELETE,ilAnswerQueue,ilAnswerQueue,0,0,0) ;
  if(ilRc != RC_SUCCESS)
  {
   dbg(TRACE,"que QUE_DELETE <%d> failed <%d>",ilAnswerQueue, ilRc) ;
  }
  else
  {
   dbg(DEBUG,"TriggerAction: queue <%d> <%s> deleted", ilAnswerQueue, &clQueueName[0]) ;
  }/* end of if */
 }/* end of if */

 return (ilRc) ;
	
}
static int WaitAndCheckQueue(int ipTimeout, int ipModId, ITEM **prpItem)
{
	int ilRc          = RC_SUCCESS;                      /* Return code */
	int ilWaitCounter = 0;

	do
	{
		sleep(1);
		ilWaitCounter += 1;

		ilRc = CheckQueue(ipModId,prpItem);
		if(ilRc != RC_SUCCESS)
		{
			if(ilRc != QUE_E_NOMSG)
			{
				dbg(TRACE,"WaitAndCheckQueue: CheckQueue <%d> failed <%d>",mod_id,ilRc);
			}/* end of if */
		}/* end of if */
	}while((ilRc == QUE_E_NOMSG) && (ilWaitCounter < ipTimeout));

	if(ilWaitCounter >= ipTimeout)
	{
		dbg(TRACE,"WaitAndCheckQueue: timeout reached <%d>",ipTimeout);
		ilRc = RC_FAIL;
	}/* end of if */

	return ilRc;
}


static int CheckQueue(int ipModId, ITEM **prpItem)
{
	int     ilRc       = RC_SUCCESS;                      /* Return code */
	int     ilItemSize = 0;
	EVENT *prlEvent    = NULL;
static	ITEM  *prlItem      = NULL;
	
	ilItemSize = I_SIZE;
	
	ilRc = que(QUE_GETBIGNW,ipModId,ipModId,PRIORITY_3,ilItemSize,(char *) &prlItem);
	if(ilRc == RC_SUCCESS)
	{
		dbg(TRACE,"DebugPrintItem with prlItem follows");
		DebugPrintItem(TRACE,prlItem);
		*prpItem = prlItem;
		dbg(TRACE,"DebugPrintItem with prpItem follows");
		DebugPrintItem(TRACE,*prpItem);
		prlEvent = (EVENT*) ((prlItem)->text);

		switch( prlEvent->command )
		{
			case    HSB_STANDBY :
				dbg(TRACE,"CheckQueue: HSB_STANDBY");
				HandleQueues();
				ctrl_sta = prlEvent->command;
				break;
	
			case    HSB_COMING_UP   :
				dbg(TRACE,"CheckQueue: HSB_COMING_UP");
				ctrl_sta = prlEvent->command;
				break;
	
			case    HSB_ACTIVE  :
				dbg(TRACE,"CheckQueue: HSB_ACTIVE");
				ctrl_sta = prlEvent->command;
				break;
	
			case    HSB_ACT_TO_SBY  :
				dbg(TRACE,"CheckQueue: HSB_ACT_TO_SBY");
				HandleQueues();
				ctrl_sta = prlEvent->command;
				break;
	
			case    HSB_DOWN    :
				dbg(TRACE,"CheckQueue: HSB_DOWN");
				ctrl_sta = prlEvent->command;
				/* Acknowledge the item 
				ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
				if( ilRc != RC_SUCCESS )
				{
					HandleQueErr(ilRc);
				}  */
				Terminate(30);
				break;
		
		case    HSB_STANDALONE  :
				dbg(TRACE,"CheckQueue: HSB_STANDALONE");
				ctrl_sta = prlEvent->command;
				break;
	
			case    SHUTDOWN    :
				dbg(TRACE,"CheckQueue: SHUTDOWN");
				/* Acknowledge the item */
				ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
				if( ilRc != RC_SUCCESS )
				{
					/* handle que_ack error */
					HandleQueErr(ilRc);
				} /* fi */
				Terminate(30);
				break;
	
			case    RESET       :
				ilRc = Reset();
				break;
	
			case    EVENT_DATA  :
				/* DebugPrintItem(DEBUG,prgItem); */
				/* DebugPrintEvent(DEBUG,prlEvent); */
				break;
	
			case    TRACE_ON :
				dbg_handle_debug(prlEvent->command);
				break;
	
			case    TRACE_OFF :
				dbg_handle_debug(prlEvent->command);
				break;


			default         :
				dbg(TRACE,"CheckQueue: unknown event");
				DebugPrintItem(TRACE,prlItem);
				DebugPrintEvent(TRACE,prlEvent);
				break;
	
		} /* end switch */
	
		/* Acknowledge the item */
		ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
		if( ilRc != RC_SUCCESS )
		{
			/* handle que_ack error */
			HandleQueErr(ilRc);
		} /* fi */
	}else{
		if(ilRc != QUE_E_NOMSG)
		{
			dbg(TRACE,"CheckQueue: que(QUE_GETBIG,%d,%d,...) failed <%d>",ipModId,ipModId,ilRc);
			HandleQueErr(ilRc);
		}/* end of if */
	}/* end of if */
       
	return ilRc;
}/* end of CheckQueue */



/****************************************************************************
* Function: GetUrnoFromSelection
* Parameter:           
*			 
* Return:
* Description: gets the Urno from the SQL Statement "WHERE URNO = 'xxxxxxx'"
*			   
*****************************************************************************/
static int GetUrnoFromSelection(char *pcpSelection,char *pcpUrno)
{
	char *pclFirst;
	char *pclLast;
	char pclTmp[24];
	int ilLen;
	
	pclFirst = strchr(pcpSelection,'\'');
	if (pclFirst != NULL)
	{
		pclLast  = strrchr(pcpSelection,'\'');
		if (pclLast == NULL)
		{
			pclLast = pclFirst + strlen(pclFirst);
		}
		ilLen = pclLast-pclFirst < 22 ? pclLast - pclFirst : 22;
		strncpy(pclTmp,++pclFirst,ilLen);
		pclTmp[ilLen-1] = '\0';
		strcpy(pcpUrno,pclTmp);
	}
	else
		{
		 pclFirst = pcpSelection;
		 while (!isdigit(*pclFirst) && *pclFirst != '\0')
		 {
			pclFirst++;
			}
		strcpy(pcpUrno,pclFirst);
		}
	return RC_SUCCESS;
}


static int SaveIndexInfo (ARRAYINFO *prpArrayInfo)
{
  int    ilRc       = RC_SUCCESS ;
  long   llRow      = 0 ;
  FILE  *prlFile    = NULL ;
  char   clDel      = ',' ;
  char   clFile[512] ;
  char  *pclTrimBuf = NULL ;

  dbg (TRACE,"SaveIndexInfo: <%s>",prpArrayInfo->crArrayName) ;

  if (ilRc == RC_SUCCESS)
  {
   sprintf (&clFile[0],"%s/%s_%s.info",getenv("DBG_PATH"),mod_name,prpArrayInfo->crArrayName) ;

   errno = 0 ;
   prlFile = fopen (&clFile[0], "w") ;
   if (prlFile == NULL)
   {
    dbg (TRACE, "SaveIndexInfo: fopen <%s> failed <%d-%s>", &clFile[0], errno, strerror(errno)) ;
    ilRc = RC_FAIL ;
   } /* end of if */
  } /* end of if */

  if (ilRc == RC_SUCCESS)
  {
   if (igTestOutput > 2) 
   {
    dbg (DEBUG, "ArrayName      <%s>\n", prpArrayInfo->crArrayName) ;      
    dbg (DEBUG, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList) ;  
    dbg (DEBUG, "Idx01FieldList <%s>\n", prpArrayInfo->crIdx01FieldList) ;  
   }

   fprintf (prlFile, "ArrayName      <%s>\n", prpArrayInfo->crArrayName) ; fflush(prlFile) ;
   fprintf (prlFile, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList) ; fflush(prlFile) ;
   fprintf (prlFile, "Idx01FieldList <%s>\n", prpArrayInfo->crIdx01FieldList) ; fflush(prlFile) ;

   if (igTestOutput > 1) 
   {
    dbg(DEBUG,"IndexData") ; 
   }
   fprintf(prlFile,"IndexData\n"); fflush(prlFile);

   llRow = ARR_FIRST ;
   do
   {
    ilRc = CEDAArrayFindKey (&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), &(prpArrayInfo->rrIdx01Handle), prpArrayInfo->crIdx01Name, &llRow, prpArrayInfo->lrIdx01RowLen, prpArrayInfo->pcrIdx01RowBuf);
    if (ilRc == RC_SUCCESS)
    {
     if (igTestOutput > 1) 
     {
      dbg(DEBUG, "SaveIndexInfo: Key <%ld> <%s>",llRow,prpArrayInfo->pcrIdx01RowBuf) ;
     }

     if (strlen (prpArrayInfo->pcrIdx01RowBuf) == 0)
     {
      ilRc = CEDAArrayGetRow(&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), llRow, clDel, prpArrayInfo->lrArrayRowLen, prpArrayInfo->pcrArrayRowBuf);
      if(ilRc == RC_SUCCESS)
      {
       if (igTestOutput > 1) 
       {
        dbg (DEBUG, "SaveIndexInfo: Row <%ld> <%s>", llRow,prpArrayInfo->pcrArrayRowBuf) ;
       }
      } /* end of if */
     } /* end of if */

     pclTrimBuf = strdup (prpArrayInfo->pcrIdx01RowBuf) ;
     if (pclTrimBuf != NULL)
     {
      ilRc = GetDataItem (pclTrimBuf,prpArrayInfo->pcrIdx01RowBuf, 1, clDel, "", "  ") ;
      if (ilRc > 0)
      {
       ilRc = RC_SUCCESS ;
       fprintf (prlFile,"Key            <%ld><%s>\n",llRow,pclTrimBuf) ; fflush(prlFile) ;
      } /* end of if */

      free (pclTrimBuf) ;
      pclTrimBuf = NULL ;
     } /* end of if */

     llRow = ARR_NEXT;
    }
    else
    {
     dbg (TRACE, "SaveIndexInfo: CEDAArrayFindKey failed <%d>", ilRc) ;
    } /* end of if */
   } while (ilRc == RC_SUCCESS) ;

   if (igTestOutput > 1) 
   {
    dbg (DEBUG,"ArrayData") ;
   }
   fprintf (prlFile,"ArrayData\n"); fflush(prlFile);

   llRow = ARR_FIRST ;
   do
   {
    ilRc = CEDAArrayFindKey (&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), &(prpArrayInfo->rrIdx01Handle), prpArrayInfo->crIdx01Name, &llRow, prpArrayInfo->lrIdx01RowLen, prpArrayInfo->pcrIdx01RowBuf);
    if (ilRc == RC_SUCCESS)
    {
     ilRc = CEDAArrayGetRow(&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), llRow, clDel, prpArrayInfo->lrArrayRowLen, prpArrayInfo->pcrArrayRowBuf);
     if (ilRc == RC_SUCCESS)
     {
      if (igTestOutput > 1) 
      {
       dbg(DEBUG,"SaveIndexInfo: Row <%ld> <%s>",llRow,prpArrayInfo->pcrArrayRowBuf) ;
      }
      fprintf (prlFile,"Row            <%ld><%s>\n",llRow,prpArrayInfo->pcrArrayRowBuf) ; fflush(prlFile) ;
     }
     else
     {
      dbg (TRACE, "SaveIndexInfo: CEDAArrayGetRow failed <%d>", ilRc) ;
     } /* end of if */

     llRow = ARR_NEXT ;
    } 
    else
    {
     dbg (TRACE, "SaveIndexInfo: CEDAArrayFindKey failed <%d>", ilRc) ;
    } /* end of if */
   } while (ilRc == RC_SUCCESS) ;

   if (ilRc == RC_NOTFOUND)
   {
    ilRc = RC_SUCCESS ;
   } /* end of if */
  } /* end of if */

  if (prlFile != NULL)
  {
   fclose(prlFile) ;
   prlFile = NULL ;
   if (igTestOutput > 1) 
   {
    dbg (TRACE, "SaveIndexInfo: event saved - file <%s>", &clFile[0]) ;
   }
  } /* end of if */

  return (ilRc) ;

}

		
static int SendAnswer(int ipDest, char *pcpAnswer)
{
	int			ilRC;
	int			ilLen;
	EVENT			*prlOutEvent 	= NULL;
	BC_HEAD		*prlOutBCHead 	= NULL;
	CMDBLK		*prlOutCmdblk	= NULL;

    dbg(DEBUG,"SendAnswer: START-->");

	/* calculate size of memory we need */
	ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + strlen(pcpAnswer) + 32 ;

	/* get memory for out event */
	if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
	{
		dbg(TRACE,"<SendAnswer> %05d malloc failed, can't allocate %d bytes", __LINE__, ilLen) ;
		dbg(DEBUG,"<SendAnswer> ----- END -----") ;
		return RC_FAIL ;
	}

	/* clear buffer */
	memset((void*)prlOutEvent, 0x00, ilLen) ;

	/* set structure members */
	prlOutEvent->type	   = SYS_EVENT ;
	prlOutEvent->command 	   = EVENT_DATA ;
	prlOutEvent->originator  = ipDest ;            /* mod_id ;   */ 
	prlOutEvent->retry_count = 0 ;
	prlOutEvent->data_offset = sizeof(EVENT) ;
	prlOutEvent->data_length = ilLen - sizeof(EVENT) ;

	/* BCHead members */
	prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT)) ;
	prlOutBCHead->rc = NETOUT_NO_ACK ;
	strncpy(prlOutBCHead->dest_name, "ACTION", 10) ;

	/* CMDBLK members */
	prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data) ;
	strcpy(prlOutCmdblk->command, "FIR") ;
	strcpy(prlOutCmdblk->tw_end, "ANS,WER,ACTION") ;

	/* data */
	strcpy(prlOutCmdblk->data+2, pcpAnswer) ;

	/* send this message */
	dbg(DEBUG,"<SendAnswer> sending to Mod-ID: %d", prgEvent->originator) ;
	if ((ilRC = que(QUE_PUT, prgEvent->originator, mod_id, PRIORITY_3, ilLen, (char*)prlOutEvent)) != RC_SUCCESS)
	{
		/* delete memory */
		free((void*)prlOutEvent) ;
		dbg(TRACE,"<SendAnswer> %05d QUE_PUT returns: %d", __LINE__, ilRC) ;
		dbg(DEBUG,"<SendAnswer> ----- END -----") ;
		return RC_FAIL ;
	}

/*  
	DebugPrintBchead (DEBUG, prlOutBCHead) ;
	DebugPrintCmdblk (DEBUG, prlOutCmdblk) ;
	DebugPrintEvent  (DEBUG, prlOutEvent) ;               
*/
	/* release memory */
	free((void*)prlOutEvent);
	
    dbg(DEBUG,"SendAnswer: <-- ENDE ");
	
	/* bye bye */
	return RC_SUCCESS;
}

static int GetDebugLevel(char *pcpMode, int *pipMode)
{
	int		ilRC = RC_SUCCESS;
	char	clCfgValue[64];

	ilRC = iGetConfigRow(cgConfigFile, "SYSTEM", pcpMode, CFG_STRING,
							clCfgValue);

	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"SYSTEM %s not found in <%s>", pcpMode, cgConfigFile);
	}
	else
	{
		dbg(TRACE, "SYSTEM %s <%s>", pcpMode, clCfgValue);
		if (!strcmp(clCfgValue, "DEBUG"))
			*pipMode = DEBUG;
		else if (!strcmp(clCfgValue, "TRACE"))
			*pipMode = TRACE;
		else
			*pipMode = 0;
	}

	return ilRC;
}

static int ItemCnt(char *pcgData, char cpDelim)
{
	int	ilItemCount=0;

	if (pcgData && strlen(pcgData)) 
	{
	    ilItemCount = 1;
	    while (*pcgData) 
		{
			if (*pcgData++ == cpDelim) 
			{
				ilItemCount++;
			} /* end if */
		} /* end while */
	} /* end if */
	return ilItemCount;
}


static int DumpArray(ARRAYINFO *prpArray,char *pcpFields)
{
	int ilRc;
	int ilRecord = 0;
	long llRowNum = ARR_FIRST;
	char *pclRowBuf;

	dbg(TRACE,"Arraydump: %s", prpArray->crArrayName);
	if (pcpFields != NULL)
	{
		dbg(TRACE,"Arraydump: Fields = %s", pcpFields);
	}
	else
	{
		dbg(TRACE,"Arraydump: Fields = %s",prpArray->crArrayFieldList);
	}

	while(CEDAArrayGetRowPointer(&prpArray->rrArrayHandle,
		prpArray->crArrayName,llRowNum,(void *)&pclRowBuf) == RC_SUCCESS)       
	{
			int ilFieldNo;
		
			ilRecord++;
			llRowNum = ARR_NEXT;

			dbg(TRACE,"Record No: %d",ilRecord);
			for (ilFieldNo = 1; ilFieldNo < prpArray->lrArrayFieldCnt; ilFieldNo++)
			{
				char pclFieldName[24];

				*pclFieldName = '\0';
				GetDataItem(pclFieldName,prpArray->crArrayFieldList,
					ilFieldNo,',',"","\0\0");
				if (pcpFields != NULL)      
				{
					if(strstr(pcpFields,pclFieldName) == NULL)
					{
						continue;
					}
				}
				dbg(TRACE,"%s: <%s>",
						pclFieldName,PFIELDVAL((prpArray),pclRowBuf,ilFieldNo-1));
			}
	}

	return ilRc;

}


static int CheckDateFormat(char *pcpDate)
{
	int ilRc = RC_SUCCESS;

	char clYear[8];
	char clMonth[4];
	char clDay[4];
	char clHour[4];
	char clMin[4];
	char clSec[4];
	char *pclDate;
	int ilActualPos = 0;
	int ilLc;
	static int igDaysPerMonth[12] = {31,28,31,30,31,30,31,31,30,31,30,31};

	int ilYear,ilMonth,ilDay,ilHour,ilMin,ilSec;

	for (ilLc = 0; pcpDate[ilLc] != '\0'; ilLc++)
	{
			if(! (pcpDate[ilLc] >= '0' && pcpDate[ilLc] <= '9'))
			{
				strcpy(pcpDate," ");
				return (RC_FAIL);
			}
	}

	pclDate = pcpDate;

	StringMid(clYear,pclDate,ilActualPos,4,NOCONVERT);
	ilActualPos += 4;
	StringMid(clMonth,pclDate,ilActualPos,2,NOCONVERT);
	ilActualPos += 2;
	StringMid(clDay,pclDate,ilActualPos,2,NOCONVERT);
	ilActualPos += 2;
	StringMid(clHour,pclDate,ilActualPos,2,NOCONVERT);
	ilActualPos += 2;
	StringMid(clMin,pclDate,ilActualPos,2,NOCONVERT);
	ilActualPos += 2;
	StringMid(clSec,pclDate,ilActualPos,2,NOCONVERT);
	ilActualPos += 2;

	dbg(TRACE,"<%s><%s><%s><%s><%s><%s>",clYear,clMonth,clDay,clHour,clMin,clSec);
	ilYear = atoi(clYear);
	ilMonth = atoi(clMonth);
	ilDay = atoi(clDay);
	ilHour = atoi(clHour);
	ilMin = atoi(clMin);
	ilSec = atoi(clSec);

	if (ilYear < 1900 || ilYear > 9999)
	{
		dbg(TRACE,"CheckDateFormat: invalid year %d <%s>",ilYear,pcpDate);
		ilRc = RC_FAIL;
	}
	else
	{
		if (ilMonth < 1 || ilMonth > 12)
		{
			dbg(TRACE,"CheckDateFormat: invalid month <%s>",pcpDate);
			ilRc = RC_FAIL;
		}
		else
		{
			if (ilMonth == 2)
			{
				if ( ((ilYear % 4) == 0) && (((ilYear % 100) != 0) || (ilYear % 400) == 0)) 
				{
					/* is leap year */
					if (ilDay > 29 || ilDay < 1)
					{
						dbg(TRACE,"CheckDateFormat: invalid day <%s> in leap year",pcpDate);
						ilRc = RC_FAIL;
					}
				}
				else if (ilDay < 1 || ilDay > igDaysPerMonth[ilMonth-1])
				{
					dbg(TRACE,"CheckDateFormat: invalid day <%s>",pcpDate);
					ilRc = RC_FAIL;
				}
			}
			else
			{
				if (ilDay < 1 || ilDay > igDaysPerMonth[ilMonth-1])
				{
					dbg(TRACE,"CheckDateFormat: invalid day <%s>",pcpDate);
					ilRc = RC_FAIL;
				}
			}
			if (ilHour < 0 || ilHour > 24)
			{
				dbg(TRACE,"CheckDateFormat: invalid hour <%s>",pcpDate);
				ilRc = RC_FAIL;
			}
			if (ilMin < 0 || ilMin > 60)
			{
				dbg(TRACE,"CheckDateFormat: invalid minute <%s>",pcpDate);
				ilRc = RC_FAIL;
			}
			if (ilSec < 0 || ilSec > 60)
			{
				dbg(TRACE,"CheckDateFormat: invalid second <%s>",pcpDate);
				ilRc = RC_FAIL;
			}
		}
	}

	return ilRc;
}

static BOOL RelevantFields(char *pcpFieldList,char *pcpRelevantFields)
{
	char clActualField[8];
	int ilCntFields;
	int ilItemNo;
	int i;
	BOOL blRelevantFieldFound = FALSE;

	ilCntFields = field_count(pcpRelevantFields);
	
	dbg(TRACE,"RelevantFields <%s>",pcpRelevantFields);
	for (i = 1; i < ilCntFields; i++)
	{
		dbg(TRACE,"I=%d",i);
		get_real_item(clActualField, pcpRelevantFields, i);

		dbg(TRACE,"I=%d Field: <%s>",i,clActualField);
		ilItemNo = get_item_no(pcpFieldList,clActualField,5);

		if (ilItemNo >= 0)
		{
			blRelevantFieldFound = TRUE;
			break;
		}
	}

	if (blRelevantFieldFound)
	{
	dbg(DEBUG,"RelevantFields: Relevant field <%s> found. Returning TRUE.",
			clActualField);
	}
	else
	{
	dbg(DEBUG,"RelevantFields: No relevant fields found <%s> <%s>. Returning FALSE.",
				pcpFieldList,pcpRelevantFields);
	}
	return blRelevantFieldFound;
}

static BOOL RelevantChange(char *pcpData,char *pcpOldData,char *pcpFieldList,char *pcpRelevantFields)
{
	char clOldValue[20];
	char clNewValue[20];
	char clActualField[8];
	int ilCntFields;
	int ilItemNo;
	int i;

	ilCntFields = field_count(pcpRelevantFields);
	/*dbg(DEBUG,"RelevantChange: Old Data: <%s> new data <%s>",pcpOldData,pcpData);*/

	for (i = 1; i <= ilCntFields; i++)
	{
		get_real_item(clActualField, pcpRelevantFields,i);
		ilItemNo = get_item_no(pcpFieldList,clActualField,5) + 1;

		if (ilItemNo > 0)
		{
			get_real_item(clNewValue, pcpData, ilItemNo);
			get_real_item(clOldValue, pcpOldData, ilItemNo);

			if (strcmp(clNewValue,clOldValue) != NULL)
			{
				dbg(DEBUG,"RelevantChange: Item <%s> - old value <%s> and new value <%s> are different. Returning TRUE.",clActualField,clOldValue,clNewValue);
				return TRUE;
			}
			else
			{
				dbg(DEBUG,"RelevantChange: Item <%s> - old value <%s> and new value <%s> are identical. Checking next one.",clActualField,clOldValue,clNewValue);
			}
		}
		else
		{
			dbg(TRACE,"RelevantChange: Item <%s> not found in fieldlist <%s>",clActualField,pcpFieldList);
		}
	}

	dbg(DEBUG,"RelevantChange: No relevant changes detected. Returning FALSE.");
	return FALSE;
}

static int GetBreakTimeDiff (char *pcpLocal1,char *pcpLocal2, time_t *plpDiff)
{
	int ilRC = RC_SUCCESS;
	char clTime1[16], clTime2[16];
	struct tm	rlTimeStruct1, rlTimeStruct2;
	time_t llT1, llT2;

	*plpDiff = (time_t)0;
	if ( !pcpLocal1 || ( strlen ( pcpLocal1 ) != 14 ) )
		ilRC = RC_FAIL;
	if ( !pcpLocal2 || ( strlen ( pcpLocal2 ) != 14 ) )
		ilRC = RC_FAIL;
	if ( ilRC != RC_SUCCESS )
		dbg (DEBUG, "GetBreakTimeDiff: At least one parameter invalid");
	else
	{
		strcpy ( clTime1, pcpLocal1 );
		strcpy ( clTime2, pcpLocal2 );
		ilRC = CedaDateToTimeStruct(&rlTimeStruct1, clTime1 );
		ilRC |= CedaDateToTimeStruct(&rlTimeStruct2, clTime2 );
	}
	if ( ilRC != RC_SUCCESS )
	{
		dbg ( TRACE, "GetBreakTimeDiff: At least one call to CedaDateToTimeStruct failed" );
	}
	else
	{
		llT1 = mktime ( &rlTimeStruct1 );
		llT2 = mktime ( &rlTimeStruct2 );
		*plpDiff = llT2 - llT1;
		dbg ( TRACE, "GetBreakTimeDiff: <%s> - <%s> = %ld seconds", pcpLocal2, pcpLocal1, *plpDiff );
	}
	return ilRC;
}
static BOOL RelevantChangeJob(char *pcpData,char *pcpOldData,char *pcpFieldList)
{
	time_t llOldBreakDuration;
	time_t llNewBreakDuration;
	char clBreakFrom[20];
	char clBreakTo[20];
	int ilRc;

	/* get new break duration */
	ilRc = GetItem(clBreakFrom,pcpData,pcpFieldList,"ACFR",TRUE);
	if (ilRc < 0)
	{
		return FALSE;
	}

	ilRc = GetItem(clBreakTo,pcpData,pcpFieldList,"ACTO",TRUE);
	if (ilRc < 0)
	{
		return FALSE;
	}

	GetBreakTimeDiff(clBreakFrom,clBreakTo,&llNewBreakDuration);

	/* get old break duration */
	ilRc = GetItem(clBreakFrom,pcpOldData,pcpFieldList,"ACFR",TRUE);
	if (ilRc < 0)
	{
		return FALSE;
	}

	ilRc = GetItem(clBreakTo,pcpOldData,pcpFieldList,"ACTO",TRUE);
	if (ilRc < 0)
	{
		return FALSE;
	}
	
	GetBreakTimeDiff(clBreakFrom,clBreakTo,&llOldBreakDuration);

	/* check, if break duration changed */
	if (llNewBreakDuration != llOldBreakDuration)
	{
		dbg(DEBUG,"RelevantChangeJob: Break duration changed. Returning TRUE.");
		return TRUE;
	}
	else
	{
		dbg(DEBUG,"RelevantChangeJob: Break duration not changed. Returning FALSE.");
		return FALSE;
	}
}




static int GetStaffUrno(char *pclStaffUrno, char *pcpPeno)
{
	short slFkt = 0;
    short slCursor = 0;
	int ilRC = 0;
	char clSqlBuf[200] = "\0";


	slFkt = START;
	slCursor = 0;
	sprintf(clSqlBuf,"SELECT URNO FROM STFTAB WHERE PENO = '%s'",pcpPeno);
	ilRC = sql_if (slFkt, &slCursor, clSqlBuf, pclStaffUrno);
	if (ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"sql_if failed###################");
	}
	commit_work();
	close_my_cursor (&slCursor);
	return RC_SUCCESS;

}
