#ifndef _DEF_mks_version
#define _DEF_mks_version
#include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Std/STD_Server/Base/Server/Rms/jobhdl.c 1.2 2003/09/18 16:15:55SGT heb Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         : FSC / HEB / HAG                                           */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history :  16.8.2002: Merge SWR(Rev. 1.12),Beta4.5_2 (rev. 1,10),	  */
/*				HAJ44(Rev. 1.11, ADR44 (Rev. 1.15, except	  */
/*				modified communication jobhdl/OPSSPM)             */
/*                              RuntimeMode and StartupMode from config-file      */
/*                   20.8.2002: New communication between Opss_PM and jobhdl added from ADR-Project(HEB)*/
/*                   22.11.2002: Handling of new JobStatus "absent" merged from ADR*/
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */

static char sccs_version[] ="@(#) UFIS4.5 (c) ABB AAT/I jobhdl.c 4.5.1.9 / 2003/09/17 /HAG";

/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */

#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <time.h>
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "fditools.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "rmschkrul.h"
#include "syslib.h"
#include "AATArray.h"
#include "db_if.h"
#include "cedatime.h"

struct tm *localtime_r(const time_t *clock, struct tm *res);

#ifndef min
#define min(a,b) (a < b ? a : b)
#endif

#define CMD_SBC	    (1)
#define CMD_SSD     (2)
#define CMD_AOJ     (3)
#define CMD_IRT     (4)
#define CMD_URT     (5)
#define CMD_DRT     (6)
#define CMD_AFL     (7)
#define CMD_CJC     (8)
#define CMD_UFR     (9)
#define CMD_IFR     (10)

#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))
#define IsReallyOverlapped(start1, end1, start2, end2)	((start1) < (end2) && (start2) < (end1))
#define IsTotallyInside(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))
#define max(val1, val2)	((val1 > val2)?val1:val2)


#define JOBFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgJobArray.plrArrayFieldOfs[ipFieldNo-1]])
#define JODFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgJodArray.plrArrayFieldOfs[ipFieldNo-1]])
#define RUEFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgRueArray.plrArrayFieldOfs[ipFieldNo-1]])
#define RUDFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgRudArray.plrArrayFieldOfs[ipFieldNo-1]])
#define POOLJOBFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgPoolJobArray.plrArrayFieldOfs[ipFieldNo-1]])
#define OPENDEMFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgOpenDemArray.plrArrayFieldOfs[ipFieldNo-1]])
#define SPEFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgSpeArray.plrArrayFieldOfs[ipFieldNo-1]])
#define SPFFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgSpfArray.plrArrayFieldOfs[ipFieldNo-1]])
#define PFCFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgPfcArray.plrArrayFieldOfs[ipFieldNo-1]])
#define PERFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgPerArray.plrArrayFieldOfs[ipFieldNo-1]])
#define DELFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgDelArray.plrArrayFieldOfs[ipFieldNo-1]])
#define DRRFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgDrrArray.plrArrayFieldOfs[ipFieldNo-1]])
#define BSDFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgBsdArray.plrArrayFieldOfs[ipFieldNo-1]])
#define DRDFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgDrdArray.plrArrayFieldOfs[ipFieldNo-1]])
#define ALOFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgAloArray.plrArrayFieldOfs[ipFieldNo-1]])
#define JTYFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgJtyArray.plrArrayFieldOfs[ipFieldNo-1]])
#define TPLFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgTplArray.plrArrayFieldOfs[ipFieldNo-1]])
#define SERFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgSerArray.plrArrayFieldOfs[ipFieldNo-1]])
#define RPQFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgRpqArray.plrArrayFieldOfs[ipFieldNo-1]])
#define RPFFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgRpfArray.plrArrayFieldOfs[ipFieldNo-1]])
#define WAYFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgWayArray.plrArrayFieldOfs[ipFieldNo-1]])
#define POLFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgPolArray.plrArrayFieldOfs[ipFieldNo-1]])
#define SGMFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgSgmArray.plrArrayFieldOfs[ipFieldNo-1]])
#define SGRFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgSgrArray.plrArrayFieldOfs[ipFieldNo-1]])
#define AFTFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgAftArray.plrArrayFieldOfs[ipFieldNo-1]])
#define PARFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgParArray.plrArrayFieldOfs[ipFieldNo-1]])
#define SWGFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgSwgArray.plrArrayFieldOfs[ipFieldNo-1]])
#define WGRFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgWgrArray.plrArrayFieldOfs[ipFieldNo-1]])
#define DRGFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgDrgArray.plrArrayFieldOfs[ipFieldNo-1]])
#define DLGFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgDlgArray.plrArrayFieldOfs[ipFieldNo-1]])
#define DEMPFCFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgDemPfcArray.plrArrayFieldOfs[ipFieldNo-1]])
#define DEMPRQFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgDemPrqArray.plrArrayFieldOfs[ipFieldNo-1]])
#define DEMPOOLFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgDemPoolArray.plrArrayFieldOfs[ipFieldNo-1]])
#define POOLPERFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgPoolPerArray.plrArrayFieldOfs[ipFieldNo-1]])
#define POOLFCTFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgPoolFctArray.plrArrayFieldOfs[ipFieldNo-1]])
#define DEMIDXFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgDemIdxArray.plrArrayFieldOfs[ipFieldNo-1]])
#define TMPURNOFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgTmpUrnoArray.plrArrayFieldOfs[ipFieldNo-1]])
#define URNOURNOFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgUrnoUrnoArray.plrArrayFieldOfs[ipFieldNo-1]])
#define JOBTYPEFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgJobTypeArray.plrArrayFieldOfs[ipFieldNo-1]])
#define KEYURNOLISTFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgKeyUrnoListArray.plrArrayFieldOfs[ipFieldNo-1]])
#define FIELDSFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgFieldsArray.plrArrayFieldOfs[ipFieldNo-1]])
#define SWGINDEXFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgSwgIndexArray.plrArrayFieldOfs[ipFieldNo-1]])
#define MATCHFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgMatchArray.plrArrayFieldOfs[ipFieldNo-1]])
#define GROUPLISTFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgGroupListArray.plrArrayFieldOfs[ipFieldNo-1]])
#define GROUPNAMEFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgGroupNameArray.plrArrayFieldOfs[ipFieldNo-1]])
#define ALIDLISTFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgAlidListArray.plrArrayFieldOfs[ipFieldNo-1]])
#define URNOFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgUrnoArray.plrArrayFieldOfs[ipFieldNo-1]])

extern int get_no_of_items(char *s);

#define ARR_NAME_LEN           (28)
#define ARR_FLDLST_LEN         (512)
#define	URNOLEN					10
#define URNOS_TO_FETCH          50
#define MAXINSEL				500		/* maximum number of Job-URNOs in one Broadcast */

static char *prgCmdTxt[] = {     "SSD",    "AOJ",    "IRT",    "URT",    "DRT",    "AFL",    "CJC", "UFR",   "SBC",   "IFR",   NULL };
static int    rgCmdDef[] = {  CMD_SSD , CMD_AOJ , CMD_IRT , CMD_URT , CMD_DRT , CMD_AFL , CMD_CJC , CMD_UFR, CMD_SBC, CMD_IFR, 0 };


/******************************************************************************/
/***   Fields                                                             *****/
/******************************************************************************/

#define DEM_FIELDS "HOPO,ALID,ALOC,DEBE,DEDU,DEEN,DETY,EADB,FLGS,LADE,OBTY,OURI,OURO,RETY,TTGF,TTGT,UDGR,URNO,URUD"
#define DEM_FIELDS2 "HOPO,ALID,ALOC,DEBE,DEDU,DEEN,DETY,EADB,FLGS,LADE,OBTY,OURI,OURO,RETY,TTGF,TTGT,UDGR,URNO,URUD,MAXD,MIND,DIDE,TNAM,SECO,SIX1,SIX2,TCHD,RTWF,RTWT,UALO,ULNK,UPDE,UNDE,URUE,UTPL"
#define OPENDEM_SORTFIELDS "URNO"
#define OPENDEM_SORTDIR "D"
#define OPENDEM_SORTFIELDS2 "OURI,OURO,TNAM,SECO,DEBE,DEDU"
#define OPENDEM_SORTDIR2 "D,D,D,D,D,D"
#define OPENDEM_SORTFIELDS3 "TCHD,SIX1,SIX2,DEBE,DEDU"
#define OPENDEM_SORTDIR3 "D,D,D,D,D"
#define OPENDEM_SORTFIELDS4 "URUE,OURI,OURO,ULNK"
#define OPENDEM_SORTDIR4 "D,D,D,D"
#define OPENDEM_SORTFIELDS5 "URUE,OURI,OURO,URUD"
#define OPENDEM_SORTDIR5 "D,D,D,D"
#define OPENDEM_SORTFIELDS6 "DEBE,DEDU"
#define OPENDEM_SORTDIR6 "A,A"
#define OPENDEM_SORTFIELDS7 "DEEN,DEDU"
#define OPENDEM_SORTDIR7 "D,D"
#define OPENDEM_ADDFIELDS "MAXD,MIND,DIDE,TNAM,SECO,SIX1,SIX2,TCHD,RTWF,RTWT,UALO,ULNK,UPDE,UNDE,URUE,UTPL"

#define JOB_SORTFIELDS "JOUR,ACFR,ACTO"
#define JOB_SORTDIR "D,A,A"
#define JOB_SORTFIELDS2 "URNO,ACFR,ACTO"
#define JOB_SORTDIR2 "D,D,D"
#define JOB_SORTFIELDS3 "TXXD,ACFR,ACTO"
#define JOB_SORTDIR3 "D,A,A"
#define JOB_SORTFIELDS4 "TXXD,ACFR,ACTO"
#define JOB_SORTDIR4 "D,D,D"
#define JOB_SORTFIELDS5 "JOUR,ACFR,ACTO"
#define JOB_SORTDIR5 "D,D,D"
#define JOB_ADDFIELDS "ALID,ALOC,TXXD"

#define POOLJOB_SORTFIELDS "URNO"
#define POOLJOB_SORTDIR "D"
#define POOLJOB_SORTFIELDS2 "UAID,PJVD,ACFR,ACTO"
#define POOLJOB_SORTDIR2 "D,A,A,D"
/* #define POOLJOB_SORTFIELDS3 "TCHX"		hag 27.04.2003: not used
#define POOLJOB_SORTDIR3 "D" */
#define POOLJOB_SORTFIELDS4 "ISOK,PRIO"
#define POOLJOB_SORTDIR4 "D,D"
/* #define POOLJOB_SORTFIELDS5 "USTF,SDAY"		hag 27.04.2003: not used
#define POOLJOB_SORTDIR5 "D,D" */
#define POOLJOB_SORTFIELDS6 "WGPC,ISOK"
#define POOLJOB_SORTDIR6 "D,D"

#define POOLJOB_ADDFIELDS "ISOK,TCHX,PJVD,PRIO,WKLD,MIN2,MINT,WAYF,WAYT,UPRJ,UNXJ,SDAY,WGPC"
/*#define POOLJOB_ADDFIELDS "ISOK,TCHX,PJVD,PRIO,WKLD,MIN2,MINT,WAYF,WAYT,UPRJ,UNXJ,NJBN,NJED,SDAY,WGPC"*/

#define JOB_FIELDS "ACFR,ACT3,ACTO,CDAT,DETY,GATE,HOPO,JOUR,LSTU,PLFR,PLTO,POSI,REGN,STAT,TEXT,UAFT,UAID,UALO,UDEL,UDRD,UDSR,UJTY,URNO,USEC,USEU,USTF,TTGF,TTGT"
/*#define JOB_FIELDS2 "ACFR,ACT3,ACTO,CDAT,DETY,GATE,HOPO,JOUR,LSTU,PLFR,PLTO,POSI,REGN,STAT,TEXT,UAFT,UAID,UALO,UDEL,UDRD,UDSR,UJTY,URNO,USEC,USEU,USTF,TTGF,TTGT,ALID,ALOC,TXXD"*/
#define POOLJOB_FIELDS2 "ACFR,ACT3,ACTO,CDAT,DETY,GATE,HOPO,JOUR,LSTU,PLFR,PLTO,POSI,REGN,STAT,TEXT,UAFT,UAID,UALO,UDEL,UDRD,UDSR,UJTY,URNO,USEC,USEU,USTF,TTGF,TTGT,ISOK,TCHX,PJVD,PRIO,WKLD,MIN2,MINT,WAYF,WAYT,UPRJ,UNXJ,SDAY,WGPC"
/*#define POOLJOB_FIELDS2 "ACFR,ACT3,ACTO,CDAT,DETY,GATE,HOPO,JOUR,LSTU,PLFR,PLTO,POSI,REGN,STAT,TEXT,UAFT,UAID,UALO,UDEL,UDRD,UDSR,UJTY,URNO,USEC,USEU,USTF,TTGF,TTGT,ISOK,TCHX,PJVD,PRIO,WKLD,MIN2,MINT,WAYF,WAYT,UPRJ,UNXJ,NJBN,NJED,SDAY,WGPC"*/

#define JOD_SORTFIELDS "UDEM"
#define JOD_SORTDIR "D"
#define JOD_SORTFIELDS2 "UJOB"
#define JOD_SORTDIR2 "D"
#define JOD_SORTFIELDS3 "TCHX"
#define JOD_SORTDIR3 "D"
#define JOD_FIELDS "CDAT,HOPO,LSTU,UDEM,UJOB,URNO,USEC,USEU"
#define JOD_FIELDS2 "CDAT,HOPO,LSTU,UDEM,UJOB,URNO,USEC,USEU,TCHX"
#define JOD_ADDFIELDS "TCHX"

#define RUD_SORTFIELDS "URNO"
#define RUD_SORTDIR "D"
#define RUD_FIELDS "URUE,URNO,UGHS,UDGR,FOND,HOPO,ULNK,UPDE,UNDE,DIDE,MAXD,MIND,REDE"

#define RUE_SORTFIELDS "URNO"
#define RUE_SORTDIR "D"
#define RUE_FIELDS "UTPL,URNO,HOPO"

#define ALO_SORTFIELDS "URNO"
#define ALO_SORTDIR "D"
#define ALO_SORTFIELDS2 "ALOC"
#define ALO_SORTDIR2 "D"
#define ALO_FIELDS "REFT,URNO,ALOT,ALOC,HOPO"

#define JTY_SORTFIELDS "NAME"
#define JTY_SORTDIR "D"
#define JTY_FIELDS "NAME,URNO,HOPO"

#define SPE_SORTFIELDS "SURN"
#define SPE_SORTDIR "D"
#define SPE_FIELDS "SURN,URNO,CODE,VPFR,VPTO,HOPO"

#define SPF_SORTFIELDS "SURN,PRIO"
#define SPF_SORTDIR "D,A"
#define SPF_FIELDS "SURN,URNO,CODE,VPFR,VPTO,PRIO,HOPO"

#define PFC_SORTFIELDS "FCTC"
#define PFC_SORTDIR "D"
#define PFC_SORTFIELDS2 "URNO"
#define PFC_SORTDIR2 "D"
#define PFC_FIELDS "FCTC,URNO,PRIO,HOPO"

#define RPF_SORTFIELDS "URUD"
#define RPF_SORTDIR "D"
#define RPF_FIELDS "URUD,URNO,FCCO,UPFC,HOPO"

#define PER_SORTFIELDS "PRMC"
#define PER_SORTDIR "D"
#define PER_FIELDS "PRMC,URNO,URNO,PRIO,HOPO"

#define RPQ_SORTFIELDS "URUD"
#define RPQ_SORTDIR "D"
#define RPQ_FIELDS "URUD,URNO,QUCO,UPER,HOPO"

#define DEL_SORTFIELDS "URNO"
#define DEL_SORTDIR "D"
#define DEL_FIELDS "BSDU,URNO,FCTC,HOPO"

#define DRR_SORTFIELDS "URNO"
#define DRR_SORTDIR "D"
#define DRR_FIELDS "BSDU,URNO,DRSF,DRRN,SDAY,STFU,SBFR,SBTO,SBLU,FCTC,HOPO"

#define DRD_SORTFIELDS "STFU,SDAY,DRRN"
#define DRD_SORTDIR "D,D,D"
#define DRD_FIELDS "PRMC,URNO,FCTC,DRRN,SDAY,STFU,HOPO"

#define BSD_SORTFIELDS "URNO"
#define BSD_SORTDIR "D"
#define BSD_FIELDS "FCTC,URNO,HOPO"

#define SER_SORTFIELDS "URNO"
#define SER_SORTDIR "D"
#define SER_FIELDS "SECO,URNO,RTWF,RTWT,HOPO"

#define TPL_SORTFIELDS "URNO"
#define TPL_SORTDIR "D"
#define TPL_FIELDS "TNAM,URNO,HOPO"

#define WAY_SORTFIELDS "POBE,POEN,WTYP"
#define WAY_SORTDIR "D,D,D"
#define WAY_FIELDS "WTYP,URNO,TTGO,POBE,POEN,HOPO"

#define POL_SORTFIELDS "NAME"
#define POL_SORTDIR "D"
#define POL_SORTFIELDS2 "URNO"
#define POL_SORTDIR2 "D"
#define POL_FIELDS "NAME,URNO,HOPO"

#define SGM_SORTFIELDS "UVAL"
#define SGM_SORTDIR "D"
#define SGM_SORTFIELDS2 "USGR"
#define SGM_SORTDIR2 "D"
#define SGM_FIELDS "UVAL,URNO,TABN,USGR,HOPO"

#define SGR_SORTFIELDS "URNO"
#define SGR_SORTDIR "D"
#define SGR_SORTFIELDS2 "GRPN"
#define SGR_SORTDIR2 "D"
#define SGR_FIELDS "TABN,URNO,UGTY,GRPN,HOPO"

#define AFT_SORTFIELDS "URNO"
#define AFT_SORTDIR "D"
#define AFT_FIELDS "ACT3,URNO,REGN,PSTA,PSTD,GTA1,GTA2,GTD1,GTD2,ADID,HOPO"

#define PAR_SORTFIELDS "PAID,NAME,VALU"
#define PAR_SORTDIR "D,D,D"
#define PAR_FIELDS "APPL,NAME,URNO,PAID,HOPO,VALU"

#define SWG_SORTFIELDS "SURN"
#define SWG_SORTDIR "D"
/*#define SWG_SORTFIELDS2 "CODE"	hag 27.04.2003: not used
#define SWG_SORTDIR2 "D" */
#define SWG_FIELDS "CODE,HOPO,URNO,SURN,VPFR,VPTO"

#define DRG_SORTFIELDS "STFU,SDAY,DRRN"
#define DRG_SORTDIR "D,D,D"
#define DRG_FIELDS "SDAY,FCTC,URNO,STFU,WGPC,DRRN"

#define DLG_SORTFIELDS "UJOB"
#define DLG_SORTDIR "D"
#define DLG_FIELDS "SDAY,FCTC,URNO,WGPC,UJOB"

#define WGR_SORTFIELDS "ALID"
#define WGR_SORTDIR "D"
#define WGR_FIELDS "ALID,HOPO,URNO,ALGR,ALOC"

#define DEMPFC_SORTFIELDS "UDEM,STAT,FCOD,UPJB"
#define DEMPFC_SORTDIR "D,D,D,D"
#define DEMPFC_FIELDS "FCOD,UDEM,STAT,UPJB"

#define DEMPRQ_SORTFIELDS "UDEM,STAT,QCOD,UPJB"
#define DEMPRQ_SORTDIR "D,D,D,D"
#define DEMPRQ_FIELDS "UDEM,QCOD,STAT,UPJB"

#define DEMPOOL_SORTFIELDS "ALID,ALOC,UPOL"
#define DEMPOOL_SORTDIR "D,D,D"
#define DEMPOOL_FIELDS "ALID,ALOC,UPOL,UAID"

#define DEMIDX_SORTFIELDS "OURI,OURO,TPLN,SERN"
#define DEMIDX_SORTDIR "D,D,D,D"
#define DEMIDX_FIELDS "OURI,OURO,TPLN,SERN,KCNT"

#define POOLFCT_SORTFIELDS "UJOB,FCTC,PRIO"
#define POOLFCT_SORTDIR "D,D,D"
#define POOLFCT_FIELDS "UJOB,FCTC,PRIO"

#define POOLPER_SORTFIELDS "UJOB,PERM,PRIO"
#define POOLPER_SORTDIR "D,D,D"
#define POOLPER_FIELDS "UJOB,PERM,PRIO"

#define TMPURNO_SORTFIELDS "STAT,AIDX,URNO"
#define TMPURNO_SORTDIR "D,D,D"
#define TMPURNO_FIELDS "STAT,URNO,AIDX"

#define URNOURNO_SORTFIELDS "URN1"
#define URNOURNO_SORTDIR "D"
#define URNOURNO_SORTFIELDS2 "URN2"
#define URNOURNO_SORTDIR2 "D"
#define URNOURNO_FIELDS "STAT,URN1,URN2"

#define JOBTYPE_SORTFIELDS "ALOC,DETY"
#define JOBTYPE_SORTDIR "D,D"
#define JOBTYPE_FIELDS "ALOC,DETY,JTYP"

#define KEYURNOLIST_SORTFIELDS "UDEM,XKEY,URNO"
#define KEYURNOLIST_SORTDIR "D,D,D"
/* #define KEYURNOLIST_SORTFIELDS2 "XKEY"	hag 27.04.2003: not used
#define KEYURNOLIST_SORTDIR2 "A" */
#define KEYURNOLIST_SORTFIELDS3 "XKEY"
#define KEYURNOLIST_SORTDIR3 "D"
#define KEYURNOLIST_FIELDS "XKEY,URNO,BEGI,ENDE,UDEM"

/*#define FIELDS_SORTFIELDS "FNAM"	hag 27.04.2003: not used		
#define FIELDS_SORTDIR "D" */
#define FIELDS_FIELDS "FNAM"

/*#define SWGINDEX_SORTFIELDS "CODE"	hag 27.04.2003: not used		
#define SWGINDEX_SORTDIR "D"*/
#define SWGINDEX_SORTFIELDS2 "SIZE"
#define SWGINDEX_SORTDIR2 "D"
#define SWGINDEX_FIELDS "CODE,SIZE"

#define MATCH_SORTFIELDS "STAT,UDEM"
#define MATCH_SORTDIR "D,D"
#define MATCH_SORTFIELDS2 "STAT,UPJB"
#define MATCH_SORTDIR2 "D,D"
#define MATCH_SORTFIELDS3 "MARK"
#define MATCH_SORTDIR3 "D"
#define MATCH_SORTFIELDS4 "PRIO"
#define MATCH_SORTDIR4 "A"
#define MATCH_FIELDS "UPJB,UDEM,URUD,STAT,TSTA,MARK,DEBE,DEEN,DPRI,JPRI,PRIO"

#define GROUPLIST_SORTFIELDS "GRPN"
#define GROUPLIST_SORTDIR "D"
#define GROUPLIST_SORTFIELDS2 "UPJB"
#define GROUPLIST_SORTDIR2 "D"
#define GROUPLIST_FIELDS "GRPN,UPJB,UDEM,VALU,DEBE,DEEN"

#define GROUPNAME_SORTFIELDS "WGPC"
#define GROUPNAME_SORTDIR "D"
#define GROUPNAME_FIELDS "WGPC"

#define ALIDLIST_SORTFIELDS "ALOC,ALID"
#define ALIDLIST_SORTDIR "D,D"
#define ALIDLIST_SORTFIELDS2 "UAID"
#define ALIDLIST_SORTDIR2 "D"
#define ALIDLIST_SORTFIELDS3 "UALO,ALID"
#define ALIDLIST_SORTDIR3 "D,D"
#define ALIDLIST_FIELDS "ALID,ALOC,UAID,UALO"

#define URNO_FIELDS "URNO"
#define URNO_SORTDIR "D"
#define URNO_SORTFIELDS "URNO"


/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp       = NULL;*/
int  debug_level = 0;

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static int   igInitOK      = FALSE;
static char  cgConfigFile[512];
static EVENT  *prgOutEvent      = NULL;
struct _arrayinfo
{
    HANDLE   rrArrayHandle;
    char     crArrayName[ARR_NAME_LEN+1];
    char     crArrayFieldList[ARR_FLDLST_LEN+1];
    long     lrArrayFieldCnt;
    char    *pcrArrayRowBuf;
    long     lrArrayRowLen;
    long    *plrArrayFieldOfs;
    long    *plrArrayFieldLen;
    HANDLE   rrIdx01Handle;
    char     crIdx01Name[ARR_NAME_LEN+1];
    char     crIdx01FieldList[ARR_FLDLST_LEN+1];
    long     lrIdx01FieldCnt;
    char    *pcrIdx01RowBuf;
    long     lrIdx01RowLen;
    long    *plrIdx01FieldPos;
    long    *plrIdx01FieldOrd;
    HANDLE   rrIdx02Handle;
    char     crIdx02Name[ARR_NAME_LEN+1];
    char     crIdx02FieldList[ARR_FLDLST_LEN+1];
    long     lrIdx02FieldCnt;
    char    *pcrIdx02RowBuf;
    long     lrIdx02RowLen;
    long    *plrIdx02FieldPos;
    long    *plrIdx02FieldOrd;
    HANDLE   rrIdx03Handle;
    char     crIdx03Name[ARR_NAME_LEN+1];
    char     crIdx03FieldList[ARR_FLDLST_LEN+1];
    long     lrIdx03FieldCnt;
    char    *pcrIdx03RowBuf;
    long     lrIdx03RowLen;
    long    *plrIdx03FieldPos;
    long    *plrIdx03FieldOrd;
    HANDLE   rrIdx04Handle;
    char     crIdx04Name[ARR_NAME_LEN+1];
    char     crIdx04FieldList[ARR_FLDLST_LEN+1];
    long     lrIdx04FieldCnt;
    char    *pcrIdx04RowBuf;
    long     lrIdx04RowLen;
    long    *plrIdx04FieldPos;
    long    *plrIdx04FieldOrd;
    HANDLE   rrIdx05Handle;
    char     crIdx05Name[ARR_NAME_LEN+1];
    char     crIdx05FieldList[ARR_FLDLST_LEN+1];
    long     lrIdx05FieldCnt;
    char    *pcrIdx05RowBuf;
    long     lrIdx05RowLen;
    long    *plrIdx05FieldPos;
    long    *plrIdx05FieldOrd;
    HANDLE   rrIdx06Handle;
    char     crIdx06Name[ARR_NAME_LEN+1];
    char     crIdx06FieldList[ARR_FLDLST_LEN+1];
    long     lrIdx06FieldCnt;
    char    *pcrIdx06RowBuf;
    long     lrIdx06RowLen;
    long    *plrIdx06FieldPos;
    long    *plrIdx06FieldOrd;
    HANDLE   rrIdx07Handle;
    char     crIdx07Name[ARR_NAME_LEN+1];
    char     crIdx07FieldList[ARR_FLDLST_LEN+1];
    long     lrIdx07FieldCnt;
    char    *pcrIdx07RowBuf;
    long     lrIdx07RowLen;
    long    *plrIdx07FieldPos;
    long    *plrIdx07FieldOrd;
    HANDLE   rrIdx08Handle;
    char     crIdx08Name[ARR_NAME_LEN+1];
    char     crIdx08FieldList[ARR_FLDLST_LEN+1];
    long     lrIdx08FieldCnt;
    char    *pcrIdx08RowBuf;
    long     lrIdx08RowLen;
    long    *plrIdx08FieldPos;
    long    *plrIdx08FieldOrd;
};
typedef struct _arrayinfo ARRAYINFO;

static ARRAYINFO rgJobArray;
static ARRAYINFO rgJodArray;
static ARRAYINFO rgRudArray;
static ARRAYINFO rgRueArray;
static ARRAYINFO rgPoolJobArray;
static ARRAYINFO rgRequestPoolJobArray;
static ARRAYINFO rgOpenDemArray;
static ARRAYINFO rgRequestOpenDemArray;
static ARRAYINFO rgSpeArray;
static ARRAYINFO rgSpfArray;
static ARRAYINFO rgPfcArray;
static ARRAYINFO rgPerArray;
static ARRAYINFO rgJtyArray;
static ARRAYINFO rgDelArray;
static ARRAYINFO rgDrrArray;
static ARRAYINFO rgBsdArray;
static ARRAYINFO rgDrdArray;
static ARRAYINFO rgAloArray;
static ARRAYINFO rgAftArray;
static ARRAYINFO rgTplArray;
static ARRAYINFO rgSerArray;
static ARRAYINFO rgRpqArray;
static ARRAYINFO rgRpfArray;
static ARRAYINFO rgWayArray;
static ARRAYINFO rgPolArray;
static ARRAYINFO rgSgmArray;
static ARRAYINFO rgSgrArray;
static ARRAYINFO rgParArray;
static ARRAYINFO rgSwgArray;

static ARRAYINFO rgWgrArray;
static ARRAYINFO rgDrgArray;
static ARRAYINFO rgDlgArray;

static ARRAYINFO rgDemPfcArray;
static ARRAYINFO rgDemPrqArray;
static ARRAYINFO rgDemPoolArray;

static ARRAYINFO rgDemIdxArray;

static ARRAYINFO rgPoolPerArray;
static ARRAYINFO rgPoolFctArray;

static ARRAYINFO rgTmpUrnoArray;
static ARRAYINFO rgUrnoUrnoArray;

static ARRAYINFO rgJobTypeArray;

static ARRAYINFO rgKeyUrnoListArray;

static ARRAYINFO rgFieldsArray;
		
static ARRAYINFO rgSwgIndexArray;

static ARRAYINFO rgMatchArray;
static ARRAYINFO rgGroupListArray;
static ARRAYINFO rgGroupNameArray;

static ARRAYINFO rgAlidListArray;
static ARRAYINFO rgUrnoArray;
static ARRAYINFO rgNewDrrArray;

static long  lgEvtCnt      = 0;

static int igDemAlid;
static int igDemAloc;
static int igDemDebe;
static int igDemDedu;
static int igDemDeen;
static int igDemDety;
static int igDemEadb;
static int igDemFlgs;
static int igDemLade;
static int igDemObty;
static int igDemOuri;
static int igDemOuro;
static int igDemRety;
static int igDemTtgf;
static int igDemTtgt;
static int igDemUdgr;
static int igDemUrno;
static int igDemUrud;
static int igDemMaxd;
static int igDemMind;
static int igDemDide;

static int igDemTpln;
static int igDemSeco;
static int igDemSix1;
static int igDemSix2;
static int igDemTchd;
static int igDemRtwf;
static int igDemRtwt;
static int igDemUalo;
static int igDemUlnk;
static int igDemUpde;
static int igDemUnde;
static int igDemUrue;
static int igDemUtpl;

static int igJobAcfr;
static int igJobAct3;
static int igJobActo;
static int igJobCdat;
static int igJobDety;
static int igJobGate;
static int igJobHopo;
static int igJobJour;
static int igJobLstu;
static int igJobPlfr;
static int igJobPlto;
static int igJobPosi;
static int igJobRegn;
static int igJobStat;
static int igJobText;
static int igJobUaft;
static int igJobUaid;
static int igJobUalo;
static int igJobUdel;
static int igJobUdsr;
static int igJobUdrd;
static int igJobUjty;
static int igJobUrno;
static int igJobUsec;
static int igJobUseu;
static int igJobUstf;
static int igJobTtgf;
static int igJobTtgt;
static int igJobUtpl=-1;
static int igJobAlid;
static int igJobAloc;
static int igJobTxxd;

static int igPoolJobIsok;
static int igPoolJobTchd;
static int igPoolJobPjvd;
static int igPoolJobPrio;
static int igPoolJobWkld;
static int igPoolJobMin2;
static int igPoolJobMint;
static int igPoolJobWayf;
static int igPoolJobWayt;
static int igPoolJobUnxj;
static int igPoolJobUprj;
/*static int igPoolJobNjbn;
static int igPoolJobNjed;*/
static int igPoolJobSday;
static int igPoolJobWgpc;

static int igJodCdat;
static int igJodHopo;
static int igJodLstu;
static int igJodUdem;
static int igJodUjob;
static int igJodUrno;
static int igJodUsec;
static int igJodUseu;
static int igJodTchx;

static int igRueUrno;
static int igRueUtpl;

static int igRudUrno;
static int igRudUdgr;
static int igRudUrue;
static int igRudUghs;
static int igRudFond;
static int igRudUlnk;
static int igRudUpde;
static int igRudUnde;
static int igRudMaxd;
static int igRudMind;
static int igRudDide;
static int igRudRede;

static int igSerUrno;
static int igSerSeco;
static int igSerRtwf;
static int igSerRtwt;

static int igAloUrno;
static int igAloReft;
static int igAloAlot;
static int igAloAloc;
	
static int igTplUrno;
static int igTplTnam;

static int igJtyUrno;
static int igJtyName;

static int igSpeUrno;
static int igSpeSurn;
static int igSpeCode;
static int igSpeVpfr;
static int igSpeVpto;
static int igSpePrio;

static int igSpfUrno;
static int igSpfSurn;
static int igSpfCode;
static int igSpfVpfr;
static int igSpfVpto;
static int igSpfPrio;

static int igPfcUrno;
static int igPfcFctc;
static int igPfcPrio;

static int igRpfUrud;
static int igRpfFcco;
static int igRpfUpfc;

static int igPerPrmc;
static int igPerUrno;
static int igPerPrio;

static int igRpqUrud;
static int igRpqQuco;
static int igRpqUper;

static int igDelBsdu;
static int igDelUrno;
static int igDelFctc;

static int igDrrBsdu;
static int igDrrDrsf;
static int igDrrUrno;
static int igDrrDrrn;
static int igDrrSday;
static int igDrrStfu;
static int igDrrSblu;
static int igDrrSbfr;
static int igDrrSbto;
static int igDrrFctc;

static int igDrdUrno;
static int igDrdFctc;
static int igDrdPrmc;
static int igDrdDrrn;
static int igDrdSday;
static int igDrdStfu;

static int igDrgUrno;
static int igDrgFctc;
static int igDrgDrrn;
static int igDrgSday;
static int igDrgStfu;
static int igDrgWgpc;

static int igDlgUrno;
static int igDlgFctc;
static int igDlgSday;
static int igDlgUjob;
static int igDlgWgpc;

static int igBsdUrno;
static int igBsdFctc;

static int igWayWtyp;
static int igWayTtgo;
static int igWayPobe;
static int igWayPoen;

static int igPolUrno;
static int igPolName;

static int igSgmUval;
static int igSgmTabn;
static int igSgmUsgr;

static int igDemPfcUdem;
static int igDemPfcFcod;
static int igDemPfcStat;
static int igDemPfcUpjb;

static int igDemPrqUdem;
static int igDemPrqQcod;
static int igDemPrqStat;
static int igDemPrqUpjb;

static int igDemPoolAloc;
static int igDemPoolAlid;
static int igDemPoolUpol;
static int igDemPoolUaid;

static int igDemIdxOuri;
static int igDemIdxOuro;
static int igDemIdxTpln;
static int igDemIdxSern;
static int igDemIdxKcnt;

static int igPoolFctUjob;
static int igPoolFctFctc;
static int igPoolFctPrio;

static int igPoolPerUjob;
static int igPoolPerPerm;
static int igPoolPerPrio;

static int igSgrUrno;
static int igSgrTabn;
static int igSgrUgty;
static int igSgrGrpn;

static int igAftUrno;
static int igAftAct3;
static int igAftRegn;
static int igAftPstd;
static int igAftPsta;
static int igAftGta1;
static int igAftGtd1;
static int igAftGta2;
static int igAftGtd2;
static int igAftAdid;

static int igParAppl;
static int igParUrno;
static int igParPaid;
static int igParName;
static int igParHopo;
static int igParValu;

static int igSwgCode;
static int igSwgHopo;
static int igSwgUrno;
static int igSwgSurn;
static int igSwgVpfr;
static int igSwgVpto;

static int igWgrUrno;
static int igWgrHopo;
static int igWgrAlid;
static int igWgrAloc;
static int igWgrAlgr;

static int igDgrWgpc;
static int igDgrSday;
static int igDgrUrno;
static int igDgrStfu;
static int igDgrFctc;

static int igTmpUrnoStat;
static int igTmpUrnoAidx;
static int igTmpUrnoUrno;
  
static int igUrnoUrnoStat;
static int igUrnoUrnoUrn1;
static int igUrnoUrnoUrn2;
 
static int igJobTypeDety;
static int igJobTypeAloc;
static int igJobTypeJtyp;
 
static int igKeyUrnoListXKey;
static int igKeyUrnoListUrno;
static int igKeyUrnoListBegi;
static int igKeyUrnoListEnde;
static int igKeyUrnoListUdem;

static int igFieldsFnam;

static int igSwgIndexCode;
static int igSwgIndexSize;

static int igMatchUpjb;
static int igMatchUrud;
static int igMatchUdem;
static int igMatchStat;
static int igMatchTsta;
static int igMatchMark;
static int igMatchDebe;
static int igMatchDeen;
static int igMatchPrio;
static int igMatchDpri;
static int igMatchJpri;

static int igGroupListGrpn;
static int igGroupListUpjb;
static int igGroupListUdem;
static int igGroupListValu;
static int igGroupListDebe;
static int igGroupListDeen;

static int igGroupNameWgpc;

static int igAlidListAlid;
static int igAlidListAloc;
static int igAlidListUaid;
static int igAlidListUalo;

static int igUrnoUrno;

static int   igRouter = 1200;

static char * pcgFieldList = NULL;
static char   *pcgUrnoListBuf  = NULL ;  

static char  cgTabEnd[8];                       /* default table extension */

static char cgJobTab[14];
static char cgJodTab[14];
static char cgDemTab[14];
static char cgRudTab[14];
static char cgRueTab[14];
static char cgSpeTab[14];
static char cgSpfTab[14];
static char cgPfcTab[14];
static char cgPerTab[14];
static char cgJtyTab[14];
static char cgDelTab[14];
static char cgDrrTab[14];
static char cgBsdTab[14];
static char cgDrdTab[14];
static char cgAloTab[14];
static char cgAftTab[14];
static char cgTplTab[14];
static char cgSerTab[14];
static char cgRpqTab[14];
static char cgRpfTab[14];
static char cgWayTab[14];
static char cgPolTab[14];
static char cgSgmTab[14];
static char cgSgrTab[14];
static char cgSwgTab[14];
static char cgDrgTab[14];
static char cgDlgTab[14];
static char cgParTab[14];
static char cgWgrTab[14];

static char cgNewJobBuf[1024];
static char cgNewJodBuf[512];
static char cgTmpUrnoBuf[100];
static char cgUrnoUrnoBuf[100];
static char cgMatchBuf[100];
static char cgGroupListBuf[100];
static char cgGroupNameBuf[20];
static char cgUrnoUrnoBuf[100];
static char cgAlidListBuf[100];
static char cgIdxBuff[512];
static char cgTmpPoolRow[100];
static char cgTmpPfcRow[100];
static char cgPoolPerBuf[300];
static char cgPoolFctBuf[300];
static char cgJobTypeBuf[200];

static char cgKeyUrnoListBuf[200];

static char cgSwgIndexBuf[100];

static char cgFieldsBuf[20];

static char cgJobTypes[512];

static char cgNewDistFields[1024];

static char cgOpenDemSortFields2[512];
static char cgOpenDemSortDir2[100];
static char cgOpenDemSortFields3[512];
static char cgOpenDemSortDir3[100];
static char cgJobFields[200];

static long    lgUrnoListLen   = 0 ;

static char cgHopo[8]; /* default home airport    */
static char cgTdi1[24]; /* UTC-Loc7al Difference */
static char cgTdi2[24]; /* UTC-Local Difference */
static char cgTich[24]; /* UTC-Local Difference */
static BOOL bgAutomaticPauseAllocation;

static BOOL bgCheckOverlap;
static BOOL bgKlebeFunction;

static BOOL bgCheckAlocation;

static BOOL bgAllowEmptyAlid = FALSE;
static BOOL bgIgnoreBreaks = FALSE;
static BOOL bgDefaultIgnoreBreaks = FALSE;

static BOOL bgUseAdaptive = FALSE;

static int igEvenDistBound = 0;
static int igOverlapBuffer = 0;


static int	igAssignOffSetStart;
static int	igAssignOffSetEnd;
static int	igLoadOffSetStart;
static int	igLoadOffSetEnd;
static time_t tgLoadStart;
static time_t tgLoadEnd;
static time_t tgCurrTime;
static time_t tgWorkTime;

static char cgLoadStart[20];
static char cgLoadEnd[20];

static int igJobEndBuffer;
static int igDefaultJobEndBuffer;
static int igMinJobDistance;
static int igDefaultMinJobDistance;

/********Parameter f�r die Einteilungslogik******/

static int igWorkloadX;
static int igWorkloadY; 
static int igMinTimeX; 
static int igMinTimeY; 
static int igMinSumX; 
static int igMinSumY; 
static int igMinWayX; 
static int igMinWayY; 
static int igMinQualiX; 
static int igMinQualiY; 



long lgUjtyPol = 0;
long lgUjtyBrk = 0;
long lgUjtyDel = 0;
long lgUjtyDet = 0;
long lgUjtyFlt = 0;
int igMaxFunction = 3;
int igGrpMaxFunction = 1;
long lgUpolAlo = -1;
static	int		igStartUpMode = TRACE;
static	int		igRuntimeMode = 0;
static	BOOL	bgSendUpdJob = FALSE;
static char		cgMinAcfr[15]="";
static char		cgMaxActo[15]="";
static char cgDestName[21] = "\0";
static long   lgActUrno =0 ;
static int   igReservedUrnoCnt = 0;


/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
static int InitJobhdl();

void MyStrnCpy ( char *pcpDest, const char *pcpSource, size_t ipCount, int ipTrimRight );


static int  SetAATArrayInfo(char *pcpArrayName,char *pcpArrayFieldList, long *plpAddFieldLens, 
			    char *pcpIdx01Name, char *pcpIdx01FieldList, char *pcpIdx01Order, 
			    char *pcpIdx02Name, char *pcpIdx02FieldList, char *pcpIdx02Order, 
			    char *pcpIdx03Name, char *pcpIdx03FieldList, char *pcpIdx03Order, 
			    ARRAYINFO *prpArrayInfo);

static int  SetArrayInfo(char *pcpArrayName,char *pcpTableName,  char *pcpArrayFieldList,char *pcpAddFields, long *plpAddFieldLens, 
			 char *pcpIdx01Name, char *pcpIdx01FieldList, char *pcpIdx01Order, 
			 char *pcpIdx02Name, char *pcpIdx02FieldList, char *pcpIdx02Order, 
			 char *pcpIdx03Name, char *pcpIdx03FieldList, char *pcpIdx03Order, 
			 char *pcpIdx04Name, char *pcpIdx04FieldList, char *pcpIdx04Order, 
			 char *pcpIdx05Name, char *pcpIdx05FieldList, char *pcpIdx05Order, 
			 char *pcpIdx06Name, char *pcpIdx06FieldList, char *pcpIdx06Order, 
			 char *pcpIdx07Name, char *pcpIdx07FieldList, char *pcpIdx07Order, 
			 char *pcpIdx08Name, char *pcpIdx08FieldList, char *pcpIdx08Order, 
			 ARRAYINFO *prpArrayInfo,char *pcpSelection,BOOL bpFill,BOOL bpUseTrigger);

static int  SaveIndexInfo(ARRAYINFO *prpArrayInfo);

static int  TriggerAction(char *pcpTableName,char *pcpCommands);

static int SendAnswer(EVENT *prpEvent,int ipRc);
static int SendToQue(int ipModID, int ipPrio, BC_HEAD *prpBCHead, 
		     CMDBLK *prpCmdblk, char *pcpSelection, 
		     char *pcpFields, char *pcpData);
 
static int InitializeDemPrqRow(char *pcpDemPrqBuf);
static int InitializeDemPfcRow(char *pcpDemPfcBuf);
static int InitializeDemPoolRow(char *pcpDemPoolBuf);
static int InitializeDemIdxRow(char *pcpDemIdxBuf);
static int InitializePoolPerRow(char *pcpPoolPerBuf);
static int InitializePoolFctRow(char *pcpPoolFctBuf);
static int InitializeTmpUrnoRow(char *pcpTmpUrnoBuf,int ipAidx);
static int InitializeNewJobRow(char *pcpNewJobBuf);
static int InitializeNewJodRow(char *pcpNewJodBuf);
static int InitializeUrnoUrnoRow(char *pcpUrnoUrnoBuf);

static int InitializeJobTypeRow(char *pcpJobTypeBuf);

static int InitializeKeyUrnoListRow(char *pcpKeyUrnoListBuf);

static int InitializeSwgIndexRow(char *pcpSwgIndexBuf);

static int InitializeMatchRow(char *pcpMatchBuf);

static int InitializeFieldsRow(char *pcpFieldsBuf,char *pcpField);

static int InitializeGroupNameRow(char *pcpGroupNameBuf);

static int InitializeAlidListRow(char *pcpAlidListBuf);

static int InitializeGroupListRow(char *pcpGroupListBuf);

static int KlebeFunction (ARRAYINFO *prpJobArray);

static int ClusterDemArray(ARRAYINFO *prpOpenDemArray);

static void DebugPrintArrayInfo(int ipDebugLevel, ARRAYINFO *prpArrayInfo);

static int ChangeJobTime(char *pcpUrno,char *pcpDebe,BOOL bpDebeFound,char *pcpDeen,BOOL bpDeenFound);
static void ChangeTime(time_t tlDemTime,time_t *plpPlannedTime,time_t *plpActTime);
static void MoveJobTime(time_t tlDemTime,time_t *plpPlStart,time_t *plpPlEnd,time_t *plpActStart,time_t *plpActEnd);

static int PrepareDemPfcArray(char *pcpDemUrno,char *pcpRudUrno);
static int PrepareDemPrqArray(char *pcpDemUrno,char *pcpRudUrno);

static int PrepareJobData();
static int PrepareSingleJobData(char *pcpUrno);
static BOOL PrepareSingleDemand(char *pcpDemUrno,ARRAYINFO *prpOpenDemArray);
static int PrepareDemPoolArray(char *pcpDemAlid,char *pcpDemAloc);
static void SetJobTypeArray();
static void SetFieldsArray();
static int ProcessChangeAlid(char *pcpDemUrno,char *pcpAlid);
static int ProcessSSD(char *pcpData, ARRAYINFO *prpOpenDemArray);
static int ProcessAOJ(ARRAYINFO *prpOpenDemArray,ARRAYINFO *prpPoolJobArray);
static int ProcessAFL(char *pcpFields, char *pcpData);
static int ProcessCJC();
static int AssignMAJobs(ARRAYINFO *prpOpenDemArray, ARRAYINFO *prpPoolJobArray);

				
static int CreateGroupMAJobs(ARRAYINFO *prpOpenDemArray,ARRAYINFO *prpPoolJobArray,
			     char *pcpGroupKey);

static int CreateCombinedMaJobs(ARRAYINFO *prpOpenDemArray,ARRAYINFO *prpPoolJobArray,
				char *pcpDemUrno,char *pcpDemUrue,char *pcpDemOuri,
				char *pcpDemOuro,char *pcpDemUpde,char *pcpDemUnde,int ipIndex);
										

static int CreateSplittedMAJobs(ARRAYINFO *prpPoolJobArray,
				char *pcpDemUrno,char *pcpRudUrno,char *pcpTpln,char *pcpAlid,char *pcpAloc,long lpTtgt,int ipRtwt,int ipIndex,
				time_t tpDebe,time_t tpDeen,long lpMoveOffSet,long lpMind,long lpMaxd,long *plpRealTtgt);
static int FindMatchingPoolJobs(char *pcpAlid,char *pcpAloc, char *pcpRudUrno,char *pcpTpln, time_t tpDebe,time_t tpDeen,time_t tpLade,
				long lpTtgt,int ipWtypeTo,int ipPrio,ARRAYINFO *prpPoolJobArray,
				char *pcpIsOk);

static BOOL FindMatchingSinglePoolJob(char *pcpPoolJobRow,char *pcpAlid,char *pcpAloc, char *pcpRudUrno,
				      time_t tpDebe,time_t tpDeen,time_t tpLade,
				      long lpTtgt,int ipWtypeTo,int ipPrio);


static BOOL CheckSinglePoolJob(char *pcpPoolJobRow, char *pcpRudUrno,char *pcpAlid,time_t tpDebe,time_t tpDeen,time_t tpLade,
			       long lpTtgt,int ipWtypeTo,int ipPrio);

static int CalcOptimalValues( ARRAYINFO *prpPoolJobArray,char *pcpAlid, time_t tpDebe,time_t tpDeen,long lpWayTo,
			      int ipWtypeTo,char *pcpIsOk);

static int ProcessNewPoolJob(char *pcpUrno,ARRAYINFO *prpPoolJobArray,BOOL bpIsReallyNew, BOOL bpCreateBreak);

static int ProcessChangePoolJob(char *pcpUrno);
static int ProcessDemChange(char *pcpFields, char *pcpData);
static int ProcessDeleteDemand(char *pcpUrno);
static int ProcessDeletePoolJob(char *pcpUrno);
static int CreateBreakForNewPoolJob(char *pcpUrno,char *pcpUdsr,char *pcpUstf,char *pcpSblu,
				    char *pcpSbfr,char *pcpSbto,char *pcpPoolAcfr,char *pcpPoolActo);

static int PrepareDemArray(ARRAYINFO *prpOpenDemArray);

static int  GetFieldLength(char *pcpTana, char *pcpFina, long *pipLen);
static int  GetRowLength(char *pcpTana, char *pcpFieldList, long *pipLen);
static int GetLogicalRowLength(char *pcpTotalFieldList,char *pcpFieldList, long *plpFieldSizes,long *plpLen);
static int WaitAndCheckQueue(int ipTimeout, int ipModId, ITEM **prpItem);
static int  CheckQueue(int ipModId, ITEM **prpItem);

static int TimeToStr(char *pcpTime,time_t lpTime);
static int StrToTime(char *pcpTime,time_t *plpTime);

static int GetItemNo(char *pcpFieldName,char *pcpFields,
		     int *pipItemNo);

static int GetItemData(char *pcpFieldName,char *pcpFields,char *pcpData,char *pcpDest);
static void TrimRight(char *pcpBuffer);
static void ToUpper(char *pcpText);
static int LocalToUtc(char *pcpTime);
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,char *pcpCfgBuffer);

static double EvalMaxFunction(double dpXVal, int lpXmax,int lpYmax);
static double EvalMinFunction(double dpXVal, int lpXmax,int lpYmax);

static BOOL CheckFunction(char *pcpPoolUrno,char *pcpDemUrno,int ipPrio);
static BOOL CheckQualification(char *pcpPoolUrno,char *pcpDemUrno,int ipPrio, int ipCheckBound);

static double CalcTimeBetweenJobs(char *pcpUpol, time_t tpStartTime,time_t tpEndTime);
static void CalcTimeAndWayDist(char *pcpUpol,char *pcpDemAlid, time_t tpPoolStart,time_t tpDemStart,
			       long *plpTimeDist,long *plpWayTo,int ipWtypeTo,long *plpUrnoPrevJob,long *plpUrnoNextJob);

BOOL UngarischerAlgorithmus(ARRAYINFO *prpMatchArray,int ipDemCount);

BOOL CheckAlocation(char *pcpAloc,char *pcpUalo, BOOL bpUseUrno);

static void	Terminate(int ipSleep);            /* Terminate program      */

static int	Reset(void);                       /* Reset program          */
static void	HandleSignal(int);                 /* Handles signals        */
static void	HandleErr(int);                    /* Handles general errors */
static void	HandleQueErr(int);                 /* Handles queuing errors */
static int	HandleData(void);                  /* Handles event data     */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/

static int  GetCommand(char *pcpCommand, int *pipCmd);
static int	GetDebugLevel(char *, int *);

/**********Externe Funktionen*********/
extern void GetServerTimeStamp(char *pcpType, int ipFormat,long lpTimeDiff, char *pcpTimeStamp);
static BOOL IsDemandToAssign ( char * pcpDemFlgs );
static int ReassignBreak ( char *pcpPoolJobRow, char *pcpBreakJobRow, long lpRow );
static int GetBreakInterval ( char *pcpDrrUrno, time_t *ptpBrkIntStart, time_t *ptpBrkIntEnd );
static int AddPoolFctRowIfNew ( char *pcpUjob, char *pcpFctc, int ipPrio );
static int AddPoolPerRowIfNew ( char *pcpUjob, char *pcpPrmc, int ipPrio );
static int HandleRelDrr (char *pcpBCData );
static int HandleRelJob (char *pcpBCData, char *pcpBCSelection );
static int UpdateJobRecord ( long lpRowSrc, long lpRowDest, ARRAYINFO *prpDest );
static int UpdateMinMax ( char *pcpAcfr, char *pcpActo );
static int SaveJobs ( BOOL bpSendUpdJob );
static int GetTotalRowLength( char *pcpFieldList, ARRAYINFO *prpArray, long *plpLen);

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
    int	ilRc = RC_SUCCESS;			/* Return code			*/
    int	ilCnt = 0;
    int ilOldDebugLevel;
    INITIALIZE;			/* General initialization	*/
    ilOldDebugLevel = debug_level;
    debug_level = TRACE;

    dbg(TRACE,"MAIN: version <%s>",sccs_version);

    /* Attach to the MIKE queues */
    do
    {
	ilRc = init_que();
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
	    sleep(6);
	    ilCnt++;
	}/* end of if */
    }while((ilCnt < 10) && (ilRc != RC_SUCCESS));

    if(ilRc != RC_SUCCESS)
    {
	dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
	sleep(60);
	exit(1);
    }else{
	dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }/* end of if */

    do
    {
	ilRc = init_db();
	if (ilRc != RC_SUCCESS)
	{
	    check_ret(ilRc);
	    dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
	    sleep(6);
	    ilCnt++;
	} /* end of if */
    } while((ilCnt < 10) && (ilRc != RC_SUCCESS));

    if(ilRc != RC_SUCCESS)
    {
	dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
	sleep(60);
	exit(2);
    }else{
	dbg(TRACE,"MAIN: init_db() OK!");
    } /* end of if */

    /* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */

    sprintf(cgConfigFile,"%s/demfnd",getenv("BIN_PATH"));
    ilRc = TransferFile(cgConfigFile);
    if(ilRc != RC_SUCCESS)
    {
	dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */

    /* 20000619 bch start */
    sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
    dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
    ilRc = TransferFile(cgConfigFile);
    if(ilRc != RC_SUCCESS)
    { 
	dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    }
    /* 20000619 bch end */


    ilRc = SendRemoteShutdown(mod_id);
    if(ilRc != RC_SUCCESS)
    {
	dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    } /* end of if */

    if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
	dbg(DEBUG,"MAIN: waiting for status switch ...");
	HandleQueues();
    }/* end of if */

    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
	dbg(TRACE,"MAIN: initializing ...");
	if(igInitOK == FALSE)
	{
	    ilRc = InitJobhdl();
	    if(ilRc != RC_SUCCESS)
	    {
		dbg(TRACE,"InitDemfnd: init failed!");
	    } /* end of if */
	}/* end of if */
    } else {
	Terminate(60);
    }/* end of if */


    dbg(TRACE,"MAIN: initializing OK");

    debug_level = igRuntimeMode; 

    for(;;)
    {
	ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);

	/* depending on the size of the received item  */
	/* a realloc could be made by the que function */
	/* so do never forget to set event pointer !!! */

	prgEvent = (EVENT *) prgItem->text;
				
	if( ilRc == RC_SUCCESS )
	{
	    /* Acknowledge the item */
	    ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
	    if( ilRc != RC_SUCCESS ) 
	    {
		/* handle que_ack error */
		HandleQueErr(ilRc);
	    } /* fi */
			
	    lgEvtCnt++;

	    switch( prgEvent->command )
	    {
		case	HSB_STANDBY	:
		    ctrl_sta = prgEvent->command;
		    HandleQueues();
		    break;	
		case	HSB_COMING_UP	:
		    ctrl_sta = prgEvent->command;
		    HandleQueues();
		    break;	
		case	HSB_ACTIVE	:
		    ctrl_sta = prgEvent->command;
		    break;	
		case	HSB_ACT_TO_SBY	:
		    ctrl_sta = prgEvent->command;
		    /* CloseConnection(); */
		    HandleQueues();
		    break;	
		case	HSB_DOWN	:
		    /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
		    ctrl_sta = prgEvent->command;
		    Terminate(0);
		    break;	
		case	HSB_STANDALONE	:
		    ctrl_sta = prgEvent->command;
		    ResetDBCounter();
		    break;	
		case	REMOTE_DB :
		    /* ctrl_sta is checked inside */
		    HandleRemoteDB(prgEvent);
		    break;
		case	SHUTDOWN	:
		    /* process shutdown - maybe from uutil */
		    Terminate(0);
		    break;
					
		case	RESET		:
		    ilRc = Reset();
		    break;
					
		case	EVENT_DATA	:
		    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
		    {
			ilRc = HandleData();
			if(ilRc != RC_SUCCESS)
			{
			    HandleErr(ilRc);
			}/* end of if */
		    }else{
			dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
			DebugPrintItem(TRACE,prgItem);
			DebugPrintEvent(TRACE,prgEvent);
		    }/* end of if */
		    break;
					
		case	TRACE_ON :
		    dbg_handle_debug(prgEvent->command);
		    break;
		case	TRACE_OFF :
		    dbg_handle_debug(prgEvent->command);
		    break;
		case   333 :
		    ProcessCJC();	
		    break;
		case   667 :
		    SaveIndexInfo(&rgOpenDemArray) ;
		    SaveIndexInfo(&rgJobArray) ;
		    SaveIndexInfo(&rgJodArray) ;
		    SaveIndexInfo(&rgPoolJobArray) ;
		    SaveIndexInfo(&rgRequestPoolJobArray) ;
		    SaveIndexInfo(&rgRequestOpenDemArray) ;
		    break ;
		case	668 :
		    SaveIndexInfo(&rgRueArray) ;
		    SaveIndexInfo(&rgRudArray) ;
		    SaveIndexInfo(&rgSpeArray) ;
		    SaveIndexInfo(&rgSpfArray) ;
		    SaveIndexInfo(&rgPfcArray) ;
		    SaveIndexInfo(&rgPerArray) ;
		    SaveIndexInfo(&rgJtyArray) ;
		    SaveIndexInfo(&rgDelArray) ;
		    SaveIndexInfo(&rgDrrArray) ;
		    SaveIndexInfo(&rgBsdArray) ;
		    SaveIndexInfo(&rgDrdArray) ;
		    SaveIndexInfo(&rgAloArray) ;
		    SaveIndexInfo(&rgTplArray) ;
		    SaveIndexInfo(&rgSerArray) ;
		    SaveIndexInfo(&rgRpqArray) ;
		    SaveIndexInfo(&rgRpfArray) ;
		    SaveIndexInfo(&rgPolArray) ;
		    SaveIndexInfo(&rgSgmArray) ;
		    SaveIndexInfo(&rgSgrArray) ;
		    SaveIndexInfo(&rgWgrArray) ;
		    SaveIndexInfo(&rgWayArray) ;
		    SaveIndexInfo(&rgParArray) ;
		    SaveIndexInfo(&rgDrgArray) ;
		    SaveIndexInfo(&rgDlgArray) ;
		    SaveIndexInfo(&rgSwgArray) ;
		    break;
		case	669 :
		    SaveIndexInfo(&rgDemPoolArray) ;
		    SaveIndexInfo(&rgDemPfcArray) ;
		    SaveIndexInfo(&rgDemPrqArray) ;
		    SaveIndexInfo(&rgDemIdxArray) ;
		    SaveIndexInfo(&rgPoolPerArray) ;
		    SaveIndexInfo(&rgPoolFctArray) ;
		    SaveIndexInfo(&rgTmpUrnoArray) ;
		    SaveIndexInfo(&rgUrnoUrnoArray) ;
		    SaveIndexInfo(&rgJobTypeArray) ;
		    SaveIndexInfo(&rgKeyUrnoListArray) ;
		    SaveIndexInfo(&rgFieldsArray) ;
		    SaveIndexInfo(&rgGroupNameArray) ;
		    SaveIndexInfo(&rgAlidListArray) ;
				
		    break;
		case   670 :
		    SaveIndexInfo(&rgAftArray) ;
		    break ;

		default			:
		    dbg(TRACE,"MAIN: unknown event");
		    DebugPrintItem(TRACE,prgItem);
		    DebugPrintEvent(TRACE,prgEvent);
		    break;

	    } /* end switch */
	} else {
	    /* Handle queuing errors */
	    HandleQueErr(ilRc);
		
	} /* end else */
		
    } /* end for */
	
    /* exit(0); */
	
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int InitFieldIndex()
{
    int ilRc = RC_SUCCESS;
    int ilCol,ilPos;

    FindItemInList(DEM_FIELDS,"ALID",',',&igDemAlid,&ilCol,&ilPos);
    FindItemInList(DEM_FIELDS,"ALOC",',',&igDemAloc,&ilCol,&ilPos);
    FindItemInList(DEM_FIELDS,"DEDU",',',&igDemDedu,&ilCol,&ilPos);
    FindItemInList(DEM_FIELDS,"DEBE",',',&igDemDebe,&ilCol,&ilPos);
    FindItemInList(DEM_FIELDS,"DEEN",',',&igDemDeen,&ilCol,&ilPos);
    FindItemInList(DEM_FIELDS,"DETY",',',&igDemDety,&ilCol,&ilPos);
    FindItemInList(DEM_FIELDS,"EADB",',',&igDemEadb,&ilCol,&ilPos);
    FindItemInList(DEM_FIELDS,"FLGS",',',&igDemFlgs,&ilCol,&ilPos);
    FindItemInList(DEM_FIELDS,"LADE",',',&igDemLade,&ilCol,&ilPos);
    FindItemInList(DEM_FIELDS,"OBTY",',',&igDemObty,&ilCol,&ilPos);
    FindItemInList(DEM_FIELDS,"OURI",',',&igDemOuri,&ilCol,&ilPos);
    FindItemInList(DEM_FIELDS,"OURO",',',&igDemOuro,&ilCol,&ilPos);
    FindItemInList(DEM_FIELDS,"RETY",',',&igDemRety,&ilCol,&ilPos);
    FindItemInList(DEM_FIELDS,"TTGF",',',&igDemTtgf,&ilCol,&ilPos);
    FindItemInList(DEM_FIELDS,"TTGT",',',&igDemTtgt,&ilCol,&ilPos);
    FindItemInList(DEM_FIELDS,"UDGR",',',&igDemUdgr,&ilCol,&ilPos);
    FindItemInList(DEM_FIELDS,"URNO",',',&igDemUrno,&ilCol,&ilPos);
    FindItemInList(DEM_FIELDS,"URUD",',',&igDemUrud,&ilCol,&ilPos);

    FindItemInList(DEM_FIELDS2,"MAXD",',',&igDemMaxd,&ilCol,&ilPos);
    FindItemInList(DEM_FIELDS2,"MIND",',',&igDemMind,&ilCol,&ilPos);
    FindItemInList(DEM_FIELDS2,"DIDE",',',&igDemDide,&ilCol,&ilPos);
    FindItemInList(DEM_FIELDS2,"TNAM",',',&igDemTpln,&ilCol,&ilPos);
    FindItemInList(DEM_FIELDS2,"SECO",',',&igDemSeco,&ilCol,&ilPos);
    FindItemInList(DEM_FIELDS2,"SIX1",',',&igDemSix1,&ilCol,&ilPos);
    FindItemInList(DEM_FIELDS2,"SIX2",',',&igDemSix2,&ilCol,&ilPos);
    FindItemInList(DEM_FIELDS2,"TCHD",',',&igDemTchd,&ilCol,&ilPos);
    FindItemInList(DEM_FIELDS2,"RTWF",',',&igDemRtwf,&ilCol,&ilPos);
    FindItemInList(DEM_FIELDS2,"RTWT",',',&igDemRtwt,&ilCol,&ilPos);
    FindItemInList(DEM_FIELDS2,"UALO",',',&igDemUalo,&ilCol,&ilPos);
    FindItemInList(DEM_FIELDS2,"ULNK",',',&igDemUlnk,&ilCol,&ilPos);
    FindItemInList(DEM_FIELDS2,"UPDE",',',&igDemUpde,&ilCol,&ilPos);
    FindItemInList(DEM_FIELDS2,"UNDE",',',&igDemUnde,&ilCol,&ilPos);
    FindItemInList(DEM_FIELDS2,"URUE",',',&igDemUrue,&ilCol,&ilPos);
	FindItemInList(DEM_FIELDS2,"UTPL",',',&igDemUtpl,&ilCol,&ilPos);
	
	/* physical fields for rgJobArray */
    FindItemInList(cgJobFields,"ACFR",',',&igJobAcfr,&ilCol,&ilPos);
    FindItemInList(cgJobFields,"ACT3",',',&igJobAct3,&ilCol,&ilPos);
    FindItemInList(cgJobFields,"ACTO",',',&igJobActo,&ilCol,&ilPos);
    FindItemInList(cgJobFields,"CDAT",',',&igJobCdat,&ilCol,&ilPos);
    FindItemInList(cgJobFields,"DETY",',',&igJobDety,&ilCol,&ilPos);
    FindItemInList(cgJobFields,"GATE",',',&igJobGate,&ilCol,&ilPos);
    FindItemInList(cgJobFields,"HOPO",',',&igJobHopo,&ilCol,&ilPos);
    FindItemInList(cgJobFields,"JOUR",',',&igJobJour,&ilCol,&ilPos);
    FindItemInList(cgJobFields,"LSTU",',',&igJobLstu,&ilCol,&ilPos);
    FindItemInList(cgJobFields,"PLFR",',',&igJobPlfr,&ilCol,&ilPos);
    FindItemInList(cgJobFields,"PLTO",',',&igJobPlto,&ilCol,&ilPos);
    FindItemInList(cgJobFields,"POSI",',',&igJobPosi,&ilCol,&ilPos);
    FindItemInList(cgJobFields,"REGN",',',&igJobRegn,&ilCol,&ilPos);
    FindItemInList(cgJobFields,"STAT",',',&igJobStat,&ilCol,&ilPos);
    FindItemInList(cgJobFields,"TEXT",',',&igJobText,&ilCol,&ilPos);
    FindItemInList(cgJobFields,"UAFT",',',&igJobUaft,&ilCol,&ilPos);
    FindItemInList(cgJobFields,"UAID",',',&igJobUaid,&ilCol,&ilPos);
    FindItemInList(cgJobFields,"UALO",',',&igJobUalo,&ilCol,&ilPos);
    FindItemInList(cgJobFields,"UDEL",',',&igJobUdel,&ilCol,&ilPos);
    FindItemInList(cgJobFields,"UDRD",',',&igJobUdrd,&ilCol,&ilPos);
    FindItemInList(cgJobFields,"UDSR",',',&igJobUdsr,&ilCol,&ilPos);
    FindItemInList(cgJobFields,"UJTY",',',&igJobUjty,&ilCol,&ilPos);
    FindItemInList(cgJobFields,"URNO",',',&igJobUrno,&ilCol,&ilPos);
    FindItemInList(cgJobFields,"USEC",',',&igJobUsec,&ilCol,&ilPos);
    FindItemInList(cgJobFields,"USEU",',',&igJobUseu,&ilCol,&ilPos);
    FindItemInList(cgJobFields,"USTF",',',&igJobUstf,&ilCol,&ilPos);
    FindItemInList(cgJobFields,"TTGF",',',&igJobTtgf,&ilCol,&ilPos);
    FindItemInList(cgJobFields,"TTGT",',',&igJobTtgt,&ilCol,&ilPos);
	FindItemInList(cgJobFields,"UTPL",',',&igJobUtpl,&ilCol,&ilPos);
	dbg ( DEBUG, "InitFieldIndex: Index of JOBTAB.UTPL <%d>", igJobUtpl );
	/* logical fields for rgJobArray */
    FindItemInList(cgJobFields,"ALID",',',&igJobAlid,&ilCol,&ilPos);
    FindItemInList(cgJobFields,"ALOC",',',&igJobAloc,&ilCol,&ilPos);
    FindItemInList(cgJobFields,"TXXD",',',&igJobTxxd,&ilCol,&ilPos);

    FindItemInList(POOLJOB_FIELDS2,"ISOK",',',&igPoolJobIsok,&ilCol,&ilPos);
    FindItemInList(POOLJOB_FIELDS2,"TCHX",',',&igPoolJobTchd,&ilCol,&ilPos);
    FindItemInList(POOLJOB_FIELDS2,"PJVD",',',&igPoolJobPjvd,&ilCol,&ilPos);
    FindItemInList(POOLJOB_FIELDS2,"PRIO",',',&igPoolJobPrio,&ilCol,&ilPos);
    FindItemInList(POOLJOB_FIELDS2,"WKLD",',',&igPoolJobWkld,&ilCol,&ilPos);
    FindItemInList(POOLJOB_FIELDS2,"MIN2",',',&igPoolJobMin2,&ilCol,&ilPos);
    FindItemInList(POOLJOB_FIELDS2,"MINT",',',&igPoolJobMint,&ilCol,&ilPos);
    FindItemInList(POOLJOB_FIELDS2,"WAYT",',',&igPoolJobWayt,&ilCol,&ilPos);
    FindItemInList(POOLJOB_FIELDS2,"WAYF",',',&igPoolJobWayf,&ilCol,&ilPos);
    FindItemInList(POOLJOB_FIELDS2,"UPRJ",',',&igPoolJobUprj,&ilCol,&ilPos);
    FindItemInList(POOLJOB_FIELDS2,"UNXJ",',',&igPoolJobUnxj,&ilCol,&ilPos);
    /*FindItemInList(POOLJOB_FIELDS2,"NJED",',',&igPoolJobNjed,&ilCol,&ilPos);
    FindItemInList(POOLJOB_FIELDS2,"NJBN",',',&igPoolJobNjbn,&ilCol,&ilPos);*/
    FindItemInList(POOLJOB_FIELDS2,"SDAY",',',&igPoolJobSday,&ilCol,&ilPos);
    FindItemInList(POOLJOB_FIELDS2,"WGPC",',',&igPoolJobWgpc,&ilCol,&ilPos);
	
    FindItemInList(JOD_FIELDS,"CDAT",',',&igJodCdat,&ilCol,&ilPos);
    FindItemInList(JOD_FIELDS,"HOPO",',',&igJodHopo,&ilCol,&ilPos);
    FindItemInList(JOD_FIELDS,"LSTU",',',&igJodLstu,&ilCol,&ilPos);
    FindItemInList(JOD_FIELDS,"UJOB",',',&igJodUjob,&ilCol,&ilPos);
    FindItemInList(JOD_FIELDS,"UDEM",',',&igJodUdem,&ilCol,&ilPos);
    FindItemInList(JOD_FIELDS,"URNO",',',&igJodUrno,&ilCol,&ilPos);
    FindItemInList(JOD_FIELDS,"USEC",',',&igJodUsec,&ilCol,&ilPos);
    FindItemInList(JOD_FIELDS,"USEU",',',&igJodUseu,&ilCol,&ilPos);
    FindItemInList(JOD_FIELDS2,"TCHX",',',&igJodTchx,&ilCol,&ilPos);

	
    FindItemInList(RUD_FIELDS,"URNO",',',&igRudUrno,&ilCol,&ilPos);
    FindItemInList(RUD_FIELDS,"URUE",',',&igRudUrue,&ilCol,&ilPos);
    FindItemInList(RUD_FIELDS,"UGHS",',',&igRudUghs,&ilCol,&ilPos);
    FindItemInList(RUD_FIELDS,"UDGR",',',&igRudUdgr,&ilCol,&ilPos);
    FindItemInList(RUD_FIELDS,"FOND",',',&igRudFond,&ilCol,&ilPos);
    FindItemInList(RUD_FIELDS,"ULNK",',',&igRudUlnk,&ilCol,&ilPos);
    FindItemInList(RUD_FIELDS,"UPDE",',',&igRudUpde,&ilCol,&ilPos);
    FindItemInList(RUD_FIELDS,"UNDE",',',&igRudUnde,&ilCol,&ilPos);
    FindItemInList(RUD_FIELDS,"MAXD",',',&igRudMaxd,&ilCol,&ilPos);
    FindItemInList(RUD_FIELDS,"MIND",',',&igRudMind,&ilCol,&ilPos);
    FindItemInList(RUD_FIELDS,"DIDE",',',&igRudDide,&ilCol,&ilPos);
    FindItemInList(RUD_FIELDS,"REDE",',',&igRudRede,&ilCol,&ilPos);

    FindItemInList(RUE_FIELDS,"URNO",',',&igRueUrno,&ilCol,&ilPos);
    FindItemInList(RUE_FIELDS,"UTPL",',',&igRueUtpl,&ilCol,&ilPos);

    FindItemInList(SER_FIELDS,"URNO",',',&igSerUrno,&ilCol,&ilPos);
    FindItemInList(SER_FIELDS,"SECO",',',&igSerSeco,&ilCol,&ilPos);
    FindItemInList(SER_FIELDS,"RTWF",',',&igSerRtwf,&ilCol,&ilPos);
    FindItemInList(SER_FIELDS,"RTWT",',',&igSerRtwt,&ilCol,&ilPos);

    FindItemInList(ALO_FIELDS,"URNO",',',&igAloUrno,&ilCol,&ilPos);
    FindItemInList(ALO_FIELDS,"REFT",',',&igAloReft,&ilCol,&ilPos);
    FindItemInList(ALO_FIELDS,"ALOT",',',&igAloAlot,&ilCol,&ilPos);
    FindItemInList(ALO_FIELDS,"ALOC",',',&igAloAloc,&ilCol,&ilPos);
	
    FindItemInList(AFT_FIELDS,"URNO",',',&igAftUrno,&ilCol,&ilPos);
    FindItemInList(AFT_FIELDS,"ACT3",',',&igAftAct3,&ilCol,&ilPos);
    FindItemInList(AFT_FIELDS,"REGN",',',&igAftRegn,&ilCol,&ilPos);
    FindItemInList(AFT_FIELDS,"PSTD",',',&igAftPstd,&ilCol,&ilPos);
    FindItemInList(AFT_FIELDS,"PSTA",',',&igAftPsta,&ilCol,&ilPos);
    FindItemInList(AFT_FIELDS,"GTA1",',',&igAftGta1,&ilCol,&ilPos);
    FindItemInList(AFT_FIELDS,"GTA2",',',&igAftGta2,&ilCol,&ilPos);
    FindItemInList(AFT_FIELDS,"GTD1",',',&igAftGtd1,&ilCol,&ilPos);
    FindItemInList(AFT_FIELDS,"GTD2",',',&igAftGtd2,&ilCol,&ilPos);
    FindItemInList(AFT_FIELDS,"ADID",',',&igAftAdid,&ilCol,&ilPos);
	
    FindItemInList(TPL_FIELDS,"URNO",',',&igTplUrno,&ilCol,&ilPos);
    FindItemInList(TPL_FIELDS,"TNAM",',',&igTplTnam,&ilCol,&ilPos);

    FindItemInList(JTY_FIELDS,"URNO",',',&igJtyUrno,&ilCol,&ilPos);
    FindItemInList(JTY_FIELDS,"NAME",',',&igJtyName,&ilCol,&ilPos);

    FindItemInList(SPE_FIELDS,"URNO",',',&igSpeUrno,&ilCol,&ilPos);
    FindItemInList(SPE_FIELDS,"SURN",',',&igSpeSurn,&ilCol,&ilPos);
    FindItemInList(SPE_FIELDS,"CODE",',',&igSpeCode,&ilCol,&ilPos);
    FindItemInList(SPE_FIELDS,"VPFR",',',&igSpeVpfr,&ilCol,&ilPos);
    FindItemInList(SPE_FIELDS,"VPTO",',',&igSpeVpto,&ilCol,&ilPos);
    FindItemInList(SPE_FIELDS,"PRIO",',',&igSpePrio,&ilCol,&ilPos);
	dbg ( TRACE, "Index of SPETAB-fields PRIO <%d>", igSpePrio );

    FindItemInList(SPF_FIELDS,"URNO",',',&igSpfUrno,&ilCol,&ilPos);
    FindItemInList(SPF_FIELDS,"SURN",',',&igSpfSurn,&ilCol,&ilPos);
    FindItemInList(SPF_FIELDS,"CODE",',',&igSpfCode,&ilCol,&ilPos);
    FindItemInList(SPF_FIELDS,"VPFR",',',&igSpfVpfr,&ilCol,&ilPos);
    FindItemInList(SPF_FIELDS,"VPTO",',',&igSpfVpto,&ilCol,&ilPos);
    FindItemInList(SPF_FIELDS,"PRIO",',',&igSpfPrio,&ilCol,&ilPos);


    FindItemInList(PFC_FIELDS,"URNO",',',&igPfcUrno,&ilCol,&ilPos);
    FindItemInList(PFC_FIELDS,"FCTC",',',&igPfcFctc,&ilCol,&ilPos);
    FindItemInList(PFC_FIELDS,"PRIO",',',&igPfcPrio,&ilCol,&ilPos);

    FindItemInList(RPF_FIELDS,"URUD",',',&igRpfUrud,&ilCol,&ilPos);
    FindItemInList(RPF_FIELDS,"FCCO",',',&igRpfFcco,&ilCol,&ilPos);
    FindItemInList(RPF_FIELDS,"UPFC",',',&igRpfUpfc,&ilCol,&ilPos);

    FindItemInList(RPQ_FIELDS,"URUD",',',&igRpqUrud,&ilCol,&ilPos);
    FindItemInList(RPQ_FIELDS,"QUCO",',',&igRpqQuco,&ilCol,&ilPos);
    FindItemInList(RPQ_FIELDS,"UPER",',',&igRpqUper,&ilCol,&ilPos);
	
    FindItemInList(PER_FIELDS,"URNO",',',&igPerUrno,&ilCol,&ilPos);
    FindItemInList(PER_FIELDS,"PRMC",',',&igPerPrmc,&ilCol,&ilPos);
    FindItemInList(PER_FIELDS,"PRIO",',',&igPerPrio,&ilCol,&ilPos);
	
    FindItemInList(DEL_FIELDS,"BSDU",',',&igDelBsdu,&ilCol,&ilPos);
    FindItemInList(DEL_FIELDS,"URNO",',',&igDelUrno,&ilCol,&ilPos);
    FindItemInList(DEL_FIELDS,"FCTC",',',&igDelFctc,&ilCol,&ilPos);

    FindItemInList(DRR_FIELDS,"BSDU",',',&igDrrBsdu,&ilCol,&ilPos);
    FindItemInList(DRR_FIELDS,"DRSF",',',&igDrrDrsf,&ilCol,&ilPos);
    FindItemInList(DRR_FIELDS,"URNO",',',&igDrrUrno,&ilCol,&ilPos);
    FindItemInList(DRR_FIELDS,"DRRN",',',&igDrrDrrn,&ilCol,&ilPos);
    FindItemInList(DRR_FIELDS,"SDAY",',',&igDrrSday,&ilCol,&ilPos);
    FindItemInList(DRR_FIELDS,"STFU",',',&igDrrStfu,&ilCol,&ilPos);
    FindItemInList(DRR_FIELDS,"SBLU",',',&igDrrSblu,&ilCol,&ilPos);
    FindItemInList(DRR_FIELDS,"SBFR",',',&igDrrSbfr,&ilCol,&ilPos);
    FindItemInList(DRR_FIELDS,"SBTO",',',&igDrrSbto,&ilCol,&ilPos);
    FindItemInList(DRR_FIELDS,"FCTC",',',&igDrrFctc,&ilCol,&ilPos);

    FindItemInList(DRD_FIELDS,"URNO",',',&igDrdUrno,&ilCol,&ilPos);
    FindItemInList(DRD_FIELDS,"PRMC",',',&igDrdPrmc,&ilCol,&ilPos);
    FindItemInList(DRD_FIELDS,"FCTC",',',&igDrdFctc,&ilCol,&ilPos);
    FindItemInList(DRD_FIELDS,"DRRN",',',&igDrdDrrn,&ilCol,&ilPos);
    FindItemInList(DRD_FIELDS,"SDAY",',',&igDrdSday,&ilCol,&ilPos);
    FindItemInList(DRD_FIELDS,"STFU",',',&igDrdStfu,&ilCol,&ilPos);
	
    FindItemInList(DRG_FIELDS,"URNO",',',&igDrgUrno,&ilCol,&ilPos);
    FindItemInList(DRG_FIELDS,"FCTC",',',&igDrgFctc,&ilCol,&ilPos);
    FindItemInList(DRG_FIELDS,"DRRN",',',&igDrgDrrn,&ilCol,&ilPos);
    FindItemInList(DRG_FIELDS,"SDAY",',',&igDrgSday,&ilCol,&ilPos);
    FindItemInList(DRG_FIELDS,"STFU",',',&igDrgStfu,&ilCol,&ilPos);
    FindItemInList(DRG_FIELDS,"WGPC",',',&igDrgWgpc,&ilCol,&ilPos);
	
    FindItemInList(DLG_FIELDS,"URNO",',',&igDlgUrno,&ilCol,&ilPos);
    FindItemInList(DLG_FIELDS,"FCTC",',',&igDlgFctc,&ilCol,&ilPos);
    FindItemInList(DLG_FIELDS,"SDAY",',',&igDlgSday,&ilCol,&ilPos);
    FindItemInList(DLG_FIELDS,"UJOB",',',&igDlgUjob,&ilCol,&ilPos);
    FindItemInList(DLG_FIELDS,"WGPC",',',&igDlgWgpc,&ilCol,&ilPos);	

    FindItemInList(BSD_FIELDS,"URNO",',',&igBsdUrno,&ilCol,&ilPos);
    FindItemInList(BSD_FIELDS,"FCTC",',',&igBsdFctc,&ilCol,&ilPos);

    FindItemInList(WAY_FIELDS,"WTYP",',',&igWayWtyp,&ilCol,&ilPos);
    FindItemInList(WAY_FIELDS,"TTGO",',',&igWayTtgo,&ilCol,&ilPos);
    FindItemInList(WAY_FIELDS,"POBE",',',&igWayPobe,&ilCol,&ilPos);
    FindItemInList(WAY_FIELDS,"POEN",',',&igWayPoen,&ilCol,&ilPos);

    FindItemInList(POL_FIELDS,"URNO",',',&igPolUrno,&ilCol,&ilPos);
    FindItemInList(POL_FIELDS,"NAME",',',&igPolName,&ilCol,&ilPos);

    FindItemInList(SGM_FIELDS,"UVAL",',',&igSgmUval,&ilCol,&ilPos);
    FindItemInList(SGM_FIELDS,"TABN",',',&igSgmTabn,&ilCol,&ilPos);
    FindItemInList(SGM_FIELDS,"USGR",',',&igSgmUsgr,&ilCol,&ilPos);

    FindItemInList(SGR_FIELDS,"URNO",',',&igSgrUrno,&ilCol,&ilPos);
    FindItemInList(SGR_FIELDS,"TABN",',',&igSgrTabn,&ilCol,&ilPos);
    FindItemInList(SGR_FIELDS,"UGTY",',',&igSgrUgty,&ilCol,&ilPos);
    FindItemInList(SGR_FIELDS,"GRPN",',',&igSgrGrpn,&ilCol,&ilPos);

    FindItemInList(PAR_FIELDS,"APPL",',',&igParAppl,&ilCol,&ilPos);
    FindItemInList(PAR_FIELDS,"URNO",',',&igParUrno,&ilCol,&ilPos);
    FindItemInList(PAR_FIELDS,"PAID",',',&igParPaid,&ilCol,&ilPos);
    FindItemInList(PAR_FIELDS,"NAME",',',&igParName,&ilCol,&ilPos);
    FindItemInList(PAR_FIELDS,"HOPO",',',&igParHopo,&ilCol,&ilPos);
    FindItemInList(PAR_FIELDS,"VALU",',',&igParValu,&ilCol,&ilPos);

    FindItemInList(SWG_FIELDS,"CODE",',',&igSwgCode,&ilCol,&ilPos);
    FindItemInList(SWG_FIELDS,"HOPO",',',&igSwgHopo,&ilCol,&ilPos);
    FindItemInList(SWG_FIELDS,"URNO",',',&igSwgUrno,&ilCol,&ilPos);
    FindItemInList(SWG_FIELDS,"SURN",',',&igSwgSurn,&ilCol,&ilPos);
    FindItemInList(SWG_FIELDS,"VPFR",',',&igSwgVpfr,&ilCol,&ilPos);
    FindItemInList(SWG_FIELDS,"VPTO",',',&igSwgVpto,&ilCol,&ilPos);


    FindItemInList(WGR_FIELDS,"URNO",',',&igWgrUrno,&ilCol,&ilPos);
    FindItemInList(WGR_FIELDS,"HOPO",',',&igWgrHopo,&ilCol,&ilPos);
    FindItemInList(WGR_FIELDS,"ALID",',',&igWgrAlid,&ilCol,&ilPos);
    FindItemInList(WGR_FIELDS,"ALOC",',',&igWgrAloc,&ilCol,&ilPos);
    FindItemInList(WGR_FIELDS,"ALGR",',',&igWgrAlgr,&ilCol,&ilPos);

    FindItemInList(DEMPFC_FIELDS,"FCOD",',',&igDemPfcFcod,&ilCol,&ilPos);
    FindItemInList(DEMPFC_FIELDS,"UDEM",',',&igDemPfcUdem,&ilCol,&ilPos);
    FindItemInList(DEMPFC_FIELDS,"STAT",',',&igDemPfcStat,&ilCol,&ilPos);
    FindItemInList(DEMPFC_FIELDS,"UPJB",',',&igDemPfcUpjb,&ilCol,&ilPos);

    FindItemInList(DEMPRQ_FIELDS,"QCOD",',',&igDemPrqQcod,&ilCol,&ilPos);
    FindItemInList(DEMPRQ_FIELDS,"UDEM",',',&igDemPrqUdem,&ilCol,&ilPos);
    FindItemInList(DEMPRQ_FIELDS,"STAT",',',&igDemPrqStat,&ilCol,&ilPos);
    FindItemInList(DEMPRQ_FIELDS,"UPJB",',',&igDemPrqUpjb,&ilCol,&ilPos);

    FindItemInList(DEMPOOL_FIELDS,"ALOC",',',&igDemPoolAloc,&ilCol,&ilPos);
    FindItemInList(DEMPOOL_FIELDS,"ALID",',',&igDemPoolAlid,&ilCol,&ilPos);
    FindItemInList(DEMPOOL_FIELDS,"UPOL",',',&igDemPoolUpol,&ilCol,&ilPos);
    FindItemInList(DEMPOOL_FIELDS,"UAID",',',&igDemPoolUaid,&ilCol,&ilPos);

    FindItemInList(DEMIDX_FIELDS,"OURI",',',&igDemIdxOuri,&ilCol,&ilPos);
    FindItemInList(DEMIDX_FIELDS,"OURO",',',&igDemIdxOuro,&ilCol,&ilPos);
    FindItemInList(DEMIDX_FIELDS,"TPLN",',',&igDemIdxTpln,&ilCol,&ilPos);
    FindItemInList(DEMIDX_FIELDS,"SERN",',',&igDemIdxSern,&ilCol,&ilPos);
    FindItemInList(DEMIDX_FIELDS,"KCNT",',',&igDemIdxKcnt,&ilCol,&ilPos);

    FindItemInList(POOLPER_FIELDS,"UJOB",',',&igPoolPerUjob,&ilCol,&ilPos);
    FindItemInList(POOLPER_FIELDS,"PERM",',',&igPoolPerPerm,&ilCol,&ilPos);
    FindItemInList(POOLPER_FIELDS,"PRIO",',',&igPoolPerPrio,&ilCol,&ilPos);

    FindItemInList(POOLFCT_FIELDS,"UJOB",',',&igPoolFctUjob,&ilCol,&ilPos);
    FindItemInList(POOLFCT_FIELDS,"FCTC",',',&igPoolFctFctc,&ilCol,&ilPos);
    FindItemInList(POOLFCT_FIELDS,"PRIO",',',&igPoolFctPrio,&ilCol,&ilPos);

    FindItemInList(TMPURNO_FIELDS,"STAT",',',&igTmpUrnoStat,&ilCol,&ilPos);
    FindItemInList(TMPURNO_FIELDS,"AIDX",',',&igTmpUrnoAidx,&ilCol,&ilPos);
    FindItemInList(TMPURNO_FIELDS,"URNO",',',&igTmpUrnoUrno,&ilCol,&ilPos);
	
    FindItemInList(URNOURNO_FIELDS,"STAT",',',&igUrnoUrnoStat,&ilCol,&ilPos);
    FindItemInList(URNOURNO_FIELDS,"URN1",',',&igUrnoUrnoUrn1,&ilCol,&ilPos);
    FindItemInList(URNOURNO_FIELDS,"URN2",',',&igUrnoUrnoUrn2,&ilCol,&ilPos);
	
    FindItemInList(JOBTYPE_FIELDS,"DETY",',',&igJobTypeDety,&ilCol,&ilPos);
    FindItemInList(JOBTYPE_FIELDS,"ALOC",',',&igJobTypeAloc,&ilCol,&ilPos);
    FindItemInList(JOBTYPE_FIELDS,"JTYP",',',&igJobTypeJtyp,&ilCol,&ilPos);
	
    FindItemInList(KEYURNOLIST_FIELDS,"XKEY",',',&igKeyUrnoListXKey,&ilCol,&ilPos);
    FindItemInList(KEYURNOLIST_FIELDS,"URNO",',',&igKeyUrnoListUrno,&ilCol,&ilPos);
    FindItemInList(KEYURNOLIST_FIELDS,"BEGI",',',&igKeyUrnoListBegi,&ilCol,&ilPos);
    FindItemInList(KEYURNOLIST_FIELDS,"ENDE",',',&igKeyUrnoListEnde,&ilCol,&ilPos);
    FindItemInList(KEYURNOLIST_FIELDS,"UDEM",',',&igKeyUrnoListUdem,&ilCol,&ilPos);


    FindItemInList(FIELDS_FIELDS,"FNAM",',',&igFieldsFnam,&ilCol,&ilPos);

    FindItemInList(SWGINDEX_FIELDS,"CODE",',',&igSwgIndexCode,&ilCol,&ilPos);
    FindItemInList(SWGINDEX_FIELDS,"SIZE",',',&igSwgIndexSize,&ilCol,&ilPos);

    FindItemInList(MATCH_FIELDS,"UPJP",',',&igMatchUpjb,&ilCol,&ilPos);
    FindItemInList(MATCH_FIELDS,"URUD",',',&igMatchUrud,&ilCol,&ilPos);
    FindItemInList(MATCH_FIELDS,"UDEM",',',&igMatchUdem,&ilCol,&ilPos);
    FindItemInList(MATCH_FIELDS,"STAT",',',&igMatchStat,&ilCol,&ilPos);
    FindItemInList(MATCH_FIELDS,"TSTA",',',&igMatchTsta,&ilCol,&ilPos);
    FindItemInList(MATCH_FIELDS,"MARK",',',&igMatchMark,&ilCol,&ilPos);
    FindItemInList(MATCH_FIELDS,"DEBE",',',&igMatchDebe,&ilCol,&ilPos);
    FindItemInList(MATCH_FIELDS,"DEEN",',',&igMatchDeen,&ilCol,&ilPos);
    FindItemInList(MATCH_FIELDS,"PRIO",',',&igMatchPrio,&ilCol,&ilPos);
    FindItemInList(MATCH_FIELDS,"DPRI",',',&igMatchDpri,&ilCol,&ilPos);
    FindItemInList(MATCH_FIELDS,"JPRI",',',&igMatchJpri,&ilCol,&ilPos);

    FindItemInList(GROUPLIST_FIELDS,"GRPN",',',&igGroupListGrpn,&ilCol,&ilPos);
    FindItemInList(GROUPLIST_FIELDS,"UPJB",',',&igGroupListUpjb,&ilCol,&ilPos);
    FindItemInList(GROUPLIST_FIELDS,"UDEM",',',&igGroupListUdem,&ilCol,&ilPos);
    FindItemInList(GROUPLIST_FIELDS,"VALU",',',&igGroupListValu,&ilCol,&ilPos);
    FindItemInList(GROUPLIST_FIELDS,"DEBE",',',&igGroupListDebe,&ilCol,&ilPos);
    FindItemInList(GROUPLIST_FIELDS,"DEEN",',',&igGroupListDeen,&ilCol,&ilPos);

    FindItemInList(GROUPNAME_FIELDS,"WGPC",',',&igGroupNameWgpc,&ilCol,&ilPos);

    FindItemInList(ALIDLIST_FIELDS,"ALID",',',&igAlidListAlid,&ilCol,&ilPos);
    FindItemInList(ALIDLIST_FIELDS,"ALOC",',',&igAlidListAloc,&ilCol,&ilPos);
    FindItemInList(ALIDLIST_FIELDS,"UAID",',',&igAlidListUaid,&ilCol,&ilPos);
    FindItemInList(ALIDLIST_FIELDS,"UALO",',',&igAlidListUalo,&ilCol,&ilPos);

    FindItemInList(URNO_FIELDS,"URNO",',',&igUrnoUrno,&ilCol,&ilPos);
	
    return ilRc;
}

static int InitJobhdl()
{
    int	ilRc = RC_SUCCESS;			/* Return code */
    char pclSection[64];
    char pclKeyword[64];
    char pclSelection[512];
    char pclSqlBuf[2560];
    char pclDataArea[2560];
    char pclErr[2560];
    short slCursor;
    short slSqlFunc;
    char pclJtytab[10];
    char pclApttab[10];
    long pllAddFieldLens[20];
    char pclAddFields[256];
    char clKey[12];
    long llFieldLength;
    long llRowNum = ARR_FIRST;
    char *pclJtyRow = NULL;
    char *pclAloRow = NULL;
    char clSdayStart[20];
    char clSdayEnd[20];
	int	 ilCol,ilPos, ilJtyUrnoIdx;

    long llNext = ARR_NEXT;	
	char clJobLoadStart[16];
	
    /* read TableExtension from SGS.TAB */
    FindItemInList(JTY_FIELDS,"URNO",',',&ilJtyUrnoIdx,&ilCol,&ilPos);
    
    if(ilRc == RC_SUCCESS)
    {
	sprintf(pclSection,"ALL");
	sprintf(pclKeyword,"TABEND");

	ilRc = tool_search_exco_data(pclSection, pclKeyword, cgTabEnd);
	if (ilRc != RC_SUCCESS || strlen(cgTabEnd) <= 0)
	{
	    dbg(TRACE,"InitJobhdl() Error reading TableExtension: [%s] %s not found in ceda/conf/sgs.tab",pclSection,pclKeyword);
	    ilRc = RC_FAIL;
	    Terminate(60);
	}
	else
	{
	    sprintf(pclJtytab,"JTY%s",cgTabEnd);
	    sprintf(pclApttab,"APT%s",cgTabEnd);
	    dbg(TRACE,"InitJobhdl() TableExtension = <%s>",cgTabEnd);
	}
    }

	
    /* read HomeAirport from SGS.TAB */
    if(ilRc == RC_SUCCESS)
    {
	memset((void*)&cgHopo[0], 0x00, 8);

	sprintf(pclSection,"SYS");
	sprintf(pclKeyword,"HOMEAP");

	ilRc = tool_search_exco_data(pclSection, pclKeyword, cgHopo);
	if (ilRc != RC_SUCCESS || strlen(cgHopo) <= 0)
	{
	    dbg(TRACE,"InitDemfnd() Error reading HomeAirport: [%s] %s not found in ceda/conf/sgs.tab",pclSection,pclKeyword);
	    ilRc = RC_FAIL;
	    Terminate(60);
	}
	else
	{
	    dbg(TRACE,"InitDemfnd() HomeAirport = <%s>",cgHopo);
	}
    }

    if(ilRc == RC_SUCCESS)
    {
	GetDebugLevel("STARTUP_MODE", &igStartUpMode);
	GetDebugLevel("RUNTIME_MODE", &igRuntimeMode);
    }
    debug_level = igStartUpMode;
	
    if(ilRc == RC_SUCCESS)
    {
	slSqlFunc = START;
	slCursor = 0;
	sprintf(pclSqlBuf,"SELECT TICH,TDI1,TDI2 FROM %s WHERE APC3='%s'",pclApttab,cgHopo);
	ilRc = sql_if(slSqlFunc,&slCursor,pclSqlBuf,pclDataArea);
	if (ilRc != DB_SUCCESS)
	{
	    get_ora_err(ilRc,pclErr);
	    dbg(TRACE,"InitDemfnd() Error reading UTC-Local difference in APTTAB: ilRc <%d>\nORA-ERR <%s>\nSqlBuf <%s>",ilRc,pclErr,pclSqlBuf);
	    ilRc = RC_FAIL;
	}
	else
	{
	    strcpy(cgTdi1,"60");
	    strcpy(cgTdi2,"60");
	    get_fld(pclDataArea,0,STR,14,cgTich);
	    get_fld(pclDataArea,1,STR,14,cgTdi1);
	    get_fld(pclDataArea,2,STR,14,cgTdi2);
	    TrimRight(cgTich);
	    if (*cgTich != '\0')
	    {
		TrimRight(cgTdi1);
		TrimRight(cgTdi2);
		sprintf(cgTdi1,"%d",atoi(cgTdi1)*60);
		sprintf(cgTdi2,"%d",atoi(cgTdi2)*60);
	    }
	    dbg(TRACE,"InitJobhdl() Read %s -> TICH: <%s> TDI1: <%s> TDI2 <%s>",pclApttab,cgTich,cgTdi1,cgTdi2);
	}
	close_my_cursor(&slCursor);
	commit_work();
	slCursor = 0;
    }
	
    if(ilRc == RC_SUCCESS)
    {
	char pclTmpText[100];
	int  ilOffSet = 0;
	char clCurrTime[20];

	GetServerTimeStamp("UTC", 1, 0, clCurrTime);

	StrToTime(clCurrTime,&tgCurrTime);

	sprintf(pclSection,"MAIN");
	sprintf(pclKeyword,"CURRENT_TIME");
	if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) == RC_SUCCESS)
	{
	    StrToTime(pclTmpText,&tgCurrTime);
	    dbg(TRACE,"InitJobhdl() Reading Current Time <%s>" ,pclTmpText);
	}

	sprintf(pclSection,"MAIN");
	sprintf(pclKeyword,"ASSIGN_OFFSET_START");
	if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) == RC_SUCCESS)
	{
	    ilOffSet = atol(pclTmpText);
	    igAssignOffSetStart = (int) ilOffSet*60;
	}
	else
	{
	    igAssignOffSetStart = 60;
	}
	sprintf(pclKeyword,"ASSIGN_OFFSET_END");
	if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) == RC_SUCCESS)
	{
	    ilOffSet = atol(pclTmpText);
	    igAssignOffSetEnd = (int) ilOffSet*60;
	}
	else
	{
	    igAssignOffSetEnd = 120;
	}
		
	sprintf(pclKeyword,"LOAD_OFFSET_START");
	if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) == RC_SUCCESS)
	{
	    ilOffSet = atol(pclTmpText);
	    igLoadOffSetStart = (int) ilOffSet*60*60;
	}
	else
	{
	    igLoadOffSetStart = -(12*60*60);
	}
	sprintf(pclKeyword,"LOAD_OFFSET_END");
	if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) == RC_SUCCESS)
	{
	    ilOffSet = atol(pclTmpText);
	    igLoadOffSetEnd = (int) ilOffSet*60*60;
	}
	else
	{
	    igLoadOffSetEnd = 24*60*60;
	}
		
	sprintf(pclKeyword,"AUTOMATIC_PAUSE_ALLOCATION");

	ReadConfigEntry(pclSection,pclKeyword,pclTmpText);
	ToUpper(pclTmpText);
	if(!strcmp(pclTmpText,"YES"))
	{
	    bgAutomaticPauseAllocation = TRUE;
	}
	else
	{
	    bgAutomaticPauseAllocation = FALSE;
	}

	sprintf(pclKeyword,"IGNORE_BREAKS");

	ReadConfigEntry(pclSection,pclKeyword,pclTmpText);
	ToUpper(pclTmpText);
	if(!strcmp(pclTmpText,"YES"))
	{
	    bgDefaultIgnoreBreaks = TRUE;
	}
	else
	{
	    bgDefaultIgnoreBreaks = FALSE;
	}



	sprintf(pclKeyword,"CHECKOVERLAP");

	ReadConfigEntry(pclSection,pclKeyword,pclTmpText);
	ToUpper(pclTmpText);
	if(!strcmp(pclTmpText,"YES"))
	{
	    bgCheckOverlap = TRUE;
	}
	else
	{
	    bgCheckOverlap = FALSE;
	}

	sprintf(pclKeyword,"CHECKALOC");

	bgCheckAlocation = FALSE;
	if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) == RC_SUCCESS)
	{
	    ToUpper(pclTmpText);
	    if(!strcmp(pclTmpText,"NO"))
	    {
		bgCheckAlocation = FALSE;
	    }
	    else
	    {
		bgCheckAlocation = TRUE;
	    }
	}


	sprintf(pclKeyword,"KLEBEFUNCTION");

	ReadConfigEntry(pclSection,pclKeyword,pclTmpText);
	ToUpper(pclTmpText);
	if(!strcmp(pclTmpText,"YES"))
	{
	    bgKlebeFunction = TRUE;
	}
	else
	{
	    bgKlebeFunction = FALSE;
	}

	sprintf(pclKeyword,"OVERLAPBUFFER");
	if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) == RC_SUCCESS)
	{
	    igOverlapBuffer  = atoi(pclTmpText) * 60;
	}
	else
	{
	    igOverlapBuffer  = 5 * 60;
	}

	sprintf(pclKeyword,"WORKLOADX");
	if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) == RC_SUCCESS)
	{
	    igWorkloadX  = atol(pclTmpText);
	}
	else
	{
	    igWorkloadX  = 100;
	}
	sprintf(pclKeyword,"WORKLOADY");
	if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) == RC_SUCCESS)
	{
	    igWorkloadY  = atol(pclTmpText);
	}
	else
	{
	    igWorkloadY  = 10;
	}
	sprintf(pclKeyword,"MINTIMEX");
	if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) == RC_SUCCESS)
	{
	    igMinTimeX  = atol(pclTmpText);
	}
	else
	{
	    igMinTimeX  = 30;
	}
	sprintf(pclKeyword,"MINTIMEY");
	if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) == RC_SUCCESS)
	{
	    igMinTimeY  = atol(pclTmpText);
	}
	else
	{
	    igMinTimeY  = 8;
	}
	sprintf(pclKeyword,"MINSUMX");
	if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) == RC_SUCCESS)
	{
	    igMinSumX  = atol(pclTmpText);
	}
	else
	{
	    igMinSumX  = 50;
	}
	sprintf(pclKeyword,"MINSUMY");
	if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) == RC_SUCCESS)
	{
	    igMinSumY  = atol(pclTmpText);
	}
	else
	{
	    igMinSumY  = 1;
	}
	sprintf(pclKeyword,"MINWAYX");
	if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) == RC_SUCCESS)
	{
	    igMinWayX  = atol(pclTmpText);
	}
	else
	{
	    dbg(TRACE,"InitJobhdl() Error reading [%s] %s in jobhdl.cfg -" ,pclSection,pclKeyword);
	    igMinWayX  = 100;
	}
	sprintf(pclKeyword,"MINWAYY");
	if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) == RC_SUCCESS)
	{
	    igMinWayY  = atol(pclTmpText);
	}
	else
	{
	    igMinWayY  = 15;
	}
	sprintf(pclKeyword,"MINQUALIX");
	if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) == RC_SUCCESS)
	{
	    igMinQualiX  = atol(pclTmpText);
	}
	else
	{
	    igMinQualiX  = 10;
	}
	sprintf(pclKeyword,"MINQUALIY");
	if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) == RC_SUCCESS)
	{
	    igMinQualiY  = atol(pclTmpText);
	}
	else
	{
	    igMinQualiY  = 0;
	}

	sprintf(pclKeyword,"MIN_JOB_DISTANCE");
	if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) == RC_SUCCESS)
	{
	    igDefaultMinJobDistance = atoi(pclTmpText);
	}
	else
	{
	    igDefaultMinJobDistance  = 0;
	}

	sprintf(pclKeyword,"JOB_END_BUFFER");
	if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) == RC_SUCCESS)
	{
	    igDefaultJobEndBuffer = atoi(pclTmpText);
	}
	else
	{
	    igDefaultJobEndBuffer  = 0;
	}

	sprintf(pclKeyword,"EVENDISTBOUND");
	if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) == RC_SUCCESS)
	{
	    igEvenDistBound = atoi(pclTmpText);
	}
	else
	{
	    igEvenDistBound  = 2;
	}

	sprintf(pclKeyword,"ADAPTIVE");
	if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) == RC_SUCCESS)
	{
	    ToUpper(pclTmpText);
	    if(!strcmp(pclTmpText,"YES"))
	    {
		bgUseAdaptive = TRUE;
	    }
	    else
	    {
		bgUseAdaptive = FALSE;
	    }

	}
	else
	{
	    bgUseAdaptive = FALSE;
	}

	sprintf(pclKeyword,"JOBTYPES");
	if(ReadConfigEntry(pclSection,pclKeyword,cgJobTypes) != RC_SUCCESS)
	{
	    strcpy(cgJobTypes,"AFT;AFT;FLT,FID;GATEAREA;GTA,FID;GAT;GAT,FID;CIC;CCI");
	}

	sprintf(pclKeyword,"MAXFUNCTION");
	if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) == RC_SUCCESS)
	{
	    igMaxFunction = atoi(pclTmpText);
	}

	sprintf(pclKeyword,"GRPMAXFUNCTION");
	if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) == RC_SUCCESS)
	{
	    igGrpMaxFunction = atoi(pclTmpText);
	}

	sprintf(pclKeyword,"NEWDISTFIELDS");
	if(ReadConfigEntry(pclSection,pclKeyword,cgNewDistFields) != RC_SUCCESS)
	{
	    strcpy(cgNewDistFields,"FLGS");
	}
	sprintf(pclKeyword,"OPENDEMSORTFIELDS");
	strcpy(cgOpenDemSortFields2,"OURI,OURO,TNAM,SECO,");
	strcpy(cgOpenDemSortDir2,"D,D,D,D,");
	strcpy(cgOpenDemSortFields3,"TCHD,SIX1,SIX2,");
	strcpy(cgOpenDemSortDir3,"D,D,D,");
	if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) != RC_SUCCESS)
	{
	    strcat(cgOpenDemSortFields2,"DEBE,DEDU");
	    strcat(cgOpenDemSortFields3,"DEBE,DEDU");
	}
	else
	{
	    strcat(cgOpenDemSortFields2,pclTmpText);
	    strcat(cgOpenDemSortFields3,pclTmpText);
	}

	sprintf(pclKeyword,"OPENDEMSORTDIR");
	if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) != RC_SUCCESS)
	{
	    strcat(cgOpenDemSortDir2,"D,D");
	    strcat(cgOpenDemSortDir3,"D,D");
	}
	else
	{
	    strcat(cgOpenDemSortDir2,pclTmpText);
	    strcat(cgOpenDemSortDir3,pclTmpText);
	}

	sprintf(pclKeyword,"SendUPDJOB");
	if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) != RC_SUCCESS)
	{
		dbg(TRACE,"InitDemhdl: not found: <%s> <%s> in <%s> take default",
			pclSection,pclKeyword,cgConfigFile);
	}
	else
	{
		if ( !strcmp ( pclTmpText, "YES" ) )
			bgSendUpdJob = TRUE;
	}

    }
    dbg(TRACE,"InitJobhdl() igAssignOffSetStart = %d",igAssignOffSetStart);
    dbg(TRACE,"InitJobhdl() igAssignOffSetEnd = %d",igAssignOffSetEnd);
    dbg(TRACE,"InitJobhdl() igLoadOffSetStart = %d",igLoadOffSetStart);
    dbg(TRACE,"InitJobhdl() igLoadOffSetEnd = %d",igLoadOffSetEnd);
    dbg(TRACE,"InitJobhdl() bgAutomaticPauseAllocation = %d",bgAutomaticPauseAllocation);
	
    dbg(TRACE,"InitJobhdl() bgCheckOverlap = %d",bgCheckOverlap);
    dbg(TRACE,"InitJobhdl() cgOpenDemSortDir2 = %s",cgOpenDemSortDir2);
    dbg(TRACE,"InitJobhdl() cgOpenDemSortFields2 = %s",cgOpenDemSortFields2);
    dbg(TRACE,"InitJobhdl() bgSendUpdJob = %d",bgSendUpdJob );
	
	
    if(ilRc == RC_SUCCESS)
    {
	sprintf(cgJobTab,"JOB%s",cgTabEnd);
	sprintf(cgDemTab,"DEM%s",cgTabEnd);
	sprintf(cgJodTab,"JOD%s",cgTabEnd);
	sprintf(cgRueTab,"RUE%s",cgTabEnd);
	sprintf(cgRudTab,"RUD%s",cgTabEnd);
	sprintf(cgSpeTab,"SPE%s",cgTabEnd);
	sprintf(cgSpfTab,"SPF%s",cgTabEnd);
	sprintf(cgPfcTab,"PFC%s",cgTabEnd);
	sprintf(cgPerTab,"PER%s",cgTabEnd);
	sprintf(cgJtyTab,"JTY%s",cgTabEnd);
	sprintf(cgDelTab,"DEL%s",cgTabEnd);
	sprintf(cgDrrTab,"DRR%s",cgTabEnd);
	sprintf(cgBsdTab,"BSD%s",cgTabEnd);
	sprintf(cgDrdTab,"DRD%s",cgTabEnd);
	sprintf(cgAloTab,"ALO%s",cgTabEnd);
	sprintf(cgAftTab,"AFT%s",cgTabEnd);
	sprintf(cgTplTab,"TPL%s",cgTabEnd);
	sprintf(cgSerTab,"SER%s",cgTabEnd);
	sprintf(cgRpqTab,"RPQ%s",cgTabEnd);
	sprintf(cgRpfTab,"RPF%s",cgTabEnd);
	sprintf(cgWayTab,"WAY%s",cgTabEnd);
	sprintf(cgPolTab,"POL%s",cgTabEnd);
	sprintf(cgSgrTab,"SGR%s",cgTabEnd);
	sprintf(cgSgmTab,"SGM%s",cgTabEnd);
	sprintf(cgSwgTab,"SWG%s",cgTabEnd);
	sprintf(cgDrgTab,"DRG%s",cgTabEnd);
	sprintf(cgDlgTab,"DLG%s",cgTabEnd);
	sprintf(cgParTab,"PAR%s",cgTabEnd);
	sprintf(cgWgrTab,"WGR%s",cgTabEnd);
    }
	
    if(ilRc == RC_SUCCESS)
    {
	ilRc = CEDAArrayInitialize(60,10);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: CEDAArrayInitialize failed <%d>",ilRc);
	}/* end of if */
    }/* end of if */
    if(ilRc == RC_SUCCESS)
    {

	tgLoadStart = tgCurrTime + igLoadOffSetStart;
	tgLoadEnd = tgCurrTime + igLoadOffSetEnd;
	TimeToStr(cgLoadStart,tgLoadStart);
	TimeToStr(clJobLoadStart,tgLoadStart-24*60*60);
	TimeToStr(cgLoadEnd,tgLoadEnd);
		
	MyStrnCpy ( clSdayStart, cgLoadStart, 8, 1 );
	MyStrnCpy ( clSdayEnd, cgLoadEnd, 8, 1 );
		
	GetFieldLength("RUD","MAXD",&(pllAddFieldLens[0]) );
	GetFieldLength("RUD","MIND",&(pllAddFieldLens[1]) );
	GetFieldLength("RUD","DIDE",&(pllAddFieldLens[2]) );

	GetFieldLength("TPL","TNAM",&llFieldLength );
	pllAddFieldLens[3] = llFieldLength;
	GetFieldLength("SER","SECO",&(pllAddFieldLens[4]) );
	pllAddFieldLens[5] = 4;
	pllAddFieldLens[6] = 4;
	pllAddFieldLens[7] = 3;
	GetFieldLength("SER","RTWF",&(pllAddFieldLens[8]) );
	GetFieldLength("SER","RTWF",&(pllAddFieldLens[9]) );
	GetFieldLength("ALO","URNO",&(pllAddFieldLens[10]) );
	GetFieldLength("RUD","ULNK",&(pllAddFieldLens[11]) );
	GetFieldLength("RUD","UPDE",&(pllAddFieldLens[12]) );
	GetFieldLength("RUD","UNDE",&(pllAddFieldLens[13]) );
	GetFieldLength("RUD","URUE",&(pllAddFieldLens[14]) );
	GetFieldLength("RUE","UTPL",&(pllAddFieldLens[15])  );
	pllAddFieldLens[16] = 0;

	ilRc = SetArrayInfo(cgTplTab,cgTplTab,TPL_FIELDS,NULL,NULL,
			    "TPLIDX1",TPL_SORTFIELDS,TPL_SORTDIR,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    &rgTplArray,NULL,TRUE,TRUE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetArrayInfo TPLTAB failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgTplArray);
	    dbg(TRACE,"InitJobhdl: SetArrayInfo TPLTAB OK");
	}/* end of if */

				
	ilRc = SetArrayInfo("DEMOPEN",cgDemTab,DEM_FIELDS,OPENDEM_ADDFIELDS,pllAddFieldLens,
			    "OPENDEMIDX1",OPENDEM_SORTFIELDS,OPENDEM_SORTDIR,
			    "OPENDEMIDX2",cgOpenDemSortFields2,cgOpenDemSortDir2,
			    "OPENDEMIDX3",cgOpenDemSortFields3,cgOpenDemSortDir3,
			    "OPENDEMIDX4",OPENDEM_SORTFIELDS4,OPENDEM_SORTDIR4,
			    "OPENDEMIDX5",OPENDEM_SORTFIELDS5,OPENDEM_SORTDIR5,
			    "OPENDEMIDX6",OPENDEM_SORTFIELDS6,OPENDEM_SORTDIR6,
			    "OPENDEMIDX7",OPENDEM_SORTFIELDS7,OPENDEM_SORTDIR7,
			    NULL,NULL,NULL,
			    &rgOpenDemArray,NULL,FALSE,FALSE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetArrayInfo OPENDEM failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgOpenDemArray);

	    llRowNum = ARR_FIRST;
		
	    dbg(TRACE,"InitJobhdl: SetArrayInfo OPENDEM OK");
	}/* end of if */
	if(ilRc == RC_SUCCESS)
	{
	    ilRc = TriggerAction(cgDemTab,"URT,DRT"); 	
	}
	if(ilRc != RC_SUCCESS) 
	{ 
	    dbg(TRACE,"SetArrayInfo: TriggerAction DEMTAB failed <%d>",ilRc); 
	}/* end of if */ 
		
	ilRc = SetArrayInfo("REDEMOPEN",cgDemTab,DEM_FIELDS,OPENDEM_ADDFIELDS,pllAddFieldLens,
			    "REOPENDEMIDX1",OPENDEM_SORTFIELDS,OPENDEM_SORTDIR,
			    "REOPENDEMIDX2",cgOpenDemSortFields2,cgOpenDemSortDir2,
			    "REOPENDEMIDX3",cgOpenDemSortFields3,cgOpenDemSortDir3,
			    "REOPENDEMIDX4",OPENDEM_SORTFIELDS4,OPENDEM_SORTDIR4,
			    "REOPENDEMIDX5",OPENDEM_SORTFIELDS5,OPENDEM_SORTDIR5,
			    "REOPENDEMIDX6",OPENDEM_SORTFIELDS6,OPENDEM_SORTDIR6,
			    "REOPENDEMIDX7",OPENDEM_SORTFIELDS7,OPENDEM_SORTDIR7,
			    NULL,NULL,NULL,
			    &rgRequestOpenDemArray,NULL,FALSE,FALSE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetArrayInfo REOPENDEM failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgRequestOpenDemArray);

	    CEDAArraySendChanges2BCHDL(&rgRequestOpenDemArray.rrArrayHandle,rgRequestOpenDemArray.crArrayName,TRUE);
            CEDAArraySendChanges2ACTION(&rgRequestOpenDemArray.rrArrayHandle,rgRequestOpenDemArray.crArrayName,TRUE);

	    llRowNum = ARR_FIRST;
		
	    dbg(TRACE,"InitJobhdl: SetArrayInfo REOPENDEM OK");
	}/* end of if */


	ilRc = SetArrayInfo(cgJtyTab,cgJtyTab,JTY_FIELDS,NULL,NULL,
			    "JTYIDX1",JTY_SORTFIELDS,JTY_SORTDIR,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    &rgJtyArray,NULL,TRUE,TRUE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetArrayInfo JTYTAB failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgJtyArray);
	    dbg(TRACE,"InitJobhdl: SetArrayInfo JTYTAB OK");
	}/* end of if */

/*Hier wird die Urno f�r Jobtype == POL gestezt*/


	strcpy(clKey,"POL");
	llRowNum = ARR_FIRST;
	if(CEDAArrayFindRowPointer(&(rgJtyArray.rrArrayHandle),
				   &(rgJtyArray.crArrayName[0]),
				   &(rgJtyArray.rrIdx01Handle),
				   &(rgJtyArray.crIdx01Name[0]),
				   clKey,&llRowNum,
				   (void *) &pclJtyRow ) == RC_SUCCESS)
	{
	    lgUjtyPol = atol(JTYFIELD(pclJtyRow,ilJtyUrnoIdx));
	}
	else
	{
	    dbg(TRACE,"InitJobhdl: error lgUjtyPol not initialised");
	}
		
	strcpy(clKey,"BRK");

	llRowNum = ARR_FIRST;
	if(CEDAArrayFindRowPointer(&(rgJtyArray.rrArrayHandle),
				   &(rgJtyArray.crArrayName[0]),
				   &(rgJtyArray.rrIdx01Handle),
				   &(rgJtyArray.crIdx01Name[0]),
				   clKey,&llRowNum,
				   (void *) &pclJtyRow ) == RC_SUCCESS)
	{
	    lgUjtyBrk = atol(JTYFIELD(pclJtyRow,ilJtyUrnoIdx));
	}
	else
	{
	    dbg(TRACE,"InitJobhdl: error lgUjtyBrk not initialised");
	}

	strcpy(clKey,"DET");

	llRowNum = ARR_FIRST;
	if(CEDAArrayFindRowPointer(&(rgJtyArray.rrArrayHandle),
				   &(rgJtyArray.crArrayName[0]),
				   &(rgJtyArray.rrIdx01Handle),
				   &(rgJtyArray.crIdx01Name[0]),
				   clKey,&llRowNum,
				   (void *) &pclJtyRow ) == RC_SUCCESS)
	{
	    lgUjtyDet = atol(JTYFIELD(pclJtyRow,ilJtyUrnoIdx));
	}
	else
	{
	    dbg(TRACE,"InitJobhdl: error lgUjtyDet not initialised");
	}

	strcpy(clKey,"DEL");

	llRowNum = ARR_FIRST;
	if(CEDAArrayFindRowPointer(&(rgJtyArray.rrArrayHandle),
				   &(rgJtyArray.crArrayName[0]),
				   &(rgJtyArray.rrIdx01Handle),
				   &(rgJtyArray.crIdx01Name[0]),
				   clKey,&llRowNum,
				   (void *) &pclJtyRow ) == RC_SUCCESS)
	{
	    lgUjtyDel = atol(JTYFIELD(pclJtyRow,ilJtyUrnoIdx));
	}
	else
	{
	    dbg(TRACE,"InitJobhdl: error lgUjtyDel not initialised");
	}


	strcpy(clKey,"FLT");

	llRowNum = ARR_FIRST;

	if(CEDAArrayFindRowPointer(&(rgJtyArray.rrArrayHandle),
				   &(rgJtyArray.crArrayName[0]),
				   &(rgJtyArray.rrIdx01Handle),
				   &(rgJtyArray.crIdx01Name[0]),
				   clKey,&llRowNum,
				   (void *) &pclJtyRow ) == RC_SUCCESS)
	{
	    lgUjtyFlt = atol(JTYFIELD(pclJtyRow,ilJtyUrnoIdx));
	}
	else
	{
	    dbg(TRACE,"InitJobhdl: error lgUjtyFlt not initialised");
	}

	dbg(TRACE,"InitJobhdl: SetArrayInfo JOBTAB start");
	/* sprintf(pclSelection,"WHERE UJTY != '%ld' AND ACTO > '%s' AND ACFR < '%s' AND HOPO = '%s'",lgUjtyPol,cgLoadStart,cgLoadEnd,cgHopo); */
	sprintf(pclSelection,"WHERE UJTY != '%ld' AND (ACFR BETWEEN '%s' AND '%s') AND HOPO = '%s'",lgUjtyPol,clJobLoadStart,cgLoadEnd,cgHopo);
		
	dbg(TRACE,"InitJobhdl: JOB Selection <%s>",pclSelection);

	GetFieldLength("JOB","URNO",&(pllAddFieldLens[0]) );
	GetFieldLength("ALO","ALOC",&(pllAddFieldLens[1]) );
	pllAddFieldLens[2] = 5;
	pllAddFieldLens[3] = 0;

	strcpy ( cgJobFields, JOB_FIELDS );
	if ( GetFieldLength( "JOB", "UTPL", &llFieldLength ) == RC_SUCCESS )
		strcat ( cgJobFields, ",UTPL" );
	else
		dbg ( TRACE,"InitJobhdl: field JOBTAB.UTPL not in DB configuration" );
	
	ilRc = SetArrayInfo(cgJobTab,cgJobTab,cgJobFields,JOB_ADDFIELDS,pllAddFieldLens,
						"JOBIDX1",JOB_SORTFIELDS,JOB_SORTDIR,
						"JOBIDX2",JOB_SORTFIELDS2,JOB_SORTDIR2,
						"JOBIDX3",JOB_SORTFIELDS3,JOB_SORTDIR3,				
						"JOBIDX4",JOB_SORTFIELDS4,JOB_SORTDIR4,				
						"JOBIDX5",JOB_SORTFIELDS5,JOB_SORTDIR5,				
						NULL,NULL,NULL,
						NULL,NULL,NULL,
						NULL,NULL,NULL,
						&rgJobArray,pclSelection,TRUE,TRUE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetArrayInfo JOBTAB failed <%d>",ilRc);
	}
	else
	{
	    DebugPrintArrayInfo(DEBUG,&rgJobArray);                
	    CEDAArraySendChanges2BCHDL(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,TRUE);
        CEDAArraySendChanges2ACTION(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,TRUE);
	    dbg(TRACE,"InitJobhdl: SetArrayInfo JOBTAB OK");
	}/* end of if */
	strcat ( cgJobFields, "," );
	strcat ( cgJobFields, JOB_ADDFIELDS );

	sprintf(pclSelection, "((UJTY != %ld)  && (HOPO == %s)) ",lgUjtyPol,cgHopo);

	ilRc = CEDAArraySetFilter(&rgJobArray.rrArrayHandle,
				  rgJobArray.crArrayName,pclSelection);

	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: CEDAArraySetFilter JOBTAB failed <%d>",ilRc);
	}

	/*  Now cgJobFields is set, so we can call InitFieldIndex */
	InitFieldIndex();

	dbg(TRACE,"InitJobhdl: SetArrayInfo JODTAB start");

	sprintf(pclSelection,"WHERE CDAT >= '%s' AND HOPO = '%s'",cgLoadStart,cgHopo);
	
	pllAddFieldLens[0] = 3;

	ilRc = SetArrayInfo(cgJodTab,cgJodTab,JOD_FIELDS,JOD_ADDFIELDS,pllAddFieldLens,
			    "JODIDX1",JOD_SORTFIELDS,JOD_SORTDIR,
			    "JODIDX2",JOD_SORTFIELDS2,JOD_SORTDIR2,						
			    "JODIDX3",JOD_SORTFIELDS3,JOD_SORTDIR3,						
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    &rgJodArray,pclSelection,TRUE,TRUE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetArrayInfo JODTAB failed <%d>",ilRc);
	}
	else
	{
	    DebugPrintArrayInfo(DEBUG,&rgJodArray);
	    CEDAArraySendChanges2BCHDL(&rgJodArray.rrArrayHandle,rgJodArray.crArrayName,TRUE);
        CEDAArraySendChanges2ACTION(&rgJodArray.rrArrayHandle,rgJodArray.crArrayName,TRUE);

	    dbg(TRACE,"InitJobhdl: SetArrayInfo JODTAB OK");
	}/* end of if */
	dbg(DEBUG,"InitJobhdl: SetArrayInfo RUETAB start");

	ilRc = SetArrayInfo(cgRueTab,cgRueTab,RUE_FIELDS,NULL,NULL,
			    "RUEIDX1",RUE_SORTFIELDS,RUE_SORTDIR,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    &rgRueArray,pclSelection,TRUE,TRUE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetArrayInfo RUETAB failed <%d>",ilRc);
	}
	else
	{
	    DebugPrintArrayInfo(DEBUG,&rgRueArray);
	    dbg(TRACE,"InitJobhdl: SetArrayInfo RUETAB OK");
	}/* end of if */
		
	sprintf(pclSelection,"WHERE URUE IN (SELECT URNO FROM RUETAB WHERE RUST!='2') OR URNO IN (SELECT DISTINCT URUD FROM DEMTAB WHERE DEBE BETWEEN '%s' AND '%s')",clJobLoadStart,cgLoadEnd);

	ilRc = SetArrayInfo(cgRudTab,cgRudTab,RUD_FIELDS,NULL,NULL,
			    "RUDIDX1",RUD_SORTFIELDS,RUD_SORTDIR,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    &rgRudArray,pclSelection,TRUE,TRUE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetArrayInfo RUDTAB failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgRudArray);
	    dbg(TRACE,"InitJobhdl: SetArrayInfo RUDTAB OK");
	}/* end of if */


	pllAddFieldLens[0] = 4;
	pllAddFieldLens[1] = 1;
	pllAddFieldLens[2] = 1;
	pllAddFieldLens[3] = 20;
	pllAddFieldLens[4] = 10;
	pllAddFieldLens[5] = 10;
	pllAddFieldLens[6] = 10;
	pllAddFieldLens[7] = 10;
	pllAddFieldLens[8] = 10;
	GetFieldLength("JOB","URNO",&(pllAddFieldLens[9]) );
	GetFieldLength("JOB","URNO",&(pllAddFieldLens[10]) );
	/*GetFieldLength("JOB","ACFR",&(pllAddFieldLens[11]) );
	GetFieldLength("JOB","ACTO",&(pllAddFieldLens[12]) );*/
	GetFieldLength("DRR","SDAY",&(pllAddFieldLens[11]) );
	GetFieldLength("SWG","CODE",&(pllAddFieldLens[12]) );
	pllAddFieldLens[13] = 0;
		
	/* sprintf(pclSelection,"WHERE UJTY = '%ld' AND ACTO > '%s' AND ACFR < '%s' AND HOPO = '%s'",lgUjtyPol,cgLoadStart,cgLoadEnd,cgHopo); */
	sprintf(pclSelection,"WHERE UJTY = '%ld' AND (ACFR BETWEEN '%s' AND '%s') AND HOPO = '%s'",lgUjtyPol,clJobLoadStart,cgLoadEnd,cgHopo);

	dbg(TRACE,"InitJobhdl: POOLJOB Selection <%s>",pclSelection);
	ilRc = SetArrayInfo("JOBPOOL",cgJobTab,JOB_FIELDS,POOLJOB_ADDFIELDS,pllAddFieldLens,
			    "POOLJOBIDX1",POOLJOB_SORTFIELDS,POOLJOB_SORTDIR,
			    "POOLJOBIDX2",POOLJOB_SORTFIELDS2,POOLJOB_SORTDIR2,
			    NULL,NULL,NULL,  /*"POOLJOBIDX3",POOLJOB_SORTFIELDS3,POOLJOB_SORTDIR3,*/
			    "POOLJOBIDX4",POOLJOB_SORTFIELDS4,POOLJOB_SORTDIR4,	
			    NULL,NULL,NULL,  /*"POOLJOBIDX5",POOLJOB_SORTFIELDS5,POOLJOB_SORTDIR5,*/
			    "POOLJOBIDX6",POOLJOB_SORTFIELDS6,POOLJOB_SORTDIR6,									
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    &rgPoolJobArray,pclSelection,TRUE,FALSE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetArrayInfo POOLJOB failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgPoolJobArray);
	    dbg(TRACE,"InitJobhdl: SetArrayInfo POOLJOB OK");
	}/* end of if */

	sprintf(pclSelection,
		"((UJTY == %ld)  && (HOPO == %s)) ",lgUjtyPol,cgHopo);
	ilRc = CEDAArraySetFilter(&rgPoolJobArray.rrArrayHandle,
				  rgPoolJobArray.crArrayName,pclSelection);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: CEDAArraySetFilter POOLJOB failed <%d>",ilRc);
	}

				
	ilRc = SetArrayInfo("REJOBPOOL",cgJobTab,JOB_FIELDS,POOLJOB_ADDFIELDS,pllAddFieldLens,
			    "REPOOLJOBIDX1",POOLJOB_SORTFIELDS,POOLJOB_SORTDIR,
			    "REPOOLJOBIDX2",POOLJOB_SORTFIELDS2,POOLJOB_SORTDIR2,
			    NULL,NULL,NULL,  /*"REPOOLJOBIDX3",POOLJOB_SORTFIELDS3,POOLJOB_SORTDIR3,*/
			    "REPOOLJOBIDX4",POOLJOB_SORTFIELDS4,POOLJOB_SORTDIR4,	
			    NULL,NULL,NULL,  /*"REPOOLJOBIDX5",POOLJOB_SORTFIELDS5,POOLJOB_SORTDIR5,*/
			    "REPOOLJOBIDX6",POOLJOB_SORTFIELDS6,POOLJOB_SORTDIR6,	
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    &rgRequestPoolJobArray,NULL,FALSE,FALSE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetArrayInfo REPOOLJOB failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgRequestPoolJobArray);
	    dbg(TRACE,"InitJobhdl: SetArrayInfo REPOOLJOB OK");
	}/* end of if */

	sprintf(pclSelection,
		"((UJTY == %ld)  && (HOPO == %s)) ",lgUjtyPol,cgHopo);
	ilRc = CEDAArraySetFilter(&rgPoolJobArray.rrArrayHandle,
				  rgPoolJobArray.crArrayName,pclSelection);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: CEDAArraySetFilter POOLJOB failed <%d>",ilRc);
	}

	sprintf(pclSelection,"WHERE VPFR < '%s' AND ((VPTO > '%s') OR (VPTO = ' ')) AND HOPO = '%s'",cgLoadEnd,cgLoadStart,cgHopo);
	dbg(TRACE,"InitJobhdl: SPE Selection <%s>",pclSelection);

	ilRc = SetArrayInfo(cgSpeTab,cgSpeTab,SPE_FIELDS,NULL,NULL,
			    "SPEIDX1",SPE_SORTFIELDS,SPE_SORTDIR,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    &rgSpeArray,NULL,TRUE,TRUE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetArrayInfo SPETAB failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgSpeArray);
	    dbg(TRACE,"InitJobhdl: SetArrayInfo SPETAB OK");
	}/* end of if */
		
	dbg(TRACE,"InitJobhdl: SPF Selection <%s>",pclSelection);

	ilRc = SetArrayInfo(cgSpfTab,cgSpfTab,SPF_FIELDS,NULL,NULL,
			    "SPFIDX1",SPF_SORTFIELDS,SPF_SORTDIR,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    &rgSpfArray,pclSelection,TRUE,TRUE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetArrayInfo SPFTAB failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgSpfArray);
	    dbg(TRACE,"InitJobhdl: SetArrayInfo SPFTAB OK");
	}/* end of if */

	ilRc = SetArrayInfo(cgPfcTab,cgPfcTab,PFC_FIELDS,NULL,NULL,
			    "PFCIDX1",PFC_SORTFIELDS,PFC_SORTDIR,
			    "PFCIDX2",PFC_SORTFIELDS2,PFC_SORTDIR2,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    &rgPfcArray,NULL,TRUE,TRUE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetArrayInfo PFCTAB failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgPfcArray);
	    dbg(TRACE,"InitJobhdl: SetArrayInfo PFCTAB OK");
	}/* end of if */

	ilRc = SetArrayInfo(cgPerTab,cgPerTab,PER_FIELDS,NULL,NULL,
			    "PERIDX1",PER_SORTFIELDS,PER_SORTDIR,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    &rgPerArray,NULL,TRUE,TRUE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetArrayInfo PERTAB failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgPerArray);
	    dbg(TRACE,"InitJobhdl: SetArrayInfo PERTAB OK");
	}/* end of if */

	ilRc = SetArrayInfo(cgDelTab,cgDelTab,DEL_FIELDS,NULL,NULL,
			    "DELIDX1",DEL_SORTFIELDS,DEL_SORTDIR,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    &rgDelArray,NULL,TRUE,TRUE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetArrayInfo DELTAB failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgDelArray);
	    dbg(TRACE,"InitJobhdl: SetArrayInfo DELTAB OK");
	}/* end of if */

	/* sprintf(pclSelection,"WHERE AVTO >= '%s' AND AVFR <= '%s' AND HOPO = '%s' AND ROSL = '3' AND ROSS != 'L'",
			cgLoadStart,cgLoadEnd,cgHopo); original selection replaced by */

	sprintf(pclSelection,"WHERE (SDAY BETWEEN '%s' AND '%s') AND HOPO = '%s' AND ROSL = '3' AND ROSS != 'L'",
			clSdayStart,clSdayEnd,cgHopo);
	dbg(TRACE,"InitJobhdl: DRRTAB Selection <%s>",pclSelection);

	ilRc = SetArrayInfo(cgDrrTab,cgDrrTab,DRR_FIELDS,NULL,NULL,
			    "DRRIDX1",DRR_SORTFIELDS,DRR_SORTDIR,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    &rgDrrArray,pclSelection,TRUE,TRUE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetArrayInfo DRRTAB failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgDrrArray);
	    dbg(TRACE,"InitJobhdl: SetArrayInfo DRRTAB OK");
	}/* end of if */

	/*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/
	if(ilRc == RC_SUCCESS)
	{
		sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);
		ilRc = SetArrayInfo("NEWDRR","DRRTAB",DRR_FIELDS,NULL,NULL,
							NULL,NULL,NULL, /*"DRRIDX1",DRR_SORTFIELDS,DRR_SORTDIR,  hag27.08.2003 unused */
							NULL,NULL,NULL,
							NULL,NULL,NULL,
							NULL,NULL,NULL,
							NULL,NULL,NULL,
							NULL,NULL,NULL,
							NULL,NULL,NULL,
							NULL,NULL,NULL,
							&rgNewDrrArray,pclSelection,FALSE, FALSE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitPolhdl: SetArrayInfo NEWDRR failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"InitPolhdl: SetArrayInfo NEWDRR OK");
		}/* end of if */
	}
	/*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/

	sprintf(pclSelection,"WHERE (VATO >= '%s' OR VATO = ' ' ) AND VAFR <= '%s' AND HOPO = '%s'",cgLoadStart,cgLoadEnd,cgHopo);
	dbg(TRACE,"InitJobhdl: BSDTAB Selection <%s>",pclSelection);

	ilRc = SetArrayInfo(cgBsdTab,cgBsdTab,BSD_FIELDS,NULL,NULL,
			    "BSDIDX1",BSD_SORTFIELDS,BSD_SORTDIR,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    &rgBsdArray,pclSelection,TRUE,TRUE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetArrayInfo BSDTAB failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgBsdArray);
	    dbg(TRACE,"InitJobhdl: SetArrayInfo BSDTAB OK");
	}/* end of if */

	sprintf(pclSelection,"WHERE DRDT >= '%s' AND DRDF <= '%s' AND HOPO = '%s'",cgLoadStart,cgLoadEnd,cgHopo);
	dbg(TRACE,"InitJobhdl: DRDTAB Selection <%s>",pclSelection);
	ilRc = SetArrayInfo(cgDrdTab,cgDrdTab,DRD_FIELDS,NULL,NULL,
			    "DRDIDX1",DRD_SORTFIELDS,DRD_SORTDIR,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    &rgDrdArray,pclSelection,TRUE,TRUE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetArrayInfo DRDTAB failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgDrdArray);
	    dbg(TRACE,"InitJobhdl: SetArrayInfo DRDTAB OK");
	}/* end of if */

	ilRc = SetArrayInfo(cgAloTab,cgAloTab,ALO_FIELDS,NULL,NULL,
			    "ALOIDX1",ALO_SORTFIELDS,ALO_SORTDIR,
			    "ALOIDX2",ALO_SORTFIELDS2,ALO_SORTDIR2,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    &rgAloArray,NULL,TRUE,TRUE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetArrayInfo ALOTAB failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgAloArray);
	    dbg(TRACE,"InitJobhdl: SetArrayInfo ALOTAB OK");
	}/* end of if */

	strcpy(clKey,"POOL");
	llRowNum = ARR_FIRST;
	if(CEDAArrayFindRowPointer(&(rgAloArray.rrArrayHandle),
				   &(rgAloArray.crArrayName[0]),
				   &(rgAloArray.rrIdx02Handle),
				   &(rgAloArray.crIdx02Name[0]),
				   clKey,&llRowNum,
				   (void *) &pclAloRow ) == RC_SUCCESS)
	{
	    lgUpolAlo = atol(ALOFIELD(pclAloRow,igAloUrno));
	}
	else
	{
	    dbg(TRACE,"InitJobhdl: error lgUpolAlo not initialised");
	}
		


	/* sprintf(pclSelection,"WHERE ((TIFA >= '%s' AND TIFA <= '%s' AND DES3 = '%s') OR (TIFD >= '%s' AND TIFD <= '%s' AND ORG3 = '%s'))",cgLoadStart,cgLoadEnd,cgHopo,cgLoadStart,cgLoadEnd,cgHopo); */
	sprintf(pclSelection,"WHERE (((TIFA BETWEEN '%s' AND '%s') AND DES3 = '%s') OR ((TIFD BETWEEN '%s' AND '%s') AND ORG3 = '%s'))",cgLoadStart,cgLoadEnd,cgHopo,cgLoadStart,cgLoadEnd,cgHopo);

	ilRc = SetArrayInfo(cgAftTab,cgAftTab,AFT_FIELDS,NULL,NULL,
			    "AFTIDX1",AFT_SORTFIELDS,AFT_SORTDIR,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    &rgAftArray,pclSelection,TRUE,TRUE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetArrayInfo AFTTAB failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgAftArray);
	    dbg(TRACE,"InitJobhdl: SetArrayInfo AFTTAB OK");
	}/* end of if */

	sprintf(pclSelection,
		"(((TIFA >= %s)  && (TIFA <= %s)  && (DES3 == %s)) || ((TIFD >= %s)  && (TIFD <= %s)  && (ORG3 == %s))) ",cgLoadStart,cgLoadEnd,cgHopo,cgLoadStart,cgLoadEnd,cgHopo);
	ilRc = CEDAArraySetFilter(&rgAftArray.rrArrayHandle,
				  rgAftArray.crArrayName,pclSelection);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: CEDAArraySetFilter AFTTAB failed <%d>",ilRc);
	}


	sprintf(pclSelection,"WHERE (VATO >= '%s' OR VATO = ' ' ) AND VAFR <= '%s' AND HOPO = '%s'",cgLoadStart,cgLoadEnd,cgHopo);
	dbg(TRACE,"InitJobhdl: SERTAB Selection <%s>",pclSelection);
	ilRc = SetArrayInfo(cgSerTab,cgSerTab,SER_FIELDS,NULL,NULL,
			    "SERIDX1",SER_SORTFIELDS,SER_SORTDIR,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    &rgSerArray,pclSelection,TRUE,TRUE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetArrayInfo SERTAB failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgSerArray);
	    dbg(TRACE,"InitJobhdl: SetArrayInfo SERTAB OK");
	}/* end of if */

	sprintf ( pclSelection, "where urud in (select URNO FROM RUDTAB WHERE URUE IN (Select urno from ruetab where rust!='2') or urno in (SELECT DISTINCT URUD FROM DEMTAB WHERE DEBE BETWEEN '%s' AND '%s'))",
			  clJobLoadStart, cgLoadEnd);
	ilRc = SetArrayInfo(cgRpfTab,cgRpfTab,RPF_FIELDS,NULL,NULL,
			    "RPFIDX1",RPF_SORTFIELDS,RPF_SORTDIR,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    &rgRpfArray,pclSelection,TRUE,TRUE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetArrayInfo RPFTAB failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgRpfArray);
	    dbg(TRACE,"InitJobhdl: SetArrayInfo RPFTAB OK");
	}/* end of if */

	ilRc = SetArrayInfo(cgRpqTab,cgRpqTab,RPQ_FIELDS,NULL,NULL,
			    "RPQIDX1",RPQ_SORTFIELDS,RPQ_SORTDIR,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    &rgRpqArray,pclSelection,TRUE,TRUE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetArrayInfo RPQTAB failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgRpqArray);
	    dbg(TRACE,"InitJobhdl: SetArrayInfo RPQTAB OK");
	}/* end of if */

	ilRc = SetArrayInfo(cgWayTab,cgWayTab,WAY_FIELDS,NULL,NULL,
			    "WAYIDX1",WAY_SORTFIELDS,WAY_SORTDIR,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    &rgWayArray,NULL,TRUE,TRUE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetArrayInfo WAYTAB failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgWayArray);
	    dbg(TRACE,"InitJobhdl: SetArrayInfo WAYTAB OK");
	}/* end of if */

	ilRc = SetArrayInfo(cgPolTab,cgPolTab,POL_FIELDS,NULL,NULL,
			    "POLIDX1",POL_SORTFIELDS,POL_SORTDIR,
			    "POLIDX2",POL_SORTFIELDS2,POL_SORTDIR2,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    &rgPolArray,NULL,TRUE,TRUE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetArrayInfo POLTAB failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgPolArray);
	    dbg(TRACE,"InitJobhdl: SetArrayInfo POLTAB OK");
	}/* end of if */


	ilRc = SetArrayInfo(cgSgrTab,cgSgrTab,SGR_FIELDS,NULL,NULL,
			    "SGRIDX1",SGR_SORTFIELDS,SGR_SORTDIR,
			    "SGRIDX2",SGR_SORTFIELDS2,SGR_SORTDIR2,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    &rgSgrArray,NULL,TRUE,TRUE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetArrayInfo SGRTAB failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgSgrArray);
	    dbg(TRACE,"InitJobhdl: SetArrayInfo SGRTAB OK");
	}/* end of if */

	ilRc = SetArrayInfo(cgSgmTab,cgSgmTab,SGM_FIELDS,NULL,NULL,
			    "SGMIDX1",SGM_SORTFIELDS,SGM_SORTDIR,
			    "SGMIDX2",SGM_SORTFIELDS2,SGM_SORTDIR2,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    &rgSgmArray,NULL,TRUE,TRUE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetArrayInfo SGMTAB failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgSgmArray);
	    dbg(TRACE,"InitJobhdl: SetArrayInfo SGMTAB OK");
	}/* end of if */


		
	sprintf(pclSelection,"WHERE (APPL = 'JOBHDL' AND HOPO = '%s')",cgHopo);

	dbg(TRACE,"InitJobhdl: PARTAB Selection <%s>",pclSelection);
	ilRc = SetArrayInfo(cgParTab,cgParTab,PAR_FIELDS,NULL,NULL,
			    "PARIDX1",PAR_SORTFIELDS,PAR_SORTDIR,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    &rgParArray,pclSelection,TRUE,TRUE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetArrayInfo PARTAB failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgParArray);
	    dbg(TRACE,"InitJobhdl: SetArrayInfo PARTAB OK");
	}/* end of if */

	sprintf(pclSelection,
		"((APPL == JOBHDL) && (HOPO == %s)) ",cgHopo);
	ilRc = CEDAArraySetFilter(&rgParArray.rrArrayHandle,
				  rgParArray.crArrayName,pclSelection);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: CEDAArraySetFilter PARTAB failed <%d>",ilRc);
	}

	sprintf(pclSelection,"WHERE (VPTO >= '%s' OR VPTO = ' ' ) AND VPFR <= '%s' AND HOPO = '%s'",cgLoadStart,cgLoadEnd,cgHopo);
	dbg(TRACE,"InitJobhdl: SWGTAB Selection <%s>",pclSelection);
	ilRc = SetArrayInfo(cgSwgTab,cgSwgTab,SWG_FIELDS,NULL,NULL,
			    "SWGIDX1",SWG_SORTFIELDS,SWG_SORTDIR,
			    NULL,NULL,NULL, /* "SWGIDX2",SWG_SORTFIELDS2,SWG_SORTDIR2,*/
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    &rgSwgArray,pclSelection,TRUE,TRUE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetArrayInfo SWGTAB failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgSwgArray);
	    dbg(TRACE,"InitJobhdl: SetArrayInfo SWGTAB OK");
	}/* end of if */

	sprintf(pclSelection,"WHERE SDAY >= '%s' AND SDAY <= '%s'  AND HOPO = '%s'",clSdayStart,clSdayEnd,cgHopo);
	dbg(TRACE,"InitJobhdl: DRGTAB Selection <%s>",pclSelection);
	ilRc = SetArrayInfo(cgDrgTab,cgDrgTab,DRG_FIELDS,NULL,NULL,
			    "DRGIDX1",DRG_SORTFIELDS,DRG_SORTDIR,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    &rgDrgArray,pclSelection,TRUE,TRUE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetArrayInfo DRGTAB failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgDrgArray);
	    dbg(TRACE,"InitJobhdl: SetArrayInfo DRGTAB OK");
	}/* end of if */


	sprintf(pclSelection,"WHERE SDAY >= '%s' AND SDAY <= '%s'  AND HOPO = '%s'",clSdayStart,clSdayEnd,cgHopo);
	dbg(TRACE,"InitJobhdl: DLGTAB Selection <%s>",pclSelection);
	ilRc = SetArrayInfo(cgDlgTab,cgDlgTab,DLG_FIELDS,NULL,NULL,
			    "DLGIDX1",DLG_SORTFIELDS,DLG_SORTDIR,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    &rgDlgArray,pclSelection,TRUE,TRUE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetArrayInfo DLGTAB failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgDlgArray);
	    dbg(TRACE,"InitJobhdl: SetArrayInfo DLGTAB OK");
	}


	ilRc = SetArrayInfo(cgWgrTab,cgWgrTab,WGR_FIELDS,NULL,NULL,
			    "WGRIDX1",WGR_SORTFIELDS,WGR_SORTDIR,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    NULL,NULL,NULL,
			    &rgWgrArray,NULL,TRUE,TRUE);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetArrayInfo WGRTAB failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgWgrArray);
	    dbg(TRACE,"InitJobhdl: SetArrayInfo WGRTAB OK");
	}/* end of if */



	/* AATArrays*/

		
	ilRc = GetFieldLength(cgRpfTab,"FCCO",&llFieldLength);
	pllAddFieldLens[0] = llFieldLength;
	ilRc = GetFieldLength(cgDemTab,"URUD",&llFieldLength);
	pllAddFieldLens[1] = llFieldLength;
	pllAddFieldLens[2] = 4;
	pllAddFieldLens[3] = llFieldLength;
	pllAddFieldLens[4] = 0;

	ilRc = SetAATArrayInfo("DEMPFC",DEMPFC_FIELDS,pllAddFieldLens,"DEMPFCIDX1",DEMPFC_SORTFIELDS,DEMPFC_SORTDIR,NULL,NULL,NULL,NULL,NULL,NULL,&rgDemPfcArray);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetAATArrayInfo DEMPFC failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgDemPfcArray);
	    dbg(TRACE,"InitJobhdl: SetAATArrayInfo DEMPFC OK");
	}/* end of if */


	ilRc = GetFieldLength(cgRpqTab,"QUCO",&llFieldLength);
	pllAddFieldLens[1] = llFieldLength;
	ilRc = GetFieldLength(cgDemTab,"URUD",&llFieldLength);
	pllAddFieldLens[0] = llFieldLength;
	pllAddFieldLens[2] = 4;
	pllAddFieldLens[3] = llFieldLength;
	pllAddFieldLens[4] = 0;

	ilRc = SetAATArrayInfo("DEMPRQ",DEMPRQ_FIELDS,pllAddFieldLens,"DEMPRQIDX1",DEMPRQ_SORTFIELDS,DEMPRQ_SORTDIR,NULL,NULL,NULL,NULL,NULL,NULL,&rgDemPrqArray);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetAATArrayInfo DEMPRQ failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgDemPrqArray);
	    dbg(TRACE,"InitJobhdl: SetAATArrayInfo DEMPRQ OK");
	}/* end of if */


	ilRc = GetFieldLength(cgDemTab,"ALID",&llFieldLength);
	pllAddFieldLens[0] = llFieldLength;
	ilRc = GetFieldLength(cgDemTab,"ALOC",&llFieldLength);
	pllAddFieldLens[1] = llFieldLength;
	ilRc = GetFieldLength(cgPolTab,"URNO",&llFieldLength);
	pllAddFieldLens[2] = llFieldLength;
	pllAddFieldLens[3] = llFieldLength;
	pllAddFieldLens[4] = 0;

	ilRc = SetAATArrayInfo("DEMPOOL",DEMPOOL_FIELDS,pllAddFieldLens,"DEMPOOLIDX1",DEMPOOL_SORTFIELDS,DEMPOOL_SORTDIR,NULL,NULL,NULL,NULL,NULL,NULL,&rgDemPoolArray);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetAATArrayInfo DEMPOOL failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgDemPoolArray);
	    dbg(TRACE,"InitJobhdl: SetAATArrayInfo DEMPOOL OK");
	}/* end of if */

					
	pllAddFieldLens[0] = 30;
	pllAddFieldLens[1] = 30;
	pllAddFieldLens[2] = 30;
	pllAddFieldLens[3] = 0;

	ilRc = SetAATArrayInfo("JOBTYPE",JOBTYPE_FIELDS,pllAddFieldLens,"JOBTYPEIDX1",JOBTYPE_SORTFIELDS,JOBTYPE_SORTDIR,NULL,NULL,NULL,NULL,NULL,NULL,&rgJobTypeArray);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetAATArrayInfo JOBTYPE failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgJobTypeArray);
	    dbg(TRACE,"InitJobhdl: SetAATArrayInfo JOBTYPE OK");
	}/* end of if */




	ilRc = GetFieldLength(cgDemTab,"OURI",&llFieldLength);
	pllAddFieldLens[0] = llFieldLength;
	ilRc = GetFieldLength(cgDemTab,"OURO",&llFieldLength);
	pllAddFieldLens[1] = llFieldLength;
	ilRc = GetFieldLength(cgTplTab,"TNAM",&llFieldLength);
	pllAddFieldLens[2] = llFieldLength;
	ilRc = GetFieldLength(cgSerTab,"SECO",&llFieldLength );
	pllAddFieldLens[3] = llFieldLength;

	pllAddFieldLens[4] = 3;
	pllAddFieldLens[5] = 0;

	ilRc = SetAATArrayInfo("DEMIDX",DEMIDX_FIELDS,pllAddFieldLens,"DEMIDXIDX1",DEMIDX_SORTFIELDS,DEMIDX_SORTDIR,NULL,NULL,NULL,NULL,NULL,NULL,&rgDemIdxArray);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetAATArrayInfo DEMIDX failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgDemIdxArray);
	    dbg(TRACE,"InitJobhdl: SetAATArrayInfo DEMIDX OK");
	}/* end of if */


	ilRc = GetFieldLength(cgJobTab,"URNO",&llFieldLength);
	pllAddFieldLens[0] = llFieldLength;
	ilRc = GetFieldLength(cgSpeTab,"CODE",&llFieldLength);
	pllAddFieldLens[1] = llFieldLength;
	ilRc = GetFieldLength(cgSpeTab,"PRIO",&llFieldLength);
	pllAddFieldLens[2] = llFieldLength;
	pllAddFieldLens[3] = 0;

	ilRc = SetAATArrayInfo("POOLPER",POOLPER_FIELDS,pllAddFieldLens,"POOLPERIDX1",POOLPER_SORTFIELDS,POOLPER_SORTDIR,NULL,NULL,NULL,NULL,NULL,NULL,&rgPoolPerArray);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetAATArrayInfo POOLPER failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgPoolPerArray);
	    dbg(TRACE,"InitJobhdl: SetAATArrayInfo POOLPER OK");
	}/* end of if */


	ilRc = GetFieldLength(cgJobTab,"URNO",&llFieldLength);
	pllAddFieldLens[0] = llFieldLength;
	ilRc = GetFieldLength(cgSpfTab,"CODE",&llFieldLength);
	pllAddFieldLens[1] = llFieldLength;
	ilRc = GetFieldLength(cgSpfTab,"PRIO",&llFieldLength);
	pllAddFieldLens[2] = llFieldLength;
	pllAddFieldLens[3] = 0;

	ilRc = SetAATArrayInfo("POOLFCT",POOLFCT_FIELDS,pllAddFieldLens,"POOLFCTIDX1",POOLFCT_SORTFIELDS,POOLFCT_SORTDIR,NULL,NULL,NULL,NULL,NULL,NULL,&rgPoolFctArray);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetAATArrayInfo POOLFCT failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgPoolFctArray);
	    dbg(TRACE,"InitJobhdl: SetAATArrayInfo POOLFCT OK");
	}/* end of if */


	pllAddFieldLens[0] = 1;
	pllAddFieldLens[2] = 3;
	ilRc = GetFieldLength(cgJobTab,"URNO",&llFieldLength);
	pllAddFieldLens[1] = llFieldLength;
	pllAddFieldLens[3] = 0;

	ilRc = SetAATArrayInfo("TMPURNO",TMPURNO_FIELDS,pllAddFieldLens,"TMPURNOIDX1",TMPURNO_SORTFIELDS,TMPURNO_SORTDIR,NULL,NULL,NULL,NULL,NULL,NULL,&rgTmpUrnoArray);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetAATArrayInfo TMPURNO failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgTmpUrnoArray);
	    dbg(TRACE,"InitJobhdl: SetAATArrayInfo TMPURNO OK");
	}/* end of if */


	pllAddFieldLens[0] = 3;
	ilRc = GetFieldLength(cgJobTab,"URNO",&llFieldLength);
	pllAddFieldLens[1] = llFieldLength;
	pllAddFieldLens[2] = llFieldLength;
	pllAddFieldLens[3] = 0;

	ilRc = SetAATArrayInfo("URNOURNO",URNOURNO_FIELDS,pllAddFieldLens,"URNOURNOIDX1",URNOURNO_SORTFIELDS,URNOURNO_SORTDIR,"URNOURNOIDX2",URNOURNO_SORTFIELDS2,URNOURNO_SORTDIR2,NULL,NULL,NULL,&rgUrnoUrnoArray);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetAATArrayInfo URNOURNO failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgUrnoUrnoArray);
	    dbg(TRACE,"InitJobhdl: SetAATArrayInfo URNOURNO OK");
	}/* end of if */


		
		
	pllAddFieldLens[0] = 10;
	ilRc = GetFieldLength(cgJobTab,"URNO",&llFieldLength);
	pllAddFieldLens[1] = llFieldLength;
	ilRc = GetFieldLength(cgJobTab,"ACFR",&llFieldLength);
	pllAddFieldLens[2] = llFieldLength;
	pllAddFieldLens[3] = llFieldLength;
	ilRc = GetFieldLength(cgDemTab,"URNO",&llFieldLength);
	pllAddFieldLens[4] = llFieldLength;
	pllAddFieldLens[5] = 0;

	ilRc = SetAATArrayInfo("KEYURNOLIST",KEYURNOLIST_FIELDS,pllAddFieldLens
			       ,"KEYURNOLISTIDX1",KEYURNOLIST_SORTFIELDS,KEYURNOLIST_SORTDIR,
			       NULL,NULL,NULL, /*KEYURNOLISTIDX2",KEYURNOLIST_SORTFIELDS2,KEYURNOLIST_SORTDIR2,*/
			       "KEYURNOLISTIDX3",KEYURNOLIST_SORTFIELDS3,KEYURNOLIST_SORTDIR3,
			       &rgKeyUrnoListArray);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetAATArrayInfo KEYURNOLIST failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgKeyUrnoListArray);
	    dbg(TRACE,"InitJobhdl: SetAATArrayInfo KEYURNOLIST OK");
	}/* end of if */


		
		
	pllAddFieldLens[0] = 4;
	pllAddFieldLens[1] = 0;

	ilRc = SetAATArrayInfo("FIELDS",FIELDS_FIELDS,pllAddFieldLens,
							NULL,NULL,NULL, /*"FIELDSIDX1",FIELDS_SORTFIELDS,FIELDS_SORTDIR,*/
							NULL,NULL,NULL,NULL,NULL,NULL,&rgFieldsArray);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetAATArrayInfo FIELDSLIST failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgFieldsArray);
	    dbg(TRACE,"InitJobhdl: SetAATArrayInfo FIELDSLIST OK");
	}/* end of if */

	GetFieldLength(cgSwgTab,"CODE",&llFieldLength);
	pllAddFieldLens[0] = llFieldLength;
	pllAddFieldLens[1] = 3;
	pllAddFieldLens[2] = 0;

	ilRc = SetAATArrayInfo("SWGINDEX",SWGINDEX_FIELDS,pllAddFieldLens,
						    NULL,NULL,NULL,/*"SWGINDEXIDX1",SWGINDEX_SORTFIELDS,SWGINDEX_SORTDIR,*/
							"SWGINDEXIDX2",SWGINDEX_SORTFIELDS2,SWGINDEX_SORTDIR2,NULL,NULL,NULL,&rgSwgIndexArray);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetAATArrayInfo SWGINDEX failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgSwgIndexArray);
	    dbg(TRACE,"InitJobhdl: SetAATArrayInfo SWGINDEX OK");
	}/* end of if */


	GetFieldLength(cgJobTab,"URNO",&llFieldLength);
	pllAddFieldLens[0] = llFieldLength;
	pllAddFieldLens[1] = llFieldLength;
	pllAddFieldLens[2] = llFieldLength;
	pllAddFieldLens[3] = 4;
	pllAddFieldLens[4] = 4;
	pllAddFieldLens[5] = 4;
	GetFieldLength(cgDemTab,"DEBE",&llFieldLength);

	pllAddFieldLens[6] = llFieldLength;
	pllAddFieldLens[7] = llFieldLength;
	pllAddFieldLens[8] = 5;
	pllAddFieldLens[9] = 5;
	pllAddFieldLens[10] = 5;
	pllAddFieldLens[11] = 0;

	ilRc = SetAATArrayInfo("MATCH",MATCH_FIELDS,pllAddFieldLens,"MATCHIDX1",MATCH_SORTFIELDS,MATCH_SORTDIR,"MATCHIDX2",MATCH_SORTFIELDS2,MATCH_SORTDIR2,"MATCHIDX3",MATCH_SORTFIELDS3,MATCH_SORTDIR3,&rgMatchArray);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetAATArrayInfo MATCH failed <%d>",ilRc);
	}
	else
	{
		
		strcpy(rgMatchArray.crIdx04Name, "MATCH_PRIO" );
		strcpy(rgMatchArray.crIdx04FieldList, "PRIO" );

		if ( CEDAArrayCreateSimpleIndexUp (&(rgMatchArray.rrArrayHandle),&(rgMatchArray.crArrayName[0]),
										   &(rgMatchArray.rrIdx04Handle),&(rgMatchArray.crIdx04Name[0]),	
		                                  "PRIO") != RC_SUCCESS )
			dbg(TRACE,"InitJobhdl: CEDAArrayCreateSimpleIndexUp MATCH failed" );
	    DebugPrintArrayInfo(DEBUG,&rgMatchArray);
	    dbg(TRACE,"InitJobhdl: SetAATArrayInfo MATCH OK");
	}/* end of if */

	GetFieldLength(cgSwgTab,"CODE",&llFieldLength);
	pllAddFieldLens[0] = llFieldLength;
	GetFieldLength(cgJobTab,"URNO",&llFieldLength);
	pllAddFieldLens[1] = llFieldLength;
	pllAddFieldLens[2] = llFieldLength;
	pllAddFieldLens[3] = 20;
	GetFieldLength(cgDemTab,"DEBE",&llFieldLength);
	pllAddFieldLens[4] = llFieldLength;
	pllAddFieldLens[5] = llFieldLength;

	pllAddFieldLens[6] = 0;
		

	ilRc = SetAATArrayInfo("GROUPLIST",GROUPLIST_FIELDS,pllAddFieldLens,"GROUPLISTIDX1",GROUPLIST_SORTFIELDS,GROUPLIST_SORTDIR,"GROUPLISTIDX2",GROUPLIST_SORTFIELDS2,GROUPLIST_SORTDIR2,NULL,NULL,NULL,&rgGroupListArray);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetAATArrayInfo GROUPLIST failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgGroupListArray);
	    dbg(TRACE,"InitJobhdl: SetAATArrayInfo GROUPLIST OK");
	}/* end of if */

	GetFieldLength(cgSwgTab,"CODE",&llFieldLength);
	pllAddFieldLens[0] = llFieldLength;
	pllAddFieldLens[1] = 0;
		

	ilRc = SetAATArrayInfo("GROUPNAME",GROUPNAME_FIELDS,pllAddFieldLens,"GROUPNAMEIDX1",GROUPNAME_SORTFIELDS,GROUPNAME_SORTDIR,NULL,NULL,NULL,NULL,NULL,NULL,&rgGroupNameArray);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetAATArrayInfo GROUPNAME failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgGroupNameArray);
	    dbg(TRACE,"InitJobhdl: SetAATArrayInfo GROUPNAME OK");
	}/* end of if */


	GetFieldLength(cgDemTab,"ALID",&llFieldLength);
	pllAddFieldLens[0] = llFieldLength;
	GetFieldLength(cgDemTab,"ALOC",&llFieldLength);
	pllAddFieldLens[1] = llFieldLength;
	GetFieldLength(cgJobTab,"UAID",&llFieldLength);
	pllAddFieldLens[2] = llFieldLength;
	GetFieldLength(cgJobTab,"UALO",&llFieldLength);
	pllAddFieldLens[3] = llFieldLength;
	pllAddFieldLens[4] = 0;
		

	ilRc = SetAATArrayInfo("ALIDLIST",ALIDLIST_FIELDS,pllAddFieldLens
			       ,"ALIDLISTIDX1",ALIDLIST_SORTFIELDS,ALIDLIST_SORTDIR
			       ,"ALIDLISTIDX2",ALIDLIST_SORTFIELDS2,ALIDLIST_SORTDIR2
			       ,"ALIDLISTIDX3",ALIDLIST_SORTFIELDS3,ALIDLIST_SORTDIR3
			       ,&rgAlidListArray);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetAATArrayInfo ALIDLIST failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgAlidListArray);
	    dbg(TRACE,"InitJobhdl: SetAATArrayInfo ALIDLIST OK");
	}/* end of if */

	pllAddFieldLens[0] = 20;

	ilRc = SetAATArrayInfo("URNO",URNO_FIELDS,pllAddFieldLens
			       ,"URNOIDX1",URNO_SORTFIELDS,URNO_SORTDIR
			       ,NULL,NULL,NULL,NULL,NULL,NULL,&rgUrnoArray);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitJobhdl: SetAATArrayInfo URNO failed <%d>",ilRc);
	}else{
	    DebugPrintArrayInfo(DEBUG,&rgAlidListArray);
	    dbg(TRACE,"InitJobhdl: SetAATArrayInfo URNO OK");
	}/* end of if */


/**************
		Die geladenen PoolJobs werden prepariert
*******************/
	sprintf(clKey,"");
	ProcessNewPoolJob(clKey,&rgPoolJobArray,TRUE,FALSE);

	SetJobTypeArray();
	SetFieldsArray();
	PrepareJobData();

	llNext = ARR_NEXT;
	InitializeDemPoolRow(cgTmpPoolRow);
	strcpy(DEMPOOLFIELD(cgTmpPoolRow,igDemPoolAlid),"");
	strcpy(DEMPOOLFIELD(cgTmpPoolRow,igDemPoolAloc),"");
	strcpy(DEMPOOLFIELD(cgTmpPoolRow,igDemPoolUpol),"");
	strcpy(DEMPOOLFIELD(cgTmpPoolRow,igDemPoolUaid),"");
	ilRc = AATArrayAddRow(&(rgDemPoolArray.rrArrayHandle),&(rgDemPoolArray.crArrayName[0]),&llNext,(void *)cgTmpPoolRow);
	AATArrayPutField(&(rgDemPoolArray.rrArrayHandle),
			 &(rgDemPoolArray.crArrayName[0]),NULL,"UPOL",llNext,TRUE,"") ;
	AATArrayPutField(&(rgDemPoolArray.rrArrayHandle),
			 &(rgDemPoolArray.crArrayName[0]),NULL,"UAID",llNext,TRUE,"") ;
	AATArrayPutField(&(rgDemPoolArray.rrArrayHandle),
			 &(rgDemPoolArray.crArrayName[0]),NULL,"ALID",llNext,TRUE,"dummyfsc") ;
	AATArrayPutField(&(rgDemPoolArray.rrArrayHandle),
			 &(rgDemPoolArray.crArrayName[0]),NULL,"ALOC",llNext,TRUE,"fsc") ;

    }
    if(ilRc == RC_SUCCESS)
    {
	igInitOK = TRUE;
    }
    else
    {
	igInitOK = FALSE;
    }

	ilRc = TriggerAction("RELDRR", "SBC" );
	if(ilRc != RC_SUCCESS)
		dbg(TRACE,"InitJobhdl: TriggerAction <RELDRR> failed <%d>",ilRc);
	else
		dbg(DEBUG,"InitJobhdl: TriggerAction <RELDRR> OK" );
	
	ilRc = TriggerAction("RELJOB", "SBC" );
	if(ilRc != RC_SUCCESS)
		dbg(TRACE,"InitJobhdl: TriggerAction <RELJOB> failed <%d>",ilRc);
	else
		dbg(DEBUG,"InitJobhdl: TriggerAction <RELJOB> OK" );
	
	debug_level = igRuntimeMode; 

    return ilRc;
	
} /* end of initialize */

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int	ilRc = RC_SUCCESS;				/* Return code */
	
    dbg(TRACE,"Reset: now resetting");
	
    return ilRc;
	
} /* end of Reset */

 
/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
    debug_level = DEBUG;
    dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);
    switch(pipSig)
    {
	default	:
	    Terminate(60);
	    break;
    } /* end of switch */
    exit(0);
	
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
	
    return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
	
    switch(pipErr) {
	case	QUE_E_FUNC	:	/* Unknown function */
	    dbg(TRACE,"<%d> : unknown function",pipErr);
	    break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
	    dbg(TRACE,"<%d> : malloc failed",pipErr);
	    break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
	    dbg(TRACE,"<%d> : msgsnd failed",pipErr);
	    break;
	case	QUE_E_GET	:	/* Error using msgrcv */
	    dbg(TRACE,"<%d> : msgrcv failed",pipErr);
	    break;
	case	QUE_E_EXISTS	:
	    dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
	    break;
	case	QUE_E_NOFIND	:
	    dbg(TRACE,"<%d> : route not found ",pipErr);
	    break;
	case	QUE_E_ACKUNEX	:
	    dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
	    break;
	case	QUE_E_STATUS	:
	    dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
	    break;
	case	QUE_E_INACTIVE	:
	    dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
	    break;
	case	QUE_E_MISACK	:
	    dbg(TRACE,"<%d> : missing ack ",pipErr);
	    break;
	case	QUE_E_NOQUEUES	:
	    dbg(TRACE,"<%d> : queue does not exist",pipErr);
	    break;
	case	QUE_E_RESP	:	/* No response on CREATE */
	    dbg(TRACE,"<%d> : no response on create",pipErr);
	    break;
	case	QUE_E_FULL	:
	    dbg(TRACE,"<%d> : too many route destinations",pipErr);
	    break;
	case	QUE_E_NOMSG	:	/* No message on queue */
	    dbg(TRACE,"<%d> : no messages on queue",pipErr);
	    break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
	    dbg(TRACE,"<%d> : invalid originator=0",pipErr);
	    break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
	    dbg(TRACE,"<%d> : queues are not initialized",pipErr);
	    break;
	case	QUE_E_ITOBIG	:
	    dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
	    break;
	case	QUE_E_BUFSIZ	:
	    dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
	    break;
	default			:	/* Unknown queue error */
	    dbg(TRACE,"<%d> : unknown error",pipErr);
	    break;
    } /* end switch */
         
    return;
} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
    int	ilRc = RC_SUCCESS;			/* Return code */
    int	ilBreakOut = FALSE;
	
    do
    {
	ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
	/* depending on the size of the received item  */
	/* a realloc could be made by the que function */
	/* so do never forget to set event pointer !!! */
	prgEvent = (EVENT *) prgItem->text;	
	if( ilRc == RC_SUCCESS )
	{
	    /* Acknowledge the item */
	    ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
	    if( ilRc != RC_SUCCESS ) 
	    {
		/* handle que_ack error */
		HandleQueErr(ilRc);
	    } /* fi */
		
	    switch( prgEvent->command )
	    {
		case	HSB_STANDBY	:
		    ctrl_sta = prgEvent->command;
		    break;	
	
		case	HSB_COMING_UP	:
		    ctrl_sta = prgEvent->command;
		    break;	
	
		case	HSB_ACTIVE	:
		    ctrl_sta = prgEvent->command;
		    ilBreakOut = TRUE;
		    break;	
		case	HSB_ACT_TO_SBY	:
		    ctrl_sta = prgEvent->command;
		    break;	
	
		case	HSB_DOWN	:
		    /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
		    ctrl_sta = prgEvent->command;
		    Terminate(0);
		    break;	
	
		case	HSB_STANDALONE	:
		    ctrl_sta = prgEvent->command;
		    ResetDBCounter();
		    ilBreakOut = TRUE;
		    break;	
		case	REMOTE_DB :
		    /* ctrl_sta is checked inside */
		    HandleRemoteDB(prgEvent);
		    break;
		case	SHUTDOWN	:
		    Terminate(0);
		    break;
						
		case	RESET		:
		    ilRc = Reset();
		    break;
						
		case	EVENT_DATA	:
		    dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
		    DebugPrintItem(TRACE,prgItem);
		    DebugPrintEvent(TRACE,prgEvent);
		    break;
					
		case	TRACE_ON :
		    dbg_handle_debug(prgEvent->command);
		    break;
		case	TRACE_OFF :
		    dbg_handle_debug(prgEvent->command);
		    break;
		default			:
		    dbg(TRACE,"HandleQueues: unknown event");
		    DebugPrintItem(TRACE,prgItem);
		    DebugPrintEvent(TRACE,prgEvent);
		    break;
	    } /* end switch */
	} else {
	    /* Handle queuing errors */
	    HandleQueErr(ilRc);
	} /* end else */
    } while (ilBreakOut == FALSE);
    if(igInitOK == FALSE)
    {
	ilRc = InitJobhdl();
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"InitDemfnd: init failed!");
	} /* end of if */
    }/* end of if */
    /* OpenConnection(); */
} /* end of HandleQueues */
	
/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData()
{
    int	ilRc = RC_SUCCESS;			/* Return code */
    int      ilCmd          = 0;

    BC_HEAD *prlBchead      = NULL;
    CMDBLK  *prlCmdblk      = NULL;
    char    *pclSelection   = NULL;
    char    *pclFields      = NULL;
    char    *pclData        = NULL;
    char     clUjty[20];
    char     clUrno[20];
    char     *pclNewline;
    char 		clUrnoList[2400];
    char 		clTable[24];
    char clTwStart[128] = "\0";
    char clTwEnd[128] = "\0";
	BOOL	blSingleBCs = TRUE;

	cgMinAcfr[0]='\0';
	cgMaxActo[0]='\0';

    prlBchead    = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
    prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
    pclSelection = prlCmdblk->data;
    pclFields    = pclSelection + strlen(pclSelection) + 1;
   
    pclData      = pclFields + strlen(pclFields) + 1;
    strcpy(cgDestName,prlBchead->dest_name);
    strcpy(clTwStart,prlCmdblk->tw_start);
    strcpy(clTwEnd,prlCmdblk->tw_end);

 
    pclNewline = strchr(pclData,'\n');
    if (pclNewline != NULL)
    {
	*pclNewline = '\0';
    }
    pcgFieldList  = pclFields;

    dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);

    DebugPrintBchead(DEBUG,prlBchead);
    DebugPrintCmdblk(DEBUG,prlCmdblk);
    dbg(DEBUG,"selection <%s>",pclSelection);
    dbg(DEBUG,"fields    <%s>",pclFields);
	dbg(DEBUG,"Data      <%s>",pclData);

    ilRc = GetCommand(&prlCmdblk->command[0],&ilCmd);
    if(ilRc == RC_SUCCESS)
    {
		if ( bgSendUpdJob && ( (ilCmd == CMD_SSD) || (ilCmd == CMD_AFL) ) )
			blSingleBCs = FALSE;
		CEDAArraySendChanges2BCHDL( &rgJobArray.rrArrayHandle,rgJobArray.crArrayName,
									blSingleBCs );
		CEDAArraySendChanges2ACTION(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,
									blSingleBCs);
		CEDAArraySendChanges2BCHDL( &rgJodArray.rrArrayHandle,rgJodArray.crArrayName,
								    blSingleBCs);
		CEDAArraySendChanges2ACTION(&rgJodArray.rrArrayHandle,rgJodArray.crArrayName,
									blSingleBCs);

		switch (ilCmd)
		{
			case CMD_IFR:
				/*  rename IFR-event to IRT, otherwise CEDAArrayEventUpdate2 doesn't work */
				strcpy (prlCmdblk->command, "IRT" );
			case CMD_IRT :
				if(strcmp(prlCmdblk->obj_name,cgDemTab) != 0)
				{
					ilRc = CEDAArrayEventUpdate2(prgEvent,clUrnoList,clTable);
				}
				if((ilRc == RC_SUCCESS) && (strcmp(prlCmdblk->obj_name,cgJobTab) == 0))
				{
					GetItemData("UJTY",pclFields,pclData,clUjty);		
					GetItemData("URNO",pclFields,pclData,clUrno);		
					dbg(TRACE,"HandleData IRT UJTY <%s>",clUjty);
					dbg(TRACE,"HandleData IRT URNO <%s>",clUrno);
			
					if(atol(clUrno) > 0)
					{
						if(atol(clUjty) == lgUjtyPol)
						{
							ProcessNewPoolJob(clUrno, &rgPoolJobArray,TRUE,bgAutomaticPauseAllocation);
						}
						else
						{
							PrepareSingleJobData(clUrno);
						}
					}

				}
				break;
			case CMD_UFR:
				/*  rename UFR-event to URT, otherwise CEDAArrayEventUpdate2 doesn't work */
				strcpy (prlCmdblk->command, "URT" );
			case CMD_URT :			
				if(strcmp(prlCmdblk->obj_name,cgDemTab) == 0)
				{
					int ilOldDebugLevel = debug_level;
					if ( ilOldDebugLevel < TRACE )
					debug_level = TRACE;
					/***********************/
					dbg(TRACE,"HandleData URT DemChange pclData<%s>",pclData);
					ProcessDemChange(pclFields,pclData);
					debug_level = ilOldDebugLevel;
					/*************/
				}
				else
				{
					ilRc = CEDAArrayEventUpdate2(prgEvent,clUrnoList,clTable);
				}
				/********
				if(strcmp(prlCmdblk->obj_name,cgJobTab) == 0)
				{
					if(GetItemData("UJTY",pclFields,pclData,clUjty) == RC_SUCCESS)
					{
						GetItemData("URNO",pclFields,pclData,clUrno);		
						dbg(TRACE,"HandleData DRT UJTY <%s>",clUjty);
						dbg(TRACE,"HandleData DRT URNO <%s>",clUrno);
							
						if(atol(clUjty) == lgUjtyPol)
						{
							ProcessChangePoolJob(clUrno);
						}
					}
				}
				******/
				break;
			case CMD_DRT :
				if(strcmp(prlCmdblk->obj_name,cgDemTab) == 0)
				{
					void *pcvNxt = NULL;                                           
					char *pclU = NULL;  
					pcvNxt = NULL;
					while ((pclU=GetNextUrnoFromSelection(pclSelection,&pcvNxt))!= NULL)  
					{
						dbg(TRACE,"HandleData DRT DEMURNO <%s>",pclSelection);
						if(atol(pclU) > 0)
						{
							ProcessDeleteDemand(pclU);
						}
					}
				}
				else
				{
					ilRc = CEDAArrayEventUpdate2(prgEvent,clUrnoList,clTable);
					if(strcmp(prlCmdblk->obj_name,cgJobTab) == 0)
					{
						void *pcvNxt = NULL;                                           
						char *pclU = NULL;  
						pcvNxt = NULL;
						while ((pclU=GetNextUrnoFromSelection(pclSelection,&pcvNxt))!= NULL)  
						{
							dbg(TRACE,"HandleData DRT JOBURNO <%s>",pclSelection);
							if(atol(pclU) > 0)
							{
								ProcessDeletePoolJob(pclU);
							}
						}
					}
				}
				break;
			case CMD_SSD : /* Send Selected Demands  */
				ProcessSSD(pclData, &rgOpenDemArray);
				break;
			case CMD_AFL:    /* Assign Flights*/
				SendAnswer(prgEvent,RC_SUCCESS);
				ProcessAFL(pclFields,pclData);	
				(void) tools_send_info_flag(1900,0, cgDestName, "", "JOBHDL",
								"", "", clTwStart, clTwEnd,
								"SBC","AFLEND","AFLEND","","",0);          

				break;
			case CMD_CJC:    /* Check Conflicts*/
				ProcessCJC();		
				break;
			case CMD_SBC:
				if(strcmp(prlCmdblk->obj_name,"RELJOB") == 0)
				{
					ilRc = HandleRelJob ( pclData, pclSelection );
					dbg ( DEBUG, "HandleData: HandleRelJob returned <%d>", ilRc );
				}		
				if(strcmp(prlCmdblk->obj_name,"RELDRR") == 0)
				{
					ilRc = HandleRelDrr ( pclData );
					dbg ( DEBUG, "HandleData: HandleRelDrr returned <%d>", ilRc );
				}
				break;

			default:
				break;	
		}
	
    }

    dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);
    return ilRc;
	
} /* end of HandleData */

/******************************************************************************/
/* Space for Appendings (Source-Code Control)                                 */
/******************************************************************************/

/******************************************************************************/
/******************************************************************************/
static int GetCommand(char *pcpCommand, int *pipCmd)
{
    int	ilRc   = RC_SUCCESS;			/* Return code */
    int ilLoop = 0;
    char clCommand[8];
	
    memset(&clCommand[0],0x00,8);

    ilRc = get_real_item(&clCommand[0],pcpCommand,1);
    if(ilRc > 0)
    {
	ilRc = RC_FAIL;
    }/* end of if */

    while((ilRc != 0) && (prgCmdTxt[ilLoop] != NULL))
    {
	ilRc = strcmp(&clCommand[0],prgCmdTxt[ilLoop]);
	ilLoop++;
    }/* end of while */

    if(ilRc == 0)
    {
	ilLoop--;
	dbg(DEBUG,"GetCommand: <%s> <%d>",prgCmdTxt[ilLoop],rgCmdDef[ilLoop]);
	*pipCmd = rgCmdDef[ilLoop];
    }else{
	dbg(TRACE,"GetCommand: <%s> is not valid",&clCommand[0]);
	ilRc = RC_FAIL;
    }/* end of if */

    return(ilRc);
	
} /* end of GetCommand */


/******************************************************************************/
/*                                                                            */
/******************************************************************************/
static int  SetArrayInfo(char *pcpArrayName,char *pcpTableName,  char *pcpArrayFieldList,char *pcpAddFields, long *plpAddFieldLens, 
			 char *pcpIdx01Name, char *pcpIdx01FieldList, char *pcpIdx01Order, 
			 char *pcpIdx02Name, char *pcpIdx02FieldList, char *pcpIdx02Order, 
			 char *pcpIdx03Name, char *pcpIdx03FieldList, char *pcpIdx03Order, 
			 char *pcpIdx04Name, char *pcpIdx04FieldList, char *pcpIdx04Order, 
			 char *pcpIdx05Name, char *pcpIdx05FieldList, char *pcpIdx05Order, 
			 char *pcpIdx06Name, char *pcpIdx06FieldList, char *pcpIdx06Order, 
			 char *pcpIdx07Name, char *pcpIdx07FieldList, char *pcpIdx07Order, 
			 char *pcpIdx08Name, char *pcpIdx08FieldList, char *pcpIdx08Order, 
			 ARRAYINFO *prpArrayInfo,char *pcpSelection,BOOL bpFill,BOOL bpUseTrigger)
{
    int	ilRc   = RC_SUCCESS;				/* Return code */
    int ilLoop = 0;
    char clOrderTxt[4];
    long llTmpRowLength1 = 0;
    long llTmpRowLength2 = 0;
    if(prpArrayInfo == NULL)
    {
	dbg(TRACE,"SetArrayInfo: invalid last parameter : null pointer not valid");
	ilRc = RC_FAIL;
    }else{
	memset(prpArrayInfo,0x00,sizeof(prpArrayInfo));

	prpArrayInfo->rrArrayHandle = -1;
	prpArrayInfo->rrIdx01Handle = -1;
	prpArrayInfo->rrIdx02Handle = -1;
	prpArrayInfo->rrIdx03Handle = -1;
	prpArrayInfo->rrIdx04Handle = -1;
	prpArrayInfo->rrIdx05Handle = -1;
	prpArrayInfo->rrIdx06Handle = -1;
	prpArrayInfo->rrIdx07Handle = -1;
	prpArrayInfo->rrIdx08Handle = -1;

	if(pcpArrayName != NULL)
	{
	    if(strlen(pcpArrayName) <= ARR_NAME_LEN)
	    {
		strcpy(prpArrayInfo->crArrayName,pcpArrayName);
	    }/* end of if */
	}/* end of if */

	if(pcpArrayFieldList != NULL)
	{
	    if(strlen(pcpArrayFieldList) <= ARR_FLDLST_LEN)
	    {
		strcpy(prpArrayInfo->crArrayFieldList,pcpArrayFieldList);
	    }/* end of if */
	}/* end of if */

	if(pcpAddFields != NULL)
	{
	    if(strlen(pcpAddFields) + 
	       strlen(prpArrayInfo->crArrayFieldList) <= (size_t)ARR_FLDLST_LEN)
	    {
		strcat(prpArrayInfo->crArrayFieldList,",");
		strcat(prpArrayInfo->crArrayFieldList,pcpAddFields);
		dbg(TRACE,"<%s>",prpArrayInfo->crArrayFieldList);
	    }/* end of if */
	}/* end of if */


	if(ilRc == RC_SUCCESS)
	{
	    prpArrayInfo->lrArrayFieldCnt = get_no_of_items(pcpArrayFieldList);
	    if (pcpAddFields != NULL)
	    {
		prpArrayInfo->lrArrayFieldCnt += get_no_of_items(pcpAddFields);
	    }

	    prpArrayInfo->plrArrayFieldOfs = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
	    if(prpArrayInfo->plrArrayFieldOfs == NULL)
	    {
		dbg(TRACE,"SetArrayInfo: plrArrayFieldOfs calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
		ilRc = RC_FAIL;
	    }/* end of if */
	}/* end of if */


	if(pcpIdx01Name != NULL)
	{
	    if(strlen(pcpIdx01Name) <= ARR_NAME_LEN)
	    {
		strcpy(prpArrayInfo->crIdx01Name,pcpIdx01Name);
	    }/* end of if */
	}/* end of if */

	if(pcpIdx01FieldList != NULL)
	{
	    if(strlen(pcpIdx01FieldList) <= ARR_FLDLST_LEN)
	    {
		strcpy(prpArrayInfo->crIdx01FieldList,pcpIdx01FieldList);
	    }/* end of if */
	}/* end of if */
	if(pcpIdx02Name != NULL)
	{
	    if(strlen(pcpIdx02Name) <= ARR_NAME_LEN)
	    {
		strcpy(prpArrayInfo->crIdx02Name,pcpIdx02Name);
	    }/* end of if */
	}/* end of if */

	if(pcpIdx02FieldList != NULL)
	{
	    if(strlen(pcpIdx02FieldList) <= ARR_FLDLST_LEN)
	    {
		strcpy(prpArrayInfo->crIdx02FieldList,pcpIdx02FieldList);
	    }/* end of if */
	}/* end of if */

	if(pcpIdx03Name != NULL)
	{
	    if(strlen(pcpIdx03Name) <= ARR_NAME_LEN)
	    {
		strcpy(prpArrayInfo->crIdx03Name,pcpIdx03Name);
	    }/* end of if */
	}/* end of if */

	if(pcpIdx03FieldList != NULL)
	{
	    if(strlen(pcpIdx03FieldList) <= ARR_FLDLST_LEN)
	    {
		strcpy(prpArrayInfo->crIdx03FieldList,pcpIdx03FieldList);
	    }/* end of if */
	}/* end of if */


	if(pcpIdx04Name != NULL)
	{
	    if(strlen(pcpIdx04Name) <= ARR_NAME_LEN)
	    {
		strcpy(prpArrayInfo->crIdx04Name,pcpIdx04Name);
	    }/* end of if */
	}/* end of if */

	if(pcpIdx04FieldList != NULL)
	{
	    if(strlen(pcpIdx04FieldList) <= ARR_FLDLST_LEN)
	    {
		strcpy(prpArrayInfo->crIdx04FieldList,pcpIdx04FieldList);
	    }/* end of if */
	}/* end of if */
	if(pcpIdx05Name != NULL)
	{
	    if(strlen(pcpIdx05Name) <= ARR_NAME_LEN)
	    {
		strcpy(prpArrayInfo->crIdx05Name,pcpIdx05Name);
	    }/* end of if */
	}/* end of if */

	if(pcpIdx05FieldList != NULL)
	{
	    if(strlen(pcpIdx05FieldList) <= ARR_FLDLST_LEN)
	    {
		strcpy(prpArrayInfo->crIdx05FieldList,pcpIdx05FieldList);
	    }/* end of if */
	}/* end of if */
	if(pcpIdx06Name != NULL)
	{
	    if(strlen(pcpIdx06Name) <= ARR_NAME_LEN)
	    {
		strcpy(prpArrayInfo->crIdx06Name,pcpIdx06Name);
	    }/* end of if */
	}/* end of if */

	if(pcpIdx06FieldList != NULL)
	{
	    if(strlen(pcpIdx06FieldList) <= ARR_FLDLST_LEN)
	    {
		strcpy(prpArrayInfo->crIdx06FieldList,pcpIdx06FieldList);
	    }/* end of if */
	}/* end of if */
	if(pcpIdx07Name != NULL)
	{
	    if(strlen(pcpIdx07Name) <= ARR_NAME_LEN)
	    {
		strcpy(prpArrayInfo->crIdx07Name,pcpIdx07Name);
	    }/* end of if */
	}/* end of if */

	if(pcpIdx07FieldList != NULL)
	{
	    if(strlen(pcpIdx07FieldList) <= ARR_FLDLST_LEN)
	    {
		strcpy(prpArrayInfo->crIdx07FieldList,pcpIdx07FieldList);
	    }/* end of if */
	}/* end of if */
	if(pcpIdx08Name != NULL)
	{
	    if(strlen(pcpIdx08Name) <= ARR_NAME_LEN)
	    {
		strcpy(prpArrayInfo->crIdx08Name,pcpIdx08Name);
	    }/* end of if */
	}/* end of if */

	if(pcpIdx08FieldList != NULL)
	{
	    if(strlen(pcpIdx08FieldList) <= ARR_FLDLST_LEN)
	    {
		strcpy(prpArrayInfo->crIdx08FieldList,pcpIdx08FieldList);
	    }/* end of if */
	}/* end of if */
    }/* end of if */


    if(ilRc == RC_SUCCESS)
    {
	prpArrayInfo->plrArrayFieldLen = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
	if(prpArrayInfo->plrArrayFieldLen == NULL)
	{
	    dbg(TRACE,"SetArrayInfo: plrArrayFieldLen calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
	    ilRc = RC_FAIL;
	}else{
	    *(prpArrayInfo->plrArrayFieldLen) = -1;
	}/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
       	ilRc = GetRowLength(pcpTableName,pcpArrayFieldList,&(prpArrayInfo->lrArrayRowLen));
       	if(ilRc != RC_SUCCESS)
       	{
	    dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpArrayName,pcpArrayFieldList);
      	}/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
	prpArrayInfo->lrArrayRowLen++;
	prpArrayInfo->pcrArrayRowBuf = (char *) calloc(1,(prpArrayInfo->lrArrayRowLen+1));
	if(prpArrayInfo->pcrArrayRowBuf == NULL)
	{
	    ilRc = RC_FAIL;
	    dbg(TRACE,"SetArrayInfo: calloc failed");
	}/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS && pcpIdx01FieldList != NULL)
    {
		ilRc = GetTotalRowLength( pcpIdx01FieldList, prpArrayInfo,
								  &(prpArrayInfo->lrIdx01RowLen) );
		/*
       	ilRc = GetRowLength(pcpTableName,pcpIdx01FieldList,&llTmpRowLength1);
       	if(ilRc != RC_SUCCESS)
       	{
	    dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpIdx01Name,pcpIdx01FieldList);
       	}
		ilRc = GetLogicalRowLength(pcpAddFields,pcpIdx01FieldList,plpAddFieldLens,&llTmpRowLength2);
		prpArrayInfo->lrIdx01RowLen = llTmpRowLength1 + llTmpRowLength2;*/
		if(ilRc != RC_SUCCESS)
       	{
		    dbg(TRACE,"SetArrayInfo: GetTotalRowLength failed <%s> <%s>",pcpIdx01Name,pcpIdx01FieldList);
       	}

    }/* end of if */

    if(ilRc == RC_SUCCESS && pcpIdx02FieldList != NULL)
    {
		ilRc = GetTotalRowLength(pcpIdx02FieldList, prpArrayInfo,
								 &(prpArrayInfo->lrIdx02RowLen) );
		/*
       	ilRc = GetRowLength(pcpTableName,pcpIdx02FieldList,&llTmpRowLength1);
		
       	if(ilRc != RC_SUCCESS)
       	{
	    dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpIdx02Name,pcpIdx02FieldList);
       	}

		ilRc = GetLogicalRowLength(pcpAddFields,pcpIdx02FieldList,plpAddFieldLens,&llTmpRowLength2);
		prpArrayInfo->lrIdx02RowLen = llTmpRowLength1 + llTmpRowLength2;
		*/
		if(ilRc != RC_SUCCESS)
       	{
		    dbg(TRACE,"SetArrayInfo: GetTotalRowLength failed <%s> <%s>",pcpIdx02Name,pcpIdx02FieldList);
       	}/* end of if */

    }/* end of if */

    if(ilRc == RC_SUCCESS && pcpIdx03FieldList != NULL)
    {
		ilRc = GetTotalRowLength(pcpIdx03FieldList, prpArrayInfo,
								 &(prpArrayInfo->lrIdx03RowLen) );
		/*
       	ilRc = GetRowLength(pcpTableName,pcpIdx03FieldList,&llTmpRowLength1);
		
       	if(ilRc != RC_SUCCESS)
       	{
	    dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpIdx03Name,pcpIdx03FieldList);
       	}

		ilRc = GetLogicalRowLength(pcpAddFields,pcpIdx03FieldList,plpAddFieldLens,&llTmpRowLength2);
		prpArrayInfo->lrIdx03RowLen = llTmpRowLength1 + llTmpRowLength2;
		*/    
		if(ilRc != RC_SUCCESS)
       	{
			dbg(TRACE,"SetArrayInfo: GetTotalRowLength failed <%s> <%s>",pcpIdx03Name,pcpIdx03FieldList);
       	}/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS && pcpIdx04FieldList != NULL)
    {
		ilRc = GetTotalRowLength( pcpIdx04FieldList, prpArrayInfo,
								  &(prpArrayInfo->lrIdx04RowLen) );
		/*
       	ilRc = GetRowLength(pcpTableName,pcpIdx04FieldList,&llTmpRowLength1);
		
       	if(ilRc != RC_SUCCESS)
       	{
	    dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpIdx04Name,pcpIdx04FieldList);
       	}

		ilRc = GetLogicalRowLength(pcpAddFields,pcpIdx04FieldList,plpAddFieldLens,&llTmpRowLength2);
		prpArrayInfo->lrIdx04RowLen = llTmpRowLength1 + llTmpRowLength2;
        */
		if(ilRc != RC_SUCCESS)
       	{
			dbg(TRACE,"GetTotalRowLength: GetRowLength failed <%s> <%s>",pcpIdx04Name,pcpIdx04FieldList);
       	}/* end of if */
    }/* end of if */

   	
    if(ilRc == RC_SUCCESS && pcpIdx05FieldList != NULL)
    {
		ilRc = GetTotalRowLength(pcpIdx05FieldList, prpArrayInfo,
								 &(prpArrayInfo->lrIdx05RowLen) );
		/*
       	ilRc = GetRowLength(pcpTableName,pcpIdx05FieldList,&llTmpRowLength1);
		
       	if(ilRc != RC_SUCCESS)
       	{
	    dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpIdx05Name,pcpIdx05FieldList);
       	}

		ilRc = GetLogicalRowLength(pcpAddFields,pcpIdx05FieldList,plpAddFieldLens,&llTmpRowLength2);
		prpArrayInfo->lrIdx05RowLen = llTmpRowLength1 + llTmpRowLength2;
        */
		if(ilRc != RC_SUCCESS)
       	{
		    dbg(TRACE,"SetArrayInfo: GetTotalRowLength failed <%s> <%s>",pcpIdx05Name,pcpIdx05FieldList);
       	}/* end of if */
    }/* end of if */

   	
    if(ilRc == RC_SUCCESS && pcpIdx06FieldList != NULL)
    {
		ilRc = GetTotalRowLength(pcpIdx06FieldList, prpArrayInfo,
								 &(prpArrayInfo->lrIdx06RowLen) );
		/*
       	ilRc = GetRowLength(pcpTableName,pcpIdx06FieldList,&llTmpRowLength1);
		
       	if(ilRc != RC_SUCCESS)
       	{
	    dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpIdx06Name,pcpIdx06FieldList);
       	}

		ilRc = GetLogicalRowLength(pcpAddFields,pcpIdx06FieldList,plpAddFieldLens,&llTmpRowLength2);
		prpArrayInfo->lrIdx06RowLen = llTmpRowLength1 + llTmpRowLength2;
		*/
		if(ilRc != RC_SUCCESS)
       	{
			dbg(TRACE,"SetArrayInfo: GetTotalRowLength failed <%s> <%s>",pcpIdx06Name,pcpIdx06FieldList);
       	}/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS && pcpIdx07FieldList != NULL)
    {
		ilRc = GetTotalRowLength(pcpIdx07FieldList, prpArrayInfo,
								 &(prpArrayInfo->lrIdx07RowLen) );
		/*
       	ilRc = GetRowLength(pcpTableName,pcpIdx07FieldList,&llTmpRowLength1);
		
       	if(ilRc != RC_SUCCESS)
       	{
	    dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpIdx07Name,pcpIdx07FieldList);
       	}

		ilRc = GetLogicalRowLength(pcpAddFields,pcpIdx07FieldList,plpAddFieldLens,&llTmpRowLength2);
		prpArrayInfo->lrIdx07RowLen = llTmpRowLength1 + llTmpRowLength2;
        */
		if(ilRc != RC_SUCCESS)
       	{
			dbg(TRACE,"SetArrayInfo: GetTotalRowLength failed <%s> <%s>",pcpIdx07Name,pcpIdx07FieldList);
       	}/* end of if */
    }/* end of if */


    if(ilRc == RC_SUCCESS && pcpIdx08FieldList != NULL)
    {
		ilRc = GetTotalRowLength(pcpIdx08FieldList, prpArrayInfo,
								 &(prpArrayInfo->lrIdx08RowLen) );
		/*
       	ilRc = GetRowLength(pcpTableName,pcpIdx08FieldList,&llTmpRowLength1);
		
       	if(ilRc != RC_SUCCESS)
       	{
	    dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpIdx08Name,pcpIdx08FieldList);
       	}

		ilRc = GetLogicalRowLength(pcpAddFields,pcpIdx08FieldList,plpAddFieldLens,&llTmpRowLength2);
		prpArrayInfo->lrIdx08RowLen = llTmpRowLength1 + llTmpRowLength2;
        */
		if(ilRc != RC_SUCCESS)
       	{
			dbg(TRACE,"SetArrayInfo: GetTotalRowLength failed <%s> <%s>",pcpIdx08Name,pcpIdx08FieldList);
       	}/* end of if */
    }/* end of if */

   	
    if(ilRc == RC_SUCCESS  && pcpIdx01FieldList != NULL)
    {
	prpArrayInfo->lrIdx01RowLen++;
	dbg(TRACE,"SetArrayInfo: lrIdx01RowLen <%ld>",prpArrayInfo->lrIdx01RowLen);
	prpArrayInfo->pcrIdx01RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx01RowLen+1));
	if(prpArrayInfo->pcrIdx01RowBuf == NULL)
	{
	    ilRc = RC_FAIL;
	    dbg(TRACE,"SetArrayInfo: calloc failed");
	}/* end of if */
    }/* end of if */
	
 
    if(ilRc == RC_SUCCESS  && pcpIdx01FieldList != NULL)
    {
	prpArrayInfo->lrIdx01FieldCnt = get_no_of_items(pcpIdx01FieldList);
	dbg(TRACE,"SetArrayInfo: lrIdx01FieldCnt <%ld>",prpArrayInfo->lrIdx01FieldCnt);

	prpArrayInfo->plrIdx01FieldPos = (long *) calloc(prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
	if(prpArrayInfo->plrIdx01FieldPos == NULL)
	{
	    dbg(TRACE,"SetArrayInfo: plrIdx01FieldPos calloc(%d,%d) failed",prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
	    ilRc = RC_FAIL;
	}else{
	    *(prpArrayInfo->plrIdx01FieldPos) = -1;
	}/* end of if */
    }/* end of if */

 
    if(ilRc == RC_SUCCESS  && pcpIdx01FieldList != NULL)
    {
	prpArrayInfo->plrIdx01FieldOrd = (long *) calloc(prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
	if(prpArrayInfo->plrIdx01FieldOrd == NULL)
	{
	    dbg(TRACE,"SetArrayInfo: plrIdx01FieldOrd calloc(%d,%d) failed",prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
	    ilRc = RC_FAIL;
	}else{
	    for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrIdx01FieldCnt ; ilLoop++)
	    {
		dbg(TRACE,"SetArrayInfo: ilLoop <%d>",ilLoop);
		ilRc = get_real_item(&clOrderTxt[0],pcpIdx01Order,ilLoop+1);
		if(ilRc > 0)
		{
		    switch(clOrderTxt[0])
		    {
			case 'A' :
			    prpArrayInfo->plrIdx01FieldOrd[ilLoop] = ARR_ASC;
			    break;
			case 'D' :
			    prpArrayInfo->plrIdx01FieldOrd[ilLoop] = ARR_DESC;
			    break;
			default :
			    prpArrayInfo->plrIdx01FieldOrd[ilLoop] = ARR_DESC;
			    break;
		    }/* end of switch */

		    ilRc = RC_SUCCESS;
		}else{
		    prpArrayInfo->plrIdx01FieldOrd[ilLoop] = -1;
		}/* end of for */
	    }/* end of for */
	}/* end of if */
    }/* end of if */

	
	if(ilRc == RC_SUCCESS && pcpIdx02FieldList != NULL)
    {
	prpArrayInfo->lrIdx02RowLen++;
	prpArrayInfo->pcrIdx02RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx02RowLen+1));
	if(prpArrayInfo->pcrIdx02RowBuf == NULL)
	{
	    ilRc = RC_FAIL;
	    dbg(TRACE,"SetArrayInfo: calloc failed");
	}/* end of if */
    }/* end of if */
	
 	if(ilRc == RC_SUCCESS && pcpIdx02FieldList != NULL)
    {
	prpArrayInfo->lrIdx02FieldCnt = get_no_of_items(pcpIdx02FieldList);

	prpArrayInfo->plrIdx02FieldPos = (long *) calloc(prpArrayInfo->lrIdx02FieldCnt,sizeof(long));
	if(prpArrayInfo->plrIdx02FieldPos == NULL)
	{
	    dbg(TRACE,"SetArrayInfo: plrIdx02FieldPos calloc(%d,%d) failed",prpArrayInfo->lrIdx02FieldCnt,sizeof(long));
	    ilRc = RC_FAIL;
	}else{
	    *(prpArrayInfo->plrIdx02FieldPos) = -1;
	}/* end of if */
    }/* end of if */


 
    if(ilRc == RC_SUCCESS && pcpIdx02FieldList != NULL)
    {
	prpArrayInfo->plrIdx02FieldOrd = (long *) calloc(prpArrayInfo->lrIdx02FieldCnt,sizeof(long));
	if(prpArrayInfo->plrIdx02FieldOrd == NULL)
	{
	    dbg(TRACE,"SetArrayInfo: plrIdx02FieldOrd calloc(%d,%d) failed",prpArrayInfo->lrIdx02FieldCnt,sizeof(long));
	    ilRc = RC_FAIL;
	}else{
	    for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrIdx02FieldCnt ; ilLoop++)
	    {
		ilRc = get_real_item(&clOrderTxt[0],pcpIdx02Order,ilLoop+1);
		if(ilRc > 0)
		{
		    switch(clOrderTxt[0])
		    {
			case 'A' :
			    prpArrayInfo->plrIdx02FieldOrd[ilLoop] = ARR_ASC;
			    break;
			case 'D' :
			    prpArrayInfo->plrIdx02FieldOrd[ilLoop] = ARR_DESC;
			    break;
			default :
			    prpArrayInfo->plrIdx02FieldOrd[ilLoop] = ARR_DESC;
			    break;
		    }/* end of switch */

		    ilRc = RC_SUCCESS;
		}else{
		    prpArrayInfo->plrIdx02FieldOrd[ilLoop] = -1;
		}/* end of for */
	    }/* end of for */
	}/* end of if */
    }/* end of if */
	

    if(ilRc == RC_SUCCESS && pcpIdx03FieldList != NULL)
    {
	prpArrayInfo->lrIdx03RowLen++;
	prpArrayInfo->pcrIdx03RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx03RowLen+1));
	if(prpArrayInfo->pcrIdx03RowBuf == NULL)
	{
	    ilRc = RC_FAIL;
	    dbg(TRACE,"SetArrayInfo: calloc failed");
	}/* end of if */
    }/* end of if */
	

    if(ilRc == RC_SUCCESS && pcpIdx03FieldList != NULL)
    {
	prpArrayInfo->lrIdx03FieldCnt = get_no_of_items(pcpIdx03FieldList);

	prpArrayInfo->plrIdx03FieldPos = (long *) calloc(prpArrayInfo->lrIdx03FieldCnt,sizeof(long));
	if(prpArrayInfo->plrIdx03FieldPos == NULL)
	{
	    dbg(TRACE,"SetArrayInfo: plrIdx03FieldPos calloc(%d,%d) failed",prpArrayInfo->lrIdx03FieldCnt,sizeof(long));
	    ilRc = RC_FAIL;
	}else{
	    *(prpArrayInfo->plrIdx03FieldPos) = -1;
	}/* end of if */
    }/* end of if */


    if(ilRc == RC_SUCCESS && pcpIdx03FieldList != NULL)
    {
	prpArrayInfo->plrIdx03FieldOrd = (long *) calloc(prpArrayInfo->lrIdx03FieldCnt,sizeof(long));
	if(prpArrayInfo->plrIdx03FieldOrd == NULL)
	{
	    dbg(TRACE,"SetArrayInfo: plrIdx03FieldOrd calloc(%d,%d) failed",prpArrayInfo->lrIdx03FieldCnt,sizeof(long));
	    ilRc = RC_FAIL;
	}else{
	    for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrIdx03FieldCnt ; ilLoop++)
	    {
		ilRc = get_real_item(&clOrderTxt[0],pcpIdx03Order,ilLoop+1);
		if(ilRc > 0)
		{
		    switch(clOrderTxt[0])
		    {
			case 'A' :
			    prpArrayInfo->plrIdx03FieldOrd[ilLoop] = ARR_ASC;
			    break;
			case 'D' :
			    prpArrayInfo->plrIdx03FieldOrd[ilLoop] = ARR_DESC;
			    break;
			default :
			    prpArrayInfo->plrIdx03FieldOrd[ilLoop] = ARR_DESC;
			    break;
		    }/* end of switch */

		    ilRc = RC_SUCCESS;
		}else{
		    prpArrayInfo->plrIdx03FieldOrd[ilLoop] = -1;
		}/* end of for */
	    }/* end of for */
	}/* end of if */
    }/* end of if */
		

    if(ilRc == RC_SUCCESS && pcpIdx04FieldList != NULL)
    {
	prpArrayInfo->lrIdx04RowLen++;
	prpArrayInfo->pcrIdx04RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx04RowLen+1));
	if(prpArrayInfo->pcrIdx04RowBuf == NULL)
	{
	    ilRc = RC_FAIL;
	    dbg(TRACE,"SetArrayInfo: calloc failed");
	}/* end of if */
    }/* end of if */
	

    if(ilRc == RC_SUCCESS && pcpIdx04FieldList != NULL)
    {
	prpArrayInfo->lrIdx04FieldCnt = get_no_of_items(pcpIdx04FieldList);

	prpArrayInfo->plrIdx04FieldPos = (long *) calloc(prpArrayInfo->lrIdx04FieldCnt,sizeof(long));
	if(prpArrayInfo->plrIdx04FieldPos == NULL)
	{
	    dbg(TRACE,"SetArrayInfo: plrIdx04FieldPos calloc(%d,%d) failed",prpArrayInfo->lrIdx04FieldCnt,sizeof(long));
	    ilRc = RC_FAIL;
	}else{
	    *(prpArrayInfo->plrIdx04FieldPos) = -1;
	}/* end of if */
    }/* end of if */


    if(ilRc == RC_SUCCESS && pcpIdx04FieldList != NULL)
    {
	prpArrayInfo->plrIdx04FieldOrd = (long *) calloc(prpArrayInfo->lrIdx04FieldCnt,sizeof(long));
	if(prpArrayInfo->plrIdx04FieldOrd == NULL)
	{
	    dbg(TRACE,"SetArrayInfo: plrIdx04FieldOrd calloc(%d,%d) failed",prpArrayInfo->lrIdx04FieldCnt,sizeof(long));
	    ilRc = RC_FAIL;
	}else{
	    for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrIdx04FieldCnt ; ilLoop++)
	    {
		ilRc = get_real_item(&clOrderTxt[0],pcpIdx04Order,ilLoop+1);
		if(ilRc > 0)
		{
		    switch(clOrderTxt[0])
		    {
			case 'A' :
			    prpArrayInfo->plrIdx04FieldOrd[ilLoop] = ARR_ASC;
			    break;
			case 'D' :
			    prpArrayInfo->plrIdx04FieldOrd[ilLoop] = ARR_DESC;
			    break;
			default :
			    prpArrayInfo->plrIdx04FieldOrd[ilLoop] = ARR_DESC;
			    break;
		    }/* end of switch */

		    ilRc = RC_SUCCESS;
		}else{
		    prpArrayInfo->plrIdx04FieldOrd[ilLoop] = -1;
		}/* end of for */
	    }/* end of for */
	}/* end of if */
    }/* end of if */
		

    if(ilRc == RC_SUCCESS && pcpIdx05FieldList != NULL)
    {
	prpArrayInfo->lrIdx05RowLen++;
	prpArrayInfo->pcrIdx05RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx05RowLen+1));
	if(prpArrayInfo->pcrIdx05RowBuf == NULL)
	{
	    ilRc = RC_FAIL;
	    dbg(TRACE,"SetArrayInfo: calloc failed");
	}/* end of if */
    }/* end of if */
	

    if(ilRc == RC_SUCCESS && pcpIdx05FieldList != NULL)
    {
	prpArrayInfo->lrIdx05FieldCnt = get_no_of_items(pcpIdx05FieldList);

	prpArrayInfo->plrIdx05FieldPos = (long *) calloc(prpArrayInfo->lrIdx05FieldCnt,sizeof(long));
	if(prpArrayInfo->plrIdx05FieldPos == NULL)
	{
	    dbg(TRACE,"SetArrayInfo: plrIdx05FieldPos calloc(%d,%d) failed",prpArrayInfo->lrIdx05FieldCnt,sizeof(long));
	    ilRc = RC_FAIL;
	}else{
	    *(prpArrayInfo->plrIdx05FieldPos) = -1;
	}/* end of if */
    }/* end of if */


    if(ilRc == RC_SUCCESS && pcpIdx05FieldList != NULL)
    {
	prpArrayInfo->plrIdx05FieldOrd = (long *) calloc(prpArrayInfo->lrIdx05FieldCnt,sizeof(long));
	if(prpArrayInfo->plrIdx05FieldOrd == NULL)
	{
	    dbg(TRACE,"SetArrayInfo: plrIdx05FieldOrd calloc(%d,%d) failed",prpArrayInfo->lrIdx05FieldCnt,sizeof(long));
	    ilRc = RC_FAIL;
	}else{
	    for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrIdx05FieldCnt ; ilLoop++)
	    {
		ilRc = get_real_item(&clOrderTxt[0],pcpIdx05Order,ilLoop+1);
		if(ilRc > 0)
		{
		    switch(clOrderTxt[0])
		    {
			case 'A' :
			    prpArrayInfo->plrIdx05FieldOrd[ilLoop] = ARR_ASC;
			    break;
			case 'D' :
			    prpArrayInfo->plrIdx05FieldOrd[ilLoop] = ARR_DESC;
			    break;
			default :
			    prpArrayInfo->plrIdx05FieldOrd[ilLoop] = ARR_DESC;
			    break;
		    }/* end of switch */

		    ilRc = RC_SUCCESS;
		}else{
		    prpArrayInfo->plrIdx05FieldOrd[ilLoop] = -1;
		}/* end of for */
	    }/* end of for */
	}/* end of if */
    }/* end of if */
	

    if(ilRc == RC_SUCCESS && pcpIdx06FieldList != NULL)
    {
	prpArrayInfo->lrIdx06RowLen++;
	prpArrayInfo->pcrIdx06RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx06RowLen+1));
	if(prpArrayInfo->pcrIdx06RowBuf == NULL)
	{
	    ilRc = RC_FAIL;
	    dbg(TRACE,"SetArrayInfo: calloc failed");
	}/* end of if */
    }/* end of if */
	

    if(ilRc == RC_SUCCESS && pcpIdx06FieldList != NULL)
    {
	prpArrayInfo->lrIdx06FieldCnt = get_no_of_items(pcpIdx06FieldList);

	prpArrayInfo->plrIdx06FieldPos = (long *) calloc(prpArrayInfo->lrIdx06FieldCnt,sizeof(long));
	if(prpArrayInfo->plrIdx06FieldPos == NULL)
	{
	    dbg(TRACE,"SetArrayInfo: plrIdx06FieldPos calloc(%d,%d) failed",prpArrayInfo->lrIdx06FieldCnt,sizeof(long));
	    ilRc = RC_FAIL;
	}else{
	    *(prpArrayInfo->plrIdx06FieldPos) = -1;
	}/* end of if */
    }/* end of if */


    if(ilRc == RC_SUCCESS && pcpIdx06FieldList != NULL)
    {
	prpArrayInfo->plrIdx06FieldOrd = (long *) calloc(prpArrayInfo->lrIdx06FieldCnt,sizeof(long));
	if(prpArrayInfo->plrIdx06FieldOrd == NULL)
	{
	    dbg(TRACE,"SetArrayInfo: plrIdx06FieldOrd calloc(%d,%d) failed",prpArrayInfo->lrIdx06FieldCnt,sizeof(long));
	    ilRc = RC_FAIL;
	}else{
	    for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrIdx06FieldCnt ; ilLoop++)
	    {
		ilRc = get_real_item(&clOrderTxt[0],pcpIdx06Order,ilLoop+1);
		if(ilRc > 0)
		{
		    switch(clOrderTxt[0])
		    {
			case 'A' :
			    prpArrayInfo->plrIdx06FieldOrd[ilLoop] = ARR_ASC;
			    break;
			case 'D' :
			    prpArrayInfo->plrIdx06FieldOrd[ilLoop] = ARR_DESC;
			    break;
			default :
			    prpArrayInfo->plrIdx06FieldOrd[ilLoop] = ARR_DESC;
			    break;
		    }/* end of switch */

		    ilRc = RC_SUCCESS;
		}else{
		    prpArrayInfo->plrIdx06FieldOrd[ilLoop] = -1;
		}/* end of for */
	    }/* end of for */
	}/* end of if */
    }/* end of if */
	
    if(ilRc == RC_SUCCESS && pcpIdx07FieldList != NULL)
    {
	prpArrayInfo->lrIdx07RowLen++;
	prpArrayInfo->pcrIdx07RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx07RowLen+1));
	if(prpArrayInfo->pcrIdx07RowBuf == NULL)
	{
	    ilRc = RC_FAIL;
	    dbg(TRACE,"SetArrayInfo: calloc failed");
	}/* end of if */
    }/* end of if */
	

    if(ilRc == RC_SUCCESS && pcpIdx07FieldList != NULL)
    {
	prpArrayInfo->lrIdx07FieldCnt = get_no_of_items(pcpIdx07FieldList);

	prpArrayInfo->plrIdx07FieldPos = (long *) calloc(prpArrayInfo->lrIdx07FieldCnt,sizeof(long));
	if(prpArrayInfo->plrIdx07FieldPos == NULL)
	{
	    dbg(TRACE,"SetArrayInfo: plrIdx07FieldPos calloc(%d,%d) failed",prpArrayInfo->lrIdx07FieldCnt,sizeof(long));
	    ilRc = RC_FAIL;
	}else{
	    *(prpArrayInfo->plrIdx07FieldPos) = -1;
	}/* end of if */
    }/* end of if */


    if(ilRc == RC_SUCCESS && pcpIdx07FieldList != NULL)
    {
	prpArrayInfo->plrIdx07FieldOrd = (long *) calloc(prpArrayInfo->lrIdx07FieldCnt,sizeof(long));
	if(prpArrayInfo->plrIdx07FieldOrd == NULL)
	{
	    dbg(TRACE,"SetArrayInfo: plrIdx07FieldOrd calloc(%d,%d) failed",prpArrayInfo->lrIdx07FieldCnt,sizeof(long));
	    ilRc = RC_FAIL;
	}else{
	    for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrIdx07FieldCnt ; ilLoop++)
	    {
		ilRc = get_real_item(&clOrderTxt[0],pcpIdx07Order,ilLoop+1);
		if(ilRc > 0)
		{
		    switch(clOrderTxt[0])
		    {
			case 'A' :
			    prpArrayInfo->plrIdx07FieldOrd[ilLoop] = ARR_ASC;
			    break;
			case 'D' :
			    prpArrayInfo->plrIdx07FieldOrd[ilLoop] = ARR_DESC;
			    break;
			default :
			    prpArrayInfo->plrIdx07FieldOrd[ilLoop] = ARR_DESC;
			    break;
		    }/* end of switch */

		    ilRc = RC_SUCCESS;
		}else{
		    prpArrayInfo->plrIdx07FieldOrd[ilLoop] = -1;
		}/* end of for */
	    }/* end of for */
	}/* end of if */
    }/* end of if */
	
    if(ilRc == RC_SUCCESS && pcpIdx08FieldList != NULL)
    {
	prpArrayInfo->lrIdx08RowLen++;
	prpArrayInfo->pcrIdx08RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx08RowLen+1));
	if(prpArrayInfo->pcrIdx08RowBuf == NULL)
	{
	    ilRc = RC_FAIL;
	    dbg(TRACE,"SetArrayInfo: calloc failed");
	}/* end of if */
    }/* end of if */
	

    if(ilRc == RC_SUCCESS && pcpIdx08FieldList != NULL)
    {
	prpArrayInfo->lrIdx08FieldCnt = get_no_of_items(pcpIdx08FieldList);

	prpArrayInfo->plrIdx08FieldPos = (long *) calloc(prpArrayInfo->lrIdx08FieldCnt,sizeof(long));
	if(prpArrayInfo->plrIdx08FieldPos == NULL)
	{
	    dbg(TRACE,"SetArrayInfo: plrIdx08FieldPos calloc(%d,%d) failed",prpArrayInfo->lrIdx08FieldCnt,sizeof(long));
	    ilRc = RC_FAIL;
	}else{
	    *(prpArrayInfo->plrIdx08FieldPos) = -1;
	}/* end of if */
    }/* end of if */


    if(ilRc == RC_SUCCESS && pcpIdx08FieldList != NULL)
    {
	prpArrayInfo->plrIdx08FieldOrd = (long *) calloc(prpArrayInfo->lrIdx08FieldCnt,sizeof(long));
	if(prpArrayInfo->plrIdx08FieldOrd == NULL)
	{
	    dbg(TRACE,"SetArrayInfo: plrIdx08FieldOrd calloc(%d,%d) failed",prpArrayInfo->lrIdx08FieldCnt,sizeof(long));
	    ilRc = RC_FAIL;
	}else{
	    for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrIdx08FieldCnt ; ilLoop++)
	    {
		ilRc = get_real_item(&clOrderTxt[0],pcpIdx08Order,ilLoop+1);
		if(ilRc > 0)
		{
		    switch(clOrderTxt[0])
		    {
			case 'A' :
			    prpArrayInfo->plrIdx08FieldOrd[ilLoop] = ARR_ASC;
			    break;
			case 'D' :
			    prpArrayInfo->plrIdx08FieldOrd[ilLoop] = ARR_DESC;
			    break;
			default :
			    prpArrayInfo->plrIdx08FieldOrd[ilLoop] = ARR_DESC;
			    break;
		    }/* end of switch */

		    ilRc = RC_SUCCESS;
		}else{
		    prpArrayInfo->plrIdx08FieldOrd[ilLoop] = -1;
		}/* end of for */
	    }/* end of for */
	}/* end of if */
    }/* end of if */
	

    if(ilRc == RC_SUCCESS)
    {
	if(plpAddFieldLens != NULL)
	{
	    dbg(TRACE,"SetArrayInfo: plpAddFieldLens <%ld> arrayname <%s>",plpAddFieldLens[0],pcpArrayName);
	}

	ilRc = CEDAArrayCreate(&(prpArrayInfo->rrArrayHandle),pcpArrayName,pcpTableName,pcpSelection,pcpAddFields,plpAddFieldLens,pcpArrayFieldList,&(prpArrayInfo->plrArrayFieldLen[0]),&(prpArrayInfo->plrArrayFieldOfs[0]));

	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"SetArrayInfo: CEDAArrayCreate failed <%d>",ilRc);
	}/* end of if */
    }/* end of if */

		

 
    if(ilRc == RC_SUCCESS && bpFill)
    {
	ilRc = CEDAArrayFill(&(prpArrayInfo->rrArrayHandle),pcpArrayName,NULL);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"SetArrayInfo: CEDAArrayFill failed <%d>",ilRc);
	}/* end of if */
    }/* end of if */
	

    if(ilRc == RC_SUCCESS && pcpIdx01Name != NULL)
    {
	ilRc = CEDAArrayCreateMultiIndex(&(prpArrayInfo->rrArrayHandle),pcpArrayName,&(prpArrayInfo->rrIdx01Handle),pcpIdx01Name,pcpIdx01FieldList,&(prpArrayInfo->plrIdx01FieldOrd[0]));

	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"SetArrayInfo: CEDAArrayCreateMultiIndex 1 failed <%d>",ilRc);
	}/* end of if */
    }/* end of if */
	

    if(ilRc == RC_SUCCESS && pcpIdx02Name != NULL)
    {
	ilRc = CEDAArrayCreateMultiIndex(&(prpArrayInfo->rrArrayHandle),pcpArrayName,&(prpArrayInfo->rrIdx02Handle),pcpIdx02Name,pcpIdx02FieldList,&(prpArrayInfo->plrIdx02FieldOrd[0]));

	if(ilRc != RC_SUCCESS )
	{
	    dbg(TRACE,"SetArrayInfo: CEDAArrayCreateMultiIndex 2 failed <%d>",ilRc);
	}/* end of if */
    }/* end of if */
	

    if(ilRc == RC_SUCCESS && pcpIdx03Name != NULL)
    {
	ilRc = CEDAArrayCreateMultiIndex(&(prpArrayInfo->rrArrayHandle),pcpArrayName,&(prpArrayInfo->rrIdx03Handle),pcpIdx03Name,pcpIdx03FieldList,&(prpArrayInfo->plrIdx03FieldOrd[0]));

	if(ilRc != RC_SUCCESS )
	{
	    dbg(TRACE,"SetArrayInfo: CEDAArrayCreateMulti 3 Indexfailed <%d>",ilRc);
	}/* end of if */
    }/* end of if */


    if(ilRc == RC_SUCCESS && pcpIdx04Name != NULL)
    {
	ilRc = CEDAArrayCreateMultiIndex(&(prpArrayInfo->rrArrayHandle),pcpArrayName,&(prpArrayInfo->rrIdx04Handle),pcpIdx04Name,pcpIdx04FieldList,&(prpArrayInfo->plrIdx04FieldOrd[0]));

	if(ilRc != RC_SUCCESS )
	{
	    dbg(TRACE,"SetArrayInfo: CEDAArrayCreateMulti 4 Indexfailed <%d>",ilRc);
	}/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS && pcpIdx05Name != NULL)
    {
	ilRc = CEDAArrayCreateMultiIndex(&(prpArrayInfo->rrArrayHandle),pcpArrayName,&(prpArrayInfo->rrIdx05Handle),pcpIdx05Name,pcpIdx05FieldList,&(prpArrayInfo->plrIdx05FieldOrd[0]));

	if(ilRc != RC_SUCCESS )
	{
	    dbg(TRACE,"SetArrayInfo: CEDAArrayCreateMulti 5 Indexfailed <%d>",ilRc);
	}/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS && pcpIdx06Name != NULL)
    {
	ilRc = CEDAArrayCreateMultiIndex(&(prpArrayInfo->rrArrayHandle),pcpArrayName,&(prpArrayInfo->rrIdx06Handle),pcpIdx06Name,pcpIdx06FieldList,&(prpArrayInfo->plrIdx06FieldOrd[0]));

	if(ilRc != RC_SUCCESS )
	{
	    dbg(TRACE,"SetArrayInfo: CEDAArrayCreateMulti 6 Indexfailed <%d>",ilRc);
	}/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS && pcpIdx07Name != NULL)
    {
	ilRc = CEDAArrayCreateMultiIndex(&(prpArrayInfo->rrArrayHandle),pcpArrayName,&(prpArrayInfo->rrIdx07Handle),pcpIdx07Name,pcpIdx07FieldList,&(prpArrayInfo->plrIdx07FieldOrd[0]));

	if(ilRc != RC_SUCCESS )
	{
	    dbg(TRACE,"SetArrayInfo: CEDAArrayCreateMulti 7 Indexfailed <%d>",ilRc);
	}/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS && pcpIdx08Name != NULL)
    {
	ilRc = CEDAArrayCreateMultiIndex(&(prpArrayInfo->rrArrayHandle),pcpArrayName,&(prpArrayInfo->rrIdx08Handle),pcpIdx08Name,pcpIdx08FieldList,&(prpArrayInfo->plrIdx08FieldOrd[0]));

	if(ilRc != RC_SUCCESS )
	{
	    dbg(TRACE,"SetArrayInfo: CEDAArrayCreateMulti 8 Indexfailed <%d>",ilRc);
	}/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS && bpUseTrigger) 
    { 
		if ( strncmp (pcpTableName,"AFT", 3 ) == 0 )
			ilRc = TriggerAction(pcpTableName,"IFR,UFR"); 
		else
			ilRc = TriggerAction(pcpTableName,"URT,IRT,DRT"); 
		 
		if(ilRc != RC_SUCCESS) 
		{ 
			dbg(TRACE,"SetArrayInfo: TriggerAction failed <%d>",ilRc); 
		}/* end of if */ 
    }/* end of if */ 
 	
 
    return(ilRc);
	
} /* end of SetArrayInfo */
/******************************************************************************/
/*                                                                            */
/******************************************************************************/

static int  SetAATArrayInfo(char *pcpArrayName,char *pcpArrayFieldList, long *plpFieldLens, 
			    char *pcpIdx01Name, char *pcpIdx01FieldList, char *pcpIdx01Order, 
			    char *pcpIdx02Name, char *pcpIdx02FieldList, char *pcpIdx02Order, 
			    char *pcpIdx03Name, char *pcpIdx03FieldList, char *pcpIdx03Order, 
			    ARRAYINFO *prpArrayInfo)
{
    int	ilRc   = RC_SUCCESS;				/* Return code */
    int ilLoop = 0;
    char clOrderTxt[4];

    if(prpArrayInfo == NULL)
    {
	dbg(TRACE,"SetArrayInfo: invalid last parameter : null pointer not valid");
	ilRc = RC_FAIL;
    }else{
	memset(prpArrayInfo,0x00,sizeof(prpArrayInfo));

	prpArrayInfo->rrArrayHandle = -1;
	prpArrayInfo->rrIdx01Handle = -1;
	prpArrayInfo->rrIdx02Handle = -1;
	prpArrayInfo->rrIdx03Handle = -1;

	if(pcpArrayName != NULL)
	{
	    if(strlen(pcpArrayName) <= ARR_NAME_LEN)
	    {
		strcpy(prpArrayInfo->crArrayName,pcpArrayName);
	    }/* end of if */
	}/* end of if */

	if(pcpArrayFieldList != NULL)
	{
	    if(strlen(pcpArrayFieldList) <= ARR_FLDLST_LEN)
	    {
		strcpy(prpArrayInfo->crArrayFieldList,pcpArrayFieldList);
	    }/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
	    prpArrayInfo->lrArrayFieldCnt = get_no_of_items(pcpArrayFieldList);


	    prpArrayInfo->plrArrayFieldOfs = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
	    if(prpArrayInfo->plrArrayFieldOfs == NULL)
	    {
		dbg(TRACE,"SetArrayInfo: plrArrayFieldOfs calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
		ilRc = RC_FAIL;
	    }/* end of if */
	    else
	    {
		prpArrayInfo->plrArrayFieldOfs[0] = 0;
		for(ilLoop = 1 ; ilLoop < prpArrayInfo->lrArrayFieldCnt ; ilLoop++)
		{
		    prpArrayInfo->plrArrayFieldOfs[ilLoop] = prpArrayInfo->plrArrayFieldOfs[ilLoop -1] + (plpFieldLens[ilLoop-1] +1);
		}/* end of for */
	    }
	}/* end of if */


	if(pcpIdx01Name != NULL)
	{
	    if(strlen(pcpIdx01Name) <= ARR_NAME_LEN)
	    {
		strcpy(prpArrayInfo->crIdx01Name,pcpIdx01Name);
	    }/* end of if */
	}/* end of if */

	if(pcpIdx01FieldList != NULL)
	{
	    if(strlen(pcpIdx01FieldList) <= ARR_FLDLST_LEN)
	    {
		strcpy(prpArrayInfo->crIdx01FieldList,pcpIdx01FieldList);
	    }/* end of if */
	}/* end of if */
	if(pcpIdx02Name != NULL)
	{
	    if(strlen(pcpIdx02Name) <= ARR_NAME_LEN)
	    {
		strcpy(prpArrayInfo->crIdx02Name,pcpIdx02Name);
	    }/* end of if */
	}/* end of if */

	if(pcpIdx02FieldList != NULL)
	{
	    if(strlen(pcpIdx02FieldList) <= ARR_FLDLST_LEN)
	    {
		strcpy(prpArrayInfo->crIdx02FieldList,pcpIdx02FieldList);
	    }/* end of if */
	}/* end of if */

	if(pcpIdx03Name != NULL)
	{
	    if(strlen(pcpIdx03Name) <= ARR_NAME_LEN)
	    {
		strcpy(prpArrayInfo->crIdx03Name,pcpIdx03Name);
	    }/* end of if */
	}/* end of if */

	if(pcpIdx03FieldList != NULL)
	{
	    if(strlen(pcpIdx03FieldList) <= ARR_FLDLST_LEN)
	    {
		strcpy(prpArrayInfo->crIdx03FieldList,pcpIdx03FieldList);
	    }/* end of if */
	}/* end of if */


    }/* end of if */


    if(ilRc == RC_SUCCESS)
    {
	prpArrayInfo->plrArrayFieldLen = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
	if(prpArrayInfo->plrArrayFieldLen == NULL)
	{
	    dbg(TRACE,"SetArrayInfo: plrArrayFieldLen calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
	    ilRc = RC_FAIL;
	}else{
	    *(prpArrayInfo->plrArrayFieldLen) = -1;
	}/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
       	ilRc = GetLogicalRowLength(pcpArrayFieldList,pcpArrayFieldList,plpFieldLens,&(prpArrayInfo->lrArrayRowLen));
       	if(ilRc != RC_SUCCESS)
       	{
	    dbg(TRACE,"SetArrayInfo: GetLogicalRowLength failed <%s> <%s>",pcpArrayName,pcpArrayFieldList);
      	}/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
	prpArrayInfo->lrArrayRowLen++;
	prpArrayInfo->pcrArrayRowBuf = (char *) calloc(1,(prpArrayInfo->lrArrayRowLen+1));
	if(prpArrayInfo->pcrArrayRowBuf == NULL)
	{
	    ilRc = RC_FAIL;
	    dbg(TRACE,"SetArrayInfo: calloc failed");
	}/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS && pcpIdx01Name != NULL)
    {
       	ilRc = GetLogicalRowLength(pcpArrayFieldList,pcpIdx01FieldList,plpFieldLens,&(prpArrayInfo->lrIdx01RowLen));
       	if(ilRc != RC_SUCCESS)
       	{
	    dbg(TRACE,"SetArrayInfo: GetLogicalRowLength failed <%s> <%s>",pcpIdx01Name,pcpIdx01FieldList);
       	}/* end of if */
    }/* end of if */
    if(ilRc == RC_SUCCESS && pcpIdx02Name != NULL)
    {
       	ilRc = GetLogicalRowLength(pcpArrayFieldList,pcpIdx02FieldList,plpFieldLens,&(prpArrayInfo->lrIdx02RowLen));
       	if(ilRc != RC_SUCCESS)
       	{
	    dbg(TRACE,"SetArrayInfo: GetLogicalRowLength failed <%s> <%s>",pcpIdx02Name,pcpIdx02FieldList);
       	}/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS && pcpIdx03Name != NULL)
    {
       	ilRc = GetLogicalRowLength(pcpArrayFieldList,pcpIdx03FieldList,plpFieldLens,&(prpArrayInfo->lrIdx03RowLen));
       	if(ilRc != RC_SUCCESS)
       	{
	    dbg(TRACE,"SetArrayInfo: GetLogicalRowLength failed <%s> <%s>",pcpIdx03Name,pcpIdx03FieldList);
       	}/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS && pcpIdx01Name != NULL)
    {
	prpArrayInfo->lrIdx01RowLen++;
	prpArrayInfo->pcrIdx01RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx01RowLen+1));
	if(prpArrayInfo->pcrIdx01RowBuf == NULL)
	{
	    ilRc = RC_FAIL;
	    dbg(TRACE,"SetArrayInfo: calloc failed");
	}/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS && pcpIdx01Name != NULL)
    {
	prpArrayInfo->lrIdx01FieldCnt = get_no_of_items(pcpIdx01FieldList);

	prpArrayInfo->plrIdx01FieldPos = (long *) calloc(prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
	if(prpArrayInfo->plrIdx01FieldPos == NULL)
	{
	    dbg(TRACE,"SetArrayInfo: plrIdx01FieldPos calloc(%d,%d) failed",prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
	    ilRc = RC_FAIL;
	}else{
	    *(prpArrayInfo->plrIdx01FieldPos) = -1;
	}/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS && pcpIdx01Name != NULL)
    {
	prpArrayInfo->plrIdx01FieldOrd = (long *) calloc(prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
	if(prpArrayInfo->plrIdx01FieldOrd == NULL)
	{
	    dbg(TRACE,"SetArrayInfo: plrIdx01FieldOrd calloc(%d,%d) failed",prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
	    ilRc = RC_FAIL;
	}else{
	    for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrIdx01FieldCnt ; ilLoop++)
	    {
		ilRc = get_real_item(&clOrderTxt[0],pcpIdx01Order,ilLoop+1);
		if(ilRc > 0)
		{
		    switch(clOrderTxt[0])
		    {
			case 'A' :
			    prpArrayInfo->plrIdx01FieldOrd[ilLoop] = ARR_ASC;
			    break;
			case 'D' :
			    prpArrayInfo->plrIdx01FieldOrd[ilLoop] = ARR_DESC;
			    break;
			default :
			    prpArrayInfo->plrIdx01FieldOrd[ilLoop] = ARR_DESC;
			    break;
		    }/* end of switch */

		    ilRc = RC_SUCCESS;
		}else{
		    prpArrayInfo->plrIdx01FieldOrd[ilLoop] = -1;
		}/* end of for */
	    }/* end of for */
	}/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS && pcpIdx02Name != NULL)
    {
	prpArrayInfo->lrIdx02RowLen++;
	prpArrayInfo->pcrIdx02RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx02RowLen+1));
	if(prpArrayInfo->pcrIdx02RowBuf == NULL)
	{
	    ilRc = RC_FAIL;
	    dbg(TRACE,"SetArrayInfo: calloc failed");
	}/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS && pcpIdx02Name != NULL)
    {
	prpArrayInfo->lrIdx02FieldCnt = get_no_of_items(pcpIdx02FieldList);

	prpArrayInfo->plrIdx02FieldPos = (long *) calloc(prpArrayInfo->lrIdx02FieldCnt,sizeof(long));
	if(prpArrayInfo->plrIdx02FieldPos == NULL)
	{
	    dbg(TRACE,"SetArrayInfo: plrIdx02FieldPos calloc(%d,%d) failed",prpArrayInfo->lrIdx02FieldCnt,sizeof(long));
	    ilRc = RC_FAIL;
	}else{
	    *(prpArrayInfo->plrIdx02FieldPos) = -1;
	}/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS && pcpIdx02Name != NULL)
    {
	prpArrayInfo->plrIdx02FieldOrd = (long *) calloc(prpArrayInfo->lrIdx02FieldCnt,sizeof(long));
	if(prpArrayInfo->plrIdx02FieldOrd == NULL)
	{
	    dbg(TRACE,"SetArrayInfo: plrIdx02FieldOrd calloc(%d,%d) failed",prpArrayInfo->lrIdx02FieldCnt,sizeof(long));
	    ilRc = RC_FAIL;
	}else{
	    for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrIdx02FieldCnt ; ilLoop++)
	    {
		ilRc = get_real_item(&clOrderTxt[0],pcpIdx02Order,ilLoop+1);
		if(ilRc > 0)
		{
		    switch(clOrderTxt[0])
		    {
			case 'A' :
			    prpArrayInfo->plrIdx02FieldOrd[ilLoop] = ARR_ASC;
			    break;
			case 'D' :
			    prpArrayInfo->plrIdx02FieldOrd[ilLoop] = ARR_DESC;
			    break;
			default :
			    prpArrayInfo->plrIdx02FieldOrd[ilLoop] = ARR_DESC;
			    break;
		    }/* end of switch */

		    ilRc = RC_SUCCESS;
		}else{
		    prpArrayInfo->plrIdx02FieldOrd[ilLoop] = -1;
		}/* end of for */
	    }/* end of for */
	}/* end of if */
    }/* end of if */


    if(ilRc == RC_SUCCESS && pcpIdx03Name != NULL)
    {
	prpArrayInfo->lrIdx03RowLen++;
	prpArrayInfo->pcrIdx03RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx03RowLen+1));
	if(prpArrayInfo->pcrIdx03RowBuf == NULL)
	{
	    ilRc = RC_FAIL;
	    dbg(TRACE,"SetArrayInfo: calloc failed");
	}/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS && pcpIdx03Name != NULL)
    {
	prpArrayInfo->lrIdx03FieldCnt = get_no_of_items(pcpIdx03FieldList);

	prpArrayInfo->plrIdx03FieldPos = (long *) calloc(prpArrayInfo->lrIdx03FieldCnt,sizeof(long));
	if(prpArrayInfo->plrIdx03FieldPos == NULL)
	{
	    dbg(TRACE,"SetArrayInfo: plrIdx03FieldPos calloc(%d,%d) failed",prpArrayInfo->lrIdx03FieldCnt,sizeof(long));
	    ilRc = RC_FAIL;
	}else{
	    *(prpArrayInfo->plrIdx03FieldPos) = -1;
	}/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS && pcpIdx03Name != NULL)
    {
	prpArrayInfo->plrIdx03FieldOrd = (long *) calloc(prpArrayInfo->lrIdx03FieldCnt,sizeof(long));
	if(prpArrayInfo->plrIdx03FieldOrd == NULL)
	{
	    dbg(TRACE,"SetArrayInfo: plrIdx03FieldOrd calloc(%d,%d) failed",prpArrayInfo->lrIdx03FieldCnt,sizeof(long));
	    ilRc = RC_FAIL;
	}else{
	    for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrIdx03FieldCnt ; ilLoop++)
	    {
		ilRc = get_real_item(&clOrderTxt[0],pcpIdx03Order,ilLoop+1);
		if(ilRc > 0)
		{
		    switch(clOrderTxt[0])
		    {
			case 'A' :
			    prpArrayInfo->plrIdx03FieldOrd[ilLoop] = ARR_ASC;
			    break;
			case 'D' :
			    prpArrayInfo->plrIdx03FieldOrd[ilLoop] = ARR_DESC;
			    break;
			default :
			    prpArrayInfo->plrIdx03FieldOrd[ilLoop] = ARR_DESC;
			    break;
		    }/* end of switch */

		    ilRc = RC_SUCCESS;
		}else{
		    prpArrayInfo->plrIdx03FieldOrd[ilLoop] = -1;
		}/* end of for */
	    }/* end of for */
	}/* end of if */
    }/* end of if */



    if(ilRc == RC_SUCCESS)
    {
		
	ilRc = AATArrayCreate(&(prpArrayInfo->rrArrayHandle),pcpArrayName,TRUE,1000,prpArrayInfo->lrArrayFieldCnt,plpFieldLens,pcpArrayFieldList,NULL);
	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"SetAAtArrayInfo: AATArrayCreate failed <%d>",ilRc);
	}/* end of if */
    }/* end of if */



    if(ilRc == RC_SUCCESS && pcpIdx01Name != NULL)
    {
	ilRc = CEDAArrayCreateMultiIndex(&(prpArrayInfo->rrArrayHandle),pcpArrayName,&(prpArrayInfo->rrIdx01Handle),pcpIdx01Name,pcpIdx01FieldList,&(prpArrayInfo->plrIdx01FieldOrd[0]));

	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"SetAAtArrayInfo: CEDAArrayCreateMultiIndexfailed <%d>",ilRc);
	}/* end of if */
    }/* end of if */
 
    if(ilRc == RC_SUCCESS && pcpIdx02Name != NULL)
    {
	ilRc = CEDAArrayCreateMultiIndex(&(prpArrayInfo->rrArrayHandle),pcpArrayName,&(prpArrayInfo->rrIdx02Handle),pcpIdx02Name,pcpIdx02FieldList,&(prpArrayInfo->plrIdx02FieldOrd[0]));

	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"SetAAtArrayInfo: CEDAArrayCreateMultiIndexfailed <%d>",ilRc);
	}/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS && pcpIdx03Name != NULL)
    {
	ilRc = CEDAArrayCreateMultiIndex(&(prpArrayInfo->rrArrayHandle),pcpArrayName,&(prpArrayInfo->rrIdx03Handle),pcpIdx03Name,pcpIdx03FieldList,&(prpArrayInfo->plrIdx03FieldOrd[0]));

	if(ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"SetAAtArrayInfo: CEDAArrayCreateMultiIndexfailed <%d>",ilRc);
	}/* end of if */
    }/* end of if */

    return(ilRc);
	
} /* end of SetAAtArrayInfo */

/*************************************************************************************/
/*************************************************************************************/

static void DebugPrintArrayInfo(int ipDebugLevel, ARRAYINFO *prpArrayInfo)
{
    int ilLoop = 0;
	
    if(prpArrayInfo == NULL)
    {
	dbg(ipDebugLevel,"DebugPrintArrayInfo: nothing to print");
	return;
    }/* end of if */

    dbg(ipDebugLevel,"DebugPrintArrayInfo: rrArrayHandle         <%d>",prpArrayInfo->rrArrayHandle);
    dbg(ipDebugLevel,"DebugPrintArrayInfo: crArrayName           <%s>",&(prpArrayInfo->crArrayName[0]));
    dbg(ipDebugLevel,"DebugPrintArrayInfo: crArrayFieldList      <%s>",&(prpArrayInfo->crArrayFieldList[0]));
    dbg(ipDebugLevel,"DebugPrintArrayInfo: lrArrayFieldCnt       <%d>",prpArrayInfo->lrArrayFieldCnt);
    dbg(ipDebugLevel,"DebugPrintArrayInfo: lrArrayRowLen         <%d>",prpArrayInfo->lrArrayRowLen);
    dbg(ipDebugLevel,"DebugPrintArrayInfo: pcrArrayRowBuf        <%8.8x>",prpArrayInfo->pcrArrayRowBuf);
    dbg(ipDebugLevel,"DebugPrintArrayInfo: plrArrayFieldLen      <%8.8x>",prpArrayInfo->plrArrayFieldLen);

    for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrArrayFieldCnt ; ilLoop++)
    {
	dbg(ipDebugLevel,"DebugPrintArrayInfo: plrArrayFieldLen[%3.3d] <%ld>",ilLoop,prpArrayInfo->plrArrayFieldLen[ilLoop]);
    }/* end of for */

    dbg(ipDebugLevel,"DebugPrintArrayInfo: plrArrayFieldOfs      <%8.8x>",prpArrayInfo->plrArrayFieldOfs);

    for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrArrayFieldCnt ; ilLoop++)
    {
	dbg(ipDebugLevel,"DebugPrintArrayInfo: plrArrayFieldOfs[%3.3d] <%ld>",ilLoop,prpArrayInfo->plrArrayFieldOfs[ilLoop]);
    }/* end of for */

    dbg(ipDebugLevel,"DebugPrintArrayInfo: rrIdx01Handle         <%d>",prpArrayInfo->rrIdx01Handle);
    dbg(ipDebugLevel,"DebugPrintArrayInfo: crIdx01Name           <%s>",&(prpArrayInfo->crIdx01Name[0]));
    dbg(ipDebugLevel,"DebugPrintArrayInfo: crIdx01FieldList      <%s>",&(prpArrayInfo->crIdx01FieldList[0]));
    dbg(ipDebugLevel,"DebugPrintArrayInfo: lrIdx01FieldCnt       <%d>",prpArrayInfo->lrIdx01FieldCnt);
    dbg(ipDebugLevel,"DebugPrintArrayInfo: lrIdx01RowLen         <%ld>",prpArrayInfo->lrIdx01RowLen);
    dbg(ipDebugLevel,"DebugPrintArrayInfo: pcrIdx01RowBuf        <%8.8x>",prpArrayInfo->pcrIdx01RowBuf);

    dbg(ipDebugLevel,"DebugPrintArrayInfo: plrIdx01FieldPos      <%8.8x>",prpArrayInfo->plrIdx01FieldPos);

    for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrIdx01FieldCnt ; ilLoop++)
    {
	dbg(ipDebugLevel,"DebugPrintArrayInfo: plrIdx01FieldPos[%3.3d] <%ld>",ilLoop,prpArrayInfo->plrIdx01FieldPos[ilLoop]);
    }/* end of for */

    dbg(ipDebugLevel,"DebugPrintArrayInfo: plrIdx01FieldOrd      <%8.8x>",prpArrayInfo->plrIdx01FieldOrd);

    for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrIdx01FieldCnt ; ilLoop++)
    {
	dbg(ipDebugLevel,"DebugPrintArrayInfo: plrIdx01FieldOrd[%3.3d] <%ld>",ilLoop,prpArrayInfo->plrIdx01FieldOrd[ilLoop]);
    }/* end of for */

} /* end of DebugPrintArrayInfo */


	

/******************************************************************************/
/******************************************************************************/
static int GetRowLength(char *pcpTana, char *pcpFieldList, long *plpLen)
{
    int	 ilRc        = RC_SUCCESS;			/* Return code */
    int  ilNoOfItems = 0;
    int  ilLoop      = 0;
    long llFldLen    = 0;
    long llRowLen    = 0;
    char clFina[8];
	
    ilNoOfItems = get_no_of_items(pcpFieldList);

    dbg(TRACE,"GetRowLength: Tana <%s> FieldList <%s> NoOfItems <%ld>",pcpTana,pcpFieldList,ilNoOfItems);
    ilLoop = 1;
    do
    {
	ilRc = get_real_item(&clFina[0],pcpFieldList,ilLoop);
	
	if(ilRc > 0)
	{
	    ilRc = RC_SUCCESS;
	    if(GetFieldLength(pcpTana,&clFina[0],&llFldLen) == RC_SUCCESS)
	    {
		llRowLen++;
		llRowLen += llFldLen;
	    }/* end of if */

	}/* end of if */
	ilLoop++;
    }while(ilLoop <= ilNoOfItems);


    *plpLen = llRowLen;

    dbg(TRACE,"GetRowLength: llRowLen <%ld>",llRowLen);

    return(ilRc);
	
} /* end of GetRowLength */

static int GetLogicalRowLength(char *pcpTotalFieldList,char *pcpFieldList, long *plpFieldSizes,long *plpLen)
{
    int	 ilRc        = RC_SUCCESS;			/* Return code */
    int  ilNoOfItems = 0;
    int  ilItemNo = 0;
    int  ilLoop      = 0;
    long llRowLen    = 0;
    char clFina[8];
	
    if (pcpTotalFieldList != NULL && pcpFieldList != NULL)
    {
	ilNoOfItems = get_no_of_items(pcpFieldList);

		
	ilLoop = 1;
	do
	{
	    get_real_item(clFina,pcpFieldList,ilLoop);

	    if(GetItemNo(clFina,pcpTotalFieldList,&ilItemNo) == RC_SUCCESS) 
	    {
		llRowLen++;
		llRowLen += plpFieldSizes[ilItemNo-1];
		dbg(TRACE,"GetLogicalRowLength:  clFina <%s> plpFieldSizes[ilItemNo-1] <%ld>",clFina,plpFieldSizes[ilItemNo-1]);
	    }

	    ilLoop++;
	}while(ilLoop <= ilNoOfItems);
    }
    if(ilRc == RC_SUCCESS)
    {
	*plpLen = llRowLen;
    }/* end of if */

    dbg(TRACE,"GetLogicalRowLength:  FieldList <%s> plpLen <%ld>",pcpFieldList,*plpLen);
    return(ilRc);
	
} /* end of GetLogicalRowLength */



/******************************************************************************/
/******************************************************************************/
static int GetFieldLength(char *pcpTana, char *pcpFina, long *plpLen)
{
    int  ilRc  = RC_SUCCESS;			/* Return code */
    int  ilCnt = 0;
    char clTaFi[32];
    char clFele[16];
    char clFldLst[16];

    ilCnt = 1;
    sprintf(&clTaFi[0],"%s,%s",pcpTana,pcpFina);
    sprintf(&clFldLst[0],"TANA,FINA"); /* wird von syslib zerst�rt - konstante nicht m�glich */

 
    ilRc = syslibSearchSystabData(&cgTabEnd[0],&clFldLst[0],&clTaFi[0],"FELE",&clFele[0],&ilCnt,"");
    switch (ilRc)
    {
	case RC_SUCCESS :
	    *plpLen = atoi(&clFele[0]);
	    /* dbg(DEBUG,"GetFieldLength: <%s,%s,%s> <%d>",pcpHopo,pcpTana,pcpFina,*plpLen);*/
	    break;

	case RC_NOT_FOUND :

	    dbg(TRACE,"GetFieldLength: pcpTana <%s> pcpFina<%s>",pcpTana,pcpFina);
	    dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_NOT_FOUND <%s>",ilRc,&clTaFi[0]);
	    break;

	case RC_FAIL :
	    dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_FAIL",ilRc);
	    break;

	default :
	    dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> unexpected",ilRc);
	    break;
    }/* end of switch */

    return(ilRc);
	
} /* end of GetFieldLength */



/******************************************************************************/
/******************************************************************************/

static int ProcessDemChange(char *pcpFields, char *pcpData)
{
    int	ilRc        = RC_SUCCESS; 
    int ilItemNoUrno;
    int ilItemNoDeen;
    int ilItemNoDebe;
    int ilItemNoAlid;
    char clUrno[20];
    char clDebe[24];
    char clDeen[24];
    char clAlid[24];
    char *pclFieldsRow = NULL;
    long llAction = ARR_FIRST;
    BOOL blDebeFound = FALSE;
    BOOL blDeenFound = FALSE;
    BOOL blDistributeNew = FALSE;
    BOOL blChangeAlid = TRUE;
	
    dbg(TRACE,"ProcessDemChange START");
    ilRc = GetItemNo("URNO",pcpFields,&ilItemNoUrno);
    if(ilRc == RC_SUCCESS)
    {
	get_real_item(clUrno,pcpData,ilItemNoUrno);
	ilRc = GetItemNo("DEEN",pcpFields,&ilItemNoDeen);
	if(ilRc == RC_SUCCESS) 
	{
	    blDeenFound = TRUE;
	}
	ilRc = GetItemNo("DEBE",pcpFields,&ilItemNoDebe);
	if(ilRc == RC_SUCCESS) 
	{
	    blDebeFound = TRUE;
	}
	if(blDebeFound || blDeenFound)
	{
		
	    dbg(DEBUG,"HandleData: ChangeJobTimeStart pcpData %s ,NoUrno %d , NoDebe %d , NoDeen %d" ,pcpData,ilItemNoUrno,ilItemNoDebe,ilItemNoDeen);
	    if(blDebeFound)
	    {
		get_real_item(clDebe,pcpData,ilItemNoDebe);
	    }
	    if(blDeenFound)
	    {
		get_real_item(clDeen,pcpData,ilItemNoDeen);
	    }
	    dbg(DEBUG,"ProcessDemChange: ChangeJobTimeStart DemUrno %s , Debe %s, Deen %s",clUrno,clDebe,clDeen);
	    ilRc = ChangeJobTime(clUrno,clDebe,blDebeFound,clDeen,blDeenFound);
	}
	 
	llAction = ARR_FIRST;

	while((AATArrayGetRowPointer(&(rgFieldsArray.rrArrayHandle),
				     &(rgFieldsArray.crArrayName[0]),llAction,(void *)&pclFieldsRow) == RC_SUCCESS) && !blDistributeNew)
	{
	    blDistributeNew = (GetItemNo(FIELDSFIELD(pclFieldsRow,igFieldsFnam),pcpFields,&ilItemNoDebe)
			       == RC_SUCCESS)?TRUE:FALSE;	
	    llAction = ARR_NEXT;
		  
	}
	if(blDistributeNew)
	{
	    if(PrepareSingleDemand(clUrno,&rgRequestOpenDemArray))
	    {
		AssignMAJobs(&rgRequestOpenDemArray,&rgPoolJobArray);
		blChangeAlid = FALSE;
	    }
	}
	if(blChangeAlid)
	{
	    if(GetItemNo("ALID",pcpFields,&ilItemNoAlid)
	       == RC_SUCCESS)
	    {
		get_real_item(clAlid,pcpData,ilItemNoAlid);
		ProcessChangeAlid(clUrno,clAlid);
	    }
	}
    }
    dbg(TRACE,"ProcessDemChange END");

    return ilRc;
}



/******************************************************************************/
/******************************************************************************/
static BOOL PrepareSingleDemand(char *pcpDemUrno,ARRAYINFO *prpOpenDemArray)
{
    BOOL blAssign = FALSE;
    char pclSelection[456];

    int ilRc = RC_SUCCESS;
    char clTpln[140];
    char clUlnk[20];
    char clUtpl[20];
    char clUrue[20];
    char clUpde[20];
    char clUnde[20];
    char clMaxd[20];
    char clMind[20];
    char clDide[20];
    char clRede[33];

    long llAction = ARR_FIRST;
    long llRowNum = ARR_FIRST;
    long llRowNum2 = ARR_FIRST;
    long llRowNum3 = ARR_FIRST;
    long llRowNum4 = ARR_FIRST;
    long llRowNum5 = ARR_FIRST;

    char *pclDemRow = NULL;
    char *pclOpenDemRow = NULL;
    char *pclRudRow = NULL;
    char *pclSerRow = NULL;
    char *pclAloRow = NULL;
    char *pclJodRow = NULL;
    char *pclJobRow = NULL;
    char *pclTplRow = NULL;
    char *pclRueRow = NULL;
    char *pclParRow = NULL;

    char clParKey[200];

    char clCurrTime[20];
    char clStatus[20];
    char clKey[9];
    char clFlgs[20];
	
    BOOL blAssignReally = FALSE;

    time_t tlDebe,tlDeen;

    BOOL blDeleteJobs;

    dbg(TRACE,"PrepareSingleDemand START");
    sprintf(clKey,"");
	
    GetServerTimeStamp("UTC", 1, 0, clCurrTime);

    StrToTime(clCurrTime,&tgCurrTime);

    llRowNum = ARR_FIRST;

    blAssign = FALSE;

    while(CEDAArrayFindRowPointer(&(rgJodArray.rrArrayHandle),
				  &(rgJodArray.crArrayName[0]),
				  &(rgJodArray.rrIdx01Handle),
				  &(rgJodArray.crIdx01Name[0]),
				  pcpDemUrno,&llRowNum,
				  (void *) &pclJodRow ) == RC_SUCCESS)
    {
	CEDAArrayPutField(&(rgJodArray.rrArrayHandle),&(rgJodArray.crArrayName[0]),NULL,"TCHX",
			  llRowNum,"x") ;	

	llRowNum2 = ARR_FIRST;
	if(CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
				   &(rgJobArray.crArrayName[0]),
				   &(rgJobArray.rrIdx02Handle),
				   &(rgJobArray.crIdx02Name[0]),
				   JODFIELD(pclJodRow,igJodUjob),&llRowNum2,
				   (void *) &pclJobRow ) == RC_SUCCESS)
	{
	    blAssign = TRUE;
	}
	llRowNum = ARR_NEXT;
    }

    if(blAssign)
    {
		sprintf(pclSelection,"WHERE URNO =  '%s'",pcpDemUrno);
		dbg(TRACE,"PrepareSingleDemand Dem Selection: %s",pclSelection);

		llAction = ARR_FIRST;
		CEDAArrayRefill(&(prpOpenDemArray->rrArrayHandle),&(prpOpenDemArray->crArrayName[0]),pclSelection,NULL,llAction);

			

		if(CEDAArrayFindRowPointer(&(prpOpenDemArray->rrArrayHandle),
					   &(prpOpenDemArray->crArrayName[0]),
					   &(prpOpenDemArray->rrIdx01Handle),
					   &(prpOpenDemArray->crIdx01Name[0]),
					   pcpDemUrno,&llAction,
					   (void *) &pclOpenDemRow ) == RC_SUCCESS)

		{
			blAssign = FALSE;
				
			llRowNum = ARR_FIRST;

			if(CheckAlocation(OPENDEMFIELD(pclOpenDemRow,igDemAloc),JOBFIELD(pclJobRow,igJobUalo), TRUE))
			{
				strcpy(clFlgs,OPENDEMFIELD(pclOpenDemRow,igDemFlgs));
				StrToTime(OPENDEMFIELD(pclOpenDemRow,igDemDebe),&tlDebe);
				StrToTime(OPENDEMFIELD(pclOpenDemRow,igDemDeen),&tlDeen);

				llRowNum2 = ARR_FIRST;
				dbg(TRACE,"PrepareSingleDemand Urud, <%s>",OPENDEMFIELD(pclOpenDemRow,igDemUrud));
				if(CEDAArrayFindRowPointer(&(rgRudArray.rrArrayHandle),
							   &(rgRudArray.crArrayName[0]),
							   &(rgRudArray.rrIdx01Handle),
							   &(rgRudArray.crIdx01Name[0]),
							   OPENDEMFIELD(pclOpenDemRow,igDemUrud),&llRowNum2,
							   (void *) &pclRudRow ) == RC_SUCCESS)
				{
					llRowNum3 = ARR_FIRST;
					dbg(TRACE,"PrepareSingleDemand Urue, <%s>",RUDFIELD(pclRudRow,igRudUrue));

					if(CEDAArrayFindRowPointer(&(rgRueArray.rrArrayHandle),
								   &(rgRueArray.crArrayName[0]),
								   &(rgRueArray.rrIdx01Handle),
								   &(rgRueArray.crIdx01Name[0]),
								   RUDFIELD(pclRudRow,igRudUrue),&llRowNum3,
								   (void *) &pclRueRow ) == RC_SUCCESS)
					{
						llRowNum4 = ARR_FIRST;

						strcpy ( clUtpl, RUEFIELD(pclRueRow,igRueUtpl) );											
						dbg(TRACE,"PrepareSingleDemand Utpl, <%s>", clUtpl );
			
						if(CEDAArrayFindRowPointer(&(rgTplArray.rrArrayHandle),
									   &(rgTplArray.crArrayName[0]),
									   &(rgTplArray.rrIdx01Handle),
									   &(rgTplArray.crIdx01Name[0]),
									   RUEFIELD(pclRueRow,igRueUtpl),&llRowNum4,
									   (void *) &pclTplRow ) == RC_SUCCESS)
						{
							llRowNum5 = ARR_FIRST;
							sprintf(clParKey,"DISTNEW,%s",TPLFIELD(pclTplRow,igTplTnam));
							sprintf(clTpln,"%s",TPLFIELD(pclTplRow,igTplTnam));
							dbg(TRACE,"PrepareSingleDemand clParKey, <%s>",clParKey);
							if(CEDAArrayFindRowPointer(&(rgParArray.rrArrayHandle),
										   &(rgParArray.crArrayName[0]),
										   &(rgParArray.rrIdx01Handle),
										   &(rgParArray.crIdx01Name[0]),
										   clParKey,&llRowNum5,
										   (void *) &pclParRow ) == RC_SUCCESS)
							{							
								blAssignReally = (atol(PARFIELD(pclParRow,igParValu)) == 1)?TRUE:FALSE;
												
								blAssign = TRUE;
								dbg(TRACE,"PrepareSingleDemand blAssignReally, <%d>",blAssignReally);
							}
						}
					}
				}
			}
			if(blAssign)
			{
		 
				if((tlDebe < tgCurrTime + igAssignOffSetStart || tlDebe > tgCurrTime + igAssignOffSetEnd))
				{
					blAssign = FALSE;
				}		 
				else
				{
					blAssign = FALSE;
					llRowNum = ARR_FIRST;
					while(CEDAArrayFindRowPointer(&(rgJodArray.rrArrayHandle),
								  &(rgJodArray.crArrayName[0]),
								  &(rgJodArray.rrIdx03Handle),
								  &(rgJodArray.crIdx03Name[0]),
								  "x",&llRowNum,
								  (void *) &pclJodRow ) == RC_SUCCESS)
					{
					blDeleteJobs = TRUE;
					llRowNum2 = ARR_FIRST;
					if(CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
								   &(rgJobArray.crArrayName[0]),
								   &(rgJobArray.rrIdx02Handle),
								   &(rgJobArray.crIdx02Name[0]),
								   JODFIELD(pclJodRow,igJodUjob),&llRowNum2,
								   (void *) &pclJobRow ) == RC_SUCCESS)
					{

						blDeleteJobs = FALSE;
						strcpy(clStatus,JOBFIELD(pclJobRow,igJobStat));
						if((clStatus[0] == 'P' )&& (clStatus[2] != '1' ) && (clStatus[4] != '1' ))
						{
						CEDAArrayDeleteRow(&(rgJobArray.rrArrayHandle),&(rgJobArray.crArrayName[0]),llRowNum2);
						blDeleteJobs = TRUE;
						blAssign = TRUE;
						}
					}
					if(blDeleteJobs)
					{
						CEDAArrayDeleteRow(&(rgJodArray.rrArrayHandle),&(rgJodArray.crArrayName[0]),llRowNum);
						llRowNum = ARR_FIRST;
					}
					else
					{
						CEDAArrayPutField(&(rgJodArray.rrArrayHandle),&(rgJodArray.crArrayName[0]),NULL,"TCHX",
								  llRowNum," ") ;	
						llRowNum = ARR_NEXT;
					}
					}
				}

				if(clFlgs[1] != '1')
				{
					if ( IsDemandToAssign ( clFlgs ) )
					/*  if(clFlgs[5] != '1') hag20021002 */

					{
						if(blAssign )
						{
							llRowNum = ARR_FIRST;
							if(CEDAArrayFindRowPointer(&(rgRudArray.rrArrayHandle),
										   &(rgRudArray.crArrayName[0]),
										   &(rgRudArray.rrIdx01Handle),
										   &(rgRudArray.crIdx01Name[0]),
										   OPENDEMFIELD(pclOpenDemRow,igDemUrud),&llRowNum,
										   (void *) &pclRudRow ) == RC_SUCCESS)
							{
								llRowNum = ARR_FIRST;
								sprintf(clUlnk,"%s",RUDFIELD(pclRudRow,igRudUlnk));
								sprintf(clUrue,"%s",RUDFIELD(pclRudRow,igRudUrue));
								sprintf(clUpde,"%s",RUDFIELD(pclRudRow,igRudUpde));
								sprintf(clUnde,"%s",RUDFIELD(pclRudRow,igRudUnde));
								sprintf(clMaxd,"%s",RUDFIELD(pclRudRow,igRudMaxd));
								sprintf(clMind,"%s",RUDFIELD(pclRudRow,igRudMind));
								sprintf(clDide,"%s",RUDFIELD(pclRudRow,igRudDide));
								sprintf(clRede,"%s",RUDFIELD(pclRudRow,igRudRede));


								if(CEDAArrayFindRowPointer(&(rgSerArray.rrArrayHandle),
											   &(rgSerArray.crArrayName[0]),
											   &(rgSerArray.rrIdx01Handle),
											   &(rgSerArray.crIdx01Name[0]),
											   RUDFIELD(pclRudRow,igRudUghs),&llRowNum,
											   (void *) &pclSerRow ) == RC_SUCCESS)
								{

									CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),
											  &(prpOpenDemArray->crArrayName[0]),NULL,"RTWF",llAction,SERFIELD(pclSerRow,igSerRtwf)) ;
									CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),
											  &(prpOpenDemArray->crArrayName[0]),NULL,"RTWT",llAction,SERFIELD(pclSerRow,igSerRtwt)) ;
								}

							}

							CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),
									  &(prpOpenDemArray->crArrayName[0]),NULL,"TNAM",llAction,clTpln) ;
							CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),
									  &(prpOpenDemArray->crArrayName[0]),NULL,"ULNK",llAction,clUlnk) ;
							CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),
									  &(prpOpenDemArray->crArrayName[0]),NULL,"URUE",llAction,clUrue) ;
							CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),
									  &(prpOpenDemArray->crArrayName[0]),NULL,"UPDE",llAction,"0") ;
							CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),
									  &(prpOpenDemArray->crArrayName[0]),NULL,"UNDE",llAction,"0") ;
							CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),
									  &(prpOpenDemArray->crArrayName[0]),NULL,"MAXD",llAction,clMaxd) ;
							CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),
									  &(prpOpenDemArray->crArrayName[0]),NULL,"MIND",llAction,clMind) ;
							CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),
									  &(prpOpenDemArray->crArrayName[0]),NULL,"DIDE",llAction,clDide) ;
							CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),
									  &(prpOpenDemArray->crArrayName[0]),NULL,"TCHD",llAction,"1") ;
							CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),
									  &(prpOpenDemArray->crArrayName[0]),NULL,"SIX1",llAction,"1") ;
							CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),
									  &(prpOpenDemArray->crArrayName[0]),NULL,"SIX2",llAction,"1") ;
							CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),
									  &(prpOpenDemArray->crArrayName[0]),NULL,"UTPL",llAction,clUtpl) ;

							dbg(TRACE,"PrepareSingleDemand clRede <%s> ",clRede);

							if(atol(clUpde) > 0)
							{
								if(clRede[0] == '1')
								{
									CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),
											  &(prpOpenDemArray->crArrayName[0]),NULL,"UPDE",llAction,clUpde) ;
								}
							}
							if(atol(clUnde) > 0)
							{
								if(clRede[2] == '1')
								{
									CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),
											  &(prpOpenDemArray->crArrayName[0]),NULL,"UNDE",llAction,clUnde) ;
								}
							}

							llRowNum = ARR_FIRST;
							if(CEDAArrayFindRowPointer(&(rgAloArray.rrArrayHandle),
										   &(rgAloArray.crArrayName[0]),
										   &(rgAloArray.rrIdx02Handle),
										   &(rgAloArray.crIdx02Name[0]),
										   OPENDEMFIELD(pclOpenDemRow,igDemAloc),&llRowNum,
										   (void *) &pclAloRow ) == RC_SUCCESS)
							{
								if(CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),&(prpOpenDemArray->crArrayName[0]),NULL,"UALO",llAction,ALOFIELD(pclAloRow,igAloUrno)) != RC_SUCCESS)
								{
									dbg(TRACE,"PrepareDemData Put Field UALO failed: %s",ALOFIELD(pclAloRow,igAloUrno));
								}
							}
							if(strncmp(OPENDEMFIELD(pclOpenDemRow,igDemRety),"100",3) == 0)
							{
								PrepareDemPfcArray(OPENDEMFIELD(pclOpenDemRow,igDemUrno),OPENDEMFIELD(pclOpenDemRow,igDemUrud));
								PrepareDemPrqArray(OPENDEMFIELD(pclOpenDemRow,igDemUrno),OPENDEMFIELD(pclOpenDemRow,igDemUrud));
								PrepareDemPoolArray(OPENDEMFIELD(pclOpenDemRow,igDemAlid),OPENDEMFIELD(pclOpenDemRow,igDemAloc));
							}	
						}
					}
					else
					{
						blAssignReally = FALSE;
					}
				}
				else
				{
					blAssignReally = FALSE;
					if(blAssign)
					{
						CEDAArrayDeleteRow(&(prpOpenDemArray->rrArrayHandle),
								   &(prpOpenDemArray->crArrayName[0]),llAction);
						ilRc = CEDAArrayWriteDB(&(prpOpenDemArray->rrArrayHandle),&(prpOpenDemArray->crArrayName[0]),
									NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);
					}
				}

					
				ilRc = CEDAArrayWriteDB(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);
				if (ilRc != RC_SUCCESS)
				{
					int ilOldDebugLevel = debug_level;
					debug_level = TRACE;
					dbg(TRACE,"PrepareSingleDemand Error writing to JOBTAB ");
					debug_level = ilOldDebugLevel;

				}
				ilRc = CEDAArrayWriteDB(&rgJodArray.rrArrayHandle,rgJodArray.crArrayName,NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);
				if (ilRc != RC_SUCCESS)
				{
					int ilOldDebugLevel = debug_level;
					debug_level = TRACE;
					dbg(TRACE,"PrepareSingleDemand Error writing to JODTAB ");
					debug_level = ilOldDebugLevel;

				}

			}
		}
    }

    llRowNum = ARR_FIRST;
    while(CEDAArrayFindRowPointer(&(rgJodArray.rrArrayHandle),
				  &(rgJodArray.crArrayName[0]),
				  &(rgJodArray.rrIdx03Handle),
				  &(rgJodArray.crIdx03Name[0]),
				  "x",&llRowNum,
				  (void *) &pclJodRow ) == RC_SUCCESS)
    {
	CEDAArrayPutField(&(rgJodArray.rrArrayHandle),&(rgJodArray.crArrayName[0]),NULL,"TCHX",
			  llRowNum," ") ;	
	llRowNum = ARR_NEXT;
    }
    ilRc = CEDAArrayWriteDB(&rgJodArray.rrArrayHandle,rgJodArray.crArrayName,NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);
    if (ilRc != RC_SUCCESS)
    {
	int ilOldDebugLevel = debug_level;
	debug_level = TRACE;
	dbg(TRACE,"PrepareSingleDemand 2 Error writing to JODTAB ");
	debug_level = ilOldDebugLevel;

    }



    dbg(TRACE,"PrepareSingleDemand END blAssign <%d>",blAssign);
 
    return (blAssignReally && blAssign);

}


static int ProcessChangeAlid(char *pcpDemUrno,char *pcpAlid)
{
    int ilRc = RC_SUCCESS;
    long llAction = ARR_FIRST;
    long llRowNum = ARR_FIRST;
    long llNext = ARR_FIRST;
    long llJobRowNum = -1;
    char *pclDot;
    char clSqlBuf[124];
    short slCursor;
    short slSqlFunc;

    char clXYTab[12];
    char clXYName[12];
    static char clXYUrno[20];
    char clKey[20];
    char clAlidKey[40];

    char *pclJobRow = NULL;
    char *pclJodRow = NULL;
    char *pclAlidListRow = NULL;
    char *pclAloRow = NULL;

    BOOL blUaidFound = FALSE;
    dbg ( DEBUG, "ProcessChangeAlid: UDEM <%s> ALID <%s>", pcpDemUrno, pcpAlid );
    while(CEDAArrayFindRowPointer(&(rgJodArray.rrArrayHandle),
				  &(rgJodArray.crArrayName[0]),
				  &(rgJodArray.rrIdx01Handle),
				  &(rgJodArray.crIdx01Name[0]),
				  pcpDemUrno,&llAction,
				  (void *) &pclJodRow ) == RC_SUCCESS)
    {
	llRowNum = ARR_FIRST;
	dbg ( DEBUG, "ProcessChangeAlid: found JOD row <%d> UDEM <%s> UJOB <%s>", llAction,
	      JODFIELD(pclJodRow,igJodUdem), JODFIELD(pclJodRow,igJodUjob) );
	if(CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
				   &(rgJobArray.crArrayName[0]),
				   &(rgJobArray.rrIdx02Handle),
				   &(rgJobArray.crIdx02Name[0]),
				   JODFIELD(pclJodRow,igJodUjob),&llRowNum,
				   (void *) &pclJobRow ) == RC_SUCCESS)
	{
	    llJobRowNum = llRowNum;
	    llRowNum = ARR_FIRST;
	    dbg ( DEBUG, "ProcessChangeAlid: found JOB row <%d> URNO <%s> UALO <%s>", llJobRowNum,
		  JOBFIELD(pclJobRow,igJobUrno), JOBFIELD(pclJobRow,igJobUalo) );
	    sprintf(clAlidKey,"%s,%s",JOBFIELD(pclJobRow,igJobUalo),pcpAlid);
	    if( AATArrayFindRowPointer(&(rgAlidListArray.rrArrayHandle),&(rgAlidListArray.crArrayName[0]),
				       &(rgAlidListArray.rrIdx03Handle),
				       &(rgAlidListArray.crIdx03Name[0]),
				       clAlidKey,&llRowNum,
				       (void *) &pclAlidListRow ) == RC_SUCCESS)
	    {
		sprintf(clXYUrno,ALIDLISTFIELD(pclAlidListRow,igAlidListUaid));
		blUaidFound = TRUE;
	    }
	    else
	    {
		llRowNum = ARR_FIRST;
			
		if(CEDAArrayFindRowPointer(&(rgAloArray.rrArrayHandle),
					   &(rgAloArray.crArrayName[0]),
					   &(rgAloArray.rrIdx01Handle),
					   &(rgAloArray.crIdx01Name[0]),
					   JOBFIELD(pclJobRow,igJobUalo),&llRowNum,
					   (void *) &pclAloRow ) == RC_SUCCESS)
		{
		    sprintf(clKey,ALOFIELD(pclAloRow,igAloReft));


		    if ((pclDot = strchr(clKey,'.')) != NULL)
		    {
			pclDot++;
			strcpy(clXYName,pclDot);
			pclDot--;
			*pclDot = '\0';
			strcpy(clXYTab,clKey);
			sprintf(clSqlBuf,
				"SELECT URNO FROM %sTAB WHERE %s = '%s' AND HOPO = '%s'",clXYTab,clXYName,pcpAlid,cgHopo);
			slSqlFunc = START;
			slCursor = 0;
			ilRc = sql_if(slSqlFunc,&slCursor,clSqlBuf,clXYUrno);
			if(ilRc == RC_SUCCESS)
			{
			    llNext = ARR_NEXT;
			    blUaidFound = TRUE;
			    InitializeAlidListRow(cgAlidListBuf);
								
			    ilRc = AATArrayAddRow(&(rgAlidListArray.rrArrayHandle),&(rgAlidListArray.crArrayName[0]),&llNext,(void *)cgAlidListBuf);
			    AATArrayPutField(&(rgAlidListArray.rrArrayHandle),
					     &(rgAlidListArray.crArrayName[0]),NULL,"ALID",llNext,TRUE,pcpAlid);
			    AATArrayPutField(&(rgAlidListArray.rrArrayHandle),
					     &(rgAlidListArray.crArrayName[0]),NULL,"ALOC",llNext,TRUE,ALOFIELD(pclAloRow,igAloAloc));
			    AATArrayPutField(&(rgAlidListArray.rrArrayHandle),
					     &(rgAlidListArray.crArrayName[0]),NULL,"UAID",llNext,TRUE,clXYUrno);
			    AATArrayPutField(&(rgAlidListArray.rrArrayHandle),
					     &(rgAlidListArray.crArrayName[0]),NULL,"UALO",llNext,TRUE,ALOFIELD(pclAloRow,igAloUrno));

			}
			else
			    dbg ( TRACE, "ProcessChangeAlid: ALID <%s> not found as <%s> in %sTAB", 
				  pcpAlid,clXYName, clXYTab );
			close_my_cursor(&slCursor);
			commit_work();
			slCursor = 0;
		    }
		}
	    }
	}

	if(blUaidFound)
	{
	    if ( llJobRowNum > -1 )
	    {
		dbg ( DEBUG, "ProcessChangeAlid: Found JobRow <%ld> URNO <%s> UAID old <%s> UAID new <%s>",
		      llJobRowNum, JOBFIELD(pclJobRow,igJobUrno), JOBFIELD(pclJobRow,igJobUaid), clXYUrno );
		CEDAArrayPutField(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,"UAID",llJobRowNum,clXYUrno);
		CEDAArrayPutField(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,"ALID",llJobRowNum,pcpAlid);
		ilRc = CEDAArrayWriteDB(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);
		if (ilRc != RC_SUCCESS)
		{
		    int ilOldDebugLevel = debug_level;
		    if ( ilOldDebugLevel < TRACE )
			debug_level = TRACE;
		    dbg(TRACE,"ProcessChangeAlid Error writing to JOBTAB ");
		    debug_level = ilOldDebugLevel;
		}
	    }
	    else
		dbg ( TRACE, "ProcessChangeAlid: Job URNO <%s> not found",
		      JODFIELD(pclJodRow,igJodUjob) );
	}

	llAction = ARR_NEXT;
    }
    return RC_SUCCESS;
}

/******************************************************************************/
/******************************************************************************/

static int InitializeDemPfcRow(char *pcpDemPfcBuf)
{
    int ilRc = RC_SUCCESS;
	
    memset(pcpDemPfcBuf,0,rgDemPfcArray.lrArrayRowLen);


    strcpy(DEMPFCFIELD(pcpDemPfcBuf,igDemPfcFcod)," ");
    strcpy(DEMPFCFIELD(pcpDemPfcBuf,igDemPfcUdem)," ");
    strcpy(DEMPFCFIELD(pcpDemPfcBuf,igDemPfcStat)," ");
    strcpy(DEMPFCFIELD(pcpDemPfcBuf,igDemPfcUpjb),"0");

    return ilRc;
}

static int InitializeDemPrqRow(char *pcpDemPrqBuf)
{
    int ilRc = RC_SUCCESS;
	
    memset(pcpDemPrqBuf,0,rgDemPrqArray.lrArrayRowLen);

    strcpy(DEMPRQFIELD(pcpDemPrqBuf,igDemPrqQcod)," ");
    strcpy(DEMPRQFIELD(pcpDemPrqBuf,igDemPrqUdem)," ");
    strcpy(DEMPRQFIELD(pcpDemPrqBuf,igDemPrqStat)," ");
    strcpy(DEMPRQFIELD(pcpDemPrqBuf,igDemPrqUpjb),"0");

    return ilRc;
}

static int InitializeDemPoolRow(char *pcpDemPoolBuf)
{
    int ilRc = RC_SUCCESS;
	
    memset(pcpDemPoolBuf,0,rgDemPoolArray.lrArrayRowLen);

    strcpy(DEMPOOLFIELD(pcpDemPoolBuf,igDemPoolAlid)," ");
    strcpy(DEMPOOLFIELD(pcpDemPoolBuf,igDemPoolAloc)," ");
    strcpy(DEMPOOLFIELD(pcpDemPoolBuf,igDemPoolUpol)," ");
    strcpy(DEMPOOLFIELD(pcpDemPoolBuf,igDemPoolUaid)," ");

    return ilRc;
}

static int InitializeDemIdxRow(char *pcpDemIdxBuf)
{
    int ilRc = RC_SUCCESS;
	
    memset(pcpDemIdxBuf,0,rgDemIdxArray.lrArrayRowLen);

    strcpy(DEMIDXFIELD(pcpDemIdxBuf,igDemIdxOuri)," ");
    strcpy(DEMIDXFIELD(pcpDemIdxBuf,igDemIdxOuro)," ");
    strcpy(DEMIDXFIELD(pcpDemIdxBuf,igDemIdxTpln)," ");
    strcpy(DEMIDXFIELD(pcpDemIdxBuf,igDemIdxSern)," ");
    strcpy(DEMIDXFIELD(pcpDemIdxBuf,igDemIdxKcnt),"1");

    return ilRc;
}

static int InitializePoolPerRow(char *pcpPoolPerBuf)
{
    int ilRc = RC_SUCCESS;
	
    memset(pcpPoolPerBuf,' ',rgPoolPerArray.lrArrayRowLen);

    strcpy(POOLPERFIELD(pcpPoolPerBuf,igPoolPerUjob)," ");
    strcpy(POOLPERFIELD(pcpPoolPerBuf,igPoolPerPerm)," ");
    strcpy(POOLPERFIELD(pcpPoolPerBuf,igPoolPerPrio),"1");

    return ilRc;
}

static int InitializePoolFctRow(char *pcpPoolFctBuf)
{
    int ilRc = RC_SUCCESS;
	
    memset(pcpPoolFctBuf,' ',rgPoolFctArray.lrArrayRowLen);

    strcpy(POOLFCTFIELD(pcpPoolFctBuf,igPoolFctUjob)," ");
    strcpy(POOLFCTFIELD(pcpPoolFctBuf,igPoolFctFctc)," ");
    strcpy(POOLFCTFIELD(pcpPoolFctBuf,igPoolFctPrio),"1");

    return ilRc;
}

static int InitializeTmpUrnoRow(char *pcpTmpUrnoBuf,int ipAidx)
{
    int ilRc = RC_SUCCESS;
    char clAidx[3];

    sprintf(clAidx,"%d",ipAidx);
	
    memset(pcpTmpUrnoBuf,0,rgTmpUrnoArray.lrArrayRowLen);

    strcpy(TMPURNOFIELD(pcpTmpUrnoBuf,igTmpUrnoStat),"x");
    strcpy(TMPURNOFIELD(pcpTmpUrnoBuf,igTmpUrnoAidx),clAidx);
    strcpy(TMPURNOFIELD(pcpTmpUrnoBuf,igTmpUrnoUrno)," ");

    return ilRc;
}


static int InitializeJobTypeRow(char *pcpJobTypeBuf)
{
    int ilRc = RC_SUCCESS;
	
    memset(pcpJobTypeBuf,' ',rgJobTypeArray.lrArrayRowLen);

    strcpy(JOBTYPEFIELD(pcpJobTypeBuf,igJobTypeDety),"1");
    strcpy(JOBTYPEFIELD(pcpJobTypeBuf,igJobTypeAloc),"2");
    strcpy(JOBTYPEFIELD(pcpJobTypeBuf,igJobTypeJtyp),"3");

    return ilRc;
}

static int InitializeKeyUrnoListRow(char *pcpKeyUrnoListBuf)
{
    int ilRc = RC_SUCCESS;
	
    memset(pcpKeyUrnoListBuf,' ',rgKeyUrnoListArray.lrArrayRowLen);

    strcpy(KEYURNOLISTFIELD(pcpKeyUrnoListBuf,igKeyUrnoListXKey)," ");
    strcpy(KEYURNOLISTFIELD(pcpKeyUrnoListBuf,igKeyUrnoListUrno)," ");
    strcpy(KEYURNOLISTFIELD(pcpKeyUrnoListBuf,igKeyUrnoListBegi)," ");
    strcpy(KEYURNOLISTFIELD(pcpKeyUrnoListBuf,igKeyUrnoListEnde)," ");
    strcpy(KEYURNOLISTFIELD(pcpKeyUrnoListBuf,igKeyUrnoListUdem)," ");

    return ilRc;
}

static int InitializeSwgIndexRow(char *pcpSwgIndexBuf)
{
    int ilRc = RC_SUCCESS;
	
    memset(pcpSwgIndexBuf,' ',rgSwgIndexArray.lrArrayRowLen);

    strcpy(SWGINDEXFIELD(pcpSwgIndexBuf,igSwgIndexCode)," ");
    strcpy(SWGINDEXFIELD(pcpSwgIndexBuf,igSwgIndexSize),"0");

    return ilRc;
}

static int InitializeMatchRow(char *pcpMatchBuf)
{
    int ilRc = RC_SUCCESS;
	
    memset(pcpMatchBuf,' ',rgMatchArray.lrArrayRowLen);

    strcpy(MATCHFIELD(pcpMatchBuf,igMatchUpjb)," ");
    strcpy(MATCHFIELD(pcpMatchBuf,igMatchUrud)," ");
    strcpy(MATCHFIELD(pcpMatchBuf,igMatchUdem)," ");
    strcpy(MATCHFIELD(pcpMatchBuf,igMatchStat)," ");
    strcpy(MATCHFIELD(pcpMatchBuf,igMatchTsta)," ");
    strcpy(MATCHFIELD(pcpMatchBuf,igMatchMark)," ");
    strcpy(MATCHFIELD(pcpMatchBuf,igMatchPrio)," ");
    strcpy(MATCHFIELD(pcpMatchBuf,igMatchDpri)," ");
    strcpy(MATCHFIELD(pcpMatchBuf,igMatchJpri)," ");

    return ilRc;
}
static int InitializeGroupListRow(char *pcpGroupListBuf)
{
    int ilRc = RC_SUCCESS;
	
    memset(pcpGroupListBuf,' ',rgGroupListArray.lrArrayRowLen);

    strcpy(GROUPLISTFIELD(pcpGroupListBuf,igGroupListUpjb)," ");
    strcpy(GROUPLISTFIELD(pcpGroupListBuf,igGroupListUdem)," ");
    strcpy(GROUPLISTFIELD(pcpGroupListBuf,igGroupListGrpn)," ");
    strcpy(GROUPLISTFIELD(pcpGroupListBuf,igGroupListValu)," ");

    return ilRc;
}

static int InitializeGroupNameRow(char *pcpGroupNameBuf)
{
    int ilRc = RC_SUCCESS;
	
    memset(pcpGroupNameBuf,' ',rgGroupNameArray.lrArrayRowLen);

    strcpy(GROUPNAMEFIELD(pcpGroupNameBuf,igGroupNameWgpc)," ");

    return ilRc;
}

static int InitializeAlidListRow(char *pcpAlidListBuf)
{
    int ilRc = RC_SUCCESS;
	
    memset(pcpAlidListBuf,' ',rgAlidListArray.lrArrayRowLen);

    strcpy(ALIDLISTFIELD(pcpAlidListBuf,igAlidListAlid)," ");
    strcpy(ALIDLISTFIELD(pcpAlidListBuf,igAlidListAloc)," ");
    strcpy(ALIDLISTFIELD(pcpAlidListBuf,igAlidListUaid)," ");
    strcpy(ALIDLISTFIELD(pcpAlidListBuf,igAlidListUalo)," ");

    return ilRc;
}

static int InitializeFieldsRow(char *pcpFieldsBuf,char *pcpField)
{
    int ilRc = RC_SUCCESS;
	
    memset(pcpFieldsBuf,' ',rgFieldsArray.lrArrayRowLen);

    strcpy(FIELDSFIELD(pcpFieldsBuf,igFieldsFnam),pcpField);
	
    return ilRc;
}

static int InitializeUrnoUrnoRow(char *pcpUrnoUrnoBuf)
{
    int ilRc = RC_SUCCESS;
	
    memset(pcpUrnoUrnoBuf,0,rgUrnoUrnoArray.lrArrayRowLen);

    strcpy(URNOURNOFIELD(pcpUrnoUrnoBuf,igUrnoUrnoStat),"x");
    strcpy(URNOURNOFIELD(pcpUrnoUrnoBuf,igUrnoUrnoUrn1)," ");
    strcpy(URNOURNOFIELD(pcpUrnoUrnoBuf,igUrnoUrnoUrn2)," ");

    return ilRc;
}


static int InitializeNewJobRow(char *pcpNewJobBuf)
{
    int ilRc = RC_SUCCESS;
    char pclUrno[32];
    char pclNow[3200];

    /* GetNextValues(pclUrno,1); */
	GetNextUrno(pclUrno);
    TimeToStr(pclNow,time(NULL));

    memset(pcpNewJobBuf,' ',rgJobArray.lrArrayRowLen);

    strcpy(JOBFIELD(pcpNewJobBuf,igJobAcfr)," ");
    strcpy(JOBFIELD(pcpNewJobBuf,igJobAct3)," ");
    strcpy(JOBFIELD(pcpNewJobBuf,igJobActo)," ");
    strcpy(JOBFIELD(pcpNewJobBuf,igJobCdat),pclNow);
    strcpy(JOBFIELD(pcpNewJobBuf,igJobDety)," ");
    strcpy(JOBFIELD(pcpNewJobBuf,igJobGate)," ");
    strcpy(JOBFIELD(pcpNewJobBuf,igJobHopo),cgHopo);
    strcpy(JOBFIELD(pcpNewJobBuf,igJobJour)," ");
    strcpy(JOBFIELD(pcpNewJobBuf,igJobLstu),pclNow);
    strcpy(JOBFIELD(pcpNewJobBuf,igJobPlfr)," ");
    strcpy(JOBFIELD(pcpNewJobBuf,igJobPlto)," ");
    strcpy(JOBFIELD(pcpNewJobBuf,igJobPosi)," ");
    strcpy(JOBFIELD(pcpNewJobBuf,igJobRegn)," ");
    strcpy(JOBFIELD(pcpNewJobBuf,igJobStat),"P");
    strcpy(JOBFIELD(pcpNewJobBuf,igJobText)," ");
    strcpy(JOBFIELD(pcpNewJobBuf,igJobUaft),"0");
    strcpy(JOBFIELD(pcpNewJobBuf,igJobUaid)," ");
    strcpy(JOBFIELD(pcpNewJobBuf,igJobUalo)," ");
    strcpy(JOBFIELD(pcpNewJobBuf,igJobUdel)," ");
    strcpy(JOBFIELD(pcpNewJobBuf,igJobUdrd)," ");
    strcpy(JOBFIELD(pcpNewJobBuf,igJobUdsr)," ");
    strcpy(JOBFIELD(pcpNewJobBuf,igJobUjty)," ");
    strcpy(JOBFIELD(pcpNewJobBuf,igJobUrno),pclUrno);
    strcpy(JOBFIELD(pcpNewJobBuf,igJobUsec),"JOBHDL");
    strcpy(JOBFIELD(pcpNewJobBuf,igJobUseu)," ");
    strcpy(JOBFIELD(pcpNewJobBuf,igJobUstf)," ");
    strcpy(JOBFIELD(pcpNewJobBuf,igJobTtgt)," ");
    strcpy(JOBFIELD(pcpNewJobBuf,igJobTtgf)," ");

    dbg(TRACE,"Urno: <%s>",pclUrno);
    return ilRc;
}

static int InitializeNewJodRow(char *pcpNewJodBuf)
{
    int ilRc = RC_SUCCESS;
    char pclUrno[32];
    char pclNow[3200];

    /* GetNextValues(pclUrno,1); */
	GetNextUrno(pclUrno);
    TimeToStr(pclNow,time(NULL));

    memset(pcpNewJodBuf,0,rgJodArray.lrArrayRowLen);

    strcpy(JODFIELD(pcpNewJodBuf,igJodCdat),pclNow);
    strcpy(JODFIELD(pcpNewJodBuf,igJodHopo),cgHopo);
    strcpy(JODFIELD(pcpNewJodBuf,igJodLstu),pclNow);
    strcpy(JODFIELD(pcpNewJodBuf,igJodUrno),pclUrno);
    strcpy(JODFIELD(pcpNewJodBuf,igJodUsec),"JOBHDL");
    strcpy(JODFIELD(pcpNewJodBuf,igJodUseu)," ");
    strcpy(JODFIELD(pcpNewJodBuf,igJodUdem)," ");
    strcpy(JODFIELD(pcpNewJodBuf,igJodUjob)," ");

    dbg(TRACE,"Urno: <%s>",pclUrno);
    return ilRc;
}

static int PrepareDemPfcArray(char *pcpDemUrno,char *pcpRudUrno)
{
    int	ilRc = RC_SUCCESS;             /* Return code */
 
    long llRowNum = ARR_FIRST;
    long llRowNum2 = ARR_FIRST;
    long llRowNum3 = ARR_FIRST;
    long llNext = ARR_NEXT;
    char *pclDemRpfRow = NULL;
    char *pclRpfRow = NULL;
    char *pclSgmRow = NULL;
    char *pclPfcRow = NULL;
    char clKey[20];
 

    dbg ( DEBUG, "PrepareDemPfcArray: UDEM <%s> URUD <%s>", pcpDemUrno, pcpRudUrno );
    if(AATArrayFindRowPointer(&(rgDemPfcArray.rrArrayHandle),
			      &(rgDemPfcArray.crArrayName[0]),
			      &(rgDemPfcArray.rrIdx01Handle),
			      &(rgDemPfcArray.crIdx01Name[0]),
			      pcpRudUrno,&llRowNum,
			      (void *) &pclDemRpfRow ) == RC_NOTFOUND)
    {
		llRowNum = ARR_FIRST;
		while(CEDAArrayFindRowPointer(&(rgRpfArray.rrArrayHandle),
						  &(rgRpfArray.crArrayName[0]),
						  &(rgRpfArray.rrIdx01Handle),
						  &(rgRpfArray.crIdx01Name[0]),
						  pcpRudUrno,&llRowNum,
						  (void *) &pclRpfRow ) == RC_SUCCESS)
		{
			sprintf(clKey,RPFFIELD(pclRpfRow,igRpfFcco));

			TrimRight ( clKey );
			llRowNum = ARR_NEXT;
			if(strlen(clKey) > 1)
			{
				/*  dbg ( DEBUG, "PrepareDemPfcArray: Going to add row to rgDemPfcArray FCTC <%s> STAT <1>", 
					  RPFFIELD(pclRpfRow,igRpfFcco) ); */
				InitializeDemPfcRow(cgTmpPfcRow);
				strcpy(DEMPFCFIELD(cgTmpPfcRow,igDemPfcFcod),RPFFIELD(pclRpfRow,igRpfFcco));
				strcpy(DEMPFCFIELD(cgTmpPfcRow,igDemPfcUdem),pcpRudUrno);
				strcpy(DEMPFCFIELD(cgTmpPfcRow,igDemPfcStat),"1");
				dbg ( DEBUG, "PrepareDemPfcArray: New row for rgDemPfcArray: FCTC <%s> UDEM <%s> STAT <%s>", 
					  DEMPFCFIELD(cgTmpPfcRow,igDemPfcFcod), DEMPFCFIELD(cgTmpPfcRow,igDemPfcUdem), 
					  DEMPFCFIELD(cgTmpPfcRow,igDemPfcStat) );
				AATArrayAddRow(&(rgDemPfcArray.rrArrayHandle),&(rgDemPfcArray.crArrayName[0]),&llNext,(void *)cgTmpPfcRow);
			}
			else
			{
   				sprintf(clKey,RPFFIELD(pclRpfRow,igRpfUpfc));
				dbg ( DEBUG, "PrepareDemPfcArray: Group of functions <%s>", clKey );
				while(CEDAArrayFindRowPointer(&(rgSgmArray.rrArrayHandle),
								  &(rgSgmArray.crArrayName[0]),
								  &(rgSgmArray.rrIdx02Handle),
								  &(rgSgmArray.crIdx02Name[0]),
								  clKey,&llRowNum2,
								  (void *) &pclSgmRow ) == RC_SUCCESS)
				{
					llRowNum2 = ARR_NEXT;
					llRowNum3 = ARR_FIRST;
					dbg ( DEBUG, "PrepareDemPfcArray: Function <%s> is group member", 
						  SGMFIELD(pclSgmRow,igSgmUval) );
					while(CEDAArrayFindRowPointer(&(rgPfcArray.rrArrayHandle),
								  &(rgPfcArray.crArrayName[0]),
								  &(rgPfcArray.rrIdx02Handle),
								  &(rgPfcArray.crIdx02Name[0]),
								  SGMFIELD(pclSgmRow,igSgmUval),&llRowNum3,
								  (void *) &pclPfcRow ) == RC_SUCCESS)
					{
						llRowNum3 = ARR_NEXT;
						InitializeDemPfcRow(cgTmpPfcRow);
						sprintf ( DEMPFCFIELD(cgTmpPfcRow,igDemPfcFcod), "%-10s", PFCFIELD(pclPfcRow,igPfcFctc) ); 
						strcpy(DEMPFCFIELD(cgTmpPfcRow,igDemPfcUdem),pcpRudUrno);
						strcpy(DEMPFCFIELD(cgTmpPfcRow,igDemPfcStat),"2");
						dbg ( DEBUG, "PrepareDemPfcArray: New row for rgDemPfcArray: FCTC <%s> UDEM <%s> STAT <%s>", 
							  DEMPFCFIELD(cgTmpPfcRow,igDemPfcFcod), DEMPFCFIELD(cgTmpPfcRow,igDemPfcUdem), 
							  DEMPFCFIELD(cgTmpPfcRow,igDemPfcStat) );
						if ( AATArrayAddRow( &(rgDemPfcArray.rrArrayHandle),&(rgDemPfcArray.crArrayName[0]),
											 &llNext,(void *)cgTmpPfcRow) == RC_SUCCESS )
						{
							/*AATArrayPutField( &(rgDemPfcArray.rrArrayHandle),&(rgDemPfcArray.crArrayName[0]),
											  0, "FCOD", llNext, TRUE, PFCFIELD(pclPfcRow,igPfcFctc) );*/
						}
					}
				}
			}
		}		
    }
    return ilRc;
}


static int PrepareDemPrqArray(char *pcpDemUrno,char *pcpRudUrno)
{
    int	ilRc = RC_SUCCESS;             /* Return code */
 
    long llRowNum = ARR_FIRST;
    long llRowNum2 = ARR_FIRST;
    long llRowNum3 = ARR_FIRST;
    long llNext = ARR_NEXT;
    char *pclDemPrqRow = NULL;
    char *pclRpqRow = NULL;
    char *pclSgmRow = NULL;
    char *pclRudRow = NULL;
    char *pclPrqRow = NULL;
  
    char clKey[24];
 
	dbg ( DEBUG, "PrepareDemPrqArray: UDEM <%s> URUD <%s>", pcpDemUrno, pcpRudUrno );
    if(AATArrayFindRowPointer(&(rgDemPrqArray.rrArrayHandle),
			      &(rgDemPrqArray.crArrayName[0]),
			      &(rgDemPrqArray.rrIdx01Handle),
			      &(rgDemPrqArray.crIdx01Name[0]),
			      pcpRudUrno,&llRowNum,
			      (void *) &pclDemPrqRow ) != RC_SUCCESS)
    {
		llRowNum = ARR_FIRST;
		while(CEDAArrayFindRowPointer(&(rgRpqArray.rrArrayHandle),
						  &(rgRpqArray.crArrayName[0]),
						  &(rgRpqArray.rrIdx01Handle),
						  &(rgRpqArray.crIdx01Name[0]),
						  pcpRudUrno,&llRowNum,
						  (void *) &pclRpqRow ) == RC_SUCCESS)
		{
			sprintf(clKey,RPQFIELD(pclRpqRow,igRpqQuco));

			dbg ( DEBUG, "PrepareDemPrqArray: Going to add row to rgDemPrqArray clKey <%s>", clKey );
    		
			llRowNum = ARR_NEXT;
			if(strlen(clKey) > 1)
			{
				InitializeDemPrqRow(cgTmpPfcRow);
				strcpy(DEMPRQFIELD(cgTmpPfcRow,igDemPrqQcod),clKey);
				strcpy(DEMPRQFIELD(cgTmpPfcRow,igDemPrqUdem),pcpRudUrno);
				strcpy(DEMPRQFIELD(cgTmpPfcRow,igDemPrqStat),"1");
				AATArrayAddRow(&(rgDemPrqArray.rrArrayHandle),&(rgDemPrqArray.crArrayName[0]),&llNext,(void *)cgTmpPfcRow);
			}
			else
			{
				sprintf(clKey,RPQFIELD(pclRpqRow,igRpqUper));
				while(CEDAArrayFindRowPointer(&(rgSgmArray.rrArrayHandle),
								  &(rgSgmArray.crArrayName[0]),
								  &(rgSgmArray.rrIdx02Handle),
								  &(rgSgmArray.crIdx02Name[0]),
								  clKey,&llRowNum2,
								  (void *) &pclSgmRow ) == RC_SUCCESS)
				{
					llRowNum2 = ARR_NEXT;
					while(CEDAArrayFindRowPointer(&(rgPerArray.rrArrayHandle),
								  &(rgPerArray.crArrayName[0]),
								  &(rgPerArray.rrIdx01Handle),
								  &(rgPerArray.crIdx01Name[0]),
								  SGMFIELD(pclSgmRow,igSgmUval),&llRowNum3,
								  (void *) &pclPrqRow ) == RC_SUCCESS)
					{
						llRowNum3 = ARR_NEXT;
						InitializeDemPrqRow(cgTmpPfcRow);
						strcpy(DEMPRQFIELD(cgTmpPfcRow,igDemPrqQcod),PERFIELD(pclPrqRow,igPerPrmc));
						strcpy(DEMPRQFIELD(cgTmpPfcRow,igDemPrqUdem),pcpRudUrno);
						strcpy(DEMPRQFIELD(cgTmpPfcRow,igDemPrqStat),"2");
						AATArrayAddRow(&(rgDemPrqArray.rrArrayHandle),&(rgDemPrqArray.crArrayName[0]),&llNext,(void *)cgTmpPfcRow);
					}
				}
			}
		}	
		llRowNum = ARR_FIRST;
		if(CEDAArrayFindRowPointer(&(rgRudArray.rrArrayHandle),
					   &(rgRudArray.crArrayName[0]),
					   &(rgRudArray.rrIdx01Handle),
					   &(rgRudArray.crIdx01Name[0]),
					   pcpRudUrno,&llRowNum,
					   (void *) &pclRudRow ) == RC_SUCCESS)
		{

			llRowNum = ARR_FIRST;
			while(CEDAArrayFindRowPointer(&(rgRpqArray.rrArrayHandle),
						  &(rgRpqArray.crArrayName[0]),
						  &(rgRpqArray.rrIdx01Handle),
						  &(rgRpqArray.crIdx01Name[0]),
						  RUDFIELD(pclRudRow,igRudUdgr),&llRowNum,
						  (void *) &pclRpqRow ) == RC_SUCCESS)
			{
				llRowNum = ARR_NEXT;

						
				sprintf(clKey,RPQFIELD(pclRpqRow,igRpqQuco));
				dbg ( DEBUG, "PrepareDemPrqArray: Found group qualification <%s>", RPQFIELD(pclRpqRow,igRpqUper) );

				llRowNum = ARR_NEXT;
				if(strlen(clKey) > 1)
				{
					InitializeDemPrqRow(cgTmpPfcRow);
					strcpy(DEMPRQFIELD(cgTmpPfcRow,igDemPrqQcod),clKey);
					strcpy(DEMPRQFIELD(cgTmpPfcRow,igDemPrqUdem),pcpRudUrno);
					strcpy(DEMPRQFIELD(cgTmpPfcRow,igDemPrqStat),"3");
					AATArrayAddRow(&(rgDemPrqArray.rrArrayHandle),&(rgDemPrqArray.crArrayName[0]),&llNext,(void *)cgTmpPfcRow);
				}
				else
				{
					sprintf(clKey,RPQFIELD(pclRpqRow,igRpqUper));
					while(CEDAArrayFindRowPointer(&(rgSgmArray.rrArrayHandle),
								  &(rgSgmArray.crArrayName[0]),
								  &(rgSgmArray.rrIdx02Handle),
								  &(rgSgmArray.crIdx02Name[0]),
								  clKey,&llRowNum2,
								  (void *) &pclSgmRow ) == RC_SUCCESS)
					{
						llRowNum2 = ARR_NEXT;
						while(CEDAArrayFindRowPointer(&(rgPerArray.rrArrayHandle),
										  &(rgPerArray.crArrayName[0]),
										  &(rgPerArray.rrIdx01Handle),
										  &(rgPerArray.crIdx01Name[0]),
										  SGMFIELD(pclSgmRow,igSgmUval),&llRowNum3,
										  (void *) &pclPrqRow ) == RC_SUCCESS)
						{
							llRowNum3 = ARR_NEXT;
							InitializeDemPrqRow(cgTmpPfcRow);
							strcpy(DEMPRQFIELD(cgTmpPfcRow,igDemPrqQcod),PERFIELD(pclPrqRow,igPerPrmc));
							strcpy(DEMPRQFIELD(cgTmpPfcRow,igDemPrqUdem),pcpRudUrno);
							strcpy(DEMPRQFIELD(cgTmpPfcRow,igDemPrqStat),"4");
							AATArrayAddRow(&(rgDemPrqArray.rrArrayHandle),&(rgDemPrqArray.crArrayName[0]),&llNext,(void *)cgTmpPfcRow);
						}
					}
				}
			}
		}
    }
    return ilRc;
}

static int PrepareDemPoolArray(char *pcpDemAlid,char *pcpDemAloc)
{
    int	ilRc = RC_SUCCESS;             
 
    long llRowNum = ARR_FIRST;
    long llRowNum2 = ARR_FIRST;
    long llRowNum3 = ARR_FIRST;
    long llNext = ARR_NEXT;
    long llFirst = ARR_FIRST;

    char *pclTmpUrnoRow = NULL;
    char *pclDemPoolRow = NULL;
    char *pclAlidListRow = NULL;
    char *pclAloRow = NULL;
    char *pclPolRow = NULL;
    char *pclSgmRow = NULL;
    char *pclSgrRow = NULL;

  
    char *pclDot;
    char clSqlBuf[124];
    short slCursor;
    short slSqlFunc;

    char clAlid[20] = "";
    char clXYTab[12];
    char clXYName[12];
    static char clXYUrno[20];
    char clKey[20];
    char clAlidKey[40];

    int ilIndex = 0;
    long llUgty = 0;

    BOOL blDemPoolFound = FALSE;
    BOOL blUaloFound = FALSE;

    dbg(TRACE,"PrepareDemPoolArray  pcpDemAlid <%s>  pcpDemAloc <%s>",pcpDemAlid,pcpDemAloc);
  
    strcpy(clAlid,pcpDemAlid);
    TrimRight(clAlid);
    dbg(TRACE,"clAlid <%s> ",clAlid);
  
 
    if(strlen(clAlid) > 1)
    {

	CEDAArrayDelete(&(rgTmpUrnoArray.rrArrayHandle),&(rgTmpUrnoArray.crArrayName[0]));

	sprintf(clAlidKey,"%s,%s",pcpDemAlid,pcpDemAloc);
	if(AATArrayFindRowPointer(&(rgDemPoolArray.rrArrayHandle),
				  &(rgDemPoolArray.crArrayName[0]),
				  &(rgDemPoolArray.rrIdx01Handle),
				  &(rgDemPoolArray.crIdx01Name[0]),
				  clAlidKey,&llFirst,
				  (void *) &pclDemPoolRow ) == RC_NOTFOUND)
	{
	    llRowNum = ARR_FIRST;

	    sprintf(clAlidKey,"%s,%s",pcpDemAloc,pcpDemAlid);
	    if( AATArrayFindRowPointer(&(rgAlidListArray.rrArrayHandle),&(rgAlidListArray.crArrayName[0]),
				       &(rgAlidListArray.rrIdx01Handle),
				       &(rgAlidListArray.crIdx01Name[0]),
				       clAlidKey,&llRowNum,
				       (void *) &pclAlidListRow ) == RC_SUCCESS)
	    {
		sprintf(clXYUrno,ALIDLISTFIELD(pclAlidListRow,igAlidListUaid));
		blUaloFound = TRUE;
	    }
	    else
	    {
		llRowNum = ARR_FIRST;
		
		if(CEDAArrayFindRowPointer(&(rgAloArray.rrArrayHandle),
					   &(rgAloArray.crArrayName[0]),
					   &(rgAloArray.rrIdx02Handle),
					   &(rgAloArray.crIdx02Name[0]),
					   pcpDemAloc,&llRowNum,
					   (void *) &pclAloRow ) == RC_SUCCESS)
		{
		    sprintf(clKey,ALOFIELD(pclAloRow,igAloReft));


		    if ((pclDot = strchr(clKey,'.')) != NULL)
		    {
			pclDot++;
			strcpy(clXYName,pclDot);
			pclDot--;
			*pclDot = '\0';
			strcpy(clXYTab,clKey);
			sprintf(clSqlBuf,
				"SELECT URNO FROM %sTAB WHERE %s = '%s' AND HOPO = '%s'",clXYTab,clXYName,clAlid,cgHopo);
			slSqlFunc = START;
			slCursor = 0;
			ilRc = sql_if(slSqlFunc,&slCursor,clSqlBuf,clXYUrno);
			if(ilRc == RC_SUCCESS)
			{
			    llNext = ARR_NEXT;
			    blUaloFound = TRUE;
			    InitializeAlidListRow(cgAlidListBuf);
						
			    ilRc = AATArrayAddRow(&(rgAlidListArray.rrArrayHandle),&(rgAlidListArray.crArrayName[0]),&llNext,(void *)cgAlidListBuf);

			    AATArrayPutField(&(rgAlidListArray.rrArrayHandle),
					     &(rgAlidListArray.crArrayName[0]),NULL,"ALID",llNext,TRUE,pcpDemAlid);
			    AATArrayPutField(&(rgAlidListArray.rrArrayHandle),
					     &(rgAlidListArray.crArrayName[0]),NULL,"ALOC",llNext,TRUE,pcpDemAloc);
			    AATArrayPutField(&(rgAlidListArray.rrArrayHandle),
					     &(rgAlidListArray.crArrayName[0]),NULL,"UAID",llNext,TRUE,clXYUrno);
			    AATArrayPutField(&(rgAlidListArray.rrArrayHandle),
					     &(rgAlidListArray.crArrayName[0]),NULL,"UALO",llNext,TRUE,ALOFIELD(pclAloRow,igAloUrno));

			}
			close_my_cursor(&slCursor);
			commit_work();
			slCursor = 0;
		    }
		}
	    }
	    if(blUaloFound)
	    {

		InitializeTmpUrnoRow(cgTmpUrnoBuf,1);
		strcpy(TMPURNOFIELD(cgTmpUrnoBuf,igTmpUrnoUrno),clXYUrno);
		dbg(TRACE,"PrepareDemPoolArray Init TmpUrno  <%s>",TMPURNOFIELD(cgTmpUrnoBuf,igTmpUrnoUrno));

		ilRc = AATArrayAddRow(&(rgTmpUrnoArray.rrArrayHandle),&(rgTmpUrnoArray.crArrayName[0]),&llNext,(void *)cgTmpUrnoBuf);


		for(ilIndex = 1; ilIndex <= 10; ilIndex++)
		{

		    sprintf(clKey,"x,%d",ilIndex);

		    llFirst = ARR_FIRST;
		    while( AATArrayFindRowPointer(&(rgTmpUrnoArray.rrArrayHandle),&(rgTmpUrnoArray.crArrayName[0]),
						  &(rgTmpUrnoArray.rrIdx01Handle),
						  &(rgTmpUrnoArray.crIdx01Name[0]),
						  clKey,&llFirst,
						  (void *) &pclTmpUrnoRow ) == RC_SUCCESS)
		    {


			dbg(TRACE,"PrepareDemPoolArray  stat  <%s>",TMPURNOFIELD(pclTmpUrnoRow,igTmpUrnoStat));
			dbg(TRACE,"PrepareDemPoolArray  aidx  <%s>",TMPURNOFIELD(pclTmpUrnoRow,igTmpUrnoAidx));
			dbg(TRACE,"PrepareDemPoolArray  TmpUrno  <%s>",TMPURNOFIELD(pclTmpUrnoRow,igTmpUrnoUrno));
			if(AATArrayPutField(&(rgTmpUrnoArray.rrArrayHandle),
					    &(rgTmpUrnoArray.crArrayName[0]),NULL,"STAT",llFirst,TRUE,"y") != RC_SUCCESS)
			{
			    dbg(TRACE,"PrepareDemPool error AATArrayPutField STAT y ");
			    llFirst = ARR_NEXT;
			}
			else
			{
			    dbg(TRACE,"PrepareDemPool  AATArrayPutField STAT y ");
			    llFirst = ARR_FIRST;
			}
			llRowNum = ARR_FIRST;
			while(CEDAArrayFindRowPointer(&(rgSgmArray.rrArrayHandle),
						      &(rgSgmArray.crArrayName[0]),
						      &(rgSgmArray.rrIdx01Handle),
						      &(rgSgmArray.crIdx01Name[0]),
						      TMPURNOFIELD(pclTmpUrnoRow,igTmpUrnoUrno),&llRowNum,
						      (void *) &pclSgmRow ) == RC_SUCCESS)	
			{
			    llRowNum2 = ARR_FIRST;
			    if(CEDAArrayFindRowPointer(&(rgSgrArray.rrArrayHandle),
						       &(rgSgrArray.crArrayName[0]),
						       &(rgSgrArray.rrIdx01Handle),
						       &(rgSgrArray.crIdx01Name[0]),
						       SGMFIELD(pclSgmRow,igSgmUsgr),&llRowNum2,
						       (void *) &pclSgrRow ) == RC_SUCCESS)
			    {
				llUgty = atol(SGRFIELD(pclSgrRow,igSgrUgty));

				if(llUgty == lgUpolAlo)
				{
				    llRowNum3 = ARR_FIRST;
				    dbg(TRACE,"PrepareDemPool  pclSgrRow Urno <%s> ",SGRFIELD(pclSgrRow,igSgrGrpn));
				    if(CEDAArrayFindRowPointer(&(rgPolArray.rrArrayHandle),
							       &(rgPolArray.crArrayName[0]),
							       &(rgPolArray.rrIdx01Handle),
							       &(rgPolArray.crIdx01Name[0]),
							       SGRFIELD(pclSgrRow,igSgrGrpn),&llRowNum3,
							       (void *) &pclPolRow ) == RC_SUCCESS)
				    {

					llNext = ARR_NEXT;
					InitializeDemPoolRow(cgTmpPoolRow);
					strcpy(DEMPOOLFIELD(cgTmpPoolRow,igDemPoolAlid),pcpDemAlid);
					strcpy(DEMPOOLFIELD(cgTmpPoolRow,igDemPoolUpol),POLFIELD(pclPolRow,igPolUrno));
					strcpy(DEMPOOLFIELD(cgTmpPoolRow,igDemPoolUaid),clXYUrno);
					ilRc = AATArrayAddRow(&(rgDemPoolArray.rrArrayHandle),&(rgDemPoolArray.crArrayName[0]),&llNext,(void *)cgTmpPoolRow);
					if(ilRc == RC_SUCCESS)
					{
					    AATArrayPutField(&(rgDemPoolArray.rrArrayHandle),
							     &(rgDemPoolArray.crArrayName[0]),NULL,"ALID",llNext,TRUE,pcpDemAlid);
					    AATArrayPutField(&(rgDemPoolArray.rrArrayHandle),
							     &(rgDemPoolArray.crArrayName[0]),NULL,"ALOC",llNext,TRUE,pcpDemAloc);
					    AATArrayPutField(&(rgDemPoolArray.rrArrayHandle),
							     &(rgDemPoolArray.crArrayName[0]),NULL,"UPOL",llNext,TRUE,POLFIELD(pclPolRow,igPolUrno));
					    AATArrayPutField(&(rgDemPoolArray.rrArrayHandle),
							     &(rgDemPoolArray.crArrayName[0]),NULL,"UAID",llNext,TRUE,clXYUrno);
					    blDemPoolFound = TRUE;
					}

				    }
				}
				else
				{
				    llNext = ARR_NEXT;
				    InitializeTmpUrnoRow(cgTmpUrnoBuf,ilIndex + 1);
				    strcpy(TMPURNOFIELD(cgTmpUrnoBuf,igTmpUrnoUrno),SGRFIELD(pclSgrRow,igSgrUrno));
				    ilRc = AATArrayAddRow(&(rgTmpUrnoArray.rrArrayHandle),&(rgTmpUrnoArray.crArrayName[0]),&llNext,(void *)cgTmpUrnoBuf);

				}
			    }
			    llRowNum = ARR_NEXT;
			}
		    }
		}
		if(!blDemPoolFound)
		{
		    llNext = ARR_NEXT;
		    InitializeDemPoolRow(cgTmpPoolRow);
		    strcpy(DEMPOOLFIELD(cgTmpPoolRow,igDemPoolAlid),pcpDemAlid);
		    strcpy(DEMPOOLFIELD(cgTmpPoolRow,igDemPoolUpol),"");
		    strcpy(DEMPOOLFIELD(cgTmpPoolRow,igDemPoolUaid),clXYUrno);
		    ilRc = AATArrayAddRow(&(rgDemPoolArray.rrArrayHandle),&(rgDemPoolArray.crArrayName[0]),&llNext,(void *)cgTmpPoolRow);
		    if(ilRc == RC_SUCCESS)
		    {
			AATArrayPutField(&(rgDemPoolArray.rrArrayHandle),
					 &(rgDemPoolArray.crArrayName[0]),NULL,"ALID",llNext,TRUE,pcpDemAlid);
			AATArrayPutField(&(rgDemPoolArray.rrArrayHandle),
					 &(rgDemPoolArray.crArrayName[0]),NULL,"ALOC",llNext,TRUE,pcpDemAloc);
			AATArrayPutField(&(rgDemPoolArray.rrArrayHandle),
					 &(rgDemPoolArray.crArrayName[0]),NULL,"UPOL",llNext,TRUE,"");
			AATArrayPutField(&(rgDemPoolArray.rrArrayHandle),
					 &(rgDemPoolArray.crArrayName[0]),NULL,"UAID",llNext,TRUE,clXYUrno);
			blDemPoolFound = TRUE;
		    }
		}
	    }
	}
    }
    return ilRc;
}






/******************************************************************************/
/******************************************************************************/

static int ClusterDemArray(ARRAYINFO *prpOpenDemArray)
{
    int	ilRc = RC_SUCCESS;             /* Return code */
    long llAction = ARR_FIRST;
    long llRowNum = ARR_FIRST;
    long llFirst = ARR_FIRST;
    long llNext = ARR_NEXT;
    long llRowCount = 0;
    char clKey[1000];
    char clKey1[20];
  

    char *pclDemRow = NULL;
    char *pclIdxRow = NULL;
    long llCount = 0;
    char clCount[10];
    double ldSix1 = 0;
    double ldSix2 = 0;
    char clSix1[10];
    char clSix2[10];
    long llSort;


    dbg(TRACE,"ClusterDemArray Start  ");

    ilRc = CEDAArrayGetRowCount(&(prpOpenDemArray->rrArrayHandle),&(prpOpenDemArray->crArrayName[0]),&llRowCount);

    dbg(TRACE,"ClusterDemArray OpenDem RowCount <%ld> ilRc <%ld>  ",llRowCount,ilRc);
    llRowNum = ARR_FIRST;
    sprintf(clKey1,"");
    while((ilRc = CEDAArrayFindRowPointer(&(prpOpenDemArray->rrArrayHandle),
					  &(prpOpenDemArray->crArrayName[0]),
					  &(prpOpenDemArray->rrIdx01Handle),
					  &(prpOpenDemArray->crIdx01Name[0]),
					  clKey1,&llRowNum,
					  (void *) &pclDemRow )) == RC_SUCCESS)										
    {
	  
	dbg(DEBUG,"ClusterDemArray pclDemRowUrno <%s> ilRc<%d> ",OPENDEMFIELD(pclDemRow,igDemUrno),ilRc);

	sprintf(clKey,"%s,%s,%s,%s",OPENDEMFIELD(pclDemRow,igDemOuri),
		OPENDEMFIELD(pclDemRow,igDemOuro),
		OPENDEMFIELD(pclDemRow,igDemTpln),
		OPENDEMFIELD(pclDemRow,igDemSeco));
		
	dbg(DEBUG,"ClusterDemArray clKey <%s> ",clKey);
		
	llFirst = ARR_FIRST;
	if(AATArrayFindRowPointer(&(rgDemIdxArray.rrArrayHandle),&(rgDemIdxArray.crArrayName[0]),
				  &(rgDemIdxArray.rrIdx01Handle),
				  &(rgDemIdxArray.crIdx01Name[0]),
				  clKey,&llFirst,
				  (void *) &pclIdxRow ) != RC_SUCCESS)
	{
	    dbg(DEBUG,"ClusterDemArray NewIdxRow  ");

	    llNext = ARR_NEXT;
	    InitializeDemIdxRow(cgIdxBuff);
	    strcpy(DEMIDXFIELD(cgIdxBuff,igDemIdxOuri),OPENDEMFIELD(pclDemRow,igDemOuri));
	    strcpy(DEMIDXFIELD(cgIdxBuff,igDemIdxOuro),OPENDEMFIELD(pclDemRow,igDemOuro));
	    strcpy(DEMIDXFIELD(cgIdxBuff,igDemIdxTpln),OPENDEMFIELD(pclDemRow,igDemTpln));
	    strcpy(DEMIDXFIELD(cgIdxBuff,igDemIdxSern),OPENDEMFIELD(pclDemRow,igDemSeco));

	    if(AATArrayAddRow(&(rgDemIdxArray.rrArrayHandle),&(rgDemIdxArray.crArrayName[0]),&llNext,(void *)cgIdxBuff) != RC_SUCCESS)
	    {
		dbg(TRACE,"ClusterDemArray error AATArrayAddRow  ");
	    }
	    if(ilRc = CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),
					&(prpOpenDemArray->crArrayName[0]),NULL,"SIX2",llRowNum,"1") != RC_SUCCESS)
	    {
		dbg(TRACE,"ClusterDemArray error CEDAArrayPutField SIX2 Wert 1 ilRc <%d> llRowNum <ld>",ilRc,llRowNum);
	    }

	}
	else
	{
	    llCount = atol(DEMIDXFIELD(pclIdxRow,igDemIdxKcnt));
	    dbg(DEBUG,"ClusterDemArray llCount <%ld> ",llCount);
	    llCount++;

	    sprintf(clCount,"%ld",llCount);
				
	    dbg(DEBUG,"ClusterDemArray clCount <%s> ",clCount);

	    if(AATArrayPutField(&(rgDemIdxArray.rrArrayHandle),
				&(rgDemIdxArray.crArrayName[0]),NULL,"KCNT",llFirst,TRUE,clCount) != RC_SUCCESS)
	    {
		dbg(TRACE,"ClusterDemArray error AATArrayPutField KCNT <%s> ",clCount);
	    }
	    if(CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),
				 &(prpOpenDemArray->crArrayName[0]),NULL,"SIX2",llRowNum,clCount) != RC_SUCCESS)
	    {
		dbg(TRACE,"ClusterDemArray error CEDAArrayPutField SIX2 Wert <%s> ",clCount);
	    }
	    /*********/
	}
	llRowNum = ARR_NEXT;
    }

  
    sprintf(clKey1,"");
    while((ilRc = CEDAArrayFindRowPointer(&(rgDemIdxArray.rrArrayHandle),
					  &(rgDemIdxArray.crArrayName[0]),
					  &(rgDemIdxArray.rrIdx01Handle),
					  &(rgDemIdxArray.crIdx01Name[0]),
					  clKey1,&llAction,
					  (void *) &pclIdxRow )) == RC_SUCCESS)
    {

	llAction = ARR_NEXT;
	sprintf(clKey,"%s,%s,%s,%s",DEMIDXFIELD(pclIdxRow,igDemIdxOuri),
		DEMIDXFIELD(pclIdxRow,igDemIdxOuro),
		DEMIDXFIELD(pclIdxRow,igDemIdxTpln),
		DEMIDXFIELD(pclIdxRow,igDemIdxSern));

	llCount = atol(DEMIDXFIELD(pclIdxRow,igDemIdxKcnt));

	dbg(TRACE,"ClusterDemArray clKey <%s> llCount2 <%ld> ",clKey,llCount);



	llRowNum = ARR_FIRST;

	while((ilRc = CEDAArrayFindRowPointer(&(prpOpenDemArray->rrArrayHandle),
					      &(prpOpenDemArray->crArrayName[0]),
					      &(prpOpenDemArray->rrIdx02Handle),
					      &(prpOpenDemArray->crIdx02Name[0]),
					      clKey,&llRowNum,
					      (void *) &pclDemRow )) == RC_SUCCESS)
	{
			  
	    llSort = atol(OPENDEMFIELD(pclDemRow,igDemSix2));
			  
	    if(llSort < igEvenDistBound)
	    {
		ldSix1 = 1/llSort;
		ldSix2 = 0;
	    }
	    else
	    {
		if(bgUseAdaptive)
		{
		    ldSix2 = llCount/(-llSort) +1;
		}
		else
		{
		    ldSix2 = 1;
		}

		ldSix1 = 0;
	    }

	    sprintf(clSix1,"%1.2f",ldSix1);
	    sprintf(clSix2,"%1.2f",ldSix2);
	    dbg(TRACE,"ClusterDemArray clSix1 <%s> clSix2 <%s> llCount <%d>",clSix1,clSix2);
	    if(CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),
				 &(prpOpenDemArray->crArrayName[0]),NULL,"SIX1",llRowNum,clSix1) != RC_SUCCESS)
	    {		
		dbg(TRACE,"ClusterDemArray error CEDAArrayPutField clSix1 <%s> ",clSix1);
	    }
	    if(CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),
				 &(prpOpenDemArray->crArrayName[0]),NULL,"SIX2",llRowNum,clSix2) != RC_SUCCESS)
	    {		
		dbg(TRACE,"ClusterDemArray error CEDAArrayPutField clSix2 <%s> ",clSix2);
	    }
	    llRowNum = ARR_NEXT;
	}
    }
	
    dbg(TRACE,"ClusterDemArray End  ");

    return ilRc;
}

/******************************************************************************/
/******************************************************************************/

static int PrepareDemArray(ARRAYINFO *prpOpenDemArray)
{
    int	ilRc = RC_SUCCESS;             /* Return code */
  
    long llAction = ARR_FIRST;
    long llRowNum = ARR_FIRST;
    long llRowNum2 = ARR_FIRST;
    char clSeco[20];
    char clRtwf[20];
    char clRtwt[20];
    char clTpln[140];
    char clUlnk[20];
    char clUrue[20];
    char clUpde[20];
    char clUnde[20];
    char clMaxd[20];
    char clMind[20];
    char clDide[20];
    char clUtpl[20];
    char clRede[33];
 
    char *pclDemRow = NULL;
    char *pclRudRow = NULL;
    char *pclRueRow = NULL;
    char *pclTplRow = NULL;
    char *pclSerRow = NULL;
    char *pclAloRow = NULL;

    dbg(TRACE,"PrepareDemArray Start");
    while(ilRc = CEDAArrayFindRowPointer(&(prpOpenDemArray->rrArrayHandle),
					 &(prpOpenDemArray->crArrayName[0]),
					 &(prpOpenDemArray->rrIdx03Handle),
					 &(prpOpenDemArray->crIdx03Name[0]),"1",
					 &llAction,(void *)&pclDemRow) == RC_SUCCESS)
    {
	llRowNum = ARR_FIRST;
	if(CEDAArrayFindRowPointer(&(rgRudArray.rrArrayHandle),
				   &(rgRudArray.crArrayName[0]),
				   &(rgRudArray.rrIdx01Handle),
				   &(rgRudArray.crIdx01Name[0]),
				   OPENDEMFIELD(pclDemRow,igDemUrud),&llRowNum,
				   (void *) &pclRudRow ) == RC_SUCCESS)
	{
	    sprintf(clUlnk,"%s",RUDFIELD(pclRudRow,igRudUlnk));
	    sprintf(clUrue,"%s",RUDFIELD(pclRudRow,igRudUrue));
	    sprintf(clUpde,"%s",RUDFIELD(pclRudRow,igRudUpde));
	    sprintf(clUnde,"%s",RUDFIELD(pclRudRow,igRudUnde));
	    sprintf(clMaxd,"%s",RUDFIELD(pclRudRow,igRudMaxd));
	    sprintf(clMind,"%s",RUDFIELD(pclRudRow,igRudMind));
	    sprintf(clDide,"%s",RUDFIELD(pclRudRow,igRudDide));
	    sprintf(clRede,"%s",RUDFIELD(pclRudRow,igRudRede));


	    dbg(TRACE,"PrepareDemArray clUlnk <%s> clUrue <%s> clUpde <%s> clUnde <%s> ",clUlnk,clUrue,clUpde,clUnde);

	    llRowNum = ARR_FIRST;
	    if(CEDAArrayFindRowPointer(&(rgSerArray.rrArrayHandle),
				       &(rgSerArray.crArrayName[0]),
				       &(rgSerArray.rrIdx01Handle),
				       &(rgSerArray.crIdx01Name[0]),
				       RUDFIELD(pclRudRow,igRudUghs),&llRowNum,
				       (void *) &pclSerRow ) == RC_SUCCESS)
	    {

		sprintf(clSeco,"%s",SERFIELD(pclSerRow,igSerSeco));
		sprintf(clRtwf,"%s",SERFIELD(pclSerRow,igSerRtwf));
		sprintf(clRtwt,"%s",SERFIELD(pclSerRow,igSerRtwt));
	    }
	    else
	    {
		dbg(TRACE,"PrepareDemData Service not found UGHS: <%s>",RUDFIELD(pclRudRow,igRudUghs));
	    }


	    llRowNum = ARR_FIRST;
	    if(CEDAArrayFindRowPointer(&(rgRueArray.rrArrayHandle),
				       &(rgRueArray.crArrayName[0]),
				       &(rgRueArray.rrIdx01Handle),
				       &(rgRueArray.crIdx01Name[0]),
				       RUDFIELD(pclRudRow,igRudUrue),&llRowNum,
				       (void *) &pclRueRow ) == RC_SUCCESS)
	    {
			strcpy ( clUtpl, RUEFIELD(pclRueRow,igRueUtpl) );
		llRowNum = ARR_FIRST;
		if(CEDAArrayFindRowPointer(&(rgTplArray.rrArrayHandle),
					   &(rgTplArray.crArrayName[0]),
					   &(rgTplArray.rrIdx01Handle),
					   &(rgTplArray.crIdx01Name[0]),
					   RUEFIELD(pclRueRow,igRueUtpl),&llRowNum,
					   (void *) &pclTplRow ) == RC_SUCCESS)
		    sprintf(clTpln,"%s",TPLFIELD(pclTplRow,igTplTnam));
				
	    }
	}
	else
	{
	    dbg(TRACE,"PrepareDemData Rud not found Urud: <%s>",OPENDEMFIELD(pclDemRow,igDemUrud));
	}



	
		
	if(strncmp(OPENDEMFIELD(pclDemRow,igDemRety),"100",3) == 0)
	{
	    PrepareDemPfcArray(OPENDEMFIELD(pclDemRow,igDemUrno),OPENDEMFIELD(pclDemRow,igDemUrud));
	    PrepareDemPrqArray(OPENDEMFIELD(pclDemRow,igDemUrno),OPENDEMFIELD(pclDemRow,igDemUrud));
	    PrepareDemPoolArray(OPENDEMFIELD(pclDemRow,igDemAlid),OPENDEMFIELD(pclDemRow,igDemAloc));
	}
		
	

	llRowNum2 = ARR_FIRST;
	if(CEDAArrayFindRowPointer(&(rgAloArray.rrArrayHandle),
				   &(rgAloArray.crArrayName[0]),
				   &(rgAloArray.rrIdx02Handle),
				   &(rgAloArray.crIdx02Name[0]),
				   OPENDEMFIELD(pclDemRow,igDemAloc),&llRowNum2,
				   (void *) &pclAloRow ) == RC_SUCCESS)
	{
	    dbg(TRACE,"PrepareDemData Line %05d:",__LINE__);
	    dbg(TRACE,"PrepareDemData AloUrno <%s>",ALOFIELD(pclAloRow,igAloUrno));
	    if(CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),&(prpOpenDemArray->crArrayName[0]),NULL,"UALO",llAction,ALOFIELD(pclAloRow,igAloUrno)) != RC_SUCCESS)
	    {
		dbg(TRACE,"PrepareDemData Put Field UALO failed: %s",ALOFIELD(pclAloRow,igAloUrno));
	    }
	}
	if(CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),&(prpOpenDemArray->crArrayName[0]),NULL,"SECO",llAction,clSeco) != RC_SUCCESS)
	{
	    dbg(TRACE,"PrepareDemData Put Field SECO failed: %s",clSeco);
	}

	if(CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),&(prpOpenDemArray->crArrayName[0]),NULL,"RTWF",llAction,clRtwf) != RC_SUCCESS)
	{
	    dbg(TRACE,"PrepareDemData Put Field RTWF failed: %s",clRtwf);
	}

	if(CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),&(prpOpenDemArray->crArrayName[0]),NULL,"RTWT",llAction,clRtwt) != RC_SUCCESS)
	{
	    dbg(TRACE,"PrepareDemData Put Field RTWT failed: %s",clRtwt);
	}

	if(CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),&(prpOpenDemArray->crArrayName[0]),NULL,"URUE",llAction,clUrue) != RC_SUCCESS)
	{
	    dbg(TRACE,"PrepareDemData Put Field Urue failed: %s",clUrue);
	}

	if(CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),&(prpOpenDemArray->crArrayName[0]),NULL,"ULNK",llAction,clUlnk) != RC_SUCCESS)
	{
	    dbg(TRACE,"PrepareDemData Put Field Ulnk failed: %s",clUlnk);
	}

	if(CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),&(prpOpenDemArray->crArrayName[0]),NULL,"UPDE",llAction,"0") != RC_SUCCESS)
	{
	    dbg(TRACE,"PrepareDemData Put Field Upde failed: %s",clUpde);
	}

	if(CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),&(prpOpenDemArray->crArrayName[0]),NULL,"UNDE",llAction,"0") != RC_SUCCESS)
	{
	    dbg(TRACE,"PrepareDemData Put Field Unde failed: %s",clUnde);
	}

	if(CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),
			     &(prpOpenDemArray->crArrayName[0]),NULL,"MAXD",llAction,clMaxd) != RC_SUCCESS)
	{
	    dbg(TRACE,"PrepareDemData Put Field Maxd failed: %s",clMaxd);
	}
	if(CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),
			     &(prpOpenDemArray->crArrayName[0]),NULL,"MIND",llAction,clMind) != RC_SUCCESS)
	{
	    dbg(TRACE,"PrepareDemData Put Field Mind failed: %s",clMind);
	}
	if(CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),
			     &(prpOpenDemArray->crArrayName[0]),NULL,"DIDE",llAction,clDide) != RC_SUCCESS)
	{
	    dbg(TRACE,"PrepareDemData Put Field DIDE failed: %s",clDide);
	}

	if(CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),&(prpOpenDemArray->crArrayName[0]),NULL,"TNAM",llAction,clTpln) != RC_SUCCESS)
	{
	    dbg(TRACE,"PrepareDemData Put Field Tpln failed: %s",clTpln);
	}

	if(CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),&(prpOpenDemArray->crArrayName[0]),NULL,"SIX1",llAction,"0") != RC_SUCCESS)
	{
	    dbg(TRACE,"PrepareDemData Put Field SIX1 failed");
	}

	if(CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),&(prpOpenDemArray->crArrayName[0]),NULL,"SIX2",llAction,"0") != RC_SUCCESS)
	{
	    dbg(TRACE,"PrepareDemData Put Field SIX2 failed");
	}
							
	if(CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),&(prpOpenDemArray->crArrayName[0]),NULL,"UTPL",llAction,clUtpl) != RC_SUCCESS)
	{
	    dbg(TRACE,"PrepareDemData Put Field UTPL failed");
	}

	dbg(TRACE,"PrepareDemData clRede <%s> ",clRede);

	if(atol(clUpde) > 0)
	{
	    if(clRede[0] == '1')
	    {
		CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),
				  &(prpOpenDemArray->crArrayName[0]),NULL,"UPDE",llAction,clUpde) ;
	    }
	}
	if(atol(clUnde) > 0)
	{
	    if(clRede[2] == '1')
	    {
		CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),
				  &(prpOpenDemArray->crArrayName[0]),NULL,"UNDE",llAction,clUnde) ;
	    }
	}

	llAction = ARR_NEXT;
    }
    ClusterDemArray(prpOpenDemArray);

    dbg(TRACE,"PrepareDemArray End");

    return ilRc;
}


static void SetJobTypeArray()
{
    char clTmpJobType[400];
    char clTmpJobVal[100];
    long llNext = ARR_NEXT;
    int ilCount;
    int ilNoOfJobTypes = get_no_of_items(cgJobTypes);
	
    for(ilCount = 0; ilCount < ilNoOfJobTypes; ilCount++)
    {
		
	if(GetDataItem(clTmpJobType,cgJobTypes,ilCount+1,',',""," \0") >= 0)
	{
	    char clTmp[32];
	    llNext = ARR_FIRST;
	    InitializeJobTypeRow(cgJobTypeBuf);
	    GetDataItem(clTmpJobVal,clTmpJobType,1,';',""," \0");
	    sprintf(clTmp,"%-30s",clTmpJobVal);
	    strcpy(JOBTYPEFIELD(cgJobTypeBuf,igJobTypeDety),clTmp);
	    /*******************/
	    GetDataItem(clTmpJobVal,clTmpJobType,2,';',""," \0");
	    sprintf(clTmp,"%-30s",clTmpJobVal);
	    strcpy(JOBTYPEFIELD(cgJobTypeBuf,igJobTypeAloc),clTmp);
	    GetDataItem(clTmpJobVal,clTmpJobType,3,';',""," \0");
	    sprintf(clTmp,"%-30s",clTmpJobVal);
	    strcpy(JOBTYPEFIELD(cgJobTypeBuf,igJobTypeJtyp),clTmp);

			
	    AATArrayAddRow(&(rgJobTypeArray.rrArrayHandle),&(rgJobTypeArray.crArrayName[0]),&llNext,(void *)cgJobTypeBuf);
	}
    }
}
/******************************************************************************/
/******************************************************************************/
static void SetFieldsArray()
{
    char clTmpFieldVal[20];
    long llNext = ARR_NEXT;
    int ilCount;
    int ilNoOfFields = get_no_of_items(cgNewDistFields);
    for(ilCount = 0; ilCount < ilNoOfFields; ilCount++)
    {
		
	if(GetDataItem(clTmpFieldVal,cgNewDistFields,ilCount+1,',',""," \0") >= 0)
	{
	    llNext = ARR_NEXT;
	    dbg(TRACE,"SetFieldsArray clTmpFieldVal <%s>",clTmpFieldVal);
	    InitializeFieldsRow(cgFieldsBuf,clTmpFieldVal);

	    AATArrayAddRow(&(rgFieldsArray.rrArrayHandle),&(rgFieldsArray.crArrayName[0]),&llNext,(void *)cgNewDistFields);
	    AATArrayPutField(&(rgFieldsArray.rrArrayHandle),
			     &(rgFieldsArray.crArrayName[0]),NULL,"FNAM",llNext,TRUE,
			     clTmpFieldVal) ;
			
	}
    }

}
/******************************************************************************/
/******************************************************************************/

static int PrepareJobData()
{
    int	ilRc = RC_SUCCESS;             /* Return code */
    char pclSelection[456];
    long llAction = ARR_FIRST;
    long llRowNum = ARR_FIRST;
    long llNext = ARR_NEXT;
    char *pclJobRow = NULL;
    char *pclJodRow = NULL;
    char *pclAloRow = NULL;
    char *pclAlidListRow = NULL;
	
	
    char *pclDot;
    char clSqlBuf[124];
    short slCursor;
    short slSqlFunc;

    char clXYTab[12];
    char clXYName[12];
    static char clXYAlid[20];
    char clKey[20];
    BOOL blAlidFound = FALSE;
	long llJobType;


    while(CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
				  &(rgJobArray.crArrayName[0]),
				  &(rgJobArray.rrIdx01Handle),
				  &(rgJobArray.crIdx01Name[0]),
				  "",
				  &llAction,(void *)&pclJobRow) == RC_SUCCESS)
    {

	llRowNum = ARR_FIRST;
		
	if( AATArrayFindRowPointer(&(rgAlidListArray.rrArrayHandle),&(rgAlidListArray.crArrayName[0]),
				   &(rgAlidListArray.rrIdx02Handle),
				   &(rgAlidListArray.crIdx02Name[0]),
				   JOBFIELD(pclJobRow,igJobUaid),&llRowNum,
				   (void *) &pclAlidListRow ) == RC_SUCCESS)
	{
	    sprintf(clXYAlid,ALIDLISTFIELD(pclAlidListRow,igAlidListAlid));
	    blAlidFound = TRUE;
	}
	else
	{
	    llRowNum = ARR_FIRST;
			
	    if(atol(JOBFIELD(pclJobRow,igJobUaid)) > 0) /*FSC insert for PRF 2064 */
	    {

		if(CEDAArrayFindRowPointer(&(rgAloArray.rrArrayHandle),
					   &(rgAloArray.crArrayName[0]),
					   &(rgAloArray.rrIdx01Handle),
					   &(rgAloArray.crIdx01Name[0]),
					   JOBFIELD(pclJobRow,igJobUalo),&llRowNum,
					   (void *) &pclAloRow ) == RC_SUCCESS)
		{
		    sprintf(clKey,ALOFIELD(pclAloRow,igAloReft));

					
		    if ((pclDot = strchr(clKey,'.')) != NULL)
		    {
			pclDot++;
			strcpy(clXYName,pclDot);
			pclDot--;
			*pclDot = '\0';
			strcpy(clXYTab,clKey);
			if (atol(JOBFIELD(pclJobRow,igJobUaid))  > 0 )
			{
			    sprintf(clSqlBuf,
				    "SELECT %s FROM %sTAB WHERE URNO = '%s' AND HOPO = '%s'",clXYName,clXYTab,JOBFIELD(pclJobRow,igJobUaid),cgHopo);
			    slSqlFunc = START;
			    slCursor = 0;
			    ilRc = sql_if(slSqlFunc,&slCursor,clSqlBuf,clXYAlid);
			    dbg(DEBUG,"sql_if Selection <%s>",clSqlBuf);
			    dbg(DEBUG,"%05d:clXYAlid <%s>",__LINE__,clXYAlid);
			    if(ilRc == RC_SUCCESS)
			    {
				llNext = ARR_NEXT;
				blAlidFound = TRUE;
				InitializeAlidListRow(cgAlidListBuf);
				
				ilRc = AATArrayAddRow(&(rgAlidListArray.rrArrayHandle),&(rgAlidListArray.crArrayName[0]),&llNext,(void *)cgAlidListBuf);

				AATArrayPutField(&(rgAlidListArray.rrArrayHandle),
						 &(rgAlidListArray.crArrayName[0]),NULL,"ALID",llNext,TRUE,clXYAlid);
				AATArrayPutField(&(rgAlidListArray.rrArrayHandle),
						 &(rgAlidListArray.crArrayName[0]),NULL,"ALOC",llNext,TRUE,ALOFIELD(pclAloRow,igAloAloc));
				AATArrayPutField(&(rgAlidListArray.rrArrayHandle),
						 &(rgAlidListArray.crArrayName[0]),NULL,"UAID",llNext,TRUE,JOBFIELD(pclJobRow,igJobUaid));
				AATArrayPutField(&(rgAlidListArray.rrArrayHandle),
						 &(rgAlidListArray.crArrayName[0]),NULL,"UALO",llNext,TRUE,ALOFIELD(pclAloRow,igAloUrno));


			    }
			    close_my_cursor(&slCursor);
			    commit_work();
			    slCursor = 0;
			}			
		    }
		}
	    }
	}
	if(blAlidFound)
	{
	    CEDAArrayPutField(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,"ALID",llAction,clXYAlid);
	}
	blAlidFound = FALSE;

	llRowNum = ARR_FIRST;
	llJobType = atol(JOBFIELD(pclJobRow,igJobUjty));
	if( (llJobType!=lgUjtyBrk) && (llJobType!=lgUjtyDel) && (llJobType!=lgUjtyDet) )
	{
		if(CEDAArrayFindRowPointer(&(rgJodArray.rrArrayHandle),
					   &(rgJodArray.crArrayName[0]),
					   &(rgJodArray.rrIdx02Handle),
					   &(rgJodArray.crIdx02Name[0]),
					   JOBFIELD(pclJobRow,igJobUrno),&llRowNum,
					   (void *) &pclJodRow ) == RC_NOTFOUND)
		{
			sprintf(pclSelection,"WHERE UJOB = '%s'",JOBFIELD(pclJobRow,igJobUrno));
			dbg(DEBUG,"PrepareJobData Jod Selection  <%s>",pclSelection); 
			CEDAArrayRefill(&rgJodArray.rrArrayHandle,rgJodArray.crArrayName,pclSelection,NULL,ARR_NEXT);
		}
	}
	llAction = ARR_NEXT;
    }

    ilRc = CEDAArrayWriteDB(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);
    if (ilRc != RC_SUCCESS)
    {
	int ilOldDebugLevel = debug_level;
	debug_level = TRACE;
	dbg(TRACE,"PrepareJobData Error writing to JOBTAB ");
	debug_level = ilOldDebugLevel;

    }

    return ilRc;
}


/******************************************************************************/
/******************************************************************************/

static int PrepareSingleJobData(char *pcpUrno)
{
    int	ilRc = RC_SUCCESS;             /* Return code */
    long llFirst = ARR_FIRST;
    long llRowNum = ARR_FIRST;
    long llNext = ARR_NEXT;
    char *pclJobRow = NULL;

    char *pclAloRow = NULL;
    char * pclAlidListRow = NULL;
	
    char *pclDot;
    char clSqlBuf[124];
    short slCursor;
    short slSqlFunc;

    char clXYTab[12];
    char clXYName[12];
    static char clXYAlid[20];
    char clKey[20];
    BOOL blAlidFound = FALSE;

    if(CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
			       &(rgJobArray.crArrayName[0]),
			       &(rgJobArray.rrIdx02Handle),
			       &(rgJobArray.crIdx02Name[0]),
			       pcpUrno,&llFirst,
			       (void *) &pclJobRow ) == RC_SUCCESS)
    {

	llRowNum = ARR_FIRST;


	if( AATArrayFindRowPointer(&(rgAlidListArray.rrArrayHandle),&(rgAlidListArray.crArrayName[0]),
				   &(rgAlidListArray.rrIdx02Handle),
				   &(rgAlidListArray.crIdx02Name[0]),
				   JOBFIELD(pclJobRow,igJobUaid),&llRowNum,
				   (void *) &pclAlidListRow ) == RC_SUCCESS)
	{
	    sprintf(clXYAlid,ALIDLISTFIELD(pclAlidListRow,igAlidListAlid));
	    blAlidFound = TRUE;
	}
	else
	{
	    llRowNum = ARR_FIRST;
	    if(CEDAArrayFindRowPointer(&(rgAloArray.rrArrayHandle),
				       &(rgAloArray.crArrayName[0]),
				       &(rgAloArray.rrIdx01Handle),
				       &(rgAloArray.crIdx01Name[0]),
				       JOBFIELD(pclJobRow,igJobUalo),&llRowNum,
				       (void *) &pclAloRow ) == RC_SUCCESS)
	    {
		sprintf(clKey,ALOFIELD(pclAloRow,igAloReft));

				
		if ((pclDot = strchr(clKey,'.')) != NULL)
		{
		    pclDot++;
		    strcpy(clXYName,pclDot);
		    pclDot--;
		    *pclDot = '\0';
		    strcpy(clXYTab,clKey);
				
		    if (atol(JOBFIELD(pclJobRow,igJobUaid))  > 0 )
		    {
			sprintf(clSqlBuf,
				"SELECT %s FROM %sTAB WHERE URNO = '%s' AND HOPO = '%s'",clXYName,clXYTab,JOBFIELD(pclJobRow,igJobUaid),cgHopo);
			slSqlFunc = START;
			slCursor = 0;
			ilRc = sql_if(slSqlFunc,&slCursor,clSqlBuf,clXYAlid);
			dbg(DEBUG,"sql_if Selection <%s>",clSqlBuf);
			dbg(DEBUG,"%05d:clXYAlid <%s>",__LINE__,clXYAlid);
			if(ilRc == RC_SUCCESS)
			{
			    llNext = ARR_NEXT;
			    blAlidFound = TRUE;
			    InitializeAlidListRow(cgAlidListBuf);
			
			    ilRc = AATArrayAddRow(&(rgAlidListArray.rrArrayHandle),&(rgAlidListArray.crArrayName[0]),&llNext,(void *)cgAlidListBuf);

			    AATArrayPutField(&(rgAlidListArray.rrArrayHandle),
					     &(rgAlidListArray.crArrayName[0]),NULL,"ALID",llNext,TRUE,clXYAlid);
			    AATArrayPutField(&(rgAlidListArray.rrArrayHandle),
					     &(rgAlidListArray.crArrayName[0]),NULL,"ALOC",llNext,TRUE,ALOFIELD(pclAloRow,igAloAloc));
			    AATArrayPutField(&(rgAlidListArray.rrArrayHandle),
					     &(rgAlidListArray.crArrayName[0]),NULL,"UAID",llNext,TRUE,JOBFIELD(pclJobRow,igJobUaid));
			    AATArrayPutField(&(rgAlidListArray.rrArrayHandle),
					     &(rgAlidListArray.crArrayName[0]),NULL,"UALO",llNext,TRUE,ALOFIELD(pclAloRow,igAloUrno));


			}
			close_my_cursor(&slCursor);
			commit_work();
			slCursor = 0;	
		    }		
		}
	    }
	}
	if(blAlidFound)
	{
	    CEDAArrayPutField(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,"ALID",llFirst,clXYAlid);
	}
	
    }
    ilRc = CEDAArrayWriteDB(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);
    if (ilRc != RC_SUCCESS)
    {
	int ilOldDebugLevel = debug_level;
	debug_level = TRACE;
	dbg(TRACE,"PrepareSingleJobData Error writing to JOBTAB ");
	debug_level = ilOldDebugLevel;

    }

    return ilRc;
}


/******************************************************************************/
/******************************************************************************/

static int ProcessNewPoolJob(char *pcpUrno,ARRAYINFO *prpPoolJobArray,BOOL bpIsReallyNew, BOOL bpCreateBreak)
{
    int	ilRc = RC_SUCCESS;             /* Return code */
    long llAction = ARR_FIRST;
    long llRowNum = ARR_FIRST;
    long llFirst = ARR_FIRST;
    long llNext = ARR_NEXT;
    char clKey[100];
    char *pclPoolJobRow = NULL;
    char *pclDrrRow = NULL;
    char *pclBsdRow = NULL;
    char *pclSpfRow = NULL;
    char *pclSpeRow = NULL;
    char *pclDrdRow = NULL;
    char *pclDrgRow = NULL;
    char *pclDlgRow = NULL;
    char *pclGroupNameRow = NULL;
    char *pclSwgRow = NULL;

    char *pclDelRow = NULL;
    char *pclPolRow = NULL;
    char *pclSgrRow = NULL;
    char *pclSgmRow = NULL;
    char *pclPipe = NULL;
    char *pclPipe2 = NULL;
    char clPrmc[400];
    char clPerm[40];
    char clFctc[12];
    char clSpfFctc[12];
    BOOL blFunctionFound = FALSE;
    BOOL blSpfCodeFound = FALSE;

    long llPoolJobStart;
    long llPoolJobEnd;
    long llVato;
    long llVafr;
    int ilPrio = 1, ilIndDisact;
    char clPrio[5];
    char clSpfVpfr[20] = "\0";
    char clSpfVpto[20] = "\0";
    char pclNowTime[20] = "\0";
    char clSwgVpto[20] = "\0";

	if ( !pcpUrno[0] )
	{	/* all Pooljobs are handle -> disactivate indices which are not used */
		/*							  and may cause reorganization */
		CEDAArrayDisactivateIndex ( &(prpPoolJobArray->rrArrayHandle),	
									&(prpPoolJobArray->crArrayName[0]),
									&(prpPoolJobArray->rrIdx02Handle),
									&(prpPoolJobArray->crIdx02Name[0]));
		CEDAArrayDisactivateIndex ( &(prpPoolJobArray->rrArrayHandle),	
									&(prpPoolJobArray->crArrayName[0]),
									&(prpPoolJobArray->rrIdx04Handle),
									&(prpPoolJobArray->crIdx04Name[0]));
		CEDAArrayDisactivateIndex ( &(prpPoolJobArray->rrArrayHandle),	
									&(prpPoolJobArray->crArrayName[0]),
									&(prpPoolJobArray->rrIdx06Handle),
									&(prpPoolJobArray->crIdx06Name[0]));
		dbg(TRACE,"ProcessNewPoolJob Indices disactivated");
	}

    while(CEDAArrayFindRowPointer(&(prpPoolJobArray->rrArrayHandle),
				  &(prpPoolJobArray->crArrayName[0]),
				  &(prpPoolJobArray->rrIdx01Handle),
				  &(prpPoolJobArray->crIdx01Name[0]),
				  pcpUrno,&llRowNum,
				  (void *) &pclPoolJobRow ) == RC_SUCCESS)
    {
		dbg(TRACE,"ProcessNewPoolJob UDSR <%s>",JOBFIELD(pclPoolJobRow,igJobUdsr));
		dbg(TRACE,"ProcessNewPoolJob UPoolJob <%s>",JOBFIELD(pclPoolJobRow,igJobUrno));
		blFunctionFound = FALSE;
		llFirst = ARR_FIRST;

		llPoolJobStart = atol(POOLJOBFIELD(pclPoolJobRow,igJobAcfr));
		llPoolJobEnd = atol(POOLJOBFIELD(pclPoolJobRow,igJobActo));
		/* Process basic data's workgroup assignment */
		while(CEDAArrayFindRowPointer(&(rgSwgArray.rrArrayHandle),
						  &(rgSwgArray.crArrayName[0]),
						  &(rgSwgArray.rrIdx01Handle),
						  &(rgSwgArray.crIdx01Name[0]),
						  JOBFIELD(pclPoolJobRow,igJobUstf) ,&llFirst,
						  (void *) &pclSwgRow ) == RC_SUCCESS)														
		{
			llVato = atol(SWGFIELD(pclSwgRow,igSwgVpto));
			llVafr = atol(SWGFIELD(pclSwgRow,igSwgVpfr));
			strcpy ( clSwgVpto, SWGFIELD(pclSwgRow,igSwgVpto) );
			TrimRight(clSwgVpto);
				
			/* hag20020328: doesn't work with year 2010 
			   if( (llPoolJobStart > llVafr) && ((llPoolJobEnd < llVato) || (llVato <= 0)) )	*/
			if( ( strcmp(POOLJOBFIELD(pclPoolJobRow,igJobAcfr), SWGFIELD(pclSwgRow,igSwgVpfr)) >0 ) &&
				( (strcmp(POOLJOBFIELD(pclPoolJobRow,igJobActo),clSwgVpto) <0 ) || !(clSwgVpto[0]) ) )
			{
				dbg(TRACE,"ProcessNewPoolJob SwgCode <%s>",SWGFIELD(pclSwgRow,igSwgCode));

				CEDAArrayPutField(&(prpPoolJobArray->rrArrayHandle),
						  &(prpPoolJobArray->crArrayName[0]),NULL,"WGPC",llRowNum,SWGFIELD(pclSwgRow,igSwgCode));

				llFirst = ARR_FIRST;
				if(AATArrayFindRowPointer(&(rgGroupNameArray.rrArrayHandle),
							  &(rgGroupNameArray.crArrayName[0]),
							  &(rgGroupNameArray.rrIdx01Handle),
							  &(rgGroupNameArray.crIdx01Name[0]),
							  SWGFIELD(pclSwgRow,igSwgCode) ,&llFirst,
							  (void *) &pclGroupNameRow ) == RC_NOTFOUND)
				{
					llFirst = ARR_FIRST;
					InitializeGroupNameRow(cgGroupNameBuf);
					AATArrayAddRow(&(rgGroupNameArray.rrArrayHandle),&(rgGroupNameArray.crArrayName[0]),&llFirst,(void *)cgGroupNameBuf);
					AATArrayPutField(&(rgGroupNameArray.rrArrayHandle),
							 &(rgGroupNameArray.crArrayName[0]),NULL,"WGPC",llFirst,TRUE,SWGFIELD(pclSwgRow,igSwgCode));
				}
			}
			else
			{
				dbg(TRACE,"ProcessNewPoolJob SwgCode <%s> not valid <%s> <%s>",SWGFIELD(pclSwgRow,igSwgCode),
					SWGFIELD(pclSwgRow,igSwgVpfr),SWGFIELD(pclSwgRow,igSwgVpto));
				dbg (TRACE,"ProcessNewPoolJob Swg not valid <%ld->%ld> Pooljob <%ld->%ld>", 
					 llVafr, llVato, llPoolJobStart, llPoolJobEnd );
			}
		}

		llFirst = ARR_FIRST;
		/*  Process employees shift DRRTAB */
		if(CEDAArrayFindRowPointer(&(rgDrrArray.rrArrayHandle),
					   &(rgDrrArray.crArrayName[0]),
					   &(rgDrrArray.rrIdx01Handle),
					   &(rgDrrArray.crIdx01Name[0]),
					   JOBFIELD(pclPoolJobRow,igJobUdsr) ,&llFirst,
					   (void *) &pclDrrRow ) == RC_SUCCESS)
		{
			dbg(TRACE,"ProcessNewPoolJob Drr found <%s>",pclDrrRow);
			blFunctionFound = FALSE;

			if(bpCreateBreak == TRUE)
			{
				CreateBreakForNewPoolJob(JOBFIELD(pclPoolJobRow,igJobUrno), JOBFIELD(pclPoolJobRow,igJobUdsr),
										 JOBFIELD(pclPoolJobRow,igJobUstf), DRRFIELD(pclDrrRow,igDrrSblu),
										 DRRFIELD(pclDrrRow,igDrrSbfr), DRRFIELD(pclDrrRow,igDrrSbto),
										 JOBFIELD(pclPoolJobRow,igJobAcfr), JOBFIELD(pclPoolJobRow,igJobActo));
			}
					
			/*if(bpIsReallyNew) hag:20020131:  WGPC must be filled even if pooljob is not new */
			{
				sprintf(clKey,"%s,%s,%s",DRRFIELD(pclDrrRow,igDrrStfu),
					DRRFIELD(pclDrrRow,igDrrSday),
					DRRFIELD(pclDrrRow,igDrrDrrn));		
						
				llFirst = ARR_FIRST;
				/*  process group delegation  DLGTAB */		
				if(CEDAArrayFindRowPointer(&(rgDlgArray.rrArrayHandle),
							   &(rgDlgArray.crArrayName[0]),
							   &(rgDlgArray.rrIdx01Handle),
							   &(rgDlgArray.crIdx01Name[0]),
							   JOBFIELD(pclPoolJobRow,igJobUrno)  ,&llFirst,
							   (void *) &pclDlgRow ) == RC_SUCCESS)
				{
					dbg(TRACE,"ProcessNewPoolJob Dlg found <%s> Row <%ld> JobUrno <%s>",
					pclDlgRow,llFirst, JOBFIELD(pclPoolJobRow,igJobUrno) );
					blFunctionFound = TRUE;
					/*
					InitializePoolFctRow(cgPoolFctBuf);
							
					strcpy(POOLFCTFIELD(cgPoolFctBuf,igPoolFctUjob),JOBFIELD(pclPoolJobRow,igJobUrno));
					strcpy(POOLFCTFIELD(cgPoolFctBuf,igPoolFctFctc),DLGFIELD(pclDlgRow,igDlgFctc));

					AATArrayAddRow(&(rgPoolFctArray.rrArrayHandle),&(rgPoolFctArray.crArrayName[0]),&llNext,(void *)cgPoolFctBuf);
					*/
					strcpy(clFctc,DLGFIELD(pclDlgRow,igDlgFctc));
					AddPoolFctRowIfNew ( JOBFIELD(pclPoolJobRow,igJobUrno), clFctc, 1 );

					dbg(TRACE,"ProcessNewPoolJob DlgWgpc <%s>",DLGFIELD(pclDlgRow,igDlgWgpc));

					CEDAArrayPutField(&(prpPoolJobArray->rrArrayHandle),
							  &(prpPoolJobArray->crArrayName[0]),NULL,"WGPC",llRowNum,DLGFIELD(pclDlgRow,igDlgWgpc));

					llFirst = ARR_FIRST;
					if(AATArrayFindRowPointer(&(rgGroupNameArray.rrArrayHandle),
								  &(rgGroupNameArray.crArrayName[0]),
								  &(rgGroupNameArray.rrIdx01Handle),
								  &(rgGroupNameArray.crIdx01Name[0]),
								  DLGFIELD(pclDlgRow,igDlgWgpc) ,&llFirst,
								  (void *) &pclGroupNameRow ) == RC_NOTFOUND)
					{
						llFirst = ARR_FIRST;
						InitializeGroupNameRow(cgGroupNameBuf);
						AATArrayAddRow(&(rgGroupNameArray.rrArrayHandle),&(rgGroupNameArray.crArrayName[0]),&llFirst,(void *)cgGroupNameBuf);
						AATArrayPutField(&(rgGroupNameArray.rrArrayHandle),
										 &(rgGroupNameArray.crArrayName[0]),NULL,"WGPC",llFirst,TRUE,DLGFIELD(pclDlgRow,igDlgWgpc));
					}
				}
				else
				{
					llFirst = ARR_FIRST;
					if(CEDAArrayFindRowPointer(&(rgDrgArray.rrArrayHandle),
											   &(rgDrgArray.crArrayName[0]),
											   &(rgDrgArray.rrIdx01Handle),
											   &(rgDrgArray.crIdx01Name[0]),
											   clKey ,&llFirst,
											   (void *) &pclDrgRow ) == RC_SUCCESS)
					{
						dbg(TRACE,"ProcessNewPoolJob Drg found <%s>",pclDrgRow);
							
						blFunctionFound = TRUE;
						/*
						InitializePoolFctRow(cgPoolFctBuf);
									
						strcpy(POOLFCTFIELD(cgPoolFctBuf,igPoolFctUjob),JOBFIELD(pclPoolJobRow,igJobUrno));
						strcpy(POOLFCTFIELD(cgPoolFctBuf,igPoolFctFctc),DRGFIELD(pclDrgRow,igDrgFctc));
						AATArrayAddRow(&(rgPoolFctArray.rrArrayHandle),&(rgPoolFctArray.crArrayName[0]),&llNext,(void *)cgPoolFctBuf);
						*/
						strcpy(clFctc,DRGFIELD(pclDrgRow,igDrgFctc));
						AddPoolFctRowIfNew ( JOBFIELD(pclPoolJobRow,igJobUrno), clFctc, 1 );										
						dbg(TRACE,"ProcessNewPoolJob DrgWgpc <%s>",DRGFIELD(pclDrgRow,igDrgWgpc));

						CEDAArrayPutField(&(prpPoolJobArray->rrArrayHandle),
								  &(prpPoolJobArray->crArrayName[0]),NULL,"WGPC",llRowNum,DRGFIELD(pclDrgRow,igDrgWgpc));
						if(AATArrayFindRowPointer(&(rgGroupNameArray.rrArrayHandle),
									  &(rgGroupNameArray.crArrayName[0]),
									  &(rgGroupNameArray.rrIdx01Handle),
									  &(rgGroupNameArray.crIdx01Name[0]),
									  DRGFIELD(pclDrgRow,igDrgWgpc),&llFirst,
									  (void *) &pclGroupNameRow ) == RC_NOTFOUND)
						{
							llFirst = ARR_FIRST;
							InitializeGroupNameRow(cgGroupNameBuf);
							AATArrayAddRow(&(rgGroupNameArray.rrArrayHandle),
										   &(rgGroupNameArray.crArrayName[0]),
										   &llFirst,(void *)cgGroupNameBuf);
							AATArrayPutField(&(rgGroupNameArray.rrArrayHandle),
											 &(rgGroupNameArray.crArrayName[0]),
											 NULL,"WGPC",llFirst,TRUE,DRGFIELD(pclDrgRow,igDrgWgpc) );
						}
					}
				}				
				llFirst = ARR_FIRST;
				if(CEDAArrayFindRowPointer(&(rgDrdArray.rrArrayHandle),
							   &(rgDrdArray.crArrayName[0]),
							   &(rgDrdArray.rrIdx01Handle),
							   &(rgDrdArray.crIdx01Name[0]),
							   clKey ,&llFirst,
							   (void *) &pclDrdRow ) == RC_SUCCESS)
				{
					dbg(TRACE,"ProcessNewPoolJob Drd found <%s>",pclDrdRow);
					
					strcpy(clPrmc,DRDFIELD(pclDrdRow,igDrdPrmc));

					pclPipe = clPrmc;
					while ((pclPipe2 = strchr(pclPipe,'|')) != NULL)
					{
						*pclPipe2 = '\0';
						strcpy(clPerm,pclPipe);
						pclPipe2++;
						pclPipe = pclPipe2;
						
						AddPoolPerRowIfNew ( JOBFIELD(pclPoolJobRow,igJobUrno), clPerm, 1 );
						/*		
						InitializePoolPerRow(cgPoolPerBuf);
									
						strcpy(POOLPERFIELD(cgPoolPerBuf,igPoolPerUjob),JOBFIELD(pclPoolJobRow,igJobUrno));
						strcpy(POOLPERFIELD(cgPoolPerBuf,igPoolPerPerm),clPerm);
						AATArrayAddRow(&(rgPoolPerArray.rrArrayHandle),&(rgPoolPerArray.crArrayName[0]),&llNext,(void *)cgPoolPerBuf);
						*/
					}
					/*
					InitializePoolPerRow(cgPoolPerBuf);
							
					strcpy(POOLPERFIELD(cgPoolPerBuf,igPoolPerUjob),JOBFIELD(pclPoolJobRow,igJobUrno));
					strcpy(POOLPERFIELD(cgPoolPerBuf,igPoolPerPerm),pclPipe);
					AATArrayAddRow(&(rgPoolPerArray.rrArrayHandle),&(rgPoolFctArray.crArrayName[0]),&llNext,(void *)cgPoolPerBuf);
					*/
					AddPoolPerRowIfNew ( JOBFIELD(pclPoolJobRow,igJobUrno), pclPipe, 1 );

					if(!blFunctionFound )
					{
						/*
						InitializePoolFctRow(cgPoolFctBuf);
									
						strcpy(POOLFCTFIELD(cgPoolFctBuf,igPoolFctUjob),JOBFIELD(pclPoolJobRow,igJobUrno));
						strcpy(POOLFCTFIELD(cgPoolFctBuf,igPoolFctFctc),DRDFIELD(pclDrdRow,igDrdFctc));
						AATArrayAddRow(&(rgPoolFctArray.rrArrayHandle),&(rgPoolFctArray.crArrayName[0]),&llNext,(void *)cgPoolFctBuf);
						*/
						strcpy(clFctc,DRDFIELD(pclDrdRow,igDrdFctc));
						AddPoolFctRowIfNew ( JOBFIELD(pclPoolJobRow,igJobUrno), clFctc, 1 );

					}							
				}
				else
				{
					llFirst = ARR_FIRST;
					if(CEDAArrayFindRowPointer(&(rgDelArray.rrArrayHandle),
											   &(rgDelArray.crArrayName[0]),
											   &(rgDelArray.rrIdx01Handle),
											   &(rgDelArray.crIdx01Name[0]),
											   JOBFIELD(pclPoolJobRow,igJobUdel) ,&llFirst,
											   (void *) &pclDelRow ) == RC_SUCCESS)
					{
						dbg(TRACE,"ProcessNewPoolJob Del found <%s>",pclDelRow);

						blFunctionFound = TRUE;
						/*
						InitializePoolFctRow(cgPoolFctBuf);
									
						strcpy(POOLFCTFIELD(cgPoolFctBuf,igPoolFctUjob),JOBFIELD(pclPoolJobRow,igJobUrno));
						strcpy(POOLFCTFIELD(cgPoolFctBuf,igPoolFctFctc),DELFIELD(pclDelRow,igDelFctc));
						AATArrayAddRow(&(rgPoolFctArray.rrArrayHandle),&(rgPoolFctArray.crArrayName[0]),&llNext,(void *)cgPoolFctBuf);
						*/
						strcpy(clFctc,DELFIELD(pclDelRow,igDelFctc));
						AddPoolFctRowIfNew ( JOBFIELD(pclPoolJobRow,igJobUrno), clFctc, 1 );
					}
					if(!blFunctionFound )
					{					
						strcpy(clFctc,DRRFIELD(pclDrrRow,igDrrFctc));
						TrimRight(clFctc); 
						if(strlen(clFctc) > 1)
						{
							blFunctionFound = TRUE;
							
							/* InitializePoolFctRow(cgPoolFctBuf); */
							/* hag 20021320:take original FCTC again, to keep trailing blanks */
							strcpy(clFctc,DRRFIELD(pclDrrRow,igDrrFctc)); 
							clFctc[5] = '\0';	/* hag: 20011207: only to be sure, normally there is no fctc longer than 5 chars */

							/*strcpy(POOLFCTFIELD(cgPoolFctBuf,igPoolFctUjob),JOBFIELD(pclPoolJobRow,igJobUrno));
							strcpy(POOLFCTFIELD(cgPoolFctBuf,igPoolFctFctc),clFctc );
							AATArrayAddRow(&(rgPoolFctArray.rrArrayHandle),&(rgPoolFctArray.crArrayName[0]),&llNext,(void *)cgPoolFctBuf);*/
							AddPoolFctRowIfNew ( JOBFIELD(pclPoolJobRow,igJobUrno), clFctc, 1 );
						}
					}
					llFirst = ARR_FIRST;

					if(!blFunctionFound && (CEDAArrayFindRowPointer(&(rgBsdArray.rrArrayHandle),
											&(rgBsdArray.crArrayName[0]),
											&(rgBsdArray.rrIdx01Handle),
											&(rgBsdArray.crIdx01Name[0]),
											DRRFIELD(pclDrrRow,igDrrBsdu) ,&llFirst,
											(void *) &pclBsdRow )) == RC_SUCCESS)
					{
						/* InitializePoolFctRow(cgPoolFctBuf); */
									
						strcpy(clFctc,BSDFIELD(pclBsdRow,igBsdFctc));
						TrimRight(clFctc);
						if(strlen(clFctc) > 1)
						{
							blFunctionFound = TRUE;
							/*
							strcpy(POOLFCTFIELD(cgPoolFctBuf,igPoolFctUjob),JOBFIELD(pclPoolJobRow,igJobUrno));
							strcpy(POOLFCTFIELD(cgPoolFctBuf,igPoolFctFctc),BSDFIELD(pclBsdRow,igBsdFctc));
							AATArrayAddRow(&(rgPoolFctArray.rrArrayHandle),&(rgPoolFctArray.crArrayName[0]),&llNext,(void *)cgPoolFctBuf);*/
							AddPoolFctRowIfNew ( JOBFIELD(pclPoolJobRow,igJobUrno), BSDFIELD(pclBsdRow,igBsdFctc), 1 );
						}
					}
					/*FSC changes Multi Function*/

					/*if(!blFunctionFound )
					  {*/

					blSpfCodeFound = FALSE;
					llAction = ARR_FIRST;
					if(!blFunctionFound )
					{
						dbg(TRACE,"ProcessNewPoolJob No Function found UPJB <%s>",JOBFIELD(pclPoolJobRow,igJobUrno));
					}
					while (CEDAArrayFindRowPointer(&(rgSpfArray.rrArrayHandle),
								   &(rgSpfArray.crArrayName[0]),
								   &(rgSpfArray.rrIdx01Handle),
								   &(rgSpfArray.crIdx01Name[0]),
								   DRRFIELD(pclDrrRow,igDrrStfu) ,&llAction,
								   (void *) &pclSpfRow ) == RC_SUCCESS)
					{	
						BOOL blAddRow = TRUE;
						llAction = ARR_NEXT;

						dbg(TRACE,"ProcessNewPoolJob SpfCode <%s> SpfPrio <%s>",SPFFIELD(pclSpfRow,igSpfCode),SPFFIELD(pclSpfRow,igSpfPrio));
						strcpy(clSpfVpfr,SPFFIELD(pclSpfRow,igSpfVpfr));
						strcpy(clSpfVpto,SPFFIELD(pclSpfRow,igSpfVpto));
						TimeToStr(pclNowTime,tgCurrTime);
						dbg(TRACE,"%05d: SPF: vpfr: <%s>, currenttime: <%s>, vpto: <%s>",__LINE__,clSpfVpfr,pclNowTime,clSpfVpto);
						TrimRight(clSpfVpto);

						if((strcmp(clSpfVpfr,pclNowTime) <= 0) && 
						   ((strcmp(clSpfVpto,pclNowTime) >= 0) || (strcmp(clSpfVpto, "")== 0 )))
						{
							dbg(TRACE,"%05d: SPF has valid period",__LINE__);

							/* InitializePoolFctRow(cgPoolFctBuf); */
								
							ilPrio = atoi(SPFFIELD(pclSpfRow,igSpfPrio));
							if(ilPrio <= 0)
							{
								ilPrio = 2;
							}
		 						
							if(blFunctionFound )
							{
								strcpy(clSpfFctc,SPFFIELD(pclSpfRow,igSpfCode));

								TrimRight(clSpfFctc);
								if(strcmp(clSpfFctc,clFctc) == 0)  
								{
									blSpfCodeFound = TRUE;
									blAddRow = FALSE;
									ilPrio++;
								}
							}

							if(blAddRow)
							{
								/*
								sprintf(clPrio,"%d",ilPrio);
								strcpy(POOLFCTFIELD(cgPoolFctBuf,igPoolFctPrio),clPrio);
								strcpy(POOLFCTFIELD(cgPoolFctBuf,igPoolFctUjob),JOBFIELD(pclPoolJobRow,igJobUrno));
								strcpy(POOLFCTFIELD(cgPoolFctBuf,igPoolFctFctc),SPFFIELD(pclSpfRow,igSpfCode));
											
											
								llNext = ARR_NEXT;
								AATArrayAddRow(&(rgPoolFctArray.rrArrayHandle),&(rgPoolFctArray.crArrayName[0]),&llNext,(void *)cgPoolFctBuf);*/
								AddPoolFctRowIfNew ( JOBFIELD(pclPoolJobRow,igJobUrno), SPFFIELD(pclSpfRow,igSpfCode), ilPrio );
							}
						}
					}
		/*					}*/
					llAction = ARR_FIRST;
					while (CEDAArrayFindRowPointer(&(rgSpeArray.rrArrayHandle),
								   &(rgSpeArray.crArrayName[0]),
								   &(rgSpeArray.rrIdx01Handle),
								   &(rgSpeArray.crIdx01Name[0]),
								   DRRFIELD(pclDrrRow,igDrrStfu) ,&llAction,
								   (void *) &pclSpeRow ) == RC_SUCCESS)
					{		
						llAction = ARR_NEXT;
						/*
						InitializePoolPerRow(cgPoolPerBuf);
						if( (igSpePrio >0) && ( atoi(SPEFIELD(pclSpeRow,igSpePrio)) > 0) )
						{
							strcpy(POOLPERFIELD(cgPoolPerBuf,igPoolPerPrio),SPEFIELD(pclSpeRow,igSpePrio));
						}

						strcpy(POOLPERFIELD(cgPoolPerBuf,igPoolPerUjob),JOBFIELD(pclPoolJobRow,igJobUrno));
						strcpy(POOLPERFIELD(cgPoolPerBuf,igPoolPerPerm),SPEFIELD(pclSpeRow,igSpeCode));
									
						AATArrayAddRow(&(rgPoolPerArray.rrArrayHandle),&(rgPoolPerArray.crArrayName[0]),&llNext,(void *)cgPoolPerBuf);
						llNext = ARR_NEXT;*/
						strcpy(clSpfVpfr,SPEFIELD(pclSpeRow,igSpeVpfr));
						strcpy(clSpfVpto,SPEFIELD(pclSpeRow,igSpeVpto));
						TrimRight(clSpfVpto);
				
						/* hag20030327: Is qualification valid ??? */ 
						if( ( strcmp(POOLJOBFIELD(pclPoolJobRow,igJobAcfr), clSpfVpfr) >0 ) &&
							( (strcmp(POOLJOBFIELD(pclPoolJobRow,igJobActo),clSpfVpto) <0 ) || !(clSpfVpto[0]) ) )
						{
							dbg ( DEBUG, " Qual. <%s> is valid (%s to %s) for pool job <%s>", 
									  SPEFIELD(pclSpeRow,igSpeCode), clSpfVpfr, clSpfVpto, JOBFIELD(pclPoolJobRow,igJobUrno) );
							ilPrio = 1;
							if( igSpePrio >0 )
								ilPrio = max ( 1, atoi(SPEFIELD(pclSpeRow,igSpePrio)) ) ;
							AddPoolPerRowIfNew ( JOBFIELD(pclPoolJobRow,igJobUrno), SPEFIELD(pclSpeRow,igSpeCode), ilPrio );
						}
						else
							dbg ( DEBUG, " Qual. <%s> not valid (%s to %s) for pool job <%s>", 
									  SPEFIELD(pclSpeRow,igSpeCode), clSpfVpfr, clSpfVpto, JOBFIELD(pclPoolJobRow,igJobUrno) );

					}
				}
			}
			dbg (DEBUG, "ProcessNewPoolJob: Going to set SDAY" );
			CEDAArrayPutField(&(prpPoolJobArray->rrArrayHandle),
					  &(prpPoolJobArray->crArrayName[0]),NULL,"SDAY",llRowNum,DRRFIELD(pclDrrRow,igDrrSday));
		}
		dbg (DEBUG, "ProcessNewPoolJob: Going to set PJVD to 1" );
		CEDAArrayPutField(&(prpPoolJobArray->rrArrayHandle),
				  &(prpPoolJobArray->crArrayName[0]),NULL,"PJVD",llRowNum,"1");
		dbg (DEBUG, "ProcessNewPoolJob: Going to set ISOK" );
		CEDAArrayPutField(&(prpPoolJobArray->rrArrayHandle),
				  &(prpPoolJobArray->crArrayName[0]),NULL,"ISOK",llRowNum,"y");
		dbg (DEBUG, "ProcessNewPoolJob: Going to set TCHX" );
		CEDAArrayPutField(&(prpPoolJobArray->rrArrayHandle),
				  &(prpPoolJobArray->crArrayName[0]),NULL,"TCHX",llRowNum,"0");
		
		llFirst = ARR_FIRST;
		if(CEDAArrayFindRowPointer(&(rgPolArray.rrArrayHandle),
					   &(rgPolArray.crArrayName[0]),
					   &(rgPolArray.rrIdx02Handle),
					   &(rgPolArray.crIdx02Name[0]),
					   JOBFIELD(pclPoolJobRow,igJobUaid),&llFirst,
					   (void *) &pclPolRow ) == RC_SUCCESS)
		{
			llFirst = ARR_FIRST;
			if(CEDAArrayFindRowPointer(&(rgSgrArray.rrArrayHandle),
						   &(rgSgrArray.crArrayName[0]),
						   &(rgSgrArray.rrIdx02Handle),
						   &(rgSgrArray.crIdx02Name[0]),
						   POLFIELD(pclPolRow,igPolName),&llFirst,
						   (void *) &pclSgrRow ) == RC_SUCCESS)
			{
				llFirst = ARR_FIRST;
				if(CEDAArrayFindRowPointer(&(rgSgmArray.rrArrayHandle),
							   &(rgSgmArray.crArrayName[0]),
							   &(rgSgmArray.rrIdx02Handle),
							   &(rgSgmArray.crIdx02Name[0]),
							   SGRFIELD(pclSgrRow,igSgrUrno),&llFirst,
							   (void *) &pclSgmRow ) == RC_SUCCESS)
				{
					CEDAArrayPutField(&(prpPoolJobArray->rrArrayHandle),
							  &(prpPoolJobArray->crArrayName[0]),NULL,"PJVD",llRowNum,"2");
					dbg (DEBUG, "ProcessNewPoolJob: done set PJVD to 2" );
				}
			}
		}

		llRowNum = ARR_NEXT;
    }
	if ( !pcpUrno[0] )
	{	/* all Pooljobs are handle -> disactivate indices which are not used */
		/*							  and may cause reorganization */
		CEDAArrayActivateIndex (&(prpPoolJobArray->rrArrayHandle),	
								&(prpPoolJobArray->crArrayName[0]),
								&(prpPoolJobArray->rrIdx02Handle),
								&(prpPoolJobArray->crIdx02Name[0]));
		CEDAArrayActivateIndex (&(prpPoolJobArray->rrArrayHandle),	
								&(prpPoolJobArray->crArrayName[0]),
								&(prpPoolJobArray->rrIdx04Handle),
								&(prpPoolJobArray->crIdx04Name[0]));
		CEDAArrayActivateIndex (&(prpPoolJobArray->rrArrayHandle),	
								&(prpPoolJobArray->crArrayName[0]),
								&(prpPoolJobArray->rrIdx06Handle),
								&(prpPoolJobArray->crIdx06Name[0]));
		dbg(TRACE,"ProcessNewPoolJob Indices activated again");
	}
    CEDAArrayWriteDB(&(prpPoolJobArray->rrArrayHandle),
		     &(prpPoolJobArray->crArrayName[0]),NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);

    return ilRc;
}
/******************************************************************************/
/******************************************************************************/

static int ProcessChangePoolJob(char *pcpUrno)
{
    int	ilRc = RC_SUCCESS;             /* Return code */
    long llRowNum1 = ARR_FIRST;
    char *pclPoolFctRow = NULL;
    char *pclPoolPerRow = NULL;

    while(AATArrayFindRowPointer(&(rgPoolPerArray.rrArrayHandle),
				 &(rgPoolPerArray.crArrayName[0]),
				 &(rgPoolPerArray.rrIdx01Handle),
				 &(rgPoolPerArray.crIdx01Name[0]),
				 pcpUrno,&llRowNum1,
				 (void *) &pclPoolPerRow ) == RC_SUCCESS)
    {
	CEDAArrayDeleteRow(&(rgPoolPerArray.rrArrayHandle),&(rgPoolPerArray.crArrayName[0]),llRowNum1);
	llRowNum1 = ARR_NEXT;
    }

    llRowNum1 = ARR_FIRST;
    while(AATArrayFindRowPointer(&(rgPoolFctArray.rrArrayHandle),
				 &(rgPoolFctArray.crArrayName[0]),
				 &(rgPoolFctArray.rrIdx01Handle),
				 &(rgPoolFctArray.crIdx01Name[0]),
				 pcpUrno,&llRowNum1,
				 (void *) &pclPoolFctRow ) == RC_SUCCESS)
    {
	CEDAArrayDeleteRow(&(rgPoolFctArray.rrArrayHandle),&(rgPoolFctArray.crArrayName[0]),llRowNum1);
	llRowNum1 = ARR_NEXT;
    }

    ProcessNewPoolJob(pcpUrno, &rgPoolJobArray,FALSE,FALSE);

    return ilRc;
}
/******************************************************************************/
/******************************************************************************/


static int ProcessDeleteDemand(char *pcpUrno)
{
    int	ilRc = RC_SUCCESS;             /* Return code */
    long llRowNum = ARR_FIRST;
    long llRowNum2 = ARR_FIRST;
    long llFirst = ARR_FIRST;
    char *pclJobRow = NULL;
    char *pclJodRow = NULL;

    BOOL blJobDeleted = FALSE;

    while(CEDAArrayFindRowPointer(&(rgJodArray.rrArrayHandle),
				  &(rgJodArray.crArrayName[0]),
				  &(rgJodArray.rrIdx01Handle),
				  &(rgJodArray.crIdx01Name[0]),
				  pcpUrno,&llRowNum,
				  (void *) &pclJodRow ) == RC_SUCCESS)
    {
	llFirst = ARR_FIRST;
	CEDAArrayPutField(&(rgJodArray.rrArrayHandle),&(rgJodArray.crArrayName[0]),NULL,"TCHX",
			  llRowNum,"aa") ;	
	dbg(TRACE,"ProcessDeleteDemand JobUrno found <%s>",JODFIELD(pclJodRow,igJodUjob));
	if(CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
				   &(rgJobArray.crArrayName[0]),
				   &(rgJobArray.rrIdx02Handle),
				   &(rgJobArray.crIdx02Name[0]),
				   JODFIELD(pclJodRow,igJodUjob),&llFirst,
				   (void *) &pclJodRow ) == RC_SUCCESS)
	{
	    CEDAArrayPutField(&(rgJobArray.rrArrayHandle),&(rgJobArray.crArrayName[0]),NULL,"TXXD",
			      llFirst,"bb") ;
	}
		
	blJobDeleted = TRUE;
	llRowNum = ARR_NEXT;
    }
									

							
								
    if(blJobDeleted)
    {
	llRowNum2 = ARR_FIRST;
	while(CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
				      &(rgJobArray.crArrayName[0]),
				      &(rgJobArray.rrIdx03Handle),&(rgJobArray.crIdx03Name[0]),
				      "bb",
				      &llRowNum2,(void *)&pclJobRow) == RC_SUCCESS)
	{
	    CEDAArrayDeleteRow(&(rgJobArray.rrArrayHandle),&(rgJobArray.crArrayName[0]),llRowNum2);
	    llRowNum2 = ARR_FIRST;
	}
	llRowNum2 = ARR_FIRST;
	while(CEDAArrayFindRowPointer(&(rgJodArray.rrArrayHandle),
				      &(rgJodArray.crArrayName[0]),
				      &(rgJodArray.rrIdx03Handle),&(rgJodArray.crIdx03Name[0]),
				      "aa",
				      &llRowNum2,(void *)&pclJodRow) == RC_SUCCESS)
	{
	    CEDAArrayDeleteRow(&(rgJodArray.rrArrayHandle),&(rgJodArray.crArrayName[0]),llRowNum2);
	    llRowNum2 = ARR_FIRST;
	}


	if (CEDAArrayWriteDB(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK) != RC_SUCCESS)
	{
			
	    int ilOldDebugLevel = debug_level;
	    debug_level = TRACE;
	    dbg(TRACE,"ProcessDeleteDemand CEDAArrayWriteDB Job error");
	    debug_level = ilOldDebugLevel;

	}
	if (CEDAArrayWriteDB(&rgJodArray.rrArrayHandle,rgJodArray.crArrayName,NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK) != RC_SUCCESS)
	{
	    int ilOldDebugLevel = debug_level;
	    debug_level = TRACE;
	    dbg(TRACE,"ProcessDeleteDemand Error writing to JODTAB ");
	    debug_level = ilOldDebugLevel;
	}
    }
    return ilRc;
}

static int ProcessDeletePoolJob(char *pcpUrno)
{
    int	ilRc = RC_SUCCESS;             /* Return code */
    long llRowNum = ARR_FIRST;
    long llRowNum2 = ARR_FIRST;
    long llFirst = ARR_FIRST;
    char *pclJobRow = NULL;
    char *pclJodRow = NULL;

    BOOL blJobDeleted = FALSE;

    if(atol(pcpUrno) > 0)
    {
	while(CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
				      &(rgJobArray.crArrayName[0]),
				      &(rgJobArray.rrIdx01Handle),
				      &(rgJobArray.crIdx01Name[0]),
				      pcpUrno,&llRowNum,
				      (void *) &pclJobRow ) == RC_SUCCESS)
	{
			
	    dbg(TRACE,"ProcessDeletePoolJob Job found Urno<%s>",JOBFIELD(pclJobRow,igJobUrno));
	    llFirst = ARR_FIRST;
	    while(CEDAArrayFindRowPointer(&(rgJodArray.rrArrayHandle),
					  &(rgJodArray.crArrayName[0]),
					  &(rgJodArray.rrIdx01Handle),
					  &(rgJobArray.crIdx01Name[0]),
					  JOBFIELD(pclJobRow,igJobUrno),&llFirst,
					  (void *) &pclJodRow ) == RC_SUCCESS)
	    {
		CEDAArrayDeleteRow(&(rgJodArray.rrArrayHandle),&(rgJodArray.crArrayName[0]),llFirst);
		llFirst = ARR_FIRST;
	    }
	    CEDAArrayDeleteRow(&(rgJobArray.rrArrayHandle),&(rgJobArray.crArrayName[0]),llRowNum);
	    blJobDeleted = TRUE;
	    llRowNum = ARR_FIRST;
	}
	if(blJobDeleted)
	{
	    CEDAArrayWriteDB(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);
	    CEDAArrayWriteDB(&rgJodArray.rrArrayHandle,rgJodArray.crArrayName,NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);
	}
    }
    return ilRc;
}


/******************************************************************************/
/******************************************************************************/
static int CreateBreakForNewPoolJob(char *pcpUrno,char *pcpUdsr,char *pcpUstf,char *pcpSblu,
				    char *pcpSbfr,char *pcpSbto,char *pcpPoolAcfr,char *pcpPoolActo)
{
    int	ilRc = RC_SUCCESS;             /* Return code */
    char clJtyBrkUrno[20];
    char clPauseEnd[20];
    long llNext = ARR_NEXT;
    char clNow[128];
    char clUrno[20];
    char pclPoolStart[20];
    char pclPoolEnd[20];
    char pclBreakStart[20];
    char pclBreakEnd[20];
    long llRowNum = ARR_FIRST;
    char *pclJobRow = NULL;
    int ilSblu = 0;
    time_t tlSbfr;
    time_t tlPoolStart;
    time_t tlPoolEnd;
 
    dbg(DEBUG,"***** Start CreateBreakForNewPoolJob() *****");

    while((CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
				   &(rgJobArray.crArrayName[0]),
				   &(rgJobArray.rrIdx01Handle),
				   &(rgJobArray.crIdx01Name[0]),
				   pcpUrno,&llRowNum,
				   (void *) &pclJobRow ) == RC_SUCCESS) && (ilRc == RC_SUCCESS))
    {
	if(atol(JOBFIELD(pclJobRow,igJobUjty)) == lgUjtyBrk)
	{
	    ilRc = RC_FAIL;
	}
	llRowNum = ARR_NEXT;
    }
    /* create the pause */
    if(ilRc == RC_SUCCESS)
    {
	StrToTime(pcpPoolAcfr,&tlPoolStart);
	tlPoolStart = tlPoolStart - 120;
	TimeToStr(pclPoolStart,tlPoolStart);
	dbg(DEBUG,"Adjusted Pool Job Begin (-2min) <%s>",pclPoolStart);

	StrToTime(pcpPoolActo,&tlPoolEnd);
	tlPoolEnd = tlPoolEnd + 120;
	TimeToStr(pclPoolEnd,tlPoolEnd);
	dbg(DEBUG,"Adjusted Pool Job End (+2min) <%s>",pclPoolEnd);

	ilSblu = atoi(pcpSblu) * 60;
	dbg(DEBUG,"DRR.SBLU (default pause length in seconds) <%d>",ilSblu);

	dbg(DEBUG,"DRR.SBFR (Pausenlage begin - local) <%s>",pcpSbfr);
	strcpy(pclBreakStart,pcpSbfr);
	LocalToUtc(pclBreakStart);
	dbg(DEBUG,"DRR.SBFR (Pausenlage begin - UTC)   <%s>",pclBreakStart);


	dbg(DEBUG,"DRR.SBTO (Pausenlage end - local) <%s>",pcpSbto);
	strcpy(pclBreakEnd,pcpSbto);

	LocalToUtc(pclBreakEnd);
	dbg(DEBUG,"DRR.SBTO (Pausenlage end - UTC)   <%s>",pclBreakEnd);

	if(ilSblu <= 0 || StrToTime(pclBreakStart,&tlSbfr) != RC_SUCCESS)
	{
	    dbg(DEBUG,"DRR.SBLU <%d> or DRR.SBFR <%d not valid",ilSblu,pclBreakStart);
	    ilRc = RC_FAIL;
	}
	else
	{
	    tlSbfr = tlSbfr + ilSblu;
	    TimeToStr(clPauseEnd,tlSbfr);

	    /* if the pause doesn't fit in the Pausenlage then truncate it */
	    if(strcmp(clPauseEnd,pclBreakEnd) > 0)
	    {
		strcpy(clPauseEnd,pclBreakEnd);
		dbg(DEBUG,"PauseEnd outside Pausenlage - truncate to <%s>",clPauseEnd);
	    }


	    dbg(DEBUG,"Calculated Pause End <%s>",clPauseEnd);
			
	    /* allocate pauses only if the pause is completely within the pool job */
	
	    dbg(DEBUG,"Pause <%s> - <%s>  PoolJob <%s> - <%s>",pclBreakStart,clPauseEnd,pclPoolStart,pclPoolEnd);
	    if(strcmp(pclPoolStart,pclBreakStart) <= 0 && strcmp(pclPoolEnd,clPauseEnd) >= 0)
	    {
		
		sprintf(clJtyBrkUrno,"%ld",lgUjtyBrk);
		/* GetNextValues(clUrno,1);  */
		GetNextUrno(clUrno);
		TimeToStr(clNow,time(NULL));

		memset(cgNewJobBuf,' ',rgJobArray.lrArrayRowLen);

		CEDAArrayAddRow(&(rgJobArray.rrArrayHandle),&(rgJobArray.crArrayName[0]),&llNext,(void *)cgNewJobBuf);
		CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"URNO",llNext,clUrno);
		CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"CDAT",llNext,clNow);
		CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"HOPO",llNext,cgHopo);
		CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"STAT",llNext,"P 0");
		CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"USEC",llNext,"JOBHDL");
		CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"UAFT",llNext,"0");

		CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"JOUR",llNext,pcpUrno);
		CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"PLFR",llNext,pclBreakStart);
		CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"ACFR",llNext,pclBreakStart);
		CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"PLTO",llNext,clPauseEnd);
		CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"ACTO",llNext,clPauseEnd);
		CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"UDSR",llNext,pcpUdsr);
		CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"UJTY",llNext,clJtyBrkUrno);
		CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"USTF",llNext,pcpUstf);
		if (CEDAArrayWriteDB(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK) != RC_SUCCESS)
		{
		    int ilOldDebugLevel = debug_level;
		    debug_level = TRACE;
		    dbg(TRACE,"CreateBreakForNewPoolJob CEDAArrayWriteDB Job error");
		    debug_level = ilOldDebugLevel;
		}
	    }
	    else
	    {
		dbg(DEBUG,"Pause NOT within this pool job!");
		ilRc = RC_FAIL;
	    }
	}
    }
  

    dbg(DEBUG,"***** CreateBreakForNewPoolJob() returns %s *****",(ilRc == RC_SUCCESS )? "RC_SUCCESS" : "RC_FAIL");
    return ilRc;
}


/******************************************************************************/
/******************************************************************************/
static int ProcessSSD(char *pcpData, ARRAYINFO *prpOpenDemArray)
{
    int	ilRc = RC_SUCCESS;             /* Return code */
    char pclSelection[1024];
    char clUrnoSelection[1024];
    long llAction = ARR_FIRST;
    long llRowNum = ARR_FIRST;
 
    int ilItemNo = 1;
    char *pclDemRow = NULL;
    char *pclJodRow = NULL;
    char *pclJobRow = NULL;
    char *pclRudRow = NULL;
    char clKey[9];
    char clFlgs[20];
  
    BOOL  blOpenDemFound = FALSE;
    BOOL  blNoOperateDem = FALSE;
    BOOL  blRowDeleted = FALSE;

/***********************
  int ilOldDebugLevel;


  ilOldDebugLevel = debug_level;
  debug_level = TRACE;
**********************/

    sprintf(clKey,"");
	
    ilItemNo = 1;
    while (GetDataItem(clUrnoSelection,pcpData,ilItemNo,'|',""," \0"))
    {
	sprintf(pclSelection,"WHERE URNO IN (%s)",clUrnoSelection);
	dbg(TRACE,"ProcessSSD Dem Selection: %s",pclSelection);
	CEDAArrayRefill(&rgOpenDemArray.rrArrayHandle,rgOpenDemArray.crArrayName,pclSelection,NULL,llAction);
	llAction = ARR_NEXT;
	ilItemNo++;
    }


    llAction = ARR_FIRST;
	
    while(ilRc = CEDAArrayFindRowPointer(&(rgOpenDemArray.rrArrayHandle),
					 &(rgOpenDemArray.crArrayName[0]),
					 &(rgOpenDemArray.rrIdx01Handle),
					 &(rgOpenDemArray.crIdx01Name[0]),
					 "",&llAction,(void *)&pclDemRow) == RC_SUCCESS)
    {
		
	llRowNum = ARR_FIRST;
	strcpy(clFlgs,OPENDEMFIELD(pclDemRow,igDemFlgs));
	

	llRowNum = ARR_FIRST;
	blNoOperateDem = FALSE;
	if(CEDAArrayFindRowPointer(&(rgRudArray.rrArrayHandle),
				   &(rgRudArray.crArrayName[0]),
				   &(rgRudArray.rrIdx01Handle),
				   &(rgRudArray.crIdx01Name[0]),
				   OPENDEMFIELD(pclDemRow,igDemUrud),&llRowNum,
				   (void *) &pclRudRow ) == RC_SUCCESS)
	{
	    blNoOperateDem = (atoi(RUDFIELD(pclRudRow,igRudFond)) == 1)?TRUE:FALSE;
	}

/*		if(blNoOperateDem || (clFlgs[1] == '1') || (clFlgs[5] == '1') ) hag20021002 */
	if ( blNoOperateDem || !IsDemandToAssign(clFlgs) )

	{
	    CEDAArrayPutField(&(rgOpenDemArray.rrArrayHandle),&(rgOpenDemArray.crArrayName[0]),NULL,"TCHD",
			      llAction,"-1") ;	
	}
	else
	{
	    CEDAArrayPutField(&(rgOpenDemArray.rrArrayHandle),&(rgOpenDemArray.crArrayName[0]),NULL,"TCHD",
			      llAction,"1") ;	

	    llRowNum = ARR_FIRST;
	    while(CEDAArrayFindRowPointer(&(rgJodArray.rrArrayHandle),
					  &(rgJodArray.crArrayName[0]),
					  &(rgJodArray.rrIdx01Handle),
					  &(rgJodArray.crIdx01Name[0]),
					  OPENDEMFIELD(pclDemRow,igDemUrno),&llRowNum,
					  (void *) &pclJodRow ) == RC_SUCCESS)
	    {
		llRowNum = ARR_FIRST;
		dbg(TRACE,"ProcessSSD Jod Found Ujob <%s> Udem <%s>",JODFIELD(pclJodRow,igJodUjob),OPENDEMFIELD(pclDemRow,igDemUrno));
		if(CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
					   &(rgJobArray.crArrayName[0]),
					   &(rgJobArray.rrIdx02Handle),
					   &(rgJobArray.crIdx02Name[0]),
					   JODFIELD(pclJodRow,igJodUjob),&llRowNum,
					   (void *) &pclJobRow ) == RC_SUCCESS)
		{
		    CEDAArrayPutField(&(rgOpenDemArray.rrArrayHandle),&(rgOpenDemArray.crArrayName[0]),NULL,"TCHD",
				      llAction,"-1") ;	
		    dbg(TRACE,"ProcessSSD Job Found");
		}
		llRowNum = ARR_NEXT;
	    }
	}
	llAction = ARR_NEXT;
    }

    PrepareDemArray(prpOpenDemArray);
/* In OpenDemArray sind die preparierten Bedarfe
   In DemArray ist leer*/

    ProcessAOJ(&rgOpenDemArray,&rgPoolJobArray);

    bgIgnoreBreaks = bgDefaultIgnoreBreaks;


    igJobEndBuffer = igDefaultJobEndBuffer;
    igMinJobDistance = igDefaultMinJobDistance;
    bgAllowEmptyAlid = FALSE;

    /***********************
	 debug_level = ilOldDebugLevel;
    ***********************/

    return(ilRc) ;

} /* ProcessSSD */

/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
static int ProcessAFL(char *pcpFields, char *pcpData)
{
    int	ilRc        = RC_SUCCESS;             /* Return code */
    int ilItemNo,ilCol,ilPos;
    int ili = 0;
    char pclNoOfDemands[24];
    char pclAddData[256];
    char clPoolUrnos[1024];
    char clTmpBuf[20];
    char pclSelection[1024];
    char clUrnoSelection[1024] = "\0";
    char clUrnoSelection2[1024] = "\0";

    char clPoolSelection[256];
    long llAction = ARR_FIRST;
    long llRowNum = ARR_FIRST;
    long llRowNum2 = ARR_FIRST;
    long llRowNum3 = ARR_FIRST;
    char *pclDemRow = NULL;
    char *pclJodRow = NULL;
    char *pclJobRow = NULL;

    long llNoOfDemands = 0;

    long llUrnoLen = 0;
	
    char clLoadStart[20];
    char clLoadEnd[20];
    char clAssignTyp[10];
    char clResourceTab[10];
    char clStatus[20];
    char clFlgs[20];
    BOOL blDeleteJobs = FALSE;
    BOOL blDeleteOnly = FALSE;
    BOOL blJobIsOk = FALSE;
    BOOL blJobFound = FALSE;
    long llUrnoRowNum = ARR_FIRST;
    char *pclUrnoRow = NULL;
    int ilUrnoCount = 0;
    char clTmpUrno[20] = "\0";
    time_t tlLoadStart = tgLoadStart;
    time_t tlLoadEnd = tgLoadEnd;

    llUrnoRowNum = ARR_FIRST;
    while(AATArrayFindRowPointer(&(rgUrnoArray.rrArrayHandle),
				 &(rgUrnoArray.crArrayName[0]),
				 &(rgUrnoArray.rrIdx01Handle),
				 &(rgUrnoArray.crIdx01Name[0]),
				 "",&llUrnoRowNum,
				 (void *) &pclUrnoRow ) == RC_SUCCESS)
    {
		llUrnoRowNum = ARR_FIRST;
		AATArrayDeleteRow(&(rgUrnoArray.rrArrayHandle),&(rgUrnoArray.crArrayName[0]),llUrnoRowNum);
		/*llUrnoRowNum = ARR_NEXT;*/
    }

    llUrnoRowNum = ARR_FIRST;
    while(AATArrayFindRowPointer(&(rgUrnoArray.rrArrayHandle),
				 &(rgUrnoArray.crArrayName[0]),
				 &(rgUrnoArray.rrIdx01Handle),
				 &(rgUrnoArray.crIdx01Name[0]),
				 "",&llUrnoRowNum,
				 (void *) &pclUrnoRow ) == RC_SUCCESS)
    {
		dbg(TRACE,"%05d: Record in Array found",__LINE__);
		llUrnoRowNum = ARR_NEXT;
    }

    dbg(TRACE,"ProcessAFL pcpFields<%s> pcpData<%s>",pcpFields,pcpData);

    ilRc = FindItemInList(pcpFields,"NODEM",',',&ilItemNo,&ilCol,&ilPos);

    dbg(TRACE,"ProcessAFL ilRc<%d> ilItemNo<%d>",ilRc,ilItemNo);
    if (ilRc == RC_SUCCESS)
    {
	GetDataItem(pclNoOfDemands,pcpData,ilItemNo,';',""," \0");
	dbg(TRACE,"ProcessAFL pclNoOfDemands %s",pclNoOfDemands);
	llNoOfDemands = atol(pclNoOfDemands);
    }
    if (llNoOfDemands > 0)
    {

		ilRc = GetFieldLength(cgDemTab,"URNO",&llUrnoLen);
		if (ilRc == RC_SUCCESS)
		{
			llUrnoLen++;
		} /* end of if */
		else
		{
			llUrnoLen = 14;
		} /* end of else */

		lgUrnoListLen = llNoOfDemands * (llUrnoLen + 2) + 1;
  
		pcgUrnoListBuf = (char *) realloc(pcgUrnoListBuf, (lgUrnoListLen + 1));
		if (pcgUrnoListBuf == NULL)
		{
			ilRc = RC_FAIL ;
			dbg (TRACE, "ProcessAFL: realloc failed - pcgUrnoListBuf");
		}
		else
		{		   
			memset(pcgUrnoListBuf,0x00,lgUrnoListLen);
		} /* end of if */

		ilRc = FindItemInList(pcpFields,"DEMANDS",',',&ilItemNo,&ilCol,&ilPos);
		if (ilRc == RC_SUCCESS)
		{
			GetDataItem(pcgUrnoListBuf,pcpData,ilItemNo,';',""," \0");
		}

		dbg(TRACE,"ProcessAFL: Count <%ld> Demand Urnos: <%s>",llNoOfDemands,pcgUrnoListBuf);

		ilItemNo = 1;
		while (GetDataItem(clUrnoSelection,pcgUrnoListBuf,ilItemNo,'|',""," \0"))
		{
				
	/*assure that one demand is only handled one time, because opss_pm sends the same demands too often*/
			ilUrnoCount = get_no_of_items(clUrnoSelection);
			llUrnoRowNum = ARR_FIRST;
			for (ili = 1; ili <= ilUrnoCount; ili++)
			{
			ilRc = get_real_item(clTmpUrno,clUrnoSelection,ili);
			llUrnoRowNum = ARR_FIRST;
			if (AATArrayFindRowPointer(&(rgUrnoArray.rrArrayHandle),
						   &(rgUrnoArray.crArrayName[0]),
						   &(rgUrnoArray.rrIdx01Handle),
						   &(rgUrnoArray.crIdx01Name[0]),
						   clTmpUrno,&llUrnoRowNum,
						   (void *) &pclUrnoRow) != RC_SUCCESS)
			{
				ilRc = AATArrayAddRowPart(&rgUrnoArray.rrArrayHandle,rgUrnoArray.crArrayName,&llUrnoRowNum,"URNO",clTmpUrno);
				strcat(clUrnoSelection2,clTmpUrno);
				strcat(clUrnoSelection2,",");
			}
								
			}
			if (strlen(clUrnoSelection2) != 0)
			{
			clUrnoSelection2[strlen(clUrnoSelection2)-1] = '\0';

	/* end of workaround */
			sprintf(pclSelection,"WHERE URNO IN (%s)",clUrnoSelection2);
			CEDAArrayRefill(&rgRequestOpenDemArray.rrArrayHandle,rgRequestOpenDemArray.crArrayName,pclSelection,pclAddData,llAction);
			}
			llAction = ARR_NEXT;
			ilItemNo++;
				
			clUrnoSelection2[0] = '\0';
			clUrnoSelection[0] = '\0';
		}

	/*********/
		llAction = ARR_FIRST;

		if(CEDAArrayFindRowPointer(&(rgRequestOpenDemArray.rrArrayHandle),
					   &(rgRequestOpenDemArray.crArrayName[0]),
					   &(rgRequestOpenDemArray.rrIdx06Handle),
					   &(rgRequestOpenDemArray.crIdx06Name[0]),
					   "",&llAction,(void *)&pclDemRow) == RC_SUCCESS)
		{
			StrToTime(OPENDEMFIELD(pclDemRow,igDemDebe),&tlLoadStart );
		}

		llAction = ARR_FIRST;
		if(CEDAArrayFindRowPointer(&(rgRequestOpenDemArray.rrArrayHandle),
					   &(rgRequestOpenDemArray.crArrayName[0]),
					   &(rgRequestOpenDemArray.rrIdx07Handle),
					   &(rgRequestOpenDemArray.crIdx07Name[0]),
					   "",&llAction,(void *)&pclDemRow) == RC_SUCCESS)
		{
			StrToTime(OPENDEMFIELD(pclDemRow,igDemDeen),&tlLoadEnd );
		}
	/******/

		ilRc = FindItemInList(pcpFields,"WOALID",',',&ilItemNo,&ilCol,&ilPos);
		if (ilRc == RC_SUCCESS)
		{
			GetDataItem(clTmpBuf,pcpData,ilItemNo,';',""," \0");
			bgAllowEmptyAlid = (atoi(clTmpBuf) == 1)?TRUE:FALSE;
		}
		ilRc = FindItemInList(pcpFields,"IBREAK",',',&ilItemNo,&ilCol,&ilPos);
		if (ilRc == RC_SUCCESS)
		{
			GetDataItem(clTmpBuf,pcpData,ilItemNo,';',""," \0");
			bgIgnoreBreaks = (atoi(clTmpBuf) == 0)?TRUE:FALSE;
		}
		ilRc = FindItemInList(pcpFields,"JEB",',',&ilItemNo,&ilCol,&ilPos);
		if (ilRc == RC_SUCCESS)
		{
			GetDataItem(clTmpBuf,pcpData,ilItemNo,';',""," \0");
			igJobEndBuffer = atoi(clTmpBuf);
		}

		ilRc = FindItemInList(pcpFields,"JMD",',',&ilItemNo,&ilCol,&ilPos);
		if (ilRc == RC_SUCCESS)
		{
			GetDataItem(clTmpBuf,pcpData,ilItemNo,';',""," \0");
			igMinJobDistance = atoi(clTmpBuf);
		}

		ilRc = FindItemInList(pcpFields,"ATYP",',',&ilItemNo,&ilCol,&ilPos);
		if (ilRc == RC_SUCCESS)
		{
			GetDataItem(clAssignTyp,pcpData,ilItemNo,';',""," \0");
			if(strcmp(clAssignTyp,"OPEN") == 0)
			{
			blDeleteJobs = FALSE;
			}
			else
			{
			blDeleteJobs = TRUE;
			}
			if(strcmp(clAssignTyp,"DEL") == 0)
			{
			blDeleteOnly = TRUE;
			}
			else
			{
			blDeleteOnly = FALSE;
			}


		}
		ilRc = FindItemInList(pcpFields,"RESTAP",',',&ilItemNo,&ilCol,&ilPos);
		if (ilRc == RC_SUCCESS)
		{
			GetDataItem(clResourceTab,pcpData,ilItemNo,';',""," \0");
		}

		ilRc = FindItemInList(pcpFields,"URES",',',&ilItemNo,&ilCol,&ilPos);
		if (ilRc == RC_SUCCESS)
		{
			GetDataItem(clPoolUrnos,pcpData,ilItemNo,';',""," \0");
		}

		/*********
			  TimeToStr(clLoadStart,tgLoadStart);
			  TimeToStr(clLoadEnd,tgLoadEnd);
		****/
		TimeToStr(clLoadStart,tlLoadStart);
		TimeToStr(clLoadEnd,tlLoadEnd);

	/*CEDAArrayDelete added by HEB to avoid oracle error while clPoolUrnos is empty*/ 

		if (strcmp(clPoolUrnos,"") == 0)
		{
			CEDAArrayDelete (&(rgRequestPoolJobArray.rrArrayHandle),&(rgRequestPoolJobArray.crArrayName[0]));
		}
		else
		{
			sprintf(clPoolSelection,"WHERE UJTY = '%ld' AND ACTO > '%s' AND ACFR < '%s' AND UAID IN (%s) AND HOPO = '%s'",lgUjtyPol,clLoadStart,clLoadEnd,clPoolUrnos,cgHopo);
			dbg(TRACE,"ProcessAFL clPoolSelection <%s> ",clPoolSelection);

			CEDAArrayRefill(&(rgRequestPoolJobArray.rrArrayHandle),&(rgRequestPoolJobArray.crArrayName[0]),clPoolSelection,pclAddData,ARR_FIRST) ;
		}
		ProcessNewPoolJob("",&rgRequestPoolJobArray,FALSE,FALSE);

		llRowNum = ARR_FIRST;

		while(CEDAArrayFindRowPointer(&(rgRequestOpenDemArray.rrArrayHandle),
						  &(rgRequestOpenDemArray.crArrayName[0]),
						  &(rgRequestOpenDemArray.rrIdx01Handle),
						  &(rgRequestOpenDemArray.crIdx01Name[0]),
						  "",&llRowNum,(void *)&pclDemRow) == RC_SUCCESS)
		{
			llRowNum2 = ARR_LAST;	/* reverse processing of array. Otherwise deletion of 2nd job on one demand can fail */
			blJobIsOk = TRUE;
			blJobFound = FALSE;
			strcpy(clFlgs,OPENDEMFIELD(pclDemRow,igDemFlgs));

			/*  if(clFlgs[1] == '1' || (clFlgs[1] == '5') ) hag20021002 */
			if ( !IsDemandToAssign(clFlgs) )
			{
				CEDAArrayPutField(&(rgRequestOpenDemArray.rrArrayHandle),&(rgRequestOpenDemArray.crArrayName[0]),NULL,"TCHD",
									llRowNum,"-1") ;	
				dbg ( DEBUG, "ProcessAFL: Set TCHD=-1 for UDEM <%s> (IsDemandToAssign returned 0)", OPENDEMFIELD(pclDemRow,igDemUrno) );
			}
			else
			{
				CEDAArrayPutField(&(rgRequestOpenDemArray.rrArrayHandle),&(rgRequestOpenDemArray.crArrayName[0]),NULL,"TCHD",
						  llRowNum,"1") ;	
				dbg ( DEBUG, "ProcessAFL: Set TCHD=1 for UDEM <%s> (IsDemandToAssign returned 1)", OPENDEMFIELD(pclDemRow,igDemUrno) );

				while((CEDAArrayFindRowPointer(&(rgJodArray.rrArrayHandle),
								   &(rgJodArray.crArrayName[0]),
								   &(rgJodArray.rrIdx01Handle),
								   &(rgJodArray.crIdx01Name[0]),
								   OPENDEMFIELD(pclDemRow,igDemUrno),&llRowNum2,
								   (void *) &pclJodRow ) == RC_SUCCESS) /*&& blJobIsOk*/)
				{
							
					llRowNum3 = ARR_FIRST;
					blJobFound = FALSE;		
					if(CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
								   &(rgJobArray.crArrayName[0]),
								   &(rgJobArray.rrIdx02Handle),
								   &(rgJobArray.crIdx02Name[0]),
								   JODFIELD(pclJodRow,igJodUjob),&llRowNum3,
								   (void *) &pclJobRow ) == RC_SUCCESS)  
					{
						blJobFound = TRUE;
						strcpy(clStatus,JOBFIELD(pclJobRow,igJobStat));
						dbg(TRACE,"ProcessAFL DemUrno <%s> clStatus <%s>",OPENDEMFIELD(pclDemRow,igDemUrno),clStatus);
						if((clStatus[0] == 'P' )&& (clStatus[2] != '1' ) && (clStatus[4] != '1' ))
						{
							blJobIsOk = TRUE;
						}
						else
						{
							blJobIsOk = FALSE;
						}
					}
				
					if(blJobFound && blJobIsOk)
					{
						if(blDeleteJobs)
						{
							char clJour[21];
							strcpy(clJour,JOBFIELD(pclJobRow,igJobJour));
							llAction = ARR_FIRST;
							if ( CEDAArrayFindRowPointer(&(rgRequestPoolJobArray.rrArrayHandle),
														 &(rgRequestPoolJobArray.crArrayName[0]),
														 &(rgRequestPoolJobArray.rrIdx01Handle),
														 &(rgRequestPoolJobArray.crIdx01Name[0]),
														 clJour,&llAction, (void *) &pclJodRow ) 
								 == RC_SUCCESS )
							{	/* Job is done by a person of the selected pools */		
								UpdateMinMax ( JOBFIELD(pclJobRow,igJobAcfr), 
											   JOBFIELD(pclJobRow,igJobActo) );
								CEDAArrayDeleteRow(&(rgJobArray.rrArrayHandle),&(rgJobArray.crArrayName[0]),llRowNum3);
								CEDAArrayDeleteRow(&(rgJodArray.rrArrayHandle),&(rgJodArray.crArrayName[0]),llRowNum2);
							}
							else
							{
								CEDAArrayPutField(&(rgRequestOpenDemArray.rrArrayHandle),&(rgRequestOpenDemArray.crArrayName[0]),
												  NULL,"TCHD", llRowNum,"-1") ;	
								dbg ( DEBUG, "ProcessAFL: Set TCHD=-1 for UDEM <%s> (PoolJob not in req. pool)", OPENDEMFIELD(pclDemRow,igDemUrno) );
							}
						}
						else
						{
							if(blJobFound)
							{
								CEDAArrayPutField(&(rgRequestOpenDemArray.rrArrayHandle),&(rgRequestOpenDemArray.crArrayName[0]),NULL,"TCHD",
										  llRowNum,"-1") ;	
								dbg ( DEBUG, "ProcessAFL: Set TCHD=-1 for UDEM <%s> (blDeleteJobs is FALSE)", OPENDEMFIELD(pclDemRow,igDemUrno) );
							}
						}
										
					}
					else
					{
						if ( blJobFound && !blJobIsOk )
						{
							CEDAArrayPutField(&(rgRequestOpenDemArray.rrArrayHandle),&(rgRequestOpenDemArray.crArrayName[0]),NULL,"TCHD",
									  llRowNum,"-1") ;	
							dbg ( DEBUG, "ProcessAFL: Set TCHD=-1 for UDEM <%s> (blJobIsOk is FALSE)", OPENDEMFIELD(pclDemRow,igDemUrno) );
						}
					}
					llRowNum2 = ARR_PREV;
				}	/* new end of while */
			}
			llRowNum = ARR_NEXT;
		}

		/*
		ilRc = CEDAArrayWriteDB(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);
		if (ilRc != RC_SUCCESS)
		{
			int ilOldDebugLevel = debug_level;
			debug_level = TRACE;
			dbg(TRACE,"ProcessAFL Error writing to JOBTAB ");
			debug_level = ilOldDebugLevel;

		}

		ilRc = CEDAArrayWriteDB(&rgJodArray.rrArrayHandle,rgJodArray.crArrayName,NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);
		if (ilRc != RC_SUCCESS)
		{
			int ilOldDebugLevel = debug_level;
			debug_level = TRACE;
			dbg(TRACE,"ProcessAFL Error writing to JODTAB, ilRc :<%d>",ilRc);
			debug_level = ilOldDebugLevel;
		}*/
		ilRc = SaveJobs ( bgSendUpdJob );
		dbg ( TRACE, "ProcessAFL: SaveJobs returns RC <%d>", ilRc );
	
		if(!blDeleteOnly)
		{
			PrepareDemArray(&rgRequestOpenDemArray);
			ProcessAOJ(&rgRequestOpenDemArray,&rgRequestPoolJobArray);
		}

    }


    dbg(TRACE,"ProcessAFL End");

    return(ilRc) ;

} /* ProcessAFL */

/**********************************************************************************************/
/**********************************************************************************************/

static int ProcessAOJ(ARRAYINFO *prpOpenDemArray,ARRAYINFO *prpPoolJobArray)
{
    int	ilRc        = RC_SUCCESS;             /* Return code */


    AssignMAJobs(prpOpenDemArray,prpPoolJobArray);
    return(ilRc) ;

} /* ProcessAOJ */

static int ProcessCJC()
{
    int	ilRc        = RC_FAIL;             /* Return code */
 
    time_t tlPrevEnd,tlPrevStart,tlNextStart,tlNextEnd,tlCurrTime;
    time_t tlPoolEnd,tlPoolStart;
    BOOL blPrevNotSet;
    long llRowNum = ARR_FIRST;
    long llRowNum2 = ARR_FIRST;
    long llRowNum3 = ARR_FIRST;
    long llPrevRow = ARR_FIRST;
    char *pclPoolJobRow = NULL;
    char *pclJobRow = NULL;
    char *pclJodRow = NULL;
    char clStatus[20], clPrevStatus[20];
    long llJtyUrno, llPrevUjty;
    char clCurrTime[20];
    char *pclPrevJobRow = NULL;
	int  ilSolved = RC_FAIL;
 
    BOOL blFirstJob;
    dbg(TRACE,"ProcessCJC START");
 
    GetServerTimeStamp("UTC", 1, 0, clCurrTime);

    StrToTime(clCurrTime,&tlCurrTime);
    tlPrevEnd = tlCurrTime;
    tlPrevStart = tlCurrTime;

    if(bgKlebeFunction)
    {
	ilRc = KlebeFunction(&rgJobArray);
	if(ilRc == RC_SUCCESS)
	{
	    ilRc = CEDAArrayWriteDB(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);
	    if (ilRc != RC_SUCCESS)
	    {
		int ilOldDebugLevel = debug_level;
		debug_level = TRACE;
		dbg(TRACE,"ProcessCJC Error writing to JOBTAB ");
		debug_level = ilOldDebugLevel;

	    }
	}
    }
    if(bgCheckOverlap)
    {
	int ilOldDebugLevel = debug_level;
	if ( debug_level < TRACE )
		debug_level = TRACE;


	ilRc = RC_FAIL;
	while(CEDAArrayGetRowPointer(&(rgPoolJobArray.rrArrayHandle),
				     &(rgPoolJobArray.crArrayName[0]),llRowNum,(void *)&pclPoolJobRow) == RC_SUCCESS)
	{

	    blFirstJob = TRUE;
	    llRowNum2 = ARR_FIRST;

	    dbg(TRACE,"ProcessCJC UPooljob <%s>",POOLJOBFIELD(pclPoolJobRow,igJobUrno));
	
	    dbg(TRACE,"ProcessCJC PoolStart <%s>",POOLJOBFIELD(pclPoolJobRow,igJobAcfr));
	    dbg(TRACE,"ProcessCJC PoolEnd <%s>",POOLJOBFIELD(pclPoolJobRow,igJobActo));

	    StrToTime(POOLJOBFIELD(pclPoolJobRow,igJobAcfr),&tlPoolStart);
	    StrToTime(POOLJOBFIELD(pclPoolJobRow,igJobActo),&tlPoolEnd);

	    dbg(TRACE,"ProcessCJC tlPoolStart <%ld>",tlPoolStart);
	    dbg(TRACE,"ProcessCJC zlPoolEnd <%ld>",tlPoolEnd);


	    while(CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
					  &(rgJobArray.crArrayName[0]),
					  &(rgJobArray.rrIdx01Handle),&(rgJobArray.crIdx01Name[0]),
					  POOLJOBFIELD(pclPoolJobRow,igJobUrno),
					  &llRowNum2,(void *)&pclJobRow) == RC_SUCCESS)		
	    {

			dbg(TRACE,"ProcessCJC Ujob <%s>",JOBFIELD(pclJobRow,igJobUrno));
			dbg(TRACE,"ProcessCJC JobStart <%s>",JOBFIELD(pclJobRow,igJobAcfr));
			dbg(TRACE,"ProcessCJC JobEnd <%s>",JOBFIELD(pclJobRow,igJobActo));

				
			StrToTime(JOBFIELD(pclJobRow,igJobAcfr),&tlNextStart);
			StrToTime(JOBFIELD(pclJobRow,igJobActo),&tlNextEnd);

			dbg(TRACE,"ProcessCJC tlStart <%ld>",tlNextStart);
			dbg(TRACE,"ProcessCJC zlEnd <%ld>",tlNextEnd);

			if(CheckAlocation("",JOBFIELD(pclJobRow,igJobUalo), TRUE))
			{

				blPrevNotSet = FALSE;
				if(tlNextStart > tlCurrTime + igAssignOffSetStart && tlNextStart < tlCurrTime + igAssignOffSetEnd)
				{		
					if(((tlPoolEnd + igOverlapBuffer <= tlNextEnd) || (tlPoolStart - igOverlapBuffer >= tlNextStart)) ||
					   ((tlNextStart < tlPrevEnd - igOverlapBuffer) && !blFirstJob))
					{
						strcpy(clStatus,JOBFIELD(pclJobRow,igJobStat));
						llJtyUrno = atol(JOBFIELD(pclJobRow,igJobUjty));
						ilSolved = RC_FAIL;
						/* if 2n Job is a non-confirmed break, try to move the break */
						if ( (llJtyUrno == lgUjtyBrk) && (clStatus[0] == 'P') && (clStatus[2] != '1') )
							ilSolved = ReassignBreak ( pclPoolJobRow, pclJobRow, llRowNum2 );
						if ( (ilSolved != RC_SUCCESS) && pclPrevJobRow )
						{
							strcpy(clPrevStatus,JOBFIELD(pclPrevJobRow,igJobStat));
							llPrevUjty = atol(JOBFIELD(pclPrevJobRow,igJobUjty));
							/* if 1st Job is a non-confirmed break, try to move the break */
							if ( (llPrevUjty==lgUjtyBrk) && (clPrevStatus[0]=='P') && (clPrevStatus[2]!='1') )
							{
								ilSolved = ReassignBreak ( pclPoolJobRow, pclPrevJobRow, llPrevRow );
								/* index now is placed on previous job (break) */
								llRowNum2 = ARR_NEXT;
								CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
														&(rgJobArray.crArrayName[0]),
														&(rgJobArray.rrIdx01Handle),
														&(rgJobArray.crIdx01Name[0]),
														POOLJOBFIELD(pclPoolJobRow,igJobUrno),
														&llRowNum2,(void *)&pclJobRow) ;	
								/* index now is placed on correct job again */
							}
						}
						if ( ilSolved == RC_SUCCESS )
							ilRc = RC_SUCCESS;
						if( (ilSolved!= RC_SUCCESS) &&
							(clStatus[0] == 'P' )&& (clStatus[2] != '1' ) && (clStatus[4] != '1' )&& (llJtyUrno != lgUjtyBrk && llJtyUrno != lgUjtyDel && llJtyUrno != lgUjtyDet))
						{
							llRowNum3 = ARR_FIRST;
							while(CEDAArrayFindRowPointer(&(rgJodArray.rrArrayHandle),
											  &(rgJodArray.crArrayName[0]),
											  &(rgJodArray.rrIdx02Handle),&(rgJodArray.crIdx02Name[0]),
											  JOBFIELD(pclJobRow,igJobUrno),
											  &llRowNum3,(void *)&pclJodRow) == RC_SUCCESS)
							{
								CEDAArrayPutField(&(rgJodArray.rrArrayHandle),&(rgJodArray.crArrayName[0]),NULL,"TCHX",
										  llRowNum3,"zz") ;	

								llRowNum3 = ARR_NEXT;
							}
										
							CEDAArrayPutField(&(rgJobArray.rrArrayHandle),&(rgJobArray.crArrayName[0]),NULL,"TXXD",
									  llRowNum2,"yy") ;	

							dbg(TRACE,"ProcessCJC Job deleted");

							ilRc = RC_SUCCESS;
							if(!blFirstJob)
							{
								tlNextEnd = tlPrevEnd;
								pclJobRow = pclPrevJobRow;
							}
							else
							{
								blPrevNotSet = TRUE;
							}
									
						}
					}
				}

				if(!blPrevNotSet)
				{
					tlPrevEnd = tlNextEnd;
					blFirstJob = FALSE;
					pclPrevJobRow = pclJobRow ;
					llPrevRow = llRowNum2;
				}
					
			}
			llRowNum2 = ARR_NEXT;
	    }
	    llRowNum = ARR_NEXT;
	}

	llRowNum2 = ARR_FIRST;
	while(CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
				      &(rgJobArray.crArrayName[0]),
				      &(rgJobArray.rrIdx03Handle),&(rgJobArray.crIdx03Name[0]),
				      "yy",
				      &llRowNum2,(void *)&pclJobRow) == RC_SUCCESS)
	{
	    dbg(TRACE,"ProcessCJC DeleteJob 1 URNO <%s>",JOBFIELD(pclJobRow,igJobUrno));

	    CEDAArrayDeleteRow(&(rgJobArray.rrArrayHandle),&(rgJobArray.crArrayName[0]),llRowNum2);
	    llRowNum2 = ARR_FIRST;
	}
	llRowNum2 = ARR_FIRST;
	while(CEDAArrayFindRowPointer(&(rgJodArray.rrArrayHandle),
				      &(rgJodArray.crArrayName[0]),
				      &(rgJodArray.rrIdx03Handle),&(rgJodArray.crIdx03Name[0]),
				      "zz",
				      &llRowNum2,(void *)&pclJodRow) == RC_SUCCESS)
	{
	    CEDAArrayDeleteRow(&(rgJodArray.rrArrayHandle),&(rgJodArray.crArrayName[0]),llRowNum2);
	    llRowNum2 = ARR_FIRST;
	}

	llRowNum = ARR_FIRST;
	while(CEDAArrayGetRowPointer(&(rgPoolJobArray.rrArrayHandle),
				     &(rgPoolJobArray.crArrayName[0]),llRowNum,(void *)&pclPoolJobRow) == RC_SUCCESS)
	{

	    blFirstJob = TRUE;
	    llRowNum2 = ARR_FIRST;
	    dbg(TRACE,"ProcessCJC UPooljob2 <%s>",POOLJOBFIELD(pclPoolJobRow,igJobUrno));

	    dbg(TRACE,"ProcessCJC PoolStart2 <%s>",POOLJOBFIELD(pclPoolJobRow,igJobAcfr));
	    dbg(TRACE,"ProcessCJC PoolEnd2 <%s>",POOLJOBFIELD(pclPoolJobRow,igJobActo));


	    StrToTime(POOLJOBFIELD(pclPoolJobRow,igJobAcfr),&tlPoolStart);
	    StrToTime(POOLJOBFIELD(pclPoolJobRow,igJobActo),&tlPoolEnd);
	
	    dbg(TRACE,"ProcessCJC tlPoolStart2 <%ld>",tlPoolStart);
	    dbg(TRACE,"ProcessCJC zlPoolEnd2 <%ld>",tlPoolEnd);

	    while(CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
					  &(rgJobArray.crArrayName[0]),
					  &(rgJobArray.rrIdx05Handle),&(rgJobArray.crIdx05Name[0]),
					  POOLJOBFIELD(pclPoolJobRow,igJobUrno),
					  &llRowNum2,(void *)&pclJobRow) == RC_SUCCESS)		
	    {

		dbg(TRACE,"ProcessCJC Ujob <%s>",JOBFIELD(pclJobRow,igJobUrno));
		
		dbg(TRACE,"ProcessCJC Start2 <%s>",JOBFIELD(pclJobRow,igJobAcfr));
		dbg(TRACE,"ProcessCJC End2 <%s>",JOBFIELD(pclJobRow,igJobActo));


		StrToTime(JOBFIELD(pclJobRow,igJobAcfr),&tlNextStart);
		StrToTime(JOBFIELD(pclJobRow,igJobActo),&tlNextEnd);

		dbg(TRACE,"ProcessCJC tlStart2 <%ld>",tlNextStart);
		dbg(TRACE,"ProcessCJC zlEnd2 <%ld>",tlNextEnd);
		if(CheckAlocation("",JOBFIELD(pclJobRow,igJobUalo), TRUE))
		{

		    blPrevNotSet = FALSE;
		    if(tlNextStart > tlCurrTime + igAssignOffSetStart && tlNextStart < tlCurrTime + igAssignOffSetEnd)
		    {		
			if(((tlPoolEnd + igOverlapBuffer <= tlNextEnd) || (tlPoolStart - igOverlapBuffer >= tlNextStart)) ||
			   ((tlNextEnd > tlPrevStart - igOverlapBuffer) && !blFirstJob))
			{
			    strcpy(clStatus,JOBFIELD(pclJobRow,igJobStat));
			    llJtyUrno = atol(JOBFIELD(pclJobRow,igJobUjty));
			    if((clStatus[0] == 'P' )&& (clStatus[2] != '1' ) && (clStatus[4 ] != '1' )&& (llJtyUrno != lgUjtyBrk && llJtyUrno != lgUjtyDel && llJtyUrno != lgUjtyDet))
			    {
				llRowNum3 = ARR_FIRST;
				while(CEDAArrayFindRowPointer(&(rgJodArray.rrArrayHandle),
							      &(rgJodArray.crArrayName[0]),
							      &(rgJodArray.rrIdx02Handle),&(rgJodArray.crIdx02Name[0]),
							      JOBFIELD(pclJobRow,igJobUrno),
							      &llRowNum3,(void *)&pclJodRow) == RC_SUCCESS)
				{
				    CEDAArrayPutField(&(rgJodArray.rrArrayHandle),&(rgJodArray.crArrayName[0]),NULL,"TCHX",
						      llRowNum3,"zz") ;	

				    llRowNum3 = ARR_NEXT;
				}
							
				CEDAArrayPutField(&(rgJobArray.rrArrayHandle),&(rgJobArray.crArrayName[0]),NULL,"TXXD",
						  llRowNum2,"yy") ;	

				dbg(TRACE,"ProcessCJC  Job2 deleted");
				ilRc = RC_SUCCESS;
				if(!blFirstJob)
				{
				    tlNextStart = tlPrevStart;
				}
				else
				{
				    blPrevNotSet = TRUE;
				}
							
			    }
			}
		    }

		    if(!blPrevNotSet)
		    {
			tlPrevStart = tlNextStart;
			blFirstJob = FALSE;
		    }
		    llRowNum2 = ARR_NEXT;
		}
	    }
	    llRowNum = ARR_NEXT;
	}

	llRowNum2 = ARR_FIRST;
	while(CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
				      &(rgJobArray.crArrayName[0]),
				      &(rgJobArray.rrIdx03Handle),&(rgJobArray.crIdx03Name[0]),
				      "yy",
				      &llRowNum2,(void *)&pclJobRow) == RC_SUCCESS)
	{
	    dbg(TRACE,"ProcessCJC DeleteJob 2 URNO <%s>",JOBFIELD(pclJobRow,igJobUrno));
	    CEDAArrayDeleteRow(&(rgJobArray.rrArrayHandle),&(rgJobArray.crArrayName[0]),llRowNum2);
	    llRowNum2 = ARR_FIRST;
	}
	llRowNum2 = ARR_FIRST;
	while(CEDAArrayFindRowPointer(&(rgJodArray.rrArrayHandle),
				      &(rgJodArray.crArrayName[0]),
				      &(rgJodArray.rrIdx03Handle),&(rgJodArray.crIdx03Name[0]),
				      "zz",
				      &llRowNum2,(void *)&pclJodRow) == RC_SUCCESS)
	{
	    CEDAArrayDeleteRow(&(rgJodArray.rrArrayHandle),&(rgJodArray.crArrayName[0]),llRowNum2);
	    llRowNum2 = ARR_FIRST;
	}
	if(ilRc == RC_SUCCESS)
	{
	    ilRc = CEDAArrayWriteDB(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);
	    if (ilRc != RC_SUCCESS)
	    {
		int ilOldDebugLevel = debug_level;
		debug_level = TRACE;
		dbg(TRACE,"ProcessCJC Error writing to JOBTAB ");
		debug_level = ilOldDebugLevel;

	    }

	    ilRc = CEDAArrayWriteDB(&rgJodArray.rrArrayHandle,rgJodArray.crArrayName,NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);
	    if (ilRc != RC_SUCCESS)
	    {
		int ilOldDebugLevel = debug_level;
		debug_level = TRACE;
		dbg(TRACE,"ProcessCJC Error writing to JODTAB ");
		debug_level = ilOldDebugLevel;
	    }
	}
	debug_level = ilOldDebugLevel;

    }


    dbg(TRACE,"ProcessCJC END");
    return(ilRc) ;

} /* ProcessCJC */

static int FindMatchingPoolJobs(char *pcpAlid,char *pcpAloc, char *pcpRudUrno,char *pcpTpln, time_t tpDebe,time_t tpDeen,time_t tpLade,long lpTtgt,int ipWtypeTo,
				int ipPrio,ARRAYINFO *prpPoolJobArray,char *pcpIsOk)
{
    int ilRc = RC_FAIL;
    long llRowNum2 = ARR_FIRST;
    long llRowNum3 = ARR_FIRST;
    long llRowNum4 = ARR_FIRST;
    
  
    char *pclDemPoolRow = NULL;
    char *pclPoolJobRow = NULL;
    char *pclParRow = NULL;
    char *pclGroupNameRow = NULL;

    int ilPoolCount = 0;
    int ilFound = 0;
	
    char clPoolJobKey[20];
    time_t tlActEadb, tlActLade;
    BOOL blIsOk = TRUE;
    char clAlid[30];
    char clAlidKey[30];
    char clParKey[200];
		
    char clTmpTime[20];

    char clTmpBegin[20];
    char clTmpEnde[20];

    BOOL blAlidIsValid = TRUE;

    BOOL blUseAllPoolJobs = FALSE;

    strcpy(clAlid,pcpAlid);
    TrimRight(clAlid);

    TimeToStr(clTmpBegin,tpDebe + 2*60*60);
    TimeToStr(clTmpEnde,tpDeen - 2*60*60);

    /* dbg(TRACE,"FindMatchingPoolJobs clAlid <%s>",clAlid);
    dbg(TRACE,"FindMatchingPoolJobs clTmpBegin <%s>",clTmpBegin);
    dbg(TRACE,"FindMatchingPoolJobs clTmpEnde <%s>",clTmpEnde); 
	*/
    if(strlen(clAlid) <= 1)
    {
		if(bgAllowEmptyAlid)
		{
			sprintf(clAlidKey,"dummyfsc,fsc");
			blUseAllPoolJobs = TRUE;
		}
		else
		{
			blAlidIsValid = FALSE;
		}
    }
    else
    {
		sprintf(clAlidKey,"%s,%s",pcpAlid,pcpAloc);
    }

    llRowNum2 = ARR_FIRST;
    while((CEDAArrayFindRowPointer(&(rgDemPoolArray.rrArrayHandle),
				   &(rgDemPoolArray.crArrayName[0]),
				   &(rgDemPoolArray.rrIdx01Handle),
				   &(rgDemPoolArray.crIdx01Name[0]),
				   clAlidKey,&llRowNum2,
				   (void *) &pclDemPoolRow ) == RC_SUCCESS) && blAlidIsValid)
    {
		llRowNum2 = ARR_NEXT;
		ilPoolCount++;
		dbg(TRACE,"FindMatchingPoolJobs pclDemPoolRow <%s>  ilPooJobCount <%d> ",DEMPOOLFIELD(pclDemPoolRow,igDemPoolUpol),ilPoolCount);
		llRowNum3 = ARR_FIRST;
		if(blUseAllPoolJobs || (atol(DEMPOOLFIELD(pclDemPoolRow,igDemPoolUpol)) <= 0))
		{
			strcpy(clPoolJobKey,"");
		}
		else
		{
			strcpy(clPoolJobKey,DEMPOOLFIELD(pclDemPoolRow,igDemPoolUpol));
		}

		llRowNum3 = ARR_FIRST;

		while(CEDAArrayFindRowPointer(&(prpPoolJobArray->rrArrayHandle),
						  &(prpPoolJobArray->crArrayName[0]),
						  &(prpPoolJobArray->rrIdx02Handle),
						  &(prpPoolJobArray->crIdx02Name[0]),
						  clPoolJobKey,&llRowNum3,
						  (void *) &pclPoolJobRow ) == RC_SUCCESS)
		{
			blIsOk = FALSE;
				
				
			llRowNum4 = ARR_FIRST;
			sprintf(clParKey,"ASGR,%s,1",pcpTpln);

			dbg(DEBUG,"FindMatchingPoolJobs clParKey <%s>",clParKey); /* hag 20030715 */

			if((strcmp(clTmpEnde,POOLJOBFIELD(pclPoolJobRow,igJobActo)) <= 0) && (strcmp(clTmpBegin,POOLJOBFIELD(pclPoolJobRow,igJobAcfr)) >= 0))
			{
				blIsOk = TRUE;
				if(CEDAArrayFindRowPointer(&(rgParArray.rrArrayHandle),
							   &(rgParArray.crArrayName[0]),
							   &(rgParArray.rrIdx01Handle),
							   &(rgParArray.crIdx01Name[0]),
							   clParKey,&llRowNum4,
							   (void *) &pclParRow ) == RC_SUCCESS)
				{

					llRowNum4 = ARR_FIRST;
					dbg(DEBUG,"FindMatchingPoolJobs ASGR found ");
					dbg(DEBUG,"FindMatchingPoolJobs Ustf <%s>",POOLJOBFIELD(pclPoolJobRow,igJobUstf));
					if((AATArrayFindRowPointer(&(rgGroupNameArray.rrArrayHandle),
								   &(rgGroupNameArray.crArrayName[0]),
								   &(rgGroupNameArray.rrIdx01Handle),
								   &(rgGroupNameArray.crIdx01Name[0]),
								   POOLJOBFIELD(pclPoolJobRow,igPoolJobWgpc),&llRowNum4,
								   (void *) &pclGroupNameRow ) == RC_SUCCESS) && blIsOk)
					{
						blIsOk = FALSE;
					}
				}
			}
			if(blIsOk)
			{
				if(CheckSinglePoolJob(pclPoolJobRow,pcpRudUrno,pcpAlid,tpDebe,tpDeen, tpLade,
							  lpTtgt, ipWtypeTo, ipPrio))
				{
					dbg(TRACE,"FindMatchingPoolJobs Valid PoolJobUrno <%s>",POOLJOBFIELD(pclPoolJobRow,igJobUrno));
					if(CEDAArrayPutField(&(prpPoolJobArray->rrArrayHandle),&(prpPoolJobArray->crArrayName[0]),NULL,"ISOK",llRowNum3,pcpIsOk) != RC_SUCCESS)
					{
						dbg(TRACE,"FindMatchingPoolJobs PutField ISOK <%s> failed ",pcpIsOk);
					}
					if(CEDAArrayPutField(&(prpPoolJobArray->rrArrayHandle),&(prpPoolJobArray->crArrayName[0]),NULL,"TCHX",llRowNum3,"1") != RC_SUCCESS)
					{
						dbg(TRACE,"FindMatchingPoolJobs PutField TCHD 1 failed ");
					}
					/*
					TimeToStr(clTmpTime,tlActEadb);
					if(CEDAArrayPutField(&(prpPoolJobArray->rrArrayHandle),&(prpPoolJobArray->crArrayName[0]),NULL,"NJBN",llRowNum3,clTmpTime) != RC_SUCCESS)
					{
						dbg(TRACE,"FindMatchingPoolJobs PutField NJBN <%s> failed ",clTmpTime);
					}
					TimeToStr(clTmpTime,tlActLade);
					if(CEDAArrayPutField(&(prpPoolJobArray->rrArrayHandle),&(prpPoolJobArray->crArrayName[0]),NULL,"NJED",llRowNum3,clTmpTime) != RC_SUCCESS)
					{
						dbg(TRACE,"FindMatchingPoolJobs PutField NJED <%s> failed ",clTmpTime);
					}*/
					ilFound ++;
					ilRc = RC_SUCCESS;
				}
			}
			llRowNum3 = ARR_NEXT;
		}
		
    }
	dbg(TRACE,"FindMatchingPoolJobs tpDebe <%ld> tpDeen <%ld> tpLade <%ld> Set %d pooljobs to <%s>",
		tpDebe,tpDeen, tpLade, ilFound, pcpIsOk );
	/*  next lines test */
	TimeToStr(clTmpBegin,tpDebe);
	TimeToStr(clTmpEnde, tpDeen);
	TimeToStr(clTmpTime,tpLade);
	dbg(TRACE,"FindMatchingPoolJobs DEBE <%s DEEN <%s> LADE <%s> Set %d pooljobs to <%s>",
		clTmpBegin,clTmpEnde, clTmpTime, ilFound, pcpIsOk );
	
    return ilRc;
}

static BOOL FindMatchingSinglePoolJob(char *pcpPoolJobRow,char *pcpAlid,char *pcpAloc, char *pcpRudUrno,
				      time_t tpDebe,time_t tpDeen,time_t tpLade,
				      long lpTtgt,int ipWtypeTo,int ipPrio)
{
    BOOL blFound = FALSE;
    BOOL blAlidIsValid = TRUE;

    BOOL blUseAllPoolJobs = FALSE;

    long llRowNum2 = ARR_FIRST;

    char *pclDemPoolRow = NULL;
    char clAlid[20];
    char clAlidKey[40];
    strcpy(clAlid,pcpAlid);
    TrimRight(clAlid);

    dbg(TRACE,"FindMatchingSinglePoolJob clAlid <%s>",clAlid);
	
    if(strlen(clAlid) <= 1)
    {
	if(bgAllowEmptyAlid)
	{
	    blUseAllPoolJobs = TRUE;
	}
	else
	{
	    blAlidIsValid = FALSE;
	}
    }

    if(blAlidIsValid)
    {
		
	dbg(TRACE,"FindMatchingSinglePoolJob clAlidx <%s>",clAlid);
	if(!blUseAllPoolJobs)
	{
	    llRowNum2 = ARR_FIRST;
	    sprintf(clAlidKey,"%s,%s,%s",pcpAlid,pcpAloc,POOLJOBFIELD(pcpPoolJobRow,igJobUaid));
	    if(CEDAArrayFindRowPointer(&(rgDemPoolArray.rrArrayHandle),
				       &(rgDemPoolArray.crArrayName[0]),
				       &(rgDemPoolArray.rrIdx01Handle),
				       &(rgDemPoolArray.crIdx01Name[0]),
				       clAlidKey,&llRowNum2,
				       (void *) &pclDemPoolRow ) == RC_SUCCESS)
	    {
		blFound = TRUE;
	    }
	    else
	    {
		llRowNum2 = ARR_FIRST;
		sprintf(clAlidKey,"%s,%s",pcpAlid,pcpAloc);
		dbg(TRACE,"FindMatchingSinglePoolJob clAlidKey2 <%s>",clAlidKey);
		if(CEDAArrayFindRowPointer(&(rgDemPoolArray.rrArrayHandle),
					   &(rgDemPoolArray.crArrayName[0]),
					   &(rgDemPoolArray.rrIdx01Handle),
					   &(rgDemPoolArray.crIdx01Name[0]),
					   clAlidKey,&llRowNum2,
					   (void *) &pclDemPoolRow ) == RC_SUCCESS)
		{
		    dbg(TRACE,"FindMatchingSinglePoolJob UPOL <%s>",DEMPOOLFIELD(pclDemPoolRow,igDemPoolUpol));

		    if(atol(DEMPOOLFIELD(pclDemPoolRow,igDemPoolUpol)) <= 0)
		    {
			blFound = TRUE;
		    }

		}
	    }

	}
	else
	{
	    blFound = TRUE;
	}

	if(blFound)
	{
	    blFound = CheckSinglePoolJob(pcpPoolJobRow,pcpRudUrno,pcpAlid,tpDebe,tpDeen, tpLade,
					 lpTtgt, ipWtypeTo, ipPrio);
	}

    }

    return blFound;
}

static BOOL CheckSinglePoolJob(char *pcpPoolJobRow, char *pcpRudUrno,char *pcpAlid,time_t tpDebe,time_t tpDeen,time_t tpLade,
			       long lpTtgt,int ipWtypeTo,int ipPrio)
{
	 
    
    long llRowNum4 = ARR_FIRST;
    long llRowNum5 = ARR_FIRST;
    long llRowNum6 = ARR_FIRST;
  
	
    char *pclJobRow = NULL;
    char *pclWayRow = NULL;
    char *pclWgrRow1 = NULL;
    char *pclWgrRow2 = NULL;
	
    time_t tlJobStart, tlJobEnd;
    time_t tlActEadb, tlActLade;
    BOOL blIsOk = TRUE;
    int ilOldTtgf = 0;
    int ilOldTtgt = 0;
    int ilNewTtgt = 0;
    char clWayKey1[60];
    char clWayKey2[60];
    char clAlid[11] = "";
    char clStat[20];
	
    char clEadb[20];
    char clLade[20];
	
    long llDuration;
    long llJtyUrno;
    char clStatus[20];

    if(StrToTime(POOLJOBFIELD(pcpPoolJobRow,igJobAcfr),&tlJobStart) != RC_SUCCESS)
    {
		dbg(TRACE,"CheckSinglePoolJob StrToTime Pool tlJobStart failed <%s>",POOLJOBFIELD(pcpPoolJobRow,igJobAcfr));
    }

    if(StrToTime(POOLJOBFIELD(pcpPoolJobRow,igJobActo),&tlJobEnd) != RC_SUCCESS)
    {
		dbg(TRACE,"CheckSinglePoolJob StrToTime Pool tlJobEnd failed <%s>",POOLJOBFIELD(pcpPoolJobRow,igJobActo));
    }

    dbg(TRACE,"CheckSinglePoolJob URNO <%s> from <%s> till <%s>", POOLJOBFIELD(pcpPoolJobRow,igJobUrno),
		POOLJOBFIELD(pcpPoolJobRow,igJobAcfr),POOLJOBFIELD(pcpPoolJobRow,igJobActo));

    dbg(TRACE,"CheckSinglePoolJob  TCHD <%s> ISOK <%s>",POOLJOBFIELD(pcpPoolJobRow,igPoolJobTchd),
														POOLJOBFIELD(pcpPoolJobRow,igPoolJobIsok));

/*HEB Wegezeit*/ 
    tlJobEnd -= (igJobEndBuffer * 60);
	
    strcpy(clStatus,POOLJOBFIELD(pcpPoolJobRow,igJobStat));
	
    blIsOk = TRUE;
    /* hag 20020822: Handle new pooljob-status "Absent" */
    if(clStatus[0] == 'F' || clStatus[2] == '1' || clStatus[4] == 'A')
    {
		blIsOk = FALSE;
    }
	
    if(blIsOk)
    {
		tlActEadb = max(tpDebe,tlJobStart + lpTtgt);
		tlActLade = min(tpLade,tlJobEnd);
		llDuration = (tpDeen - tpDebe);
		TimeToStr(clEadb,tlActEadb);
		TimeToStr(clLade,tlActLade);

		blIsOk = (tlActEadb <= tlActLade - llDuration);

		dbg(TRACE,"CheckSinglePoolJob  clEadb <%s> clLade <%s> blIsOk <%d>",clEadb, clLade, blIsOk);
		dbg(TRACE,"CheckSinglePoolJob  tlEadb <%ld>  tlLabe <%ld> llDuration <%ld>", tlActEadb, tlActLade, llDuration );
    }

    if(blIsOk)
    {
		if(CheckFunction(POOLJOBFIELD(pcpPoolJobRow,igJobUrno),pcpRudUrno,ipPrio) == FALSE)
		{
			blIsOk = FALSE;
			dbg(TRACE,"CheckSinglePoolJob CheckFunction == FALSE PU<%s>, DU<%s>, Prio<%d>",POOLJOBFIELD(pcpPoolJobRow,igJobUrno),pcpRudUrno,ipPrio);
		}
		else
			if(CheckQualification(POOLJOBFIELD(pcpPoolJobRow,igJobUrno),pcpRudUrno,ipPrio,2) == FALSE)
			{
				blIsOk = FALSE;
				dbg(TRACE,"CheckSinglePoolJob CheckQualification == FALSE PU<%s>, DU<%s>, Prio<%d>",POOLJOBFIELD(pcpPoolJobRow,igJobUrno),pcpRudUrno,ipPrio);
			}

    }
    if(blIsOk)
    {
		llRowNum4 = ARR_FIRST;
			
		dbg(TRACE,"CheckSinglePoolJob JOUR <%s>",POOLJOBFIELD(pcpPoolJobRow,igJobUrno));
		while((CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
						   &(rgJobArray.crArrayName[0]),
						   &(rgJobArray.rrIdx01Handle),
						   &(rgJobArray.crIdx01Name[0]),
						   POOLJOBFIELD(pcpPoolJobRow,igJobUrno),&llRowNum4,
						   (void *) &pclJobRow ) == RC_SUCCESS) && blIsOk )
		{
			dbg(TRACE,"CheckSinglePoolJob Job Found JobUrno <%s>", JOBFIELD(pclJobRow,igJobUrno));
			if(StrToTime(JOBFIELD(pclJobRow,igJobAcfr),&tlJobStart) != RC_SUCCESS)
			{
				dbg(TRACE,"CheckSinglePoolJob StrToTime tlJobStart failed <%s>",JOBFIELD(pclJobRow,igJobAcfr));
			}

			if(StrToTime(JOBFIELD(pclJobRow,igJobActo),&tlJobEnd) != RC_SUCCESS)
			{
				dbg(TRACE,"CheckSinglePoolJob StrToTime tlJobEnd failed <%s>",JOBFIELD(pclJobRow,igJobActo));
			}
				
			ilOldTtgf = atoi(JOBFIELD(pclJobRow,igJobTtgf));
			ilOldTtgt = atoi(JOBFIELD(pclJobRow,igJobTtgt));

			llJtyUrno = atoi(JOBFIELD(pclJobRow,igJobUjty));

			strncpy(clAlid,pcpAlid,10);

			llRowNum5 = ARR_FIRST;
			llRowNum6 = ARR_FIRST;
			sprintf(clWayKey1,"%s,%s,%d",JOBFIELD(pclJobRow,igJobAlid),clAlid,ipWtypeTo);
			sprintf(clWayKey2,"%s,%s,%d",clAlid,JOBFIELD(pclJobRow,igJobAlid),ipWtypeTo);
			dbg(DEBUG,"CheckSinglePoolJob JOBAlid <%s> pcpAlid <%s>",JOBFIELD(pclJobRow,igJobAlid),clAlid);

			if((CEDAArrayFindRowPointer(&(rgWgrArray.rrArrayHandle),
						&(rgWgrArray.crArrayName[0]),
						&(rgWgrArray.rrIdx01Handle),
						&(rgWgrArray.crIdx01Name[0]),
						JOBFIELD(pclJobRow,igJobAlid),&llRowNum5,
						(void *) &pclWgrRow1 ) == RC_SUCCESS))
			{
				if(CEDAArrayFindRowPointer(&(rgWgrArray.rrArrayHandle),
							   &(rgWgrArray.crArrayName[0]),
							   &(rgWgrArray.rrIdx01Handle),
							   &(rgWgrArray.crIdx01Name[0]),
							   clAlid,&llRowNum6,
							   (void *) &pclWgrRow2 ) == RC_SUCCESS)
				{
					sprintf(clWayKey1,"%s,%s,%d",WGRFIELD(pclWgrRow1,igWgrAlgr),
						WGRFIELD(pclWgrRow2,igWgrAlgr),ipWtypeTo);
					sprintf(clWayKey2,"%s,%s,%d",WGRFIELD(pclWgrRow2,igWgrAlgr),
						WGRFIELD(pclWgrRow1,igWgrAlgr),ipWtypeTo);
					dbg(TRACE,"CheckSinglePoolJob WGR found ");
				}
			}
								
			dbg(TRACE,"CheckSinglePoolJob JobStart <%s> JobEnd <%s> ",JOBFIELD(pclJobRow,igJobAcfr),JOBFIELD(pclJobRow,igJobActo));
			dbg(TRACE,"CheckSinglePoolJob tlJobEnd <%ld> ilOldTtgf <%ld>  tlActLade <%ld> llDuration <%ld>",tlJobEnd,ilOldTtgf,tlActLade,llDuration);
			
			if((tlJobEnd - ilOldTtgf  +  igMinJobDistance*60)  <= (tlActLade - llDuration))
			{
				/*  gefundener Job liegt echt vor dem Bedarf */
				llRowNum5 = ARR_FIRST;
				llRowNum6 = ARR_FIRST;
				if(CEDAArrayFindRowPointer(&(rgWayArray.rrArrayHandle),
							   &(rgWayArray.crArrayName[0]),
							   &(rgWayArray.rrIdx01Handle),
							   &(rgWayArray.crIdx01Name[0]),
							   clWayKey1,&llRowNum5,
							   (void *) &pclWayRow ) == RC_SUCCESS)
				{
					ilNewTtgt = atol(WAYFIELD(pclWayRow,igWayTtgo)) * 60;
				}
				else
				{
					if(CEDAArrayFindRowPointer(&(rgWayArray.rrArrayHandle),
								   &(rgWayArray.crArrayName[0]),
								   &(rgWayArray.rrIdx01Handle),
								   &(rgWayArray.crIdx01Name[0]),
								   clWayKey2,&llRowNum6,
								   (void *) &pclWayRow ) == RC_SUCCESS)
					{
						ilNewTtgt = atol(WAYFIELD(pclWayRow,igWayTtgo)) * 60;
					}
					else
					{
						dbg (DEBUG, "CheckSinglePoolJob:  WayKey not found clWayKey1 <%s>",clWayKey1) ;
						ilNewTtgt = lpTtgt;
					}
				}
				if((tlJobEnd - ilOldTtgf +  igMinJobDistance*60 ) > tlActLade - llDuration - ilNewTtgt )
				{
					blIsOk = FALSE;
				}
				else
				{
					tlActEadb = max(tlActEadb,tlJobEnd - (ilOldTtgf - ilNewTtgt - (igMinJobDistance) * 60 ));
				}
				dbg(DEBUG,"CheckSinglePoolJob Line <%d> blIsOk <%d>  ",__LINE__,blIsOk);

			}
			else
			{
						
				llRowNum5 = ARR_FIRST;
				llRowNum6 = ARR_FIRST;

				if(CEDAArrayFindRowPointer(&(rgWayArray.rrArrayHandle),
							   &(rgWayArray.crArrayName[0]),
							   &(rgWayArray.rrIdx01Handle),
							   &(rgWayArray.crIdx01Name[0]),
							   clWayKey1,&llRowNum5,
							   (void *) &pclWayRow ) == RC_SUCCESS)
				{
					ilNewTtgt = atol(WAYFIELD(pclWayRow,igWayTtgo)) * 60;
					dbg (TRACE, "CheckSinglePoolJob:  WayKey found clWayKey1 <%s>",clWayKey1) ;
				}
				else
				{
					if(CEDAArrayFindRowPointer(&(rgWayArray.rrArrayHandle),
								   &(rgWayArray.crArrayName[0]),
								   &(rgWayArray.rrIdx01Handle),
								   &(rgWayArray.crIdx01Name[0]),
								   clWayKey2,&llRowNum6,
								   (void *) &pclWayRow ) == RC_SUCCESS)
					{
						ilNewTtgt = atol(WAYFIELD(pclWayRow,igWayTtgo)) * 60;
						dbg (TRACE, "CheckSinglePoolJob:  WayKey found clWayKey2 <%s>",clWayKey2) ;
					}
					else
					{

						dbg (DEBUG, "CheckSinglePoolJob:  WayKey not found clWayKey1 <%s>",clWayKey1) ;
						ilNewTtgt = ilOldTtgt;
					}				
						
							
				}
				if(tlJobStart + ilOldTtgt - ilNewTtgt - (igMinJobDistance*60)  < tlActEadb + llDuration)
				{
					blIsOk = FALSE;
				}
				else
				{
					tlActLade = tlActEadb + llDuration;
				}
				dbg(DEBUG,"CheckSinglePoolJob Line <%d> blIsOk <%d>  ",__LINE__,blIsOk);
			}

			strcpy(clStat,JOBFIELD(pclJobRow,igJobStat));
			if(clStat[0] == 'F' || (bgIgnoreBreaks && (llJtyUrno == lgUjtyBrk)))
			{
				blIsOk = TRUE;
			}
			dbg(DEBUG,"CheckSinglePoolJob Line <%d> blIsOk <%d>  ",__LINE__,blIsOk);
			dbg(DEBUG,"CheckSinglePoolJob tlActEadb <%ld> tlActLade <%ld>", tlActEadb, tlActLade );
			llRowNum4 = ARR_NEXT;
		}

		dbg(TRACE,"CheckSinglePoolJob Line <%d> blIsOk <%d>  ",__LINE__,blIsOk);
		dbg(TRACE,"CheckSinglePoolJob tlActEadb <%ld> tlActLade <%ld>", tlActEadb, tlActLade );

    }
    return blIsOk;

}

static int CalcOptimalValues( ARRAYINFO *prpPoolJobArray,char *pcpAlid, time_t tpDebe,time_t tpDeen,long lpWayTo,int ipWtypeTo,char *pcpIsOk)
{
    int ilRc = RC_SUCCESS;
    long llRowNum = ARR_FIRST;
    double dlTotalDist = 0;
    double dlShiftDur = 0;
    double dlWorkLoad = 0;
    char clWorkLoad[20];
    char clMinTotal[20];
    char clPrevUrno[20];
    char clNextUrno[20];
    char clWayTo[20];
    char clMin2[20];

    double dlTmpVal;
    double dlSumPrio;

    long llTimeDist = 0;
    long llUrnoPrevJob = 0;
    long llUrnoNextJob = 0;
    long llWayTo = lpWayTo;

    long llMinTimeDistTotal = 100000;
    long llMinTimeDist2 = 100000;
    long llMinWayToGoTo = 100000;

    char clPrio[20];
    char *pclPoolJobRow = NULL;
    time_t tlPoolJobStart,tlPoolJobEnd;
    int	 ilIndDisact; 

    /*  CEDAArrayPutField for field "PRIO" causes resorting of array. Thus not all records
	are considered in loop */
    ilIndDisact = CEDAArrayDisactivateIndex ( &(prpPoolJobArray->rrArrayHandle),	
					      &(prpPoolJobArray->crArrayName[0]),
					      &(prpPoolJobArray->rrIdx04Handle),
					      &(prpPoolJobArray->crIdx04Name[0]));
    dbg ( DEBUG, "CalcOptimalValues: CEDAArrayDisactivateIndex returns <%d>", ilIndDisact );		  		  

    while(CEDAArrayFindRowPointer(&(prpPoolJobArray->rrArrayHandle),
				  &(prpPoolJobArray->crArrayName[0]),
				  &(prpPoolJobArray->rrIdx04Handle),
				  &(prpPoolJobArray->crIdx04Name[0]),
				  pcpIsOk,&llRowNum,
				  (void *) &pclPoolJobRow ) == RC_SUCCESS)
    {
	dbg(TRACE,"CalcOptimalValues pclPoolJobRow <%s> Tchd <%s> pcpIsOk <%s>",POOLJOBFIELD(pclPoolJobRow,igJobUrno),POOLJOBFIELD(pclPoolJobRow,igPoolJobTchd),pcpIsOk);
	if(StrToTime(POOLJOBFIELD(pclPoolJobRow,igJobAcfr),&tlPoolJobStart) != RC_SUCCESS)
	{
	    dbg(TRACE,"CalcOptimalValues StrToTime tlPoolJobStart failed <%s>",POOLJOBFIELD(pclPoolJobRow,igJobAcfr));
	}
	if(StrToTime(POOLJOBFIELD(pclPoolJobRow,igJobActo),&tlPoolJobEnd) != RC_SUCCESS)
	{
	    dbg(TRACE,"CalcOptimalValues StrToTime tlPoolJobEnd failed <%s>",POOLJOBFIELD(pclPoolJobRow,igJobActo));
	}
	dlTotalDist = CalcTimeBetweenJobs(POOLJOBFIELD(pclPoolJobRow,igJobUrno), tlPoolJobStart,tlPoolJobEnd);


	dlShiftDur = tlPoolJobEnd - tlPoolJobStart;
	if(dlShiftDur > 0)
	{
	    dlWorkLoad = ((dlShiftDur - dlTotalDist)/dlShiftDur)*100;
	}
	else
	{
	    dlWorkLoad = 101;
	}

	sprintf(clWorkLoad,"%03.2f",dlWorkLoad);
	sprintf(clMinTotal,"%07.0f",dlTotalDist);

	CalcTimeAndWayDist(POOLJOBFIELD(pclPoolJobRow,igJobUrno),pcpAlid, tlPoolJobStart,tpDebe,
			   &llTimeDist,&llWayTo,ipWtypeTo,&llUrnoPrevJob,&llUrnoNextJob);
		
	sprintf(clMin2,"%07ld",llTimeDist);
	sprintf(clWayTo,"%ld",llWayTo);
	sprintf(clPrevUrno,"%ld",llUrnoPrevJob);
	sprintf(clNextUrno,"%ld",llUrnoNextJob);


	llMinTimeDistTotal = (llMinTimeDistTotal > dlTotalDist)?dlTotalDist:llMinTimeDistTotal;
	llMinTimeDist2 = (llMinTimeDist2 > llTimeDist)?llTimeDist:llMinTimeDist2;
	llMinWayToGoTo = (llMinWayToGoTo > llWayTo)?llWayTo:llMinWayToGoTo;

	if(CEDAArrayPutField(&(prpPoolJobArray->rrArrayHandle),&(prpPoolJobArray->crArrayName[0]),NULL,"WKLD",llRowNum,clWorkLoad) != RC_SUCCESS)
	{
	    dbg(TRACE,"CalcOptimalValues PutField WKLD <%s> failed ",clWorkLoad);
	}
	if(CEDAArrayPutField(&(prpPoolJobArray->rrArrayHandle),&(prpPoolJobArray->crArrayName[0]),NULL,"MINT",llRowNum,clMinTotal) != RC_SUCCESS)
	{
	    dbg(TRACE,"CalcOptimalValues PutField MINT <%s> failed ",clMinTotal);
	}
	if(CEDAArrayPutField(&(prpPoolJobArray->rrArrayHandle),&(prpPoolJobArray->crArrayName[0]),NULL,"MIN2",llRowNum,clMin2) != RC_SUCCESS)
	{
	    dbg(TRACE,"CalcOptimalValues PutField MIN2 <%s> failed ",clMin2);
	}
	if(CEDAArrayPutField(&(prpPoolJobArray->rrArrayHandle),&(prpPoolJobArray->crArrayName[0]),NULL,"WAYT",llRowNum,clWayTo) != RC_SUCCESS)
	{
	    dbg(TRACE,"CalcOptimalValues PutField WAYT <%s> failed ",clWayTo);
	}
	if(CEDAArrayPutField(&(prpPoolJobArray->rrArrayHandle),&(prpPoolJobArray->crArrayName[0]),NULL,"UNXJ",llRowNum,clNextUrno) != RC_SUCCESS)
	{
	    dbg(TRACE,"CalcOptimalValues PutField UNXJ <%s> failed ",clNextUrno);
	}
	if(CEDAArrayPutField(&(prpPoolJobArray->rrArrayHandle),&(prpPoolJobArray->crArrayName[0]),NULL,"UPRJ",llRowNum,clPrevUrno) != RC_SUCCESS)
	{
	    dbg(TRACE,"CalcOptimalValues PutField UPRJ <%s> failed ",clPrevUrno);
	}


	llRowNum = ARR_NEXT;
    }

    if(llMinTimeDistTotal < 1)
    {
	llMinTimeDistTotal = 1;
    }
    if(llMinTimeDist2 < 1)
    {
	llMinTimeDist2 = 1;
    }
    if(llMinWayToGoTo < 1)
    {
	llMinWayToGoTo = 1;
    }


    llRowNum = ARR_FIRST;
    while(CEDAArrayFindRowPointer(&(prpPoolJobArray->rrArrayHandle),
				  &(prpPoolJobArray->crArrayName[0]),
				  &(prpPoolJobArray->rrIdx04Handle),
				  &(prpPoolJobArray->crIdx04Name[0]),
				  pcpIsOk,&llRowNum,
				  (void *) &pclPoolJobRow ) == RC_SUCCESS)
    {
	dlSumPrio = 0;
	dlWorkLoad = atof(POOLJOBFIELD(pclPoolJobRow,igPoolJobWkld));
	dlTotalDist = atof(POOLJOBFIELD(pclPoolJobRow,igPoolJobMint));
	llTimeDist = atol(POOLJOBFIELD(pclPoolJobRow,igPoolJobMin2));
	llWayTo = atol(POOLJOBFIELD(pclPoolJobRow,igPoolJobWayt));

	dlSumPrio += EvalMaxFunction(dlWorkLoad, igWorkloadX,igWorkloadY);

	dbg(TRACE,"CalcOptimalValues  dlSumPrio <%15.2f> WorkLoad <%f> ",dlSumPrio,dlWorkLoad);

	dlTmpVal = ((dlTotalDist - llMinTimeDistTotal)/llMinTimeDistTotal) * 100;
	if (dlTmpVal < 0)
	{
	    dlTmpVal = 0;
	}
	dlSumPrio += EvalMinFunction(dlTmpVal, igMinSumX,igMinSumY);

	dbg(TRACE,"CalcOptimalValues  dlSumPrio <%15.2f> TotalDist <%f> ",dlSumPrio,dlTmpVal);


		
	dlTmpVal = ((llTimeDist - llMinTimeDist2)/llMinTimeDist2) * 100;
	dbg(TRACE,"CalcOptimalValues  dlTmpVal <%15.2f> llTimeDist <%ld> llMinTimeDist2 <%ld>",dlTmpVal,llTimeDist,llMinTimeDist2);
	if (dlTmpVal < 0)
	{
	    dlTmpVal = 0;
	}
	dlSumPrio += EvalMaxFunction(dlTmpVal, igMinTimeX,igMinTimeY);

	dbg(TRACE,"CalcOptimalValues  dlSumPrio <%15.2f> MinTime <%f> ",dlSumPrio,dlTmpVal);

		



	dlTmpVal = ((llWayTo - llMinWayToGoTo)/llMinWayToGoTo) * 100;
	if (dlTmpVal < 0)
	{
	    dlTmpVal = 0;
	}
	dlSumPrio += EvalMaxFunction(dlTmpVal, igMinWayX,igMinWayY);

	dbg(TRACE,"CalcOptimalValues  dlSumPrio <%15.2f> MinWay <%f> PoolJob <%s>",dlSumPrio,dlTmpVal,
	    POOLJOBFIELD(pclPoolJobRow,igJobUrno) );


	sprintf(clPrio,"%015.2f",dlSumPrio);
	
	if(CEDAArrayPutField(&(prpPoolJobArray->rrArrayHandle),&(prpPoolJobArray->crArrayName[0]),NULL,"PRIO",llRowNum,clPrio) != RC_SUCCESS)
	{
	    dbg(TRACE,"CalcOptimalValues PutField clPrio <%s> failed ",clPrio);
	}

	llRowNum = ARR_NEXT;
    }

    /*  Activate index again to get the correct sorting of pooljobs outside */
    ilIndDisact = CEDAArrayActivateIndex ( &(prpPoolJobArray->rrArrayHandle),	
					   &(prpPoolJobArray->crArrayName[0]),
					   &(prpPoolJobArray->rrIdx04Handle),
					   &(prpPoolJobArray->crIdx04Name[0]));
    dbg ( DEBUG, "CalcOptimalValues: CEDAArrayActivateIndex returns <%d>", ilIndDisact );		  		  
    return ilRc;
}



static int AssignMAJobs(ARRAYINFO *prpOpenDemArray, ARRAYINFO *prpPoolJobArray)
{
    int ilRc = RC_SUCCESS;
    long llRowNum = ARR_FIRST;
    long llRowNum1 = ARR_FIRST;
    long llRowNum2 = ARR_FIRST;
    long llRowNum3 = ARR_FIRST;
    long llRowNum5 = ARR_FIRST;
    long llRowNum6 = ARR_FIRST;
    long llRowNum7 = ARR_FIRST;
    long llRowNum8 = ARR_FIRST;
    long llNext = ARR_NEXT;
    char *pclDemRow = NULL;
    char *pclDemPoolRow = NULL;
    char *pclPoolJobRow = NULL;
    char *pclJobRow = NULL;
    char *pclWayRow = NULL;
    char *pclWgrRow1 = NULL;
    char *pclWgrRow2 = NULL;
    char *pclAftRow = NULL;
    char *pclJobTypeRow = NULL;
    char *pclJtyRow = NULL;
    char *pclKeyUrnoListRow = NULL;
    long llMoveOffSet;
    int ilIndex = 1;
    time_t tlDebe,tlDeen,tlLade,tlJobStart,tlJobEnd;
    long llTtgf, llTtgt;
	
    long llJobCount = 0;

    char clKey[50];
    char clWayKey1[60];
    char clWayKey2[60];
    int ilNewTtgt;
    int ilOldTtgt;
    char clJobStart[20];
    char clJobEnd[20];
    char clNewTtgt[20];
    char clAlidKey[30];
    char clIndex[10];

    BOOL blJobCreated = FALSE;
	
    char clUjtyFlt[20];
    char clNow[20];
    char clUrno[20];
	
    char clAlid[20] = "";
    char clJTKey[100];
    char clTmpKey[10];
    char clGroupKey[100];
    char clTmpAloc[40];
    char clTmpObty[40];
	char clUsec[33];

    long llRowCount;
	
    long llUlnk;
    long llOuro;
    long llOuri;
    long llUpde;
    long llUnde;

    long llMaxd;
    long llMind;
	
    long llRealTtgt;
    /*time_t tlTmpStart;
      time_t tlTmpEnd;*/

    int ilDety = -1;
    BOOL blJtySet = FALSE;
	BOOL blSplittedJobsPossible = TRUE;

	sprintf ( clUsec, "JOBHDL/%s", cgDestName );
    for(ilIndex = 1; ilIndex <=igMaxFunction; ilIndex++)
    {
	llRowNum = ARR_FIRST;
	sprintf(clKey,"%d",ilIndex);

	dbg(DEBUG,"AssignMAJobs ilIndex <%d>",ilIndex);
	while(CEDAArrayFindRowPointer(&(prpOpenDemArray->rrArrayHandle),
				      &(prpOpenDemArray->crArrayName[0]),
				      &(prpOpenDemArray->rrIdx03Handle),
				      &(prpOpenDemArray->crIdx03Name[0]),
				      clKey,&llRowNum,
				      (void *) &pclDemRow ) == RC_SUCCESS)
	{

	    sprintf(clIndex,"%d",(ilIndex+1));
	    CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),&(prpOpenDemArray->crArrayName[0]),NULL,"TCHD",llRowNum,clIndex);

			
			
	    dbg(DEBUG,"AssignMAJobs DemAlid <%s>",OPENDEMFIELD(pclDemRow,igDemAlid));
	    if(StrToTime(OPENDEMFIELD(pclDemRow,igDemDebe),&tlDebe) != RC_SUCCESS)
	    {
			dbg(TRACE,"AssignMAJobs StrToTime tlDebe failed <%s>",OPENDEMFIELD(pclDemRow,igDemDebe));
	    }

	    if(StrToTime(OPENDEMFIELD(pclDemRow,igDemDeen),&tlDeen) != RC_SUCCESS)
	    {
			dbg(TRACE,"AssignMAJobs StrToTime tlDeen failed <%s>",OPENDEMFIELD(pclDemRow,igDemDeen));
	    }
	    if(StrToTime(OPENDEMFIELD(pclDemRow,igDemLade),&tlLade) != RC_SUCCESS)
	    {
			dbg(TRACE,"AssignMAJobs StrToTime tlLade failed <%s>",OPENDEMFIELD(pclDemRow,igDemLade));
			tlLade = (time_t)0;
			dbg(TRACE,"AssignMAJobs tlLade : <%d>",tlLade);
	    }
			
	    llTtgf = atol(OPENDEMFIELD(pclDemRow,igDemTtgf));
	    llTtgt = atol(OPENDEMFIELD(pclDemRow,igDemTtgt));

	    dbg(DEBUG,"AssignMAJobs llTtgf <%ld> llTtgt <%ld>",llTtgf,llTtgt);


	    /*tlDeen -= llTtgf;*/ /* HEB*/ 
	    tlDebe += llTtgt;
	    if(tlLade > tlDeen)
	    {
			llMoveOffSet = tlLade - tlDeen;
	    }
	    else
	    {
			llMoveOffSet = 0;
	    }

	    llUlnk = atol(OPENDEMFIELD(pclDemRow,igDemUlnk));
	    llOuro = atol(OPENDEMFIELD(pclDemRow,igDemOuro));
	    llOuri = atol(OPENDEMFIELD(pclDemRow,igDemOuri));
	    llUpde = atol(OPENDEMFIELD(pclDemRow,igDemUpde));
	    llUnde = atol(OPENDEMFIELD(pclDemRow,igDemUnde));

	    llRowNum = ARR_FIRST;

		/* discativate indices containing ISOK to prevent unnecessary resorting */
		CEDAArrayDisactivateIndex ( &(prpPoolJobArray->rrArrayHandle),	
									&(prpPoolJobArray->crArrayName[0]),
									&(prpPoolJobArray->rrIdx04Handle),
									&(prpPoolJobArray->crIdx04Name[0]));
		CEDAArrayDisactivateIndex ( &(prpPoolJobArray->rrArrayHandle),	
									&(prpPoolJobArray->crArrayName[0]),
									&(prpPoolJobArray->rrIdx06Handle),
									&(prpPoolJobArray->crIdx06Name[0]));

	    llRowNum5 = ARR_FIRST;
	    sprintf(clTmpKey,"");

	    while(CEDAArrayFindRowPointer(&(prpPoolJobArray->rrArrayHandle),
					  &(prpPoolJobArray->crArrayName[0]),
					  &(prpPoolJobArray->rrIdx01Handle),
					  &(prpPoolJobArray->crIdx01Name[0]),
					  clTmpKey,&llRowNum5,
					  (void *) &pclPoolJobRow ) == RC_SUCCESS)
	    {
			ilRc = CEDAArrayPutField(&(prpPoolJobArray->rrArrayHandle),&(prpPoolJobArray->crArrayName[0]),NULL,"ISOK",llRowNum5," ");
			CEDAArrayPutField(&(prpPoolJobArray->rrArrayHandle),&(prpPoolJobArray->crArrayName[0]),NULL,"TCHX",llRowNum5,"0");
			llRowNum5 = ARR_NEXT;
	    }

		CEDAArrayActivateIndex (&(prpPoolJobArray->rrArrayHandle),	
								&(prpPoolJobArray->crArrayName[0]),
								&(prpPoolJobArray->rrIdx04Handle),
								&(prpPoolJobArray->crIdx04Name[0]));
		CEDAArrayActivateIndex (&(prpPoolJobArray->rrArrayHandle),	
								&(prpPoolJobArray->crArrayName[0]),
								&(prpPoolJobArray->rrIdx06Handle),
								&(prpPoolJobArray->crIdx06Name[0]));

	    if(llUlnk > 0 && (llOuro > 0 || llOuri > 0))
	    {
			
			sprintf(clGroupKey,"%s,%s,%s,%s",OPENDEMFIELD(pclDemRow,igDemUrue),OPENDEMFIELD(pclDemRow,igDemOuri),
				OPENDEMFIELD(pclDemRow,igDemOuro),OPENDEMFIELD(pclDemRow,igDemUlnk));
			CreateGroupMAJobs(prpOpenDemArray,prpPoolJobArray,clGroupKey);
			blSplittedJobsPossible = FALSE;
	    }
	    else if ((llUpde > 0 || llUnde > 0) && (llOuro > 0 || llOuri > 0))
	    {
			CreateCombinedMaJobs(prpOpenDemArray,prpPoolJobArray,
						 OPENDEMFIELD(pclDemRow,igDemUrno),OPENDEMFIELD(pclDemRow,igDemUrue),
						 OPENDEMFIELD(pclDemRow,igDemOuri),OPENDEMFIELD(pclDemRow,igDemOuro),
						 OPENDEMFIELD(pclDemRow,igDemUpde),OPENDEMFIELD(pclDemRow,igDemUnde),ilIndex);
			blSplittedJobsPossible = FALSE;
	    }
	    else
	    {
			if(atoi(OPENDEMFIELD(pclDemRow,igDemDide)) == 0)
			{
				llMaxd = 0;
				llMind = 0;
			}
			else
			{
				llMaxd = atol(OPENDEMFIELD(pclDemRow,igDemMaxd));
				llMind = atol(OPENDEMFIELD(pclDemRow,igDemMind));
			}
			CreateSplittedMAJobs(prpPoolJobArray,
						 OPENDEMFIELD(pclDemRow,igDemUrno),OPENDEMFIELD(pclDemRow,igDemUrud),
						 OPENDEMFIELD(pclDemRow,igDemTpln),
						 OPENDEMFIELD(pclDemRow,igDemAlid),
						 OPENDEMFIELD(pclDemRow,igDemAloc),llTtgt,
						 atoi(OPENDEMFIELD(pclDemRow,igDemRtwt)),ilIndex,
						 tlDebe,tlDeen,llMoveOffSet,llMind,llMaxd,&llRealTtgt);
			blSplittedJobsPossible = TRUE;
	    }
			
	    blJobCreated = FALSE;
	    llRowNum3 = ARR_FIRST;
	    CEDAArrayGetRowCount(&(rgKeyUrnoListArray.rrArrayHandle),&(rgKeyUrnoListArray.crArrayName[0]),&llRowCount);
	    dbg(TRACE,"AssignMAJobs rgKeyUrnoListArray Rowcount  <%d>",llRowCount);
	    llJobCount = 0;
	    while(AATArrayFindRowPointer(&(rgKeyUrnoListArray.rrArrayHandle),
					 &(rgKeyUrnoListArray.crArrayName[0]),
					 &(rgKeyUrnoListArray.rrIdx03Handle),
					 &(rgKeyUrnoListArray.crIdx03Name[0]),
					 "",&llRowNum3,
					 (void *) &pclKeyUrnoListRow) == RC_SUCCESS)
	    {
		llJobCount++;
				
		llRowNum3 = ARR_NEXT;
		llRowNum5 = ARR_FIRST;
		dbg(DEBUG ,"AssignMAJobs igKeyUrnoListUrno <%s>",KEYURNOLISTFIELD(pclKeyUrnoListRow,igKeyUrnoListUrno));
		if(CEDAArrayFindRowPointer(&(prpPoolJobArray->rrArrayHandle),
					   &(prpPoolJobArray->crArrayName[0]),
					   &(prpPoolJobArray->rrIdx01Handle),
					   &(prpPoolJobArray->crIdx01Name[0]),
					   KEYURNOLISTFIELD(pclKeyUrnoListRow,igKeyUrnoListUrno),&llRowNum5,
					   (void *) &pclPoolJobRow ) == RC_SUCCESS)
		{
		    /*  InitializeNewJobRow(cgNewJobBuf);  hag 04.08.2003: all members will be overwritten later anyhow */
		    dbg(TRACE,"AssignMAJobs Hurra endlich ein neuer Job ");

		    llRowNum8 = ARR_FIRST;
		    if(CEDAArrayFindRowPointer(&(prpOpenDemArray->rrArrayHandle),
					       &(prpOpenDemArray->crArrayName[0]),
					       &(prpOpenDemArray->rrIdx01Handle),
					       &(prpOpenDemArray->crIdx01Name[0]),
					       KEYURNOLISTFIELD(pclKeyUrnoListRow,igKeyUrnoListUdem),&llRowNum8,
					       (void *) &pclDemRow ) == RC_SUCCESS)
					
		    {
			llRowNum2 = ARR_FIRST;
						
			strncpy(clAlid,OPENDEMFIELD(pclDemRow,igDemAlid),10);
			if(CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
						   &(rgJobArray.crArrayName[0]),
						   &(rgJobArray.rrIdx02Handle),
						   &(rgJobArray.crIdx02Name[0]),
						   POOLJOBFIELD(pclPoolJobRow,igPoolJobUprj),&llRowNum2,
						   (void *) &pclJobRow ) == RC_SUCCESS)
			{
			    ilOldTtgt = atoi(JOBFIELD(pclJobRow,igJobTtgf));
							
			    if(ilOldTtgt > 0)
			    {
				CEDAArrayPutField(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,"TTGF",llRowNum2,"0");
				if(StrToTime(JOBFIELD(pclJobRow,igJobActo),&tlJobEnd) != RC_SUCCESS)
				{
				    dbg(TRACE,"AssignMAJobs StrToTime tlJobEnd failed <%s>",JOBFIELD(pclJobRow,igJobAcfr));
				}
				tlJobEnd -= ilOldTtgt ;
				TimeToStr(clJobEnd,tlJobEnd);
				CEDAArrayPutField(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,"ACTO",llRowNum2,clJobEnd);
				if(StrToTime(JOBFIELD(pclJobRow,igJobPlto),&tlJobEnd) != RC_SUCCESS)
				{
				    dbg(TRACE,"AssignMAJobs StrToTime tlJobEnd failed <%s>",JOBFIELD(pclJobRow,igJobPlto));
				}
				tlJobEnd -= ilOldTtgt ;
				TimeToStr(clJobEnd,tlJobEnd);
				CEDAArrayPutField(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,"PLTO",llRowNum2,clJobEnd);
			    }

			}
			llRowNum2 = ARR_FIRST;
						

			dbg (TRACE, "AssignMAJobs:  clAlid <%s> DemAlid <%s>",clAlid,OPENDEMFIELD(pclDemRow,igDemAlid)) ;
			if(CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
						   &(rgJobArray.crArrayName[0]),
						   &(rgJobArray.rrIdx02Handle),
						   &(rgJobArray.crIdx02Name[0]),
						   POOLJOBFIELD(pclPoolJobRow,igPoolJobUnxj),&llRowNum2,
						   (void *) &pclJobRow ) == RC_SUCCESS)
			{
/*
  StrToTime(JOBFIELD(pclJobRow,igJobAcfr),tlTmpStart);
  StrToTime(KEYURNOLISTFIELD(pclKeyUrnoListRow,igKeyUrnoListEnde),tlTmpEnd);

  if(tlTmpStart - tlTmpEnd < 15*60)
  {*/
			    sprintf(clWayKey1,"%s,%s,%s",JOBFIELD(pclJobRow,igJobAlid),clAlid,
				    OPENDEMFIELD(pclDemRow,igDemRtwt));
			    sprintf(clWayKey2,"%s,%s,%s",clAlid,JOBFIELD(pclJobRow,igJobAlid),
				    OPENDEMFIELD(pclDemRow,igDemRtwt));
								
								

			    llRowNum7 = ARR_FIRST;
			    llRowNum6 = ARR_FIRST;
			    if(CEDAArrayFindRowPointer(&(rgWgrArray.rrArrayHandle),
						       &(rgWgrArray.crArrayName[0]),
						       &(rgWgrArray.rrIdx01Handle),
						       &(rgWgrArray.crIdx01Name[0]),
						       JOBFIELD(pclJobRow,igJobAlid),&llRowNum7,
						       (void *) &pclWgrRow1 ) == RC_SUCCESS)
			    {
				if(CEDAArrayFindRowPointer(&(rgWgrArray.rrArrayHandle),
							   &(rgWgrArray.crArrayName[0]),
							   &(rgWgrArray.rrIdx01Handle),
							   &(rgWgrArray.crIdx01Name[0]),
							   clAlid,&llRowNum6,
							   (void *) &pclWgrRow2 ) == RC_SUCCESS)
				{
				    sprintf(clWayKey1,"%s,%s,%s",WGRFIELD(pclWgrRow1,igWgrAlgr),
					    WGRFIELD(pclWgrRow2,igWgrAlgr),OPENDEMFIELD(pclDemRow,igDemRtwt));
				    sprintf(clWayKey2,"%s,%s,%s",WGRFIELD(pclWgrRow2,igWgrAlgr),
					    WGRFIELD(pclWgrRow1,igWgrAlgr),OPENDEMFIELD(pclDemRow,igDemRtwt));
				}
			    }

			    ilOldTtgt = atoi(JOBFIELD(pclJobRow,igJobTtgt));

								

			    llRowNum1 = ARR_FIRST;
			    llRowNum7 = ARR_FIRST;
			    if(CEDAArrayFindRowPointer(&(rgWayArray.rrArrayHandle),
						       &(rgWayArray.crArrayName[0]),
						       &(rgWayArray.rrIdx01Handle),
						       &(rgWayArray.crIdx01Name[0]),
						       clWayKey1,&llRowNum1,
						       (void *) &pclWayRow ) == RC_SUCCESS)
			    {
				ilNewTtgt = atol(WAYFIELD(pclWayRow,igWayTtgo)) * 60;
				dbg (TRACE, "AssignMAJobs:  WayKey found clWayKey1 <%s> ilNewTtgt <%d>",clWayKey1,ilNewTtgt) ;
			    }
			    else
			    {
				if(CEDAArrayFindRowPointer(&(rgWayArray.rrArrayHandle),
							   &(rgWayArray.crArrayName[0]),
							   &(rgWayArray.rrIdx01Handle),
							   &(rgWayArray.crIdx01Name[0]),
							   clWayKey2,&llRowNum7,
							   (void *) &pclWayRow ) == RC_SUCCESS)
				{	
				    ilNewTtgt = atol(WAYFIELD(pclWayRow,igWayTtgo)) * 60;
				    dbg (TRACE, "AssignMAJobs:  WayKey found clWayKey1 <%s> ilNewTtgt <%d>",clWayKey1,ilNewTtgt) ;
				}
				else
				{
				    dbg (TRACE, "AssignMAJobs:  WayKey not found clWayKey <%s>",clWayKey1) ;

				    ilNewTtgt = ilOldTtgt;
				}
			    }

			    if(StrToTime(JOBFIELD(pclJobRow,igJobAcfr),&tlJobStart) != RC_SUCCESS)
			    {
				dbg(TRACE,"AssignMAJobs StrToTime tlJobStart failed <%s>",JOBFIELD(pclJobRow,igJobAcfr));
			    }
			    tlJobStart += ilOldTtgt - ilNewTtgt;

			    TimeToStr(clJobStart,tlJobStart);
			    CEDAArrayPutField(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,"ACFR",llRowNum2,clJobStart);

			    if(StrToTime(JOBFIELD(pclJobRow,igJobPlfr),&tlJobStart) != RC_SUCCESS)
			    {
				dbg(TRACE,"AssignMAJobs StrToTime tlJobStart failed <%s>",JOBFIELD(pclJobRow,igJobAcfr));
			    }
			    tlJobStart += ilOldTtgt - ilNewTtgt;

			    TimeToStr(clJobStart,tlJobStart);
			    CEDAArrayPutField(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,"PLFR",llRowNum2,clJobStart);



			    sprintf(clNewTtgt,"%d",ilNewTtgt);
								
			    CEDAArrayPutField(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,"TTGT",llRowNum2,clNewTtgt);
								
			    /*}*/
			}
			/* GetNextValues(clUrno,1);  */
			GetNextUrno(clUrno);

			TimeToStr(clNow,time(NULL));

			memset(cgNewJobBuf,' ',rgJobArray.lrArrayRowLen);

			if(CEDAArrayAddRow(&(rgJobArray.rrArrayHandle),&(rgJobArray.crArrayName[0]),&llNext,(void *)cgNewJobBuf) == RC_SUCCESS)
			{
		
			    CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"URNO",llNext,clUrno);
			    CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"CDAT",llNext,clNow);
			    CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"HOPO",llNext,cgHopo);
			    CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"STAT",llNext,"P 0");
			    CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"USEC",llNext, clUsec );

			    CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"PLFR",llNext,KEYURNOLISTFIELD(pclKeyUrnoListRow,igKeyUrnoListBegi));
			    CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"ACFR",llNext,KEYURNOLISTFIELD(pclKeyUrnoListRow,igKeyUrnoListBegi));
			    CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"PLTO",llNext,KEYURNOLISTFIELD(pclKeyUrnoListRow,igKeyUrnoListEnde));
			    CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"ACTO",llNext,KEYURNOLISTFIELD(pclKeyUrnoListRow,igKeyUrnoListEnde));
				UpdateMinMax ( KEYURNOLISTFIELD(pclKeyUrnoListRow,igKeyUrnoListBegi), 
							   KEYURNOLISTFIELD(pclKeyUrnoListRow,igKeyUrnoListEnde) );

			    CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"DETY",llNext,OPENDEMFIELD(pclDemRow,igDemDety));
			    CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"TTGT",llNext,POOLJOBFIELD(pclPoolJobRow,igPoolJobWayt));
			    CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"TTGF",llNext,POOLJOBFIELD(pclPoolJobRow,igPoolJobWayf));
			    CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"JOUR",llNext,POOLJOBFIELD(pclPoolJobRow,igJobUrno));
				if ( igJobUtpl > 0 )
					CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"UTPL",llNext,OPENDEMFIELD(pclDemRow,igDemUtpl));
							
			    ilRc = CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"ALID",llNext,clAlid);
														
			    llRowNum6 = ARR_FIRST;
			    sprintf(clAlidKey,"%s,%s",OPENDEMFIELD(pclDemRow,igDemAlid),OPENDEMFIELD(pclDemRow,igDemAloc));

			    if(CEDAArrayFindRowPointer(&(rgDemPoolArray.rrArrayHandle),
						       &(rgDemPoolArray.crArrayName[0]),
						       &(rgDemPoolArray.rrIdx01Handle),
						       &(rgDemPoolArray.crIdx01Name[0]),
						       clAlidKey,&llRowNum6,
						       (void *) &pclDemPoolRow ) == RC_SUCCESS)
			    {
				CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"UAID",llNext,DEMPOOLFIELD(pclDemPoolRow,igDemPoolUaid));
			    }
			    CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"UALO",llNext,OPENDEMFIELD(pclDemRow,igDemUalo));
			    CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"UDEL",llNext,POOLJOBFIELD(pclPoolJobRow,igJobUdel));
			    CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"UDRD",llNext,POOLJOBFIELD(pclPoolJobRow,igJobUdrd));
			    CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"UDSR",llNext,POOLJOBFIELD(pclPoolJobRow,igJobUdsr));
			    sprintf(clUjtyFlt,"%ld",lgUjtyFlt);
			    CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"UJTY",llNext,clUjtyFlt);
			    CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"USTF",llNext,POOLJOBFIELD(pclPoolJobRow,igJobUstf));

			    if(strncmp(OPENDEMFIELD(pclDemRow,igDemAloc),"GAT",3) == 0)
			    {
				CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"GATE",llNext,OPENDEMFIELD(pclDemRow,igDemAlid));
			    }
			    if(strncmp(OPENDEMFIELD(pclDemRow,igDemAloc),"PST",3) == 0)	/*hag20020324%*/
			    { 
				CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"POSI",llNext,OPENDEMFIELD(pclDemRow,igDemAlid));
			    }
			    if(strncmp(OPENDEMFIELD(pclDemRow,igDemAloc),"REGN",4) == 0)
			    {
				CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"REGN",llNext,OPENDEMFIELD(pclDemRow,igDemAlid));
			    }

			    CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"UAFT",llNext,"0");

			    ilDety = atoi(OPENDEMFIELD(pclDemRow,igDemDety));	
			    blJtySet = FALSE;
			    llRowNum6 = ARR_FIRST;
			    dbg (TRACE, "AssignMAJobs:  Dety <%s> ilDety <%d>",OPENDEMFIELD(pclDemRow,igDemDety),ilDety) ;
							
			    if(ilDety == 0)
			    {
				blJtySet = TRUE;
				CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"UAFT",llNext,OPENDEMFIELD(pclDemRow,igDemOuro));
				if(CEDAArrayFindRowPointer(&(rgAftArray.rrArrayHandle),
							   &(rgAftArray.crArrayName[0]),
							   &(rgAftArray.rrIdx01Handle),
							   &(rgAftArray.crIdx01Name[0]),
							   OPENDEMFIELD(pclDemRow,igDemOuro),&llRowNum6,
							   (void *) &pclAftRow ) == RC_SUCCESS)
				{
				    dbg (TRACE, "AssignMAJobs:  Outbound ") ;
				    CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"UAFT",llNext,OPENDEMFIELD(pclDemRow,igDemOuro));
				    CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"ACT3",llNext,AFTFIELD(pclAftRow,igAftAct3));
				    /*  hag20020324: fill GATE, REGN, POSI from afttab, if demand allocation is a different one */
				    if(strncmp(OPENDEMFIELD(pclDemRow,igDemAloc),"GAT",3) != 0)
					CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"GATE",llNext,
							  AFTFIELD(pclAftRow,igAftGtd1));
				    if(strncmp(OPENDEMFIELD(pclDemRow,igDemAloc),"PST",3) != 0)
					CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"POSI",llNext,
							  AFTFIELD(pclAftRow,igAftPstd));
				    if(strncmp(OPENDEMFIELD(pclDemRow,igDemAloc),"REGN",4) != 0)
					CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"REGN",llNext,
							  AFTFIELD(pclAftRow,igAftRegn));
				    /*end hag20020324*/
				}
				else 
				{
				    llRowNum6 = ARR_FIRST;
				    if(CEDAArrayFindRowPointer(&(rgAftArray.rrArrayHandle),
							       &(rgAftArray.crArrayName[0]),
							       &(rgAftArray.rrIdx01Handle),
							       &(rgAftArray.crIdx01Name[0]),
							       OPENDEMFIELD(pclDemRow,igDemOuri),&llRowNum6,
							       (void *) &pclAftRow ) == RC_SUCCESS)
				    {
					dbg (TRACE, "AssignMAJobs:  Inbound ") ;
					CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"UAFT",llNext,OPENDEMFIELD(pclDemRow,igDemOuri));
					CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"ACT3",llNext,AFTFIELD(pclAftRow,igAftAct3));					
					/*  hag20020324: fill GATE, REGN, POSI from afttab, if demand allocation is a different one */
					if(strncmp(OPENDEMFIELD(pclDemRow,igDemAloc),"GAT",3) != 0)
					    CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"GATE",llNext,
							      AFTFIELD(pclAftRow,igAftGta1));
					if(strncmp(OPENDEMFIELD(pclDemRow,igDemAloc),"PST",3) != 0)
					    CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"POSI",llNext,
							      AFTFIELD(pclAftRow,igAftPsta));
					if(strncmp(OPENDEMFIELD(pclDemRow,igDemAloc),"REGN",4) != 0)
					    CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"REGN",llNext,
							      AFTFIELD(pclAftRow,igAftRegn));
					/*end hag20020324*/
				    }
				}
			    }
			    if(ilDety == 1)
			    {
				blJtySet = TRUE;

				llRowNum6 = ARR_FIRST;
				CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"UAFT",llNext,OPENDEMFIELD(pclDemRow,igDemOuri));
				if(CEDAArrayFindRowPointer(&(rgAftArray.rrArrayHandle),
							   &(rgAftArray.crArrayName[0]),
							   &(rgAftArray.rrIdx01Handle),
							   &(rgAftArray.crIdx01Name[0]),
							   OPENDEMFIELD(pclDemRow,igDemOuri),&llRowNum6,
							   (void *) &pclAftRow ) == RC_SUCCESS)
				{
				    dbg (TRACE, "AssignMAJobs: Single Inbound ") ;
				    CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"UAFT",llNext,OPENDEMFIELD(pclDemRow,igDemOuri));
				    CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"ACT3",llNext,AFTFIELD(pclAftRow,igAftAct3));					
				    /*  hag20020324: fill GATE, REGN, POSI from afttab, if demand allocation is a different one */
				    if(strncmp(OPENDEMFIELD(pclDemRow,igDemAloc),"GAT",3) != 0)
					CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"GATE",llNext,
							  AFTFIELD(pclAftRow,igAftGta1));
				    if(strncmp(OPENDEMFIELD(pclDemRow,igDemAloc),"PST",3) != 0)
					CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"POSI",llNext,
							  AFTFIELD(pclAftRow,igAftPsta));
				    if(strncmp(OPENDEMFIELD(pclDemRow,igDemAloc),"REGN",4) != 0)
					CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"REGN",llNext,
							  AFTFIELD(pclAftRow,igAftRegn));
				    /*end hag20020324*/
				}
			    }
			    if(ilDety == 2)
			    {
				blJtySet = TRUE;
				CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"UAFT",llNext,OPENDEMFIELD(pclDemRow,igDemOuro));
				llRowNum6 = ARR_FIRST;
				if(CEDAArrayFindRowPointer(&(rgAftArray.rrArrayHandle),
							   &(rgAftArray.crArrayName[0]),
							   &(rgAftArray.rrIdx01Handle),
							   &(rgAftArray.crIdx01Name[0]),
							   OPENDEMFIELD(pclDemRow,igDemOuro),&llRowNum6,
							   (void *) &pclAftRow ) == RC_SUCCESS)
				{
				    dbg (TRACE, "AssignMAJobs: Single Outbound ") ;
				    CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"UAFT",llNext,OPENDEMFIELD(pclDemRow,igDemOuro));
				    CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"ACT3",llNext,AFTFIELD(pclAftRow,igAftAct3));					
				    /*  hag20020324: fill GATE, REGN, POSI from afttab, if demand allocation is a different one */
				    if(strncmp(OPENDEMFIELD(pclDemRow,igDemAloc),"GAT",3) != 0)
					CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"GATE",llNext,
							  AFTFIELD(pclAftRow,igAftGtd1));
				    if(strncmp(OPENDEMFIELD(pclDemRow,igDemAloc),"PST",3) != 0)
					CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"POSI",llNext,
							  AFTFIELD(pclAftRow,igAftPstd));
				    if(strncmp(OPENDEMFIELD(pclDemRow,igDemAloc),"REGN",4) != 0)
					CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"REGN",llNext,
							  AFTFIELD(pclAftRow,igAftRegn));
				    /*end hag20020324*/
				}
			    }

			    if(!blJtySet)
			    {

				dbg (TRACE, "AssignMAJobs: OBTY == <%s> DemUrno = <%s>",OPENDEMFIELD(pclDemRow,igDemObty),OPENDEMFIELD(pclDemRow,igDemUrno)) ;
				llRowNum6 = ARR_FIRST;
				strcpy(clTmpAloc,OPENDEMFIELD(pclDemRow,igDemAloc));
				TrimRight(clTmpAloc);
				strcpy(clTmpObty,OPENDEMFIELD(pclDemRow,igDemObty));
				TrimRight(clTmpObty);
								
				sprintf(clJTKey,"%s,%s",clTmpAloc,clTmpObty);
				if(CEDAArrayFindRowPointer(&(rgJobTypeArray.rrArrayHandle),
							   &(rgJobTypeArray.crArrayName[0]),
							   &(rgJobTypeArray.rrIdx01Handle),
							   &(rgJobTypeArray.crIdx01Name[0]),
							   clJTKey,&llRowNum6,
							   (void *) &pclJobTypeRow ) == RC_SUCCESS)
				{
				    llRowNum6 = ARR_FIRST;
				    if(CEDAArrayFindRowPointer(&(rgJtyArray.rrArrayHandle),
							       &(rgJtyArray.crArrayName[0]),
							       &(rgJtyArray.rrIdx01Handle),
							       &(rgJtyArray.crIdx01Name[0]),
							       JOBTYPEFIELD(pclJobTypeRow,igJobTypeJtyp),&llRowNum6,
							       (void *) &pclJtyRow ) == RC_SUCCESS)
				    {
					CEDAArrayPutField(&(rgJobArray.rrArrayHandle),rgJobArray.crArrayName,NULL,"UJTY",llNext,JTYFIELD(pclJtyRow,igJtyUrno));	
				    }
				}
			    }
			    /* InitializeNewJodRow(cgNewJodBuf); hag 04.08.2003: all members will be overwritten later anyhow */
			    CEDAArrayAddRow(&(rgJodArray.rrArrayHandle),&(rgJodArray.crArrayName[0]),&llNext,(void *)cgNewJodBuf);
																					
			    TimeToStr(clNow,time(NULL));

			    memset(cgNewJodBuf,' ',rgJodArray.lrArrayRowLen);

			    CEDAArrayPutField(&(rgJodArray.rrArrayHandle),rgJodArray.crArrayName,NULL,"CDAT",llNext,clNow);					
			    CEDAArrayPutField(&(rgJodArray.rrArrayHandle),rgJodArray.crArrayName,NULL,"HOPO",llNext,cgHopo);					
			    CEDAArrayPutField(&(rgJodArray.rrArrayHandle),rgJodArray.crArrayName,NULL,"LSTU",llNext,clNow);					
			    CEDAArrayPutField(&(rgJodArray.rrArrayHandle),rgJodArray.crArrayName,NULL,"UJOB",llNext,clUrno);					
			    CEDAArrayPutField(&(rgJodArray.rrArrayHandle),rgJodArray.crArrayName,NULL,"USEC",llNext,clUsec);		
			    /* GetNextValues(clUrno,1);  */
				GetNextUrno(clUrno);

			    CEDAArrayPutField(&(rgJodArray.rrArrayHandle),rgJodArray.crArrayName,NULL,"URNO",llNext,clUrno);		
			    CEDAArrayPutField(&(rgJodArray.rrArrayHandle),rgJodArray.crArrayName,NULL,"UDEM",llNext,OPENDEMFIELD(pclDemRow,igDemUrno));		
			    CEDAArrayPutField(&(rgJodArray.rrArrayHandle),rgJodArray.crArrayName,NULL,"ACFR",llNext,KEYURNOLISTFIELD(pclKeyUrnoListRow,igKeyUrnoListBegi));		
			    CEDAArrayPutField(&(rgJodArray.rrArrayHandle),rgJodArray.crArrayName,NULL,"ACTO",llNext,KEYURNOLISTFIELD(pclKeyUrnoListRow,igKeyUrnoListEnde));		
							
			}
					
			if( (llJobCount == llRowCount) || !blSplittedJobsPossible )
			{
			    CEDAArrayDeleteRow(&(prpOpenDemArray->rrArrayHandle),&(prpOpenDemArray->crArrayName[0]),llRowNum8);
			}
					
		    }
		    blJobCreated = TRUE;
		}
				
	    }

	    if(blJobCreated)
	    {
		CEDAArrayDelete(&(rgKeyUrnoListArray.rrArrayHandle),&(rgKeyUrnoListArray.crArrayName[0]));

	    }

	}
    }
    /*************
    ilRc = CEDAArrayWriteDB(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);
    if (ilRc != RC_SUCCESS)
    {
	int ilOldDebugLevel = debug_level;
	debug_level = TRACE;
	dbg(TRACE,"AssignMAJobs Error writing to JOBTAB ");
	debug_level = ilOldDebugLevel;
    }

    ilRc = CEDAArrayWriteDB(&rgJodArray.rrArrayHandle,rgJodArray.crArrayName,NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);
    if (ilRc != RC_SUCCESS)
    {
	int ilOldDebugLevel = debug_level;
	debug_level = TRACE;
	dbg(TRACE,"AssignMAJobs Error writing to JODTAB ");
	debug_level = ilOldDebugLevel;
    }
	*/
	ilRc = SaveJobs ( bgSendUpdJob );
	dbg ( TRACE, "AssignMAJobs: SaveJobs returns RC <%d>", ilRc );
    CEDAArrayWriteDB(&(prpPoolJobArray->rrArrayHandle),
		     &(prpPoolJobArray->crArrayName[0]),NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);

    /******/


	
    return(ilRc) ;

}

#if 0
static int CreateSplittedMAJobs(ARRAYINFO *prpPoolJobArray,
				char *pcpDemUrno,char *pcpRudUrno,char *pcpTpln,char *pcpAlid,char *pcpAloc,long lpTtgt,int ipRtwt,int ipIndex,
				time_t tpDebe,time_t tpDeen,long lpMoveOffSet,long lpMind,long lpMaxd,long *plpRealTtgt)
{
    int ilRc = RC_SUCCESS;
    long llRowNum = ARR_FIRST;
    long llRowNum1 = ARR_FIRST;
    
    char *pclPoolJobRow = NULL;
    char *pclJobRow = NULL;
    char *pclKeyUrnoListRow = NULL;
    int ilKeyCount = 0;
    BOOL blOptimalFound = FALSE;
    time_t tlSplittedDebe = tpDebe;
    time_t tlSplittedDeen = tpDeen;
    time_t tlTmpDeen = tpDeen;
    time_t tlTmpLatestDeen = tpDeen;
	
    char pclDemStart[20];
    char pclDemEnd[20];
    char clKey[10];
    char clUrnoKey[50];
    char clTmpKey[50];

    int ilMinSplit = 1;

    double dlDuration = 0;
    long llMaxd = lpMaxd;
    long llMind = lpMind;
    BOOL blJobsAreValid = TRUE;


    if(lpMaxd <= 0 && lpMind <= 0)
    {
		llMaxd = tpDeen - tpDebe;
		llMind = tpDeen - tpDebe;
    }


    dbg(TRACE,"CreateSplittedMAJobs llMaxd <%ld> llMind <%ld>",llMaxd,llMind);


    sprintf(clTmpKey,"%s",pcpDemUrno);

    tlSplittedDebe = tpDebe;

    tlSplittedDeen = tpDebe + llMind;

    tlTmpLatestDeen = min ( tlTmpLatestDeen, tpDebe + llMaxd );
	
    llRowNum1 = ARR_FIRST;

    while(AATArrayFindRowPointer(&(rgKeyUrnoListArray.rrArrayHandle),
				 &(rgKeyUrnoListArray.crArrayName[0]),
				 &(rgKeyUrnoListArray.rrIdx01Handle),
				 &(rgKeyUrnoListArray.crIdx01Name[0]),
				 clTmpKey,&llRowNum1,
				 (void *) &pclKeyUrnoListRow ) == RC_SUCCESS)
    {
		AATArrayDeleteRow(&(rgKeyUrnoListArray.rrArrayHandle),&(rgKeyUrnoListArray.crArrayName[0]),llRowNum1);
		llRowNum1 = ARR_NEXT;
    }

    dbg(TRACE,"Before FindMatchingPoolJobs 1 pcpDemUrno <%s>",pcpDemUrno);
    if( FindMatchingPoolJobs(pcpAlid,pcpAloc,pcpRudUrno,pcpTpln, tlSplittedDebe, tlSplittedDeen,tlSplittedDeen ,lpTtgt,ipRtwt,ipIndex,prpPoolJobArray,"x1") == RC_SUCCESS)
    {
		CalcOptimalValues( prpPoolJobArray,pcpAlid, tlSplittedDebe, tlSplittedDeen,lpTtgt,ipRtwt,"x1");
			
		ilKeyCount = 1;
		blOptimalFound = FALSE;
		while(!blOptimalFound)
		{
			sprintf(clKey,"x%d",ilKeyCount);
			sprintf(clUrnoKey,"%s,x%d",pcpDemUrno,ilKeyCount);
			llRowNum = ARR_FIRST;

			dbg(DEBUG,"CreateSplittedMAJobs: Looking for Pooljob with key <%s> ",clKey );
			if(CEDAArrayFindRowPointer(&(prpPoolJobArray->rrArrayHandle),
						   &(prpPoolJobArray->crArrayName[0]),
						   &(prpPoolJobArray->rrIdx04Handle),
						   &(prpPoolJobArray->crIdx04Name[0]),
						   clKey,&llRowNum,
						   (void *) &pclPoolJobRow ) == RC_SUCCESS)
			{
				llRowNum1 = ARR_FIRST;

				CEDAArrayPutField(&(prpPoolJobArray->rrArrayHandle),
						  &(prpPoolJobArray->crArrayName[0]),NULL,"ISOK",llRowNum,"y") ;
				ilKeyCount++;
				dbg(DEBUG,"POOLJOBFIELD(pclPoolJobRow,igPoolJobUnxj): <%s> ",POOLJOBFIELD(pclPoolJobRow,igPoolJobUnxj));
				if((atoi(POOLJOBFIELD(pclPoolJobRow,igPoolJobUnxj)) != 0) && 
				   (CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
								&(rgJobArray.crArrayName[0]),
								&(rgJobArray.rrIdx02Handle),
								&(rgJobArray.crIdx02Name[0]),
								POOLJOBFIELD(pclPoolJobRow,igPoolJobUnxj),&llRowNum1,
								(void *) &pclJobRow ) == RC_SUCCESS))
				{
					dbg(DEBUG,"%05d: JOBFIELD(pclJobRow,igJobUrno): <%s>,JOBFIELD(pclJobRow,igJobJour : <%s>",__LINE__,JOBFIELD(pclJobRow,igJobUrno),JOBFIELD(pclJobRow,igJobJour)); 
					StrToTime(JOBFIELD(pclJobRow,igJobAcfr),&tlTmpDeen);
					dbg(DEBUG,"CreateSplittedMAJobs: tlTmpDeen <%ld> after job <%s>", tlTmpDeen, JOBFIELD(pclJobRow,igJobUrno) );				
				}
				else
				{
					StrToTime(JOBFIELD(pclPoolJobRow,igJobActo),&tlTmpDeen);
					tlTmpDeen -= igJobEndBuffer*60;
					dbg(DEBUG,"CreateSplittedMAJobs: tlTmpDeen <%ld> after pooljob <%s>", tlTmpDeen, JOBFIELD(pclPoolJobRow,igJobUrno) );				
				}
						
				tlTmpDeen = min(tlTmpDeen,tlTmpLatestDeen );
				dlDuration = tpDeen - tlTmpDeen;

				dbg(TRACE,"CreateSplittedMAJobs llDuration (Rest of Demand) <%f>",dlDuration);

				if(dlDuration > 0)
				{
					ilMinSplit = ceil(dlDuration/llMaxd);
					tlTmpDeen = min(tlTmpDeen,tpDeen - ilMinSplit*llMind );
					dbg(TRACE,"CreateSplittedMAJobs ilMinSplit<%d>",ilMinSplit);
				}

				/* blJobsAreValid = FALSE; */
				if(tlTmpDeen == tpDeen)
				{
					blOptimalFound = TRUE;
					/* blJobsAreValid = TRUE; */
				}
				*plpRealTtgt = atol(POOLJOBFIELD(pclPoolJobRow,igPoolJobWayt));
				TimeToStr(pclDemStart,tlSplittedDebe - atol(POOLJOBFIELD(pclPoolJobRow,igPoolJobWayt)));
				tlSplittedDebe = tlTmpDeen;
				TimeToStr(pclDemEnd,tlSplittedDebe);

				if ( strcmp ( pclDemStart, pclDemEnd ) < 0 )
					blJobsAreValid = TRUE;
				else
					blJobsAreValid = FALSE;

				tlSplittedDeen = tlSplittedDebe + llMind;
				tlTmpLatestDeen = min(tlSplittedDebe + llMaxd,tpDeen);

				if ( tlSplittedDeen >= tlTmpLatestDeen )
				{
					dbg ( TRACE, "CreateSplittedMAJobs: break loop no split possible tlSplittedDeen <%ld>  tlTmpLatestDeen <%ld>",
						  tlSplittedDeen, tlTmpLatestDeen );
					blOptimalFound = TRUE;
				}

				dbg ( TRACE, "CreateSplittedMAJobs: tlSplittedDebe <%ld> tlSplittedDeen <%ld> tlTmpLatestDeen <%ld>",
					  tlSplittedDebe,tlSplittedDeen, tlTmpLatestDeen );

				llRowNum1 = ARR_FIRST;
						
				if(AATArrayFindRowPointer(&(rgKeyUrnoListArray.rrArrayHandle),
							  &(rgKeyUrnoListArray.crArrayName[0]),
							  &(rgKeyUrnoListArray.rrIdx01Handle),
							  &(rgKeyUrnoListArray.crIdx01Name[0]),
							  clUrnoKey,&llRowNum1,
							  (void *) &pclKeyUrnoListRow ) == RC_SUCCESS)
				{
					AATArrayPutField(&(rgKeyUrnoListArray.rrArrayHandle),
							 &(rgKeyUrnoListArray.crArrayName[0]),NULL,"URNO",llRowNum1,TRUE,
							 POOLJOBFIELD(pclPoolJobRow,igJobUrno)) ;
					AATArrayPutField(&(rgKeyUrnoListArray.rrArrayHandle),
							 &(rgKeyUrnoListArray.crArrayName[0]),NULL,"BEGI",llRowNum1,TRUE,
							 pclDemStart) ;
					AATArrayPutField(&(rgKeyUrnoListArray.rrArrayHandle),
							 &(rgKeyUrnoListArray.crArrayName[0]),NULL,"ENDE",llRowNum1,TRUE,
							 pclDemEnd) ;
					dbg(TRACE,"CreateSplittedMAJobs Modified clUrnoKey<%s>",clUrnoKey);
				}
				else
				{
					dbg(TRACE,"CreateSplittedMAJobs Created clUrnoKey<%s>",clUrnoKey);
					llRowNum1 = ARR_FIRST;
					InitializeKeyUrnoListRow(cgKeyUrnoListBuf);
							
					AATArrayAddRow(&(rgKeyUrnoListArray.rrArrayHandle),&(rgKeyUrnoListArray.crArrayName[0]),
						   &llRowNum1,(void *)cgKeyUrnoListBuf);

					AATArrayPutField(&(rgKeyUrnoListArray.rrArrayHandle),
							 &(rgKeyUrnoListArray.crArrayName[0]),NULL,"URNO",llRowNum1,TRUE,
							 POOLJOBFIELD(pclPoolJobRow,igJobUrno)) ;
					AATArrayPutField(&(rgKeyUrnoListArray.rrArrayHandle),
							 &(rgKeyUrnoListArray.crArrayName[0]),NULL,"BEGI",llRowNum1,TRUE,
							 pclDemStart) ;
					AATArrayPutField(&(rgKeyUrnoListArray.rrArrayHandle),
							 &(rgKeyUrnoListArray.crArrayName[0]),NULL,"ENDE",llRowNum1,TRUE,
							 pclDemEnd) ;
					AATArrayPutField(&(rgKeyUrnoListArray.rrArrayHandle),
							 &(rgKeyUrnoListArray.crArrayName[0]),NULL,"UDEM",llRowNum1,TRUE,
							 pcpDemUrno) ;
					AATArrayPutField(&(rgKeyUrnoListArray.rrArrayHandle),
							 &(rgKeyUrnoListArray.crArrayName[0]),NULL,"XKEY",llRowNum1,TRUE,
							 clKey) ;
				
				}
				dbg ( TRACE, "CreateSplittedMAJobs: Set job BEGI <%s> ENDE <%s> UDEM <%s> UPJB <%s>",
					  pclDemStart, pclDemEnd, pcpDemUrno, POOLJOBFIELD(pclPoolJobRow,igJobUrno) );
				if(!blOptimalFound)
				{
					sprintf(clKey,"x%d",ilKeyCount);

					dbg(TRACE,"CreateSplittedMAJobs: Before FindMatchingPoolJobs 2: clKey<%s> pcpDemUrno <%s>",clKey, pcpDemUrno);

					if (FindMatchingPoolJobs(pcpAlid,pcpAloc, pcpRudUrno, pcpTpln,tlSplittedDebe, tlSplittedDeen,tlSplittedDeen,lpTtgt,ipRtwt,ipIndex,prpPoolJobArray,clKey) == RC_SUCCESS)
					{
						CalcOptimalValues( prpPoolJobArray,pcpAlid, tlSplittedDebe, tlSplittedDeen,lpTtgt,ipRtwt,clKey);
					}
				}
					
			}
			else
			{
				ilKeyCount--;
				if(ilKeyCount <= 0)
				{
					blOptimalFound = TRUE;
				}
				dbg ( TRACE, "CreateSplittedMAJobs: Decreased ilKeyCount to <%d>",
					  ilKeyCount );
			}

		}
    }
    if(!blJobsAreValid)
    {
	CEDAArrayDelete(&(rgKeyUrnoListArray.rrArrayHandle),&(rgKeyUrnoListArray.crArrayName[0]));
    }

    return ilRc;
}
#endif 

static int CreateGroupMAJobs(ARRAYINFO *prpOpenDemArray,ARRAYINFO *prpPoolJobArray,
			     char *pcpGroupKey)
{
    int ilRc = RC_SUCCESS;
    long llRowNum = ARR_FIRST;
    long llRowNum1 = ARR_FIRST;
    long llRowNum2 = ARR_FIRST;
    long llRowNum3 = ARR_FIRST;
    long llRowNum4 = ARR_FIRST;
    long llRowNum5 = ARR_FIRST;
    long llRowNum6 = ARR_FIRST;
    int  ilDemandGroupSize = 0;
    int  ilGroupSize = 0;
    char *pclDemRow = NULL;
    char *pclPoolJobRow = NULL;
    char *pclPoolPerRow = NULL;
    char *pclSwgIndexRow = NULL;
    char *pclGroupListRow = NULL;
    char *pclGroupListRow2 = NULL;
    char *pclMatchRow = NULL;
    char *pclDemPrqRow = NULL;
    char *pclGroupNameRow = NULL;
    long llMinStart = 0;
    long llMaxEnd = 0;
    char clSize[5];
    time_t tlDebe,tlDeen,tlLade;
    long llTtgt,llTtgf;
    BOOL blUdgrQuali = TRUE;
    char clKey[30];
    char clKey2[50];
    char pclAlid[30];
    long llRtwt = 1;
    char pclDeen[20];
    char pclDebe[20];
    BOOL blIsValid = TRUE;
    int ilMatchingCount = 0, ilPrio;

    BOOL blMatching = FALSE;
    dbg(TRACE,"CreateGroupMAJobs pcpGroupKey <%s>",pcpGroupKey);
    while(CEDAArrayFindRowPointer(&(prpOpenDemArray->rrArrayHandle),
				  &(prpOpenDemArray->crArrayName[0]),
				  &(prpOpenDemArray->rrIdx04Handle),
				  &(prpOpenDemArray->crIdx04Name[0]),
				  pcpGroupKey,&llRowNum,
				  (void *) &pclDemRow ) == RC_SUCCESS)
    {
		CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),&(prpOpenDemArray->crArrayName[0]),NULL,"TCHD",llRowNum,"9");
		if(ilDemandGroupSize == 0)
		{
			llMinStart = atol(OPENDEMFIELD(pclDemRow,igDemDebe));
			llMaxEnd = atol(OPENDEMFIELD(pclDemRow,igDemDeen));
		}
		llMinStart = min(llMinStart,atol(OPENDEMFIELD(pclDemRow,igDemDebe)));
		llMaxEnd = max(llMaxEnd,atol(OPENDEMFIELD(pclDemRow,igDemDeen)));

		ilDemandGroupSize++;
		llRowNum = ARR_NEXT;
    }

    dbg(TRACE,"CreateGroupMAJobs ilDemandGroupSize <%d>",ilDemandGroupSize);

    llRowNum = ARR_FIRST;
    CEDAArrayDelete(&(rgSwgIndexArray.rrArrayHandle),&(rgSwgIndexArray.crArrayName[0]));
    while(AATArrayGetRowPointer(&(rgGroupNameArray.rrArrayHandle),
				&(rgGroupNameArray.crArrayName[0]),
				llRowNum,(void *) &pclGroupNameRow ) == RC_SUCCESS)
														
    {
		llRowNum2 = ARR_FIRST;
		ilGroupSize = 0;
		while(CEDAArrayFindRowPointer(&(prpPoolJobArray->rrArrayHandle),
						  &(prpPoolJobArray->crArrayName[0]),
						  &(prpPoolJobArray->rrIdx06Handle),
						  &(prpPoolJobArray->crIdx06Name[0]),
						  GROUPNAMEFIELD(pclGroupNameRow,igGroupNameWgpc),&llRowNum2,
						  (void *) &pclPoolJobRow ) == RC_SUCCESS)
		{
			if(atol(POOLJOBFIELD(pclPoolJobRow,igJobActo)) > llMinStart && atol(POOLJOBFIELD(pclPoolJobRow,igJobAcfr)) < llMaxEnd)
			{
				ilGroupSize++;
				CEDAArrayPutField(&(prpPoolJobArray->rrArrayHandle),&(prpPoolJobArray->crArrayName[0]),NULL,"ISOK",llRowNum2,"gr");
			}
			llRowNum2 = ARR_NEXT;
		}
		llRowNum2 = ARR_FIRST;

		sprintf(clSize,"%d",ilGroupSize);

		InitializeSwgIndexRow(cgSwgIndexBuf);

		AATArrayAddRow(&(rgSwgIndexArray.rrArrayHandle),&(rgSwgIndexArray.crArrayName[0]),
				   &llRowNum2,(void *)cgSwgIndexBuf);

		AATArrayPutField(&(rgSwgIndexArray.rrArrayHandle),
				 &(rgSwgIndexArray.crArrayName[0]),NULL,"CODE",llRowNum2,TRUE,
				 GROUPNAMEFIELD(pclGroupNameRow,igGroupNameWgpc)) ;
		AATArrayPutField(&(rgSwgIndexArray.rrArrayHandle),
				 &(rgSwgIndexArray.crArrayName[0]),NULL,"SIZE",llRowNum2,TRUE,
				 clSize);
		llRowNum = ARR_NEXT;

    }



    llRowNum2 = ARR_FIRST;
    sprintf(clSize,"%d",ilDemandGroupSize);
    while(AATArrayFindRowPointer(&(rgSwgIndexArray.rrArrayHandle),
				 &(rgSwgIndexArray.crArrayName[0]),
				 &(rgSwgIndexArray.rrIdx02Handle),
				 &(rgSwgIndexArray.crIdx02Name[0]),
				 clSize,&llRowNum2,
				 (void *) &pclSwgIndexRow ) == RC_SUCCESS)
    {
		llRowNum = ARR_FIRST;
		ilMatchingCount = 0;
		blMatching = FALSE;
		dbg(TRACE,"CreateGroupMAJobs IndexCode <%s>",SWGINDEXFIELD(pclSwgIndexRow,igSwgIndexCode));

		sprintf(clKey2,"%s,gr",SWGINDEXFIELD(pclSwgIndexRow,igSwgIndexCode));
		dbg(TRACE,"CreateGroupMAJobs clKey2 <%s>",clKey2);

		CEDAArrayDelete(&(rgMatchArray.rrArrayHandle),&(rgMatchArray.crArrayName[0]));
		while((CEDAArrayFindRowPointer(&(prpPoolJobArray->rrArrayHandle),
						   &(prpPoolJobArray->crArrayName[0]),
						   &(prpPoolJobArray->rrIdx06Handle),
						   &(prpPoolJobArray->crIdx06Name[0]),
						   clKey2,&llRowNum,
						   (void *) &pclPoolJobRow ) == RC_SUCCESS))
		{
			blMatching = FALSE;
			
			llRowNum3 = ARR_FIRST;
			while(CEDAArrayFindRowPointer(&(prpOpenDemArray->rrArrayHandle),
						  &(prpOpenDemArray->crArrayName[0]),
						  &(prpOpenDemArray->rrIdx04Handle),
						  &(prpOpenDemArray->crIdx04Name[0]),
						  pcpGroupKey,&llRowNum3,
						  (void *) &pclDemRow ) == RC_SUCCESS)
			{

				StrToTime(OPENDEMFIELD(pclDemRow,igDemDebe),&tlDebe);

				StrToTime(OPENDEMFIELD(pclDemRow,igDemDeen),&tlDeen);
			
				StrToTime(OPENDEMFIELD(pclDemRow,igDemLade),&tlLade);
				
				llTtgf = atol(OPENDEMFIELD(pclDemRow,igDemTtgf));

				llTtgt = atol(OPENDEMFIELD(pclDemRow,igDemTtgt));

				dbg(TRACE,"CreateGroupMAJobs Urno <%s> Urud <%s>",OPENDEMFIELD(pclDemRow,igDemUrno),OPENDEMFIELD(pclDemRow,igDemUrud));
				dbg(TRACE,"CreateGroupMAJobs Debe <%s> llTtgt <%s>",OPENDEMFIELD(pclDemRow,igDemDebe),OPENDEMFIELD(pclDemRow,igDemTtgt));
				dbg(TRACE,"CreateGroupMAJobs Deen <%s> llTtgf <%s>",OPENDEMFIELD(pclDemRow,igDemDeen),OPENDEMFIELD(pclDemRow,igDemTtgf));

				tlDeen -= llTtgf;
				tlDebe += llTtgt;

				tlLade = tlDeen;

				llRtwt = atoi(OPENDEMFIELD(pclDemRow,igDemRtwt));
				strcpy(pclAlid,OPENDEMFIELD(pclDemRow,igDemAlid));
				for ( ilPrio=1; ilPrio<= igGrpMaxFunction; ilPrio++ )
				{
					if(FindMatchingSinglePoolJob(pclPoolJobRow,OPENDEMFIELD(pclDemRow,igDemAlid),OPENDEMFIELD(pclDemRow,igDemAloc),
									 OPENDEMFIELD(pclDemRow,igDemUrud),tlDebe,tlDeen,tlLade,llTtgt,
									 llRtwt,ilPrio))
					{
						dbg(TRACE,"CreateGroupMAJobs FindMatchingSinglePoolJob Success PoolJobUrno <%s> Demand <%s> Prio <%d>",
							POOLJOBFIELD(pclPoolJobRow,igJobUrno), OPENDEMFIELD(pclDemRow,igDemUrno), ilPrio );

						llRowNum4 = ARR_FIRST;
						InitializeMatchRow(cgMatchBuf);
						AATArrayAddRow(&(rgMatchArray.rrArrayHandle),&(rgMatchArray.crArrayName[0]),&llRowNum4,(void *)cgMatchBuf);
						AATArrayPutField(&(rgMatchArray.rrArrayHandle),
								 &(rgMatchArray.crArrayName[0]),NULL,"STAT",llRowNum4,TRUE,"0") ;
						AATArrayPutField(&(rgMatchArray.rrArrayHandle),
								 &(rgMatchArray.crArrayName[0]),NULL,"UPJB",llRowNum4,TRUE,POOLJOBFIELD(pclPoolJobRow,igJobUrno)) ;
						AATArrayPutField(&(rgMatchArray.rrArrayHandle),
								 &(rgMatchArray.crArrayName[0]),NULL,"UDEM",llRowNum4,TRUE,OPENDEMFIELD(pclDemRow,igDemUrno)) ;
						AATArrayPutField(&(rgMatchArray.rrArrayHandle),
								 &(rgMatchArray.crArrayName[0]),NULL,"URUD",llRowNum4,TRUE,OPENDEMFIELD(pclDemRow,igDemUrud)) ;
						TimeToStr(pclDebe,tlDebe);
						AATArrayPutField(&(rgMatchArray.rrArrayHandle),
								 &(rgMatchArray.crArrayName[0]),NULL,"DEBE",llRowNum4,TRUE,pclDebe) ;
						TimeToStr(pclDeen,tlDeen);
						AATArrayPutField(&(rgMatchArray.rrArrayHandle),
								 &(rgMatchArray.crArrayName[0]),NULL,"DEEN",llRowNum4,TRUE,pclDeen) ;
								
						blMatching = TRUE;
						break;  /* we found one a prio which can be used for the job, so we don't have to go on */
					}
				}
				llRowNum3 = ARR_NEXT;		
			}
			if(blMatching)
			{
				ilMatchingCount++;
			}
			llRowNum = ARR_NEXT;
		}
		blUdgrQuali = FALSE;
		if(ilMatchingCount == ilDemandGroupSize)
		{
			blUdgrQuali = FALSE;
			llRowNum3 = ARR_FIRST;
			if(UngarischerAlgorithmus(&rgMatchArray,ilDemandGroupSize))
			{
				while(AATArrayFindRowPointer(&(rgMatchArray.rrArrayHandle),
								 &(rgMatchArray.crArrayName[0]),
								 &(rgMatchArray.rrIdx02Handle),
								 &(rgMatchArray.crIdx02Name[0]),
								 "1",&llRowNum3,
								 (void *) &pclMatchRow ) == RC_SUCCESS)
				{
					llRowNum4 = ARR_FIRST;
					if(CheckQualification(MATCHFIELD(pclMatchRow,igMatchUpjb),MATCHFIELD(pclMatchRow,igMatchUrud),1,3))
					{
						blUdgrQuali = TRUE;
					}
					InitializeGroupListRow(cgGroupListBuf);
					AATArrayAddRow(&(rgGroupListArray.rrArrayHandle),&(rgGroupListArray.crArrayName[0]),&llRowNum4,(void *)cgGroupListBuf);
					AATArrayPutField(&(rgGroupListArray.rrArrayHandle),
							 &(rgGroupListArray.crArrayName[0]),NULL,"GRPN",llRowNum4,TRUE,SWGINDEXFIELD(pclSwgIndexRow,igSwgIndexCode)) ;
					AATArrayPutField(&(rgGroupListArray.rrArrayHandle),
							 &(rgGroupListArray.crArrayName[0]),NULL,"UPJB",llRowNum4,TRUE,MATCHFIELD(pclMatchRow,igMatchUpjb)) ;
					AATArrayPutField(&(rgGroupListArray.rrArrayHandle),
							 &(rgGroupListArray.crArrayName[0]),NULL,"UDEM",llRowNum4,TRUE,MATCHFIELD(pclMatchRow,igMatchUdem)) ;
						
					AATArrayPutField(&(rgGroupListArray.rrArrayHandle),
							 &(rgGroupListArray.crArrayName[0]),NULL,"DEBE",llRowNum4,TRUE,MATCHFIELD(pclMatchRow,igMatchDebe)) ;
					AATArrayPutField(&(rgGroupListArray.rrArrayHandle),
							 &(rgGroupListArray.crArrayName[0]),NULL,"DEEN",llRowNum4,TRUE,MATCHFIELD(pclMatchRow,igMatchDeen)) ;
					llRowNum3 = ARR_NEXT;
				}
						
				
				llRowNum3 = ARR_FIRST;
						
				blIsValid = TRUE;
				while((CEDAArrayFindRowPointer(&(prpOpenDemArray->rrArrayHandle),
								   &(prpOpenDemArray->crArrayName[0]),
								   &(prpOpenDemArray->rrIdx04Handle),
								   &(prpOpenDemArray->crIdx04Name[0]),
								   pcpGroupKey,&llRowNum3,
								   (void *) &pclDemRow ) == RC_SUCCESS) && blIsValid)
				{
					llRowNum4 = ARR_FIRST;
					sprintf(clKey,"%s,%d",OPENDEMFIELD(pclDemRow,igDemUrud),3);
					while((CEDAArrayFindRowPointer(&(rgDemPrqArray.rrArrayHandle),
								   &(rgDemPrqArray.crArrayName[0]),
								   &(rgDemPrqArray.rrIdx01Handle),
								   &(rgDemPrqArray.crIdx01Name[0]),
								   clKey,&llRowNum4,
								   (void *) &pclDemPrqRow ) == RC_SUCCESS)	&& blIsValid)
					{

						llRowNum5 = ARR_FIRST;
						blIsValid = FALSE;
						while((CEDAArrayFindRowPointer(&(rgGroupListArray.rrArrayHandle),
									   &(rgGroupListArray.crArrayName[0]),
									   &(rgGroupListArray.rrIdx01Handle),
									   &(rgGroupListArray.crIdx01Name[0]),
									   SWGINDEXFIELD(pclSwgIndexRow,igSwgIndexCode),&llRowNum5,
									   (void *) &pclGroupListRow ) == RC_SUCCESS)	&& !blIsValid)
						{
							llRowNum6 = ARR_FIRST;
							sprintf(clKey2,"%s,%s,%d",GROUPLISTFIELD(pclGroupListRow,igGroupListUpjb),DEMPRQFIELD(pclDemPrqRow,igDemPrqQcod),1);
							if(CEDAArrayFindRowPointer(&(rgPoolPerArray.rrArrayHandle),
									   &(rgPoolPerArray.crArrayName[0]),
									   &(rgPoolPerArray.rrIdx01Handle),
									   &(rgPoolPerArray.crIdx01Name[0]),
									   clKey2,&llRowNum6,
									   (void *) &pclPoolPerRow ) == RC_SUCCESS)
							{
								blIsValid = TRUE;
							}
						}
					}
				}
			}
		}
		if(!blUdgrQuali || !blIsValid)
		{
			dbg(TRACE,"CreateGroupMAJobs blUdgrQuali <%d> blIsValid <%d>",blUdgrQuali,blIsValid);

			AATArrayPutField(&(rgSwgIndexArray.rrArrayHandle),
					 &(rgSwgIndexArray.crArrayName[0]),NULL,"SIZE",llRowNum2,TRUE,"x") ;
		}
		llRowNum2 = ARR_NEXT;
    }			

/*******Todo Schleife �ber SwgIndex und optimale Werte berechnen**/

    llRowNum = ARR_FIRST;
    while(AATArrayFindRowPointer(&(rgSwgIndexArray.rrArrayHandle),
				 &(rgSwgIndexArray.crArrayName[0]),
				 &(rgSwgIndexArray.rrIdx02Handle),
				 &(rgSwgIndexArray.crIdx02Name[0]),
				 clSize,&llRowNum,
				 (void *) &pclSwgIndexRow ) == RC_SUCCESS)
    {	
		llRowNum2 = ARR_FIRST;
		while(CEDAArrayFindRowPointer(&(rgGroupListArray.rrArrayHandle),
						  &(rgGroupListArray.crArrayName[0]),
						  &(rgGroupListArray.rrIdx01Handle),
						  &(rgGroupListArray.crIdx01Name[0]),
						  SWGINDEXFIELD(pclSwgIndexRow,igSwgIndexCode),&llRowNum2,
						  (void *) &pclGroupListRow ) == RC_SUCCESS)
		{
			llRowNum3 = ARR_FIRST;
			if(CEDAArrayFindRowPointer(&(prpPoolJobArray->rrArrayHandle),
						   &(prpPoolJobArray->crArrayName[0]),
						   &(prpPoolJobArray->rrIdx01Handle),
						   &(prpPoolJobArray->crIdx01Name[0]),
						   GROUPLISTFIELD(pclGroupListRow,igGroupListUpjb),&llRowNum3,
						   (void *) &pclPoolJobRow ) == RC_SUCCESS)
			{
				CEDAArrayPutField(&(prpPoolJobArray->rrArrayHandle),&(prpPoolJobArray->crArrayName[0]),NULL,"ISOK",llRowNum3,"x1");
						
				CEDAArrayPutField(&(prpPoolJobArray->rrArrayHandle),&(prpPoolJobArray->crArrayName[0]),NULL,"TCHX",llRowNum3,"1");
			}
			llRowNum2 = ARR_NEXT;
		}
		llRowNum = ARR_NEXT;
    }

    CalcOptimalValues(prpPoolJobArray,pclAlid, tlDebe,tlDeen,llTtgt,llRtwt,"x1");

    llRowNum = ARR_FIRST;
    if(CEDAArrayFindRowPointer(&(prpPoolJobArray->rrArrayHandle),
			       &(prpPoolJobArray->crArrayName[0]),
			       &(prpPoolJobArray->rrIdx04Handle),
			       &(prpPoolJobArray->crIdx04Name[0]),
			       "x1",&llRowNum,
			       (void *) &pclPoolJobRow ) == RC_SUCCESS)
    {
		llRowNum2 = ARR_FIRST;
		if(CEDAArrayFindRowPointer(&(rgGroupListArray.rrArrayHandle),
					   &(rgGroupListArray.crArrayName[0]),
					   &(rgGroupListArray.rrIdx02Handle),
					   &(rgGroupListArray.crIdx02Name[0]),
					   POOLJOBFIELD(pclPoolJobRow,igJobUrno),&llRowNum2,
					   (void *) &pclGroupListRow ) == RC_SUCCESS)
		{
			llRowNum3 = ARR_FIRST;
			while(CEDAArrayFindRowPointer(&(rgGroupListArray.rrArrayHandle),
						  &(rgGroupListArray.crArrayName[0]),
						  &(rgGroupListArray.rrIdx01Handle),
						  &(rgGroupListArray.crIdx01Name[0]),
						  GROUPLISTFIELD(pclGroupListRow,igGroupListGrpn),&llRowNum3,
						  (void *) &pclGroupListRow2 ) == RC_SUCCESS)
			{

				llRowNum1 = ARR_FIRST;
				InitializeKeyUrnoListRow(cgKeyUrnoListBuf);
						
				AATArrayAddRow(&(rgKeyUrnoListArray.rrArrayHandle),&(rgKeyUrnoListArray.crArrayName[0]),
						   &llRowNum1,(void *)cgKeyUrnoListBuf);

				AATArrayPutField(&(rgKeyUrnoListArray.rrArrayHandle),
						 &(rgKeyUrnoListArray.crArrayName[0]),NULL,"URNO",llRowNum1,TRUE,
						 GROUPLISTFIELD(pclGroupListRow2,igGroupListUpjb)) ;

				AATArrayPutField(&(rgKeyUrnoListArray.rrArrayHandle),
						 &(rgKeyUrnoListArray.crArrayName[0]),NULL,"UDEM",llRowNum1,TRUE,
						 GROUPLISTFIELD(pclGroupListRow2,igGroupListUdem)) ;
						
				StrToTime(GROUPLISTFIELD(pclGroupListRow2,igGroupListDebe),&tlDebe);
				StrToTime(GROUPLISTFIELD(pclGroupListRow2,igGroupListDeen),&tlDeen);
						
				tlDeen += atol(POOLJOBFIELD(pclPoolJobRow,igPoolJobWayf));
				tlDebe -= atol(POOLJOBFIELD(pclPoolJobRow,igPoolJobWayt));

				TimeToStr(pclDebe,tlDebe);
				TimeToStr(pclDeen,tlDeen);
						
				AATArrayPutField(&(rgKeyUrnoListArray.rrArrayHandle),
						 &(rgKeyUrnoListArray.crArrayName[0]),NULL,"BEGI",llRowNum1,TRUE,
						 pclDebe) ;
						
				AATArrayPutField(&(rgKeyUrnoListArray.rrArrayHandle),
						 &(rgKeyUrnoListArray.crArrayName[0]),NULL,"ENDE",llRowNum1,TRUE,
						 pclDeen) ;
						
				llRowNum3 = ARR_NEXT;
					
			}
		}
    }

    CEDAArrayDelete(&(rgSwgIndexArray.rrArrayHandle),&(rgSwgIndexArray.crArrayName[0]));
    CEDAArrayDelete(&(rgGroupListArray.rrArrayHandle),&(rgGroupListArray.crArrayName[0]));

    dbg(TRACE,"CreateGroupMAJobs End");


    return ilRc;
}

static int CreateCombinedMaJobs(ARRAYINFO *prpOpenDemArray,ARRAYINFO *prpPoolJobArray,
				char *pcpDemUrno,char *pcpDemUrue,char *pcpDemOuri,
				char *pcpDemOuro,char *pcpDemUpde,char *pcpDemUnde,int ipIndex)
{
    int ilRc = RC_SUCCESS;
    char *pclDemRow = NULL;
    char *pclNextDemRow = NULL;
    char *pclPrevDemRow = NULL;
    char *pclKeyUrnoListRow = NULL;
    char *pclMatchRow = NULL;
    long llRowNum = ARR_FIRST;
    long llNext = ARR_NEXT;
    long llUpde = atol(pcpDemUpde);
    long llUnde = atol(pcpDemUnde);
    long llUdem = atol(pcpDemUrno);
    BOOL blStop = FALSE;
    BOOL blPoolJobJound;
    char clKey[100];
    char clUrnoKey[50];
    char clPoolJobUrno[20];
    time_t tlDebe,tlDeen,tlLade;
    time_t tlTotalDebe,tlTotalDeen;
    long llTtgf,llTtgt,llMoveOffSet;
    char clIndex[20];
    char clDebe[20];
    char clDeen[20];
    int ilPos = 50;
    char clPos[4];
    long llRealTtgt;
    BOOL blFirstDemand = TRUE;
    dbg(TRACE,"CreateCombinedMaJobs Start");

    sprintf(clUrnoKey,"%s",pcpDemUrno);
    llRowNum = ARR_FIRST;
    if(CEDAArrayFindRowPointer(&(prpOpenDemArray->rrArrayHandle),
			       &(prpOpenDemArray->crArrayName[0]),
			       &(prpOpenDemArray->rrIdx01Handle),
			       &(prpOpenDemArray->crIdx01Name[0]),
			       clUrnoKey,&llRowNum,
			       (void *) &pclDemRow ) == RC_SUCCESS)
    {

	sprintf(clIndex,"%d",(ipIndex+1));
	CEDAArrayPutField(&(prpOpenDemArray->rrArrayHandle),&(prpOpenDemArray->crArrayName[0]),NULL,"TCHD",llRowNum,clIndex);

	llRowNum = ARR_FIRST;
		
	if(StrToTime(OPENDEMFIELD(pclDemRow,igDemDebe),&tlDebe) != RC_SUCCESS)
	{
	    dbg(TRACE,"CreateCombinedMaJobs StrToTime tlDebe failed <%s>",OPENDEMFIELD(pclDemRow,igDemDebe));
	}

	if(StrToTime(OPENDEMFIELD(pclDemRow,igDemDeen),&tlDeen) != RC_SUCCESS)
	{
	    dbg(TRACE,"CreateCombinedMaJobs StrToTime tlDeen failed <%s>",OPENDEMFIELD(pclDemRow,igDemDeen));
	}
	if(StrToTime(OPENDEMFIELD(pclDemRow,igDemLade),&tlLade) != RC_SUCCESS)
	{
	    dbg(TRACE,"CreateCombinedMaJobs StrToTime tlLade failed <%s>",OPENDEMFIELD(pclDemRow,igDemLade));
	}

		
	llTtgf = atol(OPENDEMFIELD(pclDemRow,igDemTtgf));
	llTtgt = atol(OPENDEMFIELD(pclDemRow,igDemTtgt));

	llRealTtgt = llTtgt;
	dbg(DEBUG,"CreateCombinedMaJobs llTtgf <%ld> llTtgt <%ld>",llTtgf,llTtgt);


	tlDeen -= llTtgf;
	tlDebe += llTtgt;
	if(tlLade > tlDeen)
	{
	    llMoveOffSet = tlLade - tlDeen;
	}
	else
	{
	    llMoveOffSet = 0;
	}

	tlTotalDebe = tlDebe;
	tlTotalDeen = tlDeen;
	CEDAArrayDelete(&(rgMatchArray.rrArrayHandle),&(rgMatchArray.crArrayName[0]));

	sprintf(clPos,"%d",ilPos);
	InitializeMatchRow(cgMatchBuf);
	AATArrayAddRow(&(rgMatchArray.rrArrayHandle),&(rgMatchArray.crArrayName[0]),&llNext,(void *)cgMatchBuf);
	AATArrayPutField(&(rgMatchArray.rrArrayHandle),
			 &(rgMatchArray.crArrayName[0]),NULL,"MARK",llNext,TRUE,clPos) ;
	AATArrayPutField(&(rgMatchArray.rrArrayHandle),
			 &(rgMatchArray.crArrayName[0]),NULL,"UDEM",llNext,TRUE,OPENDEMFIELD(pclDemRow,igDemUrno)) ;
	AATArrayPutField(&(rgMatchArray.rrArrayHandle),
			 &(rgMatchArray.crArrayName[0]),NULL,"URUD",llNext,TRUE,OPENDEMFIELD(pclDemRow,igDemUrud)) ;
	TimeToStr(clDebe,tlDebe);
	AATArrayPutField(&(rgMatchArray.rrArrayHandle),
			 &(rgMatchArray.crArrayName[0]),NULL,"DEBE",llNext,TRUE,OPENDEMFIELD(pclDemRow,igDemDebe)) ;
	TimeToStr(clDeen,tlDeen);
	AATArrayPutField(&(rgMatchArray.rrArrayHandle),
			 &(rgMatchArray.crArrayName[0]),NULL,"DEEN",llNext,TRUE,OPENDEMFIELD(pclDemRow,igDemDeen)) ;

		
	
	llUpde = atol(OPENDEMFIELD(pclDemRow,igDemUpde));
	if(llUpde > 0)
	{
	    sprintf(clKey,"%s,%s,%s,%s",pcpDemUrue,pcpDemOuri,pcpDemOuro,OPENDEMFIELD(pclDemRow,igDemUpde));
	    llRowNum = ARR_FIRST;
	    while((CEDAArrayFindRowPointer(&(prpOpenDemArray->rrArrayHandle),
					   &(prpOpenDemArray->crArrayName[0]),
					   &(prpOpenDemArray->rrIdx05Handle),
					   &(prpOpenDemArray->crIdx05Name[0]),
					   clKey,&llRowNum,
					   (void *) &pclPrevDemRow ) == RC_SUCCESS) && !blStop)
	    {
		llRowNum = ARR_FIRST;
				
		if(StrToTime(OPENDEMFIELD(pclPrevDemRow,igDemDebe),&tlDebe) != RC_SUCCESS)
		{
		    dbg(TRACE,"CreateCombinedMaJobs StrToTime tlDebe failed <%s>",OPENDEMFIELD(pclPrevDemRow,igDemDebe));
		}

		if(StrToTime(OPENDEMFIELD(pclPrevDemRow,igDemDeen),&tlDeen) != RC_SUCCESS)
		{
		    dbg(TRACE,"CreateCombinedMaJobs StrToTime tlDeen failed <%s>",OPENDEMFIELD(pclPrevDemRow,igDemDeen));
		}
		if(StrToTime(OPENDEMFIELD(pclPrevDemRow,igDemLade),&tlLade) != RC_SUCCESS)
		{
		    dbg(TRACE,"CreateCombinedMaJobs StrToTime tlLade failed <%s>",OPENDEMFIELD(pclPrevDemRow,igDemLade));
		}

				
		llTtgf = atol(OPENDEMFIELD(pclPrevDemRow,igDemTtgf));
		llTtgt = atol(OPENDEMFIELD(pclPrevDemRow,igDemTtgt));

		llRealTtgt = llTtgt;

		dbg(DEBUG,"CreateCombinedMaJobs llTtgf <%ld> llTtgt <%ld>",llTtgf,llTtgt);


		tlDeen -= llTtgf;
		tlDebe += llTtgt;
		if(tlLade > tlDeen)
		{
		    llMoveOffSet = tlLade - tlDeen;
		}
		else
		{
		    llMoveOffSet = 0;
		}

		sprintf(clPos,"%d",++ilPos);

		tlTotalDebe = (tlDebe < tlTotalDebe)?tlDebe:tlTotalDebe;
		InitializeMatchRow(cgMatchBuf);
		llNext = ARR_NEXT;
		AATArrayAddRow(&(rgMatchArray.rrArrayHandle),&(rgMatchArray.crArrayName[0]),&llNext,(void *)cgMatchBuf);

		AATArrayPutField(&(rgMatchArray.rrArrayHandle),
				 &(rgMatchArray.crArrayName[0]),NULL,"MARK",llNext,TRUE,clPos) ;

		AATArrayPutField(&(rgMatchArray.rrArrayHandle),
				 &(rgMatchArray.crArrayName[0]),NULL,"UDEM",llNext,TRUE,OPENDEMFIELD(pclPrevDemRow,igDemUrno)) ;
		AATArrayPutField(&(rgMatchArray.rrArrayHandle),
				 &(rgMatchArray.crArrayName[0]),NULL,"URUD",llNext,TRUE,OPENDEMFIELD(pclPrevDemRow,igDemUrud)) ;
		TimeToStr(clDebe,tlDebe);
		AATArrayPutField(&(rgMatchArray.rrArrayHandle),
				 &(rgMatchArray.crArrayName[0]),NULL,"DEBE",llNext,TRUE,OPENDEMFIELD(pclPrevDemRow,igDemDebe)) ;
		TimeToStr(clDeen,tlDeen);
		AATArrayPutField(&(rgMatchArray.rrArrayHandle),
				 &(rgMatchArray.crArrayName[0]),NULL,"DEEN",llNext,TRUE,OPENDEMFIELD(pclPrevDemRow,igDemDeen)) ;
		llUpde = atol(OPENDEMFIELD(pclPrevDemRow,igDemUpde));

		if(llUpde > 0)
		{
		    sprintf(clKey,"%s,%s,%s,%s",OPENDEMFIELD(pclPrevDemRow,igDemUrue),OPENDEMFIELD(pclPrevDemRow,igDemOuri),
			    OPENDEMFIELD(pclPrevDemRow,igDemOuro),OPENDEMFIELD(pclPrevDemRow,igDemUpde));
		    llRowNum = ARR_FIRST;
		}
		else
		{
		    blStop = TRUE;
		}
	    }
	}
	
	ilPos = 50;
	llUnde = atol(OPENDEMFIELD(pclDemRow,igDemUnde));
	if(llUnde > 0)
	{
	    blStop = FALSE;
	    sprintf(clKey,"%s,%s,%s,%s",pcpDemUrue,pcpDemOuri,pcpDemOuro,OPENDEMFIELD(pclDemRow,igDemUnde));
	    llRowNum = ARR_FIRST;
	    while((CEDAArrayFindRowPointer(&(prpOpenDemArray->rrArrayHandle),
					   &(prpOpenDemArray->crArrayName[0]),
					   &(prpOpenDemArray->rrIdx05Handle),
					   &(prpOpenDemArray->crIdx05Name[0]),
					   clKey,&llRowNum,
					   (void *) &pclNextDemRow ) == RC_SUCCESS) && !blStop)
	    {

		llRowNum = ARR_FIRST;
				
		if(StrToTime(OPENDEMFIELD(pclNextDemRow,igDemDebe),&tlDebe) != RC_SUCCESS)
		{
		    dbg(TRACE,"CreateCombinedMaJobs StrToTime tlDebe failed <%s>",OPENDEMFIELD(pclNextDemRow,igDemDebe));
		}

		if(StrToTime(OPENDEMFIELD(pclNextDemRow,igDemDeen),&tlDeen) != RC_SUCCESS)
		{
		    dbg(TRACE,"CreateCombinedMaJobs StrToTime tlDeen failed <%s>",OPENDEMFIELD(pclNextDemRow,igDemDeen));
		}
		if(StrToTime(OPENDEMFIELD(pclNextDemRow,igDemLade),&tlLade) != RC_SUCCESS)
		{
		    dbg(TRACE,"CreateCombinedMaJobs StrToTime tlLade failed <%s>",OPENDEMFIELD(pclNextDemRow,igDemLade));
		}

				
		llTtgf = atol(OPENDEMFIELD(pclNextDemRow,igDemTtgf));
		llTtgt = atol(OPENDEMFIELD(pclNextDemRow,igDemTtgt));

		dbg(DEBUG,"CreateCombinedMaJobs llTtgf <%ld> llTtgt <%ld>",llTtgf,llTtgt);


		tlDeen -= llTtgf;
		tlDebe += llTtgt;
		if(tlLade > tlDeen)
		{
		    llMoveOffSet = tlLade - tlDeen;
		}
		else
		{
		    llMoveOffSet = 0;
		}

		sprintf(clPos,"%d",--ilPos);

		tlTotalDeen = (tlDeen > tlTotalDeen)?tlDeen:tlTotalDeen;
		InitializeMatchRow(cgMatchBuf);
		llNext = ARR_NEXT;
		AATArrayAddRow(&(rgMatchArray.rrArrayHandle),&(rgMatchArray.crArrayName[0]),&llNext,(void *)cgMatchBuf);
		AATArrayPutField(&(rgMatchArray.rrArrayHandle),
				 &(rgMatchArray.crArrayName[0]),NULL,"MARK",llNext,TRUE,clPos);
		AATArrayPutField(&(rgMatchArray.rrArrayHandle),
				 &(rgMatchArray.crArrayName[0]),NULL,"UDEM",llNext,TRUE,OPENDEMFIELD(pclNextDemRow,igDemUrno)) ;
		AATArrayPutField(&(rgMatchArray.rrArrayHandle),
				 &(rgMatchArray.crArrayName[0]),NULL,"URUD",llNext,TRUE,OPENDEMFIELD(pclNextDemRow,igDemUrud)) ;
		TimeToStr(clDebe,tlDebe);
		AATArrayPutField(&(rgMatchArray.rrArrayHandle),
				 &(rgMatchArray.crArrayName[0]),NULL,"DEBE",llNext,TRUE,OPENDEMFIELD(pclNextDemRow,igDemDebe)) ;
		TimeToStr(clDeen,tlDeen);
		AATArrayPutField(&(rgMatchArray.rrArrayHandle),
				 &(rgMatchArray.crArrayName[0]),NULL,"DEEN",llNext,TRUE,OPENDEMFIELD(pclNextDemRow,igDemDeen)) ;
		llUnde = atol(OPENDEMFIELD(pclNextDemRow,igDemUnde));

		if(llUnde > 0)
		{
		    sprintf(clKey,"%s,%s,%s,%s",OPENDEMFIELD(pclNextDemRow,igDemUrue),OPENDEMFIELD(pclNextDemRow,igDemOuri),
			    OPENDEMFIELD(pclNextDemRow,igDemOuro),OPENDEMFIELD(pclNextDemRow,igDemUnde));
		    llRowNum = ARR_FIRST;
		}
		else
		{
		    blStop = TRUE;
		}
	    }
	}

	CreateSplittedMAJobs(prpPoolJobArray,
			     OPENDEMFIELD(pclDemRow,igDemUrno),OPENDEMFIELD(pclDemRow,igDemUrud),
			     OPENDEMFIELD(pclDemRow,igDemTpln),
			     OPENDEMFIELD(pclDemRow,igDemAlid),OPENDEMFIELD(pclDemRow,igDemAloc),llTtgt,
			     atoi(OPENDEMFIELD(pclDemRow,igDemRtwt)),ipIndex,
			     tlTotalDebe,tlTotalDeen,llMoveOffSet,0,0,&llRealTtgt);

	llRowNum = ARR_FIRST;
	blPoolJobJound = FALSE;
	if(AATArrayFindRowPointer(&(rgKeyUrnoListArray.rrArrayHandle),
				  &(rgKeyUrnoListArray.crArrayName[0]),
				  &(rgKeyUrnoListArray.rrIdx01Handle),
				  &(rgKeyUrnoListArray.crIdx01Name[0]),
				  "",&llRowNum,
				  (void *) &pclKeyUrnoListRow) == RC_SUCCESS)
	{
	    blPoolJobJound = TRUE;
	    strcpy(clPoolJobUrno,KEYURNOLISTFIELD(pclKeyUrnoListRow,igKeyUrnoListUrno));

	}

	if(blPoolJobJound)
	{
	    CEDAArrayDelete(&(rgKeyUrnoListArray.rrArrayHandle),&(rgKeyUrnoListArray.crArrayName[0]));

	    blFirstDemand = TRUE;
	    llRowNum = ARR_FIRST;
	    while(CEDAArrayFindRowPointer(&(rgMatchArray.rrArrayHandle),
					  &(rgMatchArray.crArrayName[0]),
					  &(rgMatchArray.rrIdx03Handle),
					  &(rgMatchArray.crIdx03Name[0]),
					  "",&llRowNum,
					  (void *) &pclMatchRow ) == RC_SUCCESS)
	    {
		InitializeKeyUrnoListRow(cgKeyUrnoListBuf);
				
		if(blFirstDemand)
		{
		    TimeToStr(clDebe,tlTotalDebe - llRealTtgt);
		    AATArrayPutField(&(rgMatchArray.rrArrayHandle),
				     &(rgMatchArray.crArrayName[0]),NULL,"DEBE",llRowNum,TRUE,
				     clDebe) ;
		}
		blFirstDemand = FALSE;



		llNext = ARR_NEXT;
		AATArrayAddRow(&(rgKeyUrnoListArray.rrArrayHandle),&(rgKeyUrnoListArray.crArrayName[0]),
			       &llNext,(void *)cgKeyUrnoListBuf);

		AATArrayPutField(&(rgKeyUrnoListArray.rrArrayHandle),
				 &(rgKeyUrnoListArray.crArrayName[0]),NULL,"URNO",llNext,TRUE,
				 clPoolJobUrno) ;
		AATArrayPutField(&(rgKeyUrnoListArray.rrArrayHandle),
				 &(rgKeyUrnoListArray.crArrayName[0]),NULL,"BEGI",llNext,TRUE,
				 MATCHFIELD(pclMatchRow,igMatchDebe));
		AATArrayPutField(&(rgKeyUrnoListArray.rrArrayHandle),
				 &(rgKeyUrnoListArray.crArrayName[0]),NULL,"XKEY",llNext,TRUE,
				 MATCHFIELD(pclMatchRow,igMatchMark));
		AATArrayPutField(&(rgKeyUrnoListArray.rrArrayHandle),
				 &(rgKeyUrnoListArray.crArrayName[0]),NULL,"ENDE",llNext,TRUE,
				 MATCHFIELD(pclMatchRow,igMatchDeen)) ;
		AATArrayPutField(&(rgKeyUrnoListArray.rrArrayHandle),
				 &(rgKeyUrnoListArray.crArrayName[0]),NULL,"UDEM",llNext,TRUE,
				 MATCHFIELD(pclMatchRow,igMatchUdem)) ;
			
		llRowNum = ARR_NEXT;
	    }
	}
    }

    dbg(TRACE,"CreateCombinedMaJobs Start");

    return ilRc;
}
										



/* 20000619 bch start */
static int GetItemData(char *pcpFieldName,char *pcpFields,char *pcpData,char *pcpDest)
{
    int ilRc = RC_FAIL;
    int ilLc;
    int ilItemCount;
    char pclFieldName[1200];

    ilItemCount = get_no_of_items(pcpFields);

    for(ilLc = 1; ilLc <= ilItemCount && ilRc != RC_SUCCESS; ilLc++)
    {
	get_real_item(pclFieldName,pcpFields,ilLc);
	if(!strcmp(pclFieldName,pcpFieldName))
	{
	    get_real_item(pcpDest,pcpData,ilLc);
	    ilRc = RC_SUCCESS;
	}
    }

    return ilRc;
}
/* 20000619 bch end */

static int GetItemNo(char *pcpFieldName,char *pcpFields,
		     int *pipItemNo)
{
    int ilRc = RC_FAIL;
    int ilLc;
    int ilItemCount;
    char pclFieldName[1200];

    *pipItemNo = 0;

    ilItemCount = get_no_of_items(pcpFields);

    for(ilLc = 1; ilLc <= ilItemCount && ilRc != RC_SUCCESS; ilLc++)
    {
	get_real_item(pclFieldName,pcpFields,ilLc);
	if(!strcmp(pclFieldName,pcpFieldName))
	{
	    *pipItemNo = ilLc;
	    ilRc = RC_SUCCESS;
	}
    }

    return ilRc;
}


static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,char *pcpCfgBuffer)
{
	
    char pclSection[124];
    char pclKeyword[124];

    strcpy(pclSection,pcpSection);
    strcpy(pclKeyword,pcpKeyword);

    return iGetConfigEntry(cgConfigFile,pclSection,pclKeyword,CFG_STRING,pcpCfgBuffer);
}

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/*correct timefunction HEB*/
static int StrToTime(char *pcpTime,time_t *plpTime)
{

    struct tm *prlTm;
    struct tm rlTm;
    int i = 0;
    time_t tlNow;
    int ilIsDst = 0;
    int ilYear,ilMon,ilDay,ilHour,ilMin,ilSec;
    char clTmpString[124];
    char clTime[124];


    tlNow = time(NULL);
    prlTm = (struct tm *)localtime_r((const time_t*)&tlNow,&rlTm);
    tlNow = mktime(&rlTm);
    for (i=1; i<=2; i++)
    {
	strcpy(clTime,pcpTime);

	strncpy(clTmpString,clTime,4);
	clTmpString[4] = '\0';
	ilYear = atoi(clTmpString)-1900;

	strncpy(clTmpString,&clTime[4],2);
	clTmpString[2] = '\0';
	ilMon = atoi(clTmpString)-1;

	strncpy(clTmpString,&clTime[6],2);
	clTmpString[2] = '\0';
	ilDay = atoi(clTmpString);

	strncpy(clTmpString,&clTime[6],2);
	clTmpString[2] = '\0';
	ilDay = atoi(clTmpString);

	strncpy(clTmpString,&clTime[8],2);
	clTmpString[2] = '\0';
	ilHour = atoi(clTmpString);

	strncpy(clTmpString,&clTime[10],2);
	clTmpString[2] = '\0';
	ilMin = atoi(clTmpString);

	strncpy(clTmpString,&clTime[12],2);
	clTmpString[2] = '\0';
	ilSec = atoi(clTmpString);

	rlTm.tm_year = ilYear;
	rlTm.tm_mon  = ilMon;
	rlTm.tm_mday = ilDay;
	rlTm.tm_hour = ilHour;
	rlTm.tm_min = ilMin;
	rlTm.tm_sec = ilSec;

	ilIsDst = rlTm.tm_isdst;
	tlNow = mktime(&rlTm);
	if ((i == 1) && (ilIsDst != rlTm.tm_isdst) && (ilHour == 2))
	{
	    rlTm.tm_isdst = 0;
	}
 
    } /* end of for*/


    dbg(DEBUG,"StrToTime: %02d.%02d.%04d %02d:%02d Now: %ld",rlTm.tm_mday,
	rlTm.tm_mon+1,rlTm.tm_year+1900,rlTm.tm_hour,rlTm.tm_min,tlNow);
    if (tlNow != (time_t) -1)
    {
	dbg(DEBUG,"StrToTime: pcpTime <%s> plptime: <%d>", pcpTime, tlNow);
	*plpTime = tlNow;
	return RC_SUCCESS;
    }
    *plpTime = time(NULL);
    return RC_FAIL;
}



#if 0
static int StrToTime(char *pcpTime,time_t *plpTime)
{
    struct tm *_tm;
    time_t now;
    char   _tmpc[6];

/*	dbg(DEBUG,"StrToTime, <%s>",pcpTime);*/
	
    if (strlen(pcpTime) < 12 /*|| (atol(pcpTime) <= 0)*/)
    {
	*plpTime = time(0L);
	return RC_FAIL;
    } /* end if */

    now = time(0L);
    _tm = (struct tm *)gmtime(&now);

    _tmpc[2] = '\0';
/*
  strncpy(_tmpc,pcpTime+12,2);
  _tm -> tm_sec = atoi(_tmpc);
*/
    _tm -> tm_sec = 0;
	
    strncpy(_tmpc,pcpTime+10,2);
    _tm -> tm_min = atoi(_tmpc);
    strncpy(_tmpc,pcpTime+8,2);
    _tm -> tm_hour = atoi(_tmpc);
    strncpy(_tmpc,pcpTime+6,2);
    _tm -> tm_mday = atoi(_tmpc);
    strncpy(_tmpc,pcpTime+4,2);
    _tm -> tm_mon = atoi(_tmpc)-1;
    strncpy(_tmpc,pcpTime,4);
    _tmpc[4] = '\0';
    _tm -> tm_year = atoi(_tmpc)-1900;
    _tm -> tm_wday = 0;
    _tm -> tm_yday = 0;
    _tm->tm_isdst = -1; /* no adjustment of daylight saving time */
    now = mktime(_tm);
    dbg(DEBUG,"StrToTime: %02d.%02d.%04d %02d:%02d Now: %ld",_tm->tm_mday,
	_tm->tm_mon+1,_tm->tm_year+1900,_tm->tm_hour,_tm->tm_min,now);
    if (now != (time_t) -1)
    {
	/*now +=  + 3600;*/
	/*now -=  3600;*/
	*plpTime = now;
	return RC_SUCCESS;
    }
    *plpTime = time(NULL);
    return RC_FAIL;
}
#endif
/*correct timefunction HEB*/
static int TimeToStr(char *pcpTime,time_t lpTime) 
{               
    struct tm *prlTm;
    struct tm rlTm;

    prlTm = (struct tm *)localtime_r(&lpTime,&rlTm);

    sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
	    rlTm.tm_year+1900,rlTm.tm_mon+1,rlTm.tm_mday,rlTm.tm_hour,
	    rlTm.tm_min,rlTm.tm_sec);
    return (rlTm.tm_wday == 0 ? 7 : rlTm.tm_wday) + '0';
}                      




#if 0					
static int TimeToStr(char *pcpTime,time_t lpTime) 
{                                                                               
    struct tm *_tm;
	
    _tm = (struct tm *)localtime(&lpTime);
    sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
	    _tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
	    _tm->tm_min,_tm->tm_sec);
    return (_tm->tm_wday == 0 ? 7 : _tm->tm_wday) + '0';                          
}                                                                               
#endif




static void ToUpper(char *pcpText)
{
    int ilLen = strlen(pcpText);
    int ilChar;
    for(ilChar = 0; ilChar < ilLen; ilChar++)
    {
	pcpText[ilChar] = toupper(pcpText[ilChar]);
    }
}

static int LocalToUtc(char *pcpTime)
{
    int ilRc = RC_SUCCESS;
    char *pclTdi;
    char clLocal[32];

    strcpy(clLocal,pcpTime);
    pclTdi = (strcmp(pcpTime,cgTich) < 0) ? cgTdi1 : cgTdi2;
    AddSecondsToCEDATime(pcpTime,(time_t)0-atol(pclTdi),1);

    return ilRc;
}



static void MoveJobTime(time_t tlDemTime,time_t *plpPlStart,time_t *plpPlEnd,time_t *plpActStart,time_t *plpActEnd)
{
    long llOffSet  = 0L;
    time_t tlStart,tlActStart;
    time_t tlEnd,tlActEnd;

    tlStart = *plpPlStart;
    tlActStart = *plpActStart;
    tlEnd = *plpPlEnd;
    tlActEnd = *plpActEnd;

    ChangeTime(tlDemTime,plpPlStart,plpActStart);
    llOffSet = *plpPlStart - tlStart;
    tlEnd += llOffSet;
    llOffSet = *plpActStart - tlActStart;
    tlActEnd += llOffSet;

    *plpPlEnd = tlEnd;
    *plpActEnd = tlActEnd  ;
}

static void ChangeTime(time_t tlDemTime,time_t *plpPlannedTime,time_t *plpActTime)
{
    long llOffSet  = 0L;
    time_t tlStart,tlActStart;

    tlStart = *plpPlannedTime;
    tlActStart = *plpActTime;

    llOffSet = tlDemTime - tlStart;
    dbg(DEBUG,"ChangeTime llOffSet <%ld>",llOffSet);
    if(llOffSet < 0)
    {
	if(tlActStart < tlStart)
	{
	    tlActStart = (tlActStart < tlDemTime)?tlActStart : tlDemTime;
	}
	else
	{
	    tlActStart = tlDemTime +(tlActStart - tlStart);
	}
    }
    else
    {
	if(tlActStart > tlStart)
	{
	    tlActStart = (tlActStart > tlDemTime)?tlActStart : tlDemTime;
	}
	else
	{
	    dbg(DEBUG,"ChangeTime tlActStart <%ld> tlStart <%ld> diff <%ld>",tlActStart,tlStart,tlActStart - tlStart);
	    tlActStart = tlDemTime +(tlActStart - tlStart);
	    dbg(DEBUG,"ChangeTime tlActStart <%ld> ",tlActStart);
	}
		
    }

    *plpPlannedTime = tlDemTime;
    *plpActTime = tlActStart  ;
}

static int ChangeJobTime(char *pcpUrno,char *pcpDebe,BOOL bpDebeFound,char *pcpDeen,BOOL bpDeenFound)
{
    int	ilRc        = RC_SUCCESS;             /* Return code */
    int	ilTimeRC        = RC_SUCCESS;             /* Return code */

    char *pclData;
    char pclPannedTime[30];
    char pclActTime[30];
    char pclStatus[15]; 

    long llRowNum = ARR_FIRST;
    long llFirst = ARR_FIRST;
    long llRowCount = 0L;
    char *pclJodRow = NULL;
    char *pclJobRow = NULL;
    time_t tlStart,tlEnd,tlActStart,tlActEnd,tlDebe,tlDeen;

    llRowNum = ARR_FIRST;
    while(CEDAArrayFindRowPointer(&(rgJodArray.rrArrayHandle),
				  &(rgJodArray.crArrayName[0]),
				  &(rgJodArray.rrIdx01Handle),
				  &(rgJodArray.crIdx01Name[0]),
				  pcpUrno,&llRowNum,
				  (void *) &pclJodRow ) == RC_SUCCESS)
    {
	llRowNum = ARR_NEXT;
	dbg(DEBUG,"ChangeJobTime:Ujob <%s> ",JODFIELD(pclJodRow,igJodUjob)); 
	llFirst = ARR_FIRST;
	if(CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
				   &(rgJobArray.crArrayName[0]),
				   &(rgJobArray.rrIdx02Handle),
				   &(rgJobArray.crIdx02Name[0]),
				   JODFIELD(pclJodRow,igJodUjob),&llFirst,
				   (void *) &pclJobRow ) == RC_SUCCESS)
	{

	    CEDAArrayPutField(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,"TXXD",llFirst,"xx");
	    llRowCount++;
	}
    }


    if(llRowCount >0)
    {
			
	llRowNum = ARR_FIRST;

	dbg(DEBUG,"ChangeJobTime:pcpUrno <%s> ",pcpUrno);      
	dbg(DEBUG,"ChangeJobTime:Job Count = %ld ",llRowCount);      
	if(ilRc == RC_SUCCESS)
	{
	    int ilIndex = 0;
	    long llOffSet = 0L;
	    long llActOffSet = 0L;

	    BOOL blDebeIsSet = FALSE;
	    if(bpDebeFound)
	    {
		ilTimeRC = StrToTime(pcpDebe,&tlDebe);
	    }
	    if(bpDeenFound && ilTimeRC == RC_SUCCESS)
	    {
		ilTimeRC = StrToTime(pcpDeen,&tlDeen);
	    }

	    if(ilTimeRC == RC_SUCCESS)
	    {
		llFirst = ARR_FIRST;
		while(CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
					      &(rgJobArray.crArrayName[0]),
					      &(rgJobArray.rrIdx03Handle),
					      &(rgJobArray.crIdx03Name[0]),
					      "xx",&llFirst,
					      (void *) &pclData ) == RC_SUCCESS)

		{	 
		    ilIndex++;

		    strcpy(pclStatus,JOBFIELD(pclData,igJobStat)); 
		    dbg(DEBUG,"pcStatus %s, igJobStat %d",pclStatus,igJobStat);
		    dbg(DEBUG,"pclData %s, igJobPlfr %d",pclData,igJobPlfr);
		    dbg(DEBUG," 1 Ujob <%s>",JOBFIELD(pclData,igJobUrno));

		    ilTimeRC = StrToTime(JOBFIELD(pclData,igJobPlfr),&tlStart);
		    dbg(DEBUG,"1StrToTime(JOBFIELD:Plrf %ld ,RC:%d",tlStart,ilTimeRC);
		    if(ilTimeRC == RC_SUCCESS)
		    {
			ilTimeRC = StrToTime(JOBFIELD(pclData,igJobPlto),&tlEnd);
			dbg(DEBUG,"2StrToTime(JOBFIELD:Plto %ld ,RC:%d",tlEnd,ilTimeRC);
		    }			
		    if(ilTimeRC == RC_SUCCESS)
		    {
			ilTimeRC = StrToTime(JOBFIELD(pclData,igJobAcfr),&tlActStart);
			dbg(DEBUG,"3StrToTime(JOBFIELD:Actfr %ld ,RC:%d",tlActStart,ilTimeRC);
		    }
		    if(ilTimeRC == RC_SUCCESS)
		    {
			ilTimeRC = StrToTime(JOBFIELD(pclData,igJobActo),&tlActEnd);
			dbg(DEBUG,"4StrToTime(JOBFIELD:Actto %ld ,RC:%d",tlActEnd,ilTimeRC);
		    }

		    if(ilTimeRC == RC_SUCCESS)
		    {
			if(llRowCount == 1)
			{
			    if(bpDebeFound  && *pclStatus == 'P')
			    {
				ChangeTime(tlDebe,&tlStart ,&tlActStart);
											
				TimeToStr(pclPannedTime,tlStart);
				dbg(DEBUG,"pclPlannedTime  %s",pclPannedTime);
				if(CEDAArrayPutField(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,"PLFR",llFirst,pclPannedTime) != RC_SUCCESS)
				{
				    ilTimeRC = RC_FAIL;
				    dbg(DEBUG,"ArrayPutField failed(Plrf %s",pclPannedTime);
				    break;
				}
				TimeToStr(pclActTime,tlActStart);
				dbg(DEBUG,"pclActTime  %s",pclActTime);
				if(CEDAArrayPutField(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,"ACFR",llFirst,pclActTime) != RC_SUCCESS)
				{
				    ilTimeRC = RC_FAIL;
				    dbg(DEBUG,"ArrayPutField failed(Actfr %s",pclActTime);
				    break;
				}
			    }
			    if(bpDeenFound && *pclStatus != 'F')
			    {
				ChangeTime(tlDeen,&tlEnd ,&tlActEnd);
				TimeToStr(pclPannedTime,tlEnd);
				dbg(DEBUG,"pclPlannedTime end %s",pclPannedTime);
				if(CEDAArrayPutField(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,"PLTO",llFirst,pclPannedTime) != RC_SUCCESS)
				{
				    ilTimeRC = RC_FAIL;
				    dbg(DEBUG,"ArrayPutField failed(Plto %s",pclPannedTime);
				    break;
				}
				TimeToStr(pclActTime,tlActEnd);
				dbg(DEBUG,"pclActTime end  %s",pclActTime);
				if(CEDAArrayPutField(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,"ACTO",llFirst,pclActTime) != RC_SUCCESS)
				{
				    ilTimeRC = RC_FAIL;
				    dbg(DEBUG,"ArrayPutField failed(Actto %s",pclActTime);
				}
			    }
			}
			else  
			{
			    BOOL blMoveAllJobs = FALSE;
			    time_t tlOrgStart = tlStart;
			    time_t tlOrgActStart = tlActStart;
			    if(bpDebeFound && !bpDeenFound && *pclStatus == 'P' )
			    {
				tlOrgStart = tlStart;
				tlOrgActStart = tlActStart;
				if(ilIndex == 1)
				{
				    MoveJobTime(tlDebe,&tlStart,&tlEnd ,&tlActStart,&tlActEnd);
				    if(tlDebe > tlOrgStart)
				    {
					blMoveAllJobs = TRUE;
				    }
				    else
				    {
					llOffSet = tlStart - tlOrgStart;
					llActOffSet = tlActStart - tlOrgActStart;
				    }
				}
				else if(ilIndex != llRowCount)
				{
				    if(blMoveAllJobs)
				    {
					if(tlDebe > tlStart)
					{
					    MoveJobTime(tlDebe,&tlStart,&tlEnd ,&tlActStart,&tlActEnd);
					}
				    }else
				    {
					tlStart += llOffSet;
					tlEnd += llOffSet;
					tlActStart += llOffSet;
					tlActEnd += llActOffSet;
				    }
				}

			    }

			    if(bpDebeFound && bpDeenFound && *pclStatus == 'P')
			    {
				blDebeIsSet = TRUE;
				/*	  if(tlDebe < tlStart)*/
				{
				    blDebeIsSet= TRUE;
				    tlOrgStart = tlStart;
				    tlOrgActStart = tlActStart;
				    if(ilIndex == 1)
				    {
					MoveJobTime(tlDebe,&tlStart,&tlEnd ,&tlActStart,&tlActEnd);
					dbg(DEBUG,"MoveJobTime Actrf %ld ",tlActStart);
					if(tlDebe > tlOrgStart)
					{
					    blMoveAllJobs = TRUE;

					    llOffSet = tlStart - tlOrgStart;
					    llActOffSet = tlActStart - tlOrgActStart;
					}
				    }
				    else if(ilIndex != llRowCount)
				    {
					tlEnd += llOffSet;
					tlActStart += llOffSet;
					tlActEnd += llActOffSet;			     
				    }
				}
			    }

			    TimeToStr(pclPannedTime,tlStart);
			    if(CEDAArrayPutField(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,"PLFR",llFirst,pclPannedTime) != RC_SUCCESS)
			    {
				ilTimeRC = RC_FAIL;
				dbg(DEBUG,"ArrayPutField failed(Plrf %s Start %d",pclPannedTime,tlStart);
				break;
			    }
			    TimeToStr(pclPannedTime,tlEnd);
			    if(CEDAArrayPutField(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,"PLTO",llFirst,pclPannedTime) != RC_SUCCESS)
			    {
				ilTimeRC = RC_FAIL;
				dbg(DEBUG,"ArrayPutField failed(Plto %s End %d",pclPannedTime,tlEnd);
				break;
			    }
			    TimeToStr(pclActTime,tlActStart);
			    if(CEDAArrayPutField(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,"ACFR",llFirst,pclActTime) != RC_SUCCESS)
			    {
				ilTimeRC = RC_FAIL;
				dbg(DEBUG,"ArrayPutField failed(Actrf %s ActStart %d",pclActTime,tlActStart);
				break;
			    }
			    TimeToStr(pclActTime,tlActEnd);
			    if(CEDAArrayPutField(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,"ACTO",llFirst,pclActTime) != RC_SUCCESS)
			    {
				ilTimeRC = RC_FAIL;
				dbg(DEBUG,"ArrayPutField failed(Actto %s ActEnd %d",pclActTime,tlActEnd);
				break;
			    }
			}
		    }
		    llFirst = ARR_NEXT;
		}						
			
		if(ilTimeRC == RC_SUCCESS && llRowCount > 1)
		{
		    BOOL blMoveAllJobs = FALSE;
		    ilIndex = 0;
		    llFirst = ARR_FIRST;
		    while(CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
						  &(rgJobArray.crArrayName[0]),
						  &(rgJobArray.rrIdx04Handle),
						  &(rgJobArray.crIdx04Name[0]),
						  "xx",&llFirst,
						  (void *) &pclData ) == RC_SUCCESS)

		    {
			ilIndex++;
					       
			strcpy(pclStatus,JOBFIELD(pclData,igJobStat)); 
			dbg(DEBUG,"pcStatus %s, igJobStat %d",pclStatus,igJobStat);
			dbg(DEBUG," 2 Ujob <%s>",JOBFIELD(pclData,igJobUrno));

			dbg(DEBUG,"pclData %s Index %d  ",pclData,ilIndex);
			ilTimeRC = StrToTime(JOBFIELD(pclData,igJobPlfr),&tlStart);
			dbg(DEBUG,"StrToTime(JOBFIELD<-:Plrf %ld ,RC:%d  ",tlStart,ilTimeRC);
			if(ilTimeRC == RC_SUCCESS)
			{
			    ilTimeRC = StrToTime(JOBFIELD(pclData,igJobPlto),&tlEnd);
			    dbg(DEBUG,"StrToTime(JOBFIELD<-:Plto %ld ,RC:%d",tlEnd,ilTimeRC);
			}			
			if(ilTimeRC == RC_SUCCESS)
			{
			    ilTimeRC = StrToTime(JOBFIELD(pclData,igJobAcfr),&tlActStart);
			    dbg(DEBUG,"StrToTime(JOBFIELD<-:Actfr %ld ,RC:%d",tlActStart,ilTimeRC);
			}
			if(ilTimeRC == RC_SUCCESS)
			{
			    ilTimeRC = StrToTime(JOBFIELD(pclData,igJobActo),&tlActEnd);
			    dbg(DEBUG,"StrToTime(JOBFIELD<-:Actto %ld ,RC:%d",tlActEnd,ilTimeRC);
			}

			if(ilTimeRC == RC_SUCCESS)
			{
			    time_t tlOrgEnd = tlEnd;
			    time_t tlOrgActEnd = tlActEnd;
			    if(bpDeenFound)
			    {
				tlOrgEnd = tlEnd;
				tlOrgActEnd = tlActEnd;
				if(ilIndex == 1)
				{
				    if(*pclStatus == 'P')
				    {
					MoveJobTime(tlDeen,&tlEnd ,&tlStart,&tlActEnd,&tlActStart);
				    }
				    else if(*pclStatus == 'C')
				    {
					ChangeTime(tlDeen,&tlEnd ,&tlActEnd);
				    }
				    dbg(DEBUG,"MoveJobTime Deen %ld tlEnd %ld tlStart %ld tlActEnd %ld tlActStart %ld",tlDeen,tlEnd,tlStart,tlActEnd,tlActStart);
				    if(tlDeen < tlOrgEnd)
				    {
					blMoveAllJobs = TRUE;
				    }else
				    {
					llOffSet = tlEnd - tlOrgEnd;
					llActOffSet = tlActEnd - tlOrgActEnd;
				    }
				}
				else if(ilIndex != llRowCount && *pclStatus == 'P')
				{
				    if(blMoveAllJobs)
				    {
					if(tlDeen < tlEnd)
					{
					    MoveJobTime(tlDeen,&tlEnd ,&tlStart,&tlActEnd,&tlActStart);
					}
				    }
				    else
				    {
					tlStart += llOffSet;
					tlEnd += llOffSet;
					tlActStart += llOffSet;
					tlActEnd += llActOffSet;
				    }
				}
			    }

			    TimeToStr(pclPannedTime,tlStart);
			    dbg(DEBUG,"ArrayPutField Plrf %s",pclPannedTime);
			    if(CEDAArrayPutField(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,"PLFR",llFirst,pclPannedTime) != RC_SUCCESS)
			    {
				ilTimeRC = RC_FAIL;
				dbg(DEBUG,"ArrayPutField failed(Plrf %s",pclPannedTime);
				break;
			    }
			    TimeToStr(pclPannedTime,tlEnd);
			    if(CEDAArrayPutField(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,"PLTO",llFirst,pclPannedTime) != RC_SUCCESS)
			    {
				ilTimeRC = RC_FAIL;
				dbg(DEBUG,"ArrayPutField failed(Plto %s",pclPannedTime);
				break;
			    }
			    TimeToStr(pclActTime,tlActStart);
			    if(CEDAArrayPutField(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,"ACFR",llFirst,pclActTime) != RC_SUCCESS)
			    {
				ilTimeRC = RC_FAIL;
				dbg(DEBUG,"ArrayPutField failed(Actrf %s",pclActTime);
				break;
			    }
			    dbg(TRACE,"JobUrno vor PutField: <%s>, Acto <%s>",JOBFIELD(pclData,igJobUrno),JOBFIELD(pclData,igJobActo));
			    TimeToStr(pclActTime,tlActEnd);
			    dbg(DEBUG,"ArrayPutField Acto %s",pclActTime);
						
			    if(CEDAArrayPutField(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,"ACTO",llFirst,pclActTime) != RC_SUCCESS)
			    {
				ilTimeRC = RC_FAIL;
				dbg(DEBUG,"ArrayPutField failed(Actto %s",pclActTime);
				break;
			    }
			    else
			    {
				dbg(TRACE,"pclActTime: <%s>",pclActTime);
			    }
						
			    dbg(TRACE,"JobUrno nach PutField: <%s> Acto <%s>",JOBFIELD(pclData,igJobUrno),JOBFIELD(pclData,igJobActo));
			}
			llFirst = ARR_NEXT;
		    }
							
		}

		if(llRowCount > 1 && ilTimeRC == RC_SUCCESS && !blDebeIsSet && bpDebeFound)
		{
		    BOOL blMoveAllJobs = FALSE;
		    ilIndex = 0;

		    llFirst = ARR_FIRST;
		    while(CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
						  &(rgJobArray.crArrayName[0]),
						  &(rgJobArray.rrIdx03Handle),
						  &(rgJobArray.crIdx03Name[0]),
						  "xx",&llFirst,
						  (void *) &pclData ) == RC_SUCCESS)
		    {
			ilIndex++;
			strcpy(pclStatus,JOBFIELD(pclData,igJobStat)); 
			dbg(DEBUG,"pcStatus %s, igJobStat %d",pclStatus,igJobStat);

			ilTimeRC = StrToTime(JOBFIELD(pclData,igJobPlfr),&tlStart);
			if(ilTimeRC == RC_SUCCESS)
			{
			    ilTimeRC = StrToTime(JOBFIELD(pclData,igJobPlto),&tlEnd);
			}			
			if(ilTimeRC == RC_SUCCESS)
			{
			    ilTimeRC = StrToTime(JOBFIELD(pclData,igJobAcfr),&tlActStart);
			}
			if(ilTimeRC == RC_SUCCESS)
			{
			    ilTimeRC = StrToTime(JOBFIELD(pclData,igJobActo),&tlActEnd);
			}

			if(ilTimeRC == RC_SUCCESS && *pclStatus == 'P')
			{
			    time_t tlOrgStart = tlStart;
			    time_t tlOrgActStart = tlActStart;
			    if(bpDebeFound)
			    {
				tlOrgStart = tlStart;
				tlOrgActStart = tlActStart;
				if(ilIndex == 1)
				{
				    MoveJobTime(tlDebe,&tlStart,&tlEnd ,&tlActStart,&tlActEnd);
				    if(tlDebe > tlOrgStart)
				    {
					blMoveAllJobs = TRUE;
				    }else
				    {
					llOffSet = tlStart - tlOrgStart;
					llActOffSet = tlActStart - tlOrgActStart;
				    }
				}
				else if(ilIndex != llRowCount)
				{
				    if(blMoveAllJobs)
				    {
					if(tlDebe > tlStart)
					{			     
					    MoveJobTime(tlDebe,&tlStart,&tlEnd ,&tlActStart,&tlActEnd);
					}
				    }
				    else
				    {
					tlStart += llOffSet;
					tlEnd += llOffSet;
					tlActStart += llOffSet;
					tlActEnd += llActOffSet;
				    }
				}
			    }

			    TimeToStr(pclPannedTime,tlStart);
			    if(CEDAArrayPutField(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,"PLFR",llFirst,pclPannedTime) != RC_SUCCESS)
			    {
				ilTimeRC = RC_FAIL;
				break;
			    }
			    TimeToStr(pclPannedTime,tlEnd);
			    if(CEDAArrayPutField(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,"PLTO",llFirst,pclPannedTime) != RC_SUCCESS)
			    {
				ilTimeRC = RC_FAIL;
				break;
			    }
			    TimeToStr(pclActTime,tlActStart);
			    if(CEDAArrayPutField(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,"ACFR",llFirst,pclActTime) != RC_SUCCESS)
			    {
				ilTimeRC = RC_FAIL;
				break;
			    }
			    TimeToStr(pclActTime,tlActEnd);
			    if(CEDAArrayPutField(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,"ACTO",llFirst,pclActTime) != RC_SUCCESS)
			    {
				ilTimeRC = RC_FAIL;
				break;
			    }
						
			}		
			llFirst = ARR_NEXT;
		    }
		}
	    }
		
	}
	llRowNum = ARR_FIRST;
	while(CEDAArrayFindRowPointer(&(rgJodArray.rrArrayHandle),
				      &(rgJodArray.crArrayName[0]),
				      &(rgJodArray.rrIdx01Handle),
				      &(rgJodArray.crIdx01Name[0]),
				      pcpUrno,&llRowNum,
				      (void *) &pclJodRow ) == RC_SUCCESS)
	{
	    llRowNum = ARR_NEXT;
	    dbg(DEBUG,"ChangeJobTime:Ujob <%s> ",JODFIELD(pclJodRow,igJodUjob)); 
	    llFirst = ARR_FIRST;
	    if(CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
				       &(rgJobArray.crArrayName[0]),
				       &(rgJobArray.rrIdx02Handle),
				       &(rgJobArray.crIdx02Name[0]),
				       JODFIELD(pclJodRow,igJodUjob),&llFirst,
				       (void *) &pclJobRow ) == RC_SUCCESS)
	    {
		CEDAArrayPutField(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,"TXXD",llFirst," ");
	    }
	}



					
	if(ilTimeRC == RC_SUCCESS)
	{
	    ilTimeRC = CEDAArrayWriteDB(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);
	}
	if(ilTimeRC != RC_SUCCESS)
	{
	    int ilOldDebugLevel = debug_level;
	    debug_level = TRACE;
	    dbg(TRACE,"ChangeJobTime Error writing to JOBTAB ");
	    debug_level = ilOldDebugLevel;
	}


    } 
    return(ilTimeRC) ;

} /* end of ChangeJobTime */



/******************************************************************************/
/******************************************************************************/

/******************************************************************************/ 
/******************************************************************************/ 
static int TriggerAction(char *pcpTableName,char *pcpCommands) 
{ 
    int           ilRc          = RC_SUCCESS ;    /* Return code */ 
    EVENT        *prlEvent      = NULL ; 
    BC_HEAD      *prlBchead     = NULL ; 
    CMDBLK       *prlCmdblk     = NULL ; 
    /* hag 20030715  ACTIONConfig *prlAction     = NULL ;  */
	ACTIONConfig rlAction;
    char         *pclSelection  = NULL ; 
    char         *pclFields     = NULL ; 
    char         *pclData       = NULL ; 
    long          llActSize     = 0 ; 
    int           ilAnswerQueue = 0 ; 
    char          clQueueName[16] ; 
	int			 ilDebugLevel;
	 
    if(ilRc == RC_SUCCESS) 
    { 
 
    }/* end of if */ 
	 
 
    if (ilRc == RC_SUCCESS) 
    { 
	sprintf (&clQueueName[0],"%s2", mod_name) ; 
 
	ilRc = GetDynamicQueue(&ilAnswerQueue,&clQueueName[0]); 
	if (ilRc != RC_SUCCESS) 
	{ 
	    dbg(TRACE,"TriggerAction: GetDynamicQueue failed <%d>",ilRc); 
	} 
	else 
	{ 
	    dbg(DEBUG,"TriggerAction: got <%d> for <%s>",ilAnswerQueue, &clQueueName[0]); 
	}/* end of if */ 
    }/* end of if */ 
	 
    if (ilRc == RC_SUCCESS) 
    { 
	llActSize = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + sizeof(ACTIONConfig) + 666; 
	prgOutEvent = realloc(prgOutEvent,llActSize); 
	if(prgOutEvent == NULL) 
	{ 
	    dbg(TRACE,"TriggerAction: realloc out event <%d> bytes failed",llActSize); 
	    ilRc = RC_FAIL ; 
	} /* end of if */ 
    } /* end of if */ 
 
    if(ilRc == RC_SUCCESS) 
    { 
	prlBchead     = (BC_HEAD *) ((char *) prgOutEvent + sizeof(EVENT)) ; 
	prlCmdblk     = (CMDBLK *)  ((char *) prlBchead->data) ; 
	pclSelection  = prlCmdblk->data ; 
	pclFields     = pclSelection + strlen (pclSelection) + 1 ; 
	pclData       = pclFields + strlen (pclFields) + 1 ; 
	strcpy (pclData, "DYN") ; 

	memset(&rlAction,0,sizeof(ACTIONConfig));
	/*  prlAction     = (ACTIONConfig *) (pclData + strlen(pclData) + 1) ; */
		 
	prgOutEvent->type        = USR_EVENT ; 
	prgOutEvent->command     = EVENT_DATA ; 
	prgOutEvent->originator  = ilAnswerQueue ; 
	prgOutEvent->retry_count = 0 ; 
	prgOutEvent->data_offset = sizeof(EVENT) ; 
	prgOutEvent->data_length = llActSize-sizeof(EVENT) ; 
 
	rlAction.iEmptyFieldHandling = iDELETE_ALL_BLANKS ; 
	rlAction.iIgnoreEmptyFields = 0 ; 
	strcpy(rlAction.pcSndCmd, "") ; 
	sprintf(rlAction.pcSectionName,"%s_%s", mod_name,pcpTableName) ; 
	strcpy(rlAction.pcTableName, pcpTableName) ; 
	strcpy(rlAction.pcFields, "") ; 
	strcpy(rlAction.pcSectionCommands, pcpCommands) ;    
	rlAction.iModID = mod_id ;   
	rlAction.iSuspendOwn = 1;
	/**************************************** 
     DebugPrintEvent(DEBUG,prgOutEvent) ; 
     DebugPrintBchead(DEBUG,prlBchead) ; 
     DebugPrintCmdblk(DEBUG,prlCmdblk) ; 
     dbg (DEBUG,"TriggerAction: selection <%s>",pclSelection) ; 
     dbg (DEBUG,"TriggerAction: fields    <%s>",pclFields) ; 
     dbg (DEBUG,"TriggerAction: data      <%s>",pclData) ; 
     DebugPrintACTIONConfig(DEBUG,prlAction) ; 
	 DebugPrintEvent(DEBUG,prgOutEvent) ; 
	****************************************/ 
     
	if(ilRc == RC_SUCCESS) 
	{ 
	    rlAction.iADFlag = iDELETE_SECTION ; 
		dbg(DEBUG,"TriggerAction: ACTIONConfig for deletion of section" ) ; 
	     DebugPrintACTIONConfig(DEBUG,&rlAction) ; 
	    /* 20030710 HAG/JIM: avoid enlignment error by writing int to pclData */
	    memcpy(pclData + strlen(pclData) + 1,&rlAction,sizeof(ACTIONConfig));
	    ilRc = que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_2, llActSize, (char *) prgOutEvent) ;  
	    if(ilRc != RC_SUCCESS) 
	    { 
		dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRc) ; 
	    } 
	    else 
	    { 
			ilDebugLevel = debug_level;
			debug_level = 0;
			ilRc = WaitAndCheckQueue(10, ilAnswerQueue,&prgItem) ; 
			debug_level = ilDebugLevel;
			if(ilRc != RC_SUCCESS) 
			{ 
				dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRc) ; 
			} 
			else 
			{ 
				/* return value for delete does not matter !!! */ 
			 
			} /* end of if */ 
	    }/* end of if */ 
	}/* end of if */ 
 
	if(ilRc == RC_SUCCESS) 
	{ 
	    rlAction.iADFlag = iADD_SECTION ; 
		dbg(DEBUG,"TriggerAction: ACTIONConfig for addition of section" ) ; 
	    DebugPrintACTIONConfig(DEBUG,&rlAction) ; 
	    /* 20030710 HAG/JIM: avoid enlignment error by writing int to pclData */
	    memcpy(pclData + strlen(pclData) + 1,&rlAction,sizeof(ACTIONConfig));
	    ilRc = que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_2, llActSize, 
		       (char *) prgOutEvent ) ; 
	    if(ilRc != RC_SUCCESS) 
	    { 
			dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRc) ; 
	    } 
	    else 
	    { 
			ilDebugLevel = debug_level;
			debug_level = 0;
			ilRc = WaitAndCheckQueue(10,ilAnswerQueue,&prgItem) ; 
			debug_level = ilDebugLevel;
			if(ilRc != RC_SUCCESS) 
			{ 
				dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRc) ; 
			} 
			else 
			{ 
				prlEvent     = (EVENT *)   ((char *)prgItem->text) ; 
				prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT)) ; 
				prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data) ; 
				pclSelection = (char *)    prlCmdblk->data ; 
				pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1 ; 
				pclData      = (char *)    pclFields + strlen(pclFields) + 1 ; 
 
				if(strcmp(pclData,"SUCCESS") != 0) 
				{ 
					dbg(TRACE,"TriggerAction: add dynamic action config failed") ; 
					DebugPrintItem(DEBUG,prgItem) ; 
					DebugPrintEvent(DEBUG,prlEvent) ; 
					DebugPrintBchead(DEBUG,prlBchead) ; 
					DebugPrintCmdblk(DEBUG,prlCmdblk) ; 
					dbg (DEBUG,"TriggerAction: selection <%s>",pclSelection) ; 
					dbg (DEBUG,"TriggerAction: fields    <%s>",pclFields) ; 
					dbg(DEBUG,"TriggerAction: data      <%s>",pclData) ; 
					ilRc = RC_FAIL ; 
				} 
				else 
				{ 
					dbg(DEBUG,"TriggerAction: add dynamic action config OK") ; 
				}/* end of if */ 
			}/* end of if */ 
	    }/* end of if */ 
	}/* end of if */ 
 
	ilRc = que(QUE_DELETE,ilAnswerQueue,ilAnswerQueue,0,0,0) ; 
	if(ilRc != RC_SUCCESS) 
	{ 
	    dbg(TRACE,"que QUE_DELETE <%d> failed <%d>",ilAnswerQueue, ilRc) ; 
	} 
	else 
	{ 
	    dbg(DEBUG,"TriggerAction: queue <%d> <%s> deleted", ilAnswerQueue, &clQueueName[0]) ; 
	}/* end of if */ 
    }/* end of if */ 
 
    return (ilRc) ; 
	 
} /* end of TriggerAction */ 
 

static int SendAnswer(EVENT *prpEvent,int ipRc)
{
    int	      ilRc           = RC_SUCCESS;			/* Return code */
	
    BC_HEAD *prlBchead       = NULL;
    CMDBLK  *prlCmdblk       = NULL;
    char    *pclSelection    = NULL;
    char    *pclFields       = NULL;
    char    *pclData         = NULL;
	

	

    prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
    prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
    pclSelection = prlCmdblk->data;
    pclFields    = pclSelection + strlen(pclSelection) + 1;
    pclData      = pclFields + strlen(pclFields) + 1;

    prlBchead->rc = ipRc;

    dbg(TRACE,"SendAnswer to: %d",prpEvent->originator);
    if ((ilRc=SendToQue(prpEvent->originator,
			PRIORITY_4, prlBchead, prlCmdblk,
			pclSelection,pclFields,pclData)) != RC_SUCCESS)
    {
	dbg(TRACE,"%05d SendToQue returns <%d>", __LINE__, ilRc);
	dbg(DEBUG," ----- END -----");
    }
    else
    {
	dbg(TRACE,"%05d SendToQue (%d) returns <%d>", __LINE__,
	    prpEvent->originator,ilRc);
    }

    return ilRc;
}

/******************************************************************************/
/* the SendToQue routine                                                      */
/******************************************************************************/
static int SendToQue(int ipModID, int ipPrio, BC_HEAD *prpBCHead, 
		     CMDBLK *prpCmdblk, char *pcpSelection, 
		     char *pcpFields, char *pcpData)
{
    int			ilRc;
    int			ilLen;
    EVENT			*prlOutEvent 	= NULL;
    BC_HEAD		*prlOutBCHead 	= NULL;
    CMDBLK		*prlOutCmdblk	= NULL;

    dbg(DEBUG,"<SendToQue> ----- START -----");

    /* calculate size of memory we need */
    ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 
	strlen(pcpSelection) + strlen(pcpFields) + strlen(pcpData) + 20; 

    /* get memory for out event */
    if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
    {
	dbg(TRACE,"<SendToQue> %05d malloc failed, can't allocate %d bytes", __LINE__, ilLen);
	dbg(DEBUG,"<SendToQue> ----- END -----");
	return RC_FAIL;
    }

    /* clear buffer */
    memset((void*)prlOutEvent, 0x00, ilLen);

    /* set structure members */
    prlOutEvent->type 		 = SYS_EVENT;
    prlOutEvent->command 	 = EVENT_DATA;
    prlOutEvent->originator  = mod_id;
    prlOutEvent->retry_count = 0;
    prlOutEvent->data_offset = sizeof(EVENT);
    prlOutEvent->data_length = ilLen - sizeof(EVENT);

    /* BCHead members */
    prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
    memcpy(prlOutBCHead, prpBCHead, sizeof(BC_HEAD));

    /* CMDBLK members */
    prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
    memcpy(prlOutCmdblk, prpCmdblk, sizeof(CMDBLK));

    /* Selection */
    strcpy(prlOutCmdblk->data, pcpSelection);

    /* fields */
    strcpy(prlOutCmdblk->data+strlen(pcpSelection)+1, pcpFields);

    /* data */
    strcpy(prlOutCmdblk->data+strlen(pcpSelection)+strlen(pcpFields)+2, pcpData);

    /* send this message */
    dbg(DEBUG,"<SendToQue> sending to Mod-ID: %d", ipModID);
    if ((ilRc = que(QUE_PUT, ipModID, mod_id, ipPrio, ilLen, (char*)prlOutEvent)) != RC_SUCCESS)
    {
	dbg(TRACE,"<SendToQue> %05d QUE_PUT returns: %d", __LINE__, ilRc);
	return RC_FAIL;
    }

    /* delete memory */
    free((void*)prlOutEvent);

    dbg(DEBUG,"<SendToQue> ----- END -----");

    /* bye bye */
    return RC_SUCCESS;
}



/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
    /* unset SIGCHLD ! DB-Child will terminate ! */
    debug_level = DEBUG;
    dbg(TRACE,"Terminate: now DB logoff ...");

    signal(SIGCHLD,SIG_IGN);

    logoff();

    dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


    dbg(TRACE,"Terminate: now leaving ...");

    fclose(outp);

    sleep(ipSleep);
	
    exit(0);
	
} /* end of Terminate */
 

/******************************************************************************/
/******************************************************************************/
static int WaitAndCheckQueue(int ipTimeout, int ipModId, ITEM **prpItem)
{
    int ilRc          = RC_SUCCESS;                      /* Return code */
    int ilWaitCounter = 0;

    do
    {
	sleep(1);
	ilWaitCounter += 1;

	ilRc = CheckQueue(ipModId,prpItem);
	if(ilRc != RC_SUCCESS)
	{
	    if(ilRc != QUE_E_NOMSG)
	    {
		dbg(TRACE,"WaitAndCheckQueue: CheckQueue <%d> failed <%d>",mod_id,ilRc);
	    }/* end of if */
	}/* end of if */
    }while((ilRc == QUE_E_NOMSG) && (ilWaitCounter < ipTimeout));

    if(ilWaitCounter >= ipTimeout)
    {
	dbg(TRACE,"WaitAndCheckQueue: timeout reached <%d>",ipTimeout);
	ilRc = RC_FAIL;
    }/* end of if */
    return ilRc;

}/* end of WaitAndCheckQueue */
 

/******************************************************************************/
static int CheckQueue(int ipModId, ITEM **prpItem)
{
    int     ilRc       = RC_SUCCESS;                      /* Return code */
    int     ilItemSize = 0;
    EVENT *prlEvent    = NULL;
    static	ITEM  *prlItem      = NULL;

    ilItemSize = I_SIZE;
	
    ilRc = que(QUE_GETBIGNW,ipModId,ipModId,PRIORITY_3,ilItemSize,(char *) &prlItem);
    if(ilRc == RC_SUCCESS)
    {
	dbg(TRACE,"DebugPrintItem with prlItem follows");
	DebugPrintItem(TRACE,prlItem);
	*prpItem = prlItem;
	dbg(TRACE,"DebugPrintItem with prpItem follows");
	DebugPrintItem(TRACE,*prpItem);
	prlEvent = (EVENT*) ((prlItem)->text);

	switch( prlEvent->command )
	{
	    case    HSB_STANDBY :
		dbg(TRACE,"CheckQueue: HSB_STANDBY");
		HandleQueues();
		ctrl_sta = prlEvent->command;
		break;
	
	    case    HSB_COMING_UP   :
		dbg(TRACE,"CheckQueue: HSB_COMING_UP");
		ctrl_sta = prlEvent->command;
		break;
	
	    case    HSB_ACTIVE  :
		dbg(TRACE,"CheckQueue: HSB_ACTIVE");
		ctrl_sta = prlEvent->command;
		break;
	
	    case    HSB_ACT_TO_SBY  :
		dbg(TRACE,"CheckQueue: HSB_ACT_TO_SBY");
		HandleQueues();
		ctrl_sta = prlEvent->command;
		break;
	
	    case    HSB_DOWN    :
		dbg(TRACE,"CheckQueue: HSB_DOWN");
		ctrl_sta = prlEvent->command;
		/* Acknowledge the item 
		   ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
		   if( ilRc != RC_SUCCESS )
		   {
					
		   HandleQueErr(ilRc);
		   } */
		Terminate(1);
		break;
		
	    case    HSB_STANDALONE  :
		dbg(TRACE,"CheckQueue: HSB_STANDALONE");
		ctrl_sta = prlEvent->command;
		break;
	
	    case    SHUTDOWN    :
		dbg(TRACE,"CheckQueue: SHUTDOWN");
		/* Acknowledge the item */
		ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
		if( ilRc != RC_SUCCESS )
		{
		    /* handle que_ack error */
		    HandleQueErr(ilRc);
		} /* fi */
		Terminate(1);
		break;
	
	    case    RESET       :
		ilRc = Reset();
		break;
	
	    case    EVENT_DATA  :
		/* DebugPrintItem(DEBUG,prgItem); */
		/* DebugPrintEvent(DEBUG,prlEvent); */
		break;
	
	    case    TRACE_ON :
		dbg_handle_debug(prlEvent->command);
		break;
	
	    case    TRACE_OFF :
		dbg_handle_debug(prlEvent->command);
		break;


	    default         :
		dbg(TRACE,"CheckQueue: unknown event");
		DebugPrintItem(TRACE,prlItem);
		DebugPrintEvent(TRACE,prlEvent);
		break;
	
	} /* end switch */
	
	/* Acknowledge the item */
	ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
	if( ilRc != RC_SUCCESS )
	{
	    /* handle que_ack error */
	    HandleQueErr(ilRc);
	} /* fi */
    }else{
	if(ilRc != QUE_E_NOMSG)
	{
	    dbg(TRACE,"CheckQueue: que(QUE_GETBIG,%d,%d,...) failed <%d>",ipModId,ipModId,ilRc);
	    HandleQueErr(ilRc);
	}/* end of if */
    }/* end of if */
       
    return ilRc;
}/* end of CheckQueue */


static void TrimRight(char *s)
{
    int i = 0;    /* search for last non-space character */
    for (i = strlen(s) - 1; i >= 0 && isspace(s[i]); i--); /* trim off right spaces */    
    s[++i] = '\0';
}


/******************************************************************************/
/******************************************************************************/
static int SaveIndexInfo (ARRAYINFO *prpArrayInfo)
{
    int    ilRC       = RC_SUCCESS ;
    long   llRow      = 0 ;
    FILE  *prlFile    = NULL ;
    char   clDel      = ',' ;
    char   clFile[512] ;
    char  *pclTrimBuf = NULL ;
    long llRowCount;
    int ilCount = 0;
    char  *pclTestBuf = NULL ;


    dbg (DEBUG,"SaveIndexInfo: ArrayName <%s>",prpArrayInfo->crArrayName) ;

    if (ilRC == RC_SUCCESS)
    {
	sprintf (&clFile[0],"%s/%s_%s.info",getenv("DBG_PATH"),mod_name,prpArrayInfo->crArrayName) ;

	errno = 0 ;
	prlFile = fopen (&clFile[0], "w") ;
	if (prlFile == NULL)
	{
	    dbg (DEBUG, "SaveIndexInfo: fopen <%s> failed <%d-%s>", &clFile[0], errno, strerror(errno)) ;
	    ilRC = RC_FAIL ;
	} /* end of if */
    } /* end of if */

    if (ilRC == RC_SUCCESS)
    {
	{
	    dbg (DEBUG, "ArrayName      <%s>\n", prpArrayInfo->crArrayName) ;      
	    dbg (DEBUG, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList) ;  
	    dbg (DEBUG, "Idx01FieldList <%s>\n", prpArrayInfo->crIdx01FieldList) ;  
	}

	fprintf (prlFile, "ArrayName      <%s>\n", prpArrayInfo->crArrayName) ; fflush(prlFile) ;
	fprintf (prlFile, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList) ; fflush(prlFile) ;
	fprintf (prlFile, "Idx01FieldList <%s>\n", prpArrayInfo->crIdx01FieldList) ; fflush(prlFile) ;

	{
	    dbg(DEBUG,"IndexData") ; 
	}
	fprintf(prlFile,"IndexData\n"); fflush(prlFile);

	dbg(DEBUG,"FindKey <%s>",prpArrayInfo->pcrIdx01RowBuf);
	*prpArrayInfo->pcrIdx01RowBuf = '\0';
	llRow = ARR_FIRST ;
	do
	{
	    ilRC = CEDAArrayFindKey (&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), &(prpArrayInfo->rrIdx01Handle), prpArrayInfo->crIdx01Name, &llRow, prpArrayInfo->lrIdx01RowLen, prpArrayInfo->pcrIdx01RowBuf);
	    if (ilRC == RC_SUCCESS)
	    {
    
		dbg(DEBUG, "SaveIndexInfo: Key <%ld> <%s>",llRow,prpArrayInfo->pcrIdx01RowBuf) ;
    

		if (strlen (prpArrayInfo->pcrIdx01RowBuf) == 0)
		{
		    ilRC = CEDAArrayGetRow(&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), llRow, clDel, prpArrayInfo->lrArrayRowLen, prpArrayInfo->pcrArrayRowBuf);
		    if(ilRC == RC_SUCCESS)
		    {
			dbg (DEBUG, "SaveIndexInfo: Row <%ld> <%s>", llRow,prpArrayInfo->pcrArrayRowBuf) ;
		    } /* end of if */
		} /* end of if */

		pclTrimBuf = strdup (prpArrayInfo->pcrIdx01RowBuf) ;
		if (pclTrimBuf != NULL)
		{
		    ilRC = GetDataItem (pclTrimBuf,prpArrayInfo->pcrIdx01RowBuf, 1, clDel, "", "  ") ;
		    if (ilRC > 0)
		    {
			ilRC = RC_SUCCESS ;
			fprintf (prlFile,"Key            <%ld><%s>\n",llRow,pclTrimBuf) ; fflush(prlFile) ;
		    } /* end of if */

		    free (pclTrimBuf) ;
		    pclTrimBuf = NULL ;
		} /* end of if */

		llRow = ARR_NEXT;
	    }
	    else
	    {
		dbg (DEBUG, "SaveIndexInfo: CEDAArrayFindKey failed <%d>", ilRC) ;
	    } /* end of if */
	} while (ilRC == RC_SUCCESS) ;

	{
	    dbg (DEBUG,"ArrayData") ;
	}
	fprintf (prlFile,"ArrayData\n"); fflush(prlFile);

	llRow = ARR_FIRST ;

	CEDAArrayGetRowCount(&(prpArrayInfo->rrArrayHandle),&(prpArrayInfo->crArrayName[0]),&llRowCount);

	llRow = ARR_FIRST;
	dbg(DEBUG,"Array  <%s> data follows Rows %ld",
	    prpArrayInfo->crArrayName,llRowCount);
	do
	{
	    ilRC = CEDAArrayGetRowPointer(&(prpArrayInfo->rrArrayHandle),
					  &(prpArrayInfo->crArrayName[0]),llRow,(void *)&pclTestBuf);
	    if (ilRC == RC_SUCCESS)
	    {
		for( ilCount = 0; ilCount < prpArrayInfo->lrArrayFieldCnt; ilCount++)
		{
				
		    fprintf (prlFile,"<%s>",&pclTestBuf[prpArrayInfo->plrArrayFieldOfs[ilCount]]) ; 
		    fflush(prlFile) ;
		    dbg (DEBUG, "SaveIndexInfo: ilCount <%ld> <%s>", ilCount,&pclTestBuf[prpArrayInfo->plrArrayFieldOfs[ilCount]]) ;
		}
		fprintf (prlFile,"\n") ; 
		dbg (DEBUG, "\n") ;
		fflush(prlFile) ;
		llRow = ARR_NEXT;
	    }
	    else
	    {
		dbg(DEBUG,"GetRowPointer failed RC=%d",ilRC);
	    }
	} while (ilRC == RC_SUCCESS);

	if (ilRC == RC_NOTFOUND)
	{
	    ilRC = RC_SUCCESS ;
	} /* end of if */
    } /* end of if */

    if (prlFile != NULL)
    {
	fclose(prlFile) ;
	prlFile = NULL ;
  
	dbg (DEBUG, "SaveIndexInfo: event saved - file <%s>", &clFile[0]) ;
  
    } /* end of if */

    return (ilRC) ;

}/* end of SaveIndexInfo */


/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
static int KlebeFunction (ARRAYINFO *prpJobArray)
{
    int    ilRC       = RC_FAIL ;
    long   llRowNum   = ARR_FIRST ;
    char   *pclJobRow = NULL;
    char   clKey[10];
    char   clStatus[10];
    char   clJobStart[20];
    char   clJobEnd[20];
    time_t tlCurrStart, tlCurrEnd;
    long	 llDuration = 0;
    long   llJtyUrno = 0;

    BOOL blTimeIsChanged = FALSE;

    char	 clCurrTime[20];

    sprintf(clKey,"");

    dbg(TRACE,"KlebeFunction START");

    GetServerTimeStamp("UTC", 1, 0, clCurrTime);

    dbg(TRACE,"KlebeFunction clCurrTime <%s>",clCurrTime);


    StrToTime(clCurrTime,&tgCurrTime);
  
    while(CEDAArrayFindRowPointer(&(prpJobArray->rrArrayHandle),
				  &(prpJobArray->crArrayName[0]),
				  &(prpJobArray->rrIdx01Handle),
				  &(prpJobArray->crIdx01Name[0]),
				  clKey,&llRowNum,
				  (void *) &pclJobRow ) == RC_SUCCESS)
    {
	strcpy(clStatus,JOBFIELD(pclJobRow,igJobStat));
	dbg(TRACE,"KlebeFunction clStatus <%s>",clStatus);

	if(CheckAlocation("",JOBFIELD(pclJobRow,igJobUalo), TRUE))
	{
	    llJtyUrno = atol(JOBFIELD(pclJobRow,igJobUjty));
	    if(llJtyUrno != lgUjtyBrk && llJtyUrno != lgUjtyDel && llJtyUrno != lgUjtyDet)
	    {
		if(clStatus[0] == 'P')
		{
		    StrToTime(JOBFIELD(pclJobRow,igJobAcfr),&tlCurrStart);
		    StrToTime(JOBFIELD(pclJobRow,igJobActo),&tlCurrEnd);
			
		    dbg(TRACE,"KlebeFunction tlCurrStart <%ld>",tlCurrStart);
		    dbg(TRACE,"KlebeFunction tgCurrTime <%ld>",tgCurrTime);

		    if(tlCurrStart < tgCurrTime)
		    {

			llDuration = tlCurrEnd - tlCurrStart;
					  
			tlCurrStart = tgCurrTime;
			tlCurrEnd = tlCurrStart + llDuration;
					
			TimeToStr(clJobStart,tlCurrStart);
			TimeToStr(clJobEnd,tlCurrEnd);

			if(CEDAArrayPutField(&(prpJobArray->rrArrayHandle),&(prpJobArray->crArrayName[0]),NULL,"ACFR",llRowNum,clJobStart) != RC_SUCCESS)
			{
			    dbg(TRACE,"KlebeFunction CEDAArrayPutField failed ACFR <%s>",clJobStart); 
			}
			if(CEDAArrayPutField(&(prpJobArray->rrArrayHandle),&(prpJobArray->crArrayName[0]),NULL,"ACTO",llRowNum,clJobEnd) != RC_SUCCESS)
			{
			    dbg(TRACE,"KlebeFunction CEDAArrayPutField failed ACTO <%s>",clJobEnd); 
			}
			blTimeIsChanged = TRUE;
		    }
		}
		if(clStatus[0] == 'C')
		{
		    StrToTime(JOBFIELD(pclJobRow,igJobActo),&tlCurrEnd);
		    if(tlCurrEnd < tgCurrTime)
		    {
			tlCurrEnd = tgCurrTime;

			TimeToStr(clJobEnd,tlCurrEnd);

			if(CEDAArrayPutField(&(prpJobArray->rrArrayHandle),&(prpJobArray->crArrayName[0]),NULL,"ACTO",llRowNum,clJobEnd) != RC_SUCCESS)
			{
			    dbg(TRACE,"KlebeFunction CEDAArrayPutField failed ACTO <%s>",clJobEnd); 
			}
			blTimeIsChanged = TRUE;
		    }
		}
	    }		  
	}
	llRowNum   = ARR_NEXT ;
    }
    if(blTimeIsChanged)
    {
	ilRC = RC_SUCCESS;
    }
    dbg(TRACE,"KlebeFunction END");
			  
    return (ilRC) ;
}



/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

static BOOL CheckFunction(char *pcpPoolUrno,char *pcpDemUrno,int ipPrio)
{
    BOOL blIsValid = TRUE;
    int ilIndex = 1;
    long llRowNum = ARR_FIRST;
    long llRowNum2 = ARR_FIRST;
    char *pclDemPfcRow = NULL;
    char *pclPoolFctRow = NULL;

    char clKey[50];
    char clKey2[50];

    BOOL blOneOfFound = FALSE;

    for (ilIndex = 1; ilIndex <= 2; ilIndex++)
    {
	sprintf(clKey,"%s,%d",pcpDemUrno,ilIndex);
	dbg (DEBUG, "CheckFunction:  clKey <%s>",clKey) ;
	llRowNum = ARR_FIRST;
	blOneOfFound = FALSE;
	while((CEDAArrayFindRowPointer(&(rgDemPfcArray.rrArrayHandle),
				       &(rgDemPfcArray.crArrayName[0]),
				       &(rgDemPfcArray.rrIdx01Handle),
				       &(rgDemPfcArray.crIdx01Name[0]),
				       clKey,&llRowNum,
				       (void *) &pclDemPfcRow ) == RC_SUCCESS) && !blOneOfFound)
	{
	    sprintf(clKey2,"%s,%s,%d",pcpPoolUrno,DEMPFCFIELD(pclDemPfcRow,igDemPfcFcod),ipPrio);
	    llRowNum2 = ARR_FIRST;
			
	    dbg (DEBUG, "CheckFunction:  clKey2 <%s>",clKey2) ;

	    if(ilIndex == 2)
	    {
		blOneOfFound = TRUE;
		blIsValid = TRUE;
	    }
	    if(CEDAArrayFindRowPointer(&(rgPoolFctArray.rrArrayHandle),
				       &(rgPoolFctArray.crArrayName[0]),
				       &(rgPoolFctArray.rrIdx01Handle),
				       &(rgPoolFctArray.crIdx01Name[0]),
				       clKey2,&llRowNum2,
				       (void *) &pclPoolFctRow ) == RC_NOTFOUND)
	    {
		blIsValid = FALSE;
		blOneOfFound = FALSE;
	    }

			
	    llRowNum = ARR_NEXT;
	}

    }
    return blIsValid;
}


static BOOL CheckQualification(char *pcpPoolUrno,char *pcpDemUrno,int ipPrio, int ipCheckBound)
{
    BOOL blIsValid = TRUE;
    int ilIndex = 1;
    long llRowNum = ARR_FIRST;
    long llRowNum2 = ARR_FIRST;
    char *pclDemPrqRow = NULL;
    char *pclPoolPerRow = NULL;

    char clKey[50];
    char clKey2[50];

    BOOL blOneOfFound = FALSE;
    for (ilIndex = 1; ilIndex <= ipCheckBound; ilIndex++)
    {
		sprintf(clKey,"%s,%d",pcpDemUrno,ilIndex);
		dbg (DEBUG, "CheckQualifikation:  clKey <%s>",clKey) ;
		llRowNum = ARR_FIRST;
		blOneOfFound = FALSE;
		while((CEDAArrayFindRowPointer(&(rgDemPrqArray.rrArrayHandle),
						   &(rgDemPrqArray.crArrayName[0]),
						   &(rgDemPrqArray.rrIdx01Handle),
						   &(rgDemPrqArray.crIdx01Name[0]),
						   clKey,&llRowNum,
						   (void *) &pclDemPrqRow ) == RC_SUCCESS) && !blOneOfFound)
		{
			if ( igSpePrio > 0 )
				sprintf(clKey2,"%s,%s,%d",pcpPoolUrno,DEMPRQFIELD(pclDemPrqRow,igDemPrqQcod),ipPrio);
			else
				sprintf(clKey2,"%s,%s",pcpPoolUrno,DEMPRQFIELD(pclDemPrqRow,igDemPrqQcod) );

			llRowNum2 = ARR_FIRST;
				
			dbg (DEBUG, "CheckQualifikation:  clKey2 <%s>",clKey2) ;

			if(ilIndex == 2 || ilIndex == 4)
			{
				blOneOfFound = TRUE;
				blIsValid = TRUE;
			}
			if(CEDAArrayFindRowPointer(&(rgPoolPerArray.rrArrayHandle),
						   &(rgPoolPerArray.crArrayName[0]),
						   &(rgPoolPerArray.rrIdx01Handle),
						   &(rgPoolPerArray.crIdx01Name[0]),
						   clKey2,&llRowNum2,
						   (void *) &pclPoolPerRow ) == RC_NOTFOUND)
			{
				blOneOfFound = FALSE;
				blIsValid = FALSE;
				dbg (DEBUG, "CheckQualifikation:  clKey2 <%s> not found blOneOfFound <%d> blIsValid <%d>",
						    clKey2, blOneOfFound, blIsValid ) ;
			}
			else
				dbg (DEBUG, "CheckQualifikation:  clKey2 <%s> found in Row <%ld> blOneOfFound <%d> blIsValid <%d>",
						    clKey2, llRowNum2, blOneOfFound, blIsValid ) ;

			llRowNum = ARR_NEXT;
		}
    }
    return blIsValid;
}






/* Hier stehen die Auswertungsfunktionen f�r die Einteilungslogik*/

static double EvalMinFunction(double dpXVal, int lpXmax,int lpYmax)
{
    double dlRealValue = (double)lpYmax;
    double dlXmax = (double)lpXmax;
    double dlYmax = (double)lpYmax;

    dlRealValue = (dlYmax/dlXmax)*dpXVal;
    return min(dlRealValue,dlYmax);
}

static double EvalMaxFunction(double dpXVal, int lpXmax,int lpYmax)
{
    double dlRealValue = 0;
    double dlXmax = (double)lpXmax;
    double dlYmax = (double)lpYmax;

    dlRealValue = (-dlYmax/dlXmax)*dpXVal + dlYmax;

    return max(dlRealValue,0);
}

static double CalcTimeBetweenJobs(char *pcpUpol, time_t tpStartTime,time_t tpEndTime)
{
    double dlDiff = 0;
    time_t tlCurrStart = tpEndTime;
    time_t tlCurrEnd = tpStartTime;
    char *pclJobRow;
    long llRowNum = ARR_FIRST;

    while(CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
				  &(rgJobArray.crArrayName[0]),
				  &(rgJobArray.rrIdx01Handle),
				  &(rgJobArray.crIdx01Name[0]),
				  pcpUpol,&llRowNum,
				  (void *) &pclJobRow ) == RC_SUCCESS)
    {
	if (strstr(JOBFIELD(pclJobRow,igJobUjty),"2011") == NULL)
	{

	    StrToTime(JOBFIELD(pclJobRow,igJobAcfr),&tlCurrStart);

	    dlDiff += tlCurrStart - tlCurrEnd;

	    StrToTime(JOBFIELD(pclJobRow,igJobActo),&tlCurrEnd);
	}
	llRowNum = ARR_NEXT;
    }
    tlCurrStart = tpEndTime;

    dlDiff += tlCurrStart - tlCurrEnd;

    return dlDiff;
}

/*DemStart ist Begin ohne Wegezeit*/
static void CalcTimeAndWayDist(char *pcpUpol,char *pcpDemAlid, time_t tpPoolStart,time_t tpDemStart,
			       long *plpTimeDist,long *plpWayTo,int ipWtypeTo,long *plpUrnoPrevJob,long *plpUrnoNextJob)
{
    time_t tlCurrEnd = tpPoolStart;
    time_t tlOldEnd = tpPoolStart;
    int ilCurrTtgt = *plpWayTo;
    int ilOldTtgt = *plpWayTo;
    char clWayKey1[60];
    char clWayKey2[60];
    char clAlid[20] = "";
    char *pclJobRow;
    char *pclWayRow;
    char *pclWgrRow1;
    char *pclWgrRow2;
    long llRowNum = ARR_FIRST;
    long llRowNum2 = ARR_FIRST;
    long llRowNum5 = ARR_FIRST;
    long llRowNum6 = ARR_FIRST;
    long llPrevUrno = 0L;
    long llNextUrno = 0L;
	long llJobtyp = 0L;
    BOOL blStop = FALSE;

    strncpy(clAlid,pcpDemAlid,10);

    *plpTimeDist = tpDemStart - tlOldEnd;

    while((CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
				   &(rgJobArray.crArrayName[0]),
				   &(rgJobArray.rrIdx01Handle),
				   &(rgJobArray.crIdx01Name[0]),
				   pcpUpol,&llRowNum,
				   (void *) &pclJobRow ) == RC_SUCCESS) && !blStop)
    {
		StrToTime(JOBFIELD(pclJobRow,igJobActo),&tlCurrEnd);

		ilCurrTtgt = atoi(JOBFIELD(pclJobRow,igJobTtgt));
		llJobtyp = atol(JOBFIELD(pclJobRow,igJobUjty));
		
		if ( bgIgnoreBreaks && (llJobtyp == lgUjtyBrk) )
		{	
			dbg ( DEBUG,"CalcTimeAndWayDist: ignore break job <%s>", JOBFIELD(pclJobRow,igJobUrno) );
		}
		else
		{	/* if bgIgnoreBreaks and Job is a break -> don't execute this */ 

			if(tlCurrEnd - (ilCurrTtgt) <= tpDemStart)
			{
				tlOldEnd = tlCurrEnd - (ilCurrTtgt);

				llRowNum5 = ARR_FIRST;
				llRowNum6 = ARR_FIRST;
				sprintf(clWayKey1,"%s,%s,%d",JOBFIELD(pclJobRow,igJobAlid),clAlid,ipWtypeTo);
				sprintf(clWayKey2,"%s,%s,%d",clAlid,JOBFIELD(pclJobRow,igJobAlid),ipWtypeTo);

				dbg(DEBUG,"CalcTimeAndWayDist JOBAlid <%s> pcpAlid <%s>",JOBFIELD(pclJobRow,igJobAlid),clAlid);
				if((CEDAArrayFindRowPointer(&(rgWgrArray.rrArrayHandle),
							&(rgWgrArray.crArrayName[0]),
							&(rgWgrArray.rrIdx01Handle),
							&(rgWgrArray.crIdx01Name[0]),
							JOBFIELD(pclJobRow,igJobAlid),&llRowNum5,
							(void *) &pclWgrRow1 ) == RC_SUCCESS))
				{
					if(CEDAArrayFindRowPointer(&(rgWgrArray.rrArrayHandle),
								   &(rgWgrArray.crArrayName[0]),
								   &(rgWgrArray.rrIdx01Handle),
								   &(rgWgrArray.crIdx01Name[0]),
								   clAlid,&llRowNum6,
								   (void *) &pclWgrRow2 ) == RC_SUCCESS)
					{
						sprintf(clWayKey1,"%s,%s,%d",WGRFIELD(pclWgrRow1,igWgrAlgr),
							WGRFIELD(pclWgrRow2,igWgrAlgr),ipWtypeTo);
						sprintf(clWayKey2,"%s,%s,%d",WGRFIELD(pclWgrRow2,igWgrAlgr),
							WGRFIELD(pclWgrRow1,igWgrAlgr),ipWtypeTo);
						dbg(DEBUG,"CalcTimeAndWayDist WGR found ");

					}
				}

				llRowNum2 = ARR_FIRST;
				if(CEDAArrayFindRowPointer(&(rgWayArray.rrArrayHandle),
							   &(rgWayArray.crArrayName[0]),
							   &(rgWayArray.rrIdx01Handle),
							   &(rgWayArray.crIdx01Name[0]),
							   clWayKey1,&llRowNum2,
							   (void *) &pclWayRow ) == RC_SUCCESS)
				{
					ilOldTtgt = atol(WAYFIELD(pclWayRow,igWayTtgo))* 60;
				}
				else
				{
					llRowNum2 = ARR_FIRST;
					if(CEDAArrayFindRowPointer(&(rgWayArray.rrArrayHandle),
								   &(rgWayArray.crArrayName[0]),
								   &(rgWayArray.rrIdx01Handle),
								   &(rgWayArray.crIdx01Name[0]),
								   clWayKey2,&llRowNum2,
								   (void *) &pclWayRow ) == RC_SUCCESS)
					{
						ilOldTtgt = atol(WAYFIELD(pclWayRow,igWayTtgo))* 60;
					}
					else
					{
						dbg (DEBUG, "CalcTimeAndWayDist:  WayKey not found clWayKey <%s>",clWayKey1) ;
						ilOldTtgt = *plpWayTo;
					}
				}

				llPrevUrno = atol(JOBFIELD(pclJobRow,igJobUrno));
			}
			else
			{
				llNextUrno = atol(JOBFIELD(pclJobRow,igJobUrno));
				blStop = TRUE;
			}
		}
		*plpTimeDist = min(*plpTimeDist,(tpDemStart - tlOldEnd));
		llRowNum = ARR_NEXT;
    }

    *plpUrnoPrevJob = llPrevUrno;
    *plpUrnoNextJob = llNextUrno;
    *plpWayTo = ilOldTtgt;
	

}




	
BOOL UngarischerAlgorithmus(ARRAYINFO *prpMatchArray,int ipDemCount)
{
    BOOL blIsValid = TRUE;
    BOOL blBreak = FALSE;
    long llRowNum = ARR_FIRST;
    long llRowNum2 = ARR_FIRST;
    char * pclMatchRow = NULL;
    char * pclMatchRow2 = NULL;
    char * pclUrnoUrnoRow = NULL;
    char clKey[30];
    int ilMatchCount = 0, ilCount;
	char clDpri[6], clJpri[6], clPrio[10];

    dbg(TRACE,"UngarischerAlgorithmus Start ipDemCount <%d>",ipDemCount);

    
	/*  Initialize DPRI with the count of occurances of this demand */
	llRowNum = ARR_FIRST;
	while(AATArrayFindRowPointer(&(prpMatchArray->rrArrayHandle),
									 &(prpMatchArray->crArrayName[0]),
									 &(prpMatchArray->rrIdx01Handle),
									 &(prpMatchArray->crIdx01Name[0]),
									 "",&llRowNum, (void *)&pclMatchRow ) == RC_SUCCESS)
	{
		strcpy ( clDpri, MATCHFIELD(pclMatchRow,igMatchDpri) );
		if ( atoi(clDpri) <= 0 )		/* DPRI not initialized */
		{
			sprintf ( clKey, "0,%s", MATCHFIELD(pclMatchRow,igMatchUdem) );
			ilCount = 0;
		    llRowNum = ARR_FIRST;
			while(AATArrayFindRowPointer(&(prpMatchArray->rrArrayHandle),
										 &(prpMatchArray->crArrayName[0]),
									     &(prpMatchArray->rrIdx01Handle),
									     &(prpMatchArray->crIdx01Name[0]),
										 clKey,&llRowNum, (void *)&pclMatchRow ) == RC_SUCCESS)
			{
				ilCount++;
				llRowNum = ARR_NEXT;
			}
			dbg ( DEBUG, "UngarischerAlgorithmus: Found <%d> entries with DEM-Key <%s>",
				 ilCount, clKey );
			llRowNum = ARR_FIRST;
			sprintf ( clDpri, "%05d", ilCount );
			while(AATArrayFindRowPointer(&(prpMatchArray->rrArrayHandle),
										 &(prpMatchArray->crArrayName[0]),
									     &(prpMatchArray->rrIdx01Handle),
									     &(prpMatchArray->crIdx01Name[0]),
										 clKey,&llRowNum, (void *)&pclMatchRow ) == RC_SUCCESS)
			{
				AATArrayPutField(&(prpMatchArray->rrArrayHandle),
								&(prpMatchArray->crArrayName[0]),NULL,"DPRI",llRowNum,TRUE,clDpri) ;
				llRowNum = ARR_NEXT;
			}
		}
		llRowNum = ARR_NEXT;
	}

	/*  Initialize JPRI with the count of occurances of this pooljob */
    llRowNum = ARR_FIRST;
	while(AATArrayFindRowPointer(&(prpMatchArray->rrArrayHandle),
									 &(prpMatchArray->crArrayName[0]),
									 &(prpMatchArray->rrIdx02Handle),
									 &(prpMatchArray->crIdx02Name[0]),
									 "",&llRowNum, (void *)&pclMatchRow ) == RC_SUCCESS)
	{
		strcpy ( clJpri, MATCHFIELD(pclMatchRow,igMatchJpri) );
		if ( atoi(clJpri) <= 0 )		/* JPRI not initialized */
		{
			sprintf ( clKey, "0,%s", MATCHFIELD(pclMatchRow,igMatchUpjb) );
			ilCount = 0;
		    llRowNum = ARR_FIRST;
			while(AATArrayFindRowPointer(&(prpMatchArray->rrArrayHandle),
										 &(prpMatchArray->crArrayName[0]),
									     &(prpMatchArray->rrIdx02Handle),
									     &(prpMatchArray->crIdx02Name[0]),
										 clKey,&llRowNum, (void *)&pclMatchRow ) == RC_SUCCESS)
			{
				ilCount++;
				llRowNum = ARR_NEXT;
			}
			dbg ( DEBUG, "UngarischerAlgorithmus: Found <%d> entries with PJB-Key <%s>",
				 ilCount, clKey );
			llRowNum = ARR_FIRST;
			sprintf ( clJpri, "%05d", ilCount );
			while(AATArrayFindRowPointer(&(prpMatchArray->rrArrayHandle),
										 &(prpMatchArray->crArrayName[0]),
									     &(prpMatchArray->rrIdx02Handle),
									     &(prpMatchArray->crIdx02Name[0]),
										 clKey,&llRowNum, (void *)&pclMatchRow ) == RC_SUCCESS)
			{
				AATArrayPutField(&(prpMatchArray->rrArrayHandle),
								&(prpMatchArray->crArrayName[0]),NULL,"JPRI",llRowNum,TRUE,clJpri) ;
				llRowNum = ARR_NEXT;
			}
		}
		llRowNum = ARR_NEXT;
	}

	/*  Initialize PRIO with the DPRI * JPRI */
    llRowNum = ARR_FIRST;
	while(AATArrayFindRowPointer(&(prpMatchArray->rrArrayHandle),
									 &(prpMatchArray->crArrayName[0]),
									 &(prpMatchArray->rrIdx01Handle),
									 &(prpMatchArray->crIdx01Name[0]),
									 "",&llRowNum, (void *)&pclMatchRow ) == RC_SUCCESS)
	{
		strcpy ( clDpri, MATCHFIELD(pclMatchRow,igMatchDpri) );
		strcpy ( clJpri, MATCHFIELD(pclMatchRow,igMatchJpri) );
		ilCount = atoi(clDpri) * atoi(clJpri);
		sprintf( clPrio, "%05d", ilCount );
		clPrio[6] = '\0';
		AATArrayPutField(&(prpMatchArray->rrArrayHandle),
						&(prpMatchArray->crArrayName[0]),NULL,"PRIO",llRowNum,TRUE,clPrio) ;
		llRowNum = ARR_NEXT;
	}

    llRowNum = ARR_FIRST;
	if ( debug_level > TRACE )
	{
		while(AATArrayFindRowPointer(&(prpMatchArray->rrArrayHandle),
									 &(prpMatchArray->crArrayName[0]),
									 &(prpMatchArray->rrIdx01Handle),
									 &(prpMatchArray->crIdx01Name[0]),
									 "",&llRowNum, (void *)&pclMatchRow ) == RC_SUCCESS)
		{
			dbg(DEBUG,"UngarischerAlgorithmus: Initial Data Row <%ld> UPJB <%s>  UDEM <%s> DPRI <%s> JPRI <%s> PRIO <%s>",
				llRowNum, MATCHFIELD(pclMatchRow,igMatchUpjb), MATCHFIELD(pclMatchRow,igMatchUdem),
				MATCHFIELD(pclMatchRow,igMatchDpri), MATCHFIELD(pclMatchRow,igMatchJpri), 
				MATCHFIELD(pclMatchRow,igMatchPrio) );
			llRowNum = ARR_NEXT;
		}
	}

    
	CEDAArrayDelete(&(rgUrnoUrnoArray.rrArrayHandle),&(rgUrnoUrnoArray.crArrayName[0]));
    /***Initialisierung des Matcharrays Beginn***/


    llRowNum = ARR_FIRST;
    while(AATArrayFindRowPointer(&(prpMatchArray->rrArrayHandle),
				 &(prpMatchArray->crArrayName[0]),
				 &(prpMatchArray->rrIdx04Handle),
				 &(prpMatchArray->crIdx04Name[0]),
				 "",&llRowNum,
				 (void *) &pclMatchRow ) == RC_SUCCESS)
    {
		dbg(TRACE,"UngarischerAlgorithmus MATCHFIELD(pclMatchRow,igMatchUpjb) <%s>",
			MATCHFIELD(pclMatchRow,igMatchUpjb));

		llRowNum2 = ARR_FIRST;
		if(AATArrayFindRowPointer(&(rgUrnoUrnoArray.rrArrayHandle),
					  &(rgUrnoUrnoArray.crArrayName[0]),
					  &(rgUrnoUrnoArray.rrIdx01Handle),
					  &(rgUrnoUrnoArray.crIdx01Name[0]),
					  MATCHFIELD(pclMatchRow,igMatchUpjb),&llRowNum2,
					  (void *) &pclUrnoUrnoRow ) == RC_SUCCESS)
		{
			dbg(TRACE,"UngarischerAlgorithmus URN1 <%s> URN2 <%s>",
			URNOURNOFIELD(pclUrnoUrnoRow,igUrnoUrnoUrn1),URNOURNOFIELD(pclUrnoUrnoRow,igUrnoUrnoUrn2));


			AATArrayPutField(&(prpMatchArray->rrArrayHandle),
					 &(prpMatchArray->crArrayName[0]),NULL,"STAT",llRowNum,TRUE,"-1") ;
			AATArrayPutField(&(prpMatchArray->rrArrayHandle),
					 &(prpMatchArray->crArrayName[0]),NULL,"TSTA",llRowNum,TRUE,"-1") ;
			AATArrayPutField(&(prpMatchArray->rrArrayHandle),
					 &(prpMatchArray->crArrayName[0]),NULL,"MARK",llRowNum,TRUE,"0") ;
		}
		else
		{
			llRowNum2 = ARR_FIRST;

			dbg(TRACE,"UngarischerAlgorithmus MATCHFIELD(pclMatchRow,igMatchUdem) <%s>",MATCHFIELD(pclMatchRow,igMatchUdem));

			if(AATArrayFindRowPointer(&(rgUrnoUrnoArray.rrArrayHandle),
						  &(rgUrnoUrnoArray.crArrayName[0]),
						  &(rgUrnoUrnoArray.rrIdx02Handle),
						  &(rgUrnoUrnoArray.crIdx02Name[0]),
						  MATCHFIELD(pclMatchRow,igMatchUdem),&llRowNum2,
						  (void *) &pclUrnoUrnoRow ) == RC_SUCCESS)
			{
				dbg(TRACE,"UngarischerAlgorithmus URN1 <%s> URN2 <%s>",
					URNOURNOFIELD(pclUrnoUrnoRow,igUrnoUrnoUrn1),URNOURNOFIELD(pclUrnoUrnoRow,igUrnoUrnoUrn2));

				AATArrayPutField(&(prpMatchArray->rrArrayHandle),
						 &(prpMatchArray->crArrayName[0]),NULL,"STAT",llRowNum,TRUE,"-1") ;
				AATArrayPutField(&(prpMatchArray->rrArrayHandle),
						 &(prpMatchArray->crArrayName[0]),NULL,"TSTA",llRowNum,TRUE,"-1") ;
				AATArrayPutField(&(prpMatchArray->rrArrayHandle),
						 &(prpMatchArray->crArrayName[0]),NULL,"MARK",llRowNum,TRUE,"1") ;

			}
			else
			{

				dbg(TRACE,"UngarischerAlgorithmus NewUrnoUrnoRow");
				AATArrayPutField(&(prpMatchArray->rrArrayHandle),
						 &(prpMatchArray->crArrayName[0]),NULL,"STAT",llRowNum,TRUE,"1") ;
				AATArrayPutField(&(prpMatchArray->rrArrayHandle),
						 &(prpMatchArray->crArrayName[0]),NULL,"TSTA",llRowNum,TRUE,"1") ;
				AATArrayPutField(&(prpMatchArray->rrArrayHandle),
						 &(prpMatchArray->crArrayName[0]),NULL,"MARK",llRowNum,TRUE,"0") ;

				InitializeUrnoUrnoRow(cgUrnoUrnoBuf);
				AATArrayAddRow(&(rgUrnoUrnoArray.rrArrayHandle),&(rgUrnoUrnoArray.crArrayName[0]),&llRowNum2,(void *)cgUrnoUrnoBuf);
				AATArrayPutField(&(rgUrnoUrnoArray.rrArrayHandle),
						 &(rgUrnoUrnoArray.crArrayName[0]),NULL,"URN1",llRowNum2,TRUE,MATCHFIELD(pclMatchRow,igMatchUpjb));
				AATArrayPutField(&(rgUrnoUrnoArray.rrArrayHandle),
						 &(rgUrnoUrnoArray.crArrayName[0]),NULL,"URN2",llRowNum2,TRUE,MATCHFIELD(pclMatchRow,igMatchUdem));
					
			}
		}

		llRowNum = ARR_NEXT;
    }


    /***Initialisierung des Matcharrays Ende***/



    llRowNum = ARR_FIRST;
	
/**********************/

    while(AATArrayGetRowPointer(&(rgUrnoUrnoArray.rrArrayHandle),
				&(rgUrnoUrnoArray.crArrayName[0]),
				llRowNum,
				(void *) &pclUrnoUrnoRow ) == RC_SUCCESS)
    {
		llRowNum2 = ARR_FIRST;
			
		sprintf(clKey,"-1,%s",URNOURNOFIELD(pclUrnoUrnoRow,igUrnoUrnoUrn1));

		dbg(DEBUG,"UngarischerAlgorithmus clKey <%s>",clKey);

		while(AATArrayFindRowPointer(&(prpMatchArray->rrArrayHandle),
						 &(prpMatchArray->crArrayName[0]),
						 &(prpMatchArray->rrIdx02Handle),
						 &(prpMatchArray->crIdx02Name[0]),
						 clKey,&llRowNum2,
						 (void *) &pclMatchRow ) == RC_SUCCESS)
		{
			AATArrayPutField(&(prpMatchArray->rrArrayHandle),
					 &(prpMatchArray->crArrayName[0]),NULL,"MARK",llRowNum2,TRUE,"0") ;

			llRowNum2 = ARR_NEXT;
				
		}
		llRowNum = ARR_NEXT;
    }


    llRowNum = ARR_FIRST;
	/*  f�r alle MA, die noch nichts tun, aber eine Bedarf abdecken k�nnten */
	while(AATArrayFindRowPointer(&(prpMatchArray->rrArrayHandle),
				 &(prpMatchArray->crArrayName[0]),
				 &(prpMatchArray->rrIdx03Handle),
				 &(prpMatchArray->crIdx03Name[0]),
				 "1",&llRowNum,
				 (void *) &pclMatchRow ) == RC_SUCCESS)
    {
		AATArrayPutField(&(prpMatchArray->rrArrayHandle),
				 &(prpMatchArray->crArrayName[0]),NULL,"MARK",llRowNum,TRUE,"0") ;
		AATArrayPutField(&(prpMatchArray->rrArrayHandle),
				 &(prpMatchArray->crArrayName[0]),NULL,"TSTA",llRowNum,TRUE,"1") ;
		dbg ( DEBUG, "Ungar1: Row <%ld> MARK <0> TSTA<1>", llRowNum );
		sprintf(clKey,"1,%s",MATCHFIELD(pclMatchRow,igMatchUpjb));

		blBreak = FALSE;
			
		llRowNum = ARR_FIRST;
		/*  sicherstellen, da� der MA nicht schon etwas anderes macht */
		if(AATArrayFindRowPointer(&(prpMatchArray->rrArrayHandle),
					  &(prpMatchArray->crArrayName[0]),
					  &(prpMatchArray->rrIdx02Handle),
					  &(prpMatchArray->crIdx02Name[0]),
					  clKey,&llRowNum,
					  (void *) &pclMatchRow ) == RC_NOTFOUND)
		{	
				
			llRowNum = ARR_FIRST;
			sprintf(clKey,"1,%s",MATCHFIELD(pclMatchRow,igMatchUdem));
			/*  suche den MA2, der z. Z. f�r den Bedarf eingeteilt ist */
			if(AATArrayFindRowPointer(&(prpMatchArray->rrArrayHandle),
						  &(prpMatchArray->crArrayName[0]),
						  &(prpMatchArray->rrIdx01Handle),
						  &(prpMatchArray->crIdx01Name[0]),
						  clKey,&llRowNum,
						  (void *) &pclMatchRow ) == RC_SUCCESS)
			{
				AATArrayPutField(&(prpMatchArray->rrArrayHandle),
						 &(prpMatchArray->crArrayName[0]),NULL,"TSTA",llRowNum,TRUE,"-1") ;
				dbg ( DEBUG, "Ungar2: Row <%ld> TSTA<-1>", llRowNum );
				llRowNum = ARR_FIRST;
				sprintf(clKey,"-1,%s",MATCHFIELD(pclMatchRow,igMatchUpjb));
				/*  gibt es einen Bedarf den der gefundene MA2 stattdessen abdecken kann */
				if(AATArrayFindRowPointer(&(prpMatchArray->rrArrayHandle),
										  &(prpMatchArray->crArrayName[0]),
										  &(prpMatchArray->rrIdx02Handle),
										  &(prpMatchArray->crIdx02Name[0]),
										  clKey,&llRowNum,
										  (void *) &pclMatchRow ) == RC_SUCCESS)
				{
					/*  �berpr�fe, ob der gefundene Bedarf auch offen ist */
					sprintf(clKey,"1,%s",MATCHFIELD(pclMatchRow,igMatchUdem));
					llRowNum2 = ARR_FIRST;
					if(AATArrayFindRowPointer(&(prpMatchArray->rrArrayHandle),
								  &(prpMatchArray->crArrayName[0]),
								  &(prpMatchArray->rrIdx01Handle),
								  &(prpMatchArray->crIdx01Name[0]),
								  clKey, &llRowNum2, (void *) &pclMatchRow2) == RC_NOTFOUND )
					{
						AATArrayPutField(&(prpMatchArray->rrArrayHandle),
								 &(prpMatchArray->crArrayName[0]),NULL,"TSTA",llRowNum,TRUE,"1") ;
						dbg ( DEBUG, "Ungar3: Row <%ld> TSTA<1>", llRowNum );
						blBreak = TRUE;
					}
				}
			}
			else
			{
				blBreak = TRUE;
			}
		}
		llRowNum = ARR_FIRST;
		while(AATArrayGetRowPointer(&(prpMatchArray->rrArrayHandle),
						&(prpMatchArray->crArrayName[0]),
						llRowNum, (void *) &pclMatchRow ) == RC_SUCCESS)
		/*while(AATArrayFindRowPointer(&(prpMatchArray->rrArrayHandle),
									prpMatchArray->crArrayName,
									&(prpMatchArray->rrIdx02Handle),
									prpMatchArray->crIdx02Name, "", &llRowNum,
									(void *) &pclMatchRow ) == RC_SUCCESS)*/
		{
			if(blBreak)
			{
				AATArrayPutField(&(prpMatchArray->rrArrayHandle),
								 &(prpMatchArray->crArrayName[0]),NULL,"STAT",ARR_CURRENT, 
								 TRUE,MATCHFIELD(pclMatchRow,igMatchTsta)) ;
				dbg ( DEBUG, "Ungar4: UPJB <%s> UDEM <%s>  STAT<%s>", MATCHFIELD(pclMatchRow,igMatchUpjb), 
							  MATCHFIELD(pclMatchRow,igMatchUdem), MATCHFIELD(pclMatchRow,igMatchTsta) );
			}
			else
			{
				AATArrayPutField(&(prpMatchArray->rrArrayHandle),
								 &(prpMatchArray->crArrayName[0]),NULL,"TSTA",ARR_CURRENT,
								 TRUE,MATCHFIELD(pclMatchRow,igMatchStat)) ;
				dbg ( DEBUG, "Ungar5: UPJB <%s> UDEM <%s>  TSTA<%s>", MATCHFIELD(pclMatchRow,igMatchUpjb), 
							  MATCHFIELD(pclMatchRow,igMatchUdem), MATCHFIELD(pclMatchRow,igMatchStat) );
			}
			llRowNum = ARR_NEXT;
		}
			
		llRowNum = ARR_FIRST;

    }

    llRowNum = ARR_FIRST;
    while(AATArrayFindRowPointer(&(prpMatchArray->rrArrayHandle),
				 &(prpMatchArray->crArrayName[0]),
				 &(prpMatchArray->rrIdx01Handle),
				 &(prpMatchArray->crIdx01Name[0]),
				 "1",&llRowNum,
				 (void *) &pclMatchRow ) == RC_SUCCESS)
    {
		ilMatchCount++;
		dbg ( DEBUG, "UngarischerAlgorithmus: Matcharray Status <1> UPJB <%s> UDEM <%s>",
			  MATCHFIELD(pclMatchRow,igMatchUpjb), MATCHFIELD(pclMatchRow,igMatchUdem) );
		llRowNum = ARR_NEXT;
    }
    if(ilMatchCount == ipDemCount)
    {
		blIsValid = TRUE;
    }
    else
    {
		blIsValid = FALSE;
    }

    dbg(TRACE,"UngarischerAlgorithmus End blIsValid <%d>",blIsValid);

    return 	blIsValid;
	
}


void MyStrnCpy ( char *pcpDest, const char *pcpSource, size_t ipCount, int ipTrimRight )
{
    strncpy ( pcpDest, pcpSource, ipCount );
    pcpDest[ipCount]='\0';
    if ( ipTrimRight )
	TrimRight ( pcpDest );
}


BOOL CheckAlocation(char *pcpAloc,char *pcpUalo, BOOL bpUseUrno)
{
    char *pclAloRow = NULL;
    char *pclParRow = NULL;
    long llRowNum = ARR_FIRST;
    char clKey[40];
    BOOL blIsValid = TRUE;
    if(bgCheckAlocation)
    {
	blIsValid = FALSE;
	if(bpUseUrno)
	{
	    llRowNum = ARR_FIRST;
	    if (CEDAArrayFindRowPointer(&(rgAloArray.rrArrayHandle),
					&(rgAloArray.crArrayName[0]),
					&(rgAloArray.rrIdx01Handle),
					&(rgAloArray.crIdx01Name[0]),
					pcpUalo,&llRowNum,
					(void *) &pclAloRow ) == RC_SUCCESS)
	    {
		sprintf(clKey,"ALOC,%s,1",ALOFIELD(pclAloRow,igAloAloc));
		blIsValid = TRUE;
	    }
	}
	else
	{
	    sprintf(clKey,"ALOC,%s,1",pcpAloc);
	    blIsValid = TRUE;
	}
	if(blIsValid)
	{
	    blIsValid = FALSE;
	    dbg(TRACE,"CheckAlocation clKey, <%s>",clKey);
	    llRowNum = ARR_FIRST;
	    if(CEDAArrayFindRowPointer(&(rgParArray.rrArrayHandle),
				       &(rgParArray.crArrayName[0]),
				       &(rgParArray.rrIdx01Handle),
				       &(rgParArray.crIdx01Name[0]),
				       clKey,&llRowNum,
				       (void *) &pclParRow ) == RC_SUCCESS)
	    {							
		blIsValid = TRUE;
	    }
	}
    }
    dbg(TRACE,"CheckAlocation End blIsValid <%d>",blIsValid);

    return blIsValid;
}

static int GetDebugLevel(char *pcpMode, int *pipMode)
{
    int		ilRC = RC_SUCCESS;
    char	clCfgValue[64];

    ilRC = iGetConfigRow(cgConfigFile, "SYSTEM", pcpMode, CFG_STRING,
			 clCfgValue);

    if(ilRC != RC_SUCCESS)
    {
	dbg(TRACE,"SYSTEM %s not found in <%s>", pcpMode, cgConfigFile);
    }
    else
    {
	dbg(TRACE, "SYSTEM %s <%s>", pcpMode, clCfgValue);
	if (!strcmp(clCfgValue, "DEBUG"))
	    *pipMode = DEBUG;
	else if (!strcmp(clCfgValue, "TRACE"))
	    *pipMode = TRACE;
	else
	    *pipMode = 0;
    }

    return ilRC;
}


/*  decide whether a demand can be assigned automatically, i. e.
    Flag "Demand invalid" is not set and
    Flag "No Automatic assignment" is not activated and
    Falg "Demand deactivated" is not activated			*/
static BOOL IsDemandToAssign ( char * pcpDemFlgs )
{
    int ilLen;
    if ( !pcpDemFlgs )
	return FALSE;
    ilLen = strlen (pcpDemFlgs);
    if ( (ilLen>1) && pcpDemFlgs[1]=='1' )
	return FALSE;				/* Demand invalid */
    if ( (ilLen>5) && pcpDemFlgs[5]=='1' )
	return FALSE;				/* No Automatic assignment */
    if ( (ilLen>7) && pcpDemFlgs[7]=='1' )
	return FALSE;				/* Demand deactivated */
    return TRUE;
}


static int ReassignBreak ( char *pcpPoolJobRow, char *pcpBreakJobRow, long lpRow )
{
	char	clCurrTime[20], clDrrUrno[21], clStart[21], clEnd[21], clJour[21];
	time_t  tlOriBrkStart, tlOriBrkEnd, tlBrkIntStart, tlBrkIntEnd ;
	int		ilBreaklength;
    time_t	tlBreakStart, tlBreakEnd, tlJobStart,tlJobEnd,tlCurrTime;
	time_t	tlLastStartOk = (time_t)-1;
	long	llRowNum = ARR_FIRST;
	char	*pclJobRow=0;
	int		ilRC = RC_FAIL;
	BOOL	blFound=FALSE;

	if ( !pcpPoolJobRow || !pcpBreakJobRow )
		return RC_FAIL;

	strcpy ( clDrrUrno, POOLJOBFIELD(pcpPoolJobRow,igJobUdsr) );
	strcpy ( clJour, POOLJOBFIELD(pcpPoolJobRow,igJobUrno) );
	StrToTime(JOBFIELD(pcpBreakJobRow,igJobAcfr),&tlOriBrkStart);
	StrToTime(JOBFIELD(pcpBreakJobRow,igJobActo),&tlOriBrkEnd);
	ilBreaklength = tlOriBrkEnd - tlOriBrkStart;

	dbg ( TRACE, "ReassignBreak: Break Start <%s> End <%s> USTF <%s>",
		  JOBFIELD(pcpBreakJobRow,igJobAcfr), JOBFIELD(pcpBreakJobRow,igJobActo), 
		  JOBFIELD(pcpBreakJobRow,igJobUstf) );

	if ( GetBreakInterval ( clDrrUrno, &tlBrkIntStart, &tlBrkIntEnd ) == RC_SUCCESS )
	{
		GetServerTimeStamp("UTC", 1, 0, clCurrTime);
		StrToTime(clCurrTime,&tlCurrTime);

		/*  BreakBegin = max ( Pausenlage, now ) */
		tlBreakStart = max ( tlCurrTime, tlBrkIntStart );
		tlBreakEnd = tlBreakStart + ilBreaklength;
		
		while( CEDAArrayFindRowPointer( &(rgJobArray.rrArrayHandle),
										&(rgJobArray.crArrayName[0]),
										&(rgJobArray.rrIdx01Handle),
										&(rgJobArray.crIdx01Name[0]),
										clJour, &llRowNum,(void *)&pclJobRow) 
				== RC_SUCCESS)
		{
			if ( llRowNum != lpRow )
			{
				StrToTime(JOBFIELD(pclJobRow,igJobAcfr),&tlJobStart);
				StrToTime(JOBFIELD(pclJobRow,igJobActo),&tlJobEnd);
				
				if ( tlJobEnd<=tlBreakStart )
				{
					dbg ( DEBUG, "ReassignBreak: Job from <%s> to <%s> USTF <%s> before break",
								  JOBFIELD(pclJobRow,igJobAcfr), JOBFIELD(pclJobRow,igJobActo), 
								  JOBFIELD(pclJobRow,igJobUstf) );
					llRowNum = ARR_NEXT;
					continue;	/* job ends before actual break start, not interesting */
				}
				if ( tlJobStart>=tlBrkIntEnd )
				{
					dbg ( DEBUG, "ReassignBreak: Job from <%s> to <%s> USTF <%s> outside break interval",
								  JOBFIELD(pclJobRow,igJobAcfr), JOBFIELD(pclJobRow,igJobActo), 
								  JOBFIELD(pclJobRow,igJobUstf) );
					break;		/* job outside break interval, investigation finished */
				}
				if ( tlJobStart>=tlBreakEnd )
				{
					if ( tlBreakStart < tlOriBrkStart )
						/*  if we are before the original break, move break directly before
							the non-overlapping job */
						tlLastStartOk = tlJobStart - ilBreaklength;
					else
						tlLastStartOk = tlBreakStart;
					if ( tlLastStartOk > tlOriBrkStart )
					{
						dbg ( DEBUG, "ReassignBreak: After job from <%s> to <%s> USTF <%s> break behind original break found",
									  JOBFIELD(pclJobRow,igJobAcfr), JOBFIELD(pclJobRow,igJobActo), 
								  JOBFIELD(pclJobRow,igJobUstf) );
						break;		/*  we are already after the original break -> stop */
					}
				}
				tlBreakStart = tlJobEnd;
				tlBreakEnd = tlBreakStart + ilBreaklength;
				if ( tlBreakEnd > tlBrkIntEnd )
				{
					dbg ( DEBUG, "ReassignBreak: After job from <%s> to <%s> USTF <%s> end of break interval reached",
								  JOBFIELD(pclJobRow,igJobAcfr), JOBFIELD(pclJobRow,igJobActo), 
								  JOBFIELD(pclJobRow,igJobUstf) );
					break;
				}
			}
			llRowNum = ARR_NEXT;
		}	
		if ( tlBreakEnd <= tlBrkIntEnd ) /*  after the last job but inside break interval is ok */
			tlLastStartOk = tlBreakStart;

	}
	/*  reset current key to actual record */
	llRowNum = ARR_FIRST;
	while( !blFound &&
		   ( CEDAArrayFindRowPointer( &(rgJobArray.rrArrayHandle),
										&(rgJobArray.crArrayName[0]),
										&(rgJobArray.rrIdx01Handle),
										&(rgJobArray.crIdx01Name[0]),
										clJour, &llRowNum,(void *)&pclJobRow) 
				== RC_SUCCESS) )
	{
		if (llRowNum == lpRow )
			blFound = TRUE;
		else
			llRowNum = ARR_NEXT;
	}
	if ( blFound )
	{
		if ( tlLastStartOk != (time_t)-1 )
		{
			TimeToStr ( clStart, tlLastStartOk );
			TimeToStr ( clEnd, tlLastStartOk + ilBreaklength );
			dbg ( TRACE, "ReassignBreak: Break Moved to Start <%s> End <%s>",
				  clStart, clEnd );
			ilRC = CEDAArrayPutField( &(rgJobArray.rrArrayHandle),
									  &(rgJobArray.crArrayName[0]),
									  NULL,"ACTO",lpRow,clEnd ) ;
			ilRC |= CEDAArrayPutField( &(rgJobArray.rrArrayHandle),
									  &(rgJobArray.crArrayName[0]),
									  NULL,"ACFR",lpRow,clStart ) ;
			if ( ilRC != RC_SUCCESS )
				dbg ( TRACE, "ReassignBreak: Set new times failed RC <%d>", ilRC );
		}
	}
	else 
		dbg ( TRACE, "ReassignBreak: Couldn't find Job Row <%d>", lpRow );
	return ilRC;
}

static int GetBreakInterval ( char *pcpDrrUrno, time_t *ptpBrkIntStart, time_t *ptpBrkIntEnd )
{
	int ilRC ;
	long llRow = ARR_FIRST;
	char *pclDrrRow=NULL;
	char clBreakStart[21], clBreakEnd[21];
	
	ilRC = CEDAArrayFindRowPointer(&(rgDrrArray.rrArrayHandle),
								   &(rgDrrArray.crArrayName[0]),
								   &(rgDrrArray.rrIdx01Handle),
								   &(rgDrrArray.crIdx01Name[0]),
								   pcpDrrUrno, &llRow, (void *)&pclDrrRow ) ;
	dbg(TRACE,"GetBreakInterval: Search Drr for URNO <%s> RC <%d>",pcpDrrUrno, ilRC );
	if ( ilRC == RC_SUCCESS )
	{
		strcpy(clBreakStart, DRRFIELD(pclDrrRow,igDrrSbfr) );
		strcpy(clBreakEnd, DRRFIELD(pclDrrRow,igDrrSbto) );
		LocalToUtc(clBreakStart);
		LocalToUtc(clBreakEnd);
		dbg(DEBUG,"GetBreakInterval: UTC Start <%s> End <%s>",clBreakStart, clBreakEnd );
		StrToTime(clBreakStart, ptpBrkIntStart);		
		StrToTime(clBreakEnd, ptpBrkIntEnd);
	}
	return ilRC;
}

static int AddPoolFctRowIfNew ( char *pcpUjob, char *pcpFctc, int ipPrio )
{
	char clKey[51], clPrio[5];
	long llRow = ARR_FIRST; 
	char *pclResult = 0;
	int  ilRC = RC_SUCCESS;

    sprintf(clKey,"%s,%s,%d",pcpUjob, pcpFctc, ipPrio);
			
    if (CEDAArrayFindRowPointer(&(rgPoolFctArray.rrArrayHandle),
								&(rgPoolFctArray.crArrayName[0]),
							    &(rgPoolFctArray.rrIdx01Handle),
							    &(rgPoolFctArray.crIdx01Name[0]),
								clKey,&llRow, (void*)&pclResult ) == RC_NOTFOUND)
	{
		InitializePoolFctRow (cgPoolFctBuf);
		strcpy ( POOLFCTFIELD(cgPoolFctBuf,igPoolFctUjob), pcpUjob );
		strcpy ( POOLFCTFIELD(cgPoolFctBuf,igPoolFctFctc),pcpFctc );
		if ( ipPrio != 1 ) 
		{
			sprintf(clPrio,"%d",ipPrio);
			strcpy(POOLFCTFIELD(cgPoolFctBuf,igPoolFctPrio),clPrio);
		}
		llRow = ARR_NEXT;
		ilRC = AATArrayAddRow(&(rgPoolFctArray.rrArrayHandle),&(rgPoolFctArray.crArrayName[0]),&llRow,(void *)cgPoolFctBuf);
		dbg ( DEBUG, "AddPoolFctRowIfNew: Added UJOB <%s> FCTC <%s> PRIO <%d>  ilRC <%d>", 
			  pcpUjob, pcpFctc, ipPrio, ilRC );
	}
	else
		dbg ( DEBUG, "AddPoolFctRowIfNew: Found UJOB <%s> FCTC <%s> PRIO <%d> in row <%ld>", 
			  pcpUjob, pcpFctc, ipPrio, llRow );
	return ilRC;
}

static int AddPoolPerRowIfNew ( char *pcpUjob, char *pcpPrmc, int ipPrio )
{
	char clKey[51], clPrio[5];
	long llRow = ARR_FIRST; 
	char *pclResult = 0;
	int  ilRC = RC_SUCCESS;

    sprintf(clKey,"%s,%s,%d",pcpUjob, pcpPrmc, ipPrio);
			
    if (CEDAArrayFindRowPointer(&(rgPoolPerArray.rrArrayHandle),
								&(rgPoolPerArray.crArrayName[0]),
							    &(rgPoolPerArray.rrIdx01Handle),
							    &(rgPoolPerArray.crIdx01Name[0]),
								clKey,&llRow, (void*)&pclResult ) == RC_NOTFOUND)
	{
		InitializePoolPerRow (cgPoolPerBuf);
		strcpy ( POOLPERFIELD(cgPoolPerBuf,igPoolPerUjob),pcpUjob );
		strcpy ( POOLPERFIELD(cgPoolPerBuf,igPoolPerPerm),pcpPrmc );
		if ( ipPrio != 1 ) 
		{
			sprintf(clPrio,"%d",ipPrio);
			strcpy(POOLPERFIELD(cgPoolPerBuf,igPoolPerPrio),clPrio);
		}
		llRow = ARR_NEXT;
		ilRC = AATArrayAddRow(&(rgPoolPerArray.rrArrayHandle),&(rgPoolPerArray.crArrayName[0]),&llRow,(void *)cgPoolPerBuf);
		dbg ( DEBUG, "AddPoolPerRowIfNew: Added UJOB <%s> FCTC <%s> PRIO <%d>  ilRC <%d>", 
			  pcpUjob, pcpPrmc, ipPrio, ilRC );
	}
	else
		dbg ( DEBUG, "AddPoolPerRowIfNew: Found UJOB <%s> FCTC <%s> PRIO <%d> in row <%ld>", 
			  pcpUjob, pcpPrmc, ipPrio, llRow );
	return ilRC;
}

void ReplaceChar ( char *pcpStr, char cpOld, char cpNew )
{
	char *pclPos, *pclAct;
	pclAct = pcpStr;
	while ( pclAct )
	{
		if ( pclPos = strchr ( pclAct, cpOld ) ) 
			*pclPos = cpNew;
		pclAct = pclPos;
	}
}


static int HandleRelJob (char *pcpBCData, char *pcpBCSelection )
{
	int		ilRC = RC_SUCCESS, ilItemNo=1, ilLen, ilBytes;
	char	*pclBuff, *pclRest;
	char	clDel='-';
	char	clDel2='\262' /* mapped comma */;
	char    clStart[15]="", clEnd[15]="", clUrno[11], *pclSelection;
	int     ilRC1, ilRC2;
	BOOL	blDone = FALSE, blOverlap = FALSE;
	BOOL	blBreaks=FALSE, blPoolJobs=FALSE;
	char    clUstf[11], clUjty[11];
	long	llRowCount=0, llRowNum = ARR_FIRST, llFunc;
	char	*pclRow=0, *pclOldRow=0;
	int		ilLenPool, ilLenBreak, ilPoolCnt=0, ilBrkCnt=0, ilDiff;
	
	if ( !pcpBCData  || !pcpBCSelection )
		return RC_FAIL;

	dbg(TRACE,"HandleRelJob: Data <%s>", pcpBCData );
	dbg(TRACE,"HandleRelJob: Selection <%s>", pcpBCSelection );

	ilBytes = strlen(pcpBCData) * 3 / 2 + 130;
	pclBuff = (char*)calloc ( 1, ilBytes ) ;
	pclSelection = (char*)calloc ( 1, ilBytes ) ;
	if ( !pclBuff || !pclSelection )
	{
		dbg(TRACE,"HandleRelJob: calloc of <%d> bytes failed", ilBytes );
		if ( pclBuff )
			free ( pclBuff );
		return RC_NOMEM;
	}
	else
		dbg (DEBUG, "HandleRelJob: Data len <%d> now allocated <%d>", 
					strlen(pcpBCData), ilBytes );

	/* Check release dates, maybe nothing to be done */
	if ( !blDone && (ilRC == RC_SUCCESS) )
	{
		if ( GetDataItem(pclBuff, pcpBCData, 1, clDel, "", "\0\0") > 0)
		{
			strncpy ( clStart, pclBuff, 8 );
			clStart[8] = '\0';
		}
		if ( GetDataItem(pclBuff, pcpBCData, 2, clDel, "", "\0\0") > 0)
		{
			strncpy ( clEnd, pclBuff, 8 );
			clEnd[8] = '\0';
		}
		if ( (strlen(clEnd)<8) || (strlen (clStart)<8) )
			ilRC = RC_INIT_FAIL;
	}
	if ( !blDone && (ilRC == RC_SUCCESS) )
	{
		strcat ( clEnd, "235959" );
		strcat ( clStart, "000000" );
		LocalToUtc ( clStart );
		LocalToUtc ( clEnd );
		blOverlap = ((strcmp(clStart,cgLoadEnd) <= 0) && (strcmp(clEnd,cgLoadStart) >= 0));
		dbg ( TRACE, "HandleRelJob: Start <%s> End <%s> overlaps timeframe ? <%d>", 
			  clStart, clEnd, blOverlap );
		if ( !blOverlap )
			blDone = TRUE;
	}
	if ( !blDone && (ilRC == RC_SUCCESS) )
	{	
		sprintf ( clUjty, "%ld", lgUjtyPol );
		if ( strstr ( pcpBCSelection, clUjty ) )
		{
			blPoolJobs = TRUE;
			dbg (DEBUG, "HandleRelJob: Pooljobs in selection" );
		}
		sprintf ( clUjty, "%ld", lgUjtyBrk );
		if ( strstr ( pcpBCSelection, clUjty ) )
		{
			blBreaks = TRUE;
			dbg (DEBUG, "HandleRelJob: breaks in selection" );
		}
		if (!blPoolJobs && ! blBreaks )
		{	/* wrong job types -> nothing to do */
			blDone = TRUE;
		}
	}
	if ( !blDone && (ilRC == RC_SUCCESS) )
	{	
		if ( GetDataItem(pclBuff, pcpBCData, 3, clDel, "", "\0\0") > 0)
		{
			ReplaceChar ( pclBuff, clDel2, ',' );

			/* don't load days not included in the loaded timeframe */
			if ( strcmp ( clStart, cgLoadStart ) < 0 )
				strcpy ( clStart, cgLoadStart );
			if ( strcmp ( cgLoadEnd, clEnd ) < 0 )
				strcpy ( clEnd, cgLoadEnd );

			dbg (DEBUG, "HandleRelJob: Reload timeframe <%s> - <%s>", clStart, clEnd );

			sprintf( pclSelection,"WHERE UJTY in ('%ld','%ld') AND ACTO > '%s' AND ACFR < '%s' AND HOPO = '%s' AND USTF in (",
					 lgUjtyPol,lgUjtyBrk, clStart,clEnd,cgHopo);
			while(GetDataItem(clUstf, pclBuff, ilItemNo, ',', "", "\0\0") > 0)
			{
				ilLen = strlen (pclSelection);
				if ( ilItemNo > 1 )
					sprintf ( &(pclSelection[ilLen]), ",'%s'", clUstf );
				else
					sprintf ( &(pclSelection[ilLen]), "'%s'", clUstf );
				ilItemNo++;
			}
			strcat ( pclSelection, ")" );
			dbg ( TRACE, "HandleRelJob: Reloading JOBTAB <%s>", pclSelection );
			ilRC = CEDAArrayRefill(&rgRequestPoolJobArray.rrArrayHandle,
								   rgRequestPoolJobArray.crArrayName,
								   pclSelection, NULL, ARR_FIRST);
			dbg(TRACE,"HandleRelJob: New Jobs Refill RC=%d",ilRC);
		}
		else
		{
			ilRC = RC_INIT_FAIL;
		}
	}	
	if ( !blDone && (ilRC == RC_SUCCESS) )
	{
		CEDAArrayGetRowCount(&rgRequestPoolJobArray.rrArrayHandle,
							 rgRequestPoolJobArray.crArrayName,&llRowCount);
		dbg(TRACE,"HandleRelJob: %ld records read in REJOBPOOL", llRowCount );
		/* compare new jobs with old jobs */
		ilLenPool = rgPoolJobArray.plrArrayFieldOfs[igPoolJobIsok];	/* compare only real field */
		ilLenBreak = rgJobArray.plrArrayFieldOfs[igJobAlid];		/* compare only real field */
		dbg ( TRACE, "ComparePoolJobRows: Length to compare: pooljobs <%d>, breaks <%d>", 
			ilLenPool, ilLenBreak );

		llRowNum = ARR_LAST;
		while ( CEDAArrayGetRowPointer(&rgRequestPoolJobArray.rrArrayHandle,
									   rgRequestPoolJobArray.crArrayName,
									   llRowNum,(void *)&pclRow) == RC_SUCCESS )
		{	
			strcpy ( clUrno, JOBFIELD(pclRow,igJobUrno)	);
			TrimRight ( clUrno );
			llFunc = ARR_FIRST;

			if( atol(JOBFIELD(pclRow,igJobUjty)) == lgUjtyBrk)
			{
				dbg ( TRACE, "HandleRelJob: Checking for old break JOB record with URNO <%s>", clUrno ); 
				if ( CEDAArrayFindRowPointer( &(rgJobArray.rrArrayHandle),
											  rgJobArray.crArrayName,
											  &(rgJobArray.rrIdx02Handle),
											  rgJobArray.crIdx02Name,
											  clUrno, &llFunc, (void *) &pclOldRow ) == RC_SUCCESS )
				{	/* Old break with same URNO found -> compare record */
					ilDiff =  strncmp ( pclRow, pclOldRow, ilLenBreak );
					if ( ilDiff )
					{
						ilRC1 = UpdateJobRecord ( ARR_CURRENT, llFunc, &rgJobArray );
						dbg ( TRACE, "HandleRelJob: UpdateJobRecord <%s> RC <%d>", clUrno, ilRC1 ); 
					}
					ilRC1 = CEDAArrayDeleteRow(&(rgRequestPoolJobArray.rrArrayHandle),
											   rgRequestPoolJobArray.crArrayName, ARR_CURRENT );
					dbg ( TRACE, "HandleRelJob: CEDAArrayDeleteRow on new break record RC <%d>", ilRC1 ); 
				}
				else
					ilBrkCnt++;
			}		
			else if ( atol(JOBFIELD(pclRow,igJobUjty)) == lgUjtyPol)
			{
				dbg ( TRACE, "HandleRelJob: Checking for old Pool JOB record with URNO <%s>", clUrno ); 
				if ( CEDAArrayFindRowPointer( &(rgPoolJobArray.rrArrayHandle),
											  rgPoolJobArray.crArrayName,
											  &(rgPoolJobArray.rrIdx01Handle),
											  rgPoolJobArray.crIdx01Name,
											  clUrno, &llFunc, (void *) &pclOldRow ) == RC_SUCCESS )
				{	/* Old pooljob with same URNO found -> compare record  */
					ilDiff =  strncmp ( pclRow, pclOldRow, ilLenPool );
					if ( ilDiff )
					{
						ilRC1 = UpdateJobRecord ( ARR_CURRENT, llFunc, &rgPoolJobArray );
						dbg ( TRACE, "HandleRelJob: UpdateJobRecord <%s> RC <%d>", clUrno, ilRC1 ); 
					}
					ilRC1 = CEDAArrayDeleteRow(&(rgRequestPoolJobArray.rrArrayHandle),
												 rgRequestPoolJobArray.crArrayName, ARR_CURRENT );
					dbg ( TRACE, "HandleRelJob: CEDAArrayDeleteRow on new pooljob record RC <%d>", ilRC1 ); 
				}
				else
					ilPoolCnt++;
			}
			llRowNum = ARR_PREV;
		}
		CEDAArrayGetRowCount(&rgRequestPoolJobArray.rrArrayHandle,
							 rgRequestPoolJobArray.crArrayName,&llRowCount);
		dbg ( TRACE, "HandleRelJob: Going to reload <%ld> records, <%d> Pooljobs <%d> Breaks",
			  llRowCount, ilPoolCnt, ilBrkCnt );
		ilLen = max ( 13*ilPoolCnt + 21, 13*ilBrkCnt + 21 );
		if ( ilLen > ilBytes )
		{
			pclSelection = (char*)realloc ( pclSelection, ilLen ) ;
			pclBuff = (char*)realloc ( pclBuff, ilLen ) ;
			if ( !pclBuff || !pclSelection )
			{
				dbg(TRACE,"HandleRelJob: realloc of <%d> bytes failed", ilBytes );
				ilRC = RC_NOMEM;
			}
			else
				dbg (DEBUG, "HandleRelJob: buffers len <%d> rellocated", ilLen );
		}
		if ( llRowCount <= 0 )
			blDone = TRUE;
	}
	if ( !blDone && (ilRC == RC_SUCCESS) )
	{
		ilPoolCnt = 0;
		ilBrkCnt = 0;
		strcpy ( pclSelection, "WHERE URNO IN (" );	/* to collect new pooljob URNOs */
		strcpy ( pclBuff, "WHERE URNO IN (" );		/* to collect new break URNOs */

		llRowNum = ARR_FIRST;
		while ( CEDAArrayGetRowPointer(&rgRequestPoolJobArray.rrArrayHandle,
									   rgRequestPoolJobArray.crArrayName,
									   llRowNum,(void *)&pclRow) == RC_SUCCESS )
		{	
			strcpy ( clUrno, JOBFIELD(pclRow,igJobUrno)	);
			TrimRight ( clUrno );
			if ( atol(JOBFIELD(pclRow,igJobUjty)) == lgUjtyPol)
			{
				ilLen = strlen (pclSelection);
				if ( ilPoolCnt > 0 )
					sprintf ( &(pclSelection[ilLen]), ",'%s'", clUrno );
				else
					sprintf ( &(pclSelection[ilLen]), "'%s'", clUrno );
				ilPoolCnt++;
			}
			if ( atol(JOBFIELD(pclRow,igJobUjty)) == lgUjtyBrk)
			{
				ilLen = strlen (pclBuff);
				if ( ilBrkCnt > 0 )
					sprintf ( &(pclBuff[ilLen]), ",'%s'", clUrno );
				else
					sprintf ( &(pclBuff[ilLen]), "'%s'", clUrno );
				ilBrkCnt++;
			}
			llRowNum = ARR_NEXT;
		}
		if ( ilPoolCnt > 0 )
		{
			strcat ( pclSelection, ")" );
			dbg ( TRACE, "HandleRelJob: Selection for refill pooljobs\n<%s>", pclSelection );
			ilRC = CEDAArrayRefill(&rgPoolJobArray.rrArrayHandle,rgPoolJobArray.crArrayName,
								   pclSelection, NULL, ARR_NEXT );
			dbg(TRACE,"HandleRelJob: POOLJOBARRAY Refill RC=%d",ilRC);
		}
		if ( ilBrkCnt > 0 )
		{
			strcat ( pclBuff, ")" );
			dbg ( TRACE, "HandleRelJob: Selection for refill breaks\n<%s>", pclBuff );
			ilRC1 = CEDAArrayRefill(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,
								   pclBuff, NULL, ARR_NEXT );
			dbg(TRACE,"HandleRelJob: JOBARRAY Refill RC=%d",ilRC1);
			ilRC |= ilRC1;
		}		
	}
	
	if ( !blDone && (ilRC == RC_SUCCESS) )
	{
		llRowNum = ARR_FIRST;
		while ( CEDAArrayGetRowPointer(&rgRequestPoolJobArray.rrArrayHandle,
									   rgRequestPoolJobArray.crArrayName,
									   llRowNum,(void *)&pclRow) == RC_SUCCESS )
		{	
			if ( atol(JOBFIELD(pclRow,igJobUjty)) == lgUjtyPol)
			{
				strcpy ( clUrno, JOBFIELD(pclRow,igJobUrno)	);
				TrimRight ( clUrno );
				
				ilRC1 = ProcessNewPoolJob(clUrno, &rgPoolJobArray, TRUE, bgAutomaticPauseAllocation);
				dbg ( TRACE, "HandleRelJob: ProcessNewPoolJob URNO <%s> RC <%d>", clUrno, ilRC1 ); 
			}
			llRowNum = ARR_NEXT;
		}
	}

	if ( pclBuff )
		free ( pclBuff );
	if ( pclSelection )
		free ( pclSelection );
	return ilRC;

}

static int HandleRelDrr (char *pcpBCData )
{
	int		ilRC, ilItemNo=1, ilLen;
	char	*pclBuff;
	char	clDel='-';
	char	clDel2='\262' /* mapped comma */;
	char    clStart[15]="", clEnd[15]="", clRelLevels[11], *pclSelection;
	int    ilRC1, ilRC2, ilLevel1=0, ilLevel2=0;
	BOOL	blDone = FALSE, blOverlap = FALSE;
	char    clUstf[11], clUdrr[11];
	long	llRowCount=0, llRowNum = ARR_FIRST, llFunc;
	char	*pclRow=0, *pclOldRow=0;

	if ( !pcpBCData )
		return RC_FAIL;

	dbg(TRACE,"HandleRelDrr: Data <%s>", pcpBCData );

	pclBuff = (char*)calloc ( 1, strlen(pcpBCData)+1 ) ;
	ilLen = strlen(pcpBCData) * 3 / 2 + 130;
	pclSelection = (char*)calloc ( 1, ilLen ) ;
	if ( !pclBuff || !pclSelection )
	{
		dbg(TRACE,"HandleRelDrr: calloc of <%d> bytes failed", ilLen );
		if ( pclBuff )
			free ( pclBuff );
		return RC_NOMEM;
	}
	else
		dbg (DEBUG, "HandleRelDrr: Data len <%d> now allocated <%d>", 
					strlen(pcpBCData), ilLen );

	/* Check release levels, maybe nothing to be done */
	if ( GetDataItem(pclBuff, pcpBCData, 4, clDel, "", "\0\0") > 0 )
	{
		if ( strlen (pclBuff) > 10 )
			pclBuff[10] = '\0';		/* to avoid buffer overflow */
		ilRC1 = GetDataItem(clRelLevels, pclBuff, 1, clDel2, "", "\0\0");
		if ( ilRC1 > 0 )
			ilLevel1 = atoi ( clRelLevels );
		ilRC2 = GetDataItem(clRelLevels, pclBuff, 2, clDel2, "", "\0\0");
		if ( ilRC1 > 0 )
			ilLevel2 = atoi ( clRelLevels );
		dbg(TRACE,"HandleRelDrr: Found Release <%d>--><%d>", ilLevel1, ilLevel2 );
		if ( !ilLevel1 || !ilLevel2 )	/* both release levels found ? */
			ilRC = RC_NOT_FOUND;
		else
		{	/* if not released to daily list, nothing to do */
			if ( ilLevel2 != 3 )
				blDone = TRUE;
		}
	}
	else
	{
		dbg(TRACE,"HandleRelDrr: wrong parameter count" );
		return RC_FAIL;
	}
	
	/* Check release dates, maybe nothing to be done */
	if ( !blDone && (ilRC == RC_SUCCESS) )
	{
		if ( GetDataItem(pclBuff, pcpBCData, 1, clDel, "", "\0\0") > 0)
		{
			strncpy ( clStart, pclBuff, 8 );
			clStart[8] = '\0';
		}
		if ( GetDataItem(pclBuff, pcpBCData, 2, clDel, "", "\0\0") > 0)
		{
			strncpy ( clEnd, pclBuff, 8 );
			clEnd[8] = '\0';
		}
		if ( (strlen(clEnd)<8) || (strlen (clStart)<8) )
			ilRC = RC_INIT_FAIL;
	}
	if ( !blDone && (ilRC == RC_SUCCESS) )
	{
		strcat ( clEnd, "235959" );
		strcat ( clStart, "000000" );
		LocalToUtc ( clStart );
		LocalToUtc ( clEnd );
		blOverlap = ((strcmp(clStart,cgLoadEnd) <= 0) && (strcmp(clEnd,cgLoadStart) >= 0));
		dbg ( TRACE, "HandleRelDrr: Start <%s> End <%s> overlaps timeframe ? <%d>", 
			  clStart, clEnd, blOverlap );
		if ( !blOverlap )
			blDone = TRUE;
	}
	if ( !blDone && (ilRC == RC_SUCCESS) )
	{
		if ( GetDataItem(pclBuff, pcpBCData, 3, clDel, "", "\0\0") > 0)
		{
			ReplaceChar ( pclBuff, clDel2, ',' );

			/* don't load days not included in the loaded timeframe */
			if ( strcmp ( clStart, cgLoadStart ) < 0 )
				strcpy ( clStart, cgLoadStart );
			if ( strcmp ( cgLoadEnd, clEnd ) < 0 )
				strcpy ( clEnd, cgLoadEnd );

			sprintf(pclSelection,
				    "WHERE AVTO >= '%s' AND AVFR <= '%s' AND HOPO = '%s' AND ROSL = '3' AND ROSS != 'L' AND STFU IN (",
					 clStart ,clEnd,cgHopo);
			while(GetDataItem(clUstf, pclBuff, ilItemNo, ',', "", "\0\0") > 0)
			{
				ilLen = strlen (pclSelection);
				if ( ilItemNo > 1 )
					sprintf ( &(pclSelection[ilLen]), ",'%s'", clUstf );
				else
					sprintf ( &(pclSelection[ilLen]), "'%s'", clUstf );
				ilItemNo++;
			}
			strcat ( pclSelection, ")" );
			dbg ( TRACE, "HandleRelDrr: Reloading DRRTAB <%s>", pclSelection );
			ilRC = CEDAArrayRefill(&rgNewDrrArray.rrArrayHandle,rgNewDrrArray.crArrayName,
								   pclSelection, NULL, ARR_FIRST);
			dbg(TRACE,"HandleRelDrr: NEWDRR Refill RC=%d",ilRC);
		}
		else
			ilRC = RC_INIT_FAIL;
	}	
	if ( !blDone && (ilRC == RC_SUCCESS) )
	{
		CEDAArrayGetRowCount(&rgNewDrrArray.rrArrayHandle,
							 rgNewDrrArray.crArrayName,&llRowCount);
		dbg(TRACE,"HandleRelDrr: %ld records read in NEWDRR", llRowCount );
		/* delete all old DRR records in rgDrrArray with the same URNOs */
		while ( CEDAArrayGetRowPointer(&rgNewDrrArray.rrArrayHandle,
									   rgNewDrrArray.crArrayName,
									   llRowNum,(void *)&pclRow) == RC_SUCCESS )
		{	
			strcpy ( clUdrr, DRRFIELD(pclRow,igDrrUrno)	);
			TrimRight ( clUdrr );
			dbg ( TRACE, "HandleRelDrr: Checking for old DRR record with URNO <%s>", clUdrr ); 
			llFunc = ARR_FIRST;
			if ( CEDAArrayFindRowPointer( &(rgDrrArray.rrArrayHandle),
										  rgDrrArray.crArrayName,
										  &(rgDrrArray.rrIdx01Handle),
										  rgDrrArray.crIdx01Name,
										  clUdrr, &llFunc, (void *) &pclOldRow ) == RC_SUCCESS )
			{
				/* Old Record with same URNO found -> delete record insert new one */
				ilRC1 = CEDAArrayDeleteRow(&(rgDrrArray.rrArrayHandle),
										  rgDrrArray.crArrayName, llFunc );
				dbg ( TRACE, "HandleRelDrr: CEDAArrayDeleteRow on old DRR-record RC <%d>", ilRC1 ); 
			}		
			llRowNum = ARR_NEXT;
		}
		/* copy of record from rgNewDrrArray to rgDrrArray doesn't work */
		/* so refill rgDrrArray with the same selection */
		ilRC = CEDAArrayRefill(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,
							   pclSelection, NULL, ARR_NEXT );
		dbg(TRACE,"HandleRelDrr: DRRTAB Refill RC=%d",ilRC);
	}
	if ( pclBuff )
		free ( pclBuff );
	if ( pclSelection )
		free ( pclSelection );
	return ilRC;
}



static int UpdateJobRecord ( long lpRowSrc, long lpRowDest, ARRAYINFO *prpDest )	
{
	int		ilRC = RC_SUCCESS;
	long	llRowWrite = ARR_FIRST;	/*  row in rgSdiArray */
	long	*pllFields=0, llRow = lpRowDest;

	dbg ( DEBUG, "UpdateJobRecord: Going to update row <%ld> in <%s>", 
		  llRow, prpDest->crArrayName );	

	pllFields = (long*)calloc( rgRequestPoolJobArray.lrArrayFieldCnt, sizeof(long) );
	if ( !pllFields )
	{
		dbg ( TRACE, "UpdateJobRecord: calloc failed" );
		return RC_NOMEM;
	}
	pllFields[0] = -1;

	memset ( rgRequestPoolJobArray.pcrArrayRowBuf, 0, sizeof(rgRequestPoolJobArray.pcrArrayRowBuf) );

	ilRC = CEDAArrayGetFields( &rgRequestPoolJobArray.rrArrayHandle,
							   rgRequestPoolJobArray.crArrayName, pllFields, 
				  			   rgRequestPoolJobArray.crArrayFieldList, ',',
							   rgRequestPoolJobArray.lrArrayRowLen, lpRowSrc,
							   (void*)rgRequestPoolJobArray.pcrArrayRowBuf);
	if ( ilRC == RC_SUCCESS )
	{
		ilRC = CEDAArrayPutFields( &(prpDest->rrArrayHandle),
								   prpDest->crArrayName, 0, &llRow, 
								   prpDest->crArrayFieldList,
								   prpDest->pcrArrayRowBuf );	
		if ( ilRC != RC_SUCCESS )
			dbg ( TRACE, "UpdateJobRecord: CEDAArrayPutFields Row <%ld> failed RC <%d>", 
				  llRow, ilRC );
	}
	else
		dbg ( TRACE, "UpdateJobRecord: CEDAArrayGetFields Row <%ld> failed RC <%d>", 
			  lpRowSrc, ilRC );
	if ( pllFields )
		free ( pllFields  );
	return ilRC;
}

static int SaveJobs ( BOOL bpSendUpdJob )
{
	int ilRc1, ilRc = RC_SUCCESS;
	char *pclUpdSelLst = NULL;
	char *pclUpdCmdLst = NULL;
	long  llBytes, llNumUrnos = NULL, llCount;
	int	  ilModified=0, ilDeleted=0, ilIdx=1;
	char clCmd[11], clUrno[11], clDel=(char)10;
	char	clDel2='\262' /* mapped comma */;
	char *pclData = 0;
	char *pclModified = 0;
	char *pclDeleted = 0;
	char clTwEnd[33];
	BOOL blFinished = FALSE;

	if ( bpSendUpdJob )
	{	/*	if we send a single BC for all updates, we have to save rgJodArray first,
			to have the actual values for JOBTAB in pclUpdSelLst and pclUpdCmdLst */
		ilRc1 = CEDAArrayWriteDB( &rgJodArray.rrArrayHandle,rgJodArray.crArrayName,
								 NULL,NULL,NULL,&llNumUrnos,ARR_COMMIT_ALL_OK);
		if ( ilRc1 != RC_SUCCESS )
		{
			int ilOldDebugLevel = debug_level;
			debug_level = TRACE;
			dbg(TRACE,"SaveJobs: no changes written to JODTAB ");
			debug_level = ilOldDebugLevel;
		}
	}	
	
    ilRc1 = CEDAArrayWriteDBGetChanges(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,
									  &pclUpdCmdLst,&pclUpdSelLst,NULL,NULL,
									  &llNumUrnos,ARR_COMMIT_ALL_OK);
	
    if ( (ilRc1 != RC_SUCCESS) && (llNumUrnos>0) )
    {
		int ilOldDebugLevel = debug_level;
		debug_level = TRACE;
		dbg(TRACE,"SaveJobs: no changes written to JOBTAB ");
		debug_level = ilOldDebugLevel;
    }

	if ( bpSendUpdJob && pclUpdCmdLst && pclUpdSelLst )
	{	/*  allocate memory for local variables and calculate time frame	*/
		/*	Form: "Date1,Date2,DEL:n,URNO1,..,URNOn,UPD:m,URNO1,..,URNOm"	*/
		llCount = min ( MAXINSEL, llNumUrnos );
		llBytes = 2*15 + 20 + llCount * (URNOLEN+1);

		dbg(TRACE,"SaveJobs: llCount <%ld> llNumUrnos <%d> llBytes <%ld>", llCount, llNumUrnos, llBytes );

		pclModified = malloc ( llNumUrnos * (URNOLEN+1) );
		if ( pclModified )
			*pclModified = '\0';
		pclDeleted = malloc ( llNumUrnos * (URNOLEN+1) );
		if ( pclDeleted )
			*pclDeleted = '\0';
		pclData = malloc ( llBytes );
		if ( pclData )
			*pclData = '\0';
		
		if ( !pclData )
			ilRc = RC_NOMEM;
		else
		{
			do 
			{
				if ( GetDataItem (clCmd, pclUpdCmdLst, ilIdx, clDel, "", "  ") > 0 )
				{
					if ( GetDataItem (clUrno, pclUpdSelLst, ilIdx, clDel, "", "  ") > 0 )
					{
						if ( strstr (clCmd, "DRT" ) )
						{
							ilDeleted ++;
							sprintf ( pclDeleted+strlen(pclDeleted), "%c%s", clDel2,clUrno );
						}
						else 
						{
							ilModified ++;
							sprintf ( pclModified+strlen(pclModified), "%c%s", clDel2,clUrno );
						}
					}
					else
						ilRc = RC_WRONGDATA;
					ilIdx++;
				}
				else
					blFinished = TRUE;

				llCount = ilDeleted + ilModified;
				if ( ( llCount >= MAXINSEL ) ||
					 ( ( llCount>0) && blFinished ) )
				{
					dbg(TRACE,"SaveJobs: Preparing new broadcast" );

					/*	Form: "Date1,Date2,DEL:n,URNO1,..,URNOn,UPD:m,URNO1,..,URNOm"	*/
					sprintf ( pclData, "%s,%s,DEL:%d%s,UPD:%d%s", cgMinAcfr, cgMaxActo, 
							  ilDeleted, pclDeleted, ilModified, pclModified );
					dbg(TRACE,"SaveJobs: Broadcast Data <%s>", pclData );
	
					sprintf (clTwEnd, "%s,%s,jobhdl", cgHopo,cgTabEnd );
					ilRc = tools_send_info_flag( 1900,0, "BCHDL", "JOBHDL", "", "", 
												 "", "", clTwEnd, "SBC","UPDJOB","",
												 "ACFR,ACTO,DEL,UPD",pclData,0 );          
					ilRc |= tools_send_info_flag( 7400,0, "ACTION", "JOBHDL", "", "", 
												  "", "", clTwEnd, "SBC","UPDJOB","",
												  "ACFR,ACTO,DEL,UPD",pclData,0 );          
					ilDeleted = ilModified = 0;
					*pclModified = '\0';
					*pclDeleted = '\0';
				}

			} while (!blFinished);
		}
	}
	if ( !bpSendUpdJob )
	{	/*	if we send separate BCs for all updates, we have to save rgJodArray
			at last to let OPSS-PM get BCs in the expected order */
		ilRc1 = CEDAArrayWriteDB( &rgJodArray.rrArrayHandle,rgJodArray.crArrayName,
								 NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);
		if ( ilRc1 != RC_SUCCESS )
		{
			int ilOldDebugLevel = debug_level;
			debug_level = TRACE;
			dbg(TRACE,"SaveJobs: no changes written to JODTAB ");
			debug_level = ilOldDebugLevel;
		}
	}
	if ( pclModified )
		free ( pclModified );
	if ( pclDeleted )
		free ( pclDeleted );
	if ( pclData )
		free ( pclData );

	return ilRc;
}

/*  Store earliest ACFR and last ACTO for later use in common BC */
static int UpdateMinMax ( char *pcpAcfr, char *pcpActo )
{
	
	if ( !pcpAcfr || !pcpActo )
		return RC_INVALID;

	if ( !cgMinAcfr[0] || ( strcmp ( pcpAcfr, cgMinAcfr ) < 0 ) )
		strcpy ( cgMinAcfr, pcpAcfr );
	if ( strcmp(cgMaxActo, pcpActo ) < 0 )
		strcpy ( cgMaxActo, pcpActo );
	return RC_SUCCESS;
}

/********************************************************************/
static int GetNextUrno(char *pcpUrno)
{
	int ilRC = RC_SUCCESS ;
	char  pclDataArea[IDATA_AREA_SIZE] ;
	
	if (igReservedUrnoCnt <= 0)
	{                            /* hole Urno buffer -> get new Urnos */
		memset( pclDataArea, 0, IDATA_AREA_SIZE) ;
		if ((ilRC = GetNextValues(pclDataArea, URNOS_TO_FETCH)) != RC_SUCCESS)
		{
			dbg ( TRACE,"GetNextValues failed RC <%d>", ilRC );
		}
		else
		{
			strcpy ( pcpUrno, pclDataArea );
			igReservedUrnoCnt = URNOS_TO_FETCH - 1 ;
			lgActUrno = atol(pcpUrno);
			dbg ( DEBUG, "GetNextUrno: fetched %d Urnos. Next is <%s>", 
				  URNOS_TO_FETCH, pcpUrno ) ;
		}
	}
	else
	{
		igReservedUrnoCnt-- ;
		lgActUrno++ ;
		sprintf ( pcpUrno, "%ld", lgActUrno );      /* get next reserved Urno */
		dbg ( DEBUG, "GetNextUrno: Next Urno <%s>, %d Urnos remaining", 
			  pcpUrno, igReservedUrnoCnt ) ;
	}
	
	return (ilRC) ;
} /* getNextUrno () */


static int GetTotalRowLength1(char *pcpTana, char *pcpFieldList, char *pcpAddFields,
							 long *plpAddFieldSizes,long *plpLen)
{
    int	 ilRc        = RC_SUCCESS;			/* Return code */
    int  ilNoOfItems = 0;
    int  ilItemNo = 0;
    int  ilLoop      = 0;
    long llRowLen    = 0;
    long llFldLen    = 0;
    char clFina[8];
	
    if (pcpAddFields != NULL && pcpFieldList != NULL)
    {
		ilNoOfItems = get_no_of_items(pcpFieldList);
			
		ilLoop = 1;
		do
		{
			get_real_item(clFina,pcpFieldList,ilLoop);

			if(GetItemNo(clFina,pcpAddFields,&ilItemNo) == RC_SUCCESS) 
			{
				llRowLen++;
				llRowLen += plpAddFieldSizes[ilItemNo-1];
				dbg(DEBUG,"GetTotalRowLength: log. Field clFina <%s> Length <%ld>",clFina,plpAddFieldSizes[ilItemNo-1]);
			}
			else
			    if(GetFieldLength(pcpTana,&clFina[0],&llFldLen) == RC_SUCCESS)
				{
					llRowLen++;
					llRowLen += llFldLen;
					dbg(DEBUG,"GetTotalRowLength: phys. Field clFina <%s> Length <%ld>",clFina,llFldLen);
				}/* end of if */
				else
					ilRc == RC_FAIL;
			ilLoop++;
		}while(ilLoop <= ilNoOfItems);
	}
	if(ilRc == RC_SUCCESS)
	{
		*plpLen = llRowLen;
	}/* end of if */

	dbg(DEBUG,"GetLogicalRowLength:  FieldList <%s> Length <%ld>",pcpFieldList,*plpLen);
	return(ilRc);
	
} 

static int GetTotalRowLength( char *pcpFieldList, ARRAYINFO *prpArray, long *plpLen)
{
    int	 ilRc        = RC_SUCCESS;			/* Return code */
    int  ilNoOfItems = 0;
    int  ilItemNo = 0;
    int  ilLoop      = 0;
    long llRowLen    = 0;
    long llFldLen    = 0;
    char clFina[8];
	
    if (prpArray != NULL && pcpFieldList != NULL)
    {
		ilNoOfItems = get_no_of_items(pcpFieldList);
			
		ilLoop = 1;
		do
		{
			get_real_item(clFina,pcpFieldList,ilLoop);

			if(GetItemNo(clFina,prpArray->crArrayFieldList,&ilItemNo) == RC_SUCCESS) 
			{
				llRowLen++;
				llRowLen += prpArray->plrArrayFieldLen[ilItemNo-1];
				dbg(DEBUG,"GetTotalRowLength: clFina <%s> Length <%ld>",clFina,prpArray->plrArrayFieldLen[ilItemNo-1]);
			}
			else
				ilRc == RC_FAIL;
			ilLoop++;
		}while(ilLoop <= ilNoOfItems);
	}
	if(ilRc == RC_SUCCESS)
	{
		*plpLen = llRowLen;
	}/* end of if */

	dbg(DEBUG,"GetTotalRowLength:  FieldList <%s> Length <%ld>",pcpFieldList,*plpLen);
	return(ilRc);
	
} /* end of GetTotalRowLength */



static int CreateSplittedMAJobs(ARRAYINFO *prpPoolJobArray,
				char *pcpDemUrno,char *pcpRudUrno,char *pcpTpln,char *pcpAlid,char *pcpAloc,long lpTtgt,int ipRtwt,int ipIndex,
				time_t tpDebe,time_t tpDeen,long lpMoveOffSet,long lpMind,long lpMaxd,long *plpRealTtgt)
{
    int ilRc = RC_SUCCESS;
    long llRowNum = ARR_FIRST;
    long llRowNum1 = ARR_FIRST;
    
    char *pclPoolJobRow = NULL;
    char *pclJobRow = NULL;
    char *pclKeyUrnoListRow = NULL;
    int ilKeyCount = 0;
    BOOL blOptimalFound = FALSE;
	BOOL blNextJobFound = FALSE;
    time_t tlSplittedDebe = tpDebe;
    time_t tlSplittedDeen = tpDeen;
    time_t tlTmpDeen = tpDeen;
    time_t tlTmpLatestDeen = tpDeen;
    time_t tlTmpStart;
	
    char pclDemStart[20];
    char pclDemEnd[20];
    char clKey[10];
    char clUrnoKey[50];
    char clTmpKey[50];

    int ilMinSplit = 1;
    int ilJobCount = 0;

    double dlRest = 0;
    long llMaxd = lpMaxd;
    long llMind = lpMind;

    if(lpMaxd <= 0 && lpMind <= 0)
    {
		llMaxd = tpDeen - tpDebe;
		llMind = tpDeen - tpDebe;
    }


    dbg(TRACE,"CreateSplittedMAJobs pcpDemUrno <%s> llMaxd <%ld> llMind <%ld>",pcpDemUrno,llMaxd,llMind);


    sprintf(clTmpKey,"%s",pcpDemUrno);

    tlSplittedDebe = tpDebe;

    tlSplittedDeen = tpDebe + llMind;

    tlTmpLatestDeen = min ( tlTmpLatestDeen, tpDebe + llMaxd );
	
    llRowNum1 = ARR_FIRST;

    while(AATArrayFindRowPointer(&(rgKeyUrnoListArray.rrArrayHandle),
				 &(rgKeyUrnoListArray.crArrayName[0]),
				 &(rgKeyUrnoListArray.rrIdx01Handle),
				 &(rgKeyUrnoListArray.crIdx01Name[0]),
				 clTmpKey,&llRowNum1,
				 (void *) &pclKeyUrnoListRow ) == RC_SUCCESS)
    {
		AATArrayDeleteRow(&(rgKeyUrnoListArray.rrArrayHandle),&(rgKeyUrnoListArray.crArrayName[0]),llRowNum1);
		llRowNum1 = ARR_NEXT;
    }

	ilKeyCount = 1;
	strcpy ( clKey, "x1" );
/*    while ( ( FindMatchingPoolJobs(pcpAlid,pcpAloc,pcpRudUrno,pcpTpln, tlSplittedDebe, tlSplittedDeen,
								tlSplittedDeen ,lpTtgt,ipRtwt,ipIndex,prpPoolJobArray,clKey) == RC_SUCCESS)
			 && !blOptimalFound )*/
	while ( !blOptimalFound )
    {
		FindMatchingPoolJobs(pcpAlid,pcpAloc,pcpRudUrno,pcpTpln, tlSplittedDebe, tlSplittedDeen,
							 tlSplittedDeen ,lpTtgt,ipRtwt,ipIndex,prpPoolJobArray,clKey);
		CalcOptimalValues( prpPoolJobArray,pcpAlid, tlSplittedDebe, tlSplittedDeen,lpTtgt,ipRtwt,clKey);
		dbg(DEBUG,"CreateSplittedMAJobs: Looking for Pooljob with key <%s> ",clKey );
		llRowNum = ARR_FIRST;
		blNextJobFound = FALSE;
		while ( !blNextJobFound &&
			    ( CEDAArrayFindRowPointer(&(prpPoolJobArray->rrArrayHandle),
										  &(prpPoolJobArray->crArrayName[0]),
										  &(prpPoolJobArray->rrIdx04Handle),
										  &(prpPoolJobArray->crIdx04Name[0]),
										  clKey,&llRowNum,
										 (void *) &pclPoolJobRow ) == RC_SUCCESS)
			  )
		{
			llRowNum1 = ARR_FIRST;

			CEDAArrayPutField(&(prpPoolJobArray->rrArrayHandle),
					  &(prpPoolJobArray->crArrayName[0]),NULL,"ISOK",llRowNum,"y") ;
			dbg(DEBUG,"POOLJOBFIELD(pclPoolJobRow,igPoolJobUnxj): <%s> ",POOLJOBFIELD(pclPoolJobRow,igPoolJobUnxj));
			if((atoi(POOLJOBFIELD(pclPoolJobRow,igPoolJobUnxj)) != 0) && 
			   (CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
							&(rgJobArray.crArrayName[0]),
							&(rgJobArray.rrIdx02Handle),
							&(rgJobArray.crIdx02Name[0]),
							POOLJOBFIELD(pclPoolJobRow,igPoolJobUnxj),&llRowNum1,
							(void *) &pclJobRow ) == RC_SUCCESS))
			{
				/*dbg(DEBUG,"%05d: JOBFIELD(pclJobRow,igJobUrno): <%s>,JOBFIELD(pclJobRow,igJobJour : <%s>",__LINE__,JOBFIELD(pclJobRow,igJobUrno),JOBFIELD(pclJobRow,igJobJour)); */
				StrToTime(JOBFIELD(pclJobRow,igJobAcfr),&tlTmpDeen);
				dbg(DEBUG,"CreateSplittedMAJobs: tlTmpDeen <%ld> after job <%s>", tlTmpDeen, JOBFIELD(pclJobRow,igJobUrno) );				
			}
			else
			{
				StrToTime(JOBFIELD(pclPoolJobRow,igJobActo),&tlTmpDeen);
				tlTmpDeen -= igJobEndBuffer*60;
				dbg(DEBUG,"CreateSplittedMAJobs: tlTmpDeen <%ld> after pooljob <%s>", tlTmpDeen, JOBFIELD(pclPoolJobRow,igJobUrno) );				
			}
					
			tlTmpDeen = min(tlTmpDeen,tlTmpLatestDeen );
			dlRest = tpDeen - tlTmpDeen;

			dbg(DEBUG,"CreateSplittedMAJobs Rest of Demand <%f>",dlRest);

			if(dlRest > 0)
			{
				ilMinSplit = ceil(dlRest/llMaxd);
				tlTmpDeen = min(tlTmpDeen,tpDeen - ilMinSplit*llMind );
				dbg(DEBUG,"CreateSplittedMAJobs ilMinSplit<%d>",ilMinSplit);
			}

			*plpRealTtgt = atol(POOLJOBFIELD(pclPoolJobRow,igPoolJobWayt));

			tlTmpStart = tlSplittedDebe - atol(POOLJOBFIELD(pclPoolJobRow,igPoolJobWayt));
			TimeToStr(pclDemStart,tlTmpStart);
			TimeToStr(pclDemEnd,tlTmpDeen);

			if ( (tlTmpDeen >= tlSplittedDeen ) && (tlTmpStart<tlTmpDeen) )
			{
				blNextJobFound = TRUE;
				sprintf(clUrnoKey,"%s,x%d",pcpDemUrno,ilKeyCount);

				dbg ( TRACE, "CreateSplittedMAJobs: Set job BEGI <%s> ENDE <%s> clUrnoKey<%s> UPJB <%s> ",
					  pclDemStart, pclDemEnd, clUrnoKey, POOLJOBFIELD(pclPoolJobRow,igJobUrno) );
				llRowNum1 = ARR_FIRST;
				InitializeKeyUrnoListRow(cgKeyUrnoListBuf);
						
				AATArrayAddRow(&(rgKeyUrnoListArray.rrArrayHandle),&(rgKeyUrnoListArray.crArrayName[0]),
					   &llRowNum1,(void *)cgKeyUrnoListBuf);

				AATArrayPutField(&(rgKeyUrnoListArray.rrArrayHandle),
						 &(rgKeyUrnoListArray.crArrayName[0]),NULL,"URNO",llRowNum1,TRUE,
						 POOLJOBFIELD(pclPoolJobRow,igJobUrno)) ;
				AATArrayPutField(&(rgKeyUrnoListArray.rrArrayHandle),
						 &(rgKeyUrnoListArray.crArrayName[0]),NULL,"BEGI",llRowNum1,TRUE,
						 pclDemStart) ;
				AATArrayPutField(&(rgKeyUrnoListArray.rrArrayHandle),
						 &(rgKeyUrnoListArray.crArrayName[0]),NULL,"ENDE",llRowNum1,TRUE,
						 pclDemEnd) ;
				AATArrayPutField(&(rgKeyUrnoListArray.rrArrayHandle),
						 &(rgKeyUrnoListArray.crArrayName[0]),NULL,"UDEM",llRowNum1,TRUE,
						 pcpDemUrno) ;
				AATArrayPutField(&(rgKeyUrnoListArray.rrArrayHandle),
						 &(rgKeyUrnoListArray.crArrayName[0]),NULL,"XKEY",llRowNum1,TRUE,
						 clKey) ;
				ilJobCount ++;
				if(tlTmpDeen == tpDeen)
				{
					blOptimalFound = TRUE;
					dbg(TRACE,"CreateSplittedMAJobs: Break outer loop, Demand fully covered");
				}
			}
			else
			{
				dbg ( TRACE, "CreateSplittedMAJobs: BEGI <%s> END <%s> UPJB <%s> cancelled",
					  pclDemStart, pclDemEnd, POOLJOBFIELD(pclPoolJobRow,igJobUrno) );
			}
		} /* end of loop: while (!blNextJobFound && ... */
		if ( !blOptimalFound )
		{
			/* set start for next job */
			tlSplittedDebe = blNextJobFound ? tlTmpDeen : tlSplittedDeen;
			tlSplittedDeen = tlSplittedDebe + llMind;
			tlTmpLatestDeen = min(tlSplittedDebe + llMaxd,tpDeen);

			if ( tlSplittedDeen >= tlTmpLatestDeen )
			{
				dbg ( TRACE, "CreateSplittedMAJobs: break loop no split possible tlSplittedDeen <%ld>  tlTmpLatestDeen <%ld>",
					  tlSplittedDeen, tlTmpLatestDeen );
				blOptimalFound = TRUE;
			}

			llRowNum = ARR_FIRST;
		}
		ilKeyCount++;
		sprintf(clKey,"x%d",ilKeyCount);
    }	/* end of loop: while ( !blOptimalFound ) */
	dbg ( TRACE, "CreateSplittedMAJobs: Created %d jobs on Demand <%s>", ilJobCount, pcpDemUrno );
	
    return ilRc;
}

