REM Generierung der STANDARD Clients
REM
Echo Build Standard4.5 Clients Start > con 

Rem BEGIN
date /T 
time /T 


Rem ---Check for folder c:\ufis\system---
IF EXIST c:\ufis\system\nul GOTO cont1 
Echo Create folder C:\ufis\system 
md c:\ufis\system 
:cont1

IF EXIST C:\Ufis_Bin\Runtimedlls\nul goto cont2 
md C:\Ufis_Bin\Runtimedlls
:cont2

IF EXIST C:\tmp\nul goto cont3 
md C:\tmp 
:cont3

set logfile=C:\Ufis_Bin\AUH_Standard_Clients.txt

ECHO Project UFIS STANDARD 4.5 > %logfile%

mkdir c:\Ufis_Bin\ClassLib >> %logfile%
mkdir c:\Ufis_Bin\ClassLib\Include >> %logfile%
mkdir c:\Ufis_Bin\ClassLib\Include\jm >> %logfile%
mkdir C:\Ufis_Bin\ForaignLibs >> %logfile%
mkdir C:\Ufis_Bin\Help >> %logfile%

copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Regularexpressionlib\lib\*.lib C:\Ufis_Bin\ForaignLibs >> %logfile%
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Regularexpressionlib\lib\*.pdb C:\Ufis_Bin\ForaignLibs >> %logfile%
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Regularexpressionlib\lib\*.dll C:\Ufis_Bin\Runtimedlls >> %logfile%
copy ..\..\..\_Standard\_Standard_client\_Standard_configuration\Clientruntime\*.* C:\Ufis_Bin\Runtimedlls >> %logfile%
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Regularexpressionlib\include\*.h c:\Ufis_Bin\ClassLib\Include >> %logfile%
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Regularexpressionlib\include\jm\*.h c:\Ufis_Bin\ClassLib\Include\jm >> %logfile%
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_ClassLib\*.h c:\Ufis_Bin\ClassLib\Include >> %logfile%

copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Wrapper\*.h c:\Ufis_Bin\ClassLib\Include >> %logfile%
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Classlib\Classlib.rc c:\Ufis_Bin\ClassLib\Include >> %logfile%

copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_HtmlHelp\lib\*.lib C:\Ufis_Bin\ForaignLibs >> %logfile%
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_HtmlHelp\include\*.h c:\Ufis_Bin\ClassLib\Include >> %logfile%
copy ..\_Standard_help\*.chm c:\Ufis_Bin\help >> %logfile%

copy ..\AUH_configuration\*.ini c:\Ufis_Bin >> %logfile%
copy ..\AUH_configuration\*.cfg c:\Ufis_Bin >> %logfile%
copy ..\AUH_configuration\*.bmp c:\Ufis_Bin >> %logfile%
copy ..\AUH_configuration\TextTable.txt c:\Ufis_Bin >> %logfile%
copy ..\AUH_Dutyroster\_Standard_RosteringPrint\RosteringPrint.ulf c:\Ufis_Bin >> %logfile%

copy copy_for_install.bat c:\Ufis_Bin >> %logfile%

REM ------------------------Share-----------------------------------------------------------
cd ..\..\..\_Standard\_Standard_client

cd _Standard_Share\_Standard_Ufis32dll >> %logfile%
ECHO Build Ufis32.dll... > con
msdev Ufis32.dsp /MAKE "Ufis32 - Debug" /REBUILD >> %logfile%
msdev Ufis32.dsp /MAKE "Ufis32 - Release" /REBUILD >> %logfile%
If exist c:\Ufis_Bin\Debug\Ufis32.ilk del c:\Ufis_Bin\Debug\Ufis32.ilk >> %logfile%
If exist c:\Ufis_Bin\Debug\Ufis32.pdb del c:\Ufis_Bin\Debug\Ufis32.pdb >> %logfile%
If exist c:\Ufis_Bin\Debug\Ufis32.exp del c:\Ufis_Bin\Debug\Ufis32.exp >> %logfile%
If exist c:\Ufis_Bin\Release\Ufis32.ilk del c:\Ufis_Bin\Release\Ufis32.ilk >> %logfile%
If exist c:\Ufis_Bin\Release\Ufis32.pdb del c:\Ufis_Bin\Release\Ufis32.pdb >> %logfile%
If exist c:\Ufis_Bin\Release\Ufis32.exp del c:\Ufis_Bin\Release\Ufis32.exp >> %logfile%
copy c:\ufis_bin\release\Ufis32.dll c:\ufis_bin\debug\*.* >> %logfile%
copy c:\ufis_bin\release\Ufis32.lib c:\ufis_bin\debug\*.* >> %logfile%
cd ..\..

ECHO Build Classlib... > con
cd _Standard_Share\_Standard_ClassLib
msdev CCSClass.dsp /MAKE "CCSClass - Win32 Debug" /REBUILD >> %logfile%
msdev CCSClass.dsp /MAKE "CCSClass - Win32 Release" /REBUILD >> %logfile%
msdev CCSClass.dsp /MAKE "CCSClass - Win32 Dll Debug" /REBUILD >> %logfile%
msdev CCSClass.dsp /MAKE "CCSClass - Win32 Dll Release" /REBUILD >> %logfile%
cd ..\..

ECHO Build BcServ... > con
cd _Standard_Share\_Standard_BcServ
rem msdev BcServ32.dsp /MAKE "BcServ - Debug" /REBUILD
msdev BcServ32.dsp /MAKE "BcServ - Release" /REBUILD >> %logfile%
If exist c:\Ufis_Bin\Release\BcServ32.ilk del c:\Ufis_Bin\Release\BcServ32.ilk >> %logfile%
If exist c:\Ufis_Bin\Release\BcServ32.pdb del c:\Ufis_Bin\Release\BcServ32.pdb >> %logfile%
cd ..\..

ECHO Build BcProxy... > con
cd _Standard_Share\_Standard_BcProxy
Rem ---remove read-only state---
attrib *.* -r >> %logfile%
rem msdev BcProxy.dsp /MAKE "BcProxy - Win32 Debug" /REBUILD
msdev BcProxy.dsp /MAKE "BcProxy - Win32 Release" /REBUILD >> %logfile%
If exist c:\Ufis_Bin\Release\BcProxy.ilk del c:\Ufis_Bin\Release\BcProxy.ilk >> %logfile%
If exist c:\Ufis_Bin\Release\BcProxy.pdb del c:\Ufis_Bin\Release\BcProxy.pdb >> %logfile%
cd ..\..

ECHO Build UfisAppMng... > con
cd _Standard_Share\_Standard_UfisApManager
Rem ---remove read-only state---
attrib *.* -r
rem msdev UfisAppMng.dsp /MAKE "UfisAppMng - Win32 Debug" /REBUILD
msdev UfisAppMng.dsp /MAKE "UfisAppMng - Win32 Release" /REBUILD >> %logfile%
copy c:\ufis_bin\release\UFISAppMng.tlb c:\ufis\system >> %logfile%
If exist c:\Ufis_Bin\Release\UfisAppMng.ilk del c:\Ufis_Bin\Release\UfisAppMng.ilk >> %logfile%
If exist c:\Ufis_Bin\Release\UfisAppMng.pdb del c:\Ufis_Bin\Release\UfisAppMng.pdb >> %logfile%
cd ..\..

ECHO Build UfisApplMgr... > con
cd _Standard_Share\_Standard_UfisApplMgr 
Rem ---remove read-only state---
attrib *.* -r >> %logfile%
rem msdev UfisApplMgr.dsp /MAKE "UfisApplMgr - Win32 Debug" /REBUILD
msdev UfisApplMgr.dsp /MAKE "UfisApplMgr - Win32 Release" /REBUILD >> %logfile%
copy c:\ufis_bin\release\UFISApplMgr.tlb c:\ufis\system >> %logfile%
If exist c:\Ufis_Bin\Release\UfisApplMgr.ilk del c:\Ufis_Bin\Release\UfisApplMgr.ilk >> %logfile%
If exist c:\Ufis_Bin\Release\UfisApplMgr.pdb del c:\Ufis_Bin\Release\UfisApplMgr.pdb >> %logfile%
cd ..\..

ECHO Build Tabocx... > con
cd _Standard_Share\_Standard_TABocx
rem msdev TAB.dsp /MAKE "TAB - Win32 Debug" /REBUILD
msdev TAB.dsp /MAKE "TAB - Win32 Release" /REBUILD >> %logfile%
If exist c:\Ufis_Bin\Release\TAB.ilk del c:\Ufis_Bin\Release\TAB.ilk >> %logfile%
If exist c:\Ufis_Bin\Release\TAB.pdb del c:\Ufis_Bin\Release\TAB.pdb >> %logfile%
cd ..\..

ECHO Build UfisCedaocx... > con
cd _Standard_Share\_Standard_Ufiscedaocx 
rem msdev UfisCom.dsp /MAKE "UfisCom - Win32 Debug" /REBUILD
msdev UfisCom.dsp /MAKE "UfisCom - Win32 Release" /REBUILD >> %logfile%
If exist c:\Ufis_Bin\Release\UfisCom.ilk del c:\Ufis_Bin\Release\UfisCom.ilk >> %logfile%
If exist c:\Ufis_Bin\Release\UfisCom.pdb del c:\Ufis_Bin\Release\UfisCom.pdb >> %logfile%
cd ..\..

ECHO Build UComocx... > con
cd _Standard_Share\_Standard_UCom
rem msdev UCom.dsp /MAKE "UCom - Win32 Debug" /REBUILD
msdev UCom.dsp /MAKE "UCom - Win32 Release" /REBUILD >> %logfile%
If exist c:\Ufis_Bin\Release\UCom.ilk del c:\Ufis_Bin\Release\UCom.ilk >> %logfile%
If exist c:\Ufis_Bin\Release\UCom.pdb del c:\Ufis_Bin\Release\UCom.pdb >> %logfile%
cd ..\..

ECHO Build Ganttocx... > con
cd _Standard_Share\_Standard_Ganttocx
rem msdev UGantt.dsp /MAKE "UGantt - Win32 Debug" /REBUILD
msdev UGantt.dsp /MAKE "UGantt - Win32 Release" /REBUILD >> %logfile%
If exist c:\Ufis_Bin\Release\UGantt.ilk del c:\Ufis_Bin\Release\UGantt.ilk >> %logfile%
If exist c:\Ufis_Bin\Release\UGantt.pdb del c:\Ufis_Bin\Release\UGantt.pdb >> %logfile%
cd ..\..

ECHO Build TrafficLightocx... > con
cd _Standard_share\_Standard_TrafficLight
msdev TrafficLight.dsp /MAKE "TrafficLight - Win32 Release" /REBUILD >> %logfile%
If exist c:\Ufis_Bin\Release\TrafficLight.ilk del c:\Ufis_Bin\Release\TrafficLight.ilk >> %logfile%
If exist c:\Ufis_Bin\Release\TrafficLight.pdb del c:\Ufis_Bin\Release\TrafficLight.pdb >> %logfile%
cd ..\..

ECHO Build AatLoginocx... > con
cd _Standard_Share\_Standard_Login
rem msdev AatLogin.dsp /MAKE "AatLogin - Win32 Debug" /REBUILD
msdev AatLogin.dsp /MAKE "AatLogin - Win32 Release" /REBUILD >> %logfile%
If exist c:\Ufis_Bin\Release\AatLogin.ilk del c:\Ufis_Bin\Release\AatLogin.ilk >> %logfile%
If exist c:\Ufis_Bin\Release\AatLogin.pdb del c:\Ufis_Bin\Release\AatLogin.pdb >> %logfile%
cd ..\..

ECHO Build BCComServer... > con
cd _Standard_share\_Standard_BCComServer
Rem ---remove read-only state---
attrib *.* -r
msdev BCComServer.dsp /MAKE "BCComServer - Win32 Release" /REBUILD >> %logfile%
copy BCComServer.tlb c:\Ufis_Bin\Release
If exist c:\Ufis_Bin\Release\BCComServer.ilk del c:\Ufis_Bin\Release\BCComServer.ilk >> %logfile%
If exist c:\Ufis_Bin\Release\BCComServer.pdb del c:\Ufis_Bin\Release\BCComServer.pdb >> %logfile%
cd ..\..

ECHO Build UCEditocx... > con
cd _Standard_Share\_Standard_UCEditocx
msdev UCEdit.dsp /MAKE "UCEdit - Win32 Unicode Release" /REBUILD >> %logfile%
If exist c:\Ufis_Bin\Release\UCEdit.ilk del c:\Ufis_Bin\Release\UCEdit.ilk >> %logfile%
If exist c:\Ufis_Bin\Release\UCEdit.pdb del c:\Ufis_Bin\Release\UCEdit.pdb >> %logfile%
cd ..\..

ECHO Build Alerter... > con
cd _Standard_Share\_Standard_Alerter
msdev Alerter.dsp /MAKE "Alerter - Win32 Release" /REBUILD >> %logfile%
If exist c:\Ufis_Bin\Release\Alerter.ilk del c:\Ufis_Bin\Release\Alerter.ilk >> %logfile%
If exist c:\Ufis_Bin\Release\Alerter.pdb del c:\Ufis_Bin\Release\Alerter.pdb >> %logfile%
cd ..\..

ECHO Build BdpsPass... > con
cd _Standard_Share\_Standard_Bdpspass
msdev Bdpspass.dsp /MAKE "Bdpspass - Win32 Debug" /REBUILD >> %logfile%
rem msdev Bdpspass.dsp /MAKE "Bdpspass - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\Bdpspass.ilk del c:\Ufis_Bin\Debug\Bdpspass.ilk >> %logfile%
If exist c:\Ufis_Bin\Debug\Bdpspass.pdb del c:\Ufis_Bin\Debug\Bdpspass.pdb >> %logfile%
cd ..\..

ECHO Build BdpsSec... > con
cd _Standard_Share\_Standard_Bdpssec
msdev Bdps_sec.dsp /MAKE "Bdps_sec - Win32 Debug" /REBUILD >> %logfile%
rem msdev Bdps_sec.dsp /MAKE "Bdps_sec - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\Bdps_sec.ilk del c:\Ufis_Bin\Debug\Bdps_sec.ilk >> %logfile%
If exist c:\Ufis_Bin\Debug\Bdps_sec.pdb del c:\Ufis_Bin\Debug\Bdps_sec.pdb >> %logfile%
cd ..\..

ECHO Build BdpsUif... > con
cd _Standard_Share\_Standard_Bdpsuif
msdev Bdpsuif.dsp /MAKE "Bdpsuif - Win32 Debug" /REBUILD >> %logfile%
rem msdev Bdpsuif.dsp /MAKE "Bdpsuif - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\Bdpsuif.ilk del c:\Ufis_Bin\Debug\Bdpsuif.ilk >> %logfile%
If exist c:\Ufis_Bin\Debug\Bdpsuif.pdb del c:\Ufis_Bin\Debug\Bdpsuif.pdb >> %logfile%
cd ..\..

ECHO Build UniCodeDialog... > con
cd _Standard_share\_Standard_UCDialog
msdev UCDialog.dsp /MAKE "UCDialog - Win32 Debug" /REBUILD >> %logfile%
If exist c:\Ufis_Bin\Debug\UCDialog.ilk del c:\Ufis_Bin\Debug\UCDialog.ilk >> %logfile%
If exist c:\Ufis_Bin\Debug\UCDialog.pdb del c:\Ufis_Bin\Debug\UCDialog.pdb >> %logfile%
cd ..\..

ECHO Build LanguageTool... > con
cd _Standard_Share\_Standard_Languagetool
msdev CoCo3.dsp /MAKE "CoCo3 - Win32 Debug" /REBUILD >> %logfile%
rem msdev CoCo3.dsp /MAKE "CoCo3 - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\CoCo3.ilk del c:\Ufis_Bin\Debug\CoCo3.ilk >> %logfile%
If exist c:\Ufis_Bin\Debug\CoCo3.pdb del c:\Ufis_Bin\Debug\CoCo3.pdb >> %logfile%
cd ..\..

ECHO Build LogTabViewer... > con
cd _Standard_Share\_Standard_Loggingtableviewer 
vb6 /make LogTab_Viewer.vbp /out C:\Ufis_Bin\vbappl.log >> %logfile%
cd ..\..

ECHO Build Rules Checker... > con
cd _Standard_Share\_Standard_ConsistencyChecker 
vb6 /make RulesChecker.vbp /out C:\Ufis_Bin\vbappl.log >> %logfile%
cd ..\..

ECHO Build Ufis Database Tool... > con
cd _Standard_Share\_Standard_UfisDatabaseTool
vb6 /make UfisDatabaseTool.vbp /out C:\Ufis_Bin\vbappl.log >> %logfile%
cd ..\..

ECHO Build RMS Log Viewer... > con
cd _Standard_Share\_Standard_BdpssecReports
vb6 /make RMSLogViewer.vbp /out C:\Ufis_Bin\vbappl.log >> %logfile%
cd ..\..

ECHO Build Efficiency Reports... > con
cd _Standard_Share\_Standard_EfficiencyReports
vb6 /make EfficiencyReports.vbp /out C:\Ufis_Bin\vbappl.log >> %logfile%
cd ..\..


REM ------------------------Flight--------------------------------------

ECHO Build Fips... > con
rem cd _Standard_Flight\_Standard_Fips
cd C:\Sandboxes\UFIS4.5\branches\4.5.05\4.5.05.29_STD\_Standard\_Standard_Client\_Standard_Flight\_Standard_Fips
msdev FPMS.dsp /MAKE "FPMS - Win32 Debug" /REBUILD >> %logfile%
rem msdev FPMS.dsp /MAKE "FPMS - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\FIPS.ilk del c:\Ufis_Bin\Debug\FIPS.ilk >> %logfile%
If exist c:\Ufis_Bin\Debug\FIPS.pdb del c:\Ufis_Bin\Debug\FIPS.pdb >> %logfile%
cd ..\..

ECHO Build FipsCute... > con
rem cd _Standard_Flight\_Standard_Fipscute
cd C:\Sandboxes\UFIS4.5\Ufis\_Standard\_Standard_Client\_Standard_Flight\_Standard_Fipscute
msdev CuteIF.dsp /MAKE "CuteIF - Win32 Debug" /REBUILD >> %logfile%
rem msdev CuteIF.dsp /MAKE "CuteIF - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\CuteIF.ilk del c:\Ufis_Bin\Debug\CuteIF.ilk >> %logfile%
If exist c:\Ufis_Bin\Debug\CuteIF.pdb del c:\Ufis_Bin\Debug\CuteIF.pdb >> %logfile%
cd c:\

ECHO Build FipsRules... > con
cd C:\Sandboxes\UFIS4.5\branches\4.5.05\4.5.05.29_STD\_Standard\_Standard_Client\_Standard_Flight\_Standard_Ruleseditorflight
msdev Rules.dsp /MAKE "Rules - Win32 Debug" /REBUILD >> %logfile%
rem msdev Rules.dsp /MAKE "Rules - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\Rules.ilk del c:\Ufis_Bin\Debug\Rules.ilk >> %logfile%
If exist c:\Ufis_Bin\Debug\Rules.pdb del c:\Ufis_Bin\Debug\Rules.pdb >> %logfile%
cd C:\

ECHO Build FlightDataGenerator... > con
cd C:\Sandboxes\UFIS4.5\Ufis\_Standard\_Standard_Client\_Standard_Flight\_Standard_FlightDataGenerator
vb6 /make FlightDataGenerator.vbp /out C:\Ufis_Bin\vbappl.log >> %logfile%
cd ..\..

ECHO Build Telexpool... > con
cd _Standard_Flight\_Standard_Telexpool
vb6 /make Telexpool.vbp /out C:\Ufis_Bin\vbappl.log >> %logfile%
cd ..\..

ECHO Build LoadTabViewer... > con
cd _Standard_Flight\_Standard_LoaTabViewer
vb6 /make LoadTabViewer.vbp /out C:\Ufis_Bin\vbappl.log >> %logfile%
cd ..\..

ECHO Build ImportFlights... > con
cd _Standard_Flight\_Standard_ImportExportTool
vb6 /make ImportFlights.vbp /out C:\Ufis_Bin\vbappl.log >> %logfile%
cd ..\..

ECHO Build ExportTool... > con
cd _Standard_Flight\_Standard_ImportExportTool\VB6_ExportTool
vb6 /make Export.vbp /out C:\Ufis_Bin\vbappl.log >> %logfile%
cd ..\..\..

ECHO Build DaCo3Tool... > con
cd _Standard_Flight\_Standard_DaCo3Tool
vb6 /make DaCo3Tool.vbp /out C:\Ufis_Bin\vbappl.log >> %logfile%
cd ..\..

ECHO Build Flight Data Tool... > con
cd _Standard_Flight\_Standard_FlightDataTool\FlightDataTool
vb6 /make FlightDataTool.vbp /out C:\Ufis_Bin\vbappl.log >> %logfile%
cd ..\..\..

ECHO Build Online Changes Monitor... > con
cd _Standard_Flight\_Standard_FlightDataTool\RealTimeMonitor
vb6 /make OnlineChangesMonitor.vbp /out C:\Ufis_Bin\vbappl.log >> %logfile%
cd ..\..\..

REM ECHO Build Flight Audit Log... > con
REM cd _Standard_Flight\_Standard_FlightDataTool\FlightAuditLog
REM vb6 /make FlightAuditLog.vbp /out C:\Ufis_Bin\vbappl.log
REM cd ..\..\..

ECHO Build Ufis.Utils.dll... > con
cd _Standard_UfisAddOn\_Standard_Utilities
devenv Utilities.sln /Rebuild Release /project ufis.utils >> %logfile%
cd ..\..

ECHO Build ZedGraph... > con
cd _Standard_UfisAddOn\_Standard_ZedGraph
devenv ZedGraph.sln /Rebuild Release /project ZedGraph >> %logfile%
cd ..\..

ECHO Build FIPS Reports UserControls... > con
cd _Standard_Flight\_Standard_FIPSReport
devenv FIPS_Reports.sln /Rebuild Release /project UserControls >> %logfile%
cd ..\..

ECHO Build FIPS Reports... > con
cd _Standard_Flight\_Standard_FIPSReport
devenv FIPS_Reports.sln /Rebuild Release /project FIPS_Reports >> %logfile%
cd ..\..

ECHO Build Flight Permits... > con
cd _Standard_Flight\_Standard_Permit
devenv FlightPermits.sln /Rebuild Release /project FlightPermits >> %logfile%
cd ..\..

REM ------------------------Fids--------------------------------------

ECHO Build Fidas... > con
cd _Standard_Fids\_Standard_Fidas
msdev Fidas.dsp /MAKE "Fidas - Win32 Debug" /REBUILD >> %logfile%
rem msdev Fidas.dsp /MAKE "Fidas - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\Fidas.ilk del c:\Ufis_Bin\Debug\Fidas.ilk >> %logfile%
If exist c:\Ufis_Bin\Debug\Fidas.pdb del c:\Ufis_Bin\Debug\Fidas.pdb >> %logfile%
cd ..\..

REM -----------------------Dutyroster-------------------------------------

ECHO Build Coverage... > con
cd _Standard_Dutyroster\_Standard_Coverage
msdev Coverage.dsp /MAKE "Coverage - Win32 Debug" /REBUILD >> %logfile%
msdev Coverage.dsp /MAKE "Coverage - Win32 AutoCoverageDebug" /REBUILD >> %logfile%
rem msdev Coverage.dsp /MAKE "Coverage - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\Coverage.ilk del c:\Ufis_Bin\Debug\Coverage.ilk >> %logfile%
If exist c:\Ufis_Bin\Debug\Coverage.pdb del c:\Ufis_Bin\Debug\Coverage.pdb >> %logfile%
If exist c:\Ufis_Bin\Debug\AutoCoverage.ilk del c:\Ufis_Bin\Debug\AutoCoverage.ilk >> %logfile%
If exist c:\Ufis_Bin\Debug\AutoCoverage.pdb del c:\Ufis_Bin\Debug\AutoCoverage.pdb >> %logfile%
cd ..\..

ECHO Build InitAccount... > con
cd _Standard_Dutyroster\_Standard_Initaccount
msdev InitializeAccount.dsp /MAKE "InitializeAccount - Win32 Debug" /REBUILD >> %logfile%
rem msdev InitializeAccount.dsp /MAKE "InitializeAccount - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\InitializeAccount.ilk del c:\Ufis_Bin\Debug\InitializeAccount.ilk >> %logfile%
If exist c:\Ufis_Bin\Debug\InitializeAccount.pdb del c:\Ufis_Bin\Debug\InitializeAccount.pdb >> %logfile%
cd ..\..

ECHO Build Rostering... > con
cd _Standard_Dutyroster\_Standard_Rostering
rem msdev Rostering.dsp /MAKE "Rostering - Win32 Debug" /REBUILD
msdev Rostering.dsp /MAKE "Rostering - Win32 Release" /REBUILD >> %logfile%
If exist c:\Ufis_Bin\Release\Rostering.ilk del c:\Ufis_Bin\Release\Rostering.ilk >> %logfile%
If exist c:\Ufis_Bin\Release\Rostering.pdb del c:\Ufis_Bin\Release\Rostering.pdb >> %logfile%
cd ..\..

ECHO Build RosteringPrint... > con
cd _Standard_Dutyroster\_Standard_RosteringPrint
vb6 /make RosteringPrint.vbp /out C:\Ufis_Bin\vbappl.log >> %logfile%
cd ..\..

ECHO Build StaffTerm... > con
cd _Standard_Dutyroster\_Standard_Staffterm
msdev Staffterm.dsp /MAKE "Staffterm - Win32 Debug" /REBUILD >> %logfile%
rem msdev Staffterm.dsp /MAKE "Staffterm - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\Staffterm.ilk del c:\Ufis_Bin\Debug\Staffterm.ilk >> %logfile%
If exist c:\Ufis_Bin\Debug\Staffterm.pdb del c:\Ufis_Bin\Debug\Staffterm.pdb >> %logfile%
cd ..\..

ECHO Build StaffBalance... > con
cd _Standard_Dutyroster\_Standard_Staffbalance
Rem ---remove read-only state of help-files---
attrib .\hlp\*.* -r
msdev StaffBalance.dsp /MAKE "StaffBalance - Win32 Debug" /REBUILD >> %logfile%
rem msdev StaffBalance.dsp /MAKE "StaffBalance - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\StaffBalance.ilk del c:\Ufis_Bin\Debug\StaffBalance.ilk >> %logfile%
If exist c:\Ufis_Bin\Debug\StaffBalance.pdb del c:\Ufis_Bin\Debug\StaffBalance.pdb >> %logfile%
cd ..\..

ECHO Build AbsencePlanningTool... > con
cd _Standard_DutyRoster\_Standard_AbsencePlanning
vb6 /make AbsencePlanning.vbp /out C:\Ufis_Bin\vbappl.log >> %logfile%
cd ..\..


REM ---------------------------RMS---------------------------------------------------

ECHO Build StaticGroupEditor... > con
cd _Standard_RMS\_Standard_Staticgroupeditor
msdev Grp.dsp /MAKE "Grp - Win32 Debug" /REBUILD >> %logfile%
rem msdev Grp.dsp /MAKE "Grp - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\Grp.ilk del c:\Ufis_Bin\Debug\Grp.ilk >> %logfile%
If exist c:\Ufis_Bin\Debug\Grp.pdb del c:\Ufis_Bin\Debug\Grp.pdb >> %logfile%
cd ..\..

ECHO Build ServiceCatalog... > con
cd _Standard_RMS\_Standard_Servicecatalog
msdev ServiceCatalog.dsp /MAKE "ServiceCatalog - Win32 Debug" /REBUILD >> %logfile%
rem msdev ServiceCatalog.dsp /MAKE "ServiceCatalog - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\ServiceCatalog.ilk del c:\Ufis_Bin\Debug\ServiceCatalog.ilk >> %logfile%
If exist c:\Ufis_Bin\Debug\ServiceCatalog.pdb del c:\Ufis_Bin\Debug\ServiceCatalog.pdb >> %logfile%
cd ..\..

ECHO Build ProfileEditor... > con
cd _Standard_RMS\_Standard_ProfileEditor
msdev ProfileEditor.dsp /MAKE "ProfileEditor - Win32 Debug" /REBUILD >> %logfile%
rem msdev ProfileEditor.dsp /MAKE "ProfileEditor - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\ProfileEditor.ilk del c:\Ufis_Bin\Debug\ProfileEditor.ilk >> %logfile%
If exist c:\Ufis_Bin\Debug\ProfileEditor.pdb del c:\Ufis_Bin\Debug\ProfileEditor.pdb >> %logfile%
cd ..\..

ECHO Build PoolAllocation... > con
cd _Standard_RMS\_Standard_PoolAloc
msdev PoolAlloc.dsp /MAKE "PoolAlloc - Win32 Debug" /REBUILD >> %logfile%
rem msdev PoolAlloc.dsp /MAKE "PoolAlloc - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\PoolAlloc.ilk del c:\Ufis_Bin\Debug\PoolAlloc.ilk >> %logfile%
If exist c:\Ufis_Bin\Debug\PoolAlloc.pdb del c:\Ufis_Bin\Debug\PoolAlloc.pdb >> %logfile%
cd ..\..

ECHO Build RulesRms... > con
cd _Standard_RMS\_Standard_RulesEditorRms
msdev Regelwerk.dsp /MAKE "Regelwerk - Win32 Debug" /REBUILD >> %logfile%
rem msdev Regelwerk.dsp /MAKE "Regelwerk - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\Regelwerk.ilk del c:\Ufis_Bin\Debug\Regelwerk.ilk >> %logfile%
If exist c:\Ufis_Bin\Debug\Regelwerk.pdb del c:\Ufis_Bin\Debug\Regelwerk.pdb >> %logfile%
cd ..\..

ECHO Build OpssPm... > con
cd _Standard_RMS\_Standard_Opsspm
msdev OpssPm.dsp /MAKE "OpssPm - Win32 Debug" /REBUILD >> %logfile%
rem msdev OpssPm.dsp /MAKE "OpssPm - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\OpssPm.ilk del c:\Ufis_Bin\Debug\OpssPm.ilk >> %logfile%
If exist c:\Ufis_Bin\Debug\OpssPm.pdb del c:\Ufis_Bin\Debug\OpssPm.pdb >> %logfile%
cd ..\..

ECHO Build WgrTool... > con
cd _Standard_Rms\_Standard_Wgrtool
msdev Wgrtool.dsp /MAKE "Wgrtool - Win32 Debug" /REBUILD >> %logfile%
rem msdev Wgrtool.dsp /MAKE "Wgrtool - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Debug\Wgrtool.ilk del c:\Ufis_Bin\Debug\Wgrtool.ilk >> %logfile%
If exist c:\Ufis_Bin\Debug\Wgrtool.pdb del c:\Ufis_Bin\Debug\Wgrtool.pdb >> %logfile%
cd ..\..

REM ------------------------UfisAddOn---------------------------------------------------
ECHO Build AllocationPlan... > con
cd _Standard_UfisAddOn\_Standard_AllocationPlan
devenv AllocationPlan.sln /Rebuild Release /project Standard_AllocationPlan >> %logfile%
cd ..\..

ECHO _Standard_DataChanges... > con
cd _Standard_UfisAddOn\_Standard_AllocationPlan
devenv AllocationPlan.sln /Rebuild Release /project Standard_AllocationPlan >> %logfile%
cd ..\..

ECHO Build StatusManager... > con
cd _Standard_UfisAddOn\_Standard_StatusManager
devenv Status_Manager.sln /Rebuild Release /project Status_Manager >> %logfile%
cd ..\..

ECHO Build CheckListProcessing... > con
cd _Standard_UfisAddOn\_Standard_CheckListProcessing\CheckListProcessing
devenv CheckListProcessing.sln /Rebuild Release /project CheckListProcessing >> %logfile%
cd ..\..\..

ECHO Build LocationChanges... > con
cd _Standard_UfisAddOn\_Standard_DataChanges\Data_Changes
devenv Data_Changes.sln /Rebuild Release /project Data_Changes >> %logfile%
cd ..\..\..

ECHO Build HUB_Manager... > con
cd _Standard_UfisAddOn\_Standard_ConnectionManager
vb6 /make HUB_Manager.vbp /out C:\Ufis_Bin\vbappl.log >> %logfile%
cd ..\..

ECHO Build ULD_Manager... > con
cd _Standard_UfisAddOn\_Standard_ULDManager
vb6 /make ULD_Manager.vbp /out C:\Ufis_Bin\vbappl.log >> %logfile%
cd ..\..

ECHO Build StaffInfo... > con
cd _Standard_UfisAddOn\_Standard_StaffInfo
vb6 /make StaffInfo.vbp /out C:\Ufis_Bin\vbappl.log >> %logfile%
cd ..\..


:end
ECHO ...Done > con >> %logfile%
Rem END 
time/T >> %logfile%