REM Generierung der STANDARD Clients
REM
Echo Build Standard4.5 Clients > con

Rem BEGIN
date /T
time /T 


Rem ---Check for folder c:\ufis\system---
IF EXIST c:\ufis\system\nul GOTO cont1
Echo Create folder C:\ufis\system
md c:\ufis\system
:cont1

IF EXIST C:\Ufis_Bin\Runtimedlls\nul goto cont2
md C:\Ufis_Bin\Runtimedlls
:cont2

IF EXIST C:\tmp\nul goto cont3
md C:\tmp
:cont3

set logfile=C:\Ufis_Bin\Std_WPF_Client.txt


ECHO Project UFIS STANDARD 4.5 > %logfile%

rem -----------STANDARD_SHARE_FOR_WPF-------------------
setlocal >> %logfile%
call "C:\Program Files\Microsoft Visual Studio 10.0\VC\vcvarsall.bat" x86 >> %logfile%

pushd C:\Sandboxes\UFIS4.5\Ufis\AUH\AUH_Client\AUH_ClientMake >> %logfile%

REM ufis.Entities.dll.....
cd ..\..\.. >> %logfile%
ECHO Build Ufis.Entities..> con >> %logfile%
cd _Standard\_Standard_Client\_Standard_Share\_Standard_Ufis.Entities >> %logfile%
msbuild Ufis.Entities.sln /t:Rebuild /p:Configuration=Release >> %logfile%

rem ufis.IO.dll
cd ..\ >> %logfile%
ECHO Build Ufis.IO..> con >> %logfile%
cd _Standard_Ufis.IO >> %logfile%
msbuild Ufis.IO.sln /t:Rebuild /p:Configuration=Release >> %logfile%

rem ufis.ADLoginUser.dll
cd ..\ >> C:\Ufis_Bin\STANDARD.txt
ECHO Build Ufis.ADLoginUser..> con >> %logfile%
cd _Standard_Ufis.ADLoginUser\Ufis.ADLoginUser >> %logfile%
msbuild Ufis.ADLoginUser.sln /t:Rebuild /p:Configuration=Release >> %logfile%

rem ufis.Broadcast.dll
cd ..\.. >> %logfile%
ECHO Build Ufis.Broadcast..>> %logfile%
cd _Standard_Ufis.Broadcast >> %logfile%
msbuild Ufis.Broadcast.sln /t:Rebuild /p:Configuration=Release >> %logfile%

rem Ufis.Broadcast.ceda.dll
cd ..\ >> %logfile%
ECHO Build Ufis.Broadcast.Ceda..> con >> %logfile%
cd _Standard_Ufis.Broadcast.Ceda >> %logfile%
msbuild Ufis.Broadcast.Ceda.sln /t:Rebuild /p:Configuration=Release >> %logfile%

rem Ufis.Data.dll
cd ..\ >> %logfile%
ECHO Build Ufis.Data..>> %logfile%
cd _Standard_Ufis.Data >> %logfile%
msbuild Ufis.Data.sln /t:Rebuild /p:Configuration=Release >> %logfile%

rem Ufis.Resources.ImageLibrary.dll
cd ..\ >> %logfile%
ECHO Build Ufis.Resources.ImageLibrary..> con >> %logfile%
cd _Standard_Ufis.Resources.ImageLibrary >> %logfile%
msbuild Ufis.Resources.ImageLibrary.sln /t:Rebuild /p:Configuration=Release >> %logfile%


rem Ufis.Security.dll
cd ..\ >> %logfile%
ECHO Build _Standard_Ufis.Security..> con >> %logfile%
cd _Standard_Ufis.Security >> %logfile%
msbuild Ufis.Security.sln /t:Rebuild /p:Configuration=Release >> %logfile%

rem Ufis.Security.Ceda.dll
cd ..\ >> %logfile%
ECHO Build _Standard_Ufis.Security.Ceda..> con >> %logfile%
cd _Standard_Ufis.Security.Ceda >> %logfile%
msbuild Ufis.Security.Ceda.sln /t:Rebuild /p:Configuration=Release >> %logfile%

rem Ufis.MVVM.dll
cd ..\ >> %logfile%
ECHO Build _Standard_Ufis.MVVM..> con >> %logfile%
cd _Standard_Ufis.MVVM >> %logfile%
msbuild Ufis.MVVM.sln /t:Rebuild /p:Configuration=Release >> %logfile%

rem Ufis.Resources.dll
cd ..\ >> %logfile%
ECHO Build _Standard_Ufis.Resources..> con >> %logfile%
cd _Standard_Ufis.Resources >> %logfile%
msbuild Ufis.Resources.sln /t:Rebuild /p:Configuration=Release >> %logfile%

rem Ufis.Utilities.dll
cd ..\ >> %logfile%
ECHO Build _Standard_Ufis.Utilities..> con >> %logfile%
cd _Standard_Ufis.Utilities >> %logfile%
msbuild Ufis.Utilities.sln /t:Rebuild /p:Configuration=Release >> %logfile%

rem Ufis.UserLayout.dll
cd ..\ >> %logfile%
ECHO Build _Standard_Ufis.UserLayout..> con >> %logfile%
cd _Standard_Ufis.UserLayout >> %logfile%
msbuild Ufis.UserLayout.sln /t:Rebuild /p:Configuration=Release >> %logfile%

rem Ufis.Flight.Filter.dll
cd ..\ >> %logfile%
ECHO Build _Standard_Ufis.Flight.Filter..> con >> %logfile%
cd _Standard_Ufis.Flight.Filter >> %logfile%
msbuild Ufis.Flight.Filter.sln /t:Rebuild /p:Configuration=Release >> %logfile%

rem ------------ build WPF Applications -------------------

rem AllocationPlan.exe
cd ..\.. >> %logfile%
ECHO Build_Standard_AllocationPlan..> con >> %logfile%
cd _Standard_UfisAddOn\_Standard_AllocationPlan >> %logfile%
msbuild AllocationPlan.sln /t:Rebuild /p:Configuration=Release >> %logfile%

rem FIM.exe
cd ..\ >> %logfile%
ECHO Build_Standard_FIM..> con >> %logfile%
cd _Standard_FIM >> %logfile%
msbuild FIM.sln /t:Rebuild /p:Configuration=Release >> %logfile%

rem FlightViewer.exe
cd ..\ >> %logfile%
ECHO Build_Standard_FlightViewer..> con >> %logfile%
cd _Standard_FlightViewer >> %logfile%
msbuild FlightViewer.sln /t:Rebuild /p:Configuration=Release >> %logfile%

rem Standard_Reports.exe
cd ..\ >> %logfile%
ECHO Build_Standard_Reports..> con >> %logfile%
cd _Standard_Reports >> %logfile%
msbuild Standard_Reports.sln /t:Rebuild /p:Configuration=Release >> %logfile%

rem GroundControlManager.exe
cd ..\.. >> %logfile%
ECHO _Standard_GroundControlManager..> con >> %logfile%
cd _Standard_Flight\_Standard_GroundControlManager >> %logfile%
msbuild GroundControlManager.sln /t:Rebuild /p:Configuration=Release >> %logfile%
 
popd >> %logfile%
endlocal >> %logfile%

:end 

ECHO ...Done > con >> %logfile%
Rem END 
time/T >> %logfile%
