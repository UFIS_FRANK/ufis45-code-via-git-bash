General Terms and Conditions of Purchase and Sale of UFIS Airport Solutions GmbH

�1 General
�1.1 The terms and conditions stated herein shall be valid for and apply to all business transactions with UFIS Airport Solutions GmbH (hereafter referred to as UFIS-AS).
�1.2 These general business terms shall also be applicable or operative with respect to contrary conditions incorporated by Buyer, even if such terms are not expressly objected to. The terms and conditions stated herein shall also be applicable with regard to all future business transactions, even if no reference is made to them in each and every individual case.
�2 Conclusion of Contract
�2.1 All purchase offers shall be subject to confirmation with respect to execution, quantities, prices, delivery period, and availability of goods.
�2.2 The sales contract shall enter into full force and effect only upon written confirmation of order and upon a written agreement between the Buyer and UFIS-AS concerning the services to be performed.
�2.3 All documents shall remain the property of UFIS-AS. Upon request, they must be returned to UFIS-AS at any time. They may only be made available to third parties upon prior written consent of UFIS-AS. 
�2.4 During the term of this contract, Buyer shall commission other consulting firms to supply goods or the performance of services which are subject to the contract awarded to UFIS-AS only after prior consultation with UFIS-AS.
�2.5 UFIS-AS shall be entitled to appoint sub-contractors for the performance of its contractual services or the delivery of goods under a contract awarded to it. 
�3 Subject Matter of the Contract
�3.1 In accordance with the terms and conditions stated in the order and the order confirmation, the subject matter of this contract shall be:
a) the right of use of Standard Software pursuant with the provision of the relevant documentation and/or specification of services,
b) The development and right of use of Individual Software according to the contents of the system analysis and/or the Contract Requirements Manual,
c) any alteration to and/or modification of software and other software services pursuant to the applicable price list. 
�3.2 The delivery of data carriers and other corporeal objects shall not be subject matter of this contract; if necessary, a separate contract must be concluded for this purpose.
�4 Scope of Services
�4.1 The Buyer shall be provided with the Standard Software products including a verbal description (user documentation with operating manual). 
�4.2 As far as Individual Software products are concerned, UFIS-AS shall conduct the programming and system tests on the basis of the system analysis. Furthermore, UFIS-AS shall prepare the user documentation. 
�4.3 As far as Standard and Individual Software products are concerned, UFIS-AS shall provide software operating training for the Buyer or user on the respective system against payment of an additional charge. 
�4.4 The aforementioned provisions shall also apply to program alterations and/or modifications and other software services. 
�4.5 The performance data contained in the system analyses, documentation etc. are for descriptive purposes only and shall not be deemed a warranty of particular qualities. Such a warranty requires an explicit and separate agreement.


�5 Buyer Support
�5.1 The Buyer shall immediately provide UFIS-AS with all information required to perform the contractual services.
�5.2 Upon request, Buyer shall make available to UFIS-AS sufficient test times on the customer application computer system.
�5.3 Additional services and delays, which become necessary due to incorrect or incomplete statements made by the Buyer shall be borne by the latter.
�5.4 As a matter of principle, UFIS-AS will perform the contractual services ate its principal place of business. Should on-site services on the part of UFIS-AS become necessary, deviating from the contractual provisions agreed, all additional costs incurred by UFIS-AS will be charge separately.
�5.5 Buyer shall adhere to the service and maintenance instructions of UFIS-AS and shall especially replace obsolete data carriers in due course. All costs resulting from the non-compliance with this provision shall be borne by Buyer, even during the stipulated warranty period.
�6 Supply and Service Times
�6.1 In order to comply with the supply and service times agreed upon in writing, it is indispensable that all necessary documents have been submitted and all other services to be rendered have been performed properly and in due time; otherwise all delivery and supply times will be extended accordingly. 
�6.2 Should a failure to comply with the supply and service times be caused by force majeure, disturbances in production, operation or transport, strikes, lock-outs, war, riots or civil commotion, mobilisation, or by any other circumstances beyond the reasonable control of UFIS-AS, supply and service times shall be extended accordingly.
�6.3 In the event that such failure to comply with the final delivery date is the responsibility of UFIS-AS, Buyer - provided he can prove to have suffered a damage or loss from such a delay - is entitled to liquidate damages in the amount of 0.5% for every full week of delay up to a total of 5% of the value of that part of the deliveries or services that could not be used or performed in due time because of these deliveries  or services had not been made or performed within the agreed time period. Further claims for damages on the part of the Buyer which exceed the aforestated amount of 5% shall be excluded in all cases even after the expiry of a reasonable period of grace. This shall not be applicable in cases where the scope of liability is prescribed by an imperative statutory provision governing acts of intent or gross negligence. 
�7 Acceptance
�7.1 Acceptance of goods supplied and the services rendered shall be made in the form of a system acceptance test carried out by UFIS-AS; for this purpose, test data provided by the Buyer shall be used. The system test shall be carried out immediately upon Notice of the Technical System Operability on the user system (customer application computer system) or another appropriate system.
�7.2 if any deviation from the Contract requirements manual (Detailed Specification) becomes apparent during the system test which prevent the proper application of the software products, UFIS-AS shall arrange without any further delay, a demonstration of the agreed program specifications as laid down in the Contractual Requirements Manual with the help of the test data provided. 
�7.3 Following the completion of the first system test, an additional test shall be carried out immediately.
�7.4 If the system test reveals that the services of UFIS-AS correspond to the specification, Buyer shall confirm the same in writing by a Certificate of Acceptance.
�7.5 
If the system test was not carried out within 10 days after Notice of Technical Operability by UFIS-AS due to reasons for which UFIS-AS was not responsible and if Buyer operates the supplies and services of UFIS-AS, it will be assumed that the supplies made and the services rendered by UFIS-AS have been accepted. UFIS-AS shall make references to these legal consequences in the notice of technical operability.
�8 Terms of Payment
�8.1 The total fee agreed upon shall be due and payable as follows:
* 30% upon order, payable not later than 10 days upon confirmation of order by UFIS-AS;
* 30% upon expiry of half the contractual delivery period;
* 30% upon acceptance of the programs after execution of the system acceptance tests or, in case of delayed acceptance by Buyer, no later than 10 days after Notice of Technical Operability has been given by UFIS-AS;
* 10% after installation, but no later than 4 weeks after initial operation of the system by Buyer or 4 weeks after maturity of the last partial payment, provided that the delay in acceptance was not the fault of UFIS-AS. 
�8.2 Insofar as services performed are invoiced on a time basis, UFIS-AS will charge at its rates valid at the time. The fees are based on a 5 day working week of 8 hours per day taking into account all times spent for travelling. If the fees are calculated on a time basis, UFIS-AS will make out interim monthly invoices.
�8.3 Buyer shall, if not agreed otherwise by the parties, bear the following charges:
* charges for accommodation, board and lodging of UFIS-AS employees working at the installation site within the maximum rates admissible under applicable tax laws;
* travel costs of UFIS-AS employees to and from the installation site within the maximum rates admissible under tax law; each member of staff is entitled to travel home within the area of the Federal Republic of Germany once a week; all charges and travel costs will be charged by UFIS-AS on a monthly basis. Travelling time is considered to be working time and will be charged separately, insofar as legally admissible.
�8.4 In addition to the agreed prices, Buyer must pay UFIS-AS the statutory VAT, if any.
�8.5 All invoices are payable without any deduction immediately upon receipt, but no later than 14 days from the date of invoice.
�8.6 Payment must be made by means of a bank transfer or by cheque and without additional costs to UFIS-AS. Any other method of payment requires prior written consent of UFIS-AS. Discounting charges or interest if any, must be reimbursed to UFIS-AS. Regardless of the method of payment, any amount due shall be deemed settled the day on which UFIS-AS can unrestrictedly dispose of these moneys.
�8.7 In the event that term of payment agreed upon is exceeded by more than 14 days, Buyer must pay interest of 5% p.a. and after date of first request for payment, interest equal to interest payable by UFIS-AS to its bank for refinancing purposes. 
�9 Licence
�9.1 Against payment of the contractually stipulated compensation, UFIS-AS will grant to Buyer a non-exclusive and non-transferable licence for the use of the software products and documentation.
�9.2 Against payment of the contractually stipulated compensation, UFIS-AS will also grant to Buyer a non-exclusive and non-transferable licence for the use of the know-how which was made available to him by UFIS-AS.
�9.3 Buyer will use the software exclusively on the hardware stipulated in the take-over statement and on the program carriers released by UFIS-AS. Buyer shall treat the software products and documentation as well as know-how strictly confidential and will meet al safety precautions required to protect the products from being disclosed to unauthorised third parties.
�9.4 In the event of a culpable violation of sub-clauses 2 and 3 of this clause, UFIS-AS will - irrespective of the right to claim further damages - retain the right to request payment of a contractual penalty amounting to DM 20,000 in each individual case of such culpable violation, provided,  however, that is case of a continued commission of the same offence these commissions shall not be regarded as only one violation. 
�9.5 Subject to the written consent of UFIS-AS, Buyer shall be entitled to adapt and/or alter the software products and documentation to its specific purposes at its own cost and risk.
�10 Warranties and Liabilities
�10.1 In the event that the deliveries made and the services performed by UFIS-AS are not in compliance with the contractual covenants regarding the scope of services to be rendered, Buyer is obliged to inform UFIS-AS to this in writing no later than 10 working days following delivery or performance. In the event of hidden defects, the period of notice shall commence at the time the defect was discovered for the first time. Any defects are to be described and specified in detail in such notice of defects.
�10.2 UFIS-AS shall be obliged to remedy a defect if notification of defect was made in due time. This obligation shall lapse, if Buyer has intervened in the deliveries and services of UFIS-AS. If works are to be executed by UFIS-AS at the installation site, Buyer shall bear all charges mentioned in �8.3. 
�10.3 The warranty period shall be 12 months. The warranty period shall commence upon the date of acceptance or, in case of delayed acceptance on the part of Buyer, it shall begin with the date of notification of completion by UFIS-AS.
�10.4 Claims against UFIS-AS, its representatives or agents arising from the impossibility of performance, default, non-performance, positive violation of claims, culpa in contrahendo, and tortious acts shall be excluded, if such damage was not caused intentionally or by gross negligence. All damage claims - insofar as legally permissible - expire no later than upon the expiry of the agreed warranty period (see �10.3). 
�11 Prohibition Against Hiring Away Employees
During the term of the contractual relationship between UFIS-AS and Buyer and within the period of 12 months after termination of this relationship, Buyer will not employ any UFIS-AS employees in his own business nor will he employ said persons in any other way or in any other affiliated companies.
�12 Ownership and Copyright
�12.1 All ownership rights and copyrights regarding all organisation documents, systems, programs, form drafts, and data carriers which are provided by UFIS-AS will remain the property of UFIS-AS .
�12.2 If a licence is granted to Buyer, the latter is not entitled to take possession of all source programs, coding records and documentation. Buyer is entitled to take possession of all source programs, coding records and documentation only if the programs were developed especially for him, if the complete costs for organisation and programming were paid by him and if the contract was duly performed for him. Irrespective of this, UFIS-AS is entitled to continue using the programs or parts thereof.
�12.3 At any time, UFIS-AS is entitled to alter, expand or replace programs prepared by it with newly developed programs.
�13 Final Provisions
�13.1 Insofar as Buyer and user are not identical, the Buyer has assigned its rights vis-�-vis UFIS-AS to the user, or if the user asserts rights of Buyer against UFIS-AS, these General Business Terms shall similarly apply to the user. 
�13.2 Place of performance for the purpose of all transactions shall be UFIS-AS's principal place of business.
�13.3 In case of any litigation arising herefrom, the courts of Mannheim shall have exclusive jurisdiction.
�13.4 All contractual relations between UFIS-AS and Buyer shall be governed exclusively by the laws of the Federal Republic of Germany.
�13.5 Any contractual alterations or amendments must be made in writing. Any verbal subsidiary agreements shall be invalid.
�13.6 Should any terms and conditions be invalid or unenforceable, such terms shall be replaced by new terms which reasonably approximate to the purpose of the agreements hereunder.
Revision 1.01 Date: February 1, 2005

