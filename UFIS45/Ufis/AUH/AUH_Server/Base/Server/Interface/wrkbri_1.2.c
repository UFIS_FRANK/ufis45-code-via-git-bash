#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/AUH/AUH_Server/Base/Server/Interface/wrkbri.c 1.2 2012/08/03 03:40:00SGT fya Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :                                                           */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */
/* 20030630 JIM: may core!: dbg(TRACE,"Command: <%s>,prlCmdblk->command")     */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */

static char sccs_version[] ="@(#) UFIS44 (c) ABB AAT/I skeleton.c 44.1.0 / 11.12.2002 HEB";

/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */


#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <unistd.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "fditools.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "rmschkrul.h"
#include "syslib.h"
#include "db_if.h"
#include <time.h>
#include <cedatime.h>
#include <stdarg.h>
#include <cedatime.h>
#include "helpful.h"

#ifdef _HPUX_SOURCE
	extern int daylight;
	extern long timezone;
	extern char *tzname[2];
#else
	extern int _daylight;
	extern long _timezone;
	extern char *_tzname[2];
#endif

int debug_level = 0;
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static EVENT *prgOutEvent  = NULL;

static char cgProcessName[20] = "\0";
static char  cgConfigFile[512] = "\0";
static char  cgHopo[8] = "\0";                         /* default home airport    */
static char  cgTabEnd[8] ="\0";                       /* default table extension */

static long lgEvtCnt = 0;

//Frank 20120803 v1.2
static char pcgCodeForALC2[126] = "\0";
static int igLastPartOfCSGN = 0;
//Frank@WorkBridge
//#define TEST
//#define TEST1
#define READ_BUFF 10000
#define RC_NOTFOUND -130
static int	igIs8AM = FALSE;
static char pcgMainDelimiter[2];
static char pcgMinorDelimiter[2];
static int  igDoUpdateFile;
static int  igDoDailyFile;
static char pcgUpdateFilePath[256];
static char pcgUpdateWorkPath[256];
static char pcgDailyFilePath[256];
static char pcgDailyWorkPath[256];
static char pcgUpdateFlightRangeLow[15];
static char pcgUpdateFlightRangeHigh[15];
static int  igUpdateFlightRangeLow;
static int  igUpdateFlightRangeHigh;
static int  igUpdateResetToMidnight;
static int  igDailyResetToMidnight;
static char pcgDailyFlightRangeLow[15];
static char pcgDailyFlightRangeHigh[15];
static int  igDailyFlightRangeLow;
static int  igDailyFlightRangeHigh;
static char pcgUpdateExcludeFtyp[256];
static char pcgUpdateExcludeTtyp[256];
static char pcgUpdateExcludeStyp[256];
static char pcgDailyExcludeFtyp[256];
static char pcgDailyExcludeTtyp[256];
static char pcgDailyExcludeStyp[256];
static char pcgLoadDSSN[256];
static char pcgValBookF[256];
static char pcgValBookB[256];
static char pcgValBookE[256];
static char pcgValActualF[256];
static char pcgValActualB[256];
static char pcgValActualE[256];
static char pcgValConfigF[256];
static char pcgValConfigB[256];
static char pcgValConfigE[256];
static char pcgValCargo[256];
static char pcgValMail[256];
static char pcgValBags[256];
static char pcgDSSN[256];
static char pcgDailyFileName[256];
static char pcgUpdateFileName[256];
static char pcgDailyFileNameFilePath[256];
static char pcgUpdateFileNameFilePath[256];
static char pcgCurrDateForDaily[15], pcgDayOfWeek[3];
static char pcgCurrDateForUpdate[15];
static char pcgConfigFile[512];
static char pcgCfgBuffer[512];

typedef struct 
{
    short SqlCursor;
    short SqlFunc;
    char 	SqlBuf[2048];
    char 	SqlBufCommon[2048];
    char 	SqlBufMostCommon[2048];
} SQLREC;

typedef struct
{
	char URNO[13];
	char FLNO[13];
	char ALC2[13];//CR
	char ALC3[16];//Frank 20120803 v1.2
	char CSGN[16];//Frank 20120803 v1.2
	char FLTN[13];//FLNO-CR-FLNS
	char FLNS[13];
	char REGN[13];//REG
	char ORG3[13];//DEP
	char DES3[13];//ARR
	char STOD[15];//STD
	char STOA[15];//STA
	char ETDI[15];//ETD
	char ETAI[15];//ETA
	char AIRB[15];//ATD
	char LAND[15];//ATA
	char FTYP[13];//ST
	char ACT3[13];//AC
	char STYP[13];//FT
	char PSTA[13];//ST
	char PSTD[13];//ST
	char GTA1[13];//GATE
	char GTD1[13];//GATE
	char CKIF[13];//CHKFROM
	char CKIT[13];//CHKTO
	char STEV[13];//TERM
	char ADID[13];//for PSTA/PSTD
} AFTTAB;

typedef struct
{
	char PRMTName[100];
	char PRMTCount[100];
} DPXTAB;

typedef struct
{
	char ValBookFCount[100];
	char ValBookBCount[100];
	char ValBookECount[100];
	char ValActualFCount[100];
	char ValActualBCount[100];
	char ValActualECount[100];
	char ValConfigFCount[100];
	char ValConfigBCount[100];
	char ValConfigECount[100];
	char ValCargoCount[100];
	char ValMailCount[100];
	char ValBagsCount[100];
} LOATAB;

typedef struct
{
	char FLNU[100];
	char ALC2[100];
	char FLTN[100];
	char FLNS[100];
	char ETAI[100];
	char PAXCount[100];
	char BagCount[100];
} TRANSFER;

//Frank@WorkBridge

/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
static int  Init_Process();
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,char *pcpCfgBuffer);
static int	Reset(void);                       /* Reset program          */
static void	Terminate(int ipSleep);            /* Terminate program      */
static void	HandleSignal(int);                 /* Handles signals        */
static void	HandleErr(int);                    /* Handles general errors */
static void   HandleQueErr(int);                 /* Handles queuing errors */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/
static int	HandleData(EVENT *prpEvent);       /* Handles event data     */
static int  GetCommand(char *pcpCommand, int *pipCmd);
//Frank@WorkBridge
extern int RunSql( char *pcpSqlBuf, char *pcpSqlData );
static int GetCurrDateForDaily(void);
static int GetCurrDateForUpdate(void);
static char *GetCurrTime(void);
void dbglog(char *fmt, ...);
static int TrimSpace( char *pcpInStr );
extern int GetNoOfElements(char *s, char c);
extern int  get_real_item (char *, char *, int);
int  get_real_item_semicolon (char *, char *, int);
extern int  get_item(int, char *, char *, int, char *, char *, char *);
static void TrimRight(char *pcpBuffer);
//Frank@WorkBridge

//Frank v1.1b
int IsALC2_FLNO_FLTN_STOA_STOD_NULL(AFTTAB rpAFTTAB);
//Frank v1.2
static int IsAlc2FlnoFltnNullForAdHocFlight(AFTTAB rpAFTTAB, char *pcpALC2, char *pcpFLTN, char *pcpFLNS);
/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
	int	ilRc = RC_SUCCESS;			/* Return code			*/
	int	ilCnt = 0;
	int ilOldDebugLevel = 0;


	INITIALIZE;			/* General initialization	*/



	debug_level = TRACE;

	strcpy(cgProcessName,argv[0]);

	dbg(TRACE,"MAIN: version <%s>",sccs_version);

	/* Attach to the MIKE queues */
	do
	{
		ilRc = init_que();
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		}/* end of if */
	}while((ilCnt < 10) && (ilRc != RC_SUCCESS));

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
		sleep(60);
		exit(1);
	}else{
		dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
	}/* end of if */

	do
	{
		ilRc = init_db();
		if (ilRc != RC_SUCCESS)
		{
			check_ret(ilRc);
			dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		} /* end of if */
	} while((ilCnt < 10) && (ilRc != RC_SUCCESS));

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
		sleep(60);
		exit(2);
	}else{
		dbg(TRACE,"MAIN: init_db() OK!");
	} /* end of if */

	/* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */

	sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
	dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
	ilRc = TransferFile(cgConfigFile);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */

	sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
	dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
	ilRc = TransferFile(cgConfigFile);
	if(ilRc != RC_SUCCESS)
	{ 
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */

	ilRc = SendRemoteShutdown(mod_id);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
	} /* end of if */

	if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
	{
		dbg(DEBUG,"MAIN: waiting for status switch ...");
		HandleQueues();
	}/* end of if */

	if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
	{
		dbg(TRACE,"MAIN: initializing ...");
			ilRc = Init_Process();
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"Init_Process: init failed!");
			} /* end of if */

	} else {
		Terminate(30);
	}/* end of if */



	dbg(TRACE,"=====================");
	dbg(TRACE,"MAIN: initializing OK");
	dbg(TRACE,"Caution, I'm not a skeleton, and do something");
	dbg(TRACE,"=====================");
	

	for(;;)
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* Acknowledge the item */
		ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
		if( ilRc != RC_SUCCESS ) 
		{
			/* handle que_ack error */
			HandleQueErr(ilRc);
		} /* fi */

		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */

		prgEvent = (EVENT *) prgItem->text;
				
		if( ilRc == RC_SUCCESS )
		{
			
			lgEvtCnt++;

			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				break;	
			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				/* CloseConnection(); */
				HandleQueues();
				break;	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(1);
				break;	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				break;	
			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;
			case	SHUTDOWN	:
				/* process shutdown - maybe from uutil */
				Terminate(1);
				break;
					
			case	RESET		:
				ilRc = Reset();
				break;
					
			case	EVENT_DATA	:
				if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
				{
					ilRc = HandleData(prgEvent);
					if(ilRc != RC_SUCCESS)
					{
						HandleErr(ilRc);
					}/* end of if */
				}else{
					dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
				}/* end of if */
				break;
				

			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;
			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;
			default			:
				dbg(TRACE,"MAIN: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */

		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
		
	} /* end for */
	
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/

static int ReadConfigEntry(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer)
{
    int ilRc = RC_SUCCESS;

    char clSection[124] = "\0";
    char clKeyword[124] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRc = iGetConfigEntry(cgConfigFile,clSection,clKeyword,
			   CFG_STRING,pcpCfgBuffer);
    if(ilRc != RC_SUCCESS)
    {
	dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
    } 
    else
    {
	dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s",
	    clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
    }/* end of if */
    return ilRc;
}


static int Init_Process()
{
	int  ilRc = RC_SUCCESS;			/* Return code */
  char clSection[64] = "\0";
  char clKeyword[64] = "\0";
  int ilOldDebugLevel = 0;
	long pclAddFieldLens[12];
	char pclAddFields[256] = "\0";
	char pclSqlBuf[2560] = "\0";
	char pclSelection[1024] = "\0";
 	short slCursor = 0;
	short slSqlFunc = 0;
	char  clBreak[24] = "\0";
	
	char pclTmpBuf[256] = "\0";
	char pclDebugLevel[128] = "\0";

	debug_level = DEBUG;

	if(ilRc == RC_SUCCESS)
	{
		/* read HomeAirPort from SGS.TAB */
		ilRc = tool_search_exco_data("SYS", "HOMEAP", cgHopo);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"<Init_Process> EXTAB,SYS,HOMEAP not found in SGS.TAB");
			Terminate(30);
		} else {
			dbg(TRACE,"<Init_Process> home airport <%s>",cgHopo);
		}
	}
	
	if(ilRc == RC_SUCCESS)
	{

		ilRc = tool_search_exco_data("ALL","TABEND", cgTabEnd);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"<Init_Process> EXTAB,ALL,TABEND not found in SGS.TAB");
			Terminate(30);
		} else {
			dbg(TRACE,"<Init_Process> table extension <%s>",cgTabEnd);
		}
	}

	if (ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"Init_Process failed");
	}
	
	sprintf(pcgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
  dbg(TRACE,"Config File is <%s>",pcgConfigFile);	
  
  //getting config entries
  //MAIN
  dbg(DEBUG,"------------MAIN Config Section starts------------");
  ilRc = iGetConfigEntry(pcgConfigFile,"MAIN","DEBUG_LEVEL",CFG_STRING,pclDebugLevel);
  if(ilRc == RC_SUCCESS)
  {	
	  if (strcmp(pclDebugLevel,"DEBUG") == 0)
	  {
	     debug_level = DEBUG;
	  }
	  else
	  {
	     if (strcmp(pclDebugLevel,"TRACE") == 0)
	     {
	        debug_level = TRACE;
	     }
	     else
	     {
	        if (strcmp(pclDebugLevel,"NULL") == 0)
	        {
	           debug_level = 0;
	        }
	        else
	        {
	           debug_level = TRACE;
	        }
	     }
	  }
  }
  else
  {
  	debug_level = DEBUG;//default value
  }
  switch(debug_level)
  {
  	case DEBUG:
  		dbg(DEBUG,"debug_level = DEBUG");
  		break;
  	case TRACE:
  		dbg(DEBUG,"debug_level = TRACE");
  		break;
  	default:
  		dbg(DEBUG,"debug_level = UNKNOWN or NULL");
  		break;
  }
  
  ilRc = iGetConfigEntry(pcgConfigFile,"MAIN","DO_UPDATE_FILE",CFG_STRING,pclTmpBuf);
  if(ilRc == RC_SUCCESS)
	{
		if (strcmp(pclTmpBuf,"YES") == 0)
		{
			igDoUpdateFile = TRUE;
		}	
		else
		{
			igDoUpdateFile = FALSE;
		}	
	}
	else
	{
		igDoUpdateFile = TRUE;//default value
	}
	if(igDoUpdateFile == TRUE)
	{
		dbg(DEBUG,"DO_UPDATE_FILE = YES");
	}
	else
	{
		dbg(DEBUG,"DO_UPDATE_FILE = NO");
	}
	
	ilRc = iGetConfigEntry(pcgConfigFile,"MAIN","DO_DAILY_FILE",CFG_STRING,pclTmpBuf);
  if(ilRc == RC_SUCCESS)
	{
		if (strcmp(pclTmpBuf,"YES") == 0)
		{
			igDoDailyFile = TRUE;
		}	
		else
		{
			igDoDailyFile = FALSE;
		}	
	}
	else
	{
		igDoDailyFile = TRUE;//default value
	}
	if(igDoDailyFile == TRUE)
	{
		dbg(DEBUG,"DO_DAILY_FILE = YES");
	}
	else
	{
		dbg(DEBUG,"DO_DAILY_FILE = NO");
	}
  
  ilRc = iGetConfigEntry(pcgConfigFile,"MAIN","Main_Delemiter",CFG_STRING,pclTmpBuf);
  if(ilRc == RC_SUCCESS)
  {
  	strcpy(pcgMainDelimiter,pclTmpBuf);
  }
  else
  {
  	sprintf(pcgMainDelimiter,";");//default value	
  }
  dbg(DEBUG,"MainDelemiter = %s",pcgMainDelimiter);
  
  ilRc = iGetConfigEntry(pcgConfigFile,"MAIN","Minor_Delemiter",CFG_STRING,pclTmpBuf);
  if(ilRc == RC_SUCCESS)
  {
  	strcpy(pcgMinorDelimiter,pclTmpBuf);
  }
  else
  {
  	sprintf(pcgMinorDelimiter,",");//default value	
  }
  dbg(DEBUG,"pcgMinorDelimiter = %s",pcgMinorDelimiter);
  dbg(DEBUG,"------------MAIN Config Section ends------------");

  //Frank 20120803 v1.2
  ilRc = iGetConfigEntry(pcgConfigFile,"MAIN","CODE_FOR_CSGN",CFG_STRING,pclTmpBuf);
  if( ilRc == RC_SUCCESS )
  {
	strcpy(pcgCodeForALC2,pclTmpBuf);
        dbg(TRACE,"pcgCodeForALC2<%s>",pcgCodeForALC2);
  }
  else
  {
	memset(pcgCodeForALC2,0,sizeof(pcgCodeForALC2));
  }
  
  ilRc = iGetConfigEntry(pcgConfigFile,"MAIN","LAST_PART_OF_CSGN",CFG_STRING,pclTmpBuf);
  if( ilRc == RC_SUCCESS )
  {
	igLastPartOfCSGN = atoi(pclTmpBuf);
	dbg(TRACE,"igLastPartOfCSGN is <%d>",igLastPartOfCSGN);
  }
  else
  {	
	igLastPartOfCSGN = 0;	
  }
  //Frank 20120803 v1.2
  
  //UPDATE
  dbg(DEBUG,"------------UPDATE Config Section starts------------");
  ilRc = iGetConfigEntry(pcgConfigFile,"UPDATE","file_path",CFG_STRING,pclTmpBuf);
  if(ilRc == RC_SUCCESS)
  {
  	strcpy(pcgUpdateFilePath,pclTmpBuf);
  }
  else
  {
  	sprintf(pcgUpdateFilePath,"/ceda/exco/FILES/WORKBRIDGE/");	
  }
  dbg(DEBUG,"UPDATE file_path = %s",pcgUpdateFilePath);
  
  ilRc = iGetConfigEntry(pcgConfigFile,"UPDATE","work_path",CFG_STRING,pclTmpBuf);
  if(ilRc == RC_SUCCESS)
  {
  	strcpy(pcgUpdateWorkPath,pclTmpBuf);
  }
  else
  {
  	sprintf(pcgUpdateWorkPath,"/ceda/exco/FILES/WORKBRIDGE/arc/");	
  }
  dbg(DEBUG,"UPDATE work_path = %s",pcgUpdateWorkPath);//pcgUpdateFilePath
  
  //GetCurrDateForUpdate();
  GetServerTimeStamp("UTC",2,0,pcgCurrDateForDaily);
  ilRc = iGetConfigEntry(pcgConfigFile,"UPDATE","file_name",CFG_STRING,pclTmpBuf);
  if(ilRc == RC_SUCCESS)
  {
  	strcpy(pcgUpdateFileName,pclTmpBuf);
  }
  else
  {
  	sprintf(pcgUpdateFileName,"%sUFISWB%s.txt",pcgUpdateWorkPath,pcgCurrDateForUpdate);
  }
  //dbg(DEBUG,"UPDATE file_name = %s",pcgUpdateFileName);
  
  ilRc = iGetConfigEntry(pcgConfigFile,"UPDATE","flight_range",CFG_STRING,pclTmpBuf);
  if(ilRc == RC_SUCCESS)
  {
  	get_real_item(pcgUpdateFlightRangeLow, pclTmpBuf,1);
    get_real_item(pcgUpdateFlightRangeHigh,pclTmpBuf,2);
    igUpdateFlightRangeLow  = atoi(pcgUpdateFlightRangeLow);
    igUpdateFlightRangeHigh = atoi(pcgUpdateFlightRangeHigh);
  }
  else
  {
  	igUpdateFlightRangeLow  = -24;
  	igUpdateFlightRangeHigh = 24;
  }	
  dbg(DEBUG,"UPDATE flight_range = %d,%d",igUpdateFlightRangeLow,igUpdateFlightRangeHigh);
  
  ilRc = iGetConfigEntry(pcgConfigFile,"UPDATE","reset_to_midnight",CFG_STRING,pclTmpBuf);
  //dbg(DEBUG,"<<<<<<<<<<<<<<<<<<<<%s",pclTmpBuf);
  if(ilRc == RC_SUCCESS)
  {
  	if(strcmp(pclTmpBuf,"N")==0)
  	{
  		igUpdateResetToMidnight = FALSE;
  	}
  	else
  	{
  		igUpdateResetToMidnight = TRUE;
  	}
	}
	if(igUpdateResetToMidnight == TRUE)
	{
		dbg(DEBUG,"UPDATE reset_to_midnight = Y");
	}
	else
	{
		dbg(DEBUG,"UPDATE reset_to_midnight = N");
	}
	
	ilRc = iGetConfigEntry(pcgConfigFile,"UPDATE","exclude_ftyp",CFG_STRING,pclTmpBuf);
	if(ilRc == RC_SUCCESS)
	{
		strcpy(pcgUpdateExcludeFtyp,pclTmpBuf);
	}
	else
	{
		sprintf(pcgUpdateExcludeFtyp,"'X','N','T','G'");
	}
	dbg(DEBUG,"UPDATE exclude_ftyp = %s",pcgUpdateExcludeFtyp);
	
	ilRc = iGetConfigEntry(pcgConfigFile,"UPDATE","exclude_ttyp",CFG_STRING,pclTmpBuf);
	if(ilRc == RC_SUCCESS)
	{
		strcpy(pcgUpdateExcludeTtyp,pclTmpBuf);
	}
	/*else
	{
		sprintf(pcgUpdateExcludeTtyp,"'9'");
	}*/
	dbg(DEBUG,"UPDATE exclude_ttyp = %s",pcgUpdateExcludeTtyp);
	
	ilRc = iGetConfigEntry(pcgConfigFile,"UPDATE","exclude_styp",CFG_STRING,pclTmpBuf);
	if(ilRc == RC_SUCCESS)
	{
		strcpy(pcgUpdateExcludeStyp,pclTmpBuf);
	}
	/*else
	{
		sprintf(pcgUpdateExcludeStyp,"'9'");
	}*/
	dbg(DEBUG,"UPDATE exclude_styp = %s",pcgUpdateExcludeStyp);
	
	dbg(DEBUG,"------------UPDATE Config Section ends------------");
	
	//Daily
	dbg(DEBUG,"------------DAILY Config Section starts------------");
	ilRc = iGetConfigEntry(pcgConfigFile,"DAILY","file_path",CFG_STRING,pclTmpBuf);
  if(ilRc == RC_SUCCESS)
  {
  	strcpy(pcgDailyFilePath,pclTmpBuf);
  }
  else
  {
  	sprintf(pcgDailyFilePath,"/ceda/exco/FILES/WORKBRIDGE/");	
  }
  dbg(DEBUG,"DAILY file_path = %s",pcgDailyFilePath);
  
  ilRc = iGetConfigEntry(pcgConfigFile,"DAILY","work_path",CFG_STRING,pclTmpBuf);
  if(ilRc == RC_SUCCESS)
  {
  	strcpy(pcgDailyWorkPath,pclTmpBuf);
  }
  else
  {
  	sprintf(pcgDailyFilePath,"/ceda/exco/FILES/WORKBRIDGE/arc/");	
  }
  dbg(DEBUG,"DAILY file_path = %s",pcgDailyWorkPath);
  
  /*GetCurrDateForDaily();*/
  GetServerTimeStamp("UTC",2,0,pcgCurrDateForDaily);
  
  ilRc = iGetConfigEntry(pcgConfigFile,"DAILY","file_name",CFG_STRING,pclTmpBuf);
  if(ilRc == RC_SUCCESS)
  {
  	strcpy(pcgDailyFileName,pclTmpBuf);
  }
  else
  {
  	sprintf(pcgDailyFileName,"%sUFISWB%s080000.txt",pcgDailyWorkPath,pcgCurrDateForDaily);
  }
   dbg(DEBUG,"DAILY file_name = %s",pcgDailyFileName);
  
  ilRc = iGetConfigEntry(pcgConfigFile,"DAILY","flight_range",CFG_STRING,pclTmpBuf);
  if(ilRc == RC_SUCCESS)
  {
    get_real_item(pcgDailyFlightRangeLow, pclTmpBuf,1);
    get_real_item(pcgDailyFlightRangeHigh,pclTmpBuf,2);
    igDailyFlightRangeLow  = atoi(pcgDailyFlightRangeLow);
    igDailyFlightRangeHigh = atoi(pcgDailyFlightRangeHigh);
  }
  else
  {
  	igDailyFlightRangeLow  = 0;
  	igDailyFlightRangeHigh = 72;
  }
  dbg(DEBUG,"DAILY flight_range = %d,%d",igDailyFlightRangeLow,igDailyFlightRangeHigh);
  
  ilRc = iGetConfigEntry(pcgConfigFile,"DAILY","reset_to_midnight",CFG_STRING,pclTmpBuf);
  //dbg(DEBUG,"<<<<<<<<<<<<<<<<<<<<%s",pclTmpBuf);
  if(ilRc == RC_SUCCESS)
  {
  	if(strcmp(pclTmpBuf,"N")==0)
  	{
  		igDailyResetToMidnight = FALSE;
  	}
  	else
  	{
  		igDailyResetToMidnight = TRUE;
  	}
	}
	if(igDailyResetToMidnight == TRUE)
	{
		dbg(DEBUG,"DAILY reset_to_midnight = Y");
	}
	else
	{
		dbg(DEBUG,"DAILY reset_to_midnight = N");
	}
	
	ilRc = iGetConfigEntry(pcgConfigFile,"DAILY","exclude_ftyp",CFG_STRING,pclTmpBuf);
	if(ilRc == RC_SUCCESS)
	{
		strcpy(pcgDailyExcludeFtyp,pclTmpBuf);
	}
	/*else
	{
		sprintf(pcgDailyExcludeFtyp,"'X','N','T','G'");
	}
	*/
	dbg(DEBUG,"DAILY exclude_ftyp = %s",pcgDailyExcludeFtyp);
	
	ilRc = iGetConfigEntry(pcgConfigFile,"DAILY","exclude_ttyp",CFG_STRING,pclTmpBuf);
	if(ilRc == RC_SUCCESS)
	{
		strcpy(pcgDailyExcludeTtyp,pclTmpBuf);
	}
	/*else
	{
		sprintf(pcgDailyExcludeTtyp,"'9'");
	}
	*/
	dbg(DEBUG,"DAILY exclude_ttyp = %s",pcgDailyExcludeTtyp);
	
	ilRc = iGetConfigEntry(pcgConfigFile,"DAILY","exclude_styp",CFG_STRING,pclTmpBuf);
	if(ilRc == RC_SUCCESS)
	{
		strcpy(pcgDailyExcludeStyp,pclTmpBuf);
	}
	/*else
	{
		sprintf(pcgDailyExcludeTtyp,"'9'");
	}
	*/
	dbg(DEBUG,"DAILY exclude_styp = %s",pcgDailyExcludeStyp);
 	dbg(DEBUG,"------------DAILY Config Section ends------------");
 
 	//Load
 	dbg(DEBUG,"------------LOAD Config Section starts------------");
 	ilRc = iGetConfigEntry(pcgConfigFile,"LOAD","LOAD_DSSN",CFG_STRING,pclTmpBuf);
 	if(ilRc == RC_SUCCESS)
 	{
 			strcpy(pcgLoadDSSN,pclTmpBuf);
 	}
 	else
 	{
 		sprintf(pcgLoadDSSN,"USR,PFI,AIM,LDM,PTM,DLS");
 	}
 	dbg(DEBUG,"LOAD_DSSN = %s",pcgLoadDSSN);
 	
 	ilRc = iGetConfigEntry(pcgConfigFile,"LOAD","VAL_BOOK_F",CFG_STRING,pclTmpBuf);
 	if(ilRc == RC_SUCCESS)
 	{
 			strcpy(pcgValBookF,pclTmpBuf);
 	}
 	else
 	{
 		sprintf(pcgValBookF,"LOA;PXB;F;#;#;");
 	}
 	dbg(DEBUG,"VAL_BOOK_F = %s",pcgValBookF);
	
 	ilRc = iGetConfigEntry(pcgConfigFile,"LOAD","VAL_BOOK_B",CFG_STRING,pclTmpBuf);
 	if(ilRc == RC_SUCCESS)
 	{
 			strcpy(pcgValBookB,pclTmpBuf);
 	}
 	else
 	{
 		sprintf(pcgValBookB,"LOA;PXB;B;#;#;");
 	}
 	dbg(DEBUG,"VAL_BOOK_B = %s",pcgValBookB);
 	
 	ilRc = iGetConfigEntry(pcgConfigFile,"LOAD","VAL_BOOK_E",CFG_STRING,pclTmpBuf);
 	if(ilRc == RC_SUCCESS)
 	{
 			strcpy(pcgValBookE,pclTmpBuf);
 	}
 	else
 	{
 		sprintf(pcgValBookE,"LOA;PXB;E;#;#;");
 	}
 	dbg(DEBUG,"VAL_BOOK_E = %s",pcgValBookE);
 	
 	ilRc = iGetConfigEntry(pcgConfigFile,"LOAD","VAL_ACTUAL_F",CFG_STRING,pclTmpBuf);
 	if(ilRc == RC_SUCCESS)
 	{
 			strcpy(pcgValActualF,pclTmpBuf);
 	}
 	else
 	{
 		sprintf(pcgValActualF,"LOA;POB;F;#;#;");
 	}
 	dbg(DEBUG,"VAL_ACTUAL_F = %s",pcgValActualF);
 	
 	ilRc = iGetConfigEntry(pcgConfigFile,"LOAD","VAL_ACTUAL_B",CFG_STRING,pclTmpBuf);
 	if(ilRc == RC_SUCCESS)
 	{
 			strcpy(pcgValActualB,pclTmpBuf);
 	}
 	else
 	{
 		sprintf(pcgValActualB,"LOA;POB;B;#;#;");
 	}
 	dbg(DEBUG,"VAL_ACTUAL_B = %s",pcgValActualB);
 	
 	ilRc = iGetConfigEntry(pcgConfigFile,"LOAD","VAL_ACTUAL_E",CFG_STRING,pclTmpBuf);
 	if(ilRc == RC_SUCCESS)
 	{
 			strcpy(pcgValActualE,pclTmpBuf);
 	}
 	else
 	{
 		sprintf(pcgValActualE,"LOA;POB;E;#;#;");
 	}
 	dbg(DEBUG,"VAL_ACTUAL_E = %s",pcgValActualE);
 	
 	ilRc = iGetConfigEntry(pcgConfigFile,"LOAD","VAL_CONFIG_F",CFG_STRING,pclTmpBuf);
 	if(ilRc == RC_SUCCESS)
 	{
 			strcpy(pcgValConfigF,pclTmpBuf);
 	}
 	else
 	{
 		sprintf(pcgValConfigF,"LOA;PCF;F;#;#;");
 	}
 	dbg(DEBUG,"VAL_CONFIG_F = %s",pcgValConfigF);
 	
 	ilRc = iGetConfigEntry(pcgConfigFile,"LOAD","VAL_CONFIG_B",CFG_STRING,pclTmpBuf);
 	if(ilRc == RC_SUCCESS)
 	{
 			strcpy(pcgValConfigB,pclTmpBuf);
 	}
 	else
 	{
 		sprintf(pcgValConfigB,"LOA;PCF;B;#;#;");
 	}
 	dbg(DEBUG,"VAL_CONFIG_B = %s",pcgValConfigB);
 	
 	ilRc = iGetConfigEntry(pcgConfigFile,"LOAD","VAL_CONFIG_E",CFG_STRING,pclTmpBuf);
 	if(ilRc == RC_SUCCESS)
 	{
 			strcpy(pcgValConfigE,pclTmpBuf);
 	}
 	else
 	{
 		sprintf(pcgValConfigE,"LOA;PCF;E;#;#;");
 	}
 	dbg(DEBUG,"VAL_CONFIG_E = %s",pcgValConfigE);
 	
 	ilRc = iGetConfigEntry(pcgConfigFile,"LOAD","VAL_CARGO",CFG_STRING,pclTmpBuf);
 	if(ilRc == RC_SUCCESS)
 	{
 			strcpy(pcgValCargo,pclTmpBuf);
 	}
 	else
 	{
 		sprintf(pcgValCargo,"LOA;LOA;C;#;#;");
 	}
 	dbg(DEBUG,"VAL_CARGO = %s",pcgValCargo);
 	
 	ilRc = iGetConfigEntry(pcgConfigFile,"LOAD","VAL_MAIL",CFG_STRING,pclTmpBuf);
 	if(ilRc == RC_SUCCESS)
 	{
 			strcpy(pcgValMail,pclTmpBuf);
 	}
 	else
 	{
 		sprintf(pcgValMail,"LOA;LOA;M;#;#;");
 	}
 	dbg(DEBUG,"VAL_MAIL = %s",pcgValMail);
 	
 	ilRc = iGetConfigEntry(pcgConfigFile,"LOAD","VAL_BAGS",CFG_STRING,pclTmpBuf);
 	if(ilRc == RC_SUCCESS)
 	{
 			strcpy(pcgValBags,pclTmpBuf);
 	}
 	else
 	{
 		sprintf(pcgValBags,"LOA;LOA;B;#;#;");
 	}
 	dbg(DEBUG,"VAL_BAGS = %s",pcgValBags);
 	
 	dbg(DEBUG,"------------LOAD Config Section ends------------");
 	
 	//Transfer
 	dbg(DEBUG,"------------TRANSFER Config Section starts------------");
 	ilRc = iGetConfigEntry(pcgConfigFile,"TRANSFER","DSSN",CFG_STRING,pclTmpBuf);
 	if(ilRc == RC_SUCCESS)
 	{
 			strcpy(pcgDSSN,pclTmpBuf);
 	}
 	else
 	{
 		sprintf(pcgDSSN,"USR,PFI,PTM");
 	}
 	dbg(DEBUG,"DSSN = %s",pcgDSSN);
 	dbg(DEBUG,"------------TRANSFER Config Section ends------------");
 	
  return(ilRc);
} /* end of initialize */

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
	int	ilRc = RC_SUCCESS;				/* Return code */
	
	dbg(TRACE,"Reset: now resetting");
	
	return ilRc;
	
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
	/* unset SIGCHLD ! DB-Child will terminate ! */

	dbg(TRACE,"Terminate: now DB logoff ...");

	signal(SIGCHLD,SIG_IGN);

	logoff();

	dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


	dbg(TRACE,"Terminate: now leaving ...");

	fclose(outp);

	sleep(ipSleep < 1 ? 1 : ipSleep);
	
	exit(0);
	
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/

static void HandleSignal(int pipSig)
{
	dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);

	switch(pipSig)
	{
	default	:
		Terminate(1);
		break;
	} /* end of switch */

	exit(1);
	
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
	dbg(TRACE,"HandleErr: <%d> : unknown ERROR",pipErr);

	return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
	switch(pipErr) {
	case	QUE_E_FUNC	:	/* Unknown function */
		dbg(TRACE,"<%d> : unknown function",pipErr);
		break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
		dbg(TRACE,"<%d> : malloc failed",pipErr);
		break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
		dbg(TRACE,"<%d> : msgsnd failed",pipErr);
		break;
	case	QUE_E_GET	:	/* Error using msgrcv */
		dbg(TRACE,"<%d> : msgrcv failed",pipErr);
		break;
	case	QUE_E_EXISTS	:
		dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
		break;
	case	QUE_E_NOFIND	:
		dbg(TRACE,"<%d> : route not found ",pipErr);
		break;
	case	QUE_E_ACKUNEX	:
		dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
		break;
	case	QUE_E_STATUS	:
		dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
		break;
	case	QUE_E_INACTIVE	:
		dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
		break;
	case	QUE_E_MISACK	:
		dbg(TRACE,"<%d> : missing ack ",pipErr);
		break;
	case	QUE_E_NOQUEUES	:
		dbg(TRACE,"<%d> : queue does not exist",pipErr);
		break;
	case	QUE_E_RESP	:	/* No response on CREATE */
		dbg(TRACE,"<%d> : no response on create",pipErr);
		break;
	case	QUE_E_FULL	:
		dbg(TRACE,"<%d> : too many route destinations",pipErr);
		break;
	case	QUE_E_NOMSG	:	/* No message on queue */
		dbg(TRACE,"<%d> : no messages on queue",pipErr);
		break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
		dbg(TRACE,"<%d> : invalid originator=0",pipErr);
		break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
		dbg(TRACE,"<%d> : queues are not initialized",pipErr);
		break;
	case	QUE_E_ITOBIG	:
		dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
		break;
	case	QUE_E_BUFSIZ	:
		dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
		break;
	default			:	/* Unknown queue error */
		dbg(TRACE,"<%d> : unknown error",pipErr);
		break;
	} /* end switch */
         
	return;

} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	int	ilBreakOut = FALSE;
	
	do
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;	

		if( ilRc == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRc != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRc);
			} /* fi */
		
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				ilBreakOut = TRUE;
				break;	

			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(1);
				break;	
	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				ilBreakOut = TRUE;
				break;	

			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;

			case	SHUTDOWN	:
				Terminate(1);
				break;
						
			case	RESET		:
				ilRc = Reset();
				break;
						
			case	EVENT_DATA	:
				dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
					
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;

			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;

			default			:
				dbg(TRACE,"HandleQueues: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
	} while (ilBreakOut == FALSE);

			ilRc = Init_Process();
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitDemhdl: init failed!");
			} 
     

} /* end of HandleQueues */
	
/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{
	int	   ilRc           = RC_SUCCESS;			/* Return code */
	int    ilCmd          = 0;

	BC_HEAD *prlBchead       = NULL;
	CMDBLK  *prlCmdblk       = NULL;
	char    *pclSelection    = NULL;
	char    *pclFields       = NULL;
	char    *pclData         = NULL;
	char    *pclRow          = NULL;
	char 		clUrnoList[2400];
	char 		clTable[34];
	int			ilUpdPoolJob = TRUE;
	
//Frank@WorkBridge
	SQLREC rlAFTSQL, rlDPXSQL, rlLOASQL;
	SQLREC rlTRANSFERSQL,rlTRANSFERSQL1,rlTRANSFERSQL2,rlTRANSFERSQL3;
	
	AFTTAB rlAFTTAB;
	DPXTAB rlDPXTAB;
	LOATAB rlLOATAB;
	TRANSFER rlTRANSFER;
	
	char pclSqlBuf[1024];
  char pclSqlData[2048] = "\0";
  char pclTmpStr[2048];
  
  int ilI = 0;
  int ilJ = 0;
  int ilFlag = FALSE;
	int ilNoEle = 0;
	char pclDSSNSource[10][100];
	char pclDSSNSourceForTransfer[10][100];
	char pclValBookF[10][100];
	char pclValBookB[10][100];
	char pclValBookE[10][100];
	char pclValActualF[10][100];
	char pclValActualB[10][100];
	char pclValActualE[10][100];
	char pclValConfigF[10][100];
	char pclValConfigB[10][100];
	char pclValConfigE[10][100];
	char pclValCargo[10][100];
	char pclValMail[10][100];
	char pclValBags[10][100];	 
	int ilIsNull = TRUE;
	
	int ilIsNotExisted = TRUE;
	int ilNextDSSNforLOATAB = TRUE;
	
	//Frank v1.2 20120803
	char pclALC2[16] = "\0";
	char pclFLNS[16] = "\0";
	char pclFLTN[16] = "\0";
	//Frank v1.1b
	int ilLineCount = 0;
	
	char flno1[9];
	char flno2[9];
	FILE *fptrDaily,*fptrUpdate,*fptrDailyCopy,*fptrUpdateCopy;
	char buff[READ_BUFF];
	
	int ilRead = 0;	
	
	memset(&buff,0,sizeof(buff));
//Frank@WorkBridge
	
	igIs8AM = FALSE;

	prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
	prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
	pclSelection = prlCmdblk->data;
	pclFields    = pclSelection + strlen(pclSelection) + 1;
	pclData      = pclFields + strlen(pclFields) + 1;


	strcpy(clTable,prlCmdblk->obj_name);

	dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);

	/****************************************/
	//DebugPrintBchead(TRACE,prlBchead);
	//DebugPrintCmdblk(TRACE,prlCmdblk);
	/* 20030630 JIM: may core!: dbg(TRACE,"Command: <%s>,prlCmdblk->command"); */
	dbg(TRACE,"Command: <%s>",prlCmdblk->command);
	dbg(TRACE,"originator follows event = %p ",prpEvent);

	dbg(TRACE,"originator<%d>",prpEvent->originator);
	dbg(TRACE,"selection follows Selection = %p ",pclSelection);
	dbg(TRACE,"selection <%s>",pclSelection);
	dbg(TRACE,"fields    <%s>",pclFields);
	dbg(TRACE,"data      <%s>",pclData);
	/****************************************/
	
	dbg(DEBUG,"line<%5d>before",__LINE__);
	if(strncmp(prlCmdblk->command,"UPD",3)==0 || strncmp(prlCmdblk->command,"DAY",3)==0)
	{
		if(strncmp(prlCmdblk->command,"DAY",3)==0)
		{
			igIs8AM = TRUE;
		}
		else
		{
			igIs8AM = FALSE;
		}
		//dbg(DEBUG,"line<%5d>hello auhapp, I am wrkbri",__LINE__);
		
		//Frank@WorkBridge
		
		//1.Getting the records from AFTTAB
		dbg(DEBUG,"Frank@WorkBridge<line-%d>Getting the records from AFTTAB",__LINE__);
		
		/*GetCurrDateForUpdate();*/
		GetServerTimeStamp("UTC",1,0,pcgCurrDateForUpdate);
		/*GetServerTimeStamp("UTC",2,0,pcgCurrDateForDaily);*/
		/*
		strcpy(pcgDailyFlightRangeLow,pcgCurrDateForUpdate);
		strcpy(pcgDailyFlightRangeHigh,pcgCurrDateForUpdate);
		strcpy(pcgUpdateFlightRangeLow,pcgCurrDateForUpdate);
		strcpy(pcgUpdateFlightRangeHigh,pcgCurrDateForUpdate);
		*/
		
		/*AddSecondsToCEDATime(pcgDailyFlightRangeLow, igDailyFlightRangeLow*60*60,1);*/
    GetServerTimeStamp("UTC",1,igDailyFlightRangeLow*60*60,pcgDailyFlightRangeLow);
    
    /*AddSecondsToCEDATime(pcgDailyFlightRangeHigh,igDailyFlightRangeHigh*60*60,1);*/
    GetServerTimeStamp("UTC",1,igDailyFlightRangeHigh*60*60,pcgDailyFlightRangeHigh);
    
    /*AddSecondsToCEDATime(pcgUpdateFlightRangeLow, igUpdateFlightRangeLow*60*60,1);*/
    GetServerTimeStamp("UTC",1,igUpdateFlightRangeLow*60*60,pcgUpdateFlightRangeLow);
    
    /*AddSecondsToCEDATime(pcgUpdateFlightRangeHigh,igUpdateFlightRangeHigh*60*60,1);*/
    GetServerTimeStamp("UTC",1,igUpdateFlightRangeHigh*60*60,pcgUpdateFlightRangeHigh);
    
    dbg(DEBUG,"<line %d>pcgCurrDateForDaily = %s",__LINE__,pcgCurrDateForDaily);
    dbg(DEBUG,"pcgCurrDateForUpdate = %s",pcgCurrDateForUpdate);
    dbg(DEBUG,"pcgDailyFlightRangeLow = %s",pcgDailyFlightRangeLow);
    dbg(DEBUG,"pcgDailyFlightRangeHigh = %s",pcgDailyFlightRangeHigh);
    dbg(DEBUG,"pcgUpdateFlightRangeLow = %s",pcgUpdateFlightRangeLow);
    dbg(DEBUG,"pcgUpdateFlightRangeHigh = %s",pcgUpdateFlightRangeHigh);
    
    memset( &rlAFTSQL, 0, sizeof(SQLREC) );
    sprintf( rlAFTSQL.SqlBuf, "SELECT URNO,FLNO,ALC2,ALC3,CSGN,FLTN,FLNS,"//1-7
			"REGN,ORG3,DES3,STOD,STOA,ETDI,"//8-13
		 	"ETAI,AIRB,LAND,FTYP,ACT3,STYP,"//14-19
		 	"PSTA,PSTD,GTA1,GTD1,"//20-23
		 	"CKIF,CKIT,STEV,ADID "//24-27
			"FROM AFTTAB ");
		dbg(DEBUG,"++++++++++++++++++++Common SQL for AFTTAB = \n\n%s++++++++++++++++++++\n\n",rlAFTSQL.SqlBuf);
		
    //Update                 		
		if(igIs8AM == FALSE)
		{
			if(igUpdateResetToMidnight == TRUE)
			{
				dbg(DEBUG,"------Update reset");
				memset(pclTmpStr,0,sizeof(pclTmpStr));
				strncpy(pclTmpStr,pcgUpdateFlightRangeLow,8);
				strcat(pclTmpStr,"000000");
				//dbg(DEBUG,"------pclTmpStr = %s",pclTmpStr);
				memset(pcgUpdateFlightRangeLow,0,sizeof(pcgUpdateFlightRangeLow));
				strcpy(pcgUpdateFlightRangeLow,pclTmpStr);
				
				memset(pclTmpStr,0,sizeof(pclTmpStr));
				strncpy(pclTmpStr,pcgUpdateFlightRangeHigh,8);
				strcat(pclTmpStr,"000000");
				//dbg(DEBUG,"------pclTmpStr = %s",pclTmpStr);
				memset(pcgUpdateFlightRangeHigh,0,sizeof(pcgUpdateFlightRangeHigh));
				strcpy(pcgUpdateFlightRangeHigh,pclTmpStr);
			}
			
			memset(pclTmpStr,0,sizeof(pclTmpStr));
			sprintf(pclTmpStr,"WHERE ((ORG3='%s' AND STOD BETWEEN '%s' AND '%s') "
				"OR (DES3='%s' AND STOA BETWEEN '%s' AND '%s')) "
				,cgHopo,pcgUpdateFlightRangeLow,pcgUpdateFlightRangeHigh,cgHopo,pcgUpdateFlightRangeLow,pcgUpdateFlightRangeHigh);
			
			strcat(rlAFTSQL.SqlBuf,pclTmpStr);
			
			if(strlen(pcgUpdateExcludeFtyp) != 0)
			{
				memset(pclTmpStr,0,sizeof(pclTmpStr));
				sprintf(pclTmpStr,"AND FTYP NOT IN (%s) ",pcgUpdateExcludeFtyp);
				strcat(rlAFTSQL.SqlBuf,pclTmpStr);
			}
			if(strlen(pcgUpdateExcludeTtyp) != 0)
			{
				memset(pclTmpStr,0,sizeof(pclTmpStr));
				sprintf(pclTmpStr,"AND TTYP NOT IN (%s) ",pcgUpdateExcludeTtyp);
				strcat(rlAFTSQL.SqlBuf,pclTmpStr);
			}
			if(strlen(pcgUpdateExcludeStyp) != 0)
			{
				memset(pclTmpStr,0,sizeof(pclTmpStr));
				sprintf(pclTmpStr,"AND STYP NOT IN (%s)",pcgUpdateExcludeStyp);
				strcat(rlAFTSQL.SqlBuf,pclTmpStr);
			}
			
			//sprintf(pcgUpdateFileName,"/ceda/exco/FILES/WORKBRIDGE/UFISWB%s",pcgCurrDateForUpdate);
			sprintf(pcgUpdateFileName,"%sUFISWB%s.txt",pcgUpdateWorkPath,pcgCurrDateForUpdate);
			dbg(DEBUG,"----------UPDATE file_name = %s",pcgUpdateFileName);
			
			if(igDoUpdateFile == TRUE)
			{
				dbg(DEBUG,"open update");
				fptrUpdate = fopen( pcgUpdateFileName, "w" );
			  if( fptrUpdate == NULL )
			  {
			      dbglog( "Cannot open file for writing <%s> error <%s><%d>", pcgUpdateFileName, errno, strerror(errno) );
			      exit( -1 );
			  }
			}
			else
			{
				dbg(DEBUG,"DO_UPDATE_FILE = NO");
			}
    }
    else if(igIs8AM == TRUE)//Daily 
    {
    	if(igDailyResetToMidnight == TRUE)
    	{	
	    	dbg(DEBUG,"------Daily reset");
				memset(pclTmpStr,0,sizeof(pclTmpStr));
				strncpy(pclTmpStr,pcgDailyFlightRangeLow,8);
				strcat(pclTmpStr,"000000");
				//dbg(DEBUG,"------@@pclTmpStr = %s",pclTmpStr);
				memset(pcgDailyFlightRangeLow,0,sizeof(pcgDailyFlightRangeLow));
				strcpy(pcgDailyFlightRangeLow,pclTmpStr);
				
				memset(pclTmpStr,0,sizeof(pclTmpStr));
				strncpy(pclTmpStr,pcgDailyFlightRangeHigh,8);
				strcat(pclTmpStr,"000000");
				//dbg(DEBUG,"------pclTmpStr = %s",pclTmpStr);
				memset(pcgDailyFlightRangeHigh,0,sizeof(pcgDailyFlightRangeHigh));
				strcpy(pcgDailyFlightRangeHigh,pclTmpStr);
			}
			
			memset(pclTmpStr,0,sizeof(pclTmpStr));
			sprintf(pclTmpStr,"WHERE ((ORG3='%s' AND STOD BETWEEN '%s' AND '%s') "
				"OR (DES3='%s' AND STOA BETWEEN '%s' AND '%s')) "
				,cgHopo,pcgDailyFlightRangeLow,pcgDailyFlightRangeHigh,cgHopo,pcgDailyFlightRangeLow,pcgDailyFlightRangeHigh);
				
			strcat(rlAFTSQL.SqlBuf,pclTmpStr);
    	
		 	if(strlen(pcgDailyExcludeFtyp) != 0)
			{
				memset(pclTmpStr,0,sizeof(pclTmpStr));
				sprintf(pclTmpStr,"AND FTYP NOT IN (%s) ",pcgDailyExcludeFtyp);
				strcat(rlAFTSQL.SqlBuf,pclTmpStr);
			}
			if(strlen(pcgDailyExcludeTtyp) != 0)
			{
				memset(pclTmpStr,0,sizeof(pclTmpStr));
				sprintf(pclTmpStr,"AND TTYP NOT IN (%s) ",pcgDailyExcludeTtyp);
				strcat(rlAFTSQL.SqlBuf,pclTmpStr);
			}
			if(strlen(pcgDailyExcludeStyp) != 0)
			{
				memset(pclTmpStr,0,sizeof(pclTmpStr));
				sprintf(pclTmpStr,"AND STYP NOT IN (%s)",pcgDailyExcludeStyp);
				strcat(rlAFTSQL.SqlBuf,pclTmpStr);
			}
			
			//sprintf(pcgDailyFileName,"/ceda/exco/FILES/WORKBRIDGE/UFISWB%s080000",pcgCurrDateForDaily);
			if(igDoDailyFile == TRUE)
			{	
				dbg(DEBUG,"open daily");
				
				fptrDaily = fopen( pcgDailyFileName, "r" );
				if(fptrDaily == NULL)
				{
					fptrDaily = fopen( pcgDailyFileName, "w" );
			  	if( fptrDaily == NULL )
			  	{
			      dbglog( "Cannot open file for writing <%s> error <%s><%d>", pcgDailyFileName, errno, strerror(errno) );
			      exit( -1 );
			  	}
	    	}
	    	else
	    	{
	    		sprintf(pcgDailyFileName,"%sUFISWB%s080001.txt",pcgDailyWorkPath,pcgCurrDateForDaily);
	    		fptrDaily = fopen( pcgDailyFileName, "w" );
	    		if( fptrDaily == NULL )
			  	{
			      dbglog( "Cannot open file for writing <%s> error <%s><%d>", pcgDailyFileName, errno, strerror(errno) );
			      exit( -1 );
			  	}
	    	}
    	}
    	else
    	{
    		dbg(DEBUG,"DO_DAILY_FILE = NO");
			}    
    }
    
    #ifdef TEST
		strcat(rlAFTSQL.SqlBuf," AND ROWNUM < 2");
		#endif
		
    dbg(DEBUG,"++++++++++++++++++++Complete SQL for AFTTAB = \n\n%s++++++++++++++++++++\n\n",rlAFTSQL.SqlBuf);
    dbg(DEBUG,"Frank@WorkBridge<pcgDailyFileName = %s>",pcgDailyFileName);
    dbg(DEBUG,"Frank@WorkBridge<pcgUpdateFileName = %s>",pcgUpdateFileName);
    
    /*SELECT URNO,FLNO,ALC2,FLTN,FLNS,
						 REGN,ORG3,DES3,STOD,STOA,ETDI,
		 				 ETAI,AIRB,LAND,FTYP,ACT3,STYP,
		 				 PSTA,PSTD,GTA1,GTD1,
		 				 CKIF,CKIT,STEV,ADID
						 FROM AFTTAB
						 WHERE ((ORG3='%s' AND STOD BETWEEN '%s' AND '%s') OR (DES3='%s' AND STOA BETWEEN '%s' AND '%s')) 
						 AND FTYP NOT IN (%s) AND TTYP NOT IN (%s) AND STYP NOT IN (%s)*/
    
    rlAFTSQL.SqlCursor = 0;
    rlAFTSQL.SqlFunc = START;
		memset( &rlAFTTAB, 0, sizeof(rlAFTTAB) );
		while( (sql_if( rlAFTSQL.SqlFunc, &rlAFTSQL.SqlCursor, rlAFTSQL.SqlBuf, pclSqlData )) == DB_SUCCESS )
		{
			//Frank v1.1b
			if(ilLineCount != 0)
			{
				if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
	      {
	      	fprintf( fptrUpdate, "\n" );
	      }
	      else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
	      {
	      	fprintf( fptrDaily, "\n" );
	      }
	    }
	    ilLineCount++;
      //Frank v1.1b
      
			rlAFTSQL.SqlFunc = NEXT;
			
      get_fld(pclSqlData,FIELD_1,STR,12,rlAFTTAB.URNO);		TrimSpace(rlAFTTAB.URNO); 
      get_fld(pclSqlData,FIELD_2,STR,12,rlAFTTAB.FLNO); 	TrimSpace(rlAFTTAB.FLNO);
      get_fld(pclSqlData,FIELD_3,STR,12,rlAFTTAB.ALC2);  	TrimSpace(rlAFTTAB.ALC2);
      get_fld(pclSqlData,FIELD_4,STR,12,rlAFTTAB.ALC3);  	TrimSpace(rlAFTTAB.ALC3);
      get_fld(pclSqlData,FIELD_5,STR,12,rlAFTTAB.CSGN);  	TrimSpace(rlAFTTAB.CSGN);
      get_fld(pclSqlData,FIELD_6,STR,12,rlAFTTAB.FLTN); 	TrimSpace(rlAFTTAB.FLTN);
      get_fld(pclSqlData,FIELD_7,STR,12,rlAFTTAB.FLNS);  	TrimSpace(rlAFTTAB.FLNS);
      get_fld(pclSqlData,FIELD_8,STR,12,rlAFTTAB.REGN);		TrimSpace(rlAFTTAB.REGN);
      get_fld(pclSqlData,FIELD_9,STR,12,rlAFTTAB.ORG3); 	TrimSpace(rlAFTTAB.ORG3);
      get_fld(pclSqlData,FIELD_10,STR,12,rlAFTTAB.DES3);		TrimSpace(rlAFTTAB.DES3);
      get_fld(pclSqlData,FIELD_11,STR,14,rlAFTTAB.STOD);		TrimSpace(rlAFTTAB.STOD);
      get_fld(pclSqlData,FIELD_12,STR,14,rlAFTTAB.STOA); 	TrimSpace(rlAFTTAB.STOA);
      get_fld(pclSqlData,FIELD_13,STR,14,rlAFTTAB.ETDI); 	TrimSpace(rlAFTTAB.ETDI);
      get_fld(pclSqlData,FIELD_14,STR,14,rlAFTTAB.ETAI); 	TrimSpace(rlAFTTAB.ETAI);
      get_fld(pclSqlData,FIELD_15,STR,14,rlAFTTAB.AIRB); 	TrimSpace(rlAFTTAB.AIRB);
      get_fld(pclSqlData,FIELD_16,STR,14,rlAFTTAB.LAND); 	TrimSpace(rlAFTTAB.LAND);
      get_fld(pclSqlData,FIELD_17,STR,12,rlAFTTAB.FTYP);	TrimSpace(rlAFTTAB.FTYP);
      get_fld(pclSqlData,FIELD_18,STR,12,rlAFTTAB.ACT3);  TrimSpace(rlAFTTAB.ACT3);
      get_fld(pclSqlData,FIELD_19,STR,12,rlAFTTAB.STYP);	TrimSpace(rlAFTTAB.STYP);
      get_fld(pclSqlData,FIELD_20,STR,12,rlAFTTAB.PSTA);	TrimSpace(rlAFTTAB.PSTA);
      get_fld(pclSqlData,FIELD_21,STR,12,rlAFTTAB.PSTD);	TrimSpace(rlAFTTAB.PSTD);
      get_fld(pclSqlData,FIELD_22,STR,12,rlAFTTAB.GTA1);	TrimSpace(rlAFTTAB.GTA1);
      get_fld(pclSqlData,FIELD_23,STR,12,rlAFTTAB.GTD1);	TrimSpace(rlAFTTAB.GTD1);
      get_fld(pclSqlData,FIELD_24,STR,12,rlAFTTAB.CKIF);	TrimSpace(rlAFTTAB.CKIF);
      get_fld(pclSqlData,FIELD_25,STR,12,rlAFTTAB.CKIT);	TrimSpace(rlAFTTAB.CKIT);
      get_fld(pclSqlData,FIELD_26,STR,12,rlAFTTAB.STEV);	TrimSpace(rlAFTTAB.STEV);
      get_fld(pclSqlData,FIELD_27,STR,12,rlAFTTAB.ADID);	TrimSpace(rlAFTTAB.ADID);
      
      dbg(DEBUG,"line<%d>rlAFTTAB.URNO<%s>",__LINE__,rlAFTTAB.URNO);
      dbg(DEBUG,"line<%d>rlAFTTAB.FLNO<%s>",__LINE__,rlAFTTAB.FLNO);
      dbg(DEBUG,"line<%d>rlAFTTAB.ALC2<%s>",__LINE__,rlAFTTAB.ALC2);
      dbg(DEBUG,"line<%d>rlAFTTAB.ALC3<%s>",__LINE__,rlAFTTAB.ALC3);
      dbg(DEBUG,"line<%d>rlAFTTAB.CSGN<%s>",__LINE__,rlAFTTAB.CSGN);
      dbg(DEBUG,"line<%d>rlAFTTAB.FLTN<%s>",__LINE__,rlAFTTAB.FLTN);
      dbg(DEBUG,"line<%d>rlAFTTAB.FLNS<%s>",__LINE__,rlAFTTAB.FLNS);
      dbg(DEBUG,"line<%d>rlAFTTAB.REGN<%s>",__LINE__,rlAFTTAB.REGN);
      dbg(DEBUG,"line<%d>rlAFTTAB.ORG3<%s>",__LINE__,rlAFTTAB.ORG3);
      dbg(DEBUG,"line<%d>rlAFTTAB.DES3<%s>",__LINE__,rlAFTTAB.DES3);
      dbg(DEBUG,"line<%d>rlAFTTAB.STOD<%s>",__LINE__,rlAFTTAB.STOD);
      dbg(DEBUG,"line<%d>rlAFTTAB.STOA<%s>",__LINE__,rlAFTTAB.STOA);
      dbg(DEBUG,"line<%d>rlAFTTAB.ETDI<%s>",__LINE__,rlAFTTAB.ETDI);
      dbg(DEBUG,"line<%d>rlAFTTAB.ETAI<%s>",__LINE__,rlAFTTAB.ETAI);
      dbg(DEBUG,"line<%d>rlAFTTAB.AIRB<%s>",__LINE__,rlAFTTAB.AIRB);
      dbg(DEBUG,"line<%d>rlAFTTAB.LAND<%s>",__LINE__,rlAFTTAB.LAND);
      dbg(DEBUG,"line<%d>rlAFTTAB.FTYP<%s>",__LINE__,rlAFTTAB.FTYP);
      dbg(DEBUG,"line<%d>rlAFTTAB.ACT3<%s>",__LINE__,rlAFTTAB.ACT3);
      dbg(DEBUG,"line<%d>rlAFTTAB.STYP<%s>",__LINE__,rlAFTTAB.STYP);
      dbg(DEBUG,"line<%d>rlAFTTAB.PSTA<%s>",__LINE__,rlAFTTAB.PSTA);
      dbg(DEBUG,"line<%d>rlAFTTAB.PSTD<%s>",__LINE__,rlAFTTAB.PSTD); 
      dbg(DEBUG,"line<%d>rlAFTTAB.GTA1<%s>",__LINE__,rlAFTTAB.GTA1);
      dbg(DEBUG,"line<%d>rlAFTTAB.GTD1<%s>",__LINE__,rlAFTTAB.GTD1);
      dbg(DEBUG,"line<%d>rlAFTTAB.CKIF<%s>",__LINE__,rlAFTTAB.CKIF);
      dbg(DEBUG,"line<%d>rlAFTTAB.CKIT<%s>",__LINE__,rlAFTTAB.CKIT);
      dbg(DEBUG,"line<%d>rlAFTTAB.STEV<%s>",__LINE__,rlAFTTAB.STEV);
      dbg(DEBUG,"line<%d>rlAFTTAB.ADID<%s>",__LINE__,rlAFTTAB.ADID);
      
      //Frank v1.1b
      //dbg(DEBUG,"HandleData Checking the ALC2, FLNO, FLNS, STOA and STOD begins");
      //ilRc == IsALC2_FLNO_FLTN_STOA_STOD_NULL(rlAFTTAB);
      
      dbg(DEBUG,"HandleData Checking the STOA and STOD begins");
      ilRc = IsSTOA_STOD_NULL(rlAFTTAB);
      if(ilRc != RC_SUCCESS)
      {
      	dbg(TRACE,"HandleData STOA or STOD is invalid, continue");
      	continue;
	//return ilRc;
      }
      else
      {
      	dbg(TRACE,"HandleData STOA and STOD is valid, continue check ALC2,ALC3,FLNO and FLTN");
      }
	
      //Frank 20120803 v1.2
      //rlAftrec.ALC2,rlAftrec.ALC3,rlAftrec.FLNO,rlAftrec.FLTN have been TrimRight() in sendAftData
      if( strlen(pcgCodeForALC2) != 0 && igLastPartOfCSGN != 0 )
      {
	ilRc = IsAlc2FlnoFltnNullForAdHocFlight(rlAFTTAB,pclALC2,pclFLTN,pclFLNS);
	if( ilRc == RC_SUCCESS )
	{
        	strcpy(rlAFTTAB.ALC2,pclALC2);
		strcpy(rlAFTTAB.FLTN,pclFLTN);
		strcpy(rlAFTTAB.FLNS,pclFLNS);	
	}
      }
      else
      {
	dbg(TRACE,"pcgCodeForALC2 is null and igLastPartOfCSGN is zero, so do not modify the values of ALC3 & FLNO,but check them null or not");
	ilRc == IsALC2_FLNO_FLTN_NULL(rlAFTTAB);
	if( ilRc != RC_SUCCESS)
	{	
      		dbg(TRACE,"HandleData ALC2 or FLNO or FLTN is invalid, continue");
      		continue;
		//return ilRc;
	}
      }
      //Frank 20120803 v1.2
      
      if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
      {	
		    //fprintf(fptrUpdate,"%s%s",rlAFTTAB.URNO,pcgMainDelimiter);
		    //fprintf(fptrUpdate,"%s%s",rlAFTTAB.FLNO,pcgMainDelimiter);
		    fprintf(fptrUpdate,"%s%s",rlAFTTAB.ALC2,pcgMainDelimiter);
		    fprintf(fptrUpdate,"%s%s%s",rlAFTTAB.FLTN,rlAFTTAB.FLNS,pcgMainDelimiter);
		    fprintf(fptrUpdate,"%s%s",rlAFTTAB.REGN,pcgMainDelimiter);
		    fprintf(fptrUpdate,"%s%s",rlAFTTAB.ORG3,pcgMainDelimiter);
		    fprintf(fptrUpdate,"%s%s",rlAFTTAB.DES3,pcgMainDelimiter);
                    
		    memset(pclTmpStr,0,sizeof(pclTmpStr));
		    strncpy(pclTmpStr,rlAFTTAB.STOD,12);
		    fprintf(fptrUpdate,"%s%s",pclTmpStr,pcgMainDelimiter);
		    
		    memset(pclTmpStr,0,sizeof(pclTmpStr));
		    strncpy(pclTmpStr,rlAFTTAB.STOA,12);
		    fprintf(fptrUpdate,"%s%s",pclTmpStr,pcgMainDelimiter);
		    
		    memset(pclTmpStr,0,sizeof(pclTmpStr));
		    strncpy(pclTmpStr,rlAFTTAB.ETDI,12);
		    fprintf(fptrUpdate,"%s%s",pclTmpStr,pcgMainDelimiter);
		    
		    memset(pclTmpStr,0,sizeof(pclTmpStr));
		    strncpy(pclTmpStr,rlAFTTAB.ETAI,12);
		    fprintf(fptrUpdate,"%s%s",pclTmpStr,pcgMainDelimiter);
		    
		    memset(pclTmpStr,0,sizeof(pclTmpStr));
		    strncpy(pclTmpStr,rlAFTTAB.AIRB,12);
		    fprintf(fptrUpdate,"%s%s",pclTmpStr,pcgMainDelimiter);
		    
		    memset(pclTmpStr,0,sizeof(pclTmpStr));
		    strncpy(pclTmpStr,rlAFTTAB.LAND,12);
		    fprintf(fptrUpdate,"%s%s",pclTmpStr,pcgMainDelimiter);
		    
		    /*
		    fprintf(fptrUpdate,"%s%s",rlAFTTAB.STOD,pcgMainDelimiter);
		    fprintf(fptrUpdate,"%s%s",rlAFTTAB.STOA,pcgMainDelimiter);
		    fprintf(fptrUpdate,"%s%s",rlAFTTAB.ETDI,pcgMainDelimiter);
		    fprintf(fptrUpdate,"%s%s",rlAFTTAB.ETAI,pcgMainDelimiter);
		    fprintf(fptrUpdate,"%s%s",rlAFTTAB.AIRB,pcgMainDelimiter);
		    fprintf(fptrUpdate,"%s%s",rlAFTTAB.LAND,pcgMainDelimiter);
			*/
		    fprintf(fptrUpdate,"%s%s",rlAFTTAB.FTYP,pcgMainDelimiter);
		    fprintf(fptrUpdate,"%s%s",rlAFTTAB.ACT3,pcgMainDelimiter);
		    
			  /*  
			  fprintf(fptrUpdate,"%s%s",rlAFTTAB.STYP,pcgMainDelimiter);
	      fprintf(fptrUpdate,"%s%s",rlAFTTAB.PSTA,pcgMainDelimiter);
	      fprintf(fptrUpdate,"%s%s",rlAFTTAB.PSTD,pcgMainDelimiter);
	      fprintf(fptrUpdate,"%s%s",rlAFTTAB.GTA1,pcgMainDelimiter);
	      fprintf(fptrUpdate,"%s%s",rlAFTTAB.GTD1,pcgMainDelimiter);
	      fprintf(fptrUpdate,"%s%s",rlAFTTAB.CKIF,pcgMainDelimiter);
	      fprintf(fptrUpdate,"%s%s",rlAFTTAB.CKIT,pcgMainDelimiter);
	      fprintf(fptrUpdate,"%s%s",rlAFTTAB.STEV,pcgMainDelimiter);
	      */ 
      }
      else  if(igDoDailyFile == TRUE && igIs8AM==TRUE)
      {
      	//fprintf(fptrDaily,"%s%s",rlAFTTAB.URNO,pcgMainDelimiter);
		    //fprintf(fptrDaily,"%s%s",rlAFTTAB.FLNO,pcgMainDelimiter);
		    fprintf(fptrDaily,"%s%s",rlAFTTAB.ALC2,pcgMainDelimiter);
		    fprintf(fptrDaily,"%s%s%s",rlAFTTAB.FLTN,rlAFTTAB.FLNS,pcgMainDelimiter);
		    fprintf(fptrDaily,"%s%s",rlAFTTAB.REGN,pcgMainDelimiter);
		    fprintf(fptrDaily,"%s%s",rlAFTTAB.ORG3,pcgMainDelimiter);
		    fprintf(fptrDaily,"%s%s",rlAFTTAB.DES3,pcgMainDelimiter);
		    fprintf(fptrDaily,"%s%s",rlAFTTAB.STOD,pcgMainDelimiter);
		    fprintf(fptrDaily,"%s%s",rlAFTTAB.STOA,pcgMainDelimiter);
		    fprintf(fptrDaily,"%s%s",rlAFTTAB.ETDI,pcgMainDelimiter);
		    fprintf(fptrDaily,"%s%s",rlAFTTAB.ETAI,pcgMainDelimiter);
		    fprintf(fptrDaily,"%s%s",rlAFTTAB.AIRB,pcgMainDelimiter);
		    fprintf(fptrDaily,"%s%s",rlAFTTAB.LAND,pcgMainDelimiter);
		    fprintf(fptrDaily,"%s%s",rlAFTTAB.FTYP,pcgMainDelimiter);
		    fprintf(fptrDaily,"%s%s",rlAFTTAB.ACT3,pcgMainDelimiter);
		    
			  /*  
			  fprintf(fptrDaily,"%s%s",rlAFTTAB.STYP,pcgMainDelimiter);
	      fprintf(fptrDaily,"%s%s",rlAFTTAB.PSTA,pcgMainDelimiter);
	      fprintf(fptrDaily,"%s%s",rlAFTTAB.PSTD,pcgMainDelimiter);
	      fprintf(fptrDaily,"%s%s",rlAFTTAB.GTA1,pcgMainDelimiter);
	      fprintf(fptrDaily,"%s%s",rlAFTTAB.GTD1,pcgMainDelimiter);
	      fprintf(fptrDaily,"%s%s",rlAFTTAB.CKIF,pcgMainDelimiter);
	      fprintf(fptrDaily,"%s%s",rlAFTTAB.CKIT,pcgMainDelimiter);
	      fprintf(fptrDaily,"%s%s",rlAFTTAB.STEV,pcgMainDelimiter);
	      */ 
      }
    	
    	#ifdef TEST
    		dbg(DEBUG,"**************pcgLoadDSSN");
    	#endif
    	
    	memset(pclTmpStr,0,sizeof(pclTmpStr));
    	memset(pclDSSNSource,0,sizeof(pclDSSNSource));
    	ilNoEle = GetNoOfElements(pcgLoadDSSN,',');
 		 	for (ilI = 1; ilI <= ilNoEle; ilI++)
  		{
    		get_real_item(pclTmpStr,pcgLoadDSSN,ilI);
    		strcpy(pclDSSNSource[ilI - 1],pclTmpStr);
    		
    		#ifdef TEST
    			dbg(DEBUG,"%s",pclTmpStr);
    			dbg(DEBUG,"%s",pclDSSNSource[ilI - 1]);
    		#endif
    	}
    	
    	#ifdef TEST
				dbg(DEBUG,"**************pclValBookF");
    	#endif
    	
    	memset(pclTmpStr,0,sizeof(pclTmpStr));
    	memset(pclValBookF,0,sizeof(pclValBookF));
    	ilNoEle = GetNoOfElements(pcgValBookF,';');
 		 	for (ilI = 1; ilI < ilNoEle; ilI++)
  		{
    		get_real_item_semicolon(pclTmpStr,pcgValBookF,ilI);
    		strcpy(pclValBookF[ilI - 1],pclTmpStr);
    		
    		#ifdef TEST
    			dbg(DEBUG,"%s",pclValBookF[ilI - 1]);
    		#endif
    	}	
    	
    	#ifdef TEST
    		dbg(DEBUG,"**************pclValBookB");
    	#endif
    	memset(pclTmpStr,0,sizeof(pclTmpStr));
    	memset(pclValBookB,0,sizeof(pclValBookB));
    	ilNoEle = GetNoOfElements(pcgValBookB,';');
 		 	for (ilI = 1; ilI < ilNoEle; ilI++)
  		{
    		get_real_item_semicolon(pclTmpStr,pcgValBookB,ilI);
    		strcpy(pclValBookB[ilI - 1],pclTmpStr);
    		
    		#ifdef TEST
    			dbg(DEBUG,"%s",pclValBookB[ilI - 1]);
    		#endif
    	}	
    	
    	#ifdef TEST
    		dbg(DEBUG,"**************pclValBookE");
    	#endif
    	
    	memset(pclTmpStr,0,sizeof(pclTmpStr));
    	memset(pclValBookE,0,sizeof(pclValBookE));
    	ilNoEle = GetNoOfElements(pcgValBookE,';');
 		 	for (ilI = 1; ilI < ilNoEle; ilI++)
  		{
    		get_real_item_semicolon(pclTmpStr,pcgValBookE,ilI);
    		strcpy(pclValBookE[ilI - 1],pclTmpStr);
    		
    		#ifdef TEST
    			dbg(DEBUG,"%s",pclValBookE[ilI - 1]);
    		#endif
    	}	
    	
    	#ifdef TEST
    		dbg(DEBUG,"**************pclValActualF");
    	#endif
    	
    	memset(pclTmpStr,0,sizeof(pclTmpStr));
    	memset(pclValActualF,0,sizeof(pclValActualF));
    	ilNoEle = GetNoOfElements(pcgValActualF,';');
 		 	for (ilI = 1; ilI < ilNoEle; ilI++)
  		{
    		get_real_item_semicolon(pclTmpStr,pcgValActualF,ilI);
    		strcpy(pclValActualF[ilI - 1],pclTmpStr);
    		
    		#ifdef TEST
    			dbg(DEBUG,"%s",pclValActualF[ilI - 1]);
				#endif    	
    	}	
    	
    	#ifdef TEST
    		dbg(DEBUG,"**************pclValActualB");
    	#endif
    	
    	memset(pclTmpStr,0,sizeof(pclTmpStr));
    	memset(pclValActualB,0,sizeof(pclValActualB));
    	ilNoEle = GetNoOfElements(pcgValActualB,';');
 		 	for (ilI = 1; ilI < ilNoEle; ilI++)
  		{
    		get_real_item_semicolon(pclTmpStr,pcgValActualB,ilI);
    		strcpy(pclValActualB[ilI - 1],pclTmpStr);
    		
    		#ifdef TEST
    			dbg(DEBUG,"%s",pclValActualB[ilI - 1]);
    		#endif
    	}	
    	
    	#ifdef TEST
    		dbg(DEBUG,"**************pclValActualE");
    	#endif
    	
    	memset(pclTmpStr,0,sizeof(pclTmpStr));
    	memset(pclValActualE,0,sizeof(pclValActualE));
    	ilNoEle = GetNoOfElements(pcgValActualE,';');
 		 	for (ilI = 1; ilI < ilNoEle; ilI++)
  		{
    		get_real_item_semicolon(pclTmpStr,pcgValActualE,ilI);
    		strcpy(pclValActualE[ilI - 1],pclTmpStr);
    		
    		#ifdef TEST
    			dbg(DEBUG,"%s",pclValActualE[ilI - 1]);
    		#endif
    	}	
 			
 			#ifdef TEST
 				dbg(DEBUG,"**************pclValConfigF");
 			#endif
 			
    	memset(pclTmpStr,0,sizeof(pclTmpStr));
    	memset(pclValConfigF,0,sizeof(pclValConfigF));
    	ilNoEle = GetNoOfElements(pcgValConfigF,';');
 		 	for (ilI = 1; ilI < ilNoEle; ilI++)
  		{
    		get_real_item_semicolon(pclTmpStr,pcgValConfigF,ilI);
    		strcpy(pclValConfigF[ilI - 1],pclTmpStr);
    		
    		#ifdef TEST
    			dbg(DEBUG,"%s",pclValConfigF[ilI - 1]);
    		#endif
    	}	
    	
    	#ifdef TEST
    		dbg(DEBUG,"**************pclValConfigB");
    	#endif
    	
    	memset(pclTmpStr,0,sizeof(pclTmpStr));
    	memset(pclValConfigB,0,sizeof(pclValConfigB));
    	ilNoEle = GetNoOfElements(pcgValConfigB,';');
 		 	for (ilI = 1; ilI < ilNoEle; ilI++)
  		{
    		get_real_item_semicolon(pclTmpStr,pcgValConfigB,ilI);
    		strcpy(pclValConfigB[ilI - 1],pclTmpStr);
    		
    		#ifdef TEST
    			dbg(DEBUG,"%s",pclValConfigB[ilI - 1]);
				#endif    	
    	}	
    	
    	#ifdef TEST
   			dbg(DEBUG,"**************pclValConfigE");
   		#endif
   		
    	memset(pclTmpStr,0,sizeof(pclTmpStr));
    	memset(pclValConfigE,0,sizeof(pclValConfigE));
    	ilNoEle = GetNoOfElements(pcgValConfigE,';');
 		 	for (ilI = 1; ilI < ilNoEle; ilI++)
  		{
    		get_real_item_semicolon(pclTmpStr,pcgValConfigE,ilI);
    		strcpy(pclValConfigE[ilI - 1],pclTmpStr);
    		
    		#ifdef TEST
    			dbg(DEBUG,"%s",pclValConfigE[ilI - 1]);
    		#endif
    	}	
			
			#ifdef TEST
    		dbg(DEBUG,"**************pclValCargo");
    	#endif
    	
    	memset(pclTmpStr,0,sizeof(pclTmpStr));
    	memset(pclValCargo,0,sizeof(pclValCargo));
    	ilNoEle = GetNoOfElements(pcgValCargo,';');
 		 	for (ilI = 1; ilI < ilNoEle; ilI++)
  		{
    		get_real_item_semicolon(pclTmpStr,pcgValCargo,ilI);
    		strcpy(pclValCargo[ilI - 1],pclTmpStr);
    		
    		#ifdef TEST
    			dbg(DEBUG,"%s",pclValCargo[ilI - 1]);
    		#endif
    	}	
    	
    	#ifdef TEST
    		dbg(DEBUG,"**************pclValMail");
    	#endif
    	
    	memset(pclTmpStr,0,sizeof(pclTmpStr));
    	memset(pclValMail,0,sizeof(pclValMail));
    	ilNoEle = GetNoOfElements(pcgValMail,';');
 		 	for (ilI = 1; ilI < ilNoEle; ilI++)
  		{
    		get_real_item_semicolon(pclTmpStr,pcgValMail,ilI);
    		strcpy(pclValMail[ilI - 1],pclTmpStr);
    		
    		#ifdef TEST
    			dbg(DEBUG,"%s",pclValMail[ilI - 1]);
    		#endif
    	}
    	
    	#ifdef TEST
    		dbg(DEBUG,"**************pclValBags");
    	#endif
    	
    	memset(pclTmpStr,0,sizeof(pclTmpStr));
    	memset(pclValBags,0,sizeof(pclValBags));
    	ilNoEle = GetNoOfElements(pcgValBags,';');
 		 	for (ilI = 1; ilI < ilNoEle; ilI++)
  		{
    		get_real_item_semicolon(pclTmpStr,pcgValBags,ilI);
    		strcpy(pclValBags[ilI - 1],pclTmpStr);
    		
    		#ifdef TEST
    			dbg(DEBUG,"%s",pclValBags[ilI - 1]);
    		#endif
    	}
    	
    	#ifdef TEST
    		dbg(DEBUG,"**************pcgDSSN");
    	#endif
    	
    	memset(pclTmpStr,0,sizeof(pclTmpStr));
    	memset(pclDSSNSourceForTransfer,0,sizeof(pclDSSNSourceForTransfer));
    	ilNoEle = GetNoOfElements(pcgDSSN,',');
 		 	for (ilI = 1; ilI <= ilNoEle; ilI++)
  		{
    		get_real_item(pclTmpStr,pcgDSSN,ilI);
    		strcpy(pclDSSNSourceForTransfer[ilI - 1],pclTmpStr);
    		
    		//dbg(DEBUG,"%s",pclTmpStr);
    		#ifdef TEST
    			dbg(DEBUG,"%s",pclDSSNSourceForTransfer[ilI - 1]);
    		#endif
    	}
    	
    	//-------------------------------------
    	memset( &rlLOASQL.SqlBuf,0,sizeof(rlLOASQL.SqlBuf));
    	sprintf( rlLOASQL.SqlBuf, "SELECT SUM(VALU) FROM LOATAB WHERE FLNU='%s' ",rlAFTTAB.URNO);
      strcpy(rlLOASQL.SqlBufMostCommon, rlLOASQL.SqlBuf);
      
    	//DSSN
    	//VAL_CONFIG_F
    	memset( &rlLOATAB, 0, sizeof(rlLOATAB) );
    	memset( &rlLOASQL.SqlBuf,0,sizeof(rlLOASQL.SqlBuf));
      strcpy( rlLOASQL.SqlBuf,rlLOASQL.SqlBufMostCommon);
      //dbg(DEBUG,"Frank@WorkBridge<line-%d>Getting the VAL_CONFIG_F records from LOATAB",__LINE__);
      //dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
      
      ilNoEle = GetNoOfElements(pcgValConfigF,';');
      for(ilI=0; ilI< ilNoEle-1; ilI++)
      {
      	if(strcmp(pclValConfigF[ilI],"#")!=0)
      	{
      		switch(ilI)
      		{
      			case 0:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND TYPE='%s' ",pclValConfigF[0]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 1:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND STYP='%s' ",pclValConfigF[1]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 2:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND SSTP='%s' ",pclValConfigF[2]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 3:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND SSST='%s' ",pclValConfigF[3]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			default:
      				break;
      		}
      	}
      }
      memset( &rlLOATAB, 0, sizeof(rlLOATAB) );
      memset( &rlLOASQL.SqlBufCommon, 0, sizeof(rlLOASQL.SqlBufCommon) );
      strcpy(rlLOASQL.SqlBufCommon,rlLOASQL.SqlBuf);
      //dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
      
      ilIsNull = TRUE;
      ilNoEle = GetNoOfElements(pcgLoadDSSN,',');
 		 	for (ilI = 1; ilI <= ilNoEle; ilI++)
  		{
  			rlLOASQL.SqlCursor = 0;
    		rlLOASQL.SqlFunc = START;
    		
    		memset(pclTmpStr,0,sizeof(pclTmpStr));
    		memset(rlLOASQL.SqlBuf,0,sizeof(rlLOASQL.SqlBuf));
    		strcpy(rlLOASQL.SqlBuf,rlLOASQL.SqlBufCommon);
    		sprintf(pclTmpStr,"AND DSSN='%s' ",pclDSSNSource[ilI-1]);
      	strcat(rlLOASQL.SqlBuf,pclTmpStr);
      	
      	#ifdef TEST
      		dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
      	#endif
    		sql_if( rlLOASQL.SqlFunc, &rlLOASQL.SqlCursor, rlLOASQL.SqlBuf, pclSqlData );
    		get_fld(pclSqlData,FIELD_1,STR,12,rlLOATAB.ValConfigFCount);
    		close_my_cursor( &rlLOASQL.SqlCursor );
  			TrimSpace(rlLOATAB.ValConfigFCount); 
  			
  			#ifdef TEST
    			dbg(DEBUG,"-----rlLOATAB.ValConfigFCount = %d--------",atoi(rlLOATAB.ValConfigFCount));
    		#endif
    		
    		if(atoi(rlLOATAB.ValConfigFCount) != 0)
    		{
    			ilIsNull = FALSE;
    			
    			if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
    			{
    				fprintf(fptrUpdate,"%s%s",rlLOATAB.ValConfigFCount,pcgMainDelimiter);
    			}
    			else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
    			{
    				fprintf(fptrDaily,"%s%s",rlLOATAB.ValConfigFCount,pcgMainDelimiter);
    			}
    			break;
    		}
    	}
    	if(ilIsNull == TRUE)
    	{
    		if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
    		{
    			fprintf(fptrUpdate,"%s%s","0",pcgMainDelimiter);
    		}
    		else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
    		{
    			fprintf(fptrDaily,"%s%s","0",pcgMainDelimiter);
    		}
    	}//VAL_CONFIG_F
    	
    	//VAL_CONFIG_B
    	memset( &rlLOATAB, 0, sizeof(rlLOATAB) );
    	memset( &rlLOASQL.SqlBuf,0,sizeof(rlLOASQL.SqlBuf));
      strcpy( rlLOASQL.SqlBuf,rlLOASQL.SqlBufMostCommon);
      //dbg(DEBUG,"Frank@WorkBridge<line-%d>Getting the VAL_CONFIG_B records from LOATAB",__LINE__);
      //dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
      
      ilNoEle = GetNoOfElements(pcgValConfigB,';');
      for(ilI=0; ilI< ilNoEle-1; ilI++)
      {
      	if(strcmp(pclValConfigB[ilI],"#")!=0)
      	{
      		switch(ilI)
      		{
      			case 0:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND TYPE='%s' ",pclValConfigB[0]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 1:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND STYP='%s' ",pclValConfigB[1]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 2:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND SSTP='%s' ",pclValConfigB[2]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 3:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND SSST='%s' ",pclValConfigB[3]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			default:
      				break;
      		}
      	}
      }
      memset( &rlLOATAB, 0, sizeof(rlLOATAB) );
      memset( &rlLOASQL.SqlBufCommon, 0, sizeof(rlLOASQL.SqlBufCommon) );
      strcpy(rlLOASQL.SqlBufCommon,rlLOASQL.SqlBuf);
      //dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
      
      ilIsNull = TRUE;
      ilNoEle = GetNoOfElements(pcgLoadDSSN,',');
 		 	for (ilI = 1; ilI <= ilNoEle; ilI++)
  		{
  			rlLOASQL.SqlCursor = 0;
    		rlLOASQL.SqlFunc = START;
    		
    		memset(pclTmpStr,0,sizeof(pclTmpStr));
    		memset(rlLOASQL.SqlBuf,0,sizeof(rlLOASQL.SqlBuf));
    		strcpy(rlLOASQL.SqlBuf,rlLOASQL.SqlBufCommon);
    		sprintf(pclTmpStr,"AND DSSN='%s' ",pclDSSNSource[ilI-1]);
      	strcat(rlLOASQL.SqlBuf,pclTmpStr);
      	
      	#ifdef TEST
      		dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
    		#endif
    		
    		sql_if( rlLOASQL.SqlFunc, &rlLOASQL.SqlCursor, rlLOASQL.SqlBuf, pclSqlData );
    		get_fld(pclSqlData,FIELD_1,STR,12,rlLOATAB.ValConfigBCount);
  			TrimSpace(rlLOATAB.ValConfigBCount); 
  			close_my_cursor( &rlLOASQL.SqlCursor );
  			
  			#ifdef TEST
    		dbg(DEBUG,"-----rlLOATAB.ValConfigBCount = %d--------",atoi(rlLOATAB.ValConfigBCount));
    		#endif
    		
    		if(atoi(rlLOATAB.ValConfigBCount) != 0)
    		{
    			ilIsNull = FALSE;
    			if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
    			{
    				fprintf(fptrUpdate,"%s%s",rlLOATAB.ValConfigBCount,pcgMainDelimiter);
    			}
    			else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
    			{
    				fprintf(fptrDaily,"%s%s",rlLOATAB.ValConfigBCount,pcgMainDelimiter);
    			}
    			break;
    		}
    	}
    	if(ilIsNull == TRUE)
    	{
    		if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
    		{
    			fprintf(fptrUpdate,"%s%s","0",pcgMainDelimiter);
    		}
    		else  if(igDoDailyFile == TRUE && igIs8AM==TRUE)
    		{
    			fprintf(fptrDaily,"%s%s","0",pcgMainDelimiter);
    		}
    		
    	}
    	//VAL_CONFIG_B
    	
    	//VAL_CONFIG_E
    	memset( &rlLOATAB, 0, sizeof(rlLOATAB) );
    	memset( &rlLOASQL.SqlBuf,0,sizeof(rlLOASQL.SqlBuf));
      strcpy( rlLOASQL.SqlBuf,rlLOASQL.SqlBufMostCommon);
      //dbg(DEBUG,"Frank@WorkBridge<line-%d>Getting the VAL_CONFIG_E records from LOATAB",__LINE__);
      //dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
      
      ilNoEle = GetNoOfElements(pcgValConfigE,';');
      for(ilI=0; ilI< ilNoEle-1; ilI++)
      {
      	if(strcmp(pclValConfigE[ilI],"#")!=0)
      	{
      		switch(ilI)
      		{
      			case 0:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND TYPE='%s' ",pclValConfigE[0]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 1:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND STYP='%s' ",pclValConfigE[1]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 2:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND SSTP='%s' ",pclValConfigE[2]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 3:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND SSST='%s' ",pclValConfigE[3]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			default:
      				break;
      		}
      	}
      }
      memset( &rlLOATAB, 0, sizeof(rlLOATAB) );
      memset( &rlLOASQL.SqlBufCommon, 0, sizeof(rlLOASQL.SqlBufCommon) );
      strcpy(rlLOASQL.SqlBufCommon,rlLOASQL.SqlBuf);
     // dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
      
      ilIsNull = TRUE;
      ilNoEle = GetNoOfElements(pcgLoadDSSN,',');
 		 	for (ilI = 1; ilI <= ilNoEle; ilI++)
  		{
  			rlLOASQL.SqlCursor = 0;
    		rlLOASQL.SqlFunc = START;
    		
    		memset(pclTmpStr,0,sizeof(pclTmpStr));
    		memset(rlLOASQL.SqlBuf,0,sizeof(rlLOASQL.SqlBuf));
    		strcpy(rlLOASQL.SqlBuf,rlLOASQL.SqlBufCommon);
    		sprintf(pclTmpStr,"AND DSSN='%s' ",pclDSSNSource[ilI-1]);
      	strcat(rlLOASQL.SqlBuf,pclTmpStr);
      	
      	#ifdef TEST
      		dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
    		#endif
    		
    		sql_if( rlLOASQL.SqlFunc, &rlLOASQL.SqlCursor, rlLOASQL.SqlBuf, pclSqlData );
    		get_fld(pclSqlData,FIELD_1,STR,12,rlLOATAB.ValConfigECount);
    		close_my_cursor( &rlLOASQL.SqlCursor );
  			TrimSpace(rlLOATAB.ValConfigECount); 
  			
  			#ifdef TEST
    			dbg(DEBUG,"-----rlLOATAB.ValConfigECount = %d--------",atoi(rlLOATAB.ValConfigECount));
    		#endif
    		
    		if(atoi(rlLOATAB.ValConfigECount) != 0)
    		{
    			ilIsNull = FALSE;
    			if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
    			{
    				fprintf(fptrUpdate,"%s%s",rlLOATAB.ValConfigECount,pcgMainDelimiter);
    			}
    			else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
    			{
    				fprintf(fptrDaily,"%s%s",rlLOATAB.ValConfigECount,pcgMainDelimiter);
    			}
    			break;
    		}
    	}
    	if(ilIsNull == TRUE)
    	{
    		if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
    		{
    				fprintf(fptrUpdate,"%s%s","0",pcgMainDelimiter);
    		}
    		else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
    		{
    				fprintf(fptrDaily,"%s%s","0",pcgMainDelimiter);
    		}
    	}
    	//VAL_CONFIG_E
    	
    	
    	//VAL_CARGO
    	memset( &rlLOATAB, 0, sizeof(rlLOATAB) );
    	memset( &rlLOASQL.SqlBuf,0,sizeof(rlLOASQL.SqlBuf));
      strcpy( rlLOASQL.SqlBuf,rlLOASQL.SqlBufMostCommon);
      //dbg(DEBUG,"Frank@WorkBridge<line-%d>Getting the VAL_CARGO records from LOATAB",__LINE__);
      //dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
      
      ilNoEle = GetNoOfElements(pcgValCargo,';');
      for(ilI=0; ilI< ilNoEle-1; ilI++)
      {
      	if(strcmp(pclValCargo[ilI],"#")!=0)
      	{
      		switch(ilI)
      		{
      			case 0:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND TYPE='%s' ",pclValCargo[0]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 1:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND STYP='%s' ",pclValCargo[1]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 2:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND SSTP='%s' ",pclValCargo[2]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 3:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND SSST='%s' ",pclValCargo[3]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			default:
      				break;
      		}
      	}
      }
      memset( &rlLOATAB, 0, sizeof(rlLOATAB) );
      memset( &rlLOASQL.SqlBufCommon, 0, sizeof(rlLOASQL.SqlBufCommon) );
      strcpy(rlLOASQL.SqlBufCommon,rlLOASQL.SqlBuf);
      //dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
      
      ilIsNull = TRUE;
      ilNoEle = GetNoOfElements(pcgLoadDSSN,',');
 		 	for (ilI = 1; ilI <= ilNoEle; ilI++)
  		{
  			rlLOASQL.SqlCursor = 0;
    		rlLOASQL.SqlFunc = START;
    		
    		memset(pclTmpStr,0,sizeof(pclTmpStr));
    		memset(rlLOASQL.SqlBuf,0,sizeof(rlLOASQL.SqlBuf));
    		strcpy(rlLOASQL.SqlBuf,rlLOASQL.SqlBufCommon);
    		sprintf(pclTmpStr,"AND DSSN='%s' ",pclDSSNSource[ilI-1]);
      	strcat(rlLOASQL.SqlBuf,pclTmpStr);
      	
      	#ifdef TEST
      		dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
    		#endif
    		sql_if( rlLOASQL.SqlFunc, &rlLOASQL.SqlCursor, rlLOASQL.SqlBuf, pclSqlData );
    		get_fld(pclSqlData,FIELD_1,STR,12,rlLOATAB.ValCargoCount);
    		close_my_cursor( &rlLOASQL.SqlCursor );
  			TrimSpace(rlLOATAB.ValCargoCount); 
  			
  			#ifdef TEST
    			dbg(DEBUG,"-----rlLOATAB.ValCargoCount = %d--------",atoi(rlLOATAB.ValCargoCount));
    		#endif
    		
    		if(atoi(rlLOATAB.ValCargoCount) != 0)
    		{
    			ilIsNull = FALSE;
    			
    			if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
    			{
    				fprintf(fptrUpdate,"%s%s",rlLOATAB.ValCargoCount,pcgMainDelimiter);
    			}
    			else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
    			{
    				fprintf(fptrDaily,"%s%s",rlLOATAB.ValCargoCount,pcgMainDelimiter);
    			}
    			break;
    		}
    	}
    	if(ilIsNull == TRUE)
    	{
    		if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
    		{
    			fprintf(fptrUpdate,"%s%s","0",pcgMainDelimiter);
    		}
    		else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
    		{
    			fprintf(fptrDaily,"%s%s","0",pcgMainDelimiter);
    		}
    	}//VAL_CARGO
    	
    	//VAL_MAIL
    	memset( &rlLOATAB, 0, sizeof(rlLOATAB) );
    	memset( &rlLOASQL.SqlBuf,0,sizeof(rlLOASQL.SqlBuf));
      strcpy( rlLOASQL.SqlBuf,rlLOASQL.SqlBufMostCommon);
     // dbg(DEBUG,"Frank@WorkBridge<line-%d>Getting the VAL_MAIL records from LOATAB",__LINE__);
      //dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
      
      ilNoEle = GetNoOfElements(pcgValMail,';');
      for(ilI=0; ilI< ilNoEle-1; ilI++)
      {
      	if(strcmp(pclValMail[ilI],"#")!=0)
      	{
      		switch(ilI)
      		{
      			case 0:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND TYPE='%s' ",pclValMail[0]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 1:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND STYP='%s' ",pclValMail[1]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 2:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND SSTP='%s' ",pclValMail[2]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 3:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND SSST='%s' ",pclValMail[3]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			default:
      				break;
      		}
      	}
      }
      memset( &rlLOATAB, 0, sizeof(rlLOATAB) );
      memset( &rlLOASQL.SqlBufCommon, 0, sizeof(rlLOASQL.SqlBufCommon) );
      strcpy(rlLOASQL.SqlBufCommon,rlLOASQL.SqlBuf);
      //dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
      
      ilIsNull = TRUE;
      ilNoEle = GetNoOfElements(pcgLoadDSSN,',');
 		 	for (ilI = 1; ilI <= ilNoEle; ilI++)
  		{
  			rlLOASQL.SqlCursor = 0;
    		rlLOASQL.SqlFunc = START;
    		
    		memset(pclTmpStr,0,sizeof(pclTmpStr));
    		memset(rlLOASQL.SqlBuf,0,sizeof(rlLOASQL.SqlBuf));
    		strcpy(rlLOASQL.SqlBuf,rlLOASQL.SqlBufCommon);
    		sprintf(pclTmpStr,"AND DSSN='%s' ",pclDSSNSource[ilI-1]);
      	strcat(rlLOASQL.SqlBuf,pclTmpStr);
      	
      	#ifdef TEST
      		dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
    		#endif
    		
    		sql_if( rlLOASQL.SqlFunc, &rlLOASQL.SqlCursor, rlLOASQL.SqlBuf, pclSqlData );
    		get_fld(pclSqlData,FIELD_1,STR,12,rlLOATAB.ValMailCount);
    		close_my_cursor( &rlLOASQL.SqlCursor );
  			TrimSpace(rlLOATAB.ValMailCount); 
  			
  			#ifdef TEST
    			dbg(DEBUG,"-----rlLOATAB.ValMailCount = %d--------",atoi(rlLOATAB.ValMailCount));
    		#endif
    		
    		if(atoi(rlLOATAB.ValMailCount) != 0)
    		{
    			ilIsNull = FALSE;
    			
    			if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
    			{
    				fprintf(fptrUpdate,"%s%s",rlLOATAB.ValMailCount,pcgMainDelimiter);
    			}
    			else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
    			{
    				fprintf(fptrDaily,"%s%s",rlLOATAB.ValMailCount,pcgMainDelimiter);
    			}
    			break;
    		}
    	}
    	if(ilIsNull == TRUE)
    	{
    		if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
    		{
    			fprintf(fptrUpdate,"%s%s","0",pcgMainDelimiter);
    		}
    		else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
    		{
    			fprintf(fptrDaily,"%s%s","0",pcgMainDelimiter);
    		}
    	}//
    	
    	if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
    	{	
    		fprintf(fptrUpdate,"%s%s",rlAFTTAB.STYP,pcgMainDelimiter);
    		
    		if(strncmp(rlAFTTAB.ADID,"A",1)==0)
    		{
      		fprintf(fptrUpdate,"%s%s",rlAFTTAB.PSTA,pcgMainDelimiter);
      	}
      	else if(strncmp(rlAFTTAB.ADID,"D",1)==0)
      	{
      		fprintf(fptrUpdate,"%s%s",rlAFTTAB.PSTD,pcgMainDelimiter);
    		}
    	}
    	else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
    	{
    		fprintf(fptrDaily,"%s%s",rlAFTTAB.STYP,pcgMainDelimiter);
    		
    		if(strncmp(rlAFTTAB.ADID,"A",1)==0)
    		{
      		fprintf(fptrDaily,"%s%s",rlAFTTAB.PSTA,pcgMainDelimiter);
      	}
      	else if(strncmp(rlAFTTAB.ADID,"D",1)==0)
      	{
      		fprintf(fptrDaily,"%s%s",rlAFTTAB.PSTD,pcgMainDelimiter);
    		}
    	}
    	
    	//VAL_BOOK_F-----------------------------------------------------------------
      memset( &rlLOATAB, 0, sizeof(rlLOATAB) );
      memset(	&rlLOASQL.SqlBuf,0,sizeof(rlLOASQL.SqlBuf));
      strcpy( rlLOASQL.SqlBuf,rlLOASQL.SqlBufMostCommon);
      //dbg(DEBUG,"Frank@WorkBridge<line-%d>Getting the VAL_BOOK_F records from LOATAB",__LINE__);
      //dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
      
      ilNoEle = GetNoOfElements(pcgValBookF,';');
      for(ilI=0; ilI< ilNoEle-1; ilI++)
      {
      	if(strcmp(pclValBookF[ilI],"#")!=0)
      	{
      		switch(ilI)
      		{
      			case 0:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND TYPE='%s' ",pclValBookF[0]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 1:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND STYP='%s' ",pclValBookF[1]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 2:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND SSTP='%s' ",pclValBookF[2]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 3:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND SSST='%s' ",pclValBookF[3]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			default:
      				break;
      		}
      	}
      }
      memset( &rlLOATAB, 0, sizeof(rlLOATAB) );
      memset( &rlLOASQL.SqlBufCommon, 0, sizeof(rlLOASQL.SqlBufCommon) );
      strcpy(rlLOASQL.SqlBufCommon,rlLOASQL.SqlBuf);
      //dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
      //
      ilIsNull = TRUE;
      ilNoEle = GetNoOfElements(pcgLoadDSSN,',');
 		 	for (ilI = 1; ilI <= ilNoEle; ilI++)
  		{
  			rlLOASQL.SqlCursor = 0;
    		rlLOASQL.SqlFunc = START;
    		
    		memset(pclTmpStr,0,sizeof(pclTmpStr));
    		memset(rlLOASQL.SqlBuf,0,sizeof(rlLOASQL.SqlBuf));
    		strcpy(rlLOASQL.SqlBuf,rlLOASQL.SqlBufCommon);
    		sprintf(pclTmpStr,"AND DSSN='%s' ",pclDSSNSource[ilI-1]);
      	strcat(rlLOASQL.SqlBuf,pclTmpStr);
      	
      	#ifdef TEST
      		dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
    		#endif
    		
    		sql_if( rlLOASQL.SqlFunc, &rlLOASQL.SqlCursor, rlLOASQL.SqlBuf, pclSqlData );
    		get_fld(pclSqlData,FIELD_1,STR,12,rlLOATAB.ValBookFCount);
    		close_my_cursor( &rlLOASQL.SqlCursor );
  			TrimSpace(rlLOATAB.ValBookFCount); 
  			
  			#ifdef TEST
    			dbg(DEBUG,"-----rlLOATAB.ValBookFCount = %d--------",atoi(rlLOATAB.ValBookFCount));
    		#endif
    		
    		if(atoi(rlLOATAB.ValBookFCount) != 0)
    		{
    			ilIsNull = FALSE;
    			
    			if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
    			{
    				fprintf(fptrUpdate,"%s%s",rlLOATAB.ValBookFCount,pcgMainDelimiter);
    			}
    			else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
    			{
    				fprintf(fptrDaily,"%s%s",rlLOATAB.ValBookFCount,pcgMainDelimiter);
    			}
    			break;
    		}
    	}
    	if(ilIsNull == TRUE)
    	{
    		if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
    		{
    			fprintf(fptrUpdate,"%s%s","0",pcgMainDelimiter);
    		}
    		else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
    		{
    			fprintf(fptrDaily,"%s%s","0",pcgMainDelimiter);
    		}
    	}
      //
      
      /*
      VAL_BOOK_B
    	*/
    	memset( &rlLOATAB, 0, sizeof(rlLOATAB) );
    	memset( &rlLOASQL.SqlBuf,0,sizeof(rlLOASQL.SqlBuf));
      strcpy( rlLOASQL.SqlBuf,rlLOASQL.SqlBufMostCommon);
      //dbg(DEBUG,"Frank@WorkBridge<line-%d>Getting the VAL_BOOK_B records from LOATAB",__LINE__);
      //dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
      
      ilNoEle = GetNoOfElements(pcgValBookB,';');
      for(ilI=0; ilI< ilNoEle-1; ilI++)
      {
      	if(strcmp(pclValBookB[ilI],"#")!=0)
      	{
      		switch(ilI)
      		{
      			case 0:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND TYPE='%s' ",pclValBookB[0]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 1:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND STYP='%s' ",pclValBookB[1]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 2:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND SSTP='%s' ",pclValBookB[2]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 3:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND SSST='%s' ",pclValBookB[3]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			default:
      				break;
      		}
      	}
      }
      memset( &rlLOATAB, 0, sizeof(rlLOATAB) );
      memset( &rlLOASQL.SqlBufCommon, 0, sizeof(rlLOASQL.SqlBufCommon) );
      strcpy(rlLOASQL.SqlBufCommon,rlLOASQL.SqlBuf);
      //dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
      //
      ilIsNull = TRUE;
      ilNoEle = GetNoOfElements(pcgLoadDSSN,',');
 		 	for (ilI = 1; ilI <= ilNoEle; ilI++)
  		{
  			rlLOASQL.SqlCursor = 0;
    		rlLOASQL.SqlFunc = START;
    		
    		memset(pclTmpStr,0,sizeof(pclTmpStr));
    		memset(rlLOASQL.SqlBuf,0,sizeof(rlLOASQL.SqlBuf));
    		strcpy(rlLOASQL.SqlBuf,rlLOASQL.SqlBufCommon);
    		sprintf(pclTmpStr,"AND DSSN='%s' ",pclDSSNSource[ilI-1]);
      	strcat(rlLOASQL.SqlBuf,pclTmpStr);
      	
      	#ifdef TEST
      		dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
    		#endif
    		
    		sql_if( rlLOASQL.SqlFunc, &rlLOASQL.SqlCursor, rlLOASQL.SqlBuf, pclSqlData );
    		get_fld(pclSqlData,FIELD_1,STR,12,rlLOATAB.ValBookBCount);
    		close_my_cursor( &rlLOASQL.SqlCursor );
  			TrimSpace(rlLOATAB.ValBookBCount); 
  			
  			#ifdef TEST
    			dbg(DEBUG,"-----rlLOATAB.ValBookBCount = %d--------",atoi(rlLOATAB.ValBookBCount));
    		#endif
    			
    		if(atoi(rlLOATAB.ValBookBCount) != 0)
    		{
    			ilIsNull = FALSE;
    			
    			if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
    			{
    				fprintf(fptrUpdate,"%s%s",rlLOATAB.ValBookBCount,pcgMainDelimiter);
    			}
    			else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
    			{
    				fprintf(fptrDaily,"%s%s",rlLOATAB.ValBookBCount,pcgMainDelimiter);
    			}
    			break;
    		}
    	}
    	if(ilIsNull == TRUE)
    	{
    		if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
    		{
    			fprintf(fptrUpdate,"%s%s","0",pcgMainDelimiter);
    		}
    		else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
    		{
    			fprintf(fptrDaily,"%s%s","0",pcgMainDelimiter);
    		}
    	}
    	
    	//VAL_BOOK_E
    	memset( &rlLOATAB, 0, sizeof(rlLOATAB) );
    	memset(rlLOASQL.SqlBuf,0,sizeof(rlLOASQL.SqlBuf));
      strcpy( rlLOASQL.SqlBuf,rlLOASQL.SqlBufMostCommon);
      //dbg(DEBUG,"Frank@WorkBridge<line-%d>Getting the VAL_BOOK_E records from LOATAB",__LINE__);
      //dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
      
      ilNoEle = GetNoOfElements(pcgValBookE,';');
      for(ilI=0; ilI< ilNoEle-1; ilI++)
      {
      	if(strcmp(pclValBookE[ilI],"#")!=0)
      	{
      		switch(ilI)
      		{
      			case 0:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND TYPE='%s' ",pclValBookE[0]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 1:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND STYP='%s' ",pclValBookE[1]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 2:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND SSTP='%s' ",pclValBookE[2]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 3:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND SSST='%s' ",pclValBookE[3]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			default:
      				break;
      		}
      	}
      }
      memset( &rlLOATAB, 0, sizeof(rlLOATAB) );
      strcpy(rlLOASQL.SqlBufCommon,rlLOASQL.SqlBuf);
      //dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
      
      ilIsNull = TRUE;
      ilNoEle = GetNoOfElements(pcgLoadDSSN,',');
 		 	for (ilI = 1; ilI <= ilNoEle; ilI++)
  		{
  			rlLOASQL.SqlCursor = 0;
    		rlLOASQL.SqlFunc = START;
    		
    		memset(pclTmpStr,0,sizeof(pclTmpStr));
    		memset(rlLOASQL.SqlBuf,0,sizeof(rlLOASQL.SqlBuf));
    		strcpy(rlLOASQL.SqlBuf,rlLOASQL.SqlBufCommon);
    		sprintf(pclTmpStr,"AND DSSN='%s' ",pclDSSNSource[ilI-1]);
      	strcat(rlLOASQL.SqlBuf,pclTmpStr);
      	
      	//dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
    		sql_if( rlLOASQL.SqlFunc, &rlLOASQL.SqlCursor, rlLOASQL.SqlBuf, pclSqlData );
    		get_fld(pclSqlData,FIELD_1,STR,12,rlLOATAB.ValBookECount);
  			TrimSpace(rlLOATAB.ValBookECount); 
    		//dbg(DEBUG,"-----rlLOATAB.ValBookECount = %d--------",atoi(rlLOATAB.ValBookECount));
    		close_my_cursor( &rlLOASQL.SqlCursor );
    		if(atoi(rlLOATAB.ValBookECount) != 0)
    		{
    			ilIsNull = FALSE;
    			
    			if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
    			{
    				fprintf(fptrUpdate,"%s%s",rlLOATAB.ValBookECount,pcgMainDelimiter);
    			}
    			else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
    			{
    				fprintf(fptrDaily,"%s%s",rlLOATAB.ValBookECount,pcgMainDelimiter);
    			}
    			break;
    		}
    	}
    	if(ilIsNull == TRUE)
    	{
    		if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
    		{
    			fprintf(fptrUpdate,"%s%s","0",pcgMainDelimiter);
    		}
    		else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
    		{
    			fprintf(fptrDaily,"%s%s","0",pcgMainDelimiter);
    		}
    	}
    	//
			
			//VAL_ACTUAL_F----------
			memset( &rlLOATAB, 0, sizeof(rlLOATAB) );
    	memset( &rlLOASQL.SqlBuf,0,sizeof(rlLOASQL.SqlBuf));
      strcpy( rlLOASQL.SqlBuf,rlLOASQL.SqlBufMostCommon);
     // dbg(DEBUG,"Frank@WorkBridge<line-%d>Getting the VAL_ACTUAL_F records from LOATAB",__LINE__);
      //dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
      
      ilNoEle = GetNoOfElements(pcgValActualF,';');
      for(ilI=0; ilI< ilNoEle-1; ilI++)
      {
      	if(strcmp(pclValActualF[ilI],"#")!=0)
      	{
      		switch(ilI)
      		{
      			case 0:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND TYPE='%s' ",pclValActualF[0]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 1:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND STYP='%s' ",pclValActualF[1]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 2:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND SSTP='%s' ",pclValActualF[2]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 3:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND SSST='%s' ",pclValActualF[3]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			default:
      				break;
      		}
      	}
      }
      memset( &rlLOATAB, 0, sizeof(rlLOATAB) );
      memset( &rlLOASQL.SqlBufCommon, 0, sizeof(rlLOASQL.SqlBufCommon) );
      strcpy(rlLOASQL.SqlBufCommon,rlLOASQL.SqlBuf);
      //dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
      
      ilIsNull = TRUE;
      ilNoEle = GetNoOfElements(pcgLoadDSSN,',');
 		 	for (ilI = 1; ilI <= ilNoEle; ilI++)
  		{
  			rlLOASQL.SqlCursor = 0;
    		rlLOASQL.SqlFunc = START;
    		
    		memset(pclTmpStr,0,sizeof(pclTmpStr));
    		memset(rlLOASQL.SqlBuf,0,sizeof(rlLOASQL.SqlBuf));
    		strcpy(rlLOASQL.SqlBuf,rlLOASQL.SqlBufCommon);
    		sprintf(pclTmpStr,"AND DSSN='%s' ",pclDSSNSource[ilI-1]);
      	strcat(rlLOASQL.SqlBuf,pclTmpStr);
      	
      	#ifdef TEST
      		dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
    		#endif
    		
    		sql_if( rlLOASQL.SqlFunc, &rlLOASQL.SqlCursor, rlLOASQL.SqlBuf, pclSqlData );
    		get_fld(pclSqlData,FIELD_1,STR,12,rlLOATAB.ValActualFCount);
    		close_my_cursor( &rlLOASQL.SqlCursor );
  			TrimSpace(rlLOATAB.ValActualFCount); 
  			
  			#ifdef TEST
    			dbg(DEBUG,"-----rlLOATAB.ValActualFCount = %d--------",atoi(rlLOATAB.ValActualFCount));
    		#endif
    		
    		if(atoi(rlLOATAB.ValActualFCount) != 0)
    		{
    			ilIsNull = FALSE;
    			
    			if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
    			{
    				fprintf(fptrUpdate,"%s%s",rlLOATAB.ValActualFCount,pcgMainDelimiter);
    			}
    			else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
    			{
    				fprintf(fptrDaily,"%s%s",rlLOATAB.ValActualFCount,pcgMainDelimiter);
    			}
    			break;
    		}
    	}
    	if(ilIsNull == TRUE)
    	{
    		if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
    		{
    			fprintf(fptrUpdate,"%s%s","0",pcgMainDelimiter);
    		}
    		else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
    		{
    			fprintf(fptrDaily,"%s%s","0",pcgMainDelimiter);
    		}
    	}
    	
			//VAL_ACTUAL_B----------
			memset( &rlLOATAB, 0, sizeof(rlLOATAB) );
    	memset(rlLOASQL.SqlBuf,0,sizeof(rlLOASQL.SqlBuf));
      strcpy( rlLOASQL.SqlBuf,rlLOASQL.SqlBufMostCommon);
     // dbg(DEBUG,"Frank@WorkBridge<line-%d>Getting the VAL_ACTUAL_B records from LOATAB",__LINE__);
     // dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
      
      ilNoEle = GetNoOfElements(pcgValActualB,';');
      for(ilI=0; ilI< ilNoEle-1; ilI++)
      {
      	if(strcmp(pclValActualB[ilI],"#")!=0)
      	{
      		switch(ilI)
      		{
      			case 0:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND TYPE='%s' ",pclValActualB[0]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 1:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND STYP='%s' ",pclValActualB[1]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 2:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND SSTP='%s' ",pclValActualB[2]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 3:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND SSST='%s' ",pclValActualB[3]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			default:
      				break;
      		}
      	}
      }
      memset( &rlLOATAB, 0, sizeof(rlLOATAB) );
      memset( &rlLOASQL.SqlBufCommon, 0, sizeof(rlLOASQL.SqlBufCommon) );
      strcpy(rlLOASQL.SqlBufCommon,rlLOASQL.SqlBuf);
     // dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
      
      ilIsNull = TRUE;
      ilNoEle = GetNoOfElements(pcgLoadDSSN,',');
 		 	for (ilI = 1; ilI <= ilNoEle; ilI++)
  		{
  			rlLOASQL.SqlCursor = 0;
    		rlLOASQL.SqlFunc = START;
    		
    		memset(pclTmpStr,0,sizeof(pclTmpStr));
    		memset(rlLOASQL.SqlBuf,0,sizeof(rlLOASQL.SqlBuf));
    		strcpy(rlLOASQL.SqlBuf,rlLOASQL.SqlBufCommon);
    		sprintf(pclTmpStr,"AND DSSN='%s' ",pclDSSNSource[ilI-1]);
      	strcat(rlLOASQL.SqlBuf,pclTmpStr);
      	
      	#ifdef TEST
      		dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
    		#endif
    		
    		sql_if( rlLOASQL.SqlFunc, &rlLOASQL.SqlCursor, rlLOASQL.SqlBuf, pclSqlData );
    		get_fld(pclSqlData,FIELD_1,STR,12,rlLOATAB.ValActualFCount);
    		close_my_cursor( &rlLOASQL.SqlCursor );
  			TrimSpace(rlLOATAB.ValActualFCount); 
  			
  			#ifdef TEST
    			dbg(DEBUG,"-----rlLOATAB.ValActualBCount = %d--------",atoi(rlLOATAB.ValActualBCount));
    		#endif
    		
    		if(atoi(rlLOATAB.ValActualBCount) != 0)
    		{
    			ilIsNull = FALSE;
    			
    			if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
    			{
    				fprintf(fptrUpdate,"%s%s",rlLOATAB.ValActualBCount,pcgMainDelimiter);
    			}
    			else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
    			{
    				fprintf(fptrDaily,"%s%s",rlLOATAB.ValActualBCount,pcgMainDelimiter);
    			}
    			break;
    		}
    	}
    	if(ilIsNull == TRUE)
    	{
    		if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
    		{
    			fprintf(fptrUpdate,"%s%s","0",pcgMainDelimiter);
    		}
    		else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
    		{
    			fprintf(fptrDaily,"%s%s","0",pcgMainDelimiter);
    		}
    	}
			//----------
			
			//VAL_ACTUAL_E----------
			memset( &rlLOATAB, 0, sizeof(rlLOATAB) );
    	memset(rlLOASQL.SqlBuf,0,sizeof(rlLOASQL.SqlBuf));
      strcpy( rlLOASQL.SqlBuf,rlLOASQL.SqlBufMostCommon);
     // dbg(DEBUG,"Frank@WorkBridge<line-%d>Getting the VAL_ACTUAL_E records from LOATAB",__LINE__);
     // dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
      
      ilNoEle = GetNoOfElements(pcgValActualE,';');
      for(ilI=0; ilI< ilNoEle-1; ilI++)
      {
      	if(strcmp(pclValActualE[ilI],"#")!=0)
      	{
      		switch(ilI)
      		{
      			case 0:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND TYPE='%s' ",pclValActualE[0]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 1:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND STYP='%s' ",pclValActualE[1]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 2:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND SSTP='%s' ",pclValActualE[2]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 3:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND SSST='%s' ",pclValActualE[3]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			default:
      				break;
      		}
      	}
      }
      memset( &rlLOATAB, 0, sizeof(rlLOATAB) );
      memset( &rlLOASQL.SqlBufCommon, 0, sizeof(rlLOASQL.SqlBufCommon) );
      strcpy(rlLOASQL.SqlBufCommon,rlLOASQL.SqlBuf);
     // dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
      
      ilIsNull = TRUE;
      ilNoEle = GetNoOfElements(pcgLoadDSSN,',');
 		 	for (ilI = 1; ilI <= ilNoEle; ilI++)
  		{
  			rlLOASQL.SqlCursor = 0;
    		rlLOASQL.SqlFunc = START;
    		
    		memset(pclTmpStr,0,sizeof(pclTmpStr));
    		memset(rlLOASQL.SqlBuf,0,sizeof(rlLOASQL.SqlBuf));
    		strcpy(rlLOASQL.SqlBuf,rlLOASQL.SqlBufCommon);
    		sprintf(pclTmpStr,"AND DSSN='%s' ",pclDSSNSource[ilI-1]);
      	strcat(rlLOASQL.SqlBuf,pclTmpStr);
      	
      	#ifdef TEST
      		dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
    		#endif
    		
    		sql_if( rlLOASQL.SqlFunc, &rlLOASQL.SqlCursor, rlLOASQL.SqlBuf, pclSqlData );
    		get_fld(pclSqlData,FIELD_1,STR,12,rlLOATAB.ValActualECount);
    		close_my_cursor( &rlLOASQL.SqlCursor );
  			TrimSpace(rlLOATAB.ValActualECount); 
  			
  			#ifdef TEST
    			dbg(DEBUG,"-----rlLOATAB.ValActualECount = %d--------",atoi(rlLOATAB.ValActualECount));
    		#endif
    		
    		if(atoi(rlLOATAB.ValActualECount) != 0)
    		{
    			ilIsNull = FALSE;
    			
    			if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
    			{
    				fprintf(fptrUpdate,"%s%s",rlLOATAB.ValActualECount,pcgMainDelimiter);
    			}
    			else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
    			{
    				fprintf(fptrDaily,"%s%s",rlLOATAB.ValActualECount,pcgMainDelimiter);
    			}
    			break;
    		}
    	}
    	if(ilIsNull == TRUE)
    	{
    		if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
    		{
    			fprintf(fptrUpdate,"%s%s","0",pcgMainDelimiter);
    		}
    		else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
    		{
    			fprintf(fptrDaily,"%s%s","0",pcgMainDelimiter);
    		}
    	}
			//VAL_ACTUAL_E----------
			
			//VAL_BAGS
			memset( &rlLOATAB, 0, sizeof(rlLOATAB) );
    	memset(rlLOASQL.SqlBuf,0,sizeof(rlLOASQL.SqlBuf));
      strcpy( rlLOASQL.SqlBuf,rlLOASQL.SqlBufMostCommon);
     // dbg(DEBUG,"Frank@WorkBridge<line-%d>Getting the VAL_BAGS records from LOATAB",__LINE__);
     // dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
      
      ilNoEle = GetNoOfElements(pcgValBags,';');
      for(ilI=0; ilI< ilNoEle-1; ilI++)
      {
      	if(strcmp(pclValBags[ilI],"#")!=0)
      	{
      		switch(ilI)
      		{
      			case 0:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND TYPE='%s' ",pclValBags[0]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 1:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND STYP='%s' ",pclValBags[1]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 2:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND SSTP='%s' ",pclValBags[2]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			case 3:
      				memset(pclTmpStr,0,sizeof(pclTmpStr));
      				sprintf(pclTmpStr,"AND SSST='%s' ",pclValBags[3]);
      				strcat(rlLOASQL.SqlBuf,pclTmpStr);
      				break;
      			default:
      				break;
      		}
      	}
      }
      memset( &rlLOATAB, 0, sizeof(rlLOATAB) );
      memset( &rlLOASQL.SqlBufCommon, 0, sizeof(rlLOASQL.SqlBufCommon) );
      strcpy(rlLOASQL.SqlBufCommon,rlLOASQL.SqlBuf);
     // dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
      
      ilIsNull = TRUE;
      ilNoEle = GetNoOfElements(pcgLoadDSSN,',');
 		 	for (ilI = 1; ilI <= ilNoEle; ilI++)
  		{
  			rlLOASQL.SqlCursor = 0;
    		rlLOASQL.SqlFunc = START;
    		
    		memset(pclTmpStr,0,sizeof(pclTmpStr));
    		memset(rlLOASQL.SqlBuf,0,sizeof(rlLOASQL.SqlBuf));
    		strcpy(rlLOASQL.SqlBuf,rlLOASQL.SqlBufCommon);
    		sprintf(pclTmpStr,"AND DSSN='%s' ",pclDSSNSource[ilI-1]);
      	strcat(rlLOASQL.SqlBuf,pclTmpStr);
      	
      	#ifdef TEST
      		dbg(DEBUG,"rlLOASQL.SqlBuf = %s",rlLOASQL.SqlBuf);
    		#endif
    		
    		sql_if( rlLOASQL.SqlFunc, &rlLOASQL.SqlCursor, rlLOASQL.SqlBuf, pclSqlData );
    		get_fld(pclSqlData,FIELD_1,STR,12,rlLOATAB.ValBagsCount);
    		close_my_cursor( &rlLOASQL.SqlCursor );
  			TrimSpace(rlLOATAB.ValBagsCount); 
  			
  			#ifdef TEST
    			dbg(DEBUG,"-----rlLOATAB.ValBagsCount = %d--------",atoi(rlLOATAB.ValBagsCount));
    		#endif
    		
    		if(atoi(rlLOATAB.ValBagsCount) != 0)
    		{
    			ilIsNull = FALSE;
    			
    			if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
    			{
    				fprintf(fptrUpdate,"%s%s",rlLOATAB.ValBagsCount,pcgMainDelimiter);
    			}
					else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
					{
						fprintf(fptrDaily,"%s%s",rlLOATAB.ValBagsCount,pcgMainDelimiter);
					}
    			break;
    		}
    	}
    	if(ilIsNull == TRUE)
    	{
    		if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
    		{
    			fprintf(fptrUpdate,"%s%s","0",pcgMainDelimiter);
    		}
    		else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
    		{
    			fprintf(fptrDaily,"%s%s","0",pcgMainDelimiter);	
    		}
    	}
			//VAL_BAGS
			
			if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
			{
				if(strncmp(rlAFTTAB.STEV," ",1)==0 || strlen(rlAFTTAB.STEV) == 0)
				{
					fprintf(fptrUpdate,"%s%s","0",pcgMainDelimiter);
				}
				else
				{	
					fprintf(fptrUpdate,"%s%s",rlAFTTAB.STEV,pcgMainDelimiter);
				}
				
				if(strncmp(rlAFTTAB.ADID,"A",1)==0)
				{
					if(strncmp(rlAFTTAB.GTA1," ",1)==0 || strlen(rlAFTTAB.GTA1) == 0)
					{
						fprintf(fptrUpdate,"%s%s","0",pcgMainDelimiter);
					}
					else
					{	
						fprintf(fptrUpdate,"%s%s",rlAFTTAB.GTA1,pcgMainDelimiter);
					}
				}
				else if(strncmp(rlAFTTAB.ADID,"D",1)==0)
				{
					if(strncmp(rlAFTTAB.GTD1," ",1)==0 || strlen(rlAFTTAB.GTD1) == 0)
					{
						fprintf(fptrUpdate,"%s%s","0",pcgMainDelimiter);
					}
					else
					{	
						fprintf(fptrUpdate,"%s%s",rlAFTTAB.GTD1,pcgMainDelimiter);
					}
				}
				
				if(strncmp(rlAFTTAB.CKIF," ",1)==0 || strlen(rlAFTTAB.CKIF) == 0)
				{
	      	fprintf(fptrUpdate,"%s%s","0",pcgMainDelimiter);
	      }
	      else
	      {
	      	fprintf(fptrUpdate,"%s%s",rlAFTTAB.CKIF,pcgMainDelimiter);
	      }
	      
	      if(strncmp(rlAFTTAB.CKIT," ",1)==0 || strlen(rlAFTTAB.CKIT) == 0)
				{
	      	//fprintf(fptrUpdate,"%s%s","0",pcgMainDelimiter);
	      	fprintf(fptrUpdate,"%s","0");
	      }
	      else
	      {
	       	//fprintf(fptrUpdate,"%s%s",rlAFTTAB.CKIT,pcgMainDelimiter);
	       	fprintf(fptrUpdate,"%s",rlAFTTAB.CKIT);
	      }
    	}
    	else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
    	{
				if(strncmp(rlAFTTAB.STEV," ",1)==0 || strlen(rlAFTTAB.STEV) == 0)
				{
					fprintf(fptrDaily,"%s%s","0",pcgMainDelimiter);
				}
				else
				{	
					fprintf(fptrDaily,"%s%s",rlAFTTAB.STEV,pcgMainDelimiter);
				}
				
				if(strncmp(rlAFTTAB.ADID,"A",1)==0)
				{
					if(strncmp(rlAFTTAB.GTA1," ",1)==0 || strlen(rlAFTTAB.GTA1) == 0)
					{
						fprintf(fptrDaily,"%s%s","0",pcgMainDelimiter);
					}
					else
					{	
						fprintf(fptrDaily,"%s%s",rlAFTTAB.GTA1,pcgMainDelimiter);
					}
				}
				else if(strncmp(rlAFTTAB.ADID,"D",1)==0)
				{
					if(strncmp(rlAFTTAB.GTD1," ",1)==0 || strlen(rlAFTTAB.GTD1) == 0)
					{
						fprintf(fptrDaily,"%s%s","0",pcgMainDelimiter);
					}
					else
					{	
						fprintf(fptrDaily,"%s%s",rlAFTTAB.GTD1,pcgMainDelimiter);
					}
				}
				
				if(strncmp(rlAFTTAB.CKIF," ",1)==0 || strlen(rlAFTTAB.CKIF) == 0)
				{
	      	fprintf(fptrDaily,"%s%s","0",pcgMainDelimiter);
	      }
	      else
	      {
	      	fprintf(fptrDaily,"%s%s",rlAFTTAB.CKIF,pcgMainDelimiter);
	      }
	      
	      if(strncmp(rlAFTTAB.CKIT," ",1)==0 || strlen(rlAFTTAB.CKIT) == 0)
				{
	      	//fprintf(fptrDaily,"%s%s","0",pcgMainDelimiter);
	      	fprintf(fptrDaily,"%s","0");
	      }
	      else
	      {
	       	//fprintf(fptrDaily,"%s%s",rlAFTTAB.CKIT,pcgMainDelimiter);
	       	fprintf(fptrDaily,"%s",rlAFTTAB.CKIT);
	      }
    	}
    	
    	//Special Service Request
    	memset( &rlDPXSQL.SqlBuf,0,sizeof(rlDPXSQL.SqlBuf));
      sprintf( rlDPXSQL.SqlBuf, "SELECT PRMT,COUNT(*) FROM DPXTAB WHERE FLNU=%s GROUP BY PRMT", rlAFTTAB.URNO );
      
      dbg(DEBUG,"Frank@WorkBridge<line-%d><SQL \n\n%s\n>",__LINE__,rlDPXSQL.SqlBuf);
      
      rlDPXSQL.SqlCursor = 0;
    	rlDPXSQL.SqlFunc = START;
    	
    	ilIsNotExisted = TRUE;
    	memset( &rlDPXTAB, 0, sizeof(rlDPXTAB) );
    	
    	/*SELECT PRMT,COUNT(*) FROM DPXTAB WHERE FLNU=rlAFTTAB.URNO GROUP BY PRMT*/
    	ilI = 0;
    	ilFlag = FALSE;
    	while( (sql_if( rlDPXSQL.SqlFunc, &rlDPXSQL.SqlCursor, rlDPXSQL.SqlBuf, pclSqlData )) == DB_SUCCESS)
    	{
    		ilI++;
    		ilFlag = TRUE;
    		rlDPXSQL.SqlFunc = NEXT;
    		get_fld(pclSqlData,FIELD_1,STR,12,rlDPXTAB.PRMTName);		TrimSpace(rlDPXTAB.PRMTName); 
      	get_fld(pclSqlData,FIELD_2,STR,12,rlDPXTAB.PRMTCount); 	TrimSpace(rlDPXTAB.PRMTCount);
      	
      	/*if(strlen(rlDPXTAB.PRMTName) != 0 && strncmp(rlDPXTAB.PRMTName," ",1)!=0)
      	{
	      		dbg(DEBUG,"-----rlDPXTAB.PRMTName = %s <> rlDPXTAB.PRMTCount = %s--------",rlDPXTAB.PRMTName,rlDPXTAB.PRMTCount);
	      	
	      	if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
	      	{
	      		fprintf(fptrUpdate,"SSR-%s%s",rlDPXTAB.PRMTName,pcgMinorDelimiter);
	      		fprintf(fptrUpdate,"%s%s",rlDPXTAB.PRMTCount,pcgMinorDelimiter);
	      	}
	      	else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
	      	{
	      		fprintf(fptrDaily,"%s%s",rlDPXTAB.PRMTName,pcgMinorDelimiter);
	      		fprintf(fptrDaily,"%s%s",rlDPXTAB.PRMTCount,pcgMinorDelimiter);
	      	}
	      	ilIsNotExisted = FALSE;
	      }*/
	      if(strlen(rlDPXTAB.PRMTName) != 0 && strncmp(rlDPXTAB.PRMTName," ",1)!=0)
      	{
	      		dbg(DEBUG,"-----rlDPXTAB.PRMTName = %s <> rlDPXTAB.PRMTCount = %s--------",rlDPXTAB.PRMTName,rlDPXTAB.PRMTCount);
	      	
	      	if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
	      	{
	      		if(ilI == 1)
	      		{
	      			fprintf(fptrUpdate,"%s%s%s%s",pcgMainDelimiter,rlDPXTAB.PRMTName,pcgMinorDelimiter,rlDPXTAB.PRMTCount);
	      		}
	      		else
	      		{
	      			fprintf(fptrUpdate,"%s%s%s%s",pcgMinorDelimiter,rlDPXTAB.PRMTName,pcgMinorDelimiter,rlDPXTAB.PRMTCount);
						}	      	
	      	}
	      	else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
	      	{
	      		if(ilI == 1)
	      		{
	      			fprintf(fptrDaily,"%s%s%s%s",pcgMainDelimiter,rlDPXTAB.PRMTName,pcgMinorDelimiter,rlDPXTAB.PRMTCount);
	      		}
	      		else
	      		{
	      			fprintf(fptrDaily,"%s%s%s%s",pcgMinorDelimiter,rlDPXTAB.PRMTName,pcgMinorDelimiter,rlDPXTAB.PRMTCount);
	      		}
	      	}
	      }//ilIsNotExisted = FALSE;
    	}
    	close_my_cursor( &rlDPXSQL.SqlCursor );
    	if(igDoUpdateFile == TRUE && igIs8AM==FALSE && ilFlag != TRUE)
  		{
  			fprintf(fptrUpdate,"%s",pcgMainDelimiter);
  		}
  		else if(igDoDailyFile == TRUE && igIs8AM==TRUE && ilFlag != TRUE)
  		{
  			fprintf(fptrDaily,"%s",pcgMainDelimiter);
  		}
  		
    	/*if(ilIsNotExisted == TRUE)
    	{	
    		if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
    		{
    			fprintf(fptrUpdate,"SSR-%s",pcgMainDelimiter);
    		}
    		else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
    		{
    			fprintf(fptrDaily,"%s",pcgMainDelimiter);
    		}
    	}*/
    	
			
//Transfer
			if(strncmp(rlAFTTAB.ORG3,"AUH",3)==0)
			{
				memset( &rlTRANSFERSQL, 0, sizeof(rlTRANSFERSQL) );
				memset( &rlTRANSFERSQL.SqlBufCommon, 0, sizeof(rlTRANSFERSQL.SqlBufCommon) );
				
				sprintf( rlTRANSFERSQL.SqlBuf,"SELECT DISTINCT(FLNU) FROM LOATAB WHERE COFL=%s AND FLNU>0 AND "
																			"((TYPE='PAX' and STYP=' ' and SSTP='TD') OR "
																			 "(TYPE='BAG' and STYP='S' and SSTP='TD')) ", rlAFTTAB.URNO );
																			 
				strcpy(rlTRANSFERSQL.SqlBufCommon,rlTRANSFERSQL.SqlBuf);
				
				ilNextDSSNforLOATAB = TRUE;
				ilNoEle = GetNoOfElements(pcgDSSN,',');
				for (ilI = 1; ilI <= ilNoEle; ilI++)
				{
					memset(pclTmpStr,0,sizeof(pclTmpStr));
					memset( &rlTRANSFERSQL.SqlBuf, 0, sizeof(rlTRANSFERSQL.SqlBuf) );
					strcpy(rlTRANSFERSQL.SqlBuf,rlTRANSFERSQL.SqlBufCommon);
					sprintf(pclTmpStr,"AND DSSN='%s' ",pclDSSNSourceForTransfer[ilI-1]);
					strcat(rlTRANSFERSQL.SqlBuf,pclTmpStr);
					
					dbg(DEBUG,"<line%d>rlTRANSFERSQL.SqlBuf = \n\n%s\n\n",__LINE__,rlTRANSFERSQL.SqlBuf);
					
					rlTRANSFERSQL.SqlCursor = 0;
	 				rlTRANSFERSQL.SqlFunc = START;
	 				
	 		/*sprintf( rlTRANSFERSQL.SqlBuf,"SELECT DISTINCT(FLNU) FROM LOATAB WHERE COFL=%s AND FLNU>0 AND "
																			"((TYPE='PAX' and STYP=' ' and SSTP='TD') OR "
																			 "(TYPE='BAG' and STYP='S' and SSTP='TD')) ", rlAFTTAB.URNO );*/
	 				memset( &rlTRANSFER, 0, sizeof(rlTRANSFER) );
	 				ilNextDSSNforLOATAB = TRUE;
	 				ilJ = 0;
	 				while( (sql_if( rlTRANSFERSQL.SqlFunc, &rlTRANSFERSQL.SqlCursor, rlTRANSFERSQL.SqlBuf, pclSqlData )) == DB_SUCCESS)
	 				{
	 					ilJ++;
	 					ilNextDSSNforLOATAB = FALSE;
	 					rlTRANSFERSQL.SqlFunc = NEXT;
	 					
	 					memset( &rlTRANSFER.FLNU, 0, sizeof(rlTRANSFER.FLNU) );
	 					
	 					get_fld(pclSqlData,FIELD_1,STR,12,rlTRANSFER.FLNU);
	 					TrimSpace(rlTRANSFER.FLNU);
	 					
	 					dbg(DEBUG,"++++++rlTRANSFER.FLNU = <%s>",rlTRANSFER.FLNU);
						
						memset( &rlTRANSFERSQL1, 0, sizeof(rlTRANSFERSQL1) );
						sprintf( rlTRANSFERSQL1.SqlBuf, "SELECT ALC2,FLTN,FLNS,ETAI FROM AFTTAB WHERE URNO='%s' ",rlTRANSFER.FLNU);
						
						dbg(DEBUG,"<line%d>rlTRANSFERSQL1.SqlBuf = \n\n%s\n",__LINE__,rlTRANSFERSQL1.SqlBuf);
					
						rlTRANSFERSQL1.SqlCursor = 0;
	 					rlTRANSFERSQL1.SqlFunc = START;
																			
						if(strlen(pcgUpdateExcludeFtyp) != 0)
						{
							memset(pclTmpStr,0,sizeof(pclTmpStr));
							sprintf(pclTmpStr,"AND FTYP NOT IN (%s) ",pcgUpdateExcludeFtyp);
							strcat(rlTRANSFERSQL1.SqlBuf,pclTmpStr);
						}
						
						/*sprintf( rlTRANSFERSQL1.SqlBuf, "SELECT ALC2,FLTN,FLNS,ETAI FROM AFTTAB WHERE URNO='%s' AND FTYP NOT IN()",
																																														rlTRANSFER.FLNU);*/
						if(sql_if(rlTRANSFERSQL1.SqlFunc, &rlTRANSFERSQL1.SqlCursor, rlTRANSFERSQL1.SqlBuf, pclSqlData) == DB_SUCCESS)
						{
							memset( &rlTRANSFER.ALC2, 0, sizeof(rlTRANSFER.ALC2) );
							memset( &rlTRANSFER.FLTN, 0, sizeof(rlTRANSFER.FLTN) );
							memset( &rlTRANSFER.FLNS, 0, sizeof(rlTRANSFER.FLNS) );
							memset( &rlTRANSFER.ETAI, 0, sizeof(rlTRANSFER.ETAI) );
							
							get_fld(pclSqlData,FIELD_1,STR,12,rlTRANSFER.ALC2);
					 		get_fld(pclSqlData,FIELD_2,STR,12,rlTRANSFER.FLTN);
					 		get_fld(pclSqlData,FIELD_3,STR,12,rlTRANSFER.FLNS);
					 		get_fld(pclSqlData,FIELD_4,STR,14,rlTRANSFER.ETAI);
					 		TrimSpace(rlTRANSFER.ALC2);
					 		TrimSpace(rlTRANSFER.FLTN);
					 		TrimSpace(rlTRANSFER.FLNS);
					 		TrimSpace(rlTRANSFER.ETAI);
					 		
					 		dbg(DEBUG,"line<%d>rlTRANSFER.ALC2<%s>",__LINE__,rlTRANSFER.ALC2);
					 		dbg(DEBUG,"line<%d>rlTRANSFER.FLTN<%s>",__LINE__,rlTRANSFER.FLTN);
					 		dbg(DEBUG,"line<%d>rlTRANSFER.FLNS<%s>",__LINE__,rlTRANSFER.FLNS);
					 		dbg(DEBUG,"line<%d>rlTRANSFER.ETAI<%s>",__LINE__,rlTRANSFER.ETAI);
					 		
					 		if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
						 	{
						 		if(ilJ == 1 && ilFlag != TRUE)
						 		{
    							fprintf(fptrUpdate,"%s%s%s",pcgMainDelimiter,rlTRANSFER.ALC2,pcgMinorDelimiter);
						 		}
						 		else if(ilJ == 1 && ilFlag == TRUE)
						 		{
						 			fprintf(fptrUpdate,"%s%s%s",pcgMainDelimiter,rlTRANSFER.ALC2,pcgMinorDelimiter);
						 		}
						 		else
						 		{
						 			fprintf(fptrUpdate,"%s%s%s",pcgMinorDelimiter,rlTRANSFER.ALC2,pcgMinorDelimiter);
						 		}
						 		fprintf(fptrUpdate,"%s%s%s",rlTRANSFER.FLTN,rlTRANSFER.FLNS,pcgMinorDelimiter);
    						fprintf(fptrUpdate,"%s%s",rlTRANSFER.ETAI,pcgMinorDelimiter);
						 	}
					 		else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
					 		{
					 			if(ilJ == 1 && ilFlag != TRUE)
					 			{
					 				fprintf(fptrDaily,"%s%s%s",pcgMainDelimiter,rlTRANSFER.ALC2,pcgMinorDelimiter);
  							}
  							else if(ilJ == 1 && ilFlag == TRUE)
  							{
  								fprintf(fptrDaily,"%s%s%s",pcgMainDelimiter,rlTRANSFER.ALC2,pcgMinorDelimiter);
  							}
  							else
  							{
  								fprintf(fptrDaily,"%s%s%s",pcgMinorDelimiter,rlTRANSFER.ALC2,pcgMinorDelimiter);
  							}
  							fprintf(fptrDaily,"%s%s%s",rlTRANSFER.FLTN,rlTRANSFER.FLNS,pcgMinorDelimiter);
  							fprintf(fptrDaily,"%s%s",rlTRANSFER.ETAI,pcgMinorDelimiter);
					 		}					 
						}
						else
						{
							//the alc2,fltn+flns,etai are all null
							if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
					 		{
					 			if(ilJ == 1 && ilFlag != TRUE)
					 			{
									fprintf(fptrUpdate,"%s%s",pcgMainDelimiter,pcgMinorDelimiter);
								}
								else if(ilJ == 1 && ilFlag == TRUE)
								{
									fprintf(fptrUpdate,"%s%s",pcgMainDelimiter,pcgMinorDelimiter);
								}
								else
								{
									fprintf(fptrUpdate,"%s%s",pcgMinorDelimiter,pcgMinorDelimiter);
								}
								fprintf(fptrUpdate,"%s",pcgMinorDelimiter);
								fprintf(fptrUpdate,"%s",pcgMinorDelimiter);
					 		}
					 		else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
					 		{
					 			if(ilJ == 1 && ilFlag != TRUE)
					 			{
					 				fprintf(fptrDaily,"%s%s",pcgMainDelimiter,pcgMinorDelimiter);
								}
								else if(ilJ == 1 && ilFlag == TRUE)
								{
									fprintf(fptrDaily,"%s%s",pcgMainDelimiter,pcgMinorDelimiter);
								}
								else
								{
									fprintf(fptrDaily,"%s%s",pcgMinorDelimiter,pcgMinorDelimiter);
								}
								fprintf(fptrDaily,"%s",pcgMinorDelimiter);
								fprintf(fptrDaily,"%s",pcgMinorDelimiter);
					 		}	
						}
						
						close_my_cursor( &rlTRANSFERSQL1.SqlCursor );
	 					
	 					/*3 get the number of PAX and BAG*/
						
						/*PAX*/
						memset( &rlTRANSFERSQL2, 0, sizeof(rlTRANSFERSQL2) );
						sprintf( rlTRANSFERSQL2.SqlBuf, "SELECT SUM(VALU) FROM LOATAB WHERE FLNU=%s AND "
																						"TYPE='PAX' AND STYP=' ' AND "
																						"SSTP='TD' AND COFL='%s' ",rlTRANSFER.FLNU,rlAFTTAB.URNO);
						
						strcpy(rlTRANSFERSQL2.SqlBufCommon,rlTRANSFERSQL2.SqlBuf);
						
						rlTRANSFERSQL2.SqlCursor = 0;
						rlTRANSFERSQL2.SqlFunc = START;
						
						memset(pclTmpStr,0,sizeof(pclTmpStr));
						memset(rlTRANSFERSQL2.SqlBuf,0,sizeof(rlTRANSFERSQL2.SqlBuf));
        		strcpy(rlTRANSFERSQL2.SqlBuf,rlTRANSFERSQL2.SqlBufCommon);
        		sprintf(pclTmpStr,"AND DSSN='%s' AND VALU<>' '",pclDSSNSourceForTransfer[ilI-1]);
     				strcat(rlTRANSFERSQL2.SqlBuf,pclTmpStr);
     				
     				dbg(DEBUG,"<line%d>rlTRANSFERSQL2.SqlBuf = \n\n%s",__LINE__,rlTRANSFERSQL2.SqlBuf);
	 					
	 					sql_if( rlTRANSFERSQL2.SqlFunc, &rlTRANSFERSQL2.SqlCursor, rlTRANSFERSQL2.SqlBuf, pclSqlData );
						get_fld(pclSqlData,FIELD_1,STR,12,rlTRANSFER.PAXCount);
						close_my_cursor( &rlTRANSFERSQL2.SqlCursor );
						TrimSpace(rlTRANSFER.PAXCount);
						
						dbg(DEBUG,"line<%d>PAXCount = <%s>\n",__LINE__,rlTRANSFER.PAXCount);
						
						if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
						{
							fprintf(fptrUpdate,"%s%s",rlTRANSFER.PAXCount,pcgMinorDelimiter);
						}
						else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
						{
							fprintf(fptrDaily,"%s%s",rlTRANSFER.PAXCount,pcgMinorDelimiter);
						}
						
						/*BAG*/
						memset( &rlTRANSFERSQL3, 0, sizeof(rlTRANSFERSQL3) );
						sprintf( rlTRANSFERSQL3.SqlBuf, "SELECT SUM(VALU) FROM LOATAB WHERE FLNU=%s AND "
																						"TYPE='BAG' AND STYP='S' AND "
																						"SSTP='TD' AND COFL='%s' ",rlTRANSFER.FLNU,rlAFTTAB.URNO);
						
						strcpy(rlTRANSFERSQL3.SqlBufCommon,rlTRANSFERSQL3.SqlBuf);
						
						rlTRANSFERSQL3.SqlCursor = 0;
						rlTRANSFERSQL3.SqlFunc = START;
						
						memset(pclTmpStr,0,sizeof(pclTmpStr));
						memset(rlTRANSFERSQL3.SqlBuf,0,sizeof(rlTRANSFERSQL3.SqlBuf));
		        strcpy(rlTRANSFERSQL3.SqlBuf,rlTRANSFERSQL3.SqlBufCommon);
		        sprintf(pclTmpStr,"AND DSSN='%s' AND VALU<>' '",pclDSSNSourceForTransfer[ilI-1]);
		     		strcat(rlTRANSFERSQL3.SqlBuf,pclTmpStr);
		     		
		     		dbg(DEBUG,"<line%d>rlTRANSFERSQL3.SqlBuf = \n\n%s",__LINE__,rlTRANSFERSQL3.SqlBuf);
		     		
		     		sql_if( rlTRANSFERSQL3.SqlFunc, &rlTRANSFERSQL3.SqlCursor, rlTRANSFERSQL3.SqlBuf, pclSqlData );
						get_fld(pclSqlData,FIELD_1,STR,12,rlTRANSFER.BagCount);  
						close_my_cursor( &rlTRANSFERSQL3.SqlCursor );        
						TrimSpace(rlTRANSFER.BagCount);
						
						dbg(DEBUG,"line<%d>BagCount = <%s>\n",__LINE__,rlTRANSFER.BagCount);
						
						if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
						{
							//fprintf(fptrUpdate,"%s%s",rlTRANSFER.BagCount,pcgMinorDelimiter);
							fprintf(fptrUpdate,"%s",rlTRANSFER.BagCount);
						}
						else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
						{
							//fprintf(fptrDaily,"%s%s",rlTRANSFER.BagCount,pcgMinorDelimiter);
							fprintf(fptrDaily,"%s",rlTRANSFER.BagCount);
						}
	 				}
	 				close_my_cursor( &rlTRANSFERSQL.SqlCursor );
	 				
	 				if(ilNextDSSNforLOATAB == FALSE)
	 				{
	 					//ilNextDSSNforLOATAB = TRUE;
	 					break;
	 				}
				}// the end of for loop
				/*Frank v1.1a*/
				
				if(ilNextDSSNforLOATAB == TRUE)
 				{
 					if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
					{
						fprintf(fptrUpdate,"%s",pcgMainDelimiter);
					}
					else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
					{
						fprintf(fptrDaily,"%s",pcgMainDelimiter);
					}
 				}
 				
				/*Frank v1.1a*/
			}
			else //if(strcmp(rlAFTTAB.ORG3,"AUH")!=0)
			{
				if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
				{
					fprintf(fptrUpdate,"%s",pcgMainDelimiter);
				}
				else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
				{
					fprintf(fptrDaily,"%s",pcgMainDelimiter);
				}
			}
//Transfer
			
      memset( &rlAFTTAB, 0, sizeof(rlAFTTAB) );
      
      /*
      if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
      {
      	fprintf( fptrUpdate, "\n" );
      }
      else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
      {
      	fprintf( fptrDaily, "\n" );
      }
      */
	 	}//while loop AFT 
		close_my_cursor( &rlAFTSQL.SqlCursor );
		
		if(igDoUpdateFile == TRUE && igIs8AM==FALSE)
		{
			dbg(DEBUG,"close update");
			fclose( fptrUpdate );
			
			dbg(DEBUG,"copy update");
			fptrUpdate=fopen( pcgUpdateFileName, "r" );
			if( fptrUpdate == NULL )
      {
        dbglog( "Cannot open file for writing <%s> error <%s><%d>", pcgUpdateFileName, errno, strerror(errno) );
        exit( -1 );
      }
      
      sprintf(pcgUpdateFileNameFilePath,"%sUFISWB%s.txt",pcgUpdateFilePath,pcgCurrDateForUpdate);
      fptrUpdateCopy = fopen( pcgUpdateFileNameFilePath, "w" );
      if( fptrUpdateCopy == NULL )
      {
        dbglog( "Cannot open file for writing <%s> error <%s><%d>", pcgUpdateFileNameFilePath, errno, strerror(errno) );
        exit( -1 );
      }
      dbg(DEBUG,"pcgUpdateFileNameFilePath=%s",pcgUpdateFileNameFilePath);
      
      memset(&buff,0,sizeof(buff));
      while((ilRead=fread(buff,sizeof(char),READ_BUFF,fptrUpdate))>0)
			{
       fwrite(buff,sizeof(char),ilRead,fptrUpdateCopy);
      }   
      
      fclose(fptrUpdateCopy);
			fclose(fptrUpdate);	
		}
		else if(igDoDailyFile == TRUE && igIs8AM==TRUE)
		{
			dbg(DEBUG,"close daily");
			fclose( fptrDaily );
			
			dbg(DEBUG,"copy update");
			fptrDaily=fopen( pcgDailyFileName, "r" );
			if( fptrDaily == NULL )
      {
        dbglog( "Cannot open file for writing <%s> error <%s><%d>", pcgDailyFileName, errno, strerror(errno) );
        exit( -1 );
      }
      
     	sprintf(pcgDailyFileNameFilePath,"%sUFISWB%s080000.txt",pcgDailyFilePath,pcgCurrDateForDaily);
     	
     	if(fopen(pcgDailyFileNameFilePath,"r")!=NULL)
     	{
     		memset(&pcgDailyFileNameFilePath,0,sizeof(pcgDailyFileNameFilePath));
     		sprintf(pcgDailyFileNameFilePath,"%sUFISWB%s080001.txt",pcgDailyFilePath,pcgCurrDateForDaily);
     	}
     	
      fptrDailyCopy = fopen( pcgDailyFileNameFilePath, "w" );
      if( fptrDailyCopy == NULL )
      {
        dbglog( "Cannot open file for writing <%s> error <%s><%d>", pcgDailyFileNameFilePath, errno, strerror(errno) );
        exit( -1 );
      }
      dbg(DEBUG,"pcgDailyFileNameFilePath=%s",pcgDailyFileNameFilePath);
      
      while((ilRead=fread(buff,sizeof(char),READ_BUFF,fptrDaily))>0)
			{
       fwrite(buff,sizeof(char),ilRead,fptrDailyCopy);
      }   
      
      fclose(fptrDailyCopy);
			fclose(fptrDaily);	
		}
	}
	dbg(DEBUG,"line<%5d>after",__LINE__);
	
	//Frank@WorkBridge
	
	dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);

	return(RC_SUCCESS);
	
} /* end of HandleData */

/* ******************************************************************** */
/*                                                                      */
/* Function:    GetCurrDate()                                           */
/*                                                                      */
/* Parameters:  pcgCurrDate - receives the current date.                */
/*              pcgDayOfWeek - receives the day of the week number.     */
/*                                                                      */
/* Returns: RC_SUCCESS or RC_FAIL.                                      */
/*                                                                      */
/* Description: Returns pcpCurrDate as "yyyymmddhhmmss" and             */
/*              pcpDayOfWeek where 1=Mon,2=Tues .... 7=Sun.             */
/*                                                                      */
/* ******************************************************************** */
static int GetCurrDateForUpdate( void ) 
{
    int ilRc = RC_SUCCESS;

    time_t _lCurTime;
    struct tm *plCurTime;
    struct tm rlTm;

    _lCurTime = time(0L);
    plCurTime = (struct tm *)localtime(&_lCurTime);
    rlTm = *plCurTime;

    memset(pcgCurrDateForUpdate,0,15);
    memset(pcgDayOfWeek,0,3);

    strftime(pcgCurrDateForUpdate,15,"%""Y%""m%""d%""H%""M%""S",&rlTm);
    strftime(pcgDayOfWeek,2,"%w",&rlTm);
    if(pcgDayOfWeek[0] == '0')
        pcgDayOfWeek[0] = '7';

    dbg(DEBUG,"GetCurrDate() Date <%s> DayOfWeek <%s>",
                    pcgCurrDateForUpdate,pcgDayOfWeek);
    return ilRc;

} /* end GetCurrDate() */

static int GetCurrDateForDaily( void ) 
{
    int ilRc = RC_SUCCESS;

    time_t _lCurTime;
    struct tm *plCurTime;
    struct tm rlTm;

    _lCurTime = time(0L);
    plCurTime = (struct tm *)localtime(&_lCurTime);
    rlTm = *plCurTime;

    memset(pcgCurrDateForDaily,0,15);

    strftime(pcgCurrDateForDaily,15,"%""Y%""m%""d",&rlTm);
   
    dbg(DEBUG,"GetCurrDateToDay() Date <%s>",pcgCurrDateForDaily);
    return ilRc;

} /* end GetCurrDateToDay() */

static char *GetCurrTime(void) 
{

    static char clCurTime[20];
    time_t      _lCurTime;
    struct tm   *plCurTime;
    struct tm   rlTm;

    _lCurTime = time(0L);
    plCurTime = (struct tm *)localtime(&_lCurTime);
    rlTm = *plCurTime;
    memset(clCurTime,0,20);
    strftime(clCurTime,20,"%""d/%""m/%""Y %""H:%""M:%""S",&rlTm);

    return clCurTime;

} /* end GetCurrTime() */

void dbglog(char *fmt, ...)
{
    va_list args;
    struct tm *tmptr;
    char pclTmStr[24];
    time_t tlNow;
    char pclPrint[1024];


    tlNow = time(0);
    tmptr = localtime( (time_t *)&tlNow );
    sprintf( pclTmStr, "%2.2d-%2.2d-%2.2d %2.2d:%2.2d:%2.2d ", 
                       tmptr->tm_mday, tmptr->tm_mon+1, tmptr->tm_year%100,
                       tmptr->tm_hour, tmptr->tm_min, tmptr->tm_sec );

     va_start(args, fmt);
     sprintf( pclPrint, "%s", pclTmStr );
     strcat( pclPrint, fmt );
     strcat( pclPrint, "\n" );
     vprintf( pclPrint, args );
     va_end( args );
}

int TrimSpace( char *pcpInStr )
{
    int ili = 0;
    int ilLen;
    char *pclOutStr = NULL;
    char *pclP;
    char *pclFunc = "TrimSpace";

    ilLen = strlen( pcpInStr );
    if( ilLen <= 0 )
        return;

    pclOutStr = (char *)malloc(ilLen + 10);
    pclP = pcpInStr;

    /* Trim front spaces */
    while( pclP && *pclP == ' ' )
        pclP++;

    while( *pclP )
    {
       pclOutStr[ili++] = *pclP;
       if( *pclP != ' ' )
           pclP++;
       else
       {
           while( *pclP == ' ' )
               pclP++;
       }
    }
    /* Trim back space */
    if( pclOutStr[ili-1] == ' ' )
        ili--;
    pclOutStr[ili] = '\0';
    strcpy( pcpInStr, pclOutStr );
    if( pclOutStr != NULL )
        free( (char *)pclOutStr );
}

int  get_real_item_semicolon(char *dest, char *src, int ItemNbr)
{
	int rc = -1;
	int gt_rc;
	int b_len = 0;
	int trim = TRUE;
	if (ItemNbr < 0){
		trim = FALSE;
		ItemNbr = - ItemNbr;
	} /* end if */
	gt_rc = get_item(ItemNbr, src, dest, b_len, ";", "\0", "\0");
	if (gt_rc == TRUE){
		if (trim == TRUE){
			str_trm_rgt(dest, " ", TRUE);
		} /* end if */
		rc = strlen(dest);
	}/* end if */
	return rc;
}

static void TrimRight(char *pcpBuffer)
{
  char *pclBlank = &pcpBuffer[strlen(pcpBuffer)-1];

  if (strlen(pcpBuffer) == 0)
  {
     strcpy(pcpBuffer, " ");
  }
  else
  {
     while (isspace(*pclBlank) && pclBlank != pcpBuffer)
     {
        *pclBlank = '\0';
        pclBlank--;
     }
  }
} /* End of TrimRight */

int IsALC2_FLNO_FLTN_STOA_STOD_NULL(AFTTAB rpAFTTAB)
{
	char *pclFunc = "IsALC2_FLNO_FLTN_STOA_STOD_NULL";
	if(strlen(rpAFTTAB.ALC2) == 0)
	{
		dbg(DEBUG,"%s ALC2 is NULL",pclFunc);
		return 1;
	}
	
	if(strlen(rpAFTTAB.FLNO) == 0 && strlen(rpAFTTAB.FLTN) == 0)
	{
		dbg(DEBUG,"%s FLNO & FLTN is NULL",pclFunc);
		return 2;
	}
	
	if(strlen(rpAFTTAB.ADID)==0)
	{
		dbg(DEBUG,"%s ADID is NULL",pclFunc);
		return 3;
	}
	else
	{
		if(strncmp(rpAFTTAB.ADID,"A",1)==0)
		{
			if(strlen(rpAFTTAB.STOA)==0 || strncmp(rpAFTTAB.STOA," ",1)==0)
			{
				dbg(DEBUG,"%s For arrvial flight, STOA is NULL",pclFunc);
				return 4;
			}
		}
		else if(strncmp(rpAFTTAB.ADID,"A",1)!=0 || strncmp(rpAFTTAB.STOD," ",1)==0)
		{
			if(strlen(rpAFTTAB.STOD)==0)
			{
				dbg(DEBUG,"%s For departure flight, STOD is NULL",pclFunc);
				return 5;
			}
		}
	}
	
	dbg(DEBUG,"%s Check finish, return 0(successful status)",pclFunc);
	return 0;
}

int IsSTOA_STOD_NULL(AFTTAB rpAFTTAB)
{
	char *pclFunc = "IsSTOA_STOD_NULL";
	if(strlen(rpAFTTAB.ADID)==0)
	{
		dbg(DEBUG,"%s ADID is NULL",pclFunc);
		return RC_FAIL;
	}
	else
	{
		if(strncmp(rpAFTTAB.ADID,"A",1)==0)
		{
			if(strlen(rpAFTTAB.STOA)==0 || strncmp(rpAFTTAB.STOA," ",1)==0)
			{
				dbg(DEBUG,"%s For arrvial flight, STOA is NULL",pclFunc);
				return RC_FAIL;
			}
		}
		else if(strncmp(rpAFTTAB.ADID,"A",1)!=0 || strncmp(rpAFTTAB.STOD," ",1)==0)
		{
			if(strlen(rpAFTTAB.STOD)==0)
			{
				dbg(DEBUG,"%s For departure flight, STOD is NULL",pclFunc);
				return RC_FAIL;
			}
		}
	}
	
	dbg(DEBUG,"%s Check finish,successful",pclFunc);
	return RC_SUCCESS;
}

int IsALC2_FLNO_FLTN_NULL(AFTTAB rpAFTTAB)
{
	char *pclFunc = "IsALC2_FLNO_FLTN_NULL";
	if(strlen(rpAFTTAB.ALC2) == 0)
	{
		dbg(DEBUG,"%s ALC2 is NULL",pclFunc);
		return RC_FAIL;
	}
	
	if(strlen(rpAFTTAB.FLNO) == 0 && strlen(rpAFTTAB.FLTN) == 0)
	{
		dbg(DEBUG,"%s FLNO & FLTN is NULL",pclFunc);
		return RC_FAIL;
	}
	
	dbg(DEBUG,"%s Check finish, return 0(successful status)",pclFunc);
	return RC_SUCCESS;
}

int IsAlc2FlnoFltnNullForAdHocFlight(AFTTAB rpAftrec,char *pcpALC2,char *pcpFLTN, char *pcpFLNS)//char *pcpFLNO,char *pcpFLTN)
{	
	char *pclOrg;
	char *pclFunc = "IsAlc2FlnoFltnNullForAdHocFlighta";
	int ilLengthOfCsgn = 0;
	
	//Frank 20120803 v1.2
        //rlAftrec.ALC2,rlAftrec.ALC3,rlAftrec.FLNO,rlAftrec.FLTN have been TrimRight() in sendAftData
	dbg(DEBUG,"%s: Checking ALC2,ALC3,FLNO and FLTN null or not",pclFunc);
	
	//test
	/*
	memset(rpAftrec.ALC2,0,sizeof(rpAftrec.ALC2));
	memset(rpAftrec.FLTN,0,sizeof(rpAftrec.FLTN));
	memset(rpAftrec.FLNS,0,sizeof(rpAftrec.FLNS));
	memset(rpAftrec.CSGN,0,sizeof(rpAftrec.CSGN));
	strcpy(rpAftrec.CSGN,"678");
	*/
	//test
	
	if( strlen(rpAftrec.ALC2) == 0 )//ALC2 is null
	{	
		dbg(DEBUG,"%s: ALC2 is null",pclFunc);
		//dbg(DEBUG,"%s: ALC2 is null,ALC3 is not null",pclFunc);
		//modify the ALC2 using pcgCodeForALC2
		dbg(DEBUG,"%s The original rpAftrec.ALC2 is <%s>,pcgCodeForALC2<%s>",pclFunc,rpAftrec.ALC2,pcgCodeForALC2);
		memset(pcpALC2,0,sizeof(pcpALC2));
		strcpy(pcpALC2,pcgCodeForALC2);
		dbg(DEBUG,"%s The modified pcpALC2 is <%s>",pclFunc,pcpALC2);
		/*
		if( strlen(rpAftrec.ALC3) == 0 )//ALC3 is null
		{
        		dbg(DEBUG,"%s:Both of ALC2 and ALC3 are null,which is impossible. If we come here, then something worng with fips",pclFunc);
			return RC_FAIL;
		}
		else//ALC3 is not null
		{
		*/	
			if( strlen(rpAftrec.FLTN) == 0 && strlen(rpAftrec.FLNS) ==0 )	
			{	
				/*
				//dbg(DEBUG,"%s: ALC2 is null,ALC3 is not null",pclFunc);
				//modify the ALC2 using pcgCodeForALC2
				dbg(DEBUG,"%s The original rpAftrec.ALC2 is <%s>,pcgCodeForALC2<%s>",pclFunc,rpAftrec.ALC2,pcgCodeForALC2);		
				memset(pcpALC2,0,sizeof(pcpALC2));
				strcpy(pcpALC2,pcgCodeForALC2);
				dbg(DEBUG,"%s The modified pcpALC2 is <%s>",pclFunc,pcpALC2);
				*/

				
				dbg(DEBUG,"%s The original rpAftrec.FLTN is <%s>,rpAftrec.FLNS<%s>",pclFunc,rpAftrec.FLTN,rpAftrec.FLNS);
				memset(pcpFLTN,0,sizeof(pcpFLTN));
				memset(pcpFLNS,0,sizeof(pcpFLNS));
				
				ilLengthOfCsgn = strlen(rpAftrec.CSGN);
				if( ilLengthOfCsgn >= igLastPartOfCSGN)
				{
					pclOrg = rpAftrec.CSGN + ilLengthOfCsgn - igLastPartOfCSGN;
					strcpy(pcpFLTN,pclOrg);
					dbg(DEBUG,"%s The modified rpAftrec.FLTN<%s>,rpAftrec.FLNS<%s> which is last <%d> of rpAftrec.CSGN<%s>",pclFunc,pcpFLTN,pcpFLNS,igLastPartOfCSGN,rpAftrec.CSGN);
				}
				else
				{
					strcpy(pcpFLTN,rpAftrec.CSGN);
					dbg(DEBUG,"%s The ilLengthOfCsgn is <%d>pcpFLTN<%s>",pclFunc,ilLengthOfCsgn,pcpFLTN);
				}			
				
				/*
				strcpy(pcpFLTN,rpAftrec.FLTN);
				strcpy(pcpFLNS,rpAftrec.FLNS);
				*/
				//return RC_SUCCESS;
			}
			else
			{
				//dbg(DEBUG,"%s: FLNO are FLTN are not both null",pclFunc);
				//return RC_FAIL;
				strcpy(pcpFLTN,rpAftrec.FLTN);
				strcpy(pcpFLNS,rpAftrec.FLNS);
				//return RC_SUCCESS;
			}
			return RC_SUCCESS;
		//}
		
	}
	else
	{
		dbg(DEBUG,"%s: ALC2 is not null",pclFunc);
		return RC_FAIL;
	}
}
