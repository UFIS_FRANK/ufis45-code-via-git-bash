#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/AUH/AUH_Server/Base/Server/Interface/euratc.c 1.17 2012/06/05 12:28:14SIN ble Exp  $";
#endif /* _DEF_mks_version */
/* ********************************************************************** */
/*  Eurocat - ATC system in AUH                                           */
/*                                                                        */
/*  Program       : EURATC                                                */
/*                  EUROCAT ATC SYSTEM                                    */
/*  Revision date :                                                       */
/*                                                                        */
/*  Author        :  MEI                                                  */
/*                                                                        */
/*  HISTORY       :                                                       */
/*  MEI 2011-11-23: Code Creation                                         */
/*  MEI 2012-02-29: Change ETAE field update to ETAA (v1.3)               */
/*  MEI 2012-03-02: ATC send multiple message in high speed, try to reduce traffic to FLIGHT process (v1.4) */
/*  MEI 2012-03-05: Format CSGN, and fill up 3-letter APC (v1.5) */
/*  MEI 2012-03-07: Format CSGN to FLNO. Change flight searching method(v1.6) */
/*  MEI 2012-03-09: FindFlight stepping is wrong. End up got the wrong daily flight(v1.7) */
/*  MEI 2012-03-12: Fine tune the time base to current time (v1.8) */
/*  MEI 2012-03-15: When only ETA is received, skip flight found which has LANDED (v1.9) */
/*  MEI 2012-03-19: Do not fill FLNO when ALC is not in basic data (v1.10) */
/*  MEI 2012-03-21: ATC send wrong DLFL to outstanding flight, ignore DLFL if flight has not land(v1.11) */
/*  MEI 2012-03-27: Change call sign mapping to base on ADID, include ALC3 when IFR (v1.12) */
/*  MEI 2012-03-28: Store bay sending as another record (v1.13) */
/*  MEI 2012-03-29: When creating circular flight, fill in the arrival time info (v1.14) */
/*  MEI 2012-04-03: Throw away early ETA and stepping still got bug (v1.15) */
/*  MEI 2012-04-04: Ignore LAND and AIRB timing if it is X hours from current time.  */
/*                  Added 3 new columns in ATCTAB FLDS,DATA,RAWD           */
/*                  Not good SendBay logic, change                 (v1.16) */
/*  MEI 2012-06-05: Ignore all update (unless it contain LAND/AIRB time) if current flight has been landed or airborne */
/*                  Implement Flight finalized for dep flights as well (v1.17) */
/* ********************************************************************** */
/*                                                                            */
/* source-code-control-system version string                                  */

static char sccs_version[] ="@(#) UFIS45 (c) ABB AAT/I skeleton.c 45.1.0 / 11.12.2002 HEB";

/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */


#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "fditools.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "syslib.h"
#include "db_if.h"
#include "urno_fn.inc"
#include <time.h>
#include <cedatime.h>


#ifdef _HPUX_SOURCE
    extern int daylight;
    extern long timezone;
    extern char *tzname[2];
#else
    extern int _daylight;
    extern long _timezone;
    extern char *_tzname[2];
#endif

#define MSGTYPE_DELETE  1
#define MSGTYPE_UPDATE  2
#define MSGTYPE_INSERT  3

int debug_level = 0;
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static EVENT *prgOutEvent  = NULL;

static char pcgFtypSrchStr[160] = "\0";
static char cgProcessName[20] = "\0";
static char  cgConfigFile[512] = "\0";
static char  cgHopo[8] = "\0";                         /* default home airport    */
static char  cgTabEnd[8] ="\0";                       /* default table extension */
static char pcgTables[30][20];
static long lgEvtCnt = 0;

typedef struct 
{
    int igArrStepLeft;
    int igArrStepRight;
    int igArrMaxLeft;
    int igArrMaxRight;
    int igDepStepLeft;
    int igDepStepRight;
    int igDepMaxLeft;
    int igDepMaxRight;
    int igIgnoreETATimeRange;
    int igIgnoreLandTimeRange;
    int igIgnoreAirbTimeRange;
    char bgProcessInsert;
    char bgProcessUpdate;
    char bgProcessDelete;
    char bgMapCallSign;
    char bgSendBay;
    char bgBaySendWithoutATC;
    char pcgSrchExcFtyp[64];
    char pcgSrchIncFtyp[64];
} INFO_NODE;

typedef struct 
{
    char pcgFtyp[8];
    char pcgTtyp[8];
    char pcgStyp[8];
} INSFLT_NODE;

typedef struct 
{
    char bgUpdCallSign;
    char bgUpdEstOfbl;
    char bgUpdEstArr;
    char bgUpdLandAirb;
    char bgUpdAct5;
} UPDFLT_NODE;

typedef struct 
{
    char bgDeleteFlt;
    char bgMarkArrFltClose;
    char bgMarkDepFltClose;
    char pclSetFtyp[8];
} DELFLT_NODE;

typedef struct 
{
    char pcgMsgID[8];
    char pcgACID[12];
    char pcgORG3[8];
    char pcgORG4[8];
    char pcgEOBT[8];
    char pcgCSGN[12];
    char cgADID;
} ATC_HDR;

typedef struct 
{
    char pcgDES3[8];
    char pcgDES4[8];
    char pcgSSRC[8];
    char pcgACT5[8];
    char pcgIAIR[8];
    char pcgILND[8];
    char pcgRWYA[8];
    char pcgRWYD[8];
    char pcgIBAY[8];
    char pcgIETA[8];
} ATC_MSG;

typedef struct 
{
    char pcgURNO[12];
    char pcgUAFT[12];
    char pcgFLNO[12];
    char pcgFLDA[12];
    char pcgREGN[12];
    char pcgONOF[16];
    char pcgTIFT[16];
    char pcgPKST[8];
    char pcgCDAT[16];
    char pcgLSTU[16];
    char pcgUSEC[32];
    char pcgLAND[16];
    char pcgAIRB[16];
    char pcgALC2[4];
    char pcgALC3[4];
} ATC_REC;

/* Config */
INFO_NODE rgINFO;
INSFLT_NODE rgIFLT;
UPDFLT_NODE rgUFLT;
DELFLT_NODE rgDFLT;

static int igNumDaysInAtc;
static int igFlight;
static int igWmqAtc;
static char pcgHopo4[8];

/* Data */
ATC_HDR rgHDR;
ATC_MSG rgMSG;
ATC_REC rgATC;

/******************************************************************************/
/* Function prototypes                                                        */
/******************************************************************************/
static int  Init_Process();
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer);
static int  Reset(void);                       /* Reset program          */
static void Terminate(int ipSleep);            /* Terminate program      */
static void HandleSignal(int);                 /* Handles signals        */
static void HandleErr(int);                    /* Handles general errors */
static void HandleQueErr(int);                 /* Handles queuing errors */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/
static int HandleData(EVENT *prpEvent);       /* Handles event data     */
static int GetCommand(char *pcpCommand, int *pipCmd);

/*static int RunSQL( char *pcpSelection, char *pcpData );*/
static int GetHeader( char *pcpData );
static int GetMsgBody( char *pcpData );
static int MapCallSign( char *pcpSource, char *pcpMapped );
static int FindFlight( char *pcpMsgType );
static int FindFlightInATC();
static int InsertAtcRecord( char *pcpCmd, char *pcpAftFields, char *pcpAftData, char *pcpRawData );
static int UpdateAtcRecord( char *pcpCmd, char *pcpAftFields, char *pcpAftData );
static int ProcessFltInsert();
static int ProcessFltUpdate();
static int ProcessFltDelete();
static int CleanDB();
static int SendBayInfo( char *pcpFields, char *pcpData, char *pcpUaft );      
static int ExtractAFTData( char *pcpFields, char *pcpData, char *pcpFieldReqd, char *pcpResult );
static int GuessTheDate( char *pcpBaseDateTime, char *pcpMsgTime, char *pcpResultDate, char *pcpFormat );
static int TrimSpace( char *pcpInStr );
static int UTCTimeDiff ( char *pcpTimeStr1, char *pcpTimeStr2, int *pipMinDiff );
static int GetTimeValue ( char *pcpTimeStr, time_t *ptpTimeValue );
static int FormatCsgnFlno( char *pcpCSGN, char *pcpFLNO);


/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
    int ilRc = RC_SUCCESS;          /* Return code          */
    int ilCnt = 0;
    int ilOldDebugLevel = 0;


    INITIALIZE;         /* General initialization   */

    debug_level = TRACE;

    strcpy(cgProcessName,argv[0]);

    dbg(TRACE,"MAIN: version <%s>",sccs_version);

    /* Attach to the MIKE queues */
    do
    {
        ilRc = init_que();
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        }/* end of if */
    }while((ilCnt < 10) && (ilRc != RC_SUCCESS));

    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
        sleep(60);
        exit(1);
    }else{
        dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }/* end of if */

    do
    {
        ilRc = init_db();
        if (ilRc != RC_SUCCESS)
        {
            check_ret(ilRc);
            dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        } /* end of if */
    } while((ilCnt < 10) && (ilRc != RC_SUCCESS));

    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
        sleep(60);
        exit(2);
    }else{
        dbg(TRACE,"MAIN: init_db() OK!");
    } /* end of if */

    /* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */

    sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
    dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
    ilRc = TransferFile(cgConfigFile);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */

    sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
    dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
    ilRc = TransferFile(cgConfigFile);
    if(ilRc != RC_SUCCESS)
    { 
        dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */

    ilRc = SendRemoteShutdown(mod_id);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    } /* end of if */

    if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
        dbg(DEBUG,"MAIN: waiting for status switch ...");
        HandleQueues();
    }/* end of if */

    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
        dbg(TRACE,"MAIN: initializing ...");
            ilRc = Init_Process();
            if(ilRc != RC_SUCCESS)
            {
                dbg(TRACE,"Init_Process: init failed!");
            } /* end of if */

    } else {
        Terminate(30);
    }/* end of if */



    dbg(TRACE,"=====================");
    dbg(TRACE,"MAIN: initializing OK");
    dbg(TRACE,"=====================");
    

    for(;;)
    {
        ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* Acknowledge the item */
        ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
        if( ilRc != RC_SUCCESS ) 
        {
            /* handle que_ack error */
            HandleQueErr(ilRc);
        } /* fi */

        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */

        prgEvent = (EVENT *) prgItem->text;
                
        if( ilRc == RC_SUCCESS )
        {          
            lgEvtCnt++;

            switch( prgEvent->command )
            {
                case    HSB_STANDBY :
                        ctrl_sta = prgEvent->command;
                        HandleQueues();
                        break;  
                case    HSB_COMING_UP   :
                        ctrl_sta = prgEvent->command;
                        HandleQueues();
                        break;  
                case    HSB_ACTIVE  :
                        ctrl_sta = prgEvent->command;
                        break;  
                case    HSB_ACT_TO_SBY  :
                        ctrl_sta = prgEvent->command;
                        /* CloseConnection(); */
                        HandleQueues();
                        break;  
                case    HSB_DOWN    :
                        /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                        ctrl_sta = prgEvent->command;
                        Terminate(1);
                        break;  
                case    HSB_STANDALONE  :
                        ctrl_sta = prgEvent->command;
                        ResetDBCounter();
                        break;  
                case    REMOTE_DB :
                        /* ctrl_sta is checked inside */
                        HandleRemoteDB(prgEvent);
                        break;
                case    SHUTDOWN    :
                        /* process shutdown - maybe from uutil */
                        Terminate(1);
                        break;
                    
                case    RESET       :
                        ilRc = Reset();
                        break;
                    
                case    EVENT_DATA  :
                        if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
                        {
                            ilRc = HandleData(prgEvent);
                            /*if(ilRc != RC_SUCCESS)
                            {
                                HandleErr(ilRc);
                            }*//* end of if */
                        }
                        else
                        {
                            dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
                            DebugPrintItem(TRACE,prgItem);
                            DebugPrintEvent(TRACE,prgEvent);
                        }/* end of if */
                        break;
                
                case    TRACE_ON :
                        dbg_handle_debug(prgEvent->command);
                        break;
                case    TRACE_OFF :
                        dbg_handle_debug(prgEvent->command);
                        break;
            } /* end switch */
        } 
        else 
        {
            /* Handle queuing errors */
            HandleQueErr(ilRc);
        } /* end else */
    } /* end for */
    
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer)
{
    int ilRc = RC_SUCCESS;

    char clSection[124] = "\0";
    char clKeyword[124] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRc = iGetConfigEntry(cgConfigFile,clSection,clKeyword,
               CFG_STRING,pcpCfgBuffer);
    if(ilRc != RC_SUCCESS)
    {
    dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
    } 
    else
    {
    dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s",
        clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
    }/* end of if */
    return ilRc;
}


static int Init_Process()
{
    int ilRc = RC_SUCCESS;         /* Return code */
    int ilOldDebugLevel = 0;
    int ilCount;
    char clSection[64] = "\0";
    char clKeyword[64] = "\0";
    char clBreak[24] = "\0";
    char pclTmpStr[128] = "\0";
    char pclOneConfig[128] = "\0";
    char *pclFunc = "Init_Process";


    if(ilRc == RC_SUCCESS)
    {
        /* read HomeAirPort from SGS.TAB */
        ilRc = tool_search_exco_data("SYS", "HOMEAP", cgHopo);
        if (ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"<Init_Process> EXTAB,SYS,HOMEAP not found in SGS.TAB");
            Terminate(30);
        } else {
            dbg(TRACE,"<Init_Process> home airport <%s>",cgHopo);
        }
    }

    
    if(ilRc == RC_SUCCESS)
    {

        ilRc = tool_search_exco_data("ALL","TABEND", cgTabEnd);
        if (ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"<Init_Process> EXTAB,ALL,TABEND not found in SGS.TAB");
            Terminate(30);
        } else {
            dbg(TRACE,"<Init_Process> table extension <%s>",cgTabEnd);
       }
    }

    debug_level = DEBUG;
    ilCount = 0;
    syslibSearchDbData("APTTAB","APC3",cgHopo,"APC4",pcgHopo4,&ilCount,"\n") ;
    dbg( TRACE, "APC3 <%s> APC4 <%s>", cgHopo, pcgHopo4 );


    /** Reading ATC_INFO **/
    dbg( TRACE, "......................................" );
    dbg( TRACE, "......... ATC_INFO SECTION ..........." );
    dbg( TRACE, "......................................" );
    memset( &rgINFO, 0, sizeof(rgINFO) );
    ilRc = ReadConfigEntry( "ATC_INFO", "PROCESS_INSERT", pclOneConfig ); 
    rgINFO.bgProcessInsert = FALSE;
    if( ilRc == RC_SUCCESS && pclOneConfig[0] == 'Y' )
        rgINFO.bgProcessInsert = TRUE;
    dbg( TRACE, "ilRc <%d> PROCESS_INSERT = <%s>", 
                ilRc, rgINFO.bgProcessInsert == TRUE ? "TRUE" : "FALSE"  );

    ilRc = ReadConfigEntry( "ATC_INFO", "PROCESS_UPDATE", pclOneConfig ); 
    rgINFO.bgProcessUpdate = FALSE;
    if( ilRc == RC_SUCCESS && pclOneConfig[0] == 'Y' )
        rgINFO.bgProcessUpdate = TRUE;
    dbg( TRACE, "ilRc <%d> PROCESS_UPDATE = <%s>", 
                ilRc, rgINFO.bgProcessUpdate == TRUE ? "TRUE" : "FALSE"  );

    ilRc = ReadConfigEntry( "ATC_INFO", "PROCESS_DELETE", pclOneConfig ); 
    rgINFO.bgProcessDelete = FALSE;
    if( ilRc == RC_SUCCESS && pclOneConfig[0] == 'Y' )
        rgINFO.bgProcessDelete = TRUE;
    dbg( TRACE, "ilRc <%d> PROCESS_DELETE = <%s>", 
                ilRc, rgINFO.bgProcessDelete == TRUE ? "TRUE" : "FALSE"  );

    ilRc = ReadConfigEntry( "ATC_INFO", "MAP_CALLSIGN", pclOneConfig ); 
    rgINFO.bgMapCallSign = FALSE;
    if( ilRc == RC_SUCCESS && pclOneConfig[0] == 'Y' )
        rgINFO.bgMapCallSign = TRUE;
    dbg( TRACE, "ilRc <%d> MAP_CALLSIGN = <%s>", 
                ilRc, rgINFO.bgMapCallSign == TRUE ? "TRUE" : "FALSE"  );

    ilRc = ReadConfigEntry( "ATC_INFO", "SEND_BAY", pclOneConfig ); 
    rgINFO.bgSendBay = FALSE;
    if( ilRc == RC_SUCCESS && pclOneConfig[0] == 'Y' )
        rgINFO.bgSendBay = TRUE;
    dbg( TRACE, "ilRc <%d> SEND_BAY = <%s>", 
                ilRc, rgINFO.bgSendBay == TRUE ? "TRUE" : "FALSE"  );

    ilRc = ReadConfigEntry( "ATC_INFO", "BAY_SEND_WITHOUT_ATC", pclOneConfig ); 
    rgINFO.bgBaySendWithoutATC = FALSE;
    if( ilRc == RC_SUCCESS && pclOneConfig[0] == 'Y' )
        rgINFO.bgBaySendWithoutATC = TRUE;
    dbg( TRACE, "ilRc <%d> BAY_SEND_WITHOUT_ATC = <%s>", 
                ilRc, rgINFO.bgBaySendWithoutATC == TRUE ? "TRUE" : "FALSE"  );


    pclTmpStr[0] = '\0';
    rgINFO.pcgSrchExcFtyp[0] = '\0';
    rgINFO.pcgSrchIncFtyp[0] = '\0';
    ReadConfigEntry( "ATC_INFO", "SEARCH_EXCLUDE_FTYP", pclTmpStr ); 
    if( strlen(pclTmpStr) > 0 )
        sprintf( rgINFO.pcgSrchExcFtyp, " FTYP NOT IN (%s)", pclTmpStr );

    pclTmpStr[0] = '\0';
    ReadConfigEntry( "ATC_INFO", "SEARCH_INCLUDE_FTYP", pclTmpStr ); 
    if( strlen(pclTmpStr) > 0 )
        sprintf( rgINFO.pcgSrchIncFtyp, " FTYP IN (%s)", pclTmpStr );
    if( strlen(rgINFO.pcgSrchExcFtyp) > 0 && strlen(rgINFO.pcgSrchIncFtyp) > 0 ) 
        sprintf( pcgFtypSrchStr, " AND %s AND %s AND", rgINFO.pcgSrchExcFtyp, rgINFO.pcgSrchIncFtyp );
    else if( strlen(rgINFO.pcgSrchExcFtyp) > 0 || strlen(rgINFO.pcgSrchIncFtyp) > 0 ) 
        sprintf( pcgFtypSrchStr, " AND %s AND ", (strlen(rgINFO.pcgSrchExcFtyp) > 0) ? rgINFO.pcgSrchExcFtyp : rgINFO.pcgSrchIncFtyp );
    else
        strcpy( pcgFtypSrchStr, " AND " );
    dbg( TRACE, "ilRc <%d> FTYP SEARCH STRING = <%s>", ilRc, pcgFtypSrchStr );



    ilRc = ReadConfigEntry( "ATC_INFO", "ARR_SEARCH_HOURS_RULE", pclOneConfig ); 
    rgINFO.igArrStepLeft = 12;
    rgINFO.igArrStepRight = 12;
    rgINFO.igArrMaxLeft = -12;
    rgINFO.igArrMaxRight = 12;
    if( ilRc == RC_SUCCESS )
    {
        get_real_item( pclTmpStr, pclOneConfig, 1 );          
        rgINFO.igArrStepLeft = atoi(pclTmpStr);
        get_real_item( pclTmpStr, pclOneConfig, 2 );          
        rgINFO.igArrStepRight = atoi(pclTmpStr);
        get_real_item( pclTmpStr, pclOneConfig, 3 );          
        rgINFO.igArrMaxLeft = atoi(pclTmpStr);
        get_real_item( pclTmpStr, pclOneConfig, 4 );          
        rgINFO.igArrMaxRight = atoi(pclTmpStr);
    }
    if( rgINFO.igArrStepLeft == 0 )
        rgINFO.igArrStepLeft = 12;
    if( rgINFO.igArrStepRight == 0 )
        rgINFO.igArrStepRight = 12;
    dbg( TRACE, "ilRc <%d> ARR_SEARCH_HOURS_RULE = <%d,%d,%d,%d> in hours ", 
                ilRc, rgINFO.igArrStepLeft, rgINFO.igArrStepRight,
                rgINFO.igArrMaxLeft, rgINFO.igArrMaxRight );

    ilRc = ReadConfigEntry( "ATC_INFO", "DEP_SEARCH_HOURS_RULE", pclOneConfig ); 
    rgINFO.igDepStepLeft = 1;
    rgINFO.igDepStepRight = 1;
    rgINFO.igDepMaxLeft = -12;
    rgINFO.igDepMaxRight = 12;
    if( ilRc == RC_SUCCESS )
    {
        get_real_item( pclTmpStr, pclOneConfig, 1 );          
        rgINFO.igDepStepLeft = atoi(pclTmpStr);
        get_real_item( pclTmpStr, pclOneConfig, 2 );          
        rgINFO.igDepStepRight = atoi(pclTmpStr);
        get_real_item( pclTmpStr, pclOneConfig, 3 );          
        rgINFO.igDepMaxLeft = atoi(pclTmpStr);
        get_real_item( pclTmpStr, pclOneConfig, 4 );          
        rgINFO.igDepMaxRight = atoi(pclTmpStr);
    }
    if( rgINFO.igDepStepLeft == 0 )
        rgINFO.igDepStepLeft = 1;
    if( rgINFO.igDepStepRight == 0 )
        rgINFO.igDepStepRight = 1;
    dbg( TRACE, "ilRc <%d> DEP_SEARCH_HOURS_RULE = <%d,%d,%d,%d> in hours ", 
                ilRc, rgINFO.igDepStepLeft, rgINFO.igDepStepRight,
                rgINFO.igDepMaxLeft, rgINFO.igDepMaxRight );

    rgINFO.igIgnoreETATimeRange = -1;
    ilRc = ReadConfigEntry( "ATC_INFO", "IGNORE_ETA_TIME_RANGE", pclOneConfig ); 
    if( ilRc == RC_SUCCESS && atoi(pclOneConfig) > 0 )
        rgINFO.igIgnoreETATimeRange = atoi(pclOneConfig);
    dbg( TRACE, "ilRc <%d> IGNORE_ETA_TIME_RANGE = <%d> hours <%d> minutes", 
                ilRc, rgINFO.igIgnoreETATimeRange, rgINFO.igIgnoreETATimeRange*60 );
    rgINFO.igIgnoreETATimeRange *= 60;

    rgINFO.igIgnoreLandTimeRange = -1;
    ilRc = ReadConfigEntry( "ATC_INFO", "IGNORE_LAND_TIME_RANGE", pclOneConfig ); 
    if( ilRc == RC_SUCCESS && atoi(pclOneConfig) > 0 )
        rgINFO.igIgnoreLandTimeRange = atoi(pclOneConfig);
    dbg( TRACE, "ilRc <%d> IGNORE_LAND_TIME_RANGE = <%d> hours <%d> minutes", 
                ilRc, rgINFO.igIgnoreLandTimeRange, rgINFO.igIgnoreLandTimeRange*60 );
    rgINFO.igIgnoreLandTimeRange *= 60;

    rgINFO.igIgnoreAirbTimeRange = -1;
    ilRc = ReadConfigEntry( "ATC_INFO", "IGNORE_AIRB_TIME_RANGE", pclOneConfig ); 
    if( ilRc == RC_SUCCESS && atoi(pclOneConfig) > 0 )
        rgINFO.igIgnoreAirbTimeRange = atoi(pclOneConfig);
    dbg( TRACE, "ilRc <%d> IGNORE_AIRB_TIME_RANGE = <%d> hours <%d> minutes", 
                ilRc, rgINFO.igIgnoreAirbTimeRange, rgINFO.igIgnoreAirbTimeRange*60 );
    rgINFO.igIgnoreAirbTimeRange *= 60;


    /** Reading INSERT_FLIGHT_INFO **/

    if( rgINFO.bgProcessInsert == TRUE )
    {
        dbg( TRACE, "................................................" );
        dbg( TRACE, "......... INSERT_FLIGHT_INFO SECTION ..........." );
        dbg( TRACE, "................................................" );
        memset( &rgIFLT, 0, sizeof(rgIFLT) );
 
        ilRc = ReadConfigEntry( "INSERT_FLIGHT", "INSERT_WITH_FTYP", pclOneConfig ); 
        if( ilRc == RC_SUCCESS )
            strcpy( rgIFLT.pcgFtyp, pclOneConfig );
        if( strlen(rgIFLT.pcgFtyp) <= 0 )
            strcpy( rgIFLT.pcgFtyp, " " );
        dbg( TRACE, "ilRc <%d> INSERT_WITH_FTYP = <%s>", ilRc, rgIFLT.pcgFtyp  );

        ilRc = ReadConfigEntry( "INSERT_FLIGHT", "INSERT_WITH_TTYP", pclOneConfig ); 
        if( ilRc == RC_SUCCESS )
            strcpy( rgIFLT.pcgTtyp, pclOneConfig );
        if( strlen(rgIFLT.pcgTtyp) <= 0 )
            strcpy( rgIFLT.pcgTtyp, " " );
        dbg( TRACE, "ilRc <%d> INSERT_WITH_TTYP = <%s>", ilRc, rgIFLT.pcgTtyp  );

        ilRc = ReadConfigEntry( "INSERT_FLIGHT", "INSERT_WITH_STYP", pclOneConfig ); 
        if( ilRc == RC_SUCCESS )
            strcpy( rgIFLT.pcgStyp, pclOneConfig );
        if( strlen(rgIFLT.pcgStyp) <= 0 )
            strcpy( rgIFLT.pcgStyp, " " );
        dbg( TRACE, "ilRc <%d> INSERT_WITH_STYP = <%s>", ilRc, rgIFLT.pcgStyp  );
    }

    if( rgINFO.bgProcessUpdate == TRUE )
    {
        dbg( TRACE, "................................................" );
        dbg( TRACE, "......... UPDATE_FLIGHT_INFO SECTION ..........." );
        dbg( TRACE, "................................................" );
        memset( &rgUFLT, 0, sizeof(rgUFLT) );
 
        ilRc = ReadConfigEntry( "UPDATE_FLIGHT", "UPDATE_CALLSIGN", pclOneConfig ); 
        rgUFLT.bgUpdCallSign = FALSE;
        if( ilRc == RC_SUCCESS && pclOneConfig[0] == 'Y' )
            rgUFLT.bgUpdCallSign = TRUE;
        dbg( TRACE, "ilRc <%d> UPDATE_CALLSIGN = <%s>", 
                    ilRc, rgUFLT.bgUpdCallSign == TRUE ? "TRUE" : "FALSE" );

        ilRc = ReadConfigEntry( "UPDATE_FLIGHT", "UPDATE_ESTIMATED_OFBL", pclOneConfig ); 
        rgUFLT.bgUpdEstOfbl = FALSE;
        if( ilRc == RC_SUCCESS && pclOneConfig[0] == 'Y' )
            rgUFLT.bgUpdEstOfbl = TRUE;
        dbg( TRACE, "ilRc <%d> UPDATE_ESTIMATED_OFBL = <%s>", 
                    ilRc, rgUFLT.bgUpdEstOfbl == TRUE ? "TRUE" : "FALSE" );

        ilRc = ReadConfigEntry( "UPDATE_FLIGHT", "UPDATE_ETOA", pclOneConfig ); 
        rgUFLT.bgUpdEstArr = FALSE;
        if( ilRc == RC_SUCCESS && pclOneConfig[0] == 'Y' )
            rgUFLT.bgUpdEstArr = TRUE;
        dbg( TRACE, "ilRc <%d> UPDATE_ETOA = <%s>", 
                    ilRc, rgUFLT.bgUpdEstArr == TRUE ? "TRUE" : "FALSE" );

        ilRc = ReadConfigEntry( "UPDATE_FLIGHT", "UPDATE_LAND_AIRB", pclOneConfig ); 
        rgUFLT.bgUpdLandAirb = FALSE;
        if( ilRc == RC_SUCCESS && pclOneConfig[0] == 'Y' )
            rgUFLT.bgUpdLandAirb = TRUE;
        dbg( TRACE, "ilRc <%d> UPDATE_LAND_AIRB = <%s>", 
                    ilRc, rgUFLT.bgUpdLandAirb == TRUE ? "TRUE" : "FALSE" );

        ilRc = ReadConfigEntry( "UPDATE_FLIGHT", "UPDATE_ACT5", pclOneConfig ); 
        rgUFLT.bgUpdAct5 = FALSE;
        if( ilRc == RC_SUCCESS && pclOneConfig[0] == 'Y' )
            rgUFLT.bgUpdAct5 = TRUE;
        dbg( TRACE, "ilRc <%d> UPDATE_ACT5 = <%s>", 
                    ilRc, rgUFLT.bgUpdAct5 == TRUE ? "TRUE" : "FALSE" );

    }

    if( rgINFO.bgProcessDelete == TRUE )
    {
        dbg( TRACE, "................................................" );
        dbg( TRACE, "......... DELETE_FLIGHT_INFO SECTION ..........." );
        dbg( TRACE, "................................................" );
        memset( &rgUFLT, 0, sizeof(rgUFLT) );
 
        ilRc = ReadConfigEntry( "DELETE_FLIGHT", "DELETE_FLIGHT", pclOneConfig ); 
        rgDFLT.bgDeleteFlt = FALSE;
        if( ilRc == RC_SUCCESS && pclOneConfig[0] == 'Y' )
            rgDFLT.bgDeleteFlt = TRUE;
        dbg( TRACE, "ilRc <%d> DELETE_FLIGHT = <%s>", 
                    ilRc, rgDFLT.bgDeleteFlt == TRUE ? "TRUE" : "FALSE" );

        if( rgDFLT.bgDeleteFlt == FALSE )
        {
            strcpy( rgDFLT.pclSetFtyp, " " );
            ilRc = ReadConfigEntry( "DELETE_FLIGHT", "NOT_DELETE_BUT_SET_FTYP", pclOneConfig ); 
            if( ilRc == RC_SUCCESS )
                strcpy( rgDFLT.pclSetFtyp, pclOneConfig );
            dbg( TRACE, "ilRc <%d> NOT_DELETE_BUT_SET_FTYP = <%s>", ilRc, rgDFLT.pclSetFtyp  );
        }
    }
    else
    {
         rgDFLT.bgMarkArrFltClose = FALSE;
         ilRc = ReadConfigEntry( "DELETE_FLIGHT", "MARK_ARR_FLIGHT_CLOSE", pclOneConfig ); 
         if( ilRc == RC_SUCCESS && pclOneConfig[0] == 'Y' )
            rgDFLT.bgMarkArrFltClose = TRUE;
         dbg( TRACE, "ilRc <%d> MARK_ARR_FLIGHT_CLOSE = <%s>", 
                     ilRc, rgDFLT.bgMarkArrFltClose == TRUE ? "TRUE" : "FALSE" );

         rgDFLT.bgMarkDepFltClose = FALSE;
         ilRc = ReadConfigEntry( "DELETE_FLIGHT", "MARK_DEP_FLIGHT_CLOSE", pclOneConfig ); 
         if( ilRc == RC_SUCCESS && pclOneConfig[0] == 'Y' )
            rgDFLT.bgMarkDepFltClose = TRUE;
         dbg( TRACE, "ilRc <%d> MARK_DEP_FLIGHT_CLOSE = <%s>", 
                     ilRc, rgDFLT.bgMarkDepFltClose == TRUE ? "TRUE" : "FALSE" );
    }

    igNumDaysInAtc = 10;
    ilRc = ReadConfigEntry( "DATA_ARCHIVING", "NUM_DATA_DAYS", pclOneConfig ); 
    if( ilRc == RC_SUCCESS )
        igNumDaysInAtc = atoi(pclOneConfig);
    if( igNumDaysInAtc <= 0 || igNumDaysInAtc >= 9999 )
        igNumDaysInAtc = 10;
    dbg( TRACE, "NUM_DATA_DAYS = <%d>", igNumDaysInAtc );


    igFlight = tool_get_q_id( "flight" );
    dbg( TRACE, "FLIGHT MOD_ID = <%d>", igFlight );
    igWmqAtc = tool_get_q_id( "wmqatc" );
    dbg( TRACE, "WMQATC MOD_ID = <%d>", igWmqAtc );


    /** Reading MAIN **/
    dbg( TRACE, ".................................." );
    dbg( TRACE, "......... MAIN SECTION ..........." );
    dbg( TRACE, ".................................." );
    ilRc = ReadConfigEntry( "MAIN", "DEBUG_LEVEL", pclTmpStr);
    if( ilRc == RC_SUCCESS )
    {
        if( !strcmp(pclTmpStr,"DEBUG") )
            debug_level = DEBUG;
        else if( !strcmp(pclTmpStr,"OFF") )
            debug_level = 0;
    }

    return( RC_SUCCESS );
    
} /* end of initialize */


/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int ilRc = RC_SUCCESS;              /* Return code */
    
    dbg(TRACE,"Reset: now resetting");
    
    return ilRc;
    
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
    /* unset SIGCHLD ! DB-Child will terminate ! */

    dbg(TRACE,"Terminate: now DB logoff ...");

    signal(SIGCHLD,SIG_IGN);

    logoff();

    dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


    dbg(TRACE,"Terminate: now leaving ...");

    fclose(outp);

    sleep(ipSleep < 1 ? 1 : ipSleep);
    
    exit(0);
    
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/

static void HandleSignal(int pipSig)
{
    dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);

    switch(pipSig)
    {
    default :
        Terminate(1);
        break;
    } /* end of switch */

    exit(1);
    
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
    dbg(TRACE,"HandleErr: <%d> : unknown ERROR",pipErr);

    return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    switch(pipErr) {
    case    QUE_E_FUNC  :   /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :   /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND  :   /* Error using msgsnd */
        dbg(TRACE,"<%d> : msgsnd failed",pipErr);
        break;
    case    QUE_E_GET   :   /* Error using msgrcv */
        dbg(TRACE,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX   :
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE  :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES  :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP  :   /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL  :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG :   /* No message on queue */
        dbg(TRACE,"<%d> : no messages on queue",pipErr);
        break;
    case    QUE_E_INVORG    :   /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :   /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    default         :   /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
         
    return;

} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
    int ilRc = RC_SUCCESS;          /* Return code */
    int ilBreakOut = FALSE;
    
    do
    {
        ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text; 

        if( ilRc == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRc != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRc);
            } /* fi */
        
            switch( prgEvent->command )
            {
            case    HSB_STANDBY :
                ctrl_sta = prgEvent->command;
                break;  
    
            case    HSB_COMING_UP   :
                ctrl_sta = prgEvent->command;
                break;  
    
            case    HSB_ACTIVE  :
                ctrl_sta = prgEvent->command;
                ilBreakOut = TRUE;
                break;  

            case    HSB_ACT_TO_SBY  :
                ctrl_sta = prgEvent->command;
                break;  
    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate(1);
                break;  
    
            case    HSB_STANDALONE  :
                ctrl_sta = prgEvent->command;
                ResetDBCounter();
                ilBreakOut = TRUE;
                break;  

            case    REMOTE_DB :
                /* ctrl_sta is checked inside */
                HandleRemoteDB(prgEvent);
                break;

            case    SHUTDOWN    :
                Terminate(1);
                break;
                        
            case    RESET       :
                ilRc = Reset();
                break;
                        
            case    EVENT_DATA  :
                dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;

            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;

            default         :
                dbg(TRACE,"HandleQueues: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        } else {
            /* Handle queuing errors */
            HandleQueErr(ilRc);
        } /* end else */
    } while (ilBreakOut == FALSE);

            ilRc = Init_Process();
            if(ilRc != RC_SUCCESS)
            {
                dbg(TRACE,"init failed!");
            } 
     

} /* end of HandleQueues */
    



/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{
    int  ilFlightInATC;
    int     blMsgType;
    int    ilRc           = RC_SUCCESS;         /* Return code */
    int      ilCmd          = 0;
    int ili;
    BC_HEAD *prlBchead       = NULL;
    CMDBLK  *prlCmdblk       = NULL;
    char    *pclSelection    = NULL;
    char    *pclFields       = NULL;
    char    *pclData         = NULL;
    char    *pclRow          = NULL;
    char    clTable[34];
    char pclMsgHdr[8] = "\0";
    char pclTmpStr[256] = "\0";
    char *pclP;

    prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
    prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
    pclSelection = prlCmdblk->data;
    pclFields    = pclSelection + strlen(pclSelection) + 1;
    pclData      = pclFields + strlen(pclFields) + 1;


    strcpy(clTable,prlCmdblk->obj_name);

    dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);

    /****************************************/
    DebugPrintBchead(TRACE,prlBchead);
    DebugPrintCmdblk(TRACE,prlCmdblk);
    /* 20030630 JIM: may core!: dbg(TRACE,"Command: <%s>,prlCmdblk->command"); */
    dbg(TRACE,"Command: <%s>",prlCmdblk->command);
    dbg(TRACE,"originator follows event = %p ",prpEvent);

    dbg(TRACE,"originator<%d>",prpEvent->originator);
    dbg(TRACE,"selection follows Selection = %p ",pclSelection);
    dbg(TRACE,"selection <%s>",pclSelection);
    dbg(TRACE,"fields    <%s>",pclFields);
    dbg(TRACE,"data      <%s>",pclData);
    /****************************************/
    sprintf( pclMsgHdr, "%4.4s", pclData );
    dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);

    if( !strcmp(prlCmdblk->command, "ATC") )
    {
        if( strcmp( pclMsgHdr, "FLPL" ) && strcmp( pclMsgHdr,"DLFL" ) )
        {
            dbg( TRACE, "ERROR! Unrecognised message from ATC <%s>", pclData );
            dbg( TRACE, "=====================================END ================================" );
            return RC_FAIL;
        }
        blMsgType = -1;
        memset( &rgATC, 0, sizeof(rgATC) );  
        memset( &rgHDR, 0, sizeof(rgHDR) );  
        memset( &rgMSG, 0, sizeof(rgMSG) );  
        GetServerTimeStamp("UTC",1,0,rgATC.pcgCDAT);
        strcpy( rgATC.pcgUSEC, "thales" );

        GetHeader(pclData);
        if( !strcmp( pclMsgHdr,"FLPL" ) )
            GetMsgBody(pclData);
        ilRc = FindFlight( pclMsgHdr );

        if( !strcmp( pclMsgHdr,"FLPL" ) )
        {
           blMsgType = MSGTYPE_INSERT;
           if( ilRc == RC_SUCCESS )
                blMsgType = MSGTYPE_UPDATE;
           else
           {
               ilFlightInATC = FindFlightInATC();
               if( ilFlightInATC == RC_SUCCESS )
                   blMsgType = MSGTYPE_UPDATE;
           }
        }
        else if( !strcmp( pclMsgHdr,"DLFL" ) )
        {
           if( ilRc == RC_SUCCESS )
                blMsgType = MSGTYPE_DELETE;
        }

        /* recording purposes */
        InsertAtcRecord( " ", " "," ", pclData );

        if( blMsgType <= 0 )
        {
            dbg( TRACE, "nothing to process for ATC data <%s>", pclData );
            dbg( TRACE, "=====================================END ================================" );
            return RC_SUCCESS;
        }

        if( blMsgType == MSGTYPE_INSERT )
            ProcessFltInsert();
        else if( blMsgType == MSGTYPE_UPDATE )
            ProcessFltUpdate();
        else if( blMsgType == MSGTYPE_DELETE )
            ProcessFltDelete();
        if( blMsgType == MSGTYPE_UPDATE && rgINFO.bgSendBay == TRUE )
            SendBayInfo( " ", " ", " " );
    }
    else if( !strcmp(prlCmdblk->command, "CDB") ) /* Clean DB */
    {
        dbg( TRACE, "DB Cleaning...." );
        CleanDB();
        dbg( TRACE, "DB Cleaned" );
    }
    else if( !strcmp(prlCmdblk->command, "UFR") ||
             !strcmp(prlCmdblk->command, "IFR") ) 
    {
        if( rgINFO.bgSendBay == TRUE )
        {
            /* Get the Flight URNO from the selection string */
            strcpy( pclTmpStr, pclSelection );
            pclP = strchr( pclTmpStr, '=' ); 
            if( pclP )
            {
                pclP++;
                strcpy( pclTmpStr, pclP );
                pclP = strchr( pclTmpStr, '\n' );
                if( pclP )
                    *pclP = '\0';
            }
            SendBayInfo(pclFields,pclData,pclTmpStr);
        }
    }
    dbg( TRACE, "=====================================END ================================" );
    return(RC_SUCCESS);
    
} /* end of HandleData */

/*
static int RunSQL( char *pcpSelection, char *pcpData )
{
    int ilRc = DB_SUCCESS;
    short slSqlFunc = 0;
    short slSqlCursor = 0;
    char pclErrBuff[128];
    char *pclFunc = "RunSQL";

    dbg( DEBUG, "%s: SQL <%s>", pclFunc, pcpSelection );
    ilRc = sql_if ( START, &slSqlCursor, pcpSelection, pcpData );
    close_my_cursor ( &slSqlCursor );

    if( ilRc == DB_ERROR )
    {
        get_ora_err( ilRc, &pclErrBuff[0] );
        dbg( TRACE, "%s OraError! <%s>", pclFunc, &pclErrBuff[0] );
    }
    return ilRc;
}*/

/** sample msg **/
/** FLPLAIC940  OMAA2000AIC940  OMAA20003735A320M200413R132VIDP2250      **/
static int GetHeader( char *pcpData )
{
    int ilRc;
    int ili;
    char pclTmpStr[128] = "\0";
    char *pclFunc = "GetHeader";

    memcpy( rgHDR.pcgMsgID, pcpData, 4 );
    memcpy( rgHDR.pcgACID, &pcpData[4], 8 ); TrimSpace(rgHDR.pcgACID);
    memcpy( rgHDR.pcgORG4, &pcpData[12], 4 );
    memcpy( rgHDR.pcgEOBT, &pcpData[16], 4 );

    rgHDR.cgADID = 'A';
    if( !strcmp(pcgHopo4, rgHDR.pcgORG4) )
        rgHDR.cgADID = 'D';

    strcpy( rgHDR.pcgCSGN, rgHDR.pcgACID ); 
    if( rgINFO.bgMapCallSign == TRUE )
        MapCallSign( rgHDR.pcgACID, rgHDR.pcgCSGN );
    dbg( DEBUG, "%s: MsgID <%s>", pclFunc, rgHDR.pcgMsgID );
    dbg( DEBUG, "%s: ACID <%s> - CSGN <%s>", pclFunc, rgHDR.pcgACID, rgHDR.pcgCSGN );
    dbg( DEBUG, "%s: ORG4 <%s>", pclFunc, rgHDR.pcgORG4 );
    dbg( DEBUG, "%s: EOBT <%s>", pclFunc, rgHDR.pcgEOBT );
    dbg( DEBUG, "%s: ADID <%c>", pclFunc, rgHDR.cgADID );

    FormatCsgnFlno( rgHDR.pcgCSGN, rgATC.pcgFLNO );
}

static int GetMsgBody( char *pcpData )
{
    int ilRc;
    char *pclFunc = "GetMsgBody";

    memcpy( rgMSG.pcgSSRC, &pcpData[36], 4 ); TrimSpace(rgMSG.pcgSSRC);
    memcpy( rgMSG.pcgACT5, &pcpData[40], 4 ); TrimSpace(rgMSG.pcgACT5);
    memcpy( rgMSG.pcgIAIR, &pcpData[45], 4 ); TrimSpace(rgMSG.pcgIAIR);
    memcpy( rgMSG.pcgRWYD, &pcpData[49], 3 ); TrimSpace(rgMSG.pcgRWYD);
    memcpy( rgMSG.pcgIBAY, &pcpData[52], 4 ); TrimSpace(rgMSG.pcgIBAY);
    memcpy( rgMSG.pcgDES4, &pcpData[56], 4 ); TrimSpace(rgMSG.pcgDES4);
    memcpy( rgMSG.pcgIETA, &pcpData[60], 4 ); TrimSpace(rgMSG.pcgIETA);
    memcpy( rgMSG.pcgILND, &pcpData[64], 4 ); TrimSpace(rgMSG.pcgILND);
    memcpy( rgMSG.pcgRWYA, &pcpData[68], 3 ); TrimSpace(rgMSG.pcgRWYA);

    dbg( DEBUG, "%s: SSRC <%s>", pclFunc, rgMSG.pcgSSRC ); 
    dbg( DEBUG, "%s: ACT5 <%s>", pclFunc, rgMSG.pcgACT5 ); 
    dbg( DEBUG, "%s: IAIR <%s>", pclFunc, rgMSG.pcgIAIR ); 
    dbg( DEBUG, "%s: RWYD <%s>", pclFunc, rgMSG.pcgRWYD ); 
    dbg( DEBUG, "%s: IBAY <%s>", pclFunc, rgMSG.pcgIBAY ); 
    dbg( DEBUG, "%s: DES4 <%s>", pclFunc, rgMSG.pcgDES4 ); 
    dbg( DEBUG, "%s: IETA <%s>", pclFunc, rgMSG.pcgIETA ); 
    dbg( DEBUG, "%s: ILND <%s>", pclFunc, rgMSG.pcgILND ); 
    dbg( DEBUG, "%s: RWYA <%s>", pclFunc, rgMSG.pcgRWYA ); 
}

static int MapCallSign( char *pcpSource, char *pcpMapped )
{
    int ilRc;
    char pclSqlBuf[1024] = "\0";
    char pclSqlData[512] = "\0";
    char *pclFunc = "MapCallSign";

    sprintf( pclSqlBuf, "SELECT TRIM(CSGN) FROM FIMTAB WHERE ICSG = '%s' AND DSSN = 'ATC' AND ADID = '%c' "
                        "AND VAFR <= '%s' AND "
                        "(VATO = ' ' OR VATO >= '%s')", pcpSource, rgHDR.cgADID, rgATC.pcgCDAT, rgATC.pcgCDAT );
    ilRc = RunSQL( pclSqlBuf, pclSqlData );
    dbg( TRACE, "%s: ilRc <%d> SQL <%s>", pclFunc, ilRc, pclSqlBuf );
    strcpy( pcpMapped, pcpSource );
    if( ilRc == DB_SUCCESS )
        strcpy( pcpMapped, pclSqlData );
    dbg( DEBUG, "%s: Source <%s> Mapped <%s>", pclFunc, pcpSource, pcpMapped );
    return ilRc;
}

static int FindFlight( char *pcpMsgType )
{
    int ilRc;
    int ilCount;
    int ilStepLeft;
    int ilStepRight;
    int ilMaxLeft;
    int ilMaxRight;
    int ilMoveLeft;
    int ilMoveRight;
    char pclSqlBuf[1024] = "\0";
    char pclSqlData[1024] = "\0";
    char pclOneData[512] = "\0";
    char pclBegin[20] = "\0";
    char pclEnd[20] = "\0";
    char pclNow[20] = "\0";
    char pclOrg3[4] = "\0";
    char pclOnbe[16] = "\0";
    char pclOfbl[16] = "\0";
    char pclBaseTime[16] = "\0";
    char pclMarkSrch[32] = "\0";
    char pclTmpStr[1024] = "\0";

    char *pclFunc = "FindFlight";

    strcpy( pclMarkSrch, " " );
    if( strcmp( pcpMsgType, "DLFL" ) ) 
	{
		if( rgHDR.cgADID == 'A' && rgDFLT.bgMarkArrFltClose == TRUE )
			strcpy( pclMarkSrch, " AND RACO = ' ' " );
		if( rgHDR.cgADID == 'D' && rgDFLT.bgMarkDepFltClose == TRUE )
			strcpy( pclMarkSrch, " AND RACO = ' ' " );
	}
    ilCount = 0;
    syslibSearchDbData("APTTAB","APC4",rgHDR.pcgORG4,"APC3",rgHDR.pcgORG3,&ilCount,"\n") ;
    dbg( TRACE, "%s: APC3 <%s> APC4 <%s>", pclFunc, rgHDR.pcgORG3, rgHDR.pcgORG4 );
    
    strcpy( pclNow, rgATC.pcgCDAT );
/*    sprintf( pclOfbl, "%8.8s%4.4s00", pclNow, rgHDR.pcgEOBT );
    dbg( TRACE, "%s: Estimated OFBL timing string <%s><%s>", pclFunc, rgHDR.pcgEOBT, pclOfbl );*/

    /* Finding flight. This is a great challenge */
    if( rgHDR.cgADID == 'A' )
    {
        if( strlen(rgMSG.pcgILND) > 0 )
        {
            dbg( TRACE, "%s: Guess Arrival Time base on LANDING time", pclFunc );
            GuessTheDate( pclNow, rgMSG.pcgILND, pclBaseTime, "HHMM" );
        }
        else if( strlen(rgMSG.pcgIETA) > 0 )
        {
            dbg( TRACE, "%s: Guess Arrival Time base on ETA time", pclFunc );
            GuessTheDate( pclNow, rgMSG.pcgIETA, pclBaseTime, "HHMM" );
        }
        else
        {
            dbg( TRACE, "%s: Guess Arrival Time base on current time", pclFunc );
            strcpy( pclBaseTime, pclNow );
        }

        ilStepLeft = -rgINFO.igArrStepLeft;     ilStepRight = rgINFO.igArrStepRight;
        ilMaxLeft = rgINFO.igArrMaxLeft;        ilMaxRight = rgINFO.igArrMaxRight;
    }
    else
    {
        if( strlen(rgMSG.pcgIAIR) > 0 )
        {
            dbg( TRACE, "%s: Guess Dep Time base on AIRBORNE time", pclFunc );
            GuessTheDate( pclNow, rgMSG.pcgIAIR, pclBaseTime, "HHMM" );
        }
        else
        {
            dbg( TRACE, "%s: Guess Dep Time base on OFBL", pclFunc );
            GuessTheDate( pclNow, rgHDR.pcgEOBT, pclBaseTime, "HHMM" );
        }

        ilStepLeft = -rgINFO.igDepStepLeft;     ilStepRight = rgINFO.igDepStepRight;
        ilMaxLeft = rgINFO.igDepMaxLeft;        ilMaxRight = rgINFO.igDepMaxRight;
    }

/*ARR_SEARCH_HOURS_RULE = 5,2,-72,+12
DEP_SEARCH_HOURS_RULE = 3,3,-10,+12*/ 

    ilMoveLeft = ilStepLeft;
    ilMoveRight = ilStepRight;
    while( ilMoveLeft >= ilMaxLeft || ilMoveRight <= ilMaxRight )
    {
        strcpy( pclBegin, pclBaseTime ); 
        strcpy( pclEnd, pclBaseTime );
        dbg( DEBUG, "%s MoveLeft <%d> MoveRight <%d> MaxLeft <%d> MaxRight <%d>", pclFunc, ilMoveLeft, ilMoveRight, ilMaxLeft, ilMaxRight );
        AddSecondsToCEDATime(pclBegin,ilMoveLeft*SECONDS_PER_HOUR,1);
        AddSecondsToCEDATime(pclEnd,ilMoveRight*SECONDS_PER_HOUR,1);

        if( rgHDR.cgADID == 'A' )
        {
/*            sprintf( pclSqlBuf, "SELECT URNO,TRIM(FLDA),TRIM(REGN),TRIM(PSTA),TRIM(FLNO),TRIM(ONBE),TRIM(TIFA) FROM AFTTAB WHERE "
                                "(CSGN = '%s' OR FLNO = '%s') AND ADID = 'A' AND "
                                "(ORG4 = '%s' OR VIA4 = '%s') AND "
                                "ONBL = ' ' AND TIFA BETWEEN '%s' AND '%s' "
                                "ORDER BY TIFA ASC", 
                                rgHDR.pcgCSGN, rgHDR.pcgCSGN, rgHDR.pcgORG4, rgHDR.pcgORG4, pclBegin, pclEnd );
*/            sprintf( pclSqlBuf, "SELECT URNO,TRIM(FLDA),TRIM(REGN),TRIM(PSTA),TRIM(FLNO),TRIM(ONBE),TRIM(TIFA),TRIM(LAND) FROM AFTTAB WHERE "
                                "(CSGN = '%s') AND DES3 = '%s' %s "
                                "TIFA BETWEEN '%s' AND '%s' %s "
                                "ORDER BY TIFA ASC", 
                                rgHDR.pcgCSGN, cgHopo, pcgFtypSrchStr, pclBegin, pclEnd, pclMarkSrch );
        }
        else
        {
/*            sprintf( pclSqlBuf, "SELECT URNO,TRIM(FLDA),TRIM(REGN),TRIM(PSTD),TRIM(FLNO),TRIM(OFBL),TRIM(TIFD) FROM AFTTAB WHERE "
                                "(CSGN = '%s' OR FLNO = '%s') AND ADID <> 'A' AND "
                                "(ORG4 = '%s' OR VIA4 = '%s') AND "
                                "TIFD BETWEEN '%s' AND '%s' "
                                "ORDER BY TIFD ASC", 
                                rgHDR.pcgCSGN, rgHDR.pcgCSGN, rgHDR.pcgORG4, rgHDR.pcgORG4, pclBegin, pclEnd );
*/            sprintf( pclSqlBuf, "SELECT URNO,TRIM(FLDA),TRIM(REGN),TRIM(PSTD),TRIM(FLNO),TRIM(OFBL),TRIM(TIFD),TRIM(AIRB) FROM AFTTAB WHERE "
                                "(CSGN = '%s') AND ORG3 = '%s' %s "
                                "TIFD BETWEEN '%s' AND '%s' %s "
                                "ORDER BY TIFD ASC", 
                                rgHDR.pcgCSGN, cgHopo, pcgFtypSrchStr, pclBegin, pclEnd, pclMarkSrch );
        }
        ilRc = RunSQL( pclSqlBuf, pclSqlData );
        dbg( TRACE, "%s: ilRc <%d> SQL <%s>", pclFunc, ilRc, pclSqlBuf );
        if( ilRc == DB_SUCCESS )
            break;
        if( ilMoveLeft <= ilMaxLeft && ilMoveRight >= ilMaxRight )
            break;
        if( ilMoveLeft > ilMaxLeft )
            ilMoveLeft += ilStepLeft;
        if( ilMoveRight < ilMaxRight )
            ilMoveRight += ilStepRight; 
        if( ilMoveLeft < ilMaxLeft )
            ilMoveLeft = ilMaxLeft;
        if( ilMoveRight > ilMaxRight )
            ilMoveRight = ilMaxRight;
    } /* while loop */
    if( ilRc != DB_SUCCESS )
    {
        dbg( TRACE, "%s: NO FLIGHT FOUND!!!", pclFunc );
        return RC_FAIL;
    }

    dbg( TRACE, "%s: FOUND FLIGHT!!! ADID <%c>", pclFunc, rgHDR.cgADID );
    get_fld( pclSqlData,FIELD_1,STR,20,rgATC.pcgUAFT ); dbg( DEBUG, "%s: AFTTAB.URNO <%s>", pclFunc, rgATC.pcgUAFT );
    get_fld( pclSqlData,FIELD_2,STR,10,rgATC.pcgFLDA ); dbg( DEBUG, "%s: AFTTAB.FLDA <%s>", pclFunc, rgATC.pcgFLDA );
    get_fld( pclSqlData,FIELD_3,STR,10,rgATC.pcgREGN ); dbg( DEBUG, "%s: AFTTAB.REGN <%s>", pclFunc, rgATC.pcgREGN );
    get_fld( pclSqlData,FIELD_4,STR,10,rgATC.pcgPKST ); dbg( DEBUG, "%s: AFTTAB.PSTA/PSTD <%s>", pclFunc, rgATC.pcgPKST );
    get_fld( pclSqlData,FIELD_5,STR,20,rgATC.pcgFLNO ); dbg( DEBUG, "%s: AFTTAB.FLNO <%s>", pclFunc, rgATC.pcgFLNO );
    get_fld( pclSqlData,FIELD_6,STR,20,rgATC.pcgONOF ); dbg( DEBUG, "%s: AFTTAB.ONBE/OFBL <%s>", pclFunc, rgATC.pcgONOF );
    get_fld( pclSqlData,FIELD_7,STR,20,rgATC.pcgTIFT ); dbg( DEBUG, "%s: AFTTAB.TIFA/TIFD <%s>", pclFunc, rgATC.pcgTIFT );
    if( rgHDR.cgADID == 'A' )
    {
        get_fld( pclSqlData,FIELD_8,STR,20,rgATC.pcgLAND ); dbg( DEBUG, "%s: AFTTAB.LAND <%s>", pclFunc, rgATC.pcgLAND );
    }
    else
    {
        get_fld( pclSqlData,FIELD_8,STR,20,rgATC.pcgAIRB ); dbg( DEBUG, "%s: AFTTAB.AIRB <%s>", pclFunc, rgATC.pcgAIRB );
    }
    return RC_SUCCESS;
} /* FindFlight */

static int FindFlightInATC()
{
    int ilRc;
    int ilCount;
    char pclSqlBuf[1024] = "\0";
    char pclSqlData[1024] = "\0";
    char pclTimeSearch[20] = "\0";
    char pclTmpStr[1024] = "\0";

    char *pclFunc = "FindFlightInATC";

    strcpy( pclTimeSearch, rgATC.pcgCDAT );
    AddSecondsToCEDATime(pclTimeSearch,-120,1);
    sprintf( pclSqlBuf, "SELECT UAFT,TRIM(TIFT) FROM ATCTAB WHERE ICSG = '%s' AND ACTI = 'IFR' AND CDAT <> ' ' AND CDAT >= '%s'",
                        rgHDR.pcgACID, pclTimeSearch );
    ilRc = RunSQL( pclSqlBuf, pclSqlData );
    dbg( TRACE, "%s: ilRc <%d> SQL <%s>", pclFunc, ilRc, pclSqlBuf );
    if( ilRc == DB_SUCCESS )
    {
        get_fld( pclSqlData,FIELD_1,STR,20,rgATC.pcgUAFT ); dbg( DEBUG, "%s: ATCTAB.UAFT <%s>", pclFunc, rgATC.pcgUAFT );
        get_fld( pclSqlData,FIELD_2,STR,20,rgATC.pcgTIFT ); dbg( DEBUG, "%s: ATCTAB.TIFT <%s>", pclFunc, rgATC.pcgTIFT );
        return RC_SUCCESS;
    }
    return RC_FAIL;

} /* FindFlightInATC */

static int InsertAtcRecord( char *pcpCmd, char *pcpAftFields, char *pcpAftData, char *pcpRawData )
{
    int ilRc;
    int ilUrno;
    char pclSqlBuf[1024] = "\0";
    char pclSqlData[512] = "\0";
    char *pclFunc = "InsertAtcRecord";

    ilUrno = NewUrnos( "ATCTAB", 1 );
    sprintf( rgATC.pcgURNO, "%d", ilUrno );
    sprintf( pclSqlBuf, "INSERT INTO ATCTAB "
                        "(URNO,MGID,ICSG,ORG4,DES4,IOFB,CSGN,SSRC,ACT5,IAIR,"
                         "ILND,RWYD,RWYA,IBAY,IETA,UAFT,FLNO,FLDA,ONOF,TIFT,"
                         "REGN,PKST,CDAT,USEC,ACTI,LAND,AIRB,RAWD,FLDS,DATA) "
                        "VALUES "
                        "('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',"
                         "'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',"
                         "'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s') ",
                        rgATC.pcgURNO, rgHDR.pcgMsgID,rgHDR.pcgACID, rgHDR.pcgORG4, rgMSG.pcgDES4, 
                        rgHDR.pcgEOBT, rgHDR.pcgCSGN, rgMSG.pcgSSRC, rgMSG.pcgACT5, rgMSG.pcgIAIR, 
                        rgMSG.pcgILND, rgMSG.pcgRWYD, rgMSG.pcgRWYA, rgMSG.pcgIBAY, rgMSG.pcgIETA, 
                        rgATC.pcgUAFT, rgATC.pcgFLNO, rgATC.pcgFLDA, rgATC.pcgONOF, rgATC.pcgTIFT, 
                        rgATC.pcgREGN, rgATC.pcgPKST, rgATC.pcgCDAT, rgATC.pcgUSEC, pcpCmd,        
                        rgATC.pcgLAND, rgATC.pcgAIRB, pcpRawData,    pcpAftFields,  pcpAftData );
    ilRc = RunSQL( pclSqlBuf, pclSqlData );
    dbg( DEBUG, "%s: ilRc <%d> Buf <%s>", pclFunc, ilRc, pclSqlBuf );
    return ilRc;
}   

static int UpdateAtcRecord( char *pcpCmd, char *pcpAftFields, char *pcpAftData )
{
    int ilRc;
    int ilUrno;
    char pclSqlBuf[1024] = "\0";
    char pclSqlData[512] = "\0";
    char pclTmpStr[512] = "\0";
    char *pclFunc = "UpdateAtcRecord";

    GetServerTimeStamp("UTC",1,0,rgATC.pcgLSTU);
    sprintf( pclSqlBuf, "UPDATE ATCTAB SET "
                        "UAFT=%s,FLNO='%s',FLDA='%s',ONOF='%s',TIFT='%s',REGN='%s',PKST='%s',LSTU='%s',"
                        "AIRB='%s',LAND='%s',FLDS='%s',DATA='%s' ",
                        rgATC.pcgUAFT, rgATC.pcgFLNO, rgATC.pcgFLDA, rgATC.pcgONOF, rgATC.pcgTIFT, 
                        rgATC.pcgREGN, rgATC.pcgPKST, rgATC.pcgLSTU, rgATC.pcgAIRB, rgATC.pcgLAND,
                        pcpAftFields,  pcpAftData );
    if( pcpCmd[0] != ' ' )
    {
        sprintf( pclTmpStr, ", ACTI = '%s' ", pcpCmd );
        strcat( pclSqlBuf, pclTmpStr );
    }
    sprintf( pclTmpStr, "WHERE URNO = %s ", rgATC.pcgURNO );
    strcat( pclSqlBuf, pclTmpStr );

    ilRc = RunSQL( pclSqlBuf, pclSqlData );
    dbg( DEBUG, "%s: ilRc <%d> Buf <%s>", pclFunc, ilRc, pclSqlBuf );
    return ilRc;
}   

static int ProcessFltInsert()
{
    int ilRc;
    int ilCount;
    int ilCircularFlight;
    int ilDiffMin;
    char pclCmd[12] = "\0";
    char pclAftFields[1024] = "\0";
    char pclAftData[1024] = "\0";
    char pclSTOA[20] = "\0";
    char pclSTOD[20] = "\0";
    char pclETOA[20] = "\0";
    char pclETOD[20] = "\0";
    char pclTmpStr[1024] = "\0";
    char *pclFunc = "ProcessFltInsert";

    if( rgINFO.bgProcessInsert == FALSE )
    {
        dbg( TRACE, "%s: INSERT OFF", pclFunc );
        UpdateAtcRecord( "IOF", " ", " " );
        return RC_SUCCESS;
    }

    GetNextValues( rgATC.pcgUAFT, 1 );
    syslibSearchDbData("APTTAB","APC4",rgMSG.pcgDES4,"APC3",rgMSG.pcgDES3,&ilCount,"\n") ;
    ilCircularFlight = FALSE;
    if( strlen(rgHDR.pcgORG4) > 0 && strlen(rgMSG.pcgDES4) > 0 && !strcmp( rgHDR.pcgORG4,rgMSG.pcgDES4 ) )
        ilCircularFlight = TRUE;


    if( rgHDR.cgADID == 'A' || ilCircularFlight == TRUE )
    {
        strcpy( rgATC.pcgLAND, " " );
        strcpy( pclSTOA, rgATC.pcgCDAT );
        if( strlen(rgMSG.pcgILND) > 0 )
        {
            GuessTheDate( rgATC.pcgCDAT, rgMSG.pcgILND, rgATC.pcgLAND, "HHMM" );
            strcpy( pclSTOA, rgATC.pcgLAND );
        }

        strcpy( pclETOA, " " );
        if( strlen(rgMSG.pcgIETA) > 0 )
        {
            GuessTheDate( rgATC.pcgCDAT, rgMSG.pcgIETA, pclETOA, "HHMM" );
            if( strlen(rgMSG.pcgILND) <= 0 )
                strcpy( pclSTOA, pclETOA );
            
            if( rgINFO.igIgnoreETATimeRange > 0 )
            {
                /* Decide whether want to publish ETAA */
                UTCTimeDiff(pclETOA, rgATC.pcgCDAT, &ilDiffMin); ilDiffMin = abs(ilDiffMin);
                dbg( DEBUG, "%s: [ETOA] <%s> ilDiffMin <%d>", pclFunc, pclETOA, ilDiffMin );
            }
        }
        strcpy( rgATC.pcgTIFT, pclSTOA );
    }
    
    if( rgHDR.cgADID == 'D' || ilCircularFlight == TRUE )
    {
        strcpy( rgATC.pcgAIRB, " " );
        strcpy( pclSTOD, rgATC.pcgCDAT );
        if( strlen(rgMSG.pcgIAIR) > 0 )
        {
            GuessTheDate( rgATC.pcgCDAT, rgMSG.pcgIAIR, rgATC.pcgAIRB, "HHMM" );
            strcpy( pclSTOD, rgATC.pcgAIRB );
        }

        strcpy( pclETOD, " " );
        if( strlen(rgHDR.pcgEOBT) > 0 )
        {
            GuessTheDate( rgATC.pcgCDAT, rgHDR.pcgEOBT, pclETOD, "HHMM" );
            if( strlen(rgMSG.pcgIAIR) <= 0 )
                strcpy( pclSTOD, pclETOD );
        }
        strcpy( rgATC.pcgTIFT, pclSTOD );
    }

    /* Prepare common fields first */
    sprintf( pclAftFields, "%s", "URNO,ORG3,ORG4,DES3,DES4,CSGN,SSRC,ACT5,FTYP,TTYP,"
                                 "STYP,FLNO,ALC2,ALC3" );
    sprintf( pclAftData, "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,"
                         "%s,%s,%s,%s", 
                         rgATC.pcgUAFT, rgHDR.pcgORG3, rgHDR.pcgORG4, rgMSG.pcgDES3, rgMSG.pcgDES4, 
                         rgHDR.pcgCSGN, rgMSG.pcgSSRC, rgMSG.pcgACT5,rgIFLT.pcgFtyp,rgIFLT.pcgTtyp, 
                         rgIFLT.pcgStyp, rgATC.pcgFLNO, rgATC.pcgALC2, rgATC.pcgALC3 );

    /* Prepare arrival fields */
    if( rgHDR.cgADID == 'A' || ilCircularFlight == TRUE )
    {
        sprintf( pclTmpStr, "%s", ",STOA,LNDA,RWYA" );
        strcat( pclAftFields, pclTmpStr );
        sprintf( pclTmpStr, ",%s,%s,%s", 
                            pclSTOA, rgATC.pcgLAND, rgMSG.pcgRWYA );
        strcat( pclAftData, pclTmpStr );

        if( rgINFO.igIgnoreETATimeRange <= 0 || 
           (rgINFO.igIgnoreETATimeRange > 0 && ilDiffMin < rgINFO.igIgnoreETATimeRange) )
        {
            strcat( pclAftFields, ",ETAA" );
            sprintf( pclTmpStr, ",%s", pclETOA );
            strcat( pclAftData, pclTmpStr );
        }
    }

    /* Prepare departure fields */
    if( rgHDR.cgADID == 'D' || ilCircularFlight == TRUE )
    {
        sprintf( pclTmpStr, "%s", ",STOD,ETDA,AIRA,RWYD" );
        strcat( pclAftFields, pclTmpStr );
        sprintf( pclTmpStr, ",%s,%s,%s,%s", 
                            pclSTOD, pclETOD, rgATC.pcgAIRB, rgMSG.pcgRWYD );
        strcat( pclAftData, pclTmpStr );
    }
    strcpy( pclCmd, "IFR" );
    dbg( TRACE, "%s CMD <%s>", pclFunc, pclCmd );
    dbg( TRACE, "%s FLD <%s>", pclFunc, pclAftFields );
    dbg( TRACE, "%s DATA <%s>", pclFunc, pclAftData );
    SendCedaEvent( igFlight, 0, mod_name, " ",  " ", " ", pclCmd, "AFTTAB", 
                   " ", pclAftFields, pclAftData, " ", 3, NETOUT_NO_ACK );
    UpdateAtcRecord( pclCmd, pclAftFields, pclAftData );
    return ilRc;
}

static int ProcessFltUpdate()
{
    int ilRc;
    int ilETADiffMin = 0;
    int ilLandDiffMin = 0;
    int ilAirbDiffMin = 0;
    int blHasEtaTiming;
    char pclCmd[12] = "\0";
    char pclAftFields[1024] = "\0";
    char pclAftData[1024] = "\0";
    char pclEstimateDT[20] = "\0";
    char pclActualDT[20] = "\0";
    char pclWhere[128] = "\0";
    char pclSelection[128] = "\0";
    char *pclFunc = "ProcessFltUpdate";

    if( rgINFO.bgProcessUpdate == FALSE )
    {
        dbg( TRACE, "%s: UPDATE OFF", pclFunc );
        UpdateAtcRecord( "UOF", " ", " " );
        return RC_SUCCESS;
    }

    blHasEtaTiming = FALSE;

    if( rgHDR.cgADID == 'A' )
    {
        strcpy( pclEstimateDT, " " );
        if( strlen(rgMSG.pcgIETA) > 0 )
        {
            GuessTheDate( rgATC.pcgTIFT, rgMSG.pcgIETA, pclEstimateDT, "HHMM" );
            blHasEtaTiming = TRUE;

            if( rgINFO.igIgnoreETATimeRange > 0 )
            {
                /* Decide whether want to publish ETAA */
                UTCTimeDiff(pclEstimateDT, rgATC.pcgCDAT, &ilETADiffMin); 
                ilETADiffMin = abs(ilETADiffMin);
                dbg( DEBUG, "%s: [ETOA] <%s> ilETADiffMin <%d>", pclFunc, pclEstimateDT, ilETADiffMin );
            }
        }

        strcpy( pclActualDT, " " );
        if( strlen(rgMSG.pcgILND) > 0 )
        {
            GuessTheDate( rgATC.pcgTIFT, rgMSG.pcgILND, pclActualDT, "HHMM" );
            if( rgINFO.igIgnoreLandTimeRange > 0 )
            {
                /* Decide whether want to publish LAND */
                UTCTimeDiff(pclActualDT, rgATC.pcgCDAT, &ilLandDiffMin); 
                ilLandDiffMin = abs(ilLandDiffMin);
                dbg( DEBUG, "%s: [LAND] <%s> ilLandDiffMin <%d>", pclFunc, pclActualDT, ilLandDiffMin );
            }
        }
		/* v1.17 */
		else
		{ 
			/* No landing time receive, check existing LAND */
			if( strlen(rgATC.pcgLAND) > 0 )
			{
                dbg( DEBUG, "%s: EMPTY LAND! Ignore whole message. Flight has LANDING time filled", pclFunc );
			    UpdateAtcRecord( "IGN", " ", " " );
				return RC_SUCCESS;
			}
		}

        strcpy( pclAftFields, "SSRC,RWYA" );
        sprintf( pclAftData, "%s,%s", rgMSG.pcgSSRC, rgMSG.pcgRWYA );
        if( rgUFLT.bgUpdEstArr == TRUE && 
           (rgINFO.igIgnoreETATimeRange <= 0 || 
           (rgINFO.igIgnoreETATimeRange > 0 && ilETADiffMin < rgINFO.igIgnoreETATimeRange)) )
        { 
            strcat( pclAftFields, "," );
            strcat( pclAftFields, "ETAA" );
            strcat( pclAftData, "," );
            strcat( pclAftData, pclEstimateDT );
        }
        if( rgUFLT.bgUpdLandAirb == TRUE &&
           (rgINFO.igIgnoreLandTimeRange <= 0 || 
           (rgINFO.igIgnoreLandTimeRange > 0 && ilLandDiffMin < rgINFO.igIgnoreLandTimeRange)) )
        {
            strcat( pclAftFields, "," );
            strcat( pclAftFields, "LNDA" );
            strcat( pclAftData, "," );
            strcat( pclAftData, pclActualDT );
        }
        if( rgUFLT.bgUpdAct5 == TRUE )
        {
            strcat( pclAftFields, "," );
            strcat( pclAftFields, "ACT5" );
            strcat( pclAftData, "," );
            strcat( pclAftData, rgMSG.pcgACT5 );
        }
    }

	/* departure flight */
    else
    {
        strcpy( pclEstimateDT, " " );
        if( strlen(rgHDR.pcgEOBT) > 0 )
            GuessTheDate( rgATC.pcgTIFT, rgHDR.pcgEOBT, pclEstimateDT, "HHMM" );

        strcpy( pclActualDT, " " );
        if( strlen(rgMSG.pcgIAIR) > 0 )
        {
            GuessTheDate( rgATC.pcgTIFT, rgMSG.pcgIAIR, pclActualDT, "HHMM" );
            if( rgINFO.igIgnoreAirbTimeRange > 0 )
            {
                /* Decide whether want to publish AIRB */
                UTCTimeDiff(pclActualDT, rgATC.pcgCDAT, &ilAirbDiffMin); 
                ilAirbDiffMin = abs(ilAirbDiffMin);
                dbg( DEBUG, "%s: [AIRB] <%s> ilAirbDiffMin <%d>", pclFunc, pclActualDT, ilAirbDiffMin );
            }
        }
		/* v1.17 */
		else
		{
			/* No airborne time receive, check existing AIRB */
			if( strlen(rgATC.pcgAIRB) > 0 )
			{
                dbg( DEBUG, "%s: EMPTY AIRB! Ignore whole message. Flight has AIRBORNE time filled", pclFunc );
			    UpdateAtcRecord( "IGN", " ", " " );
				return RC_SUCCESS;
			}
		}

        strcpy( pclAftFields, "SSRC,RWYD" );
        sprintf( pclAftData, "%s,%s", rgMSG.pcgSSRC, rgMSG.pcgRWYD );
        if( rgUFLT.bgUpdLandAirb == TRUE &&
           (rgINFO.igIgnoreAirbTimeRange <= 0 || 
           (rgINFO.igIgnoreAirbTimeRange > 0 && ilAirbDiffMin < rgINFO.igIgnoreAirbTimeRange)) )
        {
            strcat( pclAftFields, "," );
            strcat( pclAftFields, "AIRA" );
            strcat( pclAftData, "," );
            strcat( pclAftData, pclActualDT );
        }
        if( rgUFLT.bgUpdAct5 == TRUE )
        {
            strcat( pclAftFields, "," );
            strcat( pclAftFields, "ACT5" );
            strcat( pclAftData, "," );
            strcat( pclAftData, rgMSG.pcgACT5 );
        }
    }

    strcpy( pclCmd, "UFR" );
    sprintf( pclWhere, "WHERE URNO = %s", rgATC.pcgUAFT );
    dbg( TRACE, "%s CMD <%s>", pclFunc, pclCmd );
    dbg( TRACE, "%s FLD <%s>", pclFunc, pclAftFields );
    dbg( TRACE, "%s DATA <%s>", pclFunc, pclAftData );
    dbg( TRACE, "%s SELECT <%s>", pclFunc, pclWhere );
    SendCedaEvent( igFlight, 0, mod_name, " ",  " ", " ", pclCmd, "AFTTAB", 
                   pclWhere, pclAftFields, pclAftData, " ", 3, NETOUT_NO_ACK );
    UpdateAtcRecord( pclCmd, pclAftFields, pclAftData );

    /*** To activate TMO calculation, EXCO command is to be sent ***/
    /*** No Flight URNO required, FLIGHT process looks for the flight with CSGN ***/
    /** Fields needed: CSGN,ORGS4,DES4,ADID (ARRIVAL FLIGHTS ONLY) **/
/** Extract from Athen log file **/
/*
flight 20110519002807:531:ATC TABLE  <AFTTAB>
flight 20110519002807:531:ATC FIELDS <CSGN,ORG4,DES4,ETDA,ETAA,AIRA,LNDA,RWYA,ACT5>
flight 20110519002807:531:ATC (OLD)  <ORG4,DES4,ETDA>
flight 20110519002807:531:ATC DATA   <CSA420,LKPR,LGAV,20110518200000,20110518221923,,,03L,B735>
flight 20110519002807:532:ATC (OLD)  <,,>
flight 20110519002807:532:ATC SELECT <ATC>
*/

/* Removed, no longer valid  22-FEB-2012 */
/*    if( blHasEtaTiming == TRUE )
    {
        if( strlen(rgMSG.pcgSSRC) > 0 )
        {
            strcpy( pclAftFields, "CSGN,ORGS4,DES4,ADID" );
            sprintf( pclAftData, "%s,%s,%s,%s", rgHDR.pcgCSGN, rgHDR.pcgORG4, rgMSG.pcgDES4, rgHDR.cgADID );
            sprintf( pclWhere, "ATC,SSR,%s", rgATC.pcgUAFT );
            SendCedaEvent( igFlight, 0, mod_name, " ",  " ", " ", "EXCO", "AFTTAB", 
                           pclWhere, pclAftFields, pclAftData, " ", 3, NETOUT_NO_ACK );
        }
    }
*/
}

static int ProcessFltDelete()
{
    int ilRc;
    char pclCmd[12] = "\0";
    char pclAftFields[1024] = "\0";
    char pclAftData[1024] = "\0";
    char pclWhere[128] = "\0";
    char pclTmpStr[128] = "\0";
	char blArrival;
    char *pclFunc = "ProcessFltDelete";

    sprintf( pclWhere, "WHERE URNO = %s", rgATC.pcgUAFT );

    if( rgINFO.bgProcessDelete == FALSE )
    {
        dbg( TRACE, "%s: DELETE OFF", pclFunc );

		blArrival = FALSE;
        if( strcmp( rgHDR.pcgORG4, pcgHopo4 ) )
			blArrival = TRUE;
        if( (blArrival == TRUE && rgDFLT.bgMarkArrFltClose == TRUE) ||
			(blArrival == FALSE && rgDFLT.bgMarkDepFltClose == TRUE) )
		{
	        if( blArrival == TRUE )
		        strcpy( pclTmpStr, rgATC.pcgLAND );
			if( blArrival == FALSE )
				strcpy( pclTmpStr, rgATC.pcgAIRB );
            if( strlen(pclTmpStr) <= 0 )
            {
                dbg( TRACE, "%s FLIGHT has not LAND/AIRB, ignore DLFL", pclFunc );
                UpdateAtcRecord( "IGN", " ", " " );
                return RC_SUCCESS;
            }
            sprintf( pclAftFields, "%s", "RACO" );
            sprintf( pclAftData, "%4.4s", &rgATC.pcgCDAT[10] );

            strcpy( pclCmd, "UFR" );
            dbg( TRACE, "%s CMD <%s>", pclFunc, pclCmd );
            dbg( TRACE, "%s FLD <%s>", pclFunc, pclAftFields );
            dbg( TRACE, "%s DATA <%s>", pclFunc, pclAftData );
            dbg( TRACE, "%s SELECT <%s>", pclFunc, pclWhere );

            SendCedaEvent( igFlight, 0, mod_name, " ",  " ", " ", pclCmd, "AFTTAB", 
                           pclWhere, pclAftFields, pclAftData, " ", 3, NETOUT_NO_ACK );
            UpdateAtcRecord( "FIN", pclAftFields, pclAftData );
        }
		else
            UpdateAtcRecord( "DOF", " ", " " );
        return RC_SUCCESS;
    }

    if( rgDFLT.bgDeleteFlt == FALSE )
    {
        sprintf( pclAftFields, "%s", "FTYP" );
        sprintf( pclAftData, "%s", rgDFLT.pclSetFtyp );

        strcpy( pclCmd, "UFR" );
        dbg( TRACE, "%s CMD <%s>", pclFunc, pclCmd );
        dbg( TRACE, "%s FLD <%s>", pclFunc, pclAftFields );
        dbg( TRACE, "%s DATA <%s>", pclFunc, pclAftData );
        dbg( TRACE, "%s SELECT <%s>", pclFunc, pclWhere );

        SendCedaEvent( igFlight, 0, mod_name, " ",  " ", " ", pclCmd, "AFTTAB", 
                       pclWhere, pclAftFields, pclAftData, " ", 3, NETOUT_NO_ACK );
    }
    else
    {
        strcpy( pclCmd, "DFR" );
        dbg( TRACE, "%s CMD <%s>", pclFunc, pclCmd );
        strcpy( pclAftFields, " " );
        strcpy( pclAftData, " " );
        dbg( TRACE, "%s SELECT <%s>", pclFunc, pclWhere );

        SendCedaEvent( igFlight, 0, mod_name, " ",  " ", " ", pclCmd, "AFTTAB", 
                       pclWhere, " ", " ", " ", 3, NETOUT_NO_ACK );
    }
    UpdateAtcRecord( pclCmd, pclAftFields, pclAftData );
    return RC_SUCCESS;
}

static int CleanDB()
{
    int ilRc;
    char pclSqlBuf[1024] = "\0";
    char pclSqlData[512] = "\0";
    char pclNow[20] = "\0";
    char *pclFunc = "CleanDB";

    GetServerTimeStamp("UTC",1,0,pclNow);
    AddSecondsToCEDATime(pclNow,-igNumDaysInAtc*24*60*60,1);
    sprintf( pclSqlBuf, "DELETE FROM ATCTAB WHERE CDAT < '%s'", pclNow );
    ilRc = RunSQL( pclSqlBuf, pclSqlData );
    dbg( TRACE, "%s: ilRc <%d> SQL <%s>", pclFunc, ilRc, pclSqlBuf );
    return RC_SUCCESS;
}

static int SendBayInfo( char *pcpFields, char *pcpData, char *pcpUaft )
{
    int ilRc;
    int ilFoundATC;
    int ilItemNo;
    char pclSqlBuf[1024] = "\0";
    char pclSqlData[1024] = "\0";
    char pclMessage[128] = "\0";
    char pclUATC[20] = "\0";
    char pclTmpStr[512] = "\0";
    char *pclP;
    char *pclFunc = "SendBayInfo";

    dbg( DEBUG, "%s: FIELD <%s>", pclFunc, pcpFields );
    dbg( DEBUG, "%s: DATA <%s>", pclFunc, pcpData );
    /* This area is receiving bay info from FIPS or other sources */
    if( strcmp( pcpFields, " " ) )
    {
        memset( &rgATC, 0, sizeof(rgATC) );
        memset( &rgHDR, 0, sizeof(rgHDR) );
        memset( &rgMSG, 0, sizeof(rgMSG) );

        strcpy( rgATC.pcgUAFT, pcpUaft );
        if( ilRc == RC_FAIL )
        {
            dbg( TRACE, "%s: Invalid Bay Info. UAFT not found", pclFunc );
            return RC_FAIL;
        }

        /** Get ATC flight header */
        sprintf( pclSqlBuf, "SELECT TRIM(URNO),TRIM(ICSG),TRIM(ORG4),TRIM(IOFB) FROM ATCTAB WHERE UAFT = %s AND MGID <> 'GATE'"
                            "ORDER BY CDAT DESC", rgATC.pcgUAFT );
        ilFoundATC = RunSQL( pclSqlBuf, pclSqlData );
        dbg( TRACE, "%s: ilRc <%d> SQL <%s>", pclFunc, ilFoundATC, pclSqlBuf );
        if( ilFoundATC != DB_SUCCESS )
        {
            dbg( TRACE, "%s: ATC info not found in ATCTAB", pclFunc );
            if( rgINFO.bgBaySendWithoutATC == FALSE )
                return RC_SUCCESS;

            ExtractAFTData( pcpFields, pcpData, "ORG4", rgHDR.pcgORG4 );
            ExtractAFTData( pcpFields, pcpData, "CSGN", rgHDR.pcgCSGN );
            ExtractAFTData( pcpFields, pcpData, "OFBL", rgATC.pcgONOF );
            if( strlen(rgATC.pcgONOF) > 0 )
                sprintf( rgHDR.pcgEOBT, "%4.4s", &rgATC.pcgONOF[8] );
        }
        else
        {
            get_fld( pclSqlData,FIELD_1,STR,20,pclTmpStr ); 
            dbg( TRACE, "%s: Found in ATCTAB URNO <%s>", pclFunc, pclTmpStr );
            get_fld( pclSqlData,FIELD_2,STR,20,rgHDR.pcgCSGN ); 
            get_fld( pclSqlData,FIELD_3,STR,20,rgHDR.pcgORG4 );
            get_fld( pclSqlData,FIELD_4,STR,20,rgHDR.pcgEOBT ); 
        }
        strcpy( rgHDR.pcgACID, rgHDR.pcgCSGN );

        /* arrival */
        if( strcmp(rgHDR.pcgORG4, pcgHopo4) )
            ilRc = ExtractAFTData( pcpFields, pcpData, "PSTA", rgATC.pcgPKST );
        else
            ilRc = ExtractAFTData( pcpFields, pcpData, "PSTD", rgATC.pcgPKST );
    }
    else
        dbg( TRACE, "%s: =====> ATC msg received. Prepare Bay Info", pclFunc );

    dbg( DEBUG, "%s: ACID <%s> CSGN <%s>", pclFunc, rgHDR.pcgACID, rgHDR.pcgCSGN );
    dbg( DEBUG, "%s: ORG4 <%s>", pclFunc, rgHDR.pcgORG4 );
    dbg( DEBUG, "%s: EOBT <%s>", pclFunc, rgHDR.pcgEOBT );
    dbg( DEBUG, "%s: BAY <%s>", pclFunc, rgATC.pcgPKST );

    TrimSpace( rgATC.pcgPKST );
    if( strlen(rgATC.pcgPKST) <= 0 )
    {
        dbg( TRACE, "%s: No stand allocated at this moment", pclFunc );
        return RC_SUCCESS;
    }

    if( strlen(rgHDR.pcgACID) <= 0 )
    {
        dbg( TRACE, "%s: No callsign to send to ATC", pclFunc );
        return RC_SUCCESS;
    }
    if( strlen(rgHDR.pcgORG4) <= 0 )
    {
        dbg( TRACE, "%s: No origin to send to ATC", pclFunc );
        return RC_SUCCESS;
    }
    if( strlen(rgHDR.pcgEOBT) <= 0 )
    {
        dbg( TRACE, "%s: No estimated off block timing to send to ATC", pclFunc );
        return RC_SUCCESS;
    }
    GetServerTimeStamp("UTC",1,0,rgATC.pcgCDAT);
    strcpy( rgATC.pcgUSEC, mod_name );
    strcpy( rgHDR.pcgMsgID, "GATE" );

    sprintf( pclMessage, "GATE%-8.8s%-4.4s%-4.4s%-4.4s", rgHDR.pcgACID, rgHDR.pcgORG4, rgHDR.pcgEOBT, rgATC.pcgPKST );
    dbg( TRACE, "%s: Msg to ATC <%s>", pclFunc, pclMessage );
    SendCedaEvent( igWmqAtc, 0, mod_name, " ",  " ", " ", "ATC", " ", " ", " ", pclMessage, " ", 3, NETOUT_NO_ACK );

    InsertAtcRecord( "OUT", pcpFields, pcpData, pclMessage );

    return RC_SUCCESS;
}

static int ExtractAFTData( char *pcpFields, char *pcpData, char *pcpFieldReqd, char *pcpResult )
{
    int ilRc;
    int ilItemNo;
    char *pclFunc = "ExtractAFTData";

    ilItemNo = get_item_no( pcpFields, pcpFieldReqd, 5 ) + 1;
    if( ilItemNo > 0 )
    { 
        get_real_item( pcpResult, pcpData, ilItemNo ); 
        dbg( DEBUG, "%s: <%s> <%s>", pclFunc, pcpFieldReqd, pcpResult ); 
        return RC_SUCCESS;
    }
    return RC_FAIL;
}

/* BaseTime : UFIS Date Time format, 14 char */
/* MsgTime: format is specified as pcpFormat eg. HHMM */
/* ResultDate: UFIS Date Time format, 14 char */
static int GuessTheDate( char *pcpBaseDateTime, char *pcpMsgTime, char *pcpResultDate, char *pcpFormat )
{
    int ilRc;
    int ilMinimumMin, ilDiffMin;
    char pclTmpStr[256] = "\0";
    char pclBaseTime[20] = "\0";
    char pclBaseDateTime[20] = "\0";
    char pclOneDayBefore[20] = "\0";
    char pclOneDayAfter[20] = "\0";
    char pclToday[20] = "\0";
    char *pclFunc = "GuessTheDate";

    strcpy( pcpResultDate, " " );
    if( !strcmp( pcpFormat, "HHMM" ) )
    {
        sprintf( pclToday, "%8.8s%4.4s00", pcpBaseDateTime, pcpMsgTime );
       /* dbg( TRACE, "%s: Today <%s>", pclFunc, pclToday );*/
    }
    strcpy( pclOneDayAfter, pclToday );
    AddSecondsToCEDATime(pclOneDayAfter,SECONDS_PER_DAY,1);
    strcpy( pclOneDayBefore, pclToday );
    AddSecondsToCEDATime(pclOneDayBefore,-SECONDS_PER_DAY,1);
    /*dbg( TRACE, "%s: OneDayBefore <%s> OneDayAfter <%s>", pclFunc, pclOneDayBefore, pclOneDayAfter );*/

   
    strcpy( pcpResultDate, " " );
    ilMinimumMin = 999999999;
    UTCTimeDiff(pclToday, pcpBaseDateTime, &ilDiffMin); ilDiffMin = abs(ilDiffMin);
    dbg( DEBUG, "%s: [TODAY] <%s> ilDiffMin <%d>", pclFunc, pclToday, ilDiffMin );
    if( ilDiffMin < ilMinimumMin )
    {
        strcpy( pcpResultDate, pclToday );
        ilMinimumMin = ilDiffMin;
    }
    UTCTimeDiff(pclOneDayBefore, pcpBaseDateTime, &ilDiffMin);  ilDiffMin = abs(ilDiffMin);  
    dbg( DEBUG, "%s: [ONE_DAY_BEFORE] <%s> ilDiffMin <%d>", pclFunc, pclOneDayBefore, ilDiffMin );
    if( ilDiffMin < ilMinimumMin )
    {
        strcpy( pcpResultDate, pclOneDayBefore );
        ilMinimumMin = ilDiffMin;
    }
    UTCTimeDiff(pclOneDayAfter, pcpBaseDateTime, &ilDiffMin); ilDiffMin = abs(ilDiffMin);
    dbg( DEBUG, "%s: [ONE_DAY_AFTER] <%s> ilDiffMin <%d>", pclFunc, pclOneDayAfter, ilDiffMin );
    if( ilDiffMin < ilMinimumMin )
    {
        strcpy( pcpResultDate, pclOneDayAfter );
        ilMinimumMin = ilDiffMin;
    }
    dbg( TRACE, "%s: RESULT OF GUESS <%s>", pclFunc, pcpResultDate );
    return RC_SUCCESS;
}

int TrimSpace( char *pcpInStr )
{
    int ili = 0;
    int ilLen;
    char *pclOutStr = NULL;
    char *pclP;
    char *pclFunc = "TrimSpace";

    ilLen = strlen( pcpInStr );
    if( ilLen <= 0 )
        return;

    pclOutStr = (char *)malloc(ilLen + 10);
    pclP = pcpInStr;

    /* Trim front spaces */
    while( pclP && *pclP == ' ' )
        pclP++;

    while( *pclP )
    {
       pclOutStr[ili++] = *pclP;
       if( *pclP != ' ' )
           pclP++;
       else
       {
           while( *pclP == ' ' )
               pclP++;
       }
    }
    /* Trim back space */
    if( pclOutStr[ili-1] == ' ' )
        ili--;
    pclOutStr[ili] = '\0';
    strcpy( pcpInStr, pclOutStr );
    if( pclOutStr != NULL )
        free( (char *)pclOutStr );
}


/********************************************************************
 Calculate Time (YYYYMMDDHHMISS) Difference between 2 UTC time (UFIS date time format)
 Return the number of minutes
********************************************************************/
static int UTCTimeDiff ( char *pcpTimeStr1, char *pcpTimeStr2, int *pipMinDiff )
{       
    time_t tlTime1, tlTime2;
    char pclTmpStr[128];
    char *pclFunc = "UTCTimeDiff";
            
    if( strlen(pcpTimeStr1) != 14 || strlen(pcpTimeStr2) != 14 )
        return RC_FAIL;

    /*dbg( DEBUG, "%s: Time Strings <%s> - <%s>", pclFunc, pcpTimeStr1, pcpTimeStr2 );   */
    GetTimeValue( pcpTimeStr1, &tlTime1 );
    GetTimeValue( pcpTimeStr2, &tlTime2 );
    *pipMinDiff = (tlTime1 - tlTime2) / SECONDS_PER_MINUTE;
    /*dbg( DEBUG, "%s: Time Diff <%d>", pclFunc, *pipMinDiff );   */
    return RC_SUCCESS;
}

static int GetTimeValue ( char *pcpTimeStr, time_t *ptpTimeValue )
{
    struct tm rlTm;
    time_t tlNow, tlTimeValue;
    char pclTmpStr[128] = "\0";
    char *pclFunc = "GetTimeValue";

    tlNow = time(0);
    gmtime_r( &tlNow, &rlTm );
    
    sprintf( pclTmpStr, "%4.4s", pcpTimeStr );     rlTm.tm_year = atoi(pclTmpStr) - 1900;
    sprintf( pclTmpStr, "%2.2s", &pcpTimeStr[4] ); rlTm.tm_mon  = atoi(pclTmpStr) - 1;
    sprintf( pclTmpStr, "%2.2s", &pcpTimeStr[6] ); rlTm.tm_mday = atoi(pclTmpStr);
    sprintf( pclTmpStr, "%2.2s", &pcpTimeStr[8] ); rlTm.tm_hour = atoi(pclTmpStr);
    sprintf( pclTmpStr, "%2.2s", &pcpTimeStr[10] );rlTm.tm_min  = atoi(pclTmpStr);
    sprintf( pclTmpStr, "%2.2s", &pcpTimeStr[12] );rlTm.tm_sec  = atoi(pclTmpStr);
    tlTimeValue = mktime( (struct tm *)&rlTm );
   /* dbg( DEBUG, "%s: Time Value <%d>", pclFunc, tlTimeValue ); */
    *ptpTimeValue = tlTimeValue;
    return RC_SUCCESS;
}        

static int FormatCsgnFlno( char *pcpCSGN, char *pcpFLNO)
{
    int blCanFormatCsgn = TRUE;
    int ilRc;
    int ili, ilj, ilLen;
    int ilCount;
    int ilFltn;
    char clFlns;
    char pclAlc2[8] = "\0";
    char pclAlc3[8] = "\0";
    char pclCsgn[20] = "\0";
    char pclFlno[20] = "\0";
    char pclFltn[512] = "\0";
    char *pclFunc = "FormatCsgnFlno";

    strcpy( pcpFLNO, " " );
    blCanFormatCsgn = TRUE;
    ili = 2;
    if( isdigit(pcpCSGN[2]) )
    {
        memcpy( pclAlc2, pcpCSGN, 2 );
        ilRc = syslibSearchDbData( "ALTTAB","ALC2",pclAlc2,"ALC3",pclAlc3,&ilCount,"\n"); TrimSpace(pclAlc3);
    }
    else
    {
        ili = 3;
        memcpy( pclAlc3, pcpCSGN, 3 );
        ilRc = syslibSearchDbData( "ALTTAB","ALC3",pclAlc3,"ALC2",pclAlc2,&ilCount,"\n"); TrimSpace(pclAlc2);
    }
    if( ilRc != RC_SUCCESS ) 
    {
        dbg( DEBUG, "%s: ALC 2<%s> OR 3<%s> not found in basic data", pclFunc, pclAlc2, pclAlc3 );
        blCanFormatCsgn = FALSE;
    }
    else
    {
        ilLen = strlen(pcpCSGN);
        for( ili, ilj = 0; ili < ilLen; ili++ )
        {
            if( !isdigit(pcpCSGN[ili]) && (ili != ilLen-1) ) /* cater for suffix as well */
            {
                blCanFormatCsgn = FALSE;
                break;
            }
            pclFltn[ilj++] = pcpCSGN[ili];
        }
    }

    if( blCanFormatCsgn == FALSE )
    {
        strcpy( pcpFLNO, " " );
        return RC_FAIL;
    }
    if( !isdigit(pcpCSGN[ilLen-1]) )
        clFlns = pcpCSGN[ilLen-1];
    ilFltn = atoi(pclFltn);
    if( ilFltn > 0 )
    {
        sprintf( pcpCSGN, "%-3.3s%3.3d%c", pclAlc3, ilFltn, clFlns );   
        strcpy( pcpFLNO, pcpCSGN );
        if( strlen(pclAlc2) > 0 )
        {
            sprintf( pcpFLNO, "%-3.3s%3.3d%c", pclAlc2, ilFltn, clFlns );
            if( strlen(pclAlc3) <= 0 )
                sprintf( pcpCSGN, "%-2.2s%3.3d%c", pclAlc2, ilFltn, clFlns );   
        }
        strcpy( rgATC.pcgALC2, pclAlc2 );
        strcpy( rgATC.pcgALC3, pclAlc3 );
    }
    dbg( TRACE, "%s: Complete! CSGN = <%s> FLNO = <%s>", pclFunc, pcpCSGN, pcpFLNO );
    return RC_SUCCESS;
} /* FormatCsgnFlno */

 
