#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/AUH/AUH_Server/Base/Server/Interface/erphdl.c.2.12 2013/07/30 16:00:00 won Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :                                                           */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */
/* 20120906 FYA v0.01: Creation                                               */
/* 20121102 WON v0.02: Baseline 3 commands adh, atc, sch                      */
/* 20121105 WON v0.03: Fixed bugs for adh, sch, dly, ufa, ufs.                */
/*                     Commented atc bec. it will be part of billing          */
/* 20121203 WON v0.04: Completed by Frank.									  */
/* 2012				                                                          */
/*					   Only adhoc table will have adho='X' others adho!='X'	  */
/*                     Change DLY to SCHD command                             */
/*                     Revise VIAL value.                                     */
/*                     Corrected Fields based on Jira 2617                    */
/*                                                                            */
/* 20130312 WON V0.07: Change SCHD to DLY command.                            */
/* 20130403 WON V0.08: ERPDLY for departure flights only                      */
/*					   For Pax tables, 0 should be the value for " " or NULL  */
/* 20130408 WON V0.09: Revised DLY to SCHD and totally revised logic of       */
/*                     getting the records.                                   */
/* 20130425 WON v0.10  ERPADH flow to include ADHO = 'X' OR PAID <> 'K'       */
/*                     flights                                                */ 
/*                     Change UFA to UADH                                     */
/*                     All ADHO!='X' removed.                                 */ 
/* 20130507 WON V0.11:	Revised ERPSCH flow in relation to join/split (Jira   */
/*                      2261)                                                 */
/* 20130527 WON V1.12:  Fixed Monday to Monday issue (esp. when date is       */
/*                      Monday)                                               */
/* 20130528 WON V1.13:  Fixed populateERPSCH.  If stat='R' or 'F' on DB,      */
/*                      dont change.                                          */
/*                      Fixed field mapping on populateERPSCH.                */
/* 20130529 WON V1.14   Insert only 1 record in erpbill when its a rotation   */
/*                      Modified populateERPADH to correct mapping            */
/*                      Make sure that LAND is not empty before inserting in  */
/*                      ERPADH                                                */
/*                      Change erpadh airline name to afttab.blto             */
/*                                                                            */
/* 20130601 WON V1.15   Changes BILL command.                                 */
/* 20130620 WON V2.00	Finalize everything.  Remove the commented part       */
/*						sending to FLIGHT in doAllERPBILLTables				  */
/* 20130624 WON V2.01	Make sure both flights are adhoc before processing as */
/*                      adhoc Flights.                                        */
/*                      For ERPBIL.  Make sure to get Departure Flight Type   */
/*                      and Terminal in rotation flight.                      */
/* 20130625 WON V2.02   In searching FPETAB, added two more fields REGN and   */
/*                      ACT5                                                  */
/*                      Convert special character to readable character in    */
/*                      erpsch alfn.                                          */
/* 20130626 WON V2.03	Corrected ERPSCH.  Arrival should have null DAP3 and  */
/*                      and DAP4 while Departure should have null SAP3 and    */
/*                      SAP4                                                  */
/*                      Corrected erpsch and make sure ftyp is null or        */
/*                      CANCELLED only                                        */
/* 20130703 WON V2.04	Corrected the call in ERPBILL to flight.  Instead of  */
/*                      DQC, it will be DQF                                   */
/*                      Make sure in erpbill both dqcs are "P"                */
/*                      ERPPXR must only have rows when STOP='Transit'        */
/* 20130710 WON V2.05	Corrected fields in erppxh DES3,ORG3,ACT3.            */
/* 20130710 WON V2.06	All adhoc flights must also have entries in erpatc    */
/*                      when BILL is run                                      */
/*                      ERPBILL->SCHE = NO for ADHOC, otherwise YES           */
/*                      If flno is empty get CSGN on erp  tables.             */
/*                      Do not select alfn if alc2=' '                        */
/* 20130723 WON V2.07   jira AUHADIA-309. Properly close open cursors.        */
/* 20130725 WON V2.08   To resolve issues regarding deleted flight in ERPSCH  */
/* 20130722 WON V2.09   Jira no. UFIS-3848                                    */
/*                      (1) Handling arrival only flights. Needs to fill up   */
/*                          erpatc.                                           */
/*                      (2) Handling departure and rotation flights and send  */
/*                          everything. And freeze the flight(s).             */
/*                      (3) Added new command SNDQ                            */
/*                      (4) Revise processDeleteERPSCH to send erpsch.urno    */
/*                          to queue                                          */
/* 20130804 WON V2.10   (1) Jira AUHADIA-190 - "AODB is to send Adult pax     */
/*                          value ONLY when Male and Female data is not       */
/*                          available. Do not transmit all three.             */
/* 20130819 WON V2.11   In rotation flight, make sure that ERPATC for arrival */
/*                      is populated.  As of the moment, if Flight is         */
/*                      rotation, it populates only DEPARTURE erpatc due to   */
/*                      handling of arrival flight                            */
/* 20130820 WON V2.12   (1) ALFN is from departure flight nor arrival         */
/*                      (2) For MOPA in erpadh, make sure CASH flight be      */
/*                          be considered as CASH.                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */
static char sccs_version[] ="@(#) UFIS (c) ABB AAT/I erphdl.c 45.1.0 / "__DATE__" "__TIME__" / VBL";
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "db_if.h"
#include "urno_fn.h"
#include "erphdl.h"


/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp       = NULL;*/

int  debug_level = TRACE;
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static int   igInitOK      = FALSE;
static char  cgConfigFile[512];
static long lgEvtCnt = 0;
char pcgBillUrno[16];
REC_DESC rgRecDesc;

/******************************************************************************/
/* Function prototypes                                                          */
/******************************************************************************/
static int Init_Process();
static int ReadConfigEntry(char *pcpFname, char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer);
static int    Reset(void);                       /* Reset program          */
static void    Terminate(void);                   /* Terminate program      */
static void    HandleSignal(int);                 /* Handles signals        */
static void    HandleErr(int);                    /* Handles general errors */
static void    HandleQueErr(int);                 /* Handles queuing errors */
static int  HandleData(EVENT *prpEvent);       /* Handles event data     */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/
int formatVIAL(int ipCount, char *pcpIn, char *pcpOut, char *pcpSeparator);
int reprocessERPSCHRecords(char *pcpYesterday, char *pcpMonday, char *pcpNextMonday, char *pclDateString);
int processAdditionalERPSCHRecords(char *pcpYesterday, char *pcpMonday, char *pcpNextMonday, char *pclDateString);
int processERPSCH(char *pcpRkey, char *pcpDateString);
int processDeleteERPSCH(char *pcpUrno);
int getSqlResult(char *pcpData, char *pcpSql, char *pcpFields, int ipFields, int ipWithQuotes);
int processUSCHCmd(char *pcpFlnu, int ipFtypFlag);
/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/


MAIN
{
    int    ilRc = RC_SUCCESS;            /* Return code            */
    int    ilCnt = 0;
    
    INITIALIZE;            /* General initialization    */
    
    dbg(TRACE,"MAIN: version <%s>",sccs_version);
    /* Attach to the MIKE queues */
    do
    {
        ilRc = init_que();
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        }/* end of if */
    }while((ilCnt < 10) && (ilRc != RC_SUCCESS));
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
        sleep(60);
        exit(1);
    }else{
        dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }/* end of if */

    do
    {
        ilRc = init_db();
        if (ilRc != RC_SUCCESS)
        {
            check_ret(ilRc);
            dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        } /* end of if */
    } while((ilCnt < 10) && (ilRc != RC_SUCCESS));

    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
        sleep(60);
        exit(2);
    }else{
        dbg(TRACE,"MAIN: init_db() OK!");
    } /* end of if */
    
    /* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */
    sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
    dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
    ilRc = TransferFile(cgConfigFile);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */
    
    sprintf(cgLoadConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),"load");
    dbg(TRACE,"cgLoadConfigFile <%s>",cgLoadConfigFile);
    ilRc = TransferFile(cgLoadConfigFile);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgLoadConfigFile);
    } /* end of if */

    ilRc = SendRemoteShutdown(mod_id);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    } /* end of if */
    if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
        dbg(DEBUG,"MAIN: waiting for status switch ...");
        HandleQueues();
    }/* end of if */
    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
        dbg(TRACE,"MAIN: initializing ...");
        if(igInitOK == FALSE)
        {
            ilRc = Init_Process();
            if(ilRc != RC_SUCCESS)
            {
                dbg(TRACE,"Init_Process: init failed!");
            } /* end of if */
        }/* end of if */
    } else {
        Terminate();
    }/* end of if */
    
    InitFieldIndex();
    
    dbg(TRACE,"=====================");
    dbg(TRACE,"MAIN: initializing OK");
    dbg(TRACE,"=====================");
		    
    for(;;)
    {
        ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* Acknowledge the item */
        ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
        if( ilRc != RC_SUCCESS ) 
        {
            /* handle que_ack error */
            HandleQueErr(ilRc);
        } /* fi */
        
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text;
                
        if( ilRc == RC_SUCCESS )
        {
            lgEvtCnt++;
            
            /* Acknowledge the item */
            ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRc != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRc);
            } /* fi */
            
            switch( prgEvent->command )
            {
            case    HSB_STANDBY    :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;    
            case    HSB_COMING_UP    :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;    
            case    HSB_ACTIVE    :
                ctrl_sta = prgEvent->command;
                break;    
            case    HSB_ACT_TO_SBY    :
                ctrl_sta = prgEvent->command;
                /* CloseConnection(); */
                HandleQueues();
                break;    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate();
                break;    
            case    HSB_STANDALONE    :
                ctrl_sta = prgEvent->command;

                break;    

            case    SHUTDOWN    :
                /* process shutdown - maybe from uutil */
                Terminate();
                break;
                    
            case    RESET        :
                ilRc = Reset();
                break;
                    
            case    EVENT_DATA    :
                if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
                {        
                        dbg(TRACE,"process name       <%s>",argv[0]);
                    ilRc = HandleData(prgEvent);
                    if(ilRc != RC_SUCCESS)
                    {
                        HandleErr(ilRc);
                    }/* end of if */
                }else{
                    dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
                    DebugPrintItem(TRACE,prgItem);
                    DebugPrintEvent(TRACE,prgEvent);
                }/* end of if */
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
            default            :
                dbg(TRACE,"MAIN: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        } else {
            /* Handle queuing errors */
            HandleQueErr(ilRc);
        } /* end else */
        
    } /* end for */
    
    exit(0);
    
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int Init_Process()
{
    int		ilRc = RC_SUCCESS;            /* Return code */
    char	pclSelection[iMAX_BUF_SIZE] = "\0";
    char	*pclFunc = "Init_Process";
    int ilTmp;
    
    //Frank v0.1
    int ilNoSec = 0;
    char pclDssn[1024]="\0";
    char pclType[32]="\0";
    char pclStyp[32]="\0";
    char pclSstp[32]="\0";
    char pclSsst[32]="\0";
    char pclApc3[32]="\0"; 
    char pclTmp[iMAX_BUF_SIZE]="\0"; 
    char pclUfisConfigFile[1024];
    char aclClieBuffer[1024];
    char aclServBuffer[1024];
    char aclBuffer[1024];
    
    /* now reading from configfile or from database */
    SetSignals(HandleSignal);
    igInitOK = TRUE;
    
    if(ilRc == RC_SUCCESS)
    {
        /* read HomeAirPort from SGS.TAB */
        ilRc = tool_search_exco_data("SYS", "HOMEAP", cgHopo);
        if (ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"%s EXTAB,SYS,HOMEAP not found in SGS.TAB",pclFunc);
            Terminate();
        } else {
            dbg(TRACE,"%s home airport <%s>",pclFunc,cgHopo);
        }
    }

	//DEBUG_LEVEL
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "MAIN","DEBUG_LEVEL",pclSelection);
    if(ilRc == RC_SUCCESS)
    {    
        if( !strncmp( pclSelection, "DEBUG", 5 ) )
        {
            debug_level = DEBUG;
        }
        else
        {
            debug_level = TRACE;
        }
    }    

    if( debug_level == DEBUG)
    {
        dbg(TRACE,"%s debug_level is DEBUG",pclFunc);
    } 
    else
    {
        dbg(TRACE,"%s debug_level is TRACE",pclFunc);
        debug_level = TRACE;
    }
    
    /* Get Client and Server chars */
    sprintf(pclUfisConfigFile,"%s/ufis_ceda.cfg",getenv("CFG_PATH"));
    ilRc = iGetConfigEntry(pclUfisConfigFile, "CHAR_PATCH", "CLIENT_CHARS",
                           CFG_STRING, aclClieBuffer);
    if (ilRc == RC_SUCCESS)
    {
    	int i, j;
    	
        ilRc = iGetConfigEntry(pclUfisConfigFile, "CHAR_PATCH", "SERVER_CHARS",
                               CFG_STRING, aclServBuffer);
        if (ilRc == RC_SUCCESS)
        {
            i = GetNoOfElements(aclClieBuffer,',');
            if ( i == GetNoOfElements(aclServBuffer,','))
            {
                memset(pcgServerChars,0x00,100*sizeof(char));
                memset(pcgClientChars,0x00,100*sizeof(char));
                for ( j=0; j < i; j++)
                {
                    GetDataItem(aclBuffer,aclServBuffer,j+1,',',"","\0\0");
                    pcgServerChars[j*2] = atoi(aclBuffer);
                    pcgServerChars[j*2+1] = ',';
                }
                pcgServerChars[(j-1)*2+1] = 0x00;
                for ( j=0; j < i; j++)
                {
                    GetDataItem(aclBuffer,aclClieBuffer,j+1,',',"","\0\0");
                    pcgClientChars[j*2] = atoi(aclBuffer);
                    pcgClientChars[j*2+1] = ',';
                }
                pcgClientChars[(j-1)*2+1] = 0x00;
                dbg (DEBUG,"New Clientchars <%s> dec <%s>",pcgClientChars,aclClieBuffer);
                dbg (DEBUG,"New Serverchars <%s> dec <%s>",pcgServerChars,aclServBuffer);
                dbg (DEBUG," Serverchars <%d> ",strlen(pcgServerChars));
                dbg (DEBUG,"%s  und count <%d> ",aclBuffer,strlen(pcgServerChars));
                dbg (DEBUG,"i <%d>",i);
            }
        } 
        else 
        {
            ilRc = RC_SUCCESS;
            dbg(DEBUG,"Use standard (old) serverchars");
        }
    } 
    else 
    {
        dbg(DEBUG,"Use standard (old) serverchars");
        ilRc = RC_SUCCESS;
    }
    
   if (ilRc != RC_SUCCESS)
   {
      strcpy(pcgClientChars,"\042\047\054\012\015");
      strcpy(pcgServerChars,"\260\261\262\263\263");
      ilRc = RC_SUCCESS;
   }

    ilRc=getERPBILLConfig();
    if ( ilRc==RC_FAIL )
    {
        Terminate();	
    }
    printERPBILLConfig();

    ilRc=getERPSCHConfig();
    if ( ilRc==RC_FAIL )
    {
        Terminate();	
    }
    printERPSCHConfig();    
    
    ilRc=getERPATCConfig();
    if ( ilRc==RC_FAIL )
    {
        Terminate();	
    }
    printERPATCConfig();

    ilRc=getERPPXHConfig();
    if ( ilRc==RC_FAIL )
    {
        Terminate();	
    }
    printERPPXHConfig();

    ilRc=getERPPXRConfig();
    if ( ilRc==RC_FAIL )
    {
        Terminate();	
    }
    printERPPXRConfig();

    ilRc=getERPPXMConfig();
    if ( ilRc==RC_FAIL )
    {
        Terminate();	
    }
    printERPPXMConfig();

    ilRc=getERPPXOConfig();
    if ( ilRc==RC_FAIL )
    {
        Terminate();	
    }
    printERPPXOConfig();

    ilRc=getERPPXTConfig();
    if ( ilRc==RC_FAIL )
    {
        Terminate();	
    }
    printERPPXTConfig();

    
    /*--- for wmerp and flight queue number   ---*/
    ilRc = ReadConfigEntry(cgConfigFile, "MAIN","LOAD_CONFIG", pclSelection);
    if ( strcmp(pclSelection, "") )
        strcpy(pcgLoadFile, pclSelection);   
    dbg(DEBUG, "Init_Process:: LOAD_CONFIG in Config[%s], Final LOAD_CONFIG[%s]", 
        pclSelection, pcgLoadFile);
    getLoadConfig();
    printLoadConfig();
      
    ilRc = ReadConfigEntry(cgConfigFile, "MAIN","DUPLICATE_FOLDER", pclSelection);
    if ( strcmp(pclSelection, "") )
        strcpy(pcgDuplicateFolder, pclSelection);   
    dbg(DEBUG, "Init_Process:: DUPLICATE_FOLDER in Config[%s], Final DUPLICATE_FOLDER[%s]", 
        pclSelection, pcgDuplicateFolder);    
        
    ilRc = ReadConfigEntry(cgConfigFile, "MAIN","IMPLEMENTATION_DATE", pclSelection);
    if ( strcmp(pclSelection, "") )
        strcpy(pcgImplementationDate, pclSelection);   
    else
        strcpy(pcgImplementationDate, "20130722000000");
    dbg(DEBUG, "Init_Process:: IMPLEMENTATION_DATE in Config[%s], Final IMPLEMENTATION_DATE[%s]", 
        pclSelection, pcgImplementationDate);

    ilRc = ReadConfigEntry(cgConfigFile, "MAIN","NULL_FOLDER", pclSelection);
    if ( strcmp(pclSelection, "") )
        strcpy(pcgNullFolder, pclSelection);   
    dbg(DEBUG, "Init_Process:: NULL_FOLDER in Config[%s], Final NULL_FOLDER[%s]", 
        pclSelection, pcgNullFolder);

    ilRc = ReadConfigEntry(cgConfigFile, "MAIN","TIMEZONE", pclSelection);
    if ( strcmp(pclSelection, "") )
        igTimeZone=atoi(pclSelection); 
    dbg(DEBUG, "Init_Process:: Timezone in Config[%s], Final TimeZone[%d]", 
        pclSelection, igTimeZone);

    ilRc = ReadConfigEntry(cgConfigFile, "MAIN","WMERPB_QUEUE", pclSelection);
    if ( strcmp(pclSelection, "") )
        igWMERPBQueue=atoi(pclSelection); 
    else
    {
        ilTmp=tool_get_q_id ("wmerpb");   
        if ( ilTmp>0 )
            igWMERPBQueue=ilTmp;    	
    }           
    dbg(DEBUG, "Init_Process:: WMERPB_QUEUE in Config[%s], tool_get_q_id[%d] Final ProcessID[%d]", 
        pclSelection, ilTmp, igWMERPBQueue);
                
    ilRc = ReadConfigEntry(cgConfigFile, "MAIN","WMERPA_QUEUE", pclSelection);
    if ( strcmp(pclSelection, "") )
        igWMERPAQueue=atoi(pclSelection);   
    else
    {
        ilTmp=tool_get_q_id ("wmerpa");   
        if ( ilTmp>0 )
            igWMERPAQueue=ilTmp;        	
    }     
    dbg(DEBUG, "Init_Process:: WMERPA_QUEUE in Config[%s], tool_get_q_id[%d] Final ProcessID[%d]", 
        pclSelection, ilTmp, igWMERPAQueue);

    ilRc = ReadConfigEntry(cgConfigFile, "MAIN","WMERPS_QUEUE", pclSelection);
    if ( strcmp(pclSelection, "") )
        igWMERPSQueue=atoi(pclSelection);      
    else
    {
        ilTmp=tool_get_q_id ("wmerps");   
        if ( ilTmp>0 )
            igWMERPSQueue=ilTmp;    	
    }  
    
    dbg(DEBUG, "Init_Process:: WMERPS_QUEUE in Config[%s], tool_get_q_id[%d], ProcessID[%d]", 
        pclSelection, ilTmp, igWMERPSQueue);

    ilRc = ReadConfigEntry(cgConfigFile, "MAIN","FLIGHT_QUEUE", pclSelection);
    if ( strcmp(pclSelection, "") )
        igFlightQueue=atoi(pclSelection);      
    else
    {
        ilTmp=tool_get_q_id ("flight");   
        if ( ilTmp>0 )
            igFlightQueue=ilTmp;    	
    }  
    dbg(DEBUG, "Init_Process:: FLIGHT_QUEUE in Config[%s], tool_get_q_id[%d], ProcessID[%d]", 
        pclSelection, ilTmp, igFlightQueue);      

    ilRc = ReadConfigEntry(cgConfigFile, "MAIN","ERPADH_QUEUE", pclSelection);
    if ( strcmp(pclSelection, "") )
        igERPADHQueue=atoi(pclSelection);   
    else
    {
        ilTmp=tool_get_q_id ("erpadh");   
        if ( ilTmp>0 )
            igERPADHQueue=ilTmp;        	
    }     
    dbg(DEBUG, "Init_Process:: ERPADH_QUEUE in Config[%s], tool_get_q_id[%d] Final ProcessID[%d]", 
        pclSelection, ilTmp, igERPADHQueue);



                  
    //--------------[DAILY_SCHEDULE]--------------
    //STOA
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "DAILY_SCHEDULE","STOA",pclSelection);
    if( ilRc != RC_SUCCESS )
    {
         dbg(TRACE,"%s Daily STOA not found ,STOA use default <MONDAY,0600,MONDAY+1,0600>",pclFunc);
         memset(rgTM_Daily.pcpSTOAConfig,0,sizeof(rgTM_Daily.pcpSTOAConfig));
         strcpy(rgTM_Daily.pcpSTOAConfig,"MONDAY,0600,MONDAY+1,0600");
    }
    else
    {
         strcpy(rgTM_Daily.pcpSTOAConfig,pclSelection);
    }
    
    //SELECTION_CRITERIA
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "DAILY_SCHEDULE","SELECTION_CRITERIA",pclSelection);
    if( ilRc != RC_SUCCESS )
		{
		  dbg(TRACE,"%s SELECTION_CRITERIA not found",pclFunc);
		  rgTM_Daily.CommCfgPart.iNoOfSections_SelCri == 0;
			dbg(TRACE,"%s rgTM_Daily.CommCfgPart.iNoOfSections_SelCri == 0",pclFunc);
		}
		else
    {
        ilRc = FillCfgStructSelCri(pclSelection,&(rgTM_Daily.CommCfgPart),"DAILY_SCHEDULE");
        if( ilRc == RC_FAIL)
        {
            rgTM_Daily.CommCfgPart.iNoOfSections_SelCri == 0;
            dbg(TRACE,"%s rgTM_Daily.CommCfgPart.iNoOfSections_SelCri == 0",pclFunc);
        }
    }
    
    //ROTATION_RECORDS
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "DAILY_SCHEDULE","ROTATION_RECORDS",pclSelection);
    if( ilRc != RC_SUCCESS )
		{
		   dbg(TRACE,"%s Daily ROTATION_RECORDS not found",pclFunc);
		   strcpy(rgTM_Daily.CommCfgPart.pcpRotationConfig,"NO");
		   dbg(TRACE,"%s rgTM_Daily.CommCfgPart.pcpRotationConfigAdHoc == \"NO\"",pclFunc);
		}
		else
    {
        strcpy(rgTM_Daily.CommCfgPart.pcpRotationConfig,pclSelection);
    }
    
    //FLIGHT_CAT
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "DAILY_SCHEDULE","FLIGHT_CAT",pclSelection);
    if( ilRc != RC_SUCCESS )
		{
			dbg(TRACE,"%s FLIGHT_CAT not found<%s>",pclFunc);
			rgTM_Daily.CommCfgPart.iNoOfSections_FltCat == 0;
			dbg(TRACE,"%s rgTM_Daily.CommCfgPart.iNoOfSections_FltCat == 0",pclFunc);
		}
		else
    {
        ilRc =FillCfgStructFltCat(pclSelection,&(rgTM_Daily.CommCfgPart),"DAILY_SCHEDULE");
        if( ilRc == RC_FAIL)
        {
            rgTM_Daily.CommCfgPart.iNoOfSections_SelCri == 0;
            dbg(TRACE,"%s rgTM_Daily.CommCfgPart.iNoOfSections_FltCat == 0",pclFunc);
        }
    }
    
    //DEST_TABLE
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "DAILY_SCHEDULE","DEST_TABLE",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s DEST_TABLE not found",pclFunc);
           Terminate();
       }
       else
    {
        strcpy(rgTM_Daily.CommCfgPart.pcpDestTableConfig,pclSelection);
    }
    
    //RECV_CMD
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "DAILY_SCHEDULE","RECV_CMD",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s RECV_CMD not found",pclFunc);
           Terminate();
       }
       else
    {
        strcpy(rgTM_Daily.CommCfgPart.pcpRecvCmdConfig,pclSelection);
    }
    //[DAILY_SCHEDULE_END]
    
    //--------------[ATC_FLIGHT_MOVEMENT]--------------
    //LAND
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "ATC_FLIGHT_MOVEMENT","LAND",pclSelection);
    if( ilRc != RC_SUCCESS )
    {
         dbg(TRACE,"%s Month LAND not found ,LAND use default <MONTH-1,000000,MONTH,000000>",pclFunc);
         memset(rgTM_Month.pcpLANDConfig,0,sizeof(rgTM_Month.pcpLANDConfig));
         strcpy(rgTM_Month.pcpLANDConfig,"MONTH-1,000000,MONTH,000000");
    }
    else
    {
         strcpy(rgTM_Month.pcpLANDConfig,pclSelection);
    }
    
    //AIRB
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "ATC_FLIGHT_MOVEMENT","AIRB",pclSelection);
    if( ilRc != RC_SUCCESS )
    {
         dbg(TRACE,"%s Month AIRB not found ,AIRB use default <MONTH-1,000000,MONTH,000000>",pclFunc);
         memset(rgTM_Month.pcpAIRBConfig,0,sizeof(rgTM_Month.pcpAIRBConfig));
         strcpy(rgTM_Month.pcpAIRBConfig,"MONTH-1,000000,MONTH,000000");
    }
    else
    {
         strcpy(rgTM_Month.pcpAIRBConfig,pclSelection);
    }
    
    //SELECTION_CRITERIA
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "ATC_FLIGHT_MOVEMENT","SELECTION_CRITERIA",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s Month SELECTION_CRITERIA not found",pclFunc);
           rgTM_Month.CommCfgPart.iNoOfSections_SelCri == 0;
        dbg(TRACE,"%s rgTM_Month.CommCfgPart.iNoOfSections_SelCri == 0",pclFunc);
       }
       else
    {
        ilRc = FillCfgStructSelCri(pclSelection,&(rgTM_Month.CommCfgPart),"ATC_FLIGHT_MOVEMENT");
        if( ilRc == RC_FAIL)
        {
            rgTM_Month.CommCfgPart.iNoOfSections_SelCri == 0;
            dbg(TRACE,"%s rgTM_Month.CommCfgPart.iNoOfSections_SelCri == 0",pclFunc);
        }
    }
    
    //ROTATION_RECORDS
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "ATC_FLIGHT_MOVEMENT","ROTATION_RECORDS",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s Month ROTATION_RECORDS not found",pclFunc);
           strcpy(rgTM_Month.CommCfgPart.pcpRotationConfig,"NO");
           dbg(TRACE,"%s rgTM_Month.CommCfgPart.pcpRotationConfigAdHoc == \"NO\"",pclFunc);
       }
       else
    {
        strcpy(rgTM_Month.CommCfgPart.pcpRotationConfig,pclSelection);
    }
    
    //FLIGHT_CAT
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "ATC_FLIGHT_MOVEMENT","FLIGHT_CAT",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s Month FLIGHT_CAT not found",pclFunc);
           rgTM_Month.CommCfgPart.iNoOfSections_FltCat == 0;
        dbg(TRACE,"%s rgTM_Month.CommCfgPart.iNoOfSections_FltCat == 0",pclFunc);
       }
       else
    {
        ilRc =FillCfgStructFltCat(pclSelection,&(rgTM_Month.CommCfgPart),"ATC_FLIGHT_MOVEMENT");
        if( ilRc == RC_FAIL)
        {
            rgTM_Month.CommCfgPart.iNoOfSections_FltCat == 0;
            dbg(TRACE,"%s rgTM_Month.CommCfgPart.iNoOfSections_FltCat == 0",pclFunc);
        }
    }
    
    //DEST_TABLE
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "ATC_FLIGHT_MOVEMENT","DEST_TABLE",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s Month DEST_TABLE not found",pclFunc);
           Terminate();
       }
       else
    {
        strcpy(rgTM_Month.CommCfgPart.pcpDestTableConfig,pclSelection);
    }
    
    //RECV_CMD
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "ATC_FLIGHT_MOVEMENT","RECV_CMD",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s Month RECV_CMD not found",pclFunc);
           Terminate();
       }
       else
    {
        strcpy(rgTM_Month.CommCfgPart.pcpRecvCmdConfig,pclSelection);
    }
    //--------------[ATC_FLIGHT_MOVEMENT_END]--------------
    
    //--------------[PAX_ADHOC]--------------
    //SELECTION_CRITERIA
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "PAX_ADHOC","SELECTION_CRITERIA",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s AdHoc SELECTION_CRITERIA not found",pclFunc);
           rgTM_AdHoc.CommCfgPart.iNoOfSections_SelCri == 0;
        dbg(TRACE,"%s rgTM_AdHoc.CommCfgPart.iNoOfSections_SelCri == 0",pclFunc);
       }
       else
    {
        ilRc =FillCfgStructSelCri(pclSelection,&(rgTM_AdHoc.CommCfgPart),"PAX_ADHOC");
        if( ilRc == RC_FAIL)
        {
            rgTM_AdHoc.CommCfgPart.iNoOfSections_SelCri == 0;
            dbg(TRACE,"%s rgTM_AdHoc.CommCfgPart.iNoOfSections_SelCri == 0",pclFunc);
        }
    }
    
    //ROTATION_RECORDS
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "PAX_ADHOC","ROTATION_RECORDS",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s AdHoc ROTATION_RECORDS not found",pclFunc);
           strcpy(rgTM_AdHoc.CommCfgPart.pcpRotationConfig,"NO");
           dbg(TRACE,"%s rgTM_AdHoc.CommCfgPart.pcpRotationConfigAdHoc == \"NO\"",pclFunc);
       }
       else
    {
        strcpy(rgTM_AdHoc.CommCfgPart.pcpRotationConfig,pclSelection);
    }
    
    //DEST_TABLE
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "PAX_ADHOC","DEST_TABLE",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s AdHoc DEST_TABLE not found",pclFunc);
           Terminate();
       }
       else
    {
        strcpy(rgTM_AdHoc.CommCfgPart.pcpDestTableConfig,pclSelection);
    }
    
    //RECV_CMD
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "PAX_ADHOC","RECV_CMD",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s AdHoc RECV_CMD not found",pclFunc);
           Terminate();
       }
       else
    {
        strcpy(rgTM_AdHoc.CommCfgPart.pcpRecvCmdConfig,pclSelection);
    }
    //--------------[PAX_ADHOC_END]--------------
    
    //--------------[AIR_BILL_HANDLING]--------------
    //LAND
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "AIR_BILL_HANDLING","LAND",pclSelection);
    if( ilRc != RC_SUCCESS )
    {
         dbg(TRACE,"%s BILL LAND not found ,LAND use default <DAY-2,000000,DAY-1,000000>",pclFunc);
         memset(rgTM_Bill.pcpLANDConfig,0,sizeof(rgTM_Bill.pcpLANDConfig));
         strcpy(rgTM_Bill.pcpLANDConfig,"DAY-2,000000,DAY-1,000000");
    }
    else
    {
         strcpy(rgTM_Bill.pcpLANDConfig,pclSelection);
    }
    
    //AIRB
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "AIR_BILL_HANDLING","AIRB",pclSelection);
    if( ilRc != RC_SUCCESS )
    {
         dbg(TRACE,"%s Bill AIRB not found ,AIRB use default <DAY-2,000000,DAY-1,000000>",pclFunc);
         memset(rgTM_Bill.pcpAIRBConfig,0,sizeof(rgTM_Bill.pcpAIRBConfig));
         strcpy(rgTM_Bill.pcpAIRBConfig,"DAY-2,000000,DAY-1,000000");
    }
    else
    {
         strcpy(rgTM_Bill.pcpAIRBConfig,pclSelection);
    }
    
    //SELECTION_CRITERIA
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "AIR_BILL_HANDLING","SELECTION_CRITERIA",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s Bill SELECTION_CRITERIA not found",pclFunc);
           rgTM_Bill.CommCfgPart.iNoOfSections_SelCri == 0;
        dbg(TRACE,"%s rgTM_Bill.CommCfgPart.iNoOfSections_SelCri == 0",pclFunc);
       }
       else
    {
    	ilRc = FillCfgStructSelCri(pclSelection,&(rgTM_Bill.CommCfgPart),"AIR_BILL_HANDLING");
        if( ilRc == RC_FAIL)
        {
            rgTM_Bill.CommCfgPart.iNoOfSections_SelCri == 0;
            dbg(TRACE,"%s rgTM_Bill.CommCfgPart.iNoOfSections_SelCri == 0",pclFunc);
        }
    }
    
    //ROTATION_RECORDS
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "AIR_BILL_HANDLING","ROTATION_RECORDS",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s Bill ROTATION_RECORDS not found",pclFunc);
           Terminate();
       }
       else
    {
        strcpy(rgTM_Bill.CommCfgPart.pcpRotationConfig,pclSelection);
    }
    
    //FLIGHT_CAT
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "AIR_BILL_HANDLING","FLIGHT_CAT",pclSelection);
    if( ilRc != RC_SUCCESS )
    {
        dbg(TRACE,"%s Bill FLIGHT_CAT not found",pclFunc);
        rgTM_Bill.CommCfgPart.iNoOfSections_FltCat == 0;
        dbg(TRACE,"%s rgTM_Bill.CommCfgPart.iNoOfSections_FltCat == 0",pclFunc);
    }
    else
    {
        ilRc =FillCfgStructFltCat(pclSelection,&(rgTM_Bill.CommCfgPart),"AIR_BILL_HANDLING");
        if( ilRc == RC_FAIL)
        {
            rgTM_Bill.CommCfgPart.iNoOfSections_FltCat == 0;
            dbg(TRACE,"%s rgTM_Bill.CommCfgPart.iNoOfSections_FltCat == 0",pclFunc);
        }
    }
    
    //FLIGHT_TYPE
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "AIR_BILL_HANDLING","FLIGHT_TYPE",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s Bill FLIGHT_TYPE not found",pclFunc);
           rgTM_Bill.CommCfgPart.iNoOfSections_FltTyp == 0;
        dbg(TRACE,"%s rgTM_Bill.iNoOfSections_FltTyp == 0",pclFunc);
       }
       else
    {
        ilRc = FillCfgStructBillFltTyp(pclSelection,&rgTM_Bill);
        if( ilRc == RC_FAIL)
        {
            rgTM_Bill.CommCfgPart.iNoOfSections_FltTyp == 0;
            dbg(TRACE,"%s rgTM_Bill.iNoOfSections_FltTyp == 0",pclFunc);
        }
    }
    
    //Modified
    //DB_FLDS
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "AIR_BILL_HANDLING","DB_FLDS",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s Bill DB_FLDS not found",pclFunc);
           memset(rgTM_Bill.LoadCfg.pclDbFldsName,0,sizeof(rgTM_Bill.LoadCfg.pclDbFldsName));
        dbg(TRACE,"%s rgTM_Bill.LoadCfg.pclDbFldsName is null",pclFunc);
       }
       else
    {
        strncpy(rgTM_Bill.LoadCfg.pclDbFldsName,pclSelection,strlen(pclSelection));
        dbg(TRACE,"%s rgTM_Bill.LoadCfg.pclDbFldsName<%s>",pclFunc,rgTM_Bill.LoadCfg.pclDbFldsName);
    }
      
    //LOAD_FLDS
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "AIR_BILL_HANDLING","LOAD_FLDS",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s Bill LOAD_FLDS not found",pclFunc);
           memset(rgTM_Bill.LoadCfg.pclLoadFldsCriName,0,sizeof(rgTM_Bill.LoadCfg.pclLoadFldsCriName));
        dbg(TRACE,"%s rgTM_Bill.LoadCfg.pclLoadFldsCriName is null",pclFunc);
       }
       else
    {
        strncpy(rgTM_Bill.LoadCfg.pclLoadFldsCriName,pclSelection,strlen(pclSelection));
        dbg(TRACE,"%s rgTM_Bill.LoadCfg.pclLoadFldsCriName<%s>",pclFunc,rgTM_Bill.LoadCfg.pclLoadFldsCriName);
        
        ilRc = iGetConfigEntryWOSec(cgLoadConfigFile,rgTM_Bill.LoadCfg.pclLoadFldsCriName,CFG_STRING,rgTM_Bill.LoadCfg.pclLoadFldsCri);
        
        if( ilRc == OK )
        {
            dbg(DEBUG,"%s rgTM_Bill.LoadCfg.pclLoadFldsCri<%s>",pclFunc,rgTM_Bill.LoadCfg.pclLoadFldsCri);
            
            ExtractLoaWhereClauseValues(rgTM_Bill.LoadCfg.pclLoadFldsCri, pclDssn, pclType,pclStyp, pclSstp, pclSsst,pclApc3);
            
            FormatDssnFldCfg(pclDssn);
                CollectAllDssnWORepeat(pclDssn);
            
            dbg(DEBUG,"%s pclDssn<%s>, pclType<%s>,pclStyp<%s>, pclSstp<%s>, pclSsst<%s>,pclApc3<%s>",pclFunc,pclDssn, pclType,pclStyp, pclSstp, pclSsst,pclApc3);
            
              strcpy(rgTM_Bill.LoadCfg.ConcreteCfg.pclDssn, pclDssn);
              strcpy(rgTM_Bill.LoadCfg.ConcreteCfg.pclType, pclType);
              strcpy(rgTM_Bill.LoadCfg.ConcreteCfg.pclStyp, pclStyp);
              strcpy(rgTM_Bill.LoadCfg.ConcreteCfg.pclSstp, pclSstp);
              strcpy(rgTM_Bill.LoadCfg.ConcreteCfg.pclSsst, pclSsst);
              strcpy(rgTM_Bill.LoadCfg.ConcreteCfg.pclApc3, pclApc3);
            
            dbg(DEBUG,"%s rgTM_Bill.LoadCfg.ConcreteCfg.pclDssnList<%s>, rgTM_Bill.LoadCfg.ConcreteCfg.pclType<%s>,rgTM_Bill.LoadCfg.ConcreteCfg.pclStyp<%s>, rgTM_Bill.LoadCfg.ConcreteCfg.pclSstp<%s>, rgTM_Bill.LoadCfg.ConcreteCfg.pclSsst<%s>",pclFunc,rgTM_Bill.LoadCfg.ConcreteCfg.pclDssn, rgTM_Bill.LoadCfg.ConcreteCfg.pclType,rgTM_Bill.LoadCfg.ConcreteCfg.pclStyp, rgTM_Bill.LoadCfg.ConcreteCfg.pclSstp, rgTM_Bill.LoadCfg.ConcreteCfg.pclSsst,rgTM_Bill.LoadCfg.ConcreteCfg.pclApc3);
        }
        else
        {
            dbg(TRACE,"iGetConfigEntryWOSec fails, return");
            return ilRc;
        }
    }
       rgTM_Bill.ilCount=1;
       
    //DEST_TABLE
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "AIR_BILL_HANDLING","DEST_TABLE",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s Bill DEST_TABLE not found",pclFunc);
           Terminate();
       }
       else
    {
        strcpy(rgTM_Bill.CommCfgPart.pcpDestTableConfig,pclSelection);
    }
    
    //RECV_CMD
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "AIR_BILL_HANDLING","RECV_CMD",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s Bill RECV_CMD not found",pclFunc);
           Terminate();
       }
       else
    {
        strcpy(rgTM_Bill.CommCfgPart.pcpRecvCmdConfig,pclSelection);
    }
    //--------------[AIR_BILL_HANDLING_END]--------------
    
    //--------------[PAX_HEADER]--------------
    //SELECTION_CRITERIA
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "PAX_HEADER","SELECTION_CRITERIA",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s PaxHeader SELECTION_CRITERIA not found",pclFunc);
           rgTM_Bill.CommCfgPart.iNoOfSections_SelCri == 0;
        dbg(TRACE,"%s rgTM_PaxHeader.CommCfgPart.iNoOfSections_SelCri == 0",pclFunc);
       }
       else
    {
        ilRc = FillCfgStructSelCri(pclSelection,&(rgTM_PaxHeader.CommCfgPart),"PAX_HEADER");
        if( ilRc == RC_FAIL)
        {
            rgTM_PaxHeader.CommCfgPart.iNoOfSections_SelCri == 0;
            dbg(TRACE,"%s rgTM_PaxHeader.CommCfgPart.iNoOfSections_SelCri == 0",pclFunc);
        }
    }
    
    //ROTATION_RECORDS
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "PAX_HEADER","ROTATION_RECORDS",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s PaxHeader ROTATION_RECORDS not found",pclFunc);
           Terminate();
       }
       else
    {
        strcpy(rgTM_PaxHeader.CommCfgPart.pcpRotationConfig,pclSelection);
    }
    
    //FLIGHT_CAT
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "PAX_HEADER","FLIGHT_CAT",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s PaxHeader FLIGHT_CAT not found",pclFunc);
           rgTM_PaxHeader.CommCfgPart.iNoOfSections_FltCat == 0;
        dbg(TRACE,"%s rgTM_PaxHeader.CommCfgPart.iNoOfSections_FltCat == 0",pclFunc);
       }
       else
    {
        ilRc = FillCfgStructFltCat(pclSelection,&(rgTM_PaxHeader.CommCfgPart),"PAX_HEADER");
        if( ilRc == RC_FAIL)
        {
            rgTM_PaxHeader.CommCfgPart.iNoOfSections_FltCat == 0;
            dbg(TRACE,"%s rgTM_PaxHeader.CommCfgPart.iNoOfSections_FltCat == 0",pclFunc);
        }
    }
    
    //DB_FLDS & LOAD_FLDS
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "PAX_HEADER","DB_FLDS",pclSelection);
       if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s PaxHeader DB_FLDS not found",pclFunc);
           rgTM_PaxHeader.SLoadCfg = NULL;
        dbg(TRACE,"%s rgTM_PaxHeader.SLoadCfg is NULL",pclFunc);
       }
       else
    {
        ilRc = ReadConfigEntry( cgConfigFile, "PAX_HEADER","LOAD_FLDS",pclTmp);
        if( ilRc == RC_SUCCESS )
        {
            ilRc = FillCfgStructDB_LOAD_flds(pclSelection,pclTmp,&rgTM_PaxHeader,"PAX_HEADER");
        }
        else
        {
            dbg(TRACE,"%s PaxHeader LOAD_FLDS not found",pclFunc);
            rgTM_PaxHeader.SLoadCfg = NULL;
               dbg(TRACE,"%s rgTM_PaxHeader.SLoadCfg is NULL",pclFunc);
        }
    }
    
    //DEST_TABLE
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "PAX_HEADER","DEST_TABLE",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s PaxHeader DEST_TABLE not found",pclFunc);
           Terminate();
       }
       else
    {
        strcpy(rgTM_PaxHeader.CommCfgPart.pcpDestTableConfig,pclSelection);
    }
    
    //RECV_CMD
    /*
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "PAX_HEADER","RECV_CMD",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s PaxHeader RECV_CMD not found",pclFunc);
           Terminate();
       }
       else
    {
        strcpy(rgTM_PaxHeader.pcpRecvCmdConfig,pclSelection);
    }
    */
    //--------------[PAX_HEADER_END]--------------
    
    //--------------[PAX_TRANSFER]--------------
    //
    //SELECTION_CRITERIA
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "PAX_TRANSFER","SELECTION_CRITERIA",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s PaxTransfer SELECTION_CRITERIA not found",pclFunc);
           rgTM_PaxTransfer.CommCfgPart.iNoOfSections_SelCri == 0;
        dbg(TRACE,"%s rgTM_PaxTransfer.CommCfgPart.iNoOfSections_SelCri == 0",pclFunc);
       }
       else
    {
        ilRc = FillCfgStructSelCri(pclSelection,&(rgTM_PaxTransfer.CommCfgPart),"PAX_TRANSFER");
        if( ilRc == RC_FAIL)
        {
            rgTM_PaxTransfer.CommCfgPart.iNoOfSections_SelCri == 0;
            dbg(TRACE,"%s rgTM_PaxTransfer.CommCfgPart.iNoOfSections_SelCri == 0",pclFunc);
        }
    }
    
    //ROTATION_RECORDS
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "PAX_TRANSFER","ROTATION_RECORDS",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s PaxTransfer ROTATION_RECORDS not found",pclFunc);
           Terminate();
       }
       else
    {
        strcpy(rgTM_PaxTransfer.CommCfgPart.pcpRotationConfig,pclSelection);
    }
    
    //FLIGHT_CAT
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "PAX_TRANSFER","FLIGHT_CAT",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s PaxTransfer FLIGHT_CAT not found",pclFunc);
           rgTM_PaxTransfer.CommCfgPart.iNoOfSections_FltCat == 0;
        dbg(TRACE,"%s rgTM_PaxTransfer.CommCfgPart.iNoOfSections_FltCat == 0",pclFunc);
       }
       else
    {
        ilRc = FillCfgStructFltCat(pclSelection,&(rgTM_PaxTransfer.CommCfgPart),"PAX_TRANSFER");
        if( ilRc == RC_FAIL)
        {
            rgTM_PaxTransfer.CommCfgPart.iNoOfSections_FltCat == 0;
            dbg(TRACE,"%s rgTM_PaxTransfer.CommCfgPart.iNoOfSections_FltCat == 0",pclFunc);
        }
    }
    
    //DB_FLDS & LOAD_FLDS
    memset(pclSelection,0,sizeof(pclSelection));
    memset(pclTmp,0,sizeof(pclTmp));
    ilRc = ReadConfigEntry( cgConfigFile, "PAX_TRANSFER","DB_FLDS",pclSelection);
       if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s PaxTransfer DB_FLDS not found",pclFunc);
           rgTM_PaxHeader.SLoadCfg = NULL;
        dbg(TRACE,"%s rgTM_PaxTransfer.SLoadCfg is NULL",pclFunc);
       }
       else
    {
        ilRc = ReadConfigEntry( cgConfigFile, "PAX_TRANSFER","LOAD_FLDS",pclTmp);
        if( ilRc == RC_SUCCESS )
        {
            ilRc = FillCfgStructDB_LOAD_flds(pclSelection,pclTmp,&rgTM_PaxTransfer,"PAX_TRANSFER");
        }
        else
        {
            dbg(TRACE,"%s PaxTransfer LOAD_FLDS not found",pclFunc);
            rgTM_PaxTransfer.SLoadCfg = NULL;
               dbg(TRACE,"%s rgTM_PaxTransfer.SLoadCfg is NULL",pclFunc);
        }
    }
    
    //DEST_TABLE
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "PAX_TRANSFER","DEST_TABLE",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s PaxTransfer DEST_TABLE not found",pclFunc);
           Terminate();
       }
       else
    {
        strcpy(rgTM_PaxTransfer.CommCfgPart.pcpDestTableConfig,pclSelection);
    }
    //
    //--------------[PAX_TRANSFER_END]--------------
    
    //--------------[PAX_MULTI_ROUTE]--------------
    //
    //SELECTION_CRITERIA
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "PAX_MULTI_ROUTE","SELECTION_CRITERIA",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s PaxMultiRoute SELECTION_CRITERIA not found",pclFunc);
           rgTM_PaxMultiRoute.CommCfgPart.iNoOfSections_SelCri == 0;
        dbg(TRACE,"%s rgTM_PaxMultiRoute.CommCfgPart.iNoOfSections_SelCri == 0",pclFunc);
       }
       else
    {
        ilRc = FillCfgStructSelCri(pclSelection,&(rgTM_PaxMultiRoute.CommCfgPart),"PAX_MULTI_ROUTE");
        if( ilRc == RC_FAIL)
        {
            rgTM_PaxMultiRoute.CommCfgPart.iNoOfSections_SelCri == 0;
            dbg(TRACE,"%s rgTM_PaxMultiRoute.CommCfgPart.iNoOfSections_SelCri == 0",pclFunc);
        }
    }
    
    //ROTATION_RECORDS
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "PAX_MULTI_ROUTE","ROTATION_RECORDS",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s PaxMultiRoute ROTATION_RECORDS not found",pclFunc);
           Terminate();
       }
       else
    {
        strcpy(rgTM_PaxMultiRoute.CommCfgPart.pcpRotationConfig,pclSelection);
    }
    
    //FLIGHT_CAT
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "PAX_MULTI_ROUTE","FLIGHT_CAT",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s PaxMultiRoute FLIGHT_CAT not found",pclFunc);
           rgTM_PaxMultiRoute.CommCfgPart.iNoOfSections_FltCat == 0;
        dbg(TRACE,"%s rgTM_PaxMultiRoute.CommCfgPart.iNoOfSections_FltCat == 0",pclFunc);
       }
       else
    {
        ilRc = FillCfgStructFltCat(pclSelection,&(rgTM_PaxMultiRoute.CommCfgPart),"PAX_MULTI_ROUTE");
        if( ilRc == RC_FAIL)
        {
            rgTM_PaxMultiRoute.CommCfgPart.iNoOfSections_FltCat == 0;
            dbg(TRACE,"%s rgTM_PaxMultiRoute.CommCfgPart.iNoOfSections_FltCat == 0",pclFunc);
        }
    }
    
    //DB_FLDS & LOAD_FLDS
    memset(pclSelection,0,sizeof(pclSelection));
    memset(pclTmp,0,sizeof(pclTmp));
    ilRc = ReadConfigEntry( cgConfigFile, "PAX_MULTI_ROUTE","DB_FLDS",pclSelection);
       if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s PaxMultiRoute DB_FLDS not found",pclFunc);
           rgTM_PaxMultiRoute.SLoadCfg = NULL;
        dbg(TRACE,"%s rgTM_PaxMultiRoute.SLoadCfg is NULL",pclFunc);
       }
       else
    {
        ilRc = ReadConfigEntry( cgConfigFile, "PAX_MULTI_ROUTE","LOAD_FLDS",pclTmp);
        if( ilRc == RC_SUCCESS )
        {
            ilRc = FillCfgStructDB_LOAD_flds(pclSelection,pclTmp,&rgTM_PaxMultiRoute,"PAX_MULTI_ROUTE");
        }
        else
        {
            dbg(TRACE,"%s PaxMultiRoute LOAD_FLDS not found",pclFunc);
            rgTM_PaxTransfer.SLoadCfg = NULL;
               dbg(TRACE,"%s PaxMultiRoute.SLoadCfg is NULL",pclFunc);
        }
    }
    
    //DEST_TABLE
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "PAX_MULTI_ROUTE","DEST_TABLE",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s PaxMultiRoute DEST_TABLE not found",pclFunc);
           Terminate();
       }
       else
    {
        strcpy(rgTM_PaxMultiRoute.CommCfgPart.pcpDestTableConfig,pclSelection);
    }
    //
    //--------------[PAX_MULTI_ROUTE_END]--------------
    
    //--------------[PAX_TRANSIT]--------------
    //
    //SELECTION_CRITERIA
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "PAX_TRANSIT","SELECTION_CRITERIA",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s PaxTransit SELECTION_CRITERIA not found",pclFunc);
           rgTM_PaxTransit.CommCfgPart.iNoOfSections_SelCri == 0;
        dbg(TRACE,"%s rgTM_PaxTransit.CommCfgPart.iNoOfSections_SelCri == 0",pclFunc);
       }
       else
    {
        ilRc = FillCfgStructSelCri(pclSelection,&(rgTM_PaxTransit.CommCfgPart),"PAX_TRANSIT");
        if( ilRc == RC_FAIL)
        {
            rgTM_PaxTransit.CommCfgPart.iNoOfSections_SelCri == 0;
            dbg(TRACE,"%s rgTM_PaxTransit.CommCfgPart.iNoOfSections_SelCri == 0",pclFunc);
        }
    }
    
    //ROTATION_RECORDS
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "PAX_TRANSIT","ROTATION_RECORDS",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s PaxTransit ROTATION_RECORDS not found",pclFunc);
           Terminate();
       }
       else
    {
        strcpy(rgTM_PaxTransit.CommCfgPart.pcpRotationConfig,pclSelection);
    }
    
    //FLIGHT_CAT
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "PAX_TRANSIT","FLIGHT_CAT",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s PaxTransit FLIGHT_CAT not found",pclFunc);
           rgTM_PaxTransit.CommCfgPart.iNoOfSections_FltCat == 0;
        dbg(TRACE,"%s rgTM_PaxTransit.CommCfgPart.iNoOfSections_FltCat == 0",pclFunc);
       }
       else
    {
        ilRc = FillCfgStructFltCat(pclSelection,&(rgTM_PaxTransit.CommCfgPart),"PAX_TRANSIT");
        if( ilRc == RC_FAIL)
        {
            rgTM_PaxTransit.CommCfgPart.iNoOfSections_FltCat == 0;
            dbg(TRACE,"%s rgTM_PaxTransit.CommCfgPart.iNoOfSections_FltCat == 0",pclFunc);
        }
    }
    
    //DB_FLDS & LOAD_FLDS
    memset(pclSelection,0,sizeof(pclSelection));
    memset(pclTmp,0,sizeof(pclTmp));
    ilRc = ReadConfigEntry( cgConfigFile, "PAX_TRANSIT","DB_FLDS",pclSelection);
       if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s PaxTransit DB_FLDS not found",pclFunc);
           rgTM_PaxTransit.SLoadCfg = NULL;
        dbg(TRACE,"%s rgTM_PaxTransit.SLoadCfg is NULL",pclFunc);
       }
       else
    {
        ilRc = ReadConfigEntry( cgConfigFile, "PAX_TRANSIT","LOAD_FLDS",pclTmp);
        if( ilRc == RC_SUCCESS )
        {
            ilRc = FillCfgStructDB_LOAD_flds(pclSelection,pclTmp,&rgTM_PaxTransit,"PAX_TRANSIT");
        }
        else
        {
            dbg(TRACE,"%s PaxTransit LOAD_FLDS not found",pclFunc);
            rgTM_PaxTransit.SLoadCfg = NULL;
               dbg(TRACE,"%s PaxTransit.SLoadCfg is NULL",pclFunc);
        }
    }
    
    //DEST_TABLE
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "PAX_TRANSIT","DEST_TABLE",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s PaxTransit DEST_TABLE not found",pclFunc);
           Terminate();
       }
       else
    {
        strcpy(rgTM_PaxTransit.CommCfgPart.pcpDestTableConfig,pclSelection);
    }
    //
    //--------------[PAX_TRANSIT_END]--------------
    
    //--------------[BILL_DELAY]--------------
    //SELECTION_CRITERIA
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "BILL_DELAY","SELECTION_CRITERIA",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s BillDelay SELECTION_CRITERIA not found",pclFunc);
           rgTM_BillDelay.CommCfgPart.iNoOfSections_SelCri == 0;
        dbg(TRACE,"%s rgTM_BillDelay.CommCfgPart.iNoOfSections_SelCri == 0",pclFunc);
       }
       else
    {
        ilRc = FillCfgStructSelCri(pclSelection,&(rgTM_BillDelay.CommCfgPart),"BILL_DELAY");
        if( ilRc == RC_FAIL)
        {
            rgTM_BillDelay.CommCfgPart.iNoOfSections_SelCri == 0;
            dbg(TRACE,"%s rgTM_BillDelay.CommCfgPart.iNoOfSections_SelCri == 0",pclFunc);
        }
    }
    
    //ROTATION_RECORDS
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "BILL_DELAY","ROTATION_RECORDS",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s BillDelay ROTATION_RECORDS not found",pclFunc);
           Terminate();
       }
       else
    {
        strcpy(rgTM_BillDelay.CommCfgPart.pcpRotationConfig,pclSelection);
    }
    
    //FLIGHT_CAT
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "BILL_DELAY","FLIGHT_CAT",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s BillDelay FLIGHT_CAT not found",pclFunc);
           rgTM_BillDelay.CommCfgPart.iNoOfSections_FltCat == 0;
        dbg(TRACE,"%s rgTM_BillDelay.CommCfgPart.iNoOfSections_FltCat == 0",pclFunc);
       }
       else
    {
        ilRc = FillCfgStructFltCat(pclSelection,&(rgTM_BillDelay.CommCfgPart),"BILL_DELAY");
        if( ilRc == RC_FAIL)
        {
            rgTM_BillDelay.CommCfgPart.iNoOfSections_FltCat == 0;
            dbg(TRACE,"%s rgTM_BillDelay.CommCfgPart.iNoOfSections_FltCat == 0",pclFunc);
        }
    }
    
    //DB_FLDS & LOAD_FLDS
    memset(pclSelection,0,sizeof(pclSelection));
    memset(pclTmp,0,sizeof(pclTmp));
    ilRc = ReadConfigEntry( cgConfigFile, "BILL_DELAY","DB_FLDS",pclSelection);
       if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s BillDelay DB_FLDS not found",pclFunc);
           rgTM_BillDelay.SLoadCfg = NULL;
        dbg(TRACE,"%s rgTM_BillDelay.SLoadCfg is NULL",pclFunc);
       }
       else
    {
        ilRc = ReadConfigEntry( cgConfigFile, "BILL_DELAY","LOAD_FLDS",pclTmp);
        if( ilRc == RC_SUCCESS )
        {
            ilRc = FillCfgStructDB_LOAD_flds(pclSelection,pclTmp,&rgTM_BillDelay,"PAX_TRANSIT");
        }
        else
        {
            dbg(TRACE,"%s BillDelay LOAD_FLDS not found",pclFunc);
            rgTM_BillDelay.SLoadCfg = NULL;
               dbg(TRACE,"%s BillDelay.SLoadCfg is NULL",pclFunc);
        }
    }
    
    //DEST_TABLE
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "BILL_DELAY","DEST_TABLE",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s BillDelay DEST_TABLE not found",pclFunc);
           Terminate();
       }
       else
    {
        strcpy(rgTM_BillDelay.CommCfgPart.pcpDestTableConfig,pclSelection);
    }
    //--------------[BILL_DELAY_END]--------------
    
    //--------------[PAX_OFFLOAD]--------------
    //SELECTION_CRITERIA
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "PAX_OFFLOAD","SELECTION_CRITERIA",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s OffLoad SELECTION_CRITERIA not found",pclFunc);
           rgTM_OffLoad.CommCfgPart.iNoOfSections_SelCri == 0;
        dbg(TRACE,"%s rgTM_OffLoad.CommCfgPart.iNoOfSections_SelCri == 0",pclFunc);
       }
       else
    {
        ilRc = FillCfgStructSelCri(pclSelection,&(rgTM_OffLoad.CommCfgPart),"PAX_OFFLOAD");
        if( ilRc == RC_FAIL)
        {
            rgTM_OffLoad.CommCfgPart.iNoOfSections_SelCri == 0;
            dbg(TRACE,"%s rgTM_OffLoad.CommCfgPart.iNoOfSections_SelCri == 0",pclFunc);
        }
    }
    
    //ROTATION_RECORDS
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "PAX_OFFLOAD","ROTATION_RECORDS",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s OffLoad ROTATION_RECORDS not found",pclFunc);
           Terminate();
       }
       else
    {
        strcpy(rgTM_OffLoad.CommCfgPart.pcpRotationConfig,pclSelection);
    }
    
    //FLIGHT_CAT
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "PAX_OFFLOAD","FLIGHT_CAT",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s OffLoad FLIGHT_CAT not found",pclFunc);
           rgTM_OffLoad.CommCfgPart.iNoOfSections_FltCat == 0;
        dbg(TRACE,"%s rgTM_OffLoad.CommCfgPart.iNoOfSections_FltCat == 0",pclFunc);
       }
       else
    {
        ilRc = FillCfgStructFltCat(pclSelection,&(rgTM_OffLoad.CommCfgPart),"PAX_OFFLOAD");
        if( ilRc == RC_FAIL)
        {
            rgTM_OffLoad.CommCfgPart.iNoOfSections_FltCat == 0;
            dbg(TRACE,"%s rgTM_OffLoad.CommCfgPart.iNoOfSections_FltCat == 0",pclFunc);
        }
    }
    
    //DB_FLDS & LOAD_FLDS
    memset(pclSelection,0,sizeof(pclSelection));
    memset(pclTmp,0,sizeof(pclTmp));
    ilRc = ReadConfigEntry( cgConfigFile, "PAX_OFFLOAD","DB_FLDS",pclSelection);
       if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s OffLoad DB_FLDS not found",pclFunc);
           rgTM_OffLoad.SLoadCfg = NULL;
        dbg(TRACE,"%s rgTM_OffLoad.SLoadCfg is NULL",pclFunc);
       }
       else
    {
        ilRc = ReadConfigEntry( cgConfigFile, "PAX_OFFLOAD","LOAD_FLDS",pclTmp);
        if( ilRc == RC_SUCCESS )
        {
            ilRc = FillCfgStructDB_LOAD_flds(pclSelection,pclTmp,&rgTM_BillDelay,"PAX_TRANSIT");
        }
        else
        {
            dbg(TRACE,"%s OffLoad LOAD_FLDS not found",pclFunc);
            rgTM_OffLoad.SLoadCfg = NULL;
               dbg(TRACE,"%s rgTM_OffLoad.SLoadCfg is NULL",pclFunc);
        }
    }
    
    //DEST_TABLE
    memset(pclSelection,0,sizeof(pclSelection));
    ilRc = ReadConfigEntry( cgConfigFile, "PAX_OFFLOAD","DEST_TABLE",pclSelection);
    if( ilRc != RC_SUCCESS )
       {
           dbg(TRACE,"%s OffLoad DEST_TABLE not found",pclFunc);
           Terminate();
       }
       else
    {
        strcpy(rgTM_OffLoad.CommCfgPart.pcpDestTableConfig,pclSelection);
    }
    //--------------[PAX_OFFLOAD_END]--------------
    
    dbg(DEBUG,"pcgAllDssn<%s>",pcgAllDssn);
    
    return(ilRc);
} /* end of initialize */

static int FillCfgStructBillFltTyp(char *pcpSelection, BillMain *prpBillMain)
{
    char    *pclFunc = "FillCfgStructBillFltTyp";
    char  *pclS = NULL;
    UINT  ilCurrentSection = 0;
    char    pclCurrentSection[iMAX_BUF_SIZE] = "\0";
    char    pclSelection[iMAX_BUF_SIZE] = "\0";
    int     ilSubOption = 0;
    int     ilCount = 0;
    char     *pclTmp = NULL;
    int     ilRc = RC_SUCCESS;
    
    if(strlen(pcpSelection) == 0)
    {
        dbg(TRACE,"%s pcpSelection is null",pclFunc);
        return RC_FAIL;
    }
    else
    {
        /* convert to uppercase */
        StringUPR((UCHAR*)pcpSelection);
        
        /* get number of currently used sections... */
        (*prpBillMain).CommCfgPart.iNoOfSections_FltTyp = GetNoOfElements(pcpSelection, ',');
        dbg(DEBUG,"%s iNoOfSections_FltTyp<%d>",pclFunc,(*prpBillMain).CommCfgPart.iNoOfSections_FltTyp);
        
        /* memory for section */
        if(((*prpBillMain).CommCfgPart.prSection_FltTyp = (ESection*)malloc((*prpBillMain).CommCfgPart.iNoOfSections_FltTyp*sizeof(ESection))) == NULL)
    {
            dbg(TRACE,"%s can't run without memory (3)",pclFunc);
            (*prpBillMain).CommCfgPart.prSection_FltTyp = NULL;
            return RC_FAIL;
    }
        
    // start reading sub section configurations
        for(ilCurrentSection=0; ilCurrentSection < (*prpBillMain).CommCfgPart.iNoOfSections_FltTyp; ilCurrentSection++)
        {
            /* clear memory */
            if((pclS = GetDataField(pcpSelection, ilCurrentSection, ',')) == NULL)
            {
                dbg(TRACE,"%s can't read current section number %d in section MAIN", pclFunc, ilCurrentSection);
                Terminate();
            }
            
            /* store current section */
            memset((void*)pclCurrentSection, 0x00, iMAX_BUF_SIZE);
            strcpy(pclCurrentSection, pclS);
            dbg(DEBUG,"%s pclCurrentSection<%s>",pclFunc,pclCurrentSection);
            
            /* store section name in structure... */
            strcpy((*prpBillMain).CommCfgPart.prSection_FltTyp[ilCurrentSection].pcSectionName, pclCurrentSection);
            dbg(DEBUG,"%s pcSectionName<%s>",pclFunc,(*prpBillMain).CommCfgPart.prSection_FltTyp[ilCurrentSection].pcSectionName);
            
            memset(pclSelection,0,sizeof(pclSelection));
            ReadConfigEntry( cgConfigFile,"AIR_BILL_HANDLING",pclCurrentSection,pclSelection);
            if(ilRc != RC_SUCCESS)
          {
              dbg(TRACE,"%s %s is not found<%s>", pclFunc,pclCurrentSection,pclSelection);
              memset( (*prpBillMain).CommCfgPart.prSection_FltTyp[ilCurrentSection].pcType,0,iMIN_BUF_SIZE);
              memset( (*prpBillMain).CommCfgPart.prSection_FltTyp[ilCurrentSection].pcSpecifier,0,iMIN_BUF_SIZE);
              memset( (*prpBillMain).CommCfgPart.prSection_FltTyp[ilCurrentSection].pcRange,0,iMAX_BUF_SIZE);
          }
          else
          {
              ilSubOption = GetNoOfElements(pclSelection, ':');
              for(ilCount=0; ilCount < ilSubOption; ilCount++)
              {
                  pclTmp = NULL;
                  if((pclTmp = GetDataField(pclSelection, ilCount, ':')) == NULL)
                  {
                      memset((*prpBillMain).CommCfgPart.prSection_FltTyp[ilCurrentSection].pcType,0,iMIN_BUF_SIZE);
                      memset((*prpBillMain).CommCfgPart.prSection_FltTyp[ilCurrentSection].pcSpecifier,0,iMIN_BUF_SIZE);
                      memset((*prpBillMain).CommCfgPart.prSection_FltTyp[ilCurrentSection].pcRange,0,iMAX_BUF_SIZE);
                  }
                  else
                  {
                      //if( strstr("F,G,GA,H,HM,M,NS,P,S,T",pclTmp) !=0 )
                      if( strstr("SCHEDULED,ADHOC",pclTmp) !=0  )
                      {
                          strcpy((*prpBillMain).CommCfgPart.prSection_FltTyp[ilCurrentSection].pcMark, pclTmp);
                          dbg(DEBUG,"%s pcMark<%s>",pclFunc,(*prpBillMain).CommCfgPart.prSection_FltTyp[ilCurrentSection].pcMark);
                      }
                      else if( strstr(pclTmp,"TTYP") !=0 )
                      {
                          strcpy((*prpBillMain).CommCfgPart.prSection_FltTyp[ilCurrentSection].pcType, pclTmp);
                          dbg(DEBUG,"%s pcType<%s>",pclFunc,(*prpBillMain).CommCfgPart.prSection_FltTyp[ilCurrentSection].pcType);
                      }
                      else if( strstr(pclTmp,"IN") !=0 || strstr(pclTmp,"NOT") != 0)
                      {
                          strcpy((*prpBillMain).CommCfgPart.prSection_FltTyp[ilCurrentSection].pcSpecifier,pclTmp);
                          dbg(DEBUG,"%s pcSpecifier<%s>",pclFunc,(*prpBillMain).CommCfgPart.prSection_FltTyp[ilCurrentSection].pcSpecifier);
                      }
                      else
                      {
                          strcpy((*prpBillMain).CommCfgPart.prSection_FltTyp[ilCurrentSection].pcRange,pclTmp);
                          dbg(DEBUG,"%s pcRange<%s>",pclFunc,(*prpBillMain).CommCfgPart.prSection_FltTyp[ilCurrentSection].pcRange);
                      }
                      //dbg(DEBUG,"%s pclTmp<%s>",pclFunc,pclTmp);
                  }
              }//inner for loop ends
          }
        }//outer for loop ends -> sections
    }//pcpSelection is null or not
    
    return RC_SUCCESS;
}

static int FillCfgStructFltCat(char *pcpSelection, CommonCfg *CommCfgPart,char *pclConfigName)
{
    char    *pclFunc = "FillCfgStructFltCat";
    char  *pclS = NULL;
    UINT  ilCurrentSection = 0;
    char    pclCurrentSection[iMAX_BUF_SIZE] = "\0";
    char    pclSelection[iMAX_BUF_SIZE] = "\0";
    int     ilSubOption = 0;
    int     ilCount = 0;
    char     *pclTmp = NULL;
    int     ilRc = RC_SUCCESS;
    
    if(strlen(pcpSelection) == 0)
    {
        dbg(TRACE,"%s pcpSelection is null",pclFunc);
        return RC_FAIL;
    }
    else
    {
        /* convert to uppercase */
        StringUPR((UCHAR*)pcpSelection);
        
        /* get number of currently used sections... */
        (*CommCfgPart).iNoOfSections_FltCat = GetNoOfElements(pcpSelection, ',');
        dbg(DEBUG,"%s iNoOfSections_FltCat<%d>",pclFunc,(*CommCfgPart).iNoOfSections_FltCat);
        
        /* memory for section */
        if(((*CommCfgPart).prSection_FltCat = (ESection*)malloc((*CommCfgPart).iNoOfSections_FltCat*sizeof(ESection))) == NULL)
        {
            dbg(TRACE,"%s can't run without memory (3)",pclFunc);
            (*CommCfgPart).prSection_FltCat = NULL;
            Terminate();
        }
        
        // start reading sub section configurations
        for(ilCurrentSection=0; ilCurrentSection < (*CommCfgPart).iNoOfSections_FltCat; ilCurrentSection++)
        {
            /* clear memory */
            if((pclS = GetDataField(pcpSelection, ilCurrentSection, ',')) == NULL)
            {
                dbg(TRACE,"%s can't read current section number %d in section MAIN", pclFunc, ilCurrentSection);
                return RC_FAIL;
            }
            
            /* store current section */
            memset((void*)pclCurrentSection, 0x00, iMAX_BUF_SIZE);
            strcpy(pclCurrentSection, pclS);
            dbg(DEBUG,"%s pclCurrentSection<%s>",pclFunc,pclCurrentSection);
            
            /* store section name in structure... */
            strcpy((*CommCfgPart).prSection_FltCat[ilCurrentSection].pcSectionName, pclCurrentSection);
            dbg(DEBUG,"%s pcSectionName<%s>",pclFunc,(*CommCfgPart).prSection_FltCat[ilCurrentSection].pcSectionName);
            
            memset(pclSelection,0,sizeof(pclSelection));
            ReadConfigEntry( cgConfigFile,pclConfigName,pclCurrentSection,pclSelection);
            if(ilRc != RC_SUCCESS)
          {
              dbg(TRACE,"%s %s is not found<%s>", pclFunc,pclCurrentSection,pclSelection);
              memset( (*CommCfgPart).prSection_FltCat[ilCurrentSection].pcType,0,iMIN_BUF_SIZE);
              memset( (*CommCfgPart).prSection_FltCat[ilCurrentSection].pcSpecifier,0,iMIN_BUF_SIZE);
              memset( (*CommCfgPart).prSection_FltCat[ilCurrentSection].pcRange,0,iMAX_BUF_SIZE);
          }
          else
          {
              ilSubOption = GetNoOfElements(pclSelection, ':');
              for(ilCount=0; ilCount < ilSubOption; ilCount++)
              {
                  pclTmp = NULL;
                  if((pclTmp = GetDataField(pclSelection, ilCount, ':')) == NULL)
                  {
                      memset((*CommCfgPart).prSection_FltCat[ilCurrentSection].pcType,0,iMIN_BUF_SIZE);
                      memset((*CommCfgPart).prSection_FltCat[ilCurrentSection].pcSpecifier,0,iMIN_BUF_SIZE);
                      memset((*CommCfgPart).prSection_FltCat[ilCurrentSection].pcRange,0,iMAX_BUF_SIZE);
                  }
                  else
                  {
                      if( strstr("F,G,GA,H,HM,M,NS,P,S,T,FREIGHTER,SCHEDULED,Cargo Flight,Passenger Flight",pclTmp) !=0  )
                      {
                          strcpy((*CommCfgPart).prSection_FltCat[ilCurrentSection].pcMark, pclTmp);
                      }
                      else if( strstr(pclTmp,"TTYP") !=0 )
                      {
                          strcpy((*CommCfgPart).prSection_FltCat[ilCurrentSection].pcMark, (*CommCfgPart).prSection_FltCat[ilCurrentSection].pcRange);
                          replaceChar((*CommCfgPart).prSection_FltCat[ilCurrentSection].pcMark, '_', ' ');
                          strcpy((*CommCfgPart).prSection_FltCat[ilCurrentSection].pcType, pclTmp);
                      }
                      else if( strstr(pclTmp,"IN") !=0 || strstr(pclTmp,"NOT") != 0)
                      {
                          strcpy((*CommCfgPart).prSection_FltCat[ilCurrentSection].pcSpecifier,pclTmp);
                          dbg(DEBUG,"%s pcSpecifier<%s>",pclFunc,(*CommCfgPart).prSection_FltCat[ilCurrentSection].pcSpecifier);
                      }
                      else
                      {
                          strcpy((*CommCfgPart).prSection_FltCat[ilCurrentSection].pcRange,pclTmp);
                          dbg(DEBUG,"%s pcRange<%s>",pclFunc,(*CommCfgPart).prSection_FltCat[ilCurrentSection].pcRange);
                      }
                      //dbg(DEBUG,"%s pclTmp<%s>",pclFunc,pclTmp);
                  }
              }//inner for loop ends
          }
        }//outer for loop ends -> sections
    }//pcpSelection is null or not
    
    return RC_SUCCESS;
}

static int FillCfgStructSelCri(char *pcpSelection, CommonCfg *CommCfgPart,char *pclConfigName)
{
    char    *pclFunc = "FillCfgStructSelCri";
    char  *pclS = NULL;
    UINT  ilCurrentSection = 0;
    char    pclCurrentSection[iMAX_BUF_SIZE] = "\0";
    char    pclSelection[iMAX_BUF_SIZE] = "\0";
    int     ilSubOption = 0;
    int     ilCount = 0;
    char     *pclTmp = NULL;
    int     ilRc = RC_SUCCESS;
    
    if(strlen(pcpSelection) == 0)
    {
        dbg(TRACE,"%s pcpSelection is null",pclFunc);
        return RC_FAIL;
    }
    else
    {
        /* convert to uppercase */
        StringUPR((UCHAR*)pcpSelection);
        
        /* get number of currently used sections... */
        (*CommCfgPart).iNoOfSections_SelCri = GetNoOfElements(pcpSelection, ',');
        dbg(DEBUG,"%s iNoOfSections_SelCri<%d>",pclFunc,(*CommCfgPart).iNoOfSections_SelCri);
        
        /* memory for section */
        if(((*CommCfgPart).prSection_SelCri = (ESection*)malloc((*CommCfgPart).iNoOfSections_SelCri*sizeof(ESection))) == NULL)
        {
            dbg(TRACE,"%s can't run without memory (3)",pclFunc);
            (*CommCfgPart).prSection_SelCri = NULL;
            Terminate();
        }
        
        // start reading sub section configurations
        for(ilCurrentSection=0; ilCurrentSection < (*CommCfgPart).iNoOfSections_SelCri; ilCurrentSection++)
        {
            /* clear memory */
            if((pclS = GetDataField(pcpSelection, ilCurrentSection, ',')) == NULL)
            {
                dbg(TRACE,"%s can't read current section number %d in section MAIN", pclFunc, ilCurrentSection);
                return RC_FAIL;
            }
            
            /* store current section */
            memset((void*)pclCurrentSection, 0x00, iMAX_BUF_SIZE);
            strcpy(pclCurrentSection, pclS);
            dbg(DEBUG,"%s pclCurrentSection<%s>",pclFunc,pclCurrentSection);
            
            /* store section name in structure... */
            strcpy((*CommCfgPart).prSection_SelCri[ilCurrentSection].pcSectionName, pclCurrentSection);
            dbg(DEBUG,"%s pcSectionName<%s>",pclFunc,(*CommCfgPart).prSection_SelCri[ilCurrentSection].pcSectionName);
            
            memset(pclSelection,0,sizeof(pclSelection));
            ReadConfigEntry( cgConfigFile,pclConfigName,pclCurrentSection,pclSelection);
            if(ilRc != RC_SUCCESS)
          {
              dbg(TRACE,"%s %s is not found<%s>", pclFunc,pclCurrentSection,pclSelection);
              memset( (*CommCfgPart).prSection_SelCri[ilCurrentSection].pcType,0,iMIN_BUF_SIZE);
              memset( (*CommCfgPart).prSection_SelCri[ilCurrentSection].pcSpecifier,0,iMIN_BUF_SIZE);
              memset( (*CommCfgPart).prSection_SelCri[ilCurrentSection].pcRange,0,iMAX_BUF_SIZE);
          }
          else
          {
              ilSubOption = GetNoOfElements(pclSelection, ':');
              for(ilCount=0; ilCount < ilSubOption; ilCount++)
              {
                  pclTmp = NULL;
                  if((pclTmp = GetDataField(pclSelection, ilCount, ':')) == NULL)
                  {
                      memset((*CommCfgPart).prSection_SelCri[ilCurrentSection].pcType,0,iMIN_BUF_SIZE);
                      memset((*CommCfgPart).prSection_SelCri[ilCurrentSection].pcSpecifier,0,iMIN_BUF_SIZE);
                      memset((*CommCfgPart).prSection_SelCri[ilCurrentSection].pcRange,0,iMAX_BUF_SIZE);
                  }
                  else
                  {
                      if( strstr(pclTmp,"FREIGHTER") !=0 || strstr(pclTmp,"SCHEDULED") !=0 )
                      {
                          strcpy((*CommCfgPart).prSection_SelCri[ilCurrentSection].pcMark, pclTmp);
                          dbg(DEBUG,"%s pcMark<%s>",pclFunc,(*CommCfgPart).prSection_SelCri[ilCurrentSection].pcMark);
                      }
                      else if ( 
                      	         strstr(pclTmp,"TTYP") !=0 ||
                      	         strstr(pclTmp,"FTYP") !=0 
                      	      )
                      {
                          strcpy((*CommCfgPart).prSection_SelCri[ilCurrentSection].pcType, pclTmp);
                          dbg(DEBUG,"%s pcType<%s>",pclFunc,(*CommCfgPart).prSection_SelCri[ilCurrentSection].pcType);
                      }
                      else if( strstr(pclTmp,"IN") !=0 || strstr(pclTmp,"NOT") != 0)
                      {
                          strcpy((*CommCfgPart).prSection_SelCri[ilCurrentSection].pcSpecifier,pclTmp);
                          dbg(DEBUG,"%s pcSpecifier<%s>",pclFunc,(*CommCfgPart).prSection_SelCri[ilCurrentSection].pcSpecifier);
                      }
                      else
                      {
                          strcpy((*CommCfgPart).prSection_SelCri[ilCurrentSection].pcRange,pclTmp);
                          dbg(DEBUG,"%s pcRange<%s>",pclFunc,(*CommCfgPart).prSection_SelCri[ilCurrentSection].pcRange);
                      }
                      //dbg(DEBUG,"%s pclTmp<%s>",pclFunc,pclTmp);
                  }
              }//inner for loop ends
          }
        }//outer for loop ends -> sections
    }//pcpSelection is null or not
    return RC_SUCCESS;
}

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int ReadConfigEntry(char *pcpFname, char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer)
{
    int ilRc = RC_SUCCESS;

    char clSection[124] = "\0";
    char clKeyword[124] = "\0";

    strcpy(pcpCfgBuffer, "");
    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRc = iGetConfigEntry(pcpFname,clSection,clKeyword,
               CFG_STRING,pcpCfgBuffer);
    if(ilRc != RC_SUCCESS){
        dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
    } 
    else{
        dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s",
        clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
    }/* end of if */
    return ilRc;
}
/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int    ilRc = RC_SUCCESS;                /* Return code */
    
    dbg(TRACE,"Reset: now resetting");
    
    return ilRc;
    
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate()
{

    dbg(TRACE,"Terminate: now leaving ...");
    
    exit(0);
    
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
    int    ilRc = RC_SUCCESS;            /* Return code */
    dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);
    switch(pipSig)
    {
    default    :
        Terminate();
        break;
    } /* end of switch */
    exit(0);
    
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
    int    ilRc = RC_SUCCESS;
    return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    int    ilRc = RC_SUCCESS;
    
    switch(pipErr) {
    case    QUE_E_FUNC    :    /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :    /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND    :    /* Error using msgsnd */
        dbg(TRACE,"<%d> : msgsnd failed",pipErr);
        break;
    case    QUE_E_GET    :    /* Error using msgrcv */
        dbg(TRACE,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX    :
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE    :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES    :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP    :    /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL    :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG    :    /* No message on queue */
        dbg(TRACE,"<%d> : no messages on queue",pipErr);
        break;
    case    QUE_E_INVORG    :    /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :    /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    default            :    /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
         
    return;
} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
    int    ilRc = RC_SUCCESS;            /* Return code */
    int    ilBreakOut = FALSE;
    
    do
    {
        ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text;    
        if( ilRc == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRc != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRc);
            } /* fi */
        
            switch( prgEvent->command )
            {
            case    HSB_STANDBY    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_COMING_UP    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_ACTIVE    :
                ctrl_sta = prgEvent->command;
                ilBreakOut = TRUE;
                break;    
            case    HSB_ACT_TO_SBY    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate();
                break;    
    
            case    HSB_STANDALONE    :
                ctrl_sta = prgEvent->command;

                ilBreakOut = TRUE;
                break;    

            case    SHUTDOWN    :
                Terminate();
                break;
                        
            case    RESET        :
                ilRc = Reset();
                break;
                        
            case    EVENT_DATA    :
                dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
            default            :
                dbg(TRACE,"HandleQueues: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        } else {
            /* Handle queuing errors */
            HandleQueErr(ilRc);
        } /* end else */
    } while (ilBreakOut == FALSE);
    if(igInitOK == FALSE)
    {
            ilRc = Init_Process();
            if(ilRc != RC_SUCCESS)
            {
                dbg(TRACE,"Init_Process: init failed!");
            } /* end of if */
    }/* end of if */
    /* OpenConnection(); */
} /* end of HandleQueues */
    

/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{ 
    int ilRc = RC_SUCCESS;         
    int ilCmd = 0;
    int ilUpdPoolJob = TRUE;
    int ilNumdays;
    
    char *pclFunc = "HandleData";

    BC_HEAD *prlBchead       = NULL;
    CMDBLK  *prlCmdblk       = NULL;
    char    *pclSelection    = NULL;
    char    *pclFields       = NULL;
    char    *pclData         = NULL;
    char    *pclRow          = NULL;
    char    clUrnoList[2400];
    char    clTable[34];
    
    int ilCurWeekDay = 0;
    int ilCurMonth = 0;
    char pclDateString[16] = "\0";
    
    char pclFieldListUpd[2048] = "\0";
    char pclDataListUpd[2048] = "\0";
    char pclFieldListIns[2048] = "\0";
    char pclDataListIns[2048] = "\0";
    
    char pclFromBillLAND[32] = "\0";
    char pclToBillLAND[32] = "\0";
    char pclFromBillAIRB[32] = "\0";
    char pclToBillAIRB[32] = "\0";
    char pclFromDaily[16] = "\0";
    char pclToDaily[16] = "\0";
    char pclFromMonthLAND[16] = "\0";
    char pclToMonthLAND[16] = "\0";
    char pclFromMonthAIRB[16] = "\0";
    char pclToMonthAIRB[16] = "\0";
    
    char pclUrno[32]="\0";
    char pclTmpField[32]="\0";
    char pclTmpData[32768]="\0";
    //
    short slSqlFunc = 0;
    short slCursor = 0;
    char pclDataArea[8192] = "\0";
    char pclAftSelection[8192] = "\0";
    int flds_count=0;
    int ilProcessId = 0;
    int ili = 0;
    char pclTmp[1024];
    
      //
    
    prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
    prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
    pclSelection = prlCmdblk->data;
    pclFields    = pclSelection + strlen(pclSelection) + 1;
    pclData      = pclFields + strlen(pclFields) + 1;


    strcpy(clTable,prlCmdblk->obj_name);

    dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);
    
    memset(pcgAftDestName, '\0', (sizeof(prlBchead->dest_name) + 1));
    strncpy (pcgAftDestName, prlBchead->dest_name, sizeof(prlBchead->dest_name));
    memset(pcgAftRecvName, '\0', (sizeof(prlBchead->recv_name) + 1));
    strncpy(pcgAftRecvName, prlBchead->recv_name, sizeof(prlBchead->recv_name));
    strcpy (pcgAftTwStart,prlCmdblk->tw_start);
    strcpy (pcgAftTwEnd,prlCmdblk->tw_end);

       
    dbg(TRACE,"Command: <%s>",prlCmdblk->command);
    dbg(TRACE,"Obj_name: <%s>",prlCmdblk->obj_name);
    dbg(TRACE,"Tw_start: <%s>",prlCmdblk->tw_start);
    dbg(TRACE,"originator follows event = %p ",prpEvent);

    dbg(TRACE,"originator<%d>",prpEvent->originator);
    dbg(TRACE,"selection follows Selection = %p ",pclSelection);
    dbg(TRACE,"selection <%s>",pclSelection);
    dbg(TRACE,"fields    <%s>",pclFields);
    dbg(TRACE,"data      <%s>",pclData);
    
    ilRc = GetWeekDayAndMonthDateStr(&ilCurWeekDay,&ilCurMonth,pclDateString);
    if( ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"GetWeekDayAndMonthDateStr() fails, return");
        return RC_FAIL;
    }
    dbg(DEBUG,"HandleData:: ilWeekDay<%d>,ilCurMonth<%d>,pclDateString<%s>",ilCurWeekDay,ilCurMonth,pclDateString);
    dbg(TRACE,"HandleData:: cmd<%s>",prlCmdblk->command);
 	
 

    if ( strcmp(prlCmdblk->command,"SCHQ")==0 )
    {
        dbg(TRACE, "HandleData:: processing SCHQ command START");
        ilRc = sendCedaEventWithLog(igWMERPSQueue,0,pcgAftDestName,pcgAftRecvName,
                                    pcgAftTwStart,pcgAftTwEnd,"SCH","ERPSCH","",
                                    "STATUS","OK","",4,NETOUT_NO_ACK); 
        dbg(TRACE, "HandleData:: processing SCHQ command END Result[%d]", ilRc);
    }	
    else if(strcmp(prlCmdblk->command,"ADH")== 0)
    {
        dbg(TRACE, "HandleData:: processing ADH command START");

        flds_count = get_flds_count(pcgErpAdhFields);
        slCursor = 0;
        sprintf(pclAftSelection,"SELECT URNO FROM ERPADH WHERE STAT='F' ");

        dbg(DEBUG,"HandleData:: pclAftSelection<%s>",pclAftSelection);
        ilRc = sql_if (START, &slCursor, pclAftSelection, pclDataArea);
        while( ilRc == DB_SUCCESS )
        {
            BuildItemBuffer(pclDataArea, NULL, 1, ",");
            dbg(TRACE,"HandleData:: ADH: URNO[%s]", pclDataArea);

            dbg(TRACE, "HandleData:; ADH: Sending to WMERPA");
            ilRc = sendCedaEventWithLog(igWMERPAQueue,0,pcgAftDestName,pcgAftRecvName,
                                        pcgAftTwStart,pcgAftTwEnd,"ADH","ERPADH","",
                                       "URNO",pclDataArea,"",4,NETOUT_NO_ACK);

            ilRc = sql_if (NEXT, &slCursor, pclAftSelection, pclDataArea);
        }
        close_my_cursor(&slCursor);
 
        dbg(START, "HandleData:: processing ADH command END Result[%d]", ilRc); 
    } 
    else if( !strcmp(prlCmdblk->command,"UADH") )
    {
        dbg(TRACE,"HandleData:: processing UADH cmd START");
        
        if( strlen(pclData)==0 )
        {
            dbg(TRACE,"HandleData:: pclData<%s> is invalid", pclData);
        }
        else if(strlen(pclFields)==0 || strstr(pclFields,"URNO")==0 || strncmp(pclFields," ",1)==0)
        {
            dbg(TRACE,"HandleData:: pclFields<%s> is invalid or does not contain URNO",pclData);
        }
        else
        {
            flds_count = get_flds_count(pclFields);
            for(ili=0;ili <flds_count;ili++)
            {
                get_fld_value(pclFields,ili+1,pclTmpField);
                get_fld_value(pclData,ili+1,pclTmpData);
                if(strcmp(pclTmpField,"URNO")==0)
                {
                    strcpy(pclUrno,pclTmpData);
                    break;
                }
            }
            dbg(DEBUG,"HandleData:: pclUrno<%s>line<%d>", pclUrno,__LINE__); 
            
            ilRc=processUADHCmd(pclUrno);    
        }
    } 
    else if( !strcmp(prlCmdblk->command,"FPE") )
    {
        dbg(TRACE,"HandleData:: processing FPE cmd START");
        
        if( strlen(pclData)==0 )
        {
            dbg(TRACE,"HandleData:: pclData<%s> is invalid", pclData);
        }
        else if(strlen(pclFields)==0 || strstr(pclFields,"URNO")==0 || strncmp(pclFields," ",1)==0)
        {
            dbg(TRACE,"HandleData:: pclFields<%s> is invalid or does not contain CSGN",pclData);
        }
        else
        {
            flds_count = get_flds_count(pclFields);
            for(ili=0;ili <flds_count;ili++)
            {
                get_fld_value(pclFields,ili+1,pclTmpField);
                get_fld_value(pclData,ili+1,pclTmpData);
                if(strcmp(pclTmpField,"URNO")==0)
                {
                    strcpy(pclUrno,pclTmpData);
                    break;
                }
            }
            dbg(DEBUG,"HandleData:: URNO<%s>line<%d>", pclUrno,__LINE__); 
            
            ilRc=processFPE(pclUrno, pclDateString);    
        }
    }          
    // USCH & DSCH
    else if ( 
    	       !strcmp(prlCmdblk->command,"USCH") || 
    	       !strcmp(prlCmdblk->command,"DSCH") 
    	    )
    {
        dbg(TRACE,"HandleData:: processing USCH/DSCH cmd START");
        
        if( strlen(pclData)==0 )
        {
            dbg(TRACE,"HandleData:: pclData<%s> is invalid", pclData);
        }
        else if(strlen(pclFields)==0 || strstr(pclFields,"URNO")==0 || strncmp(pclFields," ",1)==0)
        {
            dbg(TRACE,"HandleData:: pclFields<%s> is invalid or does not contain URNO",pclData);
        }
        else
        {
            int iFtypFlag=0;

            flds_count = get_flds_count(pclFields);

            for(ili=0;ili <flds_count;ili++)
            {
                get_fld_value(pclFields,ili+1,pclTmpField);
                get_fld_value(pclData,ili+1,pclTmpData);

                if(strcmp(pclTmpField,"URNO")==0)
                {
                    strcpy(pclUrno,pclTmpData);
                }
                else if (strcmp(pclTmpField,"FTYP")==0)
                     iFtypFlag=1;    
            }
            dbg(DEBUG,"HandleData:: pclUrno<%s>line<%d>",pclUrno,__LINE__); 
            
            if ( !strcmp(prlCmdblk->command,"USCH") )
                ilRc=processUSCHCmd(pclUrno, iFtypFlag);
            else if ( !strcmp(prlCmdblk->command,"DSCH") ) 
            	ilRc=processDeleteERPSCH(pclUrno);	
                    	
        }
        dbg(TRACE,"HandleData:: processing USCH/DSCH cmd END [%d]", ilRc);
    }  
    //SCH
    else if (
         strcmp(prlCmdblk->command,"SCH")== 0 && 
         strlen(rgTM_Daily.CommCfgPart.pcpRecvCmdConfig) != 0 && 
         strcmp(rgTM_Daily.CommCfgPart.pcpRecvCmdConfig," ") != 0 
       )
    {
        dbg(TRACE, "HandleData:: processing SCH command START");
        memset(pclFromDaily, 0, sizeof(pclFromDaily));
        memset(pclToDaily, 0, sizeof(pclToDaily));
        ilRc = CalTimeWindowDaily(ilCurWeekDay,pclFromDaily,pclToDaily);
        
        if( ilRc == RC_SUCCESS)
        {
            AddHHMMSS(pclFromDaily,pclToDaily,rgTM_Daily.pcpSTOAConfig);
            dbg(TRACE, "HandleData:: pclFromDaily[%s] pclToDaily[%s] pclDateString[%s]", 
                pclFromDaily, pclToDaily, pclDateString); 
            processSCHCmd(pclFromDaily,pclToDaily);
        }
        else
        {
            dbg(TRACE,"CalTimeWindowDaily() fails");
            ilRc=RC_FAIL;
        }
        
        dbg(TRACE, "HandleData:: processing SCH command END Result[%d]", ilRc);        
        /****************************************/
    }
    else if(strcmp(prlCmdblk->command,"SCHD")== 0)
    {
    	char pclToday[15];
    	char pclYesterday[15];
    	char pclMonday[15];
    	char pclNextMonday[15];
    	
    	dbg(DEBUG, "HandleData:: processing SCH command START");
    	
        ilRc = getSCHDDates(pclToday, pclYesterday, pclMonday, pclNextMonday);
        ilRc = reprocessERPSCHRecords(pclYesterday, pclMonday, pclNextMonday, pclDateString);
        ilRc = processAdditionalERPSCHRecords(pclYesterday, pclMonday, pclNextMonday, pclDateString);

        dbg(DEBUG, "HandleData:: processing SCH command END [%d]", ilRc);
    }
  
    //ERP
    else if ( strcmp(prlCmdblk->command,"ERP")== 0 )
    {
        dbg(TRACE, "HandleData:: processing ERP command");
        
        if ( strlen(pclData)==0 )
        {
            dbg(TRACE,"HandleData:: pclData<%s> is invalid",pclData);
        }
        else
        {
            processERPCmd(pclData);
        }

        dbg(TRACE, "HandleData:: processing ERP command End");        
    }
    else if(strcmp(prlCmdblk->command,"BILL")== 0)
    {
    	dbg(TRACE, "HandleData:: processing BILL cmd START");
        ilRc = CalTimeWindowBill(pclDateString,pclFromBillLAND,pclToBillLAND,pclFromBillAIRB,pclToBillAIRB);
        if( ilRc == RC_SUCCESS)
        {
            dbg(TRACE,"pclFromBillLAND<%s>pclToBillLAND<%s>pclFromBillAIRB<%s>pclToBillAIRB<%s>",pclFromBillLAND,pclToBillLAND,pclFromBillAIRB,pclToBillAIRB);
            
            AddHHMMSS(pclFromBillLAND,pclToBillLAND,rgTM_Bill.pcpLANDConfig);
            AddHHMMSS(pclFromBillAIRB,pclToBillAIRB,rgTM_Bill.pcpAIRBConfig);
            
            dbg(DEBUG,"%s pclFromBillLAND<%s>",pclFunc,pclFromBillLAND);
            dbg(DEBUG,"%s pclToBillLAND<%s>",pclFunc,pclToBillLAND);
            dbg(DEBUG,"%s pclFromBillAIRB<%s>",pclFunc,pclFromBillAIRB);
            dbg(DEBUG,"%s pclToBillAIRB<%s>",pclFunc,pclToBillAIRB);
            
            processBILL(pclFromBillLAND,pclToBillLAND,pclFromBillAIRB,pclToBillAIRB,pclDateString);
        }
        else
        {
            dbg(TRACE,"CalTimeWindowBill() fails, return");
            return RC_FAIL;
        }
    }

    else
    {
        dbg(TRACE,"%s cmd<%s>",pclFunc,prlCmdblk->command);
    }
    
    dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);

    /****************************************/
    return ilRc;
    
} /* end of HandleData */

int processFPE(char *pcpUrno, char *pcpDate)
{
    int ilRC=RC_SUCCESS;
    char pclSql[1024];
    char pclData[1024];
    char pclRkey[32];
    char pclCsgn[16];
    char pclRegn[16];
    char pclVato[16];
    char pclVafr[16];
    char pclAdid[4];
    short slCursor=0;
    
    dbg(TRACE, "processFPE:: Start");
    
    dbg(TRACE, "processFPE:: URNO[%s], pcpDate[%s]", pcpUrno, pcpDate);
    
    sprintf(pclSql, "SELECT CSGN,REGN,VAFR,VATO,ADID FROM FPETAB WHERE URNO='%s' ", 
            pcpUrno);
    ilRC=sql_if(START, &slCursor, pclSql, pclData);
    dbg(TRACE, "processFPE:: executing[%s][%d]", pclSql, ilRC);
    
    if ( ilRC==RC_FAIL )
    {
        dbg(TRACE, "processFPE:: DB ERROR!!!");	
    }
    else if ( ilRC==ORA_NOT_FOUND )
    {
        dbg(TRACE, "processFPE:: No record found");
        ilRC=RC_FAIL;
    }
    else
    {
        BuildItemBuffer(pclData, "CSGN,REGN,VAFR,VATO,ADID", 5, ",");
        get_real_item(pclCsgn, pclData, 1); 
        if ( !strcmp(pclCsgn, "") )
        	strcpy(pclCsgn, " ");
               
        get_real_item(pclRegn, pclData, 2);  
        if ( !strcmp(pclRegn, "") )
        	strcpy(pclRegn, " ");
        	  
        get_real_item(pclVafr, pclData, 3);  
         if ( !strcmp(pclVafr, "") )
        	strcpy(pclVafr, " ");
        	         
        get_real_item(pclVato, pclData, 4);   
        if ( !strcmp(pclVato, "") )
        	strcpy(pclVato, " ");
        	        
        get_real_item(pclAdid, pclData, 5);    
        if ( !strcmp(pclAdid, "") )
        	strcpy(pclAdid, " ");

    	dbg(TRACE, "processFPE:: CSGN[%s] REGN[%s] VAFR[%s] VATO[%s] ADID[%s]", 
    	    pclCsgn, pclRegn, pclVafr, pclVato, pclAdid); 

        if ( !strcmp(pclAdid, "A") )
        {
            sprintf(pclSql, "SELECT RKEY FROM AFTTAB "
                            "WHERE ADHO='X' "
                            "AND CSGN='%s' "
                            "AND (STOA BETWEEN '%s' and '%s') "
                            "AND ADID='A' ", 
                    pclCsgn, pclVafr, pclVato);
        }
        else
        {
            sprintf(pclSql, "SELECT RKEY FROM AFTTAB "
                            "WHERE ADHO='X' "
                            "AND CSGN='%s' "
                            "AND (STOD BETWEEN '%s' and '%s') "
                            "AND ADID='D' ", 
                    pclCsgn, pclVafr, pclVato);
        }        	

        close_my_cursor(&slCursor);  
        ilRC=sql_if(START, &slCursor, pclSql, pclRkey);
        dbg(TRACE, "processFPE:: executing [%s][%d]", pclSql, ilRC);
        if ( ilRC==RC_FAIL )
        {
            dbg(TRACE, "processFPE:: DB ERROR!!!");	
        }
        else if ( ilRC==ORA_NOT_FOUND )
        {
            dbg(TRACE, "processFPE:: No record found");
            ilRC=RC_SUCCESS;
        }
        else
        {
    	    dbg(TRACE, "processFPE:: processing RKEY[%s]", pclRkey);
    	    ilRC=doERPADH(pclRkey);
        }
    }
    close_my_cursor(&slCursor);
    
    dbg(TRACE, "processFPE:: End Result[%d]", ilRC);    
    return ilRC;	
}

static int FilterAFTDataAndBuildSql(char *pclFieldSource,char*pclDataSource,char *pclFieldListUpd,char *pclFieldListIns,char *pclDataListIns,char *pclTabName,char *pcpFlnu)
{
    char *pclFunc = "FilterAFTDataAndBuildSql";
    int ilRc = RC_SUCCESS;
    int ilNoField = 0;
    int ilNoData = 0;
    int ilCount = 0;
  char pclTmpFieldBuildIns[32] = "\0";
  char pclTmpDataBuildIns[32] = "\0";
  char pclTmpFieldBuildUpd[32] = "\0";
  char pclTmpDataBuildUpd[32] = "\0";
  char pclTmpFieldExtract[32] = "\0";
  char pclTmpDataExtract[32] = "\0";
  
  if(strlen(pclFieldSource)==0 || strncmp(pclFieldSource," ",1)==0)
  {
      dbg(TRACE,"%s pclFieldSource is null,return",pclFunc);
      return RC_FAIL;
  }
  
  if(strlen(pclDataSource)==0 || strncmp(pclDataSource," ",1)==0)
  {
      dbg(TRACE,"%s pclDataSource is null,return",pclFunc);
      return RC_FAIL;
  }
  
  if(strlen(pclTabName)==0 || strncmp(pclTabName," ",1)==0)
  {
      dbg(TRACE,"%s pclTabName is null,return",pclFunc);
      return RC_FAIL;
  }
    
    ilNoField = GetNoOfElements(pclFieldSource,',');
    ilNoData = GetNoOfElements(pclDataSource,',');
    dbg(TRACE,"%s ilNoField<%d>ilNoData<%d>",pclFunc,ilNoField,ilNoData);
    
    memset(pclFieldListUpd,0,sizeof(pclFieldListUpd));
    memset(pclFieldListIns,0,sizeof(pclFieldListIns));
    memset(pclDataListIns,0,sizeof(pclDataListIns));
    memset(pcpFlnu,0,sizeof(pcpFlnu));
    
    for(ilCount = 1; ilCount <= ilNoField; ilCount++)
  {
        memset(pclTmpDataExtract,0,sizeof(pclTmpDataExtract));
        memset(pclTmpFieldExtract,0,sizeof(pclTmpFieldExtract));
        memset(pclTmpDataBuildIns,0,sizeof(pclTmpDataBuildIns));
      memset(pclTmpFieldBuildIns,0,sizeof(pclTmpFieldBuildIns));
      memset(pclTmpFieldBuildUpd,0,sizeof(pclTmpFieldBuildUpd));
                  
        GetDataItem(pclTmpFieldExtract,pclFieldSource,ilCount,','," ","\0\0");
        GetDataItem(pclTmpDataExtract,pclDataSource,ilCount,','," ","\0\0");
        
        dbg(TRACE,"-------%s ilCount<%d><%s>=<%s>",pclFunc,ilCount,pclTmpFieldExtract,pclTmpDataExtract);
        
        if(strcmp(pclTmpFieldExtract,"URNO")==0)
        {
            strcpy(pclTmpFieldExtract,"FLNU");
            strcpy(pcpFlnu,pclTmpDataExtract);
        }
        
        if(strcmp(pclTabName,"ERPSCH")==0)
        {
            if(strstr(pcgErpSchFields,pclTmpFieldExtract)==0)
            {
                dbg(TRACE,"%s pcgErpSchFields<%s> does not contain<%s>",pclFunc,pcgErpSchFields,pclTmpFieldExtract);
            }
            else
            {
                dbg(DEBUG,"%s ERPSCH<%s>",pclFunc,pclTmpFieldExtract);
                
                if(strlen(pclFieldListIns)==0)
                {
                    sprintf(pclTmpFieldBuildIns,"%s",pclTmpFieldExtract);
                    sprintf(pclTmpDataBuildIns,"'%s'",pclTmpDataExtract);
                }
                else
                {
                    sprintf(pclTmpFieldBuildIns,",%s",pclTmpFieldExtract);
                    sprintf(pclTmpDataBuildIns,",'%s'",pclTmpDataExtract);
                }
                strcat(pclFieldListIns,pclTmpFieldBuildIns);
                strcat(pclDataListIns,pclTmpDataBuildIns);
                
                dbg(TRACE,"#####%s pclTmpFieldBuildIns<%s>,pclTmpDataExtract<%s>,pclFieldListIns<%s>pclDataListIns<%s>",pclFunc,pclTmpFieldBuildIns,pclTmpDataExtract,pclFieldListIns,pclDataListIns);
                
                if(strlen(pclFieldListUpd)==0)
                {
                    sprintf(pclTmpFieldBuildUpd,"%s='%s'",pclTmpFieldExtract,pclTmpDataExtract);
                }
                else
                {
                    sprintf(pclTmpFieldBuildUpd,",%s='%s'",pclTmpFieldExtract,pclTmpDataExtract);
                }
                strcat(pclFieldListUpd,pclTmpFieldBuildUpd);
            }
        }
        else if(strcmp(pclTabName,"ERPADH")==0)
        {
            if(strstr(pcgErpAdhFields,pclTmpFieldExtract)==0)
            {
                dbg(TRACE,"%s pcgErpAdhFields<%s> does not contain<%s>",pclFunc,pcgErpAdhFields,pclTmpFieldExtract);
            }
            else
            {
                dbg(DEBUG,"%s ERPADH<%s>",pclFunc,pclTmpFieldExtract);
                
                if(strlen(pclFieldListIns)==0)
                {
                    sprintf(pclTmpFieldBuildIns,"%s",pclTmpFieldExtract);
                    sprintf(pclTmpDataBuildIns,"'%s'",pclTmpDataExtract);
                }
                else
                {
                    sprintf(pclTmpFieldBuildIns,",%s",pclTmpFieldExtract);
                    sprintf(pclTmpDataBuildIns,",'%s'",pclTmpDataExtract);
                }
                strcat(pclFieldListIns,pclTmpFieldBuildIns);
                strcat(pclDataListIns,pclTmpDataBuildIns);
                
                dbg(TRACE,"#####%s pclTmpFieldBuildIns<%s>,pclTmpDataExtract<%s>,pclFieldListIns<%s>pclDataListIns<%s>",pclFunc,pclTmpFieldBuildIns,pclTmpDataExtract,pclFieldListIns,pclDataListIns);
                
                if(strlen(pclFieldListUpd)==0)
                {
                    sprintf(pclTmpFieldBuildUpd,"%s='%s'",pclTmpFieldExtract,pclTmpDataExtract);
                }
                else
                {
                    sprintf(pclTmpFieldBuildUpd,",%s='%s'",pclTmpFieldExtract,pclTmpDataExtract);
                }
                strcat(pclFieldListUpd,pclTmpFieldBuildUpd);
            }
        }
        else
        {
            dbg(TRACE,"%s pclTabName is invalid",pclFunc);
        }
            
    }
    
    strcat(pclFieldListUpd,",STAT='R'");
    
    return ilRc;
}

static int CalTimeWindowBill(char *pcpDateString,char *pcpFromLAND, char *pcpToLAND,char *pcpFromAIRB, char *pcpToAIRB)
{
    char *pclFunc = "CalTimeWindowBill";
    int ilOffsetLAND1 = 0;
    int ilOffsetLAND2 = 0;
    int ilOffsetAIRB1 = 0;
    int ilOffsetAIRB2 = 0;
    char pclLAND1stOption[256] = "\0";
    char pclLAND3rdOption[256] = "\0";
    char pclAIRB1stOption[256] = "\0";
    char pclAIRB3rdOption[256] = "\0";
    int ilRc = RC_SUCCESS;
    char pclTmp[256] = "\0";
    int ilPreMonth = 0;
    char buffer[4];
    char pclRef[8] = "\0";
    
    //LAND = DAY-2,000000,DAY-1,000000
    ilRc = GetConfigOption(1,pclLAND1stOption,rgTM_Bill.pcpLANDConfig);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"%s GetConfigOption(1,pclLAND1stOption,rgTM_Bill.pcpLANDConfig) fails, return",pclFunc);
        return RC_FAIL;
    }
    dbg(DEBUG,"%s pclLAND1stOption<%s>",pclFunc,pclLAND1stOption);
    
    ilRc = GetConfigOption(3,pclLAND3rdOption,rgTM_Bill.pcpLANDConfig);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"%s GetConfigOption(3,pclLAND3rdOption,rgTM_Bill.pcpLANDConfig) fails, return",pclFunc);
        return RC_FAIL;
    }
    dbg(DEBUG,"%s pclLAND3rdOption<%s>",pclFunc,pclLAND3rdOption);
    
    //AIRB = DAY-2,000000,DAY-1,000000
    ilRc = GetConfigOption(1,pclAIRB1stOption,rgTM_Bill.pcpAIRBConfig);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"%s GetConfigOption(1,pclAIRB1stOption,rgTM_Bill.pcpAIRBConfig) fails, return",pclFunc);
        return RC_FAIL;
    }
    dbg(DEBUG,"%s pclAIRB1stOption<%s>",pclFunc,pclAIRB1stOption);
    
    ilRc = GetConfigOption(3,pclAIRB3rdOption,rgTM_Bill.pcpAIRBConfig);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"%s GetConfigOption(3,pclAIRB3rdOption,rgTM_Bill.pcpAIRBConfig) fails, return",pclFunc);
        return RC_FAIL;
    }
    dbg(DEBUG,"%s pclAIRB3rdOption<%s>",pclFunc,pclLAND3rdOption);
    
    strncpy(pclRef,pclLAND1stOption,strlen("DAY"));
    
    CalTimeFromCfg(pclLAND1stOption,pclRef,pcpDateString,pcpFromLAND);
    CalTimeFromCfg(pclLAND3rdOption,pclRef,pcpDateString,pcpToLAND);
    CalTimeFromCfg(pclAIRB1stOption,pclRef,pcpDateString,pcpFromAIRB);
    CalTimeFromCfg(pclAIRB3rdOption,pclRef,pcpDateString,pcpToAIRB);
    
    return RC_SUCCESS;
}

static int CalTimeFromCfg(char *pcpSource,char *pcpRef,char *pcpDateString,char *pcpDest)
{
    char *pclFunc = "CalTimeFromCfg";
    char pclTmpOffset[4]="\0";
    int ilTmpOffset = 0;
    int ilSign = 1;
    char pclTmp[256] = "\0";
    int ilMonth = 0;
    char pclMonth[32] = "\0";
    
    memset(pclTmp,0,sizeof(pclTmp));
    memset(pclTmpOffset,0,sizeof(pclTmpOffset));
    
    if(strncmp(pcpRef,"DAY",3)==0)
    {
        if(strlen(pcpSource) == (strlen(pcpRef)+2))
        {
            if(strncmp(pcpSource+strlen(pcpRef),"-",1)==0)
            {
                dbg(DEBUG,"%s ilSign is -1",pclFunc);
                ilSign = -1;
            }
            else if(strncmp(pcpSource+strlen(pcpRef),"+",1)==0)
            {
                dbg(DEBUG,"%s ilSign is 1",pclFunc);
            }
            else
            {
                dbg(TRACE,"%s unknow sign, return<%c>",*(pcpSource+strlen(pcpRef)));
                return RC_FAIL;
            }
            
            strncpy(pclTmp,pcpDateString,8);
            strcat(pclTmp,"000000");
            dbg(DEBUG,"%s pclTmp<%s>",pclFunc,pclTmp);
            
            strncpy(pclTmpOffset,pcpSource+strlen(pcpRef)+1,1);
            ilTmpOffset = atoi(pclTmpOffset);
            AddSecondsToCEDATime(pclTmp, ConvertDay2Sec(ilTmpOffset)*ilSign, 1);
            
            dbg(TRACE,"%s pclTmp<%s>",pclFunc,pclTmp);
            strncpy(pcpDest,pclTmp,8);
        }
        else if(strlen(pcpSource) == strlen(pcpRef))
        {
            dbg(DEBUG,"%s Current day",pclFunc);
            strncpy(pcpDest,pcpDateString,8);
        }
        else    if(strlen(pcpSource) == (strlen(pcpRef)+3))
        {
            if(strncmp(pcpSource+strlen(pcpRef),"-",1)==0)
            {
                dbg(DEBUG,"%s ilSign is -1",pclFunc);
                ilSign = -1;
            }
            else if(strncmp(pcpSource+strlen(pcpRef),"+",1)==0)
            {
                dbg(DEBUG,"%s ilSign is 1",pclFunc);
            }
            else
            {
                dbg(TRACE,"%s unknow sign, return<%c>",*(pcpSource+strlen(pcpRef)));
                return RC_FAIL;
            }
            
            strncpy(pclTmp,pcpDateString,8);
            strcat(pclTmp,"000000");
            dbg(DEBUG,"%s pclTmp<%s>",pclFunc,pclTmp);
            
            strncpy(pclTmpOffset,pcpSource+strlen(pcpRef)+1,2);
            ilTmpOffset = atoi(pclTmpOffset);
            AddSecondsToCEDATime(pclTmp, ConvertDay2Sec(ilTmpOffset)*ilSign, 1);
            
            dbg(TRACE,"%s pclTmp<%s>",pclFunc,pclTmp);
            strncpy(pcpDest,pclTmp,8);

        }
        else
        {
            dbg(TRACE,"Invalid Format, return");
            return RC_FAIL;
        }
    }
    else if(strncmp(pcpRef,"MONTH",5)==0)
    {
        //dbg(TRACE,"%s The reference is MONTH",pclFunc);
        
        if(strlen(pcpSource) == (strlen(pcpRef)+2))
        {
            strncpy(pclTmpOffset,pcpSource+strlen(pcpRef)+1,1);
            ilTmpOffset = atoi(pclTmpOffset);
            
            dbg(DEBUG,"%s ilTmpOffset<%d>",pclFunc,ilTmpOffset);
            
            strncpy(pclTmp,pcpDateString,4);
            if(strncmp(pcpDateString+4,"0",1)==0)
            {
                strcat(pclTmp,"0");
                strncpy(pclMonth,pcpDateString+4,1);
                ilMonth = atoi(pclMonth)-ilTmpOffset;
                memset(pclMonth,0,sizeof(pclMonth));
                if(itoa(ilMonth,pclMonth,10) == NULL)
                {
                    dbg(DEBUG,"%s Error getting month",pclFunc);
                    return RC_FAIL;
                }
                strcat(pclTmp,pclMonth);
                strcat(pclTmp,pcpDateString+5);
                strcpy(pcpDest,pclTmp);
                strcat(pcpDest,"000000");
            }
            else
            {
                strncpy(pclMonth,pcpDateString+4,2);
                //dbg(DEBUG,"%s pclMonth<%s>",pclFunc,pclMonth);
                ilMonth = atoi(pclMonth)-ilTmpOffset;
                //dbg(DEBUG,"%s ilMonth<%d>",pclFunc,ilMonth);
                memset(pclMonth,0,sizeof(pclMonth));
                if(itoa(ilMonth,pclMonth,10) == NULL)
                {
                    dbg(DEBUG,"%s Error getting month",pclFunc);
                    return RC_FAIL;
                }
                
                if(strlen(pclMonth)==1)
                {
                    strcat(pclTmp,"0");
                }
                
                dbg(DEBUG,"%s pclMonth<%s>",pclFunc,pclMonth);
                strcat(pclTmp,pclMonth);
                strcat(pclTmp,pcpDateString+6);
                dbg(DEBUG,"%s pclTmp<%s>",pclFunc,pclTmp);
                strcpy(pcpDest,pclTmp);
                strcat(pcpDest,"000000");
            }
        }
        else if(strlen(pcpSource) == strlen(pcpRef))
        {
            dbg(DEBUG,"%s Current Month",pclFunc);
            strncpy(pcpDest,pcpDateString,8);
            strcat(pcpDest,"000000");
        }
        else    if(strlen(pcpSource) == (strlen(pcpRef)+3))
        {
            strncpy(pclTmpOffset,pcpSource+strlen(pcpRef)+1,2);
            ilTmpOffset = atoi(pclTmpOffset);
            
            strncpy(pclTmp,pcpDateString,4);
            if(strncmp(pcpDateString+4,"0",1)==0)
            {
                strcat(pclTmp,"0");
                strncpy(pclMonth,pcpDateString+4,1);
                ilMonth = atoi(pclMonth)-ilTmpOffset;
                memset(pclMonth,0,sizeof(pclMonth));
                if(itoa(ilMonth,pclMonth,10) == NULL)
                {
                    dbg(DEBUG,"%s Error getting month",pclFunc);
                    return RC_FAIL;
                }
                strcat(pclTmp,pclMonth);
                strcat(pclTmp,pcpDateString+5);
                strcpy(pcpDest,pclTmp);
                strcat(pcpDest,"000000");
            }
            else
            {
                strncpy(pclMonth,pcpDateString+4,2);
                //dbg(DEBUG,"%s pclMonth<%s>",pclFunc,pclMonth);
                ilMonth = atoi(pclMonth)-ilTmpOffset;
                //dbg(DEBUG,"%s ilMonth<%d>",pclFunc,ilMonth);
                memset(pclMonth,0,sizeof(pclMonth));
                if(itoa(ilMonth,pclMonth,10) == NULL)
                {
                    dbg(DEBUG,"%s Error getting month",pclFunc);
                    return RC_FAIL;
                }
                
                if(strlen(pclMonth)==1)
                {
                    strcat(pclTmp,"0");
                }
                
                dbg(DEBUG,"%s pclMonth<%s>",pclFunc,pclMonth);
                strcat(pclTmp,pclMonth);
                strcat(pclTmp,pcpDateString+6);
                dbg(DEBUG,"%s pclTmp<%s>",pclFunc,pclTmp);
                strcpy(pcpDest,pclTmp);
                strcat(pcpDest,"000000");
            }
        }
        else
        {
            dbg(TRACE,"Invalid Format, return");
            return RC_FAIL;
        }
    }
    return RC_SUCCESS;
}

void get_fld_value(char *buf,short fld_no,char *dest)
{
    int i=0;
    char pclTmp[1024];
    int iSize=strlen(buf);
    int iTmp=1;
    int a=0;
    int b=0;
    
    strcpy(dest, "");
    if ( fld_no==0 )
    {
        strcpy(dest, "");
    }
   else
    {
        for (a=0; a<iSize; a++)
        {
        	if ( iTmp==fld_no )
                break;	
            
            if ( buf[a]==',' )
                iTmp++;
        }	
        
        memset(pclTmp, 0, sizeof(pclTmp));
        for (; a<iSize; a++)
        {
            if ( buf[a]==',' )
                break;
            else
                pclTmp[b++]=buf[a];
        }	
        strcpy(dest, pclTmp);
    }
}

int get_flds_count(char *buf)
{
    int i=0;
    char *ret;
    char myDataArea[4096] = "\0";
    strncpy(myDataArea,buf,strlen(buf));
    
    ret = strtok(myDataArea, ","); 
    while(ret != NULL)
    {
        i++;
        ret = strtok(NULL, ","); 
    }
    
    return i;
}

int get_flds_countPlus(char *buf)
{
    int i=0;
    char *ret;
    char myDataArea[4096] = "\0";
    strncpy(myDataArea,buf,strlen(buf));
    
    ret = strtok(myDataArea, "+"); 
    while(ret != NULL)
    {
        i++;
        ret = strtok(NULL, "+"); 
    }
    
    return i;
}

int get_flds_countDelimiter(char *buf,char *pclDelimiter)
{
    int i=0;
    char *ret;
    char myDataArea[4096] = "\0";
    strncpy(myDataArea,buf,strlen(buf));
    
    ret = strtok(myDataArea, pclDelimiter); 
    while(ret != NULL)
    {
        i++;
        ret = strtok(NULL, pclDelimiter); 
    }
    
    return i;
}

/*
void get_fld_valueDelimiter(char *buf,short fld_no,char *dest,char *pclDelimiter)
{
    int i=0;
    char *ret;
    char myDataArea[4096] = "\0";
    strncpy(myDataArea,buf,strlen(buf));
    
    ret = strtok(myDataArea, pclDelimiter); 
    for(i=1;i<fld_no;i++)
    {
       ret = strtok(NULL, pclDelimiter); 
  }
  strncpy(dest,ret,strlen(ret));
}
*/

int TrimSpace( char *pcpInStr )
{
    int ili = 0;
    int ilLen;
    char *pclOutStr = NULL;
    char *pclP;
    char *pclFunc = "TrimSpace";

    ilLen = strlen( pcpInStr );
    if( ilLen <= 0 )
        return;

    pclOutStr = (char *)malloc(ilLen + 10);
    pclP = pcpInStr;

    /* Trim front spaces */
    while( pclP && *pclP == ' ' )
        pclP++;

    while( *pclP )
    {
       pclOutStr[ili++] = *pclP;
       if( *pclP != ' ' )
           pclP++;
       else
       {
           while( *pclP == ' ' )
               pclP++;
       }
    }
    /* Trim back space */
    if( pclOutStr[ili-1] == ' ' )
        ili--;
    pclOutStr[ili] = '\0';
    strcpy( pcpInStr, pclOutStr );
    if( pclOutStr != NULL )
        free( (char *)pclOutStr );
}

static int AddHHMMSS(char *pclDestFrom,char *pclDestTo,char *pclConfigString)
{
    int ilRc = RC_SUCCESS;
    char *pclFunc = "AddHHMMSS";
    char pcl2ndOption[32] = "\0";
  char pcl4thOption[32] = "\0";
    
    ilRc = GetConfigOption(2,pcl2ndOption,pclConfigString);
    if(ilRc == RC_SUCCESS)
        {
            dbg(DEBUG,"%s pcl2ndOption<%s>",pclFunc,pcl2ndOption);
    }
    else
    {
        dbg(TRACE,"%s GetConfigOption(2,pcl2ndOption,pclConfigString) fails,return",pclFunc);
        return ilRc;    
    }
    
    ilRc = GetConfigOption(4,pcl4thOption,pclConfigString);
    if(ilRc == RC_SUCCESS)
        {
            dbg(DEBUG,"%s pcl4thOption<%s>",pclFunc,pcl4thOption);
    }
    else
    {
        dbg(TRACE,"%s GetConfigOption(4,pcl4thOption,pclConfigString) fails,return",pclFunc);
        return ilRc;    
    }
    
    strcat(pclDestFrom,pcl2ndOption);
    strcat(pclDestTo,pcl4thOption);
    
    return RC_SUCCESS;
}

static void TrimRight(char *s)
{
    int iRet=0;
    int iSize=strlen(s);
    int a=iSize-1;
    
    for (; a>0; a--)
    {
        if ( s[a]==' ' )
            s[a]=0;
        else
            break;    	
    }
    
    return ;
}

static int ConvertDay2Sec(int ilTime)
{
    return (ilTime*24*60*60);
}

static int GetConfigOption(int ilNo, char *pcpValue, char *OptionString)
{
    char *pclFunc = "GetConfigOption";
    int ilNumber = 0;
    
    ilNumber = get_flds_count(OptionString);
    
    if(ilNo < 0 || ilNo > ilNumber)
    {
        dbg(TRACE,"%s ilNo<%d> is out of range[0,%d]",pclFunc, ilNo, ilNumber);
        return RC_FAIL;
    }    
    
  get_fld_value(OptionString,ilNo,pcpValue);
  dbg(DEBUG,"%s pcpValue<%s>",pclFunc, pcpValue);
  
  return RC_SUCCESS;
}

static int GetWeekDayAndMonthDateStr(int *ipDay, int *ipMonth,char *pcpDateString)
{
    char *pclFunc = "GetWeekDayAndMonthDateStr";
  time_t rawtime;
  struct tm * timeinfo;
  char pclTmp[16] = "\0";

  if(time(&rawtime) == -1)
  {
      dbg(TRACE,"%s time() error, return",pclFunc);
      return RC_FAIL;
  }
  
  timeinfo = localtime(&rawtime);
  if(timeinfo == NULL)
  {
      dbg(TRACE,"%s localtime() error, return",pclFunc);
      return RC_FAIL;
  }
  
  dbg(TRACE,"%s Current local time and date: %s",pclFunc, asctime(timeinfo));
    
  dbg(DEBUG,"%s timeinfo->tm_wday<%d>,\
  tm_sec<%d>,\
  tm_min<%d>,\
  tm_hour<%d>,\
  tm_mday<%d>,\
  tm_mon<%d>,\
  tm_year,<%d>",
  pclFunc,
    timeinfo->tm_wday,
    timeinfo->tm_sec,
    timeinfo->tm_min,
    timeinfo->tm_hour,
    timeinfo->tm_mday,
    timeinfo->tm_mon,
    timeinfo->tm_year
    );

  *ipDay = timeinfo->tm_wday;  
  *ipMonth = timeinfo->tm_mon+1;
  
  sprintf(pcpDateString,"%d",timeinfo->tm_year+1900);
  
  //Jan-Sep
  if( ((timeinfo->tm_mon+1)/10) == 0 )
  {
      strcat(pcpDateString,"0");
      memset(pclTmp,0,sizeof(pclTmp));
      sprintf(pclTmp,"%d",timeinfo->tm_mon+1);
      strcat(pcpDateString,pclTmp);
  }
  else//Oct-Dec
  {
      memset(pclTmp,0,sizeof(pclTmp));
      sprintf(pclTmp,"%d",timeinfo->tm_mon+1);
      strcat(pcpDateString,pclTmp);
  }
  
  //Day1-9
  if( ((timeinfo->tm_mday)/10) == 0 )
  {
      strcat(pcpDateString,"0");
      memset(pclTmp,0,sizeof(pclTmp));
      sprintf(pclTmp,"%d",timeinfo->tm_mday);
      strcat(pcpDateString,pclTmp);
  }
  else//Day10-28|30|31
  {
      memset(pclTmp,0,sizeof(pclTmp));
      sprintf(pclTmp,"%d",timeinfo->tm_mday);
      strcat(pcpDateString,pclTmp);
  }
  return RC_SUCCESS;
}
//iGetConfigEntryWOSec(cgLoadConfigFile,rgTM_Bill.LoadCfg.pclLoadFldsCriName,CFG_STRING,rgTM_Bill.LoadCfg.pclLoadFldsCri);
int iGetConfigEntryWOSec(char *file,char *param,short type,char *dest)
{
    FILE    *fp;
    char    sCfgin[iMAX_READ_BUF];
    char    LocalSect[32];
    int    iValue;
    short    found;
    
    int ilRc = 0;
    char *pclFunc = "iGetConfigEntryWOSec";

    if ( (fp = (FILE *)fopen(file,"r")) == (FILE *)NULL) {
        return E_FOPEN;
    } /* end if */

    found = FALSE;

    while ((fgets(sCfgin,iMAX_READ_BUF,fp)) && (found == FALSE)   ) 
    {
        /*    iStrup(sCfgin);  converts a string to upper case letters */
        //if ( strncmp((char *)(sGet_word(sCfgin,1)),param,strlen(param)) == 0) {
            
            TrimRight(sCfgin);
            //dbg(DEBUG,"%s file1<%s>",pclFunc,sCfgin);
            if(strlen(sCfgin)!=0 && strncmp(sCfgin,"#",1)!=0 && strncmp(sCfgin," ",1))
            {
                //dbg(DEBUG,"%s file2<%s>",pclFunc,(char *)(sGet_word(sCfgin,1)));
                //dbg(DEBUG,"%s param<%s>",pclFunc,param); 
                
                //ilRc = strncmp((char *)(sGet_word(sCfgin,1)),param,strlen(param));
                ilRc = strcmp((char *)(sGet_word(sCfgin,1)),param);
                if(ilRc==0)
                {
                    
                    //dbg(DEBUG,"%s file1<%s>",pclFunc,sCfgin);
                    //dbg(DEBUG,"%s file2<%s>",pclFunc,(char *)(sGet_word(sCfgin,1)));
                    //dbg(DEBUG,"%s param<%s>",pclFunc,param); 
                    
                    found = TRUE;
                    switch ( type ) 
                    {
                        case    CFG_STRING     :
                            strcpy(dest,(char *)(sGet_word(sCfgin,3)));
                            break;    
                        case    CFG_INT        :
                                iValue =(int)atoi((char *)(sGet_word(sCfgin,3)));
                            memcpy(dest,&iValue,sizeof(int));
                            break;    
                    } /* end switch */
                } /* end if */
            }
    } /* end while */

    fclose(fp);
    return ( found == FALSE ) ? E_PARAM : OK;    
}

static void ExtractLoaWhereClauseValues (char *pcpArgDefn,
              char *pcpDssnList, 
              char *pcpType, char *pcpStyp, char *pcpSstp, char *pcpSsst,char *pcpApc3) 
{
  char    pclFunc[] = "ExtractLoaWhereClauseValues: ";
  char    pclTemp [ENTRY_VALUE_SIZE], *pclPtr1, *pclPtr2, pclTemp2 [ENTRY_VALUE_SIZE];
  int    jj;
  char  pclType [32],pclStyp [32], pclSstp [32], pclSsst [32],pclApc3[32];

  pclPtr1 = strstr (pcpArgDefn,"[DSSN|");
  if (pclPtr1 == '\0')
    return;

  while ((*pclPtr1) != '|')
    pclPtr1++;

  pclPtr1++;

  jj = 0;
  while ((*pclPtr1) != ']')
  {
    pcpDssnList [jj] = (*pclPtr1);
    pclPtr1++; jj++;
  }
  pcpDssnList [jj] = '\0';

  pclPtr1--;
  while ((*pclPtr1) != '[')
    pclPtr1++;

  pclPtr1++;

  pclPtr2 = pclPtr1;
  while ((*pclPtr2) != ']')
    pclPtr2++;

  memset (pclTemp2,'\0',sizeof(pclTemp2));
  strncpy (pclTemp2,pclPtr1,(pclPtr2-pclPtr1));

  (void) get_real_item (pclType, pclTemp2, 1);
  (void) get_real_item (pclStyp, pclTemp2, 2); 
  if ((strlen (pclStyp) == 0) || (pclStyp[0] == '#'))
    strcpy (pclStyp, " ");

  (void) get_real_item (pclSstp, pclTemp2, 3);
  if ((strlen (pclSstp) == 0) || (pclSstp[0] == '#'))
    strcpy (pclSstp, " ");

  (void) get_real_item (pclSsst, pclTemp2, 4);
  if ((strlen (pclSsst) == 0) || (pclSsst[0] == '#'))
    strcpy (pclSsst, " ");
   
  (void) get_real_item (pclApc3, pclTemp2, 4);
  if ((strlen (pclApc3) == 0) || (pclApc3[0] == '#'))
    strcpy (pclApc3, " ");

  // sprintf (pcpWhereClause," TYPE='%s' AND STYP='%s' AND SSTP='%s' AND SSST='%s' ",pclType,pclStyp,pclSstp,pclSsst);
  strcpy (pcpType,pclType);
  strcpy (pcpStyp,pclStyp);
  strcpy (pcpSstp,pclSstp);
  strcpy (pcpSsst,pclSsst);
  strcpy (pcpSsst,pclApc3);

  return;
}

static void InitFieldIndex()
{
    int ilCol,ilPos;
    char *pclFunc = "InitFieldIndex";
    
    //char pcgAftFields[FIELDS_LEN] = "URNO,RKEY,FLNO,FLNS,STAT,HOPO,ALC2,ALC3,VIAL,ACT5,FTYP,STOA,STOD,ORG4,DES4,REGN,LAND,AIRB,PSTA,PSTD";
    FindItemInList(pcgAftFields,"URNO",',',&igAftUrno,&ilCol,&ilPos);
    FindItemInList(pcgAftFields,"RKEY",',',&igAftRkey,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"FLNO",',',&igAftFlno,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"STAT",',',&igAftStat,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"HOPO",',',&igAftHopo,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"ALC2",',',&igAftAlc2,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"ALC3",',',&igAftAlc3,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"VIAL",',',&igAftVial,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"VIAN",',',&igAftVian,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"ACT5",',',&igAftAct5,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"FTYP",',',&igAftFtyp,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"STOA",',',&igAftStoa,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"STOD",',',&igAftStod,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"ORG3",',',&igAftOrg3,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"DES3",',',&igAftDes3,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"REGN",',',&igAftRegn,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"LAND",',',&igAftLand,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"AIRB",',',&igAftAirb,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"PSTA",',',&igAftPsta,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"PSTD",',',&igAftPstd,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"ORG4",',',&igAftOrg4,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"DES4",',',&igAftDes4,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"CDAT",',',&igAftCdat,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"TTYP",',',&igAftTtyp,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"ACT3",',',&igAftAct3,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"JFNO",',',&igAftJfno,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"JCNT",',',&igAftJcnt,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"ADHO",',',&igAftAdho,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"ADID",',',&igAftAdid,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"CSGN",',',&igAftCsgn,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"ETDI",',',&igAftEtdi,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"ETAI",',',&igAftEtai,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"DQCS",',',&igAftDqcs,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"RWYA",',',&igAftRwya,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"RWYD",',',&igAftRwyd,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"TGA1",',',&igAftTga1,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"TGD1",',',&igAftTgd1,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"STEV",',',&igAftStev,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"MOPA",',',&igAftMopa,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"MTOW",',',&igAftMtow,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"BLTO",',',&igAftBlto,&ilCol,&ilPos);
  FindItemInList(pcgAftFields,"PAID",',',&igAftPaid,&ilCol,&ilPos);

  igAftFields=get_flds_count(pcgAftFields);
  
  igErpPxhFields=get_flds_count(pcgErpPxhFields);
  igErpPxtFields=get_flds_count(pcgErpPxtFields);
  igErpPxrFields=get_flds_count(pcgErpPxrFields);
  igErpPxmFields=get_flds_count(pcgErpPxmFields);  
  igErpOffFields=get_flds_count(pcgErpOffFields);
  igErpPxoFields=get_flds_count(pcgErpPxoFields);

  
  igErpAtcFields=get_flds_count(pcgErpAtcFields);
  igErpAdhFields=get_flds_count(pcgErpAdhFields);
  igErpBillFields=get_flds_count(pcgErpBillFields);
  igErpSchFields=get_flds_count(pcgErpSchFields);

  igFpeFields=get_flds_count(pcgFpeFields);
  igActFields=get_flds_count(pcgActFields);
  
  /*
  dbg(DEBUG,"%s : igAftUrno<%d>",pclFunc,igAftUrno);
     dbg(DEBUG,"%s : igAftRkey<%d>",pclFunc,igAftRkey);
  dbg(DEBUG,"%s : igAftFlno<%d>",pclFunc,igAftFlno);
  dbg(DEBUG,"%s : igAftFlns<%d>",pclFunc,igAftFlns);
  dbg(DEBUG,"%s : igAftStat<%d>",pclFunc,igAftStat);
  dbg(DEBUG,"%s : igAftHopo<%d>",pclFunc,igAftHopo);
  dbg(DEBUG,"%s : igAftAlc2<%d>",pclFunc,igAftAlc2);
     dbg(DEBUG,"%s : igAftAlc3<%d>",pclFunc,igAftAlc3);
  dbg(DEBUG,"%s : igAftVial<%d>",pclFunc,igAftVial);
  dbg(DEBUG,"%s : igAftAct5<%d>",pclFunc,igAftAct5);
  dbg(DEBUG,"%s : igAftFtyp<%d>",pclFunc,igAftFtyp);
  dbg(DEBUG,"%s : igAftStoa<%d>",pclFunc,igAftStoa);
  dbg(DEBUG,"%s : igAftStod<%d>",pclFunc,igAftStod);
  dbg(DEBUG,"%s : igAftOrg3<%d>",pclFunc,igAftOrg3);
  dbg(DEBUG,"%s : igAftDes3<%d>",pclFunc,igAftDes3);
  dbg(DEBUG,"%s : igAftRegn<%d>",pclFunc,igAftRegn);
  dbg(DEBUG,"%s : igAftLand<%d>",pclFunc,igAftLand);
  dbg(DEBUG,"%s : igAftAirb<%d>",pclFunc,igAftAirb);
  dbg(DEBUG,"%s : igAftPsta<%d>",pclFunc,igAftPsta);
  dbg(DEBUG,"%s : igAftPstd<%d>",pclFunc,igAftPstd);
  dbg(DEBUG,"%s : igAftOrg4<%d>",pclFunc,igAftOrg4);
  dbg(DEBUG,"%s : igAftDes4<%d>",pclFunc,igAftDes4);
  */
  
  //char pcgErpBillFields[FIELDS_LEN] = "URNO,FLNU,STAT,HOPO,REGN,LAND,PSTA,PSTD";
  FindItemInList(pcgErpBillFields,"URNO",',',&igErpBillUrno,&ilCol,&ilPos);
  FindItemInList(pcgErpBillFields,"STAT",',',&igErpBillStat,&ilCol,&ilPos);
  FindItemInList(pcgErpBillFields,"HOPO",',',&igErpBillHopo,&ilCol,&ilPos);
  FindItemInList(pcgErpBillFields,"REGN",',',&igErpBillRegn,&ilCol,&ilPos);
  FindItemInList(pcgErpBillFields,"LAND",',',&igErpBillLand,&ilCol,&ilPos);
  FindItemInList(pcgErpBillFields,"PSTA",',',&igErpBillPsta,&ilCol,&ilPos);
  FindItemInList(pcgErpBillFields,"PSTD",',',&igErpBillPstd,&ilCol,&ilPos);
  
}

int doERPADH(char *pcpRkey)
{
	int ilRC=RC_SUCCESS;
    AFTREC rlAftArr;
    AFTREC rlAftDep;
    ERPADHREC stAdh;    	
    int ilSend=1;
    int ilProcessId;
    int ilCount=0;
    int ilAdhocArr=1;
    int ilAdhocDep=1;
    dbg(TRACE, "doERPADH:: Start");	
    
    ilRC=getPairedFlights(pcpRkey, &rlAftArr, &rlAftDep);   

    if ( ilRC==RC_SUCCESS )
    {
        dbg(DEBUG, "doERPADH:: Arrival: Found[%d] urno[%s] adho[%s] paid[%s]",
            rlAftArr.iFound, rlAftArr.URNO, rlAftArr.ADHO, rlAftArr.PAID); 
        dbg(DEBUG, "doERPADH:: Departure: Found[%d] urno[%s] adho[%s] paid[%s]",
            rlAftDep.iFound, rlAftDep.URNO, rlAftDep.ADHO, rlAftDep.PAID); 

        ilAdhocArr=1;
        if ( rlAftArr.iFound )
            ilAdhocArr=isAdhoc(rlAftArr.URNO, rlAftArr.ADHO, rlAftArr.PAID);
                    
        ilAdhocDep=1;
        if ( rlAftDep.iFound )
            ilAdhocDep=isAdhoc(rlAftDep.URNO, rlAftDep.ADHO, rlAftDep.PAID);
             
        dbg(TRACE, "doERPADH:: Arrival Adhoc[%d], Departure Adhoc[%d]",
            ilAdhocArr, ilAdhocDep);
        if ( !ilAdhocArr || !ilAdhocDep )
        {
            ilRC=RC_FAIL;
            dbg(TRACE, 	"doERPADH:: One of the Flight is not ADHOC!!!  Skipping!");
        }
            
        if ( ilRC==RC_SUCCESS )
        {
            ilRC=processNULLCheck("ERPADH", &rlAftArr, &rlAftDep, &ilCount);	
            dbg(TRACE, "doERPADH:: Found NULL[%d]", ilCount);
            if ( ilCount )
            {
                dbg(TRACE, "doERPADH:: ERROR!!!  Mandatory Fields NULL!");
                ilRC=RC_FAIL;
            }
        }
        
        if ( ilRC==RC_SUCCESS  )
        {
            ilRC=processDuplicates("ERPADH", &rlAftArr, &rlAftDep, &ilCount);
            dbg(TRACE, "doERPADH:: Found Duplicates[%d]", ilCount);
            if ( ilCount )
            {
                dbg(TRACE, "doERPADH:: ERROR!!!  Duplicate Found!");
                ilRC=RC_FAIL;
            }            
        }
        
        if ( ilRC==RC_SUCCESS )
        {
            ilRC=populateERPADH(&stAdh, &rlAftArr, &rlAftDep);    
            printERPADH(&stAdh);
        
            if ( ilRC==RC_SUCCESS )
            {
                if (stAdh.iFound==1)
                    ilRC=updateERPADH(&stAdh);
                else
                {
                    dbg(TRACE, "doERPADH:: Checking if Land is empty [%s]", rlAftArr.LAND);	
                    if ( strcmp(rlAftArr.LAND, "") && strcmp(rlAftArr.LAND, " ") )
                        ilRC=insertERPADH(&stAdh);
                    else
                    {
                        ilSend=0;
                        dbg(TRACE, "doERPADH:: Skipping Insert into ERPADH because no landing");
                    }
                }
                
                if ( ilRC==RC_SUCCESS )
                {
                    if ( ilSend )
                    {
                        dbg(TRACE,"doERPADH:: SENDING ADH COMMAND TO WMERPA");
                        ilRC=sendCedaEventWithLog(igWMERPAQueue,0,pcgAftDestName,pcgAftRecvName,
                                                  pcgAftTwStart,pcgAftTwEnd,"ADH","ERPADH","",
                                                 "URNO",stAdh.URNO,"",4,NETOUT_NO_ACK);    
                    } 
                    else
                    {
                        dbg(TRACE, "doERPADH:: No send to broker");	
                    } 
                }
            }
        }
    }    
    dbg(TRACE, "doERPADH:: END Result[%d]", ilRC);	
    return ilRC;    
}

int processUADHCmd(char *pcpFlnu)
{
    int ilRC=RC_SUCCESS;
    char pclSql[1024];
    short slCursor=0;
    char pclRkey[32];
    int ilSend=1;
    
    dbg(TRACE,"processUADH:: Start");
    
    dbg(TRACE, "processUADH:: strFlnu[%s] ",
        pcpFlnu);

    sprintf(pclSql,"SELECT RKEY FROM afttab WHERE urno='%s' AND (adho='X' or PAID <> 'K') ", pcpFlnu);
    dbg(DEBUG,"processUADH:: selecting in afttab[%s]", pclSql);

    ilRC=sql_if(START, &slCursor, pclSql, pclRkey);
    if ( ilRC==DB_SUCCESS )
    {
    	dbg(TRACE, "processUADH:: processing Rkey[%s]", pclRkey);
        ilRC=doERPADH(pclRkey);       
    }
    else if ( ilRC==1 )
    {
    	ilRC=RC_FAIL;
        dbg(TRACE, "processUADH:: record not found ");
    }
    else if ( ilRC==RC_FAIL )
        dbg(TRACE, "processUADH:: ERROR! DB problem");
        
    close_my_cursor(&slCursor);
        
    dbg(TRACE,"processUADH:: End Result[%d]", ilRC);
      
    return ilRC;
}

int populateERPATC(ERPATCREC *st, AFTREC *stDefault)
{
    int iRet=RC_SUCCESS;
    int iUrno;
    int iIndex;
    struct tm tmTmp;
    int iFlightFlag=0;
    char strTmp[128];
    int a=0;
    char *strPtr;

    if ( !strcmp(stDefault->ADID, "D") )
        iFlightFlag=1;
    else 
        iFlightFlag=2;
    
    dbg(DEBUG, "populateERPATC:: Default Urno[%s] iFlightFlag[%d]", 
        stDefault->URNO, iFlightFlag);    

    iRet=getERPATC((char *)stDefault->URNO, st);
        
    if ( iRet==RC_SUCCESS )
    {
        if ( st->iFound )
        {
        	
        }
        else
        {
            iUrno=NewUrnos("ERPATC",1);
            sprintf(st->URNO, "%d", iUrno);
            strcpy(st->STAT, "R");
            strcpy(st->FLNU, stDefault->URNO);
        }

        strcpy(st->FLNO, stDefault->CSGN);
        
        strcpy(st->ORG4, stDefault->ORG4);
        strcpy(st->DES4, stDefault->DES4);
        strcpy(st->REGN, stDefault->REGN);
        
        getFlightCategory(st->CATE, stDefault->TTYP, &rgERPATCConfig.rlFlightCat);       
        if ( iFlightFlag==ARRIVALFLIGHT )
        {
            convertDate(stDefault->STOA, "YYYYMMDDhhmmss", st->SCHE, "DD/MM/YYYY"); 
            strcpy(st->MOVE, "Arrival");  
            convertDate(stDefault->LAND, "YYYYMMDDhhmmss", st->FLDT, "DD/MM/YYYY");  
            convertDate(stDefault->LAND, "YYYYMMDDhhmmss", st->LAND, "hhmm");     
            strcpy(st->PARK, stDefault->PSTA); 
            strcpy(st->RUNW, stDefault->RWYA);
            strcpy(st->TERM, stDefault->TGA1);         
        }
        else if ( iFlightFlag==DEPARTUREFLIGHT )
        {
            convertDate(stDefault->STOD, "YYYYMMDDhhmmss", st->SCHE, "DD/MM/YYYY");     
            strcpy(st->MOVE, "Departure");
            convertDate(stDefault->AIRB, "YYYYMMDDhhmmss", st->FLDT, "DD/MM/YYYY");  
            convertDate(stDefault->AIRB, "YYYYMMDDhhmmss", st->AIRB, "hhmm");  
            strcpy(st->PARK, stDefault->PSTD);
            strcpy(st->RUNW, stDefault->RWYD);
            strcpy(st->TERM, stDefault->TGD1);
        }
    }
    
    dbg(DEBUG, "populateERPATC:: Default Urno[%s] iFlightFlag[%d], Result[%d]", 
        stDefault->URNO, iFlightFlag, iRet);       
    return iRet;    
}

int updateERPATC(const ERPATCREC *st)
{
    int iRet=RC_SUCCESS;
    char strSql[8192];
    char strErr[513];
    
    memset(strSql, 0, sizeof(strSql));
    sprintf(strSql, "UPDATE erpatc "
                    "SET STAT='%s'"
                    ", SCHE='%s'"
                    ", FLNO='%s'"
                    ", MOVE='%s'"
                    ", ORG4='%s'"
                    
                    ", DES4='%s'"
                    ", REGN='%s'"
                    ", FLDT='%s'"
                    ", LAND='%s'"
                    ", AIRB='%s'"
                    
                    ", CATE='%s'"
                    ", PARK='%s'"
                    ", RUNW='%s'"
                                      
                    "WHERE URNO='%s' ",
                    st->STAT, st->SCHE, st->FLNO, st->MOVE, st->ORG4, 
                    st->DES4, st->REGN, st->FLDT, st->LAND, st->AIRB, 
                    st->CATE, st->PARK, st->RUNW, st->URNO);    

    iRet=executeSql(strSql,strErr);               

    return iRet;    
}

int insertERPATC(const ERPATCREC *st)
{
    int iRet=RC_SUCCESS;
    char strSql[8192];
    char strErr[513];
    
    memset(strSql, 0, sizeof(strSql));
    strcpy(strSql, "INSERT INTO erpatc "
                   "("
                   "URNO, FLNU, STAT, SCHE, FLNO, "
                   "MOVE, ORG4, DES4, REGN, FLDT, "
                   "LAND, AIRB, CATE, PARK, RUNW, "
                   "TERM"
                   ")");
    sprintf(strSql, "%s VALUES "
                    "("
                    "'%s', '%s', '%s', '%s', '%s',"
                    "'%s', '%s', '%s', '%s', '%s',"
                    "'%s', '%s', '%s', '%s', '%s',"
                    "'%s' "
                    ")",
            strSql, 
            st->URNO, st->FLNU, st->STAT, st->SCHE, st->FLNO,    
            st->MOVE, st->ORG4, st->DES4, st->REGN, st->FLDT,     
            st->LAND, st->AIRB, st->CATE, st->PARK, st->RUNW,   
            st->TERM);    
            
    iRet=executeSql(strSql,strErr);               

    return iRet;    
}


int getERPATC(char *strFlnu, ERPATCREC *st)
{
    int iRet=RC_SUCCESS;
    short slCursor = 0;
    char strSql[2048];
    char pclDataBuf[32728];
        
    dbg(DEBUG, "getERPATC:: strFlnu[%s]", strFlnu);

    memset(st, 0, sizeof(ERPATCREC));
    
    sprintf(strSql, "SELECT %s FROM erpatc WHERE flnu='%s' ", pcgErpAtcFields, strFlnu);
    
    memset(pclDataBuf, 0, sizeof(pclDataBuf));
    iRet=sql_if (START, &slCursor, strSql, pclDataBuf);  
    dbg(DEBUG, "getERPATC:: executing [%s][%d]", strSql, iRet);
    
    if ( iRet==DB_SUCCESS )
    {
        st->iFound=1;
        BuildItemBuffer (pclDataBuf, pcgErpAtcFields, igErpAtcFields, ",");
        get_real_item(st->URNO,pclDataBuf,1);         
        get_real_item(st->FLNU,pclDataBuf,2);         
        get_real_item(st->STAT,pclDataBuf,3);     
    }
    else if ( iRet==1 )
    {
        dbg(DEBUG, "getERPATC:: Record not found");
        iRet=RC_SUCCESS;
        st->iFound=0;
    }
    else if ( iRet==-1 )
    {
        dbg(DEBUG, "getERPATC:: ERROR!!! DB Failed");
        st->iFound=0;
    }
    close_my_cursor(&slCursor);
        
    dbg(DEBUG, "getERPATC:: strFlnu[%s] Result[%d]", strFlnu, iRet);
    return iRet;
}

void printERPATC(ERPATCREC *stERP)
{
    dbg(DEBUG, "printERPATC:: Start");
   
    dbg(DEBUG, "printERPATC:: iFound[%d]", stERP->iFound);
    dbg(DEBUG, "printERPATC:: URNO[%s]", stERP->URNO);
    dbg(DEBUG, "printERPATC:: FLNU[%s]", stERP->FLNU);
    dbg(DEBUG, "printERPATC:: STAT[%s]", stERP->STAT);
    dbg(DEBUG, "printERPATC:: SCHE[%s]", stERP->SCHE);
    dbg(DEBUG, "printERPATC:: FLNO[%s]", stERP->FLNO);

    dbg(DEBUG, "printERPATC:: MOVE[%s]", stERP->MOVE);
    dbg(DEBUG, "printERPATC:: ORG4[%s]", stERP->ORG4);
    dbg(DEBUG, "printERPATC:: DES4[%s]", stERP->DES4);
    dbg(DEBUG, "printERPATC:: REGN[%s]", stERP->REGN);
    dbg(DEBUG, "printERPATC:: FLDT[%s]", stERP->FLDT);

    dbg(DEBUG, "printERPATC:: LAND[%s]", stERP->LAND);
    dbg(DEBUG, "printERPATC:: AIRB[%s]", stERP->AIRB);
    dbg(DEBUG, "printERPATC:: CATE[%s]", stERP->CATE);
    dbg(DEBUG, "printERPATC:: PARK[%s]", stERP->PARK);
    dbg(DEBUG, "printERPATC:: RUNW[%s]", stERP->RUNW);
    dbg(DEBUG, "printERPATC:: TERM[%s]", stERP->TERM);

    dbg(DEBUG, "printERPATC:: End");

    return;        
}

int getAircraftConfigLoad(char *pcpFlnu, char *pcpAdid, char *pcpOut)
{
    int ilRC=RC_SUCCESS;
    int ilTmp=0;
    _LOATAB *rl;
    int ilSize=0;
    int ilFirst=0;
    int ilBusiness=0;
    int ilEconomy=0;
    char pclTmp[1024];
    
    dbg(DEBUG, "getAircraftConfigLoad:: Start pcpFlnu[%s], pcpAdid[%s]", pcpFlnu, pcpAdid);
    strcpy(pcpOut, "");
    
    if ( !strcmp(pcpAdid, "A") )
    {
        rl=rgLoatabArr;
        ilSize=igLoatabArrCount;
    }
    else
    {
        rl=rgLoatabDep;
        ilSize=igLoatabDepCount;
    }
    	
    ilTmp=getFormulaValue("LOAD_CONFIG_FIRST", rl, ilSize, pclTmp);
    ilFirst=atoi(pclTmp);

    ilTmp=getFormulaValue("LOAD_CONFIG_BUSINESS", rl, ilSize, pclTmp);
    ilBusiness=atoi(pclTmp);

    ilTmp=getFormulaValue("LOAD_CONFIG_ECONOMY", rl, ilSize, pclTmp);
    ilEconomy=atoi(pclTmp);
    ilTmp=getFormulaValue("LOAD_CONFIG_ECONOMY2", rl, ilSize, pclTmp);
    ilEconomy+=atoi(pclTmp);
    
    if ( !ilFirst && !ilBusiness && !ilEconomy )
        sprintf(pcpOut, "%dF%dC%dE", ilFirst, ilBusiness, ilEconomy);
    else
        sprintf(pcpOut, "%02dF%02dC%02dE", ilFirst, ilBusiness, ilEconomy);
    
    dbg(DEBUG, "getAircraftConfigLoad:: End Result[%d] pcpOut[%s]", ilRC, pcpOut);
    return ilRC;	
}

int populateERPBILL(ERPBILLREC *st, AFTREC *stAftArr, AFTREC *stAftDep)
{
    int iRet=RC_SUCCESS;
    int iTmp;
    int iUrno;
    int iFlightFlag=0;
    char strTmp01[128];
    char strTmp02[128];
    AFTREC *stDefault;
    ACTTABREC stAct;
    double dDiff;

    memset(st, 0, sizeof(ERPBILLREC));
    if ( stAftDep->iFound )
    {
        iFlightFlag|=1;
        stDefault=stAftDep;
    }
        
    if ( stAftArr->iFound )
    {
        iFlightFlag|=2;
        stDefault=stAftArr;
    }   
        
    dbg(TRACE, "populateERPBILL:: Default Urno[%s] Arrival Urno[%s], Departure Urno[%s], iFlightFlag[%d]", 
        stDefault->URNO, stAftArr->URNO, stAftDep->URNO, iFlightFlag);    


    if ( iFlightFlag==0 )
    {
        dbg(TRACE, "populateERPBILL:: ERROR!!! No URNO to process.");	
        iRet=RC_FAIL;
    }
    else
    {    	
        iRet=getERPBILL((char *)stDefault->URNO, st);
        
        if ( iRet==RC_SUCCESS )
        {
            if ( st->iFound )
            {
                dbg(TRACE, "populateERPBILL:: Record for update");	
            }
            else
            {
                dbg(TRACE, "populateERPBILL:: Record for insert");	
                
                iUrno=NewUrnos("ERPBILL",1);
                sprintf(st->URNO, "%d", iUrno);	
                strcpy(st->FLNU, stDefault->URNO);
            }
            
            strcpy(st->STAT, "E");
            strcpy(st->HOPO, cgHopo);
/*                
            getSysDate(strTmp01);
            sprintf(strTmp01, "%8.8s", strTmp01);   
            sprintf(strTmp02, "%8.8s", stDefault->CDAT);     
            timeDiff(strTmp01, strTmp02, &dDiff);
            if ( dDiff<=(60*60*24*4) )
                strcpy(st->SCHE, "NO");
            else
                strcpy(st->SCHE, "YES");    
            dbg(DEBUG,  "populateERPBILL:: System Date[%s] CDAT[%s] [%0.2lf]<=[%0.2lf] SCHE[%s]",
                strTmp01, strTmp02, dDiff, (double)(60*60*24*4), st->SCHE);
*/        
            if ( !strcmp(stAftArr->FLNO, stAftDep->FLNO) && strcmp(stAftArr->ORG3, stAftDep->DES3) )
                strcpy(st->STOP, "Transit");    
            else
                strcpy(st->STOP, "Turnaround");
                   
            strcpy(st->ACT3, stDefault->ACT5);
            strcpy(st->REGN, stDefault->REGN);
            sprintf(st->MMVN, "M%s", st->URNO);
            strcpy(st->ACSZ, stDefault->acszBill);
            strcpy(st->UMVN,st->URNO);
            sprintf(st->MTOW, "%d", atoi(stDefault->MTOW)*1000);
            getFlightCategory(st->CATE, stDefault->TTYP, &rgERPBILLConfig.rlFlightCat);
            

            if ( stAftDep->iFound )
            {
                if ( !strcmp(stAftDep->ADHO, "X") )
                    strcpy(st->TYPE, "ADHOC");
                else   
                    strcpy(st->TYPE, stAftDep->typeBill);            	
            }
            else
            {	
                if ( !strcmp(stDefault->ADHO, "X") )
                    strcpy(st->TYPE, "ADHOC");
                else   
                    strcpy(st->TYPE, stDefault->typeBill);
            } 
            getAircraftConfigLoad(stDefault->URNO, stDefault->ADID, st->CFGL);
            
            if ( iFlightFlag==DEPARTUREFLIGHT )
            {
            	strcpy(st->ALFN, stAftDep->alfn);
            	
   	            if ( !strcmp(stAftDep->ADHO, "X") )
   	                strcpy(st->SCHE, "NO");
   	            else
   	                strcpy(st->SCHE, "YES");	    

                strcpy(st->MOVE, "Departure"); 
                removeSpace(stAftDep->FLNO, st->DFLT);
                if ( !strcmp(st->DFLT, "") )
                    strcpy(st->DFLT, stAftDep->CSGN);
                
                strcpy(strTmp01, stAftDep->STOD); 
                AddSecondsToCEDATime(strTmp01, igTimeZone*60*60, 1);    
                convertDate(strTmp01, "YYYYMMDDhhmmss", st->DDAT, "DD/MM/YYYY");
                convertDate(strTmp01, "YYYYMMDDhhmmss", st->STTD, "hhmm");
                
                strcpy(st->DES3, stAftDep->DES3);
                strcpy(st->PSTD, stAftDep->PSTD);
                
             	strcpy(strTmp01, stAftDep->AIRB); 
                AddSecondsToCEDATime(strTmp01, igTimeZone*60*60, 1);    
                convertDate(strTmp01, "YYYYMMDDhhmmss", st->AIRB, "DD/MM/YYYY");      
                sprintf(st->ADEP, "%d", convertTimeToSec(&strTmp01[8]));     
                getSTEVMap(stAftDep->STEV, st->TERM);              	
            }
            else if ( iFlightFlag==ARRIVALFLIGHT )
            {
   	            if ( !strcmp(stAftArr->ADHO, "X") )
   	                strcpy(st->SCHE, "NO");
   	            else
   	                strcpy(st->SCHE, "YES");
   	            
   	            strcpy(st->MOVE, "Arrival");
                removeSpace(stAftArr->FLNO, st->ALFT);
                if ( !strcmp(st->ALFT, "") )
                    strcpy(st->ALFT, stAftArr->CSGN);
                    
                strcpy(strTmp01, stAftArr->STOA); 
                AddSecondsToCEDATime(strTmp01, igTimeZone*60*60, 1);    
                convertDate(strTmp01, "YYYYMMDDhhmmss", st->ADAT, "DD/MM/YYYY");
                convertDate(strTmp01, "YYYYMMDDhhmmss", st->STTA, "hhmm");
                
                strcpy(st->ORG3, stAftArr->ORG3);
                strcpy(st->PSTA, stAftArr->PSTA);
                
                strcpy(strTmp01, stAftArr->LAND); 
                AddSecondsToCEDATime(strTmp01, igTimeZone*60*60, 1);                    
                convertDate(strTmp01, "YYYYMMDDhhmmss", st->LAND, "DD/MM/YYYY");                               
                sprintf(st->AARR, "%d", convertTimeToSec(&strTmp01[8]));                 
            }
            else
            {
            	strcpy(st->ALFN, stAftDep->alfn);
            	
   	            if ( !strcmp(stAftArr->ADHO, "X") && !strcmp(stAftDep->ADHO, "X"))
   	                strcpy(st->SCHE, "NO");
   	            else
   	                strcpy(st->SCHE, "YES");
            	
                strcpy(st->MOVE, "Arrival/Departure");	
                removeSpace(stAftArr->FLNO, st->ALFT);
                if ( !strcmp(st->ALFT, "") )
                    strcpy(st->ALFT, stAftArr->CSGN);
                    
                strcpy(strTmp01, stAftArr->STOA); 
                AddSecondsToCEDATime(strTmp01, igTimeZone*60*60, 1);    
                convertDate(strTmp01, "YYYYMMDDhhmmss", st->ADAT, "DD/MM/YYYY");
                convertDate(strTmp01, "YYYYMMDDhhmmss", st->STTA, "hhmm");
                
                strcpy(st->ORG3, stAftArr->ORG3);
                strcpy(st->PSTA, stAftArr->PSTA);
                
                strcpy(strTmp01, stAftArr->LAND); 
                AddSecondsToCEDATime(strTmp01, igTimeZone*60*60, 1);    
                convertDate(strTmp01, "YYYYMMDDhhmmss", st->LAND, "DD/MM/YYYY");
                sprintf(st->AARR, "%d", convertTimeToSec(&strTmp01[8]));
                
                removeSpace(stAftDep->FLNO, st->DFLT);
                if ( !strcmp(st->DFLT, "") )
                    strcpy(st->DFLT, stAftDep->CSGN);
                                    
                strcpy(strTmp01, stAftDep->STOD); 
                AddSecondsToCEDATime(strTmp01, igTimeZone*60*60, 1);    
                convertDate(strTmp01, "YYYYMMDDhhmmss", st->DDAT, "DD/MM/YYYY");
                convertDate(strTmp01, "YYYYMMDDhhmmss", st->STTD, "hhmm");                
                
                strcpy(st->DES3, stAftDep->DES3);
                strcpy(st->PSTD, stAftDep->PSTD);
                
             	strcpy(strTmp01, stAftDep->AIRB); 
                AddSecondsToCEDATime(strTmp01, igTimeZone*60*60, 1);    
                convertDate(strTmp01, "YYYYMMDDhhmmss", st->AIRB, "DD/MM/YYYY");      
                sprintf(st->ADEP, "%d", convertTimeToSec(&strTmp01[8]));                  
                
                getSTEVMap((char *)stAftDep->STEV, st->TERM); 
            }
        }
    }
    dbg(DEBUG, "populateERPATC:: Arrival Urno[%s], Departure Urno[%s], iFlightFlag[%d], Result[%d]", 
        stAftArr->URNO, stAftDep->URNO, iFlightFlag, iRet);       
    return iRet;    
}

int getERPBILL(char *strFlnu, ERPBILLREC *stERP)
{
    int iRet=RC_SUCCESS;
    short slCursor = 0;
    char strSql[2048];
    char pclDataBuf[32728];
        
    dbg(DEBUG, "getERPBILL:: strFlnu[%s]", strFlnu);

    memset(stERP, 0, sizeof(ERPBILLREC));
    
    sprintf(strSql, "SELECT %s FROM erpbill WHERE flnu='%s' ", pcgErpBillFields, strFlnu);

    memset(pclDataBuf, 0, sizeof(pclDataBuf));
    iRet=sql_if (START, &slCursor, strSql, pclDataBuf);  
    dbg(DEBUG, "getERPBILL:: executing [%s][%d]", strSql, iRet);    

    if ( iRet==DB_SUCCESS )
    {
        stERP->iFound=1;
        BuildItemBuffer (pclDataBuf, pcgErpBillFields, igErpBillFields, ",");

        get_real_item(stERP->URNO,pclDataBuf,1);    
        get_real_item(stERP->FLNU,pclDataBuf,2);         
        get_real_item(stERP->STAT,pclDataBuf,3);     
    }
    else if ( iRet==1 )
    {
        dbg(DEBUG, "getERPBILL:: Record not found");
        iRet=RC_SUCCESS;
        stERP->iFound=0;
    }
    else if ( iRet==-1 )
    {
        dbg(DEBUG, "getERPBILL:: ERROR!!! DB Failed");
        stERP->iFound=0;
    }
    close_my_cursor(&slCursor);
        
    dbg(DEBUG, "getERPBILL:: strFlnu[%s] Result[%d]", strFlnu, iRet);
    return iRet;
}

void printERPBILL(ERPBILLREC *stERP)
{
    dbg(DEBUG, "printERPBILL:: Start");
   
    dbg(DEBUG, "printERPBILL:: iFound[%d]", stERP->iFound);
    dbg(DEBUG, "printERPBILL:: URNO[%s]", stERP->URNO);
    dbg(DEBUG, "printERPBILL:: FLNU[%s]", stERP->FLNU);
    dbg(DEBUG, "printERPBILL:: STAT[%s]", stERP->STAT);
    dbg(DEBUG, "printERPBILL:: HOPO[%s]", stERP->HOPO);
    dbg(DEBUG, "printERPBILL:: ALFN[%s]", stERP->ALFN);

    dbg(DEBUG, "printERPBILL:: SCHE[%s]", stERP->SCHE);
    dbg(DEBUG, "printERPBILL:: STOP[%s]", stERP->STOP);
    dbg(DEBUG, "printERPBILL:: MOVE[%s]", stERP->MOVE);
    dbg(DEBUG, "printERPBILL:: ACT3[%s]", stERP->ACT3);
    dbg(DEBUG, "printERPBILL:: REGN[%s]", stERP->REGN);

    dbg(DEBUG, "printERPBILL:: ALFT[%s]", stERP->ALFT);
    dbg(DEBUG, "printERPBILL:: ADAT[%s]", stERP->ADAT);
    dbg(DEBUG, "printERPBILL:: ORG3[%s]", stERP->ORG3);
    dbg(DEBUG, "printERPBILL:: PSTA[%s]", stERP->PSTA);
    dbg(DEBUG, "printERPBILL:: LAND[%s]", stERP->LAND);
    
    dbg(DEBUG, "printERPBILL:: DFLT[%s]", stERP->DFLT);
    dbg(DEBUG, "printERPBILL:: DDAT[%s]", stERP->DDAT);
    dbg(DEBUG, "printERPBILL:: DES3[%s]", stERP->DES3);
    dbg(DEBUG, "printERPBILL:: PSTD[%s]", stERP->PSTD);
    dbg(DEBUG, "printERPBILL:: AIRB[%s]", stERP->AIRB);

    dbg(DEBUG, "printERPBILL:: CFGL[%s]", stERP->CFGL);
    dbg(DEBUG, "printERPBILL:: MMVN[%s]", stERP->MMVN);
    dbg(DEBUG, "printERPBILL:: ACSZ[%s]", stERP->ACSZ);
    dbg(DEBUG, "printERPBILL:: UMVN[%s]", stERP->UMVN);
    dbg(DEBUG, "printERPBILL:: STTA[%s]", stERP->STTA);

    dbg(DEBUG, "printERPBILL:: STTD[%s]", stERP->STTD);
    dbg(DEBUG, "printERPBILL:: AARR[%s]", stERP->AARR);
    dbg(DEBUG, "printERPBILL:: ADEP[%s]", stERP->ADEP);
    dbg(DEBUG, "printERPBILL:: TERM[%s]", stERP->TERM);
    dbg(DEBUG, "printERPBILL:: MTOW[%s]", stERP->MTOW);

    dbg(DEBUG, "printERPBILL:: CATE[%s]", stERP->CATE);
    dbg(DEBUG, "printERPBILL:: TYPE[%s]", stERP->TYPE);

    dbg(DEBUG, "printERPBILL:: End");
                                                                                                                    
    return;        
}

int insertERPBILL(const ERPBILLREC *st)
{
    int iRet=RC_SUCCESS;
    char strSql[8192];
    char strErr[513];
    
    memset(strSql, 0, sizeof(strSql));
    strcpy(strSql, "INSERT INTO erpbill "
                   "("
                   "URNO, FLNU, STAT, HOPO, ALFN, "
                   "SCHE, STOP, MOVE, ACT3, REGN, "
                   "ALFT, ADAT, ORG3, PSTA, LAND, "
                   "DFLT, DDAT, DES3, PSTD, AIRB, "
                   "CFGL, MMVN, ACSZ, UMVN, STTA, "
                   "STTD, AARR, ADEP, TERM, MTOW, "
                   "CATE, TYPE "
                   ")");
                   
    sprintf(strSql, "%s VALUES "
                    "("
                    "'%s', '%s', '%s', '%s', '%s',"
                    "'%s', '%s', '%s', '%s', '%s',"
                    "'%s', '%s', '%s', '%s', '%s',"
                    "'%s', '%s', '%s', '%s', '%s',"
                    "'%s', '%s', '%s', '%s', '%s',"
                    "'%s', '%s', '%s', '%s', '%s',"
                    "'%s', '%s' "
                    ")",
            strSql, 
            st->URNO, st->FLNU, st->STAT, st->HOPO, st->ALFN,    
            st->SCHE, st->STOP, st->MOVE, st->ACT3, st->REGN,     
            st->ALFT, st->ADAT, st->ORG3, st->PSTA, st->LAND,   
            st->DFLT, st->DDAT, st->DES3, st->PSTD, st->AIRB,   
            st->CFGL, st->MMVN, st->ACSZ, st->UMVN, st->STTA,   
            st->STTD, st->AARR, st->ADEP, st->TERM, st->MTOW,   
            st->CATE, st->TYPE);    

    iRet=executeSql(strSql,strErr);               

    return iRet;    
}

int updateERPBILL(const ERPBILLREC *st)
{
    int iRet=RC_SUCCESS;
    char strSql[8192];
    char strErr[513];
    
    memset(strSql, 0, sizeof(strSql));
    sprintf(strSql, "UPDATE erpbill "
                    "SET STAT='%s'"
                    ", HOPO='%s'"
                    ", ALFN='%s'"
                    ", SCHE='%s'"
                    ", STOP='%s'"
                    
                    ", MOVE='%s'"
                    ", ACT3='%s'"
                    ", REGN='%s'"
                    ", ALFT='%s'"
                    ", ADAT='%s'"
                    
                    ", ORG3='%s'"
                    ", PSTA='%s'"
                    ", LAND='%s'"
                    ", DFLT='%s'"
                    ", DDAT='%s'"

                    ", DES3='%s'"
                    ", PSTD='%s'"
                    ", AIRB='%s'"
                    ", CFGL='%s'"
                    ", MMVN='%s'"
                                                                                                                                                                                
                    ", ACSZ='%s'"
                    ", UMVN='%s'"
                    ", STTA='%s'"
                    ", STTD='%s'"
                    ", AARR='%s'"
                                                                                                                                                                               
                    ", ADEP='%s'"
                    ", TERM='%s'"
                    ", MTOW='%s'"
                    ", CATE='%s'"
                    ", TYPE='%s'"

                    "WHERE URNO='%s' ",
                    
                    st->STAT, st->HOPO, st->ALFN, st->SCHE, st->STOP, 
                    st->MOVE, st->ACT3, st->REGN, st->ALFT, st->ADAT, 
                    st->ORG3, st->PSTA, st->LAND, st->DFLT, st->DDAT, 
                    st->DES3, st->PSTD, st->AIRB, st->CFGL, st->MMVN,
                    st->ACSZ, st->UMVN, st->STTA, st->STTD, st->AARR,
                    st->ADEP, st->TERM, st->MTOW, st->CATE, st->TYPE,
                    st->URNO);    
                                                                                                                                                                                 
    iRet=executeSql(strSql,strErr);               

    return iRet;    
}

int updateERPADH(const ERPADHREC *st)
{
    int iRet=RC_SUCCESS;
    char strSql[8192];
    char strErr[513];
    
    memset(strSql, 0, sizeof(strSql));
    sprintf(strSql, "UPDATE erpadh "
                    "SET STAT='%s'"
                    ", AFLT='%s'"
                    ", ADAT='%s'"
                    ", REGN='%s'"
                    ", CATE='%s'"
                    
                    ", LAND='%s'"
                    ", DDAT='%s'"
                    ", ETDI='%s'"
                    ", MOPA='%s'"
                    ", ALFN='%s'"
                    
                    ", CDAT='%s'"
                    ", PSTA='%s'"
                    ", AIRB='%s'"
                    ", OPER='%s'"
                    ", HOPO='%s'"

                    ", MTOW='%s'"
                    ", RCNO='%s'"
                    ", ETAI='%s'"
                    ", TXND='%s'"
                    ", PERM='%s'"
                    
                    "WHERE URNO='%s' ",
                    st->STAT, st->AFLT, st->ADAT, st->REGN, st->CATE, 
                    st->LAND, st->DDAT, st->ETDI, st->MOPA, st->ALFN, 
                    st->CDAT, st->PSTA, st->AIRB, st->OPER, st->HOPO, 
                    st->MTOW, st->RCNO, st->ETAI, st->TXND, st->PERM,
                    st->URNO);    
            
    iRet=executeSql(strSql,strErr);               

    return iRet;    
}

int insertERPADH(const ERPADHREC *st)
{
    int iRet=RC_SUCCESS;
    char strSql[8192];
    char strErr[513];
    
    memset(strSql, 0, sizeof(strSql));
    strcpy(strSql, "INSERT INTO erpadh "
                   "("
                   "URNO, FLNU, STAT, AFLT, ADAT, "
                   "REGN, CATE, LAND, DDAT, ETDI, "
                   "MOPA, ALFN, CDAT, PSTA, AIRB, "
                   "OPER, HOPO, MTOW, RCNO, ETAI, "
                   "TXND, PERM"
                   ")");
    sprintf(strSql, "%s VALUES "
                    "("
                    "'%s', '%s', '%s', '%s', '%s',"
                    "'%s', '%s', '%s', '%s', '%s',"
                    "'%s', '%s', '%s', '%s', '%s',"
                    "'%s', '%s', '%s', '%s', '%s',"
                    "'%s', '%s'"
                    ")",
            strSql, 
            st->URNO, st->FLNU, st->STAT, st->AFLT, st->ADAT,    
            st->REGN, st->CATE, st->LAND, st->DDAT, st->ETDI,     
            st->MOPA, st->ALFN, st->CDAT, st->PSTA, st->AIRB,   
            st->OPER, st->HOPO, st->MTOW, st->RCNO, st->ETAI,  
            st->TXND, st->PERM);    
            
    iRet=executeSql(strSql,strErr);               

    return iRet;    
}

int checkDuplicate(AFTREC *rpParam, char *pcpUrnos, char *pcpDuplicates)
{
    int ilRC=RC_SUCCESS;
    int ilTmp=RC_SUCCESS;
    char pclSql[1024];
    char pclData[2048];
    char pclFields[]="URNO,FLNO,CSGN,STOA,STOD,ORG3,DES3";
    int ilFieldCount=7;
    char pclUrno[16];
    char pclFlno[16];
    char pclCsgn[16];
    char pclStoa[16];
    char pclStod[16];
    char pclOrg3[4];
    char pclDes3[4];
    short slCursor=0;
    
    dbg(TRACE, "checkDuplicate:: Start");   
    
    strcpy(pcpDuplicates,"");
    strcpy(pcpUrnos, "");
    
    if ( !strcmp(rpParam->ADHO, "X") )
    {
        if ( !strcmp(rpParam->ADID, "A") )
        {
            sprintf(pclSql, "SELECT %s FROM AFTTAB WHERE CSGN='%s' AND STOA='%s' AND URNO!='%s' ",
                    pclFields, rpParam->CSGN, rpParam->STOA, rpParam->URNO);
        }
        else if ( !strcmp(rpParam->ADID, "D") )
        {
            sprintf(pclSql, "SELECT %s FROM AFTTAB WHERE CSGN='%s' AND STOD='%s' AND URNO!='%s' ",
                    pclFields, rpParam->CSGN, rpParam->STOD, rpParam->URNO);   	
        }    	
    }
    else
    {
        if ( !strcmp(rpParam->ADID, "A") )
        {
        	if ( strcmp(rpParam->FLNO, "") )
        	{
                sprintf(pclSql, "SELECT %s FROM AFTTAB WHERE FLNO='%s' AND STOA='%s' AND URNO!='%s' ",
                        pclFields, rpParam->FLNO, rpParam->STOA, rpParam->URNO);
            }
            else
            {
                sprintf(pclSql, "SELECT %s FROM AFTTAB WHERE CSGN='%s' AND STOA='%s' AND URNO!='%s' ",
                        pclFields, rpParam->CSGN, rpParam->STOA, rpParam->URNO);
            }
        }
        else if ( !strcmp(rpParam->ADID, "D") )
        {
        	if ( strcmp(rpParam->FLNO, "") )
            {
                sprintf(pclSql, "SELECT %s FROM AFTTAB WHERE FLNO='%s' AND STOD='%s' AND URNO!='%s' ",
                        pclFields, rpParam->FLNO, rpParam->STOD, rpParam->URNO);   	
            }
            else
            {
                sprintf(pclSql, "SELECT %s FROM AFTTAB WHERE CSGN='%s' AND STOD='%s' AND URNO!='%s' ",
                        pclFields, rpParam->CSGN, rpParam->STOD, rpParam->URNO);   	
            }
        }
    }
    
    ilRC=sql_if(START, &slCursor, pclSql, pclData);
    dbg(TRACE, "checkDuplicate:: executing[%s], Result[%d]", pclSql, ilRC);

    if ( ilRC==RC_FAIL )
    {
        dbg(TRACE, "checkDuplicate:: DB ERROR!");	
    }
    else if ( ilRC==ORA_NOT_FOUND )
    {
        dbg(TRACE, "checkDuplicate:: No record found!");
        ilRC=RC_SUCCESS;	
    }
    else
    {
        while ( ilTmp==RC_SUCCESS )
        {
            BuildItemBuffer(pclData, pclFields, ilFieldCount, ",");
            get_real_item(pclUrno,pclData,1); 
            get_real_item(pclFlno,pclData,2); 
            get_real_item(pclCsgn,pclData,3);   
            get_real_item(pclStoa,pclData,4);   
            get_real_item(pclStod,pclData,5);   
            get_real_item(pclOrg3,pclData,6);   
            get_real_item(pclDes3,pclData,7);   
            
            dbg(TRACE, "checkDuplicate:: URNO[%s], FLNO[%s], CSGN[%s], STOA[%s], STOD[%s], ORG3[%s], DES3[%s]",
                pclUrno, pclFlno, pclCsgn, pclStoa, pclStod, pclOrg3, pclDes3);
             
            if ( !strcmp(pcpUrnos, "") )
                strcpy(pcpUrnos, pclUrno);
            else
                sprintf(pcpUrnos, "%s,%s", pcpUrnos, pclUrno);
            	   
            sprintf(pcpDuplicates, "%s%s,%s,%s,%s,%s,%s,%s\n",
                    pcpDuplicates, pclUrno, pclFlno, pclStoa, pclStod, pclOrg3, pclDes3, pclCsgn);
            ilTmp=sql_if(NEXT, &slCursor, pclSql, pclData);   	
        }	
    }
    close_my_cursor(&slCursor); 
    
    dbg(DEBUG, "checkDuplicate:: pcpDuplicates[%s] pcpUrnos[%s]", pcpDuplicates, pcpUrnos);
    dbg(TRACE, "checkDuplicate:: End Result[%d]", ilRC);
    return ilRC;	
}

int populateERPADH(ERPADHREC *stERP, AFTREC *stAftArr, AFTREC *stAftDep)
{
    int iRet=RC_SUCCESS;
    int iFlightFlag=0;
    int iTmp=0;
    int iUrno;
    char strTmp[32];
    char strSysDate[32];
    FPETABREC stFpe;
    AFTREC *stDefault;
    
    memset(stERP, 0, sizeof(ERPADHREC));
    if ( stAftDep->iFound )
    {
        iFlightFlag|=1;
        stDefault=stAftDep;
    }
        
    if ( stAftArr->iFound )
    {
        iFlightFlag|=2;
        stDefault=stAftArr;
    }

    dbg(DEBUG, "populateERPADH:: Arrival Urno[%s], Departure Urno[%s] iFlightFlag[%d]", 
        stAftArr->URNO, stAftDep->URNO, iFlightFlag);

    iRet=getERPADH((char *)stDefault->URNO, stERP);
    if ( iRet==RC_SUCCESS )
    {
    	if ( stDefault->ADID[0]=='A' )
            iRet=getFPETAB(stDefault->CSGN, stDefault->REGN, stDefault->ACT5, stDefault->STOA, &stFpe);
        else
            iRet=getFPETAB(stDefault->CSGN, stDefault->REGN, stDefault->ACT5, stDefault->STOD, &stFpe);
    }
        
    if ( iRet==RC_SUCCESS )
    {
        if ( stERP->iFound )
        {
            if ( 
                   strcmp(stERP->STAT, "R")  &&
            	   strcmp(stERP->STAT, "F")
                )	
            {
                strcpy(stERP->STAT, "U");
            }        	
        }
        else
        {
            iUrno=NewUrnos("ERPADH",1);
            sprintf(stERP->URNO, "%d", iUrno);                        
            strcpy(stERP->FLNU, stDefault->URNO);   
            
            strcpy(stERP->STAT, "R");                    	
        }
        
        
        strcpy(stERP->AFLT, stAftArr->CSGN);

        strcpy(stERP->REGN, stDefault->REGN);
        strcpy(stERP->CATE, "NS");
        

        if ( !strcmp(stDefault->MOPA, "K") )
            strcpy(stERP->MOPA, "Credit");
        else if ( !strcmp(stDefault->MOPA, "B") )
            strcpy(stERP->MOPA, "Cash");
        else if ( strcmp(stDefault->PAID, "K") )
            strcpy(stERP->MOPA, "Cash");
        else
            strcpy(stERP->MOPA, " ");

        strcpy(strTmp, stDefault->CDAT); 
        AddSecondsToCEDATime(strTmp, igTimeZone*60*60, 1);                
        convertDate(strTmp, "YYYYMMDDhhmmss", stERP->CDAT, "DD/MM/YYYY hh:mm"); 
        strcpy(stERP->PSTA, stDefault->PSTA);
        
        if ( stFpe.iFound )
            strcpy(stERP->OPER,stFpe.OPNA);  
           
        strcpy(stERP->HOPO, cgHopo); 
        
        if ( strcmp(stDefault->MTOW, "") ) 
            strcpy(stERP->MTOW, stDefault->MTOW); 
        else if ( stFpe.iFound )
            strcpy(stERP->MTOW,stFpe.MTOW);          
             
        if ( stFpe.iFound )
        {
            strcpy(strTmp, stFpe.CDAT); 
            AddSecondsToCEDATime(strTmp, igTimeZone*60*60, 1);                
            convertDate(strTmp, "YYYYMMDDhhmmss", stERP->TXND, "DD/MM/YYYY"); 

            if ( strcmp("Permit Reqd", stFpe.PERN) )
                strcpy(stERP->PERM,stFpe.PERN);    
        }   
        else
        {
            strcpy(strTmp, stDefault->CDAT); 
            AddSecondsToCEDATime(strTmp, igTimeZone*60*60, 1);                
            convertDate(strTmp, "YYYYMMDDhhmmss", stERP->TXND, "DD/MM/YYYY");         	
        }

        strcpy(stERP->ALFN, stDefault->BLTO);                   	
        if ( iFlightFlag==DEPARTUREFLIGHT )
        {
            convertDate(stAftDep->ETDI, "YYYYMMDD", stERP->DDAT, "DD-MON-YYYY");    
            if ( strcmp(stAftDep->ETDI, "" ) )
                sprintf(stERP->ETDI, "%d", convertTimeToSec(&stAftDep->ETDI[8]));        
            
            if ( strcmp(stAftDep->AIRB, "") ) 
                sprintf(stERP->AIRB, "%d", convertTimeToSec(&stAftDep->AIRB[8]));  
        }
        else if ( iFlightFlag==ARRIVALFLIGHT )
        {
            convertDate(stAftArr->LAND, "YYYYMMDD", stERP->ADAT, "DD-MON-YYYY");   
            if ( strcmp(stAftArr->LAND, "") ) 
                sprintf(stERP->LAND, "%d", convertTimeToSec(&stAftArr->LAND[8]));   
                 
            if ( strcmp(stAftArr->ETAI, "") )     
        	    sprintf(stERP->ETAI, "%d", convertTimeToSec(&stAftArr->ETAI[8]));  
        }
        else
        {
            convertDate(stAftArr->LAND, "YYYYMMDD", stERP->ADAT, "DD-MON-YYYY");    
            if ( strcmp(stAftArr->LAND, "") )
                sprintf(stERP->LAND, "%d", convertTimeToSec(&stAftArr->LAND[8])); 

            convertDate(stAftDep->ETDI, "YYYYMMDD", stERP->DDAT, "DD-MON-YYYY");  
            if ( strcmp(stAftDep->ETDI, "") )  
                sprintf(stERP->ETDI, "%d", convertTimeToSec(&stAftDep->ETDI[8]));  
                  
            if ( strcmp(stAftDep->AIRB, "") )
                sprintf(stERP->AIRB, "%d", convertTimeToSec(&stAftDep->AIRB[8]));   
                
            if ( strcmp(stAftArr->ETAI, "") )    
                sprintf(stERP->ETAI, "%d", convertTimeToSec(&stAftArr->ETAI[8]));       	
        }


    }
    else
        dbg(DEBUG, "populateERPADH:: getERPADH failed");
        
    dbg(DEBUG, "populateERPADH:: Arrival Urno[%s], Departure Urno[%s], iFlightFlag[%d], Result[%d]", 
        stAftArr->URNO, stAftDep->URNO, iFlightFlag, iRet);
    return iRet;    
}

int getERPADH(char *strFlnu, ERPADHREC *stERP)
{
    int iRet=RC_SUCCESS;
    short slCursor = 0;
    char strSql[2048];
    char pclDataBuf[32728];
        
    dbg(DEBUG, "getERPADH:: strFlnu[%s]", strFlnu);

    memset(stERP, 0, sizeof(ERPADHREC));
    
    sprintf(strSql, "SELECT %s FROM erpadh WHERE flnu='%s' ", pcgErpAdhFields, strFlnu);

    memset(pclDataBuf, 0, sizeof(pclDataBuf));
    iRet=sql_if (START, &slCursor, strSql, pclDataBuf);  
    dbg(DEBUG, "getERPADH:: executing [%s][%d]", strSql, iRet);    

    if ( iRet==DB_SUCCESS )
    {
        stERP->iFound=1;
        BuildItemBuffer (pclDataBuf, pcgErpAdhFields, igErpAdhFields, ",");
        get_real_item(stERP->URNO,pclDataBuf,1);         
        get_real_item(stERP->FLNU,pclDataBuf,2);         
        get_real_item(stERP->STAT,pclDataBuf,3);    
    }
    else if ( iRet==1 )
    {
        dbg(DEBUG, "getERPADH:: Record not found");
        iRet=RC_SUCCESS;
        stERP->iFound=0;
    }
    else if ( iRet==-1 )
    {
        dbg(DEBUG, "getERPADH:: ERROR!!! DB Failed");
        stERP->iFound=0;
    }
    close_my_cursor(&slCursor);
        
    dbg(DEBUG, "getERPADH:: strFlnu[%s] Result[%d]", strFlnu, iRet);
    return iRet;
}

int populateERPPXH(ERPPXHREC *st, AFTREC *stDefault, char *pcpBillUrno)
{
    int ilRC=RC_SUCCESS;
    int ilFlightFlag=0;
    int ilUrno=0;
    char pclTmp[1024];
    char pclTmp2[1024];
    double dDiff=0;
    int a=0;
    _LOATAB *rlLoatabDefault;
    int ilLoatabCount=0;
    int ilAdult=0;
    
    
    dbg(TRACE, "populateERPPXH:: Start");
          
    if ( !strcmp(stDefault->ADID, "D") )
    {
        ilFlightFlag=1;
        rlLoatabDefault=rgLoatabDep;
        ilLoatabCount=igLoatabDepCount;
    }
    else
    {
        ilFlightFlag=2;
        rlLoatabDefault=rgLoatabArr;
        ilLoatabCount=igLoatabArrCount;
    }
    
    dbg(DEBUG, "populateERPPXH:: Default Urno[%s], ilFlightFlag[%d]", 
        stDefault->URNO, ilFlightFlag);

    ilRC=getERPPXH((char *)stDefault->URNO, st);
    if ( ilRC==RC_SUCCESS )
    {
        if ( st->iFound )
        {
        	
        }
        else
        {
            ilUrno=NewUrnos("ERPPXH",1);
            sprintf(st->URNO, "%d", ilUrno);
            strcpy(st->FLNU, stDefault->URNO);        	
        }
        
        st->iFlightFlag=ilFlightFlag;
        
        removeSpace(stDefault->FLNO, st->FLNO);
        if ( !strcmp(st->FLNO, "") )
            strcpy(st->FLNO, stDefault->CSGN);
            
        strcpy(st->HOPO, cgHopo);

        getSysDate(pclTmp);
        sprintf(pclTmp, "%8.8s", pclTmp);             
        sprintf(pclTmp2, "%8.8s", stDefault->CDAT);                        
        timeDiff(pclTmp, pclTmp2, &dDiff);
        if ( dDiff<=(60*60*24*4) )
            strcpy(st->OPER, "UNSCHEDULED");
        else
            strcpy(st->OPER, "SCHEDULED");  
        

        strcpy(st->REGN, stDefault->REGN); 

        strcpy(st->ACT3, stDefault->ACT5); 
        formatVIAL(atoi(stDefault->VIAN), stDefault->VIAL, st->VIAL, ".");
        strcpy(st->ALC2, stDefault->ALC2); 
        strcpy(st->MMVN, pcpBillUrno); 
                                               
        if ( ilFlightFlag==DEPARTUREFLIGHT )
        {
        	formatVIAL(atoi(stDefault->VIAN), stDefault->VIAL, pclTmp, "/");
        	if ( !strcmp(pclTmp, "") )
        	    strcpy(st->DES3, stDefault->DES3); 
        	else
        	    sprintf(st->DES3, "%s/%s", pclTmp, stDefault->DES3);
        	            
        	strcpy(pclTmp, stDefault->STOD);
        	AddSecondsToCEDATime(pclTmp, igTimeZone*60*60, 1); 
            convertDate(pclTmp, "YYYYMMDD", st->FLDA, "DD/MM/YYYY"); 
            convertDate(pclTmp, "YYYYMMDDhhmmss", st->TDEP, "hhmm"); 
             
            strcpy(st->MOVE, "DEPARTURE");
            
        	strcpy(pclTmp, stDefault->AIRB);
        	AddSecondsToCEDATime(pclTmp, igTimeZone*60* 60, 1); 
            convertDate(pclTmp, "YYYYMMDDhhmmss", st->ADEP, "DD/MM/YYYY hh:mm");
            
            getCKIT(st->TERM, stDefault->URNO);
            if ( !strcmp(st->TERM, "") || !strcmp(st->TERM, " ") )
                strcpy(st->TERM, stDefault->STEV);
            
            for (a=0; a<rgERPPXHConfig.mapDep.ilCount; a++)
	        {
	        	rgERPPXHConfig.mapDep.rec[a].ilFound=getFormulaValue(rgERPPXHConfig.mapDep.rec[a].LOAD_FIELD, rlLoatabDefault, ilLoatabCount, rgERPPXHConfig.mapDep.rec[a].VALU);
	        }	
	        
	        ilRC=computeExpression(&rgERPPXHConfig.mapExpDep, rlLoatabDefault, ilLoatabCount);
        }
        else    
        {
            formatVIAL(atoi(stDefault->VIAN), stDefault->VIAL, pclTmp, "/");
        	if ( !strcmp(pclTmp, "") )
        	    strcpy(st->ORG3, stDefault->ORG3); 
        	else
        	    sprintf(st->ORG3, "%s/%s", stDefault->ORG3, pclTmp);

        	
        	strcpy(pclTmp, stDefault->STOA);
        	AddSecondsToCEDATime(pclTmp, igTimeZone*60*60, 1); 
            convertDate(pclTmp, "YYYYMMDD", st->FLDA, "DD/MM/YYYY"); 
            convertDate(pclTmp, "YYYYMMDDhhmmss", st->TARR, "hhmm"); 
            
            strcpy(st->MOVE, "ARRIVAL");
            
        	strcpy(pclTmp, stDefault->LAND);
        	AddSecondsToCEDATime(pclTmp, igTimeZone*60*60, 1); 
            convertDate(pclTmp, "YYYYMMDDhhmmss", st->AARR, "DD/MM/YYYY hh:mm");
            
            if ( !strcmp(stDefault->TGA1, "") || !strcmp(stDefault->TGA1, " ") )
                strcpy(st->TERM, stDefault->STEV);  
            else
                strcpy(st->TERM, stDefault->TGA1);  
            
            for (a=0; a<rgERPPXHConfig.mapArr.ilCount; a++)
	        {
	        	rgERPPXHConfig.mapArr.rec[a].ilFound=getFormulaValue(rgERPPXHConfig.mapArr.rec[a].LOAD_FIELD, rlLoatabDefault, ilLoatabCount, rgERPPXHConfig.mapArr.rec[a].VALU);
	        }   
	        
	        ilRC=computeExpression(&rgERPPXHConfig.mapExpArr, rlLoatabDefault, ilLoatabCount);         
        }
        
        strcpy(st->NOND, "0"); 
        getAircraftConfigLoad(stDefault->URNO, stDefault->ADID, st->CFGL);

    }
    else
        dbg(DEBUG, "populateERPPXH:: getERPPXH failed");
        
    dbg(TRACE, "populateERPPXH:: Default Urno[%s] iFlightFlag[%d], Result[%d]", 
        stDefault->URNO, ilFlightFlag, ilRC);
    return ilRC;    
}

int getERPPXH(char *strFlnu, ERPPXHREC *st)
{
    int iRet=RC_SUCCESS;
    short slCursor = 0;
    char strSql[2048];
    char pclDataBuf[32728];
        
    dbg(DEBUG, "getERPPXH:: strFlnu[%s]", strFlnu);

    memset(st, 0, sizeof(ERPPXHREC));
    
    sprintf(strSql, "SELECT %s FROM erppxh WHERE flnu='%s' ", pcgErpPxhFields, strFlnu);

    memset(pclDataBuf, 0, sizeof(pclDataBuf));
    iRet=sql_if (START, &slCursor, strSql, pclDataBuf);  
    dbg(DEBUG, "getERPPXH:: executing [%s][%d]", strSql, iRet);   
    if ( iRet==DB_SUCCESS )
    {
        st->iFound=1;
        BuildItemBuffer (pclDataBuf, pcgErpPxhFields, igErpPxhFields, ",");
        get_real_item(st->URNO,pclDataBuf,1);         
        get_real_item(st->FLNU,pclDataBuf,2);  
    }
    else if ( iRet==1 )
    {
        dbg(DEBUG, "getERPPXH:: Record not found");
        iRet=RC_SUCCESS;
        st->iFound=0;
    }
    else if ( iRet==-1 )
    {
        dbg(DEBUG, "getERPPXH:: ERROR!!! DB Failed");
        st->iFound=0;
    }
    close_my_cursor(&slCursor);
        
    dbg(DEBUG, "getERPPXH:: strFlnu[%s] Result[%d]", strFlnu, iRet);
    return iRet;
}

int updateERPPXH(const ERPPXHREC *st)
{
    int ilRC=RC_SUCCESS;
    char pclSql[8192];
    char pclErr[513];
    DB_CONFIG_PAIR *rl;
    DB_CONFIG_PAIR *rlExp;
    int ilSize=0;
    int ilSizeExp=0;
    int a=0;
    int ilAdult=0;
    int ilMale=0;
    int ilFemale=0;

    if ( st->iFlightFlag==DEPARTUREFLIGHT )
    {
        rl=rgERPPXHConfig.mapDep.rec;	
        ilSize=rgERPPXHConfig.mapDep.ilCount;
        
        rlExp=rgERPPXHConfig.mapExpDep.rec;	
        ilSizeExp=rgERPPXHConfig.mapExpDep.ilCount;
    }
    else
    {
        rl=rgERPPXHConfig.mapArr.rec;	
        ilSize=rgERPPXHConfig.mapArr.ilCount;

        rlExp=rgERPPXHConfig.mapExpArr.rec;	
        ilSizeExp=rgERPPXHConfig.mapExpArr.ilCount;
    }
    
    sprintf(pclSql, "UPDATE ERPPXH SET");
    
    sprintf(pclSql, "%s"
                      " FLNO='%s', FLDA='%s', HOPO='%s'"            
                     ", MOVE='%s', OPER='%s', ORG3='%s', DES3='%s', REGN='%s'"
                     ", AARR='%s', ADEP='%s', ACT3='%s', VIAL='%s'"
                     ", CFGL='%s', ALC2='%s', TERM='%s', NOND='%s', TARR='%s'"
                     ", TDEP='%s', MMVN='%s' ",
            pclSql, 
            st->FLNO, st->FLDA, st->HOPO,
            st->MOVE, st->OPER, st->ORG3, st->DES3, st->REGN,
            st->AARR, st->ADEP, st->ACT3, st->VIAL,
            st->CFGL, st->ALC2, st->TERM, st->NOND, st->TARR,
            st->TDEP, st->MMVN);
    
    for (a=0; a<ilSize;a++)
    {
        if ( !strcmp(rl[a].LOAD_FIELD, "LOAD_PAX_MALE") ) 
        {
            if ( rl[a].ilFound==RC_FAIL )
                ilMale=RC_FAIL;
            else
                ilMale=atoi(rl[a].VALU);
        }
        else if ( !strcmp(rl[a].LOAD_FIELD, "LOAD_PAX_FEMALE") ) 
        {
        	if ( rl[a].ilFound==RC_FAIL )
                ilFemale=RC_FAIL;
            else
                ilFemale=atoi(rl[a].VALU); 
        }
        else if ( !strcmp(rl[a].LOAD_FIELD, "LOAD_PAX_ADULT") ) 
        {
        	if ( rl[a].ilFound==RC_FAIL )
                ilAdult=RC_FAIL;
            else
                ilAdult=atoi(rl[a].VALU); 
        }
        else
            sprintf(pclSql, "%s, %s='%s'", pclSql, rl[a].DB_FIELD, rl[a].VALU);	
    }

    if ( ilMale==RC_FAIL && ilFemale==RC_FAIL )
    {
    	if ( ilAdult==RC_FAIL )
    	    ilAdult=0;
        
        sprintf(pclSql, "%s, %s='%d'", pclSql, "ADUL", ilAdult);	
    }
    else
    {
    	if ( ilMale==RC_FAIL )
    	    ilMale=0;
    	if ( ilFemale==RC_FAIL )
    	    ilFemale=0;
    	
        sprintf(pclSql, "%s, %s='%d'", pclSql, "MALE", ilMale);	
        sprintf(pclSql, "%s, %s='%d'", pclSql, "FEMA", ilFemale);	
    }
    
    for (a=0; a<ilSizeExp;a++)
    {
        sprintf(pclSql, "%s, %s='%s'", pclSql, rlExp[a].DB_FIELD, rlExp[a].VALU);	
    }
    sprintf(pclSql, "%s WHERE URNO='%s' " , pclSql, st->URNO);
    ilRC=executeSql(pclSql,pclErr);               

    return ilRC;    
}

int insertERPPXH(const ERPPXHREC *st)
{
    int ilRC=RC_SUCCESS;
    char pclSql[8192];
    char pclErr[513];
    char pclAddlField[1024];
    char pclAddlValu[4096];
    int a=0;
    DB_CONFIG_PAIR *rl;
    int ilSize=0;
    DB_CONFIG_PAIR *rlExp;
    int ilSizeExp=0;
    int ilAdult=0;
    int ilMale=0;
    int ilFemale=0;

    if ( st->iFlightFlag==DEPARTUREFLIGHT )
    {
        rl=rgERPPXHConfig.mapDep.rec;	
        ilSize=rgERPPXHConfig.mapDep.ilCount;

        rlExp=rgERPPXHConfig.mapExpDep.rec;	
        ilSizeExp=rgERPPXHConfig.mapExpDep.ilCount;
    }
    else
    {
        rl=rgERPPXHConfig.mapArr.rec;	
        ilSize=rgERPPXHConfig.mapArr.ilCount;

        rlExp=rgERPPXHConfig.mapExpArr.rec;	
        ilSizeExp=rgERPPXHConfig.mapExpArr.ilCount;
    }
    
    strcpy(pclAddlField, "");
    strcpy(pclAddlValu, "");

    for (a=0; a<ilSize; a++)
    {
        if ( !strcmp(rl[a].LOAD_FIELD, "LOAD_PAX_MALE") ) 
        {
            if ( rl[a].ilFound==RC_FAIL )
                ilMale=RC_FAIL;
            else
                ilMale=atoi(rl[a].VALU);
        }
        else if ( !strcmp(rl[a].LOAD_FIELD, "LOAD_PAX_FEMALE") ) 
        {
        	if ( rl[a].ilFound==RC_FAIL )
                ilFemale=RC_FAIL;
            else
                ilFemale=atoi(rl[a].VALU); 
        }
        else if ( !strcmp(rl[a].LOAD_FIELD, "LOAD_PAX_ADULT") ) 
        {
        	if ( rl[a].ilFound==RC_FAIL )
                ilAdult=RC_FAIL;
            else
                ilAdult=atoi(rl[a].VALU); 
        }
        else
        {
            sprintf(pclAddlField, "%s,%s", pclAddlField, rl[a].DB_FIELD);	
            sprintf(pclAddlValu, "%s,'%s'", pclAddlValu, rl[a].VALU);
        }
    } 

    if ( ilMale==RC_FAIL && ilFemale==RC_FAIL )
    {
    	if ( ilAdult==RC_FAIL )
    	    ilAdult=0;
    	    
        sprintf(pclAddlField, "%s,%s", pclAddlField, "ADUL");	
        sprintf(pclAddlValu, "%s,'%d'", pclAddlValu, ilAdult);
    }
    else
    {
    	if ( ilMale==RC_FAIL )
    	    ilMale=0;
    	if ( ilFemale==RC_FAIL )
    	    ilFemale=0;
    	
        sprintf(pclAddlField, "%s,%s,%s", pclAddlField, "MALE", "FEMA");	
        sprintf(pclAddlValu, "%s,'%d','%d'", pclAddlValu, ilMale, ilFemale);
    }
    
    for (a=0; a<ilSizeExp; a++)
    {
        sprintf(pclAddlField, "%s,%s", pclAddlField, rlExp[a].DB_FIELD);	
        sprintf(pclAddlValu, "%s,'%s'", pclAddlValu, rlExp[a].VALU);	
    } 
    
    strcpy(pclSql, "INSERT INTO ERPPXH "
                   "( "   
                   "URNO, FLNU, FLNO, FLDA, HOPO"
                 ", MOVE, OPER, ORG3, DES3, REGN"
                 ", AARR, ADEP, ACT3, VIAL"
                 ", CFGL, ALC2, TERM, NOND, TARR"
                 ", TDEP, MMVN");
    sprintf(pclSql, "%s%s", pclSql, pclAddlField);	
               
    sprintf(pclSql, "%s) VALUES (", pclSql); 
    
    sprintf(pclSql, "%s "
                    "'%s', '%s', '%s', '%s', '%s'"
                  ", '%s', '%s', '%s', '%s', '%s'"
                  ", '%s', '%s', '%s', '%s'"
                  ", '%s', '%s', '%s', '%s', '%s'"
                  ", '%s', '%s'", 
            pclSql,     
            st->URNO, st->FLNU, st->FLNO, st->FLDA, st->HOPO,
            st->MOVE, st->OPER, st->ORG3, st->DES3, st->REGN,
            st->AARR, st->ADEP, st->ACT3, st->VIAL,
            st->CFGL, st->ALC2, st->TERM, st->NOND, st->TARR,
            st->TDEP, st->MMVN);
    
    sprintf(pclSql, "%s%s", pclSql, pclAddlValu);	
    sprintf(pclSql, "%s)", pclSql); 
 
    ilRC=executeSql(pclSql,pclErr);        
    
    return ilRC;    
}

void printERPPXH(ERPPXHREC *st)
{
	int a=0;
    dbg(DEBUG, "printERPPXH:: Start");
   
    dbg(DEBUG, "printERPPXH:: iFound[%d]", st->iFound);
    
    dbg(DEBUG, "printERPPXH:: URNO[%s]", st->URNO);
    dbg(DEBUG, "printERPPXH:: FLNU[%s]", st->FLNU);
    dbg(DEBUG, "printERPPXH:: FLNO[%s]", st->FLNO);
    dbg(DEBUG, "printERPPXH:: FLDA[%s]", st->FLDA);
    dbg(DEBUG, "printERPPXH:: HOPO[%s]", st->HOPO);

    dbg(DEBUG, "printERPPXH:: MOVE[%s]", st->MOVE);
    dbg(DEBUG, "printERPPXH:: OPER[%s]", st->OPER);
    dbg(DEBUG, "printERPPXH:: ORG3[%s]", st->ORG3);
    dbg(DEBUG, "printERPPXH:: DES3[%s]", st->DES3);
    dbg(DEBUG, "printERPPXH:: REGN[%s]", st->REGN);
    
    dbg(DEBUG, "printERPPXH:: AARR[%s]", st->AARR);
    dbg(DEBUG, "printERPPXH:: ADEP[%s]", st->ADEP);
    //dbg(DEBUG, "printERPPXH:: MALE[%s]", st->MALE);
    //dbg(DEBUG, "printERPPXH:: FEMA[%s]", st->FEMA);
    //dbg(DEBUG, "printERPPXH:: ADUL[%s]", st->ADUL);
    
    //dbg(DEBUG, "printERPPXH:: CHLD[%s]", st->CHLD);
    //dbg(DEBUG, "printERPPXH:: INFT[%s]", st->INFT);
    //dbg(DEBUG, "printERPPXH:: FIRS[%s]", st->FIRS);
    //dbg(DEBUG, "printERPPXH:: BUSN[%s]", st->BUSN);
    dbg(DEBUG, "printERPPXH:: ECPX[%s]", st->ECPX);
    
    //dbg(DEBUG, "printERPPXH:: CREW[%s]", st->CREW);
    //dbg(DEBUG, "printERPPXH:: JUMP[%s]", st->JUMP);
    //dbg(DEBUG, "printERPPXH:: BAGW[%s]", st->BAGW);
    //dbg(DEBUG, "printERPPXH:: CGOW[%s]", st->CGOW);
    //dbg(DEBUG, "printERPPXH:: MALW[%s]", st->MALW);
    
    dbg(DEBUG, "printERPPXH:: JOIN[%s]", st->JOIN);
    dbg(DEBUG, "printERPPXH:: DCPX[%s]", st->DCPX);
    dbg(DEBUG, "printERPPXH:: TCPX[%s]", st->TCPX);
    dbg(DEBUG, "printERPPXH:: ACT3[%s]", st->ACT3);
    dbg(DEBUG, "printERPPXH:: VIAL[%s]", st->VIAL);
    
    dbg(DEBUG, "printERPPXH:: CFGL[%s]", st->CFGL);
    dbg(DEBUG, "printERPPXH:: ALC2[%s]", st->ALC2);
    dbg(DEBUG, "printERPPXH:: TERM[%s]", st->TERM);
    dbg(DEBUG, "printERPPXH:: NOND[%s]", st->NOND);
    dbg(DEBUG, "printERPPXH:: TARR[%s]", st->TARR);
    
    dbg(DEBUG, "printERPPXH:: TDEP[%s]", st->TDEP);
    dbg(DEBUG, "printERPPXH:: MMVN[%s]", st->MMVN);
    
    if ( st->iFlightFlag==DEPARTUREFLIGHT )
    {
        for (a=0; a<rgERPPXHConfig.mapDep.ilCount; a++)
        {
            dbg(DEBUG, "printERPPXH:: [%s][%s][%s]", 
                rgERPPXHConfig.mapDep.rec[a].DB_FIELD, rgERPPXHConfig.mapDep.rec[a].LOAD_FIELD, rgERPPXHConfig.mapDep.rec[a].VALU);   	
        }  
            	
        for (a=0; a<rgERPPXHConfig.mapExpDep.ilCount; a++)
        {
            dbg(DEBUG, "printERPPXH:: [%s][%s][%s]", 
                rgERPPXHConfig.mapExpDep.rec[a].DB_FIELD, rgERPPXHConfig.mapExpDep.rec[a].LOAD_FIELD, rgERPPXHConfig.mapExpDep.rec[a].VALU);   	
        }      	
    }
    else 
    {
        for (a=0; a<rgERPPXHConfig.mapArr.ilCount; a++)
        {
            dbg(DEBUG, "printERPPXH:: [%s][%s][%s]", 
                rgERPPXHConfig.mapArr.rec[a].DB_FIELD, rgERPPXHConfig.mapArr.rec[a].LOAD_FIELD, rgERPPXHConfig.mapArr.rec[a].VALU);   	
        }      	

        for (a=0; a<rgERPPXHConfig.mapExpArr.ilCount; a++)
        {
            dbg(DEBUG, "printERPPXH:: [%s][%s][%s]", 
                rgERPPXHConfig.mapExpArr.rec[a].DB_FIELD, rgERPPXHConfig.mapExpArr.rec[a].LOAD_FIELD, rgERPPXHConfig.mapExpArr.rec[a].VALU);   	
        }      	
    }

    dbg(DEBUG, "printERPPXH:: End");

    return;        
}

int populateERPPXT(ERPPXTREC *st, AFTREC *stDefault, char *pcpERPPXHUrno)
{
    int ilRC=RC_SUCCESS;
    int ilFlightFlag=0;
    int iUrno;
    char pclTmp[1024];
    int ilTmp=0;
    _LOATAB *rlLoatabDefault;
    int ilLoatabCount=0;
    int a=0;
    
      
    if ( !strcmp(stDefault->ADID, "D") )
    {
        ilFlightFlag=1;
        rlLoatabDefault=rgLoatabDep;
        ilLoatabCount=igLoatabDepCount;
    }
    else
    {
        ilFlightFlag=2;
        rlLoatabDefault=rgLoatabArr;
        ilLoatabCount=igLoatabArrCount;
    }
    
    dbg(TRACE, "populateERPPXT:: FLNU[%s], pcpERPPXHUrno[%s], ilFlightFlag[%d]", 
        stDefault->URNO, pcpERPPXHUrno, ilFlightFlag);

    ilRC=getERPPXT(stDefault->URNO, pcpERPPXHUrno, st);
    
    if ( ilRC==RC_SUCCESS )
    {
        if ( st->iFound )
        {
        	
        }
        else
        {
            iUrno=NewUrnos("ERPPXT",1);
            sprintf(st->URNO, "%d", iUrno);
            strcpy(st->FLNU, stDefault->URNO);        	
            strcpy(st->PAXH, pcpERPPXHUrno);
        }
        
        st->iFlightFlag=ilFlightFlag;
        
        removeSpace(stDefault->FLNO, st->FLNO); 
        if ( !strcmp(st->FLNO, "") )
            strcpy(st->FLNO, stDefault->CSGN);
            
        if ( ilFlightFlag==DEPARTUREFLIGHT )
        {
        	strcpy(pclTmp, stDefault->STOD);
        	AddSecondsToCEDATime(pclTmp, igTimeZone*60*60, 1); 
            convertDate(pclTmp, "YYYYMMDD", st->FLDA, "DD/MM/YYYY"); 
            
            strcpy(st->DES3, stDefault->DES3);
            
   	        for (a=0; a<rgERPPXTConfig.mapDep.ilCount; a++)
	        {
	        	ilTmp=getFormulaValue(rgERPPXTConfig.mapDep.rec[a].LOAD_FIELD, rlLoatabDefault, ilLoatabCount, rgERPPXTConfig.mapDep.rec[a].VALU);
	        } 
	        
	        ilTmp=computeExpression(&rgERPPXTConfig.mapExpDep, rlLoatabDefault, ilLoatabCount);

        }
        else
        {
        	strcpy(pclTmp, stDefault->STOA);
        	AddSecondsToCEDATime(pclTmp, igTimeZone*60*60, 1); 
            convertDate(pclTmp, "YYYYMMDD", st->FLDA, "DD/MM/YYYY");  
            
            strcpy(st->DES3, stDefault->ORG3);     	

            for (a=0; a<rgERPPXTConfig.mapArr.ilCount; a++)
	        {
	        	ilTmp=getFormulaValue(rgERPPXTConfig.mapArr.rec[a].LOAD_FIELD, rlLoatabDefault, ilLoatabCount, rgERPPXTConfig.mapArr.rec[a].VALU);
	        } 
	        
	        ilTmp=computeExpression(&rgERPPXTConfig.mapExpArr, rlLoatabDefault, ilLoatabCount);
        }
    }
    else
        dbg(DEBUG, "populateERPPXT:: getERPPXT failed");
        
    dbg(TRACE, "populateERPPXT:: End Result[%d], FLNU[%s], pcpERPPXHUrno[%s], ilFlightFlag[%d], FOUND[%d]", 
        ilRC, stDefault->URNO, pcpERPPXHUrno, ilFlightFlag, st->iFound);
    return ilRC;    
}

int getERPPXT(char *pcpFlnu, char *pcpERPPXHUrno, ERPPXTREC *st)
{
    int iRet=RC_SUCCESS;
    short slCursor = 0;
    char strSql[2048];
    char pclDataBuf[32728];
        
    dbg(DEBUG, "getERPPXT:: pcpFlnu[%s], pcpERPPXHUrno[%s]", pcpFlnu, pcpERPPXHUrno);

    memset(st, 0, sizeof(ERPPXTREC));
    
    sprintf(strSql, "SELECT %s FROM erppxt WHERE FLNU='%s' and PAXH='%s' ", pcgErpPxtFields, pcpFlnu, pcpERPPXHUrno);
    memset(pclDataBuf, 0, sizeof(pclDataBuf));
    iRet=sql_if (START, &slCursor, strSql, pclDataBuf);  
    dbg(DEBUG, "getERPPXT:: executing [%s][%d]", strSql, iRet);    

    if ( iRet==DB_SUCCESS )
    {
        st->iFound=1;
        BuildItemBuffer (pclDataBuf, pcgErpPxtFields, igErpPxtFields, ",");
        get_real_item(st->URNO,pclDataBuf,1);     
        get_real_item(st->FLNU,pclDataBuf,2);     
        get_real_item(st->PAXH,pclDataBuf,3);     
    }
    else if ( iRet==1 )
    {
        dbg(DEBUG, "getERPPXT:: Record not found");
        iRet=RC_SUCCESS;
        st->iFound=0;
    }
    else if ( iRet==-1 )
    {
        dbg(DEBUG, "getERPPXT:: ERROR!!! DB Failed");
        st->iFound=0;
    }
    close_my_cursor(&slCursor);
        
    dbg(DEBUG, "getERPPXT:: pcpFlnu[%s], pcpERPPXHUrno[%s] Result[%d]", pcpFlnu, pcpERPPXHUrno, iRet);
    return iRet;
}

int insertERPPXT(const ERPPXTREC *st)
{
    int ilRC=RC_SUCCESS;
    char pclSql[8192];
    char pclErr[513];
    char pclAddlField[1024];
    char pclAddlValu[8192];
    DB_CONFIG_PAIR *rl;
    int ilSize=0;
    DB_CONFIG_PAIR *rlExp;
    int ilSizeExp=0;
    int a=0;

    if ( st->iFlightFlag==DEPARTUREFLIGHT )
    {
        rl=rgERPPXTConfig.mapDep.rec;	
        ilSize=rgERPPXTConfig.mapDep.ilCount;

        rlExp=rgERPPXTConfig.mapExpDep.rec;	
        ilSizeExp=rgERPPXTConfig.mapExpDep.ilCount;
    }
    else
    {
        rl=rgERPPXTConfig.mapArr.rec;	
        ilSize=rgERPPXTConfig.mapArr.ilCount;

        rlExp=rgERPPXTConfig.mapExpArr.rec;	
        ilSizeExp=rgERPPXTConfig.mapExpArr.ilCount;
    }
    
    strcpy(pclAddlField, "");
    strcpy(pclAddlValu, "");

    for (a=0; a<ilSize; a++)
    {
        sprintf(pclAddlField, "%s,%s", pclAddlField, rl[a].DB_FIELD);	
        sprintf(pclAddlValu, "%s,'%s'", pclAddlValu, rl[a].VALU);	
    }

    for (a=0; a<ilSizeExp; a++)
    {
        sprintf(pclAddlField, "%s,%s", pclAddlField, rlExp[a].DB_FIELD);	
        sprintf(pclAddlValu, "%s,'%s'", pclAddlValu, rlExp[a].VALU);	
    }


   
    strcpy(pclSql, "INSERT INTO ERPPXT "
                   "( "   
                   "URNO, FLNU, PAXH, FLNO, FLDA"
                 ", DES3");   
    sprintf(pclSql, "%s%s", pclSql, pclAddlField);              
    sprintf(pclSql, "%s) VALUES (", pclSql); 
    
    sprintf(pclSql, "%s "
                    "'%s', '%s', '%s', '%s', '%s'"
                  ", '%s' ", 
            pclSql,     
            st->URNO, st->FLNU, st->PAXH, st->FLNO, st->FLDA,
            st->DES3);            
    sprintf(pclSql, "%s%s", pclSql, pclAddlValu);
    sprintf(pclSql, "%s)", pclSql); 

    ilRC=executeSql(pclSql,pclErr);      
    
    return ilRC;    
}

int updateERPPXT(const ERPPXTREC *st)
{
    int ilRC=RC_SUCCESS;
    char pclSql[8192];
    char pclErr[513];
    DB_CONFIG_PAIR *rl;
    int ilSize=0;
    DB_CONFIG_PAIR *rlExp;
    int ilSizeExp=0;
    int a=0;

    if ( st->iFlightFlag==DEPARTUREFLIGHT )
    {
        rl=rgERPPXTConfig.mapDep.rec;	
        ilSize=rgERPPXTConfig.mapDep.ilCount;

        rlExp=rgERPPXTConfig.mapExpDep.rec;	
        ilSizeExp=rgERPPXTConfig.mapExpDep.ilCount;
    }
    else
    {
        rl=rgERPPXTConfig.mapArr.rec;	
        ilSize=rgERPPXTConfig.mapArr.ilCount;

        rlExp=rgERPPXTConfig.mapExpArr.rec;	
        ilSizeExp=rgERPPXTConfig.mapExpArr.ilCount;
    }
    
    sprintf(pclSql, "UPDATE ERPPXT SET");
    
    sprintf(pclSql, "%s"
                      " FLNO='%s', FLDA='%s'"            
                     ", DES3='%s' ",
            pclSql, 
            st->FLNO, st->FLDA,
            st->DES3);


    for (a=0; a<ilSize;a++)
    {
        sprintf(pclSql, "%s, %s='%s'", pclSql, rl[a].DB_FIELD, rl[a].VALU);	
    }

    for (a=0; a<ilSizeExp;a++)
    {
        sprintf(pclSql, "%s, %s='%s'", pclSql, rlExp[a].DB_FIELD, rlExp[a].VALU);	
    }
     
    sprintf(pclSql, "%s WHERE URNO='%s' " , pclSql, st->URNO);
    ilRC=executeSql(pclSql,pclErr);               

    return ilRC;    
}

void printERPPXT(ERPPXTREC *st)
{
	int a=0;
	
    dbg(DEBUG, "printERPPXT:: Start");
   
    dbg(DEBUG, "printERPPXT:: iFound[%d]", st->iFound);
    
    dbg(DEBUG, "printERPPXT:: URNO[%s]", st->URNO);
    dbg(DEBUG, "printERPPXT:: FLNU[%s]", st->FLNU);
    dbg(DEBUG, "printERPPXT:: PAXH[%s]", st->PAXH);
    dbg(DEBUG, "printERPPXT:: FLNO[%s]", st->FLNO);
    dbg(DEBUG, "printERPPXT:: FLDA[%s]", st->FLDA);

    dbg(DEBUG, "printERPPXT:: DES3[%s]", st->DES3);

    if ( st->iFlightFlag==DEPARTUREFLIGHT )
    {
        for (a=0; a<rgERPPXTConfig.mapDep.ilCount; a++)
        {
            dbg(DEBUG, "printERPPXT:: [%s][%s][%s]", 
                rgERPPXTConfig.mapDep.rec[a].DB_FIELD, rgERPPXTConfig.mapDep.rec[a].LOAD_FIELD, rgERPPXTConfig.mapDep.rec[a].VALU);   	
        }      	

        for (a=0; a<rgERPPXTConfig.mapExpDep.ilCount; a++)
        {
            dbg(DEBUG, "printERPPXT:: [%s][%s][%s]", 
                rgERPPXTConfig.mapExpDep.rec[a].DB_FIELD, rgERPPXTConfig.mapExpDep.rec[a].LOAD_FIELD, rgERPPXTConfig.mapExpDep.rec[a].VALU);   	
        }      	
    }
    else 
    {
        for (a=0; a<rgERPPXTConfig.mapArr.ilCount; a++)
        {
            dbg(DEBUG, "printERPPXT:: [%s][%s][%s]", 
                rgERPPXTConfig.mapArr.rec[a].DB_FIELD, rgERPPXTConfig.mapArr.rec[a].LOAD_FIELD, rgERPPXTConfig.mapArr.rec[a].VALU);   	
        }      
        	
        for (a=0; a<rgERPPXTConfig.mapExpArr.ilCount; a++)
        {
            dbg(DEBUG, "printERPPXT:: [%s][%s][%s]", 
                rgERPPXTConfig.mapExpArr.rec[a].DB_FIELD, rgERPPXTConfig.mapExpArr.rec[a].LOAD_FIELD, rgERPPXTConfig.mapExpArr.rec[a].VALU);   	
        }      	
    }

    dbg(DEBUG, "printERPPXT:: End");
    return;        
}

int populateERPPXR(ERPPXRREC *st, AFTREC *stDefault, char *pcpERPPXHUrno)
{
    int ilRC=RC_SUCCESS;
    int ilFlightFlag=0;
    int iUrno;
    char pclTmp[1024];
    int ilTmp=0;
    _LOATAB *rlLoatabDefault;
    int ilLoatabCount=0;
    int a=0;
    
    
    dbg(TRACE, "populateERPPXR:: Start");
          
    if ( !strcmp(stDefault->ADID, "D") )
    {
        ilFlightFlag=1;
        rlLoatabDefault=rgLoatabDep;
        ilLoatabCount=igLoatabDepCount;
    }
    else
    {
        ilFlightFlag=2;
        rlLoatabDefault=rgLoatabArr;
        ilLoatabCount=igLoatabArrCount;
    }
    dbg(DEBUG, "populateERPPXR:: Default Urno[%s], pcpERPPXHUrno[%s], ilFlightFlag[%d]", 
        stDefault->URNO, pcpERPPXHUrno, ilFlightFlag);

    ilRC=getERPPXR(stDefault->URNO, pcpERPPXHUrno, st);
    if ( ilRC==RC_SUCCESS )
    {
        if ( st->iFound )
        {
        	
        }
        else
        {
            iUrno=NewUrnos("ERPPXR",1);
            sprintf(st->URNO, "%d", iUrno);
            strcpy(st->FLNU, stDefault->URNO);        	
            strcpy(st->PAXH, pcpERPPXHUrno);
        }

        st->iFlightFlag=ilFlightFlag;
        
        removeSpace(stDefault->FLNO, st->FLNO); 
        if ( !strcmp(st->FLNO, "") )
            strcpy(st->FLNO, stDefault->CSGN);
            
        if ( ilFlightFlag==DEPARTUREFLIGHT )
        {
        	strcpy(pclTmp, stDefault->STOD);
        	AddSecondsToCEDATime(pclTmp, igTimeZone*60*60, 1); 
            convertDate(pclTmp, "YYYYMMDD", st->FLDA, "DD/MM/YYYY"); 
              
            strcpy(st->DES3, stDefault->DES3);

	        for (a=0; a<rgERPPXRConfig.mapDep.ilCount; a++)
	        {
	        	ilTmp=getFormulaValue(rgERPPXRConfig.mapDep.rec[a].LOAD_FIELD, rlLoatabDefault, ilLoatabCount, rgERPPXRConfig.mapDep.rec[a].VALU);
	        } 
	        
	        ilTmp=computeExpression(&rgERPPXRConfig.mapExpDep, rlLoatabDefault, ilLoatabCount);
        }
        else
        {
        	strcpy(pclTmp, stDefault->STOA);
        	AddSecondsToCEDATime(pclTmp, igTimeZone*60*60, 1); 
            convertDate(pclTmp, "YYYYMMDD", st->FLDA, "DD/MM/YYYY"); 
            
            strcpy(st->DES3, stDefault->ORG3);
            
            for (a=0; a<rgERPPXRConfig.mapArr.ilCount; a++)
	        {
	        	ilTmp=getFormulaValue(rgERPPXRConfig.mapArr.rec[a].LOAD_FIELD, rlLoatabDefault, ilLoatabCount, rgERPPXRConfig.mapArr.rec[a].VALU);
	        } 
	        
	        ilTmp=computeExpression(&rgERPPXRConfig.mapExpArr, rlLoatabDefault, ilLoatabCount);
        } 
 
    }
    else
        dbg(DEBUG, "populateERPPXR:: getERPPXR failed");
        
    dbg(DEBUG, "populateERPPXR:: Result[%d] Default Urno[%s], pcpERPPXHUrno[%s], ilFlightFlag[%d], Found[%d]", 
        ilRC, stDefault->URNO, pcpERPPXHUrno, ilFlightFlag, st->iFound);
    return ilRC;    
}

int getERPPXR(char *pcpFlnu, char *pcpERPPXHUrno, ERPPXRREC *st)
{
    int iRet=RC_SUCCESS;
    short slCursor = 0;
    char strSql[2048];
    char pclDataBuf[32728];
        
    dbg(DEBUG, "getERPPXR:: Start: pcpFlnu[%s], pcpERPPXHUrno[%s]", pcpFlnu, pcpERPPXHUrno);

    memset(st, 0, sizeof(ERPPXRREC));
    
    sprintf(strSql, "SELECT %s FROM erppxr WHERE FLNU='%s' and PAXH='%s' ", pcgErpPxrFields, pcpFlnu, pcpERPPXHUrno);

    memset(pclDataBuf, 0, sizeof(pclDataBuf));
    iRet=sql_if (START, &slCursor, strSql, pclDataBuf);  
    dbg(DEBUG, "getERPPXR:: executing [%s][%d]", strSql, iRet);  
    
    if ( iRet==DB_SUCCESS )
    {
        st->iFound=1;
        BuildItemBuffer (pclDataBuf, pcgErpPxrFields, igErpPxrFields, ",");
        get_real_item(st->URNO,pclDataBuf,1);         
        get_real_item(st->PAXH,pclDataBuf,2);    
        get_real_item(st->FLNU,pclDataBuf,3);   
    }
    else if ( iRet==1 )
    {
        dbg(DEBUG, "getERPPXR:: Record not found");
        iRet=RC_SUCCESS;
        st->iFound=0;
    }
    else if ( iRet==-1 )
    {
        dbg(DEBUG, "getERPPXR:: ERROR!!! DB Failed");
        st->iFound=0;
    }
    close_my_cursor(&slCursor);
        
    dbg(DEBUG, "getERPPXR:: pcpFlnu[%s], pcpERPPXHUrno[%s] Result[%d]", pcpFlnu, pcpERPPXHUrno, iRet);
    return iRet;
}

int insertERPPXR(const ERPPXRREC *st)
{
    int ilRC=RC_SUCCESS;
    char pclSql[8192];
    char pclErr[513];
    char pclAddlField[1024];
    char pclAddlValu[8192];
    DB_CONFIG_PAIR *rl;
    int ilSize=0;
    DB_CONFIG_PAIR *rlExp;
    int ilSizeExp=0;
    int a=0;

    if ( st->iFlightFlag==DEPARTUREFLIGHT )
    {
        rl=rgERPPXRConfig.mapDep.rec;	
        ilSize=rgERPPXRConfig.mapDep.ilCount;

        rlExp=rgERPPXRConfig.mapExpDep.rec;	
        ilSizeExp=rgERPPXRConfig.mapExpDep.ilCount;
    }
    else
    {
        rl=rgERPPXRConfig.mapArr.rec;	
        ilSize=rgERPPXRConfig.mapArr.ilCount;

        rlExp=rgERPPXRConfig.mapExpArr.rec;	
        ilSizeExp=rgERPPXRConfig.mapExpArr.ilCount;
    }
    
    strcpy(pclAddlField, "");
    strcpy(pclAddlValu, "");

    for (a=0; a<ilSize; a++)
    {
        sprintf(pclAddlField, "%s,%s", pclAddlField, rl[a].DB_FIELD);	
        sprintf(pclAddlValu, "%s,'%s'", pclAddlValu, rl[a].VALU);	
    }

    for (a=0; a<ilSizeExp; a++)
    {
        sprintf(pclAddlField, "%s,%s", pclAddlField, rlExp[a].DB_FIELD);	
        sprintf(pclAddlValu, "%s,'%s'", pclAddlValu, rlExp[a].VALU);	
    }
       
    strcpy(pclSql, "INSERT INTO ERPPXR "
                   "( "   
                   "URNO, PAXH, FLNU, FLNO, FLDA"
                 ", DES3");   
    sprintf(pclSql, "%s%s", pclSql, pclAddlField);
                  
    sprintf(pclSql, "%s) VALUES (", pclSql); 
    
    sprintf(pclSql, "%s "
                    "'%s', '%s', '%s', '%s', '%s'"
                  ", '%s' ", 
            pclSql,     
            st->URNO, st->PAXH, st->FLNU, st->FLNO, st->FLDA,
            st->DES3);    
    sprintf(pclSql, "%s%s", pclSql, pclAddlValu);	        
    sprintf(pclSql, "%s)", pclSql); 

    ilRC=executeSql(pclSql,pclErr);      

    return ilRC;    
}

int updateERPPXR(const ERPPXRREC *st)
{
    int ilRC=RC_SUCCESS;
    char pclSql[8192];
    char pclErr[513];
    DB_CONFIG_PAIR *rl;
    int ilSize=0;
    DB_CONFIG_PAIR *rlExp;
    int ilSizeExp=0;
    int a=0;

    if ( st->iFlightFlag==DEPARTUREFLIGHT )
    {
        rl=rgERPPXRConfig.mapDep.rec;	
        ilSize=rgERPPXRConfig.mapDep.ilCount;

        rlExp=rgERPPXRConfig.mapExpDep.rec;	
        ilSizeExp=rgERPPXRConfig.mapExpDep.ilCount;
    }
    else
    {
        rl=rgERPPXRConfig.mapArr.rec;	
        ilSize=rgERPPXRConfig.mapArr.ilCount;

        rlExp=rgERPPXRConfig.mapExpArr.rec;	
        ilSizeExp=rgERPPXRConfig.mapExpArr.ilCount;
    }

    sprintf(pclSql, "UPDATE ERPPXR SET");
    
    sprintf(pclSql, "%s"
                      " FLNO='%s', FLDA='%s'"            
                     ", DES3='%s' ",
            pclSql, 
            st->FLNO, st->FLDA,
            st->DES3);

    for (a=0; a<ilSize;a++)
    {
        sprintf(pclSql, "%s, %s='%s'", pclSql, rl[a].DB_FIELD, rl[a].VALU);	
    }

    for (a=0; a<ilSizeExp;a++)
    {
        sprintf(pclSql, "%s, %s='%s'", pclSql, rlExp[a].DB_FIELD, rlExp[a].VALU);	
    }
     
    sprintf(pclSql, "%s WHERE URNO='%s' " , pclSql, st->URNO);
    ilRC=executeSql(pclSql,pclErr);               
  
    return ilRC;    
}

void printERPPXR(ERPPXRREC *st)
{
	int a=0;
    dbg(DEBUG, "printERPPXR:: Start");
   
    dbg(DEBUG, "printERPPXR:: iFound[%d]", st->iFound);
    
    dbg(DEBUG, "printERPPXR:: URNO[%s]", st->URNO);
    dbg(DEBUG, "printERPPXR:: PAXH[%s]", st->PAXH);
    dbg(DEBUG, "printERPPXR:: FLNU[%s]", st->FLNU);
    dbg(DEBUG, "printERPPXR:: FLNO[%s]", st->FLNO);
    dbg(DEBUG, "printERPPXR:: FLDA[%s]", st->FLDA);
    
    dbg(DEBUG, "printERPPXR:: ECON[%s]", st->ECON);
    dbg(DEBUG, "printERPPXR:: DES3[%s]", st->DES3);

    if ( st->iFlightFlag==DEPARTUREFLIGHT )
    {
        for (a=0; a<rgERPPXRConfig.mapDep.ilCount; a++)
        {
            dbg(DEBUG, "printERPPXR:: [%s][%s][%s]", 
                rgERPPXRConfig.mapDep.rec[a].DB_FIELD, rgERPPXRConfig.mapDep.rec[a].LOAD_FIELD, rgERPPXRConfig.mapDep.rec[a].VALU);   	
        }      	

        for (a=0; a<rgERPPXRConfig.mapExpDep.ilCount; a++)
        {
            dbg(DEBUG, "printERPPXR:: [%s][%s][%s]", 
                rgERPPXRConfig.mapExpDep.rec[a].DB_FIELD, rgERPPXRConfig.mapExpDep.rec[a].LOAD_FIELD, rgERPPXRConfig.mapExpDep.rec[a].VALU);   	
        }      	
    }
    else 
    {
        for (a=0; a<rgERPPXRConfig.mapArr.ilCount; a++)
        {
            dbg(DEBUG, "printERPPXR:: [%s][%s][%s]", 
                rgERPPXRConfig.mapArr.rec[a].DB_FIELD, rgERPPXRConfig.mapArr.rec[a].LOAD_FIELD, rgERPPXRConfig.mapArr.rec[a].VALU);   	
        }      
        	
        for (a=0; a<rgERPPXRConfig.mapExpArr.ilCount; a++)
        {
            dbg(DEBUG, "printERPPXR:: [%s][%s][%s]", 
                rgERPPXRConfig.mapExpArr.rec[a].DB_FIELD, rgERPPXRConfig.mapExpArr.rec[a].LOAD_FIELD, rgERPPXRConfig.mapExpArr.rec[a].VALU);   	
        }      	
    }
    
    dbg(DEBUG, "printERPPXR:: End");
    return;        
}

int populateERPPXM(ERPPXMREC *st, AFTREC *stDefault, char *pcpERPPXHUrno)
{
    int ilRC=RC_SUCCESS;
    int ilFlightFlag=0;
    int iUrno;
    char pclTmp[1024];
    int ilTmp=0;
    int a=0;
    _LOATAB *rlLoatabDefault;
    int ilLoatabCount=0;
     
    if ( !strcmp(stDefault->ADID, "D") )
    {
        ilFlightFlag=1;
        rlLoatabDefault=rgLoatabDep;
        ilLoatabCount=igLoatabDepCount;
    }
    else
    {
        ilFlightFlag=2;
        rlLoatabDefault=rgLoatabArr;
        ilLoatabCount=igLoatabArrCount;
    }
    
    dbg(DEBUG, "populateERPPXM:: Default Urno[%s], pcpERPPXHUrno[%s] ilFlightFlag[%d]", 
        stDefault->URNO, pcpERPPXHUrno, ilFlightFlag);

    ilRC=getERPPXM(stDefault->URNO, pcpERPPXHUrno, st);
    if ( ilRC==RC_SUCCESS )
    {
        if ( st->iFound )
        {
        	
        }
        else
        {
            iUrno=NewUrnos("ERPPXM",1);
            sprintf(st->URNO, "%d", iUrno);
            strcpy(st->FLNU, stDefault->URNO);  
            strcpy(st->PAXH, pcpERPPXHUrno);     	
        }

        st->iFlightFlag=ilFlightFlag;
        
        if ( ilFlightFlag==DEPARTUREFLIGHT )
        {
            strcpy(st->APT3, stDefault->DES3);        	

            for (a=0; a<rgERPPXMConfig.mapDep.ilCount; a++)
	        {
	        	ilTmp=getFormulaValue(rgERPPXMConfig.mapDep.rec[a].LOAD_FIELD, rlLoatabDefault, ilLoatabCount, rgERPPXMConfig.mapDep.rec[a].VALU);
	        } 
	        
	        ilTmp=computeExpression(&rgERPPXMConfig.mapExpDep, rlLoatabDefault, ilLoatabCount);
        }
        else
        {
            strcpy(st->APT3, stDefault->ORG3);	  
            
            for (a=0; a<rgERPPXMConfig.mapArr.ilCount; a++)
	        {
	        	ilTmp=getFormulaValue(rgERPPXMConfig.mapArr.rec[a].LOAD_FIELD, rlLoatabDefault, ilLoatabCount, rgERPPXMConfig.mapArr.rec[a].VALU);
	        } 
	        
	        ilTmp=computeExpression(&rgERPPXMConfig.mapExpArr, rlLoatabDefault, ilLoatabCount);
        }        
    }
    else
        dbg(DEBUG, "populateERPPXM:: getERPPXM failed");
        
    dbg(DEBUG, "populateERPPXM:: End Result[%d], Default Urno[%s], pcpERPPXHUrno[%s] ilFlightFlag[%d], Found[%d]", 
        ilRC, stDefault->URNO, pcpERPPXHUrno, ilFlightFlag, st->iFound);
    return ilRC;    
}

int getERPPXM(char *pcpFlnu, char *pcpERPPXHUrno, ERPPXMREC *st)
{
    int iRet=RC_SUCCESS;
    short slCursor = 0;
    char strSql[2048];
    char pclDataBuf[32728];
        
    dbg(DEBUG, "getERPPXM:: pcpFlnu[%s], pcpERPPXHUrno[%s]", pcpFlnu, pcpERPPXHUrno);

    memset(st, 0, sizeof(ERPPXMREC));
    
    sprintf(strSql, "SELECT %s FROM ERPPXM WHERE FLNU='%s' AND PAXH='%s' ", pcgErpPxmFields, pcpFlnu, pcpERPPXHUrno);
    memset(pclDataBuf, 0, sizeof(pclDataBuf));
    iRet=sql_if (START, &slCursor, strSql, pclDataBuf);  
    dbg(DEBUG, "getERPPXM:: executing [%s][%d]", strSql, iRet);    

    if ( iRet==DB_SUCCESS )
    {
        st->iFound=1;
        BuildItemBuffer (pclDataBuf, pcgErpPxmFields, igErpPxmFields, ",");
        get_real_item(st->URNO,pclDataBuf,1);         
        get_real_item(st->FLNU,pclDataBuf,2);   
        get_real_item(st->PAXH,pclDataBuf,3);   
    }
    else if ( iRet==1 )
    {
        dbg(DEBUG, "getERPPXM:: Record not found");
        iRet=RC_SUCCESS;
        st->iFound=0;
    }
    else if ( iRet==-1 )
    {
        dbg(DEBUG, "getERPPXM:: ERROR!!! DB Failed");
        st->iFound=0;
    }
    close_my_cursor(&slCursor);
        
    dbg(DEBUG, "getERPPXM:: End Result[%d] pcpFlnu[%s], pcpERPPXHUrno[%s]", iRet, pcpFlnu, pcpERPPXHUrno);
    return iRet;
}

int insertERPPXM(const ERPPXMREC *st)
{
    int ilRC=RC_SUCCESS;
    char pclSql[8192];
    char pclErr[513];
    char pclAddlField[1024];
    char pclAddlValu[8192];
    DB_CONFIG_PAIR *rl;
    int ilSize=0;
    DB_CONFIG_PAIR *rlExp;
    int ilSizeExp=0;
    int a=0;

    if ( st->iFlightFlag==DEPARTUREFLIGHT )
    {
        rl=rgERPPXMConfig.mapDep.rec;	
        ilSize=rgERPPXMConfig.mapDep.ilCount;

        rlExp=rgERPPXMConfig.mapExpDep.rec;	
        ilSizeExp=rgERPPXMConfig.mapExpDep.ilCount;
    }
    else
    {
        rl=rgERPPXMConfig.mapArr.rec;	
        ilSize=rgERPPXMConfig.mapArr.ilCount;

        rlExp=rgERPPXMConfig.mapExpArr.rec;	
        ilSizeExp=rgERPPXMConfig.mapExpArr.ilCount;
    }
    
    strcpy(pclAddlField, "");
    strcpy(pclAddlValu, "");

    for (a=0; a<ilSize; a++)
    {
        sprintf(pclAddlField, "%s,%s", pclAddlField, rl[a].DB_FIELD);	
        sprintf(pclAddlValu, "%s,'%s'", pclAddlValu, rl[a].VALU);	
    }

    for (a=0; a<ilSizeExp; a++)
    {
        sprintf(pclAddlField, "%s,%s", pclAddlField, rlExp[a].DB_FIELD);	
        sprintf(pclAddlValu, "%s,'%s'", pclAddlValu, rlExp[a].VALU);	
    }
       
    strcpy(pclSql, "INSERT INTO ERPPXM "
                   "( "   
                   "URNO, FLNU, PAXH, APT3 ");   
    sprintf(pclSql, "%s%s", pclSql, pclAddlField);
                   
    sprintf(pclSql, "%s) VALUES (", pclSql); 
    
    sprintf(pclSql, "%s "
                    "'%s', '%s', '%s', '%s' ", 
            pclSql,     
            st->URNO, st->FLNU, st->PAXH, st->APT3);  
    sprintf(pclSql, "%s%s", pclSql, pclAddlValu);          
    sprintf(pclSql, "%s)", pclSql); 
   
    ilRC=executeSql(pclSql,pclErr);      
    
    return ilRC;    
}

int updateERPPXM(const ERPPXMREC *st)
{
    int ilRC=RC_SUCCESS;
    char pclSql[8192];
    char pclErr[513];
    DB_CONFIG_PAIR *rl;
    int ilSize=0;
    DB_CONFIG_PAIR *rlExp;
    int ilSizeExp=0;
    int a=0;

    if ( st->iFlightFlag==DEPARTUREFLIGHT )
    {
        rl=rgERPPXMConfig.mapDep.rec;	
        ilSize=rgERPPXMConfig.mapDep.ilCount;

        rlExp=rgERPPXMConfig.mapExpDep.rec;	
        ilSizeExp=rgERPPXMConfig.mapExpDep.ilCount;
    }
    else
    {
        rl=rgERPPXMConfig.mapArr.rec;	
        ilSize=rgERPPXMConfig.mapArr.ilCount;

        rlExp=rgERPPXMConfig.mapExpArr.rec;	
        ilSizeExp=rgERPPXMConfig.mapExpArr.ilCount;
    }
    
    sprintf(pclSql, "UPDATE ERPPXM SET");
    
    sprintf(pclSql, "%s"
                      " APT3='%s' ",
            pclSql, 
            st->APT3);
 
    for (a=0; a<ilSize;a++)
    {
        sprintf(pclSql, "%s, %s='%s'", pclSql, rl[a].DB_FIELD, rl[a].VALU);	
    }

    for (a=0; a<ilSizeExp;a++)
    {
        sprintf(pclSql, "%s, %s='%s'", pclSql, rlExp[a].DB_FIELD, rlExp[a].VALU);	
    }     
                
    sprintf(pclSql, "%s WHERE URNO='%s' " , pclSql, st->URNO);
    ilRC=executeSql(pclSql,pclErr);               

    return ilRC;    
}

void printERPPXM(ERPPXMREC *st)
{
	int a=0;
	
    dbg(DEBUG, "printERPPXM:: Start");
   
    dbg(DEBUG, "printERPPXM:: iFound[%d]", st->iFound);
    
    dbg(DEBUG, "printERPPXM:: URNO[%s]", st->URNO);
    dbg(DEBUG, "printERPPXM:: FLNU[%s]", st->FLNU);
    dbg(DEBUG, "printERPPXM:: PAXH[%s]", st->PAXH);
    dbg(DEBUG, "printERPPXM:: APT3[%s]", st->APT3);

    if ( st->iFlightFlag==DEPARTUREFLIGHT )
    {
        for (a=0; a<rgERPPXMConfig.mapDep.ilCount; a++)
        {
            dbg(DEBUG, "printERPPXM:: [%s][%s][%s]", 
                rgERPPXMConfig.mapDep.rec[a].DB_FIELD, rgERPPXMConfig.mapDep.rec[a].LOAD_FIELD, rgERPPXMConfig.mapDep.rec[a].VALU);   	
        }      	

        for (a=0; a<rgERPPXMConfig.mapExpDep.ilCount; a++)
        {
            dbg(DEBUG, "printERPPXM:: [%s][%s][%s]", 
                rgERPPXMConfig.mapExpDep.rec[a].DB_FIELD, rgERPPXMConfig.mapExpDep.rec[a].LOAD_FIELD, rgERPPXMConfig.mapExpDep.rec[a].VALU);   	
        }      	
    }
    else 
    {
        for (a=0; a<rgERPPXMConfig.mapArr.ilCount; a++)
        {
            dbg(DEBUG, "printERPPXM:: [%s][%s][%s]", 
                rgERPPXMConfig.mapArr.rec[a].DB_FIELD, rgERPPXMConfig.mapArr.rec[a].LOAD_FIELD, rgERPPXMConfig.mapArr.rec[a].VALU);   	
        }      
        	
        for (a=0; a<rgERPPXMConfig.mapExpArr.ilCount; a++)
        {
            dbg(DEBUG, "printERPPXM:: [%s][%s][%s]", 
                rgERPPXMConfig.mapExpArr.rec[a].DB_FIELD, rgERPPXMConfig.mapExpArr.rec[a].LOAD_FIELD, rgERPPXMConfig.mapExpArr.rec[a].VALU);   	
        }      	
    }
    dbg(DEBUG, "printERPPXM:: End");

    return;        
}


int populateERPOFF(ERPOFFREC *st, AFTREC *stArr, char *pcpERPPXHArrUrno, AFTREC *stDep, char *pcpERPPXHDepUrno)
{
    int ilRC=RC_SUCCESS;
    int ilTmp;
    int iUrno=0;
    
    dbg(DEBUG, "populateERPOFF:: Arrival: Found[%d] Urno[%s] pcpERPPXHArrUrno[%s], Departure: Found[%d] Urno[%s] pcpERPPXDepHUrno[%s]",
        stArr->iFound, stArr->URNO, pcpERPPXHArrUrno, stDep->iFound, stDep->URNO, pcpERPPXHDepUrno);

    for (iUrno=0; iUrno<MAX_ERPOFFREC; iUrno++)
        strcpy(st[iUrno].URNO, "");
        
    if ( stDep->iFound )
    {
        ilRC=getERPOFF(pcpERPPXHDepUrno, "ON FROM TXFR", &st[INDEX_ON_FROM_TXFR]);
        if ( ilRC==RC_SUCCESS )
        {
            if ( st[INDEX_ON_FROM_TXFR].iFound )
            {
        	
            }
            else
            {
                iUrno=NewUrnos("ERPOFF",1);
                sprintf(st[INDEX_ON_FROM_TXFR].URNO, "%d", iUrno);
                strcpy(st[INDEX_ON_FROM_TXFR].PAXH, pcpERPPXHDepUrno);
                strcpy(st[INDEX_ON_FROM_TXFR].SERV, "ON FROM TXFR");
            }
    	    getFormulaValue("LOAD_BAG_WT_ACTUAL_TRANSFER_DEP", rgLoatabDep, igLoatabDepCount, st[INDEX_ON_FROM_TXFR].BAGW);
    	    getFormulaValue("LOAD_DEP_OFFLOAD_CARGO_FOR_TRANSFER", rgLoatabDep, igLoatabDepCount, st[INDEX_ON_FROM_TXFR].CRGO);
    	    getFormulaValue("LOAD_DEP_OFFLOAD_MAIL_FOR_TRANSFER", rgLoatabDep, igLoatabDepCount, st[INDEX_ON_FROM_TXFR].MAIL);
        }    
        else
            dbg(DEBUG, "populateERPOFF:: getERPOFF [ON_FROM_TXFR] failed");

        ilRC=getERPOFF(pcpERPPXHDepUrno, "ON FROM AUH", &st[INDEX_ON_FROM_AUH]);
        if ( ilRC==RC_SUCCESS )
        {
            if ( st[INDEX_ON_FROM_AUH].iFound )
            {
        	
            }
            else
            {
                iUrno=NewUrnos("ERPOFF",1);
                sprintf(st[INDEX_ON_FROM_AUH].URNO, "%d", iUrno);
                strcpy(st[INDEX_ON_FROM_AUH].PAXH, pcpERPPXHDepUrno);
                strcpy(st[INDEX_ON_FROM_AUH].SERV, "ON FROM AUH");
            }
     	    getFormulaValue("LOAD_BAG_WT_LOCAL_DEP", rgLoatabDep, igLoatabDepCount, st[INDEX_ON_FROM_AUH].BAGW);
    	    getFormulaValue("ONLOAD_FROM_HOPO_DEP_CGO", rgLoatabDep, igLoatabDepCount, st[INDEX_ON_FROM_AUH].CRGO);
    	    getFormulaValue("ONLOAD_FROM_HOPO_DEP_MAIL", rgLoatabDep, igLoatabDepCount, st[INDEX_ON_FROM_AUH].MAIL);   	
        }    
        else
            dbg(DEBUG, "populateERPOFF:: getERPOFF [INDEX_ON_FROM_AUH] failed");
    }
    
    if ( stArr->iFound )
    {
        ilRC=getERPOFF(pcpERPPXHArrUrno, "OFF AT AUH", &st[INDEX_OFF_AT_AUH]);
        if ( ilRC==RC_SUCCESS )
        {
            if ( st[INDEX_OFF_AT_AUH].iFound )
            {
        	
            }
            else
            {
                iUrno=NewUrnos("ERPOFF",1);
                sprintf(st[INDEX_OFF_AT_AUH].URNO, "%d", iUrno);
                strcpy(st[INDEX_OFF_AT_AUH].PAXH, pcpERPPXHArrUrno);
                strcpy(st[INDEX_OFF_AT_AUH].SERV, "OFF AT AUH");
            }  
          	
            getFormulaValue("LOAD_BAG_WT_LOCAL_ARR", rgLoatabArr, igLoatabArrCount, st[INDEX_OFF_AT_AUH].BAGW);
            getFormulaValue("LOAD_CARGO_WEIGHT_ARR_HOPO", rgLoatabArr, igLoatabArrCount, st[INDEX_OFF_AT_AUH].CRGO);
            getFormulaValue("LOAD_MAIL_WEIGHT_ARR_HOPO", rgLoatabArr, igLoatabArrCount, st[INDEX_OFF_AT_AUH].MAIL);   	
        }    
        else
            dbg(DEBUG, "populateERPOFF:: getERPOFF [INDEX_OFF_AT_AUH] failed");

        ilRC=getERPOFF(pcpERPPXHArrUrno, "OFF FOR TXFR", &st[INDEX_OFF_FOR_TXFR]);
        if ( ilRC==RC_SUCCESS )
        {
            if ( st[INDEX_OFF_FOR_TXFR].iFound )
            {
        	
            }
            else
            {
                iUrno=NewUrnos("ERPOFF",1);
                sprintf(st[INDEX_OFF_FOR_TXFR].URNO, "%d", iUrno);
                strcpy(st[INDEX_OFF_FOR_TXFR].PAXH, pcpERPPXHArrUrno);
                strcpy(st[INDEX_OFF_FOR_TXFR].SERV, "OFF FOR TXFR");
            }
    	    /*
            getFormulaValue("LOAD_BAG_WT_ACTUAL_TRANSFER_ARR", rgLoatabArr, igLoatabArrCount, st[INDEX_OFF_FOR_TXFR].BAGW);
            getFormulaValue("LOAD_ARR_OFFLOAD_CARGO_FOR_TRANSFER", rgLoatabArr, igLoatabArrCount, st[INDEX_OFF_FOR_TXFR].CRGO);
            getFormulaValue("LOAD_ARR_OFFLOAD_MAIL_FOR_TRANSFER", rgLoatabArr, igLoatabArrCount, st[INDEX_OFF_FOR_TXFR].MAIL);   
            */
            strcpy(st[INDEX_OFF_FOR_TXFR].BAGW, "0");
            strcpy(st[INDEX_OFF_FOR_TXFR].CRGO, "0");
            strcpy(st[INDEX_OFF_FOR_TXFR].MAIL, "0");
        }    
        else
            dbg(DEBUG, "populateERPOFF:: getERPOFF [OFF_FOR_TXFR] failed");

        ilRC=getERPOFF(pcpERPPXHArrUrno, "TRANSIT", &st[INDEX_TRANSIT]);
        if ( ilRC==RC_SUCCESS )
        {
            if ( st[INDEX_TRANSIT].iFound )
            {
        	
            }
            else
            {
                iUrno=NewUrnos("ERPOFF",1);
                sprintf(st[INDEX_TRANSIT].URNO, "%d", iUrno);
                strcpy(st[INDEX_TRANSIT].PAXH, pcpERPPXHArrUrno);
                strcpy(st[INDEX_TRANSIT].SERV, "TRANSIT");
            }
     	    getFormulaValue("LOAD_TRANSIT_BAG_WT_ARR", rgLoatabArr, igLoatabArrCount, st[INDEX_TRANSIT].BAGW);
    	    getFormulaValue("LOAD_TRANSIT_CARGO_WT_ARR", rgLoatabArr, igLoatabArrCount, st[INDEX_TRANSIT].CRGO);
    	    getFormulaValue("LOAD_TRANSIT_MAIL_WT_ARR", rgLoatabArr, igLoatabArrCount, st[INDEX_TRANSIT].MAIL);   	
        }    
        else
            dbg(DEBUG, "populateERPOFF:: getERPOFF [TRANSIT] failed");
    }
            
    return ilRC;    
}

int getERPOFF(char *pcpPAXH, char *pcpServ, ERPOFFREC *st)
{
    int iRet=RC_SUCCESS;
    short slCursor = 0;
    char strSql[2048];
    char pclDataBuf[32728];
        
    dbg(DEBUG, "getERPOFF:: pcpPAXH[%s], pcpServ[%s]", pcpPAXH, pcpServ);

    memset(st, 0, sizeof(ERPOFFREC));    
    sprintf(strSql, "SELECT %s FROM erpoff WHERE PAXH='%s' AND SERV='%s' ", pcgErpOffFields, pcpPAXH, pcpServ);
    dbg(DEBUG, "getERPOFF:: executing [%s]", strSql);    

    memset(pclDataBuf, 0, sizeof(pclDataBuf));
    iRet=sql_if (START, &slCursor, strSql, pclDataBuf);  

    if ( iRet==DB_SUCCESS )
    {
        st->iFound=1;
        BuildItemBuffer (pclDataBuf, pcgErpOffFields, igErpOffFields, ",");
        get_real_item(st->URNO,pclDataBuf,1);         
        get_real_item(st->PAXH,pclDataBuf,2);    
        get_real_item(st->SERV,pclDataBuf,3);    
    }
    else if ( iRet==1 )
    {
        dbg(DEBUG, "getERPOFF:: Record not found");
        iRet=RC_SUCCESS;
        st->iFound=0;
    }
    else if ( iRet==-1 )
    {
        dbg(DEBUG, "getERPOFF:: ERROR!!! DB Failed");
        st->iFound=0;
    }
    close_my_cursor(&slCursor);
        
    dbg(DEBUG, "getERPOFF:: End Result[%d] pcpPAXH[%s]", iRet, pcpPAXH);
    return iRet;
}

int insertERPOFF(const ERPOFFREC *st)
{
    int ilRC=RC_SUCCESS;
    int a=0;
    char pclData[32768];
    char pclFields[1024];
    char pclValueGuides[1024];
    char pclErr[513];
    
    memset(&pclData, 0, sizeof(pclData));
    for (a=0; a<MAX_ERPOFFREC;a++)
    {
    	if ( strcmp(st[a].URNO, "") )
    	{
            sprintf(pclData, "%s%s,%s,%s,%s,%s,%s\n", 
                    pclData, st[a].URNO, st[a].PAXH, st[a].SERV, 
                    st[a].BAGW[0]==0?"0":st[a].BAGW, 
                    st[a].CRGO[0]==0?"0":st[a].CRGO,
                    st[a].MAIL[0]==0?"0":st[a].MAIL);        	
        }
    }

    if ( strlen(pclData) )
    {
        sprintf(pclFields, "URNO,PAXH,SERV,BAGW,CRGO,MAIL");
    	strcpy(pclValueGuides, ":VURNO,:VPAXH,:VSERV,:VBAGW,:VCRGO,:VMAIL");
    	
    	ilRC=sqlArrayInsert("ERPOFF", pclFields, pclValueGuides, pclData);
    }     
    
    return ilRC;    
}

void printERPOFF(ERPOFFREC *st)
{
    int a=0;	
    dbg(DEBUG, "printERPOFF:: Start");
   
    dbg(DEBUG, "printERPOFF:: Total Record [%d]", MAX_ERPOFFREC);
    for (a=0; a<MAX_ERPOFFREC; a++)
    {
        dbg(DEBUG, "printERPOFF:: Record[%d/%d]", a+1, MAX_ERPOFFREC);   
        dbg(DEBUG, "printERPOFF:: iFound[%d]", st[a].iFound);
    
        dbg(DEBUG, "printERPOFF:: URNO[%s]", st[a].URNO);
        dbg(DEBUG, "printERPOFF:: PAXH[%s]", st[a].PAXH);
        dbg(DEBUG, "printERPOFF:: SERV[%s]", st[a].SERV);
        dbg(DEBUG, "printERPOFF:: BAGW[%s]", st[a].BAGW);
        dbg(DEBUG, "printERPOFF:: CRGO[%s]", st[a].CRGO);

        dbg(DEBUG, "printERPOFF:: MAIL[%s]", st[a].MAIL);
    }
    dbg(DEBUG, "printERPOFF:: End");
       
    return;        
}


int populateERPPXO(ERPPXOREC *st, AFTREC *stDefault, char *pcpERPPXHUrno)
{
    int ilRC=RC_SUCCESS;
    int ilFlightFlag=0;
    int iUrno;
    char pclTmp[1024];
    int ilTotal=0;
    int ilTmp=0;
    _LOATAB *rlLoatabDefault;
    int ilLoatabCount=0;
    int a=0;
    
    if ( !strcmp(stDefault->ADID, "D") )
    {
        ilFlightFlag=1;
        rlLoatabDefault=rgLoatabDep;
        ilLoatabCount=igLoatabDepCount;
    }
    else
    {
        ilFlightFlag=2;
        rlLoatabDefault=rgLoatabArr;
        ilLoatabCount=igLoatabArrCount;
    }
    
    dbg(DEBUG, "populateERPPXO:: Default Urno[%s], char *pcpERPPXHUrno[%s], ilFlightFlag[%d]", 
        stDefault->URNO,pcpERPPXHUrno, ilFlightFlag);

    ilRC=getERPPXO(stDefault->URNO, pcpERPPXHUrno, st);
    if ( ilRC==RC_SUCCESS )
    {
        if ( st->iFound )
        {
        	
        }
        else
        {
            iUrno=NewUrnos("ERPPXO",1);
            sprintf(st->URNO, "%d", iUrno);
            strcpy(st->FLNU, stDefault->URNO);   
            strcpy(st->PAXH, pcpERPPXHUrno);     	
        }
        
        st->iFlightFlag=ilFlightFlag;
        
        if ( ilFlightFlag==DEPARTUREFLIGHT )
        {
            strcpy(st->APT3, stDefault->DES3);	
            
	        for (a=0; a<rgERPPXOConfig.mapDep.ilCount; a++)
	        {
	        	ilTmp=getFormulaValue(rgERPPXOConfig.mapDep.rec[a].LOAD_FIELD, rlLoatabDefault, ilLoatabCount, rgERPPXOConfig.mapDep.rec[a].VALU);
	        } 
	        
	        ilTmp=computeExpression(&rgERPPXOConfig.mapExpDep, rlLoatabDefault, ilLoatabCount);
        }
        else
        {
            strcpy(st->APT3, stDefault->ORG3);	
            for (a=0; a<rgERPPXOConfig.mapArr.ilCount; a++)
	        {
	        	ilTmp=getFormulaValue(rgERPPXOConfig.mapArr.rec[a].LOAD_FIELD, rlLoatabDefault, ilLoatabCount, rgERPPXOConfig.mapArr.rec[a].VALU);
	        } 
	        
	        ilTmp=computeExpression(&rgERPPXOConfig.mapExpArr, rlLoatabDefault, ilLoatabCount);
        }
    }
    else
        dbg(DEBUG, "populateERPPXO:: getERPPXO failed");
        
    dbg(DEBUG, "populateERPPXO:: END Result[%d], Default Urno[%s], char *pcpERPPXHUrno[%s], ilFlightFlag[%d] Found[%d]", 
        ilRC, stDefault->URNO, pcpERPPXHUrno, ilFlightFlag, st->iFound);
    return ilRC;    
}

int getERPPXO(char *pcpFlnu, char *pcpERPPXHUrno, ERPPXOREC *st)
{
    int iRet=RC_SUCCESS;
    short slCursor = 0;
    char strSql[2048];
    char pclDataBuf[32728];
        
    dbg(DEBUG, "getERPPXO:: pcpFlnu[%s], pcpERPPXHUrno[%s]", pcpFlnu, pcpERPPXHUrno);

    memset(st, 0, sizeof(ERPPXOREC));
    
    sprintf(strSql, "SELECT %s FROM ERPPXO WHERE FLNU='%s' and PAXH='%s' ", pcgErpPxoFields, pcpFlnu, pcpERPPXHUrno);
   
    memset(pclDataBuf, 0, sizeof(pclDataBuf));
    iRet=sql_if (START, &slCursor, strSql, pclDataBuf);  
    dbg(DEBUG, "getERPPXO:: executing [%s][%d]", strSql, iRet); 
    
    if ( iRet==DB_SUCCESS )
    {
        st->iFound=1;
        BuildItemBuffer (pclDataBuf, pcgErpPxoFields, igErpPxoFields, ",");
        get_real_item(st->URNO,pclDataBuf,1);         
        get_real_item(st->FLNU,pclDataBuf,2);   
        get_real_item(st->PAXH,pclDataBuf,3);   
    }
    else if ( iRet==1 )
    {
        dbg(DEBUG, "getERPPXO:: Record not found");
        iRet=RC_SUCCESS;
        st->iFound=0;
    }
    else if ( iRet==-1 )
    {
        dbg(DEBUG, "getERPPXO:: ERROR!!! DB Failed");
        st->iFound=0;
    }
    close_my_cursor(&slCursor);
        
    dbg(DEBUG, "getERPPXO:: End Result[%d] pcpFlnu[%s], pcpERPPXHUrno[%s]", 
        iRet, pcpFlnu, pcpERPPXHUrno);
    return iRet;
}

int insertERPPXO(const ERPPXOREC *st)
{
    int ilRC=RC_SUCCESS;
    char pclSql[8192];
    char pclErr[513];
    char pclAddlField[1024];
    char pclAddlValu[8192];
    DB_CONFIG_PAIR *rl;
    int ilSize=0;
    DB_CONFIG_PAIR *rlExp;
    int ilSizeExp=0;
    int a=0;

    if ( st->iFlightFlag==DEPARTUREFLIGHT )
    {
        rl=rgERPPXOConfig.mapDep.rec;	
        ilSize=rgERPPXOConfig.mapDep.ilCount;

        rlExp=rgERPPXOConfig.mapExpDep.rec;	
        ilSizeExp=rgERPPXOConfig.mapExpDep.ilCount;
    }
    else
    {
        rl=rgERPPXOConfig.mapArr.rec;	
        ilSize=rgERPPXOConfig.mapArr.ilCount;

        rlExp=rgERPPXOConfig.mapExpArr.rec;	
        ilSizeExp=rgERPPXOConfig.mapExpArr.ilCount;
    }

    strcpy(pclAddlField, "");
    strcpy(pclAddlValu, "");

    for (a=0; a<ilSize; a++)
    {
        sprintf(pclAddlField, "%s,%s", pclAddlField, rl[a].DB_FIELD);	
        sprintf(pclAddlValu, "%s,'%s'", pclAddlValu, rl[a].VALU);	
    }

    for (a=0; a<ilSizeExp; a++)
    {
        sprintf(pclAddlField, "%s,%s", pclAddlField, rlExp[a].DB_FIELD);	
        sprintf(pclAddlValu, "%s,'%s'", pclAddlValu, rlExp[a].VALU);	
    }
       
    strcpy(pclSql, "INSERT INTO ERPPXO "
                   "( "   
                   "URNO, FLNU, PAXH, APT3");    
    sprintf(pclSql, "%s%s", pclSql, pclAddlField);             
    sprintf(pclSql, "%s) VALUES (", pclSql); 
    
    sprintf(pclSql, "%s "
                    "'%s', '%s', '%s', '%s'", 
            pclSql,     
            st->URNO, st->FLNU, st->PAXH, st->APT3 ); 
    sprintf(pclSql, "%s%s", pclSql, pclAddlValu);	           
    sprintf(pclSql, "%s)", pclSql); 
       
    ilRC=executeSql(pclSql,pclErr);      

    return ilRC;    
}

int updateERPPXO(const ERPPXOREC *st)
{
    int ilRC=RC_SUCCESS;
    char pclSql[8192];
    char pclErr[513];
    DB_CONFIG_PAIR *rl;
    int ilSize=0;
    DB_CONFIG_PAIR *rlExp;
    int ilSizeExp=0;
    int a=0;

    if ( st->iFlightFlag==DEPARTUREFLIGHT )
    {
        rl=rgERPPXOConfig.mapDep.rec;	
        ilSize=rgERPPXOConfig.mapDep.ilCount;

        rlExp=rgERPPXOConfig.mapExpDep.rec;	
        ilSizeExp=rgERPPXOConfig.mapExpDep.ilCount;
    }
    else
    {
        rl=rgERPPXOConfig.mapArr.rec;	
        ilSize=rgERPPXOConfig.mapArr.ilCount;

        rlExp=rgERPPXOConfig.mapExpArr.rec;	
        ilSizeExp=rgERPPXOConfig.mapExpArr.ilCount;
    }
    
    sprintf(pclSql, "UPDATE ERPPXO SET");
    
    sprintf(pclSql, "%s"
                      " APT3='%s'",
            pclSql, 
            st->APT3);


    for (a=0; a<ilSize;a++)
    {
        sprintf(pclSql, "%s, %s='%s'", pclSql, rl[a].DB_FIELD, rl[a].VALU);	
    }

    for (a=0; a<ilSizeExp;a++)
    {
        sprintf(pclSql, "%s, %s='%s'", pclSql, rlExp[a].DB_FIELD, rlExp[a].VALU);	
    }
                
    sprintf(pclSql, "%s WHERE URNO='%s' " , pclSql, st->URNO);
    ilRC=executeSql(pclSql,pclErr);               

    return ilRC;    
}

void printERPPXO(ERPPXOREC *st)
{
	int a=0;
	
    dbg(DEBUG, "printERPPXO:: Start");
   
    dbg(DEBUG, "printERPPXO:: iFound[%d]", st->iFound);
    
    dbg(DEBUG, "printERPPXO:: URNO[%s]", st->URNO);
    dbg(DEBUG, "printERPPXO:: FLNU[%s]", st->FLNU);
    dbg(DEBUG, "printERPPXO:: PAXH[%s]", st->PAXH);
    dbg(DEBUG, "printERPPXO:: APT3[%s]", st->APT3);

    if ( st->iFlightFlag==DEPARTUREFLIGHT )
    {
        for (a=0; a<rgERPPXOConfig.mapDep.ilCount; a++)
        {
            dbg(DEBUG, "printERPPXO:: [%s][%s][%s]", 
                rgERPPXOConfig.mapDep.rec[a].DB_FIELD, rgERPPXOConfig.mapDep.rec[a].LOAD_FIELD, rgERPPXOConfig.mapDep.rec[a].VALU);   	
        }      	

        for (a=0; a<rgERPPXOConfig.mapExpDep.ilCount; a++)
        {
            dbg(DEBUG, "printERPPXO:: [%s][%s][%s]", 
                rgERPPXOConfig.mapExpDep.rec[a].DB_FIELD, rgERPPXOConfig.mapExpDep.rec[a].LOAD_FIELD, rgERPPXOConfig.mapExpDep.rec[a].VALU);   	
        }      	
    }
    else 
    {
        for (a=0; a<rgERPPXOConfig.mapArr.ilCount; a++)
        {
            dbg(DEBUG, "printERPPXO:: [%s][%s][%s]", 
                rgERPPXOConfig.mapArr.rec[a].DB_FIELD, rgERPPXOConfig.mapArr.rec[a].LOAD_FIELD, rgERPPXOConfig.mapArr.rec[a].VALU);   	
        }      
        	
        for (a=0; a<rgERPPXOConfig.mapExpArr.ilCount; a++)
        {
            dbg(DEBUG, "printERPPXO:: [%s][%s][%s]", 
                rgERPPXOConfig.mapExpArr.rec[a].DB_FIELD, rgERPPXOConfig.mapExpArr.rec[a].LOAD_FIELD, rgERPPXOConfig.mapExpArr.rec[a].VALU);   	
        }      	
    }
    
    dbg(DEBUG, "printERPPXO:: End");

    return;        
}


int getFormulaValue(char *pcpFormula, _LOATAB *rlLoatab, int ipLoatabCount, char *pcpOut)
{
    int ilRC=RC_SUCCESS;
    int ilIndex=0;
    char pclTmp[1024];
    
    dbg(DEBUG, "getFormulaValue:: Start: Formula[%s]", pcpFormula);
    
    ilIndex=getLoadConfigIndex(pcpFormula);
    if ( ilIndex==RC_FAIL )
    {
        dbg(TRACE, "getFormulaValue:: ERROR!!! [%s] not defined!", pcpFormula);
        strcpy(pcpOut, "0");	
    }
    else
    {
        ilRC=getBestLoaTabValue(&rgLoadConfig[ilIndex], rlLoatab, ipLoatabCount, pclTmp);
        sprintf(pcpOut, "%d", atoi(pclTmp));    	
    }
    
    
    dbg(DEBUG, "getFormulaValue:: End Result[%d] [%s][%s]", ilRC, pcpFormula, pcpOut);
    return ilRC;    	
}

int getBestLoaTabValue(_LOADCONFIG *rl, _LOATAB *rpLoatab, int ipLoatabCount, char *pcpOut)
{
    int ilRC=RC_FAIL;
    char pclTmp[1024]="";
    char pclValu[16]="";
    char *pclPtr;
    char *pclPtr2;
    
    dbg(DEBUG, "getBestLoaTabValue:: Start");
    
    strcpy(pcpOut, "");
    strcpy(pclTmp, rl->DSSN);
    pclPtr=strtok_r(pclTmp, ",", &pclPtr2);
    while (pclPtr)
    {
    	dbg(DEBUG, "getBestLoaTabValue:: Searching for DSSN[%s]", pclPtr);
    	ilRC=getLoaTabValue(rl->TYPE, rl->STYP, rl->SSTP, rl->SSST, rl->APC3, pclPtr, rpLoatab, ipLoatabCount, pclValu);

        if ( strcmp(pclValu, "") )
        {
        	ilRC=RC_SUCCESS;
            break;
        }
        pclPtr=strtok_r(NULL, ",", &pclPtr2);    	
    }
    strcpy(pcpOut, pclValu);
    dbg(DEBUG, "getBestLoaTabValue:: End Result[%d]", ilRC);    
    return ilRC;	
}

int getLoaTabValue(char *pcpType, char *pcpStyp, char *pcpSstp, char *pcpSsst, char *pcpApc3, char *pcpDssn, _LOATAB *rpLoatab, int ipLoatabCount, char *pcpOut)
{
    int ilRC=RC_FAIL;
    char pclBestValu[16];
    int a=0;
    
    strcpy(pcpOut, "");
    dbg(DEBUG, "getLoaTabValue:: Start");
    
    dbg(DEBUG, "getLoaTabValue:: TYPE[%s] STYP[%s] SSTP[%s] SSST[%s] APC3[%s] DSSN[%s]",
        pcpType, pcpStyp, pcpSstp, pcpSsst, pcpApc3, pcpDssn);

    for (a=0; a<ipLoatabCount; a++)
    {
        if (
               !strcmp(pcpType, rpLoatab[a].TYPE) &&
               !strcmp(pcpStyp, rpLoatab[a].STYP) &&
               !strcmp(pcpSstp, rpLoatab[a].SSTP) &&
               !strcmp(pcpSsst, rpLoatab[a].SSST) &&
               !strcmp(pcpApc3, rpLoatab[a].APC3) &&
               !strcmp(pcpDssn, rpLoatab[a].DSSN) 
           )
        {
        	dbg(DEBUG, "getLoaTabValue:: Match Found[%s]", rpLoatab[a].VALU);
        	strcpy(pcpOut, rpLoatab[a].VALU);
        	ilRC=RC_SUCCESS;
            break;	
        }	
    }
    
    if ( a>=ipLoatabCount )
        dbg(DEBUG, "getLoaTabValue:: No Match Found");  
        
    dbg(DEBUG, "getLoaTabValue:: End Result[%d][%s]", ilRC,pcpOut);
    return ilRC;	
}







int getACTTAB(char *strAct3, ACTTABREC *st)
{
    int iRet=RC_SUCCESS;
    short slCursor = 0;
    char strSql[2048];
    char pclDataBuf[32728];
        
    dbg(DEBUG, "getACTTAB:: strAct3[%s]", strAct3);

    memset(st, 0, sizeof(*st));
    sprintf(strSql, "SELECT %s FROM acttab WHERE act3='%s' ", pcgActFields, strAct3);
    dbg(DEBUG, "getACTTAB:: executing [%s]", strSql);
    
    memset(pclDataBuf, 0, sizeof(pclDataBuf));
    iRet=sql_if (START, &slCursor, strSql, pclDataBuf);  

    if ( iRet==DB_SUCCESS )
    {
        st->iFound=1;
        BuildItemBuffer (pclDataBuf, pcgActFields, igActFields, ",");
        get_real_item(st->ACT3,pclDataBuf,1);         
        get_real_item(st->ACT5,pclDataBuf,2);         
        get_real_item(st->ACBT,pclDataBuf,3);         
  
    }
    else if ( iRet==1 )
    {
        dbg(DEBUG, "getACTTAB:: Record not found");
        iRet=RC_SUCCESS;
        st->iFound=0;
    }
    else if ( iRet==-1 )
    {
        dbg(DEBUG, "getACTTAB:: ERROR!!! DB Failed");
        st->iFound=0;
    }
    close_my_cursor(&slCursor);
        
    dbg(DEBUG, "getACTTAB:: strAct3[%s] Result[%d]", strAct3, iRet);
    return iRet;
}

void printACTTAB(ACTTABREC *st)
{
    dbg(DEBUG, "printACTTAB:: Start");
   
    dbg(DEBUG, "printACTTAB:: iFound[%d]", st->iFound);
    dbg(DEBUG, "printACTTAB:: ACT3[%s]", st->ACT3);
    dbg(DEBUG, "printACTTAB:: ACT5[%s]", st->ACT5);
    dbg(DEBUG, "printACTTAB:: ACBT[%s]", st->ACBT);

    dbg(DEBUG, "printACTTAB:: End");
    return;        
}

int getFPETAB(char *strCsgn, char *pcpRegn, char *pcpAct5, char *strDate, FPETABREC *st)
{
    int iRet=RC_SUCCESS;
    short slCursor = 0;
    char strSql[2048];
    char pclDataBuf[32728];
        
    dbg(DEBUG, "getFPETAB:: strCsgn[%s], pcpRegn[%s], pcpAct5[%s], strDate[%s]", strCsgn, pcpRegn, pcpAct5, strDate);
    
    memset(st, 0, sizeof(*st));
    sprintf(strSql, "SELECT %s FROM fpetab WHERE csgn='%s' and VAFR<='%s' and VATO>='%s' and ACT5='%s' and REGN='%s' ", 
            pcgFpeFields, strCsgn, strDate, strDate, pcpAct5, pcpRegn);
    dbg(DEBUG, "getFPETAB:: executing [%s]", strSql);
    
    memset(pclDataBuf, 0, sizeof(pclDataBuf));
    iRet=sql_if (START, &slCursor, strSql, pclDataBuf);  

    if ( iRet==DB_SUCCESS )
    {
        st->iFound=1;
        BuildItemBuffer (pclDataBuf, pcgFpeFields, igFpeFields, ",");
        get_real_item(st->ADFP,pclDataBuf,1);         
        get_real_item(st->ALCO,pclDataBuf,2);         
        get_real_item(st->CDAT,pclDataBuf,3);         
        get_real_item(st->CSGN,pclDataBuf,4);         
        get_real_item(st->MTOW,pclDataBuf,5);         
        get_real_item(st->OPNA,pclDataBuf,6);         
        get_real_item(st->PERN,pclDataBuf,7);         
        get_real_item(st->REPN,pclDataBuf,8);         
        get_real_item(st->URNO,pclDataBuf,9);         
        get_real_item(st->VAFR,pclDataBuf,10);         
        get_real_item(st->VATO,pclDataBuf,11);   
    }
    else if ( iRet==1 )
    {
        dbg(DEBUG, "getFPETAB:: Record not found");
        iRet=RC_SUCCESS;
        st->iFound=0;
    }
    else if ( iRet==-1 )
    {
        dbg(DEBUG, "getFPETAB:: ERROR!!! DB Failed");
        st->iFound=0;
    }
    close_my_cursor(&slCursor);
        
    dbg(DEBUG, "getFPETAB:: strCsgn[%s], pcpRegn[%s], pcpAct5[%s], strDate[%s] Result[%d]", strCsgn, pcpRegn, pcpAct5, strDate, iRet);
    return iRet;
}

void printFPETAB(FPETABREC *st)
{
    dbg(DEBUG, "printFPETAB:: Start");
   
    dbg(DEBUG, "printFPETAB:: iFound[%d]", st->iFound);
    dbg(DEBUG, "printFPETAB:: ADFP[%s]", st->ADFP);
    dbg(DEBUG, "printFPETAB:: CSGN[%s]", st->CSGN);
    dbg(DEBUG, "printFPETAB:: URNO[%s]", st->URNO);
    dbg(DEBUG, "printFPETAB:: VAFR[%s]", st->VAFR);
    dbg(DEBUG, "printFPETAB:: VATO[%s]", st->VATO);

    dbg(DEBUG, "printFPETAB:: End");
    return;        
}

void printERPADH(ERPADHREC *stERP)
{
    dbg(DEBUG, "printERPADH:: Start");
   
    dbg(DEBUG, "printERPADH:: iFound[%d]", stERP->iFound);
    dbg(DEBUG, "printERPADH:: URNO[%s]", stERP->URNO);
    dbg(DEBUG, "printERPADH:: FLNU[%s]", stERP->FLNU);
    dbg(DEBUG, "printERPADH:: STAT[%s]", stERP->STAT);
    dbg(DEBUG, "printERPADH:: AFLT[%s]", stERP->AFLT);
    dbg(DEBUG, "printERPADH:: ADAT[%s]", stERP->ADAT);

    dbg(DEBUG, "printERPADH:: REGN[%s]", stERP->REGN);
    dbg(DEBUG, "printERPADH:: CATE[%s]", stERP->CATE);
    dbg(DEBUG, "printERPADH:: LAND[%s]", stERP->LAND);
    dbg(DEBUG, "printERPADH:: DDAT[%s]", stERP->DDAT);
    dbg(DEBUG, "printERPADH:: ETDI[%s]", stERP->ETDI);

    dbg(DEBUG, "printERPADH:: MOPA[%s]", stERP->MOPA);
    dbg(DEBUG, "printERPADH:: ALFN[%s]", stERP->ALFN);
    dbg(DEBUG, "printERPADH:: CDAT[%s]", stERP->CDAT);
    dbg(DEBUG, "printERPADH:: PSTA[%s]", stERP->PSTA);
    dbg(DEBUG, "printERPADH:: AIRB[%s]", stERP->AIRB);
    
    dbg(DEBUG, "printERPADH:: OPER[%s]", stERP->OPER);
    dbg(DEBUG, "printERPADH:: HOPO[%s]", stERP->HOPO);
    dbg(DEBUG, "printERPADH:: MTOW[%s]", stERP->MTOW);
    dbg(DEBUG, "printERPADH:: RCNO[%s]", stERP->RCNO);
    dbg(DEBUG, "printERPADH:: ETAI[%s]", stERP->ETAI);

    dbg(DEBUG, "printERPADH:: TXND[%s]", stERP->TXND);
    dbg(DEBUG, "printERPADH:: PERM[%s]", stERP->PERM);

    dbg(DEBUG, "printERPADH:: End");
    return;        
}

int getERPSCHMonday(char *pcpMonday)
{
    int ilRC=RC_SUCCESS;
    char pclSql[1024];
    char pclErr[513];
    char pclData[1024];
    char pclYesterday[16];
    char pclNow[16];
    char pclMonday[16];
    char pclNextMonday[16];    
    short slCursor=0;
    int ilUrno=0;
    
    dbg(TRACE, "getERPSCHMonday:: Start");
    
    strcpy(pcpMonday, "");
    sprintf(pclSql, "SELECT ALFN FROM ERPSCH WHERE HOPO='UFIS' ");

    slCursor=0;
    ilRC=sql_if(START,&slCursor,pclSql,pclData);
    dbg(TRACE, "getERPSCHMonday:: executing[%s] Result[%d]", pclSql, ilRC);
    if ( ilRC==RC_FAIL )
    {
        dbg(TRACE, "getERPSCHMonday:: DB ERROR!");	
    }
    else if ( ilRC==ORA_NOT_FOUND )
    {
        dbg(TRACE, "getERPSCHMonday:: Record not found!");
        getSCHDDates(pclNow, pclYesterday, pclMonday, pcpMonday);
       
        ilUrno=NewUrnos("ERPSCH",1);      
        sprintf(pclSql, "INSERT INTO ERPSCH (URNO,HOPO,ALFN) VALUES ('%d', '%s', '%s') ",
                ilUrno, "UFIS", pcpMonday);
        ilRC=executeSql(pclSql,pclErr); 
        if ( ilRC!=RC_SUCCESS  )
            dbg(TRACE, "getERPSCHMonday:: INSERT DB ERROR[%s]!", pclErr);
    }
    else
    {
        BuildItemBuffer (pclData, "ALFN", 1, ",");
        get_real_item(pcpMonday,pclData,1);
    }	  
    close_my_cursor(&slCursor);
    dbg(TRACE, "getERPSCHMonday:: Monday[%s]", pcpMonday);
    dbg(TRACE, "getERPSCHMonday:: End Result[%d]", ilRC);    
    return ilRC;	
}

int processUSCHCmd(char *pcpFlnu, int ipFtypFlag)
{
    int ilRC=RC_SUCCESS;
    short slCursor = 0;
    char pclSql[2048];
    char pclTmp[1024];
    char pclErr[513];
    char pclDataArea[8192];
    char pclRkey[16];
    char pclToday[15];
    char pclYesterday[15];
    char pclMonday[15];
    char pclNextMonday[15];
    int ilCount=0;	
    int ilAdhocArr=0;
    int ilAdhocDep=0;
    AFTREC rlAftArr;
    AFTREC rlAftDep;
    ERPSCHREC rlSch;
    int ilTmp;
    FILE *fptr;
        
    dbg(TRACE,"processUSCHCmd:: START: strFlnu[%s] iFtypeFlag[%d]",
        pcpFlnu, ipFtypFlag);

    ilRC=getERPSCHMonday(pclNextMonday);
    sprintf(pclSql,"SELECT RKEY FROM AFTTAB WHERE URNO='%s' ", 
            pcpFlnu);  

    if( rgTM_Daily.CommCfgPart.iNoOfSections_SelCri>0 )
    {
        for(ilCount=0;ilCount < rgTM_Daily.CommCfgPart.iNoOfSections_SelCri; ilCount++)
        {
            sprintf(pclTmp," AND %s ",rgTM_Daily.CommCfgPart.prSection_SelCri[ilCount].pcType);
            strcat(pclSql,pclTmp);
            
            if(strncmp(rgTM_Daily.CommCfgPart.prSection_SelCri[ilCount].pcSpecifier,"NOT",3)==0)
            {
                sprintf(pclTmp," NOT IN ");
            }
            else
            {
                sprintf(pclTmp," IN ");
            }
            strcat(pclSql,pclTmp);
            
            sprintf(pclTmp,"(%s)",rgTM_Daily.CommCfgPart.prSection_SelCri[ilCount].pcRange);
            strcat(pclSql,pclTmp);
        }        
    }
                  
    dbg(TRACE,"processUSCHCmd:: executing[%s]", pclSql);

    slCursor = 0;
    memset(pclDataArea, 0, sizeof(pclDataArea));
    memset(pclRkey, 0, sizeof(pclRkey));
    ilRC=sql_if(START,&slCursor,pclSql,pclDataArea);
  
    if ( ilRC==RC_SUCCESS )
    {
        BuildItemBuffer (pclDataArea, "RKEY", 1, ",");
        get_real_item(pclRkey,pclDataArea,1);

        ilRC=getPairedFlights(pclRkey, &rlAftArr, &rlAftDep);   
        if ( ilRC==RC_SUCCESS )
        {
            dbg(DEBUG, "processUSCHCmd:: Arrival: Found[%d] urno[%s] adho[%s] paid[%s]",
                rlAftArr.iFound, rlAftArr.URNO, rlAftArr.ADHO, rlAftArr.PAID); 
            dbg(DEBUG, "processUSCHCmd:: Departure: Found[%d] urno[%s] adho[%s] paid[%s]",
                rlAftDep.iFound, rlAftDep.URNO, rlAftDep.ADHO, rlAftDep.PAID); 
            
            ilAdhocArr=1;
            if ( rlAftArr.iFound )
                ilAdhocArr=isAdhoc(rlAftArr.URNO, rlAftArr.ADHO, rlAftArr.PAID);
                        
            ilAdhocDep=1;
            if ( rlAftDep.iFound )
                ilAdhocDep=isAdhoc(rlAftDep.URNO, rlAftDep.ADHO, rlAftDep.PAID);
                 
            dbg(TRACE, "processUSCHCmd:: Arrival Adhoc[%d], Departure Adhoc[%d]",
                ilAdhocArr, ilAdhocDep);
            if ( ilAdhocArr && ilAdhocDep )
            {
            	ilRC=RC_FAIL;
                dbg(TRACE, 	"processUSCHCmd:: Adhoc Flights!!!  Skipping!");
            }
                
                     
            if ( ilRC==RC_SUCCESS )
            {
                ilRC=processNULLCheck("ERPSCH", &rlAftArr, &rlAftDep, &ilCount);	
                dbg(TRACE, "processUSCHCmd:: Found NULL[%d]", ilCount);
                if ( ilCount )
                {
                    dbg(TRACE, "processUSCHCmd:: ERROR!!!  Mandatory Fields NULL!");
                    ilRC=RC_FAIL;
                }
            }
            
            if ( ilRC==RC_SUCCESS  )
            {
                ilRC=processDuplicates("ERPSCH", &rlAftArr, &rlAftDep, &ilCount);
                dbg(TRACE, "processUSCHCmd:: Found Duplicates[%d]", ilCount);
                if ( ilCount )
                {
                    dbg(TRACE, "processUSCHCmd:: ERROR!!!  Duplicate Found!");
                    ilRC=RC_FAIL;
                }            
            }
            
            if ( ilRC==RC_SUCCESS ) 
            {        	
                if ( rlAftArr.iFound==0 )
                {
                    dbg(DEBUG, "processUSCHCmd:: Departure Flight Only.  Checking for STOD[%s] < End Monday[%s]", 
                        rlAftDep.STOD, pclNextMonday);	
                    if ( strcmp(rlAftDep.STOD, pclNextMonday) >= 0 )
                    {
                       dbg(DEBUG, "processUSCHCmd:: FAILED!!!");
                       ilRC=RC_FAIL; 
                    }
                    else
                    {
                       dbg(DEBUG, "processUSCHCmd:: Condition Pass");	
                    }
                }
                else if ( rlAftDep.iFound==0 )
                {
                    dbg(DEBUG, "processUSCHCmd:: Arrival Flight Only.  Checking for STOA[%s] < End Monday[%s]", 
                        rlAftArr.STOA, pclNextMonday);	
                    if ( strcmp(rlAftArr.STOA, pclNextMonday) >= 0 )
                    {
                       dbg(DEBUG, "processUSCHCmd:: FAILED!!!");
                       ilRC=RC_FAIL; 
                    }
                    else
                    {
                       dbg(DEBUG, "processUSCHCmd:: Condition Pass");	
                    }
                }
                else
                {
                    dbg(DEBUG, "processUSCHCmd:: Rotation Flight.  Checking for STOA[%s] < End Monday[%s]", 
                        rlAftArr.STOA, pclNextMonday);	
                    if ( strcmp(rlAftArr.STOA, pclNextMonday) >= 0 )
                    {
                       dbg(DEBUG, "processUSCHCmd:: FAILED!!!");
                       ilRC=RC_FAIL; 
                    }	
                    else
                    {
                       dbg(DEBUG, "processUSCHCmd:: Condition Pass");	
                    }	
                }
                
                if ( ilRC==RC_SUCCESS )
                {
                    ilRC=populateERPSCH(&rlSch, &rlAftArr, &rlAftDep);    
                
                    if ( ilRC==RC_SUCCESS )
                    {   
                    	printERPSCH(&rlSch);       
                        if (rlSch.iFound==1)
                        {
                        	dbg(DEBUG, "processUSCHCmd::  ERPSCH record found");
                            dbg(DEBUG, "processUSCHCmd::  Updating ERPSCH record");
                            ilRC=updateERPSCH(&rlSch);
                        }
                        else
                        {
                        	dbg(DEBUG, "processUSCHCmd::  Inserting ERPSCH record");
                            ilRC=insertERPSCH(&rlSch);
                        }
                        if ( ilRC==RC_SUCCESS )
                        {
                            dbg(TRACE,"processUSCHCmd:: (for USCH/DSCH) SENDING SCH COMMAND TO WMERPS");
                            ilRC = sendCedaEventWithLog(igWMERPSQueue,0,pcgAftDestName,pcgAftRecvName,
                                                        pcgAftTwStart,pcgAftTwEnd,"SCH","ERPSCH","",
                                                        "FLNU",rlSch.FLNU,"",4,NETOUT_NO_ACK);    
                    
                            if ( 
                                   strcmp("", rlSch.DEPU) && 
                                   strcmp(rlSch.FLNU, rlSch.DEPU)
                               )
                            {
                            	sprintf(pclSql, "SELECT URNO FROM ERPSCH WHERE FLNU='%s'", rlSch.DEPU);
                            	ilTmp=getTableUrno(pclTmp, pclSql);
                            	dbg(DEBUG, "processUSCHCmd:: Result[%d], Departure[%s], erpsch Urno[%s]",
                            	    ilTmp, rlSch.DEPU, pclTmp);  
                            	if ( strcmp(pclTmp, "") )
                            	{                        	
                            	    dbg(TRACE, "processUSCHCmd:: Deleting unnecessary departure flight (if there is)");	
                                    sprintf(pclSql, "UPDATE ERPSCH SET STAT='U', FTYP='CANCELLED' WHERE FLNU='%s' ", rlSch.DEPU);
                                    ilRC=executeSql(pclSql,pclErr);   
                
                                    if ( ilRC==RC_FAIL )
                                        dbg(TRACE, "processUSCHCmd:: DB ERROR!");	  
                                    else if ( ilRC==ORA_NOT_FOUND ) 
                                    {
                                	    dbg(TRACE, "processUSCHCmd:: No Data Found!");	  
                                        ilRC=RC_SUCCESS;
                                    }
                                    else if ( ilRC==RC_SUCCESS )
                                    {
                                        dbg(TRACE, "processUSCHCmd:: Record updated");	 
                                        dbg(TRACE,"processUSCHCmd:: (for USCH/DSCH) SENDING SCH COMMAND TO WMERPS"); 	
                                        ilRC = sendCedaEventWithLog(igWMERPSQueue,0,pcgAftDestName,pcgAftRecvName,
                                                                    pcgAftTwStart,pcgAftTwEnd,"SCH","ERPSCH","",
                                                                    "FLNU",rlSch.DEPU,"",4,NETOUT_NO_ACK);  
                                    }
                                }                                                    
                            }	
                        }
                    }
                }            
            }
        }   
    }
    else if ( ilRC==ORA_NOT_FOUND )
        dbg(DEBUG, "processUSCHCmd:: Record not found/Too Advance ");
    else if ( ilRC==RC_FAIL )
        dbg(DEBUG, "processUSCHCmd:: ERROR! DB problem");

    close_my_cursor(&slCursor);
        
    dbg(DEBUG,"processUSCHCmd:: End: Arrival[%s] Departure[%s] Result[%d]",
        rlAftArr.URNO, rlAftDep.URNO, ilRC);

    return ilRC;
}

int processNULLCheck(char *pcpTable, AFTREC *rpArr, AFTREC *rpDep, int *ipFlag)
{
    int ilRC=RC_SUCCESS;
    char pclFile[1024];
    char pclTmp[1024];
    char pclErr[513];
    FILE *fptr;
    int ilComma=0;
    
    dbg(TRACE, "processNullCheck:: Start");
    *ipFlag=0;    
    
    if ( rpArr->iFound )
    {
        dbg(TRACE, "processNullCheck:: Checking Arrival For Null SEQ_NUM[%s], ARR_DATE[%s], ALC2[%s], DES3[%s]",
            rpArr->URNO, rpArr->STOA, rpArr->ALC2, rpArr->DES3 );	
        if ( 
               !strcmp(rpArr->URNO, "") ||
               !strcmp(rpArr->STOA, "") ||
               !strcmp(rpArr->ALC2, "") ||
               !strcmp(rpArr->DES3, "")
           )
        {
            dbg(TRACE, "processNullCheck:: ERROR!!! Arrival has NULL values"); 	
            *ipFlag=1;
            
            removeSpace(rpArr->FLNO, pclTmp);
            sprintf(pclFile, "%s/%s%s.null", pcgDuplicateFolder, pclTmp, rpArr->STOA);
            fptr=fopen(pclFile, "w");    
            if ( fptr==NULL )
            {
                dbg(TRACE, "processNullCheck:: Cannot open file[%s] for writing", pclTmp);	
            }
            else
            {
          	    fprintf(fptr, "Transaction:\n");
           	    fprintf(fptr, "URNO,FLNO,STOA,STOD,ORG3,DES3\n");
            	
          	    if ( rpArr->iFound )
           	    {
           		    fprintf(fptr, "\nArrival Details\n");
           	        fprintf(fptr, "%s,%s,%s,%s,%s,%s\n",
           	                rpArr->URNO, rpArr->FLNO, rpArr->STOA, rpArr->STOD, rpArr->ORG3, rpArr->DES3);
           	    }   
           	    fclose(fptr);         	            
            } 
        }
    }
        
    if ( rpDep->iFound )
    {
        dbg(TRACE, "processNullCheck:: Checking Departure For Null SEQ_NUM[%s], DEP_DATE[%s], ALC2[%s], ORG3[%s]",
            rpDep->URNO, rpDep->STOD, rpDep->ALC2, rpDep->DES3 );	
        if ( 
               !strcmp(rpDep->URNO, "") ||
               !strcmp(rpDep->STOD, "") ||
               !strcmp(rpDep->ALC2, "") ||
               !strcmp(rpDep->ORG3, "")
           )
        {
            dbg(TRACE, "processNullCheck:: ERROR!!! Departure has NULL values"); 	
            *ipFlag=1;
            if ( rpArr->iFound ) 
            {
                removeSpace(rpArr->FLNO, pclTmp);
                sprintf(pclFile, "%s/%s%s.null", pcgDuplicateFolder, pclTmp, rpArr->STOA);
            }
            else
            {
                removeSpace(rpDep->FLNO, pclTmp);
                sprintf(pclFile, "%s/%s%s.null", pcgDuplicateFolder, pclTmp, rpDep->STOD);
            }
                            
            fptr=fopen(pclFile, "w");    
            if ( fptr==NULL )
            {
                dbg(TRACE, "processNullCheck:: Cannot open file[%s] for writing", pclFile);	
            }
            else
            {
          	    fprintf(fptr, "Transaction:\n");
           	    fprintf(fptr, "URNO,FLNO,STOA,STOD,ORG3,DES3\n");
            	
          	    if ( rpArr->iFound )
           	    {
           		    fprintf(fptr, "\nArrival Details\n");
           	        fprintf(fptr, "%s,%s,%s,%s,%s,%s\n",
           	                rpDep->URNO, rpDep->FLNO, rpDep->STOA, rpDep->STOD, rpDep->ORG3, rpDep->DES3);
           	    }   
           	    fclose(fptr);         	            
            } 
        }
    }
    
    if ( *ipFlag )
    {
    	ilComma=0;
    	sprintf(pclTmp, "DELETE FROM %s WHERE STAT!='S' AND FLNU IN (", pcpTable);
    	
    	if ( rpArr->iFound )
    	{
    		sprintf(pclTmp, "%s '%s'", pclTmp, rpArr->URNO);
    	    ilComma=1;	
    	}
    	
    	if ( rpDep->iFound )
    	{
    		if ( ilComma )
                sprintf(pclTmp, "%s, '%s'", pclTmp, rpDep->URNO);
    		else
    		{
    		    sprintf(pclTmp, "%s '%s'", pclTmp, rpDep->URNO);
                ilComma=1;	
            }
    	}
    	
    	sprintf(pclTmp, "%s )", pclTmp);
    	dbg(TRACE, "processNullCheck:: Deleting NULL if possible [%s]", pclTmp);

    	ilRC=executeSql(pclTmp, pclErr);
    	if ( ilRC==ORA_NOT_FOUND )
    	{
    	    dbg(TRACE, "processNullCheck:: No record found");
    	    ilRC=RC_SUCCESS;	
    	}
    }   	
                
    dbg(TRACE, "processNullCheck:: End Result[%d]", ilRC);
    return ilRC;    	
}

int processDuplicates(char *pcpTable, AFTREC *rpArr, AFTREC *rpDep, int *ipFlag)
{
    int ilRC=RC_SUCCESS;
    char pclArrDupUrnos[2048]="";
    char pclArrDuplicates[2048]="";
    char pclDepDupUrnos[2048]="";
    char pclDepDuplicates[2048]="";
    char pclTmp[2048];
    char pclFile[2048];
    char pclErr[513];
    FILE *fptr;
    int ilComma=0;
    char *pclPtr1, *pclPtr2;
    
    dbg(TRACE, "processDuplicates:: Start");
    *ipFlag=0;
    
    if ( rpArr->iFound )
    {
        dbg(TRACE, "processDuplicates:: Checking Arrival For Duplicates");	
        ilRC=checkDuplicate(rpArr, pclArrDupUrnos, pclArrDuplicates);
    }
        
    if ( rpDep->iFound )
    {
        dbg(TRACE, "processDuplicates:: Checking Departure For Duplicates");		
        ilRC=checkDuplicate(rpDep, pclDepDupUrnos, pclDepDuplicates);
    }
    
    dbg(TRACE, "processDuplicates:: Arrival Duplicates[%s][%s]", pclArrDuplicates, pclArrDupUrnos);
    dbg(TRACE, "processDuplicates:: Departure Duplicates[%s][%s]", pclDepDuplicates, pclDepDupUrnos);

    if ( strcmp(pclArrDuplicates, "") || strcmp(pclDepDuplicates, "") )
    {
    	*ipFlag=1;
    	
     	if ( rpArr->iFound )
     	{
            removeSpace(rpArr->FLNO, pclTmp);
            sprintf(pclFile, "%s/%s%s.dup", pcgDuplicateFolder, pclTmp, rpArr->STOA);
        }
        else
        {
            removeSpace(rpDep->FLNO, pclTmp);
            sprintf(pclFile, "%s/%s%s.dup", pcgDuplicateFolder, pclTmp, rpDep->STOD);
        }       
        dbg(TRACE, "processDuplicates:: Writing Duplicate Report in [%s]", pclFile);
            
        fptr=fopen(pclFile, "w");    
        if ( fptr==NULL )
        {
            dbg(TRACE, "processDuplicates:: Cannot open file[%s] for writing", pclFile);	
        }
        else
        {
          	fprintf(fptr, "Transaction:\n");
           	fprintf(fptr, "URNO,FLNO,STOA,STOD,ORG3,DES3,CSGN\n");
            	
          	if ( rpArr->iFound )
           	{
           		fprintf(fptr, "\nArrival Details\n");
           	    fprintf(fptr, "%s,%s,%s,%s,%s,%s,%s\n",
           	            rpArr->URNO, rpArr->FLNO, rpArr->STOA, rpArr->STOD, rpArr->ORG3, rpArr->DES3, rpArr->CSGN);
           	}            	            
            	
            if ( rpDep->iFound )
            {
            	fprintf(fptr, "\nDeparture Details\n");
                fprintf(fptr, "%s,%s,%s,%s,%s,%s,%s\n",
                        rpDep->URNO, rpDep->FLNO, rpDep->STOA, rpDep->STOD, rpDep->ORG3, rpDep->DES3, rpDep->CSGN);
            }
            	
            if ( strcmp(pclArrDuplicates, "") )
            {     	
                fprintf(fptr, "\nDuplicates For Arrival Details\n");
                fprintf(fptr, "%s\n", pclArrDuplicates);
            }
            	
            if ( strcmp(pclDepDuplicates, "") )
            {     	
                fprintf(fptr, "\nDuplicates For Departure Details\n");
                fprintf(fptr, "%s\n", pclDepDuplicates);
            }
                  	
            fclose(fptr);	
        }        	       	
    }
      
    if ( *ipFlag )
    {
    	ilComma=0;
    	sprintf(pclTmp, "DELETE FROM %s WHERE STAT!='S' AND FLNU IN (", pcpTable);
    	
    	if ( rpArr->iFound )
    	{
    		sprintf(pclTmp, "%s '%s'", pclTmp, rpArr->URNO);
    	    ilComma=1;	
    	}
    	
    	if ( rpDep->iFound )
    	{
    		if ( ilComma )
                sprintf(pclTmp, "%s, '%s'", pclTmp, rpDep->URNO);
    		else
    		{
    		    sprintf(pclTmp, "%s '%s'", pclTmp, rpDep->URNO);
                ilComma=1;	
            }
    	}
    	
    	if ( strcmp(pclArrDupUrnos, "") )
    	{
    	    pclPtr1=strtok_r(pclArrDupUrnos, ",", &pclPtr2);
	        while (pclPtr1)
	        {
	        	if ( ilComma )
	        	{
	        	    sprintf(pclTmp, "%s,'%s'", pclTmp, pclPtr1);		
	        	}
	        	else
	        	{
	        	    ilComma=1;
	        	    sprintf(pclTmp, "%s '%s'", pclTmp, pclPtr1);	
	        	}
	            pclPtr1=strtok_r(NULL, ",", &pclPtr2);	
	        }
    	}

    	if ( strcmp(pclDepDupUrnos, "") )
    	{
    	    pclPtr1=strtok_r(pclDepDupUrnos, ",", &pclPtr2);
	        while (pclPtr1)
	        {
	        	if ( ilComma )
	        	{
	        	    sprintf(pclTmp, "%s, '%s'", pclTmp, pclPtr1);		
	        	}
	        	else
	        	{
	        	    ilComma=1;
	        	    sprintf(pclTmp, "%s '%s'", pclTmp, pclPtr1);	
	        	}
	            pclPtr1=strtok_r(NULL, ",", &pclPtr2);	
	        }
    	}    	
    	sprintf(pclTmp, "%s )", pclTmp);
    	dbg(TRACE, "processDuplicates:: Deleting Duplicates if possible [%s]", pclTmp);

    	ilRC=executeSql(pclTmp, pclErr);
    	if ( ilRC==ORA_NOT_FOUND )
    	{
    	    dbg(TRACE, "processDuplicates:: No record found");
    	    ilRC=RC_SUCCESS;	
    	}
    }   	
                
    dbg(TRACE, "processDuplicates:: End Result[%d]", ilRC);
    return ilRC;    	
}


static int processERPCmd(char *pclFlnu)
{
    int ilRC=RC_SUCCESS;
    int ilTmp;
    char pclFlnuArr[16];
    char pclFlnuDep[16];
    char pclRkey[16];
        
    dbg(TRACE, "processERPCmd:: Start");
    dbg(TRACE, "processERPCmd::  Processing [%s]", pclFlnu);
   
    ilTmp = get_flds_count(pclFlnu);
    get_fld_value(pclFlnu,1,pclFlnuArr);
    get_fld_value(pclFlnu,2,pclFlnuDep);
       
    if ( strcmp(pclFlnuArr, "") )
        ilRC=getRkey(pclFlnuArr, pclRkey);
    else
        ilRC=getRkey(pclFlnuDep, pclRkey);

    dbg(TRACE, "processERPCmd:: Result[%d], Arrival[%s], Departure[%s], Rkey[%s]", 
        ilRC, pclFlnuArr, pclFlnuDep, pclRkey);	
     
    if ( ilRC==RC_SUCCESS )
    {
        ilRC=doAllERPBILLTables(pclRkey, 1);
        dbg(TRACE, "processERPCmd:: doAllERPBILLTables Result[%d], pclRkey[%s]", ilRC, pclRkey);                   
    }
    
    dbg(TRACE, "processERPCmd:: End Result[%d]", ilRC);	
    return ilRC;
}

int processBILL(char *pcpFromLAND, char *pcpToLAND,char *pcpFromAIRB,char *pcpToAIRB,char *pcpDate)
{
	int ilRC=RC_SUCCESS;
	int ilTmp=RC_SUCCESS;
	char pclSql[2048];
	int ilCount=0;
    short slCursor=0;
    char pclRkey[1024];
    int ilRecordCount=0;
    FILE *fptr;
    
    dbg(TRACE, "processBILL:: Start");
    
    fptr=fopen("/ceda/bin/erpbil-BILL.txt", "w");
    if ( fptr==NULL )
    {
        dbg(TRACE, "processBILL:: ERROR!!! Cannot open file /ceda/bin/erpbil-BILL.txt for writing");
        ilRC=RC_FAIL;    	
    }
    else
    {
        sprintf(pclSql, "SELECT DISTINCT(RKEY) FROM AFTTAB");
        sprintf(pclSql, "%s WHERE (DQCS='C' or DQCS='P') AND (LAND<'%s' OR AIRB<'%s')", pclSql, pcpFromLAND,pcpFromAIRB);
        if(rgTM_Bill.CommCfgPart.iNoOfSections_SelCri > 0)
        {
            for(ilCount=0;ilCount < rgTM_Bill.CommCfgPart.iNoOfSections_SelCri; ilCount++)
            {
                sprintf(pclSql, "%s AND %s", pclSql, rgTM_Bill.CommCfgPart.prSection_SelCri[ilCount].pcType);
                if(strncmp(rgTM_Bill.CommCfgPart.prSection_SelCri[ilCount].pcSpecifier,"NOT",3)==0)
                    sprintf(pclSql, "%s NOT IN", pclSql);
                else
                    sprintf(pclSql ,"%s IN", pclSql);

                sprintf(pclSql,"%s (%s)", pclSql, rgTM_Bill.CommCfgPart.prSection_SelCri[ilCount].pcRange);
            }
        }
        else
        {
            dbg(TRACE, "processBILL:: rgTM_Bill.iNoOfSections_SelCri <= 0, not extra sql query conditions");    
        }
    
        dbg(TRACE, "processBILL:: pclSql <%s> ", pclSql);
    
        ilRC=sql_if(START, &slCursor, pclSql, pclRkey);    
        if ( ilRC==RC_FAIL )
        {
            dbg(TRACE, "processBILL:: DB ERROR!");    	
        }
        else if  ( ilRC==ORA_NOT_FOUND )
        {
            dbg(TRACE, "processBILL:: No record to process");
            ilRC=RC_SUCCESS;	
        }
        else
        {
            while( ilTmp==RC_SUCCESS )
            {
        	    fprintf(fptr, "%s\n", pclRkey); 
                ilTmp=sql_if(NEXT, &slCursor, pclSql, pclRkey);	
            }
        }
        close_my_cursor(&slCursor);
        fclose(fptr);            	
    }

    if ( ilRC==RC_SUCCESS )
    {
        fptr=fopen("/ceda/bin/erpbil-BILL.txt", "r");
        if ( fptr==NULL )
        {
            dbg(TRACE, "processBILL:: ERROR!!! Cannot open file /ceda/bin/erpbil-BILL.txt for reading");
            ilRC=RC_FAIL;    	
        }
        else
        {
            while ( fgets(pclRkey, 1000, fptr) )
            {
                pclRkey[strlen(pclRkey)-1]=0;
        	    dbg(TRACE, "processBILL:: [%d] processing RKEY[%s]", ++ilRecordCount, pclRkey);
        	
        	    ilRC=doAllERPBILLTables(pclRkey, 0);

        	    dbg(TRACE, "processBILL:: [%d] processing RKEY[%s] End Result[%d]", ilRecordCount, pclRkey, ilRC);
            }
            fclose(fptr);	
        }    	
    }
    
    dbg(TRACE, "processBILL:: Sending to WMERPB");
    ilRC = sendCedaEventWithLog(igWMERPBQueue, 0,pcgAftDestName,pcgAftRecvName,
                                pcgAftTwStart,pcgAftTwEnd,"BILL","","",
                               "STATUS","OK","",4,NETOUT_NO_ACK); 
                                                          
    dbg(TRACE, "processBILL:: Total Record Process[%d]", ilRecordCount);
    dbg(TRACE, "processBILL:: End Result[%d]", ilRC);
    return ilRC;
}

int getRkey(char *pcpFlnu, char *pcpOut)
{
    int ilRC=RC_SUCCESS;
    char pclSql[1024];
    char pclRkey[16];
    short slCursor=0;
    
    dbg(TRACE, "getRkey:: Start pcpFlnu[%s]", pcpFlnu);
    
    strcpy(pcpOut, "");
    sprintf(pclSql, "SELECT RKEY FROM AFTTAB WHERE URNO='%s' ", pcpFlnu);
    ilRC=sql_if(START,&slCursor,pclSql,pclRkey);
    dbg(DEBUG, "getRkey:: executing [%s][%d]", pclSql, ilRC);
    if ( ilRC==RC_FAIL )
    {
        dbg(TRACE, "getRkey:: DB ERROR!!!");	
    }
    else if ( ilRC==ORA_NOT_FOUND )
    {
        dbg(TRACE, "getRkey:: Record Not Found!");
    }
    else
    {
        strcpy(pcpOut, pclRkey);	
    }
    close_my_cursor(&slCursor);
    dbg(TRACE, "getRkey:: End Result[%d], pcpFlnu[%s], pcpOut[%s]",
        ilRC, pcpFlnu, pcpOut);
    return ilRC;    	
}

int doAllERPBILLTables(char *pcpRkey, int ipSendToBroker)
{
    int ilRC=RC_SUCCESS;
    AFTREC rlAftArr;
    AFTREC rlAftDep;
    AFTREC *rlDef;
    ERPBILLREC rlERPBill;
    ERPATCREC rlERPATC;
    ERPPXHREC rlERPPXHArr;
    ERPPXHREC rlERPPXHDep;
    ERPPXTREC rlERPPXT;
    ERPPXRREC rlERPPXR;  
    ERPPXMREC rlERPPXM;
    ERPPXOREC rlERPPXO;
    ERPOFFREC rlERPOFF[MAX_ERPOFFREC];
    int ilFlag=0;
    char pclTmp[1024];
    
    dbg(TRACE, "doAllERPBILLTables:: Start");
    dbg(TRACE, "doAllERPBILLTables:: pcpRkey[%s]", pcpRkey);

    ilRC=getPairedFlights(pcpRkey, &rlAftArr, &rlAftDep);

    if ( ilRC==RC_SUCCESS )
    {
        if ( rlAftArr.iFound )
        {
            if ( 
                   !strcmp(rlAftArr.DQCS, "P") ||
                   !strcmp(rlAftArr.DQCS, "C") ||
                   !strcmp(rlAftArr.DQCS, "S") ||
                   !strcmp(rlAftArr.DQCS, "SR") ||
                   !strcmp(rlAftArr.DQCS, "LCMP") ||
                   !strcmp(rlAftArr.DQCS, "LPMC")
               )
                ;
            else
            {
                dbg(TRACE, "doAllERPBILLTables:: ERROR!!!  Arrival DQCS is not P/C/S/SR! Urno[%s] DQCS[%s]",
                    rlAftArr.URNO, rlAftArr.DQCS);
                ilRC=RC_FAIL; 	
            }	
        }	

        if ( rlAftDep.iFound )
        {
            if ( 
                  !strcmp(rlAftDep.DQCS, "P") || 
                  !strcmp(rlAftDep.DQCS, "C") ||
                  !strcmp(rlAftDep.DQCS, "S") ||
                  !strcmp(rlAftDep.DQCS, "SR") ||
                  !strcmp(rlAftDep.DQCS, "LCMP") ||
                  !strcmp(rlAftDep.DQCS, "LPMC")
                )
                ;
            else
            {
                dbg(TRACE, "doAllERPBILLTables:: ERROR!!!  Departure DQCS is not P/C/S/SR! Urno[%s] DQCS[%s]",
                    rlAftDep.URNO, rlAftDep.DQCS);
                ilRC=RC_FAIL; 	
            }	
        }	
    }
    
    if ( ilRC==RC_SUCCESS )
    {
        ilRC=processNULLCheck("ERPBILL", &rlAftArr, &rlAftDep, &ilFlag);	
        dbg(TRACE, "doAllERPBILLTables:: Found NULL[%d]", ilFlag);
        if ( ilFlag )
        {
             dbg(TRACE, "doAllERPBILLTables:: ERROR!!!  Mandatory Fields NULL!");
             ilRC=RC_FAIL;
        }
    }
        
    if ( ilRC==RC_SUCCESS  )
    {
        ilRC=processDuplicates("ERPBILL", &rlAftArr, &rlAftDep, &ilFlag);
        dbg(TRACE, "doAllERPBILLTables:: Found Duplicates[%d]", ilFlag);
        if ( ilFlag )
        {
            dbg(TRACE, "doAllERPBILLTables:: ERROR!!!  Duplicate Found!");
            ilRC=RC_FAIL;
        }            
    }
    
    if ( ilRC==RC_SUCCESS )
    {
    	dbg(TRACE, "doAllERPBILLTables:: Arrival Found[%d] Urno[%s], Departure Found[%d] Urno[%s]", 
    	    rlAftArr.iFound, rlAftArr.URNO, rlAftDep.iFound, rlAftDep.URNO);    	
        if (  rlAftArr.iFound==1 && rlAftDep.iFound==0 )
        {
        	dbg(TRACE, "doAllERPBILLTables:: Arrival Flight Only processing!");
            ilRC= populateERPATC(&rlERPATC, &rlAftArr);    
            printERPATC(&rlERPATC);
            if ( ilRC==RC_SUCCESS )
            {
                if (rlERPATC.iFound==1)
                {
                    dbg(TRACE, "doAllERPBILLTables:: Already exists in ERPATC!  Do nothing!");
                    ilRC=RC_FAIL;
                }
                else
                    ilRC=insertERPATC(&rlERPATC);
            }   
  
            if ( ilRC==RC_SUCCESS )
            {
                dbg(TRACE, "doAllERPBILLTables:: Checking if Default is an adhoc Flight: URNO[%s], ADHO[%s], PAID[%s]",
                    rlAftArr.URNO, rlAftArr.ADHO, rlAftArr.PAID);
                if ( !strcmp(rlAftArr.ADHO, "X") || strcmp(rlAftArr.PAID, "K") )
                {
                    dbg(TRACE, "doAllERPBILLTables:: Flight is ADHOC.  Sending flight to erpadh");
                	    
                    sendCedaEventWithLog(igERPADHQueue,0,pcgAftDestName,pcgAftRecvName,
                                         pcgAftTwStart,pcgAftTwEnd,"UADH","ERPADH","",
                                         "URNO",rlDef->URNO,"",4,NETOUT_NO_ACK);                              
                }
            }

            if ( rlAftArr.iFound==1 )
            {
                dbg(TRACE, "doAllERPBILLTables::  Send to Flight. Update STAT of Arrival[%s] to dqcs='S' ",
                    rlAftArr.URNO);     
                sprintf (pcgAftSel,"WHERE URNO = '%s'",rlAftArr.URNO);
                ilRC=sendCedaEventWithLog(igFlightQueue,0,pcgAftDestName,pcgAftRecvName,
                                          pcgAftTwStart,pcgAftTwEnd,"DQF","AFTTAB",pcgAftSel,
                                          "DQCS","S","",4,NETOUT_NO_ACK); 
            }
                                              
            if ( ilRC==RC_SUCCESS )
            {
                if ( ipSendToBroker )
                {
                    dbg(TRACE, "doAllERPBILLTables:: Sending to WMERPB");
                    /*
                    ilRC = sendCedaEventWithLog(igWMERPBQueue,0,pcgAftDestName,pcgAftRecvName,
                                                pcgAftTwStart,pcgAftTwEnd,"BILL","ERPATC","",
                                                "URNO", rlERPATC.URNO,"",4,NETOUT_NO_ACK); 
                    */
                    ilRC = sendCedaEventWithLog(igWMERPBQueue, 0,pcgAftDestName,pcgAftRecvName,
                                                pcgAftTwStart,pcgAftTwEnd,"BILL","","",
                                                "STATUS","OK","",4,NETOUT_NO_ACK);  
                }
            }     	
        }   	
    	else
    	{
            if ( rlAftArr.iFound==1 )
                rlDef=&rlAftArr;
            else
                rlDef=&rlAftDep;
            
        	dbg(DEBUG, "doAllERPBILLTables:: rlAftArr:iFound[%d], Urno[%s], rlAftDep:iFound[%d], Urno[%s]",
        	    rlAftArr.iFound, rlAftArr.URNO, rlAftDep.iFound, rlAftDep.URNO);
        	if ( rlAftArr.iFound==1 )
        	{    		    
        		dbg(TRACE, "doAllERPBILLTables:: Arrival/Rotation Flight:  Use Arrival Details");
        		ilRC=getERPBILL(rlAftArr.URNO, &rlERPBill);
                dbg(TRACE, "doAllERPBILLTables:: getERPBILL[%d] ERPBIL:Found[%d], URNO[%s], FLNU[%s], STAT[%s]", 
                    ilRC, rlERPBill.iFound, rlERPBill.URNO, rlERPBill.FLNU, rlERPBill.STAT);
            }
        	else if ( rlAftDep.iFound==1 )
        	{
        		dbg(TRACE, "doAllERPBILLTables:: Departure Flight");
        		ilRC=getERPBILL(rlAftDep.URNO, &rlERPBill);
                dbg(TRACE, "doAllERPBILLTables:: getERPBILL[%d] ERPBIL:Found[%d], URNO[%s], FLNU[%s], STAT[%s]", 
                    ilRC, rlERPBill.iFound, rlERPBill.URNO, rlERPBill.FLNU, rlERPBill.STAT);
            }
            else
            {
                dbg(TRACE, "doAllERPBILLTables:: ERROR! Flight Not Found!");
                ilRC=RC_FAIL;
            }
            
            if ( ilRC==RC_SUCCESS )
            {
                if ( rlAftArr.iFound==1 )
                {
                    ilRC=getERPPXH(rlAftArr.URNO, &rlERPPXHArr);      
                    dbg(TRACE, "doAllERPBILLTables:: Arrival getERPPXH[%d] ERPPXH:Found[%d], URNO[%s], FLNU[%s]", 
                        ilRC, rlERPPXHArr.iFound, rlERPPXHArr.URNO, rlERPPXHArr.FLNU); 	
                }
            }
        
            if ( ilRC==RC_SUCCESS )
            {
                if ( rlAftDep.iFound==1 )
                {
                    ilRC=getERPPXH(rlAftDep.URNO, &rlERPPXHDep);      
                    dbg(TRACE, "doAllERPBILLTables:: Departure getERPPXH[%d] ERPPXH:Found[%d], URNO[%s], FLNU[%s]", 
                        ilRC, rlERPPXHDep.iFound, rlERPPXHDep.URNO, rlERPPXHDep.FLNU); 	
                }
            }
             
            if ( ilRC==RC_SUCCESS )
            {
 	    	    if ( rlERPBill.iFound==1 )
 	    	    {
 	    	        if ( !strcmp(rlERPBill.STAT, "E") )
   	    	        {
   	                    ilRC=deleteAllERPBILLTables(rlAftArr.URNO, rlAftDep.URNO, 
   	                                                rlERPBill.URNO,
   	                                                rlERPPXHArr.URNO, rlERPPXHDep.URNO);
                        dbg(DEBUG, "doAllERPBILLTables:: executing deleteAllERPBILLTables For Departure[%s] Result[%d]", rlAftArr.URNO, ilRC);
                    }
                    else
                    {
                	    dbg(TRACE, "doAllERPBILLTables:: ERPBILL: Found[%d], URNO[%s], FLNU[%s], STAT[%s]",
                	        rlERPBill.iFound, rlERPBill.URNO, rlERPBill.FLNU, rlERPBill.STAT);
                        dbg(TRACE, "doAllERPBILLTables:: ERROR!!! ERPBILL Found and Stat!=E");
                        ilRC=RC_FAIL;	
                    }
                }
            }        
        
            if ( ilRC==RC_SUCCESS )
            {        
                if ( rlAftArr.iFound==1 )
                {
                	dbg(TRACE, "doAllERPBILLTables:: get Arrival LOATAB records");
                	rgLoatabArr=allocateLoatabRecords(rlAftArr.URNO, &igLoatabArrCount);
                	if ( igLoatabArrCount>0 )
                	{
                        ilRC=getLoatabRecords(rlAftArr.URNO, rgLoatabArr, igLoatabArrCount);	
                        printLoatab(rgLoatabArr, igLoatabArrCount);
                    }
                }
            }
            
            if ( ilRC==RC_SUCCESS )
            {        
                if ( rlAftDep.iFound==1 )
                {
                	dbg(TRACE, "doAllERPBILLTables:: get Departure LOATAB records");
                	rgLoatabDep=allocateLoatabRecords(rlAftDep.URNO, &igLoatabDepCount);
                	if ( igLoatabDepCount>0 )
                	{
                        ilRC=getLoatabRecords(rlAftDep.URNO, rgLoatabDep, igLoatabDepCount);	
                        printLoatab(rgLoatabDep, igLoatabDepCount);
                    }
                }
            }
            
            if ( ilRC==RC_SUCCESS )
            {
            	dbg(TRACE, "doAllERPBILLTables:: For ERPBILL");
                ilRC=populateERPBILL(&rlERPBill, &rlAftArr, &rlAftDep);    
                printERPBILL(&rlERPBill);
                if ( ilRC==RC_SUCCESS )
                {
                    if (rlERPBill.iFound==1)
                    {
                    	dbg(TRACE, "doAllERPBILLTables:: updateERPBILL Result[%d]", ilRC); 
                        ilRC=updateERPBILL(&rlERPBill);
                    }
                    else
                    {
                    	dbg(TRACE, "doAllERPBILLTables:: insertERPBILL Result[%d]", ilRC); 
                        ilRC=insertERPBILL(&rlERPBill);
                    }
                }
                        
                if ( ilRC==RC_SUCCESS )
                {
                	dbg(TRACE, "doAllERPBILLTables:: For ERPDLY: rlAftArr.iFound[%d], rlAftDep.iFound[%d]",
                	    rlAftArr.iFound, rlAftDep.iFound);
                	if ( rlAftArr.iFound==1 )
                	{
                        ilRC=doERPDLY(rlAftArr.URNO, rlERPBill.URNO);	            
                        dbg(TRACE, "doAllERPBILLTables:: doERPDLY on Arrival Result[%d]", ilRC); 
                    }
                    
                    if ( ilRC==RC_SUCCESS )
                    {
                 	    if ( rlAftDep.iFound==1 )
                	    {
                            ilRC=doERPDLY(rlAftDep.URNO, rlERPBill.URNO);	            
                            dbg(TRACE, "doAllERPBILLTables:: doERPDLY on Departure Result[%d]", ilRC); 
                        }                       	
                    }
                }
                            
                if ( ilRC==RC_SUCCESS )
                {
                    ilRC= populateERPATC(&rlERPATC, &rlAftArr);    
                    printERPATC(&rlERPATC);
                    if ( ilRC==RC_SUCCESS )
                    {
                        if (rlERPATC.iFound==1)
                        {
                            dbg(TRACE, "doAllERPBILLTables:: Already exists in ERPATC for Arrival!  Do nothing!");
                        }
                        else
                            ilRC=insertERPATC(&rlERPATC);
                    }   
            
                    if ( ilRC==RC_SUCCESS )
                    {
                        if ( rlAftDep.iFound==1 )
                        {
                            ilRC= populateERPATC(&rlERPATC, &rlAftDep);    
                            printERPATC(&rlERPATC);
                            if ( ilRC==RC_SUCCESS )
                            {
                                if (rlERPATC.iFound==1)
                                    ilRC=updateERPATC(&rlERPATC);
                                else
                                    ilRC=insertERPATC(&rlERPATC);
                            }
                            dbg(TRACE, "doAllERPBILLTables:: For Departure ERPATC Result[%d]", ilRC);
                        }
                    }
                }
                
                if ( ilRC==RC_SUCCESS )
                {
                	dbg(TRACE, "doAllERPBILLTables:: Checking if Default is an adhoc Flight: URNO[%s], ADHO[%s], PAID[%s]",
                	    rlDef->URNO, rlDef->ADHO, rlDef->PAID);
                    if ( !strcmp(rlDef->ADHO, "X") || strcmp(rlDef->PAID, "K") )
                    {
                	    dbg(TRACE, "doAllERPBILLTables:: Flight is ADHOC.  Sending flight to erpadh");
                	    
                        sendCedaEventWithLog(igERPADHQueue,0,pcgAftDestName,pcgAftRecvName,
                                             pcgAftTwStart,pcgAftTwEnd,"UADH","ERPADH","",
                                             "URNO",rlDef->URNO,"",4,NETOUT_NO_ACK);                              
                    }
                }
                
                if ( ilRC==RC_SUCCESS )
                {
                    if ( rlAftArr.iFound==1 )
                    {
                        ilRC= populateERPPXH(&rlERPPXHArr, &rlAftArr, rlERPBill.MMVN);    
                        
                        /*  check if its rotation and change details for departure */
                        if ( rlAftDep.iFound==1 )
                        {
                            formatVIAL(atoi(rlAftDep.VIAN), rlAftDep.VIAL, pclTmp, "/");
                	        if ( !strcmp(pclTmp, "") )
                	            strcpy(rlERPPXHArr.DES3, rlAftDep.DES3); 
                	        else
                	            sprintf(rlERPPXHArr.DES3, "%s/%s", pclTmp, rlAftDep.DES3);
                        }
                        
                        printERPPXH(&rlERPPXHArr);
                        if ( ilRC==RC_SUCCESS )
                        {
                            if (rlERPPXHArr.iFound==1)
                                ilRC=updateERPPXH(&rlERPPXHArr);
                            else
                                ilRC=insertERPPXH(&rlERPPXHArr);
                        }
                        dbg(TRACE, "doAllERPBILLTables:: For Arrival ERPPXH Result[%d]", ilRC);
                    }
                    
                    if ( ilRC==RC_SUCCESS )
                    {
                        if ( rlAftDep.iFound==1 )
                        {
                            ilRC= populateERPPXH(&rlERPPXHDep, &rlAftDep, rlERPBill.MMVN);  
                        
                            /*  check if its rotation and change details for Arrival */
                            if ( rlAftArr.iFound==1 )
                            {
                                formatVIAL(atoi(rlAftArr.VIAN), rlAftArr.VIAL, pclTmp, "/");
                	            if ( !strcmp(pclTmp, "") )
                	                strcpy(rlERPPXHDep.ORG3, rlAftArr.ORG3); 
                 	            else
                	                sprintf(rlERPPXHDep.ORG3, "%s/%s", rlAftArr.ORG3, pclTmp);	
                            }
                            printERPPXH(&rlERPPXHDep);
            
                            if ( ilRC==RC_SUCCESS )
                            {
                                if ( rlERPPXHDep.iFound==1 )
                                    ilRC=updateERPPXH(&rlERPPXHDep);
                                else
                                    ilRC=insertERPPXH(&rlERPPXHDep);
                            }
                            dbg(TRACE, "doAllERPBILLTables:: For Departure ERPPXH Result[%d]", ilRC);
                        }
                    }
                }
            
                if ( ilRC==RC_SUCCESS )
                {
                    if ( rlAftArr.iFound==1 )
                    {
                        ilRC= populateERPPXT(&rlERPPXT, &rlAftArr, rlERPPXHArr.URNO);    
                        printERPPXT(&rlERPPXT);
                        if ( ilRC==RC_SUCCESS )
                        {
                            if ( rlERPPXT.iFound==1 )
                                ilRC=updateERPPXT(&rlERPPXT);
                            else
                                ilRC=insertERPPXT(&rlERPPXT);
                        }
                        dbg(TRACE, "doAllERPBILLTables:: For Arrival ERPPXT Result[%d]", ilRC);
                    }
            
                    if ( ilRC==RC_SUCCESS )
                    {
                        if ( rlAftDep.iFound==1 )
                        {
                            ilRC= populateERPPXT(&rlERPPXT, &rlAftDep, rlERPPXHDep.URNO);    
                            printERPPXT(&rlERPPXT);
                            if ( ilRC==RC_SUCCESS )
                            {
                                if (rlERPPXT.iFound==1)
                                    ilRC=updateERPPXT(&rlERPPXT);
                                else
                                    ilRC=insertERPPXT(&rlERPPXT);
                            }
                            dbg(TRACE, "doAllERPBILLTables:: For Departure ERPPXT Result[%d]", ilRC);
                        }
                    }
                }
            
                if ( ilRC==RC_SUCCESS )
                {
                    if ( rlAftArr.iFound )
                    {
                        ilRC= populateERPPXR(&rlERPPXR, &rlAftArr, rlERPPXHArr.URNO);    
                        printERPPXR(&rlERPPXR);
                        if ( ilRC==RC_SUCCESS )
                        {
                        	if ( !strcmp(rlERPBill.STOP, "Transit") )
                        	{
                                if (rlERPPXR.iFound==1)
                                    ilRC=updateERPPXR(&rlERPPXR);
                                else
                                    ilRC=insertERPPXR(&rlERPPXR);
                            }
                        }
                        dbg(TRACE, "doAllERPBILLTables:: For Arrival ERPPXR Result[%d]", ilRC);
                    }
                }
            
                if ( ilRC==RC_SUCCESS )
                {
                    if ( rlAftArr.iFound==1 )
                    {
                        ilRC= populateERPPXM(&rlERPPXM, &rlAftArr, rlERPPXHArr.URNO);    
                        printERPPXM(&rlERPPXM);
                        if ( ilRC==RC_SUCCESS )
                        {
                            if (rlERPPXM.iFound==1)
                                ilRC=updateERPPXM(&rlERPPXM);
                            else
                                ilRC=insertERPPXM(&rlERPPXM);
                        }
                        dbg(TRACE, "doAllERPBILLTables:: For Arrival ERPPXM Result[%d]", ilRC);
                    }
                    
                    if ( ilRC==RC_SUCCESS )
                    {
                        if ( rlAftDep.iFound==1 )
                        {
                            ilRC= populateERPPXM(&rlERPPXM, &rlAftDep, rlERPPXHDep.URNO);    
                            printERPPXM(&rlERPPXM);
                            if ( ilRC==RC_SUCCESS )
                            {
                                if (rlERPPXM.iFound==1)
                                    ilRC=updateERPPXM(&rlERPPXM);
                                else
                                    ilRC=insertERPPXM(&rlERPPXM);
                            }
                            dbg(TRACE, "doAllERPBILLTables:: For Departure ERPPXM Result[%d]", ilRC);
                        }
                    }
                }
                
                if ( ilRC==RC_SUCCESS )
                {
                	ilRC=populateERPOFF(rlERPOFF, &rlAftArr, rlERPPXHArr.URNO, &rlAftDep, rlERPPXHDep.URNO);   
                	printERPOFF(rlERPOFF);
                	if ( ilRC==RC_SUCCESS )
                	{
                	    ilRC=insertERPOFF(rlERPOFF);	
                	}
                }
                
                if ( ilRC==RC_SUCCESS )
                {
                    if ( ilRC==RC_SUCCESS )
                    {
                        if ( rlAftDep.iFound==1 )
                        {
                            ilRC= populateERPPXO(&rlERPPXO, &rlAftDep, rlERPPXHDep.URNO);    
                            printERPPXO(&rlERPPXO);
                            if ( ilRC==RC_SUCCESS )
                            {
                                if (rlERPPXO.iFound==1)
                                    ilRC=updateERPPXO(&rlERPPXO);
                                else
                                    ilRC=insertERPPXO(&rlERPPXO);
                            }
                            dbg(TRACE, "doAllERPBILLTables:: For Departure ERPPXO Result[%d]", ilRC);
                        }
                    }
                }
            
                if ( ilRC==RC_SUCCESS )
                {
                    ilRC=UpdErpBillStat(rlERPBill.URNO);
                }
                
                if ( ilRC==RC_SUCCESS )
                {
                    if ( rlAftArr.iFound==1 )
                    {
                        dbg(TRACE, "doAllERPBILLTables::  Send to Flight. Update STAT of Arrival[%s] to dqcs='S' ",
                            rlAftArr.URNO);     
                        sprintf (pcgAftSel,"WHERE URNO = '%s'",rlAftArr.URNO);
                        ilRC=sendCedaEventWithLog(igFlightQueue,0,pcgAftDestName,pcgAftRecvName,
                                                  pcgAftTwStart,pcgAftTwEnd,"DQF","AFTTAB",pcgAftSel,
                                                  "DQCS","S","",4,NETOUT_NO_ACK); 
                    }
                }
            
                if ( ilRC==RC_SUCCESS )
                {
                	if ( rlAftDep.iFound==1 )
                	{
                        dbg(TRACE, "doAllERPBILLTables::  Send to Flight. Update STAT of Departure[%s] to dqcs='S' ",
                            rlAftDep.URNO);     
                        sprintf (pcgAftSel,"WHERE URNO = '%s'",rlAftDep.URNO);
                        ilRC=sendCedaEventWithLog(igFlightQueue,0,pcgAftDestName,pcgAftRecvName,
                                         pcgAftTwStart,pcgAftTwEnd,"DQF","AFTTAB",pcgAftSel,
                                         "DQCS","S","",4,NETOUT_NO_ACK); 
                    }
                }
            
                if ( ilRC==RC_SUCCESS )
                {
                    if ( ipSendToBroker )
                    {
                        dbg(TRACE, "doAllERPBILLTables:: Sending to WMERPB");
                        ilRC = sendCedaEventWithLog(igWMERPBQueue,0,pcgAftDestName,pcgAftRecvName,
                                                    pcgAftTwStart,pcgAftTwEnd,"BILL","ERPBILL","",
                                                    "URNO", rlERPBill.URNO,"",4,NETOUT_NO_ACK);  
                    }
                }
            }
        }
    }
            
    if ( rgLoatabArr!=NULL )
    {
        free(rgLoatabArr);
        rgLoatabArr=NULL;
        igLoatabArrCount=0;	
    }                 
 
    if ( rgLoatabDep!=NULL )
    {
        free(rgLoatabDep);
        rgLoatabDep=NULL;
        igLoatabDepCount=0;	
    }                 
    dbg(TRACE, "doAllERPBILLTables:: End[%d]", ilRC);    
    return ilRC;	
}

int getLoadConfig()
{
    int ilRC=RC_SUCCESS;
    FILE *fptr;
    char pclLine[8192];
    char pclTmp[8192];
    char *pclPtr;
    char *pclPtr2;
    char *pclPtrHeader;
    char *pclPtrFooter;
    _LOADCONFIG *rlTmp;
    int a=0;
    int b=0;
    int ilSize=0;
        
    dbg(TRACE, "getLoadConfig:: Start");
    
    rgLoadConfig=NULL;
    igLoadConfigCount=0;
    fptr=fopen(pcgLoadFile, "r");
    if ( fptr==NULL )
    {
        dbg(TRACE, "getLoadConfig:: Critical Error!!! File[%s] not found!", pcgLoadFile);
        sleep(5);
        ilRC=RC_FAIL;	
    }
    else
    {
    	while ( fgets(pclLine, 8191, fptr) )
    	{
    	    pclLine[strlen(pclLine)-1]=0;
    	    dbg(DEBUG, "getLoadConfig:: processing[%s]", pclLine);
    	    if ( pclLine[0]=='#' )
    	        continue;
    	    else
    	    {
    	        strcpy(pclTmp, pclLine);
    	        pclPtrHeader=strtok_r(pclTmp, "=", &pclPtr);
    	        if ( pclPtrHeader )	
    	            trim(pclPtrHeader, ' ');
   	            pclPtrFooter=strtok_r(NULL, "=", &pclPtr);
   	            if ( pclPtrFooter )
   	                trim(pclPtrFooter, ' ');
 
  	            if ( pclPtrHeader && strcmp(pclPtrHeader, "") )
   	            {
   	                if ( igLoadConfigCount==0 )
   	                {
   	                    rgLoadConfig=malloc(sizeof(_LOADCONFIG));
   	                    if ( rgLoadConfig==NULL )
   	                    {
   	                        dbg(TRACE, "getLoadConfig:: ERROR!!! Cannot Allocate Memory for rgLoadConfig");
   	                        break;	
   	                    }	
   	                }
   	                else
   	                {
   	                    rlTmp=(_LOADCONFIG *)realloc(rgLoadConfig, sizeof(_LOADCONFIG)*(igLoadConfigCount+1));
                        if ( rlTmp )
                        {
                            rgLoadConfig=rlTmp;
                            memset(&rgLoadConfig[igLoadConfigCount], 0, sizeof(_LOADCONFIG));
                        }
                        else
                        {
                            dbg(TRACE, "getLoadConfig:: ERROR!!! Cannot ReAllocate Memory for rgLoadConfig");
   	                        break;	
                        }
   	                }  
   	                
   	                strcpy(rgLoadConfig[igLoadConfigCount].NAME,  pclPtrHeader);
   	                strcpy(rgLoadConfig[igLoadConfigCount].FORMULA,  pclLine);

   	                strcpy(pclTmp, pclPtrFooter);
   	                ilSize=strlen(pclTmp);
   	                
   	                for (a=0; a<ilSize; a++)    
                    {
                        if (pclTmp[a]==',')
                        {
                            a++;
                            break;	
                        }                        	
                    }
                    
                    b=0;
                    for (; a<ilSize; a++)
                    {
                    	if ( pclTmp[a]==']' )
                    	    break;
                        rgLoadConfig[igLoadConfigCount].DSSN[b++]=pclTmp[a];    	
                    }
                    if ( rgLoadConfig[igLoadConfigCount].DSSN[strlen(rgLoadConfig[igLoadConfigCount].DSSN)-1]==',' )
                        rgLoadConfig[igLoadConfigCount].DSSN[strlen(rgLoadConfig[igLoadConfigCount].DSSN)-1]=0;
 
                    pclPtr=&pclTmp[a];
                    if ( pclPtr )
                    {
                        while ( pclPtr )
                        {
                            if ( pclPtr[0]=='[' )
                                break;
                            pclPtr++;
                        }
                     
                        pclPtr++;     
                        while ( pclPtr )
                        {
                	        if ( pclPtr[0]!=' ' )
                                break;
                            pclPtr++;
                        }  
                
                        strcpy(pclTmp, pclPtr);
                        pclPtr=strtok_r(pclTmp, ",",&pclPtr2);
                        if ( pclPtr )
                        {
                		    if ( !strcmp(pclPtr, "#") )
                			    strcpy(rgLoadConfig[igLoadConfigCount].TYPE, " ");
                		    else
                                strcpy(rgLoadConfig[igLoadConfigCount].TYPE, pclPtr);
                        }
                        
                        pclPtr=strtok_r(NULL, ",",&pclPtr2);
                        if ( pclPtr )
                        {
                  		    if ( !strcmp(pclPtr, "#") )
                		        strcpy(rgLoadConfig[igLoadConfigCount].STYP, " ");
                		    else
                                strcpy(rgLoadConfig[igLoadConfigCount].STYP, pclPtr);
                        }
                        
                        pclPtr=strtok_r(NULL, ",",&pclPtr2);
                        if ( pclPtr )
                        {
                	        if ( !strcmp(pclPtr, "#") )
                			    strcpy(rgLoadConfig[igLoadConfigCount].SSTP, " ");
                		    else
                                strcpy(rgLoadConfig[igLoadConfigCount].SSTP, pclPtr);                     
                        } 
                         
                        pclPtr=strtok_r(NULL, ",",&pclPtr2);
                        if ( pclPtr2 )
                        {
                	        strcpy(rgLoadConfig[igLoadConfigCount].SSST, pclPtr);
                            if ( rgLoadConfig[igLoadConfigCount].SSST[strlen(rgLoadConfig[igLoadConfigCount].SSST)-1] == ']' )
                                rgLoadConfig[igLoadConfigCount].SSST[strlen(rgLoadConfig[igLoadConfigCount].SSST)-1]=0;
                            if ( !strcmp(rgLoadConfig[igLoadConfigCount].SSST, "#") )
                          	    strcpy(rgLoadConfig[igLoadConfigCount].SSST, " ");
                        }

                        pclPtr=strtok_r(NULL, ",",&pclPtr2);
                        if ( pclPtr )
                        {
                	        if ( pclPtr[strlen(pclPtr)-1] == ']' )
                                pclPtr[strlen(pclPtr)-1]=0;
                	        
                	        if ( !strcmp(pclPtr, "-") )
                	             strcpy(rgLoadConfig[igLoadConfigCount].APC3, cgHopo);
                         	else if ( !strcmp(pclPtr, "#") )
                	            strcpy(rgLoadConfig[igLoadConfigCount].APC3, " ");
                            else
                            	strcpy(rgLoadConfig[igLoadConfigCount].APC3,pclPtr);
                        } 
                    }
                          	
                    igLoadConfigCount++;
   	            }
    	    }	
    	}
        fclose(fptr);	
    }
    dbg(TRACE, "getLoadConfig:: End Result[%d]", ilRC);
    
    return ilRC;	
}

void printLoadConfig()
{
	int a=0;
	
    dbg(TRACE, "printLoadConfig:: Start");
    for (a=0; a<igLoadConfigCount; a++)
    {
        dbg(TRACE, "printLoadConfig:: Record[%d/%d]", a+1, igLoadConfigCount);
        dbg(TRACE, "printLoadConfig::     NAME[%s]", rgLoadConfig[a].NAME);
        dbg(TRACE, "printLoadConfig::     STR[%s]", rgLoadConfig[a].FORMULA);
        dbg(TRACE, "printLoadConfig::     TYPE[%s], STYP[%s], SSTP[%s], SSST[%s], APC3[%s]", 
            rgLoadConfig[a].TYPE, rgLoadConfig[a].STYP, rgLoadConfig[a].SSTP, rgLoadConfig[a].SSST, rgLoadConfig[a].APC3);
        dbg(TRACE, "printLoadConfig::     DSSN[%s]", rgLoadConfig[a].DSSN);            	
    }
    dbg(TRACE, "printLoadConfig:: End");
    return;    	
}

int getLoadConfigIndex(char *pcpName)
{
	int ilRC=RC_FAIL;
	int a=0;
	
    dbg(DEBUG, "getLoadConfigIndex:: Start[%s]", pcpName);
    
    for (a=0; a<igLoadConfigCount; a++)
    {
        if ( !strcmp(rgLoadConfig[a].NAME, pcpName) )
        {
        	ilRC=a;
            break;
        }	
    }
    dbg(DEBUG, "getLoadConfigIndex:: End Result Index[%d]", ilRC);
    return ilRC;
}

void printLoatab(_LOATAB *rl, int ipSize)
{
	int a=0;
    dbg(DEBUG, "printLoatab:: Start");
    dbg(DEBUG, "printLoatab:: Total Record[%d]", ipSize);

    for (a=0; a<ipSize; a++)
    {
	    dbg(DEBUG, "printLoatab:: Record[%03d/%03d]   URNO[%s], FLNU[%s], TYPE[%s], STYP[%s], SSTP[%s], SSST[%s], APC3[%s], DSSN[%s], VALU[%s]",
            a+1, ipSize, rl[a].URNO, rl[a].FLNU, rl[a].TYPE, rl[a].STYP, rl[a].SSTP, rl[a].SSST, rl[a].APC3, rl[a].DSSN, rl[a].VALU);	
    }
    dbg(DEBUG, "printLoatab:: End");
    return;    	
}

_LOATAB *allocateLoatabRecords(char *pcpFlnu, int *ipCount)
{
	_LOATAB *rl=NULL;
	int ilRC=RC_SUCCESS;
	char pclSql[1024];
	char pclData[1024];
	short slCursor=0;
	
    dbg(TRACE, "allocateLoatabRecords:: Start");
    
    sprintf(pclSql, "SELECT COUNT(1) FROM LOATAB WHERE FLNU='%s'", pcpFlnu);
    ilRC=sql_if(START,&slCursor, pclSql, pclData);  
    dbg(TRACE, "allocateLoatabRecords:: executing [%s][%d]", pclSql, ilRC);
    if ( ilRC==RC_FAIL )
    {
        dbg(TRACE, "allocateLoatabRecords:: DB ERROR!!!");
    }
    else 
    {
        dbg(TRACE, "allocateLoatabRecords:: Total LOATAB Record[%s]", pclData);	
        *ipCount=atoi(pclData);
        if ( *ipCount>0 )
        {
             rl=(_LOATAB *)malloc((sizeof(_LOATAB)*(*ipCount)));	
             if ( rl==NULL )
             {
                 dbg(TRACE, "allocateLoatabRecords:: Cannot allocate LOATAB");
                 ilRC=RC_FAIL;	
             }
             memset(rl, 0, sizeof(_LOATAB)*(*ipCount));
        }
    } 
    close_my_cursor(&slCursor);
        
    dbg(TRACE, "allocateLoatabRecords:: End[%d]", rl);
    return rl; 	
}

int getLoatabRecords(char *pcpFlnu, _LOATAB *rpLoa, int ipCount)
{
    int ilRC=RC_SUCCESS;
    int ilTmp=RC_SUCCESS;
    int ilCount=0;
    char pclSql[2048];
    char pclData[8192];
    short slCursor=0;

    
    dbg(TRACE, "getLoatabRecords:: Start");
    dbg(TRACE, "getLoatabRecords:: FLNU[%s]", pcpFlnu);

    sprintf(pclSql, "SELECT URNO, FLNU, TYPE, STYP, SSTP, SSST, APC3, DSSN, VALU FROM LOATAB WHERE FLNU='%s'", pcpFlnu);
 	ilRC = sql_if(START,&slCursor, pclSql ,pclData);
 	if ( ilRC==RC_SUCCESS )
 	{
	    while ( ilTmp==DB_SUCCESS )
	    {
	    	if ( ilCount<ipCount )
	    	{
		        BuildItemBuffer(pclData, "URNO,FLNU,TYPE,STYP,SSTP,SSST,APC3,DSSN,VALU", 9, ",");
		        get_real_item(rpLoa[ilCount].URNO,pclData,1); 
		        get_real_item(rpLoa[ilCount].FLNU,pclData,2); 
		
		        get_real_item(rpLoa[ilCount].TYPE,pclData,3); 
		        if ( !strcmp(rpLoa[ilCount].TYPE, "") )
		        strcpy(rpLoa[ilCount].TYPE, " ");

		        get_real_item(rpLoa[ilCount].STYP,pclData,4); 
		        if ( !strcmp(rpLoa[ilCount].STYP, "") )
		            strcpy(rpLoa[ilCount].STYP, " ");

		        get_real_item(rpLoa[ilCount].SSTP,pclData,5); 
		        if ( !strcmp(rpLoa[ilCount].SSTP, "") )
		            strcpy(rpLoa[ilCount].SSTP, " ");

	            get_real_item(rpLoa[ilCount].SSST,pclData,6); 
		        if ( !strcmp(rpLoa[ilCount].SSST, "") )
		            strcpy(rpLoa[ilCount].SSST, " ");

		        get_real_item(rpLoa[ilCount].APC3,pclData,7); 
		        if ( !strcmp(rpLoa[ilCount].APC3, "") )
		            strcpy(rpLoa[ilCount].APC3, " ");

		        get_real_item(rpLoa[ilCount].DSSN,pclData,8); 
		        if ( !strcmp(rpLoa[ilCount].DSSN, "") )
		            strcpy(rpLoa[ilCount].DSSN, " ");

		        get_real_item(rpLoa[ilCount].VALU,pclData,9); 
		        if ( !strcmp(rpLoa[ilCount].VALU, "") )
		            strcpy(rpLoa[ilCount].VALU, " ");
	        
                 ilCount++;
		    }
		    else
		    {
		        dbg(TRACE, "getLoatabRecords:: Allocation error. ");
		        ilRC=RC_FAIL;
		        break;	
		    }
		    ilTmp = sql_if(NEXT,&slCursor,pclSql,pclData);	
		}
    }
    close_my_cursor(&slCursor); 
    dbg(TRACE, "getLoatabRecords:: End Result[%d]", ilRC);
    return ilRC;	
}

int doERPDLY(char *pcpFlnu, char *pcpBillNo)
{
    int ilRC=RC_SUCCESS;
    int ilTmp=RC_SUCCESS;
    char pclSql[1024];
    char pclSqlData[2048];
    char pclErr[513];
    char pclFields[1024];
    char pclValues[1024];
    char pclData[32768];
    char pclFurn[16];
    char pclDeca[8];
    char pclDecn[8];
    char pclMean[128];
    char pclDura[8];
    char pclDCFTABFields[]="FURN,DECA,DECN,MEAN,DURA";
    char pclDCFTABCount=5;
    short slCursor=0;
    int ilUrno=0;
    
    dbg(TRACE, "doERPDLY:: Start ");
    
    dbg(TRACE, "doERPDLY:: pcpFlnu[%s], pcpBillNo[%s]", pcpFlnu, pcpBillNo);
    memset(&pclData, 0, sizeof(pclData));
    
    sprintf(pclSql, "SELECT %s FROM DCFTAB WHERE FURN='%s' ", pclDCFTABFields, pcpFlnu);
    ilRC = sql_if (START, &slCursor, pclSql, pclSqlData);
    if ( ilRC==RC_FAIL )
    {
        dbg(TRACE, "doERPDLY:: DB ERROR!");	
    }
    else if ( ilRC==ORA_NOT_FOUND )
    {
        dbg(TRACE, "doERPDLY:: No record found!");		
        ilRC=RC_SUCCESS;
    }
    else
    {
        while( ilTmp == DB_SUCCESS )
        {
            BuildItemBuffer(pclSqlData, pclDCFTABFields, pclDCFTABCount, ",");
            get_real_item(pclFurn,pclSqlData,1);
            get_real_item(pclDeca,pclSqlData,2);
            get_real_item(pclDecn,pclSqlData,3);
            get_real_item(pclMean,pclSqlData,4);
            get_real_item(pclDura,pclSqlData,5);
            dbg(TRACE, "doERPDLY:: FURN[%s], pclDeca[%s], pclDecn[%s], pclMean[%s], pclDura[%s]", 
                pclFurn, pclDeca, pclDecn, pclMean, pclDura);	
            
            ilUrno=NewUrnos("ERPDLY",1);
            sprintf(pclData, "%s%d,%s", 
                    pclData, ilUrno, pcpBillNo);            	
             
            if ( !strcmp(pclDecn, "") )
                sprintf(pclData, "%s,%s", pclData,pclDeca);
            else
                sprintf(pclData, "%s,%s", pclData,pclDecn);
             
            if ( !strcmp(pclDura, "") )
            {
                sprintf(pclData, "%s,%s,%s\n", pclData,pclMean,"0");	
            }
            else
            {
            	sprintf(pclDura, "%d", convertTimeToSec(pclDura));
            	sprintf(pclData, "%s,%s,%s\n", pclData,pclMean,pclDura);	
            }
            ilTmp=sql_if (NEXT, &slCursor, pclSql, pclSqlData);
        }    
        
        if ( ilRC==RC_SUCCESS )
        {
    	    sprintf(pclFields, "URNO,BILL,CODE,DES1,TIME");
    	    strcpy(pclValues,  ":VURNO,:VBILL,:VCODE,:VDES1,:VTIME");
    	
    	    ilRC=sqlArrayInsert("ERPDLY", pclFields, pclValues, pclData);
        }        	
    }
    close_my_cursor(&slCursor);



    dbg(TRACE, "doERPDLY:: End Result[%d] ", ilRC);
    return ilRC;	
}

int sqlArrayInsert(char *pcpTableName, char *pcpField, char *pcpValue, char *pcpData)       
{
    int ilRC = RC_SUCCESS;
    char pclSql[2048];
    short slFkt = START;
    short slCursor = 0;

    dbg(TRACE, "sqlArrayInsert:: Start");
   
    dbg(DEBUG, "sqlArrayInsert:: Table[%s], Field[%s], Value[%s]",
        pcpTableName, pcpField, pcpValue);
    dbg(DEBUG, "sqlArrayInsert:: pcpData[%s]", pcpData);
    sprintf(pclSql,"INSERT INTO %s (%s) VALUES (%s)", pcpTableName, pcpField, pcpValue);
    
    ilRC = SqlIfArray(slFkt, &slCursor,pclSql, pcpData, 0, &rgRecDesc);
    dbg(TRACE, "sqlArrayInsert:: executing [%s][%d]", pclSql, ilRC);
    if (ilRC == DB_SUCCESS)
        commit_work();
    else
        rollback();
    close_my_cursor(&slCursor);
    
    dbg(TRACE, "sqlArrayInsert:: End Result[%d]", ilRC);
    return ilRC;
}

int deleteAllERPBILLTables(char *pcpArrFlnu, char *pcpDepFlnu, char *pcpBillNo, char *pcpERPPXHUrnoArr, char *pcpERPPXHUrnoDep)
{
    int ilRC=RC_SUCCESS;
    ERPBILLREC rlERPBILL;
    char pclSql[1024];
    char pclErr[513];
    
    dbg(TRACE, "deleteAllERPBILLTables:: START");
    dbg(TRACE, "deleteAllERPBILLTables:: pcpArrFlnu[%s], pcpDepFlnu[%s], pcpBillNo[%s]", pcpArrFlnu, pcpDepFlnu, pcpBillNo);
    
     
    sprintf(pclSql, "DELETE FROM ERPBILL WHERE FLNU IN ('%s', '%s') AND STAT='E' ", 
            pcpArrFlnu, pcpDepFlnu);
    ilRC=executeSql(pclSql,pclErr);  
    
        
    if ( ilRC==RC_SUCCESS )
    {
        sprintf(pclSql, "DELETE FROM ERPDLY WHERE BILL='%s' ", pcpBillNo);
        ilRC=executeSql(pclSql,pclErr);    
        if ( ilRC==ORA_NOT_FOUND )
            ilRC=RC_SUCCESS;	
        
        if ( ilRC==RC_SUCCESS )
        {
            sprintf(pclSql, "DELETE FROM ERPATC WHERE FLNU IN ('%s') ",
                    pcpDepFlnu);
            ilRC=executeSql(pclSql,pclErr);    
            if ( ilRC==ORA_NOT_FOUND )
                ilRC=RC_SUCCESS;	
        }

        if ( ilRC==RC_SUCCESS )
        {
            sprintf(pclSql, "DELETE FROM ERPPXH WHERE FLNU IN ('%s', '%s') ",
                    pcpArrFlnu, pcpDepFlnu);
            ilRC=executeSql(pclSql,pclErr);    
            if ( ilRC==ORA_NOT_FOUND )
                ilRC=RC_SUCCESS;	
        }

        if ( ilRC==RC_SUCCESS )
        {
            sprintf(pclSql, "DELETE FROM ERPPXT WHERE FLNU IN ('%s', '%s') ",
                    pcpArrFlnu, pcpDepFlnu);
            ilRC=executeSql(pclSql,pclErr);    
            if ( ilRC==ORA_NOT_FOUND )
                ilRC=RC_SUCCESS;	
        }

        if ( ilRC==RC_SUCCESS )
        {
            sprintf(pclSql, "DELETE FROM ERPPXR WHERE FLNU IN ('%s', '%s') ",
                    pcpArrFlnu, pcpDepFlnu);
            ilRC=executeSql(pclSql,pclErr);    
            if ( ilRC==ORA_NOT_FOUND )
                ilRC=RC_SUCCESS;	
        }

        if ( ilRC==RC_SUCCESS )
        {
            sprintf(pclSql, "DELETE FROM ERPPXM WHERE FLNU IN ('%s', '%s') ",
                    pcpArrFlnu, pcpDepFlnu);
            ilRC=executeSql(pclSql,pclErr);    
            if ( ilRC==ORA_NOT_FOUND )
                ilRC=RC_SUCCESS;	
        }
        
        if ( ilRC==RC_SUCCESS )
        {
            sprintf(pclSql, "DELETE FROM ERPOFF WHERE PAXH IN ('%s', '%s') ",
                    pcpERPPXHUrnoArr, pcpERPPXHUrnoDep);
            ilRC=executeSql(pclSql,pclErr);    
            if ( ilRC==ORA_NOT_FOUND )
                ilRC=RC_SUCCESS;	
        }

        if ( ilRC==RC_SUCCESS )
        {
            sprintf(pclSql, "DELETE FROM ERPPXO WHERE FLNU IN ('%s', '%s') ",
                    pcpArrFlnu, pcpDepFlnu);
            ilRC=executeSql(pclSql,pclErr);    
            if ( ilRC==ORA_NOT_FOUND )
                ilRC=RC_SUCCESS;	
        }
    }
    else if ( ilRC==ORA_NOT_FOUND )
    {
        dbg(TRACE, "deleteAllERPBILLTables:: No Record Found");
        ilRC=RC_SUCCESS;	
    }

    dbg(TRACE, "deleteAllERPBILLTables:: END Result[%d]", ilRC);    
    return ilRC;	
}

int sendCedaEventWithLog(int  ipRouteId, /* adress to send the answer   */
                         int  ipOrgId,       /* Set to mod_id if < 1        */
                         char *pcpDstNam,    /* BC_HEAD.dest_name           */
                         char *pcpRcvNam,    /* BC_HEAD.recv_name           */
                         char *pcpStart,     /* CMDBLK.tw_start             */
                         char *pcpEnd,       /* CMDBLK.tw_end               */
                         char *pcpCommand,   /* CMDBLK.command              */
                         char *pcpTabName,   /* CMDBLK.obj_name             */
                         char *pcpSelection, /* Selection Block             */
                         char *pcpFields,    /* Field Block                 */
                         char *pcpData,      /* Data Block                  */
                         char *pcpErrDscr,   /* Error description           */
                         int  ipPrio,        /* 0 or Priority               */
                         int  ipRetCode)  
{
    int ilRC=RC_SUCCESS;
    
    dbg(TRACE, "sendCedaEventWithLog:: Start");
    
    dbg(TRACE, "sendCedaEventWithLog:: ipRouteId[%d]", ipRouteId);
    dbg(TRACE, "sendCedaEventWithLog:: ipOrgId[%d]", ipOrgId);
    dbg(TRACE, "sendCedaEventWithLog:: pcpDstNam[%s]", pcpDstNam);
    dbg(TRACE, "sendCedaEventWithLog:: pcpRcvNam[%s]", pcpRcvNam);
    dbg(TRACE, "sendCedaEventWithLog:: pcpStart[%s]", pcpStart);
    dbg(TRACE, "sendCedaEventWithLog:: pcpEnd[%s]", pcpEnd);
    dbg(TRACE, "sendCedaEventWithLog:: pcpCommand[%s]", pcpCommand);
    dbg(TRACE, "sendCedaEventWithLog:: pcpTabName[%s]", pcpTabName);
    dbg(TRACE, "sendCedaEventWithLog:: pcpSelection[%s]", pcpSelection);
    dbg(TRACE, "sendCedaEventWithLog:: pcpFields[%s]", pcpFields);
    dbg(TRACE, "sendCedaEventWithLog:: pcpData[%s]", pcpData);
    dbg(TRACE, "sendCedaEventWithLog:: pcpErrDscr[%s]", pcpErrDscr);
    dbg(TRACE, "sendCedaEventWithLog:: ipPrio[%d]", ipPrio);
    dbg(TRACE, "sendCedaEventWithLog:: ipRetCode[%d]", ipRetCode);

    ilRC = SendCedaEvent (ipRouteId, 
                          ipOrgId,
                          pcpDstNam,
                          pcpRcvNam,
                          pcpStart, 
                          pcpEnd, 
                          pcpCommand,
                          pcpTabName, 
                          pcpSelection,
                          pcpFields,
                          pcpData,
                          pcpErrDscr,
                          ipPrio,
                          ipRetCode);

    dbg(TRACE, "sendCedaEventWithLog:: End [%d]", ilRC);
    return ilRC;
}

static int UpdErpBillStat(char *pclUrno)
{
    char *pclFunc = "UpdErpBillStat";
    int ilRc = RC_SUCCESS;
    char pclTabName[16] = "ERPBILL";
    char pclSqlBufUpd[8192] = "\0";
    
    if(strlen(pclUrno)==0 || strncmp(pclUrno," ",1)==0 || atoi(pclUrno)==0)
    {
        dbg(TRACE,"%s pclFlnu<%s> is invalid",pclFunc,pclUrno);
        return RC_FAIL;
    }

    sprintf( pclSqlBufUpd,"UPDATE %s SET STAT='R' WHERE URNO='%s'",pclTabName,pclUrno);
    dbg(TRACE,"%s pclSqlBufUpd<%s>",pclFunc,pclSqlBufUpd);
  
  ilRc = UpdateTab(pclSqlBufUpd,pclTabName);
  if(ilRc == ORA_NOT_FOUND)
  {
  
      dbg(TRACE,"Not Found<%s>",pclFunc,pclSqlBufUpd);
      ilRc=RC_FAIL;
  }
  else if(ilRc == DB_SUCCESS)
  {
      dbg(TRACE,"%s Successful<%s>",pclFunc,pclSqlBufUpd);
  }
  else
  {
      dbg(TRACE,"Fail<%s>",pclFunc,pclSqlBufUpd);
  }
  
  return ilRc;
}

static int CountRrdInLoatab(char *pclUrno,int *pilCount)
{
    char *pclFunc = "CountRrdInLoatab";
    int ilRc = RC_SUCCESS;
    
    char pclSqlBuf[1024] = "\0";
    char pclSqlData[1024] = "\0"; 
    short slSqlFunc = 0;    
  char pclDssnRange[1024] = "\0";
  
  *pilCount = 0;
  
  if(strlen(pclUrno)==0 || strncmp(pclUrno," ",1)==0)
  {
      dbg(TRACE,"%s pclUrno<%s> is invaild",pclFunc,pclUrno);
      return RC_FAIL;
  }
  
  sprintf(pclDssnRange,"%s",pcgAllDssn);
  dbg(TRACE,"%s pcgAllDssn<%s>,pclDssnRange<%s>",pclFunc,pcgAllDssn,pclDssnRange);
  
  sprintf(pclSqlBuf,"SELECT COUNT(*) FROM LOATAB WHERE FLNU='%s' AND DSSN IN(%s)",pclUrno,pclDssnRange);
  dbg(TRACE,"%s pclSqlBuf<%s>",pclFunc,pclSqlBuf);
  
  ilRc = RunSQL(pclSqlBuf,pclSqlData);
    if( ilRc == DB_SUCCESS )
    {
        dbg(TRACE,"%s Executes successfully<%s>",pclFunc,pclSqlBuf);
        dbg(TRACE,"%s The number of records<%d> When FLNU='%s' AND DSSN IN(%s)",pclFunc,atoi(pclSqlData),pclUrno,pclDssnRange);
        
            
        *pilCount = atoi(pclSqlData);
        
        return RC_SUCCESS;
    }
    else if(ilRc == ORA_NOT_FOUND)
    {
        dbg(TRACE,"%s RunSQL Not found",pclFunc);
        *pilCount = 0;
        return RC_SUCCESS;
    }
    else
    {
        dbg(TRACE,"%s RunSQL Error",pclFunc);
        return RC_FAIL;
    }
}

static int GetUrno(char *pclUrno,char *pclTabName)
{
    char *pclFunc = "GetUrno";
    int ilRc = RC_SUCCESS;
    int ilNextUrno = 0;
    
    ilNextUrno = NewUrnos(pclTabName,1);
    if(ilNextUrno > 0)
    {
        if(itoa(ilNextUrno,pclUrno,10) == NULL)
        {
            dbg(DEBUG,"%s Error converting new URNO",pclFunc);
            return RC_FAIL;
        }
    }
    else
    {
          dbg(DEBUG,"%s Error getting new URNO",pclFunc);
      return RC_FAIL;
    }
    dbg(TRACE,"%s the URNO for %s<%s>",pclFunc,pclTabName,pclUrno);
    
    return ilRc;
}

static int InsErpBill(AFTREC rlAft,char *pcpFieldList,char *pcpDataList,AFTREC rlAftDep)
{
    char *pclFunc = "InsErpBill";
    int ilRc = RC_SUCCESS;
    char pclTabName[16] = "ERPBILL";
    
    //Insert or Update SqlBuf
    char pclUrno[16] = "\0";
    char pclSqlBufIns[8192] = "\0";

    
    char pclFieldList[8192] = "\0";
    char pclDataList[8192] = "\0";
    
    int flds_count = 0;
    int ilRecordCount = 0;
    
    UINT  ilCurrentSection = 0;
    
    if(strlen(rlAft.URNO)==0 || strncmp(rlAft.URNO," ",1)==0)
    {
        dbg(TRACE,"%s pclFlnu<%s> is invalid",pclFunc,rlAft.URNO);
        return RC_FAIL;
    }
    
    if(strlen(pcpFieldList)==0 || strncmp(pcpFieldList," ",1)==0)
    {
        dbg(TRACE,"%s pcpFieldList<%s> is invalid",pclFunc,pcpFieldList);
        return RC_FAIL;
    }
    
    if(strlen(pcpDataList)==0 || strncmp(pcpDataList," ",1)==0)
    {
        dbg(TRACE,"%s pcpDataList<%s> is invalid",pclFunc,pcpDataList);
        return RC_FAIL;
    }
    
    memset(pclSqlBufIns,0,sizeof(pclSqlBufIns));
    
    GetUrno(pclUrno,pclTabName);
    
    
    for(ilCurrentSection=0; ilCurrentSection < rgTM_Bill.CommCfgPart.iNoOfSections_FltTyp; ilCurrentSection++)
    {
        if(strstr(rgTM_Bill.CommCfgPart.prSection_FltTyp[ilCurrentSection].pcRange,rlAft.TTYP))
        {
            strcpy(rlAft.typeBill,rgTM_Bill.CommCfgPart.prSection_FltTyp[ilCurrentSection].pcMark);
        }
    }
    
    //Frank 20121109 v0.5
    dbg(DEBUG,"rlAft.FLNO<%s>",rlAft.FLNO);
    dbg(DEBUG,"rlAftDep.FLNO<%s>",rlAftDep.FLNO);
    dbg(DEBUG,"rlAft.ORG3<%s>",rlAft.ORG3);
    dbg(DEBUG,"rlAftDep.DES3<%s>",rlAftDep.DES3);
    
    if ( !strcmp(rlAft.FLNO, rlAftDep.FLNO) && strcmp(rlAft.ORG3, rlAftDep.DES3) )
        strcpy(rlAft.stopBill, "Transit");    
    else
        strcpy(rlAft.stopBill, "Turnaround");  
    
    strcpy(pcgBillUrno, pclUrno);    
    //sprintf(pclFieldList,"URNO,FLNU,STAT,ORG3,DES3,REGN,HOPO,ALFN,SCHE,MOVE,ACT3,ALFT,ADAT,DDAT,MMVN,PSTA,LAND,AIRB,ACSZ,STTA,AARR,CATE,UMVN,");
    sprintf(pclFieldList,"URNO,FLNU,STAT,ORG3,DES3,"
                         "REGN,HOPO,ALFN,STOP,SCHE,"
                         "MOVE,ACT3,ALFT,ADAT,DDAT,"
                         "MMVN,PSTA,PSTD,LAND,DFLT,"
                         "AIRB,ACSZ,STTA,STTD,AARR,ADEP,MTOW,CATE,UMVN,");
    sprintf(pclDataList,"%s,%s,'E','%s','%s',"
                        "'%s','%s','%s','%s','%s',"
                        "'%s','%s','%s','%s','%s',"
                        "'%s','%s','%s','%s','%s',"
                        "'%s','%s','%s','%s','%s','%s','%s','%s',%s,",
            pclUrno,rlAft.URNO,rlAft.ORG3,rlAft.DES3,
            rlAft.REGN, rlAft.HOPO,rlAft.alfn,rlAft.stopBill,rlAft.sche,
            rlAft.move,rlAft.ACT3,rlAft.aflt,rlAft.adatBill,rlAft.ddatBill,
            rlAft.mmvnBill,rlAft.PSTA,rlAft.PSTD,rlAft.landBill,rlAftDep.aflt,
            rlAft.airbBill,rlAft.acszBill,rlAft.sttaBill,rlAft.sttdBill/*,rlAft.aarrBill*/,rlAft.aarrBill,rlAft.adepBill,rlAft.mtowBill,rlAft.cateBill,pclUrno);
    
    strcat(pclFieldList,pcpFieldList);
    strcat(pclDataList,pcpDataList);
    
    sprintf( pclSqlBufIns,    "INSERT INTO %s (%s) VALUES (%s)",pclTabName,pclFieldList,pclDataList);
    dbg(TRACE,"%s pclSqlBufIns<%s>",pclFunc,pclSqlBufIns);
    
    ilRc = InsertTab(pclSqlBufIns,pclTabName);
    if(ilRc != DB_SUCCESS)
    {
        dbg(TRACE,"Fail<%s>",pclFunc,pclSqlBufIns);
    }
    else
    {
        dbg(TRACE,"%s Successful<%s>",pclFunc,pclSqlBufIns);
    }
    return ilRc;
}  

static int UpdateTab(char *pclSqlBufUpd,char *pclTabName)
{
    char *pclFunc = "UpdTab";
    int ilRc = RC_SUCCESS;
    short slSqlFunc = 0;
    short slCursor = 0;
    char pclDataArea[8192] = "\0";
    
    slSqlFunc = START;
    slCursor = 0;
    if((ilRc = sql_if(slSqlFunc, &slCursor, pclSqlBufUpd, pclDataArea)) == ORA_NOT_FOUND)
    {
        dbg(TRACE,"%s Updating Not Found %s",pclFunc,pclTabName);
    }
    else if(ilRc == DB_SUCCESS)
    {
        commit_work();
        dbg(TRACE,"%s Updating successful %s",pclFunc,pclTabName);
    }
    else
    {
        dbg(TRACE,"%s Error updating %s",pclFunc,pclTabName);
    }
    close_my_cursor(&slCursor);
    
    return ilRc;
} 

static int InsertTab(char *pclSqlBufIns,char *pclTabName)
{
    char *pclFunc = "InsTab";
    int ilRc = RC_SUCCESS;
    short slSqlFunc = 0;
    short slCursor = 0;
    char pclDataArea[8192] = "\0";
    
    slSqlFunc = START;
    slCursor = 0;
    if((ilRc = sql_if(slSqlFunc,&slCursor,pclSqlBufIns,pclDataArea))==DB_SUCCESS)
    {
        commit_work();
    }
    else
    {
        dbg(TRACE,"%s Error inserting into %s",pclFunc,pclTabName);
   }
    close_my_cursor(&slCursor);
    
    return ilRc;
}

static int FillCfgStructDB_LOAD_flds(char *pcpDB_flds,char *pcpLoad_flds,PaxMain *prpPaxMain,char *pclSectionName)
{
    int ilCount = 0;
    int ili = 0;
    int ilCountWOStar = 0;
  int ilNoOfDB_flds = 0;
    int    ilNoOfLoad_flds = 0;
    int    ilNoOfDB_fldsWStar = 0;
    int ilPlus = 0;
    int    ilRc = RC_SUCCESS;
    int ilArr=1;
    int ilDep=1;
    int ilNormal=1;
    int ilComplex=1;
    char    pclTmpValue_DB[64]="\0";
    char    pclTmpValue_LOAD[64]="\0";
    char    *pclFunc = "FillCfgStructDB_LOAD_flds";
    char    pclSelection[iMAX_BUF_SIZE] = "\0";
    char pclTmp[4096] = "\0";
    char *pclTmpArr = "\0";
    char *pclTmpDep = "\0";
    char *pclTmpComplex = "\0";
    char *pclTmpADComplex = "\0";
    char pclTmpADComplexModify[128] = "\0";
    char pclDssn[1024]="\0";
    char pclType[32]="\0";
    char pclStyp[32]="\0";
    char pclSstp[32]="\0";
    char pclSsst[32]="\0"; 
    char pclApc3[32]="\0";
    
    char pclP[64] ="\0";
    
    if(strlen(pclSectionName) == 0 || strlen(pclSectionName) == 0 )
    {
        dbg(TRACE,"%s pclSectionName<%s> is null",pclFunc,pclSectionName);
        return RC_FAIL;
    }
    else
    {
        dbg(TRACE,"%s pclSectionName is <%s>",pclFunc,pclSectionName);
    }
    
    /*
    if(strlen(pcpDB_flds) == 0 || strlen(pcpLoad_flds) == 0 )
    {
        dbg(TRACE,"%s pcpDB_flds or/and pcpLoad_flds is null",pclFunc);
        return RC_FAIL;
    }
    else
  {*/
    ilNoOfDB_flds = get_flds_count(pcpDB_flds);
    ilNoOfLoad_flds = get_flds_count(pcpLoad_flds);
    //dbg(DEBUG,"%s pcpDB_flds<%s>,ilNoOfDB_flds<%d>,pcpLoad_flds<%s>,ilNoOfLoad_flds<%d>",pclFunc,pcpDB_flds,ilNoOfDB_flds,pcpLoad_flds,ilNoOfLoad_flds);
    
    if(ilNoOfDB_flds < ilNoOfLoad_flds)
    {
        dbg(TRACE,"%s the number of DB_FLDS <the number of LOAD_FLDS,rgTM_PaxHeader.LoadCfg & rgTM_PaxHeader.SpecialLoadCfg are NULL",pclFunc);
        //rgTM_PaxHeader.SLoadCfg = NULL;
        (*prpPaxMain).SLoadCfg = NULL;
        return RC_FAIL;
    }
    else
    {
        
        for(ilCount=1; ilCount<=ilNoOfDB_flds; ilCount++)
        {
            memset(pclTmpValue_DB,0,sizeof(pclTmpValue_DB));
            get_fld_value(pcpDB_flds,ilCount,pclTmpValue_DB);
            
            if(strncmp(pclTmpValue_DB,"*",1)==0)
            {
                ilNoOfDB_fldsWStar++;
            }
        }
        dbg(DEBUG,"%s pcpDB_flds<%s>,ilNoOfDB_flds<%d>,pcpLoad_flds<%s>,ilNoOfLoad_flds<%d>ilNoOfDB_fldsWStar<%d>",pclFunc,pcpDB_flds,ilNoOfDB_flds,pcpLoad_flds,ilNoOfLoad_flds,ilNoOfDB_fldsWStar);
        
        if( (ilNoOfDB_fldsWStar+ilNoOfLoad_flds) !=  ilNoOfDB_flds)
        {
            dbg(TRACE,"%s (ilNoOfDB_fldsWStar+ilNoOfLoad_flds) !=  ilNoOfDB_flds",pclFunc);
            return RC_FAIL;
        }
        else
        {
            (*prpPaxMain).iNoOfDB_FLDS = ilNoOfDB_flds;
            (*prpPaxMain).iNoOfLOAD_FLDS = ilNoOfLoad_flds;
            (*prpPaxMain).iNoOfLOAD_FLDSWStar = ilNoOfDB_fldsWStar;
            // memory for SLoadCfg
            if(((*prpPaxMain).SLoadCfg = (SLoadCfg*)malloc((*prpPaxMain).iNoOfDB_FLDS*sizeof(SLoadCfg))) == NULL)
            {
                    dbg(TRACE,"%s can't run without memory (SLoadCfg)",pclFunc);
                    (*prpPaxMain).SLoadCfg = NULL;
                    return RC_FAIL;
            }
            
            for(ilCount=0; ilCount<(*prpPaxMain).iNoOfDB_FLDS; ilCount++)
            {
                memset(pclTmpValue_DB,0,sizeof(pclTmpValue_DB));
                memset(pclTmpValue_LOAD,0,sizeof(pclTmpValue_LOAD));
                
                get_fld_value(pcpDB_flds,ilCount+1,pclTmpValue_DB);
                //dbg(DEBUG,"%s pclTmpValue_DB<%s>",pclFunc,pclTmpValue_DB);
                strcpy((*prpPaxMain).SLoadCfg[ilCount].pclDbFldsName,pclTmpValue_DB);
                dbg(DEBUG,"%s (*prpPaxMain).SLoadCfg[%d].pclDbFldsName<%s>",pclFunc,ilCount,(*prpPaxMain).SLoadCfg[ilCount].pclDbFldsName);
                
                //*
                if(strncmp(pclTmpValue_DB,"*",1)==0)
                {
                    (*prpPaxMain).SLoadCfg[ilCount].ilADCfg = 1;
                    (*prpPaxMain).SLoadCfg[ilCount].ilMoreThan1LoadCfgFlag = 0;
                    
                    ReadConfigEntry( cgConfigFile,pclSectionName,(*prpPaxMain).SLoadCfg[ilCount].pclDbFldsName,pclTmp);
                    //dbg(DEBUG,"%s pclTmp_Star<%s>",pclFunc,pclTmp);
                    
                    strcpy((*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsName,pclTmp);
                    
                    dbg(DEBUG,"%s (*prpPaxMain).SLoadCfg[%d].pclLoadFldsName<%s>",pclFunc,ilCount,(*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsName);
                    
                    if( strstr((*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsName,"A:")!=0
                     || strstr((*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsName,"D:")!=0 )
                    {
                        if(strstr((*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsName,"+")==0)
                        {
                            //Arrival
                            if((pclTmpArr = GetDataField((*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsName, ARR, '|'))!=NULL)
                            {
                                //dbg(DEBUG,"%s pclTmpArr<%s>",pclFunc,pclTmpArr);
                                    
                                if(strstr(pclTmpArr,"A:")!=0)
                                {
                                    strcpy((*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsNameArr,pclTmpArr+strlen("A:"));
                                    dbg(DEBUG,"%s (*prpPaxMain).SLoadCfg[%d].pclLoadFldsNameArr<%s>",pclFunc,ilCount,(*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsNameArr);
                                    
                                    SetZeroDTSSS(pclDssn,pclType,pclStyp,pclSstp,pclSsst);
                                    
                                    memset(pclP,0,sizeof(pclP));
                                    ilRc = iGetConfigEntryWOSec(cgLoadConfigFile,(*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsNameArr,CFG_STRING,pclP);
                                    //dbg(DEBUG,"pclP<%s>",pclP);
                                    
                                    if(ilRc==OK)
                                    {
                                        strcpy((*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsCriArr,pclP);
                                        
                                        dbg(DEBUG,"(*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsCriArr<%s>",(*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsCriArr);
                                        
                                        ExtractLoaWhereClauseValues((*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsCriArr,pclDssn, pclType,pclStyp, pclSstp, pclSsst,pclApc3);
                                        
                                        FormatDssnFldCfg(pclDssn);
                                        CollectAllDssnWORepeat(pclDssn);
                                        
                                        strcpy((*prpPaxMain).SLoadCfg[ilCount].ConcreteCfgArr.pclDssn, pclDssn);
                                        strcpy((*prpPaxMain).SLoadCfg[ilCount].ConcreteCfgArr.pclType, pclType);
                                        strcpy((*prpPaxMain).SLoadCfg[ilCount].ConcreteCfgArr.pclStyp, pclStyp);
                                        strcpy((*prpPaxMain).SLoadCfg[ilCount].ConcreteCfgArr.pclSstp, pclSstp);
                                        strcpy((*prpPaxMain).SLoadCfg[ilCount].ConcreteCfgArr.pclSsst, pclSsst);
                                        strcpy((*prpPaxMain).SLoadCfg[ilCount].ConcreteCfgArr.pclApc3, pclApc3);
                                        
                                        dbg(DEBUG,"%s +Arr<%d> pclDssn<%s>,pclType<%s>,pclStyp<%s>,pclSstp<%s>,pclSsst<%s>,pclApc3<%s>",pclFunc,ilArr++,(*prpPaxMain).SLoadCfg[ilCount].ConcreteCfgArr.pclDssn,(*prpPaxMain).SLoadCfg[ilCount].ConcreteCfgArr.pclType,(*prpPaxMain).SLoadCfg[ilCount].ConcreteCfgArr.pclStyp,(*prpPaxMain).SLoadCfg[ilCount].ConcreteCfgArr.pclSstp,(*prpPaxMain).SLoadCfg[ilCount].ConcreteCfgArr.pclSsst,(*prpPaxMain).SLoadCfg[ilCount].ConcreteCfgArr.pclApc3);
                                    }
                                    else
                                    {
                                        dbg(TRACE,"%s %s has no corresponding Field Criteria",pclFunc,(*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsNameDep);
                                    }
                                }
                            }
                            else
                            {
                                memset((*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsNameArr,0,sizeof((*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsNameArr));
                                dbg(DEBUG,"%s (*prpPaxMain).SLoadCfg[%d].pclLoadFldsNameArr is null",pclFunc,ilCount);
                            }
                            
                            //Departure
                            if((pclTmpDep = GetDataField((*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsName, DEP, '|'))!=NULL)
                            {
                                //dbg(DEBUG,"%s pclTmpDep<%s>",pclFunc,pclTmpDep);
                                    
                                if(strstr(pclTmpDep,"D:")!=0)
                                {
                                    strcpy((*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsNameDep,pclTmpDep+strlen("D:"));
                                    dbg(DEBUG,"%s (*prpPaxMain).SLoadCfg[%d].pclLoadFldsNameDep<%s>",pclFunc,ilCount,(*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsNameDep);
                                    
                                    SetZeroDTSSS(pclDssn,pclType,pclStyp,pclSstp,pclSsst);
                                    
                                    memset(pclP,0,sizeof(pclP));
                                    ilRc = iGetConfigEntryWOSec(cgLoadConfigFile,(*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsNameDep,CFG_STRING,pclP);
                                    //dbg(DEBUG,"pclP<%s>",pclP);
                                    
                                    if(ilRc==OK)
                                    {
                                        strcpy((*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsCriDep,pclP);
                                        
                                        dbg(DEBUG,"(*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsCriDep<%s>",(*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsCriDep);
                                        
                                        ExtractLoaWhereClauseValues((*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsCriDep,pclDssn, pclType,pclStyp, pclSstp, pclSsst,pclApc3);
                                        
                                        FormatDssnFldCfg(pclDssn);
                                        CollectAllDssnWORepeat(pclDssn);
                                        
                                        strcpy((*prpPaxMain).SLoadCfg[ilCount].ConcreteCfgDep.pclDssn, pclDssn);
                                        strcpy((*prpPaxMain).SLoadCfg[ilCount].ConcreteCfgDep.pclType, pclType);
                                        strcpy((*prpPaxMain).SLoadCfg[ilCount].ConcreteCfgDep.pclStyp, pclStyp);
                                        strcpy((*prpPaxMain).SLoadCfg[ilCount].ConcreteCfgDep.pclSstp, pclSstp);
                                        strcpy((*prpPaxMain).SLoadCfg[ilCount].ConcreteCfgDep.pclSsst, pclSsst);
                                        strcpy((*prpPaxMain).SLoadCfg[ilCount].ConcreteCfgDep.pclApc3, pclApc3);
                                        
                                        dbg(DEBUG,"%s +Dep<%d> pclDssn<%s>,pclType<%s>,pclStyp<%s>,pclSstp<%s>,pclSsst<%s>,pclApc3<%s>",pclFunc,ilDep++,(*prpPaxMain).SLoadCfg[ilCount].ConcreteCfgDep.pclDssn,(*prpPaxMain).SLoadCfg[ilCount].ConcreteCfgDep.pclType,(*prpPaxMain).SLoadCfg[ilCount].ConcreteCfgDep.pclStyp,(*prpPaxMain).SLoadCfg[ilCount].ConcreteCfgDep.pclSstp,(*prpPaxMain).SLoadCfg[ilCount].ConcreteCfgDep.pclSsst,(*prpPaxMain).SLoadCfg[ilCount].ConcreteCfgDep.pclApc3);
                                    }
                                    else
                                    {
                                        dbg(TRACE,"%s %s has no corresponding Field Criteria",pclFunc,(*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsNameDep);
                                    }
                                }
                            }
                            else
                            {
                                memset((*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsNameDep,0,sizeof((*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsNameDep));
                                
                                dbg(DEBUG,"%s (*prpPaxMain).SLoadCfg[%d].pclLoadFldsNameDep is null",pclFunc,ilCount);
                            }
                        }
                        else //if(strstr((*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsName,"+")!=0)
                        {
                            (*prpPaxMain).SLoadCfg[ilCount].ilMoreThan1LoadCfgFlag = 1;
                            
                            //-------------Arrival--------------------
                            if((pclTmpArr = GetDataField((*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsName, ARR, '|'))!=NULL)
                            {
                                dbg(DEBUG,"%s pclTmpArr<%s>",pclFunc,pclTmpArr);
                                
                                if(strstr(pclTmpArr,"A:")!=0)
                                {
                                    strcpy((*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsNameArr,pclTmpArr+strlen("A:"));
                                    dbg(DEBUG,"%s (*prpPaxMain).SLoadCfg[%d].pclLoadFldsNameArr<%s>",pclFunc,ilCount,(*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsNameArr);
                                    
                                    //dbg(DEBUG,"get_flds_count<%d>",get_flds_count((*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsNameArr));
                                    
                                    for(ili=0; ili< (get_flds_count((*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsNameArr)-1); ili++)
                                    {
                                        memset(pclTmpADComplexModify,0,sizeof(pclTmpADComplexModify));
                                        pclTmpADComplex = GetDataField((*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsNameArr,ili, '+');
                                        
                                        if(ili ==0)
                                        {
                                            strcpy(pclTmpADComplexModify,pclTmpADComplex);
                                            pclTmpADComplexModify[strlen(pclTmpADComplexModify)-1] = '\0';
                                        }
                                        else
                                        {
                                            strcpy(pclTmpADComplexModify,pclTmpADComplex+1);
                                        }
                                        
                                        dbg(DEBUG,"%s #Arr pclTmpADComplexModify[%d]<%s>",pclFunc,ili,pclTmpADComplexModify);
                                        
                                        memset(pclP,0,sizeof(pclP));
                                        //ilRc = iGetConfigEntryWOSec(cgLoadConfigFile,(*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsNameArr,CFG_STRING,pclP);
                                        ilRc = iGetConfigEntryWOSec(cgLoadConfigFile,pclTmpADComplexModify,CFG_STRING,pclP);
                                        //dbg(DEBUG,"pclP<%s>",pclP);
                                        
                                        if(ilRc==OK)
                                        {
                                            ExtractLoaWhereClauseValues(pclP,pclDssn, pclType,pclStyp, pclSstp, pclSsst,pclApc3);
                                            
                                            FormatDssnFldCfg(pclDssn);
                                            CollectAllDssnWORepeat(pclDssn);
                                            
                                            dbg(DEBUG,"%s #Arr pclDssn<%s>,pclType<%s>,pclStyp<%s>,pclSstp<%s>,pclSsst<%s>,pclApc3<%s>",pclFunc,pclDssn,pclType,pclStyp,pclSstp,pclSsst,pclApc3);
                                        }
                                        else
                                        {
                                            dbg(TRACE,"%s %s has no corresponding Field Criteria",pclFunc,(*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsNameArr);
                                        }
                                    }
                                }
                                else
                                {
                                    memset((*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsNameArr,0,sizeof((*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsNameArr));
                                    dbg(DEBUG,"%s (*prpPaxMain).SLoadCfg[%d].pclLoadFldsNameArr is null",pclFunc,ilCount);
                                }
                            }
                            
                            //-------------Departure-------------
                            if((pclTmpDep = GetDataField((*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsName, DEP,'|'))!=NULL)
                            {
                                dbg(DEBUG,"%s pclTmpDep<%s>",pclFunc,pclTmpDep);
                                if(strstr(pclTmpDep,"D:")!=0)
                                {
                                    strcpy((*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsNameDep,pclTmpDep+strlen("D:"));
                                    dbg(DEBUG,"%s (*prpPaxMain).SLoadCfg[%d].pclLoadFldsNameDep<%s>",pclFunc,ilCount,(*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsNameDep);
                                    
                                    for(ili=0; ili< (get_flds_count((*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsNameDep)-1); ili++)
                                    {    
                                        memset(pclTmpADComplexModify,0,sizeof(pclTmpADComplexModify));
                                        pclTmpADComplex = GetDataField((*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsNameDep, ili, '+');
                                        
                                        if(ili ==0)
                                        {
                                            strcpy(pclTmpADComplexModify,pclTmpADComplex);
                                            pclTmpADComplexModify[strlen(pclTmpADComplexModify)-1] = '\0';
                                        }
                                        else
                                        {
                                            strcpy(pclTmpADComplexModify,pclTmpADComplex+1);
                                        }
                                    
                                        dbg(DEBUG,"%s #Dep pclTmpADComplexModify[%d]<%s>",pclFunc,ili,pclTmpADComplexModify);
                                        
                                        memset(pclP,0,sizeof(pclP));
                                        ilRc = iGetConfigEntryWOSec(cgLoadConfigFile,pclTmpADComplexModify,CFG_STRING,pclP);
                                        //dbg(DEBUG,"pclP<%s>",pclP);
                                    
                                        if(ilRc==OK)
                                        {
                                            ExtractLoaWhereClauseValues(pclP,pclDssn, pclType,pclStyp, pclSstp, pclSsst,pclApc3);
                                            
                                            FormatDssnFldCfg(pclDssn);
                                            CollectAllDssnWORepeat(pclDssn);
                                            
                                            dbg(DEBUG,"%s #Dep pclDssn<%s>,pclType<%s>,pclStyp<%s>,pclSstp<%s>,pclSsst<%s>,pclApc3<%s>",pclFunc,pclDssn,pclType,pclStyp,pclSstp,pclSsst,pclApc3);
                                        }
                                        else
                                        {
                                            dbg(TRACE,"%s %s has no corresponding Field Criteria",pclFunc,(*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsNameDep);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                memset((*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsNameDep,0,sizeof((*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsNameDep));
                                
                                dbg(DEBUG,"%s (*prpPaxMain).SLoadCfg[%d].pclLoadFldsNameDep is null",pclFunc,ilCount);
                            }
                        }
                    }
                    else
                    {
                        //Complex
                        (*prpPaxMain).SLoadCfg[ilCount].ilADCfg = 1;
                        (*prpPaxMain).SLoadCfg[ilCount].ilMoreThan1LoadCfgFlag = 2;
                        ilPlus = GetNoOfElements((*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsName, '+');
                        //dbg(DEBUG,"ilPlus<%d>",ilPlus);
                        (*prpPaxMain).SLoadCfg[ilCount].ilComplexNo = ilPlus;
                        
                        
                        if(((*prpPaxMain).SLoadCfg[ilCount].pclComplexFldsName = (char*)malloc(ilPlus*32)) == NULL)
                        {
                            dbg(TRACE,"%s can't run without memory (pclComplexFldsName)",pclFunc);
                            (*prpPaxMain).SLoadCfg[ilCount].pclComplexFldsName = NULL;
                            return RC_FAIL;
                        }
                        
                        if(((*prpPaxMain).SLoadCfg[ilCount].pclComplexFldsCri = (char*)malloc(ilPlus*32)) ==NULL)
                        {
                            dbg(TRACE,"%s can't run without memory (pclComplexFldsCri)",pclFunc);
                            (*prpPaxMain).SLoadCfg[ilCount].pclComplexFldsCri = NULL;
                            return RC_FAIL;
                        }
                        
                        if(((*prpPaxMain).SLoadCfg[ilCount].ComplexCfg = (EDTSSSFldCfg*)malloc(ilPlus*sizeof(EDTSSSFldCfg))) == NULL)
                        {
                            dbg(TRACE,"%s can't run without memory (ComplexCfg)",pclFunc);
                            (*prpPaxMain).SLoadCfg[ilCount].ComplexCfg = NULL;
                            return RC_FAIL;
                        }
                        
                        for(ili=0;ili<ilPlus;ili++)
                        {
                            memset((*prpPaxMain).SLoadCfg[ilCount].pclComplexFldsName+ili,0,32);
                            memset((*prpPaxMain).SLoadCfg[ilCount].pclComplexFldsCri+ili,0,32);
                            memset((*prpPaxMain).SLoadCfg[ilCount].ComplexCfg+ili,0,sizeof((*prpPaxMain).SLoadCfg[ilCount].ComplexCfg));
                            
                            if((pclTmpComplex = GetDataField((*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsName, ili, '+')) != NULL)
                            {
                                //dbg(DEBUG,"%s pclTmpComplex<%s>",pclFunc,pclTmpComplex);
                                
                                //Modified
                                if(ili==0)
                                {
                                    strncpy((*prpPaxMain).SLoadCfg[ilCount].pclComplexFldsName+ili,pclTmpComplex+strlen("*:"),(int)(strlen(pclTmpComplex)-3));
                                }
                                else
                                {
                                    strcpy((*prpPaxMain).SLoadCfg[ilCount].pclComplexFldsName+ili,pclTmpComplex+strlen(","));
                                }
                                dbg(DEBUG,"%s (*prpPaxMain).SLoadCfg[%d].pclComplexFldsName+%d<%s>",pclFunc,ilCount,ili,(*prpPaxMain).SLoadCfg[ilCount].pclComplexFldsName+ili);
                                
                                memset(pclP,0,sizeof(pclP));
                                ilRc = iGetConfigEntryWOSec(cgLoadConfigFile,(*prpPaxMain).SLoadCfg[ilCount].pclComplexFldsName+ili,CFG_STRING,pclP);
                                
                                //dbg(DEBUG,"pclP<%s>",pclP);
                                strcpy((*prpPaxMain).SLoadCfg[ilCount].pclComplexFldsCri+ili,pclP);
                                dbg(DEBUG,"(*prpPaxMain).SLoadCfg[%d].pclComplexFldsCri+%d<%s>",ilCount,ili,(*prpPaxMain).SLoadCfg[ilCount].pclComplexFldsCri+ili);
                                
                                SetZeroDTSSS(pclDssn,pclType,pclStyp,pclSstp,pclSsst);
                                
                                if(ilRc==OK)
                                {
                                    ExtractLoaWhereClauseValues((*prpPaxMain).SLoadCfg[ilCount].pclComplexFldsCri+ili, pclDssn, pclType,pclStyp, pclSstp, pclSsst,pclApc3);
                                    
                                    FormatDssnFldCfg(pclDssn);
                                    CollectAllDssnWORepeat(pclDssn);
                                    
                                    strcpy((*prpPaxMain).SLoadCfg[ilCount].ComplexCfg[ili].pclDssn, pclDssn);
                                    strcpy((*prpPaxMain).SLoadCfg[ilCount].ComplexCfg[ili].pclType, pclType);
                                    strcpy((*prpPaxMain).SLoadCfg[ilCount].ComplexCfg[ili].pclStyp, pclStyp);
                                    strcpy((*prpPaxMain).SLoadCfg[ilCount].ComplexCfg[ili].pclSstp, pclSstp);
                                    strcpy((*prpPaxMain).SLoadCfg[ilCount].ComplexCfg[ili].pclSsst, pclSsst);
                                    strcpy((*prpPaxMain).SLoadCfg[ilCount].ComplexCfg[ili].pclApc3, pclApc3);
                                    
                                    dbg(DEBUG,"%s +Complex<%d> pclDssn<%s>,pclType<%s>,pclStyp<%s>,pclSstp<%s>,pclSsst<%s>,pclApc3<%s>",pclFunc,ilComplex++,(*prpPaxMain).SLoadCfg[ilCount].ComplexCfg[ili].pclDssn,(*prpPaxMain).SLoadCfg[ilCount].ComplexCfg[ili].pclType,(*prpPaxMain).SLoadCfg[ilCount].ComplexCfg[ili].pclStyp,(*prpPaxMain).SLoadCfg[ilCount].ComplexCfg[ili].pclSstp,(*prpPaxMain).SLoadCfg[ilCount].ComplexCfg[ili].pclSsst,(*prpPaxMain).SLoadCfg[ilCount].ComplexCfg[ili].pclApc3);
                                }
                            }
                        }
                    }
                }
                else
                {
                    //Normal
                    (*prpPaxMain).SLoadCfg[ilCount].ilADCfg = 0;
                    (*prpPaxMain).SLoadCfg[ilCount].ilMoreThan1LoadCfgFlag = 0;
                    get_fld_value(pcpLoad_flds,ilCountWOStar+1,pclTmpValue_LOAD);
                    ilCountWOStar++;
                    
                    strcpy((*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsName,pclTmpValue_LOAD);
                    
                    dbg(DEBUG,"%s (*prpPaxMain).SLoadCfg[%d].pclLoadFldsName<%s>",pclFunc,ilCount,(*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsName);
                    
                    ilRc = iGetConfigEntryWOSec(cgLoadConfigFile,(*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsName,CFG_STRING,(*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsCri);
                    
                    dbg(DEBUG,"%s (*prpPaxMain).SLoadCfg[%d].pclLoadFldsCri<%s>",pclFunc,ilCount,(*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsCri);
                    
                    SetZeroDTSSS(pclDssn,pclType,pclStyp,pclSstp,pclSsst);
                    
                    if( ilRc == OK )
                    {
                        ExtractLoaWhereClauseValues ((*prpPaxMain).SLoadCfg[ilCount].pclLoadFldsCri, pclDssn, pclType,pclStyp, pclSstp, pclSsst,pclApc3);
                        
                        FormatDssnFldCfg(pclDssn);
                        CollectAllDssnWORepeat(pclDssn);
                        
                    strcpy((*prpPaxMain).SLoadCfg[ilCount].ConcreteCfg.pclDssn, pclDssn);
                    strcpy((*prpPaxMain).SLoadCfg[ilCount].ConcreteCfg.pclType, pclType);
                    strcpy((*prpPaxMain).SLoadCfg[ilCount].ConcreteCfg.pclStyp, pclStyp);
                    strcpy((*prpPaxMain).SLoadCfg[ilCount].ConcreteCfg.pclSstp, pclSstp);
                    strcpy((*prpPaxMain).SLoadCfg[ilCount].ConcreteCfg.pclSsst, pclSsst);
                    strcpy((*prpPaxMain).SLoadCfg[ilCount].ConcreteCfg.pclApc3, pclApc3);
                    
                      dbg(DEBUG,"%s +Normal<%d> pclDssn<%s>,pclType<%s>,pclStyp<%s>,pclSstp<%s>,pclSsst<%s>,pclApc3<%s>",pclFunc,ilNormal++,(*prpPaxMain).SLoadCfg[ilCount].ConcreteCfg.pclDssn, (*prpPaxMain).SLoadCfg[ilCount].ConcreteCfg.pclType,(*prpPaxMain).SLoadCfg[ilCount].ConcreteCfg.pclStyp, (*prpPaxMain).SLoadCfg[ilCount].ConcreteCfg.pclSstp, (*prpPaxMain).SLoadCfg[ilCount].ConcreteCfg.pclSsst,(*prpPaxMain).SLoadCfg[ilCount].ConcreteCfg.pclApc3);
                    }
                }
            }
        }
    }
//}
}

static void SetZeroDTSSS(char *pcpDssn,char *pcpType,char *pcpStyp,char *pcpSstp,char *pcpSsst)
{
    memset(pcpDssn,0,sizeof(pcpDssn));
    memset(pcpType,0,sizeof(pcpType));
    memset(pcpStyp,0,sizeof(pcpStyp));
    memset(pcpSstp,0,sizeof(pcpSstp));
    memset(pcpSsst,0,sizeof(pcpSsst));
}

static void FormatDssnFldCfg(char *pclDssn)
{
    char *pclFunc = "FormatDssnFldCfg";
    char pclTmp[1024] = "\0";
    
    memset(pclTmp,0,sizeof(pclTmp));
    if(strncmp(pclDssn,",",1)==0)
    {
        strcpy(pclTmp,(pclDssn)+1);
        
        if(strncmp(pclDssn+strlen(pclDssn)-1,",",1)==0)
        {
            pclTmp[strlen(pclTmp)-1] = '\0';
            //dbg(DEBUG,"%s pclTmp<%s>",pclFunc,pclTmp);
            memset(pclDssn,0,sizeof(pclDssn));
            strcpy(pclDssn,pclTmp);
        }
    }
    
    //dbg(DEBUG,"%s pclDssn<%s>",pclFunc,pclDssn);
}

static void CollectAllDssnWORepeat(char *pclDssn)
{
    //int ilRc = RC_SUCCESS; 
    char *pclFunc="CollectAllDssn";
    char pclTmp[16]="\0";
    char pclTmpDssn[16]="\0";
    int ilCount = 0;
    int ili = 0;
    
    if(strlen(pclDssn)==0 || strncmp(pclDssn," ",1)==0)
    {
        dbg(TRACE,"%s pclDssn is invalid",pclFunc);
    }
    else
    {
        ilCount = get_flds_count(pclDssn);
        for(ili=1;ili<=ilCount;ili++)
        {
            get_fld_value(pclDssn,ili,pclTmpDssn);
            if(strstr(pcgAllDssn,pclTmpDssn)==0)
            {
                {
                    if(igDssnCount == 0)
                    {
                        sprintf(pclTmp,"'%s'",pclTmpDssn);
                    }
                    else
                    {
                        sprintf(pclTmp,",'%s'",pclTmpDssn);
                    }
                    strcat(pcgAllDssn,pclTmp);
                    igDssnCount++;
                    dbg(DEBUG,"%s pclDssn<%s> has been added into pcgAllDssn,igDssnCount<%d>",pclFunc,pclTmpDssn,igDssnCount);
                }
            }
        }
    }
}

int getERPPXHConfig()
{
    int ilRC=RC_SUCCESS;
    
    dbg(TRACE, "getERPPXHConfig:: Start");

    memset(&rgERPPXHConfig, 0, sizeof(_ERPPXHCONFIG));
    ilRC=getFieldsConfig(&rgERPPXHConfig.mapArr, &rgERPPXHConfig.mapDep, "PAX_HEADER");
    if ( ilRC==RC_SUCCESS )
    {
        ilRC=getAdditionalFieldsConfig(&rgERPPXHConfig.mapExpArr, &rgERPPXHConfig.mapExpDep, "PAX_HEADER");    	
    }
    
    dbg(TRACE, "getERPPXHConfig:: End Result[%d]", ilRC);
    return ilRC;	
}

void printERPPXHConfig()
{
	int a=0;
	
    dbg(TRACE, "printERPPXHConfig:: Start");
    
    dbg(TRACE, "printERPPXHConfig:: Arrival Fields[%d], TABLENAME[%s]",
        rgERPPXHConfig.mapArr.ilCount, rgERPPXHConfig.mapArr.TABLENAME);
    for (a=0; a<rgERPPXHConfig.mapArr.ilCount; a++)
    {
        dbg(TRACE, "printERPPXHConfig:: Record[%d/%d] FIELD[%s], LoadField[%s]",
            a+1, rgERPPXHConfig.mapArr.ilCount, rgERPPXHConfig.mapArr.rec[a].DB_FIELD, rgERPPXHConfig.mapArr.rec[a].LOAD_FIELD); 	
    }
    
    dbg(TRACE, "printERPPXHConfig:: Departure Fields[%d], TABLENAME[%s]",
        rgERPPXHConfig.mapDep.ilCount, rgERPPXHConfig.mapDep.TABLENAME);
    for (a=0; a<rgERPPXHConfig.mapDep.ilCount; a++)
    {
        dbg(TRACE, "printERPPXHConfig:: Record[%d/%d] FIELD[%s], LoadField[%s]",
            a+1, rgERPPXHConfig.mapDep.ilCount, rgERPPXHConfig.mapDep.rec[a].DB_FIELD, rgERPPXHConfig.mapDep.rec[a].LOAD_FIELD); 	
    }

    dbg(TRACE, "printERPPXHConfig:: Additional Arrival Fields[%d], TABLENAME[%s]",
        rgERPPXHConfig.mapExpArr.ilCount, rgERPPXHConfig.mapExpArr.TABLENAME);
    for (a=0; a<rgERPPXHConfig.mapExpArr.ilCount; a++)
    {
        dbg(TRACE, "printERPPXHConfig:: Record[%d/%d] FIELD[%s], LoadField[%s]",
            a+1, rgERPPXHConfig.mapExpArr.ilCount, rgERPPXHConfig.mapExpArr.rec[a].DB_FIELD, rgERPPXHConfig.mapExpArr.rec[a].LOAD_FIELD); 	
    }
    
    dbg(TRACE, "printERPPXHConfig:: Additional Departure Fields[%d], TABLENAME[%s]",
        rgERPPXHConfig.mapExpDep.ilCount, rgERPPXHConfig.mapExpDep.TABLENAME);
    for (a=0; a<rgERPPXHConfig.mapExpDep.ilCount; a++)
    {
        dbg(TRACE, "printERPPXHConfig:: Record[%d/%d] FIELD[%s], LoadField[%s]",
            a+1, rgERPPXHConfig.mapExpDep.ilCount, rgERPPXHConfig.mapExpDep.rec[a].DB_FIELD, rgERPPXHConfig.mapExpDep.rec[a].LOAD_FIELD); 	
    }
    
    dbg(TRACE, "printERPPXHConfig:: End");
    return;    	
}

int getERPPXRConfig()
{
    int ilRC=RC_SUCCESS;
    
    dbg(TRACE, "getERPPXRConfig:: Start");

    memset(&rgERPPXRConfig, 0, sizeof(_ERPPXRCONFIG));
    ilRC=getFieldsConfig(&rgERPPXRConfig.mapArr, &rgERPPXRConfig.mapDep, "PAX_TRANSIT");
    if ( ilRC==RC_SUCCESS )
    {
        ilRC=getAdditionalFieldsConfig(&rgERPPXRConfig.mapExpArr, &rgERPPXRConfig.mapExpDep, "PAX_TRANSIT");    	
    }
    dbg(TRACE, "getERPPXRConfig:: End Result[%d]", ilRC);
    return ilRC;	
}

void printERPPXRConfig()
{
	int a=0;
	
    dbg(TRACE, "printERPPXRConfig:: Start");
    
    dbg(TRACE, "printERPPXRConfig:: Arrival Fields[%d], TABLENAME[%s]",
        rgERPPXRConfig.mapArr.ilCount, rgERPPXRConfig.mapArr.TABLENAME);
    for (a=0; a<rgERPPXRConfig.mapArr.ilCount; a++)
    {
        dbg(TRACE, "printERPPXRConfig:: Record[%d/%d] FIELD[%s], LoadField[%s]",
            a+1, rgERPPXRConfig.mapArr.ilCount, rgERPPXRConfig.mapArr.rec[a].DB_FIELD, rgERPPXRConfig.mapArr.rec[a].LOAD_FIELD); 	
    }
    
    dbg(TRACE, "printERPPXRConfig:: Departure Fields[%d], TABLENAME[%s]",
        rgERPPXRConfig.mapDep.ilCount, rgERPPXRConfig.mapDep.TABLENAME);
    for (a=0; a<rgERPPXRConfig.mapDep.ilCount; a++)
    {
        dbg(TRACE, "printERPPXRConfig:: Record[%d/%d] FIELD[%s], LoadField[%s]",
            a+1, rgERPPXRConfig.mapDep.ilCount, rgERPPXRConfig.mapDep.rec[a].DB_FIELD, rgERPPXRConfig.mapDep.rec[a].LOAD_FIELD); 	
    }

    dbg(TRACE, "printERPPXRConfig:: Additional Arrival Fields[%d], TABLENAME[%s]",
        rgERPPXRConfig.mapExpArr.ilCount, rgERPPXRConfig.mapExpArr.TABLENAME);
    for (a=0; a<rgERPPXRConfig.mapExpArr.ilCount; a++)
    {
        dbg(TRACE, "printERPPXRConfig:: Record[%d/%d] FIELD[%s], LoadField[%s]",
            a+1, rgERPPXRConfig.mapExpArr.ilCount, rgERPPXRConfig.mapExpArr.rec[a].DB_FIELD, rgERPPXRConfig.mapExpArr.rec[a].LOAD_FIELD); 	
    }
    
    dbg(TRACE, "printERPPXRConfig:: Additional Departure Fields[%d], TABLENAME[%s]",
        rgERPPXRConfig.mapExpDep.ilCount, rgERPPXRConfig.mapExpDep.TABLENAME);
    for (a=0; a<rgERPPXRConfig.mapExpDep.ilCount; a++)
    {
        dbg(TRACE, "printERPPXRConfig:: Record[%d/%d] FIELD[%s], LoadField[%s]",
            a+1, rgERPPXRConfig.mapExpDep.ilCount, rgERPPXRConfig.mapExpDep.rec[a].DB_FIELD, rgERPPXRConfig.mapExpDep.rec[a].LOAD_FIELD); 	
    }
    
    dbg(TRACE, "printERPPXRConfig:: End");
    return;    	
}

int getERPPXMConfig()
{
    int ilRC=RC_SUCCESS;
    
    dbg(TRACE, "getERPPXMConfig:: Start");

    memset(&rgERPPXMConfig, 0, sizeof(_ERPPXMCONFIG));
    ilRC=getFieldsConfig(&rgERPPXMConfig.mapArr, &rgERPPXMConfig.mapDep, "PAX_MULTI_ROUTE");
    if ( ilRC==RC_SUCCESS )
    {
        ilRC=getAdditionalFieldsConfig(&rgERPPXMConfig.mapExpArr, &rgERPPXMConfig.mapExpDep, "PAX_MULTI_ROUTE");    	
    }
    dbg(TRACE, "getERPPXMConfig:: End Result[%d]", ilRC);
    return ilRC;	
}

void printERPPXMConfig()
{
	int a=0;
	
    dbg(TRACE, "printERPPXMConfig:: Start");
    
    dbg(TRACE, "printERPPXMConfig:: Arrival Fields[%d], TABLENAME[%s]",
        rgERPPXMConfig.mapArr.ilCount, rgERPPXMConfig.mapArr.TABLENAME);
    for (a=0; a<rgERPPXMConfig.mapArr.ilCount; a++)
    {
        dbg(TRACE, "printERPPXMConfig:: Record[%d/%d] FIELD[%s], LoadField[%s]",
            a+1, rgERPPXMConfig.mapArr.ilCount, rgERPPXMConfig.mapArr.rec[a].DB_FIELD, rgERPPXMConfig.mapArr.rec[a].LOAD_FIELD); 	
    }
    
    dbg(TRACE, "printERPPXMConfig:: Departure Fields[%d], TABLENAME[%s]",
        rgERPPXMConfig.mapDep.ilCount, rgERPPXMConfig.mapDep.TABLENAME);
    for (a=0; a<rgERPPXMConfig.mapDep.ilCount; a++)
    {
        dbg(TRACE, "printERPPXMConfig:: Record[%d/%d] FIELD[%s], LoadField[%s]",
            a+1, rgERPPXMConfig.mapDep.ilCount, rgERPPXMConfig.mapDep.rec[a].DB_FIELD, rgERPPXMConfig.mapDep.rec[a].LOAD_FIELD); 	
    }

    dbg(TRACE, "printERPPXMConfig:: Additional Arrival Fields[%d], TABLENAME[%s]",
        rgERPPXMConfig.mapExpArr.ilCount, rgERPPXMConfig.mapExpArr.TABLENAME);
    for (a=0; a<rgERPPXMConfig.mapExpArr.ilCount; a++)
    {
        dbg(TRACE, "printERPPXMConfig:: Record[%d/%d] FIELD[%s], LoadField[%s]",
            a+1, rgERPPXMConfig.mapExpArr.ilCount, rgERPPXMConfig.mapExpArr.rec[a].DB_FIELD, rgERPPXMConfig.mapExpArr.rec[a].LOAD_FIELD); 	
    }
    
    dbg(TRACE, "printERPPXMConfig:: Additional Departure Fields[%d], TABLENAME[%s]",
        rgERPPXMConfig.mapExpDep.ilCount, rgERPPXMConfig.mapExpDep.TABLENAME);
    for (a=0; a<rgERPPXMConfig.mapExpDep.ilCount; a++)
    {
        dbg(TRACE, "printERPPXMConfig:: Record[%d/%d] FIELD[%s], LoadField[%s]",
            a+1, rgERPPXMConfig.mapExpDep.ilCount, rgERPPXMConfig.mapExpDep.rec[a].DB_FIELD, rgERPPXMConfig.mapExpDep.rec[a].LOAD_FIELD); 	
    }
    
    dbg(TRACE, "printERPPXMConfig:: End");
    return;    	
}

int getERPPXOConfig()
{
    int ilRC=RC_SUCCESS;
    
    dbg(TRACE, "getERPPXOConfig:: Start");

    memset(&rgERPPXOConfig, 0, sizeof(_ERPPXOCONFIG));
    ilRC=getFieldsConfig(&rgERPPXOConfig.mapArr, &rgERPPXOConfig.mapDep, "PAX_ONWARD_MULTI_ROUTE");
    if ( ilRC==RC_SUCCESS )
    {
        ilRC=getAdditionalFieldsConfig(&rgERPPXOConfig.mapExpArr, &rgERPPXOConfig.mapExpDep, "PAX_ONWARD_MULTI_ROUTE");    	
    }
    dbg(TRACE, "getERPPXOConfig:: End Result[%d]", ilRC);
    return ilRC;	
}

void printERPPXOConfig()
{
	int a=0;
	
    dbg(TRACE, "printERPPXOConfig:: Start");
    
    dbg(TRACE, "printERPPXOConfig:: Arrival Fields[%d], TABLENAME[%s]",
        rgERPPXOConfig.mapArr.ilCount, rgERPPXOConfig.mapArr.TABLENAME);
    for (a=0; a<rgERPPXOConfig.mapArr.ilCount; a++)
    {
        dbg(TRACE, "printERPPXOConfig:: Record[%d/%d] FIELD[%s], LoadField[%s]",
            a+1, rgERPPXOConfig.mapArr.ilCount, rgERPPXOConfig.mapArr.rec[a].DB_FIELD, rgERPPXOConfig.mapArr.rec[a].LOAD_FIELD); 	
    }
    
    dbg(TRACE, "printERPPXOConfig:: Departure Fields[%d], TABLENAME[%s]",
        rgERPPXOConfig.mapDep.ilCount, rgERPPXOConfig.mapDep.TABLENAME);
    for (a=0; a<rgERPPXOConfig.mapDep.ilCount; a++)
    {
        dbg(TRACE, "printERPPXOConfig:: Record[%d/%d] FIELD[%s], LoadField[%s]",
            a+1, rgERPPXOConfig.mapDep.ilCount, rgERPPXOConfig.mapDep.rec[a].DB_FIELD, rgERPPXOConfig.mapDep.rec[a].LOAD_FIELD); 	
    }

    dbg(TRACE, "printERPPXOConfig:: Additional Arrival Fields[%d], TABLENAME[%s]",
        rgERPPXOConfig.mapExpArr.ilCount, rgERPPXOConfig.mapExpArr.TABLENAME);
    for (a=0; a<rgERPPXOConfig.mapExpArr.ilCount; a++)
    {
        dbg(TRACE, "printERPPXOConfig:: Record[%d/%d] FIELD[%s], LoadField[%s]",
            a+1, rgERPPXOConfig.mapExpArr.ilCount, rgERPPXOConfig.mapExpArr.rec[a].DB_FIELD, rgERPPXOConfig.mapExpArr.rec[a].LOAD_FIELD); 	
    }
    
    dbg(TRACE, "printERPPXOConfig:: Additional Departure Fields[%d], TABLENAME[%s]",
        rgERPPXOConfig.mapExpDep.ilCount, rgERPPXOConfig.mapExpDep.TABLENAME);
    for (a=0; a<rgERPPXOConfig.mapExpDep.ilCount; a++)
    {
        dbg(TRACE, "printERPPXOConfig:: Record[%d/%d] FIELD[%s], LoadField[%s]",
            a+1, rgERPPXOConfig.mapExpDep.ilCount, rgERPPXOConfig.mapExpDep.rec[a].DB_FIELD, rgERPPXOConfig.mapExpDep.rec[a].LOAD_FIELD); 	
    }
    
    dbg(TRACE, "printERPPXOConfig:: End");
    return;    	
}


int getERPPXTConfig()
{
    int ilRC=RC_SUCCESS;
    
    dbg(TRACE, "getERPPXTConfig:: Start");

    memset(&rgERPPXTConfig, 0, sizeof(_ERPPXTCONFIG));
    ilRC=getFieldsConfig(&rgERPPXTConfig.mapArr, &rgERPPXTConfig.mapDep, "PAX_TRANSFER");
    if ( ilRC==RC_SUCCESS )
    {
        ilRC=getAdditionalFieldsConfig(&rgERPPXTConfig.mapExpArr, &rgERPPXTConfig.mapExpDep, "PAX_TRANSFER");    	
    }
    dbg(TRACE, "getERPPXTConfig:: End Result[%d]", ilRC);
    return ilRC;	
}

void printERPPXTConfig()
{
	int a=0;
	
    dbg(TRACE, "printERPPXTConfig:: Start");
    
    dbg(TRACE, "printERPPXTConfig:: Arrival Fields[%d], TABLENAME[%s]",
        rgERPPXTConfig.mapArr.ilCount, rgERPPXTConfig.mapArr.TABLENAME);
    for (a=0; a<rgERPPXTConfig.mapArr.ilCount; a++)
    {
        dbg(TRACE, "printERPPXTConfig:: Record[%d/%d] FIELD[%s], LoadField[%s]",
            a+1, rgERPPXTConfig.mapArr.ilCount, rgERPPXTConfig.mapArr.rec[a].DB_FIELD, rgERPPXTConfig.mapArr.rec[a].LOAD_FIELD); 	
    }
    
    dbg(TRACE, "printERPPXTConfig:: Departure Fields[%d], TABLENAME[%s]",
        rgERPPXTConfig.mapDep.ilCount, rgERPPXTConfig.mapDep.TABLENAME);
    for (a=0; a<rgERPPXTConfig.mapDep.ilCount; a++)
    {
        dbg(TRACE, "printERPPXTConfig:: Record[%d/%d] FIELD[%s], LoadField[%s]",
            a+1, rgERPPXTConfig.mapDep.ilCount, rgERPPXTConfig.mapDep.rec[a].DB_FIELD, rgERPPXTConfig.mapDep.rec[a].LOAD_FIELD); 	
    }

    dbg(TRACE, "printERPPXTConfig:: Additional Arrival Fields[%d], TABLENAME[%s]",
        rgERPPXTConfig.mapExpArr.ilCount, rgERPPXTConfig.mapExpArr.TABLENAME);
    for (a=0; a<rgERPPXTConfig.mapExpArr.ilCount; a++)
    {
        dbg(TRACE, "printERPPXTConfig:: Record[%d/%d] FIELD[%s], LoadField[%s]",
            a+1, rgERPPXTConfig.mapExpArr.ilCount, rgERPPXTConfig.mapExpArr.rec[a].DB_FIELD, rgERPPXTConfig.mapExpArr.rec[a].LOAD_FIELD); 	
    }
    
    dbg(TRACE, "printERPPXTConfig:: Additional Departure Fields[%d], TABLENAME[%s]",
        rgERPPXTConfig.mapExpDep.ilCount, rgERPPXTConfig.mapExpDep.TABLENAME);
    for (a=0; a<rgERPPXTConfig.mapExpDep.ilCount; a++)
    {
        dbg(TRACE, "printERPPXTConfig:: Record[%d/%d] FIELD[%s], LoadField[%s]",
            a+1, rgERPPXTConfig.mapExpDep.ilCount, rgERPPXTConfig.mapExpDep.rec[a].DB_FIELD, rgERPPXTConfig.mapExpDep.rec[a].LOAD_FIELD); 	
    }
    
    dbg(TRACE, "printERPPXTConfig:: End");
    return;    	
}
int computeExpression(DB_CONFIG_MAP *rp, _LOATAB *rpLoatab, int ipLoatabCount)
{
    int ilRC=RC_SUCCESS;
    char pclTmp[1024];
    char *pclPtr1, *pclPtr2;
    char pclOp[8];
    char pclValu[32];
    int a=0;
    int iFirst=0;
    int ilTotal=0;
    
    dbg(DEBUG, "computeExpression:: Start");

    for (a=0; a<rp->ilCount; a++)
    {
        dbg(TRACE, "computeExpression:: Record[%d/%d] Field[%s] Loa[%s]",
            a+1, rp->ilCount, rp->rec[a].DB_FIELD, rp->rec[a].LOAD_FIELD);	
        strcpy(pclTmp, rp->rec[a].LOAD_FIELD);
        
        strcpy(pclOp, "+");
        pclPtr1=strtok_r(pclTmp, ",", &pclPtr2);
        ilTotal=0;
        while ( pclPtr1 )
        {
            dbg(DEBUG, "computeExpression:: Op[%s], Field[%s]", pclOp, pclPtr1);	
            getFormulaValue(pclPtr1, rpLoatab, ipLoatabCount, pclValu);

            switch (pclOp[0])
            {
                case '+':
                    ilTotal+=atoi(pclValu);
                    break;	
                case '-':
                    ilTotal-=atoi(pclValu);
                    break;	
                case '*':
                    ilTotal*=atoi(pclValu);
                    break;	
                case '/':
                    ilTotal/=atoi(pclValu);
                    break;	
            }

            pclPtr1=strtok_r(NULL, ",", &pclPtr2);
            if ( pclPtr1 )
            {
                strcpy(pclOp, pclPtr1);
            }
            pclPtr1=strtok_r(NULL, ",", &pclPtr2);    	
        }
        sprintf(rp->rec[a].VALU, "%d", ilTotal);
    }
    dbg(DEBUG, "computeExpression:: End Result[%d]", ilRC);
    return ilRC;	
}

int getAdditionalFieldsConfig(DB_CONFIG_MAP *rpArr, DB_CONFIG_MAP *rpDep, char *pcpHeader)
{
    int ilRC=RC_SUCCESS;
    char pclFieldLines[1024];
    char *pclFieldPtr, *pclFieldPtr2;
    char pclFormula[1024];
    char *pclFormulaPtr, *pclFormulaPtr2;
    DB_CONFIG_PAIR *rlPairTmp;
    DB_CONFIG_MAP *rlMapDef;
    int ilSize=0;
   
    dbg(TRACE, "getAdditionalFieldsConfig:: START pcpHeader[%s]", pcpHeader);
    
    ilRC=checkHeaderExists(pcpHeader);
    if ( ilRC==RC_SUCCESS )
    {
    	ilRC = ReadConfigEntry(cgConfigFile, pcpHeader, "DEST_TABLE", pclFieldLines);
        strcpy(rpArr->TABLENAME, pclFieldLines);
        strcpy(rpDep->TABLENAME, pclFieldLines);
        
        ilRC = ReadConfigEntry(cgConfigFile, pcpHeader, "ADDL_FIELDS", pclFieldLines);
        pclFieldPtr=strtok_r(pclFieldLines, ",", &pclFieldPtr2);
        while ( pclFieldPtr )
        {
            trim(pclFieldPtr, ' ');
            dbg(TRACE, "getAdditionalFieldsConfig:: Expression[%s]", pclFieldPtr);	
            
            ilRC = ReadConfigEntry(cgConfigFile, pcpHeader, pclFieldPtr, pclFormula);
            trim(pclFormula, ' ');
            dbg(TRACE, "getAdditionalFieldsConfig:: Expression[%s]", pclFormula);
            
            pclFormulaPtr=strtok_r(pclFormula, ":", &pclFormulaPtr2);
            dbg(TRACE, "getAdditionalFieldsConfig:: ADID[%s]", pclFormulaPtr);
            if ( !strcmp(pclFormulaPtr, "A") )
                rlMapDef=rpArr;
            else if ( !strcmp(pclFormulaPtr, "D") )
                rlMapDef=rpDep;
            
            ilSize=rlMapDef->ilCount;
            if ( !ilSize )
            {
        	    rlMapDef->rec=malloc(sizeof(DB_CONFIG_PAIR)); 	
        	    if ( rlMapDef->rec==NULL )
        	    {
        	    	dbg(TRACE, "getAdditionalFieldsConfig:: Cannot malloc rlMapDef->rec");
        	        ilRC=RC_FAIL;
        	        break;	
        	    }            	
            }
            else
            {
                rlPairTmp=realloc(rlMapDef->rec, sizeof(DB_CONFIG_PAIR)*(ilSize+1));
        	    if ( rlPairTmp==NULL )
        	    {
        	    	dbg(TRACE, "getAdditionalFieldsConfig:: Cannot realloc rlMapDef->rec");
        	        ilRC=RC_FAIL;
        	        break;	
        	    }
                rlMapDef->rec=rlPairTmp;           	
            }
            
            if ( ilRC==RC_SUCCESS )
            {
            	memset(&rlMapDef->rec[ilSize], 0, sizeof(DB_CONFIG_PAIR));
                
                pclFormulaPtr=strtok_r(NULL, ":", &pclFormulaPtr2);
                
                if ( pclFormulaPtr )
                {
                	trim(pclFormulaPtr, ' ');
                	strcpy(rlMapDef->rec[ilSize].DB_FIELD, pclFormulaPtr);
                }                
                

                pclFormulaPtr=strtok_r(NULL, ":", &pclFormulaPtr2);
                if ( pclFormulaPtr )
                {
                	trim(pclFormulaPtr, ' ');
                	strcpy(rlMapDef->rec[ilSize].LOAD_FIELD, pclFormulaPtr);
                }
            }

            if ( ilRC==RC_SUCCESS )
            {
            	rlMapDef->ilCount++;
            }
            pclFieldPtr=strtok_r(NULL, ",", &pclFieldPtr2);
        }
    }
    
    dbg(TRACE, "getAdditionalFieldsConfig:: END Result[%d] pcpHeader[%s]", ilRC, pcpHeader);
    return ilRC;	
}


int getFieldsConfig(DB_CONFIG_MAP *rpArr, DB_CONFIG_MAP *rpDep, char *pcpHeader)
{
    int ilRC=RC_SUCCESS;
    char pclFieldLines[2048];
    char pclArrFieldLines[2048];
    char pclDepFieldLines[2048];
    char *pclPtrFld1, *pclPtrFld2;
    char *pclPtrArr1, *pclPtrArr2;
    char *pclPtrDep1, *pclPtrDep2;
    DB_CONFIG_PAIR *rlTmp;
    
    
    dbg(TRACE, "getFieldsConfig:: START pcpHeader[%s]", pcpHeader);
    ilRC=checkHeaderExists(pcpHeader);
    if ( ilRC==RC_SUCCESS )
    {
        ilRC = ReadConfigEntry(cgConfigFile, pcpHeader, "DEST_TABLE", pclFieldLines);
        strcpy(rpArr->TABLENAME, pclFieldLines);
        strcpy(rpDep->TABLENAME, pclFieldLines);
         
        ilRC = ReadConfigEntry(cgConfigFile, pcpHeader, "DB_FLDS", pclFieldLines);		
        ilRC = ReadConfigEntry(cgConfigFile, pcpHeader, "ARR_LOAD_FLDS", pclArrFieldLines);		
        ilRC = ReadConfigEntry(cgConfigFile, pcpHeader, "DEP_LOAD_FLDS", pclDepFieldLines);		
        
        dbg(TRACE, "getFieldsConfig:: DB_FIELDS[%s]", pclFieldLines);
        dbg(TRACE, "getFieldsConfig:: ARR_LOAD_FLDS[%s]", pclArrFieldLines);
        dbg(TRACE, "getFieldsConfig:: DEP_LOAD_FLDS[%s]", pclDepFieldLines);
        
        pclPtrFld1=strtok_r(pclFieldLines, ",", &pclPtrFld2);
        pclPtrArr1=strtok_r(pclArrFieldLines, ",", &pclPtrArr2);
        pclPtrDep1=strtok_r(pclDepFieldLines, ",", &pclPtrDep2);
        
        while ( pclPtrFld1 )
        {
        	if ( pclPtrFld1 )
        	    trim(pclPtrFld1, ' ');
        	if ( pclPtrArr1 )
        	    trim(pclPtrArr1, ' ');
        	if ( pclPtrDep1 )
        	    trim(pclPtrDep1, ' ');

        	dbg(TRACE, "getFieldsConfig:: pclPtrFld1[%s], pclPtrArr1[%s], pclPtrDep1[%s]", 
        	    pclPtrFld1, pclPtrArr1, pclPtrDep1);
        	            	 
        	if ( !rpArr->ilCount )
        	{
        	    rpArr->rec=malloc(sizeof(DB_CONFIG_PAIR)); 	
        	    if ( rpArr->rec==NULL )
        	    {
        	    	dbg(TRACE, "getFieldsConfig:: Cannot malloc rpArr->rec");
        	        ilRC=RC_FAIL;
        	        break;	
        	    }
        	    rpDep->rec=malloc(sizeof(DB_CONFIG_PAIR)); 	
        	    if ( rpDep->rec==NULL )
        	    {
        	    	dbg(TRACE, "getFieldsConfig:: Cannot malloc rpDep->rec");
        	        ilRC=RC_FAIL;
        	        break;	
        	    }
        	}
        	else
        	{
        	    rlTmp=realloc(rpArr->rec, sizeof(DB_CONFIG_PAIR)*(rpArr->ilCount+1));
        	    if ( rlTmp==NULL )
        	    {
        	    	dbg(TRACE, "getFieldsConfig:: Cannot realloc rpArr->rec");
        	        ilRC=RC_FAIL;
        	        break;	
        	    }
                rpArr->rec=rlTmp;
                
        	    rlTmp=realloc(rpDep->rec, sizeof(DB_CONFIG_PAIR)*(rpDep->ilCount+1));
        	    if ( rlTmp==NULL )
        	    {
        	    	dbg(TRACE, "getFieldsConfig:: Cannot realloc rpDep->rec");
        	        ilRC=RC_FAIL;
        	        break;	
        	    }
                rpDep->rec=rlTmp;
        	}  
        	
        	memset(&rpArr->rec[rpArr->ilCount], 0, sizeof(DB_CONFIG_PAIR));
        	memset(&rpDep->rec[rpDep->ilCount], 0, sizeof(DB_CONFIG_PAIR));
        	
        	strcpy(rpArr->rec[rpArr->ilCount].DB_FIELD, pclPtrFld1);
        	strcpy(rpDep->rec[rpDep->ilCount].DB_FIELD, pclPtrFld1);

        	if ( pclPtrArr1 )
        	    strcpy(rpArr->rec[rpArr->ilCount].LOAD_FIELD, pclPtrArr1);
        	if ( pclPtrDep1 )   
        	    strcpy(rpDep->rec[rpDep->ilCount].LOAD_FIELD, pclPtrDep1);        

        	rpArr->ilCount++;
        	rpDep->ilCount++;
        	    
            pclPtrFld1=strtok_r(NULL, ",", &pclPtrFld2);
            pclPtrArr1=strtok_r(NULL, ",", &pclPtrArr2);
            pclPtrDep1=strtok_r(NULL, ",", &pclPtrDep2);
        }
    }  
    dbg(TRACE, "getFieldsConfig:: END Result[%d] pcpHeader[%s]", ilRC, pcpHeader);
    return ilRC;	
}

int getERPBILLConfig()
{
    int ilRC=RC_SUCCESS;
    
    dbg(TRACE, "getERPBILLConfig:: Start");
    
    memset(&rgERPBILLConfig, 0, sizeof(_ERPBILLCONFIG));

    ilRC=getFlightCategoryConfig(&rgERPBILLConfig.rlFlightCat, "AIR_BILL_HANDLING");

    dbg(TRACE, "getERPBILLConfig:: End Result[%d]", ilRC);
    return ilRC;	
}

void printERPBILLConfig()
{
	int a=0;
	
    dbg(TRACE, "printERPBILLConfig:: Start");

    dbg(TRACE, "printERPBILLConfig:: Total Flight Catergory Records[%d]", rgERPBILLConfig.rlFlightCat.ilCount);
    for (a=0; a<rgERPBILLConfig.rlFlightCat.ilCount; a++)
    {
        dbg(TRACE, "printERPBILLConfig:: Record[%d/%d], NAME[%s], DESC[%s], FIELD[%s], VALUE[%s]",
            a+1, rgERPBILLConfig.rlFlightCat.ilCount, rgERPBILLConfig.rlFlightCat.rec[a].NAME, rgERPBILLConfig.rlFlightCat.rec[a].DESC,
            rgERPBILLConfig.rlFlightCat.rec[a].FIELD, rgERPBILLConfig.rlFlightCat.rec[a].VALUE); 	    
    }

    dbg(TRACE, "printERPBILLConfig:: End");
    return;
}

int getERPSCHConfig()
{
    int ilRC=RC_SUCCESS;
    
    dbg(TRACE, "getERPSCHConfig:: Start");
    
    memset(&rgERPSCHConfig, 0, sizeof(_ERPSCHCONFIG));
 
    ilRC=getFlightCategoryConfig(&rgERPSCHConfig.rlFlightCat, "DAILY_SCHEDULE");
 
    dbg(TRACE, "getERPSCHConfig:: End Result[%d]", ilRC);
    return ilRC;	
}

void printERPSCHConfig()
{
	int a=0;
	
    dbg(TRACE, "printERPSCHConfig:: Start");

    dbg(TRACE, "printERPSCHConfig:: Total Flight Catergory Records[%d]", rgERPSCHConfig.rlFlightCat.ilCount);
    for (a=0; a<rgERPSCHConfig.rlFlightCat.ilCount; a++)
    {
        dbg(TRACE, "printERPSCHConfig:: Record[%d/%d], NAME[%s], DESC[%s], FIELD[%s], VALUE[%s]",
            a+1, rgERPSCHConfig.rlFlightCat.ilCount, rgERPSCHConfig.rlFlightCat.rec[a].NAME, rgERPSCHConfig.rlFlightCat.rec[a].DESC,
            rgERPSCHConfig.rlFlightCat.rec[a].FIELD, rgERPSCHConfig.rlFlightCat.rec[a].VALUE); 	    
    }

    dbg(TRACE, "printERPSCHConfig:: End");
    return;
}


int getERPATCConfig()
{
    int ilRC=RC_SUCCESS;
    
    dbg(TRACE, "getERPATCConfig:: Start");
    
    memset(&rgERPATCConfig, 0, sizeof(_ERPATCCONFIG));
 
    ilRC=getFlightCategoryConfig(&rgERPATCConfig.rlFlightCat, "ATC_FLIGHT_MOVEMENT");
 
    dbg(TRACE, "getERPATCConfig:: End Result[%d]", ilRC);
    return ilRC;	
}

void printERPATCConfig()
{
	int a=0;
	
    dbg(TRACE, "printERPATCConfig:: Start");

    dbg(TRACE, "printERPATCConfig:: Total Flight Catergory Records[%d]", rgERPATCConfig.rlFlightCat.ilCount);
    for (a=0; a<rgERPATCConfig.rlFlightCat.ilCount; a++)
    {
        dbg(TRACE, "printERPATCConfig:: Record[%d/%d], NAME[%s], DESC[%s], FIELD[%s], VALUE[%s]",
            a+1, rgERPATCConfig.rlFlightCat.ilCount, rgERPATCConfig.rlFlightCat.rec[a].NAME, rgERPATCConfig.rlFlightCat.rec[a].DESC,
            rgERPATCConfig.rlFlightCat.rec[a].FIELD, rgERPATCConfig.rlFlightCat.rec[a].VALUE); 	    
    }

    dbg(TRACE, "printERPATCConfig:: End");
    return;
}

int checkHeaderExists(char *pcpHeader)
{
    int ilRC=RC_SUCCESS;
    FILE *fptr;
    char pclLine[1024];
    char pclTmp[1024];
    int ilFound=0;

    fptr=fopen(cgConfigFile, "r");
    if ( fptr==NULL )
    {
        dbg(TRACE, "checkHeaderExists:: Cannot open config file [%s]", cgConfigFile);
        ilRC=RC_FAIL;	
    }
    else
    {
    	ilFound=0;
    	sprintf(pclTmp, "[%s]", pcpHeader);
        while ( fgets(pclLine, 1023, fptr) )
        {
        	pclLine[strlen(pclLine)-1]=0;
        	trim(pclLine, ' ');
        	if ( pclLine[0]=='#' )
        	     continue;
            else if ( !strcmp(pclLine, pclTmp) )
            {
            	ilFound=1;
                break;
            }
        }
        
        if ( !ilFound )
        {
            dbg(TRACE, "checkHeaderExists:: Cannot find %s in config file!", pclTmp);
            ilRC=RC_FAIL;
        }
        else
        {
        	ilFound=0;
        	sprintf(pclTmp, "[%s_END]", pcpHeader);
            while ( fgets(pclLine, 1023, fptr) )
            {
            	pclLine[strlen(pclLine)-1]=0;
            	trim(pclLine, ' ');
        	    if ( !strcmp(pclLine, pclTmp) )
        	    {
        	        ilFound=1;
        	        break;	
        	    }
        	}
        	
        	if ( !ilFound )
        	{
                dbg(TRACE, "checkHeaderExists:: Cannot find %s in config file!", pclTmp);
                ilRC=RC_FAIL;
        	}
        }
        fclose(fptr);	
    }    
    return ilRC;	
}

int getFlightCategoryConfig(_FLIGHTCAT *rp, char *pcpHeader)
{
    int ilRC=RC_SUCCESS;
    char pclLine[1024];
    char pclTmp[1024];
    char *pclPtrHead;
    char *pclPtrBody;
    char *pclPtrTmp;
    char *pclPtrTmp2;
    int ilFound=0;
    _FLIGHTCATRECORD *rlTmp;
    
    dbg(TRACE, "getFlightCategoryConfig:: Start Header[%s]", pcpHeader);
 
    ilRC=checkHeaderExists(pcpHeader);

    if ( ilRC==RC_SUCCESS )
    {
    	dbg(TRACE, "getFlightCategoryConfig:: Get Flight Category");
        ilRC = ReadConfigEntry(cgConfigFile, pcpHeader, "FLIGHT_CAT",pclLine);	
        if ( ilRC==RC_SUCCESS )
        {
            pclPtrHead=strtok_r(pclLine, ",", &pclPtrTmp);
            while ( pclPtrHead )
            {
                trim(pclPtrHead, ' ');
                //ilFound= ReadConfigEntry(cgConfigFile, pcpHeader, pclPtrHead, pclTmp);	
                ilFound=readConfigFile(cgConfigFile, pcpHeader, pclPtrHead, pclTmp);
                trim(pclTmp,' ');
                dbg(TRACE, "getFlightCategoryConfig:: Flight Category[%s][%s]", pclPtrHead, pclTmp);
                
                
                if ( rp->ilCount==0 )
                {
                    rp->rec=malloc(sizeof(_FLIGHTCATRECORD));
                }
                else
                {
                    rlTmp=realloc(rp->rec, sizeof(_FLIGHTCATRECORD)*(rp->ilCount+1));
                    rp->rec=rlTmp;
                }

                if ( rp->rec )
                {
                	memset(&rp->rec[rp->ilCount], 0, sizeof(_FLIGHTCATRECORD));
                	strcpy(rp->rec[rp->ilCount].NAME, pclPtrHead);

                    pclPtrBody=strtok_r(pclTmp, ":", &pclPtrTmp2);
                    if ( pclPtrBody )                   
                        strcpy(rp->rec[rp->ilCount].DESC, pclPtrBody);   	
          
                    pclPtrBody=strtok_r(NULL, ":", &pclPtrTmp2);                   
                    if ( pclPtrBody )
                        strcpy(rp->rec[rp->ilCount].FIELD, pclPtrBody);   	
                      	
                    pclPtrBody=strtok_r(NULL, ":", &pclPtrTmp2);     
                    if ( pclPtrBody )              
                        strcpy(rp->rec[rp->ilCount].VALUE, pclPtrBody);   	
                 }
                else
                {
                    dbg(TRACE, "getFlightCategoryConfig:: Cannot allocate memory to rp->rec");
                    ilRC=RC_FAIL;
                    break;
                }
                rp->ilCount++;
                
                pclPtrHead=strtok_r(NULL, ",", &pclPtrTmp);
            }
        }
    }
    dbg(TRACE, "getFlightCategoryConfig:: End Result[%d]", ilRC);
    return ilRC;	
}

static int CalTimeWindowDaily(int ilCurWeekDay, char *pcpFrom, char *pcpTo)
{
    char *pclFunc = "CalTimeWindowDaily";
    int ilOffset1 = 0;
    int ilOffset2 = 0;
    char pclSTOA1stOption[256] = "\0";
    char pclSTOA3rdOption[256] = "\0";
    int ilRc = RC_SUCCESS;
    
    //STOA = MONDAY,060000,MONDAY+1,060000
    int ilSTOACfg1stFld = 0;
    int ilSTOACfg3rdFld = 0;
    
    ilRc = GetConfigOption(1,pclSTOA1stOption,rgTM_Daily.pcpSTOAConfig);
    if(ilRc == RC_SUCCESS)
    {
        dbg(DEBUG,"%s pclSTOA1stOption<%s>",pclFunc,pclSTOA1stOption);
        ilSTOACfg1stFld = ConvertDayFromStrngToInt(pclSTOA1stOption);
        if(ilSTOACfg1stFld < 0 || ilSTOACfg1stFld > 6)
        {
            dbg(TRACE,"%s ilSTOACfg1stFld<%d>is out of [0,6],return",pclFunc,ilSTOACfg1stFld);
            return RC_FAIL;
        }
        
        dbg(DEBUG,"%s ilSTOACfg1stFld<%d>",pclFunc,ilSTOACfg1stFld);
        
        ilOffset1 = CalDateOffset(ilCurWeekDay,ilSTOACfg1stFld);
        if(ilOffset1 < 0 || ilOffset1 > 7)
        {
            dbg(TRACE,"%s ilOffset is invalid, return",pclFunc);
            return RC_FAIL;
        }
        
        //ilRc = GetSTOAConfigOption(3,pclSTOA3rdOption);
        ilRc = GetConfigOption(3,pclSTOA3rdOption,rgTM_Daily.pcpSTOAConfig);
        if(ilRc == RC_SUCCESS)
        {
            dbg(DEBUG,"%s pclSTOA3rdOption<%s>",pclFunc,pclSTOA3rdOption);
            
            ilSTOACfg3rdFld = ConvertDayFromStrngToInt(pclSTOA3rdOption);
            if(ilSTOACfg3rdFld < 0 || ilSTOACfg3rdFld > 6)
            {
                dbg(TRACE,"%s ilSTOACfg3rdFld<%d>is out of [0,6],return",pclFunc,ilSTOACfg3rdFld);
                return RC_FAIL;
            }
            
            dbg(DEBUG,"%s ilSTOACfg3rdFld<%d>",pclFunc,ilSTOACfg3rdFld);
            
            ilOffset2 = CalDateOffset(ilSTOACfg1stFld,ilSTOACfg3rdFld);
            if(ilOffset2 < 0 || ilOffset2 > 7)
            {
                dbg(TRACE,"%s ilOffset is invalid, return",pclFunc);
                return RC_FAIL;
            }
            
            GetDateStrBasedOnOffset(pcpFrom,ilOffset1);
            if( ilRc == RC_SUCCESS)
        {
            dbg(DEBUG,"%s pcpFrom<%s>",pclFunc,pcpFrom);
        }
        else
        {
            dbg(DEBUG,"%s GetDateStrBasedOnOffset(pcpFrom,ilOffset1) fails,exit",pclFunc);
        }
            
            GetDateStrBasedOnOffset(pcpTo,ilOffset1+ilOffset2);
            if( ilRc == RC_SUCCESS)
        {
            dbg(DEBUG,"%s pcpTo<%s>",pclFunc,pcpTo);
        }
        else
        {
            dbg(DEBUG,"%s GetDateStrBasedOnOffset(pcpTo,ilOffset1+ilOffset2) fails,exit",pclFunc);
        }
          
            return RC_SUCCESS;
        }
        else
        {
            dbg(TRACE,"%s GetConfigOption(3,pclSTOA3rdOption,rgTM_Daily.pcpSTOAConfig) fails, return",pclFunc);
            return RC_FAIL;
        }
    }
    else
    {
        dbg(TRACE,"%s GetConfigOption(1,pclSTOA1stOption,rgTM_Daily.pcpSTOAConfig) fails, return",pclFunc);
        return RC_FAIL;
    }
}

static void GetFieldAndShow(char *pclArrOrDep,AFTREC *rlAft,int ilCount,char *pclDataArea)
{
    char *pclFunc = "GetFieldAndShow";
    int ilRc = RC_SUCCESS;
    char pclTmpFlnoCha[32] = "\0";
    char pclTmpFlnoNum[32] = "\0";
    char pclTmpYear[32] = "\0";
    char pclTmpMonth[32] = "\0";
    char pclTmpDay[32] = "\0";
    char pclSqlBuf[1024] = "\0";
    char pclSqlData[4096] = "\0";
    char pclTmpDateString[64] = "\0";
    char *pclPointer = "\0";
    int ili = 0;
    int ilJCNT = 0;
    int ilVIAN = 0;
    char pclTmpALC2[256] = "\0";
    char pclTmpALC3[256] = "\0";
    char pclTmp[256] = "\0";
    char pclTmpFLNO[1024] = "\0";
    
    //Frank 20121109 v0.5
    ACTTABREC stAct;
    int iTmp;
    
    memset(rlAft, 0, sizeof(AFTREC));
    get_fld_value(pclDataArea,igAftUrno,rlAft->URNO); TrimRight(rlAft->URNO);
    get_fld_value(pclDataArea,igAftRkey,rlAft->RKEY); TrimRight(rlAft->RKEY);
    get_fld_value(pclDataArea,igAftStat,rlAft->STAT); TrimRight(rlAft->STAT);
    get_fld_value(pclDataArea,igAftOrg4,rlAft->ORG4); TrimRight(rlAft->ORG4);
    get_fld_value(pclDataArea,igAftDes4,rlAft->DES4); TrimRight(rlAft->DES4);
    get_fld_value(pclDataArea,igAftRegn,rlAft->REGN); TrimRight(rlAft->REGN);
    get_fld_value(pclDataArea,igAftLand,rlAft->LAND); TrimRight(rlAft->LAND);
    get_fld_value(pclDataArea,igAftAirb,rlAft->AIRB); TrimRight(rlAft->AIRB);
    get_fld_value(pclDataArea,igAftRkey,rlAft->RKEY); TrimRight(rlAft->RKEY);
    get_fld_value(pclDataArea,igAftFlno,rlAft->FLNO); TrimRight(rlAft->FLNO);
    get_fld_value(pclDataArea,igAftStod,rlAft->STOD); TrimRight(rlAft->STOD);
    get_fld_value(pclDataArea,igAftStoa,rlAft->STOA); TrimRight(rlAft->STOA);
    get_fld_value(pclDataArea,igAftHopo,rlAft->HOPO); TrimRight(rlAft->HOPO);
    get_fld_value(pclDataArea,igAftAlc2,rlAft->ALC2); TrimRight(rlAft->ALC2);
    get_fld_value(pclDataArea,igAftAlc3,rlAft->ALC3); TrimRight(rlAft->ALC3);
    get_fld_value(pclDataArea,igAftVial,rlAft->VIAL); TrimRight(rlAft->VIAL);
    get_fld_value(pclDataArea,igAftVian,rlAft->VIAN); TrimRight(rlAft->VIAN);
    get_fld_value(pclDataArea,igAftAct5,rlAft->ACT5); TrimRight(rlAft->ACT5);
    get_fld_value(pclDataArea,igAftFtyp,rlAft->FTYP); TrimRight(rlAft->FTYP);
    get_fld_value(pclDataArea,igAftPsta,rlAft->PSTA); TrimRight(rlAft->PSTA);
    get_fld_value(pclDataArea,igAftPstd,rlAft->PSTD); TrimRight(rlAft->PSTD);
    get_fld_value(pclDataArea,igAftOrg3,rlAft->ORG3); TrimRight(rlAft->ORG3);
    get_fld_value(pclDataArea,igAftDes3,rlAft->DES3); TrimRight(rlAft->DES3);
    get_fld_value(pclDataArea,igAftCdat,rlAft->CDAT); TrimRight(rlAft->CDAT);
    get_fld_value(pclDataArea,igAftTtyp,rlAft->TTYP); TrimRight(rlAft->TTYP);
    get_fld_value(pclDataArea,igAftAct3,rlAft->ACT3); TrimRight(rlAft->ACT3);
    get_fld_value(pclDataArea,igAftJfno,rlAft->JFNO); TrimRight(rlAft->JFNO);
    get_fld_value(pclDataArea,igAftJcnt,rlAft->JCNT); TrimRight(rlAft->JCNT);
    get_fld_value(pclDataArea,igAftAdho,rlAft->ADHO); TrimRight(rlAft->ADHO);
    get_fld_value(pclDataArea,igAftAdid,rlAft->ADID); TrimRight(rlAft->ADID);
    get_fld_value(pclDataArea,igAftCsgn,rlAft->CSGN); TrimRight(rlAft->CSGN);
    get_fld_value(pclDataArea,igAftEtdi,rlAft->ETDI); TrimRight(rlAft->ETDI);
    get_fld_value(pclDataArea,igAftEtai,rlAft->ETAI); TrimRight(rlAft->ETAI);
    get_fld_value(pclDataArea,igAftDqcs,rlAft->DQCS); TrimRight(rlAft->DQCS);
    get_fld_value(pclDataArea,igAftRwya,rlAft->RWYA); TrimRight(rlAft->RWYA);
    get_fld_value(pclDataArea,igAftRwyd,rlAft->RWYD); TrimRight(rlAft->RWYD);
    get_fld_value(pclDataArea,igAftTga1,rlAft->TGA1); TrimRight(rlAft->TGA1);
    get_fld_value(pclDataArea,igAftTgd1,rlAft->TGD1); TrimRight(rlAft->TGD1);
    get_fld_value(pclDataArea,igAftStev,rlAft->STEV); TrimRight(rlAft->STEV);
    get_fld_value(pclDataArea,igAftMopa,rlAft->MOPA); TrimRight(rlAft->MOPA);
    get_fld_value(pclDataArea,igAftMtow,rlAft->MTOW); TrimRight(rlAft->MTOW);
    get_fld_value(pclDataArea,igAftBlto,rlAft->BLTO); TrimRight(rlAft->BLTO);
    get_fld_value(pclDataArea,igAftPaid,rlAft->PAID); TrimRight(rlAft->PAID);
  
    if(strlen(rlAft->STOA)!=0 && strncmp(rlAft->STOA," ",1)!=0)
    {
        if( strncmp(rlAft->STOA,"X",1)==0 || strncmp(rlAft->STOA,"N",1)==0)
        {
              strcpy(rlAft->ftypDaily,"CANCELLED");
              dbg(DEBUG,"%s ftypDaily",pclFunc,rlAft->ftypDaily);
          }
    }
   
    if(strlen(rlAft->VIAL) == 0 || strncmp(rlAft->VIAL+1," ",1) == 0 || strncmp(rlAft->VIAN," ",1) == 0 || atoi(rlAft->VIAN) == 0)
    {    
        dbg(DEBUG,"VIAL or VIAN is NULL or space or zero");
    }
    else
    {
        ilVIAN = atoi(rlAft->VIAN);
        pclPointer = rlAft->VIAL;
        
        for(ili=0;ili < ilVIAN;ili++)
        {
            memset(pclTmp,0,sizeof(pclTmp));
            if(ili==0)
            {
                strncpy(pclTmp,pclPointer+1,3);
               }
            else
            {
                strncpy(pclTmp,pclPointer,3);
            }
            
            if(ili == 0)
            {
                pclPointer += 121;
            }
            else
            {
                pclPointer += 120;
            }    
          strcat(rlAft->vialDaily,pclTmp);
          
          if(ili < (ilVIAN-1))
          {
              strcat(rlAft->vialDaily,".");
          }
      }
    }    
  
    if(strlen(rlAft->JFNO) == 0 || strncmp(rlAft->JFNO," ",1) == 0 || strncmp(rlAft->JCNT," ",1) == 0 || atoi(rlAft->JCNT) == 0)
    {    
        dbg(DEBUG,"JFNO or JCNT is NULL or space or zero");
    }
    else
    {
        ilJCNT = atoi(rlAft->JCNT);
        pclPointer = rlAft->JFNO;

        memset(rlAft->acosDaily, 0, sizeof(*(rlAft->acosDaily)));
        for(ili=0; ili < ilJCNT;ili++)
        {
            if ( strlen(pclPointer)>=7 )
            {
                if ( ili==0 )
                    sprintf(rlAft->acosDaily, "%s%2.2s%4.4s", rlAft->acosDaily, pclPointer, &pclPointer[3]);
                else
                    sprintf(rlAft->acosDaily, "%s.%2.2s%4.4s", rlAft->acosDaily, pclPointer, &pclPointer[3]);
                pclPointer += 9;
            }
         }
        //acosDaily
    }
    
    strcpy(rlAft->sectDaily,rlAft->ORG3);
    strcat(rlAft->sectDaily,"-");
    strcat(rlAft->sectDaily,"AUH");
    
    strcpy(rlAft->sap3Daily,rlAft->ORG3);
    strcpy(rlAft->dap3Daily,rlAft->DES3);
    
    memset(pclSqlBuf,0,sizeof(pclSqlBuf));
    memset(pclSqlData,0,sizeof(pclSqlData));
    sprintf(pclSqlBuf,"SELECT APC4 FROM APTTAB WHERE APC3='%s'",rlAft->sap3Daily);
    dbg(TRACE,"%s pclSqlBuf<%s>",pclFunc,pclSqlBuf);
    ilRc = RunSQL(pclSqlBuf,pclSqlData);
    if( ilRc == DB_SUCCESS )
    {
        TrimRight(pclSqlData);
        memset(rlAft->sap4Daily,0,sizeof(rlAft->sap4Daily));
        dbg(TRACE,"%s Executes successfully<%s>pclSqlData<%s>",pclFunc,pclSqlBuf,pclSqlData);
        strcpy(rlAft->sap4Daily,pclSqlData);
    }
    else if(ilRc == ORA_NOT_FOUND)
    {
        dbg(TRACE,"%s RunSQL<%s> Not found",pclFunc,pclSqlBuf);
    }
    else
    {
        dbg(TRACE,"%s RunSQL Error<%s>",pclFunc,pclSqlBuf);
    }
    
    memset(pclSqlBuf,0,sizeof(pclSqlBuf));
    memset(pclSqlData,0,sizeof(pclSqlData));
    sprintf(pclSqlBuf,"SELECT APC4 FROM APTTAB WHERE APC3='%s'",rlAft->dap3Daily);
    dbg(TRACE,"%s pclSqlBuf<%s>",pclFunc,pclSqlBuf);
    ilRc = RunSQL(pclSqlBuf,pclSqlData);
    if( ilRc == DB_SUCCESS )
    {
        TrimRight(pclSqlData);
        memset(rlAft->dap4Daily,0,sizeof(rlAft->dap4Daily));
        dbg(TRACE,"%s Executes successfully<%s>pclSqlData<%s>",pclFunc,pclSqlBuf,pclSqlData);
        strcpy(rlAft->dap4Daily,pclSqlData);
    }
    else if(ilRc == ORA_NOT_FOUND)
    {
        dbg(TRACE,"%s RunSQL<%s> Not found",pclFunc,pclSqlBuf);
    }
    else
    {
        dbg(TRACE,"%s RunSQL Error<%s>",pclFunc,pclSqlBuf);
    }
  
    memset(pclSqlBuf,0,sizeof(pclSqlBuf));
    memset(pclSqlData,0,sizeof(pclSqlData));

    if ( strcmp(rlAft->ALC2, " ") )
        sprintf(pclSqlBuf,"SELECT ALFN FROM ALTTAB WHERE ALC2='%s'",rlAft->ALC2);
    else if ( strcmp(rlAft->ALC3, " ") )
        sprintf(pclSqlBuf,"SELECT ALFN FROM ALTTAB WHERE ALC3='%s'",rlAft->ALC3);
  
    dbg(TRACE,"%s pclSqlBuf<%s>",pclFunc,pclSqlBuf);
    if ( strcmp(pclSqlBuf, "") )
    {
        ilRc = RunSQL(pclSqlBuf,pclSqlData);
        if( ilRc == DB_SUCCESS )
        {
    	    TrimRight(pclSqlData);
            memset(rlAft->alfn,0,sizeof(rlAft->alfn));
            dbg(TRACE,"%s Executes successfully<%s>pclSqlData<%s>",pclFunc,pclSqlBuf,pclSqlData);
            ConvertDbStringToClient(pclSqlData);
            strcpy(rlAft->alfn,pclSqlData);
        }
        else if(ilRc == ORA_NOT_FOUND)
        {
            dbg(TRACE,"%s RunSQL<%s> Not found",pclFunc,pclSqlBuf);
        }
        else
        {
            dbg(TRACE,"%s RunSQL Error<%s>",pclFunc,pclSqlBuf);
        }
    }
      
  strncpy(pclTmpFlnoCha,rlAft->FLNO,3);TrimRight(pclTmpFlnoCha);
  strcpy(pclTmpFlnoNum,rlAft->FLNO+3);TrimRight(pclTmpFlnoNum);
  memset(rlAft->flno,0,sizeof(rlAft->flno));
  strcpy(rlAft->flno,pclTmpFlnoCha);
  strcat(rlAft->flno,pclTmpFlnoNum);
  
  //FLDA
  if(strlen(rlAft->STOA)!=0 && strncmp(rlAft->STOA," ",1)!=0)
  {
      strncpy(pclTmpDay,rlAft->STOA+6,2);
      strncpy(pclTmpMonth,rlAft->STOA+4,2);
      strncpy(pclTmpYear,rlAft->STOA,4);
      memset(rlAft->flda,0,sizeof(rlAft->flda));
      
      strcpy(rlAft->flda,pclTmpDay);
      strcat(rlAft->flda,"/");
        strcat(rlAft->flda,pclTmpMonth);
      strcat(rlAft->flda,"/");
      strcat(rlAft->flda,pclTmpYear);
    }
    
    //APT3
  memset(rlAft->apt3,0,sizeof(rlAft->apt3));
  strcpy(rlAft->apt3,rlAft->ORG3);
  
  //AARR
  memset(pclTmpDay,0,sizeof(pclTmpDay));
  memset(pclTmpMonth,0,sizeof(pclTmpMonth));
  memset(pclTmpYear,0,sizeof(pclTmpYear));
  if(strlen(rlAft->LAND)!=0 && strncmp(rlAft->LAND," ",1)!=0)
  {
      strncpy(pclTmpDay,rlAft->LAND+6,2);
      strncpy(pclTmpMonth,rlAft->LAND+4,2);
      strncpy(pclTmpYear,rlAft->LAND,4);
      memset(rlAft->aarr,0,sizeof(rlAft->aarr));
      
      convertDate(rlAft->LAND, "YYYYMMDDhhmmss", rlAft->aarr, "DD/MM/YYYY hh:mm"); 
  }
    
  if(strcmp(pclArrOrDep,"Arr")==0)
  {
      strcpy(rlAft->move,"ARRIVAL");
  }
  else if(strcmp(pclArrOrDep,"Dep")==0)
  {
      strcpy(rlAft->move,"DEPARTURE");
  }
    
  //ADEP
  memset(pclTmpDay,0,sizeof(pclTmpDay));
  memset(pclTmpMonth,0,sizeof(pclTmpMonth));
  memset(pclTmpYear,0,sizeof(pclTmpYear));
  if(strlen(rlAft->AIRB)!=0 && strncmp(rlAft->AIRB," ",1)!=0)
  {
      strncpy(pclTmpDay,rlAft->AIRB+6,2);
      strncpy(pclTmpMonth,rlAft->AIRB+4,2);
      strncpy(pclTmpYear,rlAft->AIRB,4);
      memset(rlAft->adep,0,sizeof(rlAft->adep));
         
      convertDate(rlAft->AIRB, "YYYYMMDDhhmmss", rlAft->adep, "DD/MM/YYYY hh:mm"); 
 }
    
    strcpy(rlAft->oper,"SCHEDULED");
  
  //dbg(DEBUG,"%s-------%s<%d>--------",pclFunc,pclArrOrDep,ilCount);
  dbg(DEBUG,"%s-------%s--------",pclFunc,pclArrOrDep);
  dbg(DEBUG,"%s URNO <%s>", pclFunc,rlAft->URNO);
  dbg(DEBUG,"%s RKEY <%s>", pclFunc,rlAft->RKEY);
  dbg(DEBUG,"%s STAT <%s>", pclFunc,rlAft->STAT);
  dbg(DEBUG,"%s ORG4 <%s>", pclFunc,rlAft->ORG4);
  dbg(DEBUG,"%s DES4 <%s>", pclFunc,rlAft->DES4);
  dbg(DEBUG,"%s REGN <%s>", pclFunc,rlAft->REGN);
  dbg(DEBUG,"%s LAND <%s>", pclFunc,rlAft->LAND);
  dbg(DEBUG,"%s AIRB <%s>", pclFunc,rlAft->AIRB);
  dbg(DEBUG,"%s RKEY <%s>", pclFunc,rlAft->RKEY);
  dbg(DEBUG,"%s FLNO <%s>", pclFunc,rlAft->FLNO);
  dbg(DEBUG,"%s STOD <%s>", pclFunc,rlAft->STOD);
  dbg(DEBUG,"%s STOA <%s>", pclFunc,rlAft->STOA);
  dbg(DEBUG,"%s HOPO <%s>", pclFunc,rlAft->HOPO);
  dbg(DEBUG,"%s ALC2 <%s>", pclFunc,rlAft->ALC2);
  dbg(DEBUG,"%s ALC3 <%s>", pclFunc,rlAft->ALC3);
  dbg(DEBUG,"%s VIAL <%s>", pclFunc,rlAft->VIAL);
  dbg(DEBUG,"%s VIAN <%s>", pclFunc,rlAft->VIAN);
  dbg(DEBUG,"%s ACT5 <%s>", pclFunc,rlAft->ACT5);
  dbg(DEBUG,"%s FTYP <%s>", pclFunc,rlAft->FTYP);
  dbg(TRACE,"%s PSTA <%s>", pclFunc,rlAft->PSTA);
  dbg(TRACE,"%s PSTD <%s>", pclFunc,rlAft->PSTD);
  dbg(TRACE,"%s ORG3 <%s>", pclFunc,rlAft->ORG3);
  dbg(TRACE,"%s DES3 <%s>", pclFunc,rlAft->DES3);
  dbg(TRACE,"%s flno <%s>", pclFunc,rlAft->flno);
  dbg(TRACE,"%s flda <%s>", pclFunc,rlAft->flda);
  dbg(TRACE,"%s apt3 <%s>", pclFunc,rlAft->apt3);
  dbg(TRACE,"%s aarr <%s>", pclFunc,rlAft->aarr);
  dbg(TRACE,"%s adep <%s>", pclFunc,rlAft->adep);
  dbg(TRACE,"%s move <%s>", pclFunc,rlAft->move);
  dbg(TRACE,"%s oper <%s>", pclFunc,rlAft->oper);
  dbg(TRACE,"%s alfn <%s>", pclFunc,rlAft->alfn);
  dbg(TRACE,"%s CDAT <%s>", pclFunc,rlAft->CDAT);
  dbg(TRACE,"%s TTYP <%s>", pclFunc,rlAft->TTYP);
  dbg(TRACE,"%s JFNO <%s>", pclFunc,rlAft->JFNO);
  dbg(TRACE,"%s JCNT <%s>", pclFunc,rlAft->JCNT);
  
  dbg(TRACE,"%s ADHO <%s>", pclFunc,rlAft->ADHO);
  dbg(TRACE,"%s ADID <%s>", pclFunc,rlAft->ADID);
  dbg(TRACE,"%s CSGN <%s>", pclFunc,rlAft->CSGN);
  dbg(TRACE,"%s ETDI <%s>", pclFunc,rlAft->ETDI);
  dbg(TRACE,"%s ETAI <%s>", pclFunc,rlAft->ETAI);

  dbg(TRACE,"%s DQCS <%s>", pclFunc,rlAft->DQCS);
  dbg(TRACE,"%s RWYA <%s>", pclFunc,rlAft->RWYA);
  dbg(TRACE,"%s RWYD <%s>", pclFunc,rlAft->RWYD);
  dbg(TRACE,"%s TGA1 <%s>", pclFunc,rlAft->TGA1);
  dbg(TRACE,"%s TGD1 <%s>", pclFunc,rlAft->TGD1);

  dbg(TRACE,"%s STEV <%s>", pclFunc,rlAft->STEV);
  dbg(TRACE,"%s MOPA <%s>", pclFunc,rlAft->MOPA);
  dbg(TRACE,"%s MTOW <%s>", pclFunc,rlAft->MTOW);
  dbg(TRACE,"%s BLTO <%s>", pclFunc,rlAft->BLTO);
  dbg(TRACE,"%s PAID <%s>", pclFunc,rlAft->PAID);
      
  dbg(TRACE,"%s acosDaily <%s>", pclFunc,rlAft->acosDaily);
  dbg(TRACE,"%s sap3Daily <%s>", pclFunc,rlAft->sap3Daily);
  dbg(TRACE,"%s dap3Daily <%s>", pclFunc,rlAft->dap3Daily);
  dbg(TRACE,"%s sap4Daily <%s>", pclFunc,rlAft->sap4Daily);
  dbg(TRACE,"%s dap4Daily <%s>", pclFunc,rlAft->dap4Daily);
  dbg(TRACE,"%s vialDaily <%s>", pclFunc,rlAft->vialDaily);
  dbg(TRACE,"%s sectDaily <%s>", pclFunc,rlAft->sectDaily);
  
  strcpy(rlAft->aflt,rlAft->flno);
  dbg(DEBUG,"%s aflt<%s>",pclFunc,rlAft->aflt);
  
  memset(pclTmpDay,0,sizeof(pclTmpDay));
  memset(pclTmpMonth,0,sizeof(pclTmpMonth));
  memset(pclTmpYear,0,sizeof(pclTmpYear));
  if(strlen(rlAft->STOA)!=0 && strncmp(rlAft->STOA," ",1)!=0)
  {
      strncpy(pclTmpDay,rlAft->STOA+6,2);
      strncpy(pclTmpMonth,rlAft->STOA+4,2);
      strncpy(pclTmpYear,rlAft->STOA,4);
      memset(rlAft->adatBill,0,sizeof(rlAft->adatBill));
      
      strcpy(rlAft->adatBill,pclTmpDay);
      strcat(rlAft->adatBill,"/");
        strcat(rlAft->adatBill,pclTmpMonth);
      strcat(rlAft->adatBill,"/");
      strcat(rlAft->adatBill,pclTmpYear);
    }
    dbg(DEBUG,"%s adatBill<%s>",pclFunc,rlAft->adatBill);
    
    strcpy(rlAft->adatDaily,rlAft->adatBill);
    dbg(DEBUG,"%s adatDaily<%s>",pclFunc,rlAft->adatDaily);
    
    memset(pclTmpDay,0,sizeof(pclTmpDay));
  memset(pclTmpMonth,0,sizeof(pclTmpMonth));
  memset(pclTmpYear,0,sizeof(pclTmpYear));
  if(strlen(rlAft->STOD)!=0 && strncmp(rlAft->STOD," ",1)!=0)
  {
      strncpy(pclTmpDay,rlAft->STOD+6,2);
      strncpy(pclTmpMonth,rlAft->STOD+4,2);
      strncpy(pclTmpYear,rlAft->STOD,4);
         memset(rlAft->ddatBill,0,sizeof(rlAft->ddatBill));
      
      strcpy(rlAft->ddatBill,pclTmpDay);
      strcat(rlAft->ddatBill,"/");
        strcat(rlAft->ddatBill,pclTmpMonth);
      strcat(rlAft->ddatBill,"/");
      strcat(rlAft->ddatBill,pclTmpYear);
    }
    dbg(DEBUG,"%s ddatBill<%s>",pclFunc,rlAft->ddatBill);
    
    //strcpy(rlAft->umvnBill,rlAft->URNO);
    strcpy(rlAft->mmvnBill,"M");
    strcat(rlAft->mmvnBill,rlAft->URNO);
    //dbg(DEBUG,"%s umvnBill<%s>",pclFunc,rlAft->umvnBill);
    dbg(DEBUG,"%s mmvnBill<%s>",pclFunc,rlAft->mmvnBill);
    
  if(strlen(rlAft->STOA)!=0 && strncmp(rlAft->STOA," ",1)!=0)
  {
      strcpy(rlAft->sttaBill,rlAft->STOA+8);
      if((*(rlAft->sttaBill))=='0')
      {
          memset(rlAft->sttaBill,0,sizeof(rlAft->sttaBill));
          strcpy(rlAft->sttaBill,rlAft->STOA+9);
      }
  }
  dbg(DEBUG,"%s sttaBill<%s>",pclFunc,rlAft->sttaBill);
  
  if(strlen(rlAft->STOD)!=0 && strncmp(rlAft->STOD," ",1)!=0)
  {
      strcpy(rlAft->sttdBill,rlAft->STOD+8);
      if((*(rlAft->sttdBill))=='0')
      {
          memset(rlAft->sttdBill,0,sizeof(rlAft->sttdBill));
          strcpy(rlAft->sttdBill,rlAft->STOD+9);
      }
  }
  dbg(DEBUG,"%s sttdBill<%s>",pclFunc,rlAft->sttdBill);
  
  strcpy(rlAft->sttaDaily,rlAft->sttaBill);
  dbg(DEBUG,"%s sttaDaily<%s>",pclFunc,rlAft->sttaDaily);
  
  if(strlen(rlAft->LAND)!=0 && strncmp(rlAft->LAND," ",1)!=0)
  {
      strcpy(rlAft->aarrBill,rlAft->LAND+8);
      if((*(rlAft->aarrBill))=='0')
      {
          memset(rlAft->aarrBill,0,sizeof(rlAft->aarrBill));
          strcpy(rlAft->aarrBill,rlAft->LAND+9);
      }
  }
  dbg(DEBUG,"%s aarrBill<%s>",pclFunc,rlAft->aarrBill);
  
  if(strlen(rlAft->AIRB)!=0 && strncmp(rlAft->AIRB," ",1)!=0)
  {
      strcpy(rlAft->adepBill,rlAft->AIRB+8);
      if((*(rlAft->adepBill))=='0')
      {
          memset(rlAft->adepBill,0,sizeof(rlAft->adepBill));
          strcpy(rlAft->adepBill,rlAft->AIRB+9);
      }
  }
  dbg(DEBUG,"%s adepBill<%s>",pclFunc,rlAft->adepBill);
  /*
  memset(pclTmpDay,0,sizeof(pclTmpDay));
  memset(pclTmpMonth,0,sizeof(pclTmpMonth));
  memset(pclTmpYear,0,sizeof(pclTmpYear));
  if(strlen(rlAft->LAND)!=0 && strncmp(rlAft->LAND," ",1)!=0)
  {
      strncpy(pclTmpDay,rlAft->LAND+6,2);
      strncpy(pclTmpMonth,rlAft->LAND+4,2);
      strncpy(pclTmpYear,rlAft->LAND,4);
         memset(rlAft->ddatBill,0,sizeof(rlAft->ddatBill));
      
      strcpy(rlAft->aarrBill,pclTmpDay);
      strcat(rlAft->aarrBill,"/");
        strcat(rlAft->aarrBill,pclTmpMonth);
      strcat(rlAft->aarrBill,"/");
      strcat(rlAft->aarrBill,pclTmpYear);
  }
  dbg(DEBUG,"%s aarrBill<%s>",pclFunc,rlAft->aarrBill);
  */ 
    GetValueFromMemory(rlAft->TTYP,rgTM_Bill.CommCfgPart,rlAft->cateBill,"FltCat");
    dbg(DEBUG,"%s cateBill<%s>",pclFunc,rlAft->cateBill);
    
    GetValueFromMemory(rlAft->TTYP,rgTM_Bill.CommCfgPart,rlAft->typeBill,"FltTyp");
    dbg(DEBUG,"%s typeBill<%s>",pclFunc,rlAft->typeBill);
    
    GetValueFromMemory(rlAft->TTYP,rgTM_Daily.CommCfgPart,rlAft->cateDaily,"FltCat");
    dbg(DEBUG,"%s cateDaily<%s>",pclFunc,rlAft->cateDaily);
    
    if(strcmp(rlAft->cateDaily,"SCHEDULED")==0)
    {
        strcpy(rlAft->sflgDaily,"1");
    }
    else if(strcmp(rlAft->cateDaily,"UNSCHEDULED")==0)
    {
        strcpy(rlAft->sflgDaily,"2");
    }
    
    if(strlen(rlAft->AIRB)!=0 && strncmp(rlAft->AIRB," ",1)!=0)
  {
        strncpy(rlAft->airbMonth,rlAft->AIRB+8,4);
        dbg(DEBUG,"%s airbMonth<%s>",pclFunc,rlAft->airbMonth);
    }
    
    if(strlen(rlAft->LAND)!=0 && strncmp(rlAft->LAND," ",1)!=0)
  {
        strncpy(rlAft->landMonth,rlAft->LAND+8,4);
        dbg(DEBUG,"%s landMonth<%s>",pclFunc,rlAft->landMonth);
    }
    
    strcpy(rlAft->statMonth,"E");
    dbg(DEBUG,"%s statMonth<%s>",pclFunc,rlAft->statMonth);
    
    if(strlen(rlAft->AIRB)!=0 && strncmp(rlAft->AIRB," ",1)!=0)
    {
    	convertDate(rlAft->AIRB, "YYYYMMDD", rlAft->airbBill, "DD/MM/YYYY"); 
        dbg(DEBUG,"%s airbBill<%s>",pclFunc,rlAft->airbBill);
    }
    
    if(strlen(rlAft->LAND)!=0 && strncmp(rlAft->LAND," ",1)!=0)
    {
  	    convertDate(rlAft->LAND, "YYYYMMDD", rlAft->landBill, "DD/MM/YYYY"); 
        dbg(DEBUG,"%s landBill<%s>",pclFunc,rlAft->landBill);
        
        strcpy(rlAft->landAdHoc,rlAft->landBill);
        dbg(DEBUG,"%s landAdHoc<%s>",pclFunc,rlAft->landAdHoc);
    }
    
    memset(pclSqlBuf,0,sizeof(pclSqlBuf));
    memset(pclSqlData,0,sizeof(pclSqlData));
    memset(rlAft->mtowBill,0,sizeof(rlAft->mtowBill));
    if(strlen(rlAft->REGN)!=0 && strncmp(rlAft->REGN," ",1)!=0)
    {
        sprintf(pclSqlBuf,"SELECT MTOW FROM ACRTAB WHERE REGN='%s'",rlAft->REGN);
        dbg(TRACE,"%s pclSqlBuf<%s>",pclFunc,pclSqlBuf);
        ilRc = RunSQL(pclSqlBuf,pclSqlData);
        if( ilRc == DB_SUCCESS )
        {
            TrimRight(pclSqlData);
            dbg(DEBUG,"%s pclSqlData<%s>",pclFunc,pclSqlData);
            
            strcpy(rlAft->mtowBill,pclSqlData);
            dbg(TRACE,"%s Executes successfully<%s>pclSqlData<%s>",pclFunc,pclSqlBuf,pclSqlData);
        }
        else if(ilRc == ORA_NOT_FOUND)
        {
            dbg(TRACE,"%s RunSQL<%s> Not found",pclFunc,pclSqlBuf);
        }
        else
        {
            dbg(TRACE,"%s RunSQL Error<%s>",pclFunc,pclSqlBuf);
        }
  }
  else
  {
      strcpy(rlAft->mtowBill,"0");
  }
  //(rlAft->mtowBill) = atof(rlAft->mtowBill)*1000;
  dbg(DEBUG,"%s mtowBill<%s>strlen(mtowBill)<%d>",pclFunc,rlAft->mtowBill,strlen(rlAft->mtowBill));
    
    
    strcpy(rlAft->scheDaily,"SCHEDULED");
    dbg(DEBUG,"%s scheDaily<%s>",pclFunc,rlAft->scheDaily);
    
    strcpy(rlAft->cateAdHoc,"NS");
    dbg(DEBUG,"%s cateAdHoc<%s>",pclFunc,rlAft->cateAdHoc);
    
    memset(rlAft->airbAdHoc,0,sizeof(rlAft->airbAdHoc));
    
    if(strlen(rlAft->ACT3)!=0 && strncmp(rlAft->ACT3," ",1)!=0)
    {
        iTmp=getACTTAB(rlAft->ACT3, &stAct);
        printACTTAB(&stAct);
        
      if ( !strcmp(stAct.ACBT, "N") )
          strcpy(rlAft->acszBill, "Narrowbody");
      else if ( !strcmp(stAct.ACBT, "W") )
          strcpy(rlAft->acszBill, "Widebody");
      else
          strcpy(rlAft->acszBill, "");
    }
}

static void GetValueFromMemory(char *pclTtyp,CommonCfg CommCfgPart,char *pclDest,char *pclCategory)
{
    char *pclFunc = "GetValueFromMemory";
    int ilCurrentSection=0;
    
     
    if(strcmp(pclCategory,"FltCat")==0)
    {
        dbg(DEBUG, "FLTCATE [%d]", CommCfgPart.iNoOfSections_FltCat);
        for(ilCurrentSection=0; ilCurrentSection < CommCfgPart.iNoOfSections_FltCat; ilCurrentSection++)
        {
            if(strstr(CommCfgPart.prSection_FltCat[ilCurrentSection].pcRange,pclTtyp)!=0)
            {
                strcpy(pclDest,CommCfgPart.prSection_FltCat[ilCurrentSection].pcMark);
                dbg(DEBUG,"%s pclDest<%s>",pclFunc,pclDest);
                break;
            }
        }
    }
    else if(strcmp(pclCategory,"FltTyp")==0)
    {
        for(ilCurrentSection=0; ilCurrentSection < CommCfgPart.iNoOfSections_FltTyp; ilCurrentSection++)
        {
            if(strstr(CommCfgPart.prSection_FltTyp[ilCurrentSection].pcRange,pclTtyp)!=0)
            {
                strcpy(pclDest,CommCfgPart.prSection_FltTyp[ilCurrentSection].pcMark);
                dbg(DEBUG,"%s pclDest<%s>",pclFunc,pclDest);
                break;
            }
        }
    }
}

int getPairedFlights(char *strUrno, AFTREC *stArr, AFTREC *stDep)
{
    int iRet=RC_SUCCESS;
    int iTmp;
    AFTREC stTmp;
    char strSql[2048];
    short slCursor;
    char pclDataArea[8192];
    char strAdid[32];
   
    dbg(DEBUG, "getPairedFlights:: Urno[%s]", strUrno);
   
    memset(stArr, 0, sizeof(AFTREC));
    memset(stDep, 0, sizeof(AFTREC));

    sprintf(strSql, "SELECT %s FROM afttab WHERE rkey='%s' AND adid='A' ", pcgAftFields, strUrno);
    slCursor = 0;
    memset(pclDataArea, 0, sizeof(pclDataArea));
    iTmp=sql_if(START, &slCursor, strSql, pclDataArea);    
    dbg(DEBUG, "getPairedFlights:: selecting the paired flight Arrival [%s][%d]", strSql, iTmp);
    
    if ( iTmp==DB_SUCCESS )
    {
        BuildItemBuffer(pclDataArea, pcgAftFields, igAftFields, ",");  
        get_real_item(strAdid,pclDataArea,igAftAdid);
        GetFieldAndShow("Arr", stArr, 1 ,pclDataArea);
        stArr->iFound=1;
        
        if ( strcmp(stArr->STOA, pcgImplementationDate)<0 )
        {    
        	dbg(TRACE, "getPairedFlights:: Arrival Flight STOA[%s] < Implementation Date[%s]", 
        	    stArr->STOA, pcgImplementationDate); 	
            iTmp=RC_FAIL;	
        }
    }
    close_my_cursor(&slCursor);
   
    if ( iTmp!=RC_FAIL )
    {
        sprintf(strSql,"SELECT %s FROM afttab WHERE rkey='%s' AND adid='D' ", pcgAftFields, strUrno);                
       
        dbg(DEBUG, "getPairedFlights:: selecting the paired flight Departure[%s] ", strSql);     
        slCursor = 0;
        memset(pclDataArea, 0, sizeof(pclDataArea));
        iTmp=sql_if(START, &slCursor, strSql, pclDataArea);
        if ( iTmp==DB_SUCCESS )
        {
            BuildItemBuffer(pclDataArea, NULL, igAftFields, ","); 
            GetFieldAndShow("Dep", stDep, 1 ,pclDataArea);
            stDep->iFound=1;
            if ( strcmp(stDep->STOD, pcgImplementationDate)<0 )
            {    
                dbg(TRACE, "getPairedFlights:: Departure Flight STOD[%s] < Implementation Date[%s]", 
        	        stArr->STOD, pcgImplementationDate); 	
                iTmp=RC_FAIL;	
            }
        }
        close_my_cursor(&slCursor);   
    }
   
    if ( iTmp==RC_FAIL )
        iRet=RC_FAIL;
       
    dbg(DEBUG, "getPairedFlights:: Arrival Urno[%s], Departure Urno[%s], Result[%d]",
        stArr->URNO, stDep->URNO, iRet);
    return iRet;   
}

int populateERPSCH(ERPSCHREC *stERP, AFTREC *stAftArr, AFTREC *stAftDep)
{
    int iRet=RC_SUCCESS;
    int iUrno;
    struct tm tmTmp;
    int iFlightFlag=0;
    char strWhere[1024];
    char strTmp[1024];
    char pclErr[513];
    int a=0;
    char *strPtr;
    AFTREC *stDefault;
    FILE *fptr;

    memset(stERP, 0, sizeof(ERPSCHREC));
    if ( stAftDep->iFound )
    {
        iFlightFlag|=1;
        stDefault=stAftDep;
    }
    
    if ( stAftArr->iFound )
    {
        iFlightFlag|=2;
        stDefault=stAftArr;
    }  
    dbg(DEBUG, "populateERPSCH:: Arrival Urno[%s], Departure Urno[%s], iFlightFlag[%d]", 
        stAftArr->URNO, stAftDep->URNO, iFlightFlag);    

    if ( iFlightFlag==0 )
    {
        dbg(TRACE, "poputalteERPSCH:: ERROR!!! No URNO to process.");	
        iRet=RC_FAIL;
    }
    else
    {
    	strcpy(stERP->FLNU, stDefault->URNO);  
        strcpy(stERP->HOPO, cgHopo);  
        strcpy(stERP->ALC2, stDefault->ALC2);                
        strcpy(stERP->ALFN, stDefault->alfn);               
        strcpy(stERP->ALC3, stDefault->ALC3);
                       
        removeSpace(stAftArr->FLNO, strTmp);
        strcpy(stERP->AFLT, strTmp);   
        if ( !strcmp(stERP->AFLT, "") )
            strcpy(stERP->AFLT, stAftArr->CSGN);   
                
        removeSpace(stAftDep->FLNO, strTmp);         
        strcpy(stERP->DFLT, strTmp);
        if ( !strcmp(stERP->DFLT, "") ) 
            strcpy(stERP->DFLT, stAftDep->CSGN);
                 
        strcpy(stERP->ACOS, stAftArr->acosDaily);
        strcpy(stERP->DCOS, stAftDep->acosDaily);
                             	
        if ( iFlightFlag==DEPARTUREFLIGHT )
        {
            strcpy(strTmp, stAftDep->STOD); 
            AddSecondsToCEDATime(strTmp, igTimeZone*60*60, 1);                
            getDayOfWeek(strTmp, stERP->FDAY);
            convertDate(strTmp, "YYYYMMDD", stERP->ADAT, "DD/MM/YYYY");  
            sprintf(stERP->STTD, "%d", convertTimeToSec(&strTmp[8]));                 
            convertDate(strTmp, "YYYYMMDD", stERP->DDAT, "DD/MM/YYYY"); 
                        
            strcpy(stERP->DAP3, stAftDep->DES3);
            strcpy(stERP->DAP4, stAftDep->DES4);
                    
            formatVIAL(atoi(stAftDep->VIAN), stAftDep->VIAL, stERP->VIAL, ".");
            sprintf(stERP->SECT,"%s-%s", stAftDep->ORG3, stAftDep->DES3);  
            strcpy(stERP->STOP, "Turnaround");  
                
            convertDate(stAftDep->STOD, "YYYYMMDDhhmmss", stERP->STOD, "YYYYMMDDhhmm");              
                
            formatVIAL(atoi(stAftDep->VIAN), stAftDep->VIAL, strTmp, "-");                
            if ( !strcmp(strTmp, "") )
                sprintf(stERP->FULL, "%s-%s", 
                        stAftDep->ORG3, stAftDep->DES3);                	
            else
                sprintf(stERP->FULL, "%s-%s/%s", 
                        stAftDep->ORG3, strTmp, stAftDep->DES3);
                
        }
        else if ( iFlightFlag==ARRIVALFLIGHT )
        {
            strcpy(strTmp, stAftArr->STOA); 
            AddSecondsToCEDATime(strTmp, igTimeZone*60*60, 1);                
            getDayOfWeek(strTmp, stERP->FDAY);
            convertDate(strTmp, "YYYYMMDD", stERP->ADAT, "DD/MM/YYYY");            
            sprintf(stERP->STTA, "%d", convertTimeToSec(&strTmp[8]));
                    
            strcpy(stERP->SAP3, stAftArr->ORG3);
            strcpy(stERP->SAP4, stAftArr->ORG4);  
                    
            formatVIAL(atoi(stAftArr->VIAN), stAftArr->VIAL, stERP->VIAL, ".");
            sprintf(stERP->SECT,"%s-%s", stAftArr->ORG3, stAftArr->DES3);
            strcpy(stERP->STOP, "Turnaround");
                
            convertDate(stAftArr->STOA, "YYYYMMDDhhmmss", stERP->STOA, "YYYYMMDDhhmm");    
                
            formatVIAL(atoi(stAftArr->VIAN), stAftArr->VIAL, strTmp, "-");
            if ( !strcmp(strTmp, "") )
                sprintf(stERP->FULL, "%s-%s", 
                        stAftArr->ORG3, stAftArr->DES3);                	
            else
                sprintf(stERP->FULL, "%s/%s-%s", 
                        stAftArr->ORG3, strTmp, stAftArr->DES3);
        }
        else
        {
            strcpy(strTmp, stAftArr->STOA); 
            AddSecondsToCEDATime(strTmp, igTimeZone*60*60, 1);                
            getDayOfWeek(strTmp, stERP->FDAY);
            convertDate(strTmp, "YYYYMMDD", stERP->ADAT, "DD/MM/YYYY"); 
            sprintf(stERP->STTA, "%d", convertTimeToSec(&strTmp[8]));
                    
            strcpy(strTmp, stAftDep->STOD); 
            AddSecondsToCEDATime(strTmp, igTimeZone*60*60, 1);  
            sprintf(stERP->STTD, "%d", convertTimeToSec(&strTmp[8]));
            convertDate(strTmp, "YYYYMMDD", stERP->DDAT, "DD/MM/YYYY"); 
                 
            strcpy(stERP->SAP3, stAftArr->ORG3);  
            strcpy(stERP->DAP3, stAftDep->DES3);
            strcpy(stERP->SAP4, stAftArr->ORG4);  
            strcpy(stERP->DAP4, stAftDep->DES4);
                    
            formatVIAL(atoi(stAftArr->VIAN), stAftArr->VIAL, stERP->VIAL, ".");       
            sprintf(stERP->SECT,"%s-AUH-%s", stAftArr->ORG3, stAftDep->DES3);     
                
            if ( !strcmp(stAftArr->FLNO, stAftDep->FLNO) && strcmp(stAftArr->ORG3, stAftDep->DES3) )
                strcpy(stERP->STOP, "Transit");    
            else
                strcpy(stERP->STOP, "Turnaround");   
                
            convertDate(stAftArr->STOA, "YYYYMMDDhhmmss", stERP->STOA, "YYYYMMDDhhmm"); 
            convertDate(stAftDep->STOD, "YYYYMMDDhhmmss", stERP->STOD, "YYYYMMDDhhmm"); 
                
            formatVIAL(atoi(stAftArr->VIAN), stAftArr->VIAL, strTmp, "-");
            if ( !strcmp(strTmp, "") )                	
                sprintf(stERP->FULL, "%s-%s", stAftArr->ORG3,stAftArr->DES3);
            else
                sprintf(stERP->FULL, "%s/%s-%s", 
                        stAftArr->ORG3, strTmp, stAftArr->DES3);

            formatVIAL(atoi(stAftDep->VIAN), stAftDep->VIAL, strTmp, "-");
            if ( !strcmp(strTmp, "") )
                sprintf(stERP->FULL, "%s-%s", stERP->FULL, stAftDep->DES3);
            else
                sprintf(stERP->FULL, "%s-%s/%s", stERP->FULL, strTmp, stAftDep->DES3); 
                    
            strcpy(stERP->DEPU, stAftDep->URNO);   
        }

        strcpy(stERP->ACT5, stDefault->ACT5);                   
        getFlightCategory(stERP->CATE, stDefault->TTYP, &rgERPSCHConfig.rlFlightCat);      
        strcpy(stERP->SCHE, "SCHEDULED");   

        if ( stDefault->FTYP[0]=='X' || stDefault->FTYP[0]=='N' )
        {
            double dDiff=0;
                
            strcpy(stERP->FTYP, "CANCELLED");
            if ( iFlightFlag==DEPARTUREFLIGHT )
                timeDiff(stAftDep->STOD, "", &dDiff);
            else 
                timeDiff(stAftArr->STOA, "", &dDiff);
                    
            if ( dDiff<0 )
                strcpy(stERP->CNTY,"Without Notice");   
            else if ( dDiff<=(60*60*4) )
                strcpy(stERP->CNTY,"Within 4 hours of STA");
            else if ( dDiff<=(60*60*8) )
                strcpy(stERP->CNTY,"Within 8 hours of STA");
            else if ( dDiff<=(60*60*12) )
                strcpy(stERP->CNTY,"Within 12 hours of STA");
            else
                strcpy(stERP->CNTY,"Without Notice");   
        }
        strcpy(stERP->SFLG, "1"); 

        strcpy(strWhere, "WHERE");
        if ( iFlightFlag==DEPARTUREFLIGHT )
        {
            if ( !strcmp(stERP->DFLT, "") )
                sprintf(strWhere, "%s DFLT IS NULL", strWhere);
            else
                sprintf(strWhere, "%s DFLT='%s'", strWhere, stERP->DFLT);
        
            if ( !strcmp(stERP->DDAT, "") )
                sprintf(strWhere, "%s AND DDAT IS NULL", strWhere);
            else
                sprintf(strWhere, "%s AND DDAT='%s'", strWhere, stERP->DDAT);
            
            if ( !strcmp(stERP->STTD, "") )
                sprintf(strWhere, "%s AND STTD IS NULL", strWhere);
            else
                sprintf(strWhere, "%s AND STTD='%s'", strWhere, stERP->STTD);
        	
        }
        else
        {
            if ( !strcmp(stERP->AFLT, "") )
                sprintf(strWhere, "%s AFLT IS NULL", strWhere);
            else
                sprintf(strWhere, "%s AFLT='%s'", strWhere, stERP->AFLT);
        
            if ( !strcmp(stERP->ADAT, "") )
                sprintf(strWhere, "%s AND ADAT IS NULL", strWhere);
            else
                sprintf(strWhere, "%s AND ADAT='%s'", strWhere, stERP->ADAT);
            
            if ( !strcmp(stERP->STTA, "") )
                sprintf(strWhere, "%s AND STTA IS NULL", strWhere);
            else
                sprintf(strWhere, "%s AND STTA='%s'", strWhere, stERP->STTA);
        }
        
        iRet=getERPSCH(strWhere, stERP);
        if ( iRet==RC_SUCCESS )
        {        	
            if ( stERP->iFound )
            {
                if ( 
            		  strcmp(stERP->STAT, "R")  &&
            		  strcmp(stERP->STAT, "F")
            	   )	
            	{
                    strcpy(stERP->STAT, "U");
                }
            }
            else
            {
                iUrno=NewUrnos("ERPSCH",1);                
                sprintf(stERP->URNO, "%d", iUrno);     
                strcpy(stERP->STAT, "R");         	
            }   
        }
    }
    
    dbg(DEBUG, "populateERPSCH:: Arrival Urno[%s], Departure Urno[%s], iFlightFlag[%d], Result[%d]", 
        stAftArr->URNO, stAftDep->URNO, iFlightFlag, iRet);       
    return iRet;    
}

void printERPSCH(ERPSCHREC *stERP)
{
    dbg(DEBUG, "printERPSCH:: Start");
   
    dbg(DEBUG, "printERPSCH:: iFound[%d]", stERP->iFound);
    dbg(DEBUG, "printERPSCH:: URNO[%s]", stERP->URNO);
    dbg(DEBUG, "printERPSCH:: FLNU[%s]", stERP->FLNU);
    dbg(DEBUG, "printERPSCH:: STAT[%s]", stERP->STAT);
    dbg(DEBUG, "printERPSCH:: HOPO[%s]", stERP->HOPO);
    
    dbg(DEBUG, "printERPSCH:: ALC2[%s]", stERP->ALC2);
    dbg(DEBUG, "printERPSCH:: ALFN[%s]", stERP->ALFN);
    dbg(DEBUG, "printERPSCH:: ALC3[%s]", stERP->ALC3);
    dbg(DEBUG, "printERPSCH:: FDAY[%s]", stERP->FDAY);
    dbg(DEBUG, "printERPSCH:: AFLT[%s]", stERP->AFLT);
    
    dbg(DEBUG, "printERPSCH:: DFLT[%s]", stERP->DFLT);
    dbg(DEBUG, "printERPSCH:: ACOS[%s]", stERP->ACOS);
    dbg(DEBUG, "printERPSCH:: DCOS[%s]", stERP->DCOS);
    dbg(DEBUG, "printERPSCH:: SAP3[%s]", stERP->SAP3);
    dbg(DEBUG, "printERPSCH:: DAP3[%s]", stERP->DAP3);
    
    dbg(DEBUG, "printERPSCH:: SAP4[%s]", stERP->SAP4);    
    dbg(DEBUG, "printERPSCH:: DAP4[%s]", stERP->DAP4);
    dbg(DEBUG, "printERPSCH:: VIAL[%s]", stERP->VIAL);
    dbg(DEBUG, "printERPSCH:: ADAT[%s]", stERP->ADAT);
    dbg(DEBUG, "printERPSCH:: STTA[%s]", stERP->STTA);
    
    dbg(DEBUG, "printERPSCH:: STTD[%s]", stERP->STTD);
    dbg(DEBUG, "printERPSCH:: ACT5[%s]", stERP->ACT5);
    dbg(DEBUG, "printERPSCH:: SECT[%s]", stERP->SECT);
    dbg(DEBUG, "printERPSCH:: CATE[%s]", stERP->CATE);
    dbg(DEBUG, "printERPSCH:: SCHE[%s]", stERP->SCHE);
    
    dbg(DEBUG, "printERPSCH:: STOP[%s]", stERP->STOP);    
    dbg(DEBUG, "printERPSCH:: FTYP[%s]", stERP->FTYP);
    dbg(DEBUG, "printERPSCH:: CNTY[%s]", stERP->CNTY);
    dbg(DEBUG, "printERPSCH:: ADAT[%s]", stERP->ADAT);
    dbg(DEBUG, "printERPSCH:: SFLG[%s]", stERP->SFLG);

    dbg(DEBUG, "printERPSCH:: STOA[%s]", stERP->STOA);
    dbg(DEBUG, "printERPSCH:: STOD[%s]", stERP->STOD);
    dbg(DEBUG, "printERPSCH:: FULL[%s]", stERP->FULL);
    
    dbg(DEBUG, "printERPSCH:: DEPU[%s]", stERP->DEPU);

    dbg(DEBUG, "printERPSCH:: End");
    return;        
}

int updateERPSCH(const ERPSCHREC *st)
{
    int iRet=RC_SUCCESS;
    char strSql[32768];
    char strErr[513];

    dbg(DEBUG, "updateERPSCH:: Start");
    sprintf(strSql, "UPDATE erpsch "
                    "SET STAT='%s'"
                    ", HOPO='%s'"
                    ", ALC2='%s'"
                    ", ALFN='%s'"
                    ", ALC3='%s'"
                    
                    ", FDAY='%s'"
                    ", AFLT='%s'"
                    ", DFLT='%s'"
                    ", ACOS='%s'"
                    ", DCOS='%s'"
                    
                    ", SAP3='%s'"
                    ", DAP3='%s'"
                    ", SAP4='%s'"
                    ", DAP4='%s'"
                    ", VIAL='%s'"

                    ", ADAT='%s'"
                    ", STTA='%s'"
                    ", STTD='%s'"
                    ", ACT5='%s'"
                    ", SECT='%s'"

                    ", CATE='%s'"
                    ", SCHE='%s'"
                    ", STOP='%s'"
                    ", FTYP='%s'"
                    ", CNTY='%s'"

                    ", SFLG='%s'"
                    ", DDAT='%s'"
                    ", STOA='%s'"
                    ", STOD='%s'"
                    ", FULL='%s'"
                    
                    ", DEPU='%s' "
                    ", FLNU='%s' " 
                    "WHERE URNO='%s' ",
                    st->STAT, st->HOPO, st->ALC2, st->ALFN, st->ALC3, 
                    st->FDAY, st->AFLT, st->DFLT, st->ACOS, st->DCOS, 
                    st->SAP3, st->DAP3, st->SAP4, st->DAP4, st->VIAL, 
                    st->ADAT, st->STTA, st->STTD, st->ACT5, st->SECT,
                    st->CATE, st->SCHE, st->STOP, st->FTYP, st->CNTY,
                    st->SFLG, st->DDAT, st->STOA, st->STOD, st->FULL,
                    st->DEPU, st->FLNU,
                    st->URNO);    
    dbg(DEBUG, "updateERPSCH:: SQL[%s]", strSql);                                                                                                                                                                               
     
    iRet=executeSql(strSql,strErr);               

    return iRet;    
}

int insertERPSCH(const ERPSCHREC *st)
{
    int iRet=RC_SUCCESS;
    char strSql[32768];
    char strErr[513];
    
    memset(strSql, 0, sizeof(strSql));
    sprintf(strSql, "INSERT INTO erpsch "
                   "("
                   "%s"
                   ")", pcgErpSchFields);
    sprintf(strSql, "%s VALUES "
                    "("
                    "'%s', '%s', '%s', '%s', '%s',"
                    "'%s', '%s', '%s', '%s', '%s',"
                    "'%s', '%s', '%s', '%s', '%s',"
                    "'%s', '%s', '%s', '%s', '%s',"
                    "'%s', '%s', '%s', '%s', '%s',"
                    "'%s', '%s', '%s', '%s', '%s',"
                    "'%s', '%s', '%s')",
            strSql, 
            st->URNO, st->FLNU, st->STAT, st->HOPO, st->ALC2,    
            st->ALFN, st->ALC3, st->FDAY, st->AFLT, st->DFLT,     
            st->ACOS, st->DCOS, st->SAP3, st->DAP3, st->SAP4,   
            st->DAP4, st->VIAL, st->ADAT, st->STTA, st->STTD,  
            st->ACT5, st->SECT, st->CATE, st->SCHE, st->STOP,
            st->FTYP, st->CNTY, st->SFLG, st->DDAT, st->STOA,
            st->STOD, st->FULL, st->DEPU);    
            
    iRet=executeSql(strSql,strErr);               

    return iRet;    
}

int getERPSCH(char *pcpWhere, ERPSCHREC *stERP)
{
    int iRet=RC_SUCCESS;
    short slCursor = 0;
    char strSql[2048];
    char pclDataBuf[32728];
        
    dbg(DEBUG, "getERPSCH:: pcpWhere[%s]", pcpWhere);

    sprintf(strSql, "SELECT %s FROM erpsch %s", pcgErpSchFields, pcpWhere);
    dbg(DEBUG, "getERPSCH:: executing [%s]", strSql);
    
    memset(pclDataBuf, 0, sizeof(pclDataBuf));
    iRet=sql_if (START, &slCursor, strSql, pclDataBuf);  

    if ( iRet==DB_SUCCESS )
    {
        BuildItemBuffer (pclDataBuf, pcgErpSchFields, igErpSchFields, ",");
        stERP->iFound=1;        
        get_real_item(stERP->URNO,pclDataBuf,1);      
        get_real_item(stERP->STAT,pclDataBuf,3);   
    }
    else if ( iRet==1 )
    {
        dbg(DEBUG, "getERPSCH:: Record not found");
        iRet=RC_SUCCESS;
        stERP->iFound=0;
    }
    else if ( iRet==-1 )
    {
        dbg(DEBUG, "getERPSCH:: ERROR!!! DB Failed");
        stERP->iFound=0;
    }
    close_my_cursor(&slCursor);
        
    dbg(DEBUG, "getERPSCH:: pcpWhere[%s] Result[%d]", pcpWhere, iRet);
    return iRet;
}

int isAdhoc(char *pcpUrno, char *pcpAdho, char *pcpPaid)
{
    int ilRC=0;
    
    if ( !strcmp(pcpAdho, "X") || strcmp(pcpPaid, "K") )
        ilRC=1;
    else 
        ilRC=0;
        
    dbg(TRACE, "isAdhoc::pcpUrno[%s], pcpAdho[%s], pcpPaid[%s] Result[%d]", 
        pcpUrno, pcpAdho, pcpPaid, ilRC);
                
    return ilRC;        
}

static int processSCHCmd(char *pcpStoaFrom, char *pcpStoaTo)
{
    int iRet=RC_SUCCESS;
    int iTmp;
    char strSql[2048];
    int ilCount=0;
    int ilFlag=0;
    short slCursor=0;
    char pclDataAreaArr[8192];
    char strUrno[32];
    char strAdid[32];
    char strRkey[1024];
    AFTREC rlAftArr;
    AFTREC rlAftDep;
    ERPSCHREC stErp;
    FILE *fptr;
    int ilAdhocArr=0;
    int ilAdhocDep=0;
            
    dbg(TRACE, "processSCH:: Start");
    dbg(TRACE, "processSCH:: pcpStoaFrom[%s] pcpStoaTo[%s] ",
        pcpStoaFrom, pcpStoaTo);     

    sprintf(strSql, "SELECT DISTINCT(RKEY)RKEY FROM afttab "
                    "WHERE ( (stoa>='%s' AND stoa<'%s') or (stod>='%s' AND stod<'%s') )",
                    pcpStoaFrom, pcpStoaTo,pcpStoaFrom, pcpStoaTo); 

    if( rgTM_Daily.CommCfgPart.iNoOfSections_SelCri>0 )
    {
        for(ilCount=0;ilCount < rgTM_Daily.CommCfgPart.iNoOfSections_SelCri; ilCount++)
        {
            sprintf(strSql,"%s AND %s", strSql, rgTM_Daily.CommCfgPart.prSection_SelCri[ilCount].pcType);
            
            if(strncmp(rgTM_Daily.CommCfgPart.prSection_SelCri[ilCount].pcSpecifier,"NOT",3)==0)
            {
                sprintf(strSql,"%s NOT IN", strSql);
            }
            else
            {
                sprintf(strSql,"%s IN", strSql);
            }
            sprintf(strSql,"%s (%s)",strSql, rgTM_Daily.CommCfgPart.prSection_SelCri[ilCount].pcRange);
        }        
    }
    dbg(DEBUG, "processSCH:: Get Records [%s]", strSql);

    fptr=fopen("/ceda/bin/erpsch-SCH.txt", "w");
    if ( fptr==NULL )
    {
        dbg(TRACE, "processSCH:: ERROR!!! Cannot open file /ceda/bin/erpsch-SCH.txt for writing");
        iRet=RC_FAIL;	
    }
    
    if ( iRet==RC_SUCCESS )
    {
        iTmp=sql_if(START,&slCursor,strSql,pclDataAreaArr);
        while ( iTmp==DB_SUCCESS )
        {
            BuildItemBuffer (pclDataAreaArr, "RKEY", 1, ",");
            get_real_item(strRkey,pclDataAreaArr,1);
            fprintf(fptr, "%s\n", strRkey);
            iTmp=sql_if(NEXT,&slCursor,strSql,pclDataAreaArr);        
        }     
        close_my_cursor(&slCursor);      
        fclose(fptr); 
    }

    fptr=fopen("/ceda/bin/erpsch-SCH.txt", "r");
    if ( fptr==NULL )
    {
        dbg(TRACE, "processSCH:: ERROR!!! Cannot open file /ceda/bin/erpsch-SCH.txt for reading");
        iRet=RC_FAIL;	
    }
    
    if ( iRet==RC_SUCCESS )
    {
    	ilCount=0;
        while ( fgets(strRkey, 1000, fptr) )
        {
            strRkey[strlen(strRkey)-1]=0;
            dbg(TRACE, "processSCH:: [%d] processing Rkey[%s]", ++ilCount,strRkey);	

            iRet=getPairedFlights(strRkey, &rlAftArr, &rlAftDep);   
            if ( iRet==RC_SUCCESS )
            {     
                dbg(DEBUG, "processSCH:: Arrival: Found[%d] urno[%s] adho[%s] paid[%s]",
                    rlAftArr.iFound, rlAftArr.URNO, rlAftArr.ADHO, rlAftArr.PAID); 
                dbg(DEBUG, "processSCH:: Departure: Found[%d] urno[%s] adho[%s] paid[%s]",
                    rlAftDep.iFound, rlAftDep.URNO, rlAftDep.ADHO, rlAftDep.PAID); 
                
                ilAdhocArr=1;
                if ( rlAftArr.iFound )
                    ilAdhocArr=isAdhoc(rlAftArr.URNO, rlAftArr.ADHO, rlAftArr.PAID);
                        
                ilAdhocDep=1;
                if ( rlAftDep.iFound )
                    ilAdhocDep=isAdhoc(rlAftDep.URNO, rlAftDep.ADHO, rlAftDep.PAID);
                 
                dbg(TRACE, "processSCH:: Arrival Adhoc[%d], Departure Adhoc[%d]",
                    ilAdhocArr, ilAdhocDep);
                if ( ilAdhocArr && ilAdhocDep )
                {
                	iRet=RC_FAIL;
                    dbg(TRACE, 	"processSCH:: Adhoc Flights!!!  Skipping!");
                }
                
                if ( iRet==RC_SUCCESS )
                {
                    iRet=processNULLCheck("ERPSCH", &rlAftArr, &rlAftDep, &ilFlag);	
                    dbg(TRACE, "processSCH:: Found NULL[%d]", ilFlag);
                    if ( ilFlag )
                    {
                        dbg(TRACE, "processSCH:: ERROR!!!  Mandatory Fields NULL!");
                        iRet=RC_FAIL;
                    }
                }
                
                if ( iRet==RC_SUCCESS  )
                {
                    iRet=processDuplicates("ERPSCH", &rlAftArr, &rlAftDep, &ilFlag);
                    dbg(TRACE, "processSCH:: Found Duplicates[%d]", ilFlag);
                    if ( ilFlag )
                    {
                        dbg(TRACE, "processSCH:: ERROR!!!  Duplicate Found!");
                        iRet=RC_FAIL;
                    }            
                }
                
                if ( iRet==RC_SUCCESS )
                {
                    iRet=populateERPSCH(&stErp, &rlAftArr, &rlAftDep);    
                    if ( iRet==RC_SUCCESS )
                    {
                    	printERPSCH(&stErp);
                        if (stErp.iFound==1)
                            iRet=updateERPSCH(&stErp);
                        else
                            iRet=insertERPSCH(&stErp);
                    }
                }
            }
            dbg(TRACE, "processSCH:: processing Rkey[%s], Result[%d]", strRkey, iRet);	
        }
        fclose(fptr);
    }
    
    updateERPSCHMonday(pcpStoaTo);   
        
    
    if ( iRet==RC_SUCCESS )
    {
          dbg(TRACE,"processSCH:: (for SCH) SENDING SCH COMMAND TO WMERPS");
          iRet = sendCedaEventWithLog(igWMERPSQueue,0,pcgAftDestName,pcgAftRecvName,
                               pcgAftTwStart,pcgAftTwEnd,"SCH","ERPSCH","",
                               "STATUS","OK","",4,NETOUT_NO_ACK);                
    }
                    
    dbg(TRACE, "processSCH:: pcpStoaFrom[%s] pcpStoaTo[%s] Result[%d]",
        pcpStoaFrom, pcpStoaTo, iRet);         
    return iRet;    
}

int updateERPSCHMonday(char *pcpMonday)
{
    int ilRC=RC_SUCCESS;
    char pclSql[1024];
    char pclURNO[16];
    char pclHOPO[10]="UFIS";
    char pclALFN[32];
    char pclData[1024];
    char pclErr[513];
    short slCursor;
    int ilUrno=0;
    
    dbg(DEBUG, "updateERPSCHMonday:: Start");
    sprintf(pclSql, "SELECT URNO,HOPO,ALFN FROM ERPSCH "
                    "WHERE HOPO='%s' ",
            pclHOPO);

    ilRC=sql_if(START, &slCursor, pclSql, pclData);
    dbg(TRACE, "updateERPSCHMonday:: executing[%s], Result[%d]", pclSql, ilRC);

    if ( ilRC==RC_FAIL )
    {
        dbg(TRACE, "updateERPSCHMonday:: DB ERROR!!!");
    }
    else if ( ilRC==ORA_NOT_FOUND )
    {
        dbg(TRACE, "updateERPSCHMonday:: Record Not Found");	
        
        ilUrno=NewUrnos("ERPSCH",1);      
        sprintf(pclSql, "INSERT INTO ERPSCH (URNO,HOPO,ALFN) VALUES ('%d', '%s', '%s') ",
                ilUrno, pclHOPO, pcpMonday);
        ilRC=executeSql(pclSql,pclErr); 
        if ( ilRC!=RC_SUCCESS  )
            dbg(TRACE, "updateERPSCHMonday:: INSERT DB ERROR[%s]!", pclErr);
    }
    else
    {
        BuildItemBuffer (pclData, "URNO,HOPO,ALFN,STAT", 4, ",");
        get_real_item(pclURNO,pclData,1);
        get_real_item(pclHOPO,pclData,2);
        get_real_item(pclALFN,pclData,3);
	
	    dbg(TRACE, "updateERPSCHMonday:: Record Found: URNO[%s] HOPO[%s] ALFN[%s]",
	        pclURNO, pclHOPO, pclALFN);
	        
        sprintf(pclSql, "UPDATE ERPSCH SET ALFN='%s' "
                        "WHERE URNO='%s' ",
                        pcpMonday, pclURNO);
        ilRC=executeSql(pclSql,pclErr); 
        if ( ilRC!=RC_SUCCESS  )
            dbg(TRACE, "updateERPSCHMonday:: UPDATE DB ERROR[%s]!", pclErr);	        
    }   
    close_my_cursor(&slCursor);
      
    dbg(DEBUG, "updateERPSCHMonday:: End");
    return ilRC;	
}

static int CalTimeWindowMonth(char *pcpDateString,char *pcpFromLAND, char *pcpToLAND,char *pcpFromAIRB, char *pcpToAIRB)
{
    char *pclFunc = "CalTimeWindowMonthCalTimeWindowMonth";
    int ilOffsetLAND1 = 0;
    int ilOffsetLAND2 = 0;
    int ilOffsetAIRB1 = 0;
    int ilOffsetAIRB2 = 0;
    char pclLAND1stOption[256] = "\0";
    char pclLAND3rdOption[256] = "\0";
    char pclAIRB1stOption[256] = "\0";
    char pclAIRB3rdOption[256] = "\0";
    int ilRc = RC_SUCCESS;
    char pclTmp[256] = "\0";
    int ilPreMonth = 0;
    char buffer[4];
    char pclRef[8] = "\0";
    
    //LAND = MONTH-1,000000,MONTH,000000
    ilRc = GetConfigOption(1,pclLAND1stOption,rgTM_Month.pcpLANDConfig);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"%s GetConfigOption(1,pclLAND1stOption,rgTM_Month.pcpLANDConfig) fails, return",pclFunc);
        return RC_FAIL;
    }
    dbg(DEBUG,"%s pclLAND1stOption<%s>",pclFunc,pclLAND1stOption);
    
    ilRc = GetConfigOption(3,pclLAND3rdOption,rgTM_Month.pcpLANDConfig);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"%s GetConfigOption(3,pclLAND3rdOption,rgTM_Month.pcpLANDConfig) fails, return",pclFunc);
        return RC_FAIL;
    }
    dbg(DEBUG,"%s pclLAND3rdOption<%s>",pclFunc,pclLAND3rdOption);
    
    //AIRB = DAY-2,000000,DAY-1,000000
    ilRc = GetConfigOption(1,pclAIRB1stOption,rgTM_Month.pcpAIRBConfig);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"%s GetConfigOption(1,pclAIRB1stOption,rgTM_Month.pcpAIRBConfig) fails, return",pclFunc);
        return RC_FAIL;
    }
    dbg(DEBUG,"%s pclAIRB1stOption<%s>",pclFunc,pclAIRB1stOption);
    
    ilRc = GetConfigOption(3,pclAIRB3rdOption,rgTM_Month.pcpAIRBConfig);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"%s GetConfigOption(3,pclAIRB3rdOption,rgTM_Month.pcpAIRBConfig) fails, return",pclFunc);
        return RC_FAIL;
    }
    dbg(DEBUG,"%s pclAIRB3rdOption<%s>",pclFunc,pclLAND3rdOption);
    
    strncpy(pclRef,pclLAND1stOption,strlen("MONTH"));
    
    CalTimeFromCfg(pclLAND1stOption,pclRef,pcpDateString,pcpFromLAND);
    CalTimeFromCfg(pclLAND3rdOption,pclRef,pcpDateString,pcpToLAND);
    CalTimeFromCfg(pclAIRB1stOption,pclRef,pcpDateString,pcpFromAIRB);
    CalTimeFromCfg(pclAIRB3rdOption,pclRef,pcpDateString,pcpToAIRB);
    
    return RC_SUCCESS;
}

static int GetDateStrBasedOnOffset(char *pcpDateString,int ilOffset)
{
    char *pclFunc = "GetDateStrBasedOnOffset";
    
    char pclCurrUTCTime[32] = "\0";
    char pclCurrLOCTime[32] = "\0";

    GetServerTimeStamp("UTC", 1, 0, pclCurrUTCTime);
    GetServerTimeStamp("LOC", 1, 0, pclCurrLOCTime);
    
    AddSecondsToCEDATime(pclCurrUTCTime, ConvertDay2Sec(ilOffset), 1);
    
    strncpy(pcpDateString,pclCurrUTCTime,8);

  return RC_SUCCESS;
}

int ConvertDayFromStrngToInt(char *pclDayString)
{
    char *pclFunc = "ConvertDayFromStrngToInt";
    if(pclDayString == NULL)
    {
        dbg(TRACE,"%s pclDayString is null, return",pclFunc);
        return RC_FAIL;
    }
    else
    {
        if(strncmp(pclDayString,"MON",3) == 0)
        {
            return 1;
        }
        else if(strncmp(pclDayString,"TUE",3) == 0)
        {
            return 2;
        }
        else if(strncmp(pclDayString,"WED",3) == 0)
        {
            return 3;
        }
        else if(strncmp(pclDayString,"THU",3) == 0)
        {
            return 4;
        }
        else if(strncmp(pclDayString,"FRI",3) == 0)
        {
            return 5;
        }
        else if(strncmp(pclDayString,"SAT",3) == 0)
        {
            return 6;
        }
        else if(strncmp(pclDayString,"SUN",3) == 0)
        {
            return 0;
        }
    }
}

int CalDateOffset(int ilFrom,int ilTo)
{
    char *pclFunc = "CalOffset";
    int ilOffset = 0;
    if( (ilFrom < 0 || ilFrom > 6) && ( ilTo < 0 || ilTo > 6) )
    {
        dbg(TRACE,"%s ilFrom<%d> or ilTo<%d> is out of range[0,6]",pclFunc,ilFrom,ilTo);
        return RC_FAIL;
    }
    else
    {
        dbg(DEBUG,"%s ilFrom is <%d>,ilTo is <%d>",pclFunc,ilFrom,ilTo);
        
        if(ilFrom == ilTo)
        {
            ilOffset = 7;
        }
        else//ilCurWeekDay != ilSTOACfg1stFld
        {
            if(ilFrom == SUN)
            {
                ilOffset = ilTo - ilFrom;
            }
            else//ilCurWeekDay != SUN
            {
                ilOffset = ilTo - ilFrom + 7;
            }
        }
        dbg(DEBUG,"%s ilOffset is <%d>",pclFunc,ilOffset);
    }
    
    return ilOffset;
}

static void SearchMemory(char *pclValue,int ilCountRecord,char *pclDssn,char *pclType,char *pclStyp,char *pclSstp,char *pclSsst,char *pclFieldName,char *pclApc3)
{
    char *pclFunc = "SearchMemory";
    int ilRc = RC_SUCCESS;
    int ili = 0;
    
    memset(pclValue,0,sizeof(pclValue));
    
    for(ili=0; ili<ilCountRecord; ili++)
    {    
        
        if((strstr(pclDssn,(prgDTSSSFldCfg+ili)->pclDssn)!=0)
         &&(strcmp(pclType,(prgDTSSSFldCfg+ili)->pclType)==0) 
         &&(strcmp(pclStyp,(prgDTSSSFldCfg+ili)->pclStyp)==0)
         &&(strcmp(pclSstp,(prgDTSSSFldCfg+ili)->pclSstp)==0)
         &&(strcmp(pclSsst,(prgDTSSSFldCfg+ili)->pclSsst)==0)
         )
        {
            if(strcmp(pclApc3,(prgDTSSSFldCfg+ili)->pclApc3)==0)// || strcmp(pclApc3,cgHopo)==0)
            {
                dbg(TRACE,"%s The record has been found at<%d> for <%s> value<%s>",pclFunc,ili+1,pclFieldName,(prgDTSSSFldCfg+ili)->pclValu);
                strcpy(pclValue,(prgDTSSSFldCfg+ili)->pclValu);
                break;
            }
        }
    }
    if(strlen(pclValue)==0)
    {
        dbg(TRACE,"%s The record has not been found in memory for %s",pclFunc,pclFieldName);
    }
}

static void GetValueFromLoaTab(int ilCountRecord,char *pclValu,char *pclConfigString,char *pclFieldName)
{
    char *pclFunc = "GetValueFromLoaTab";
    
    char pclDssn[1024]="\0";
    char pclType[32]="\0";
    char pclStyp[32]="\0";
    char pclSstp[32]="\0";
    char pclSsst[32]="\0"; 
    char pclApc3[32]="\0";
    
    SetZeroDTSSS(pclDssn,pclType,pclStyp,pclSstp,pclSsst);
    
    ExtractLoaWhereClauseValues(pclConfigString,pclDssn,pclType,pclStyp,pclSstp,pclSsst,pclApc3);
    
    FormatDssnFldCfg(pclDssn);
    
    dbg(DEBUG,"%s %s pclDssn<%s>,pclType<%s>,pclStyp<%s>,pclSstp<%s>,pclSsst<%s>,pclApc3<%s>",pclFunc,pclFieldName,pclDssn,pclType,pclStyp,pclSstp,pclSsst,pclApc3);
    
    SearchMemory(pclValu,ilCountRecord,pclDssn,pclType,pclStyp,pclSstp,pclSsst,pclFieldName,pclApc3);
}
/******************************************************************************/
/* Space for Appendings (Source-Code Control)                                 */
/******************************************************************************/
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
int getDayOfWeek(char *strYYYYMMDDhhmmss, char *strOut)
{
    int iRet=RC_SUCCESS;
    struct tm stTm;
    
    convertDateToStructTm(strYYYYMMDDhhmmss, &stTm);
    dbg(DEBUG, "getDayOfWeek:: stTm.tm_wday[%d]", stTm.tm_wday);
    switch (stTm.tm_wday)
    {
        case 0:
            strcpy(strOut, "SUNDAY");
            break;
        case 1:
            strcpy(strOut, "MONDAY");
            break;
        case 2:
            strcpy(strOut, "TUESDAY");
            break;
        case 3:
            strcpy(strOut, "WEDNESDAY");
            break;
        case 4:
            strcpy(strOut, "THURSDAY");
            break;
        case 5:
            strcpy(strOut, "FRIDAY");
            break;
        case 6:
            strcpy(strOut, "SATURDAY");
            break; 
        default:
            strcpy(strOut, "");
            break;                
    }
    dbg(DEBUG, "getDayOfWeek:: strYYYYMMDDhhmmss[%s] strOut[%s]", strYYYYMMDDhhmmss, strOut);
    return iRet;
}

int convertDateToStructTm(char *strYYYYMMDDhhmmss, struct tm *stTm)
{
    int iRet=RC_SUCCESS;
    char YYYY[5];
    char MM[3];
    char DD[3];
    char hh[3];
    char mi[3];
    char ss[3];
    struct tm *stTmp;
    time_t tNow=time(NULL);
    int iSize=strlen(strYYYYMMDDhhmmss);
    
    stTmp=gmtime_r(&tNow, stTm);
    
    if ( iSize>=4 )    
    {
        sprintf(YYYY, "%4.4s", strYYYYMMDDhhmmss);
        stTm->tm_year=atoi(YYYY)-1900;
    }
    else
         stTm->tm_year=0;
    
    if ( iSize>=6 )
    {
         sprintf(MM, "%2.2s", &strYYYYMMDDhhmmss[4]);
         stTm->tm_mon = atoi(MM)-1;
    }
    else
        stTm->tm_mon = 0;
    
    if ( iSize>=8 )
    {
        sprintf(DD, "%2.2s", &strYYYYMMDDhhmmss[6]);
        stTm->tm_mday=atoi(DD);
    }
    else
        stTm->tm_mday=0;
        
    if ( iSize>=10 )
    {
        sprintf(hh, "%2.2s", &strYYYYMMDDhhmmss[8]);
        stTm->tm_hour=atoi(hh);
    }
    else
        stTm->tm_hour=0;
    
    if ( iSize>=12 )
    {
        sprintf(mi, "%2.2s", &strYYYYMMDDhhmmss[10]);
        stTm->tm_min = atoi(mi);
    }
    else
        stTm->tm_min = 0;
        
    if ( iSize>=14 )
    {
        sprintf(ss, "%2.2s", &strYYYYMMDDhhmmss[12]);
        stTm->tm_sec=atoi(ss);
    }
    else
        stTm->tm_sec=0;

    tNow=mktime(stTm);
    stTmp=localtime_r(&tNow, stTm);

    return iRet;    
}

int convertDate(char *str, char *strFMT, char *strOut, char *strFMTOut)
{
    int iRet=RC_SUCCESS;
    int a=0;
    int iStrSize=strlen(str);
    int iStrFMTSize=strlen(strFMT);
    int iStrFMTOutSize=strlen(strFMTOut);
    char yyyy[5];
    int iy=0;
    char mm[3];
    int im=0;
    char dd[3];
    int id=0;
    char sep[2];
    int is=0;
    char hh[3];
    int ih=0;
    char mi[3];
    int imi=0;
    char ss[3];
    int iss=0;    
    char strMonth[32];
    char strTmpOut[64];
    
    memset(strTmpOut, 0, sizeof(strTmpOut));
    dbg(DEBUG, "convertDate:: str[%s] strFMT[%s], strFMTOut[%s] Start", str, strFMT, strFMTOut);
    if ( iStrSize<iStrFMTSize )
    {
        dbg(DEBUG, "convertDate:: str[%s] is less than strFMT[%s] ");
        iRet=RC_FAIL;    
    }
    else
    {
        memset(yyyy, 0, sizeof(yyyy));
        memset(mm, 0, sizeof(mm));
        memset(dd, 0, sizeof(dd));
        memset(sep, 0, sizeof(sep));
        
        for (a=0; a<iStrFMTSize; a++)
        {
            if ( strFMT[a]=='Y' )
            {
                if ( iy==4 )
                    ;
                else
                    yyyy[iy++]=str[a];    
            }
            else if ( strFMT[a]=='M' )
            {
                 if ( im==2 )
                    ;
                else
                    mm[im++]=str[a];                   
            }
            else if ( strFMT[a]=='D' )
            {
                 if ( id==2 )
                    ;
                else
                    dd[id++]=str[a];                      
            }
            else if ( strFMT[a]=='h' )
            {
                 if ( ih==2 )
                    ;
                else
                    hh[ih++]=str[a];                      
            }
            else if ( strFMT[a]=='m' )
            {
                 if ( imi==2 )
                    ;
                else
                    mi[imi++]=str[a];                      
            }
            else if ( strFMT[a]=='s' )
            {
                 if ( iss==2 )
                    ;
                else
                    ss[iss++]=str[a];                      
            }
        }   
        
        iy=0;
        im=0;
        id=0;
        ih=0;
        imi=0;
        iss=0;
        for (a=0; a<iStrFMTOutSize; a++)  
        {
            if ( strFMTOut[a]=='Y' )
            {
                if ( iy==4 )
                    strTmpOut[a]=' ';
                else
                    strTmpOut[a]=yyyy[iy++];    
            }
            else if ( strFMTOut[a]=='M' )
            {
                if (strstr(strFMTOut, "MON")!=NULL)
                {
                    switch (atoi(mm))
                    {
                        case 1: 
                            strcpy(strMonth, "JAN");    
                            break;
                        case 2: 
                            strcpy(strMonth, "FEB");    
                            break;
                        case 3: 
                            strcpy(strMonth, "MAR");    
                            break;
                        case 4: 
                            strcpy(strMonth, "APR");    
                            break;
                        case 5: 
                            strcpy(strMonth, "MAY");    
                            break;
                        case 6: 
                            strcpy(strMonth, "JUN");    
                            break;
                        case 7: 
                            strcpy(strMonth, "JUL");    
                            break;
                        case 8: 
                            strcpy(strMonth, "AUG");    
                            break;
                        case 9: 
                            strcpy(strMonth, "SEP");    
                            break;
                        case 10: 
                            strcpy(strMonth, "OCT");    
                            break;
                        case 11: 
                            strcpy(strMonth, "NOV");    
                            break;
                        case 12: 
                            strcpy(strMonth, "DEC");    
                            break;
                        default:
                            strcpy(strMonth, "   ");                            
                    }    
                    
                    sprintf(strTmpOut, "%s%s", strTmpOut, strMonth);
                    a+=2;
                }
                else
                {
                    if ( im==2 )
                        strTmpOut[a]=' ';
                    else
                        strTmpOut[a]=mm[im++];
                }    
            }
            else if ( strFMTOut[a]=='D' )
            {
                if ( id==2 )
                    strTmpOut[a]=' ';
                else
                    strTmpOut[a]=dd[id++];    
            }             
            else if ( strFMTOut[a]=='h' )
            {
                if ( ih==2 )
                    strTmpOut[a]=' ';
                else
                    strTmpOut[a]=hh[ih++];    
            }            
            else if ( strFMTOut[a]=='m' )
            {
                if ( imi==2 )
                    strTmpOut[a]=' ';
                else
                    strTmpOut[a]=mi[imi++];    
            }             
            else if ( strFMTOut[a]=='s' )
            {
                if ( iss==2 )
                    strTmpOut[a]=' ';
                else
                    strTmpOut[a]=ss[is++];    
            } 
            else
                strTmpOut[a]=strFMTOut[a];     
        }   
        strTmpOut[a]=0;        
    }

    strcpy(strOut, strTmpOut);
    dbg(DEBUG, "convertDate:: str[%s] strFMT[%s], strOut[%s] strFMTOut[%s] End", str, strFMT, strOut, strFMTOut);
    return iRet;
}

int convertTimeToSec(char *strhhmmss)
{
    char hh[3];
    char mi[3];
    char ss[3];
    int iSize=strlen(strhhmmss);
    int iRet=0;
    
    memset(hh,0, sizeof(hh));
    memset(mi,0, sizeof(mi));
    memset(ss,0, sizeof(ss));
    if ( iSize>=2 )
    {
        hh[0]=strhhmmss[0];
        hh[1]=strhhmmss[1];            
    }
    
    if ( iSize>=4 )
    {
        mi[0]=strhhmmss[2];
        mi[1]=strhhmmss[3];            
    }
    
    if ( iSize>=6 )
    {
        ss[0]=strhhmmss[4];
        ss[1]=strhhmmss[5];            
    }
    
    iRet=atoi(hh)*60*60 + atoi(mi)*60 + atoi(ss);
    return iRet;    
}

int getSysDate(char *strYYYYMMDDhhmmss)
{
    int iRet=RC_SUCCESS;
    
    time_t tnow=time(NULL);
    struct tm tmRes;
    
    gmtime_r(&tnow, &tmRes);
    sprintf(strYYYYMMDDhhmmss,"%04d%02d%02d%02d%02d%02d",
            tmRes.tm_year+1900, tmRes.tm_mon+1, tmRes.tm_mday,
            tmRes.tm_hour, tmRes.tm_min, tmRes.tm_sec);
    return iRet;    
}

int executeSql(char *strSql, char *strErr)
{
    int iRet=0;
    int slSqlFunc=START;
    short slActCursor=0;
    char data_area[32768];
    unsigned long ilBufLen = 512;    /* modified int -> long as defined */
    unsigned long ilErrLen = 0;    /* modified int -> long as defined */
  
    dbg(DEBUG, "executeSql:: Start");  
    strcpy(strErr,"");
    iRet = sql_if(slSqlFunc, &slActCursor,strSql, data_area);
    
    if ( iRet==RC_SUCCESS )
        commit_work();
    else
    {
        sqlglm(strErr,&ilBufLen,&ilErrLen);
        strErr[ilErrLen]=0;
        rollback();
    } 
    close_my_cursor(&slActCursor);   
     
    dbg(TRACE, "executeSql:: strSql [%s] Error[%s] Result[%d]", strSql, strErr, iRet);     
    return iRet;    
}

int removeSpace(char *strIn, char *strOut)
{
    int iSize=strlen(strIn);
    int a=0;
    int b=0;
    
    for (a=0; a<iSize; a++)
    {
        if ( strIn[a]==' ' )
            ;
        else
            strOut[b++]=strIn[a];     
    }
    strOut[b]=0;    
}

int getFlightCategory(char *strOut, char *pclTtyp, _FLIGHTCAT *rp)
{
	int ilRC=RC_SUCCESS;
	int a=0;
	
    dbg(DEBUG, "getFlightCategory:: pclTtyp[%s], Record Count[%d]", pclTtyp, rp->ilCount);	
    
    strcpy(strOut, "");
    
    for (a=0; a<rp->ilCount; a++)
    {
        if ( strstr(rp->rec[a].VALUE, pclTtyp)!=NULL )
        {
        	strcpy(strOut, rp->rec[a].DESC);
            break;	
        }	
    }

    dbg(DEBUG, "getFlightCategory:: Result[%d], Out[%s], pclTtyp[%s], Record Count[%d]", 
        ilRC, strOut, pclTtyp, rp->ilCount);	
    return ilRC;
}


int getFieldIndex(char *strFind, char *strFields, int iMax)
{
    int iRet=-1;
    int iCnt=0;
    char *strPtr, *strPtr2;
    char strTmp[2048];
    
    strcpy(strTmp, strFields);
    strPtr=strtok_r(strTmp, ",", &strPtr2);
    while (strPtr)
    {
        if ( !strcmp(strPtr, strFind) )
        {
            iRet=iCnt;
            break;
        }
        iCnt++;    
        strPtr=strtok_r(NULL, ",", &strPtr2);    
    }
    
    if ( iRet>=iMAX )
        iRet=-1;
        
    return iRet;    
}

int timeDiff(char *strYYYYMMDDhhmmss1, char *strYYYYMMDDhhmmss2, double *dOut)
{
    char sysDate[15];
    struct tm tm1, tm2;
    time_t t1, t2;
    
    getSysDate(sysDate);
    
    if ( !strcmp(strYYYYMMDDhhmmss1,"") )
        convertDateToStructTm(sysDate, &tm1);
    else
         convertDateToStructTm(strYYYYMMDDhhmmss1, &tm1);
 
    if ( !strcmp(strYYYYMMDDhhmmss2,"") )
        convertDateToStructTm(sysDate, &tm2);
    else
        convertDateToStructTm(strYYYYMMDDhhmmss2, &tm2) ;   
    
    t1= mktime(&tm1);
    t2= mktime(&tm2);
    *dOut=difftime(t1,t2);
    
    dbg(DEBUG,"timeDiff:: sysdate[%s], time1[%s][%d], time2[%s][%d], Result[%f]",
        sysDate, strYYYYMMDDhhmmss1, t1, strYYYYMMDDhhmmss2, t2, *dOut);
    return 0; 
        
}

int getSTEVMap(char *strStev, char *strRet)
{
    if ( !strcmp(strStev, "A") )
        strcpy(strRet, "1");
    else if ( !strcmp(strStev, "F") )
        strcpy(strRet, "C0");
    else if ( !strcmp(strStev, "1") )
        strcpy(strRet, "1");
    else if ( !strcmp(strStev, "2") )
        strcpy(strRet, "2");
    else if ( !strcmp(strStev, "3") )
        strcpy(strRet, "3");
    else
        strcpy(strRet,"1");
        
    dbg(DEBUG, "getSTEVMap:: strStev[%s], strRet[%s]", strStev, strRet);
    return 0;            
}

int convertDateToLocalTime(char *strYYYYMMDDhhmmss, char *strOut)
{
    int iRet=RC_SUCCESS;
    struct tm stTm;
    struct tm *stTmp;
    time_t t;
    
    dbg(DEBUG, "convertDateToLocalTime:: timezone diff[%d]", timezone);
    if ( strlen(strYYYYMMDDhhmmss)>=8 )
    {
        convertDateToStructTm(strYYYYMMDDhhmmss, &stTm);
        t=mktime(&stTm);
        stTmp=gmtime_r(&t, &stTm);
    
        sprintf(strOut, "%04d%02d%02d%02d%02d%02d",
                stTm.tm_year+1900, stTm.tm_mon+1, stTm.tm_mday, 
                stTm.tm_hour, stTm.tm_min, stTm.tm_sec);
    }
    else
        strcpy(strOut,"");
        
    dbg(DEBUG, "convertDateToLocalTime:: strYYYYMMDDhhmmss[%s], strOut[%s] Result[%d]", 
        strYYYYMMDDhhmmss, strOut, iRet);
      
    return iRet;    
}

int formatVIAL(int ipCount, char *pcpIn, char *pcpOut, char *pcpSeparator)
{
	int ilRC=RC_SUCCESS;
	char *pclPtr;
	int a=0;
	
    dbg(DEBUG, "formatVIAL:: Start");
    
    dbg(DEBUG, "formatVIAL:: ipCount[%d], Input[%s], Separator[%s]", ipCount, pcpIn, pcpSeparator);
    strcpy(pcpOut, "");
    pclPtr=pcpIn;
    for (a=0; a<ipCount;  a++)
    {
    	if ( strlen(pclPtr)>3 )
    	{
    		pclPtr++;
            if (a)
                sprintf(pcpOut,"%s%s%3.3s", pcpOut, pcpSeparator,pclPtr);
            else
                sprintf(pcpOut,"%3.3s", pclPtr);
                            
            pclPtr+=120;  
        }  
    }    

    dbg(DEBUG, "formatVIAL:: In[%s], Out[%s]", pcpIn, pcpOut);
    dbg(DEBUG, "formatVIAL:: End Result[%d]", ilRC);
    return ilRC;	
}

int addFlightsERPSCH(char *pcpDateSearch, char *pcpDate)
{
	int ilRC=RC_SUCCESS;
	int ilCount=0;
	char pclSql[2048];
	char pclTmp[1024];
	int iTmp=0;
	short slCursorArr = 0;
	char pclDataAreaArr[8192];
	char pclRkey[32];
    AFTREC rlAftArr;
    AFTREC rlAftDep;
    ERPSCHREC stErp;
        	
    dbg(TRACE, "addFlightsERPSCH:: Start");	
   
    memset(&pclSql, 0, sizeof(pclSql));
    memset(&pclTmp, 0, sizeof(pclTmp));
       
    sprintf(pclSql, "SELECT DISTINCT(RKEY) RKEY FROM afttab "
                    "WHERE ( stoa like '%s%%' or stod like '%s%%' ) "
                    "AND RKEY NOT IN (SELECT FLNU FROM ERPSCH) ", pcpDateSearch, pcpDateSearch); 
    
    if( rgTM_Daily.CommCfgPart.iNoOfSections_SelCri>0 )
    {
        for(ilCount=0;ilCount < rgTM_Daily.CommCfgPart.iNoOfSections_SelCri; ilCount++)
        {
            sprintf(pclTmp," AND %s ",rgTM_Daily.CommCfgPart.prSection_SelCri[ilCount].pcType);
            strcat(pclSql,pclTmp);
            
            if(strncmp(rgTM_Daily.CommCfgPart.prSection_SelCri[ilCount].pcSpecifier,"NOT",3)==0)
            {
                sprintf(pclTmp," NOT IN ");
            }
            else
            {
                sprintf(pclTmp," IN ");
            }
            strcat(pclSql,pclTmp);
            
            sprintf(pclTmp,"(%s)",rgTM_Daily.CommCfgPart.prSection_SelCri[ilCount].pcRange);
            strcat(pclSql,pclTmp);
        }        
    }
                     
    dbg(TRACE, "addFlightsERPSCH:: SQL[%s]", pclSql);
    iTmp=sql_if(START, &slCursorArr, pclSql, pclDataAreaArr);
    while ( iTmp==DB_SUCCESS )
    {
        BuildItemBuffer (pclDataAreaArr, "RKEY", 1, ",");
        get_real_item(pclRkey,pclDataAreaArr,1);

        dbg(DEBUG, "addFlightsERPSCH:: Rkey[%s]", pclRkey);
        memset(&rlAftArr, 0, sizeof(rlAftArr));
        memset(&rlAftDep, 0, sizeof(rlAftDep));
        memset(&stErp, 0, sizeof(stErp));
        
        ilRC=getPairedFlights(pclRkey, &rlAftArr, &rlAftDep);        
        dbg(DEBUG, "addFlightsERPSCH:: paired flights: arrival[%s] departure[%s]", rlAftArr.URNO, rlAftDep.URNO); 
        if ( ilRC==RC_SUCCESS )
        {
            ilRC=populateERPSCH(&stErp, &rlAftArr, &rlAftDep);    
            if ( ilRC==RC_SUCCESS )
            {
            	printERPSCH(&stErp);
                if (stErp.iFound==1)
                    ilRC=updateERPSCH(&stErp);
                else
                    ilRC=insertERPSCH(&stErp);
            }
        }
        iTmp=sql_if(NEXT,&slCursorArr,pclSql,pclDataAreaArr);        
    }      
    close_my_cursor(&slCursorArr);    
    dbg(TRACE, "addFlightsERPSCH:: End Result[%d]", ilRC);
    return ilRC;
}

int getSCHDDates(char *pcpToday, char *pcpYesterday, char *pcpMonday, char *pcpNextMonday)
{
	int ilRC=RC_SUCCESS;
	
	time_t tNow=time(NULL);
	time_t tYesterday=time(NULL)-86400;
	time_t tMonday;
	time_t tNextMonday;
	
	struct tm *rlDef;
	struct tm rlNow;
	struct tm rlYesterday;
	struct tm rlMonday;
	struct tm rlNextMonday;
	char pclTmp[strlen(rgTM_Daily.pcpSTOAConfig)+1];
	char *pclTimeMonday, *pclTimeNextMonday;
	char *pclPtr;
	
	dbg(DEBUG, "getSCHDDates:: Start");
	    
    rlDef=gmtime_r(&tNow, &rlNow);
    sprintf(pcpToday, "%04d%02d%02d%02d%02d%02d", 
            rlNow.tm_year+1900, rlNow.tm_mon+1, rlNow.tm_mday,
            rlNow.tm_hour, rlNow.tm_min, rlNow.tm_sec);
            
    rlDef=gmtime_r(&tYesterday, &rlYesterday);
    sprintf(pcpYesterday, "%04d%02d%02d%02d%02d%02d", 
            rlYesterday.tm_year+1900, rlYesterday.tm_mon+1, rlYesterday.tm_mday,
            rlYesterday.tm_hour, rlYesterday.tm_min, rlYesterday.tm_sec);


    switch (rlNow.tm_wday)
    {
        case 0: // Sunday
        {
        	dbg(DEBUG, "SUNDAY");
/*        	
        	tMonday=tNow-(6*86400);
        	tNextMonday=tNow+86400;      
*/  	
        	tMonday=tNow+86400;
        	tNextMonday=tNow+(8*86400);      
            break;	
        }	
        case 1: // Monday
        {
        	dbg(DEBUG, "MONDAY");
        	tMonday=tNow;
        	tNextMonday=tNow+(7*86400);        	
            break;	
        }	
        case 2: // Tuesday
        {
        	dbg(DEBUG, "TUESDAY");
        	tMonday=tNow-86400;
        	tNextMonday=tNow+(6*86400);        	
            break;	
        }	
        case 3: // Wednesday
        {
        	dbg(DEBUG, "WEDNESDAY");
        	tMonday=tNow-(2*86400);
        	tNextMonday=tNow+(5*86400);        	
            break;	
        }	
        case 4: // Thursday
        {
        	dbg(DEBUG, "THURSDAY");
        	tMonday=tNow-(3*86400);
        	tNextMonday=tNow+(4*86400);        	
            break;	
        }	
        case 5: // Friday
        {
        	dbg(DEBUG, "FRIDAY");
        	tMonday=tNow-(4*86400);
        	tNextMonday=tNow+(3*86400);        	
            break;	
        }	
        case 6: // Saturday
        {
        	dbg(DEBUG, "SATURDAY");
 /*       	
        	tMonday=tNow-(5*86400);
        	tNextMonday=tNow+(2*86400);        	
 */
        	tMonday=tNow+(2*86400);
        	tNextMonday=tNow+(9*86400);        	
            break;	
        }	

    }
	
	strcpy(pclTmp, rgTM_Daily.pcpSTOAConfig);
	pclTimeMonday=strtok_r(pclTmp, ",", &pclPtr);
	if ( pclTimeMonday!=NULL )
	    pclTimeMonday=strtok_r(NULL, ",", &pclPtr);
	if ( pclTimeMonday!=NULL )
	    pclTimeNextMonday=strtok_r(NULL, ",", &pclPtr);	
	if ( pclTimeNextMonday!=NULL )
	    pclTimeNextMonday=strtok_r(NULL, ",", &pclPtr);
	
    rlDef=gmtime_r(&tMonday, &rlMonday);
    sprintf(pcpMonday, "%04d%02d%02d%s", 
            rlMonday.tm_year+1900, rlMonday.tm_mon+1, rlMonday.tm_mday,
            pclTimeMonday);
            

    rlDef=gmtime_r(&tNextMonday, &rlNextMonday);
    sprintf(pcpNextMonday, "%04d%02d%02d%s", 
            rlNextMonday.tm_year+1900, rlNextMonday.tm_mon+1, rlNextMonday.tm_mday,
            pclTimeNextMonday);
            

    dbg(TRACE, "getSCHDDates:: pcpToday[%s], pcpYesterday[%s], pcpMonday[%s], pcpNextMonday[%s]",
        pcpToday, pcpYesterday, pcpMonday, pcpNextMonday);
    dbg(DEBUG, "getSCHDDates:: End Result[%d]", ilRC);
    return ilRC;    	
}

int getSqlResult(char *pcpData, char *pcpSql, char *pcpFields, int ipFields, int ipWithQuotes)
{
    int ilRC=RC_SUCCESS;
    int ilTmp;
    char pclResult[32768];
    char pclTmp[2048];
    short slCursor=0;
    int slSqlFunc=START;
    int a=0;
    int ilComma=0;
    
    dbg(DEBUG, "getSqlResult:: Start");

    dbg(TRACE, "getSqlResult:: executing [%s]", pcpSql);
    pcpData[0]=0;
    while ( (ilTmp=sql_if(slSqlFunc,&slCursor,pcpSql,pclResult))==RC_SUCCESS )
    {
    	BuildItemBuffer (pclResult, pcpFields, ipFields, ",");
    	for (a=1; a<=ipFields; a++)
    	{
    	    get_real_item(pclTmp,pclResult,a);

    	    if ( ilComma )
    	    {
    	    	if ( ipWithQuotes )
    	            sprintf(pcpData, "%s,'%s'", pcpData, pclTmp);
    	        else	
    	            sprintf(pcpData, "%s,%s", pcpData, pclTmp);	
    	    }
    	    else    	    
    	    {
    	    	if ( ipWithQuotes )
    	            sprintf(pcpData, "'%s'", pclTmp);	
    	    	else
    	            strcpy(pcpData, pclTmp);	
    	        ilComma=1;
    	    }
    	}
        slSqlFunc=NEXT;
    }
    close_my_cursor(&slCursor);

    if ( ilTmp==RC_FAIL )
        ilRC=RC_FAIL;
    else if ( pcpData[0]==0 )
        ilRC=ORA_NOT_FOUND;	
       
    dbg(DEBUG, "getSqlResult:: End Result[%d], pcpData[%s]", ilRC, pcpData);    
    return ilRC;	
}

int processDeleteERPSCH(char *pcpUrno)
{
    int ilRC=RC_SUCCESS;
    char pclSql[1024];
    char pclUrno[32];
    char pclErr[513];
    
    dbg(TRACE, "processDeleteERPSCH:: Start");

    sprintf(pclSql, "SELECT URNO FROM ERPSCH WHERE FLNU='%s' ", pcpUrno);
    ilRC=getTableUrno(pclUrno, pclSql);
    dbg(DEBUG, "processUSCHCmd:: Erpsch Urno[%s]", pclUrno);
        
    if ( ilRC==RC_SUCCESS )
    {                	
        dbg(TRACE,"processDeleteERPSCH:: (for USCH/DSCH cmd) SENDING SCH COMMAND TO WMERPS");
        ilRC = sendCedaEventWithLog(igWMERPSQueue, 0, pcgAftDestName, pcgAftRecvName,
                                    pcgAftTwStart, pcgAftTwEnd, "SCH", "ERPSCH","",
                                    "FLNU", pcpUrno, "", 4, NETOUT_NO_ACK);    
               
        sprintf(pclSql, "UPDATE ERPSCH SET STAT='U', FTYP='CANCELLED' WHERE FLNU='%s'", pcpUrno);
        ilRC=executeSql(pclSql,pclErr);   
        
        if ( ilRC==ORA_NOT_FOUND ) 
            ilRC=RC_SUCCESS;
    }
    
    dbg(TRACE, "processDeleteERPSCH:: End Result[%d]", ilRC);
    return ilRC;	
}

int processERPSCH(char *pcpRkey, char *pcpDateString)
{
    int ilRC=RC_SUCCESS;
	char pclDelete[1001];
	int ilFirst;
    char pclErr[513];
    int ilTmp;
    AFTREC rlAftArr;
    AFTREC rlAftDep;
    ERPSCHREC stSch;
    
    dbg(TRACE, "processERPSCH:: Start");
                
    ilTmp=getPairedFlights(pcpRkey, &rlAftArr, &rlAftDep);  
    if ( ilTmp==RC_FAIL )
        dbg(TRACE, "processERPSCH:: Error in getPairedFlights");
    else
    {
        if (
               strcmp(rlAftArr.URNO, "") ||
               strcmp(rlAftDep.URNO, "")
           )
        {	
            strcpy(pclDelete, "UPDATE erpsch set STAT='D' WHERE FLNU IN (");
            ilFirst=1;
            if ( strcmp(rlAftArr.URNO, "") )
            {
                sprintf(pclDelete, "%s '%s' ", pclDelete, rlAftArr.URNO);
                ilFirst=0;
            }
                    
            if ( strcmp(rlAftDep.URNO, "") )
            {
                if ( ilFirst )
                    sprintf(pclDelete, "%s '%s' ", pclDelete, rlAftDep.URNO);	                           
                else
                   	sprintf(pclDelete, "%s, '%s' ", pclDelete, rlAftDep.URNO);	
            }
            sprintf(pclDelete, "%s) ", pclDelete);
 
            ilRC=executeSql(pclDelete,pclErr); 
            if (ilRC!=RC_FAIL)
                ilRC=RC_SUCCESS;    
            dbg(TRACE, "processERPSCH:: Executing [%s][, Result[%d][%s]", pclDelete, ilRC, pclErr);
            
            if ( ilRC==RC_SUCCESS )
                ilRC=populateERPSCH(&stSch, &rlAftArr, &rlAftDep);    

            if ( ilRC==RC_SUCCESS )
            {
            	printERPSCH(&stSch);
                ilRC=insertERPSCH(&stSch);
            }
        }
    }
            
    dbg(TRACE, "processERPSCH:: End");
    return ilRC;    	
}

int reprocessERPSCHRecords(char *pcpYesterday, char *pcpMonday, char *pcpNextMonday, char *pcpDateString)
{
	int ilRC=RC_SUCCESS;
	char pclSql[1001];
	char pclTmp[1001];
	int ilCount;
    short slCursorArr = 0;
    char pclDataAreaArr[8192];
    int ilTmp;
   
    
    dbg(TRACE, "reprocessERPSCHRecords:: Start");
    
    sprintf(pclSql, "SELECT DISTINCT(RKEY)RKEY "
                    "FROM afttab "
                    "WHERE ( (stoa>='%s' AND stoa<'%s') or "
                            "(stod>='%s' AND stod<'%s') ) ",
            pcpMonday, pcpNextMonday, pcpMonday, pcpNextMonday);

    if( rgTM_Daily.CommCfgPart.iNoOfSections_SelCri>0 )
    {
        for(ilCount=0;ilCount < rgTM_Daily.CommCfgPart.iNoOfSections_SelCri; ilCount++)
        {
            sprintf(pclTmp," AND %s ",rgTM_Daily.CommCfgPart.prSection_SelCri[ilCount].pcType);
            sprintf(pclSql,"%s%s", pclSql, pclTmp);
            
            if(strncmp(rgTM_Daily.CommCfgPart.prSection_SelCri[ilCount].pcSpecifier,"NOT",3)==0)
                strcpy(pclTmp," NOT IN ");
            else
                strcpy(pclTmp," IN ");
 
            sprintf(pclSql,"%s%s(%s) ",pclSql,pclTmp, rgTM_Daily.CommCfgPart.prSection_SelCri[ilCount].pcRange);
        }        
    }

    sprintf(pclSql, "%s AND ( LSTU like '%8.8s%%' OR CDAT like '%8.8s%%' ) AND "
                    "URNO IN  (SELECT FLNU FROM ERPSCH) ",
            pclSql, pcpYesterday, pcpYesterday);           
    dbg(DEBUG, "reprocessERPSCHRecords:: SQL[%s]", pclSql);      
    
    
    memset(&pclDataAreaArr, 0, sizeof(pclDataAreaArr));

    slCursorArr = 0;
    ilTmp=sql_if(START,&slCursorArr,pclSql,pclDataAreaArr);
    if ( ilTmp==RC_FAIL )
    {
    	dbg(TRACE, "reprocessERPSCHRecords:: DB ERROR!!! ");
        ilRC=RC_FAIL;	
    }
    else
    {
        while ( ilTmp==DB_SUCCESS )
        {
            BuildItemBuffer (pclDataAreaArr, "RKEY", 1, ",");
            get_real_item(pclTmp,pclDataAreaArr,1);
            dbg(DEBUG, "reprocessERPSCHRecords:: Processing RKEY[%s]", pclTmp);
            
            ilRC=processERPSCH(pclTmp, pcpDateString);

            ilTmp=sql_if(NEXT,&slCursorArr,pclSql,pclDataAreaArr);         
        }
    }
    close_my_cursor(&slCursorArr); 
    dbg(TRACE, "reprocessERPSCHRecords:: End Result[%d]", ilRC);
    return ilRC;
}

int processAdditionalERPSCHRecords(char *pcpYesterday, char *pcpMonday, char *pcpNextMonday, char *pcpDateString)
{
	int ilRC=RC_SUCCESS;
	char pclSql[1001];
	char pclTmp[1001];
	int ilCount;
    short slCursorArr = 0;
    char pclDataAreaArr[8192];
    int ilTmp;
    
    
    dbg(TRACE, "processAdditionalERPSCHRecords:: Start");
    
    sprintf(pclSql, "SELECT DISTINCT(RKEY)RKEY "
                    "FROM afttab "
                    "WHERE ( (stoa>='%s' AND stoa<'%s') or "
                            "(stod>='%s' AND stod<'%s') ) ",
            pcpMonday, pcpNextMonday, pcpMonday, pcpNextMonday);

    if( rgTM_Daily.CommCfgPart.iNoOfSections_SelCri>0 )
    {
        for(ilCount=0;ilCount < rgTM_Daily.CommCfgPart.iNoOfSections_SelCri; ilCount++)
        {
            sprintf(pclTmp," AND %s ",rgTM_Daily.CommCfgPart.prSection_SelCri[ilCount].pcType);
            sprintf(pclSql,"%s%s", pclSql, pclTmp);
            
            if(strncmp(rgTM_Daily.CommCfgPart.prSection_SelCri[ilCount].pcSpecifier,"NOT",3)==0)
                strcpy(pclTmp," NOT IN ");
            else
                strcpy(pclTmp," IN ");
 
            sprintf(pclSql,"%s%s(%s) ",pclSql,pclTmp, rgTM_Daily.CommCfgPart.prSection_SelCri[ilCount].pcRange);
        }        
    }

    sprintf(pclSql, "%s AND ( LSTU like '%8.8s%%' OR CDAT like '%8.8s%%' ) AND "
                    "URNO NOT IN  (SELECT FLNU FROM ERPSCH) ",
            pclSql, pcpYesterday, pcpYesterday);           
    dbg(DEBUG, "processAdditionalERPSCHRecords:: SQL[%s]", pclSql);      
    
    
    memset(&pclDataAreaArr, 0, sizeof(pclDataAreaArr));

    slCursorArr = 0;
    ilTmp=sql_if(START,&slCursorArr,pclSql,pclDataAreaArr);
    if ( ilTmp==RC_FAIL )
    {
    	dbg(TRACE, "processAdditionalERPSCHRecords:: DB ERROR!!! ");
        ilRC=RC_FAIL;	
    }
    else
    {
        while ( ilTmp==DB_SUCCESS )
        {
            BuildItemBuffer (pclDataAreaArr, "RKEY", 1, ",");
            get_real_item(pclTmp,pclDataAreaArr,1);
            dbg(DEBUG, "processAdditionalERPSCHRecords:: Processing RKEY[%s]", pclTmp);
            
            ilRC=processERPSCH(pclTmp, pcpDateString);

            ilTmp=sql_if(NEXT,&slCursorArr,pclSql,pclDataAreaArr);         
        }
    }
    close_my_cursor(&slCursorArr);  
    dbg(TRACE, "processAdditionalERPSCHRecords:: End Result[%d]", ilRC);
    return ilRC;
}

int rtrim(char *str, char cTrim)
{
    int iRet=0;
    int iSize=strlen(str);
    int a=iSize-1;
    
    for (; a>0; a--)
    {
        if ( str[a]==cTrim )
            str[a]=0;
        else
            break;    	
    }
    
    return iRet;	
}

int ltrim(char *str, char cTrim)
{
	int iRet=0;
	int a=0;
    char *strPtr=malloc(sizeof(char)*(strlen(str)+1));
   
    if ( strPtr )
    {   
    	if ( strlen(str)==0 )
    	    ;
    	else
    	{
            for ( a=0; a<strlen(str); a++)
            {
                if (str[a]!=cTrim)
                    break;	
            }    
    
            if ( a<strlen(str) )
            {
               strcpy(strPtr, &str[a]);	
            }
    
            strcpy(str, strPtr);
        }
        free(strPtr);
        strPtr=NULL;
    }
    else
        dbg(TRACE, "ltrim:: Cannot allocate strPtr");
        
    return iRet;
}

int trim(char *str, char cTrim)
{
	int iRet=0;
	
	ltrim(str, cTrim);
	rtrim(str, cTrim);
	
    return iRet;
}

int replaceChar(char *pcpString, char cpOld, char cpNew)
{
    int ilRC=RC_SUCCESS;
    int ilSize=strlen(pcpString);
    int a=0;
    
    for (a=0; a<ilSize; a++)
    {
        if (pcpString[a]==cpOld)
            pcpString[a]=cpNew;	
    }
    
    return ilRC;	
}

int readConfigFile(char *strFile, char *strHeader, char *strKey, char *strVal)
{
	int iRet=RC_FAIL;
	int a=0;
	int iHeaderFound=0;
	char strKeyTmp[1025], strValTmp[1025];
	char strHeaderTmp[1025];
	char strLine[1025];
	char *strPtr, *strPtr2;
    FILE *fptr;
    
    strcpy(strVal,"");
    
    fptr=fopen(strFile, "r");
    if ( fptr==NULL )	
    {
    	dbg(TRACE,"Cannot open file [%s]", strFile);	
    }
    else
    {
    	if ( !strcmp(strHeader, "") )
    	{
    	    while ( fgets(strLine,1024, fptr) )	
    	    {
    	        if ( strLine[0]=='#' )
        	 	    continue;        	 	       
        	 	
        	 	strLine[strlen(strLine)-1]=0;	 
        	 	strPtr=strtok_r(strLine, "=", &strPtr2);
                if ( strPtr )
                {
                	strcpy(strKeyTmp, strPtr);
                    trim(strKeyTmp, ' ');
                        
                    if ( !strcmp(strKey, strKeyTmp) )
                    {
                     
                        strPtr=strtok_r(NULL, "=", &strPtr2);	
                        if ( strPtr )
                        {
                            strcpy(strValTmp, strPtr);
                            if ( strcmp(strValTmp, " ") )
                                trim(strValTmp, ' ');
                                
                            strcpy(strVal, strValTmp);
                            
                            strPtr=strtok_r(NULL, "=", &strPtr2);	
                            while (strPtr)
                            {
                                strcpy(strValTmp, strPtr);
                                if ( strcmp(strValTmp, " ") )
                                    trim(strValTmp, ' ');
                                sprintf(strVal, "%s=%s", strVal, strValTmp);
                                strPtr=strtok_r(NULL, "=", &strPtr2);	
                            }
                        
                            iRet=RC_SUCCESS;
                            break;
                        }
                    }
                }	
    	    } 
    	}
    	else
    	{
    	    sprintf(strHeaderTmp, "[%s]", strHeader);
            while ( fgets(strLine,1024, fptr) )
            {
                strLine[strlen(strLine)-1]=0;	
                if ( strcmp(strLine, strHeaderTmp) )
                    continue;
                else
                {
                    iHeaderFound=1;	
                    break;
                }                
            }
        
            if ( iHeaderFound )
            {
        	    sprintf(strHeaderTmp, "[%s_END]", strHeader);
        	    while ( fgets(strLine,1024, fptr) )
        	    {
        	 	    if ( strLine[0]=='#' )
        	 	        continue;
        	 	    	
        	 	    strLine[strlen(strLine)-1]=0;	
         	 	    if ( !strcmp(strLine, strHeaderTmp) )
                        break;
                    else 
                    {
                    	strPtr=strtok_r(strLine, "=", &strPtr2);
                        if ( strPtr )
                        {
                            strcpy(strKeyTmp, strPtr);
                            trim(strKeyTmp, ' ');
                        
                            if ( !strcmp(strKey, strKeyTmp) )
                            {
                            	strPtr=strtok_r(NULL, "=", &strPtr2);	

                                if ( strPtr )
                                {
                                    strcpy(strValTmp, strPtr);
                                    if ( strcmp(strValTmp, " ") )
                                        trim(strValTmp, ' ');
                                    strcpy(strVal, strValTmp);
                            
                                    strPtr=strtok_r(NULL, "=", &strPtr2);	
                                    while (strPtr)
                                    {
                                        strcpy(strValTmp, strPtr);
                                        if ( strcmp(strValTmp, " ") )
                                            trim(strValTmp, ' ');
                                        sprintf(strVal, "%s=%s", strVal, strValTmp);
                                        strPtr=strtok_r(NULL, "=", &strPtr2);	
                                    }
                        
                                    iRet=RC_SUCCESS;
                                    break;
                                }
                                
                            }
                        }
                    }
                }
            }	
        }	
        fclose(fptr);
    }
    dbg(TRACE, "readConfigFile:: Key[%s] Value[%s]", strKey, strVal);
    return iRet;
}

int getTableUrno(char *pcpOut, char *pcpSql)
{
    int ilRC=RC_SUCCESS;
    short slCursor;
    char pclData[1024];
    
    dbg(DEBUG, "getTableUrno:: Start");
    strcpy(pcpOut, "");
    ilRC = sql_if (START, &slCursor, pcpSql, pclData);
    dbg(DEBUG, "getTableUrno:: Execute[%s][%d]", pcpSql, ilRC);
    if ( ilRC==RC_FAIL )
    {
        dbg(TRACE, "getTableUrno:: DB ERROR!!!");	
    }
    else if ( ilRC==ORA_NOT_FOUND )
    {
        dbg(TRACE, "getTableUrno:: No record found");
        ilRC=RC_FAIL;
    }
    else
    {
        strcpy(pcpOut, pclData);  	
    }    
    close_my_cursor(&slCursor);
    dbg(DEBUG, "getTableUrno:: End Result[%d] Urno[%s]", ilRC, pcpOut);
    return ilRC;	
}

int ChangeCharFromTo(char *pcpData,char *pcpFrom,char *pcpTo)
{
  int ilRC = RC_SUCCESS;
  int i;
  char pclFunc[]="ChangeCharFromTo:";
  char *pclData;

  if(pcpData == NULL)
    return -1;
  else if(pcpFrom == NULL)
    return -2;
  else if(pcpTo == NULL)
    return -3;
  else
    {
      pclData = pcpData;

      while(*pclData != 0x00)
    {
      for(i=0; pcpFrom[i] != 0x00 ; i++ )
        {
          if(pcpFrom[i] == *pclData)
        *pclData = pcpTo[i];
        }
      pclData++;
    }
    }

  return RC_SUCCESS;
}

int getCKIT(char *pcpOut, char *pcpFlnu)
{
    int ilRC=RC_SUCCESS;
    char pclSql[1024];
    char pclData[1024];
    short slCursor;
    
    dbg(DEBUG, "getCKIT:: START Flnu[%s]", pcpFlnu);
    
    strcpy(pcpOut, "");
    sprintf(pclSql, "SELECT CKIT FROM CCATAB WHERE FLNU='%s' ", pcpFlnu);
    ilRC=sql_if(START, &slCursor, pclSql, pclData);
    dbg(TRACE, "getCKIT:: executing[%s][%d]", pclSql, ilRC);
    
    if ( ilRC==RC_FAIL )
    {
        dbg(TRACE, "getCKIT:: DB ERROR!!!");	
    }
    else if ( ilRC==ORA_NOT_FOUND )
    {
        dbg(TRACE, "getCKIT:: No record found");
        ilRC=RC_SUCCESS;
    }
    else
    {
        BuildItemBuffer(pclData, "CKIT", 1, ",");
        get_real_item(pcpOut, pclData, 1);
    } 
    close_my_cursor(&slCursor);
    
    dbg(DEBUG, "getCKIT:: END Result[%d] pcpOut[%s]", ilRC, pcpOut);  
    return ilRC;	
}
