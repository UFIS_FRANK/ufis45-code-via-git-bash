#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/loahdl.c 1.15 1/2/2013 11:07:58 AM Exp fya $";
#endif /* _DEF_mks_version */
/********************************************************************************
 *          
 * LOAD HANDLER
 *                      
 * Author         : dka 
 * Date           : 20120127                                                    
 * Description    : UFIS-1387 
 *                                                                     
 * Update history :                                                   
 *  20120127 DKA 1.00   First release.
 *  20120328 DKA 1.01    Allow formula arguments with '!' indicator to be skipped and
 *            the rest of arguments to continue to be considered, rather
 *            than abandon the formula.
 *
 *  20121012 WON 1.02    Do formula computation and insert on loatab (formula base on loahdl.cfg and load.cfg)
 *  20121012 WON 1.03    Revise formula for groups
 *  20121022 WON 1.04    Release.
 *  20121022 WON 1.05    Added time when doing loatab computation(DSSN=SYS)
 *  20121109 WON 1.06    Uploaded to AUHTEST
 *  20121206 FYA v1.07   Using SqlArrayInsertTab to insert into LOATAB based on fomulas for LOA command
 *  20121210 FYA v1.08   Replace finding items in config files by in memory
 *  20121228 WON v1.09   SearchConfigOption returns 0 when found, change the condition after the function
 *                       to compare with RC_SUCCESS instead of 1.
 *  20130502 WON v1.10   For Jira 3147.  Same URNO twice in LOATAB.  Insertion of loatab was affected when sqlarray was created.
 *  20130503 WON v1.11   Replace GetNextValue with NewUrno
 *  20120517 WON V1.12   Implemented a new command POP.  POP takes two parameters, start and end date.  Will get all afttab records within the said date 
 *                       and process.
 *  20130521 WON v1.13   Separate formula for arrival and departure.                   
 *  20130521 WON v1.14   
 *  20130521 WON v1.15   Modified to break after finding one dssn. 
 ********************************************************************************/

/**********************************************************************************/
/*                                                                                */
/* be carefule with strftime or similar functions !!!                             */
/*                                                                                */
/**********************************************************************************/
/* This program is a MIKE main program */

#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "db_if.h"
#include "ct.h"
#include "urno_fn.inc"

/******************************************************************************/
/* Macros */
/******************************************************************************/

#define     N_SECTIONS      64   /* no. of sections from cfg */
#define        N_FORMULAE    32   /* no. of formulae per config section */
#define        N_PARAMETERS    128   /* no. of parameter lines per config section */
#define     RC_DATA_ERR 2222 
#define        UTC_TIME  5601
#define        LOCAL_TIME  5602
#define        ENTRY_NAME_SIZE    64
#define        ENTRY_VALUE_SIZE  256
#define     AFT_MSG_SIZE  256

#define        MAX_CTFLT_VALUE 4

//Frank v1.08 20121210
#define LOAHDlLINES 500
#define LOADLINES 1000
#define MAX_RESULT 6
/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/

int  debug_level = DEBUG;
//Frank UFIS-2229
char pcgInformProcess[1024] = "\0";

/******************************************************************************/
/* Declarations                                                               */
/******************************************************************************/
typedef struct
{
    int iFound;
    char DSSN[8];
    char VALU[8];
    int iValue;     
}RESULT;
RESULT rgResult[MAX_RESULT];


typedef struct t_struct_config_formula_s {
  char name [ENTRY_NAME_SIZE];
  char defn [ENTRY_VALUE_SIZE]; 
} t_struct_config_formula;

typedef struct t_struct_config_variable_s {
  char name [ENTRY_NAME_SIZE];
  char dssn [ENTRY_NAME_SIZE]; 
  /* char where [ENTRY_VALUE_SIZE]; */
  char type [32];
  char styp [32];
  char sstp [32];
  char ssst [32];
  char apc3 [32];
} t_struct_config_variable;

typedef struct t_struct_config_section_s {
  char name [ENTRY_NAME_SIZE];
  char aft_fields [ENTRY_VALUE_SIZE];
  char aft_arr_prio [ENTRY_VALUE_SIZE];
  char aft_dep_prio [ENTRY_VALUE_SIZE];
  t_struct_config_formula formula [N_FORMULAE];
  t_struct_config_variable variable [N_PARAMETERS]; 
} t_struct_config_section;

static t_struct_config_section sgConfig [N_SECTIONS];

typedef struct
{
    char *strString;   
    char *strFormula; 
    char *name;
    char *dssn; 
    char *type;
    char *styp;
    char *sstp;
    char *ssst;   
    char *apc3;     
    char valu[32];
}LOALINEAR;

char *cgLoadConfigFile;
char *cgFormulaDSSN;

int iFormula;
LOALINEAR *sFormula;

int iFormulaDep;
LOALINEAR *sFormulaDep;

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static int   igGlNoAck     = 0;          /* flag for acknowledgement */
static int   igQueOut      = 0;
static int   igInitOK      = FALSE;
static char  cgConfigFile[512];
static char  cgCedaSend[10] = "\0";
static char  cgCedaSendPrio[10] = "\0";
static char  cgCedaSendCommand[10] = "\0";
static char  cgHopo[8] = "\0";                         /* default home airport    */
static char  cgTabEnd[8] ="\0";                       /* default table extension */
static int   igModIdSqlhdl = 506;       /* target ID for SQLHDL */
static char  pcgUserName [64];        /* last sender's username if any */
static char  pcgCedaCmd [64];        /* last command */

static char pcgAftSections [128];    /* all the defined sections in config */

static int  igModIdFlight = -1;            /* the FLIGHT process  & it's message */
static char pcgAftCmd [32];
static char pcgAftFld [AFT_MSG_SIZE];
static char pcgAftSel [AFT_MSG_SIZE];
static char pcgAftVal [AFT_MSG_SIZE];
static char pcgAftDestName [64];
static char pcgAftRecvName [64];
static char pcgAftTwStart [64];
static char pcgAftTwEnd [64];
static char pcgAftTable [64];
char pcgDepAdid[16];
char pcgArrAdid[16];

//Frank v1.07
char pcgLoaBuf[32768];
static char pcgNewUrnos[1000*16];
REC_DESC rgRecDesc;

//Frank v1.08
char pcgLoahdlConfigFile[LOAHDlLINES][1024];
char pcgLoadConfigFile[LOADLINES][1024];

int igComplex=0;
/******************************************************************************/
/* For ct library */
/******************************************************************************/

static char pcgLoaTabName[] = "LOATAB";
static char pcgLoaTabGrid[] = "LoaTab";
static char pcgLoaTabFldList[] = "APC3,DSSN,FLNO,FLNU,HOPO,IDNT,RURN,SSST,SSTP,STYP,TIME,TYPE,URNO,VALU";
static char pcgLoaTabFldSize[] = "3   ,3   ,9   ,10  ,3   ,22  ,10  ,3   ,10  ,3   ,14  ,3   ,10  ,6   ";
static char pcgLoaTabNulStrg[] = ",,,0,,,0,,,,,,0,";

static char pcgLoaTabArrivalGrid[]="LoaTabArrival";

static STR_DESC argCtFlValue[MAX_CTFLT_VALUE+1];

static STR_DESC rgCtTabLine;
static STR_DESC rgCtTabData;
static STR_DESC rgCtTabList;
static STR_DESC rgCtTabUrno;

static STR_DESC rgCtFlLine;
static STR_DESC rgCtFlData;
static STR_DESC rgCtHtLine;

/******************************************************************************/
/* Function prototypes                                                          */
/******************************************************************************/
static int  InitPgm();
static int  Reset(void); 
static void Terminate(void);
static void HandleSignal(int);
static void HandleErr(int);         /* General errors */
static void HandleQueErr(int);
static int  HandleData(void);       /* CEDA  event data     */
static void HandleQueues(void);     /* Waiting for Sts.-switch*/

static int  TimeToStr (char *pcpTime,time_t lpTime, int ipFlag); /* handy */
static int  SendToCeda (char *pcpBuffer,char *pcpMyTime,long plpMyReason,
          char *pcpUrno);
static void HandleLOA_Cmd (char *pcpDssn, char *pcpFlnu);
static void LoadAftConfig (void);
static void ReplaceChar(char *pcpBuffer, char clOrgChar, char clDestChar);
/*static void ExtractLoaWhereClauseValues (char *pcpArgDefn,
              char *pcpDssnList, char *pcpWhereClause); */
static void ExtractLoaWhereClauseValues (char *pcpArgDefn,
              char *pcpDssnList, 
              char *pcpType, char *pcpStyp, char *pcpSstp, char *pcpSsst, char *pcpApc3); 
static void PrintLoaConfig (void);
static int  DoSelect (char *pcpFieldList, char *pcpWhereClause, char *pcpTable,
              char *pcpDataBuf);
static int  GetFlightDetails (char *pcpUrno, char *pcpAdid, char *pcpRkey, char *pcpFlno,
              char *pcpSchedule);
static int  ScanConfig (char *pcpAdid, char *pcpDssn, char *pcpFlnu, char *pcpRkey);
static int  InitConfigStructure (void);
static int  GetLoatabValue (char *pcpDssn, char *pcpType, char *pcpStyp,
              char *pcpSstp, char *pcpSsst, char *pcpValu); 
static int  GetVariableValue (int ipSectionIdx, char *pcpVarName,
  char *pcpInputDssn, int *ipVal);


//Frank UFIS-2229
//void SeparateINFORM_PROCESS(char *pcpInformProcess, char *pcpProcessesConfig, char *pcpDSSNConfig);
static int SeparateString(char *pclConfigString,char *pcp1stPart, char *pcp2ndPart);
static int TrimSpace( char *pcpInStr );

int getLoaFormula(char *pcpStart, char *pcpEnd);
int evaluateFormulas(RESULT *rpResult, char *str, int *iValueFlag, int *iValue);
int parseExpression(char *str, char *dssn, char *type, char *styp, char *sstp, char *ssst, char *apc3);
int readFormula(char *str, char *strRet);
int computeFormulas(char *strFlnu, char *strRkey, char *pcpAdid);
int executeSql(char *strSql, char *strErr);
int insertLoaTab(char strSql[][1025], int iSize);
int getGridIndex(char strGrid[], char *strflnu, char *strType, char *strStyp, char *strSstp, char *strSsst, char *strApc3, char *strDssn);

//Frank v1.07
static int FillArray(char *pcpValueList);
static int SqlArrayInsert(char *pcpFldList, char *pcpValList, char *pcpDatList);

//Frank v1.08
static void LoadFilesIntoMemory(void);
static int SearchConfigOption(char *str, char *strRet,char *pcpArrayName);

int processPop(char *pcpStart, char *pcpEnd);
int addResultValue(RESULT *rpResult, char *pcpOp, char *pcpDSSN, char *pcpValu, int ipFound);
int addResult(RESULT *rpResult1, RESULT *rpResult2, char *pcpOp);
void printResult();
int getBestResultIndex();
void printFormulas();
/******************************************************************************/
/* For CT library                                                             */ 
/******************************************************************************/

static int InitMyTab(char *pcpCtTabName, char *pcpCtFldList, char *pcpCtFldSize);
static int LoadGridData(char *pcpGridName, char *pcpTblName, char *pcpSqlKey);
int TAB_TrimRecordData(char *pclTextBuff,char *pcpFldSep);

/******************************************************************************/
/* External functions                                                         */
/******************************************************************************/

extern int get_real_item (char *, char *, int);
extern int get_item_no (char *, char *, int);

/******************************************************************************/
/* Internal variables                                                         */
/******************************************************************************/

#define DATABLK_SIZE (1024)

static char pcgDataArea[DATABLK_SIZE];
static char pcgSqlBuf[DATABLK_SIZE];

int  igDbgSave = DEBUG;
int  igSendToModid = 9560;
int  igSendPrio = 3;
int igComples=0;

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/

MAIN
{
    int    ilRC = RC_SUCCESS;            /* Return code            */
    int    ilCnt = 0;
    
    INITIALIZE;            /* General initialization    */

    dbg(TRACE,"MAIN: version <%s>",mks_version);
    /* handles signal       */
    (void)SetSignals(HandleSignal);
    (void)UnsetSignals();

    /* Attach to the MIKE queues */
    do
    {
        ilRC = init_que();
        if(ilRC != RC_SUCCESS)
        {
            dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        }/* end of if */
    }while((ilCnt < 10) && (ilRC != RC_SUCCESS));
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
        sleep(60);
        exit(1);
    }else{
        dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }/* end of if */

    sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
    ilRC = TransferFile(cgConfigFile);
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */
    sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
    /* uncomment if necessary */
    /* ilRC = TransferFile(cgConfigFile); */
    /* if(ilRC != RC_SUCCESS) */
    /* { */
    /*     dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile); */
    /* } */ /* end of if */
    ilRC = SendRemoteShutdown(mod_id);
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    } /* end of if */
    if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
        dbg(DEBUG,"MAIN: waiting for status switch ...");
        HandleQueues();
    }/* end of if */
    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
        dbg(TRACE,"MAIN: initializing ...");
        if(igInitOK == FALSE)
        {
            ilRC = InitPgm();
            if(ilRC != RC_SUCCESS)
            {
                dbg(TRACE,"MAIN: init failed!");
            } /* end of if */
        }/* end of if */
    } else {
        Terminate();
    }/* end of if */
    dbg(TRACE,"MAIN: initializing OK");
    dbg(TRACE,"==================== Entering main pgm loop  =================");
    for(;;)
    {
        dbg(TRACE,"==================== START/END =================");
        ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text;
                
        if( ilRC == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRC != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRC);
            } /* fi */
            
            switch( prgEvent->command )
            {
            case    HSB_STANDBY    :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;    
            case    HSB_COMING_UP    :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;    
            case    HSB_ACTIVE    :
                ctrl_sta = prgEvent->command;
                break;    
            case    HSB_ACT_TO_SBY    :
                ctrl_sta = prgEvent->command;
                /* CloseConnection(); */
                HandleQueues();
                break;    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate();
                break;    
            case    HSB_STANDALONE    :
                ctrl_sta = prgEvent->command;

                break;    

            case    SHUTDOWN    :
                /* process shutdown - maybe from uutil */
                Terminate();
                break;
                    
            case    RESET        :
                ilRC = Reset();
                break;
                    
            case    EVENT_DATA    :
                if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
                {
                    ilRC = HandleData();
                    if(ilRC != RC_SUCCESS)
                    {
                        HandleErr(ilRC);
                    }/* end of if */
                }else{
                    dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
                    DebugPrintItem(TRACE,prgItem);
                    DebugPrintEvent(TRACE,prgEvent);
                }/* end of if */
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
            default            :
                dbg(TRACE,"MAIN: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */ 
        }
   
    } /* end for */
    
    exit(0);
    
} /* end of MAIN */

/******************************************************************************
 * ReadConfigEntry
 ******************************************************************************
 */

static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,
     char *pcpCfgBuffer, char *pcpDefault)
{
    int ilRC = RC_SUCCESS;

    char clSection[256] = "\0";
    char clKeyword[256] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRC = iGetConfigEntry(cgConfigFile,clSection,clKeyword,
               CFG_STRING,pcpCfgBuffer);
    if(ilRC != RC_SUCCESS)
    {
    dbg(TRACE,"ReadConfigEntry: Not found in %s: [%s] <%s>",cgConfigFile,clSection,clKeyword);
    dbg(TRACE,"ReadConfigEntry: use default-value: <%s>",pcpDefault);
    strcpy(pcpCfgBuffer,pcpDefault);
    } 
    else
    {
    dbg(TRACE,"ReadConfigEntry: Config Entry [%s],<%s>:<%s> found in %s",
        clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
    }/* end of if */
    return ilRC;
}

int getLoaFormula(char *pcpStart, char *pcpEnd)
{
    int iRet=RC_SUCCESS;
    int iTmp=0;
    int iFound=0;
    char pclTmp[1025];
    char pclField[1025];
    char pclValue[1025];
    int a;
    char *strPtr;
    char *strPtr2;
    FILE *fptr;
    LOALINEAR *sLoaLinearRealloc;
    int iFormulaTmp;
    LOALINEAR *sFormulaTmp;
    
    dbg(DEBUG,"getLoaFormula:: Start");
    dbg(DEBUG, "getLoaFormula:: Start[%s], End[%s]", pcpStart, pcpEnd);
    
    memset(pclTmp, 0, sizeof(pclTmp));
    ReadConfigEntry("MAIN","LOAD_CFG_FILE", pclTmp, "");
    if ( !strcmp(pclTmp, "") )
    {
        iRet=RC_FAIL;
        dbg(TRACE,"getLoaFormula:: Error!!! Load Configuration File [LOAD_CFG_FILE] missing");    
    }
    else
    {
        cgLoadConfigFile=(char *)malloc(sizeof(char)*(strlen(pclTmp)+1));
        if ( cgLoadConfigFile )
            strcpy(cgLoadConfigFile, pclTmp);
        else
        {
            iRet=RC_FAIL;
            dbg(TRACE, "getLoaFormula:: Critical Error!!! Cannot allocate cgLoadConfigFile");
        }        
    }
            
    if ( iRet==RC_SUCCESS )
    {        
        memset(pclTmp, 0, sizeof(pclTmp));        
        ReadConfigEntry("MAIN","LOAD_FORMULA_DSSN", pclTmp, "SYS");
        
        cgFormulaDSSN=(char *)malloc(sizeof(char)*(strlen(pclTmp)+1));
        if ( cgFormulaDSSN )
            strcpy(cgFormulaDSSN, pclTmp);
        else
        {
            iRet=RC_FAIL;
            dbg(TRACE, "getLoaFormula:: Critical Error!!! Cannot allocate cgFormulaDSSN");
        }        
    }

    if ( iRet==RC_SUCCESS )
    {        
        ReadConfigEntry("MAIN","DEPARTURE_ADID", pcgDepAdid, "D");
        ReadConfigEntry("MAIN","ARRIVAL_ADID", pcgArrAdid, "A");
    }
         
    if ( iRet==RC_SUCCESS )
    {
        iFound=0;
        fptr=fopen(cgConfigFile, "r");
        while ( fgets(pclTmp, 1024, fptr) )
        {
            if ( pclTmp[0]=='#' )
                continue;
                
            if ( pclTmp[strlen(pclTmp)-1]=='\n' )
                pclTmp[strlen(pclTmp)-1]=0;
                
            if ( !strcmp(pclTmp, pcpStart) )
            {
                iFound=1;
                break;    
            }       
        }
        
        if ( iFound )
        {
            sFormulaTmp=NULL;
            iFormulaTmp=0;
            while ( fgets(pclTmp, 1024, fptr) )
            {
                  if ( pclTmp[0]=='#' )
                    continue;
                
                if ( pclTmp[strlen(pclTmp)-1]=='\n' )
                    pclTmp[strlen(pclTmp)-1]=0;
                
                dbg(DEBUG,"getLoaFormula:: Reading [%s]", pclTmp);
                if ( !strcmp(pclTmp, pcpEnd) )
                {
                    break;    
                } 
                else if ( strcmp(pclTmp, "") )
                {
                    sLoaLinearRealloc=(LOALINEAR *)realloc(sFormulaTmp, (sizeof(LOALINEAR)*(iFormulaTmp+1)));
                    if (  sLoaLinearRealloc==NULL )
                    {
                        iRet=RC_FAIL;
                        dbg(TRACE, "getLoaFormula:: Critical Error!!! Cannot reallocate sFormula");                        
                    }
                    else
                    {
                        sFormulaTmp=sLoaLinearRealloc;
                        memset(&sFormulaTmp[iFormulaTmp], 0, sizeof(LOALINEAR));
                        sFormulaTmp[iFormulaTmp].strString=(char *)malloc(sizeof(char)*(strlen(pclTmp)+1));
                        if ( sFormulaTmp[iFormulaTmp].strString )
                            strcpy(sFormulaTmp[iFormulaTmp].strString, pclTmp);
                        else
                        {
                            iRet=RC_FAIL;
                            dbg(TRACE, "getLoaFormula:: Critical Error!!! Cannot allocate sFormula->strString");                             
                        } 
                        
                        strPtr=pclTmp;
                        if ( iRet==RC_SUCCESS )
                        {
                            while (strPtr[0]!=0)
                            {
                                if ( strPtr[0]!=' ' && strPtr[0]!=0 )
                                    break;
                                strPtr++;    
                            }
                        
                            a=0;
                            memset(pclField, 0, sizeof(pclField)); 
                            while (strPtr[0]!=0)
                            {
                                if (strPtr[0]==' ' || strPtr[0]    == '=')
                                    break;
                                pclField[a++]=strPtr[0];  
                                strPtr++;
                            }                                

                            sFormulaTmp[iFormulaTmp].name=(char *)malloc(sizeof(char)*(strlen(pclField)+1));
                            if ( sFormulaTmp[iFormulaTmp].name )
                                strcpy(sFormulaTmp[iFormulaTmp].name, pclField);
                            else
                            {
                                iRet=RC_FAIL;
                                dbg(TRACE, "getLoaFormula:: Critical Error!!! Cannot allocate sFormula->name");                             
                            }                             
                            
                            sFormulaTmp[iFormulaTmp].dssn=(char *)malloc(sizeof(char)*(strlen(cgFormulaDSSN)+1));
                            if ( sFormulaTmp[iFormulaTmp].dssn )
                                strcpy(sFormulaTmp[iFormulaTmp].dssn, cgFormulaDSSN);
                            else
                            {
                                iRet=RC_FAIL;
                                dbg(TRACE, "getLoaFormula:: Critical Error!!! Cannot allocate sFormula->dssn");                             
                            } 
                        }

                        
                        if ( iRet==RC_SUCCESS )
                        {
                            while ( strPtr[0]!=0 )
                            {
                                if ( strPtr[0]==' ' || strPtr[0]=='=' || strPtr[0]=='[' ) 
                                    strPtr++;
                                else
                                    break;
                            }
                                           
                            a=0;
                            memset(pclField, 0, sizeof(pclField)); 
                            while (strPtr[0]!=0)
                            {
                                if (strPtr[0]==' ' || strPtr[0]    == ',' || strPtr[0]==']')
                                    break;
                                pclField[a++]=strPtr[0];  
                                strPtr++;
                            }

                            sFormulaTmp[iFormulaTmp].type=(char *)malloc(sizeof(char)*(strlen(pclField)+1));
                            if ( sFormulaTmp[iFormulaTmp].type )
                                strcpy(sFormulaTmp[iFormulaTmp].type, pclField);
                            else
                            {
                                iRet=RC_FAIL;
                                dbg(TRACE, "getLoaFormula:: Critical Error!!! Cannot allocate sFormula->type");                             
                            }                           
                        }    
                             
                        if ( iRet==RC_SUCCESS )
                        {
                            while ( strPtr[0]!=0 )
                            {
                                if ( strPtr[0]==' ' || strPtr[0]==',' ) 
                                    strPtr++;
                                else
                                    break;
                            }
                                           
                            a=0;
                            memset(pclField, 0, sizeof(pclField)); 
                            while (strPtr[0]!=0)
                            {
                                if (strPtr[0]==' ' || strPtr[0]    == ',' || strPtr[0]==']')
                                    break;
                                pclField[a++]=strPtr[0];  
                                strPtr++;
                            }
                            
                            if ( !strcmp(pclField, "#") || !strcmp(pclField, ""))
                                strcpy(pclField, " ");
                                
                            sFormulaTmp[iFormulaTmp].styp=(char *)malloc(sizeof(char)*(strlen(pclField)+1));
                            if ( sFormulaTmp[iFormulaTmp].styp )
                                strcpy(sFormulaTmp[iFormulaTmp].styp, pclField);
                            else
                            {
                                iRet=RC_FAIL;
                                dbg(TRACE, "getLoaFormula:: Critical Error!!! Cannot allocate sFormula->styp");                             
                            }                             
                                                        
                        }    
                           
                        if ( iRet==RC_SUCCESS )
                        {
                            while ( strPtr[0]!=0 )
                            {
                                if ( strPtr[0]==' ' || strPtr[0]==',' ) 
                                    strPtr++;
                                else
                                    break;
                            }
                                           
                            a=0;
                            memset(pclField, 0, sizeof(pclField)); 
                            while (strPtr[0]!=0)
                            {
                                if (strPtr[0]==' ' || strPtr[0]    == ',' || strPtr[0]==']')
                                    break;
                                pclField[a++]=strPtr[0];  
                                strPtr++;
                            }
                            
                            if ( !strcmp(pclField, "#") || !strcmp(pclField, ""))
                                strcpy(pclField, " ");
                                
                            sFormulaTmp[iFormulaTmp].sstp=(char *)malloc(sizeof(char)*(strlen(pclField)+1));
                            if ( sFormulaTmp[iFormulaTmp].sstp )
                                strcpy(sFormulaTmp[iFormulaTmp].sstp, pclField);
                            else
                            {
                                iRet=RC_FAIL;
                                dbg(TRACE, "getLoaFormula:: Critical Error!!! Cannot allocate sFormula->sstp");                             
                            }                        
                        }  

                        if ( iRet==RC_SUCCESS )
                        {
                            while ( strPtr[0]!=0 )
                            {
                                if ( strPtr[0]==' ' || strPtr[0]==',' ) 
                                    strPtr++;
                                else
                                    break;
                            }
                                           
                            a=0;
                            memset(pclField, 0, sizeof(pclField)); 
                            while (strPtr[0]!=0)
                            {
                                if (strPtr[0]==' ' || strPtr[0]    == ',' || strPtr[0]==']')
                                    break;
                                pclField[a++]=strPtr[0];  
                                strPtr++;
                            }
                            
                            if ( !strcmp(pclField, "#") || !strcmp(pclField, "") )
                                strcpy(pclField, " ");
                                
                            sFormulaTmp[iFormulaTmp].ssst=(char *)malloc(sizeof(char)*(strlen(pclField)+1));
                            if ( sFormulaTmp[iFormulaTmp].ssst )
                                strcpy(sFormulaTmp[iFormulaTmp].ssst, pclField);
                            else
                            {
                                iRet=RC_FAIL;
                                dbg(TRACE, "getLoaFormula:: Critical Error!!! Cannot allocate sFormula->ssst");                             
                            }                        
                        }    

                        if ( iRet==RC_SUCCESS )
                        {
                            while ( strPtr[0]!=0 )
                            {
                                if ( strPtr[0]==' ' || strPtr[0]==',' ) 
                                    strPtr++;
                                else
                                    break;
                            }
                                           
                            a=0;
                            memset(pclField, 0, sizeof(pclField)); 
                            while (strPtr[0]!=0)
                            {
                                if (strPtr[0]==' ' || strPtr[0]    == ',' || strPtr[0]==']')
                                    break;
                                pclField[a++]=strPtr[0];  
                                strPtr++;
                            }
                            
                            if ( !strcmp(pclField, "#") || !strcmp(pclField, "") )
                                strcpy(pclField, " ");                           
                            else if ( !strcmp(pclField, "-") )
                                strcpy(pclField, cgHopo);
                                
                            sFormulaTmp[iFormulaTmp].apc3=(char *)malloc(sizeof(char)*(strlen(pclField)+1));
                            if ( sFormulaTmp[iFormulaTmp].apc3 )
                                strcpy(sFormulaTmp[iFormulaTmp].apc3, pclField);
                            else
                            {
                                iRet=RC_FAIL;
                                dbg(TRACE, "getLoaFormula:: Critical Error!!! Cannot allocate sFormula->apc3");                             
                            }                        
                        }  
                        
                        if ( iRet==RC_SUCCESS )
                        {
                            while ( strPtr[0]!=0 )
                            {
                                if ( strPtr[0]==' ' || strPtr[0]==',' || strPtr[0]==']' || strPtr[0]=='[') 
                                    strPtr++;
                                else
                                    break;
                            }                            
                        }
                        
                        if ( strPtr )
                        {
                            if ( strPtr[strlen(strPtr)-1]==']' )
                                strPtr[strlen(strPtr)-1]=0;
                            sFormulaTmp[iFormulaTmp].strFormula=(char *)malloc(sizeof(char)*(strlen(strPtr)+1));
                            if ( sFormulaTmp[iFormulaTmp].strFormula )
                                strcpy(sFormulaTmp[iFormulaTmp].strFormula, strPtr);
                        }
                            
                        iFormulaTmp++;    
                    } 
                }      
            }            
        }

    }
    
    if ( !strcmp(pcpStart, "[ARR_LOAD_SECTIONS]") )
    {
        iFormula=iFormulaTmp;
        sFormula=sFormulaTmp;        
    }
    else
    {
        iFormulaDep=iFormulaTmp;
        sFormulaDep=sFormulaTmp;       
    }
    return iRet;    
}

int evaluateFormulas(RESULT *rpResult, char *str, int *iValueFlag, int *iValue)
{
    int iRet=RC_SUCCESS;
    char pclLoaType [32], pclLoaStyp [32], pclLoaSstp [32];
    char pclLoaSsst [32], pclLoaValu [32], pclLoaDssn [32];
    char pclLoaApc3 [32], pclLoaUrno [32], pclLoaFlnu [32];    
    char *strPtr, *strPtr2;
    char *strPtr3, *strPtr4;
    char strParse[1025];
    char cUnary;
    char strOp[32];
    char strField[128];
    char strTmp[1025];
    char strLine[1025];
    char dssn[1025];
    char type[32];
    char styp[32];
    char sstp[32];
    char ssst[32];
    char apc3[32];
    int a=0;
    int iTmp = 0;
    int iSize;
    int iSizeArrival;
    int iSizeIndex;
    char *strGrid;
    int iGridIndex;   
    int iGridArrivalIndex; 
    int iFirst=1;
    char *pclFunc = "evaluateFormulas";
    
    dbg(DEBUG, "evaluateFormulas:: str[%s] ", str);

    memset(rpResult, 0, sizeof(RESULT)*MAX_RESULT);
    *iValue=0;
    *iValueFlag=0;
    memset(strOp, 0, sizeof(strOp));

    iSize = CT_GetLineCount(pcgLoaTabGrid);     
    iSizeArrival = CT_GetLineCount(pcgLoaTabArrivalGrid);     

    strcpy(strTmp, str);
    strPtr=strtok_r(strTmp, ",", &strPtr2);
    while ( strPtr )
    {
        if ( strPtr[0]=='(' )
        {
            while ( strPtr )
            {
                if ( isalpha(strPtr[0]) )
                    break;
                strPtr++;    
            }

            memset(strParse,0,sizeof(strParse));
            a=0;

            while (strPtr)
            {
                if ( strPtr[0]==' ' || strPtr[0]==')' )
                    break;
                strParse[a++]=strPtr[0];
                strPtr++;
            }  
            
            memset(strLine, 0, sizeof(strLine));
                        
            dbg(DEBUG, "evaluateFormulas:: COMPLEX FORMULA Start [%s]", strParse);
            iTmp = SearchConfigOption(strParse, strLine,"LOAHDL");
            if(iTmp == RC_FAIL)
            {
                iTmp = SearchConfigOption(strParse, strLine,"LOAD");
            }
            
            if ( iTmp==RC_SUCCESS )
            {            
                int iValueFlagTmp=0;
                int iValueTmp=0;
                RESULT rlTmp[MAX_RESULT];
                
                memset(rlTmp, 0, sizeof(RESULT)*MAX_RESULT);                    
                strPtr3=strtok_r(strLine, "[", &strPtr4);
                strPtr3=strtok_r(NULL, "[", &strPtr4);
                strPtr3=strtok_r(NULL, "[", &strPtr4);
                
                if ( strPtr3 )
                {
                    strPtr3[strlen(strPtr3)-1]=0;
                    dbg(DEBUG, "evaluateFormulas:: COMPLEX FORMULA Breakdown [%s]", strPtr3);
                    evaluateFormulas(rlTmp, strPtr3, &iValueFlagTmp, &iValueTmp); 
                    igComplex=1;
                    
                    dbg(DEBUG, "evaluateFormulas:: COMPLEX FORMULA Adding result[%s] to total", strPtr3);                    
                    addResult(rpResult, rlTmp, strOp);
                    
                    if ( iValueFlagTmp )
                    {
                        if ( *iValueFlag==0 )
                            *iValueFlag=1;
                        
                        if ( iFirst )
                            *iValue=iValueTmp;
                        else if ( !strcmp(strOp, "+") )
                            *iValue+=iValueTmp;
                        else if ( !strcmp(strOp, "-") )
                            *iValue-=iValueTmp;
                        else if ( !strcmp(strOp, "*") )
                            *iValue*=iValueTmp;
                        else if ( !strcmp(strOp, "/") )
                           *iValue/=iValueTmp;
                        dbg(DEBUG, "evaluateFormulas:: Running Total Complex[%d]", *iValue);                          
                    }
                    dbg(DEBUG, "evaluateFormulas:: COMPLEX FORMULA End [%s][%d]", strPtr3, *iValue);
                }
            }    
            else
                dbg(DEBUG, "evaluateFormulas:: ERROR!!! COMPLEX FORMULA NOT FOUND [%s]", strParse);           
        }   
        else
        {
            if ( strPtr[0]=='-' || strPtr[0]=='+' )
            {
                cUnary=strPtr[0];
                strPtr++;       
            }
            else if ( isalnum(strPtr[0]) )
            {
                cUnary='+';        
            } 
            
            if ( isalpha(strPtr[0]) )
            {
                memset(dssn, 0, sizeof(dssn));
                memset(type, 0, sizeof(type));
                memset(styp, 0, sizeof(styp));
                memset(sstp, 0, sizeof(sstp));
                memset(ssst, 0, sizeof(ssst));
                memset(apc3, 0, sizeof(apc3));               
                
                memset(strLine,0,sizeof(strLine));
                iTmp = SearchConfigOption(strPtr, strLine,"LOAHDL");
                if(iTmp == RC_FAIL)
                {
                    iTmp = SearchConfigOption(strPtr, strLine,"LOAD");
                }
                if(iTmp == RC_SUCCESS)
                {
                    ExtractLoaWhereClauseValues (strLine, dssn, type, styp, sstp, ssst, apc3) ;
                }
                
                dbg(DEBUG, "evaluateFormulas:: strPtr[%s], Found[%d] dssn[%s], type[%s], styp[%s], sstp[%s], ssst[%s], apc3[%s], strOp[%s]",
                    strPtr, iTmp, dssn, type, styp, sstp, ssst, apc3, strOp);
                                    
                if (
                       !strcmp(strPtr, "LOAD_TRANSIT_PAX_FIRST_DEP") || 
                       !strcmp(strPtr, "LOAD_TRANSIT_PAX_BUSINESS_DEP") || 
                       !strcmp(strPtr, "LOAD_TRANSIT_PAX_ECONOMY_DEP") || 
                       !strcmp(strPtr, "LOAD_TRANSIT_PAX_ECONOMY2_DEP") || 
                       !strcmp(strPtr, "LOAD_TRANSIT_PAX_INFANT_DEP") || 
                       !strcmp(strPtr, "LOAD_TRANSIT_PAX_TOTAL_DEP") ||
                       !strcmp(strPtr, "LOAD_TRANSIT_BAG_WT_DEP") ||
                       !strcmp(strPtr, "LOAD_TRANSIT_BAG_QTY_DEP") 
                   )
                {
                    strGrid=pcgLoaTabArrivalGrid;
                    iSizeIndex=iSizeArrival;
                } 
                else
                {
                    strGrid=pcgLoaTabGrid;
                    iSizeIndex=iSize;
                } 
                if ( iTmp==RC_SUCCESS )
                {
                    strPtr3=strtok_r(dssn, ",", &strPtr4);
                    while (strPtr3)
                    {
                        TrimSpace(strPtr3);
                        for (iGridIndex=0;iGridIndex<iSizeIndex;iGridIndex++)
                        {
                            CT_GetMultiValues(strGrid, iGridIndex, "URNO", pclLoaUrno);
                            CT_GetMultiValues(strGrid, iGridIndex, "FLNU", pclLoaFlnu);
                            CT_GetMultiValues(strGrid, iGridIndex, "DSSN", pclLoaDssn);
                            CT_GetMultiValues(strGrid, iGridIndex, "TYPE", pclLoaType);
                            CT_GetMultiValues(strGrid, iGridIndex, "STYP", pclLoaStyp);
                            CT_GetMultiValues(strGrid, iGridIndex, "SSTP", pclLoaSstp);
                            CT_GetMultiValues(strGrid, iGridIndex, "SSST", pclLoaSsst);
                            CT_GetMultiValues(strGrid, iGridIndex, "APC3", pclLoaApc3);
                            CT_GetMultiValues(strGrid, iGridIndex, "VALU", pclLoaValu);
                                      
                            if ( 
                                       !strcmp(pclLoaDssn, strPtr3) &&
                                       !strcmp(pclLoaType, type) &&
                                       !strcmp(pclLoaStyp, styp) &&
                                       !strcmp(pclLoaSstp, sstp) &&
                                       !strcmp(pclLoaSsst, ssst) &&
                                       !strcmp(pclLoaApc3, apc3) 
                                   )
                            { 
                                   break;
                            }
                        }         
                        
                           
                        if ( iGridIndex < iSizeIndex )
                        {
                            dbg(DEBUG, "evaluateFormulas:: addResultValue 1");
                            addResultValue(rpResult, strOp, strPtr3, pclLoaValu, TRUE);
                            break;
                        }
                        strPtr3=strtok_r(NULL, ",", &strPtr4);
                    }
                    
                    if ( iGridIndex<iSizeIndex )
                    {
                        dbg(DEBUG,"evaluateFormulas:: Record FOUND in Loatab: DSSN[%s] VALU[%s]", pclLoaDssn, pclLoaValu);
                        if ( !strcmp(pclLoaValu, "") )
                            ;
                        else
                        {
                            if ( *iValueFlag==0 )
                                *iValueFlag=1;
                            
                            if ( iFirst )
                                *iValue=atoi(pclLoaValu);
                               else if ( !strcmp(strOp, "+") )
                                *iValue+=atoi(pclLoaValu);
                            else if ( !strcmp(strOp, "-") )
                                *iValue-=atoi(pclLoaValu);
                            else if ( !strcmp(strOp, "*") )
                                *iValue*=atoi(pclLoaValu);
                            else if ( !strcmp(strOp, "/") )
                                *iValue/=atoi(pclLoaValu);
                            dbg(DEBUG, "evaluateFormulas:: Running Total Expression[%d]", *iValue);                                                 
                        }
                    }
                    else
                        dbg(DEBUG,"computeFormulas:: Record NOT FOUND in Loatab    ");
                     
                }
            }
            else if ( isdigit(strPtr[0]) )
            {         
                 sprintf(strField, "%c%s", cUnary, strPtr);    
                 dbg(DEBUG, "evaluateFormulas:: addResultValue 3");

                 addResultValue(rpResult, strOp, "", strField, FALSE);
                                  
                 if ( *iValueFlag==0 )
                     *iValueFlag=1;    
                 
                 if ( iFirst )
                     *iValue=atoi(strField);
                 else if ( !strcmp(strOp, "+") )
                     *iValue+=atoi(strField);
                 else if ( !strcmp(strOp, "-") )
                      *iValue-=atoi(strField);
                 else if ( !strcmp(strOp, "*") )
                     *iValue*=atoi(strField);
                 else if ( !strcmp(strOp, "/") )
                     *iValue/=atoi(strField);        
                     
                 dbg(DEBUG, "evaluateFormulas:: Running Total Digit[%d]", *iValue);                
            }  
        }
                        
        strPtr=strtok_r(NULL, ",", &strPtr2);        
        if ( strPtr )
        {
            strcpy(strOp, strPtr);      
            iFirst=0;                     
        }
        strPtr=strtok_r(NULL, ",", &strPtr2);
    }
 
    dbg(DEBUG, "evaluateFormulas:: str[%s] iValueFlag[%d] iValue[%d] Result[%d] ", str, *iValueFlag, *iValue, iRet);
    return iRet;    
}

int readFormula(char *str, char *strRet)
{
    int iRet=RC_SUCCESS;
    FILE *fptr;
    char strLine[1025];
    
    dbg(DEBUG, "readFormula:: str[%s]", str);
 
    fptr=fopen(cgConfigFile, "r");
    dbg(DEBUG, "readFormula:: Opening Default Config File[%s][%d]", cgConfigFile, fptr);

    if ( fptr )
    {
        while (fgets(strLine, 1024, fptr))
        {
            if ( strLine[0]=='#' )
                continue;
                
            if ( strcmp(strLine, "") )
                strLine[strlen(strLine)-1]=0;
                
            if ( !strncmp(str, strLine, strlen(str)) )
            {
                iRet=1;
                break;    
            }    
        }    
        fclose(fptr);
    }
    else
        iRet=RC_FAIL;

        
    if ( iRet==0 )
    {
        fptr=fopen(cgLoadConfigFile, "r");
        dbg(DEBUG, "readFormula:: Opening File[%s][%d]", cgLoadConfigFile, fptr);        
        if ( fptr )
        {
            while (fgets(strLine, 1024, fptr))
            {
                if ( strLine[0]=='#' )
                    continue;
                    
                if ( strcmp(strLine, "") )
                    strLine[strlen(strLine)-1]=0;

                if ( !strncmp(str, strLine, strlen(str)) )
                {
                    iRet=1;
                    break;    
                }

            }    
            fclose(fptr); 
        }                   
    }
    
    if ( iRet==1 )
        strcpy(strRet, strLine);
    else
        strcpy(strRet, "");

    dbg(DEBUG, "readFormula:: str[%s] Result[%d]", str, iRet);    
    return iRet;
}

//SearchConfigOption(strParse, strLine,pcgLoahdlConfigFile);
//SearchConfigOption(strParse, strLine,pcgLoadConfigFile);
static int SearchConfigOption(char *str, char *strRet,char *pcpArrayName)
{
    char * pclFunc = "SearchConfigOption";
    int ilCount = 0;
    int ilRc = RC_FAIL;
    
    //char pcgLoahdlConfigFile[LOAHDlLINES][1024];
    //char pcgLoadConfigFile[LOADLINES][1024];
    
    for(ilCount=0;ilCount < LOAHDlLINES;ilCount++)
    {
        //dbg(DEBUG,"%s:pcgLoahdlConfigFile[%d]<%s>",pclFunc,ilCount,pcgLoahdlConfigFile[ilCount]);
        //ilRc = RC_FAIL;
        
        if(strcmp(pcpArrayName,"LOAHDL")==0)
        {
            if(strncmp(str,pcgLoahdlConfigFile[ilCount],strlen(str)) == 0)
            {
                strcpy(strRet, pcgLoahdlConfigFile[ilCount]);
                dbg(DEBUG,"%s: Found in loahdl.cfg<%s>",pclFunc,strRet);
                ilRc = RC_SUCCESS;
                break;
            }
        }
        else if(strcmp(pcpArrayName,"LOAD")==0)
        {
            if(strncmp(str,pcgLoadConfigFile[ilCount],strlen(str)) == 0)
            {
                strcpy(strRet, pcgLoadConfigFile[ilCount]);
                dbg(DEBUG,"%s: Found in load.cfg<%s>",pclFunc,strRet);
                ilRc = RC_SUCCESS;
                break;
            }
        }
    }
    return ilRc;
}

int parseExpression(char *str, char *dssn, char *type, char *styp, char *sstp, char *ssst, char *apc3)
{
    int iRet=0;
    int iTmp=0;    
    char strLine[1025];
    FILE *fptr;
    
    dbg(DEBUG, "parseExpression:: str[%s]", str);
    
    fptr=fopen(cgConfigFile, "r");
    if ( fptr )
    {
        while (fgets(strLine, 1024, fptr))
        {
            if ( strLine[0]=='#' )
                 continue;
                 
            if ( strcmp(strLine, "") )
                strLine[strlen(strLine)-1]=0;
                
            if ( !strncmp(str, strLine, strlen(str)) )
            {
                iRet=1;
                break;    
            }    
        }    
        fclose(fptr);
    }

    if ( iRet==0 )
    {
        fptr=fopen(cgLoadConfigFile, "r");
         if ( fptr )
        {
            while (fgets(strLine, 1024, fptr))
            {
                if ( strLine[0]=='#' )
                    continue;
                 
                if ( strcmp(strLine, "") )
                    strLine[strlen(strLine)-1]=0;

                if ( !strncmp(str, strLine, strlen(str)) )
                {
                    iRet=1;
                    break;    
                }

            }    
            fclose(fptr); 
        }                   
    }

    if ( iRet )
        ExtractLoaWhereClauseValues (strLine, dssn, type, styp, sstp, ssst, apc3) ;


    dbg(DEBUG, "parseExpression:: str[%s], dssn[%s], type[%s], styp[%s], sstp[%s], ssst[%s], apc3[%s], Result[%d]", 
        str, dssn, type, styp, sstp, ssst, apc3, iRet);

    return iRet;
}

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/

static int InitPgm()
{
 int    ilRC = RC_SUCCESS;            /* Return code */
 char     pclDbgLevel [iMIN_BUF_SIZE];
 char     pclDummy [iMIN_BUF_SIZE];
 char     pclDummy2 [iMIN_BUF_SIZE],pclTmpBuf [64];
 int ilCnt = 0 ;
 int ilItem = 0 ;
 int ilLoop = 0 ;
 int jj, kk;

  /* now reading from configfile or from database */
  SetSignals(HandleSignal);

  do
  {
    ilRC = init_db();
    if (ilRC != RC_SUCCESS)
    {
      check_ret(ilRC);
      dbg(TRACE,"InitPgm: init_db() failed! waiting 6 sec ...");
      sleep(6);
      ilCnt++;
    } /* end of if */
  } while((ilCnt < 10) && (ilRC != RC_SUCCESS));

  if(ilRC != RC_SUCCESS)
  {
    dbg(TRACE,"InitPgm: init_db() failed! waiting 60 sec ...");
    sleep(60);
    exit(2);
  }else{
    dbg(TRACE,"InitPgm: init_db() OK!");
  } /* end of if */

  (void) ReadConfigEntry("MAIN","DEBUG_LEVEL",pclDbgLevel,"TRACE");
  if (strcmp(pclDbgLevel,"DEBUG") == 0)
  {
    dbg(DEBUG,"InitPgm: DEBUG_LEVEL IS <DEBUG>");
    igDbgSave = DEBUG;
  } 
  else if (strcmp(pclDbgLevel,"OFF") == 0) 
  {
    dbg(DEBUG,"InitPgm: DEBUG_LEVEL IS <OFF>");
    igDbgSave = 0;
  } 
  else 
  {
    dbg(DEBUG,"InitPgm: DEBUG_LEVEL IS <TRACE>");
    igDbgSave = TRACE;
  }

  igModIdFlight = tool_get_q_id ("flight");
  if ((igModIdFlight == RC_FAIL) || (igModIdFlight == RC_NOT_FOUND))
  {
    dbg(TRACE,"InitPgm: ====== ERROR ====== Flight Mod Id not found, default to 7800");
    igModIdFlight = 7800; 
  }
  else
    dbg(TRACE,"InitPgm: Flight Mod Id <%d>", igModIdFlight);

  (void) ReadConfigEntry("MAIN","AFT_SECTIONS", pcgAftSections, "NONE");
   //Frank UFIS-2229  
  (void) ReadConfigEntry("MAIN","INFORM_PROCESS",pcgInformProcess,"NONE");  
  dbg(TRACE,"pcgInformProcess<%s>",pcgInformProcess);

  (void) InitConfigStructure ();

  /*
    Fill up the global configuration structure
  */

  LoadAftConfig ();
  PrintLoaConfig ();
  if(ilRC == RC_SUCCESS)
  {
    /* read HomeAirPort from SGS.TAB */
    ilRC = tool_search_exco_data("SYS", "HOMEAP", cgHopo);
    if (ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"InitPgm: EXTAB,SYS,HOMEAP not found in SGS.TAB");
      Terminate();
    }
    else
    {
      dbg(TRACE,"InitPgm: home airport <%s>",cgHopo);
    }
  }

  if(ilRC == RC_SUCCESS)
  { 
    ilRC = tool_search_exco_data("ALL","TABEND", cgTabEnd);
    if (ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"InitPgm: EXTAB,ALL,TABEND not found in SGS.TAB");
      Terminate();
    }
    else
    {
      dbg(TRACE,"InitPgm: table extension <%s>",cgTabEnd);
    }
  }

  InitMyTab(pcgLoaTabGrid,pcgLoaTabFldList,pcgLoaTabFldSize);
  InitMyTab(pcgLoaTabArrivalGrid,pcgLoaTabFldList,pcgLoaTabFldSize);
  strcpy (pcgAftCmd,"UFR");   

  igInitOK = TRUE;
  debug_level = igDbgSave;

  ilRC=getLoaFormula("[ARR_LOAD_SECTIONS]", "[ARR_LOAD_SECTIONS_END]");
  ilRC=getLoaFormula("[DEP_LOAD_SECTIONS]", "[DEP_LOAD_SECTIONS_END]");
  printFormulas();
  
  if ( ilRC==RC_FAIL )
  {
    dbg(TRACE,"InitPgm: getLoaFormula() failed! please correct configuration file(s)... waiting 30 sec ...");
    sleep(30);
    exit(2);      
  }

  if ( ilRC==RC_FAIL )
  {
    dbg(TRACE,"InitPgm: getLoaDepFormula() failed! please correct configuration file(s)... waiting 30 sec ...");
    sleep(30);
    exit(2);      
  }
    
  //Frank v1.08 20121210
  LoadFilesIntoMemory();
  
  dbg(DEBUG,"----------------");
  
  return(ilRC);

} /* end of initialize */

//Frank v1.08 20121210
static void LoadFilesIntoMemory()
{
    char *pclFunc = "LoadFilesIntoMemory";
    int ilRc=RC_SUCCESS;
    int ilCount = 0;
    FILE *fptr;
    char strLine[1025] = "\0";
    
    memset(pcgLoahdlConfigFile,0,sizeof(pcgLoahdlConfigFile));
    memset(pcgLoadConfigFile,0,sizeof(pcgLoadConfigFile));
    memset(strLine,0,sizeof(strLine));
    
    fptr=fopen(cgConfigFile, "r");
    dbg(DEBUG,"%s:cgConfigFile<%s>",pclFunc,cgConfigFile);
  if(fptr != NULL)
  {
        while (fgets(strLine, 1024, fptr))
    {
        if(strLine[0] == '#')
        {
            continue;
        }
      else
      {
          ilCount++;
          strncpy(pcgLoahdlConfigFile[ilCount],strLine,strlen(strLine)-1);
          //dbg(DEBUG,"<%s>pcgLoahdlConfigFile[%d]<%s>",pclFunc,ilCount,pcgLoahdlConfigFile[ilCount]);
      }
    }    
    fclose(fptr);
  }
  else
  {
      dbg(DEBUG, "%<s> Opening Default Config File[%s] error<%d><%s>",pclFunc,cgConfigFile,errno,strerror(errno));
    }
    
    fptr = NULL;
    ilCount = 0;
    memset(strLine,0,sizeof(strLine));
    fptr=fopen(cgLoadConfigFile, "r");
    dbg(DEBUG,"%s:cgLoadConfigFile<%s>",pclFunc,cgLoadConfigFile);
  if(fptr != NULL)
  {
        while (fgets(strLine, 1024, fptr))
    {
        if(strLine[0] == '#' || strlen(strLine)-1 == 0)
        {
            continue;
        }
      else
      {
          ilCount++;
          strncpy(pcgLoadConfigFile[ilCount],strLine,strlen(strLine)-1);
          //dbg(DEBUG,"<%s>pcgLoahdlConfigFile[%d]<%s>",pclFunc,ilCount,pcgLoadConfigFile[ilCount]);
      }
    }    
    fclose(fptr);
  }
  else
  {
      dbg(DEBUG, "%<s> Opening Default Config File[%s] error<%d><%s>",pclFunc,cgConfigFile,errno,strerror(errno));
    }
}

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int    ilRC = RC_SUCCESS;                /* Return code */
    
    return ilRC;
    
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate()
{ 
    dbg(TRACE,"Terminate: now leaving ...");
    
    exit(0);
    
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int ipSig)
{
    int    ilRC = RC_SUCCESS;            /* Return code */
    switch(ipSig)
    {
        case SIGTERM:
            dbg(TRACE,"HandleSignal: Received Signal <%d> (SIGTERM). Terminating now...",ipSig);
            break;
        case SIGALRM:
            dbg(TRACE,"HandleSignal: Received Signal<%d>(SIGALRM)",ipSig);
            return;
            break;
        default:
            dbg(TRACE,"HandleSignal: Received Signal<%d>",ipSig);
            return;
            break;
    } /* end of switch */
    exit(0);
    
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
    int    ilRC = RC_SUCCESS;
    return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    int    ilRC = RC_SUCCESS;
    
    switch(pipErr) {
    case    QUE_E_FUNC    :    /* Unknown function */
        dbg(TRACE,"HandleQueErr: <%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :    /* Malloc reports no memory */
        dbg(TRACE,"HandleQueErr: <%d> malloc failed",pipErr);
        break;
    case    QUE_E_SEND    :    /* Error using msgsnd */
        dbg(TRACE,"HandleQueErr: <%d> msgsnd failed",pipErr);
        break;
    case    QUE_E_GET    :    /* Error using msgrcv */
        dbg(TRACE,"HandleQueErr: <%d> msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"HandleQueErr: <%d> route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"HandleQueErr: <%d> route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX    :
        dbg(TRACE,"HandleQueErr: <%d> unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"HandleQueErr: <%d>  unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE    :
        dbg(TRACE,"HandleQueErr: <%d> queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"HandleQueErr: <%d> missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES    :
        dbg(TRACE,"HandleQueErr: <%d> queue does not exist",pipErr);
        break;
    case    QUE_E_RESP    :    /* No response on CREATE */
        dbg(TRACE,"HandleQueErr: <%d> no response on create",pipErr);
        break;
    case    QUE_E_FULL    :
        dbg(TRACE,"HandleQueErr: <%d> too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG    :    /* No message on queue */
        dbg(TRACE,"HandleQueErr: <%d> no messages on queue",pipErr);
        break;
    case    QUE_E_INVORG    :    /* Mod id by que call is 0 */
        dbg(TRACE,"HandleQueErr: <%d> invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :    /* Queues is not initialized*/
        dbg(TRACE,"HandleQueErr: <%d> queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"HandleQueErr: <%d> requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"HandleQueErr: <%d> receive buffer to small ",pipErr);
        break;
    default            :    /* Unknown queue error */
        dbg(TRACE,"HandleQueErr: <%d> unknown error",pipErr);
        break;
    } /* end switch */
         
    return;
} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
    int    ilRC = RC_SUCCESS;            /* Return code */
    int    ilBreakOut = FALSE;
    
    do
    {
        ilRC = que(QUE_GETBIGNW,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text;    
        if( ilRC == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRC != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRC);
            } /* fi */
        
            switch( prgEvent->command )
            {
            case    HSB_STANDBY    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_COMING_UP    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_ACTIVE    :
                ctrl_sta = prgEvent->command;
                ilBreakOut = TRUE;
                break;    
            case    HSB_ACT_TO_SBY    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate();
                break;    
    
            case    HSB_STANDALONE    :
                ctrl_sta = prgEvent->command;

                ilBreakOut = TRUE;
                break;    

            case    SHUTDOWN    :
                Terminate();
                break;
                        
            case    RESET        :
                ilRC = Reset();
                break;
                        
            case    EVENT_DATA    :
                dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
            default            :
                dbg(TRACE,"HandleQueues: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        }
        
    } while (ilBreakOut == FALSE);
    if(igInitOK == FALSE)
    {
            ilRC = InitPgm();
            if(ilRC != RC_SUCCESS)
            {
                dbg(TRACE,"HandleQueues: : init failed!");
            } /* end of if */
    }/* end of if */
} /* end of HandleQueues */
    

/******************************************************************************
 * HandleData
 ******************************************************************************
 */

static int HandleData()
{
    int ilRC = RC_SUCCESS; 
    int que_out;          /* Sender que id */
    int ilItemNo,jj;
    BC_HEAD *prlBchd = NULL;  /* Broadcast heade */
    CMDBLK  *prlCmdblk = NULL;    /* Command Block */
    char *pclSel = '\0';
    char *pclFld = '\0';
    char *pclData = '\0';
    char pclFields [4096], pclValues [4096];
    char *pclOldData = '\0';  
    char pclTable [32];
    char pclDssn [32], pclFlnu [32];
    char pclStart[16];
    char pclEnd[16];
  
    igQueOut = prgEvent->originator;   /* Queue-id des Absenders */
    prlBchd = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
    prlCmdblk = (CMDBLK  *) ((char *)prlBchd->data);
    
    pclSel = prlCmdblk->data;
    pclFld = pclSel + strlen(pclSel)+1;
    pclData = pclFld + strlen(pclFld)+1;
    strncpy (pclFields, pclFld, 4096);
    strncpy (pclValues, pclData, 4096);

    pclOldData= strtok(pclData,"\n");
    pclOldData= strtok(NULL,"\n");
    dbg(TRACE,"FROM <%d> TBL <%s>",igQueOut,prlCmdblk->obj_name);
    strcpy (pclTable,prlCmdblk->obj_name);
    dbg(TRACE,"Cmd <%s> Que (%d) WKS <%s> Usr <%s>",
        prlCmdblk->command, prgEvent->originator, prlBchd->recv_name, prlBchd->dest_name);
    dbg(TRACE,"Prio (%d) TWS <%s> TWE <%s>", prgItem->priority, prlCmdblk->tw_start, prlCmdblk->tw_end);
    strcpy (pcgAftTwStart,prlCmdblk->tw_start);
    strcpy (pcgAftTwEnd,prlCmdblk->tw_end);
    memset(pcgAftDestName, '\0', (sizeof(prlBchd->dest_name) + 1));
    strncpy (pcgAftDestName, prlBchd->dest_name, sizeof(prlBchd->dest_name));
    memset(pcgAftRecvName, '\0', (sizeof(prlBchd->recv_name) + 1));
    strncpy(pcgAftRecvName, prlBchd->recv_name, sizeof(prlBchd->recv_name));
    dbg(TRACE,"Sel <%s> ", pclSel);
    dbg(TRACE,"Fld <%s> ", pclFld);
    dbg(TRACE,"Dat <%s> ", pclData);
    if (pclOldData != '\0')
        dbg(TRACE,"Old <%s> ", pclOldData);

    if (prlBchd->rc == NETOUT_NO_ACK)
    {
        igGlNoAck = TRUE;
        dbg(TRACE,"No Answer Expected (NO_ACK is true)"); 
    }
    else
    {
        igGlNoAck = FALSE;
        dbg(TRACE,"Sender Expects Answer (NO_ACK is false)");
    }
 
    strcpy (pcgCedaCmd,prlCmdblk->command);
    
    if (!(strcmp (prlCmdblk->command, "LOA"))) 
    {
        ilItemNo = get_item_no (pclFld, "DSSN", 5) + 1;
        if (ilItemNo > 0)   /* get_item_no returns -1 if not found */
        {
            (void) get_real_item (pclDssn, pclData, ilItemNo);
        }
        else
        {
            dbg(TRACE,"========= No DSSN in LOA message ========");
            strcpy (pclDssn,""); 
        }

        ilItemNo = get_item_no (pclFld, "FLNU", 5) + 1;
        if (ilItemNo > 0)   /* get_item_no returns -1 if not found */
        {
           (void) get_real_item (pclFlnu, pclData, ilItemNo);
        }
        else
        { 
            dbg(TRACE,"========= No FLNU in LOA message ========");
            strcpy (pclFlnu,""); 
        }
        
        dbg(DEBUG,"pclDssn<%s>, pclFlnu<%s>",pclDssn, pclFlnu);
        HandleLOA_Cmd (pclDssn, pclFlnu);
    }
    else if ( !strcmp (prlCmdblk->command, "POP") ) 
    {
        dbg(TRACE, "HandleData:: POP");
        
        memset(pclStart, 0, sizeof(pclStart));
        memset(pclEnd, 0, sizeof(pclEnd));
        
        if (ilItemNo > 0)   
        {
            get_real_item (pclStart, pclData, 1);
            get_real_item (pclEnd, pclData, 2);
        }
        dbg(TRACE, "HandleData:: Start[%s], End[%s]", pclStart, pclEnd);   
        
        if ( 
              strcmp(pclStart, "") &&
              strcmp(pclEnd, "") 
           )
        {
            ilRC=processPop(pclStart, pclEnd);    
        }
        else
            dbg(TRACE, "HandleData:: Invalid POP parameters");        
    }
    
  if ((igGlNoAck == FALSE) && ((strcmp (prlCmdblk->command, "LOA")) == 0)) 
  {
    (void) tools_send_info_flag(igQueOut,
      0,        /* will be set to mod_id */
      "loahdl",        /* user name */
      "",        /* hostname by bchdl */
      "CEDA",        /* workstation name */
      "",        /* unused? */
      "",        /* unused? */
      pcgAftTwStart,pcgAftTwEnd,
      "LOA",
      "",        /* table name */
      "",        /* selection */
      "",        /* field list */
      "",        /* data list */
      NETOUT_NO_ACK);     /* should this be zero ? */
    dbg (TRACE,"Sent acknowledgement to <%d>", igQueOut); 
  }
  
  return ilRC;
    
} /* end of HandleData */

int getBestResultIndex()
{
    int ilRC=-1;
    int a=0;
    
    for (a=0; a<MAX_RESULT; a++) 
    {
        if ( strcmp(rgResult[a].VALU, "") )    
            break;
    }
    
    if ( a<MAX_RESULT )
        ilRC=a;
    else
        ilRC=0;
        
    return ilRC;    
}

void printResult()
{
    int a=0;
    
    dbg(DEBUG, "printResult:: Start");
    
    for (a=0; a<MAX_RESULT; a++)
    {
        dbg(DEBUG, "printResult:: Found[%d], DSSN[%s], VALU[%s], iValue[%d]", 
            rgResult[a].iFound, rgResult[a].DSSN, rgResult[a].VALU, rgResult[a].iValue);
    }
    
    dbg (DEBUG, "printResult:: End");
    return;
}

int addResult(RESULT *rpResult1, RESULT *rpResult2, char *pcpOp)
{
    int ilRC=RC_SUCCESS;
    int a=0;
    int b=0;
    int ilIndex=0;
    int ilFound=0;
    
    
    dbg(DEBUG, "addResult:: Start");
    dbg(DEBUG, "addResult:: OP[%s]", pcpOp);
                
    
    for (ilIndex=0; ilIndex<MAX_RESULT; ilIndex++)
    {
        if ( !strcmp(rpResult1[ilIndex].DSSN, "") )
            break;    
    }
    
    dbg(DEBUG, "addResult:: ilIndex[%d]", ilIndex);
        

    for (a=0; a<MAX_RESULT; a++)
    {
        dbg(DEBUG, "addResult:: [%d]DSSN for Result2[%s]",     
            a, rpResult2[a].DSSN);
            
        if ( !strcmp(rpResult2[a].DSSN, "") )
            break;
        
        
        ilFound=0;
        for (b=0; b<MAX_RESULT; b++)
        {
            dbg(DEBUG, "addResult:: [%d] DSSN for Result1 [%s]", 
                b, rpResult1[b].DSSN);
            
            if ( !strcmp(rpResult1[b].DSSN, "") )
                break;
                            
            if ( !strcmp(rpResult2[a].DSSN, rpResult1[b].DSSN) )
            {
                ilFound=1;
                if ( strcmp(rpResult2[a].VALU, "") )
                {
                    strcpy(rpResult1[b].VALU, rpResult2[a].VALU);
                            
                    switch (pcpOp[0])
                    {
                        case '+':
                            rpResult1[b].iValue+=rpResult2[a].iValue;
                            break;
                        case '-':
                            rpResult1[b].iValue-=rpResult2[a].iValue;
                            break;
                        case '*':
                            rpResult1[b].iValue*=rpResult2[a].iValue;
                            break;
                        case '/':
                            rpResult1[b].iValue/=rpResult2[a].iValue;
                            break;    
                        default:
                            rpResult1[b].iValue+=rpResult2[a].iValue;     
                    }             
                }
                break;
            }                 
        }
        
        if ( !ilFound )
        {
            if ( ilIndex<MAX_RESULT )
            {
                rpResult1[ilIndex]=rpResult2[a];    
                if ( pcpOp[0]=='-' )
                {
                    rpResult1[ilIndex].iValue*=-1;
                }
                ilIndex++;  
            }
            else
            {
                dbg(TRACE, "addResult::  ERROR!!! PLS INCREASE MAX_RESULT");
                ilRC=RC_FAIL;    
            }    
        }
    }   

   
    dbg(DEBUG, "addResult:: End Result[%d]", ilRC);
    return ilRC;    
}

int addResultValue(RESULT *rpResult, char *pcpOp, char *pcpDSSN, char *pcpValu, int ipFound)
{
    int ilRC=RC_SUCCESS;
    int ilFound=0;
    int a=0;
    
    dbg(DEBUG, "addResultValue:: Start");
    
    dbg(DEBUG, "addResultValue:: pcpOp[%s], pcpDSSN[%s], pcpValu[%s], ipFound[%d]",
        pcpOp, pcpDSSN, pcpValu, ipFound);
    
    
    if ( !strcmp(pcpDSSN, "") )
    {
        dbg(DEBUG, "addResultValue:: Add to all");
        for (a=0; a<MAX_RESULT; a++)
        {
            switch (pcpOp[0])
            {
                case '+':
                    rpResult[a].iValue+=atoi(pcpValu);
                    break;
                case '-':
                    rpResult[a].iValue-=atoi(pcpValu);
                    break;
                case '*':
                    rpResult[a].iValue*=atoi(pcpValu);
                    break;
                case '/':
                    rpResult[a].iValue/=atoi(pcpValu);
                    break;    
                default:
                    rpResult[a].iValue+=atoi(pcpValu);               
            }             
        }                
    }
    else
    {
        for (a=0; a<MAX_RESULT; a++)
        {
            if ( !strcmp(rpResult[a].DSSN, pcpDSSN) )
            {
                ilFound=1;
                break;    
            }
            else if ( !strcmp(rpResult[a].DSSN, "") )    
                break;
        }
    
        dbg(DEBUG, "addResultValue:: ilFound[%d]", ilFound);
        if ( ilFound )
        {
            rpResult[a].iFound|=ipFound;
        
            if ( 
                   strcmp(pcpValu, "") &&
                   strcmp(pcpValu, " ") 
               ) 
                strcpy(rpResult[a].VALU, pcpValu);
            
            switch (pcpOp[0])
            {
                case '+':
                    rpResult[a].iValue+=atoi(pcpValu);
                    break;
                case '-':
                    rpResult[a].iValue-=atoi(pcpValu);
                    break;
                case '*':
                    rpResult[a].iValue*=atoi(pcpValu);
                    break;
                case '/':
                    rpResult[a].iValue/=atoi(pcpValu);
                    break;    
                default:
                    rpResult[a].iValue+=atoi(pcpValu);               
            }      
        }
        else
        {
            if ( a<MAX_RESULT )
            {
                rpResult[a].iFound=ipFound;
                strcpy(rpResult[a].DSSN, pcpDSSN);    
            
                if ( 
                     strcmp(pcpValu, "")&&
                     strcmp(pcpValu, " ") 
                   )             
                    strcpy(rpResult[a].VALU, pcpValu);
                
                rpResult[a].iValue=atoi(pcpValu);    
            }
            else
                dbg(TRACE, "addResultValue::  ERROR!!! CONTACT UFIS TO INCREASE BUFFER");    
        }
        dbg(TRACE, "addResultValue:: Running total[%d]",rpResult[a].iValue);
    }
    
        
    dbg(DEBUG, "addResultValue:: End[%d]", ilRC);
    return ilRC;    
}

int processPop(char *pcpStart, char *pcpEnd)
{
    int ilRC=RC_SUCCESS;
    int ilTmp;
    char pclCond[1024];
    char pclSql[1024];
    char pclData[2048];
    char pclUrno[32];
    char pclRkey[32];
    char pclAdid[8];
    short slCursor = 0;
    char pclStart[16];
    char pclEnd[16];
  
    dbg(TRACE, "processPop:: Start");
    
    if ( strlen(pcpStart)==8 )
        sprintf(pclStart, "%s000000", pcpStart);
    else
        strcpy(pclStart, pcpStart);

    if ( strlen(pcpEnd)==8 )
        sprintf(pclEnd, "%s235959", pcpEnd);
    else
        strcpy(pclEnd, pcpEnd);
        
    sprintf(pclSql, "SELECT URNO,RKEY,ADID FROM AFTTAB WHERE (STOD BETWEEN '%s' and '%s') OR (STOA BETWEEN '%s' AND '%s') ",
            pclStart, pclEnd, pclStart, pclEnd);
    
    dbg(TRACE, "processPop:: executing sql[%s]", pclSql);

    slCursor = 0;
          
    ilTmp=sql_if(START, &slCursor, pclSql, pclData);
    while ( ilTmp==DB_SUCCESS )
    {
        BuildItemBuffer(pclData, "URNO,RKEY,ADID", 3, ",");
        get_real_item(pclUrno,pclData,1); 
        get_real_item(pclRkey,pclData,2); 
        get_real_item(pclAdid,pclData,3); 
        
        // please remove
        //strcpy(pclUrno, "23106250");
        //strcpy(pclRkey, "23108946");
        dbg(TRACE, "processPop:: Processing Urno[%s], Rkey[%s]", pclUrno, pclRkey);
 
        sprintf (pclCond, "WHERE FLNU='%s' AND DSSN!='%s' ",pclUrno, cgFormulaDSSN);
        LoadGridData(pcgLoaTabGrid, pcgLoaTabName, pclCond);

        if ( strcmp(pclUrno,pclRkey) )
        {
            sprintf (pclCond,"WHERE FLNU='%s' AND DSSN!='%s' ",pclRkey, cgFormulaDSSN);
            LoadGridData(pcgLoaTabArrivalGrid, pcgLoaTabName, pclCond);
        }
         
        ilRC=computeFormulas(pclUrno, pclRkey, pclAdid);
        dbg(TRACE, "ProcessPop:: computeFormulas Result[%d]", ilRC);
        //break;
        ilTmp=sql_if(NEXT, &slCursor, pclSql, pclData);
    }
    close_my_cursor(&slCursor);
        
    
    if ( ilTmp==ORA_NOT_FOUND )
        ilRC=RC_SUCCESS;
   
              
    dbg(TRACE, "processPop:: End Result[%d]", ilRC);
    return ilRC;    
}

/************************************************************************
 *      
 *  HandleLOA_Cmd 
 *
 *  
 ************************************************************************
 */

void HandleLOA_Cmd (char *pcpDssn, char *pcpFlnu) 
{ 
    char pclFunc [] = "HandleLOA_Cmd:";
    char clNow [32] = "\0";
    int jj,kk,ilRC;
    char pclTmpBuf [64], pclAdid [32], pclFlno [32], pclSchedule [32], pclRkey[32];
  
    //Frank UFIS-2229
    int ilNumProcesses = 0;
    char pclProcessesConfig[1024] = "\0";
    char pclDSSNConfig[1024] = "\0"; 
    int ilCount = 0;
    char pclProcess[64] = "\0";
    int ilProcessId = 0;
  
    //Frank UFIS-2229
    char pcl1stPart[1024] = "\0";
    char pcl2ndPart[1024] = "\0";
    char *pcl1stSeparator = NULL;
    char *pcl2ndSeparator = NULL;
    char pcl1stProcess[1024] = "\0";
    char pcl1stDSSN[1024] = "\0";
    char pcl2ndProcess[1024] = "\0";
    char pcl2ndDSSN[1024] = "\0";
    
    int ilRc = RC_SUCCESS;
 
    /* (void) TimeToStr (clNow, time(NULL), UTC_TIME); */
    //dbg(DEBUG,"<%s>pcpFlnu<%s>pcpFlnu[0]<%c>",pclFunc,pcpFlnu,pcpFlnu[0]);
    
    if ((pcpDssn [0] == '\0') || (pcpFlnu [0] == '\0'))
    {
        dbg (TRACE, "%s DSSN and/or FLNU empty - doing nothing", pclFunc);
        return;
    }

    if (GetFlightDetails (pcpFlnu, pclAdid, pclRkey, pclFlno, pclSchedule) != RC_SUCCESS)
    {
        dbg(TRACE,"%s Flight not found urno <%s> or SQL error",pclFunc,pcpFlnu);
        return;
    }
    dbg(TRACE,"%s Flight found <%s><%s><%s> rkey<%s>",pclFunc,pclFlno,pclAdid,pclSchedule, pclRkey);

    memset (pcgAftFld,'\0',sizeof(pcgAftFld));
    memset (pcgAftSel,'\0',sizeof(pcgAftSel));
    memset (pcgAftVal,'\0',sizeof(pcgAftVal));

    ScanConfig (pclAdid,pcpDssn,pcpFlnu, pclRkey); 

    sprintf (pcgAftSel,"WHERE URNO = '%s'",pcpFlnu);

    if (strlen(pcgAftVal) > 0)
    {
       ilRC = SendCedaEvent(igModIdFlight,0,pcgAftDestName,pcgAftRecvName,
                            pcgAftTwStart,pcgAftTwEnd,pcgAftCmd,
                            pcgAftTable,pcgAftSel,pcgAftFld,pcgAftVal,
                            "",4,NETOUT_NO_ACK);

       dbg(DEBUG,"%s Selection <%s>",pclFunc,pcgAftSel);
       dbg(DEBUG,"%s FieldList <%s>",pclFunc,pcgAftFld);
       dbg(DEBUG,"%s ValueList <%s>",pclFunc,pcgAftVal);
       dbg(DEBUG,"%s Sent <%s>. Returned <%d>",pclFunc,pcgAftCmd,ilRC); 
    }
    else
    {
        dbg(TRACE,"%s Nothing to send to FLIGHT",pclFunc); 
    }
  
    //Frank UFIS-2229
    //SeparateINFORM_PROCESS(pcgInformProcess,pclProcessesConfig,pclDSSNConfig);
    SeparateString(pcgInformProcess,pcl1stPart,pcl2ndPart);
    TrimSpace(pcl1stPart);
    TrimSpace(pcl2ndPart);
  
    if(strlen(pcl1stPart) != 0)
    {
        //dbg(TRACE,"1st<%s>\n",pcl1stPart);
        pcl1stSeparator = strstr(pcl1stPart,"|");
        strncpy(pcl1stProcess,pcl1stPart,(int)(pcl1stSeparator-pcl1stPart));
        dbg(TRACE,"%s pcl1stProcess<%s>",pclFunc,pcl1stProcess);
        strcpy(pcl1stDSSN,pcl1stPart + (int)(pcl1stSeparator-pcl1stPart)+1);
        //dbg(TRACE,"#1<%s>\n",pcl1stDSSN);
        
        if(strlen(pcl1stProcess) == 0 || strlen(pcl1stDSSN) == 0)
        {
            dbg(TRACE,"%s At least one of pcl1stProcess<%s> or pcl1stDSSN<%s> is null",pclFunc,pcl1stProcess,pcl1stDSSN);
        }
        else
        {
            if(strncmp(pcl1stDSSN,"*",1) == 0 || strstr(pcl1stDSSN, pcpDssn) != 0)
            {
                ilNumProcesses = get_no_of_items(pcl1stProcess);
                for(ilCount = 1; ilCount <= ilNumProcesses; ilCount++ )
                {
                    memset(pclProcess,0,sizeof(pclProcess));
                    get_real_item (pclProcess, pcl1stProcess, ilCount);
                
                    dbg(TRACE,"%s pclProcess<%s>",pclFunc,pclProcess);
                        
                    if ( !strcmp(pclProcess, "dqahdl") )
                    {    
                        //Frank v1.07
                        InitRecordDescriptor(&rgRecDesc,1024,0,FALSE);
                        ilRc = computeFormulas(pcpFlnu, pclRkey, pclAdid);
                    }   
                    
                    if(ilRc==DB_SUCCESS)
                    {
                        ilProcessId = tool_get_q_id (pclProcess);
                        if ((ilProcessId == RC_FAIL) || (ilProcessId == RC_NOT_FOUND))
                        {
                            dbg(TRACE,"%s: ====== ERROR ====== %s Mod Id not found",pclFunc,pclProcess);
                        }
                        else
                        {
                            dbg(TRACE,"%s %s Mod Id <%d>",pclFunc, pclProcess, ilProcessId);
                    
                            if(ilProcessId!=0)
                            {
                                ilRC = SendCedaEvent(ilProcessId,0,pcgAftDestName,pcgAftRecvName,
                                                     pcgAftTwStart,pcgAftTwEnd,"LOA",
                                                     pcgAftTable,pcgAftSel,"FLNU",pcpFlnu,
                                                     "",4,NETOUT_NO_ACK);
                            
                                /*LOA, field:FLNU,data:AFTTAB.URNO*/
                            }
                            else
                            {
                                dbg(TRACE,"%s ilProcessId<%d>==0",pclFunc,ilProcessId);
                            }
                        }
                    }//for loop ends
                }
            }
            else
            {
                dbg(TRACE,"%s pcl1stDSSN<%s> is not * and does not contain pcpDssn<%s>",pclFunc,pcl1stDSSN,pcpDssn);
            }
        }
    }    
    
    if(strlen(pcl2ndPart) != 0)
    {
        //dbg(TRACE,"2nd<%s>\n",pcl2ndPart);
        pcl2ndSeparator = strstr(pcl2ndPart,"|");
        strncpy(pcl2ndProcess,pcl2ndPart,(int)(pcl2ndSeparator-pcl2ndPart));
        dbg(TRACE,"%s pcl2ndProcess<%s>",pclFunc,pcl2ndProcess);
        strcpy(pcl2ndDSSN,pcl2ndPart + (int)(pcl2ndSeparator-pcl2ndPart)+1);
        //dbg(TRACE,"#2<%s>\n",pcl2ndDSSN);
        
        if( strlen(pcl2ndProcess) == 0 || strlen(pcl2ndDSSN) == 0 )
        {
            dbg(TRACE,"%s At least one of pcl2ndProcess<%s> or pcl2ndDSSN<%s> is null",pclFunc,pcl2ndProcess,pcl2ndDSSN);
        }
        else
        {
            if( strncmp(pcl2ndDSSN,"*",1) == 0 || strstr(pcl2ndDSSN,pcpDssn) != 0 )
            {
                ilNumProcesses = get_no_of_items(pcl2ndProcess);
                for(ilCount = 1; ilCount <= ilNumProcesses; ilCount++ )
                {
                    memset(pclProcess,0,sizeof(pclProcess));
                    get_real_item (pclProcess, pcl2ndProcess, ilCount);
                
                    dbg(TRACE,"%s pclProcess<%s>",pclFunc,pclProcess);
                    ilProcessId = tool_get_q_id (pclProcess);
                
                    if ((ilProcessId == RC_FAIL) || (ilProcessId == RC_NOT_FOUND))
                    {
                        dbg(TRACE,"%s: ====== ERROR ====== %s Mod Id not found",pclFunc,pclProcess);
                    }
                    else
                    {
                        dbg(TRACE,"%s %s Mod Id <%d>",pclFunc, pclProcess, ilProcessId);
                    
                        if(ilProcessId!=0)
                        {
                            ilRC = SendCedaEvent(ilProcessId,0,pcgAftDestName,pcgAftRecvName,
                                                 pcgAftTwStart,pcgAftTwEnd,"LOA",pcgAftTable,pcgAftSel,
                                                 pcgAftFld,pcgAftVal,"",4,NETOUT_NO_ACK);
                        }
                        else
                        {
                            dbg(TRACE,"%s ilProcessId<%d>==0",pclFunc,ilProcessId);
                        }
                    }
                }//for loop ends
            }
            else
            {
                dbg(TRACE,"%s pcl2ndDSSN<%s> is not * and does not contain pcpDssn<%s>",pclFunc,pcl2ndDSSN,pcpDssn);
            }
        }
    }

     return;
}

//Frank UFIS-2229
/*
void SeparateINFORM_PROCESS(char *pcpInformProcess, char *pcpProcessesConfig, char *pcpDSSNConfig)
{
    char *pclFunc = "SeparateINFORM_PROCESS";
    char *pclCommaPosition = NULL;
    
    memset(pcpProcessesConfig,0,sizeof(pcpProcessesConfig));
  memset(pcpDSSNConfig,0,sizeof(pcpDSSNConfig));
    
    if( strlen(pcpInformProcess) == 0 || strcmp(pcpInformProcess,"NONE") == 0)
    {
        dbg(TRACE,"%s pcpInformProcess<%s> is invalid -> return",pclFunc, pcpInformProcess);
        return;
    }
    else
    {
        //Frank UFIS-2229
    [MAIN]
    INFORM_PROCESS = dqahdl,DSSN|*
    IMFORM_PROCESS = [dqahdl|*],[xxx,yyy|LDM,PTM];
    [MAIN_END]
    1>Separate INFORM_PROCESS to two parts
    //
        pclCommaPosition = strstr (pcpInformProcess,",");
      
    dbg(TRACE,"*pclCommaPosition<%c>",*pclCommaPosition);
    //if (*pclCommaPosition == '\0')
    if(strlen(pclCommaPosition) == 0)
      {
        dbg(TRACE,"%s pcpInformProcess<%s> must contain two parts separated by ,",pclFunc, pcpInformProcess);
    }
    else
    {
    dbg(TRACE,"pcpInformProcess<%s>pclCommaPosition-pcpInformProcess<%d>",pcpInformProcess,(int)(pclCommaPosition-pcpInformProcess));
        strncpy( pcpProcessesConfig, pcpInformProcess, (int)(pclCommaPosition-pcpInformProcess) );
    
        strcpy( pcpDSSNConfig, pcpInformProcess + (int)(pclCommaPosition-pcpInformProcess) + strlen("DSSN|") +1 );
        
        dbg(TRACE,"pcpProcessesConfig<%s>,pcpDSSNConfig<%s>", pcpProcessesConfig, pcpDSSNConfig);
    }
    }
}
*/

int SeparateString(char *pclConfigString,char *pcp1stPart, char *pcp2ndPart)
{
    char *pclFunc = "SeparateString";
    char *pcl1stLeftSquareBacketPosition = NULL;
    char *pcl1stRightSquareBacketPosition = NULL;
    char *pcl2ndLeftSquareBacketPosition = NULL;
    char *pcl2ndRightSquareBacketPosition = NULL;

    char *pcl1stCommaPosition = NULL;

    char pcl1stPart[1024] = "\0";
    char pcl2ndPart[1024] = "\0";    
    
    if( strlen(pclConfigString) == 0 || strcmp(pclConfigString,"NONE") == 0)
      {    
          dbg(TRACE,"%s pclConfigString<%s> is invalid -> return",pclFunc, pclConfigString);
        return;
   } 
    
    pcl1stCommaPosition = strstr(pclConfigString,",");
    
    if( pcl1stCommaPosition != NULL)// && strstr(pclConfigString,"*") != 0 )
    {
        //1st '['    
        pcl1stLeftSquareBacketPosition = strstr(pclConfigString,"[");
        //dbg(TRACE,"*pcl1stLeftSquareBacketPosition<%c>\n",*pcl1stLeftSquareBacketPosition);
        
        //1st ']'
        pcl1stRightSquareBacketPosition = strstr(pclConfigString,"]");
        //dbg(TRACE,"*pcl1stRightSquareBacketPosition<%c>\n",*pcl1stRightSquareBacketPosition);
        
        //if 1st ']' is at the front of 1st '['
        if( (int)(pcl1stRightSquareBacketPosition - pcl1stLeftSquareBacketPosition) < 0)
        {
            dbg(TRACE,"The 1st ']' is at the front of 1st '[' -> return\n");
            return;
        }
                
        memset(pcl1stPart,0,sizeof(pcl1stPart));
        strncpy(pcl1stPart, pclConfigString+1,(int)(pcl1stRightSquareBacketPosition-pcl1stLeftSquareBacketPosition)-1);
        //dbg(TRACE,"pcl1stPart<%s>\n",pcl1stPart);
        
        //2nd '['
        pcl2ndLeftSquareBacketPosition = strstr(pclConfigString,"[");
    //dbg(TRACE,"*pcl2ndLeftSquareBacketPosition<%c>\n",*pcl2ndLeftSquareBacketPosition);
        //2nd ']'
        pcl2ndRightSquareBacketPosition = strstr(pclConfigString,"]");
                //dbg(TRACE,"*pcl2ndRightSquareBacketPosition<%c>\n",*pcl2ndRightSquareBacketPosition);
        
    //2nd ']' is at the front of 2nd '['
    if( (int)(pcl2ndRightSquareBacketPosition - pcl2ndLeftSquareBacketPosition) < 0)
    {   
            dbg(TRACE,"The 2nd ']' is at the front of 2nd '[' -> return\n");
            return;
    } 
        
        //pclConfig = "[dqahdl|*],[xxx,yyy|LDM,PTM]";
        //pcl2ndPart<xxx,yyy|L>
        memset(pcl2ndPart,0,sizeof(pcl2ndPart));
    strcpy(pcl2ndPart, pclConfigString+(int)(pcl1stCommaPosition-pclConfigString)+1);
                
        *(pcl2ndPart + strlen(pcl2ndPart) - 1) = '\0';
        //dbg(TRACE,"pcl2ndPart<%s>\n",pcl2ndPart);
    
        //dqahdl|*
        strcpy(pcp1stPart,pcl1stPart);
        //xxx,yyy|LDM,PTM
        strcpy(pcp2ndPart,pcl2ndPart);
    }
    else
    {
        pcl1stLeftSquareBacketPosition = strstr(pclConfigString,"[");
        //dbg(TRACE,"*pcl1stLeftSquareBacketPosition<%c>\n",*pcl1stLeftSquareBacketPosition);
        pcl1stRightSquareBacketPosition = strstr(pclConfigString,"]");
        //dbg(TRACE,"*pcl1stRightSquareBacketPosition<%c>\n",*pcl1stRightSquareBacketPosition);
        
        //1st ']' is at the front of 1st '['
                if( (int)(pcl1stRightSquareBacketPosition - pcl1stLeftSquareBacketPosition) < 0)
            {
                        dbg(TRACE,"The 1st ']' is at the front of 1st '[' -> return\n");
                        return;
                }
        
        memset(pcl1stPart,0,sizeof(pcl1stPart));
                strncpy(pcl1stPart, pclConfigString+1,(int)(pcl1stRightSquareBacketPosition-pcl1stLeftSquareBacketPosition)-1);
                //dbg(TRACE,"pcl1stPart<%s>\n",pcl1stPart);
        strcpy(pcp1stPart,pcl1stPart);
    }
    return 0;
}


/********************************************************************************/

/******************************************************************************
 * Handy 
 ******************************************************************************
 */

static int TimeToStr(char *pcpTime,time_t lpTime, int ipFlag)
{
    struct tm *_tm;
    char   _tmpc[6];

    if (ipFlag == UTC_TIME)
      _tm = (struct tm *)gmtime(&lpTime);
    else if (ipFlag == LOCAL_TIME)
      _tm = (struct tm *)localtime(&lpTime);
    else
    {
      dbg(TRACE,"TimeToStr: ERROR: called with unknown flag - defaulting to UTC");
      _tm = (struct tm *)gmtime(&lpTime);
    }
    
    /*      strftime(pcpTime,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",_tm); */

    sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
            _tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
            _tm->tm_min,_tm->tm_sec);

    return RC_SUCCESS;

}     /* end of TimeToStr */

/******************************************************************************
 *
 *  LoadAftConfig - read and parse the configuration into the global 
 *    configuration structure. The structure is proposed so as to reduce
 *    repeated parsing code and re-reading the config file at each CEDA
 *    message.
 ******************************************************************************
 */

static void LoadAftConfig (void)
{

  char pclFunc[] = "LoadAftConfig: ";
  
  int ilRC = RC_SUCCESS;

  char pclSection [64], pclTemp [ENTRY_VALUE_SIZE], pclTemp2 [ENTRY_VALUE_SIZE];
  char pclTemp3[ENTRY_VALUE_SIZE],pclArgRaw [ENTRY_VALUE_SIZE];
  char pclAftArrPrio [ENTRY_VALUE_SIZE],pclAftDepPrio [ENTRY_VALUE_SIZE];
  char pclFormula [ENTRY_VALUE_SIZE];
  char pclArgName [ENTRY_VALUE_SIZE],pclDssnList [ENTRY_VALUE_SIZE];
  char pclArgName2 [ENTRY_VALUE_SIZE];
  char pclLoaWhereClause [ENTRY_VALUE_SIZE];
  int  jj,kk,mm,nn,ilSection,ilFrmlIdx,ilVarIdx,ilNoPrio,ilNoArgs;
  int  repeated,ilNumSections;
  char pclType [32],pclStyp [32],pclSstp [32],pclSsst [32], pclApc3[32]; 

  ilNumSections = get_no_of_items(pcgAftSections);
  for(jj = 1; jj <= ilNumSections; jj++ )
  {
    ilSection = jj - 1;
    (void) get_real_item (pclSection, pcgAftSections, jj); 
    strcpy (sgConfig [ilSection].name,pclSection);

    ReadConfigEntry(pclSection,"AFT_FIELDS", pclTemp2, "");
    strcpy (sgConfig [ilSection].aft_fields, pclTemp2);
    
    ReadConfigEntry(pclSection,"AFT_ARR_PRIO", pclAftArrPrio, "");
    strcpy (sgConfig [ilSection].aft_arr_prio, pclAftArrPrio);

    ReadConfigEntry(pclSection,"AFT_DEP_PRIO", pclAftDepPrio, "");
    strcpy (sgConfig [ilSection].aft_dep_prio, pclAftDepPrio);

    ilFrmlIdx = 0;
    ilVarIdx = 0;

    /*
      Extract the formula names, then get each formula definition. Convert the
      spaces in the formula to commas for get_real_item later on.
    */
    ilNoPrio = get_no_of_items(pclAftArrPrio);
 
    for (kk = 1; kk <=  ilNoPrio; kk++)
    {
      (void) get_real_item (pclFormula, pclAftArrPrio, kk); 
      ReadConfigEntry(pclSection,pclFormula,pclTemp2, "");
      ReplaceChar(pclTemp2,' ',',');
      strcpy (sgConfig [ilSection].formula [ilFrmlIdx].name, pclFormula);
      /* v.1.01 - note that "defn" retains the '!' flags. */
      strcpy (sgConfig [ilSection].formula [ilFrmlIdx].defn, pclTemp2);
      ilFrmlIdx++; 

      /* 
        Get each argument from the formula, then lookup the arg's definition, 
        and parse it into a dssn list and loatab where clause.
      */
      ilNoArgs = get_no_of_items (pclTemp2);
      for (mm = 1; mm <=  ilNoArgs; mm++)     
      {
        (void) get_real_item (pclArgName, pclTemp2, mm); 

        /* v.1.01 - check for '!' flag which means skippable argument */
        if ((strlen (pclArgName) > 0) && (pclArgName[0] == '!'))
        {
          /*
             Remove the '!' from the argument name so that it can be found
             further down from the current section. E.g. if formula arg was
             !VAL_BOOK, then the definition "VAL_BOOK =" will not be found
             unless the '!' is removed here.
          */
          strcpy (pclArgName2,pclArgName);
          strcpy (pclArgName,&(pclArgName2[1]));
        } 

        if ((strcmp (pclArgName,"+") == 0) || (strcmp (pclArgName,"-") == 0))
          continue;

        /*
          If arg already parsed, skip it. 
        */
        for (nn = 0,repeated=0; nn < N_PARAMETERS; nn++)
        {
          if ((sgConfig [ilSection].variable [nn].name [0]) == '\0')
            break;   /* reached end of already assigned variables */

          if (strcmp(sgConfig [ilSection].variable [nn].name,pclArgName) == 0)
          {
            dbg(DEBUG,"%s Argument <%s> repeated, skipped",pclFunc,pclTemp3);
            repeated = 1;
            break;
          } 
        }
        if (repeated == 1)
          continue; 

        ReadConfigEntry(pclSection,pclArgName,pclArgRaw,"");
        if (strlen (pclArgRaw) == 0) /* in case missed out in config */
          continue;
        ExtractLoaWhereClauseValues (pclArgRaw, pclDssnList, pclType,
          pclStyp, pclSstp, pclSsst, pclApc3);
        strcpy (sgConfig [ilSection].variable [ilVarIdx].name, pclArgName);
        strcpy (sgConfig [ilSection].variable [ilVarIdx].dssn, pclDssnList);
        strcpy (sgConfig [ilSection].variable [ilVarIdx].type, pclType);
        strcpy (sgConfig [ilSection].variable [ilVarIdx].styp, pclStyp);
        strcpy (sgConfig [ilSection].variable [ilVarIdx].sstp, pclSstp);
        strcpy (sgConfig [ilSection].variable [ilVarIdx].ssst, pclSsst);
        strcpy (sgConfig [ilSection].variable [ilVarIdx].apc3, pclApc3);
        ilVarIdx++; 
      }
    }

    /*
       Next repeat for the departure formula. Skip repeated formula names.
    */

    ilNoPrio = get_no_of_items(pclAftDepPrio);
    for (kk = 1; kk <=  ilNoPrio; kk++)
    {
      (void) get_real_item (pclFormula, pclAftDepPrio, kk); 

      /*
        If formula was found in AFT_ARR_PRIO, skip it. Note that formula
        start only at entry index 3
      */
      for (nn = 0,repeated=0; nn < N_FORMULAE; nn++)
      {
        if ((sgConfig [ilSection].formula [nn].name [0]) == '\0')
            break;   /* reached end of already assigned formula */

        if (strcmp(sgConfig [ilSection].formula [nn].name,pclFormula) == 0)
        {
          dbg(DEBUG,"%s Formula <%s> repeated, skipped",pclFunc,pclFormula);
          repeated = 1;
          break;
        } 
      }
      if (repeated == 1)
        continue; 


      ReadConfigEntry(pclSection,pclFormula,pclTemp2, "");
      ReplaceChar(pclTemp2,' ',',');
      strcpy (sgConfig [ilSection].formula [ilFrmlIdx].name, pclFormula);
      strcpy (sgConfig [ilSection].formula [ilFrmlIdx].defn, pclTemp2);
      ilFrmlIdx++; 

      /* 
        Get each argument from the formula, then lookup the arg's definition, 
        and parse it into a dssn list and loatab where clause.
      */
      ilNoArgs = get_no_of_items (pclTemp2);
      for (mm = 1; mm <=  ilNoArgs; mm++)     
      {
        (void) get_real_item (pclArgName, pclTemp2, mm); 
        if ((strcmp (pclArgName,"+") == 0) || (strcmp (pclArgName,"-") == 0))
          continue;

        /*
          If arg already parsed, skip it. 
        */
        for (nn = 0,repeated=0; nn < N_PARAMETERS; nn++)
        {
          if ((sgConfig [ilSection].variable [nn].name [0]) == '\0')
            break;   /* reached end of already assigned variables */

          if (strcmp(sgConfig [ilSection].variable [nn].name,pclArgName) == 0)
          {
            dbg(DEBUG,"%s Argument <%s> repeated, skipped",pclFunc,pclArgName);
            repeated = 1;
            break;
          } 
        }
        if (repeated == 1)
          continue; 

        ReadConfigEntry(pclSection,pclArgName,pclArgRaw,"");
        if (strlen (pclArgRaw) == 0) /* in case missed out in config */
          continue;
        /*ExtractLoaWhereClauseValues (pclArgRaw, pclDssnList, pclLoaWhereClause); */

        ExtractLoaWhereClauseValues (pclArgRaw, pclDssnList, pclType,
          pclStyp, pclSstp, pclSsst, pclApc3);

        strcpy (sgConfig [ilSection].variable [ilVarIdx].name, pclArgName); 
        strcpy (sgConfig [ilSection].variable [ilVarIdx].dssn, pclDssnList);
        strcpy (sgConfig [ilSection].variable [ilVarIdx].type, pclType);
        strcpy (sgConfig [ilSection].variable [ilVarIdx].styp, pclStyp);
        strcpy (sgConfig [ilSection].variable [ilVarIdx].sstp, pclSstp);
        strcpy (sgConfig [ilSection].variable [ilVarIdx].ssst, pclSsst);
        strcpy (sgConfig [ilSection].variable [ilVarIdx].apc3, pclApc3);
        ilVarIdx++; 

      }
    }
  } 
}

/******************************************************************************
 *
 *  ReplaceChar - lifted from fdihxx.src
 *
 ******************************************************************************
 */

static void ReplaceChar(char *pcpBuffer, char clOrgChar, char clDestChar)
{
   int ilI;

   if (strlen(pcpBuffer) > 0)
   {
      for (ilI = 0; ilI < strlen(pcpBuffer); ilI++)
      {
         if (pcpBuffer[ilI] == clOrgChar)
         {
            pcpBuffer[ilI] = clDestChar;
         }
      }
   }
}

/******************************************************************************
 *
 *  ExtractLoaWhereClauseValues - given a formula value argument raw definition,
 *    extract the DSSN list and convert the LOA fields into a where clause.
 *
 *    pcpArgDefn sample is [DSSN|,AIM,LDM,] [PAX,,,B]
 *     or
 *    pcpArgDefn sample is [DSSN|,AIM,LDM,] [PAX,#,#,B]
 *
 ******************************************************************************
 */

static void ExtractLoaWhereClauseValues (char *pcpArgDefn,
              char *pcpDssnList, 
              char *pcpType, char *pcpStyp, char *pcpSstp, char *pcpSsst,char *pcpApc3) 
{
  char    pclFunc[] = "ExtractLoaWhereClauseValues: ";
  char    pclTemp [ENTRY_VALUE_SIZE], *pclPtr1, *pclPtr2, pclTemp2 [ENTRY_VALUE_SIZE];
  int    jj;
  char  pclType [32],pclStyp [32], pclSstp [32], pclSsst [32], pclApc3[32];

  pclPtr1 = strstr (pcpArgDefn,"[DSSN|");
  if (pclPtr1 == '\0')
    return;
 
  while ((*pclPtr1) != '|')
    pclPtr1++;

  pclPtr1++;

  jj = 0;
  while ((*pclPtr1) != ']')
  {
    pcpDssnList [jj] = (*pclPtr1);
    pclPtr1++; jj++;
  }
  pcpDssnList [jj] = '\0';

  pclPtr1--;
  while ((*pclPtr1) != '[')
    pclPtr1++;

  pclPtr1++;

  pclPtr2 = pclPtr1;
  while ((*pclPtr2) != ']')
    pclPtr2++;

  memset (pclTemp2,'\0',sizeof(pclTemp2));
  strncpy (pclTemp2,pclPtr1,(pclPtr2-pclPtr1));

  (void) get_real_item (pclType, pclTemp2, 1);
  (void) get_real_item (pclStyp, pclTemp2, 2); 
  if ((strlen (pclStyp) == 0) || (pclStyp[0] == '#'))
    strcpy (pclStyp, " ");

  (void) get_real_item (pclSstp, pclTemp2, 3);
  if ((strlen (pclSstp) == 0) || (pclSstp[0] == '#'))
    strcpy (pclSstp, " ");

  (void) get_real_item (pclSsst, pclTemp2, 4);
  if ((strlen (pclSsst) == 0) || (pclSsst[0] == '#'))
    strcpy (pclSsst, " ");
   
  (void) get_real_item (pclApc3, pclTemp2, 5);
  if ((strlen (pclApc3) == 0) || (pclApc3[0] == '#'))
    strcpy (pclApc3, " ");
  else if ( pclApc3[0]=='-' )
    strcpy(pclApc3, cgHopo);
 
  /* sprintf (pcpWhereClause," TYPE='%s' AND STYP='%s' AND SSTP='%s' AND SSST='%s' ",
    pclType,pclStyp,pclSstp,pclSsst); */
  strcpy (pcpType,pclType);
  strcpy (pcpStyp,pclStyp);
  strcpy (pcpSstp,pclSstp);
  strcpy (pcpSsst,pclSsst);
  strcpy (pcpApc3,pclApc3);
  
  return;

}

/******************************************************************************
 *
 *      PrintLoaConfig
 *
 ******************************************************************************
 */

static void PrintLoaConfig (void)
{
  int    nn, ilSectionIdx;
  
  for (ilSectionIdx = 0; ilSectionIdx < N_SECTIONS; ilSectionIdx++)
  {
    if (sgConfig [ilSectionIdx].name [0] == '\0')
      break; /* reached end of sections in configuration */

    dbg(TRACE,"===============[%s]===========================================",
      sgConfig [ilSectionIdx].name);
    dbg(TRACE,"AFT_FIELDS <%s>",sgConfig [ilSectionIdx].aft_fields);
    dbg(TRACE,"AFT_ARR_PRIO <%s>",sgConfig [ilSectionIdx].aft_arr_prio);
    dbg(TRACE,"AFT_DEP_PRIO <%s>",sgConfig [ilSectionIdx].aft_dep_prio);

    dbg(TRACE,"------------ Formulae ------------------");

    for (nn = 0; nn < N_FORMULAE; nn++)
    {
      if (sgConfig [ilSectionIdx].formula [nn].name [0] == '\0')
        break;
      dbg(TRACE,"<%s> = <%s>",
        sgConfig [ilSectionIdx].formula [nn].name,
        sgConfig [ilSectionIdx].formula [nn].defn);
    }

    dbg(TRACE,"------------ Variables ------------------");

    for (nn = 0; nn < N_PARAMETERS; nn++)
    {
      if (sgConfig [ilSectionIdx].variable [nn].name [0] == '\0')
        break;
      dbg(TRACE,"<%s> : DSSN <%s> TYPE <%s> STYP <%s> SSTP <%s> SSST <%s>",
        sgConfig [ilSectionIdx].variable [nn].name,
        sgConfig [ilSectionIdx].variable [nn].dssn,
        sgConfig [ilSectionIdx].variable [nn].type,
        sgConfig [ilSectionIdx].variable [nn].styp,
        sgConfig [ilSectionIdx].variable [nn].sstp,
        sgConfig [ilSectionIdx].variable [nn].ssst);
    }

  dbg(TRACE,"===============[%s_END]===========================================",
    sgConfig [ilSectionIdx].name);
  }
  return;
}

/******************************************************************************
 *
 *      DoSelect - G.P. handy, I hope..
 *
 ******************************************************************************
 */

static int DoSelect (char *pcpFieldList, char *pcpWhereClause, char *pcpTable,
    char *pcpDataBuf)
{
  char pclFunc[] = "DoSelect: ";
  int ilRC = RC_SUCCESS;
  short slFkt = 0;
  short slCursor = 0;
  char clSqlBuf [2048] = "\0";
  int ilNoOfRec = 0, ilNoOfItems;
  int ilGetRc = DB_SUCCESS;

  sprintf (clSqlBuf,"SELECT %s FROM %s WHERE %s",
    pcpFieldList,pcpTable,pcpWhereClause);
  dbg(TRACE,"%s <%s>",pclFunc,clSqlBuf);

  ilNoOfRec = 0;
  slFkt = START;
  slCursor = 0;
  ilGetRc = sql_if (slFkt, &slCursor, clSqlBuf, pcpDataBuf);
  if (ilGetRc == RC_SUCCESS)
  {
    ilNoOfItems = get_no_of_items (pcpFieldList);
    BuildItemBuffer (pcpDataBuf, pcpFieldList, ilNoOfItems, ","); 
  }
  else if (ilGetRc == SQL_NOTFOUND)
  {
    dbg(TRACE,"%s SQL_IF not found <%d>",pclFunc,ilGetRc);
  }
  else
  {
    dbg(TRACE,"%s SQL_IF error <%d>",pclFunc,ilGetRc);
  }
    
    //Frank v1.08
  commit_work();
  close_my_cursor (&slCursor);
  return ilGetRc; 
}

/******************************************************************************
 *
 *  GetFlightDetails 
 *
 ******************************************************************************
 */

static int GetFlightDetails (char *pcpUrno, char *pcpAdid, char *pcpRkey, char *pcpFlno,
              char *pcpSchedule)
{
  char    pclFieldList [1024];
  char  pclWhereClause [1024];
  char  pclDataBuf [1024],pclStoa [64],pclStod [64];
  int    ilRcDb;
  
  strcpy (pclFieldList,"ADID,RKEY,FLNO,STOA,STOD"); 
  sprintf(pclWhereClause,"URNO = '%s'",pcpUrno);
  ilRcDb = DoSelect (pclFieldList, pclWhereClause, "AFTTAB", pclDataBuf);
  if (ilRcDb == RC_SUCCESS)
  {
    (void) get_real_item (pcpAdid, pclDataBuf, 1);
    (void) get_real_item (pcpRkey, pclDataBuf, 2);
    (void) get_real_item (pcpFlno, pclDataBuf, 3);
    (void) get_real_item (pclStoa, pclDataBuf, 4);
    (void) get_real_item (pclStod, pclDataBuf, 5);
    if (strcmp (pcpAdid,"A") == 0)
      strcpy (pcpSchedule,pclStoa);
    else if (strcmp (pcpAdid,"D") == 0)
      strcpy (pcpSchedule,pclStod);
    else
      strcpy (pcpSchedule,"");
  }
  return ilRcDb;
}

int getGridIndex(char strGrid[], char *strflnu, char *strType, char *strStyp, char *strSstp, char *strSsst, char *strApc3, char *strDssn)
{
    int iRet=-1;
    int a=0;
    int iSize=CT_GetLineCount(strGrid);
    char pclLoaType [32], pclLoaStyp [32], pclLoaSstp [32];
    char pclLoaSsst [32], pclLoaValu [32], pclLoaDssn [32];
    char pclLoaApc3 [32], pclLoaUrno [32], pclLoaFlnu [32];
        
    for (a=0; a<iSize; a++)
    {
        CT_GetMultiValues(strGrid, a, "URNO", pclLoaUrno);
        CT_GetMultiValues(strGrid, a, "FLNU", pclLoaFlnu);
        CT_GetMultiValues(strGrid, a, "DSSN", pclLoaDssn);
        CT_GetMultiValues(strGrid, a, "TYPE", pclLoaType);
        CT_GetMultiValues(strGrid, a, "STYP", pclLoaStyp);
        CT_GetMultiValues(strGrid, a, "SSTP", pclLoaSstp);
        CT_GetMultiValues(strGrid, a, "SSST", pclLoaSsst);
        CT_GetMultiValues(strGrid, a, "APC3", pclLoaApc3);
        CT_GetMultiValues(strGrid, a, "VALU", pclLoaValu);   
        /* 
        dbg(DEBUG,"getGridIndex:: LOATAB [%d/%d] urno[%s] flnu[%s] dssn[%s] type[%s] styp[%s] sstp[%s] ssst[%s] apc3[%s] valu[%s]",
            a+1, iSize, pclLoaUrno, pclLoaFlnu, pclLoaDssn,pclLoaType,pclLoaStyp,pclLoaSstp,pclLoaSsst,pclLoaApc3,pclLoaValu);  
            */        
        if ( 
               !strcmp(pclLoaDssn, strDssn) &&
               !strcmp(pclLoaType, strType) &&
               !strcmp(pclLoaStyp, strStyp) &&
               !strcmp(pclLoaSstp, strSstp) &&
               !strcmp(pclLoaSsst, strSsst) &&
               !strcmp(pclLoaApc3, strApc3) 
           )
        {
             iRet=a;
             break;
        }
          
    }
    dbg(DEBUG,"getGridIndex:: Result[%d]", iRet);
    return iRet;    
}

int computeFormulas(char *strFlnu, char *strRkey, char *pcpAdid)
{
    int iRet=RC_SUCCESS;
    int iTmp;
    int iSize, iSizeArrival;
    int a=0, b=0;
    char pclLoaType [32], pclLoaStyp [32], pclLoaSstp [32];
    char pclLoaSsst [32], pclLoaValu [32], pclLoaDssn [32];
    char pclLoaApc3 [32], pclLoaUrno [32], pclLoaFlnu [32];
    char strSql[1025];
    char strTmp[1025];
    char strErr[513];
    char *strPtr, *strPtr2;
    
    //Frank v1.07
    char *pclFunc = "computeFormulas";
    char pclCurrentTime[32] = "\0";
    char buffer[32] = "\0";
    char pclValueList[8192] = "\0";
    char pclFieldList[2048] = "\0";
    int iUrno=NewUrnos("LOATAB",iFormula);
    int ilIndex=0;
    int iValueFlag=0;
    int iValue;
    int iFormulaTmp;
    LOALINEAR *sFormulaTmp;

    
    dbg(TRACE, "computeFormulas:: Start");
    dbg(TRACE, "computeFormulas:: FLNU[%s], RKEY[%s], ADID[%s]",  strFlnu, strRkey, pcpAdid);
    
    if(iUrno <=0 )
    {
        dbg(TRACE,"computeFormulas:: >iUrno<%d> is invalid, return RC_FAIL", iUrno);
        return RC_FAIL;
    }
    else
    {
        dbg(TRACE,"computeFormulas:: >iUrno<%d> is valid", iUrno);
    }
   

    
    sprintf(strSql, "DELETE FROM loatab WHERE flnu='%s' AND DSSN='%s' ", strFlnu, cgFormulaDSSN);
    iTmp=executeSql(strSql,strErr);  
     
    iSize = CT_GetLineCount(pcgLoaTabGrid);
    /*
    dbg(DEBUG, "computeFormulas:: LOATAB records for [%s]", strFlnu);
    
    dbg(DEBUG, "Time X0<%d>",time(0));
    for (a=0;a<iSize;a++)
    {
        CT_GetMultiValues(pcgLoaTabGrid, a, "URNO", pclLoaUrno);
        CT_GetMultiValues(pcgLoaTabGrid, a, "FLNU", pclLoaFlnu);
        CT_GetMultiValues(pcgLoaTabGrid, a, "DSSN", pclLoaDssn);
        CT_GetMultiValues(pcgLoaTabGrid, a, "TYPE", pclLoaType);
        CT_GetMultiValues(pcgLoaTabGrid, a, "STYP", pclLoaStyp);
        CT_GetMultiValues(pcgLoaTabGrid, a, "SSTP", pclLoaSstp);
        CT_GetMultiValues(pcgLoaTabGrid, a, "SSST", pclLoaSsst);
        CT_GetMultiValues(pcgLoaTabGrid, a, "APC3", pclLoaApc3);
        CT_GetMultiValues(pcgLoaTabGrid, a, "VALU", pclLoaValu);
        dbg(DEBUG,"computeFormulas:: LOATAB [%d/%d] urno[%s] flnu[%s] dssn[%s] type[%s] styp[%s] sstp[%s] ssst[%s] apc3[%s] valu[%s]",
            a+1, iSize, pclLoaUrno, pclLoaFlnu, pclLoaDssn,pclLoaType,pclLoaStyp,pclLoaSstp,pclLoaSsst,pclLoaApc3,pclLoaValu);
    }
        dbg(DEBUG, "Time X1<%d>",time(0));
        
    iSizeArrival = CT_GetLineCount(pcgLoaTabArrivalGrid);
     
    dbg(DEBUG, "computeFormulas:: LOATAB Arrival records for [%s]", strRkey);
    
    dbg(DEBUG, "Time Y0<%d>",time(0));
    for (a=0;a<iSizeArrival;a++)
    {
        CT_GetMultiValues(pcgLoaTabArrivalGrid, a, "URNO", pclLoaUrno);
        CT_GetMultiValues(pcgLoaTabArrivalGrid, a, "FLNU", pclLoaFlnu);
        CT_GetMultiValues(pcgLoaTabArrivalGrid, a, "DSSN", pclLoaDssn);
        CT_GetMultiValues(pcgLoaTabArrivalGrid, a, "TYPE", pclLoaType);
        CT_GetMultiValues(pcgLoaTabArrivalGrid, a, "STYP", pclLoaStyp);
        CT_GetMultiValues(pcgLoaTabArrivalGrid, a, "SSTP", pclLoaSstp);
        CT_GetMultiValues(pcgLoaTabArrivalGrid, a, "SSST", pclLoaSsst);
        CT_GetMultiValues(pcgLoaTabArrivalGrid, a, "APC3", pclLoaApc3);
        CT_GetMultiValues(pcgLoaTabArrivalGrid, a, "VALU", pclLoaValu);
        dbg(DEBUG,"computeFormulas:: LOATAB [%d/%d] urno[%s] flnu[%s] dssn[%s] type[%s] styp[%s] sstp[%s] ssst[%s] apc3[%s] valu[%s]",
            a+1, iSizeArrival, pclLoaUrno, pclLoaFlnu, pclLoaDssn,pclLoaType,pclLoaStyp,pclLoaSstp,pclLoaSsst,pclLoaApc3,pclLoaValu);
    }
    dbg(DEBUG, "Time Y1<%d>",time(0));
    */
    
    if ( iTmp==RC_FAIL )
    {
       dbg(DEBUG,"computeFormulas::ERROR!!! DB failed [%s][%s]", strSql, strErr);    
       iRet=RC_FAIL;
    }
    else
    {
        memset(pcgLoaBuf, 0, sizeof(pcgLoaBuf));
        
        if ( strstr(pcgArrAdid, pcpAdid) )
        {
        	dbg(DEBUG, "computeFormulas:: Getting ARRIVAL Formulas");
            iFormulaTmp=iFormula;
            sFormulaTmp=sFormula;	
        }
        else
        {
        	dbg(DEBUG, "computeFormulas:: Getting DEPARTURE Formulas");
            iFormulaTmp=iFormulaDep;
            sFormulaTmp=sFormulaDep;	        	
        }

        for (a=0; a<iFormulaTmp; a++)
        {
            memset(pclCurrentTime,0,sizeof(pclCurrentTime));
            memset(pclValueList,0,sizeof(pclValueList));
            GetServerTimeStamp("UTC",1,0,pclCurrentTime);
                
            dbg(DEBUG, "computeFormulas:: [%d/%d] Evaluating [%s]", a+1, iFormulaTmp, sFormulaTmp[a].strString);   
            if ( iSize==0 )
            {        
                memset(pclValueList,0,sizeof(pclValueList));
                
                sprintf(pclValueList,"%d,%s,%s,%s,%s,%s,%s,%s,%s,%s",
                        iUrno++, strFlnu, sFormulaTmp[a].type, sFormulaTmp[a].styp, sFormulaTmp[a].sstp,
                        sFormulaTmp[a].ssst, sFormulaTmp[a].apc3, sFormulaTmp[a].dssn, " ", pclCurrentTime);
            }
            else
            {    
            	igComplex=0;
                iValue=0;
                iValueFlag=0;                
                memset(sFormulaTmp[a].valu, 0, sizeof(sFormulaTmp[a].valu));

                if( 
                       !strcmp(sFormulaTmp[a].name, "LOAD_PAX_ACTUAL_TRANSFER_FIRST_DEP") ||
                       !strcmp(sFormulaTmp[a].name, "LOAD_PAX_ACTUAL_TRANSFER_BUSINESS_DEP") ||
                       !strcmp(sFormulaTmp[a].name, "LOAD_PAX_ACTUAL_TRANSFER_ECONOMY_DEP") ||
                       !strcmp(sFormulaTmp[a].name, "LOAD_PAX_ACTUAL_TRANSFER_ECONOMY2_DEP") ||
                       !strcmp(sFormulaTmp[a].name, "LOAD_PAX_ACTUAL_TRANSFER_INFANT_DEP") 
                  )
                {                    
                    strcpy(strTmp, sFormulaTmp[a].strFormula);
                    strPtr=strtok_r(strTmp, ",", &strPtr2);
                    iTmp=0;
                    while (strPtr)
                    {
                        if ( !iTmp )
                            strcpy(strSql, strPtr);
                        else
                            sprintf(strSql, "%s,%s", strSql, strPtr);
                            
                        strPtr=strtok_r(NULL, ",", &strPtr2);    
                        if ( ++iTmp==3 )
                            break;
                    }
                    
                    if ( !strcmp(sFormulaTmp[a].name, "LOAD_PAX_ACTUAL_TRANSFER_FIRST_DEP") )
                    {
                        strcpy(strSql, "LOAD_IN_TRNSFR_PIN_DEP_FIRST,-,LOAD_BUS_TRNSFR_DEP_FIRST");
                        if ( 
                              getGridIndex(pcgLoaTabGrid, strFlnu, "PAX", "F", "T", " ", " ", "USR")==-1 && 
                              getGridIndex(pcgLoaTabGrid, strFlnu, "PAX", "F", "T", " ", " ", "PTM")==-1
                           )
                            evaluateFormulas(rgResult, strSql, &iValueFlag, &iValue);  
                        else
                            evaluateFormulas(rgResult, sFormulaTmp[a].strFormula, &iValueFlag, &iValue); 
                    }
                    else if ( !strcmp(sFormulaTmp[a].name, "LOAD_PAX_ACTUAL_TRANSFER_BUSINESS_DEP") )
                    {
                        strcpy(strSql, "LOAD_IN_TRNSFR_PIN_DEP_BUSINESS,-,LOAD_BUS_TRNSFR_DEP_BUSINESS");
                        if ( 
                              getGridIndex(pcgLoaTabGrid, strFlnu, "PAX", "B", "T", " ", " ", "USR")==-1 && 
                              getGridIndex(pcgLoaTabGrid, strFlnu, "PAX", "B", "T", " ", " ", "PTM")==-1
                           )
                            evaluateFormulas(rgResult, strSql, &iValueFlag, &iValue);  
                        else
                            evaluateFormulas(rgResult, sFormulaTmp[a].strFormula, &iValueFlag, &iValue); 
                    }
                    else if ( !strcmp(sFormulaTmp[a].name, "LOAD_PAX_ACTUAL_TRANSFER_ECONOMY_DEP") )
                    {
                        strcpy(strSql, "LOAD_IN_TRNSFR_PIN_DEP_ECONOMY,-,LOAD_BUS_TRNSFR_DEP_ECONOMY");
                        if ( 
                              getGridIndex(pcgLoaTabGrid, strFlnu, "PAX", "E", "T", " ", " ", "USR")==-1 && 
                              getGridIndex(pcgLoaTabGrid, strFlnu, "PAX", "E", "T", " ", " ", "PTM")==-1
                           )
                            evaluateFormulas(rgResult, strSql, &iValueFlag, &iValue);  
                        else
                            evaluateFormulas(rgResult, sFormulaTmp[a].strFormula, &iValueFlag, &iValue); 
                    }
                    else if ( !strcmp(sFormulaTmp[a].name, "LOAD_PAX_ACTUAL_TRANSFER_ECONOMY2_DEP") )
                    {
                         strcpy(strSql, "LOAD_IN_TRNSFR_PIN_DEP_ECONOMY2,-,LOAD_BUS_TRNSFR_DEP_ECONOMY2");
                        if ( 
                              getGridIndex(pcgLoaTabGrid, strFlnu, "PAX", "U", "T", " ", " ", "USR")==-1 && 
                              getGridIndex(pcgLoaTabGrid, strFlnu, "PAX", "U", "T", " ", " ", "PTM")==-1
                           )
                            evaluateFormulas(rgResult, strSql, &iValueFlag, &iValue);  
                        else
                            evaluateFormulas(rgResult, sFormulaTmp[a].strFormula, &iValueFlag, &iValue);                            
                    }         
                    else if ( !strcmp(sFormulaTmp[a].name, "LOAD_PAX_ACTUAL_TRANSFER_INFANT_DEP") )
                    {
                        strcpy(strSql, "LOAD_IN_TRNSFR_PIN_DEP_INFANT,-,LOAD_BUS_TRNSFR_DEP_INFANT");
                        if ( 
                              getGridIndex(pcgLoaTabGrid, strFlnu, "PAX", " ", "T", "I", " ", "USR")==-1 && 
                              getGridIndex(pcgLoaTabGrid, strFlnu, "PAX", " ", "T", "I", " ", "PTM")==-1
                           )
                            evaluateFormulas(rgResult, strSql, &iValueFlag, &iValue);  
                        else
                            evaluateFormulas(rgResult, sFormulaTmp[a].strFormula, &iValueFlag, &iValue); 
                    }           
                }
                else
                    evaluateFormulas(rgResult, sFormulaTmp[a].strFormula, &iValueFlag, &iValue);      
                              
                printResult();    
                
                dbg(DEBUG, "computeFormulas:: igComplex[%d], iValueFlag[%d], iValue[%d] ",
                    igComplex, iValueFlag, iValue);
                if ( igComplex )
                {
                    if ( iValueFlag)
                    {
                        sprintf(pclValueList,"%d,%s,%s,%s,%s,%s,%s,%s,%d,%s",
                               iUrno++, strFlnu, sFormulaTmp[a].type, sFormulaTmp[a].styp, sFormulaTmp[a].sstp, sFormulaTmp[a].ssst, sFormulaTmp[a].apc3,
                               sFormulaTmp[a].dssn, iValue, pclCurrentTime);                    	
                    }
                    else
                    {
                        sprintf(pclValueList,"%d,%s,%s,%s,%s,%s,%s,%s,%s,%s",
                                iUrno++, strFlnu, sFormulaTmp[a].type, sFormulaTmp[a].styp, sFormulaTmp[a].sstp, sFormulaTmp[a].ssst, sFormulaTmp[a].apc3,
                                sFormulaTmp[a].dssn," ",pclCurrentTime);                    	
                    }	
                }
                else
                {                   
                    ilIndex=getBestResultIndex()  ;
                    dbg(DEBUG, "computeFormulas:: Best: Index[%d], iFound[%d], DSSN[%s], VALU[%s], iValue[%d]",
                        ilIndex, rgResult[ilIndex].iFound, rgResult[ilIndex].DSSN, rgResult[ilIndex].VALU, rgResult[ilIndex].iValue);              
                
                    if ( !strcmp(rgResult[ilIndex].VALU, "") )
                    {
                        sprintf(pclValueList,"%d,%s,%s,%s,%s,%s,%s,%s,%s,%s",
                                iUrno++, strFlnu, sFormulaTmp[a].type, sFormulaTmp[a].styp, sFormulaTmp[a].sstp, sFormulaTmp[a].ssst, sFormulaTmp[a].apc3,
                                sFormulaTmp[a].dssn," ",pclCurrentTime);
                    }                
                    else
                    {
                        sprintf(pclValueList,"%d,%s,%s,%s,%s,%s,%s,%s,%d,%s",
                               iUrno++, strFlnu, sFormulaTmp[a].type, sFormulaTmp[a].styp, sFormulaTmp[a].sstp, sFormulaTmp[a].ssst, sFormulaTmp[a].apc3,
                               sFormulaTmp[a].dssn, rgResult[ilIndex].iValue, pclCurrentTime);
                    }
                }
            }
            
            dbg(TRACE, "computeFormulas:: pclValueList[%s]", pclValueList);
            //Frank v1.07
            //fill array-------------------------------------------------
            dbg(TRACE,"computeFormulas:: Count NO.<%d>", a);
            FillArray(pclValueList);
            //fill array-------------------------------------------------
        }    
        

        if (strlen(pcgLoaBuf) > 0)
        {
            strcpy(pclFieldList,"URNO,FLNU,TYPE,STYP,SSTP,SSST,APC3,DSSN,VALU,TIME");
            strcpy(pclValueList,":VURNO,:VFLNU,:VTYPE,:VSTYP,:VSSTP,:VSSST,:VAPC3,:VDSSN,:VVALU,:VTIME");
            
            //insertLoaTab(strLoa, iFormula);
            dbg(DEBUG,"computeFormulas:: Time Before SqlArrayInsert<%d>", time(0));
            iRet = SqlArrayInsert(pclFieldList,pclValueList,pcgLoaBuf);
            dbg(DEBUG,"computeFormulas:: Time After SqlArrayInsert<%d>", time(0));
            
            if(iRet == DB_SUCCESS)
            {
                dbg(TRACE,"computeFormulas:: SqlArrayInsert succeeds");
            }
            else
            {
                dbg(TRACE,"computeFormulas:: SqlArrayInsert fails");
            }
        }
    }
    
    dbg(DEBUG, "computeFormulas:: End FLNU[%s], Result[%d]", strFlnu, iRet);
    return iRet;    
}

/******************************************************************************
 *
 *  ScanConfig 
 *
 *  v.1.01 - handle '!' flag to skip formula members whose values could not
 *  be evaluated or found.
 *
 ******************************************************************************
 */

static int  ScanConfig (char *pcpAdid, char *pcpDssn, char *pcpFlnu, char *pcpRkey)
{
  char pclFunc [] = "ScanConfig: ";
  char pclLoaCond [128];
  int  llCurLine,llMaxLine;
  char pclLoaType [32], pclLoaStyp [32], pclLoaSstp [32];
  char pclLoaSsst [32], pclLoaValu [32], pclLoaDssn [32];
  int  ilNumSections, ilSection, ilFormula, ilEntry, ilPrio; 
  int  nn,jj,kk,iFlagSumOk,iFlagStopFormulaScan,ilVarRes; 
  int  ilFormulaMembers,fm, ilRC = RC_SUCCESS;
  char pclCurPrio [ENTRY_VALUE_SIZE],pclFormula [ENTRY_NAME_SIZE];
  char pclFormulaMember [64],pclTemp [AFT_MSG_SIZE];
  char pclFormulaMember2[64];
  int  ilSum, ilCurVal;
  char pclLastOp [32];

  /*
    Retrieve all LOATAB rows for the FLNO in one select (select from loatab)
  */
  sprintf (pclLoaCond,"WHERE FLNU='%s' AND DSSN!='%s' ",pcpFlnu, cgFormulaDSSN);
  LoadGridData(pcgLoaTabGrid, pcgLoaTabName, pclLoaCond);
  
  if ( strcmp(pcpFlnu,pcpRkey) )
  {
      sprintf (pclLoaCond,"WHERE FLNU='%s' AND DSSN!='%s' ",pcpRkey, cgFormulaDSSN);
      LoadGridData(pcgLoaTabArrivalGrid, pcgLoaTabName, pclLoaCond);
  }
  
  llMaxLine = CT_GetLineCount(pcgLoaTabGrid) - 1;

  dbg(DEBUG,"DSSN\t|TYPE\t|STYP\t|SSTP\t|SSST\t|VALU\t|"); 
  for (llCurLine=0;llCurLine<=llMaxLine;llCurLine++)
  {
    CT_GetMultiValues(pcgLoaTabGrid, llCurLine, "DSSN", pclLoaDssn);
    CT_GetMultiValues(pcgLoaTabGrid, llCurLine, "TYPE", pclLoaType);
    CT_GetMultiValues(pcgLoaTabGrid, llCurLine, "STYP", pclLoaStyp);
    CT_GetMultiValues(pcgLoaTabGrid, llCurLine, "SSTP", pclLoaSstp);
    CT_GetMultiValues(pcgLoaTabGrid, llCurLine, "SSST", pclLoaSsst);
    /* no tab for VALU so can see if it is a space */
    CT_GetMultiValues(pcgLoaTabGrid, llCurLine, "VALU", pclLoaValu);
    dbg(DEBUG,"%s\t|%s\t|%s\t|%s\t|%s\t|%s|",
      pclLoaDssn,pclLoaType,pclLoaStyp,pclLoaSstp,pclLoaSsst,pclLoaValu);
  }

  /* Loop through all sections from config file */ 
  for (ilSection = 0; ilSection < N_SECTIONS; ilSection++)
  {
    if (sgConfig [ilSection].name [0] == '\0')
      break; /* reached end of sections in configuration */

    dbg(TRACE,"%s ========= Working on section [%s]=================",
      pclFunc,sgConfig [ilSection].name);

    strcpy(pclCurPrio,"");
    if (pcpAdid[0] == 'A')
    {
      strcpy (pclCurPrio,sgConfig [ilSection].aft_arr_prio);
    }
    else if (pcpAdid[0] == 'D')
    {
      strcpy (pclCurPrio,sgConfig [ilSection].aft_dep_prio);
    }

    ilPrio = get_no_of_items(pclCurPrio);
    iFlagStopFormulaScan = 0; /* set when a formula is completely successful */
    for (kk = 1; kk <=  ilPrio; kk++)
    {
      if (iFlagStopFormulaScan == 1)
      {
        dbg(TRACE,"%s Stop scanning formulae for <%s>",pclFunc,sgConfig [ilSection].name);
        break;
      }
      iFlagSumOk = 0;
      (void) get_real_item (pclFormula, pclCurPrio, kk); 
      dbg(TRACE,"%s ---------- Working on Formula <%s> ----------",pclFunc,pclFormula);
      
      for (ilFormula = 0; ilFormula < N_FORMULAE,iFlagStopFormulaScan == 0; ilFormula++)
      {
        ilSum = 0;
        if (sgConfig [ilSection].formula [ilFormula].name [0] == '\0')
          break; /* no more formula defined in this section */
        if (strcmp ((sgConfig [ilSection].formula [ilFormula].name),pclFormula) != 0)
          continue;

        /* Loop through all constituents of the formula line */
        ilFormulaMembers = get_no_of_items(sgConfig [ilSection].formula [ilFormula].defn);
        memset (pclLastOp, '\0', sizeof (pclLastOp));
        for (fm = 1; fm <=  ilFormulaMembers; fm++)
        {
          (void) get_real_item (pclFormulaMember,
            sgConfig [ilSection].formula [ilFormula].defn,fm); 

          if ((pclFormulaMember [0] != '+') && (pclFormulaMember [0] != '-'))
          {
            dbg(TRACE,"%s Working on variable <%s>",pclFunc,pclFormulaMember);

            /* v.1.01 */
            strcpy (pclFormulaMember2,""); /* doubles up as a flag so must reset */
            if ((strlen(pclFormulaMember) > 0) && (pclFormulaMember[0] == '!'))
            {
              dbg(TRACE,"%s <%s> can be ignored if not found",pclFunc,pclFormulaMember);
              strcpy(pclFormulaMember2,pclFormulaMember);
              strcpy(pclFormulaMember,&(pclFormulaMember2[1]));
            }
            ilVarRes = GetVariableValue (ilSection, pclFormulaMember, pcpDssn, &ilCurVal);

            /*
               Ugly hack - quick fix is to change ilVarRes and ilCurVal and leave
               rest of loop intact.
            */

            if ((strlen (pclFormulaMember2) > 0) &&
                (ilVarRes == RC_FAIL) &&
                (pclFormulaMember2[0] == '!'))
            {
              ilVarRes = RC_SUCCESS;
              ilCurVal = 0;
              dbg(TRACE,"%s Ignore <%s> by setting it to zero",pclFunc,pclFormulaMember);
            }

            if (ilVarRes == RC_FAIL)
            {
              dbg(TRACE,"%s No value for <%s> - abandon formula",
                pclFunc,pclFormulaMember);
              break;
            } 
            else
            { 
              if (fm == ilFormulaMembers)
              {
                /*
                  We have now successfully found all members of the formula
                */
                iFlagStopFormulaScan = 1;

              }
              if (pclLastOp [0] != '\0')
              {
                if (pclLastOp [0] == '+')
                  ilSum += ilCurVal;

                if (pclLastOp [0] == '-')
                  ilSum -= ilCurVal; 

                dbg(DEBUG,"%s Operator <%c> on <%d>, new Sum = <%d>",
                  pclFunc,pclLastOp [0],ilCurVal,ilSum);
              }
              else
              {
                /* Must be the first variable in formula member list */
                ilSum = ilCurVal; 
                dbg(DEBUG,"%s Assign first variable <%d> to Sum",pclFunc,ilCurVal);
              }
            }
          }
          else
          { 
            pclLastOp [0] = pclFormulaMember [0];
            pclLastOp [1] = '\0';
            dbg(TRACE,"%s Found operator <%s>",pclFunc,pclLastOp);
          }
        }
      }
      dbg(TRACE,"%s ----- End work for Formula <%s> -------",pclFunc,pclFormula);
    }

    dbg(TRACE,"%s =======Section End [%s_END]================",
        pclFunc,sgConfig [ilSection].name);

    if (iFlagStopFormulaScan == 1)
    {
      if (strlen (pcgAftFld) == 0)
        strcpy (pcgAftFld,sgConfig [ilSection].aft_fields);
      else
      {
        strcat (pcgAftFld,",");
        strcat (pcgAftFld,sgConfig [ilSection].aft_fields);
      }

      if (strlen (pcgAftVal) == 0)
        sprintf (pcgAftVal,"%d",ilSum); 
      else
      {
        sprintf (pclTemp,",%d",ilSum);
        strcat (pcgAftVal,pclTemp); 
      }
    }
  }
  return ilRC;
}

/******************************************************************************
 *
 *  InitConfigStructure
 *
 ******************************************************************************
 */
static int  InitConfigStructure (void)
{
  int ilRC = RC_SUCCESS,jj,kk;

  /* Init the global configuration structure */
  /* Not the most elegant, but short of time. C++ map class preferred */

  dbg(TRACE,"InitConfigStructure starting");
  for (jj = 0; jj < N_SECTIONS; jj++)
  {
    memset (sgConfig [jj].name,'\0', ENTRY_NAME_SIZE);
    memset (sgConfig [jj].aft_fields,'\0', ENTRY_VALUE_SIZE);   
    memset (sgConfig [jj].aft_arr_prio,'\0', ENTRY_VALUE_SIZE);   
    memset (sgConfig [jj].aft_dep_prio,'\0', ENTRY_VALUE_SIZE);    
 
    for (kk = 0; kk < N_FORMULAE; kk++)   
    {
      memset (sgConfig [jj].formula[kk].name,'\0', ENTRY_NAME_SIZE);    
      memset (sgConfig [jj].formula[kk].defn,'\0', ENTRY_VALUE_SIZE);
    }

    for (kk = 0; kk < N_PARAMETERS; kk++)   
    {
      memset (sgConfig [jj].variable[kk].name,'\0', ENTRY_NAME_SIZE);    
      memset (sgConfig [jj].variable[kk].dssn,'\0', ENTRY_NAME_SIZE);    
      /*memset (sgConfig [jj].variable[kk].where,'\0', ENTRY_VALUE_SIZE); */
      memset (sgConfig [jj].variable[kk].type,'\0', 32);
      memset (sgConfig [jj].variable[kk].styp,'\0', 32);
      memset (sgConfig [jj].variable[kk].sstp,'\0', 32); 
      memset (sgConfig [jj].variable[kk].ssst,'\0', 32); 
    }
  }
  dbg(TRACE,"InitConfigStructure completed");

  return ilRC;
}

/******************************************************************************
 *
 *  InitMyTab - for ct use. Copied from cophdl
 *
 ******************************************************************************
 */

static int InitMyTab(char *pcpCtTabName, char *pcpCtFldList, char *pcpCtFldSize)
{
  int ilRC = RC_SUCCESS;
  int i = 0;

  if (strcmp(pcpCtTabName,"STR_DESC") != 0)
  {
    CT_CreateArray(pcpCtTabName);
    CT_SetFieldList(pcpCtTabName,pcpCtFldList);
    CT_SetLengthList(pcpCtTabName, pcpCtFldSize);
    CT_SetDataPoolAllocSize(pcpCtTabName, 1000);
    dbg(TRACE,"CT DATA GRID <%s> INITIALIZED",pcpCtTabName);
  }
  else
  {

    for (i=0;i<=MAX_CTFLT_VALUE;i++)
    {
      CT_InitStringDescriptor(&argCtFlValue[i]);
      argCtFlValue[i].Value = malloc(4096);
      argCtFlValue[i].AllocatedSize = 4096;
      argCtFlValue[i].Value[0] = 0x00;
    }

    /* static STR_DESC rgCtTabLine; */
    CT_InitStringDescriptor(&rgCtTabLine);
    rgCtTabLine.Value = malloc(4096);
    rgCtTabLine.AllocatedSize = 4096;
    rgCtTabLine.Value[0] = 0x00;

    /* static STR_DESC rgCtTabData; */
    CT_InitStringDescriptor(&rgCtTabData);
    rgCtTabData.Value = malloc(4096);
    rgCtTabData.AllocatedSize = 4096;
    rgCtTabData.Value[0] = 0x00;



    /* static STR_DESC rgCtFlLine; */
    CT_InitStringDescriptor(&rgCtFlLine);
    rgCtFlLine.Value = malloc(4096);
    rgCtFlLine.AllocatedSize = 4096;
    rgCtFlLine.Value[0] = 0x00;


    /* static STR_DESC rgCtFlData; */
    CT_InitStringDescriptor(&rgCtFlData);
    rgCtFlData.Value = malloc(4096);
    rgCtFlData.AllocatedSize = 4096;
    rgCtFlData.Value[0] = 0x00;

    /* static STR_DESC rgCtHtLine; */
    CT_InitStringDescriptor(&rgCtHtLine);
    rgCtHtLine.Value = malloc(4096);
    rgCtHtLine.AllocatedSize = 4096;
    rgCtHtLine.Value[0] = 0x00;


    /* static STR_DESC rgCtTabList; */
    /* static STR_DESC rgCtTabUrno; */

/*
static STR_DESC rgCtAftLine;
static STR_DESC rgCtAftData;
static STR_DESC rgCtAftList;
static STR_DESC rgCtAftUrno;


static STR_DESC rgCtCfdLine;
static STR_DESC rgCtCfdData;
static STR_DESC rgCtCfgList;
static STR_DESC rgCtCfgUrno;

static STR_DESC rgCtFltLine;
static STR_DESC rgCtFltData;
static STR_DESC rgCtFltList;
static STR_DESC rgCtFltUrno;
*/

    dbg(TRACE,"STRING BUFFERS <%s> INITIALIZED",pcpCtTabName);
  }
  return ilRC;
} /* end of InitMyTab */


void printFormulas()
{
    int a=0;
    
    dbg(TRACE, "printFormulas:: Start");
    
    dbg(TRACE, "printFormulas:: Arrival Adid[%s], Departure Adid[%s]", pcgArrAdid, pcgDepAdid);
    dbg(TRACE, "printFormulas:: TOTAL ARRIVAL [%d]", iFormula);

    for (a=0; a<iFormula; a++)
    {
        dbg(TRACE, "printFormulas:: Arrival[%d/%d], string[%s]", 
            a+1, iFormula, sFormula[a].strString);    
        dbg(TRACE, "printFormulas::     name[%s], dssn[%s], type[%s], styp[%s], sstp[%s], ssst[%s], apc3[%s]",
            sFormula[a].name, sFormula[a].dssn, sFormula[a].type, sFormula[a].styp, sFormula[a].sstp, 
            sFormula[a].ssst, sFormula[a].apc3);
        dbg(TRACE, "printFormulas::     Formula[%s]", sFormula[a].strFormula);
    }
    
    dbg(TRACE, "printFormulas:: TOTAL DEPARTURE [%d]", iFormulaDep);
    for (a=0; a<iFormulaDep; a++)
    {
        dbg(TRACE, "printFormulas:: Departure[%d/%d], string[%s]", 
            a+1, iFormulaDep, sFormulaDep[a].strString);    
        dbg(TRACE, "printFormulas::     name[%s], dssn[%s], type[%s], styp[%s], sstp[%s], ssst[%s], apc3[%s]",
            sFormulaDep[a].name, sFormulaDep[a].dssn, sFormulaDep[a].type, sFormulaDep[a].styp, sFormulaDep[a].sstp, 
            sFormulaDep[a].ssst, sFormulaDep[a].apc3);
        dbg(TRACE, "printFormulas::     Formula[%s]", sFormulaDep[a].strFormula);
    }
        
    dbg(TRACE, "printFormulas:: End");
    return;        
}

/******************************************************************************
 *
 *  LoadGridData - copied from cophdl
 *
 ******************************************************************************
 */

static int LoadGridData(char *pcpGridName, char *pcpTblName, char *pcpSqlKey)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  char pclTblName[16] = "";
  char pclFldList[1024] = "";
  char pclSqlCond[1024] = "";
  char pclSqlData[1024] = "";
  char pclSqlBuff[1024] = "";
  long llCurTabLine = 0;
  long llMaxTabLine = 0;
  int ilFldCnt = 0;
  int ilLegCnt = 0, ilCount;
  short slCursor = 0;
  short slFkt = 0;
  dbg(TRACE,"---------------------------------");
  dbg(TRACE,"LOADING <%s> RECORDS",pcpGridName);
  dbg(TRACE,"---------------------------------");
  CT_ResetContent(pcpGridName);
  CT_GetFieldList(pcpGridName, pclFldList);

  strcpy(pclTblName,pcpTblName);
  strcpy(pclSqlCond,pcpSqlKey);
  sprintf(pclSqlBuff,"SELECT %s FROM %s %s ",pclFldList,pclTblName,pclSqlCond);
  dbg(TRACE,"<%s>",pclSqlBuff);
  ilFldCnt = field_count(pclFldList);
  slCursor = 0;
  slFkt = START;
  ilGetRc = DB_SUCCESS;
  ilCount = 0;
  while (ilGetRc == DB_SUCCESS)
  {
    ilGetRc = sql_if(slFkt,&slCursor,pclSqlBuff,pclSqlData);
    if (ilGetRc == DB_SUCCESS)
    {
      BuildItemBuffer(pclSqlData,"",ilFldCnt,",");
      TAB_TrimRecordData(pclSqlData, ",");
      /*dbg(DEBUG,"<%s>",pclSqlData); */
      CT_InsertTextLine(pcpGridName,pclSqlData);
      ilCount++;
    }
    slFkt = NEXT;
  }
  close_my_cursor(&slCursor);
  dbg(TRACE,"----Loaded %d records -------------------------------",ilCount);

  return ilRc;
}

/******************************************************************************
 *
 *  TAB_TrimRecordData - copied from cophdl
 *
 ******************************************************************************
 */

int TAB_TrimRecordData(char *pclTextBuff,char *pcpFldSep)
{
  int ilCnt = 0;
  char *pclDest = NULL;
  char *pclSrc = NULL;
  char *pclLast = NULL; 
  /* last non blank byte position */
  /* but keep at least one blank */
  pclDest = pclTextBuff;
  pclSrc  = pclTextBuff;
  pclLast  = pclDest - 1;
  if (*pclSrc == ' ')
  {
    pclLast = pclTextBuff;
    pclSrc++;
    pclDest++;
  }
  while (*pclSrc != '\0')
  {
    if (*pclSrc == pcpFldSep[0])
    {
      pclDest = pclLast + 1;
      *pclDest = *pclSrc;
      pclSrc++;
      pclDest++;
      if (*pclSrc == ' ')
      {
        pclLast = pclDest;
      }
    }
    *pclDest = *pclSrc;
    if (*pclDest != ' ')
    {
      pclLast = pclDest;
    }
    pclDest++;
    pclSrc++;
  }
  pclDest = pclLast + 1;
  *pclDest = '\0';
  return ilCnt;
} /* end TAB_TrimRecordData */

/******************************************************************************
 *
 *  GetLoatabValue - find the matching line in the CT array of current LOA values.
 *    Return RC_SUCCESS is a valid (i.e. non space) value found.
 *    Return RC_FAIL if no value or a space is found.
 *
 ******************************************************************************
 */

static int  GetLoatabValue (char *pcpDssn, char *pcpType, char *pcpStyp,
              char *pcpSstp, char *pcpSsst, char *pcpValu)
{
  int  ilRC = RC_FAIL;
  char pclFunc [] = "GetLoatabValue: ";
  int  llCurLine,llMaxLine;
  char pclLoaType [32], pclLoaStyp [32], pclLoaSstp [32];
  char pclLoaSsst [32], pclLoaDssn [32];

  /*
    If dssn list begins with a comma the first call will be with
    an empty dssn.
  */
  if (strcmp (pcpDssn,"") == 0)
    return ilRC;

  llMaxLine = CT_GetLineCount(pcgLoaTabGrid) - 1;

  for (llCurLine=0;llCurLine<=llMaxLine;llCurLine++)
  {
    CT_GetMultiValues(pcgLoaTabGrid, llCurLine, "DSSN", pclLoaDssn);
    CT_GetMultiValues(pcgLoaTabGrid, llCurLine, "TYPE", pclLoaType);
    CT_GetMultiValues(pcgLoaTabGrid, llCurLine, "STYP", pclLoaStyp);
    CT_GetMultiValues(pcgLoaTabGrid, llCurLine, "SSTP", pclLoaSstp);
    CT_GetMultiValues(pcgLoaTabGrid, llCurLine, "SSST", pclLoaSsst);

    if ((strcmp (pcpDssn,pclLoaDssn) == 0) &&
      (strcmp (pcpType,pclLoaType) == 0) &&
      (strcmp (pcpStyp,pclLoaStyp) == 0) &&
      (strcmp (pcpSstp, pclLoaSstp) == 0) &&
      (strcmp (pcpSsst,pclLoaSsst) == 0))
    { 
      CT_GetMultiValues(pcgLoaTabGrid, llCurLine, "VALU", pcpValu);
      dbg(TRACE,"%s Found match <%s><%s><%s><%s><%s><%s>",
        pclFunc,pclLoaDssn,pclLoaType,pclLoaStyp,pclLoaSstp,pclLoaSsst,pcpValu);
      ilRC = RC_SUCCESS;

      if (strcmp (pcpValu," ") == 0)
      {
        dbg(TRACE,"%s But value is blank",pclFunc);
        ilRC = RC_FAIL;
      }
      break;
    }
  } 

  if (ilRC == RC_FAIL)
    dbg(TRACE,"%s No match for <%s><%s><%s><%s><%s>",
        pclFunc,pcpDssn,pcpType,pcpStyp,pcpSstp,pcpSsst);

  return ilRC;
}

/******************************************************************************
 *
 *  GetVariableValue - find variable name within a section, and try to get its
 *    value from Loatab grid. If received dssn is not in priority, RC_FAIL.
 *    If no value available, RC_FAIL.
 *
 ******************************************************************************
 */
static int  GetVariableValue (int ipSectionIdx, char *pcpVarName,
  char *pcpInputDssn, int *ipVal)
{
  char pclFunc [] = "GetVariableValue: ";
  int ilRC = RC_SUCCESS, ilRC2,ilNoDssn,jj;
  int idxVar;
  char pclCurVarDssn [32],pclValue [32];

  for (idxVar = 0; idxVar < N_PARAMETERS; idxVar++)
  {
    if (sgConfig [ipSectionIdx].variable[idxVar].name [0] == '\0')
    {
      dbg(TRACE,"%s Could not find variable <%s> in section <%s>",
        pclFunc,pcpVarName,sgConfig [ipSectionIdx].name);
      ilRC = RC_FAIL; 
      break;
    }

    if (strcmp (sgConfig [ipSectionIdx].variable[idxVar].name,pcpVarName) == 0)
    {
      if (strstr (sgConfig [ipSectionIdx].variable[idxVar].dssn,pcpInputDssn) == '\0')
      {
        dbg(TRACE,"%s Input DSSN <%s> not in variable's <%s> DSSN list <%s>",
          pclFunc,pcpInputDssn,sgConfig [ipSectionIdx].variable[idxVar].name,
          sgConfig [ipSectionIdx].variable[idxVar].dssn);
        ilRC = RC_FAIL; 
        break;
      }

      ilNoDssn = get_no_of_items (sgConfig [ipSectionIdx].variable[idxVar].dssn);
      ilRC2 = RC_FAIL;
      for (jj = 1; jj <= ilNoDssn; jj++ )
      {
        (void) get_real_item (pclCurVarDssn, sgConfig [ipSectionIdx].variable[idxVar].dssn, jj); 
        ilRC2 = GetLoatabValue (pclCurVarDssn,
          sgConfig [ipSectionIdx].variable[idxVar].type,
          sgConfig [ipSectionIdx].variable[idxVar].styp,
          sgConfig [ipSectionIdx].variable[idxVar].sstp,
          sgConfig [ipSectionIdx].variable[idxVar].ssst,
          pclValue);

        if (ilRC2 == RC_SUCCESS)
        {
          *ipVal = atoi (pclValue);
          dbg(TRACE,"%s Found match <%s><%s><%d>",pclFunc,
            sgConfig [ipSectionIdx].variable[idxVar].name,pclCurVarDssn,*ipVal);

          /* Stop scaning the variable's dssn list */
          break;
        } 
      }
      ilRC = ilRC2;
      /* No more scanning to find the matching variable name */
      break;
    }
  }
  
  return ilRC;
}

int TrimSpace( char *pcpInStr )
{
    int ili = 0; 
    int ilLen;
    char *pclOutStr = NULL;
    char *pclP;
    char *pclFunc = "TrimSpace";

    ilLen = strlen( pcpInStr );
    if( ilLen <= 0 )
        return;

    pclOutStr = (char *)malloc(ilLen + 10); 
    pclP = pcpInStr;

    /* Trim front spaces */
    while( pclP && *pclP == ' ' )
        pclP++;

    while( *pclP )
    {    
       pclOutStr[ili++] = *pclP;
       if( *pclP != ' ' )
           pclP++;
       else 
       {    
           while( *pclP == ' ' )
               pclP++;
       }    
    }    
    /* Trim back space */
    if( pclOutStr[ili-1] == ' ' )
        ili--;
    pclOutStr[ili] = '\0';
    strcpy( pcpInStr, pclOutStr );
    if( pclOutStr != NULL )
        free( (char *)pclOutStr );
}

//Frank v1.07
/*
int insertLoaTab(char strSql[][1025], int iSize)
{
    char strErr[513];
    int iRet=RC_SUCCESS;
    int iTmp;
    int a=0;
    
    dbg(DEBUG, "insertLoaTab:: Start");
    
    for (a=0; a<iSize; a++)
    {
        dbg(DEBUG, "insertLoatab:: Inserting [%d/%d]", a+1, iSize);
        memset(strErr, 0, sizeof(char)*513);
        iRet=executeSql(strSql[a],strErr); 
    }
    
    dbg(DEBUG, "insertLoaTab:: Result[%d]", iRet);
    return iRet;    
}
*/

int executeSql(char *strSql, char *strErr)
{
    int iRet=0;
    short    slActCursor;
    char data_area[1024];
    unsigned long ilBufLen = 512;    // modified int -> long as defined
    unsigned long ilErrLen = 0;    // modified int -> long as defined

                  iRet = sql_if(START,&slActCursor,strSql,data_area);
         //ilRCdb = SqlIfArray(slFkt,&slCursor,pclSqlBuf,pcpDatList,0,&rgRecDesc);
    if ( iRet==RC_FAIL )
    {
        sqlglm(strErr,&ilBufLen,&ilErrLen);
        strErr[ilErrLen]=0;
    } 
    commit_work();        
    close_my_cursor(&slActCursor);    
    dbg(DEBUG, "executeSql:: strSql [%s] Error[%s] Result[%d]", strSql, strErr, iRet);     
    return iRet;    
}

static int FillArray(char *pcpValueList)
{
    int ilRC = RC_SUCCESS;
    char *pclFunc="FillArray:";
  
    dbg(DEBUG, "FillArray:: Start");
    
    sprintf(pcgLoaBuf, "%s%s\n", pcgLoaBuf, pcpValueList);
    
    dbg(DEBUG, "FillArray:: End Result[%d]", ilRC);   
    return ilRC;
}

static int SqlArrayInsert(char *pcpFldList, char *pcpValList, char *pcpDatList)
{
    int ilRC = RC_SUCCESS;
    char *pclFunc="SqlArrayInsert";
    int ilRCdb = DB_SUCCESS;
    char pclSqlBuf[32728];
    short slFkt = 0;
    short slCursor = 0;

    dbg(TRACE, "SqlArrayInsert:: Start");
   
    sprintf(pclSqlBuf,"INSERT INTO LOATAB (%s) VALUES (%s)",pcpFldList,pcpValList);
    
    dbg(TRACE, "SqlArrayInsert:: executing[%s]", pclSqlBuf);
    dbg(TRACE, "SqlArrayInsert:: data[%s]", pcpDatList);

    slCursor = 0;
    slFkt = 0;
  
    ilRCdb = SqlIfArray(slFkt,&slCursor,pclSqlBuf,pcpDatList,0,&rgRecDesc);
    if (ilRCdb == DB_SUCCESS)
    {
        commit_work();
    }
    else
    {
        rollback();
        dbg(TRACE,"SqlArrayInsert:: Error[%d]",ilRCdb);
    }
    close_my_cursor(&slCursor);
    
    dbg(TRACE, "SqlArrayInsert:: End Result[%d]", ilRCdb);
    return ilRCdb;
} /* End of SqlArrayInsert */
/******************************************************************************
 *
 *  
 *
 ******************************************************************************
 */

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */



