#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/dqahdl.c 1.12 07/11/2013 11:00:00 AM WON Exp  $";
#endif /* _DEF_mks_version */


/*************************************************
Note:  Version .01
Initial release.
Bug Founds: 
1.	Move check--> separate arrival to departure
2.	No load record -> error instead of pass
3.  If MACRO definition not found i.e TOTAL_PAX etc do nothing no dml.
4.	# is blank

Version .02
1.	Fixed 4 issues from version 01.
2.	Provide a command DQC to perform load and move check for testing purposes (no data updates)
3.	No data update if status_upd=NO just perform load and move check.
4.  When updating flight (AFTTAB) use SendCedaEvent instead of directly sending sql commands.

Version .03
1.	TIM command for scheduled task.
2.	DQC command for testing (without DB)


Version .04
1.	TIM command for up to 26 scheduled tasks.
2.  fine tuning.

Version .05
1.	Resolve bugs
2.	Use new function doCheck (for LOA, DQC, TIM)

Version .06 
1.	Uses doCheck function on all cmds.

Version .07
1.	Fixed more bugs.

Version .08
1.	Load check no record is pass
2.	Communication with dqamgr working.

Version .09:

1.	Make sure only confirm/unconfirm send DQC on flight.
2.	if strApc3 is null dont include in condition.

Version .10
1.	Implemented on confirm checking of arrival flight.
2.	Implemented on unconfirm checking of departure flight.

Version .11:
1.	Include MC in checking of arrival and departure flight.
2.  If all in Loatab check failed in the CONDITION check, DQCS=""
3.	Load Check if no record found and its blank check, then it fails.  Negative check stays the same if no record found pass.

Version .12:
1.	When no record found in load blank check, set iTmp to fail not iRet.
2.	Checking of Arrival and Departure flight on Confirm/Unconfirm, added the field ADID=A (arrival)/ ADID=D(departure)

Version .13:
1.	If ftyp=T or G skip validation.
2. 	When sending broadcast in insertDQCTAB use DQC as command.
3.	If confirm departure, check arrival (also include if its move check and status = MC), for unconfirm vise versa

Version .14:
1.	Initial release in AUHTest

Version .15
1.	CFM/UNCFM can confirm and unconfirm paired flights.

Version .16 20121022
1.	2nd Release.

Version .17 20121109
1.	Change ERP process.  ERP processing should involve rotation flight.
2.	Uploaded to AUHTEST

Version .18 20121122
1.	Change processERP to process those records with ADID=B only.
2.	(20121206) Read loatab table only once instead of many times.

Version .19
1.	Delete dqctab must come right before insert dqctab
2.	Added conditions for processing MOVE dqc (UFR,IFR,ERP,TIM)
3.	Addec conditions for processing LOAD dqc (TIM,ERP,LOA)
4.	Make flight queue number configurable.  Entry in dqahdl.cfg added FLIGHT_QUEUE = n.  If this entry is not found, default of 7800 is use.
5.	Send broadcast for dqctab only if there is a change in DQCS.
6.	Send to flight only if there is a change in DQCS
7.	Make commmands for move and load configurable

Version .20 (YaFei)
1      Read TIFA and TIFD from afttab
2      Add function to Read DQCTab (ReadDQCTab)
3      Disable the Broadcast for TIFA and TIFD later than Current Time(UTC) + Tim_Gap
4      Disable the Broadcast for No DQCTAB record change

Version .21 (WON)
1.	Implemented no broadcast if twEnd contains ",ImportTool,".

Version 1.06 (WON)
1.	UFIS-3082.  Ignore flight updates if best time of the flight < current time + x minutes(configurable).
2.	Added ADDITIONAL_MINUTES for the number of minutes to be added.
3.  Skip if towing or ground (FTYP=T or G).
4.  Modified schedule handler to include DAY handling.
5.  Negative value Check corrected on loatab size.

Version 1.07 (WON)
1.	If no record on loatab, dq check will fail (originally its a pass).

Version 1.08 (WON)
1.	Configurable MOVE conditions (to skip or not).  Take note if pass then continue with MOVE check.
2.	Configurable DQ conditions (to skip whole dqc or not).  Take note if pass then continue.

Version 1.09 (WON)
1.  Added more logs to trace why there are entries in afttab with dqcs='%F%' without records in dqc
2.  Added DQC_IMPLEMENTATION_DATE to make sure only records after this date are process.

Version 1.10 (WON)
1.  Do cleanup (set afttab.dqcs=' ' via flight) and delete from dqctab when DQ Skip.

Version 1.11 (WON)
1.  When CONFIRM, reject those with completeness check failed.

Version 1.12 (WON)
1.  When responding, make sure that queue id is greater than or equal to 10k (only respond to front end).
*************************************************/


/********************************************************************************
 *          
 * DQA HANDLER
 *                      
 * Author         : won 
 * Date           : 20120901                                                   
 * Description    : UFIS-1879, UFIS-2184
 *                                                                     
 * Update history :                                                   
 *     20120127 WON 1.00   First release.
 *
 *  Note:  MAX_SELECT_ITEMS in db_if.h
 *
 ********************************************************************************/


/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <pthread.h>


#include "debugrec.h"
#include "db_if.h"
#include "urno_fn.inc"

//#include "ugccsma.h"
//#include "msgno.h"
//#include "glbdef.h"
//#include "quedef.h"
//#include "uevent.h"
//#include "sthdef.h"
//#include "debugrec.h"
//#include "hsbsub.h"

//#include "ct.h"


/******************************************************************************/
/* Macros */
/******************************************************************************/
//  Definition for AFTTAB field.  Take note that vial is 1024 characters long, so if you need this data
//	Make sure you adjust the size of the array string.
/*
char **strArrAfttabFieldValues;
char **strArrAfttabFieldNames ;
int iAfttabFieldsCount;     
*/
                          
#define MAX_ERROR	2048
#define DB_NO_ROW 1
#define MAX_FLNU 10
#define MAX_TYPE 3
#define MAX_STYP 3
#define MAX_SSTP 10
#define MAX_SSST 3
#define MAX_APC3 3
#define MAX_DSSN 3
#define MAX_VALU 32
#define FRONTEND_QUEUE 10000


typedef struct
{
	char FLNU[MAX_FLNU+100];
	char TYPE[MAX_TYPE+100];
	char STYP[MAX_STYP+100];
	char SSTP[MAX_SSTP+100];
	char SSST[MAX_SSST+100];
	char APC3[MAX_APC3+100];
	char DSSN[MAX_DSSN+100];
	char VALU[MAX_VALU+100];	
}LOATAB;
LOATAB arrLoa[MAX_ERROR];
int iLoaCount;

typedef struct
{
    char strUrno[11];
    char strFlnu[11];
    char strType[11];
    char strReas[257];
    char cPass;
    char strHopo[9];	
}DQCTAB;
DQCTAB sDqctab[MAX_ERROR];
DQCTAB gsDqctab;
int iDqctabCount;


DQCTAB sgOldDqcTab[MAX_ERROR];
static int igOldDqcCnt = 0;
static int igTimGapSec = 10800; /* 3 hour Gap */
static int igDqcRead = FALSE;

char strArrAfttabError[MAX_ERROR][257];
int iArrAfttabErrorCount=0;


/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
int  debug_level = DEBUG;
int igFlightQueue = 7800;
char pcgDqaCmd[128];
char pcgLoadCmd[128];
char pcgMoveCmd[128];

/******************************************************************************/
/* Declarations                                                               */
/******************************************************************************/
typedef struct
{
    char strFieldName[5];
    char *strFieldDesc;    	
} DBFIELDSCONFIG;
DBFIELDSCONFIG *arrAfttabDBFieldsConfig=NULL;
int iArrAfttabDBFieldsConfigCount=0;
char *strAfttabFieldNames;
char **strArrAfttabFieldValues;

DBFIELDSCONFIG *arrLoatabDBFieldsConfig=NULL;
int iArrLoatabDBFieldsConfigCount=0;
char *strLoatabFieldNames;
int igAdditionalMinutes=0;
char pcgImplementationDate[20];

typedef struct
{
	char *strString;
	char *strFieldBlankCheck;
    char *strName;
    char *strField;
    char *strOp;
    char *strValue;
}FIELDCHECK;

typedef struct
{
	char *strDataType;
    char cUnary1;
    char *strField1;
    char *strOp;
    char cUnary2;
    char *strField2;	
}EXPRESSION;

typedef struct
{
	char *strString;
	char *strName;
    char *strFlightType;	
    char *strMsg;
    char *strExp1;
    EXPRESSION sExp1;
    char *strOp;
    char *strExp2;
    EXPRESSION sExp2;
    char *strValue;
}VALUECHECK;

typedef struct
{
    char *strAfttabDQCSUpdate;
    char *strType;
    char *strBlankReason;
    char *strAllBlankCheck;
    char *strArrBlankCheck;
    char *strDepBlankCheck;
    FIELDCHECK *arrAllBlankCheck;
    int iArrAllBlankCheckCount;
    FIELDCHECK *arrArrBlankCheck;
    int iArrArrBlankCheckCount;
    FIELDCHECK *arrDepBlankCheck;
    int iArrDepBlankCheckCount;
    
    char *strValueCheck;
    VALUECHECK *arrValueCheck;
    int iArrValueCheckCount;

    int iFieldCheck;
    FIELDCHECK *sFieldCheck;    
    char *strConditions;
}MOVECONFIG;
MOVECONFIG sMoveConfig;

typedef struct
{
    int iFieldCheck;
    FIELDCHECK *sFieldCheck;    
    char *strConditions;  	
}DQC;
DQC rgDQC;

typedef struct
{
    char *strKey;
    char *strValue;	
}PAIR;

typedef struct
{
    PAIR *sPair;
    int iMapCount;
}MAP;

typedef struct
{
	char *strName;
	char *strString;
	char *strMsg;
    FIELDCHECK sCond;
    char *strType;
    char *strStyp;
    char *strSstp;
    char *strSsst;
    char *strApc3;	
    char *strValue;
    char *strOp;
    char *strValue2;
}LOADCHECK;

typedef struct
{
	char *strName;
	char *strMsg;
	char *strString;
    LOADCHECK sLoadCheck1;
    char *strOp;
    LOADCHECK sLoadCheck2;	
}LOADEXP;

typedef struct
{
    char *strHeader;
    char *strConditions;
    int iCondCheckCount;
    FIELDCHECK *sCondCheck;
    LOADCHECK sLoadCheck;
    char *strAcceptZero;
    char *strBlankReas;
}LOADHEADER;

typedef struct
{
    char *strHeader;
    char *strConditions;
    char *strValueCheck;

    int iFieldCheck;
    FIELDCHECK *sFieldCheck;
    int iLoadCheck;
    LOADCHECK *sLoadCheck;
}LOADBLANK;


typedef struct
{
	char *strHeader;
    char *strConditions;
    char *strVC;
    int iFieldCheck;	
    FIELDCHECK *sFieldCheck;
    int iLoadExp;
    LOADEXP *sLoadExp;
}LOADVALUECHECK;

typedef struct
{
    char *strAfttabDQCSUpdate;
    char *strType;	
    char *strFlightRef;
    char *strLoadBlankCheckSession;
    char *strLoadValueCheckSession;
    char *strLoadNegCheck;
    char *strLoadFile;
    
    int iLoadHeader;
    LOADHEADER *sLoadHeader;   
     
    int iLoadVC;
    LOADVALUECHECK *sLoadVC;
    
    int iLoadBlank;
    LOADBLANK *sLoadBlank;    
}LOADCONFIG;
LOADCONFIG sLoadConfig;

typedef struct
{
    char *strFieldName;
    char *strValue;	
}DBFIELDS;

typedef struct
{
    int iColumn;
    DBFIELDS *sDBFields;	
}DBROWS;

typedef struct
{
    char *strName;
    int iRow;
    DBROWS *sDBRows;
}DBSELECT;
DBSELECT sDBSelectLoaTab;

typedef struct
{
	char *strName;
	char *strTimeRef;
    char *strTimeZone;  
    char *strStartTime;
    char *strEndTime;
    char *strArrivalField; 
    char *strDepartureField;
    char *strDQCS;
}SCHED;
SCHED *sSched;
int iSched=0;





/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem=NULL; /* The queue item pointer  */
static EVENT *prgEvent=NULL;  /* The event pointer */
static int   igQueOut      = 0;
static char  cgConfigFile[512];
static int   igInitOK=FALSE ;
static int   igItemLen=0; /* length of incoming item */
int  igDbgSave = DEBUG;
static char  cgHopo[8] = "";      /* default home airport    */
static char  cgTabEnd[8] ="";     /* default table extension */


static int igToBcHdl = 1900;
static char pcgRecvName[128]; /* BC-Head used as WKS-Name */
static char pcgDestName[128]; /* BC-Head used as USR-Name */
static char  pcgTwStart[128];
static char  pcgTwEnd[128];
BC_HEAD *prlBchd = NULL;  /* Broadcast header */
CMDBLK  *prlCmdblk = NULL;    /* Command Block */
static int igBroadCast = TRUE;
    
//static int  igModIdFlight = -1;	        /* the FLIGHT process  & it's message */
//static char pcgAftSections [128];	/* all the defined sections in config */

//static char pcgLoaTabGrid[] = "LoaTab";
//static char pcgLoaTabFldList[] = "APC3,DSSN,FLNO,FLNU,HOPO,IDNT,RURN,SSST,SSTP,STYP,TIME,TYPE,URNO,VALU";
//static char pcgLoaTabFldSize[] = "3   ,3   ,9   ,10  ,3   ,22  ,10  ,3   ,10  ,3   ,14  ,3   ,10  ,6   ";
//static char pcgAftCmd [32];

//static t_struct_config_section sgConfig [N_SECTIONS];

/******************************************************************************/
/* Function prototypes                                                          */
/******************************************************************************/
static void HandleSignal(int);
static void HandleQueues();
static int InitPgm();
static void Terminate();
static void HandleQueErr(int pipErr);
static int  Reset(void); 
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,char *pcpCfgBuffer, char *pcpDefault);
static int  HandleData(void);       /* CEDA  event data     */
static void HandleErr(int);         /* General errors */

int fileExist(char *str);
int rtrim(char *str, char cTrim);
int ltrim(char *str, char cTrim);
int trim(char *str, char cTrim);

int readConfigFile(char *strFile, char *strHeader, char *strKey, char *strVal);
int readHeader(char *strFile, char *strHeader);
int getConfig(char *pStrFile);
int addArrDBFields(char *str, char *strDBTableName, DBFIELDSCONFIG *sDB, char **strFieldNames);
void printConfig();
int parseLoadCheck(char *str, LOADCHECK *sLoad);
int parseLoadExpression(char *str, char *strField, char *strOp, char *strField2);

int processERP(char *strFlnuArr, char *strFlnuDep);
int processTIM(char *strCmd);
int processDQC(char *strFlnu);
int processDFR(char *strFlnu);
int processIFRUFR(char *strFlnu);
int processLOA(char *strFlnu);
int processCFM(char *strFlnu, char *strArr, char *strDep, char *strType);
int processUNCFM(char *strFlnu, char *strArr, char *strDep, char *strType);
int doMoveCheck(int *ipCompleteFlag, int *ipPlausibilityFlag);
int doCheck(char *strFlnu, char *strCmd, char *strType, char *pStrDQCS, int iPair, char *strNewDQCS);
int doLoadCheck(char *strFlnu, int *ipCompleteness, int *ipPlausibility);
int readLoadHeader(char *strFile, char *strHeader, LOADHEADER *sHeader);
int readLoadBlank(char *strFile, char *strHeader, LOADBLANK *sLoadBlank);
int readLoadValueCheck(char *strFile, char *strHeader, LOADVALUECHECK *sHeader);

char **getDBFieldsInDB(char *strTableName, char **strFieldNames, int *iSize, int *iRet);
char **getAfttabRecord(char *strFlnu, int *iRet);
char **getAfttabRecordUsingRkey(char *strRkey, char *strAdid, int *iRet);
int getAfttabRecords(char *strSql);
int doBlankCheck(char *strFields);
int doFieldCheck(FIELDCHECK *sField);
int doVCCheck();
int evaluateLoadExp(char *strFlnu, LOADEXP *sLoad);
int evaluateExp(EXPRESSION *sValue, double *dResult);
int evaluateDate(char *strField1, char *strOp, char *strField2, double *dRes);
int getArrTabFieldIndex(char *strFind, DBFIELDSCONFIG *sDB, int iMax);
int getMapIndex(char *strFind, MAP *sDB);
int parseExpression(char *str, EXPRESSION *sExp);
int findInSelection(char *strField, char *strValue);
char **parseString(char *strString, char *strDelimeter, int *iSize);
int getFieldCheck(char *strFile, char *strHeader, char *strTable, char *strField, FIELDCHECK *sFieldCheck);
int getDBFieldDesc(char *strField, char *strTableName, char **strDBFieldDesc);
void freeStringArray(char **strArr, int iSize);
//int checkSTEV_X();
//int parseStringToFieldOpValue(char *strString, char *strField, char *strOp, char *strValue);
int printAfttabErrors();

void populateDQCTab(DQCTAB *sDq, char *strUrno, char *strFlnu, char *strType, char *strReas, char cPass, char *strHopo);
int deleteDQCTab(char *strflnu, char *strType);
int insertDQCTab(char *strCmd, int iFlag); 
int executeSql(char *strSql, char *strErr);
int updateFlightDQCS(char *strDqcs, char *strUrNo, char *strCmd);
void clearDBROWS(DBSELECT *sDBSelect);
int populateDBRows(char *strSql, char *strFields, int iFieldCount, DBSELECT *sDBSelect);
void printDBSelect(DBSELECT *sDB);

static int TriggerBchdlAction(char *strProcessName, char *pcpCmd, char *pcpTable, char *pcpSelection,
                              char *pcpFields, char *pcpData, char *pcpTwStart,
                              char *pcpTwEnd, int iClient, int iServer);
int getLoaRecords(char *strFlnu);   
int getLoaIndex(char *strFlnu, char *strType, char *strStyp, char *strSstp, char *strSsst, char *strApc3, char *strDssn);
static int sqlArrayInsert(char *pcpFldList, char *pcpValList, char *pcpDatList);

static int ReadDQCTab(char *pcpFlnu, char *pcpType);
static int DQCStaChk(char *pcpOldDqcs, char *pcpNewDqcs);
static int DQCTabChgChk(void);
int timeDiff(char *pcpDate1, char *pcpDate2, double *pdpOut);
int getSysDate(char *pcpDate);
int convertDateToStructTm(char *pcpDate, struct tm *rpTm);
int doCleanUp(char *pcpFlnu, char *pcpCmd);
int doCompletenessCheckGroup(char *pcpFields, FIELDCHECK *rpFields, int ipCount);
                           
/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
    int    ilRC = RC_SUCCESS;            /* Return code            */
    int    ilCnt=0;

    INITIALIZE;            /* General initialization    */
	
    /* handles signal       */
    (void)SetSignals(HandleSignal);
	(void) UnsetSignals ();
	
    /* Attach to the MIKE queues */
    do
    {
        ilRC = init_que();
        if(ilRC != RC_SUCCESS)
        {
            dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        }
    }while((ilCnt < 10) && (ilRC != RC_SUCCESS));

	
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
        sleep(60);
        exit(1);
    }
    else
    {
        dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }
       
    sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);

    dbg(TRACE,"MAIN: ctrl_sta[%d], HSB_STANDALONE[%d], HSB_ACTIVE[%d], HSB_ACT_TO_SBY[%d] ", ctrl_sta, HSB_STANDALONE, HSB_ACTIVE, HSB_ACT_TO_SBY);
    if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
        dbg(DEBUG,"MAIN: waiting for status switch ...");
        HandleQueues();
    }
    
    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
        dbg(TRACE,"MAIN: initializing ...");
        if(igInitOK == FALSE)
        {
            ilRC = InitPgm();
            if(ilRC != RC_SUCCESS)
            {
                dbg(TRACE,"MAIN: init failed!");
            } 
        }
    }
    else
    {
        Terminate();
    }

    dbg(TRACE,"==================== Entering main pgm loop  ================="); 
    sleep(10);
    if ( getConfig(cgConfigFile)==RC_FAIL )
    {
        dbg(TRACE, "Program exiting...  Please correct items (stated) on the config file... Restarting in 15 seconds");
        sleep(10);
        exit(1);
    }
        
    printConfig();

    //strArrAfttabFieldNames=getDBFieldsInDB("AFTTAB", &strAfttabFieldNames, &iAfttabFieldsCount, &ilRC); 
    if ( ilRC==RC_FAIL)
    {
        dbg(TRACE,"MAIN:: DB ERROR or MISSING AFTTAB Table");
        exit(1);	
    }    

    for(;;)
    {
        dbg(TRACE,"==================== START/END =================");
        ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,0,(char *)&prgItem);
        // ilRC = que (QUE_GETBIG, 0, mod_id, PRIORITY_3, 0, (char *) &prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text;
        if( ilRC == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRC != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRC);
            } /* fi */

            switch( prgEvent->command )
            {
                case    HSB_STANDBY    :
                	dbg(TRACE, "HSB_STANDBY");
                    ctrl_sta = prgEvent->command;
                    HandleQueues();
                    break;    
                case    HSB_COMING_UP    :
                	dbg(TRACE, "HSB_COMING_UP");
                    ctrl_sta = prgEvent->command;
                    HandleQueues();
                    break;    
                case    HSB_ACTIVE    :
                	dbg(TRACE, "HSB_ACTIVE");
                    ctrl_sta = prgEvent->command;
                    break;    
                case    HSB_ACT_TO_SBY    :
                	dbg(TRACE, "HSB_ACT_TO_SBY");
                    ctrl_sta = prgEvent->command;
                    /* CloseConnection(); */
                    HandleQueues();
                    break;    
                case    HSB_DOWN    :
                	dbg(TRACE, "HSB_DOWN");
                    /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                    ctrl_sta = prgEvent->command;
                    Terminate();
                    break;    
                case    HSB_STANDALONE    :
                	dbg(TRACE, "HSB_STANDALONE");
                    ctrl_sta = prgEvent->command;
                    break;    
                case    SHUTDOWN    :
                	dbg(TRACE, "SHUTDOWN");
                    /* process shutdown - maybe from uutil */
                    Terminate();
                    break;                    
                case    RESET        :
                	dbg(TRACE, "RESET");
                    ilRC = Reset();
                    break;
                case    EVENT_DATA    :
                	dbg(TRACE, "EVENT_DATA");
                    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
                    {
                        ilRC = HandleData();
                        if(ilRC != RC_SUCCESS)
                        {
                            HandleErr(ilRC);
                        }/* end of if */
                    }
                    else
                    {
                        dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
                        DebugPrintItem(TRACE,prgItem);
                        DebugPrintEvent(TRACE,prgEvent);
                    }/* end of if */
                    break;
                    
                case    TRACE_ON :
                	dbg(TRACE, "TRACE_ON");
                    dbg_handle_debug(prgEvent->command);
                    break;
                case    TRACE_OFF :
                	dbg(TRACE, "TRACE_OFF");
                    dbg_handle_debug(prgEvent->command);
                    break;
                default            :
                    dbg(TRACE,"MAIN: unknown event");
                    DebugPrintItem(TRACE,prgItem);
                    DebugPrintEvent(TRACE,prgEvent);
                    break;
            }
        }   
    } 
    
    exit(0);    
}

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int ipSig)
{
    int    ilRC = RC_SUCCESS;            /* Return code */
    switch(ipSig)
    {
        case SIGTERM:
            dbg(TRACE,"HandleSignal: Received Signal <%d> (SIGTERM). Terminating now...",ipSig);
            break;
        case SIGALRM:
            dbg(TRACE,"HandleSignal: Received Signal<%d>(SIGALRM)",ipSig);
            return;
            break;
        default:
            dbg(TRACE,"HandleSignal: Received Signal<%d>",ipSig);
            return;
            break;
    } /* end of switch */
    exit(0);
    
} /* end of HandleSignal */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
    int    ilRC = RC_SUCCESS;            /* Return code */
    int    ilBreakOut = FALSE;
    
    do
    {
        ilRC = que(QUE_GETBIGNW,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        dbg(TRACE, "XXXXXX");
        prgEvent = (EVENT *) prgItem->text; 
        
           
        if( ilRC == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRC != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRC);
            } /* fi */
        
            switch( prgEvent->command )
            {
                case    HSB_STANDBY    :
                    ctrl_sta = prgEvent->command;
                    break;    
    
                case    HSB_COMING_UP    :
                    ctrl_sta = prgEvent->command;
                    break;    
    
                case    HSB_ACTIVE    :
                    ctrl_sta = prgEvent->command;
                    ilBreakOut = TRUE;
                    break;    
            
                case    HSB_ACT_TO_SBY    :
                    ctrl_sta = prgEvent->command;
                    break;    
    
                case    HSB_DOWN    :
                    /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                    ctrl_sta = prgEvent->command;
                    Terminate();
                    break;    
    
                case    HSB_STANDALONE    :
                    ctrl_sta = prgEvent->command;
                    ilBreakOut = TRUE;
                    break;    

                case    SHUTDOWN    :
                    Terminate();
                    break;
                        
                case    RESET        :
                    ilRC = Reset();
                    break;
                        
                case    EVENT_DATA    :
                    dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
                    DebugPrintItem(TRACE,prgItem);
                    DebugPrintEvent(TRACE,prgEvent);
                    break;
                    
                case    TRACE_ON :
                    dbg_handle_debug(prgEvent->command);
                    break;
        
                case    TRACE_OFF :
                    dbg_handle_debug(prgEvent->command);
                    break;
            
                default            :
                    dbg(TRACE,"HandleQueues: unknown event");
                    DebugPrintItem(TRACE,prgItem);
                    DebugPrintEvent(TRACE,prgEvent);
                    break;
            } /* end switch */
        }
        
    } while (ilBreakOut == FALSE);
    if(igInitOK == FALSE)
    {
        ilRC = InitPgm();
        if(ilRC != RC_SUCCESS)
        {
            dbg(TRACE,"HandleQueues: : init failed!");
        } 
    }
}


/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int InitPgm()
{
    int    ilRC = RC_SUCCESS;            /* Return code */
    char     pclDbgLevel [iMIN_BUF_SIZE];
    char     pclDummy [iMIN_BUF_SIZE];
    char     pclDummy2 [iMIN_BUF_SIZE],pclTmpBuf [64];
    int ilCnt = 0 ;
    int ilItem = 0 ;
    int ilLoop = 0 ;
    int jj, kk;

    /* now reading from configfile or from database */
    SetSignals(HandleSignal);

    do
    {
        ilRC = init_db();
        if (ilRC != RC_SUCCESS)
        {
            check_ret(ilRC);
            dbg(TRACE,"InitPgm: init_db() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        } /* end of if */
    } while((ilCnt < 10) && (ilRC != RC_SUCCESS));

    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"InitPgm: init_db() failed! waiting 60 sec ...");
        sleep(60);
        exit(2);
    }
    else
    {
        dbg(TRACE,"InitPgm: init_db() OK!");
    }
    
    (void) ReadConfigEntry("MAIN","DEBUG_LEVEL",pclDbgLevel,"TRACE");
    if (strcmp(pclDbgLevel,"DEBUG") == 0)
    {
        dbg(DEBUG,"InitPgm: DEBUG_LEVEL IS <DEBUG>");
        igDbgSave = DEBUG;
    } 
    else if (strcmp(pclDbgLevel,"OFF") == 0) 
    {
        dbg(DEBUG,"InitPgm: DEBUG_LEVEL IS <OFF>");
        igDbgSave = 0;
    } 
    else 
    {
        dbg(DEBUG,"InitPgm: DEBUG_LEVEL IS <TRACE>");
        igDbgSave = TRACE;
    }

   (void) ReadConfigEntry("MAIN","DQC_IMPLEMENTATION_DATE",pcgImplementationDate,"");
   (void) ReadConfigEntry("MAIN","ADDITIONAL_MINUTES",pclTmpBuf,"0");
   igAdditionalMinutes=atoi(pclTmpBuf);

/*
    igModIdFlight = tool_get_q_id ("flight");
    if ((igModIdFlight == RC_FAIL) || (igModIdFlight == RC_NOT_FOUND))
    {
        dbg(TRACE,"InitPgm: ====== ERROR ====== Flight Mod Id not found, default to 7800");
        igModIdFlight = 7800; 
    }
    else
        dbg(TRACE,"InitPgm: Flight Mod Id <%d>", igModIdFlight);

    (void) ReadConfigEntry("MAIN","AFT_SECTIONS", pcgAftSections, "NONE");
    */
   

    if(ilRC == RC_SUCCESS)
    {
        /* read HomeAirPort from SGS.TAB */
        ilRC = tool_search_exco_data("SYS", "HOMEAP", cgHopo);
        if (ilRC != RC_SUCCESS)
        {
            dbg(TRACE,"InitPgm: EXTAB,SYS,HOMEAP not found in SGS.TAB");
            Terminate();
        }
        else
        {
           dbg(TRACE,"InitPgm: home airport <%s>",cgHopo);
        }
    }

    if(ilRC == RC_SUCCESS)
    { 
        ilRC = tool_search_exco_data("ALL","TABEND", cgTabEnd);
        if (ilRC != RC_SUCCESS)
        {
            dbg(TRACE,"InitPgm: EXTAB,ALL,TABEND not found in SGS.TAB");
            Terminate();
        }
        else
        {
            dbg(TRACE,"InitPgm: table extension <%s>",cgTabEnd);
        }
    }

    igInitOK = TRUE;
    debug_level = igDbgSave;
    return(ilRC);
}

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate()
{ 
    dbg(TRACE,"Terminate: now leaving ...");
    
    exit(0);    
}

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    int    ilRC = RC_SUCCESS;
    
    switch(pipErr) 
    {
        case    QUE_E_FUNC    :    /* Unknown function */
            dbg(TRACE,"HandleQueErr: <%d> : unknown function",pipErr);
            break;
        case    QUE_E_MEMORY    :    /* Malloc reports no memory */
            dbg(TRACE,"HandleQueErr: <%d> malloc failed",pipErr);
            break;
        case    QUE_E_SEND    :    /* Error using msgsnd */
            dbg(TRACE,"HandleQueErr: <%d> msgsnd failed",pipErr);
            break;
        case    QUE_E_GET    :    /* Error using msgrcv */
            dbg(TRACE,"HandleQueErr: <%d> msgrcv failed",pipErr);
            break;
        case    QUE_E_EXISTS    :
            dbg(TRACE,"HandleQueErr: <%d> route/queue already exists ",pipErr);
            break;
        case    QUE_E_NOFIND    :
            dbg(TRACE,"HandleQueErr: <%d> route not found ",pipErr);
            break;
        case    QUE_E_ACKUNEX    :
            dbg(TRACE,"HandleQueErr: <%d> unexpected ack received ",pipErr);
            break;
        case    QUE_E_STATUS    :
            dbg(TRACE,"HandleQueErr: <%d>  unknown queue status ",pipErr);
            break;
        case    QUE_E_INACTIVE    :
            dbg(TRACE,"HandleQueErr: <%d> queue is inaktive ",pipErr);
            break;
        case    QUE_E_MISACK    :
            dbg(TRACE,"HandleQueErr: <%d> missing ack ",pipErr);
            break;
        case    QUE_E_NOQUEUES    :
            dbg(TRACE,"HandleQueErr: <%d> queue does not exist",pipErr);
            break;
        case    QUE_E_RESP    :    /* No response on CREATE */
            dbg(TRACE,"HandleQueErr: <%d> no response on create",pipErr);
            break;
        case    QUE_E_FULL    :
            dbg(TRACE,"HandleQueErr: <%d> too many route destinations",pipErr);
            break;
        case    QUE_E_NOMSG    :    /* No message on queue */
            dbg(TRACE,"HandleQueErr: <%d> no messages on queue",pipErr);
            break;
        case    QUE_E_INVORG    :    /* Mod id by que call is 0 */
            dbg(TRACE,"HandleQueErr: <%d> invalid originator=0",pipErr);
            break;
        case    QUE_E_NOINIT    :    /* Queues is not initialized*/
            dbg(TRACE,"HandleQueErr: <%d> queues are not initialized",pipErr);
            break;
        case    QUE_E_ITOBIG    :
            dbg(TRACE,"HandleQueErr: <%d> requestet itemsize to big ",pipErr);
            break;
        case    QUE_E_BUFSIZ    :
            dbg(TRACE,"HandleQueErr: <%d> receive buffer to small ",pipErr);
            break;
        default            :    /* Unknown queue error */
            dbg(TRACE,"HandleQueErr: <%d> unknown error",pipErr);
            break;
    } 
         
    return;
} 

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int    ilRC = RC_SUCCESS;      
    
    dbg(TRACE,"Reset: now resetting");
           
    return ilRC;   
} 

/*******************************************************************************
* ReadConfigEntry
*******************************************************************************/
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,char *pcpCfgBuffer, char *pcpDefault)
{
    int ilRC = RC_SUCCESS;
    char clSection[256] = "\0";
    char clKeyword[256] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRC = iGetConfigEntry(cgConfigFile,clSection,clKeyword, CFG_STRING,pcpCfgBuffer);
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"ReadConfigEntry: Not found in %s: [%s] <%s>",cgConfigFile,clSection,clKeyword);
        dbg(TRACE,"ReadConfigEntry: use default-value: <%s>",pcpDefault);
        strcpy(pcpCfgBuffer,pcpDefault);
    } 
    else
    {
        dbg(TRACE,"ReadConfigEntry: Config Entry [%s],<%s>:<%s> found in %s", clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
    }
    
    return ilRC;
}

/******************************************************************************
* HandleData
******************************************************************************/
static int HandleData()
{
	int ilRC = RC_FAIL; 
    int iFlnuLocation=-1;
	int iDQCSLocation=-1;
	int iTypeLocation=-1;
	int iArrivalLocation=-1;
	int iDepartureLocation=-1;
	char strFlnu[32];
	char strArrivalFlnu[32];
	char strDepartureFlnu[32];
	int a=0;
	        
    char    *pclSelect;
    char    *pclFields;
    char    *pclData;
    
    char **strParamFieldNames=NULL;
    int iParamFieldNamesCnt=0;
    char **strParamFieldValues=NULL;
    int iParamFieldValuesCnt=0;

    igQueOut  = prgEvent->originator;
    prlBchd = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
    prlCmdblk = (CMDBLK  *) ((char *)prlBchd->data);
    pclSelect = (char *)prlCmdblk->data;
    pclFields = (char *)pclSelect + strlen(pclSelect) + 1;
    pclData   = (char *)pclFields + strlen(pclFields) + 1;
    strcpy(pcgTwStart,prlCmdblk->tw_start);
    strcpy(pcgTwEnd,prlCmdblk->tw_end);
 
    strParamFieldNames=parseString(pclFields, ",", &iParamFieldNamesCnt);
    strParamFieldValues=parseString(pclData, ",", &iParamFieldValuesCnt);
    
    memset (pcgDestName, 0, (sizeof (prlBchd->dest_name) + 1));
    strncpy (pcgDestName, prlBchd->dest_name, sizeof (prlBchd->dest_name));
    memset (pcgRecvName, '\0', (sizeof (prlBchd->recv_name) + 1));
    strncpy (pcgRecvName, prlBchd->recv_name, sizeof (prlBchd->recv_name));
    
    dbg(TRACE, "HandleData:: CMD[%s]: Parameter Fields[%s][%d] & Parameter Values[%s][%d]", 
        prlCmdblk->command,pclFields, iParamFieldNamesCnt, pclData, iParamFieldValuesCnt);    
    dbg(TRACE,"HandleData:: CMD <%s> TBL <%s>", prlCmdblk->command,prlCmdblk->obj_name);
    dbg(TRACE,"HandleData:: FROM <%d> WKS <%s> USR <%s>",
        igQueOut,prlBchd->recv_name,prlBchd->dest_name);

    dbg(TRACE, "HandleData:: prlCmdblk->tw_start[%s], prlCmdblk->tw_end[%s]", 
        prlCmdblk->tw_start, prlCmdblk->tw_end);
    strcpy (pcgTwEnd, prlCmdblk->tw_end);
    if ( strstr(pcgTwEnd, ",ImportTool,")!=NULL && strstr(prlCmdblk->tw_start,".NBC.") == NULL )
        sprintf(pcgTwStart, ".NBC.,%s", prlCmdblk->tw_start);   	
    else
        strcpy (pcgTwStart, prlCmdblk->tw_start);
        

    if (strstr(pcgTwStart,".NBC.") != NULL)
		igBroadCast = FALSE; 
	else
	    igBroadCast = TRUE; 
	 
	dbg(TRACE, "HandleData:: pcgTwStart[%s] Broadcast Status[%d]", pcgTwStart, igBroadCast);   
	    
                    
    if ( iParamFieldNamesCnt!=iParamFieldValuesCnt )
        dbg(TRACE, "HandleData:: CMD[%s]: Parameter Fields[%s][%d] & Parameter Values[%s][%d] not equal", 
            prlCmdblk->command,pclFields, iParamFieldNamesCnt, pclData, iParamFieldValuesCnt);        	
    else
    {        
        dbg(TRACE, "Commands [%s]\n",prlCmdblk->command );   
        if ( !strcmp(prlCmdblk->command, "ERP") )
        {    	    
            /* MEI */
            //(void) new_tools_send_sql(igQueOut,prlBchd,prlCmdblk, "URNO", "1");  

			for (a=0; a<iParamFieldNamesCnt; a++)
	        {
	            if ( !strcmp("AURN", strParamFieldNames[a]) )
                    iArrivalLocation=a;   		
                else if ( !strcmp("DURN", strParamFieldNames[a]) )
                    iDepartureLocation=a;  	
	        }
	    
	        if ( iArrivalLocation==-1 )
	        {
                dbg(TRACE,"HandleData:: ERP: Parameter Field [AURN] missing");	    
            }
	        else if ( iDepartureLocation==-1 )
	        {
                dbg(TRACE,"HandleData:: ERP: Parameter Field [DURN] missing");	    
            }
	        else
	        {
	        	dbg(TRACE, "HandleData:: ERP Parameter(s): Arrival FLNU[%s] Departure FLNU[%s]",
	        	    strParamFieldValues[iArrivalLocation],strParamFieldValues[iDepartureLocation]);  
	        	    
	            ilRC=processERP(strParamFieldValues[iArrivalLocation],strParamFieldValues[iDepartureLocation]);
	        }
        }
        else if ( !strcmp(prlCmdblk->command, "DFR") )
        {
	        for (a=0; a<iParamFieldNamesCnt; a++)
	        {
	            if ( !strcmp("URNO", strParamFieldNames[a]) )
	            {
	    	        iFlnuLocation=a;
	                break;     	
	            }		
	        }
	    
	        if ( iFlnuLocation==-1 )
	            dbg(TRACE,"HandleData:: DFR: Parameter Field [FLNU] missing");	    
            else
            {
                dbg(TRACE, "HandleData:: DFR Parameter(s): FLNU[%s]",
	        	    strParamFieldValues[iFlnuLocation]);  
	            ilRC=processDFR(strParamFieldValues[iFlnuLocation]);        	
	        }
        }
        else if ( 
    	            !strcmp(prlCmdblk->command, "IFR") ||
    	            !strcmp(prlCmdblk->command, "UFR") 
    	        )
	    {
	       
	        for (a=0; a<iParamFieldNamesCnt; a++)
	        {
	            if ( !strcmp("URNO", strParamFieldNames[a]) )
	            {
	    	        iFlnuLocation=a;
	                break;     	
	            }		
	        }
	    
	        if ( iFlnuLocation==-1 )
	            dbg(TRACE,"HandleData:: IFRUFR: Parameter Field [URNO] missing");	    
            else
            {
                dbg(TRACE, "HandleData:: IFR/UFR Parameter(s): URNO[%s]",
	        	    strParamFieldValues[iFlnuLocation]);  
	            ilRC=processIFRUFR(strParamFieldValues[iFlnuLocation]);    		
	        }
	    }
	    else if ( !strcmp(prlCmdblk->command, "LOA") ) 
	    {
	        for (a=0; a<iParamFieldNamesCnt; a++)
	        {
	            if ( !strcmp("FLNU", strParamFieldNames[a]) )
	            {
	    	        iFlnuLocation=a;
	                break;     	
	            }		
	        }
	    
	        if ( iFlnuLocation==-1 )
	            dbg(TRACE,"HandleData:: LOA: Parameter Field [FLNU] missing");	    
            else
            {
                dbg(TRACE, "HandleData:: LOA Parameter(s): FLNU[%s]",
	        	    strParamFieldValues[iFlnuLocation]);  
	            ilRC=processLOA(strParamFieldValues[iFlnuLocation]);    
	        }		
	    }
        else if ( !strcmp(prlCmdblk->command, "CFM") )
	    {
	        for (a=0; a<iParamFieldNamesCnt; a++)
	        {
	            if ( !strcmp("FLNU", strParamFieldNames[a]) )
	    	        iFlnuLocation=a;
                else if ( !strcmp("DQCS", strParamFieldNames[a]) )
                    iDQCSLocation=a;
                else if ( !strcmp("TYPE", strParamFieldNames[a]) )
                    iTypeLocation=a;   		
                else if ( !strcmp("AURN", strParamFieldNames[a]) )
                    iArrivalLocation=a;   		
                else if ( !strcmp("DURN", strParamFieldNames[a]) )
                    iDepartureLocation=a;   		
	        }
	    
	        if ( iDQCSLocation==-1 )
	            dbg(TRACE, "HandleData:: CFM: Parameter Field [DQCS] missing");   
	        else if ( iTypeLocation==-1 )
	            dbg(TRACE, "HandleData:: CFM: Parameter Field [TYPE] missing");     
	        else if ( 
	        	      iFlnuLocation==-1 &&
	        	      (iArrivalLocation==-1 || iDepartureLocation==-1)
	        	    ) 
	        	    dbg(TRACE, "HandleData:: CFM: Flight number(s) missing");   
	        	    
	        else
            {
                memset(strFlnu, 0, sizeof(strFlnu));
	            memset(strArrivalFlnu, 0, sizeof(strArrivalFlnu));
	            memset(strDepartureFlnu, 0, sizeof(strDepartureFlnu));
	        
	            if ( iFlnuLocation!=-1 )
	                strcpy(strFlnu, strParamFieldValues[iFlnuLocation]);
	            else
	            {
	                strcpy(strArrivalFlnu, strParamFieldValues[iArrivalLocation]);
	                strcpy(strDepartureFlnu, strParamFieldValues[iDepartureLocation]);
	            }
	                
            	dbg(TRACE, "HandleData:: CFM Parameter(s): FLNU[%s], ARRFLNU[%s] <==> DEPFLNU[%s], DQCS[%s] TYPE[%s]",
            	    strFlnu, strArrivalFlnu, strDepartureFlnu, strParamFieldValues[iDQCSLocation], strParamFieldValues[iTypeLocation]);
 
            
                if ( 
                      !strcmp(strParamFieldValues[iTypeLocation], "MOVE") ||
                      !strcmp(strParamFieldValues[iTypeLocation], "LOAD")
                    )
                {   
                    if ( !strcmp(strParamFieldValues[iDQCSLocation], "C") )
                    {
                        ilRC=processCFM(strFlnu, strArrivalFlnu, strDepartureFlnu, strParamFieldValues[iTypeLocation]);    
	                }
	                else if ( 
	            	          !strcmp(strParamFieldValues[iDQCSLocation], "")  ||
	            	          !strcmp(strParamFieldValues[iDQCSLocation], " ")  
	            	        )
	            	{
                        ilRC=processUNCFM(strFlnu, strArrivalFlnu, strDepartureFlnu, strParamFieldValues[iTypeLocation]);  
 	                }
 	                else
                        dbg(TRACE, "HandleData:: Invalid DQCS [C/ ] only");                
 	            }
 	            else
 	                dbg(TRACE, "HandleData:: Invalid Parameter Type [MOVE/LOAD] only");
 	        }		
	    }
        else if ( !strcmp(prlCmdblk->command, "DQC") )
        {
	        for (a=0; a<iParamFieldNamesCnt; a++)
	        {
	            if ( !strcmp("FLNU", strParamFieldNames[a]) )
	            {
	    	       iFlnuLocation=a;
	               break;     	
	            }		
	        }
	    
	        if ( iFlnuLocation==-1 )
	        {
                dbg(TRACE,"HandleData:: DQC: Parameter Field [FLNU] missing");	    
            }
	        else
	        {
	        	dbg(TRACE, "HandleData:: DQC Parameter(s): FLNU[%s]",
	        	    strParamFieldValues[iFlnuLocation]);  
	            ilRC=processDQC(strParamFieldValues[iFlnuLocation]);
	        }        	
        }
        else if (  !strncmp(prlCmdblk->command, "TIM", 3)   )
        {
            ilRC=processTIM(prlCmdblk->command);
        }
	    else
	    	dbg(TRACE, "HandleData:: Invalid CMD[%s]", prlCmdblk->command);
	    	
        freeStringArray(strParamFieldNames, iParamFieldNamesCnt);	
        freeStringArray(strParamFieldValues, iParamFieldValuesCnt);	
    }
         
    return ilRC;    
}

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
    int    ilRC = RC_SUCCESS;
    return;
}

int doCheck(char *strFlnu, char *strCmd, char *strType, char *pStrDQCS, int iPair, char *strNewDQCS)
{
    int iRet=RC_SUCCESS;
    int iMove=RC_SUCCESS;
    int iMoveFlag=0;
    int iLoad=RC_SUCCESS;
    int iLoadFlag=0;
    int iIndexDQCS;
    int iIndexDes3;
    int iIndexOrg3;
    int iIndexRkey;
    int iIndexFtyp;
    int iIndexTifa;
    int iIndexTifd;
    char strDQCSMove[3];
    char strOldDQCSMove[3];
    char strDQCSLoad[3];    
    char strOldDQCSLoad[3];    
    char strOldDQCS[5];
    char strDQCS[5];
    char strTmp[257];
    char strSql[257];
    DBSELECT stAfttabTmp;
    int iTmp;
    char strDeleteDQCTABType[5]="";
    char strCmdWithComma[11];
    int ilStaRst = 0;
    int ilChkDif = 0;
    int ilBchFlg = TRUE;
    int ilAction = TRUE;
    char pclLmtTim[32] = "\0";
    double dlDiff=0;
    int a=0;
    int ilMovePlausibilityFlag=RC_SUCCESS;
    int ilMoveCompletenessFlag=RC_SUCCESS;
    int ilLoadPlausibilityFlag=RC_SUCCESS;
    int ilLoadCompletenessFlag=RC_SUCCESS;
    
    dbg(DEBUG, "doCheck:: strFlnu[%s] strCmd[%s] strType[%s] pStrDQCS[%s]", 
       strFlnu, strCmd, strType, pStrDQCS);

    sprintf(strCmdWithComma, ",%s,", strCmd);
               
    memset(&strDQCS, 0, sizeof(strDQCS));
    memset(&strOldDQCS, 0, sizeof(strOldDQCS));
    memset(&strDQCSMove, 0, sizeof(strDQCSMove));
    memset(&strOldDQCSMove, 0, sizeof(strOldDQCSMove));
    memset(&strDQCSLoad, 0, sizeof(strDQCSLoad));
    memset(&strOldDQCSLoad, 0, sizeof(strOldDQCSLoad));
        
    memset(sDqctab, 0, sizeof(sDqctab));
    iDqctabCount=0;
        
    memset(&gsDqctab, 0, sizeof(DQCTAB));
    strcpy(gsDqctab.strFlnu, strFlnu);
    gsDqctab.cPass='F';
        
    memset(&strArrAfttabError, 0, sizeof(strArrAfttabError));
    iArrAfttabErrorCount=0;
    iMove=iLoad=iRet=RC_SUCCESS;
        
    strArrAfttabFieldValues=getAfttabRecord(strFlnu, &iRet);
    if ( iRet==RC_FAIL )
        dbg(TRACE, "doCheck:: ERROR!!! DB Error/Record not found in afttab");
    else
    {
        for (a=0; a<rgDQC.iFieldCheck; a++)
        {
            dbg(TRACE, "doCheck:: [%d/%d] CondCheck Name[%s]: Field[%s] Op[%s] Value[%s]", 
                a, rgDQC.iFieldCheck,
                rgDQC.sFieldCheck[a].strName, 
                rgDQC.sFieldCheck[a].strField, 
                rgDQC.sFieldCheck[a].strOp, 
                rgDQC.sFieldCheck[a].strValue);	
    
            iTmp=doFieldCheck(&rgDQC.sFieldCheck[a]);
            if ( iTmp==RC_FAIL )
                break;
        }
    
        if ( iTmp==RC_FAIL )
        {
            dbg(TRACE, "doCheck:: Skip DQC Check");	
            
            iIndexDQCS=getArrTabFieldIndex("DQCS", arrAfttabDBFieldsConfig, iArrAfttabDBFieldsConfigCount);
            if ( iIndexDQCS==-1 )
          	{
          	    dbg(TRACE, "doCheck:: ERROR!!! Field DQCS not define.");
          	    iRet=RC_FAIL;
          	}
          	else 
          	{
          		dbg(TRACE, "doCheck:: DQCS[%s]", strArrAfttabFieldValues[iIndexDQCS]);
          	    if ( 
          		     !strcmp(strArrAfttabFieldValues[iIndexDQCS], " ") ||
          		     !strcmp(strArrAfttabFieldValues[iIndexDQCS], "")
          	       )
          	    {
          		    dbg(TRACE, "doCheck:: Skipping Clean up");
          	    }
          	    else
          	    {
          		    dbg(TRACE, "doCheck:: do Cleanup");
          		    iRet=doCleanUp(strFlnu, strCmd);
          	    }
            }
        }
        else
        {
            dbg(TRACE, "doCheck:: Continue");	
      
            iIndexDQCS=getArrTabFieldIndex("DQCS", arrAfttabDBFieldsConfig, iArrAfttabDBFieldsConfigCount);
            iIndexOrg3=getArrTabFieldIndex("ORG3", arrAfttabDBFieldsConfig, iArrAfttabDBFieldsConfigCount); 
            iIndexDes3=getArrTabFieldIndex("DES3", arrAfttabDBFieldsConfig, iArrAfttabDBFieldsConfigCount);  
            iIndexRkey=getArrTabFieldIndex("RKEY", arrAfttabDBFieldsConfig, iArrAfttabDBFieldsConfigCount);  
            iIndexFtyp=getArrTabFieldIndex("FTYP", arrAfttabDBFieldsConfig, iArrAfttabDBFieldsConfigCount);  
            iIndexTifa=getArrTabFieldIndex("TIFA", arrAfttabDBFieldsConfig, iArrAfttabDBFieldsConfigCount);  
            iIndexTifd=getArrTabFieldIndex("TIFD", arrAfttabDBFieldsConfig, iArrAfttabDBFieldsConfigCount);  
          	if ( iIndexDQCS==-1 )
          	{
          	    dbg(TRACE, "doCheck:: ERROR!!! Field DQCS not define.");
          	    iRet=RC_FAIL;
          	}
            else if ( iIndexOrg3==-1 )
          	{
          	    dbg(TRACE, "doCheck:: ERROR!!! Field ORG3 not define.");
          	    iRet=RC_FAIL;
            }
            else if ( iIndexDes3==-1 )
          	{
          	    dbg(TRACE, "doCheck:: ERROR!!! Field DES3 not define.");
          	    iRet=RC_FAIL;
            }      	
            else if ( iIndexRkey==-1 )
          	{
          	    dbg(TRACE, "doCheck:: ERROR!!! Field RKEY not define.");
          	    iRet=RC_FAIL;
            }         
            else if ( iIndexFtyp==-1 )
          	{
          	    dbg(TRACE, "doCheck:: ERROR!!! Field FTYP not define.");
          	    iRet=RC_FAIL;
            }      	
            else
          	{	
          	    int ilSkip=0;
          	    
          		dbg(TRACE, "doCheck:: Flight[%s], DQCS[%s], ORG3[%s], DES3[%s] RKEY[%s] FTYP[%s] TIFA[%s], TIFD[%s], DQC Implementation Date[%s], Addl Min[%d]", 
          		    strFlnu, strArrAfttabFieldValues[iIndexDQCS], strArrAfttabFieldValues[iIndexOrg3], strArrAfttabFieldValues[iIndexDes3], 
          		    strArrAfttabFieldValues[iIndexRkey], strArrAfttabFieldValues[iIndexFtyp],
          		    strArrAfttabFieldValues[iIndexTifa], strArrAfttabFieldValues[iIndexTifd], pcgImplementationDate, (igAdditionalMinutes*60));
          		    
                if ( !strcmp(strArrAfttabFieldValues[iIndexDes3], cgHopo) )
                {
                	if ( strcmp(pcgImplementationDate, strArrAfttabFieldValues[iIndexTifa]) > 0 )
                	    ilSkip=1;
                	else
          		        timeDiff(strArrAfttabFieldValues[iIndexTifa], "", &dlDiff);
          		}
          	    else
          	    {
                   	if ( strcmp(pcgImplementationDate, strArrAfttabFieldValues[iIndexTifd]) > 0 )
                	    ilSkip=1;
                	else
   	    	            timeDiff(strArrAfttabFieldValues[iIndexTifd], "", &dlDiff);
          		}
          		
   	    	    if ( (dlDiff)>(igAdditionalMinutes*60) )
   	    	        ilSkip=1;    

          	    if ( ilSkip )
          	    {
          	        dbg(TRACE, "doCheck:: Skipping...  Invalid Flight Date (either too advance or too late)! ");	
          	        if ( 
          		         !strcmp(strArrAfttabFieldValues[iIndexDQCS], " ") ||
          		         !strcmp(strArrAfttabFieldValues[iIndexDQCS], "")
          	           )
          	        {
          		        dbg(TRACE, "doCheck:: Skipping Clean up");
          	        }
          	        else
          	        {
          		        dbg(TRACE, "doCheck:: do Cleanup");
          		        iRet=doCleanUp(strFlnu, strCmd);
          	        }
          	        
          	        if ( igQueOut>=FRONTEND_QUEUE )
          	            (void) new_tools_send_sql(igQueOut,prlBchd,prlCmdblk, "STATUS", "Error!!! Invalid Flight Date (either too advance or too late)");  
          	    }
          	    else
          	    {
          	        strcpy(strOldDQCS, strArrAfttabFieldValues[iIndexDQCS]);  
              	    
          	        if ( 
          	        	  iPair==0 && 
          	              !strcmp(strCmd, "CFM") && 
          	              !strcmp(pStrDQCS, "C") &&
          	              strcmp(strArrAfttabFieldValues[iIndexDQCS], "C") &&
          	              strcmp(strArrAfttabFieldValues[iIndexDQCS], "S") &&
          	              !strcmp(strType,"MOVE")     	              
          	            )
          	       
          	        {
                        if ( !strcmp(strArrAfttabFieldValues[iIndexOrg3], cgHopo) )
                        {
                            dbg(TRACE, "doCheck::Departure Flight: Check Arrival Flight ");	
                            
                            sprintf(strSql, "SELECT urno, dqcs, des3, org3, rkey "
                                            "FROM afttab "
                                            "WHERE urno='%s' AND adid='A' ",
                                            strArrAfttabFieldValues[iIndexRkey]);
                                            
                            memset(&stAfttabTmp, 0, sizeof(DBSELECT));                
                            iTmp=populateDBRows(strSql, "URNO,DQCS,DES3,ORG3,RKEY", 5, &stAfttabTmp);   
                            if ( stAfttabTmp.iRow>0 )
                            {
                                dbg(TRACE, "doCheck:: Found Arrival Record: Urno[%s], DQCS[%s], DES3[%s], ORG3[%s], RKEY[%s]",
                                    stAfttabTmp.sDBRows[0].sDBFields[0].strValue,	
                                    stAfttabTmp.sDBRows[0].sDBFields[1].strValue,	
                                    stAfttabTmp.sDBRows[0].sDBFields[2].strValue,	
                                    stAfttabTmp.sDBRows[0].sDBFields[3].strValue,	
                                    stAfttabTmp.sDBRows[0].sDBFields[4].strValue);	
                                
                                if ( 
                                	   !strcmp(stAfttabTmp.sDBRows[0].sDBFields[1].strValue, "C") ||
                                	   !strcmp(stAfttabTmp.sDBRows[0].sDBFields[1].strValue, "S") ||
                                	   ( !strcmp(strType, "MOVE") && strstr(stAfttabTmp.sDBRows[0].sDBFields[1].strValue,"MC") ) 
                                   )
                                    ;
                                else
                                {
                                    iRet=RC_FAIL;
                           	        if ( igQueOut>=FRONTEND_QUEUE )
                                    {
                                        dbg(TRACE,"doCheck:: Sending to Client igQueOut[%d], prlBchd[%s], prlCmdblk[%s], Field[STATUS] Value[%s]",
                                            igQueOut, prlBchd, prlCmdblk, "Error!!! Arrival Flight is NOT confirm/sent");
                                        (void) new_tools_send_sql(igQueOut,prlBchd,prlCmdblk, "STATUS", "Error!!! Arrival Flight is NOT confirm/sent");                                       
                                    }
                                }
                            } 
                            else
                            {
                                dbg(TRACE,"doCheck::No Arrival Flight (paired)");	
                            }
                            clearDBROWS(&stAfttabTmp);       
                        }	
                        else
                        {
                            dbg(TRACE,"doCheck:: Departure Flight[%s][%s]: No need to get Departure",
                                strArrAfttabFieldValues[iIndexDes3], cgHopo);	                	
                        }   	
          	        }
          	        else if ( 
          	        	      iPair==0 &&
          	        	      !strcmp(strCmd, "CFM") && 
          	        	      !strcmp(pStrDQCS, " ") &&
          	        	      (
          	        	         strcmp(strArrAfttabFieldValues[iIndexDQCS], "C") ||
          	                     strcmp(strArrAfttabFieldValues[iIndexDQCS], "S")    
          	        	      ) &&
          	        	      !strcmp(strType,"MOVE")  
          	        	    )
          	        {                        	    	
                        if ( !strcmp(strArrAfttabFieldValues[iIndexDes3], cgHopo) )
                        {
                            dbg(TRACE, "doCheck::Arrival Flight: Check Departure Flight ");	
                            
                            sprintf(strSql, "SELECT urno, dqcs, des3, org3, rkey "
                                            "FROM afttab "
                                            "WHERE urno<>'%s' and rkey='%s' AND adid='D' ",
                                            strFlnu, strFlnu);
                                            
                            memset(&stAfttabTmp, 0, sizeof(DBSELECT));                
                            iTmp=populateDBRows(strSql, "URNO,DQCS,DES3,ORG3,RKEY", 5, &stAfttabTmp);   
                            if ( stAfttabTmp.iRow>0 )
                            {
                                dbg(TRACE, "doCheck:: Found Departure Record: Urno[%s], DQCS[%s], DES3[%s], ORG3[%s], RKEY[%s]",
                                    stAfttabTmp.sDBRows[0].sDBFields[0].strValue,	
                                    stAfttabTmp.sDBRows[0].sDBFields[1].strValue,	
                                    stAfttabTmp.sDBRows[0].sDBFields[2].strValue,	
                                    stAfttabTmp.sDBRows[0].sDBFields[3].strValue,	
                                    stAfttabTmp.sDBRows[0].sDBFields[4].strValue);	
                                
                                if ( 
                                	   !strcmp(stAfttabTmp.sDBRows[0].sDBFields[1].strValue, "C") ||
                                	   !strcmp(stAfttabTmp.sDBRows[0].sDBFields[1].strValue, "S") ||
                                	   ( !strcmp(strType, "MOVE") && strstr(stAfttabTmp.sDBRows[0].sDBFields[1].strValue, "MC") ) 
                                   )
                                {
                                    iRet=RC_FAIL;
                           	        if ( igQueOut>=FRONTEND_QUEUE )
                                    {
                                        dbg(TRACE,"doCheck:: Sending to Client igQueOut[%d], prlBchd[%s], prlCmdblk[%s], Field[STATUS] Value[%s]",
                                            igQueOut, prlBchd, prlCmdblk, "Error!!! Departure Flight is confirm/sent");
                                        (void) new_tools_send_sql(igQueOut,prlBchd,prlCmdblk, "STATUS", "Error!!! Departure Flight is confirm/sent");                                       
                                    }
                                }
                            } 
                            else
                            {
                                dbg(TRACE,"doCheck:: No Departure Flight (paired)");	
                            }
                            clearDBROWS(&stAfttabTmp);       
                        }	
                        else
                        {
                            dbg(TRACE,"doCheck:: Departure Flight[%s][%s]: No need to get departure",
                                strArrAfttabFieldValues[iIndexDes3], cgHopo);	                	
                        }
          	        }
                   
          	        if ( iRet==RC_SUCCESS )
          	        {
                        if ( !strcmp(strArrAfttabFieldValues[iIndexDQCS], "S")  )
                        {
                            dbg(TRACE, "doCheck:: Do Nothing [S]");	
                        }
                        else 
                        {
                            if ( !strcmp(strArrAfttabFieldValues[iIndexDQCS], "") )
                            {
                                strcpy(strOldDQCSMove, "MF");
                        	    strcpy(strOldDQCSLoad, "LF");
                        	    
                        	    dbg(TRACE, "doCheck:: Do Move & Load Check");

                                iMove=doMoveCheck(&ilMoveCompletenessFlag, &ilMovePlausibilityFlag); 
                                iMoveFlag=1;
                                    
                                iLoad=doLoadCheck(strFlnu, &ilLoadCompletenessFlag, &ilLoadPlausibilityFlag);
                                iLoadFlag=1;     	    	
                            }
                            else 
                            {
                                if ( !strcmp(strArrAfttabFieldValues[iIndexDQCS], "F") )
                                {
                                    strcpy(strOldDQCSMove, "MF");	
                                    strcpy(strOldDQCSLoad, "LF");	
                                }
                                else if ( !strcmp(strArrAfttabFieldValues[iIndexDQCS], "C") )
                                {
                                    strcpy(strOldDQCSMove, "MC");	
                                    strcpy(strOldDQCSLoad, "LC");	
                                }
                                else if ( !strcmp(strArrAfttabFieldValues[iIndexDQCS], "P") )
                                {
                                    strcpy(strOldDQCSMove, "MP");	
                                    strcpy(strOldDQCSLoad, "LP");	
                                }
                                else if ( strlen(strArrAfttabFieldValues[iIndexDQCS])==4 )
                                {
                                    sprintf(strOldDQCSLoad, "%2.2s", strArrAfttabFieldValues[iIndexDQCS]);	
                                    sprintf(strOldDQCSMove, "%2.2s", &strArrAfttabFieldValues[iIndexDQCS][2]);
                                }  
                                else
                                {
                                    dbg(TRACE, "doCheck:: Invalid DQCS");
                                    iRet=RC_FAIL;	
                                }
                        
                                if ( iRet==RC_SUCCESS )
                                { 
                                    if ( 
                        	             !strcmp(strCmd, "DQC") ||
                                         (
                                           !strcmp(strCmd, "CFM") &&
                                           !strcmp(strType, "MOVE") 
                                         ) || 
                                         (
                                            (
                                                // !strcmp(strCmd,"ERP") ||
                                                //!strcmp(strCmd,"IFR") ||
                                                //!strcmp(strCmd,"UFR") ||
                                                //!strncmp(strCmd, "TIM", 3)
                                                strstr(pcgMoveCmd, strCmdWithComma)!=NULL
                                            ) &&
                                            strcmp(strOldDQCSMove, "MC") 
                                         )   
                                       )
                                    {    	
                                    	dbg(TRACE, "doCheck:: Do Move Check");
                                    	if ( !strcmp(strDeleteDQCTABType, "") )
                                    	    strcpy(strDeleteDQCTABType, "MOVE");
                                    	else
                                    		strcpy(strDeleteDQCTABType, "");

                                        iMove=doMoveCheck(&ilMoveCompletenessFlag, &ilMovePlausibilityFlag);
                                        iMoveFlag=1; 
                                    }
                                }
                        
                                if ( iRet==RC_SUCCESS )
                                {   
                                    if ( 
                                          !strcmp(strCmd, "DQC") ||
                                          (
                                             !strcmp(strCmd, "CFM") &&
                                             !strcmp(strType, "LOAD")
                                          ) || 
                                          (
                                             (                                         
                                                 //!strcmp(strCmd, "ERP") ||
                                                 //!strcmp(strCmd, "LOA") ||
                                                 //!strncmp(strCmd, "TIM", 3)
                                                 strstr(pcgLoadCmd, strCmdWithComma)!=NULL
                                             ) &&
                                             strcmp(strOldDQCSLoad, "LC")
                                          )      		                
                                       )
                                    {
                             	        dbg(TRACE, "doCheck:: Do Load Check");  
                             	        if ( !strcmp(strDeleteDQCTABType, "") )  	
                             	            strcpy(strDeleteDQCTABType, "LOAD");	
                             	        else
                             	            strcpy(strDeleteDQCTABType, "");		    	        
                         	            iLoad=doLoadCheck(strFlnu, &ilLoadCompletenessFlag, &ilLoadPlausibilityFlag);
                                        iLoadFlag=1;
                                    }    	            
                                } 	        
                            }    
                            
                            if ( iRet==RC_SUCCESS )
                            {
                            	dbg(TRACE, "doCheck:: iMoveFlag[%d] iMove[%d], ilMoveCompletenessFlag[%d]",
                            	    iMoveFlag, iMove, ilMoveCompletenessFlag);
                                dbg(TRACE, "doCheck:: iLoadFlag[%d] iLoad[%d], ilLoadCompletenessFlag[%d]", 
                                    iLoadFlag, iLoad, ilLoadCompletenessFlag);
                                if ( iMoveFlag )
                                {
                                    if ( iMove==RC_SUCCESS )
                                    {
                                        if ( !strcmp(strCmd, "CFM") && !strcmp(strType, "MOVE") && !strcmp(pStrDQCS,"C") )
                                        {
                                        	if ( ilMoveCompletenessFlag==RC_SUCCESS )
                                                strcpy(strDQCSMove, "MC");
                                            else
                                                strcpy(strDQCSMove, "MF");
                                        }
                                        else
                                            strcpy(strDQCSMove, "MP");
                                    }
                                    else
                                    {
                                        if ( !strcmp(strCmd, "CFM") && !strcmp(strType, "MOVE") && !strcmp(pStrDQCS,"C") )
                                        {
                                        	if ( ilMoveCompletenessFlag==RC_SUCCESS )
                                                strcpy(strDQCSMove, "MC");
                                            else
                                                strcpy(strDQCSMove, "MF");
                                        }
                                        else
                                            strcpy(strDQCSMove, "MF");
                                    }
                                }
                                else
                                    strcpy(strDQCSMove, strOldDQCSMove);
                                   
                                if ( iLoadFlag )
                                {
                                    if ( iLoad==1 )
                                   	{
                                   	    strcpy(strDQCSLoad, "");
                                   	}
                                    else if ( iLoad==RC_SUCCESS )
                                    {
                                        if ( !strcmp(strCmd, "CFM") && !strcmp(strType, "LOAD") && !strcmp(pStrDQCS,"C") )
                                        {
                                        	if ( ilLoadCompletenessFlag==RC_SUCCESS )
                                                strcpy(strDQCSLoad, "LC");
                                            else
                                                strcpy(strDQCSLoad, "LF");
                                        }
                                        else
                                            strcpy(strDQCSLoad, "LP");
                                    }
                                    else
                                    {
                                        if ( !strcmp(strCmd, "CFM") && !strcmp(strType, "LOAD") && !strcmp(pStrDQCS,"C") )
                                        {
                                        	if ( ilLoadCompletenessFlag==RC_SUCCESS )
                                                strcpy(strDQCSLoad, "LC");
                                            else
                                                strcpy(strDQCSLoad, "LF");
                                        }
                                        else
                                            strcpy(strDQCSLoad, "LF");
                                    }
                                }
                                else
                                    strcpy(strDQCSLoad, strOldDQCSLoad);
                                       
                                if ( !strcmp(strDQCSLoad, "") )
                                    strcpy(strDQCS, "");
                                else if ( strDQCSMove[1]==strDQCSLoad[1] )
                                    strDQCS[0]=strDQCSMove[1];
                                else
                                    sprintf(strDQCS, "%s%s", strDQCSLoad, strDQCSMove);   
                            
                                printAfttabErrors();                    
                                      
                                if ( strcmp(strCmd, "DQC") /*&& iLoad!=1 */  )
                                {
                                	int iFlag=1;
                                	
                                    ilStaRst = DQCStaChk(strOldDQCS, strDQCS);
                                    dbg(TRACE, "doCheck:: ilStaRst[%d], ilAction[%d], ilBchFlg[%d]", ilStaRst, ilAction, ilBchFlg);
                                    if (ilStaRst > 0)
                                    {
                                        ilAction = TRUE;
                                        GetServerTimeStamp ("UTC", 1, (long)igTimGapSec, pclLmtTim);
                                        dbg(DEBUG, "Check TIME LmTim<%s>, TIFA<%s>, TIFD<%s> ",pclLmtTim, strArrAfttabFieldValues[iIndexTifa], strArrAfttabFieldValues[iIndexTifd]); 
                                        if ((strcmp(strArrAfttabFieldValues[iIndexTifa],pclLmtTim) > 0) &&
                                           (strcmp(strArrAfttabFieldValues[iIndexTifd],pclLmtTim) > 0) )
                                            ilBchFlg = FALSE;
                                        else
                                            ilBchFlg = TRUE;
                                    }
                                    else
                                        ilAction = FALSE;
        
                                    dbg(TRACE, "doCheck:: 2nd Check ilStaRst[%d], ilAction[%d], ilBchFlg[%d]", ilStaRst, ilAction, ilBchFlg);
                                    if ((ilStaRst == 2) && (ilBchFlg == TRUE) && (igDqcRead == TRUE))
                                    {
                                        (void) ReadDQCTab(strFlnu, strDeleteDQCTABType);
                                        ilChkDif = DQCTabChgChk();
                                        if (ilChkDif == FALSE)
                                        {
                                            ilAction = FALSE;
                                            ilBchFlg = FALSE;
                                        }
                                    }
                                    dbg(TRACE, "doCheck:: Check Result StaRst<%d>, ChkDif<%d>, ActFlg<%d>, BchFlg<%d> ", 
                                        ilStaRst, ilChkDif, ilAction, ilBchFlg);
                                    if (ilAction == TRUE)
                                    {
                                	    iRet=deleteDQCTab(strFlnu, strDeleteDQCTABType);	
                        	            iRet=insertDQCTab(strCmd, ilBchFlg);
                                    }
                        	    }
                        	    else
                        	        dbg(TRACE, "doCheck:: Command [%s] No insertDQCTab operations involve or iLoad[%d]=1", strCmd, iLoad);    	    	        
                        	                            
                                if ( iRet==RC_FAIL )
                                    dbg(TRACE, "doCheck:: ERROR!!! insertDQCTab()");
                                else
                                {    	                
                                    
                                    if ( !strcmp(strCmd, "DQC") )
                                        dbg(TRACE, "doCheck:: Command [%s] No updateFlightDQCS operations involve", strCmd);
                                    else if ( !strcmp(strCmd, "ERP") )
                                        dbg(TRACE, "doCheck:: Command [%s] No updateFlightDQCS operations involve", strCmd);
                                    else
                                    {
                                    	if ( strcmp(strOldDQCS,strDQCS) )
                        	                iRet=updateFlightDQCS(strDQCS, strFlnu, strCmd);
                        	        }
                        	            
                                    if ( iRet==RC_FAIL )
                                        dbg(TRACE, "doCheck:: Error in updateFlightDQCS()");	
                                }                
                            }    
                            
                            if ( iRet==RC_SUCCESS )
                            {
                                if ( !strcmp(strCmd, "ERP") && !strcmp(strOldDQCS, "C") )
                                    iRet=RC_SUCCESS;
                                else if ( iMove==-1 || iLoad==-1 )
                                    iRet=RC_FAIL;                          
                                       
                                if ( strcmp(strCmd, "DQC") && !strcmp(sMoveConfig.strAfttabDQCSUpdate, "YES") )
                                {
                                    if ( !strcmp(strCmd, "CFM") && iPair==0 )
                                    {
                                    	if  ( !strcmp(pStrDQCS, "C") )
                                    	{
                                            if ( ilMoveCompletenessFlag==RC_SUCCESS && ilLoadCompletenessFlag==RC_SUCCESS )
                                            {	
                           	          	        if ( igQueOut>=FRONTEND_QUEUE )
                                                {
                                       	            dbg(TRACE,"doCheck:: CFM: Sending to Client igQueOut[%d], prlBchd[%s], prlCmdblk[%s], Field[DQCS] Value[%s]",
                                       	                igQueOut, prlBchd, prlCmdblk, strDQCS);
                                                    (void) new_tools_send_sql(igQueOut,prlBchd,prlCmdblk, "DQCS", strDQCS);                          
                                                }
                                            }
                                            else 
                                            {
                                            	if ( ilMoveCompletenessFlag==RC_FAIL )
                                            	{
                           		          	        if ( igQueOut>=FRONTEND_QUEUE )
                                                    {
                                                        dbg(TRACE,"doCheck:: CFM: Sending MoveCompleteness Fail To Client igQueOut[%d], prlBchd[%s], prlCmdblk[%s], Field[STATUS] Value[%s]",
                                                            igQueOut, prlBchd, prlCmdblk, "Error!!! Movement completeness failed");
                                                        (void) new_tools_send_sql(igQueOut,prlBchd,prlCmdblk, "STATUS", "Error!!! Movement completeness failed"); 
                                                    }
                                                }
                                                else
                                                {
                              	          	        if ( igQueOut>=FRONTEND_QUEUE )
                                                    {
                                                        dbg(TRACE,"doCheck:: CFM: Sending LoadCompleteness Fail To Client igQueOut[%d], prlBchd[%s], prlCmdblk[%s], Field[STATUS] Value[%s]",
                                                            igQueOut, prlBchd, prlCmdblk, "Error!!! Load completeness failed");
                                                        (void) new_tools_send_sql(igQueOut,prlBchd,prlCmdblk, "STATUS", "Error!!! Load completeness failed");                                                 	
                                                    }
                                                }
                                            }                                    		
                                    	}
                                    	else
                                    	{
                   		          	        if ( igQueOut>=FRONTEND_QUEUE )
                                            {
                                       	        dbg(TRACE,"doCheck:: Sending to Client igQueOut[%d], prlBchd[%s], prlCmdblk[%s], Field[DQCS] Value[%s]",
                                       	            igQueOut, prlBchd, prlCmdblk, strDQCS);
                                                (void) new_tools_send_sql(igQueOut,prlBchd,prlCmdblk, "DQCS", strDQCS);  
                                            }
                                        }                        
                                    }
                                }      
                            }
                        }	    
                    }
                }
          	}   
        }
        
        freeStringArray(strArrAfttabFieldValues, iArrAfttabDBFieldsConfigCount);  	
    }
                 
    strcpy(strNewDQCS, strDQCS);
    dbg(DEBUG, "doCheck:: strFlnu[%s] Old DQCS[%s] New DQCS[%s] iMoveFlag[%d] iMove[%d] iLoadFlag[%d] iLoad[%d] Result[%d]", 
        strFlnu, strOldDQCS, strDQCS, iMoveFlag, iMove, iLoadFlag, iLoad, iRet);
    
    return iRet;	
}

int doMoveCheck(int *ipCompleteness, int *ipPlausibility)
{
    int iRet=RC_SUCCESS;
    int iIndexDes3=0;
    int iIndexOrg3=0;
    int iIndexHopo=0;
    int iIndexFtyp=0;
    int iTmp=0;
    int a=0;
    
    dbg(TRACE, "doMoveCheck:: Start");

    *ipCompleteness=RC_SUCCESS;
    *ipPlausibility=RC_SUCCESS;
    
    for (a=0; a<sMoveConfig.iFieldCheck; a++)
    {
        dbg(TRACE, "doMoveCheck:: [%d/%d] CondCheck Name[%s]: Field[%s] Op[%s] Value[%s]", 
            a, sMoveConfig.iFieldCheck,
            sMoveConfig.sFieldCheck[a].strName, 
            sMoveConfig.sFieldCheck[a].strField, 
            sMoveConfig.sFieldCheck[a].strOp, 
            sMoveConfig.sFieldCheck[a].strValue);	
    
        iTmp=doFieldCheck(&sMoveConfig.sFieldCheck[a]);
        if ( iTmp==RC_FAIL )
            break;
    }
    
    if ( iTmp==RC_FAIL )
    {
        dbg(TRACE, "doMoveCheck:: Skipping Movement Check");	
    }
    else if ( iTmp==RC_SUCCESS )
    {
        dbg(TRACE, "doMoveCheck:: Must do move check");	
           
        strcpy(gsDqctab.strType, "MOVE");
    
        iIndexDes3=getArrTabFieldIndex("DES3", arrAfttabDBFieldsConfig, iArrAfttabDBFieldsConfigCount);
        iIndexOrg3=getArrTabFieldIndex("ORG3", arrAfttabDBFieldsConfig, iArrAfttabDBFieldsConfigCount);
        iIndexHopo=getArrTabFieldIndex("HOPO", arrAfttabDBFieldsConfig, iArrAfttabDBFieldsConfigCount);
        iIndexFtyp=getArrTabFieldIndex("FTYP", arrAfttabDBFieldsConfig, iArrAfttabDBFieldsConfigCount);
    
        dbg(TRACE, "doMoveCheck:: Perform ALL_BLANK_CHECK[%s]", sMoveConfig.strAllBlankCheck);

        iRet=doCompletenessCheckGroup(sMoveConfig.strAllBlankCheck, sMoveConfig.arrAllBlankCheck, sMoveConfig.iArrAllBlankCheckCount);
        if ( iRet==RC_FAIL )
            *ipCompleteness=RC_FAIL;

        dbg(TRACE, "doMoveCheck:: After All Blank Check[%s], Running Total for Errors[%d]", sMoveConfig.strAllBlankCheck, iDqctabCount);
        
        
        //  ARR is DES3 = HOPO.
        if ( iIndexDes3!=-1 && iIndexHopo!=-1 )
        {
    	    dbg(TRACE, "doMoveCheck:: Des3[%s] Hopo[%s]",
    	        strArrAfttabFieldValues[iIndexDes3], cgHopo);
    	    if ( !strcmp(strArrAfttabFieldValues[iIndexDes3], cgHopo) )
            {
                dbg(TRACE, "doMoveCheck:: Des3[%s] Hopo[%s] equal.  Perform ARR_BLANK_CHECK[%s]", 
                    strArrAfttabFieldValues[iIndexDes3], cgHopo, sMoveConfig.strArrBlankCheck);

                iRet|=doCompletenessCheckGroup(sMoveConfig.strArrBlankCheck, sMoveConfig.arrArrBlankCheck, sMoveConfig.iArrArrBlankCheckCount);
                if ( iRet==RC_FAIL )
                    *ipCompleteness=RC_FAIL;   

                dbg(TRACE, "doMoveCheck:: After Arrival Blank Check[%s], Running Total for Errors[%d]", sMoveConfig.strArrBlankCheck, iDqctabCount);
            }
            else
                dbg(DEBUG, "doMoveCheck:: No Arrival Blank Check");
        }
           
    
        //  DEP is ORG3 = HOPO.    
        if ( iIndexOrg3!=-1 && iIndexHopo!=-1 )
        {
    	    dbg(TRACE, "doMoveCheck:: Org3[%s] Hopo[%s]",
    	        strArrAfttabFieldValues[iIndexOrg3], cgHopo);    	      
    	    if ( !strcmp(strArrAfttabFieldValues[iIndexOrg3], cgHopo) )
    	    {
    		    dbg(TRACE, "doMoveCheck:: Org3[%s] Hopo[%s] equal.  Perform DEP_BLANK_CHECK[%s]", 
                    strArrAfttabFieldValues[iIndexOrg3], cgHopo, sMoveConfig.strDepBlankCheck);

                iRet|=doCompletenessCheckGroup(sMoveConfig.strDepBlankCheck, sMoveConfig.arrDepBlankCheck, sMoveConfig.iArrDepBlankCheckCount);
                if ( iRet==RC_FAIL )
                    *ipCompleteness=RC_FAIL;  

                dbg(TRACE, "doMoveCheck:: After Departure Blank Check[%s], Running Total for Errors[%d]", sMoveConfig.strDepBlankCheck, iDqctabCount);
            }
            else
                dbg(DEBUG, "doMoveCheck:: No Departure Blank Check");
        }
        
        iTmp=doVCCheck();
        if ( iTmp==RC_FAIL )
            *ipPlausibility=RC_FAIL;
        iRet|=iTmp;
            
        if ( iRet==RC_SUCCESS )
        {
            populateDQCTab(&sDqctab[iDqctabCount], "", gsDqctab.strFlnu, gsDqctab.strType, "PASS", 'P', gsDqctab.strHopo);  
            iArrAfttabErrorCount++;
            iDqctabCount++ ; 	
        }
    }
    else
    {
        dbg(TRACE, "doMoveCheck:: Skip movement check");
    }
    
    dbg(DEBUG, "doMoveCheck:: Result[%d], ipCompleteness[%d], Success[%d], Fail[%d]", 
        iRet, *ipCompleteness, RC_SUCCESS, RC_FAIL);
    return iRet;       	
}

int getLoaIndex(char *strFlnu, char *strType, char *strStyp, char *strSstp, char *strSsst, char *strApc3, char *strDssn)
{
    int iRet=-1;
    int a=0;
    
    dbg(DEBUG, "getLoaIndex:: strFlnu[%s], strType[%s], strSstp[%s], strSsst[%s], strApc3[%s], strDssn[%s]",
        strFlnu, strType, strStyp, strSstp, strSsst, strApc3, strDssn) ;   
    for (a=0; a<iLoaCount; a++)
    {
        if ( 
        	    !strcmp(strFlnu, arrLoa[a].FLNU) &&
        	    !strcmp(strType, arrLoa[a].TYPE) &&
        	    !strcmp(strStyp, arrLoa[a].STYP) &&
        	    !strcmp(strSstp, arrLoa[a].SSTP) &&
        	    !strcmp(strSsst, arrLoa[a].SSST) &&
        	    !strcmp(strApc3, arrLoa[a].APC3) &&
        	    !strcmp(strDssn, arrLoa[a].DSSN)         	    
            )
        {
            iRet=a;
            break;	
        }        	
    }    
    dbg(DEBUG, "getLoaIndex:: strFlnu[%s], strType[%s], strStyp[%s], strSstp[%s], strSsst[%s], strApc3[%s], strDssn[%s] Result[%d]",
        strFlnu, strType, strStyp, strSstp, strSsst, strApc3, strDssn, iRet) ;       
    return iRet;	
}

int getLoaRecords(char *strFlnu)
{
	int iRet=RC_SUCCESS;
	int iTmp;
	char strSql[2048];
	char pclSqlData[32768];
    short slCursor = 0;
    char strFields[]="flnu,type,styp,sstp,ssst,apc3,dssn,valu";
    char iFieldCount=8;
	
	dbg(DEBUG, "getLoaRecords:: Start [%s]", strFlnu);	
	
	iLoaCount=0;
	memset(arrLoa, 0, sizeof(arrLoa));
	
	sprintf(strSql, "SELECT %s FROM loatab WHERE flnu='%s' ",
	        strFields,strFlnu);

    dbg(DEBUG, "getLoaRecords:: get record [%s]", strSql);	
	        
	iTmp = sql_if(START,&slCursor,strSql,pclSqlData);
	while ( iTmp==DB_SUCCESS )
	{
		BuildItemBuffer (pclSqlData, strFields, iFieldCount, ",");
		get_real_item(arrLoa[iLoaCount].FLNU,pclSqlData,1); 
		
		get_real_item(arrLoa[iLoaCount].TYPE,pclSqlData,2); 
		if ( !strcmp(arrLoa[iLoaCount].TYPE, "") )
		    strcpy(arrLoa[iLoaCount].TYPE, " ");
		    
		get_real_item(arrLoa[iLoaCount].STYP,pclSqlData,3); 
		if ( !strcmp(arrLoa[iLoaCount].STYP, "") )
		    strcpy(arrLoa[iLoaCount].STYP, " ");
		    		
		get_real_item(arrLoa[iLoaCount].SSTP,pclSqlData,4); 
		if ( !strcmp(arrLoa[iLoaCount].SSTP, "") )
		    strcpy(arrLoa[iLoaCount].SSTP, " ");
		    
		get_real_item(arrLoa[iLoaCount].SSST,pclSqlData,5); 
		if ( !strcmp(arrLoa[iLoaCount].SSST, "") )
		    strcpy(arrLoa[iLoaCount].SSST, " ");
		    
		get_real_item(arrLoa[iLoaCount].APC3,pclSqlData,6); 
		if ( !strcmp(arrLoa[iLoaCount].APC3, "") )
		    strcpy(arrLoa[iLoaCount].APC3, " ");
		    
		get_real_item(arrLoa[iLoaCount].DSSN,pclSqlData,7); 		
		get_real_item(arrLoa[iLoaCount].VALU,pclSqlData,8); 
		dbg(TRACE, "LOA[%d][%s][%s][%s][%s][%s][%s][%s][%s]", 
		    iLoaCount, arrLoa[iLoaCount].FLNU, arrLoa[iLoaCount].TYPE, arrLoa[iLoaCount].STYP,
		    arrLoa[iLoaCount].SSTP, arrLoa[iLoaCount].SSST, arrLoa[iLoaCount].APC3, arrLoa[iLoaCount].DSSN,
		    arrLoa[iLoaCount].VALU);
		iLoaCount++;
		iTmp = sql_if(NEXT,&slCursor,strSql,pclSqlData);				
	}        
	close_my_cursor(&slCursor);
	        
	dbg(DEBUG, "getLoaRecords:: Flnu [%s] Count[%d] Result[%d]", strFlnu, iLoaCount, iRet);	
    return iRet;
}

/***************************************************************************************************************************
Return Value:
	0	-	Check was made and everything is successful
	-1	-	Check was made and some headers failed.
	1	-	No check was made bec. all prerequisite conditions failed.
***************************************************************************************************************************/
int doLoadCheck(char *strFlnu, int *ipCompleteness, int *ipPlausibility)
{
    int iRet=RC_SUCCESS;
    char strSql[1025];
    char strTmp[1025];
    char *strPtr, *strPtr2;
    int iFound=0;
    int a=0, b=0, c=0;
    int iIndexDSSN=-1;
    int iIndexVALU=-1;
    int iLoaIndex;
    int iTmp=RC_SUCCESS;
    int iCheck=0;
    
    
    dbg(DEBUG, "doLoadCheck:: Start");
    
    *ipCompleteness=RC_SUCCESS;
    *ipPlausibility=RC_SUCCESS;
    
    strcpy(gsDqctab.strType, "LOAD");
    dbg(DEBUG, "doLoadCheck:: Checking for LoadBlank: [%d]", sLoadConfig.iLoadBlank);
    
    iRet=getLoaRecords(strFlnu);
    if ( iLoaCount==0 )
    {
        strcpy(strArrAfttabError[iArrAfttabErrorCount], sLoadConfig.sLoadBlank[a].sLoadCheck[b].strMsg);                                
               populateDQCTab(&sDqctab[iDqctabCount], "", gsDqctab.strFlnu, gsDqctab.strType, "No LOAD record", 
               'F', gsDqctab.strHopo);
        iArrAfttabErrorCount++;
        iDqctabCount++;
        *ipCompleteness=RC_FAIL;
        iRet=RC_FAIL; 
    }	 
    else
    {
        for (a=0; a<sLoadConfig.iLoadBlank; a++)
        {
            dbg(DEBUG, "doLoadCheck:: LoadBlank[%d] Header[%s], Condition[%s]", 
                a, sLoadConfig.sLoadBlank[a].strHeader, sLoadConfig.sLoadBlank[a].strConditions);	
       
            if ( sLoadConfig.sLoadBlank[a].iFieldCheck==0 )
            	iTmp=RC_SUCCESS;
            else
                iTmp=RC_FAIL;
                
            for (b=0; b<sLoadConfig.sLoadBlank[a].iFieldCheck; b++)
            {
                dbg(DEBUG, "doLoadCheck:: (LoadBlank)[%d] CondCheck Name[%s]: Field[%s] Op[%s] Value[%s]", 
                    b, sLoadConfig.sLoadBlank[a].sFieldCheck[b].strName, 
                    sLoadConfig.sLoadBlank[a].sFieldCheck[b].strField, 
                    sLoadConfig.sLoadBlank[a].sFieldCheck[b].strOp, 
                    sLoadConfig.sLoadBlank[a].sFieldCheck[b].strValue);	
                    
                iTmp=doFieldCheck(&sLoadConfig.sLoadBlank[a].sFieldCheck[b]);
       
                if ( iTmp==RC_FAIL )
                    break;
            }
       
            if ( iTmp==RC_SUCCESS )
            {
            	iCheck++;
                dbg(TRACE, "doLoadCheck:: (LoadBlank)[%d]Need Further Check from Loatab: No of Conditions [%d] ", 
                    sLoadConfig.iLoadBlank, sLoadConfig.sLoadBlank[a].iLoadCheck);   
                for (b=0; b<sLoadConfig.sLoadBlank[a].iLoadCheck; b++)
                {
                	dbg(DEBUG, "doLoadCheck:: (LoadBlank) Load[%d/%d]: strType[%s], strStyp[%s], strSstp[%s], strSsst[%s] strApc3[%s], Op[%s] Value[%s]",
                	    b+1, sLoadConfig.sLoadBlank[a].iLoadCheck, 
                	    sLoadConfig.sLoadBlank[a].sLoadCheck[b].strType, 
                        sLoadConfig.sLoadBlank[a].sLoadCheck[b].strStyp,
                        sLoadConfig.sLoadBlank[a].sLoadCheck[b].strSstp,
                        sLoadConfig.sLoadBlank[a].sLoadCheck[b].strSsst,
                        sLoadConfig.sLoadBlank[a].sLoadCheck[b].strApc3,
                        sLoadConfig.sLoadBlank[a].sLoadCheck[b].strOp,
                        sLoadConfig.sLoadBlank[a].sLoadCheck[b].strValue);  
                        
                    iFound=0;   
       
                   	dbg(DEBUG, "doLoadCheck:: (LoadBlank) Checking [%s] Op[%s] Value[%s]", 
                   	    sLoadConfig.sLoadBlank[a].sLoadCheck[b].sCond.strField, 
                   	    sLoadConfig.sLoadBlank[a].sLoadCheck[b].sCond.strOp, 
                   	    sLoadConfig.sLoadBlank[a].sLoadCheck[b].sCond.strValue
                   	   );
                           
                    strcpy(strTmp, sLoadConfig.sLoadBlank[a].sLoadCheck[b].sCond.strValue);
                    strPtr=strtok_r(strTmp, ",", &strPtr2);
                  
                    while (strPtr)
                    {
                        dbg(DEBUG, "doLoadCheck:: Check[%s]", strPtr);
                        iLoaIndex=getLoaIndex(strFlnu, 
                                              sLoadConfig.sLoadBlank[a].sLoadCheck[b].strType, 
                                              sLoadConfig.sLoadBlank[a].sLoadCheck[b].strStyp,
                                              sLoadConfig.sLoadBlank[a].sLoadCheck[b].strSstp, 
                                              sLoadConfig.sLoadBlank[a].sLoadCheck[b].strSsst,
                                              sLoadConfig.sLoadBlank[a].sLoadCheck[b].strApc3,
                                              strPtr);
                        if ( iLoaIndex!=-1 )
                        {
                            dbg(DEBUG, "doLoadCheck:: DSSN Match Expected[%s] strOp[%s] dbValue[%s]",
                                sLoadConfig.sLoadBlank[a].sLoadCheck[b].strValue, 
                                sLoadConfig.sLoadBlank[a].sLoadCheck[b].strOp, 
                                arrLoa[iLoaIndex].VALU); 
                                    	
                            if ( !strcmp(sLoadConfig.sLoadBlank[a].sLoadCheck[b].strOp, "=") )
                            {
                                if ( 
                                   	  !strcmp(sLoadConfig.sLoadBlank[a].sLoadCheck[b].strValue, " ") ||
                                      !strcmp(sLoadConfig.sLoadBlank[a].sLoadCheck[b].strValue, "") 
                                    )
                                {
                                    if ( 
                                   	      !strcmp(arrLoa[iLoaIndex].VALU, "") ||
                                   	      !strcmp(arrLoa[iLoaIndex].VALU, " ") 
                                   	   )
                                    {
                                        dbg(DEBUG,"doLoadCheck:: VALU Space Pass[%s]", arrLoa[iLoaIndex].VALU);
                                  	    iFound=1;
                                   	    break;
                                   	}
                                }	
                                else if ( !strcmp(sLoadConfig.sLoadBlank[a].sLoadCheck[b].strValue, "0") )
                                {
                                    if ( !strcmp(arrLoa[iLoaIndex].VALU, "0") )
                                    {
                                         	dbg(DEBUG,"doLoadCheck:: VALU Zero Pass[%s]", arrLoa[iLoaIndex].VALU);
                                            iFound=1;
                                            break;	
                                   	}	
                                }
                            } 
                        }
                        
                        if ( iFound )
                            break;                            
                        strPtr=strtok_r(NULL, ",", &strPtr2);                     
                    }  
                     
                    
                    if (
                    	 iTmp==RC_FAIL ||
                    	 (iTmp==RC_SUCCESS && iFound)
                       )
                    {
                        strcpy(strArrAfttabError[iArrAfttabErrorCount], sLoadConfig.sLoadBlank[a].sLoadCheck[b].strMsg);                                
                        populateDQCTab(&sDqctab[iDqctabCount], "", gsDqctab.strFlnu, gsDqctab.strType, strArrAfttabError[iArrAfttabErrorCount], 
                                      'F', gsDqctab.strHopo);
                        iArrAfttabErrorCount++;
                        iDqctabCount++;
              	  		*ipCompleteness=RC_FAIL;
        	  	        iRet=RC_FAIL; 
        	  	    }           	         	
                }           
            }         	
        }       
    
        dbg(DEBUG, "doLoadCheck:: Checking for LoadVC: [%d], Current iRet[%d]", sLoadConfig.iLoadVC, iRet);
        for (a=0; a<sLoadConfig.iLoadVC; a++)
        {
            dbg(DEBUG, "doLoadCheck:: [%d] Header[%s], Condition[%s]", a, sLoadConfig.sLoadVC[a].strHeader, sLoadConfig.sLoadVC[a].strConditions);	
            
            if (sLoadConfig.sLoadVC[a].iFieldCheck==0)
            	iTmp=RC_SUCCESS;
            else
                iTmp=RC_FAIL;
                
            for (b=0; b<sLoadConfig.sLoadVC[a].iFieldCheck; b++)
            {
                dbg(DEBUG, "doLoadCheck::[%d] CondCheck Name[%s]: Field[%s] Op[%s] Value[%s]", 
                    b, sLoadConfig.sLoadVC[a].sFieldCheck[b].strName, 
                    sLoadConfig.sLoadVC[a].sFieldCheck[b].strField, 
                    sLoadConfig.sLoadVC[a].sFieldCheck[b].strOp, 
                    sLoadConfig.sLoadVC[a].sFieldCheck[b].strValue);	
                    
                iTmp=doFieldCheck(&sLoadConfig.sLoadVC[a].sFieldCheck[b]);
    
                if ( iTmp==RC_FAIL )
                    break;
            }    	
            
            if ( iTmp==RC_SUCCESS )
            {
            	iCheck++;
                dbg(TRACE, "doLoadCheck:: Need Further Check from Loatab: No of Conditions [%d]", sLoadConfig.sLoadVC[a].iLoadExp);
                for (b=0; b<sLoadConfig.sLoadVC[a].iLoadExp; b++)
                {
                    dbg(TRACE, "doLoadCheck:: LoadExp[%d/%d]", b+1, sLoadConfig.sLoadVC[a].iLoadExp);	
                    iTmp=evaluateLoadExp(strFlnu, &sLoadConfig.sLoadVC[a].sLoadExp[b]);
                    if ( iTmp!=0 )
                    {
                        strcpy(strArrAfttabError[iArrAfttabErrorCount], sLoadConfig.sLoadVC[a].sLoadExp[b].strMsg);   
                        populateDQCTab(&sDqctab[iDqctabCount], "", gsDqctab.strFlnu, gsDqctab.strType, strArrAfttabError[iArrAfttabErrorCount], 
                                           'F', gsDqctab.strHopo);
    
                        iArrAfttabErrorCount++;
                        iDqctabCount++;
                        *ipPlausibility=RC_FAIL;
                        iRet=RC_FAIL;                   	
                    }
                }
            }
        }
    }

    if ( iRet==RC_SUCCESS )
    {
        populateDQCTab(&sDqctab[iDqctabCount], "", gsDqctab.strFlnu, gsDqctab.strType, "PASS", 'P', gsDqctab.strHopo); 
        iArrAfttabErrorCount++;
        iDqctabCount++ ;  	
    }
        
    dbg(TRACE, "doLoadCheck:: End Result[%d]", iRet);
    return iRet;	
}

/*************************************************
Returns 0 if false
        -1 if function error
        1 if evaluate expression success.
**************************************************/
int evaluateLoadExp(char *strFlnu, LOADEXP *sLoad)
{
    int iRet=0;
    int iCheckEmpty=0;
    int iVal1;
    int iVal2;
    int iTmp;
    int a=0;
    int iLoaIndex;
    int iFound1=0, iFound2=0;
    char strSql[1025];
    char strTmp[1025];
    char *strPtr, *strPtr2;
    

    dbg(DEBUG, "evaluateLoadExp:: sLoadCheck1: strString[%s], strType[%s], strStyp[%s], strSstp[%s], strSsst[%s], strApc3[%s], strValue[%s], strOp[%s], strValue2[%s]",
        sLoad->sLoadCheck1.strString, 
        sLoad->sLoadCheck1.strType, 
        sLoad->sLoadCheck1.strStyp, 
        sLoad->sLoadCheck1.strSstp,
        sLoad->sLoadCheck1.strSsst, 
        sLoad->sLoadCheck1.strApc3, 
        sLoad->sLoadCheck1.strValue, 
        sLoad->sLoadCheck1.strOp, 
        sLoad->sLoadCheck1.strValue2);
    dbg(DEBUG, "evaluateLoadExp:: sLoadCheck1: sCond: strFieldBlankCheck[%s], strName[%s], strField[%s], strOp[%s], strValue[%s]",
        sLoad->sLoadCheck1.sCond.strFieldBlankCheck, 
        sLoad->sLoadCheck1.sCond.strName, 
        sLoad->sLoadCheck1.sCond.strField, 
        sLoad->sLoadCheck1.sCond.strOp, 
        sLoad->sLoadCheck1.sCond.strValue);
              
    if ( sLoad->sLoadCheck1.strValue==NULL )
    {
    	if ( sLoad->sLoadCheck1.strString==NULL )
    	    ;
    	else
    	{
     	    strcpy(strTmp, sLoad->sLoadCheck1.sCond.strValue);
            dbg(DEBUG, "evaluateLoadExp:: Parsing Values[%s]", strTmp);
            strPtr=strtok_r(strTmp, ",", &strPtr2);
            while (strPtr)
            {
                dbg(DEBUG,"evaluateLoadExp:: Checking [%s]", strPtr);
                iLoaIndex=getLoaIndex(strFlnu, sLoad->sLoadCheck1.strType, sLoad->sLoadCheck1.strStyp, 
                                      sLoad->sLoadCheck1.strSstp, sLoad->sLoadCheck1.strSsst, 
                                      sLoad->sLoadCheck1.strApc3, strPtr);

                if ( iLoaIndex!=-1 )
                {
                    dbg(DEBUG, "evaluateLoadExp:: Equal DSSN, Value[%s]", 
                        arrLoa[iLoaIndex].VALU);
                    if ( 
                          !strcmp(arrLoa[iLoaIndex].VALU, "") ||
                          !strcmp(arrLoa[iLoaIndex].VALU, " ") 
                       )
                        ;
                    else                         	       
                    {
                        dbg(DEBUG, "evaluateLoadExp:: sLoadCheck1: Value[%s] Op[%s] Value2[%s]",
                            arrLoa[iLoaIndex].VALU,
                            sLoad->sLoadCheck1.strOp,
                            sLoad->sLoadCheck1.strValue2);
                   	    iVal1=atoi(arrLoa[iLoaIndex].VALU);
                        if ( sLoad->sLoadCheck1.strOp!=NULL )
                        {
                            if ( !strcmp(sLoad->sLoadCheck1.strOp, "+") )
                                iVal1+=atoi(sLoad->sLoadCheck1.strValue2);
                            else if ( !strcmp(sLoad->sLoadCheck1.strOp, "-") )
                                iVal1-=atoi(sLoad->sLoadCheck1.strValue2);
                            else if ( !strcmp(sLoad->sLoadCheck1.strOp, "*") )
                                iVal1*=atoi(sLoad->sLoadCheck1.strValue2);
                            else if ( !strcmp(sLoad->sLoadCheck1.strOp, "/") )
                                iVal1/=atoi(sLoad->sLoadCheck1.strValue2);
                        }
                        iFound1=1;
                        break;	
                    }                         	 
                }
                            
                if ( iFound1 )
                    break;

                if ( iFound1 )
                    break;
                strPtr=strtok_r(NULL, ",", &strPtr2);                     	
            }                    
        }
  	
    }
    else if ( !strcmp(sLoad->sLoadCheck1.strValue, " ") )
    {
    	 iCheckEmpty=1;
    }
    else
    {
    	iFound1=1;
        iVal1=atoi(sLoad->sLoadCheck1.strValue);
    }
    
    if ( iRet!=-1 )
    {
        if ( iFound1 )
            dbg(DEBUG, "evaluateLoadExp:: iVal1[%d]", iVal1);
        else
    	    dbg(DEBUG, "evaluateLoadExp:: iVal1 no value");    
        
        dbg(DEBUG, "evaluateLoadExp:: sLoadCheck2: strString[%s], strType[%s], strStyp[%s], strSstp[%s], strSsst[%s], strApc3[%s], strValue[%s], strOp[%s], strValue2[%s]",
            sLoad->sLoadCheck2.strString, 
            sLoad->sLoadCheck2.strType, 
            sLoad->sLoadCheck2.strStyp, 
            sLoad->sLoadCheck2.strSstp,
            sLoad->sLoadCheck2.strSsst, 
            sLoad->sLoadCheck2.strApc3, 
            sLoad->sLoadCheck2.strValue, 
            sLoad->sLoadCheck2.strOp, 
            sLoad->sLoadCheck2.strValue2);
 
        dbg(DEBUG, "evaluateLoadExp:: sLoadCheck2: sCond: strFieldBlankCheck[%s], strName[%s], strField[%s], strOp[%s], strValue[%s]",
            sLoad->sLoadCheck2.sCond.strFieldBlankCheck, 
            sLoad->sLoadCheck2.sCond.strName, 
            sLoad->sLoadCheck2.sCond.strField, 
            sLoad->sLoadCheck2.sCond.strOp, 
            sLoad->sLoadCheck2.sCond.strValue);
        
        
        if ( sLoad->sLoadCheck2.strValue==NULL )
        {
            if ( sLoad->sLoadCheck2.strString==NULL )
        	    ;
        	else    
            {
          	    strcpy(strTmp, sLoad->sLoadCheck2.sCond.strValue);
           	    dbg(DEBUG, "evaluateLoadExp:: Parsing Values[%s]", strTmp);
                strPtr=strtok_r(strTmp, ",", &strPtr2);
                while (strPtr)
                {
                    dbg(DEBUG,"evaluateLoadExp:: Checking [%s]", strPtr);
                    iLoaIndex=getLoaIndex(strFlnu, sLoad->sLoadCheck1.strType, sLoad->sLoadCheck1.strStyp, 
                                          sLoad->sLoadCheck1.strSstp, sLoad->sLoadCheck1.strSsst, 
                                          sLoad->sLoadCheck1.strApc3, strPtr);

                    if ( iLoaIndex!=-1 )
                    {
                        dbg(DEBUG, "evaluateLoadExp:: Equal DSSN, Value[%s]", 
                            arrLoa[iLoaIndex].VALU);
                        if ( 
                              !strcmp(arrLoa[iLoaIndex].VALU, "") ||
                              !strcmp(arrLoa[iLoaIndex].VALU, " ") 
                            )
                            ;
                   	    else                         	       
                        {
                            iVal2=atoi(arrLoa[iLoaIndex].VALU);
                            if ( sLoad->sLoadCheck2.strOp!=NULL )
                            {
                                if ( !strcmp(sLoad->sLoadCheck2.strOp, "+") )
                                    iVal2+=atoi(sLoad->sLoadCheck2.strValue2);
                                else if ( !strcmp(sLoad->sLoadCheck2.strOp, "-") )
                                    iVal2-=atoi(sLoad->sLoadCheck2.strValue2);
                                else if ( !strcmp(sLoad->sLoadCheck2.strOp, "*") )
                                 	iVal2*=atoi(sLoad->sLoadCheck2.strValue2);
                                else if ( !strcmp(sLoad->sLoadCheck2.strOp, "/") )
                                 	iVal2/=atoi(sLoad->sLoadCheck2.strValue2);
                            }
                            	        
                            iFound2=1;
                        }                         	 
                    }
                    
                    if ( iFound2 )
                        break;
                    strPtr=strtok_r(NULL, ",", &strPtr2);
                }                 	
            }  	
        }
        else if ( !strcmp(sLoad->sLoadCheck2.strValue, " ") )
        {
        	 iCheckEmpty=1;
        }
        else
        {
        	iFound2=1;
            iVal2=atoi(sLoad->sLoadCheck2.strValue);
        }        
        
        if ( iRet!=-1 )
        {
            if ( iFound2 )
                dbg(DEBUG, "evaluateLoadExp:: iVal2[%d]", iVal2);
            else
        	    dbg(DEBUG, "evaluateLoadExp:: iVal2 no value");    
        }
    }
    
    if ( iRet!=-1 && iFound1 && iFound2 )
    {
        dbg(DEBUG,"evaluateLoadExp:: Evaluate Field[%d] Op[%s] Value[%d]", iVal1, sLoad->strOp, iVal2);	
        if ( sLoad->strOp!= NULL )
        {
            if ( !strcmp(sLoad->strOp, "=") )
                iRet=(iVal1==iVal2);	
            else if ( !strcmp(sLoad->strOp, "!=") )
                iRet=(iVal1!=iVal2);	
            else if ( !strcmp(sLoad->strOp, ">") )
                iRet=(iVal1>iVal2);	
            else if ( !strcmp(sLoad->strOp, ">=") )
                iRet=(iVal1>=iVal2);	
            else if ( !strcmp(sLoad->strOp, "<") )
                iRet=(iVal1<iVal2);	
            else if ( !strcmp(sLoad->strOp, "<=") )
                iRet=(iVal1<=iVal2);	
                
        }
    } 
    
    dbg(DEBUG,"evaluateLoadExp:: Return Value[%d]", iRet);
    return iRet;	
}

void printDBSelect(DBSELECT *sDB)
{
    int a=0;
    int b=0;
    
    dbg(DEBUG, "printDBSelect:: Total Rows[%d]", sDB->iRow);
    for (a=0; a<sDB->iRow; a++)
    {
    	dbg(DEBUG, "printDBSelect:: Row [%d], Total Columns[%d]", a, sDB->sDBRows[a].iColumn);
        for (b=0; b<sDB->sDBRows[a].iColumn; b++)
        {
            dbg(DEBUG, "printDBSelect::           Column[%d] Field[%s] Value[%s]",
                b, sDB->sDBRows[a].sDBFields[b].strFieldName, sDB->sDBRows[a].sDBFields[b].strValue);
                
        }   	
    }
    return;	
}
int populateDBRows(char *strSql, char *strFields, int iFieldCount, DBSELECT *sDBSelect)
{
    int iRet=RC_SUCCESS;
    int iTmp=0;
    int a=0;
    int iAllocate=0;
    short slCursor = 0;
    char  pclDataBuf[32726];
    char strTmp[1026];
    char strValue[1026];
    char *strPtr, *strPtr2;
    DBROWS *sDBRowsRealloc;
    DBFIELDS *sDBFieldsTmp;
    
    sDBSelect->iRow=0;
    dbg(DEBUG, "populateDBRows:: SQL[%s], FieldCount[%d]", strSql, iFieldCount);
    iTmp = sql_if (START, &slCursor, strSql, pclDataBuf);     

    while ( iTmp == DB_SUCCESS)
    {
        BuildItemBuffer (pclDataBuf, strFields, iFieldCount, ",");
        iAllocate=0;
        if ( sDBSelect->iRow==0 )
        {
            sDBSelect->sDBRows=(DBROWS *)malloc(sizeof(DBROWS));
            if ( sDBSelect->sDBRows )
                iAllocate=1;
        }
        else
        {
            sDBRowsRealloc=(DBROWS *)realloc(sDBSelect->sDBRows, sizeof(DBROWS)*(sDBSelect->iRow+1));
            if ( sDBRowsRealloc )
            {
                sDBSelect->sDBRows=sDBRowsRealloc;
                iAllocate=1;
            }
        }

        if ( iAllocate )
        { 
        	memset(&sDBSelect->sDBRows[sDBSelect->iRow], 0, sizeof(DBROWS));
        	sDBSelect->sDBRows[sDBSelect->iRow].iColumn=iFieldCount;
        	sDBSelect->sDBRows[sDBSelect->iRow].sDBFields=(DBFIELDS *)malloc(sizeof(DBFIELDS)*iFieldCount);
        	if ( sDBSelect->sDBRows[sDBSelect->iRow].sDBFields )
        	{
        		strcpy(strTmp, strFields);
        		strPtr=strtok_r(strTmp, ",", &strPtr2);
                for (a=0; a<iFieldCount; a++)
                {
                   get_real_item(strValue,pclDataBuf,a+1); 
                   sDBSelect->sDBRows[sDBSelect->iRow].sDBFields[a].strValue=(char *)malloc(sizeof(char)*(strlen(strValue)+1));
                   if ( sDBSelect->sDBRows[sDBSelect->iRow].sDBFields[a].strValue )
                       strcpy(sDBSelect->sDBRows[sDBSelect->iRow].sDBFields[a].strValue, strValue);
                   else
                       dbg(TRACE, "populateDBRows:: Cannot allocate strFieldValues");
                                            
                   sDBSelect->sDBRows[sDBSelect->iRow].sDBFields[a].strFieldName=(char *)malloc(sizeof(char)*(strlen(strPtr)+1));
                   if ( sDBSelect->sDBRows[sDBSelect->iRow].sDBFields[a].strFieldName )
                       strcpy(sDBSelect->sDBRows[sDBSelect->iRow].sDBFields[a].strFieldName, strPtr);
                   else
                       dbg(TRACE, "populateDBRows:: Cannot allocate strFieldName");
                   strPtr=strtok_r(NULL, ",", &strPtr2);   
                }
            }
            sDBSelect->iRow++;
        }
        iTmp = sql_if(NEXT,&slCursor, strSql, pclDataBuf);   
    }

    if ( iTmp==-1 )
        iRet=RC_FAIL;
        
    close_my_cursor(&slCursor);
    dbg(DEBUG, "populateDBRows:: SQL[%s], Rows Fetched[%d] iRet[%d]", strSql, sDBSelect->iRow, iRet);
    return iRet;	
}

void clearDBROWS(DBSELECT *sDBSelect)
{ 
    int iRet=RC_SUCCESS;
    int a=0,b=0;
 	
    for (a=0; a<sDBSelect->iRow; a++)
    {
        for (b=0; b<sDBSelect->sDBRows[a].iColumn; b++)
        {
        	if ( sDBSelect->sDBRows[a].sDBFields[b].strFieldName )
        	{
                free(sDBSelect->sDBRows[a].sDBFields[b].strFieldName);
                sDBSelect->sDBRows[a].sDBFields[b].strFieldName=NULL;                
            }
            
            if ( sDBSelect->sDBRows[a].sDBFields[b].strValue )
            {
                free(sDBSelect->sDBRows[a].sDBFields[b].strValue); 
                sDBSelect->sDBRows[a].sDBFields[b].strValue=NULL; 
            }
        }   
        
        if ( sDBSelect->sDBRows[a].sDBFields )
        {
            free(sDBSelect->sDBRows[a].sDBFields);
            sDBSelect->sDBRows[a].sDBFields=NULL;
        }
    }
    
    if ( sDBSelect->sDBRows )
    {
        free(sDBSelect->sDBRows);
        sDBSelect->sDBRows=NULL;
    }

    sDBSelect->iRow=0;
    sDBSelect->sDBRows=NULL;
    
    return;
}

int processERP(char *strFlnuArr, char *strFlnuDep)
{	
    int iRet=RC_SUCCESS;
    int iTmp;
    int iCnt=0;
    char strRkey[32];
    char strErr[1024];
    char strTmp[1024];
    char strDQCSArr[5];
    char strDQCSDep[5];
    char strDQCSB[5];
    char pclFlnuArr[32];
    char pclFlnuDep[32];
    char pclFlnuB[32];
    char **strAftB=NULL;
    char **strAftArr=NULL;
    char **strAftDep=NULL;
    int iIndexAdid=-1;
    int iIndexRkey=-1;
    int iIndexUrno=-1;
    int iDoCheck=0;
    short slCursor = 0;
    char pclDataBuf[1024] = "";
    
    	    
    dbg(TRACE,"====================   processERP:: Start strFlnuArr[%s] strFlnuDep[%s]  ====================", 
        strFlnuArr, strFlnuDep);	
    
    memset(strErr,0, sizeof(strErr));
    memset(strDQCSArr, 0, sizeof(strDQCSArr));
    memset(strDQCSDep, 0, sizeof(strDQCSDep));
    memset(strDQCSB, 0, sizeof(strDQCSB));
    memset(pclFlnuArr, 0, sizeof(pclFlnuArr));
    memset(pclFlnuDep, 0, sizeof(pclFlnuDep));
    memset(pclFlnuB, 0, sizeof(pclFlnuB));
    
    if ( !strcmp(strFlnuArr, "") && !strcmp(strFlnuDep, "") )
    {
        iRet=RC_FAIL;
        sprintf(strErr, "Arrival & Departure Flight empty");	
    }
    else 
    {
        sprintf(strTmp, "SELECT DISTINCT(rkey) RKEY FROM afttab WHERE urno in ('%s', '%s') ", strFlnuArr, strFlnuDep); 
        dbg(DEBUG,"processERP:: getting Rkey[%s]", strTmp); 
  	    iTmp=sql_if (START, &slCursor, strTmp, pclDataBuf); 
  	    iCnt = 0;
        while ( iTmp == DB_SUCCESS )
        {
            BuildItemBuffer (pclDataBuf, "RKEY", 1, ",");
            get_real_item(strRkey,pclDataBuf,1);         
            iCnt++;
            iTmp = sql_if(NEXT,&slCursor,strTmp,pclDataBuf);
        }
        close_my_cursor(&slCursor);
        dbg(DEBUG, "processERP:: Rkey Count[%d]", iCnt);
        
        if ( iTmp==RC_FAIL )
        {
        	dbg(DEBUG, "processERP:: DB Error on getting rkey[%s]", strTmp);
        	strcpy(strErr, "DB Error on getting rotation key");
            iRet=RC_FAIL;	
        }
        else
        {
            if ( iCnt==0 )
            {
                iRet=RC_FAIL;
                dbg(DEBUG, "processERP:: No record found.");
                sprintf(strErr, "No record found in DB");
            }
            else if ( iCnt>1 )
            {
            	iRet=RC_FAIL;
                dbg(DEBUG, "processERP:: Not a Rotation Flight");
           	    sprintf(strErr, "Not a Rotation Flight");
            }	
        }
    }

    if ( iRet==RC_SUCCESS )
    {
        strAftArr=getAfttabRecordUsingRkey(strRkey, "A", &iRet);  
     	if ( iRet==RC_FAIL )
     	{
     	    dbg(DEBUG, "processERP:: getAfttabRecordUsingRkey for arrival failed."); 	
     	    strcpy(strErr, "DB Error on getting arrival flight");
     	}
     	else
     	{
            strAftDep=getAfttabRecordUsingRkey(strRkey, "D", &iRet);  
            if ( iRet==RC_FAIL )
            {
     	        dbg(DEBUG, "processERP:: getAfttabRecordUsingRkey for arrival failed."); 	
     	        strcpy(strErr, "DB Error on getting departure flight");
     	    }     
        } 	
    }  
    
    if ( iRet==RC_SUCCESS )
    {
    	iIndexAdid=getArrTabFieldIndex("ADID", arrAfttabDBFieldsConfig, iArrAfttabDBFieldsConfigCount); 
    	iIndexRkey=getArrTabFieldIndex("RKEY", arrAfttabDBFieldsConfig, iArrAfttabDBFieldsConfigCount); 
        iIndexUrno=getArrTabFieldIndex("URNO", arrAfttabDBFieldsConfig, iArrAfttabDBFieldsConfigCount); 
        
        dbg(DEBUG, "processERP:: Arrival: Urno[%s] Rkey[%s] Adid[%s]",
            strAftArr[iIndexUrno], strAftArr[iIndexRkey], strAftArr[iIndexAdid]); 
        dbg(DEBUG, "processERP:: Arrival: Urno[%s] Rkey[%s] Adid[%s]",
            strAftDep[iIndexUrno], strAftDep[iIndexRkey], strAftDep[iIndexAdid]); 

        iDoCheck=1;
        if ( iRet==RC_SUCCESS && strAftArr[iIndexUrno]!=NULL )
        {
            iRet=doCheck(strAftArr[iIndexUrno], "ERP", "", "", 0, strDQCSArr);	
            strcpy(pclFlnuArr, strAftArr[iIndexUrno]);
            iCnt++;
        }
        sprintf(strErr, "%s", strDQCSArr);
               
        if ( strAftDep[iIndexUrno]!=NULL )
        {
            if ( iRet==RC_SUCCESS )
                iRet=doCheck(strAftDep[iIndexUrno], "ERP", "", "", 0, strDQCSDep);		
            else
                doCheck(strFlnuDep, "ERP", "", "", 0, strDQCSDep);	
            strcpy(pclFlnuDep, strAftDep[iIndexUrno]);	 	
        }                     
        sprintf(strErr, "%s,%s", strErr, strDQCSDep);
                  
        if ( !strcmp(strErr, ",") )
        {
        	if ( strcmp(strFlnuArr, "" ) )
        	{
                strAftB=getAfttabRecord(strFlnuArr, &iRet);
                strcpy(pclFlnuB, strFlnuArr);	 	
            }
            else
            {
             	strAftB=getAfttabRecord(strFlnuDep, &iRet);
             	strcpy(pclFlnuB, strFlnuDep);	 	
            }
            
            if ( iRet==RC_SUCCESS ) 
            {
            	iRet=doCheck(strAftB[iIndexUrno], "ERP", "", "", 0, strDQCSB);
            	if ( strcmp(strFlnuArr, "" ) )
                    sprintf(strErr, "%s,", strDQCSB);
                else
                	sprintf(strErr, ",%s", strDQCSB);
                	
            	freeStringArray(strAftB, iArrAfttabDBFieldsConfigCount);   	
            }	
        }        
        freeStringArray(strAftArr, iArrAfttabDBFieldsConfigCount);       	
        freeStringArray(strAftDep, iArrAfttabDBFieldsConfigCount);       	
    }
    
    if ( iRet==RC_SUCCESS )
    {
        if ( strcmp(pclFlnuArr, "") ) 
            updateFlightDQCS("SR", pclFlnuArr, "DQC");
        if ( strcmp(pclFlnuDep, "") )             
            updateFlightDQCS("SR", pclFlnuDep, "DQC");
        if ( strcmp(pclFlnuB, "") )             
            updateFlightDQCS("SR", pclFlnuB, "DQC");
        
        if ( strcmp(pclFlnuArr, "") || strcmp(pclFlnuDep, "") )
        {
            sprintf(strTmp,"%s,%s", pclFlnuArr, pclFlnuDep);
            iRet = TriggerBchdlAction("erpbil", "ERP","AFTTAB", "", "FLNUARR,FLNUDEP", 
                                      strTmp, pcgTwStart,pcgTwEnd,FALSE,TRUE);    

  	        if ( igQueOut>=FRONTEND_QUEUE )
            {
                dbg(TRACE,"processERP:: Sending to Client igQueOut[%d], prlBchd[%s], prlCmdblk[%s], Field[DQCS] Value[%s]",
                    igQueOut, prlBchd, "ERP", strErr);
                (void) new_tools_send_sql(igQueOut,prlBchd,prlCmdblk, "DQCS", strErr);   
            }
        }
    }
    else if ( iDoCheck )
    {
    	if ( strcmp(pclFlnuArr, "") ) 
            updateFlightDQCS(strDQCSArr, pclFlnuArr, "DQC"); 
        if ( strcmp(pclFlnuDep, "") )   	
            updateFlightDQCS(strDQCSDep, pclFlnuDep, "DQC");   
            
        if ( strcmp(pclFlnuB, "") )   	
            updateFlightDQCS(strDQCSB, pclFlnuB, "DQC");   
            
        if ( igQueOut>=FRONTEND_QUEUE )
        {    
            dbg(TRACE,"processERP:: Sending to Client igQueOut[%d], prlBchd[%s], prlCmdblk[%s], Field[DQCS] Value[%s]",
                igQueOut, prlBchd, "ERP", strErr);
            (void) new_tools_send_sql(igQueOut,prlBchd,prlCmdblk, "DQCS", strErr);     	
        }
    }
    else
    {
        if ( igQueOut>=FRONTEND_QUEUE )
        {
            dbg(TRACE,"processERP:: Sending to Client igQueOut[%d], prlBchd[%s], prlCmdblk[%s], Field[STATUS] Value[%s]",
                igQueOut, prlBchd, "ERP", strErr);
            (void) new_tools_send_sql(igQueOut,prlBchd,prlCmdblk, "STATUS", strErr);     
        }
    }  

    dbg(TRACE, "processERP:: strErr[%s]", strErr);                                        
    dbg(TRACE,"====================   processERP:: End strFlnuArr[%s] strFlnuDep[%s] Result[%d] RC_SUCCESS[%d] RC_FAIL[%d]  ====================", 
        strFlnuArr, strFlnuDep, iRet, RC_SUCCESS, RC_FAIL);	
    return iRet;	
}

int processTIM(char *strCmd)
{
	int iRet=RC_SUCCESS;	
	int a=0;
	int iSchedIndex=-1;
    DBSELECT sDBSelectAftTab;
    int iMultiplier=1;
    time_t tNow=time(NULL);
    struct tm *tmTime;
    int iTmp=0;
    time_t tTmp;
    char strTimeNow[15];
    char strStartDate[15];
    char strEndDate[15];            
    struct tm tmTmp;
    char strSql[1025];
    char *strPtr, *strPtr2;
    char strTmp[1025];
    int iFirst=1;
    int iIndexUrno;
	int iIndexDQCS;
	char strDQCS[5];


//int doCheck(char *strFlnu, char *strCmd, char *strType, char *pStrDQCS);    	    
//    iRet=doCheck("54219350", "CFM", "MOVE", " ");
  //  return;	    
    dbg(TRACE,"====================   processTIM:: Cmd[%s] Start   ====================", strCmd);
    for (a=0; a<iSched; a++)
    {
        if ( !strcmp(sSched[a].strName, strCmd) )
        {
            iSchedIndex=a;
            break;	
        }	
    }
    
    if ( iSchedIndex==-1 )
    {
        dbg(TRACE, "processTIM:: ERROR!!! Cmd[%s]Configuration not found! ");	
    }
    else
    {
        memset(&sDBSelectAftTab, 0, sizeof(sDBSelectAftTab));

        if ( !strcmp(sSched[iSchedIndex].strTimeRef, "DAY") )
            iMultiplier*=60*60*24;
        else if ( !strcmp(sSched[iSchedIndex].strTimeRef, "HOUR") )
            iMultiplier*=60*60;
        else if ( !strcmp(sSched[iSchedIndex].strTimeRef, "MIN") )
            iMultiplier*=60;
          
        tmTime=gmtime_r(&tNow, &tmTmp);	
        sprintf(strTimeNow, "%04d%02d%02d%02d%02d%02d",
                (tmTmp.tm_year+1900), (tmTmp.tm_mon+1), tmTmp.tm_mday,
                tmTmp.tm_hour, tmTmp.tm_min, tmTmp.tm_sec);

        tTmp=tNow +
             atoi(sSched[iSchedIndex].strStartTime)*iMultiplier +
             atoi(sSched[iSchedIndex].strTimeZone)*60*60;
        tmTime=gmtime_r(&tTmp, &tmTmp);	
    
        if ( !strcmp(sSched[iSchedIndex].strTimeRef, "DAY") ) 
        {
        	tmTmp.tm_hour=0;
            tmTmp.tm_min=0;
            tmTmp.tm_sec=0;
        }
        else if ( !strcmp(sSched[iSchedIndex].strTimeRef, "HOUR") ) 
        {
            tmTmp.tm_min=0;
            tmTmp.tm_sec=0;
        }
        else if ( !strcmp(sSched[iSchedIndex].strTimeRef, "MIN") )
            tmTmp.tm_sec=0;
            
        sprintf(strStartDate, "%04d%02d%02d%02d%02d%02d",
                (tmTmp.tm_year+1900), (tmTmp.tm_mon+1), tmTmp.tm_mday,
                tmTmp.tm_hour, tmTmp.tm_min, tmTmp.tm_sec);
             
        tTmp=tNow +
             atoi(sSched[iSchedIndex].strEndTime)*iMultiplier +
             atoi(sSched[iSchedIndex].strTimeZone)*60*60;
        tmTime=gmtime_r(&tTmp, &tmTmp);	
        if ( !strcmp(sSched[iSchedIndex].strTimeRef, "DAY") ) 
        {
        	tmTmp.tm_hour=0;
            tmTmp.tm_min=0;
            tmTmp.tm_sec=0;
        }  
        else if ( !strcmp(sSched[iSchedIndex].strTimeRef, "HOUR") ) 
        {
            tmTmp.tm_min=0;
            tmTmp.tm_sec=0;
        }
        else if ( !strcmp(sSched[iSchedIndex].strTimeRef, "MIN") )
            tmTmp.tm_sec=0;
            
        sprintf(strEndDate, "%04d%02d%02d%02d%02d%02d",
                (tmTmp.tm_year+1900), (tmTmp.tm_mon+1), tmTmp.tm_mday,
                tmTmp.tm_hour, tmTmp.tm_min, tmTmp.tm_sec);     

        dbg(DEBUG, "processTIM:: TimeNow[%s] Start Date[%s] End Date[%s]", strTimeNow, strStartDate, strEndDate);    

/*    
        sprintf(strSql, "SELECT %s "
    	        		"FROM afttab "
    		        	"WHERE ( "
    		    	             "( DES3=HOPO AND (%s BETWEEN '%s' AND '%s') ) OR "
    		    	             "( ORG3=HOPO AND (%s BETWEEN '%s' AND '%s') ) "
    		    	          ") ",
    		    strAfttabFieldNames, sSched[iSchedIndex].strArrivalField, strStartDate, strEndDate,
       		    sSched[iSchedIndex].strDepartureField, strStartDate, strEndDate);
*/
        sprintf(strSql, "SELECT %s FROM afttab WHERE ( (des3='%s' ", 
    		    strAfttabFieldNames, cgHopo);
 
        iFirst=1;
        strcpy(strTmp, sSched[iSchedIndex].strArrivalField);
        strPtr=strtok_r(strTmp, ",", &strPtr2);
        while ( strPtr )
        {
            trim(strPtr, ' ');
            if ( strcmp(strPtr, "") )
            {
                if ( iFirst )
                {
                    iFirst=0;	
                    sprintf(strSql, "%s AND ( (%s BETWEEN '%s' AND '%s') ",
                            strSql, strPtr, strStartDate, strEndDate);
                }	
                else
                {
                    sprintf(strSql, "%s OR 	(%s BETWEEN '%s' AND '%s') ",
                            strSql, strPtr, strStartDate, strEndDate);
                }
            }	
            strPtr=strtok_r(NULL, ",", &strPtr2);
        }   
        
        if ( !iFirst )
            strcat(strSql, ") )");
        else     
            strcat(strSql, ") ");
        
        sprintf(strSql, "%s OR (ORG3='%s' ", strSql, cgHopo);
        
        iFirst=1;
        strcpy(strTmp, sSched[iSchedIndex].strDepartureField);
        strPtr=strtok_r(strTmp, ",", &strPtr2);
        while ( strPtr )
        {
            trim(strPtr, ' ');
            if ( strcmp(strPtr, "") )
            {
                if ( iFirst )
                {
                    iFirst=0;	
                    sprintf(strSql, "%s AND ( (%s BETWEEN '%s' AND '%s') ",
                            strSql, strPtr, strStartDate, strEndDate);
                }	
                else
                {
                    sprintf(strSql, "%s OR 	(%s BETWEEN '%s' AND '%s') ",
                            strSql, strPtr, strStartDate, strEndDate);
                }
            }	
            strPtr=strtok_r(NULL, ",", &strPtr2);
        }   
        
        if ( !iFirst )
            strcat(strSql, ") )");
        else     
            strcat(strSql, ") ");
            
        strcat(strSql, ")  ");        
    		 
        if ( !strcmp(sSched[iSchedIndex].strDQCS,"*") )
            ;
        else if ( 
        	       !strcmp(sSched[iSchedIndex].strDQCS,"") ||
        	       !strcmp(sSched[iSchedIndex].strDQCS," ") 
        	    )
            sprintf(strSql, "%s AND (dqcs is NULL or dqcs=' ' or dqcs='' ) ", strSql);	        
        else
        {
        	iFirst=1;
        	strcpy(strTmp, sSched[iSchedIndex].strDQCS);
        	strPtr=strtok_r(strTmp, ",", &strPtr2);    
        	strPtr=strtok_r(NULL, ",", &strPtr2);
        	while (strPtr)
            {
            	trim(strPtr, ' ');
             	if ( strcmp(strPtr, "") )
            	{
            		if ( iFirst )
            		{
            			if ( !strncmp(sSched[iSchedIndex].strDQCS, "NOT", 3) )
                            sprintf(strSql, "%s AND dqcs NOT IN ('%s'", strSql, strPtr);	
                        else
                            sprintf(strSql, "%s AND dqcs IN ('%s'", strSql, strPtr);	
                        iFirst=0;
                    }
                    else
                    {
                        sprintf(strSql, "%s, '%s'", strSql, strPtr);	
                    }
                        
                }
                strPtr=strtok_r(NULL, ",", &strPtr2);
            }
            if ( !iFirst )
                strcat(strSql, ") ");            
        }

        dbg(TRACE, "processTim:: SELECT [%s]", strSql);
        iRet=populateDBRows(strSql, strAfttabFieldNames, iArrAfttabDBFieldsConfigCount, &sDBSelectAftTab);
        if ( iRet==RC_FAIL )
        {
            dbg(TRACE, "processTIM:: ERROR!!! populateDBRows (AFTTAB) DB error");
            iRet=RC_FAIL;
        }
        else if ( sDBSelectAftTab.iRow==0 )
        {
            dbg(TRACE, "processTIM:: WARNING!!! populateDBRows (AFTTAB) No Record Found" );
            iRet=RC_FAIL;
        }
        else
        {
            iIndexUrno=getArrTabFieldIndex("URNO", arrAfttabDBFieldsConfig, iArrAfttabDBFieldsConfigCount);
            iIndexDQCS=getArrTabFieldIndex("DQCS", arrAfttabDBFieldsConfig, iArrAfttabDBFieldsConfigCount);
            if ( iIndexUrno==-1 || iIndexDQCS==-1 )
            {
     	        dbg(TRACE, "processTIM:: ERROR!!! URNO/DQCS not defined in selection");	 
                iRet=RC_FAIL; 
            }
            else
            {
     	        dbg(TRACE, "processTIM::  Processing [%d] rows", sDBSelectAftTab.iRow);
     	 
                for (a=0; a<sDBSelectAftTab.iRow; a++)
                {
                    dbg(TRACE, "processTIM:: Row[%d/%d] Urno[%s] DQCS[%s]",	
                        a+1, sDBSelectAftTab.iRow, 
                        sDBSelectAftTab.sDBRows[a].sDBFields[iIndexUrno].strValue,
                        sDBSelectAftTab.sDBRows[a].sDBFields[iIndexDQCS].strValue);
                    
                    iTmp=doCheck(sDBSelectAftTab.sDBRows[a].sDBFields[iIndexUrno].strValue, "TIM", "", "", 0, strDQCS);
    	        }
    	    }               
    	}
        clearDBROWS(&sDBSelectAftTab);  
    }

    dbg(TRACE,"processTIM:: Cmd[%s] End", strCmd);	      
    dbg(TRACE,"====================   processTIM:: End   ====================");	
    return iRet;	
}

int processDQC(char *strFlnu)
{	
    int iRet=RC_FAIL;
    char strDQCS[5];
     	    
    dbg(TRACE,"====================   processDQC:: Start flnu[%s]   ====================", strFlnu);	

    iRet=doCheck(strFlnu, "DQC", "", "", 0, strDQCS);
    
    dbg(TRACE,"====================   processDQC:: End flnu[%s] Result[%d] RC_SUCCESS[%d] RC_FAIL[%d]  ====================", 
        strFlnu, iRet, RC_SUCCESS, RC_FAIL);	
    return iRet;	
}

int processIFRUFR(char *strFlnu)
{	
    int iRet=RC_SUCCESS;
    char strDQCS[5];
     	    
    dbg(TRACE,"====================   processIFRUFR:: Start flnu[%s]   ====================", strFlnu);	

    iRet=doCheck(strFlnu, "UFR", "MOVE", "", 0, strDQCS);
         
    dbg(TRACE,"====================   processIFRUFR:: End flnu[%s] Result[%d] RC_SUCCESS[%d] RC_FAIL[%d]  ====================", 
        strFlnu, iRet, RC_SUCCESS, RC_FAIL);
        	
    return iRet;	
}

int processLOA(char *strFlnu)
{	
    int iRet=RC_SUCCESS;
    char strDQCS[5];
    	    
    dbg(TRACE,"====================   processLOA:: Start flnu[%s]   ====================", strFlnu);	

    iRet=doCheck(strFlnu, "LOA", "LOAD", "", 0, strDQCS);
         
    dbg(TRACE,"====================   processLOA:: End flnu[%s] Result[%d] RC_SUCCESS[%d] RC_FAIL   ====================", 
        strFlnu, iRet, RC_SUCCESS, RC_FAIL);	
    return iRet;	
}

int processDFR(char *strFlnu)
{	
    int iRet=RC_SUCCESS;
    	    
    dbg(TRACE,"====================   processDFR Start flnu[%s]   ====================", strFlnu);	

    iRet=deleteDQCTab(strFlnu, "");
    		
    dbg(TRACE,"====================   processDFR End flnu[%s] Result[%d] RC_SUCCESS[%d] RC_FAIL[%d]   ====================", 
        strFlnu, iRet, RC_SUCCESS, RC_FAIL);	
    return iRet;	
}

int processCFM(char *strFlnu, char *strArr, char *strDep, char *strType)
{	
    int iRet=RC_SUCCESS;
    int iTmp;
    int a;
    DBSELECT stAfttabTmp;   
    const int iIndexUrno=0;
    const int iIndexDQCS=1;
    const int iIndexAdid=2;
    const int iIndexRKey=3;
    char strDQCS[5];
    char strTmp[1025];
    char strSql[1025];
    int iFirst=1;  
    	    
    dbg(TRACE,"====================   processCFM:: Start flnu[%s] or (ARRURNO[%s] DEPURNO[%s]) Type[%s]   ====================", 
        strFlnu, strArr, strDep, strType);	

    memset(strTmp, 0, sizeof(strTmp));
    if ( strcmp(strFlnu, "") )
        iTmp=doCheck(strFlnu, "CFM", strType, "C", 0, strDQCS);
    else if ( strcmp(strArr, "") && strcmp(strDep,"") )
    {
    	sprintf(strSql, "SELECT urno, dqcs, adid,rkey FROM afttab WHERE RKEY='%s' ORDER BY adid ", strArr);
        iTmp=populateDBRows(strSql, "URNO,DQCS,ADID,RKEY", 4, &stAfttabTmp);  
        dbg(DEBUG,"processCFM:: Number of Rows [%d] Status[%d]", stAfttabTmp.iRow, iTmp);

        for (a=0; a<stAfttabTmp.iRow; a++)
        {
            dbg(DEBUG, "processCFM:: [%d/%d]: Urno[%s] DQCS[%s] Adid[%s] RKey[%s]", a+1, stAfttabTmp.iRow,
                stAfttabTmp.sDBRows[a].sDBFields[iIndexUrno].strValue,     	
                stAfttabTmp.sDBRows[a].sDBFields[iIndexDQCS].strValue,       	
                stAfttabTmp.sDBRows[a].sDBFields[iIndexAdid].strValue,       	
                stAfttabTmp.sDBRows[a].sDBFields[iIndexRKey].strValue);    	

            iTmp=doCheck(stAfttabTmp.sDBRows[a].sDBFields[iIndexUrno].strValue, "CFM", strType, "C", 1, strDQCS);
            if ( 
                  !strcmp(stAfttabTmp.sDBRows[a].sDBFields[iIndexUrno].strValue, strArr) ||
                  !strcmp(stAfttabTmp.sDBRows[a].sDBFields[iIndexUrno].strValue, strDep) 
               )
            {
                if ( iFirst )
                {
                    iFirst=0;
                    sprintf(strTmp, "%s", strDQCS);	
                } 
                else
          	        sprintf(strTmp, "%s,%s", strTmp, strDQCS);   	
            } 
        }
        clearDBROWS(&stAfttabTmp);    	
        
        if ( igQueOut>=FRONTEND_QUEUE )
        {
            dbg(TRACE,"processCFM:: Sending to Client igQueOut[%d], prlBchd[%s], prlCmdblk[%s], Field[DQCS] Value[%s]",
                igQueOut, prlBchd, prlCmdblk, strTmp);
            (void) new_tools_send_sql(igQueOut,prlBchd,prlCmdblk, "DQCS", strTmp);  
        }
    }
                                         
    dbg(TRACE,"====================   processCFM:: End flnu[%s] or (ARRURNO[%s] DEPURNO[%s]) STRDQCS[%s] Result[%d] RC_SUCCESS[%d] RC_FAIL[%d]   ====================", 
        strFlnu, strArr, strDep, strTmp, iRet, RC_SUCCESS, RC_FAIL);	
    return iRet;	
}

int processUNCFM(char *strFlnu, char *strArr, char *strDep, char *strType)
{	
    int iRet=RC_SUCCESS;
    int iTmp;
    int a;
    DBSELECT stAfttabTmp;   
    const int iIndexUrno=0;
    const int iIndexDQCS=1;
    const int iIndexAdid=2;
    const int iIndexRKey=3;
    char strDQCS[5];
    char strTmp[1025];
    char strSql[1025];
    int iFirst=1; 
    	    
    dbg(TRACE,"====================   processUNCFM:: Start flnu[%s] or (ARRURNO[%s] DEPURNO[%s]) Type[%s]   ====================", 
        strFlnu, strArr, strDep, strType);	

    memset(strTmp, 0, sizeof(strTmp));
    if ( strcmp(strFlnu, "") )
        iRet=doCheck(strFlnu, "CFM", strType, " ", 0, strDQCS);
    else if ( strcmp(strArr, "") && strcmp(strDep,"") )
    {
        sprintf(strSql, "SELECT urno, dqcs, adid,rkey FROM afttab WHERE RKEY='%s' ORDER BY adid desc ", strArr);
        iTmp=populateDBRows(strSql, "URNO,DQCS,ADID,RKEY", 4, &stAfttabTmp);  
        dbg(DEBUG,"processUNCFM:: Number of Rows [%d] Status[%d]", stAfttabTmp.iRow, iTmp);
        memset(strTmp, 0, sizeof(strTmp));
        for (a=0; a<stAfttabTmp.iRow; a++)
        {
            dbg(DEBUG, "processUNCFM:: [%d/%d]: Urno[%s] DQCS[%s] Adid[%s] RKey[%s]", a+1, stAfttabTmp.iRow,
                stAfttabTmp.sDBRows[a].sDBFields[iIndexUrno].strValue,     	
                stAfttabTmp.sDBRows[a].sDBFields[iIndexDQCS].strValue,       	
                stAfttabTmp.sDBRows[a].sDBFields[iIndexAdid].strValue,       	
                stAfttabTmp.sDBRows[a].sDBFields[iIndexRKey].strValue);    	

            iTmp=doCheck(stAfttabTmp.sDBRows[a].sDBFields[iIndexUrno].strValue, "CFM", strType, " ", 1, strDQCS);
            if ( 
                  !strcmp(stAfttabTmp.sDBRows[a].sDBFields[iIndexUrno].strValue, strArr) ||
                  !strcmp(stAfttabTmp.sDBRows[a].sDBFields[iIndexUrno].strValue, strDep) 
               )
            {
            	if ( iFirst )
                {
                    iFirst=0;
                    sprintf(strTmp, "%s", strDQCS);	
                } 
                else
          	        sprintf(strTmp, "%s,%s", strTmp, strDQCS);      	
            }
        }
        
        if ( igQueOut>=FRONTEND_QUEUE )
        {
            dbg(TRACE,"processUNCFM:: Sending to Client igQueOut[%d], prlBchd[%s], prlCmdblk[%s], Field[DQCS] Value[%s]",
                igQueOut, prlBchd, prlCmdblk, strTmp);
            (void) new_tools_send_sql(igQueOut,prlBchd,prlCmdblk, "DQCS", strTmp);  
        }
    }
        
    dbg(TRACE,"====================   processUNCFM:: End flnu[%s] or (ARRURNO[%s] DEPURNO[%s]) STRDQCS[%s] Result[%d] RC_SUCCESS[%d] RC_FAIL[%d]   ====================", 
        strFlnu, strArr, strDep, strTmp, iRet, RC_SUCCESS, RC_FAIL);	
        
    return iRet;	
}

int doFieldCheck(FIELDCHECK *sField)
{
	int iRet=RC_SUCCESS;
	int iFound=0;
	int iIndex=0;
	char strTmp[1024];
	
    dbg(DEBUG, "doFieldCheck::Name[%s] Field[%s] Ops[%s] Value[%s]",
        sField->strName, sField->strField, sField->strOp, sField->strValue);
    
    iIndex=getArrTabFieldIndex(sField->strField, arrAfttabDBFieldsConfig, iArrAfttabDBFieldsConfigCount);
    if ( iIndex==-1 )
    {
        dbg(DEBUG, "doFieldCheck:: Configuration[%s]. Field[%s] not in AFTTAB. ", 
            sField->strName, sField->strField);
        sprintf(strArrAfttabError[iArrAfttabErrorCount], "Configuration[%s]. Field[%s] not in AFTTAB. ", 
                sField->strName, sField->strField);

        populateDQCTab(&sDqctab[iDqctabCount], "", gsDqctab.strFlnu, gsDqctab.strType, strArrAfttabError[iArrAfttabErrorCount], 
                       'F', gsDqctab.strHopo);

        iArrAfttabErrorCount++;
        iDqctabCount++;
        
        iRet=RC_FAIL;            	
    }
    else	
    {
    	if ( !strcmp(sField->strOp, "=") )
    	{
    	    if ( strcmp(strArrAfttabFieldValues[iIndex], sField->strValue) )
    	    {
                dbg(DEBUG, "doFieldCheck:: Configuration[%s]. Field[%s] with Value[%s] = [%s] FAILED. ", 
                    sField->strName, arrAfttabDBFieldsConfig[iIndex].strFieldDesc, strArrAfttabFieldValues[iIndex], sField->strValue);  	
    	        iRet=RC_FAIL;
    	    }
    	    else
    	    {
                dbg(DEBUG, "doFieldCheck:: Configuration[%s]. Field[%s] with Value[%s] = [%s] Success. ", 
                    sField->strName, arrAfttabDBFieldsConfig[iIndex].strFieldDesc, strArrAfttabFieldValues[iIndex], sField->strValue);     	    	
    	    }
    	}
    	else if ( !strcmp(sField->strOp, "!=") )
    	{
    	    if ( !strcmp(strArrAfttabFieldValues[iIndex], sField->strValue) )
    	    {
    	    	dbg(DEBUG, "doFieldCheck:: Configuration[%s]. Field[%s] with Value[%s] != [%s] Failed. ", 
                    sField->strName, arrAfttabDBFieldsConfig[iIndex].strFieldDesc, strArrAfttabFieldValues[iIndex], sField->strValue);    
    	        iRet=RC_FAIL;
    	    }
    	    else
    	    {
                dbg(DEBUG, "doFieldCheck:: Configuration[%s]. Field[%s] with Value[%s] != [%s] Success. ", 
                    sField->strName, arrAfttabDBFieldsConfig[iIndex].strFieldDesc, strArrAfttabFieldValues[iIndex], sField->strValue);     	    	
    	    }    	
    	}
        else if ( !strcmp(sField->strOp, "NOT") )
        {    	 
    	  	iFound=findInSelection(strArrAfttabFieldValues[iIndex], sField->strValue);
    	  	if ( iFound )
    	  	{  	  		
                dbg(DEBUG, "doFieldCheck:: Configuration[%s]. Field[%s] with Value[%s] NOT selection[%s] FAILED. ", 
                    sField->strName, arrAfttabDBFieldsConfig[iIndex].strFieldDesc, strArrAfttabFieldValues[iIndex], sField->strValue);  	  		
      	  		
    	  	    iRet=RC_FAIL;	
    	  	}
    	  	else
    	  		dbg(DEBUG, "doFieldCheck:: Configuration[%s]. Field[%s] with Value[%s] NOT selection[%s] SUCCESS. ", 
                    sField->strName, arrAfttabDBFieldsConfig[iIndex].strFieldDesc, strArrAfttabFieldValues[iIndex], sField->strValue);  
        }	
        else if ( !strcmp(sField->strOp, "IS") )
        {
     	  	iFound=findInSelection(strArrAfttabFieldValues[iIndex], sField->strValue);
    	  	if ( !iFound )
    	  	{
                dbg(DEBUG, "doFieldCheck:: Configuration[%s]. Field[%s] with Value[%s] IS selection[%s] FAILED. ", 
                    sField->strName, arrAfttabDBFieldsConfig[iIndex].strFieldDesc, strArrAfttabFieldValues[iIndex], sField->strValue);  	
         	  		
    	  	    iRet=RC_FAIL;	
    	  	}       
    	  	else
    	  		dbg(DEBUG, "doFieldCheck:: Configuration[%s]. Field[%s] with Value[%s] IS selection[%s] SUCCESS. ", 
                    sField->strName, arrAfttabDBFieldsConfig[iIndex].strFieldDesc, strArrAfttabFieldValues[iIndex], sField->strValue);  	
        }
    }    
    		
    dbg(DEBUG, "doFieldCheck:: Name[%s] Field[%s] Ops[%s] Value[%s], Result[%d]",
        sField->strName, sField->strField, sField->strOp, sField->strValue, iRet);

    return iRet;        
}

int getArrTabFieldIndex(char *strFind, DBFIELDSCONFIG *sDB, int iMax)
{
    int iRet=RC_FAIL;
    int a=0;
           
    if ( sDB==NULL )
        ;
    else
    {
        for (a=0; a<iMax; a++)
        {
            if ( !strcmp(sDB[a].strFieldName, strFind) )
                break;
        }
    
        if ( a < iMax )
            iRet=a;
    }    	

    return iRet;   	
}

int getMapIndex(char *strFind, MAP *sMap)
{
    int iRet=RC_FAIL;
    int a=0;
     
    if ( sMap==NULL )
        ;
    else
    {	           
        for (a=0; a<sMap->iMapCount; a++)
        {
            if ( !strcmp(sMap->sPair[a].strKey, strFind) )
                break;
        }
    
        if ( a < sMap->iMapCount )
            iRet=a;
    }
       
    return iRet;   	
}

int findInSelection(char *strField, char *strValue)
{
    int iFound=0;
    char *strTmp=(char *)malloc(sizeof(char)*(strlen(strValue)+1));
    char *strPtr, *strPtr2;
    
    if ( strTmp )
    {
        strcpy(strTmp, strValue);
        strPtr=strtok_r(strTmp, ",", &strPtr2);
        while ( strPtr )
        {
    	    if ( !strcmp(strField, strPtr) )
    	    {
    	        iFound=1;
    	        break;	
    	    }
            strPtr=strtok_r(NULL, ",", &strPtr2);    	
        }
        free(strTmp);
        strTmp=NULL;
    }
    else
    {
        dbg(TRACE, "findInSelection:: Cannot allocate strTmp");	
    }
    
    return iFound;    	
}

int getDBFieldDesc(char *strField, char *strTableName, char **strDBFieldDesc)
{
    int iRet=RC_SUCCESS;
    char strSql[101];
    short slCursor = 0;
    char  pclDataBuf[2048];
    char strTmp[1024];

    dbg(DEBUG, "getDBFieldDesc:: strField[%s], strTableName[%s]", strField, strTableName);
    
    sprintf(strSql, "SELECT addi FROM systab WHERE tana='%s' and fina='%s' ",
            strTableName, strField);
      
    dbg(DEBUG, "getDBFieldDesc:: Executing[%s]", strSql);        
    iRet=sql_if (START, &slCursor, strSql, pclDataBuf);      		
    if( iRet==0 )
    {
        BuildItemBuffer (pclDataBuf, "addi", 1, ",");   
        get_real_item(strTmp,pclDataBuf,1); 
        *strDBFieldDesc=(char *)malloc(sizeof(char)* (strlen(strTmp)+1));
        if ( *strDBFieldDesc )
            strcpy(*strDBFieldDesc, strTmp);   	
        else
        {
            dbg(TRACE, "getDBFieldDesc:: ERROR!!! Cannot allocate strDBFieldDesc" ); 
            iRet=RC_FAIL;	
        }
    }
    /*
    else if ( iRet==1 )
    {
        dbg(TRACE, "getDBFieldDesc:: WARNING!!! No definition found for [%s.%s].", strTableName, strField);
        sprintf(strTmp, "[%s.%s]", strTableName, strField);
        *strDBFieldDesc=(char *)malloc(sizeof(char)* (strlen(strTmp)+1));
        if ( *strDBFieldDesc )
        {
            strcpy(*strDBFieldDesc, strTmp);   	
            iRet=RC_SUCCESS;
        }
        else
        {
            dbg(TRACE, "getDBFieldDesc:: ERROR!!! Cannot allocate strDBFieldDesc" ); 
            iRet=RC_FAIL;	
        }        
    }
    */
    else
    {
        dbg(TRACE, "getDBFieldDesc:: ERROR!!! DB Failed [%s]", strField);	
    }
    
    dbg(DEBUG, "getDBFieldDesc:: strField[%s], strTableName[%s], Result[%d]", strField, strTableName, iRet);
    close_my_cursor(&slCursor);               
    return iRet;    	
}

char **getDBFieldsInDB(char *strTableName, char **strFieldNames, int *iSize, int *iRet)
{
    char **strArr;
    char strSql[1024];
    short slCursor = 0;
    char  pclDataBuf[32726];
    char strTmp[132];
    int iStrSize=1;
    int a=0;
    int iFirst=1;
       
    sprintf(strSql, "SELECT COUNT(COLUMN_NAME) column_count FROM ALL_TAB_COLUMNS WHERE TABLE_NAME='%s' ORDER BY COLUMN_NAME", strTableName);
    
    *iSize=-1;
    *iRet = sql_if (START, &slCursor, strSql, pclDataBuf);      		
    if( *iRet==DB_SUCCESS )
    {
        BuildItemBuffer (pclDataBuf, "column_count", 1, ",");   
        get_real_item(strTmp,pclDataBuf,1); 
        *iSize=atoi(strTmp);        		
    }
    close_my_cursor(&slCursor);
    
    if ( *iSize> -1 )
    {
        strArr=(char **)malloc(*iSize*sizeof(char *));
        if ( strArr )
        {
            sprintf(strSql, "SELECT COLUMN_NAME FROM ALL_TAB_COLUMNS WHERE TABLE_NAME='%s' order by COLUMN_NAME", strTableName); 
            *iRet = sql_if (START, &slCursor, strSql, pclDataBuf);      		
            if( *iRet==DB_SUCCESS )
            {
    	        while ( *iRet==DB_SUCCESS )
                {
                    BuildItemBuffer (pclDataBuf, "COLUMN_NAME", 1, ",");
                    get_real_item(strTmp,pclDataBuf,1); 	 
                
                    strArr[a]=(char *)malloc((strlen(strTmp)+1)*sizeof(char));
                    if ( strArr[a] )
                    {
                        strcpy(strArr[a++], strTmp);
                    }
                    else
                    {
                        dbg(TRACE, "getDBFieldsInDB::Cannot allocate strArr");	
                    }
                    iStrSize+=strlen(strTmp)+1;                
                
                    *iRet = sql_if(NEXT,&slCursor,strSql,pclDataBuf);
                }
            
        	    *strFieldNames=(char *)malloc(sizeof(char)*iStrSize);
        	    if ( *strFieldNames )
        	    {
                    memset(*strFieldNames, 0, (sizeof(char)*iStrSize));
                    for (a=0; a<*iSize; a++)
                    {
                        if ( iFirst )
                        {
                            iFirst=0;
                            strcpy(*strFieldNames, strArr[a]);
                        }
                        else
                            sprintf(*strFieldNames, "%s,%s", *strFieldNames, strArr[a]);
                    }
                }
                else
                {
                    dbg(TRACE, "getDBFieldsInDB:: Cannot allocate strFieldNames");	
                }
            }
            close_my_cursor(&slCursor);
        }
        else
        {
            dbg(TRACE, "getDBFieldsInDB:: Cannot allocate strArr");	
        }	
    }
    else
    {
       dbg(TRACE,"getDBFieldsInDB::Error in Executing SQL [%s][%d]", strSql, iRet);	 
    } 

    return strArr;	
}

char **getAfttabRecord(char *strFlnu, int *iRet)
{
	char **strRet=NULL;
    char strSql[32726];
    char strTmp[2048];
    short slCursor = 0;
    char  pclDataBuf[32726];
    int a=0;
        
    dbg(DEBUG, "getAfttabRecord:: strFlnu[%s]", strFlnu);    
    *iRet=RC_FAIL;
    strRet=(char **)malloc(iArrAfttabDBFieldsConfigCount*sizeof(char *));
    for (a=0; a<iArrAfttabDBFieldsConfigCount; a++)
        strRet[a]=NULL;
        
    if ( strRet )
    { 
        sprintf(strSql, "SELECT %s "
    	    			"FROM afttab "
    		    		"WHERE urno='%s' ", 
    		    strAfttabFieldNames, strFlnu);
        dbg(DEBUG, "getAfttabRecord:: Executing [%s]", strSql);
        a = sql_if (START, &slCursor, strSql, pclDataBuf);      		
        if( a==0 )
        {
        	*iRet=RC_SUCCESS;
            BuildItemBuffer (pclDataBuf, strAfttabFieldNames, iArrAfttabDBFieldsConfigCount, ",");
            for (a=0; a<iArrAfttabDBFieldsConfigCount; a++)       
            {   
                get_real_item(strTmp,pclDataBuf,a+1); 
                strRet[a]=(char *)malloc((strlen(strTmp)+1)*sizeof(char));
                strcpy(strRet[a], strTmp);
            }
        }
        else if ( a==1 )
        	dbg(TRACE,"getAfttabRecord::ERROR!!! No record found");	
        else
        {
            dbg(TRACE,"getAfttabRecord::ERROR!!! DB ERROR");	 
        }
     
        close_my_cursor(&slCursor);
    }
    else
    {
        dbg(TRACE, "getAfttabRecord:: ERROR!!! Cannot allocate strRet");	
    }
    
    dbg(DEBUG, "getAfttabRecord:: strFlnu[%s], Result[%d]", strFlnu, *iRet);    
    return strRet;
}

char **getAfttabRecordUsingRkey(char *strRkey, char *strAdid, int *iRet)
{
	char **strRet=NULL;
    char strSql[32726];
    char strTmp[2048];
    short slCursor = 0;
    char  pclDataBuf[32726];
    int a=0;
        
    dbg(DEBUG, "getAfttabRecordUsingRkey:: strFlnu[%s]", strRkey);    
    *iRet=RC_FAIL;
    strRet=(char **)malloc(iArrAfttabDBFieldsConfigCount*sizeof(char *));
    for (a=0; a<iArrAfttabDBFieldsConfigCount; a++)
        strRet[a]=NULL;
        
    if ( strRet )
    { 
        sprintf(strSql, "SELECT %s "
    	    			"FROM afttab "
    		    		"WHERE rkey='%s' AND adid='%s' ", 
    		    strAfttabFieldNames, strRkey, strAdid);
        dbg(DEBUG, "getAfttabRecordUsingRkey:: Executing [%s]", strSql);
        a = sql_if (START, &slCursor, strSql, pclDataBuf);      		
        if( a==0 )
        {
        	*iRet=RC_SUCCESS;
            BuildItemBuffer (pclDataBuf, strAfttabFieldNames, iArrAfttabDBFieldsConfigCount, ",");
            for (a=0; a<iArrAfttabDBFieldsConfigCount; a++)       
            {   
                get_real_item(strTmp,pclDataBuf,a+1); 
                strRet[a]=(char *)malloc((strlen(strTmp)+1)*sizeof(char));
                strcpy(strRet[a], strTmp);
            }
        }
        else if ( a==1 )
        {
        	*iRet=RC_SUCCESS;
        	dbg(TRACE,"getAfttabRecordUsingRkey::ERROR!!! No record found");
        }	
        else
        {
            dbg(TRACE,"getAfttabRecordUsingRkey::ERROR!!! DB ERROR");	 
        }
     
        close_my_cursor(&slCursor);
    }
    else
    {
        dbg(TRACE, "getAfttabRecordUsingRkey:: ERROR!!! Cannot allocate strRet");	
    }
    
    dbg(DEBUG, "getAfttabRecordUsingRkey:: strFlnu[%s], Result[%d]", strRkey, *iRet);    
    return strRet;
}
/******************************************************
returns 1 if evaluated.
returns 0 no evaluation needed.
******************************************************/
int evaluateExp(EXPRESSION *sValue, double *dResult)
{
    int iIndex;
    char strField1[128];
    char strField2[128];
    
    dbg(DEBUG, "evaluateExp:: strField1[%s] strOp[%s] strField2[%s]", sValue->strField1, sValue->strOp, sValue->strField2);
    *dResult=0;
    
    memset(&strField1, 0, sizeof(strField1));
    memset(&strField2, 0, sizeof(strField2));

    iIndex=getArrTabFieldIndex(sValue->strField1, arrAfttabDBFieldsConfig, iArrAfttabDBFieldsConfigCount);
    if ( iIndex==-1 )
    {
        if ( sValue->cUnary1==0 )
            strcpy(strField1, sValue->strField1);
        else
         	sprintf(strField1, "%c%s", sValue->cUnary1, sValue->strField1); 	
    }
    else
    {
    	if ( !strcmp(strArrAfttabFieldValues[iIndex], "") )
    	    return 0;
    	    
        if ( sValue->cUnary1==0 )
            strcpy(strField1, strArrAfttabFieldValues[iIndex]);
        else
       	    sprintf(strField1, "%c%s", sValue->cUnary1, strArrAfttabFieldValues[iIndex]); 
    }
    
    dbg(DEBUG, "evaluateExp:: Field1 Value[%s]", strField1);
    
    iIndex=getArrTabFieldIndex(sValue->strField2, arrAfttabDBFieldsConfig, iArrAfttabDBFieldsConfigCount);

    if ( iIndex==-1 )
    {
        if ( sValue->cUnary2==0 )
            strcpy(strField2, sValue->strField2);
        else
        	sprintf(strField2, "%c%s", sValue->cUnary2, sValue->strField2); 	
    }
    else
    {
    	if ( !strcmp(strArrAfttabFieldValues[iIndex], "") )
    	    return 0;
    	    
        if ( sValue->cUnary2==0 )
            strcpy(strField2, strArrAfttabFieldValues[iIndex]);
        else
            sprintf(strField2, "%c%s", sValue->cUnary2, strArrAfttabFieldValues[iIndex]);
    }
    dbg(DEBUG, "evaluateExp:: Field2 Value[%s]", strField2);

   if ( !strcmp(sValue->strDataType, "DATETIME") )
   {
       evaluateDate(strField1, sValue->strOp, strField2, dResult);
       dbg(DEBUG, "evaluateExp:: Value1[%s] VALUE2[%s] RESULT[%f]", strField1, strField2, *dResult);   
    }
    return 1;	
}

time_t convertStringDateToTime(char *str)
{
    char yyyy[5];
    char mm[3];
    char dd[3];
    char hh[3];
    char mi[3];
    char ss[3];
    time_t tRet;
    struct tm tmTime;
    
    memset(&yyyy, 0, sizeof(yyyy));	
    memset(&mm, 0, sizeof(mm));	
    memset(&dd, 0, sizeof(dd));	
    memset(&hh, 0, sizeof(hh));	
    memset(&mi, 0, sizeof(mi));	
    memset(&ss, 0, sizeof(ss));	    
    
    sprintf(yyyy, "%4.4s", str);
    sprintf(mm, "%2.2s", str+4);
    sprintf(dd, "%2.2s", str+6);
    sprintf(hh, "%2.2s", str+8);
    sprintf(mi, "%2.2s", str+10);
    sprintf(ss, "%2.2s", str+12);
    
    memset(&tmTime, 0, sizeof(struct tm));
    tmTime.tm_sec=atoi(ss);
    tmTime.tm_min=atoi(mi);
    tmTime.tm_hour=atoi(hh);
    tmTime.tm_mday=atoi(dd);
    tmTime.tm_mon=atoi(mm)-1;
    tmTime.tm_year=atoi(yyyy)-1900;
    
    /*
    time_t tNow=time(NULL);
    struct tm tmRes;
    
    gmtime_r(&tNow, &tmRes);
    dbg(TRACE, "Value of time now [%d][%d][%d][%d] [%d][%d][%d][%d][%d]",
                tmRes.tm_sec, tmRes.tm_min, tmRes.tm_hour, tmRes.tm_mday, 
                tmRes.tm_mon, tmRes.tm_year, tmRes.tm_wday, tmRes.tm_yday, tmRes.tm_isdst);

    memset(&tmTime, 0, sizeof(struct tm));
    tmTime.tm_sec=tmRes.tm_sec;
    tmTime.tm_min= tmRes.tm_min;
    tmTime.tm_hour=tmRes.tm_hour;
    tmTime.tm_mday=tmRes.tm_mday;
    tmTime.tm_mon=tmRes.tm_mon;
    tmTime.tm_year=tmRes.tm_year;
    */
    
    tRet=mktime(&tmTime);

    //dbg(TRACE, "Breakdown: [%s][%s][%s][%s][%s][%s][%d]", yyyy, mm, dd, hh, mi, ss, tmTime.tm_isdst);
    //dbg(TRACE, "TIME [%d][%d]", tNow, tRet);
    return tRet;
    
}

int evaluateDate(char *strField1, char *strOp, char *strField2, double *dRes)
{
    int iRet=RC_SUCCESS;
    *dRes=0;
    
    dbg(DEBUG, "evaluateDate:: strField1[%s] strOp[%s] strField2[%s] ", strField1, strOp, strField2);
    if ( strOp==NULL )
    {
        *dRes=strtod(strField1, NULL)*60;	
    }
    else
    {
    	 time_t t1, t2;
    	 
    	 t1=convertStringDateToTime(strField1);
    	 t2=convertStringDateToTime(strField2);
    	 
    	 if ( !strcmp(strOp, "-") )
    	     *dRes=t1-t2;    
    	 else if ( !strcmp(strOp, "+") )
    	     *dRes=t1+t2;    
    	 if ( !strcmp(strOp, "/") )
    	     *dRes=t1/t2;    
    	 if ( !strcmp(strOp, "*") )
    	     *dRes=t1*t2;    
    	 
//    	 double difftime(time_t time1, time_t time0);
    }
 
   dbg(DEBUG, "evaluateDate:: strField1[%s] strOp[%s] strField2[%s] = Value[%lf] Result[%d]", strField1, strOp, strField2, *dRes, iRet);
   return iRet;	
}

int doVCCheck()
{
    int iRet=RC_SUCCESS;
    int iTmp;
    int a=0;
    int iHopoIndex=3;
    int iOrg3Index=2;
    int iDes3Index=1;
    double dRes1=0;
    double dRes2=0;
    
    dbg(DEBUG, "doVCCheck:: VC Conditions [%d]", sMoveConfig.iArrValueCheckCount);
    for (a=0; a<sMoveConfig.iArrValueCheckCount; a++)
    {
    	dRes1=dRes2=0;
    	dbg(DEBUG, "doVCCheck:: VC[%d]: strName[%s] strString[%s]", a, sMoveConfig.arrValueCheck[a].strName, sMoveConfig.arrValueCheck[a].strString);
        dbg(DEBUG, "doVCCheck::VC[%d]: strFlightType[%s] DES3[%s] ORG3[%s] HOPO[%s] strExp1[%s] strOp[%s] strExp2[%s]",
            a, sMoveConfig.arrValueCheck[a].strFlightType, strArrAfttabFieldValues[iDes3Index],
            strArrAfttabFieldValues[iOrg3Index], cgHopo, sMoveConfig.arrValueCheck[a].strExp1, 
            sMoveConfig.arrValueCheck[a].strOp, sMoveConfig.arrValueCheck[a].strExp2);
            
        if ( !strcmp(sMoveConfig.arrValueCheck[a].strFlightType, "A") )
        {
            if ( strcmp(cgHopo, strArrAfttabFieldValues[iDes3Index]) )
            {
            	dbg(DEBUG, "doVCCheck::VC[%d] Skip VC Test", a);
                continue;
            }
        }
        else if ( !strcmp(sMoveConfig.arrValueCheck[a].strFlightType, "D") )
        {
            if ( strcmp(cgHopo, strArrAfttabFieldValues[iOrg3Index]) )
            {
            	dbg(DEBUG, "doVCCheck::VC[%d] Skip VC Test",a);
                continue;       	
            }
        }    
            	
        iTmp=evaluateExp(&sMoveConfig.arrValueCheck[a].sExp1, &dRes1);
        iTmp&=evaluateExp(&sMoveConfig.arrValueCheck[a].sExp2, &dRes2);
        
        if ( iTmp )
        {
            dbg(DEBUG, "doVCCheck::VC[%d] need evaluation",a);
            if ( !strcmp(sMoveConfig.arrValueCheck[a].strOp, ">") )
            {
                if (dRes1>dRes2)
                {
                	strcpy(strArrAfttabError[iArrAfttabErrorCount], sMoveConfig.arrValueCheck[a].strMsg);
                	
                    populateDQCTab(&sDqctab[iDqctabCount], "", gsDqctab.strFlnu, gsDqctab.strType, strArrAfttabError[iArrAfttabErrorCount], 
                                   'F', gsDqctab.strHopo);
                               
                    iArrAfttabErrorCount++;
                    iDqctabCount++;
                
                	iRet=RC_FAIL;
                }    	
            }
            else if ( !strcmp(sMoveConfig.arrValueCheck[a].strOp, ">=") )
            {
                if (dRes1>=dRes2)
                {
                	strcpy(strArrAfttabError[iArrAfttabErrorCount], sMoveConfig.arrValueCheck[a].strMsg);
                	
                    populateDQCTab(&sDqctab[iDqctabCount], "", gsDqctab.strFlnu, gsDqctab.strType, strArrAfttabError[iArrAfttabErrorCount], 
                               'F', gsDqctab.strHopo);
                               
                    iArrAfttabErrorCount++;
                    iDqctabCount++;
                
                	iRet=RC_FAIL;
                }    	
            }
            else if ( !strcmp(sMoveConfig.arrValueCheck[a].strOp, "<") )
            {
                if (dRes1<dRes2)
                {
                	strcpy(strArrAfttabError[iArrAfttabErrorCount], sMoveConfig.arrValueCheck[a].strMsg);
                	
                    populateDQCTab(&sDqctab[iDqctabCount], "", gsDqctab.strFlnu, gsDqctab.strType, strArrAfttabError[iArrAfttabErrorCount], 
                                   'F', gsDqctab.strHopo);
                               
                    iArrAfttabErrorCount++;
                    iDqctabCount++;
                
                	iRet=RC_FAIL;
                }    	
            }
            else if ( !strcmp(sMoveConfig.arrValueCheck[a].strOp, "<=") )
            {
                if (dRes1<=dRes2)
                {
                	strcpy(strArrAfttabError[iArrAfttabErrorCount], sMoveConfig.arrValueCheck[a].strMsg);
                	
                	populateDQCTab(&sDqctab[iDqctabCount], "", gsDqctab.strFlnu, gsDqctab.strType, strArrAfttabError[iArrAfttabErrorCount], 
                                  'F', gsDqctab.strHopo);
                	
                	iArrAfttabErrorCount++;
                	iDqctabCount++;
                	
                	iRet=RC_FAIL;
                }    	
            }
            else if ( !strcmp(sMoveConfig.arrValueCheck[a].strOp, "=") )
            {
                if (dRes1==dRes2)
                {
                	strcpy(strArrAfttabError[iArrAfttabErrorCount], sMoveConfig.arrValueCheck[a].strMsg);
                
                    populateDQCTab(&sDqctab[iDqctabCount], "", gsDqctab.strFlnu, gsDqctab.strType, strArrAfttabError[iArrAfttabErrorCount], 
                                   'F', gsDqctab.strHopo);
                               
                    iArrAfttabErrorCount++;
                    iDqctabCount++;
                
                	iRet=RC_FAIL;
                }    	
            }
            else if ( !strcmp(sMoveConfig.arrValueCheck[a].strOp, "!=") || !strcmp(sMoveConfig.arrValueCheck[a].strOp, "<>") )
            {
                if (dRes1!=dRes2)
                {
                	strcpy(strArrAfttabError[iArrAfttabErrorCount], sMoveConfig.arrValueCheck[a].strMsg);
                	
                    populateDQCTab(&sDqctab[iDqctabCount], "", gsDqctab.strFlnu, gsDqctab.strType, strArrAfttabError[iArrAfttabErrorCount], 
                               'F', gsDqctab.strHopo);                               
                   
                    iArrAfttabErrorCount++;
                    iDqctabCount++;
                
                	iRet=RC_FAIL;
                }    	
            }
            
            	
        }
            
        dbg(DEBUG, "doVCCheck::VC[%d] Verdict [%lf][%s][%lf] Result[%d]", a, dRes1, sMoveConfig.arrValueCheck[a].strOp, dRes2, iRet);           
    }
    
    dbg(DEBUG, "doVCCheck:: VC Conditions [%d], Result [%d]", sMoveConfig.iArrValueCheckCount, iRet);    
    return iRet;	
}

int doBlankCheck(char *strFields)
{
    int iRet=RC_SUCCESS;
    int a=0;
    char strTmp[1024];
    char *strPtr, *strPtr2;
    int iIndex;
 
    dbg(DEBUG, "doBlankCheck:: Check empty fields in [%s]", strFields);
    if ( strFields!=NULL )
    {	
        strcpy(strTmp, strFields);
        strPtr=strtok_r(strTmp, ",", &strPtr2);

        while ( strPtr )
        {
        	dbg(TRACE, "doBlankCheck strPtr: [%s]", strPtr);
    	    
    	    iIndex=getArrTabFieldIndex(strPtr, arrAfttabDBFieldsConfig, iArrAfttabDBFieldsConfigCount);
            if ( iIndex==-1 )
            {
                dbg(TRACE, "doBlankCheck:: Field[%s] not found!", strPtr);	
                iRet=RC_FAIL;
            }
            else
            {
               if ( !strcmp(strArrAfttabFieldValues[iIndex], "") )
               {
                   dbg(DEBUG, "doBlankCheck:: Field[%s] is empty.", strPtr);
                   sprintf(strArrAfttabError[iArrAfttabErrorCount], sMoveConfig.strBlankReason, arrAfttabDBFieldsConfig[iIndex].strFieldDesc);
                        
                   populateDQCTab(&sDqctab[iDqctabCount], "", gsDqctab.strFlnu, gsDqctab.strType, strArrAfttabError[iArrAfttabErrorCount], 
                                  'F', gsDqctab.strHopo);
                               
                   iArrAfttabErrorCount++;
                   iDqctabCount++;
                   iRet=RC_FAIL;
                   
                }
            }

            strPtr=strtok_r(NULL, ",", &strPtr2);	
        }
    }
    dbg(DEBUG, "doBlankCheck:: Check empty fields in [%s] Result[%d]", strFields, iRet);
    return iRet;	
}

void freeStringArray(char **strArr, int iSize)
{
    int a=0;
    
    for (a=0; a<iSize; a++)
    {
    	if ( strArr[a]!=NULL )
    	{
            free(strArr[a]);	
            strArr[a]=NULL;	
        }
    }
    free(strArr);
    strArr=NULL;
}
/*
char **parseString(char *strString, char *strDelimeter, int *iSize)
{
	char **strRet=NULL;
	int iCnt=0;
	char strTmp[4096];
	char *strPtr, *strPtr2;
	
	*iSize=-1;
	
    strcpy(strTmp, strString);
    strPtr=strtok_r(strTmp, strDelimeter, &strPtr2);
    while ( strPtr )
    {
        iCnt++;
        strPtr=strtok_r(NULL, strDelimeter, &strPtr2);	        
    }
   
    strRet=(char **)malloc(iCnt*sizeof(char *));
    if ( strRet )
    {
        iCnt=0;
        strcpy(strTmp, strString);
        strPtr=strtok_r(strTmp, strDelimeter, &strPtr2);
        while ( strPtr )
        {
            strRet[iCnt]=(char *)malloc((strlen(strPtr)+1)*sizeof(char));
            strcpy(strRet[iCnt++], strPtr);
            strPtr=strtok_r(NULL, strDelimeter, &strPtr2);	        
        }

        *iSize=iCnt;
    }
    else
    {
        dbg(TRACE, "parseString:: Cannot allocate strRet");	
    }
    return strRet;
}
*/
char **parseString(char *strString, char *strDelimeter, int *iSize)
{
	char **strRet=NULL;
	char strTmp[1025];
	int iLen=0;
    int iCount=1;
    int a=0;
	
	*iSize=-1;
	for (a=0; a<strlen(strString); a++)
	{
	    if ( strString[a]==strDelimeter[0] )
	        iCount++;	
	}
	
    strRet=(char **)malloc(iCount*sizeof(char *));
    if ( strRet )
    {
    	iCount=0;
    	memset(&strTmp, 0, sizeof(strTmp));
        iLen=0;
        for (a=0; a<strlen(strString); a++)
        {        	
            if ( strString[a]==strDelimeter[0] )
            {
            	strRet[iCount]=(char *)malloc((strlen(strTmp)+1)*sizeof(char));
                strcpy(strRet[iCount++], strTmp);
                iLen=0;	
                memset(&strTmp, 0, sizeof(strTmp));
            }
            else
                strTmp[iLen++]=strString[a];
        }
        strRet[iCount]=(char *)malloc((strlen(strTmp)+1)*sizeof(char));
        strcpy(strRet[iCount++], strTmp);
        *iSize=iCount;
    }
    else
    {
        dbg(TRACE, "parseString:: Cannot allocate strRet");	
    }

    return strRet;
}

int printAfttabErrors()
{
     int iRet=0;
     int a=0;
     
     for (a=0; a<iDqctabCount; a++)
     {
         dbg(TRACE,"Insert in DQCTAB [%d] FLNU[%s], TYPE[%s], REAS[%s]",
             a+1, sDqctab[a].strFlnu, sDqctab[a].strType, sDqctab[a].strReas);	
     }

     return iRet;	
}

int updateFlightDQCS(char *strDqcs, char *strUrNo, char *strCmd)
{
    int iRet=RC_SUCCESS;
    short slActCursor;
    char strSql[1024];
    char strErr[513];
    char strWhere[257];
    
    dbg(TRACE, "updateFlightDQCS:: strDqcs[%s], strUrNo[%s], strCmd[%s]", strDqcs, strUrNo,strCmd);
/*    
    if ( !strcmp(sMoveConfig.strAfttabDQCSUpdate, "YES") )
    { 	
    	sprintf(strWhere, "WHERE URNO='%s' ", strUrNo); 
        dbg(DEBUG, "updateFlightDQCS:: Send to Flight: <7800><flight><DQAHDL><%s><AFTTAB> strWhere[%s] DQCS[%s]",
            strCmd, strWhere, strDqcs);
        iRet = SendCedaEvent(7800,0,"flight","DQAHDL",pcgTwStart,pcgTwEnd,strCmd,"AFTTAB", strWhere,"DQCS",strDqcs,"",3,NETOUT_NO_ACK);
    }
*/
    if ( !strcmp(sMoveConfig.strAfttabDQCSUpdate, "YES") )
    { 	
    	sprintf(strWhere, "WHERE URNO='%s' ", strUrNo); 
        dbg(TRACE, "updateFlightDQCS:: Send to Flight: <%d><flight><DQAHDL><DQC><AFTTAB> strWhere[%s] DQCS[%s]",
            igFlightQueue,strWhere, strDqcs);
        iRet = SendCedaEvent(igFlightQueue,0,"flight","DQAHDL",pcgTwStart,pcgTwEnd,"DQC","AFTTAB", strWhere,"DQCS",strDqcs,"",3,NETOUT_NO_ACK);
    }
    
    else
        dbg(TRACE, "updateFlightDQCS:: strAfttabDQCSUpdate[%s]... No data change", sMoveConfig.strAfttabDQCSUpdate);

    dbg(DEBUG, "updateFlightDQCS:: strDqcs[%s], strUrNo[%s], strCmd[%s], Result[%d]", strDqcs, strUrNo,strCmd, iRet);   
    return iRet;	
}

void populateDQCTab(DQCTAB *sDQ, char *strUrno, char *strFlnu, char *strType, char *strReas, char cPass, char *strHopo)
{
    strcpy(sDQ->strUrno, strUrno);
    strcpy(sDQ->strFlnu, strFlnu);
    strcpy(sDQ->strType, strType);
    strcpy(sDQ->strReas, strReas);
    sDQ->cPass=cPass;
    strcpy(sDQ->strHopo, strHopo);
    return;
}

int deleteDQCTab(char *strFlnu, char *strType)
{
    int iRet=RC_SUCCESS;
    char strSql[1025];
    short slActCursor;
    char data_area[1024];
    char strErr[512];
    unsigned long ilBufLen = 512;	
	unsigned long ilErrLen = 512;	
	  
    dbg(TRACE,"deleteDQCTab:: strFlnu[%s] strType[%s]", strFlnu, strType);
    	        
	if ( !strcmp(sMoveConfig.strAfttabDQCSUpdate, "YES") )
	{
	    memset(strErr, 0, sizeof(strErr));
	            	
        sprintf(strSql, "DELETE FROM dqctab "
                "WHERE flnu='%s' ", strFlnu);
 
        if ( strcmp(strType,"") )
            sprintf(strSql, "%s AND TYPE='%s' ", strSql, strType);
 
       
        iRet = sql_if(START,&slActCursor,strSql,data_area);
        if ( iRet!=RC_SUCCESS )
        {
            sqlglm(strErr,&ilBufLen,&ilErrLen);
            strErr[ilErrLen]=0;
        
            if ( !strncmp(strErr, "ORA-01403:", 10) )
                iRet=RC_SUCCESS;
        }
        commit_work();
        close_my_cursor(&slActCursor);
        dbg(DEBUG,"deleteDQCTab:: [%s][%d][%s]", strSql, iRet, strErr);
    }
    else
        dbg(TRACE, "deleteDQCTab:: strAfttabDQCSUpdate[%s]... No data change", sMoveConfig.strAfttabDQCSUpdate);
    
    dbg(TRACE,"deleteDQCTab:: strFlnu[%s] strType[%s], iRet[%d]", strFlnu, strType, iRet);
    return iRet;	
}

int insertDQCTab(char *strCmd, int iFlag)
{
    int iRet=RC_SUCCESS;
    char strSql[2048];
    char strErr[513];
    char pclValueList[2000];
    static char pcgLoaBuf[1000*512];
    char *pclDQCTABFieldList="URNO,FLNU,TYPE,REAS,PASS,HOPO";
	char *pclDQCTABValueList=":VURNO,:VFLNU,:VTYPE,:VREAS,:VPASS,:VHOPO";

    int igArrayPtr;
    int a=0;
    int iUrno;
  
    dbg(DEBUG, "insertDQCTab:: strAfttabDQCSUpdate[%s]", sMoveConfig.strAfttabDQCSUpdate);
    
    memset(pclValueList, 0, sizeof(pclValueList));
    memset(pcgLoaBuf, 0, sizeof(pcgLoaBuf));
    igArrayPtr = 0;
    
    if ( !strcmp(sMoveConfig.strAfttabDQCSUpdate, "YES") )
    {	
        if ( iDqctabCount )
    	{
    	    iUrno=NewUrnos("DQCTAB",iDqctabCount);
    		if ( iUrno!=-1 )
    		{
                for (a=0; a<iDqctabCount; a++)
                {
                	/*
                    sprintf(strSql, "INSERT INTO dqctab	"
                                    "       (urno, flnu, type, reas, pass, hopo) "
                                    "VALUES ('%d', '%s', '%s', '%s', '%c', '%s') ",
                                    iUrno, sDqctab[a].strFlnu, sDqctab[a].strType, sDqctab[a].strReas, sDqctab[a].cPass, cgHopo );
                
                    memset(strErr, 0, sizeof(char)*513);
                    iRet=executeSql(strSql,strErr); 
                    */             	        
                	sprintf(pclValueList, "%d,%s,%s,%s,%c,%s",
                	        iUrno, sDqctab[a].strFlnu, sDqctab[a].strType, sDqctab[a].strReas, sDqctab[a].cPass, cgHopo);
                	StrgPutStrg(pcgLoaBuf,&igArrayPtr,pclValueList,0,-1,"\n");

                	iUrno++;
                }
                
                if (strlen(pcgLoaBuf) > 0)
				{
				    igArrayPtr--;
				    pcgLoaBuf[igArrayPtr] = 0;
				}
				  
                iRet = sqlArrayInsert(pclDQCTABFieldList,pclDQCTABValueList,pcgLoaBuf);

                if ( igBroadCast && iFlag )
                { 
                    dbg(DEBUG, "insertDQCTab:: Send to Broadcast: igToBcHdl[%d], pcgDestName[%s], pcgRecvName[%s], strCmd[%s] Table[%s], strFlnu[%s]",
                        igToBcHdl, pcgDestName, pcgRecvName, "DQC", "DQCTAB", sDqctab[0].strFlnu);
                    (void) tools_send_info_flag (igToBcHdl, 0, pcgDestName, "", pcgRecvName, "", "", pcgTwStart, pcgTwEnd,
                                                "DQC", "DQCTAB", sDqctab[0].strFlnu,
                                                "FLNU", sDqctab[0].strFlnu, 0);
                }
            }   
            else
            {
                dbg(TRACE, "insertDQCTab:: ERROR!!! Cannot generate URNO ");	
                iRet=RC_FAIL;
            }        
        }
    }
    else
        dbg(TRACE, "insertDQCTab:: strAfttabDQCSUpdate[%s]... No data change", sMoveConfig.strAfttabDQCSUpdate);
    return iRet;	
}

static int sqlArrayInsert(char *pcpFldList, char *pcpValList, char *pcpDatList)
{
    int ilRC = RC_SUCCESS;
    int ilRCdb = DB_SUCCESS;
    char pclSqlBuf[1024];
    short slFkt = 0;
    short slCursor = 0;
    REC_DESC rgRecDesc;
    
    dbg(DEBUG,"sqlArrayInsert:: Start ");
    dbg(DEBUG,"sqlArrayInsert:: pcpFldList[%s], pcpValList[%s] ", pcpFldList, pcpValList);
    dbg(DEBUG,"sqlArrayInsert:: pcpDatList[%s] ", pcpDatList);
    
    sprintf(pclSqlBuf,"INSERT INTO DQCTAB (%s) VALUES (%s)",pcpFldList,pcpValList);
    dbg(DEBUG, "sqlArrayInsert:: SQL[%s]", pclSqlBuf);
    
	InitRecordDescriptor(&rgRecDesc,1024,0,FALSE);
    ilRCdb = SqlIfArray(slFkt,&slCursor,pclSqlBuf,pcpDatList,0,&rgRecDesc);
    if (ilRCdb == DB_SUCCESS)
        commit_work();
    else
        rollback();

    close_my_cursor(&slCursor);
    
    dbg(DEBUG,"sqlArrayInsert:: Result[%d] ", ilRCdb);
    return ilRCdb;
}

int executeSql(char *strSql, char *strErr)
{
    int iRet=0;
    short	slActCursor;
    char data_area[1024];
    unsigned long ilBufLen = 512;	/* modified int -> long as defined */
	unsigned long ilErrLen = 0;	/* modified int -> long as defined */

    iRet = sql_if(START,&slActCursor,strSql,data_area);
    if ( iRet==RC_FAIL )
    {
        sqlglm(strErr,&ilBufLen,&ilErrLen);
        strErr[ilErrLen]=0;
    } 
    commit_work();        
    close_my_cursor(&slActCursor);    
    dbg(DEBUG, "executeSql:: strSql [%s] Error[%s] Result[%d]", strSql, strErr, iRet);     
    return iRet;	
}

int addArrDBFields(char *str, char *strDBTableName, DBFIELDSCONFIG *sDB, char **strFieldNames)
{
	int iRet=RC_SUCCESS;
	char *strPtr;
	char *strRealloc;
	
	dbg(TRACE,"addArrDBFields:: str[%s]", str);
    iRet=getDBFieldDesc(str, strDBTableName, &strPtr);
    if ( iRet==RC_SUCCESS )
    {
        strcpy(sDB->strFieldName, str); 
        sDB->strFieldDesc=strPtr; 
        strRealloc=(char *)realloc(*strFieldNames, sizeof(char)*(strlen(*strFieldNames)+6));
        if ( strRealloc )
        {
        	*strFieldNames=strRealloc;
            sprintf(*strFieldNames, "%s,%s", *strFieldNames, str); 
        }
        else
        {
            dbg(TRACE, "addArrDBFields:: Cannot reallocate strFieldNames");
            iRet=RC_FAIL;
        }
    }
    else
    {
        dbg(TRACE, "addArrDBFields:: ERROR!!! getDBFieldDesc() failed");
    }
	dbg(DEBUG,"addArrDBFields:: str[%s] Result[%d]", str, iRet);    
    return iRet; 		
}

int getConfig(char *strFile)
{
    int iRet=RC_SUCCESS;
    int iTmp;
    int a=0;
    int iFirst=0;
    char strTmp[1025];
    char strVC[1025];
    char *strPtr, *strPtr2;
    char *strPtr3, *strPtr4;
    char *strRealloc;
    DBFIELDSCONFIG *sDB;
    DBFIELDSCONFIG *sDBFieldsConfigRealloc;
    FIELDCHECK *sFieldCheckRealloc;
    VALUECHECK *sValueCheckRealloc;
    LOADHEADER *sLoadHeaderRealloc;
    LOADVALUECHECK *sLoadVCRealloc;
    LOADBLANK *sLoadBlankRealloc;


    dbg(TRACE,"---   int getConfig(char *strFile) ---");	
    
    memset(&sMoveConfig, 0, sizeof(MOVECONFIG));
    memset(&sLoadConfig, 0, sizeof(LOADCONFIG));
    memset(&rgDQC, 0, sizeof(DQC));
    
    iArrAfttabDBFieldsConfigCount=0;
    
    arrAfttabDBFieldsConfig=(DBFIELDSCONFIG *)malloc(sizeof(DBFIELDSCONFIG)*9); 
    if ( arrAfttabDBFieldsConfig )
    {
        memset(arrAfttabDBFieldsConfig, 0, sizeof(DBFIELDSCONFIG)*9);
 
        strcpy(arrAfttabDBFieldsConfig[0].strFieldName, "URNO");    
        iRet=getDBFieldDesc(arrAfttabDBFieldsConfig[0].strFieldName, "AFT", &arrAfttabDBFieldsConfig[0].strFieldDesc);
        iArrAfttabDBFieldsConfigCount++;
    
        if ( iRet==RC_SUCCESS )
        {
            strcpy(arrAfttabDBFieldsConfig[1].strFieldName, "DES3");    
            iRet=getDBFieldDesc(arrAfttabDBFieldsConfig[1].strFieldName, "AFT", &arrAfttabDBFieldsConfig[1].strFieldDesc);
            iArrAfttabDBFieldsConfigCount++;
        }
        
        if ( iRet==RC_SUCCESS )
        { 
            strcpy(arrAfttabDBFieldsConfig[2].strFieldName, "ORG3");    
            iRet=getDBFieldDesc(arrAfttabDBFieldsConfig[2].strFieldName, "AFT", &arrAfttabDBFieldsConfig[2].strFieldDesc);
            iArrAfttabDBFieldsConfigCount++;
        }
        
        if ( iRet==RC_SUCCESS )
        {
            strcpy(arrAfttabDBFieldsConfig[3].strFieldName, "HOPO");    
            iRet=getDBFieldDesc(arrAfttabDBFieldsConfig[3].strFieldName, "AFT", &arrAfttabDBFieldsConfig[3].strFieldDesc);
            iArrAfttabDBFieldsConfigCount++; 
        }
        
        if ( iRet==RC_SUCCESS )
        {
            strcpy(arrAfttabDBFieldsConfig[4].strFieldName, "DQCS");    
            iRet=getDBFieldDesc(arrAfttabDBFieldsConfig[4].strFieldName, "AFT", &arrAfttabDBFieldsConfig[4].strFieldDesc);
            iArrAfttabDBFieldsConfigCount++;
        }
        if ( iRet==RC_SUCCESS )
        {
            strcpy(arrAfttabDBFieldsConfig[5].strFieldName, "RKEY");    
            iRet=getDBFieldDesc(arrAfttabDBFieldsConfig[5].strFieldName, "AFT", &arrAfttabDBFieldsConfig[5].strFieldDesc);
            iArrAfttabDBFieldsConfigCount++;
        }
        
        if ( iRet==RC_SUCCESS )
        {
            strcpy(arrAfttabDBFieldsConfig[6].strFieldName, "FTYP");    
            iRet=getDBFieldDesc(arrAfttabDBFieldsConfig[6].strFieldName, "AFT", &arrAfttabDBFieldsConfig[6].strFieldDesc);
            iArrAfttabDBFieldsConfigCount++;
        }

        if ( iRet==RC_SUCCESS )
        {
            strcpy(arrAfttabDBFieldsConfig[7].strFieldName, "TIFA");    
            iRet=getDBFieldDesc(arrAfttabDBFieldsConfig[7].strFieldName, "AFT", &arrAfttabDBFieldsConfig[7].strFieldDesc);
            iArrAfttabDBFieldsConfigCount++;
        }
        if ( iRet==RC_SUCCESS )
        {
            strcpy(arrAfttabDBFieldsConfig[8].strFieldName, "TIFD");    
            iRet=getDBFieldDesc(arrAfttabDBFieldsConfig[8].strFieldName, "AFT", &arrAfttabDBFieldsConfig[8].strFieldDesc);
            iArrAfttabDBFieldsConfigCount++;
        }
    }
    else
    {
        dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate arrAfttabDBFieldsConfig (URNO, DES3, ORG3, DQCS, RKEY, FTYP)");	
        iRet=RC_FAIL;
    }
    
    if ( iRet==RC_SUCCESS )
    {
        strAfttabFieldNames=(char *)malloc(sizeof(char)*45);
        if ( strAfttabFieldNames )
            strcpy(strAfttabFieldNames, "URNO,DES3,ORG3,HOPO,DQCS,RKEY,FTYP,TIFA,TIFD");
        else
        {
    	    dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate strAfttabFieldNames (URNO, DES3, ORG3, HOPO, DQCS, RKEY,FTYP)");
    	    iRet=RC_FAIL;
    	}
    }
    
    if ( iRet==RC_SUCCESS )
    {    	
        iTmp=iGetConfigEntry(strFile, "MAIN", "BC_TIM_GAP", CFG_STRING, strTmp);
        if (( iTmp == RC_SUCCESS ) && (strlen(strTmp) > 0))
             igTimGapSec = atoi(strTmp) * 60; /* change Min to Sec */
    	dbg(DEBUG, "BroadCast Time Window <%d>", igTimGapSec);

        iTmp=iGetConfigEntry(strFile, "MAIN", "DQC_READ_ENABLE", CFG_STRING, strTmp);
        if (( iTmp == RC_SUCCESS ) && (strcmp(strTmp, "YES") == 0))
        {    
    	    dbg(DEBUG, "DQC Read Enable");
            igDqcRead = TRUE;
        }
        else
            igDqcRead = FALSE;
    }
 
 
    if ( iRet==RC_SUCCESS )
    {    	
        iTmp=iGetConfigEntry(strFile, "FLIGHT", "STATUS_UPD", CFG_STRING, strTmp);
        if ( iTmp!=RC_SUCCESS )
            strcpy(strTmp, "NO");    
 
        sMoveConfig.strAfttabDQCSUpdate=(char *)malloc(sizeof(char)*(strlen(strTmp)+1));  
        if ( sMoveConfig.strAfttabDQCSUpdate )
            strcpy(sMoveConfig.strAfttabDQCSUpdate, strTmp);
        else
        {
            dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sMoveConfig.strAfttabDQCSUpdate"); 
            iRet=RC_FAIL;
        }
    }
    
    if ( iRet==RC_SUCCESS )
    {    	
        iTmp=iGetConfigEntry(strFile, "FLIGHT", "FLIGHT_QUEUE", CFG_STRING, strTmp);
        if ( iTmp==RC_SUCCESS )
            igFlightQueue=atoi(strTmp);
    }
    
    if ( iRet==RC_SUCCESS )
    {    	
        iTmp=iGetConfigEntry(strFile, "FLIGHT", "DQA_CMD", CFG_STRING, strTmp);
        if ( iTmp==RC_SUCCESS )
            sprintf(pcgDqaCmd, ",%s,", strTmp);
        else
        	memset(pcgDqaCmd,0,sizeof(pcgDqaCmd));
    }
    
    if ( iRet==RC_SUCCESS )
    {    	
        iTmp=iGetConfigEntry(strFile, "FLIGHT", "MOVE_CHECK_CMD", CFG_STRING, strTmp);
        if ( iTmp==RC_SUCCESS )
            sprintf(pcgMoveCmd,",%s,", strTmp);
        else
        	strcpy(pcgMoveCmd, pcgDqaCmd);
    }
    
    if ( iRet==RC_SUCCESS )
    {    	
        iTmp=iGetConfigEntry(strFile, "FLIGHT", "LOAD_CHECK_CMD", CFG_STRING, strTmp);
        if ( iTmp==RC_SUCCESS )
            sprintf(pcgLoadCmd, ",%s,", strTmp);
        else
        	strcpy(pcgLoadCmd, pcgDqaCmd);
    }  
    
    if ( iRet==RC_SUCCESS )
    {
        iTmp=readConfigFile(strFile, "DQ_CHECK", "CONDITIONS", strTmp);
        if ( strlen(strTmp)==0 )
        {
            rgDQC.strConditions=NULL;
            rgDQC.sFieldCheck=NULL;
            rgDQC.iFieldCheck=0;
        }     
        else
        {
        	char strCheck[1025];
        	
            rgDQC.strConditions=(char *)malloc(sizeof(char)*(strlen(strTmp)+1));
            if ( rgDQC.strConditions )
            {
                strcpy(rgDQC.strConditions, strTmp);	

                rgDQC.iFieldCheck=0;
                strPtr=strtok_r(strTmp, ",", &strPtr2);

                while (strPtr)
                {   
            	    if ( strcmp(strPtr, " ") )
            	        trim(strPtr, ' ');

            	    iTmp=readConfigFile(strFile, "DQ_CHECK", strPtr, strCheck);
        	        if ( iTmp==RC_SUCCESS )
        	        {        	    	
        	            sFieldCheckRealloc=(FIELDCHECK *)realloc(rgDQC.sFieldCheck, sizeof(FIELDCHECK)*(rgDQC.iFieldCheck+1));    
        	            if ( sFieldCheckRealloc )          
        	            { 
        	    	        rgDQC.sFieldCheck=sFieldCheckRealloc;
                            memset(&rgDQC.sFieldCheck[rgDQC.iFieldCheck], 0, sizeof(FIELDCHECK));
                            iRet=getFieldCheck(strFile, "DQ_CHECK", "AFT", strPtr, &rgDQC.sFieldCheck[rgDQC.iFieldCheck]);
                            rgDQC.iFieldCheck++;
                        }
                        else
                        {
                            dbg(TRACE, "getConfig:: Cannot reallocate rgDQC->sFieldCheck"); 
                            iRet=RC_FAIL;
                        }                        
                    }
                    if ( iRet==RC_FAIL )
                        break;
                    strPtr=strtok_r(NULL, ",", &strPtr2);
                }
            }
            else
            {
                dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate rgDQC.strConditions");
                iRet=RC_FAIL;		
            }        	
        }       

    }
             
    if ( iRet==RC_SUCCESS )
    {               
        iTmp=readConfigFile(strFile, "AFTTAB_CHK", "TYPE", strTmp);
        sMoveConfig.strType=(char *)malloc(sizeof(char)*(strlen(strTmp)+1));  
        if ( sMoveConfig.strType ) 
            strcpy(sMoveConfig.strType, strTmp);
        else
        {
    	    dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sMoveConfig.strType");   
    	    iRet=RC_FAIL;
    	}  
    }

    if ( iRet==RC_SUCCESS )
    {
        iTmp=readConfigFile(strFile, "AFTTAB_CHK", "CONDITIONS", strTmp);
        if ( strlen(strTmp)==0 )
        {
            sMoveConfig.strConditions=NULL;
            sMoveConfig.sFieldCheck=NULL;
            sMoveConfig.iFieldCheck=0;
        }     
        else
        {
        	char strCheck[1025];
        	
            sMoveConfig.strConditions=(char *)malloc(sizeof(char)*(strlen(strTmp)+1));
            if ( sMoveConfig.strConditions )
            {
                strcpy(sMoveConfig.strConditions, strTmp);	

                sMoveConfig.iFieldCheck=0;
                strPtr=strtok_r(strTmp, ",", &strPtr2);

                while (strPtr)
                {   
            	    if ( strcmp(strPtr, " ") )
            	        trim(strPtr, ' ');

            	    iTmp=readConfigFile(strFile, "AFTTAB_CHK", strPtr, strCheck);
        	        if ( iTmp==RC_SUCCESS )
        	        {        	    	
        	            sFieldCheckRealloc=(FIELDCHECK *)realloc(sMoveConfig.sFieldCheck, sizeof(FIELDCHECK)*(sMoveConfig.iFieldCheck+1));    
        	            if ( sFieldCheckRealloc )          
        	            { 
        	    	        sMoveConfig.sFieldCheck=sFieldCheckRealloc;
                            memset(&sMoveConfig.sFieldCheck[sMoveConfig.iFieldCheck], 0, sizeof(FIELDCHECK));
                            iRet=getFieldCheck(strFile, "AFTTAB_CHK", "AFT", strPtr, &sMoveConfig.sFieldCheck[sMoveConfig.iFieldCheck]);
                            sMoveConfig.iFieldCheck++;
                        }
                        else
                        {
                            dbg(TRACE, "getConfig:: Cannot reallocate sMoveConfig->sFieldCheck"); 
                            iRet=RC_FAIL;
                        }
                        
                    }
                    if ( iRet==RC_FAIL )
                        break;
                    strPtr=strtok_r(NULL, ",", &strPtr2);
                }
            }
            else
            {
                dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sMoveConfig.strConditions");
                iRet=RC_FAIL;		
            }        	
        }       

    }

    if ( iRet==RC_SUCCESS )
    {
        iTmp=readConfigFile(strFile, "AFTTAB_CHK", "BLANK_REAS", strTmp);
        sMoveConfig.strBlankReason=(char *)malloc(sizeof(char)*(strlen(strTmp)+1));  
        if ( sMoveConfig.strBlankReason )
            strcpy(sMoveConfig.strBlankReason, strTmp);
        else
        {
            dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sMoveConfig.strBlankReason");  
            iRet=RC_FAIL;
        }
    }
    
    if ( iRet==RC_SUCCESS )
    {       
        sMoveConfig.iArrAllBlankCheckCount=0;
        iFirst=1;
        iTmp=readConfigFile(strFile, "AFTTAB_CHK", "ALL_BLANK_CHECK", strTmp);    
        strPtr=strtok_r(strTmp, ",", &strPtr2);        
        while ( strPtr )
        {
        	if ( strcmp(strPtr, " ") )
        	    trim(strPtr, ' ');
            if ( strlen(strPtr)==4 )
            {
                if ( iFirst )
                {
                    iFirst=0;
                    sMoveConfig.strAllBlankCheck=(char *)malloc(sizeof(char)*5);
                    if ( sMoveConfig.strAllBlankCheck )
                        strcpy(sMoveConfig.strAllBlankCheck, strPtr);	
                    else                             
                    {
                    	dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sMoveConfig.strAllBlankCheck"); 
                    	iRet=RC_FAIL;
                    }
                }
                else
                {
                	strRealloc=(char *)realloc(sMoveConfig.strAllBlankCheck, (sizeof(char)*(strlen(sMoveConfig.strAllBlankCheck)+6)));
                	if ( strRealloc )
                	{
                		sMoveConfig.strAllBlankCheck=strRealloc;
                  	    sprintf(sMoveConfig.strAllBlankCheck, "%s,%s", sMoveConfig.strAllBlankCheck, strPtr);
                  	}
                  	else
                  	{
                  	    dbg(TRACE, "getConfig:: ERROR!!! Cannot reallocate sMoveConfig.strAllBlankCheck");	 
                  	    iRet=RC_FAIL;
                  	}
                }
                
                if ( iRet==RC_SUCCESS )
                {
                    if ( getArrTabFieldIndex(strPtr, arrAfttabDBFieldsConfig, iArrAfttabDBFieldsConfigCount)==-1 )
                    {
                    	sDB=(DBFIELDSCONFIG *)malloc(sizeof(DBFIELDSCONFIG));
                    	if ( sDB )
                    	{
                    	    memset(sDB, 0, sizeof(DBFIELDSCONFIG));
                    	    iTmp=addArrDBFields(strPtr, "AFT", sDB, &strAfttabFieldNames);
                    	    if ( iTmp==RC_SUCCESS )
                    	    {
                                sDBFieldsConfigRealloc=(DBFIELDSCONFIG *)realloc(arrAfttabDBFieldsConfig, sizeof(DBFIELDSCONFIG)*(iArrAfttabDBFieldsConfigCount+1)); 
                                if ( sDBFieldsConfigRealloc )
                                {
                            	    arrAfttabDBFieldsConfig=sDBFieldsConfigRealloc;
                                    memset(&arrAfttabDBFieldsConfig[iArrAfttabDBFieldsConfigCount], 0, sizeof(DBFIELDSCONFIG));             
                                    arrAfttabDBFieldsConfig[iArrAfttabDBFieldsConfigCount]=*sDB;
                                    iArrAfttabDBFieldsConfigCount++;  
                                }          	
                                else
                                {
                                    dbg(TRACE, "getConfig:: ERROR!!! Cannot reallocate arrAfttabDBFieldsConfig for All Blank Check");
                                    iRet=RC_FAIL;	
                                }	
                    	    }	
                    	    else
                    	    {
                                dbg(TRACE, "getConfig:: ERROR!!! All Blank Check addArrDBFields Failed");	 
                  	            iRet=RC_FAIL;	
                    	    }
               	            free(sDB);
               	            sDB=NULL;
               	        }
               	        else
               	        {
               	            dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sDB for All Blank Check");	
               	            iRet=RC_FAIL;
               	        }
               	        	
                    }
                }                    
            }
            else 
            {
                sFieldCheckRealloc=(FIELDCHECK *)realloc(sMoveConfig.arrAllBlankCheck, sizeof(FIELDCHECK)*(sMoveConfig.iArrAllBlankCheckCount+1));   
                if ( sFieldCheckRealloc )
                {      
                	sMoveConfig.arrAllBlankCheck =sFieldCheckRealloc;     
                    memset(&sMoveConfig.arrAllBlankCheck[sMoveConfig.iArrAllBlankCheckCount], 0, sizeof(FIELDCHECK));
                    iRet=getFieldCheck(strFile, "AFTTAB_CHK", "AFT", strPtr, &sMoveConfig.arrAllBlankCheck[sMoveConfig.iArrAllBlankCheckCount]);
                    sMoveConfig.iArrAllBlankCheckCount++;       
                }   
                else
                {
                    dbg(TRACE, "getConfig:: ERROR!!! Cannot reallocate arrAllBlankCheck for All Blank Check");	
                    iRet=RC_FAIL;
                }  
            }
            
            if ( iRet==RC_FAIL )
                break;
            strPtr=strtok_r(NULL, ",", &strPtr2);	
        }
    }
    
    if ( iRet==RC_SUCCESS )
    {
    	sMoveConfig.iArrArrBlankCheckCount=0;
        iFirst=1;
        iTmp=readConfigFile(strFile, "AFTTAB_CHK", "ARR_BLANK_CHECK", strTmp);
        strPtr=strtok_r(strTmp, ",", &strPtr2);
        while ( strPtr )
        {
        	if ( strcmp(strPtr, " ") )
        	    trim(strPtr, ' ');
        	    
            if ( strlen(strPtr)==4 )
            {
                if ( iFirst )
                {
                    iFirst=0;
                    sMoveConfig.strArrBlankCheck=(char *)malloc(sizeof(char)*5);
                    if ( sMoveConfig.strArrBlankCheck )
                        strcpy(sMoveConfig.strArrBlankCheck, strPtr);	
                    else
                    {
                    	dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sMoveConfig.strArrBlankCheck");	
                    	iRet=RC_FAIL;
                    }
                }
                else
                {
                	strRealloc=(char *)realloc(sMoveConfig.strArrBlankCheck, (sizeof(char) * (strlen(sMoveConfig.strArrBlankCheck)+6)));
                	if ( strRealloc )
                	{
                		sMoveConfig.strArrBlankCheck=strRealloc;
                  	    sprintf(sMoveConfig.strArrBlankCheck, "%s,%s", sMoveConfig.strArrBlankCheck, strPtr);
                  	}
                  	else
                  	{
                  	    dbg(TRACE, "getConfig:: ERROR!!! Cannot reallocate sMoveConfig.strArrBlankCheck");	
                  	    iRet=RC_FAIL;
                  	}
                }
                
                if ( iRet==RC_SUCCESS )
                {
                    if ( getArrTabFieldIndex(strPtr, arrAfttabDBFieldsConfig, iArrAfttabDBFieldsConfigCount)==-1 )
                    {
                    	sDB=(DBFIELDSCONFIG *)malloc(sizeof(DBFIELDSCONFIG));
                    	if ( sDB )
                    	{
                    	    memset(sDB, 0, sizeof(DBFIELDSCONFIG));
                    	    iTmp=addArrDBFields(strPtr, "AFT", sDB, &strAfttabFieldNames);
                    	    if ( iTmp==RC_SUCCESS )
                    	    {
                                sDBFieldsConfigRealloc=(DBFIELDSCONFIG *)realloc(arrAfttabDBFieldsConfig, sizeof(DBFIELDSCONFIG)*(iArrAfttabDBFieldsConfigCount+1)); 
                                if ( sDBFieldsConfigRealloc )
                                {
                            	    arrAfttabDBFieldsConfig=sDBFieldsConfigRealloc;
                                    memset(&arrAfttabDBFieldsConfig[iArrAfttabDBFieldsConfigCount], 0, sizeof(DBFIELDSCONFIG));             
                                    arrAfttabDBFieldsConfig[iArrAfttabDBFieldsConfigCount]=*sDB;
                                    iArrAfttabDBFieldsConfigCount++; 
                                }
                                else
                                {
                                    dbg(TRACE, "getConfig:: ERROR!!! Cannot reallocate arrAfttabDBFieldsConfig for Arr Blank Check");
                                    iRet=RC_FAIL;
                                }                               		
                    	    }	
               	            free(sDB);
               	            sDB=NULL;
               	        }
               	        else
               	        {
               	            dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sDB for Arr Blank Check"); 
               	            iRet=RC_FAIL;	
               	        }
                    }
                }
            }
            else 
            {
                sFieldCheckRealloc=(FIELDCHECK *)realloc(sMoveConfig.arrArrBlankCheck, sizeof(FIELDCHECK)*(sMoveConfig.iArrArrBlankCheckCount+1));  	
                if ( sFieldCheckRealloc )
                {       
                	sMoveConfig.arrArrBlankCheck=sFieldCheckRealloc;      
                    memset(&sMoveConfig.arrArrBlankCheck[sMoveConfig.iArrArrBlankCheckCount], 0, sizeof(FIELDCHECK));
                    iRet=getFieldCheck(strFile, "AFTTAB_CHK", "AFT", strPtr, &sMoveConfig.arrArrBlankCheck[sMoveConfig.iArrArrBlankCheckCount]);            
                    sMoveConfig.iArrArrBlankCheckCount++;  
                }   
                else
                {
                    dbg(TRACE, "getConfig:: Cannot reallocate arrArrBlankCheck for Arr Blank Check");	
                    iRet=RC_FAIL;
                }        
            }
            
            if ( iRet==RC_FAIL )
                 break;
            strPtr=strtok_r(NULL, ",", &strPtr2);	
        }
    }
    
    if ( iRet==RC_SUCCESS )
    {
    	sMoveConfig.iArrDepBlankCheckCount=0;
        iFirst=1;
        iTmp=readConfigFile(strFile, "AFTTAB_CHK", "DEP_BLANK_CHECK", strTmp);
        strPtr=strtok_r(strTmp, ",", &strPtr2);
        while ( strPtr )
        {
        	if ( strcmp(strPtr, " ") )
        	    trim(strPtr, ' ');
        	
        	if ( strlen(strPtr)==4 )
            {
                if ( iFirst )
                {
                    iFirst=0;
                    sMoveConfig.strDepBlankCheck=(char *)malloc(sizeof(char)*5);
                    if ( sMoveConfig.strDepBlankCheck )
                        strcpy(sMoveConfig.strDepBlankCheck, strPtr);	
                    else
                    {
                        dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sMoveConfig.strDepBlankCheck");
                        iRet=RC_FAIL;
                    }
                }
                else
                {
                	strRealloc=(char *)realloc(sMoveConfig.strDepBlankCheck, (sizeof(char)*(strlen(sMoveConfig.strDepBlankCheck)+6)));
                	if ( strRealloc )
                	{
                	    sMoveConfig.strDepBlankCheck=strRealloc;
                	    sprintf(sMoveConfig.strDepBlankCheck, "%s,%s", sMoveConfig.strDepBlankCheck, strPtr);
                	}
                	else
                	{
                	    dbg(TRACE, "getConfig:: ERROR!!! Cannot reallocate sMoveConfig.strDepBlankCheck");	
                	    iRet=RC_FAIL;
                	}
                }  
                
                if ( iRet==RC_SUCCESS )
                {
                    if ( getArrTabFieldIndex(strPtr, arrAfttabDBFieldsConfig, iArrAfttabDBFieldsConfigCount)==-1 )
                    {
                        sDB=(DBFIELDSCONFIG *)malloc(sizeof(DBFIELDSCONFIG));
                        if ( sDB )
                        {
                            memset(sDB, 0, sizeof(DBFIELDSCONFIG));
                            iTmp=addArrDBFields(strPtr, "AFT", sDB, &strAfttabFieldNames);
                            if ( iTmp==RC_SUCCESS )
                            {
                                sDBFieldsConfigRealloc=(DBFIELDSCONFIG *)realloc(arrAfttabDBFieldsConfig, sizeof(DBFIELDSCONFIG)*(iArrAfttabDBFieldsConfigCount+1)); 
                                if ( sDBFieldsConfigRealloc )
                                {
                                    arrAfttabDBFieldsConfig=sDBFieldsConfigRealloc;
                                    memset(&arrAfttabDBFieldsConfig[iArrAfttabDBFieldsConfigCount], 0, sizeof(DBFIELDSCONFIG));             
                                    arrAfttabDBFieldsConfig[iArrAfttabDBFieldsConfigCount]=*sDB;
                                    iArrAfttabDBFieldsConfigCount++;         
                                }
                                else
                                {
                                    dbg(TRACE, "getConfig:: ERROR!!! Cannot reallocate arrAfttabDBFieldsConfig for Dep Blank Check");	
                                    iRet=RC_FAIL;
                                }   		
                    	    }	
               	            free(sDB);
               	            sDB=NULL;
               	        }
               	        else
               	        {
               	            dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sDB for Dep Blank Check");	
               	            iRet=RC_FAIL;
               	        }
                    }
            }   }
            else 
            {
                sFieldCheckRealloc=(FIELDCHECK *)realloc(sMoveConfig.arrDepBlankCheck, sizeof(FIELDCHECK)*(sMoveConfig.iArrDepBlankCheckCount+1));    
                if ( sFieldCheckRealloc )
                {           
                	sMoveConfig.arrDepBlankCheck=sFieldCheckRealloc;
                    memset(&sMoveConfig.arrDepBlankCheck[sMoveConfig.iArrDepBlankCheckCount], 0, sizeof(FIELDCHECK));
                    iRet=getFieldCheck(strFile, "AFTTAB_CHK", "AFT", strPtr, &sMoveConfig.arrDepBlankCheck[sMoveConfig.iArrDepBlankCheckCount]);
                    sMoveConfig.iArrDepBlankCheckCount++;  
                }     
                else
                {
                    dbg(TRACE, "getConfig:: ERROR!!! Cannot reallocate sMoveConfig.arrDepBlankCheck for Dep Blank Check");	
                    iRet=RC_FAIL;
                }     
            }
            
            if ( iRet==RC_FAIL )
                break;	
            strPtr=strtok_r(NULL, ",", &strPtr2);	
        }
    }
    

    
    if ( iRet==RC_SUCCESS )
    {
        sMoveConfig.iArrValueCheckCount=0;
        iTmp=readConfigFile(strFile, "AFTTAB_CHK", "VALUE_CHECK", strTmp); 
        sMoveConfig.strValueCheck=(char *)malloc(sizeof(char)*(strlen(strTmp)+1));
        if ( sMoveConfig.strValueCheck )
   	        strcpy(sMoveConfig.strValueCheck, strTmp); 
   	    else
   	    {
   	        dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sMoveConfig.strValueCheck");
   	        iRet=RC_FAIL;
   	    }
   	    
   	    if ( iRet==RC_SUCCESS )
   	    {    
            strPtr=strtok_r(strTmp, ",", &strPtr2);
            while ( strPtr )
            {
            	if ( strcmp(strPtr, " ") )
            	    trim(strPtr, ' ');
            	    
                iTmp=readConfigFile(strFile, "AFTTAB_CHK", strPtr, strVC);
                if ( strcmp(strVC, "") )
                {
                    sValueCheckRealloc=(VALUECHECK *)realloc(sMoveConfig.arrValueCheck, sizeof(VALUECHECK)*(sMoveConfig.iArrValueCheckCount+1)); 
                    if ( sValueCheckRealloc )
                    {
                    	sMoveConfig.arrValueCheck=sValueCheckRealloc;
                        memset(&sMoveConfig.arrValueCheck[sMoveConfig.iArrValueCheckCount], 0, sizeof(VALUECHECK)); 
                        
                        sMoveConfig.arrValueCheck[sMoveConfig.iArrValueCheckCount].strName=(char *)malloc(sizeof(char)*(strlen(strPtr)+1));
                        if ( sMoveConfig.arrValueCheck[sMoveConfig.iArrValueCheckCount].strName )
                            strcpy(sMoveConfig.arrValueCheck[sMoveConfig.iArrValueCheckCount].strName, strPtr);   
                        else
                        {
                            dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sMoveConfig.arrValueCheck[sMoveConfig.iArrValueCheckCount].strName");
                       	    iRet=RC_FAIL;
                       	}
                       	
                        sMoveConfig.arrValueCheck[sMoveConfig.iArrValueCheckCount].strString=(char *)malloc(sizeof(char)*(strlen(strVC)+1));
                        if ( sMoveConfig.arrValueCheck[sMoveConfig.iArrValueCheckCount].strString )
                            strcpy(sMoveConfig.arrValueCheck[sMoveConfig.iArrValueCheckCount].strString, strVC);   
                        else
                        {
                            dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sMoveConfig.arrValueCheck[sMoveConfig.iArrValueCheckCount].strString");
                       	    iRet=RC_FAIL;
                       	}
                       	
                       	if ( iRet==RC_SUCCESS)
                       	{
                            strPtr3=strtok_r(strVC, ",", &strPtr4);
                            if ( strPtr3 )
                            {
                	            if ( strcmp(strPtr3, " ") )
                	                trim(strPtr3, ' ');    
                	            
                	            sMoveConfig.arrValueCheck[sMoveConfig.iArrValueCheckCount].strFlightType=(char *)malloc(sizeof(char)*(strlen(strPtr3)+1));
                                if ( sMoveConfig.arrValueCheck[sMoveConfig.iArrValueCheckCount].strFlightType )
                                    strcpy(sMoveConfig.arrValueCheck[sMoveConfig.iArrValueCheckCount].strFlightType, strPtr3);   
                                else
                                {
                                    dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sMoveConfig.arrValueCheck[sMoveConfig.iArrValueCheckCount].strFlightType");
                       	            iRet=RC_FAIL;
                       	        }
                       	        
                       	        if ( iRet==RC_SUCCESS )
                       	        {
                                    strPtr3=strtok_r(NULL, ",", &strPtr4);
                                    if ( strPtr3 )
                                    {
                                    	if ( strcmp(strPtr3, " ") )
                                    	    trim(strPtr3, ' ');
                                    	    
                                        sMoveConfig.arrValueCheck[sMoveConfig.iArrValueCheckCount].strMsg=(char *)malloc(sizeof(char)*(strlen(strPtr3)+1));
                                        if ( sMoveConfig.arrValueCheck[sMoveConfig.iArrValueCheckCount].strMsg )
                                            strcpy(sMoveConfig.arrValueCheck[sMoveConfig.iArrValueCheckCount].strMsg, strPtr3);
                                        else
                                        {
                                            dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sMoveConfig.arrValueCheck[sMoveConfig.iArrValueCheckCount].strMsg");
                                            iRet=RC_FAIL;
                                        }
                                    }
                                
                                } 
                                
                                if ( iRet==RC_SUCCESS )
                                { 
                                    strPtr3=strtok_r(NULL, ",", &strPtr4);
                                    if ( strPtr3 )
                                    {
                                    	if ( strcmp(strPtr3, " ") )
                                    	    trim(strPtr3, ' ');
                                    	    
                                        sMoveConfig.arrValueCheck[sMoveConfig.iArrValueCheckCount].strExp1=(char *)malloc(sizeof(char)*(strlen(strPtr3)+1));
                                        if ( sMoveConfig.arrValueCheck[sMoveConfig.iArrValueCheckCount].strExp1 )
                                        {
                                            strcpy(sMoveConfig.arrValueCheck[sMoveConfig.iArrValueCheckCount].strExp1, strPtr3);
                                            iRet=parseExpression(strPtr3, &sMoveConfig.arrValueCheck[sMoveConfig.iArrValueCheckCount].sExp1);
                                        }
                                        else
                                        {
                                            dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sMoveConfig.arrValueCheck[sMoveConfig.iArrValueCheckCount].strExp1" );
                                            iRet=RC_FAIL;
                                        }         	
                                    } 
                                }
 
                                if ( iRet==RC_SUCCESS )
                                {
                                    strPtr3=strtok_r(NULL, ",", &strPtr4);     
                                    if ( strPtr3 )
                                    {
                                    	if ( strcmp(strPtr3, " ") )
                                    	    trim(strPtr3, ' ');
                                    	    
                                        sMoveConfig.arrValueCheck[sMoveConfig.iArrValueCheckCount].strOp=(char *)malloc(sizeof(char)*(strlen(strPtr3)+1));
                                        if ( sMoveConfig.arrValueCheck[sMoveConfig.iArrValueCheckCount].strOp )
                                            strcpy(sMoveConfig.arrValueCheck[sMoveConfig.iArrValueCheckCount].strOp, strPtr3);
                                        else
                                        {
                                            dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sMoveConfig.arrValueCheck[sMoveConfig.iArrValueCheckCount].strOp");
                                            iRet=RC_FAIL;
                                        }                                        
                                    }           	
                                } 
                                                                      
                                if ( iRet==RC_SUCCESS )
                                {
                        	        strPtr3=strtok_r(NULL, ",", &strPtr4); 
                                    if ( strPtr3 )
                                    {
                                    	if ( strcmp(strPtr3, " ") )
                                    	    trim(strPtr3, ' ');
                                    	    
                            	        sMoveConfig.arrValueCheck[sMoveConfig.iArrValueCheckCount].strExp2=(char *)malloc(sizeof(char)*(strlen(strPtr3)+1));
                                        if ( sMoveConfig.arrValueCheck[sMoveConfig.iArrValueCheckCount].strExp2 )
                                        {
                                            strcpy(sMoveConfig.arrValueCheck[sMoveConfig.iArrValueCheckCount].strExp2, strPtr3);
                                            iRet=parseExpression(strPtr3, &sMoveConfig.arrValueCheck[sMoveConfig.iArrValueCheckCount].sExp2);
                                    
                                        }
                                        else
                                        {                                                                          
                                	        iRet=RC_FAIL;
                                            dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sMoveConfig.arrValueCheck[sMoveConfig.iArrValueCheckCount].strExp2");
                                        }
                                        strPtr3=strtok_r(NULL, ",", &strPtr4);            	
                                    } 
                                }    
                                
                                if ( iRet==RC_SUCCESS )
                                {   
                                    if ( strPtr3 )
                                    {
                                        sMoveConfig.arrValueCheck[sMoveConfig.iArrValueCheckCount].strValue=(char *)malloc(sizeof(char)*(strlen(strPtr3)+1));
                                        if ( sMoveConfig.arrValueCheck[sMoveConfig.iArrValueCheckCount].strValue ) 
                                            strcpy(sMoveConfig.arrValueCheck[sMoveConfig.iArrValueCheckCount].strValue, strPtr3);
                                        else
                                        {
                                        	iRet=RC_FAIL;
                                            dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sMoveConfig.arrValueCheck[sMoveConfig.iArrValueCheckCount].strValue");
                                        }
                                    } 
                                }         
                                sMoveConfig.iArrValueCheckCount++;                 	                  		
                       	    }
                       	}
                    }	
                    else
                    {
                        dbg(TRACE, "getConfig:: ERROR!!! Cannot reallocate sMoveConfig.arrValueCheck"); 
                        iRet=RC_FAIL;
                    }   
                }

                if ( iRet==RC_FAIL )
                    break;
            	strPtr=strtok_r(NULL, ",", &strPtr2);	    	 
            }
        }
    }

    if ( iRet==RC_SUCCESS )
    {
        memset(&sLoadConfig, 0, sizeof(LOADCONFIG));
        memset(&sDBSelectLoaTab, 0, sizeof(DBSELECT));
        sDBSelectLoaTab.strName=(char *)malloc(sizeof(char)*7);
        if ( sDBSelectLoaTab.strName )
            strcpy(sDBSelectLoaTab.strName, "LOATAB");
        else
        {
            dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sDBSelectLoaTab.strName");
            iRet=RC_FAIL;	
        }
    }        
     
       
    if ( iRet==RC_SUCCESS )
    {
    	iArrLoatabDBFieldsConfigCount=0;    
        arrLoatabDBFieldsConfig=(DBFIELDSCONFIG *)malloc(sizeof(DBFIELDSCONFIG)*6); 
        if ( arrLoatabDBFieldsConfig )
        {
            memset(arrLoatabDBFieldsConfig, 0, sizeof(DBFIELDSCONFIG)*6);
        
            strcpy(arrLoatabDBFieldsConfig[0].strFieldName, "URNO");    
            iRet=getDBFieldDesc(arrLoatabDBFieldsConfig[0].strFieldName, "LOA", &arrLoatabDBFieldsConfig[0].strFieldDesc);
            iArrLoatabDBFieldsConfigCount++;
        
            if ( iRet==RC_SUCCESS )
            {
                strcpy(arrLoatabDBFieldsConfig[1].strFieldName, "TYPE");    
                iRet=getDBFieldDesc(arrLoatabDBFieldsConfig[1].strFieldName, "LOA", &arrLoatabDBFieldsConfig[1].strFieldDesc);
                iArrLoatabDBFieldsConfigCount++;
            }
            
            if ( iRet==RC_SUCCESS )
            {             
                strcpy(arrLoatabDBFieldsConfig[2].strFieldName, "STYP");    
                iRet=getDBFieldDesc(arrLoatabDBFieldsConfig[2].strFieldName, "LOA", &arrLoatabDBFieldsConfig[2].strFieldDesc);
                iArrLoatabDBFieldsConfigCount++;
            }
            
            if ( iRet==RC_SUCCESS )
            {            
                strcpy(arrLoatabDBFieldsConfig[3].strFieldName, "SSTP");    
                iRet=getDBFieldDesc(arrLoatabDBFieldsConfig[3].strFieldName, "LOA", &arrLoatabDBFieldsConfig[3].strFieldDesc);
                iArrLoatabDBFieldsConfigCount++;
            }
            
            if ( iRet==RC_SUCCESS )
            {
                strcpy(arrLoatabDBFieldsConfig[4].strFieldName, "SSST");    
                iRet=getDBFieldDesc(arrLoatabDBFieldsConfig[4].strFieldName, "LOA", &arrLoatabDBFieldsConfig[4].strFieldDesc);
                iArrLoatabDBFieldsConfigCount++;
            }

            if ( iRet==RC_SUCCESS )
            {
                strcpy(arrLoatabDBFieldsConfig[5].strFieldName, "APC3");    
                iRet=getDBFieldDesc(arrLoatabDBFieldsConfig[5].strFieldName, "LOA", &arrLoatabDBFieldsConfig[5].strFieldDesc);
                iArrLoatabDBFieldsConfigCount++;
            }            
        }    
        else
        {
     	    dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate arrLoatabDBFieldsConfig");
    	    iRet=RC_FAIL;       	
        }     
    }
    
    if ( iRet==RC_SUCCESS )
    {
        strLoatabFieldNames=(char *)malloc(sizeof(char)*30);
        if ( strLoatabFieldNames )
            strcpy(strLoatabFieldNames, "URNO,TYPE,STYP,SSTP,SSST,APC3");
        else
        {
        	iRet=RC_FAIL;
            dbg(TRACE, "getConfig:: Cannot allocate strAfttabFieldNames (URNO,TYPE,STYP,SSTP,SSST,APC3)");
        }
    }
    	
    if ( iRet==RC_SUCCESS )
    {	
        iTmp=readConfigFile(strFile, "FLIGHT", "STATUS_UPD", strTmp);
        if ( iRet!=RC_SUCCESS )
            strcpy(strTmp, "NO");       
               
        sLoadConfig.strAfttabDQCSUpdate=(char *)malloc(sizeof(char)*(strlen(strTmp)+1));  
        if ( sLoadConfig.strAfttabDQCSUpdate )
            strcpy(sLoadConfig.strAfttabDQCSUpdate, strTmp);
        else
        {
        	iRet=RC_FAIL;
            dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sLoadConfig.strAfttabDQCSUpdate");
        }
    }

    if ( iRet==RC_SUCCESS )
    {
        iTmp=readConfigFile(strFile, "LOATAB_CHK", "FLIGHT_REFERENCE", strTmp);
        sLoadConfig.strFlightRef=(char *)malloc(sizeof(char)*(strlen(strTmp)+1));  
        if ( sLoadConfig.strFlightRef ) 
           strcpy(sLoadConfig.strFlightRef, strTmp);   
        else
        {
            iRet=RC_FAIL;
            dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sLoadConfig.strFlightRef");
        }

        if ( iRet==RC_SUCCESS && strlen(strTmp)==4 )
        {
            if ( getArrTabFieldIndex(strTmp, arrLoatabDBFieldsConfig, iArrLoatabDBFieldsConfigCount)==-1 )
            {
            	sDB=malloc(sizeof(DBFIELDSCONFIG));
            	if ( sDB )
            	{
            	    memset(sDB, 0, sizeof(DBFIELDSCONFIG));
            	    iTmp=addArrDBFields(strTmp, "LOA", sDB, &strLoatabFieldNames);
            	    if ( iTmp==RC_SUCCESS )
                    {
                        sDBFieldsConfigRealloc=realloc(arrLoatabDBFieldsConfig, sizeof(DBFIELDSCONFIG)*(iArrLoatabDBFieldsConfigCount+1)); 
                        if ( sDBFieldsConfigRealloc )
                        {
                        	arrLoatabDBFieldsConfig=sDBFieldsConfigRealloc;
                            memset(&arrLoatabDBFieldsConfig[iArrLoatabDBFieldsConfigCount], 0, sizeof(DBFIELDSCONFIG));             
                            arrLoatabDBFieldsConfig[iArrLoatabDBFieldsConfigCount]=*sDB;
                            iArrLoatabDBFieldsConfigCount++;            		
                        }
                        else
                        {
                            dbg(TRACE, "getConfig:: ERROR!!! Cannot reallocate arrLoatabDBFieldsConfig");
                            iRet=RC_FAIL;
                        }
                    }	
                    else
                    {
                        dbg(TRACE, "getConfig:: ERROR!!! addArrDBFields Failed.");
                        iRet=RC_FAIL;  	
                    }
               	    free(sDB);
               	    sDB=NULL;
                }
                else
                {
                    dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sDB ");
                    iRet=RC_FAIL;
                }
            }    	
        }   
    }
    
    if ( iRet==RC_SUCCESS )
    {    
        iTmp=readConfigFile(strFile, "LOATAB_CHK", "TYPE", strTmp);
        sLoadConfig.strType=(char *)malloc(sizeof(char)*(strlen(strTmp)+1));  
        if ( sLoadConfig.strType )
            strcpy(sLoadConfig.strType, strTmp);       
        else
        {
            iRet=RC_FAIL;
            dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sLoadConfig.strType ");
        }
    }
    
    if ( iRet==RC_SUCCESS )
    {
        iTmp=readConfigFile(strFile, "LOATAB_CHK", "LOAD_BLANK_CHECK_SESSION", strTmp);
        sLoadConfig.strLoadBlankCheckSession=(char *)malloc(sizeof(char)*(strlen(strTmp)+1));  
        if ( sLoadConfig.strLoadBlankCheckSession )
            strcpy(sLoadConfig.strLoadBlankCheckSession, strTmp);   
        else
        {
            iRet=RC_FAIL;
            dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sLoadConfig.strLoadBlankCheckSession ");
        }    
    }

    if ( iRet==RC_SUCCESS )
    {
        iTmp=readConfigFile(strFile, "LOATAB_CHK", "LOAD_VALUE_CHECK_SESSION", strTmp);
        sLoadConfig.strLoadValueCheckSession=(char *)malloc(sizeof(char)*(strlen(strTmp)+1));  
        if ( sLoadConfig.strLoadValueCheckSession )
            strcpy(sLoadConfig.strLoadValueCheckSession, strTmp);  
        else
        {
            iRet=RC_FAIL;
            dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sLoadConfig.strLoadValueCheckSession ");
        }     
    }

    if ( iRet==RC_SUCCESS )
    {
        iTmp=readConfigFile(strFile, "LOATAB_CHK", "LOAD_CONFIG_FILE", strTmp);
        sLoadConfig.strLoadFile=(char *)malloc(sizeof(char)*(strlen(strTmp)+1)); 
        if ( sLoadConfig.strLoadFile ) 
            strcpy(sLoadConfig.strLoadFile, strTmp);      
        else
        {
            iRet=RC_FAIL;
            dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sLoadConfig.strLoadFile ");
        }    
    }
    
    
    if ( iRet==RC_SUCCESS )
    {
        strcpy(strTmp, sLoadConfig.strLoadBlankCheckSession);
        sLoadConfig.iLoadBlank=0;
        strPtr=strtok_r(strTmp, ",", &strPtr2);
        while ( strPtr)
        {
        	if ( strcmp(strPtr, " ") )
        	    trim(strPtr, ' ');
            dbg(DEBUG, "getConfig:: Load Blank Check HEADER: [%s]", strPtr);
        	sLoadBlankRealloc=realloc(sLoadConfig.sLoadBlank,(sizeof(LOADBLANK)*(sLoadConfig.iLoadBlank+1)));
        	if ( sLoadBlankRealloc )
        	{
        		sLoadConfig.sLoadBlank=sLoadBlankRealloc;
        	    memset(&sLoadConfig.sLoadBlank[sLoadConfig.iLoadBlank], 0, sizeof(LOADBLANK));
        	    sLoadConfig.sLoadBlank[sLoadConfig.iLoadBlank].strHeader=(char *)malloc(sizeof(char)*(strlen(strPtr)+1));
        	    if ( sLoadConfig.sLoadBlank[sLoadConfig.iLoadBlank].strHeader )
        	    {
         	        strcpy(sLoadConfig.sLoadBlank[sLoadConfig.iLoadBlank].strHeader, strPtr);         	    
         	        iRet=readLoadBlank(strFile, strPtr, &sLoadConfig.sLoadBlank[sLoadConfig.iLoadBlank]);         	     	
                    sLoadConfig.iLoadBlank++;
                }
                else
                {
                    iRet=RC_FAIL;
                    dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sLoadConfig.sLoadBlank[%d].strHeader ",
                        sLoadConfig.iLoadBlank);
                }
            }
            else
            {
            	iRet=RC_FAIL;
                dbg(TRACE, "getConfig:: ERROR!!! Cannot reallocate sLoadConfig.sLoadBlank");
            }
            
            if ( iRet==RC_FAIL )
                break;
            strPtr=strtok_r(NULL, ",", &strPtr2);
        }
    }
    
    if ( iRet==RC_SUCCESS )
    {
        strcpy(strTmp, sLoadConfig.strLoadValueCheckSession);
	    strPtr=strtok_r(strTmp, ",", &strPtr2);
	    sLoadConfig.iLoadVC=0;
	    while (strPtr)
        {
        	if ( strcmp(strPtr, " ") )
        	    trim(strPtr, ' ');
        	    
        	sLoadVCRealloc=realloc(sLoadConfig.sLoadVC,(sizeof(LOADVALUECHECK)*(sLoadConfig.iLoadVC+1)));
         	if ( sLoadVCRealloc )
        	{
        		sLoadConfig.sLoadVC=sLoadVCRealloc;
        	    memset(&sLoadConfig.sLoadVC[sLoadConfig.iLoadVC], 0, sizeof(LOADVALUECHECK));
        	    sLoadConfig.sLoadVC[sLoadConfig.iLoadVC].strHeader=malloc(sizeof(char)*(strlen(strPtr)+1));
         	    strcpy(sLoadConfig.sLoadVC[sLoadConfig.iLoadVC].strHeader, strPtr);
        
             	iRet=readLoadValueCheck(strFile, strPtr, &sLoadConfig.sLoadVC[sLoadConfig.iLoadVC]); 
         	     	
                sLoadConfig.iLoadVC++;
            }
            else
            {
                dbg(TRACE, "getConfig:: ERROR!!! Cannot reallocate sLoadConfig.sLoadHeader");
                iRet=RC_FAIL;
            }
                
            if ( iRet==RC_FAIL )
                break;
                
            strPtr=strtok_r(NULL, ",", &strPtr2);    
        }    	
    }
    
    dbg(TRACE, "getConfig:: get Scheduled Tasks");
    sSched=NULL;
    iSched=0;
    if ( iRet==RC_SUCCESS )
    {
    	char strHeader[257];
    	SCHED *sSchedRealloc;
    	
        for (a='A'; a<='Z'; a++)
        {
            sprintf(strHeader, "TIM%c", a);
            if ( readHeaderConfigFile(strFile, strHeader)==RC_SUCCESS )
            {
                dbg(DEBUG, "getConfig:: FOUND Header[%s]", strHeader);
                sSchedRealloc=(SCHED *)realloc(sSched, (sizeof(SCHED)*(iSched+1)));
                if (sSchedRealloc)
                {
                    sSched=sSchedRealloc;
                    memset(&sSched[iSched], 0, sizeof(SCHED));
                    
                    sSched[iSched].strName=(char *)malloc(sizeof(char)*(strlen(strHeader)+1));
                    if ( sSched[iSched].strName )
                         strcpy(sSched[iSched].strName, strHeader);	
                    else
                    {
                        dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sSched[%d].strName	", iSched);
                        iRet=RC_FAIL;
                    }
                    
                    if ( iRet==RC_SUCCESS )
                    {
                        iTmp=readConfigFile(strFile, strHeader, "TIME_REF", strTmp);
                        if ( !strcmp(strTmp, "") )
                            strcpy(strTmp, "HOUR");
            
                        sSched[iSched].strTimeRef=(char *)malloc(sizeof(char)*(strlen(strTmp)+1));  
                        if ( sSched[iSched].strTimeRef )
                            strcpy(sSched[iSched].strTimeRef, strTmp);       
                        else
                        {
                            iRet=RC_FAIL;
                            dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sSched[%d].strTimeRef ", iSched);
                        }   
                    }
                    
                    if ( iRet==RC_SUCCESS )
                    {
                        iTmp=readConfigFile(strFile, strHeader, "TIMEZONE", strTmp);
                        sSched[iSched].strTimeZone=(char *)malloc(sizeof(char)*(strlen(strTmp)+1));  
                        if ( sSched[iSched].strTimeZone )
                            strcpy(sSched[iSched].strTimeZone, strTmp);       
                        else
                        {
                            iRet=RC_FAIL;
                            dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sSched[%d].strTimeZone ", iSched);
                        } 
                    }
                    
                    if ( iRet==RC_SUCCESS )
                    {
                        iTmp=readConfigFile(strFile, strHeader, "START_TIME", strTmp);
                        sSched[iSched].strStartTime=(char *)malloc(sizeof(char)*(strlen(strTmp)+1));  
                        if ( sSched[iSched].strStartTime )
                            strcpy(sSched[iSched].strStartTime, strTmp);       
                        else
                        {
                            iRet=RC_FAIL;
                            dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sSched[%d].strStartTime ", iSched);
                        }           	
                    } 	

                    if ( iRet==RC_SUCCESS )
                    {
                        iTmp=readConfigFile(strFile, strHeader, "END_TIME", strTmp);
                        sSched[iSched].strEndTime=(char *)malloc(sizeof(char)*(strlen(strTmp)+1));  
                        if ( sSched[iSched].strEndTime )
                            strcpy(sSched[iSched].strEndTime, strTmp);       
                        else
                        {
                            iRet=RC_FAIL;
                            dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sSched[%d].strEndTime ", iSched);
                        }           	
                    }  
                    
                    if ( iRet==RC_SUCCESS )
                    {
                        iTmp=readConfigFile(strFile, strHeader, "ARRIVAL_FIELD", strTmp);
                        sSched[iSched].strArrivalField=(char *)malloc(sizeof(char)*(strlen(strTmp)+1));  
                        if ( sSched[iSched].strArrivalField )
                            strcpy(sSched[iSched].strArrivalField, strTmp);       
                        else
                        {
                            iRet=RC_FAIL;
                            dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sSched[%d].strArrivalField ", iSched);
                        }           	
                    }   
                    
                    if ( iRet==RC_SUCCESS )
                    {
                        iTmp=readConfigFile(strFile, strHeader, "DEPARTURE_FIELD", strTmp);
                        sSched[iSched].strDepartureField=(char *)malloc(sizeof(char)*(strlen(strTmp)+1));  
                        if ( sSched[iSched].strDepartureField )
                            strcpy(sSched[iSched].strDepartureField, strTmp);       
                        else
                        {
                            iRet=RC_FAIL;
                            dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sSched[%d].strDepartureField ", iSched);
                        }           	
                    }    
                    
                    if ( iRet==RC_SUCCESS )
                    {
                        iTmp=readConfigFile(strFile, strHeader, "DQCS", strTmp);
                        sSched[iSched].strDQCS=(char *)malloc(sizeof(char)*(strlen(strTmp)+1));  
                        if ( sSched[iSched].strDQCS )
                            strcpy(sSched[iSched].strDQCS, strTmp);       
                        else
                        {
                            iRet=RC_FAIL;
                            dbg(TRACE, "getConfig:: ERROR!!! Cannot allocate sSched[%d].strDQCS ", iSched);
                        }           	
                    }                   
                                         
                    iSched++;	
                }
                else
                {
                	dbg(TRACE, "getConfig:: ERROR!!! Cannot reallocate sSchedRealloc");
                    iRet=RC_FAIL;	
                }
            }
        }
    }
       
    return iRet;
}

int readLoadBlank(char *strFile, char *strHeader, LOADBLANK *sLoadBlank)
{
    int iRet=RC_SUCCESS;
    char strTmp[1025];
    char strCheck[1025];
    char strLoadCheck[1025];
    char *strPtr, *strPtr2;
    char *strPtrLoad, *strPtrLoad2;
    int iTmp=0;
    FIELDCHECK *sFieldCheckRealloc;
    LOADCHECK *sLoadCheckRealloc;
    

    dbg(DEBUG, "readLoadBlank:: strFile[%s] strHeader[%s]", strFile, strHeader);
    
    iTmp=readConfigFile(strFile, strHeader, "CONDITIONS", strTmp);
    if ( strlen(strTmp)==0 )
    {
        sLoadBlank->strConditions=NULL;
        sLoadBlank->sFieldCheck=NULL;
        sLoadBlank->iFieldCheck=0;
    }
    else
    {
        sLoadBlank->strConditions=(char *)malloc(sizeof(char)*(strlen(strTmp)+1));
        if ( sLoadBlank->strConditions )
        {
            strcpy(sLoadBlank->strConditions, strTmp);	

            sLoadBlank->iFieldCheck=0;
            strPtr=strtok_r(strTmp, ",", &strPtr2);
            while (strPtr)
            {   
            	if ( strcmp(strPtr, " ") )
            	    trim(strPtr, ' ');

            	iTmp=readConfigFile(strFile, strHeader, strPtr, strCheck);
        	    if ( iTmp==RC_SUCCESS )
        	    {        	    	
        	        sFieldCheckRealloc=(FIELDCHECK *)realloc(sLoadBlank->sFieldCheck, sizeof(FIELDCHECK)*(sLoadBlank->iFieldCheck+1));    
        	        if ( sFieldCheckRealloc )          
        	        { 
        	    	    sLoadBlank->sFieldCheck=sFieldCheckRealloc;
                        memset(&sLoadBlank->sFieldCheck[sLoadBlank->iFieldCheck], 0, sizeof(FIELDCHECK));
                        iRet=getFieldCheck(strFile, strHeader, "AFT", strPtr, &sLoadBlank->sFieldCheck[sLoadBlank->iFieldCheck]);
                        sLoadBlank->iFieldCheck++;
                    }
                    else
                    {
                        dbg(TRACE, "readLoadBlank:: Cannot reallocate readLoadBlank->sFieldCheck"); 
                        iRet=RC_FAIL;
                    }
                }
                
                if ( iRet==RC_FAIL )
                    break;
                strPtr=strtok_r(NULL, ",", &strPtr2);
            }
        }
        else
        {
            dbg(TRACE, "readLoadBlank:: ERROR!!! Cannot allocate sLoadBlank->strConditions");
            iRet=RC_FAIL;		
        }
    }

    if ( iRet==RC_SUCCESS )
    {
        iTmp=readConfigFile(strFile, strHeader, "VALUE_CHECK", strTmp);
        if ( strlen(strTmp)==0 )
        {
            sLoadBlank->strValueCheck=NULL;
            sLoadBlank->sLoadCheck=NULL;
            sLoadBlank->iLoadCheck=0;
        }
        else
        {
            sLoadBlank->strValueCheck=(char *)malloc(sizeof(char)*(strlen(strTmp)+1));
            if ( sLoadBlank->strValueCheck )
            {
                strcpy(sLoadBlank->strValueCheck, strTmp);	
                sLoadBlank->iLoadCheck=0;
                strPtr=strtok_r(strTmp, ",", &strPtr2);
                while (strPtr)
                {   
                	if ( strcmp(strPtr, " ") )
                	    trim(strPtr, ' ');
                	    
                    iTmp=readConfigFile(strFile, strHeader, strPtr, strCheck);
                    
                    if ( strlen(strCheck)==0 )
                    {
                        sLoadBlank->sLoadCheck=NULL;
                        sLoadBlank->iLoadCheck=0;	
                    }
                    else
                    {
                        sLoadCheckRealloc=(LOADCHECK *)realloc(sLoadBlank->sLoadCheck, sizeof(LOADCHECK)*(sLoadBlank->iLoadCheck+1));  
                        if ( sLoadCheckRealloc )
                        {
                            sLoadBlank->sLoadCheck = sLoadCheckRealloc;
                            
                            sLoadBlank->sLoadCheck[sLoadBlank->iLoadCheck].strName=(char *)malloc(sizeof(char)*(strlen(strPtr)+1));
                            if ( sLoadBlank->sLoadCheck[sLoadBlank->iLoadCheck].strName )
                                strcpy(sLoadBlank->sLoadCheck[sLoadBlank->iLoadCheck].strName, strPtr);
                            else
                            {
                                dbg(TRACE, "readLoadBlank:: ERROR!!! Cannot allocate sLoadBlank->sLoadCheck[%d].strName", 
                                    sLoadBlank->iLoadCheck);
                                iRet=RC_FAIL;                           	
                            }
        
                            if ( iRet==RC_SUCCESS )
                            {
                                sLoadBlank->sLoadCheck[sLoadBlank->iLoadCheck].strString=(char *)malloc(sizeof(char)*(strlen(strCheck)+1));
                                if ( sLoadBlank->sLoadCheck[sLoadBlank->iLoadCheck].strString )
                                    strcpy(sLoadBlank->sLoadCheck[sLoadBlank->iLoadCheck].strString, strCheck);
                                else
                                {
                                    dbg(TRACE, "readLoadBlank:: ERROR!!! Cannot allocate sLoadBlank->sLoadCheck[%d].strString", 
                                        sLoadBlank->iLoadCheck);
                                    iRet=RC_FAIL;                           	
                                }
                            }
                            
                            if ( iRet==RC_SUCCESS )
                            {    
                                strPtrLoad=strtok_r(strCheck, ",", &strPtrLoad2);
                                if ( strPtrLoad )
                                {
                                	if ( strcmp(strPtrLoad, " ") )
                                	    trim(strPtrLoad, ' ') ;
                                	    
                                	sLoadBlank->sLoadCheck[sLoadBlank->iLoadCheck].strMsg=(char *)malloc(sizeof(char)*(strlen(strPtrLoad)+1));
                                	if ( sLoadBlank->sLoadCheck[sLoadBlank->iLoadCheck].strMsg )
                                	    strcpy(sLoadBlank->sLoadCheck[sLoadBlank->iLoadCheck].strMsg, strPtrLoad);
                                    else
                                    {
                                        dbg(TRACE, "readLoadBlank:: ERROR!!! Cannot allocate sLoadBlank->sLoadCheck[sLoadBlank->iLoadCheck].strMsg", 
                                            sLoadBlank->iLoadCheck);
                                        iRet=RC_FAIL;                           	
                                    }                                	
                                
                                    if ( iRet==RC_SUCCESS )
                                    {
                                        strPtrLoad=strtok_r(NULL, ",", &strPtrLoad2);
                                        if ( strPtrLoad )
                                        {
                                        	if ( strcmp(strPtrLoad, " ") )
                                    	        trim(strPtrLoad, ' ');
                                    	
                                            iTmp=readConfigFile(strFile, strHeader, strPtrLoad, strLoadCheck);
                                            if ( iTmp==RC_FAIL )
                                                iTmp=readConfigFile(sLoadConfig.strLoadFile, "", strPtrLoad, strLoadCheck); 
                                            if ( iTmp==RC_SUCCESS )
                                                iTmp=parseLoadCheck(strLoadCheck, &sLoadBlank->sLoadCheck[sLoadBlank->iLoadCheck]); 
                                            else
                                            {
                                                dbg(TRACE, "readLoadBlank:: ERROR!!! Cannot find [%s] on [dqahdl.cfg] and [%s]", 
                                                            strPtrLoad, sLoadConfig.strLoadFile);
                                                iRet=RC_FAIL;                                            	
                                            }                                           	
                                        }
                                    }

                                    if ( iRet==RC_SUCCESS )
                                    {
                                        strPtrLoad=strtok_r(NULL, ",", &strPtrLoad2);
                                        if ( strPtrLoad )
                                        {
                                        	if ( strcmp(strPtrLoad, " ") )
                                        	    trim(strPtrLoad, ' ');
                                        	    
                                            sLoadBlank->sLoadCheck[sLoadBlank->iLoadCheck].strOp=(char *)malloc(sizeof(char)*(strlen(strPtrLoad)+1));
                                	        if ( sLoadBlank->sLoadCheck[sLoadBlank->iLoadCheck].strOp )
                                	            strcpy(sLoadBlank->sLoadCheck[sLoadBlank->iLoadCheck].strOp, strPtrLoad);   
                                            else
                                            {
                                                dbg(TRACE, "readLoadBlank:: ERROR!!! Cannot allocate sLoadBlank->sLoadCheck[sLoadBlank->iLoadCheck].strOp", 
                                                    sLoadBlank->iLoadCheck);
                                                iRet=RC_FAIL;                           	
                                            }                                  	                                    	
                                        }  
                                    }
                                    
                                    if ( iRet==RC_SUCCESS )
                                    {
                                    	strPtrLoad=strtok_r(NULL, ",", &strPtrLoad2);
                                        if ( strPtrLoad )
                                        {
                                            if ( strcmp(strPtrLoad, " ") )
                                        	    trim(strPtrLoad, ' ');
                                        	    
                                            sLoadBlank->sLoadCheck[sLoadBlank->iLoadCheck].strValue=(char *)malloc(sizeof(char)*(strlen(strPtrLoad)+1));
                                	        if ( sLoadBlank->sLoadCheck[sLoadBlank->iLoadCheck].strValue )
                                	            strcpy(sLoadBlank->sLoadCheck[sLoadBlank->iLoadCheck].strValue, strPtrLoad);   
                                	        else
                                            {
                                                dbg(TRACE, "readLoadBlank:: ERROR!!! Cannot allocate sLoadBlank->sLoadCheck[sLoadBlank->iLoadCheck].strValue", 
                                                    sLoadBlank->iLoadCheck);
                                                iRet=RC_FAIL;                           	
                                            }   
                                	    }                        	
                                    }                            
                                }
                            }  
                                                            
                            sLoadBlank->iLoadCheck++;	
                        }
                        ////
                        else
                        {
                            dbg(DEBUG, "readLoadBlank:: Cannot reallocate sLoadBlank->sLoadCheck");
                            iRet=RC_FAIL;
                        }                       
                    }
                    
                    if ( iRet==RC_FAIL )
                        break;
                    strPtr=strtok_r(NULL,",", &strPtr2);	
                }
            }
            else
            {
                dbg(TRACE, "readLoadBlank:: ERROR!!! Cannot allocate sLoadBlank->strValueCheck");
                iRet=RC_FAIL;		
            }
        }
    }    	    
    dbg(DEBUG, "readLoadBlank:: strFile[%s] strHeader[%s] Result[%d]", strFile, strHeader, iRet);
    return iRet;	
}

int readLoadHeader(char *strFile, char *strHeader, LOADHEADER *sHeader)
{
    int iRet=RC_SUCCESS;
    int iTmp=0;
    char strTmp[1025];
    char strLoad[1025];
    char strCheck[1025];
    char *strPtr, *strPtr2;
    char strFileName[257];
    FIELDCHECK *sFieldCheckRealloc;
    
    dbg(DEBUG, "readLoadHeader:: strFile[%s] strHeader[%s]", strFile, strHeader);
    
    iTmp=readConfigFile(strFile, strHeader, "CONDITIONS", strTmp);
    if ( strlen(strTmp)==0 )
    {
        sHeader->strConditions=NULL;
        sHeader->sCondCheck=NULL;
    }
    else
    {
        sHeader->strConditions=(char *)malloc(sizeof(char)*(strlen(strTmp)+1));
        if ( sHeader->strConditions )
        {
            strcpy(sHeader->strConditions, strTmp);	
            sHeader->iCondCheckCount=0;
            strPtr=strtok_r(strTmp, ",", &strPtr2);
            while (strPtr)
            {
            	if ( strcmp(strPtr, " ") )
            	    trim(strPtr, ' ');
            	    
        	    iTmp=readConfigFile(strFile, strHeader, strPtr, strCheck);
        	    if ( iTmp==RC_SUCCESS )
        	    {
        	    	
        	        sFieldCheckRealloc=(FIELDCHECK *)realloc(sHeader->sCondCheck, sizeof(FIELDCHECK)*(sHeader->iCondCheckCount+1));    
        	        if ( sFieldCheckRealloc )          
        	        { 
        	    	    sHeader->sCondCheck=sFieldCheckRealloc;
                        memset(&sHeader->sCondCheck[sHeader->iCondCheckCount], 0, sizeof(FIELDCHECK));
                        iRet=getFieldCheck(strFile, strHeader, "AFT", strPtr, &sHeader->sCondCheck[sHeader->iCondCheckCount]);
                        sHeader->iCondCheckCount++;
                    }
                    else
                        dbg(TRACE, "readLoadHeader:: Cannot reallocate sHeader->sCondCheck"); 
                }
                strPtr=strtok_r(NULL, ",", &strPtr2);	
 
        	    iTmp=readConfigFile(strFile, strHeader, "ACCEPT_ZERO", strCheck);
                if ( iTmp==RC_SUCCESS )
                {
                    sHeader->strAcceptZero=(char *)malloc(sizeof(char)*(strlen(strCheck)+1));    	
                    if ( sHeader->strAcceptZero ) 
                        strcpy(sHeader->strAcceptZero, strCheck);                	
                }
                else
                {
                    sHeader->strAcceptZero=(char *)malloc(sizeof(char)*6);    	
                    if ( sHeader->strAcceptZero ) 
                        strcpy(sHeader->strAcceptZero, "FALSE");
                }
                
        	    iTmp=readConfigFile(strFile, strHeader, "BLANK_REAS", strCheck);
                if ( iTmp==RC_SUCCESS )
                {
                    sHeader->strBlankReas=(char *)malloc(sizeof(char)*(strlen(strCheck)+1));    	
                    if ( sHeader->strBlankReas ) 
                        strcpy(sHeader->strBlankReas, strCheck);                	
                }
                
                sprintf(strLoad, "%s_TOTAL", strHeader);
        	    iTmp=readConfigFile(strFile, strHeader, strLoad, strCheck);
        	    if ( iTmp==RC_SUCCESS )
                {
                	if ( !strcmp(strCheck, "LOAD_CONFIG_FILE") )
                	    iTmp=readConfigFile(sLoadConfig.strLoadFile, "", strLoad, strCheck);    
                	else if ( fileExist(strCheck) )
                	{
                	    strcpy(strFileName, strCheck);
                	    iTmp=readConfigFile(strFileName, "", strLoad, strCheck);    	
                	}
                	
                    sHeader->sLoadCheck.strString=(char *)malloc(sizeof(char)*(strlen(strCheck)+1));    	
                    if ( sHeader->sLoadCheck.strString ) 
                    {
                        strcpy(sHeader->sLoadCheck.strString, strCheck);  
                       
                        sHeader->sLoadCheck.sCond.strName=(char *)malloc(sizeof(char)*(strlen(strLoad)+1));
                        if ( sHeader->sLoadCheck.sCond.strName )
                            strcpy(sHeader->sLoadCheck.sCond.strName, strLoad);  
                        iTmp=parseLoadCheck(strCheck, &sHeader->sLoadCheck);                               
                    }      	
                } 
            }
        }
        else
            dbg(TRACE, "readLoadHeader:: Cannot Allocate sHeader->strConditions");   
    }
    
    dbg(DEBUG, "readLoadHeader:: strFile[%s] strHeader[%s] Result[%d]", strFile, strHeader, iRet);
    return iRet;	
}

int readLoadValueCheck(char *strFile, char *strHeader, LOADVALUECHECK *sHeader)
{
    int iRet=RC_SUCCESS;
    int iTmp=0;
    char strTmp[1025];
    char strVCTmp[1025];
    char strLoadCheck[1025];
    char *strPtr, *strPtr2;
    char *strPtr3, *strPtr4;
    char strFileName[257];
    FIELDCHECK *sFieldCheckRealloc;
    LOADEXP *sLoadExpRealloc;
    
    dbg(DEBUG, "readLoadValueCheck:: strFile[%s] strHeader[%s]", strFile, strHeader);

    sHeader->strConditions=NULL;
    sHeader->sFieldCheck=NULL;
    sHeader->iFieldCheck=0;               
    iTmp=readConfigFile(strFile, strHeader, "CONDITIONS", strTmp);
    if ( strlen(strTmp)==0 )
        ;
    else
    {
    	dbg(DEBUG, "readLoadValueCheck:: Processing Conditions [%s]", strTmp);
        sHeader->strConditions=(char *)malloc(sizeof(char)*(strlen(strTmp)+1));
        if ( sHeader->strConditions )
        {
            strcpy(sHeader->strConditions, strTmp);	            
            strPtr=strtok_r(strTmp, ",", &strPtr2);
            while (strPtr)
            {
           	    trim(strPtr, ' ');
                if ( strcmp(strPtr, "") )
                {
        	        sFieldCheckRealloc=(FIELDCHECK *)realloc(sHeader->sFieldCheck, sizeof(FIELDCHECK)*(sHeader->iFieldCheck+1));    
        	        if ( sFieldCheckRealloc )          
        	        { 
        	            sHeader->sFieldCheck=sFieldCheckRealloc;
                        memset(&sHeader->sFieldCheck[sHeader->iFieldCheck], 0, sizeof(FIELDCHECK));
                        iRet=getFieldCheck(strFile, strHeader, "AFT", strPtr, &sHeader->sFieldCheck[sHeader->iFieldCheck]);
                        sHeader->iFieldCheck++;
                    }
                    else
                    {
                        dbg(TRACE, "readLoadValueCheck:: ERROR!!! Cannot reallocate sHeader->sFieldCheck"); 
                        iRet=RC_FAIL;
                    }
                }
                if ( iRet==RC_FAIL )
                    break;
                strPtr=strtok_r(NULL, ",", &strPtr2);	       	     
            }
        }
        else
        {
            iRet=RC_FAIL;
            dbg(TRACE, "readLoadValueCheck:: ERROR!!! Cannot Allocate sHeader->strConditions");   
        }
    }

    sHeader->sLoadExp=NULL;
    sHeader->iLoadExp=0;	
    if ( iRet==RC_SUCCESS )
    {
        iTmp=readConfigFile(strFile, strHeader, "VALUE_CHECK", strTmp);
        if ( strlen(strTmp)==0 )
            ;
        else
        {
            dbg(DEBUG, "readLoadValueCheck:: Processing VALUE_CHECK [%s]", strTmp);
        	sHeader->strVC=(char *)malloc(sizeof(char)*(strlen(strTmp)+1));
            if ( sHeader->strVC )
            {
                strcpy(sHeader->strVC, strTmp);	
                
                strPtr=strtok_r(strTmp, ",", &strPtr2);
                while (strPtr)
                {    
                	trim(strPtr, ' ');
                	    
                    iTmp=readConfigFile(strFile, strHeader, strPtr, strVCTmp);
                     if ( strlen(strVCTmp)==0 )
                        ;
                    else
                    {
                        sLoadExpRealloc=(LOADEXP *)realloc(sHeader->sLoadExp, sizeof(LOADEXP)*(sHeader->iLoadExp+1));    
            	        if ( sLoadExpRealloc )          
            	        { 
            	            sHeader->sLoadExp=sLoadExpRealloc;
                            memset(&sHeader->sLoadExp[sHeader->iLoadExp], 0, sizeof(LOADEXP));
                            
                            sHeader->sLoadExp[sHeader->iLoadExp].strName=(char *)malloc(sizeof(char)*(strlen(strPtr)+1));
                            if ( sHeader->sLoadExp[sHeader->iLoadExp].strName )
                                strcpy(sHeader->sLoadExp[sHeader->iLoadExp].strName, strPtr);
                            else
                            {
                                iRet=RC_FAIL;
                                dbg(TRACE, "readLoadValueCheck:: ERROR!!! Cannot allocate sHeader->sLoadExp[%d].strName",
                                    sHeader->iLoadExp); 	
                            }
                               
                            if ( iRet==RC_SUCCESS )
                            {    
                                sHeader->sLoadExp[sHeader->iLoadExp].strString=(char *)malloc(sizeof(char)*(strlen(strVCTmp)+1));
                                if ( sHeader->sLoadExp[sHeader->iLoadExp].strString )
                                    strcpy(sHeader->sLoadExp[sHeader->iLoadExp].strString, strVCTmp);
                                else
                                {
                                    iRet=RC_FAIL;
                                    dbg(TRACE, "readLoadValueCheck:: ERROR!!! Cannot allocate sHeader->sLoadExp[%d].strName",
                                        sHeader->iLoadExp); 	
                                }                      
                            }   
                                
                            if ( iRet==RC_SUCCESS )
                            {                               
                                strPtr3=strtok_r(strVCTmp, ",", &strPtr4);
                                if ( strPtr3 )
                                {
                                	if ( strcmp(strPtr, " ") )
                                	    trim(strPtr, ' ');
                                	    
                            	    sHeader->sLoadExp[sHeader->iLoadExp].strMsg=(char *)malloc(sizeof(char)*(strlen(strPtr3)+1));
                            	    if ( sHeader->sLoadExp[sHeader->iLoadExp].strMsg )
                                        strcpy(sHeader->sLoadExp[sHeader->iLoadExp].strMsg, strPtr3);    
                                    else
                                    {
                                        iRet=RC_FAIL;
                                        dbg(TRACE, "readLoadValueCheck:: ERROR!!! Cannot allocate sHeader->sLoadExp[%d].strMsg",
                                            sHeader->iLoadExp); 	
                                    }                                        	
                                }
                            }
                            
                            if ( iRet==RC_SUCCESS )
                            {                           
                                strPtr3=strtok_r(NULL, ",", &strPtr4);
         	            	
                                if ( strPtr3 )
                                {
                                    if ( !strcmp(strPtr3, " ") )
              	                    {
              	                        sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck1.strValue=(char *)malloc(sizeof(char)*(strlen(strPtr3)+1));
                                   	    if ( sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck1.strValue )
                                   	    {
                                   	        strcpy(sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck1.strValue, strPtr3);	
                                   	    }
                                   	    else
                                   	    { 
                                   	        iRet=RC_FAIL;
                                            dbg(TRACE, "readLoadValueCheck:: Cannot allocate sHeader->sLoadExp[%d].sLoadCheck1.strValue",
                                                sHeader->iLoadExp); 	   	
                                   	    }
               	                    }            	            
                                    else 
                                    {
                                        trim(strPtr3, ' ');
                                        if ( strPtr3[0]=='(' )
                                        {
                                    	    char strF1[257];
                                       	    char strOp[257];
                                       	    char strF2[257];
                                            	
                                            parseLoadExpression(strPtr3, strF1, strOp, strF2);
                                
                                            sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck1.strString=(char *)malloc(sizeof(char)*(strlen(strF1)+1));
                                            if ( sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck1.strString )
                                                strcpy(sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck1.strString, strF1);
                                   	        else
                                   	        { 
                                   	            iRet=RC_FAIL;
                                                dbg(TRACE, "readLoadValueCheck:: Cannot allocate sHeader->sLoadExp[%d].sLoadCheck1.strString",
                                                    sHeader->iLoadExp); 	   	
                                   	        }                                             
                                       
                                            if ( iRet==RC_SUCCESS )
                                            {
                                                iTmp=readConfigFile(strFile, strHeader, strF1, strLoadCheck);
                                                if ( iTmp==RC_FAIL )
                                                    iTmp=readConfigFile(sLoadConfig.strLoadFile, "", strF1, strLoadCheck); 
                                                if ( iTmp==RC_SUCCESS )
                                                    iRet=parseLoadCheck(strLoadCheck, &sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck1); 
                                                else
                                                {
                                                    dbg(TRACE, "readLoadBlank:: ERROR!!! Cannot find [%s] on [dqahdl.cfg] and [%s]", 
                                                        strF1, sLoadConfig.strLoadFile);
                                                    iRet=RC_FAIL;    
                                                }
                                            }
                                            
                                            if ( iRet==RC_SUCCESS )
                                            {
                                                sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck1.strOp=(char *)malloc(sizeof(char)*(strlen(strOp)+1));
                                                if ( sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck1.strOp )
                                                    strcpy(sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck1.strOp, strOp);
                                                else
                                                {
                                                    iRet=RC_FAIL;
                                                    dbg(TRACE, "readLoadValueCheck:: ERROR!!! Cannot Allocate sHeader->sLoadExp[%d].sLoadCheck1.strOp",
                                                        sHeader->iLoadExp);  	
                                                }
                                            }
                                            
                                            if ( iRet==RC_SUCCESS )
                                            { 
                                                sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck1.strValue2=(char *)malloc(sizeof(char)*(strlen(strF2)+1));
                                                if ( sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck1.strValue2 )
                                                    strcpy(sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck1.strValue2, strF2); 
                                                else
                                                {
                                                    iRet=RC_FAIL;
                                                    dbg(TRACE, "readLoadValueCheck:: ERROR!!! Cannot Allocate sHeader->sLoadExp[%d].sLoadCheck1.strValue2",
                                                        sHeader->iLoadExp);  	
                                                }
                                            }                                                                                                                                                       
                                        }   
                                        else if ( isalpha(strPtr3[0]) )
                                        {
                                            sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck1.strString=(char *)malloc(sizeof(char)*(strlen(strPtr3)+1));
                                            if ( sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck1.strString )
                                                strcpy(sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck1.strString, strPtr3);
                                       
                                            iTmp=readConfigFile(strFile, strHeader, strPtr3, strLoadCheck);
                                            if ( iTmp==RC_FAIL )
                                                iTmp=readConfigFile(sLoadConfig.strLoadFile, "", strPtr3, strLoadCheck); 
                                            if ( iTmp==RC_SUCCESS )
                                               iTmp=parseLoadCheck(strLoadCheck, &sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck1); 
                                            else
                                            {
                                                dbg(TRACE, "readLoadBlank:: ERROR!!! Cannot find [%s] on [dqahdl.cfg] and [%s]", 
                                                    strPtr3, sLoadConfig.strLoadFile);
                                                iRet=RC_FAIL;    
                                            }
                                        } 
                                        else
                                        {
                                            sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck1.strValue=(char *)malloc(sizeof(char)*(strlen(strPtr3)+1));
                                  	        if ( sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck1.strValue )
                                  	            strcpy(sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck1.strValue, strPtr3);
                                            else
                                            {
                                                iRet=RC_FAIL;
                                                dbg(TRACE, "readLoadValueCheck:: ERROR!!! Cannot Allocate sHeader->sLoadExp[%d].sLoadCheck1.strValue",
                                                    sHeader->iLoadExp);  	
                                            }                               	            
                                        }
                                    }	
                                }
                            }
                         
                            if ( iRet==RC_SUCCESS )
                            {
                                strPtr3=strtok_r(NULL, ",", &strPtr4);
                                if ( strPtr3 )
                                {           	
                                	if ( strcmp(strPtr3, " ") )
                                	    trim(strPtr3, ' ');
                                	    
                                    sHeader->sLoadExp[sHeader->iLoadExp].strOp=(char *)malloc(sizeof(char)*(strlen(strPtr3)+1));
                                    if ( sHeader->sLoadExp[sHeader->iLoadExp].strOp )
                                        strcpy (sHeader->sLoadExp[sHeader->iLoadExp].strOp, strPtr3);
                                    else
                                    {
                                        iRet=RC_FAIL;
                                        dbg(TRACE, "readLoadValueCheck:: ERROR!!! Cannot Allocate sHeader->sLoadExp[%d].strOp",
                                            sHeader->iLoadExp);  	
                                    }   
                                }
                            }
                            
                            if ( iRet==RC_SUCCESS )
                            {
                                strPtr3=strtok_r(NULL, ",", &strPtr4);
                                if ( strPtr3 )
                                {
                                    if ( !strcmp(strPtr3, " ") )
              	                    {
              	                        sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck2.strValue=(char *)malloc(sizeof(char)*(strlen(strPtr3)+1));
                                   	    if ( sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck2.strValue )
                                   	        strcpy(sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck2.strValue, strPtr3);	
                                        else
                                        {
                                            iRet=RC_FAIL;
                                            dbg(TRACE, "readLoadValueCheck:: ERROR!!! Cannot Allocate sHeader->sLoadExp[%d].sLoadCheck2.strValue",
                                                sHeader->iLoadExp);  	
                                        }                                  	    
               	                    }            	            
                                    else 
                                    {
                                        trim(strPtr3, ' ');
                                        if ( strPtr3[0]=='(' )
                                        {
                                    	    char strF1[257];
                                       	    char strOp[257];
                                       	    char strF2[257];
                                            	
                                            parseLoadExpression(strPtr3, strF1, strOp, strF2);
                               
                                            sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck2.strString=(char *)malloc(sizeof(char)*(strlen(strF1)+1));
                                            if ( sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck2.strString )
                                                strcpy(sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck2.strString, strF1);
                                            else
                                            {
                                                iRet=RC_FAIL;
                                                dbg(TRACE, "readLoadValueCheck:: ERROR!!! Cannot Allocate sHeader->sLoadExp[%d].sLoadCheck2.strString",
                                                    sHeader->iLoadExp);  	
                                            }  
                                                   
                                            if ( iRet==RC_SUCCESS )
                                            {                                  
                                                iTmp=readConfigFile(strFile, strHeader, strF1, strLoadCheck);
                                                if ( iTmp==RC_FAIL )
                                                    iTmp=readConfigFile(sLoadConfig.strLoadFile, "", strF1, strLoadCheck); 
                                                if ( iTmp==RC_SUCCESS )
                                                    iTmp=parseLoadCheck(strLoadCheck, &sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck2); 
                                                else
                                                {
                                                    dbg(TRACE, "readLoadBlank:: ERROR!!! Cannot find [%s] on [dqahdl.cfg] and [%s]", 
                                                        strF1, sLoadConfig.strLoadFile);
                                                    iRet=RC_FAIL;                                             	
                                                }
                                            }
                                            
                                            if ( iRet==RC_SUCCESS )
                                            {
                                                sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck2.strOp=(char *)malloc(sizeof(char)*(strlen(strOp)+1));
                                                if ( sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck2.strOp )
                                                    strcpy(sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck2.strOp, strOp);
                                                else
                                                {
                                                    iRet=RC_FAIL;
                                                    dbg(TRACE, "readLoadValueCheck:: ERROR!!! Cannot Allocate sHeader->sLoadExp[%d].sLoadCheck2.strOp",
                                                        sHeader->iLoadExp);  	
                                                }  
                                            }
                                            
                                            if ( iRet==RC_SUCCESS )
                                            {
                                                sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck2.strValue2=(char *)malloc(sizeof(char)*(strlen(strF2)+1));
                                                if ( sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck2.strValue2 )
                                                    strcpy(sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck2.strValue2, strF2);   
                                                else
                                                {
                                                    iRet=RC_FAIL;
                                                    dbg(TRACE, "readLoadValueCheck:: ERROR!!! Cannot Allocate sHeader->sLoadExp[%d].sLoadCheck2.strValue2",
                                                        sHeader->iLoadExp);  	
                                                }  
                                            } 
                                                                                                                                                                                                
                                        }   
                                        else if ( isalpha(strPtr3[0]) )
                                        {
                                            sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck2.strString=(char *)malloc(sizeof(char)*(strlen(strPtr3)+1));
                                            if ( sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck2.strString )
                                                strcpy(sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck2.strString, strPtr3);
                                            else
                                            {
                                                iRet=RC_FAIL;
                                                dbg(TRACE, "readLoadValueCheck:: ERROR!!! Cannot Allocate sHeader->sLoadExp[%d].sLoadCheck2.strString",
                                                    sHeader->iLoadExp);  	
                                            }  
                                                   
                                            if ( iRet==RC_SUCCESS )
                                            {                                           
                                                iTmp=readConfigFile(strFile, strHeader, strPtr3, strLoadCheck);
                                                if ( iTmp==RC_FAIL )
                                                    iTmp=readConfigFile(sLoadConfig.strLoadFile, "", strPtr3, strLoadCheck); 
                                                if ( iTmp==RC_SUCCESS )
                                                    iTmp=parseLoadCheck(strLoadCheck, &sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck2); 
                                                else
                                                {
                                                    dbg(TRACE, "readLoadBlank:: ERROR!!! Cannot find [%s] on [dqahdl.cfg] and [%s]", 
                                                        strPtr3, sLoadConfig.strLoadFile);
                                                    iRet=RC_FAIL;                                             	
                                                }
                                            }
                                        } 
                                        else
                                        {
                                            sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck2.strValue=(char *)malloc(sizeof(char)*(strlen(strPtr3)+1));
                                  	        if ( sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck2.strValue )
                                  	            strcpy(sHeader->sLoadExp[sHeader->iLoadExp].sLoadCheck2.strValue, strPtr3);
                                  	        else
                                            {
                                                iRet=RC_FAIL;
                                                dbg(TRACE, "readLoadValueCheck:: ERROR!!! Cannot Allocate sHeader->sLoadExp[%d].sLoadCheck2.strValue",
                                                    sHeader->iLoadExp);  	
                                            }  
                                        }
                                    }	
                                }   
                            }
                            
                            if ( iRet==RC_SUCCESS )                                                
                                sHeader->iLoadExp++;                        
                        }
                        else
                        {
                            dbg(TRACE, "readLoadValueCheck:: Cannot reallocate sLoadExpRealloc"); 
                            iRet=RC_FAIL;
                        }
    
                    }
                    
                    if ( iRet==RC_FAIL )
                        break;
                    strPtr=strtok_r(NULL,",", &strPtr2);	
                }
            }
            else
            {
                iRet=RC_FAIL;
                dbg(TRACE, "readLoadValueCheck:: ERROR!!! Cannot Allocate sHeader->strVC");   
            }
    
        }    
    }
    dbg(DEBUG, "readLoadValueCheck:: strFile[%s] strHeader[%s] CountVC[%d] Result[%d]", strFile, strHeader, sHeader->iLoadExp, iRet);
    return iRet;	
}

int parseLoadExpression(char *str, char *strField, char *strOp, char *strField2)
{
    int iRet=0;
    char *strPtr;
    int a=0;
    
    ltrim(str, '(');
    ltrim(str, ' ');

    a=0;
    strPtr=str;                        
    while (strPtr)
    {
        if ( isalnum(strPtr[0]) || strPtr[0]=='_' ) 
        {
            strField[a++]=strPtr[0];
            strPtr++;
        }
        else
            break;                                                  	
    } 
    strField[a]=0;
    
    while (strPtr)
    {
        if (strPtr[0]!=' ')
            break;
        strPtr++;
    }

    if (strPtr)
    {
        strOp[0]=strPtr[0];
        strOp[1]=0;
    }
    
    strPtr++;
    while (strPtr)
    {
        if (strPtr[0]!=' ')
            break;	
        strPtr++;
    }


    if ( strPtr )
    {
        a=0;
        while (strPtr)
        {
            if ( strPtr[0]==')' || strPtr[0]==' ' || strPtr[0]==0 )
                break;
            strField2[a++]=strPtr[0];
            strPtr++;	
        }	
        strField2[a]=0;
    }   
                       
    return iRet;
}

int parseLoadCheck(char *str, LOADCHECK *sLoad)
{
    int iRet=RC_SUCCESS;
    char *strPtr, *strPtr2;
    char strTmp[257];
    int a=0;
    int iDB;
    DBFIELDSCONFIG *sDB;
    DBFIELDSCONFIG *sDBFieldsConfigRealloc;
    
    dbg(TRACE, "parseLoadCheck:: [%s]", str);
    
    if ( !strcmp(str, "") )
    {
    	dbg(DEBUG, "parseLoadCheck:: ERROR!!! Empty string []");
        iRet=RC_FAIL;    	
    }
    else
    {
        memset(strTmp, 0, sizeof(strTmp));    
        strPtr=strstr(str, "[");
        strPtr++;
        
        if ( strPtr )
        {      
            sLoad->sCond.strFieldBlankCheck=(char *)malloc(sizeof(char)*5);
            if ( sLoad->sCond.strFieldBlankCheck ) 
            {
                strcpy(sLoad->sCond.strFieldBlankCheck, "VALU");
           
                if ( getArrTabFieldIndex("VALU",arrLoatabDBFieldsConfig, iArrLoatabDBFieldsConfigCount)==-1 )
                { 
                    sDB=(DBFIELDSCONFIG *)malloc(sizeof(DBFIELDSCONFIG));
                    if ( sDB )
                    {
                        memset(sDB, 0, sizeof(DBFIELDSCONFIG));
                        iDB=addArrDBFields("VALU", "LOA", sDB, &strLoatabFieldNames);                     
                        if ( iDB==RC_SUCCESS )
                        {
                            sDBFieldsConfigRealloc=(DBFIELDSCONFIG *)realloc(arrLoatabDBFieldsConfig, sizeof(DBFIELDSCONFIG)*(iArrLoatabDBFieldsConfigCount+1)); 
                            if ( sDBFieldsConfigRealloc )
                            {
                                arrLoatabDBFieldsConfig=sDBFieldsConfigRealloc;
                                memset(&arrLoatabDBFieldsConfig[iArrLoatabDBFieldsConfigCount], 0, sizeof(DBFIELDSCONFIG));             
                                arrLoatabDBFieldsConfig[iArrLoatabDBFieldsConfigCount]=*sDB;
                                iArrLoatabDBFieldsConfigCount++;  
                            }          	
                            else
                            {
                                dbg(TRACE, "parseLoadCheck:: ERROR!!! addArrDBFields() failed");	
                                iRet=RC_FAIL;
                            }	
                        }	
                        else
                        {
                        	dbg(TRACE, "parseLoadCheck:: ERROR!!! Cannot reallocate arrLoatabDBFieldsConfig");	
                            iRet=RC_FAIL;	
                        }	
               	        free(sDB);
               	        sDB=NULL;
                    }
                    else
                    {
                        iRet=RC_FAIL;
                        dbg(TRACE, "parseLoadCheck:: ERROR!!! Cannot allocate sDB");                   	
                    }
                }
            }
            else
            {
                iRet=RC_FAIL;
                dbg(TRACE, "parseLoadCheck:: ERROR!!! Cannot allocate sLoad->sCond.strFieldBlankCheck");	
            }
            
            if ( iRet==RC_SUCCESS )
            {
                while ( strPtr )
                {
                	if ( strPtr[0]=='|' )
                		break;
                    else
                        strTmp[a++]=strPtr[0];	
                    strPtr++;
                }   
                
                if ( strlen(strTmp) )
                {
                    sLoad->sCond.strField=(char *)malloc(sizeof(char)*(strlen(strTmp)+1));
                    if ( sLoad->sCond.strField )
                    { 
                        strcpy(sLoad->sCond.strField, strTmp);
               
                        if ( getArrTabFieldIndex(strTmp,arrLoatabDBFieldsConfig, iArrLoatabDBFieldsConfigCount)==-1 )
                        { 
                            sDB=(DBFIELDSCONFIG *)malloc(sizeof(DBFIELDSCONFIG));
                            if ( sDB )
                            {
                                memset(sDB, 0, sizeof(DBFIELDSCONFIG));
                                iDB=addArrDBFields(strTmp, "LOA", sDB, &strLoatabFieldNames);
                                if ( iDB==RC_SUCCESS )
                                {
                                    sDBFieldsConfigRealloc=(DBFIELDSCONFIG *)realloc(arrLoatabDBFieldsConfig, sizeof(DBFIELDSCONFIG)*(iArrLoatabDBFieldsConfigCount+1)); 
                                    if ( sDBFieldsConfigRealloc )
                                    {
                                        arrLoatabDBFieldsConfig=sDBFieldsConfigRealloc;
                                        memset(&arrLoatabDBFieldsConfig[iArrLoatabDBFieldsConfigCount], 0, sizeof(DBFIELDSCONFIG));             
                                        arrLoatabDBFieldsConfig[iArrLoatabDBFieldsConfigCount]=*sDB;
                                        iArrLoatabDBFieldsConfigCount++;  
                                    }          	
                                    else
                                    {
                                        dbg(TRACE, "parseLoadCheck:: ERROR!!! Cannot reallocate arrLoatabDBFieldsConfig");	
                                        iRet=RC_FAIL;
                                    }	
                                }	
                                else
                                {
                                    iRet=RC_FAIL;
                                    dbg(TRACE, "parseLoadCheck:: ERROR!!! addArrDBFields Failed");                                    	
                                }
                   	            free(sDB);
                   	            sDB=NULL;
                            }
                            else
                            {
                                iRet=RC_FAIL;
                                dbg(TRACE, "parseLoadCheck:: ERROR!!! Cannot allocate sDB");                               	
                            }
                        }          
                    }   
                    else
                    {
                        iRet=RC_FAIL;
                        dbg(TRACE, "parseLoadCheck:: ERROR!!! Cannot allocate sLoad->sCond.strField");                    	
                    }     
                }
            }
            
            if ( iRet==RC_SUCCESS )
            {
                if ( strPtr )
                {        
                    while ( strPtr )
                    {
                        if ( strPtr[0]==',' )
                            break;
                         strPtr++;
                    }
                         
                    strPtr++;     
                    
                    while ( strPtr )
                    {
                        if ( strPtr[0]!=' ' )
                            break;
                         strPtr++;
                    }  
                   
                    if ( strPtr )
                    {
                        memset(strTmp, 0, sizeof(strTmp));       
                        a=0;
                        while ( strPtr )
                        { 
                            if ( strPtr[0]==']' )
                                break;
                            strTmp[a++]=strPtr[0];
                            strPtr++;
                        }  
                
                        for (a=strlen(strTmp)-1; a>0; a--)
                        {
                            if ( !isalnum(strTmp[a]) )
                                strTmp[a]=0;
                            else
                                break;
                        }
        
                        sLoad->sCond.strValue=(char *)malloc(sizeof(char)*(strlen(strTmp)+1));
                        if ( sLoad->sCond.strValue )
                            strcpy(sLoad->sCond.strValue, strTmp);       
                        else
                        {
                            iRet=RC_FAIL;
                            dbg(TRACE, "parseLoadCheck:: ERROR!!! Cannot allocate  sLoad->sCond.strValue");   
                        }
        
                        strcpy(strTmp, "IS");
                        sLoad->sCond.strOp=(char *)malloc(sizeof(char)*(strlen(strTmp)+1));
                        if ( sLoad->sCond.strOp )
                            strcpy(sLoad->sCond.strOp, strTmp);  
                        else
                        {
                            iRet=RC_FAIL;
                            dbg(TRACE, "parseLoadCheck:: ERROR!!! Cannot allocate  sLoad->sCond.strOp");   
                        }
                    }
                }
            }
        }       
    
        if ( iRet==RC_SUCCESS )
        {
            if ( strPtr )
            {
                while ( strPtr )
                {
                    if ( strPtr[0]=='[' )
                        break;
                    strPtr++;
                }
                     
                strPtr++;     
                while ( strPtr )
                {
                	if ( strPtr[0]!=' ' )
                        break;
                    strPtr++;
                }  
                
                strcpy(strTmp, strPtr);
                strPtr=strtok_r(strTmp, ",",&strPtr2);
                if ( strPtr )
                {
                	sLoad->strType=(char *)malloc(sizeof(char)*(strlen(strPtr)+1));
                	if ( sLoad->strType )
                	{                		
                		if ( !strcmp(strPtr, "#") )
                			strcpy(sLoad->strType, " ");
                		else
                            strcpy(sLoad->strType, strPtr);
                    }
                    else
                    {
                        iRet=RC_FAIL;
                        dbg(TRACE, "parseLoadCheck:: ERROR!!! Cannot allocate  sLoad->strType");   
                    }                        
                }
                
                strPtr=strtok_r(NULL, ",",&strPtr2);
                if ( strPtr )
                {
                	sLoad->strStyp=(char *)malloc(sizeof(char)*(strlen(strPtr)+1));
                	if ( sLoad->strStyp )
                	{
                		if ( !strcmp(strPtr, "#") )
                		    strcpy(sLoad->strStyp, " ");
                		else
                            strcpy(sLoad->strStyp, strPtr);
                    }
                    else
                    {
                        iRet=RC_FAIL;
                        dbg(TRACE, "parseLoadCheck:: ERROR!!! Cannot allocate sLoad->strStyp");   
                    } 
                }
            
                strPtr=strtok_r(NULL, ",",&strPtr2);
                if ( strPtr )
                {
                	sLoad->strSstp=(char *)malloc(sizeof(char)*(strlen(strPtr)+1));
                	if ( sLoad->strSstp )
                	{
                		if ( !strcmp(strPtr, "#") )
                			strcpy(sLoad->strSstp, " ");
                		else
                            strcpy(sLoad->strSstp, strPtr);
                    }
                    else
                    {
                        iRet=RC_FAIL;
                        dbg(TRACE, "parseLoadCheck:: ERROR!!! Cannot allocate sLoad->strSstp");   
                    } 
                }
                
                strPtr=strtok_r(NULL, ",",&strPtr2);
                if ( strPtr )
                {
                	sLoad->strSsst=(char *)malloc(sizeof(char)*(strlen(strPtr)+1));
                	if ( sLoad->strSsst )
                	{
                        strcpy(sLoad->strSsst, strPtr);
                        if ( sLoad->strSsst[strlen(sLoad->strSsst)-1] == ']' )
                            sLoad->strSsst[strlen(sLoad->strSsst)-1]=0;
                        if ( !strcmp(sLoad->strSsst, "#") )
                          	strcpy(sLoad->strSsst, " ");
                    }
                    else
                    {
                        iRet=RC_FAIL;
                        dbg(TRACE, "parseLoadCheck:: ERROR!!! Cannot allocate sLoad->strSsst");   
                    } 
                }   
                else
                	sLoad->strSsst=NULL;
                	
                strPtr=strtok_r(NULL, ",",&strPtr2);
                if ( strPtr )
                {
                	if ( strPtr[strlen(strPtr)-1] == ']' )
                        strPtr[strlen(strPtr)-1]=0;
                	if ( !strcmp(strPtr, "-") )
                	{
                	    sLoad->strApc3=(char *)malloc(sizeof(char)*(strlen(cgHopo)+1));
                	    if ( sLoad->strApc3 )
                	        strcpy(sLoad->strApc3, cgHopo);
                	    else
                	    {
                            iRet=RC_FAIL;
                            dbg(TRACE, "parseLoadCheck:: ERROR!!! Cannot allocate sLoad->strApc3");                 	    	
                	    }
                		
                	}
                	else
                	{
                	    sLoad->strApc3=(char *)malloc(sizeof(char)*(strlen(strPtr)+1));
                	    if ( sLoad->strApc3 )
                	    {
                	        strcpy(sLoad->strApc3, strPtr);
                	        if ( !strcmp(sLoad->strApc3, "#") )
                	            strcpy(sLoad->strApc3, " ");
                	    }
                        else
                        {
                            iRet=RC_FAIL;
                            dbg(TRACE, "parseLoadCheck:: ERROR!!! Cannot allocate sLoad->strSsst");   
                        }
                    } 
                }  
                else
                    sLoad->strApc3=NULL;	 
                     
            }
        }
    }
    
    dbg(TRACE, "parseLoadCheck:: [%s] Result[%d]", str, iRet);        
    return iRet;	
}

int parseExpression(char *str, EXPRESSION *sExp)
{
    int iRet=RC_SUCCESS;
    char strTmp[1024];
    int iIndex=0;
    int a=0;
    int iSize=strlen(str);
    int iDB;
    DBFIELDSCONFIG *sDB, *sDBFieldsConfigRealloc;
    
    dbg(DEBUG, "parseExpression:: str[%s]", str);
    
    sExp->strDataType=(char *)malloc(sizeof(char)*9);
    if ( sExp->strDataType )
    {
        strcpy(sExp->strDataType, "DATETIME");   
            
        if ( str[iIndex]=='(' )
            iIndex++;
        
        if ( !isalnum(str[iIndex]) )
            sExp->cUnary1=str[iIndex++];
        else
        	sExp->cUnary1=0;	
        	
        memset(&strTmp, 0, sizeof(strTmp));
        for (;iIndex<iSize;iIndex++)
        {
            if ( isalnum(str[iIndex]) )
         	    strTmp[a++]=str[iIndex];  
            else 	
                break;
        }    

        sExp->strField1=(char *)malloc(sizeof(char)*(strlen(strTmp)+1));
        if ( sExp->strField1 )
             strcpy(sExp->strField1, strTmp);
        else
        {
            dbg(TRACE, "parseExpression:: ERROR!!! Cannot allocate sExp->strField1");
            iRet=RC_FAIL;
        }
        
        if ( iRet==RC_SUCCESS )
        {
            if ( strlen(strTmp)==4 && isalpha(strTmp[0]) )
            {
                if ( getArrTabFieldIndex(strTmp, arrAfttabDBFieldsConfig, iArrAfttabDBFieldsConfigCount)==-1 )
                {
                    sDB=(DBFIELDSCONFIG *)malloc(sizeof(DBFIELDSCONFIG));
                    if ( sDB )
                    {
            	        memset(sDB, 0, sizeof(DBFIELDSCONFIG));
                        iDB=addArrDBFields(strTmp, "AFT", sDB, &strAfttabFieldNames);
                        if ( iDB==RC_SUCCESS )
                        {
                            sDBFieldsConfigRealloc=(DBFIELDSCONFIG *)realloc(arrAfttabDBFieldsConfig, sizeof(DBFIELDSCONFIG)*(iArrAfttabDBFieldsConfigCount+1)); 
                            if ( sDBFieldsConfigRealloc )
                            {
                            	arrAfttabDBFieldsConfig=sDBFieldsConfigRealloc;
                                memset(&arrAfttabDBFieldsConfig[iArrAfttabDBFieldsConfigCount], 0, sizeof(DBFIELDSCONFIG));             
                                arrAfttabDBFieldsConfig[iArrAfttabDBFieldsConfigCount]=*sDB;
                                iArrAfttabDBFieldsConfigCount++;            		
                            }	
                            else
                            {
                                dbg(TRACE, "parseExpression:: ERROR!!! Cannot reallocate arrAfttabDBFieldsConfig");
                                iRet=RC_FAIL;
                            }
                        }
                        else
                        {
                        	dbg(TRACE, "parseExpression:: ERROR!!! addArrDBFields Failed");
                            iRet=RC_FAIL;	
                        }
               	        free(sDB);
               	        sDB=NULL;
               	    }
               	    else
               	    {
               	        dbg(TRACE, "parseExpression:: ERRROR!!! Cannot allocate sDB for strField1");
               	        iRet=RC_FAIL;
               	    }
                }
            }
        }
           
        if ( iRet==RC_SUCCESS )
        {
            a=0;
            memset(&strTmp, 0, sizeof(strTmp));
            if ( iIndex<iSize )
            {
                strTmp[a++]=str[iIndex++];
                if ( iIndex<iSize )
                {
                    if ( !isalnum(str[iIndex]) )
                    {
                        if ( str[iIndex]=='+' || str[iIndex]=='-' )	
                            ;
                        else
                	        strTmp[a++]=str[iIndex++];	
                    }
                }
                sExp->strOp=(char *)malloc(sizeof(char)*(strlen(strTmp)+1));
                if ( sExp->strOp )
                    strcpy(sExp->strOp, strTmp);
                else
                {
                    dbg(TRACE, "parseExpression:: Cannot allocate sExp->strOp");
                    iRet=RC_FAIL;
                }
            }
        }
        
        if ( iRet==RC_SUCCESS )
        {
            if ( iIndex<iSize )
            {        
                if ( !isalnum(str[iIndex]) )
                    sExp->cUnary2=str[iIndex++]; 
                else
                    sExp->cUnary2=0; 
            }
    
            a=0;
            memset(&strTmp, 0, sizeof(strTmp));
            for (;iIndex<iSize;iIndex++)
            {
                if ( isalnum(str[iIndex]) )
                	strTmp[a++]=str[iIndex];   
                else
                    break;	   
            }    
            
            sExp->strField2=(char *)malloc(sizeof(char)*(strlen(strTmp)+1));
            if ( sExp->strField2 )
                strcpy(sExp->strField2, strTmp);
            else
            {
            	iRet=RC_FAIL;
                dbg(TRACE, "parseExpression:: ERROR!!! Cannot allocate sExp->strField2");
            }
              
            if ( iRet==RC_SUCCESS )
            {  
                if ( strlen(strTmp)==4 && isalpha(strTmp[0]) )
                {
                    if ( getArrTabFieldIndex(strTmp, arrAfttabDBFieldsConfig, iArrAfttabDBFieldsConfigCount)==-1 )
                    {
                    	sDB=(DBFIELDSCONFIG *)malloc(sizeof(DBFIELDSCONFIG));
                    	if ( sDB )
                    	{
                	        memset(sDB, 0, sizeof(DBFIELDSCONFIG));
                            iDB=addArrDBFields(strTmp, "AFT", sDB, &strAfttabFieldNames);
                            if ( iDB==RC_SUCCESS )
                            {
                                sDBFieldsConfigRealloc=(DBFIELDSCONFIG *)realloc(arrAfttabDBFieldsConfig, sizeof(DBFIELDSCONFIG)*(iArrAfttabDBFieldsConfigCount+1)); 
                                if ( sDBFieldsConfigRealloc )
                                {
                                	arrAfttabDBFieldsConfig=sDBFieldsConfigRealloc;
                                    memset(&arrAfttabDBFieldsConfig[iArrAfttabDBFieldsConfigCount], 0, sizeof(DBFIELDSCONFIG));             
                                    arrAfttabDBFieldsConfig[iArrAfttabDBFieldsConfigCount]=*sDB;
                                    iArrAfttabDBFieldsConfigCount++;    
                                }     
                                else
                                {
                                	iRet=RC_FAIL;
                                    dbg(TRACE, "parseExpression:: Cannot reallocate arrAfttabDBFieldsConfig");  		
                                }
                            }	
                            else
                            {
                        	    dbg(TRACE, "parseExpression:: ERROR!!! addArrDBFields Failed");
                                iRet=RC_FAIL;	
                            }
                   	        free(sDB);
                   	        sDB=NULL;
                   	    }
                   	    else
                   	        dbg(TRACE, "parseExpression:: Cannot allocate sDB for sExp->Field2");
                	}   
                }
            }
        }
    }    
    else
    {
        dbg(TRACE, "parseExpression:: ERROR!!! Cannot allocate sExp->strDataType"); 
        iRet=RC_FAIL;
    }
    
    dbg(DEBUG, "parseExpression:: str[%s] Result[%d]", str, iRet);
           
    return iRet;	
}
void printConfig()
{
	int a=0, b=0;
	
	dbg(TRACE, "Implementation Date: [%s]", pcgImplementationDate);
	dbg(TRACE, "Additional Minutes: [%d]", igAdditionalMinutes);
	dbg(TRACE, "Flight Queue No: [%d]", igFlightQueue);
	dbg(TRACE, "DQA Commands: [%s]", pcgDqaCmd);
	dbg(TRACE, "Move Commands: [%s]", pcgMoveCmd);
	dbg(TRACE, "Load Commands: [%s]", pcgLoadCmd);
	
	dbg(TRACE, "DQC Conditions:");
	dbg(TRACE, "     strConditions[%s][%d]", rgDQC.strConditions, rgDQC.iFieldCheck);
    for (b=0; b<rgDQC.iFieldCheck; b++)
    {
        dbg(TRACE, "          sFieldCheck[%d]: strName[%s] strField[%s] strOp[%s] strValue[%s]", 
            b, rgDQC.sFieldCheck[b].strName, rgDQC.sFieldCheck[b].strField,
            rgDQC.sFieldCheck[b].strOp, rgDQC.sFieldCheck[b].strValue);	
        	
    }  
    
    dbg(TRACE, "Configuration Move");
    dbg(TRACE, "     strAfttabDQCSUpdate [%s]", sMoveConfig.strAfttabDQCSUpdate);
    dbg(TRACE, "     strType [%s]", sMoveConfig.strType);
 
    dbg(TRACE, "     strConditions[%s][%d]", sMoveConfig.strConditions, sMoveConfig.iFieldCheck);
    for (b=0; b<sMoveConfig.iFieldCheck; b++)
    {
        dbg(TRACE, "          sFieldCheck[%d]: strName[%s] strField[%s] strOp[%s] strValue[%s]", 
            b, sMoveConfig.sFieldCheck[b].strName, sMoveConfig.sFieldCheck[b].strField,
            sMoveConfig.sFieldCheck[b].strOp, sMoveConfig.sFieldCheck[b].strValue);	
        	
    }  
        
    dbg(TRACE, "     strBlankReason [%s]", sMoveConfig.strBlankReason);
    dbg(TRACE, "     strAllBlankCheck [%s]", sMoveConfig.strAllBlankCheck);
    dbg(TRACE, "     iArrAllBlankCheckCount [%d]", sMoveConfig.iArrAllBlankCheckCount);
    
  
        
    for (a=0; a<sMoveConfig.iArrAllBlankCheckCount; a++)
    {
        dbg(TRACE, "     arrAllBlankCheck[%d]", a);
        dbg(TRACE, "          strName[%s]", sMoveConfig.arrAllBlankCheck[a].strName);
        dbg(TRACE, "          strString[%s]", sMoveConfig.arrAllBlankCheck[a].strString);
        dbg(TRACE, "          strFieldBlankCheck[%s]", sMoveConfig.arrAllBlankCheck[a].strFieldBlankCheck);
        dbg(TRACE, "          strField[%s]", sMoveConfig.arrAllBlankCheck[a].strField);
        dbg(TRACE, "          strOp[%s]", sMoveConfig.arrAllBlankCheck[a].strOp);
        dbg(TRACE, "          strValue[%s]", sMoveConfig.arrAllBlankCheck[a].strValue);    	
    }    
    
    dbg(TRACE, "     strArrBlankCheck [%s]", sMoveConfig.strArrBlankCheck);
    dbg(TRACE, "     iArrArrBlankCheckCount [%d]", sMoveConfig.iArrArrBlankCheckCount);
    for (a=0; a<sMoveConfig.iArrArrBlankCheckCount; a++)
    {
        dbg(TRACE, "     arrArrBlankCheck[%d]", a);
        dbg(TRACE, "          strName[%s]", sMoveConfig.arrArrBlankCheck[a].strName);
        dbg(TRACE, "          strString[%s]", sMoveConfig.arrArrBlankCheck[a].strString);
        dbg(TRACE, "          strFieldBlankCheck[%s]", sMoveConfig.arrArrBlankCheck[a].strFieldBlankCheck);
        dbg(TRACE, "          strField[%s]", sMoveConfig.arrArrBlankCheck[a].strField);
        dbg(TRACE, "          strOp[%s]", sMoveConfig.arrArrBlankCheck[a].strOp);
        dbg(TRACE, "          strValue[%s]", sMoveConfig.arrArrBlankCheck[a].strValue);    	
    }
    
    dbg(TRACE, "     strDepBlankCheck [%s]", sMoveConfig.strDepBlankCheck);
    dbg(TRACE, "     iArrDepBlankCheckCount [%d]", sMoveConfig.iArrAllBlankCheckCount);    
    for (a=0; a<sMoveConfig.iArrDepBlankCheckCount; a++)
    {
        dbg(TRACE, "     arrDepBlankCheck[%d]", a);
        dbg(TRACE, "          strName[%s]", sMoveConfig.arrDepBlankCheck[a].strName);
        dbg(TRACE, "          strString[%s]", sMoveConfig.arrDepBlankCheck[a].strString);
        dbg(TRACE, "          strFieldBlankCheck[%s]", sMoveConfig.arrDepBlankCheck[a].strFieldBlankCheck);
        dbg(TRACE, "          strField[%s]", sMoveConfig.arrDepBlankCheck[a].strField);
        dbg(TRACE, "          strOp[%s]", sMoveConfig.arrDepBlankCheck[a].strOp);
        dbg(TRACE, "          strValue[%s]", sMoveConfig.arrDepBlankCheck[a].strValue);    	
    }
    
    dbg(TRACE, "     strValueCheck[%s]", sMoveConfig.strValueCheck);
    dbg(TRACE, "     iArrValueCheckCount[%d]", sMoveConfig.iArrValueCheckCount);
        
    for (a=0; a<sMoveConfig.iArrValueCheckCount; a++)
    {
    	dbg(TRACE, "     arrValueCheck[%d]: strName[%s] strString[%s]", a, sMoveConfig.arrValueCheck[a].strName, sMoveConfig.arrValueCheck[a].strString);
        dbg(TRACE, "     arrValueCheck[%d]: strFlightType[%s], strMsg[%s], strExp1[%s], strOp[%s], strExp2[%s], strValue[%s]",
            a, sMoveConfig.arrValueCheck[a].strFlightType, sMoveConfig.arrValueCheck[a].strMsg, sMoveConfig.arrValueCheck[a].strExp1,
            sMoveConfig.arrValueCheck[a].strOp, sMoveConfig.arrValueCheck[a].strExp2, sMoveConfig.arrValueCheck[a].strValue);
        dbg(TRACE, "     sExp1[%d]: strDataType[%s], cUnary1[%c], strField1[%s], strOp[%s], cUnary2[%c], strField2[%s]",
            a, sMoveConfig.arrValueCheck[a].sExp1.strDataType, sMoveConfig.arrValueCheck[a].sExp1.cUnary1, sMoveConfig.arrValueCheck[a].sExp1.strField1, 
            sMoveConfig.arrValueCheck[a].sExp1.strOp, sMoveConfig.arrValueCheck[a].sExp1.cUnary2, sMoveConfig.arrValueCheck[a].sExp1.strField2);
        dbg(TRACE, "     sExp2[%d]: strDataType[%s], cUnary1[%c], strField1[%s], strOp[%s], cUnary2[%c], strField2[%s]",
            a, sMoveConfig.arrValueCheck[a].sExp2.strDataType, sMoveConfig.arrValueCheck[a].sExp2.cUnary1, sMoveConfig.arrValueCheck[a].sExp2.strField1, 
            sMoveConfig.arrValueCheck[a].sExp2.strOp, sMoveConfig.arrValueCheck[a].sExp2.cUnary2, sMoveConfig.arrValueCheck[a].sExp2.strField2);
    }
    

    dbg(TRACE, "     strAfttabFieldNames[%s]", strAfttabFieldNames);
    dbg(TRACE, "     iArrAfttabDBFieldsConfigCount[%d]", iArrAfttabDBFieldsConfigCount);
    for (a=0; a<iArrAfttabDBFieldsConfigCount; a++)
    {
        dbg(TRACE, "     arrAfttabDBFieldsConfig[%d]: strFieldName[%s], strFieldDesc[%s]",
            a, arrAfttabDBFieldsConfig[a].strFieldName, arrAfttabDBFieldsConfig[a].strFieldDesc);
        	
    }


    dbg(TRACE, "Configuration Load");
    dbg(TRACE, "     strAfttabDQCSUpdate [%s]", sLoadConfig.strAfttabDQCSUpdate);
    dbg(TRACE, "     strType [%s]", sLoadConfig.strType);
    dbg(TRACE, "     strFlightRef [%s]", sLoadConfig.strFlightRef);
    dbg(TRACE, "     strLoadBlankCheckSession [%s]", sLoadConfig.strLoadBlankCheckSession);
    dbg(TRACE, "     strLoadValueCheckSession [%s]", sLoadConfig.strLoadValueCheckSession);
    dbg(TRACE, "     strLoadFile [%s]", sLoadConfig.strLoadFile);

    dbg(TRACE, "     iLoadBlank[%d]", sLoadConfig.iLoadBlank);
    for (a=0; a<sLoadConfig.iLoadBlank; a++)
    {
        dbg(TRACE, "     sLoadBlank[%d]: strHeader[%s] Conditions[%s] ValueCheck[%s] ", 
            a, sLoadConfig.sLoadBlank[a].strHeader, sLoadConfig.sLoadBlank[a].strConditions, sLoadConfig.sLoadBlank[a].strValueCheck); 
        dbg(TRACE, "     sLoadBlank[%d]: FieldCheck[%d] ", 
            a, sLoadConfig.sLoadBlank[a].iFieldCheck); 
              	
        for (b=0; b<sLoadConfig.sLoadBlank[a].iFieldCheck; b++)
        {
            dbg(TRACE, "          sFieldCheck[%d]: strName[%s] strField[%s] strOp[%s] strValue[%s]", 
                b, sLoadConfig.sLoadBlank[a].sFieldCheck[b].strName, sLoadConfig.sLoadBlank[a].sFieldCheck[b].strField,
                sLoadConfig.sLoadBlank[a].sFieldCheck[b].strOp, sLoadConfig.sLoadBlank[a].sFieldCheck[b].strValue);	
        	
        }    
        
        dbg(TRACE, "     sLoadBlank[%d]: iLoadCheck[%d] ", 
            a, sLoadConfig.sLoadBlank[a].iLoadCheck); 

        for (b=0; b<sLoadConfig.sLoadBlank[a].iLoadCheck; b++)
        {
            dbg(TRACE, "          sLoadCheck[%d]: strName[%s] strString[%s]", 
                b, sLoadConfig.sLoadBlank[a].sLoadCheck[b].strName, sLoadConfig.sLoadBlank[a].sLoadCheck[b].strString);	
            dbg(TRACE, "          sLoadCheck[%d]: strMsg[%s] ", 
                b, sLoadConfig.sLoadBlank[a].sLoadCheck[b].strMsg);	
            dbg(TRACE, "          sLoadCheck[%d]: strType[%s] strStyp[%s] strSstp[%s], strSsst[%s], strApc3[%s]",
                b, sLoadConfig.sLoadBlank[a].sLoadCheck[b].strType,
                sLoadConfig.sLoadBlank[a].sLoadCheck[b].strStyp,
                sLoadConfig.sLoadBlank[a].sLoadCheck[b].strSstp,
                sLoadConfig.sLoadBlank[a].sLoadCheck[b].strSsst,
                sLoadConfig.sLoadBlank[a].sLoadCheck[b].strApc3); 

            dbg(TRACE, "          sLoadCheck[%d]: sCond: strField[%s], strOp[%s], strValue[%s]",
                b, 
                sLoadConfig.sLoadBlank[a].sLoadCheck[b].sCond.strField,
                sLoadConfig.sLoadBlank[a].sLoadCheck[b].sCond.strOp,
                sLoadConfig.sLoadBlank[a].sLoadCheck[b].sCond.strValue); 

                   
            dbg(TRACE, "          sLoadCheck[%d]: strOp[%s], strValue[%s]", 
                b, sLoadConfig.sLoadBlank[a].sLoadCheck[b].strOp,
                sLoadConfig.sLoadBlank[a].sLoadCheck[b].strValue);	
      	   
        }    
                   
    }
     
    dbg(TRACE, "     iLoadVC[%d]", sLoadConfig.iLoadVC);
    for (a=0; a<sLoadConfig.iLoadVC; a++)
    {
        dbg(TRACE, "     sLoadVC[%d]: strHeader[%s] ", a, sLoadConfig.sLoadVC[a].strHeader);
        dbg(TRACE, "          strConditions[%s][%d]", sLoadConfig.sLoadVC[a].strConditions, sLoadConfig.sLoadVC[a].iFieldCheck);
        for (b=0; b<sLoadConfig.sLoadVC[a].iFieldCheck; b++)
        {
            dbg(TRACE, "          sFieldCheck[%d]: strName[%s] strField[%s] strOp[%s] strValue[%s]", 
                b, sLoadConfig.sLoadVC[a].sFieldCheck[b].strName, sLoadConfig.sLoadVC[a].sFieldCheck[b].strField,
                sLoadConfig.sLoadVC[a].sFieldCheck[b].strOp, sLoadConfig.sLoadVC[a].sFieldCheck[b].strValue);	
        }   
        
        dbg(TRACE, "          strVC[%s][%d]", sLoadConfig.sLoadVC[a].strVC, sLoadConfig.sLoadVC[a].iLoadExp);
        for (b=0; b<sLoadConfig.sLoadVC[a].iLoadExp; b++)
        {
            dbg(TRACE, "               sLoadExp[%d]: strName[%s]",  b, sLoadConfig.sLoadVC[a].sLoadExp[b].strName);
            dbg(TRACE, "               sLoadExp[%d]: strMsg[%s]",  b, sLoadConfig.sLoadVC[a].sLoadExp[b].strMsg);
            dbg(TRACE, "               sLoadExp[%d]: strString[%s]",  b, sLoadConfig.sLoadVC[a].sLoadExp[b].strString);
            dbg(TRACE, "               sLoadCheck1: strString[%s]",  sLoadConfig.sLoadVC[a].sLoadExp[b].sLoadCheck1.strString);
            dbg(TRACE, "               sLoadCheck1: sCond: strFieldBlankCheck[%s] strName[%s] strField[%s] strOp[%s] strValue[%s]",  
                sLoadConfig.sLoadVC[a].sLoadExp[b].sLoadCheck1.sCond.strFieldBlankCheck,
                sLoadConfig.sLoadVC[a].sLoadExp[b].sLoadCheck1.sCond.strName,
                sLoadConfig.sLoadVC[a].sLoadExp[b].sLoadCheck1.sCond.strField,
                sLoadConfig.sLoadVC[a].sLoadExp[b].sLoadCheck1.sCond.strOp,
                sLoadConfig.sLoadVC[a].sLoadExp[b].sLoadCheck1.sCond.strValue
                );
            dbg(TRACE, "               sLoadCheck1: strType[%s] strStyp[%s] strSstp[%s] strSsst[%s] strApc3[%s]",  
                sLoadConfig.sLoadVC[a].sLoadExp[b].sLoadCheck1.strType,
                sLoadConfig.sLoadVC[a].sLoadExp[b].sLoadCheck1.strStyp,
                sLoadConfig.sLoadVC[a].sLoadExp[b].sLoadCheck1.strSstp,
                sLoadConfig.sLoadVC[a].sLoadExp[b].sLoadCheck1.strSsst,
                sLoadConfig.sLoadVC[a].sLoadExp[b].sLoadCheck1.strApc3
               );
            dbg(TRACE, "               sLoadCheck1: strValue[%s] strOp[%s] strValue2[%s]",  
                sLoadConfig.sLoadVC[a].sLoadExp[b].sLoadCheck1.strValue,
                sLoadConfig.sLoadVC[a].sLoadExp[b].sLoadCheck1.strOp,
                sLoadConfig.sLoadVC[a].sLoadExp[b].sLoadCheck1.strValue2);
                
            dbg(TRACE, "               strOp [%s]", sLoadConfig.sLoadVC[a].sLoadExp[b].strOp);
            
            dbg(TRACE, "               sLoadCheck2: strString[%s]",  sLoadConfig.sLoadVC[a].sLoadExp[b].sLoadCheck2.strString);
            dbg(TRACE, "               sLoadCheck2: sCond: strFieldBlankCheck[%s] strName[%s] strField[%s] strOp[%s] strValue[%s]",  
                sLoadConfig.sLoadVC[a].sLoadExp[b].sLoadCheck2.sCond.strFieldBlankCheck,
                sLoadConfig.sLoadVC[a].sLoadExp[b].sLoadCheck2.sCond.strName,
                sLoadConfig.sLoadVC[a].sLoadExp[b].sLoadCheck2.sCond.strField,
                sLoadConfig.sLoadVC[a].sLoadExp[b].sLoadCheck2.sCond.strOp,
                sLoadConfig.sLoadVC[a].sLoadExp[b].sLoadCheck2.sCond.strValue
                );
            dbg(TRACE, "               sLoadCheck2: strType[%s] strStyp[%s] strSstp[%s] strSsst[%s] strApc3[%s]",  
                sLoadConfig.sLoadVC[a].sLoadExp[b].sLoadCheck2.strType,
                sLoadConfig.sLoadVC[a].sLoadExp[b].sLoadCheck2.strStyp,
                sLoadConfig.sLoadVC[a].sLoadExp[b].sLoadCheck2.strSstp,
                sLoadConfig.sLoadVC[a].sLoadExp[b].sLoadCheck2.strSsst,
                sLoadConfig.sLoadVC[a].sLoadExp[b].sLoadCheck2.strSsst,
                sLoadConfig.sLoadVC[a].sLoadExp[b].sLoadCheck2.strApc3                                
               );
            dbg(TRACE, "               sLoadCheck2: strValue[%s] strOp[%s] strValue2[%s]",  
                sLoadConfig.sLoadVC[a].sLoadExp[b].sLoadCheck2.strValue,
                sLoadConfig.sLoadVC[a].sLoadExp[b].sLoadCheck2.strOp,
                sLoadConfig.sLoadVC[a].sLoadExp[b].sLoadCheck2.strValue2);                            	
        }
    }
        
    dbg(TRACE, "     strLoatabFieldNames[%s]", strLoatabFieldNames);
    dbg(TRACE, "     iArrLoatabDBFieldsConfigCount[%d]", iArrLoatabDBFieldsConfigCount);
    for (a=0; a<iArrLoatabDBFieldsConfigCount; a++)
    {
        dbg(TRACE, "     arrLoatabDBFieldsConfig[%d]: strFieldName[%s], strFieldDesc[%s]",
            a, arrLoatabDBFieldsConfig[a].strFieldName, arrLoatabDBFieldsConfig[a].strFieldDesc);
    }  
    

    dbg(TRACE, "");
    dbg(TRACE, "Scheduled Jobs: [%d]", iSched);
    for (a=0; a<iSched; a++)
    {
    	dbg(TRACE, "Schedule [%d/%d] Name[%s]", a+1, iSched, sSched[a].strName);
        dbg(TRACE, "     TimeRef[%s]", sSched[a].strTimeRef);
        dbg(TRACE, "     TimeZone[%s]", sSched[a].strTimeZone);
        dbg(TRACE, "     StartTime[%s]", sSched[a].strStartTime);
        dbg(TRACE, "     EndTime[%s]", sSched[a].strEndTime);
        dbg(TRACE, "     ArrivalField[%s]", sSched[a].strArrivalField);
        dbg(TRACE, "     DepartureField[%s]", sSched[a].strDepartureField);
        dbg(TRACE, "     DQCS[%s]", sSched[a].strDQCS);  
    }	    
    return;
}

int getFieldCheck(char *strFile, char *strHeader, char *strTable, char *strField, FIELDCHECK *sFieldCheck)
{
    int iRet=RC_SUCCESS;
    int iFirst=1;
    char strTmp[1025];
    char strValue[1025];
    char *strPtr, *strPtr2;
    int iDB;
    DBFIELDSCONFIG *sDB, *sDBFieldsConfigRealloc;
    

    dbg(DEBUG,"getFieldCheck:: Start: strFile[%s], strHeader[%s], strTable:[%s], strField[%s]",
        strFile, strHeader, strTable, strField);
        
  
    sFieldCheck->strFieldBlankCheck=(char *)malloc(sizeof(char)*(strlen(strField)+1));
    if ( sFieldCheck->strFieldBlankCheck ) 
        sprintf(sFieldCheck->strFieldBlankCheck, "%4.4s", strField);
    else
    {
        dbg(TRACE, "getFieldCheck:: ERROR!!! Cannot allocate FieldCheck->strFieldBlankCheck");
        iRet=RC_FAIL;
    }


    if ( iRet==RC_SUCCESS && strlen(sFieldCheck->strFieldBlankCheck)==4 )
    {
        if ( !strcmp(strTable, "AFT") )
        {
            if ( (getArrTabFieldIndex(sFieldCheck->strFieldBlankCheck, arrAfttabDBFieldsConfig, iArrAfttabDBFieldsConfigCount))==-1 )
            {
                sDB=(DBFIELDSCONFIG *)malloc(sizeof(DBFIELDSCONFIG));
            	if ( sDB )
            	{
                    memset(sDB, 0, sizeof(DBFIELDSCONFIG));
      
                    iDB=addArrDBFields(sFieldCheck->strFieldBlankCheck, strTable, sDB, &strAfttabFieldNames);
                    if ( iDB==RC_SUCCESS )
                    {
                	    sDBFieldsConfigRealloc=(DBFIELDSCONFIG *)realloc(arrAfttabDBFieldsConfig, sizeof(DBFIELDSCONFIG)*(iArrAfttabDBFieldsConfigCount+1)); 
                	    if ( sDBFieldsConfigRealloc )
                	    {
                	      	arrAfttabDBFieldsConfig=sDBFieldsConfigRealloc;
                            memset(&arrAfttabDBFieldsConfig[iArrAfttabDBFieldsConfigCount], 0, sizeof(DBFIELDSCONFIG));             
                            arrAfttabDBFieldsConfig[iArrAfttabDBFieldsConfigCount]=*sDB;
                            iArrAfttabDBFieldsConfigCount++;        
                        }
                        else
                        {
                            dbg(TRACE, "getFieldCheck:: ERROR!!! Cannot reallocate arrAfttabDBFieldsConfig");  
                            iRet=RC_FAIL;
                        }  		
                    }	
                    else
                    {
                        dbg(DEBUG, "getFieldCheck:: ERROR!!! addArrDBFields() failed");
                        //iRet=RC_FAIL;	
                    }
                   	free(sDB);
                   	sDB=NULL;
                }
                else
                {
                    dbg(TRACE, "getFieldCheck:: Cannot allocate sDB for (AFT) strField");	
                    iRet=RC_FAIL;
                }
            }
        }
    }

    memset(&strValue, 0, sizeof(char)*1025);    
    if ( iRet==RC_SUCCESS )
    {
        sFieldCheck->strName=(char *)malloc(sizeof(char)*(strlen(strField)+1));
        if ( sFieldCheck->strName ) 
            strcpy(sFieldCheck->strName, strField);
        else
        {
            dbg(TRACE, "getFieldCheck:: ERROR!!! Cannot allocate sFieldCheck->strName");
            iRet=RC_FAIL;
        }
    }
        
    if ( iRet==RC_SUCCESS )
    {       
        iDB=readConfigFile(strFile, strHeader, strField, strTmp);
        sFieldCheck->strString=(char *)malloc(sizeof(char)*(strlen(strTmp)+1));
        if ( sFieldCheck->strString ) 
            strcpy(sFieldCheck->strString, strTmp);
        else
        {
            dbg(TRACE, "getFieldCheck:: ERROR!!! Cannot allocate sFieldCheck->strString");
            iRet=RC_FAIL;
        }
    }
    
            
    if ( iRet==RC_SUCCESS )
    {
        strPtr=strtok_r(strTmp, ",", &strPtr2);        
        if ( strPtr )
        {
        	if ( strcmp(strPtr, " ") )
        	    trim(strPtr, ' ');
        	    
            sFieldCheck->strField=(char *)malloc(sizeof(char)*(strlen(strPtr)+1));
            if ( sFieldCheck->strField )
                strcpy(sFieldCheck->strField, strPtr);  
            else
            {
                dbg(TRACE, "getFieldCheck:: Cannot allocate sFieldCheck->strField");
                iRet=RC_FAIL;
            }            
        
            if ( iRet==RC_SUCCESS )
            {
                if ( !strcmp(strTable, "AFT") )
                {    
                    if ( (getArrTabFieldIndex(strPtr, arrAfttabDBFieldsConfig, iArrAfttabDBFieldsConfigCount))==-1 )
                    {
                	    sDB=(DBFIELDSCONFIG *)malloc(sizeof(DBFIELDSCONFIG));
                	    if ( sDB )
                	    {
                            memset(sDB, 0, sizeof(DBFIELDSCONFIG));
                            iDB=addArrDBFields(strPtr, strTable, sDB, &strAfttabFieldNames);
                            if ( iDB==RC_SUCCESS )
                            {
                    	        sDBFieldsConfigRealloc=(DBFIELDSCONFIG *)realloc(arrAfttabDBFieldsConfig, sizeof(DBFIELDSCONFIG)*(iArrAfttabDBFieldsConfigCount+1)); 
                    	        if ( sDBFieldsConfigRealloc )
                    	        {
                    	        	arrAfttabDBFieldsConfig=sDBFieldsConfigRealloc;
                                    memset(&arrAfttabDBFieldsConfig[iArrAfttabDBFieldsConfigCount], 0, sizeof(DBFIELDSCONFIG));             
                                    arrAfttabDBFieldsConfig[iArrAfttabDBFieldsConfigCount]=*sDB;
                                    iArrAfttabDBFieldsConfigCount++;        
                                }
                                else
                                {
                                    dbg(TRACE, "getFieldCheck:: ERROR!!! Cannot reallocate arrAfttabDBFieldsConfig");    
                                    iRet=RC_FAIL;
                                }		
                            }	
                            else
                            {
                                dbg(DEBUG, "getFieldCheck:: ERROR!!! addArrDBFields() failed");
                                iRet=RC_FAIL;	
                            }  
                       	    free(sDB);
                       	    sDB=NULL;
                       	}
                       	else
                       	{
                       	    dbg(TRACE, "getFieldCheck:: ERROR!!! Cannot allocate sDB for (AFT) strField");	  
                       	    iRet=RC_FAIL;
                       	}                       	    
                    }
                }    
                else if ( !strcmp(strTable, "LOA") )
                {        
                    if ( getArrTabFieldIndex(strPtr, arrLoatabDBFieldsConfig, iArrLoatabDBFieldsConfigCount)==-1 )
                    {
                	    sDB=(DBFIELDSCONFIG *)malloc(sizeof(DBFIELDSCONFIG));
                	    if ( sDB )
                	    {
                            memset(sDB, 0, sizeof(DBFIELDSCONFIG));
                            iDB=addArrDBFields(strPtr, strTable, sDB, &strAfttabFieldNames);
                            if ( iDB==RC_SUCCESS )
                            {
                    	        sDBFieldsConfigRealloc=(DBFIELDSCONFIG *)realloc(arrLoatabDBFieldsConfig, sizeof(DBFIELDSCONFIG)*(iArrLoatabDBFieldsConfigCount+1)); 
                    	        if ( sDBFieldsConfigRealloc )
                    	        {
                    	        	arrLoatabDBFieldsConfig=sDBFieldsConfigRealloc;
                                    memset(&arrLoatabDBFieldsConfig[iArrLoatabDBFieldsConfigCount], 0, sizeof(DBFIELDSCONFIG));             
                                    arrLoatabDBFieldsConfig[iArrLoatabDBFieldsConfigCount]=*sDB;
                                    iArrLoatabDBFieldsConfigCount++;    
                                }        	
                                else     
                                {
                                    dbg(TRACE, "getFieldCheck:: ERROR!!! Cannot reallocate arrLoatabDBFieldsConfig");	
                                    iRet=RC_FAIL;
                                }
                            }	
                            else
                            {
                                dbg(DEBUG, "getFieldCheck:: ERROR!!! addArrDBFields() failed");
                                iRet=RC_FAIL;	
                            }
                       	    free(sDB);
                       	    sDB=NULL;
                       	}
                       	else
                       	{
                       	    dbg(TRACE, "getFieldCheck:: ERROR!!! Cannot allocate sDB for (LOA) strField");	
                       	    iRet=RC_FAIL;
                       	}
                       		
                    }
                }    
            }
        }  
        
        if ( iRet==RC_SUCCESS )
        {
            strPtr=strtok_r(NULL, ",", &strPtr2);
            if ( strPtr )
            {
            	if ( strcmp(strPtr, " ") )
            	    trim(strPtr, ' ');
            	    
                sFieldCheck->strOp=(char *)malloc(sizeof(char)*(strlen(strPtr)+1));
                if ( sFieldCheck->strOp )
                    strcpy(sFieldCheck->strOp, strPtr);   
                else
                {
                	dbg(TRACE, "getFieldCheck:: ERROR!!! Cannot allocate sFieldCheck->strOp");	 
                	iRet=RC_FAIL;
                }     	
            }
            
            if ( iRet==RC_SUCCESS )
            {
                strPtr=strtok_r(NULL, ",", &strPtr2);
                while (strPtr)
                {
                	if ( strcmp(strPtr, " " ) )
                	    trim(strPtr, ' ');
                	    
            	    if ( iFirst )
            	    {
            	        iFirst=0;
            	        strcpy(strValue, strPtr);	
            	    }
            	    else
            	        sprintf(strValue, "%s,%s", strValue, strPtr);
                    strPtr=strtok_r(NULL, ",", &strPtr2); 	
                }
            
                if ( strlen(strValue)>0 )
                {
                    sFieldCheck->strValue=(char *)malloc(sizeof(char)*(strlen(strValue)+1));
                    if ( sFieldCheck->strValue )
                        strcpy(sFieldCheck->strValue, strValue);  
                    else
                    {
                        dbg(TRACE, "getFieldCheck:: ERROR!!! Cannot allocate sFieldCheck->strValue" );
                        iRet=RC_FAIL;
                    }
                }
                
                if ( iRet==RC_SUCCESS )
                    dbg(DEBUG, "FIELD[%s][%s][%s]", sFieldCheck->strName, sFieldCheck->strOp, sFieldCheck->strValue);
            }
        }    
    }
    
    dbg(DEBUG,"getFieldCheck:: strFile[%s], strHeader[%s], strTable:[%s], strField[%s], Result[%d]",
        strFile, strHeader, strTable, strField, iRet);
    return iRet;	
}

int readConfigFile(char *strFile, char *strHeader, char *strKey, char *strVal)
{
	int iRet=RC_FAIL;
	int a=0;
	int iHeaderFound=0;
	char strKeyTmp[1025], strValTmp[1025];
	char strHeaderTmp[1025];
	char strLine[1025];
	char *strPtr, *strPtr2;
    FILE *fptr;
    
    strcpy(strVal,"");
    
    fptr=fopen(strFile, "r");
    if ( fptr==NULL )	
    {
    	dbg(TRACE,"Cannot open file [%s]", strFile);	
    }
    else
    {
    	if ( !strcmp(strHeader, "") )
    	{
    	    while ( fgets(strLine,1024, fptr) )	
    	    {
    	        if ( strLine[0]=='#' )
        	 	    continue;        	 	       
        	 	
        	 	strLine[strlen(strLine)-1]=0;	 
        	 	strPtr=strtok_r(strLine, "=", &strPtr2);
                if ( strPtr )
                {
                	strcpy(strKeyTmp, strPtr);
                    trim(strKeyTmp, ' ');
                        
                    if ( !strcmp(strKey, strKeyTmp) )
                    {
                     
                        strPtr=strtok_r(NULL, "=", &strPtr2);	
                        if ( strPtr )
                        {
                            strcpy(strValTmp, strPtr);
                            if ( strcmp(strValTmp, " ") )
                                trim(strValTmp, ' ');
                                
                            strcpy(strVal, strValTmp);
                            
                            strPtr=strtok_r(NULL, "=", &strPtr2);	
                            while (strPtr)
                            {
                                strcpy(strValTmp, strPtr);
                                if ( strcmp(strValTmp, " ") )
                                    trim(strValTmp, ' ');
                                sprintf(strVal, "%s=%s", strVal, strValTmp);
                                strPtr=strtok_r(NULL, "=", &strPtr2);	
                            }
                        
                            iRet=RC_SUCCESS;
                            break;
                        }
                    }
                }	
    	    } 
    	}
    	else
    	{
    	    sprintf(strHeaderTmp, "[%s]", strHeader);
            while ( fgets(strLine,1024, fptr) )
            {
                strLine[strlen(strLine)-1]=0;	
                if ( strcmp(strLine, strHeaderTmp) )
                    continue;
                else
                {
                    iHeaderFound=1;	
                    break;
                }                
            }
        
            if ( iHeaderFound )
            {
        	    sprintf(strHeaderTmp, "[%s_END]", strHeader);
        	    while ( fgets(strLine,1024, fptr) )
        	    {
        	 	    if ( strLine[0]=='#' )
        	 	        continue;
        	 	    	
        	 	    strLine[strlen(strLine)-1]=0;	
         	 	    if ( !strcmp(strLine, strHeaderTmp) )
                        break;
                    else 
                    {
                    	strPtr=strtok_r(strLine, "=", &strPtr2);
                        if ( strPtr )
                        {
                            strcpy(strKeyTmp, strPtr);
                            trim(strKeyTmp, ' ');
                        
                            if ( !strcmp(strKey, strKeyTmp) )
                            {
                            	strPtr=strtok_r(NULL, "=", &strPtr2);	

                                if ( strPtr )
                                {
                                    strcpy(strValTmp, strPtr);
                                    if ( strcmp(strValTmp, " ") )
                                        trim(strValTmp, ' ');
                                    strcpy(strVal, strValTmp);
                            
                                    strPtr=strtok_r(NULL, "=", &strPtr2);	
                                    while (strPtr)
                                    {
                                        strcpy(strValTmp, strPtr);
                                        if ( strcmp(strValTmp, " ") )
                                            trim(strValTmp, ' ');
                                        sprintf(strVal, "%s=%s", strVal, strValTmp);
                                        strPtr=strtok_r(NULL, "=", &strPtr2);	
                                    }
                        
                                    iRet=RC_SUCCESS;
                                    break;
                                }
                                
                            }
                        }
                    }
                }
            }	
        }	
        fclose(fptr);
    }
    dbg(TRACE, "readConfigFile:: Key[%s] Value[%s]", strKey, strVal);
    return iRet;
}

int readHeaderConfigFile(char *strFile, char *strHeader)
{
	int iRet=RC_FAIL;
	char strHeaderTmp[1025];
	char strLine[1025];
	FILE *fptr;
    
    fptr=fopen(strFile, "r");
    if ( fptr==NULL )	
    {
    	dbg(TRACE,"Cannot open file [%s]", strFile);	
    }
    else
    {
  	    sprintf(strHeaderTmp, "[%s]", strHeader);
        while ( fgets(strLine,1024, fptr) )
        {
            strLine[strlen(strLine)-1]=0;	
            if ( strcmp(strLine, strHeaderTmp) )
                continue;
            else
            {
                iRet=RC_SUCCESS;	
                break;
            }                
        }
	
        fclose(fptr);
    }
    
    return iRet;
}

static int TriggerBchdlAction(char *strProcessName, char *pcpCmd, char *pcpTable, char *pcpSelection,
                              char *pcpFields, char *pcpData, char *pcpTwStart,
                              char *pcpTwEnd, int iClient, int iServer)
{
    int ilRC = RC_SUCCESS;
    int iProcessID;
    
    if ( iClient )
    {
    	if ( isalpha(strProcessName[0] ) )
    	    iProcessID = tool_get_q_id(strProcessName);
        else
            iProcessID =atoi(strProcessName);
            
        ilRC = SendCedaEvent(iProcessID,0,strProcessName,"CEDA",pcpTwStart,pcpTwEnd,pcpCmd,pcpTable,
                             pcpSelection,pcpFields,pcpData,"",3,NETOUT_NO_ACK);
        dbg(TRACE,"TriggerBchdlAction::Send Broadcast to client: Process<%s> ID<%d> Cmd<%s> Table<%s> Selection<%s> Field<%s> Value<%.100s>",
            strProcessName, iProcessID, pcpCmd,pcpTable,pcpSelection,pcpFields,pcpData);
    }

    if ( iServer==1 )
    {    
        iProcessID=tool_get_q_id(strProcessName);

        ilRC = SendCedaEvent(iProcessID, 0, strProcessName,"CEDA",pcpTwStart,pcpTwEnd,pcpCmd,pcpTable,
                             pcpSelection,pcpFields,pcpData,"",3,NETOUT_NO_ACK);
        dbg(TRACE,"TriggerBchdlAction::Send Broadcast to server: Process<%s>id<%d> Cmd<%s> Table<%s> Selection<%s> Field<%s> Data<%.100s>",
            strProcessName, iProcessID, pcpCmd,pcpTable,pcpSelection,pcpFields,pcpData);                             
    }

    return ilRC;
} /* End of TriggerBchdlAction */

static int TriggerBchdlActionV2(int iProcessID , char *pcpCmd, char *pcpTable, char *pcpSelection,
                              char *pcpFields, char *pcpData, char *pcpTwStart,
                              char *pcpTwEnd, int iClient, int iServer)
{
    int ilRC = RC_SUCCESS;
 
    ilRC = SendCedaEvent(iProcessID,0,"dqahdl","CEDA",pcpTwStart,pcpTwEnd,"WON",pcpTable,
                             pcpSelection,pcpFields,pcpData,"",3,NETOUT_NO_ACK);
    dbg(TRACE,"TriggerBchdlAction::Send Broadcast to client: Process<%s> ID<%d> Cmd<%s> Table<%s> Selection<%s> Field<%s> Value<%.100s>",
        "dqahdl", iProcessID, pcpCmd,pcpTable,pcpSelection,pcpFields,pcpData);
  
    return ilRC;
} /* End of TriggerBchdlAction */

int rtrim(char *str, char cTrim)
{
    int iRet=0;
    int iSize=strlen(str);
    int a=iSize-1;
    
    for (; a>0; a--)
    {
        if ( str[a]==cTrim )
            str[a]=0;
        else
            break;    	
    }
    
    return iRet;	
}

int ltrim(char *str, char cTrim)
{
	int iRet=0;
	int a=0;
    char *strPtr=malloc(sizeof(char)*(strlen(str)+1));
   
    if ( strPtr )
    {   
    	if ( strlen(str)==0 )
    	    ;
    	else
    	{
            for ( a=0; a<strlen(str); a++)
            {
                if (str[a]!=cTrim)
                    break;	
            }    
    
            if ( a<strlen(str) )
            {
               strcpy(strPtr, &str[a]);	
            }
    
            strcpy(str, strPtr);
        }
        free(strPtr);
        strPtr=NULL;
    }
    else
        dbg(TRACE, "ltrim:: Cannot allocate strPtr");
        
    return iRet;
}

int trim(char *str, char cTrim)
{
	int iRet=0;
	
	ltrim(str, cTrim);
	rtrim(str, cTrim);
	
    return iRet;
}

int fileExist(char *str)
{
    int iRet=0;
    FILE *fptr;
    
    fptr=fopen(str, "r");
    if ( fptr )
    {
    	iRet=1;
        fclose(fptr);
    }
    
    return iRet;	
}

static int ReadDQCTab(char *pcpFlnu, char *pcpType)
{
    int ilRc = RC_SUCCESS;
    int ilGetRc = DB_SUCCESS;
    int ilFldCnt = 0;
    int ilRecCnt = 0;
    char pclSqlBuf[1024];
    char pclSelBuf[512];
    char pclSqlDat[8192];

    short slCursor = 0;
    short slFkt = 0;
    char pclFunc[] = "ReadDQCTab:";
    char pclFldLst[] = "URNO,TYPE,REAS";
	  
    dbg(DEBUG,"%s Flnu[%s] Type[%s]", pclFunc, pcpFlnu, pcpType); 
    memset(sgOldDqcTab, 0, sizeof(sgOldDqcTab));
    	        
    if ( !strcmp(sMoveConfig.strAfttabDQCSUpdate, "YES") )
    {
        if ( strcmp(pcpType,"") == 0 )
            sprintf(pclSelBuf, "WHERE FLNU='%s' ", pcpFlnu, pcpType);
        else
            sprintf(pclSelBuf, "WHERE FLNU='%s' AND TYPE='%s' ", pcpFlnu, pcpType);

        sprintf(pclSqlBuf, "SELECT %s FROM DQCTAB %s ORDER BY URNO ", pclFldLst, pclSelBuf);
       
        /* dbg(DEBUG,"%s Action Sql[%s]", pclFunc, pclSqlBuf); */
        slFkt = START;
        ilRecCnt = 0;
        ilFldCnt = field_count(pclFldLst);
        while( (ilGetRc = sql_if(slFkt, &slCursor,pclSqlBuf, pclSqlDat)) == DB_SUCCESS)
        {
            BuildItemBuffer (pclSqlDat, "", ilFldCnt, ",");
            (void) get_real_item (sgOldDqcTab[ilRecCnt].strUrno, pclSqlDat, 1);
            (void) get_real_item (sgOldDqcTab[ilRecCnt].strType, pclSqlDat, 2);
            (void) get_real_item (sgOldDqcTab[ilRecCnt].strReas, pclSqlDat, 3);
            strcpy(sgOldDqcTab[ilRecCnt].strFlnu, pcpFlnu);
            ilRecCnt ++;
            slFkt = NEXT;
        }
        close_my_cursor(&slCursor);
        igOldDqcCnt = ilRecCnt;
    }
    return ilRc;	
} /* end of ReadDQCTab */

/*****************************************************/
/*** Status Check Result:                       ******/
/*** 0: No Need BroadCast                       ******/
/*** 1: Need BroadCast                          ******/
/*** 2: Check Old DQATAB for BroadCast          ******/
static int DQCStaChk(char *pcpOldDqcs, char *pcpNewDqcs)
{
    int ilRc = RC_SUCCESS;
    int ilRst = 0;
    char pclOldDqc[6] = "\0";
    char pclNewDqc[6] = "\0";
    char pclFunc[] = "DQCStaChk:";

    dbg(DEBUG,"%s: OldDqcs<%s>, NewDqcs<%s>", pclFunc, pcpOldDqcs, pcpNewDqcs);
    if (strcmp(pcpOldDqcs, pcpNewDqcs) == 0)
    {
        if (strstr(pcpNewDqcs, "F") != NULL)
            ilRst = 2;
        else
            ilRst = 0;
    }
    else
        ilRst = 1;
    return ilRst;
} /* end of DQCStaChk */

static int DQCTabChgChk(void)
{
    int ilRc = RC_SUCCESS;
    int ilCnt = 0;
    int ilOldCnt = 0;
    int ilFound = FALSE;
    int ilDif = TRUE;
 
    if (iDqctabCount != igOldDqcCnt)
        ilDif = TRUE;
    else
    {
        for (ilCnt = 0; ilCnt < iDqctabCount; ilCnt++)
        {
  /*        dbg(DEBUG,"New DQCTAB NO<%d> Reason<%s>", ilCnt, sDqctab[ilCnt].strReas); */
            ilFound = FALSE;
            for (ilOldCnt = 0; ilCnt < igOldDqcCnt; ilOldCnt++)
            {
                if (strcmp(sDqctab[ilCnt].strReas, sgOldDqcTab[ilOldCnt].strReas) == 0) 
                {
                    ilFound = TRUE;
                    break;
                }
            }
            if (ilFound == FALSE)
            {
                ilDif = TRUE;
                break;
            }
        }
        if (ilFound == TRUE)
            ilDif = FALSE;
    }
    return ilDif;
} /* end of DQCTabChgChk */
 
int timeDiff(char *pcpDate1, char *pcpDate2, double *pdpOut)
{
	int ilRC=RC_SUCCESS;
    char pclSysDate[15];
    struct tm tm1, tm2;
    time_t t1, t2;
    
    getSysDate(pclSysDate);
    
    if ( !strcmp(pcpDate1,"") )
        convertDateToStructTm(pclSysDate, &tm1);
    else
         convertDateToStructTm(pcpDate1, &tm1);
 
    if ( !strcmp(pcpDate2,"") )
        convertDateToStructTm(pclSysDate, &tm2);
    else
        convertDateToStructTm(pcpDate2, &tm2) ;   
    
    t1= mktime(&tm1);
    t2= mktime(&tm2);
    *pdpOut=difftime(t1,t2);
    
    dbg(DEBUG,"timeDiff:: pclSysDate[%s], time1[%s][%d], time2[%s][%d], Result[%f]",
        pclSysDate, pcpDate1, t1, pcpDate2, t2, *pdpOut);
    return ilRC; 
        
}

int getSysDate(char *pcpDate)
{
    int ilRC=RC_SUCCESS;
    
    time_t tlNow=time(NULL);
    struct tm rlRes;
    
    gmtime_r(&tlNow, &rlRes);
    sprintf(pcpDate,"%04d%02d%02d%02d%02d%02d",
            rlRes.tm_year+1900, rlRes.tm_mon+1, rlRes.tm_mday,
            rlRes.tm_hour, rlRes.tm_min, rlRes.tm_sec);
    return ilRC;    
}

int convertDateToStructTm(char *pcpDate, struct tm *stTm)
{
    int iRet=RC_SUCCESS;
    char YYYY[5];
    char MM[3];
    char DD[3];
    char hh[3];
    char mi[3];
    char ss[3];
    struct tm *stTmp;
    time_t tNow=time(NULL);
    int iSize=strlen(pcpDate);
    
    stTmp=gmtime_r(&tNow, stTm);
    
    if ( iSize>=4 )    
    {
        sprintf(YYYY, "%4.4s", pcpDate);
        stTm->tm_year=atoi(YYYY)-1900;
    }
    else
         stTm->tm_year=0;
    
    if ( iSize>=6 )
    {
         sprintf(MM, "%2.2s", &pcpDate[4]);
         stTm->tm_mon = atoi(MM)-1;
    }
    else
        stTm->tm_mon = 0;
    
    if ( iSize>=8 )
    {
        sprintf(DD, "%2.2s", &pcpDate[6]);
        stTm->tm_mday=atoi(DD);
    }
    else
        stTm->tm_mday=0;
        
    if ( iSize>=10 )
    {
        sprintf(hh, "%2.2s", &pcpDate[8]);
        stTm->tm_hour=atoi(hh);
    }
    else
        stTm->tm_hour=0;
    
    if ( iSize>=12 )
    {
        sprintf(mi, "%2.2s", &pcpDate[10]);
        stTm->tm_min = atoi(mi);
    }
    else
        stTm->tm_min = 0;
        
    if ( iSize>=14 )
    {
        sprintf(ss, "%2.2s", &pcpDate[12]);
        stTm->tm_sec=atoi(ss);
    }
    else
        stTm->tm_sec=0;

    tNow=mktime(stTm);
    stTmp=localtime_r(&tNow, stTm);

    return iRet;    
}

int doCleanUp(char *pcpFlnu, char *pcpCmd)
{
	int ilRC=RC_SUCCESS;
	
	dbg(DEBUG, "doCleanUp:: Start");
	
    ilRC=deleteDQCTab(pcpFlnu, "");	
    if ( ilRC==RC_SUCCESS )
        ilRC=updateFlightDQCS(" ", pcpFlnu, pcpCmd);
    
    dbg(DEBUG, "doCleanUp:: End Result[%d]", ilRC);
    return ilRC;
}

int doCompletenessCheckGroup(char *pcpFields, FIELDCHECK *rpFields, int ipCount)
{
    int ilRC=RC_SUCCESS;
    int ilTmp;
    int a=0;
    
    dbg(DEBUG, "doCompletenessCheckGroup:: START");
    
    if ( pcpFields==NULL || !strcmp(pcpFields, "") )
    {
        dbg(DEBUG, "doCompletenessCheckGroup:: Nothing to check");	
    }
    else
    {
        ilRC=doBlankCheck(pcpFields); 	
        for (a=0; a<ipCount; a++)
        {
            ilTmp=doFieldCheck(&rpFields[a]);
            if ( ilTmp==RC_SUCCESS )
            {
        	    dbg(TRACE,"doCompletenessCheckGroup:: Condition satisfied.  Check Value");
                ilTmp=getArrTabFieldIndex(rpFields[a].strFieldBlankCheck, arrAfttabDBFieldsConfig, iArrAfttabDBFieldsConfigCount);
                if ( ilTmp==-1 )
                {
                    dbg(TRACE, "doCompletenessCheckGroup:: Field not found[%s]", rpFields[a].strFieldBlankCheck);
                    ilRC=RC_FAIL; 	
                }
                else
                {
                    dbg(TRACE,"doCompletenessCheckGroup:; Value of [%s][%d][%s]", 
                        rpFields[a].strFieldBlankCheck, ilTmp, strArrAfttabFieldValues[ilTmp]);
                        
                    if ( !strcmp(strArrAfttabFieldValues[ilTmp], "") )
                    {
                        sprintf(strArrAfttabError[iArrAfttabErrorCount], sMoveConfig.strBlankReason, arrAfttabDBFieldsConfig[ilTmp].strFieldDesc);
                	    dbg(TRACE,"doMoveCheck:; Value of [%s][%d][%s] is empty [%s]", 
                	        sMoveConfig.arrAllBlankCheck[a].strFieldBlankCheck, ilTmp, strArrAfttabFieldValues[ilTmp], strArrAfttabError[iArrAfttabErrorCount]);
        
                        populateDQCTab(&sDqctab[iDqctabCount], "", gsDqctab.strFlnu, gsDqctab.strType, strArrAfttabError[iArrAfttabErrorCount], 
                                       'F', gsDqctab.strHopo);
                        iArrAfttabErrorCount++;
                        iDqctabCount++;
          	  		    ilRC=RC_FAIL;	
                    }	
                }
            }
        }        
    }
    dbg(TRACE, "doCompletenessCheckGroup:: END Result[%d]", ilRC);
    return ilRC;	
}

