rem Copies all generated files from the SCM-Structue into the Install-Shield directory.
rem 

rem set drive to J: (IS 6.2) or K: (IS 8.0) 
set drive=k:

rem Copy ini- and config-files
copy c:\ufis_bin\*.ini %drive%\ATH_PRM_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\*.cfg %drive%\ATH_PRM_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\*.bmp %drive%\ATH_PRM_Build\ufis_client_appl\system\*.*


rem copy applications
copy c:\ufis_bin\debug\bdps_sec.exe %drive%\ATH_PRM_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpspass.exe %drive%\ATH_PRM_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpsuif.exe %drive%\ATH_PRM_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\autocoverage.exe %drive%\ATH_PRM_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\fips.exe %drive%\ATH_PRM_Build\ufis_client_appl\applications\*.*
rem copy c:\ufis_bin\debug\rules.exe %drive%\ATH_PRM_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\grp.exe %drive%\ATH_PRM_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\opsspm.exe %drive%\ATH_PRM_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\regelwerk.exe %drive%\ATH_PRM_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\servicecatalog.exe %drive%\ATH_PRM_Build\ufis_client_appl\applications\*.*

REM Copy system-files
copy c:\ufis_bin\release\bcproxy.exe %drive%\ATH_PRM_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcproxy.tlb %drive%\ATH_PRM_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcserv32.exe %drive%\ATH_PRM_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufis32.dll %drive%\ATH_PRM_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.exe %drive%\ATH_PRM_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.tlb %drive%\ATH_PRM_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisapplmgr.exe %drive%\ATH_PRM_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisapplmgr.tlb %drive%\ATH_PRM_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.ocx %drive%\ATH_PRM_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.tlb %drive%\ATH_PRM_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.ocx %drive%\ATH_PRM_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.tlb %drive%\ATH_PRM_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucom.ocx %drive%\ATH_PRM_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucom.tlb %drive%\ATH_PRM_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.ocx %drive%\ATH_PRM_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.tlb %drive%\ATH_PRM_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\aatlogin.ocx %drive%\ATH_PRM_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\aatlogin.tlb %drive%\ATH_PRM_Build\ufis_client_appl\system\*.*


rem Copy Help-Files
copy c:\ufis_bin\Help\*.chm %drive%\ATH_PRM_Build\ufis_client_appl\help\*.*
