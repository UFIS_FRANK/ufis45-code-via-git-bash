using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Microsoft.Win32;
using System.Text;
using Ufis.Utils;
using Ufis.Data;

namespace CheckListProcessing
{
	//TEST URNOs f�r Fl�ge (AFT)
	// 230684429,170568412,186463640,187589482

	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class frmMain : System.Windows.Forms.Form
	{
		#region Own Section

		private IDatabase myDB = null;
		private ITable tabCLB = null;
		private ITable tabCLP = null;
		private ITable tabAFT = null;
		private ITable tabREM = null;
		private string [] myPermits = {"1", "1", "1", "1"};
		private string myTimeIn = "L";

		public string [] args;
		private string myAftUrno = "";
		private string currRemark = "";
		private int myTabScrollPos = 0;

		#endregion my Section

		private System.Windows.Forms.Panel panelToolbar;
		private System.Windows.Forms.PictureBox picToolbar;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.ImageList imgButtons;
		private System.Windows.Forms.Button btnPrint;
		private System.Windows.Forms.Button btnExit;
		private System.Windows.Forms.Button btnEditTexts;
		private System.Windows.Forms.Label lblSepa;
		private System.Windows.Forms.Panel panelWorkArea;
		private System.Windows.Forms.Panel panelBottom;
		private System.Windows.Forms.Label lblRemark;
		private System.Windows.Forms.Panel panelAreaSection;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox cbArea;
		private System.Windows.Forms.Label lblFlight;
		private AxTABLib.AxTAB tabList;
		private System.Windows.Forms.Panel panelTab;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox txtURNOS;
		private System.Windows.Forms.TextBox txtURNO;
		private System.Windows.Forms.TextBox txtRemark;
        private System.Windows.Forms.DateTimePicker dtCHTM;
		private System.ComponentModel.IContainer components;


		public frmMain()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//Read ceda.ini and set it global to UT (UfisTools)
			string strServer="";
			string strHopo = "";
			Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\ceda.ini");
			strServer = myIni.IniReadValue("GLOBAL", "HOSTNAME");
			strHopo = myIni.IniReadValue("GLOBAL", "HOMEAIRPORT");
			UT.ServerName = strServer;
			UT.Hopo = strHopo;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.panelToolbar = new System.Windows.Forms.Panel();
            this.txtURNO = new System.Windows.Forms.TextBox();
            this.txtURNOS = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btnEditTexts = new System.Windows.Forms.Button();
            this.imgButtons = new System.Windows.Forms.ImageList(this.components);
            this.lblSepa = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.picToolbar = new System.Windows.Forms.PictureBox();
            this.panelWorkArea = new System.Windows.Forms.Panel();
            this.panelTab = new System.Windows.Forms.Panel();
            this.dtCHTM = new System.Windows.Forms.DateTimePicker();
            this.tabList = new AxTABLib.AxTAB();
            this.panelAreaSection = new System.Windows.Forms.Panel();
            this.lblFlight = new System.Windows.Forms.Label();
            this.cbArea = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panelBottom = new System.Windows.Forms.Panel();
            this.txtRemark = new System.Windows.Forms.TextBox();
            this.lblRemark = new System.Windows.Forms.Label();
            this.panelToolbar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picToolbar)).BeginInit();
            this.panelWorkArea.SuspendLayout();
            this.panelTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabList)).BeginInit();
            this.panelAreaSection.SuspendLayout();
            this.panelBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelToolbar
            // 
            this.panelToolbar.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelToolbar.Controls.Add(this.txtURNO);
            this.panelToolbar.Controls.Add(this.txtURNOS);
            this.panelToolbar.Controls.Add(this.button1);
            this.panelToolbar.Controls.Add(this.btnEditTexts);
            this.panelToolbar.Controls.Add(this.lblSepa);
            this.panelToolbar.Controls.Add(this.btnExit);
            this.panelToolbar.Controls.Add(this.btnPrint);
            this.panelToolbar.Controls.Add(this.btnSave);
            this.panelToolbar.Controls.Add(this.picToolbar);
            this.panelToolbar.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelToolbar.Location = new System.Drawing.Point(0, 0);
            this.panelToolbar.Name = "panelToolbar";
            this.panelToolbar.Size = new System.Drawing.Size(840, 40);
            this.panelToolbar.TabIndex = 0;
            // 
            // txtURNO
            // 
            this.txtURNO.Location = new System.Drawing.Point(432, 8);
            this.txtURNO.Name = "txtURNO";
            this.txtURNO.Size = new System.Drawing.Size(112, 20);
            this.txtURNO.TabIndex = 8;
            this.txtURNO.Visible = false;
            // 
            // txtURNOS
            // 
            this.txtURNOS.Location = new System.Drawing.Point(576, 0);
            this.txtURNOS.Multiline = true;
            this.txtURNOS.Name = "txtURNOS";
            this.txtURNOS.Size = new System.Drawing.Size(176, 32);
            this.txtURNOS.TabIndex = 7;
            this.txtURNOS.Text = "230684429,170568412,186463640,187589482";
            this.txtURNOS.Visible = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(328, 8);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "button1";
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnEditTexts
            // 
            this.btnEditTexts.BackColor = System.Drawing.Color.Transparent;
            this.btnEditTexts.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnEditTexts.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditTexts.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEditTexts.ImageIndex = 1;
            this.btnEditTexts.ImageList = this.imgButtons;
            this.btnEditTexts.Location = new System.Drawing.Point(184, 0);
            this.btnEditTexts.Name = "btnEditTexts";
            this.btnEditTexts.Size = new System.Drawing.Size(136, 36);
            this.btnEditTexts.TabIndex = 5;
            this.btnEditTexts.Text = "  &Edit Basic Data";
            this.btnEditTexts.UseVisualStyleBackColor = false;
            this.btnEditTexts.Click += new System.EventHandler(this.btnEditTexts_Click);
            // 
            // imgButtons
            // 
            this.imgButtons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgButtons.ImageStream")));
            this.imgButtons.TransparentColor = System.Drawing.Color.White;
            this.imgButtons.Images.SetKeyName(0, "");
            this.imgButtons.Images.SetKeyName(1, "");
            this.imgButtons.Images.SetKeyName(2, "");
            this.imgButtons.Images.SetKeyName(3, "");
            // 
            // lblSepa
            // 
            this.lblSepa.BackColor = System.Drawing.Color.Transparent;
            this.lblSepa.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblSepa.Location = new System.Drawing.Point(150, 0);
            this.lblSepa.Name = "lblSepa";
            this.lblSepa.Size = new System.Drawing.Size(34, 36);
            this.lblSepa.TabIndex = 4;
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.Transparent;
            this.btnExit.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.ImageIndex = 2;
            this.btnExit.ImageList = this.imgButtons;
            this.btnExit.Location = new System.Drawing.Point(761, 0);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 36);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "  &Exit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.Color.Transparent;
            this.btnPrint.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrint.ImageIndex = 3;
            this.btnPrint.ImageList = this.imgButtons;
            this.btnPrint.Location = new System.Drawing.Point(75, 0);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 36);
            this.btnPrint.TabIndex = 2;
            this.btnPrint.Text = "  &Print";
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Transparent;
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.ImageIndex = 0;
            this.btnSave.ImageList = this.imgButtons;
            this.btnSave.Location = new System.Drawing.Point(0, 0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 36);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "  &Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // picToolbar
            // 
            this.picToolbar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picToolbar.Location = new System.Drawing.Point(0, 0);
            this.picToolbar.Name = "picToolbar";
            this.picToolbar.Size = new System.Drawing.Size(836, 36);
            this.picToolbar.TabIndex = 1;
            this.picToolbar.TabStop = false;
            this.picToolbar.Paint += new System.Windows.Forms.PaintEventHandler(this.picToolbar_Paint);
            // 
            // panelWorkArea
            // 
            this.panelWorkArea.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelWorkArea.Controls.Add(this.panelTab);
            this.panelWorkArea.Controls.Add(this.panelAreaSection);
            this.panelWorkArea.Controls.Add(this.panelBottom);
            this.panelWorkArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelWorkArea.Location = new System.Drawing.Point(0, 40);
            this.panelWorkArea.Name = "panelWorkArea";
            this.panelWorkArea.Size = new System.Drawing.Size(840, 470);
            this.panelWorkArea.TabIndex = 1;
            // 
            // panelTab
            // 
            this.panelTab.Controls.Add(this.dtCHTM);
            this.panelTab.Controls.Add(this.tabList);
            this.panelTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelTab.Location = new System.Drawing.Point(0, 24);
            this.panelTab.Name = "panelTab";
            this.panelTab.Size = new System.Drawing.Size(836, 330);
            this.panelTab.TabIndex = 6;
            // 
            // dtCHTM
            // 
            this.dtCHTM.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtCHTM.Location = new System.Drawing.Point(318, 155);
            this.dtCHTM.Name = "dtCHTM";
            this.dtCHTM.Size = new System.Drawing.Size(200, 20);
            this.dtCHTM.TabIndex = 7;
            this.dtCHTM.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtCHTM_KeyPress);
            // 
            // tabList
            // 
            this.tabList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabList.Location = new System.Drawing.Point(0, 0);
            this.tabList.Name = "tabList";
            this.tabList.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabList.OcxState")));
            this.tabList.Size = new System.Drawing.Size(836, 330);
            this.tabList.TabIndex = 1;
            this.tabList.SendLButtonClick += new AxTABLib._DTABEvents_SendLButtonClickEventHandler(this.tabList_SendLButtonClick);
            this.tabList.RowSelectionChanged += new AxTABLib._DTABEvents_RowSelectionChangedEventHandler(this.tabList_RowSelectionChanged);
            this.tabList.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabList_SendLButtonDblClick);
            this.tabList.OnVScroll += new AxTABLib._DTABEvents_OnVScrollEventHandler(this.tabList_OnVScroll);
            // 
            // panelAreaSection
            // 
            this.panelAreaSection.Controls.Add(this.lblFlight);
            this.panelAreaSection.Controls.Add(this.cbArea);
            this.panelAreaSection.Controls.Add(this.label1);
            this.panelAreaSection.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelAreaSection.Location = new System.Drawing.Point(0, 0);
            this.panelAreaSection.Name = "panelAreaSection";
            this.panelAreaSection.Size = new System.Drawing.Size(836, 24);
            this.panelAreaSection.TabIndex = 5;
            // 
            // lblFlight
            // 
            this.lblFlight.BackColor = System.Drawing.Color.Black;
            this.lblFlight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblFlight.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFlight.ForeColor = System.Drawing.Color.Lime;
            this.lblFlight.Location = new System.Drawing.Point(232, 0);
            this.lblFlight.Name = "lblFlight";
            this.lblFlight.Size = new System.Drawing.Size(604, 24);
            this.lblFlight.TabIndex = 8;
            this.lblFlight.Text = "Flight:             (double click on time column to edit => press Enter to save)";
            this.lblFlight.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbArea
            // 
            this.cbArea.Dock = System.Windows.Forms.DockStyle.Left;
            this.cbArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbArea.Location = new System.Drawing.Point(48, 0);
            this.cbArea.Name = "cbArea";
            this.cbArea.Size = new System.Drawing.Size(184, 21);
            this.cbArea.TabIndex = 7;
            this.cbArea.SelectedIndexChanged += new System.EventHandler(this.cbArea_SelectedIndexChanged);
            this.cbArea.SelectedValueChanged += new System.EventHandler(this.cbArea_SelectedValueChanged);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Black;
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Lime;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 24);
            this.label1.TabIndex = 6;
            this.label1.Text = "Area:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panelBottom
            // 
            this.panelBottom.Controls.Add(this.txtRemark);
            this.panelBottom.Controls.Add(this.lblRemark);
            this.panelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelBottom.Location = new System.Drawing.Point(0, 354);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(836, 112);
            this.panelBottom.TabIndex = 4;
            // 
            // txtRemark
            // 
            this.txtRemark.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRemark.Location = new System.Drawing.Point(0, 24);
            this.txtRemark.MaxLength = 256;
            this.txtRemark.Multiline = true;
            this.txtRemark.Name = "txtRemark";
            this.txtRemark.Size = new System.Drawing.Size(836, 88);
            this.txtRemark.TabIndex = 1;
            this.txtRemark.TextChanged += new System.EventHandler(this.txtRemark_TextChanged);
            // 
            // lblRemark
            // 
            this.lblRemark.BackColor = System.Drawing.Color.Black;
            this.lblRemark.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblRemark.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRemark.ForeColor = System.Drawing.Color.Lime;
            this.lblRemark.Location = new System.Drawing.Point(0, 0);
            this.lblRemark.Name = "lblRemark";
            this.lblRemark.Size = new System.Drawing.Size(836, 24);
            this.lblRemark.TabIndex = 0;
            this.lblRemark.Text = "Remarks";
            // 
            // frmMain
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(840, 510);
            this.Controls.Add(this.panelWorkArea);
            this.Controls.Add(this.panelToolbar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.Text = "frmMain";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.frmMain_Closing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.panelToolbar.ResumeLayout(false);
            this.panelToolbar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picToolbar)).EndInit();
            this.panelWorkArea.ResumeLayout(false);
            this.panelTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabList)).EndInit();
            this.panelAreaSection.ResumeLayout(false);
            this.panelBottom.ResumeLayout(false);
            this.panelBottom.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string [] args) 
		{
			frmMain olDlg = new frmMain();
			olDlg.args = args;
			Application.Run(olDlg);
		}

		private void HandleCommandLine(string [] args)
		{
			string [] argList = args[0].Split(',');

			if(argList.Length == 4)
			{
				string [] olPerm = argList[2].Split(';');
				myAftUrno	= argList[0];
				UT.UserName	= argList[1];
				if(olPerm.Length == 4)
				{
					myPermits	= argList[2].Split(';');
				}
				else
				{

					argList[2] += ";1";
					myPermits	= argList[2].Split(';');
				}
				myTimeIn	= argList[3];
				if(myPermits[0] != "1")
				{
					tabList.Enabled = false;
				}
			}
			else
			{
				Application.Exit();
			}
		}

		private void frmMain_Load(object sender, System.EventArgs e)
		{

			if(args.Length != 1) 
			{

				//No login procedure necessary because this is auxilliary application
				//which shall not be started stand alone
				//if(LoginProcedure() == false)
				{
					Application.Exit();
					return;
				}
			}
			HandleCommandLine(args);
			if(UT.UserName == "")
			{
				UT.UserName = "Default";
			}
			myDB = UT.GetMemDB();
			bool result = myDB.Connect(UT.ServerName,UT.UserName,UT.Hopo,"TAB","CHECKLIST");
           
			myDB.IndexUpdateImmediately = true;
			this.Text = "Check-List-Processing  ----  on server " + UT.ServerName;
			if(myPermits[0] != "1")
			{
				this.Text +=  "  ---- NO PERMITS TO CHANGE ANYTHING!!!" ;
				btnSave.Enabled = false;
			}
			if(myPermits[1] != "1")
			{
				btnEditTexts.Enabled = false;
			}
			if(myPermits[2] != "1")
			{
				btnPrint.Enabled = false;
			}
			//Data
			Data.LoadData(myAftUrno);
			tabCLB = myDB["CLB"];
			tabCLP = myDB["CLP"];
			tabAFT = myDB["AFT"];
			tabREM = myDB["REM"];

//			string theKey = "Software\\CheckListProcessing";
//			RegistryKey rk = Registry.LocalMachine.OpenSubKey(theKey, true);
//			if(rk == null)
//			{
//				rk = Registry.LocalMachine.OpenSubKey("Software", true);
//				theKey = "CheckListProcessing";
//				rk.CreateSubKey(theKey);
//				rk.Close();
//			}

			btnExit.Parent = picToolbar;
			btnEditTexts.Parent = picToolbar;
			lblSepa.Parent = picToolbar;
			btnPrint.Parent = picToolbar;
			btnSave.Parent = picToolbar;
			dtCHTM.Visible = false;
			dtCHTM.CustomFormat = "dd.MM.yyyy / HH:mm";

			InitTabs();
			ReorgCombobox();
			
			IRow [] rows =  tabAFT.RowsByIndexValue("URNO", myAftUrno);
			if(rows.Length > 0)
			{
				string strValues = "Flight: " + rows[0]["FLNO"];
				lblFlight.Text = strValues;
			}
			SetRemark();
			this.BringToFront();
		}
		private void SetRemark()
		{
			if(tabREM.Count > 0) 
			{
				currRemark = tabREM[0]["TEXT"];
				txtRemark.Text = tabREM[0]["TEXT"];
			}
			else
			{
				currRemark = "";
				txtRemark.Text = "";
			}
			btnSave.BackColor = Color.Transparent;
		}
		private void InitTabs()
		{
			tabList.ResetContent();
			tabList.HeaderString = "URNO,UAFT,Done,Order,Section,Item,Date/Time (local)";
			tabList.LogicalFieldList = "URNO,UAFT,CHEK,ORDE,SECT,TEXT,CHTM";
			tabList.ColumnAlignmentString = "L,L,L,R,L,L,L";
			tabList.ColumnWidthString = "10,10,1,3,32,64,14";
			tabList.HeaderLengthString = "-1,-1,40,-1,-1,400,150";
			tabList.SetColumnBoolProperty(2, "Y", "N");
            

			int idx = 0;
			idx = UT.GetItemNo(tabList.LogicalFieldList, "CHTM");
			tabList.DateTimeSetColumnFormat( idx , "YYYYMMDDhhmmss", "DD'.'MM'.'YYYY' ''/'' 'hh':'mm");
			tabList.LifeStyle = true;
			tabList.CursorLifeStyle = true;
			tabList.LineHeight = 18;
			tabList.FontName = "Arial";
			tabList.FontSize = 15;
			tabList.HeaderFontSize = 15;
			tabList.LineHeight = 16;
			tabList.SetTabFontBold(true);


		}
		private void btnExit_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void frmMain_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
//			DialogResult olResult;
//			olResult = MessageBox.Show(this, "Do you want to exit the application?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question); 
//			if ( olResult == DialogResult.Yes)
//			{
//				e.Cancel = false;
//			}
//			else
//			{
//				e.Cancel = true;
//			}
		}

		private void picToolbar_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, picToolbar, Color.WhiteSmoke, Color.Gray);
		}

		private void btnEditTexts_Click(object sender, System.EventArgs e)
		{
			frmDataEntryGrid olDlg = new frmDataEntryGrid();
			olDlg.currTabelName = "CLB"; 
			olDlg.myCaption = "Check List Section and text processing ...                        ";
			olDlg.myHeaderLen = "0,40,200,400";
			int ilWidth = 0;
			string [] arrW = olDlg.myHeaderLen.Split(',');
			for(int i = 0; i < arrW.Length; i++)
			{
				ilWidth += Convert.ToInt32( arrW[i]);
			}
			olDlg.myWidth = ilWidth+30;
			olDlg.myHeaderString = "URNO,Order,Section,Text";
			olDlg.myFields = "URNO,ORDE,SECT,TEXT";
			olDlg.ShowDialog();
			string strArea = cbArea.Text;
			ReorgCombobox();
			HandelAreaSelection(strArea);

		}
		private void ReorgCombobox()
		{
			int i = 0;
			cbArea.Items.Clear(); 

			for( i = 0; i < tabCLB.Count; i++)	
			{
				if(cbArea.FindStringExact(tabCLB[i]["SECT"], -1) == -1)
				{
					cbArea.Items.Add(tabCLB[i]["SECT"]);
				}
			}
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			string strValues = "Flight: ";
			string strURNO = txtURNO.Text;
			Data.LoadData(strURNO);
			IRow [] rows =  tabAFT.RowsByIndexValue("URNO", strURNO);
			if(rows.Length > 0)
			{
				strValues = "Flight: " + rows[0]["FLNO"];
				lblFlight.Text = strValues;
			}
			myAftUrno = strURNO;
			string strArea = cbArea.Text;
			HandelAreaSelection(strArea);
			SetRemark();

		}

		private void cbArea_SelectedValueChanged(object sender, System.EventArgs e)
		{
			string strArea = cbArea.Text;
            //TraceMsg("Selected " + strArea);
			HandelAreaSelection(strArea);
		}

        private void TraceMsg(string msg)
        {
            MessageBox.Show(msg);
        }
		private void HandelAreaSelection(string strArea)
		{
			int i = 0;
			bool blSectionFound = false;

			tabList.ResetContent();
				
			tabCLP.Sort("ORDE", true);
			IRow [] rows = tabCLP.RowsByIndexValue("UAFT", myAftUrno);
			for( i = 0; i < rows.Length; i++)
			{
				if(rows[i]["SECT"] == strArea)
				{
					string strValues = "";
					strValues += rows[i]["URNO"] + ",";
					strValues += rows[i]["UAFT"] + ",";
					strValues += rows[i]["CHEK"] + ",";
					strValues += rows[i]["ORDE"] + ",";
					strValues += rows[i]["SECT"] + ",";
					strValues += rows[i]["TEXT"] + ",";
					strValues += rows[i]["CHTM"];
					tabList.InsertTextLine(strValues, false);
					blSectionFound = true;
				}
			}
			if( blSectionFound == true)
			{
				for( i = 0; i < tabCLB.Count; i++)
				{
					if(tabCLB[i]["SECT"] == strArea)
					{
						bool blEntryFound = false;
						for( int j = 0; j < rows.Length && blEntryFound == false; j++)
						{
							if(tabCLB[i]["TEXT"] == rows[j]["TEXT"])
							{
								blEntryFound = true;
							}
						}
						if(blEntryFound == false)
						{
							//Insert it
							IRow newRow = tabCLP.CreateEmptyRow();
							//newRow["URNO"];
							newRow["UAFT"] = myAftUrno;
							newRow["CHEK"] = "N";
							newRow["ORDE"] = tabCLB[i]["ORDE"];
							newRow["SECT"] = tabCLB[i]["SECT"];
							newRow["TEXT"] = tabCLB[i]["TEXT"];
							newRow["CHTM"] = tabCLB[i]["CHTM"];
							newRow.Status = State.Created;
							tabCLP.Add(newRow);
							string strValues = "";
							strValues += newRow["URNO"] + ",";
							strValues += newRow["UAFT"] + ",";
							strValues += newRow["CHEK"] + ",";
							strValues += newRow["ORDE"] + ",";
							strValues += newRow["SECT"] + ",";
							strValues += newRow["TEXT"] + ",";
							strValues += newRow["CHTM"];
							tabList.InsertTextLine(strValues, false);
						}
					}
				}				
				tabCLP.Release(300, "LATE,NOBC,NOACTION,NOLOG", null);
			}
			if( blSectionFound == false)
			{
				int cnt=0;
				tabCLB.Sort("ORDE", true);
				for( i = 0; i < tabCLB.Count; i++)
				{
					if(tabCLB[i]["SECT"] == strArea)
					{
						IRow newRow = tabCLP.CreateEmptyRow();
						//newRow["URNO"];
						newRow["UAFT"] = myAftUrno;
						newRow["CHEK"] = "N";
						newRow["ORDE"] = tabCLB[i]["ORDE"];
						newRow["SECT"] = tabCLB[i]["SECT"];
						newRow["TEXT"] = tabCLB[i]["TEXT"];
						newRow["CHTM"] = tabCLB[i]["CHTM"];
						newRow.Status = State.Created;
						tabCLP.Add(newRow);
                        tabCLP.Save(newRow);//Added by AM @20070723
						cnt++;
					}
				}
				if(cnt > 0)
				{
                    //tabCLP.Release(300, "LATE,NOBC,NOACTION,NOLOG", null);//Remarked by AM @20070723
                    HandelAreaSelection(strArea);
				}
			}
			tabList.Refresh();
		}

		private void tabList_SendLButtonClick(object sender, AxTABLib._DTABEvents_SendLButtonClickEvent e)
		{
			int llLine = e.lineNo;
			if(llLine > -1 && myPermits[0] == "1")
			{
				string strUrno = tabList.GetFieldValue(llLine, "URNO");
				IRow [] rows = tabCLP.RowsByIndexValue("URNO", strUrno);
				if(e.colNo == 2 && rows.Length > 0)
				{
					string strDone = tabList.GetFieldValue(llLine, "CHEK");
					rows[0]["CHEK"] = strDone;
					if(strDone == "N")
					{
						tabList.SetFieldValues(llLine, "CHTM", "");
						rows[0]["CHTM"] = "";
					}
					else
					{
						string strTime = UT.DateTimeToCeda(DateTime.Now);
						tabList.SetFieldValues(llLine, "CHTM", strTime);
						rows[0]["CHTM"] = strTime;
					}
					tabList.Refresh();
					rows[0].Status = State.Modified;
                    //tabCLP.Release(300, "LATE,NOBC,NOACTION,NOLOG", null);//Remark by AM (20070723)
                    tabCLP.Save(rows[0]);
				}
			}
		}

		private void btnPrint_Click(object sender, System.EventArgs e)
		{
			string strFlno = "";
			IRow [] rows =  tabAFT.RowsByIndexValue("URNO", myAftUrno);
			if(rows.Length > 0)
			{
				strFlno = rows[0]["FLNO"];
			}
			rptFlightRelatedOverview r = new rptFlightRelatedOverview(tabList, strFlno, cbArea.Text, txtRemark.Text );
			frmPrintPreview p = new frmPrintPreview(r);
			p.Show();
		}

		private void txtRemark_TextChanged(object sender, System.EventArgs e)
		{
			if(myPermits[0] == "1")
			{
				string strTxt = txtRemark.Text;
				if(strTxt != currRemark)
				{
					btnSave.BackColor = Color.Red;
				}
				else
				{
					btnSave.BackColor = Color.Transparent;
				}
			}
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			if(btnSave.BackColor == Color.Red)
			{
				IRow row = null;
				if(tabREM.Count == 0)
				{
					row = tabREM.CreateEmptyRow();
					row.Status = State.Created;
					row["USEC"] = UT.UserName;
					row["CDAT"] = UT.DateTimeToCeda(DateTime.Now);
					tabREM.Add(row);
				}
				else
				{
					row = tabREM[0];
					row.Status = State.Modified;
					row["USEU"] = UT.UserName;
					row["LSTU"] = UT.DateTimeToCeda(DateTime.Now);
				}
				//RURN,APPL,RTAB,PURP,TEXT,CDAT,LSTU,USEC,USEU
				row["RURN"] = myAftUrno;
				row["APPL"] = "CHKLSTPR";
				row["RTAB"] = "AFT";
				row["PURP"] = "REMA";
				row["TEXT"] = txtRemark.Text;
				tabREM.Release(300, "LATE,NOBC,NOACTION,NOLOG", null);
				btnSave.BackColor = Color.Transparent;
			}
		}

		private void tabList_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			
			if(myPermits[3] == "0")
			{
				dtCHTM.Visible = false;
				return;
			}
			if(e.lineNo > -1 && e.colNo == UT.GetItemNo(tabList.LogicalFieldList, "CHTM"))
			{
				SetTimePicker();
			}
			else
			{
				dtCHTM.Visible = false;
			}
		}
		private void SetTimePicker()
		{
			int currSel = tabList.GetCurrentSelected();
			if(currSel > -1)
			{
				int i = 0;
				string strCHTM = tabList.GetFieldValue(currSel, "CHTM");
				int tmpLeft = 0;
				int tmpRight = 0;
				int tmpFaktor = currSel - myTabScrollPos + 1;
				int tmpTop = tabList.Top + (tmpFaktor * tabList.LineHeight);
				string [] arrHeader = tabList.HeaderLengthString.Split(',');
				int idx = UT.GetItemNo(tabList.LogicalFieldList, "CHTM");
				for( i = 0; i < idx; i++)
				{
					int tmp = Convert.ToInt32( arrHeader[i]);
					if(tmp == -1)
						tmp = 0;
					tmpLeft += tmp;
				}
				tmpLeft += tabList.Left;
				tmpRight = tmpLeft + Convert.ToInt32( arrHeader[idx]);
				dtCHTM.Top = tmpTop;
				dtCHTM.Left = tmpLeft;
				dtCHTM.Width = tmpRight - tmpLeft;

				DateTime datCHTM;
				if(strCHTM != "")
				{
					datCHTM = UT.CedaFullDateToDateTime(strCHTM);
				}
				else
				{
					if(UT.IsTimeInUtc == true)
						datCHTM = DateTime.UtcNow;
					else
						datCHTM = DateTime.Now;
				}
				dtCHTM.Value = datCHTM;
				dtCHTM.ShowUpDown = true;
				dtCHTM.Visible = true;
				dtCHTM.Select();
			}
		}

		private void tabList_OnVScroll(object sender, AxTABLib._DTABEvents_OnVScrollEvent e)
		{
			myTabScrollPos = e.lineNo;
			dtCHTM.Visible = false;
		}

		private void tabList_RowSelectionChanged(object sender, AxTABLib._DTABEvents_RowSelectionChangedEvent e)
		{
			dtCHTM.Visible = false;
		}

		private void dtCHTM_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if(e.KeyChar == (char)13)
			{
				int idx = tabList.GetCurrentSelected();
				if(idx > -1 && myPermits[3] == "1")
				{
					string strUrno = tabList.GetFieldValue(idx, "URNO");
					IRow [] rows = tabCLP.RowsByIndexValue("URNO", strUrno);
					if(rows.Length > 0)
					{
						DateTime datCHTM = dtCHTM.Value;
						string strDone = tabList.GetFieldValue(idx, "CHEK");
						rows[0]["CHEK"] = strDone;
						if(strDone == "N")
						{
							tabList.SetFieldValues(idx, "CHTM", "");
							rows[0]["CHTM"] = "";
						}
						else
						{
							string strTime = UT.DateTimeToCeda(datCHTM);
							tabList.SetFieldValues(idx, "CHTM", strTime);
							rows[0]["CHTM"] = strTime;
						}
						tabList.Refresh();
						rows[0].Status = State.Modified;
                        //tabCLP.Release(300, "LATE,NOBC,NOACTION,NOLOG", null);//Remarked by AM @ 20070723
                        tabCLP.Save(rows[0]);//Added by AM@20070723
						dtCHTM.Visible = false;
					}
				}
			}
		}

        private void cbArea_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

	
	}
}
