using System;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Runtime.InteropServices;
using System.Collections;
using System.ComponentModel;
using System.Text;
using Ufis.Data;

namespace Ufis.Utils
{
	/// <summary>
	/// UT is the UfisTool Class. It provides helper tools 
	/// such as item methods and color definition. A lot of methods 
	/// are defined as static, so the user does not need to declare
	/// an instance of this class.
	/// </summary>
	/// <remarks>none.</remarks>
	/// <example>
	/// The following codes demonstrate some useful calls of the <B>UT</B> class.
	/// <code>
	/// [C#]
	/// using Ufis.Data;
	/// using Ufis.Utils;
	/// 
	/// public myClass
	/// {
	///		IDatabase myDB = null;		// IDatabase instance for data access.
	///		ITable myAdresses = null;	// ITable instance for "Adresses" table.
	///		
	///		public myClass(string opParameter)
	///		{
	///			myDB = UT.GetMemDB();	// Gets ( and generates) the one and only instance
	///			string tmpStr = "A,B,C,D,E,F";
	///			int itemNo = UT.GetItemNo(tmpStr, "C" ); // itemNo = 2 as the index of "C"
	///			
	///			//produce an exception and add it to the internal exception array of UT.
	///			int f = 0, v = 1, r = 0;
	///			try
	///			{
	///				r = (int)v/f; // This produces a division by zero exception
	///			}
	///			catch(System.Exception err)
	///			{
	///				String para="";
	///				para = "Para: " + opParameter; //set here the parameters of this method.
	///				// add the exception to the internal array
	///				// to be able to see it by displaying the frmException dialog.
	///				UT.AddException("myClass", "myClass() constructor", 
	///								err.Message.ToString(), para, "");
	///			}
	///			
	///			DateTime myNow = DateTime.Now;
	///			string strCedaNow = UT.DateTimeToCeda(myNow);
	///			// strCedaNow = e.g. "20051009124500";
	///			
	///			DateTime myTifa;
	///			string strTime = UT.CedaFullDateToDateTime("20051010121200");
	///			// strTime contains: 10.10.2005 12:12:00
	///		}
	/// }
	/// </code>
	/// </example>
	public class UT
	{
		#region Members
		#region Colors
		/// <summary>
		/// Red: colRed = 255
		/// </summary>
		/// <remarks>This colour definition can directly be used to set colours
		/// for the TAB.ocx control. Otherwise you have to convert the .NET colours
		/// into the long values represented on COLORREF in C++. But we thought this
		/// makes is easier.</remarks>
		public static int colRed = 255;
		///<summary>
		///Green: colGreen = 49152
		///</summary>
		/// <remarks>This colour definition can directly be used to set colours
		/// for the TAB.ocx control. Otherwise you have to convert the .NET colours
		/// into the long values represented on COLORREF in C++. But we thought this
		/// makes is easier.</remarks>
		public static int colGreen = 49152;
		///<summary>
		/// Light Green: colLightGreen = 8454016
		///</summary>
		/// <remarks>This colour definition can directly be used to set colours
		/// for the TAB.ocx control. Otherwise you have to convert the .NET colours
		/// into the long values represented on COLORREF in C++. But we thought this
		/// makes is easier.</remarks>
		public static int colLightGreen = 8454016;
		///<summary>
		/// Yellow: colYellow = 65535
		///</summary>
		/// <remarks>This colour definition can directly be used to set colours
		/// for the TAB.ocx control. Otherwise you have to convert the .NET colours
		/// into the long values represented on COLORREF in C++. But we thought this
		/// makes is easier.</remarks>
		public static int colYellow = 65535;
		///<summary>
		/// Light Yellow: colLightYellow = 12648447
		///</summary>
		/// <remarks>This colour definition can directly be used to set colours
		/// for the TAB.ocx control. Otherwise you have to convert the .NET colours
		/// into the long values represented on COLORREF in C++. But we thought this
		/// makes is easier.</remarks>
		public static int colLightYellow = 12648447;
		///<summary>
		/// <p>static member use: UT.colBlack</p><p>Black: colBlack = 0</p>
		///</summary>
		/// <remarks>This colour definition can directly be used to set colours
		/// for the TAB.ocx control. Otherwise you have to convert the .NET colours
		/// into the long values represented on COLORREF in C++. But we thought this
		/// makes is easier.</remarks>
		public static int colBlack = 0;
		///<summary>
		/// White: colWhite = 16777215
		///</summary>
		/// <remarks>This colour definition can directly be used to set colours
		/// for the TAB.ocx control. Otherwise you have to convert the .NET colours
		/// into the long values represented on COLORREF in C++. But we thought this
		/// makes is easier.</remarks>
		public static int colWhite = 16777215;
		///<summary>
		/// Blue: colBlue = 16711680
		///</summary>
		/// <remarks>This colour definition can directly be used to set colours
		/// for the TAB.ocx control. Otherwise you have to convert the .NET colours
		/// into the long values represented on COLORREF in C++. But we thought this
		/// makes is easier.</remarks>
		public static int colBlue = 16711680;
		///<summary>
		/// Gray: colGray = 12632256
		///</summary>
		/// <remarks>This colour definition can directly be used to set colours
		/// for the TAB.ocx control. Otherwise you have to convert the .NET colours
		/// into the long values represented on COLORREF in C++. But we thought this
		/// makes is easier.</remarks>
		public static int colGray = 12632256;
		///<summary>
		/// colLightGray = 14737632
		///</summary>
		/// <remarks>This colour definition can directly be used to set colours
		/// for the TAB.ocx control. Otherwise you have to convert the .NET colours
		/// into the long values represented on COLORREF in C++. But we thought this
		/// makes is easier.</remarks>
		public static int colLightGray = 14737632;
		///<summary>
		/// colCyan = 16776960
		///</summary>
		/// <remarks>This colour definition can directly be used to set colours
		/// for the TAB.ocx control. Otherwise you have to convert the .NET colours
		/// into the long values represented on COLORREF in C++. But we thought this
		/// makes is easier.</remarks>
		public static int colCyan = 16776960;
		///<summary>
		/// colDarkGreen = 32768
		///</summary>
		/// <remarks>This colour definition can directly be used to set colours
		/// for the TAB.ocx control. Otherwise you have to convert the .NET colours
		/// into the long values represented on COLORREF in C++. But we thought this
		/// makes is easier.</remarks>
		public static int colDarkGreen = 32768;
		/// <summary>
		/// colPurple = 12583104
		/// </summary>
		/// <remarks>This colour definition can directly be used to set colours
		/// for the TAB.ocx control. Otherwise you have to convert the .NET colours
		/// into the long values represented on COLORREF in C++. But we thought this
		/// makes is easier.</remarks>
		public static int colPurple = 12583104;
		/// <summary>
		/// colOrange = 33023
		/// </summary>
		/// <remarks>This colour definition can directly be used to set colours
		/// for the TAB.ocx control. Otherwise you have to convert the .NET colours
		/// into the long values represented on COLORREF in C++. But we thought this
		/// makes is easier.</remarks>
		public static int colOrange = 33023;
		/// <summary>
		/// colLightOrange = 8438015
		/// </summary>
		/// <remarks>This colour definition can directly be used to set colours
		/// for the TAB.ocx control. Otherwise you have to convert the .NET colours
		/// into the long values represented on COLORREF in C++. But we thought this
		/// makes is easier.</remarks>
		public static int colLightOrange = 8438015;
		/// <summary>
		/// colPink = 16744703
		/// </summary>
		/// <remarks>This colour definition can directly be used to set colours
		/// for the TAB.ocx control. Otherwise you have to convert the .NET colours
		/// into the long values represented on COLORREF in C++. But we thought this
		/// makes is easier.</remarks>
		public static int colPink = 16744703;

		#endregion Color

        private static bool _logMsg = true;
        private static string _logFilePath = @"C:\TMP\";
        private static string _logFileNamePrefix = "GenLog";
        const string LOG_FILE_EXT = ".TXT";
        private static DateTime _lastLogClearDate = new DateTime(1, 1, 1);
        private static string _logStartMsg = "";


        public static void SetLogStartMsg(string msg)
        {
            if (msg == null) _logStartMsg = "";
            else _logStartMsg = msg.Trim();
        }
        public static void StartLogging() { _logMsg = true; }
        public static void StopLogging() { _logMsg = false; }
        public static string GetLogFilePath() { return _logFilePath; }
        public static void SetLogFilePath(string path)
        {
            if (string.IsNullOrEmpty(path)) throw new ApplicationException("Invalid Path");
            string st = path.Trim();
            if (st[st.Length - 1] == '\\')
            {
                _logFilePath = st;
            }
            else
            {
                _logFilePath = st + @"\";
            }            
        }

        public static void SetLogFileNamePrefix(string fileNamePrefix)
        {
            if (string.IsNullOrEmpty(fileNamePrefix)) throw new ApplicationException("Invalid Log File Prefix");
            _logFileNamePrefix = fileNamePrefix.Trim();

        }
        

        public static string GetLogFileName(DateTime dt)
        {
            return String.Format( GetLogFilePath() + _logFileNamePrefix + @"{0:00}{1:00}{2}", dt.Month, dt.Day, LOG_FILE_EXT );
        }

        const int MAX_BK_LOG_CNT = 100;//Maximum Number of backup Log.
        const long MAX_LOG_SIZE = 3200000;//Maximum Log File Size in Byte
        private static long _logLineCnt = 0;


        public static void ClearLog()
        {
            try
            {
                DateTime curDate = DateTime.Now;
                TimeSpan ts = curDate.Subtract(_lastLogClearDate);
                if (ts.Days != 0)
                {
                    for (int i = 7; i < 100; i++)
                    {//Delete Backup Logs from -7 to -100 days
                        try
                        {
                            string logFileName = GetLogFileName(curDate.AddDays(-i));
                            System.IO.File.Delete(logFileName);//Delete the main log for that day
                            for (int j = 0; j < MAX_BK_LOG_CNT; j++)
                            {
                                try
                                {
                                    System.IO.File.Delete(GetBkFileName( logFileName, j));//Delete the backup log for that day
                                }
                                catch (Exception)
                                {
                                }
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                    _lastLogClearDate = curDate;
                }
            }
            catch (Exception)
            {
            }
        }

        private static string GetBkFileName(string logFileName, int bkNo)
        {
            string f1 = "";
            int len = logFileName.Length;
            if (logFileName.EndsWith(LOG_FILE_EXT))
            {
                f1 = logFileName.Substring(0, len - LOG_FILE_EXT.Length);
            }

            return string.Format("{0}_{1:00}{2}", f1, bkNo, LOG_FILE_EXT);
        }

        private static string MoveToBkFile(string logFilePath)
        {
            bool moveFlag = false;
            string bkFile = "";
            if ((System.IO.File.Exists(logFilePath)) && (_logLineCnt % 200) == 0)
            {//Do it on every 200 lines 
                try
                {                    
                    System.IO.FileInfo fInfo = new System.IO.FileInfo(logFilePath);
                    if (fInfo.Length > MAX_LOG_SIZE)
                    {
                        for (int i = 0; i < MAX_BK_LOG_CNT; i++)
                        {
                            bkFile = GetBkFileName(logFilePath, i);
                            if (System.IO.File.Exists(bkFile)) continue;
                            fInfo.MoveTo(bkFile);
                            fInfo = null;
                            moveFlag = true;
                            System.IO.File.AppendAllText(bkFile, GetFormattedLogMsg("Backup Completed.", System.DateTime.Now));
                            _logLineCnt = 0;
                            break;
                        }
                    }
                }
                catch (Exception)
                {
                }
            }
            if (!moveFlag) bkFile = "";
            return bkFile;
        }

        private static object _lockLog = new object();
        public static void LogMsg(string msg)
        {
            if (_logMsg)
            {
                try
                {
                    lock (_lockLog)
                    {
                        ClearLog();
                        DateTime now = DateTime.Now;
                        string logFilePath = GetLogFileName(now);
                        try
                        {
                            string bkFileName = MoveToBkFile(logFilePath);//Backup the file
                            if (bkFileName.Trim() != "")
                            {
                                System.IO.File.AppendAllText(logFilePath, GetFormattedLogMsg("Backup the previous File to " + bkFileName + ". " + _logStartMsg, now));
                                _logLineCnt++;
                            }
                        }
                        catch (Exception)
                        {
                        }
                        System.IO.File.AppendAllText(logFilePath, GetFormattedLogMsg(msg, now) );
                        _logLineCnt++;
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private static string GetFormattedLogMsg( string msg, DateTime now )
        {
            return String.Format("{0:00}{1:00}{2:00}{3:00}.{4:000} : {5}{6}", now.Day, now.Hour, now.Minute, now.Second, now.Millisecond, msg, Environment.NewLine);
        }

		/// <summary>
		/// Delegate for new exeption occured
		/// </summary>
		/// <param name="Cnt">The number of currently stored exceptions in
		/// <see cref="arrExceptions"/>.</param>
		/// <remarks>You can register an event handler to display
		/// the <see cref="frmExceptions"/> dialog for debug purposes. This
		/// is useful to get the <B>UT class</B> internally catched exceptions.</remarks>
		public delegate void ExceptionOccured(int Cnt);
		/// <summary>
		/// Event function for the delegate "ExceptionOccured"
		/// </summary>
		/// <remarks>This is the <B>Event</B>, which will be triggered from
		/// the delegate <see cref="ExceptionOccured"/>.</remarks>
		public static event ExceptionOccured OnExceptionOccured;

		/// <summary>
		/// This dialog can be used for onsite debugging purposes to show
		/// all stored exception, which might have occured during programm runtime.
		/// </summary>
		/// <remarks>You can use the UT member for the <see cref="frmExceptions"/> dialog
		/// or your own instance within you application.</remarks>
		public static frmExceptions dlgException;
		/// <summary>
		/// Stores all exeptions, please refer to AddException Method.
		/// </summary>
		/// <remarks>You can call the <see cref="frmExceptions"/> dialog to show
		/// all occured exception or you can use the UT's <B>arrExceptions</B> array.
		/// </remarks>
		public static System.Collections.ArrayList arrExceptions = new ArrayList(1000);
		/// <summary>
		/// For global usage of the connected server name.
		/// </summary>
		/// <remarks>To have a global accessible instance of the <B>ServerName</B>
		/// you can simply use this one.</remarks>
		public static String ServerName = "";
		/// <summary>
		/// For global usage of HOPO
		/// </summary>
		/// <remarks>To have a global accessible instance of the <B>Hopo</B>
		/// you can simply use this one.</remarks>
		public static String Hopo = "";
		/// <summary>
		/// For global usage of logged in User
		/// </summary>
		/// <remarks>To have a global accessible instance of the <B>UserName</B>
		/// you can simply use this one.</remarks>
		public static String UserName = "";
		/// <summary>
		/// Contains the applications timeframe FROM for further global usage.
		/// </summary>
		/// <remarks>To have a global accessible instance of the <B>TimeFrameFrom</B>
		/// you can simply use this one.</remarks>
		public static DateTime TimeFrameFrom;
		/// <summary>
		/// Contains the applications timeframe TO for further global usage.
		/// </summary>
		/// <remarks>To have a global accessible instance of the <B>TimeFrameTo</B>
		/// you can simply use this one.</remarks>
		public static DateTime TimeFrameTo;
		/// <summary>
		/// Provides a search window. Can be used to
		/// ask the user for a search string to be found
		/// for each purpose.
		/// </summary>
		/// <remarks>To have a global accessible instance of the <B>frmSearchDlg</B>
		/// you can simply use this one.</remarks>
		public static frmSearchDlg omSearchDlg;
		/// <summary>
		/// Stores the last seach text to be reused if necessary.
		/// </summary>
		/// <remarks>none.</remarks>
		public static String LastSearchText;
		/// <summary>
		/// Global Identifier if times are in UTC or not
		/// </summary>
		/// <remarks>To have a global accessible instance of the <B>IsTimeInUtc</B>
		/// you can simply use this one.</remarks>
		public static bool IsTimeInUtc=false;
		/// <summary>
		/// Containts then UTC-Offest in hours
		/// </summary>
		/// <remarks>To have a global accessible instance of the <B>UTCOffset</B>
		/// you can simply use this one.</remarks>
		public static short UTCOffset=0;
		/// <summary>
		/// This is an indicator, whether the CEDA.ini was read
		/// for the parameter [GLOBAL]UTCCONVERTIONS=YES/NO to ensure that
		/// the CEDA.ini is read once.
		/// </summary>
		public static bool readTimeConsideration=false;
		/// <summary>
		/// Stores the value for [GLOBAL]UTCCONVERTIONS=YES/NO. This is the
		/// indicator whether times should be convertable from UTC to local
		/// and vice versa due to the fact that the times in PVG are stored
		/// in Bejing local ==> so the UTCToLocal would fail. This has an impact
		/// on the UTCToLocal and LocalToUTC. If the value is "true" the convertion
		/// will be done, otherwise the input parameter will be returned without
		/// any change. For the case that the parameter does not exist in CEDA.ini
		/// the default is to convert all times as prior. 
		/// </summary>
		public static bool ChangeTimeTimesUTCLocal=true;

		private static IDatabase myMemDB = null;

		/// <summary>
		/// The instance of UFIS com to ensure, that the application
		/// developer can instantiate one instance of <B>UFISCOMLib.UfisComClass</B>
		/// and use it everywhere within his applicatio namespace.
		/// </summary>
		/// <remarks>When you create an instance and use it, pleas make
		/// sure that you call the <B>myUfisCom.CleanupCom()</B> method to
		/// disconnect from the server in a clean way.</remarks>
		public static UFISCOMLib.UfisComClass myUfisCom=null;
		#endregion
		/// <summary>
		/// Simple Contructor of UT
		/// </summary>
		/// <remarks>This is not necessary, because the class and all members are static
		/// so you need not to call the constructor.</remarks>
		public UT()
		{
		}
		/// <summary>
		/// Returns the one and only <see cref="Ufis.Data.IDatabase"/> object, which
		/// is necessary to process database data.
		/// </summary>
		/// <returns>The instance of <B>IDatabase</B></returns>
		/// <example>
		/// The following code snippet demonstrate the usage of <B>GetMemDB</B>:
		/// <code>
		/// [C#]
		/// IDatabase myDB = UT.GetMemDB();
		/// </code>
		/// </example>
		/// <remarks>none.</remarks>
		public static IDatabase GetMemDB()
		{
			if(myMemDB == null)
			{
                //myMemDB = Database.Create();
                myMemDB = Database.CreateSync();
			}
			return myMemDB;
		}

		/// <summary>
		/// Disposes the one and only <see cref="Ufis.Data.IDatabase"/> object.
		/// </summary>
		/// <remarks>Should be called when the application is terminated.</remarks>
		public static void DisposeMemDB()
		{
			if(myMemDB != null)
			{
				((IDisposable)myMemDB).Dispose();
			}
		}

		#region Resource validities and blocking Times
		/// <summary>
		/// Retrieves the effective availability and blocking period for the resources based on the validity 
		/// contained in resTable and blocking information contained in blkTable. The effective availability
		/// is loaded into the parameter resAvailTable and blocking into the parameter resBlkTable.
		/// 
		/// The parameters fromDate and toDate determines the range for data is loaded.
		/// 
		/// </summary>
		/// <param name="resTable">Contains details of all resources whose availability has be found. resTable should have following fields
		///		CNAM,URNO,VAFR,VATO. The parameter passed as resNameField (e.g.:CNAM) should be present in this table.</param>
		/// <param name="fmDtResField">Field name inside the resTable which has the information on the validity start date for the resource</param>
		/// <param name="toDtResField">Field name inside the resTable which has the information on the validity end date for the resource</param>
		/// <param name="resNameField">Field name inside the resTable which has the name of the resource</param>
		/// <param name="blkTable">Contains all the blocking periods for different resources. blkTable should have following fields
		///		BURN,NAFR,NATO,DAYS,TIFR,TITO</param>
		/// <param name="fmDtBlkField">Field name inside the blkTable which has the information on the blocking start date for the resource</param>
		/// <param name="toDtBlkField">Field name inside the blkTable which has the information on the blocking end date for the resource</param>
		/// <param name="fromDate">In combination with toDate defines the date range </param>
		/// <param name="toDate">In combination with fromDate defines the date range </param>
		/// <param name="resAvailTable">Contains the information of all the date ranges for which the particular resource is available</param>
		/// <param name="resBlkTable">Contains the information of all the date ranges for which the particular resource is blocked</param>
		public static void GetAvailability(ref ITable resTable, string fmDtResField, string toDtResField, string resNameField, ref ITable blkTable, string fmDtBlkField, string toDtBlkField, DateTime fromDate, DateTime toDate, ref ITable resAvailTable, ref ITable resBlkTable)
		{
			string strUrno = "";
			string strResName = "";
			string[] strData;
			Char[] seperator = {'#'};
			DateTime dtValidFm;
			DateTime dtValidTo;
			int range;
			IRow rowData;
			bool hasBlockInfo = false;
			ArrayList resBlkCol = new ArrayList();

			if (blkTable != null && blkTable.Count > 0 )
			{
				blkTable.Sort("BURN",true);
				hasBlockInfo = true;
			}
			for (int i = 0; i < resTable.Count; i++) 
			{
				strUrno = resTable[i]["URNO"];
				strResName = resTable[i][resNameField];
				if (strUrno != "")
				{
					dtValidFm = CedaFullDateToDateTime(resTable[i][fmDtResField]);
					if (resTable[i][toDtResField].Length < 1)
						dtValidTo = toDate;
					else 
						dtValidTo = CedaFullDateToDateTime(resTable[i][toDtResField]);
					range = GetRange(dtValidFm,dtValidTo,fromDate,toDate);

					if (hasBlockInfo) 
						GetBlockedPeriod(strUrno,fromDate,toDate,fmDtBlkField,toDtBlkField, ref blkTable, ref resBlkCol);
					switch(range)
					{
						case 1:
							PopulateAvailability(strUrno,strResName,range,fromDate,toDate,fromDate,dtValidTo,ref resBlkCol,ref resAvailTable);
							break;
						case 2:
							PopulateAvailability(strUrno,strResName,range,fromDate,toDate,dtValidFm,dtValidTo,ref resBlkCol,ref resAvailTable);
							break;
						case 3:
							PopulateAvailability(strUrno,strResName,range,fromDate,toDate,dtValidFm,toDate,ref resBlkCol,ref resAvailTable);
							break;
						case 4:
							PopulateAvailability(strUrno,strResName,range,fromDate,toDate,fromDate,toDate,ref resBlkCol,ref resAvailTable);
							break;
						case 5:
							PopulateAvailability(strUrno,strResName,range,fromDate,toDate,fromDate,toDate,ref resBlkCol,ref resAvailTable);
							break;
					}
					for (int j=0; j<resBlkCol.Count; j++) 
					{
						strData = resBlkCol[j].ToString().Split(seperator,3);
						rowData = resBlkTable.CreateEmptyRow();
						rowData["URNO"] = strUrno;
						rowData["NAFR"] = strData[0];
						rowData["NATO"] = strData[1];
						rowData["NAME"] = strResName;
						rowData.Status = State.Created;
						resBlkTable.Add(rowData);
					}
					resBlkCol.Clear();
				}
			}
		}

		/// <summary>
		/// Checks if the resource contained in the resTable has an availability for the date range specified.
		/// </summary>
		/// <param name="resTable">Contains details of all resources whose availability has be found. resTable should have following fields
		///		CNAM,URNO,VAFR,VATO	</param>
		/// <param name="fmDtResField">Field name inside the resTable which has the information on the validity start date for the resource</param>
		/// <param name="toDtResField">Field name inside the resTable which has the information on the validity end date for the resource</param>
		/// <param name="blkTable">Contains all the blocking periods for different resources. blkTable should have following fields
		///		BURN,NAFR,NATO,DAYS,TIFR,TITO</param>
		/// <param name="fmDtBlkField">Field name inside the blkTable which has the information on the blocking start date for the resource</param>
		/// <param name="toDtBlkField">Field name inside the blkTable which has the information on the blocking end date for the resource</param>
		/// <param name="fromDate">In combination with toDate defines the date range </param>
		/// <param name="toDate">In combination with fromDate defines the date range </param>
		/// <returns>TRUE, if the resource is available in the date range else FALSE</returns>
		public static bool IsAvailable(ref ITable resTable, string fmDtResField, string toDtResField, ref ITable blkTable, string fmDtBlkField, string toDtBlkField, DateTime fromDate, DateTime toDate)
		{
			string strUrno = "";
			Char[] seperator = {'#'};
			DateTime dtValidFm;
			DateTime dtValidTo;
			int range;
			bool hasBlockInfo = false;
			bool status = false;
			ArrayList resBlkCol = new ArrayList();

			if (blkTable != null && blkTable.Count > 0 )
			{
				blkTable.Sort("BURN",true);
				hasBlockInfo = true;
			}
			for (int i=0; i<resTable.Count; i++) 
			{
				strUrno = resTable[i]["URNO"];
				if (strUrno != "")
				{
					dtValidFm = CedaFullDateToDateTime(resTable[i][fmDtResField]);
					if (resTable[i][toDtResField].Length < 1)
						dtValidTo = toDate;
					else 
						dtValidTo = CedaFullDateToDateTime(resTable[i][toDtResField]);
					range = GetRange(dtValidFm,dtValidTo,fromDate,toDate);

					if (hasBlockInfo) 
						GetBlockedPeriod(strUrno,fromDate,toDate,fmDtBlkField,toDtBlkField, ref blkTable, ref resBlkCol);
					switch(range)
					{
						case 1:
							status = CheckAvailability(strUrno,range,fromDate,toDate,fromDate,dtValidTo,ref resBlkCol);
							break;
						case 2:
							status = CheckAvailability(strUrno,range,fromDate,toDate,dtValidFm,dtValidTo,ref resBlkCol);
							break;
						case 3:
							status = CheckAvailability(strUrno,range,fromDate,toDate,dtValidFm,toDate,ref resBlkCol);
							break;
						case 4:
							status = CheckAvailability(strUrno,range,fromDate,toDate,fromDate,toDate,ref resBlkCol);
							break;
						case 5:
							status = CheckAvailability(strUrno,range,fromDate,toDate,fromDate,toDate,ref resBlkCol);
							break;
					}
				}
			}
			return status;
		}
	
		/// <summary>
		/// This compares the dates contained in the first 2 fields wrt to last 2 date fields and 
		/// returns the following status codes:
		/// 
		/// 1 - fromDate less than refFromDate and toDate lies between refFromDate and refToDate
		/// 2 -	fromDate and toDate lies inside the date range defined by refFromDate and refToDate
		/// 3 - fromDate less than refToDate and toDate is greater than refToDate
		/// 4 - fromDate is less than refFromDate and toDate is greater than refToDate
		/// 5 - toDate is less than refFromDt or fromDt is greater than refToDate
		/// 
		/// </summary>
		/// <param name="fromDate">From date to be compared</param>
		/// <param name="toDate">To date to be compared</param>
		/// <param name="refFromDate">Reference from date based on which the comparison is to be performed</param>
		/// <param name="refToDate">Reference to date based on which the comparison is to be performed</param>
		/// <returns>Status code - 1/2/3/4/5 based on conditions</returns>
		static internal int GetRange(DateTime fromDate, DateTime toDate, DateTime refFromDate, DateTime refToDate)
		{
			if (fromDate < refFromDate && IsBetween(toDate,refFromDate,refToDate))
				return 1;
			else if (IsWithIn(fromDate,toDate,refFromDate,refToDate))
				return 2;
			else if (toDate > refToDate && IsBetween(fromDate,refFromDate,refToDate))
				return 3;
			else if (fromDate < refFromDate && toDate > refToDate)
				return 4;
			else
				return 5;
		}

		static internal void GetBlockedTimes(string strUrno, DateTime ropStartTime, DateTime ropEndTime, string fmDtBlkField,string toDtBlkField,IRow row,ArrayList ropBlockedTimes)
		{
			string olDays = row["DAYS"];

			DateTime olStartTime = ropStartTime;
			DateTime Nafr = CedaFullDateToDateTime(row[fmDtBlkField]);
			if (Nafr > olStartTime)
				olStartTime = Nafr;

			DateTime olEndTime = ropEndTime;
			DateTime Nato = ropEndTime;
			if (row[toDtBlkField].Length != 0)
			{
				Nato = CedaFullDateToDateTime(row[toDtBlkField]);
				if (Nato < olEndTime)
					olEndTime = Nato;
			}

			// convert tifr and tito from utc to local
			string olLocalTifr = row["TIFR"];
			string olLocalTito = row["TITO"];

			olStartTime = new DateTime(olStartTime.Year,olStartTime.Month,olStartTime.Day,0,0,0);
			olEndTime = new DateTime(olEndTime.Year,olEndTime.Month,olEndTime.Day,23,59,59);

			StringBuilder strBuf = new StringBuilder();

			for (DateTime olCurrent = olStartTime; olCurrent <= olEndTime; olCurrent += new TimeSpan(1,0,0,0))
			{

				int weekDay = 0;
				int	posInd;

				switch (olCurrent.DayOfWeek)
				{
					case DayOfWeek.Monday:
						weekDay = 1;
						break;
					case DayOfWeek.Tuesday:
						weekDay = 2;
						break;
					case DayOfWeek.Wednesday:
						weekDay = 3;
						break;
					case DayOfWeek.Thursday:
						weekDay = 4;
						break;
					case DayOfWeek.Friday:
						weekDay = 5;
						break;
					case DayOfWeek.Saturday:
						weekDay = 6;
						break;
					case DayOfWeek.Sunday:
						weekDay = 7;
						break;
				}

				posInd = olDays.IndexOf(weekDay.ToString());
				if (posInd >= 0)
				{
					DateTime olTifr = Nafr;
					if (olLocalTifr.Length != 0)
					{
						DateTime olTmpTifr = new DateTime(olCurrent.Year,olCurrent.Month,olCurrent.Day,int.Parse(olLocalTifr.Substring(0,2)),int.Parse(olLocalTifr.Substring(2)),0);						
						if (olTmpTifr > olTifr)
							olTifr = olTmpTifr;
					}

					// if Tito is empty or after Nato then use Nato
					DateTime olTito = (row[toDtBlkField].Length != 0) ? Nato : ropEndTime;
					if (olLocalTito.Length != 0)
					{
						DateTime olTmpTito = new DateTime(olCurrent.Year,olCurrent.Month,olCurrent.Day,int.Parse(olLocalTito.Substring(0,2)),int.Parse(olLocalTito.Substring(2)),0);						
						if (olTmpTito < olTifr)
						{
							// if Tito is before Tifr then add one day to Tito, i.e. the blocking time is before and after midnight
							olTmpTito += new TimeSpan(1,0,0,0);
							olTmpTito = new DateTime(olTmpTito.Year,olTmpTito.Month,olTmpTito.Day,int.Parse(olLocalTito.Substring(0,2)),int.Parse(olLocalTito.Substring(2)),0);
						}

						if (olTmpTito < olTito)
						{
							olTito = olTmpTito;
						}
					}

					// ensure that the bar times are within the specified time frame
					if (olTifr < ropStartTime)
						olTifr = ropStartTime;
					if (olTito > ropEndTime)
						olTito = ropEndTime;

					// check if we can merge this bar with existing overlapping bars (they are already ordered by time)
					bool blNotMerged = true;
					for (int ilB = 0; ilB < ropBlockedTimes.Count; ilB++)
					{
						string[] olBlk = ((string)ropBlockedTimes[ilB]).Split('#');
						DateTime Tifr  = CedaFullDateToDateTime(olBlk[0]);
						DateTime Tito  = CedaFullDateToDateTime(olBlk[1]);
						if (IsOverlapped(Tifr,Tito, olTifr, olTito))
						{
							if (olTifr < Tifr)
								Tifr = olTifr;
							if (olTito > Tito)
								Tito = olTito;
							blNotMerged = false;

							strBuf.Append(DateTimeToCeda(Tifr));
							strBuf.Append("#");
							strBuf.Append(DateTimeToCeda(Tito));
							strBuf.Append("#");
							strBuf.Append(strUrno);

							ropBlockedTimes[ilB] = strBuf.ToString();
							strBuf.Length = 0;
						}

					}

					if (blNotMerged && olTifr < olTito)
					{
						strBuf.Append(DateTimeToCeda(olTifr));
						strBuf.Append("#");
						strBuf.Append(DateTimeToCeda(olTito));
						strBuf.Append("#");
						strBuf.Append(strUrno);

						ropBlockedTimes.Add(strBuf.ToString());
						strBuf.Length = 0;
					}
						
				}
			}
		}

		/// <summary>
		/// Loads the block period for the specific resource in the time range into the collection resBlkCol.
		/// </summary>
		/// <param name="strUrno">URNO of the resource</param>
		/// <param name="fromDate">From Date</param>
		/// <param name="toDate">To Date</param>
		/// <param name="fmDtBlkField">Field name inside the blkTable which has the information on the blocking start date for the resource</param>
		/// <param name="toDtBlkField">Field name inside the blkTable which has the information on the blocking end date for the resource</param>
		/// <param name="blkTable">Contains all the blocking periods for different resources</param>
		/// <param name="resBlkCol">Block details for the particular resource will be loaded into this variable</param>
		static internal void GetBlockedPeriod(string strUrno, DateTime fromDate, DateTime toDate, string fmDtBlkField,string toDtBlkField, ref ITable blkTable, ref ArrayList resBlkCol)
		{
			StringBuilder strBuf = new StringBuilder();

			ArrayList olBlockedTimes = new ArrayList();
			for (int j = 0; j < blkTable.Count; j++) 
			{
				if (strUrno.Equals(blkTable[j]["BURN"])) 
				{
					GetBlockedTimes(strUrno,fromDate,toDate,fmDtBlkField,toDtBlkField,blkTable[j],olBlockedTimes);
				}
			}
			resBlkCol.AddRange(olBlockedTimes);

		}
		/// <summary>
		/// Checks availability of the resource based on the period defined by fromDate and toDate.
		/// </summary>
		/// <param name="strUrno">URNO of the resource</param>
		/// <param name="option">Type of overlap</param>
		/// <param name="fmDate">From date for date range</param>
		/// <param name="toDate">To date of date range</param>
		/// <param name="validFrom">Validity start period for the resource</param>
		/// <param name="validTo">Validity end for the resource</param>
		/// <param name="resBlkCol">Contains the information of all the date ranges for which the particular resource is blocked</param>
		/// <returns>TRUE if there is availability of resource for the range else FALSE</returns>
		static internal bool CheckAvailability(string strUrno, int option, DateTime fmDate, DateTime toDate, DateTime validFrom, DateTime validTo, ref ArrayList resBlkCol) 
		{
			bool status = false;
			DateTime blkStTime;
			DateTime blkEndTime;
			string[] strData;
			Char[] seperator = {'#'};
			StringBuilder strBuf = new StringBuilder();

			switch (option)
			{
				case 1:
				case 2:
				case 3:
				case 4:
					if (fmDate < validFrom) 
					{
						fmDate = validFrom;
					}
					if (resBlkCol.Count > 0)
						resBlkCol.Sort();
					for (int j=0; j<resBlkCol.Count; j++) 
					{
						strData = resBlkCol[j].ToString().Split(seperator,3);
						blkStTime = CedaFullDateToDateTime(strData[0]);
						blkEndTime = CedaFullDateToDateTime(strData[1]);
						if (fmDate < blkStTime)
						{
							status = true;
							fmDate = blkEndTime.AddSeconds(1);
						} 
						else if (fmDate==blkStTime)
						{
							fmDate = blkEndTime.AddSeconds(1);
						}
					}
					if (fmDate < validTo) 
					{
						status = true;
						fmDate = validTo.AddSeconds(1);
					}
					break;
				case 5:
					status = true;
					break;
			}
			return status;
		}

		/// <summary>
		/// This loads the effective availability of the resource based on the blocking information and validity of 
		/// the resource for the date range.
		/// </summary>
		/// <param name="strUrno">URNO of the resource</param>
		/// <param name="strResName">Name of the resource</param>
		/// <param name="option">Type of overlap</param>
		/// <param name="fmDate">From date for date range</param>
		/// <param name="toDate">To date of date range</param>
		/// <param name="validFrom">Validity start period for the resource</param>
		/// <param name="validTo">Validity end for the resource</param>
		/// <param name="resBlkCol">Contains the information of all the date ranges for which the particular resource is blocked</param>
		/// <param name="resAvailTable">Contains the information of all the date ranges for which the all resource is available</param>
		static internal void PopulateAvailability(string strUrno, string strResName, int option, DateTime fmDate, DateTime toDate, DateTime validFrom, DateTime validTo, ref ArrayList resBlkCol, ref ITable resAvailTable) 
		{
			IRow rowData;
			DateTime blkStTime;
			DateTime blkEndTime;
			string[] strData;
			Char[] seperator = {'#'};
			StringBuilder strBuf = new StringBuilder();

			switch (option)
			{
				case 1:
				case 2:
				case 3:
				case 4:
					if (fmDate < validFrom) 
					{
						strBuf.Append(DateTimeToCeda(fmDate)).Append("#")
							.Append(DateTimeToCeda(validFrom.AddSeconds(-1))).Append("#")
							.Append(strUrno);
						resBlkCol.Add(strBuf.ToString());
						strBuf.Remove(0,strBuf.Length);
						fmDate = validFrom;
					}
					if (resBlkCol.Count > 0)
						resBlkCol.Sort();
					for (int j=0; j<resBlkCol.Count; j++) 
					{
						strData = resBlkCol[j].ToString().Split(seperator,3);
						blkStTime = CedaFullDateToDateTime(strData[0]);
						blkEndTime = CedaFullDateToDateTime(strData[1]);
						if (fmDate < blkStTime)
						{
							rowData = resAvailTable.CreateEmptyRow();
							rowData["URNO"] = strUrno;
							rowData["VAFR"] = DateTimeToCeda(fmDate);
							rowData["VATO"] = DateTimeToCeda(blkStTime.AddSeconds(-1));
							rowData["NAME"] = strResName;
							rowData.Status = State.Created;
							resAvailTable.Add(rowData);
							fmDate = blkEndTime.AddSeconds(1);
						} 
						else if (fmDate==blkStTime)
						{
							fmDate = blkEndTime.AddSeconds(1);
						}
					}
					if (fmDate < validTo) 
					{
						rowData = resAvailTable.CreateEmptyRow();
						rowData["URNO"] = strUrno;
						rowData["VAFR"] = DateTimeToCeda(fmDate);
						rowData["VATO"] = DateTimeToCeda(validTo);
						rowData["NAME"] = strResName;
						rowData.Status = State.Created;
						resAvailTable.Add(rowData);
						fmDate = validTo.AddSeconds(1);
					}
					if (fmDate < toDate) 
					{
						strBuf.Append(DateTimeToCeda(fmDate)).Append("#")
							.Append(DateTimeToCeda(toDate)).Append("#")
							.Append(strUrno);
						resBlkCol.Add(strBuf.ToString());
						strBuf.Remove(0,strBuf.Length);
					}
					break;
				case 5:
					resBlkCol.Clear();
					strBuf.Append(DateTimeToCeda(fmDate)).Append("#")
						.Append(DateTimeToCeda(toDate)).Append("#")
						.Append(strUrno);
					resBlkCol.Add(strBuf.ToString());
					strBuf.Remove(0,strBuf.Length);
					break;
			}
		}
		#endregion Resource validities and blocking Times
		#region UFIS String helper functions
		/// <summary>
		/// returns a valid UifsCom object
		/// </summary>
		/// <returns>An instance of <see cref="Ufis.Data.IUfisComWriter"/>.</returns>
		/// <remarks>none.</remarks>
		public static IUfisComWriter GetUfisCom()
		{
			if(myMemDB == null)
			{
				myMemDB = GetMemDB();                
			}
			return myMemDB.Writer;
		}
		/// <summary>
		/// Counts the items in a string, e.g. "A,B,C" has 3 items
		/// </summary>
		/// <param name="itemlist">The item list as separated string</param>
		/// <param name="ItemSep">The item separator as string</param>
		/// <returns>The found number of items</returns>
		/// <remarks>none.</remarks>
		/// <example>
		/// The following example demonstrate the usage of <B>ItemCount</B>:
		/// <code>
		/// [C#]
		/// 
		/// string items = "A;B;C;D;F;G";
		/// int cnt = UT.ItemCount(items, ";"); // cnt = 6
		/// </code>
		/// </example>
		public static int ItemCount(String itemlist, String ItemSep )
		{
			int Result = 0;
			int SepPos;
			int SepLen;
			try
			{
				if(itemlist != "")
				{
					Result = 1;
					SepLen = ItemSep.Length;
					SepPos = 0;
					do
					{
						SepPos = itemlist.IndexOf(ItemSep, SepPos, itemlist.Length-SepPos);
						if(SepPos >= 0)
						{
							Result++;
							SepPos = SepPos + SepLen;
						}
					}while(SepPos >= 0);

				}
			}
			catch(System.Exception err)
			{
				String para="";
				para = "Para: itemlist=" + itemlist + " ItemSep=" + ItemSep;
				UT.AddException("UT", "ItemCount", err.Message.ToString(), para, "");
			}
			return Result;
		}//End ItemCount

		/// <summary>
		/// Identifies the position of an item insside a string and returns
		/// the postion in a comma separated string
		/// </summary>
		/// <param name="itemlist">Comma separated list of items</param>
		/// <param name="ItemValue">Item for which the position is searched for</param>
		/// <returns>the number or -1 in case of not found or empty string</returns>
		/// <remarks>The return value is 0 based, that means for the first item 
		/// you will be a 0.</remarks>
		/// <example>
		/// The following example demonstrate the usage of <B>GetItemNo</B>:
		/// <code>
		/// [C#]
		/// 
		/// string items = "A;B;C;D;F;G";
		/// int itemNo = UT.GetItemNo(items, "F"); // itemNo = 4
		/// </code>
		/// </example>
		public static int GetItemNo(String itemlist, String ItemValue )
		{
			int ret = -1;
			String strItemList;
			String strVal;
			int pos;
			strItemList = "," + itemlist + ",";
			strVal = "," + ItemValue + ",";

			try
			{
				pos = strItemList.IndexOf (strVal, 0, strItemList.Length );
				if (pos >= 0)
				{
					if(pos == 0)
					{
						ret = 0;
					}
					else
					{
						ret = ItemCount(strItemList.Substring(0, pos), ",") - 1;
					}
				}
			}
			catch(System.Exception err)
			{
				String para="";
				para = "Para: itemlist=" + itemlist + " ItemValue=" + ItemValue;
				UT.AddException("UT", "ItemCount", err.Message.ToString(), para, "");
			}
			return ret;
		}//End function GetItemNo

		/// <summary>
		/// Extracts an item out of a separated string.
		/// </summary>
		/// <param name="cpTxt">The separated string</param>
		/// <param name="ipNbr">The item number which are looking for</param>
		/// <param name="cpSep">The item separator for the string</param>
		/// <returns>The extracted item or "" in case of not found</returns>
		/// <example>
		/// The following example demonstrate the usage of <B>GetItem</B>:
		/// <code>
		/// [C#]
		/// 
		/// string items = "A;B;C;D;F;G";
		/// string oneitem = UT.GetItem(items, 4, ";"); // oneitem = "F"
		/// </code>
		/// </example>
		/// <remarks>none.</remarks>
		public static String GetItem(String cpTxt, int ipNbr, String cpSep)
		{
			int ilSepLen, ilFirstPos, ilLastPos;
			int ilItmLen= 0, ilItmNbr;
			String ret = "";
			try
			{
				if( ipNbr >= 0 )
				{
					ilSepLen = cpSep.Length;
					ilFirstPos =0;
					ilLastPos = 1;
					ilItmNbr = -1;
					while ((ilItmNbr < ipNbr) && (ilLastPos > 0))
					{
						ilItmNbr = ilItmNbr + 1;
						ilLastPos = cpTxt.IndexOf(cpSep, ilFirstPos, cpTxt.Length-ilFirstPos);
						if ((ilItmNbr < ipNbr) && (ilLastPos > 0))
						{
							ilFirstPos = ilLastPos + ilSepLen;
						}
					}
					if ( ilLastPos < ilFirstPos)
						ilLastPos = cpTxt.Length;
					if ((ilItmNbr == ipNbr) && (ilLastPos > ilFirstPos))
						ilItmLen = ilLastPos - ilFirstPos;
					ret = cpTxt.Substring( ilFirstPos, ilItmLen);
				}
			}
			catch(System.Exception err)
			{
				String para="";
				para = "Para: cpTxt=" + cpTxt + " ipNbr=" + ipNbr + " cpSep=" + cpSep;
				UT.AddException("UT", "ItemCount", err.Message.ToString(), para, "");
			}
			return ret;
		}
		/// <summary>
		/// Implements and calls a common usable search dialog.
		/// It return the text to be searched as string, which was entered by the user.
		/// </summary>
		/// <returns>Text to be searched</returns>
		/// <remarks>none.</remarks>
		/// <example>
		/// The following example demonstrate the usage of <B>ShowSearchDialog</B>:
		/// <code>
		/// [C#]
		/// 
		/// string val = UT.ShowSearchDialog();
		/// </code>
		/// </example>
		/// <remarks>none.</remarks>
		public static String ShowSearchDialog()
		{
			if(omSearchDlg == null)
			{
				omSearchDlg = new frmSearchDlg(LastSearchText);
			}
			omSearchDlg.ShowDialog();
			//omSearchDlg = null;
			return LastSearchText;
		}
		/// <summary>
		/// Draws a gradient for an object's graphic with a specified angle.
		/// </summary>
		/// <param name="gr">The graohics object.</param>
		/// <param name="Angle">Angle for the gradient.</param>
		/// <param name="ctl">The control to be gradiented.</param>
		/// <param name="fromColor">From colour.</param>
		/// <param name="toColor">To colour.</param>
		/// <example>
		/// The following example demonstrate the usage of <B>DrawGradient</B>:
		/// <code>
		///		UT.DrawGradient(e.Graphics, 90f, myButton, Color.WhiteSmoke, Color.Gray);
		/// </code>
		/// </example>
		/// <remarks>You must call this method in the <B>Paint</B> event of
		/// a control, e.g. a picturebox or a button to have a valid
		/// System.Drawing.Graphics object.</remarks>
		public static void DrawGradient(System.Drawing.Graphics gr, float Angle, System.Windows.Forms.Control ctl,
			System.Drawing.Color fromColor, System.Drawing.Color toColor)
		{
			int x = ctl.ClientRectangle.Width;
			int y = ctl.ClientRectangle.Height;
			try
			{
				LinearGradientBrush olBrush = new LinearGradientBrush(ctl.ClientRectangle, fromColor, toColor, Angle );
				gr.FillRectangle(olBrush, 0, 0, x, y);
				olBrush.Dispose();
			}
			catch(System.Exception err)
			{
				String strPara;
				strPara = "gr: <" + gr.ToString() + "> ";
				strPara += "Angle: <" + Angle.ToString() + "> ";
				strPara += "ctl: <" + ctl.GetType().ToString() + "> ";
				strPara += "fromColor: <" + fromColor.ToString() + "> ";
				strPara += "toColor: <" + toColor.ToString() + ">";
				AddException( "UT", "DrawGradient", err.Message.ToString(), strPara, "");
			}
		}// END DrawGradient(...)
		/// <summary>
		/// Converts a DateTime object in the (ceda)format into the "YYYYMMDDhhmmss" format
		/// </summary>
		/// <param name="opTime">The date/time object</param>
		/// <returns>string in "YYYYMMDDhhmmss" format</returns>
		/// <remarks>none.</remarks>
		public static String DateTimeToCeda(DateTime opTime)
		{
			String ret="";
			try
			{
				ret = opTime.Year.ToString() + opTime.Month.ToString().PadLeft(2,'0') + opTime.Day.ToString().PadLeft(2,'0') +
					opTime.Hour.ToString().PadLeft(2,'0') + opTime.Minute.ToString().PadLeft(2,'0') + opTime.Second.ToString().PadLeft(2,'0');
			}
			catch(System.Exception err)
			{
				String para="";
				para = "Para: opTime=" + opTime;
				UT.AddException("UT", "ItemCount", err.Message.ToString(), para, "");
			}
			return ret;
		}
		/// <summary>
		/// Converts a Ceda date-time format to a DateTime object.
		/// We assume that the format is "YYYYMMDDhhmmss"
		/// </summary>
		/// <param name="strDate">the date as strin, e.g. 20030510231000</param>
		/// <returns>DateTime object</returns>
		/// <remarks>none.</remarks>
		public static DateTime CedaFullDateToDateTime(String strDate)
		{
			DateTime datRet;
			datRet = CedaDateToDateTime(strDate);
			return datRet;
		}
		/// <summary>
		/// Converts a ceda date into a DateTime object.
		/// The length of the string should be 14 o 8, which
		/// means with date part only or a full date-time format.
		/// </summary>
		/// <param name="strDate">the date as string, e.g. 20031012090900 or only the
		/// Date part as 20031012</param>
		/// <returns>DateTime object</returns>
		/// <remarks>none.</remarks>
		public static DateTime CedaDateToDateTime(String strDate)
		{
			DateTime datRet;
			int ilYY=0;
			int ilMo=0;
			int ilDD=0;
			int ilHH=0;
			int ilMM=0;
			int ilSS=0;
			
			datRet = DateTime.Now;
			try
			{
				if (strDate.Trim() != "")
				{
					if (strDate.Length > 8)
					{
						ilYY = Convert.ToInt32(strDate.Substring(0,4));
						ilMo = Convert.ToInt32(strDate.Substring(4,2));
						ilDD = Convert.ToInt32(strDate.Substring(6,2));
						ilHH = Convert.ToInt32(strDate.Substring(8,2));
						ilMM = Convert.ToInt32(strDate.Substring(10,2));
						ilSS = Convert.ToInt32(strDate.Substring(12,2));
						datRet = new DateTime(ilYY, ilMo, ilDD, ilHH, ilMM,ilSS);
					}	
					else //Date part exists without time
					{
						ilYY = Convert.ToInt32(strDate.Substring(0,4));
						ilMo = Convert.ToInt32(strDate.Substring(4,2));
						ilDD = Convert.ToInt32(strDate.Substring(6,2));
						datRet = new DateTime(ilYY, ilMo, ilDD, 0, 0, 0 );
					}
				}
			}
			catch(System.Exception err)
			{
				String para="";
				para += "Para: strDate=" + strDate;
				UT.AddException("UT", "CedaDateToDateTime", err.Message.ToString(), para, "");
			}
			return datRet;
		}//END CedaDateToDateTime
		/// <summary>
		/// Converts a ceda time into a DateTime object. The date part will be set to 01.01.1900.
		/// The length of the string should be 14 o 8, which
		/// means with date part only or a full date-time format.
		/// </summary>
		/// <param name="strDate">the date as string, e.g. 20031012090900 or only the
		/// time part as 090900</param>
		/// <returns>DateTime object</returns>
		/// <remarks>none.</remarks>
		public static DateTime CedaTimeToDateTime(String strDate)
		{
			DateTime datRet;
			int ilYY=0;
			int ilMo=0;
			int ilDD=0;
			int ilHH=0;
			int ilMM=0;
			int ilSS=0;
			
			datRet = DateTime.Now;
			try
			{
				if (strDate.Trim() != "")
				{
					if (strDate.Length > 8)
					{
						ilYY = Convert.ToInt32(strDate.Substring(0,4));
						ilMo = Convert.ToInt32(strDate.Substring(4,2));
						ilDD = Convert.ToInt32(strDate.Substring(6,2));
						ilHH = Convert.ToInt32(strDate.Substring(8,2));
						ilMM = Convert.ToInt32(strDate.Substring(10,2));
						ilSS = Convert.ToInt32(strDate.Substring(12,2));
						datRet = new DateTime(ilYY, ilMo, ilDD, ilHH, ilMM,ilSS );
					}
					else
					{
						if(strDate.IndexOf(strDate, 0) > -1)
						{
							ilHH = Convert.ToInt32(GetItem(strDate, 0, ":"));
							ilMM = Convert.ToInt32(GetItem(strDate, 1, ":"));
							ilSS = Convert.ToInt32(GetItem(strDate, 2, ":"));
							datRet = new DateTime(1900, 1, 1, ilHH, ilMM, ilSS ); //let's assum the 01.01.1900
						}
						else
						{
							datRet = new DateTime(1900,1,1, Convert.ToInt32(strDate.Substring(0,2)),
								Convert.ToInt32(strDate.Substring(2,2)), 0);
						}
					}
				}
			}
			catch(System.Exception err)
			{
				AddException("UT", "CedaTimeToDateTime", err.Message.ToString(), "strDate: " + strDate, "");
			}
			return datRet;
		}//END CedaTimeToDateTime(String strDate)

		/// <summary>
		/// Add an Excetion description to the internal Exception Array.
		/// This will be used to display the exceptions in the frmExection form.
		/// Further more it fires a delegate, that a new exception has occured.
		/// </summary>
		/// <param name="strModule">The modul name, which produced the exception.</param>
		/// <param name="strMethod">The method, property or event, which produced the exception.</param>
		/// <param name="strDescription">The exception description.</param>
		/// <param name="strParameters">The parameters of the call in a string.</param>
		/// <param name="strCallstackInfo">The call stack information.</param>
		/// <returns>The total of current exceptions in the arrException</returns>
		/// <remarks>The internal collected exception can be visualized by
		/// calling the <see cref="frmExceptions"/> form.</remarks>
		public static int AddException(String strModule, String strMethod, String strDescription, 
			String strParameters, String strCallstackInfo)
		{
			String str; 
			char c = (char)15;

			String sepa = c.ToString();
			str = strModule + sepa + strMethod + sepa + strDescription + sepa + strParameters + sepa + strCallstackInfo;
			arrExceptions.Add(str);
			//ExceptionOccured ex = new ExceptionOccured(null, arrExceptions.Count);

			if (OnExceptionOccured != null)
			{
				OnExceptionOccured(arrExceptions.Count);
			}

			return arrExceptions.Count;
		}//END AddException(String strException)

		/// <summary>
		/// Converts a time in local format to UTC format.
		/// </summary>
		/// <param name="localTime">The local time.</param>
		/// <returns>The converted local time as UTC time.</returns>
		/// <remarks>none.</remarks>
		public static DateTime LocalToUtc(DateTime localTime)
		{
//readTimeConsideration
//ChangeTimeTimesUTCLocal
			DateTime datRet = localTime;
			if(readTimeConsideration == false)
			{
				string strUTCConvertion = "";
				IniFile myIni = new IniFile("C:\\Ufis\\system\\ceda.ini");
				strUTCConvertion = myIni.IniReadValue("GLOBAL", "UTCCONVERTIONS");
				if(strUTCConvertion == "NO")
				{
					ChangeTimeTimesUTCLocal = false;
				}
				readTimeConsideration = true;
			}
			if(ChangeTimeTimesUTCLocal == true)
			{
				datRet = TimeZone.CurrentTimeZone.ToUniversalTime(localTime);
			}
			return datRet;
		}

		/// <summary>
		/// Converts a time in UTC format to local format.
		/// </summary>
		/// <param name="utcTime">The UTC time to be converted.</param>
		/// <returns>The converted UTC time as local time.</returns>
		/// <remarks>none.</remarks>
		public static DateTime UtcToLocal(DateTime utcTime)
		{
			DateTime datRet = utcTime;
			if(readTimeConsideration == false)
			{
				string strUTCConvertion = "";
				IniFile myIni = new IniFile("C:\\Ufis\\system\\ceda.ini");
				strUTCConvertion = myIni.IniReadValue("GLOBAL", "UTCCONVERTIONS");
				if(strUTCConvertion == "NO")
				{
					ChangeTimeTimesUTCLocal = false;
				}
				readTimeConsideration = true;
			}
			if(ChangeTimeTimesUTCLocal == true)
			{
				return TimeZone.CurrentTimeZone.ToLocalTime(utcTime);
			}
			return datRet;
		}


		private static string clientChars = "\x22\x27,\x0A\x0D\x3A\x7B\x7D\x5B\x5D\x7C\x5E\x3F\x28\x29\x3B\x40";
		private static string serverChars = "\xB0\xB1�\xB4\xB3\xBF\x93\x94\x95\x96\x97\x98\x99\x9B\x9C\x9D\x9E";

		/// <summary>
		/// Converts a string in client format to ceda format
		/// </summary>
		/// <param name="clientString">The string, which was produced by the
		/// application, e.g. user entry in a textbox.</param>
		/// <returns>The cleaned string so that CEDA can use it.</returns>
		/// <remarks>Converts e.g. "," to "�" and many more characters.
		/// <c><P></P>
		/// clientChars are:<P></P>
		/// "\x22\x27,\x0A\x0D\x3A\x7B\x7D\x5B\x5D\x7C\x5E\x3F\x28\x29\x3B\x40"<P></P>
		/// serverChars are:<P></P>
		/// "\xB0\xB1�\xB4\xB3\xBF\x93\x94\x95\x96\x97\x98\x99\x9B\x9C\x9D\x9E"
		/// </c>
		/// </remarks>
		public static string MakeCedaString(string clientString)
		{
			string serverString = clientString;
			for (int i = 0; i < clientChars.Length; i++)
			{
				serverString = serverString.Replace(clientChars[i],serverChars[i]);
			}
			
			return serverString;
		}

		/// <summary>
		/// Converts a string in ceda format to client format
		/// </summary>
		/// <param name="cedaString">The CEDA string, which contains unreadable
		/// character to be converted into a readable format.</param>
		/// <returns>The cleaned string for the user.</returns>
		/// <remarks>Converts e.g. "�" to "," and many more characters.
		/// <c><P></P>
		/// clientChars are:<P></P>
		/// "\x22\x27,\x0A\x0D\x3A\x7B\x7D\x5B\x5D\x7C\x5E\x3F\x28\x29\x3B\x40"<P></P>
		/// serverChars are:<P></P>
		/// "\xB0\xB1�\xB4\xB3\xBF\x93\x94\x95\x96\x97\x98\x99\x9B\x9C\x9D\x9E"
		/// </c>
		/// </remarks>
		public static string MakeClientString(string cedaString)
		{
			string clientString = cedaString;

			for (int i = 0; i < serverChars.Length; i++)
			{
				clientString = clientString.Replace(serverChars[i],clientChars[i]);
			}

			return clientString;

		}
		/// <summary>
		/// This method extracts the value of a key item from the string <B>pcpTextBuff</B>.
		/// You can consider this method to be a poor XML function :-)
		/// </summary>
		/// <param name="pcpTextBuff">The string, which contains some key items
		/// in a special syntax.</param>
		/// <param name="pcpKeyWord">The Key word to be searched for.</param>
		/// <param name="pcpItemEnd">The end identifier.</param>
		/// <returns>A string containing the value of the key item or empty.</returns>
		/// <example>
		/// The following example demonstrate the usage of <B>GetKeyItem</B>:
		/// <code>
		/// [C#]
		/// using Ufis.Utils;
		/// 
		/// class myClass
		/// {
		///		string myName	= "";
		///		string myStreet	= "";
		///		string myCity   = "";
		///		
		///		// Lets assume that the string "theAdressData" contains the following:
		///		// "&lt;NAME&gt;John Doe,&lt;STRET&gt;Elmstreet,&lt;CITY&gt;Denver"
		///		public myClass(string theAdressData)
		///		{
		///			myName	 = UT.GetKeyItem(theAdressData, "&lt;NAME&gt;", "&gt;")
		///			myStreet = UT.GetKeyItem(theAdressData, "&lt;STREET&gt;", "&gt;")
		///			myCity	 = UT.GetKeyItem(theAdressData, "&lt;CITY&gt;", "&gt;")
		///			// the result is myName="John Doe", myStreet="Elmstreet", myCity="Denver"
		///		}
		/// }
		/// </code>
		/// </example>
		/// <remarks>none.</remarks>
		public static string GetKeyItem(string pcpTextBuff,string pcpKeyWord,string pcpItemEnd) 
		{ 
			int	 pclDataBegin = -1; 
			int	 pclDataEnd = -1; 
			pclDataBegin = pcpTextBuff.IndexOf(pcpKeyWord); 
			/* Search the keyword */ 
			if (pclDataBegin >= 0) 
			{ 
				/* Did we find it? Yes. */ 
				pclDataBegin += pcpKeyWord.Length; 
				/* Skip behind the keyword */ 
				pclDataEnd = pcpTextBuff.IndexOf(pcpItemEnd,pclDataBegin); 
				/* Search end of data */ 
				if (pclDataEnd == -1) 
				{ 
					/* End not found? */ 
					return pcpTextBuff.Substring(pclDataBegin); 
					/* Take the whole string */ 
				} /* end if */ 
				else
				{
					int llDataSize = pclDataEnd - pclDataBegin; 
					return pcpTextBuff.Substring(pclDataBegin,llDataSize);
				}
			}
			else
			{
				return string.Empty;
			}
		}

		/// <summary>
		/// Use this method if the target of a delegate or event handler requires an invocation
		/// </summary>
		/// <param name="delegates">An array of delegates.</param>
		/// <param name="args">An oject array of arguments.</param>
		/// <remarks>This method is the central method to ensure
		/// syncronized invokation of delegates. 
		/// <P><B>NOTE:</B></P>
		/// This is important for Multi Threaded environments.
		/// <see cref="MainThreadBroadcastHandler"/>.
		/// </remarks>
		static public void Invoke(Delegate[] delegates,Object[] args)
		{
			for (int i = 0; i < delegates.Length; i++)
			{
				// target object derived from Control class ?
				if (delegates[i].Target is ISynchronizeInvoke)
				{
					ISynchronizeInvoke isv = (ISynchronizeInvoke)delegates[i].Target;
					if (isv.InvokeRequired)
					{
						isv.Invoke(delegates[i],args);
					}
					else
					{
						isv.Invoke(delegates[i],args);
					}
				}
				else	// not derived from Control class
				{
					System.Diagnostics.Debug.WriteLine("Target object of delegate is not derived from ISynchronizeInvoke");
				}
			}				

		}

		#endregion
		#region Overlapping Condition Checks
		/// <summary>
		/// Detects overlapping when start of time frame 1 and frame 2 are not equal.
		/// </summary>
		/// <param name="start1">Start of time frame 1</param>
		/// <param name="end1">End of time frame 1</param>
		/// <param name="start2">Start of time frame 2</param>
		/// <param name="end2">End of time frame 2</param>
		/// <returns>True = is overlapped, false not</returns>
		public static bool IsReallyOverlapped(DateTime start1, DateTime end1, DateTime start2, DateTime end2)
		{
			if((start1 < end2) && (start2 < end1)) return true;
			else return false;
		}
		/// <summary>
		/// Detects whether time frame 1 is within time frame 2.
		/// </summary>
		/// <param name="start1">Start of time frame 1</param>
		/// <param name="end1">End of time frame 1</param>
		/// <param name="start2">Start of time frame 2</param>
		/// <param name="end2">End of time frame 2</param>
		/// <returns>True = is within, false = outside</returns>
		public static bool IsWithIn(DateTime start1, DateTime end1, DateTime start2, DateTime end2)	
		{
			if((start1 >= start2) && (end1 <= end2)) return true;
			else return false;
		}
		/// <summary>
		/// Detects whether a time value is between a time frame.
		/// </summary>
		/// <param name="val">Time value to be checked.</param>
		/// <param name="start">Start of time frame.</param>
		/// <param name="end">End of time frame.</param>
		/// <returns>True = is between, false is not.</returns>
		public static bool IsBetween(DateTime val, DateTime start, DateTime end) 
		{
			if(start <= val && val <= end) return true;
			else return false;
		}
		/// <summary>
		/// Detects whether a time value is between an integer range.
		/// </summary>
		/// <param name="val">Time value to be checked.</param>
		/// <param name="start">Start of integer range.</param>
		/// <param name="end">End of integer range.</param>
		/// <returns>True = is between, false is not.</returns>
		public static bool IsBetween(int val, int start, int end) 
		{
			if(start <= val && val <= end) return true;
			else return false;
		}
		/// <summary>
		/// Checks if time frame 1 and time frame 2 are overlapping
		/// </summary>
		/// <param name="start1">Start of time frame 1.</param>
		/// <param name="end1">End of time frame 1.</param>
		/// <param name="start2">Start of time frame 2.</param>
		/// <param name="end2">End of time frame 2.</param>
		/// <returns>True = is overlapped, false is not</returns>
		public static bool IsOverlapped(DateTime start1, DateTime end1, DateTime start2, DateTime end2) 
		{
			if((start1 <= end2) && (start2 <= end1)) return true;
			else return false;
		}
		#endregion Overlapping Condition Checks
	}// End Class
//#if null	
	/// <summary>
	/// The namespace Ufis.Utils provides helper classes and static methods for common
	/// development purposes. The class UT contains colour definitions to be used directly
	/// in TAB.ocx, because the .NET predefined colours cannot be used directly. Some methods
	/// provide the converting routines for ceda date/time format to .NET DateTime and vice versa.
	/// Some string and item functions are implemented to handle string requirements. Moreover
	/// UT keeps the one and only instance of IDatabase and the developer can get it by calling
	/// <see cref="UT.GetMemDB()"/>. Some helpful methods to simply show the search dialog and return
	/// the search criteria, <see cref="UT.AddException(String, String, String, String, String)"/> method to add application exception, 
	/// which can be displayed by calling a <see cref="UT.dlgException"/> instance for debugging 
	/// purposes. A <see cref="UT.DrawGradient"/> method can be used to draw a gradient for any 
	/// control. The <see cref="BCBlinker"/> control can be used to animate the broadcast processing.
	/// The <see cref="frmAbout"/> provides an about box with version, build and application
	/// information.
	/// </summary>
	public class NamespaceDoc
	{
		/// <summary>
		/// This class is necessary for the Namespace summery, which is
		/// generated from a class named "NamespaceDoc".
		/// </summary>
		public NamespaceDoc()
		{
		}
	}
//#endif
}
