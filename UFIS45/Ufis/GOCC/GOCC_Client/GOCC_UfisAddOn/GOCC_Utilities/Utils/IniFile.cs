using System;
using System.Runtime.InteropServices;
using System.Text;


namespace Ufis.Utils
{
	/// <summary>
	/// Due to the fact that .NET does not provide any functionality to
	/// read the old fashioned ini files this class helps to do this. 
	/// </summary>
	/// <remarks>none</remarks>
	/// <example>
	/// The following sample demonstrate how to create an instance and
	/// read ini file entries. This code can be used in the _Load method
	/// of the application's main window.
	/// <code>
	/// Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\ceda.ini");
	/// string strServer = myIni.IniReadValue("GLOBAL", "HOSTNAME");
	/// string strHopo = myIni.IniReadValue("GLOBAL", "HOMEAIRPORT");
	/// UT.ServerName = strServer;
	/// UT.Hopo = strHopo;
	/// </code>
	/// </example>
	public class IniFile
	{
		/// <summary>
		/// The path to the ini file including the file name.
		/// </summary>
		/// <remarks>none</remarks>
		public string path;

		[DllImport("kernel32")]
		private static extern long WritePrivateProfileString(string section,
			string key,string val,string filePath);
		[DllImport("kernel32")]
		private static extern int GetPrivateProfileString(string section,
			string key,string def, StringBuilder retVal,
			int size,string filePath);

		internal string	CheckFilePath(string ropFilePath)
		{
			string olFilePath = ropFilePath;
			olFilePath = olFilePath.ToUpper();
			olFilePath = olFilePath.Replace('/','\\');

			int ind = olFilePath.LastIndexOf('\\');
			if (ind < 0)	// nothing to do
				return olFilePath;

			if (olFilePath.Substring(0,ind).CompareTo("C:\\UFIS\\SYSTEM") != 0)
				return olFilePath;

			string olConfigFileName = Environment.GetEnvironmentVariable("CEDA");
			if (olConfigFileName == null)
				return olFilePath;

			olConfigFileName = olConfigFileName.ToUpper();
			olConfigFileName = olConfigFileName.Replace('/','\\');

			int ind2 = olConfigFileName.LastIndexOf('\\');
			if (ind2 < 0)	// nothing to do
				return olFilePath;

			olFilePath = olConfigFileName.Substring(0,ind2) + olFilePath.Substring(ind);

			return olFilePath;
		}

		/// <summary>
		/// INIFile Constructor.
		/// </summary>
		/// <param name="INIPath">The path including the file name of the ini file.</param>
		/// <remarks>none</remarks>
		public IniFile(string INIPath)
		{
			path = this.CheckFilePath(INIPath);
		}
		/// <summary>
		/// Write Data to the INI File
		/// </summary>
		/// <param name="Section">Section name in the ini file.</param>
		/// <param name="Key">Key Name within the section.</param>
		/// <param name="Value">Value Name</param>
		/// <remarks>none</remarks>
		public void IniWriteValue(string Section,string Key,string Value)
		{
			WritePrivateProfileString(Section,Key,Value,this.path);
		}
        
		/// <summary>
		/// Read Data Value From the Ini File
		/// </summary>
		/// <param name="Section">Section name within the ini file.</param>
		/// <param name="Key">Key name within the section.</param>
		/// <returns>The value for the Key.</returns>
		/// <remarks>none</remarks>
		public string IniReadValue(string Section,string Key)
		{
			StringBuilder temp = new StringBuilder(255);
			int i = GetPrivateProfileString(Section,Key,"",temp, 
				255, this.path);
			return temp.ToString();

		}

        /// <summary>
        /// Get Server Host Name
        /// e.g green
        /// </summary>
        /// <returns>Server Host Name (e.g. green)</returns>
        public string GetServerHostName()
        {
            return IniReadValue("GLOBAL", "HOSTNAME");
        }

        /// <summary>
        /// Get Home Airport Name
        /// e.g SIN - Singapore Changi airport
        /// </summary>
        /// <returns>Home Airport Name (e.g. SIN)</returns>
        public string GetHomeAriportName()
        {
            return IniReadValue("GLOBAL", "HOMEAIRPORT");
        }
	}
}
