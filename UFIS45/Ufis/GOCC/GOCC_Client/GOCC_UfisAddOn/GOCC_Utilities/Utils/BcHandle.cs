using System;
using System.Runtime.InteropServices;
using System.Text;
using System.IO;
using System.Collections;

namespace Ufis.Data
{
	/// <summary>
	/// Summary description for BcHandle.
	/// </summary>
	public class BcHandle
	{
		enum ErrorCodes
		{
			RC_FAIL			= -1,
			RC_SUCCESS		= 0,
			RC_EMPTY_MSG	= 1,
			RC_SHUTDOWN     =-5,
			RC_COMM_FAIL	=-2,
			RC_INIT_FAIL	=-3,
			RC_CEDA_FAIL	=-4,
			RC_ALREADY_INIT	=-6,
			RC_NOT_FOUND	=-7,
			RC_DATA_CUT		=-8,
			RC_TIMEOUT		=-9,
			RC_ALREADY_CONNECTED = -10,
			RC_MISSING_ANSWER	 = -11,
			RC_BUSY				 = -31,
			RC_TO_MANY_APP		 = -32,
		}

		private const int BC_HEAD_dest_name_offset	= 0;	
		private const int BC_HEAD_orig_name_offset	= 10;	
		private const int BC_HEAD_recv_name_offset	= 20;	
		private const int BC_HEAD_bc_num_offset		= 30;	
		private const int BC_HEAD_seq_id_offset		= 32;	
		private const int BC_HEAD_tot_buf_offset	= 42;	
		private const int BC_HEAD_act_buf_offset	= 44;	
		private const int BC_HEAD_req_seq_id_offset = 46;	
		private const int BC_HEAD_rc_offset			= 56;	
		private const int BC_HEAD_tot_size_offset	= 58;	
		private const int BC_HEAD_cmd_size_offset	= 60;	
		private const int BC_HEAD_data_size_offset	= 62;	
		private const int BC_HEAD_data_offset		= 64;	

		private const int BC_HEAD_size = 66;	

    
		private const int CMDBLK_command_offset = 0;	
		private const int CMDBLK_obj_name_offset= 6;	
		private const int CMDBLK_order_offset	= 39;	
		private const int CMDBLK_tw_start_offset= 41;	
		private const int CMDBLK_tw_end_offset	= 74;	
		private const int CMDBLK_data_offset	= 107;	

		private const int CMDBLK_size = 108;	

		private const int COMMIF_command_offset = 0;	
		private const int COMMIF_length_offset	= 4;	
		private const int COMMIF_data_offset	= 8;	

		private const int COMMIF_size = 12;	

		private const int DATAPACKETLEN = 1024-12;
		private const int NETREAD_SIZE  = 0x8000;
		private const int PACKET_LEN    = 1024;
		private const int NET_SHUTDOWN	= 0x1;
		private const int NET_DATA		= 0x2;
		private const int PACKET_DATA	= 65436;	// i.e. -100
		private const int PACKET_START	= 65336;	// i.e. -200
		private const int PACKET_END	= 65236;	// i.e. -300

		
		private const short SELECTION	= 0;
		private const short FIELDS		= 1;
		private const short DATA		= 2;

		private const string DEFAULT_BC_FN	= "C:\\TMP\\BCDATA.DAT";   
		private const int	 FIRSTBC		= 20000;     
		private const int	 MAXBCNUM		= 30000;

		public struct REC_BC
		{
			public byte[]	BcHead;
			public byte[]	CmdBlk;
			public byte[]	DataBuffer;
			public int		DataBufferSize;
		};

		public struct BCDATA
		{
			public string ReqId;
			public string Dest1;
			public string Dest2;
			public string Cmd;
			public string Object;
			public string Seq;
			public string Tws;
			public string Twe;
			public string Selection;
			public string Fields;
			public string Data;
			public short  BcNum;
		};

		private REC_BC	rmBcData;

		private static  int imLastReturnCode;
		private	string	pcmReqId;
		private	string	pcmUser;

		//@ManMemo: are we connected to server now?
		private	static bool bmIsConnected;
		//@ManMemo: Name of the actual host
		private	string	pcmHostName;
		private string	pcmRealHostName;

		//@ManMemo: Type of the actual host
		private	string	pcmHostType;
		private string	pcmRealHostType;
		//@ManMemo: full path name of the configuration file
		private string	pcmConfigPath;
		//@ManMemo: return code of last ceda action

		// Internal attributes
		private static int imCount;             // number of instances created
		private string		pcmAppName;

		private	string		pcmTableExt;
		private	string		pcmHomeAirport;

		private	string		pcmVersion = "4.5.0.1";
		private	string		pcmInternalBuild = "";

		// CCSBcHandle members
		private	static short	llNextBcNum = 0;
		private static string	clHopos = "";
		private	static bool		blUseHopos = false;
		private static bool		bgExtendLog = false;
		private static short	sgMaxBcNum = MAXBCNUM;
		private	static StreamWriter	rgOutFile = null;

		private	string	pcmBcLogfile;
		private FileStream	prmBcFile = null;
		// CCSBcHandle members end

		[DllImport("Ufis32.dll", CharSet=CharSet.Ansi)]
		public static extern int InitCom(string HostType, string CedaHost);
		[DllImport("Ufis32.dll", CharSet=CharSet.Ansi)]
		public static extern int CallCeda(string req_id,string dest1,string dest2,string cmd,string obj,string seq,string tws,string twe,string selection,string fields,StringBuilder data,string data_dest);
		[DllImport("Ufis32.dll", CharSet=CharSet.Ansi)]
		public static extern int CleanupCom();

		[DllImport("Ufis32.dll", CharSet=CharSet.Ansi)]
		public static extern int UfisDllAdmin(string parm1,string parm2,string parm3);

		[DllImport("Ufis32.dll", CharSet=CharSet.Ansi)]
		public static extern int GetBufferHandle(string buf_slot, ref int pHandle);

		// GetResultBuffer(..) is here for temporary using and testing purpose (because it is
		// very SLOW!! -- according to Manfred). It should be removed when the actual buffer
		// accessing routine in CEDADATA.CPP is completed.
		//
		[DllImport("Ufis32.dll", CharSet=CharSet.Ansi)]
		public static extern int GetResultBuffer(StringBuilder pResult, int buflen, string buf_slot,int line_no, int line_cnt,StringBuilder item_list);

		[DllImport("Ufis32.dll", CharSet=CharSet.Ansi)]
		public static extern int GetBc (StringBuilder req_id,StringBuilder dest1,StringBuilder dest2, StringBuilder cmd, StringBuilder obj,StringBuilder seq, StringBuilder tws, StringBuilder twe, StringBuilder selection, StringBuilder fields,StringBuilder data, StringBuilder bc_num) ;

		[DllImport("Ufis32.dll", CharSet=CharSet.Ansi)]
		public static extern int RegisterBcWindow(int hlBcWindow);

		[DllImport("Ufis32.dll", CharSet=CharSet.Ansi)]
		public static extern int UnRegisterBcWindow(int hlBcWindow);

		[DllImport("Ufis32.dll", CharSet=CharSet.Ansi)]
		public static extern int GetWorkstationName (StringBuilder ws_name);

		[DllImport("Ufis32.dll", CharSet=CharSet.Ansi)]
		public static extern int GetNumberOfLines (StringBuilder buf_slot);

		[DllImport("Ufis32.dll", CharSet=CharSet.Ansi)]
		public static extern int GetActualBcNum ();

		[DllImport("Ufis32.dll", CharSet=CharSet.Ansi)]
		public static extern int GetFirstBcNum ();

		[DllImport("Ufis32.dll", CharSet=CharSet.Ansi)]
		public static extern int GetLostBcCount();

		[DllImport("Ufis32.dll", CharSet=CharSet.Ansi)]
		public static extern int GetRealWorkstationName (StringBuilder ws_name);
		
		[DllImport("kernel32")]
		private static extern long WritePrivateProfileString(string section,string key,string val,string filePath);
		[DllImport("kernel32")]
		private static extern int GetPrivateProfileString(string section,string key,string def, StringBuilder retVal,int size,string filePath);

		public BcHandle(string pcpAppName)
		{
			rmBcData.BcHead = new byte[BC_HEAD_size];
			rmBcData.CmdBlk = new byte[CMDBLK_size];
			rmBcData.DataBuffer = new byte[12*(PACKET_LEN+10)];
			rmBcData.DataBufferSize = (12*(PACKET_LEN+10));

			if (pcpAppName != null)
				this.pcmAppName = pcpAppName;
			imCount++;  // count how many "CedaCom" instances
		}

		public BcHandle(string pcpHostName, string pcpHostType,string pcpAppName)
		{
			rmBcData.BcHead = new byte[BC_HEAD_size];
			rmBcData.CmdBlk = new byte[CMDBLK_size];
			rmBcData.DataBuffer = new byte[12*(PACKET_LEN+10)];
			rmBcData.DataBufferSize = (12*(PACKET_LEN+10));

			if (pcpAppName != null)
				this.pcmAppName = pcpAppName;
			imCount++;  // count how many "CedaCom" instances
 
			// establish connection automatically for the first time
			if (bmIsConnected == false)
				Initialize(pcpHostName, pcpHostType);  // use default host in configuration file
		}

		public void SetAppName(string appName)
		{
			this.pcmAppName = appName;
		}

		public void SetUserName(string userName)
		{
			this.pcmUser = userName; 
		}

		public void SetHomeAirport(string hopo)
		{
			this.pcmHomeAirport = hopo; 
		}

		public void SetTableExtension(string tabExt)
		{
			this.pcmTableExt = tabExt; 
		}

		public bool CleanUpCom()
		{                    
			imCount--;

			// close the connection if any
			if (bmIsConnected == true)
			{
				 bmIsConnected = false;
				 CleanupCom();
			}
			return true;
		}

		public bool Initialize(string pcpHostName,string pcpHostType)
		{
			// retrieve the configuration path from OS environment
			pcmConfigPath = "C:\\UFIS\\SYSTEM\\CEDA.INI"; 
    
			string pclDllTmpLogName;
			string pclDllLogMode;

			pclDllTmpLogName = "C:\\TMP\\" + pcmAppName + "Dll.Log";
		    
			StringBuilder tmp = new StringBuilder(512);
			GetPrivateProfileString(pcmAppName, "DLL-LOGMODE", "TRACE",	tmp, 512, pcmConfigPath);
			pclDllLogMode = tmp.ToString();

			UfisDllAdmin("TRACEFN",pclDllTmpLogName,"");
			UfisDllAdmin("TRACE","ON",pclDllTmpLogName);

			// close any connection to CEDA server if exist
			if (bmIsConnected == true)
			{
				bmIsConnected = false;
				CleanupCom();
			}

			// check if we need to read host name and type from "CEDA.INI"
			// warning: both host name and type will be truncated to 12 characters.
			//
			if (pcpHostName == null)
			{
				GetPrivateProfileString(pcmAppName, "HOSTNAME", "",tmp, 512, pcmConfigPath);
				pcmHostName = tmp.ToString();
			}
			else
			{
				pcmHostName = pcpHostName;
			}

			if (pcpHostType == null)
			{
				GetPrivateProfileString(pcmAppName, "HOSTTYPE", "",tmp, 512, pcmConfigPath);
				pcmHostType = tmp.ToString();
			}
			else
			{
				pcmHostType = pcpHostType;
			}

		    
			pcmRealHostType = pcmHostType;
			pcmRealHostName = pcmHostName;

			imLastReturnCode = InitCom(pcmRealHostType, pcmRealHostName);

			// initialize communication with CEDA
			if (imLastReturnCode != 0)
			{
				return false;
			}

			GetWorkstationName(tmp);
			pcmReqId = tmp.ToString();

			imLastReturnCode = 0;
			bmIsConnected = true;

			UfisDllAdmin("TRACE", "ON", "FATAL");

			return true;
		}

		public bool RegisterBcForm(System.Windows.Forms.UserControl myForm)
		{
			return RegisterBcWindow((int)myForm.Handle) == 0;
		}

		public bool UnRegisterBcForm(System.Windows.Forms.UserControl myForm)
		{
			return UnRegisterBcWindow((int)myForm.Handle) == 0;
		}

		public string LastError()
		{
			string olRet;

			switch (imLastReturnCode)
			{
				case 1:
					// will actually never happen coz only used within the dll
					olRet = "RC_EMPTY_MSG: DLL returned empty content";
					break;
				case 0:
					olRet = "RC_SUCCESS: Everything OK";
					break;
				case -1:
					olRet = "RC_FAIL: General serious error";
					break;
				case -2:
					olRet = "RC_COMM_FAIL: WINSOCK.DLL error";
					break;
				case -3:
					olRet = "RC_INIT_FAIL: Open the log file";
					break;
				case -4:
					olRet = "RC_CEDA_FAIL: CEDA reports an error";
					break;
				case -5:
					olRet = "RC_SHUTDOWN: CEDA reports shutdown";
					break;
				case -6:
					olRet = "RC_ALREADY_INIT: InitComm called up for a second time";
					break;
				case -7:
					olRet = "RC_NOT_FOUND: Data not found (More than 7 applications are running)";
					break;
				case -8:
					olRet = "RC_DATA_CUT: Data incomplete";
					break;
				case -9:
					olRet = "RC_TIMEOUT: CEDA reports a time-out";
					break;
				case -10:
					olRet = "RC_ALREADY_CONNECTED: InitComm has already been called";
					break;
				case -11:
					olRet = "RC_MISSING_ANSWER: No reply";
					break;
				case -31:
					olRet = "RC_BUSY: UFIS.DLL is busy";
					break;
				case -32:
					olRet = "RC_TO_MANY_APP: Too many Applications started (Kill BcServ32.exe)";
					break;
				default:
					olRet = "Unknown error"; 
					break;
			}

			return olRet;
		}

		private bool ReReadBc()
		{
			bool blRc = true;

			string req_id	= this.pcmReqId;
			string dest1	= this.pcmUser;			// Username
			string dest2	= req_id;
			string cmd		= "RBS";		
			string obj		= "";	
			string seq		= "";				// sort
			string tws		= "0";			// base id
			string twe		= this.pcmHomeAirport + "," + this.pcmTableExt + "," + this.pcmAppName + "," + this.pcmVersion + "," + this.pcmInternalBuild;
			string selection= "";
			string fields	= "";
			StringBuilder data	= new StringBuilder(2048);
			string data_dest= "BUF1";

			int rc = CallCeda(req_id,dest1,dest2,cmd,obj,seq,tws,twe,selection,fields,data,data_dest);

			return blRc;
		}

		public bool GetBc(int ipBcNum,ArrayList ropBcs)
		{
			short	slActualBcNum = 0; 
			short	slActualBcNumAfterWrap = 0; 
			bool	blBcNumHasWrapped = false; 
			byte[]	pclPacketBuf = new byte[PACKET_LEN+4];
			BCDATA rlBcData = new BCDATA();

			if (bgExtendLog && pcmBcLogfile.Length > 0)
			{

				rgOutFile = new StreamWriter(pcmBcLogfile,true);
				if (rgOutFile != null)
				{
					rgOutFile.WriteLine("***********************************************************************************"); 
					string msg = string.Format("GetBc started, ipBcNum: {0} llNextBcNum: {1}",ipBcNum,llNextBcNum);
					rgOutFile.WriteLine(msg);
					rgOutFile.Close();
					rgOutFile = null;
				}
			}

			if (ipBcNum < 1)
			{
				return false;
			}


			if (llNextBcNum == 0)
			{
				string pclConfigPath;
				string pclBcExtendLog;
				StringBuilder pclTmpText = new StringBuilder(512);

				pclConfigPath = "C:\\UFIS\\SYSTEM\\CEDA.INI"; 

				GetPrivateProfileString(pcmAppName, "BC-LOGFILE", "",pclTmpText,512, pclConfigPath);
				pcmBcLogfile = pclTmpText.ToString();
				pcmBcLogfile = pcmBcLogfile.TrimEnd();

				GetPrivateProfileString(pcmAppName, "BC-EXTLOG", "NO",pclTmpText,512, pclConfigPath);
				pclBcExtendLog = pclTmpText.ToString();
				pclBcExtendLog = pclBcExtendLog.TrimEnd();
				if (pclBcExtendLog == "YES")
				{
					bgExtendLog = true;
				}

				GetPrivateProfileString("GLOBAL", "MAXBCNUM", "30000",pclTmpText,512, pclConfigPath);
				sgMaxBcNum = short.Parse(pclTmpText.ToString());

				GetPrivateProfileString("GLOBAL", "HOPOS", "",pclTmpText,512, pclConfigPath);
				if (pclTmpText.ToString().Length > 0)
				{
					blUseHopos = true;
				}
			}

			if (prmBcFile == null)
			{
				prmBcFile = new FileStream(DEFAULT_BC_FN,FileMode.Open,FileAccess.Read,FileShare.ReadWrite);
			}

			if (prmBcFile == null)
			{
				return false;
			}

			slActualBcNum = (short) ipBcNum; //::GetActualBcNum(); 
	
			if (llNextBcNum == 0) 
			{
				llNextBcNum = (short) ipBcNum;  //(short) ::GetFirstBcNum(); 
			}

			if (slActualBcNum < (llNextBcNum-200))  // MCU saudumme loesung (auf deutsch: Quick Hack )
			{
				if (bgExtendLog && pcmBcLogfile.Length > 0)
				{

					rgOutFile = new StreamWriter(pcmBcLogfile,true);
					if (rgOutFile != null)
					{
						rgOutFile.WriteLine(string.Format("BcNumHasWrapped, slActualBcNum: {0}  (llNextBcNum-1): {1}",slActualBcNum,(llNextBcNum-1)));
						rgOutFile.Close();
						rgOutFile = null;
					}
				}

				blBcNumHasWrapped = true;
				slActualBcNumAfterWrap = slActualBcNum;
				slActualBcNum = sgMaxBcNum;
			}

			bool blBcOK = slActualBcNum >= llNextBcNum;

			while(blBcOK)
			{
				prmBcFile.Seek(FIRSTBC+(llNextBcNum*PACKET_LEN),SeekOrigin.Begin);
				prmBcFile.Read(pclPacketBuf,0,PACKET_LEN);

				byte[] olCmd = new byte[2];
				olCmd[0] = pclPacketBuf[COMMIF_command_offset+1];
				olCmd[1] = pclPacketBuf[COMMIF_command_offset];
				short ilCmd = BitConverter.ToInt16(olCmd,0);

				if(ilCmd == 0)
				{
					// we lost at least one BC
					ReReadBc();
					if(pcmBcLogfile.Length > 0)
					{
						rgOutFile = new StreamWriter(pcmBcLogfile,true);
						if (rgOutFile != null)
						{
							rgOutFile.WriteLine(string.Format("Broadcast lost: {0}",llNextBcNum));
							rgOutFile.Close();
							rgOutFile = null;
						}
					}
					return false;
				}

				if(pcmBcLogfile.Length > 0)
				{
					rgOutFile = new StreamWriter(pcmBcLogfile,true);
					if (rgOutFile != null)
					{
						rgOutFile.WriteLine(string.Format("BcOK, we've got llNextBcNum: {0}",llNextBcNum));
						rgOutFile.Close();
						rgOutFile = null;
					}
				}

				byte[] olActBuf = new byte[2];
				olActBuf[1] = pclPacketBuf[COMMIF_data_offset+BC_HEAD_act_buf_offset+1];
				olActBuf[0] = pclPacketBuf[COMMIF_data_offset+BC_HEAD_act_buf_offset];
				short ilActBuf = BitConverter.ToInt16(olActBuf,0);

				if (ilActBuf == 1)
				{
					Array.Copy(pclPacketBuf,COMMIF_data_offset,rmBcData.BcHead,0,BC_HEAD_size);
					Array.Copy(pclPacketBuf,COMMIF_data_offset+BC_HEAD_data_offset,rmBcData.CmdBlk,0,CMDBLK_size);

				}

				byte[] olTotSize = new byte[2];
				olTotSize[1] = pclPacketBuf[COMMIF_data_offset+BC_HEAD_tot_size_offset+1];
				olTotSize[0] = pclPacketBuf[COMMIF_data_offset+BC_HEAD_tot_size_offset];
				short ilTotSize = BitConverter.ToInt16(olTotSize,0);

				if (rmBcData.DataBufferSize < ilTotSize)
				{
					byte[] oldBuffer = rmBcData.DataBuffer;
					rmBcData.DataBuffer = new byte[ilTotSize+40];
					Array.Copy(oldBuffer,0,rmBcData.DataBuffer,0,rmBcData.DataBufferSize);
					rmBcData.DataBufferSize = ilTotSize;
				} 

				byte[] olCmdSize = new byte[2];
				olCmdSize[1] = rmBcData.BcHead[BC_HEAD_cmd_size_offset+1];
				olCmdSize[0] = rmBcData.BcHead[BC_HEAD_cmd_size_offset];
				short ilCmdSize = BitConverter.ToInt16(olCmdSize,0);

				int ilOffset = (ilActBuf - 1) * ilCmdSize;

				olCmdSize[1] = pclPacketBuf[COMMIF_data_offset+BC_HEAD_cmd_size_offset+1];
				olCmdSize[0] = pclPacketBuf[COMMIF_data_offset+BC_HEAD_cmd_size_offset];
				ilCmdSize = BitConverter.ToInt16(olCmdSize,0);

				if ((ilOffset + ilCmdSize) <= rmBcData.DataBufferSize)
				{
					Array.Copy(pclPacketBuf,COMMIF_data_offset+BC_HEAD_data_offset+CMDBLK_data_offset,rmBcData.DataBuffer,ilOffset,ilCmdSize);
					rmBcData.DataBuffer[ilOffset+ilCmdSize] = 0;
				}

				byte[] olBc_Num = new byte[2];
				olBc_Num[1] = pclPacketBuf[COMMIF_data_offset+BC_HEAD_bc_num_offset+1];
				olBc_Num[0] = pclPacketBuf[COMMIF_data_offset+BC_HEAD_bc_num_offset];
				short ilBc_Num = BitConverter.ToInt16(olBc_Num,0);

				llNextBcNum = (short) (ilBc_Num+1);  

				if (bgExtendLog && pcmBcLogfile.Length > 0)
				{

					rgOutFile = new StreamWriter(pcmBcLogfile,true);
					if (rgOutFile != null)
					{
						rgOutFile.WriteLine(string.Format("llNextBcNum = (short) (prlBcHead->bc_num+1): {0}",llNextBcNum));
						rgOutFile.Close();
						rgOutFile = null;
					}
				}
		
				if (llNextBcNum > sgMaxBcNum) 
				{
					llNextBcNum = 1;
					blBcNumHasWrapped = true;
					//			slActualBcNum = slActualBcNumAfterWrap;

					if (bgExtendLog && pcmBcLogfile.Length > 0)
					{
						rgOutFile = new StreamWriter(pcmBcLogfile,true);
						if (rgOutFile != null)
						{
							rgOutFile.WriteLine(string.Format("llNextBcNum = (short) (prlBcHead->bc_num+1): {0}  slActualBcNum {1}",llNextBcNum,slActualBcNum));
							rgOutFile.Close();
							rgOutFile = null;
						}
					}
				}

				byte[] olTotBuf = new byte[2];
				olTotBuf[1] = pclPacketBuf[COMMIF_data_offset+BC_HEAD_tot_buf_offset+1];
				olTotBuf[0] = pclPacketBuf[COMMIF_data_offset+BC_HEAD_tot_buf_offset];
				short ilTotBuf = BitConverter.ToInt16(olTotBuf,0);

				if (ilActBuf == ilTotBuf)
				{
//					if (prlPacket != null)
					{
						ASCIIEncoding olAE = new ASCIIEncoding();
						rlBcData.ReqId	= olAE.GetString(rmBcData.BcHead,BC_HEAD_recv_name_offset,strlen(rmBcData.BcHead,BC_HEAD_recv_name_offset));

						rlBcData.Dest1	= olAE.GetString(rmBcData.BcHead,BC_HEAD_dest_name_offset,strlen(rmBcData.BcHead,BC_HEAD_dest_name_offset));
						rlBcData.Dest2	= olAE.GetString(rmBcData.BcHead,BC_HEAD_recv_name_offset,strlen(rmBcData.BcHead,BC_HEAD_recv_name_offset));

						rlBcData.Cmd	= olAE.GetString(rmBcData.CmdBlk,CMDBLK_command_offset,strlen(rmBcData.CmdBlk,CMDBLK_command_offset));
						rlBcData.Object	= olAE.GetString(rmBcData.CmdBlk,CMDBLK_obj_name_offset,strlen(rmBcData.CmdBlk,CMDBLK_obj_name_offset));
						rlBcData.Seq	= olAE.GetString(rmBcData.CmdBlk,CMDBLK_order_offset,strlen(rmBcData.CmdBlk,CMDBLK_order_offset));
						rlBcData.Tws	= olAE.GetString(rmBcData.CmdBlk,CMDBLK_tw_start_offset,strlen(rmBcData.CmdBlk,CMDBLK_tw_start_offset));
						rlBcData.Twe	= olAE.GetString(rmBcData.CmdBlk,CMDBLK_tw_end_offset,strlen(rmBcData.CmdBlk,CMDBLK_tw_end_offset));

						rlBcData.Selection	= olAE.GetString(rmBcData.DataBuffer,0,strlen(rmBcData.DataBuffer,0));
						rlBcData.Fields		= olAE.GetString(rmBcData.DataBuffer,rlBcData.Selection.Length+1,strlen(rmBcData.DataBuffer,rlBcData.Selection.Length+1));
						rlBcData.Data		= olAE.GetString(rmBcData.DataBuffer,rlBcData.Selection.Length+1+rlBcData.Fields.Length+1,strlen(rmBcData.DataBuffer,rlBcData.Selection.Length+1+rlBcData.Fields.Length+1));

						byte[] olBcNum = new byte[2];
						olBcNum[1] = rmBcData.BcHead[BC_HEAD_bc_num_offset+1];
						olBcNum[0] = rmBcData.BcHead[BC_HEAD_bc_num_offset];
						rlBcData.BcNum = BitConverter.ToInt16(olBcNum,0);

						/** MCU: 20010409 Check Hopo ***/
						string clActHopo = "";
						bool blHopoOk    = true;

						ArrayList items = new ArrayList(rlBcData.Twe.Split(','));						
						clActHopo = (string)items[1];

						if (clActHopo.Length > 0 && blUseHopos && clHopos.Length > 0)
						{
							if (clHopos.IndexOf(clActHopo) < 0)
							{
								blHopoOk = false;
							}
						}

						if (blHopoOk)
						{
							ropBcs.Add(rlBcData);
						}

						if(pcmBcLogfile.Length > 0)
						{
							DateTime olNow = DateTime.Now;

							rgOutFile = new StreamWriter(pcmBcLogfile,true);
							if (rgOutFile != null)
							{
								rgOutFile.WriteLine(string.Format("{0} BcNum: {1}",olNow.ToString("dd.MM.yyyy HH:mm:ss"),rlBcData.BcNum));
								rgOutFile.WriteLine(string.Format("ReqId    : {0}",rlBcData.ReqId));
								rgOutFile.WriteLine(string.Format("Cmd      : {0}",rlBcData.Cmd));
								rgOutFile.WriteLine(string.Format("Object   : {0}",rlBcData.Object));
								rgOutFile.WriteLine(string.Format("Selection: {0}",rlBcData.Selection));
								rgOutFile.WriteLine(string.Format("Fields   : {0}",rlBcData.Fields));
								rgOutFile.WriteLine(string.Format("Data     : {0}",rlBcData.Data));
								rgOutFile.Close();
								rgOutFile = null;
							}

						}
					}
				}
				else
				{
						if(pcmBcLogfile.Length > 0)
						{
							rgOutFile = new StreamWriter(pcmBcLogfile,true);
							if (rgOutFile != null)
							{
								byte[] olBcNum = new byte[2];
								olBcNum[0] = pclPacketBuf[COMMIF_data_offset+BC_HEAD_bc_num_offset+1];
								olBcNum[1] = pclPacketBuf[COMMIF_data_offset+BC_HEAD_bc_num_offset];
								int ilBcNum = BitConverter.ToInt16(olBcNum,0);
								rgOutFile.WriteLine(string.Format("ReqId: {0} BcNum(part): {1}\n",rlBcData.ReqId,ilBcNum));
								rgOutFile.Close();
								rgOutFile = null;
							}
						}

				}
				//if(llNextBcNum > slActualBcNum && !blBcNumHasWrapped)
				if((llNextBcNum > slActualBcNum) || blBcNumHasWrapped)
				{
					// we fetched all pending BCs
					blBcOK = false;
					if (bgExtendLog && pcmBcLogfile.Length > 0)
					{

						if(pcmBcLogfile.Length > 0)
						{
							rgOutFile = new StreamWriter(pcmBcLogfile,true);
							if (rgOutFile != null)
							{
								rgOutFile.WriteLine(string.Format("No more BC's pending: llNextBcNum: {0} slActualBcNum: {1}  blBcNumHasWrapped: {2}",llNextBcNum,slActualBcNum,blBcNumHasWrapped));
								rgOutFile.Close();
								rgOutFile = null;
							}
						}

					}
				}
				else
				{
					if (bgExtendLog && pcmBcLogfile.Length > 0)
					{
						rgOutFile = new StreamWriter(pcmBcLogfile,true);
						if (rgOutFile != null)
						{
							rgOutFile.WriteLine(string.Format("There are more BC's pending: llNextBcNum: {0} slActualBcNum: {1}  blBcNumHasWrapped: {2}",llNextBcNum,slActualBcNum,blBcNumHasWrapped));
							rgOutFile.Close();
							rgOutFile = null;
						}

					}

				}

			}

			return true;
		}

		private int strlen(byte[] opData,int ipOffset)
		{
			for (int i = ipOffset; i < opData.Length; i++)
			{
				if (opData[i] == 0)
				{
					return i - ipOffset;
				}
			}

			return 0;
		}

	}
}
