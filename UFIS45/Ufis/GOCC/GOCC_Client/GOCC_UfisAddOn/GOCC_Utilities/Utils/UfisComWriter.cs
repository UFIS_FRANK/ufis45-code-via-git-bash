#define UseUtf8Encoding

using System;
using System.ComponentModel;
using System.Collections;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Ufis.Utils;


namespace Ufis.Data
{
	/// <summary>
	/// This interface is implemented in the class <B>UfisComReader</B>.
	/// It provides all interfaces and communication relevant properties
	/// to communicate with the CEDA SQLHDL.
	/// </summary>
	/// <remarks>Please instantiata this interface to start your own
	/// comunication with the CEDA SQLHDL and do not instatiate <B>UfisComWriter</B>.
	/// </remarks>
	public interface IUfisComWriter
	{
		/// <summary>
		/// Server name of the UFIS server to establish a connection.
		/// </summary>
		/// <remarks>Instead of the name you can alternatively use the TCP/IP adress.</remarks>
		string	ServerName
		{
			get;
		}
		/// <summary>
		/// The name of the workstation, where the application is running.
		/// </summary>
		/// <remarks>This must be set in order to see the producer of
		/// a SQLHDL request in the CEDA logfiles.</remarks>
		string	WorkStation
		{
			get;
		}
		/// <summary>
		/// Name of the current user of this application.
		/// </summary>
		/// <remarks>This must be set in order to see the producer of
		/// a SQLHDL request in the CEDA logfiles.</remarks>
		string	UserName
		{
			get;
		}
		/// <summary>
		/// The home airport, where the project is running.
		/// </summary>
		/// <remarks>Please use this parameter correctly to ensure that you get data
		/// from the database.</remarks>
		string	HomeAirport
		{
			get;
		}
		/// <summary>
		/// Returns the last error message. This can be an SQL error or a socket error.
		/// </summary>
		/// <remarks>none.</remarks>
		string	LastErrorMessage
		{
			get;
		}
		/// <summary>
		/// Indicate if the connection must keep alive after a server call.
		/// <P></P>
		/// True: the connection will be kept.
		/// <P></P>
		/// False: The connection will be closed.
		/// </summary>
		/// <remarks>none.</remarks>
		bool	StayConnected
		{
			get;
			set;
		}
		/// <summary>
		/// Establishes the connection to the UFIS server.
		/// </summary>
		/// <param name="ServerName">Server name.</param>
		/// <param name="AccessMethod">"CEDA" or "ODBC"</param>
		/// <returns>true = success, false = failed.</returns>
		/// <remarks>OBDC is not used.</remarks>
		bool	InitCom(string ServerName, string AccessMethod);
		/// <summary>
		/// Disconnects from the server.
		/// </summary>
		/// <returns>true = success, false = failed.</returns>
		/// <remarks>none.</remarks>
		bool	CleanupCom();
		/// <summary>
		/// Returns the name of the workstation where the application in running.
		/// </summary>
		/// <returns>A string with the workstation name.</returns>
		/// <remarks>none.</remarks>
		string	GetWorkstationName();
		/// <summary>
		/// Set the necessary parameters to get data from the UFIS server. This
		/// has nothing to do with the communication itself, it is for data accessing
		/// purposes because the SQLHDL has it's own intelligence => do not wonder.
		/// </summary>
		/// <param name="UserName">Name of the current user.</param>
		/// <param name="HomeAirport">Home airport.</param>
		/// <param name="TableExt">Table extention "TAB"</param>
		/// <returns>true = success, false = failed.</returns>
		/// <remarks>none.</remarks>
		bool	SetCedaPerameters(string UserName, string HomeAirport, string TableExt);
        bool SetCedaPerameters(string UserName, string HomeAirport, string TableExt, string moduleName);
		/// <summary>
		/// not longer used. This method call the server to request or update data.
		/// The work will be done by the SQLHDL.
		/// </summary>
		/// <param name="Command">CECA router command, such as "URT", "IRT" etc.</param>
		/// <param name="Object">Database table name.</param>
		/// <param name="Fields">Database table fields as a comma separated list.</param>
		/// <param name="Where">SQL where clause.</param>
		/// <param name="Data">Data as a comma separated list.</param>
		/// <returns>true = success, false = failed.</returns>
		/// <remarks>none.</remarks>
		string	Ufis(string Command, string Object, string Fields, string Where,string Data);
		/// <summary>
		/// Return the number of data from the last read request.
		/// </summary>
		/// <returns>The number of data in the internal buffer array.</returns>
		/// <remarks>none.</remarks>
		long	GetBufferCount();
		/// <summary>
		/// Returns the data from the buffer array at line <B>BufferIndex</B>.
		/// </summary>
		/// <param name="BufferIndex">Index of the buffer array.</param>
		/// <returns>The data of the line as comma separated list.</returns>
		/// <remarks>none.</remarks>
		string	GetBufferLine(long BufferIndex);
		/// <summary>
		/// Returns the record at buffer index 0 (zero). Depending on the parameter
		/// the current index 0 will be removed or not.
		/// </summary>
		/// <param name="WithClear">true = clear the buffer after read.
		/// <P></P>False = do not clear buffer[0].</param>
		/// <returns>A comma separated string with data of buffer[0].</returns>
		/// <remarks>none.</remarks>
		string	GetDataBuffer(bool WithClear);
		/// <summary>
		/// Clears the entire data buffer.
		/// </summary>
		/// <remarks>none.</remarks>
		void	ClearDataBuffer();
		/// <summary>
		/// Performs a classical insert, select, update or delete command. It calls the
		/// SQLHDL to handle the request.
		/// </summary>
		/// <param name="Command">CECA router command, such as "URT", "IRT" etc.</param>
		/// <param name="Object">Database table name.</param>
		/// <param name="Fields">Database table fields as a comma separated list.</param>
		/// <param name="Data">Data as a comma separated list.</param>
		/// <param name="Where">SQL where clause.</param>
		/// <param name="Timout">Timeout parameter.</param>
		/// <returns>0 = success, not 0 = failed.</returns>
		/// <remarks>none.</remarks>
		short	CallServer(string Command, string Object, string Fields, string Data, string Where, string Timout);
	}

	/// <summary>
	/// Implements the <B>IUfisComWriter</B> interface. This class is responsible to
	/// perform the classical SQL statements as well as other specialized server actions.
	/// This is the trigger to manager the server actions from the client application point
	/// of view.
	/// </summary>
	/// <remarks>Do not instantiat this class. In regular cases everything is done
	/// for you with the help of <see cref="IDatabase"/> and <see cref="ITable"/>.
	/// You can consider this class a blackbox.</remarks>
	internal class UfisComWriter : System.ComponentModel.Component,IUfisComWriter
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Net.Sockets.Socket omCedaSock = null;

		private string	omAccessMethod;
		private string	omServerName;
		private string	omWorkStation;
		private string	omUserName;
		private string	omHomeAirport;
		private string	omTableExt;
		private string	omModule;
		private ArrayList	omCurrFields;
		private string	omTwe;
		private string	omTws;
		private string	omSeq;
		private string  omReqid;
		private string  omDest1;
		private string  omDest2;
		private string  omCmd;
		private string  omObject;
		private string  omOrigName;
		private string	omSelection;
		private string	omFields;
		private ArrayList	omDataArray;
		private	string	omLastErrorMessage;
		private static string olStaticRest = "";

		private	bool	bmStayConnected = false;
		private	bool	bmIsConnected	= false;
		private	bool	bmIsInCallServer= false;
		private int		imWindowsCodePage=1252;
		/// <summary>
		/// Socket error codes that could occur during server communication.
		/// </summary>
		enum ErrorCodes
		{
			/// <summary>
			/// Fail.
			/// </summary>
			RC_FAIL			= -1,
			/// <summary>
			/// No error
			/// </summary>
			RC_SUCCESS		= 0,
			/// <summary>
			/// Empty message occured.
			/// </summary>
			RC_EMPTY_MSG	= 1,
			/// <summary>
			/// Server was shut down.
			/// </summary>
			RC_SHUTDOWN     =-5,
			/// <summary>
			/// Comminication failed.
			/// </summary>
			RC_COMM_FAIL	=-2,
			/// <summary>
			/// Intitialisation failed.
			/// </summary>
			RC_INIT_FAIL	=-3,
			/// <summary>
			/// CEDA error.
			/// </summary>
			RC_CEDA_FAIL	=-4,
			/// <summary>
			/// Already init.
			/// </summary>
			RC_ALREADY_INIT	=-6,
			/// <summary>
			/// Nothing found.
			/// </summary>
			RC_NOT_FOUND	=-7,
			/// <summary>
			/// Data was cut.
			/// </summary>
			RC_DATA_CUT		=-8,
			/// <summary>
			/// Timeout.
			/// </summary>
			RC_TIMEOUT		=-9,
			/// <summary>
			/// Tried to reconnect.
			/// </summary>
			RC_ALREADY_CONNECTED = -10,
			/// <summary>
			/// No answer.
			/// </summary>
			RC_MISSING_ANSWER	 = -11,
			//Server busy.
			RC_BUSY				 = -31,
			/// <summary>
			/// To many applications connected.
			/// </summary>
			RC_TO_MANY_APP		 = -32,
		}

		private const int BC_HEAD_dest_name_offset	= 0;	
		private const int BC_HEAD_orig_name_offset	= 10;	
		private const int BC_HEAD_recv_name_offset	= 20;	
		private const int BC_HEAD_bc_num_offset		= 30;	
		private const int BC_HEAD_seq_id_offset		= 32;	
		private const int BC_HEAD_tot_buf_offset	= 42;	
		private const int BC_HEAD_act_buf_offset	= 44;	
		private const int BC_HEAD_req_seq_id_offset = 46;	
		private const int BC_HEAD_rc_offset			= 56;	
		private const int BC_HEAD_tot_size_offset	= 58;	
		private const int BC_HEAD_cmd_size_offset	= 60;	
		private const int BC_HEAD_data_size_offset	= 62;	
		private const int BC_HEAD_data_offset		= 64;	

		private const int BC_HEAD_size = 66;	

    
		private const int CMDBLK_command_offset = 0;	
		private const int CMDBLK_obj_name_offset= 6;	
		private const int CMDBLK_order_offset	= 39;	
		private const int CMDBLK_tw_start_offset= 41;	
		private const int CMDBLK_tw_end_offset	= 74;	
		private const int CMDBLK_data_offset	= 107;	

		private const int CMDBLK_size = 108;	

		private const int COMMIF_command_offset = 0;	
		private const int COMMIF_length_offset	= 4;	
		private const int COMMIF_data_offset	= 8;	

		private const int COMMIF_size = 12;	

		private const int DATAPACKETLEN = 1024-12;
		private const int NETREAD_SIZE  = 0x8000;
		private const int PACKET_LEN    = 1024;
		private const int NET_SHUTDOWN	= 0x1;
		private const int NET_DATA		= 0x2;
		private const int PACKET_DATA	= 65436;	// i.e. -100
		private const int PACKET_START	= 65336;	// i.e. -200
		private const int PACKET_END	= 65236;	// i.e. -300

		
		private const short SELECTION	= 0;
		private const short FIELDS		= 1;
		private const short DATA		= 2;

		/// <summary>
		/// Server name of the UFIS server to establish a connection.
		/// </summary>
		/// <remarks>Instead of the name you can alternatively use the TCP/IP adress.</remarks>
		public string ServerName
		{
			get
			{
				return omServerName;
			}
		}
		/// <summary>
		/// The name of the workstation, where the application is running.
		/// </summary>
		/// <remarks>This must be set in order to see the producer of
		/// a SQLHDL request in the CEDA logfiles.</remarks>
		public string WorkStation
		{
			get
			{
				return omWorkStation;
			}
		}
		/// <summary>
		/// Name of the current user of this application.
		/// </summary>
		/// <remarks>This must be set in order to see the producer of
		/// a SQLHDL request in the CEDA logfiles.</remarks>
		public string UserName
		{
			get
			{
				return omUserName;
			}
		}
		/// <summary>
		/// The home airport, where the project is running.
		/// </summary>
		/// <remarks>Please use this parameter correctly to ensure that you get data
		/// from the database.</remarks>
		public string HomeAirport
		{
			get
			{
				return omHomeAirport;
			}
		}
		/// <summary>
		/// Returns the last error message. This can be an SQL error or a socket error.
		/// </summary>
		/// <remarks>none.</remarks>
		public string LastErrorMessage
		{
			get
			{
				return omLastErrorMessage;
			}
		}

		/// <summary>
		/// Indicate if the connection must keep alive after a server call.
		/// <P></P>
		/// True: the connection will be kept.
		/// <P></P>
		/// False: The connection will be closed.
		/// </summary>
		/// <remarks>none.</remarks>
		public bool	StayConnected
		{
			get
			{
				return this.bmStayConnected;
			}
			set
			{
				this.StayConnected = value;
			}
		}

		/// <summary>
		/// Establishes the connection to the UFIS server.
		/// </summary>
		/// <param name="opServerName">Server name.</param>
		/// <param name="opAccessMethod">"CEDA" or "ODBC"</param>
		/// <returns>true = success, false = failed.</returns>
		/// <remarks>OBDC is not used.</remarks>
		public bool	InitCom(string opServerName, string opAccessMethod)
		{
			if (this.bmIsConnected)
				return false;

			omAccessMethod = opAccessMethod;
			omServerName   = opServerName;

			string	olOrigServer,olSvr1,olSvr2;
			bool blHas2Servers = false;

			olOrigServer = opServerName;
			int ilTmpPos = olOrigServer.LastIndexOf('/');
			if (ilTmpPos != -1)
			{
				olSvr1 = olOrigServer.Substring(0,ilTmpPos);
				++ilTmpPos;
				olSvr2 = olOrigServer.Substring(ilTmpPos);
				blHas2Servers = true;
			}
			else
			{
				olSvr1 = opServerName;
			}

			if (omAccessMethod == "")
			{
				omAccessMethod = "CEDA";
			}

			if (omAccessMethod == "CEDA")
			{
				omCedaSock = new System.Net.Sockets.Socket(AddressFamily.InterNetwork,SocketType.Stream,ProtocolType.Tcp);

				if (blHas2Servers == true)
				{
					System.Net.IPHostEntry olHostEntry = System.Net.Dns.Resolve(olSvr1);
					System.Net.IPAddress olAddress = olHostEntry.AddressList[0];
					int ilPort = 3350;
					System.Net.IPEndPoint olEndPoint1 = new System.Net.IPEndPoint(olAddress,ilPort);
					omCedaSock.Connect(olEndPoint1);
				}
				else
				{
					System.Net.IPHostEntry olHostEntry = System.Net.Dns.Resolve(olSvr1);
					System.Net.IPAddress olAddress = olHostEntry.AddressList[0];
					int ilPort = 3350;
					System.Net.IPEndPoint olEndPoint1 = new System.Net.IPEndPoint(olAddress,ilPort);
					omCedaSock.Connect(olEndPoint1);
				}

				omWorkStation = GetWorkstationName();
				if (this.bmIsInCallServer == false && this.bmStayConnected == false)
				{
					this.CleanupCom();
				}
				else
				{
					this.bmIsConnected = true;
				}
				return true;
			}
			else	// any other connection type
				return false;

		}
		/// <summary>
		/// Disconnects from the server.
		/// </summary>
		/// <returns>true = success, false = failed.</returns>
		/// <remarks>none.</remarks>
		public bool		CleanupCom()
		{
			if (omCedaSock != null)
			{
				omCedaSock.Shutdown(SocketShutdown.Both);
				omCedaSock = null;
				this.bmIsConnected = false;
			}
			return true;
		}

		/// <summary>
		/// Returns the name of the workstation where the application in running.
		/// </summary>
		/// <returns>A string with the workstation name.</returns>
		/// <remarks>none.</remarks>
		public string	GetWorkstationName()
		{
			return Dns.GetHostName();
		}

		/// <summary>
		/// Set the necessary parameters to get data from the UFIS server. This
		/// has nothing to do with the communication itself, it is for data accessing
		/// purposes because the SQLHDL has it's own intelligence => do not wonder.
		/// </summary>
		/// <param name="opUserName">Name of the current user.</param>
		/// <param name="opHomeAirport">Home airport.</param>
		/// <param name="opTableExt">Table extention "TAB"</param>
		/// <returns>true = success, false = failed.</returns>
		/// <remarks>none.</remarks>
		public bool		SetCedaPerameters(string opUserName, string opHomeAirport, string opTableExt)
		{
			omUserName	 = opUserName;
			omHomeAirport= opHomeAirport;
			omTableExt	 = opTableExt;
			return true;
		}

        public bool SetCedaPerameters(string opUserName, string opHomeAirport, string opTableExt, string moduleName)
        {
            this.omModule = moduleName;
            return SetCedaPerameters(opUserName, opHomeAirport, opTableExt);
        }

		/// <summary>
		/// not longer used. This method call the server to request or update data.
		/// The work will be done by the SQLHDL.
		/// </summary>
		/// <param name="opCommand">CECA router command, such as "URT", "IRT" etc.</param>
		/// <param name="opObject">Database table name.</param>
		/// <param name="opFields">Database table fields as a comma separated list.</param>
		/// <param name="opWhere">SQL where clause.</param>
		/// <param name="opData">Data as a comma separated list.</param>
		/// <returns>true = success, false = failed.</returns>
		/// <remarks>none.</remarks>
		public string	Ufis(string opCommand, string opObject, string opFields, string opWhere,string opData)
		{
			string olResult;
			string olCommand = opCommand;
			string olObject  = opObject;
			string olTimeout = opData;

			string olFields = opFields;
			omCurrFields.Clear();
			omCurrFields.AddRange(opFields.Split(','));

			string olWhere = opWhere;
			string olData  = opData;

			if (omTwe.Length == 0)
			{
				omTwe = omHomeAirport + "," + omTableExt + "," + omModule;
			}
			if (olObject.Length == 3)
			{
				if (omTableExt.Length == 3)
				{
					olObject += omTableExt;
				}
				else
				{
					//					throw();	// invalid table extension
				}
			}


			if (omAccessMethod == "CEDA" )
			{
				ErrorCodes ec = CallCeda(omWorkStation,omUserName,omWorkStation,olCommand,olObject,omSeq,omTws,omTwe,olWhere,olFields,olData,olTimeout);
			}
			else    //if(omAccessMethod == "ODBC")
			{
			}

			return olResult = string.Format("%d records",this.omDataArray.Count);

		}

		/// <summary>
		/// Return the number of data from the last read request.
		/// </summary>
		/// <returns>The number of data in the internal buffer array.</returns>
		/// <remarks>none.</remarks>
		public long		GetBufferCount()
		{
			return omDataArray.Count;
		}

		/// <summary>
		/// Returns the data from the buffer array at line <B>BufferIndex</B>.
		/// </summary>
		/// <param name="opBufferIndex">Index of the buffer array.</param>
		/// <returns>The data of the line as comma separated list.</returns>
		/// <remarks>none.</remarks>
		public string	GetBufferLine(long opBufferIndex)
		{
			return (string)omDataArray[(int)opBufferIndex];
		}

		/// <summary>
		/// Returns the record at buffer index 0 (zero). Depending on the parameter
		/// the current index 0 will be removed or not.
		/// </summary>
		/// <param name="bpWithClear">true = clear the buffer after read.
		/// <P></P>False = do not clear buffer[0].</param>
		/// <returns>A comma separated string with data of buffer[0].</returns>
		/// <remarks>none.</remarks>
		public string	GetDataBuffer(bool bpWithClear)
		{
			StringBuilder strResult = new StringBuilder("");
			if (bpWithClear)
			{
				while(omDataArray.Count > 0)
				{
					strResult.Append(omDataArray[0]);
					strResult.Append('\n');
					omDataArray.RemoveAt(0);
				}
			}
			else
			{
				for(int i = 0; i < omDataArray.Count; i++)
				{
					strResult.Append(omDataArray[i]);
					strResult.Append('\n');
				}
			}

			return strResult.ToString();
		}

		/// <summary>
		/// Clears the entire data buffer.
		/// </summary>
		/// <remarks>none.</remarks>
		public void		ClearDataBuffer()
		{
			
		}

		/// <summary>
		/// Performs a classical insert, select, update or delete command. It calls the
		/// SQLHDL to handle the request.
		/// </summary>
		/// <param name="opCommand">CECA router command, such as "URT", "IRT" etc.</param>
		/// <param name="opObject">Database table name.</param>
		/// <param name="opFields">Database table fields as a comma separated list.</param>
		/// <param name="opData">Data as a comma separated list.</param>
		/// <param name="opWhere">SQL where clause.</param>
		/// <param name="opTimeout">Timeout parameter.</param>
		/// <returns>0 = success, not 0 = failed.</returns>
		/// <remarks>none.</remarks>
		public short	CallServer(string opCommand, string opObject, string opFields, string opData, string opWhere, string opTimeout)
		{
			short ilReturn		= 0;
			string olCommand	= opCommand;
			string olObject		= opObject;
			string olWorkstation= this.omWorkStation;
			string olUsername	= this.omUserName;
			string olSeq		= this.omSeq;
			string olTws		= this.omTws;
			string olTwe		= this.omTwe;

			this.omCurrFields.Clear();
			this.omCurrFields.AddRange(opFields.Split(','));
			string olFields= opFields;
			string olWhere = opWhere;
			string olData  = opData;
			string olTimeout = opTimeout;

			if (olTwe.Length == 0)
			{
				olTwe = this.omHomeAirport + ',' + this.omTableExt + ',' + this.omModule;
			}

			if (this.bmIsConnected == false)
			{
				this.bmIsInCallServer = true;
				InitCom(omServerName, omAccessMethod);
				this.bmIsInCallServer = false;
			}

			if (this.omAccessMethod == "CEDA")
			{
				ErrorCodes ec = CallCeda(olWorkstation,olUsername,olWorkstation,olCommand,olObject,olSeq,olTws,olTwe,olWhere,olFields,olData,olTimeout);
				if (ec == ErrorCodes.RC_SUCCESS)
					ilReturn = 0;
				else
					ilReturn = 1;
			}
			else
			{
				ilReturn = 4711;
			}

			if (this.bmStayConnected == false)
			{
				if (this.bmIsConnected == true)
				{
					CleanupCom();
				}
			}

			return ilReturn;
		}

		private int	CalcBufLen (string opSelection,string opFields,string opData)
		{               
			int cmd_size, data_size, tot_size;
	
			cmd_size	= CMDBLK_size - 1 + opSelection.Length + opFields.Length;

			data_size	= opData.Length;

			tot_size	= BC_HEAD_size - 1 + cmd_size + data_size + 4;
	
			return tot_size;
		}

		private ErrorCodes SendBuffer(byte[] opPbc_head, int opBuflen)
		{
			Encoding olAE = Encoding.GetEncoding(this.imWindowsCodePage);
	
			byte[] lData = new byte[DATAPACKETLEN + COMMIF_size];
			int remain  = opBuflen;
			int offs    = 0;
			int send_len= 0;
			do
			{
				if (remain > DATAPACKETLEN ) 
				{
					byte[] olCommand = BitConverter.GetBytes((ushort)PACKET_DATA); 
					lData[COMMIF_command_offset+0] = olCommand[1];
					lData[COMMIF_command_offset+1] = olCommand[0];

					byte[] olLength  = BitConverter.GetBytes((uint)DATAPACKETLEN);
					lData[COMMIF_length_offset+0] = olLength[3];
					lData[COMMIF_length_offset+1] = olLength[2];
					lData[COMMIF_length_offset+2] = olLength[1];
					lData[COMMIF_length_offset+3] = olLength[0];

					Array.Copy(opPbc_head,offs,lData,COMMIF_data_offset,DATAPACKETLEN);
					send_len = DATAPACKETLEN + COMMIF_size;

					remain -= DATAPACKETLEN;
					offs+=DATAPACKETLEN;
				}
				else 
				{
					byte[] olCommand = BitConverter.GetBytes((ushort)PACKET_END); 
					lData[COMMIF_command_offset+0] = olCommand[1];
					lData[COMMIF_command_offset+1] = olCommand[0];

					byte[] olLength  = BitConverter.GetBytes((uint)(remain));
					lData[COMMIF_length_offset+0] = olLength[3];
					lData[COMMIF_length_offset+1] = olLength[2];
					lData[COMMIF_length_offset+2] = olLength[1];
					lData[COMMIF_length_offset+3] = olLength[0];

					Array.Copy(opPbc_head,offs,lData,COMMIF_data_offset,remain);
					send_len = remain + COMMIF_size;

					remain = 0;
				}
                   
				int rc = this.omCedaSock.Send(lData,send_len,SocketFlags.None);
				if (rc != send_len) 
				{
					System.Diagnostics.Trace.WriteLine("send failed");
					return ErrorCodes.RC_COMM_FAIL;
				} /* end if */
				else 
				{
					do 
					{
						byte[] olAck = new byte[COMMIF_size];
						rc = this.omCedaSock.Receive(olAck,0);
						if ( rc != COMMIF_size) 
						{
							System.Diagnostics.Trace.WriteLine("receive ACK failed");
//$$							Sleep(1);
						} /* end if */
					} while ( rc <= 0 );
				}
				
			}
			while (remain > 0);

			return ErrorCodes.RC_SUCCESS;
		}

		private ErrorCodes	CallCeda(string opReq_id,string opDest1,string opDest2,string opCmd,
            string opObj,string opSeq,string opTws,string opTwe,
            string opSelection,string opFields,string opData,string opTimeout)
		{
            
            DateTime dt1 = DateTime.Now;
            UT.LogMsg(String.Format("CallCeda Start : {0:0000}{1:00}{2:00}{3:00}{4:00}{5:00}.{6:000}, ", 
                dt1.Year, dt1.Month, dt1.Day, dt1.Hour, dt1.Minute, dt1.Second, dt1.Millisecond) + 
                "sel <" + opSelection + "> fld <" + opFields + "> data <" +opData + ">" 
                + ", reqId<" + opReq_id + ">" 
                + ", Dest1<" + opDest1 + ">, Dest2<" + opDest2 + ">"
                + ", Cmd<" + opCmd + ">, Obj<" + opObj + ">"
                + ", Seq<" + opSeq + ">, Tws<" + opTws + ">"
                + ", Twe<" + opTwe + ">, Sel<" + opSelection + ">"
                + ", TimeOut<" + opTimeout + ">" 
                );
			int olBuf_len = CalcBufLen(opSelection,opFields,opData);
			byte[] olBc_Head = new Byte[olBuf_len];
			Array.Clear(olBc_Head,0,olBc_Head.Length);

			Encoding olAE = Encoding.GetEncoding(this.imWindowsCodePage);

			// copy all data to the structure
			olAE.GetBytes(opDest1,0,System.Math.Min(opDest1.Length,10),olBc_Head,BC_HEAD_dest_name_offset);		// dest_name
			olAE.GetBytes(opTimeout,0,System.Math.Min(opTimeout.Length,10),olBc_Head,BC_HEAD_orig_name_offset); // orig_name 	
			olAE.GetBytes(opDest2,0,System.Math.Min(opDest2.Length,10),olBc_Head,BC_HEAD_recv_name_offset);    	// recv_name

			byte[] olBc_num = BitConverter.GetBytes((short)0);
			olBc_Head[BC_HEAD_bc_num_offset]   = olBc_num[0];		// bc_num
			olBc_Head[BC_HEAD_bc_num_offset+1] = olBc_num[1];
			
			olAE.GetBytes(opReq_id,0,System.Math.Min(opReq_id.Length,10),olBc_Head,BC_HEAD_seq_id_offset);				// seq_id    	

			byte[] olTot_buf = BitConverter.GetBytes((short)0);		// tot_buf
			olBc_Head[BC_HEAD_tot_buf_offset]	= olTot_buf[0];
			olBc_Head[BC_HEAD_tot_buf_offset+1] = olTot_buf[1];

			byte[] olAct_buf = BitConverter.GetBytes((short)0);		// act_buf
			olBc_Head[BC_HEAD_act_buf_offset]	= olAct_buf[0];
			olBc_Head[BC_HEAD_act_buf_offset+1] = olAct_buf[1];

			olAE.GetBytes(opCmd,0,Math.Min(opCmd.Length,6),olBc_Head,BC_HEAD_data_offset+CMDBLK_command_offset);	// command
			olAE.GetBytes(opObj,0,Math.Min(opObj.Length,33),olBc_Head,BC_HEAD_data_offset+CMDBLK_obj_name_offset);											// obj_name     
			olAE.GetBytes(opSeq,0,Math.Min(opSeq.Length,2),olBc_Head,BC_HEAD_data_offset+CMDBLK_order_offset);
			olAE.GetBytes(opTws,0,Math.Min(opTws.Length,33),olBc_Head,BC_HEAD_data_offset+CMDBLK_tw_start_offset);
			olAE.GetBytes(opTwe,0,Math.Min(opTwe.Length,33),olBc_Head,BC_HEAD_data_offset+CMDBLK_tw_end_offset);

			int ilCmd_size = 4 - 1 + opSelection.Length + opFields.Length; 
			byte[] olCmdSize = BitConverter.GetBytes((short)ilCmd_size);
			olBc_Head[BC_HEAD_cmd_size_offset]	= olCmdSize[0];
			olBc_Head[BC_HEAD_cmd_size_offset+1]= olCmdSize[1];

			byte[] olDataSize = BitConverter.GetBytes((short)opData.Length);
			olBc_Head[BC_HEAD_data_size_offset]	 = olDataSize[0];
			olBc_Head[BC_HEAD_data_size_offset+1] = olDataSize[1];

			int ilTotSize = BC_HEAD_size - 1 + ilCmd_size + opData.Length;
			byte[] olTotSize = BitConverter.GetBytes((short)ilTotSize);
			olBc_Head[BC_HEAD_tot_size_offset]	= olTotSize[0];
			olBc_Head[BC_HEAD_tot_size_offset+1]= olTotSize[1];

			int ilIndex = BC_HEAD_data_offset + CMDBLK_size - 1;
			olAE.GetBytes(opSelection,0,opSelection.Length,olBc_Head,ilIndex);
			ilIndex += opSelection.Length + 1;

			olAE.GetBytes(opFields,0,opFields.Length,olBc_Head,ilIndex);
			ilIndex += opFields.Length + 1;

			this.ConvertStringToBytes(olAE,opData,olBc_Head,ilIndex);
            DateTime dt2 = DateTime.Now;
            TimeSpan ts = dt2.Subtract(dt1);
            dt1 = dt2;
            UT.LogMsg("CallCeda: b4 send time : " + ts.TotalMilliseconds.ToString());
			SendBuffer(olBc_Head,olBuf_len);
            dt2 = DateTime.Now;
            ts = dt2.Subtract(dt1);
            dt1 = dt2;
            UT.LogMsg("CallCeda: send time : " + ts.TotalMilliseconds.ToString());

            ErrorCodes ilTotalReturn;
            dt1 = DateTime.Now;
			ilTotalReturn = ReceiveUfisData();
            dt2 = DateTime.Now;
            ts = dt2.Subtract(dt1);
            dt1 = dt2;
            UT.LogMsg("CallCeda: received time : " + ts.TotalMilliseconds.ToString());
            UT.LogMsg(String.Format("CallCeda End : {0:0000}{1:00}{2:00}{3:00}{4:00}{5:00}.{6:000}", dt1.Year, dt1.Month, dt1.Day, dt1.Hour, dt1.Minute, dt1.Second, dt1.Millisecond)); 
            return ilTotalReturn;
		}

		private int strlen(byte[] opData,int ipOffset)
		{
			for (int i = ipOffset; i < opData.Length; i++)
			{
				if (opData[i] == 0)
				{
					return i - ipOffset;
				}
			}

			return 0;
		}

		private string ConvertBytesToString(Encoding olAE,byte[] pclTotalDataBuffer,int ipIndex,int llTransferBytes)
		{
#if	UseUtf8Encoding
			// we can't use UTF8Encoder class because this class tries to convert 2 bytes to a single unicode character
			return olAE.GetString(pclTotalDataBuffer,ipIndex,llTransferBytes);
#else
			string olTotalDataBuffer = string.Empty;
			StringBuilder builder = new StringBuilder(llTransferBytes);
			byte[]	pair = new byte[2]; 
			pair[1] = 0;
			for (int i = 0; i < llTransferBytes; i++)
			{
				pair[0] = pclTotalDataBuffer[ipIndex+i];
				builder.Append(BitConverter.ToChar(pair,0));
			}
			return builder.ToString();
#endif
		}

		private void ConvertStringToBytes(Encoding olAE,string opData,byte[] olBc_Head,int ilIndex)
		{
#if	UseUtf8Encoding
			olAE.GetBytes(opData,0,opData.Length,olBc_Head,ilIndex);
#else
			for (int i = 0; i < opData.Length; i++)
			{
				byte[] result = BitConverter.GetBytes(opData[i]);
				olBc_Head[ilIndex+i] = result[0];				
			}
#endif
			
		}

		private bool AddToList(Encoding olAE,ref string olList,byte[] pcpActData,int ipOffset,ref int ipLen)
		{
			bool blFound = false;

			int ilLen = strlen(pcpActData,ipOffset);

			if (ilLen < ipLen)
			{
				blFound = true;
				olList += this.ConvertBytesToString(olAE,pcpActData,ipOffset,ilLen);
				ipLen  -= ilLen + 1;
			}

			if (!blFound)
			{
				olList += this.ConvertBytesToString(olAE,pcpActData,ipOffset,ipLen);
			}

			return blFound;
		}

		private bool MakeRow(Encoding olAE,byte[] pclDataBuf,int ipOffset,int spCommand, int ipLength,bool bpFirstLine)
		{
			string olDat	= string.Empty;
			string olNewStr	= string.Empty;
			string olRest	= string.Empty;

			if (bpFirstLine)
			{
				olStaticRest = string.Empty;
			}

			if (olStaticRest.Length != 0)
			{
				int ilolDatLen;
				string	TmpBuf = this.ConvertBytesToString(olAE,pclDataBuf,ipOffset,ipLength);
				olDat  = olStaticRest + TmpBuf;	//pclDataBuf;
				ilolDatLen = olDat.Length;
				ipLength += olStaticRest.Length;
			}
			else
			{
				olDat = this.ConvertBytesToString(olAE,pclDataBuf,ipOffset,ipLength);
			}

			int ilIdx = olDat.IndexOf("\n");
			int ilCrIdx = olDat.IndexOf("\r");
			if (ilIdx == -1)
			{
				olRest = olDat.Substring(0,ipLength);
			}

			while (ilIdx != -1)
			{
				if (ilIdx != 0)
				{
					if( ilCrIdx != -1)
					{
						olNewStr = olDat.Substring(0,ilIdx-1);
						int ilRight = olDat.Length - ilIdx - 1;
						olRest	 = olDat.Substring(olDat.Length - ilRight);
						ipLength -= (ilIdx + 1);   /// + 1 wg CR
					}
					else
					{
						olNewStr = olDat.Substring(0,ilIdx);
						int ilRight = olDat.Length - ilIdx - 1;
						olRest	 = olDat.Substring (olDat.Length - ilRight);
						ipLength -= (ilIdx + 1);   
					}
					omDataArray.Add (olNewStr);
					int ilTest = olNewStr.Length;
					olDat = olRest;
					ilIdx = olDat.IndexOf("\n");
					ilCrIdx = olDat.IndexOf("\r");
				}
				else
				{
					if (ilCrIdx != -1)
					{
						olNewStr = olDat.Substring(0,ilIdx-1);
						int ilRight = olDat.Length - ilIdx - 1;
						olRest	 = olDat.Substring(olDat.Length - ilRight);
						ipLength -= (ilIdx + 1);   /// + 1 wg CR
					}
					else
					{
						olNewStr = olDat.Substring(0,ilIdx);
						int ilRight = olDat.Length - ilIdx - 1;
						olRest	 = olDat.Substring (olDat.Length - ilRight);
						ipLength -= (ilIdx + 1);   
					}
					int ilTest = olNewStr.Length;
					olDat = olRest;
					ilIdx = olDat.IndexOf  ("\n");
					ilCrIdx = olDat.IndexOf("\r");
				}
			}
			if ((ushort)spCommand == PACKET_DATA)
			{
				olStaticRest = olRest.Substring(0,ipLength);
			} 
			else if ((ushort)spCommand == PACKET_END)
			{
				if (ipLength > 1)
				{
					if (olDat.Length >= ipLength)
					{
						omDataArray.Add (olDat.Substring(0,ipLength));
					}
					else
					{
						omDataArray.Add (olDat);
					}
				}
			}
			return true;	
		}


		private ErrorCodes ReceiveUfisData()
		{
			this.omDataArray.Clear();
			this.omSelection = string.Empty;
			this.omFields = string.Empty;
			this.omLastErrorMessage = string.Empty;

			Encoding olAE = Encoding.GetEncoding(this.imWindowsCodePage);
			
			ErrorCodes ret	= ErrorCodes.RC_SUCCESS;	
			int packet_len	= NETREAD_SIZE;
			int	rc			= 0;
			int count		= 0;
			int cur_len		= 0;
			int p_len		= 0;
			byte[] pdata	= new byte[PACKET_LEN];

			bool blReady	 = false;
			bool blFirstLine = true;
			bool blFirstData = true;
			short slStatus	 = SELECTION; 
			bool blDelFound	 = false;

			while (!blReady)
			{
				int	cur_pdata = 0;	// offset
				cur_len		  = 0;
				p_len		  = PACKET_LEN;

				while ((ret == ErrorCodes.RC_SUCCESS) && (cur_len < PACKET_LEN) && (cur_len < p_len))
				{
					rc = omCedaSock.Receive(pdata,cur_pdata,PACKET_LEN - cur_len, SocketFlags.None);
					System.Diagnostics.Trace.WriteLine(rc.ToString());
					if (rc <= 0)
					{
						blReady = true;
						ret = ErrorCodes.RC_FAIL;
					} 
					else
					{
						if (rc == 0)
						{
							// MCU 96.10.11 we should return RC_FAIL if the connection has been closed
							ret = ErrorCodes.RC_FAIL;
						}
						else
						{
							cur_len	  += rc;
							cur_pdata += rc;
							if (cur_len >= COMMIF_size)
							{   
								/* How many bytes will come ? */
								byte[] olLength = new Byte[4];
								olLength[0] = pdata[COMMIF_length_offset+3];
								olLength[1] = pdata[COMMIF_length_offset+2];
								olLength[2] = pdata[COMMIF_length_offset+1];
								olLength[3] = pdata[COMMIF_length_offset];
								p_len = BitConverter.ToInt32(olLength,0) + COMMIF_size;
							} 
							ret = ErrorCodes.RC_SUCCESS;
						}
					}
				}

				int ilDataLen = 0;
				int ilActData = 0;
				bool blReady2 = true;

				if (ret == ErrorCodes.RC_SUCCESS)
				{
					byte[] olAck = new byte[COMMIF_size];
					byte[] olCmd = BitConverter.GetBytes((ushort)PACKET_DATA);
					olAck[COMMIF_command_offset]   = olCmd[1];
					olAck[COMMIF_command_offset+1] = olCmd[0];

					byte[] olLength = BitConverter.GetBytes(COMMIF_size);
					olAck[COMMIF_length_offset] = olLength[3];
					olAck[COMMIF_length_offset] = olLength[2];
					olAck[COMMIF_length_offset] = olLength[1];
					olAck[COMMIF_length_offset] = olLength[0];

					omCedaSock.Send(olAck,0,COMMIF_size,SocketFlags.None);

					olCmd[0] = pdata[COMMIF_command_offset+1];
					olCmd[1] = pdata[COMMIF_command_offset];
					int ilCmd = BitConverter.ToInt16(olCmd,0);

					olLength[0] = pdata[COMMIF_length_offset+3];
					olLength[1] = pdata[COMMIF_length_offset+2];
					olLength[2] = pdata[COMMIF_length_offset+1];
					olLength[3] = pdata[COMMIF_length_offset];
					int ilLength = BitConverter.ToInt32(olLength,0);

					if (blFirstLine)
					{
						//	int ilGarbageLen = sizeof(BC_HEAD) -1 + sizeof(CMDBLK) - 1;
						int ilGarbageLen = BC_HEAD_size - 1 + CMDBLK_size - 1 - 1;
						ilDataLen = ilLength - ilGarbageLen;

						int ilTest	  = ilGarbageLen + 8;
						int ilBcHead  = COMMIF_data_offset;
						int ilRc = BitConverter.ToInt16(pdata,ilBcHead + BC_HEAD_rc_offset);
						if(ilRc < 0 )
						{
							slStatus = 14; //to prevent that switch/case of status will be entered
							int ilData = ilBcHead + BC_HEAD_data_offset;
							omLastErrorMessage = this.ConvertBytesToString(olAE,pdata,ilData,strlen(pdata,ilData));
							ret = ErrorCodes.RC_FAIL;
						}
						else
						{
							int ilCmdBlk  = ilBcHead + BC_HEAD_data_offset;			//	(CMDBLK  *) BcHead->data;
							ilActData	  = ilCmdBlk + CMDBLK_data_offset;			//	(char *) CmdBlk->data;
							int ilSelKey  = ilActData;								//	(char *) CmdBlk->data;

							int ilFldLst  = ilSelKey + strlen (pdata,ilSelKey) + 1;
							int ilDatLst  = ilFldLst + strlen (pdata,ilFldLst) + 1;

							blFirstLine	= false;
							int ilSeqId = ilBcHead + BC_HEAD_seq_id_offset;
							omReqid		= this.ConvertBytesToString(olAE,pdata,ilSeqId,strlen(pdata,ilSeqId));

							int ilDest1 = ilBcHead + BC_HEAD_dest_name_offset;								
							omDest1		= this.ConvertBytesToString(olAE,pdata,ilDest1,strlen(pdata,ilDest1));

							int ilDest2 = ilBcHead + BC_HEAD_recv_name_offset;								
							omDest2		= this.ConvertBytesToString(olAE,pdata,ilDest2,strlen(pdata,ilDest2));

							int ilCommand	= ilCmdBlk + CMDBLK_command_offset;								
							omCmd		= this.ConvertBytesToString(olAE,pdata,ilCommand,strlen(pdata,ilCommand));

							int ilObject= ilCmdBlk + CMDBLK_obj_name_offset;								
							omObject	= this.ConvertBytesToString(olAE,pdata,ilObject,strlen(pdata,ilObject));

							int ilSeq	= ilBcHead + BC_HEAD_seq_id_offset;								
							omSeq		= this.ConvertBytesToString(olAE,pdata,ilSeq,strlen(pdata,ilSeq));

							int ilTws	= ilCmdBlk + CMDBLK_tw_start_offset;								
							omTws		= this.ConvertBytesToString(olAE,pdata,ilTws,strlen(pdata,ilTws));

							int ilTwe	= ilCmdBlk + CMDBLK_tw_end_offset;								
							omTwe		= this.ConvertBytesToString(olAE,pdata,ilTwe,strlen(pdata,ilTwe));

							int ilOrig	= ilBcHead + BC_HEAD_orig_name_offset;								
							omOrigName	= this.ConvertBytesToString(olAE,pdata,ilOrig,strlen(pdata,ilOrig));
						}
					}
					else
					{
						ilActData  = COMMIF_data_offset;
						ilDataLen  = ilLength;
					}
					do
					{
						switch(slStatus)
						{
							case SELECTION: 
								blDelFound = AddToList(olAE,ref this.omSelection,pdata,ilActData,ref ilDataLen);
								break;
							case FIELDS:  
								blDelFound = AddToList(olAE,ref this.omFields,pdata,ilActData,ref ilDataLen);
								break;
							case DATA: 
								MakeRow (olAE,pdata,ilActData,ilCmd,ilDataLen,blFirstData);
								blFirstData = false;
								break;
						};
						blReady2 = true;

						if (slStatus != DATA)
						{
							if (blDelFound)
							{
								ilActData += strlen(pdata,ilActData)+1;
								slStatus++;
								blReady2 = false;
								blDelFound = false;
							}
						}
					} while (! blReady2 );

					if ((ushort)ilCmd == PACKET_END) 
					{
						blReady = true;
					}
				
				} /* fi */
			} /* end while */
			return ret;
		}



		/// <summary>
		/// Contructor.
		/// </summary>
		/// <param name="container">This is a .NET component so the container is relevant.</param>
		/// <remarks>none.</remarks>
		public UfisComWriter(System.ComponentModel.IContainer container)
		{
			/// <summary>
			/// Required for Windows.Forms Class Composition Designer support
			/// </summary>
			container.Add(this);
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//

			IniFile	iniFile = new IniFile("c:\\ufis\\system\\ceda.ini");

			string tmpValue = iniFile.IniReadValue("GLOBAL","WINDOWSCODEPAGE");
			if (tmpValue.Length == 0)
			{
				this.imWindowsCodePage = 1252;
			}
			else
			{
				this.imWindowsCodePage = int.Parse(tmpValue);
			}


			this.omModule		= string.Empty;
			this.omCmd			= string.Empty;
			this.omDest1		= string.Empty;
			this.omDest2		= string.Empty;
			this.omHomeAirport	= string.Empty;
			this.omObject		= string.Empty;
			this.omOrigName		= string.Empty;
			this.omReqid		= string.Empty;
			this.omSelection	= string.Empty;
			this.omSeq			= string.Empty;
			this.omServerName	= string.Empty;
			this.omTableExt		= string.Empty;
			this.omTwe			= string.Empty;
			this.omTws			= string.Empty;
			this.omUserName		= string.Empty;
			this.omWorkStation	= string.Empty;
			this.omCurrFields = new ArrayList();
			this.omDataArray  = new ArrayList();
		}
		/// <summary>
		/// Default contructor.
		/// </summary>
		/// <remarks>none.</remarks>
		public UfisComWriter()
		{
			/// <summary>
			/// Required for Windows.Forms Class Composition Designer support
			/// </summary>
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			this.omModule		= string.Empty;
			this.omCmd			= string.Empty;
			this.omDest1		= string.Empty;
			this.omDest2		= string.Empty;
			this.omHomeAirport	= string.Empty;
			this.omObject		= string.Empty;
			this.omOrigName		= string.Empty;
			this.omReqid		= string.Empty;
			this.omSelection	= string.Empty;
			this.omSeq			= string.Empty;
			this.omServerName	= string.Empty;
			this.omTableExt		= string.Empty;
			this.omTwe			= string.Empty;
			this.omTws			= string.Empty;
			this.omUserName		= string.Empty;
			this.omWorkStation	= string.Empty;
			this.omCurrFields = new ArrayList();
			this.omDataArray  = new ArrayList();
		}

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
		}
		#endregion
	}
}
