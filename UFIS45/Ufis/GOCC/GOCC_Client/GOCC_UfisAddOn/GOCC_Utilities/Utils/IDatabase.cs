using System;
using System.Collections;

namespace Ufis.Data
{
	/// <summary>
	/// The state indicate the current status of a record stored in <see cref="IRow"/>. 
	/// The state can be new, updated, deleted or untouched.
	/// </summary>
	/// <remarks>
	/// After reading data from the database the current state of an <see cref="IRow"/>
	/// is Unchanged.
	/// </remarks>
	public	enum	State
	{
		/// <summary>
		/// A new <see cref="IRow"/> was created.
		/// </summary>
		Created,			
		/// <summary>
		/// The IRow had not been touched and has it's intitial state since it was
		/// fetched from the database.
		/// </summary>
		Unchanged,			
		/// <summary>
		/// The IRow has been changed by the application (or indirectly by the user).
		/// </summary>
		Modified,			
		/// <summary>
		/// The row has been deleted.
		/// </summary>
		Deleted				
	}

	/// <summary>
	/// An instance of the EventArgs class is delegated when a broadcast occured. The data is
	/// extracted from the data stream, delivered by broadcast syntax and is stored in the 
	/// single string member of this class.
	/// </summary>
	/// <remarks>
	/// This will be delegated an you should implement a static class, which is responsible
	/// for the your application internal data changes communication. This is to be considered
	/// as an internal data hub. This is the entry point for broadcasts into you application.
	/// <B>DatabaseEventArgs</B> is used by the delegate <see cref="DatabaseEventHandler"/>.
	/// <P></P><P></P><B>NOTE:</B><P></P>
	/// There is a multi level mechanism to incorporate broadcasts. The following cases
	/// or a combination of the cases must be taken into account for application 
	/// development:
	/// <P></P>
	/// <list type="bullet">
	///		<listheader>
	///			<description>ajksjljk</description>
	///		</listheader>
	///		<item>
	///			Automatically done by <see cref="IDatabase"/> and <see cref="ITable"/>.
	///			This is the case for table oriented broadcasts and no specific further
	///			methods are defined by the application developer to care about the broadcasts.
	///			The broadcast data will be incorporated into the respective <B>ITable's</B> rows.
	///			In this case the application will get no notification that a broadcast occured.
	///			This might be reasonable for basic data, which have to be looked up or presented
	///			in choice list but not more.
	///		</item>
	///		<item>
	///			A braodcast was received, which has nothing to do with a specific ITable
	///			object, but the application has to react nevertheless on that broadcast. For this
	///			purpose the <see cref="DatabaseEventHandler"/> delegate is called. You must 
	///			register for this delegate and write an <B>event method</B> to get the braodcast
	///			information as <see cref="DatabaseEventArgs"/> parameter to decide the further
	///			processing. This is useful for "SBC" commands and the "CLO" command, which are
	///			independent from database tables. This delegate is produced from the 
	///			<see cref="IDatabase"/> instance.
	///		</item>
	///		<item>
	///			A broadcast was received for a table. This table contains 1.000.000 records. 
	///			Normally you decide to load just a subset of all records following a certain
	///			criteria (SQL where clause), meaning that you are insterested in broadcasts that
	///			fit this criteria. The Ufis.Data namespace class instances cannot decide for you
	///			which data is relevant for the application. In order to make the decision, if a
	///			broadcast is interesting for the application we need some kind of callback 
	///			mechanism. The <see cref="ITable.OnUpdateRequestHandler"/> and the delegate <see cref="UpdateRequestHandler"/>
	///			handle the callback structure. You application code must register for this
	///			delegate to get the information which current broadcast is received. The 
	///			<B>event method</B> must examinate the content of the broadcast and 
	///			<B>return true or false</B>
	///			to inform the <B>ITable</B> object about to consider it in or not.
	///			Usually you application had loaded flight data for a certain time frame. This
	///			is the way to test, if the broadcast's data fit into this time frame or not.
	///		</item>
	///		<item>
	///			A table sends a delegate that it has processed a broadcast and stored the data
	///			to the inner list of <see cref="IRow"/>s. Your applicationo must register for
	///			the <see cref="DatabaseTableEventHandler"/> delegate event to get the information
	///			about the new data.
	///		</item>
	/// </list>
	/// </remarks>
	/// <example>
	/// The following sample class is a skeleton, which can be used to be copied
	/// into you code as the data hub. It registers for the DatabaseEventArgs.
	/// The class name <B>DE</B> is for <B>DataExchange</B>.
	/// <code>
	/// [C#]
	/// public class  DE
	/// {
	///		static IDatabase myDB = null;		//must be set from your application
	///		static ITable    myAdresses = null; //must be set from your application
	/// 	static ITable    myCustomers = null;//must be set from your application
	/// 	static BCBlinker myBCBlinker = null;//must be set from your application
	/// 
	///		public delegate void AnythingChanged( object sender, string strOstUrno, State state);
	///		public static event AnythingChanged OnAnythingChanged;
	/// 
	/// 	// necessary for CLO messages to be distributed to your application
	/// 	public delegate void CLO_Message();
	/// 	public static event CLO_Message OnCLO_Message;
	/// 
	/// 	static public void InitDBObjects(Ufis.Utils.BCBlinker blinker)
	/// 	{
	/// 		myBCBlinker = blinker;
	/// 		myDB = UT.GetMemDB();
	/// 		myAdresses = myDB["ADRESSES"];
	/// 		myCustomers = myDB["CUSTOMERS"];
	/// 		// Register for the delegate from IDatabase
	/// 		myDB.OnEventHandler += new DatabaseEventHandler(DatabaseEventHandler);
	/// 	}
	/// 
	/// 	//This is called from the registered event method to distribute into 
	/// 	// your application, so you must register this delegate.
	/// 	static public void Call_AnythingChanged(DatabaseEventArgs eventArgs)
	/// 	{
	/// 		if(OnAnythingChanged != null)
	/// 			OnAnythingChanged(eventArgs);
	/// 	}
	/// 	static void DatabaseEventHandler(object obj,DatabaseEventArgs eventArgs)
	/// 	{
	/// 		myBCBlinker.Blink();
	/// 		if (eventArgs.command == "SBC")
	/// 		{
	/// 			switch(eventArgs.obj)
	/// 			{
	/// 				case "ADRESSES":
	/// 					//Call a delegate to inform your application
	/// 					break;
	/// 				case "CUSTOMERS":
	/// 					//Call a delegate to inform your application
	/// 					break;
	/// 				default:
	/// 					break;
	/// 			}
	/// 		}
	/// 		if(eventArgs.command == "CLO")
	/// 		{
	/// 			// Recation CLO=CEDA-close message (server has switched) 
	/// 			if(OnCLO_Message != null)
	/// 			{
	/// 				OnCLO_Message();//
	/// 			}
	/// 		}
	/// 	}
	/// }
	/// </code>
	/// </example>
	public class	DatabaseEventArgs : EventArgs
	{
		/// <summary>
		/// The constructor. The parameters identify the command as a ceda router
		/// command, e.g. "IRT", "URT", "DRT" etc. the obj specifies the database
		/// table.
		/// </summary>
		/// <param name="command">The ceda router command.</param>
		/// <param name="obj">Name of the database table.</param>
		/// <remarks>none.</remarks>
		public	DatabaseEventArgs(string command,string obj)
		{
			this.command = command;
			this.obj	 = obj;
		}
		/// <summary>
		/// The user
		/// </summary>
		/// <remarks>none.</remarks>
		public string	reqId;
		/// <summary>
		/// TO DO
		/// </summary>
		/// <remarks>none.</remarks>
		public string	dest1;
		/// <summary>
		/// TO DO
		/// </summary>
		/// <remarks>none.</remarks>
		public string	dest2;
		/// <summary>
		/// The ceda router command.
		/// </summary>
		/// <remarks>none.</remarks>
		public string	command;
		/// <summary>
		/// The database table.
		/// </summary>
		/// <remarks>none.</remarks>
		public string	obj;
		/// <summary>
		/// TO DO
		/// </summary>
		/// <remarks>none.</remarks>
		public string	seq;
		/// <summary>
		/// Is for the client application. Whatever was stored you will get back by the broadcast.
		/// </summary>
		/// <remarks>none.</remarks>
		public string	tws;
		/// <summary>
		/// Contains a string "Hopo,Tabext,Application,Version number"
		/// </summary>
		/// <remarks>none.</remarks>
		public string	twe;
		/// <summary>
		/// An SQL Where statement
		/// </summary>
		/// <remarks>none.</remarks>
		public string	selection;
		/// <summary>
		/// The affected database fields.
		/// </summary>
		/// <remarks>none.</remarks>
		public string	fields;
		/// <summary>
		/// The data in a comma separated string.
		/// </summary>
		/// <remarks>none.</remarks>
		public string	data;
		/// <summary>
		/// The broadcast number.
		/// </summary>
		/// <remarks>none.</remarks>
		public string	bcnum;
		/// <summary>
		/// The broadcast attachment.
		/// </summary>
		/// <remarks>none.</remarks>
		public string	attachment;
		/// <summary>
		/// Placeholder and currently not used.
		/// </summary>
		/// <remarks>none.</remarks>
		public string	additional;
	}

	/// <summary>
	/// An instance of this class is delegated after a broadcast has been worked in the
	/// local representation of the database table <see cref="ITable"/>. The class contains
	/// all needed information about the changes of the data, meaning the router command, 
	/// the table name, the modified fields, the modified data and the previous/old data.
	/// </summary>
	/// <example>
	/// The following sample class is a skeleton, which can be used to be copied
	/// into you code as the data hub. It registers for the DatabaseTableEventArgs.
	/// The class name <B>DE</B> is for <B>DataExchange</B>.
	/// This sample class is similar to the sample class in <see cref="DatabaseEventArgs"/>, but
	/// considers other aspects.
	/// <code>
	/// [C#]
	/// public class  DE
	/// {
	///		static IDatabase myDB = null;		//must be set from your application
	///		static ITable    myAdresses = null; //must be set from your application
	/// 	static ITable    myCustomers = null;//must be set from your application
	/// 	static BCBlinker myBCBlinker = null;//must be set from your application
	/// 
	///		public delegate void AnythingChanged( object sender, string strOstUrno, State state);
	///		public static event AnythingChanged OnAnythingChanged;
	/// 
	/// 	// necessary for CLO messages to be distributed to your application
	/// 	public delegate void CLO_Message();
	/// 	public static event CLO_Message OnCLO_Message;
	/// 
	/// 	static public void InitDBObjects(Ufis.Utils.BCBlinker blinker)
	/// 	{
	/// 		myBCBlinker = blinker;
	/// 		myDB = UT.GetMemDB();
	/// 		myAdresses = myDB["ADRESSES"];
	/// 		myCustomers = myDB["CUSTOMERS"];
	/// 		// Register for the delegate from IDatabase
	/// 		myDB.OnTableEventHandler += new DatabaseTableEventHandler(TableEventHandler); 
	/// 	}
	/// 
	/// 	static void TableEventHandler(object obj,DatabaseTableEventArgs eventArgs)
	/// 	{
	/// 		myBCBlinker.Blink();						
	/// 		ITable myTable = myDB[eventArgs.table];
	/// 		if (myTable == myAdresses)
	/// 		{
	/// 			HandleAdressesEvent(obj,eventArgs); //To be implemented in your App.
	/// 		}
	/// 		if(myTable == myCustomers)
	/// 		{
	/// 			HandleCustomersEvent(obj,eventArgs); //To be implemented in your App.
	///			}
	///		}
	/// }
	/// </code>
	/// </example>
	/// <remarks>none.</remarks>
	public class	DatabaseTableEventArgs : EventArgs
	{
		/// <summary>
		/// Constructor. Stores directly the parameters concerning the table, command
		/// and the record as <see cref="IRow"/>.
		/// </summary>
		/// <param name="command">Ceda router command.</param>
		/// <param name="table">Database table name.</param>
		/// <param name="row">IRow instance of the record.</param>
		/// <remarks>none.</remarks>
		public	DatabaseTableEventArgs(string command,string table,IRow row)
		{
			this.command = command;
			this.table	 = table;
			this.row	 = row;
		}
		/// <summary>
		/// The ceda router command, such as "IRT", "URT", "DRT"
		/// </summary>
		/// <remarks>none.</remarks>
		public string		command;
		/// <summary>
		/// The database table name.
		/// </summary>
		/// <remarks>none.</remarks>
		public string		table;
		/// <summary>
		/// An array, which contains the modified fields.
		/// </summary>
		/// <remarks>none.</remarks>
		public ArrayList	modifiedFields = new ArrayList();
		/// <summary>
		/// An array, which contains the modified data.
		/// </summary>
		/// <remarks>none.</remarks>
		public ArrayList	modifiedData   = new ArrayList();
		/// <summary>
		/// An array, which contains the previous (or old) data.
		/// </summary>
		/// <remarks>none.</remarks>
		public ArrayList	previousData   = new ArrayList();
		/// <summary>
		/// The record represented as <see cref="IRow"/>.
		/// </summary>
		/// <remarks>none.</remarks>
		public IRow			row;

        /// <summary>
        /// Get Original Data (before modify) and New Data (after modify) for a given field name
        /// </summary>
        /// <param name="fieldName">Field Name to search for data</param>
        /// <param name="orgData">Original Data (Before Modify)</param>
        /// <param name="newData">New Data (After Modify)</param>
        /// <returns></returns>
        public bool GetDataFor(string fieldName, out object orgData, out object newData)
        {
            bool found = false;
            if ((fieldName == null) || (fieldName.Trim() == "")) throw new ApplicationException("No Field Name to get the data.");
            int cnt = modifiedFields.Count;
            orgData = null;
            newData = null;
            fieldName = fieldName.Trim().ToUpper();

            for (int i = 0; i < cnt; i++)
            {
                if (modifiedFields[i].ToString() == fieldName)
                {
                    if (i < modifiedData.Count) newData = modifiedData[i];
                    if (i < previousData.Count) orgData = previousData[i];
                    found = true;
                    break;
                }
            }
            return found;
        }
    }

    /// <summary>
    /// This delegate will be called by the framework if the broadcast has been successfully processed by the table object
    /// </summary>
    /// <param name="sender">The sender of this delegate.</param>
    /// <param name="eventData">The change information for a table</param>
    /// <remarks>
    /// There is a multi level mechanism to incorporate broadcasts. The following cases
    /// or a combination of the cases must be taken into account for application 
    /// development:
    /// <P></P>
    /// <list type="bullet">
    ///		<listheader>
    ///			<description>ajksjljk</description>
    ///		</listheader>
    ///		<item>
    ///			Automatically done by <see cref="IDatabase"/> and <see cref="ITable"/>.
    ///			This is the case for table oriented broadcasts and no specific further
    ///			methods are defined by the application developer to care about the broadcasts.
    ///			The broadcast data will be incorporated into the respective <B>ITable's</B> rows.
    ///			In this case the application will get no notification that a broadcast occured.
    ///			This might be reasonable for basic data, which have to be looked up or presented
    ///			in choice list but not more.
    ///		</item>
    ///		<item>
    ///			A braodcast was received, which has nothing to do with a specific ITable
    ///			object, but the application has to react nevertheless on that broadcast. For this
    ///			purpose the <see cref="DatabaseEventHandler"/> delegate is called. You must 
    ///			register for this delegate and write an <B>event method</B> to get the braodcast
    ///			information as <see cref="DatabaseEventArgs"/> parameter to decide the further
    ///			processing. This is useful for "SBC" commands and the "CLO" command, which are
    ///			independent from database tables. This delegate is produced from the 
    ///			<see cref="IDatabase"/> instance.
    ///		</item>
    ///		<item>
    ///			A broadcast was received for a table. This table contains 1.000.000 records. 
    ///			Normally you decide to load just a subset of all records following a certain
    ///			criteria (SQL where clause), meaning that you are insterested in broadcasts that
    ///			fit this criteria. The Ufis.Data namespace class instances cannot decide for you
    ///			which data is relevant for the application. In order to make the decision, if a
    ///			broadcast is interesting for the application we need some kind of callback 
    ///			mechanism. The <see cref="ITable.OnUpdateRequestHandler"/> and the delegate <see cref="UpdateRequestHandler"/>
    ///			handle the callback structure. You application code must register for this
    ///			delegate to get the information which current broadcast is received. The 
    ///			<B>event method</B> must examinate the content of the broadcast and 
    ///			<B>return true or false</B>
    ///			to inform the <B>ITable</B> object about to consider it in or not.
    ///			Usually you application had loaded flight data for a certain time frame. This
    ///			is the way to test, if the broadcast's data fit into this time frame or not.
    ///		</item>
    ///		<item>
    ///			A table sends a delegate that it has processed a broadcast and stored the data
    ///			to the inner list of <see cref="IRow"/>s. Your applicationo must register for
    ///			the <see cref="DatabaseTableEventHandler"/> delegate event to get the information
    ///			about the new data.
    ///		</item>
    /// </list>
    /// </remarks>
    public delegate void DatabaseTableEventHandler(object sender, DatabaseTableEventArgs eventData);		

	/// <summary>
	/// This delegate will be called by the framework for each broadcast which couldn't be handled by the framework
	/// </summary>
	/// <param name="sender">The originator of this delegate.</param>
	/// <param name="eventData">Contains the information about the changes.</param>
	/// <remarks>
	/// There is a multi level mechanism to incorporate broadcasts. The following cases
	/// or a combination of the cases must be taken into account for application 
	/// development:
	/// <P></P>
	/// <list type="bullet">
	///		<listheader>
	///			<description>ajksjljk</description>
	///		</listheader>
	///		<item>
	///			Automatically done by <see cref="IDatabase"/> and <see cref="ITable"/>.
	///			This is the case for table oriented broadcasts and no specific further
	///			methods are defined by the application developer to care about the broadcasts.
	///			The broadcast data will be incorporated into the respective <B>ITable's</B> rows.
	///			In this case the application will get no notification that a broadcast occured.
	///			This might be reasonable for basic data, which have to be looked up or presented
	///			in choice list but not more.
	///		</item>
	///		<item>
	///			A braodcast was received, which has nothing to do with a specific ITable
	///			object, but the application has to react nevertheless on that broadcast. For this
	///			purpose the <see cref="DatabaseEventHandler"/> delegate is called. You must 
	///			register for this delegate and write an <B>event method</B> to get the braodcast
	///			information as <see cref="DatabaseEventArgs"/> parameter to decide the further
	///			processing. This is useful for "SBC" commands and the "CLO" command, which are
	///			independent from database tables. This delegate is produced from the 
	///			<see cref="IDatabase"/> instance.
	///		</item>
	///		<item>
	///			A broadcast was received for a table. This table contains 1.000.000 records. 
	///			Normally you decide to load just a subset of all records following a certain
	///			criteria (SQL where clause), meaning that you are insterested in broadcasts that
	///			fit this criteria. The Ufis.Data namespace class instances cannot decide for you
	///			which data is relevant for the application. In order to make the decision, if a
	///			broadcast is interesting for the application we need some kind of callback 
	///			mechanism. The <see cref="ITable.OnUpdateRequestHandler"/> and the delegate <see cref="UpdateRequestHandler"/>
	///			handle the callback structure. You application code must register for this
	///			delegate to get the information which current broadcast is received. The 
	///			<B>event method</B> must examinate the content of the broadcast and 
	///			<B>return true or false</B>
	///			to inform the <B>ITable</B> object about to consider it in or not.
	///			Usually you application had loaded flight data for a certain time frame. This
	///			is the way to test, if the broadcast's data fit into this time frame or not.
	///		</item>
	///		<item>
	///			A table sends a delegate that it has processed a broadcast and stored the data
	///			to the inner list of <see cref="IRow"/>s. Your applicationo must register for
	///			the <see cref="DatabaseTableEventHandler"/> delegate event to get the information
	///			about the new data.
	///		</item>
	/// </list>
	/// </remarks>
	public delegate	void DatabaseEventHandler(object sender,DatabaseEventArgs eventData);	

	/// <summary>
	/// This delegate will be called by the framework when running in single threaded mode to ensure that the broadcast evaluation runs within the main thread
	/// </summary>
	/// <param name="pReqId">User ID</param>
	/// <param name="pDest1">TO DO</param>
	/// <param name="pDest2">TO DO</param>
	/// <param name="pCmd">Router command</param>
	/// <param name="pObject">Database table name</param>
	/// <param name="pSeq">TO DO</param>
	/// <param name="pTws">Application relevant field</param>
	/// <param name="pTwe">User,Wks,application</param>
	/// <param name="pSelection">SQL Where statement</param>
	/// <param name="pFields">Database table fields.</param>
	/// <param name="pData">Data</param>
	/// <param name="pBcNum">Broadcast number</param>
	/// <param name="pAttachment">Broadcast attachment an further information.</param>
	/// <param name="pAdditional">For later usage.</param>
	/// <param name="stBcHdl">A delegate, which responsible to serialize all
	/// delegates concerning the <see cref="ITable.OnUpdateRequestHandler"/>, <see cref="DatabaseTableEventHandler"/> 
	/// and <see cref="ITable.OnUpdateRequestHandler"/>.</param>
	/// <remarks>
	/// <P><B>Problem Domain:</B></P>
	/// When a broadcast is received, in priour version the delegate was directly sent to the
	/// application in order to process the broadcast. The problem is, that the BCProxy instance forces
	/// the .NET environment to start a second thread, which implies that we now have to consider
	/// a multi threaded environment. 
	/// <P><B>The idea:</B></P>
	/// The <B>MainThreadBroadcastHandler</B> controls that not two methods access concurrently
	/// the <see cref="IDatabase"/>, <see cref="ITable"/> and <see cref="IRow"/> methods.
	/// To ensure this the calls must be performed by calling the methods via <B>Invoke</B>.<P></P>
	/// To avoid the implementation of calling <B>Invoke</B> for each delegate from the 
	/// application's point of view, the <B>MainThreadBroadcastHandler</B> delegate is sent and
	/// the application has to register for this delegate and in the delegate's event method the
	/// <see cref="SingleThreadedBroadcastHandler"/> and redirect the call to the <b>IDatabase</b>
	/// with all parameters, which internally syncronizes the calls to force serialization. From
	/// now on the real data delegates are sent.
	/// <P></P><P></P><P></P>
	/// <img src="MainThreadBroadcastHandler.jpg"></img>
	/// <P></P><P></P><P></P>
	/// </remarks>
	/// <example>
	/// The following example demonstrates the usage of <B>MainThreadBroadcastHandler</B>:
	/// <code>
	/// [C#]
	/// private void YourForm_Load()
	/// {
	///		myDB.OnMainThreadBroadcastHandler = new MainThreadBroadcastHandler(SingleThreadedBcEvent);
	/// }
	///	private void SingleThreadedBcEvent(string pReqId, string pDest1, 
	///									   string pDest2,string pCmd, string pObject, 
	///									   string pSeq,string pTws, string pTwe, string pSelection, 
	///									   string pFields, string pData, string pBcNum, 
	///									   string pAttachment,string pAdditional,
	///									   SingleThreadedBroadcastHandler singleThreadedBroadcastHandler)
	/// {
	///     singleThreadedBroadcastHandler(pReqId,pDest1,pDest2,pCmd,pObject,pSeq,pTws,pTwe,
	///									   pSelection,pFields,pData,pBcNum,pAttachment,pAdditional);			
	/// }
	///
	/// </code>
	/// </example>
	public delegate void MainThreadBroadcastHandler(string pReqId, string pDest1, string pDest2,
													string pCmd, string pObject, string pSeq,
													string pTws, string pTwe, string pSelection, 
													string pFields, string pData, string pBcNum,
													string pAttachment,string pAdditional, 
													SingleThreadedBroadcastHandler stBcHdl);

	// <IMG alt="" src=".\Images\MainThreadBroadcastHandler.jpg">
	// <img src="C:\Entw\Help_Ufis_Utils_Images\MainThreadBroadcastHandler.jpg">

	
	
	/// <summary>
	/// This delegate must be called within the application specific InternalRequestHandler 
	/// when running in single threaded mode.
	/// </summary>
	/// <param name="pReqId">User ID</param>
	/// <param name="pDest1">TO DO</param>
	/// <param name="pDest2">TO DO</param>
	/// <param name="pCmd">Router command</param>
	/// <param name="pObject">Database table name</param>
	/// <param name="pSeq">TO DO</param>
	/// <param name="pTws">Application relevant field</param>
	/// <param name="pTwe">User,Wks,application</param>
	/// <param name="pSelection">SQL Where statement</param>
	/// <param name="pFields">Database table fields.</param>
	/// <param name="pData">Data</param>
	/// <param name="pBcNum">Broadcast number</param>
	/// <param name="pAttachment">Broadcast attachment an further information.</param>
	/// <param name="pAdditional">For later usage.</param>
	/// <remarks>
	/// <P><B>Problem Domain:</B></P>
	/// When a broadcast is received, in priour version the delegate was directly sent to the
	/// application in order to process the broadcast. The problem is, that the BCProxy instance forces
	/// the .NET environment to start a second thread, which implies that we now have to consider
	/// a multi threaded environment. 
	/// <P><B>The idea:</B></P>
	/// The <B>MainThreadBroadcastHandler</B> controls that not two methods access concurrently
	/// the <see cref="IDatabase"/>, <see cref="ITable"/> and <see cref="IRow"/> methods.
	/// To ensure this the calls must be performed by calling the methods via <B>Invoke</B>.<P></P>
	/// To avoid the implementation of calling <B>Invoke</B> for each delegate from the 
	/// application's point of view, the <B>MainThreadBroadcastHandler</B> delegate is sent and
	/// the application has to register for this delegate and in the delegate's event method the
	/// <see cref="SingleThreadedBroadcastHandler"/> and redirect the call to the <b>IDatabase</b>
	/// with all parameters, which internally syncronizes the calls to force serialization. From
	/// now on the real data delegates are sent.
	/// <see href="C:\Entw\Help_Ufis_Utils_Images\MainThreadBroadcastHandler.jpg"></see> 
	/// <P></P>
	/// </remarks>
	/// <example>
	/// The following example demonstrates the usage of <B>MainThreadBroadcastHandler</B>:
	/// <code>
	/// [C#]
	/// private void YourForm_Load()
	/// {
	///		myDB.OnMainThreadBroadcastHandler = new MainThreadBroadcastHandler(SingleThreadedBcEvent);
	/// }
	///	private void SingleThreadedBcEvent(string pReqId, string pDest1, 
	///									   string pDest2,string pCmd, string pObject, 
	///									   string pSeq,string pTws, string pTwe, string pSelection, 
	///									   string pFields, string pData, string pBcNum, 
	///									   string pAttachment,string pAdditional,
	///									   SingleThreadedBroadcastHandler singleThreadedBroadcastHandler)
	/// {
	///     singleThreadedBroadcastHandler(pReqId,pDest1,pDest2,pCmd,pObject,pSeq,pTws,pTwe,
	///									   pSelection,pFields,pData,pBcNum,pAttachment,pAdditional);			
	/// }
	///
	/// </code>
	/// </example>
	public delegate void SingleThreadedBroadcastHandler(string pReqId, string pDest1, 
														string pDest2,string pCmd, string pObject, 
														string pSeq,string pTws, string pTwe, 
														string pSelection, string pFields, 
														string pData, string pBcNum,
														string pAttachment,string pAdditional);

	/// <summary>
	/// IDatabase provides access to a single object of the internal memory object.
	/// The application may not directly access to the implementation of the interface
	/// <see cref="IDatabaseImp"/>. All public accessible members, properties and methods
	/// are declared here.
	/// </summary>
	/// <remarks>
	/// Your programm handles only one instance of the IDatabase. To get a valid instance
	/// call the UT.GetMemDB() method.
	/// </remarks>
	/// <example>
	/// This example describes how to use this interface as a skeleton for connection to 
	/// the server and intially reading the data.
	/// <code>
	/// public bool ReadData()
	/// {
	/// 	IDatabase myDB = UT.GetMemDB();
	/// 	bool result = myDB.Connect(UT.ServerName,UT.UserName,UT.Hopo,"TAB","The_ApplicationName");
	/// 	if(result == false)
	/// 	{
	/// 		return false;
	/// 	}
	/// 	ITable myTable = myDB.Bind("Adresses", "CADR", "ID,Name,Zipcoed,City", "10,32,10,32", "ID,Name,Zipcoed,City");
	/// 	myTable.Load("ORDER BY Name");
	/// 	myTable.CreateIndex("ID", "ID");
	/// 	return true;
	/// }
	/// </code>
	/// </example>
	public interface	IDatabase
	{
		/// <summary>
		/// Returns whether this instance is Thread safe or not.
		/// <seealso cref="IDatabase.SyncRoot"/>
		/// </summary>
		/// <example>
		/// This sample demonstrate the usage of instances in a multithreaded environment:
		/// <P></P>
		/// <code>
		/// [C#]
		/// IDatabase myDB = UT.GetMemDB();
		/// // Now we generate our threaded instance.
		/// IDatabase myMultiThreadedDB = (IDatabase)myDB.SyncRoot;
		/// bool snc1 = myMultiThreadedDB.IsSyncronized;
		/// bool snc2 = myDB.IsSyncronized;
		/// 
		/// // The result of snc1 = true
		/// // The result of snc2 = false
		/// </code>
		/// </example>
		/// <remarks>none.</remarks>
		bool IsSyncronized
		{
			get;
		}

		/// <summary>
		/// This returns a thread safe wrapper of an instance.
		/// </summary>
		/// <example>
		/// This sample demonstrate the usage of instances in a multithreaded environment:
		/// <P></P>
		/// <code>
		/// [C#]
		/// IDatabase myDB = UT.GetMemDB();
		/// // Now we generate our threaded instance.
		/// IDatabase myMultiThreadedDB = (IDatabase)myDB.SyncRoot;
		/// bool snc1 = myMultiThreadedDB.IsSyncronized;
		/// bool snc2 = myDB.IsSyncronized;
		/// 
		/// // The result of snc1 = true
		/// // The result of snc2 = false
		/// </code>
		/// </example>
		/// <remarks>none.</remarks>
		object	SyncRoot
		{
			get;
		}

		/// <summary>
		/// Returns the internal database <B>Reader</B> of the instance.
		/// </summary>
		/// <remarks>
		///		In a multi threaded environment the get accessor returns
		///		a new instance of reader, because the internal reader is
		///		not thread safe. <seealso cref="Writer"/>
		/// </remarks>
		IUfisComReader	Reader
		{
			get;
		}

		/// <summary>
		/// Returns the internal database <B>Writer</B> of the instance.
		/// </summary>
		/// <remarks>
		///		In a multi threaded environment the get accessor returns
		///		a new instance of <B>Writer</B>, because the internal reader is
		///		not thread safe. <seealso cref="Reader"/>
		/// </remarks>
		IUfisComWriter	Writer
		{
			get;
		}

		/// <summary>
		/// This is the <B>Event</B> for the delegate <see cref="DatabaseTableEventHandler"/>, 
		/// which will be called for each <see cref="ITable"/> relevant broadcast event.
		///  <see cref="DatabaseTableEventHandler"/> delegate.
		/// </summary>
		/// <remarks>
		/// There is a multi level mechanism to incorporate broadcasts. The following cases
		/// or a combination of the cases must be taken into account for application 
		/// development:
		/// <P></P>
		/// <list type="bullet">
		///		<listheader>
		///			<description>ajksjljk</description>
		///		</listheader>
		///		<item>
		///			Automatically done by <see cref="IDatabase"/> and <see cref="ITable"/>.
		///			This is the case for table oriented broadcasts and no specific further
		///			methods are defined by the application developer to care about the broadcasts.
		///			The broadcast data will be incorporated into the respective <B>ITable's</B> rows.
		///			In this case the application will get no notification that a broadcast occured.
		///			This might be reasonable for basic data, which have to be looked up or presented
		///			in choice list but not more.
		///		</item>
		///		<item>
		///			A braodcast was received, which has nothing to do with a specific ITable
		///			object, but the application has to react nevertheless on that broadcast. For this
		///			purpose the <see cref="DatabaseEventHandler"/> delegate is called. You must 
		///			register for this delegate and write an <B>event method</B> to get the braodcast
		///			information as <see cref="DatabaseEventArgs"/> parameter to decide the further
		///			processing. This is useful for "SBC" commands and the "CLO" command, which are
		///			independent from database tables. This delegate is produced from the 
		///			<see cref="IDatabase"/> instance.
		///		</item>
		///		<item>
		///			A broadcast was received for a table. This table contains 1.000.000 records. 
		///			Normally you decide to load just a subset of all records following a certain
		///			criteria (SQL where clause), meaning that you are insterested in broadcasts that
		///			fit this criteria. The Ufis.Data namespace class instances cannot decide for you
		///			which data is relevant for the application. In order to make the decision, if a
		///			broadcast is interesting for the application we need some kind of callback 
		///			mechanism. The <see cref="ITable.OnUpdateRequestHandler"/> and the delegate <see cref="UpdateRequestHandler"/>
		///			handle the callback structure. You application code must register for this
		///			delegate to get the information which current broadcast is received. The 
		///			<B>event method</B> must examinate the content of the broadcast and 
		///			<B>return true or false</B>
		///			to inform the <B>ITable</B> object about to consider it in or not.
		///			Usually you application had loaded flight data for a certain time frame. This
		///			is the way to test, if the broadcast's data fit into this time frame or not.
		///		</item>
		///		<item>
		///			A table sends a delegate that it has processed a broadcast and stored the data
		///			to the inner list of <see cref="IRow"/>s. Your applicationo must register for
		///			the <see cref="DatabaseTableEventHandler"/> delegate event to get the information
		///			about the new data.
		///		</item>
		/// </list>
		/// </remarks>
		DatabaseTableEventHandler	OnTableEventHandler
		{
			get;
			set;
		}

		/// <summary>
		/// This is the <B>Event</B> for the delegate, which will be called by the 
		/// framework for each broadcast which couldn't be handled by the framework
		/// </summary>
		/// <remarks>
		/// There is a multi level mechanism to incorporate broadcasts. The following cases
		/// or a combination of the cases must be taken into account for application 
		/// development:
		/// <P></P>
		/// <list type="bullet">
		///		<listheader>
		///			<description>ajksjljk</description>
		///		</listheader>
		///		<item>
		///			Automatically done by <see cref="IDatabase"/> and <see cref="ITable"/>.
		///			This is the case for table oriented broadcasts and no specific further
		///			methods are defined by the application developer to care about the broadcasts.
		///			The broadcast data will be incorporated into the respective <B>ITable's</B> rows.
		///			In this case the application will get no notification that a broadcast occured.
		///			This might be reasonable for basic data, which have to be looked up or presented
		///			in choice list but not more.
		///		</item>
		///		<item>
		///			A braodcast was received, which has nothing to do with a specific ITable
		///			object, but the application has to react nevertheless on that broadcast. For this
		///			purpose the <see cref="DatabaseEventHandler"/> delegate is called. You must 
		///			register for this delegate and write an <B>event method</B> to get the braodcast
		///			information as <see cref="DatabaseEventArgs"/> parameter to decide the further
		///			processing. This is useful for "SBC" commands and the "CLO" command, which are
		///			independent from database tables. This delegate is produced from the 
		///			<see cref="IDatabase"/> instance.
		///		</item>
		///		<item>
		///			A broadcast was received for a table. This table contains 1.000.000 records. 
		///			Normally you decide to load just a subset of all records following a certain
		///			criteria (SQL where clause), meaning that you are insterested in broadcasts that
		///			fit this criteria. The Ufis.Data namespace class instances cannot decide for you
		///			which data is relevant for the application. In order to make the decision, if a
		///			broadcast is interesting for the application we need some kind of callback 
		///			mechanism. The <see cref="ITable.OnUpdateRequestHandler"/> and the delegate <see cref="UpdateRequestHandler"/>
		///			handle the callback structure. You application code must register for this
		///			delegate to get the information which current broadcast is received. The 
		///			<B>event method</B> must examinate the content of the broadcast and 
		///			<B>return true or false</B>
		///			to inform the <B>ITable</B> object about to consider it in or not.
		///			Usually you application had loaded flight data for a certain time frame. This
		///			is the way to test, if the broadcast's data fit into this time frame or not.
		///		</item>
		///		<item>
		///			A table sends a delegate that it has processed a broadcast and stored the data
		///			to the inner list of <see cref="IRow"/>s. Your applicationo must register for
		///			the <see cref="DatabaseTableEventHandler"/> delegate event to get the information
		///			about the new data.
		///		</item>
		/// </list>
		/// </remarks>
		DatabaseEventHandler		OnEventHandler
		{
			get;
			set;
		}

		/// <summary>
		/// Register or unregister an application specific handler for the MainThreadBroadcastHandler delegate
		/// </summary>
		/// <remarks>This is the Event for the <B>MainThreadBroadcastHandler</B>.</remarks>
		MainThreadBroadcastHandler	OnMainThreadBroadcastHandler
		{
			set;
			get;
		}

		/// <summary>
		/// This indexer provides the look and feel of an array, which lists all
		/// ITable objects stored in the <see cref="IDatabase"/>.
		/// Returns the table object of type <see cref="ITable"/> associated to the 
		/// database with the name as index.
		/// </summary>
		/// <param name="name">The logical name of the <see cref="ITable"/> object.</param>
		/// <remarks>
		/// In case that the table object with this name does not exists
		/// the return value is <B>null</B>.
		/// </remarks>
		/// <example>
		/// The usage of this indexer is as follows:
		/// <code>
		/// [C#]
		/// private void method()
		/// {
		///		IDatabase myDB = UT.GetMemDB();
		///		ITable myAdresses = myDB["ADRESSES"];
		///		if(myAdresses == null)
		///		{
		///			return;
		///		}
		///		else
		///		{
		///			//Do something
		///		}
		/// }
		/// </code>
		/// </example>
		ITable					this[string name]
		{
			get;
		}

		/// <summary>
		/// Return or modify the current urno reallocation size of this object
		/// </summary>
		/// <remarks>
		/// The reallocation size means the number of URNOs (Unique Record Numbers) that
		/// shall be fetched at once. The <see cref="GetNextUrno()"/> method performs
		/// a lookup in the internal free urno list and returns the next. For the case
		/// that there are no more URNOs available it fetches "n" URNOs from the server.
		/// The number of URNOs to be fetched is specifies with this property.
		/// </remarks>
		uint					UrnoReallocationSize
		{
			get;
			set;
		}

		/// <summary>
		/// Return or modify the automatic save flag after each modification.
		/// </summary>
		/// <remarks>
		/// The application must decide, whether or not all changes in IRow objects
		/// have to be stored directly in the database. The alternative is to set the
		/// SaveImmediately property to false and collect all changes. After the work 
		/// has been finished you can call the <see cref="IDatabase.Save()"/> method
		/// which stores all changes in all ITable objects or to call the 
		/// <see cref="ITable.Save()"/> method for a dedicated table.
		/// </remarks>
		bool					SaveImmediately
		{
			get;
			set;
		}

		/// <summary>
		/// Return or modify the automatic index update after each modification.
		/// </summary>
		/// <remarks>
		/// It is strongly recommended to set this property to true. Otherwise, if
		/// you are working with indexes a lot of confusion might come up, because
		/// the indexes are not up to date. The reason is, that new broadcasts might
		/// be received and the records are updated automatically by the IDatabase/ITable
		/// couple so that the indexes should always be assure.
		/// </remarks>
		bool					IndexUpdateImmediately
		{
			get;
			set;
		}

		/// <summary>
		/// Connects this object with the database at the specified server.
		/// </summary>
		/// <param name="server">The server name to be connected.</param>
		/// <param name="userName">The current user name.</param>
		/// <param name="hopo">The home airport.</param>
		/// <param name="tableExt">The table Extention: "TAB"</param>
		/// <param name="appl">The application name.</param>
		/// <returns>true = connected, false = disconnected.</returns>
		/// <remarks>
		/// This method must be called once at the application start time. The connection
		/// to the server keeps (logically, not physically) always established.
		/// </remarks>
		/// <example>
		/// The connection need a valid IDatabase instance:
		/// <code>
		/// </code>
		/// 	IDatabase myDB = UT.GetMemDB();
		///		bool result = myDB.Connect(UT.ServerName,UT.UserName,UT.Hopo,"TAB","MY_APP_NAME");
		/// </example>
		bool Connect(string server,string userName,string hopo,string tableExt,string appl);

		/// <summary>
		/// Disconnects this object from the connected database. This should be done when the
		/// application terminates.
		/// </summary>
		/// <remarks>
		/// Please note that broadcasts from the previously connected database still will be regarded
		/// </remarks>
		void Disconnect();

		/// <summary>
		/// Creates a new ITable object and binds it to the instance to this object
		/// </summary>
		/// <param name="name">
		///		Logical name of the table, this is the name, which is
		///		used to get the ITable object from the IDatabase instance. myTab = myDB["ADRESSES"];
		/// </param>
		/// <param name="dbTableName">The table name without extention("TAB")</param>
		/// <param name="fields">
		///		All fields, including the logical fields for internal use.
		///	</param>
		/// <param name="fieldLengths">
		///		The lengths of the field in bytes as a comma separated string. 
		///		This is necessary for sorting purposes. To avoid sorting confusion
		///		please enter the correct length for each field.
		/// </param>
		/// <param name="dbFields">
		///		The fields of the database table as a comma separated string.
		///		During an SQL select statement only this fields are taken into
		///		account, even if the table contains more fields.
		/// </param>
		/// <returns>The new <see cref="ITable"/> instance created during this method call.</returns>
		/// <remarks>
		///		This method should be called only once a the start time of the application.
		///		Otherwise you must call <see cref="IDatabase.Unbind"/> to remove this ITable instance.
		/// </remarks>
		/// <example>
		/// This example demonstrate the usage of Bind() and the creation of a new 
		/// <see cref="ITable"/> instance.
		/// <code>
		/// [C#]
		///	IDatabase myDB = UT.GetMemDB();
		///	ITable myTable = myDB.Bind("APT", "APT", "URNO,APC3,APC4,APFN", "10,3,4,32", "URNO,APC3,APC4,APFN");
		/// </code>
		/// </example>
		ITable	Bind(string name,string dbTableName,string fields,string fieldLengths,string dbFields);

		/// <summary>
		/// Unbinds the ITable object identified by its name from this object
		/// </summary>
		/// <param name="name">The logical name of the <see cref="ITable"/> object.</param>
		/// <remarks>
		/// Normally you call this method a termination time of the application.
		/// </remarks>
		void	Unbind(string name);

		/// <summary>
		/// Returns whether this object has been modified or not
		/// </summary>
		/// <remarks>
		/// none.
		/// </remarks>
		bool Modified
		{
			get;
		}

		/// <summary>
		/// Saves this object with all tables if connected to the database.
		/// It iterate through all nested <see cref="ITable"/> objects and calls
		/// the <see cref="ITable.Save()"/> method.
		/// </summary>
		/// <returns>true = success, false = failed.</returns>
		/// <remarks>
		/// It is strongly recommended to use this function very careful. It depends 
		/// on the application's requirements. Usually the developer should use the 
		/// Save mothod of the ITable object directly.
		/// </remarks>
		bool Save();

		/// <summary>
		/// Returns the next unique record number available from the connected database
		/// </summary>
		/// <returns>The next free URNO as a string.</returns>
		/// <remarks>none</remarks>
		string GetNextUrno();

		/// <summary>
		/// Returns whether this database is connected or not.
		/// </summary>
		/// <remarks>none</remarks>
		bool Connected
		{
			get;
		}

		/// <summary>
		/// Return or modify the multithreading support flag for this object.
		/// </summary>
		/// <remarks>none</remarks>
		bool Multithreaded
		{
			get;
			set;
		}

        /// <summary>
        /// To pause the receiving of broadcast.
        /// </summary>
        void PauseBroadcast();//AM:20080228 - To pause the receiving of broadcast

        /// <summary>
        /// To continue to receive the broadcast.
        /// </summary>
        void ContinueBroadcast();//AM:20080228 - To continue to receive the broadcast
        string TblsWithUrnoInSt
        {//AM:20080416 - To keep the comma separated 3 Letter Table Names.
            //To use in Where Cluase to put urno in "'" (singal quote) or as number
            get;
            set;
        }
	}

}
