using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Ufis.Utils;
using Ufis.Data;
using System.Diagnostics.SymbolStore;
using BirdView;
using System.Reflection;
using Microsoft.Win32;

using System.Threading;

namespace Status_Manager
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class frmMain : System.Windows.Forms.Form
    {
        #region #region------ MyRegion
        private UT.ExceptionOccured		delegateExceptionOccured = null;
		private DE.RuleEditorClosed		delegateRuleEditorClosed = null;
		private DE.StatusOverviewClosed delegateStatusOverviewClosed = null;
		private DE.StatusDetailClosed	delegateStatusDetailClosed = null;
		private DE.BirdViewSaved		delegateBirdViewSaved = null;
		private DE.OpenDetailStatusDlg  delegateOpenDetailStatusDlg = null;
		private DE.FindList_Closed		delegateFindListClosed = null;
		private DE.NavigateToFlight     delegateNavigateToFlight = null;
		private DE.UpdateFlightData		delegateUpdateFlightData = null;
        private DE.FlightStatus_Changed delegateFlightStatusChanged = null;
		private DE.ViewNameChanged		delegateViewNameChanged = null;
		private DE.UTC_LocalTimeChanged delegateUTC_LocalTimeChanged = null;
		private DE.CLO_Message			delegateCLO_Message = null;
		private DE.ReloadCompleteOnlineStatuses delegateReloadCompleteOnlineStatuses = null;

		frmDetailStatus myDetailDlg = null;
		frmFind			myFindDlg	= null;
		private frmData DataForm;
		private frmStartup StartForm;
		public int colorArrivalBar = UT.colWhite;
		public int colorDepartureBar = UT.colCyan;
		public bool bmLeft = true;
		public string [] args;
		private IDatabase myDB = null;
		private ITable myAFT = null;
		private ITable myOST = null;
		bool isInit = false;
		string [] arrAftTimeFields;
		/// <summary>
		/// The default arrival bar len (default is 15 minutes)
		/// </summary>
		public int imArrivalBarLen = 15; 
		/// <summary>
		/// The default departure bar len (default is 15 minutes)
		/// </summary>
		public int imDepartureBarLen = 15;
		private frmRules myRulesDialog = null;
		private frmStatusOverview myStatusOverview = null;
		private string omCurrentSelectedUrno = "";
		private bool bmWithBirdView = false;
		#endregion ----- MyRegion

        #region #Region - Windows component declaration
        private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.CheckBox cbOpen;
		private System.Windows.Forms.CheckBox cbReorgGantt;
		private System.Windows.Forms.CheckBox cbNow;
		private System.Windows.Forms.CheckBox cbSetup;
		private System.Windows.Forms.CheckBox cbSearch;
		private System.Windows.Forms.CheckBox cbView;
		private System.Windows.Forms.CheckBox cbLoad;
		private System.Windows.Forms.Panel panel1;
		private AxUGANTTLib.AxUGantt myGantt;
		private System.Windows.Forms.Label txtCalc;


		private System.Windows.Forms.Label lblAboveText;
		private System.Windows.Forms.Button btnDebug;
		private System.Windows.Forms.Button btnOpenVIewPoint;
		private System.Windows.Forms.Button btnEditViewPoint;
		public System.Windows.Forms.ComboBox cbViewPoints;
		private System.Windows.Forms.Button btnNewViewPoint;
		private System.Windows.Forms.Panel panel5;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.PictureBox pictureBox2;
		private System.Windows.Forms.Label lblUTCTime;
		private System.Windows.Forms.Label lblLocalTime;
		private System.Timers.Timer timer1;
		private System.Windows.Forms.CheckBox cbRules;
		private Ufis.Utils.BCBlinker bcBlinker1;
		private System.Windows.Forms.Button btnAbout;
		private System.Windows.Forms.Button btnDeleteViewPoint;
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.CheckBox cbUTC;
		public System.Timers.Timer timerConflictCheck;
		private System.Timers.Timer timerCloseStartup;
		private System.Timers.Timer timerOpacity;
        private AxAATLOGINLib.AxAatLogin LoginControl;
        private Button btnHelp;
		private System.ComponentModel.IContainer components;

#endregion

        private string _helpFile = "";

		public frmMain()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//Read ceda.ini and set it global to UT (UfisTools)
			String strServer="";
			String strHopo = "";
			Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\ceda.ini");
			strServer = myIni.IniReadValue("GLOBAL", "HOSTNAME");
			strHopo = myIni.IniReadValue("GLOBAL", "HOMEAIRPORT");
			UT.ServerName = strServer;
			UT.Hopo = strHopo;
			string strWithBirdView = myIni.IniReadValue("STATUSMANAGER", "BIRDVIEW");
            try
            {
                _helpFile = myIni.IniReadValue("STATUSMANAGER", "HELP_FILE");
            }
            catch (Exception)
            {
                _helpFile = "";
            }
            if (_helpFile == "") { this.btnHelp.Visible = false; }
            else { this.btnHelp.Visible = true; }
			if(strWithBirdView == "TRUE")
			{
				bmWithBirdView = true;
			}
            string traceLog = myIni.IniReadValue("STATUSMANAGER", "TRACE_LOG").Trim().ToUpper();
            if (traceLog == "ON")
            {
                string logFilePath = myIni.IniReadValue("STATUSMANAGER", "LOG_FILE_PATH").Trim();
                if (logFilePath != "") UT.SetLogFilePath(logFilePath);
                UT.SetLogFileNamePrefix("SMLog");
                string startMsg = " Ver:" + Status_Manager.Properties.Resources.internalVer;
                UT.SetLogStartMsg(startMsg);
                UT.StartLogging();
                UT.LogMsg("Status Manager Start: " + startMsg );
            }
            else UT.StopLogging();

			//init BCProxy object
			//myBcProxy = new BcProxyLib.BcPrxy();
			//myBcProxy.OnBcReceive += new BcProxyLib._IBcPrxyEvents_OnBcReceiveEventHandler(BcEvent);
            UT.IsTimeInUtc = false;
            //AM 20080310 - Added In
            DateTime datLocal = DateTime.Now;
            DateTime datUTC = DateTime.UtcNow;//CFC.GetUTC();
            TimeSpan span = datLocal - datUTC;
            string strUTCConvertion = "";
            strUTCConvertion = myIni.IniReadValue("GLOBAL", "UTCCONVERTIONS");
            if(strUTCConvertion == "NO")
            {
                UT.UTCOffset = 0;
            }
            else
            {
                UT.UTCOffset = (short)span.Hours;
            }
		}

		/// <summary>
		/// Initializes the Objects and their settings, such as Gantt, and TabControl
		/// </summary>
		private void MyInit()
		{
            //AM 20080310 - Commented Out
            //DateTime datLocal = DateTime.Now;
            //DateTime datUTC = DateTime.UtcNow;//CFC.GetUTC();
            //TimeSpan span = datLocal - datUTC;
            //string strUTCConvertion = "";
            //IniFile myIni = new IniFile("C:\\Ufis\\system\\ceda.ini");
            //strUTCConvertion = myIni.IniReadValue("GLOBAL", "UTCCONVERTIONS");
            //if(strUTCConvertion == "NO")
            //{
            //    UT.UTCOffset = 0;
            //}
            //else
            //{
            //    UT.UTCOffset = (short)span.Hours;
            //}
            #region #region---Add Delegates
            delegateExceptionOccured = new UT.ExceptionOccured(OnNewExeption);
			UT.OnExceptionOccured  += delegateExceptionOccured;
			delegateRuleEditorClosed = new DE.RuleEditorClosed(frmRules_OnRuleEditorClosed);
			DE.OnRuleEditorClosed += delegateRuleEditorClosed;
			delegateStatusOverviewClosed = new DE.StatusOverviewClosed(frmStatusOverview_OnStatusOverviewClosed);
			DE.OnStatusOverviewClosed += delegateStatusOverviewClosed;
			delegateStatusDetailClosed = new DE.StatusDetailClosed(frmDetailStatus_OnStatusDetailClosed);
			DE.OnStatusDetailClosed += delegateStatusDetailClosed;
			delegateOpenDetailStatusDlg = new DE.OpenDetailStatusDlg(frmStatusOverview_OnOpenDetailStatusDlg);
			DE.OnOpenDetailStatusDlg += delegateOpenDetailStatusDlg;
			delegateFindListClosed = new Status_Manager.DE.FindList_Closed(DE_OnFindList_Closed);
			DE.OnFindList_Closed += delegateFindListClosed;
			delegateNavigateToFlight = new Status_Manager.DE.NavigateToFlight(DE_OnNavigateToFlight);
			DE.OnNavigateToFlight += delegateNavigateToFlight;
			delegateUpdateFlightData = new Status_Manager.DE.UpdateFlightData(DE_OnUpdateFlightData);
			DE.OnUpdateFlightData += delegateUpdateFlightData;
            delegateFlightStatusChanged = new DE.FlightStatus_Changed(DE_OnFlightStatusChanged);
            DE.OnFlightStatus_Changed += delegateFlightStatusChanged;
			delegateViewNameChanged = new Status_Manager.DE.ViewNameChanged(DE_OnViewNameChanged);
			DE.OnViewNameChanged += delegateViewNameChanged;
			delegateUTC_LocalTimeChanged = new Status_Manager.DE.UTC_LocalTimeChanged(DE_OnUTC_LocalTimeChanged);
			DE.OnUTC_LocalTimeChanged += delegateUTC_LocalTimeChanged;
			delegateCLO_Message = new Status_Manager.DE.CLO_Message(DE_OnCLO_Message);
			DE.OnCLO_Message += delegateCLO_Message;
			delegateReloadCompleteOnlineStatuses = new Status_Manager.DE.ReloadCompleteOnlineStatuses(DE_OnReloadCompleteOnlineStatuses);
			DE.OnReloadCompleteOnlineStatuses += delegateReloadCompleteOnlineStatuses;
            #endregion
            InitGantt();
			myAFT = myDB["AFT"];
			myOST = myDB["OST"];
			CFC.MyInit(DataForm.tabViews);
			CFC.CurrentViewName = "<DEFAULT>";
		}

		public void InitGantt()
		{
			myGantt.ResetContent();
			myGantt.TabHeaderLengthString = "100";   //init the width of the tab-columns
			myGantt.TabSetHeaderText ("All Flights");//set the tab-headlines
			myGantt.SplitterPosition = 100;          //the splitterposition depends on the
			myGantt.TabBodyFontName = "Arial";
			myGantt.TabFontSize = 14;								
			myGantt.TabHeaderFontName = "Arial";
			myGantt.TabHeaderFontSize = 14;
			myGantt.BarOverlapOffset = 10;
			myGantt.BarNumberExpandLine = 0;
			myGantt.TabBodyFontBold = true;
			myGantt.TabHeaderFontBold = true;
			myGantt.LifeStyle = true;
			//Calc the earliest and latest flight to get the timeframe for gantt
			DateTime datFrom = UT.TimeFrameFrom;
			DateTime datTo = UT.TimeFrameTo;
			DateTime datTmp;
			myAFT = myDB["AFT"];
			for(int i = 0; i < myAFT.Count; i++)
			{
				if(myAFT[i]["ADID"] == "A")
				{
					datTmp = UT.CedaFullDateToDateTime(myAFT[i]["TIFA"]);
					if(datFrom > datTmp)
						datFrom = datTmp;
				}
				if(myAFT[i]["ADID"] == "D")
				{
					datTmp = UT.CedaFullDateToDateTime(myAFT[i]["TIFD"]);
					if(datTo > datTmp)
						datTo = datTmp;
				}
			}
			if(datFrom < UT.TimeFrameFrom)
			{
				myGantt.TimeFrameFrom = datFrom.Subtract(new TimeSpan(0, 5, 0, 0, 0));
			}
			else
			{
				myGantt.TimeFrameFrom = UT.TimeFrameFrom; // CedaFullDateToVb(strTimeFrameFrom)  'dateFrom
			}
			if(datTo > UT.TimeFrameTo)
			{
				myGantt.TimeFrameTo = datTo.AddHours(5);
			}
			else
			{
				myGantt.TimeFrameTo = UT.TimeFrameTo;  // CedaFullDateToVb(strTimeFrameTo)
			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{

				if(DataForm != null)
				{
					DataForm.Dispose();
				}
				if (components != null) 
				{
					components.Dispose();
				}

				UT.DisposeMemDB();

			}
			base.Dispose( disposing );
        }

        #region #region Windows Form Designer generated code
        /// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.panelTop = new System.Windows.Forms.Panel();
            this.LoginControl = new AxAATLOGINLib.AxAatLogin();
            this.cbUTC = new System.Windows.Forms.CheckBox();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnAbout = new System.Windows.Forms.Button();
            this.cbRules = new System.Windows.Forms.CheckBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.bcBlinker1 = new Ufis.Utils.BCBlinker();
            this.lblLocalTime = new System.Windows.Forms.Label();
            this.btnDeleteViewPoint = new System.Windows.Forms.Button();
            this.lblUTCTime = new System.Windows.Forms.Label();
            this.btnOpenVIewPoint = new System.Windows.Forms.Button();
            this.cbViewPoints = new System.Windows.Forms.ComboBox();
            this.btnNewViewPoint = new System.Windows.Forms.Button();
            this.btnEditViewPoint = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnDebug = new System.Windows.Forms.Button();
            this.lblAboveText = new System.Windows.Forms.Label();
            this.cbOpen = new System.Windows.Forms.CheckBox();
            this.cbReorgGantt = new System.Windows.Forms.CheckBox();
            this.cbNow = new System.Windows.Forms.CheckBox();
            this.cbSetup = new System.Windows.Forms.CheckBox();
            this.cbSearch = new System.Windows.Forms.CheckBox();
            this.cbView = new System.Windows.Forms.CheckBox();
            this.cbLoad = new System.Windows.Forms.CheckBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtCalc = new System.Windows.Forms.Label();
            this.myGantt = new AxUGANTTLib.AxUGantt();
            this.timer1 = new System.Timers.Timer();
            this.timerConflictCheck = new System.Timers.Timer();
            this.timerCloseStartup = new System.Timers.Timer();
            this.timerOpacity = new System.Timers.Timer();
            this.btnHelp = new System.Windows.Forms.Button();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LoginControl)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.myGantt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timer1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timerConflictCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timerCloseStartup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timerOpacity)).BeginInit();
            this.SuspendLayout();
            // 
            // panelTop
            // 
            this.panelTop.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelTop.Controls.Add(this.btnHelp);
            this.panelTop.Controls.Add(this.LoginControl);
            this.panelTop.Controls.Add(this.cbUTC);
            this.panelTop.Controls.Add(this.btnAbout);
            this.panelTop.Controls.Add(this.cbRules);
            this.panelTop.Controls.Add(this.panel2);
            this.panelTop.Controls.Add(this.btnDebug);
            this.panelTop.Controls.Add(this.lblAboveText);
            this.panelTop.Controls.Add(this.cbOpen);
            this.panelTop.Controls.Add(this.cbReorgGantt);
            this.panelTop.Controls.Add(this.cbNow);
            this.panelTop.Controls.Add(this.cbSetup);
            this.panelTop.Controls.Add(this.cbSearch);
            this.panelTop.Controls.Add(this.cbView);
            this.panelTop.Controls.Add(this.cbLoad);
            this.panelTop.Controls.Add(this.pictureBox1);
            resources.ApplyResources(this.panelTop, "panelTop");
            this.panelTop.Name = "panelTop";
            // 
            // LoginControl
            // 
            resources.ApplyResources(this.LoginControl, "LoginControl");
            this.LoginControl.Name = "LoginControl";
            this.LoginControl.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("LoginControl.OcxState")));
            // 
            // cbUTC
            // 
            resources.ApplyResources(this.cbUTC, "cbUTC");
            this.cbUTC.BackColor = System.Drawing.Color.Transparent;
            this.cbUTC.ImageList = this.imageList1;
            this.cbUTC.Name = "cbUTC";
            this.cbUTC.UseVisualStyleBackColor = false;
            this.cbUTC.CheckedChanged += new System.EventHandler(this.cbUTC_CheckedChanged);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.White;
            this.imageList1.Images.SetKeyName(0, "");
            this.imageList1.Images.SetKeyName(1, "");
            this.imageList1.Images.SetKeyName(2, "");
            this.imageList1.Images.SetKeyName(3, "");
            this.imageList1.Images.SetKeyName(4, "");
            this.imageList1.Images.SetKeyName(5, "");
            this.imageList1.Images.SetKeyName(6, "");
            this.imageList1.Images.SetKeyName(7, "");
            this.imageList1.Images.SetKeyName(8, "");
            this.imageList1.Images.SetKeyName(9, "");
            this.imageList1.Images.SetKeyName(10, "");
            this.imageList1.Images.SetKeyName(11, "");
            this.imageList1.Images.SetKeyName(12, "");
            this.imageList1.Images.SetKeyName(13, "");
            this.imageList1.Images.SetKeyName(14, "");
            this.imageList1.Images.SetKeyName(15, "");
            this.imageList1.Images.SetKeyName(16, "");
            // 
            // btnAbout
            // 
            this.btnAbout.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnAbout, "btnAbout");
            this.btnAbout.ImageList = this.imageList1;
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.UseVisualStyleBackColor = false;
            this.btnAbout.Click += new System.EventHandler(this.btnAbout_Click);
            // 
            // cbRules
            // 
            resources.ApplyResources(this.cbRules, "cbRules");
            this.cbRules.BackColor = System.Drawing.Color.Transparent;
            this.cbRules.ImageList = this.imageList1;
            this.cbRules.Name = "cbRules";
            this.cbRules.UseVisualStyleBackColor = false;
            this.cbRules.CheckedChanged += new System.EventHandler(this.cbRules_CheckedChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.bcBlinker1);
            this.panel2.Controls.Add(this.lblLocalTime);
            this.panel2.Controls.Add(this.btnDeleteViewPoint);
            this.panel2.Controls.Add(this.lblUTCTime);
            this.panel2.Controls.Add(this.btnOpenVIewPoint);
            this.panel2.Controls.Add(this.cbViewPoints);
            this.panel2.Controls.Add(this.btnNewViewPoint);
            this.panel2.Controls.Add(this.btnEditViewPoint);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.pictureBox2);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // bcBlinker1
            // 
            this.bcBlinker1.BackColor = System.Drawing.SystemColors.Control;
            resources.ApplyResources(this.bcBlinker1, "bcBlinker1");
            this.bcBlinker1.Name = "bcBlinker1";
            this.bcBlinker1.Paint += new System.Windows.Forms.PaintEventHandler(this.bcBlinker1_Paint);
            // 
            // lblLocalTime
            // 
            this.lblLocalTime.BackColor = System.Drawing.Color.Silver;
            this.lblLocalTime.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            resources.ApplyResources(this.lblLocalTime, "lblLocalTime");
            this.lblLocalTime.ForeColor = System.Drawing.Color.Black;
            this.lblLocalTime.Name = "lblLocalTime";
            // 
            // btnDeleteViewPoint
            // 
            this.btnDeleteViewPoint.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnDeleteViewPoint, "btnDeleteViewPoint");
            this.btnDeleteViewPoint.ImageList = this.imageList1;
            this.btnDeleteViewPoint.Name = "btnDeleteViewPoint";
            this.btnDeleteViewPoint.UseVisualStyleBackColor = false;
            this.btnDeleteViewPoint.Click += new System.EventHandler(this.btnDeleteViewPoint_Click);
            // 
            // lblUTCTime
            // 
            this.lblUTCTime.BackColor = System.Drawing.Color.Silver;
            this.lblUTCTime.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            resources.ApplyResources(this.lblUTCTime, "lblUTCTime");
            this.lblUTCTime.ForeColor = System.Drawing.Color.Red;
            this.lblUTCTime.Name = "lblUTCTime";
            // 
            // btnOpenVIewPoint
            // 
            this.btnOpenVIewPoint.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnOpenVIewPoint, "btnOpenVIewPoint");
            this.btnOpenVIewPoint.ImageList = this.imageList1;
            this.btnOpenVIewPoint.Name = "btnOpenVIewPoint";
            this.btnOpenVIewPoint.UseVisualStyleBackColor = false;
            this.btnOpenVIewPoint.Click += new System.EventHandler(this.btnOpenVIewPoint_Click);
            // 
            // cbViewPoints
            // 
            this.cbViewPoints.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cbViewPoints, "cbViewPoints");
            this.cbViewPoints.Name = "cbViewPoints";
            this.cbViewPoints.Sorted = true;
            // 
            // btnNewViewPoint
            // 
            this.btnNewViewPoint.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnNewViewPoint, "btnNewViewPoint");
            this.btnNewViewPoint.ImageList = this.imageList1;
            this.btnNewViewPoint.Name = "btnNewViewPoint";
            this.btnNewViewPoint.UseVisualStyleBackColor = false;
            this.btnNewViewPoint.Click += new System.EventHandler(this.btnNewViewPoint_Click);
            // 
            // btnEditViewPoint
            // 
            this.btnEditViewPoint.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnEditViewPoint, "btnEditViewPoint");
            this.btnEditViewPoint.ImageList = this.imageList1;
            this.btnEditViewPoint.Name = "btnEditViewPoint";
            this.btnEditViewPoint.UseVisualStyleBackColor = false;
            this.btnEditViewPoint.Click += new System.EventHandler(this.btnEditViewPoint_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.panel5, "panel5");
            this.panel5.Name = "panel5";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.pictureBox2, "pictureBox2");
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox2_Paint);
            // 
            // btnDebug
            // 
            this.btnDebug.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnDebug, "btnDebug");
            this.btnDebug.Name = "btnDebug";
            this.btnDebug.UseVisualStyleBackColor = false;
            this.btnDebug.Click += new System.EventHandler(this.btnDebug_Click);
            // 
            // lblAboveText
            // 
            this.lblAboveText.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblAboveText, "lblAboveText");
            this.lblAboveText.ForeColor = System.Drawing.Color.Aqua;
            this.lblAboveText.Name = "lblAboveText";
            // 
            // cbOpen
            // 
            resources.ApplyResources(this.cbOpen, "cbOpen");
            this.cbOpen.BackColor = System.Drawing.Color.Transparent;
            this.cbOpen.ImageList = this.imageList1;
            this.cbOpen.Name = "cbOpen";
            this.cbOpen.UseVisualStyleBackColor = false;
            this.cbOpen.CheckedChanged += new System.EventHandler(this.cbOpen_CheckedChanged);
            // 
            // cbReorgGantt
            // 
            resources.ApplyResources(this.cbReorgGantt, "cbReorgGantt");
            this.cbReorgGantt.BackColor = System.Drawing.Color.Transparent;
            this.cbReorgGantt.ImageList = this.imageList1;
            this.cbReorgGantt.Name = "cbReorgGantt";
            this.cbReorgGantt.UseVisualStyleBackColor = false;
            this.cbReorgGantt.CheckedChanged += new System.EventHandler(this.cbReorgGantt_CheckedChanged);
            // 
            // cbNow
            // 
            resources.ApplyResources(this.cbNow, "cbNow");
            this.cbNow.BackColor = System.Drawing.Color.Transparent;
            this.cbNow.ImageList = this.imageList1;
            this.cbNow.Name = "cbNow";
            this.cbNow.UseVisualStyleBackColor = false;
            this.cbNow.CheckedChanged += new System.EventHandler(this.cbNow_CheckedChanged);
            // 
            // cbSetup
            // 
            resources.ApplyResources(this.cbSetup, "cbSetup");
            this.cbSetup.BackColor = System.Drawing.Color.Transparent;
            this.cbSetup.ImageList = this.imageList1;
            this.cbSetup.Name = "cbSetup";
            this.cbSetup.UseVisualStyleBackColor = false;
            // 
            // cbSearch
            // 
            resources.ApplyResources(this.cbSearch, "cbSearch");
            this.cbSearch.BackColor = System.Drawing.Color.Transparent;
            this.cbSearch.ImageList = this.imageList1;
            this.cbSearch.Name = "cbSearch";
            this.cbSearch.UseVisualStyleBackColor = false;
            this.cbSearch.CheckedChanged += new System.EventHandler(this.cbSearch_CheckedChanged);
            // 
            // cbView
            // 
            resources.ApplyResources(this.cbView, "cbView");
            this.cbView.BackColor = System.Drawing.Color.Transparent;
            this.cbView.ImageList = this.imageList1;
            this.cbView.Name = "cbView";
            this.cbView.UseVisualStyleBackColor = false;
            this.cbView.CheckedChanged += new System.EventHandler(this.cbView_CheckedChanged);
            // 
            // cbLoad
            // 
            resources.ApplyResources(this.cbLoad, "cbLoad");
            this.cbLoad.BackColor = System.Drawing.Color.Transparent;
            this.cbLoad.ImageList = this.imageList1;
            this.cbLoad.Name = "cbLoad";
            this.cbLoad.UseVisualStyleBackColor = false;
            this.cbLoad.CheckedChanged += new System.EventHandler(this.cbLoad_CheckedChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtCalc);
            this.panel1.Controls.Add(this.myGantt);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // txtCalc
            // 
            this.txtCalc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtCalc.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            resources.ApplyResources(this.txtCalc, "txtCalc");
            this.txtCalc.ForeColor = System.Drawing.Color.Blue;
            this.txtCalc.Name = "txtCalc";
            // 
            // myGantt
            // 
            resources.ApplyResources(this.myGantt, "myGantt");
            this.myGantt.Name = "myGantt";
            this.myGantt.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("myGantt.OcxState")));
            this.myGantt.OnLButtonDownBar += new AxUGANTTLib._DUGanttEvents_OnLButtonDownBarEventHandler(this.myGantt_OnLButtonDownBar);
            this.myGantt.ActualTimescaleDate += new AxUGANTTLib._DUGanttEvents_ActualTimescaleDateEventHandler(this.myGantt_ActualTimescaleDate);
            this.myGantt.ActualLineNo += new AxUGANTTLib._DUGanttEvents_ActualLineNoEventHandler(this.myGantt_ActualLineNo);
            this.myGantt.OnLButtonDblClkBar += new AxUGANTTLib._DUGanttEvents_OnLButtonDblClkBarEventHandler(this.myGantt_OnLButtonDblClkBar);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 60000;
            this.timer1.SynchronizingObject = this;
            this.timer1.Elapsed += new System.Timers.ElapsedEventHandler(this.timer1_Elapsed);
            // 
            // timerConflictCheck
            // 
            this.timerConflictCheck.Enabled = true;
            this.timerConflictCheck.Interval = 60000;
            this.timerConflictCheck.SynchronizingObject = this;
            this.timerConflictCheck.Elapsed += new System.Timers.ElapsedEventHandler(this.timerConflictCheck_Elapsed);
            // 
            // timerCloseStartup
            // 
            this.timerCloseStartup.Interval = 300;
            this.timerCloseStartup.SynchronizingObject = this;
            this.timerCloseStartup.Elapsed += new System.Timers.ElapsedEventHandler(this.timerCloseStartup_Elapsed);
            // 
            // timerOpacity
            // 
            this.timerOpacity.Interval = 75;
            this.timerOpacity.SynchronizingObject = this;
            // 
            // btnHelp
            // 
            this.btnHelp.BackColor = System.Drawing.Color.LightGray;
            resources.ApplyResources(this.btnHelp, "btnHelp");
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.UseVisualStyleBackColor = false;
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // frmMain
            // 
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelTop);
            this.Name = "frmMain";
            this.Closed += new System.EventHandler(this.frmMain_Closed);
            this.Resize += new System.EventHandler(this.frmMain_Resize);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.frmMain_Closing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.panelTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LoginControl)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.myGantt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timer1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timerConflictCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timerCloseStartup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timerOpacity)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string [] args) 
		{
			System.Diagnostics.Process pThis = System.Diagnostics.Process.GetCurrentProcess();

			foreach(System.Diagnostics.Process p in System.Diagnostics.Process.GetProcesses())
			{
				if(p.Id != pThis.Id && p.ProcessName == pThis.ProcessName)
				{
					MessageBox.Show("The Status Manager is already running and cannot be started twice!");

					return;
				}
			}
			frmMain olDlg = new frmMain();
			olDlg.args = args;
			Application.Run(olDlg);
        }

        #region #region Paint Metal Effect
        private void pictureBox1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			if(timerOpacity.Enabled == false)
				UT.DrawGradient(e.Graphics, 90f, pictureBox1, Color.WhiteSmoke , Color.DarkGray  );
		}
		private void pictureBox2_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			if(timerOpacity.Enabled == false)
				UT.DrawGradient(e.Graphics, 90f, pictureBox2, Color.WhiteSmoke , Color.DarkGray  );
		}


		#endregion Paint Metal Effect

		private void BcEvent(string pReqId, string pDest1, string pDest2,string pCmd, string pObject, string pSeq,string pTws, string pTwe, string pSelection, string pFields, string pData, string pBcNum)
		{
			bcBlinker1.Blink();
		}
		private void frmMain_Resize(object sender, System.EventArgs e)
		{
		}

		private void frmMain_Load(object sender, System.EventArgs e)
		{
            //TestSortedList();
            UT.LogMsg("Status Manager Starting");
			CFC.LoginControl = LoginControl;       

			myDB = UT.GetMemDB();
			bool result = myDB.Connect(UT.ServerName,UT.UserName,UT.Hopo,"TAB","STATMGR");
            myDB.Multithreaded = true;
			delegateBirdViewSaved = new DE.BirdViewSaved(OnBirdViewSaved);
			DE.OnBirdViewSaved += delegateBirdViewSaved;

			Ufis.Utils.IniFile ini = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			string strHiddenDataShow  = ini.IniReadValue("GLOBAL", "HIDDENDATA");

			timerConflictCheck.Interval = CFC.TimerIntervalConflicts * 6 * 10000;
			string strTimeFields = "STOA,STOD,TIFA,TIFD,TMOA,ETAI,ETDI,ONBL,OFBL,PABS,PAES,PDBS,PDES,PDBA,PDEA";
			arrAftTimeFields = strTimeFields.Split(',');
			System.Windows.Forms.DialogResult olResult;
			if(args.Length == 0) 
			{
				if(LoginProcedure() == false)
				{
					Application.Exit();
					return;
				}
			}
			else
			{
                if (args[0] != "/CONNECTED")
				{
					if(LoginProcedure() == false)
					{
						Application.Exit();
						return;
					}
				}	
			}

			HandleToolbar_BDPS_SEC();
			string theKey = "Software\\StatusManager";
			RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			if(rk == null)
			{
				rk = Registry.CurrentUser.OpenSubKey("Software", true);
				theKey = "StatusManager";
				rk.CreateSubKey(theKey);
				rk.Close();
			}
	
			string strUsr = LoginControl.GetUserName();
			if(strUsr == "")
				strUsr="Default";
			UT.UserName = LoginControl.GetUserName();//"Default";
			DataForm = new frmData();
			//MWO: DataForm.Show() is necessary to ensure, that all tabs are available !!!!!
			DataForm.Show();

			if(args.Length == 0 || strHiddenDataShow != "TRUE")
			{
				DataForm.Hide();
			}
			else if(args[0] != "/CONNECTED")
			{
				DataForm.Hide();
			} 
			else
			{
				DataForm.Show();
			}
			
			StartForm = new frmStartup();
			StartForm.Show();
			frmLoad olLoad = new frmLoad();
			this.lblAboveText.BackColor = Color.Transparent;
			//olLoad.Parent = StartForm;
			olResult = olLoad.ShowDialog();
			if (olResult == DialogResult.OK)
			{
                SetLoadTimeFrame(olLoad);

				DataForm.LoadData();

				MyInit();
				DE.InitDBObjects(bcBlinker1);

				StartForm.txtCurrentStatus.Text = "Allocating flights to gantt-chart";
				StartForm.txtCurrentStatus.Refresh();
				StartForm.txtDoneStatus.Refresh();

				StartForm.BringToFront();
                ShowScreenHeading();
			}
			else
			{
				Application.Exit();
			}
			lblAboveText.Parent = pictureBox1;
			cbLoad.Parent = pictureBox1;
			cbNow.Parent = pictureBox1;
			cbOpen.Parent = pictureBox1;
			cbReorgGantt.Parent = pictureBox1;
			cbSearch.Parent = pictureBox1;
			cbSetup.Parent = pictureBox1;
			cbView.Parent = pictureBox1;
			cbUTC.Parent = pictureBox1;
            //cbUTC.Checked = true;//AM 20080310
			cbRules.Parent = pictureBox1;
			btnAbout.Parent = pictureBox1;
			btnDebug.Parent = pictureBox1;
			btnEditViewPoint.Parent = pictureBox2;
			btnNewViewPoint.Parent = pictureBox2;
			btnOpenVIewPoint.Parent = pictureBox2;
			btnDeleteViewPoint.Parent = pictureBox2;
			lblUTCTime.Parent = pictureBox2;
			lblLocalTime.Parent = pictureBox2;
			ReloadCombo();
			this.Top = 0;
			this.Left = 0;
			this.Width = Screen.PrimaryScreen.WorkingArea.Width;
			DateTime localTime = DateTime.Now;
			DateTime utcTime = CFC.GetUTC();
			TimeSpan olTS = localTime - utcTime;
			lblUTCTime.Text = utcTime.ToShortDateString() + " " 
                + utcTime.ToShortTimeString() + "z";
			lblLocalTime.Text = localTime.ToShortDateString() + " " 
                + localTime.ToShortTimeString();
			//myGantt.UTCOffsetInHours = (short)-olTS.TotalHours;

            //AM 20080310 - Start
            //string strUTCConvertion = "";
            //IniFile myIni = new IniFile("C:\\Ufis\\system\\ceda.ini");
            //strUTCConvertion = myIni.IniReadValue("GLOBAL", "UTCCONVERTIONS");
            //if(strUTCConvertion == "NO")
            //{
            //    myGantt.UTCOffsetInHours = 0;
            //}
            //else
            //{
            //    myGantt.UTCOffsetInHours = (short)-olTS.TotalHours;
            //}
            //AM 20080310 - End

			ReadRegistry();

			isInit = true;
            if (this.cbUTC.Checked)
            {
                this.cbUTC.Checked = false;//AM 20080310
                //no need to call HandleUTC. It will be triggered from UTC Check Change event
            }
            else
            {
                HandleUTC();//AM 20080310
            }
            //UTCToLocal();//AM 20080310
            //if (UT.IsTimeInUtc == true)
            //{
            //    TimeSpan olSpan = new TimeSpan(0, 1, 0, 0, 0);
            //    localTime = utcTime.Subtract(olSpan);
            //}
            //if (localTime < UT.TimeFrameFrom || localTime > UT.TimeFrameTo)
            //{
            //    myGantt.ScrollTo(UT.TimeFrameFrom);
            //}
            //else
            //{
            //    myGantt.ScrollTo(localTime);
            //}
			timerCloseStartup.Enabled = true;

			// if this application does not include BirdView 
			// functionality => switch it of and hide the buttons
			if(bmWithBirdView == false)
			{
				btnNewViewPoint.Hide();
				btnEditViewPoint.Hide();
				btnDeleteViewPoint.Hide();
				cbViewPoints.Hide();
				btnOpenVIewPoint.Hide();
				panel5.Hide();
			}
			ScrollToNow();
		}

        private void ShowScreenHeading()
        {
            if (UT.IsTimeInUtc)
            {
                this.Text = "Status Manager: UTC Timeframe [" + UT.TimeFrameFrom.ToString()
                        + "] - [" + UT.TimeFrameTo.ToString() + "]" + " --- Server: "
                        + UT.ServerName + " --- Active View: [" + CFC.CurrentViewName + "]";
            }
            else
            {
                this.Text = "Status Manager: Local Timeframe [" + UT.UtcToLocal(UT.TimeFrameFrom).ToString()
                        + "] - [" + UT.UtcToLocal(UT.TimeFrameTo).ToString() + "]" + " --- Server: "
                        + UT.ServerName + " --- Active View: [" + CFC.CurrentViewName + "]";
            }
        }

        /// <summary>
        /// Prepare the toolbar according to the Privileges of the Logined User
        /// </summary>
		private void HandleToolbar_BDPS_SEC()
		{
			string prv = LoginControl.GetPrivileges("Load");
			if(prv == "-" || prv == "0")
			{
				cbLoad.Enabled = false;
			}
			prv = LoginControl.GetPrivileges("View");
			if(prv == "-" || prv == "0")
			{
				cbView.Enabled = false;
			}
			prv = LoginControl.GetPrivileges("Find");
			if(prv == "-" || prv == "0")
			{
				cbSearch.Enabled = false;
			}
			prv = LoginControl.GetPrivileges("Rules");
			if(prv == "-" || prv == "0")
			{
				cbRules.Enabled = false;
			}
			prv = LoginControl.GetPrivileges("UTC");
			if(prv == "-" || prv == "0")
			{
				cbUTC.Enabled = false;
			}
			prv = LoginControl.GetPrivileges("Overview");
			if(prv == "-" || prv == "0")
			{
				cbOpen.Enabled = false;
			}
			prv = LoginControl.GetPrivileges("NewViewpoint");
			if(prv == "-" || prv == "0")
			{
				btnNewViewPoint.Enabled = false;
			}
			prv = LoginControl.GetPrivileges("EditViewpoint");
			if(prv == "-" || prv == "0")
			{
				btnEditViewPoint.Enabled = false;
			}
			prv = LoginControl.GetPrivileges("DeleteViewpoint");
			if(prv == "-" || prv == "0")
			{
				btnDeleteViewPoint.Enabled = false;
			}
		}

        /// <summary>
        /// Login Procedure
        /// </summary>
        /// <returns>Successful Login</returns>
		private bool LoginProcedure()
		{
			string LoginAnsw = "";
			string tmpRegStrg = "";
			string strRet = "";
			//LoginControl.UfisComCtrl = UT.GetUfisCom();
			LoginControl.ApplicationName = "StatusManager";
			LoginControl.InfoCaption = "StatusManager";
			LoginControl.VersionString = Application.ProductVersion;// "4.5.0.1"; //GetApplVersion(True);
			LoginControl.InfoCaption = "Info about Status Manager";
			LoginControl.InfoButtonVisible = true;
			LoginControl.InfoUfisVersion = "Ufis Version 4.5";
			LoginControl.InfoAppVersion = "Status Manager " + Application.ProductVersion.ToString();
			LoginControl.InfoAAT = "UFIS Airport Solutions GmbH";
			LoginControl.UserNameLCase = true;
			tmpRegStrg = "StatusManager" + ",";
			//FKTTAB:  SUBD,FUNC,FUAL,TYPE,STAT
			tmpRegStrg += "InitModu,InitModu,Initialisieren (InitModu),B,-";
			tmpRegStrg += ",Load,Load,Reload Data,B,-";
			tmpRegStrg += ",View,View,View Definition,B,-";
			tmpRegStrg += ",Find,Find,Find Flight(s),B,-";
			tmpRegStrg += ",Rules,Rules,Edit Rules,B,-";
			tmpRegStrg += ",UTC,UTC,Switch Local/UTC,B,-";
			tmpRegStrg += ",Overview,Overview,Open Status-Overview,B,-";
			tmpRegStrg += ",NewViewpoint,NewViewpoint,New Viewpoint,B,-";
			tmpRegStrg += ",EditViewpoint,EditViewpoint,Edit Viewpoint,B,-";
			tmpRegStrg += ",DeleteViewpoint,DeleteViewpoint,Delete Viewpoint,B,-";
			tmpRegStrg += ",EditDetailStatus,EditDetailStatus,Edit Status Information,B,-";
			tmpRegStrg += ",CheckListProcessing,CheckListProcessing,Call Check List Processing,B,-";
			tmpRegStrg += ",CheckListProcessingSave,CheckListProcessingSave,Save,B,-";
			tmpRegStrg += ",CheckListProcessingEdit,CheckListProcessingEdit,Edit Basic Data,B,-";
			tmpRegStrg += ",CheckListProcessingPrint,CheckListProcessingPrint,Print,B,-";
			tmpRegStrg += ",CheckListProcessingTimeEdit,CheckListProcessingTimeEdit,Time Edit,B,-";

			//Append any other Privileges
			//tmpRegStrg = tmpRegStrg & ",Entry,Entry,Entry,F,1"
			//tmpRegStrg = tmpRegStrg & ",Feature,Feature,Feature,F,1"
			LoginControl.RegisterApplicationString = tmpRegStrg;
			strRet = LoginControl.ShowLoginDialog();

			//string strLoginRet = LoginControl.DoLoginSilentMode(usr, pwd);
			//strLoginRet must be "OK"
			LoginAnsw = LoginControl.GetPrivileges("InitModu");
			UT.UserName = LoginControl.GetUserName();
			if( LoginAnsw != "" && strRet != "CANCEL" )
				return true; 
			else  
				return false;
		}


        /// <summary>
        /// Read the Registry to get the Form Location and size parameter
        ///    and then set it.
        /// </summary>
		private void ReadRegistry()
		{
			int myX=-1, myY=-1, myW=-1, myH=-1;
			string regVal = "";
			string theKey = "Software\\StatusManager\\MainWindow";
			RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			if(rk != null)
			{
				regVal = rk.GetValue("X", "-1").ToString();
				myX = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Y", "-1").ToString();
				myY = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Width", "-1").ToString();
				myW = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Height", "-1").ToString();
				myH = Convert.ToInt32(regVal);
			}
			if((myW != -1 && myH != -1) && (myX < Screen.PrimaryScreen.Bounds.Width))
			{
				this.Left = myX;
				this.Top = myY;
				this.Width = myW;
				this.Height = myH;
			}
		}

		/// <summary>
		/// Assigns the flights to the gantt chart with maximized density as possible
		/// to ensure, that as much flight bars as possible are visible to get max
		/// overview
		/// </summary>
        public void AllocateFlights()
        {
            int i = 0;
            DateTime d;
            DateTime datTT;
            DateTime myGanttTimeFrom = UT.TimeFrameFrom;
            DateTime myGanttTimeTo = UT.TimeFrameTo;
            UT.IsTimeInUtc = myAFT.TimeFieldsCurrentlyInUtc;
            this.cbUTC.Checked = UT.IsTimeInUtc;
            if (!UT.IsTimeInUtc)
            {
                myGanttTimeFrom = UT.UtcToLocal(myGanttTimeFrom);
                myGanttTimeTo = UT.UtcToLocal(myGanttTimeTo);
            }
            ShowScreenHeading();//AM 20080310 - To change the heading according to UTC and Local

            InitGantt();
            txtCalc.Visible = true;
            txtCalc.BringToFront();
            txtCalc.Text = "Please wait while calculating ...";
            txtCalc.Refresh();
            myGantt.TimeScaleDuration = 12;

            myGantt.ScrollTo(UT.TimeFrameFrom);
            datTT = UT.TimeFrameTo;
            datTT = datTT.AddDays(1000);
            for (i = 0; i <= 200; i++)
            {
                int ilText;
                ilText = i + 1;
                myGantt.AddLineAt(i, i.ToString(), ilText.ToString());
                myGantt.SetLineSeparator(i, 1, UT.colBlack, 2, 1, 0);
            }
            if (UT.IsTimeInUtc == true)
                myGantt.UTCOffsetInHours = (short)-UT.UTCOffset;
            else
                myGantt.UTCOffsetInHours = 0;
            this.Cursor = Cursors.WaitCursor;

            Allocator myAllocator = new Allocator(200, myGantt.TimeFrameFrom, datTT);
            //THE ALLOCATION
            try
            {
                UT.LogMsg("frmMain:AllocateFlights: b4 AFT AcqReader Lock");
                myAFT.Lock.AcquireReaderLock(System.Threading.Timeout.Infinite);
                UT.LogMsg("frmMain:AllocateFlights: af AFT AcqReader Lock");
                #region myAFT allocation
                myAFT.Sort("TIFA,TIFD", true);
                bool isDataUtc = myAFT.TimeFieldsCurrentlyInUtc;
                for (i = 0; i < myAFT.Count; i++)
                {
                    DateTime barStart = DateTime.Now;
                    DateTime barEnd = DateTime.Now;
                    
                    if (myAFT[i]["ADID"] == "A")
                    {
                        barStart = UT.CedaFullDateToDateTime(myAFT[i]["TIFA"]);
                        barEnd = barStart.AddMinutes((double)imArrivalBarLen);
                    }
                    else if (myAFT[i]["ADID"] == "D")
                    {
                        barEnd = UT.CedaFullDateToDateTime(myAFT[i]["TIFD"]);
                        barStart = barEnd.Subtract(new TimeSpan(0, 0, imDepartureBarLen, 0, 0));
                    }
                    string strF = myAFT[i]["FLNO"];
                    if (strF == "LH 3381" || strF == "MDF7704" || strF == "A3 530")
                    {
                        strF = "";
                    }
                    int gLine = myAllocator.GetLineForTimeframe(barStart, barEnd);
                    if (gLine > -1)
                    {
                        UpdateBarForLine(myAFT[i]["URNO"], gLine, false, false);
                    }
                    else
                    {
                        //MessageBox.Show(myAFT[i]["FLNO"]);
                    }
                    if (i % 100 == 0)
                    {
                        TimeSpan olSpan2 = new TimeSpan(0, 2, 0, 0, 0);
                        d = barStart.Subtract(olSpan2);
                        myGantt.ScrollTo(d);
                        lblAboveText.Text = d.ToShortDateString();
                        lblAboveText.Refresh();
                        myGantt.Refresh();
                    }
                }

                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error due to [" + ex.Message + Environment.NewLine + ex.StackTrace, "Error");
            }
            finally
            {
                UT.LogMsg("frmMain:AllocateFlights: b4 AFT RelReader Lock");
                if (myAFT.Lock.IsReaderLockHeld) myAFT.Lock.ReleaseReaderLock();
                UT.LogMsg("frmMain:AllocateFlights: af AFT RelReader Lock");
            }
            txtCalc.Visible = false;
            myAllocator.Clear();
            //END ALLOCATION
            d = DateTime.Now;
            DateTime datUTC = CFC.GetUTC();
            TimeSpan olTS = d - datUTC;
            if (UT.IsTimeInUtc == true && UT.UTCOffset != 0)
            {
                TimeSpan olSpan = new TimeSpan(0, 1, 0, 0, 0);
                d = datUTC.Subtract(olSpan);
            }
            else
            {
                TimeSpan olSpan = new TimeSpan(0, 1, 0, 0, 0);
                d = d.Subtract(olSpan);
            }
            if (d < UT.TimeFrameFrom || d > UT.TimeFrameTo)
                myGantt.ScrollTo(UT.TimeFrameFrom);
            else
                myGantt.ScrollTo(d);
            lblAboveText.Text = d.ToShortDateString();
            lblAboveText.Refresh();
            myGantt.TimeScaleDuration = 4;
            if (UT.IsTimeInUtc)
            {
                myGantt.TimeFrameFrom = UT.TimeFrameFrom;
                myGantt.TimeFrameTo = UT.TimeFrameTo;
            }
            else
            {
                myGantt.TimeFrameFrom = UT.UtcToLocal( UT.TimeFrameFrom );
                myGantt.TimeFrameTo = UT.UtcToLocal(UT.TimeFrameTo);
            }
            myGantt.Refresh();
            this.Cursor = Cursors.Arrow;
        }//END public void AllocateFlights()

 		/// <summary>
		/// Reloads the combobox for Viewpoint Definitions
		/// </summary>
		public void ReloadCombo()
		{
			cbViewPoints.Items.Clear();
			for(int i = 0; i < DataForm.tabVPD.GetLineCount(); i++)
			{
				cbViewPoints.Items.Add(DataForm.tabVPD.GetFieldValue(i, "VIEW"));
			}
		}

		/// <summary>
		/// Identifies the color for the bar's triangle for the
		/// flight's tisa field
		/// </summary>
		/// <param name="strTisa"></param>
		/// <returns>the color as int</returns>
		public int GetTisaColor(String strTisa)
		{										 
			int ret = 0;

			switch(strTisa)
			{
				case "S":
					ret = UT.colGray;//colGray
					break;
				case "E":
					ret = UT.colWhite;//colWhite
					break;
				case "T":
					ret = UT.colYellow;//colYellow
					break;
				case "L":
					ret = UT.colDarkGreen;//colGreen
					break;
				case "O":
					ret = UT.colGreen;//colLime
					break;
				default:
					break;
			}
			return ret;
		}
		/// <summary>
		/// Identifies the color for the bar's triangle for the
		/// flight's tisd field
		/// </summary>
		/// <param name="strTisa"></param>
		/// <returns>the color as int</returns>
		public int GetTisdColor(String strTisd)
		{
			int ret = 0;
			switch(strTisd)
			{
				case "O":
					ret = UT.colGreen;
					break;
				case "A":
					ret = UT.colDarkGreen;
					break;
				case "S":
					ret = UT.colGray; 
					break;
				case "E":
					ret = UT.colWhite;
					break;
				default:
					break;
			}
			return ret;
		}

		private void cbReorgGantt_CheckedChanged(object sender, System.EventArgs e)
		{
			if(cbReorgGantt.Checked == true)
			{
				cbReorgGantt.Checked = false;
				AllocateFlights();
			}
		}

		private void btnDebug_Click(object sender, System.EventArgs e)
		{
			frmExceptions olDlg = new frmExceptions();
			olDlg.ShowDialog(this);
			if(UT.arrExceptions.Count == 0)
			{
				btnDebug.BackColor = Control.DefaultBackColor;
				String strText = "debug(" + UT.arrExceptions.Count.ToString() + ")";
				btnDebug.Text = strText;
				btnDebug.Refresh();
			}
		}

		private void OnNewExeption(int opCnt)
		{
			String strText;
			strText = "debug(" + UT.arrExceptions.Count.ToString() + ")";
			btnDebug.Text = strText;
			btnDebug.BackColor = Color.Red;
			btnDebug.Refresh();
		}

		private void frmStatusOverview_OnStatusOverviewClosed()
		{
			cbOpen.Checked = false;
		}

		private void frmRules_OnRuleEditorClosed()
		{
			cbRules.Checked = false;
		}
		private void frmDetailStatus_OnStatusDetailClosed()
		{
			myDetailDlg = null;
		}

        /// <summary>
        /// 1. Remove the delegates 
        /// 2. Disconnect the database
        /// </summary>
		private void frmMain_Closed(object sender, System.EventArgs e)
		{
			//Remove the delegates from the delegate list!!!
			UT.OnExceptionOccured  -= delegateExceptionOccured;
			DE.OnRuleEditorClosed -= delegateRuleEditorClosed;
			DE.OnStatusOverviewClosed -= delegateStatusOverviewClosed;
			DE.OnStatusDetailClosed -= delegateStatusDetailClosed;
			DE.OnOpenDetailStatusDlg -= delegateOpenDetailStatusDlg;
			DE.OnBirdViewSaved -= delegateBirdViewSaved;
			DE.OnFindList_Closed -= delegateFindListClosed;
			DE.OnNavigateToFlight -= delegateNavigateToFlight;
			DE.OnUpdateFlightData -= delegateUpdateFlightData;
            DE.OnFlightStatus_Changed -= delegateFlightStatusChanged;
			DE.OnViewNameChanged -= delegateViewNameChanged;
			DE.OnUTC_LocalTimeChanged -= delegateUTC_LocalTimeChanged;
			DE.OnCLO_Message -= delegateCLO_Message;
			DE.OnReloadCompleteOnlineStatuses -= delegateReloadCompleteOnlineStatuses;
            UT.LogMsg("Status Manager : Closed");
			myDB.Disconnect();
		}

        private void myGantt_ActualLineNo(object sender, AxUGANTTLib._DUGanttEvents_ActualLineNoEvent e)
        {

        }

		private void myGantt_ActualTimescaleDate(object sender, AxUGANTTLib._DUGanttEvents_ActualTimescaleDateEvent e)
		{
			lblAboveText.Text = e.timescaleDate.ToShortDateString();
			lblAboveText.Refresh();
		}

		private void btnEditViewPoint_Click(object sender, System.EventArgs e)
		{
			String strViewPoint = cbViewPoints.Text;
			if(strViewPoint != "")
			{
				frmViewPoint olV = new frmViewPoint(this, DataForm, "EDIT", strViewPoint);
				olV.Show();
			}
		
		}
		private void OnBirdViewSaved()
		{
			ReloadCombo();
		}

		private void btnNewViewPoint_Click(object sender, System.EventArgs e)
		{
			frmViewPoint olV = new frmViewPoint(this, DataForm, "NEW", "");
			olV.Show();
		}

		private void btnOpenVIewPoint_Click(object sender, System.EventArgs e)
		{
			String strViewPoint = cbViewPoints.Text;
			if(strViewPoint != "")
			{
				frmRunViewPoint olV = new frmRunViewPoint(this, DataForm, strViewPoint);
				olV.Show();
			}
		}

        /// <summary>
        /// Keep the current geometry in registry
        /// to use back on next time
        /// </summary>
		private void frmMain_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			DataForm.Close();
			//Store current geometry in Registry
            //To use back this setting on next time
			string theKey = "Software\\StatusManager\\MainWindow";
			RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			if(rk == null)
			{//If No registry key then create it
				rk = Registry.CurrentUser.OpenSubKey("Software\\StatusManager", true);
				rk.CreateSubKey("MainWindow");
				rk.Close();
				theKey = "Software\\StatusManager\\MainWindow";
				rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			}

			rk.SetValue("X", this.Left.ToString());
			rk.SetValue("Y", this.Top.ToString());
			rk.SetValue("Width", this.Width.ToString());
			rk.SetValue("Height", this.Height.ToString());

		}

        /// <summary>
        /// Refresh the Time information to show on the screen
        /// </summary>
		private void timer1_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			DateTime localTime = DateTime.Now;
			DateTime utcTime = CFC.GetUTC();
			lblUTCTime.Text = utcTime.ToShortDateString() + " " + utcTime.ToShortTimeString() + "z";
			lblUTCTime.Refresh();
			lblLocalTime.Text = localTime.ToShortDateString() + " " + localTime.ToShortTimeString();
			lblLocalTime.Refresh();
		}

		private void cbRules_CheckedChanged(object sender, System.EventArgs e)
		{
			if(cbRules.Checked == true)
			{
				cbRules.BackColor = Color.LightGreen;
				cbRules.Refresh();
				myRulesDialog = new frmRules();
				myRulesDialog.Owner = this;
				myRulesDialog.Show(); 
			}
			else
			{
				if(myRulesDialog != null)
				{
					myRulesDialog.Close();
				}
				cbRules.BackColor = Color.Transparent;
			}
		}

        /// <summary>
        /// To show about
        /// </summary>
		private void btnAbout_Click(object sender, System.EventArgs e)
		{
			frmAbout dlg = new frmAbout();
			dlg.strAppName = "Status Manager";
			Assembly [] assemblies = AppDomain.CurrentDomain.GetAssemblies();
			foreach(Assembly assembly in assemblies)
			{
				//if(assembly.GetName().Name == "Status_Manager")
                if ((assembly.GetName().Name == "Status_Manager") || (assembly.GetName().Name == "StatMgr_Release"))
				{
					dlg.strAppVersion = assembly.GetName().Version.ToString();
				}
			}
				
			dlg.ShowDialog(this);
		}

		private void cbOpen_CheckedChanged(object sender, System.EventArgs e)
		{
			if(cbOpen.Checked == true)
			{
				cbOpen.BackColor = Color.LightGreen;
				cbOpen.Refresh();
				myStatusOverview = new frmStatusOverview();
				myStatusOverview.Show(); 
			}
			else
			{
				if(myStatusOverview  != null)
				{
					myStatusOverview .Close();
				}
				cbOpen.BackColor = Color.Transparent;
				cbOpen.Refresh();
			}
		}

		private void cbUTC_CheckedChanged(object sender, System.EventArgs e)
		{
			HandleUTC();
		}

		private void HandleUTC()
		{
			ITable myTmpAFT = myDB["AFTTMP"];
            if (myTmpAFT != null)
            {
                try
                {
                    if (cbUTC.Checked == true)
                    {
                        cbUTC.BackColor = Color.LightGreen;
                        cbUTC.Refresh();
                        UT.IsTimeInUtc = true;
                        if (isInit == true)
                        {
                            this.Cursor = Cursors.WaitCursor;
                            myAFT.TimeFieldsCurrentlyInUtc = true;
                            myTmpAFT.TimeFieldsCurrentlyInUtc = true;
                            myOST.TimeFieldsCurrentlyInUtc = true;
                            DE.Call_UTC_LocalTimeChanged(this);
                            this.Cursor = Cursors.Arrow;
                        }
                    }
                    else
                    {
                        cbUTC.BackColor = Color.Transparent;
                        cbUTC.Refresh();
                        UT.IsTimeInUtc = false;
                        if (isInit == true)
                        {
                            this.Cursor = Cursors.WaitCursor;
                            myAFT.TimeFieldsCurrentlyInUtc = false;
                            myTmpAFT.TimeFieldsCurrentlyInUtc = false;
                            myOST.TimeFieldsCurrentlyInUtc = false;
                            DE.Call_UTC_LocalTimeChanged(this);
                            this.Cursor = Cursors.Arrow;
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogExceptionMsg(ex, "Time conversion error.");
                }
            }
		}

		private void UTCToLocal()
		{
			myAFT = myDB["AFT"];
            if (myAFT != null)
            {
                try
                {
                    myAFT.Lock.AcquireReaderLock(Timeout.Infinite);
                    string strTmp = "";
                    DateTime dat;
                    DateTime datLocal = DateTime.Now;
                    DateTime datUTC = CFC.GetUTC();
                    TimeSpan olTS = datLocal - datUTC;
                    myGantt.UTCOffsetInHours = (short)-UT.UTCOffset;//(short)-olTS.TotalHours;
                    //MessageBox.Show(olTS.TotalHours.ToString());
                    for (int i = 0; i < myAFT.Count; i++)
                    {
                        for (int j = 0; j < arrAftTimeFields.Length; j++)
                        {
                            strTmp = myAFT[i][arrAftTimeFields[j]];
                            if (strTmp != "")
                            {
                                dat = UT.CedaFullDateToDateTime(strTmp);
                                dat = dat.AddHours(olTS.TotalHours);
                                strTmp = UT.DateTimeToCeda(dat);
                                myAFT[i][arrAftTimeFields[j]] = strTmp;
                            }
                        }
                    }
                    AllocateFlights();
                }
                catch (Exception ex)
                {
                    LogExceptionMsg(ex, "UTC To Local Time conversion Error.");
                }
                finally
                {
                    if (myAFT.Lock.IsReaderLockHeld) myAFT.Lock.ReleaseReaderLock();
                }
            }
		}
        private void LocalToUTC()
        {
            myAFT = myDB["AFT"];
            if (myAFT != null)
            {
                try
                {
                    myAFT.Lock.AcquireReaderLock(Timeout.Infinite);
                    string strTmp = "";
                    DateTime dat;
                    DateTime datLocal = DateTime.Now;
                    DateTime datUTC = CFC.GetUTC();
                    TimeSpan olTS = datLocal - datUTC;
                    myGantt.UTCOffsetInHours = 0;
                    //MessageBox.Show(olTS.TotalHours.ToString());
                    for (int i = 0; i < myAFT.Count; i++)
                    {
                        for (int j = 0; j < arrAftTimeFields.Length; j++)
                        {
                            strTmp = myAFT[i][arrAftTimeFields[j]];
                            if (strTmp != "")
                            {
                                dat = UT.CedaFullDateToDateTime(strTmp);
                                dat = dat.Subtract(olTS);
                                strTmp = UT.DateTimeToCeda(dat);
                                myAFT[i][arrAftTimeFields[j]] = strTmp;
                            }
                        }
                    }
                    AllocateFlights();
                }
                catch (Exception ex)
                {
                    LogExceptionMsg(ex, "UTC To Local Time conversion Error.");
                }
                finally
                {
                    if (myAFT.Lock.IsReaderLockHeld) myAFT.Lock.ReleaseReaderLock();
                }
            }
        }

		private void bcBlinker1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, bcBlinker1, Color.WhiteSmoke, Color.LightGray);
		}

		private void cbView_CheckedChanged(object sender, System.EventArgs e)
		{
			if(cbView.Checked == true)
			{
				cbView.BackColor = Color.LightGreen;
				cbView.Refresh();
				frmView Dlg = new frmView(DataForm.tabViews);
				Dlg.ShowDialog(this);
				this.Text = "Status Manager: Timeframe [" + UT.TimeFrameFrom.ToString() + "] - [" + 
					UT.TimeFrameTo.ToString() + "]" + " --- Server: " + UT.ServerName + 
					" --- Active View: [" + CFC.CurrentViewName + "]";
				cbView.Checked = false;
				cbView.BackColor = Color.Transparent;
			}
		}

		private void myGantt_OnLButtonDblClkBar(object sender, AxUGANTTLib._DUGanttEvents_OnLButtonDblClkBarEvent e)
		{
			string strUrno = e.key;
			myAFT = myDB["AFT"];
			IRow [] rows = myAFT.RowsByIndexValue("URNO", strUrno);
			if(rows.Length > 0)
			{
				OpenDetailStatusDlg(rows[0]["URNO"], rows[0]["RKEY"], "");
				DE.Call_NavigateToFlight(this, rows[0]["URNO"]);
			}
		}

		private void OpenDetailStatusDlg(string strAftURNO, string strAftRKEY, string strOstUrno)
		{
			if(myDetailDlg == null)
			{
				myDetailDlg = new frmDetailStatus(strAftURNO, strAftRKEY, strOstUrno);
				myDetailDlg.Show();
			}
			else
			{
				myDetailDlg.SetData(strAftURNO, strAftRKEY, strOstUrno);
			}
			myDetailDlg.BringToFront();
		}

		private void timerConflictCheck_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			DE.Call_ConflicTimerElapsed();
		}

		private void frmStatusOverview_OnOpenDetailStatusDlg(string strAftURNO, string strAftRkey, string strOstUrno)
		{
			//Called from delegate
			OpenDetailStatusDlg(strAftURNO, strAftRkey, strOstUrno);
		}

        delegate void CallbackUpdateBar(string strAftUrno, bool blSelectBar, bool blRefresh );
        delegate void CallBackMethod0();

        private void UpdateBar(string strAftUrno, bool blSelectBar, bool blRefresh)
        {
            if (this.InvokeRequired)
            {//AM 20080320 - Make as thread safe
                CallbackUpdateBar d = new CallbackUpdateBar(UpdateBar);
                object[] arrObj = new object[3];
                arrObj[0] = strAftUrno;
                arrObj[1] = blSelectBar;
                arrObj[2] = blRefresh;
                this.Invoke(d, arrObj);
            }
            else
            {
                int LineNo = myGantt.GetLineNoByBarKey(strAftUrno);

                DateTime myFrom;
                DateTime myTo;
                String strBarKey = "";
                String strAdid = "";
                String strTisa = "";
                String strTisd = "";
                int interval = 0;
                String strFlno = "";
                int colBody = 0;
                int leftTriaColor = 0;
                String strStoa = "";
                DateTime datStoa;
                int rightTriaColor;
                DateTime datTimeFrameFrom = UT.TimeFrameFrom;
                DateTime datTimeFrameTo = UT.TimeFrameTo;

                //For new bars set it to the first line ==> user decides when to reorg the gantt
                if (LineNo < 0) LineNo = 0;
                if (UT.IsTimeInUtc == true)
                {
                    myTo = CFC.GetUTC();
                    myFrom = CFC.GetUTC();
                }
                else
                {
                    myTo = DateTime.Now;
                    myFrom = DateTime.Now;
                }

                leftTriaColor = -1;
                rightTriaColor = -1;

                //First we delete the bar, this is correct in order to update DFR as well
                myGantt.DeleteBarByKey(strAftUrno);

                IRow[] aftRows = myAFT.RowsByIndexValue("URNO", strAftUrno);
                if (aftRows.Length > 0)
                {
                    myGantt.DeleteAllTimeMarkers();
                    strBarKey = aftRows[0]["URNO"];
                    strAdid = aftRows[0]["ADID"];
                    strFlno = aftRows[0]["FLNO"];

                    if (strAdid == "A")
                    {
                        interval = imArrivalBarLen;
                        myFrom = UT.CedaFullDateToDateTime(aftRows[0]["TIFA"]);
                        myTo = myFrom.AddMinutes((double)interval);
                        strStoa = aftRows[0]["STOA"];
                        colBody = colorArrivalBar; //Cyan
                        if (strStoa != "")
                        {
                            datStoa = UT.CedaFullDateToDateTime(strStoa);
                        }
                        strTisa = aftRows[0]["TISA"];
                        leftTriaColor = GetTisaColor(strTisa);
                    }
                    if (strAdid == "D")
                    {
                        interval = imDepartureBarLen;
                        TimeSpan olSpan = new TimeSpan(0, 0, interval, 0, 0);
                        myTo = UT.CedaFullDateToDateTime(aftRows[0]["TIFD"]);
                        myFrom = myTo.Subtract(olSpan);
                        colBody = colorDepartureBar; //colLightBlue
                        strTisd = aftRows[0]["TISD"];
                        rightTriaColor = GetTisdColor(strTisd);
                    }
                    int CflCount = 0;
                    int AckCount = 0;
                    int TotalCount = 0;
                    int OSTCount = 0;
                    //CFC.HasFlightStatusConflicts(strBarKey, ref CflCount, ref AckCount, ref TotalCount, ref OSTCount);
                    //AM 20080425 - Use New Class. Get the Conflict Info
                    Ctrl.CtrlCfl.GetInstance().GetConflictCount(strBarKey, ref CflCount,
                        ref AckCount, ref TotalCount, ref OSTCount);
                    if (OSTCount > 0)
                    {
                        strFlno += " *";
                    }
                    if (blSelectBar == false)
                    //if(strAftUrno != omCurrentSelectedUrno)
                    {
                        if (CflCount > 0)
                        {
                            colBody = UT.colRed;
                        }
                        else if (AckCount > 0)
                        {
                            colBody = UT.colOrange;
                        }
                    }
                    else
                    {
                        colBody = UT.colPink;
                        myGantt.SetTimeMarker(myFrom, UT.colYellow);
                        myGantt.SetTimeMarker(myTo, UT.colYellow);
                    }
                    myGantt.AddBarToLine(LineNo, strBarKey, myFrom, myTo,
                        strFlno, 1,
                        UT.colCyan, UT.colBlack, UT.colRed,
                        UT.colBlack, UT.colBlack,
                        "", "", UT.colBlack, UT.colBlack);
                    myGantt.SetBarColor(strBarKey, colBody);
                    string strTriaID = leftTriaColor.ToString() + rightTriaColor.ToString();
                    myGantt.SetBarTriangles(strBarKey, strTriaID, leftTriaColor, rightTriaColor, true);
                    if (blRefresh == true)
                    {
                        myGantt.Refresh();
                    }
                }
            }
        }	

		public void UpdateBarForLine(string strAftUrno, int LineNo, bool blSelectBar, bool blRefresh)
		{
			//int LineNo = myGantt.GetLineNoByBarKey(strAftUrno);
			DateTime myFrom;
			DateTime myTo;
			String strBarKey = "";
			String strAdid = "";
			String strTisa = "";
			String strTisd = "";
			int interval=0;
			String strFlno  = "";
			int colBody = 0;
			int leftTriaColor = 0;
			String strStoa  = "";
			DateTime datStoa;
			int rightTriaColor;
			DateTime datTimeFrameFrom = UT.TimeFrameFrom;
			DateTime datTimeFrameTo = UT.TimeFrameTo;

			//For new bars set it to the first line ==> user decides when to reorg the gantt
			if(LineNo < 0) LineNo = 0; 
			if(UT.IsTimeInUtc == true)
			{
				myTo = CFC.GetUTC();
				myFrom = CFC.GetUTC();
			}
			else
			{
				myTo = DateTime.Now;
				myFrom = DateTime.Now;
			}

			leftTriaColor = -1;
			rightTriaColor = -1;
	
			//First we delete the bar, this is correct in order to update DFR as well
			myGantt.DeleteBarByKey(strAftUrno);

			IRow [] aftRows = myAFT.RowsByIndexValue("URNO", strAftUrno);
			if(aftRows.Length > 0)
			{
				myGantt.DeleteAllTimeMarkers();
				strBarKey = aftRows[0]["URNO"];
				strAdid = aftRows[0]["ADID"];
				strFlno = aftRows[0]["FLNO"];
		        
				if( strAdid == "A" )
				{
					interval = imArrivalBarLen; 
					myFrom = UT.CedaFullDateToDateTime(aftRows[0]["TIFA"]); 
					myTo =  myFrom.AddMinutes((double)interval); 
					strStoa = aftRows[0]["STOA"];
					colBody = colorArrivalBar; //Cyan
					if(strStoa != "")
					{
						datStoa = UT.CedaFullDateToDateTime(strStoa);
					}
					strTisa = aftRows[0]["TISA"];
					leftTriaColor = GetTisaColor(strTisa);
				}
				if(strAdid == "D")
				{
					interval = imDepartureBarLen;
					TimeSpan olSpan = new TimeSpan(0, 0, interval, 0, 0);
					myTo = UT.CedaFullDateToDateTime(aftRows[0]["TIFD"]);
					myFrom = myTo.Subtract(olSpan);
					colBody = colorDepartureBar; //colLightBlue
					strTisd = aftRows[0]["TISD"];
					rightTriaColor = GetTisdColor(strTisd);
				}
				int CflCount = 0;
				int AckCount = 0;
				int TotalCount = 0;
				int OSTCount = 0;
                //CFC.HasFlightStatusConflicts(strBarKey, ref CflCount, ref AckCount, ref TotalCount, ref OSTCount);
                //AM 20080425 - Use New Class. Get the Conflict Info
                Ctrl.CtrlCfl.GetInstance().GetConflictCount(strBarKey, ref CflCount,
                    ref AckCount, ref TotalCount, ref OSTCount);

				if(OSTCount > 0)
				{
					strFlno += " *";
				}
				if(blSelectBar == false)
					//if(strAftUrno != omCurrentSelectedUrno)
				{
					if(CflCount > 0)
					{
						colBody = UT.colRed;
					}
					else if(AckCount > 0)
					{
						colBody = UT.colOrange;
					}
				}
				else
				{
					colBody = UT.colPink;
					myGantt.SetTimeMarker(myFrom, UT.colYellow);
					myGantt.SetTimeMarker(myTo, UT.colYellow);
				}
                //if (!UT.IsTimeInUtc)
                //{//AM 20080310 ????
                //    short diffHours = (short) -UT.UTCOffset ;
                //    myFrom = myFrom.AddHours(diffHours);
                //    myTo = myTo.AddHours(diffHours);
                //}
				myGantt.AddBarToLine( LineNo, strBarKey, myFrom, myTo, 
					strFlno, 1,
					UT.colCyan, UT.colBlack, UT.colRed, 
					UT.colBlack, UT.colBlack, 
					"", "", UT.colBlack, UT.colBlack);
				myGantt.SetBarColor(strBarKey, colBody);
				string strTriaID = leftTriaColor.ToString() + rightTriaColor.ToString();
				myGantt.SetBarTriangles(strBarKey, strTriaID, leftTriaColor, rightTriaColor, true);
				if(blRefresh == true)
				{
					myGantt.Refresh();
				}
			}
		}
		private void myGantt_OnLButtonDownBar(object sender, AxUGANTTLib._DUGanttEvents_OnLButtonDownBarEvent e)
		{
			UpdateBar(omCurrentSelectedUrno, false, false); 
			omCurrentSelectedUrno = e.key;
			UpdateBar(e.key, true, true);
		}

		private void cbSearch_CheckedChanged(object sender, System.EventArgs e)
		{
			if(cbSearch.Checked == true)		
			{
				cbSearch.BackColor = Color.LightGreen;
				myFindDlg = new frmFind();
				myFindDlg.Owner = this;
				myFindDlg.Show();
			}
			else
			{
				if(myFindDlg != null)
				{
					myFindDlg.Close();
					myFindDlg = null;
				}
				cbSearch.BackColor = Color.Transparent;
			}
		} 

		private void DE_OnFindList_Closed()
		{
			cbSearch.Checked = false;		
			myFindDlg = null;
		}

		private void DE_OnNavigateToFlight(object sender, string strAftUrno)
		{
			if(sender == (object)this)
			{
				return; //No update if "this" is the sender
			}
			string strTIF = "";
			DateTime datScroll;

            try
            {
                UT.LogMsg("frmMain:DE_OnNavigateToFlight: b4 AFT AcqReader Lock");
                myAFT.Lock.AcquireReaderLock(System.Threading.Timeout.Infinite);
                UT.LogMsg("frmMain:DE_OnNavigateToFlight: AF AFT AcqReader Lock");
                IRow[] aftRows = myAFT.RowsByIndexValue("URNO", strAftUrno);
                if (aftRows.Length > 0)
                {
                    if (aftRows[0]["ADID"] == "A")
                    {
                        strTIF = aftRows[0]["TIFA"];
                    }
                    if (aftRows[0]["ADID"] == "D")
                    {
                        strTIF = aftRows[0]["TIFD"];
                    }
                    datScroll = UT.CedaFullDateToDateTime(strTIF);
                    TimeSpan olSpan = new TimeSpan(0, 1, 0, 0, 0);
                    datScroll = datScroll.Subtract(olSpan);
                    if (strAftUrno != omCurrentSelectedUrno)
                    {
                        UpdateBar(omCurrentSelectedUrno, false, false);
                    }
                    omCurrentSelectedUrno = strAftUrno;
                    UpdateBar(strAftUrno, true, true);
                    myGantt.ScrollTo(datScroll);
                    myGantt.Refresh();
                }
            }
            catch (Exception ex)
            {
                LogExceptionMsg(ex, "Navigation Error.");
            }
            finally
            {
                UT.LogMsg("frmMain:DE_OnNavigateToFlight: b4 AFT RelReader Lock");
                if (myAFT.Lock.IsReaderLockHeld) myAFT.Lock.ReleaseReaderLock();
                UT.LogMsg("frmMain:DE_OnNavigateToFlight: AF AFT RelReader Lock");
            }
		}

        private void LogExceptionMsg( Exception ex, string msgToDisplay )
        {
            UT.LogMsg( "frmMain:Err: " + ex.Message + Environment.NewLine + ex.StackTrace );
            if (msgToDisplay!="")
            {
                MessageBox.Show( msgToDisplay, "Error" );
            }
        }

		private void cbNow_CheckedChanged(object sender, System.EventArgs e)
		{
			if(cbNow.Checked == true)
			{
				ScrollToNow();
			}
		}

		private void ScrollToNow()
		{
			DateTime d = DateTime.Now;

            //DateTime datUTC = CFC.GetUTC();
            //TimeSpan olTS = d - datUTC;
            //if(UT.IsTimeInUtc == true && UT.UTCOffset != 0)
            //{
            //    TimeSpan olSpan = new TimeSpan(0, 1, 0, 0, 0);
            //    d = datUTC.Subtract(olSpan);
            //}
            //else
            //{
            //    TimeSpan olSpan = new TimeSpan(0, 1, 0, 0, 0);
            //    d = d.Subtract(olSpan);
            //}
            //if(d < UT.TimeFrameFrom || d > UT.TimeFrameTo)
            //{
            //    myGantt.ScrollTo(UT.TimeFrameFrom);
            //}
            //else 
            //{
            //    myGantt.ScrollTo(d);
            //}

            //AM 20080311 
            DateTime dtNow = DateTime.Now.AddHours(-1);
            if (this.cbUTC.Checked)//UTC Time
            {
                dtNow = dtNow.AddHours(-UT.UTCOffset);
            }

            if (dtNow < this.myGantt.TimeFrameFrom)
            {
                dtNow = this.myGantt.TimeFrameFrom;
            }
            else if (dtNow > this.myGantt.TimeFrameTo)
            {
                dtNow = this.myGantt.TimeFrameTo;
            }
            this.myGantt.ScrollTo(dtNow);

			myGantt.Refresh();
			lblAboveText.Text = d.ToShortDateString();
			lblAboveText.Refresh();
			cbNow.Checked = false;
		}
		private void timerCloseStartup_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			timerCloseStartup.Stop();
			timerCloseStartup.Enabled = false;
			StartForm.Refresh();
			StartForm.PrepareClose();
		}


		private void DE_OnUpdateFlightData(object sender, string strAftUrno, State state)
		{
            UT.LogMsg("frmMain:DE_OnUpdateFlightData:Start");
			if(omCurrentSelectedUrno == strAftUrno)
			{
				UpdateBar(strAftUrno, true, true); 
			}
			else
			{
				UpdateBar(strAftUrno, false, true); 
			}
            UT.LogMsg("frmMain:DE_OnUpdateFlightData:Finish");
		}

        private void DE_OnFlightStatusChanged(object sender, string strAftUrno)
        {
            UT.LogMsg("frmMain:DE_OnFlightStatusChanged:Start:" + strAftUrno);
            if (omCurrentSelectedUrno == strAftUrno)
            {
                UpdateBar(strAftUrno, true, true);
            }
            else
            {
                UpdateBar(strAftUrno, false, true);
            }
            UT.LogMsg("frmMain:DE_OnFlightStatusChanged:Finish");
        }

		private void DE_OnViewNameChanged()
		{
            if (this.InvokeRequired)
            {//AM 20080830 - Make as thread safe
                CallBackMethod0 d = new CallBackMethod0(DE_OnViewNameChanged);
                this.Invoke(d);
            }
            else
            {
                txtCalc.Visible = true;
                txtCalc.BringToFront();
                txtCalc.Refresh();
                for (int i = 0; i < myAFT.Count; i++)
                {
                    UpdateBar(myAFT[i]["URNO"], false, false);
                    if ((i % 50) == 0)
                    {
                        txtCalc.Text = "Updating View " + i.ToString() + " of " + myAFT.Count.ToString();
                        txtCalc.Refresh();
                    }
                }
                txtCalc.Visible = false;
                myGantt.Refresh();
            }
		}

		private void DE_OnUTC_LocalTimeChanged()
		{
            AllocateFlights();
            ScrollToNow();            
		}

		private void btnDeleteViewPoint_Click(object sender, System.EventArgs e)
		{
			string strViewName = cbViewPoints.Text;
			if(strViewName != "")
			{
				DialogResult r = MessageBox.Show(this, "Do you really want to delete <" + strViewName + "> ???", "Delete Question",
					MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if( r == DialogResult.Yes)
				{
					int i = 0;
					for( i = DataForm.tabVPD.GetLineCount()-1; i >= 0; i--)
					{
						if(DataForm.tabVPD.GetFieldValue(i, "VIEW") == strViewName)
						{
							DataForm.tabVPD.DeleteLine(i);
						}
					}
					for( i = DataForm.tabVPL.GetLineCount()-1; i >= 0; i--)
					{
						if(DataForm.tabVPL.GetFieldValue(i, "VIEW") == strViewName)
						{
							DataForm.tabVPL.DeleteLine(i);
						}
					}
					IUfisComWriter myUfisCom = UT.GetUfisCom();
					int ilRet = myUfisCom.CallServer("DRT", "VPDTAB", "", "", "WHERE VNAM = '" + strViewName + "'", "230");
					if(ilRet != 0)
					{
						string strERR = myUfisCom.LastErrorMessage;
						//MessageBox.Show(this, strERR);
					}
					ilRet = myUfisCom.CallServer("DRT", "VPLTAB", "", "", "WHERE VNAM = '" + strViewName + "'", "230");
					if(ilRet != 0)
					{
						string strERR = myUfisCom.LastErrorMessage;
						//MessageBox.Show(this, strERR);
					}
					ReloadCombo();
				}
				//Remove the viewpoint from the registry
				string theKey = "Software\\StatusManager";
				RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);

				if(rk != null)
				{
					theKey = "STAT_MGR_VIEWPOINT_" + strViewName;
					rk.DeleteSubKey(theKey, false );
				}
			}
		}

        private void SetLoadTimeFrame(frmLoad olLoad)
        {
            short utcOffsetHours = 0;
            if (UT.IsTimeInUtc != true)
            {
                utcOffsetHours = (short)-UT.UTCOffset;
            }
            UT.TimeFrameFrom = olLoad.datFrom.Value.AddHours(utcOffsetHours);
            UT.TimeFrameTo = olLoad.datTo.Value.AddHours(utcOffsetHours);
            //AM 20080310 - To load exact time frame. Commented out following 2 lines
            //UT.TimeFrameFrom = UT.TimeFrameFrom.Subtract(new TimeSpan(0, 12, 0, 0));
            //UT.TimeFrameTo = UT.TimeFrameTo.AddHours(12);

        }

		private void ReloadData()
		{
			StartForm = new frmStartup(); 
			StartForm.Show();
			frmLoad olLoad = new frmLoad();
			this.lblAboveText.BackColor = Color.Transparent;
			DialogResult olResult;
			olResult = olLoad.ShowDialog();
			if (olResult == DialogResult.OK)
			{
                //UT.TimeFrameFrom = olLoad.datFrom.Value;
                //UT.TimeFrameTo = olLoad.datTo.Value;
                ////UT.TimeFrameFrom = UT.TimeFrameFrom.Subtract(new TimeSpan(0, 12, 0, 0));
                ////UT.TimeFrameTo = UT.TimeFrameTo.AddHours(12);
                UT.IsTimeInUtc = false;
                SetLoadTimeFrame(olLoad);

				DataForm.LoadOnlineData(true);

				StartForm.txtCurrentStatus.Text = "Allocating flights to gantt-chart";
				StartForm.txtCurrentStatus.Refresh();
				StartForm.txtDoneStatus.Refresh();
                AllocateFlights();
				StartForm.BringToFront();
				this.Text = "Status Manager: Timeframe [" + UT.TimeFrameFrom.ToString() + "] - [" + 
					UT.TimeFrameTo.ToString() + "]" + " --- Server: " + UT.ServerName + 
					" --- Active View: [" + CFC.CurrentViewName + "]";
			}
			timerCloseStartup.Enabled = true;
		}

		private void cbLoad_CheckedChanged(object sender, System.EventArgs e)
		{
			if(cbLoad.Checked == true)
			{
				cbLoad.Checked = false;
				ReloadData();
                if (this.cbUTC.Checked == true)
                {
                    this.cbUTC.Checked = false;
                    //it will trigger the HandleUTC by Utc Check Change event.
                }
                else
                {
                    HandleUTC();
                }
			}
		}

		private void DE_OnCLO_Message()
		{
			MessageBox.Show(this, "The server has been switched!!\nPlease leave Status-Manager and restart in 3 minutes",
							"Attention", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			Application.Exit();
		}

		private void DE_OnReloadCompleteOnlineStatuses()
		{
			txtCalc.Visible = true;
			txtCalc.BringToFront();
			txtCalc.Text = "Reload Online Status ...";
			txtCalc.Refresh();
			DataForm.LoadOstData(false);
			HandleUTC();
			txtCalc.Visible = false;
			AllocateFlights();
			DE.Call_ConflicTimerElapsed();
		}

        private void btnHelp_Click(object sender, EventArgs e)
        {
            try
            {
                if (_helpFile != "")
                {
                    System.Diagnostics.Process.Start(_helpFile);
                }
                else
                {
                    MessageBox.Show("No Help File.");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Help Display Error.");
            }
        }

        //private void TestSortedList()
        //{
        //    SortedList slArr = new SortedList();
        //    slArr.Add("010", "010 C");
        //    slArr.Add("090", "090 A");
        //    ArrayList alArr = new ArrayList();
        //    alArr.Add("A 12345");
        //    alArr.Add("A 3451");
        //    alArr.Add("A 6232");
        //    alArr.Add("D 1234");
        //    alArr.Add("A 2312");
        //    alArr.Add("B 72345");
        //    alArr.Sort(new Desc());
        //    string stArr = "";
        //    for (int i = 0; i < alArr.Count; i++)
        //    {
        //        stArr += alArr[i].ToString() + Environment.NewLine;
        //    }
        //    MessageBox.Show(stArr);
        //    if (slArr.ContainsKey("010"))
        //    {
        //        slArr.Add("011-1", "011-1");
        //    }
        //    slArr.Add("30", "030 A");
        //    slArr.Add("110", "110 A");
        //    slArr.Add("011", "011 A");
        //    slArr.Add("050", "050 A");

        //    string st = "";
        //    IDictionaryEnumerator ide = slArr.GetEnumerator();

        //    while (ide.MoveNext())
        //    {
        //        st += ide.Value.ToString() + Environment.NewLine;
        //    }
        //    MessageBox.Show(st);
        //}



 	}

    //public class Desc : IComparer
    //{
    //    public int Compare(object x, object y)
    //    {
    //        int result = ((string)x).CompareTo((string)y);
    //        result = -result;
    //        return result;
    //    }
    //}
}

