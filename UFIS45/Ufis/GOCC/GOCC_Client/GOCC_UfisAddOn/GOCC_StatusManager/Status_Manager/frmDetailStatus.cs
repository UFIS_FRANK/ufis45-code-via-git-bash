using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Microsoft.Win32;
using Ufis.Utils;
using Ufis.Data;
using System.Diagnostics;

namespace Status_Manager
{
	/// <summary>
	/// Summary description for frmDetailStatus.
	/// </summary>
	public class frmDetailStatus : System.Windows.Forms.Form
	{
		#region --------------- MyRegion
		private int myTabScrollPos = 0;
		private IDatabase myDB = null;
		private ITable    myAFT = null;
		private ITable    myHSS = null;
		private ITable    myOST = null;
		private ITable    mySDE = null;
		private ITable    mySRH = null;
		private string myCurrURNO = "";
		private string myCurrRKEY = "";
		private string myCurrOstURNO = "";
		private DE.ConflicTimerElapsed myConflictTimerElapsed = null;
		private DE.OST_Changed	       delegateOST_Changed = null;
        private DE.OST_ChangedForAFlight delegateOST_ChangedForAFlight = null;
		private DE.UTC_LocalTimeChanged delegateUTC_LocalTimeChanged = null;
		private DE.UpdateFlightData		delegateUpdateFlightData = null;

		string [] myCFLTYPES_DB;  //RETY values, which have to be stored in DB   [STATUSMANAGER]CFLTYPES_DB
		string [] myCFLTYPES_GUI; //RETY values to prepare for the user Ceda.ini [STATUSMANAGER]CFLTYPES_GUI 
		#endregion ------------ MyRegion
        #region windows declaration
        private System.Windows.Forms.Label lblHeaderInfo;
		private System.Windows.Forms.Panel panelButtons;
		private System.Windows.Forms.PictureBox picButtons;
		private System.Windows.Forms.ImageList imageButtons;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnPrint;
		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.PictureBox picBody;
		private AxTABLib.AxTAB tabArrival;
		private AxTABLib.AxTAB tabDeparture;
		private AxTABLib.AxTAB tabStatusSections;
		private AxTABLib.AxTAB tabStatus;
		private AxTABLib.AxTAB tabTmp;
		private System.Windows.Forms.DateTimePicker dtCONU;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ContextMenu mnuContext;
		private System.Windows.Forms.MenuItem menuItemEnterTime;
		private System.Windows.Forms.MenuItem menuItemClearTime;
		private System.Windows.Forms.MenuItem menuItemSetCurrentTime;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem mnuItemAck;
		private AxTABLib.AxTAB tabPrint;
		private System.Windows.Forms.Button btnBatchListProcessing;
		private System.ComponentModel.IContainer components;

        #endregion
        public frmDetailStatus(string strAftUrno, string strRkey, string strOstUrno)
		{
			myCurrRKEY = strRkey;
			myCurrURNO = strAftUrno;
			myCurrOstURNO = strOstUrno;
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
                try
                {
                    RemoveEvents();
                }
                catch (Exception)
                {
                }
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDetailStatus));
            this.lblHeaderInfo = new System.Windows.Forms.Label();
            this.panelButtons = new System.Windows.Forms.Panel();
            this.btnBatchListProcessing = new System.Windows.Forms.Button();
            this.imageButtons = new System.Windows.Forms.ImageList(this.components);
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.picButtons = new System.Windows.Forms.PictureBox();
            this.panelBody = new System.Windows.Forms.Panel();
            this.tabPrint = new AxTABLib.AxTAB();
            this.label1 = new System.Windows.Forms.Label();
            this.dtCONU = new System.Windows.Forms.DateTimePicker();
            this.tabTmp = new AxTABLib.AxTAB();
            this.tabStatus = new AxTABLib.AxTAB();
            this.tabStatusSections = new AxTABLib.AxTAB();
            this.tabDeparture = new AxTABLib.AxTAB();
            this.tabArrival = new AxTABLib.AxTAB();
            this.picBody = new System.Windows.Forms.PictureBox();
            this.mnuContext = new System.Windows.Forms.ContextMenu();
            this.menuItemEnterTime = new System.Windows.Forms.MenuItem();
            this.menuItemClearTime = new System.Windows.Forms.MenuItem();
            this.menuItemSetCurrentTime = new System.Windows.Forms.MenuItem();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.mnuItemAck = new System.Windows.Forms.MenuItem();
            this.panelButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picButtons)).BeginInit();
            this.panelBody.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabTmp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabStatusSections)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabDeparture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabArrival)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBody)).BeginInit();
            this.SuspendLayout();
            // 
            // lblHeaderInfo
            // 
            this.lblHeaderInfo.BackColor = System.Drawing.Color.Black;
            this.lblHeaderInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblHeaderInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeaderInfo.ForeColor = System.Drawing.Color.Lime;
            this.lblHeaderInfo.Location = new System.Drawing.Point(0, 0);
            this.lblHeaderInfo.Name = "lblHeaderInfo";
            this.lblHeaderInfo.Size = new System.Drawing.Size(1028, 20);
            this.lblHeaderInfo.TabIndex = 0;
            this.lblHeaderInfo.Text = "Handling status for Flight";
            this.lblHeaderInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelButtons
            // 
            this.panelButtons.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelButtons.Controls.Add(this.btnBatchListProcessing);
            this.panelButtons.Controls.Add(this.btnPrint);
            this.panelButtons.Controls.Add(this.btnSave);
            this.panelButtons.Controls.Add(this.btnCancel);
            this.panelButtons.Controls.Add(this.picButtons);
            this.panelButtons.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelButtons.Location = new System.Drawing.Point(0, 20);
            this.panelButtons.Name = "panelButtons";
            this.panelButtons.Size = new System.Drawing.Size(1028, 40);
            this.panelButtons.TabIndex = 1;
            // 
            // btnBatchListProcessing
            // 
            this.btnBatchListProcessing.BackColor = System.Drawing.Color.Transparent;
            this.btnBatchListProcessing.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBatchListProcessing.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBatchListProcessing.ImageIndex = 7;
            this.btnBatchListProcessing.ImageList = this.imageButtons;
            this.btnBatchListProcessing.Location = new System.Drawing.Point(168, 0);
            this.btnBatchListProcessing.Name = "btnBatchListProcessing";
            this.btnBatchListProcessing.Size = new System.Drawing.Size(132, 36);
            this.btnBatchListProcessing.TabIndex = 39;
            this.btnBatchListProcessing.Text = "&Check List Prc.";
            this.btnBatchListProcessing.UseVisualStyleBackColor = false;
            this.btnBatchListProcessing.Click += new System.EventHandler(this.btnBatchListProcessing_Click);
            // 
            // imageButtons
            // 
            this.imageButtons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageButtons.ImageStream")));
            this.imageButtons.TransparentColor = System.Drawing.Color.White;
            this.imageButtons.Images.SetKeyName(0, "");
            this.imageButtons.Images.SetKeyName(1, "");
            this.imageButtons.Images.SetKeyName(2, "");
            this.imageButtons.Images.SetKeyName(3, "");
            this.imageButtons.Images.SetKeyName(4, "");
            this.imageButtons.Images.SetKeyName(5, "");
            this.imageButtons.Images.SetKeyName(6, "");
            this.imageButtons.Images.SetKeyName(7, "");
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.Color.Transparent;
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrint.ImageIndex = 6;
            this.btnPrint.ImageList = this.imageButtons;
            this.btnPrint.Location = new System.Drawing.Point(0, 0);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 36);
            this.btnPrint.TabIndex = 38;
            this.btnPrint.Text = "  &Print";
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Transparent;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.ImageIndex = 5;
            this.btnSave.ImageList = this.imageButtons;
            this.btnSave.Location = new System.Drawing.Point(76, 0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(92, 36);
            this.btnSave.TabIndex = 37;
            this.btnSave.Text = "&Save As";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancel.ImageIndex = 0;
            this.btnCancel.ImageList = this.imageButtons;
            this.btnCancel.Location = new System.Drawing.Point(949, 0);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 36);
            this.btnCancel.TabIndex = 36;
            this.btnCancel.Text = "  &Close";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // picButtons
            // 
            this.picButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picButtons.Location = new System.Drawing.Point(0, 0);
            this.picButtons.Name = "picButtons";
            this.picButtons.Size = new System.Drawing.Size(1024, 36);
            this.picButtons.TabIndex = 0;
            this.picButtons.TabStop = false;
            this.picButtons.Paint += new System.Windows.Forms.PaintEventHandler(this.picButtons_Paint);
            // 
            // panelBody
            // 
            this.panelBody.Controls.Add(this.tabPrint);
            this.panelBody.Controls.Add(this.label1);
            this.panelBody.Controls.Add(this.dtCONU);
            this.panelBody.Controls.Add(this.tabTmp);
            this.panelBody.Controls.Add(this.tabStatus);
            this.panelBody.Controls.Add(this.tabStatusSections);
            this.panelBody.Controls.Add(this.tabDeparture);
            this.panelBody.Controls.Add(this.tabArrival);
            this.panelBody.Controls.Add(this.picBody);
            this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBody.Location = new System.Drawing.Point(0, 60);
            this.panelBody.Name = "panelBody";
            this.panelBody.Size = new System.Drawing.Size(1028, 482);
            this.panelBody.TabIndex = 2;
            // 
            // tabPrint
            // 
            this.tabPrint.Location = new System.Drawing.Point(276, 224);
            this.tabPrint.Name = "tabPrint";
            this.tabPrint.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabPrint.OcxState")));
            this.tabPrint.Size = new System.Drawing.Size(100, 50);
            this.tabPrint.TabIndex = 8;
            this.tabPrint.Visible = false;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Black;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Lime;
            this.label1.Location = new System.Drawing.Point(0, 464);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(988, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "To change the status time double click onto the \"user\" column ==> To accept press" +
                " <ENTER>";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // dtCONU
            // 
            this.dtCONU.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtCONU.Location = new System.Drawing.Point(232, 132);
            this.dtCONU.Name = "dtCONU";
            this.dtCONU.Size = new System.Drawing.Size(200, 20);
            this.dtCONU.TabIndex = 6;
            this.dtCONU.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtCONU_KeyPress);
            // 
            // tabTmp
            // 
            this.tabTmp.Location = new System.Drawing.Point(260, 320);
            this.tabTmp.Name = "tabTmp";
            this.tabTmp.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabTmp.OcxState")));
            this.tabTmp.Size = new System.Drawing.Size(120, 44);
            this.tabTmp.TabIndex = 5;
            this.tabTmp.Visible = false;
            // 
            // tabStatus
            // 
            this.tabStatus.Location = new System.Drawing.Point(236, 64);
            this.tabStatus.Name = "tabStatus";
            this.tabStatus.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabStatus.OcxState")));
            this.tabStatus.Size = new System.Drawing.Size(1045, 400);
            this.tabStatus.TabIndex = 4;
            this.tabStatus.SendLButtonClick += new AxTABLib._DTABEvents_SendLButtonClickEventHandler(this.tabStatus_SendLButtonClick);
            this.tabStatus.RowSelectionChanged += new AxTABLib._DTABEvents_RowSelectionChangedEventHandler(this.tabStatus_RowSelectionChanged);
            this.tabStatus.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabStatus_SendLButtonDblClick);
            this.tabStatus.CloseInplaceEdit += new AxTABLib._DTABEvents_CloseInplaceEditEventHandler(this.tabStatus_CloseInplaceEdit);
            this.tabStatus.ComboSelChanged += new AxTABLib._DTABEvents_ComboSelChangedEventHandler(this.tabStatus_ComboSelChanged);
            this.tabStatus.OnVScroll += new AxTABLib._DTABEvents_OnVScrollEventHandler(this.tabStatus_OnVScroll);
            this.tabStatus.Enter += new System.EventHandler(this.tabStatus_Enter);
            this.tabStatus.Leave += new System.EventHandler(this.tabStatus_Leave);
            // 
            // tabStatusSections
            // 
            this.tabStatusSections.Location = new System.Drawing.Point(8, 64);
            this.tabStatusSections.Name = "tabStatusSections";
            this.tabStatusSections.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabStatusSections.OcxState")));
            this.tabStatusSections.Size = new System.Drawing.Size(220, 400);
            this.tabStatusSections.TabIndex = 3;
            this.tabStatusSections.SendLButtonClick += new AxTABLib._DTABEvents_SendLButtonClickEventHandler(this.tabStatusSections_SendLButtonClick);
            this.tabStatusSections.RowSelectionChanged += new AxTABLib._DTABEvents_RowSelectionChangedEventHandler(this.tabStatusSections_RowSelectionChanged);
            // 
            // tabDeparture
            // 
            this.tabDeparture.Location = new System.Drawing.Point(500, 0);
            this.tabDeparture.Name = "tabDeparture";
            this.tabDeparture.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabDeparture.OcxState")));
            this.tabDeparture.Size = new System.Drawing.Size(492, 48);
            this.tabDeparture.TabIndex = 2;
            this.tabDeparture.SendLButtonClick += new AxTABLib._DTABEvents_SendLButtonClickEventHandler(this.tabDeparture_SendLButtonClick);
            // 
            // tabArrival
            // 
            this.tabArrival.Location = new System.Drawing.Point(8, 0);
            this.tabArrival.Name = "tabArrival";
            this.tabArrival.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabArrival.OcxState")));
            this.tabArrival.Size = new System.Drawing.Size(492, 48);
            this.tabArrival.TabIndex = 1;
            this.tabArrival.SendLButtonClick += new AxTABLib._DTABEvents_SendLButtonClickEventHandler(this.tabArrival_SendLButtonClick);
            // 
            // picBody
            // 
            this.picBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picBody.Location = new System.Drawing.Point(0, 0);
            this.picBody.Name = "picBody";
            this.picBody.Size = new System.Drawing.Size(1028, 482);
            this.picBody.TabIndex = 0;
            this.picBody.TabStop = false;
            this.picBody.Paint += new System.Windows.Forms.PaintEventHandler(this.picBody_Paint);
            // 
            // mnuContext
            // 
            this.mnuContext.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemEnterTime,
            this.menuItemClearTime,
            this.menuItemSetCurrentTime,
            this.menuItem1,
            this.mnuItemAck});
            this.mnuContext.Popup += new System.EventHandler(this.mnuContext_Popup);
            // 
            // menuItemEnterTime
            // 
            this.menuItemEnterTime.Index = 0;
            this.menuItemEnterTime.Text = "&Enter Time";
            this.menuItemEnterTime.Click += new System.EventHandler(this.ctx_OpenTimePicker);
            // 
            // menuItemClearTime
            // 
            this.menuItemClearTime.Index = 1;
            this.menuItemClearTime.Text = "&Clear Time";
            this.menuItemClearTime.Click += new System.EventHandler(this.ctx_ClearTime);
            // 
            // menuItemSetCurrentTime
            // 
            this.menuItemSetCurrentTime.Index = 2;
            this.menuItemSetCurrentTime.Text = "&Set Current Time";
            this.menuItemSetCurrentTime.Click += new System.EventHandler(this.SetCurrentTime);
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 3;
            this.menuItem1.Text = "-";
            // 
            // mnuItemAck
            // 
            this.mnuItemAck.Index = 4;
            this.mnuItemAck.Text = "&Acknowledge";
            this.mnuItemAck.Click += new System.EventHandler(this.mnuItemAck_Click);
            // 
            // frmDetailStatus
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.AutoScroll = true;
            this.AutoScrollMinSize = new System.Drawing.Size(0, 280);
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(1028, 542);
            this.ContextMenu = this.mnuContext;
            this.Controls.Add(this.panelBody);
            this.Controls.Add(this.panelButtons);
            this.Controls.Add(this.lblHeaderInfo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(900, 320);
            this.Name = "frmDetailStatus";
            this.Text = "Status Manager: Flight Status ...";
            this.Load += new System.EventHandler(this.frmDetailStatus_Load);
            this.SizeChanged += new System.EventHandler(this.frmDetailStatus_SizeChanged);
            this.Closed += new System.EventHandler(this.frmDetailStatus_Closed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.frmDetailStatus_Closing);
            this.panelButtons.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picButtons)).EndInit();
            this.panelBody.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabTmp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabStatusSections)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabDeparture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabArrival)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBody)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private void ReadRegistry()
		{
			int myX=-1, myY=-1, myW=-1, myH=-1;
			string regVal = "";
			string theKey = "Software\\StatusManager\\DetailStatusInfo";
			RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			if(rk != null)
			{
				regVal = rk.GetValue("X", "-1").ToString();
				myX = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Y", "-1").ToString();
				myY = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Width", "-1").ToString();
				myW = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Height", "-1").ToString();
				myH = Convert.ToInt32(regVal);
			}
			if((myW != -1 && myH != -1) && (myX < Screen.PrimaryScreen.Bounds.Width))
			{
				this.Left = myX;
				this.Top = myY;
				this.Width = myW;
				this.Height = myH;
			}
		}
		private void WriteRegistry()
		{
			string theKey = "Software\\StatusManager\\DetailStatusInfo";
			RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			if(rk == null)
			{
				rk = Registry.CurrentUser.OpenSubKey("Software\\StatusManager", true);
				rk.CreateSubKey("DetailStatusInfo");
				rk.Close();
				theKey = "Software\\StatusManager\\DetailStatusInfo";
				rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			}

			rk.SetValue("X", this.Left.ToString());
			rk.SetValue("Y", this.Top.ToString());
			rk.SetValue("Width", this.Width.ToString());
			rk.SetValue("Height", this.Height.ToString());
		}

		private void frmDetailStatus_Load(object sender, System.EventArgs e)
		{
            ChangeTabSize();

			myDB = UT.GetMemDB();
			myAFT = myDB["AFT"];
			myHSS = myDB["HSS"];
			myOST = myDB["OST"];
			mySRH = myDB["SRH"];
		    mySDE = myDB["SDE"];

			dtCONU.Visible = false;
			dtCONU.CustomFormat = "HH:mm/dd";
			Ufis.Utils.IniFile ini = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			string strCFLTYPES_DB  = ini.IniReadValue("STATUSMANAGER", "CFLTYPES_DB");
			string strCFLTYPES_GUI = ini.IniReadValue("STATUSMANAGER", "CFLTYPES_GUI");
			myCFLTYPES_DB  = strCFLTYPES_DB.Split(',');
			myCFLTYPES_GUI = strCFLTYPES_GUI.Split(',');
			// Change "" => " "
			for(int i = 0; i < myCFLTYPES_DB.Length; i++)
			{
				if(myCFLTYPES_DB[i] == "") myCFLTYPES_DB[i] = " ";
			}

			ReadRegistry();

			myConflictTimerElapsed = new DE.ConflicTimerElapsed(frmMain_OnConflicTimerElapsed);
			DE.OnConflicTimerElapsed += myConflictTimerElapsed;//new Status_Manager.frmMain.ConflicTimerElapsed(frmMain_OnConflicTimerElapsed);
            //delegateOST_Changed = new Status_Manager.DE.OST_Changed(DE_OnOST_Changed);//AM 20080428 - Remark
            //DE.OnOST_Changed += delegateOST_Changed;//AM 20080428 - Remark
            delegateOST_ChangedForAFlight = new DE.OST_ChangedForAFlight(DE_OnOST_ChangedForAFlight);//AM 20080428
            DE.OnOST_ChangedForAFlight += delegateOST_ChangedForAFlight;//AM 20080428
            delegateUTC_LocalTimeChanged = new Status_Manager.DE.UTC_LocalTimeChanged(DE_OnUTC_LocalTimeChanged);
			DE.OnUTC_LocalTimeChanged += delegateUTC_LocalTimeChanged;
			delegateUpdateFlightData = new Status_Manager.DE.UpdateFlightData(DE_OnUpdateFlightData);
			DE.OnUpdateFlightData += delegateUpdateFlightData;

			btnCancel.Parent = picButtons;
			btnSave.Parent = picButtons;
			btnPrint.Parent = picButtons;
			btnBatchListProcessing.Parent = picButtons;
			InitTabs();

			FillFligthInfo();
			FillStatusSections();
			MakeSectionColors();
			if(myCurrOstURNO != "")
			{
				MarkCurrOst();
			}
			else
			{
				FillOnlineStatus();
			}
			string prv = CFC.LoginControl.GetPrivileges("CheckListProcessing");
			if(prv == "-" || prv == "0")
			{
				btnBatchListProcessing.Enabled = false;
			}
		}

		private void MarkCurrOst()
		{
			IRow [] rows = myOST.RowsByIndexValue("URNO", myCurrOstURNO);
			if(rows.Length > 0)
			{
				for( int i = 0; i < tabStatusSections.GetLineCount(); i++)
				{
					if( rows[0]["UHSS"] == tabStatusSections.GetFieldValue(i, "URNO"))
					{
						tabStatusSections.SetCurrentSelection(i);
						i = tabStatusSections.GetLineCount()+1;
					}
				}
			}
		}

		private void InitTabs()
		{
			// Flight Arrival
			string strColors = UT.colLightGray.ToString();
			tabArrival.ResetContent();
			tabArrival.MainHeader = true;
			tabArrival.HeaderString = "URNO,RKEY,FLNO,STA,ETA,ONBL,Na,Org,Via,Pos,Gate,A/C";
			tabArrival.LogicalFieldList = "URNO,RKEY,FLNO,STOA,ETAI,ONBL,NA,ORIG,VIA,POS,GATE,ACT3";
			tabArrival.HeaderLengthString = "0,0,50,50,50,50,40,40,50,50,50,80";
			tabArrival.SetMainHeaderValues("12", "Arrival", strColors);
			tabArrival.ShowVertScroller(false);
			tabArrival.ShowHorzScroller(false);
			tabArrival.LifeStyle = true;
			tabArrival.FontName = "Arial";
			tabArrival.FontSize = 14;
			tabArrival.HeaderFontSize = 14;
			tabArrival.EnableHeaderSizing (true);
			tabArrival.SetTabFontBold(true);
			tabArrival.LeftTextOffset = 2;
			tabArrival.LineHeight = 16;
			int idx = 0;
			idx = UT.GetItemNo(tabArrival.LogicalFieldList, "STOA");
			tabArrival.DateTimeSetColumnFormat( idx , "YYYYMMDDhhmmss", "hh':'mm'/'DD");
			idx = UT.GetItemNo(tabArrival.LogicalFieldList, "ETAI");
			tabArrival.DateTimeSetColumnFormat( idx , "YYYYMMDDhhmmss", "hh':'mm'/'DD");
			idx = UT.GetItemNo(tabArrival.LogicalFieldList, "ONBL");
			tabArrival.DateTimeSetColumnFormat( idx , "YYYYMMDDhhmmss", "hh':'mm'/'DD");

			//Flight Departure
			tabDeparture.ResetContent();
			tabDeparture.MainHeader = true;
			tabDeparture.HeaderString = "URNO,RKEY,FLNO,STD,ETD,OFBL,Na,Via,Des.,Pos,Gate,A/C";
			tabDeparture.LogicalFieldList = "URNO,RKEY,FLNO,STOD,ETDI,OFBL,NA,VIA,DES,POS,GATE,ACT";
			tabDeparture.HeaderLengthString = "0,0,50,50,50,50,40,40,50,50,50,80";
			tabDeparture.SetMainHeaderValues("12", "Departure", strColors);
			tabDeparture.ShowVertScroller(false);
			tabDeparture.ShowHorzScroller(false);
			tabDeparture.LifeStyle = true;
			tabDeparture.FontName = "Arial";
			tabDeparture.FontSize = 14;
			tabDeparture.HeaderFontSize = 14;
			tabDeparture.EnableHeaderSizing (true);
			tabDeparture.SetTabFontBold(true);
			tabDeparture.LeftTextOffset = 2;
			tabDeparture.LineHeight = 16;
			idx = UT.GetItemNo(tabDeparture.LogicalFieldList, "STOD");
			tabDeparture.DateTimeSetColumnFormat( idx , "YYYYMMDDhhmmss", "hh':'mm'/'DD");
			idx = UT.GetItemNo(tabDeparture.LogicalFieldList, "ETDI");
			tabDeparture.DateTimeSetColumnFormat( idx , "YYYYMMDDhhmmss", "hh':'mm'/'DD");
			idx = UT.GetItemNo(tabDeparture.LogicalFieldList, "OFBL");
			tabDeparture.DateTimeSetColumnFormat( idx , "YYYYMMDDhhmmss", "hh':'mm'/'DD");

			//Single Statuses	
			tabStatus.ResetContent();
			tabStatus.LifeStyle = true;
			tabStatus.MainHeader = true;
			tabStatus.HeaderString	     = "UNRO,USDE,UAFT,Expected,System,User,Rule-Name,Ref.,Stat.-Name,CFLTYPE_DB,Cfl.-Type,Time,Sort,Remark,Last Upd.,User";
			tabStatus.LogicalFieldList   = "URNO,USDE,UAFT,CONS,CONA,CONU,RULNAME,REFFIELD,STATUSNAME,CFLTYPE_DB,CONFLICTTYPE,TIME,SORT,REMA,LSTU,USEU";
			tabStatus.HeaderLengthString = "0,0,0,60,60,80,200,0,200,0,60,50,0,200,80,80";
			tabStatus.ColumnWidthString  = "10,10,10,14,14,14,32,10,32,10,10,10,10,64,14,32";
			tabStatus.ColumnAlignmentString = "L,L,L,L,L,L,L,L,L,L,L,L,R,L,L,L";
			tabStatus.NoFocusColumns = "0,1,2,3,4,5,6,7,8,9,10,11,12,14,15";
			tabStatus.ShowHorzScroller(true);
            tabStatus.ShowVertScroller(true);
            //_tabFieldArray = new TabFieldArray(tabStatus.LogicalFieldList, tabStatus.HeaderLengthString);
			tabStatus.LeftTextOffset = 2;
			tabStatus.EnableInlineEdit(true);  
			tabStatus.InplaceEditUpperCase = false;
			tabStatus.SetMainHeaderValues("3,3," + (UT.ItemCount(tabStatus.LogicalFieldList, ",")-6).ToString(), 
										 "Keys,Times,Handling Status", "0,0,0");
			tabStatus.FontName = "Arial";
			tabStatus.FontSize = 14;
			tabStatus.LineHeight = 18;
			tabStatus.HeaderFontSize = 14;
			tabStatus.SetTabFontBold(true);
			strColors = UT.colBlue + "," + UT.colBlue;
			tabStatus.CursorDecoration(tabStatus.LogicalFieldList, "B,T", "2,2", strColors);
			tabStatus.DefaultCursor = false;
			int idxField = UT.GetItemNo(tabStatus.LogicalFieldList, "CONA");
			if(idxField > -1)
			{
				tabStatus.DateTimeSetColumnFormat(idxField, "YYYYMMDDhhmmss", "hh':'mm'/'DD");
			}
			idxField = UT.GetItemNo(tabStatus.LogicalFieldList, "CONS");
			if(idxField > -1)
			{
				tabStatus.DateTimeSetColumnFormat(idxField, "YYYYMMDDhhmmss", "hh':'mm'/'DD");
			}
			idxField = UT.GetItemNo(tabStatus.LogicalFieldList, "CONU");
			if(idxField > -1)
			{
				tabStatus.DateTimeSetColumnFormat(idxField, "YYYYMMDDhhmmss", "hh':'mm'/'DD");
			}
			idxField = UT.GetItemNo(tabStatus.LogicalFieldList, "LSTU");
			if(idxField > -1)
			{
				tabStatus.DateTimeSetColumnFormat(idxField, "YYYYMMDDhhmmss", "hh':'mm'/'DD'.'MM'.'YY");
			}

		}
		private void picButtons_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, picButtons, Color.WhiteSmoke, Color.Gray);		
		}

		private void picBody_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, picBody, Color.WhiteSmoke, Color.LightGray);		
		}

		public void SetData(string strUrno, string strRkey, string strOstUrno)
		{
			myCurrRKEY = strRkey;
			myCurrURNO = strUrno;
			myCurrOstURNO = strOstUrno;
			FillFligthInfo();
			FillStatusSections();
			if(myCurrOstURNO != "")
			{
				MarkCurrOst();
			}
			else
			{
				FillOnlineStatus();
			}
			MakeSectionColors();
		}

        private void MakeSectionColors()
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            //if (this.textBox1.InvokeRequired)
            if (this.InvokeRequired)
            {
                RefreshCallback d = new RefreshCallback(MakeSectionColors);
                //this.Invoke(d, new object[] {  });
                this.Invoke(d);
            }
            else
            {
                try
                {
                    myOST.Lock.AcquireReaderLock(System.Threading.Timeout.Infinite);
                    int i = 0;
                    int j = 0;
                    bool blFound = false;
                    IRow[] ostRows = myOST.RowsByIndexValue("UAFT", myCurrURNO);
                    int cnt = tabStatusSections.GetLineCount();
                    for (i = 0; i < cnt; i++)
                    {
                        blFound = false;
                        int redCount = 0;
                        int orangeCount = 0;
                        int greenCount = 0;
                        int blueCount = 0;
                        for (j = 0; j < ostRows.Length /*&& blFound == false*/; j++)
                        {
                            if (ostRows[j]["UHSS"] == tabStatusSections.GetColumnValue(i, 0))
                            {
                                switch (ostRows[j]["COTY"])
                                {
                                    case "N":
                                        redCount++;
                                        break;
                                    case "A":
                                        orangeCount++;
                                        break;
                                    case "C":
                                        greenCount++;
                                        break;
                                    default:
                                        blueCount++;
                                        break;
                                }
                                //tabStatusSections.SetLineColor(i, UT.colBlack, UT.colCyan);
                                blFound = true; ;
                            }
                        }
                        if (blFound == true)
                        {
                            if (redCount > 0)
                                tabStatusSections.SetLineColor(i, UT.colWhite, UT.colRed);
                            else if (redCount == 0 && orangeCount > 0)
                                tabStatusSections.SetLineColor(i, UT.colBlack, UT.colLightOrange);
                            else if (redCount == 0 && orangeCount == 0 && greenCount > 0)
                                tabStatusSections.SetLineColor(i, UT.colBlack, UT.colLightGreen);
                            else if (redCount == 0 && orangeCount == 0 && greenCount == 0)
                                tabStatusSections.SetLineColor(i, UT.colBlack, UT.colCyan);
                        }
                        else
                        {
                            tabStatusSections.SetLineColor(i, UT.colBlack, UT.colWhite);
                        }
                    }
                    tabStatusSections.Refresh();

                }
                catch (Exception ex)
                {
                    LogExceptionMsg(ex,"");
                }
                finally
                {
                    if (myOST.Lock.IsReaderLockHeld) myOST.Lock.ReleaseReaderLock();
                }
            }
        }

		private void FillFligthInfo()
		{
            try
            {
                myAFT.Lock.AcquireReaderLock(System.Threading.Timeout.Infinite);
                IRow[] rows = myAFT.RowsByIndexValue("RKEY", myCurrRKEY);
                tabArrival.ResetContent();
                tabDeparture.ResetContent();
                if (rows.Length > 0)
                {
                    for (int i = 0; i < rows.Length; i++)
                    {
                        string strValues = "";
                        if (rows[i]["ADID"] == "A")
                        {
                            strValues = rows[i]["URNO"] + ",";
                            strValues += rows[i]["RKEY"] + ",";
                            strValues += rows[i]["FLNO"] + ",";
                            strValues += rows[i]["STOA"] + ",";
                            strValues += rows[i]["ETAI"] + ",";
                            strValues += rows[i]["ONBL"] + ",";
                            strValues += rows[i]["TTYP"] + ",";
                            strValues += rows[i]["ORG3"] + ",";
                            strValues += rows[i]["VIA3"] + ",";
                            strValues += rows[i]["PSTA"] + ",";
                            strValues += rows[i]["GTA1"] + ",";
                            strValues += rows[i]["ACT3"];
                            tabArrival.InsertTextLine(strValues, true);
                            if (rows[i]["URNO"] == myCurrURNO)
                            {
                                tabDeparture.SetCurrentSelection(-1);
                                tabArrival.SetCurrentSelection(0);
                            }
                            //"URNO,RKEY,FLNO,STOA,ETAI,ONBL,NA,ORIG,VIA,POS,GATE,ACT3";
                        }
                        if (rows[i]["ADID"] == "D")
                        {
                            strValues = rows[i]["URNO"] + ",";
                            strValues += rows[i]["RKEY"] + ",";
                            strValues += rows[i]["FLNO"] + ",";
                            strValues += rows[i]["STOD"] + ",";
                            strValues += rows[i]["ETDI"] + ",";
                            strValues += rows[i]["OFBL"] + ",";
                            strValues += rows[i]["TTYP"] + ",";
                            strValues += rows[i]["VIA3"] + ",";
                            strValues += rows[i]["DES3"] + ",";
                            strValues += rows[i]["PSTD"] + ",";
                            strValues += rows[i]["GTD1"] + ",";
                            strValues += rows[i]["ACT3"];
                            tabDeparture.InsertTextLine(strValues, true);
                            if (rows[i]["URNO"] == myCurrURNO)
                            {
                                tabArrival.SetCurrentSelection(-1);
                                tabDeparture.SetCurrentSelection(0);
                            }
                            //"URNO,RKEY,FLNO,STOD,ETDI,OFBL,NA,VIA,DES,POS,GATE,ACT";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogExceptionMsg(ex,"");
            }
            finally
            {
                if (myAFT.Lock.IsReaderLockHeld) myAFT.Lock.ReleaseReaderLock();
            }
			tabArrival.Refresh();
			tabDeparture.Refresh();

		}
		private void FillStatusSections()
		{
            if (this.InvokeRequired)
            {
                RefreshCallback d = new RefreshCallback(FillStatusSections);
                //this.Invoke(d, new object[] {  });
                this.Invoke(d);
            }
            else
            {
                try
                {
                    myHSS.Lock.AcquireReaderLock(System.Threading.Timeout.Infinite);
                    string strColor = UT.colGray.ToString();
                    tabStatusSections.ResetContent();
                    tabStatusSections.MainHeader = true;
                    tabStatusSections.HeaderString = "URNO,NAME";
                    tabStatusSections.HeaderLengthString = "0,350";
                    tabStatusSections.LogicalFieldList = tabStatusSections.HeaderString;
                    tabStatusSections.SetMainHeaderValues("2", "Status Sections", strColor);
                    tabStatusSections.LineHeight = 18;
                    tabStatusSections.FontName = "Arial";
                    tabStatusSections.FontSize = 14;
                    tabStatusSections.HeaderFontSize = 14;
                    tabStatusSections.SetTabFontBold(true);
                    tabStatusSections.LifeStyle = true;
                    string strColors = UT.colBlue + "," + UT.colBlue;
                    tabStatusSections.CursorDecoration(tabStatusSections.LogicalFieldList, "B,T", "2,2", strColors);
                    tabStatusSections.DefaultCursor = false;

                    tabStatusSections.InsertTextLine("-1,<*** ALL SECTIONS ***>", false);
                    for (int i = 0; i < myHSS.Count; i++)
                    {
                        tabStatusSections.InsertTextLine(myHSS[i]["URNO"] + "," + myHSS[i]["NAME"], true);
                    }
                    int idx = tabStatusSections.GetCurrentSelected();
                    if (idx == -1)
                        tabStatusSections.SetCurrentSelection(0);

                }
                catch (Exception ex)
                {
                    LogExceptionMsg(ex,"");
                }
                finally
                {
                    if(myHSS.Lock.IsReaderLockHeld) myHSS.Lock.ReleaseReaderLock();
                }
            }
		}
		private void FillOnlineStatus()
		{
            if (this.InvokeRequired)
            {
                RefreshCallback d = new RefreshCallback(FillOnlineStatus);
                //this.Invoke(d, new object[] {  });
                this.Invoke(d);
            }
            else
            {
                try
                {
                    myOST.Lock.AcquireReaderLock(System.Threading.Timeout.Infinite);
                    mySDE.Lock.AcquireReaderLock(System.Threading.Timeout.Infinite);
                    mySRH.Lock.AcquireReaderLock(System.Threading.Timeout.Infinite);
                    int i = 0;
                    int idx = tabStatusSections.GetCurrentSelected();
                    int idxStatus = tabStatus.GetCurrentSelected();
                    string strSelHSS = "";
                    StringBuilder strValues = new StringBuilder(5000);
                    IRow[] ostRows = myOST.RowsByIndexValue("UAFT", myCurrURNO);
                    string strLine = "";
                    string strStatName = "";
                    string strRuleName = "";
                    DateTime dat;
                    DateTime datCONS;
                    string strCONS = "";
                    string strCONA = "";
                    string strCONU = "";
                    int ilSort = 0;
                    string strSort = "";
                    string strMinutes = "";
                    string strCOTY = "";
                    string strCOTY_GUI = "";

                    if (UT.IsTimeInUtc == true)
                        dat = CFC.GetUTC();
                    else
                        dat = DateTime.Now;

                    if (idx > -1)
                    {
                        if (tabStatusSections.GetColumnValue(idx, 1) == "<*** ALL SECTIONS ***>")
                        {
                            idx = -1;
                        }
                        else
                        {
                            strSelHSS = tabStatusSections.GetColumnValue(idx, 0);
                        }
                    }
                    tabStatus.ResetContent();
                    tabTmp.ResetContent();
                    tabTmp.HeaderString = "URNO";
                    tabTmp.LogicalFieldList = "URNO";
                    tabTmp.HeaderLengthString = "100";
                    tabTmp.SetUniqueFields("URNO");
                    tabTmp.ShowVertScroller(false);
                    if (idx == -1)
                    {//Nothing selected, so show all Flights according to the current
                        //selected Flight URNO
                        //"URNO,USDE,UAFT,RULNAME,REFFIELD,STATUSNAME,CONFLICTTYPE";
                        for (i = 0; i < ostRows.Length; i++)
                        {
                            strCONS = "";
                            strCONA = "";
                            strCONU = "";
                            IRow[] sdeRow = mySDE.RowsByIndexValue("URNO", ostRows[i]["USDE"]);
                            if (sdeRow.Length > 0)
                            {
                                strStatName = sdeRow[0]["NAME"];
                            }
                            IRow[] srhRow = mySRH.RowsByIndexValue("URNO", ostRows[i]["USRH"]);
                            if (srhRow.Length > 0)
                            {
                                strRuleName = srhRow[0]["NAME"];
                            }
                            strCONS = ostRows[i]["CONS"];
                            strCONA = ostRows[i]["CONA"];
                            strCONU = ostRows[i]["CONU"];
                            strLine = ostRows[i]["URNO"] + ",";
                            strLine += ostRows[i]["USDE"] + ",";
                            strLine += ostRows[i]["UAFT"] + ",";
                            strLine += ostRows[i]["CONS"] + "," + ostRows[i]["CONA"] + ",";
                            strLine += ostRows[i]["CONU"] + ",";
                            strLine += strRuleName + ",";
                            strLine += ostRows[i]["RFLD"] + ",";
                            strLine += strStatName + ",";
                            strCOTY = ostRows[i]["COTY"];
                            if (strCOTY == "") strCOTY = " ";
                            strLine += strCOTY + ",";
                            strCOTY_GUI = "";
                            for (int j = 0; j < myCFLTYPES_DB.Length; j++)
                            {
                                if (myCFLTYPES_DB[j] == strCOTY)
                                {
                                    strCOTY_GUI = myCFLTYPES_GUI[j];
                                }
                            }
                            strLine += strCOTY_GUI + ",";

                            if (strCONU.Length == 14)
                            {
                                //CONU - CONS but only if CONU is set
                                DateTime datCONU = UT.CedaFullDateToDateTime(strCONU);
                                datCONS = UT.CedaFullDateToDateTime(strCONS);
                                TimeSpan olTS = datCONU - datCONS;
                                ilSort = 100000 - (int)olTS.TotalMinutes;
                                strSort = ilSort.ToString();
                                strMinutes = ((int)olTS.TotalMinutes).ToString();
                            }
                            else if (strCONA.Length == 14)
                            {
                                //CONU - CONS but only if CONU is set
                                DateTime datCONA = UT.CedaFullDateToDateTime(strCONA);
                                datCONS = UT.CedaFullDateToDateTime(strCONS);
                                TimeSpan olTS = datCONA - datCONS;
                                ilSort = 100000 - (int)olTS.TotalMinutes;
                                strSort = ilSort.ToString();
                                strMinutes = ((int)olTS.TotalMinutes).ToString();
                            }
                            else if (strCONS.Length == 14)
                            {
                                datCONS = UT.CedaFullDateToDateTime(strCONS);
                                TimeSpan olTS = dat - datCONS;
                                ilSort = 100000 - (int)olTS.TotalMinutes;
                                strSort = ilSort.ToString();
                                strMinutes = ((int)olTS.TotalMinutes).ToString();
                            }
                            else
                            {
                                strMinutes = "";
                                ilSort = 1000000;
                                strSort = ilSort.ToString();
                                //strMinutes.PadLeft(5, ' ');
                            }
                            strLine += strMinutes + "," + strSort + "," + ostRows[i]["REMA"] + "," + ostRows[i]["LSTU"] + "," + ostRows[i]["USEU"] + "\n";
                            tabTmp.InsertTextLine(ostRows[i]["UHSS"], false);
                            strValues.Append(strLine);
                        }
                    }
                    else
                    {
                        //Fill the tab for the current selected Status Section
                        for (i = 0; i < ostRows.Length; i++)
                        {
                            strCONS = "";
                            strCONA = "";
                            strCONU = "";
                            if (ostRows[i]["UHSS"] == strSelHSS)
                            {
                                IRow[] sdeRow = mySDE.RowsByIndexValue("URNO", ostRows[i]["USDE"]);
                                if (sdeRow.Length > 0)
                                {
                                    strStatName = sdeRow[0]["NAME"];
                                }
                                IRow[] srhRow = mySRH.RowsByIndexValue("URNO", ostRows[i]["USRH"]);
                                if (srhRow.Length > 0)
                                {
                                    strRuleName = srhRow[0]["NAME"];
                                }
                                strCONS = ostRows[i]["CONS"];
                                strCONA = ostRows[i]["CONA"];
                                strCONU = ostRows[i]["CONU"];

                                strLine = ostRows[i]["URNO"] + ",";
                                strLine += ostRows[i]["USDE"] + ",";
                                strLine += ostRows[i]["UAFT"] + ",";
                                strLine += ostRows[i]["CONS"] + "," + ostRows[i]["CONA"] + ",";
                                strLine += ostRows[i]["CONU"] + ",";
                                strLine += strRuleName + ",";
                                strLine += ostRows[i]["RFLD"] + ",";
                                strLine += strStatName + ",";
                                strCOTY = ostRows[i]["COTY"];
                                if (strCOTY == "") strCOTY = " ";
                                strLine += strCOTY + ",";
                                strCOTY_GUI = "";
                                for (int j = 0; j < myCFLTYPES_DB.Length; j++)
                                {
                                    if (myCFLTYPES_DB[j] == strCOTY)
                                    {
                                        strCOTY_GUI = myCFLTYPES_GUI[j];
                                    }
                                }
                                strLine += strCOTY_GUI + ",";

                                if (strCONU.Length == 14)
                                {
                                    //CONU - CONS but only if CONU is set
                                    DateTime datCONU = UT.CedaFullDateToDateTime(strCONU);
                                    datCONS = UT.CedaFullDateToDateTime(strCONS);
                                    TimeSpan olTS = datCONU - datCONS;
                                    ilSort = 100000 - (int)olTS.TotalMinutes;
                                    strSort = ilSort.ToString();
                                    strMinutes = ((int)olTS.TotalMinutes).ToString();
                                }
                                else if (strCONA.Length == 14)
                                {
                                    //CONU - CONS but only if CONU is set
                                    DateTime datCONA = UT.CedaFullDateToDateTime(strCONA);
                                    datCONS = UT.CedaFullDateToDateTime(strCONS);
                                    TimeSpan olTS = datCONA - datCONS;
                                    ilSort = 100000 - (int)olTS.TotalMinutes;
                                    strSort = ilSort.ToString();
                                    strMinutes = ((int)olTS.TotalMinutes).ToString();
                                }
                                else if (strCONS.Length == 14)
                                {
                                    datCONS = UT.CedaFullDateToDateTime(strCONS);
                                    TimeSpan olTS = dat - datCONS;
                                    ilSort = 100000 - (int)olTS.TotalMinutes;
                                    strSort = ilSort.ToString();
                                    strMinutes = ((int)olTS.TotalMinutes).ToString();
                                }
                                else
                                {
                                    strMinutes = "";
                                    ilSort = 1000000;
                                    strSort = ilSort.ToString();
                                    //strMinutes.PadLeft(5, ' ');
                                }
                                strLine += strMinutes + "," + strSort + "," + ostRows[i]["REMA"] + "," + ostRows[i]["LSTU"] + "," + ostRows[i]["USEU"] + "\n";
                                tabTmp.InsertTextLine(ostRows[i]["UHSS"], false);
                                strValues.Append(strLine);
                            }
                        }
                    }
                    tabStatusSections.Refresh();
                    tabStatus.InsertBuffer(strValues.ToString(), "\n");
                    idx = UT.GetItemNo(tabStatus.LogicalFieldList, "SORT");
                    tabStatus.Sort(idx.ToString(), true, true);

                    for (i = 0; i < tabStatus.GetLineCount(); i++)
                    {
                        int lineColor = 0;
                        int textColor = 0;
                        strCOTY = tabStatus.GetFieldValue(i, "CFLTYPE_DB");//myOST[i]["COTY"];
                        textColor = UT.colBlack;
                        switch (strCOTY)
                        {
                            case "N":
                                lineColor = UT.colRed;
                                textColor = UT.colWhite;
                                break;
                            case "A":
                                lineColor = UT.colLightOrange;
                                break;
                            case "C":
                                lineColor = UT.colLightGreen;
                                break;
                            default:
                                lineColor = UT.colWhite;
                                break;
                        }
                        tabStatus.SetLineColor(i, textColor, lineColor);
                    }
                    tabStatus.Refresh();

                    if (idxStatus > -1 && idxStatus < tabStatus.GetLineCount())
                        tabStatus.SetCurrentSelection(idxStatus);
                    else
                        tabStatus.SetCurrentSelection(0);
                }
                catch (Exception ex)
                {
                    LogExceptionMsg(ex,"");
                }
                finally
                {
                    if (mySRH.Lock.IsReaderLockHeld) mySRH.Lock.ReleaseReaderLock();
                    if (mySDE.Lock.IsReaderLockHeld) mySDE.Lock.ReleaseReaderLock();
                    if (myOST.Lock.IsReaderLockHeld) myOST.Lock.ReleaseReaderLock();
                }
            }
		}

        private void RemoveEvents()
        {
            UT.LogMsg("frmDetailStatus:RemoveEvents:Start");
            //Remove Delegate is very important!! Otherwise Unhandled Exception
            if (myConflictTimerElapsed != null) { DE.OnConflicTimerElapsed -= myConflictTimerElapsed; myConflictTimerElapsed = null; }
            if (delegateOST_Changed != null) DE.OnOST_Changed -= delegateOST_Changed;
            if (delegateOST_ChangedForAFlight != null) DE.OnOST_ChangedForAFlight -= delegateOST_ChangedForAFlight;
            if (delegateUTC_LocalTimeChanged != null) DE.OnUTC_LocalTimeChanged -= delegateUTC_LocalTimeChanged;
            if (delegateUpdateFlightData != null) DE.OnUpdateFlightData -= delegateUpdateFlightData;
            UT.LogMsg("frmDetailStatus:RemoveEvents:Finish");
        }

		private void frmDetailStatus_Closed(object sender, System.EventArgs e)
		{
            UT.LogMsg("frmDetailStatus:Closing");
            try
            {
                DE.Call_StatusDetailClosed();
            }
            catch (Exception)
            {
            }

            try
            {
                WriteRegistry();
            }
            catch (Exception)
            {
            }
            UT.LogMsg("frmDetailStatus:Closed");
		}

		private void tabArrival_SendLButtonClick(object sender, AxTABLib._DTABEvents_SendLButtonClickEvent e)
		{ 
			if(e.lineNo == 0)
			{
				tabDeparture.SetCurrentSelection(-1);
				myCurrURNO = tabArrival.GetFieldValue(0, "URNO");
				tabArrival.Refresh();
				tabDeparture.Refresh();
				MakeSectionColors();
				FillOnlineStatus();
			}
		}

		private void tabDeparture_SendLButtonClick(object sender, AxTABLib._DTABEvents_SendLButtonClickEvent e)
		{
			if(e.lineNo == 0)
			{
				tabArrival.SetCurrentSelection(-1);
				myCurrURNO = tabDeparture.GetFieldValue(0, "URNO");
				tabArrival.Refresh();
				tabDeparture.Refresh();
				MakeSectionColors();
				FillOnlineStatus(); 
			}
		}

		private void tabStatusSections_SendLButtonClick(object sender, AxTABLib._DTABEvents_SendLButtonClickEvent e)
		{
			MakeSectionColors();
			FillOnlineStatus(); 
			if(myCurrOstURNO != "")
			{
				for(int i = 0; i < tabStatus.GetLineCount(); i++)
				{
					if(myCurrOstURNO == tabStatus.GetFieldValue(i, "URNO"))
					{
						tabStatus.SetCurrentSelection(i);
						i = tabStatus.GetLineCount()+1;
					}
				}
			}
		}

		private void tabStatusSections_RowSelectionChanged(object sender, AxTABLib._DTABEvents_RowSelectionChangedEvent e)
		{
			MakeSectionColors();
			FillOnlineStatus();
			if(myCurrOstURNO != "")
			{
				for(int i = 0; i < tabStatus.GetLineCount(); i++)
				{
					if(myCurrOstURNO == tabStatus.GetFieldValue(i, "URNO"))
					{
						tabStatus.SetCurrentSelection(i);
						i = tabStatus.GetLineCount()+1;
					}
				}
			}
		}

		private void frmMain_OnConflicTimerElapsed()
		{
			FillOnlineStatus();
		}

		private void tabStatus_ComboSelChanged(object sender, AxTABLib._DTABEvents_ComboSelChangedEvent e)
		{
            string strCOTY = "";
            for (int i = 0; i < myCFLTYPES_GUI.Length; i++)
            {
                if (myCFLTYPES_GUI[i] == e.newValues)
                {
                    strCOTY = myCFLTYPES_DB[i];
                    SaveOstCOTY( e.lineNo, strCOTY );
                    break;
                }
            }
            
            //string strURNO = tabStatus.GetFieldValue(e.lineNo, "URNO");
            //try
            //{
            //    myOST.Lock.AcquireWriterLock(System.Threading.Timeout.Infinite);
            //    IRow[] ostRows = myOST.RowsByIndexValue("URNO", strURNO);
            //    if (ostRows.Length > 0)
            //    {
            //        string strCOTY = "";
            //        for (int i = 0; i < myCFLTYPES_GUI.Length; i++)
            //        {
            //            if (myCFLTYPES_GUI[i] == e.newValues)
            //            {
            //                strCOTY = myCFLTYPES_DB[i];
            //                ostRows[0]["COTY"] = strCOTY;
            //                ostRows[0]["LSTU"] = UT.DateTimeToCeda(CFC.GetUTC());
            //                ostRows[0]["USEU"] = UT.UserName;
            //                ostRows[0].Status = State.Modified;
            //                myOST.Save();
            //                //DE.Call_OST_Changed( this, strURNO, State.Modified);
            //                return;
            //            }
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    LogExceptionMsg(ex,"");
            //}
            //finally
            //{
            //    myOST.Lock.ReleaseWriterLock();
            //}
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			this.Close();
			DE.Call_StatusDetailClosed();
		}

		private void tabStatus_RowSelectionChanged(object sender, AxTABLib._DTABEvents_RowSelectionChangedEvent e)
		{
			dtCONU.Visible = false;
			if(e.lineNo > -1)
			{              
			}
		}

		private void tabStatus_OnVScroll(object sender, AxTABLib._DTABEvents_OnVScrollEvent e)
		{
			myTabScrollPos = e.lineNo;
		}


		private void tabStatus_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			string prv = CFC.LoginControl.GetPrivileges("EditDetailStatus");
			if(prv == "-" || prv == "0")
			{
				dtCONU.Visible = false;
				return;
			}
			if(e.lineNo > -1 && e.colNo == UT.GetItemNo(tabStatus.LogicalFieldList, "CONU"))
			{
				SetTimePicker();
			}
			else
			{
				dtCONU.Visible = false;
			}
		
		}
		

		private void SetTimePicker()
		{
			int currSel = tabStatus.GetCurrentSelected();
			if(currSel > -1)
			{
				int i = 0;
				string strCONU = tabStatus.GetFieldValue(currSel, "CONU");
				int tmpLeft = 0;
				int tmpRight = 0;
				int tmpFaktor = currSel - myTabScrollPos + 2;
				int tmpTop = tabStatus.Top + (tmpFaktor * tabStatus.LineHeight);
				string [] arrHeader = tabStatus.HeaderLengthString.Split(',');
				int idx = UT.GetItemNo(tabStatus.LogicalFieldList, "CONU");
				for( i = 0; i < idx; i++)
				{
					tmpLeft += Convert.ToInt32( arrHeader[i]);
				}
				tmpLeft += tabStatus.Left;
				tmpRight = tmpLeft + Convert.ToInt32( arrHeader[idx]);
				dtCONU.Top = tmpTop;
				dtCONU.Left = tmpLeft;
				dtCONU.Width = tmpRight - tmpLeft;

				DateTime datCONU;
				if(strCONU != "")
				{
					datCONU = UT.CedaFullDateToDateTime(strCONU);
				}
				else
				{
					if(UT.IsTimeInUtc == true)
						datCONU = CFC.GetUTC();
					else
						datCONU = DateTime.Now;
				}
				dtCONU.Value = datCONU;
				dtCONU.ShowUpDown = true;
				dtCONU.Visible = true;
				dtCONU.Select();
			}
		}

        //private void dtCONU_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        //{
        //    if (e.KeyChar == (char)13)
        //    {
        //        string msg = "";
        //        DateTime t1 = DateTime.Now;
        //        DateTime t2;
        //        DateTime t3 = t1;
        //        TimeSpan ts;
        //        try
        //        {
        //            int idx = tabStatus.GetCurrentSelected();
        //            if (idx > -1)
        //            {
        //                t1 = DateTime.Now;
        //                UT.LogMsg(string.Format("--------------------------------- Start {0:00}:{1:00}.{2:000}", t1.Minute, t1.Second,t1.Millisecond ));
        //                t3 = t1;
        //                myOST.Lock.AcquireWriterLock(System.Threading.Timeout.Infinite);
        //                t2 = DateTime.Now;
        //                ts = t2.Subtract(t1);
                        
        //                msg += "Lock time : " + ts.TotalMilliseconds + Environment.NewLine;
        //                t1 = t2;
        //                IRow[] rows = myOST.RowsByIndexValue("URNO", tabStatus.GetFieldValue(idx, "URNO"));
        //                if (rows.Length > 0)
        //                {
        //                    t2 = DateTime.Now;
        //                    ts = t2.Subtract(t1);
        //                    msg += "Get Row time : " + ts.TotalMilliseconds + Environment.NewLine;
        //                    t1 = t2;

        //                    string strCONU = UT.DateTimeToCeda(dtCONU.Value);
        //                    UT.LogMsg("Update user time [" + strCONU + "] to flight " + rows[0]["URNO"]);
        //                    //strCONU = strCONU.Substring(0, 8);
        //                    rows[0]["CONU"] = strCONU;
        //                    rows[0]["LSTU"] = UT.DateTimeToCeda(CFC.GetUTC());
        //                    rows[0]["USEU"] = UT.UserName;
        //                    rows[0].Status = State.Modified;

        //                    t2 = DateTime.Now;
        //                    ts = t2.Subtract(t1);
        //                    msg += "After Status time : " + ts.TotalMilliseconds + Environment.NewLine;
        //                    t1 = t2;

        //                    tabStatus.SetFieldValues(idx, "CONU", strCONU);
        //                    UT.LogMsg("After status change");
        //                    tabStatus.Refresh();
        //                    UT.LogMsg("Before Save");

        //                    t2 = DateTime.Now;
        //                    ts = t2.Subtract(t1);
        //                    msg += "After refresh time : " + ts.TotalMilliseconds + Environment.NewLine;
        //                    t1 = t2;

        //                    //myOST.Save();                            
        //                    myOST.Save(rows[0]);

        //                    t2 = DateTime.Now;
        //                    ts = t2.Subtract(t1);
        //                    msg += "After Save time : " + ts.TotalMilliseconds + Environment.NewLine;
        //                    t1 = t2;
        //                    UT.LogMsg("After Save user time [" + strCONU + "] to flight " + rows[0]["URNO"]);

        //                    //Do not call because we wait for the broadcast
        //                    //DE.Call_OST_Changed(this, strURNO, State.Modified);

        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {

        //            LogExceptionMsg(ex);
        //        }
        //        finally
        //        {
        //            myOST.Lock.ReleaseWriterLock();
        //            t2 = DateTime.Now;
        //            ts = t2.Subtract(t1);
        //            msg += "After Unlock time : " + ts.TotalMilliseconds + Environment.NewLine;
        //            t2 = DateTime.Now;
        //            ts = t2.Subtract(t3);
        //            msg += "Total time : " + ts.TotalMilliseconds + Environment.NewLine;
        //            //MessageBox.Show(msg);
                    
        //            UT.LogMsg(string.Format("--------------------------------- End {0:00}:{1:00}.{2:000}", t1.Minute, t1.Second, t1.Millisecond)+Environment.NewLine + msg );
        //        }
        //    }
        //}

        private void dtCONU_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {

                int idx = tabStatus.GetCurrentSelected();
                if (idx > -1)
                {
                    string stConu = UT.DateTimeToCeda(dtCONU.Value);
                    SaveOstCONU(idx, stConu);
                }
                e.Handled = true;
            }
        }

        private object _lockRefresh = new object();
        private bool _isWaitingToRefresh = false;
        delegate void DelVoid();

        private void RefreshData()
        {
            if (_isWaitingToRefresh) return;
            if (this.InvokeRequired)
            {
                DelVoid d = new DelVoid(RefreshData);
                this.Invoke(d);
            }
            else
            {
                try
                {
                    _isWaitingToRefresh = true;
                    System.Threading.Thread.Sleep(1000);//Wait for 1 second
                    if (!this.IsDisposed)
                    {
                        lock (_lockRefresh)
                        {
                            _isWaitingToRefresh = false;
                            this.Cursor = Cursors.WaitCursor;
                            MakeSectionColors();
                            FillOnlineStatus();
                            this.Cursor = Cursors.Arrow;
                        }
                    }
                }
                catch (Exception ex)
                {
                    UT.LogMsg("frmDetailStatus:RefreshData:Err:" + ex.Message);
                }
                finally
                {
                    this.Cursor = Cursors.Arrow;
                    _isWaitingToRefresh = false;
                }
            }
        }

        delegate void DelgObjStr(object sender, string strAftUrno);
        private void DE_OnOST_ChangedForAFlight(object sender, string strAftUrno)
        {
            if (strAftUrno == myCurrURNO)
            {
                //if (this.InvokeRequired)
                //{
                //    DelgObjStr d = new DelgObjStr(DE_OnOST_ChangedForAFlight);
                //    this.Invoke(d, new object[] { sender, strAftUrno });
                //}
                //else
                {
                    System.Threading.Thread th = new System.Threading.Thread(new System.Threading.ThreadStart(RefreshData));
                    th.IsBackground = true;
                    th.Start();
                }
            }
        }


        delegate void DelgObjStrState(object sender, string strOstUrno, State state);
		private void DE_OnOST_Changed(object sender, string strOstUrno, State state)
		{
            if (this.InvokeRequired)
            {
                DelgObjStrState d = new DelgObjStrState(DE_OnOST_Changed);
                this.Invoke(d, new object[] {sender,strOstUrno,state  });
            }
            else
            {

                //MessageBox.Show("Ost Changed from detail status");
                switch (state)
                {
                    case State.Created:
                    case State.Modified:
                        try
                        {
                            myOST.Lock.AcquireReaderLock(System.Threading.Timeout.Infinite);
                            IRow[] ostRows = myOST.RowsByIndexValue("URNO", strOstUrno);
                            if (ostRows.Length > 0)
                            {
                                string dummy = ostRows[0]["UAFT"];
                                if (ostRows[0]["UAFT"] == myCurrURNO)
                                {
                                    MakeSectionColors();
                                    FillOnlineStatus();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            LogExceptionMsg(ex,"");
                        }
                        finally
                        {
                            if (myOST.Lock.IsReaderLockHeld) myOST.Lock.ReleaseReaderLock();
                        }
                        break;
                    case State.Deleted:
                        MakeSectionColors();
                        FillOnlineStatus();
                        break;
                }
            }
		}

		private void ctx_OpenTimePicker(object sender, System.EventArgs e)
		{
			SetTimePicker();		
		}

        //private void ctx_ClearTime(object sender, System.EventArgs e)
        //{
        //    string msg = "";
        //    DateTime t1 = DateTime.Now;
        //    DateTime t2;
        //    DateTime t3 = t1;
        //    TimeSpan ts;
        //    bool hasLock = false;

        //    int currSel = tabStatus.GetCurrentSelected();
        //    if(currSel > -1)
        //    {

        //        try
        //        {
        //            myOST.Lock.AcquireWriterLock(System.Threading.Timeout.Infinite);
        //            hasLock = true;
        //            t1 = DateTime.Now;
        //            UT.LogMsg(string.Format("--------------------------------- Clear Start {0:00}:{1:00}.{2:000}", t1.Minute, t1.Second, t1.Millisecond));
        //            t3 = t1;
        //            t2 = DateTime.Now;
        //            ts = t2.Subtract(t1);

        //            msg += "Lock time : " + ts.TotalMilliseconds + Environment.NewLine;
        //            t1 = t2;

        //            IRow[] ostRows = myOST.RowsByIndexValue("URNO", tabStatus.GetFieldValue(currSel, "URNO"));
        //            if (ostRows.Length > 0)
        //            {
        //                ostRows[0]["CONU"] = "";
        //                ostRows[0]["LSTU"] = UT.DateTimeToCeda(CFC.GetUTC());
        //                ostRows[0]["USEU"] = UT.UserName;
        //                ostRows[0].Status = State.Modified;
        //                //myOST.Save();//AM 20080312 - Commented out to update the row only
        //                myOST.Save(ostRows[0]);
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            LogExceptionMsg(ex);
        //        }
        //        finally
        //        {
        //            if (hasLock)
        //            {
        //                if (myOST.Lock.IsWriterLockHeld)
        //                {
        //                    myOST.Lock.ReleaseWriterLock();
        //                }
        //                t2 = DateTime.Now;
        //                ts = t2.Subtract(t1);
        //                msg += "After Unlock time : " + ts.TotalMilliseconds + Environment.NewLine;
        //                t2 = DateTime.Now;
        //                ts = t2.Subtract(t3);
        //                msg += "Total time : " + ts.TotalMilliseconds + Environment.NewLine;
        //                //MessageBox.Show(msg);

        //                UT.LogMsg(string.Format("--------------------------------- Clear End {0:00}:{1:00}.{2:000}", t1.Minute, t1.Second, t1.Millisecond) + Environment.NewLine + msg);
        //            }
        //            else
        //            {
        //                UT.LogMsg("Fail to get the lock");
        //            }
        //        }
        //    }
        //}

        private void ctx_ClearTime(object sender, System.EventArgs e)
        {
            int currSel = tabStatus.GetCurrentSelected();
            if (currSel > -1)
            {
                SaveOstCONU(currSel, "");
            }
        }

        private void SetCurrentTime(object sender, System.EventArgs e)
        {
            int currSel = tabStatus.GetCurrentSelected();
            if (currSel > -1)
            {
                string stConu = "";
                if (UT.IsTimeInUtc == true)
                    stConu = UT.DateTimeToCeda(CFC.GetUTC());
                else
                    stConu = UT.DateTimeToCeda(DateTime.Now);

                SaveOstCONU(currSel, stConu);
            }
        }


        //private void SetCurrentTime(object sender, System.EventArgs e)
        //{
        //    int currSel = tabStatus.GetCurrentSelected();
        //    if(currSel > -1)
        //    {
        //        try
        //        {
        //            myOST.Lock.AcquireWriterLock(System.Threading.Timeout.Infinite);
        //            IRow[] ostRows = myOST.RowsByIndexValue("URNO", tabStatus.GetFieldValue(currSel, "URNO"));
        //            if (ostRows.Length > 0)
        //            {
        //                if (UT.IsTimeInUtc == true)
        //                    ostRows[0]["CONU"] = UT.DateTimeToCeda(CFC.GetUTC());
        //                else
        //                    ostRows[0]["CONU"] = UT.DateTimeToCeda(DateTime.Now);
        //                ostRows[0]["LSTU"] = UT.DateTimeToCeda(CFC.GetUTC());
        //                ostRows[0]["USEU"] = UT.UserName;
        //                ostRows[0].Status = State.Modified;
        //                myOST.Save();
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            LogExceptionMsg(ex);
        //        }
        //        finally
        //        {
        //            myOST.Lock.ReleaseWriterLock();
        //        }
        //    }
        //}

		private void tabStatus_Leave(object sender, System.EventArgs e)
		{
			for (int i = 0; i < mnuContext.MenuItems.Count; i++)
			{
				mnuContext.MenuItems[i].Visible = false;
			}
		}

		private void tabStatus_Enter(object sender, System.EventArgs e)
		{
			string prv = CFC.LoginControl.GetPrivileges("EditDetailStatus");
			if(prv == "-" || prv == "0")
			{
				for (int i = 0; i < mnuContext.MenuItems.Count; i++)
				{
					mnuContext.MenuItems[i].Visible = false;
				}
				return;
			}
			for (int i = 0; i < mnuContext.MenuItems.Count; i++)
			{
				mnuContext.MenuItems[i].Visible = true;
			}
		}

		private void DE_OnUTC_LocalTimeChanged()
		{
            if (this.InvokeRequired)
            {
                RefreshCallback d = new RefreshCallback(DE_OnUTC_LocalTimeChanged);
                this.Invoke(d);
            }
            else
            {
                SetData(myCurrURNO, myCurrRKEY, "");
            }
		}

		private void btnPrint_Click(object sender, System.EventArgs e)
		{
			string strSection = "";
			if(tabStatusSections.GetCurrentSelected() > -1)
			{
				strSection = tabStatusSections.GetFieldValue(tabStatusSections.GetCurrentSelected(), "NAME");
			}
            rptDetailStatus rpt = new rptDetailStatus(tabArrival, tabDeparture, tabStatus, strSection);
            frmPreview pt = new frmPreview(rpt);
            pt.Show();
		}

		private void tabStatus_CloseInplaceEdit(object sender, AxTABLib._DTABEvents_CloseInplaceEditEvent e)
		{
            SaveOstREMA( e.lineNo, tabStatus.GetFieldValue(e.lineNo, "REMA"));
			
            //string strURNO = tabStatus.GetFieldValue(e.lineNo, "URNO");

            //try
            //{
            //    myOST.Lock.AcquireWriterLock(System.Threading.Timeout.Infinite);
            //    IRow[] ostRows = myOST.RowsByIndexValue("URNO", strURNO);
            //    if (ostRows.Length > 0)
            //    {
            //        string strRema = tabStatus.GetFieldValue(e.lineNo, "REMA");
            //        string strOldRema = ostRows[0]["REMA"];
            //        if (strRema != strOldRema)
            //        {
            //            ostRows[0]["REMA"] = strRema;
            //            ostRows[0]["LSTU"] = UT.DateTimeToCeda(CFC.GetUTC());
            //            ostRows[0]["USEU"] = UT.UserName;
            //            ostRows[0].Status = State.Modified;
            //            myOST.Save();
            //            //DE.Call_OST_Changed( this, strURNO, State.Modified);
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    LogExceptionMsg(ex);
            //}
            //finally
            //{
            //    myOST.Lock.ReleaseWriterLock();
            //}
		}

		private void DE_OnUpdateFlightData(object sender, string strAftUrno, State state)
		{
            if ((tabDeparture.GetFieldValue(0, "URNO") == strAftUrno) ||
                   (tabArrival.GetFieldValue(0, "URNO") == strAftUrno))
            {
                if (this.InvokeRequired)
                {
                    DelgObjStrState d = new DelgObjStrState(DE_OnUpdateFlightData);
                    this.Invoke(d, new object[] { sender, strAftUrno, state });
                    //this.Invoke(d);
                }
                else
                {
                    UT.LogMsg("frmDetailStat:DE_OnUpdateFlightData:Start");
                    if ((tabDeparture.GetFieldValue(0, "URNO") == strAftUrno) ||
                       (tabArrival.GetFieldValue(0, "URNO") == strAftUrno))
                    {
                        FillFligthInfo();
                    }
                    UT.LogMsg("frmDetailStat:DE_OnUpdateFlightData:Finish");
                }
            }
		}

		private void mnuContext_Popup(object sender, System.EventArgs e)
		{
			int curSel = tabStatus.GetCurrentSelected();
			if(curSel > -1)
			{
				string strCflType = tabStatus.GetFieldValue(curSel, "CFLTYPE_DB");
				if(strCflType == "A")
				{

					mnuContext.MenuItems[4].Enabled = true;
					mnuContext.MenuItems[4].Checked = true;
				}
				else if(strCflType == "N")
				{
					mnuContext.MenuItems[4].Enabled = true; 
					mnuContext.MenuItems[4].Checked = false;
				}
				else
				{
					mnuContext.MenuItems[4].Enabled = false;
				}
			}
			else
			{
				mnuContext.MenuItems[4].Enabled = false;
			}
		}

		private void tabStatus_SendLButtonClick(object sender, AxTABLib._DTABEvents_SendLButtonClickEvent e)
		{
		
		}

        private void mnuItemAck_Click(object sender, System.EventArgs e)
        {
            int curSel = tabStatus.GetCurrentSelected();
            if (curSel > -1)
            {
                string strCflType = tabStatus.GetFieldValue(curSel, "CFLTYPE_DB");
                if (strCflType == "A" || strCflType == "N")
                {
                    string strCOTY = "";
                    if (strCflType == "A")
                        strCOTY = "N";
                    else if (strCflType == "N")
                        strCOTY = "A";

                    if (strCOTY != "")
                    {
                        SaveOstCOTY(curSel, strCOTY);
                    }
                }

                //    try
                //    {
                //        myOST.Lock.AcquireWriterLock(System.Threading.Timeout.Infinite);
                //        string strCflType = tabStatus.GetFieldValue(curSel, "CFLTYPE_DB");
                //        if (strCflType == "A" || strCflType == "N")
                //        {
                //            string strURNO = tabStatus.GetFieldValue(curSel, "URNO");
                //            IRow[] ostRows = myOST.RowsByIndexValue("URNO", strURNO);
                //            if (ostRows.Length > 0)
                //            {
                //                if (strCflType == "A")
                //                    ostRows[0]["COTY"] = "N";
                //                if (strCflType == "N")
                //                    ostRows[0]["COTY"] = "A";

                //                ostRows[0]["LSTU"] = UT.DateTimeToCeda(CFC.GetUTC());
                //                ostRows[0]["USEU"] = UT.UserName;
                //                ostRows[0].Status = State.Modified;
                //                myOST.Save();
                //            }
                //        }
                //    }
                //    catch (Exception ex)
                //    {
                //        LogExceptionMsg(ex);
                //    }
                //    finally
                //    {
                //        myOST.Lock.ReleaseWriterLock();
                //}
            }
        }    

		private void frmDetailStatus_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			this.Hide();
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			SaveFileDialog dlg = new SaveFileDialog();
			dlg.Filter = "Text-files (*.txt)|*.txt|All filed (*.*)|*.*";
			if(dlg.ShowDialog() == DialogResult.OK)
			{
				string strArrivalValues = "";
				string strDepartureValues = "";
				tabPrint.ResetContent();
				tabPrint.HeaderString = tabArrival.HeaderString + "," + tabDeparture.HeaderString;
				tabPrint.InsertTextLine(tabPrint.HeaderString, false);
				strArrivalValues = tabArrival.GetLineValues(0);
				strDepartureValues =  tabDeparture.GetLineValues(0);
				if(strArrivalValues == "")
				{
					string strTmp = "";
					int cntComma = UT.ItemCount(tabArrival.HeaderString, ",") - 1;
					strArrivalValues = strTmp.PadLeft(cntComma, ',');
				}
				if(strDepartureValues == "")
				{
					string strTmp = "";
					int cntComma = UT.ItemCount(tabDeparture.HeaderString, ",") - 1;
					strDepartureValues = strTmp.PadLeft(cntComma, ',');
				}
				tabPrint.InsertTextLine(strArrivalValues + "," + strDepartureValues, false);
				tabPrint.InsertTextLine(tabStatus.HeaderString, false);
				string strBuffer = tabStatus.GetBuffer(0, tabStatus.GetLineCount()-1, "\n");
				tabPrint.InsertBuffer(strBuffer, "\n");
				tabPrint.WriteToFile(dlg.FileName, false);
			}
		}

		private void btnBatchListProcessing_Click(object sender, System.EventArgs e)
		{
			Ufis.Utils.IniFile ini = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			string strPath  = ini.IniReadValue("STATUSMANAGER", "CHECK_LIST_PROCESSING");
			//AFTURNO,USERNAME,PERMITS(0,1,-),TIME_IN(L=Local,U=UTC)

			string strCL_Save = CFC.LoginControl.GetPrivileges("CheckListProcessingSave");
			string strCL_Edit = CFC.LoginControl.GetPrivileges("CheckListProcessingEdit");
			string strCL_Print =  CFC.LoginControl.GetPrivileges("CheckListProcessingPrint");
			string strCL_TimeEdit =  CFC.LoginControl.GetPrivileges("CheckListProcessingTimeEdit");
			if(strCL_Save == "-" || strCL_Save == "0")
				strCL_Save = "0";
			else
				strCL_Save = "1";
			if(strCL_Edit == "-" || strCL_Edit == "0")
				strCL_Edit = "0";
			else
				strCL_Edit = "1";
			if(strCL_Print == "-" || strCL_Print == "0")
				strCL_Print = "0";
			else
				strCL_Print = "1";
			if(strCL_TimeEdit == "-" || strCL_TimeEdit == "0")
				strCL_TimeEdit = "0";
			else
				strCL_TimeEdit = "1";
			 
			string strPermits = strCL_Save + ";" + strCL_Edit + ";" + strCL_Print + ";" + strCL_TimeEdit;
			
			string strParameters = myCurrURNO + "," + UT.UserName + "," + strPermits + ",L";
			try
			{
				Process.Start(strPath, strParameters);		
			}
			catch(Exception)
			{
				string strText = "Path to Batch-List-Processing could not be found.\n ==> Ceda.ini";
				MessageBox.Show(this, strText, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}


        // This delegate enables asynchronous calls for method without argument.
        delegate void RefreshCallback();

        private static void LogExceptionMsg(Exception ex, string msgToDisplay)
        {
            string msg = "Error due to " + ex.ToString() + Environment.NewLine + ex.StackTrace ;
            UT.LogMsg( msg );
            if ((msgToDisplay!=null) && (msgToDisplay!=""))
                MessageBox.Show(msgToDisplay, "Error", MessageBoxButtons.OK);
        }

        private void SaveOstFld(int idxCurSel, string fldName, string fldVal)
        {
            string msg = "";
            DateTime t1 = DateTime.Now;
            DateTime t2;
            DateTime t3 = t1;
            TimeSpan ts;
            bool hasLock = false;
            Cursor curCursor = this.Cursor;

            try
            {
                if (idxCurSel > -1)
                {
                    this.Cursor = Cursors.WaitCursor;
                    t1 = DateTime.Now;
                    myOST.Lock.AcquireWriterLock(System.Threading.Timeout.Infinite);
                    hasLock = true;
                    UT.LogMsg(string.Format("--------------------------------- Start {0:00}:{1:00}.{2:000} Fld{3} Val{4}", t1.Minute, t1.Second, t1.Millisecond, fldName, fldVal)+"," + myOST.Lock.WriterSeqNum );
                    t3 = t1;
                    t2 = DateTime.Now;
                    ts = t2.Subtract(t1);

                    msg += "Lock time : " + ts.TotalMilliseconds + Environment.NewLine;
                    t1 = t2;
                    IRow[] rows = myOST.RowsByIndexValue("URNO", tabStatus.GetFieldValue(idxCurSel, "URNO"));
                    if (rows.Length > 0)
                    {
                        rows[0].ClearChgs();//AM 20080314
                        rows[0].StartTrackChanges();//AM 20080314
                        t2 = DateTime.Now;
                        ts = t2.Subtract(t1);
                        msg += "Get Row time : " + ts.TotalMilliseconds + Environment.NewLine;
                        t1 = t2;

                        UT.LogMsg("Update [" + fldName + "], [" + fldVal + "] to flight " + rows[0]["URNO"]);
                        //strCONU = strCONU.Substring(0, 8);
                        rows[0][fldName] = fldVal;
                        rows[0]["LSTU"] = UT.DateTimeToCeda(CFC.GetUTC());
                        rows[0]["USEU"] = UT.UserName;
                        rows[0].Status = State.Modified;
                        rows[0].StopTrackChanges();//AM 20080314
                        t2 = DateTime.Now;
                        ts = t2.Subtract(t1);
                        msg += "After Status time : " + ts.TotalMilliseconds + Environment.NewLine;
                        t1 = t2;

                        tabStatus.SetFieldValues(idxCurSel, fldName, fldVal);
                        UT.LogMsg("After status change");
                        tabStatus.Refresh();
                        UT.LogMsg("Before Save");

                        t2 = DateTime.Now;
                        ts = t2.Subtract(t1);
                        msg += "After refresh time : " + ts.TotalMilliseconds + Environment.NewLine;
                        t1 = t2;

                        //myOST.Save();                            
                        myOST.Save(rows[0]);

                        t2 = DateTime.Now;
                        ts = t2.Subtract(t1);
                        msg += "After Save time : " + ts.TotalMilliseconds + Environment.NewLine;
                        t1 = t2;
                        UT.LogMsg("After Save " + rows[0]["URNO"]);

                        //Do not call because we wait for the broadcast
                        //DE.Call_OST_Changed(this, strURNO, State.Modified);

                    }
                }
            }
            catch (Exception ex)
            {
                LogExceptionMsg(ex, ex.Message);
            }
            finally
            {
                try
                {
                    if (hasLock)
                    {
                        if (myOST.Lock.IsWriterLockHeld)
                        {
                            UT.LogMsg("SaveOstFld:Before Release Writer Lock.");
                            myOST.Lock.ReleaseWriterLock();
                            UT.LogMsg("SaveOstFld:after Release Writer Lock.");
                        }
                        t2 = DateTime.Now;
                        ts = t2.Subtract(t1);
                        msg += "After Unlock time : " + ts.TotalMilliseconds + Environment.NewLine;
                        t2 = DateTime.Now;
                        ts = t2.Subtract(t3);
                        msg += "Total time : " + ts.TotalMilliseconds + Environment.NewLine;
                        //MessageBox.Show(msg);

                        UT.LogMsg(string.Format("--------------------------------- End {0:00}:{1:00}.{2:000}", t1.Minute, t1.Second, t1.Millisecond) + Environment.NewLine + msg);
                    }
                    else
                    {
                        UT.LogMsg("No lock");
                    }
                }
                catch (Exception ex)
                {
                    UT.LogMsg("Unlock Exception. " + ex.Message);
                }
                this.Cursor = curCursor;
            }

        }

        private void SaveOstCOTY(int idxCurSel, string coty)
        {
            SaveOstFld( idxCurSel, "COTY", coty );
        }

        private void SaveOstREMA(int idxCurSel, string rema)
        {
            SaveOstFld( idxCurSel, "REMA", rema );
        }

        private void SaveOstCONU(int idxCurSel, string stCedaCONU)
        {
            SaveOstFld( idxCurSel, "CONU", stCedaCONU );
            //string msg = "";
            //DateTime t1 = DateTime.Now;
            //DateTime t2;
            //DateTime t3 = t1;
            //TimeSpan ts;
            //bool hasLock = false;

            //try
            //{
            //    if (idxCurSel > -1)
            //    {
            //        t1 = DateTime.Now;
            //        myOST.Lock.AcquireWriterLock(System.Threading.Timeout.Infinite);
            //        hasLock = true;
            //        UT.LogMsg(string.Format("--------------------------------- Start {0:00}:{1:00}.{2:000}", t1.Minute, t1.Second, t1.Millisecond));
            //        t3 = t1;
            //        t2 = DateTime.Now;
            //        ts = t2.Subtract(t1);

            //        msg += "Lock time : " + ts.TotalMilliseconds + Environment.NewLine;
            //        t1 = t2;
            //        IRow[] rows = myOST.RowsByIndexValue("URNO", tabStatus.GetFieldValue(idxCurSel, "URNO"));
            //        if (rows.Length > 0)
            //        {
            //            t2 = DateTime.Now;
            //            ts = t2.Subtract(t1);
            //            msg += "Get Row time : " + ts.TotalMilliseconds + Environment.NewLine;
            //            t1 = t2;

            //            UT.LogMsg("Update user time [" + stCedaCONU + "] to flight " + rows[0]["URNO"]);
            //            //strCONU = strCONU.Substring(0, 8);
            //            rows[0]["CONU"] = stCedaCONU;
            //            rows[0]["LSTU"] = UT.DateTimeToCeda(CFC.GetUTC());
            //            rows[0]["USEU"] = UT.UserName;
            //            rows[0].Status = State.Modified;

            //            t2 = DateTime.Now;
            //            ts = t2.Subtract(t1);
            //            msg += "After Status time : " + ts.TotalMilliseconds + Environment.NewLine;
            //            t1 = t2;

            //            tabStatus.SetFieldValues(idxCurSel, "CONU", stCedaCONU);
            //            UT.LogMsg("After status change");
            //            tabStatus.Refresh();
            //            UT.LogMsg("Before Save");

            //            t2 = DateTime.Now;
            //            ts = t2.Subtract(t1);
            //            msg += "After refresh time : " + ts.TotalMilliseconds + Environment.NewLine;
            //            t1 = t2;

            //            //myOST.Save();                            
            //            myOST.Save(rows[0]);

            //            t2 = DateTime.Now;
            //            ts = t2.Subtract(t1);
            //            msg += "After Save time : " + ts.TotalMilliseconds + Environment.NewLine;
            //            t1 = t2;
            //            UT.LogMsg("After Save " + rows[0]["URNO"]);

            //            //Do not call because we wait for the broadcast
            //            //DE.Call_OST_Changed(this, strURNO, State.Modified);

            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    LogExceptionMsg(ex);
            //}
            //finally
            //{
            //    if (hasLock)
            //    {
            //        if (myOST.Lock.IsWriterLockHeld)
            //        {
            //            myOST.Lock.ReleaseWriterLock();
            //        }
            //        t2 = DateTime.Now;
            //        ts = t2.Subtract(t1);
            //        msg += "After Unlock time : " + ts.TotalMilliseconds + Environment.NewLine;
            //        t2 = DateTime.Now;
            //        ts = t2.Subtract(t3);
            //        msg += "Total time : " + ts.TotalMilliseconds + Environment.NewLine;
            //        //MessageBox.Show(msg);

            //        UT.LogMsg(string.Format("--------------------------------- Clear End {0:00}:{1:00}.{2:000}", t1.Minute, t1.Second, t1.Millisecond) + Environment.NewLine + msg);
            //    }
            //    else
            //    {
            //        UT.LogMsg("Fail to get the lock");
            //    }
            //}

        }

        private void frmDetailStatus_SizeChanged(object sender, EventArgs e)
        {
            ChangeTabSize();
        }

        private void ChangeTabSize()
        {
            int wh = this.Height;
            int ww = this.Width;

            int ty = this.tabStatus.Top;
            int tx = this.tabStatus.Left;
            int newW = ww - tx - 22;
            if (newW < 600)
            {
                this.tabStatus.Width = 600;
                //this.Width = this.tabStatus.Width + tx + 25;
                //return;
            }
            else
            {
                this.tabStatus.Width = newW;
            }
            int newH = wh - ty - 116;
            if (newH < 130)
            {
                this.tabStatus.Height = 130;
                //this.Height = this.tabStatus.Height + ty + 120;
                //return;
            }
            else
            {
                this.tabStatus.Height = newH;
            }
            //if (this.tabStatus.Height < 120)
            //{
            //    this.tabStatus.Height = 120;
            //    this.Height = this.tabStatus.Height + ty + 150;
            //}
            this.tabStatusSections.Height = this.tabStatus.Height;
            this.label1.Top = this.tabStatus.Height + ty + 1;
            try
            {
                //_tabFieldArray.ChangeLength("STATUSNAME", 300);
                //string logicalFieldList = "";
                //string hdrLengthString = "";
                //_tabFieldArray.GetTabString(out logicalFieldList, out hdrLengthString);
                //tabStatus.LogicalFieldList = logicalFieldList;
                //tabStatus.HeaderLengthString = hdrLengthString;
            }
            catch (Exception)
            {
            }
        }

        //TabFieldArray _tabFieldArray = null;

	}



    public class TabFieldArray
    {
        private ArrayList _arr = new ArrayList();
        public TabFieldArray() { }
        public TabFieldArray(string logicalFieldList, string headerLengthString)
        {
            char[] del = {','};
            string[] arrFields = logicalFieldList.Split(del);
            string[] hdrLengths = headerLengthString.Split(del);
            int cnt = arrFields.Length;
            int hdrLen = 0;
            for (int i = 0; i < cnt; i++)
            {
                hdrLen = 0;
                try
                {
                    hdrLen = Int32.Parse(hdrLengths[i]);
                }
                catch (Exception)
                {
                }
                Add(arrFields[i], hdrLen);
            }
        }

        public void Add(string logicalFieldName, int headerLength)
        {
            _arr.Add( new TabField(logicalFieldName, headerLength ));
        }

        public void ChangeLength(string logicalFieldName, int headerLength)
        {
            int cnt = _arr.Count;
            for (int i = 0; i < cnt; i++)
            {
                TabField fld = (TabField)_arr[i];
                if (fld.LogicalFieldName == logicalFieldName) fld.HeaderLength = headerLength;
            }
        }

        public void GetTabString(out string logicalFieldList, out string headerLengthString)
        {
            logicalFieldList = "";
            headerLengthString = "";
            int cnt = _arr.Count;
            if (cnt > 0)
            {
                TabField fld = (TabField)_arr[0];
                logicalFieldList += fld.LogicalFieldName;
                headerLengthString += fld.HeaderLength.ToString();
                for (int i = 1; i < cnt; i++)
                {
                    fld = (TabField)_arr[i];
                    logicalFieldList += "," + fld.LogicalFieldName;
                    headerLengthString += "," + fld.HeaderLength.ToString();
                }
            }
        }

        class TabField
        {
            private string _logFieldName = "";
            private int _headerLength = 0;

            public string LogicalFieldName
            {
                get { return _logFieldName; }
                set { _logFieldName = value; }
            }

            public int HeaderLength
            {
                get { return _headerLength; }
                set { _headerLength = value; }
            }

            public TabField(string logicalFieldName, int headerLength)
            {
                _logFieldName = logicalFieldName;
                _headerLength = headerLength;
            }
        }
    }
}
