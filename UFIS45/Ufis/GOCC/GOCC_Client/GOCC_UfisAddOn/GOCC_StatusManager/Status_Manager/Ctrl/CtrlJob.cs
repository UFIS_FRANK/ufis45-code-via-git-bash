using System;
using System.Collections.Generic;
using System.Text;

using System.Collections;
using System.Threading;

using Ufis.Data;
using Ufis.Utils;

using Status_Manager.DB;
using Status_Manager.DS;
using Status_Manager.UI;

namespace Status_Manager.Ctrl
{
    public class CtrlJob
    {
        private static CtrlJob _this = null;
        private static object objLockJobProcess = new object();
        private static string _utpl = "";

        private static bool _showJob = false;
        private static bool _allowShowJob = true;

        private CtrlJob()
        {
        }

        public bool ShowJob
        {
            get
            {
                if (!AllowShowJob) return false;
                else return _showJob;
            }
            set
            {
                if (!AllowShowJob) _showJob = false;
                else
                {
                    _showJob = value;
                }
            }
        }

        public bool AllowShowJob
        {
            get { return _allowShowJob; }
            set
            {
                _allowShowJob = value;
            }
        }

        public static CtrlJob GetInstance()
        {
            if (_this == null)
            {
                _this = new CtrlJob();
                _allowShowJob = CtrlIni.GetInstance().IsAllowToShowJob;
            }
            return _this;
        }

        /// <summary>
        /// Load Job Related Data
        /// </summary>
        /// <param name="result"></param>
        public void LoadJobRelatedData(ref string result)
        {
            result = "";
           
            if (AllowShowJob)
            {
                lock (objLockJobProcess)
                {
                    ArrayList uaftArrSt = CtrlFlight.GetUAftArrStForSelection();
                    DbJob dbJob = DbJob.GetInstance();
                    DbStf dbStf = DbStf.GetInstance();
                    DbTpl dbTpl = DbTpl.GetInstance();
                    DbEqu dbEqu = DbEqu.GetInstance();
                    DbDrr dbDrr = DbDrr.GetInstance();

                    result = dbJob.LoadJobData(uaftArrSt, UT.DateTimeToCeda(UT.TimeFrameFrom), UT.DateTimeToCeda(UT.TimeFrameTo));
                    dbJob.AllowToSaveToDatabase = false;

                    dbStf.LoadStfData(new ArrayList());
                    dbTpl.LoadTplData();
                    dbEqu.LoadEquData(new ArrayList());
                    dbDrr.LoadDrrData(new ArrayList());                    
                }
            }
        }

        //public void ThLoadJobRelatedData()
        //{
        //    Thread th = new Thread(new ParameterizedThreadStart(LoadJobRelatedData));
        //    th.IsBackground = true;
        //    object result= new object();
        //    th.Start(result);
        //}

        public void GetJobInfo(string arrUaft, string depUaft,
            out string arrJobInfo, out string depJobInfo)
        {
            arrJobInfo = "";
            depJobInfo = "";
            if (ShowJob)
            {
                if (_utpl != "")
                {
                    DbJob dbJob = DbJob.GetInstance();
                    if (dbJob.IsLoadingData)
                    {
                        arrJobInfo = "Loading Data...";
                        depJobInfo = "Loading Data...";
                    }
                    else
                    {
                        DSJob dsJob = new DSJob();
                        string[] ustfStArr;
                        string[] uequStArr;
                        string[] udrrStArr;
                        dbJob.GetJobInfoForFlight(arrUaft, depUaft, dsJob,
                            out ustfStArr, out uequStArr, out udrrStArr);
                        DbStf.GetInstance().GetStfInfo(dsJob, ustfStArr);
                        DbEqu.GetInstance().GetEquInfo(dsJob, uequStArr);
                        DbDrr.GetInstance().GetDrrInfo(dsJob, udrrStArr);
                        string stArrSel = string.Format("UAFT='{0}' AND UTPL='{1}'", arrUaft, _utpl);
                        string stDepSel = string.Format("UAFT='{0}' AND UTPL='{1}'", depUaft, _utpl);
                        //bool isFirstEqu = true;
                        foreach (DSJob.JOBRow row in dsJob.JOB.Select(stArrSel, "UJTY,ST"))
                        {
                            if (row.UJTY == "2000") //Staff
                            {
                                arrJobInfo += row.ST + Environment.NewLine;
                            }
                            else
                            {
                                arrJobInfo += row.ST;
                            }
                        }

                        //isFirstEqu = true;
                        foreach (DSJob.JOBRow row in dsJob.JOB.Select(stDepSel, "UJTY,ST"))
                        {
                            if (row.UJTY == "2000") //Staff
                            {
                                depJobInfo += row.ST + Environment.NewLine;
                            }
                            else
                            {
                                depJobInfo += row.ST;
                            }
                        }
                    }
                }
            }
        }

        private DSKeyVal _dsTpl = null;

        public DSKeyVal RetrieveJobTemplate()
        {
            if (_dsTpl == null)
            {
                _dsTpl = new DSKeyVal();
                DbTpl.GetInstance().PopulateTplInfo(_dsTpl);
            }
            return _dsTpl;
        }

        private FrmSelJobTemplate _frmJobTplSel = null;

        private FrmSelJobTemplate FrmJobTplSel
        {
            get
            {
                if (_frmJobTplSel == null)
                {
                    _frmJobTplSel = new FrmSelJobTemplate();
                    _frmJobTplSel.OnJobTemplateSelected += new FrmSelJobTemplate.JobTemplateSelected(frm_OnJobTemplateSelected);
                    _frmJobTplSel.OnJobTemplateSelectionCancelled += new FrmSelJobTemplate.JobTemplateSelectionCancelled(frm_OnJobTemplateSelectionCancelled);
                }
                return _frmJobTplSel;
            }
        }

        public void SelectJobTemplate(System.Windows.Forms.Form parent, int xPosition, int yPosition)
        {
            if (_utpl == "")
            {
                FrmJobTplSel.VisibleCancelButton = false;
                FrmJobTplSel.MustSelectJobTemplate = true;
            }
            else
            {
                FrmJobTplSel.VisibleCancelButton = true;
                FrmJobTplSel.MustSelectJobTemplate = false;
            }

            FrmJobTplSel.Left = xPosition - 20;
            FrmJobTplSel.Top = yPosition;
            FrmJobTplSel.SelectedJobTemplateId = _utpl;
            FrmJobTplSel.ShowDialog(parent);
        }

        void frm_OnJobTemplateSelectionCancelled()
        {
            FrmJobTplSel.Hide();
        }

        void frm_OnJobTemplateSelected(string selectedId)
        {
            FrmJobTplSel.Hide();
            if (_utpl != selectedId)
            {
                _utpl = selectedId;
                DE.Call_JobTemplateChanged(_utpl);//Notify about job template changed
            }            
        }

        public void JobChanged(object sender, DatabaseTableEventArgs args)
        {
            LogMsg("JobChanged.");
            DbJob dbJob = DbJob.GetInstance();
            State status;
            string uJob, orgUaft, newUaft;
            if (!dbJob.GetJobChangedInfo(sender, args, out status, out uJob, out  orgUaft, out newUaft))
            {
                LogMsg("JobChanged:UAFT is not in arg.");
                dbJob.GetUaft(uJob, out orgUaft);
                newUaft = orgUaft;
            }
            if (orgUaft == null) orgUaft = "";
            if (newUaft == null) newUaft = "";
            LogMsg(string.Format("JobChanged:.UJOB{0},OLDUAFT{1},NEWUAFT{2}", uJob, orgUaft, newUaft));
            if ((orgUaft != "") || (newUaft != ""))
            {
                LogMsg("JobChanged:Before Call_JobChanged");
                DE.Call_JobChanged(sender, uJob, orgUaft, newUaft);
            }
        }

        private static void LogMsg(string msg)
        {
            UT.LogMsg("CtrlJob:" + msg);
        }
    }
}
