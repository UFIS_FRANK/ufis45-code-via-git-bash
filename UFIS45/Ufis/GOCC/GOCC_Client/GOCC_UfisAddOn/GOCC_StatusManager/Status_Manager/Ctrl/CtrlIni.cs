using System;
using System.Collections.Generic;
using System.Text;

using Ufis.Utils;

namespace Status_Manager.Ctrl
{
    public class CtrlIni
    {
        private static CtrlIni _this = null;
        private static IniFile _iniFile = null;

        private CtrlIni()
        {
        }

        public static CtrlIni GetInstance()
        {
            if (_this == null) _this = new CtrlIni();
            return _this;
        }

        public IniFile CurIniFileObj
        {
            get
            {
                if (_iniFile == null) _iniFile = new IniFile("C:\\Ufis\\system\\ceda.ini");
                return _iniFile;
            }
        }

        public string GetLogFilePath()
        {
            return CurIniFileObj.IniReadValue("STATUSMANAGER", "LOG_FILE_PATH").Trim();
        }

        public bool IsAllowToShowJob
        {
            get
            {
                bool allow = false;
                string st = CurIniFileObj.IniReadValue("STATUSMANAGER", "SHOW_JOB").Trim();
                if (st.ToUpper() == "Y")
                {
                    allow = true;
                }
                return allow;
            }
        }
    }
}
