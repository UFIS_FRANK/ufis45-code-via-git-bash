using System;
using System.Collections.Generic;
using System.Text;

using System.Collections;

using Ufis.Data;
using Ufis.Utils;

using Status_Manager.Ent;

namespace Status_Manager.Ctrl
{
    public class CtrlFlight
    {
        private static EntFlightUrnoInfo entFlightUrnoInfo = null;

        public static ArrayList GetUAftArrStForSelection()
        {
            if (entFlightUrnoInfo==null)  entFlightUrnoInfo = new EntFlightUrnoInfo();
            return entFlightUrnoInfo.UAftArrStForSelection;
        }

        public static void InitUaftArrStForSelection()
        {
            if (entFlightUrnoInfo == null) entFlightUrnoInfo = new EntFlightUrnoInfo();
            entFlightUrnoInfo.Init();
        }

        public static string GetUaftSelectionClause()
        {
            return string.Format("SELECT A2.URNO FROM AFTTAB A2 " +
                " WHERE RKEY IN( SELECT A.RKEY FROM AFTTAB A " +
                " WHERE ((A.TIFA BETWEEN '{0}' AND '{1}')" + 
                "' OR (A.TIFD BETWEEN '{0}' AND '{1}')) AND" + 
                " A.FTYP IN ('S', 'O', 'X'))", 
                UT.DateTimeToCeda(UT.TimeFrameFrom), 
                UT.DateTimeToCeda(UT.TimeFrameTo) );
        }

        //public static void LoadAftData()
        //{
        //}
    }
}
