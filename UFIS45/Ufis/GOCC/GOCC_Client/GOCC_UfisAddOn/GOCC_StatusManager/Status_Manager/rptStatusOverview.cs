using System;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using Ufis.Utils;
using System.Drawing;


namespace Status_Manager
{
	public class rptStatusOverview : ActiveReport
	{
		private AxTABLib.AxTAB tabStatus = null; 
		private string strViewName = "";
		private int currLine = 0;

		public rptStatusOverview(AxTABLib.AxTAB tStatus, string sViewName)
		{
			strViewName = sViewName;
			tabStatus = tStatus;
			InitializeReport();
		}

		private void rptStatusOverview_FetchData(object sender, DataDynamics.ActiveReports.ActiveReport.FetchEventArgs eArgs)
		{
			DateTime dt;
			sep1.Y1 = 0; 
			sep2.Y1 =  0;
			sep3.Y1 =  0;
			sep4.Y1 =  0;
			sep5.Y1 =  0;
			sep6.Y1 =  0;
			sep7.Y1 =  0;
			sep8.Y1 =  0;
			sep9.Y1 =  0;
			sep10.Y1 =  0;
			sep11.Y1 =  0;
			sep12.Y1 =  0;

			//MIN,TIME,STATUS,A/D,FLNO,STA/STD,ETA/ETD,ONB/OFB,POS,GATE
			txtMin.Text = " " + tabStatus.GetFieldValue(currLine, "MIN");
			if(tabStatus.GetFieldValue(currLine, "TIME") != "")
			{
				dt = UT.CedaFullDateToDateTime(tabStatus.GetFieldValue(currLine, "TIME"));
				txtTime.Text = " " + dt.ToShortTimeString() + "/" + dt.Day.ToString();
			}
			txtRule.Text = " " + tabStatus.GetFieldValue(currLine, "RULE");
			txtStatus.Text = " " + tabStatus.GetFieldValue(currLine, "STATUS");
			txtAD.Text = " " + tabStatus.GetFieldValue(currLine, "A/D");
			txtFlno.Text = " " + tabStatus.GetFieldValue(currLine, "FLNO");
			txtSta.Text = "";
			if(tabStatus.GetFieldValue(currLine, "STA/STD") != "")
			{
				dt = UT.CedaFullDateToDateTime(tabStatus.GetFieldValue(currLine, "STA/STD"));
				txtSta.Text = " " + dt.ToShortTimeString() + "/" + dt.Day.ToString();
			}
			txtEta.Text = "";
			if(tabStatus.GetFieldValue(currLine, "ETA/ETD") != "")
			{
				dt = UT.CedaFullDateToDateTime(tabStatus.GetFieldValue(currLine, "ETA/ETD"));
				txtEta.Text = " " + dt.ToShortTimeString() + "/" + dt.Day.ToString();
			}
			txtOnb.Text = "";
			if(tabStatus.GetFieldValue(currLine, "ONB/OFB") != "")
			{
				dt = UT.CedaFullDateToDateTime(tabStatus.GetFieldValue(currLine, "ONB/OFB"));
				txtOnb.Text = " " + dt.ToShortTimeString() + "/" + dt.Day.ToString();
			}
			txtPos.Text = " " + tabStatus.GetFieldValue(currLine, "POS");
			txtGat.Text = " " + tabStatus.GetFieldValue(currLine, "GATE");

			if(currLine == tabStatus.GetLineCount())
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
				currLine++;
			}
		}

		private void rptStatusOverview_ReportStart(object sender, System.EventArgs eArgs)
		{
			DateTime d		= DateTime.Now;
			txtDate.Text	= d.ToString();
			txtViewName.Text = "View Name: " + strViewName;
			//*** Read the logo if exists and set it
			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			string strPrintLogoPath = myIni.IniReadValue("STATUSMANAGER", "PRINTLOGO");
			System.Drawing.Image myImage = null;
			try
			{
				if(strPrintLogoPath != "")
				{
					myImage = new Bitmap(strPrintLogoPath);
				}
			}
			catch(Exception)
			{
			}
			if(myImage != null)
				Picture1.Image = myImage;
			//*** END Read the logo if exists and set it
		}

		private void Detail_BeforePrint(object sender, System.EventArgs eArgs)
		{
			float ilHeight = Math.Max(txtRule.Height , txtStatus.Height); 
			sep1.Y2 = ilHeight;
			sep2.Y2 = ilHeight;
			sep3.Y2 = ilHeight;
			sep4.Y2 = ilHeight;
			sep5.Y2 = ilHeight;
			sep6.Y2 = ilHeight;
			sep7.Y2 = ilHeight;
			sep8.Y2 = ilHeight;
			sep9.Y2 = ilHeight;
			sep10.Y2 = ilHeight;
			sep11.Y2 = ilHeight;
			sep12.Y2 = ilHeight;
			sepHorz.Y1 = ilHeight;
			sepHorz.Y2 = ilHeight;
		}

		#region ActiveReports Designer generated code
		private DataDynamics.ActiveReports.PageHeader PageHeader;
		private DataDynamics.ActiveReports.Label Label35;
		private DataDynamics.ActiveReports.Picture Picture1;
		private DataDynamics.ActiveReports.Label Label1;
		private DataDynamics.ActiveReports.Label Label3;
		private DataDynamics.ActiveReports.Label Label4;
		private DataDynamics.ActiveReports.Label Label5;
		private DataDynamics.ActiveReports.Label Label6;
		private DataDynamics.ActiveReports.Label Label7;
		private DataDynamics.ActiveReports.Label Label8;
		private DataDynamics.ActiveReports.Label Label9;
		private DataDynamics.ActiveReports.Label Label10;
		private DataDynamics.ActiveReports.Label Label11;
		private DataDynamics.ActiveReports.Label Label12;
		private DataDynamics.ActiveReports.Line Line1;
		private DataDynamics.ActiveReports.Line Line2;
		private DataDynamics.ActiveReports.Line Line3;
		private DataDynamics.ActiveReports.Line Line4;
		private DataDynamics.ActiveReports.Line Line5;
		private DataDynamics.ActiveReports.Line Line6;
		private DataDynamics.ActiveReports.Line Line7;
		private DataDynamics.ActiveReports.Line Line8;
		private DataDynamics.ActiveReports.Line Line9;
		private DataDynamics.ActiveReports.Line Line10;
		private DataDynamics.ActiveReports.Line Line11;
		private DataDynamics.ActiveReports.Line Line12;
		private DataDynamics.ActiveReports.Line Line13;
		private DataDynamics.ActiveReports.Label txtViewName;
		private DataDynamics.ActiveReports.Line Line14;
		private DataDynamics.ActiveReports.Detail Detail;
		private DataDynamics.ActiveReports.TextBox txtStatus;
		private DataDynamics.ActiveReports.TextBox txtAD;
		private DataDynamics.ActiveReports.TextBox txtFlno;
		private DataDynamics.ActiveReports.Line sep9;
		private DataDynamics.ActiveReports.TextBox txtEta;
		private DataDynamics.ActiveReports.TextBox txtOnb;
		private DataDynamics.ActiveReports.TextBox txtPos;
		private DataDynamics.ActiveReports.TextBox txtGat;
		private DataDynamics.ActiveReports.TextBox txtRule;
		private DataDynamics.ActiveReports.TextBox txtMin;
		private DataDynamics.ActiveReports.TextBox txtTime;
		private DataDynamics.ActiveReports.TextBox txtSta;
		private DataDynamics.ActiveReports.Line sep1;
		private DataDynamics.ActiveReports.Line sep2;
		private DataDynamics.ActiveReports.Line sep3;
		private DataDynamics.ActiveReports.Line sep4;
		private DataDynamics.ActiveReports.Line sep5;
		private DataDynamics.ActiveReports.Line sep6;
		private DataDynamics.ActiveReports.Line sep7;
		private DataDynamics.ActiveReports.Line sep8;
		private DataDynamics.ActiveReports.Line sep10;
		private DataDynamics.ActiveReports.Line sep11;
		private DataDynamics.ActiveReports.Line sepHorz;
		private DataDynamics.ActiveReports.Line sep12;
		private DataDynamics.ActiveReports.PageFooter PageFooter;
		private DataDynamics.ActiveReports.TextBox txtDate;
		private DataDynamics.ActiveReports.Label Label34;
		private DataDynamics.ActiveReports.Label Label33;
		private DataDynamics.ActiveReports.TextBox TextBox2;
		private DataDynamics.ActiveReports.TextBox TextBox3;
		private DataDynamics.ActiveReports.Label Label32;
		public void InitializeReport()
		{
			this.LoadLayout(this.GetType(), "Status_Manager.rptStatusOverview.rpx");
			this.PageHeader = ((DataDynamics.ActiveReports.PageHeader)(this.Sections["PageHeader"]));
			this.Detail = ((DataDynamics.ActiveReports.Detail)(this.Sections["Detail"]));
			this.PageFooter = ((DataDynamics.ActiveReports.PageFooter)(this.Sections["PageFooter"]));
			this.Label35 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[0]));
			this.Picture1 = ((DataDynamics.ActiveReports.Picture)(this.PageHeader.Controls[1]));
			this.Label1 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[2]));
			this.Label3 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[3]));
			this.Label4 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[4]));
			this.Label5 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[5]));
			this.Label6 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[6]));
			this.Label7 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[7]));
			this.Label8 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[8]));
			this.Label9 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[9]));
			this.Label10 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[10]));
			this.Label11 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[11]));
			this.Label12 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[12]));
			this.Line1 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[13]));
			this.Line2 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[14]));
			this.Line3 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[15]));
			this.Line4 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[16]));
			this.Line5 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[17]));
			this.Line6 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[18]));
			this.Line7 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[19]));
			this.Line8 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[20]));
			this.Line9 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[21]));
			this.Line10 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[22]));
			this.Line11 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[23]));
			this.Line12 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[24]));
			this.Line13 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[25]));
			this.txtViewName = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[26]));
			this.Line14 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[27]));
			this.txtStatus = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[0]));
			this.txtAD = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[1]));
			this.txtFlno = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[2]));
			this.sep9 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[3]));
			this.txtEta = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[4]));
			this.txtOnb = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[5]));
			this.txtPos = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[6]));
			this.txtGat = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[7]));
			this.txtRule = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[8]));
			this.txtMin = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[9]));
			this.txtTime = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[10]));
			this.txtSta = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[11]));
			this.sep1 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[12]));
			this.sep2 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[13]));
			this.sep3 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[14]));
			this.sep4 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[15]));
			this.sep5 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[16]));
			this.sep6 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[17]));
			this.sep7 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[18]));
			this.sep8 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[19]));
			this.sep10 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[20]));
			this.sep11 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[21]));
			this.sepHorz = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[22]));
			this.sep12 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[23]));
			this.txtDate = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[0]));
			this.Label34 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[1]));
			this.Label33 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[2]));
			this.TextBox2 = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[3]));
			this.TextBox3 = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[4]));
			this.Label32 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[5]));
			// Attach Report Events
			this.FetchData += new DataDynamics.ActiveReports.ActiveReport.FetchEventHandler(this.rptStatusOverview_FetchData);
			this.ReportStart += new System.EventHandler(this.rptStatusOverview_ReportStart);
			this.Detail.BeforePrint += new System.EventHandler(this.Detail_BeforePrint);
		}

		#endregion
	}
}
