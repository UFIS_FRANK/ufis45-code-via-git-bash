using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Microsoft.Win32;
using Ufis.Utils;
using Ufis.Data;

namespace Status_Manager
{
	/// <summary>
	/// Summary description for frmFind.
	/// </summary>
	public class frmFind : System.Windows.Forms.Form
	{
		#region --- MyMembers
		IDatabase myDB = null;
		ITable    myAFT = null;
		#endregion --- MyMembers
		private System.Windows.Forms.PictureBox picBody;
		private System.Windows.Forms.Label lblAirline;
		private System.Windows.Forms.TextBox txtFLNO;
		private System.Windows.Forms.Label lblFLNO;
		private System.Windows.Forms.TextBox txtOrigDest;
		private System.Windows.Forms.Label lblOrigDest;
		private System.Windows.Forms.TextBox txtNature;
		private System.Windows.Forms.Label lblNature;
		private System.Windows.Forms.TextBox txtKeyCode;
		private System.Windows.Forms.Label lblKeyCode;
		private System.Windows.Forms.Button btnDoFilter;
		private AxTABLib.AxTAB tabFlights;
		private System.Windows.Forms.TextBox txtAirline;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmFind()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmFind));
			this.picBody = new System.Windows.Forms.PictureBox();
			this.lblAirline = new System.Windows.Forms.Label();
			this.txtAirline = new System.Windows.Forms.TextBox();
			this.txtFLNO = new System.Windows.Forms.TextBox();
			this.lblFLNO = new System.Windows.Forms.Label();
			this.txtOrigDest = new System.Windows.Forms.TextBox();
			this.lblOrigDest = new System.Windows.Forms.Label();
			this.txtNature = new System.Windows.Forms.TextBox();
			this.lblNature = new System.Windows.Forms.Label();
			this.txtKeyCode = new System.Windows.Forms.TextBox();
			this.lblKeyCode = new System.Windows.Forms.Label();
			this.btnDoFilter = new System.Windows.Forms.Button();
			this.tabFlights = new AxTABLib.AxTAB();
			((System.ComponentModel.ISupportInitialize)(this.tabFlights)).BeginInit();
			this.SuspendLayout();
			// 
			// picBody
			// 
			this.picBody.Dock = System.Windows.Forms.DockStyle.Fill;
			this.picBody.Location = new System.Drawing.Point(0, 0);
			this.picBody.Name = "picBody";
			this.picBody.Size = new System.Drawing.Size(496, 548);
			this.picBody.TabIndex = 0;
			this.picBody.TabStop = false;
			this.picBody.Paint += new System.Windows.Forms.PaintEventHandler(this.picBody_Paint);
			// 
			// lblAirline
			// 
			this.lblAirline.BackColor = System.Drawing.Color.Transparent;
			this.lblAirline.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblAirline.Location = new System.Drawing.Point(16, 16);
			this.lblAirline.Name = "lblAirline";
			this.lblAirline.Size = new System.Drawing.Size(56, 16);
			this.lblAirline.TabIndex = 1;
			this.lblAirline.Text = "Airline:";
			// 
			// txtAirline
			// 
			this.txtAirline.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtAirline.Location = new System.Drawing.Point(84, 16);
			this.txtAirline.Name = "txtAirline";
			this.txtAirline.TabIndex = 2;
			this.txtAirline.Text = "";
			// 
			// txtFLNO
			// 
			this.txtFLNO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtFLNO.Location = new System.Drawing.Point(260, 16);
			this.txtFLNO.Name = "txtFLNO";
			this.txtFLNO.TabIndex = 4;
			this.txtFLNO.Text = "";
			// 
			// lblFLNO
			// 
			this.lblFLNO.BackColor = System.Drawing.Color.Transparent;
			this.lblFLNO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblFLNO.Location = new System.Drawing.Point(196, 16);
			this.lblFLNO.Name = "lblFLNO";
			this.lblFLNO.Size = new System.Drawing.Size(56, 16);
			this.lblFLNO.TabIndex = 3;
			this.lblFLNO.Text = "FLNO:";
			// 
			// txtOrigDest
			// 
			this.txtOrigDest.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtOrigDest.Location = new System.Drawing.Point(84, 40);
			this.txtOrigDest.Name = "txtOrigDest";
			this.txtOrigDest.TabIndex = 6;
			this.txtOrigDest.Text = "";
			// 
			// lblOrigDest
			// 
			this.lblOrigDest.BackColor = System.Drawing.Color.Transparent;
			this.lblOrigDest.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblOrigDest.Location = new System.Drawing.Point(16, 42);
			this.lblOrigDest.Name = "lblOrigDest";
			this.lblOrigDest.Size = new System.Drawing.Size(64, 16);
			this.lblOrigDest.TabIndex = 5;
			this.lblOrigDest.Text = "Orig/Dest:";
			// 
			// txtNature
			// 
			this.txtNature.Location = new System.Drawing.Point(84, 64);
			this.txtNature.Name = "txtNature";
			this.txtNature.TabIndex = 8;
			this.txtNature.Text = "";
			// 
			// lblNature
			// 
			this.lblNature.BackColor = System.Drawing.Color.Transparent;
			this.lblNature.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblNature.Location = new System.Drawing.Point(16, 67);
			this.lblNature.Name = "lblNature";
			this.lblNature.Size = new System.Drawing.Size(64, 16);
			this.lblNature.TabIndex = 7;
			this.lblNature.Text = "Nature:";
			// 
			// txtKeyCode
			// 
			this.txtKeyCode.Location = new System.Drawing.Point(260, 64);
			this.txtKeyCode.Name = "txtKeyCode";
			this.txtKeyCode.TabIndex = 10;
			this.txtKeyCode.Text = "";
			// 
			// lblKeyCode
			// 
			this.lblKeyCode.BackColor = System.Drawing.Color.Transparent;
			this.lblKeyCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblKeyCode.Location = new System.Drawing.Point(192, 68);
			this.lblKeyCode.Name = "lblKeyCode";
			this.lblKeyCode.Size = new System.Drawing.Size(64, 16);
			this.lblKeyCode.TabIndex = 9;
			this.lblKeyCode.Text = "Key Code:";
			// 
			// btnDoFilter
			// 
			this.btnDoFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnDoFilter.Location = new System.Drawing.Point(372, 64);
			this.btnDoFilter.Name = "btnDoFilter";
			this.btnDoFilter.TabIndex = 11;
			this.btnDoFilter.Text = "&Do Filter";
			this.btnDoFilter.Click += new System.EventHandler(this.btnDoFilter_Click);
			// 
			// tabFlights
			// 
			this.tabFlights.Location = new System.Drawing.Point(12, 104);
			this.tabFlights.Name = "tabFlights";
			this.tabFlights.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabFlights.OcxState")));
			this.tabFlights.Size = new System.Drawing.Size(468, 436);
			this.tabFlights.TabIndex = 12;
			this.tabFlights.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabFlights_SendLButtonDblClick);
			this.tabFlights.RowSelectionChanged += new AxTABLib._DTABEvents_RowSelectionChangedEventHandler(this.tabFlights_RowSelectionChanged);
			// 
			// frmFind
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(496, 548);
			this.Controls.Add(this.tabFlights);
			this.Controls.Add(this.btnDoFilter);
			this.Controls.Add(this.txtKeyCode);
			this.Controls.Add(this.txtNature);
			this.Controls.Add(this.txtOrigDest);
			this.Controls.Add(this.txtFLNO);
			this.Controls.Add(this.txtAirline);
			this.Controls.Add(this.lblKeyCode);
			this.Controls.Add(this.lblNature);
			this.Controls.Add(this.lblOrigDest);
			this.Controls.Add(this.lblFLNO);
			this.Controls.Add(this.lblAirline);
			this.Controls.Add(this.picBody);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmFind";
			this.ShowInTaskbar = false;
			this.Text = "Find Flights ...";
			this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmFind_KeyPress);
			this.Load += new System.EventHandler(this.frmFind_Load);
			this.Closed += new System.EventHandler(this.frmFind_Closed);
			((System.ComponentModel.ISupportInitialize)(this.tabFlights)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void picBody_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, picBody, Color.WhiteSmoke, Color.Gray);			
		}

		private void frmFind_Closed(object sender, System.EventArgs e)
		{
			DE.Call_FindList_Closed();
			WriteRegistry();
		}

		private void frmFind_Load(object sender, System.EventArgs e)
		{
			myDB = UT.GetMemDB();
			myAFT = myDB["AFT"];

			lblAirline.Parent = picBody;
			lblFLNO.Parent = picBody;
			lblNature.Parent = picBody;
			lblOrigDest.Parent = picBody; 
			lblKeyCode.Parent = picBody;
			tabFlights.ResetContent();
			tabFlights.HeaderString = "URNO,A/D,FLNO,Sched.,Est.,Act.,ORG/DES";
			tabFlights.LogicalFieldList = "URNO,ADID,FLNO,SCHED,EST,ACTUAL,ORGDES";
			tabFlights.HeaderLengthString = "0,40,80,80,80,80,80";
			tabFlights.ColumnWidthString = "10,2,8,14,14,14,5";
			tabFlights.LifeStyle = true;
			for(int i = 3; i <= 5; i++)
			{
				tabFlights.DateTimeSetColumn(i);
				tabFlights.DateTimeSetColumnFormat( i, "YYYYMMDDhhmmss", "hh':'mm'/'DD");
			}
			ReadRegistry();
		}

		private void btnDoFilter_Click(object sender, System.EventArgs e)
		{
			DoFilter();
		}

		private void tabFlights_RowSelectionChanged(object sender, AxTABLib._DTABEvents_RowSelectionChangedEvent e)
		{
			if(e.lineNo > -1 && e.selected == true)
			{
				string strAftUrno = tabFlights.GetFieldValue(e.lineNo, "URNO");
				DE.Call_NavigateToFlight((this), strAftUrno);
			}
		}

		private void tabFlights_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo == -1)
			{
				if( e.colNo == tabFlights.CurrentSortColumn)
				{
					if(tabFlights.SortOrderASC == true)
					{
						tabFlights.Sort( e.colNo.ToString(), false, true);
					}
					else
					{ 
						tabFlights.Sort( e.colNo.ToString(), true, true);
					}
				}
				else
				{
					tabFlights.Sort( e.colNo.ToString(), true, true);
				}
				tabFlights.Refresh();
			}
		}

		private void frmFind_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if(e.KeyChar == (char)13)
			{
				DoFilter();
			}
		}
		private void DoFilter()
		{
			string strFAlc = "";
			string strFFlno  = "";
			string strFOrig  = "";
			string strFNat  = "";
			string strFKeycode  = "";
			string strAlc2  = "";
			string strFlno  = "";
			string strAlc3  = "";
			string strOrig  = "";
			string strDest  = "";
			string strNat  = "";
			string strKeycode  = "";
			string strAdid  = "";
			bool blInsert;
			string strValues = "";
			int i;
		    
			strFAlc = txtAirline.Text;
			strFFlno = txtFLNO.Text;
			strFOrig = txtOrigDest.Text;
			strFNat = txtNature.Text;
			strFKeycode = txtKeyCode.Text;
		    
			tabFlights.ResetContent();
			for( i = 0; i < myAFT.Count; i++)
			{
				blInsert = true;
				strFlno = myAFT[i]["FLNO"].Trim();
				strAdid = myAFT[i]["ADID"].Trim();
				strAlc2 = myAFT[i]["ALC2"].Trim();
				strAlc3 = myAFT[i]["ALC3"].Trim();
				strOrig = myAFT[i]["ORG3"].Trim();
				strDest = myAFT[i]["DES3"].Trim();
				strNat = myAFT[i]["TTYP"].Trim();
				strKeycode = myAFT[i]["STEV"];
				if( strAdid == "A" )
				{
					if (strFAlc != "") 
					{
						if ( strFAlc != strAlc2  )
						{
							if ( strFAlc != strAlc3  )
							{
								blInsert = false;
							}
						}
					}
					if ( strFFlno != ""  )
					{
						if ( strFFlno != strFlno  )
						{
							blInsert = false;
						}
					}
					if ( strFOrig != ""  )
					{
						if ( strFOrig != strOrig  )
						{
							blInsert = false;
						}
					}
					if ( strFNat != ""  )
					{
						if ( strFNat != strNat  )
						{
							blInsert = false;
						}
					}
					if ( strFKeycode != ""  )
					{
						if ( strFKeycode != strKeycode  )
						{
							blInsert = false;
						}
					}
					if ( blInsert == true  ) //insert the record
					{
						strValues = myAFT[i]["URNO"] + "," + myAFT[i]["ADID"] + "," + myAFT[i]["FLNO"] + "," + 
							myAFT[i]["STOA"] + "," + myAFT[i]["ETAI"] + "," + myAFT[i]["TIFA"] + "," + 
							myAFT[i]["ORG3"];
						tabFlights.InsertTextLine( strValues, false);
					}
				}
				if ( strAdid == "D"  )
				{
					if ( (strFAlc != "")  )
					{
						if ( strFAlc != strAlc2  )
						{
							if ( strFAlc != strAlc3  )
							{
								blInsert = false;
							}
						}
					}
					if ( strFFlno != ""  )
					{
						if ( strFFlno != strFlno  )
						{
							blInsert = false;
						}
					}
					if ( strFOrig != ""  )
					{
						if ( strFOrig != strDest  )
						{
							blInsert = false;
						}
					}
					if ( strFNat != ""  )
					{
						if ( strFNat != strNat  )
						{
							blInsert = false;
						}
					}
					if ( strFKeycode != ""  )
					{
						if ( strFKeycode != strKeycode  )
						{
							blInsert = false;
						}
					}
					if ( blInsert == true  ) // insert the record
					{
						strValues = myAFT[i]["URNO"] + "," + myAFT[i]["ADID"] + "," + myAFT[i]["FLNO"] + "," + 
							myAFT[i]["STOD"] + "," + myAFT[i]["ETDI"] + "," + myAFT[i]["TIFD"] + "," + 
							myAFT[i]["DES3"];
						tabFlights.InsertTextLine( strValues, false);
					}
				}
			}
			tabFlights.Sort( "5", true, true);
			tabFlights.Refresh();
		}
		private void ReadRegistry()
		{
			int myX=-1, myY=-1, myW=-1, myH=-1;
			string regVal = "";
			string theKey = "Software\\StatusManager\\FindDialog";
			RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			if(rk != null)
			{
				regVal = rk.GetValue("X", "-1").ToString();
				myX = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Y", "-1").ToString();
				myY = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Width", "-1").ToString();
				myW = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Height", "-1").ToString();
				myH = Convert.ToInt32(regVal);
			}
			if((myW != -1 && myH != -1) && (myX < Screen.PrimaryScreen.Bounds.Width))
			{
				this.Left = myX;
				this.Top = myY;
				this.Width = myW;
				this.Height = myH;
			}
		}
		private void WriteRegistry()
		{
			string theKey = "Software\\StatusManager\\FindDialog";
			RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			if(rk == null)
			{
				rk = Registry.CurrentUser.OpenSubKey("Software\\StatusManager", true);
				rk.CreateSubKey("FindDialog");
				rk.Close();
				theKey = "Software\\StatusManager\\FindDialog";
				rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			}

			rk.SetValue("X", this.Left.ToString());
			rk.SetValue("Y", this.Top.ToString());
			rk.SetValue("Width", this.Width.ToString());
			rk.SetValue("Height", this.Height.ToString());
		}

	}
}
