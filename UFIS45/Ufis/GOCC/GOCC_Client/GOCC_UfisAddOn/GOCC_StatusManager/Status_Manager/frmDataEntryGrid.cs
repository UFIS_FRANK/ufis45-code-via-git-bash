using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Ufis.Utils;
using Ufis.Data;
namespace Status_Manager
{
	/// <summary>
	/// Summary description for frmDataEntryGrid.
	/// </summary>
	public class frmDataEntryGrid : System.Windows.Forms.Form
	{
		private System.Windows.Forms.PictureBox pictureBox1;
		private AxTABLib.AxTAB tabData;
		private System.Windows.Forms.Button btnPrint;
		private System.Windows.Forms.Label lblCaption;
		public string currTabelName = "";
		public string myHeaderString = "";
		public string myHeaderLen = "";
		public string myCaption = "";
		public string myFields = "";
		public int myWidth = -1;
		public int myHeight = -1;
		private IDatabase  myDB = null;
		private ITable myTable = null;
		private System.Windows.Forms.Button lblAdd;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Button bntDelete;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.ImageList imageList1;
		private System.ComponentModel.IContainer components;

		public frmDataEntryGrid()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmDataEntryGrid));
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.tabData = new AxTABLib.AxTAB();
			this.btnPrint = new System.Windows.Forms.Button();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.lblCaption = new System.Windows.Forms.Label();
			this.lblAdd = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnClose = new System.Windows.Forms.Button();
			this.btnSave = new System.Windows.Forms.Button();
			this.bntDelete = new System.Windows.Forms.Button();
			this.panel2 = new System.Windows.Forms.Panel();
			((System.ComponentModel.ISupportInitialize)(this.tabData)).BeginInit();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// pictureBox1
			// 
			this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pictureBox1.Location = new System.Drawing.Point(0, 0);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(648, 38);
			this.pictureBox1.TabIndex = 0;
			this.pictureBox1.TabStop = false;
			this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
			// 
			// tabData
			// 
			this.tabData.ContainingControl = this;
			this.tabData.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabData.Location = new System.Drawing.Point(0, 0);
			this.tabData.Name = "tabData";
			this.tabData.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabData.OcxState")));
			this.tabData.Size = new System.Drawing.Size(648, 564);
			this.tabData.TabIndex = 1;
			this.tabData.CloseInplaceEdit += new AxTABLib._DTABEvents_CloseInplaceEditEventHandler(this.tabData_CloseInplaceEdit);
			this.tabData.HitKeyOnLine += new AxTABLib._DTABEvents_HitKeyOnLineEventHandler(this.tabData_HitKeyOnLine);
			this.tabData.RowSelectionChanged += new AxTABLib._DTABEvents_RowSelectionChangedEventHandler(this.tabData_RowSelectionChanged);
			this.tabData.EditPositionChanged += new AxTABLib._DTABEvents_EditPositionChangedEventHandler(this.tabData_EditPositionChanged);
			// 
			// btnPrint
			// 
			this.btnPrint.BackColor = System.Drawing.Color.Transparent;
			this.btnPrint.Dock = System.Windows.Forms.DockStyle.Right;
			this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnPrint.ImageIndex = 5;
			this.btnPrint.ImageList = this.imageList1;
			this.btnPrint.Location = new System.Drawing.Point(576, 0);
			this.btnPrint.Name = "btnPrint";
			this.btnPrint.Size = new System.Drawing.Size(72, 38);
			this.btnPrint.TabIndex = 4;
			this.btnPrint.Text = "   &Print";
			this.btnPrint.Visible = false;
			// 
			// imageList1
			// 
			this.imageList1.ImageSize = new System.Drawing.Size(19, 19);
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.White;
			// 
			// lblCaption
			// 
			this.lblCaption.BackColor = System.Drawing.Color.Black;
			this.lblCaption.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblCaption.ForeColor = System.Drawing.Color.Lime;
			this.lblCaption.Location = new System.Drawing.Point(0, 0);
			this.lblCaption.Name = "lblCaption";
			this.lblCaption.Size = new System.Drawing.Size(648, 24);
			this.lblCaption.TabIndex = 5;
			this.lblCaption.Text = "Simple Data Editor";
			this.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblAdd
			// 
			this.lblAdd.BackColor = System.Drawing.Color.Transparent;
			this.lblAdd.Dock = System.Windows.Forms.DockStyle.Left;
			this.lblAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.lblAdd.ImageIndex = 0;
			this.lblAdd.ImageList = this.imageList1;
			this.lblAdd.Location = new System.Drawing.Point(0, 0);
			this.lblAdd.Name = "lblAdd";
			this.lblAdd.Size = new System.Drawing.Size(72, 38);
			this.lblAdd.TabIndex = 6;
			this.lblAdd.Text = "&Add";
			this.lblAdd.Click += new System.EventHandler(this.lblAdd_Click);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnClose);
			this.panel1.Controls.Add(this.btnSave);
			this.panel1.Controls.Add(this.bntDelete);
			this.panel1.Controls.Add(this.btnPrint);
			this.panel1.Controls.Add(this.lblAdd);
			this.panel1.Controls.Add(this.pictureBox1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 588);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(648, 38);
			this.panel1.TabIndex = 8;
			// 
			// btnClose
			// 
			this.btnClose.BackColor = System.Drawing.Color.Transparent;
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnClose.Dock = System.Windows.Forms.DockStyle.Left;
			this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnClose.ImageIndex = 4;
			this.btnClose.ImageList = this.imageList1;
			this.btnClose.Location = new System.Drawing.Point(216, 0);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(72, 38);
			this.btnClose.TabIndex = 9;
			this.btnClose.Text = "   &Close";
			// 
			// btnSave
			// 
			this.btnSave.BackColor = System.Drawing.Color.Transparent;
			this.btnSave.Dock = System.Windows.Forms.DockStyle.Left;
			this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnSave.ImageIndex = 2;
			this.btnSave.ImageList = this.imageList1;
			this.btnSave.Location = new System.Drawing.Point(144, 0);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(72, 38);
			this.btnSave.TabIndex = 8;
			this.btnSave.Text = "   &Save";
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// bntDelete
			// 
			this.bntDelete.BackColor = System.Drawing.Color.Transparent;
			this.bntDelete.Dock = System.Windows.Forms.DockStyle.Left;
			this.bntDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.bntDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.bntDelete.ImageIndex = 1;
			this.bntDelete.ImageList = this.imageList1;
			this.bntDelete.Location = new System.Drawing.Point(72, 0);
			this.bntDelete.Name = "bntDelete";
			this.bntDelete.Size = new System.Drawing.Size(72, 38);
			this.bntDelete.TabIndex = 7;
			this.bntDelete.Text = "    &Delete";
			this.bntDelete.Click += new System.EventHandler(this.bntDelete_Click);
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.tabData);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(0, 24);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(648, 564);
			this.panel2.TabIndex = 9;
			// 
			// frmDataEntryGrid
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(648, 626);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.lblCaption);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmDataEntryGrid";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Data Editor ...";
			this.Load += new System.EventHandler(this.frmDataEntryGrid_Load);
			((System.ComponentModel.ISupportInitialize)(this.tabData)).EndInit();
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void pictureBox1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, pictureBox1, Color.WhiteSmoke, Color.Gray);			
		}

		private void frmDataEntryGrid_Load(object sender, System.EventArgs e)
		{
			myDB = UT.GetMemDB();
			lblCaption.Text = myCaption;
			myTable = myDB[currTabelName];
			tabData.ResetContent();
			tabData.EnableHeaderSizing(true);
			tabData.EnableInlineEdit(true);
			tabData.AutoSizeByHeader = true;
			tabData.InplaceEditUpperCase = false;
			tabData.ShowHorzScroller(true);
			tabData.LifeStyle = true;
			lblAdd.Parent = pictureBox1;
			btnClose.Parent = pictureBox1;
			btnSave.Parent = pictureBox1;
			bntDelete.Parent = pictureBox1;
			btnPrint.Parent = pictureBox1;

			if (myWidth != -1)
			{
				int ilLeft = this.Left;
				int ilWidth = this.Width;
				this.Width = myWidth;
				this.Left -= (int)(Math.Abs(myWidth - ilWidth ))/2;
			}
			if(myHeight != -1)
			{
				this.Height = myHeight;
			}
			//tabData.CursorLifeStyle = true;
			if(myTable != null)
			{
				myTable.FillVisualizer(tabData);
				tabData.HeaderString = myHeaderString;
				tabData.HeaderLengthString = myHeaderLen;
				tabData.LogicalFieldList = myFields;
			    tabData.FontName = "Arial";
				tabData.FontSize = 15;
				tabData.HeaderFontSize = 15;
				tabData.LineHeight = 16;
				tabData.ColumnWidthString = myTable.FieldLenString;
				tabData.SetTabFontBold(true);

				string strColor = UT.colBlue + "," + UT.colBlue;
				tabData.CursorDecoration(tabData.LogicalFieldList, "B,T", "2,2", strColor);
				tabData.DefaultCursor = false;
				string strNoFocusColumns = "";
				int idx = UT.GetItemNo(myFields, "URNO");
				if(idx > -1)
					strNoFocusColumns += idx.ToString();
				idx = UT.GetItemNo(myFields, "USEC");
				if(idx > -1)
					strNoFocusColumns += "," + idx.ToString();
				idx = UT.GetItemNo(myFields, "USEU");
				if(idx > -1)
					strNoFocusColumns += "," + idx.ToString();
				idx = UT.GetItemNo(myFields, "LSTU");
				if(idx > -1)
				{
					strNoFocusColumns += "," + idx.ToString();
				    tabData.DateTimeSetColumnFormat(idx, "YYYYMMDDhhmmss", "DD'.'MM'.'YY'-'hh':'mm");
				}
				idx = UT.GetItemNo(myFields, "CDAT");
				if(idx > -1)
				{
					strNoFocusColumns += "," + idx.ToString();
					tabData.DateTimeSetColumnFormat(idx, "YYYYMMDDhhmmss", "DD'.'MM'.'YY'-'hh':'mm");
				}
				tabData.NoFocusColumns = strNoFocusColumns;

				for(int i = 0; i < tabData.GetLineCount(); i++)
				{
					tabData.SetLineTag(i, tabData.GetFieldValues(i, tabData.LogicalFieldList));
				}
				tabData.Refresh();
			}
		}

		private void lblAdd_Click(object sender, System.EventArgs e)
		{
			string strEmptyLine = "";
			for (int i = 0; i < UT.ItemCount(myHeaderString, ",")-1; i++)
			{
				strEmptyLine += ",";
			}
			tabData.InsertTextLine(strEmptyLine, true);
			tabData.SetLineColor(tabData.GetLineCount()-1, UT.colBlack, UT.colYellow);
			tabData.OnVScrollTo(tabData.GetLineCount());
		}

		private void CheckValuesChanged(int LineNo)
		{
			int tCol = -1;
			int bCol = -1;
			tabData.GetLineColor(LineNo, ref tCol, ref bCol);
			string strCurrValues = tabData.GetFieldValues(LineNo, tabData.LogicalFieldList);//tabData.GetLineValues(LineNo);
			if(strCurrValues != tabData.GetLineTag(LineNo))
			{
				if(bCol == UT.colWhite)// || bCol == UT.colRed)
				{
					tabData.SetLineColor(LineNo, UT.colBlack, UT.colLightGreen);
					tabData.Refresh();
				}
			}
			else
			{
				if(bCol == UT.colLightGreen)
				{
					tabData.SetLineColor(LineNo, UT.colBlack, UT.colWhite);
					tabData.Refresh();
				}
			}
		}
		private void btnSave_Click(object sender, System.EventArgs e)
		{
			int tColor = -1;
			int bColor = -1;
			for(int i = 0; i < tabData.GetLineCount(); i++)
			{
				tabData.GetLineColor(i, ref tColor, ref bColor);
				IRow row = null;
				string [] arrFlds = myFields.Split(',');
				if(bColor == UT.colYellow) //New
				{
					row = myTable.CreateEmptyRow();
					for(int j = 0; j < arrFlds.Length; j++)
					{
						if(arrFlds[j] != "URNO" && arrFlds[j] != "LSTU" && 
						   arrFlds[j] != "CDAT" && arrFlds[j] != "USEC" && arrFlds[j] != "USEU")
						{
							row[arrFlds[j]] = tabData.GetColumnValue(i, j);
						}
					}
					string strTime = UT.DateTimeToCeda( DateTime.Now);
					row["CDAT"] = strTime;
					tabData.SetFieldValues(i, "CDAT", strTime);
					row["USEC"] = UT.UserName;
					tabData.SetFieldValues(i, "USEC", UT.UserName);
					tabData.SetFieldValues(i, "URNO", row["URNO"]);
					row.Status = State.Created;
					myTable.Add(row);
					myTable.Save();
					tabData.SetLineColor(i, UT.colBlack , UT.colWhite);
					tabData.Refresh();
				}
				if(bColor == UT.colLightGreen)//Updated
				{
					int idxUrno = UT.GetItemNo(myFields, "URNO");
					if(idxUrno > -1)
					{
						IRow [] rows = myTable.RowsByIndexValue("URNO", tabData.GetColumnValue(i, idxUrno));
						if(rows.Length > 0)
						{
							for(int j = 0; j < arrFlds.Length; j++)
							{
								if(arrFlds[j] != "URNO" && arrFlds[j] != "LSTU" && 
									arrFlds[j] != "CDAT" && arrFlds[j] != "USEC" && arrFlds[j] != "USEU")
								{
									rows[0][arrFlds[j]] = tabData.GetColumnValue(i, j);
								}
							}
							string strTime = UT.DateTimeToCeda( DateTime.Now);
							rows[0]["LSTU"] = strTime;
							tabData.SetFieldValues(i, "LSTU", strTime);
							rows[0]["USEU"] = UT.UserName;
							tabData.SetFieldValues(i, "USEU", UT.UserName);
							rows[0].Status = State.Modified;
						}
					}
					myTable.Save();
					tabData.SetLineColor(i, UT.colBlack , UT.colWhite);
					tabData.Refresh();
				}
				if(bColor == UT.colRed) // Deleted
				{
					int idxUrno = UT.GetItemNo(myFields, "URNO");
					if(idxUrno > -1)
					{
						IRow [] rows = myTable.RowsByIndexValue("URNO", tabData.GetColumnValue(i, idxUrno));
						if(rows.Length > 0)
						{
							rows[0].Status = State.Deleted;
							myTable.Save();
						}
					}
				}
			}
			tabData.Refresh();
			//Remove the deleted (red) lines
			for(int i = tabData.GetLineCount()-1; i >= 0; i--)
			{
				tabData.GetLineColor(i, ref tColor, ref bColor);
				if(bColor == UT.colRed)
				{
					tabData.DeleteLine(i);
				}
			}
			for(int i = 0; i < tabData.GetLineCount(); i++)
			{
				string strValues = tabData.GetFieldValues(i, tabData.LogicalFieldList);//tabData.GetLineValues(i);
				tabData.SetLineTag(i, strValues);
			}
			myTable.ReorganizeIndexes();
			tabData.Refresh();
		}


		private void tabData_CloseInplaceEdit(object sender, AxTABLib._DTABEvents_CloseInplaceEditEvent e)
		{
			CheckValuesChanged(e.lineNo );
		}

		private void tabData_EditPositionChanged(object sender, AxTABLib._DTABEvents_EditPositionChangedEvent e)
		{
			CheckValuesChanged(e.lineNo );
		}

		private void tabData_RowSelectionChanged(object sender, AxTABLib._DTABEvents_RowSelectionChangedEvent e)
		{
			CheckValuesChanged(e.lineNo );
		}

		private void bntDelete_Click(object sender, System.EventArgs e)
		{
			HandleDelete();
		}
		private void HandleDelete()
		{
			int currSel = tabData.GetCurrentSelected();
			int bCol = -1;
			int tCol = -1;
			if(currSel > -1)
			{
				tabData.GetLineColor(currSel, ref tCol, ref bCol);
				if(bCol != UT.colRed)
				{
					tabData.SetLineColor(currSel, UT.colBlack, UT.colRed);
				}
				if(bCol == UT.colYellow)
				{
					tabData.DeleteLine(currSel);
				}
				if(bCol == UT.colRed)
				{
					tabData.SetLineColor(currSel, UT.colBlack, UT.colWhite);
					CheckValuesChanged(currSel);
				}
				tabData.Refresh();
			}
		}

		private void tabData_HitKeyOnLine(object sender, AxTABLib._DTABEvents_HitKeyOnLineEvent e)
		{
			if(e.key == Convert.ToInt16( Keys.Delete))
			{
				HandleDelete();
			}
		}
	}
}
