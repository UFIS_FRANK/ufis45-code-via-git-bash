using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Ufis.Utils;
using Ufis.Data;
using DataDynamics.ActiveReports;
using Microsoft.Win32;

#region Changes History
    //moe 20080522 - Add the "Show Deleted" button
    //               Disable when is there any changes in SDE Table rule/Add New Entry in SDE Table
#endregion

using Status_Manager.DS;

namespace Status_Manager
{
	/// <summary>
	/// Summary description for frmRules.
	/// </summary>
	public class frmRules : System.Windows.Forms.Form
	{
		#region ----- myMembers

		string [] myURNOFields;
		string [] myVALUEFields;
		string [] myLookupTabs;
		string [] myLookupFields;
		string myMode = "NEW";
		IRow currRow = null;
		IRow currDBRow = null;
		IRow oldRow = null;
		IDatabase myDB = null;
		string [] myRETY_DB;  //RETY values, which have to be stored in DB   [STATUSMANAGER]RETY_DB
		string [] myRETY_GUI; //RETY values to prepare for the user Ceda.ini [STATUSMANAGER]RETY_GUI 
		string [] myRTIM_DB;  //RTIM values, which have to be stored in DB   [STATUSMANAGER]RTIM_DB
		string [] myRTIM_GUI; //RTIM values to prepare for the user Ceda.ini [STATUSMANAGER]RTIM_GUI 
		string [] myAFTF_DB_ARR;  //AFTF Arrival values, which have to be stored in DB   [STATUSMANAGER]AFTF_DB
		string [] myAFTF_GUI_ARR; //AFTF Arrival values to prepare for the user Ceda.ini   [STATUSMANAGER]AFTF_GUI
		string [] myAFTF_DB_DEP;  //AFTF Departure values, which have to be stored in DB   [STATUSMANAGER]AFTF_DB
		string [] myAFTF_GUI_DEP; //AFTF Departure values to prepare for the user Ceda.ini   [STATUSMANAGER]AFTF_GUI

		bool PredecessorVisible = true;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.Button btnRecalcFlights;
		private System.Windows.Forms.Button btnClose; //If predecessor is visible
		int myCurrEditColumn = -1;
		DateTime oldValidFrom;
		DateTime oldValidTo;

		#endregion ----- myMembers

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Button btnOpen;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Label lblEvent;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label lblACType;
		private System.Windows.Forms.TextBox txtACTValue;
		private System.Windows.Forms.TextBox txtACTUrno;
		private System.Windows.Forms.TextBox txtACTGroupUrno;
		private System.Windows.Forms.TextBox txtACTGroupValue;
		private System.Windows.Forms.TextBox txtAPTGroupUrno;
		private System.Windows.Forms.TextBox txtAPTGroupValue;
		private System.Windows.Forms.Label lblAirportGroup;
		private System.Windows.Forms.TextBox txtAPTUrno;
		private System.Windows.Forms.TextBox txtAPTValue;
		private System.Windows.Forms.Label lblAirport;
		private System.Windows.Forms.Label lblRuleName;
		private System.Windows.Forms.TextBox txtRuleName;
		private System.Windows.Forms.TextBox txtALTGroupUrno;
		private System.Windows.Forms.TextBox txtALTGroupValue;
		private System.Windows.Forms.TextBox txtALTUrno;
		private System.Windows.Forms.TextBox txtALTValue;
		private System.Windows.Forms.Label lblAirline;
		private System.Windows.Forms.TextBox txtNATGroupUrno;
		private System.Windows.Forms.Label lblNatureGroup;
		private System.Windows.Forms.TextBox txtNATUrno;
		private System.Windows.Forms.TextBox txtNATValue;
		private System.Windows.Forms.Label lblNature;
		private System.Windows.Forms.Button btnACT;
		private System.Windows.Forms.Button btnACTGroup;
		private System.Windows.Forms.Button btnAPTGroup;
		private System.Windows.Forms.Button btnAPT;
		private System.Windows.Forms.Button btnALTGroup;
		private System.Windows.Forms.Button btnALT;
		private System.Windows.Forms.Button btnNATGroup;
		private System.Windows.Forms.Button btnNAT;
		private AxTABLib.AxTAB tabDefs;
		private System.Windows.Forms.PictureBox pictureBox2;
		private System.Windows.Forms.PictureBox pictureBox3;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Label lblAirineGroup;
		private System.Windows.Forms.Label lblACGroup;
		private System.Windows.Forms.Button btnAddDef;
		private System.Windows.Forms.TextBox txtNATGroupValue;
		private System.Windows.Forms.RadioButton rbArrival;
		private System.Windows.Forms.RadioButton rbDeparture;
		private System.Windows.Forms.Label lblValidFrom;
		private System.Windows.Forms.Label lblValidTo;
		private System.Windows.Forms.DateTimePicker dtValidFrom;
		private System.Windows.Forms.DateTimePicker dtValidTo;
		private System.Windows.Forms.Label lblUSEC;
		private System.Windows.Forms.Label lblUSEU;
		private System.Windows.Forms.Label lblCDAT;
		private System.Windows.Forms.TextBox txtUSEC;
		private System.Windows.Forms.TextBox txtUSEU;
		private System.Windows.Forms.TextBox txtCDAT;
		private System.Windows.Forms.TextBox txtLSTU;
		private System.Windows.Forms.Label lblLSTU;
		private System.Windows.Forms.Label lblSection;
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.Button btnNew;
		private System.Windows.Forms.Button btnStatusSection;
		private System.Windows.Forms.Panel panelHeader;
		private System.Windows.Forms.Button btnDeleteDef;
		private System.Windows.Forms.Button btnDeleteHeader;
		private System.Windows.Forms.TextBox txtSTSValue;
		private System.Windows.Forms.Button btnSTS;
		private System.Windows.Forms.TextBox txtSTSUrno;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox txtTagTest;
		private System.Windows.Forms.ContextMenu mnuContextDetail;
		private System.Windows.Forms.MenuItem mnuReactivate;
		private System.ComponentModel.IContainer components;
		private System.Windows.Forms.Label lblMAXT;
		private System.Windows.Forms.RadioButton rbTurnaround;
		private System.Windows.Forms.NumericUpDown numericUpDown1;
		private System.Windows.Forms.TextBox txtMAXT;
        private System.Windows.Forms.Label lblInMin;
        private System.Windows.Forms.CheckBox cbShowDeleted;
		private System.Windows.Forms.Button btnCopy;  

		
		public frmRules()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
           

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRules));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnRecalcFlights = new System.Windows.Forms.Button();
            this.btnCopy = new System.Windows.Forms.Button();
            this.txtTagTest = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btnDeleteHeader = new System.Windows.Forms.Button();
            this.btnStatusSection = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnOpen = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panelHeader = new System.Windows.Forms.Panel();
            this.cbShowDeleted = new System.Windows.Forms.CheckBox();
            this.btnDeleteDef = new System.Windows.Forms.Button();
            this.btnAddDef = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblInMin = new System.Windows.Forms.Label();
            this.txtMAXT = new System.Windows.Forms.TextBox();
            this.rbTurnaround = new System.Windows.Forms.RadioButton();
            this.lblMAXT = new System.Windows.Forms.Label();
            this.txtSTSValue = new System.Windows.Forms.TextBox();
            this.txtSTSUrno = new System.Windows.Forms.TextBox();
            this.btnSTS = new System.Windows.Forms.Button();
            this.lblSection = new System.Windows.Forms.Label();
            this.txtLSTU = new System.Windows.Forms.TextBox();
            this.txtCDAT = new System.Windows.Forms.TextBox();
            this.txtUSEU = new System.Windows.Forms.TextBox();
            this.txtUSEC = new System.Windows.Forms.TextBox();
            this.lblLSTU = new System.Windows.Forms.Label();
            this.lblCDAT = new System.Windows.Forms.Label();
            this.lblUSEU = new System.Windows.Forms.Label();
            this.lblUSEC = new System.Windows.Forms.Label();
            this.dtValidTo = new System.Windows.Forms.DateTimePicker();
            this.dtValidFrom = new System.Windows.Forms.DateTimePicker();
            this.lblValidTo = new System.Windows.Forms.Label();
            this.lblValidFrom = new System.Windows.Forms.Label();
            this.lblAirportGroup = new System.Windows.Forms.Label();
            this.lblNatureGroup = new System.Windows.Forms.Label();
            this.txtNATValue = new System.Windows.Forms.TextBox();
            this.lblNature = new System.Windows.Forms.Label();
            this.txtALTGroupValue = new System.Windows.Forms.TextBox();
            this.lblAirineGroup = new System.Windows.Forms.Label();
            this.txtALTValue = new System.Windows.Forms.TextBox();
            this.txtNATGroupUrno = new System.Windows.Forms.TextBox();
            this.btnNATGroup = new System.Windows.Forms.Button();
            this.txtNATGroupValue = new System.Windows.Forms.TextBox();
            this.txtAPTUrno = new System.Windows.Forms.TextBox();
            this.txtAPTValue = new System.Windows.Forms.TextBox();
            this.txtNATUrno = new System.Windows.Forms.TextBox();
            this.btnAPT = new System.Windows.Forms.Button();
            this.lblAirport = new System.Windows.Forms.Label();
            this.txtACTGroupUrno = new System.Windows.Forms.TextBox();
            this.btnNAT = new System.Windows.Forms.Button();
            this.txtACTGroupValue = new System.Windows.Forms.TextBox();
            this.txtALTGroupUrno = new System.Windows.Forms.TextBox();
            this.btnACTGroup = new System.Windows.Forms.Button();
            this.lblACGroup = new System.Windows.Forms.Label();
            this.txtACTUrno = new System.Windows.Forms.TextBox();
            this.btnALTGroup = new System.Windows.Forms.Button();
            this.txtACTValue = new System.Windows.Forms.TextBox();
            this.txtALTUrno = new System.Windows.Forms.TextBox();
            this.btnACT = new System.Windows.Forms.Button();
            this.lblACType = new System.Windows.Forms.Label();
            this.btnALT = new System.Windows.Forms.Button();
            this.lblAirline = new System.Windows.Forms.Label();
            this.txtRuleName = new System.Windows.Forms.TextBox();
            this.lblRuleName = new System.Windows.Forms.Label();
            this.txtAPTGroupUrno = new System.Windows.Forms.TextBox();
            this.txtAPTGroupValue = new System.Windows.Forms.TextBox();
            this.btnAPTGroup = new System.Windows.Forms.Button();
            this.rbArrival = new System.Windows.Forms.RadioButton();
            this.rbDeparture = new System.Windows.Forms.RadioButton();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblEvent = new System.Windows.Forms.Label();
            this.tabDefs = new AxTABLib.AxTAB();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.mnuContextDetail = new System.Windows.Forms.ContextMenu();
            this.mnuReactivate = new System.Windows.Forms.MenuItem();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panelHeader.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabDefs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Controls.Add(this.btnRecalcFlights);
            this.panel1.Controls.Add(this.btnCopy);
            this.panel1.Controls.Add(this.txtTagTest);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.btnDeleteHeader);
            this.panel1.Controls.Add(this.btnStatusSection);
            this.panel1.Controls.Add(this.btnNew);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Controls.Add(this.btnOpen);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1028, 40);
            this.panel1.TabIndex = 99;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.Transparent;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.ImageIndex = 14;
            this.btnClose.ImageList = this.imageList1;
            this.btnClose.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnClose.Location = new System.Drawing.Point(936, 0);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 40);
            this.btnClose.TabIndex = 29;
            this.btnClose.TabStop = false;
            this.btnClose.Text = "   &Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.White;
            this.imageList1.Images.SetKeyName(0, "");
            this.imageList1.Images.SetKeyName(1, "");
            this.imageList1.Images.SetKeyName(2, "");
            this.imageList1.Images.SetKeyName(3, "");
            this.imageList1.Images.SetKeyName(4, "");
            this.imageList1.Images.SetKeyName(5, "");
            this.imageList1.Images.SetKeyName(6, "");
            this.imageList1.Images.SetKeyName(7, "");
            this.imageList1.Images.SetKeyName(8, "");
            this.imageList1.Images.SetKeyName(9, "");
            this.imageList1.Images.SetKeyName(10, "");
            this.imageList1.Images.SetKeyName(11, "");
            this.imageList1.Images.SetKeyName(12, "");
            this.imageList1.Images.SetKeyName(13, "");
            this.imageList1.Images.SetKeyName(14, "");
            this.imageList1.Images.SetKeyName(15, "");
            // 
            // btnRecalcFlights
            // 
            this.btnRecalcFlights.BackColor = System.Drawing.Color.Transparent;
            this.btnRecalcFlights.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnRecalcFlights.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRecalcFlights.ImageIndex = 13;
            this.btnRecalcFlights.ImageList = this.imageList1;
            this.btnRecalcFlights.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnRecalcFlights.Location = new System.Drawing.Point(380, 0);
            this.btnRecalcFlights.Name = "btnRecalcFlights";
            this.btnRecalcFlights.Size = new System.Drawing.Size(100, 40);
            this.btnRecalcFlights.TabIndex = 28;
            this.btnRecalcFlights.TabStop = false;
            this.btnRecalcFlights.Text = "    &Recalculate";
            this.btnRecalcFlights.UseVisualStyleBackColor = false;
            this.btnRecalcFlights.Click += new System.EventHandler(this.btnRecalcFlights_Click);
            // 
            // btnCopy
            // 
            this.btnCopy.BackColor = System.Drawing.Color.Transparent;
            this.btnCopy.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnCopy.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCopy.ImageIndex = 12;
            this.btnCopy.ImageList = this.imageList1;
            this.btnCopy.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnCopy.Location = new System.Drawing.Point(304, 0);
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(75, 40);
            this.btnCopy.TabIndex = 27;
            this.btnCopy.TabStop = false;
            this.btnCopy.Text = "   C&opy";
            this.btnCopy.UseVisualStyleBackColor = false;
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // txtTagTest
            // 
            this.txtTagTest.Location = new System.Drawing.Point(736, 8);
            this.txtTagTest.Name = "txtTagTest";
            this.txtTagTest.Size = new System.Drawing.Size(100, 20);
            this.txtTagTest.TabIndex = 25;
            this.txtTagTest.Text = "ACTU";
            this.txtTagTest.Visible = false;
            // 
            // button1
            // 
            this.button1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button1.Location = new System.Drawing.Point(660, 8);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 24;
            this.button1.Text = "button1";
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnDeleteHeader
            // 
            this.btnDeleteHeader.BackColor = System.Drawing.Color.Transparent;
            this.btnDeleteHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnDeleteHeader.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDeleteHeader.ImageIndex = 4;
            this.btnDeleteHeader.ImageList = this.imageList1;
            this.btnDeleteHeader.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnDeleteHeader.Location = new System.Drawing.Point(152, 0);
            this.btnDeleteHeader.Name = "btnDeleteHeader";
            this.btnDeleteHeader.Size = new System.Drawing.Size(75, 40);
            this.btnDeleteHeader.TabIndex = 23;
            this.btnDeleteHeader.TabStop = false;
            this.btnDeleteHeader.Text = "    &Delete";
            this.btnDeleteHeader.UseVisualStyleBackColor = false;
            this.btnDeleteHeader.Click += new System.EventHandler(this.btnDeleteHeader_Click);
            // 
            // btnStatusSection
            // 
            this.btnStatusSection.BackColor = System.Drawing.Color.Transparent;
            this.btnStatusSection.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnStatusSection.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStatusSection.ImageIndex = 6;
            this.btnStatusSection.ImageList = this.imageList1;
            this.btnStatusSection.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnStatusSection.Location = new System.Drawing.Point(512, 0);
            this.btnStatusSection.Name = "btnStatusSection";
            this.btnStatusSection.Size = new System.Drawing.Size(132, 40);
            this.btnStatusSection.TabIndex = 22;
            this.btnStatusSection.TabStop = false;
            this.btnStatusSection.Text = "S&tatus Sections";
            this.btnStatusSection.UseVisualStyleBackColor = false;
            this.btnStatusSection.Click += new System.EventHandler(this.btnStatusSection_Click);
            // 
            // btnNew
            // 
            this.btnNew.BackColor = System.Drawing.Color.Transparent;
            this.btnNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnNew.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNew.ImageIndex = 2;
            this.btnNew.ImageList = this.imageList1;
            this.btnNew.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnNew.Location = new System.Drawing.Point(0, 0);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(75, 40);
            this.btnNew.TabIndex = 21;
            this.btnNew.TabStop = false;
            this.btnNew.Text = "  &New";
            this.btnNew.UseVisualStyleBackColor = false;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Transparent;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.ImageIndex = 5;
            this.btnSave.ImageList = this.imageList1;
            this.btnSave.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnSave.Location = new System.Drawing.Point(228, 0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 40);
            this.btnSave.TabIndex = 20;
            this.btnSave.TabStop = false;
            this.btnSave.Text = "   &Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnOpen
            // 
            this.btnOpen.BackColor = System.Drawing.Color.Transparent;
            this.btnOpen.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnOpen.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOpen.ImageIndex = 3;
            this.btnOpen.ImageList = this.imageList1;
            this.btnOpen.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnOpen.Location = new System.Drawing.Point(76, 0);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(75, 40);
            this.btnOpen.TabIndex = 19;
            this.btnOpen.TabStop = false;
            this.btnOpen.Text = "   &Open";
            this.btnOpen.UseVisualStyleBackColor = false;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1028, 50);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            // 
            // panelHeader
            // 
            this.panelHeader.Controls.Add(this.cbShowDeleted);
            this.panelHeader.Controls.Add(this.btnDeleteDef);
            this.panelHeader.Controls.Add(this.btnAddDef);
            this.panelHeader.Controls.Add(this.panel3);
            this.panelHeader.Controls.Add(this.label1);
            this.panelHeader.Controls.Add(this.lblEvent);
            this.panelHeader.Controls.Add(this.tabDefs);
            this.panelHeader.Controls.Add(this.pictureBox2);
            this.panelHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelHeader.Location = new System.Drawing.Point(0, 40);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(1028, 706);
            this.panelHeader.TabIndex = 1;
            // 
            // cbShowDeleted
            // 
            this.cbShowDeleted.BackColor = System.Drawing.Color.Transparent;
            this.cbShowDeleted.Checked = true;
            this.cbShowDeleted.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbShowDeleted.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbShowDeleted.Location = new System.Drawing.Point(201, 616);
            this.cbShowDeleted.Name = "cbShowDeleted";
            this.cbShowDeleted.Size = new System.Drawing.Size(130, 24);
            this.cbShowDeleted.TabIndex = 43;
            this.cbShowDeleted.Text = "&Show Deleted";
            this.cbShowDeleted.UseVisualStyleBackColor = false;
            this.cbShowDeleted.CheckedChanged += new System.EventHandler(this.cbShowDeleted_CheckedChanged);
            // 
            // btnDeleteDef
            // 
            this.btnDeleteDef.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnDeleteDef.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnDeleteDef.ImageIndex = 4;
            this.btnDeleteDef.ImageList = this.imageList1;
            this.btnDeleteDef.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnDeleteDef.Location = new System.Drawing.Point(102, 608);
            this.btnDeleteDef.Name = "btnDeleteDef";
            this.btnDeleteDef.Size = new System.Drawing.Size(75, 40);
            this.btnDeleteDef.TabIndex = 42;
            this.btnDeleteDef.Text = "De&lete";
            this.btnDeleteDef.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnDeleteDef.Click += new System.EventHandler(this.btnDeleteDef_Click);
            // 
            // btnAddDef
            // 
            this.btnAddDef.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnAddDef.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnAddDef.ImageIndex = 1;
            this.btnAddDef.ImageList = this.imageList1;
            this.btnAddDef.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnAddDef.Location = new System.Drawing.Point(21, 608);
            this.btnAddDef.Name = "btnAddDef";
            this.btnAddDef.Size = new System.Drawing.Size(75, 40);
            this.btnAddDef.TabIndex = 41;
            this.btnAddDef.Text = "&Add";
            this.btnAddDef.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAddDef.Click += new System.EventHandler(this.btnAddDef_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lblInMin);
            this.panel3.Controls.Add(this.txtMAXT);
            this.panel3.Controls.Add(this.rbTurnaround);
            this.panel3.Controls.Add(this.lblMAXT);
            this.panel3.Controls.Add(this.txtSTSValue);
            this.panel3.Controls.Add(this.txtSTSUrno);
            this.panel3.Controls.Add(this.btnSTS);
            this.panel3.Controls.Add(this.lblSection);
            this.panel3.Controls.Add(this.txtLSTU);
            this.panel3.Controls.Add(this.txtCDAT);
            this.panel3.Controls.Add(this.txtUSEU);
            this.panel3.Controls.Add(this.txtUSEC);
            this.panel3.Controls.Add(this.lblLSTU);
            this.panel3.Controls.Add(this.lblCDAT);
            this.panel3.Controls.Add(this.lblUSEU);
            this.panel3.Controls.Add(this.lblUSEC);
            this.panel3.Controls.Add(this.dtValidTo);
            this.panel3.Controls.Add(this.dtValidFrom);
            this.panel3.Controls.Add(this.lblValidTo);
            this.panel3.Controls.Add(this.lblValidFrom);
            this.panel3.Controls.Add(this.lblAirportGroup);
            this.panel3.Controls.Add(this.lblNatureGroup);
            this.panel3.Controls.Add(this.txtNATValue);
            this.panel3.Controls.Add(this.lblNature);
            this.panel3.Controls.Add(this.txtALTGroupValue);
            this.panel3.Controls.Add(this.lblAirineGroup);
            this.panel3.Controls.Add(this.txtALTValue);
            this.panel3.Controls.Add(this.txtNATGroupUrno);
            this.panel3.Controls.Add(this.btnNATGroup);
            this.panel3.Controls.Add(this.txtNATGroupValue);
            this.panel3.Controls.Add(this.txtAPTUrno);
            this.panel3.Controls.Add(this.txtAPTValue);
            this.panel3.Controls.Add(this.txtNATUrno);
            this.panel3.Controls.Add(this.btnAPT);
            this.panel3.Controls.Add(this.lblAirport);
            this.panel3.Controls.Add(this.txtACTGroupUrno);
            this.panel3.Controls.Add(this.btnNAT);
            this.panel3.Controls.Add(this.txtACTGroupValue);
            this.panel3.Controls.Add(this.txtALTGroupUrno);
            this.panel3.Controls.Add(this.btnACTGroup);
            this.panel3.Controls.Add(this.lblACGroup);
            this.panel3.Controls.Add(this.txtACTUrno);
            this.panel3.Controls.Add(this.btnALTGroup);
            this.panel3.Controls.Add(this.txtACTValue);
            this.panel3.Controls.Add(this.txtALTUrno);
            this.panel3.Controls.Add(this.btnACT);
            this.panel3.Controls.Add(this.lblACType);
            this.panel3.Controls.Add(this.btnALT);
            this.panel3.Controls.Add(this.lblAirline);
            this.panel3.Controls.Add(this.txtRuleName);
            this.panel3.Controls.Add(this.lblRuleName);
            this.panel3.Controls.Add(this.txtAPTGroupUrno);
            this.panel3.Controls.Add(this.txtAPTGroupValue);
            this.panel3.Controls.Add(this.btnAPTGroup);
            this.panel3.Controls.Add(this.rbArrival);
            this.panel3.Controls.Add(this.rbDeparture);
            this.panel3.Controls.Add(this.pictureBox3);
            this.panel3.Location = new System.Drawing.Point(0, 20);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1012, 196);
            this.panel3.TabIndex = 40;
            // 
            // lblInMin
            // 
            this.lblInMin.BackColor = System.Drawing.Color.Transparent;
            this.lblInMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblInMin.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblInMin.Location = new System.Drawing.Point(852, 7);
            this.lblInMin.Name = "lblInMin";
            this.lblInMin.Size = new System.Drawing.Size(100, 16);
            this.lblInMin.TabIndex = 59;
            this.lblInMin.Text = "in Minutes";
            // 
            // txtMAXT
            // 
            this.txtMAXT.Location = new System.Drawing.Point(792, 4);
            this.txtMAXT.MaxLength = 4;
            this.txtMAXT.Name = "txtMAXT";
            this.txtMAXT.Size = new System.Drawing.Size(56, 20);
            this.txtMAXT.TabIndex = 58;
            this.txtMAXT.Tag = "MAXT";
            this.txtMAXT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMAXT.TextChanged += new System.EventHandler(this.HeaderValueChanged);
            this.txtMAXT.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMAXT_KeyDown);
            this.txtMAXT.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMAXT_KeyPress);
            // 
            // rbTurnaround
            // 
            this.rbTurnaround.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbTurnaround.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.rbTurnaround.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rbTurnaround.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rbTurnaround.ImageList = this.imageList1;
            this.rbTurnaround.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.rbTurnaround.Location = new System.Drawing.Point(184, 4);
            this.rbTurnaround.Name = "rbTurnaround";
            this.rbTurnaround.Size = new System.Drawing.Size(72, 28);
            this.rbTurnaround.TabIndex = 57;
            this.rbTurnaround.Text = "T&urnaround";
            this.rbTurnaround.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbTurnaround.CheckedChanged += new System.EventHandler(this.rbArrival_CheckedChanged);
            // 
            // lblMAXT
            // 
            this.lblMAXT.BackColor = System.Drawing.Color.Transparent;
            this.lblMAXT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblMAXT.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblMAXT.Location = new System.Drawing.Point(688, 6);
            this.lblMAXT.Name = "lblMAXT";
            this.lblMAXT.Size = new System.Drawing.Size(100, 16);
            this.lblMAXT.TabIndex = 56;
            this.lblMAXT.Text = "Max. Groundtime:";
            // 
            // txtSTSValue
            // 
            this.txtSTSValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtSTSValue.Location = new System.Drawing.Point(368, 4);
            this.txtSTSValue.Name = "txtSTSValue";
            this.txtSTSValue.ReadOnly = true;
            this.txtSTSValue.Size = new System.Drawing.Size(200, 20);
            this.txtSTSValue.TabIndex = 52;
            this.txtSTSValue.Tag = "UHSS_VAL";
            // 
            // txtSTSUrno
            // 
            this.txtSTSUrno.BackColor = System.Drawing.SystemColors.Window;
            this.txtSTSUrno.Location = new System.Drawing.Point(596, 4);
            this.txtSTSUrno.Name = "txtSTSUrno";
            this.txtSTSUrno.ReadOnly = true;
            this.txtSTSUrno.Size = new System.Drawing.Size(80, 20);
            this.txtSTSUrno.TabIndex = 54;
            this.txtSTSUrno.TabStop = false;
            this.txtSTSUrno.Tag = "UHSS";
            this.txtSTSUrno.Visible = false;
            this.txtSTSUrno.TextChanged += new System.EventHandler(this.HeaderValueChanged);
            // 
            // btnSTS
            // 
            this.btnSTS.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSTS.ImageIndex = 15;
            this.btnSTS.ImageList = this.imageList1;
            this.btnSTS.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnSTS.Location = new System.Drawing.Point(570, 4);
            this.btnSTS.Name = "btnSTS";
            this.btnSTS.Size = new System.Drawing.Size(20, 20);
            this.btnSTS.TabIndex = 53;
            this.btnSTS.Click += new System.EventHandler(this.btnSTS_Click);
            // 
            // lblSection
            // 
            this.lblSection.BackColor = System.Drawing.Color.Transparent;
            this.lblSection.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblSection.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblSection.Location = new System.Drawing.Point(276, 7);
            this.lblSection.Name = "lblSection";
            this.lblSection.Size = new System.Drawing.Size(88, 16);
            this.lblSection.TabIndex = 51;
            this.lblSection.Text = "Status section:";
            // 
            // txtLSTU
            // 
            this.txtLSTU.BackColor = System.Drawing.SystemColors.Window;
            this.txtLSTU.Location = new System.Drawing.Point(784, 136);
            this.txtLSTU.Name = "txtLSTU";
            this.txtLSTU.ReadOnly = true;
            this.txtLSTU.Size = new System.Drawing.Size(116, 20);
            this.txtLSTU.TabIndex = 50;
            this.txtLSTU.Tag = "LSTU";
            this.txtLSTU.TextChanged += new System.EventHandler(this.HeaderValueChanged);
            // 
            // txtCDAT
            // 
            this.txtCDAT.BackColor = System.Drawing.SystemColors.Window;
            this.txtCDAT.Location = new System.Drawing.Point(784, 112);
            this.txtCDAT.Name = "txtCDAT";
            this.txtCDAT.ReadOnly = true;
            this.txtCDAT.Size = new System.Drawing.Size(116, 20);
            this.txtCDAT.TabIndex = 49;
            this.txtCDAT.Tag = "CDAT";
            this.txtCDAT.TextChanged += new System.EventHandler(this.HeaderValueChanged);
            // 
            // txtUSEU
            // 
            this.txtUSEU.BackColor = System.Drawing.SystemColors.Window;
            this.txtUSEU.Location = new System.Drawing.Point(784, 88);
            this.txtUSEU.Name = "txtUSEU";
            this.txtUSEU.ReadOnly = true;
            this.txtUSEU.Size = new System.Drawing.Size(116, 20);
            this.txtUSEU.TabIndex = 48;
            this.txtUSEU.Tag = "USEU";
            this.txtUSEU.TextChanged += new System.EventHandler(this.HeaderValueChanged);
            // 
            // txtUSEC
            // 
            this.txtUSEC.BackColor = System.Drawing.SystemColors.Window;
            this.txtUSEC.Location = new System.Drawing.Point(784, 64);
            this.txtUSEC.Name = "txtUSEC";
            this.txtUSEC.ReadOnly = true;
            this.txtUSEC.Size = new System.Drawing.Size(116, 20);
            this.txtUSEC.TabIndex = 47;
            this.txtUSEC.Tag = "USEC";
            this.txtUSEC.TextChanged += new System.EventHandler(this.HeaderValueChanged);
            // 
            // lblLSTU
            // 
            this.lblLSTU.BackColor = System.Drawing.Color.Transparent;
            this.lblLSTU.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblLSTU.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblLSTU.Location = new System.Drawing.Point(688, 140);
            this.lblLSTU.Name = "lblLSTU";
            this.lblLSTU.Size = new System.Drawing.Size(88, 16);
            this.lblLSTU.TabIndex = 46;
            this.lblLSTU.Text = "Last update:";
            // 
            // lblCDAT
            // 
            this.lblCDAT.BackColor = System.Drawing.Color.Transparent;
            this.lblCDAT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblCDAT.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblCDAT.Location = new System.Drawing.Point(688, 116);
            this.lblCDAT.Name = "lblCDAT";
            this.lblCDAT.Size = new System.Drawing.Size(88, 16);
            this.lblCDAT.TabIndex = 45;
            this.lblCDAT.Text = "Creation date:";
            // 
            // lblUSEU
            // 
            this.lblUSEU.BackColor = System.Drawing.Color.Transparent;
            this.lblUSEU.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblUSEU.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblUSEU.Location = new System.Drawing.Point(688, 92);
            this.lblUSEU.Name = "lblUSEU";
            this.lblUSEU.Size = new System.Drawing.Size(88, 16);
            this.lblUSEU.TabIndex = 44;
            this.lblUSEU.Text = "User (Modified):";
            // 
            // lblUSEC
            // 
            this.lblUSEC.BackColor = System.Drawing.Color.Transparent;
            this.lblUSEC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblUSEC.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblUSEC.Location = new System.Drawing.Point(688, 68);
            this.lblUSEC.Name = "lblUSEC";
            this.lblUSEC.Size = new System.Drawing.Size(88, 16);
            this.lblUSEC.TabIndex = 43;
            this.lblUSEC.Text = "User (Creator):";
            // 
            // dtValidTo
            // 
            this.dtValidTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtValidTo.Location = new System.Drawing.Point(452, 164);
            this.dtValidTo.MinDate = new System.DateTime(2003, 1, 1, 0, 0, 0, 0);
            this.dtValidTo.Name = "dtValidTo";
            this.dtValidTo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dtValidTo.Size = new System.Drawing.Size(140, 20);
            this.dtValidTo.TabIndex = 19;
            this.dtValidTo.Tag = "VATO";
            this.dtValidTo.Value = new System.DateTime(2020, 12, 31, 0, 0, 0, 0);
            this.dtValidTo.ValueChanged += new System.EventHandler(this.dtValidTo_ValueChanged);
            // 
            // dtValidFrom
            // 
            this.dtValidFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtValidFrom.Location = new System.Drawing.Point(124, 164);
            this.dtValidFrom.MinDate = new System.DateTime(2003, 1, 1, 0, 0, 0, 0);
            this.dtValidFrom.Name = "dtValidFrom";
            this.dtValidFrom.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dtValidFrom.Size = new System.Drawing.Size(140, 20);
            this.dtValidFrom.TabIndex = 18;
            this.dtValidFrom.Tag = "VAFR";
            this.dtValidFrom.Value = new System.DateTime(2003, 1, 1, 0, 0, 0, 0);
            this.dtValidFrom.ValueChanged += new System.EventHandler(this.dtValidFrom_ValueChanged);
            // 
            // lblValidTo
            // 
            this.lblValidTo.BackColor = System.Drawing.Color.Transparent;
            this.lblValidTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblValidTo.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblValidTo.Location = new System.Drawing.Point(360, 168);
            this.lblValidTo.Name = "lblValidTo";
            this.lblValidTo.Size = new System.Drawing.Size(56, 16);
            this.lblValidTo.TabIndex = 42;
            this.lblValidTo.Text = "Valid To:";
            // 
            // lblValidFrom
            // 
            this.lblValidFrom.BackColor = System.Drawing.Color.Transparent;
            this.lblValidFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblValidFrom.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblValidFrom.Location = new System.Drawing.Point(32, 168);
            this.lblValidFrom.Name = "lblValidFrom";
            this.lblValidFrom.Size = new System.Drawing.Size(80, 16);
            this.lblValidFrom.TabIndex = 41;
            this.lblValidFrom.Text = "Valid From:";
            // 
            // lblAirportGroup
            // 
            this.lblAirportGroup.BackColor = System.Drawing.Color.Transparent;
            this.lblAirportGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblAirportGroup.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblAirportGroup.Location = new System.Drawing.Point(32, 140);
            this.lblAirportGroup.Name = "lblAirportGroup";
            this.lblAirportGroup.Size = new System.Drawing.Size(88, 16);
            this.lblAirportGroup.TabIndex = 15;
            this.lblAirportGroup.Text = "Airport-Group:";
            // 
            // lblNatureGroup
            // 
            this.lblNatureGroup.BackColor = System.Drawing.Color.Transparent;
            this.lblNatureGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblNatureGroup.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblNatureGroup.Location = new System.Drawing.Point(360, 140);
            this.lblNatureGroup.Name = "lblNatureGroup";
            this.lblNatureGroup.Size = new System.Drawing.Size(88, 16);
            this.lblNatureGroup.TabIndex = 33;
            this.lblNatureGroup.Text = "Nature-Group:";
            // 
            // txtNATValue
            // 
            this.txtNATValue.BackColor = System.Drawing.SystemColors.Window;
            this.txtNATValue.Location = new System.Drawing.Point(452, 112);
            this.txtNATValue.Name = "txtNATValue";
            this.txtNATValue.Size = new System.Drawing.Size(116, 20);
            this.txtNATValue.TabIndex = 14;
            this.txtNATValue.Tag = "NATU_VAL";
            this.txtNATValue.Leave += new System.EventHandler(this.txtNATValue_Leave);
            // 
            // lblNature
            // 
            this.lblNature.BackColor = System.Drawing.Color.Transparent;
            this.lblNature.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblNature.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblNature.Location = new System.Drawing.Point(360, 116);
            this.lblNature.Name = "lblNature";
            this.lblNature.Size = new System.Drawing.Size(88, 16);
            this.lblNature.TabIndex = 29;
            this.lblNature.Text = "Nature:";
            // 
            // txtALTGroupValue
            // 
            this.txtALTGroupValue.BackColor = System.Drawing.SystemColors.Window;
            this.txtALTGroupValue.Location = new System.Drawing.Point(452, 88);
            this.txtALTGroupValue.Name = "txtALTGroupValue";
            this.txtALTGroupValue.ReadOnly = true;
            this.txtALTGroupValue.Size = new System.Drawing.Size(116, 20);
            this.txtALTGroupValue.TabIndex = 12;
            this.txtALTGroupValue.Tag = "ALTG_VAL";
            this.txtALTGroupValue.MouseMove += new System.Windows.Forms.MouseEventHandler(this.txtALTGroupValue_MouseMove);
            // 
            // lblAirineGroup
            // 
            this.lblAirineGroup.BackColor = System.Drawing.Color.Transparent;
            this.lblAirineGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblAirineGroup.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblAirineGroup.Location = new System.Drawing.Point(360, 92);
            this.lblAirineGroup.Name = "lblAirineGroup";
            this.lblAirineGroup.Size = new System.Drawing.Size(88, 16);
            this.lblAirineGroup.TabIndex = 25;
            this.lblAirineGroup.Text = "Airline-Group:";
            // 
            // txtALTValue
            // 
            this.txtALTValue.BackColor = System.Drawing.SystemColors.Window;
            this.txtALTValue.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtALTValue.Location = new System.Drawing.Point(452, 64);
            this.txtALTValue.Name = "txtALTValue";
            this.txtALTValue.Size = new System.Drawing.Size(116, 20);
            this.txtALTValue.TabIndex = 10;
            this.txtALTValue.Tag = "ALTU_VAL";
            this.txtALTValue.Leave += new System.EventHandler(this.txtALTValue_Leave);
            // 
            // txtNATGroupUrno
            // 
            this.txtNATGroupUrno.BackColor = System.Drawing.SystemColors.Window;
            this.txtNATGroupUrno.Location = new System.Drawing.Point(596, 136);
            this.txtNATGroupUrno.Name = "txtNATGroupUrno";
            this.txtNATGroupUrno.ReadOnly = true;
            this.txtNATGroupUrno.Size = new System.Drawing.Size(80, 20);
            this.txtNATGroupUrno.TabIndex = 36;
            this.txtNATGroupUrno.TabStop = false;
            this.txtNATGroupUrno.Tag = "NATG";
            this.txtNATGroupUrno.Visible = false;
            this.txtNATGroupUrno.TextChanged += new System.EventHandler(this.HeaderValueChanged);
            // 
            // btnNATGroup
            // 
            this.btnNATGroup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNATGroup.ImageIndex = 15;
            this.btnNATGroup.ImageList = this.imageList1;
            this.btnNATGroup.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnNATGroup.Location = new System.Drawing.Point(570, 137);
            this.btnNATGroup.Name = "btnNATGroup";
            this.btnNATGroup.Size = new System.Drawing.Size(20, 20);
            this.btnNATGroup.TabIndex = 17;
            this.btnNATGroup.Click += new System.EventHandler(this.btnNATGroup_Click);
            // 
            // txtNATGroupValue
            // 
            this.txtNATGroupValue.BackColor = System.Drawing.SystemColors.Window;
            this.txtNATGroupValue.Location = new System.Drawing.Point(452, 136);
            this.txtNATGroupValue.Name = "txtNATGroupValue";
            this.txtNATGroupValue.ReadOnly = true;
            this.txtNATGroupValue.Size = new System.Drawing.Size(116, 20);
            this.txtNATGroupValue.TabIndex = 16;
            this.txtNATGroupValue.Tag = "NATG_VAL";
            this.txtNATGroupValue.MouseMove += new System.Windows.Forms.MouseEventHandler(this.txtNATGroupValue_MouseMove);
            // 
            // txtAPTUrno
            // 
            this.txtAPTUrno.BackColor = System.Drawing.SystemColors.Window;
            this.txtAPTUrno.Location = new System.Drawing.Point(268, 112);
            this.txtAPTUrno.Name = "txtAPTUrno";
            this.txtAPTUrno.ReadOnly = true;
            this.txtAPTUrno.Size = new System.Drawing.Size(80, 20);
            this.txtAPTUrno.TabIndex = 14;
            this.txtAPTUrno.TabStop = false;
            this.txtAPTUrno.Tag = "APTU";
            this.txtAPTUrno.Visible = false;
            this.txtAPTUrno.TextChanged += new System.EventHandler(this.HeaderValueChanged);
            // 
            // txtAPTValue
            // 
            this.txtAPTValue.BackColor = System.Drawing.SystemColors.Window;
            this.txtAPTValue.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAPTValue.Location = new System.Drawing.Point(124, 112);
            this.txtAPTValue.Name = "txtAPTValue";
            this.txtAPTValue.Size = new System.Drawing.Size(116, 20);
            this.txtAPTValue.TabIndex = 6;
            this.txtAPTValue.Tag = "APTU_VAL";
            this.txtAPTValue.Leave += new System.EventHandler(this.txtAPTValue_Leave);
            // 
            // txtNATUrno
            // 
            this.txtNATUrno.BackColor = System.Drawing.SystemColors.Window;
            this.txtNATUrno.Location = new System.Drawing.Point(596, 112);
            this.txtNATUrno.Name = "txtNATUrno";
            this.txtNATUrno.ReadOnly = true;
            this.txtNATUrno.Size = new System.Drawing.Size(80, 20);
            this.txtNATUrno.TabIndex = 32;
            this.txtNATUrno.TabStop = false;
            this.txtNATUrno.Tag = "NATU";
            this.txtNATUrno.Visible = false;
            this.txtNATUrno.TextChanged += new System.EventHandler(this.HeaderValueChanged);
            // 
            // btnAPT
            // 
            this.btnAPT.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAPT.ImageIndex = 15;
            this.btnAPT.ImageList = this.imageList1;
            this.btnAPT.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnAPT.Location = new System.Drawing.Point(242, 113);
            this.btnAPT.Name = "btnAPT";
            this.btnAPT.Size = new System.Drawing.Size(20, 20);
            this.btnAPT.TabIndex = 7;
            this.btnAPT.Click += new System.EventHandler(this.btnAPT_Click);
            // 
            // lblAirport
            // 
            this.lblAirport.BackColor = System.Drawing.Color.Transparent;
            this.lblAirport.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblAirport.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblAirport.Location = new System.Drawing.Point(32, 116);
            this.lblAirport.Name = "lblAirport";
            this.lblAirport.Size = new System.Drawing.Size(88, 16);
            this.lblAirport.TabIndex = 11;
            this.lblAirport.Text = "Airport:";
            // 
            // txtACTGroupUrno
            // 
            this.txtACTGroupUrno.BackColor = System.Drawing.SystemColors.Window;
            this.txtACTGroupUrno.Location = new System.Drawing.Point(268, 88);
            this.txtACTGroupUrno.Name = "txtACTGroupUrno";
            this.txtACTGroupUrno.ReadOnly = true;
            this.txtACTGroupUrno.Size = new System.Drawing.Size(80, 20);
            this.txtACTGroupUrno.TabIndex = 10;
            this.txtACTGroupUrno.TabStop = false;
            this.txtACTGroupUrno.Tag = "ACTG";
            this.txtACTGroupUrno.Visible = false;
            this.txtACTGroupUrno.TextChanged += new System.EventHandler(this.HeaderValueChanged);
            // 
            // btnNAT
            // 
            this.btnNAT.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNAT.ImageIndex = 15;
            this.btnNAT.ImageList = this.imageList1;
            this.btnNAT.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnNAT.Location = new System.Drawing.Point(570, 113);
            this.btnNAT.Name = "btnNAT";
            this.btnNAT.Size = new System.Drawing.Size(20, 20);
            this.btnNAT.TabIndex = 15;
            this.btnNAT.Click += new System.EventHandler(this.btnNAT_Click);
            // 
            // txtACTGroupValue
            // 
            this.txtACTGroupValue.BackColor = System.Drawing.SystemColors.Window;
            this.txtACTGroupValue.Location = new System.Drawing.Point(124, 88);
            this.txtACTGroupValue.Name = "txtACTGroupValue";
            this.txtACTGroupValue.ReadOnly = true;
            this.txtACTGroupValue.Size = new System.Drawing.Size(116, 20);
            this.txtACTGroupValue.TabIndex = 4;
            this.txtACTGroupValue.Tag = "ACTG_VAL";
            this.txtACTGroupValue.MouseMove += new System.Windows.Forms.MouseEventHandler(this.txtACTGroupValue_MouseMove);
            this.txtACTGroupValue.MouseHover += new System.EventHandler(this.txtACTGroupValue_MouseHover);
            // 
            // txtALTGroupUrno
            // 
            this.txtALTGroupUrno.BackColor = System.Drawing.SystemColors.Window;
            this.txtALTGroupUrno.Location = new System.Drawing.Point(596, 88);
            this.txtALTGroupUrno.Name = "txtALTGroupUrno";
            this.txtALTGroupUrno.ReadOnly = true;
            this.txtALTGroupUrno.Size = new System.Drawing.Size(80, 20);
            this.txtALTGroupUrno.TabIndex = 28;
            this.txtALTGroupUrno.TabStop = false;
            this.txtALTGroupUrno.Tag = "ALTG";
            this.txtALTGroupUrno.Visible = false;
            this.txtALTGroupUrno.TextChanged += new System.EventHandler(this.HeaderValueChanged);
            // 
            // btnACTGroup
            // 
            this.btnACTGroup.BackColor = System.Drawing.SystemColors.Control;
            this.btnACTGroup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnACTGroup.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnACTGroup.ImageIndex = 15;
            this.btnACTGroup.ImageList = this.imageList1;
            this.btnACTGroup.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnACTGroup.Location = new System.Drawing.Point(242, 89);
            this.btnACTGroup.Name = "btnACTGroup";
            this.btnACTGroup.Size = new System.Drawing.Size(20, 20);
            this.btnACTGroup.TabIndex = 5;
            this.btnACTGroup.UseVisualStyleBackColor = false;
            this.btnACTGroup.Click += new System.EventHandler(this.btnACTGroup_Click);
            // 
            // lblACGroup
            // 
            this.lblACGroup.BackColor = System.Drawing.Color.Transparent;
            this.lblACGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblACGroup.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblACGroup.Location = new System.Drawing.Point(32, 92);
            this.lblACGroup.Name = "lblACGroup";
            this.lblACGroup.Size = new System.Drawing.Size(88, 16);
            this.lblACGroup.TabIndex = 7;
            this.lblACGroup.Text = "A/C-Group:";
            // 
            // txtACTUrno
            // 
            this.txtACTUrno.BackColor = System.Drawing.SystemColors.Window;
            this.txtACTUrno.Location = new System.Drawing.Point(268, 64);
            this.txtACTUrno.Name = "txtACTUrno";
            this.txtACTUrno.ReadOnly = true;
            this.txtACTUrno.Size = new System.Drawing.Size(80, 20);
            this.txtACTUrno.TabIndex = 6;
            this.txtACTUrno.TabStop = false;
            this.txtACTUrno.Tag = "ACTU";
            this.txtACTUrno.Visible = false;
            this.txtACTUrno.TextChanged += new System.EventHandler(this.HeaderValueChanged);
            // 
            // btnALTGroup
            // 
            this.btnALTGroup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnALTGroup.ImageIndex = 15;
            this.btnALTGroup.ImageList = this.imageList1;
            this.btnALTGroup.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnALTGroup.Location = new System.Drawing.Point(570, 89);
            this.btnALTGroup.Name = "btnALTGroup";
            this.btnALTGroup.Size = new System.Drawing.Size(20, 20);
            this.btnALTGroup.TabIndex = 13;
            this.btnALTGroup.Click += new System.EventHandler(this.btnALTGroup_Click);
            // 
            // txtACTValue
            // 
            this.txtACTValue.BackColor = System.Drawing.SystemColors.Window;
            this.txtACTValue.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtACTValue.Location = new System.Drawing.Point(124, 64);
            this.txtACTValue.Name = "txtACTValue";
            this.txtACTValue.Size = new System.Drawing.Size(116, 20);
            this.txtACTValue.TabIndex = 2;
            this.txtACTValue.Tag = "ACTU_VAL";
            this.txtACTValue.Leave += new System.EventHandler(this.txtACTValue_Leave);
            // 
            // txtALTUrno
            // 
            this.txtALTUrno.BackColor = System.Drawing.SystemColors.Window;
            this.txtALTUrno.Location = new System.Drawing.Point(596, 64);
            this.txtALTUrno.Name = "txtALTUrno";
            this.txtALTUrno.ReadOnly = true;
            this.txtALTUrno.Size = new System.Drawing.Size(80, 20);
            this.txtALTUrno.TabIndex = 24;
            this.txtALTUrno.TabStop = false;
            this.txtALTUrno.Tag = "ALTU";
            this.txtALTUrno.Visible = false;
            this.txtALTUrno.TextChanged += new System.EventHandler(this.HeaderValueChanged);
            // 
            // btnACT
            // 
            this.btnACT.BackColor = System.Drawing.SystemColors.Control;
            this.btnACT.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnACT.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnACT.ImageIndex = 15;
            this.btnACT.ImageList = this.imageList1;
            this.btnACT.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnACT.Location = new System.Drawing.Point(242, 65);
            this.btnACT.Name = "btnACT";
            this.btnACT.Size = new System.Drawing.Size(20, 20);
            this.btnACT.TabIndex = 3;
            this.btnACT.UseVisualStyleBackColor = false;
            this.btnACT.Click += new System.EventHandler(this.btnACT_Click);
            // 
            // lblACType
            // 
            this.lblACType.BackColor = System.Drawing.Color.Transparent;
            this.lblACType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblACType.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblACType.Location = new System.Drawing.Point(32, 68);
            this.lblACType.Name = "lblACType";
            this.lblACType.Size = new System.Drawing.Size(88, 16);
            this.lblACType.TabIndex = 3;
            this.lblACType.Text = "A/C-Type:";
            // 
            // btnALT
            // 
            this.btnALT.BackColor = System.Drawing.SystemColors.Control;
            this.btnALT.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnALT.ImageIndex = 15;
            this.btnALT.ImageList = this.imageList1;
            this.btnALT.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnALT.Location = new System.Drawing.Point(570, 65);
            this.btnALT.Name = "btnALT";
            this.btnALT.Size = new System.Drawing.Size(20, 20);
            this.btnALT.TabIndex = 11;
            this.btnALT.UseVisualStyleBackColor = false;
            this.btnALT.Click += new System.EventHandler(this.btnALT_Click);
            // 
            // lblAirline
            // 
            this.lblAirline.BackColor = System.Drawing.Color.Transparent;
            this.lblAirline.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblAirline.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblAirline.Location = new System.Drawing.Point(360, 68);
            this.lblAirline.Name = "lblAirline";
            this.lblAirline.Size = new System.Drawing.Size(88, 16);
            this.lblAirline.TabIndex = 21;
            this.lblAirline.Text = "Airline:";
            // 
            // txtRuleName
            // 
            this.txtRuleName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtRuleName.Location = new System.Drawing.Point(124, 40);
            this.txtRuleName.MaxLength = 32;
            this.txtRuleName.Name = "txtRuleName";
            this.txtRuleName.Size = new System.Drawing.Size(444, 20);
            this.txtRuleName.TabIndex = 0;
            this.txtRuleName.Tag = "NAME";
            this.txtRuleName.TextChanged += new System.EventHandler(this.HeaderValueChanged);
            // 
            // lblRuleName
            // 
            this.lblRuleName.BackColor = System.Drawing.Color.Transparent;
            this.lblRuleName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblRuleName.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblRuleName.Location = new System.Drawing.Point(32, 44);
            this.lblRuleName.Name = "lblRuleName";
            this.lblRuleName.Size = new System.Drawing.Size(88, 16);
            this.lblRuleName.TabIndex = 19;
            this.lblRuleName.Text = "Rule Name:";
            // 
            // txtAPTGroupUrno
            // 
            this.txtAPTGroupUrno.BackColor = System.Drawing.SystemColors.Window;
            this.txtAPTGroupUrno.Location = new System.Drawing.Point(268, 136);
            this.txtAPTGroupUrno.Name = "txtAPTGroupUrno";
            this.txtAPTGroupUrno.ReadOnly = true;
            this.txtAPTGroupUrno.Size = new System.Drawing.Size(80, 20);
            this.txtAPTGroupUrno.TabIndex = 18;
            this.txtAPTGroupUrno.TabStop = false;
            this.txtAPTGroupUrno.Tag = "APTG";
            this.txtAPTGroupUrno.Visible = false;
            this.txtAPTGroupUrno.TextChanged += new System.EventHandler(this.HeaderValueChanged);
            // 
            // txtAPTGroupValue
            // 
            this.txtAPTGroupValue.BackColor = System.Drawing.SystemColors.Window;
            this.txtAPTGroupValue.Location = new System.Drawing.Point(124, 136);
            this.txtAPTGroupValue.Name = "txtAPTGroupValue";
            this.txtAPTGroupValue.ReadOnly = true;
            this.txtAPTGroupValue.Size = new System.Drawing.Size(116, 20);
            this.txtAPTGroupValue.TabIndex = 8;
            this.txtAPTGroupValue.Tag = "APTG_VAL";
            this.txtAPTGroupValue.MouseMove += new System.Windows.Forms.MouseEventHandler(this.txtAPTGroupValue_MouseMove);
            this.txtAPTGroupValue.MouseHover += new System.EventHandler(this.txtAPTGroupValue_MouseHover);
            // 
            // btnAPTGroup
            // 
            this.btnAPTGroup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAPTGroup.ImageIndex = 15;
            this.btnAPTGroup.ImageList = this.imageList1;
            this.btnAPTGroup.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnAPTGroup.Location = new System.Drawing.Point(242, 137);
            this.btnAPTGroup.Name = "btnAPTGroup";
            this.btnAPTGroup.Size = new System.Drawing.Size(20, 20);
            this.btnAPTGroup.TabIndex = 9;
            this.btnAPTGroup.Click += new System.EventHandler(this.btnAPTGroup_Click);
            // 
            // rbArrival
            // 
            this.rbArrival.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbArrival.BackColor = System.Drawing.Color.LightGreen;
            this.rbArrival.Checked = true;
            this.rbArrival.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.rbArrival.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rbArrival.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rbArrival.ImageIndex = 7;
            this.rbArrival.ImageList = this.imageList1;
            this.rbArrival.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.rbArrival.Location = new System.Drawing.Point(32, 4);
            this.rbArrival.Name = "rbArrival";
            this.rbArrival.Size = new System.Drawing.Size(68, 28);
            this.rbArrival.TabIndex = 0;
            this.rbArrival.TabStop = true;
            this.rbArrival.Text = "Arri&val";
            this.rbArrival.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rbArrival.UseVisualStyleBackColor = false;
            this.rbArrival.CheckedChanged += new System.EventHandler(this.rbArrival_CheckedChanged);
            // 
            // rbDeparture
            // 
            this.rbDeparture.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbDeparture.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.rbDeparture.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rbDeparture.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rbDeparture.ImageIndex = 8;
            this.rbDeparture.ImageList = this.imageList1;
            this.rbDeparture.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.rbDeparture.Location = new System.Drawing.Point(104, 4);
            this.rbDeparture.Name = "rbDeparture";
            this.rbDeparture.Size = new System.Drawing.Size(76, 28);
            this.rbDeparture.TabIndex = 1;
            this.rbDeparture.Text = "De&parture";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBox3.Location = new System.Drawing.Point(0, 0);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(1012, 196);
            this.pictureBox3.TabIndex = 39;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox3_Paint);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Black;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Lime;
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(0, 216);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1012, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Status definition";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEvent
            // 
            this.lblEvent.BackColor = System.Drawing.Color.Black;
            this.lblEvent.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblEvent.ForeColor = System.Drawing.Color.Lime;
            this.lblEvent.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblEvent.Location = new System.Drawing.Point(0, 0);
            this.lblEvent.Name = "lblEvent";
            this.lblEvent.Size = new System.Drawing.Size(1012, 18);
            this.lblEvent.TabIndex = 1;
            this.lblEvent.Text = "Flight-Event";
            this.lblEvent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabDefs
            // 
            this.tabDefs.Location = new System.Drawing.Point(7, 247);
            this.tabDefs.Name = "tabDefs";
            this.tabDefs.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabDefs.OcxState")));
            this.tabDefs.RightToLeft = false;
            this.tabDefs.Size = new System.Drawing.Size(1143, 355);
            this.tabDefs.TabIndex = 18;
            this.tabDefs.TabStop = false;
            this.tabDefs.RowSelectionChanged += new AxTABLib._DTABEvents_RowSelectionChangedEventHandler(this.tabDefs_RowSelectionChanged);
            this.tabDefs.CloseInplaceEdit += new AxTABLib._DTABEvents_CloseInplaceEditEventHandler(this.tabDefs_CloseInplaceEdit);
            this.tabDefs.ComboSelChanged += new AxTABLib._DTABEvents_ComboSelChangedEventHandler(this.tabDefs_ComboSelChanged);
            this.tabDefs.EditPositionChanged += new AxTABLib._DTABEvents_EditPositionChangedEventHandler(this.tabDefs_EditPositionChanged);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBox2.Location = new System.Drawing.Point(0, 334);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(1028, 372);
            this.pictureBox2.TabIndex = 38;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox2_Paint);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(808, 36);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(92, 20);
            this.numericUpDown1.TabIndex = 0;
            // 
            // mnuContextDetail
            // 
            this.mnuContextDetail.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuReactivate});
            // 
            // mnuReactivate
            // 
            this.mnuReactivate.Index = 0;
            this.mnuReactivate.Text = "&Reactivate Status";
            // 
            // frmRules
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(1028, 746);
            this.Controls.Add(this.panelHeader);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MinimizeBox = false;
            this.Name = "frmRules";
            this.Text = "StatusMgr.: Rules ...";
            this.Load += new System.EventHandler(this.frmRules_Load);
            this.Closed += new System.EventHandler(this.frmRules_Closed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.frmRules_Closing);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmRules_KeyPress);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panelHeader.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabDefs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private void pictureBox1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, pictureBox1, Color.WhiteSmoke, Color.Gray);			
		}

		private void btnACT_Click(object sender, System.EventArgs e)
		{
			System.Windows.Forms.DialogResult olResult;
			frmChoiceList olDlg = new frmChoiceList();
			olDlg.currTable = "ACT";
			olDlg.lblCaption.Text = "Aircrafts";
			olResult = olDlg.ShowDialog();
			if(olResult == DialogResult.OK)
			{
				int sel = olDlg.tabList.GetCurrentSelected();
				string strUrno = olDlg.tabList.GetColumnValue(sel,0);
				txtACTUrno.Text = strUrno;
				string val1 = olDlg.tabList.GetColumnValue(sel,1);
				string val2 = olDlg.tabList.GetColumnValue(sel,2);
				string strValue = "";
				if(val1 == "" || val2 == "")
					strValue = val1 + val2;
				else
					strValue = val1 + "/" + val2;
				txtACTValue.Text = strValue;
				txtACTGroupUrno.Text = "";
				txtACTGroupValue.Text = "";
				txtACTValue.ForeColor = Color.Black;
			}
		}

		private void btnAPT_Click(object sender, System.EventArgs e)
		{
			System.Windows.Forms.DialogResult olResult;
			frmChoiceList olDlg = new frmChoiceList();
			olDlg.currTable = "APT";
			olDlg.lblCaption.Text = "Airports";
			olResult = olDlg.ShowDialog();
			if(olResult == DialogResult.OK)
			{
				int sel = olDlg.tabList.GetCurrentSelected();
				string strUrno = olDlg.tabList.GetColumnValue(sel,0);
				txtAPTUrno.Text = strUrno;
				string val1 = olDlg.tabList.GetColumnValue(sel,1);
				string val2 = olDlg.tabList.GetColumnValue(sel,2);
				string strValue = "";
				if(val1 == "" || val2 == "")
					strValue = val1 + val2;
				else
					strValue = val1 + "/" + val2;
				txtAPTValue.Text = strValue;
				txtAPTGroupValue.Text = "";
				txtAPTGroupUrno.Text = "";
				txtAPTValue.ForeColor = Color.Black;
			}
		}

		private void btnALT_Click(object sender, System.EventArgs e)
		{
			System.Windows.Forms.DialogResult olResult;
			frmChoiceList olDlg = new frmChoiceList();
			olDlg.lblCaption.Text = "Airlines";
			olDlg.currTable = "ALT";
			olResult = olDlg.ShowDialog();
			if(olResult == DialogResult.OK)
			{
				int sel = olDlg.tabList.GetCurrentSelected();
				string strUrno = olDlg.tabList.GetColumnValue(sel,0);
				txtALTUrno.Text = strUrno;
				string val1 = olDlg.tabList.GetColumnValue(sel,1);
				string val2 = olDlg.tabList.GetColumnValue(sel,2);
				string strValue = "";
				if(val1 == "" || val2 == "")
					strValue = val1 + val2;
				else
					strValue = val1 + "/" + val2;
				txtALTValue.Text = strValue;
				txtALTGroupValue.Text = "";
				txtALTGroupUrno.Text = "";
				txtALTValue.ForeColor = Color.Black;
			}
		}

		private void btnNAT_Click(object sender, System.EventArgs e)
		{
			System.Windows.Forms.DialogResult olResult;
			frmChoiceList olDlg = new frmChoiceList();
			olDlg.currTable = "NAT";
			olDlg.lblCaption.Text = "Natures";
			olResult = olDlg.ShowDialog();
			if(olResult == DialogResult.OK)
			{
				int sel = olDlg.tabList.GetCurrentSelected();
				string strUrno = olDlg.tabList.GetColumnValue(sel,0);
				txtNATUrno.Text = strUrno;
				txtNATValue.Text = olDlg.tabList.GetColumnValue(sel,2);
				txtNATGroupValue.Text = "";
				txtNATGroupUrno.Text = "";
				txtNATValue.ForeColor = Color.Black;
			}
		}

		private void btnACTGroup_Click(object sender, System.EventArgs e)
		{
			System.Windows.Forms.DialogResult olResult;
			frmChoiceList olDlg = new frmChoiceList();
			olDlg.isGroup = true;
			olDlg.lblCaption.Text = "Aircraft-Groups";
			olDlg.currTable = "ACT";
			olResult = olDlg.ShowDialog();
			if(olResult == DialogResult.OK)
			{
				int sel = olDlg.tabList.GetCurrentSelected();
				string strUrno = olDlg.tabList.GetColumnValue(sel,0);
				txtACTGroupUrno.Text = strUrno;
				txtACTGroupValue.Text = olDlg.tabList.GetColumnValue(sel,1);
				txtACTValue.Text = "";
				txtACTUrno.Text = "";
				txtACTValue.ForeColor = Color.Black;
			}
		}

		private void btnAPTGroup_Click(object sender, System.EventArgs e)
		{
			System.Windows.Forms.DialogResult olResult;
			frmChoiceList olDlg = new frmChoiceList();
			olDlg.isGroup = true;
			olDlg.lblCaption.Text = "Airport-Groups";
			olDlg.currTable = "APT";
			olResult = olDlg.ShowDialog();
			if(olResult == DialogResult.OK)
			{
				int sel = olDlg.tabList.GetCurrentSelected();
				string strUrno = olDlg.tabList.GetColumnValue(sel,0);
				txtAPTGroupUrno.Text = strUrno;
				txtAPTGroupValue.Text = olDlg.tabList.GetColumnValue(sel,1);
				txtAPTValue.Text = "";
				txtAPTUrno.Text = "";
				txtAPTValue.ForeColor = Color.Black;
			}
		}

		private void btnALTGroup_Click(object sender, System.EventArgs e)
		{
			System.Windows.Forms.DialogResult olResult;
			frmChoiceList olDlg = new frmChoiceList();
			olDlg.isGroup = true;
			olDlg.lblCaption.Text = "Airline-Groups";
			olDlg.currTable = "ALT";
			olResult = olDlg.ShowDialog();
			if(olResult == DialogResult.OK)
			{
				int sel = olDlg.tabList.GetCurrentSelected();
				string strUrno = olDlg.tabList.GetColumnValue(sel,0);
				txtALTGroupUrno.Text = strUrno;
				txtALTGroupValue.Text = olDlg.tabList.GetColumnValue(sel,1);
				txtALTValue.Text = "";
				txtALTUrno.Text = "";
				txtALTValue.ForeColor = Color.Black;
			}
		}

		private void btnNATGroup_Click(object sender, System.EventArgs e)
		{
			System.Windows.Forms.DialogResult olResult;
			frmChoiceList olDlg = new frmChoiceList();
			olDlg.isGroup = true;
			olDlg.currTable = "NAT";
			olDlg.lblCaption.Text = "Nature-Groups";
			olResult = olDlg.ShowDialog();
			if(olResult == DialogResult.OK)
			{
				int sel = olDlg.tabList.GetCurrentSelected();
				string strUrno = olDlg.tabList.GetColumnValue(sel,0);
				txtNATGroupUrno.Text = strUrno;
				txtNATGroupValue.Text = olDlg.tabList.GetColumnValue(sel,1);
				txtNATValue.Text = "";
				txtNATUrno.Text = "";
				txtNATValue.ForeColor = Color.Black;
			}
		}
		private void btnSTS_Click(object sender, System.EventArgs e)
		{
			System.Windows.Forms.DialogResult olResult;
			frmChoiceList olDlg = new frmChoiceList();
			olDlg.lblCaption.Text = "Status Sections";
			olDlg.currTable = "HSS";
			olResult = olDlg.ShowDialog();
			if(olResult == DialogResult.OK)
			{
				int sel = olDlg.tabList.GetCurrentSelected();
				string strUrno = olDlg.tabList.GetColumnValue(sel,0);
				txtSTSUrno.Text = strUrno;
				txtSTSValue.Text = olDlg.tabList.GetColumnValue(sel,1);
			}
		}


		private void frmRules_Load(object sender, System.EventArgs e)
		{
			
			//init the configuration for dynamical accessing the fields and values
			string strURNOFields  = "UHSS,ACTU,ACTG,APTU,APTG,ALTU,ALTG,NATU,NATG";
			string strVALUEFields = "UHSS_VAL,ACTU_VAL,ACTG_VAL,APTU_VAL,APTG_VAL,ALTU_VAL,ALTG_VAL,NATU_VAL,NATG_VAL";
			string strLookupTabs  = "HSS,ACT,GRN,APT,GRN,ALT,GRN,NAT,GRN";
			string strLookupFields= "NAME,ACT3|ACT5,GRPN,APC3|APC4,GRPN,ALC2|ALC3,GRPN,TNAM,GRPN";
			Ufis.Utils.IniFile ini = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			string strRETY_DB  = ini.IniReadValue("STATUSMANAGER", "RETY_DB");
			string strRETY_GUI = ini.IniReadValue("STATUSMANAGER", "RETY_GUI");
			string strRTIM_DB  = ini.IniReadValue("STATUSMANAGER", "RTIM_DB");
			string strRTIM_GUI = ini.IniReadValue("STATUSMANAGER", "RTIM_GUI");
			string strATFF_DB_ARR  = ini.IniReadValue("STATUSMANAGER", "AFTF_DB_ARRIVAL");
			string strATFF_GUI_ARR  = ini.IniReadValue("STATUSMANAGER", "AFTF_GUI_ARRIVAL");
			string strATFF_DB_DEP  = ini.IniReadValue("STATUSMANAGER", "AFTF_DB_DEPARTURE");
			string strATFF_GUI_DEP  = ini.IniReadValue("STATUSMANAGER", "AFTF_GUI_DEPARTURE");
			myRETY_DB = strRETY_DB.Split(',');
			myRETY_GUI = strRETY_GUI.Split(',');
			myRTIM_DB = strRTIM_DB.Split(',');
			myRTIM_GUI = strRTIM_GUI.Split(',');
			myAFTF_DB_ARR = strATFF_DB_ARR .Split(',');
			myAFTF_GUI_ARR = strATFF_GUI_ARR.Split(',');
			myAFTF_DB_DEP = strATFF_DB_DEP.Split(',');
			myAFTF_GUI_DEP = strATFF_GUI_DEP.Split(',');
			//Check the ini entries
			string strMessage = "";
			strMessage = CheckCedaIniEntries();
			if(strMessage != "")
			{
				//for the case of inconsistent ini entries to no run the
				//rules editor
				strMessage += "Rule-Editor cannot be used!";
				MessageBox.Show(this.Owner, strMessage, "ERROR!!!!!!!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                //AM 20080422 - Use 'this.Owner' instead of 'this' - To show the message in Owner Screen
				this.Close();
				return;
			}

			myDB = UT.GetMemDB();


			myURNOFields   = strURNOFields.Split(',');  
			myVALUEFields  = strVALUEFields.Split(',');
			myLookupTabs   = strLookupTabs.Split(',');
			myLookupFields =strLookupFields.Split(',');

			btnSave.Parent = pictureBox1;
			btnRecalcFlights.Parent = pictureBox1;
			btnOpen.Parent = pictureBox1;
			btnNew.Parent = pictureBox1;
			btnDeleteHeader.Parent = pictureBox1;
			btnStatusSection.Parent = pictureBox1;
			btnCopy.Parent = pictureBox1;
			btnClose.Parent = pictureBox1;
			lblACType.Parent = pictureBox3;
			lblAirline.Parent = pictureBox3;
			lblAirport.Parent = pictureBox3;
			lblAirportGroup.Parent = pictureBox3;
			lblNature.Parent = pictureBox3;
			lblNatureGroup.Parent = pictureBox3;
			lblRuleName.Parent = pictureBox3;
			lblACGroup.Parent = pictureBox3;
			lblAirineGroup.Parent = pictureBox3;
			lblValidFrom.Parent = pictureBox3;
			lblValidTo.Parent = pictureBox3;
			lblCDAT.Parent = pictureBox3;
			lblLSTU.Parent = pictureBox3;
			lblUSEC.Parent = pictureBox3;
			lblUSEU.Parent = pictureBox3;
			lblSection.Parent = pictureBox3;
			btnACT.Parent = pictureBox3;
			btnACTGroup.Parent = pictureBox3;
			txtRuleName.Parent = pictureBox3;
			rbArrival.Parent = pictureBox3;
			rbDeparture.Parent = pictureBox3;
			rbTurnaround.Parent = pictureBox3;
			lblMAXT.Parent = pictureBox3;
			lblInMin.Parent = pictureBox3;


			DateTime olFrom;
			DateTime olTo;
			olFrom = DateTime.Now;
			olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 0,0,0,0);
			olTo = new DateTime(2030, 12, 31, 23,59,59,0);
			dtValidFrom.CustomFormat = "dd.MM.yyyy - HH:mm";
			dtValidTo.CustomFormat = "dd.MM.yyyy - HH:mm";
			oldValidFrom = olFrom;
			oldValidTo = olTo;

//URNO,USRH,NAME,RETY,SDEU,AFTF,CCAF,UTPL,USRV,RTIM,FLDR,TIFR
//URNO	
//USRH	Urno auf SRHTAB
//NAME	Definitionsname
//RETY	Referenztyp(JOBx,AFT,CCA,OHNE)
//SDEU	Urno auf vorher definierten SDETAB record
//AFTF	Feldname, wenn RETY=AFT
//CCAF	Feldname, wenn RETY=CCA
//UTPL	Urno auf Template, wenn RETY = JOBx
//USRV	Evtl. URNO des Service, wenn ben�tigt
//RTIM	Referenztyp (AFT oder STAT)
//FLDR	Referenzfeld (STOA,ETAI,LAND,ONBL,STOD,ETDI,OFBL)
//TIFR	Referenz Zeitrahmen
//DELF  Deleted
//TSKN  Task Name (Task Name coming from other interfaces) 
//ALRT  Alert Icon Name (Alert Icon Name to show when one of the task has conflict)
           
			tabDefs.ResetContent();
            string stHeader = "URNO,USRH,Name,RETY,Type,AFTF,Flight,UTPL,RMS Rule-Template,Ref.-Time,SDEU,Predecessor,USRV,RTIM,RTIM_C,Time(min.),DELF";
            string stLogicalFieldList = "URNO,USRH,NAME,RETY,RETY_C,AFTF,AFTF_C,UTPL,TNAM,FLDR,SDEU,SDEU_C,USRV,RTIM,RTIM_C,TIFR,DELF";//tabDefs.HeaderString;
            string stHeaderLength = "0,0,180,0,80,0,100,0,180,60,0,180,0,0,0,80,0";
            string stColumnWidth = "10,10,32,10,10,10,10,10,32,10,64,10,10,10,10,10,1"; 

            if (frmData.HasInterfaceUpdate)
            {
                stHeader += ",Interface Task Name";
                stLogicalFieldList += ",TSKN";
                stHeaderLength += ",180";
                stColumnWidth += ",50";

                stHeader += ",ALRT";
                stLogicalFieldList += ",ALRT";
                stHeaderLength += ",0";
                stColumnWidth += ",2";

                stHeader += ",Alert";
                stLogicalFieldList += ",ALRT_C";
                stHeaderLength += ",100";
                stColumnWidth += ",20";
            }

            tabDefs.HeaderString = stHeader;
            tabDefs.LogicalFieldList = stLogicalFieldList;
            tabDefs.HeaderLengthString = stHeaderLength;
            tabDefs.ColumnWidthString = stColumnWidth;

			tabDefs.LifeStyle = true;
			tabDefs.LineHeight = 20;
			int hash = txtACTGroupUrno.GetHashCode();
			hash = txtACTGroupValue.GetHashCode();
			tabDefs.FontName = "Arial";
			//tabDefs.FontName = "Courier New";
			tabDefs.FontSize = 14;
			tabDefs.HeaderFontSize = 14;
			tabDefs.SetTabFontBold(true);
			tabDefs.ShowHorzScroller(true);
			string strColor = UT.colBlue + "," + UT.colBlue;
			tabDefs.CursorDecoration(tabDefs.LogicalFieldList, "B,T", "2,2", strColor);
			tabDefs.DefaultCursor = false;
			tabDefs.InplaceEditUpperCase = false;
			// 0    1    2     3    4      5    6     7     8    9   10    11    12   13   14     15   16   17   18   19
            //URNO,USRH,NAME,RETY,RETY_C,AFTF,AFTF_C,UTPL,TNAM,FLDR,SDEU,SDEU_C,USRV,RTIM,RTIM_C,TIFR,DELF,TSKN,ALRT,ALRT_C
			tabDefs.NoFocusColumns = "0,1,3,4,5,6,7,8,9,10,11,12,13,14,16,18";

			NewRule();
			MakeTabCombos();
            MakeAlertCombo();
			tabDefs.EnableInlineEdit(true);
			ReadRegistry();
		}
		private void ReadRegistry()
		{
			int myX=-1, myY=-1, myW=-1, myH=-1;
			string regVal = "";
			string theKey = "Software\\StatusManager\\RuleEditor";
			RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			if(rk != null)
			{
				regVal = rk.GetValue("X", "-1").ToString();
				myX = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Y", "-1").ToString();
				myY = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Width", "-1").ToString();
				myW = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Height", "-1").ToString();
				myH = Convert.ToInt32(regVal);
			}
			if((myW != -1 && myH != -1) && (myX < Screen.PrimaryScreen.Bounds.Width))
			{
				this.Left = myX;
				this.Top = myY;
				this.Width = myW;
				this.Height = myH;
			}
		}

		private string CheckCedaIniEntries()
		{
			string strMessage="";
			if(myRETY_DB.Length != myRETY_GUI.Length)
			{
				strMessage += "Ceda.ini [STATUSMANAGER]-Section Failure:\n ";
				strMessage += "RETY_DB and RETY_GUI";
				strMessage += " must have the same number of entries (comma separated)!!\nUNABLE TO RUN RULES!!!\n\n";
			}
			if(myRTIM_DB.Length != myRTIM_GUI.Length)
			{
				strMessage += "Ceda.ini [STATUSMANAGER]-Section Failure:\n ";
				strMessage += "RTIM_DB and RTIM_GUI";
				strMessage += " must have the same number of entries (comma separated)!!\nUNABLE TO RUN RULES!!!\n\n";
			}
			if(myAFTF_DB_ARR.Length != myAFTF_GUI_ARR.Length)
			{
				strMessage += "Ceda.ini [STATUSMANAGER]-Section Failure:\n ";
				strMessage += "AFTF_DB_ARRIVAL and AFTF_GUI_ARRIVAL";
				strMessage += " must have the same number of entries (comma separated)!!\nUNABLE TO RUN RULES!!!\n\n";
			}
			if(myAFTF_DB_DEP.Length != myAFTF_GUI_DEP.Length)
			{
				strMessage += "Ceda.ini [STATUSMANAGER]-Section Failure:\n ";
				strMessage += "AFTF_DB_DEPARTURE and AFTF_GUI_DEPARTURE";
				strMessage += " must have the same number of entries (comma separated)!!\nUNABLE TO RUN RULES!!!\n\n";
			}
			return strMessage;
		}

        private void MakeAlertCombo()
        {
            string strComboValues;
            string strName = "";
            int llIDX = -1;

            strName = "ALRT_C";
            llIDX = UT.GetItemNo(tabDefs.LogicalFieldList, strName);
            strComboValues = "|";
            DSAlert dsAlert = frmData.AlertTable;
            string strMsg = "";
            if (dsAlert != null)
            {
                foreach (DSAlert.ALERTRow row in dsAlert.ALERT.Select("", "NAME"))
                {
                    strComboValues += row.NAME + "|";
                    strMsg += row.ID + ":" + row.NAME + ",   ";
                }
                //MessageBox.Show(strMsg);
            }

            tabDefs.ComboOwnerDraw = false;
            tabDefs.ComboResetObject(strName);
            tabDefs.CreateComboObject(strName, /*8*/llIDX, 1, "", 100);
            tabDefs.ComboSetColumnLengthString(strName, "20");
            tabDefs.SetComboColumn(strName, llIDX);
            tabDefs.ComboAddTextLines(strName, strComboValues, "|");            
        }

		private void MakeTabCombos()
		{
			string strComboValues;
			string strName = "";
			int llIDX = -1;
			int i = 0;

			strName = "TNAM";
			llIDX = UT.GetItemNo(tabDefs.LogicalFieldList, strName);
			strComboValues = "|"; 
			ITable myTab = myDB["TPL"];
			if(myTab != null)
			{
				for( i = 0; i < myTab.Count; i++)
				{
					strComboValues += myTab[i]["TNAM"].ToString() + "|";//strComboValues + "FLIGHT|";
					//strComboValues = strComboValues + "STATUS|";
				}
			}
			tabDefs.ComboOwnerDraw = false;
			tabDefs.ComboResetObject( strName );
			tabDefs.CreateComboObject(strName, /*8*/llIDX, 1, "", 300);
			tabDefs.ComboSetColumnLengthString(strName, "64");
			tabDefs.SetComboColumn(strName, llIDX);
			tabDefs.ComboAddTextLines(strName, strComboValues, "|");

			strName = "RETY_C";
			llIDX = UT.GetItemNo(tabDefs.LogicalFieldList, strName);
			strComboValues = "|"; 
			for( i = 0; i < myRETY_DB.Length; i++)
			{
				strComboValues = strComboValues + myRETY_GUI[i] + "|";
			}
			tabDefs.ComboOwnerDraw = false;
			tabDefs.ComboResetObject( strName );
			tabDefs.CreateComboObject(strName, llIDX, 1, "", 0);
			tabDefs.ComboSetColumnLengthString(strName, "80");
			tabDefs.SetComboColumn(strName, llIDX);
			tabDefs.ComboAddTextLines(strName, strComboValues, "|");

			strName = "AFTF_C";
			llIDX = UT.GetItemNo(tabDefs.LogicalFieldList, strName);
			strComboValues = "|"; 
			if(rbArrival.Checked == true)
			{
				for( i = 0; i < myAFTF_DB_ARR.Length; i++)
				{
					strComboValues = strComboValues + myAFTF_GUI_ARR[i] + "|";
				}
			}
			else if(rbDeparture.Checked == true)
			{
				for( i = 0; i < myAFTF_DB_DEP.Length; i++)
				{
					strComboValues = strComboValues + myAFTF_GUI_DEP[i] + "|";
				}
			}
			else //Turnaround
			{
				for( i = 0; i < myAFTF_DB_ARR.Length; i++)
				{
					strComboValues = strComboValues + myAFTF_GUI_ARR[i] + "|";
				}
				for( i = 0; i < myAFTF_DB_DEP.Length; i++)
				{
					strComboValues = strComboValues + myAFTF_GUI_DEP[i] + "|";
				}
			}
			tabDefs.ComboOwnerDraw = false;
			tabDefs.ComboResetObject( strName );
			tabDefs.CreateComboObject(strName, llIDX, 1, "", 0);
			tabDefs.ComboSetColumnLengthString(strName, "80");
			tabDefs.SetComboColumn(strName, llIDX);
			tabDefs.ComboAddTextLines(strName, strComboValues, "|");


//			strName = "RTIM_C";
//			llIDX = UT.GetItemNo(tabDefs.LogicalFieldList, strName);
//			strComboValues = "|"; 
//			for( i = 0; i < myRTIM_DB.Length; i++)
//			{
//				strComboValues = strComboValues + myRTIM_GUI[i] + "|";
//			}
//			tabDefs.ComboOwnerDraw = false;
//			tabDefs.ComboResetObject( strName );
//			tabDefs.CreateComboObject(strName, llIDX, 1, "", 0);
//			tabDefs.ComboSetColumnLengthString(strName, "80");
//			tabDefs.SetComboColumn(strName, llIDX);
//			tabDefs.ComboAddTextLines(strName, strComboValues, "|");

			RebuildRefTimesCombo();
//			strName = "FLDR";
//			llIDX = UT.GetItemNo(tabDefs.LogicalFieldList, strName);
//			strComboValues = "|"; 
//			strComboValues = strComboValues + "STOA|";
//			strComboValues = strComboValues + "ETAI|";
//			strComboValues = strComboValues + "ONBL|";
//			strComboValues = strComboValues + "OFBL|";
//			strComboValues = strComboValues + "STOD|";
//			strComboValues = strComboValues + "ETDI|";
//			strComboValues = strComboValues + "CONA|";
//			tabDefs.ComboOwnerDraw = false;
//			tabDefs.ComboResetObject( strName );
//			tabDefs.CreateComboObject(strName, llIDX, 1, "", 0);
//			tabDefs.ComboSetColumnLengthString(strName, "80");
//			tabDefs.SetComboColumn(strName, llIDX);
//			tabDefs.ComboAddTextLines(strName, strComboValues, "|");

			strName = "SDEU_C";
			llIDX = UT.GetItemNo(tabDefs.LogicalFieldList, strName);
			strComboValues = "|"; 
			tabDefs.ComboOwnerDraw = false;
			tabDefs.ComboResetObject( strName );
			tabDefs.CreateComboObject(strName, llIDX, 1, "", 0);
			tabDefs.ComboSetColumnLengthString(strName, "200");
			tabDefs.SetComboColumn(strName, llIDX);
			tabDefs.ComboAddTextLines(strName, strComboValues, "|");


			tabDefs.ComboMinWidth = 10;
		}
		/// <summary>
		/// Enables/Disables the comboboxes of LineNo according to RETY ("Type") selection
		/// </summary>
		/// <param name="LineNo">Line number of tabDefs</param>
		void EnableComboboxes(int LineNo)
		{
			int idxRETY = UT.GetItemNo(tabDefs.LogicalFieldList, "RETY_C");
			string strFLDR = tabDefs.GetFieldValue(LineNo, "FLDR");
			string strRety = tabDefs.GetFieldValue(LineNo, "RETY");
			//"URNO,USRH,NAME,RETY,RETY_C,AFTF,AFTF_C,UTPL,TNAM,FLDR,SDEU,SDEU_C,USRV,RTIM,RTIM_C,TIFR"
			if( strRety == "")
			{
				tabDefs.ComboShow("TNAM", false);
				tabDefs.ComboShow("SDEU_C", false);
				tabDefs.ComboShow("FLDR", false);
				tabDefs.ComboShow("AFTF_C", false);
				PredecessorVisible = false;
			}
			else if( strRety == "AFT")
			{
				tabDefs.ComboShow("TNAM", false);
				tabDefs.ComboShow("SDEU_C", false);
				tabDefs.ComboShow("FLDR", true);
				tabDefs.ComboShow("AFTF_C", true);
				PredecessorVisible = false;
				tabDefs.SetFieldValues(LineNo, "UTPL,TNAM,SDEU,SDEU_C", ",,,");
			}
			else if( strRety == "CCAB" || strRety == "CCAE")
			{
				tabDefs.ComboShow("TNAM", false);
				tabDefs.ComboShow("AFTF_C", false);
				tabDefs.ComboShow("SDEU_C", true);
				tabDefs.ComboShow("FLDR", true);
				PredecessorVisible = false;
				tabDefs.SetFieldValues(LineNo, "AFTF,AFTF_C,UTPL,TNAM,", ",,,");
			}
			else if( strRety == "JOBI" || strRety == "JOBF" || strRety == "JOBC")
			{
				tabDefs.ComboShow("TNAM", true);
				tabDefs.ComboShow("AFTF_C", false);
				tabDefs.ComboShow("SDEU_C", true);
				tabDefs.ComboShow("FLDR", true);
				PredecessorVisible = false;
				tabDefs.SetFieldValues(LineNo, "AFTF,AFTF_C", ",");
			}
            else if (strRety == "OTIS" || strRety == "OTIE" || strRety == "OTIC" ||
               strRety == "OTOS" || strRety == "OTOE" || strRety == "OTOC" ||
                strRety == "OTHS" || strRety == "OTHE" || strRety == "OTHC")
            {
                tabDefs.ComboShow("TNAM", false);
                tabDefs.ComboShow("AFTF_C", false);
                tabDefs.ComboShow("SDEU_C", false);
                tabDefs.ComboShow("FLDR", true);
                PredecessorVisible = false;
                //tabDefs.SetFieldValues(LineNo, "AFTF,AFTF_C,UTPL,TNAM,SDEU,SDEU_C", ",,,,,");//Remove the unwanted value, not related to the rule
                tabDefs.SetFieldValues(LineNo, "AFTF,AFTF_C,UTPL,TNAM", ",,,");//Remove the unwanted value, not related to the rule
            }

			if(strFLDR == "CONA")
			{
				tabDefs.ComboShow("SDEU_C", true);
				//RebuildComboPredecessorStatus();
				PredecessorVisible = true;
			}
			else
			{
				tabDefs.ComboShow("SDEU_C", false);
				PredecessorVisible = false;
				tabDefs.Refresh();
			}
			RebuildRefTimesCombo();
			//CheckValuesChanged(LineNo);
		}
		/// <summary>
		/// Reload the predecessor combobox with the name's column values
		/// </summary>
		void RebuildComboPredecessorStatus()
		{
			if(PredecessorVisible == true)
			{
				int curSel = tabDefs.GetCurrentSelected();
				string strName = "SDEU_C";
				int llIDX = UT.GetItemNo(tabDefs.LogicalFieldList, strName);
				string strComboValues = "|"; 
				tabDefs.ComboResetContent(strName );
				for(int i = 0; i < tabDefs.GetLineCount(); i++)
				{
					if(i != curSel)
					{
						strComboValues += tabDefs.GetFieldValue(i, "NAME") + "|";
					}
				}
//				tabDefs.ComboOwnerDraw = false;
//				tabDefs.ComboResetObject( strName );
//				tabDefs.CreateComboObject(strName, llIDX, 1, "", 0);
//				tabDefs.ComboSetColumnLengthString(strName, "200");
//				tabDefs.SetComboColumn(strName, llIDX);
				tabDefs.ComboAddTextLines(strName, strComboValues, "|");
			}
		}
        private const string OTH_RETY = "OTHS,OTHE,OTHC";
        private const string OTH_FLDR = "OTAS,OTDS,OTAE,OTDE,OTAC,OTDC";
        private bool IsOthRety(string stRety)
        {
            if ((stRety == null) || (stRety.Trim() == "")) return false;
            return (OTH_RETY.IndexOf(stRety) >= 0);
        }
        private bool IsOthFldr(string stFldr)
        {
            if ((stFldr == null) || (stFldr.Trim() == "")) return false;
            return (OTH_FLDR.IndexOf(stFldr) >= 0);
        }

		void RebuildRefTimesCombo()
		{
			string strComboValues="";
			string strName = "FLDR";
			int llIDX = UT.GetItemNo(tabDefs.LogicalFieldList, strName);
            string strRety = "";
            string strAFTF = "";
            int LineNo = tabDefs.GetCurrentSelected();
            if (LineNo > -1)
            {
                strRety = tabDefs.GetFieldValue(LineNo, "RETY");
                strAFTF = tabDefs.GetFieldValue(LineNo, "AFTF");
            }

			if(rbArrival.Checked == true)
			{
				strComboValues = "|"; 
				strComboValues = strComboValues + "STOA|";
				strComboValues = strComboValues + "ETAI|";
				strComboValues = strComboValues + "ONBL|";
                if (strRety != "AFT")//AM:20080222 
				    strComboValues = strComboValues + "CONA|";
                if (strRety == "OTHS")
                {
                    strComboValues += "OTAS";
                }
                else if (strRety == "OTHE")
                {
                    strComboValues += "OTAE|";
                }
                else if (strRety == "OTHC")
                {
                    //strComboValues += "OTAC|";
                    strComboValues = "|OTAC|";
                }
                else
                {
                    //strComboValues += "OTAS|OTAE|OTAC|";
                }
			}
			else if(rbDeparture.Checked == true)
			{
				strComboValues = "|";
                strComboValues += "TIFD|";//AM:20080410
				strComboValues = strComboValues + "STOD|";
				strComboValues = strComboValues + "ETDI|";
				strComboValues = strComboValues + "OFBL|";
                if (strRety != "AFT")//AM:20080222 
				    strComboValues = strComboValues + "CONA|";
                if (strRety == "OTHS")
                {
                    strComboValues += "OTDS";
                }
                else if (strRety == "OTHE")
                {
                    strComboValues += "OTDE|";
                }
                else if (strRety == "OTHC")
                {
                    //strComboValues += "OTDC|";
                    strComboValues = "|OTDC|";
                }
                else
                {
                    //strComboValues += "OTDS|OTDE|OTDC|";
                }
			}
			else //Turnaround
			{
				strComboValues = "|"; 
				strComboValues = strComboValues + "STOA|";
				strComboValues = strComboValues + "ETAI|";
				strComboValues = strComboValues + "ONBL|";
				strComboValues = strComboValues + "STOD|";
				strComboValues = strComboValues + "ETDI|";
				strComboValues = strComboValues + "OFBL|";
                if (strRety != "AFT")//AM:20080222 
				    strComboValues = strComboValues + "CONA|";
                if (strRety == "OTHS" )
                {
                    strComboValues += "OTAS|OTDS|";
                }
                else if (strRety == "OTHE")
                {
                    strComboValues += "OTAE|OTDE|";
                }
                else if (strRety == "OTHC")
                {
                    //strComboValues += "OTAC|OTDC|";
                    strComboValues = "|OTAC|OTDC|";
                }
                else
                {
                    //strComboValues += "OTAS|OTAE|OTAC|OTDS|OTDE|OTDC|";
                }
			}
			
			if(LineNo > -1)
			{
				if( 
					(strRety == "AFT" && (strAFTF == "ONBL" || strAFTF == "OFBL")) ||
					strRety == "JOBI"
					)
				{;}
                else if (IsOthRety(strRety)) { ;}
                else
                {
                    strComboValues = strComboValues + "PLAN|";
                }
			}
			tabDefs.ComboOwnerDraw = false;
			tabDefs.ComboResetObject( strName );
			tabDefs.CreateComboObject(strName, llIDX, 1, "", 0);
			tabDefs.ComboSetColumnLengthString(strName, "80");
			tabDefs.SetComboColumn(strName, llIDX);
			tabDefs.ComboAddTextLines(strName, strComboValues, "|");
		}
		private void NewRule()
		{
			if(btnSave.BackColor == Color.Red )
			{
				DialogResult olResult;
				olResult = MessageBox.Show(this, "Do you want to save current rule?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question); 
				if ( olResult == DialogResult.Yes)
				{
					if(SaveRule() == false) return; // Do not NEW!!
				}
			}
			tabDefs.ResetContent();
			tabDefs.Refresh();
			ITable mySRHTab = myDB["SRH"];
			if(mySRHTab != null)
			{
				currDBRow = mySRHTab.CreateEmptyRow();
				currRow   = currDBRow.CopyRaw();
				currRow["ADID"] = "A";
				currRow["UHSS"] = txtSTSUrno.Text;
				currDBRow["UHSS"] = txtSTSUrno.Text;
				oldRow    = currRow.CopyRaw();
				myMode = "NEW";
				//rbArrival.Checked = true;
			}
			//UHSS_VAL UHSS
			
			ClearMyControls(this);
			myMode = "NEW";
			DateTime olFrom;
			DateTime olTo;
			olFrom = DateTime.Now;
			olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 0,0,0,0);
			olTo = new DateTime(2030, 12, 31, 23,59,59,0);
			oldValidFrom = olFrom;
			oldValidTo = olTo;
			dtValidFrom.Value = olFrom;
			dtValidTo.Value = olTo;
		}
		private bool CheckChanges()
		{
			if(CheckHeaderChanged() == true || CheckSDEChanges() == true)
			{
				btnSave.BackColor = Color.Red;
				return true;
			}
			else
			{
				btnSave.BackColor = Color.Transparent;
				return false;
			}
		}
		private bool CheckHeaderChanged()
		{
			bool changed = false;
			ArrayList arrCurr = currRow.FieldValueList();
			ArrayList arrOld  = oldRow.FieldValueList();
			for(int i = 0; i < arrCurr.Count && changed == false; i++)
			{
				if(arrCurr[i].ToString() != arrOld[i].ToString())
				{
					changed = true;
				}
			}
			if(oldValidFrom != dtValidFrom.Value || oldValidTo != dtValidTo.Value)
			{
				changed = true;
			}
			return changed;
		}
		private void CheckValuesChanged(int LineNo, bool NoPrdecessor)
		{
			int tCol = -1;
			int bCol = -1;
			tabDefs.GetLineColor(LineNo, ref tCol, ref bCol);
			string strCurrValues = tabDefs.GetFieldValues(LineNo, tabDefs.LogicalFieldList);//tabDefs.GetLineValues(LineNo);
			if(strCurrValues != tabDefs.GetLineTag(LineNo))
			{
				if(bCol == UT.colWhite)// || bCol == UT.colRed)
				{
					tabDefs.SetLineColor(LineNo, UT.colBlack, UT.colLightGreen);
					tabDefs.Refresh();
				}
			}
			else
			{
				if(bCol == UT.colLightGreen)
				{
					tabDefs.SetLineColor(LineNo, UT.colBlack, UT.colWhite);
					tabDefs.Refresh();
				}
			}
			if(CheckChanges() == true)
				btnSave.BackColor = Color.Red;
			else
				btnSave.BackColor = Color.Transparent;
		}
		private void CheckValuesChanged(int LineNo)
		{
			int tCol = -1;
			int bCol = -1;
			tabDefs.GetLineColor(LineNo, ref tCol, ref bCol);
			string strCurrValues = tabDefs.GetFieldValues(LineNo, tabDefs.LogicalFieldList);//tabDefs.GetLineValues(LineNo);
			if(strCurrValues != tabDefs.GetLineTag(LineNo))
			{
				if(bCol == UT.colWhite)// || bCol == UT.colRed)
				{
					tabDefs.SetLineColor(LineNo, UT.colBlack, UT.colLightGreen);
					tabDefs.Refresh();
				}
			}
			else
			{
				if(bCol == UT.colLightGreen)
				{
					tabDefs.SetLineColor(LineNo, UT.colBlack, UT.colWhite);
					tabDefs.Refresh();
				}
			}
			RebuildComboPredecessorStatus();
			if(CheckChanges() == true)
				btnSave.BackColor = Color.Red;
			else
				btnSave.BackColor = Color.Transparent;
		}
		private bool CheckSDEChanges()
		{
			int tCol = -1;
			int bCol = -1;
			int i = 0;
			bool isChanged = false;

			for( i = 0; i < tabDefs.GetLineCount() && isChanged == false; i++)
			{
				tabDefs.GetLineColor(i, ref tCol, ref bCol);
				if(bCol == UT.colYellow || bCol == UT.colRed || bCol == UT.colLightGreen )
				{
					isChanged = true;
				}
			}
            cbShowDeleted.Enabled = !isChanged;
			return isChanged;
		}

		private void pictureBox2_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, pictureBox2, Color.WhiteSmoke, Color.Gray);
		}

		private void pictureBox3_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, pictureBox2, Color.WhiteSmoke, Color.Gray);
		}

		private void rbArrival_CheckedChanged(object sender, System.EventArgs e)
		{
			if(rbArrival.Checked == true)
			{
				rbArrival.BackColor = Color.LightGreen;
				rbDeparture.BackColor = Color.Transparent;
				rbTurnaround.BackColor = Color.Transparent;
				txtMAXT.BackColor = Color.White;
				currRow["ADID"] = "A";
				//Now check all existing tab-lines for RFLD ==> Departure values 
				//and reset them
				string strDEP = "OFBL,STOD,ETDI";
				for(int i = 0; i < tabDefs.GetLineCount(); i++)
				{
					string strFLDR = tabDefs.GetFieldValue(i, "FLDR");
					if(strDEP.IndexOf(strFLDR, 0) > -1)
					{
						tabDefs.SetFieldValues(i, "FLDR", "");
						CheckValuesChanged(i);
					}
				}
			}
			else if(rbDeparture.Checked == true)
			{
				rbArrival.BackColor = Color.Transparent;
				rbTurnaround.BackColor = Color.Transparent;
				rbDeparture.BackColor = Color.LightGreen;
				txtMAXT.BackColor = Color.White;
				currRow["ADID"] = "D";
				string strARR = "ONBL,STOA,ETAI";
				for(int i = 0; i < tabDefs.GetLineCount(); i++)
				{
					string strFLDR = tabDefs.GetFieldValue(i, "FLDR");
					if(strARR.IndexOf(strFLDR, 0) > -1)
					{
						tabDefs.SetFieldValues(i, "FLDR", "");
						CheckValuesChanged(i);
					}
				}
				//Now check all existing tab-lines for RFLD ==> Departure values 
				//and reset them
			}
			else
			{
				rbArrival.BackColor = Color.Transparent;
				rbDeparture.BackColor = Color.Transparent;
				rbTurnaround.BackColor = Color.LightGreen;
				txtMAXT.BackColor = txtRuleName.BackColor;
				currRow["ADID"] = "T";
			}
			string strName = "AFTF_C";
			int llIDX = UT.GetItemNo(tabDefs.LogicalFieldList, strName);
			string strComboValues = "|"; 
			if(rbArrival.Checked == true)
			{
				for(int i = 0; i < myAFTF_DB_ARR.Length; i++)
				{
					strComboValues = strComboValues + myAFTF_GUI_ARR[i] + "|";
				}
			}
			else if(rbDeparture.Checked == true)
			{
				for(int i = 0; i < myAFTF_DB_DEP.Length; i++)
				{
					strComboValues = strComboValues + myAFTF_GUI_DEP[i] + "|";
				}
			}
			else //Turnaround
			{
				int i = 0;
				for( i = 0; i < myAFTF_DB_ARR.Length; i++)
				{
					strComboValues = strComboValues + myAFTF_GUI_ARR[i] + "|";
				}
				for( i = 0; i < myAFTF_DB_DEP.Length; i++)
				{
					strComboValues = strComboValues + myAFTF_GUI_DEP[i] + "|";
				}
			}
			tabDefs.ComboOwnerDraw = false;
			tabDefs.ComboResetObject( strName );
			tabDefs.CreateComboObject(strName, llIDX, 1, "", 0);
			tabDefs.ComboSetColumnLengthString(strName, "80");
			tabDefs.SetComboColumn(strName, llIDX);
			tabDefs.ComboAddTextLines(strName, strComboValues, "|");

			RebuildRefTimesCombo();
			CheckChanges();
		}

		private void btnStatusSection_Click(object sender, System.EventArgs e)
		{
			frmDataEntryGrid olDlg = new frmDataEntryGrid();
			olDlg.currTabelName = "HSS"; 
			olDlg.myCaption = "Status sections ...";
			olDlg.myHeaderLen = "0,400";
			int ilWidth = 0;
			string [] arrW = olDlg.myHeaderLen.Split(',');
			for(int i = 0; i < arrW.Length; i++)
			{
				ilWidth += Convert.ToInt32( arrW[i]);
			}
			olDlg.myWidth = ilWidth+30;
			olDlg.myHeaderString = "URNO,Name";
			olDlg.myFields = "URNO,NAME";
			olDlg.ShowDialog();
			
		}

		private void btnNew_Click(object sender, System.EventArgs e)
		{
			NewRule();
		}
		private void ClearMyControls(System.Windows.Forms.Control pControl)
		{
			foreach(System.Windows.Forms.Control olC in pControl.Controls )
			{
				string lType = olC.GetType().ToString();
				if(lType == "System.Windows.Forms.PictureBox" || lType == "System.Windows.Forms.Panel")
				{
					ClearMyControls(olC);
				}
				else if(lType == "System.Windows.Forms.TextBox" || lType == "System.Windows.Forms.ComboBox" )
				{
					if(olC.Tag != null)
					{
						if(olC.Tag.ToString() != "UHSS_VAL" && olC.Tag.ToString() != "UHSS")
						{
							olC.Text = "";
						}
					}
					else
						olC.Text = "";
				}
			}
		}
		private Control GetControlByTag(string tag, Control currCtl, ref Control myControl )
		{
			if(myControl == null)
			{
				foreach(System.Windows.Forms.Control olC in currCtl.Controls )
				{
					string lType = olC.GetType().ToString();
					if(lType == "System.Windows.Forms.PictureBox" || lType == "System.Windows.Forms.Panel")
					{
						if(olC.Tag != null)
							if(olC.Tag.ToString() != tag)
								myControl = GetControlByTag(tag.ToString(), olC, ref myControl);
							else
								myControl = olC;
						else
							myControl = GetControlByTag(tag.ToString(), olC, ref myControl);
					}
					else
					{
						if(olC.Tag != null)
						{
							if (olC.Tag.ToString() == tag)
								myControl = olC;
						}
					}
				}
			}
			return myControl;
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			Control ctl = null;
			ctl = GetControlByTag(txtTagTest.Text, this, ref ctl);
			try
			{
				ctl.Text = "Hallo";
			}
			catch(Exception err)
			{
				UT.AddException(this.Name.ToString(), "button1_Click", err.Message.ToString(),"", "");
			}
		}

		private void HeaderValueChanged(object sender, System.EventArgs e)
		{
			Control ctl = (Control)sender;
			if(ctl.Tag != null)
			{
				currRow[ctl.Tag.ToString()] = ctl.Text;
				if(ctl.Tag.ToString() == "MAXT")
				{
					//To make it easier for the stathdl to sort for MAXT
					string maxt = ctl.Text;
					maxt = maxt.PadLeft(4, ' ');
					currRow[ctl.Tag.ToString()] = maxt;
				}
				CheckChanges();
			}
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			SaveRule();
		}
		private bool SaveRule()
		{
			bool isOK = true;
			string strMessage="";
			ITable myTable = myDB["SRH"];
			if(btnSave.BackColor == Color.Transparent )
			{
				MessageBox.Show(this, "Nothing to save!!", "Info");
				return true;
			}
			//Call Validation
			strMessage = ValidateHeader();
			strMessage += ValidateDefinitions();

			if(strMessage != "")
			{
				MessageBox.Show(this, strMessage, "Save Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				isOK = false;
			}
			else //Save the rule
			{
				DateTime datNow = DateTime.Now;
				if(myMode == "NEW")
				{
					DateTime myD = new DateTime(datNow.Year, datNow.Month, datNow.Day, datNow.Hour, datNow.Minute, datNow.Second);
					txtUSEC.Text = UT.UserName;
					txtCDAT.Text = myD.ToShortDateString() + " - " + myD.ToShortTimeString();
					currRow["CDAT"] = UT.DateTimeToCeda(datNow);
					currRow["USEC"] = UT.UserName;
					currRow["DELF"] = "1";
					string maxt = currRow["MAXT"];
					maxt = maxt.PadLeft(4, ' ');
					currRow["MAXT"] = maxt;
					currDBRow.Status = State.Created;
					myTable.Add(currDBRow);

					MakeNewValtabEntry();
				}
				if(myMode == "UPDATE")
				{
					txtUSEU.Text = UT.UserName;
					txtLSTU.Text = datNow.ToShortDateString() + " - " + datNow.ToShortTimeString();
					currRow["LSTU"] = UT.DateTimeToCeda(DateTime.Now);
					currRow["USEU"] = UT.UserName;
					string maxt = currRow["MAXT"];
					maxt = maxt.PadLeft(4, ' ');
					currRow["MAXT"] = maxt;
					//currRow.Status = State.Modified;
					currDBRow.Status = State.Modified;
					ITable tabVal = myDB["VAL"];
					IRow [] rowsVal = tabVal.RowsByIndexValue("UVAL", currRow["URNO"]);
					if(rowsVal.Length > 0)
					{
						rowsVal[0]["VAFR"] = UT.DateTimeToCeda( dtValidFrom.Value);
						rowsVal[0]["VATO"] = UT.DateTimeToCeda( dtValidTo.Value);
						rowsVal[0].Status = State.Modified;
						tabVal.Save();
						oldValidFrom = dtValidFrom.Value;
						oldValidTo = dtValidTo.Value;
					}
				}
				btnSave.BackColor = Color.Transparent;
				ArrayList arrCurr = currRow.FieldValueList();
				for(int i = 0; i < arrCurr.Count; i++)
				{
					currDBRow[i] = currRow[i];
					oldRow[i] = currRow[i]; //To remember the current values, because we are in
											//updatemode from now on.
				}
				myTable.Save();
				SaveSDE();
				myMode = "UPDATE";

			}
			myTable.ReorganizeIndexes(); //TO DO THO: Das mu?intern organisiert werden
			return isOK;
		}
		void MakeNewValtabEntry()
		{
			ITable tabVal = myDB["VAL"];
			IRow rowVal = tabVal.CreateEmptyRow();
			rowVal["APPL"] = "STATMGR";
			rowVal["UVAL"] = currDBRow["URNO"];
			rowVal["FREQ"] = "1111111";
			rowVal["VAFR"] = UT.DateTimeToCeda( dtValidFrom.Value);
			rowVal["VATO"] = UT.DateTimeToCeda( dtValidTo.Value);
			rowVal.Status = State.Created;
			tabVal.Add(rowVal);
			tabVal.Save();
			tabVal.ReorganizeIndexes();
			oldValidFrom = dtValidFrom.Value;
			oldValidTo   = dtValidTo.Value;
		}
		void UpdateValtabEntry()
		{
			ITable tabVal = myDB["VAL"];
			IRow [] rowVal = tabVal.RowsByIndexValue("UVAL", currDBRow["URNO"]);
			if(rowVal.Length > 0)
			{
				rowVal[0]["VAFR"] = UT.DateTimeToCeda( dtValidFrom.Value);
				rowVal[0]["VATO"] = UT.DateTimeToCeda( dtValidTo.Value);
				rowVal[0].Status = State.Modified;
				tabVal.Save();
			}
		}
		private void SaveSDE()
		{
			int tColor = -1;
			int bColor = -1;
			ITable myTable = myDB["SDE"];
			for(int i = 0; i < tabDefs.GetLineCount(); i++)
			{
				tabDefs.GetLineColor(i, ref tColor, ref bColor);
				IRow row = null;
				ArrayList arrFlds = myTable.FieldList;
				if(bColor == UT.colYellow) //New
				{
					row = myTable.CreateEmptyRow();
					for(int j = 0; j < arrFlds.Count; j++)
					{
						string strFld = arrFlds[j].ToString();
						if(strFld != "LSTU" && 
						   strFld != "CDAT" && strFld != "USEC" && 
						   strFld != "USEU")
						{
							row[strFld] = tabDefs.GetFieldValue(i, strFld);//tabDefs.GetColumnValue(i, j);
						}
					}
					string strTime = UT.DateTimeToCeda( DateTime.Now);
					row["CDAT"] = strTime;
					tabDefs.SetFieldValues(i, "CDAT", strTime);
					row["USEC"] = UT.UserName;
					row["DELF"] = "1";
					tabDefs.SetFieldValues(i, "USEC", UT.UserName);
					tabDefs.SetFieldValues(i, "URNO", row["URNO"]);
					row.Status = State.Created;
					myTable.Add(row);
					myTable.Save();
					tabDefs.SetLineColor(i, UT.colBlack , UT.colWhite);
					tabDefs.Refresh();
				}
				if(bColor == UT.colLightGreen)//Updated
				{
					int idxUrno = UT.GetItemNo(tabDefs.LogicalFieldList, "URNO");
					if(idxUrno > -1)
					{
						IRow [] rows = myTable.RowsByIndexValue("URNO", tabDefs.GetColumnValue(i, idxUrno));
						if(rows.Length > 0)
						{
							for(int j = 0; j < arrFlds.Count; j++)
							{
								string strFld = arrFlds[j].ToString();
								if(strFld != "URNO" && strFld != "LSTU" && 
									strFld != "CDAT" && strFld != "USEC" && 
									strFld != "USEU")
								{
									rows[0][strFld] = tabDefs.GetFieldValue(i, strFld);//tabDefs.GetColumnValue(i, j);
								}
							}
							string strTime = UT.DateTimeToCeda( DateTime.Now);
							rows[0]["LSTU"] = strTime;
							tabDefs.SetFieldValues(i, "LSTU", strTime);
							rows[0]["USEU"] = UT.UserName;
							rows[0]["DELF"] = "1";
							tabDefs.SetFieldValues(i, "USEU", UT.UserName);
							rows[0].Status = State.Modified;
						}
					}
					myTable.Save();
					tabDefs.SetLineColor(i, UT.colBlack , UT.colWhite);
					tabDefs.Refresh();
				}
				if(bColor == UT.colRed) // Deleted
				{
					int idxUrno = UT.GetItemNo(tabDefs.LogicalFieldList, "URNO");
					if(idxUrno > -1)
					{
						IRow [] rows = myTable.RowsByIndexValue("URNO", tabDefs.GetColumnValue(i, idxUrno));
						if(rows.Length > 0)
						{
							rows[0]["DELF"] = "X";
							rows[0].Status = State.Modified;
							myTable.Save();
						}
					}
				}
			}
			tabDefs.Refresh();
			//Remove the deleted (red) lines
			for(int i = tabDefs.GetLineCount()-1; i >= 0; i--)
			{
				tabDefs.GetLineColor(i, ref tColor, ref bColor);
				if(bColor == UT.colRed)
				{
					//tabDefs.DeleteLine(i);
					tabDefs.SetLineColor(i, UT.colBlack, UT.colOrange);
				}
			}
			for(int i = 0; i < tabDefs.GetLineCount(); i++)
			{
				string strValues = tabDefs.GetFieldValues(i, tabDefs.LogicalFieldList);//tabDefs.GetLineValues(i);
				tabDefs.SetLineTag(i, strValues);
			}
			myTable.ReorganizeIndexes();//TO DO THO: Das mu?intern organisiert werden
            SetSDETab(_curSrhUrno);//AM 20080522 - To refresh data
			tabDefs.SetCurrentSelection(-1);
			tabDefs.Refresh();
			//handle the delete button's text
			int currSel = tabDefs.GetCurrentSelected();
			if(currSel > -1)
			{
				if(tabDefs.GetFieldValue(currSel, "DELF") == "X")
				{
					btnDeleteDef.Text = "R&eactivate";
				}
				else
				{
					btnDeleteDef.Text = "&Delete";
				}
			}
		}

		/// <summary>
		/// Validates the Header of the rule
		/// </summary>
		/// <returns>Message as Error string</returns>
		private string ValidateHeader()
		{
			string strMessage="";
			ITable myTable = myDB["SRH"];
			string strName = txtRuleName.Text;
			if(strName == "")
			{
				strMessage += "Rulename is mandatory!\n";
			}
			else
			{
				//if(myMode == "NEW")
				{
					bool isDuplicate = false;
					for (int i = 0; i < myTable.Count && isDuplicate == false; i++)
					{
						if(myTable[i]["NAME"] == strName && myTable[i]["URNO"] != currDBRow["URNO"])
						{
							isDuplicate = true;
							strMessage += "Rulename already exists\n";
						}
					}
				}
			}
			if(rbTurnaround.Checked == true)
			{
				if(txtMAXT.Text == "")
				{
					strMessage += "Max Groundtime is mandatory!\n";
				}
			}
			if(txtSTSUrno.Text == "")
			{
				strMessage += "Status Section is mandatory!\n";
			}
			DateTime datFrom = dtValidFrom.Value;
			DateTime datTo = dtValidTo.Value;
			if(datTo < datFrom)
			{
				strMessage += "Valid from is later than valid to!\n";
			}
			if(txtACTValue.ForeColor == Color.Red)
			{
				strMessage += "A/C Type is not valid!\n";
			}
			if(txtAPTValue.ForeColor == Color.Red)
			{
				strMessage += "Airport is not valid!\n";
			}
			if(txtALTValue.ForeColor == Color.Red)
			{
				strMessage += "Airline is not valid!\n";
			}
			if(txtNATValue.ForeColor == Color.Red)
			{
				strMessage += "Nature is not valid!\n";
			}
			return strMessage;
		}
		/// <summary>
		/// Validates the entries in the SDE Tab.ocx
		/// </summary>
		/// <returns>Message as error string</returns>
		private string ValidateDefinitions()
		{
			string strMessage="";
			int tCol = -1;
			int bCol = -1;
            string tMsg = "";
			for(int i = 0; i < tabDefs.GetLineCount(); i++)
            {
				tabDefs.GetLineColor(i, ref tCol, ref bCol);
                if (frmData.HasInterfaceUpdate)
                {
                    tMsg = CheckConflict(tabDefs.GetFieldValue(i, "TSKN"), tabDefs.GetFieldValue(i, "RETY"),
                        tabDefs.GetFieldValue(i, "FLDR"), i + 1);
                    if (tMsg != "") strMessage += "Grid Line " + (i + 1).ToString() + ": " + tMsg + "\n";
                }

                bool hasDetectedTSKNMissing = false;
				if(bCol == UT.colYellow || bCol == UT.colLightGreen)
				{
					string strRETY = "";
					string strFLDR = "";
					if(tabDefs.GetFieldValue(i, "NAME") == "")
					{
						strMessage += "Grid Line " + (i+1).ToString() + ": Name is mandatory!\n";
					}
					strRETY = tabDefs.GetFieldValue(i, "RETY");
					if(strRETY == "")
					{
						strMessage += "Grid Line " + (i+1).ToString() + ": Type is mandatory!\n";
					}
					else
					{
						if(strRETY == "AFT")
						{
							string strAFTF = tabDefs.GetFieldValue(i, "AFTF");
							if(strAFTF == "")
							{
								strMessage += "Grid Line " + (i+1).ToString() + ": Flight-Field is mandatory!\n";
							}
						}
                        else if ((frmData.HasInterfaceUpdate) && (!hasDetectedTSKNMissing) && (IsOthRety(strRETY)))
                        {
                            if (tabDefs.GetFieldValue(i, "TSKN").Trim() == "")
                            {
                                strMessage += "Grid Line " + (i + 1).ToString() + ": Interface_Task_Name is mandatory!\n";
                                hasDetectedTSKNMissing = true;
                            }
                        }
						else if( strRETY == "JOBI" || strRETY == "JOBF" || strRETY == "JOBC")
						{
//Do not check for mandatory: Meeting with AMA 01.12.2003
//							string strUTPL = tabDefs.GetFieldValue(i, "UTPL");
//							if(strUTPL == "")
//							{
//								strMessage += "Grid Line " + (i+1).ToString() + ": Template is mandatory for Jobs!\n";
//							}
						}

					}
					strFLDR = tabDefs.GetFieldValue(i, "FLDR");
					if(strFLDR == "")
					{
						strMessage += "Grid Line " + (i+1).ToString() + ": Ref.-Time is mandatory!\n";
					}
					else if(strFLDR == "CONA")
					{
						if(tabDefs.GetFieldValue(i, "SDEU") == "")
						{
							strMessage += "Grid Line " + (i+1).ToString() + ": Predecessor is mandatory!\n";
						}
					}
                    else if ((frmData.HasInterfaceUpdate) && (!hasDetectedTSKNMissing) && (IsOthFldr(strFLDR)))
                    {
                        if (tabDefs.GetFieldValue(i, "TSKN").Trim() == "")
                        {
                            strMessage += "Grid Line " + (i + 1).ToString() + ": Interface Task_Name is mandatory!\n";
                        }
                    }

                    tMsg = "";
                    if (!IsValidOthSetting(strFLDR, strRETY, i+1, out tMsg ))
                    {
                        strMessage +=  tMsg;
                    }

					string strTIFR = tabDefs.GetFieldValue(i, "TIFR");
					if(strTIFR != "")
					{
						bool blDigitOK = true;
						for(int j = 0; j < strTIFR.Length && blDigitOK == true; j++)
						{
							if(Char.IsDigit(strTIFR[j]) == false)
							{
								if(j == 0 && strTIFR[j] == '-')
								{;}
								else
									blDigitOK = false;
							}
						}
						if(blDigitOK == false)
						{
							strMessage += "Grid Line " + (i+1).ToString() + ": Time must be digit (e.g. -10 or 29)!\n";
						}
					}
					else
					{
						strMessage += "Grid Line " + (i+1).ToString() + ": Time is mandatory\n";
					}
				}
			}

			return strMessage;
		}

		private void btnOpen_Click(object sender, System.EventArgs e)
		{
			if(btnSave.BackColor == Color.Red )
			{
				DialogResult olResult;
				olResult = MessageBox.Show(this, "Do you want to save current rule?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question); 
				if ( olResult == DialogResult.Yes)
				{
					if(SaveRule() == false) return; // Do not NEW!!
				}
			}
			frmChoiceList olDlg = new frmChoiceList();
			olDlg.lblCaption.Text = "Rules";
			olDlg.currTable = "SRH";
			olDlg.isAddEmptyLine = false;
			
			if(olDlg.ShowDialog() == DialogResult.OK)
			{
                cbShowDeleted.Enabled = true;//AM 20080522
				ITable myTable = myDB["SRH"];
				int sel = olDlg.tabList.GetCurrentSelected();
				string strUrno = olDlg.tabList.GetColumnValue(sel,0);
				IRow [] rows = myTable.RowsByIndexValue("URNO", strUrno);

				ClearMyControls(this);

				if(rows.Length > 0)
				{
					string [] arrFields = "NAME,ACTG,ACTU,ADID,ALTG,ALTU,APTG,APTU,CDAT,LSTU,NATG,NATU,UHSS,USEC,USEU,MAXT".Split(',');
					for(int i = 0; i < arrFields.Length; i++)					
					{
						Control ctl = null;
						ctl = GetControlByTag(arrFields[i], this, ref ctl);
						if(ctl != null)
						{
							string strText = rows[0][arrFields[i]];
							strText = strText.Trim();
							ctl.Text = strText;
						}
					}

					SetCedaFields(rows[0]);
					SetValueFields(rows[0]);
					currDBRow = rows[0];
					currRow = currDBRow.CopyRaw();
					oldRow = currRow.CopyRaw();
					if(rows[0]["ADID"] == "A")
						rbArrival.Checked = true;
					else if(rows[0]["ADID"] == "D")
						rbDeparture.Checked = true;
					else //Turnaround
						rbTurnaround.Checked = true;
					SetSDETab(rows[0]["URNO"]);
					txtNATValue.ForeColor = Color.Black;
					txtALTValue.ForeColor = Color.Black;
					txtAPTValue.ForeColor = Color.Black;
					txtACTValue.ForeColor = Color.Black;
				}
				btnSave.BackColor = Color.Transparent;
				myMode = "UPDATE";
				MakeTabCombos();
			}
		}

        private string _curSrhUrno = null;//AM 20080522 - To keep the current SRHTAB Urno
		/// <summary>
		/// Set the status definitions into the tabDefs grid
		/// </summary>
		/// <param name="strUSRH"></param>
		void SetSDETab(string strUSRH)
		{
            _curSrhUrno = strUSRH;
			tabDefs.ResetContent();
			ITable myTab = myDB["SDE"];
			if(myTab != null)
			{
				IRow [] rows = myTab.RowsByIndexValue("USRH", strUSRH);
				//First insert the database field values
				int i = 0;
				if(rows.Length > 0)
				{
					int LineNo = -1;
					for(i = 0; i < rows.Length; i++)
					{
                        //AM 20080522 - Start
                        if ((!cbShowDeleted.Checked) && (rows[i]["DELF"] == "X"))
                        {//"Show Deleted" button was unchecked, no need to show the deleted item
                            continue;
                        }
                        //AM 20080522 - End
						string strValues = MakeEmptyString(tabDefs.LogicalFieldList, ',');
						tabDefs.InsertTextLine(strValues, false);
						LineNo = tabDefs.GetLineCount() - 1;
						string strFld = "";
						for(int j = 0; j < myTab.FieldList.Count; j++)
						{
							strFld = myTab.FieldList[j].ToString();
							tabDefs.SetFieldValues(LineNo, strFld, rows[i][strFld]);
                            if (strFld == "ALRT")
                            {
                                string stVal = rows[i][strFld];
                                DSAlert.ALERTRow[] alertRows = (DSAlert.ALERTRow[])frmData.AlertTable.ALERT.Select("ID='" + stVal + "'");
                                //MessageBox.Show("ALRT: " + alertRows.Length +", " + stVal );
                                if (alertRows.Length == 1)
                                {
                                    tabDefs.SetFieldValues(LineNo, "ALRT_C", alertRows[0].NAME);
                                }
                            }
							else if(strFld == "RETY")
							{
								for(int k = 0; k < myRETY_DB.Length; k++)
								{
									if(myRETY_DB[k] == rows[i][strFld])
									{
										tabDefs.SetFieldValues(LineNo, "RETY_C", myRETY_GUI[k] );
									}
								}
							}
							else if(strFld == "AFTF")
							{
								if(rbArrival.Checked == true)
								{
									for(int k = 0; k < myAFTF_DB_ARR.Length; k++)
									{
										if(myAFTF_DB_ARR[k] == rows[i][strFld])
										{
											tabDefs.SetFieldValues(LineNo, "AFTF_C", myAFTF_GUI_ARR[k] );
										}
									}
								}
								else if(rbDeparture.Checked == true)
								{
									for(int k = 0; k < myAFTF_DB_DEP.Length; k++)
									{
										if(myAFTF_DB_DEP[k] == rows[i][strFld])
										{
											tabDefs.SetFieldValues(LineNo, "AFTF_C", myAFTF_GUI_DEP[k] );
										}
									}
								}
								else //Turnaround
								{
									int k = 0;
									for( k = 0; k < myAFTF_DB_ARR.Length; k++)
									{
										if(myAFTF_DB_ARR[k] == rows[i][strFld])
										{
											tabDefs.SetFieldValues(LineNo, "AFTF_C", myAFTF_GUI_ARR[k] );
										}
									}
									for( k = 0; k < myAFTF_DB_DEP.Length; k++)
									{
										if(myAFTF_DB_DEP[k] == rows[i][strFld])
										{
											tabDefs.SetFieldValues(LineNo, "AFTF_C", myAFTF_GUI_DEP[k] );
										}
									}
								}
							}
							if(strFld == "RTIM")
							{
								for(int k = 0; k < myRTIM_DB.Length; k++)
								{
									if(myRTIM_DB[k] == rows[i][strFld])
									{
										tabDefs.SetFieldValues(LineNo, "RTIM_C", myRTIM_GUI[k] );
									}
								}
							}
							if(strFld == "UTPL")
							{
								ITable tplTable = myDB["TPL"];
								IRow [] tplRow = tplTable.RowsByIndexValue("URNO", rows[i][strFld]);
								if(tplRow.Length > 0)
								{
									tabDefs.SetFieldValues(LineNo, "TNAM", tplRow[0]["TNAM"] );
								}
							}
							if(rows[i]["DELF"] == "X")
							{
								tabDefs.SetLineColor(LineNo, UT.colBlack, UT.colOrange);
							}
						}	
					}
					for( i = 0; i < tabDefs.GetLineCount(); i++)
					{
						string strSDEU = tabDefs.GetFieldValue(i, "SDEU");
						if(strSDEU != "")
						{
							IRow [] tmpRows = myTab.RowsByIndexValue("URNO", strSDEU);
							if(tmpRows.Length > 0)
							{
								tabDefs.SetFieldValues(i, "SDEU_C", tmpRows[0]["NAME"]);
							}
						}
						string strValues = tabDefs.GetFieldValues(i, tabDefs.LogicalFieldList);//tabDefs.GetLineValues(i);
						tabDefs.SetLineTag(i, strValues);
					}
				}
			}
			tabDefs.Refresh();
		}
		void SetCedaFields(IRow row)
		{
			DateTime olD;
			if(row["CDAT"] != "")
			{
				olD = UT.CedaFullDateToDateTime(row["CDAT"]);
				txtCDAT.Text = olD.ToShortDateString() + " - " + olD.ToShortTimeString();
			}
			if(row["LSTU"] != "")
			{
				olD = UT.CedaFullDateToDateTime(row["LSTU"]);
				txtLSTU.Text = olD.ToShortDateString() + " - " + olD.ToShortTimeString();
			}
		}
		void SetValueFields(IRow row)
		{
			for(int i = 0; i < myURNOFields.Length; i++)
			{
				Control ctl = null;
				ctl = GetControlByTag(myVALUEFields[i], this, ref ctl);
				if(ctl != null)
				{
					ITable myTable = myDB[myLookupTabs[i]];
					if(myURNOFields[i] != "")
					{
						IRow [] rows = myTable.RowsByIndexValue("URNO", row[myURNOFields[i]]);
						if(rows.Length > 0)
						{
							string [] arrValues = myLookupFields[i].Split('|');
							string strText = "";
							for(int j = 0; j < arrValues.Length; j++)
							{
								string strCurrVal = rows[0][arrValues[j]];
								if(strCurrVal != "")
								{
									strText += rows[0][arrValues[j]] + "/";
								}
							}
							if(strText != "")
							{
								strText = strText.Substring(0, strText.Length -1);
							}
							ctl.Text = strText;
						}
					}
				}
			}
			//Set the validity fields
			ITable tabVal = myDB["VAL"];
			IRow [] rowsVal = tabVal.RowsByIndexValue("UVAL", row["URNO"]);
			if(rowsVal.Length > 0)
			{
				DateTime datFrom = UT.CedaFullDateToDateTime(rowsVal[0]["VAFR"]);
				DateTime datTo   = UT.CedaFullDateToDateTime(rowsVal[0]["VATO"]);
				dtValidFrom.Value = datFrom;
				dtValidTo.Value = datTo;
				oldValidFrom = datFrom;
				oldValidTo = datTo;
			}
		}

		private void btnDeleteHeader_Click(object sender, System.EventArgs e)
		{
			int i = 0;
			DialogResult olResult;
			olResult = MessageBox.Show(this, "Do you really want to delete the current rule?", "Question", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
			if(olResult != DialogResult.Yes)
			{
				return;
			}
			if(myMode == "UPDATE")
			{
				ITable myTable =  myDB["SRH"];
				currDBRow["DELF"] = "X";
				currDBRow.Status = State.Modified;
				ITable tabVal = myDB["VAL"];
				IRow [] rowsVal = tabVal.RowsByIndexValue("UVAL", currRow["URNO"]);
				if(rowsVal.Length > 0)
				{
					//DO not change Valtab!!
					//rowsVal[0].Status = State.Modified;
					//tabVal.Save();
				}
				myTable.Save();
				myTable = myDB["SDE"];
				for( i = 0; i < tabDefs.GetLineCount(); i++)
				{
					int idxUrno = UT.GetItemNo(tabDefs.LogicalFieldList, "URNO");
					if(idxUrno > -1)
					{
						IRow [] rows = myTable.RowsByIndexValue("URNO", tabDefs.GetColumnValue(i, idxUrno));
						if(rows.Length > 0)
						{
							rows[0]["DELF"] = "X";
							rows[0].Status = State.Modified;
							myTable.Save();
						}
					}
				}
			}
			NewRule();
		}

		private void btnAddDef_Click(object sender, System.EventArgs e)
		{
			NewSED();
		}
		private void NewSED()
		{
			ITable myTab = myDB["SDE"];
			if(myTab != null)
			{
				string strValues = MakeEmptyString(tabDefs.LogicalFieldList, ',');
				tabDefs.InsertTextLine(strValues, true);
				int cnt = tabDefs.GetLineCount() - 1;
				string strUrno = myDB.GetNextUrno();
				tabDefs.SetFieldValues(cnt, "URNO", strUrno);
				tabDefs.SetLineColor(cnt, UT.colBlack, UT.colYellow);
				tabDefs.SetFieldValues(cnt, "USRH", currDBRow["URNO"]);
				tabDefs.OnVScrollTo(cnt);
				CheckChanges();
			}
		}
		/// <summary>
		/// Crates an empty string for tab insertion
		/// </summary>
		/// <param name="strValueList">[separator] delimited string </param>
		/// <param name="separator">separator</param>
		/// <returns></returns>
		string MakeEmptyString(string strValueList, char separator)
		{
			int i = 0;
			string [] strArr = strValueList.Split(separator);
			string strValues = "";
			for(i = 0; i < strArr.Length-1; i++)
			{
				strValues += separator.ToString();
			}
			return strValues;
		}
		private void HandleSDEDelete()
		{
			int currSel = tabDefs.GetCurrentSelected();
			int bCol = -1;
			int tCol = -1;
			if(currSel > -1)
			{
				tabDefs.GetLineColor(currSel, ref tCol, ref bCol);
				if(bCol != UT.colRed && bCol != UT.colOrange)
				{
					tabDefs.SetLineColor(currSel, UT.colBlack, UT.colRed);
				}
				if(bCol == UT.colYellow)
				{//Newly Added Row in this screen
					tabDefs.DeleteLine(currSel);
				}
				if( bCol == UT.colOrange)
				{//Originally Deleted Row
					tabDefs.SetLineColor(currSel, UT.colBlack, UT.colLightGreen);
					tabDefs.SetFieldValues(currSel, "DELF", "1");
					btnDeleteDef.Text = "&Delete";
					CheckValuesChanged(currSel);
				}
				if(bCol == UT.colRed )
				{//Newly Deleted Row in this screen
					tabDefs.SetLineColor(currSel, UT.colBlack, UT.colWhite);
					CheckValuesChanged(currSel);
				}
				tabDefs.Refresh();
			}
		}

		private void btnDeleteDef_Click(object sender, System.EventArgs e)
		{
			HandleSDEDelete();
			CheckChanges();
		}

		/// <summary>
		/// Put the internal values into the DB columns according to the GUI/user selection
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tabDefs_ComboSelChanged(object sender, AxTABLib._DTABEvents_ComboSelChangedEvent e)
		{
			if(e.colNo == UT.GetItemNo(tabDefs.LogicalFieldList, "TNAM") )//Template Name Combo was selected
			{
				ITable myTab = myDB["TPL"];
				if(myTab != null)
				{
					bool blFound = false;
					for(int i = 0; i < myTab.Count && blFound == false; i++)
					{
						if(myTab[i]["TNAM"].ToString() == e.newValues)
						{
							blFound = true;
							tabDefs.SetFieldValues(e.lineNo, "UTPL", myTab[i]["URNO"]);
							tabDefs.Refresh();
						}
					}
				}
				EnableComboboxes(e.lineNo);
			}
			else if(e.colNo == UT.GetItemNo(tabDefs.LogicalFieldList, "FLDR") )
			{
				EnableComboboxes(e.lineNo);
				if(e.newValues != "CONA")
				{
					tabDefs.SetFieldValues(e.lineNo, "SDEU,SDEU_C", ",");
					tabDefs.Refresh();
				}
				else
				{
					RebuildComboPredecessorStatus();
				}
			}
            else if (e.colNo == UT.GetItemNo(tabDefs.LogicalFieldList, "ALRT_C"))//ALRT Combo was selected
            {
                bool blFound = false;
                DSAlert.ALERTRow[] alertRows = (DSAlert.ALERTRow[])frmData.AlertTable.ALERT.Select("NAME='" + e.newValues + "'");
                if (alertRows.Length == 1)
                {
                    blFound = true;
                    tabDefs.SetFieldValues(e.lineNo, "ALRT", alertRows[0].ID );
                    tabDefs.Refresh();
                }
                if (blFound == false)
                {
                    tabDefs.SetFieldValues(e.lineNo, "ALRT", "");
                }
                //EnableComboboxes(e.lineNo);
            }
			else if(e.colNo == UT.GetItemNo(tabDefs.LogicalFieldList, "RETY_C") )//RETY Combo was selected
			{
				bool blFound = false;
				for(int i = 0; i < myRETY_GUI.Length && blFound == false; i++)
				{
					if(myRETY_GUI[i] == e.newValues)
					{
						blFound = true;
						tabDefs.SetFieldValues(e.lineNo, "RETY", myRETY_DB[i]);
						tabDefs.Refresh();
					}
				}
				if(blFound == false)
				{
					tabDefs.SetFieldValues(e.lineNo, "RETY", "");
				}
				EnableComboboxes(e.lineNo);
			}
			else if(e.colNo == UT.GetItemNo(tabDefs.LogicalFieldList, "AFTF_C") )//RETY Combo was selected
			{
				bool blFound = false;
				if(rbArrival.Checked == true)
				{
					for(int i = 0; i < myAFTF_GUI_ARR.Length && blFound == false; i++)
					{
						if(myAFTF_GUI_ARR[i] == e.newValues)
						{
							blFound = true;
							tabDefs.SetFieldValues(e.lineNo, "AFTF", myAFTF_DB_ARR[i]);
							tabDefs.Refresh();
						}
					}
				}
				else if(rbDeparture.Checked == true)
				{
					for(int i = 0; i < myAFTF_GUI_DEP.Length && blFound == false; i++)
					{
						if(myAFTF_GUI_DEP[i] == e.newValues)
						{
							blFound = true;
							tabDefs.SetFieldValues(e.lineNo, "AFTF", myAFTF_DB_DEP[i]);
							tabDefs.Refresh();
						}
					}
				}
				else //Turnaround
				{
					int i = 0;
					for( i = 0; i < myAFTF_GUI_ARR.Length && blFound == false; i++)
					{
						if(myAFTF_GUI_ARR[i] == e.newValues)
						{
							blFound = true;
							tabDefs.SetFieldValues(e.lineNo, "AFTF", myAFTF_DB_ARR[i]);
							tabDefs.Refresh();
						}
					}
					for( i = 0; i < myAFTF_GUI_DEP.Length && blFound == false; i++)
					{
						if(myAFTF_GUI_DEP[i] == e.newValues)
						{
							blFound = true;
							tabDefs.SetFieldValues(e.lineNo, "AFTF", myAFTF_DB_DEP[i]);
							tabDefs.Refresh();
						}
					}
				}

				if(blFound == false)
				{
					tabDefs.SetFieldValues(e.lineNo, "AFTF", "");
				}
				EnableComboboxes(e.lineNo);
			}
			if(e.colNo == UT.GetItemNo(tabDefs.LogicalFieldList, "RTIM_C") )//RTIM Combo was selected
			{
				bool blFound = false;
				for(int i = 0; i < myRETY_GUI.Length && blFound == false; i++)
				{
					if(myRTIM_GUI[i] == e.newValues)
					{
						blFound = true;
						tabDefs.SetFieldValues(e.lineNo, "RTIM", myRTIM_DB[i]);
						tabDefs.Refresh();
					}
				}
				if(blFound == false)
				{
					tabDefs.SetFieldValues(e.lineNo, "RTIM", "");
				}
				EnableComboboxes(e.lineNo);
			}
			if(e.colNo == UT.GetItemNo(tabDefs.LogicalFieldList, "SDEU_C") )//RTIM Combo was selected
			{
				bool blFound = false;
				for(int i = 0; i < tabDefs.GetLineCount() && blFound == false; i++)
				{
					string strName = tabDefs.GetFieldValue(i, "NAME");
					string strUrno = tabDefs.GetFieldValue(i, "URNO");
					if(strName == e.newValues)
					{
						blFound = true;
						tabDefs.SetFieldValues(e.lineNo, "SDEU", strUrno);
						tabDefs.Refresh();
					}
				}
				if(blFound == false)
				{
					tabDefs.SetFieldValues(e.lineNo, "SDEU", "");
				}
			}
			CheckValuesChanged(e.lineNo, false);
		}

		private void tabDefs_CloseInplaceEdit(object sender, AxTABLib._DTABEvents_CloseInplaceEditEvent e)
		{
			CheckValuesChanged(e.lineNo );
		}

		private void tabDefs_EditPositionChanged(object sender, AxTABLib._DTABEvents_EditPositionChangedEvent e)
		{
			CheckValuesChanged(e.lineNo );
			myCurrEditColumn = e.colNo;

		}

		private void tabDefs_RowSelectionChanged(object sender, AxTABLib._DTABEvents_RowSelectionChangedEvent e)
		{
			int txtCol = -1;
			int bkCol = -1;
			tabDefs.GetLineColor(e.lineNo, ref txtCol, ref bkCol);
			if(bkCol == UT.colOrange)
			{
				btnDeleteDef.Text = "R&eactivate";
			}
			else
			{
				btnDeleteDef.Text = "&Delete";
			}
			EnableComboboxes(e.lineNo);
			CheckValuesChanged(e.lineNo );
		}


		private void btnCopy_Click(object sender, System.EventArgs e)
		{
			int i = 0;
			string strNewUrno = "";
			ITable mySRHTab = myDB["SRH"];
			myMode = "NEW";
			oldRow = currDBRow.CopyRaw();
			oldRow["LSTU"] = "";
			oldRow["CDAT"] = UT.DateTimeToCeda(DateTime.Now);
			oldRow["USEC"] = UT.UserName;
			oldRow["USEU"] = "";
			currDBRow = mySRHTab.CreateEmptyRow();
			string strCurrName = txtRuleName.Text;
			strCurrName += "2";
			txtRuleName.Text = strCurrName;
			string strUHSS = txtSTSUrno.Text;
			currDBRow["UHSS"] = strUHSS;
			if(rbArrival.Checked == true)
				currDBRow["ADID"] = "A";
			else if(rbDeparture.Checked == true)
				currDBRow["ADID"] = "D";
			else //Turnaround
				currDBRow["ADID"] = "T";

			strNewUrno = currDBRow["URNO"];
			for(i = 0; i < oldRow.Count; i++)
			{
				currDBRow[i] = oldRow[i];
			}
			currDBRow["URNO"] = strNewUrno;
			oldRow["URNO"] = currDBRow["URNO"];
			currDBRow["NAME"] = strCurrName;
			currRow = currDBRow.CopyRaw();
			btnSave.BackColor = Color.Red;
			for(  i = 0; i < tabDefs.GetLineCount(); i++)
			{
				string strUrno = myDB.GetNextUrno();
				tabDefs.SetFieldValues(i, "URNO", strUrno);
				tabDefs.SetLineColor(i, UT.colBlack, UT.colYellow);
				tabDefs.SetFieldValues(i, "USRH", currDBRow["URNO"]);
			}
			SaveRule();
		}

		private void txtACTValue_Leave(object sender, System.EventArgs e)
		{
			string strValue = txtACTValue.Text;
			if(strValue == "")
			{
				txtACTUrno.Text = "";
				txtACTValue.ForeColor = Color.Black;
				return; //Need no check
			}
			ITable myTab = myDB["ACT"];
			bool blFound = false;
			for(int i = 0; i < myTab.Count && blFound == false; i++)
			{
				if(strValue == myTab[i]["ACT3"] || strValue == myTab[i]["ACT5"])
				{
					txtACTUrno.Text = myTab[i]["URNO"];
					blFound = true;
				}
			}
			if(blFound == true)
			{
				txtACTValue.ForeColor = Color.Black;
				txtACTGroupUrno.Text = "";
				txtACTGroupValue.Text = "";
			}
			else
				txtACTValue.ForeColor = Color.Red;
		}

		private void txtAPTValue_Leave(object sender, System.EventArgs e)
		{
			string strValue = txtAPTValue.Text;
			if(strValue == "")
			{
				txtAPTUrno.Text = "";
				txtAPTValue.ForeColor = Color.Black;
				return; //Need no check
			}
			ITable myTab = myDB["APT"];
			bool blFound = false;
			for(int i = 0; i < myTab.Count && blFound == false; i++)
			{
				if(strValue == myTab[i]["APC3"] || strValue == myTab[i]["APC4"])
				{
					txtAPTUrno.Text = myTab[i]["URNO"];
					blFound = true;
				}
			}
			if(blFound == true)
			{
				txtAPTValue.ForeColor = Color.Black;
				txtAPTGroupUrno.Text = "";
				txtAPTGroupValue.Text = "";
			}
			else
				txtAPTValue.ForeColor = Color.Red;
		}

		private void txtALTValue_Leave(object sender, System.EventArgs e)
		{
			string strValue = txtALTValue.Text;
			if(strValue == "")
			{
				txtALTUrno.Text = "";
				txtALTValue.ForeColor = Color.Black;
				return; //Need no check
			}
			ITable myTab = myDB["ALT"];
			bool blFound = false;
			for(int i = 0; i < myTab.Count && blFound == false; i++)
			{
				if(strValue == myTab[i]["ALC2"] || strValue == myTab[i]["ALC3"])
				{
					txtALTUrno.Text = myTab[i]["URNO"];
					blFound = true;
				}
			}
			if(blFound == true)
			{
				txtALTValue.ForeColor = Color.Black;
				txtALTGroupUrno.Text = "";
				txtALTGroupValue.Text = "";
			}
			else
				txtALTValue.ForeColor = Color.Red;
		}

		private void txtNATValue_Leave(object sender, System.EventArgs e)
		{
			string strValue = txtNATValue.Text;
			if(strValue == "")
			{
				txtNATUrno.Text = "";
				txtNATValue.ForeColor = Color.Black;
				return; //Need no check
			}
			ITable myTab = myDB["NAT"];
			bool blFound = false;
			for(int i = 0; i < myTab.Count && blFound == false; i++)
			{
				if(strValue == myTab[i]["TTYP"] || strValue == myTab[i]["TNAM"])
				{
					txtNATUrno.Text = myTab[i]["URNO"];
					blFound = true;
				}
			}
			if(blFound == true)
			{
				txtNATValue.ForeColor = Color.Black;
				txtNATGroupUrno.Text = "";
				txtNATGroupValue.Text = "";
			}
			else
				txtNATValue.ForeColor = Color.Red;
		}

		private void frmRules_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if(btnSave.BackColor == Color.Red)
			{
				DialogResult olResult;
				olResult = MessageBox.Show(this, "Do you want to save current rule?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question); 
				if ( olResult == DialogResult.Yes)
				{
					if(SaveRule() == false)
					{
						e.Cancel = true;
						return; 
					}
				}
				btnSave.BackColor = Color.Transparent;
			}
			//Store current geometry in Registry
			string theKey = "Software\\StatusManager\\RuleEditor";
			RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			if(rk == null)
			{
				rk = Registry.CurrentUser.OpenSubKey("Software\\StatusManager", true);
				rk.CreateSubKey("RuleEditor");
				rk.Close();
				theKey = "Software\\StatusManager\\RuleEditor";
				rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			}

			rk.SetValue("X", this.Left.ToString());
			rk.SetValue("Y", this.Top.ToString());
			rk.SetValue("Width", this.Width.ToString());
			rk.SetValue("Height", this.Height.ToString());
		}

		private void frmRules_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if( myCurrEditColumn == 15)
			{
				if (e.KeyChar >= 48 && e.KeyChar <= 57)
				{;}
				else
					e.Handled = false;
			}
		}


		private void frmRules_Closed(object sender, System.EventArgs e)
		{
			DE.Call_RuleEditorClosed();
		}

		private void txtACTGroupValue_MouseHover(object sender, System.EventArgs e)
		{
		}

		private void txtAPTGroupValue_MouseHover(object sender, System.EventArgs e)
		{
		}

		private void txtALTGroupValue_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			string strUSGR = txtALTGroupUrno.Text;
			if(strUSGR != "")
			{
				ITable myTab = myDB["SGM"];
				IRow [] rows = myTab.RowsByIndexValue("USGR", strUSGR);
				if(rows.Length > 0)
				{
					string strTip="";
					ITable myRefTab = myDB["ALT"];
					int cnt = 0;
					for(int i = 0; i < rows.Length; i++)
					{
						IRow [] rowsACT = myRefTab.RowsByIndexValue("URNO", rows[i]["UVAL"]);
						if(rowsACT.Length > 0)
						{
							if((cnt % 25 == 0) && cnt != 0)
							{
								strTip += "\n";
							}
							strTip += rowsACT[0]["ALC2"]+"-"+rowsACT[0]["ALC3"]+",";
							cnt++;
						}
					}
					if(strTip != "")
						toolTip1.SetToolTip(txtALTGroupValue, strTip);
					else
						toolTip1.SetToolTip(txtALTGroupValue, "<None>");
				}
				else 
					toolTip1.SetToolTip(txtALTGroupValue, "<None>");
			}
		}

		private void txtACTGroupValue_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			string strUSGR = txtACTGroupUrno.Text;
			if(strUSGR != "")
			{
				ITable myTab = myDB["SGM"];
				IRow [] rows = myTab.RowsByIndexValue("USGR", strUSGR);
				if(rows.Length > 0)
				{
					string strTip="";
					ITable myACT = myDB["ACT"];
					int cnt = 0;
					for(int i = 0; i < rows.Length; i++)
					{
						IRow [] rowsACT = myACT.RowsByIndexValue("URNO", rows[i]["UVAL"]);
						if(rowsACT.Length > 0)
						{
							if((cnt % 25 == 0) && cnt != 0)
							{
								strTip += "\n";
							}
							strTip += rowsACT[0]["ACT3"]+",";
							cnt++;
						}
					}
					if(strTip != "")
						toolTip1.SetToolTip(txtACTGroupValue, strTip);
					else
						toolTip1.SetToolTip(txtACTGroupValue, "<None>");
				}
				else 
					toolTip1.SetToolTip(txtACTGroupValue, "<None>");
			}
		}

		private void txtAPTGroupValue_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			string strUSGR = txtAPTGroupUrno.Text;
			if(strUSGR != "")
			{
				ITable myTab = myDB["SGM"];
				IRow [] rows = myTab.RowsByIndexValue("USGR", strUSGR);
				if(rows.Length > 0)
				{
					string strTip="";
					ITable myRefTab = myDB["APT"];
					int cnt = 0;
					for(int i = 0; i < rows.Length; i++)
					{
						IRow [] rowsACT = myRefTab.RowsByIndexValue("URNO", rows[i]["UVAL"]);
						if(rowsACT.Length > 0)
						{
							if((cnt % 25 == 0) && cnt != 0)
							{
								strTip += "\n";
							}
							strTip += rowsACT[0]["APC3"]+",";
							cnt++;
						}
					}
					if(strTip != "")
						toolTip1.SetToolTip(txtAPTGroupValue, strTip);
					else
						toolTip1.SetToolTip(txtAPTGroupValue, "<None>");
				}
				else 
					toolTip1.SetToolTip(txtAPTGroupValue, "<None>");
			}
		}

		private void txtNATGroupValue_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			string strUSGR = txtNATGroupUrno.Text;
			if(strUSGR != "")
			{
				ITable myTab = myDB["SGM"];
				IRow [] rows = myTab.RowsByIndexValue("USGR", strUSGR);
				if(rows.Length > 0)
				{
					string strTip="";
					ITable myRefTab = myDB["NAT"];
					int cnt = 0;
					for(int i = 0; i < rows.Length; i++)
					{
						IRow [] rowsACT = myRefTab.RowsByIndexValue("URNO", rows[i]["UVAL"]);
						if(rowsACT.Length > 0)
						{
							if((cnt % 25 == 0) && cnt != 0)
							{
								strTip += "\n";
							}
							strTip += rowsACT[0]["TNAM"]+",";
							cnt++;
						}
					}
					if(strTip != "")
						toolTip1.SetToolTip(txtNATGroupValue, strTip);
					else
						toolTip1.SetToolTip(txtNATGroupValue, "<None>");
				}
				else 
					toolTip1.SetToolTip(txtNATGroupValue, "<None>");
			}
		}

		private void btnRecalcFlights_Click(object sender, System.EventArgs e)
		{
			frmRecalculateRules olDlg = new frmRecalculateRules();
			if(olDlg.ShowDialog(this) == DialogResult.OK)
			{
				//Call the Server is done from the dialog itself !!
			}
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void dtValidFrom_ValueChanged(object sender, System.EventArgs e)
		{
			CheckChanges();
		}

		private void dtValidTo_ValueChanged(object sender, System.EventArgs e)
		{
			CheckChanges();
		}

		private void txtMAXT_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if ((e.KeyChar >= 48 && e.KeyChar <= 57) || e.KeyChar == 8)
			{;}
			else
				e.Handled = true;
		}

		private void txtMAXT_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
//			if (e.KeyCode >= Keys.D0 && e.KeyCode <= Keys.D9 )
//			{;}
//			else
//				e.Handled = true;
		}

        Hashtable  htInterface = null;
        /// <summary>
        /// Check the Conflict of Setting for "Other" OTHS,OTHE,OTHC
        /// </summary>
        /// <param name="stTaskName"></param>
        /// <param name="stRety"></param>
        /// <param name="stFldr"></param>
        /// <param name="iLineNo">Start from 1</param>
        /// <returns></returns>
        private string CheckConflict(string stTaskName, string stRety, string stFldr, int iLineNo)
        {
            string errMsg = "";

            if (iLineNo < 1)
            {
                errMsg += "Invalid Line No. for Checking Conflict.";
            }
            else if (iLineNo == 1)
            {
                htInterface = new Hashtable();
            }

            string stlIName = stTaskName.Trim().ToUpper();
            string stlRety = stRety.Trim().ToUpper();
            string stlFldr = stFldr.Trim().ToUpper();
            string stSeperator = "|";

            if ((stlRety.Substring(0, 3) == "OTH") || (stlFldr.Substring(0, 2) == "OT"))
            {
                if ((stlRety.Substring(0, 3) == "OTH") && (stlFldr.Substring(0, 2) == "OT")){
                    if ((stlRety.Length > 3) && (stlFldr.Length > 3))
                    {
                        if (stlRety[3] != stlFldr[3])
                        {//Last character (S/E/C) is not same.
                            //S-Start. E-End. C-Check List
                            errMsg += "Mismatch Interface Setting.";
                        }
                    }
                }
                if (errMsg == "")
                {
                    string stKey = stlIName + stSeperator + stlRety + stSeperator + stlFldr;
                    if (htInterface.Contains(stKey))
                    {
                        errMsg += "Duplicate Entry at Line " + (int) htInterface[stKey];
                    }
                    else
                    {
                        htInterface.Add(stKey, iLineNo);
                    }
                }
            }
            return errMsg;
        }//End of Class CheckConflict

        private bool IsValidOthSetting(string stFldr, string stRety, int i , out string msg)
        {
            msg = "";

            if (rbArrival.Checked)
            {
                if (stFldr.Substring(0, 3) == "OTD")
                {
                    msg += "Grid Line " + (i + 1).ToString() + ": Invalid Interface Setting. It must be Arrival.";
                }
            }
            else if (rbDeparture.Checked)
            {
                if (stFldr.Substring(0, 3) == "OTA") 
                {
                    msg += "Grid Line " + (i + 1).ToString() + ": Invalid Interface Setting. It must be Arrival.";
                }
            }

            if ((stFldr == "OTAC") || (stFldr == "OTDC") || (stRety == "OTHC"))
            {
                if (stRety == "OTHC")
                {
                    if (!((stFldr == "OTAC") || (stFldr == "OTDC")))
                    {
                        msg += "Grid Line " + (i + 1).ToString() + ": Interface Check List must be same!\n";
                    }
                }
                else
                {
                    msg += "Grid Line " + (i + 1).ToString() + ": Interface Check List must be same!\n";
                }
            }

            if (msg == "") return true;
            else return false;
        }

        private void cbShowDeleted_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                SetSDETab(_curSrhUrno);
            }
            catch (Exception)
            {
            }
        }

}
}
