using System;
using System.Collections.Generic;
using System.Text;

using System.Collections;

using Ufis.Data;
using Ufis.Utils;

namespace Status_Manager.Ent
{
    public class EntFlightUrnoInfo
    {
        private DateTime _FromCedaDateTime;
        private DateTime _ToCedaDateTime;
        private ArrayList _uaftArr = null;

        private static object _lock = new object();
        private static bool _isAftArrLoaded = false;

        public bool AftArrLoaded
        {
            get
            {
                return _isAftArrLoaded;
            }
            set
            {
                _isAftArrLoaded = value;
            }
        }

        public void Init()
        {
            lock (_lock)
            {
                _uaftArr = null;
                _isAftArrLoaded = false;
                CreateUaftArrStForSelection();
            }
        }


        /// <summary>
        /// UAFT String Array for selection 
        /// Comma seperated UAFT String Array
        /// For example
        ///     _uaftArr[0] ==> '614235','346456','345643'
        ///     _uaftArr[1] ==> '454235','766456','135643'
        /// </summary>
        public ArrayList UAftArrStForSelection
        {
            get
            {
                if (_uaftArr == null)
                {
                    CreateUaftArrStForSelection();
                }
                return _uaftArr;
            }
        }

        private bool IsNeedToUpdateData()
        {
            bool need = false;
            if ((_FromCedaDateTime != UT.TimeFrameFrom) || (_ToCedaDateTime != UT.TimeFrameTo))
            {
                need = true;
            }
            return need;
        }

        /// <summary>
        /// Create UAFT String Array for use in selection
        /// It will create the Comma seperated UAFT String
        /// For example
        ///     _uaftArr[0] ==> '614235','346456','345643'
        ///     _uaftArr[1] ==> '454235','766456','135643'
        /// </summary>
        private void CreateUaftArrStForSelection()
        {
            lock (_lock)
            {
                if (!AftArrLoaded)
                {
                    IDatabase myDB = UT.GetMemDB();
                    ITable myAftTable = myDB["AFT"];
                    StringBuilder sb = new StringBuilder();
                    if (_uaftArr == null) _uaftArr = new ArrayList();
                    else _uaftArr.Clear();

                    int cnt = 0, cntAft = myAftTable.Count;
                    for (int i = 0; i < cntAft; i++)
                    {
                        string strAftUrno = myAftTable[i]["URNO"];

                        if (cnt == 200)
                        {
                            _uaftArr.Add(sb.ToString());
                            sb = null;
                            sb = new StringBuilder();
                            cnt = 0;
                        }

                        cnt++;

                        if (cnt == 1) sb.Append("'" + strAftUrno + "'");
                        else sb.Append(",'" + strAftUrno + "'");
                    }
                    if (cnt != 0)
                    {
                        _uaftArr.Add(sb.ToString());
                        sb = null;
                    }
                    _FromCedaDateTime = UT.TimeFrameFrom;
                    _ToCedaDateTime = UT.TimeFrameTo;
                }
                AftArrLoaded = true;
            }
        }
    }
}
