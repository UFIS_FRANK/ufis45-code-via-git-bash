using System;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using Ufis.Data;
using Ufis.Utils;
using System.Drawing;

namespace Status_Manager
{
	public class rptRules : ActiveReport
	{
		AxTABLib.AxTAB myTab = null;
		private int currLine = 0;
		private IDatabase myDB = null;
		public rptRules(AxTABLib.AxTAB pTab)
		{
			myTab = pTab;
			InitializeReport();
		}

		private void rptRules_FetchData(object sender, DataDynamics.ActiveReports.ActiveReport.FetchEventArgs eArgs)
		{
			string strURNO = myTab.GetFieldValue(currLine, "URNO");
			txtName.Text	= " " + myTab.GetFieldValue(currLine, "NAME");
			txtSection.Text = " " + myTab.GetFieldValue(currLine, "UHSS");
			txtFlight.Text	= " " + myTab.GetFieldValue(currLine, "ADID");
			
			ITable tabVal = myDB["VAL"];
			IRow [] rowsVal = tabVal.RowsByIndexValue("UVAL", strURNO);
			txtValidFrom.Text = "";
			txtValidTo.Text = "";
			if(rowsVal.Length > 0)
			{
				DateTime datFrom = UT.CedaFullDateToDateTime(rowsVal[0]["VAFR"]);
				DateTime datTo   = UT.CedaFullDateToDateTime(rowsVal[0]["VATO"]);
				txtValidFrom.Text = " " + datFrom.ToShortDateString();
				txtValidTo.Text = " " + datTo.ToShortDateString();
			}

			if(currLine == myTab.GetLineCount())
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
				currLine++;
			}
			sep0.Y1 = 0;
			sep1.Y1 = 0; 
			sep2.Y1 =  0;
			sep3.Y1 =  0;
			sep4.Y1 =  0;
			sep5.Y1 =  0;
			sep0.Y2 = txtName.Height;
			sep1.Y2 = txtName.Height; 
			sep2.Y2 =  txtName.Height;
			sep3.Y2 =  txtName.Height;
			sep4.Y2 =  txtName.Height;
			sep5.Y2 =  txtName.Height;
		}

		private void rptRules_ReportStart(object sender, System.EventArgs eArgs)
		{
			DateTime d = DateTime.Now;
			txtDate.Text = d.ToString();
			myDB = UT.GetMemDB();
			//*** Read the logo if exists and set it
			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			string strPrintLogoPath = myIni.IniReadValue("STATUSMANAGER", "PRINTLOGO");
			System.Drawing.Image myImage = null;
			try
			{
				if(strPrintLogoPath != "")
				{
					myImage = new Bitmap(strPrintLogoPath);
				}
			}
			catch(Exception)
			{
			}
			if(myImage != null)
				Picture1.Image = myImage;
			//*** END Read the logo if exists and set it
		}

		#region ActiveReports Designer generated code
		private DataDynamics.ActiveReports.PageHeader PageHeader;
		private DataDynamics.ActiveReports.Label Label9;
		private DataDynamics.ActiveReports.Label Label8;
		private DataDynamics.ActiveReports.Label Label3;
		private DataDynamics.ActiveReports.Label Label4;
		private DataDynamics.ActiveReports.Line Line1;
		private DataDynamics.ActiveReports.Line Line2;
		private DataDynamics.ActiveReports.Label Label2;
		private DataDynamics.ActiveReports.Line Line3;
		private DataDynamics.ActiveReports.Line Line4;
		private DataDynamics.ActiveReports.Line Line5;
		private DataDynamics.ActiveReports.Line Line6;
		private DataDynamics.ActiveReports.Line Line8;
		private DataDynamics.ActiveReports.TextBox TextBox3;
		private DataDynamics.ActiveReports.Line Line10;
		private DataDynamics.ActiveReports.Line Line11;
		private DataDynamics.ActiveReports.Picture Picture1;
		private DataDynamics.ActiveReports.Detail Detail;
		private DataDynamics.ActiveReports.TextBox txtValidTo;
		private DataDynamics.ActiveReports.TextBox txtValidFrom;
		private DataDynamics.ActiveReports.TextBox txtName;
		private DataDynamics.ActiveReports.TextBox txtSection;
		private DataDynamics.ActiveReports.TextBox txtFlight;
		private DataDynamics.ActiveReports.Line sep1;
		private DataDynamics.ActiveReports.Line sep2;
		private DataDynamics.ActiveReports.Line sep3;
		private DataDynamics.ActiveReports.Line Line7;
		private DataDynamics.ActiveReports.Line sep0;
		private DataDynamics.ActiveReports.Line sep4;
		private DataDynamics.ActiveReports.Line sep5;
		private DataDynamics.ActiveReports.PageFooter PageFooter;
		private DataDynamics.ActiveReports.TextBox TextBox1;
		private DataDynamics.ActiveReports.Label Label5;
		private DataDynamics.ActiveReports.TextBox TextBox2;
		private DataDynamics.ActiveReports.Label Label6;
		private DataDynamics.ActiveReports.Label Label7;
		private DataDynamics.ActiveReports.TextBox txtDate;
		public void InitializeReport()
		{
			this.LoadLayout(this.GetType(), "Status_Manager.rptRules.rpx");
			this.PageHeader = ((DataDynamics.ActiveReports.PageHeader)(this.Sections["PageHeader"]));
			this.Detail = ((DataDynamics.ActiveReports.Detail)(this.Sections["Detail"]));
			this.PageFooter = ((DataDynamics.ActiveReports.PageFooter)(this.Sections["PageFooter"]));
			this.Label9 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[0]));
			this.Label8 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[1]));
			this.Label3 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[2]));
			this.Label4 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[3]));
			this.Line1 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[4]));
			this.Line2 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[5]));
			this.Label2 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[6]));
			this.Line3 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[7]));
			this.Line4 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[8]));
			this.Line5 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[9]));
			this.Line6 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[10]));
			this.Line8 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[11]));
			this.TextBox3 = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[12]));
			this.Line10 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[13]));
			this.Line11 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[14]));
			this.Picture1 = ((DataDynamics.ActiveReports.Picture)(this.PageHeader.Controls[15]));
			this.txtValidTo = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[0]));
			this.txtValidFrom = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[1]));
			this.txtName = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[2]));
			this.txtSection = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[3]));
			this.txtFlight = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[4]));
			this.sep1 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[5]));
			this.sep2 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[6]));
			this.sep3 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[7]));
			this.Line7 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[8]));
			this.sep0 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[9]));
			this.sep4 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[10]));
			this.sep5 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[11]));
			this.TextBox1 = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[0]));
			this.Label5 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[1]));
			this.TextBox2 = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[2]));
			this.Label6 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[3]));
			this.Label7 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[4]));
			this.txtDate = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[5]));
			// Attach Report Events
			this.FetchData += new DataDynamics.ActiveReports.ActiveReport.FetchEventHandler(this.rptRules_FetchData);
			this.ReportStart += new System.EventHandler(this.rptRules_ReportStart);
		}

		#endregion
	}
}
