using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Status_Manager.UI
{
    public partial class FrmSelJobTemplate : Form
    {
        public delegate void JobTemplateSelected(string selectedId);
        public event JobTemplateSelected OnJobTemplateSelected;

        public delegate void JobTemplateSelectionCancelled();
        public event JobTemplateSelectionCancelled OnJobTemplateSelectionCancelled; 

        public FrmSelJobTemplate()
        {
            InitializeComponent();
        }

        public bool VisibleCancelButton
        {
            get { return btnCancel.Visible; }
            set { btnCancel.Visible = value; }
        }

        private bool _mustSelect = true;

        public bool MustSelectJobTemplate
        {
            get { return _mustSelect; }
            set { _mustSelect = value; }
        }

        //public string SelectedJobTemplateId()
        //{
        //    return ucJobTemplate1.GetSelectedJobTemplateId();
        //}

        public string SelectedJobTemplateId
        {
            get
            {
                return ucJobTemplate1.SelectedJobTemplateId;
            }
            set
            {
                ucJobTemplate1.LoadData();
                ucJobTemplate1.SelectedJobTemplateId = value;
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            string id = ucJobTemplate1.SelectedJobTemplateId;
            if ((id == null) || (id == ""))
            {
                if (MustSelectJobTemplate)
                {
                    MessageBox.Show("Please select a Job Template");
                }
            }
            else
            {
                JobTplSelected(id);
            }
        }

        private void JobTplSelected(string id)
        {
            this.Visible = false;
            if (OnJobTemplateSelected != null) OnJobTemplateSelected(id);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            if (OnJobTemplateSelectionCancelled != null)
                OnJobTemplateSelectionCancelled();
        }



        private void FrmSelJobTemplate_Load(object sender, EventArgs e)
        {
            //int x = this.Left;
            //int y = this.Top;
            ucJobTemplate1.OnJobTemplateSelected += new UCJobTemplate.JobTemplateSelected(ucJobTemplate1_OnJobTemplateSelected);
        }

        void ucJobTemplate1_OnJobTemplateSelected(string selectedId)
        {
            JobTplSelected(selectedId);
        }


   }
}