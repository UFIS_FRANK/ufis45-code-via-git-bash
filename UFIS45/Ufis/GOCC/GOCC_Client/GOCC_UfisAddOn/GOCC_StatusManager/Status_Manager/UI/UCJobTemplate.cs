using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using Status_Manager.Ctrl;

namespace Status_Manager.UI
{
    public partial class UCJobTemplate : UserControl
    {
        public delegate void JobTemplateSelected(string selectedId);
        public event JobTemplateSelected OnJobTemplateSelected;

        private bool _loadedData = false;
        private object _lockObj = new object();

        public UCJobTemplate()
        {
            InitializeComponent();
        }

        private void lbJobTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {
            string st = e.ToString();
        }

        private void UCJobTemplate_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        public void LoadData()
        {
            lock (_lockObj)
            {
                if (!_loadedData)
                {
                    DataView dv = CtrlJob.GetInstance().RetrieveJobTemplate().TBL.DefaultView;
                    dv.Sort = "VAL";

                    lbJobTemplate.DisplayMember = "VAL";
                    lbJobTemplate.ValueMember = "ID";
                    lbJobTemplate.DataSource = dv;
                    _loadedData = true;
                }
            }
        }

        //public Array GetSelectedJobTemplateIds()
        //{
        //    Array arr;
            
        //    lbJobTemplate.SelectedItems.CopyTo(arr, 0);
        //    return arr;
        //}

        public string GetSelectedJobTemplateId()
        {
            return lbJobTemplate.SelectedValue.ToString();
        }

        public string SelectedJobTemplateId
        {
            get
            {
                object obj = lbJobTemplate.SelectedValue;
                string id = "";
                if (obj!=null) id = obj.ToString();
                return id;
            }
            set
            {
                
                int cnt = lbJobTemplate.Items.Count;
                if (cnt > 0)
                {
                    bool found = false;
                    for (int i = 0; i < cnt; i++)
                    {
                        string st = ((DataRowView)lbJobTemplate.Items[i]).Row["ID"].ToString();
                        if (st == value)
                        {
                            found = true;
                            lbJobTemplate.SelectedIndex = i;
                            break;
                        }
                    }
                    if (!found) lbJobTemplate.SelectedIndex = 0;
                }
                //if (lbJobTemplate.SetSelected(0,.Contains(value))
                //{
                //    lbJobTemplate.SelectedValue = value;
                //}
            }
        }

        private void lbJobTemplate_DoubleClick(object sender, EventArgs e)
        {
            if (OnJobTemplateSelected != null)
                OnJobTemplateSelected(SelectedJobTemplateId);
        }


    }
}
