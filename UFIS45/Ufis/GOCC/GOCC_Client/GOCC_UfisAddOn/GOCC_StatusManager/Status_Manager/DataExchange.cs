using System;
using Ufis.Data;
using Ufis.Utils;

using System.Threading;

namespace Status_Manager
{
	/// <summary>
	/// Author: MWO 05.12.2003
	/// 
	/// Summary description for DataExchange.
	/// The class DE is supposed to be the one and only central point for
	/// application's delegates. It contains all necessary delegate definitions
	/// for this application. Moreover is has worker methods, which must be
	/// called to initiate a delegate event to update classes, which are
	/// registered for the respective delegate. The worker methods can call
	/// a number of delegates depending on the context (application's logic/requirements).
	/// This class may not be is not instantiated.
	/// All Members are static and can be called directly by e.g. "DE.DoSomthing()"
	/// 
	/// IMPORTANT: 
	/// ==========
	/// When the application starts, the static Method
	///  --- InitDBObjects(Ufis.Utils.BCBlinker blinker) --- must be called to 
	///  trigger the BCBlinker and to init the ITable, IDatabase objects for furhter usage
	/// </summary>
    public class DE
    {
        #region --- MyMembers

        static IDatabase myDB = null;
        static ITable aftTAB = null;
        static ITable aftTMP = null;
        static ITable ostTAB = null;
        static BCBlinker myBCBlinker = null;
        static ITable _jobTab = null;

        #endregion -- MyMembers

        #region --- Delegate definitions
        /// <summary>
        /// Status overview window has been closed
        /// </summary>
        public delegate void StatusOverviewClosed();
        public static event StatusOverviewClosed OnStatusOverviewClosed;
        /// <summary>
        /// frmDetailStatus must be opened.
        /// </summary>
        public delegate void OpenDetailStatusDlg(string strAftURNO, string strAftRkey, string strOstUrno);
        public static event OpenDetailStatusDlg OnOpenDetailStatusDlg;
        /// <summary>
        /// frmDetailStatus was closed
        /// </summary>
        public delegate void StatusDetailClosed();
        public static event StatusDetailClosed OnStatusDetailClosed;
        /// <summary>
        /// Timer for conflicts (and updates for the decreasing the minutes of statuses)
        /// has elapsed.
        /// </summary>
        public delegate void ConflicTimerElapsed();
        public static event ConflicTimerElapsed OnConflicTimerElapsed;
        /// <summary>
        /// frmRules was closed
        /// </summary>
        public delegate void RuleEditorClosed();
        public static event RuleEditorClosed OnRuleEditorClosed;
        /// <summary>
        /// The Birdview has been saved
        /// </summary>
        public delegate void BirdViewSaved();
        public static event BirdViewSaved OnBirdViewSaved;
        /// <summary>
        /// An online status has been changed. The sender is necessary to ensure that the originator has
        /// the chance to identify, whether he was the originator to suppress e.g. an unnecessary update.
        /// </summary>
        public delegate void OST_Changed(object sender, string strOstUrno, State state);
        public static event OST_Changed OnOST_Changed;
        /// <summary>
        /// An online status has been changed for a flight. The sender is necessary to ensure that the originator has
        /// the chance to identify, whether he was the originator to suppress e.g. an unnecessary update.
        /// </summary>
        public delegate void OST_ChangedForAFlight(object sender, string strAftUrno);//AM 20080428
        public static event OST_ChangedForAFlight OnOST_ChangedForAFlight;//AM 20080428
        /// <summary>
        /// An online status has been changed for a flight. The sender is necessary to ensure that the originator has
        /// the chance to identify, whether he was the originator to suppress e.g. an unnecessary update.
        /// </summary>
        public delegate void FlightStatus_Changed(object sender, string strAftUrno);//AM 20080514
        public static event FlightStatus_Changed OnFlightStatus_Changed;//AM 20080514
        /// <summary>
        /// To send the message that the find list has been closed
        /// </summary>
        public delegate void FindList_Closed();
        public static event FindList_Closed OnFindList_Closed;
        /// <summary>
        /// Send the message to navigate to the selected flight
        /// identified by urno of AFT
        /// </summary>
        public delegate void NavigateToFlight(object sender, string strAftUrno);
        public static event NavigateToFlight OnNavigateToFlight;
        /// <summary>
        /// Sends a delegate that flight was updated.
        /// </summary>
        public delegate void UpdateFlightData(object sender, string strAftUrno, State state);
        public static event UpdateFlightData OnUpdateFlightData;
        /// <summary>
        /// Sends a delegate that the current view has been changed.
        /// </summary>
        public delegate void ViewNameChanged();
        public static event ViewNameChanged OnViewNameChanged;
        /// <summary>
        /// Sends a delegate to That a UTC to LOCAL or LOCAL to UTC was performed
        /// </summary>
        public delegate void UTC_LocalTimeChanged();
        public static event UTC_LocalTimeChanged OnUTC_LocalTimeChanged;
        /// <summary>
        /// Sends a delegate that "CLO" message was sent by the server
        /// </summary>
        public delegate void CLO_Message();
        public static event CLO_Message OnCLO_Message;
        /// <summary>
        /// Sends a delegate that "CLO" message was sent by the server
        /// </summary>
        public delegate void ReloadCompleteOnlineStatuses();
        public static event ReloadCompleteOnlineStatuses OnReloadCompleteOnlineStatuses;

        public delegate void JobTemplateChanged(string newUtpl);
        public static event JobTemplateChanged OnJobTemplateChanged;

        /// <summary>
        /// Sends a delegate that JOB Information was changed for the JOB record with URNO=ujob for the flight uaft
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="uaft">URNO of AFTTAB</param>
        /// <param name="ujob">URNO of JOBTAB</param>
        public delegate void Job_Changed(object sender, string ujob, string orgUaft, string newUaft);
        public static event Job_Changed OnJob_Changed;

        #endregion --- Delegate definitions

        #region --- Delegate Worker Methods

        static public void Call_JobTemplateChanged(string newUtpl)
        {
            if (OnJobTemplateChanged != null)
                OnJobTemplateChanged(newUtpl);
        }

        static public void Call_JobChanged(object sender, string ujob, string orgUaft, string newUaft)
        {
            if (OnJob_Changed != null)
            {
                LogMsg("Call_JobChanged:b4");
                OnJob_Changed(sender, ujob, orgUaft, newUaft);
                LogMsg("Call_JobChanged:af");
            }
        }

        static public void Call_DetailStatusDlg(string strAftURNO, string strAftRkey, string strOstUrno)
        {
            if (OnOpenDetailStatusDlg != null)
                OnOpenDetailStatusDlg(strAftURNO, strAftRkey, strOstUrno);
        }

        static public void Call_StatusOverviewClosed()
        {
            if (OnStatusOverviewClosed != null)
                OnStatusOverviewClosed();
        }
        static public void Call_StatusDetailClosed()
        {
            if (OnStatusDetailClosed != null)
                OnStatusDetailClosed();
        }
        static public void Call_ConflicTimerElapsed()
        {
            if (OnConflicTimerElapsed != null)
                OnConflicTimerElapsed();
        }
        static public void Call_RuleEditorClosed()
        {
            if (OnRuleEditorClosed != null)
                OnRuleEditorClosed();
        }
        static public void Call_BirdViewSaved()
        {
            if (OnBirdViewSaved != null)
                OnBirdViewSaved();
        }
        static public void Call_OST_Changed(object sender, string strOstUrno, State state)
        {
            if (OnOST_Changed != null)
                OnOST_Changed(sender, strOstUrno, state);
        }
        static public void Call_FlightStatus_Changed(object sender, string strAftUrno)
        {//AM 20080514
            //if (OnFlightStatus_Changed != null)
            //{
            //    OnFlightStatus_Changed(sender, strAftUrno);
            //}
            Ufis.Utils.AsyncEvent.FireAsync(OnFlightStatus_Changed, new Object[] { sender, strAftUrno });
        }
        static public void Call_FindList_Closed()
        {
            if (OnFindList_Closed != null)
                OnFindList_Closed();
        }
        static public void Call_NavigateToFlight(object sender, string strAftUrno)
        {
            if (OnNavigateToFlight != null)
                OnNavigateToFlight(sender, strAftUrno);
        }
        static public void Call_ViewNameChanged()
        {
            if (OnViewNameChanged != null)
                OnViewNameChanged();
        }
        static public void Call_UTC_LocalTimeChanged(object sender)
        {
            if (OnUTC_LocalTimeChanged != null)
            {
                OnUTC_LocalTimeChanged();
            }
        }
        #endregion --- Worker Methods

        #region --- BroadCast Methods

        static public void InitDBObjects(Ufis.Utils.BCBlinker blinker)
        {
            myBCBlinker = blinker;
            myDB = UT.GetMemDB();
            aftTAB = myDB["AFT"];
            aftTMP = myDB["AFTTMP"];
            ostTAB = myDB["OST"];
            _jobTab = myDB["JOB"];

            aftTAB.OnUpdateRequestHandler = new UpdateRequestHandler(AFT_OnUpdateRequestHandler);
            ostTAB.OnUpdateRequestHandler = new UpdateRequestHandler(OST_OnUpdateRequestHandler);
            myDB.OnEventHandler += new DatabaseEventHandler(DatabaseEventHandler);
            myDB.OnTableEventHandler += new DatabaseTableEventHandler(TableEventHandler);

        }

        static bool IsExistInAftTab(string uaft)
        {//AM 20080430 - To check given urno is already existed in Memdb Afttab
            bool exist = false;
            try
            {
                if (aftTAB.RowsByIndexValue("URNO", uaft).Length > 0) exist = true;
            }
            catch (Exception ex)
            {
                UT.LogMsg("DataExchange:IsExistInAftTab:Err:" + ex.Message);
            }
            return exist;
        }

        static bool AFT_OnUpdateRequestHandler(string pReqId, string pDest1, string pDest2, string pCmd, string pObject, string pSeq, string pTws, string pTwe, string pSelection, string pFields, string pData, string pBcNum)
        {
            bool blRet = false;
            bool tifaInRange = false;
            bool tifdInRange = false;
            DateTime datTifa;
            DateTime datTifd;
            string uaft = pSelection;
            if (IsExistInAftTab(uaft)) blRet = true;//AM 20080430 - To include if this URNO is already in the table
            else
            {
                int idxTifa = UT.GetItemNo(pFields, "TIFA");
                int idxTifd = UT.GetItemNo(pFields, "TIFF");
                int idxFtyp = UT.GetItemNo(pFields, "FTYP");

                if (idxTifa != -1)
                {
                    datTifa = UT.CedaFullDateToDateTime(UT.GetItem(pData, idxTifa, ","));
                    if (datTifa <= UT.TimeFrameTo && datTifa >= UT.TimeFrameFrom)
                    {
                        tifaInRange = true;
                    }
                }
                if (idxTifd != -1)
                {
                    datTifd = UT.CedaFullDateToDateTime(UT.GetItem(pData, idxTifd, ","));
                    if (datTifd <= UT.TimeFrameTo && datTifd >= UT.TimeFrameFrom)
                    {
                        tifdInRange = true;
                    }
                }
                if (tifaInRange == true || tifdInRange == true)
                {
                    if (idxFtyp != -1)
                    {
                        string strFtyp = UT.GetItem(pData, idxFtyp, ",");
                        if (strFtyp == "O" || strFtyp == "S" | strFtyp == "X")
                        {
                            blRet = true;
                        }
                    }
                    else
                    {
                        blRet = true;
                    }
                }
            }

            return blRet;	// accept data to be worked into the AFT data in myDB["AFT"]
        }
        static bool OST_OnUpdateRequestHandler(string pReqId, string pDest1, string pDest2, string pCmd, string pObject, string pSeq, string pTws, string pTwe, string pSelection, string pFields, string pData, string pBcNum)
        {
            return true;	// accept data to be worked into the OST data in myDB["OST"]
        }
        /// <summary>
        ///Two types can be provided: 
        ///Command: 	SBC
        ///Table:		OST
        ///Field:		UAFT
        ///Data:		comma separated list of flight URNOs
        ///and
        ///Command: 	SBC
        ///Table:		OST
        ///Field:		SDAY
        ///Data:		SDAY1-SDAY2 8 stellig immer 2 tage auch wenn es nur einer ist
        ///			    dann eben 2 mal der selbe
        /// </summary>
        /// <param name="obj">the sender of this event</param>
        /// <param name="eventArgs">The Broadcast Data</param>
        static void DatabaseEventHandler(object obj, DatabaseEventArgs eventArgs)
        {
            myBCBlinker.Blink();
            if (eventArgs.command == "SBC")
            {
                switch (eventArgs.obj)
                {
                    case "OSTTAB":
                        if (eventArgs.fields == "UAFT")
                        {
                            //HandleStatusesForFlights(eventArgs.data);//AM 20080429 - Commented out to process as background thread
                            ThreadHandleStatusesForFlights(eventArgs.data);//AM 20080429
                        }
                        else if (eventArgs.fields == "SDAY")
                        {
                            string[] arrSdays = eventArgs.data.Split('-');
                            if (arrSdays.Length == 2)
                            {
                                DateTime dtFrom = UT.CedaDateToDateTime(arrSdays[0]);
                                DateTime dtTo = UT.CedaDateToDateTime(arrSdays[1]);
                                //IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))
                                if (UT.TimeFrameFrom <= dtTo && dtFrom <= UT.TimeFrameTo)
                                {
                                    if (OnReloadCompleteOnlineStatuses != null)
                                    {
                                        OnReloadCompleteOnlineStatuses();
                                    }
                                }
                            }
                            //HandleStatusesForTimeRange(eventArgs.data);
                        }
                        break;
                    default:
                        break;
                }
            }
            if (eventArgs.command == "ISF" && obj == aftTAB)
            {
                string strSkey = GetSkeyFromEvent(eventArgs);
                if (strSkey != "")
                {
                    HandleISF(strSkey);
                }
            }
            if (eventArgs.command == "CLO")
            {
                //CLO_Message 
                if (OnCLO_Message != null)
                {
                    OnCLO_Message();
                }
            }
        }
        /// <summary>
        /// Extracts the SKEY value from the event by examining fields and data
        /// </summary>
        /// <param name="e">The broadcast event.</param>
        /// <returns>The SKEY value if found otherwise "".</returns>
        static string GetSkeyFromEvent(DatabaseEventArgs e)
        {
            string strRet = "";
            int idx = -1;
            string[] fields = e.fields.Split(',');
            string[] data = e.data.Split(',');
            for (int i = 0; i < fields.Length; i++)
            {
                if (fields[i] == "SKEY")
                {
                    idx = i;
                    i = fields.Length + 1; //imlicit break
                }
            }
            if (idx > -1 && idx < data.Length)
            {
                strRet = data[idx];
                strRet = strRet.Trim();
            }
            return strRet;
        }
        /// <summary>
        /// Select the data from AFTTAB where skey = strSkey and within the current Timeframe.
        /// Inform the application main form about the new flight.
        /// </summary>
        /// <param name="strSkey">The skey.</param>
        static void HandleISF(string strSkey)
        {
            string strWhere = "WHERE SKEY=" + strSkey + " AND ((TIFA BETWEEN '" + UT.DateTimeToCeda(UT.TimeFrameFrom) + "' AND '" + UT.DateTimeToCeda(UT.TimeFrameTo) + "') OR (TIFD BETWEEN '" + UT.DateTimeToCeda(UT.TimeFrameFrom) + "' and '" + UT.DateTimeToCeda(UT.TimeFrameTo) + "') AND FTYP IN ('S', 'O', 'X'))";
            aftTMP.Clear();
            aftTMP.Load(strWhere);
            for (int i = 0; i < aftTMP.Count; i++)
            {
                IRow newRow = aftTAB.CreateEmptyRow();
                string urno = aftTMP[i]["URNO"];
                newRow = aftTMP[i].CopyRaw();
                newRow["URNO"] = urno;
                aftTAB.Add(newRow);
                if (OnUpdateFlightData != null)
                {
                    OnUpdateFlightData(aftTMP, newRow["URNO"], State.Unchanged);
                }
            }
        }
        static void TableEventHandler(object obj, DatabaseTableEventArgs eventArgs)
        {
            myBCBlinker.Blink();
            ITable myTable = myDB[eventArgs.table];
            if (myTable == aftTAB)
            {
                HandleAFTEvent(obj, eventArgs);
            }
            else if (myTable == ostTAB)
            {
                HandleOSTvent(obj, eventArgs);
            }
            else if (myTable ==_jobTab)
            {
                LogMsg("TableEventHandler:JobTab.");
                Ctrl.CtrlJob.GetInstance().JobChanged(obj, eventArgs);
            }
        }

        private static void LogMsg(string msg)
        {
            UT.LogMsg("DataExchange:" + msg);
        }

        static void HandleAFTEvent(object obj, DatabaseTableEventArgs eventArgs)
        {

            PreparePositionAllocationTimes(eventArgs.row["RKEY"]);

            if (aftTAB.Command("insert").IndexOf("," + eventArgs.command + ",") >= 0)
            {
                if (OnUpdateFlightData != null)
                {
                    OnUpdateFlightData(aftTAB, eventArgs.row["URNO"], State.Created);
                }
            }
            else if (aftTAB.Command("update").IndexOf("," + eventArgs.command + ",") >= 0)
            {
                if (OnUpdateFlightData != null)
                {
                    OnUpdateFlightData(aftTAB, eventArgs.row["URNO"], State.Modified);
                }
            }
            else if (aftTAB.Command("delete").IndexOf("," + eventArgs.command + ",") >= 0)
            {
                if (OnUpdateFlightData != null)
                {
                    OnUpdateFlightData(aftTAB, eventArgs.row["URNO"], State.Deleted);
                }
            }
        }
        /// <summary>
        /// Position allocation time must be calculated, this must be removed,
        /// when the final server solution is implemented.
        /// </summary>
        static void PreparePositionAllocationTimes(string strAftRkey)
        {
            IRow rowArrival = null;
            IRow rowDeparture = null;
            IRow[] rows = aftTAB.RowsByIndexValue("RKEY", strAftRkey);
            for (int i = 0; i < rows.Length; i++)
            {
                if (rows[i]["ADID"] == "A")
                {
                    rowArrival = rows[i];
                }
                if (rows[i]["ADID"] == "D")
                {
                    rowDeparture = rows[i];
                }
            }
            if (rowArrival != null && rowDeparture != null)
            {
                DateTime tifd = UT.CedaFullDateToDateTime(rowDeparture["TIFD"]);
                DateTime depFrom = tifd.Subtract(new TimeSpan(0, 1, 15, 0, 0));
                rowDeparture["PDBS"] = UT.DateTimeToCeda(depFrom);
                if (rowDeparture["TISD"] == "O" || rowDeparture["TISD"] == "A")
                {
                    rowDeparture["PDES"] = rowDeparture["TIFD"];
                }
                else
                {
                    DateTime depTo = DateTime.Now;
                    depTo = depTo.AddDays(5);
                    rowDeparture["PDES"] = UT.DateTimeToCeda(depTo);
                }

                rowArrival["PABS"] = rowArrival["TIFA"];
                rowArrival["PAES"] = rowDeparture["PDBS"];
            }
            else
            {
                if (rowArrival != null)
                {
                    if (rowArrival["TISA"] == "O")
                    {
                        DateTime arrTo = UT.CedaFullDateToDateTime(rowArrival["TIFA"]);
                        arrTo = arrTo.AddDays(2);
                        rowArrival["PABS"] = rowArrival["TIFA"];
                        rowArrival["PAES"] = UT.DateTimeToCeda(arrTo);
                    }
                }
            }

        }
        static void HandleOSTvent(object obj, DatabaseTableEventArgs eventArgs)
        {
            if (ostTAB.Command("insert").IndexOf("," + eventArgs.command + ",") >= 0)
            {
                if (OnOST_Changed != null)
                    OnOST_Changed(ostTAB, eventArgs.row["URNO"], State.Created);
                //if(OnUpdateFlightData != null)
                //{
                //    OnUpdateFlightData(aftTAB, eventArgs.row["UAFT"], State.Modified); 
                //}
                if (OnOST_ChangedForAFlight != null)
                {
                    OnOST_ChangedForAFlight(ostTAB, eventArgs.row["UAFT"]);
                }
            }
            else if (ostTAB.Command("update").IndexOf("," + eventArgs.command + ",") >= 0)
            {
                if (OnOST_Changed != null)
                    OnOST_Changed(ostTAB, eventArgs.row["URNO"], State.Modified);
                //if(OnUpdateFlightData != null)
                //{
                //    OnUpdateFlightData(aftTAB, eventArgs.row["UAFT"], State.Modified); 
                //}
                if (OnOST_ChangedForAFlight != null)
                {
                    OnOST_ChangedForAFlight(ostTAB, eventArgs.row["UAFT"]);
                }
            }
            else if (ostTAB.Command("delete").IndexOf("," + eventArgs.command + ",") >= 0)
            {
                if (OnOST_Changed != null)
                    OnOST_Changed(ostTAB, eventArgs.row["URNO"], State.Deleted);
                //if(OnUpdateFlightData != null)
                //{
                //    OnUpdateFlightData(aftTAB, eventArgs.row["UAFT"], State.Modified); 
                //}
                if (OnOST_ChangedForAFlight != null)
                {
                    OnOST_ChangedForAFlight(ostTAB, eventArgs.row["UAFT"]);
                }
            }
        }

        static void ThreadHandleStatusesForFlights(string strFlightUrnos)
        {
            UT.LogMsg("ThreadHandleStatusesForFlights:start:" + strFlightUrnos );
            Thread th = new Thread(delegate() { Status_Manager.DE.HandleStatusesForFlights(strFlightUrnos); });
            th.IsBackground = true;
            th.Start();
            UT.LogMsg("ThreadHandleStatusesForFlights:Finish:" + strFlightUrnos);
        }

        static object _lockHandleStatusForFlights = new object();

        static void HandleStatusesForFlights(string strFlightUrnos)
        {
            int i = 0;
            if (ostTAB == null) return;//AM 20080423
            
            try
            {
                string[] arrAFTUrnos = strFlightUrnos.Split(',');
                IRow[] ostRows;
                lock (_lockHandleStatusForFlights)
                {
                    Thread.Sleep(1000);//AM 20080429: Sleep for 1 second- to give time for other process
                    try
                    {
                        UT.LogMsg("HandleStatusesForFlights: B4 AcqLock");
                        ostTAB.Lock.AcquireWriterLock(System.Threading.Timeout.Infinite);
                        UT.LogMsg("HandleStatusesForFlights: Af AcqLock");
                        //Remove the rows from OST
                        strFlightUrnos = "";
                        for (i = 0; i < arrAFTUrnos.Length; i++)
                        {
                            ostRows = ostTAB.RowsByIndexValue("UAFT", arrAFTUrnos[i]);
                            UT.LogMsg("HandleStatusesForFlights:" + i + ", RowsCnt:" + ostRows.Length);
                            ostTAB.Remove(ostRows);//AM 20080429 - Remove All rows. Faster than 
                            //myDB.IndexUpdateImmediately = false;
                            //for (int j = ostRows.Length - 1; j >= 0; j--)
                            //{
                            //    ostTAB.Remove(ostRows[j]);
                            //    UT.LogMsg("HandleStatusesForFlights:" + i + "," + j);
                            //}
                            //myDB.IndexUpdateImmediately = true;
                            //ostTAB.ReorganizeIndexes();
                            UT.LogMsg("HandleStatusesForFlights:" + i + ", Finish");
                            strFlightUrnos += "'" + arrAFTUrnos[i] + "',";
                        }
                        UT.LogMsg("After remove from OSTTAB");
                        strFlightUrnos = strFlightUrnos.Substring(0, strFlightUrnos.Length - 1);
                        //Reload it
                        ostTAB.Load("WHERE UAFT in (" + strFlightUrnos + ")");
                        UT.LogMsg("After Load.");
                    }
                    catch (Exception ex)
                    {
                        UT.LogMsg("HandleStatusesForFlights:Err:" + ex.Message);
                    }
                    finally
                    {
                        UT.LogMsg("HandleStatusesForFlights:B4 rel wlock");
                        if ((ostTAB != null) && (ostTAB.Lock.IsWriterLockHeld)) ostTAB.Lock.ReleaseWriterLock();
                        UT.LogMsg("HandleStatusesForFlights:af rel wlock");
                    }
                }

                try
                {
                    UT.LogMsg("HandleStatusesForFlights:B4 acq Rlock");
                    if (ostTAB != null) ostTAB.Lock.AcquireReaderLock(System.Threading.Timeout.Infinite);
                    UT.LogMsg("HandleStatusesForFlights:AF acq Rlock");
                    for (i = 0; i < arrAFTUrnos.Length; i++)
                    {
                        ostRows = ostTAB.RowsByIndexValue("UAFT", arrAFTUrnos[i]);
                        if (OnOST_ChangedForAFlight != null)
                        {//AM 20080428
                            OnOST_ChangedForAFlight(ostTAB, arrAFTUrnos[i]);
                        }
                        for (int j = 0; j < ostRows.Length; j++)
                        {
                            if (OnOST_Changed != null)
                                OnOST_Changed(null, ostRows[j]["URNO"], ostRows[i].Status);
                        }
                    }
                }
                catch (Exception ex)
                {
                    UT.LogMsg("HandleStatusesForFlights:Acc:Err:" + ex.Message);
                }
                finally
                {
                    UT.LogMsg("HandleStatusesForFlights:B4 REL Rlock");
                    if ((ostTAB != null) && (ostTAB.Lock.IsReaderLockHeld)) ostTAB.Lock.ReleaseReaderLock();
                    UT.LogMsg("HandleStatusesForFlights:AF REL Rlock");
                }
                UT.LogMsg("After Events.");
                //Send changes into the application
                //			for( i = 0; i < arrAFTUrnos.Length; i++)
                //			{
                //				if(OnUpdateFlightData != null)
                //				{
                //					OnUpdateFlightData(aftTAB, arrAFTUrnos[i], State.Modified); 
                //				}
                //			}
            }
            catch (Exception ex)
            {
                UT.LogMsg("HandleStatusesForFlights:Err: " + ex.Message + Environment.NewLine + ex.StackTrace);
            }
            finally
            {
                UT.LogMsg("HandleStatusesForFlights: B4 RelLock");
                if ((ostTAB != null) && (ostTAB.Lock.IsWriterLockHeld)) ostTAB.Lock.ReleaseWriterLock();
                if ((ostTAB != null) && (ostTAB.Lock.IsReaderLockHeld)) ostTAB.Lock.ReleaseReaderLock();
                UT.LogMsg("HandleStatusesForFlights: Af RelLock");
            }
        }
        #endregion  BroadCast Methods
    }
}
