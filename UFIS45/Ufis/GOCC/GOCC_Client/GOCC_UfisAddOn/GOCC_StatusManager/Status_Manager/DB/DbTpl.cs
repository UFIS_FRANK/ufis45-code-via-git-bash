// Tpl Template Information 
//   Related to Database
// AUNG MOE : 20080718
//

using System;
using System.Collections.Generic;
using System.Text;

using System.Collections;

using Ufis.Data;
using Ufis.Utils;

using Status_Manager.Ent;
using Status_Manager.DS;

namespace Status_Manager.DB
{
    public class DbTpl
    {
        private static DbTpl _this = null;
        private static object _lockTplTableProp = new object();

        private IDatabase _myDB = null;
        private ITable _tplTable = null;

        private IDatabase MyDb
        {
            get
            {
                if (_myDB == null) _myDB = UT.GetMemDB();
                return _myDB;
            }
        }

        private ITable TplTable
        {
            get
            {
                _tplTable = MyDb["TPL"];
                if (_tplTable == null)
                {
                    lock (_lockTplTableProp)
                    {
                        _tplTable = MyDb["TPL"];//Get and check again
                        if (_tplTable == null)
                        {
                            _tplTable = MyDb.Bind("TPL", "TPL",
                                "URNO,TNAM",
                                "10,64",
                                "URNO,TNAM");
                            _tplTable.UseTrackChg = true;
                            _tplTable.TrackChgImmediately = false;//Not to track the changes until "StartTrackChg" for the particular row.
                        }
                    }
                }
                return _tplTable;
            }
            set
            {
                _tplTable = value;
            }
        }

        public ITable GetTplTable()
        {
            return TplTable;
        }

        private DbTpl()
        {
        }

        public static DbTpl GetInstance()
        {
            if (_this == null) _this = new DbTpl();
            return _this;
        }

        private static void LogMsg(string msg)
        {
            UT.LogMsg("DbTpl:" + msg);
        }

        public void LockTplTableForWrite()
        {
            LogMsg("B4 Lock Tpl Write");
            TplTable.Lock.AcquireWriterLock(System.Threading.Timeout.Infinite);
            LogMsg("AF Lock Tpl Write");
        }

        public void ReleaseTplTableWrite()
        {
            if (TplTable.Lock.IsWriterLockHeld)
            {
                LogMsg("B4 Release Lock Tpl Write");
                TplTable.Lock.ReleaseWriterLock();
                LogMsg("AF Release Lock Tpl Write");
            }
        }

        public void LockTplTableForRead()
        {
            LogMsg("B4 Lock Tpl Read");
            TplTable.Lock.AcquireReaderLock(System.Threading.Timeout.Infinite);
            LogMsg("AF Lock Tpl Read");
        }

        public void ReleaseTplTableRead()
        {
            if (TplTable.Lock.IsReaderLockHeld)
            {
                LogMsg("B4 Release Lock Tpl Read");
                TplTable.Lock.ReleaseReaderLock();
                LogMsg("AF Release Lock Tpl Read");
            }
        }

        private void AddTplInfoToDsTpl(IRow row, DSKeyVal dsTpl)
        {
            DSKeyVal.TBLRow dtRow = dsTpl.TBL.NewTBLRow();

            dtRow.ID = row["URNO"].ToString();
            dtRow.VAL = row["TNAM"].ToString();

            dsTpl.TBL.AddTBLRow(dtRow);
        }

        public void PopulateTplInfo(DSKeyVal dsTpl)
        {
            int cnt = TplTable.Count;
            for (int i = 0; i < cnt; i++)
            {
                AddTplInfoToDsTpl(TplTable[i], dsTpl);
            }
        }

        public string LoadTplData()
        {
            string result = "";

            //Select the TPLTAB records

            try
            {
                LockTplTableForWrite();
                DeleteTplIndex();
                TplTable.Clear();

                LogMsg("LoadTPLData: Start");
                TplTable.TimeFieldsInitiallyInUtc = true;
                TplTable.TimeFieldsCurrentlyInUtc = true;

                LogMsg("LoadTPLData: b4 load");
                TplTable.Load(" WHERE APPL='RULE_AFT' ");
                TplTable.Sort("TNAM", true);
                CreateTplIndex();

                TplTable.Command("insert", ",IRT,");
                TplTable.Command("update", ",URT,");
                TplTable.Command("delete", ",DRT,");
                TplTable.Command("read", ",GFR,");

                result = "TPLTAB " + TplTable.Count + " Records loaded";

                LogMsg("TPLTAB <" + TplTable.Count + ">");
                return result;
            }
            catch (Exception ex)
            {
                LogMsg("LoadTPLData:Err:" + ex.Message);
            }
            finally
            {
                ReleaseTplTableWrite();
            }
            return result;
        }

        private void CreateTplIndex()
        {
            TplTable.CreateIndex("URNO", "URNO");
            TplTable.CreateIndex("TNAM", "TNAM");
        }

        private void DeleteTplIndex()
        {
            try
            {
                TplTable.DeleteIndex("URNO");
                TplTable.DeleteIndex("TNAM");
            }
            catch (Exception)
            {
            }
        }

    }
}
