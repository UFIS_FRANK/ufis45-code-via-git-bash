// STF Information 
//   Related to Database
// AUNG MOE : 20080718
//

using System;
using System.Collections.Generic;
using System.Text;

using System.Collections;

using Ufis.Data;
using Ufis.Utils;

using Status_Manager.Ent;
using Status_Manager.DS;

namespace Status_Manager.DB
{
    public class DbStf
    {
        #region data member
        private static DbStf _this = null;
        private static object _lockStfTableProp = new object();

        private IDatabase _myDB = null;
        private ITable _stfTable = null;
        #endregion

        private IDatabase MyDb
        {
            get
            {
                if (_myDB == null) _myDB = UT.GetMemDB();
                return _myDB;
            }
        }

        private ITable StfTable
        {
            get
            {
                _stfTable = MyDb["STF"];
                if (_stfTable == null)
                {
                    lock (_lockStfTableProp)
                    {
                        _stfTable = MyDb["STF"];//Get and check again
                        if (_stfTable == null)
                        {
                            _stfTable = MyDb.Bind("STF", "STF",
                                "URNO,PENO,FINM,LANM",
                                "10,20,40,40",
                                "URNO,PENO,FINM,LANM");
                            //_stfTable.UseTrackChg = true;
                            //_stfTable.TrackChgImmediately = false;//Not to track the changes until "StartTrackChg" for the particular row.
                        }
                    }
                }
                return _stfTable;
            }
        }

        public ITable GetStfTable()
        {
            return StfTable;
        }

        #region Singleton
        private DbStf()
        {
        }

        public static DbStf GetInstance()
        {
            if (_this == null) _this = new DbStf();
            return _this;
        }
        #endregion
        
        private static void LogMsg(string msg)
        {
            UT.LogMsg("DbStf:" + msg);
        }

        #region Lock and Release Table
        public void LockStfTableForWrite()
        {
            LogMsg("B4 Lock Stf Write");
            StfTable.Lock.AcquireWriterLock(System.Threading.Timeout.Infinite);
            LogMsg("AF Lock Stf Write");
        }

        public void ReleaseStfTableWrite()
        {
            if (StfTable.Lock.IsWriterLockHeld)
            {
                LogMsg("B4 Release Lock Stf Write");
                StfTable.Lock.ReleaseWriterLock();
                LogMsg("AF Release Lock Stf Write");
            }
        }

        public void LockStfTableForRead()
        {
            LogMsg("B4 Lock Stf Read");
            StfTable.Lock.AcquireReaderLock(System.Threading.Timeout.Infinite);
            LogMsg("AF Lock Stf Read");
        }

        public void ReleaseStfTableRead()
        {
            if (StfTable.Lock.IsReaderLockHeld)
            {
                LogMsg("B4 Release Lock Stf Read");
                StfTable.Lock.ReleaseReaderLock();
                LogMsg("AF Release Lock Stf Read");
            }
        }
        #endregion


        private void AddDataToDsJob(IRow row, DSJob dsJob)
        {
            DSJob.STFRow dtRow = dsJob.STF.NewSTFRow();

            dtRow.URNO = row["URNO"].ToString();
            dtRow.PENO = row["PENO"].ToString();
            dtRow.FINM = row["FINM"].ToString();
            dtRow.LANM = row["LANM"].ToString();
            dsJob.STF.AddSTFRow(dtRow);
        }


        /// <summary>
        /// Get Stf Information and load them into passing dsJob
        /// </summary>
        /// <param name="dsJob">load the relevent stf info into this dataset</param>
        /// <param name="ustfArr">USTF array to load</param>
        public void GetStfInfo(DSJob dsJob, string[] ustfArr)
        {
            dsJob.STF.Clear();
            int cntArr = ustfArr.Length;
            if (cntArr > 0)
            {

                try
                {
                    LockStfTableForRead();
                    string[] missingUstfArr = new string[ustfArr.Length];
                    int cntMissing = 0;

                    for (int idxUstf = 0; idxUstf < cntArr; idxUstf++)
                    {
                        string ustf = ustfArr[idxUstf];
                        IRow[] rows = StfTable.RowsByIndexValue("URNO", ustf);
                        if ((rows == null) || (rows.Length < 1))
                        {
                            missingUstfArr[cntMissing] = ustf;
                            cntMissing++;
                        }
                        else
                        {
                            AddDataToDsJob(rows[0], dsJob);
                        }
                    }

                    if (cntMissing > 0)
                    {
                        AddStfData("'" + string.Join("','", missingUstfArr) + "'");
                        for (int i = 0; i < cntMissing; i++)
                        {
                            string ustf = missingUstfArr[i];
                            IRow[] rows = StfTable.RowsByIndexValue("URNO", ustf);
                            if ((rows == null) || (rows.Length < 1))
                            {
                            }
                            else
                            {
                                AddDataToDsJob(rows[0], dsJob);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    ReleaseStfTableRead();
                }
            }
        }


        private void AddStfData(string ustfSt)
        {
            StfTable.Load(string.Format(" WHERE URNO IN ({0})", ustfSt));
        }


        /// <summary>
        /// Load STFTAB Data
        /// Note: Existing In memory Data in STFTAB will be cleared
        /// </summary>
        /// <param name="ustfArrStForSelection">Comma seperated USTF string array (e.g. Arr[0]==>65132523,65132556,65132745 Arr[0]==>6554324523,651345256,651387685 </param>
        /// <returns>result message</returns>
        public string LoadStfData(ArrayList ustfArrStForSelection)
        {
            string result = "";

            //Select the STFTAB records according to the flight

            try
            {
                LockStfTableForWrite();
                DeleteStfIndex();
                StfTable.Clear();

                LogMsg("LoadSTFData: Start");
                StfTable.TimeFieldsInitiallyInUtc = true;
                StfTable.TimeFieldsCurrentlyInUtc = true;

                LogMsg("LoadSTFData: b4 load");
                int cntArr = ustfArrStForSelection.Count;
                for (int i = 0; i < cntArr; i++)
                {
                    try
                    {
                        if (ustfArrStForSelection[i].ToString() != "")
                        {
                            AddStfData(ustfArrStForSelection[i].ToString());
                        }
                    }
                    catch (Exception ex)
                    {
                        LogMsg("LoadStfData:Err:" + ex.Message);
                    }
                }
                StfTable.Sort("PENO", true);
                CreateStfIndex();

                StfTable.Command("insert", ",IRT,");
                StfTable.Command("update", ",URT,");
                StfTable.Command("delete", ",DRT,");
                StfTable.Command("read", ",GFR,");

                result = "STFTAB " + StfTable.Count + " Records loaded";

                LogMsg("STFTAB <" + StfTable.Count + ">");
            }
            catch (Exception ex)
            {
                LogMsg("LoadSTFData:Err:" + ex.Message);
                throw ex;
            }
            finally
            {
                ReleaseStfTableWrite();
            }
            return result;
        }

        #region table index
        private void CreateStfIndex()
        {
            StfTable.CreateIndex("URNO", "URNO");
            StfTable.CreateIndex("PENO", "PENO");
        }

        private void DeleteStfIndex()
        {
            try
            {
                StfTable.DeleteIndex("URNO");
                StfTable.DeleteIndex("PENO");
            }
            catch (Exception)
            {
            }
        }
        #endregion
    }
}
