rem Copies all generated files from the SCM-Structue into the Install-Shield directory.


rem set drive to J: (IS 6.2) or K: (IS 8.0) 
set drive=k:

rem Copy ini- and config-files
copy c:\ufis_bin\ceda.ini %drive%\gocc_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\bdps_sec.cfg %drive%\gocc_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\loatab*.* %drive%\gocc_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\telexpool.ini %drive%\gocc_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\DaCo3Tool.ini %drive%\gocc_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\*.bmp %drive%\gocc_build\ufis_client_appl\system\*.*


rem copy applications
copy c:\ufis_bin\debug\bdps_sec.exe %drive%\gocc_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpspass.exe %drive%\gocc_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpsuif.exe %drive%\gocc_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\fips.exe %drive%\gocc_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\rules.exe %drive%\gocc_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\telexpool.exe %drive%\gocc_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\loadtabviewer.exe %drive%\gocc_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\hub_manager.exe %drive%\gocc_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\daco3tool.exe %drive%\gocc_build\ufis_client_appl\applications\*.*

REM copy system-files
copy c:\ufis_bin\release\bcproxy.exe %drive%\gocc_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcproxy.tlb %drive%\gocc_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcserv32.exe %drive%\gocc_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufis32.dll %drive%\gocc_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.exe %drive%\gocc_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.tlb %drive%\gocc_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.ocx %drive%\gocc_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.tlb %drive%\gocc_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.ocx %drive%\gocc_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.tlb %drive%\gocc_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucom.ocx %drive%\gocc_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucom.tlb %drive%\gocc_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.ocx %drive%\gocc_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.tlb %drive%\gocc_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\aatlogin.ocx %drive%\gocc_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\aatlogin.tlb %drive%\gocc_build\ufis_client_appl\system\*.*


rem copy files for Status Manager and 
rem copy c:\ufis_bin\release\*lib.dll %drive%\gocc_build\ufis_client_appl\applications\interopdll\*.*
copy c:\ufis_bin\release\ufis.utils.dll %drive%\gocc_build\ufis_client_appl\applications\interopdll\*.*
copy c:\ufis_bin\release\Status_Manager.exe %drive%\gocc_build\ufis_client_appl\applications\*.*

rem Copy Help-Files
rem copy c:\ufis_bin\Help\*.chm %drive%\gocc_build\ufis_client_appl\help\*.*

rem Copy ReleaseNotes
rem copy c:\ufis_bin\releasenotes.zip %drive%\gocc_build\ufis_client_appl\rel_notes\*.*
