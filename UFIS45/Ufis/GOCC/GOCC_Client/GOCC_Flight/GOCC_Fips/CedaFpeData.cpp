// CedaFpeData.cpp - Class for Fpeipment
//

#include <stdafx.h>
#include <BasicData.h>
#include <ccsglobl.h>
#include <RotationCedaFlightData.h>
#include <CedaCcaData.h>
#include <Konflikte.h>
#include <RotationDlgCedaFlightData.h>
#include <Utils.h>
#include <Fpms.h>
#include <ButtonListDlg.h>
#include <CedaFpeData.h>
#include <process.h>
#include <CedaBasicData.h>
#include <CedaAptLocalUtc.h>

void ProcessFpeCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))

static int ByScore(const FPEDATA **pppFpe1, const FPEDATA **pppFpe2);
static int ByScore(const FPEDATA **pppFpe1, const FPEDATA **pppFpe2)
{
	return (int)((**pppFpe1).Score - (**pppFpe2).Score);
}


CedaFpeData::CedaFpeData()
{                  
    BEGIN_CEDARECINFO(FPEDATA, FpeDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_DATE(Vafr,"VAFR")
		FIELD_DATE(Vato,"VATO")
		FIELD_CHAR_TRIM(Pern,"PERN")
		FIELD_CHAR_TRIM(Adid,"ADID")
		FIELD_CHAR_TRIM(Alco,"ALCO")
		FIELD_CHAR_TRIM(Flno,"FLNO")
		FIELD_CHAR_TRIM(Sufx,"SUFX")
		FIELD_CHAR_TRIM(Regn,"REGN")
		FIELD_CHAR_TRIM(Doop,"DOOP")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(FpeDataRecInfo)/sizeof(FpeDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&FpeDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

	ogDdx.Register((void *)this, BC_FPE_INSERT, CString("FPEDATA"), CString("FPE Insert"), ProcessFpeCf);
	ogDdx.Register((void *)this, BC_FPE_UPDATE, CString("FPEDATA"), CString("FPE Update"), ProcessFpeCf);
	ogDdx.Register((void *)this, BC_FPE_DELETE, CString("FPEDATA"), CString("FPE Delete"), ProcessFpeCf);

	char pclConfigPath[256];
	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	char pclFtyps[250];
	GetPrivateProfileString(ogAppName, "FLIGHTPERMITS", "DEFAULT", pcmFlightPermitsPath, sizeof pcmFlightPermitsPath, pclConfigPath);
	GetPrivateProfileString(ogAppName, "FLIGHTPERMITS_FLIGHTTYPES", "O,S", pclFtyps, sizeof pclFtyps, pclConfigPath);
	omFtyps = pclFtyps;
	bmFlightPermitsEnabled = (strcmp(pcmFlightPermitsPath, "DEFAULT")) ? true : false;

    // Initialize table names and field names
    strcpy(pcmTableName,"FPETAB");
    pcmFieldList = "URNO,VAFR,VATO,PERN,ADID,ALCO,FLNO,SUFX,REGN,DOOP";
}


void ProcessFpeCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((CedaFpeData *)popInstance)->ProcessFpeBc(ipDDXType,vpDataPointer,ropInstanceName);
}

void CedaFpeData::ProcessFpeBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlFpeData = (struct BcStruct *) vpDataPointer;
//	ogBasicData.LogBroadcast(prlFpeData);
	FPEDATA rlFpe, *prlFpe = NULL;
	long llUrno = GetUrnoFromSelection(prlFpeData->Selection);
	GetRecordFromItemList(&rlFpe,prlFpeData->Fields,prlFpeData->Data);
	if(llUrno == 0L) llUrno = rlFpe.Urno;

	switch(ipDDXType)
	{
		case BC_FPE_INSERT:
		{
			if((prlFpe = AddFpeInternal(rlFpe)) != NULL)
			{
				ogDdx.DataChanged((void *)this, FPE_INSERT, (void *)prlFpe);
			}
			break;
		}
		case BC_FPE_UPDATE:
		{
			if((prlFpe = GetFpeByUrno(llUrno)) != NULL)
			{
				CString olOldAlco = prlFpe->Alco;
				GetRecordFromItemList(prlFpe,prlFpeData->Fields,prlFpeData->Data);
				//PrepareDataAfterRead(prlFpe);
				if(olOldAlco != prlFpe->Alco)
				{
					DeleteFpeFromAlcoMap(olOldAlco, llUrno);
					AddFpeToAlcoMap(prlFpe);
				}
				ogDdx.DataChanged((void *)this, FPE_UPDATE, (void *)prlFpe);
			}
			break;
		}
		case BC_FPE_DELETE:
		{
			if((prlFpe = GetFpeByUrno(llUrno)) != NULL)
			{
				long llFpeUrno = prlFpe->Urno;
				DeleteFpeInternal(llFpeUrno);
				ogDdx.DataChanged((void *)this, FPE_DELETE, (void *)llFpeUrno);
			}
			break;
		}
	}
}
 
CedaFpeData::~CedaFpeData()
{
	ogDdx.UnRegister(this,NOTUSED);
	ClearAll();
}

void CedaFpeData::ClearAll()
{
	omUrnoMap.RemoveAll();
	CMapPtrToPtr *polSingleMap;
	CString olAlco;
	for(POSITION rlPos = omAlcoMap.GetStartPosition(); rlPos != NULL; )
	{
		omAlcoMap.GetNextAssoc(rlPos, olAlco, (void *& ) polSingleMap);
		polSingleMap->RemoveAll();
		delete polSingleMap;
	}
	omAlcoMap.RemoveAll();

	omData.DeleteAll();
}

bool CedaFpeData::ReadFpeData()
{
	bool ilRc = true;

	if(!bmFlightPermitsEnabled)
		return ilRc;

	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512] = "";
    //sprintf(pclWhere,"WHERE VAFR <= '%s' AND VATO >= '%s'", opTo.Format("%Y%m%d%H%M%S"), opFrom.Format("%Y%m%d%H%M%S"));
    if((ilRc = CedaAction2(pclCom, pclWhere)) != true)
	{
//		ogBasicData.LogCedaError("CedaFpeData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		FPEDATA rlFpeData;
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlFpeData)) == true)
			{
				AddFpeInternal(rlFpeData);
			}
		}
		ilRc = true;
	}

//	ogBasicData.Trace("CedaFpeData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(FPEDATA), pclWhere);
    return ilRc;
}

FPEDATA *CedaFpeData::AddFpeInternal(FPEDATA &rrpFpe)
{
	FPEDATA *prlFpe = new FPEDATA;
	*prlFpe = rrpFpe;
	omData.Add(prlFpe);
	omUrnoMap.SetAt((void *)prlFpe->Urno,prlFpe);
	AddFpeToAlcoMap(prlFpe);

	return prlFpe;
}

void CedaFpeData::AddFpeToAlcoMap(FPEDATA *prpFpe)
{
	CMapPtrToPtr *polSingleMap;
	if(omAlcoMap.Lookup(prpFpe->Alco, (void *&) polSingleMap))
	{
		polSingleMap->SetAt((void *) prpFpe->Urno, prpFpe);
	}
	else
	{
		polSingleMap = new CMapPtrToPtr;
		polSingleMap->SetAt((void *) prpFpe->Urno, prpFpe);
		omAlcoMap.SetAt(prpFpe->Alco, polSingleMap);
	}
}

void CedaFpeData::DeleteFpeInternal(long lpUrno)
{
	FPEDATA *prlFpe = NULL;
	int ilNumFpes = omData.GetSize();
	for(int ilFpe = (ilNumFpes-1); ilFpe >= 0; ilFpe--)
	{
		prlFpe = &omData[ilFpe];
		if(prlFpe->Urno == lpUrno)
		{
			DeleteFpeFromAlcoMap(prlFpe->Alco, lpUrno);
			omData.DeleteAt(ilFpe);
			break;
		}
	}
	omUrnoMap.RemoveKey((void *)lpUrno);
}

void CedaFpeData::DeleteFpeFromAlcoMap(const char *pcpAlco, long lpFpeUrno)
{
	CMapPtrToPtr *polSingleMap;
	if(omAlcoMap.Lookup(pcpAlco, (void *&) polSingleMap))
	{
		polSingleMap->RemoveKey((void *) lpFpeUrno);
	}
}

FPEDATA* CedaFpeData::GetFpeByUrno(long lpUrno)
{
	FPEDATA *prlFpe = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlFpe);
	return prlFpe;
}

bool CedaFpeData::FlightHasPermit(CTime opTime, bool bpArrival, CString opRegn, CString opAlco, CString opFlno, CString opSufx)
{
	FPEDATA *prlBestFpe = NULL;
	CMapPtrToPtr *polSingleMap = NULL;
	//CString olTrace;
	if(omAlcoMap.Lookup(opAlco, (void *&) polSingleMap))
	{
		long llUrno;
		FPEDATA *prlFpe = NULL;
		int ilScore = 0, ilPrevScore = 0;
		for(POSITION rlPos = polSingleMap->GetStartPosition(); rlPos != NULL; )
		{
			polSingleMap->GetNextAssoc(rlPos, (void *&) llUrno, (void *&)prlFpe);
			if(TestMatch(prlFpe, opTime, bpArrival, opRegn, opFlno, opSufx) > 0)
				return true;
		}
	}

	return false;
}

// opTime must be UTC
FPEDATA* CedaFpeData::GetBestPermitForFlight(CTime opTime, bool bpArrival, CString opRegn, CString opAlco, CString opFlno, CString opSufx, CString *popPermitInfo /* = NULL */)
{
	DisplayTime(opTime);
	FPEDATA *prlBestFpe = NULL;
	CMapPtrToPtr *polSingleMap = NULL;
	//CCSPtrArray <FPEDATA> olPermits;
	if(omAlcoMap.Lookup(opAlco, (void *&) polSingleMap))
	{
		long llUrno;
		FPEDATA *prlFpe = NULL;
		int ilScore = 0, ilPrevScore = 0;
		for(POSITION rlPos = polSingleMap->GetStartPosition(); rlPos != NULL; )
		{
			polSingleMap->GetNextAssoc(rlPos, (void *&) llUrno, (void *&)prlFpe);
			prlFpe->Score = TestMatch(prlFpe, opTime, bpArrival, opRegn, opFlno, opSufx);
			if(prlFpe->Score > 0)
			{
				//olPermits.Add(prlFpe);
				if(prlFpe->Score > ilPrevScore)
				{
					prlBestFpe = prlFpe;
					ilPrevScore = prlFpe->Score;
				}
			}
		}
	}

//	olPermits.Sort(ByScore);
//	popPermitInfo->Empty();
//	for(int i = 0; i < olPermits.GetSize(); i++)
//	{
//		FPEDATA *prlFpe = &olPermits[i];
//		CString olInfo;
//		olInfo.Format("PERN<%s> ADID<%s> ALCO<%s> FLNO<%s> SUFX<%s> REGN<%s> DOOP<%s>\n", prlFpe->Pern, prlFpe->Adid, prlFpe->Alco, prlFpe->Flno, prlFpe->Sufx, prlFpe->Regn, prlFpe->Doop);
//		*popPermitInfo += olInfo;
//	}
	if(popPermitInfo != NULL)
	{
		popPermitInfo->Empty();
		if(prlBestFpe != NULL)
			popPermitInfo->Format("Permit Number <%s> A/D <%s> Airline <%s> Flight Num <%s> Suf. <%s> Registration <%s> Day of Op. <%s>", prlBestFpe->Pern, prlBestFpe->Adid, prlBestFpe->Alco, prlBestFpe->Flno, prlBestFpe->Sufx, prlBestFpe->Regn, prlBestFpe->Doop);
		else
			*popPermitInfo = "No Flight Permit";
	}
	return prlBestFpe;
}

// returns zero for flight permit not found 
// or positive value for match, the better the match the higher the value returned
// popTrace contains or the reason why the flight was not matched else it is empty
// opTime must be UTC
int CedaFpeData::TestMatch(FPEDATA *prpFpe, CTime opTime, bool bpArrival, CString opRegn, CString opFlno, CString opSufx, CString *popTrace /* = NULL */)
{
//	DisplayTime(opTime);
	int ilScore = 1; // found
	
	if(opTime < prpFpe->Vafr || opTime > prpFpe->Vato)
	{
		if(popTrace != NULL)
			popTrace->Format("Flight time outside permit's validity:  Flight Time = %s  Permit valid from %s to %s", opTime.Format("%d.%m.%Y %H:%M"), prpFpe->Vafr.Format("%d.%m.%Y %H:%M"), prpFpe->Vato.Format("%d.%m.%Y %H:%M"));
		return 0;
	}

	bool blArrival = (!strcmp(prpFpe->Adid,"A")) ? true : false;
	if(blArrival != bpArrival)
	{
		if(popTrace != NULL)
			popTrace->Format("Arrival/Departure mismatch:  Flight Type = %s  Permit Type = %s", bpArrival ? "Arrival" : "Departure", blArrival ? "Arrival" : "Departure");
		return 0;
	}

	if(!CheckDoop(prpFpe->Doop, opTime))
	{
		if(popTrace != NULL)
			popTrace->Format("Flight and Permit day of operation do not match");
		return 0;
	}


	// the following non-mandatory fields are for more specific flights and therefore score higher than general permits if matched

	if(strlen(prpFpe->Regn) > 0)
	{
		if(strcmp(opRegn,prpFpe->Regn))
		{
			if(popTrace != NULL)
				popTrace->Format("Flight and Permit registrations do not match:  Flight Registration %s  Permit Registration %s", opRegn, prpFpe->Regn);
			return 0;
		}
		ilScore++;
	}

	// flight number and suffix form one single key
	if(strlen(prpFpe->Flno) > 0 || strlen(prpFpe->Sufx) > 0)
	{
		if(strcmp(opFlno,prpFpe->Flno) || strcmp(opSufx,prpFpe->Sufx))
		{
			if(popTrace != NULL)
				popTrace->Format("Flight and Permit flight numbers do not match:  Flight number/suffix %s/%s  Permit flight number/suffix %s/%s", opFlno, opSufx, prpFpe->Flno, prpFpe->Sufx);
			return 0;
		}
		ilScore++;
	}

	return ilScore;
}

bool CedaFpeData::CheckDoop(CString opFpeDoop, CTime opFlightTime)
{
	CTime olFlightTime = opFlightTime;

	// must convert from UTC to local to get the correct day
	CedaAptLocalUtc::AptUtcToLocal(olFlightTime, CString(pcgHome4));

	int ilFlightDoop = olFlightTime.GetDayOfWeek() - 1;
	CString olFlightDoop;
	olFlightDoop.Format("%d", (ilFlightDoop == 0) ? 7 : ilFlightDoop);
	return (opFpeDoop.Find(olFlightDoop) != -1) ? true : false;
}

// opTime must be UTC
bool CedaFpeData::StartFlightPermitsAppl(CTime opTime, bool bpArrival, CString opRegn, CString opAlco, CString opFlno, CString opSufx)
{
	DisplayTime(opTime);
	if(!bmFlightPermitsEnabled)
		return false;

	if(opTime == TIMENULL)
		return false;

	char pclFieldList[500];
	char pclData[500];
	char pclCommand[50];

	FPEDATA *prlFpe = ogFpeData.GetBestPermitForFlight(opTime, bpArrival, opRegn, opAlco, opFlno, opSufx);
	if(prlFpe != NULL)
	{
		strcpy(pclCommand, "UPDATE");
		strcpy(pclFieldList, "ALC3,FLTN,FLNS,URNO");
		sprintf(pclData, "%s,%s,%s,%ld", opAlco, opFlno, opSufx, prlFpe->Urno);
	}
	else
	{
		CTime olLocalTime = UtcToLocal(opTime);
		strcpy(pclCommand, "INSERT");
		strcpy(pclFieldList, "ADID,ALC3,FLTN,FLNS,REGN,VAFR,VATO,DOOP,MTOW");
		int ilDoop = olLocalTime.GetDayOfWeek() - 1;
		ilDoop = (ilDoop == 0) ? 7 : ilDoop;
		CString olMtow = "";
		if(!opRegn.IsEmpty())
		{
			CString olWhere;
			olWhere.Format("WHERE REGN = '%s'", opRegn);
			ogBCD.Read( "ACR", olWhere);
			ogBCD.GetField("ACR", "REGN", opRegn, "MTOW", olMtow);
			olMtow.TrimLeft('0');
		}
		sprintf(pclData, "%s,%s,%s,%s,%s,%s,%s,%d,%s", bpArrival ? "A" : "D", opAlco, opFlno, opSufx, opRegn, olLocalTime.Format("%Y%m%d000000"), olLocalTime.Format("%Y%m%d235959"), ilDoop, olMtow);
	}

	char *args[7];
	args[0] = "child";
	args[1] = pcgUser;
	args[2] = pcgPasswd;
	args[3] = pclCommand;
	args[4] = pclFieldList;
	args[5] = pclData;
	args[6] = NULL;
	int ilRc = _spawnv(_P_NOWAIT,pcmFlightPermitsPath,args);

	CString olErr = "";
	switch(ilRc)
	{
		case 0:
			// no error
			break;
		case E2BIG:
			olErr = "Argument list exceeds 1024 bytes";
			break;
		case EINVAL:
			olErr = "mode argument is invalid";
			break;
		case ENOENT:
			olErr = "File or path is not found";
			break;
		case ENOEXEC:
			olErr = "Specified file is not executable or has invalid executable-file format";
			break;
		case ENOMEM:
			olErr = "Not enough memory is available to execute new process";
			break;
		default:
			olErr = "Unknown error returned when attempting to start the process";
			break;
	}

//	if(!olErr.IsEmpty())
//	{
//		CString olTxt;
//		olTxt.Format("Error starting the Flight Permits application (Ceda.ini FLIGHTPERMITS=\"%s\").\n\n%s", pcmFlightPermitsPath, olErr);
//		::MessageBox(NULL, olTxt, "Flight Permits", MB_OK | MB_ICONERROR);
//		return false;
//	}

	return true;
}

bool CedaFpeData::FtypEnabled(const char *pcpFtyp)
{
	return (omFtyps.Find(pcpFtyp) != -1) ? true : false;
}

CTime CedaFpeData::UtcToLocal(CTime opTime)
{
	CedaAptLocalUtc::AptUtcToLocal(opTime, CString(pcgHome4));
	return opTime;
}

void CedaFpeData::DisplayTime(CTime opTime)
{
// for debugging only!
//	::MessageBox(NULL, opTime.Format("%d.%m.%Y %H:%M"), "Time", MB_OK | MB_ICONERROR);
}