// DailyCcaPrintDlg.cpp : implementation file
//

#include <stdafx.h>
#include <fpms.h>
#include <DailyCcaPrintDlg.h>
#include <ReportSelectDlg.h>
#include <CCSGLOBL.H>
#include <BasicData.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// 
#define _MIT_TRACE_		FALSE

const int imMontagsID = 1159 ;	// IDS_STRING1159 "Montag" ( s. auch DailyPrintDlg.h ! )


/////////////////////////////////////////////////////////////////////////////
// CDailyCcaPrintDlg dialog


CDailyCcaPrintDlg::CDailyCcaPrintDlg(CWnd* pParent, char *opSelect )
	: CDialog(CDailyCcaPrintDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDailyCcaPrintDlg)
 	m_Vonzeit = _T("");
	m_Vomtag = _T("");
	//}}AFX_DATA_INIT

	pomSelectDlg = (CReportSelectDlg*) pParent ;
	pcmSelect = opSelect;
 
}


void CDailyCcaPrintDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDailyCcaPrintDlg)
	DDX_Control(pDX, IDC_VONZEIT, m_CE_Vonzeit);
	DDX_Control(pDX, IDC_VOMTAG, m_CE_Vomtag);
 	DDX_Text(pDX, IDC_VONZEIT, m_Vonzeit);
	DDX_Text(pDX, IDC_VOMTAG, m_Vomtag);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDailyCcaPrintDlg, CDialog)
	//{{AFX_MSG_MAP(CDailyCcaPrintDlg)
	ON_MESSAGE(WM_EDIT_KILLFOCUS, OnKillfocusVomtag)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDailyCcaPrintDlg message handlers

void CDailyCcaPrintDlg::OnOK() 
{
	
	CString olErrorText;
	char pclWhere[512];

	bool blNoEx = false ;
	CTime   olMinDate;
	CTime   olMaxDate;
	CString olHStr1( "00:00" ) ;
	CString olHStr2( "23:59" ) ;
	CString olBistag ;


	if(!m_CE_Vomtag.GetStatus())
	{
		olErrorText += GetString( IDS_STRING1399 ) ;
	}

	
	if(!olErrorText.IsEmpty())	
	{

		olErrorText= GetString(IDS_STRING967) + "\n\n" + olErrorText;

		MessageBox(olErrorText,GetString( ST_FEHLER),MB_ICONEXCLAMATION);
		return;
	}
	
	m_CE_Vomtag.GetWindowText(m_Vomtag);

	if( m_Vomtag.IsEmpty())
	{
		// Default: heute
		CTime olTime = CTime::GetCurrentTime();
		m_Vomtag = olTime.Format( "%d.%m.%Y" );
		olBistag = olTime.Format( "%d.%m.%Y" );
	}
	else
	{
		olBistag = m_Vomtag;
	}

	// Preset festhalten
	pomSelectDlg->omLastValues.SetMaxDate( m_Vomtag ) ;

	olMinDate = DateHourStringToDate( m_Vomtag, olHStr1 );
	olMaxDate = DateHourStringToDate( olBistag, olHStr2 );


	// CeDa Zugriff in UTC Zeit
	if( bgReportLocal ) 
	{
		ogBasicData.LocalToUtc(olMinDate);
		ogBasicData.LocalToUtc(olMaxDate);
	}

	CString olMaxStr = olMaxDate.Format( "%Y%m%d%H%M%S" );
	CString olMinStr = olMinDate.Format( "%Y%m%d%H%M%S" );

	// Selektionskriterium basteln
	if((olMaxDate != TIMENULL) && (olMinDate != TIMENULL) )
	{
		// ToDo: evt.TIFD ?
		// !!! Start mit 2 Klammern wegen Gleichheit mit Daily OPS !!!!
		// sprintf(pclWhere,"((STOD<='%s' AND STOD>='%s') AND ADID='D')", olMaxStr, olMinStr );
		sprintf(pclWhere,"((STOD<='%s' AND STOD>='%s') AND ORG3='%s')", olMaxStr, olMinStr, pcgHome );
		
	}

	if(!olErrorText.IsEmpty())	
	{
		if( MessageBox( olErrorText,GetString( IDS_WARNING), MB_YESNO ) == IDNO )
		{ 
			return;
		}
	}



	// Return Werte
	strcpy(pcmSelect, pclWhere);

	// Presets festhalten
	pomSelectDlg->omLastValues.SetMaxDate( m_Vomtag ) ;
 
	pomSelectDlg->omLastValues.SetMaxTime( omLastEnd ) ;
	pomSelectDlg->omLastValues.SetMinTime( omLastStart ) ;
	
	CDialog::OnOK();
}



void CDailyCcaPrintDlg::OnKillfocusVomtag( UINT wParam, UINT lParam ) 
{
	
	// Infofenster VonZeit mit Auswahltag versorgen
    CString olLokdum ;

	UpdateData() ;

	if(!m_CE_Vomtag.GetStatus())
	{
		CString olErrorText( GetString( IDS_STRING934) ) ;
//		CString olErrorText( GetString( IDS_STRING1399 ) ) ;
//		olErrorText= GetString(IDS_STRING967) + "\n\n" + olErrorText;

		MessageBox(olErrorText,GetString( ST_FEHLER),MB_ICONEXCLAMATION);
		return;
	}

	// Wochentag bestimmen
	CTime olLocalTime = DateStringToDate( m_Vomtag ) ;
	// wenn Datum kleiner 01.01.1970 oder groesser .2038 --> Absturz, MBR 21.05.1999
	if (olLocalTime == TIMENULL)
	{
		CTime olTime = CTime::GetCurrentTime();
		m_Vomtag = olTime.Format( "%d.%m.%Y" );
		m_CE_Vomtag.SetWindowText(m_Vomtag);
		pomSelectDlg->omLastValues.SetMaxDate( m_Vomtag ) ;
		olLocalTime = DateStringToDate( m_Vomtag ) ;
		MessageBox(GetString(IDS_STRING1643), GetString(IMFK_REPORT), MB_OK);
	}

	int ilWDay = olLocalTime.GetDayOfWeek() ;
	int ilWDStr = imMontagsID - 2 ;	// IDS_STRING1159 = Montag
	int ilSoOffset = 0 ;			// Offset auf Wochentag ( wegen SOnntag )
	if( ilWDay > 1 )
	{
		ilWDStr += ilWDay ;
		ilSoOffset = -1 ;
	}
	else
	{
		// Sonntag
		ilWDStr += ilWDay+7 ;	
		ilSoOffset = 6 ;
	}
	m_Vonzeit = GetString( ilWDStr )+", "+olLocalTime.Format( "%d.%m.%y" ) ;
	olLokdum.Format(",  %s %d", GetString( IDS_STRING1418 ), ilWDay+ilSoOffset ) ;
	m_Vonzeit += olLokdum ;
	
	m_CE_Vonzeit.SetWindowText( m_Vonzeit ) ;
	m_CE_Vonzeit.UpdateWindow() ;
	
}


BOOL CDailyCcaPrintDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_CE_Vomtag.SetTypeToDate();
	m_CE_Vomtag.SetBKColor( YELLOW );
	
	// Presets entsprechend vorherigem Aufruf
	omLastStart = pomSelectDlg->omLastValues.GetMinTime() ;
	omLastEnd = pomSelectDlg->omLastValues.GetMaxTime() ;

	if( (pomSelectDlg->omLastValues.GetMaxDate()).IsEmpty() )
	{
		CTime olLocalTime = CTime::GetCurrentTime() ;
		m_Vomtag = olLocalTime.Format( "%d.%m.%Y" )  ;
	}
	else
	{
		m_Vomtag = pomSelectDlg->omLastValues.GetMaxDate()  ;
	}

	m_CE_Vomtag.SetWindowText( m_Vomtag ) ;
	m_CE_Vomtag.UpdateWindow() ;

	// Hinweisfenster Vonzeit versorgen
	OnKillfocusVomtag( 0, 0 ) ;	
	
	UpdateData() ;
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
