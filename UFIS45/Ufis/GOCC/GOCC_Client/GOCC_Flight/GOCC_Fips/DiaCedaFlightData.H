#ifndef _DiaCedaFlightData_H_
#define _DiaCedaFlightData_H_
 
#include <BasicData.h>
#include <CCSGlobl.h>
#include <CCSDefines.h>
#include <CedaDiaCcaData.h>
#include <CedaCflData.h>
#include <RotationTableViewer.h>  

struct AllocateParameters
{
	CTimeSpan omPstLeftBuffer;
	CTimeSpan omPstRightBuffer;
	CTimeSpan omGatLeftBuffer;
	CTimeSpan omGatRightBuffer;

	bool bmAllocatePst;
	bool bmAllocateGat;
	bool bmAllocateWro;
	bool bmAllocateBlt;
	bool bmAllocateExt;
};



 
struct DIAFLIGHTDATA 
{
	char 	 Baa1[5]; 	// FlightReports
	char 	 Baa2[5]; 	// SpecialReq(Read)
	char 	 Baa3[5]; 	// SpecialReq(UnRead)
	char 	 Act3[5]; 	// Flugzeug-3-Letter Code (IATA)
	char 	 Act5[7]; 	// Flugzeug-5-Letter Code
	CTime 	 Airb; 	// Startzeit (Beste Zeit)
	char 	 Alc2[4]; 	// Fluggesellschaft (Airline 2-Letter Code)
	char 	 Alc3[5]; 	// Fluggesellschaft (Airline 3-Letter Code)
	CTime 	 B1ba; 	// Belegung Gep�ckband 1 aktueller Beginn
	CTime 	 B1ea; 	// Belegung Gep�ckband 1 aktuelles Ende
	CTime 	 B2ba; 	// Belegung Gep�ckband 2 aktueller Beginn
	CTime 	 B2ea; 	// Belegung Gep�ckband 2 aktuelles Ende
	char 	 Blt1[7]; 	// Gep�ckband 1
	char 	 Blt2[7]; 	// Gep�ckband 2
	char	 Tmb1[3];
	char	 Tmb2[3];
	char 	 Tet1[3]; 	// Terminal Ausgang 1 Ankunft
	char 	 Tet2[3]; 	// Terminal Ausgang 2 Ankunft
	char 	 Tga1[3]; 	// Terminal Gate 1 Ankunft
	char 	 Tga2[3]; 	// Terminal Gate 2 Ankunft
	char 	 Tgd1[3]; 	// Terminal Gate 1 Abflug
	char 	 Tgd2[3]; 	// Terminal Doppelgate Abflug
	char 	 Twr1[3]; 	// Warteraum Terminal
	char 	 Des3[5]; 	// Bestimmungsflughafen 3-Lettercode
	char 	 Des4[6]; 	// Bestimmungsflughafen 4-Lettercode
	CTime 	 Etai; 	// ETA-Intern (Beste Zeit)
	CTime 	 Etdi; 	// ETD-Intern (Beste Zeit)
	CTime	 Etod;	// ETD (Display)
	char 	 Ext1[7]; 	// Ausgang 1 Ankunft
	char 	 Ext2[7]; 	// Ausgang 2 Ankunft
	char 	 Flno[11]; 	// komplette Flugnummer (Airline, Nummer, Suffix)
	char 	 Flns[3]; 	// Suffix
	char 	 Fltn[7]; 	// Flugnummer
	char 	 Ftyp[3]; 	// Type des Flugs [F, P, R, X, D, T, ...]
	CTime 	 Ga1x; 	// Belegung Gate 1 Ankunft aktueller Beginn
	CTime 	 Ga1y; 	// Belegung Gate 1 Ankunft aktuelles Ende
	CTime 	 Ga2x; 	// Belegung Gate 2 Ankunft aktueller Beginn
	CTime 	 Ga2y; 	// Belegung Gate 2 Ankunft aktuelles Ende
	CTime 	 Gd1x; 	// Belegung Gate 1 Abflug aktueller Beginn
	CTime 	 Gd1y; 	// Belegung Gate 1 Abflug aktuelles Ende
	CTime 	 Gd2x; 	// Belegung Gate 2 Abflug aktueller Beginn
	CTime 	 Gd2y; 	// Belegung Gate 2 Abflug aktuelles Ende
	char 	 Gta1[7]; 	// Gate 1 Ankunft
	char 	 Gta2[7]; 	// Gate 2 Ankunft
	char 	 Gtd1[7]; 	// Gate 1 Abflug
	char 	 Gtd2[7]; 	// Doppelgate Abflug
	CTime 	 Land; 	// Landezeit (Beste Zeit)
	CTime	 Lstu;	// Time of last change!
	char 	 Ming[6]; 	// Mindestbodenzeit
	char	 Nose[5] ;	// seats - COnfiguration
	CTime 	 Ofbl; 	// Offblock- Zeit (Beste Zeit)
	CTime 	 Onbe; 	// Est. Onblock-Zeit
	CTime 	 Onbl; 	// Onblock-Zeit (Beste Zeit)
	char 	 Org3[5]; 	// Ausgangsflughafen 3-Lettercode
	char 	 Org4[6]; 	// Ausgangsflughafen 4-Lettercode
	CTime 	 Paba; 	// Belegung Position Ankunft aktueller Beginn
	CTime 	 Paea; 	// Belegung Position Ankunft aktuelles Ende
	CTime 	 Pdba; 	// Belegung Position Abflug aktueller Beginn
	CTime 	 Pdea; 	// Belegung Position Abflug aktuelles Ende
	char 	 Psta[7]; 	// Position Ankunft
	char 	 Pstd[7]; 	// Position Abflug
	char 	 Regn[14]; 	// LFZ-Kennzeichen
	long 	 Rkey; 	// Rotationsschl�ssel
	char 	 Rtyp[3]; 	// Verkettungstyp (Art/Quelle der Verkettung)
	CTime 	 Slot; 	// aktuelle Slotzeit
	CTime 	 Stoa; 	// Planm��ige Ankunftszeit STA
	CTime 	 Stod; 	// Planm��ige Abflugzeit
	CTime 	 Tifa; 	// Zeitrahmen Ankunft
	CTime 	 Tifd; 	// Zeitrahmen Abflug
	char 	 Tisa[3]; 	// Statusdaten f�r TIFA
	char 	 Tisd[3]; 	// Statusdaten f�r TISD
	CTime 	 Tmoa; 	// TMO (Beste Zeit)
	long 	 Urno; 	// Eindeutige Datensatz-Nr.
	CTime 	 W1ba; 	// Belegung Warteraum 1 aktueller Beginn
	CTime 	 W1ea; 	// Belegung Warteraum 1 Aktuelles Ende
	CTime 	 W2ba; 	// Belegung Warteraum 1 aktueller Beginn
	CTime 	 W2ea; 	// Belegung Warteraum 1 Aktuelles Ende
	char 	 Wro1[7]; 	// Warteraum
	char 	 Wro2[7]; 	// Warteraum
	char 	 Via3[4]; 	// Letzte Station vor Ankunft - Abflug
	char 	 Via4[5]; 	// Letzte Station vor Ankunft - Abflug
	char 	 Adid[2]; 	// 
	char 	 Ckif[7]; 	// Check-In From
	char 	 Ckit[7]; 	// Check-In To
	char 	 Dooa[3]; 	
	char 	 Dood[3]; 	
	char 	 Htyp[7]; 	
	char 	 Ttyp[7]; 	
	char 	 Vian[5]; 	
	char 	 Vial[1026]; 	// Liste der Zwischenstationen/-zeiten
	char 	 Gd2d[2]; 	// Demand for second Gate

	CTime 	 W1es; 	
	CTime 	 W1bs; 	
	CTime 	 W2es; 	
	CTime 	 W2bs; 	
	CTime 	 B1bs; 	
	CTime 	 B1es; 	
	char 	 Paxt[4]; 	

	CTime    Pabs;
	CTime    Paes;
	CTime    Pdbs;
	CTime    Pdes;

	CTime    Ga1b;
	CTime    Ga1e;
	CTime    Ga2b;
	CTime    Ga2e;
	CTime    Gd1b;
	CTime    Gd1e;
	CTime    Gd2b;
	CTime    Gd2e;
	char	 Styp[3];
	CTime    B2bs;
	CTime    B2es;
	CTime    Bbaa;
	CTime    Bbfa;
	char	 Flti[3];
	CTime	 Etoa;	// ETA (Fids)
	CTime 	 Nxti; 	// 
	char 	 Ader[3]; 	// Mindestbodenzeit
	CTime 	 Aird; 	
	char 	 Stat[11]; 	
	char 	 Csgn[10]; 	// Call- Sign
	char 	 Baa4[5]; 	// Def. Groundtime
	char 	 Baa5[5]; 	// Def. Groundtime
//	char 	 Bad4[5]; 	// Def. Groundtime
//	char 	 Bad5[5]; 	// Def. Groundtime
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	CTime 	 Cdat; 	// creation
	char 	 Usec[34]; 	// User who created the record
	char	 Vers[22];  // Version, 
						//In GOCC Project, it is used to keep the string to show the popup icon menu

	// Data from cca - table

	CTime	 CcaStart;
	CTime	 CcaEnd;
	char	 Cca[33];

	char	 Remark[100];
	
	int		 PgrButa;


	int      IsChanged;
	bool	blNeedDBSave;
	DIAFLIGHTDATA *prmActDBRec;

	CTime	StartPosArr;			// Act. Position allocation of arrival flight
	CTime	EndPosArr;				// Act. Position allocation of arrival flight
	CTime	StartPosDep;			// Act. Position allocation of departure flight
	CTime	EndPosDep;				// Act. Position allocation of departure flight
	bool	bmPosFollowCurrTime;	// Must the position allocation follow act. time line?
	bool	bmPosArrIsSelected;
	bool	bmPosDepIsSelected;

	CTime	StartGat1Arr;			// Act. Gate 1 allocation of arrival flight
	CTime	EndGat1Arr;				// Act. Gate 1 allocation of arrival flight
	CTime	StartGat2Arr;			// Act. Gate 2 allocation of arrival flight
	CTime	EndGat2Arr;				// Act. Gate 2 allocation of arrival flight
	CTime	StartGat1Dep;			// Act. Gate 1 allocation of departure flight
	CTime	EndGat1Dep;				// Act. Gate 1 allocation of departure flight
	CTime	StartGat2Dep;			// Act. Gate 2 allocation of departure flight
	CTime	EndGat2Dep;				// Act. Gate 2 allocation of departure flight
	bool	bmGatFollowCurrTime;	// Must the position allocation follow act. time line?
	bool	bmGat1ArrIsSelected;
	bool	bmGat2ArrIsSelected;
	bool	bmGat1DepIsSelected;
	bool	bmGat2DepIsSelected;

	CTime	StartBlt1;				// Act. Belt 1 allocation of flight
	CTime	EndBlt1;				// Act. Belt 1 allocation of flight
	CTime	StartBlt2;				// Act. Belt 2 allocation of flight
	CTime	EndBlt2;				// Act. Belt 2 allocation of flight
	bool	bmBltFollowCurrTime;	// Must the position allocation follow act. time line?
	bool	bmBlt1IsSelected;
	bool	bmBlt2IsSelected;

	CTime	StartWro1;				// Act. Wro allocation of flight
	CTime	EndWro1;				// Act. Wro allocation of flight
	CTime	StartWro2;				// Act. Wro allocation of flight
	CTime	EndWro2;				// Act. Wro allocation of flight
	bool	bmWroFollowCurrTime;	// Must the position allocation follow act. time line?
	bool	bmWro1IsSelected;
	bool	bmWro2IsSelected;

	bool	bmNCC;

	bool	bmFlightDiaArrIsSelected;
	bool	bmFlightDiaDepIsSelected;

	bool	PstaReason;
	bool	PstdReason;
	bool	Gta1Reason;
	bool	Gta2Reason;
	bool	Gtd1Reason;
	bool	Gtd2Reason;

	
	/*

	CTime	 DCcaFrom;
	CTime	 DCcaTo;
	char	 DCkic[10];
*/
	void ClearAPos(void);
	void ClearDPos(void);
	void ClearAGat(int ilNum);
	void ClearDGat(int ilNum);
	void ClearBlt(int ilNum);
	void ClearWro(int ilNum);
	
	DIAFLIGHTDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		StartPosArr = -1;
		EndPosArr	= -1;
		StartPosDep = -1;
		EndPosDep	= -1;
		bmPosFollowCurrTime = false;
		bmPosArrIsSelected = false;
		bmPosDepIsSelected = false;

		StartGat1Arr = -1;
		EndGat1Arr	= -1;
		StartGat2Arr = -1;
		EndGat2Arr	= -1;
		StartGat1Dep = -1;
		EndGat1Dep	= -1;
		StartGat2Dep = -1;
		EndGat2Dep	= -1;
		bmGatFollowCurrTime = false;
		bmGat1ArrIsSelected = false;
		bmGat2ArrIsSelected = false;
		bmGat1DepIsSelected = false;
		bmGat2DepIsSelected = false;

		StartBlt1 = -1;
		EndBlt1 = -1;
		StartBlt2 = -1;
		EndBlt2 = -1;
		bmBltFollowCurrTime = false;
		bmBlt1IsSelected = false;
		bmBlt2IsSelected = false;

		StartWro1 = -1;
		EndWro1 = -1;
		StartWro2 = -1;
		EndWro2 = -1;
		bmWroFollowCurrTime = false;
		bmWro1IsSelected = false;
		bmWro2IsSelected = false;

		bmNCC = false;
		bmFlightDiaArrIsSelected = false;
		bmFlightDiaDepIsSelected = false;

		Airb = -1;
		B1ba = -1;
		B1ea = -1;
		B2ba = -1;
		B2ea = -1;
		Etai = -1;
		Etdi = -1;
		Etod = -1;
		Ga1x = -1;
		Ga1y = -1;
		Ga2x = -1;
		Ga2y = -1;
		Gd1x = -1;
		Gd1y = -1;
		Gd2x = -1;
		Gd2y = -1;
		Land = -1;
		Lstu = -1;
		Ofbl = -1;
		Onbe = -1;
		Onbl = -1;
		Paba = -1;
		Paea = -1;
		Pdba = -1;
		Pdea = -1;
		Slot = -1;
		Stoa = -1;
		Stod = -1;
		Tifa = -1;
		Tifd = -1;
		Tmoa = -1;
		W1ba = -1;
		W1ea = -1;
		W2ba = -1;
		W2ea = -1;
		B2bs  = -1;
		B2es = -1;
		Pabs	= -1;
	    Paes	= -1;
		Pdbs	= -1;
	    Pdes	= -1;
	    Bbaa	= -1;
	    Bbfa	= -1;
		CcaStart	= -1;
		CcaEnd		= -1;
		PgrButa = 0;
		PstaReason = false;
		PstdReason = false;
		Gta1Reason = false;
		Gta2Reason = false;
		Gtd1Reason = false;
		Gtd2Reason = false;

		//DCcaFrom	= -1;
		//DCcaTo		= -1;
		IsChanged = DATA_UNCHANGED;
		blNeedDBSave = false;
		prmActDBRec = NULL;
	}
//	DIAFLIGHTDATA(const CCAFLIGHTDATA &ropFlight);

//	const DIAFLIGHTDATA& operator= (  CCAFLIGHTDATA& ropFlight);
	const DIAFLIGHTDATA& operator=( const ROTATIONFLIGHTDATA& ropRotationFlight);
}; // end DIAFLIGHTDATA
	




/////////////////////////////////////////////////////////////////////////////
// Class declaration

//@Man:
//@Memo: Read and write flight data
//@See: CedaData
/*@Doc:
  Reads and writes flight data from and to database. Stores flight data in memory 
  and provides methods for searching and retrieving flights from its internal list. 
  Does some data preparations like pre-formatting the flight number, storing flight 
  number and gate information for the corresponding inbound/outbound flight.

  Data of this class has to be updated by CEDA through broadcasts. This procedure 
  and the necessary methods will be described later with the description of 
  broadcast handling.

  {\bf DiaCedaFlightData} handles data from the EIOHDL server process.
	See the specification for table descriptions.
*/
class DiaCedaFlightData: public CCSCedaData
{
// Attributes
public:
    //@ManMemo: A map, containing the Keya/Keyd fields of all loaded flights.
    //CMapStringToPtr omKeyMap;
    CMapPtrToPtr omUrnoMap;
	CMapPtrToPtr omRkeyMap;
    CMapStringToPtr omPosMap;
    CMapStringToPtr omGatMap;
    CMapStringToPtr omBltMap;
    CMapStringToPtr omWroMap;
    CMapStringToPtr omExtMap;

	CMapPtrToPtr omPosWoResMap;
	CMapPtrToPtr omBltWoResMap;
	CMapPtrToPtr omGatWoResMap;
	CMapPtrToPtr omWroWoResMap;

	CMapPtrToPtr omPosOnTimeline;
	CMapPtrToPtr omGatOnTimeline;
	CMapPtrToPtr omWroOnTimeline;
	CMapPtrToPtr omBltOnTimeline;


   //@ManMemo: DIAFLIGHTDATA records read by ReadAllFlight().
    CCSPtrArray<DIAFLIGHTDATA> omData;
    //CCSPtrArray<DIAFLIGHTDATA> omSaveData;

	char pcmAftFieldList[2048];

	bool bmOffLine;

	bool bmAllocate;

	bool bmPosReason;
	bool bmGatReason;

	const int cimBartimesUpdateMinute;

// Operations
public:
    //@ManMemo: Default constructor
    /*@Doc:
      Initialize {\bf omRecInfo} for offsets and types for each fields in 
	  {\bf DIAFLIGHTDATA}, the {\bf pcmTableName} with table name {\bf AFTLSG}, 
	  the data members {\bf pcmFieldList} contains a list of used fields of 
	  {\bf DIAFLIGHTDATAStruct}.
    */
    //DiaCedaFlightData(CCSCedaCom *popCommHandler = NULL);  : CCSCedaData(&ogCommHandler)
    DiaCedaFlightData(bool bpConflicts = true);
    //@ManMemo: Default destructor
	~DiaCedaFlightData();
	
	void UnRegister();
	void Register();

	struct RKEYLIST
	{
		CCSPtrArray<DIAFLIGHTDATA> Rotation;

		DIAFLIGHTDATA *GetArrivalFlight(long lpDUrno);
		DIAFLIGHTDATA *GetDepartureFlight(long lpAUrno);
	};


	void ToggleOffline();
    
	//@ManMemo: Clear all flights.
    /*@Doc:
      Clear all flights.
      This method will clear {\bf omData} and {\bf omKeyMap}.
    */
	void ClearAll(void);

	void ReadAllCca();


    // internal data access.
	bool AddFlightInternal(DIAFLIGHTDATA *prpFlight, bool bpDdx = true);
	bool DeleteFlightInternal(long lpUrno, bool bpDdx = true);
	bool DeleteFlightsInternal(long lpRkey, bool bpDdx = true);

	bool ReadAllFlights(const CString &opWhere, bool bpRotation, const CString &opFOGTAB);
	void SetPreSelection(const CTime &opFrom, const CTime &opTo, const CString &opFtyps , bool bpSearchMode = false);
	void SetPreSelectionAdid(const CString &opAdidIn);
	bool PreSelectionCheck(CString &opData, CString &opFields, long lpRkey, bool bpCheckOldNewVal = false);

	bool ReadFlights(char *pcpSelection, bool bpDdx = true, bool bpIngoreView = false);
	bool ReadRkeys(CString &olRkeys, char *pcpSelection);

	void IsPosGateChanged();

	// SAS

	bool CollectAllRelatedFlights(DIAFLIGHTDATA *prpFlight, CMapPtrToPtr &ropRelFlightsMap, CMapPtrToPtr &ropRelNeighborFlightsMap);

	bool GetBestScheduledPstTime(DIAFLIGHTDATA *prpFlight, const CString &ropPos, bool bpArrival, bool bpBegin, CTime &opTime, bool bpPreCheck = false) const;
	bool GetBestScheduledGatTime(const DIAFLIGHTDATA *prpFlight, const CString &ropGat, int ipGatNo, char cpFPart, bool bpBegin, CTime &opTime, bool bpPreCheck = false) const;
	bool GetBestScheduledBltTime(const DIAFLIGHTDATA *prpFlight, const CString &ropBlt, int ipBltNo, bool bpBegin, CTime &opTime, bool bpPreCheck = false) const;
	
	bool GetBestPstTime(DIAFLIGHTDATA *prpFlight, const CString &ropPos, bool bpArrival, bool bpBegin, CTime &opTime, bool bpPreCheck = false) const;
	bool GetBestGatTime(const DIAFLIGHTDATA *prpFlight, const CString &ropGat, int ipGatNo, char cpFPart, bool bpBegin, CTime &opTime, bool bpPreCheck = false) const;
	bool GetBestBltTime(const DIAFLIGHTDATA *prpFlight, const CString &ropBlt, int ipBltNo, bool bpBegin, CTime &opTime, bool bpPreCheck = false) const;
	bool GetBestWroTime(const DIAFLIGHTDATA *prpFlight, const CString &ropWro, int ipWroNo, bool bpBegin, CTime &opTime, bool bpPreCheck = false) const;
	bool GetBestPstTimePFC(DIAFLIGHTDATA *prpFlight, const CString &ropPos, bool bpArrival, bool bpBegin, CTime &opTime, bool bpPreCheck = false) const;
	bool GetBestGatTimeGFC(const DIAFLIGHTDATA *prpFlight, const CString &ropGat, int ipGatNo, char cpFPart, bool bpBegin, CTime &opTime, bool bpPreCheck = false) const;

	bool GetBltAllocTimes(const DIAFLIGHTDATA &rrpFlight, const CString &ropBlt, int ipBltNo, CTime &ropAllocStart, CTime &ropAllocEnd, bool bpPreCheck = false);
	bool GetGatAllocTimes(const DIAFLIGHTDATA &rrpFlight, char cpFPart, const CString &ropGat, int ipGatNo, CTime &ropAllocStart, CTime &ropAllocEnd, bool bpPreCheck = false);
	bool GetWroAllocTimes(const DIAFLIGHTDATA &rrpFlight, const CString &ropWro, int ipWroNo, CTime &ropAllocStart, CTime &ropAllocEnd, bool bpPreCheck = false);
	bool GetPstAllocTimes(DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD, const CString &ropPst, CTime &ropAllocStart, CTime &ropAllocEnd, bool bpPreCheck = false);


	void CalculatePstAllocTimes(RKEYLIST *prpRkey);	
	void CalculateGatAllocTimes(DIAFLIGHTDATA &rrpFlight);
	void CalculateWroAllocTimes(DIAFLIGHTDATA &rrpFlight);
	void CalculateBltAllocTimes(DIAFLIGHTDATA &rrpFlight);

	bool UpdateBarTimes();


 	int GetWroCount(DIAFLIGHTDATA *prpFlight, CString opWro);
		
	int GetFlightsAtBlt(const CTime &ropStartAlloc, const CTime &ropEndAlloc, const CString &ropBlt, DIAFLIGHTDATA *prpFlight, CPtrArray *popFlights);
	int GetMaxOverlapAtBlt(const CTime &ropStartAlloc, const CTime &ropEndAlloc, const CString &ropBlt, DIAFLIGHTDATA *prpFlight, int& ipPax);

	int GetFlightsAtGate(const CTime &ropStartAlloc, const CTime &ropEndAlloc, const CString &ropGat, const DIAFLIGHTDATA *prpFlight, CPtrArray *popFlights) const;
	int GetMaxOverlapAtGate(const CTime &ropStartAlloc, const CTime &ropEndAlloc, const CString &ropGat, const DIAFLIGHTDATA *prpFlight);
	int GetMaxOverlapAtGateAndPax(const CTime &ropStartAlloc, const CTime &ropEndAlloc, const CString &ropGat, const DIAFLIGHTDATA *prpFlight, int& ipPax);

	int GetMaxTimerangeOverlap(const CTime &ropStartAlloc, const CTime &ropEndAlloc, const CTime *opFromTimes, const CTime *opToTimes, int ipTimesCount);
	int GetMaxTimerangeOverlapAndPax(const CTime &ropStartAlloc, const CTime &ropEndAlloc, const CTime *opFromTimes, const CTime *opToTimes, int ipTimesCount, const CUIntArray& olPaxArray, int& ipPax);


	int GetFlightsAtPosition(const DIAFLIGHTDATA *prpFlightA, const DIAFLIGHTDATA *prpFlightD, const CString &ropPst, const CTime &ropAllocStart, const CTime &ropAllocEnd);
	int GetFlightsAtPosition(const CTime &ropStartAlloc, const CTime &ropEndAlloc, const CString &ropPst, CPtrArray *popFlights);

	int GetFlightsAtWro(const CTime &ropStartAlloc, const CTime &ropEndAlloc, const CString &ropWro, DIAFLIGHTDATA *prpFlight, CPtrArray *popFlights);
	int GetMaxOverlapAtWro(const CTime &ropStartAlloc, const CTime &ropEndAlloc, const CString &ropWro, DIAFLIGHTDATA *prpFlight);
	int GetMaxOverlapAtWroAndPax(const CTime &ropStartAlloc, const CTime &ropEndAlloc, const CString &ropWro, DIAFLIGHTDATA *prpFlight, int& ipPax);
 
	bool ShallFollowTimeline(const DIAFLIGHTDATA *prpFlight);
	bool IsLastOfRotation(const DIAFLIGHTDATA *prpFlight);

	bool FlightIsOfbl(DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD);
	DIAFLIGHTDATA* GetFirstTowInRot(RKEYLIST *popRotation);
	DIAFLIGHTDATA* GetLastTowInRot(RKEYLIST *popRotation);
	bool CheckRelatedFlightConflicts(DIAFLIGHTDATA *prlFlight);


	//END SAS

	RKEYLIST *GetRotationByRkey(long lpRkey);

	bool UpdateFlight(DIAFLIGHTDATA *prpFlight, DIAFLIGHTDATA *prpFlightSave, bool bpNCC = false);

	DIAFLIGHTDATA * GetFlightAInRotation(RKEYLIST *popRotation);
	DIAFLIGHTDATA * GetFlightDInRotation(RKEYLIST *popRotation);


    //@ManMemo: Handle Broadcasts for flights.
	void ProcessFlightBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

	
	bool CheckCkiAllocate(DIAFLIGHTDATA *prpFlight, CString opCNam);

	bool CheckCkiAllocate(long lpUrno, CString opCNam);

	
	DIAFLIGHTDATA *GetArrival(const DIAFLIGHTDATA *prpFlight);
	DIAFLIGHTDATA *GetDeparture(const DIAFLIGHTDATA *prpFlight);
	//DIAFLIGHTDATA *GetJoinFlight(DIAFLIGHTDATA *prpFlight);

	bool AddToKeyMap(DIAFLIGHTDATA *prpFlight, bool bpCheck = true );
	bool DeleteFromKeyMap(DIAFLIGHTDATA *prpFlight, bool bpAddInButtonList = false );
	DIAFLIGHTDATA *GetFlightByUrno(long lpUrno);

	void GetFlightsAtPositions( CString opPst, CCSPtrArray<DIAFLIGHTDATA> &rlFlights);

	bool PreChangeFlightPos(DIAFLIGHTDATA &rrpFlight, char cpFPart, const CString &ropNewPos);
	bool PreChangeFlightBlt(DIAFLIGHTDATA &rrpFlight, const CString &ropNewBlt, const CString &ropNewTerm, int ipBltNo, bool bpWithExit = false);
	bool PreChangeFlightWro(DIAFLIGHTDATA &rrpFlight, const CString &ropNewWro, int ipWroNo);
	bool PreChangeFlightGat(DIAFLIGHTDATA &rrpFlight, char cpFPart, const CString &ropNewGat, int ipGatNo);

	bool ChangeFlightPos(const DIAFLIGHTDATA &rrpFlight, char cpFPart, const CString &ropNewPos);
	bool ChangeFlightBlt(const DIAFLIGHTDATA &rrpFlight, const CString &ropNewBlt, const CString &ropNewTerm, int ipBltNo, bool bpWithExit = false);
	bool ChangeFlightWro(const DIAFLIGHTDATA &rrpFlight, const CString &ropNewWro, int ipWroNo);
	bool ChangeFlightGat(const DIAFLIGHTDATA &rrpFlight, char cpFPart, const CString &ropNewGat, int ipGatNo);


	CBrush *GetDefBarColor(const DIAFLIGHTDATA *prpFlightA, const DIAFLIGHTDATA *prpFlightD) const;


	bool UpdateFlight(long lpUrno, CString opSelection, CString opFieldList, CString opDataList);



	bool ProcessFlightUFR(BcStruct *prlBcStruct);
	//bool ProcessFlightIFR(BcStruct *prlBcStruct);
	bool ProcessFlightISF(BcStruct *prlBcStruct);
	void ProcessCflChange(CFLDATA *prpCfl);
	void ProcessCcaChange(long *lpUrno);
	void ReloadFlights(void *vpDataPointer);
	void ReloadFlightsRAC(void *vpDataPointer);

	bool bmRotation;
	bool bmReadAll;
	
	CString omFtyps;
	CTime omFrom;
	CTime omTo;

	CString omSelection;
	CString omFOGTAB;
	bool bmReadAllFlights;
	long lmPosReasonUrno;
	long lmGatReasonUrno;

	CedaDiaCcaData omCcaData;

	void SetCcaData(DIAFLIGHTDATA *prpFlight);


	CString GetFieldList() { return CString(pcmFieldList);};

	bool Connect();
	bool Disconnect();


	static int CompareRotationFlight(const DIAFLIGHTDATA **e1, const DIAFLIGHTDATA **e2);
	static int CompareByRkey(const DIAFLIGHTDATA **e1, const DIAFLIGHTDATA **e2);


	CedaCflData *pomCflData;

	bool GetSelectedFlights(CCSPtrArray<DIAFLIGHTDATA> &olSelectedFlights, int ipSubTyp, bool bpOnlySel = false, bool bpMustBeInOneDay = true);
	bool Expand(long lpUrno, CTime opFrom , CTime opTo, CUIntArray& opDayArray, CMapStringToString& olMustHaveMap, CStringArray& opCopied, CStringArray& opNotCopied, CMapStringToString& opTypeMap, bool bpOverwrite = true);
	bool SearchFlightsToJoin(CString olFlno, CTime opDateFrom, CTime opDateTo, char clAdid, CCSPtrArray<DIAFLIGHTDATA> &opJoinData);
	bool ExpandFlightAllocations(DIAFLIGHTDATA* prlSrcFlight, DIAFLIGHTDATA* prlFlight, CTime opDate, CMapStringToString& opTypeMap, bool bpOverwrite, CStringArray& opResultMap, CMapStringToString& opFailedMap, bool bpDoCopy = false);
	void WriteMap(CString& opKey, CString& opObj, CString& opName, CTime& opBeg, CTime& opEnd, CString& opPostStr, CStringArray& opResultMap);

	bool GetExit(const CString &opBlt, int ipBelt, CString &ropExt);
	bool HasTwoGates(const DIAFLIGHTDATA *prpFlight);
	bool HasTwoWro(const DIAFLIGHTDATA *prpFlight);
	bool IsAnythingOpen(bool bpIncludePosition, const DIAFLIGHTDATA &popFlight, CString& opOpenResources);
	bool DeleteAllResourses(const DIAFLIGHTDATA &popFlight);

	bool CheckFlightConflicts(DIAFLIGHTDATA *prpFlight);

	void SaveReasons(DIAFLIGHTDATA *prpFlight);


private:
	bool bmSearchMode;
	bool bmWithConflicts;

	bool UpdateOfflineButtons();
	void SetOffLine();
	void Release();

	void CorrectNegativeBars(CTime &ropStart, CTime &ropEnd) const;
	void WriteMapToExcel(ofstream& of, CMapStringToString& opResultMap, char* opTrenner);

	bool UpdatePosBarTimes();
	bool UpdateNowButtons();
	bool CorrectPstAllocToCurrTime(const DIAFLIGHTDATA *prpFlightA, const DIAFLIGHTDATA *prpFlightD, int ipRotSize, int ipRotPos, const CTime &ropCurrUtcTime, CTime &ropAllocEnd, bool& bpOnTimeMap);
	bool CorrectGatAllocToCurrTime(const DIAFLIGHTDATA &rrpFlight, char cpFPart, int ipGatNo, const CTime &ropCurrUtcTime, CTime &ropAllocEnd, bool& bpOnTimeMap);
	bool CorrectBltAllocToCurrTime(const DIAFLIGHTDATA &rrpFlight, int ipBltNo, const CTime &ropCurrUtcTime, CTime &ropAllocEnd, bool& bpOnTimeMap);
	bool CorrectWroAllocToCurrTime(const DIAFLIGHTDATA &rrpFlight, int ipWroNo, const CTime &ropCurrUtcTime, CTime &ropAllocEnd, bool& bpOnTimeMap);

//	bool CheckFlightConflicts(DIAFLIGHTDATA *prpFlight);

	bool CedaActionUFRAFT(long lpUrno, char *pcpFieldList, char *pcpSelection, char *pcpData, DIAFLIGHTDATA *prpFlightSave, bool bpIsWriteAction = false , bool bpNCC = false );
	bool ResetOffRelStatus(DIAFLIGHTDATA *prpFlight);

	bool ProcessFlightUPSJ(const BcStruct *prpBcStruct);
	bool ProcessFlightJOF(const BcStruct *prpBcStruct);
	bool ProcessFlightSPR(const BcStruct *prpBcStruct);

	bool WoResKeyMap(DIAFLIGHTDATA *prpFlight);

	int imConnectCounter;
	bool bmActTimeInTimerange; 
	CMapPtrToPtr omNCCMap;

	int UpdatePosBarTimesMap();
	int UpdateGatBarTimesMap();
	int UpdateBltBarTimesMap();
	int UpdateWroBarTimesMap();
	bool OnTimelineMap(DIAFLIGHTDATA *prpFlight, CString opAlloc, bool bpSet = true);
	CString omAdidIn;
};

#endif
