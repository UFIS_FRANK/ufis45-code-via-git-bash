#ifndef __SPOT_ALLOCATION__

#include <SpotAllocateDlg.h>// TSC
#include <CedaBasicData.h>
#include <CCSGlobl.h>
#include <DiaCedaFlightData.h>
#include <CCSTime.h>
#include <Konflikte.h>
// "SpotAllocation.h" HeaderFile

struct PST_RESLIST
{
	CString	  Urno;
	CString   Pnam;
	double SpanMin;
	double SpanMax;
	double LengMin;
	double LengMax;
	double HeighMin;
	double HeighMax;
	CTime  Nafr;
	CTime  Nato;
	// Aircrafttype restrictions
	CString ACUserStr;
	CStringArray AssignedAC;
	CStringArray ExcludedAC;
	CStringArray PreferredAC;

	// 20070703 Origin/Destination restriction
	CString OrgDesUserStr;
	CStringArray AssignedOrgDes;
	CStringArray ExcludedOrgDes;
	CStringArray PreferredOrgDes;


	// 050301 MVy: Aircraft Registration restrictions
	CString AcrRegnUserStr ;
	CStringArray AssignedAcrRegn ;
	CStringArray ExcludedAcrRegn ;
	CStringArray PreferredAcrRegn ;

	CMapStringToString PosAndMac;
	CString Pos1;
	double Mac1; //restricted to
	CString Pos2;
	double Mac2;
	int Mult; // Max A/C
	int Prio; // Priority
	CString Brgs;
	// Nature restrictions
	CString NAUserStr;
	CStringArray AssignedNA;
	CStringArray ExcludedNA;
	CStringArray PreferredNA;
	// Airline restrictions
	CString ALUserStr;
	CStringArray AssignedAL;
	CStringArray ExcludedAL;
	CStringArray PreferredAL;

	int Sequence; // Sequence
	CString Groundtime; //Groundtime

	CStringArray RelGats; // Related Gates

	// service restrictions
	CString ServiceUserStr;
	CStringArray AssignedService;
	CStringArray ExcludedService;
	CStringArray PreferredService;

	//Allowed fight ids
	CString FltiArr;
	CString FltiDep;

	
	//For AirCraft Groups//
	CString ACGroupUserStr;
	CStringArray AssignedACGroup;
	CStringArray ExcludedACGroup;
	CStringArray PreferredACGroup;

	PST_RESLIST()
	{
		Urno = "0";SpanMin = 0.;SpanMax = 99999.;LengMin = 0.;LengMax = 99999.;HeighMin= 0.;HeighMax= 99999.;
		Pos1 = "0";Mac1 = 0;Pos2 = "0";Mac2 = 0;Mult = 0;Prio = 0;
		//Exac = "";Natr = "";Pral = "";
		ACUserStr = ""; NAUserStr = ""; ALUserStr = ""; ServiceUserStr = "";  ACGroupUserStr = ""; OrgDesUserStr = "";
		Nafr = TIMENULL;Nato = TIMENULL;
		Sequence = 1;
		FltiArr = "";
		FltiDep = "";

	}
};

struct BLT_RESLIST
{
	CString Urno;
	CString Bnam;

	int     Prio;
	int Sequence; // Sequence
	int     Maxp;

	// Airline restrictions
	CString ALUserStr;
	CStringArray AssignedAL;
	CStringArray ExcludedAL;
	CStringArray PreferredAL;

	// Org/Des restrictions
	CString OrgDesUserStr;
	CStringArray AssignedOrgDes;
	CStringArray ExcludedOrgDes;
	CStringArray PreferredOrgDes;

	// Nature restrictions
	CString NAUserStr;
	CStringArray AssignedNA;
	CStringArray ExcludedNA;
	CStringArray PreferredNA;

	// service restrictions
	CString ServiceUserStr;
	CStringArray AssignedService;
	CStringArray ExcludedService;
	CStringArray PreferredService;

	//AirCraft Parameter
	CString ACUserStr;
	CStringArray AssignedAC;
	CStringArray ExcludedAC;
	CStringArray PreferredAC;
	
	//AirCraft Groups
	CString ACGroupUserStr;
	CStringArray AssignedACGroup;
	CStringArray ExcludedACGroup;
	CStringArray PreferredACGroup;

	
	CString Flti;

	BLT_RESLIST()
	{
		Urno = "0";
		Prio = 0;
		Sequence = 0;
		Maxp = 0;
		ALUserStr = "";
		OrgDesUserStr = "";
		NAUserStr = "";
		ServiceUserStr = "";
		Flti = "";
		ACGroupUserStr = "";
		ACUserStr = "";

	}

};


struct GAT_RESLIST
{
	CString Urno;
	CString Gnam;
	CString Busg;
	CString Flti;
	CString FltiDep;
	CString RelPos1;
	CString RelPos2;
	int     Maxp;
	int     Mult;
	int     Prio;
	bool	Inbo;
	bool    Outb;
	//CString Natr;
	//CString Pral;
	CTime   Nafr;
	CTime   Nato;

	// Airline restrictions
	CString ALUserStr;
	CStringArray AssignedAL;
	CStringArray ExcludedAL;
	CStringArray PreferredAL;
	// Org/Des restrictions
	CString OrgDesUserStr;
	CStringArray AssignedOrgDes;
	CStringArray ExcludedOrgDes;
	CStringArray PreferredOrgDes;
	// Nature restrictions
	CString NAUserStr;
	CStringArray AssignedNA;
	CStringArray ExcludedNA;
	CStringArray PreferredNA;

	int Sequence; // Sequence

	// service restrictions
	CString ServiceUserStr;
	CStringArray AssignedService;
	CStringArray ExcludedService;
	CStringArray PreferredService; 

	//AirCraft Parameter
	CString ACUserStr;
	CStringArray AssignedAC;
	CStringArray ExcludedAC;
	CStringArray PreferredAC;

	//For AirCraft Groups//
	CString ACGroupUserStr;
	CStringArray AssignedACGroup;
	CStringArray ExcludedACGroup;
	CStringArray PreferredACGroup;

	CMapStringToString GateNeighbours;

	GAT_RESLIST()
	{
		Urno = "0";Flti = "";FltiDep = "";RelPos1="";RelPos2="";Maxp = 999;Mult = 0;Inbo = false;Outb = false;//Natr = "";Pral = "";
		ALUserStr = ""; OrgDesUserStr = ""; NAUserStr = ""; ServiceUserStr = "";  ACGroupUserStr = "";
		Nafr = TIMENULL;Nato = TIMENULL;Busg="";
		Sequence = 1; Prio = 0;
	}

};

struct GAT_ALLOC
{
	CString Urno;
	CTime Dube;
	CTime Duen;
	DIAFLIGHTDATA *prlFlightA;
	DIAFLIGHTDATA *prlFlightD;
	GAT_ALLOC()
	{
		Dube=-1;Duen=-1;prlFlightA=NULL;prlFlightD=NULL;
	}

};

struct PST_ALLOC
{
	CString Urno;
	CString Pnam;
	CTime Dube;
	CTime Duen;
	double Span;
	DIAFLIGHTDATA *prlFlightA;
	DIAFLIGHTDATA *prlFlightD;
	PST_ALLOC()
	{
		Dube=-1;Duen=-1;Span=0.0;prlFlightA=NULL;prlFlightD=NULL;
	}
};
/*
struct SGM_GATPOS
{
	CString Sort;
	ogBCD.SetObject("SGM","UVAL,VALU,USGR,URNO,UGTY,PRFL"); 
};
*/

struct GAT_DEMAND
{
	int				Paxt;
	int				Dura;
	CTime			Dube;
	CTime			Duen;
	DIAFLIGHTDATA	*pFlightA;
	DIAFLIGHTDATA	*pFlightD;
};

struct PST_DEMAND
{
	double			Span;
	int				Dura;
	CTime			Dube;
	CTime			Duen;
	DIAFLIGHTDATA	*pFlightA;
	DIAFLIGHTDATA	*pFlightD;
	GAT_DEMAND		*pGatDem;
	CString Type; //(I)nbound, (O)utbound, (T)owing
	PST_DEMAND()
	{
		Span = 0.0;	Dura = 0; Dube = -1; Duen = -1; pFlightA = NULL; 
		pFlightD = NULL; pGatDem  = NULL; Type = ""; 
	}
};


 class SpotAllocation
{
public:

	void Userallocatevector(CString userdatei);
	
	std::vector <CString> vData;	

	SpotAllocation();
	
	~SpotAllocation();

	void SetAllocateParameters(AllocateParameters *popParameters);

	void SetParent( CWnd *popParent);

	void Init();

	void ResetAllocation(bool bpPst, bool bpGat, bool bpBlt, bool bpWro);


	void InitNatRestriction();
	bool CheckGatGlobalNat(CString& opTyp, bool& opAlga, bool& opAlpo, bool& opAlbb, bool& opAlwr);
	bool GetLocalGlobalNat(RecordSet *prpRecord, CString& opTyp, bool& opAlga, bool& opAlpo, bool& opAlbb, bool& opAlwr);
	void GlobalNatureChange(RecordSet *prpRecord);
	void InitPstLinksToTwy();
	void RuleChanged(RecordSet* vpDataPointer, int ipDDXType);


	void CreatePositionRules();
	void GetAcgrFromAcgrList(PST_RESLIST *prpPstRes, CStringArray &opAcgr);

	void CreateBeltRules();
	void CreateGateRules();

	AllocateParameters omParameters;


	bool CheckPst(DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD, CString opPst, CCSPtrArray<KonfItem> *popKonfList = NULL, CTimeSpan *popRightBuffer = NULL, CTimeSpan *popLeftBuffer = NULL, bool bpPreCheck = false);
	bool CheckPstNeighborConf(DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD, const CString &ropPst, CCSPtrArray<KonfItem> *popKonfList = NULL, CTimeSpan *popRightBuffer = NULL, CTimeSpan *popLeftBuffer = NULL, bool bpPreCheck = false);
//	bool CheckGat(const DIAFLIGHTDATA *prpFlight, char cpFPart, CString opGat, int ipGatNo , CCSPtrArray<KonfItem> *popKonfList, const CTimeSpan *popRightBuffer = NULL, const CTimeSpan *popLeftBuffer = NULL, bool bpPreCheck = false);
	
	bool CheckGat(const DIAFLIGHTDATA *prpFlight, char cpFPart, CString opGat, int ipGatNo , CCSPtrArray<KonfItem> *popKonfList, const CTimeSpan *popRightBuffer = NULL, const CTimeSpan *popLeftBuffer = NULL, bool bpPreCheck = false, CString opPrePos = CString(""));
	
	bool CheckWro(DIAFLIGHTDATA *prpFlightD, CString opWro, int ipWroNo, CCSPtrArray<KonfItem> *popKonfList = NULL, bool bpPreCheck = false);
	bool CheckBlt(DIAFLIGHTDATA *prpFlightA, CString opBlt, int ipBltNo, CCSPtrArray<KonfItem> *popKonfList, int ipMaxCount = 0, bool bpPreCheck = false);
//	bool CheckBlt(const DIAFLIGHTDATA *prpFlightA, CString opBlt, int ipBltNo, CCSPtrArray<KonfItem> *popKonfList, int ipMaxCount);

	bool CheckGatNeighborConf(const DIAFLIGHTDATA *prpFlight, char cpFPart, const CString &opGat, int ipGatNo, CCSPtrArray<KonfItem> *popKonfList /* = NULL */, const CTimeSpan *popRightBuffer /* = NULL */, const CTimeSpan *popLeftBuffer /* = NULL */, bool bpPreCheck /*false*/);
	int GetAllGatWhereImNeighbour(GAT_RESLIST* prlPstResFrom, CMapStringToString& opNeighbours);

	bool CheckPstWithTimer(DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD, CString opPst, CCSPtrArray<KonfItem> *popKonfList, CTimeSpan *popRightBuffer, CTimeSpan *popLeftBuffer, bool bpPreCheck);

	bool ReCheckAllChangedFlights(bool bpCheckPst, bool bpCheckGat,bool bpCheckWro, bool bpCheckBlt);

	void CreateFlightList();


	bool AllocatePst(DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD, CString opPst);

	bool AllocateGat(DIAFLIGHTDATA *prlFlightA, DIAFLIGHTDATA *prlFlightD, CString opGat, int ipGat = 1);

	bool AllocateWro(DIAFLIGHTDATA *prlFlightD, CString opWro, int ipWro);

	bool AllocateBlt(DIAFLIGHTDATA *prlFlightA, CString opBlt, int ipBelt);


	bool GetPositionsByRule(DIAFLIGHTDATA *prlFlightA, DIAFLIGHTDATA *prlFlightD, CStringArray &opPsts );
	bool GetGatesByRule(DIAFLIGHTDATA *prlFlight, char cpFPart, CStringArray &opGates, int ipGatNo );
	bool GetBeltsByRule(DIAFLIGHTDATA *prlFlight, CStringArray &opBelts);
	bool GetWrosByRule(DIAFLIGHTDATA *prlFlight, CStringArray &opWros);

 
	bool CheckPosGateRelation(DIAFLIGHTDATA *prpFlight, char cpFPart, const CString &ropPst);

	CString GetGatFlightId(CString opGat);


	PST_RESLIST *GetPstRes(const CString &ropPst);
	GAT_RESLIST *GetGatRes(const CString &ropGat);
	BLT_RESLIST *GetBltRes(const CString &ropBlt);


	CCSPtrArray<PST_RESLIST> omPstRules;
	CCSPtrArray<GAT_RESLIST> omGatRules;
	CCSPtrArray<BLT_RESLIST> omBltRules;


	CCSPtrArray<DiaCedaFlightData::RKEYLIST> omFlights;

	void SetGatSort(CString opGnam);
  

	bool AutoPstAllocate();
	bool AutoPstAllocateFlights(DIAFLIGHTDATA *prlFlightA, DIAFLIGHTDATA *prlFlightD, const CString &ropRecomPos);

	bool AutoGatAllocate();
	bool AutoGatAllocatePstFlight(DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD);

	//bool AutoGatAllocateWithoutPst();

	bool AutoGatAllocatePst();

	bool AutoWroAllocate();

	bool AutoBltAllocate();
	bool AutoBltAllocateFlight(DIAFLIGHTDATA *prlFlightA, int ipBltNo);

	bool MakeBlkData(CTime &opStartTime, CTime &opEndTime,const CString &opTabn,const CString &opName, CStringArray &opNafr, CStringArray &opNato, CStringArray &opResn, CUIntArray &opIbit);

	bool PrintSortedFlightsAutoAlloc(CString& opType);
	bool PrintAutoAlloc(bool& bpPst, bool& bpGat, bool& bpBlt);

	bool bmPstConf;
	bool bmGatConf;
	bool bmWroConf;
	bool bmBltConf;

//MWO: 19.04.05 Mixed Flights considerations for Belts

	CString GetBltFlti( CString opBlt);
	bool HavePstGatRel( CString opPst);
	CString GetPstGatRelBlt( CString opPst, int ipGatNo);
//END MWO: 19.04.05 Mixed Flights considerations for Belts

	CStringArray omGatNatNoAllocList;
	CStringArray omPstNatNoAllocList;
	CStringArray omBltNatNoAllocList;
	CStringArray omWroNatNoAllocList;
	CString omGatNatNoAllocListPlain;
	CString omPstNatNoAllocListPlain;
	CString omBltNatNoAllocListPlain;
	CString omWroNatNoAllocListPlain;

	CStringArray omPstLinksToTwy;

	CWnd *pomParent;

	bool GetResBy(const CString& ropRes,const CString& ropBy, const CString& ropByName,CStringArray& ropGatesBLUE, CMapStringToString& ropGatesMapBLUE,CStringArray& ropGatesRED, CMapStringToString& ropGatesMapRED, DIAFLIGHTDATA* rrpFlight, bool bpTakeBufferTimes = false, bool bpWithoutBLK = false, bool bpPreCheck = false, bool bpOnlyInResChain = false, int ipResNo = 1);
//	bool GetPositionsBy(const CString& ropBy, const CString& ropByName,CStringArray& ropPosBLUE, CMapStringToString& ropPosMapBLUE,CStringArray& ropPosRED, CMapStringToString& ropPosMapRED, const DIAFLIGHTDATA* rrpFlight);
//	bool GetLoungesBy(const CString& ropBy, const CString& ropByName,CStringArray& ropWroBLUE, CMapStringToString& ropWroMapBLUE,CStringArray& ropWroRED, CMapStringToString& ropWroMapRED, const DIAFLIGHTDATA* rrpFlight);
	bool IsAvailable(const CString& ropType,const CString& ropName,CTime opStartAlloc,CTime opEndAlloc);
//	bool ResIsAvailable(DIAFLIGHTDATA *popFlight, const CString& ropType, const CString& ropTarget, bool bpTakeBufferTimes = false, bool bpPreCheck = false);
	bool ResIsAvailable(DIAFLIGHTDATA *popFlight, const CString& ropType, const CString& ropTarget, bool bpTakeBufferTimes = false, bool bpPreCheck = false, CString opPrePos = CString(""), int ipResNo = 1);
	bool GetGATPOSPossibleGates(DIAFLIGHTDATA *prpFlight, CStringArray& olPossibleGat, int ipGatNo = 1);
	bool GetGATPOSPossibleBelts(DIAFLIGHTDATA *prlFlightA, CStringArray &opPossibleBlt, int ipBeltNo);

	bool InitializePositionMatrix();
	bool ReleasePositionMatrix();
	bool InitializePositionMatrix(CString& opAloc,CString& opGrp);
	bool ReleasePositionMatrix(CMapStringToPtr* omPosMatrixAlocGrp);

	bool SetTerminals(DIAFLIGHTDATA *prpFlight, CString& opType);
	bool CheckWarningAllocationBufferTime(DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD, CString opPst, CCSPtrArray<KonfItem> *popKonfList, CTimeSpan *popRightBuffer, CTimeSpan *popLeftBuffer, bool pbAutoAlloc);
	int GetAllPstWhereImNeighbour(PST_RESLIST* prlPstRes, CMapStringToString& olPosAndMacNeighbours, CMapStringToPtr& olPosAndSpanNeighbours);

private:
//	bool CheckWingoverlap(const DIAFLIGHTDATA *prpFlight, const CTime &ropStartAlloc, const CTime &ropEndAlloc, const CString &ropPos, 
//						  double dpPosSpanMax, double dpPosMac, double dpMinDist, double dpACSpan, DIAFLIGHTDATA *&prpFlightTmp, const CString &ropPosFlight);
	bool CheckWingoverlap(const DIAFLIGHTDATA *prpFlight, const CTime &ropStartAlloc, const CTime &ropEndAlloc, const CString &ropPos, 
						  double dpPosSpanMax, double dpPosMac, double dpMinDist, double dpACSpan, DIAFLIGHTDATA *&prpFlightTmp, const CString &ropPosFlight,CCSPtrArray<DIAFLIGHTDATA> &ropFlights, int ipDoNotBack);
	void ParsePrefExclAssiStr(const CString &ropStr, CStringArray &ropPreferred, CStringArray &ropExcluded, CStringArray &ropAssigned) const;

	// fills the requested fields of table in a member array with the members of a group array
	void ExpandPrefExclAssiStrWithGroups(CStringArray &ropPreferredGrp, CStringArray &ropExcludedGrp, CStringArray &ropAssignedGrp, CStringArray &ropPreferred, CStringArray &ropExcluded, CStringArray &ropAssigned, CString ropTable, CString ropField1, CString ropField2);

//	bool CheckResToResLists(const CString &ropTestStr, const CStringArray &ropAssignedStrs, const CStringArray &ropExcludedStrs) const;
//	bool CheckResToResLists(const CString &ropTestStr1, const CString &ropTestStr2, const CStringArray &ropAssignedStrs, const CStringArray &ropExcludedStrs) const;
	bool CheckResToResLists(const CString &ropTestStr, const CStringArray &ropAssignedStrs, const CStringArray &ropExcludedStrs, const CStringArray &ropPreferredStrs) const;
	bool CheckResToResLists(const CString &ropTestStr1, const CString &ropTestStr2, const CStringArray &ropAssignedStrs, const CStringArray &ropExcludedStrs, const CStringArray &ropPreferredStrs) const;
	bool CheckResToResLists(const CString &ropTestStr1, const CString &ropTestStr2, const CString &ropTestStr3, const CString &ropTestStr4,const CStringArray &ropAssignedStrs, const CStringArray &ropExcludedStrs, const CStringArray &ropPreferredStrs) const;

	bool GetMatchCountPst(const DIAFLIGHTDATA *prpFlightA, const DIAFLIGHTDATA *prpFlightD, const CStringArray &ropPreferredAL, const CStringArray &ropPreferredAC, const CStringArray &ropPreferredNA, const CStringArray &ropPreferredDest,
							float &rfpGoodCount, int &ripBadCount) const;

	bool GetMatchCountPst(const DIAFLIGHTDATA *prpFlightA, const DIAFLIGHTDATA *prpFlightD, const CStringArray &ropPreferredAL, const CStringArray &ropPreferredAC, const CStringArray &ropPreferredNA,
							const CStringArray &ropPreferredService, const CStringArray &ropPreferredDest, float &rfpGoodCount, int &ripBadCount, CString& opMatchStr) const;

	bool GetMatchCountPst(const DIAFLIGHTDATA *prpFlightA, const DIAFLIGHTDATA *prpFlightD, PST_RESLIST *prpPstRule, float &rfpGoodCount, int &ripBadCount, CString& opMatchStr) const;

	bool GetMatchCountGat(const DIAFLIGHTDATA *prpFlight, const CStringArray &ropPreferredAL, const CStringArray &ropPreferredDest, const CStringArray &ropPreferredNA, 
							float &rfpGoodCount, int &ripBadCount) const;

	bool GetMatchCountGat(const DIAFLIGHTDATA *prpFlight, BLT_RESLIST *prpPstRule, float &rfpGoodCount, int &ripBadCount, CString& opMatchStr) const;
	bool GetMatchCountGat(const DIAFLIGHTDATA *prpFlight, GAT_RESLIST *prpPstRule, float &rfpGoodCount, int &ripBadCount, CString& opMatchStr) const;

	bool CheckGatFlightId(const CString &ropGat, const CString &ropFlti); 

	bool GetBestGate(const CString &ropFtliGat, const DIAFLIGHTDATA *prpFlightA, const DIAFLIGHTDATA *prpFlightD, 
					 int ipGateNo, const CTimeSpan *popGatLeftBuffer, const CTimeSpan *popGatRightBuffer, CString &ropBestGate);

	bool GetBestGate(const CString &ropFtliGat, const DIAFLIGHTDATA *prpFlight, char cpFPart,
					 int ipGateNo, const CTimeSpan *popGatLeftBuffer, const CTimeSpan *popGatRightBuffer, CString &ropBestGate);

	bool GetBestBelt(const DIAFLIGHTDATA *prpFlight, CStringArray& opGatBlts, int ipBeltNo, CString &opBestBelt, ofstream& of);

	bool GetBestBeltGatPos(DIAFLIGHTDATA *prpFlight, CString &opBestBelt, int ipBeltNo, ofstream& of);

	void AddToKonfList(const DIAFLIGHTDATA *prpFlightA, const DIAFLIGHTDATA *prpFlightD, long lpRelFUrno, char cpRelFPart, int ipType, const CString &ropConfText, CCSPtrArray<KonfItem> &ropKonfList);
	void AddToKonfList(const DIAFLIGHTDATA *prpFlight, char cpFPart, long lpRelFUrno, char cpRelFPart, int ipType, const CString &ropConfText, CCSPtrArray<KonfItem> &ropKonfList);

	bool GetRelatedBlts(const DIAFLIGHTDATA *prpFlight, int ipBltNo, CStringArray &ropBlts);

	CMapPtrToPtr omChangedFlightsRkeyMap;

	bool GetGatPosEnv(const CString& ropRes, const CString& ropBy, CString& olALOC, CString& olTAB, CString& olNAM, CString& olNAMRes, bool& backwards);
//	bool AutoBltAllocateFlightGatPos(DIAFLIGHTDATA *prlFlightA, int ipBltNo);
	bool AutoBltAllocateFlightGatPos(DIAFLIGHTDATA *prlFlightA, int ipBltNo, ofstream& of);
	bool AutoWroAllocateFlightGatPos(DIAFLIGHTDATA *prlFlightD, int ipWroNo);
	bool AllocateBltGatPos(DIAFLIGHTDATA *prlFlightA, CString opBlt, int ipBelt);
	bool AutoGatAllocatePstGatPos();
//	bool AutoGatAllocatePstFlightGatPos(DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD);
	bool AutoGatAllocatePstFlightGatPos(DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD, CString& opAllocatedGate, ofstream& of);
	bool AutoPstAllocateFlightsGatPos(DIAFLIGHTDATA *prlFlightA, DIAFLIGHTDATA *prlFlightD, const CString &ropRecomPos, CString& ropAllocatedPos, ofstream& of);
	bool CorrectPossibleMap(const CString& ropRes, const CString& ropBy, CStringArray& ropPossible, CMapStringToString& ropPossibleMap, CStringArray& ropNotCorrected, DIAFLIGHTDATA* prpFlight, int ipAllocNo);
//	bool GetBestGateGatPos(const CString &ropFtliGat, DIAFLIGHTDATA *prpFlight, char cpFPart,
//		int ipGateNo, const CTimeSpan *popGatLeftBuffer, const CTimeSpan *popGatRightBuffer, CString &ropBestGate);

	bool GetBestGateGatPos(const CString &ropFtliGat, DIAFLIGHTDATA *prpFlight, char cpFPart,
					 int ipGateNo, const CTimeSpan *popGatLeftBuffer, const CTimeSpan *popGatRightBuffer,
					 CString &ropBestGate, ofstream& of);

//	bool AutoPstAllocateFlightsGatPos(DIAFLIGHTDATA *prlFlightA, DIAFLIGHTDATA *prlFlightD, const CString &ropRecomPos);
	bool CheckPosByGate(const CString& ropRecomPos, DIAFLIGHTDATA *prpFlight);
//	int GetAllPstWhereImNeighbour(PST_RESLIST* prlPstRes, CMapStringToString& olPosAndMacNeighbours, CMapStringToPtr& olPosAndSpanNeighbours);
	LONG CheckPositionMatrix(const CString& ropFromPos,const CString& ropToPos,const CString& ropFromAct3,const CString& ropFromAct5,const CString& ropToAct3,const CString& ropToAct5);		
	LONG CheckPositionMatrix(const CString& ropAloc,const CString& ropGrp,const CString& ropFromPos,const CString& ropToPos,const CString& ropFromAct3,const CString& ropFromAct5,const CString& ropToAct3,const CString& ropToAct5);		
	bool CheckIncomp(CString& opAlo, CString& opGrp,const DIAFLIGHTDATA *prpFlight, const CTime &ropStartAlloc, const CTime &ropEndAlloc, const CString &ropPos, 
						DIAFLIGHTDATA *&prpFlightTmp, const CString &ropPosFlight,CCSPtrArray<DIAFLIGHTDATA> &ropFlights, int ipDoNotBack, CStringArray* opAptArray = NULL);

	bool ResetFlightAllocation(DIAFLIGHTDATA *prlFlight, bool bpPst, bool bpGat, bool bpBlt, bool bpWro, CTime& opCurrUtc);

	CMapStringToPtr	omPosMatrix;
	CMapStringToPtr	omPosMatrixPosAlt;
	CMapStringToPtr	omPosMatrixPosApt;
	CMapStringToPtr	omPosMatrixGatAlt;
	CMapStringToPtr	omPosMatrixGatApt;

	bool PrintSortedRulesPos();
	bool PrintSortedRulesGat();
	bool PrintSortedRulesBlt();

	bool GetFlnoForPrintMap(const DIAFLIGHTDATA *prpFlight, CString& opFlno);

	CMapStringToString omAutoAllocPst;
	CMapStringToString omAutoAllocGat;
	CMapStringToString omAutoAllocBlt;
	CMapStringToString omAutoAllocWro;

	int imMaxFBelts;
	int imActMaxF;

	CMapPtrToPtr omChangedUrnoMap;
};

#endif //__GATPOS_ALLOCATION__