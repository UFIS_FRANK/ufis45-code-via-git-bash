// RotationActDlg.cpp : implementation file
//

#include <stdafx.h>
#include <fpms.h>
#include <RotationActDlg.h>

#include <CCSGlobl.h>
#include <CedaBasicData.h>
#include <AwBasicDataDlg.h>
#include <BasicData.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void ActCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);


/////////////////////////////////////////////////////////////////////////////
// RotationActDlg dialog


RotationActDlg::RotationActDlg(CWnd* pParent /*=NULL*/, CString opAct)
	: CDialog(RotationActDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(RotationActDlg)
	m_Act3 = _T("");
	m_Act5 = _T("");
	m_Acfn = _T("");
	//}}AFX_DATA_INIT
		
	if(opAct.GetLength() > 3)
		m_Act5 = opAct;
	else
		m_Act3 = opAct;

    ogDdx.Register(this, APP_EXIT, CString("ACTDLG"), CString("Appli. exit"), ActCf);

}


RotationActDlg::~RotationActDlg()
{
	ogDdx.UnRegister(this,NOTUSED);
}


static void ActCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    RotationActDlg *polDlg = (RotationActDlg *)popInstance;

    if (ipDDXType == APP_EXIT)
        polDlg->AppExit();

}


void RotationActDlg::AppExit()
{
	EndDialog(IDCANCEL);
}




BOOL RotationActDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	AppExit(); // This was deactivated, because the checking of conflicts for this aircraft will fail,
				// if it is entered in FIPS!!!

	m_CE_Act5.SetTypeToString("X|#X|#X|#X|#X|#",5,0);
	m_CE_Act3.SetTypeToString("X|#X|#X|#",3,0);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}



void RotationActDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(RotationActDlg)
	DDX_Control(pDX, IDC_ACFN, m_CE_Acfn);
	DDX_Control(pDX, IDC_ACT5, m_CE_Act5);
	DDX_Control(pDX, IDC_ACT3, m_CE_Act3);
	DDX_Text(pDX, IDC_ACT3, m_Act3);
	DDX_Text(pDX, IDC_ACT5, m_Act5);
	DDX_Text(pDX, IDC_ACFN, m_Acfn);
	DDV_MaxChars(pDX, m_Acfn, ogBasicData.AircraftTypeDescriptionLength()+1);
//	DDV_MaxChars(pDX, m_Acfn, 32);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(RotationActDlg, CDialog)
	//{{AFX_MSG_MAP(RotationActDlg)
	ON_MESSAGE(WM_EDIT_KILLFOCUS, OnEditKillfocus)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// RotationActDlg message handlers

void RotationActDlg::OnOK() 
{
	//killfocus - check for the curent field
	NextDlgCtrl(); 
	PrevDlgCtrl();

	m_CE_Act3.GetWindowText(m_Act3);
	m_CE_Act5.GetWindowText(m_Act5);
	m_CE_Acfn.GetWindowText(m_Acfn);


	if(!m_CE_Act5.GetStatus())
		m_Act5 = "";

	if(!m_CE_Act3.GetStatus())
		m_Act3 = "";


	if(m_Act3.IsEmpty() && m_Act5.IsEmpty())
		return;


	RecordSet rlRecord(ogBCD.GetFieldCount("ACT"));				

	rlRecord[ogBCD.GetFieldIndex("ACT", "ACFN")] = m_Acfn;
	rlRecord[ogBCD.GetFieldIndex("ACT", "ACT3")] = m_Act3;
	rlRecord[ogBCD.GetFieldIndex("ACT", "ACT5")] = m_Act5;

	ogBCD.InsertRecord("ACT", rlRecord, true);


/*

	RecordSet *prlRecord = new RecordSet(ogBCD.GetFieldCount("ACT"));
	prlRecord->Values[ogBCD.GetFieldIndex("ACT", "ACFN")] = m_Acfn;
	prlRecord->Values[ogBCD.GetFieldIndex("ACT", "ACT3")] = m_Act3;
	prlRecord->Values[ogBCD.GetFieldIndex("ACT", "ACT5")] = m_Act5;
	ogBCD.InsertRecord("ACT", (*prlRecord), true);
	delete prlRecord;
*/
	CDialog::OnOK();
}



LONG RotationActDlg::OnEditKillfocus( UINT wParam, LPARAM lParam)
{
	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY*)lParam;
	if(prlNotify->Text.IsEmpty())
		return 0L;

	if((UINT)m_CE_Act5.imID == wParam)
	{
		CString olAct3;
		prlNotify->UserStatus = true;
		prlNotify->Status = !ogBCD.GetField("ACT", "ACT5", prlNotify->Text, "ACT3", olAct3 );
		if(prlNotify->Status == false)
			MessageBox(GetString(IDS_STRING936), GetString(ST_FEHLER));
		return 0L;
	}

	if((UINT)m_CE_Act3.imID == wParam)
	{
		CString olAct5;
		prlNotify->UserStatus = true;
		prlNotify->Status = !ogBCD.GetField("ACT", "ACT3", prlNotify->Text, "ACT5", olAct5 );
		if(prlNotify->Status == false)
			MessageBox(GetString(IDS_STRING937), GetString(ST_FEHLER));
		return 0L;
	}
	return 0L;

}
