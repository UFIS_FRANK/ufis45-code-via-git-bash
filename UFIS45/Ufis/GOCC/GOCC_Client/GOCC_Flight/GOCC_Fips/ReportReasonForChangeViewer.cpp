// ReportChangeForReasonViewer.cpp: implementation of the ReportChangeForReasonViewer class.

//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ReportReasonForChangeViewer.h"
#include <CCSTable.h>
#include <BasicData.h>
#include <Math.h>

int ReportReasonForChangeViewer::imTableColCharWidths[REASON_FOR_CHANGE_COLCOUNT]={8, 1, 16, 5, 16, 6, 6, 5, 30, 10};

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

ReportReasonForChangeViewer::ReportReasonForChangeViewer() :
	imOrientation(PRINT_PORTRAET),
	romTableHeaderFont(ogCourier_Bold_10), romTableLinesFont(ogCourier_Regular_9),
	fmTableHeaderFontWidth(10), fmTableLinesFontWidth(7.5)
{


	dgCCSPrintFactor = 3;
	int i=0;
	// Table header strings
	omTableHeadlines[i++]=GetString(IDS_STRING1078);    // Flight 
 	omTableHeadlines[i++]=CString("");	// Flight Type 
 	omTableHeadlines[i++]=GetString(IDS_STRING1881);	// Scheduled Time
	omTableHeadlines[i++]=GetString(IDS_STRING2812);	// Allocation Field
 	omTableHeadlines[i++]=GetString(IDS_STRING2813);	// Time of Change
 	omTableHeadlines[i++]=GetString(IDS_STRING2814);	// Old Value
 	omTableHeadlines[i++]=GetString(IDS_STRING2815);	// New Value
 	omTableHeadlines[i++]=GetString(IDS_STRING2816);	// Reason Code
 	omTableHeadlines[i++]=GetString(IDS_STRING2817);	// Reason
 	omTableHeadlines[i++]=GetString(IDS_STRING2818);	// User

	for (i=0; i < REASON_FOR_CHANGE_COLCOUNT; i++)
	{
		imTableColWidths[i] = (int) max(imTableColCharWidths[i]*fmTableLinesFontWidth, 
								omTableHeadlines[i].GetLength()*fmTableHeaderFontWidth);

	}

	pomTable = NULL;
}

ReportReasonForChangeViewer::~ReportReasonForChangeViewer()
{
	DeleteAll();
}

	
// Connects the viewer with a table
void ReportReasonForChangeViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}

// delete intern table lines
void ReportReasonForChangeViewer::DeleteAll(void)
{
	if(omLines.GetSize() > 0)
	{
		omLines.DeleteAll();
	}
}


// Load the intern line data from the given data and displays the table
void ReportReasonForChangeViewer::ChangeViewTo(const CCSPtrArray<REASON_FOR_CHANGE> &ropData, char *popDateStr)
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	pomDateStr = popDateStr;
	
	// Rebuild intern data
    MakeLines(ropData);
	// Rebuild table
	UpdateDisplay();
  
 	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}


// Transfer the data of the database to the intern data structures
void ReportReasonForChangeViewer::MakeLines(const CCSPtrArray<REASON_FOR_CHANGE> &ropData)
{ 
	// Delete the intern lines
	DeleteAll();

 	// Create the intern lines
	REPORTREASONFORCHANGE_LINEDATA rlLineData;
 	
	for(int j = ropData.GetSize() -1 ; j >= 0; j --)
	{
		CTimeSpan olDelay;
		if (MakeLineData(rlLineData, ropData[j]))
		{
			CreateLine(rlLineData);
		}
	}

	//generate the headerinformation
	CString olTimeSet = GetString(IDS_STRING1920); //UTC 
	if(bgReportLocal)
	{
		olTimeSet = GetString(IDS_STRING1921); //LOCAL
	}

	char pclHeader[256];	
	sprintf(pclHeader,GetString(IDS_STRING2578), pomDateStr,olTimeSet);
	omTableName = pclHeader;
	omPrintName = pclHeader;
}

// Copy the data from the db-record to the table-record
bool ReportReasonForChangeViewer::MakeLineData(REPORTREASONFORCHANGE_LINEDATA &rrpLineData, const REASON_FOR_CHANGE &rrpFlight)
{
	rrpLineData.Urno = rrpFlight.Urno;
	strcpy(rrpLineData.Flno, rrpFlight.Flno);
	strcpy(rrpLineData.Ftyp, rrpFlight.Ftyp);
	rrpLineData.Scdt = rrpFlight.Scdt;
	strcpy(rrpLineData.Aloc, rrpFlight.Aloc);
	rrpLineData.Cdat = rrpFlight.Cdat;
	strcpy(rrpLineData.OVal, rrpFlight.OVal);
   	strcpy(rrpLineData.NVal, rrpFlight.NVal);
	strcpy(rrpLineData.Code, rrpFlight.Code);
	strcpy(rrpLineData.Rema, rrpFlight.Rema);
	strcpy(rrpLineData.Usec, rrpFlight.Usec);
	
	// Local times choosen?
	if(bgReportLocal)
	{
		ogBasicData.UtcToLocal(rrpLineData.Scdt);
	}

	return true;		
}


// Create a intern table data record
int ReportReasonForChangeViewer::CreateLine(const REPORTREASONFORCHANGE_LINEDATA &rrpLine)
{
    for (int ilLineno = omLines.GetSize(); ilLineno > 0; ilLineno--)
	{
		if (CompareLines(rrpLine, omLines[ilLineno-1]) >= 0)
		{
            break;  // should be inserted after Lines[ilLineno-1]
		}
	}
	// Insert new line
    omLines.NewAt(ilLineno, rrpLine);
	return ilLineno;
}

// Rebuild the table from the intern data
void ReportReasonForChangeViewer::UpdateDisplay()
{	
	pomTable->ResetContent();	
	DrawTableHeader();
    
	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;	
		MakeColList(omLines[ilLc], olColList);		
		pomTable->AddTextLine(olColList, (void*)(&omLines[ilLc]));			
	}

    pomTable->DisplayTable();
}

void ReportReasonForChangeViewer::DrawTableHeader()
{
	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Font = &romTableHeaderFont;

	for (int i=0; i < REASON_FOR_CHANGE_COLCOUNT ; i++)
	{
		rlHeader.Alignment = COLALIGN_CENTER;
		rlHeader.Length = imTableColWidths[i]; 
		rlHeader.Text = omTableHeadlines[i]; 
		omHeaderDataArray.New(rlHeader);
	}

	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();

}

// Fills one row of the table
bool ReportReasonForChangeViewer::MakeColList(const REPORTREASONFORCHANGE_LINEDATA &rrpLine, CCSPtrArray<TABLE_COLUMN> &ropColList) const
{
	TABLE_COLUMN rlColumnData;

	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &romTableLinesFont;
	rlColumnData.Alignment = COLALIGN_LEFT;

	rlColumnData.Text = rrpLine.Flno;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);
 
 	rlColumnData.Text = rrpLine.Ftyp;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = rrpLine.Scdt.Format("%d.%m.%y %H:%M");
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);
	
	rlColumnData.Text = rrpLine.Aloc;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

 	rlColumnData.Text = rrpLine.Cdat.Format("%d.%m.%y %H:%M");
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);
 
 	rlColumnData.Text = rrpLine.OVal;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);
	
 	rlColumnData.Text = rrpLine.NVal;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);
	
	rlColumnData.Text = rrpLine.Code;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = rrpLine.Rema;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Text = rrpLine.Usec;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);
	
	return true;
}

// Compares two lines of the table
int ReportReasonForChangeViewer::CompareLines(const REPORTREASONFORCHANGE_LINEDATA &rrpLine1, const REPORTREASONFORCHANGE_LINEDATA &rrpLine2) const 
{
	return 0;	
}

int ReportReasonForChangeViewer::GetFlightCount(void) const
{
	return pomTable->GetLinesCount();
}

//////////////////////////////////////////////////////////////////
//// Printing routines
 
// Print table to paper
void ReportReasonForChangeViewer::PrintTableView(void)
{

	CCSPrint olPrint(NULL, imOrientation, 45);

	// Set printing fonts
	pomPrintHeaderFont = &olPrint.ogCourierNew_Bold_8;
	pomPrintLinesFont = &olPrint.ogCourierNew_Regular_8;
	fmPrintHeaderFontWidth = 7;
	fmPrintLinesFontWidth = 7;


	// calculate table column widths for printing
	for (int i=0; i < REASON_FOR_CHANGE_COLCOUNT; i++)
	{
  		imPrintColWidths[i] = (int) max(imTableColCharWidths[i]*fmPrintLinesFontWidth, 
								        omTableHeadlines[i].GetLength()*fmPrintHeaderFontWidth);
	}

	CString olFooter1,olFooter2;
	char pclFooter[256];

	sprintf(pclFooter, omTableName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));
	omFooterName = pclFooter;
    CString olTablename = GetString(IDS_STRING2819);

	// Set left footer to: "printed at: <date>"
    olFooter1.Format("%s -   %s", omFooterName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );

  	if (olPrint.InitializePrinter(imOrientation) == TRUE)
	{ 
		olPrint.imMaxLines = 57;	// Def. imMaxLines: 57 Portrait / 38 Landscape
		// Calculate number of pages
		const double dlPages = ceil((double)pomTable->GetLinesCount() / (double)(olPrint.imMaxLines - 1));
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		// At first a pagebreak
		olPrint.imLineNo = olPrint.imMaxLines + 1;	
		DOCINFO	rlDocInfo;
		memset(&rlDocInfo, 0, sizeof(DOCINFO));
		rlDocInfo.cbSize = sizeof( DOCINFO );
		rlDocInfo.lpszDocName = olTablename ;	
		olPrint.omCdc.StartDoc( &rlDocInfo );
		olPrint.imPageNo = 0;
		// Print all table lines
		for (int ilLc = 0; ilLc < pomTable->GetLinesCount(); ilLc++)
		{
			// Page break
 			if(olPrint.imLineNo >= olPrint.imMaxLines)
			{
				if(olPrint.imPageNo > 0)
				{
					// Set right footer to: "Page: %d"
					olFooter2.Format(GetString(IDS_STRING1199),olPrint.imPageNo, dlPages);
					// print footer
					olPrint.PrintUIFFooter(olFooter1,"",olFooter2);
					olPrint.omCdc.EndPage();
				}
				// print header
				PrintTableHeader(olPrint);
			}				
			// print line
			PrintTableLine(olPrint, ilLc);
		}
		// print footer
		olFooter2.Format(GetString(IDS_STRING1199),olPrint.imPageNo, dlPages);
		olPrint.PrintUIFFooter(olFooter1,"",olFooter2);
		olPrint.omCdc.EndPage();
		olPrint.omCdc.EndDoc();
	}  // if (olPrint.InitializePrin...
 	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}

bool ReportReasonForChangeViewer::PrintTableHeader(CCSPrint &ropPrint)
{
	ropPrint.omCdc.StartPage();
	ropPrint.imPageNo++;
	ropPrint.imLineNo = 0;
	
	// print page headline
	ropPrint.imLeftOffset = 150;
	ropPrint.PrintUIFHeader("",omPrintName,ropPrint.imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_NOFRAME;
	rlElement.pFont       = pomPrintHeaderFont;

	// Create table headline
	for (int ilCc = 0; ilCc < REASON_FOR_CHANGE_COLCOUNT; ilCc++)
	{
		rlElement.Length = (int)(imPrintColWidths[ilCc]*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength) 
			rlElement.Length+=igCCSPrintMoreLength; 

		rlElement.Text = omTableHeadlines[ilCc]; 
		
		rlPrintLine.NewAt(rlPrintLine.GetSize(), rlElement);
 	}

	// Print table headline
 	ropPrint.PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();

	return true;
}


bool ReportReasonForChangeViewer::PrintTableLine(CCSPrint &ropPrint, int ipLineNo)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	
	PRINTELEDATA rlElement;
	rlElement.pFont = pomPrintLinesFont;
	rlElement.FrameTop = PRINT_FRAMETHIN;
 	rlElement.FrameBottom = PRINT_FRAMETHIN;
	rlElement.FrameLeft  = PRINT_FRAMETHIN;
	rlElement.FrameRight = PRINT_FRAMETHIN;

	CString olCellValue;
	// create table line

    for (int ilCc = 0; ilCc < REASON_FOR_CHANGE_COLCOUNT; ilCc++)
	{
		rlElement.Alignment  = PRINT_LEFT;
 		rlElement.Length = (int)(imPrintColWidths[ilCc]*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength) 
		{
			rlElement.Length+=igCCSPrintMoreLength; 
		}
		// Get printing text from table	
		pomTable->GetTextFieldValue(ipLineNo, ilCc, olCellValue);
		rlElement.Text = olCellValue; 
		
 		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}

	// print table line
	ropPrint.PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll(); 
	return true;
}


 
// Print table to file
bool ReportReasonForChangeViewer::PrintPlanToFile(char *pclTrenner)
{
	ofstream of;
	CString olFileName = omTableName;
	olFileName.Remove('*');
	olFileName.Remove('.');
	olFileName.Remove(':');
	olFileName.Remove('/');
	olFileName.Replace(" ", "_");
	
	char pHeader[256];
	strcpy (pHeader, CCSLog::GetTmpPath());
	CString path = pHeader;
	omFileName =  path + "\\" + olFileName + ".csv";

	of.open( omFileName, ios::out);

	// Header
	for (int ilCc = 0; ilCc < REASON_FOR_CHANGE_COLCOUNT; ilCc++)
	{
		of  << omTableHeadlines[ilCc];
		if (ilCc < REASON_FOR_CHANGE_COLCOUNT-1)
		{
			of << pclTrenner;
		}
	}
	of << endl;

	CString olCellValue;
	 // Lines
	for (int ilLc = 0; ilLc < pomTable->GetLinesCount(); ilLc++)
	{
		for (int ilCc = 0; ilCc < REASON_FOR_CHANGE_COLCOUNT; ilCc++)
		{
			// get text from table
			pomTable->GetTextFieldValue(ilLc, ilCc, olCellValue);
			of  << olCellValue;
			if (ilCc < REASON_FOR_CHANGE_COLCOUNT-1)
			{
				of << pclTrenner;
			}
		}
		of << endl;
	}		
	
	of.close();

	return true;
}
