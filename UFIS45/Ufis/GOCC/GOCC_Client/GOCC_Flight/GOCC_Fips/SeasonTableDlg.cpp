// SeasonTableDlg.cpp : implementation file
//

#include <stdafx.h>
#include <process.h>
#include <FPMS.h>
#include <SeasonTableDlg.h>
#include <ButtonListDlg.h>
#include <CedaBasicData.h>
#include <CCSTable.h>
#include <PrivList.h>
#include <CedaImpFlightData.h>
#include <SeasonFlightPropertySheet.H>
#include <SeasonCollectDlg.h>
#include <SeasonCollectADAskDlg.h>
#include <FlightSearchTableDlg.h>
#include <RotGroundDlg.h>
#include <CCSGlobl.h>
#include <AskBox.h>
#include <Utils.h>
#include <resrc1.h>
#include <CedaFlightUtilsData.h>
#include <BasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSeasonTableDlg dialog


// Local function prototype
static void FlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);



////////////////////////////////////////////////////////////////////////////
// Konstruktor
//
CSeasonTableDlg::CSeasonTableDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSeasonTableDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSeasonTableDlg)
	//}}AFX_DATA_INIT
	pomSeasonTable = NULL;
	ogSeasonFlights.Register();
	pomParent = pParent;
	omViewer.SetViewerKey("SAISONFLIGHT");
    CDialog::Create(CSeasonTableDlg::IDD, pParent);
	m_key = "DialogPosition\\FlightSchedule";
}


////////////////////////////////////////////////////////////////////////////
// Destruktor
//
CSeasonTableDlg::~CSeasonTableDlg()
{
	TRACE("FlightTableDlg::~FlightTableDlg()\n");

	ogDdx.UnRegister(this,NOTUSED);

	
	if(pogSeasonAskDlg != NULL)
		pogSeasonAskDlg->DestroyWindow();

	ogSeasonFlights.UnRegister();
	ogSeasonFlights.ClearAll();
	delete pomSeasonTable;
}


void CSeasonTableDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSeasonTableDlg)
	DDX_Control(pDX, IDC_COMBO_ANSICHT, m_CC_Ansicht);
	DDX_Control(pDX, IDC_VERBINDEN, m_CB_Verbinden);
	DDX_Control(pDX, IDC_TEILEN, m_CB_Teilen);
	DDX_Control(pDX, IDC_SAMMELBEARBEITUNG, m_CB_Sammelbearbeitung);
 	DDX_Control(pDX, IDC_PRINT, m_CB_Drucken);
	DDX_Control(pDX, IDC_IMPORT, m_CB_Import);
	DDX_Control(pDX, IDC_FREIGEBEN, m_CB_Freigeben);
	DDX_Control(pDX, IDC_FLUKO, m_CB_Fluko);
	DDX_Control(pDX, IDC_EXCEL, m_CB_Excel);
	DDX_Control(pDX, IDC_ENTF, m_CB_Entf);
	DDX_Control(pDX, IDC_EINFUEGEN, m_CB_Einfuegen);
	DDX_Control(pDX, IDC_CXX, m_CB_Cxx);
	DDX_Control(pDX, IDC_BUTTON_ANSICHT, m_CB_Ansicht);
	DDX_Control(pDX, IDC_ANALYSE, m_CB_Analyse);
	DDX_Control(pDX, IDC_AENDERN, m_CB_Aendern);
	DDX_Control(pDX, IDC_KOPIEREN, m_CB_Kopieren);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSeasonTableDlg, CDialog)
	//{{AFX_MSG_MAP(CSeasonTableDlg)
    ON_MESSAGE(WM_TABLE_DRAGBEGIN, OnTableDragBegin)
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_MESSAGE(WM_DROP, OnDrop)  
	ON_BN_CLICKED(IDC_SCHLIESSEN, OnSchliessen)
	ON_BN_CLICKED(IDC_EINFUEGEN, OnEinfuegen)
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
	ON_BN_CLICKED(IDC_AENDERN, OnAendern)
	ON_BN_CLICKED(IDC_KOPIEREN, OnKopieren)
	ON_BN_CLICKED(IDC_TEILEN, OnTeilen)
	ON_BN_CLICKED(IDC_VERBINDEN, OnVerbinden)
	ON_BN_CLICKED(IDC_EXCEL, OnExcel)
	ON_BN_CLICKED(IDC_CXX, OnCxx)
	ON_BN_CLICKED(IDC_ENTF, OnEntf)
	ON_BN_CLICKED(IDC_ANALYSE, OnAnalyse)
	ON_BN_CLICKED(IDC_FLUKO, OnFluko)
	ON_CBN_SELCHANGE(IDC_COMBO_ANSICHT, OnSelchangeComboAnsicht)
	ON_BN_CLICKED(IDC_BUTTON_ANSICHT, OnButtonAnsicht)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_FREIGEBEN, OnFreigeben)
	ON_BN_CLICKED(IDC_PRINT, OnPrint)
	ON_BN_CLICKED(IDC_IMPORT, OnImport)
	ON_BN_CLICKED(IDC_SEARCH, OnSearch)
	ON_WM_CLOSE()
 	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_SAMMELBEARBEITUNG, OnSammelbearbeitung)
    ON_MESSAGE(WM_TABLE_LBUTTONDOWN, OnTableLButtonDown)
    ON_MESSAGE(WM_TABLE_RETURN_PRESSED, OnTableReturnPressed)
	ON_MESSAGE(WM_TABLE_SELCHANGE, OnTableSelChanged)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSeasonTableDlg message handlers

BOOL CSeasonTableDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_key = "DialogPosition\\FlightSchedule";

	omViewer.SetParentDlg(this);
   
	omDragDropTarget.RegisterTarget(this, this);
	

	/*	
	// calculate the window height
    GetClientRect(&rect);
    imDialogBarHeight = rect.bottom - rect.top;
    
	// extend dialog window to current screen width
    rect.left = 0;
    rect.top = 65;
    rect.right = 1024;
    rect.bottom = 768;
    MoveWindow(&rect);
	*/

//-------------
//Monitorsetup
	CRect olRect;

    GetClientRect(&olRect);
    imDialogBarHeight = olRect.bottom - olRect.top;

	
	GetWindowRect(&olRect);

	int ilWidth = olRect.right -olRect.left;
	int ilWhichMonitor = ogCfgData.GetMonitorForWindow(CString(MON_SEASONSCHEDULE_STRING));
	int ilMonitors = ogCfgData.GetMonitorForWindow(CString(MON_COUNT_STRING));
	int left, top, right, bottom;
	int ilCXMonitor;

//	top = 65 ;
	top = 95 ;
	bottom = ::GetSystemMetrics(SM_CYSCREEN);


	if(ilWhichMonitor == 1)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}
	else if(ilWhichMonitor == 2)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2)+(ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2)+(ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			right = GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}
	else if(ilWhichMonitor == 3)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2)+(2*ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			right = GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}
//-------------
//	MoveWindow(CRect(left, top, right, bottom));
//	SetWindowPos(&wndTop,0, 0, 0, 0, SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE);

	olRect = CRect(0, 95, ::GetSystemMetrics(SM_CXSCREEN), ::GetSystemMetrics(SM_CYSCREEN));
	GetDialogFromReg(olRect, m_key);
	SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_SHOWWINDOW);// | SWP_NOMOVE );


    pomSeasonTable = new CCSTable;
	pomSeasonTable->SetSelectMode(LBS_MULTIPLESEL | LBS_EXTENDEDSEL);
	pomSeasonTable->SetHeaderSpacing(0);
	
	//CRect rect;
    GetClientRect(&olRect);

	olRect.top = olRect.top + imDialogBarHeight;
	olRect.bottom = olRect.bottom;// - imDialogBarHeight;

    olRect.InflateRect(1, 1);     // hiding the CTable window border
    
	pomSeasonTable->SetTableData(this, olRect.left, olRect.right, olRect.top, olRect.bottom);
	omViewer.Attach(pomSeasonTable);
	omViewer.ChangeViewTo("<Default>");
	UpdateComboAnsicht();
	UpdateView();



	SetWndStatAll(ogPrivList.GetStat("SEASONBTL_CC_Ansicht"),m_CC_Ansicht);
	SetWndStatAll(ogPrivList.GetStat("SEASONBTL_CB_Ansicht"),m_CB_Ansicht);
	SetWndStatAll(ogPrivList.GetStat("SEASONBTL_CB_Freigeben"),m_CB_Freigeben);
	SetWndStatAll(ogPrivList.GetStat("SEASONBTL_CB_Einfuegen"),m_CB_Einfuegen);
//	SetWndStatAll(ogPrivList.GetStat("SEASONBTL_CB_Aendern"),m_CB_Aendern);
	SetWndStatAll(ogPrivList.GetStat("SEASONBTL_CB_Entf"),m_CB_Entf);
	SetWndStatAll(ogPrivList.GetStat("SEASONBTL_CB_Cxx"),m_CB_Cxx);
	SetWndStatAll(ogPrivList.GetStat("SEASONBTL_CB_Kopieren"),m_CB_Kopieren);
	SetWndStatAll(ogPrivList.GetStat("SEASONBTL_CB_Sammelbearbeitung"),m_CB_Sammelbearbeitung);
	SetWndStatAll(ogPrivList.GetStat("SEASONBTL_CB_Verbinden"),m_CB_Verbinden);
	SetWndStatAll(ogPrivList.GetStat("SEASONBTL_CB_Teilen"),m_CB_Teilen);
 	SetWndStatAll(ogPrivList.GetStat("SEASONBTL_CB_Import"),m_CB_Import);

	CButton *polCB = (CButton *) GetDlgItem(IDC_SEARCH);
	SetpWndStatAll(ogPrivList.GetStat("SEASONBTL_CB_Search"),polCB);

	SetWndStatAll(ogPrivList.GetStat("SEASONBTL_CB_Drucken"),m_CB_Drucken);
	SetWndStatAll(ogPrivList.GetStat("SEASONBTL_CB_Import"),m_CB_Import);
	SetWndStatAll(ogPrivList.GetStat("SEASONBTL_CB_Excel"),m_CB_Excel);

	#ifdef _FIPSVIEWER
	{
		SetWndStatAll('0',m_CB_Einfuegen);
		SetWndStatAll('0',m_CB_Kopieren);
		SetWndStatAll('0',m_CB_Entf);
		SetWndStatAll('0',m_CB_Teilen);
		SetWndStatAll('0',m_CB_Verbinden);
		SetWndStatAll('0',m_CB_Cxx);
		SetWndStatAll('0',m_CB_Import);
		SetWndStatAll('0',m_CB_Sammelbearbeitung);
		SetWndStatAll('0',m_CB_Freigeben);
	}
	#else
	{
	}
	#endif
	
	//SetWndStatAll(ogPrivList.GetStat("SEASONBTL_CB_Fluko"),m_CB_Fluko);
	//SetWndStatAll(ogPrivList.GetStat("SEASONBTL_CB_Analyse"),m_CB_Analyse);


	ogDdx.Register(this, S_DLG_GET_PREV, CString("FLIGHTDLG"), CString("Flight Update/new"), FlightTableCf);
	ogDdx.Register(this, S_DLG_GET_NEXT, CString("FLIGHTDLG"), CString("Flight Update/new"), FlightTableCf);


	// set caption
	CString olTimes;
	if (bgSeasonLocal)
		olTimes = GetString(IDS_STRING1921);
	else
		olTimes = GetString(IDS_STRING1920);
	CString olCaption = GetString(IDS_STRING1932);
	olCaption += " ("+olTimes+")";
	SetWindowText(olCaption);
/*
	m_resizeHelper.Init(this->m_hWnd);

	m_resizeHelper.Fix(IDC_BUTTON_ANSICHT,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_COMBO_ANSICHT,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_SEARCH,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_EINFUEGEN,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_ENTF,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_KOPIEREN,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_TEILEN,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_VERBINDEN,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_CXX,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_FREIGEBEN,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_SAMMELBEARBEITUNG,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_IMPORT,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_EXCEL,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_PRINT,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_SCHLIESSEN,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
*/

	ShowWindow(SW_SHOWNORMAL);


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


////////////////////////////////////////////////////////////////////////////
// und Tsch�ss...
//
void CSeasonTableDlg::OnDestroy() 
{
	SaveToReg();
	CDialog::OnDestroy();
}



void CSeasonTableDlg::SaveToReg() 
{
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
}

void CSeasonTableDlg::OnSchliessen() 
{

	if (bgPrintDlgOpen)
		return;

	if(pogSeasonAskDlg != NULL)
		pogSeasonAskDlg->DestroyWindow();
	CDialog::OnCancel();
	CDialog::DestroyWindow();
	delete this;
	pogSeasonTableDlg = NULL;
}


void CSeasonTableDlg::OnCancel()
{

	if (bgPrintDlgOpen)
		return;
	else
		CDialog::OnCancel();

}


void CSeasonTableDlg::ShowDetail() 
{
//		CSeasonDlg dlg(this);
//		dlg.DoModal();	
}

////////////////////////////////////////////////////////////////////////////
// UpdateView
//
void CSeasonTableDlg::UpdateView()
{

	CString olViewName = "<Default>";

	if (olViewName.IsEmpty() == TRUE)
	{
	//	olViewName = ogCfgData.rmUserSetup.STLV;
	}

	AfxGetApp()->DoWaitCursor(1);
    omViewer.ChangeViewTo(olViewName);
	AfxGetApp()->DoWaitCursor(-1);

	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
	pomSeasonTable->GetCTableListBox()->SetFocus();
}


////////////////////////////////////////////////////////////////////////////
// linke Maustaste auf der Flighttable gedr�ckt
//
LONG CSeasonTableDlg::OnTableLButtonDblclk( UINT wParam, LPARAM lParam)
{
	CCSTABLENOTIFY *prlNotify = (CCSTABLENOTIFY*)lParam;

	UINT ipItem = wParam;
	SEASONTABLE_LINEDATA *prlTableLine = NULL;
	prlTableLine = (SEASONTABLE_LINEDATA *)pomSeasonTable->GetTextLineData(ipItem);


	if (prlTableLine == NULL)
		return 0L;

	// determine flight
	SEASONFLIGHTDATA *prlFlight = NULL;
	SEASONFLIGHTDATA *prlDFlight = ogSeasonFlights.GetFlightByUrno(prlTableLine->DUrno);
	SEASONFLIGHTDATA *prlAFlight = ogSeasonFlights.GetFlightByUrno(prlTableLine->AUrno);

//	CString olAdid;
	char clAdid;
	if (prlAFlight != NULL && prlDFlight != NULL)
	{
		if(prlNotify->Column >= 16)
		{
			clAdid = 'D';
			prlFlight = prlDFlight;
		}
		else
		{
			clAdid = 'A';
			prlFlight = prlAFlight;
		}
	}
	else if (prlAFlight != NULL)
	{
		clAdid = 'A';
		prlFlight = prlAFlight;
	}
	else if (prlDFlight != NULL)
	{
		clAdid = 'D';
		prlFlight = prlDFlight;
	}

	if (prlFlight == NULL)
		return 0L;


	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	CFPMSApp::ShowFlightRecordData(this, prlFlight->Ftyp[0], prlFlight->Urno, prlFlight->Rkey, clAdid, prlFlight->Tifa, bgSeasonLocal, 'S');
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));


/*	if( (CString(prlFlight->Ftyp) == "T") || (CString(prlFlight->Ftyp) == "G"))
	{
		// show towing mask
		if(pogRotGroundDlg == NULL)
		{
			pogRotGroundDlg = new RotGroundDlg(this);
			pogRotGroundDlg->Create(IDD_ROTGROUND);
		}

		pogRotGroundDlg->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
		pogRotGroundDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno, olAdid[0], bgSeasonLocal);
	}
	else
	{
		// show rotation mask
	  	pogSeasonDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno, DLG_CHANGE, bgSeasonLocal, olAdid);
	}
*/

	return 0L;
}



////////////////////////////////////////////////////////////////////////////
// Button �ndern: selektierter Datensatz wird in die 
// Flugplanerfasungsmaske zum �ndern geladen
//
void CSeasonTableDlg::OnAendern() 
{
	SEASONTABLE_LINEDATA *prlTableLine = (SEASONTABLE_LINEDATA *)pomSeasonTable->GetTextLineData(pomSeasonTable->GetCurrentLine());
	if (prlTableLine != NULL)
	{
		SEASONFLIGHTDATA *prlFlight;

		if((prlFlight = ogSeasonFlights.GetFlightByUrno(prlTableLine->AUrno)) == NULL)
			prlFlight = ogSeasonFlights.GetFlightByUrno(prlTableLine->DUrno);

		if(prlFlight != NULL)
			pogSeasonDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno, DLG_CHANGE, bgSeasonLocal);
	}
}


////////////////////////////////////////////////////////////////////////////
// Button Einf�gen: die leere Flugplanerfasungsmaske wird ge�ffnet
//
void CSeasonTableDlg::OnEinfuegen() 
{
	pogSeasonDlg->NewData(this, 0L, 0L, DLG_NEW, bgSeasonLocal);
}



////////////////////////////////////////////////////////////////////////////
// Button Kopieren: selektierter Datensatz wird in die 
// Flugplanerfasungsmaske zum kopieren geladen
//
void CSeasonTableDlg::OnKopieren() 
{
	SEASONTABLE_LINEDATA *prlTableLine = (SEASONTABLE_LINEDATA *)pomSeasonTable->GetTextLineData(pomSeasonTable->GetCurrentLine());
	if (prlTableLine != NULL)
	{
		SEASONFLIGHTDATA *prlFlight;

		if((prlFlight = ogSeasonFlights.GetFlightByUrno(prlTableLine->AUrno)) == NULL)
			prlFlight = ogSeasonFlights.GetFlightByUrno(prlTableLine->DUrno);

		if(prlFlight != NULL)
			pogSeasonDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno, DLG_COPY, bgSeasonLocal);
	}
}

////////////////////////////////////////////////////////////////////////////
// Button Teilen: selektierter Datensatz wird gesplitet 
//
void CSeasonTableDlg::OnTeilen() 
{
	CListBox *polLB = pomSeasonTable->GetCTableListBox();
	if (polLB == NULL)
		return;

	int *ilItems = NULL;
	int ilAnz = polLB->GetSelCount();
	if(ilAnz > 0)
	{
		ilItems = new int[ilAnz];
	}

	if(ilItems != NULL)
		ilAnz = polLB->GetSelItems(ilAnz, ilItems);
	else
		return;


	CUIntArray olUrnos;
	for(int i = 0; i < ilAnz; i++)
	{
		SEASONTABLE_LINEDATA *prlTableLine = (SEASONTABLE_LINEDATA *)pomSeasonTable->GetTextLineData(ilItems[i]);
		if (!CheckPostFlight(prlTableLine))
		{
			if (prlTableLine != NULL && ((prlTableLine->AUrno != 0) && (prlTableLine->DUrno != 0)) && (prlTableLine->AUrno != prlTableLine->DUrno != 0) )
				olUrnos.Add(prlTableLine->AUrno);
		}
		else
		{
			if (ilAnz == 1)
				CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_MSG_POSTFLIGHT_TEXT2), GetString(IDS_MSG_POSTFLIGHT_TEXT1), MB_ICONWARNING);
		}
	}

	if(ilItems != NULL)
		delete [] ilItems;

	ilAnz = olUrnos.GetSize();
	for(i = 0; i < ilAnz; i++)
	{
		long olUrno = olUrnos.GetAt(i);
		if (olUrno > 0)
		{
			CedaFlightUtilsData olFlightUtilsData;
			if (i == 0)
			{
				char buffer[32];
				itoa(ilAnz, buffer, 10);
				CString olMessage;
				olMessage.Format(GetString(IDS_STRING2208), buffer);
				
				if( CFPMSApp::MyTopmostMessageBox(this,olMessage, GetString(ST_FRAGE), MB_YESNO) != IDYES)
					return;
			}
			olFlightUtilsData.SplitFlight(olUrno, ogSeasonFlights.GetFlightByUrno(olUrno)->Rkey, true);
		}
	}
}



////////////////////////////////////////////////////////////////////////////
// Button Cxx: selektierten Datens�tze werden anuliert  
//
void CSeasonTableDlg::OnExcel() 
{

	char pclConfigPath[256];
	char pclExcelPath[256];
	char pclTrenner[64];
	

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

    GetPrivateProfileString(ogAppName, "Excel", "DEFAULT",
        pclExcelPath, sizeof pclExcelPath, pclConfigPath);


    GetPrivateProfileString(ogAppName, "ExcelSeparator", ";",
        pclTrenner, sizeof pclTrenner, pclConfigPath);


	if(!strcmp(pclExcelPath, "DEFAULT"))
		CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING977), GetString(ST_FEHLER), MB_ICONERROR);

	CString olFileName;

	omViewer.CreateExcelFile(pclTrenner);

	olFileName = omViewer.omFileName;

	bool test = true; //only for testing error
	if (olFileName.IsEmpty() || olFileName.GetLength() > 255 || !test)
	{
		if (olFileName.IsEmpty())
		{
			CString mess = "Filename is empty !" + olFileName;
			CFPMSApp::MyTopmostMessageBox(this, mess, GetString(ST_FEHLER), MB_ICONERROR);
			return;
		}
		if (olFileName.GetLength() > 255 )
		{
			CString mess = "Length of the Filename > 255: " + olFileName;
			CFPMSApp::MyTopmostMessageBox(this, mess, GetString(ST_FEHLER), MB_ICONERROR);
			return;
		}

		CString mess = "Filename invalid: " + olFileName;
		CFPMSApp::MyTopmostMessageBox(this, mess, GetString(ST_FEHLER), MB_ICONERROR);
		return;
	}

	char pclTmp[256];
	strcpy(pclTmp, olFileName); 
//	strcpy(pclTmp, GetString(IDS_STRING1386)); 

   /* Set up parameters to be sent: */
	char *args[4];
	args[0] = "child";
	args[1] = pclTmp;
	args[2] = NULL;
	args[3] = NULL;

	int li_Path = _spawnv( _P_NOWAIT , pclExcelPath, args );
	if(li_Path == -1)
		CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_EXCELPATH_ERROR), GetString(ST_FEHLER), MB_ICONERROR);

}



////////////////////////////////////////////////////////////////////////////
// Button Freigeben f�r betrieb: bei den selektierten Datens�tze wird FTYP auf O gesetzt  
//
void CSeasonTableDlg::OnFreigeben() 
{
	SEASONTABLE_LINEDATA *prlTableLine;
	CString olUrnoList;
	char buffer[32];
	int ilAnzSel = 0;
	int i;

	CListBox *polLB = pomSeasonTable->GetCTableListBox();
	if (polLB == NULL)
		return;

	int *ilItems = NULL;
	int ilAnz = polLB->GetSelCount();
	if(ilAnz > 0)
	{
		ilItems = new int[ilAnz];
	}

	if(ilItems != NULL)
		ilAnz = polLB->GetSelItems(ilAnz, ilItems);
	else
	{
		CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING968), GetString(ST_FEHLER), MB_ICONWARNING);
		return;
	}

	SEASONFLIGHTDATA *prlFlight = NULL;
	BOOL blAllertForPostflight = FALSE;
	for(i = 0; i < ilAnz; i++)
	{
		prlTableLine = (SEASONTABLE_LINEDATA *)pomSeasonTable->GetTextLineData(ilItems[i]);

		if (!CheckPostFlight(prlTableLine))
		{
			if(prlTableLine->AUrno != 0)
			{
				prlFlight = ogSeasonFlights.GetFlightByUrno(prlTableLine->AUrno);
				if (prlFlight)
				{
					if (CString("XNS").Find(prlFlight->Ftyp[0]) >= 0 || prlFlight->Ftyp[0] == '\0')
					{
						ltoa(prlTableLine->AUrno, buffer, 10);
						olUrnoList += CString(buffer) + ',';
						ilAnzSel++;
					}
				}
			}
			if(prlTableLine->DUrno != 0)
			{
				prlFlight = ogSeasonFlights.GetFlightByUrno(prlTableLine->DUrno);
				if (prlFlight)
				{
					if (CString("XNS").Find(prlFlight->Ftyp[0]) >= 0 || prlFlight->Ftyp[0] == '\0')
					{
						ltoa(prlTableLine->DUrno, buffer, 10);
						olUrnoList += CString(buffer) + ',';
						ilAnzSel++;
					}
				}
			}
		}
		else
			blAllertForPostflight = TRUE;

	}

	if(ilItems != NULL)
		delete [] ilItems;

	if(!olUrnoList.IsEmpty())
		olUrnoList = olUrnoList.Left(olUrnoList.GetLength() - 1);


	if(ilAnzSel > 0)
	{
		itoa(ilAnzSel, buffer, 10);
		CString olMessage = CString(GetString(IDS_STRING969)) + CString(buffer) + CString(GetString(IDS_STRING978));
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		if( CFPMSApp::MyTopmostMessageBox(this,olMessage, GetString(ST_FRAGE), MB_YESNO) == IDYES)
			ogSeasonFlights.UpdateFlight(olUrnoList, "FTYP", "O");	
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}

	if (blAllertForPostflight)
		CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_MSG_POSTFLIGHT_TEXT2), GetString(IDS_MSG_POSTFLIGHT_TEXT1), MB_ICONWARNING);
}



////////////////////////////////////////////////////////////////////////////
// Button Cxx: selektierten Datens�tze werden anuliert  
//
void CSeasonTableDlg::OnCxx() 
{
	SEASONTABLE_LINEDATA *prlTableLine;
	CString olUrnoList;
	char buffer[32];
	int ilAnzSel = 0;

	CListBox *polLB = pomSeasonTable->GetCTableListBox();
	if (polLB == NULL)
		return;

	int *ilItems = NULL;
	int ilAnz = polLB->GetSelCount();
	if(ilAnz > 0)
	{
		ilItems = new int[ilAnz];
	}

	if(ilItems != NULL)
		ilAnz = polLB->GetSelItems(ilAnz, ilItems);
	else
	{
		CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING968), GetString(ST_FEHLER), MB_ICONWARNING);
		return;
	}

	if (ilAnz == LB_ERR)
	{
		CFPMSApp::MyTopmostMessageBox(this,"LB_ERR", GetString(ST_FEHLER), MB_ICONWARNING);
		return;
	}

	SEASONFLIGHTDATA *prlFlight = NULL;
	BOOL blAllertForPostflight = FALSE;
	bool blWrongFtyp = false;
	for(int i = 0; i < ilAnz; i++)
	{
		prlTableLine = (SEASONTABLE_LINEDATA *)pomSeasonTable->GetTextLineData(ilItems[i]);
		if (!CheckPostFlight(prlTableLine))
		{
			if(prlTableLine->AUrno != 0)
			{
				prlFlight = ogSeasonFlights.GetFlightByUrno(prlTableLine->AUrno);
				if (prlFlight)
				{
//					if (CString("OBZRD").Find(prlFlight->Ftyp[0]) >= 0 && strlen(prlFlight->Ftyp) > 0)

					//also X and N for changing
					if ((CString("OBZRDSXN").Find(prlFlight->Ftyp[0]) >= 0 && strlen(prlFlight->Ftyp) > 0)
						|| (CString(prlFlight->Ftyp) == "")) //prognosis
					{
						ltoa(prlTableLine->AUrno, buffer, 10);
						olUrnoList += CString(buffer) + ',';
						ilAnzSel++;
					}
					else
					{
						blWrongFtyp = true;
					}
				}
			}
			if(prlTableLine->DUrno != 0)
			{
				prlFlight = ogSeasonFlights.GetFlightByUrno(prlTableLine->DUrno);
				if (prlFlight)
				{
//					if (CString("OBZRD").Find(prlFlight->Ftyp[0]) >= 0 && strlen(prlFlight->Ftyp) > 0)

					//also X and N for changing
					if ((CString("OBZRDSXN").Find(prlFlight->Ftyp[0]) >= 0 && strlen(prlFlight->Ftyp) > 0)
						|| (CString(prlFlight->Ftyp) == "")) //prognosis
					{
						ltoa(prlTableLine->DUrno, buffer, 10);
						olUrnoList += CString(buffer) + ',';
						ilAnzSel++;
					}
					else
					{
						blWrongFtyp = true;
					}
				}
			}
		}
		else
			blAllertForPostflight = TRUE;

	}
	if(!olUrnoList.IsEmpty())
		olUrnoList = olUrnoList.Left(olUrnoList.GetLength() - 1);


	if(ilAnzSel > 0)
	{
		itoa(ilAnzSel, buffer, 10);
		CString olMessage;

		//Do you want to cancel the selected flights or set to noop?
		AskBox ACDlg(this, GetString(ST_FRAGE), GetString(IDS_STRING_CXX_NOOP), GetString(IDS_STRING_CXX), GetString(IDS_STRING_NOOP), CString(""), GetString(IDS_STRING_ASKDLG_CANCEL));
		int ilACDlg = -1;
		ilACDlg = ACDlg.DoModal();

		//Do you really want to cancel the %s selected flights?
		if (ilACDlg == 1)
			olMessage.Format(GetString(IDS_STRING_CXX_QUEST), buffer);

		//Do you really want to set the %s selected flights to NOOP?
 		else if (ilACDlg == 2)
			olMessage.Format(GetString(IDS_STRING_NOOP_QUEST), buffer); 
		else
		{
			if(ilItems != NULL)
				delete [] ilItems;
			return;
		}

		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		if( CFPMSApp::MyTopmostMessageBox(this,olMessage, GetString(ST_FRAGE), MB_YESNO) == IDYES)
		{
			//cancel
			if (ilACDlg == 1)
				ogSeasonFlights.UpdateFlight(olUrnoList, "FTYP", "X");	

			//noop
			if (ilACDlg == 2)
				ogSeasonFlights.UpdateFlight(olUrnoList, "FTYP", "N");	

//##### PRF 2055 NUR POPS44
			// if both legs cxx or noop -> don�t split the rotation -> join again !
			for(int k = 0; k < ilAnz; k++)
			{
				prlTableLine = (SEASONTABLE_LINEDATA *)pomSeasonTable->GetTextLineData(ilItems[k]);
				if (prlTableLine && !CheckPostFlight(prlTableLine))
				{
					SEASONFLIGHTDATA *prlAFlight = NULL;
					SEASONFLIGHTDATA *prlDFlight = NULL;

					if(prlTableLine->DUrno != 0)
					{
						char pclSelection[256];
						sprintf(pclSelection, " URNO = %ld", prlTableLine->DUrno);

						ogSeasonFlights.ReadFlights(pclSelection, true);
						prlDFlight = ogSeasonFlights.GetFlightByUrno(prlTableLine->DUrno);
					}

					if(prlTableLine->AUrno != 0)
					{
						char pclSelection[256];
						sprintf(pclSelection, " URNO = %ld", prlTableLine->AUrno);

						ogSeasonFlights.ReadFlights(pclSelection, true);
						prlAFlight = ogSeasonFlights.GetFlightByUrno(prlTableLine->AUrno);
					}

					if (prlAFlight && prlDFlight)
					{
						CedaFlightUtilsData olFlightUtilsData;
						olFlightUtilsData.JoinFlight(prlAFlight->Urno, prlAFlight->Rkey, prlDFlight->Urno, prlDFlight->Rkey, true);
					}
				}
			}

//#####

		}
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}
	else
	{
		if (blWrongFtyp)
		{
			CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING2096), GetString(ST_FEHLER), MB_ICONERROR);
			if(ilItems != NULL)
				delete [] ilItems;
			return;
		}

	}

	if (blAllertForPostflight)
		CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_MSG_POSTFLIGHT_TEXT2), GetString(IDS_MSG_POSTFLIGHT_TEXT1), MB_ICONWARNING);

	if(ilItems != NULL)
		delete [] ilItems;
}

////////////////////////////////////////////////////////////////////////////
// Button Enf selektierten Datens�tze werden gel�scht 
//
void CSeasonTableDlg::OnEntf() 
{
	SEASONTABLE_LINEDATA *prlTableLine;
	CString olUrnoList;
	char buffer[32];
	int ilAnzSel = 0;

	CListBox *polLB = pomSeasonTable->GetCTableListBox();
	if (polLB == NULL)
		return;

	int *ilItems = NULL;
	int ilAnz = polLB->GetSelCount();
	if(ilAnz > 0)
	{
		ilItems = new int[ilAnz];
	}

	if(ilItems != NULL)
		ilAnz = polLB->GetSelItems(ilAnz, ilItems);
	else
	{
		CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING968), GetString(ST_FEHLER), MB_ICONWARNING);
		return;
	}

	BOOL blAllertForPostflight = FALSE;
	for(int i = 0; i < ilAnz; i++)
	{
		prlTableLine = (SEASONTABLE_LINEDATA *)pomSeasonTable->GetTextLineData(ilItems[i]);
		if (!CheckPostFlight(prlTableLine))
		{
			if(prlTableLine->AUrno != 0)
			{
					ltoa(prlTableLine->AUrno, buffer, 10);
					olUrnoList += CString(buffer) + ',';
					ilAnzSel++;
			}
			if(prlTableLine->DUrno != 0)
			{
					ltoa(prlTableLine->DUrno, buffer, 10);
					olUrnoList += CString(buffer) + ',';
					ilAnzSel++;
			}
		}
		else
			blAllertForPostflight = TRUE;

	}

	if(ilItems != NULL)
		delete [] ilItems;

	if(!olUrnoList.IsEmpty())
		olUrnoList = olUrnoList.Left(olUrnoList.GetLength() - 1);



	if(ilAnzSel > 0)
	{
		itoa(ilAnzSel, buffer, 10);

		CString olMessage;

		olMessage.Format(GetString(IDS_STRING1255), buffer);
		
		if( CFPMSApp::MyTopmostMessageBox(this,olMessage, GetString(ST_FRAGE), MB_YESNO) == IDYES)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			ogSeasonFlights.DeleteFlight(olUrnoList);	
		}
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}
	if (blAllertForPostflight)
		CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_MSG_POSTFLIGHT_TEXT2), GetString(IDS_MSG_POSTFLIGHT_TEXT1), MB_ICONWARNING);
}

void CSeasonTableDlg::OnAnalyse() 
{

}



///////////////////////////////////////////////////////////////////////////////
// BWI!
void CSeasonTableDlg::OnFluko() 
{
	
}
///////////////////////////////////////////////////////////////////////////////














////////////////////////////////////////////////////////////////////////////
// Dragbeginn einer Drag&Drop Aktion
//
LONG CSeasonTableDlg::OnTableDragBegin(UINT wParam, LONG lParam)
{
	if(ogPrivList.GetStat("SEASONTAB_DragAndDrop") != '1')
		return 0L;

	int ilCol;
	int ilLine;
	bool blNothingToDo = FALSE;


	SEASONTABLE_LINEDATA *prlLine = (SEASONTABLE_LINEDATA*)pomSeasonTable->GetTextLineData(wParam);
//	if (CheckPostFlight(prlLine))
//		return 0L;

	CPoint point;
    ::GetCursorPos(&point);
    pomSeasonTable->pomListBox->ScreenToClient(&point);    
    //ScreenToClient(&point);    

	if (prlLine != NULL)
	{
		ilLine = pomSeasonTable->GetLinenoFromPoint(point);
		ilCol = pomSeasonTable->GetColumnnoFromPoint(point);
	}
	else
		return 0L;


	//if(prlLine->AUrno == prlLine->DUrno)
	//	return 0L;


	if(ilCol < 15)
	{
		if(prlLine->AUrno != 0)
		{
			omDragDropObject.CreateDWordData(DIT_FLIGHT, 1);
			omDragDropObject.AddDWord(prlLine->AUrno);
		}
		else
			return 0L;
	}
	else
	{
		if(prlLine->DUrno != 0)
		{
			omDragDropObject.CreateDWordData(DIT_FLIGHT, 1);
			omDragDropObject.AddDWord(prlLine->DUrno);
		}
		else
			return 0L;
	}

	ogBcHandle.BufferBc();

	omDragDropObject.BeginDrag();

	ogBcHandle.ReleaseBuffer();

	return 0L;
}


////////////////////////////////////////////////////////////////////////////
// Dragover einer Drag&Drop Aktion, wo auch immer
//
LONG CSeasonTableDlg::OnDragOver(UINT wParam, LONG lParam)
{
	int ilLine = -1;
	int ilCol = -1;
	long llUrno = 0;
	long llUrno2;

	CPoint olDropPosition;
    ::GetCursorPos(&olDropPosition);
    pomSeasonTable->pomListBox->ScreenToClient(&olDropPosition);    

	ilLine = pomSeasonTable->GetLinenoFromPoint(olDropPosition);
	ilCol = pomSeasonTable->GetColumnnoFromPoint(olDropPosition);

	
	if (!(0 <= ilLine && ilLine <= omViewer.omLines.GetSize()-1))
		return -1L;	

	if(ilCol < 0 || ilLine < 0)
		return -1L;	


    int ilClass = omDragDropTarget.GetDataClass(); 

	if(ilClass != DIT_FLIGHT)
		return -1L;	

	llUrno = omDragDropTarget.GetDataDWord(0);

	
	SEASONTABLE_LINEDATA *prlLine = (SEASONTABLE_LINEDATA *) pomSeasonTable->GetTextLineData(ilLine);
	if (prlLine == NULL)
		return -1L;

	//if(prlLine->AUrno == prlLine->DUrno)
	//	return -1L;


	if(ilCol < 15)
	{
		llUrno2 = prlLine->AUrno;
		if(!ogSeasonFlights.JoinFlightPreCheck(llUrno, llUrno2))
		{
			return -1L;
		}
	}
	else
	{
		llUrno2 = prlLine->DUrno;
		if(!ogSeasonFlights.JoinFlightPreCheck(llUrno, llUrno2))
		{
			return -1L;
		}
	}

	return 0L;
}

////////////////////////////////////////////////////////////////////////////
// Drop einer Drag&Drop Aktion, wo auch immer
//
LONG CSeasonTableDlg::OnDrop(UINT i, LONG l)
{
	if(OnDragOver(0,0) != 0L) //Last validation, sicher ist sicher
		return -1L;


	int ilLine = -1;
	int ilCol = -1;

	CPoint olDropPosition;
    ::GetCursorPos(&olDropPosition);
    pomSeasonTable->pomListBox->ScreenToClient(&olDropPosition);    

	ilLine = pomSeasonTable->GetLinenoFromPoint(olDropPosition);
	ilCol = pomSeasonTable->GetColumnnoFromPoint(olDropPosition);

	if (!(0 <= ilLine && ilLine <= omViewer.omLines.GetSize()-1))
		return -1L;	

	if(ilCol < 0 || ilLine < 0)
		return -1L;	


	long llUrno1 = 0;
	long llUrno2 = 0;
	

	SEASONTABLE_LINEDATA *prlLine = (SEASONTABLE_LINEDATA *) pomSeasonTable->GetTextLineData(pomSeasonTable->GetLinenoFromPoint(olDropPosition));

    int ilClass = omDragDropTarget.GetDataClass(); 


	llUrno1 = omDragDropTarget.GetDataDWord(0);

	if(ilCol < 15)
	{
		llUrno2 = prlLine->AUrno;
		if(!ogSeasonFlights.JoinFlightPreCheck(llUrno1, llUrno2))
		{
			CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING949), GetString(ST_FEHLER), MB_ICONWARNING);
			return -1L;
		}
	}
	else	
	{	
		llUrno2 = prlLine->DUrno;
		if(!ogSeasonFlights.JoinFlightPreCheck(llUrno1, llUrno2))
		{
			CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING949), GetString(ST_FEHLER), MB_ICONWARNING);
			return -1L;
		}
	}
	
	SEASONFLIGHTDATA *prlAFlight = ogSeasonFlights.GetFlightByUrno(llUrno1);
	SEASONFLIGHTDATA *prlDFlight = ogSeasonFlights.GetFlightByUrno(llUrno2);



	Verbinden(prlAFlight, prlDFlight);
	
	/*
	CString olText;

	olText.Format(GetString(IDS_STRING1475), prlAFlight->Flno, prlDFlight->Flno );
	
	if(MessageBox(olText, GetString(ST_FRAGE), MB_YESNO) == IDYES)
	{
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		if(ogSeasonFlights.JoinFlight(llUrno1, llUrno2) == false)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
			MessageBox(GetString(IDS_STRING949), GetString(ST_FEHLER), MB_ICONWARNING);
		}
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}
	*/
	return -1L;
}



void CSeasonTableDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);

//	m_resizeHelper.OnSize();

	if (this->pomSeasonTable != NULL && ::IsWindow(this->pomSeasonTable->m_hWnd))
	{
		CRect olrectTable;
		GetClientRect(&olrectTable);
		olrectTable.DeflateRect(1,1);     // hiding the CTable window border
		this->pomSeasonTable->SetPosition(olrectTable.left, olrectTable.right, olrectTable.top + imDialogBarHeight, olrectTable.bottom);

		this->pomSeasonTable->DisplayTable();
	}

	this->Invalidate();

/*

	CDialog::OnSize(nType, cx, cy);

	if(pomSeasonTable != NULL)
	{
		CRect rect;	
		GetClientRect(&rect);
		rect.top = rect.top + imDialogBarHeight;
		rect.bottom = rect.bottom;// - imDialogBarHeight;

		rect.InflateRect(1, 1);     // hiding the CTable window border
    
		pomSeasonTable->SetPosition(rect.left, rect.right, rect.top, rect.bottom);
	}
*/
}






void CSeasonTableDlg::OnSelchangeComboAnsicht() 
{
	CComboBox *polView = (CComboBox *)GetDlgItem(IDC_COMBO_ANSICHT);

	int ilItemID;
	CString olView;

	if ((ilItemID = polView->GetCurSel()) != CB_ERR)
	{
		polView->GetLBText(ilItemID, olView);
 
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		omViewer.ChangeViewTo(olView);
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}
	PostFlight(NULL, NULL);
}


void CSeasonTableDlg::OnButtonAnsicht() 
{
	ShowAnsicht();
	PostFlight(NULL, NULL);
}


void CSeasonTableDlg::ShowAnsicht()
{
	if(ogPrivList.GetStat("SEASONBTL_CB_Ansicht") != '1')
		return;


	SeasonFlightTablePropertySheet olDlg("SEASON", this, &omViewer, 0, GetString(IDS_STRING2749));
	if(olDlg.DoModal() != IDCANCEL)
	{
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		UpdateWindow();
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		omViewer.ChangeViewTo(omViewer.GetViewName());
		UpdateComboAnsicht();
	}
	else
	{
		UpdateWindow();
		UpdateComboAnsicht();
	}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}




void CSeasonTableDlg::UpdateComboAnsicht()
{
	CStringArray olArray;
	CStringArray olStrArr;
	CComboBox *polCB = (CComboBox *)GetDlgItem(IDC_COMBO_ANSICHT);
	polCB->ResetContent();
	omViewer.GetViews(olStrArr);
	for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
	{
		polCB->AddString(olStrArr[ilIndex]);
	}
	CString olViewName = omViewer.GetViewName();

	ilIndex = polCB->FindString(-1,olViewName);
		
	if (ilIndex != CB_ERR)
	{
		polCB->SetCurSel(ilIndex);
	}
} 



void CSeasonTableDlg::OnPrint() 
{
	omViewer.PrintTableView();	
}



void CSeasonTableDlg::OnImport() 
{

	char pclConfigPath[256];
	char pclExcelPath[256];

	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "IMPORTFLIGHTS", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, pclConfigPath);


	if(!strcmp(pclExcelPath, "DEFAULT"))
		MessageBox(GetString(IDS_STRING1478), GetString(ST_FEHLER), MB_ICONERROR);


	char *args[4];
	char slRunTxt[256] = "";
	args[0] = "child";
	sprintf(slRunTxt,"%s,%s",pcgUser,pcgPasswd);
//	args[1] = slRunTxt;
	args[1] = NULL;
	args[2] = NULL;
	args[3] = NULL;
	int ilReturn = _spawnv(_P_NOWAIT,pclExcelPath,args);



/*

	LPOPENFILENAME polOfn = new OPENFILENAME;
	char buffer[256] = "";
	LPSTR lpszStr;
	lpszStr = buffer;
	CString olStr = CString("c:\\tmp\\") + CString("*.dat");
	strcpy(buffer,(LPCTSTR)olStr);

	memset(polOfn, 0, sizeof(*polOfn));
	polOfn->lStructSize = sizeof(*polOfn) ;
	polOfn->lpstrFilter = "File(*.dat)\0*.dat\0";
	polOfn->lpstrFile = (LPSTR) buffer;
	polOfn->nMaxFile = 256;
	polOfn->lpstrTitle = "Import";

	if(GetOpenFileName(polOfn) == TRUE)
	{
		ogSeasonDlgFlights.lmBaseID = IDM_IMPORT;
		char pclDefPath[512];
		strcpy(pclDefPath, polOfn->lpstrFile);	
		InvalidateRect(NULL);
		UpdateWindow();
        CedaImpFlightData olImpFlightData;
		olImpFlightData.Import(pclDefPath);
		ogSeasonDlgFlights.lmBaseID = IDM_SEASON;
	}
	delete polOfn;
*/	
}




void CSeasonTableDlg::OnClose() 
{
	OnSchliessen();
	//CDialog::OnClose();
}

 
void CSeasonTableDlg::OnSammelbearbeitung() 
{

	SEASONFLIGHTDATA *prlAFlight = NULL;
	SEASONFLIGHTDATA *prlDFlight = NULL;

	CTime olTime = TIMENULL;
	CString olAdid = "";
	CString olAlc3;
	CString olFltn;
	CString olFlns;
	CString olCsgn;
	

	SEASONTABLE_LINEDATA *prlTableLine = (SEASONTABLE_LINEDATA *)pomSeasonTable->GetTextLineData(pomSeasonTable->GetCurrentLine());
	if (prlTableLine != NULL)
	{
		prlAFlight = ogSeasonFlights.GetFlightByUrno(prlTableLine->AUrno);
		prlDFlight = ogSeasonFlights.GetFlightByUrno(prlTableLine->DUrno);

		if((prlAFlight != NULL) && (prlDFlight != NULL))
		{

			SeasonCollectADAskDlg olDlg(this,prlAFlight, prlDFlight);

			if(olDlg.DoModal() == IDOK)
			{
				if (olDlg.prmSelFlight != NULL)
				{
					olAlc3 = olDlg.prmSelFlight->Alc3;
					olFltn = olDlg.prmSelFlight->Fltn;
					olFlns = olDlg.prmSelFlight->Flns;
					olCsgn = olDlg.prmSelFlight->Csgn;
				}

				olTime = olDlg.omSto;
				olAdid = olDlg.omAdid;
			}
			else
			{
				return;
			}
		}
		else
		{
			if(prlAFlight != NULL)
			{
				olAlc3 = prlAFlight->Alc3;
				olFltn = prlAFlight->Fltn;
				olFlns = prlAFlight->Flns;
				olCsgn = prlAFlight->Csgn;

				olTime = prlAFlight->Stoa;
				olAdid = "A";
			}

			if(prlDFlight != NULL)
			{
				olAlc3 = prlDFlight->Alc3;
				olFltn = prlDFlight->Fltn;
				olFlns = prlDFlight->Flns;
				olCsgn = prlDFlight->Csgn;

				olTime = prlDFlight->Stod;
				olAdid = "D";
			}
		}
	}

	// Check season for selected flight
	CString olSeason;
	if (!ogBCD.GetFieldBetween("SEA", "VPFR", "VPTO", CTimeToDBString(olTime, TIMENULL), "SEAS", olSeason ))
	{
		CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING1891), GetString(ST_FEHLER), MB_ICONERROR | MB_OK);
		return;
	}


	if(pogSeasonCollectDlg == NULL)
	{
		pogSeasonCollectDlg = new SeasonCollectDlg;
//		pogSeasonCollectDlg->Create(IDD_SEASONCOLLECT, pogButtonList);
		pogSeasonCollectDlg->Create(IDD_SEASONCOLLECT, NULL);
	}

	pogSeasonCollectDlg->LoadNewData(olAlc3, olFltn, olFlns, olAdid, olTime, olCsgn); 
	
		
}


void CSeasonTableDlg::OnSearch()
{
	pogFlightSearchTableDlg->Activate(omViewer.GetBaseViewName(), omViewer.GetViewName(), SEASONSCHEDULES);
}


void CSeasonTableDlg::SelectFlight(long lpUrno)
{
	omViewer.SelectLine(lpUrno);
}


bool CSeasonTableDlg::ShowFlight(long lpUrno)
{	
	
	return omViewer.ShowFlight(lpUrno);
}


void CSeasonTableDlg::DeselectAll(void)
{	
	
	omViewer.DeselectAll();
}


static void FlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    CSeasonTableDlg *polDlg = (CSeasonTableDlg *)popInstance;

    if (ipDDXType == S_DLG_GET_PREV)
        polDlg->SeasonDlgGetPrev((NEW_SEASONDLG_DATA *)vpDataPointer);

	if (ipDDXType == S_DLG_GET_NEXT)
        polDlg->SeasonDlgGetNext((NEW_SEASONDLG_DATA *)vpDataPointer);



}



void CSeasonTableDlg::SeasonDlgGetNext(NEW_SEASONDLG_DATA *popData)
{
	SEASONTABLE_LINEDATA *prlLine;
	SEASONFLIGHTDATA *prlFlight;

	if(popData->Parent != this)
		return;


	for(int i = omViewer.omLines.GetSize() - 1; i >= 0; i--)
	{
		prlLine = &omViewer.omLines[i];

		if((prlLine->AUrno == popData->CurrUrno) || (prlLine->DUrno == popData->CurrUrno))
		{
			break;
		}
	}

	if(i >= 0)
	{
		if(pomSeasonTable->SelectLine(i + 1 ) != LB_ERR)
		{
			prlLine = (SEASONTABLE_LINEDATA *)pomSeasonTable->GetTextLineData(pomSeasonTable->GetCurrentLine());
			
			if(prlLine != NULL)
			{
				if((prlFlight = ogSeasonFlights.GetFlightByUrno(prlLine->AUrno)) == NULL)
					prlFlight = ogSeasonFlights.GetFlightByUrno(prlLine->DUrno);

				if(prlFlight != NULL)
				{
					pogSeasonDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno, DLG_CHANGE, bgSeasonLocal);
				}
			}
		}
	}
}


void CSeasonTableDlg::SeasonDlgGetPrev(NEW_SEASONDLG_DATA *popData)
{

	SEASONTABLE_LINEDATA *prlLine;
	SEASONFLIGHTDATA *prlFlight;

	if(popData->Parent != this)
		return;

	for(int i = omViewer.omLines.GetSize() - 1; i >= 0; i--)
	{
		prlLine = &omViewer.omLines[i];

		if((prlLine->AUrno == popData->CurrUrno) || (prlLine->DUrno == popData->CurrUrno))
		{
			break;
		}
	}

	if(i >= 0)
	{
		if(pomSeasonTable->SelectLine(i - 1 ) != LB_ERR)
		{
			prlLine = (SEASONTABLE_LINEDATA *)pomSeasonTable->GetTextLineData(pomSeasonTable->GetCurrentLine());
			
			if(prlLine != NULL)
			{
				if((prlFlight = ogSeasonFlights.GetFlightByUrno(prlLine->AUrno)) == NULL)
					prlFlight = ogSeasonFlights.GetFlightByUrno(prlLine->DUrno);

				if(prlFlight != NULL)
				{
					pogSeasonDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno, DLG_CHANGE, bgSeasonLocal);
				}
			}
		}
	}
}



////////////////////////////////////////////////////////////////////////////
// Button Verbinden: selektierte Datens�tze (zwei) werden gejoint 
//
void CSeasonTableDlg::OnVerbinden() 
{
	CListBox *polLB = pomSeasonTable->GetCTableListBox();
	if (polLB == NULL)
		return;

	int *ilItems = NULL;
	int ilAnz = polLB->GetSelCount();
	if(ilAnz > 0)
	{
		ilItems = new int[ilAnz];
	}

	if(ilItems != NULL)
		ilAnz = polLB->GetSelItems(ilAnz, ilItems);


	if(ilAnz != 2)
	{
		CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING975), GetString(ST_FEHLER), MB_ICONWARNING);
		if(ilItems != NULL)
			delete [] ilItems;
		return;
	}
	SEASONTABLE_LINEDATA *prlTableLine1 = (SEASONTABLE_LINEDATA *)pomSeasonTable->GetTextLineData(ilItems[0]);
	SEASONTABLE_LINEDATA *prlTableLine2 = (SEASONTABLE_LINEDATA *)pomSeasonTable->GetTextLineData(ilItems[1]);

	if(ilItems != NULL)
		delete [] ilItems;

	if(prlTableLine1 == NULL || prlTableLine2 == NULL)
		return;

	long llAUrno = prlTableLine1->AUrno;
	long llDUrno = prlTableLine2->DUrno;


	if(!ogSeasonFlights.JoinFlightPreCheck(llAUrno, llDUrno))
	{
		llAUrno = prlTableLine2->AUrno;
		llDUrno = prlTableLine1->DUrno;
		if(!ogSeasonFlights.JoinFlightPreCheck(llAUrno, llDUrno))
		{
			llAUrno = prlTableLine2->DUrno;
			llDUrno = prlTableLine1->DUrno;
			if(!ogSeasonFlights.JoinFlightPreCheck(llAUrno, llDUrno))
			{
				llAUrno = prlTableLine2->AUrno;
				llDUrno = prlTableLine1->AUrno;
				if(!ogSeasonFlights.JoinFlightPreCheck(llAUrno, llDUrno))
				{
					CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING979), GetString(ST_FEHLER), MB_ICONWARNING);
					return;
				}
			}
		}
	}


	SEASONFLIGHTDATA *prlAFlight = ogSeasonFlights.GetFlightByUrno(llAUrno);
	SEASONFLIGHTDATA *prlDFlight = ogSeasonFlights.GetFlightByUrno(llDUrno);

	Verbinden(prlAFlight, prlDFlight);
	
	return;

}



////////////////////////////////////////////////////////////////////////////
// Verbinden: selektierte Datens�tze (zwei) werden gejoint 
//
void CSeasonTableDlg::Verbinden(SEASONFLIGHTDATA *prpAFlight, SEASONFLIGHTDATA *prpDFlight) 
{

	if(prpAFlight == NULL || prpDFlight == NULL)
		return;

	if (PostFlight(prpAFlight, prpDFlight))
	{
		CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_MSG_POSTFLIGHT_TEXT2), GetString(IDS_MSG_POSTFLIGHT_TEXT1), MB_ICONWARNING);
		return;
	}

	SEASONFLIGHTDATA rlFlight;

	//long llAUrno = prpAFlight->Urno;
	//long llDUrno = prpDFlight->Urno;


	CedaFlightUtilsData olFlightUtilsData;
	olFlightUtilsData.JoinFlight(prpAFlight->Urno, prpAFlight->Rkey, prpDFlight->Urno, prpDFlight->Rkey, false);

	  
	return;
}

//not supported yet. If you want handle the selectionset do it here.
LONG CSeasonTableDlg::OnTableLButtonDown(UINT wParam, LONG lParam)
{

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	SEASONTABLE_LINEDATA *prlTableLine;
	CString olUrnoList;
	int ilAnzSel = 0;

	CListBox *polLB = pomSeasonTable->GetCTableListBox();
	if (polLB == NULL)
		return 0L;

	int *ilItems = NULL;
	int ilAnz = polLB->GetSelCount();
	if(ilAnz > 0)
	{
		ilItems = new int[ilAnz];
	}

	if(ilItems != NULL)
		ilAnz = polLB->GetSelItems(ilAnz, ilItems);
	else
		return 0L;


	BOOL blpOnlyPostflights = TRUE;
	for(int i = 0; i < ilAnz; i++)
	{
		prlTableLine = (SEASONTABLE_LINEDATA *)pomSeasonTable->GetTextLineData(ilItems[i]);

		if (!CheckPostFlight(prlTableLine))
		{
			blpOnlyPostflights = FALSE;
			break;
		}
	}

	HandlePostFlight(blpOnlyPostflights);

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	if(ilItems != NULL)
		delete [] ilItems;

	return 0L;

}

BOOL CSeasonTableDlg::CheckPostFlight(SEASONTABLE_LINEDATA *prpTableLine)
{
	if (prpTableLine == NULL)
		return FALSE;

	SEASONFLIGHTDATA *prpAFlight = NULL;
	SEASONFLIGHTDATA *prpDFlight = NULL;

	if(prpTableLine->AUrno != 0)
		prpAFlight = ogSeasonFlights.GetFlightByUrno(prpTableLine->AUrno);

	if(prpTableLine->DUrno != 0)
		prpDFlight = ogSeasonFlights.GetFlightByUrno(prpTableLine->DUrno);

	if (PostFlight(prpAFlight, prpDFlight))
		return TRUE;
		
	return FALSE;
}

//not supported yet. If you want handle the postflight do it here.
BOOL CSeasonTableDlg::HandlePostFlight(BOOL blpPostflight)
{
/*
	// postflight: no changes allowed
	if (blpPostflight)
	{
		if (GetDlgItem(IDC_FREIGEBEN)) 
			m_CB_Freigeben.EnableWindow(FALSE);

		if (GetDlgItem(IDC_VERBINDEN)) 
			m_CB_Verbinden.EnableWindow(FALSE);
		
		return TRUE;
	}
	else
	{
		if (GetDlgItem(IDC_FREIGEBEN)) 
			m_CB_Freigeben.EnableWindow(TRUE);

		if (GetDlgItem(IDC_VERBINDEN)) 
			m_CB_Verbinden.EnableWindow(TRUE);

		return TRUE;
	}

	return FALSE;
*/
	return TRUE;
}

BOOL CSeasonTableDlg::PostFlight(SEASONFLIGHTDATA *prpAFlight, SEASONFLIGHTDATA *prpDFlight)
{

	BOOL blPost = FALSE;

	if (prpAFlight && prpDFlight)
	{
		if (prpAFlight->Tifa != TIMENULL && prpDFlight->Tifd != TIMENULL)
		{
			if (IsPostFlight(prpAFlight->Tifa,false,prpAFlight->Prfl) && IsPostFlight(prpDFlight->Tifd,false,prpDFlight->Prfl))
				blPost = TRUE;
		}
	}
	else if (!prpAFlight && prpDFlight)
	{
		if (prpDFlight->Tifd != TIMENULL)
		{
			if (IsPostFlight(prpDFlight->Tifd,false,prpDFlight->Prfl))
				blPost = TRUE;
		}
	}
	else if (prpAFlight && !prpDFlight)
	{
		if (prpAFlight->Tifa != TIMENULL)
		{
			if (IsPostFlight(prpAFlight->Tifa,false,prpAFlight->Prfl))
				blPost = TRUE;
		}
	}

	CWnd* opWnd = (CWnd*) this;
	if (opWnd)
	{
		if (blPost)
			ModifyWindowText(opWnd, GetString(IDS_STRINGWND_POSTFLIGHT), FALSE);
		else
			ModifyWindowText(opWnd, GetString(IDS_STRINGWND_POSTFLIGHT), TRUE);
	}

	return blPost;


/*
	if (prpAFlight && IsPostFlight(prpAFlight->Tifa))
		return TRUE;
		
	if (prpDFlight && IsPostFlight(prpDFlight->Tifd))
		return TRUE;

	return FALSE;
*/
}


LONG CSeasonTableDlg::OnTableReturnPressed( UINT wParam, LPARAM lParam)
{
	UINT ipItem = wParam;
	SEASONTABLE_LINEDATA *prlTableLine = NULL;
	prlTableLine = (SEASONTABLE_LINEDATA *)pomSeasonTable->GetTextLineData(ipItem);

	if (prlTableLine)
	{
		char clAdid='X';
		SEASONFLIGHTDATA *prlFlight = NULL;

		if((prlFlight = ogSeasonFlights.GetFlightByUrno(prlTableLine->AUrno)) == NULL)
		{
			prlFlight = ogSeasonFlights.GetFlightByUrno(prlTableLine->DUrno);
			clAdid = 'D';
		}
		else
			clAdid = 'A';
		
		// entsprechenden Diaolg aufrufen
		if(prlFlight)
			CFPMSApp::ShowFlightRecordData(this, prlFlight->Ftyp[0], prlFlight->Urno, prlFlight->Rkey, clAdid, prlFlight->Tifa, bgSeasonLocal, 'S');
	}

	return 0L;
}

LONG CSeasonTableDlg::OnTableSelChanged( UINT wParam, LPARAM lParam)
{
    int ilLineNo = pomSeasonTable->pomListBox->GetCurSel();    
	
	SEASONTABLE_LINEDATA *prlTableLine = NULL;
	prlTableLine = (SEASONTABLE_LINEDATA *)pomSeasonTable->GetTextLineData(ilLineNo);
//	CheckPostFlight(prlTableLine);
//	HandlePostFlight(CheckPostFlight(prlTableLine)); 

	omViewer.UpdateWindowTitle();		// 050302 MVy: adjust the informations in the dialog title, remember that the title contains number of selected items
	HandlePostFlight(CheckPostFlight(prlTableLine));
	return 0L;
}
