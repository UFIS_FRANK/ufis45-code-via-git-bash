// ReportSelectDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <resrc1.h>
#include <fpms.h>
#include <ReportSelectDlg.h>
#include <ReportSeqDlg.h>
#include <ReportSeq1.h>
#include <ReportSeqFieldDlg.h>
#include <ReportSeqField3DateTimeDlg.h>
#include <ReportSeqDateTimeDlg.h>
#include <ReportArrDepSelctDlg.h> 
#include <ReportTableDlg.h>
#include <ReportReasonForChange.h>
#include <RotationDlgCedaFlightData.h>
#include <SeasonCedaFlightData.h>
#include <CedaBasicData.h>
#include <Afxdisp.h>
#include <PrivList.h>
#include <buttonlistdlg.h>
#include <ReportMovements.h>
#include <CCSCedaData.h>
#include <OvernightTableDlg.h>
#include <Utils.h>
#include <DailyCcaPrintDlg.h>
#include <ReportReasonForChange.h> 

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



// lokale Variablen , Definitionen und Funktionen
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define _MIT_TRACE_		FALSE

// f�r Report Wochenplan
SeasonCedaFlightData omCedaFlightData;

// f�r Report der POPS Daily/Saisonliste + der CCAListe
RotationDlgCedaFlightData omDailyCedaFlightData( false );	// false => inkl. Archivtabelle 
															// sonstige Reports s. OnOK() olData


// Konstanten f�r WHERE CLAUSES
///////////////////////////////////////////////////////
const bool blAllFtyps = true ;		// Reports mit allen Flugtypen ( au�er f�r HAJ )

const CString comFtypAll("") ;								// Ftypen : alle
const CString comFtypO("AND FTYP='O'") ;					// Ftypen : operation
const CString comFtypOS("AND (FTYP='O' OR FTYP='S')") ;	// Ftypen : operation + schedule
const CString comFtypOSX("AND (FTYP='O' OR FTYP='S' OR FTYP='X')") ;	// Ftypen : operation + schedule
const CString comFtyp_SGT("AND (FTYP<>'S' AND FTYP<>'G' AND FTYP<>'T')");
  
								// Positionen zur Auswertung der Selektion( WHERE Bedingung POPS Daily !)
const int imWherePos1 = 9 ;		// Endtag
const int imWherePos2 = 36 ;	// Starttag
const int imWherePos3 = 114 ;	// Verkehrstag bei Saison

const int imProgRange = 15 ;	// Max-Range Progressanzeige



static int CompareToAlc2(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2);
static int CompareToDes3(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2);
static int CompareToTtypAlc(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2);
static int CompareToDes3Alc(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2);
static int CompareToAlcDes3(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2);
static int CompareToTtyp(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2);
static int CompareToAlc3(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2);
static int CompareToAlc2Des3(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2);
static int CompareRotation(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2);
static int CompareSkey(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2);
static int CompareToAdid(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2);
static int CompareReasonData(const REASON_FOR_CHANGE **e1, const REASON_FOR_CHANGE **e2); 
static int CompareReasonAloc(const REASON_FOR_CHANGE **e1, const REASON_FOR_CHANGE **e2); 
static int CompareReasonCdat(const REASON_FOR_CHANGE **e1, const REASON_FOR_CHANGE **e2); 

static int CompareChrono(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2);


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Struct PARTIALREAD
// Schrittweise auf DB zugreifen ( bei Listungen mit Intervallen ) 
PARTIALREAD::PARTIALREAD()
{
	WhereClause.Empty() ;
	StartTime = TIMENULL ;
	EndTime = TIMENULL ;
	Range = RRDefault ;
	Case = -1 ;
	omData = NULL ;
	Fields.Empty() ;
	onlyAFT = true ;
	Sort = false ;
	SortArt = 0 ;
}

// Vorbelegung f�r Hauptanwendung ( alle, au�er POPS case 14+15 ! )
void PARTIALREAD::Preset()
{
	// alle Intervallzugriffe f�r "NORMALE" Reports werden unter 0 zusammengefa�t    
	Case = 0 ;

	// hbe 5/99
	// WhereClause = "WHERE (STOA BETWEEN '%s' AND '%s' OR STOD BETWEEN '%s' AND '%s')";
	WhereClause.Format("WHERE ((STOA BETWEEN '%%s' AND '%%s' AND DES3='%s' ) OR ( STOD BETWEEN '%%s' AND '%%s' AND ORG3='%s'))",
				pcgHome, pcgHome );
	Range = RRWeek ;
	onlyAFT = false ;
	Sort = true ;
	SortArt = RotationDlgCedaFlightData::DefSort ;

	// m�ssen immer versorgt werden !
	// StartTime = DBStringToDateTime(oMinStr) ;
	// EndTime  = DBStringToDateTime(oMaxStr) ;
	// omData = &olData.omData ;
	// Fields = "TIFA,....." ;
}



/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CReportSelectDlg Preset 

// Konstruktor
CLastDlgValues::CLastDlgValues()
{
	MinTime = CTime::GetCurrentTime();
	MaxTime = CTime::GetCurrentTime();
	MinTimeRange = CTime::GetCurrentTime();
	MaxTimeRange = CTime::GetCurrentTime();
	Time = CTime::GetCurrentTime();
	Movement = 0;
	LoadPaxType = 0;
/*	Season=false; Term=X_TERMDEF; 
	AirlineEx="";	Verkehr="" ; 
	MinTime = CTime::GetCurrentTime() ; MaxTime = CTime::GetCurrentTime() ;
	Minuten = "180" ; Radio = 0 ;
	Airlines.Empty(); Select.Empty(); Saison.Empty(); Messe.Empty(); Flno.Empty(); 
*/
} ;

// Destruktor ( entspr. Reset )
CLastDlgValues::~CLastDlgValues()
{
/*	MinDate.Empty() ;
	MaxDate.Empty() ;
	MinTime = CTime::GetCurrentTime() ;
	MaxTime = CTime::GetCurrentTime() ;
	Season = false ;
	Term = X_TERMDEF ;
	Airlines.Empty() ;
	AirlineEx = "" ;
	Verkehr = "" ;
	Minuten = "180" ; Radio = 0 ;
	Airlines.Empty(); Select.Empty(); Saison.Empty(); Messe.Empty(); Flno.Empty(); 
*/
}


void CLastDlgValues::SetPreset(int ipTable, CTime & pStart, CTime & pEnd, CString & pSelect)
{
	switch( ipTable )
	{
	case -1:
		// Reset Presetstruct
		// ~CLastDlgValues() ;
		break;
	case 12:
	{
/*		// Wochenplan
		int ilHeute = CTime( CTime::GetCurrentTime() ).GetDayOfWeek() ;
		CTimeSpan olJump(6,0,0,0) ;	// Default f�r Sonntag
		if( ilHeute != 1 )
		{
			olJump = CTimeSpan( ilHeute - 2, 0,0,0 ) ;
		}
		pStart = CTime::GetCurrentTime() - olJump ;
		pEnd = CTime() ;
		pSelect = GetSelect() ;
		// nur 2Ltr Code
		// if( pSelect.GetLength() > 2 ) pSelect.Empty() ;
*/
	}

	break ;
	
	default:
		break ;


	}

}



/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CReportSelectDlg 


CReportSelectDlg::CReportSelectDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CReportSelectDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CReportSelectDlg)
	m_List1 = -1;
	//}}AFX_DATA_INIT
	pomReportTableDlg = NULL;
	// Default Where-Clauses f�r Reports
	omDefaultWhereSto.Format(
		" ((STOA BETWEEN '%%s' AND '%%s' AND DES3='%s' ) OR ( STOD BETWEEN '%%s' AND '%%s' AND ORG3='%s'))",
				pcgHome, pcgHome );
 	omDefaultWhereTif.Format( 
		"WHERE ((TIFA BETWEEN '%%s' AND '%%s' AND DES3='%s' ) OR ( TIFD BETWEEN '%%s' AND '%%s' AND ORG3='%s'))",
				pcgHome, pcgHome );
	CDialog::Create(CReportSelectDlg::IDD, pParent);
	m_key = "DialogPosition\\ReportsSelection";

}

CReportSelectDlg::~CReportSelectDlg()
{
	if (pomReportTableDlg != NULL)
	{
		pomReportTableDlg->DestroyWindow();
		delete pomReportTableDlg;
	}
}

void CReportSelectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CReportSelectDlg)
	DDX_Control(pDX, IDC_UTCLOCAL, m_CE_UtcToLocal);
	DDX_Control(pDX, IDC_ST_PROG, m_ST_Progress);
	DDX_Control(pDX, IDC_PR_READ, m_PR_Read);
	DDX_Control(pDX, IDC_LIST1, m_CR_List1);
	DDX_Control(pDX, IDC_LIST2, m_CR_List2);
	DDX_Control(pDX, IDC_LIST3, m_CR_List3);
	DDX_Control(pDX, IDC_LIST4, m_CR_List4);
	DDX_Control(pDX, IDC_LIST5, m_CR_List5);
	DDX_Control(pDX, IDC_LIST6, m_CR_List6);
	DDX_Control(pDX, IDC_LIST7, m_CR_List7);
	DDX_Control(pDX, IDC_LIST8, m_CR_List8);
	DDX_Control(pDX, IDC_LIST9, m_CR_List9);
	DDX_Control(pDX, IDC_LIST10, m_CR_List10);
	DDX_Control(pDX, IDC_LIST11, m_CR_List11);
	DDX_Control(pDX, IDC_LIST12, m_CR_List12);
	DDX_Control(pDX, IDC_LIST13, m_CR_List13);
	DDX_Control(pDX, IDC_LIST14, m_CR_List14);
	DDX_Control(pDX, IDC_LIST15, m_CR_List15);
	DDX_Control(pDX, IDC_LIST16, m_CR_List16);
	DDX_Control(pDX, IDC_LIST18, m_CR_List18);
	DDX_Control(pDX, IDC_LIST19, m_CR_List19);
	DDX_Control(pDX, IDC_LIST20, m_CR_List20);
	DDX_Control(pDX, IDC_LIST21, m_CR_List21);
	DDX_Control(pDX, IDC_LIST22, m_CR_List22);
	DDX_Control(pDX, IDC_LIST23, m_CR_List23);
	DDX_Control(pDX, IDC_LIST24, m_CR_List24);
	DDX_Control(pDX, IDC_LIST25, m_CR_List25);
	DDX_Control(pDX, IDC_LIST26, m_CR_List26);
	DDX_Control(pDX, IDC_LIST27, m_CR_List27);
	DDX_Control(pDX, IDC_LIST28, m_CR_List28);
	DDX_Radio(pDX, IDC_LIST20, m_List1);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CReportSelectDlg, CDialog)
	//{{AFX_MSG_MAP(CReportSelectDlg)
	ON_WM_SIZE()
 	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CReportSelectDlg 


// wenn gew�nscht Zeiten in Utc wandeln
void CReportSelectDlg::MakeUtc( CString& ropMinStr, CString& ropMaxStr )
{
	if(bgReportLocal)
	{
		CTime olUtcMinDate = DBStringToDateTime(ropMinStr);
		CTime olUtcMaxDate = DBStringToDateTime(ropMaxStr);

		ogBasicData.LocalToUtc(olUtcMinDate);
		ogBasicData.LocalToUtc(olUtcMaxDate);

		ropMinStr = olUtcMinDate.Format( "%Y%m%d%H%M%S" );
		ropMaxStr = olUtcMaxDate.Format( "%Y%m%d%H%M%S" );
	}
}

// Progressbar anzeigen
void CReportSelectDlg::ShowStatus() 
{
	m_PR_Read.ShowWindow( SW_SHOW ) ;	
	m_ST_Progress.ShowWindow( SW_SHOW ) ;
	m_ST_Progress.SetWindowText( GetString( IDS_STRING1205) ) ;
	m_ST_Progress.UpdateWindow() ;
	m_PR_Read.SetPos( 1 ) ;	
}

// Progressbar weg
void CReportSelectDlg::HideStatus() 
{
	m_ST_Progress.SetWindowText( "" ) ;
	m_ST_Progress.UpdateWindow() ;
	m_PR_Read.ShowWindow( SW_HIDE ) ;	
	m_ST_Progress.ShowWindow( SW_HIDE ) ;
}



void CReportSelectDlg::OnOK() 
{
	UpdateData(TRUE);

	CWnd* olpCwnd = pogButtonList->GetDlgItem(IDC_BEENDEN);
	if (olpCwnd)
		olpCwnd->EnableWindow(FALSE);

	StartTableDlg();

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	if (olpCwnd)
		olpCwnd->EnableWindow(TRUE);

} // end OnOK()





bool CReportSelectDlg::StartTableDlg()
{

	bgReportLocal = m_CE_UtcToLocal.GetCheck() ? true : false ;

	RotationDlgCedaFlightData olData;	// POPS: see omDailyCedaFlightData 
	RotationDlgCedaFlightData olData2;	// POPS: see omDailyCedaFlightData 

	bool blOk = false;
	CTime olUtcMinDate;
	CTime olUtcMaxDate;
	
	CString olFtyp( comFtypO ) ;	// default Operation
	olFtyp = comFtypOS;	

	//Flights by Airline
	if (m_CR_List1.GetCheck() == 1)
	{
		TRACE("CReportSelectDlg::StartTableDlg: List1\n");

		olFtyp = comFtypOSX;	

		CTime olMinDate = omLastValues.GetMinTime() ;
		CTime olMaxDate = omLastValues.GetMaxTime() ;
		char pclSelect[128];

		strcpy(pclSelect, omLastValues.GetAirlines() );

		CString olStr1, olStr2;
		olStr1 = GetString(IDS_REPORTS_RB_FlightSameAirline);
		olStr2 = GetString(IDS_STRING1046);

		CReportSeqFieldDlg olReportDlg(this, olStr1, olStr2, &olMinDate, &olMaxDate, pclSelect, "A|#A|#A|#", 2, 3);

		if(olReportDlg.DoModal() == IDOK)
		{
			
			ShowStatus();
			m_PR_Read.StepIt() ;

			// R�ckgabewerte festhalten
			omLastValues.SetMinTime( olMinDate ) ;
			omLastValues.SetMaxTime( olMaxDate ) ;
			omLastValues.SetAirlines( pclSelect ) ;

			blOk = true;

			CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
			CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
			MakeUtc( oMinStr, oMaxStr ) ;

			CString olAlc("") ;
			if(strlen(pclSelect) == 2)
			{
				olAlc.Format(" AND ALC2='%s'", pclSelect ) ;
			}
			else if(strlen(pclSelect) == 3)
			{
				olAlc.Format(" AND ALC3='%s'", pclSelect ) ;
			}

			// Portionsweise einlesen
			PARTIALREAD olPartial ;
			olPartial.Preset() ;
			CString olWherePlus ;
			olWherePlus.Format("%s %s", olAlc, olFtyp );
			olPartial.WhereClause += olWherePlus;
			olPartial.StartTime = DBStringToDateTime(oMinStr) ;
			olPartial.EndTime  = DBStringToDateTime(oMaxStr) ;
			olPartial.omData = &olData.omData ;
			olPartial.Fields = "RKEY,SKEY,SEAS,FLNO,TTYP,FTYP,ORG3,VIA3,ACT3,DES3,VIA3,ALC2,ALC3,STOA,STOD,ONBL,OFBL,VIAL,AIRB,LAND";
			
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

			bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;

			HideStatus() ;

			if( ! blReadRet )
			{
				//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
				MessageBox(GetString(IDS_STRING919), GetString(IDS_REPORTS_RB_FlightSameAirline), MB_OK);
			}
			else	
			{
				char pclInfo[100];
				oMinStr = olMinDate.Format( "%d.%m.%Y" );
				oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
				sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
				olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

				CReportTableDlg olReportTableDlg(this, 0, &(olData.omData), pclInfo, pclSelect);
				olReportTableDlg.DoModal();
			}
		}
	}

	//Flights by Origin
	else if (m_CR_List2.GetCheck() == 1)
	{
		TRACE("CReportSelectDlg::StartTableDlg: List2\n");

		CTime olMinDate = omLastValues.GetMinTime() ;
		CTime olMaxDate = omLastValues.GetMaxTime() ;
		char pclSelect[128];

		strcpy(pclSelect, omLastValues.GetOrigin() );

		CString olStr1, olStr2;
		olStr1 = GetString(REPORTS_RB_FlightsByOrigin);
		olStr2 = GetString(IDS_ORIGIN);

		CReportSeqFieldDlg olReportDlg(this, olStr1, olStr2, &olMinDate, &olMaxDate, pclSelect, "AAAA", 3, 4);

		if(olReportDlg.DoModal() == IDOK)
		{
			ShowStatus() ;
			m_PR_Read.StepIt() ;

			// R�ckgabewerte festhalten
			omLastValues.SetMinTime( olMinDate ) ;
			omLastValues.SetMaxTime( olMaxDate ) ;
			omLastValues.SetOrigin( pclSelect ) ;

			blOk = true;

			CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
			CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
			MakeUtc( oMinStr, oMaxStr ) ;

			CString olOrg("") ;
			if(strlen(pclSelect) == 3)
			{
				olOrg.Format(" AND ORG3='%s'", pclSelect ) ;
			}
			else if(strlen(pclSelect) == 4)
			{
				olOrg.Format(" AND ORG4='%s'", pclSelect ) ;
			}
			else
			{
				// Airlines nur 2Ltr oder 3Ltr-Code !
				if(!bgReports)
				{
				MessageBox("3Ltr/4Ltr-Code only!", GetString(IDS_WARNING), MB_ICONWARNING );
				return false;
				}
			}
			
			if(bgReports&&(strlen(pclSelect)==1||strlen(pclSelect)==2))
			{
              MessageBox("3Ltr/4Ltr-Code only!", GetString(IDS_WARNING), MB_ICONWARNING );
			  return false;
			}
			// Portionsweise einlesen
			// 2/99 hbe
			PARTIALREAD olPartial ;
			olPartial.Preset() ;
			CString olWherePlus ;
			olWherePlus.Format("%s %s", olOrg, olFtyp );
			if(bgReports && strlen(pclSelect)==0)
			{
			olWherePlus.Format("%s ", olFtyp );
			}
			olPartial.WhereClause.Format("WHERE (STOA BETWEEN '%%s' AND '%%s' AND DES3='%s' )", pcgHome, pcgHome );
			olPartial.WhereClause += olWherePlus;
			olPartial.StartTime = DBStringToDateTime(oMinStr) ;
			olPartial.EndTime  = DBStringToDateTime(oMaxStr) ;
			olPartial.omData = &olData.omData ;
			
			olPartial.Fields = "RKEY,SKEY,SEAS,FLNO,TTYP,VIA3,ACT3,DES3,ALC2,ALC3,STOA,STOD,ONBL,OFBL,VIAL,ORG3,ORG4,FLNO,STOA,ETAI,LAND,ACT3,ACT5,VIAN";
			if(bgReports)
			olPartial.Fields = "RKEY,SKEY,SEAS,FLNO,TTYP,VIA3,ACT3,DES3,ALC2,ALC3,STOA,STOD,ONBL,OFBL,VIAL,ORG3,ORG4,PSTA,PSTD,FLNO,STOA,ETAI,LAND,ACT3,ACT5,VIAN,REGN";
			
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

			bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
 			HideStatus() ;
			if( ! blReadRet )
			{
				//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
				MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
			}
			else	
			{
				char pclInfo[100];
				oMinStr = olMinDate.Format( "%d.%m.%Y" );
				oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
				sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
				olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

				CReportTableDlg olReportTableDlg(this, 1, &(olData.omData), pclInfo, pclSelect);
				olReportTableDlg.DoModal();
			}

			HideStatus() ;
		}
	}

	//	Flights by A/C Registration
	else if (m_CR_List3.GetCheck() == 1)
	{
		TRACE("CReportSelectDlg::StartTableDlg: List3\n");
		CTime olMinDate = omLastValues.GetMinTime() ;
		CTime olMaxDate = omLastValues.GetMaxTime() ;
		char pclSelect[128];

		strcpy(pclSelect, omLastValues.GetRegen() );

		CString olStr1, olStr2;
		olStr1 = GetString(REPORTS_RB_FlightsByRegistration);
		olStr2 = GetString(IDS_REGISTRATION);

		CReportSeqFieldDlg olReportDlg(this, olStr1, olStr2, &olMinDate, &olMaxDate, pclSelect, 
									   "#|A|'.'|'-'#|A|'.'|'-'#|A|'.'|'-'#|A|'.'|'-'#|A|'.'|'-'#|A|'.'|'-'#|A|'.'|'-'#|A|'.'|'-'#|A|'.'|'-'#|A|'.'|'-'#|A|'.'|'-'#|A|'.'|'-'", 5, 12);

		if(olReportDlg.DoModal() == IDOK)
		{
			ShowStatus() ;
			m_PR_Read.StepIt() ;

			// R�ckgabewerte festhalten
			omLastValues.SetMinTime( olMinDate ) ;
			omLastValues.SetMaxTime( olMaxDate ) ;
			omLastValues.SetRegen( pclSelect ) ;

			blOk = true;

			CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
			CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
			MakeUtc( oMinStr, oMaxStr ) ;

			CString olACType("");
			if(strlen(pclSelect) <= 12 && strlen(pclSelect) > 0)
			{
				olACType.Format(" AND REGN='%s'", pclSelect ) ;
			}
			else
			{
				// REGN max 12 Zeichen lang
				if(!bgReports)
				{
				MessageBox("12 Characters only!", GetString(IDS_WARNING), MB_ICONWARNING );
				return false;
				}
			}	
			
			// Portionsweise einlesen
			// 2/99 hbe
			PARTIALREAD olPartial ;
			olPartial.Preset() ;
			CString olWherePlus ;
			olWherePlus.Format("%s %s", olACType, olFtyp );
			if(bgReports && strlen(pclSelect)==0)
			{
				olWherePlus.Format("%s", olFtyp );
			}
			olPartial.WhereClause += olWherePlus;
			olPartial.StartTime = DBStringToDateTime(oMinStr) ;
			olPartial.EndTime  = DBStringToDateTime(oMaxStr) ;
			olPartial.omData = &olData.omData ;
			olPartial.Fields = "RKEY,SKEY,SEAS,FLNO,ACT3,DES3,STOA,STOD,ORG3,ORG4,DES3,DES4,FLNO,STOA,AIRB,LAND,ACT3,ACT5,ADID,REGN";
			if(bgReports)
				olPartial.Fields = "RKEY,SKEY,SEAS,FLNO,ACT3,DES3,STOA,STOD,ORG3,ORG4,DES3,DES4,FLNO,STOA,AIRB,LAND,ACT3,ACT5,ADID,REGN,TTYP,PSTA,PSTD,REGN";
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

			bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
 			HideStatus() ;
			if( ! blReadRet )
			{
				//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
				MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
			}
			else	
			{
				char pclInfo[100];
				oMinStr = olMinDate.Format( "%d.%m.%Y" );
				oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
				sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
				olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

				CReportTableDlg olReportTableDlg(this, 2, &(olData.omData), pclInfo, pclSelect);
				olReportTableDlg.DoModal();
			}

			HideStatus() ;
		}
	}

	// Flights by Destination
	else if (m_CR_List4.GetCheck() == 1)
	{
		TRACE("CReportSelectDlg::StartTableDlg: List4\n");

		CTime olDate;
		CTime olMinDate = omLastValues.GetMinTime() ;
		CTime olMaxDate = omLastValues.GetMaxTime() ;
		char pclSelect[128];

		strcpy(pclSelect, omLastValues.GetDestin() );

		CString olStr1, olStr2;
		olStr1 = GetString(REPORTS_RB_FlightsByDestination);
		olStr2 = GetString(IDS_DESTINATION);

		CReportSeqFieldDlg olReportDlg(this, olStr1, olStr2, &olMinDate, &olMaxDate, pclSelect, "AAAA", 3, 4);

		if(olReportDlg.DoModal() == IDOK)
		{
			ShowStatus() ;
			m_PR_Read.StepIt() ;

			// R�ckgabewerte festhalten
			omLastValues.SetMinTime( olMinDate ) ;
			omLastValues.SetMaxTime( olMaxDate ) ;
			omLastValues.SetDestin( pclSelect ) ;

			blOk = true;

			CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
			CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
			MakeUtc( oMinStr, oMaxStr ) ;

			CString olDes("");
			if(strlen(pclSelect) == 3)
			{
				olDes.Format(" AND DES3='%s'", pclSelect ) ;
			}
			else if(strlen(pclSelect) == 4)
			{
				olDes.Format(" AND DES4='%s'", pclSelect ) ;
			}
			else
			{
    			if(!bgReports)
				{
				MessageBox("3Ltr/4Ltr-Code only!", GetString(IDS_WARNING), MB_ICONWARNING );
				return false;
				}
			}
			
			if(bgReports&&(strlen(pclSelect)==1||strlen(pclSelect)==2))
			{
              MessageBox("3Ltr/4Ltr-Code only!", GetString(IDS_WARNING), MB_ICONWARNING );
			  return false;
			}
			// Portionsweise einlesen
			// 2/99 hbe
			PARTIALREAD olPartial ;
			olPartial.Preset() ;
			CString olWherePlus ;
			olWherePlus.Format("%s %s", olDes, olFtyp );
			if(bgReports && strlen(pclSelect)==0)
			{
			olWherePlus.Format("%s ", olFtyp );
			}
			olPartial.WhereClause.Format("WHERE (STOD BETWEEN '%%s' AND '%%s' AND ORG3='%s' )", 
					           pcgHome, pcgHome );
			olPartial.WhereClause += olWherePlus;
			olPartial.StartTime = DBStringToDateTime(oMinStr) ;
			olPartial.EndTime  = DBStringToDateTime(oMaxStr) ;
			olPartial.omData = &olData.omData ;
			olPartial.Fields = "RKEY,SKEY,SEAS,FLNO,TTYP,VIA3,ACT3,DES3,ALC2,ALC3,STOA,STOD,ONBL,OFBL,VIAL,ORG3,ORG4,DES3,DES4,FLNO,STOA,ETAI,LAND,AIRB,ETDI,ACT3,ACT5,VIAN";
			if(bgReports)
				olPartial.Fields = "RKEY,SKEY,SEAS,FLNO,TTYP,VIA3,ACT3,DES3,ALC2,ALC3,STOA,STOD,ONBL,OFBL,VIAL,ORG3,ORG4,PSTD,DES3,DES4,FLNO,STOA,ETAI,LAND,AIRB,ETDI,ACT3,ACT5,VIAN,REGN";
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

			bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
 			HideStatus() ;
			if( ! blReadRet )
			{
				//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
				MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
			}
			else	
			{
				char pclInfo[100];
				oMinStr = olMinDate.Format( "%d.%m.%Y" );
				oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
				sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
				olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

				CReportTableDlg olReportTableDlg(this, 3, &(olData.omData), pclInfo, pclSelect);
				olReportTableDlg.DoModal();
			}

			HideStatus() ;
		}
	}

	// Flights by A/C Type
	else if (m_CR_List5.GetCheck() == 1)
	{
		TRACE("CReportSelectDlg::StartTableDlg: List5\n");

		CTime olDate;
		char pclSelect[128];
		CTime olMinDate = omLastValues.GetMinTime() ;
		CTime olMaxDate = omLastValues.GetMaxTime() ;

		strcpy(pclSelect, omLastValues.GetAcType() );

		CString olStr1, olStr2;
		olStr1 = GetString(REPORTS_RB_FlightsByACType);
		olStr2 = GetString(IDS_AC_TYPE);

		CReportSeqFieldDlg olReportDlg(this, olStr1, olStr2, &olMinDate, &olMaxDate, pclSelect,"A|#A|#A|#A|#A|#", 3, 5);

		if(olReportDlg.DoModal() == IDOK)
		{
			ShowStatus() ;
			m_PR_Read.StepIt() ;

			// R�ckgabewerte festhalten
			omLastValues.SetMinTime( olMinDate ) ;
			omLastValues.SetMaxTime( olMaxDate ) ;
			omLastValues.SetAcType( pclSelect ) ;

			blOk = true;

			CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
			CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
			MakeUtc( oMinStr, oMaxStr ) ;

			CString olACType("");
			if(strlen(pclSelect) == 3)
			{
				olACType.Format(" AND ACT3='%s'", pclSelect ) ;
			}
			else if(strlen(pclSelect) == 5 || strlen(pclSelect) == 4)
			{
				olACType.Format(" AND ACT5='%s'", pclSelect ) ;
			}
			else
			{
				// AC-Type nur 2Ltr oder 3Ltr-Code !
				if(!bgReports)
				{
				MessageBox("IATA/ICAO-Code only!", GetString(IDS_WARNING), MB_ICONWARNING );
				return false;
				}			
			}

			if(bgReports && (strlen(pclSelect)==1||strlen(pclSelect)==2))
			{
				MessageBox("IATA/ICAO-Code only!", GetString(IDS_WARNING), MB_ICONWARNING );
				return false;
			}			
			
			// Portionsweise einlesen
			// 2/99 hbe
			PARTIALREAD olPartial ;
			olPartial.Preset() ;
			CString olWherePlus ;
			olWherePlus.Format("%s %s", olACType, olFtyp );
			if(bgReports && strlen(pclSelect)==0)
			{
				olWherePlus.Format("%s ", olFtyp );
			}
			olPartial.WhereClause += olWherePlus;
			olPartial.StartTime = DBStringToDateTime(oMinStr) ;
			olPartial.EndTime  = DBStringToDateTime(oMaxStr) ;
			olPartial.omData = &olData.omData ;
			olPartial.Fields = "RKEY,SKEY,SEAS,FLNO,TTYP,ACT3,DES3,STOA,STOD,ORG3,ORG4,DES3,DES4,FLNO,STOA,AIRB,LAND,ACT3,ACT5,ADID";
			if(bgReports)
				olPartial.Fields = "RKEY,SKEY,SEAS,FLNO,TTYP,ACT3,DES3,STOA,STOD,ORG3,ORG4,DES3,DES4,FLNO,STOA,AIRB,LAND,ACT3,ACT5,ADID,PSTA,PSTD,REGN";
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

			bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
 			HideStatus() ;
			if( ! blReadRet )
			{
				//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
				MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
			}
			else	
			{
				char pclInfo[100];
				oMinStr = olMinDate.Format( "%d.%m.%Y" );
				oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
				sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
				olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

				CReportTableDlg olReportTableDlg(this, 4, &(olData.omData), pclInfo, pclSelect);
				olReportTableDlg.DoModal();
			}

			HideStatus() ;
		}
	}

	// Flights by Nature Code
	else if (m_CR_List6.GetCheck() == 1)
	{
		TRACE("CReportSelectDlg::StartTableDlg: List6\n");

		CTime olMinDate = omLastValues.GetMinTime() ;
		CTime olMaxDate = omLastValues.GetMaxTime() ;
		char pclSelect[128];
		strcpy(pclSelect, omLastValues.GetNature() );

		CString olStr1, olStr2;
		olStr1 = GetString(REPORTS_RB_FlightsByNatureCode);
		olStr2 = GetString(IDS_NATURECODE);

		CReportSeqFieldDlg olReportDlg(this, olStr1, olStr2, &olMinDate, &olMaxDate, pclSelect, "XXXXX", 1, 5);

		if(olReportDlg.DoModal() == IDOK)
		{
			ShowStatus() ;
			m_PR_Read.StepIt() ;

			// R�ckgabewerte festhalten
			omLastValues.SetMinTime( olMinDate ) ;
			omLastValues.SetMaxTime( olMaxDate ) ;
			omLastValues.SetNature( pclSelect ) ;

			blOk = true;

			CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
			CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
			MakeUtc( oMinStr, oMaxStr ) ;

			CString olTType("");
			if(strlen(pclSelect) <= 5 && strlen(pclSelect) > 0)
			{
				olTType.Format(" AND TTYP='%s'", pclSelect ) ;
			}
			else
			{   if(!bgReports)
				{
				MessageBox("5Ltr-Code only!", GetString(IDS_WARNING), MB_ICONWARNING );
				return false;
				}
			}

			
			
			// Portionsweise einlesen
			// 2/99 hbe
			PARTIALREAD olPartial ;
			olPartial.Preset() ;
			CString olWherePlus ;
			olWherePlus.Format("%s %s", olTType, olFtyp );
			if(bgReports && strlen(pclSelect)==0)
			{
			olWherePlus.Format("%s ", olFtyp );
			}
			olPartial.WhereClause += olWherePlus;
			olPartial.StartTime = DBStringToDateTime(oMinStr) ;
			olPartial.EndTime  = DBStringToDateTime(oMaxStr) ;
			olPartial.omData = &olData.omData ;
			olPartial.Fields = "RKEY,SKEY,SEAS,FLNO,TTYP,ACT3,DES3,STOA,STOD,ORG3,ORG4,DES3,DES4,FLNO,STOA,AIRB,LAND,ACT3,ACT5,ADID";
			if(bgReports)
				olPartial.Fields = "RKEY,SKEY,SEAS,FLNO,TTYP,ACT3,DES3,STOA,STOD,ORG3,ORG4,DES3,DES4,FLNO,STOA,AIRB,LAND,ACT3,ACT5,ADID,PSTA,PSTD,REGN";
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

			bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
 			HideStatus() ;
			if( ! blReadRet )
			{
				//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
				MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
			}
			else	
			{
				char pclInfo[100];
				oMinStr = olMinDate.Format( "%d.%m.%Y" );
				oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
				sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
				olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

				CReportTableDlg olReportTableDlg(this, 5, &(olData.omData), pclInfo, pclSelect);
				olReportTableDlg.DoModal();
			}

			HideStatus() ;
		}
	}

	// Aircraft Movements per Hour
	else if (m_CR_List13.GetCheck() == 1)
	{
		TRACE("CReportSelectDlg::StartTableDlg: List13\n");
		CTime olMinDate = omLastValues.GetMinTime() ;
		CTime olMaxDate = omLastValues.GetMaxTime() ;

		int ilSelect = omLastValues.GetMovement() ;
		CReportMovementsDlg olReportDlg1(this, GetString(REPORTS_RB_AircraftMovementsPerHour), &olMinDate, &olMaxDate, &ilSelect);

		if(olReportDlg1.DoModal() == IDOK)
		{
			ShowStatus() ;
			m_PR_Read.StepIt() ;

			// R�ckgabewerte festhalten
			omLastValues.SetMinTime( olMinDate ) ;
			omLastValues.SetMaxTime( olMaxDate ) ;
			omLastValues.SetMovement( ilSelect ) ;

			blOk = true;
			CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
			CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
			MakeUtc( oMinStr, oMaxStr ) ;

			CString olOrgDes;

			PARTIALREAD olPartial;
			olPartial.Preset();

			char pclSelect[256];
			CString olWherePlusActTime;
			if (ilSelect == 1)
			{
				olPartial.WhereClause.Format("WHERE ((TIFA BETWEEN '%%s' AND '%%s' AND DES3='%s' AND ONBL BETWEEN '%s' AND '%s') OR  (TIFD BETWEEN '%%s' AND '%%s' AND ORG3='%s' AND OFBL BETWEEN '%s' AND '%s'))",
												 pcgHome, oMinStr, oMaxStr, pcgHome , oMinStr, oMaxStr);
				strcpy (pclSelect, "ONBL/OFBL");
			}
			else if (ilSelect == 2)
			{
				olPartial.WhereClause.Format("WHERE ((TIFA BETWEEN '%%s' AND '%%s' AND DES3='%s' AND LAND BETWEEN '%s' AND '%s') OR  (TIFD BETWEEN '%%s' AND '%%s' AND ORG3='%s' AND AIRB BETWEEN '%s' AND '%s'))",
												 pcgHome, oMinStr, oMaxStr, pcgHome , oMinStr, oMaxStr);
				strcpy (pclSelect, "LAND/AIRB");
			}
			else 
			{
				strcpy (pclSelect, "STA/STD");
			}


			CString olWherePlus;
			olWherePlus.Format("%s %s", olOrgDes, olFtyp );
			olPartial.WhereClause += olWherePlusActTime + olWherePlus;
			olPartial.StartTime = DBStringToDateTime(oMinStr);
			olPartial.EndTime  = DBStringToDateTime(oMaxStr);
			olPartial.omData = &olData.omData;
 			olPartial.Fields = "RKEY,SKEY,SEAS,FLNO,TTYP,ACT3,DES3,STOA,STOD,ORG3,ORG4,DES3,DES4,FLNO,URNO,PAXI,PAXT,ONBL,OFBL,LAND,AIRB";
			olPartial.Case = 0;
			
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

			bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
			HideStatus();
			if( ! blReadRet )
			{
				//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
				MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
			}
			else	
			{
				char pclInfo[100];
				oMinStr = olMinDate.Format( "%d.%m.%Y" );
				oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
				sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
				olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

				CReportTableDlg olReportTableDlg(this, 12, &(olData.omData), pclInfo, pclSelect);
				olReportTableDlg.DoModal();
			}

			HideStatus();
		}
	}

	// Inventory (A/C On Ground)
	else if (m_CR_List14.GetCheck() == 1)
	{
		TRACE("CReportSelectDlg::StartTableDlg: List14\n");

		CCSCedaData* olCCSData = new CCSCedaData;
		char pclFieldList[524];
		char pclWhere[10240]; 
		char pclFields[256];

		sprintf(pclWhere, "WHERE STAT = 'O'");
		strcpy(pclFieldList, "URNO,STAT,FTYP,FLNU");

		CCSPtrArray<RecordSet> pomData;
		if (!olCCSData->CedaAction("RT","FOG",pclFieldList,pclWhere,"","","BUF1",false,0,&pomData,4))
		{
			CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_NO_FOGTAB), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
			return false;
		}

		CString olFlnus;
		for(int i = 0; i < pomData.GetSize(); i++)
		{
//			if (i > 20)
//				break;
			RecordSet rlRec = pomData.GetAt(i);

			if (rlRec.Values.GetSize() == 4)
			{
				CString olFLNU = rlRec.Values.GetAt(3);
				if(!olFLNU.IsEmpty())
				{
					if (olFlnus.IsEmpty())
						olFlnus += olFLNU;
					else
						olFlnus += "," + olFLNU; 
				}
			}
		}

		pomData.DeleteAll();
		delete olCCSData;

//		olFlnus = "178330812,178352456,178840074,178848105,178850194,178850199,178851081,178886975";

		if (olFlnus.IsEmpty())
		{
			MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING933), MB_OK);
			return false;
		}
		else
		{
			//PRF 8283
			if(strcmp(pcgHome, "LIS") == 0)
			{
			sprintf(pclWhere,"WHERE RKEY IN (%s) AND FTYP NOT IN ('N','X')",olFlnus);
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

			sprintf(pclFields,"URNO,REGN,ACT3,ACT5,PSTA,TIFA,ADID,ONBL,LAND,FLNO,CSGN,RKEY,FTYP");
			}
			else
			{
			sprintf(pclWhere,"WHERE URNO IN (%s)",olFlnus);
				sprintf(pclFields,"URNO,REGN,ACT3,ACT5,PSTA,TIFA,ADID,ONBL,LAND");
			}
			
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

			bool blOk = true;
			CStringArray olFlnuList;
			for(int i = SplitItemList(olFlnus, &olFlnuList, 250) - 1; i >= 0; i--)
			{
				//PRF 8283
				if(strcmp(pcgHome, "LIS") == 0)
				sprintf(pclWhere,"WHERE RKEY IN (%s) AND FTYP NOT IN ('N','X')",olFlnuList[i]);
				else
				    sprintf(pclWhere,"WHERE URNO IN (%s)",olFlnuList[i]);
				if(!olData.ReadSpecial(&olData.omData, pclWhere, pclFields, true, false) )
					blOk = false;
			}

			if(!blOk)
			{
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
				MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING933), MB_OK);
			}
			else	
			{
				char pclInfo[100];
				CString olUtcStr;

				CTime olCurrDate = CTime::GetCurrentTime();
				olUtcStr = olCurrDate.Format( "%d.%m.%Y" );
				sprintf(pclInfo, "%s", olUtcStr);
				//PRF 8283 Changed for displaying the Arrival with Towing and Circular flights.
				if(strcmp(pcgHome, "LIS") == 0)
				{
					int li_count = 0;
				long liRkey = 0;
					RotationDlgCedaFlightData olData5;
					olData.omData.Sort(CompareSkey);	
					for(int j =(olData.omData.GetSize()-1);j>=0;j--)
					{
						if((olData.omData[j].Onbl!= TIMENULL) && (liRkey != olData.omData[j].Rkey))
						{
							liRkey = olData.omData[j].Rkey;
							if((strcmp(olData.omData[j].Adid,"B")==0) && (strcmp(olData.omData[j].Ftyp,"T")==0))
							{
								if(j>0)
								{
									if(strcmp(CString(olData.omData[j].Rkey),CString(olData.omData[j-1].Rkey))==0)
 										olData.omData[j].Tifa = olData.omData[j-1].Tifa;
									else
										continue;
								}
							}
							olData5.omData.NewAt(li_count,olData.omData[j]);
							li_count++;
						}
					
					}
					CReportTableDlg olReportTableDlg(this, 15, &(olData5.omData), pclInfo);
					olReportTableDlg.DoModal();
			}
		}
/*
		CString olUtcStr;
		CTime olCurrDate = CTime::GetCurrentTime();
		char pclWhere[256];
		char pclFields[100];

		blOk = true;
		if(bgReportLocal)
			ogBasicData.LocalToUtc(olCurrDate);

		olUtcStr = olCurrDate.Format( "%Y%m%d%H%M%S" );

		// immer Ftyp operation
		olFtyp = comFtypO ;
		sprintf(pclWhere,
			"WHERE REGN<>' ' AND ((TIFA<='%s' AND DES3='%s' AND ONBL<>' ') OR (TIFD<='%s' AND ORG3='%s' AND OFBL<>' ')) AND RTYP<>'E' %s",
				olUtcStr, pcgHome, olUtcStr, pcgHome, olFtyp );

		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

		sprintf(pclFields,"URNO,REGN,ACT3,ACT5,PSTA,TIFA,ADID,ONBL");
		if(!olData.ReadSpecial(&olData.omData, pclWhere, pclFields, true, false) )
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
			MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING933), MB_OK);
		}
		else	
		{
			char pclInfo[100];
			olUtcStr = olCurrDate.Format( "%d.%m.%Y" );
			sprintf(pclInfo, "%s", olUtcStr);

			CReportTableDlg olReportTableDlg(this, 15, &(olData.omData), pclInfo);
			olReportTableDlg.DoModal();
		}
*/
	}
}
	else if (m_CR_List19.GetCheck() == 1) //	DAILY CheckIn-Counter-Plan
	{
		TRACE("CReportSelectDlg::StartTableDlg: List19\n");
		char olWhere[512+1] ;		// Selection : Vorsicht bei �nderungen, wird bzgl. Tagen ausgewertet!!
									//				s. imWherePos1 - 5 !
		int ilNoFlights = 0;
		bool blReadRet = false ;		 
		bool blSeason = false ;		// akt. (9/98) Counterreport immer Daily !		
		char pclErrbuf[256];

		// liefert WHERE Bedingung in olWhere zur�ck
		CDailyCcaPrintDlg *polPrintDlg = new CDailyCcaPrintDlg( this, olWhere );

		if (polPrintDlg->DoModal() == IDOK)
		{
 			
			// Auswahl entsprechend olWhere aus CeDa 
			if(_MIT_TRACE_) TRACE( "olWhere: [%s] \n", olWhere ) ;
			CString olFlugTag( olWhere ) ;

			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			omDailyCedaFlightData.ClearAll();

			// mit Statusanzeige 
			ShowStatus() ;

			if(!blSeason) 
			{
				m_PR_Read.StepIt() ;
				blReadRet=omDailyCedaFlightData.ReadFlights( olWhere ) ;
				m_PR_Read.StepIt() ;
			}
			else
			{
				// Portionsweise einlesen 
				// noch nicht f�r ReadSpecial() vorbereitet !
				blReadRet = false ;
				VERIFY( blReadRet ) ;
			}

			if( !blReadRet ) 
			{
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
				HideStatus() ;
				
				if( _MIT_TRACE_ )
				{
					sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", olWhere);
					AfxMessageBox(pclErrbuf);
				}
				MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING1461), MB_OK);
			}
			else	
			{
				int ilViewerNo = 19 ;	// Viewer19: Daily Counterplanung
				CString olFlugTag( olWhere ) ;
				int ilNoFlights = omDailyCedaFlightData.omData.GetSize() ;
				char olInfo1[50]; 
				char olInfo2[50];
				char olInfoList[50];	// kommagetrennt Liste mit WHERE Infos

				olFlugTag = olFlugTag.Mid( imWherePos2, 14 ) ;
				sprintf( olInfoList,"%s,", olFlugTag ) ;	// Startzeitpunkt
				olFlugTag = olWhere ;
				olFlugTag = olFlugTag.Mid( imWherePos1, 14 ) ;
				strcat( olInfoList, olFlugTag ) ;	// Endzeitpunkt
				CTime olTag = DBStringToDateTime( olFlugTag ) ;

				if( bgReportLocal )	ogBasicData.UtcToLocal( olTag ) ;
				
				// Wochentag bestimmen				
				m_ST_Progress.SetWindowText( GetString( IDS_STRING1418) ) ;
				m_ST_Progress.UpdateWindow() ;
				m_PR_Read.StepIt() ;

				int ilWDay = olTag.GetDayOfWeek() ;
				ilWDay = (ilWDay == 1) ? 7 : ilWDay - 1 ;	// Verkehrstage Mo=1, Di=2 .... So=7
				int ilWDStr = 1159 - 1 + ilWDay ;	// IDS_STRING1159 - 1165 = Wochentage
				
				if( ! blSeason )
				{
					if(_MIT_TRACE_) TRACE( "Anzahl Fl�ge [%d] am [%s]\n", ilNoFlights, olFlugTag ) ;
					sprintf( olInfo1,"%s, %s", GetString(ilWDStr),olTag.Format("%d.%m.%Y"));
				}
				else
				{
					// akt. immer daily !
					ASSERT( false ) ;
				}

 				strcat( olInfoList, ",0" );
				strcpy( olInfo2, " " ) ;
 
				HideStatus() ;
				
				if( omDailyCedaFlightData.omData.GetSize() > 0 )
				{
					// olInfoList: kommagetrennt Liste mit Daten zur Where-Clause: Terminal,Starttag,Endtag
					CReportTableDlg olReportTableDlg(this, ilViewerNo, NULL, olInfo2, olInfo1, olInfoList );
					olReportTableDlg.DoModal();
				}
				else
				{
					MessageBox( GetString( IDS_STRING966), GetString( IDS_STRING1461 ), MB_ICONWARNING ) ; 
				}

			}	// end if/else (  ReadFlights(...
		}

		delete polPrintDlg;
	}

	 // Daily Flight Log
	else if (m_CR_List20.GetCheck() == 1)
	{
		TRACE("CReportSelectDlg::StartTableDlg: List20\n");

 		CTime olDate;
		CTime olMinDate = omLastValues.GetMinTimeRange() ;
		CTime olMaxDate = omLastValues.GetMaxTimeRange() ;
		int ilSelect = omLastValues.GetIATA_ICAO() ;
		int ilSel = omLastValues.GetMovement() ;
		
		CString olStr1;
		olStr1 = GetString(REPORTS_RB_DailyFlightLog);

		CReportSeqDateTimeDlg olReportDlg(this, olStr1, &olMinDate, &olMaxDate, &ilSelect,&ilSel);
		if(olReportDlg.DoModal() == IDOK)
		{
			
			ShowStatus();
			m_PR_Read.StepIt() ;

			// R�ckgabewerte festhalten
			omLastValues.SetMinTimeRange( olMinDate ) ;
			omLastValues.SetMaxTimeRange( olMaxDate ) ;
			omLastValues.SetIATA_ICAO( ilSelect ) ;
			omLastValues.SetMovement( ilSel) ;
			blOk = true;

			CString olMinStr = olMinDate.Format( "%Y%m%d%H%M%S" );
			CString olMaxStr = olMaxDate.Format( "%Y%m%d%H%M%S" );
			MakeUtc( olMinStr, olMaxStr ) ;

			PARTIALREAD olPartial;
			olPartial.Preset();
			
			char pclSelect[256];
			CString olWherePlusActTime;
			if (ilSel == 1)
			{
				olPartial.WhereClause.Format("WHERE ((TIFA BETWEEN '%%s' AND '%%s' AND DES3='%s' AND ONBL BETWEEN '%s' AND '%s') OR  (TIFD BETWEEN '%%s' AND '%%s' AND ORG3='%s' AND OFBL BETWEEN '%s' AND '%s'))",
												 pcgHome, olMinStr, olMaxStr, pcgHome , olMinStr, olMaxStr);
				strcpy (pclSelect, "ONBL/OFBL");
			}
			else if (ilSel == 2)
			{
				olPartial.WhereClause.Format("WHERE ((TIFA BETWEEN '%%s' AND '%%s' AND DES3='%s' AND LAND BETWEEN '%s' AND '%s') OR  (TIFD BETWEEN '%%s' AND '%%s' AND ORG3='%s' AND AIRB BETWEEN '%s' AND '%s'))",
												 pcgHome, olMinStr, olMaxStr, pcgHome , olMinStr, olMaxStr);
				strcpy (pclSelect, "LAND/AIRB");
			}
			else 
			{
				strcpy (pclSelect, "STA/STD");
			}
			CString olWherePlus;
			olWherePlus.Format(" %s", comFtyp_SGT);
			//olPartial.WhereClause += olWherePlus;
			olPartial.WhereClause += olWherePlusActTime + olWherePlus;
 			olPartial.StartTime = DBStringToDateTime(olMinStr);
			olPartial.EndTime  = DBStringToDateTime(olMaxStr);
			olPartial.omData = &olData.omData;	
			olPartial.Fields = "ALC2,ALC3,ACT3,ACT5,AIRB,BLT1,DCD1,DCD2,DES3,DES4,DTD1,DTD2,ETAI,ETDI,FLNO,FTYP,GTA1,GTD1,LAND,OFBL,ONBL,ORG3,ORG4,PAXT,PSTA,PSTD,REGN,REM1,RKEY,SLOT,STOA,STOD,TIFA,TIFD,TMOA,TTYP,URNO,VIA3,VIA4,WRO1";
  			
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			// Read data
			bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
 			HideStatus() ;
			if( ! blReadRet )
			{
				//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
				MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
			}
			else	
			{
				char pclInfo[100];
				olMinStr = olMinDate.Format( "%a, %d.%m.%Y %H:%M" );
				olMaxStr = olMaxDate.Format( "%a, %d.%m.%Y %H:%M" );
				sprintf(pclInfo, "%s - %s", olMinStr, olMaxStr);
				olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

 				CReportTableDlg olReportTableDlg(this, 26, &(olData.omData), pclInfo, NULL, NULL, ilSelect);
				olReportTableDlg.DoModal();
			}
 			HideStatus() ;
		}
	}

	// Overnight Flight Report
	else if (m_CR_List21.GetCheck() == 1) 
	{
		OvernightTableDlg olDlg (this);
		olDlg.DoModal();

/*
		TRACE("CReportSelectDlg::StartTableDlg: List21\n");

		CTime olMinDate = omLastValues.GetMinTime() ;
		CTime olMaxDate = omLastValues.GetMaxTime() ;

		CString olStr1;
		olStr1 = GetString(REPORTS_RB_OvernightFlightReport);

		CReportSeqDlg olReportDlg(this, olStr1, &olMinDate, &olMaxDate);

		if(olReportDlg.DoModal() == IDOK)
		{
			ShowStatus() ;
			m_PR_Read.StepIt() ;

			// R�ckgabewerte festhalten
			omLastValues.SetMinTime( olMinDate ) ;
			omLastValues.SetMaxTime( olMaxDate ) ;

			blOk = true;

			CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
			CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
			MakeUtc( oMinStr, oMaxStr ) ;

			CString olX = " AND FTYP<>'X'";

			// Portionsweise einlesen
			// 2/99 hbe
			PARTIALREAD olPartial ;
			CString olWherePlus ;
			olPartial.Preset();
			olWherePlus.Format("WHERE (STOA BETWEEN '%%s' AND '%%s' AND DES3='%s' ) ",pcgHome);
			olWherePlus += olX;
			olPartial.WhereClause = olWherePlus;
			olPartial.StartTime = DBStringToDateTime(oMinStr);
			olPartial.EndTime  = DBStringToDateTime(oMaxStr);
			olPartial.omData = &olData.omData;
 			olPartial.Fields = "RKEY";
			
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

			bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
 			HideStatus() ;
			if( ! blReadRet )
			{
				//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
				MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
			}
			else	
			{
				CString olKeyList = "";
				CString olKey = "";
				for(int i = 0; i < olData.omData.GetSize(); i++)
				{
					if(olData.omData[i].Rkey > 0)
					{
						olKey.Format("%ld",olData.omData[i].Rkey);							
						olKeyList   += olKey + CString(",");
					}
				}
				blReadRet = false;
				if(!olKeyList.IsEmpty())
				{
					olKeyList = olKeyList.Left(olKeyList.GetLength() -1 );
					olPartial.WhereClause = "WHERE RKEY IN (" + olKeyList + ")";
					olPartial.Fields = "RKEY,SKEY,STEV,FLNO,TIFA,TIFD,ORG3,VIA3,ACT3,STOA,STOD,DES3,ALC2,ALC3,SEAS,GTA1,PSTA,TTYP,GTD1,PSTD,WRO1,URNO,VIAL,CKIT,CKIF";
					olPartial.omData = &olData2.omData ;


					blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
				}
				if( ! blReadRet )
				{
					//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
				}
				else	
				{
					char pclInfo[100];
					oMinStr = olMinDate.Format( "%d.%m.%Y" );
					oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
					sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
					olData2.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

					CReportTableDlg olReportTableDlg(this, 20, &(olData2.omData), pclInfo, "",NULL,0,&olData2);
					olReportTableDlg.DoModal();
				}

			}

			HideStatus() ;
		}
*/
	}

	// Load & Pax Report
	else if (m_CR_List24.GetCheck() == 1)
	{
		if(!bgReports)
		{		
		TRACE("CReportSelectDlg::StartTableDlg: List24\n");

 		CTime olDate;
		
		CTime olMinDate = omLastValues.GetTime() ;

		CString olStr1;
		olStr1 = GetString(REPORTS_RB_LoadandPaxReport);

		ReportSeq1 olReportDlg(this, olStr1, &olMinDate);

		if(olReportDlg.DoModal() == IDOK)
		{
			TRACE("CReportSelectDlg::IDOK\n");
			ShowStatus() ;
			m_PR_Read.StepIt() ;

			// R�ckgabewerte festhalten
			omLastValues.SetTime( olMinDate ) ;
						

			blOk = true;

			CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
			CString oMaxStr = olMinDate.Format( "%Y%m%d235959" );
			MakeUtc( oMinStr, oMaxStr ) ;
 			
 			// Abfrage, ob Arrival oder Departure
			int ilSelect = omLastValues.GetLoadPaxType() ;
			ReportArrDepSelctDlg olReportArrDepSelctDlg(this, ilSelect);
			if (olReportArrDepSelctDlg.DoModal() == IDOK)
			{
				imArrDep = olReportArrDepSelctDlg.m_ListArrDep;
				omLastValues.SetLoadPaxType(imArrDep) ;
			}
			else
			{
				return false;
			}

			// Portionsweise einlesen
			// 2/99 hbe
			PARTIALREAD olPartial;
			olPartial.Preset();
			CString olWherePlus;
			olWherePlus.Format(" %s", olFtyp);
			olPartial.WhereClause += olWherePlus;
			olPartial.StartTime = DBStringToDateTime(oMinStr);
			olPartial.EndTime  = DBStringToDateTime(oMaxStr);
			olPartial.omData = &olData.omData;

			olPartial.Fields = "RKEY,SKEY,SEAS,TTYP,ACT3,ACT5,STOA,STOD,ORG3,ORG4,DES3,DES4,FLNO,AIRB,LAND,ADID,ALC3,ALC2,FTYP,REGN,GTA1,GTD1,BLT1,PSTD,PSTA,CKIT,URNO,PAX1,PAX2,PAX3,PAXF,PAXI,PAXT,MAIL,CGOT,BAGN,BAGW";
			
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

			bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
 			HideStatus() ;
			if( ! blReadRet )
			{
				TRACE("CReportSelectDlg::No data found\n");
				//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
				MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
			}
			else	
			{
				char pclInfo[100];
				oMinStr = olMinDate.Format( "%d.%m.%Y" );
				sprintf(pclInfo, "%s", oMinStr);
				olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

 				CReportTableDlg olReportTableDlg(this, 23, &(olData.omData), pclInfo, "", NULL, imArrDep);
				olReportTableDlg.DoModal();
			}

			HideStatus() ;
		}
	}

	if(bgReports)
	{
		TRACE("CReportSelectDlg::StartTableDlg: List24\n");

 		CTime olDate;
		
		CTime olMinDate = omLastValues.GetMinTime() ;
		CTime olMaxDate = omLastValues.GetMaxTime() ;


		CString olStr1;
		olStr1 = GetString(REPORTS_RB_LoadandPaxReport);

		CReportSeqDlg olReportDlg(this, olStr1, &olMinDate, &olMaxDate);

		if(olReportDlg.DoModal() == IDOK)
		{
			TRACE("CReportSelectDlg::IDOK\n");
			ShowStatus() ;
			m_PR_Read.StepIt() ;

			// R�ckgabewerte festhalten
			omLastValues.SetTime( olMinDate ) ;
						

			blOk = true;

			CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
			CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
			MakeUtc( oMinStr, oMaxStr ) ;
 			
 			// Abfrage, ob Arrival oder Departure
			int ilSelect = omLastValues.GetLoadPaxType() ;
			ReportArrDepSelctDlg olReportArrDepSelctDlg(this, ilSelect);
			if (olReportArrDepSelctDlg.DoModal() == IDOK)
			{
				imArrDep = olReportArrDepSelctDlg.m_ListArrDep;
				omLastValues.SetLoadPaxType(imArrDep) ;
			}
			else
			{
				return false;
			}

			// Portionsweise einlesen
			// 2/99 hbe
			PARTIALREAD olPartial;
			olPartial.Preset();
			CString olWherePlus;
			olWherePlus.Format(" %s", olFtyp);
			olPartial.WhereClause += olWherePlus;
			olPartial.StartTime = DBStringToDateTime(oMinStr);
			olPartial.EndTime  = DBStringToDateTime(oMaxStr);
			olPartial.omData = &olData.omData;

			olPartial.Fields = "RKEY,SKEY,SEAS,TTYP,ACT3,ACT5,STOA,STOD,ORG3,ORG4,DES3,DES4,FLNO,AIRB,LAND,ADID,ALC3,ALC2,FTYP,REGN,GTA1,GTD1,BLT1,PSTD,PSTA,CKIT,URNO,PAX1,PAX2,PAX3,PAXF,PAXI,PAXT,MAIL,CGOT,BAGN,BAGW";
			
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

			bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
 			HideStatus() ;
			if( ! blReadRet )
			{
				TRACE("CReportSelectDlg::No data found\n");
				//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
				MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
			}
			else	
			{
				char pclInfo[100];
				oMinStr = olMinDate.Format( "%d.%m.%Y" );
				oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
				sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
				olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

 				CReportTableDlg olReportTableDlg(this, 23, &(olData.omData), pclInfo, "", NULL, imArrDep);
				olReportTableDlg.DoModal();
			}

			HideStatus() ;
		}
	}
}
		
		// Load & Pax Report (User)
	
	
	else if (m_CR_List26.GetCheck() == 1)
	{
	 if(!bgReports)
	 {
		TRACE("CReportSelectDlg::StartTableDlg: List26\n");

 		CTime olDate;
			
		CTime olMinDate = omLastValues.GetTime() ;
		
		CString olStr1;
		olStr1 = GetString(REPORTS_RB_LoadandPaxReportUser);
		ReportSeq1 olReportDlg(this, olStr1, &olMinDate);

		
		if(olReportDlg.DoModal() == IDOK)
		{
			TRACE("CReportSelectDlg::IDOK\n");
			ShowStatus() ;
			m_PR_Read.StepIt() ;

			// R�ckgabewerte festhalten
			omLastValues.SetTime( olMinDate ) ;
						
			blOk = true;

			CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
			CString oMaxStr = olMinDate.Format( "%Y%m%d235959" );
			
			
			MakeUtc( oMinStr, oMaxStr ) ;
 			
 			// Abfrage, ob Arrival oder Departure
			int ilSelect = omLastValues.GetLoadPaxType() ;
			ReportArrDepSelctDlg olReportArrDepSelctDlg(this, ilSelect);
			if (olReportArrDepSelctDlg.DoModal() == IDOK)
			{
				imArrDep = olReportArrDepSelctDlg.m_ListArrDep;
				omLastValues.SetLoadPaxType(imArrDep) ;
			}
			else
			{
				return false;
			}
		
			// Portionsweise einlesen
			// 2/99 hbe
			PARTIALREAD olPartial;
			olPartial.Preset();
			CString olWherePlus;
			olWherePlus.Format(" %s", olFtyp);
			olPartial.WhereClause += olWherePlus;
			olPartial.StartTime = DBStringToDateTime(oMinStr);
			olPartial.EndTime  = DBStringToDateTime(oMaxStr);
			olPartial.omData = &olData.omData;

			olPartial.Fields = "RKEY,SKEY,SEAS,TTYP,ACT3,ACT5,STOA,STOD,ORG3,ORG4,DES3,DES4,FLNO,AIRB,LAND,ADID,ALC3,ALC2,FTYP,REGN,GTA1,GTD1,BLT1,PSTD,PSTA,CKIT,URNO,PAX1,PAX2,PAX3,PAXF,PAXI,PAXT,MAIL,CGOT,BAGN,BAGW";
			
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

			bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
 			HideStatus() ;
			if( ! blReadRet )
			{
				TRACE("CReportSelectDlg::No data found\n");
				//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
				MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
			}
			else	
			{
				char pclInfo[100];
				oMinStr = olMinDate.Format( "%d.%m.%Y" );
				sprintf(pclInfo, "%s", oMinStr);
				olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

 				CReportTableDlg olReportTableDlg(this, 25, &(olData.omData), pclInfo, "", NULL, imArrDep);
				olReportTableDlg.DoModal();
			}

			HideStatus() ;
		}
	}

	
	if(bgReports)
	{		
	
		TRACE("CReportSelectDlg::StartTableDlg: List26\n");

 		CTime olDate;
		
			
		CTime olMinDate = omLastValues.GetMinTime() ;
		CTime olMaxDate = omLastValues.GetMaxTime() ;
		
		CString olStr1;
		olStr1 = GetString(REPORTS_RB_LoadandPaxReportUser);
		CReportSeqDlg olReportDlg(this, olStr1, &olMinDate, &olMaxDate);
		
		if(olReportDlg.DoModal() == IDOK)
		{
			TRACE("CReportSelectDlg::IDOK\n");
			ShowStatus() ;
			m_PR_Read.StepIt() ;

			// R�ckgabewerte festhalten
			//omLastValues.SetTime( olMinDate ) ;
			
			omLastValues.SetMinTime( olMinDate ) ;
			omLastValues.SetMaxTime( olMaxDate ) ;
			blOk = true;

			CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
			CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
			
			
			MakeUtc( oMinStr, oMaxStr ) ;
 			
 			// Abfrage, ob Arrival oder Departure
			int ilSelect = omLastValues.GetLoadPaxType() ;
			ReportArrDepSelctDlg olReportArrDepSelctDlg(this, ilSelect);
			if (olReportArrDepSelctDlg.DoModal() == IDOK)
			{
				imArrDep = olReportArrDepSelctDlg.m_ListArrDep;
				omLastValues.SetLoadPaxType(imArrDep) ;
			}
			else
			{
				return false;
			}
		
			// Portionsweise einlesen
			// 2/99 hbe
			PARTIALREAD olPartial;
			olPartial.Preset();
			CString olWherePlus;
			olWherePlus.Format(" %s", olFtyp);
			olPartial.WhereClause += olWherePlus;
			olPartial.StartTime = DBStringToDateTime(oMinStr);
			olPartial.EndTime  = DBStringToDateTime(oMaxStr);
			olPartial.omData = &olData.omData;

			olPartial.Fields = "RKEY,SKEY,SEAS,TTYP,ACT3,ACT5,STOA,STOD,ORG3,ORG4,DES3,DES4,FLNO,AIRB,LAND,ADID,ALC3,ALC2,FTYP,REGN,GTA1,GTD1,BLT1,PSTD,PSTA,CKIT,URNO,PAX1,PAX2,PAX3,PAXF,PAXI,PAXT,MAIL,CGOT,BAGN,BAGW";
			
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

			bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
 			HideStatus() ;
			if( ! blReadRet )
			{
				TRACE("CReportSelectDlg::No data found\n");
				//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
				MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
			}
			else	
			{
				char pclInfo[100];
				oMinStr = olMinDate.Format( "%d.%m.%Y" );
				oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
				sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
				olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

 				CReportTableDlg olReportTableDlg(this, 25, &(olData.omData), pclInfo, "", NULL, imArrDep);
				olReportTableDlg.DoModal();
			}

			HideStatus() ;
		}
	}
}
	
	//GPU Usage Report
	else if (m_CR_List25.GetCheck() == 1)
	{
		TRACE("CReportSelectDlg::StartTableDlg: List25\n");

 		CTime olDate;
		CTime olMinDate = omLastValues.GetMinTime() ;
		CTime olMaxDate = omLastValues.GetMaxTime() ;

		char pclSelect[128];

		strcpy(pclSelect, omLastValues.GetGpuAirline() );

		CString olStr1, olStr2;
		olStr1 = GetString(REPORTS_RB_GPUUsageReport);
		olStr2 = GetString(IDS_STRING1046);

		CReportSeqFieldDlg olReportDlg(this, olStr1, olStr2, &olMinDate, &olMaxDate, pclSelect, "A|#A|#A|#", 2, 3);

		if(olReportDlg.DoModal() == IDOK)
		{
			
			ShowStatus();
			m_PR_Read.StepIt() ;

			// R�ckgabewerte festhalten
			omLastValues.SetMinTime( olMinDate ) ;
			omLastValues.SetMaxTime( olMaxDate ) ;
			omLastValues.SetGpuAirline( pclSelect ) ;

			blOk = true;

			CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
			CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );
			MakeUtc( oMinStr, oMaxStr ) ;

			CString olAlc("") ;
			if(strlen(pclSelect) == 2)
			{
				olAlc.Format(" AND ALC2='%s'", pclSelect ) ;
			}
			else if(strlen(pclSelect) == 3)
			{
				olAlc.Format(" AND ALC3='%s'", pclSelect ) ;
			}
 			else if(strlen(pclSelect) != 0)
			{
				// Airlines nur 2Ltr oder 3Ltr-Code !
				MessageBox("2Ltr/3Ltr-Code only!", GetString(IDS_WARNING), MB_ICONWARNING );
				return false;
			}
 			PARTIALREAD olPartial ;
			olPartial.Preset() ;
			CString olWherePlus ;
			if (strlen(pclSelect) != 0) 
			{
				olWherePlus.Format("%s", olAlc);
				olPartial.WhereClause += olWherePlus;
			}
			olPartial.StartTime = DBStringToDateTime(oMinStr) ;
			olPartial.EndTime  = DBStringToDateTime(oMaxStr) ;
			olPartial.omData = &olData.omData ;	
			olPartial.Fields = "RKEY,ACT3,AIRB,ALC2,ALC3,FLNO,LAND,PSTA,PSTD,REGN,STOA,STOD,DES3,ORG3,URNO";
 			
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

			bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
 			HideStatus() ;
			if( ! blReadRet )
			{
				//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
				MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
			}
			else	
			{
				char pclInfo[100];
				oMinStr = olMinDate.Format( "%d.%m.%Y" );
				oMaxStr = olMaxDate.Format( "%d.%m.%Y" );
				sprintf(pclInfo, "%s - %s", oMinStr, oMaxStr);
				olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

 				CReportTableDlg olReportTableDlg(this, 24, &(olData.omData), pclInfo, pclSelect);
				olReportTableDlg.DoModal();
			}
 			HideStatus() ;
		}
	}

	// Delay Report
	else if (m_CR_List27.GetCheck() == 1)
	{
		TRACE("CReportSelectDlg::StartTableDlg: List27\n");

 		CTime olDate;
		CTime olMinDate = omLastValues.GetMinTimeRange() ;
		CTime olMaxDate = omLastValues.GetMaxTimeRange() ;
		CString olAirl = omLastValues.GetDelayAirline() ;
		CString olDelayCode = omLastValues.GetDelayCode() ;
		CString olDelayMinutes = omLastValues.GetDelayMinutes() ;

		CString olTableName = GetString(REPORTS_RB_DelayReport);
		// Ask for the time span to display
		ReportSeqField3DateTimeDlg olReportDlg(this, olTableName, &olMinDate, &olMaxDate,
			GetString(IDS_STRING1517), REPSEQFIELD3_AIRLINECODE, 
			GetString(IDS_STRING1610), REPSEQFIELD3_DELAYCODE, 
			GetString(IDS_STRING1983), REPSEQFIELD3_DELAYTIME,
			olAirl, olDelayCode, olDelayMinutes);

		if(olReportDlg.DoModal() == IDOK)
		{
			ShowStatus();
			m_PR_Read.StepIt() ;

			// R�ckgabewerte festhalten
			omLastValues.SetMinTimeRange( olMinDate ) ;
			omLastValues.SetMaxTimeRange( olMaxDate ) ;
			omLastValues.SetDelayAirline(olReportDlg.m_Field1) ;
			omLastValues.SetDelayCode(olReportDlg.m_Field2) ;
			omLastValues.SetDelayMinutes(olReportDlg.m_Field3) ;

			blOk = true;

			CString olMinStr = olMinDate.Format( "%Y%m%d%H%M%S" );
			CString olMaxStr = olMaxDate.Format( "%Y%m%d%H%M%S" );
			MakeUtc( olMinStr, olMaxStr ) ;

			PARTIALREAD olPartial;
			olPartial.Preset();
						
			CString olWherePlus;
			// specify ftyp
			olWherePlus.Format(" %s", comFtyp_SGT);
			olPartial.WhereClause += olWherePlus;
			
			// specify selected airline if present
			if (!olReportDlg.m_Field1.IsEmpty())
			{
				olWherePlus.Format(" AND (ALC2 = '%s')", olReportDlg.m_Field1);
				olPartial.WhereClause += olWherePlus;
			}
			// specify selected delay code if present
			if (!olReportDlg.m_Field2.IsEmpty())
			{
				olWherePlus.Format(" AND (DCD1 = '%s' OR DCD2 = '%s')", olReportDlg.m_Field2, olReportDlg.m_Field2);
				olPartial.WhereClause += olWherePlus;
			}
			// specify selected delay time if present
			if (!olReportDlg.m_Field3.IsEmpty())
			{
//					olWherePlus.Format(" AND (DTD1 >= '%s' OR DTD2 >= '%s')", olReportDlg.m_Field3, olReportDlg.m_Field3);
//					olPartial.WhereClause += olWherePlus;
			}
			// specify selected time interval
 			olPartial.StartTime = DBStringToDateTime(olMinStr);
			olPartial.EndTime  = DBStringToDateTime(olMaxStr);
			olPartial.omData = &olData.omData;	
			olPartial.Fields = "ACT3,ACT5,DCD1,DCD2,DES3,DTD1,DTD2,FLNO,GTA1,GTD1,GTA2,GTD2,OFBL,ONBL,ORG3,PSTA,PSTD,REGN,TTYP,STOA,STOD,URNO";
  			
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			// Read data
			bool blReadRet = ReadPartialWhere( &olPartial, &m_PR_Read ) ;
 			HideStatus() ;
			if( ! blReadRet )
			{
				//sprintf(pclErrbuf,"\nKein Suchergebnis bei <%s>.\n", pclWhere);AfxMessageBox(pclErrbuf);
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
				MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING920), MB_OK);
			}
			else	
			{
				char pclInfo[100];
				olMinStr = olMinDate.Format( "%a, %d.%m.%Y %H:%M" ); 
				olMaxStr = olMaxDate.Format( "%a, %d.%m.%Y %H:%M" );
				sprintf(pclInfo, "%s - %s", olMinStr, olMaxStr);
				olData.omData.Sort(CompareSkey);	// verkn�pfte Fl�ge werden zusammengestellt

				int ipField3 = 1;
				if (!olReportDlg.m_Field3.IsEmpty())
					ipField3 = atoi(olReportDlg.m_Field3);

				char opField1[128];
				char opField2[128];
				sprintf(opField1, olReportDlg.m_Field1);
				sprintf(opField2, olReportDlg.m_Field2);


 				CReportTableDlg olReportTableDlg(this, 27, &(olData.omData), pclInfo, opField1, opField2, ipField3);
				olReportTableDlg.DoModal();
			}
 			HideStatus() ;
		}
	}
	// Change for Reason Report 
	else if (m_CR_List28.GetCheck() == 1)
	{
		TRACE("CReportSelectDlg::StartTableDlg: List28\n");

 		CTime olDate;
		CTime olMinDate = omLastValues.GetMinTimeRange() ;
		CTime olMaxDate = omLastValues.GetMaxTimeRange() ;	
		
		CReportReasonForChange olReportDlg(this,GetString(IDS_STRING2819),&olMinDate, &olMaxDate);

		if(olReportDlg.DoModal() == IDOK)
		{
			ShowStatus();
			m_PR_Read.StepIt() ;
			
			omLastValues.SetMinTimeRange( olMinDate ) ;
			omLastValues.SetMaxTimeRange( olMaxDate ) ;		

			blOk = true;

			CString olMinStr = olMinDate.Format( "%Y%m%d%H%M%S" );
			CString olMaxStr = olMaxDate.Format( "%Y%m%d%H%M%S" );
			MakeUtc( olMinStr, olMaxStr ) ;

			
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			// Read data
			bool blReadRet = olReportDlg.ReadReasonForChange(&m_PR_Read,olMinStr,olMaxStr);
 			HideStatus() ;
			if(!blReadRet )
			{				
				SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
				MessageBox(GetString(IDS_STRING919), GetString(IDS_STRING2821), MB_OK);
			}
			else	
			{
				char pclInfo[100];
				olMinStr = olMinDate.Format( "%a, %d.%m.%Y %H:%M" ); 
				olMaxStr = olMaxDate.Format( "%a, %d.%m.%Y %H:%M" );
				sprintf(pclInfo, "%s - %s", olMinStr, olMaxStr);
				
				if(olReportDlg.m_Sort1 == 1)
				{
					olReportDlg.omReasonForChangeData.Sort(CompareReasonCdat);
				}
				else if(olReportDlg.m_Sort2 == 1)
				{
					olReportDlg.omReasonForChangeData.Sort(CompareReasonData);
				}

				CReportTableDlg olReportTableDlg(this, 28);
				olReportTableDlg.SetReasonData(&(olReportDlg.omReasonForChangeData),pclInfo);
				olReportTableDlg.DoModal();
			}
 			HideStatus() ;
		}
	}	
	else
	{
 		MessageBox( GetString( IDS_STRING1947 ), GetString( IDS_STRING1948 ), MB_ICONWARNING ) ;
	}


	return true;
}



/////////////////////////////////////////////////////////////////////////////
// Portionsweise CeDa Datens�tze einlesen

bool CReportSelectDlg::ReadPartialWhere( PARTIALREAD *prpPartial, CProgressCtrl *prpProgress /* = NULL */ )
{
	
	if( prpPartial->Case < 0 || prpPartial->WhereClause.IsEmpty() ||
		prpPartial->StartTime == TIMENULL || prpPartial->EndTime == TIMENULL )
	{
		// MessageBox("Arguments (struct) incomplete!", "ReadPartialWhere", MB_OK|MB_ICONERROR) ;
		return false ;
	}

	int ilReadFunction = 0 ;	// Lesefunktion entsprechend Reportart festlegen
	switch( prpPartial->Case )
	{
		case 0:		// Sammler ( Standard )
		case 1:		// ABfl�ge BGS
		case 5:		// Messestatistiken
		case 6:		//     "
		case 7:		//     "
		case 9:		// Flugh�fen mit Freq.
			ilReadFunction = 1 ;	// Fl�ge mit ReadSpecial()
			break ;

		case 14:	// POPS Season
			ilReadFunction = 2 ;	// Fl�ge mit ReadDailyFlights() 
			break ;

		default:
			ilReadFunction = 0 ;
	}


	// Zeitraum schrittweise einlesen
	// von Starttag
	COleDateTime olNext( prpPartial->StartTime.GetYear(), prpPartial->StartTime.GetMonth(), prpPartial->StartTime.GetDay(),
						 prpPartial->StartTime.GetHour(), prpPartial->StartTime.GetMinute(), prpPartial->StartTime.GetSecond()) ;
	// bis Entag ( einschl. )
	COleDateTime olEnd( prpPartial->EndTime.GetYear(), prpPartial->EndTime.GetMonth(), prpPartial->EndTime.GetDay(),
						 prpPartial->EndTime.GetHour(), prpPartial->EndTime.GetMinute(), prpPartial->EndTime.GetSecond()) ;
	COleDateTimeSpan olPart( prpPartial->Range,0,0,0 ) ;	// Schrittweite in Tagen
	COleDateTimeSpan olGap( 0,0,0,1 ) ;						// 1 sec L�cke

	bool blRet = true ;
	int ilLoop = 0 ;
	CString olStartTag ;
	CString olEndTag ;
	CString olBetween ;

	while( olNext+olPart < olEnd ) 
	{
		if( prpProgress != NULL ) prpProgress->StepIt() ;
		olStartTag = olNext.Format("%Y%m%d%H%M%S") ; 	
		olEndTag = (olNext+olPart-olGap).Format("%Y%m%d%H%M%S") ; 	
		// TRACE("Part[%d] von [%s] bis [%s]\n", ++ilLoop, olStartTag, olEndTag ) ;
		olBetween.Format( prpPartial->WhereClause,  olStartTag, olEndTag, olStartTag, olEndTag ) ; 
		//TRACE("[%s]\n", olBetween ) ;
		switch( ilReadFunction )
		{
			case 1:		
				if( prpPartial->omData == NULL || prpPartial->Fields.IsEmpty() )
				{
					// MessageBox("Arguments (struct) incomplete!", "ReadPartialWhere",MB_OK|MB_ICONERROR) ;
					blRet = false ;
				}
				else
				{
					omDailyCedaFlightData.ReadSpecial( (CCSPtrArray<ROTATIONDLGFLIGHTDATA> *) prpPartial->omData,
						(LPCTSTR) olBetween, (LPCTSTR) prpPartial->Fields, prpPartial->onlyAFT, prpPartial->Sort, prpPartial->SortArt );
				}
				break ;

			case 2:	
				omDailyCedaFlightData.ReadDailyFlights( (LPCTSTR) olBetween )  ;
				break ;

			default:
				blRet = false ;
		}
		olNext += olPart ;
	}
	if( prpProgress != NULL ) prpProgress->StepIt() ;

	if( blRet )	// restliche Tage einlesen
	{
		olStartTag = olNext.Format("%Y%m%d%H%M%S") ; 	
		olEndTag = olEnd.Format("%Y%m%d%H%M%S") ; 	
		// TRACE("Rest[%d] von [%s] bis [%s]\n", ++ilLoop, olStartTag, olEndTag ) ;
		olBetween.Format( prpPartial->WhereClause,  olStartTag, olEndTag, olStartTag, olEndTag ) ; 
		//TRACE("[%s]\n", olBetween ) ;
		switch( ilReadFunction )
		{
			case 1:	
				omDailyCedaFlightData.ReadSpecial( (CCSPtrArray<ROTATIONDLGFLIGHTDATA> *) prpPartial->omData,
						(LPCTSTR) olBetween, (LPCTSTR) prpPartial->Fields, prpPartial->onlyAFT, prpPartial->Sort, prpPartial->SortArt );
				if( ((CCSPtrArray<ROTATIONDLGFLIGHTDATA> *) prpPartial->omData)->GetSize() < 1 )
				{
					blRet = false ;
				}
				break ;

			case 2:	
				omDailyCedaFlightData.ReadDailyFlights( (LPCTSTR) olBetween ) ;
				if( omDailyCedaFlightData.omData.GetSize() < 1 )
				{
					blRet = false ;
				}
				break ;

			default:
				blRet = false ;
		}
		
		// TRACE( "omDaily... [%d] - olData [%d]\n", omDailyCedaFlightData.omData.GetSize(), ((CCSPtrArray<ROTATIONDLGFLIGHTDATA> *) prpPartial->omData)->GetSize() ) ;
	}

	return( blRet ) ;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

void CReportSelectDlg::SaveToReg()
{
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
}


void CReportSelectDlg::OnDestroy() 
{
	SaveToReg();
	CDialog::OnDestroy();
}

void CReportSelectDlg::OnSize(UINT nType, int cx, int cy)  
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	m_resizeHelper.OnSize();
	this->Invalidate();
}

BOOL CReportSelectDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_key = "DialogPosition\\ReportsSelection";

	m_PR_Read.SetRange( 0, imProgRange ) ;	
	m_PR_Read.SetStep( 1 ) ;	
	m_PR_Read.SetPos( 0 ) ;	
	m_PR_Read.ShowWindow( SW_HIDE ) ;	// no Progress
	m_ST_Progress.ShowWindow( SW_HIDE ) ;	

	//not used and no code inside for
	m_CR_List7.ShowWindow(SW_HIDE);
	m_CR_List8.ShowWindow(SW_HIDE);
	m_CR_List9.ShowWindow(SW_HIDE);
	m_CR_List10.ShowWindow(SW_HIDE);
	m_CR_List11.ShowWindow(SW_HIDE);
	m_CR_List12.ShowWindow(SW_HIDE);
	m_CR_List15.ShowWindow(SW_HIDE);
	m_CR_List18.ShowWindow(SW_HIDE);
	m_CR_List16.ShowWindow(SW_HIDE);
	m_CR_List19.ShowWindow(SW_HIDE);
	m_CR_List22.ShowWindow(SW_HIDE);
	m_CR_List23.ShowWindow(SW_HIDE);
	m_CR_List26.ShowWindow(SW_HIDE);
	m_CR_List28.ShowWindow(SW_HIDE); 


	ogPrivList.SetSecStat(m_CR_List1, "REPORTS_RB_FlightsByAirline");
	ogPrivList.SetSecStat(m_CR_List3, "REPORTS_RB_FlightsByRegistration");
	ogPrivList.SetSecStat(m_CR_List5, "REPORTS_RB_FlightsByACType");
	ogPrivList.SetSecStat(m_CR_List6, "REPORTS_RB_FlightsByNatureCode");
	ogPrivList.SetSecStat(m_CR_List2, "REPORTS_RB_FlightsByOrigin");
	ogPrivList.SetSecStat(m_CR_List4, "REPORTS_RB_FlightsByDestination");
	ogPrivList.SetSecStat(m_CR_List19, "REPORTS_RB_CheckinReport");

	if(ogPrivList.GetStat("REPORTS_RB_CheckinReport") != '-')
		m_CR_List19.ShowWindow(SW_SHOW);

	ogPrivList.SetSecStat(m_CR_List20, "REPORTS_RB_DailyFlightLog");
	ogPrivList.SetSecStat(m_CR_List13, "REPORTS_RB_AircraftMovementsPerHour");
	ogPrivList.SetSecStat(m_CR_List14, "REPORTS_RB_Invertory");
	ogPrivList.SetSecStat(m_CR_List21, "REPORTS_RB_OvernightFlightReport");
	ogPrivList.SetSecStat(m_CR_List24, "REPORTS_RB_LoadandPaxReport");
	ogPrivList.SetSecStat(m_CR_List25, "REPORTS_RB_GPUUsageReport");
	ogPrivList.SetSecStat(m_CR_List27, "REPORTS_RB_DelayReport");
	ogPrivList.SetSecStat(m_CR_List28, "REPORTS_RB_ReasonsForChanges"); 
	if(ogPrivList.GetStat("REPORTS_RB_ReasonsForChanges") != '-')
	{
		m_CR_List28.ShowWindow(SW_SHOW);
	}	
	
	if(!bgReasonFlag)
	{
		m_CR_List28.ShowWindow(SW_HIDE);
	}	

	if (strcmp(pcgHome, "DXB") == 0)
	{
		m_CR_List26.ShowWindow(SW_SHOW);
		ogPrivList.SetSecStat(m_CR_List26, "REPORTS_RB_LoadandPaxReportUser");
	}

	m_CE_UtcToLocal.SetCheck( TRUE ) ;
	
	m_resizeHelper.Init(this->m_hWnd);

	CRect olRect;
	this->GetWindowRect(&olRect);
	GetDialogFromReg(olRect, m_key);
	SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_HIDEWINDOW);// | SWP_NOMOVE );
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CReportSelectDlg::OnCancel() 
{
	// TODO: Zus�tzlichen Bereinigungscode hier einf�gen
	
	ShowWindow(SW_HIDE);
	CWnd* olpCwnd = pogButtonList->GetDlgItem(IDC_BEENDEN);
	if (olpCwnd)
		olpCwnd->EnableWindow(TRUE);

}



// -----   lokale Funktionen
static int CompareToAlc2(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2)
{

	if((strlen((**e1).Alc2) == 0 ) && (strlen((**e2).Alc2) == 0) )
		return (int)(strcmp((**e1).Alc3,(**e2).Alc3));
	
	else if((strlen((**e1).Alc2) == 0 ) && (strlen((**e2).Alc2) != 0) )
		return (int)(strcmp((**e1).Alc3,(**e2).Alc2));

	else if((strlen((**e1).Alc2) != 0 ) && (strlen((**e2).Alc2) == 0) )
		return (int)(strcmp((**e1).Alc2,(**e2).Alc3));

	else	//if((strlen((**e1).Alc2) != 0 ) && (strlen((**e2).Alc2) != 0) )
		return (int)(strcmp((**e1).Alc2,(**e2).Alc2));

	/*
	if(strcmp((**e1).Alc2,(**e2).Alc2) == 0 )
		return (int)(strcmp((**e1).Alc3,(**e2).Alc3));
	else
		return (int)(strcmp((**e1).Alc2,(**e2).Alc2));
	*/

}

static int CompareToAlc3(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2)
{
	return (int)(strcmp((**e1).Alc3,(**e2).Alc3));
}

static int CompareToDes3(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2)
{
	return (int)(strcmp((**e1).Des3,(**e2).Des3));
}

static int CompareToAdid(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2)
{
	return (int)(strcmp((**e1).Adid,(**e2).Adid));
}

static int CompareToTtypAlc(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2)
{
	if(strcmp((**e1).Ttyp,(**e2).Ttyp) == 0 )
		return CompareToAlc2(e1,e2);
	else
		return (int)(strcmp((**e1).Ttyp,(**e2).Ttyp));
}

static int CompareToDes3Alc(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2)
{
	if(strcmp((**e1).Des3,(**e2).Des3) == 0 )
		return CompareToAlc2(e1,e2);
	else
		return (int)(strcmp((**e1).Des3,(**e2).Des3));
}

static int CompareToAlcDes3(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2)
{
	if(strcmp((**e1).Alc2,(**e2).Alc2) == 0 )
		return CompareToDes3(e1,e2);
	else
		return (int)(strcmp((**e1).Alc2,(**e2).Alc2));
}


static int CompareToTtyp(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2)
{
		return (int)(strcmp((**e1).Ttyp,(**e2).Ttyp));
}

static int CompareToAlc2Des3(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2)
{
	int ilRet;

	if((strlen((**e1).Alc2) == 0) && (strlen((**e2).Alc2) == 0))
	{
		ilRet = (int)(strcmp((**e1).Alc3,(**e2).Alc3));
	}
	else
	{
		if(strlen((**e1).Alc2) == 0)
			ilRet = (int)(strcmp((**e1).Alc3,(**e2).Alc2));
	
		if(strlen((**e2).Alc2) == 0)
			ilRet = (int)(strcmp((**e1).Alc2,(**e2).Alc3));
	}

	if((strlen((**e1).Alc2) > 0) && (strlen((**e2).Alc2) > 0))
		ilRet = (int)(strcmp((**e1).Alc2,(**e2).Alc2));



	if(ilRet == 0)
	{
		ilRet = CompareToDes3(e1,e2);
	}
	return ilRet;
}



static int CompareRotation(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2)
{
	CTime olTime1;
	CTime olTime2;

	if((strcmp((**e1).Des3, pcgHome) == 0) && (strcmp((**e1).Org3, pcgHome) != 0))
		return -1;

	if((strcmp((**e1).Des3, pcgHome) != 0) && (strcmp((**e1).Org3, pcgHome) == 0))
		return 1;

	if((**e1).Tifa == TIMENULL)
		olTime1 = (**e1).Tifd;
	else
		olTime1 = (**e1).Tifa;

	if((**e2).Tifa == TIMENULL)
		olTime2 = (**e2).Tifd;
	else
		olTime2 = (**e2).Tifa;

	return ((olTime1 == olTime2) ? 0 : ((olTime1 > olTime2) ? 1 : -1));
}


static int CompareChrono(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2)
{
	// vgl. im Zeitlauf: Ankunft - Abflug 
	// hbe am 15/9/98 z.B. f�r Messestatistik !
	CTime olTime1;
	CTime olTime2;

	if( (strcmp((**e1).Adid, "A") == 0)) olTime1 = (**e1).Tifa ;
	else if((strcmp((**e1).Adid, "D") == 0)) olTime1 = (**e1).Tifd ;
	else
	{
		if( (strcmp((**e1).Adid, "B") == 0))
		{
			if((**e1).Tifa != TIMENULL)	olTime1 = (**e1).Tifa;
			else olTime1 = (**e1).Tifd;
		}
	}

	if( (strcmp((**e2).Adid, "A") == 0)) olTime2 = (**e2).Tifa ;
	else if( (strcmp((**e2).Adid, "D") == 0)) olTime2 = (**e2).Tifd ;
	else
	{
		if((strcmp((**e2).Adid, "B") == 0))
		{
			if((**e2).Tifa != TIMENULL)	olTime2 = (**e2).Tifa;
			else olTime2 = (**e2).Tifd;
		}
	}

	
	
	return ((olTime1 == olTime2) ? 0 : ((olTime1 > olTime2) ? 1 : -1));
}


static int CompareSkey(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2)
{

	if((**e1).Rkey == (**e2).Rkey)
	{
		return CompareRotation(e1,e2);
	}
	else
	{
		return ((**e1).Rkey >  (**e2).Rkey)? 1: -1;
	}

}
// PRF 8379
static int CompareReasonData(const REASON_FOR_CHANGE **e1, const REASON_FOR_CHANGE **e2)
{
	if(strcmp((**e1).Flno,(**e2).Flno) == 0)
	{
		return CompareReasonAloc(e1,e2);
	}
	else
	{
		return ((strcmp((**e1).Flno,(**e2).Flno)) > 0)? 1: -1;
	}
}

static int CompareReasonAloc(const REASON_FOR_CHANGE **e1, const REASON_FOR_CHANGE **e2)
{
	if(strcmp((**e1).Aloc,(**e2).Aloc) == 0)
	{
		return CompareReasonCdat(e1,e2);
	}
	else
	{
		return ((strcmp((**e1).Aloc,(**e2).Aloc)) > 0)? 1: -1;
	}

}

static int CompareReasonCdat(const REASON_FOR_CHANGE **e1, const REASON_FOR_CHANGE **e2)
{
	return ((**e1).Cdat >  (**e2).Cdat)? 1: -1;
}
