
#if !defined(AFX_LISTSTRINGARRAYDLG_H__825A1800_F5AF_4E74_88C9_BCC1A1415B57__INCLUDED_)
#define AFX_CListStringArrayDlg_H__825A1800_F5AF_4E74_88C9_BCC1A1415B57__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CListStringArrayDlg.h : header file
//
#include "resrc1.h"
#include "AwBasicDataDlg.h"
/////////////////////////////////////////////////////////////////////////////
class CListStringArrayDlg : public CDialog
{
// Construction
public:
	~CListStringArrayDlg(){}
	CListStringArrayDlg(CWnd* pParent,CString opCaption = "", CStringArray *olStrArr = NULL ,CString olSelStr = "",bool blHideCancel=false);
	CString GetField();
	CString omCaption;
private:
// Dialog Data
	//{{AFX_DATA(CListStringArrayDlg)
	enum { IDD = IDD_AWBASICDATA };
	CButton	m_CB_OK;
	CButton	m_CB_Cancel;
	CStatic	m_CS_Header;
	CListBox	m_CL_List;
	//}}AFX_DATA
	
	CStringArray *omStrArray;
	CString omFieldStr;
	CString omSelStr; 
	bool bmHideCancel;
// Overrides
	//{{AFX_VIRTUAL(CListStringArrayDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CListStringArrayDlg)
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LISTSTRINGARRAYDLG_H__825A1800_F5AF_4E74_88C9_BCC1A1415B57__INCLUDED_)
