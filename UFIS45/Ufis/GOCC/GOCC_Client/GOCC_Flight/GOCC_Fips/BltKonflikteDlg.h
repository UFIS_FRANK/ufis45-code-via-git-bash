#if !defined(AFX_BltKonflikteDlg_H__BBB97141_04EA_11D2_8564_0000C04D916B__INCLUDED_)
#define AFX_BltKonflikteDlg_H__BBB97141_04EA_11D2_8564_0000C04D916B__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// BltKonflikteDlg.h : header file
//


#include <CCSTable.h>
#include <Konflikte.h>
#include <resrc1.h>
#include <CCSPrint.h>

struct BLTKONF_LINEDATA
{
	KonfIdent KonfId;
	CTime TimeOfConflict;
	CString Text;
};



/////////////////////////////////////////////////////////////////////////////
// BltKonflikteDlg dialog

class BltKonflikteDlg : public CDialog
{
// Construction
public:
	BltKonflikteDlg(CWnd* pParent = NULL);   // standard constructor

	~BltKonflikteDlg();
	void SaveToReg();
 
// Dialog Data
	//{{AFX_DATA(BltKonflikteDlg)
	enum { IDD = IDD_BLTATTENTION };
	CCSButtonCtrl	m_CB_AllConfirm;
	CCSButtonCtrl	m_CB_Confirm;
	CButton m_CB_Cancel;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(BltKonflikteDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	//}}AFX_VIRTUAL

public:

	void Attention(int opUsedFor = CONF_BLT);
	void UpdateDisplay();
	void ChangeViewTo();

	void DdxChangeKonf(const KonfIdent &rrpIdNotify);
	void DdxInsertKonf(const KonfIdent &rrpIdNotify);
	void DdxDeleteKonf(const KonfIdent &rrpIdNotify);

private:
	bool bmIsCreated;
	void OnSize(UINT nType, int cx, int cy);
	int omUsedFor;

	bool PrintTableLine(BLTKONF_LINEDATA *prpLine,bool bpLastLine); // BWi
	bool PrintTableHeader();
	bool CreateExcelFile(CString opTrenner);
	CCSPrint* pomPrint;
	CString omFileName;
	CString m_key; 

// Implementation
protected:

	void MakeLines(); 

	void DeleteAll();

	int CreateLine(BLTKONF_LINEDATA &rrpKonf);

	int CompareKonf(const BLTKONF_LINEDATA &rrpKonf1, const BLTKONF_LINEDATA &rrpKonf2) const;

	int FindLine(const KonfIdent &rrpKonfId) const;

	void InitTable(); 
	CCSTable *pomTable;

	CCSPtrArray<BLTKONF_LINEDATA> omLines;

	bool bmAttention;
	bool bmIsActiv;

	const int imDlgBarHeight;

	// Generated message map functions
	//{{AFX_MSG(BltKonflikteDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnConfirm();
	afx_msg void OnAllconfirm();
	virtual void OnCancel();
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	afx_msg void OnTimer(UINT nIDEvent);
    afx_msg LONG OnTableReturnPressed(UINT wParam, LONG lParam);
	afx_msg LONG OnTableSelChanged( UINT wParam, LPARAM lParam);
	afx_msg void OnPrint();
	afx_msg void OnExcel();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BltKonflikteDlg_H__BBB97141_04EA_11D2_8564_0000C04D916B__INCLUDED_)
