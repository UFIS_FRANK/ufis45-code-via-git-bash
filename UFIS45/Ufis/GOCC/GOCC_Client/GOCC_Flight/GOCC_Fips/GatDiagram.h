#if !defined(GATDIAGRAM_H__INCLUDED_)
#define GATDIAGRAM_H__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// GatDiagram.h : header file
//
 
#include <GatDiaViewer.h>
#include <CCSClientWnd.h>
#include <CCS3dStatic.h>
#include <CCSButtonCtrl.h>
#include <CCSTimeScale.h>
#include <CViewer.h>


/////////////////////////////////////////////////////////////////////////////
// GatDiagram frame

enum 
{
	GAT_ZEITRAUM_PAGE,
	GAT_FLUGSUCHEN_PAGE,
	GAT_GEOMETRIE_PAGE
};
 
class GatDiagram : public CFrameWnd
{
	DECLARE_DYNCREATE(GatDiagram)
public:
	GatDiagram();           // protected constructor used by dynamic creation 
	~GatDiagram();

	CDialogBar omDialogBar;

	void RedisplayAll();
	void SetTimeBand(CTime opStartTime, CTime opEndTime);
	void UpdateTimeBand();
 	void SetMarkTime(CTime opStartTime, CTime opEndTime);
	bool ShowFlight(long lpUrno, char cpFPart);
	bool ShowFlight(DIAFLIGHTDATA *prpAFlight, DIAFLIGHTDATA *prpDFlight);
	void GetCurView( char * pcpView);
	void ViewSelChange(char *pcpView);
	void UpdateWoResButton();
	void UpdateChangesButton(CString& opText, COLORREF opColor);

	void ActivateTimer();
	void DeActivateTimer();

	bool bmRepaintAll;
    CTime omStartTime;
    CTimeSpan omDuration;
    CTime omTSStartTime;
    CTimeSpan omTSDuration;
    CTimeSpan omTSInterval;
    

	CCSButtonCtrl m_CB_Offline;
	CCSButtonCtrl m_CB_WoRes;
	CCSButtonCtrl m_CB_Changes;
	bool GetLoadedFlights(CCSPtrArray<DIAFLIGHTDATA> &opFlights);
	void SaveToReg();
	void ShowTime(const CTime &ropTime);
	void SetFocusToDiagram(); //PRF 8363
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GatDiagram)
	virtual BOOL DestroyWindow();
	//}}AFX_VIRTUAL

private:

	bool UpdateDia();
	void ResetStateVariables();

    void PositionChild();
    void SetTSStartTime(CTime opTSStartTime);
	void UpdateComboBox();
	void ChangeViewTo(const char *pcpViewName,bool RememberPositions = true);
	void PrePlanMode(BOOL bpToSet,CTime opPrePlanTime);
	void ToggleOnline(void);
	bool LoadFlights(const char *pspView = NULL);

	GatDiagramViewer omViewer;
	GatChart *pomChart;

	CString omOldWhere;

    CCS3DStatic omTime;
    CCS3DStatic omDate;
    CCS3DStatic omTSDate;

    CButton m_KlebeFkt;
    CCSTimeScale omTimeScale;
    
    CCSClientWnd omClientWnd;
    CStatusBar omStatusBar;
    
    int imStartTimeScalePos;
    
	CString omCaptionText;
	bool bmIsViewOpen;


	BOOL bmNoUpdatesNow;

	CTime omTimeBandStartTime;
	CTime omTimeBandEndTime;

    CPoint omMaxTrackSize;
    CPoint omMinTrackSize;
    COLORREF lmBkColor;
    COLORREF lmTextColor;
    COLORREF lmHilightColor;

	BltOverviewTableDlg *pomBltOverviewTableDlg;
	CString m_key;

 
// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(GatDiagram)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnWoRes();
	afx_msg void OnBeenden();
	afx_msg void OnAllocate();
	afx_msg void OnOffline();
	afx_msg void OnDestroy();
	afx_msg void OnAnsicht();
	afx_msg void OnInsert();
	afx_msg void OnClose();
    afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
    afx_msg void OnPaint();
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
    afx_msg void OnTimer(UINT nIDEvent);
    afx_msg LONG OnPositionChild(WPARAM wParam, LPARAM lParam);
    afx_msg LONG OnUpdateDiagram(WPARAM wParam, LPARAM lParam);
	afx_msg void OnViewSelChange();
	afx_msg void OnCloseupView();
	afx_msg void OnZeit();
	afx_msg void OnSearch();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg LONG RepaintAll(WPARAM wParam, LPARAM lParam);
	afx_msg void OnPrint();
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	afx_msg void OnExpand();
	afx_msg void OnChanges();
	afx_msg void OnOverview();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.


#endif // !defined(GATDIAGRAM_H__INCLUDED_)
