// InfoDlg.cpp : implementation file
//

#include <stdafx.h>
//#include <BDPSUIF.h>
#include <InfoDlg.h>
#include <CCSCedadata.h>
#include <CCSCedacom.h>
#include <BasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// InfoDlg dialog
//-----------------------------------------------------------------------------------------

InfoDlg::InfoDlg(CWnd* pParent /*=NULL*/) : CDialog(InfoDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(InfoDlg)
	//}}AFX_DATA_INIT
}


void InfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(InfoDlg)
	DDX_Control(pDX, IDC_USERNAME,		m_Username);
	DDX_Control(pDX, IDC_SERVER,		m_Server);
	DDX_Control(pDX, IDC_E_COPYRIGHT1,	m_COPYRIGHT1);
	DDX_Control(pDX, IDC_E_COPYRIGHT2,	m_COPYRIGHT2);
	DDX_Control(pDX, IDC_E_UFIS1,		m_UFIS1);
	DDX_Control(pDX, IDC_E_UFIS2,		m_UFIS2);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(InfoDlg, CDialog)
	//{{AFX_MSG_MAP(InfoDlg)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// InfoDlg message handlers
//-----------------------------------------------------------------------------------------

BOOL InfoDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_Username.SetWindowText(ogBasicData.omUserID); 

	CString olServerText="";
	olServerText  += ogCommHandler.pcmRealHostName;
	olServerText  += " / ";
	olServerText  += ogCommHandler.pcmRealHostType;

	m_Server.SetWindowText(olServerText);

	m_COPYRIGHT1.SetBKColor(::GetSysColor(COLOR_BTNFACE));
	m_COPYRIGHT2.SetBKColor(::GetSysColor(COLOR_BTNFACE));
	m_UFIS1.SetBKColor(::GetSysColor(COLOR_BTNFACE));
	m_UFIS2.SetBKColor(::GetSysColor(COLOR_BTNFACE));


	CString olUfisVersion = CString(pcgVersion).Left(3);

	CString olUfis1;
	olUfis1.Format(GetString(IDS_INFO1), olUfisVersion);
	m_UFIS1.SetInitText(olUfis1);

	CString olUfis2;
	olUfis2.Format(GetString(IDS_INFO2), ogAppName, CString(" "), CString(pcgVersion));
	m_UFIS2.SetInitText(olUfis2);

//	m_UFIS1.SetInitText(GetString(IDS_INFO1));
//	m_UFIS2.SetInitText(GetString(IDS_INFO2));

	m_COPYRIGHT1.SetInitText(GetString(IDS_COPYRIGHT1));
	m_COPYRIGHT2.SetInitText(GetString(IDS_COPYRIGHT2));

	return TRUE;  
}

//-----------------------------------------------------------------------------------------

void InfoDlg::OnPaint() 
{
	CPaintDC dc(this);
}

//-----------------------------------------------------------------------------------------
