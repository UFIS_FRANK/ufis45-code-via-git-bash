#if !defined(AFX_GATPOSCONFLICTSETUPDLG_H__AC48A1C3_CC64_11D6_8217_00010215BFDE__INCLUDED_)
#define AFX_GATPOSCONFLICTSETUPDLG_H__AC48A1C3_CC64_11D6_8217_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GatPosConflictSetupDlg.h : header file
//
#include <CedaCfgData.h>
#include <GridFenster.h>

/////////////////////////////////////////////////////////////////////////////
// GatPosConflictSetupDlg dialog

class CGridFenster;

class GatPosConflictSetupDlg : public CDialog
{
// Construction
public:
	GatPosConflictSetupDlg(CWnd* pParent = NULL);   // standard constructor
	~GatPosConflictSetupDlg();
	void	SaveConflictSetup();

// Dialog Data
	//{{AFX_DATA(GatPosConflictSetupDlg)
	enum { IDD = IDD_GATPOS_SETUP };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GatPosConflictSetupDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(GatPosConflictSetupDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnPaint();
	afx_msg	LONG OnGridMessageCellClick(WPARAM wParam,LPARAM lParam);
	afx_msg	LONG OnGridMessageCellDblClick(WPARAM wParam,LPARAM lParam);
	afx_msg	LONG OnGridMessageEndEditing(WPARAM wParam,LPARAM lParam);
	afx_msg void OnLoadDefault();
	afx_msg void OnSaveDefault();
	afx_msg void OnReset();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	CCSPtrArray<CONFLICTDATA>	omConflicts;
	CGridFenster*				pomConflictSetup;

	char m_cSecState_ConflictColor ;		// 050506 MVy: conflict priority color, security state

	void	UpdateGrid();
	void GatPosConflictSetupDlg::ToggleRow(CELLPOS *prpPos);


private:
	BYTE lmCharSet;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GATPOSCONFLICTSETUPDLG_H__AC48A1C3_CC64_11D6_8217_00010215BFDE__INCLUDED_)
