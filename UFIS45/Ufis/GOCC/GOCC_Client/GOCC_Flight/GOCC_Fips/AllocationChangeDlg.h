#if !defined(AFX_ALLOCATIONCHANGEDLG_H__1E8945F3_C151_11D6_8216_00010215BFDE__INCLUDED_)
#define AFX_ALLOCATIONCHANGEDLG_H__1E8945F3_C151_11D6_8216_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AllocationChangeDlg.h : header file
//

#include <AatBitmapComboBox.h>
#include <CCSEdit.h>
#include <CedaCCAData.h>
#include <CCSButtonCtrl.h> 

/////////////////////////////////////////////////////////////////////////////
// AllocationChangeDlg dialog

struct DIAFLIGHTDATA;
class CGridFenster;

class AllocationChangeDlg : public CDialog
{
// Construction
public:
	AllocationChangeDlg(DIAFLIGHTDATA *popFlight,const CString& ropResType,const CString& ropTarget,CWnd* pParent = NULL);   // standard constructor
	~AllocationChangeDlg();

// Dialog Data
	//{{AFX_DATA(AllocationChangeDlg)
	enum { IDD = IDD_ALLOCATION_CHANGE };
	CButton	m_CheckBaggageBelt;
	CButton	m_CheckPosition;
	CButton	m_CheckLounge;
	CButton	m_CheckGate;
	CButton	m_CheckCiC_3;
	CButton	m_CheckCiC_2;
	CButton	m_CheckCiC_1;
	CButton	m_BitmapArr;
	CButton	m_BitmapDep;
	AatBitmapComboBox	m_ToPosition;
	AatBitmapComboBox	m_ToLounge;
	AatBitmapComboBox	m_ToGate;
	AatBitmapComboBox	m_ToCiC_3;
	AatBitmapComboBox	m_ToCiC_2;
	AatBitmapComboBox	m_ToCiC_1;
	AatBitmapComboBox	m_ToBaggageBelt;
	CCSEdit	m_FromPosition;
	CCSEdit	m_FromLounge;
	CCSEdit	m_FromGate;
	CCSEdit	m_FromCiC_3;
	CCSEdit	m_FromCiC_2;
	CCSEdit	m_FromCiC_1;
	CCSEdit	m_FromBaggageBelt;
	CButton	m_CheckEmpty;
	CButton	m_Recalc;
	CCSButtonCtrl m_PosReason;
	CCSButtonCtrl m_GatReason;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(AllocationChangeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(AllocationChangeDlg)
	afx_msg void OnCheckBaggageBelt();
	afx_msg void OnCheckCic1();
	afx_msg void OnCheckCic2();
	afx_msg void OnCheckCic3();
	afx_msg void OnCheckGate();
	afx_msg void OnCheckLounge();
	afx_msg void OnCheckPosition();
	afx_msg void OnSelendokToBaggageBelt();
	afx_msg void OnSelendokToCic1();
	afx_msg void OnSelendokToCic2();
	afx_msg void OnSelendokToCic3();
	afx_msg void OnSelendokToGate();
	afx_msg void OnSelendokToLounge();
	afx_msg void OnSelendokToPosition();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg	LONG OnGridMessageCellClick(WPARAM wParam,LPARAM lParam);
	afx_msg	LONG OnGridMessageEndEditing(WPARAM wParam,LPARAM lParam);
	afx_msg void OnCheckEmpty();
	afx_msg void OnRecalc();
	afx_msg void OnPosReason(); 
	afx_msg void OnGatReason(); 	
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
protected:
	DIAFLIGHTDATA*	pomFlight; 
	CString			omTarget;
	CString			omType;
	CedaCcaData		omCcaData;
	CedaCcaData		omDemandCcaData;
	CGridFenster*	pomCheckinCounter;
	bool bmInit;
private:
	void UpdatePositionList();
	void UpdateGateList();
	void UpdateBaggageBeltList();
	void UpdateLoungeList();
	void UpdateExitList();

	void UpdateGateListFromLounge();
	void UpdateGateListFromBelt();
	void UpdatePositionListFromGate();

	bool FillAatCombo (AatBitmapComboBox& ropAatCombo, CStringArray& ropArray, CMapStringToString& ropMap);
	void SetGateSelection(CString opDefault = "");
	void SetPositionSelection(CString opDefault = "");
	void SetLoungeSelection(CString opDefault = "");
	void SetBaggageBeltSelection(CString opDefault = "");

	void SetPosition();
	void SetBaggageBelt();
	void SetGate();
	void SetLounge();
	void InitTarget();

	void ReadCcaData(DIAFLIGHTDATA*	prpFlight);
	void UpdateCheckinCounterGrid();
	void UpdateCheckinCounterGrid1();

	bool GetCcaRes(CCADATA* prpCca, CMapStringToString& ropMapBLUE, CMapStringToString& ropMapRED);
	void CreateChoiceList(CCADATA* prpCca, CMapStringToString& ropMapBLUE, CMapStringToString& ropMapRED, CString& olChoiceList);
	void UpdateChoiceList(CString opCheck, int ipRow);
	void UpdateCkeckInCounter();
	bool GetCcaBitmap(int& ipBitmap, int& ipRow, CString& opSelected);
	bool EmptyLineAatCombo (AatBitmapComboBox& ropAatCombo);
	void SetOpenAllocationState();
	static int CompareCcas(const CCADATA **e1, const CCADATA **e2);
	bool m_bFirstRecalc;
	//Check the position change - PRF 8379
	void CheckReasonForChange(const CString& ropType); 
	struct REASONS
	{
		CString olPstUrno;
		CString olGatUrno;
		bool blPstResf;
		bool blGatResf;

		REASONS(void)
		{
			olPstUrno="";  olGatUrno="";
			blPstResf = false;	 blGatResf = false;
		} 
	}omReasons;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ALLOCATIONCHANGEDLG_H__1E8945F3_C151_11D6_8216_00010215BFDE__INCLUDED_)
