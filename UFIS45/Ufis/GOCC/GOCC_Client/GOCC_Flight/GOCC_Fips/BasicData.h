// basicdat.h CCSBasicData class for providing general used methods

#ifndef _BASICDATA
#define _BASICDATA

#include <InitialLoadDlg.h>
#include <ccsglobl.h>
#include <ccsptrarray.h>
#include <ccscedaData.h>
#include <CcsLog.h>
#include <CedaCfgData.h>
#include <CCSTime.h>
#include <CCSDdx.h>
#include <CCSCedaCom.h>
#include <CCSBcHandle.h>
#include <CCSMessageBox.h>

#define MAXNVBRUSHES 64

int GetItemCount(CString olList, char cpTrenner  = ',' );
int ExtractItemList(CString opSubString, CStringArray *popStrArray, char cpTrenner = ',');

CString GetListItem(CString &opList, int ipPos, bool bpCut, char cpTrenner  = ',');

bool GetListItemByField(CString &opDataList, CString &opFieldList, CString &opField, CString &opData);
bool GetListItemByField(CString &opDataList, CString &opFieldList, CString &opField, CTime &opData);


CString DeleteListItem(CString &opList, CString olItem);

int SplitItemList(CString opString, CStringArray *popStrArray, int ipMaxItem = 10, char cpTrenner = ',');

void TraceFieldAndData(CString &opFieldList,CString &opListOfData, CString opMessage);

// String rechts auff�llen
int FillRightByChar( CString &opStr, char cpSign, int ipLength ) ; 
int FillLeftByChar( CString &opStr, char cpSign, int ipLength ) ; 

// Linken Anteil von String besorgen   
int CutLeft(CString & ropLeft, CString & ropLeftover, const char * OneOfSep) ;
int CutLeft(CString & ropLeft, CString & ropLeftover, int ipNo) ;

// Zeitraumangabe in Kurzform aufbereiten (POPS) 
const int cimFullRangeLen = 21 ;	//	L�nge eines Range Strings "dd.mm.yyyy-dd.mm.yyyy"
void ShortRangeString( CString &ropStr ) ;

BOOL LoadBitmapFromBMPFile( LPTSTR szFileName, HBITMAP *phBitmap,HPALETTE *phPalette );

CString ExtractFirstToken( CString& source, CString delimiter = ";" );	// 050303 MVy: get the first string from a delimited source string, the source data are modified!
long ToLong( CString source );		// 050303 MVy: convert the string to long value, even if it is a sedecimal value written like 0x90ABCDEF
CString ToString( unsigned long value );		// 050318 MVy: helper function
CString GetFieldByUrno( CString sTable, unsigned long urno, CString sColumn );		// 050318 MVy: helper function for specific access to table field, attention: returns empty string even if there are no data in field, but asserts 
CString GetNameByUrno( CString sTable, unsigned long urno );		// 050318 MVy: helper function for specific access to table field, hint: assuming, the name column looks like tablename.first.character + "NAM", attention: returns empty string even if there are no data in field, but asserts 
unsigned long GetUrnoByName( CString sTable, CString sSearch );		// 050318 MVy: helper function for specific access to table field, hint: assuming, the name column looks like tablename.first.character + "NAM", attention: returns empty string even if there are no data in field, but asserts 
CString GetAbrvByUrno( CString sTable, unsigned long urno );
unsigned long GetUrnoByAbrv( CString sTable, CString sSearch );
unsigned long GetAirlineCodes( unsigned long urnoAgent, unsigned long urnoTask, CStringArray& strarrAirlineCodes );
CString GetAlcByUrno( unsigned long urnoAirline, CString *psValue1st = 0, CString *psValue2nd = 0 );		// 050324 MVy: helper function; return the ALC3 code and if not available the ALC2 code; fill in the variables if specified
bool IsStringIn( CStringArray& array, const char* search );		// 050323 MVy: return if array contains an identical string

#define INRANGE( MIN, VAL, MAX )\
	( (MIN) <= (VAL) && (VAL) <= (MAX) )

CTime ToTime( const CString &sText, const char* pcDefault = 0 );		// 050322 MVy: format like YYYYMMDDhhmmss or hhmm
CString ToFormat( long value, unsigned long size );		// 050322 MVy: convert a long to a string and fix the size of the result string
CString FromTime( CTime time, const char* format = "YYYYMMDDhhmmss" );		// 050322 MVy: convert a time into most common string format like YYYYMMDDhhmmss; TODO: format string is currently not used!
CString GetOCCValue( const RecordSet* rec, const char* col );		// 050322 MVy: data table access helper function
CString ExtractValue( CString &source, char* start, char terminator );		// 050331 MVy: find a string with begin and end and extract it from source; useful for configuration tupels like ITEM1=VALUE1;ITEM2=VALUE2
CTime OverwriteTime( CTime &datetime, unsigned char hour, unsigned char minute, unsigned char second );		// 050425 MVy: set time values of a datetime
CTime OverwriteTime( CTime &datetime, const CTime &newtime );		// 050425 MVy: overwrite the time hh:mm:ss of the first time by the values of the second time; useful for sth. like concatting an only date value with a only time value
CTime OverwriteDate( CTime &datetime, const CTime &newdate );		// 050425 MVy: overwrite the date dd.mm.yy of the first time by the values of the second time; useful for sth. like concatting an only date value with a only time value

/////////////////////////////////////////////////////////////////////////////
// class BasicData

class CBasicData : public CCSCedaData
{

public:

	CBasicData(void);
	~CBasicData(void);
	
	int imScreenResolutionX;
	int imScreenResolutionY;
	int imMonitorCount;
	long ConfirmTime;
	CString omUserID;

	long GetNextUrno(void);
	int imNextOrder;
	bool GetManyUrnos(int ipAnz, CUIntArray &opUrnos);


	int GetNextOrderNo();

	char *GetCedaCommand(CString opCmdType);
	void GetDiagramStartTime(CTime &opStart, CTime &opEnd);
	void SetDiagramStartTime(CTime opDiagramStartTime, CTime opEndTime);
	void SetWorkstationName(CString opWsName);
	CString GetWorkstationName();
	bool GetWindowPosition(CRect& rlPos,CString olMonitor);

	// Timeroutines

	void SetLocalDiff();

	CTimeSpan GetLocalDiff(CTime opDate)
	{
		if(opDate < omTich)
		{
			return omLocalDiff1;
		}
		else
		{
			return omLocalDiff2;
		}

		return omLocalDiff2;
	};


	CTimeSpan GetLocalDiff1() { return omLocalDiff1; };
	CTimeSpan GetLocalDiff2() { return omLocalDiff2; };
	CTime     GetTich() { return omTich; };


	void LocalToUtc(CTime &opTime);
	void UtcToLocal(CTime &opTime);

	// end time..

	int GetViaArray(CCSPtrArray<VIADATA> *opVias, CString opViaList);
	bool AddBlockingImages(CImageList& ropImageList);
	CBrush* GetBlockingBrushAt(int& ipIndex);
	bool	IsGatPosEnabled();
	bool	IsExtLmEnabled();
	bool	IsTerminalRestrictionForAirlinesAvailable();
	void LoadParameters();
	WORD AircraftTypeDescriptionLength();


public:
	static void TrimLeft(char *s);
	static void TrimRight(char *s);

	//Added a new function for getting all the connected gates for a Position , for the PRF 8494 by sisl
	int GetRelatedGatesForPosition(CString lpPosUrno,CStringArray& ropGates);
	
private:

	CUIntArray omNewUrnos;

	CTimeSpan omLocalDiff1;
	CTimeSpan omLocalDiff2;
	CTime	  omTich;

	char pcmCedaCmd[12];
	CTime omDiaStartTime;
	CTime omDiaEndTime;

	CString omDefaultComands;
	CString omActualComands;
	char pcmCedaComand[24];
	CString omWorkstationName; 
	CBrush *omNotValidBrushes[MAXNVBRUSHES];

};


#endif
