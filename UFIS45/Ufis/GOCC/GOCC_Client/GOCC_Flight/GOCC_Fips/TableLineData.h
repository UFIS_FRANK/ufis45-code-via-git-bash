// TableLineData.h: interface for the CTableLineData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TABLELINEDATA_H__828BEF4D_19D4_4F04_A7E0_3F61A6AEDA5B__INCLUDED_)
#define AFX_TABLELINEDATA_H__828BEF4D_19D4_4F04_A7E0_3F61A6AEDA5B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "CedaDiaCcaData.h"

// 050329 MVy: new class, same again in some listviews
class CTableLineData  
{
public:
	CTableLineData();
	virtual ~CTableLineData();

public:
	COLORREF clrBackground	;		// 050321 MVy: set line background color
	void HandleCheckinCounterRestrictionsForHandlingAgents( DIACCADATA *pCounter );		// 050329 MVy: do stuff for agents if possible

private:
	bool m_bEnabled ;		// 050321 MVy: color is grayed, user actions prohibited
public:		// wrapper functions for state atribute access
	void Enable();		// 050321 MVy: allow user actions
	void Disable();		// 050321 MVy: no user actions are allowed
	bool IsEnabled();		// 050321 MVy: get current state

};

#endif // !defined(AFX_TABLELINEDATA_H__828BEF4D_19D4_4F04_A7E0_3F61A6AEDA5B__INCLUDED_)
