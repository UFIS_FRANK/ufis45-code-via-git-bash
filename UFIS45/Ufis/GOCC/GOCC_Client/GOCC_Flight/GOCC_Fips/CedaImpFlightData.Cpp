// CedaImpFlightData.cpp
 
#include <stdafx.h>
#include <SeasonDlgCedaFlightData.h>
#include <CedaImpFlightData.h>
#include <AltImportDelDlg.h>
#include <ProgressDlg.h>
#include <CCSBcHandle.h>
#include <BasicData.h>
#include <CCSGlobl.h>
#include <CCSTime.h>
#include <CedaCcaData.h>
#include <CedaBasicData.h>


//--CedaImpFlightData-----------------------------------------------------------------------------------------------

CedaImpFlightData::CedaImpFlightData()
{
    BEGIN_CEDARECINFO(IMPFLIGHTDATA, CcaDataRecInfo)
		CCS_FIELD_CHAR_TRIM	(Stox,"STOX","An-Abflugzeit alternativ STOA oder STOD",1)
		CCS_FIELD_CHAR_TRIM	(Flno,"FLNO","komplette Flugnummer",1)
		CCS_FIELD_CHAR_TRIM	(ArDe,"ARDE","Arr - Dep kein DB Feld",1)
		CCS_FIELD_CHAR_TRIM	(Empt,"EMPT","Leeres Feld",1)
		CCS_FIELD_CHAR_TRIM	(Orde,"ORDE","Org-Dest",1)
		CCS_FIELD_CHAR_TRIM	(Ttyp,"TTYP","Verkehrsart",1)
		CCS_FIELD_CHAR_TRIM	(Flnj,"FLNJ","komplette Flugnummer            Partner",1)
		CCS_FIELD_CHAR_TRIM	(Stoj,"STOJ","Planm��ige Ankunfts- Abflugzeit Partner",1)
		CCS_FIELD_CHAR_TRIM	(Ardj,"ARDJ","komplette Flugnummer            Partner",1)
		CCS_FIELD_CHAR_TRIM	(Act5,"ACT5","Ausgangsflughafen               Partner",1)
		CCS_FIELD_CHAR_TRIM	(X001,"X001","Leeres Feld",1)
		CCS_FIELD_CHAR_TRIM	(X002,"X002","Leeres Feld",1)
		CCS_FIELD_CHAR_TRIM	(X003,"X003","Leeres Feld",1)
		CCS_FIELD_CHAR_TRIM	(X004,"X004","Leeres Feld",1)
		CCS_FIELD_CHAR_TRIM	(X005,"X005","Leeres Feld",1)
		CCS_FIELD_CHAR_TRIM	(X006,"X006","Leeres Feld",1)
		CCS_FIELD_CHAR_TRIM	(X007,"X007","Leeres Feld",1)
		CCS_FIELD_CHAR_TRIM	(X008,"X008","Leeres Feld",1)
		CCS_FIELD_CHAR_TRIM	(X009,"X009","Leeres Feld",1)
		CCS_FIELD_CHAR_TRIM	(X010,"X010","Leeres Feld",1)
		CCS_FIELD_CHAR_TRIM	(X011,"X011","Leeres Feld",1)
		CCS_FIELD_CHAR_TRIM	(X012,"X012","Leeres Feld",1)
		CCS_FIELD_CHAR_TRIM	(X013,"X013","Leeres Feld",1)
		CCS_FIELD_CHAR_TRIM	(X014,"X014","Leeres Feld",1)
		CCS_FIELD_CHAR_TRIM	(X015,"X015","Leeres Feld",1)
		CCS_FIELD_CHAR_TRIM	(X016,"X016","Leeres Feld",1)
		CCS_FIELD_CHAR_TRIM	(X017,"X017","Leeres Feld",1)
		CCS_FIELD_CHAR_TRIM	(X018,"X018","Leeres Feld",1)
		CCS_FIELD_CHAR_TRIM	(X019,"X019","Leeres Feld",1)
		CCS_FIELD_CHAR_TRIM	(X020,"X020","Leeres Feld",1)
		CCS_FIELD_CHAR_TRIM	(X021,"X021","Leeres Feld",1)
		CCS_FIELD_CHAR_TRIM	(Gate,"GATE","Gate Arr. oder Gate Dep.",1)
		CCS_FIELD_CHAR_TRIM	(Pstx,"PSTX","Postion Psta/Pstd",1)
		CCS_FIELD_CHAR_TRIM	(X023,"X023","Leeres Feld",1)
		CCS_FIELD_CHAR_TRIM	(Viax,"VIAX","Vial oder Vian",1)
		CCS_FIELD_CHAR_TRIM	(X024,"X024","Leeres Feld",1)
		CCS_FIELD_CHAR_TRIM	(X025,"X025","Leeres Feld",1)
		CCS_FIELD_CHAR_TRIM	(Cicf,"CICF","Leeres Feld",1)
		CCS_FIELD_CHAR_TRIM	(Twr1,"TWR1","Warteraum Terminal",1)
		CCS_FIELD_CHAR_TRIM	(X027,"X027","Leeres Feld",1)
		CCS_FIELD_CHAR_TRIM	(X028,"X028","Leeres Feld",1)
		CCS_FIELD_CHAR_TRIM	(X029,"X029","Leeres Feld",1)
		CCS_FIELD_CHAR_TRIM	(Sto2,"STO2","Planm��iges An-Abflugzsdatum",1)


  END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(CcaDataRecInfo)/sizeof(CcaDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&CcaDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	
	}
    // Initialize table names and field names
    strcpy(pcmTableName,"");
    sprintf(pcmListOfFields,
	"STOX,FLNO,ARDE,EMPT,ORDE,TTYP,FLNJ,STOJ,ARDJ,ACT5,X001,X002,X003,X004,X005,X006,X007,X008,X009,X010,X011,X012,X013,X014,X015,X016,X017,X018,X019,X020,X021,GATE,PSTX,X023,VIAX,X024,X025,CICF,TWR1,X027,X028,X029,STO2");
	pcmFieldList = pcmListOfFields; //because pcmFieldList is just a pointer

	omFlights.RemoveAll();

	polProgress = NULL;


}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaImpFlightData::Register(void)
{

}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaImpFlightData::~CedaImpFlightData(void)
{
	omRecInfo.DeleteAll();
	omDCins.DeleteAll();
	omDCinsSave.DeleteAll();
	ClearAll();
}


//--IMPORT-------- BWi--------------------------------------------------------------------------------------
// Fehler-Logfile verwalten
// Altdatendatei �ffnen und zeilenweise auslesen
// Je Zeile Feldtrenner '!' in ',' wandeln, Zeile in Stringliste schreiben und
// OneFlight aufrufen
// Anschlie�end f�r alle Fl�ge CreateFlights ausf�hren

bool CedaImpFlightData::Import(char *pcgPath)
{
	CStdioFile opFile;
	int ilLinecnt = 0;
	CString olString;
	CStdioFile opLogfile;
//	char pclDefPath[256] = "c:\\tmp\\bwi.log";
	char pclDefPath[256];
	strcpy(pclDefPath,CCSLog::GetTmpPath("\\bwi.log"));
	char pclSto2[256] = "971202";
	char pclSto2Time[256] = "971202000000";
	strcpy(pclDefPath, pcgPath);
	pclDefPath[(strlen(pclDefPath)-3)] = '\0';
	strcat(pclDefPath, "log");


	CStringArray *polDataBuff = GetDataBuff();

	// -- Datei auslesen f�r Bestimmung des Zeitintervalls
	if(!opFile.Open(pcgPath, CFile::modeRead))
	{
		MessageBox(NULL, GetString(IDS_STRING912), GetString(ST_FEHLER), MB_OK);
		return false;
	}


	/* --bool blRet = Stammdaten();
	if (!blRet)
	{
		MessageBox(NULL, "Stammdaten konnten nicht ausgewertet werden!", "Fehler", MB_OK);
		return false;
	} */

	char pcMin[7];
	char pcMax[7];
	int ilLc = 0;
	
 
	for (ilLc = 0; opFile.ReadString( olString ) != NULL; ilLc++)
	{
		IMPFLIGHTDATA *prlImp;
		prlImp = new IMPFLIGHTDATA;

		strcpy (prlImp->Line, olString);
		for(int i = olString.GetLength() - 1; i >= 0; i--)
		{
			if(olString[i] == ',')
			{
				olString.SetAt(i,' ');
			}
			if(olString[i] == '!')
			{
				olString.SetAt(i,',');
			}
		}
		ilLinecnt++;					//Zeilenz�hler f�r Bestimmung des ersten Eintrages (Init)

		polDataBuff->RemoveAll();
		polDataBuff->Add(olString);


		if ((GetBufferRecord(0,prlImp)) == true)
		{
			if (ilLinecnt ==1)
				//pcMin = pcMax = prlImp->Sto2;	// Vorbelegung
			{
				//	1. Logfile erzeugen
				strcpy(pclDefPath, pcgPath);
				pclDefPath[(strlen(pclDefPath) -4)] = '\0';
				strcat(pclDefPath, prlImp->Sto2);
				//pclDefPath[(strlen(pclDefPath) + strlen(prlImp->Sto2) -3)] = '\0';
				strcat(pclDefPath, ".log");

				if(!opLogfile.Open(pclDefPath, CFile::modeCreate | CFile::modeWrite))
				{
					MessageBox(NULL, GetString(IDS_STRING913), GetString(ST_FEHLER), MB_OK);
				}
				strcpy(pclSto2, prlImp->Sto2);

				// Vorbelegung der Vergleichsvariablen Min und Max Datum
				strcpy(pcMin, prlImp->Sto2);
				strcpy(pcMax, prlImp->Sto2);
			}

			else if (atoi(prlImp->Sto2) < atoi(pcMin))
				//pcMin = prlImp->Sto2;
				strcpy(pcMin, prlImp->Sto2);
			else if (atoi(prlImp->Sto2) > atoi(pcMax))
				//pcMax = prlImp->Sto2;
				strcpy(pcMax, prlImp->Sto2);
		}

		delete prlImp;
	
	}

	opFile.Close();
	// Ende Datei auslesen f�r Bestimmung des Zeitintervalls
	int ilRet = MinMaxDate(pcMin, pcMax, &opLogfile);

	
	//RST!
	//BWI!
	//return true;


  if (ilRet)	// ilRet == 1 oder 2
  {
	// -- Datei auslesen f�r Speicherung neuer Flugdatens�tze
	if(!opFile.Open(pcgPath, CFile::modeRead))
	{
		MessageBox(NULL, GetString(IDS_STRING912), GetString(ST_FEHLER), MB_OK);
		return false;
	}


	//Progress Bar erzeugen
	polProgress = new CProgressDlg;
	BOOL blRet = polProgress->Create(IDD_PROGRESS);

	if(blRet == FALSE)
	{
		MessageBox(NULL, GetString(IDS_STRING914), GetString(ST_FEHLER), MB_OK);
		return false;
	}
	polProgress->ShowWindow(SW_SHOWNORMAL);

	// in eingene Funktion
	polProgress->m_CP_Progress.SetRange(0, (ilLinecnt*7));
	polProgress->m_CP_Progress.SetPos(0);
	//polProgress->SetPosition(0);

	char pclMsg[256];

	strcpy(pclMsg,GetString(IDS_STRING1343));
	strcat(pclMsg, " ");
	strcpy(pclSto2Time,"19");
	strcat(pclSto2Time,pclSto2);
	strcat(pclSto2Time,"000000");
	CTime olDate = DBStringToDateTime(CString(pclSto2Time));
	CString olDateStr = olDate.Format( "%d.%m.%Y" );
	strcat(pclMsg, olDateStr);
	strcat(pclMsg, " ");
	strcat(pclMsg, GetString(IDS_STRING1342));
	polProgress->m_CE_Progresstext.SetWindowText(pclMsg);	
	polProgress->m_CE_Progresstext.UpdateWindow();
	
	//CStringArray *polDataBuff = GetDataBuff();
	for (ilLc = 0; opFile.ReadString( olString ) != NULL; ilLc++)
	{
		IMPFLIGHTDATA *prlImp;
		prlImp = new IMPFLIGHTDATA;

		strcpy (prlImp->Line, olString);
		for(int i = olString.GetLength() - 1; i >= 0; i--)
		{
			if(olString[i] == ',')
			{
				olString.SetAt(i,' ');
			}
			if(olString[i] == '!')
			{
				olString.SetAt(i,',');
			}
		}

		//ilLinecnt	= Summe Datens�tze
		//ilLc = Zeilenz�hler f�r Fehler-Logfile

		polDataBuff->RemoveAll();
		polDataBuff->Add(olString);


		if ((GetBufferRecord(0,prlImp)) == true)
		{
			//	weitere Logfiles erzeugen
			//  wenn sich das Datum Sto2 im Vergleich zum Vor-Datensatz ge�ndert hat:
			//  altes Logfile schlie�en, neues mit neuem Datum erzeugen
			if(strcmp(pclSto2 ,prlImp->Sto2))	// wenn ungleich - Dateiwechsel
			{
				CreateFlights(&opLogfile);	// ein kompletter Tag wird abgearbeitet

				strcpy(pclSto2 ,prlImp->Sto2);
				opLogfile.Close();
				strcpy(pclDefPath, pcgPath);
				pclDefPath[(strlen(pclDefPath) -4)] = '\0';
				strcat(pclDefPath, prlImp->Sto2);
				//pclDefPath[(strlen(pclDefPath) + strlen(prlImp->Sto2) -3)] = '\0';
				strcat(pclDefPath, ".log");
			
				if(!opLogfile.Open(pclDefPath, CFile::modeCreate | CFile::modeWrite))
				{
					MessageBox(NULL, GetString(IDS_STRING913), GetString(ST_FEHLER), MB_OK);
				}


				char pclMsg[256];
				strcpy(pclMsg,GetString(IDS_STRING1343));
				strcat(pclMsg, " ");
				strcpy(pclSto2Time,"19");
				strcat(pclSto2Time,pclSto2);
				strcat(pclSto2Time,"000000");
				CTime olDate = DBStringToDateTime(CString(pclSto2Time));
				CString olDateStr = olDate.Format( "%d.%m.%Y" );
				strcat(pclMsg, " ");
				strcat(pclMsg, olDateStr);
				strcat(pclMsg, GetString(IDS_STRING1342));
				polProgress->m_CE_Progresstext.SetWindowText(pclMsg);
				polProgress->m_CE_Progresstext.UpdateWindow();



				FLNOKEYLIST *prlFlno;
				POSITION pos;
				CString pString;
									// anschlie�end Speicher aufr�umen
				for( pos = omFlnoKeyMap.GetStartPosition(); pos != NULL; )
				{
					omFlnoKeyMap.GetNextAssoc( pos, pString , (void *&)prlFlno );
					prlFlno->JoinData.RemoveAll();
					delete prlFlno;
				}

				omFlnoKeyMap.RemoveAll();
				omFlights.RemoveAll();
				omFlights.DeleteAll();


			}
			
			// Progress Bar aktualisieren
 			polProgress->m_CP_Progress.OffsetPos(6);

			OneFlight(prlImp, ilLc, &opLogfile); 
		}

		delete prlImp;
	
	}
	CreateFlights(&opLogfile);	// ein kompletter Tag wird abgearbeitet

	opFile.Close();

	// Ende Datei auslesen f�r Speicherung neuer Flugdatens�tze
	


	// das letzte Logfile schlie�en
	opLogfile.Close();

	//TRACE("Anzahl Fl�ge: %d", omFlights.GetSize());
	//CreateFlights(); wird jetzt nach jedem Tag aufgerufen


  } // Ende if blRet (Neue Datens�tze erzeugen ja-nein)

	return true;
}





//-- Min Max ----------------------------------------------------------------------------------------------
int CedaImpFlightData::MinMaxDate(char *pcMin ,char * pcMax, CStdioFile *popLogfile)
// bei Ret-Code=false den Ladevorgang beenden
{
	CTime olMinDate;
	CTime olMaxDate;
	//char *pclDate;
	char pclDate[15];
	int ilRet = 2;


	strcpy(pclDate, "19");
	strcat(pclDate, pcMin);
	strcat(pclDate, "000000");
	olMinDate = DBStringToDateTime(CString(pclDate));


	strcpy(pclDate, "19");
	strcat(pclDate, pcMax);
	strcat(pclDate, "000000");
	olMaxDate = DBStringToDateTime(CString(pclDate));


	if ((olMinDate != -1) && (olMaxDate != -1))
	{

		CTime olTest = CTime::GetCurrentTime();
		if (olMinDate < CTime::GetCurrentTime())
		{
			MessageBox(NULL, GetString(IDS_STRING915), GetString(ST_FEHLER), MB_OK);
			return 0;
		}
		
		
		// Benutzer �ber L�schvorgang informieren und Korrektur des Zeitintervalls erm�gichen
		CAltImportDelDlg olAltImpDelDlg(NULL, &olMinDate, &olMaxDate, &ilRet);
		olAltImpDelDlg.DoModal();
			
	// TEST:	CString olDays("9241");
	//DaysinPeriod(olMinDate, olMaxDate, olDays);

		// Datens�tze oMinDate bis oMaxDate l�schen
		if(ilRet == 2)	
		{
			char pclWhere[128];
			char pclErrbuf[256];
			
			CString oMinStr = olMinDate.Format( "%Y%m%d000000" );
			CString oMaxStr = olMaxDate.Format( "%Y%m%d235959" );

			olMinDate = DBStringToDateTime(oMinStr);
			olMaxDate = DBStringToDateTime(oMaxStr);

			ogBasicData.LocalToUtc(olMinDate);
			ogBasicData.LocalToUtc(olMaxDate);


			oMinStr = olMinDate.Format( "%Y%m%d%H%M%S" );
			oMaxStr = olMaxDate.Format( "%Y%m%d%H%M%S" );


			sprintf(pclWhere,
				"WHERE (STOA>='%s' AND STOA<='%s') OR (STOD>='%s' AND STOD<='%s')",
				oMinStr, oMaxStr, oMinStr, oMaxStr);
			if(!ogSeasonDlgFlights.DeleteFlight(pclWhere))
			{
				sprintf(pclErrbuf,"\nFehler beim L�schen alter Datenbest�nde (%s).\n", pclWhere);
				popLogfile->WriteString( pclErrbuf );
			}  

		}
	}
	return ilRet;
}







//--ONE-FLIGHT----------------------------------------------------------------------------------------------
// Feldinhalte f�r einen Datensatz vom Importformat in das SEASONDLGFLIGHTDATA Format �bertragen
// Zuornung der Daten zu Feldern ist z. T. davon abh�ngig,ob es sich um Ankunfts- oder Ablugdaten
// handelt
// Feldinhalte werden z. T. gegen die Stammdaten validiert
// Im Fehlerfall erfolgt ein Logfile-Eintrag, die Daten werden verworfen,
// OK Daten werden gespeichert und in die Keymap eingef�gt.
void CedaImpFlightData::OneFlight(IMPFLIGHTDATA *prlImp, int ilLinecnt, CStdioFile *popLogfile)
{

	bool blOk = true;
	SEASONDLGFLIGHTDATA *prlFlight = new SEASONDLGFLIGHTDATA;
	char pclErrbuf[256];
	bool blRet;


	if (!strlen(prlImp->Flno))
	{
		if (blOk)
		{
			popLogfile->WriteString("\n");
			popLogfile->WriteString(prlImp->Line);
			popLogfile->WriteString("\n");
		}
		sprintf(pclErrbuf,GetString(IDS_STRING1344),ilLinecnt); // "Fehler in Zeile %d: Flugnummer fehlt.\n"
		popLogfile->WriteString( pclErrbuf );
		blOk = false;
	}
	else
	strcpy(prlFlight->Flno, prlImp->Flno);

	strcpy(prlFlight->Twr1, prlImp->Twr1);




	if (!strcmp(prlImp->ArDe, "A"))
	{
		//prlFlight->Stoa = DateHourStringToDate((CString)prlImp->Sto2,(CString)prlImp->Stox);
		// -- konventieren ins interne Datumsformat
		char pclDate[15];

		if ((!strlen(prlImp->Sto2)) || (!strlen(prlImp->Stox)))
		{
			if (blOk)
			{
				popLogfile->WriteString("\n");
				popLogfile->WriteString(prlImp->Line);
				popLogfile->WriteString("\n");
			}
			sprintf(pclErrbuf,GetString(IDS_STRING1345),ilLinecnt); // "Fehler in Zeile %d: An-Abflugzsdatum (-zeit) fehlt.\n"
			popLogfile->WriteString( pclErrbuf );
			blOk = false;
		}
		else
		{
			strcpy(pclDate, "19");
			strcat(pclDate, prlImp->Sto2);
			strcat(pclDate, prlImp->Stox);
			strcat(pclDate, "00");
			prlFlight->Stoa = DBStringToDateTime(CString(pclDate));

			if (prlFlight->Stoa == -1)
			{
				if (blOk)
				{
					popLogfile->WriteString("\n");
					popLogfile->WriteString(prlImp->Line);
					popLogfile->WriteString("\n");
				}
				sprintf(pclErrbuf,GetString(IDS_STRING1346),ilLinecnt);//"Fehler in Zeile %d: An-Abflugzsdatum (-zeit) ung�ltig.\n"
				popLogfile->WriteString( pclErrbuf );
				blOk = false;
			}
			else
				ogBasicData.LocalToUtc(prlFlight->Stoa);

			//CTimeSpan olSpan(365,0,0,0);
			//prlFlight->Stoa += olSpan;
		}

		strcpy(prlFlight->Gta1, prlImp->Gate);  //Gate Arr. oder Gate Dep.
		strcpy(prlFlight->Psta, prlImp->Pstx); 

		if (!strlen(prlImp->Orde))
		{
			if (blOk)
			{
				popLogfile->WriteString("\n");
				popLogfile->WriteString(prlImp->Line);
				popLogfile->WriteString("\n");
			}
			sprintf(pclErrbuf,GetString(IDS_STRING1347),ilLinecnt);//"Fehler in Zeile %d: Ankunftsflughafen fehlt.\n"
			popLogfile->WriteString( pclErrbuf );
			blOk = false;
		}
		else
		{
			strcpy(prlFlight->Org3, prlImp->Orde);
			strcpy(prlFlight->Des3, pcgHome);
		}
		
	}

	else	// also (!strcmp(prlImp->ArDe, "D"))
	{


		if ((!strlen(prlImp->Sto2)) || (!strlen(prlImp->Stox)))
		{
			if (blOk)
			{
				popLogfile->WriteString("\n");
				popLogfile->WriteString(prlImp->Line);
				popLogfile->WriteString("\n");
			}
			sprintf(pclErrbuf,GetString(IDS_STRING1345),ilLinecnt); //"Fehler in Zeile %d: An-Abflugzsdatum (-zeit) fehlt.\n"
			popLogfile->WriteString( pclErrbuf );
			blOk = false;
		}
		else
		{
			//prlFlight->Stod = DateHourStringToDate((CString)prlImp->Sto2,(CString)prlImp->Stox);
			// -- konventieren ins interne Datumsformat
			char pclDate[15];


			if ((!strlen(prlImp->Sto2)) || (!strlen(prlImp->Stox)))
			{
				if (blOk)
				{
					popLogfile->WriteString("\n");
					popLogfile->WriteString(prlImp->Line);
					popLogfile->WriteString("\n");
				}
				sprintf(pclErrbuf,GetString(IDS_STRING1345),ilLinecnt);//"Fehler in Zeile %d: An-Abflugzsdatum (-zeit) fehlt.\n"
				popLogfile->WriteString( pclErrbuf );
				blOk = false;
			}
			else
			{
				strcpy(pclDate, "19");
				strcat(pclDate, prlImp->Sto2);		//strcpy(pclDate, "19980809");
				strcat(pclDate, prlImp->Stox);
				strcat(pclDate, "00");
				prlFlight->Stod = DBStringToDateTime(CString(pclDate));
				if (prlFlight->Stod == -1)
				{
					if (blOk)
					{
						popLogfile->WriteString("\n");
						popLogfile->WriteString(prlImp->Line);
						popLogfile->WriteString("\n");
					}
					sprintf(pclErrbuf,GetString(IDS_STRING1346),ilLinecnt); //"Fehler in Zeile %d: An-Abflugzsdatum (-zeit) ung�ltig.\n"
					popLogfile->WriteString( pclErrbuf );
					blOk = false;
				}
				else
					ogBasicData.LocalToUtc(prlFlight->Stod);

				//CTimeSpan olSpan(365,0,0,0);
				//prlFlight->Stod += olSpan;
			}
		}

		if (!strlen(prlImp->Orde))
		{
			if (blOk)
			{
				popLogfile->WriteString("\n");
				popLogfile->WriteString(prlImp->Line);
				popLogfile->WriteString("\n");
			}
			sprintf(pclErrbuf,GetString(IDS_STRING1348),ilLinecnt);//"Fehler in Zeile %d: Zielflughafen fehlt.\n"
			popLogfile->WriteString( pclErrbuf );
			blOk = false;
		}
		else
		{
			strcpy(prlFlight->Des3, prlImp->Orde);
			strcpy(prlFlight->Org3, pcgHome);
		}

		strcpy(prlFlight->Gtd1, prlImp->Gate);  //Gate Arr. oder Gate Dep
		strcpy(prlFlight->Pstd, prlImp->Pstx); 

	}

	// sind PSTA oder PSTD mit blanc gef�llt, werden die Werte GTA1 oder GTD1 genommen

	if(strlen(prlFlight->Psta) == 0)
			strcpy(prlFlight->Psta, prlFlight->Gta1); 
	if(strlen(prlFlight->Pstd) == 0)
			strcpy(prlFlight->Pstd, prlFlight->Gtd1); 

	if(*(prlImp->Viax))
	{
		strcpy(prlFlight->Vian, "1");
		prlFlight->Vial[0] = ' ';
		prlFlight->Vial[1] = prlImp->Viax[0];
		prlFlight->Vial[2] = prlImp->Viax[1];
		prlFlight->Vial[3] = prlImp->Viax[2];
		for (int i = 4; i <= 119; i++)
			prlFlight->Vial[i] = ' ';
			//prlFlight->Vial[i] = '0';

	}
	else
		strcpy(prlFlight->Vian, "0"); 


		CString oTtyp(prlImp->Ttyp);
		oTtyp = oTtyp.Right(2);
		strcpy(prlFlight->Ttyp, oTtyp);

		strcpy(prlFlight->Act5, prlImp->Act5);

		strcpy(prlFlight->Ftyp, "O");


		if (strlen(prlImp->Flno))
		{
			CString olFlno, opAlc, opFltn, opFlns;
			bool blFlnoTest = true;
			CreateFlno(prlFlight->Flno, olFlno, opAlc, opFltn, opFlns, &blFlnoTest);
			if (blFlnoTest == false)
			{
				//	Logfile Eintrag
				if (blOk)
				{
					popLogfile->WriteString("\n");
					popLogfile->WriteString(prlImp->Line);
					popLogfile->WriteString("\n");
				}
				sprintf(pclErrbuf,GetString(IDS_STRING1349),ilLinecnt,prlFlight->Flno);// "Fehler in Zeile %d: Flugnummer '%s' ung�ltig.\n"
				popLogfile->WriteString( pclErrbuf );
				blOk = false;

			}
			else
			{
				strcpy(prlFlight->Flno, olFlno);
				strcpy(prlFlight->Fltn, opFltn);
				strcpy(prlFlight->Flns, opFlns);
			

				
//ALT			//sprintf(pclWhere,"WHERE ALC2='%s' OR ALC3='%s'", prlFlight->Alc3, prlFlight->Alc3);
				CString olAlc2;
				CString olAlc3;
				if(!(blRet = ogBCD.GetField("ALT", "ALC2", "ALC3", opAlc, olAlc2, olAlc3 )))
				{
					if (blOk)
					{
						//	Logfile Eintrag
						popLogfile->WriteString("\n");
						popLogfile->WriteString(prlImp->Line);
						popLogfile->WriteString("\n");
					}
					sprintf(pclErrbuf,GetString(IDS_STRING1350), ilLinecnt, opAlc);//"Fehler in Zeile %d: Fluggesellschaft '%s' nicht in den Stammdaten erfasst.\n"
					popLogfile->WriteString( pclErrbuf );
					blOk = false;
				}
				else
				{ 
					strcpy(prlFlight->Alc2, olAlc2);
					strcpy(prlFlight->Alc3, olAlc3);
				}
							
			}
		}

//APT
		if (strlen(prlImp->Orde))
		{
			CString olApc4;

			if(strcmp(prlFlight->Org3, pcgHome) == 0)
				//sprintf(pclWhere,"WHERE APC3='%s'", prlFlight->Des3);
				//ilRet = omApc3KeyMap.Lookup( prlFlight->Des3, (void *& )prlAptstruct);
				blRet = ogBCD.GetField("APT", "APC3", prlFlight->Des3, "APC4", olApc4 );
			else
				//sprintf(pclWhere,"WHERE APC3='%s'", prlFlight->Org3);
				//ilRet = omApc3KeyMap.Lookup( prlFlight->Org3, (void *& )prlAptstruct);
				blRet = ogBCD.GetField("APT", "APC3", prlFlight->Org3, "APC4", olApc4 );
			if(blRet == FALSE)
			{
			//	Logfile Eintrag
				if (blOk)
				{
					popLogfile->WriteString("\n");
					popLogfile->WriteString(prlImp->Line);
					popLogfile->WriteString("\n");
				}

				if(strcmp(prlFlight->Org3, pcgHome) == 0)
					sprintf(pclErrbuf,GetString(IDS_STRING1351),ilLinecnt,prlFlight->Des3);//"Fehler in Zeile %d: Bestimmungsflughafen '%s' nicht in den Stammdaten erfasst.\n"
				else
					sprintf(pclErrbuf,GetString(IDS_STRING1352),ilLinecnt,prlFlight->Org3);//"Fehler in Zeile %d: Ausgangsflughafen '%s' nicht in den Stammdaten erfasst.\n"

				popLogfile->WriteString( pclErrbuf );
				blOk = false;

			}
			else
			{
				if(strcmp(prlFlight->Org3, pcgHome) == 0)
					strcpy(prlFlight->Des4, olApc4);
				else
					strcpy(prlFlight->Org4, olApc4);
			}

		}


//VIAL

		if (strlen(prlImp->Viax))
		{
			CString olApc4;


			//blRet = omApc3KeyMap.Lookup( prlImp->Viax, (void *& )prlAptstruct);
			blRet = ogBCD.GetField("APT", "APC3", prlImp->Viax, "APC4", olApc4 );
			if(blRet == FALSE)
			{

			//	Logfile Eintrag
				if (blOk)
				{
					popLogfile->WriteString("\n");
					popLogfile->WriteString(prlImp->Line);
					popLogfile->WriteString("\n");
				}

				sprintf(pclErrbuf,GetString(IDS_STRING1353),ilLinecnt,prlImp->Viax);//"Fehler in Zeile %d: Flughafenbezeichnung (Vial)'%s' nicht in den Stammdaten erfasst.\n"

				popLogfile->WriteString( pclErrbuf );
				blOk = false;

			}
			else
			{
				prlFlight->Vial[4] = olApc4[0];
				prlFlight->Vial[5] = olApc4[1];
				prlFlight->Vial[6] = olApc4[2];
				prlFlight->Vial[7] = olApc4[3];

			}

		}



//PST	
		if (strlen(prlImp->Pstx))
		{
			CString olTmp;
			if(strcmp(prlFlight->Org3, pcgHome) == 0)
				//sprintf(pclWhere,"WHERE PNAM='%s'", prlFlight->Pstd);
				blRet = ogBCD.GetField("PST", "PNAM", prlFlight->Pstd, "PNAM", olTmp);

			else
				//sprintf(pclWhere,"WHERE PNAM='%s'", prlFlight->Psta);
				blRet = ogBCD.GetField("PST", "PNAM", prlFlight->Psta, "PNAM", olTmp);

			if(blRet == FALSE)
			{

			//	Logfile Eintrag
				if (blOk)
				{
					popLogfile->WriteString("\n");
					popLogfile->WriteString(prlImp->Line);
					popLogfile->WriteString("\n");
				}
				if(strcmp(prlFlight->Org3, pcgHome) == 0)
					sprintf(pclErrbuf,GetString(IDS_STRING1354),ilLinecnt,prlFlight->Pstd);//"Fehler in Zeile %d: Abflugposition '%s' nicht in den Stammdaten erfasst.\n"
				else
					sprintf(pclErrbuf,GetString(IDS_STRING1355),ilLinecnt,prlFlight->Psta);//"Fehler in Zeile %d: Ankunftsposition '%s' nicht in den Stammdaten erfasst.\n"

				popLogfile->WriteString( pclErrbuf );
				blOk = false;

			}
			//olPSTData.DeleteAll();
		}

//WRO	
		if (strlen(prlImp->Twr1))
		{
			CString olTmp;

			//sprintf(pclWhere,"WHERE WNAM='%s'", prlFlight->Twr1);

			if(!ogBCD.GetField("WRO", "WNAM", prlFlight->Twr1, "WNAM", olTmp))
			{
			//	Logfile Eintrag
				if (blOk)
				{
					popLogfile->WriteString("\n");
					popLogfile->WriteString(prlImp->Line);
					popLogfile->WriteString("\n");
				}
				sprintf(pclErrbuf,GetString(IDS_STRING1356),ilLinecnt,prlFlight->Twr1);//"Fehler in Zeile %d: Warteraum Terminal '%s' nicht in den Stammdaten erfasst.\n"

				popLogfile->WriteString( pclErrbuf );
				blOk = false;

			}
		}

//ACT  
		if (strlen(prlImp->Act5))
		{
			CString olAct3;
			
			//sprintf(pclWhere,"WHERE ACT3='%s' OR ACT5='%s'", prlFlight->Act5, prlFlight->Act5);
			if(!ogBCD.GetField("ACT", "ACT5", prlFlight->Act5, "ACT3", olAct3 ))
			{

			//	Logfile Eintrag
				if (blOk)
				{
					popLogfile->WriteString("\n");
					popLogfile->WriteString(prlImp->Line);
					popLogfile->WriteString("\n");
				}
				sprintf(pclErrbuf,GetString(IDS_STRING1357),ilLinecnt,prlFlight->Act5); //"Fehler in Zeile %d: Flugzeug-5-Letter Code '%s' nicht in den Stammdaten erfasst.\n"

				popLogfile->WriteString( pclErrbuf );
				blOk = false;

			}
			else
			{
				strcpy(prlFlight->Act3, olAct3);
			}
			//olACTData.DeleteAll();
		}


//TTYP
		if (strlen(prlImp->Ttyp))
		{
			CString olTmp;

			//sprintf(pclWhere,"WHERE TTYP='%s'", prlFlight->Ttyp);
			if(!ogBCD.GetField("NAT", "TTYP", prlFlight->Ttyp, "TTYP", olTmp))
			{

			//	Logfile Eintrag
				if (blOk)
				{
					popLogfile->WriteString("\n");
					popLogfile->WriteString(prlImp->Line);
					popLogfile->WriteString("\n");
				}
				sprintf(pclErrbuf,GetString(IDS_STRING1358),ilLinecnt,prlFlight->Ttyp);//"Fehler in Zeile %d: Verkehrsart '%s' nicht in den Stammdaten erfasst.\n"

				popLogfile->WriteString( pclErrbuf );
				blOk = false;

			}

		}

// Gate
		if (strlen(prlImp->Gate))
		{
			CString olTmp;


			if(strcmp(prlFlight->Org3, pcgHome) == 0)
				//sprintf(pclWhere,"WHERE GNAM='%s'", prlFlight->Gtd1);
				//ilRet = omGnamKeyMap.Lookup( prlFlight->Gtd1, (void *& )pVoid);
				blRet = ogBCD.GetField("GAT", "GNAM", prlFlight->Gtd1, "GNAM", olTmp);

			else
				//sprintf(pclWhere,"WHERE GNAM='%s'", prlFlight->Gta1);
				//ilRet = omGnamKeyMap.Lookup( prlFlight->Gta1, (void *& )pVoid);
				blRet = ogBCD.GetField("GAT", "GNAM", prlFlight->Gta1, "GNAM", olTmp);

			if(blRet == FALSE)
			{

			//	Logfile Eintrag
				if (blOk)
				{
					popLogfile->WriteString("\n");
					popLogfile->WriteString(prlImp->Line);
					popLogfile->WriteString("\n");
				}
				if(strcmp(prlFlight->Org3, pcgHome) == 0)
					sprintf(pclErrbuf,GetString(IDS_STRING1359),ilLinecnt,prlFlight->Gtd1);//"Fehler in Zeile %d: Gate 1 Abflug '%s' nicht in den Stammdaten erfasst.\n"
				else
					sprintf(pclErrbuf,GetString(IDS_STRING1360),ilLinecnt,prlFlight->Gta1);//"Fehler in Zeile %d: Gate 1 Ankunft '%s' nicht in den Stammdaten erfasst.\n"

				popLogfile->WriteString( pclErrbuf );
				blOk = false;

			}
			//olGATData.DeleteAll();  
		}



	if (blOk)  // Alle Validierungen fehlerfrei, die wichtigsten Felder gef�llt, Flno Format korrekt
	{
		omFlights.Add(prlFlight);

		// Datensatz in die Keymaps einf�gen 
		FLNOKEYLIST *prlFlno;
		JOINLIST *prlJoin = new JOINLIST;

		prlJoin->prlFlight = prlFlight; 

		// Flugnummer f�r verkn�pften Flugdatensatz erzeugen
		bool blFlnoTest = true;
		CString olFlno, opAlc, opFltn, opFlns;
        CreateFlno(prlImp->Flnj, olFlno, opAlc, opFltn, opFlns, &blOk);
		if (blFlnoTest == false)
		{
			//	Logfile Eintrag
			if (blOk)
			{
				popLogfile->WriteString("\n");
				popLogfile->WriteString(prlImp->Line);
				popLogfile->WriteString("\n");
			}
			sprintf(pclErrbuf,GetString(IDS_STRING1349),ilLinecnt,prlFlight->Flno);//"Fehler in Zeile %d: Flugnummer '%s' ung�ltig.\n"
			popLogfile->WriteString( pclErrbuf );
			delete prlJoin;		// Datensatz wird nicht mehr gebraucht
			delete prlFlight;	// Datensatz wird nicht mehr gebraucht
			blOk = false;

		}
		else
		{
			strcpy(prlJoin->Flnj, olFlno);

			strcpy(prlJoin->Stoj, prlImp->Stoj);
			strcpy(prlJoin->ArDe, prlImp->ArDe);
			// Checkin-Schalter				
			strcpy(prlJoin->Cicf, prlImp->Cicf);
			//prlJoin->ilCnt = omFlights.GetSize();

			//if(prlFlight->Flno != "")     
			//{
			if(omFlnoKeyMap.Lookup(prlFlight->Flno,(void *& )prlFlno) == TRUE)
			{
				//prlJoin->ilCnt = prlFlno->JoinData.GetSize() + 1;
				prlFlno->JoinData.Add(prlJoin);
			}
			else
			{
				prlFlno = new FLNOKEYLIST;
				//prlJoin->ilCnt = 1;
				omFlnoKeyMap.SetAt(prlFlight->Flno, prlFlno);

				prlFlno->JoinData.Add(prlJoin);
			}
		}
	}	

	else
		delete prlFlight;  // Datensatz wird nicht mehr gebraucht

}


//--DELETE-FLIGHT----------------------------------------------------------------------------------------------
// Aus den Flug-Datens�tze werden die Daten STOA und STOD �berpr�ft:
// Das fr�heste und das sp�teste Datum werden ermittelt
// Nach Anwenderbest�tigung werden Daten in diesem Datums-Bereich in der Datenbank gel�scht
void CedaImpFlightData::DeleteFlights(void)
{
	SEASONDLGFLIGHTDATA *prlFlight = new SEASONDLGFLIGHTDATA;
	CTime oMin;
	CTime oMax;
	
	if (omFlights.GetSize())
	{
		prlFlight = &omFlights[0];

		// Min und Max mit erstem DS Eintrag vorbelegen
		if(strcmp(prlFlight->Org3, pcgHome) == 0)
			oMin = oMax = prlFlight->Stod;
		else
			oMin = oMax = prlFlight->Stoa;
	
		// alle DS bis auf den ersten durchgehen, den kleinsten und gr��ten Zeitwert speichern
		for(int ilRecCnt = omFlights.GetSize() - 1; ilRecCnt >= 1; ilRecCnt--)	
		{
			prlFlight = &omFlights[ilRecCnt];

			if(strcmp(prlFlight->Org3, pcgHome) == 0)
			{
				if (prlFlight->Stod < oMin)
				{
					oMin = prlFlight->Stod;
				}
				else
				{	
					if (prlFlight->Stod > oMax)
						oMax = prlFlight->Stod;
				}
			}

			else if(strcmp(prlFlight->Des3, pcgHome) == 0)
			{
				if (prlFlight->Stoa < oMin)
				{
					oMin = prlFlight->Stoa;
				}
				else
				{	
					if (prlFlight->Stoa > oMax)
						oMax = prlFlight->Stoa;
				}
			}


		} 

	}

	char pclInfo[256];
	CString oMinStr = oMin.Format( "%d.%m.%Y" );
	CString oMaxStr = oMax.Format( "%d.%m.%Y" );
	sprintf(pclInfo, GetString(IDS_STRING1361),oMinStr,oMaxStr);

	MessageBox(NULL, pclInfo, GetString(IDS_STRING916), MB_OKCANCEL);

	delete prlFlight;
}



//--CREATE-FLIGHTS----------------------------------------------------------------------------------------------
// Alle Flug-Datens�tze werden in einer Schleife abgehandelt.
// Es werden Verkn�pfungen zweischen Hin- und R�ckfl�gen hergestellt.
// Die Datens�tze werden um Daten erg�nzt, der Flight-Handler aufgerufen
void CedaImpFlightData::CreateFlights(CStdioFile *popLogfile)
{
	CCSPtrArray<CCADATA> olCaaData;
	FLNOKEYLIST *prlTmpFlno;
	FLNOKEYLIST *prlFlno;
	JOINLIST *prlJoin = new JOINLIST;
	JOINLIST *prlTmpJoin = new JOINLIST;


	SEASONDLGFLIGHTDATA *prlFlight;
	SEASONDLGFLIGHTDATA *prlSecondFlight;
	CTime olPfrom;
	CTime olPto;
	int ilRet;
	char pclErrbuf[256];

	
	char pclADaySel[10];
	char pclDDaySel[10];
	int ilLc = 0;
	int j;
	int ilLinecnt = omFlights.GetSize();

	while(omFlights.GetSize())
	{
		bool blFound = false;
		ilLc++;
		prlFlight = &omFlights[0];

	  // aktuellen Flug in KeyMap suchen	
  	  if(omFlnoKeyMap.Lookup(prlFlight->Flno,(void *& )prlFlno) == TRUE)  
	  {
		

		if(strcmp(prlFlight->Des3, pcgHome) == 0)
		{
			for(int i =  prlFlno->JoinData.GetSize() - 1; i >= 0; i--)
			{
				*prlJoin = prlFlno->JoinData[i];
				if(prlJoin->prlFlight->Stoa == prlFlight->Stoa)
					break;	// der richtige ist gefunden
			}

			olPto = olPfrom = prlFlight->Stoa;
			GetDayOfWeek(prlFlight->Stoa, pclADaySel);
			GetDayOfWeek(prlFlight->Stoa, pclDDaySel);	// ggf. �berschreiben

			// R�ckflug ermitteln
			if(omFlnoKeyMap.Lookup(prlJoin->Flnj,(void *& )prlTmpFlno) == TRUE)  //DA4444
			{
				int ilFirst = prlTmpFlno->JoinData.GetSize();
				// neu
				bool bFirstSaved = false;

				for(int i = ilFirst; i > 0; i--)
				{
					*prlTmpJoin = prlTmpFlno->JoinData[i-1]; //Pointer Array in Schleife auslesen
					if(!strcmp(prlTmpJoin->ArDe, "D"))
					{

						if (prlFlight->Stoa < prlTmpJoin->prlFlight->Stod)
						{
							// neu
							//if ((i == ilFirst) || (prlTmpJoin->prlFlight->Stod < prlSecondFlight->Stod))
							if ((!bFirstSaved) || (prlTmpJoin->prlFlight->Stod < prlSecondFlight->Stod))
							{
								// Datensatz speichern
								prlSecondFlight = prlTmpJoin->prlFlight;
								bFirstSaved = true;

								olCaaData.DeleteAll();

								// Checkin-Schalter	bei Abflug
								CString olDate = prlTmpJoin->prlFlight->Stod.Format("%Y%m%d");
								olDate +=  prlTmpJoin->Cicf;
								olDate +=  "00";
								for( j = 0; j < 4; j++)
								{
									CCADATA *prlCca;
									CTime olCkbs;

									olCkbs = DBStringToDateTime(olDate);
									ogBasicData.LocalToUtc(olCkbs);

									prlCca = new CCADATA;
									olCaaData.Add(prlCca);
									olCaaData[j].Ckbs = olCkbs;
								}

								olPto = olPfrom = prlSecondFlight->Stod;
								//GetDayOfWeek(prlSecondFlight->Stod, pclADaySel);
								GetDayOfWeek(prlSecondFlight->Stod, pclDDaySel);
								blFound = true;
							}
						}
					}
				}

			}


		}
		else
		{


			for(int i =  prlFlno->JoinData.GetSize() - 1; i >= 0; i--)
			{
				*prlJoin = prlFlno->JoinData[i];
				if(prlJoin->prlFlight->Stod == prlFlight->Stod)
					break;	// der richtige ist gefunden
			}

			olPto = olPfrom = prlFlight->Stod;
			GetDayOfWeek(prlFlight->Stod, pclDDaySel);
			GetDayOfWeek(prlFlight->Stod, pclADaySel);	// ggf. �berschreiben

			olCaaData.DeleteAll();

			// Checkin-Schalter	bei Abflug
			CString olDate = prlFlight->Stod.Format("%Y%m%d");
			olDate +=  prlJoin->Cicf;
			olDate +=  "00";
			for( j = 0; j < 4; j++)
			{
				CCADATA *prlCca;
				CTime olCkbs;

				olCkbs = DBStringToDateTime(olDate);
				ogBasicData.LocalToUtc(olCkbs);

				prlCca = new CCADATA;
				olCaaData.Add(prlCca);
				olCaaData[j].Ckbs = olCkbs;
			}


			// R�ckflug ermitteln
			if(omFlnoKeyMap.Lookup(prlJoin->Flnj,(void *& )prlTmpFlno) == TRUE)  
			{
				int ilFirst = prlTmpFlno->JoinData.GetSize();
				// neu
				bool bFirstSaved = false;

				for(int i = ilFirst; i > 0; i--)
				{
					*prlTmpJoin = prlTmpFlno->JoinData[i-1];
					if(!strcmp(prlTmpJoin->ArDe, "A"))
					{
						if (prlFlight->Stod > prlTmpJoin->prlFlight->Stoa)
						{
							// neu
							//if((i == ilFirst) || (prlTmpJoin->prlFlight->Stoa > prlSecondFlight->Stoa))
							if((!bFirstSaved) || (prlTmpJoin->prlFlight->Stoa > prlSecondFlight->Stoa))
							{
								// Datensatz speichern
								prlSecondFlight = prlTmpJoin->prlFlight;
								bFirstSaved = true;
								olPto = olPfrom = prlSecondFlight->Stoa;
								GetDayOfWeek(prlSecondFlight->Stoa, pclADaySel);
								//GetDayOfWeek(prlSecondFlight->Stoa, pclDDaySel);
								blFound = true;
							}
						}
					}
				}
				//omFlnoKeyMap.RemoveKey(prlFlight->Flno);
			}

		}
	  } 

//RST!!!!

/*
		CTime olFrom(1998,  6,  5, 1, 1, 1);
		CTime olTo(1998, 10, 24, 1, 1, 1);

		strcpy(pclADaySel, "1234567");
		strcpy(pclDDaySel, "1234567");

		if (blFound)
			ilRet = ogSeasonDlgFlights.AddSeasonFlights(olFrom, olTo, pclADaySel, pclDDaySel, "1", olPfrom, olPto, prlFlight, prlSecondFlight, olCaaData);
		else if (strcmp(prlFlight->Des3, pcgHome) == 0)
			ilRet = ogSeasonDlgFlights.AddSeasonFlights(olFrom, olTo, pclADaySel, pclDDaySel, "1", olPfrom, olPto, prlFlight, NULL, olCaaData);
		else
			ilRet = ogSeasonDlgFlights.AddSeasonFlights(olFrom, olTo, pclADaySel, pclDDaySel, "1", olPfrom, olPto, NULL, prlFlight, olCaaData);


		Sleep(10000);

*/

		if (blFound)
			ilRet = ogSeasonDlgFlights.AddSeasonFlights(olPfrom, olPto, pclADaySel, pclDDaySel, "1", olPfrom, olPto, prlFlight, prlSecondFlight, olCaaData);
		else if (strcmp(prlFlight->Des3, pcgHome) == 0)
			ilRet = ogSeasonDlgFlights.AddSeasonFlights(olPfrom, olPto, pclADaySel, pclDDaySel, "1", olPfrom, olPto, prlFlight, NULL, olCaaData);
		else
			ilRet = ogSeasonDlgFlights.AddSeasonFlights(olPfrom, olPto, pclADaySel, pclDDaySel, "1", olPfrom, olPto, NULL, prlFlight, olCaaData);


		if (ilRet == -4)
		{

			if(strcmp(prlFlight->Des3, pcgHome) == 0)
			{
				CString olDateStr = prlFlight->Stoa.Format( "%d.%m.%Y %H:%M:%S" );
				sprintf(pclErrbuf,GetString(IDS_STRING1362),prlFlight->Flno, olDateStr); // "\nFlug mit Flugnummer %s, Ankunftszeit %s wurde evtl. nicht gespeichert.\n"
			}
			else
			{
				CString olDateStr = prlFlight->Stod.Format( "%d.%m.%Y %H:%M:%S" );
				sprintf(pclErrbuf,GetString(IDS_STRING1362),prlFlight->Flno, olDateStr);//"Flug mit Flugnummer %s, Abflugzeit %s wurde evtl. nicht gespeichert.\n"
			}
			popLogfile->WriteString( pclErrbuf );
		}

		if (blFound)
		{
			for(int j = omFlights.GetSize() - 1; j >= 0; j--)
			{
				if (prlSecondFlight == &omFlights[j])
				{
					omFlights.DeleteAt(j);
					break;
				}
			}
		}

		omFlights.DeleteAt(0);

		if(polProgress != NULL)
			polProgress->m_CP_Progress.OffsetPos(1);	// Progressbar weiterschieben

	}

	olCaaData.DeleteAll();

	delete prlJoin;
	delete prlTmpJoin;
}



//--CREATE-FLNO---------------------------------------------------------------------------------------------------
// In String-Komponeneten zerlegen, dabei Flugnummer auf G�ltigkeit untersuchen
// G�ltiges Muster: XXX#####X  (2-3 Buchstaben, 1-5 Ziffern, auch mit Blancs, 0-1 Buchstaben)
// ung�ltig: blOk = false

void CedaImpFlightData::CreateFlno(char* pcpFlno, CString &opFlno, CString &opAlc, CString &opFltn, CString &opFlns, bool* blOk)
{
	opFlno = "";
	*blOk = false;

	
	CString olStr(pcpFlno);

	olStr.TrimRight();
	olStr.TrimLeft();


	if(olStr.GetLength() < 3)
		return;

	//Extract Alc 
	if(isdigit(olStr[2]))
	{
		opAlc = olStr.Left(2);
		olStr = olStr.Mid(2);
	}
	else
	{
		opAlc = olStr.Left(3);
		opAlc.TrimLeft();
		olStr = olStr.Mid(3);
	}

	//Search Flns 
	for(int i = olStr.GetLength() - 1; i >= 0; i--)
	{
		if(!isdigit(olStr[i]))
		{
			opFlns = olStr[i];
			olStr = olStr.Left(i);
			break;
		}
	}

	olStr.TrimRight();
	olStr.TrimLeft();

	//Extract Fltn 
	for( i = olStr.GetLength() - 1; i >= 0; i--)
	{
		if(!isdigit(olStr[i]))
		{
			return;
		}

	}

	opFltn = olStr;

	if(opFltn.GetLength() > 5)
		*blOk = false;
	else
		*blOk = true;



	opAlc.TrimRight();
	opAlc.TrimLeft();
	opFltn.TrimRight();
	opFltn.TrimLeft();
	opFlns.TrimRight();
	opFlns.TrimLeft();
	
	opFlno = ogSeasonDlgFlights.CreateFlno(opAlc, opFltn, opFlns);
	
}




//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaImpFlightData::ClearAll(bool bpWithRegistration)
{

	FLNOKEYLIST *prlFlno;
	POSITION pos;
	CString pString;

	
	if(polProgress != NULL)
		delete polProgress;


	// Fl�ge
	for( pos = omFlnoKeyMap.GetStartPosition(); pos != NULL; )
	{
		omFlnoKeyMap.GetNextAssoc( pos, pString , (void *&)prlFlno );
		prlFlno->JoinData.RemoveAll();
		delete prlFlno;
	}
	omFlnoKeyMap.RemoveAll();

	omFlights.DeleteAll();

}


//---------------------------------------------------------------------------------------------------------
