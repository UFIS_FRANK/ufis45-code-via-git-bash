// DGanttChartReport.cpp : implementation file
//

#include <stdafx.h>
#include <resrc1.h>
#include <Resource.h>
#include <DGanttChartReport.h>
#include <DGanttBarReport.h>
#include <CCSGlobl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(DGanttChartReport, CScrollView)


#define SPACE_KO_LEFT		30
#define SPACE_KO_TOP_PRN	95
#define SPACE_KO_TOP		30
#define SPACE_KO_BOTTOM		90
#define SPACE_KO_CHART		25

/////////////////////////////////////////////////////////////////////////////
// DGanttChartReport
DGanttChartReport::DGanttChartReport()
{

}
	

DGanttChartReport::DGanttChartReport(CWnd* ppParent, CRect olRect, CString opXAxe, 
								   CString opYAxe, CString opTotalNumber, CColor opColor)
{

	omRectWnd     = olRect;
	omXAxe        = opXAxe;
	omYAxe        = opYAxe;
	omColor       = opColor;
	omTotalNumber = opTotalNumber;


	char pclTmpText[512], pclConfigPath[512];
	lmCharSet = DEFAULT_CHARSET;
	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "CHARSET", "DEFAULT", pclTmpText, sizeof pclTmpText, pclConfigPath);
	if(!strcmp(pclTmpText, "ARABIC"))
		lmCharSet = ARABIC_CHARSET;
}


DGanttChartReport::~DGanttChartReport()
{

	omBars1.DeleteAll();
	omBars2.DeleteAll();

}


BEGIN_MESSAGE_MAP(DGanttChartReport, CScrollView)
	//{{AFX_MSG_MAP(DGanttChartReport)
	ON_WM_CONTEXTMENU()
	ON_WM_ERASEBKGND()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_RBUTTONDOWN()
	ON_WM_CREATE()
	// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// DGanttChartReport message handlers


void DGanttChartReport::OnInitialUpdate()
{

	CScrollView::OnInitialUpdate();

	CSize olSizeTotal;
	CPaintDC dc(GetParent());

    LOGFONT logFont;
    memset(&logFont, 0, sizeof(LOGFONT));

	logFont.lfCharSet= lmCharSet;
    logFont.lfHeight = - MulDiv(7, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Arial");
    omArial_7.CreateFontIndirect(&logFont);
	dc.SelectObject(&omArial_7);

	// Mindestbreite eines Balkens (= 4 digits) bestimmen
	CSize olSizeMinX = dc.GetTextExtent("9999");

	// tats�chliche Breite eines Balkens ermitteln
	int ilBarWidth = 0;
	if (imDataCount > 0)
		                 // Breite - linker Rand - rechter Rand - Platz lassen
		ilBarWidth = ((omRectWnd.Width() - SPACE_KO_LEFT - 10 - 15) / imDataCount);
	// wenn srcollen Breite eines Balkens auf ermittelte Mindestberiete setzen
	if (olSizeMinX.cx > ilBarWidth)
		imBarWidth = olSizeMinX.cx + 4; // wegen Rand des Rectangles + 4;
	else
		imBarWidth = ilBarWidth;
								           
	// L�nge der X-Achse = Anzahl Balken * Breite eines Balken + Rahmen - linker + rechter Rand
	//imSizeX = imDataCount * (imBarWidth + 3) - 32;

	// L�nge der X-Achse = (Anzahl Balken+1 (samit das Ende des Balkens erreicht wird) * Breite eines Balken + Abstand
	imSizeX = (imDataCount+1) * imBarWidth + 15;
	if (imSizeX < omRectWnd.Width())
		imSizeX = omRectWnd.Width();

	if (imSizeX-20 < omRectWnd.Width())
		olSizeTotal.cx = omRectWnd.Width();
	else
		olSizeTotal.cx = imSizeX;
	olSizeTotal.cy = omRectWnd.Height() - 25;

	SetScrollSizes(MM_TEXT, olSizeTotal);

}


BOOL DGanttChartReport::OnEraseBkgnd(CDC* pDC) 
{

    // TODO: Add your message handler code here and/or call default
	COLORREF llBkColor;
	llBkColor = RGB(255, 255, 255);

    CRect olClipRect;
    pDC->GetClipBox(&olClipRect);

    CBrush olBrush(llBkColor);
    CBrush *polOldBrush = pDC->SelectObject(&olBrush);
    pDC->PatBlt(olClipRect.left, olClipRect.top,
        olClipRect.Width(), olClipRect.Height(),
        PATCOPY);
    pDC->SelectObject(polOldBrush);
    
    return TRUE;

}


void DGanttChartReport::OnDraw(CDC* pDC)
{

    CRect olClipRect;
	pDC->GetClipBox(&olClipRect);
	

	// --------------------
	// Malen des KO-Systems
	// --------------------
	// kleine Schrift w�hlen
    LOGFONT logFont;
    memset(&logFont, 0, sizeof(LOGFONT));

	CFont olArial_8;
	logFont.lfCharSet= lmCharSet;
    logFont.lfHeight = - MulDiv(8, pDC->GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Arial");
    olArial_8.CreateFontIndirect(&logFont);
	pDC->SelectObject(&olArial_8);


	// Setzen des Ausgangspunktes
	pDC->MoveTo(SPACE_KO_LEFT, SPACE_KO_TOP);
	// Y-Achse
	pDC->LineTo(SPACE_KO_LEFT, omRectWnd.Height() - SPACE_KO_BOTTOM);

	// Text der Y-Achse
	CSize olSize = pDC->GetTextExtent(omYAxe);
	pDC->TextOut(SPACE_KO_LEFT + 5, SPACE_KO_TOP, omYAxe);


	// Kordinaten der Y-Achse
	if (imTotalNumber <= 10)
	{
		for (int i = 1; i <= imTotalNumber; i++)
		{
			double dl = (double)(omRectWnd.Height() - SPACE_KO_BOTTOM - SPACE_KO_TOP - SPACE_KO_CHART) / (double)imTotalNumber;
			int ilY = (int)(dl * i);

			pDC->MoveTo(SPACE_KO_LEFT - 5, GetGanttYBeginn() - ilY);
			pDC->LineTo(SPACE_KO_LEFT + 5, GetGanttYBeginn() - ilY);

			CString olKO;
			olKO.Format("%d", i);
			CSize olSize = pDC->GetTextExtent(olKO);
			pDC->TextOut(SPACE_KO_LEFT - 5 - olSize.cx, GetGanttYBeginn() - ilY - (olSize.cy/2), olKO);
		}
	}

	if (imTotalNumber <= 100 && imTotalNumber > 10)
	{
		int ilSteps = (imTotalNumber / 10) + 1;

		for (int i = 1; i < ilSteps; i++)
		{
			double dl = (double)(omRectWnd.Height() - SPACE_KO_BOTTOM - SPACE_KO_TOP - SPACE_KO_CHART) / (double)imTotalNumber;
			int ilY = (int)(dl * 10*i);

			pDC->MoveTo(SPACE_KO_LEFT - 5, GetGanttYBeginn() - ilY);
			pDC->LineTo(SPACE_KO_LEFT + 5, GetGanttYBeginn() - ilY);

			CString olKO;
			olKO.Format("%d", i*10);
			CSize olSize = pDC->GetTextExtent(olKO);
			pDC->TextOut(SPACE_KO_LEFT - 5 - olSize.cx, GetGanttYBeginn() - ilY - (olSize.cy/2), olKO);
		}
	}
	
	if (imTotalNumber <= 1000 && imTotalNumber > 100)
	{
		int ilFactor;
		if (imTotalNumber < 300)
			ilFactor = 10;
		else
			ilFactor = 100;
		int ilSteps = (imTotalNumber / ilFactor) + 1;
		for (int i = 1; i < ilSteps; i++)
		{
			double dl = (double)(omRectWnd.Height() - SPACE_KO_BOTTOM - SPACE_KO_TOP - SPACE_KO_CHART) / (double)imTotalNumber;
			int ilY = (int)(dl * ilFactor*i);

			pDC->MoveTo(SPACE_KO_LEFT - 5, GetGanttYBeginn() - ilY);
			pDC->LineTo(SPACE_KO_LEFT + 5, GetGanttYBeginn() - ilY);

			CString olKO;
			olKO.Format("%d", i*ilFactor);
			CSize olSize = pDC->GetTextExtent(olKO);
			pDC->TextOut(SPACE_KO_LEFT - 5 - olSize.cx, GetGanttYBeginn() - ilY - (olSize.cy/2), olKO);
		}
	}
	
	//Pfeil der Y-Achse
	CPoint olPoints[3];;
	olPoints[0].x = SPACE_KO_LEFT - 3;
	olPoints[0].y = SPACE_KO_TOP;

	olPoints[1].x = SPACE_KO_LEFT + 3;
	olPoints[1].y = SPACE_KO_TOP;

	olPoints[2].x = SPACE_KO_LEFT;
	olPoints[2].y = SPACE_KO_TOP - 6;

	pDC->SetPolyFillMode(ALTERNATE);
	pDC->Polygon(olPoints, 3);

	// X-Achse
	pDC->MoveTo(SPACE_KO_LEFT, omRectWnd.Height() - SPACE_KO_BOTTOM);
	pDC->LineTo((imSizeX - 20), omRectWnd.Height() - SPACE_KO_BOTTOM);
	// Text der X-Achse
	olSize = pDC->GetTextExtent(omXAxe);
	pDC->TextOut((imSizeX - 20) - olSize.cx, omRectWnd.Height() - SPACE_KO_BOTTOM + olSize.cy, omXAxe);

	//Pfeil der X-Achse
	pDC->MoveTo(omRectWnd.Width() - SPACE_KO_LEFT, omRectWnd.Height() - SPACE_KO_BOTTOM);
	olPoints[0].x = imSizeX - 20;
	olPoints[0].y = omRectWnd.Height() - SPACE_KO_BOTTOM - 3;

	olPoints[1].x = imSizeX - 20;
	olPoints[1].y = omRectWnd.Height() - SPACE_KO_BOTTOM + 3;

	olPoints[2].x = (imSizeX - 20) + 6;
	olPoints[2].y = omRectWnd.Height() - SPACE_KO_BOTTOM;

	pDC->SetPolyFillMode(ALTERNATE);
	pDC->Polygon(olPoints, 3);


	double dl = (double)(omRectWnd.Height() - SPACE_KO_BOTTOM - SPACE_KO_TOP - SPACE_KO_CHART) / (double)imTotalNumber;
	int ilHeightTotalNumber = (int)(dl * imTotalNumber);

	omTotalNumber.Format(omTotalNumber, imTotalNumber);
	olSize = pDC->GetTextExtent(omTotalNumber);
	int ilTextKO = ((imSizeX - 20) - olSize.cx) / 2;
	pDC->TextOut(ilTextKO , GetGanttYBeginn() - ilHeightTotalNumber - (olSize.cy), omTotalNumber);

	pDC->MoveTo(SPACE_KO_LEFT, GetGanttYBeginn() - ilHeightTotalNumber);
	pDC->LineTo((imSizeX - 20), GetGanttYBeginn() - ilHeightTotalNumber);


	// Legende
	// Mitte
	pDC->SelectObject(&ogCourier_Bold_10);
	olSize = pDC->GetTextExtent(omMiddle);
	int ilXKO = (omRectWnd.Width() - olSize.cx) / 2;
	pDC->TextOut(ilTextKO, omRectWnd.Height() - olSize.cy, omMiddle);
	pDC->SelectObject(&olArial_8);
			

	// Links
	CString olLeft1;
	CString olLeft2;

	for (int i = 0; i < omLeft.Find("\n"); i++)
	{
		olLeft1 += omLeft.GetAt(i);
	}
	for (i = omLeft.Find("\n")+1; i < omLeft.GetLength(); i++)
	{
		olLeft2 += omLeft.GetAt(i);
	}

	pDC->TextOut(SPACE_KO_LEFT, omRectWnd.Height() - (4*olSize.cy), olLeft1);
	pDC->TextOut(SPACE_KO_LEFT, omRectWnd.Height() - (3*olSize.cy), olLeft2);


	// Rechts
	CString olRight1;
	CString olRight2;

	for (i = 0; i < omRight.Find("\n"); i++)
	{
		olRight1 += omRight.GetAt(i);
	}
	for (i = omRight.Find("\n")+1; i < omRight.GetLength(); i++)
	{
		olRight2 += omRight.GetAt(i);
	}

	olSize = pDC->GetTextExtent(olRight1);
	pDC->TextOut(imSizeX - 20 - olSize.cx, omRectWnd.Height() - (4*olSize.cy), olRight1);
	pDC->TextOut(imSizeX - 20 - olSize.cx, omRectWnd.Height() - (3*olSize.cy), olRight2);


	omColor.r = 0;
	omColor.g = 0;
	omColor.b = 255;
	// Balken erzeugen und ausgeben
	if (omData1.GetSize() == omXAxeText.GetSize())
	{
		for (int i = 0; i < omData1.GetSize(); i++)
		{
			omBars1.Add(new DGanttBarReport(this, omColor, omData1[i], omXAxeText[i]));
			if (omBarText.GetSize() > 0)
				omBars1[i].Draw(pDC, SPACE_KO_LEFT, i, GetBarHeight1(i), GetBarHeight1(i-1), omBarText[i]);
			else
				omBars1[i].Draw(pDC, SPACE_KO_LEFT, i, GetBarHeight1(i), GetBarHeight1(i-1));
		}
	}
	else
	{
		MessageBox("Fehler in den Daten", "DGanttChart");
	}

	omColor.r = 255;
	omColor.g = 0;
	omColor.b = 0;
	// Balken erzeugen und ausgeben
	if (omData2.GetSize() == omXAxeText.GetSize())
	{
		for (int i = 0; i < omData2.GetSize(); i++)
		{
			omBars2.Add(new DGanttBarReport(this, omColor, omData2[i], omXAxeText[i]));
			if (omBarText.GetSize() > 0)
				omBars2[i].Draw(pDC, SPACE_KO_LEFT, i, GetBarHeight2(i), GetBarHeight2(i-1), omBarText[i]);
			else
				omBars2[i].Draw(pDC, SPACE_KO_LEFT, i, GetBarHeight2(i), GetBarHeight2(i-1));
		}
	}
	else
	{
		MessageBox("Fehler in den Daten", "DGanttChart");
	}

}


void DGanttChartReport::SetData1(const CUIntArray& opData)
{

	omData1.Copy(opData);
	imDataCount = omData1.GetSize();

	SetHighAndLowData();

}


void DGanttChartReport::SetData2(const CUIntArray& opData)
{

	omData2.Copy(opData);
//	imDataCount = omData2.GetSize();

//	SetHighAndLowData();

}


void DGanttChartReport::SetXAxeText(const CStringArray& opXAxeText)
{

	omXAxeText.Copy(opXAxeText);

	if (omXAxeText.GetSize() < omData1.GetSize())
	{
		// Rest mit Leerstrings f�llen
		for (int i = omXAxeText.GetSize(); i < omData1.GetSize(); i++)
			omXAxeText.Add("");
	}

}


void DGanttChartReport::SetBarText(const CStringArray& opBarText)
{

	omBarText.Copy(opBarText);

	if (omBarText.GetSize() < omData1.GetSize())
	{
		// Rest mit Leerstrings f�llen
		for (int i = omBarText.GetSize(); i < omData1.GetSize(); i++)
			omBarText.Add("");
	}

}


int DGanttChartReport::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{

	lpCreateStruct->style &= WS_VSCROLL;
	if (CScrollView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	return 0;

}


int DGanttChartReport::GetGanttYBeginn()
{
	
	return omRectWnd.Height() - SPACE_KO_BOTTOM;

}


int DGanttChartReport::GetGanttYBeginnPrint()
{

	return (int)(omRectPrn.Height() - (SPACE_KO_BOTTOM * dmFactorY));

}


int DGanttChartReport::GetGanttBarWidthPrint()
{

	// Breite - linker Rand - rechter Rand - Platz lassen 
	//return (int)((omRectPrn.Width() - (SPACE_KO_LEFT * dmFactorX) - (10 * dmFactorX) - (5 * dmFactorX)) / imDataCount);
	return (int) ((int)((imSizeX - 20)*dmFactorX) -  (int)(SPACE_KO_LEFT*dmFactorX)) / imDataCount;

}


int DGanttChartReport::GetGanttBarWidth()
{

	
	return imBarWidth;

}


void DGanttChartReport::SetHighAndLowData()
{

	// groessten und kleinsten Wert extrahieren
	int ilCompare;
	if (omData1.GetSize() > 0)
	{
		imLow  = omData1[0];
		imHigh = omData1[0];

		for (int i = 0; i < omData1.GetSize(); i++)
		{
			ilCompare = omData1[i];
			if (ilCompare > imHigh)
				imHigh = ilCompare;
			if (ilCompare < imLow)
				imLow = ilCompare;
		}
	}

}


int DGanttChartReport::GetBarHeight1(int ipIndex)
{

	if (ipIndex > omData1.GetSize() || ipIndex < 0)
		return 0;

	double dl = (double)(omRectWnd.Height() - SPACE_KO_BOTTOM - SPACE_KO_TOP - SPACE_KO_CHART) / (double)imTotalNumber;

	// Balkenh�he = (Gesamthoehe / h�chster Wert) * aktueller Wert
	return (int)(dl * omData1[ipIndex]);

}


int DGanttChartReport::GetBarHeight2(int ipIndex)
{

	if (ipIndex > omData2.GetSize() || ipIndex < 0)
		return 0;

	double dl = (double)(omRectWnd.Height() - SPACE_KO_BOTTOM - SPACE_KO_TOP - SPACE_KO_CHART) / (double)imTotalNumber;

	// Balkenh�he = (Gesamthoehe / h�chster Wert) * aktueller Wert
	return (int)(dl * omData2[ipIndex]);

}


int DGanttChartReport::GetBarHeight1Print(int ipIndex)
{

	if (ipIndex > omData1.GetSize() || ipIndex < 0)
		return 0;

	int ilHighY = omRectPrn.Height() - (int)(SPACE_KO_BOTTOM*dmFactorY) - (int)(SPACE_KO_TOP_PRN*dmFactorY) - (int)(SPACE_KO_CHART*dmFactorY);
	double dl = (double)(ilHighY) / (double)imTotalNumber;

	// Balkenh�he = (Gesamthoehe / h�chster Wert) * aktueller Wert
	int ret = (int)(dl * omData1[ipIndex]);
	return ret;

}


int DGanttChartReport::GetBarHeight2Print(int ipIndex)
{

	if (ipIndex > omData2.GetSize() || ipIndex < 0)
		return 0;

	int ilHighY = omRectPrn.Height() - (int)(SPACE_KO_BOTTOM*dmFactorY) - (int)(SPACE_KO_TOP_PRN*dmFactorY) - (int)(SPACE_KO_CHART*dmFactorY);
	double dl = (double)(ilHighY) / (double)imTotalNumber;

	// Balkenh�he = (Gesamthoehe / h�chster Wert) * aktueller Wert
	int ret = (int)(dl * omData2[ipIndex]);
	return ret;

}


void DGanttChartReport::Print(CDC* popDC, CCSPrint* popPrint, CString opHeader, CString opFooterLeft)
{

	popDC->SetMapMode(MM_TEXT);

	popDC->GetClipBox(&omRectPrn);


    CPaintDC dc(this);
    CRect olClipRectWnd;
    dc.GetClipBox(&olClipRectWnd);

	// Umrechnungsfaktor f�rs Drucken berechnen
	int ilPixMonX = dc.GetDeviceCaps(LOGPIXELSX);
	int ilPixMonY = dc.GetDeviceCaps(LOGPIXELSY);

	int ilPixPrintX = popDC->GetDeviceCaps(LOGPIXELSX);
	int ilPixPrintY = popDC->GetDeviceCaps(LOGPIXELSY);

	dmFactorX = (double)ilPixPrintX / (double)ilPixMonX;
	dmFactorY = (double)ilPixPrintY / (double)ilPixMonY;

	// verkleinern wegen Kopf- und Fusszeile
	omRectPrn.top += (long)(25 * dmFactorX);

	omHeader = opHeader;
	omFooterLeft = opFooterLeft;
	PrintHeader(popPrint, opHeader);
	PrintFooter(popPrint, opFooterLeft, 1);

	PrintCoordinateSystem(popDC);
	PrintBars(popDC, popPrint);

	// Druck abschlie�en
	popPrint->omCdc.EndPage();
	popPrint->omCdc.EndDoc();

}


void DGanttChartReport::PrintHeader(CCSPrint* popPrint, CString opHeader)
{

	popPrint->PrintUIFHeader("", opHeader, popPrint->imFirstLine-10);

}


void DGanttChartReport::PrintFooter(CCSPrint* popPrint, CString opFooterLeft, int ipPageNo)
{

	CString olFooterRight;

	olFooterRight.Format("Page: %d", ipPageNo);
	popPrint->PrintUIFFooter(opFooterLeft,"", olFooterRight);

}


// --------------------
// Malen des KO-Systems
// --------------------
void DGanttChartReport::PrintCoordinateSystem(CDC* popDC)
{

	// kleine Schrift w�hlen
    LOGFONT logFont;
    memset(&logFont, 0, sizeof(LOGFONT));

	CFont olArial_8;
	logFont.lfCharSet= lmCharSet;
    logFont.lfHeight = - MulDiv(8, popDC->GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Arial");
    olArial_8.CreateFontIndirect(&logFont);
	popDC->SelectObject(&olArial_8);
 
	CFont olArial_10;
	logFont.lfCharSet= lmCharSet;
    logFont.lfHeight = - MulDiv(10, popDC->GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Arial");
    olArial_10.CreateFontIndirect(&logFont);

	// Setzen des Ausgangspunktes
	popDC->MoveTo((int)(SPACE_KO_LEFT*dmFactorX), (int)(SPACE_KO_TOP_PRN*dmFactorY));
	// Y-Achse          
	popDC->LineTo((int)(SPACE_KO_LEFT*dmFactorX), omRectPrn.Height() - (int)(SPACE_KO_BOTTOM*dmFactorY) );

	// Text der Y-Achse
	CSize olSize = popDC->GetTextExtent(omYAxe);
	popDC->TextOut( (int)((SPACE_KO_LEFT + 5)*dmFactorX), (int)(SPACE_KO_TOP_PRN*dmFactorY), omYAxe);

	int ilHighY = omRectPrn.Height() - (int)(SPACE_KO_BOTTOM*dmFactorY) - (int)(SPACE_KO_TOP_PRN*dmFactorY) - (int)(SPACE_KO_CHART*dmFactorY);
	// Kordinaten der Y-Achse
	if (imTotalNumber <= 10)
	{
		for (int i = 1; i <= imTotalNumber; i++)
		{
			double dl = (double)(ilHighY) / (double)imTotalNumber;
			int ilY = (int)(dl * i);

			popDC->MoveTo((int)((SPACE_KO_LEFT - 5)*dmFactorX), GetGanttYBeginnPrint() - ilY);
			popDC->LineTo((int)((SPACE_KO_LEFT + 5)*dmFactorX), GetGanttYBeginnPrint() - ilY);

			CString olKO;
			olKO.Format("%d", i);
			CSize olSize = popDC->GetTextExtent(olKO);
			popDC->TextOut((int)((SPACE_KO_LEFT - 5)*dmFactorX) - olSize.cx, GetGanttYBeginnPrint() - ilY - (olSize.cy/2), olKO);
		}
	}

	if (imTotalNumber <= 100 && imTotalNumber > 10)
	{
		int ilSteps = (imTotalNumber / 10) + 1;

		for (int i = 1; i < ilSteps; i++)
		{
			double dl = (double)(ilHighY) / (double)imTotalNumber;
			int ilY = (int)(dl * 10*i);

			popDC->MoveTo((int)((SPACE_KO_LEFT - 5)*dmFactorX), GetGanttYBeginnPrint() - ilY);
			popDC->LineTo((int)((SPACE_KO_LEFT + 5)*dmFactorX), GetGanttYBeginnPrint() - ilY);

			CString olKO;
			olKO.Format("%d", i*10);
			CSize olSize = popDC->GetTextExtent(olKO);
			popDC->TextOut((int)((SPACE_KO_LEFT - 5)*dmFactorX) - olSize.cx, GetGanttYBeginnPrint() - ilY - (olSize.cy/2), olKO);
		}
	}
	
	if (imTotalNumber <= 1000 && imTotalNumber > 100)
	{
		int ilFactor;
		if (imTotalNumber < 300)
			ilFactor = 10;
		else
			ilFactor = 100;
		int ilSteps = (imTotalNumber / ilFactor) + 1;

		for (int i = 1; i < ilSteps; i++)
		{
			double dl = (double)(ilHighY) / (double)imTotalNumber;
			int ilY = (int)(dl * ilFactor*i);

			popDC->MoveTo((int)((SPACE_KO_LEFT - 5)*dmFactorX), GetGanttYBeginnPrint() - ilY);
			popDC->LineTo((int)((SPACE_KO_LEFT + 5)*dmFactorX), GetGanttYBeginnPrint() - ilY);

			CString olKO;
			olKO.Format("%d", i*ilFactor);
			CSize olSize = popDC->GetTextExtent(olKO);
			popDC->TextOut((int)((SPACE_KO_LEFT - 5)*dmFactorX) - olSize.cx, GetGanttYBeginnPrint() - ilY - (olSize.cy/2), olKO);
		}
	}
	
	//Pfeil der Y-Achse
	CPoint olPoints[3];;
	olPoints[0].x = (long)((SPACE_KO_LEFT - 3)*dmFactorX);
	olPoints[0].y = (long)((SPACE_KO_TOP_PRN)*dmFactorY);

	olPoints[1].x = (long)((SPACE_KO_LEFT + 3)*dmFactorX);
	olPoints[1].y = (long)((SPACE_KO_TOP_PRN)*dmFactorY);

	olPoints[2].x = (long)((SPACE_KO_LEFT)*dmFactorX);
	olPoints[2].y = (long)((SPACE_KO_TOP_PRN - 6)*dmFactorY);

	popDC->SetPolyFillMode(ALTERNATE);
	popDC->Polygon(olPoints, 3);

	// X-Achse
	popDC->MoveTo((int)(SPACE_KO_LEFT*dmFactorX), omRectPrn.Height() - (int)(SPACE_KO_BOTTOM*dmFactorY));
	popDC->LineTo((int)((imSizeX - 20)*dmFactorX), omRectPrn.Height() - (int)(SPACE_KO_BOTTOM*dmFactorY));
	// Text der X-Achse
	olSize = popDC->GetTextExtent(omXAxe);
	popDC->TextOut((int)((imSizeX - 20)*dmFactorX) - olSize.cx, omRectPrn.Height() - (int)(SPACE_KO_BOTTOM*dmFactorY) + olSize.cy, omXAxe);

	//Pfeil der X-Achse
	popDC->MoveTo(omRectPrn.Width() - (int)(SPACE_KO_LEFT*dmFactorX), omRectPrn.Height() - (int)(SPACE_KO_BOTTOM*dmFactorY));
	olPoints[0].x = (long)((imSizeX - 20)*dmFactorX);
	olPoints[0].y = omRectPrn.Height() - (int)((SPACE_KO_BOTTOM - 3)*dmFactorY);

	olPoints[1].x = (long)((imSizeX - 20)*dmFactorX);
	olPoints[1].y = omRectPrn.Height() - (int)((SPACE_KO_BOTTOM + 3)*dmFactorY);

	olPoints[2].x = (long)((imSizeX - 20 + 6)*dmFactorX);
	olPoints[2].y = omRectPrn.Height() - (int)(SPACE_KO_BOTTOM*dmFactorY);

	popDC->SetPolyFillMode(ALTERNATE);
	popDC->Polygon(olPoints, 3);

	double dl = (double)(ilHighY) / (double)imTotalNumber;
	int ilHeightTotalNumber = (int)(dl * imTotalNumber);

	omTotalNumber.Format(omTotalNumber, imTotalNumber);
	olSize = popDC->GetTextExtent(omTotalNumber);
	int ilTextKO = ((int)((imSizeX - 20)*dmFactorX) - olSize.cx) / 2;
	popDC->TextOut(ilTextKO , GetGanttYBeginnPrint() - ilHeightTotalNumber - (olSize.cy), omTotalNumber);

	popDC->MoveTo((int)(SPACE_KO_LEFT*dmFactorX), GetGanttYBeginnPrint() - ilHeightTotalNumber);
	popDC->LineTo((int)((imSizeX - 20)*dmFactorX), GetGanttYBeginnPrint() - ilHeightTotalNumber);


	// Legende
	// Mitte
	//popDC->SelectObject(&ogCourier_Bold_10);
	popDC->SelectObject(&olArial_10);
	olSize = popDC->GetTextExtent(omMiddle);
	int ilXKO = (omRectPrn.Width() - olSize.cx) / 2;
	popDC->TextOut(ilTextKO, omRectPrn.Height() - olSize.cy, omMiddle);
	popDC->SelectObject(&olArial_8);
			

	// Links
	CString olLeft1;
	CString olLeft2;

	for (int i = 0; i < omLeftPrint.Find("\n"); i++)
	{
		olLeft1 += omLeftPrint.GetAt(i);
	}
	for (i = omLeftPrint.Find("\n")+1; i < omLeftPrint.GetLength(); i++)
	{
		olLeft2 += omLeftPrint.GetAt(i);
	}

	olSize = popDC->GetTextExtent(olLeft1);
	popDC->TextOut((int)(SPACE_KO_LEFT*dmFactorX), omRectPrn.Height() - (4*olSize.cy), olLeft1);
	popDC->TextOut((int)(SPACE_KO_LEFT*dmFactorX), omRectPrn.Height() - (3*olSize.cy), olLeft2);


	// Rechts
	CString olRight1;
	CString olRight2;

	for (i = 0; i < omRightPrint.Find("\n"); i++)
	{
		olRight1 += omRightPrint.GetAt(i);
	}
	for (i = omRightPrint.Find("\n")+1; i < omRightPrint.GetLength(); i++)
	{
		olRight2 += omRightPrint.GetAt(i);
	}

	olSize = popDC->GetTextExtent(olRight1);
	popDC->TextOut((int)((imSizeX - 20)*dmFactorX) - olSize.cx, omRectPrn.Height() - (4*olSize.cy), olRight1);
	popDC->TextOut((int)((imSizeX - 20)*dmFactorX) - olSize.cx, omRectPrn.Height() - (3*olSize.cy), olRight2);

}


void DGanttChartReport::PrintBars(CDC* popDC, CCSPrint* popPrint)
{


	// Balken erzeugen und ausgeben
	if (omData1.GetSize() == omXAxeText.GetSize())
	{
		int ilBarNo = 0;
		int ilPageNo = 1;
		for (int i = 0; i < omData1.GetSize(); i++, ilBarNo++)
		{
			omBars1.Add(new DGanttBarReport(this, omColor, omData1[i], omXAxeText[i]));
			if (!omBars1[i].Print(popDC, (int)(SPACE_KO_LEFT * dmFactorX), ilBarNo, (int)((GetBarHeight1Print(i))), GetBarHeight1Print(i-1),dmFactorY))
			{
				// neue Seite
				popDC->EndPage();
				// von vorne auf neuer Seite mit Balkennummer beginnen
				ilBarNo = 0;
				// i zur�cksetzen, da aktuelles i nicht gedruckt wurde
				--i;
				// auf neue Seite Kop- und Fusszeile drucken
				PrintHeader(popPrint, omHeader);
				PrintFooter(popPrint, omFooterLeft, ++ilPageNo);
			}
		}
	}
	else
	{
		MessageBox("Fehler in den Daten", "DGanttChart");
	}

	// Balken erzeugen und ausgeben
	if (omData2.GetSize() == omXAxeText.GetSize())
	{
		int ilBarNo = 0;
		int ilPageNo = 1;
		for (int i = 0; i < omData2.GetSize(); i++, ilBarNo++)
		{
			omBars2.Add(new DGanttBarReport(this, omColor, omData2[i], omXAxeText[i]));
			if (!omBars2[i].Print(popDC, (int)(SPACE_KO_LEFT * dmFactorX), ilBarNo, (int)((GetBarHeight2Print(i))), GetBarHeight2Print(i-1), dmFactorY))
			{
				// neue Seite
				popDC->EndPage();
				// von vorne auf neuer Seite mit Balkennummer beginnen
				ilBarNo = 0;
				// i zur�cksetzen, da aktuelles i nicht gedruckt wurde
				--i;
			}
		}
	}
	else
	{
		MessageBox("Fehler in den Daten", "DGanttChart");
	}

}


BOOL DGanttChartReport::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) 
{
	// TODO: Add your specialized code here and/or call the base class

	CWnd::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);

	return TRUE;
}


CFont* DGanttChartReport::GetGanttChartFont()
{

	return &omArial_7;

}


void DGanttChartReport::SetLegende(CString opLeft, CString opRight, CString opMiddle)
{

	omLeft  = GetString(IDS_STRING1755);
	omRight = GetString(IDS_STRING1756);
	omLeft  += opLeft;
	omRight += opRight;
	omMiddle = opMiddle;

	omLeftPrint  = GetString(IDS_STRING1757);
	omRightPrint = GetString(IDS_STRING1758);
	omLeftPrint  += opLeft;
	omRightPrint += opRight;

}


void DGanttChartReport::SetTotalNumnber(int ipTotalNumber)
{

	imTotalNumber = ipTotalNumber;

}