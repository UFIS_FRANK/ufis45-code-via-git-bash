#include <stdafx.h>

#include <GanttBarReport.h>
#include <CCSGlobl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


GanttBarReport::GanttBarReport(GanttChartReport* popGanttChartReport, CColor Color,
							   unsigned int ipData, CString opText/* = ""*/)
{

	pomGanttChartReport = popGanttChartReport;
	imData  = ipData;
	imXText = opText;

}


GanttBarReport::~GanttBarReport()
{

}


void GanttBarReport::Draw(CDC* ppDCD, int ipSpace, int ipBarNo, int ipHeight, CString opBarText)
{

	CString olText;
	if (opBarText.IsEmpty())
		olText.Format("%d", imData);
	else
		olText = opBarText;

	CString olArrivalText,olDepartureText ;
	olArrivalText.Format("%d",pomGanttChartReport->imArrivalData);
	olDepartureText.Format("%d",pomGanttChartReport->imDeparture);

	ppDCD->SelectObject(pomGanttChartReport->GetGanttChartFont());
	

/*    LOGFONT logFont;
    memset(&logFont, 0, sizeof(LOGFONT));

	CFont olArial_7;

    logFont.lfHeight = - MulDiv(7, ppDCD->GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Arial");
    olArial_7.CreateFontIndirect(&logFont);
	ppDCD->SelectObject(&olArial_7);

	// Text im Balken, mindestens "9999" gro�
	CSize olSizeMinX = ppDCD->GetTextExtent("9999");
	int ilBarWidth;
	if (olSizeMinX.cx > pomGanttChartReport->GetGanttBarWidth())
		ilBarWidth = olSizeMinX.cx + 4; // wegen Rand des Rectangles;
	else*/
	int ilBarWidth = pomGanttChartReport->GetGanttBarWidth();

	
	int ilBeginnX = ipSpace + ilBarWidth * ipBarNo;

	
	//              Anfang in x-richtung;         H�he des Balkens                           Ende in x-Richtung                               y-Punkt au x-Achse                           
	
	//ppDCD->Rectangle(ilBeginnX,pomGanttChartReport->GetGanttYBeginn() - ipHeight, 
		             //ilBeginnX + ilBarWidth, 
					 //(pomGanttChartReport->GetGanttYBeginn() - ipHeight)/2);

//	ppDCD->Rectangle(ilBeginnX, pomGanttChartReport->GetGanttYBeginn() - ipHeight, 
//		             ilBeginnX + ilBarWidth, pomGanttChartReport->GetGanttYBeginn());

	//changed while fixing the PRF 8494
	
	// For drawing rectangles with different brushes for Arrival and Departure
	CBrush olDepBrush(RGB(240,230,140));
	CBrush *oldBrush = ppDCD->SelectObject(&olDepBrush);
	ppDCD->Rectangle(ilBeginnX-1,pomGanttChartReport->GetGanttYBeginn() - ipHeight, 
		             ilBeginnX + ilBarWidth, 
					 (pomGanttChartReport->GetGanttYBeginn() - pomGanttChartReport->imArrivalHeight));

	CBrush olArrBrush(RGB(189,183,107));
	ppDCD->SelectObject(olArrBrush);
	ppDCD->Rectangle(ilBeginnX-1,(pomGanttChartReport->GetGanttYBeginn() - pomGanttChartReport->imArrivalHeight-1), 
		             ilBeginnX + ilBarWidth, 
					 pomGanttChartReport->GetGanttYBeginn());
	ppDCD->SelectObject(oldBrush);
	

	CSize olSize = ppDCD->GetTextExtent(olText);
	int ilX = ((ilBarWidth - olSize.cx) / 2) + ilBeginnX;
	int ilY = pomGanttChartReport->GetGanttYBeginn() - ((pomGanttChartReport->imArrivalHeight + olSize.cy) / 2);
	CString olTextD = "A="+ olArrivalText;
	if(pomGanttChartReport->imArrivalHeight!=0)
	{
		ppDCD->TextOut(ilX, ilY, olTextD);
	}

	olSize = ppDCD->GetTextExtent(olText);
	int ilDeparHeight;
	ilDeparHeight = ipHeight - pomGanttChartReport->imArrivalHeight;
	ilX = ((ilBarWidth - olSize.cx) / 2) + ilBeginnX;
	ilY = (pomGanttChartReport->GetGanttYBeginn() - pomGanttChartReport->imArrivalHeight)  - ((ilDeparHeight + olSize.cy) / 2);
	CString olTextA = "D="+ olDepartureText;
	if(pomGanttChartReport->imArrivalHeight!= ipHeight)
	{
		ppDCD->TextOut(ilX, ilY, olTextA);
	}
	ilX = ((ilBarWidth - olSize.cx) / 2) + ilBeginnX;
	ilY = pomGanttChartReport->GetGanttYBeginn() - ((ipHeight + olSize.cy) / 2);
	
	if(ipHeight == 0)
	{
	ppDCD->TextOut(ilX, ilY, olText);
	}

	// Text unter der XAchse
	olSize = ppDCD->GetTextExtent(imXText);
	ilX = ((ilBarWidth - olSize.cx) / 2) + ilBeginnX;
	ilY = pomGanttChartReport->GetGanttYBeginn() + 1;
	ppDCD->TextOut(ilX, ilY, imXText);

	
}


bool GanttBarReport::Print(CDC* ppDCD, int ipSpace, int ipBarNo, int ipHeight, 
						   double dpFactorY, CString opBarText)
{

	CString olText;
	if (opBarText.IsEmpty())
		olText.Format("%d", imData);
	else
		olText = opBarText;

    LOGFONT logFont;
    memset(&logFont, 0, sizeof(LOGFONT));

	CFont olArial_7;

	char pclTmpText[512], pclConfigPath[512];
	BYTE llCharSet = DEFAULT_CHARSET;
	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "CHARSET", "DEFAULT", pclTmpText, sizeof pclTmpText, pclConfigPath);
	if(!strcmp(pclTmpText, "ARABIC"))
		llCharSet = ARABIC_CHARSET;

	logFont.lfCharSet= llCharSet;
    logFont.lfHeight = - MulDiv(7, ppDCD->GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Arial");
    olArial_7.CreateFontIndirect(&logFont);
	ppDCD->SelectObject(&olArial_7);


	CSize olSizeMinX = ppDCD->GetTextExtent("00000");
	int ilBarWidth;
	if (olSizeMinX.cx > pomGanttChartReport->GetGanttBarWidthPrint())
		ilBarWidth = olSizeMinX.cx;
	else
		ilBarWidth = pomGanttChartReport->GetGanttBarWidthPrint();

	int ilBeginnX = (int)(ipSpace + ilBarWidth * ipBarNo);

	// neue Seite?
    CRect olClipRect;
    ppDCD->GetClipBox(&olClipRect);
			                     // eine Balkenbreite abziehen, sonst zu dicht am re. Rand
	if (ilBeginnX > (olClipRect.right - ilBarWidth))
	{
		return false;
	}

	//Fix of PRF 8494	
	/*
	//              Anfang in x-richtung;         H�he des Balkens                           Ende in x-Richtung                               y-Punkt au x-Achse                           
	ppDCD->Rectangle(ilBeginnX, pomGanttChartReport->GetGanttYBeginnPrint() - ipHeight, ilBeginnX + ilBarWidth, pomGanttChartReport->GetGanttYBeginnPrint());
	

	// Text im Balken
	CSize olSize = ppDCD->GetTextExtent(olText);
	int ilX = ((ilBarWidth - olSize.cx) / 2) + ilBeginnX;
	int ilY = pomGanttChartReport->GetGanttYBeginnPrint() - ((ipHeight + olSize.cy) / 2);
	ppDCD->TextOut(ilX, ilY, olText);


	// Text unter der XAchse
	olSize = ppDCD->GetTextExtent(imXText);
	ilX = ((ilBarWidth - olSize.cx) / 2) + ilBeginnX;
	ilY = pomGanttChartReport->GetGanttYBeginnPrint() + (int)(1 * dpFactorY);
	ppDCD->TextOut(ilX, ilY, imXText);

	*/

	int ilDeparHeight,ilX,ilY;
	CString olArrivalText,olDepartureText ;
	CString olTextD,olTextA;

	//Creating arrival and departure brushes
	CBrush olDepBrush(RGB(240,230,140));
	CBrush olArrBrush(RGB(189,183,107));

	olArrivalText.Format("%d",pomGanttChartReport->imArrivalData);
	olDepartureText.Format("%d",pomGanttChartReport->imDeparture);
	CBrush *oldBrush = ppDCD->SelectObject(&olDepBrush);

	ppDCD->Rectangle(ilBeginnX-1,pomGanttChartReport->GetGanttYBeginnPrint() - ipHeight, 
		             ilBeginnX + ilBarWidth, 
					 (pomGanttChartReport->GetGanttYBeginnPrint() - pomGanttChartReport->imArrivalHeight));

	ppDCD->SelectObject(olArrBrush);
	ppDCD->Rectangle(ilBeginnX-1,(pomGanttChartReport->GetGanttYBeginnPrint() - pomGanttChartReport->imArrivalHeight-1), 
		             ilBeginnX + ilBarWidth, 
					 pomGanttChartReport->GetGanttYBeginnPrint());
	ppDCD->SelectObject(oldBrush);

	CSize olSize = ppDCD->GetTextExtent(olText);
	ilX = ((ilBarWidth - olSize.cx) / 2) + ilBeginnX;
	ilY = pomGanttChartReport->GetGanttYBeginnPrint() - ((pomGanttChartReport->imArrivalHeight + olSize.cy) / 2);
	
	olTextD = "A="+ olArrivalText;
	if(pomGanttChartReport->imArrivalHeight!=0)
	{
		ppDCD->TextOut(ilX, ilY, olTextD);
	}
	olSize = ppDCD->GetTextExtent(olText);

	
	ilDeparHeight = ipHeight - pomGanttChartReport->imArrivalHeight;
	ilX = ((ilBarWidth - olSize.cx) / 2) + ilBeginnX;
	ilY = (pomGanttChartReport->GetGanttYBeginnPrint() - pomGanttChartReport->imArrivalHeight)  - ((ilDeparHeight + olSize.cy) / 2);

	olTextA = "D="+ olDepartureText;
	if(pomGanttChartReport->imArrivalHeight!= ipHeight)
	{
		ppDCD->TextOut(ilX, ilY, olTextA);
	}
	ilX = ((ilBarWidth - olSize.cx) / 2) + ilBeginnX;
	ilY = pomGanttChartReport->GetGanttYBeginnPrint() - ((ipHeight + olSize.cy) / 2);
	
	if(ipHeight == 0)
	{
		ppDCD->TextOut(ilX, ilY, olText);
	}

	// Text unter der XAchse
	olSize = ppDCD->GetTextExtent(imXText);
	ilX = ((ilBarWidth - olSize.cx) / 2) + ilBeginnX;
	ilY = pomGanttChartReport->GetGanttYBeginnPrint() + 1;
	ppDCD->TextOut(ilX, ilY, imXText);

	return true;
}