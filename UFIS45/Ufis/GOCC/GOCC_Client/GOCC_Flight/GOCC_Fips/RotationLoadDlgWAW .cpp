// RotationLoadDlgWAW.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <fpms.h>
#include <CCSDdx.h>
#include <CCSTime.h>
#include <CCSGlobl.h>
#include <PrivList.h>
#include <CedaBasicData.h>
#include <utils.h>
#include <RotationLoadDlgWAW.h>
#include <BasicData.h>
#include <AwBasicDataDlg.h>
#include <CedaLoaTabData.h>
#include <RotationDlgCedaFlightData.h>
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void RotationLoadDlgWAWCf(void *popInstance, int ipDDXType, 
    void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
RotationLoadDlgWAW::RotationLoadDlgWAW(CWnd* pParent, CString opAdid, CString opFlno, long lpUrnoFlight, CString opListVia, CTime opRefDat, bool bpLocalTime, bool bpEnable, CString opDest)
	: CDialog(RotationLoadDlgWAW::IDD, pParent)
{
	//{{AFX_DATA_INIT(RotationVipDlg)
	//}}AFX_DATA_INIT

	pomParent = pParent;//GetParent()
    pomTable = new CCSTable;

	//enable flag for the table
	bmEnable = bpEnable;
	omSecState = '1';

	//fields and text for header in table
	CString olHeader = GetString(IDS_LOADTABLE);
	ExtractItemList(olHeader, &omFields, ',');

	//init members with flight
	SetViaList(opAdid, opFlno, lpUrnoFlight, opListVia, opRefDat, bpLocalTime, opDest);
}


RotationLoadDlgWAW::~RotationLoadDlgWAW()
{
	ogDdx.UnRegister(this, NOTUSED);
	delete pomTable;
}

void RotationLoadDlgWAW::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(RotationLoadDlgWAW)
	DDX_Control(pDX, IDC_GMBORDER, m_CS_GMBorder);//border for table
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(RotationLoadDlgWAW, CDialog)
	//{{AFX_MSG_MAP(RotationLoadDlgWAW)
    ON_MESSAGE(WM_TABLE_LBUTTONDOWN, OnTableLButtonDown)
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
	ON_MESSAGE(WM_TABLE_MENU_SELECT, OnTableMenuSelect)
	ON_MESSAGE(WM_TABLE_IPEDIT_KILLFOCUS, OnTableIPEditKillfocus)
	ON_MESSAGE(WM_EDIT_CHANGED, OnEditChanged)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_CHECK1, OnCheckTotal)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
/////////////////////////////////////////////////////////////////////////////
BOOL RotationLoadDlgWAW::OnInitDialog() 
{
	CDialog::OnInitDialog();

//	CButton *polButton = (CButton *)GetDlgItem(IDOK);
	CWnd* plWnd = GetDlgItem(IDOK);
	if(ogPrivList.GetStat("EDITLOAD_CB_OK") == '0')
	{
		if (plWnd) 
			plWnd->EnableWindow(false);
	}

	if(ogPrivList.GetStat("EDITLOAD_CB_OK") == '-')
	{
		if (plWnd) 
			plWnd->ShowWindow(SW_HIDE);
	}

	CButton* polCB = (CButton*) GetDlgItem(IDC_CHECK1);
	polCB->SetCheck(true);
	bmAutocalc = true;
	
	InitTable();
	ShowTable();

	return TRUE;
}

void RotationLoadDlgWAW::OnCheckTotal() 
{
	// TODO: Add your control notification handler code here
	if ( ((CButton*) GetDlgItem(IDC_CHECK1))->GetCheck() )
	{
		bmAutocalc = true;
		RecalcTotals();
	}
	else
		bmAutocalc = false;

	RecalcTotalColors();
}



void RotationLoadDlgWAW::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);


	this->Invalidate();
}


void RotationLoadDlgWAW::InitTable()
{
	//size of table
	CRect olRectBorder;
	m_CS_GMBorder.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);

	pomTable->SetHeaderSpacing(0);
    pomTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomTable->SetTableEditable(true);
	pomTable->SetIPEditModus(true);
	pomTable->SetSelectMode(false);
	pomTable->SetShowSelection(false);
//	pomTable->ResetContent();
//	pomTable->SetFieldNames(CString("VIA3,VIA4,Pxfirst,Pxbus,Pxeco,PXT,PADeco,Pxinf,TrPXT,TP,JumpSeat,StripMAIL,StripCGOT,StripBAGN,StripBAGW"));
	pomTable->SetFieldNames(CString("VIA3,VIA4,Pxfirst,Pxbus,Pxeco,Pxinf,PXT,PXT-MVT,PADeco,TrPXT,TP,JumpSeat,StripBAGN,StripCGOT,StripMAIL,StripBAGW"));

	DrawHeader();
	pomTable->DisplayTable();
}

void RotationLoadDlgWAW::DrawHeader()
{
	TABLE_HEADER_COLUMN* prlHeader[20];
	CCSPtrArray <TABLE_HEADER_COLUMN> olHeaderDataArray;

	for (int i= 0; i < omFields.GetSize(); i++)
	{
		prlHeader[i] = new TABLE_HEADER_COLUMN;
		prlHeader[i]->Alignment = COLALIGN_CENTER;
		prlHeader[i]->Length = 50; 
		prlHeader[i]->Font = &ogCourier_Bold_10;
		prlHeader[i]->Text = omFields.GetAt(i);

		olHeaderDataArray.Add(prlHeader[i]);
	}

	pomTable->SetHeaderFields(olHeaderDataArray);
	pomTable->SetDefaultSeparator();

	olHeaderDataArray.DeleteAll();
}

void RotationLoadDlgWAW::ShowTable()
{
	CCSEDIT_ATTRIB rlAttrib;
	TABLE_COLUMN   rlColumnData;
	CCSPtrArray<TABLE_COLUMN> olColList;
	CCSPtrArray<VIADATA>      olVias;

	//create VIADATA from AFT.VIAL
	ogBasicData.GetViaArray(&olVias, omViaList);

	//fill table up to 8 lines (not more possible because AFT.VIAL = 1024
	for (int i = olVias.GetSize(); i < 10; i++)
	{
		VIADATA *prlVia = new VIADATA;
		olVias.Add(prlVia);
	}

	//clear table
	pomTable->ResetContent();

	//column attributes
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogMS_Sans_Serif_8;
	rlColumnData.VerticalSeparator = SEPA_NORMAL;
	rlColumnData.HorizontalSeparator = SEPA_NONE;
	rlColumnData.Alignment = COLALIGN_CENTER;

	//disable edit in table if false
	if (bmEnable)
	{
		rlColumnData.blIsEditable = true;
		rlColumnData.BkColor = RGB(255,255,255);
	}
	else
	{
		rlColumnData.blIsEditable = false;
		rlColumnData.BkColor = RGB(192,192,192);
	}


	//create lines for table
	int ilCountCol = omFields.GetSize();
	int ilCountLine = olVias.GetSize();
	bool blFini = false;
	for (int  ilLc = 0; ilLc < ilCountLine ; ilLc++)
	{
		CString olApc3 = GetField(&olVias[ilLc], omFields[0]);
		olApc3.TrimRight();
		
//		if (!olApc3.IsEmpty())
		{
			if (olApc3.IsEmpty() && omAdid == "A")
				break;


			rlColumnData.Text = olApc3;
			GetAttrib(rlAttrib, omFields[0]);
			rlColumnData.EditAttrib = rlAttrib;
			rlColumnData.blIsEditable = false;
			rlColumnData.BkColor = RGB(192,192,192);
			olColList.New(rlColumnData);

			rlColumnData.Text = GetField(&olVias[ilLc], omFields[1]);
			GetAttrib(rlAttrib, omFields[1]);
			rlColumnData.EditAttrib = rlAttrib;
			rlColumnData.blIsEditable = false;
			rlColumnData.BkColor = RGB(192,192,192);
			olColList.New(rlColumnData);

			//disable edit in table if false
			if (bmEnable)
			{
				rlColumnData.blIsEditable = true;
				rlColumnData.BkColor = RGB(255,255,255);
			}
			else
			{
				rlColumnData.blIsEditable = false;
				rlColumnData.BkColor = RGB(192,192,192);
			}


			LOADATA* prlLoa = omLoaTabData.GetLoaWAW(CString("Pxfirst"),CString("LDM"),olApc3);
			if (prlLoa)
				rlColumnData.Text = prlLoa->Valu;
			else
				rlColumnData.Text = "";

			GetAttrib(rlAttrib, omFields[2]);
			rlColumnData.EditAttrib = rlAttrib;
			olColList.New(rlColumnData);

			prlLoa = omLoaTabData.GetLoaWAW(CString("Pxbus"),CString("LDM"),olApc3);
			if (prlLoa)
				rlColumnData.Text = prlLoa->Valu;
			else
				rlColumnData.Text = "";

			GetAttrib(rlAttrib, omFields[2]);
			rlColumnData.EditAttrib = rlAttrib;
			olColList.New(rlColumnData);

			prlLoa = omLoaTabData.GetLoaWAW(CString("Pxeco"),CString("LDM"),olApc3);
			if (prlLoa)
				rlColumnData.Text = prlLoa->Valu;
			else
				rlColumnData.Text = "";

			GetAttrib(rlAttrib, omFields[2]);
			rlColumnData.EditAttrib = rlAttrib;
			olColList.New(rlColumnData);

			prlLoa = omLoaTabData.GetLoaWAW(CString("Pxinf"),CString("LDM"),olApc3);
			if (prlLoa)
				rlColumnData.Text = prlLoa->Valu;
			else
				rlColumnData.Text = "";

			GetAttrib(rlAttrib, omFields[2]);
			rlColumnData.EditAttrib = rlAttrib;
			olColList.New(rlColumnData);

			prlLoa = omLoaTabData.GetLoaWAW(CString("PXT"),CString("LDM"),olApc3);
			if (prlLoa)
				rlColumnData.Text = prlLoa->Valu;
			else
				rlColumnData.Text = "";

			GetAttrib(rlAttrib, omFields[02]);
			rlColumnData.EditAttrib = rlAttrib;
			olColList.New(rlColumnData);



			prlLoa = omLoaTabData.GetLoaWAW(CString("PXT"),CString("MVT"),olApc3);
			if (prlLoa)
				rlColumnData.Text = prlLoa->Valu;
			else
				rlColumnData.Text = "";

			GetAttrib(rlAttrib, omFields[2]);
			rlColumnData.EditAttrib = rlAttrib;
			rlColumnData.blIsEditable = false;
			rlColumnData.BkColor = RGB(192,192,192);
			olColList.New(rlColumnData);

//-----
			if (bmEnable)
			{
				rlColumnData.blIsEditable = true;
				rlColumnData.BkColor = RGB(255,255,255);
			}
			else
			{
				rlColumnData.blIsEditable = false;
				rlColumnData.BkColor = RGB(192,192,192);
			}
//-----


			prlLoa = omLoaTabData.GetLoaWAW(CString("PADeco"),CString("LDM"),olApc3);
			if (prlLoa)
				rlColumnData.Text = prlLoa->Valu;
			else
				rlColumnData.Text = "";

			GetAttrib(rlAttrib, omFields[2]);
			rlColumnData.EditAttrib = rlAttrib;
			olColList.New(rlColumnData);

			prlLoa = omLoaTabData.GetLoaWAW(CString("TrPXT"),CString("LDM"),olApc3);
			if (prlLoa)
				rlColumnData.Text = prlLoa->Valu;
			else
				rlColumnData.Text = "";

			GetAttrib(rlAttrib, omFields[2]);
			rlColumnData.EditAttrib = rlAttrib;
			olColList.New(rlColumnData);

			prlLoa = omLoaTabData.GetLoaWAW(CString("TP"),CString("LDM"),olApc3);
			if (prlLoa)
				rlColumnData.Text = prlLoa->Valu;
			else
				rlColumnData.Text = "";

			GetAttrib(rlAttrib, omFields[2]);
			rlColumnData.EditAttrib = rlAttrib;
			olColList.New(rlColumnData);

			prlLoa = omLoaTabData.GetLoaWAW(CString("JumpSeat"),CString("LDM"),olApc3);
			if (prlLoa)
				rlColumnData.Text = prlLoa->Valu;
			else
				rlColumnData.Text = "";

			GetAttrib(rlAttrib, omFields[2]);
			rlColumnData.EditAttrib = rlAttrib;
			olColList.New(rlColumnData);


			prlLoa = omLoaTabData.GetLoaWAW(CString("StripBAGN"),CString("LDM"),olApc3);
			if (prlLoa)
				rlColumnData.Text = prlLoa->Valu;
			else
				rlColumnData.Text = "";

			GetAttrib(rlAttrib, omFields[2]);
			rlColumnData.EditAttrib = rlAttrib;
			olColList.New(rlColumnData);


			prlLoa = omLoaTabData.GetLoaWAW(CString("StripCGOT"),CString("LDM"),olApc3);
			if (prlLoa)
				rlColumnData.Text = prlLoa->Valu;
			else
				rlColumnData.Text = "";

			GetAttrib(rlAttrib, omFields[2]);
			rlColumnData.EditAttrib = rlAttrib;
			olColList.New(rlColumnData);


			prlLoa = omLoaTabData.GetLoaWAW(CString("StripMAIL"),CString("LDM"),olApc3);
			if (prlLoa)
				rlColumnData.Text = prlLoa->Valu;
			else
				rlColumnData.Text = "";

			GetAttrib(rlAttrib, omFields[2]);
			rlColumnData.EditAttrib = rlAttrib;
			olColList.New(rlColumnData);


			prlLoa = omLoaTabData.GetLoaWAW(CString("StripBAGW"),CString("LDM"),olApc3);
			if (prlLoa)
				rlColumnData.Text = prlLoa->Valu;
			else
				rlColumnData.Text = "";

			GetAttrib(rlAttrib, omFields[2]);
			rlColumnData.EditAttrib = rlAttrib;
			olColList.New(rlColumnData);

			pomTable->AddTextLine(olColList, (void*)(NULL));
			if (olApc3.IsEmpty())
				break;

  
		}
		olColList.DeleteAll();
	}

	//menuitem on first column(VIA) -> "List of Airports"
	pomTable->AddMenuItem( -1, 0, 0, 101, GetString(IDS_STRING1711));
	pomTable->AddMenuItem( -1, 1, 0, 102, GetString(IDS_STRING1711));

	//show table
	olVias.DeleteAll();
	pomTable->DisplayTable();

	//set windowtext for current flight
	if (omAdid == "A")
		omCaption.Format(GetString(IDS_LOAD_ARR), omFlno);
	else
		omCaption.Format(GetString(IDS_LOAD_DEP), omFlno);

	SetWindowText(omCaption);
	RecalcTotalColors();
}

void RotationLoadDlgWAW::SetRefDate(CTime opRefDat)
{
		omRefDat = opRefDat;
}

void RotationLoadDlgWAW::SetViaList(CString opAdid, CString opFlno, long lpUrnoFlight, CString opList, CTime opRefDat, bool bpLocalTime, CString opDest)
{
	bmLocal = bpLocalTime;
	CString olAddDest = " " + opDest + "                                                                                                                                                                                                                          ";
	olAddDest = olAddDest.Left(120);
	omViaList = olAddDest + opList;

	omRefDat = opRefDat;
	if(omRefDat == TIMENULL)
		omRefDat = CTime::GetCurrentTime();

	omAdid = opAdid;
	omFlno = opFlno;
	omUrnoFlight = lpUrnoFlight;

	// set user loa data
	if (omUrnoFlight > 0)
		omLoaTabData.Read(omUrnoFlight);  
}

void RotationLoadDlgWAW::SetData(CString opAdid, CString opFlno, long lpUrnoFlight, CString opList, CTime opRefDat, bool bpLocalTime , CString opDest)
{
	SetViaList(opAdid, opFlno, lpUrnoFlight, opList, opRefDat, bpLocalTime, opDest);
	ShowTable();
}

CString RotationLoadDlgWAW::GetToolTip()
{
	return "";
	CString olToolTip;
	for(int  i = 0; i < pomTable->GetLinesCount(); i++)
	{
		CString olTmp;
		CString olApc;
		CString olTime;
		for(int j = 0; j < omFields.GetSize(); j++)
		{
			pomTable->GetTextFieldValue(i, j, olTmp);
			if (omFields[j] == "VIA3")
			{
				if (olTmp.IsEmpty())
					continue;
				else
				{
					CString olApc4;
					pomTable->GetTextFieldValue(i, j+1, olApc4);
						 
					olApc = olTmp + "/" + olApc4 + " ";
				}
			}

			if (omFields[j] == "VIA4")
			{
				if (olTmp.IsEmpty())
					continue;
				else
				{
					CString olApc3;
					pomTable->GetTextFieldValue(i, j-1, olApc3);
						 
					olApc = olApc3 + "/" + olTmp + " ";
				}
			}

			if (omAdid == "A")
			{
				if (omFields[j] == "STA" && !olTmp.IsEmpty())
					olTime = omFields[j] + ":" + olTmp;
				if (omFields[j] == "ETA" && !olTmp.IsEmpty())
					olTime = omFields[j] + ":" + olTmp;
				if (omFields[j] == "ONBL" && !olTmp.IsEmpty())
					olTime = omFields[j] + ":" + olTmp;
				if (omFields[j] == "ATA" && !olTmp.IsEmpty())
					olTime = omFields[j] + ":" + olTmp;
				if (omFields[j] == "STD" && !olTmp.IsEmpty())
					olTime = omFields[j] + ":" + olTmp;
				if (omFields[j] == "ETD" && !olTmp.IsEmpty())
					olTime = omFields[j] + ":" + olTmp;
				if (omFields[j] == "OFBL" && !olTmp.IsEmpty())
					olTime = omFields[j] + ":" + olTmp;
				if (omFields[j] == "ATD" && !olTmp.IsEmpty())
					olTime = omFields[j] + ":" + olTmp;
			}
			if (omAdid == "D")
			{
				if (omFields[j] == "STD" && !olTmp.IsEmpty())
					olTime = omFields[j] + ":" + olTmp;
				if (omFields[j] == "ETD" && !olTmp.IsEmpty())
					olTime = omFields[j] + ":" + olTmp;
				if (omFields[j] == "OFBL" && !olTmp.IsEmpty())
					olTime = omFields[j] + ":" + olTmp;
				if (omFields[j] == "ATD" && !olTmp.IsEmpty())
					olTime = omFields[j] + ":" + olTmp;
				if (omFields[j] == "STA" && !olTmp.IsEmpty())
					olTime = omFields[j] + ":" + olTmp;
				if (omFields[j] == "ETA" && !olTmp.IsEmpty())
					olTime = omFields[j] + ":" + olTmp;
				if (omFields[j] == "ONBL" && !olTmp.IsEmpty())
					olTime = omFields[j] + ":" + olTmp;
				if (omFields[j] == "ATA" && !olTmp.IsEmpty())
					olTime = omFields[j] + ":" + olTmp;
			}
		}

		if (!olApc.IsEmpty())
				olToolTip += " -> " + olApc + " " + olTime;
	}

	return olToolTip;
}

CString RotationLoadDlgWAW::GetField(VIADATA *prpVia, CString opField)
{
	CString olRet;
	CTime olTime;

	if(prpVia == NULL)
		return olRet;

	if(opField == "VIA3")
		olRet = CString(prpVia->Apc3);
	else 
	if(opField == "VIA4")
	{
		olRet = CString(prpVia->Apc4);
//		if (olRet.IsEmpty())
//			olRet = CString(prpVia->Apc3);
	}

	return olRet;

	//else 
	if(opField == "STA")
	{
		olTime = prpVia->Stoa;
//		if(bmLocal) ogBasicData.UtcToLocal(olTime);
		if(bmLocal)
		{
			ogBasicData.UtcToLocal(olTime);
		}
		olRet = DateToHourDivString(olTime, omRefDat);
	}
	else 
	if(opField == "STD")
	{
		olTime = prpVia->Stod;
//		if(bmLocal) ogBasicData.UtcToLocal(olTime);
		if(bmLocal)
		{
			ogBasicData.UtcToLocal(olTime);
		}
		olRet = DateToHourDivString(olTime, omRefDat);
	}
	else 
	if(opField == "ETA")
	{
		olTime = prpVia->Etoa;
//		if(bmLocal) ogBasicData.UtcToLocal(olTime);
		if(bmLocal)
		{
			ogBasicData.UtcToLocal(olTime);
		}
		olRet = DateToHourDivString(olTime, omRefDat);
	}
	else 
	if(opField == "ONBL")
	{
		olTime = prpVia->Onbl;
//		if(bmLocal) ogBasicData.UtcToLocal(olTime);
		if(bmLocal)
		{
			ogBasicData.UtcToLocal(olTime);
		}
		olRet = DateToHourDivString(olTime, omRefDat);
	}
	else 
	if(opField == "ATA")
	{
		olTime = prpVia->Land;
//		if(bmLocal) ogBasicData.UtcToLocal(olTime);
		if(bmLocal)
		{
			ogBasicData.UtcToLocal(olTime);
		}
		olRet = DateToHourDivString(olTime, omRefDat);
	}
	else 
	if(opField == "ETD")
	{
		olTime = prpVia->Etod;
//		if(bmLocal) ogBasicData.UtcToLocal(olTime);
		if(bmLocal)
		{
			ogBasicData.UtcToLocal(olTime);
		}
		olRet = DateToHourDivString(olTime, omRefDat);
	}
	else 
	if(opField == "OFBL")
	{
		olTime = prpVia->Ofbl;
//		if(bmLocal) ogBasicData.UtcToLocal(olTime);
		if(bmLocal)
		{
			ogBasicData.UtcToLocal(olTime);
		}
		olRet = DateToHourDivString(olTime, omRefDat);
	}
	else 
	if(opField == "ATD")
	{
		olTime = prpVia->Airb;
//		if(bmLocal) ogBasicData.UtcToLocal(olTime);
		if(bmLocal)
		{
			ogBasicData.UtcToLocal(olTime);
		}
		olRet = DateToHourDivString(olTime, omRefDat);
	}
	else 
	if(opField == "TR")
		olRet = CString(prpVia->Fids);

	return olRet;
}

void RotationLoadDlgWAW::GetAttrib(CCSEDIT_ATTRIB &ropAttrib, CString opField)
{
	if(opField == "VIA4")
	{
		ropAttrib.Type = KT_STRING;
		ropAttrib.Format = "XXXX";
		ropAttrib.TextMinLenght = 0;
		ropAttrib.TextMaxLenght = 4;
		ropAttrib.Style = ES_UPPERCASE;
	}
	else 
	if(opField == "VIA3")
	{
		ropAttrib.Type = KT_STRING;
		ropAttrib.Format = "XXX";
		ropAttrib.TextMinLenght = 0;
		ropAttrib.TextMaxLenght = 3;
		ropAttrib.Style = ES_UPPERCASE;
	}
	else 
	if(opField == "TR")
	{
		ropAttrib.Type = KT_STRING;
		ropAttrib.Format = "X";
		ropAttrib.TextMinLenght = 0;
		ropAttrib.TextMaxLenght = 1;
	}
	else 
	{
		ropAttrib.Type = KT_INT;
//		ropAttrib.ChangeDay	= true;
		ropAttrib.TextMaxLenght = 9;
	}
}

LONG RotationLoadDlgWAW::OnTableMenuSelect( UINT wParam, LPARAM lParam)
{
	ASSERT(FALSE);
	CCSTABLENOTIFY *prlNotify = (CCSTABLENOTIFY *) lParam;

	if(!bmEnable)
		return 0L;

	if(wParam == 101)
	{
/*		CCSPtrArray<VIADATA> olVias;
		ogBasicData.GetViaArray(&olVias, omViaList);
		VIADATA *prlVia = NULL;

		//set preselection for apttab
		CString olSelect;
		if (prlNotify->Line < olVias.GetSize())
		{
			prlVia = &olVias[prlNotify->Line];
			if (prlVia)
				olSelect = GetField(prlVia, "VIA3");
		}
*/
		CString olSelect;
		pomTable->GetTextFieldValue(prlNotify->Line, prlNotify->Column, olSelect);

		AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "APT","APC3,APC4,APFN", "APFN+", olSelect);
		if(polDlg->DoModal() == IDOK)
		{
			pomTable->SetIPValue(prlNotify->Line, prlNotify->Column, polDlg->GetField("APC3"));
			pomTable->SetIPValue(prlNotify->Line, prlNotify->Column+1, polDlg->GetField("APC4"));
			pomTable->DisplayTable();
		}
		delete polDlg;
	}

	//list apttab
	if(wParam == 102)
	{
/*		CCSPtrArray<VIADATA> olVias;
		ogBasicData.GetViaArray(&olVias, omViaList);
		VIADATA *prlVia = NULL;

		//set preselection for apttab
		CString olSelect;
		if (prlNotify->Line < olVias.GetSize())
		{
			prlVia = &olVias[prlNotify->Line];
			if (prlVia)
				olSelect = GetField(prlVia, "VIA4");
		}
*/
		CString olSelect;
		pomTable->GetTextFieldValue(prlNotify->Line, prlNotify->Column, olSelect);

		AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "APT","APC4,APC3,APFN", "APFN+", olSelect);
		if(polDlg->DoModal() == IDOK)
		{
			CString olApc4 = polDlg->GetField("APC4");
			CString olApc3 = polDlg->GetField("APC3");
//			if (olApc.IsEmpty())
//				olApc = polDlg->GetField("APC3");

			pomTable->SetIPValue(prlNotify->Line, prlNotify->Column, olApc4);
			pomTable->SetIPValue(prlNotify->Line, prlNotify->Column-1, olApc3);
			//refresh table, otherwise the new entry will only shown on kill focus in column VIA
			pomTable->DisplayTable();
		}
		delete polDlg;
	}

	return TRUE;
}

void RotationLoadDlgWAW::RecalcbmAutocalc()
{
	int ilCount = pomTable->GetLinesCount();
	if (ilCount <= 0)
		return;

	pomTable->ChangeColumnColor(pomTable->GetLinesCount()-1, 7, SILVER, BLACK);
	if (bmAutocalc)
	{
		pomTable->SetColumnEditable(6, false);
		pomTable->SetColumnEditable(7, false);
		pomTable->SetColumnEditable(15, false);
		if (omAdid == "D")
		{
			for (int i = 0; i < omFields.GetSize(); i++)
			{
				pomTable->SetColumnEditable(pomTable->GetLinesCount()-1, i, false);
			}
		}
	}
	else
	{
		for (int i = 0; i < omFields.GetSize(); i++)
		{
			pomTable->SetColumnEditable(pomTable->GetLinesCount()-1, i, true);
		}
		pomTable->SetColumnEditable(6, true);
		pomTable->SetColumnEditable(7, false);
		pomTable->SetColumnEditable(15, true);
	}
}

void RotationLoadDlgWAW::RecalcTotals()
{
	for(int  i = 0; i < pomTable->GetLinesCount(); i++)
	{
		CString olTmp;
		CString olApc3;
		int TTLPAX = 0;
		int TTLLOAD = 0;
		//for(int j = 0; j < omFields.GetSize(); j++)
		{
			pomTable->GetTextFieldValue(i, "VIA3", olApc3);

			pomTable->GetTextFieldValue(i, "VIA4", olTmp);

			pomTable->GetTextFieldValue(i, "Pxfirst", olTmp);
			TTLPAX += atoi(olTmp);

			pomTable->GetTextFieldValue(i, "Pxbus", olTmp);
			TTLPAX += atoi(olTmp);

			pomTable->GetTextFieldValue(i, "Pxeco", olTmp);
			TTLPAX += atoi(olTmp);

			pomTable->GetTextFieldValue(i, "Pxinf", olTmp);
			TTLPAX += atoi(olTmp);
			
			char buffer[64];
			itoa(TTLPAX, buffer,10);
			CString olValu = CString(buffer);
			pomTable->SetTextFieldValue(i, 6, olValu);

			pomTable->GetTextFieldValue(i, "PXT-MVT", olTmp);

			pomTable->GetTextFieldValue(i, "PADeco", olTmp);

			pomTable->GetTextFieldValue(i, "TrPXT", olTmp);

			pomTable->GetTextFieldValue(i, "TP", olTmp);

			pomTable->GetTextFieldValue(i, "JumpSeat", olTmp);

			pomTable->GetTextFieldValue(i, "StripMAIL", olTmp);
			TTLLOAD += atoi(olTmp);

			pomTable->GetTextFieldValue(i, "StripCGOT", olTmp);
			TTLLOAD += atoi(olTmp);

			pomTable->GetTextFieldValue(i, "StripBAGN", olTmp);
			TTLLOAD += atoi(olTmp);

			itoa(TTLLOAD, buffer,10);
			olValu = CString(buffer);
			pomTable->SetTextFieldValue(i, 15, olValu);
		}
	}

	if (omAdid == "A")
		return;

	for(i = 0; i < omFields.GetSize(); i++)
	{
		CString olTmp;
		CString olApc3;
		int TTL = 0;
		//for(int j = 0; j < pomTable->GetLinesCount(); j++)
		{
			if (i < 2)
				continue;

			for(int j = 0; j < pomTable->GetLinesCount(); j++)
			{
				pomTable->GetTextFieldValue(j, "VIA3", olApc3);

				if (!olApc3.IsEmpty())
				{
					pomTable->GetTextFieldValue(j, i, olTmp);
					TTL += atoi(olTmp);
				}
				else
				{
					char buffer[64];
					itoa(TTL, buffer,10);
					CString olValu = CString(buffer);
					pomTable->SetTextFieldValue(j, i, olValu);
				}
			}

		}
	}
}

void RotationLoadDlgWAW::RecalcTotalColors()
{
	RecalcbmAutocalc();
	for(int  i = 0; i < pomTable->GetLinesCount(); i++)
	{
		CString olTmp;
		CString olApc3;
		int TTLPAX = 0;
		int TTLLOAD = 0;
		//for(int j = 0; j < omFields.GetSize(); j++)
		{
			pomTable->GetTextFieldValue(i, "VIA3", olApc3);

			pomTable->GetTextFieldValue(i, "VIA4", olTmp);

			pomTable->GetTextFieldValue(i, "Pxfirst", olTmp);
			TTLPAX += atoi(olTmp);

			pomTable->GetTextFieldValue(i, "Pxbus", olTmp);
			TTLPAX += atoi(olTmp);

			pomTable->GetTextFieldValue(i, "Pxeco", olTmp);
			TTLPAX += atoi(olTmp);

			pomTable->GetTextFieldValue(i, "Pxinf", olTmp);
			TTLPAX += atoi(olTmp);
			
			pomTable->GetTextFieldValue(i, "PXT", olTmp);
			int ilTot = atoi(olTmp);
			if (ilTot != TTLPAX)
				pomTable->ChangeColumnColor(i, 6, RED, WHITE);
			else
				pomTable->ChangeColumnColor(i, 6, WHITE, BLACK);



			pomTable->GetTextFieldValue(i, "PXT-MVT", olTmp);

			pomTable->GetTextFieldValue(i, "PADeco", olTmp);

			pomTable->GetTextFieldValue(i, "TrPXT", olTmp);

			pomTable->GetTextFieldValue(i, "TP", olTmp);

			pomTable->GetTextFieldValue(i, "JumpSeat", olTmp);

			pomTable->GetTextFieldValue(i, "StripMAIL", olTmp);
			TTLLOAD += atoi(olTmp);

			pomTable->GetTextFieldValue(i, "StripCGOT", olTmp);
			TTLLOAD += atoi(olTmp);

			pomTable->GetTextFieldValue(i, "StripBAGN", olTmp);
			TTLLOAD += atoi(olTmp);

			pomTable->GetTextFieldValue(i, "StripBAGW", olTmp);
			ilTot = atoi(olTmp);
			if (ilTot != TTLLOAD)
				pomTable->ChangeColumnColor(i, 15, RED, WHITE);
			else
				pomTable->ChangeColumnColor(i, 15, WHITE, BLACK);
		}
	}

	if (omAdid == "A")
		return;

	for(i = 0; i < omFields.GetSize(); i++)
	{
		CString olTmp;
		CString olApc3;
		int TTL = 0;
		//for(int j = 0; j < pomTable->GetLinesCount(); j++)
		{
			if (i < 2)
				continue;

			for(int j = 0; j < pomTable->GetLinesCount(); j++)
			{
				pomTable->GetTextFieldValue(j, "VIA3", olApc3);

				if (!olApc3.IsEmpty())
				{
					pomTable->GetTextFieldValue(j, i, olTmp);
					TTL += atoi(olTmp);
				}
				else
				{
					pomTable->GetTextFieldValue(j, i, olTmp);
					int ilTot = atoi(olTmp);
					if (ilTot != TTL)
						pomTable->ChangeColumnColor(j, i, RED, WHITE);
					else
						pomTable->ChangeColumnColor(j, i, WHITE, BLACK);
				}
			}

		}
	}
}

LONG RotationLoadDlgWAW::OnTableIPEditKillfocus( UINT wParam, LPARAM lParam)
{

	CCSIPEDITNOTIFY *prlNotify = (CCSIPEDITNOTIFY*)lParam;


	if (!bmAutocalc)
	{
	}
	else
	{
		RecalcTotals();
		pomTable->DisplayTable();
	}
	RecalcTotalColors();
	return 0L;





	if(prlNotify->Text.IsEmpty())
		return 0L;

	if (omFields[prlNotify->Column] == "TR")
		return 0L;

	CString olTmp; 
	CString olTmp2;

//	if(omFields[prlNotify->Line] == "VIA" && prlNotify->Text.GetLength() == 4)
	if(omFields[prlNotify->Column] == "VIA4")// && prlNotify->Text.GetLength() == 4)
	{
		prlNotify->UserStatus = true;
		if(prlNotify->Status = ogBCD.GetField("APT", "APC3", "APC4", prlNotify->Text, olTmp, olTmp2 ))
			prlNotify->Text = olTmp2;
		else
		{
			if(prlNotify->Status = ogBCD.GetField("APT", "APC3", "APC4", prlNotify->Text, olTmp, olTmp2 ))
				prlNotify->Text = olTmp;
		}
		pomTable->SetTextFieldValue(prlNotify->Line, prlNotify->Column-1, olTmp);
	}	
	if(omFields[prlNotify->Column] == "VIA3")// && prlNotify->Text.GetLength() == 3)
	{
		prlNotify->UserStatus = true;
		if(prlNotify->Status = ogBCD.GetField("APT", "APC3", "APC4", prlNotify->Text, olTmp, olTmp2 ))
			prlNotify->Text = olTmp;
		else
		{
			if(prlNotify->Status = ogBCD.GetField("APT", "APC3", "APC4", prlNotify->Text, olTmp, olTmp2 ))
				prlNotify->Text = olTmp2;
		}
		pomTable->SetTextFieldValue(prlNotify->Line, prlNotify->Column+1, olTmp2);
	}	

	if(omFields[prlNotify->Column] != "VIA3" && omFields[prlNotify->Column] != "VIA4")
	{
		prlNotify->UserStatus = true;
		CTime olTime = HourStringToDate(prlNotify->Text, TIMENULL); 
		if (olTime == TIMENULL)
			prlNotify->Status = false;
	}

	if (!prlNotify->Status)
	{
		if(omFields[prlNotify->Column] == "VIA3")
		{
//			MessageBox(GetString(IDS_INV_APC), GetString(ST_FEHLER));
			return 0L;
		}

		if(omFields[prlNotify->Column] == "VIA4")
		{
//			MessageBox(GetString(IDS_INV_APC), GetString(ST_FEHLER));
			return 0L;
		}

//		MessageBox(GetString(IDS_INV_TIME), GetString(ST_FEHLER));
	}

	return 0L;
}

LONG RotationLoadDlgWAW::OnEditChanged( UINT wParam, LPARAM lParam)
{
	return 0L;

	if (!bmAutocalc)
		return 0L;
	else
	{
		RecalcTotals();
		pomTable->DisplayTable();

		return 0L;
	}
	//send parent that someting was changed
	//return pomParent->SendMessage(WM_EDIT_CHANGED, wParam, lParam);
		return 0L;

}

void RotationLoadDlgWAW::OnCancel() 
{
	//init table with old data
	ShowTable();
	ShowWindow(SW_HIDE);
}

void RotationLoadDlgWAW::OnOK() 
{
	//set new memberdata
	//CString opAMess = GetStatus();
	//char pcVial[1026];
	//GetViaList(pcVial);
	if (bmAutocalc)
		RecalcTotals();
	StoreLoaData();
	//omViaList = CString(pcVial);
	ShowTable();
	ShowWindow(SW_HIDE);
}

void RotationLoadDlgWAW::StoreLoaData() 
{
	bmWakeUpFlight = false;
	for(int  i = 0; i < pomTable->GetLinesCount(); i++)
	{
		CString olTmp;
		CString olApc3;
		//for(int j = 0; j < omFields.GetSize(); j++)
		{
			pomTable->GetTextFieldValue(i, "VIA3", olApc3);

			if (olApc3.IsEmpty() && omAdid == "A")
				break;


			pomTable->GetTextFieldValue(i, "Pxfirst", olTmp);
			SaveInApxTab(CString("Pxfirst"), olTmp, olApc3);

			pomTable->GetTextFieldValue(i, "Pxbus", olTmp);
			SaveInApxTab(CString("Pxbus"), olTmp, olApc3);

			pomTable->GetTextFieldValue(i, "Pxeco", olTmp);
			SaveInApxTab(CString("Pxeco"), olTmp, olApc3);

			pomTable->GetTextFieldValue(i, "Pxinf", olTmp);
			SaveInApxTab(CString("Pxinf"), olTmp, olApc3);

			pomTable->GetTextFieldValue(i, "PXT", olTmp);
			SaveInApxTab(CString("PXT"), olTmp, olApc3);

			pomTable->GetTextFieldValue(i, "PADeco", olTmp);
			SaveInApxTab(CString("PADeco"), olTmp, olApc3);

			pomTable->GetTextFieldValue(i, "TrPXT", olTmp);
			SaveInApxTab(CString("TrPXT"), olTmp, olApc3);

			pomTable->GetTextFieldValue(i, "TP", olTmp);
			SaveInApxTab(CString("TP"), olTmp, olApc3);

			pomTable->GetTextFieldValue(i, "JumpSeat", olTmp);
			SaveInApxTab(CString("JumpSeat"), olTmp, olApc3);

			pomTable->GetTextFieldValue(i, "StripMAIL", olTmp);
			SaveInApxTab(CString("StripMAIL"), olTmp, olApc3);

			pomTable->GetTextFieldValue(i, "StripCGOT", olTmp);
			SaveInApxTab(CString("StripCGOT"), olTmp, olApc3);

			pomTable->GetTextFieldValue(i, "StripBAGN", olTmp);
			SaveInApxTab(CString("StripBAGN"), olTmp, olApc3);

			pomTable->GetTextFieldValue(i, "StripBAGW", olTmp);
			SaveInApxTab(CString("StripBAGW"), olTmp, olApc3);
		}

		if (olApc3.IsEmpty())
			break;
	}


	if (bmWakeUpFlight)
		ogRotationDlgFlights.WakeUpFlight("GHS",omUrnoFlight);

}


BOOL RotationLoadDlgWAW::SaveInApxTab(CString &ropId, CString &ropData, CString &ropApc3) 
{
	LOADATA *prlApx = omLoaTabData.GetLoaWAW(ropId,CString("LDM"),ropApc3);

	if(prlApx != NULL)
	{
		if (prlApx->Valu == ropData)
		{
			prlApx->IsChanged = DATA_UNCHANGED;
		}
		else
		{
			prlApx->IsChanged = DATA_CHANGED;
			bmWakeUpFlight = true;
		}

		strcpy(prlApx->Apc3,ropApc3);
		omLoaTabData.SetLoaDataWAW(prlApx, ropId, ropData, omUrnoFlight, ropApc3);
		omLoaTabData.Save(prlApx);
	}
	else
	{
		//if(!ropData.IsEmpty())
		{
			bmWakeUpFlight = true;
			prlApx = new LOADATA;
			prlApx->IsChanged = DATA_NEW;
			strcpy(prlApx->Apc3,ropApc3);
			omLoaTabData.SetLoaDataWAW(prlApx, ropId, ropData, omUrnoFlight, ropApc3);
			omLoaTabData.Save(prlApx);
		}
	}

	return true;
}


LONG RotationLoadDlgWAW::OnTableLButtonDown(UINT wParam, LONG lParam)
{
//	CCSTABLENOTIFY *prlNotify = (CCSTABLENOTIFY*)lParam;
//	pomTable->SelectLine(prlNotify->Line);
	return -1L;
}

LONG RotationLoadDlgWAW::OnTableLButtonDblclk( UINT wParam, LPARAM lParam)
{
	return 0L;
}

static void RotationLoadDlgWAWCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    RotationLoadDlgWAW *polDlg = (RotationLoadDlgWAW *)popInstance;
}

void RotationLoadDlgWAW::SetField(VIADATA *prpVia, CString opField, CString opValue, CTime opRefDat)
{
	ASSERT(FALSE);
	return ;
	CString olApc4;
	CString olApc3;
	CTime olTime;

	if (opRefDat == TIMENULL)
		opRefDat = omRefDat;

	if(prpVia == NULL)
		return; 

	if((opField == "VIA4"))
	{
		strcpy(prpVia->Apc4, opValue);	

/*		if(ogBCD.GetField("APT", "APC4", "APC3", opValue, olApc4, olApc3 ))
		{
			strcpy(prpVia->Apc3, olApc3);	
			strcpy(prpVia->Apc4, olApc4);	
		}
		else
		{
			strcpy(prpVia->Apc3, "");	
			strcpy(prpVia->Apc4, "");	
		}
*/
	}	
	else
	if((opField == "VIA3"))
	{
		strcpy(prpVia->Apc3, opValue);	
/*
		if(ogBCD.GetField("APT", "APC3", "APC4", opValue, olApc3, olApc4 ))
		{
			strcpy(prpVia->Apc3, olApc3);	
			strcpy(prpVia->Apc4, olApc4);	
		}
		else
		{
			strcpy(prpVia->Apc3, "");	
			strcpy(prpVia->Apc4, "");	
		}
*/
	}	
	else 
	if(opField == "STA")
	{
		olTime = HourStringToDate(opValue, opRefDat); 
//		if(bmLocal) ogBasicData.LocalToUtc(olTime);
		if(bmLocal)
		{
				ogBasicData.LocalToUtc(olTime);
		}
		prpVia->Stoa = olTime;
	}
	else 
	if(opField == "STD")
	{
		olTime = HourStringToDate(opValue, opRefDat); 
//		if(bmLocal) ogBasicData.LocalToUtc(olTime);
		if(bmLocal)
		{
				ogBasicData.LocalToUtc(olTime);
		}
		prpVia->Stod = olTime;
	}
	else 
	if(opField == "ETA")
	{
		olTime = HourStringToDate(opValue, opRefDat); 
//		if(bmLocal) ogBasicData.LocalToUtc(olTime);
		if(bmLocal)
		{
				ogBasicData.LocalToUtc(olTime);
		}
		prpVia->Etoa = olTime;
	}
	else 
	if(opField == "ONBL")
	{
		olTime = HourStringToDate(opValue, opRefDat); 
//		if(bmLocal) ogBasicData.LocalToUtc(olTime);
		if(bmLocal)
		{
				ogBasicData.LocalToUtc(olTime);
		}
		prpVia->Onbl = olTime;
	}
	else 
	if(opField == "ATA")
	{
		olTime = HourStringToDate(opValue, opRefDat); 
//		if(bmLocal) ogBasicData.LocalToUtc(olTime);
		if(bmLocal)
		{
				ogBasicData.LocalToUtc(olTime);
		}
		prpVia->Land = olTime;
	}
	else 
	if(opField == "ETD")
	{
		olTime = HourStringToDate(opValue, opRefDat); 
//		if(bmLocal) ogBasicData.LocalToUtc(olTime);
		if(bmLocal)
		{
				ogBasicData.LocalToUtc(olTime);
		}
		prpVia->Etod = olTime;
	}
	else 
	if(opField == "OFBL")
	{
		olTime = HourStringToDate(opValue, opRefDat); 
//		if(bmLocal) ogBasicData.LocalToUtc(olTime);
		if(bmLocal)
		{
				ogBasicData.LocalToUtc(olTime);
		}
		prpVia->Ofbl = olTime;
	}
	else 
	if(opField == "ATD")
	{
		olTime = HourStringToDate(opValue, opRefDat); 
//		if(bmLocal) ogBasicData.LocalToUtc(olTime);
		if(bmLocal)
		{
				ogBasicData.LocalToUtc(olTime);
		}
		prpVia->Airb = olTime;
	}
	else 
	if(opField == "TR")
		strcpy(prpVia->Fids, opValue);	
}

void RotationLoadDlgWAW::GetViaList(char *pcpViaList)
{
	ASSERT(FALSE);
	return ;
	CCSPtrArray<VIADATA> olNewVias;
	VIADATA *prlVia;

	for(int  i = 0; i < pomTable->GetLinesCount(); i++)
	{
		prlVia = new VIADATA;
		olNewVias.Add(prlVia);

		CString olTmp;
		for(int j = 0; j < omFields.GetSize(); j++)
		{
			pomTable->GetTextFieldValue(i, j, olTmp);
			SetField(prlVia , omFields[j], olTmp);
		}
	}

	CString olViaList = CreateViaList(&olNewVias);
	strncpy(pcpViaList, olViaList, 1024);

	olNewVias.DeleteAll();
}

CString RotationLoadDlgWAW::CreateViaList(CCSPtrArray<VIADATA> *popVias, CTime opRefDat)
{
	ASSERT(FALSE);
	return "";

	CString olVia;
	CString olFids;
	CString olApc3;
	CString olApc4;
	CString	olTmp;
	CString	olTmp2;

	VIADATA *prlVia;

	for(int i = 0; i < popVias->GetSize(); i++)
	{
		prlVia = &(*popVias)[i];


		olApc3 = CString(prlVia->Apc3);
		olApc4 = CString(prlVia->Apc4);
		olFids = CString(prlVia->Fids);


		if(olApc3.GetLength() != 3)
			olApc3 = "   ";

		if(olApc4.GetLength() != 4)
			olApc4 = "    ";

		if(olFids.GetLength() != 1)
			olFids = " ";


		olTmp  =	olFids  +
					olApc3  +
					olApc4  +
					CTimeToDBString(prlVia->Stoa, TIMENULL) +  
					CTimeToDBString(prlVia->Etoa, TIMENULL) +  
					CTimeToDBString(prlVia->Land, TIMENULL) +  
					CTimeToDBString(prlVia->Onbl, TIMENULL) +  
					CTimeToDBString(prlVia->Stod, TIMENULL) + 
					CTimeToDBString(prlVia->Etod, TIMENULL) +  
					CTimeToDBString(prlVia->Ofbl, TIMENULL) +  
					CTimeToDBString(prlVia->Airb, TIMENULL); 
//					CTimeToDBString(prlVia->Airb, TIMENULL) +  
//					CTimeToDBString(prlVia->Ofbl, TIMENULL); 

		olTmp2 = olTmp;
		olTmp2.TrimRight();
		if(!olTmp2.IsEmpty())
			olVia += olTmp;

	}

	return olVia;
}

CString  RotationLoadDlgWAW::GetStatus()
{
	CString olRet;
	CString olTmp;

	int ilLines = pomTable->GetLinesCount();
	bool blRet;
	int ilColumn = 0;
	for(int  i = 0; i < ilLines; i++)
	{
		blRet = true;
		ilColumn = 0;

		while(blRet)
		{
			if((blRet = pomTable->GetTextFieldValue(i, ilColumn, olTmp)))
			{
				if(!pomTable->GetCellStatus(i, ilColumn))
				{
					olRet = GetString(IDS_STRING299); // "VIA"
					return olRet;
				}
			}
			ilColumn++;
		}
	}

	return olRet;
}

void RotationLoadDlgWAW::Enable( bool bpEnable )
{
	if (omSecState != '1' && bpEnable == true)
		return;

	if (bmEnable == bpEnable)
		return;

	bmEnable = bpEnable;
	if (pomTable) 
	{
		pomTable->SetTableEditable(bmEnable);
		pomTable->SetIPEditModus(bmEnable);
		ShowTable();
	}
}

void RotationLoadDlgWAW::SetSecState(char opSecState)
{
	omSecState = opSecState;
	if (opSecState != '1')
	{
		bmEnable = true;
		Enable(false); 
	}
	else
	{
		bmEnable = false;
		Enable(true); 
	}
}
