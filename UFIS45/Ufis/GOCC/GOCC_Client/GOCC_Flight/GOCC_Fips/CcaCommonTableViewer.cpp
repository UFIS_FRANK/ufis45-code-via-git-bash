// CcaCommonTableViewer.cpp 
//
//	Statistik Liste aller Abfl�ge pro LVG
#include <stdafx.h>
#include <CedaBasicData.h>
#include <CedaCcaData.h>
#include <CcaCommonTableViewer.h>
#include <CcsGlobl.h>
#include <CcsDdx.h>
#include <BasicData.h>
#include <DataSet.h>

#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include <resrc1.h> 


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

// Local function prototype
static void CheckInTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
static int CompareToCkbs(const CCADATA **e1, const CCADATA **e2);

//-----------------------------------------------------------------------------------------------

// CcaCommonTableViewer
//
CcaCommonTableViewer::CcaCommonTableViewer():
	omCcaData(CCA_COMTABLE_NEW, CCA_COMTABLE_CHANGE, CCA_COMTABLE_DELETE)
{

	omCcaData.lmBaseID = IDM_COMMONCCA;	
	omCcaData.SetFilter(true, "");
	omCcaData.Register();


	bmIsFromSearch = FALSE;
    pomCheckInTable = NULL;
	imLineno = 0;
    ogDdx.Register(this, CCA_COMTABLE_CHANGE, CString("CcaCommonTableViewer"), CString("CheckIn Update"), CheckInTableCf);
    ogDdx.Register(this, CCA_COMTABLE_NEW,    CString("CcaCommonTableViewer"), CString("CheckIn New"),    CheckInTableCf);
    ogDdx.Register(this, CCA_COMTABLE_DELETE, CString("CcaCommonTableViewer"), CString("CheckIn Delete"), CheckInTableCf);
}

//-----------------------------------------------------------------------------------------------

CcaCommonTableViewer::~CcaCommonTableViewer()
{
	ClearAll();
	ogDdx.UnRegister(this, NOTUSED);
}



void CcaCommonTableViewer::ClearAll()
{
	omCcaData.UnRegister();	
	omCcaData.ClearAll();

    DeleteAll();
	omHeaderDataArray.DeleteAll();
}


void CcaCommonTableViewer::ReadAllFlights(const CString &ropWhere)
{

	omCcaData.ReadAllFlights(ropWhere);
	omCcaData.Register();
	omCcaData.omData.Sort(CompareToCkbs);
	ChangeViewTo("<Default>");

}


CCADATA *CcaCommonTableViewer::GetCcaByUrno(long lpCcaUrno)
{
	return omCcaData.GetCcaByUrno(lpCcaUrno);
}


CCADATA *CcaCommonTableViewer::GetCcaByLineNo(int ipLineNo)
{
	if (ipLineNo < omLines.GetSize())
		return GetCcaByUrno(omLines[ipLineNo].Urno);
	else
		return NULL;
}


bool CcaCommonTableViewer::DeleteCca(long lpCcaUrno)
{
	if (!ogDataSet.DeAssignCca(lpCcaUrno))
		return omCcaData.Delete(lpCcaUrno);

	return true;
}


//-----------------------------------------------------------------------------------------------


void CcaCommonTableViewer::Attach(CCSTable *popTable)
{
    pomCheckInTable = popTable;
}

//-----------------------------------------------------------------------------------------------
int CcaCommonTableViewer::CompareCheckIn(CHECKINTABLE_LINEDATA *prpCheckIn1, CHECKINTABLE_LINEDATA *prpCheckIn2)
{
	return 0;
}

//-----------------------------------------------------------------------------------------------
void CcaCommonTableViewer::ChangeViewTo(const char *pcpViewName)
{
	DeleteAll();   
	

	if(!strcmp(pcpViewName, "<Default>"))
	{
		MakeLines();
	}
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------



//-----------------------------------------------------------------------------------------------
void CcaCommonTableViewer::MakeLines()
{
	int ilCount = omCcaData.omData.GetSize();

	CCADATA *prlCheckInData;

	for (int ilLc = 0; ilLc <= ilCount - 1 ; ilLc++)
	{
		prlCheckInData = &(omCcaData.omData[ilLc]);	

		MakeLine(prlCheckInData);
	}
}

//-----------------------------------------------------------------------------------------------
// neue Destination
void CcaCommonTableViewer::MakeLine(CCADATA *prlCheckInData)
{
	char clTmp[20];
	CString olAlc;
	CHECKINTABLE_LINEDATA rlCheckIn;

	rlCheckIn.Urno = prlCheckInData->Urno;

	if (bgCnamAtr)
		rlCheckIn.Ckic = GetCnamExt(CString(prlCheckInData->Ckic), CString(""));
	else
		rlCheckIn.Ckic = CString(prlCheckInData->Ckic);

	rlCheckIn.Ckit = CString(prlCheckInData->Ckit); 
	sprintf(clTmp,"%ld",prlCheckInData->Flnu);
	
	ogBCD.GetField("ALT", "URNO", clTmp, "ALC2", olAlc);

	rlCheckIn.Alc = olAlc; 

	rlCheckIn.Ckbs = prlCheckInData->Ckbs; 
	rlCheckIn.Ckes = prlCheckInData->Ckes; 
	
	if(bgGatPosLocal)
	{
		ogBasicData.UtcToLocal(rlCheckIn.Ckbs);
		ogBasicData.UtcToLocal(rlCheckIn.Ckes);
	}
	
	rlCheckIn.Rem1 = CString(prlCheckInData->Rema); 
	rlCheckIn.Ctyp = CString(prlCheckInData->Ctyp); 
	rlCheckIn.Disp = CString(prlCheckInData->Disp); 
		
	CreateLine(&rlCheckIn);
}


//-----------------------------------------------------------------------------------------------

void CcaCommonTableViewer::CreateLine(CHECKINTABLE_LINEDATA *prpCheckIn)
{

    int ilLineCount = omLines.GetSize();

    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareCCA(prpCheckIn, &omLines[ilLineno-1]) >= 0)
            break;  

    omLines.NewAt(ilLineno, *prpCheckIn);
}



int CcaCommonTableViewer::CompareCCA(CHECKINTABLE_LINEDATA *prpLine1, CHECKINTABLE_LINEDATA *prpLine2)
{
	int	ilCompareResult;

	if(prpLine1->Ckic == prpLine2->Ckic)
	{
		ilCompareResult = (prpLine1->Ckbs == prpLine2->Ckbs)? 0:	(prpLine1->Ckbs > prpLine2->Ckbs)? 1: -1;
	}
	else
	{
		ilCompareResult = (prpLine1->Ckic > prpLine2->Ckic)? 1: -1;
	}


	return ilCompareResult;
}










//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void CcaCommonTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 2;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();
 
	pomCheckInTable->SetShowSelection(TRUE);
	pomCheckInTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;
	rlHeader.Length = 100; 
	rlHeader.Text = GetString(IDS_STRING2299);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 100; 
	rlHeader.Text = GetString(ST_MA_NAME);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 15;
	rlHeader.Text = GetString(IDS_STRING1001);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 25;
	rlHeader.Text = GetString(IDS_STRING1529);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 120;
	rlHeader.Text = GetString(HD_DSR_AVFA_TIME);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 120;
	rlHeader.Text = GetString(HD_DSR_AVTA_TIME);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 240;
	rlHeader.Text = GetString(ST_BEMERKUNG);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 240;
	rlHeader.Text = GetString(IDS_DISP);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomCheckInTable->SetHeaderFields(omHeaderDataArray);


	pomCheckInTable->SetDefaultSeparator();
	pomCheckInTable->SetTableEditable(FALSE);

	pomCheckInTable->DisplayTable();


	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	

    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		if (omLines[ilLineNo].Ctyp == "C")
		{
			rlColumnData.Text = GetString(IDS_STRING2297);
			olColList.NewAt(olColList.GetSize(), rlColumnData);
		}
		else
		{
			rlColumnData.Text = GetString(IDS_STRING2298);
			olColList.NewAt(olColList.GetSize(), rlColumnData);
		}

		rlColumnData.Text = omLines[ilLineNo].Ckic;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Ckit;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Alc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Ckbs.Format("%d.%m.%y %H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Ckes.Format("%d.%m.%y %H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Rem1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Disp;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomCheckInTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomCheckInTable->DisplayTable();
}


//-----------------------------------------------------------------------------------------------

void CcaCommonTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool CcaCommonTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

static void CheckInTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    CcaCommonTableViewer *polViewer = (CcaCommonTableViewer *)popInstance;
    if (ipDDXType == CCA_COMTABLE_CHANGE || ipDDXType == CCA_COMTABLE_NEW) polViewer->ProcessCheckInChange((CCADATA *)vpDataPointer);
    if (ipDDXType == CCA_COMTABLE_DELETE) polViewer->ProcessCheckInDelete((CCADATA *)vpDataPointer);
} 

//-----------------------------------------------------------------------------------------------

void CcaCommonTableViewer::ProcessCheckInChange(CCADATA *prpCheckInData)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumnData;
	CCSPtrArray <TABLE_COLUMN> olColList;

	rlColumnData.Lineno = ilItem;
	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.Alignment = COLALIGN_LEFT;

		if (strcmp(prpCheckInData->Ctyp, "C") == 0)
		{
			rlColumnData.Text = GetString(IDS_STRING2297);
			olColList.NewAt(olColList.GetSize(), rlColumnData);
		}
		else
		{
			rlColumnData.Text = GetString(IDS_STRING2298);
			olColList.NewAt(olColList.GetSize(), rlColumnData);
		}



	rlColumnData.Text = prpCheckInData->Ckic;
	olColList.NewAt(olColList.GetSize(), rlColumnData);
	rlColumnData.Text = prpCheckInData->Ckit;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	CString olAlc;
	char clTmp[20];

	sprintf(clTmp,"%d",prpCheckInData->Flnu);
	bool blRet = ogBCD.GetField("ALT", "URNO", clTmp, "ALC2", olAlc);
	if(blRet == false)
		blRet = ogBCD.GetField("ALT", "URNO", clTmp, "ALC3", olAlc);
	rlColumnData.Text = olAlc; 
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	CTime olTime;
	olTime = prpCheckInData->Ckbs;
	if(bgGatPosLocal)
		ogBasicData.UtcToLocal(olTime);
	rlColumnData.Text = olTime.Format("%d.%m.%y %H:%M");
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	olTime = prpCheckInData->Ckes;
	if(bgGatPosLocal)
		ogBasicData.UtcToLocal(olTime);
	rlColumnData.Text = olTime.Format("%d.%m.%y %H:%M");
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpCheckInData->Rema;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Text = prpCheckInData->Disp;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	if (FindLine(prpCheckInData->Urno, ilItem))
	{
        CHECKINTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Ctyp = prpCheckInData->Ctyp;
		prlLine->Ckic = prpCheckInData->Ckic;
		prlLine->Ckit = prpCheckInData->Ckit;
		prlLine->Alc  = olAlc;
		prlLine->Ckbs = prpCheckInData->Ckbs;
		prlLine->Ckes = prpCheckInData->Ckes;
		prlLine->Rem1 = prpCheckInData->Rema;
		prlLine->Disp = prpCheckInData->Disp;

		pomCheckInTable->ChangeTextLine(ilItem, &olColList, (void *)prlLine);
		pomCheckInTable->DisplayTable();
	}
	else
	{
		MakeLine(prpCheckInData);
		if (FindLine(prpCheckInData->Urno, ilItem))
		{
	        CHECKINTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomCheckInTable->InsertTextLine(ilItem, olColList, (void *)prlLine);
				pomCheckInTable->DisplayTable();
			}
		}
	}
	olColList.DeleteAll();
}


//-----------------------------------------------------------------------------------------------

void CcaCommonTableViewer::ProcessCheckInDelete(CCADATA *prlCheckInData)
{
	int ilItem;
	if (FindLine(prlCheckInData->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomCheckInTable->DeleteTextLine(ilItem);
		pomCheckInTable->DisplayTable();
	}
}
//-----------------------------------------------------------------------------------------------

bool CcaCommonTableViewer::IsPassFilter(CCADATA *prlCheckInData)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

void CcaCommonTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}


//-----------------------------------------------------------------------------------------------
static int CompareToCkbs(const CCADATA **e1, const CCADATA **e2)
{
	return ((**e1).Ckbs >  (**e2).Ckbs)? 1: -1;
}
