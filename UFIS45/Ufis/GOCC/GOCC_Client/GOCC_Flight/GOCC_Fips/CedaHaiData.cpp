
 
#include <stdafx.h>
#include <afxwin.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CedaHaiData.h>
#include <BasicData.h>


 
CedaHaiData::CedaHaiData()
{
	// Create an array of CEDARECINFO for HAIDATA
	BEGIN_CEDARECINFO(HAIDATA,HaiDataRecInfo)
		CCS_FIELD_LONG(Flnu,"FLNU", "Eindeutige Datensatz-Nr.", 1)
		CCS_FIELD_LONG(Urno,"URNO", "Eindeutige Datensatz-Nr.", 1)
	 	CCS_FIELD_CHAR_TRIM(Hsna,"HSNA", "Handling Agent", 1)
		CCS_FIELD_CHAR_TRIM(Task,"TASK", "Tasks", 1)
		CCS_FIELD_CHAR_TRIM(Rema,"REMA", "Remarks", 1)
	END_CEDARECINFO //(HAIDATA)

	// Copy the record structure
	for (int i=0; i< sizeof(HaiDataRecInfo)/sizeof(HaiDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&HaiDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"HAI");
	strcpy(pcmFList,"FLNU,URNO,HSNA,TASK,REMA");
	pcmFieldList = pcmFList;


}; // end Constructor



CedaHaiData::~CedaHaiData()
{
	Clear();
	omRecInfo.DeleteAll();
}


void CedaHaiData::Clear()
{
	omUrnoMap.RemoveAll();
	omData.DeleteAll();

	POSITION pos;
	CCSPtrArray<HAIDATA> *prlArray;
	void  *pVoid;
	for( pos = omFlnuMap.GetStartPosition(); pos != NULL; )
	{
		omFlnuMap.GetNextAssoc( pos, pVoid, (void *&)prlArray );
		prlArray->RemoveAll();
		delete prlArray;
	}
	omFlnuMap.RemoveAll();

}


CString CedaHaiData::GetHsna(long lpFlightUrno, int ipNo) const
{

	CCSPtrArray<HAIDATA> *prlArray;

	if(omFlnuMap.Lookup((void *)lpFlightUrno, (void *& )prlArray) == TRUE)
	{
		if (prlArray->GetSize() > ipNo)
		{
			return CString((*prlArray)[ipNo].Hsna);
		}
	}

	return CString("");
}


bool CedaHaiData::ReadFlnus(const CString &ropFlnus) {
	CString olSelection;
	CStringArray olUrnosLists;
	bool blRc;
	bool blRet = false;

	Clear();

	// Split urnolist in 100 pieces blocks
	for(int i = SplitItemList(ropFlnus, &olUrnosLists, 100) - 1; i >= 0; i--)
	{
		olSelection.Format("WHERE FLNU IN (%s) ORDER BY HSNA", olUrnosLists[i]);
		// call CEDA
		blRet = CedaAction("RT", olSelection.GetBuffer(0));
 		blRc = blRet;
		// Read in internal buffer
		while (blRc == true)
		{
			HAIDATA *prlHai = new HAIDATA;
			if ((blRc = GetFirstBufferRecord(prlHai)) == true)
			{
 				InsertInternal(prlHai);
			}
			else 
			{
				delete prlHai;
			}
		}

	} 
	TRACE("CedaHaiData::ReadFlnus: Result: %ld records read!\n", omData.GetSize());

    return blRet;
}


bool CedaHaiData::UpdateSpecialRELHDL(const CPtrArray &ropFlightUrnos, const CPtrArray &ropRecNums, const CStringArray &ropHSNAs, const CStringArray &ropTASKs, const CStringArray &ropREMAs)
{

	CCSPtrArray<HAIDATA> *prlArray;
	CString olDelUrnos;
	CString olInsData;
	CString olUpdData;
	CString olTmpStr;
	int ilRecNum;
	int ilHaiCount;
	// for all given flights
	for (int j = 0; j < ropFlightUrnos.GetSize(); j++)
	{
		// get all hai record for the current flight
		prlArray = NULL;
 		if (omFlnuMap.Lookup((void *)ropFlightUrnos[j], (void *& )prlArray) == TRUE)
			ilHaiCount = prlArray->GetSize();
		else
			ilHaiCount = 0;

 		// loop over all given data
		for (int i = 0; i < ropRecNums.GetSize(); i++)
		{
			ilRecNum = (int)ropRecNums[i];
			if (prlArray != NULL && ilRecNum < ilHaiCount)
			{
				// there allready exists a hai record
				if (ropHSNAs[i].IsEmpty())
				{
					// collect the hai record for deletion
					olTmpStr.Format("%ld\n", (*prlArray)[ilRecNum].Urno);
					olDelUrnos += olTmpStr;
				}
				else
				{
					// collect the hai record for update
					olTmpStr.Format("%ld,%s,%s,%s,%ld\n", ropFlightUrnos[j], ropHSNAs[i], ropTASKs[i], ropREMAs[i], (*prlArray)[ilRecNum].Urno); 
					olUpdData += olTmpStr;
				}
			}
			else
			{
				// collect the hai record for insertion
				olTmpStr.Format("%ld,%ld,%s,%s,%s\n", ogBasicData.GetNextUrno(), ropFlightUrnos[j], ropHSNAs[i], ropTASKs[i], ropREMAs[i]); 
				olInsData += olTmpStr;
			}
		}
	}


	// update the database with relhdl!
	if (!olDelUrnos.IsEmpty())
 		ExecRelCommand('D', CString("[URNO=:VURNO]"), olDelUrnos);

	if (!olInsData.IsEmpty())
		ExecRelCommand('I', CString("URNO,FLNU,HSNA,TASK,REMA"), olInsData);

	if (!olUpdData.IsEmpty())
		ExecRelCommand('U', CString("FLNU,HSNA,TASK,REMA,[URNO=:VURNO]"), olUpdData);


	return true;
}



bool CedaHaiData::ExecRelCommand(char cpMode, const CString &ropFields, const CString &ropData)
{
	CString olRelComm;
	CString olFieldsTmp(ropFields);
	int ilFieldNum = olFieldsTmp.Replace(',', '0')+1; // count fields

	switch(cpMode)
	{
	case 'D': // Delete
	{
		olRelComm.Format("*CMD*,HAI%s,DRT,-1,,%s\n%s", pcgTableExt, ropFields, ropData);
		break;
	}
	case 'I': // Insert
	{
		olRelComm.Format("*CMD*,HAI%s,IRT,%d,%s\n%s", pcgTableExt, ilFieldNum, ropFields, ropData);
		break;
	}
	case 'U': // Update
	{
		olRelComm.Format("*CMD*,HAI%s,URT,%d,%s\n%s", pcgTableExt, ilFieldNum-1, ropFields, ropData);
		break;
	}
	default:
		return false;
	}
	
	return CedaAction("REL","QUICK","", olRelComm.GetBuffer(0));
}






void CedaHaiData::InsertInternal(HAIDATA *prpHai)
{
	omData.Add(prpHai);
	omUrnoMap.SetAt((void *)prpHai->Urno, prpHai);


	CCSPtrArray<HAIDATA> *prlArray;

	if(omFlnuMap.Lookup((void *)prpHai->Flnu, (void *& )prlArray) == TRUE)
	{
		prlArray->Add(prpHai);
	}
	else
	{
		prlArray = new CCSPtrArray<HAIDATA>;
		prlArray->Add(prpHai);
		omFlnuMap.SetAt((void *)prpHai->Flnu, prlArray);
	}

}



