// cCfgd.cpp - Class for handling Cfgloyee data
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <ccsptrarray.h>
#include <CCScedacom.h>
#include <CCScedadata.h>
#include <ccsddx.h>
#include <CCSbchandle.h>
#include <ccsddx.h>
#include <basicdata.h>
#include <CedaCfgData.h>
#include <cviewer.h>
#include <CedaSysTabData.h>
#include <resrc1.h>
#include <konflikte.h>
#include <CedaBasicData.h>

#include "Fpms.h"		// 050310 MVy: today only, need CanHandlePositionFlightTodayOnlyCheck()

const char* CedaCfgData::pomDefaultName = "#__DEFAULT__#";
//const int	CedaCfgData::NUMCONFLICTS = 76;
// 050228 MVy: confict rule line added for Aircraft Registration restriction

CedaCfgData::CedaCfgData()
{                  
    // Create an array of CEDARECINFO for CFGDATA
    BEGIN_CEDARECINFO(CFGDATA, CfgDataRecInfo)
        FIELD_LONG(Urno,"URNO")
        FIELD_CHAR_TRIM(Appn,"APPN")
        FIELD_CHAR_TRIM(Ctyp,"CTYP")
        FIELD_CHAR_TRIM(Ckey,"CKEY")
        FIELD_DATE(Vafr,"VAFR")
        FIELD_DATE(Vato,"VATO")
        FIELD_CHAR_TRIM(Pkno,"PKNO")
        FIELD_CHAR_TRIM(Text,"TEXT")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(CfgDataRecInfo)/sizeof(CfgDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&CfgDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"VCD");
	
	strcpy(pcmListOfFields, "URNO,APPN,CTYP,CKEY,VAFR,VATO,PKNO,TEXT");
	pcmFieldList = pcmListOfFields;
    //pcmFieldList = "URNO,APPN,CTYP,CKEY,VAFR,VATO,PKNO,TEXT";

	//ogDdx.Register((void *)this,BC_APT_CHANGE,CString("APTDATA"), CString("APT-changed"),ProcessAPTCf);
	//ogDdx.Register((void *)this,BC_CFG_CHANGE,CString("BC_CfgChange"), CString("CfgDataChange"),ProcessCfgCf);
	//ogDdx.Register((void *)this,BC_CFG_INSERT,CString("BC_CfgInsert"), CString("CfgDataChange"),ProcessCfgCf);

	NUMCONFLICTS = 79;


	sprintf(rmMonitorSetup.Appn, "CCS_%s", ogAppName);
	strcpy(rmMonitorSetup.Ctyp, "MONITORS");
	strcpy(rmMonitorSetup.Ckey, "");
	rmMonitorSetup.Vafr = CTime::GetCurrentTime();
	rmMonitorSetup.Vato = CTime(2037, 12, 31, 23, 59, 00);
	strcpy(rmMonitorSetup.Pkno, ogBasicData.omUserID);
	rmMonitorSetup.IsChanged = DATA_NEW;
	sprintf(rmMonitorSetup.Text, "%s=1#%s=1#%s=1#%s=1#%s=1#%s=1#%s=1#%s=1",
								MON_COUNT_STRING,			
								MON_SEASONSCHEDULE_STRING,	
								MON_DAILYSCHEDULE_STRING,	
								MON_SEASONROTDLG_STRING,	    
								MON_SEASONBATCH_STRING,	    
								MON_DAILYROTDLG_STRING,	    
								MON_FLIGHTDIA_STRING,	    
								MON_CCADIA_STRING);	    

	//font
	sprintf(rmFontSetup.Appn, "CCS_%s", ogAppName);
	strcpy(rmFontSetup.Ctyp, "FONT");
	strcpy(rmFontSetup.Ckey, "");
	rmFontSetup.Vafr = CTime::GetCurrentTime();
	rmFontSetup.Vato = CTime(2037, 12, 31, 23, 59, 00);
	strcpy(rmFontSetup.Pkno, ogBasicData.omUserID);
	rmFontSetup.IsChanged = DATA_NEW;
	sprintf(rmFontSetup.Text, "");

	//buffertime for gates
	sprintf(rmGateBufferTimeSetup.Appn, "CCS_%s", ogAppName);
	strcpy(rmGateBufferTimeSetup.Ctyp, "GATEBUFFERTIME");
	strcpy(rmGateBufferTimeSetup.Ckey, "");
	rmGateBufferTimeSetup.Vafr = CTime::GetCurrentTime();
	rmGateBufferTimeSetup.Vato = CTime(2037, 12, 31, 23, 59, 00);
	strcpy(rmGateBufferTimeSetup.Pkno, ogBasicData.omUserID);
	rmGateBufferTimeSetup.IsChanged = DATA_NEW;
	sprintf(rmGateBufferTimeSetup.Text, "");

	//buffertime for positions
	sprintf(rmPosBufferTimeSetup.Appn, "CCS_%s", ogAppName);
	strcpy(rmPosBufferTimeSetup.Ctyp, "POSBUFFERTIME");
	strcpy(rmPosBufferTimeSetup.Ckey, "");
	rmPosBufferTimeSetup.Vafr = CTime::GetCurrentTime();
	rmPosBufferTimeSetup.Vato = CTime(2037, 12, 31, 23, 59, 00);
	strcpy(rmPosBufferTimeSetup.Pkno, ogBasicData.omUserID);
	rmPosBufferTimeSetup.IsChanged = DATA_NEW;
	sprintf(rmPosBufferTimeSetup.Text, "");

	//Xminutes before internal best time //PRF 8379	
	sprintf(rmXminutesBufferTimeSetup.Appn, "CCS_%s", ogAppName);
	strcpy(rmXminutesBufferTimeSetup.Ctyp, "XMINUTESBUFFERTIME");
	strcpy(rmXminutesBufferTimeSetup.Ckey, "");
	rmXminutesBufferTimeSetup.Vafr = CTime::GetCurrentTime();
	rmXminutesBufferTimeSetup.Vato = CTime(2037, 12, 31, 23, 59, 00);
	strcpy(rmXminutesBufferTimeSetup.Pkno, ogBasicData.omUserID);
	rmXminutesBufferTimeSetup.IsChanged = DATA_NEW;
	sprintf(rmXminutesBufferTimeSetup.Text, "");	

	//buffertime for arcive-past
	sprintf(rmArchivePastBufferTimeSetup.Appn, "CCS_%s", ogAppName);
	strcpy(rmArchivePastBufferTimeSetup.Ctyp, "ARCHIVEPASTBUFFERTIME");
	strcpy(rmArchivePastBufferTimeSetup.Ckey, "");
	rmArchivePastBufferTimeSetup.Vafr = CTime::GetCurrentTime();
	rmArchivePastBufferTimeSetup.Vato = CTime(2037, 12, 31, 23, 59, 00);
	strcpy(rmArchivePastBufferTimeSetup.Pkno, ogBasicData.omUserID);
	rmArchivePastBufferTimeSetup.IsChanged = DATA_NEW;
	sprintf(rmArchivePastBufferTimeSetup.Text, "");

	//buffertime for arcive-future
	sprintf(rmArchiveFutureBufferTimeSetup.Appn, "CCS_%s", ogAppName);
	strcpy(rmArchiveFutureBufferTimeSetup.Ctyp, "ARCHIVEFUTUREBUFFERTIME");
	strcpy(rmArchiveFutureBufferTimeSetup.Ckey, "");
	rmArchiveFutureBufferTimeSetup.Vafr = CTime::GetCurrentTime();
	rmArchiveFutureBufferTimeSetup.Vato = CTime(2037, 12, 31, 23, 59, 00);
	strcpy(rmArchiveFutureBufferTimeSetup.Pkno, ogBasicData.omUserID);
	rmArchiveFutureBufferTimeSetup.IsChanged = DATA_NEW;
	sprintf(rmArchiveFutureBufferTimeSetup.Text, "");

	// 050309 MVy: today only or period, buffertime for usage of archive
	sprintf( rmArchivePostFlightTodayOnly.Appn, "CCS_%s", ogAppName );
	strcpy( rmArchivePostFlightTodayOnly.Ctyp, "ARCHIVEPOSTFLIGHTTODAYONLY" );
	strcpy( rmArchivePostFlightTodayOnly.Ckey, "" );
	rmArchivePostFlightTodayOnly.Vafr = CTime::GetCurrentTime();
	rmArchivePostFlightTodayOnly.Vato = CTime( 2037, 12, 31, 23, 59, 00 );
	strcpy( rmArchivePostFlightTodayOnly.Pkno, ogBasicData.omUserID );
	rmArchivePostFlightTodayOnly.IsChanged = DATA_NEW ;
	sprintf( rmArchivePostFlightTodayOnly.Text, "");

	//ResChainDlg
	sprintf(rmResChainDlg.Appn, "CCS_%s", ogAppName);
	strcpy(rmResChainDlg.Ctyp, "RESCHAINDLG");
	strcpy(rmResChainDlg.Ckey, "");
	rmResChainDlg.Vafr = CTime::GetCurrentTime();
	rmResChainDlg.Vato = CTime(2037, 12, 31, 23, 59, 00);
	strcpy(rmResChainDlg.Pkno, ogBasicData.omUserID);
	rmResChainDlg.IsChanged = DATA_NEW;
	sprintf(rmResChainDlg.Text, "");

	g_rgbHandlingAgentOccBkBar = RGB( 192,192,192 );		// light gray


	//buffertime for timeline
	sprintf(rmTimelineBuffer.Appn, "CCS_%s", ogAppName);
	strcpy(rmTimelineBuffer.Ctyp, "TIMELINEBUFFER");
	strcpy(rmTimelineBuffer.Ckey, "");
	rmTimelineBuffer.Vafr = CTime::GetCurrentTime();
	rmTimelineBuffer.Vato = CTime(2020, 12, 31, 23, 59, 00);
	strcpy(rmTimelineBuffer.Pkno, ogBasicData.omUserID);
	rmTimelineBuffer.IsChanged = DATA_NEW;
	sprintf(rmTimelineBuffer.Text, "");

}

CedaCfgData::~CedaCfgData()
{
	TRACE("CedaCfgData::~CedaCfgData called\n");
	ogDdx.UnRegister(this,NOTUSED);
	omData.DeleteAll();
	//omViews.DeleteAll();
	omUrnoMap.RemoveAll();
	omCkeyMap.RemoveAll();
	omRecInfo.DeleteAll();
	ClearAllViews();

	omConflictData.DeleteAll();
	omConflictMap.RemoveAll();
}





bool CedaCfgData::ReadCfgData()
{
    char pclWhere[512];
	bool ilRc = true;

    // Select data from the database
	//MWO TO DO CCS_FPMS ==> später APPLNAME nehmen
/*    sprintf(pclWhere, 
		    "WHERE APPN='CCS_FPMS' AND (CTYP = 'WHAT-IF' OR CTYP = 'CKI-SETUP' OR CTYP = 'WIFLFNDR')");
    if (CedaAction("RT", pclWhere) == false)
        return false;

// this section is responsible for WHATIF and SETUP
    // Load data from CedaData into the dynamic array of record
    omCkeyMap.RemoveAll();
	omUrnoMap.RemoveAll();
    omData.DeleteAll();

    for (int ilLc = 0; ilRc == true; ilLc++)
    {
		CFGDATA *prlCfg = new CFGDATA;
		if ((ilRc = GetBufferRecord(ilLc,prlCfg)) == true)
		{
			PrepareCfgData(prlCfg);
			omData.Add(prlCfg);
			omCkeyMap.SetAt(prlCfg->Ckey,prlCfg);
			omUrnoMap.SetAt((void *)prlCfg->Urno,prlCfg);
		}
		else
		{
			delete prlCfg;
		}
	}

	MakeCurrentUser(); 
*/
// this section is responsible for the VIEWS in charts and lists
	ilRc = true;
    omCkeyMap.RemoveAll();
	omUrnoMap.RemoveAll();
    omData.DeleteAll();

    // Select data from the database
	//MWO TO DO CCS_FPMS ==> später APPLNAME nehmen
    sprintf(pclWhere, "WHERE APPN='CCS_FPMS' AND (CTYP = 'VIEW-DATA' AND PKNO = '%s')", ogBasicData.omUserID);

	
	if (CedaAction("RT", pclWhere) == false)
        return false;

	//Initialize the datastructure of for the views
	ClearAllViews();
    for (int ilLc = 0; ilRc == true; ilLc++)
    {
		CFGDATA rlCfg;
		if ((ilRc = GetBufferRecord(ilLc,&rlCfg)) == true)
		{
			PrepareViewData(&rlCfg);
		}
	}

    return true;
}

/*
bool CedaCfgData::ReadConflicts(CStringArray &opLines)
{
	char pclWhere[1024]="";
	int ilLc = 0;

	opLines.RemoveAll();
	bool ilRc = true;
	bool olRc = true;
//Now we read the conflict configuration
	ilRc = true;

    // Select data from the database
    sprintf(pclWhere, "WHERE APPN='CCS_PEPPER_PDI' AND (CTYP LIKE 'CONFLICT%%' AND PKNO = '%s') ORDER BY CTYP", ogBasicData.omUserID);
    olRc = CedaAction("RT", pclWhere);

    for (ilLc = 0; ilRc == true; ilLc++)
    {
		CFGDATA rlCfg;
		if ((ilRc = GetBufferRecord(ilLc,&rlCfg)) == true)
		{
			CString olStr = CString(rlCfg.Text);
			opLines.Add(olStr);
		}
	}
	if((ilLc < 15) || (olRc == false))
	{
		opLines.RemoveAll();
		opLines.Add(CString("0;0;Kein Konflikt;0;0;!;?;65280;8454016;Kein Konflikt"));
		opLines.Add(CString("1;1;Kein Einsatz;90;60;!;?;255;33023;Es ist noch kein Bedarf fuer diesem Flug abgedeckt"));
		opLines.Add(CString("2;2;Unterdeckung;80;0;!;?;33023;16711680;Es ist nur ein Bedarf fuer diesen Flug abgedeckt"));
		opLines.Add(CString("3;4;Unterdeckung;70;10;!;?;8388863;32896;Es sind noch nicht alle Bedarfe abgedeckt"));
		opLines.Add(CString("4;8;Delay;60;3;6;6;65535;65280;Neuer Text"));
		opLines.Add(CString("5;16;Cancellation;40;0;!;?;8388736;65280;Kein Konflikt"));
		opLines.Add(CString("6;32;Diversion;30;0;!;?;8421631;65280;Kein Konflikt"));
		opLines.Add(CString("7;64;Einsatzueberlappung;40;0;!;?;12615680;33023;Überlappung mit nachfolgendem Einsatz"));
		opLines.Add(CString("8;128;Bestätigung;30;0;!;?;4227327;255;Keine Einsatzbestätigung 15 Minuten vor Einsatzbeginn"));
		opLines.Add(CString("9;256;Gate Change;30;120;!;?;4210816;65280;Neues Gate"));
		opLines.Add(CString("10;512;Kein Bedarf;99;0;!;?;16777088;65280;Kein Bedarf"));
		opLines.Add(CString("11;1024;Equipment Change;30;0;!;?;4227327;65280;Anderer Flugzeugtyp"));
		opLines.Add(CString("12;2048;Überbuchung;30;0;!;?;16711680;65280;Anderer Flugzeugtyp"));
		opLines.Add(CString("13;4096;Bedarfsüberdeckung;30;0;!;?;8421376;65280;Anderer Flugzeugtyp"));
		opLines.Add(CString("14;8192;Keine Rückmeldung;30;0;!;?;65535;65280;Anderer Flugzeugtyp"));
		opLines.Add(CString("15;16384;Nicht im Pool;30;0;!;?;32896;65280;Mitarbeiter ist nicht dem Pool zugeteilt"));
	}
    return true;
}
*/

/*
bool CedaCfgData::SaveConflictData(CFGDATA *prpCfg)
{
	CString olListOfData;
	bool olRc = true;
	char pclData[824];
	char pclSelection[1024]="";
	//First we delete the entry
	//MWO TO DO CCS_FPMS durch APPN ersetzen
	sprintf(pclSelection,"WHERE APPN='CCS_FPMS' AND (PKNO = '%s' AND CTYP = '%s')", ogBasicData.omUserID, prpCfg->Ctyp);
	olRc = CedaAction("DRT",pclSelection);

	//and create it new
	MakeCedaData(olListOfData,prpCfg);
	strcpy(pclData,olListOfData);
	olRc = CedaAction("IRT","","",pclData);

	return olRc;
}
*/

void CedaCfgData::MakeCurrentUser(void)
{
	// Fills the setup struct for the user with current login-USERID
	CCSPtrArray<CFGDATA> olData;
	int ilCountSets = 0;
	int ilCount = omData.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		CFGDATA rlC = omData[i];
		if((strcmp(rlC.Pkno, ogBasicData.omUserID) == 0) && (strcmp(rlC.Ctyp, "CKI-SETUP") == 0))
		{
			olData.NewAt(0, rlC);
			ilCountSets++;
		}
	}
	
	if(ilCountSets == 2)
	{
		CString olParameters;
		CString olTmp1, olTmp2;
		CFGDATA rlC1 = olData.GetAt(0);
		CFGDATA rlC2 = olData.GetAt(1);
		olTmp1 = rlC1.Text;
		olTmp2 = rlC2.Text;
		olParameters = olTmp1 + olTmp2;
		InterpretSetupString(olParameters, &rmUserSetup);
	}
	olData.DeleteAll();
}

void CedaCfgData::DeleteViewFromDiagram(CString opDiagram, CString olView)
{
	bool olRc = true;
	char pclSelection[1024]="";
	char pclDiagram[100]="";
	char pclViewName[100]="";

	strcpy(pclDiagram, opDiagram);
	strcpy(pclViewName, olView);
	//MWO TO DO CCS_FPMS ==> APPL
/*	sprintf(pclSelection,"WHERE APPN='CCS_FPMS' AND (PKNO = '%s' AND CKEY = '%s' AND CTYP = 'VIEW-DATA' AND TEXT LIKE '%%VIEW=%s%%')", ogBasicData.omUserID, pclDiagram, pclViewName);
	olRc = CedaAction("DRT",pclSelection);


	return;
*/

		sprintf(pclSelection,"WHERE APPN='CCS_FPMS' AND (PKNO = '%s' AND "
			"CKEY = '%s' AND CTYP = 'VIEW-DATA')", 
			ogBasicData.omUserID, opDiagram);

		CedaAction("RT", pclSelection);

		CString olUrnoList("");

		//Initialize the datastructure of for the views
		bool ilRc = true;
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			CFGDATA rlCfg;
			if ((ilRc = GetBufferRecord(ilLc,&rlCfg)) == true)
			{
				PrepareViewData(&rlCfg);

				RAW_VIEWDATA olRawData;
				BOOL blRaw = MakeRawViewData(&rlCfg, &olRawData);
				if (blRaw)
				{
					CString olRawViewName = olRawData.Name;
					CCSCedaData::MakeClientString(olRawViewName);
					if(olView == olRawViewName)
					{
						char buffer[32];
						ltoa(rlCfg.Urno, buffer, 10);

						olUrnoList += CString(buffer) + ',';
					}
				}
			}
		}
		if(!olUrnoList.IsEmpty())
		{
			olUrnoList = olUrnoList.Left(olUrnoList.GetLength() - 1);


			// to make it easier we delete all rows of the user and his configuration for the
			// specified diagram
			sprintf(pclSelection,"WHERE URNO IN (%s)",olUrnoList);
			bool blRet = CedaAction("DRT",pclSelection);
		}
}


void CedaCfgData::UpdateViewForDiagram(CString opDiagram, VIEWDATA &prpView,CString opViewName)
{
	bool olRc = true;
	CString olListOfData;
	char pclSelection[2048]="";
	char pclData[4096];

	bool blDeleteOldView = true;
	CString olSelection ("");
	CString olData ("");
	CString olDiagram ("");
	CString olText ("");
	CString olTmp ("");

	CCSPtrArray<CFGDATA> olNewViewData;

	if (opDiagram.GetLength() < 33)
		olDiagram = opDiagram;
	else
	{
		ASSERT(FALSE);
		return;
	}

	if (ogBasicData.omUserID.GetLength() > 33)
	{
		ASSERT(FALSE);
		return;
	}

//	this is for a later work, if it shall possible to change
//	the text size vcdtab by the user
	CedaSysTabData olSystab;	
	olSystab.SetTableExtension("TAB");

	char olSel[256];
	sprintf(olSel, "WHERE TANA='VCD'");

	int ilFeleText = 2000;
	if(olSystab.Read(olSel, true, "FINA,FELE"))
	{
		for(int w = olSystab.omData.GetSize() - 1; w >= 0; w--)
		{
			if(strcmp(olSystab.omData[w].Fina, "TEXT") == 0 )
				ilFeleText = olSystab.omData[w].Fele;
		}
	}
//

	int ilC1 = prpView.omNameData.GetSize();

	for(int i = 0; i < ilC1; i++)
	{
		if (opViewName == prpView.omNameData[i].ViewName) 
		{

			int ilC2 = prpView.omNameData[i].omTypeData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				if(prpView.omNameData[i].omTypeData[j].Type == "FILTER")
				{
					if(prpView.omNameData[i].ViewName != "<Default>")
					{
						CFGDATA rlCfg;
						strncpy(rlCfg.Ckey, olDiagram, 32);				//char[32]vcdtab
						strncpy(rlCfg.Pkno, ogBasicData.omUserID, 32);	//char[32]vcdtab
						strncpy(rlCfg.Ctyp, "VIEW-DATA", 32);			//char[32]vcdtab

						int ilC3 = prpView.omNameData[i].omTypeData[j].omTextData.GetSize();
						for(int k = 0; k < ilC3; k++)
						{
							olText.Format("VIEW=%s#TYPE=%s#PAGE=%s#TEXT=",prpView.omNameData[i].ViewName,prpView.omNameData[i].omTypeData[j].Type,prpView.omNameData[i].omTypeData[j].omTextData[k].Page);
							olTmp = "";
							int ilPreFix = olText.GetLength()+3;//=@@#

							int ilC4 = prpView.omNameData[i].omTypeData[j].omTextData[k].omValues.GetSize();

							for(int l = 0; l < ilC4; l++)
							{
								CString olTmpl ("");
								olTmpl.Format("%s@", prpView.omNameData[i].omTypeData[j].omTextData[k].omValues[l]);
//								olTmpl.Format("%s", prpView.omNameData[i].omTypeData[j].omTextData[k].omValues[l]);
								olTmp += olTmpl;
							}

							olText += olTmp + CString("#");

							if (olText.GetLength() < 2000/*ilFeleText*/)
							//CFGDATA->Text::char[2001]!!!VCDTAB->Text::varchar(2000),
							//change to CString and ilFeleText then you can change vcdtab::text as you want!!
							{
								rlCfg.Urno = ogBasicData.GetNextUrno();
								strncpy(rlCfg.Text, olText, 2000/*ilFeleText*/);
								rlCfg.Vafr = CTime::GetCurrentTime();
								rlCfg.Vato = rlCfg.Vafr + CTimeSpan(365*10, 0, 0, 0);

								//and save to database
								olNewViewData.NewAt(olNewViewData.GetSize(), rlCfg);

								if ((int)strlen(pclData) < olListOfData.GetLength())
								{
									blDeleteOldView = false;
									::MessageBox(NULL, "Record is too long\n Can't be saved!",GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
								}
							}
							else
							{
								blDeleteOldView = false;
								CString olMessage;
								int ilActChar = olText.GetLength() - ilPreFix;
								int ilMaxChar = 2000 - ilPreFix;
								int ilRemChar = ilActChar - ilMaxChar;
								olMessage.Format("Condition for the Filter is too long\nActual characters%d, Maximum %d\nYou must remove %d characters\nThis View can't be saved!",ilActChar,ilMaxChar,ilRemChar);
								::MessageBox(NULL, olMessage,GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
							}
							//rlCfg.IsChanged = DATA_UNCHANGED;
						}
					}
				}
				else //not filter but ==> Group, Sort etc.
				{
					olTmp = "";
					if(prpView.omNameData[i].ViewName != "<Default>")
					{
						CFGDATA rlCfg;
						strncpy(rlCfg.Ckey, olDiagram, 32);				//char[32]vcdtab
						strncpy(rlCfg.Pkno, ogBasicData.omUserID, 32);	//char[32]vcdtab
						strncpy(rlCfg.Ctyp, "VIEW-DATA", 32);			//char[32]vcdtab

						olText.Format("VIEW=%s#TYPE=%s#TEXT=",prpView.omNameData[i].ViewName, prpView.omNameData[i].omTypeData[j].Type);
						int ilPreFix = olText.GetLength()+3;//=@@#

						int ilC3 = prpView.omNameData[i].omTypeData[j].omValues.GetSize();
						for(int k = 0; k < ilC3; k++)
						{
							CString olTmpl ("");
							olTmpl.Format("%s|", prpView.omNameData[i].omTypeData[j].omValues[k]);
							olTmp += olTmpl;
						}
						olText += olTmp + CString("#");

						if (olText.GetLength() < 2000/*ilFeleText*/)
						//CFGDATA->Text::char[2001]!!!VCDTAB->Text::varchar(2000),
						//change to CString and ilFeleText then you can change vcdtab::text as you want!!
						{
							rlCfg.Urno = ogBasicData.GetNextUrno();
							strncpy(rlCfg.Text, olText, 2000/*ilFeleText*/);
							rlCfg.Vafr = CTime::GetCurrentTime();
							rlCfg.Vato = rlCfg.Vafr + CTimeSpan(365*10, 0, 0, 0);

							//and save to database
							olNewViewData.NewAt(olNewViewData.GetSize(), rlCfg);

							if ((int)strlen(pclData) < olListOfData.GetLength())
							{
								blDeleteOldView = false;
								::MessageBox(NULL, "Record is too long\n Can't be saved!",GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
							}
						}
						else
						{
							blDeleteOldView = false;
							CString olMessage;
							int ilActChar = olText.GetLength() - ilPreFix;
							int ilMaxChar = 2000 - ilPreFix;
							int ilRemChar = ilActChar - ilMaxChar;
							olMessage.Format("Condition for the Sort is too long\nActual characters%d, Maximum %d\nYou must remove %d characters\nThis View can't be saved!",ilActChar,ilMaxChar,ilRemChar);
							::MessageBox(NULL, olMessage,GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
						}




/*						CFGDATA rlCfg;
						rlCfg.Urno = ogBasicData.GetNextUrno();
						strcpy(rlCfg.Ckey, pclDiagram);
						strcpy(rlCfg.Pkno, ogBasicData.omUserID);
						strcpy(rlCfg.Ctyp, "VIEW-DATA");
						sprintf(pclText, "VIEW=%s#", prpView.omNameData[i].ViewName);
						sprintf(pclTmp, "TYPE=%s#TEXT=", prpView.omNameData[i].omTypeData[j].Type );
						strcat(pclText, pclTmp);
						pclTmp[0]='\0';
						int ilC3 = prpView.omNameData[i].omTypeData[j].omValues.GetSize();
						for(int k = 0; k < ilC3; k++)
						{
							char pclS[100]="";
							sprintf(pclS, "%s|", prpView.omNameData[i].omTypeData[j].omValues[k]);
							strcat(pclTmp, pclS);
						}
						strcat(pclTmp, "#");
						strcat(pclText, pclTmp);
						strcpy(rlCfg.Text, pclText);
						//and save to database
						MakeCedaData(olListOfData,&rlCfg);
						strcpy(pclData,olListOfData);
						olRc = CedaAction("IRT","","",pclData);
						rlCfg.IsChanged = DATA_UNCHANGED;
						pclText[0]='\0';*/



					}
				}
			}
		i = ilC1;
		}
	}

	//delete and save the values
	CString olUrnoList("");
	if (blDeleteOldView)
	{
		DeleteViewFromDiagram(olDiagram, opViewName);
		// after removing we create a completely new configuration
		for (i=0; i<olNewViewData.GetSize(); i++)
		{
			CFGDATA *rlCfg = &olNewViewData[i];
			MakeCedaData(olListOfData, rlCfg);
			strncpy(pclData, olListOfData, 4096);
			if (!CedaAction("IRT", "", "", pclData))
				::MessageBox(NULL, "Insertion failed!\n Old View is deleted!",GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
		}
	}
	olNewViewData.DeleteAll();
}

////////////////////////////////////////////////////////////////
// Interprets the raw data for one propertypage
BOOL CedaCfgData::MakeRawViewData(CFGDATA *prpCfg, RAW_VIEWDATA *prpRawData)
{
	strcpy(prpRawData->Ckey, prpCfg->Ckey);
	char *psp = NULL;
//	char pclOriginal[1024]="";
	char pclOriginal[4096]="";
	char pclVIEW[100]="";
	char pclTYPE[100]="";
	char pclPAGE[100]="";
//	char pclTEXT[1024]="";
	char pclTEXT[4096]="";

	strcpy(pclOriginal, prpCfg->Text);
	psp = strstr(pclOriginal, "#");
	while(psp != NULL)
	{
		char *pclTmp;
		char pclPart[4096];
		char pclRest[4096];
		strcpy(pclRest, psp+1);
		*psp = '\0';
		strcpy(pclPart, pclOriginal);
		strcpy(pclOriginal, pclRest);
		if((pclTmp = strstr(pclPart, "VIEW=")) != NULL)
			strcpy(pclVIEW, pclPart);
		else if((pclTmp = strstr(pclPart, "TYPE=")) != NULL)
			strcpy(pclTYPE, pclPart);
		else if((pclTmp = strstr(pclPart, "PAGE=")) != NULL)
			strcpy(pclPAGE, pclPart);
		else if((pclTmp = strstr(pclPart, "TEXT=")) != NULL)
			strcpy(pclTEXT, pclPart);

		psp = strstr(pclOriginal, "#");
	}

	psp = strstr(pclTYPE, "=");
	if(psp == NULL)
	{
		return FALSE;
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			return FALSE;
		}
		else
		{
			strcpy(prpRawData->Type, pclOriginal);
		}
	}
	psp = NULL;
	psp = strstr(pclVIEW, "=");
	if(psp == NULL)
	{
		return FALSE;
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			return FALSE;
		}
		else
		{
			strcpy(prpRawData->Name, pclOriginal);
		}
	}
	psp = NULL;
	psp = strstr(pclPAGE, "=");
	if(psp == NULL)
	{
		if(strcmp(prpRawData->Type, "FILTER") == 0)
		{
			return FALSE;
		}
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			return FALSE;
		}
		else
		{
			strcpy(prpRawData->Page, pclOriginal);
		}
	}
	psp = NULL;
	psp = strstr(pclTEXT, "=");
	if(psp == NULL)
	{
		return FALSE;
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
//			return FALSE;
		}
		else
		{
			strcpy(prpRawData->Values, pclOriginal);
		}
	}
	psp = NULL;

	//RST
	if(CString(prpRawData->Type) == CString("DISPOZEITRAUM"))
	{
		strcpy(prpRawData->Type, "FLIGHTSEARCH");
	}


	return TRUE;
}

////////////////////////////////////////////////////////////////
//MWO: extracts the values which are comma-separated and copies them
//     into CStringArray of VIEW_TEXTDATA

// FILTER: pcpSepa = "@"
// SONST   pcpSepa = "|"
void CedaCfgData::MakeViewValues(char *pspValues, CCSPtrArray<CString> *popValues, char *pcpSepa )
{
	char *psp = NULL;
//	char pclOriginal[1024]="";
	char pclOriginal[4096]="";
//	char pclValue[100]="";

	strcpy(pclOriginal, pspValues);
	psp = strstr(pclOriginal, pcpSepa/*"@ oder |"*/);
	while(psp != NULL)
	{
		char pclPart[4096];
		char pclRest[4096];
		strcpy(pclRest, psp+1);
		*psp = '\0';
		strcpy(pclPart, pclOriginal);
		strcpy(pclOriginal, pclRest);
		if(strcmp(pclPart, "") != 0)
			 popValues->NewAt(popValues->GetSize(), pclPart);
		else
			 popValues->NewAt(popValues->GetSize(), "");

		psp = strstr(pclOriginal, pcpSepa/*"@ oder |"*/);
	}
}



////////////////////////////////////////////////////////////////
// MWO: prepares the nested arrays for the CFGDATA
void CedaCfgData::PrepareViewData(CFGDATA *prpCfg)
{
	VIEWDATA *prlFoundViewData;
	VIEW_VIEWNAMES *prlFoundViewName;
	VIEW_TYPEDATA *prlFoundTypeData;
	VIEW_TEXTDATA *prlFoundTextData;
	RAW_VIEWDATA rlRawData;

	if(MakeRawViewData(prpCfg, &rlRawData) != TRUE)
	{
		return; //then the row is corrupt
	}
	prlFoundViewData = FindViewData(prpCfg);
	if(prlFoundViewData == NULL) 
	{
		//if the key does not exist we have generate an new on one from the root
		VIEW_TEXTDATA rlTextData;
		VIEW_TYPEDATA rlTypeData;
		VIEW_VIEWNAMES rlNameData;
		VIEWDATA rlViewData;
		rlViewData.Ckey = CString(rlRawData.Ckey);
		rlNameData.ViewName = CString(rlRawData.Name);
		rlTypeData.Type = CString(rlRawData.Type);
		if(strcmp(rlRawData.Type, "FILTER") == 0)
		{
			rlTextData.Page = CString(rlRawData.Page);
			MakeViewValues(rlRawData.Values, &rlTextData.omValues, "@");
			rlTypeData.omTextData.NewAt(rlTypeData.omTextData.GetSize(), rlTextData);
		}
		else
		{
			MakeViewValues(rlRawData.Values, &rlTypeData.omValues, "|");
		}
		rlNameData.omTypeData.NewAt(rlNameData.omTypeData.GetSize(), rlTypeData);
		rlViewData.omNameData.NewAt(rlViewData.omNameData.GetSize(), rlNameData);
		omViews.NewAt(omViews.GetSize(), rlViewData);

		/*************
		for ( int ilViewCount = rlViewData.omNameData.GetSize() -1; ilViewCount >= 0; ilViewCount--)
		{
			VIEW_VIEWNAMES *prlNameData = &rlViewData.omNameData[ilViewCount];
			for ( int ilNameCount = prlNameData->omTypeData.GetSize() -1; ilNameCount >= 0; ilNameCount--)
			{
				VIEW_TYPEDATA *prlTypeData = &prlNameData->omTypeData[ilViewCount];

				for ( int ilLc = prlTypeData->omTextData.GetSize() -1; ilLc >= 0; ilLc --)
				{
					prlTypeData->omTextData[ilLc].omValues.DeleteAll();
				}
				prlTypeData->omTextData.DeleteAll();
				prlTypeData->omValues.DeleteAll();
			}
			prlNameData->omTypeData.DeleteAll();
		}
		rlViewData.omNameData.DeleteAll();
		*****/
	}
	else
	{

		//the key exists, we have to look for the rest of the tree and
		//evaluate, whether parts of it do exist and merge them together
		//Now first let's look for existing of the view-name
		prlFoundViewName = FindViewNameData(&rlRawData);
		if(prlFoundViewName == NULL)
		{
			//the view itself exists but there are no entries

			VIEW_TEXTDATA rlTextData;
			VIEW_TYPEDATA rlTypeData;
			VIEW_VIEWNAMES rlNameData;
			rlNameData.ViewName = CString(rlRawData.Name);
			rlTypeData.Type = CString(rlRawData.Type);
			if(strcmp(rlRawData.Type, "FILTER") == 0)
			{
				rlTextData.Page = CString(rlRawData.Page);
				MakeViewValues(rlRawData.Values, &rlTextData.omValues, "@");
				rlTypeData.omTextData.NewAt(rlTypeData.omTextData.GetSize(), rlTextData);
			}
			else
			{
				MakeViewValues(rlRawData.Values, &rlTypeData.omValues, "|");
			}
			rlNameData.omTypeData.NewAt(rlNameData.omTypeData.GetSize(), rlTypeData);
			prlFoundViewData->omNameData.NewAt(prlFoundViewData->omNameData.GetSize(), rlNameData);
		}
		else
		{
			//there are name entries => let's have a look at typs and their data
			prlFoundTypeData = FindViewTypeData(&rlRawData);
			if(prlFoundTypeData == NULL) //***** OK
			{
				//View + name exist but no Type entries
				VIEW_TYPEDATA rlTypeData;
				VIEW_TEXTDATA rlTextData;
				rlTypeData.Type = CString(rlRawData.Type);
				if(strcmp(rlRawData.Type, "FILTER") == 0)
				{
					rlTextData.Page = CString(rlRawData.Page);
					MakeViewValues(rlRawData.Values, &rlTextData.omValues, "@");
					rlTypeData.omTextData.NewAt(rlTypeData.omTextData.GetSize(), rlTextData);
				}
				else
				{
					MakeViewValues(rlRawData.Values, &rlTypeData.omValues, "|");
				}
				prlFoundViewName->omTypeData.NewAt(prlFoundViewName->omTypeData.GetSize(), rlTypeData);
			}
			else
			{
				if(rlRawData.Type == CString("FILTER"))//***
				{   
					prlFoundTextData = FindViewTextData(&rlRawData);
					if(prlFoundTextData == NULL) //*** OK
					{
						//View + Name + Type exist but no values for Filter
						VIEW_TEXTDATA rlTextData;
						rlTextData.Page = CString(rlRawData.Page);
						MakeViewValues(rlRawData.Values, &rlTextData.omValues, "@");
						prlFoundTypeData->omTextData.NewAt(prlFoundTypeData->omTextData.GetSize(), rlTextData);
					}
					else
					{ 
						MakeViewValues(rlRawData.Values, &prlFoundTextData->omValues, "@");
					}
				}
				else
				{

				}
			}
		}
	}
			
}

/////////////////////////////////////////////////////////////////////
// MWO: Evaluates the existing of a view, e.g. Staffdia
VIEWDATA * CedaCfgData::FindViewData(CFGDATA *prlCfg)
{
	int ilCount = omViews.GetSize();

	for(int i = 0; i < ilCount; i++)
	{
		if(omViews[i].Ckey == prlCfg->Ckey)
		{
			return &omViews[i];
		}
	}
	return NULL;
}

//////////////////////////////////////////////////////////////////////////////
//MWO: Evaluates the existing of a viewname, e.g. Heute, Morgen
VIEW_VIEWNAMES * CedaCfgData::FindViewNameData(RAW_VIEWDATA *prpRawData)
{
	int ilCount = omViews.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		if(omViews[i].Ckey == prpRawData->Ckey)
		{
			int ilC2 = omViews[i].omNameData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				if(omViews[i].omNameData[j].ViewName == prpRawData->Name)
				{	
					return &omViews[i].omNameData[j];
				}
			}
		}
	}
	return NULL;
}

/////////////////////////////////////////////////////////////////////////////////
// MWO: Evaluates the existing of a viewtyp, e.g. FILTER, GROUP
VIEW_TYPEDATA * CedaCfgData::FindViewTypeData(RAW_VIEWDATA *prpRawData)
{
	int ilCount = omViews.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		if(omViews[i].Ckey == prpRawData->Ckey)
		{
			int ilC2 = omViews[i].omNameData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				if(omViews[i].omNameData[j].ViewName == CString(prpRawData->Name))
				{	
					int ilC3 = omViews[i].omNameData[j].omTypeData.GetSize();
					for(int k = 0; k < ilC3; k++)
					{
						if(omViews[i].omNameData[j].omTypeData[k].Type == CString(prpRawData->Type))
						{
							return  &omViews[i].omNameData[j].omTypeData[k];
						}
					}
				}
			}
		}
	}
	return NULL;
}
////////////////////////////////////////////////////////////////////////////////////
// MWO: Evaluates the existing of values for FILTER
VIEW_TEXTDATA * CedaCfgData::FindViewTextData(RAW_VIEWDATA *prpRawData)
{
	int ilCount = omViews.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		if(omViews[i].Ckey == prpRawData->Ckey)
		{
			int ilC2 = omViews[i].omNameData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				if(omViews[i].omNameData[j].ViewName == CString(prpRawData->Name))
				{	
					int ilC3 = omViews[i].omNameData[j].omTypeData.GetSize();
					for(int k = 0; k < ilC3; k++)
					{
						if(omViews[i].omNameData[j].omTypeData[k].Type == CString(prpRawData->Type))
						{
							int ilC4 = omViews[i].omNameData[j].omTypeData[k].omTextData.GetSize();
							for(int l = 0; l < ilC4; l++)
							{
								if(omViews[i].omNameData[j].omTypeData[k].omTextData[l].Page == CString(prpRawData->Page))
								{
									return &omViews[i].omNameData[j].omTypeData[k].omTextData[l];
								}
							}
						}
					}
				}
			}
		}
	}
	return NULL;
}

/////////////////////////////////////////////////////////////////////////
//MWO: Clear all buffers allocated in omViews
void CedaCfgData::ClearAllViews()
{
	while(omViews.GetSize() > 0)
	{
	//	VIEWDATA *prlView = &omViews[0];
		while(omViews[0].omNameData.GetSize() > 0)
		{
	//		VIEW_VIEWNAMES *prlViewName = &omViews[0].omNameData[0];
			while(omViews[0].omNameData[0].omTypeData.GetSize() > 0)
			{
	//			VIEW_TYPEDATA *prlViewType = &omViews[0].omNameData[0].omTypeData[0];
				while(omViews[0].omNameData[0].omTypeData[0].omTextData.GetSize() > 0)
				{
	//				VIEW_TEXTDATA *prlViewText = &omViews[0].omNameData[0].omTypeData[0].omTextData[0];
					while(omViews[0].omNameData[0].omTypeData[0].omTextData[0].omValues.GetSize() > 0)
					{
						CString *prlText = &omViews[0].omNameData[0].omTypeData[0].omTextData[0].omValues[0];
						omViews[0].omNameData[0].omTypeData[0].omTextData[0].omValues.DeleteAt(0);
					}
					omViews[0].omNameData[0].omTypeData[0].omTextData.DeleteAt(0);
				}
			//	omViews[0].omNameData[0].omTypeData[0].omValues.DeleteAll();
			
				while(omViews[0].omNameData[0].omTypeData[0].omValues.GetSize() > 0)
				{
					omViews[0].omNameData[0].omTypeData[0].omValues.DeleteAt(0);
				}
			
				omViews[0].omNameData[0].omTypeData.DeleteAt(0);
			}
			omViews[0].omNameData.DeleteAt(0);
		}
		omViews.DeleteAt(0);
	}
}
// Prepare some whatif data, not read from database
void CedaCfgData::PrepareCfgData(CFGDATA *prpCfg)
{
}


/////////////////////////////////////////////////////////////////////////////
// Update data methods (called from PrePlanTable class and StaffTable class)

bool CedaCfgData::InsertCfg(const CFGDATA *prpCfgData)
{

    if (CfgExist(prpCfgData->Urno))
	{
        return (UpdateCfgRecord(prpCfgData));
	}
    else
	{
        return (InsertCfgRecord(prpCfgData));
	}
}

bool CedaCfgData::UpdateCfg(const CFGDATA *prpCfgData)
{
    return(UpdateCfgRecord(prpCfgData));
}


BOOL CedaCfgData::CfgExist(long lpUrno)
{
	// don't read from database anymore, just check internal array
	CFGDATA *prpData;

	return(omUrnoMap.Lookup((void *) &lpUrno,(void *&)prpData) );
}

bool CedaCfgData::InsertCfgRecord(const CFGDATA *prpCfgData)
{
    // insert new Cfg record into table "SHFCKI"
    char *CfgFields = "URNO,CTYP,CKEY,VAFR,VATO,TEXT";
    char CfgData[512]; // CFGCKI ~ 430 Byte
    sprintf(CfgData, "%ld,%s,%s,%s,%s,%s,%s,%s",
        prpCfgData->Urno, 
		prpCfgData->Appn,
		prpCfgData->Ctyp,
		prpCfgData->Ckey,
		prpCfgData->Vafr,
		prpCfgData->Vato,
		prpCfgData->Pkno,
		prpCfgData->Text);

   return (CedaAction("IRT", "CFGCKI", CfgFields, "", "", CfgData));
}

bool CedaCfgData::UpdateCfgRecord(const CFGDATA *prpCfgData)
{
    // update Cfgl data in table "SHFCKI"
    // insert new Cfg record into table "SHFCKI"
    char *CfgFields = "CTYP,CKEY,VAFR,VATO,TEXT";
    char CfgData[512]; // SHFCKI ~ 140 byte
	char pclSelection[64];
    sprintf(CfgData, "%s,%s,%s,%s,%s,%s,%s",
		prpCfgData->Appn,
		prpCfgData->Ctyp,
		prpCfgData->Ckey,
		prpCfgData->Vafr,
		prpCfgData->Vato,
		prpCfgData->Pkno,
		prpCfgData->Text);
	sprintf(pclSelection," where URNO = '%ld%'",prpCfgData->Urno);
	bool olRc = CedaAction("URT", "CFGCKI", CfgFields, pclSelection, "", CfgData);
	//ogCCSDdx.DataChanged((void *)this,CFG_CHANGE,(void *)prpCfgData);
	return olRc;
}


/*
bool CedaCfgData::CreateCfgRequest(const CFGDATA *prpCfgData)
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
    // update Cfgl data in table "SHFCKI"
    // insert new Cfg record into table "SHFCKI"
    char *CfgFields = "CTYP,CKEY,VAFR,VATO,TEXT";
    char CfgData[512]; // SHFCKI ~ 140 byte
	char pclSelection[64];
    sprintf(CfgData, "%s,%s,%s,%s,%s,%s",
		prpCfgData->Ctyp,
		prpCfgData->Ckey,
		prpCfgData->Vafr,
		prpCfgData->Vato,
		prpCfgData->Text,
		prpCfgData->Pkno);
	sprintf(pclSelection,"'%s'",prpCfgData->Ckey);
	bool olRc = CedaAction("CKO", "CFGCKI", CfgFields, pclSelection, "", CfgData);
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	if (olRc == true)
	{
		MessageBox(NULL, CfgData, GetString(IDS_STRING910), MB_OK);
	}
	else
	{
		MessageBox(NULL, GetString(IDS_STRING911), GetString(IDS_STRING910), MB_OK);
	}
//	ogCCSDdx.DataChanged((void *)this,CFG_CHANGE,(void *)prpCfgData);
	return olRc;
}
*/

// the callback function for storing broadcasted fligthdata in FLIGHTDATA array

void  ProcessCfgCf(void *vpInstance,enum enumDDXTypes ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	 ogCfgData.ProcessCfgBc(ipDDXType,vpDataPointer,ropInstanceName);
}


void  CedaCfgData::ProcessCfgBc(enum enumDDXTypes ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{

	if ((ipDDXType == BC_CFG_CHANGE) || (ipDDXType == BC_CFG_INSERT))
	{
		CFGDATA *prpCfg;
		struct BcStruct *prlCfgData;

		prlCfgData = (struct BcStruct *) vpDataPointer;
		long llUrno = GetUrnoFromSelection(prlCfgData->Selection);

		if (omUrnoMap.Lookup((void *)llUrno,(void *& )prpCfg) == TRUE)
		{
			GetRecordFromItemList(prpCfg,
				prlCfgData->Fields,prlCfgData->Data);
			TRACE("ProcessCfgBc %s %s\n",prpCfg->Ckey,(LPCSTR)ropInstanceName);
			ogDdx.DataChanged((void *)this,CFG_CHANGE,(void *)prpCfg);
		}
		else
		{
			prpCfg = new CFGDATA;
			GetRecordFromItemList(prpCfg,
				prlCfgData->Fields,prlCfgData->Data);
			PrepareCfgData(prpCfg);
			omData.Add(prpCfg);
			omCkeyMap.SetAt(prpCfg->Ckey,prpCfg);
			omUrnoMap.SetAt((void *)prpCfg->Urno,prpCfg);
			ogDdx.DataChanged((void *)this,CFG_INSERT,(void *)prpCfg);
			TRACE("Process new Cfg %s %s\n",prpCfg->Ckey,(LPCSTR)ropInstanceName);
		}
	}
}

long  CedaCfgData::GetUrnoById(char *pclWiid)
{
	int ilWhatifCount = omData.GetSize();
	for ( int i = 0; i < ilWhatifCount; i++)
	{
		if (strcmp(omData[i].Ckey,pclWiid) == 0)
		{
			return omData[i].Urno;
		}
	}
	return 0L;
}

BOOL  CedaCfgData::GetIdByUrno(long lpUrno,char *pcpWiid)
{
	CFGDATA  *prlCfg;

	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlCfg) == TRUE)
	{
		//TRACE("GetCfgByUrno %ld \n",prlCfg->Urno);
		strcpy(pcpWiid,prlCfg->Ckey);
		return TRUE;
	}
	return FALSE;
}


CFGDATA  *CedaCfgData::GetCfgByUrno(long lpUrno)
{
	CFGDATA  *prlCfg;

	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlCfg) == TRUE)
	{
		//TRACE("GetCfgByUrno %ld \n",prlCfg->Urno);
		return prlCfg;
	}
	return NULL;
}

bool CedaCfgData::AddCfg(CFGDATA *prpCfg)
{
	CFGDATA *prlCfg = new CFGDATA;
	memcpy(prlCfg,prpCfg,sizeof(CFGDATA));
	prlCfg->IsChanged = DATA_NEW;

	omData.Add(prlCfg);
	omUrnoMap.SetAt((void *)prlCfg->Urno, prlCfg);
	omCkeyMap.SetAt(prlCfg->Ckey, prlCfg); 

	ogDdx.DataChanged((void *)this,CFG_INSERT,(void *)prlCfg);
	SaveCfg(prlCfg);
    return true;
}

bool CedaCfgData::ChangeCfgData(CFGDATA *prpCfg)
{
//	int ilLc;

	if (prpCfg->IsChanged == DATA_UNCHANGED)
	{
		prpCfg->IsChanged = DATA_CHANGED;
	}
	SaveCfg(prpCfg);

    return true;
}

bool CedaCfgData::DeleteCfg(long lpUrno)
{

	CFGDATA *prpCfg = GetCfgByUrno(lpUrno);
	if (prpCfg != NULL)
	{
		prpCfg->IsChanged = DATA_DELETED;

		omUrnoMap.RemoveKey((void *)lpUrno);
		omCkeyMap.RemoveKey(prpCfg->Ckey);

		SaveCfg(prpCfg);
	}
    return true;
}

bool CedaCfgData::SaveCfg(CFGDATA *prpCfg)
{

	bool olRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[824];

	if ((prpCfg->IsChanged == DATA_UNCHANGED) || (! bgOnline))
	{
		return true; // no change, nothing to do
	}
	switch(prpCfg->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		MakeCedaData(olListOfData,prpCfg);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpCfg->IsChanged = DATA_UNCHANGED;
		if(strcmp(prpCfg->Pkno, ogBasicData.omUserID)==0)
		{
			MakeCurrentUser();
		}
		break;
	case DATA_CHANGED:
		sprintf(pclSelection,"WHERE URNO = '%ld'",prpCfg->Urno);
		MakeCedaData(olListOfData,prpCfg);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		if (olRc != true)
		{
			prpCfg->IsChanged = DATA_NEW;
			SaveCfg(prpCfg);
		}
		else
		{
			prpCfg->IsChanged = DATA_UNCHANGED;
			//ogCCSDdx.DataChanged((void *)this,CFG_CHANGE,(void *)prpCfg);
			if(strcmp(prpCfg->Pkno, ogBasicData.omUserID)==0)
			{
				MakeCurrentUser();
			}
		}
		break;
	case DATA_DELETED:
		sprintf(pclSelection,"WHERE URNO = '%ld'",prpCfg->Urno);
		olRc = CedaAction("DRT",pclSelection);
		ogDdx.DataChanged((void *)this,CFG_DELETE,(void *)prpCfg);
/*		if (olRc == true)
		{
			for (int ilLc = 0; ilLc < omData.GetSize(); ilLc++)
			{
				if (omData[ilLc].Urno == prpCfg->Urno)
				{
					ogJobs.DeleteWhatIf(prpCfg->Urno);
					omData.DeleteAt(ilLc);
					break;
				}
			}
		}
*/
		MakeCurrentUser();
		break;
	}

	
    return true;

}

BOOL CedaCfgData::InterpretSetupString(CString popSetupString, 
									   USERSETUPDATA *prpSetupData)
{
	char *psp = NULL;
	char pclOriginal[1024]="";
	char pclStr[1024]="";
	char pclResult[256]="";

	char pclMONS[300]="";
	char pclRESO[300]="";
	char pclGACH[300]="";
	char pclSTCH[300]="";
	char pclCCCH[300]="";
	char pclFPTB[300]="";
	char pclSTTB[300]="";
	char pclGATB[300]="";
	char pclCCTB[300]="";
	char pclRQTB[300]="";
	char pclPKTB[300]="";
	char pclCVTB[300]="";
	char pclVPTB[300]="";
	char pclCFTB[300]="";
	char pclATTB[300]="";
	char pclINFO[300]="";
	char pclLHHS[300]="";
	char pclTACK[300]="";
	char pclFBCK[300]="";
	char pclSBCK[300]="";
	char pclSTCV[300]="";
	char pclGACV[300]="";
	char pclCCCV[300]="";
	char pclSTLV[300]="";
	char pclFPLV[300]="";
	char pclGBLV[300]="";
	char pclCCBV[300]="";
	char pclRQSV[300]="";
	char pclPEAV[300]="";
	char pclVPLV[300]="";
	char pclCONV[300]="";
	char pclATTV[300]="";

	strcpy(pclOriginal, popSetupString);

	// First we have to devide the incomming string into valid Commands

	// the parts of incomming string
	

	psp = strstr(pclOriginal, "#");
	while(psp != NULL)
	{
		char *pclTmp;
		char pclPart[2048];
		char pclRest[2048];
		strcpy(pclRest, psp+1);
		*psp = '\0';
		strcpy(pclPart, pclOriginal);
		strcpy(pclOriginal, pclRest);
		if((pclTmp = strstr(pclPart, "MONS=")) != NULL)
			strcpy(pclMONS, pclPart);
		else if((pclTmp = strstr(pclPart, "RESO=")) != NULL)
			strcpy(pclRESO, pclPart);
		else if((pclTmp = strstr(pclPart, "GACH=")) != NULL)
			strcpy(pclGACH, pclPart);
		else if((pclTmp = strstr(pclPart, "STCH=")) != NULL)
			strcpy(pclSTCH, pclPart);
		else if((pclTmp = strstr(pclPart, "CCCH=")) != NULL)
			strcpy(pclCCCH, pclPart);
		else if((pclTmp = strstr(pclPart, "FPTB=")) != NULL)
			strcpy(pclFPTB, pclPart);
		else if((pclTmp = strstr(pclPart, "STTB=")) != NULL)
			strcpy(pclSTTB, pclPart);
		else if((pclTmp = strstr(pclPart, "GATB=")) != NULL)
			strcpy(pclGATB, pclPart);
		else if((pclTmp = strstr(pclPart, "CCTB=")) != NULL)
			strcpy(pclCCTB, pclPart);
		else if((pclTmp = strstr(pclPart, "RQTB=")) != NULL)
			strcpy(pclRQTB, pclPart);
		else if((pclTmp = strstr(pclPart, "PKTB=")) != NULL)
			strcpy(pclPKTB, pclPart);
		else if((pclTmp = strstr(pclPart, "CVTB=")) != NULL)
			strcpy(pclCVTB, pclPart);
		else if((pclTmp = strstr(pclPart, "VPTB=")) != NULL)
			strcpy(pclVPTB, pclPart);
		else if((pclTmp = strstr(pclPart, "CFTB=")) != NULL)
			strcpy(pclCFTB, pclPart);
		else if((pclTmp = strstr(pclPart, "ATTB=")) != NULL)
			strcpy(pclATTB, pclPart);
		else if((pclTmp = strstr(pclPart, "INFO=")) != NULL)
			strcpy(pclINFO, pclPart);
		else if((pclTmp = strstr(pclPart, "LHHS=")) != NULL)
			strcpy(pclLHHS, pclPart);
		else if((pclTmp = strstr(pclPart, "TACK=")) != NULL)
			strcpy(pclTACK, pclPart);
		else if((pclTmp = strstr(pclPart, "FBCK=")) != NULL)
			strcpy(pclFBCK, pclPart);
		else if((pclTmp = strstr(pclPart, "SBCK=")) != NULL)
			strcpy(pclSBCK, pclPart);
		else if((pclTmp = strstr(pclPart, "STCV=")) != NULL)
			strcpy(pclSTCV, pclPart);
		else if((pclTmp = strstr(pclPart, "GACV=")) != NULL)
			strcpy(pclGACV, pclPart);
		else if((pclTmp = strstr(pclPart, "CCCV=")) != NULL)
			strcpy(pclCCCV, pclPart);
		else if((pclTmp = strstr(pclPart, "STLV=")) != NULL)
			strcpy(pclSTLV, pclPart);
		else if((pclTmp = strstr(pclPart, "FPLV=")) != NULL)
			strcpy(pclFPLV, pclPart);
		else if((pclTmp = strstr(pclPart, "GBLV=")) != NULL)
			strcpy(pclGBLV, pclPart);
		else if((pclTmp = strstr(pclPart, "CCBV=")) != NULL)
			strcpy(pclCCBV, pclPart);
		else if((pclTmp = strstr(pclPart, "RQSV=")) != NULL)
			strcpy(pclRQSV, pclPart);
		else if((pclTmp = strstr(pclPart, "PEAV=")) != NULL)
			strcpy(pclPEAV, pclPart);
		else if((pclTmp = strstr(pclPart, "VPLV=")) != NULL)
			strcpy(pclVPLV, pclPart);
		else if((pclTmp = strstr(pclPart, "CONV=")) != NULL)
			strcpy(pclCONV, pclPart);
		else if((pclTmp = strstr(pclPart, "ATTV=")) != NULL)
			strcpy(pclATTV, pclPart);

		psp = strstr(pclOriginal, "#");

	}


	
	psp = strstr(pclMONS, "=");
	if(psp == NULL)
	{
			prpSetupData->MONS = CString("1");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->MONS = CString("1");
		}
		else
		{
			prpSetupData->MONS =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclRESO, "=");
	if(psp == NULL)
	{
			prpSetupData->RESO = CString("1024x768");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->RESO = CString("1024x768");
		}
		else
		{
			prpSetupData->RESO =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclGACH, "=");
	if(psp == NULL)
	{
			prpSetupData->GACH  = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->GACH  = CString("L");
		}
		else
		{
			prpSetupData->GACH =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclSTCH, "=");
	if(psp == NULL)
	{
			prpSetupData->STCH = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->STCH = CString("L");
		}
		else
		{
			prpSetupData->STCH =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclCCCH, "=");
	if(psp == NULL)
	{
			prpSetupData->CCCH = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->CCCH = CString("L");
		}
		else
		{
			prpSetupData->CCCH =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclFPTB, "=");
	if(psp == NULL)
	{
			prpSetupData->FPTB = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->FPTB = CString("L");
		}
		else
		{
			prpSetupData->FPTB =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclSTTB, "=");
	if(psp == NULL)
	{
			prpSetupData->STTB = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->STTB = CString("L");
		}
		else
		{
			prpSetupData->STTB =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclGATB, "=");
	if(psp == NULL)
	{
			prpSetupData->GATB = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->GATB = CString("L");
		}
		else
		{
			prpSetupData->GATB =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclCCTB, "=");
	if(psp == NULL)
	{
			prpSetupData->CCTB = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->CCTB = CString("L");
		}
		else
		{
			prpSetupData->CCTB =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclRQTB, "=");
	if(psp == NULL)
	{
			prpSetupData->RQTB = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->RQTB = CString("L");
		}
		else
		{
			prpSetupData->RQTB =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclPKTB, "=");
	if(psp == NULL)
	{
			prpSetupData->PKTB = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->PKTB = CString("L");
		}
		else
		{
			prpSetupData->PKTB =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclCVTB, "=");
	if(psp == NULL)
	{
			prpSetupData->CVTB = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->CVTB = CString("L");
		}
		else
		{
			prpSetupData->CVTB =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclVPTB, "=");
	if(psp == NULL)
	{
			prpSetupData->VPTB = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->VPTB = CString("L");
		}
		else
		{
			prpSetupData->VPTB =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclCFTB, "=");
	if(psp == NULL)
	{
			prpSetupData->CFTB = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->CFTB = CString("L");
		}
		else
		{
			prpSetupData->CFTB =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclATTB, "=");
	if(psp == NULL)
	{
			prpSetupData->ATTB = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->ATTB = CString("L");
		}
		else
		{
			prpSetupData->ATTB =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclINFO, "=");
	if(psp == NULL)
	{
			prpSetupData->INFO = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->INFO = CString("L");
		}
		else
		{
			prpSetupData->INFO =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclLHHS, "=");
	if(psp == NULL)
	{
			prpSetupData->LHHS = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->LHHS = CString("L");
		}
		else
		{
			prpSetupData->LHHS =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclTACK, "=");
	if(psp == NULL)
	{
			prpSetupData->TACK = CString("J");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->TACK = CString("J");
		}
		else
		{
			prpSetupData->TACK =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclFBCK, "=");
	if(psp == NULL)
	{
			prpSetupData->FBCK = CString("J");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->FBCK =  CString(pclOriginal);
		}
		else
		{
			prpSetupData->FBCK =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclSBCK, "=");
	if(psp == NULL)
	{
			prpSetupData->SBCK = CString("J");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->SBCK = CString("J");
		}
		else
		{
			prpSetupData->SBCK =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclSTCV, "=");
	if(psp == NULL)
	{
			prpSetupData->STCV = CString("<Default>");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->STCV = CString("<Default>");
		}
		else
		{
			prpSetupData->STCV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclGACV, "=");
	if(psp == NULL)
	{
			prpSetupData->GACV = CString("<Default>");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->GACV = CString("<Default>");
		}
		else
		{
			prpSetupData->GACV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclCCCV, "=");
	if(psp == NULL)
	{
			prpSetupData->CCCV = CString("<Default>");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->CCCV = CString("<Default>");
		}
		else
		{
			prpSetupData->CCCV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclSTLV, "=");
	if(psp == NULL)
	{
			prpSetupData->STLV = CString("<Default>");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->STLV = CString("<Default>");
		}
		else
		{
			prpSetupData->STLV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclFPLV, "=");
	if(psp == NULL)
	{
			prpSetupData->FPLV = CString("<Default>");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->FPLV = CString("<Default>");
		}
		else
		{
			prpSetupData->FPLV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclGBLV, "=");
	if(psp == NULL)
	{
			prpSetupData->GBLV = CString("<Default>");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->GBLV = CString("<Default>");
		}
		else
		{
			prpSetupData->GBLV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclCCBV, "=");
	if(psp == NULL)
	{
			prpSetupData->CCBV = CString("<Default>");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->CCBV = CString("<Default>");
		}
		else
		{
			prpSetupData->CCBV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclRQSV, "=");
	if(psp == NULL)
	{
			prpSetupData->RQSV = CString("<Default>");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->RQSV = CString("<Default>");
		}
		else
		{
			prpSetupData->RQSV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclPEAV, "=");
	if(psp == NULL)
	{
			prpSetupData->PEAV = CString("<Default>");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->PEAV = CString("<Default>");
		}
		else
		{
			prpSetupData->PEAV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclVPLV, "=");
	if(psp == NULL)
	{
			prpSetupData->VPLV = CString("<Default>");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->VPLV = CString("<Default>");
		}
		else
		{
			prpSetupData->VPLV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclCONV, "=");
	if(psp == NULL)
	{
			prpSetupData->CONV = CString("<Default>");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->CONV = CString("<Default>");
		}
		else
		{
			prpSetupData->CONV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclATTV, "=");
	if(psp == NULL)
	{
			prpSetupData->ATTV = CString("<Default>");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->ATTV = CString("<Default>");
		}
		else
		{
			prpSetupData->ATTV =  CString(pclOriginal);
		}
	}
	psp = NULL;


	return TRUE;
}

void CedaCfgData::SetCfgData(void)
{
	//ogCommHandler.bmTurnaroundBars = ogCfgData.rmUserSetup.TACK[0] == 'J';
	//ogCommHandler.bmTurnaroundBars = ogCfgData.rmUserSetup.TACK[0] == 'J';
	//ogCommHandler.bmDemandsOnly = ogCfgData.rmUserSetup.FBCK[0] == 'N';
	//ogCommHandler.bmShowShadowBars = ogCfgData.rmUserSetup.SBCK[0] == 'J';
	
/*	CViewer rlViewer;
	rlViewer.SetViewerKey("GateDia");
	rlViewer.SelectView(ogCfgData.rmUserSetup.GACV);

	rlViewer.SetViewerKey("StaffDia");
	rlViewer.SelectView(ogCfgData.rmUserSetup.STCV);

	rlViewer.SetViewerKey("CciDia");
	rlViewer.SelectView(ogCfgData.rmUserSetup.CCCV);
	rlViewer.SetViewerKey("StaffTab");
	rlViewer.SelectView(ogCfgData.rmUserSetup.STLV);
	rlViewer.SetViewerKey("FltSched");
	rlViewer.SelectView(ogCfgData.rmUserSetup.FPLV);
	rlViewer.SetViewerKey("GateTab");
	rlViewer.SelectView(ogCfgData.rmUserSetup.GBLV);
	rlViewer.SetViewerKey("CciTab");
	rlViewer.SelectView(ogCfgData.rmUserSetup.CCBV);
	rlViewer.SetViewerKey("ReqTab");
	rlViewer.SelectView(ogCfgData.rmUserSetup.RQSV);
	rlViewer.SetViewerKey("ConfTab");
	rlViewer.SelectView(ogCfgData.rmUserSetup.CONV);
	rlViewer.SetViewerKey("PPTab");
	rlViewer.SelectView(ogCfgData.rmUserSetup.VPLV);
*/
}




bool CedaCfgData::ReadMonitorSetup()
{
	char pclWhere[2048]="";
	bool ilRc = true;
    sprintf(pclWhere, "WHERE APPN='CCS_%s' AND (CTYP = 'MONITORS' AND PKNO = '%s')", ogAppName, ogBasicData.omUserID);

	rmMonitorSetup.Urno = ogBasicData.GetNextUrno();
	sprintf(rmMonitorSetup.Appn, "CCS_%s", ogAppName);
	strcpy(rmMonitorSetup.Ctyp, "MONITORS");
	strcpy(rmMonitorSetup.Ckey, "");
	rmMonitorSetup.Vafr = CTime::GetCurrentTime();
	rmMonitorSetup.Vato = CTime(2020, 12, 31, 23, 59, 00);
	strcpy(rmMonitorSetup.Pkno, ogBasicData.omUserID);
	rmMonitorSetup.IsChanged = DATA_NEW;
	sprintf(rmMonitorSetup.Text, "%s=1#%s=1#%s=1#%s=1#%s=1#%s=1#%s=1#%s=1",
								MON_COUNT_STRING,			
								MON_SEASONSCHEDULE_STRING,	
								MON_DAILYSCHEDULE_STRING,	
								MON_SEASONROTDLG_STRING,	    
								MON_SEASONBATCH_STRING,	    
								MON_DAILYROTDLG_STRING,	    
								MON_FLIGHTDIA_STRING,	    
								MON_CCADIA_STRING);	    
	
	if (CedaAction("RT", pclWhere) == false)
        return false;

	//Initialize the datastructure of for the views
	CFGDATA *prlCfg = new CFGDATA;
	bool blFound = true;
    for (int ilLc = 0; ilRc == true; ilLc++)
    {
		if ((ilRc = GetBufferRecord(ilLc,prlCfg)) == false)
		{
			blFound = false;
/*			rmMonitorSetup.Urno = ogBasicData.GetNextUrno();
			sprintf(rmMonitorSetup.Appn, "CCS_%s", ogAppName);
			strcpy(rmMonitorSetup.Ctyp, "MONITORS");
			strcpy(rmMonitorSetup.Ckey, "");
			rmMonitorSetup.Vafr = CTime::GetCurrentTime();
			rmMonitorSetup.Vato = CTime(2020, 12, 31, 23, 59, 00);
			strcpy(rmMonitorSetup.Pkno, ogBasicData.omUserID);
			rmMonitorSetup.IsChanged = DATA_NEW;
			sprintf(rmMonitorSetup.Text, "%s=1#%s=1#%s=1#%s=1#%s=1#%s=1#%s=1",
										MON_COUNT_STRING,		
										MON_STAFF_BOARD_STRING,	
										MON_NORESOURCE_STRING,	
										MON_FLIGHTCHANGES_STRING,
										MON_FLIGHTSCHEDULE_STRING,
										MON_COVERAGE_STRING,		
										MON_RULES_STRING);
*/
		}
		else
		{
			rmMonitorSetup = *prlCfg;
			rmMonitorSetup.IsChanged = DATA_UNCHANGED;
		}
	}
	delete prlCfg;
    return true;
}

int CedaCfgData::GetMonitorForWindow(CString opWindow)
{
	int ilRet = 1;
	CStringArray olSetupList;
	ExtractItemList(CString(rmMonitorSetup.Text), &olSetupList, '#');
	for(int i = 0; i < olSetupList.GetSize(); i++)
	{
		if(olSetupList[i].Find(opWindow) != -1)
		{
			int ilPos = olSetupList[i].Find("=");
			if(ilPos != -1)
			{
				char pclTmp[100];
				char x[111];
				CString olString = olSetupList[i];
				strcpy(x, olString);
				sprintf(pclTmp, "%c", olString[ilPos+1]);
				ilRet = atoi(pclTmp);
			}
		}
	}
	return ilRet;
}

int CedaCfgData::GetMonitorCount()
{
	int ilRet = 1;
	CStringArray olSetupList;
	ExtractItemList(CString(rmMonitorSetup.Text), &olSetupList, '#');
	for(int i = 0; i < olSetupList.GetSize(); i++)
	{
		if(olSetupList[i].Find(MON_COUNT_STRING) != -1)
		{
			int ilPos = olSetupList[i].Find("=");
			if(ilPos != -1)
			{
				char pclTmp[100];
				CString olString = olSetupList[i];
				sprintf(pclTmp, "%c", olString[ilPos+1]);
				ilRet = atoi(pclTmp);
			}
		}
	}
	return ilRet;
}


// 050311 MVy: helper function to get configuration strings from db
bool CedaCfgData::ReadConfigItem( char* pcDBConfigItemName, CFGDATA& refCfgData, bool bPKNO )
{
	ASSERT( pcDBConfigItemName );

	char pclWhere[2048]="" ;
	int iLen = sprintf( pclWhere, "WHERE APPN='CCS_%s' AND CTYP = '%s'", ogAppName, pcDBConfigItemName );
	if( bPKNO ) iLen = sprintf( pclWhere + iLen, " AND PKNO = '%s'", ogBasicData.omUserID );		// append where PKNO
	
	refCfgData.Urno = ogBasicData.GetNextUrno();
	sprintf( refCfgData.Appn, "CCS_%s", ogAppName );
	strcpy( refCfgData.Ctyp, pcDBConfigItemName );
	strcpy( refCfgData.Ckey, "" );
	if( bPKNO ) 
	{
		refCfgData.Vafr = CTime::GetCurrentTime();
		refCfgData.Vato = CTime( 2020, 12, 31, 23, 59, 00 );
	};
	strcpy( refCfgData.Pkno, ogBasicData.omUserID );
	refCfgData.IsChanged = DATA_NEW ;
	sprintf( refCfgData.Text, "" );
	
	if( CedaAction( "RT", pclWhere ) == false ) return false ;

	//Initialize the datastructure of for the views
	CFGDATA *prlCfg = new CFGDATA ;
	bool blFound = true ;
	bool ilRc = true ;
	for( int ilLc = 0; ilRc == true; ilLc++ )
	{
		if( ( ilRc = GetBufferRecord( ilLc,prlCfg ) ) == false )
			blFound = false;
		else
		{
			refCfgData = *prlCfg ;
			refCfgData.IsChanged = DATA_UNCHANGED ;
		}
	}
	delete prlCfg ;

	return true ;
};	// ReadConfigItem



bool CedaCfgData::ReadFontSetup()
{
	bool bResult = ReadConfigItem( "FONT", rmFontSetup );
	if( !bResult ) return false ;
	if(!CString(rmFontSetup.Text).IsEmpty())
	{
		CStringArray olValues;
		if(ExtractItemList(CString(rmFontSetup.Text), &olValues, '#') == 14)
		{
			ogSetupFont.DeleteObject();
			ogSetupFont.CreateFont( atoi(olValues[0]), atoi(olValues[1]), atoi(olValues[2]), atoi(olValues[3]), atoi(olValues[4]), atoi(olValues[5]), atoi(olValues[6]), atoi(olValues[7]), atoi(olValues[8]), atoi(olValues[9]), atoi(olValues[10]), atoi(olValues[11]), atoi(olValues[12]), olValues[13]);
		}
	}
	return true;
}

bool CedaCfgData::GetFlightConfig()
{
	/*
	CString olTmp;
	CStringArray olArrray;

	for (int i = pomUfisCedaConfig->GetSize() - 1; i >= 0; i--)
	{
		olTmp = (*pomUfisCedaConfig)[i];
		if (olTmp.Left(5) == "UTCD,")
		{
			olArrray.RemoveAll();
			ExtractItemList(olTmp, &olArrray);
		
			if(olArrray.GetSize() > 1)
			{
				ogUtcDiff = CTimeSpan(0, 0, atoi(olArrray[1]),0);
			}
		}
		if (olTmp.Left(5) == "DSSF,")
		{
			ogDssfFields.RemoveAll();
			ExtractItemList(olTmp.Right(olTmp.GetLength() - 5), &ogDssfFields);
		}

	}

	// calculate the index of the FLTI-Field in the DSSF string
	for (i = 0; i < ogDssfFields.GetSize(); i++)
	{
		if (ogDssfFields[i] == "FLTI")
		{
			ogDssfFltiId = i;
			break;
		}
	}
	*/

	CString olTmp;
	CStringArray olArrray;

	// read type of the bcstruct
	char pclTable[64] = "AFT";
	char pclFieldList[1024] = "";
	char pclData[3000] = "";
	char pclSelection[256] = "[CONFIG]";

	bool blRet = CedaAction("GFR", pclTable, pclFieldList, pclSelection, "", pclData, "BUF1");

	egStructBcfields = EXT_CHANGED_NO;
	bool blFLTI_PRIO = false;

	// debug: dump buffer
	/*{
		TRACE( "CedaCfgData::GetFlightConfig(): Buffer has %d entries:\n", GetBufferSize() );
		for(int k = GetBufferSize() - 1; k >= 0; k--)
		{
			olTmp = omDataBuf[k];
			TRACE( "   %4d: %s\n", k, olTmp );
		};
		TRACE( "--\n" );
	}*/

	CStringArray* olDataBuf = GetDataBuff();
	for (int k = olDataBuf->GetSize() - 1; k >= 0; k--)
	{
		olTmp = olDataBuf->GetAt(k);
//	TEST	olTmp="FLTI_PRIO,ESD";

		if (olTmp == "EXT_CHANGED_OLDVAL" || olTmp == "BC_OUT_MODE_OLDVAL")
		{
			egStructBcfields = EXT_CHANGED_OLDVAL;
		}
		else if (olTmp == "EXT_CHANGED_NEWVAL" || olTmp == "BC_OUT_MODE_NEWDVAL")
		{
			egStructBcfields = EXT_CHANGED_NEWVAL;
		}
		else if (olTmp.Find("MAX_AFT_REC") != -1)
		{
			CStringArray olStrArray;
			ExtractItemList(CString(olTmp), &olStrArray);
			if(olStrArray.GetSize() == 2)
			{
				int ilTmp = atoi(olStrArray[1]) - 5;
				igMaxTowings = max(igMaxTowings,ilTmp);
			}
		}
		else if (olTmp.Find("LKEY") != -1)
		{
			CStringArray olStrArray;
			ExtractItemList(CString(olTmp), &olStrArray);
			if(olStrArray.GetSize() == 2)
			{
				ogLkeyFlight = olStrArray[1];
			}
		}
		else if (olTmp.Find("FLTI_PRIO") != -1)
		{
			CStringArray olStrArray;
			ExtractItemList(CString(olTmp), &olStrArray);
			if(olStrArray.GetSize() == 2)
			{
				ogFltiPrio = olStrArray[1];
				blFLTI_PRIO = true;
			}
		}
		else if (olTmp.Left(5) == "UTCD,")
		{
			olArrray.RemoveAll();
			ExtractItemList(olTmp, &olArrray);
		
			if(olArrray.GetSize() > 1)
			{
				ogUtcDiff = CTimeSpan(0, 0, atoi(olArrray[1]),0);
			}
		}
		else if (olTmp.Left(5) == "DSSF,")
		{
			ogDssfFields.RemoveAll();
			ExtractItemList(olTmp.Right(olTmp.GetLength() - 5), &ogDssfFields);
		}
	}

	if (blFLTI_PRIO && ogFltiPrio.IsEmpty())
	{
		ogFltiPrio = "?";
		::MessageBox(NULL,"FlightConfig: FLTI_PRIO hasn't Values" ,"ERROR FlightConfig",MB_OK);
	}

	// calculate the index of the FLTI-Field in the DSSF string
	/*for (int i = 0; i < ogDssfFields.GetSize(); i++)
	{
		if (ogDssfFields[i] == "FLTI")
		{
			ogDssfFltiId = i;
			break;
		}
	}*/

	for (int i = 0; i < ogDssfFields.GetSize(); i++)
	{
		if (ogDssfFields[i] == "FLTI")
		{
			ogDssfFltiId = i;
			
		}

		if (ogDssfFields[i] == "TTYP")
		{
			ogDssfTtypId = i;
			
		}
	}

	return true;
}



CString CedaCfgData::GetLkey()
{

	CString olTmp;
	CStringArray olArrray;
	CString olLkey;

	for (int i = pomUfisCedaConfig->GetSize() - 1; i >= 0; i--)
	{
		olTmp = (*pomUfisCedaConfig)[i];
		olArrray.RemoveAll();
		ExtractItemList(olTmp, &olArrray);
		
		if(olArrray.GetSize() > 0)
		{
			if(olArrray[0] == "LKEY")
			{
				olLkey = olArrray[1];
				break;
			}
		}
	}
	return olLkey;

}


bool CedaCfgData::ReadPopsReportDailyAlcSetup()
{
	bool bResult = ReadConfigItem( "POPS_REPORT_DAILY_ALC", rmPopsReportDailyAlcSetup, false );
	// there were really no data set in original code !
	return bResult ;
}

CString CedaCfgData::GetPopsReportDailyAlcSetup()
{
	return rmPopsReportDailyAlcSetup.Text;
}


bool CedaCfgData::ReadConflictSetup()
{
	bool bResult = ReadConfigItem( "CONFLICT_SETUP", rmConflictSetup );
	// there were really no data set in original code !
	return bResult ;
}

CString CedaCfgData::GetConflictSetup()
{
	return rmConflictSetup.Text;
}


bool CedaCfgData::ReadGateBufferTimeSetup()
{
	bool bResult = ReadConfigItem( "GATEBUFFERTIME", rmGateBufferTimeSetup );
	if( bResult ) ogGateAllocBufferTime = CTimeSpan( 0,0, atoi( rmGateBufferTimeSetup.Text ), 0 );
	return bResult ;
}

CString CedaCfgData::GetGateBufferTimeSetup()
{
	return rmGateBufferTimeSetup.Text;
}

bool CedaCfgData::ReadPosBufferTimeSetup()
{
	bool bResult = ReadConfigItem( "POSBUFFERTIME", rmPosBufferTimeSetup );
	if( bResult ) ogPosAllocBufferTime = CTimeSpan( 0,0, atoi( rmPosBufferTimeSetup.Text ), 0 );
	return bResult ;
}

CString CedaCfgData::GetPosBufferTimeSetup()
{
	return rmPosBufferTimeSetup.Text;
}

//PRF 8379  
bool CedaCfgData::ReadXminutesBufferTimeSetup()
{
	bool bResult = ReadConfigItem( "XMINUTESBUFFERTIME", rmXminutesBufferTimeSetup );
	if( bResult ) ogXminutesBufferTime = CTimeSpan( 0,0, atoi( rmXminutesBufferTimeSetup.Text ), 0 );
	return bResult ;
}
CString CedaCfgData::GetXminutes()
{
	return rmXminutesBufferTimeSetup.Text;
}


bool CedaCfgData::ReadArchivePastBufferTimeSetup()
{
	bool bResult = ReadConfigItem( "ARCHIVEPASTBUFFERTIME", rmArchivePastBufferTimeSetup );
	if( bResult ) ogTimeSpanPostFlight = CTimeSpan( atoi( rmArchivePastBufferTimeSetup.Text ), 0,0,0 );
	return bResult ;
}

CString CedaCfgData::GetArchivePastBufferTimeSetup()
{
	return rmArchivePastBufferTimeSetup.Text;
}

bool CedaCfgData::ReadResChainDlgSetup()
{
	bool bResult = ReadConfigItem( "RESCHAINDLG", rmResChainDlg );
	if( bResult ) bgShowGATPOS = CString( rmResChainDlg.Text) != "0" ;
	return bResult ;
}

CString CedaCfgData::GetResChainDlgSetup()
{
	return rmResChainDlg.Text;
}


bool CedaCfgData::ReadArchiveFutureBufferTimeSetup()
{
	bool bResult = ReadConfigItem( "ARCHIVEFUTUREBUFFERTIME", rmArchiveFutureBufferTimeSetup );
	if( bResult ) ogTimeSpanPostFlightInFuture = CTimeSpan( atoi( rmArchiveFutureBufferTimeSetup.Text ), 0,0,0 );
	return bResult ;
}

CString CedaCfgData::GetArchiveFutureBufferTimeSetup()
{
	return rmArchiveFutureBufferTimeSetup.Text;
}

// 050309 MVy: today only or period, read from DB
bool CedaCfgData::ReadArchivePostFlightTodayOnly()
{
	ASSERT( __CanHandlePositionFlightTodayOnlyCheck() );		// 050310 MVy: today only, who calls me if not configured ???
	bool bResult = ReadConfigItem( "ARCHIVEPOSTFLIGHTTODAYONLY", rmArchivePostFlightTodayOnly );
	ASSERT( rmArchivePostFlightTodayOnly.Text[0] == '1' || rmArchivePostFlightTodayOnly.Text[0] == '0' || !*rmArchivePostFlightTodayOnly.Text );
	if( bResult ) ogArchivePostFlightTodayOnly = atoi( rmArchivePostFlightTodayOnly.Text );
	return bResult ;
};	// ReadArchivePostFlightTodayOnly

// 050309 MVy: today only or period, get current state
CString CedaCfgData::GetArchivePostFlightTodayOnly()
{
	return rmArchivePostFlightTodayOnly.Text ;
};	// GetArchivePostFlightTodayOnly


// 050311 MVy: PRF6903: Check-In Counter Restrictions for Handling Agents, read from DB
//	TODO: DB contains field:
//		if text starts with digit interprete as HAG.URNO
//		if text starts with letter interprete as HAG.HNAM
bool CedaCfgData::ReadHandlingAgent()
{
	ASSERT( __CanHandleCheckinCounterRestrictionsForHandlingAgents() );		// 050311 MVy: Handling Agents, who calls me if not configured ???
	bool bResult = ReadConfigItem( "HANDLINGAGENT", rmHandlingAgent );
	if( bResult )
	{
		// 050331 MVy: more variability for future expansions
		CString s = rmHandlingAgent.Text ;
		g_ulHandlingAgent = atoi( ExtractValue( s, "HAG.URNO=", '#' ) );	// Agent
		g_rgbHandlingAgentOccBkBar = ToLong( ExtractValue( s, "COLOR.BKBAR=", '#' ) );		// Backbar color
		g_ulHandlingTask = atoi( ExtractValue( s, "HTY.URNO=", '#' ) );		// Task
		UpdateHandlingAgentAirlineCodes();
		/*
		// there are some items in this config string
		//	1.urno of the agent, HAG.URNO
		//	2.color of the occupation backbar, hex(RGB)
		//	3.urno of the task, HTY.URNO
		CStringArray olValues ;
		if( int size = ExtractItemList( rmHandlingAgent.Text, &olValues, '#' ) )
		{
			g_ulHandlingAgent = atoi( olValues[0] );
			if( size > 0 ) g_rgbHandlingAgentOccBkBar = ToLong( olValues[1] );
			if( size > 1 ) g_ulHandlingTask = atoi( olValues[2] );
			UpdateHandlingAgentAirlineCodes();
		};
		*/
	};
	return bResult ;
};	// ReadHandlingAgent

// 050331 MVy: this function will create the string put into database
void CedaCfgData::CreateCfgStringForHandlingAgent()
{
	int len = 0 ;
	char buffer[100];
	char terminator = '#' ;
	char* pc = buffer ;
	len = sprintf( pc += len, "HAG.URNO=%d%c", g_ulHandlingAgent, terminator );
	len = sprintf( pc += len, "COLOR.BKBAR=0x%0.8x%c", g_rgbHandlingAgentOccBkBar, terminator );
	len = sprintf( pc += len, "HTY.URNO=%d%c", g_ulHandlingTask, terminator );
	ASSERT( pc - buffer + len < 2001 /*sizeof( CFGDATA::Text )*/ );
	strcpy( rmHandlingAgent.Text, buffer );
};	// CreateCfgStringForHandlingAgent

// 050318 MVy: if handling agent or handling task will change the depending airline codes must be updated by calling this function
int CedaCfgData::UpdateHandlingAgentAirlineCodes()
{
	return GetAirlineCodes( g_ulHandlingAgent, g_ulHandlingTask, g_strarrHandlingAgentAirlinesCodes );
};	// UpdateHandlingAgentAirlineCodes


//-OO-//// 050311 MVy: PRF6903: Check-In Counter Restrictions for Handling Agents, get current state
//-OO-//CString CedaCfgData::GetHandlingAgent()
//-OO-//{
//-OO-//	return rmHandlingAgent.Text ;
//-OO-//};	// GetHandlingAgent


/*
int CedaCfgData::GetBufferTimeByString(const CString& opStr)
{
	int ilRet = 1;
	CStringArray olSetupList;
	ExtractItemList(CString(rmBufferTimeSetup.Text), &olSetupList, '#');
	for(int i = 0; i < olSetupList.GetSize(); i++)
	{
		if(olSetupList[i].Find(opStr) != -1)
		{
			int ilPos = olSetupList[i].Find("=");
			if(ilPos != -1)
			{
				CString olString = olSetupList[i];
				int ilLength = olString.GetLength()-1;
				olString = olString.Right(ilLength - ilPos);
				ilRet = atoi(olString);
			}
		}
	}
	return ilRet;
}
*/


void CedaCfgData::GetDefaultConflictSetup(CStringArray &opLines)
{
	opLines.RemoveAll();
	CString olConflict; 
	CString olText;

	int i = 0;
	NUMCONFLICTS = i;
//flight
	// line 0	= flight data changed  IDS_STRING1550
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1550,GetString(IDS_STRING2307),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 28	= Aircrafttype not found IDS_STRING1713
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1713,GetString(IDS_STRING2360),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 23	= Return Flight  IDS_STRING1697
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1697,GetString(IDS_STRING2355),0xFFFF,1 << 0|1 << 1|1 << 2);
	opLines.Add(CString(olConflict));

	// line 25	= Returned from Taxi  IDS_STRING1777
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1777,GetString(IDS_STRING2357),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 24	= diverted  IDS_STRING1776
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1776,GetString(IDS_STRING2356),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 1	= start-landing order  IDS_STRING1430
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1430,GetString(IDS_STRING2308),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));
 
	// line 26	= Departure/Towing before Arrival IDS_STRING1890
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1890,GetString(IDS_STRING2358),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 27	= Towing can not go off block if the flight is not on block! IDS_STRING1981
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1981,GetString(IDS_STRING2359),0xFFFF,1 << 0|1 << 1);
	opLines.Add(CString(olConflict));

	// line 2	= No onblock at touch down + n minutes IDS_STRING1236
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1236,GetString(IDS_STRING2309),15,1 << 0|1 << 1);
	opLines.Add(CString(olConflict));

	// line 8	= No take off time at offblock + n minutes    IDS_STRING1239
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1239,GetString(IDS_STRING2315),15,1 << 0|1 << 1);
	opLines.Add(CString(olConflict));

	// line 9	= No offblock at STD + n minutes  IDS_STRING1240
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1240,GetString(IDS_STRING2316),5,1 << 0);
	opLines.Add(CString(olConflict));

	// line 15	= No offblock at CTOT + n minutes   IDS_STRING2354
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2354,GetString(IDS_STRING2322),-15,1 << 0);
	opLines.Add(CString(olConflict));

	// line 3	= Actual time > ETA + n minutes   IDS_STRING1237
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1237,GetString(IDS_STRING2310),5,1 << 0);
	opLines.Add(CString(olConflict));

	// line 4	= Actual time + n minutes > next information time (Arrival)  IDS_STRING1238
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1238,GetString(IDS_STRING2311),-5,1 << 0);
	opLines.Add(CString(olConflict));

	// line 11	= Actual time + n minutes > next information time (Departure)  IDS_STRING1242
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1242,GetString(IDS_STRING2318),-5,1 << 0);
	opLines.Add(CString(olConflict));

	// line 5	= Pos:() A/C() error () from parking system  IDS_STRING1428
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1428,GetString(IDS_STRING2312),0xFFFF,0);
	opLines.Add(CString(olConflict));

	// line 6	= STA / ETA >= STD / ETD   IDS_STRING1432
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1432,GetString(IDS_STRING2313),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 7	= STD / ETD + n minutes < actual time and no DEP telegram received  IDS_STRING1549
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1549,GetString(IDS_STRING2314),5,1 << 0);
	opLines.Add(CString(olConflict));

	// line 10	= | ETD - STD | > n minutes  IDS_STRING1241
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1241,GetString(IDS_STRING2317),15,1 << 0);
	opLines.Add(CString(olConflict));

	// line 12	= STD / ETD - n minutes < actual time and no boarding information received IDS_STRING1548
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1548,GetString(IDS_STRING2319),30,1 << 0);
	opLines.Add(CString(olConflict));

	// line 13	= | ETA - STA | > n minutes  IDS_STRING1860
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1860,GetString(IDS_STRING2320),15,1 << 0);
	opLines.Add(CString(olConflict));

	// line 14	= CTOT > STD / ETD + n minutes  IDS_STRING2353
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2353,GetString(IDS_STRING2321),-10,1 << 0);
	opLines.Add(CString(olConflict));

	// line 60	= Special Requirements
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2572,GetString(IDS_STRING2572),0xFFFF,1 << 0|1 << 6);
	opLines.Add(CString(olConflict));

//position
	// line 29	= Position not available IDS_STRING1714
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1714,GetString(IDS_STRING2361),0xFFFF,1 << 0|1 << 1);
	opLines.Add(CString(olConflict));

	// line 39	= Too many aircrafts at position %s: %d Max: %d! IDS_STRING1722
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1722,GetString(IDS_STRING2371),0xFFFF,1 << 0|1 << 1);
	opLines.Add(CString(olConflict));

	// line 31	= Global nature ristriction (%s) at position %s: %s IDS_STRING1712
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1712,GetString(IDS_STRING2363),0xFFFF,1 << 0|1 << 1);
	opLines.Add(CString(olConflict));

	// line 33	= Position %s: Nature ristriction (%s): %s! IDS_STRING1716
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1716,GetString(IDS_STRING2365),0xFFFF,1 << 0|1 << 1);
	opLines.Add(CString(olConflict));

	// line 34	= Position %s: Aircraft restriction (%s): %s! IDS_STRING1717
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1717,GetString(IDS_STRING2366),0xFFFF,1 << 0|1 << 1);
	opLines.Add(CString(olConflict));

	// line 35	= Position %s: Airline restriction (%s): %s! IDS_STRING1718
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1718,GetString(IDS_STRING2367),0xFFFF,1 << 0|1 << 1);
	opLines.Add(CString(olConflict));

	// line 35.1	= Position %s: Service restriction (%s): %s! IDS_STRING2631
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2631,GetString(IDS_STRING2640),0xFFFF,1 << 0|1 << 1);
	opLines.Add(CString(olConflict));

	// line 59	= Aircraft-Restriction/Wingoverlap with aircraft (%s %s) at position %s! IDS_STRING1723
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1723,GetString(IDS_STRING2392),0xFFFF,1 << 0|1 << 1);
	opLines.Add(CString(olConflict));

	// line 36	= Aircraft wingspan at position %s: %.2f (%.2f-%.2f) %s %s! IDS_STRING1719
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1719,GetString(IDS_STRING2368),0xFFFF,1 << 0|1 << 1);
	opLines.Add(CString(olConflict));

	// line 37	= Aircraft height at position %s: %.2f (%.2f-%.2f) %s/%s! IDS_STRING1720
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1720,GetString(IDS_STRING2369),0xFFFF,1 << 0|1 << 1);
	opLines.Add(CString(olConflict));

	// line 38	= Aircraft lenght at position %s: %.2f (%.2f-%.2f) %s/%s! IDS_STRING1721
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1721,GetString(IDS_STRING2370),0xFFFF,1 << 0|1 << 1);
	opLines.Add(CString(olConflict));

	// line 32	= Pushback conflict at position %s with flight %s! IDS_STRING1816
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1816,GetString(IDS_STRING2364),0xFFFF,0);
	opLines.Add(CString(olConflict));

	// line 30	= Position %s: Flight ID from the related Gate is %s must be %s IDS_STRING1715
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1715,GetString(IDS_STRING2362),0xFFFF,1 << 0|1 << 1|1 << 2);
	opLines.Add(CString(olConflict));

	// line 16	= Position - Gate relation  IDS_STRING2303  (IDS_STRING2001)
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2303,GetString(IDS_STRING2323),0xFFFF,1 << 0|1 << 1|1 << 2);
	opLines.Add(CString(olConflict));

	// Position Dest %s: (%s): %s 
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2834,GetString(IDS_STRING2835),0xFFFF,1 << 0|1 << 1);
	opLines.Add(CString(olConflict));


//gate
	// line 41	= Gate %s not available %s - %s reason: %s! IDS_STRING1724
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1724,GetString(IDS_STRING2373),0xFFFF,1 << 0|1 << 2);
	opLines.Add(CString(olConflict));

	// line 44	= To many aircrafts at gate %s: %d Max: %d! IDS_STRING1726
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1726,GetString(IDS_STRING2376),0xFFFF,1 << 0|1 << 2);
	opLines.Add(CString(olConflict));

	// line 47	= Max Passenger of Gate %s: %s (%s)! IDS_STRING1729
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1729,GetString(IDS_STRING2379),0xFFFF,1 << 0|1 << 2);
	opLines.Add(CString(olConflict));

	// line 50	= Global nature ristriction (%s) at gate %s: %s! IDS_STRING1732
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1732,GetString(IDS_STRING2382),0xFFFF,1 << 0|1 << 2);
	opLines.Add(CString(olConflict));

	// line 49	= Gate %s: Nature restriction (%s): %s! IDS_STRING1731
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1731,GetString(IDS_STRING2381),0xFFFF,1 << 0|1 << 2);
	opLines.Add(CString(olConflict));

	// line 48	= Gate %s: Airline restriction (%s): %s! IDS_STRING1730
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1730,GetString(IDS_STRING2380),0xFFFF,1 << 0|1 << 2);
	opLines.Add(CString(olConflict));

	// line 51	= Gate %s: Orig/Dest restriction (%s): %s! IDS_STRING2006
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2006,GetString(IDS_STRING2383),0xFFFF,1 << 0|1 << 2);
	opLines.Add(CString(olConflict));

	// line 51.1	= Gate %s: Service restriction (%s): %s IDS_STRING2633
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2633,GetString(IDS_STRING2639),0xFFFF,1 << 0|1 << 2);
	opLines.Add(CString(olConflict));

	// line 43	= Diffrent Flight ID for Gate %s: % -> Can be %s ! IDS_STRING2300
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2300,GetString(IDS_STRING2375),0xFFFF,1 << 0|1 << 2);
	opLines.Add(CString(olConflict));

	// line 42	= Flight ID for Gate %s: % must be %s ! IDS_STRING1725
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1725,GetString(IDS_STRING2374),0xFFFF,1 << 0|1 << 2);
	opLines.Add(CString(olConflict));

	// line 40	= Invalid 'Pass. Boarding Bridge' - 'Bus Gate' Relation! IDS_STRING2065
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2065,GetString(IDS_STRING2372),0xFFFF,1 << 0|1 << 2);
	opLines.Add(CString(olConflict));

	// line 45	= Gate %s can not be use for inbound ! IDS_STRING1727
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1727,GetString(IDS_STRING2377),0xFFFF,1 << 0|1 << 2);
	opLines.Add(CString(olConflict));

	// line 46	= Gate %s can not be use for outbound ! IDS_STRING1728
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1728,GetString(IDS_STRING2378),0xFFFF,1 << 0|1 << 2);
	opLines.Add(CString(olConflict));

//belt
	// line 54	= Baggage belt %s not available %s - %s reason: %s! IDS_STRING1735
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1735,GetString(IDS_STRING2386),0xFFFF,1 << 0|1 << 4);
	opLines.Add(CString(olConflict));

	// line 55	= To many aircrafts at baggage belt %s: %d Max: %d! IDS_STRING1736
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1736,GetString(IDS_STRING2387),0xFFFF,1 << 0|1 << 4);
	opLines.Add(CString(olConflict));

	// line 55.1	= Too many Passengers at Baggage Belt %s: %d Max: %d! IDS_STRING2634
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2634,GetString(IDS_STRING2641),0xFFFF,1 << 0|1 << 4);
	opLines.Add(CString(olConflict));

	// line 56	= Global Nature Restriction (%s) at Belt %s: %s! IDS_STRING2339
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2339,GetString(IDS_STRING2388),0xFFFF,1 << 0|1 << 4);
	opLines.Add(CString(olConflict));

	// line 56.1	= Baggage Belt %s: Nature restriction (%s): %s! IDS_STRING2637
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2637,GetString(IDS_STRING2644),0xFFFF,1 << 0|1 << 4);
	opLines.Add(CString(olConflict));

	// line 56.2	= Baggage Belt %s: Airline restriction (%s): %s! IDS_STRING2635
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2635,GetString(IDS_STRING2642),0xFFFF,1 << 0|1 << 4);
	opLines.Add(CString(olConflict));

	// line 56.3	= Baggage Belt %s: Origin restriction (%s): %s! IDS_STRING2636
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2636,GetString(IDS_STRING2643),0xFFFF,1 << 0|1 << 4);
	opLines.Add(CString(olConflict));

	// line 56.4	= Baggage Belt %s: Service restriction (%s): %s! IDS_STRING2638
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2638,GetString(IDS_STRING2645),0xFFFF,1 << 0|1 << 4);
	opLines.Add(CString(olConflict));

//lounge
	// line 57	= Waiting Room not available %s - %s reason: %s! IDS_STRING2204
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2204,GetString(IDS_STRING2389),0xFFFF,1 << 0|1 << 5);
	opLines.Add(CString(olConflict));

	// line 52	= Too many flights at lounge %s: %d Max: %d! IDS_STRING1734
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1734,GetString(IDS_STRING2384),0xFFFF,1 << 0|1 << 5);
	opLines.Add(CString(olConflict));

	// line 62	= Too many Passengers at Lounge %s: %d Max: %d. IDS_STRING2424
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2424,GetString(IDS_STRING2646),0xFFFF,1 << 0|1 << 5);
	opLines.Add(CString(olConflict));

	// line 61	= Flight ID for Lounge %s must be %s. IDS_STRING2410
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2410,GetString(IDS_STRING2647),0xFFFF,1 << 0|1 << 5);
	opLines.Add(CString(olConflict));

	// line 53	= Diffrent Flight ID for Lounge %s: %! IDS_STRING2301
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2301,GetString(IDS_STRING2385),0xFFFF,1 << 0|1 << 5);
	opLines.Add(CString(olConflict));

	// line 58	= Global Nature Restriction (%s) at Lounge %s: %s! IDS_STRING2338
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2338,GetString(IDS_STRING2390),0xFFFF,1 << 0|1 << 5);
	opLines.Add(CString(olConflict));

//gatpos
	// line 17	= Gate - Lounge relation IDS_STRING2306
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2306,GetString(IDS_STRING2324),0xFFFF,1 << 0|1 << 2|1 << 5);
	opLines.Add(CString(olConflict));

	// line 18	= Gate/Port - Baggage belt relation IDS_STRING2304
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2304,GetString(IDS_STRING2325),0xFFFF,1 << 0|1 << 2|1 << 4);
	opLines.Add(CString(olConflict));

	// line 19	= Baggage belt - Exit relation    IDS_STRING2305
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2305,GetString(IDS_STRING2326),0xFFFF,1 << 0|1 << 4);
	opLines.Add(CString(olConflict));

	// line 20	= Check-In counter - position relation    IDS_STRING2340
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2340,GetString(IDS_STRING2327),0xFFFF,1 << 0|1 << 1|1 << 3);
	opLines.Add(CString(olConflict));

	// line 21	= Check-In counter - gate relation  IDS_STRING2341
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2341,GetString(IDS_STRING2328),0xFFFF,1 << 0|1 << 2|1 << 3);
	opLines.Add(CString(olConflict));

	// line 22	= Check-In counter - Lounge relation   IDS_STRING2342
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2342,GetString(IDS_STRING2329),0xFFFF,1 << 0|1 << 3|1 << 5);
	opLines.Add(CString(olConflict));

//	incompatible flights
	if(blIncomApPos)
	{
		//Incompatible Flights - AP Position IDS_STRING2679
		olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2679,GetString(IDS_STRING2679),0xFFFF,1 << 0|1 << 1);
		opLines.Add(CString(olConflict));
	}
	if(blIncomAlPos)
	{
		//Incompatible Flights - AL Position IDS_STRING2680
		olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2680,GetString(IDS_STRING2680),0xFFFF,1 << 0|1 << 1);
		opLines.Add(CString(olConflict));
	}
	if(blIncomApGat)
	{
		//Incompatible Flights - AP Gate IDS_STRING2681
		olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2681,GetString(IDS_STRING2681),0xFFFF,1 << 0|1 << 2);
		opLines.Add(CString(olConflict));
	}
	if(blIncomAlGat)
	{
		//Incompatible Flights - AL Gate IDS_STRING2682
		olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2682,GetString(IDS_STRING2682),0xFFFF,1 << 0|1 << 2);
		opLines.Add(CString(olConflict));
	}
	if (!ogFltiPrio.IsEmpty())
	{
		//Flight ID is %s - Allowed %s IDS_STRING2688
		olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2688,GetString(IDS_STRING2689),0xFFFF,1 << 0|1 << 1);
		opLines.Add(CString(olConflict));
	}

	// 050228 MVy: confict rule line added for Aircraft Registration restriction; new line 30 inserted
	// 050301 MVy: due to problems with data read from database this comes at the end
//	bgRuleforAcrRegn = true;
	if(bgRuleforAcrRegn) 
	{
		olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2690,GetString(IDS_STRING2691),0xFFFF,1 << 0|1 << 1);
		opLines.Add(CString(olConflict));
	}

//	bgConflictNewFlight = true;
	if (bgConflictNewFlight)
	{
		olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING1805,GetString(IDS_STRING1805),0xFFFF,1 << 0);
		opLines.Add(CString(olConflict));
	}

//	bgConflictBeltFlti = true;
	if (bgConflictBeltFlti)
	{
		olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2721,GetString(IDS_STRING2722),0xFFFF,1 << 0|1 << 4);
		opLines.Add(CString(olConflict));
	}

	if (bgTermRestrForAl)
	{
		/*
		//for positions no termonal available (UIF)
		//Terminal Position is %s: must be %s
		olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2726,GetString(IDS_STRING2729),0xFFFF,1 << 0|1 << 1);
		opLines.Add(CString(olConflict));
		*/

		//Terminal Gate is %s: must be %s
		olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2725,GetString(IDS_STRING2725),0xFFFF,1 << 0|1 << 2);
		opLines.Add(CString(olConflict));
		

		//Terminal Belt is %s: must be %s
		olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2723,GetString(IDS_STRING2723),0xFFFF,1 << 0|1 << 4);
		opLines.Add(CString(olConflict));

		//Terminal lounge is %s: must be %s
		olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2727,GetString(IDS_STRING2727),0xFFFF,1 << 0|1 << 5);
		opLines.Add(CString(olConflict));
	}

	// Gate %s: Aircraft restriction (%s): %s IDS_STRING2796
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2796,GetString(IDS_STRING2798),0xFFFF,1 << 0|1 << 2);
	opLines.Add(CString(olConflict));

	// Baggage Belt %s: Aircraft restriction (%s): %s IDS_STRING2797
	olConflict.Format("%ld;%ld;%s;%d;%u;",i++,IDS_STRING2797,GetString(IDS_STRING2799),0xFFFF,1 << 0|1 << 4);
	opLines.Add(CString(olConflict));

	NUMCONFLICTS = i;
}
/*
void CedaCfgData::GetDefaultConflictSetup(CStringArray &opLines)
{
	opLines.RemoveAll();
	CString olConflict; 
	CString olText;

		// line 0	= flight data changed  IDS_STRING1550
	olConflict.Format("%ld;%ld;%s;%d;%u;",0,IDS_STRING1550,GetString(IDS_STRING2307),0xFFFF,0);
	opLines.Add(CString(olConflict));

	// line 1	= start-landing order  IDS_STRING1430
	olConflict.Format("%ld;%ld;%s;%d;%u;",1,IDS_STRING1430,GetString(IDS_STRING2308),0xFFFF,0);
	opLines.Add(CString(olConflict));
 
	// line 2	= No onblock at touch down + n minutes IDS_STRING1236
	olConflict.Format("%ld;%ld;%s;%d;%u;",2,IDS_STRING1236,GetString(IDS_STRING2309),15,1 << 0 | 1 << 1);
	opLines.Add(CString(olConflict));

	// line 3	= Actual time > ETA + n minutes   IDS_STRING1237
	olConflict.Format("%ld;%ld;%s;%d;%u;",3,IDS_STRING1237,GetString(IDS_STRING2310),5,1 << 0);
	opLines.Add(CString(olConflict));

	// line 4	= Actual time + n minutes > next information time (Arrival)  IDS_STRING1238
	olConflict.Format("%ld;%ld;%s;%d;%u;",4,IDS_STRING1238,GetString(IDS_STRING2311),-5,1 << 0);
	opLines.Add(CString(olConflict));

	// line 5	= Pos:() A/C() error () from parking system  IDS_STRING1428
	olConflict.Format("%ld;%ld;%s;%d;%u;",5,IDS_STRING1428,GetString(IDS_STRING2312),0xFFFF,0);
	opLines.Add(CString(olConflict));

	// line 6	= STA / ETA >= STD / ETD   IDS_STRING1432
	olConflict.Format("%ld;%ld;%s;%d;%u;",6,IDS_STRING1432,GetString(IDS_STRING2313),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 7	= STD / ETD + n minutes < actual time and no DEP telegram received  IDS_STRING1549
	olConflict.Format("%ld;%ld;%s;%d;%u;",7,IDS_STRING1549,GetString(IDS_STRING2314),5,1 << 0);
	opLines.Add(CString(olConflict));

	// line 8	= No take off time at offblock + n minutes    IDS_STRING1239
	olConflict.Format("%ld;%ld;%s;%d;%u;",8,IDS_STRING1239,GetString(IDS_STRING2315),15,1 << 0);
	opLines.Add(CString(olConflict));

	// line 9	= No offblock at STD + n minutes  IDS_STRING1240
	olConflict.Format("%ld;%ld;%s;%d;%u;",9,IDS_STRING1240,GetString(IDS_STRING2316),5,1 << 0);
	opLines.Add(CString(olConflict));

	// line 10	= | ETD - STD | > n minutes  IDS_STRING1241
	olConflict.Format("%ld;%ld;%s;%d;%u;",10,IDS_STRING1241,GetString(IDS_STRING2317),15,1 << 0);
	opLines.Add(CString(olConflict));

	// line 11	= Actual time + n minutes > next information time (Departure)  IDS_STRING1242
	olConflict.Format("%ld;%ld;%s;%d;%u;",11,IDS_STRING1242,GetString(IDS_STRING2318),-5,1 << 0);
	opLines.Add(CString(olConflict));

	// line 12	= STD / ETD - n minutes < actual time and no boarding information received IDS_STRING1548
	olConflict.Format("%ld;%ld;%s;%d;%u;",12,IDS_STRING1548,GetString(IDS_STRING2319),30,1 << 0);
	opLines.Add(CString(olConflict));

	// line 13	= | ETA - STA | > n minutes  IDS_STRING1860
	olConflict.Format("%ld;%ld;%s;%d;%u;",13,IDS_STRING1860,GetString(IDS_STRING2320),15,1 << 0);
	opLines.Add(CString(olConflict));

	// line 14	= CTOT > STD / ETD + n minutes  IDS_STRING2353
	olConflict.Format("%ld;%ld;%s;%d;%u;",14,IDS_STRING2353,GetString(IDS_STRING2321),-10,1 << 0);
	opLines.Add(CString(olConflict));

	// line 15	= No offblock at CTOT + n minutes   IDS_STRING2354
	olConflict.Format("%ld;%ld;%s;%d;%u;",15,IDS_STRING2354,GetString(IDS_STRING2322),-15,1 << 0);
	opLines.Add(CString(olConflict));

	// line 16	= Position - Gate relation  IDS_STRING2303  (IDS_STRING2001)
	olConflict.Format("%ld;%ld;%s;%d;%u;",16,IDS_STRING2303,GetString(IDS_STRING2323),0xFFFF,1 << 0|1 << 1|1 << 2);
	opLines.Add(CString(olConflict));

	// line 17	= Gate - Lounge relation IDS_STRING2306
	olConflict.Format("%ld;%ld;%s;%d;%u;",17,IDS_STRING2306,GetString(IDS_STRING2324),0xFFFF,1 << 0|1 << 2|1 << 5);
	opLines.Add(CString(olConflict));

	// line 18	= Gate/Port - Baggage belt relation IDS_STRING2304
	olConflict.Format("%ld;%ld;%s;%d;%u;",18,IDS_STRING2304,GetString(IDS_STRING2325),0xFFFF,1 << 0|1 << 2|1 << 4);
	opLines.Add(CString(olConflict));

	// line 19	= Baggage belt - Exit relation    IDS_STRING2305
	olConflict.Format("%ld;%ld;%s;%d;%u;",19,IDS_STRING2305,GetString(IDS_STRING2326),0xFFFF,1 << 0|1 << 4);
	opLines.Add(CString(olConflict));

	// line 20	= Check-In counter - position relation    IDS_STRING2340
	olConflict.Format("%ld;%ld;%s;%d;%u;",20,IDS_STRING2340,GetString(IDS_STRING2327),0xFFFF,1 << 0|1 << 1|1 << 3);
	opLines.Add(CString(olConflict));

	// line 21	= Check-In counter - gate relation  IDS_STRING2341
	olConflict.Format("%ld;%ld;%s;%d;%u;",21,IDS_STRING2341,GetString(IDS_STRING2328),0xFFFF,1 << 0|1 << 3);
	opLines.Add(CString(olConflict));

	// line 22	= Check-In counter - Lounge relation   IDS_STRING2342
	olConflict.Format("%ld;%ld;%s;%d;%u;",22,IDS_STRING2342,GetString(IDS_STRING2329),0xFFFF,1 << 0|1 << 3|1 << 5);
	opLines.Add(CString(olConflict));

	// line 23	= Return Flight  IDS_STRING1697
	olConflict.Format("%ld;%ld;%s;%d;%u;",23,IDS_STRING1697,GetString(IDS_STRING2355),0xFFFF,1 << 0|1 << 3|1 << 5);
	opLines.Add(CString(olConflict));

	// line 24	= diverted  IDS_STRING1776
	olConflict.Format("%ld;%ld;%s;%d;%u;",24,IDS_STRING1776,GetString(IDS_STRING2356),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 25	= Returned from Taxi  IDS_STRING1777
	olConflict.Format("%ld;%ld;%s;%d;%u;",25,IDS_STRING1777,GetString(IDS_STRING2357),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 26	= Departure/Towing before Arrival IDS_STRING1890
	olConflict.Format("%ld;%ld;%s;%d;%u;",26,IDS_STRING1890,GetString(IDS_STRING2358),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 27	= Towing can not go off block if the flight is not on block! IDS_STRING1981
	olConflict.Format("%ld;%ld;%s;%d;%u;",27,IDS_STRING1981,GetString(IDS_STRING2359),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 28	= Aircrafttype not found IDS_STRING1713
	olConflict.Format("%ld;%ld;%s;%d;%u;",28,IDS_STRING1713,GetString(IDS_STRING2360),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 29	= Position not available IDS_STRING1714
	olConflict.Format("%ld;%ld;%s;%d;%u;",29,IDS_STRING1714,GetString(IDS_STRING2361),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 30	= Position %s: Flight ID from the related Gate is %s must be %s IDS_STRING1715
	olConflict.Format("%ld;%ld;%s;%d;%u;",30,IDS_STRING1715,GetString(IDS_STRING2362),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 31	= Global nature ristriction (%s) at position %s: %s IDS_STRING1712
	olConflict.Format("%ld;%ld;%s;%d;%u;",31,IDS_STRING1712,GetString(IDS_STRING2363),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 32	= Pushback conflict at position %s with flight %s! IDS_STRING1816
	olConflict.Format("%ld;%ld;%s;%d;%u;",32,IDS_STRING1816,GetString(IDS_STRING2364),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 33	= Position %s: Nature ristriction (%s): %s! IDS_STRING1716
	olConflict.Format("%ld;%ld;%s;%d;%u;",33,IDS_STRING1716,GetString(IDS_STRING2365),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 34	= Position %s: Aircraft restriction (%s): %s! IDS_STRING1717
	olConflict.Format("%ld;%ld;%s;%d;%u;",34,IDS_STRING1717,GetString(IDS_STRING2366),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 35	= Position %s: Airline restriction (%s): %s! IDS_STRING1718
	olConflict.Format("%ld;%ld;%s;%d;%u;",35,IDS_STRING1718,GetString(IDS_STRING2367),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 36	= Aircraft wingspan at position %s: %.2f (%.2f-%.2f) %s %s! IDS_STRING1719
	olConflict.Format("%ld;%ld;%s;%d;%u;",36,IDS_STRING1719,GetString(IDS_STRING2368),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 37	= Aircraft height at position %s: %.2f (%.2f-%.2f) %s/%s! IDS_STRING1720
	olConflict.Format("%ld;%ld;%s;%d;%u;",37,IDS_STRING1720,GetString(IDS_STRING2369),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 38	= Aircraft lenght at position %s: %.2f (%.2f-%.2f) %s/%s! IDS_STRING1721
	olConflict.Format("%ld;%ld;%s;%d;%u;",38,IDS_STRING1721,GetString(IDS_STRING2370),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 39	= Too many aircrafts at position %s: %d Max: %d! IDS_STRING1722
	olConflict.Format("%ld;%ld;%s;%d;%u;",39,IDS_STRING1722,GetString(IDS_STRING2371),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 40	= Invalid 'Pass. Boarding Bridge' - 'Bus Gate' Relation! IDS_STRING2065
	olConflict.Format("%ld;%ld;%s;%d;%u;",40,IDS_STRING2065,GetString(IDS_STRING2372),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 41	= Gate %s not available %s - %s reason: %s! IDS_STRING1724
	olConflict.Format("%ld;%ld;%s;%d;%u;",41,IDS_STRING1724,GetString(IDS_STRING2373),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 42	= Flight ID for Gate %s: % must be %s ! IDS_STRING1725
	olConflict.Format("%ld;%ld;%s;%d;%u;",42,IDS_STRING1725,GetString(IDS_STRING2374),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 43	= Diffrent Flight ID for Gate %s: % -> Can be %s ! IDS_STRING2300
	olConflict.Format("%ld;%ld;%s;%d;%u;",43,IDS_STRING2300,GetString(IDS_STRING2375),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 44	= To many aircrafts at gate %s: %d Max: %d! IDS_STRING1726
	olConflict.Format("%ld;%ld;%s;%d;%u;",44,IDS_STRING1726,GetString(IDS_STRING2376),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 45	= Gate %s can not be use for inbound ! IDS_STRING1727
	olConflict.Format("%ld;%ld;%s;%d;%u;",45,IDS_STRING1727,GetString(IDS_STRING2377),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 46	= Gate %s can not be use for outbound ! IDS_STRING1728
	olConflict.Format("%ld;%ld;%s;%d;%u;",46,IDS_STRING1728,GetString(IDS_STRING2378),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 47	= Max Passenger of Gate %s: %s (%s)! IDS_STRING1729
	olConflict.Format("%ld;%ld;%s;%d;%u;",47,IDS_STRING1729,GetString(IDS_STRING2379),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 48	= Gate %s: Airline restriction (%s): %s! IDS_STRING1730
	olConflict.Format("%ld;%ld;%s;%d;%u;",48,IDS_STRING1730,GetString(IDS_STRING2380),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 49	= Gate %s: Nature restriction (%s): %s! IDS_STRING1731
	olConflict.Format("%ld;%ld;%s;%d;%u;",49,IDS_STRING1731,GetString(IDS_STRING2381),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 50	= Global nature ristriction (%s) at gate %s: %s! IDS_STRING1732
	olConflict.Format("%ld;%ld;%s;%d;%u;",50,IDS_STRING1732,GetString(IDS_STRING2382),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 51	= Gate %s: Orig/Dest restriction (%s): %s! IDS_STRING2006
	olConflict.Format("%ld;%ld;%s;%d;%u;",51,IDS_STRING2006,GetString(IDS_STRING2383),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 52	= Too many flights at lounge %s: %d Max: %d! IDS_STRING1734
	olConflict.Format("%ld;%ld;%s;%d;%u;",52,IDS_STRING1734,GetString(IDS_STRING2384),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 53	= Diffrent Flight ID for Lounge %s: %! IDS_STRING2301
	olConflict.Format("%ld;%ld;%s;%d;%u;",53,IDS_STRING2301,GetString(IDS_STRING2385),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 54	= Baggage belt %s not available %s - %s reason: %s! IDS_STRING1735
	olConflict.Format("%ld;%ld;%s;%d;%u;",54,IDS_STRING1735,GetString(IDS_STRING2386),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 55	= To many aircrafts at baggage belt %s: %d Max: %d! IDS_STRING1736
	olConflict.Format("%ld;%ld;%s;%d;%u;",55,IDS_STRING1736,GetString(IDS_STRING2387),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 56	= Global Nature Restriction (%s) at Belt %s: %s! IDS_STRING2339
	olConflict.Format("%ld;%ld;%s;%d;%u;",56,IDS_STRING2339,GetString(IDS_STRING2388),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 57	= Waiting Room not available %s - %s reason: %s! IDS_STRING2204
	olConflict.Format("%ld;%ld;%s;%d;%u;",57,IDS_STRING2204,GetString(IDS_STRING2389),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 58	= Global Nature Restriction (%s) at Lounge %s: %s! IDS_STRING2338
	olConflict.Format("%ld;%ld;%s;%d;%u;",58,IDS_STRING2338,GetString(IDS_STRING2390),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 59	= Aircraft-Restriction/Wingoverlap with aircraft (%s %s) at position %s! IDS_STRING1723
	olConflict.Format("%ld;%ld;%s;%d;%u;",59,IDS_STRING1723,GetString(IDS_STRING2392),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 60	= Special Requirements
	olConflict.Format("%ld;%ld;%s;%d;%u;",60,IDS_STRING2572,GetString(IDS_STRING2572),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 61	= Flight ID for Lounge %s must be %s. IDS_STRING2410
	olConflict.Format("%ld;%ld;%s;%d;%u;",61,IDS_STRING2410,GetString(IDS_STRING2410),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));

	// line 62	= Too many Passengers at Lounge %s: %d Max: %d. IDS_STRING2424
	olConflict.Format("%ld;%ld;%s;%d;%u;",62,IDS_STRING2424,GetString(IDS_STRING2424),0xFFFF,1 << 0);
	opLines.Add(CString(olConflict));
}
*/
void CedaCfgData::CfgRecordFromConflict(const CString& popUsername,const CString& popRecord,CFGDATA& popCfgData)
{
	int ilMaxFields = 5;
	int ilMaxBytes  = popRecord.GetLength();

	long	ilIndex;
	long	llType;
	long	llDuration;
	CString olName;
	DWORD	ulDisplay;

	for (int ilFieldCount = 0, ilByteCount = 0; ilFieldCount < ilMaxFields; ilFieldCount++)
   	{
        // Extract next field in the specified text-line
		for (int ilFirstByteInField = ilByteCount; ilByteCount < ilMaxBytes && popRecord[ilByteCount] != ';'; ilByteCount++)
			;
		if(ilByteCount <= ilMaxBytes)
		{
			CString olField = popRecord.Mid(ilFirstByteInField,ilByteCount-ilFirstByteInField);
			ilByteCount++;
			switch(ilFieldCount)
			{
			case 0 : 
				ilIndex = atol((LPCSTR)olField);
			break;
			case 1 : 
				llType = atol((LPCSTR)olField);
			break;
			case 2 : 
				olName = olField;
			break;
			case 3 : 
				llDuration = atol((LPCSTR)olField);
			break;
			case 4 :
				ulDisplay = atol((LPCSTR)olField);
			break;					
			}
		}
	}

	popCfgData.Urno = ogBasicData.GetNextUrno();
	sprintf(popCfgData.Ctyp, "CONFLICT%02d", ilIndex);
	strncpy(popCfgData.Pkno,(LPCSTR)popUsername,sizeof(popCfgData.Pkno));
	popCfgData.Pkno[sizeof(popCfgData.Pkno)-1] = '\0';
	strncpy(popCfgData.Text,(LPCSTR)popRecord,sizeof(popCfgData.Text));
	popCfgData.Text[sizeof(popCfgData.Text)-1] = '\0';

}

bool CedaCfgData::ResetDefaultConflicts()
{
	char pclWhere[1024]="";

	bool	ilRc = true;
	bool	olRc = true;

    sprintf(pclWhere, "WHERE APPN='CCS_FPMS' AND CTYP LIKE 'CONFLICT%%' AND PKNO = '#__DEFAULT__#'");
	olRc = CedaAction("DRT", pclWhere);
    return olRc;
}

// read conflict config data
bool CedaCfgData::ReadConflicts(const CString& opUserName,CStringArray &opLines) 
{
	char pclWhere[1024]="";
	int ilLc = 0;

	opLines.RemoveAll();
	bool	ilRc = true;
	bool	olRc = true;

    sprintf(pclWhere, "WHERE APPN='CCS_FPMS' AND CTYP LIKE 'CONFLICT%%' AND PKNO = '%s' ORDER BY CTYP", opUserName);
    olRc = CedaAction("RT", pclWhere);

	ilLc = 0;
    while(olRc == true && ilRc == true)
    {
		CFGDATA rlCfg;
		CString olTxt;
		if ((ilRc = GetBufferRecord(ilLc,&rlCfg)) == true)
		{
			if(ilLc < NUMCONFLICTS)
			{
				olTxt = CString(rlCfg.Text);
				opLines.Add(olTxt);
			}
			else
			{
				olTxt.Format("Color <%d> ignored because exceeds NUMCONFLICTS <%d>\n%s",ilLc,NUMCONFLICTS,rlCfg.Text);
			}
			TRACE("%s\n",olTxt);
			ilLc++;
		}
	}

//	if(ilLc > NUMCONFLICTS || olRc == false || ResetToDefault())
	if(ilLc != NUMCONFLICTS || olRc == false || ResetToDefault())
	{
		return false;
	}
    return true;
}

bool CedaCfgData::ResetToDefault(void)
{

	bool blResetToDefault = false;
	char pclTmpText[512];
	char pclConfigPath[512];
	char pclKeyword[521];

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	strcpy(pclKeyword,"RESET_COLORS_TO_DEFAULT");
    GetPrivateProfileString(ogAppName, pclKeyword, "NO",pclTmpText, sizeof pclTmpText, pclConfigPath);
	if(!stricmp(pclTmpText,"YES"))
	{
		blResetToDefault = true;
	}
	return blResetToDefault;
}

bool CedaCfgData::SaveConflictData(const CString& opUserName,CFGDATA *prpCfg)
{
	CString olListOfData;
	bool olRc = true;
	char pclData[824];
	char pclSelection[1024]="";
	//First we delete the entry
	sprintf(pclSelection,"WHERE APPN='CCS_FPMS' AND (PKNO = '%s' AND CTYP = '%s')", opUserName, prpCfg->Ctyp);
	olRc = CedaAction("DRT",pclSelection);

	//and create it anew
	MakeCedaData(olListOfData,prpCfg);
	strcpy(pclData,olListOfData);
	olRc = CedaAction("IRT","","",pclData);

	return olRc;
}


// read the conflict types from the DB
void CedaCfgData::ReadAllConfigData(void) 
{
	omConflictData.DeleteAll();
	// get program default conflicts
	CStringArray olProgLines;
	ogCfgData.GetDefaultConflictSetup(olProgLines);
	
	// get conflicts for default user
	CStringArray olDefaultLines;
	bool ilRc = ogCfgData.ReadConflicts(ogCfgData.pomDefaultName,olDefaultLines);

	// add new programmed conflict types to the default user conflicts
	if (olDefaultLines.GetSize() < olProgLines.GetSize())
	{
		for (int ilLc = olDefaultLines.GetSize(); ilLc < olProgLines.GetSize();ilLc++)
		{
			olDefaultLines.Add(olProgLines[ilLc]);
			// write default user records
			CFGDATA rlCfg;
			ogCfgData.CfgRecordFromConflict(ogCfgData.pomDefaultName,olProgLines[ilLc],rlCfg);
			ogCfgData.SaveConflictData(ogCfgData.pomDefaultName,&rlCfg);
		}

	}

	// get conflicts for current user
	CStringArray olLines;
	ilRc = ogCfgData.ReadConflicts(ogBasicData.omUserID,olLines);
	
	// add new programmed conflict types to the current user conflicts
	if (olLines.GetSize() < olDefaultLines.GetSize())
	{
		for (int ilLc = olLines.GetSize(); ilLc < olDefaultLines.GetSize();ilLc++)
		{
			olLines.Add(olDefaultLines[ilLc]);
			// write current user records
			CFGDATA rlCfg;
			ogCfgData.CfgRecordFromConflict(ogBasicData.omUserID,olDefaultLines[ilLc],rlCfg);
			ogCfgData.SaveConflictData(ogBasicData.omUserID,&rlCfg);
		}

	}

	int ilRecCount = 0;
	for(int i = 0; i < olLines.GetSize(); i++)
	{
		CString olRecord = olLines.GetAt(i);
		CONFLICTDATA *prlConflictData = NewConflictData( olRecord );		// 050506 MVy: code moved to function
		omConflictData.Add(prlConflictData);
		omConflictMap.SetAt(prlConflictData->imType,prlConflictData);
		ilRecCount++;
	}
}

// 050506 MVy: create configuration
// Attention: this function allocates data by a NEW !!!
// code from CedaCfgData::ReadAllConfigData and GatPosConflictSetupDlg::OnLoadDefault
CONFLICTDATA* CedaCfgData::NewConflictData( CString &olRecord )
{
	CONFLICTDATA *prlConflictData = new CONFLICTDATA;

	int ilMaxFields = 5;
	int ilMaxBytes  = olRecord.GetLength();

	DWORD ulDisplay;
	CString sTest = olRecord ;		// 050303 MVy: test
	for (int ilFieldCount = 0, ilByteCount = 0; ilFieldCount < 999/*ilMaxFields*/; ilFieldCount++)
  {
		if( sTest.IsEmpty() ) break ;		// no data nothing to fetch
		CString sToken = ExtractFirstToken( sTest );

      // Extract next field in the specified text-line
		for (int ilFirstByteInField = ilByteCount; ilByteCount < ilMaxBytes && olRecord[ilByteCount] != ';'; ilByteCount++)
			;
		if(ilByteCount <= ilMaxBytes)
		{
			CString olField = olRecord.Mid(ilFirstByteInField,ilByteCount-ilFirstByteInField);
			ASSERT( olField == sToken );		// test, if it works the former code and static loop can be removed
			ilByteCount++;
			switch(ilFieldCount)
			{
			case 0 : 
				prlConflictData->imIndex = atol((LPCSTR)olField);
			break;
			case 1 : 
				prlConflictData->imType = atol((LPCSTR)olField);
			break;
			case 2 : 
				prlConflictData->omName = olField;
			break;
			case 3 : 
				prlConflictData->imDuration = atol((LPCSTR)olField);
			break;
			case 4 :		// get the bitmask shown in restriction table
				ulDisplay = atol((LPCSTR)olField);

				if (ulDisplay & 1 << 0)
					prlConflictData->bmGeneralDesktop = true;

				if (ulDisplay & 1 << 1)
					prlConflictData->bmAircraftPositions = true;

				if (ulDisplay & 1 << 2)
					prlConflictData->bmGates = true;

				if (ulDisplay & 1 << 3)
					prlConflictData->bmCheckinCounters = true;

				if (ulDisplay & 1 << 4)
					prlConflictData->bmBaggageBelts = true;

				if (ulDisplay & 1 << 5)
					prlConflictData->bmLounges = true;
//YYY	// -> marker was set in GatPosConflictSetupDlg::OnLoadDefault
				if (ulDisplay & 1 << 6)
					prlConflictData->bmFlightDia = true;

				break;

			case 5:		// 050303 MVy: PRF6981: conflict priority, store as sedecimal formatted value
				if( __CanSeeConflictPriorityColor() )		// 050323 MVy: PRF6981: conflict priority, make configurable, addendum
				{
					prlConflictData->clrPriority = ToLong( olField );		// olField must contain a sedecimal colorcode
				}
				break ;
			}
		}
	};
	return prlConflictData ;
};


CONFLICTDATA *CedaCfgData::GetConflictDataById(int ipId)
{
	
	if (ipId < omConflictData.GetSize())
	{
	  return (&omConflictData.ElementAt(ipId));
	}

	return NULL;
}

// 050303 MVy: find a globally stored conflict by its type
CONFLICTDATA* CedaCfgData::GetConflictDataByType( int iType )
{
	int ilId = iType;

	int ilKonftype = ogMapLblUrnoToKonftype.GetCount();
	if (ilId < ilKonftype)
		ilId = IDS_STRING1550;


	int iConflicts = omConflictData.GetSize();
	for( int ndxConflict = 0 ; ndxConflict < iConflicts ; ndxConflict++ )
	{
		CONFLICTDATA* pConflictData = &omConflictData[ ndxConflict ];
	  if( pConflictData->imType == ilId )
			return pConflictData ;
	};
	return 0 ;
};	// GetConflictDataByType

bool CedaCfgData::CheckKonfliktForDisplay(int ipId, int ipDisplayType)
{
	bool blRc = false;
	CONFLICTDATA *prlConflictData = NULL;


	int ilId = ipId;

	int ilKonftype = ogMapLblUrnoToKonftype.GetCount();
	if (ilId < ilKonftype)
		ilId = IDS_STRING1550;


	if (!omConflictMap.Lookup(ilId,(void *&)prlConflictData))
	{
		return blRc;
	}

	if (prlConflictData != NULL)
	{
	  switch(ipDisplayType)
	  {

	  case CONF_ALL : if (prlConflictData->bmGeneralDesktop)  blRc = true;
						break;
	  case CONF_BLT :if (prlConflictData->bmBaggageBelts)  blRc = true;
						break;
	  case CONF_POS :if (prlConflictData->bmAircraftPositions)  blRc = true;
						break;
	  case CONF_CCA :if (prlConflictData->bmCheckinCounters)  blRc = true;
						break;
	  case CONF_GAT :if (prlConflictData->bmGates)  blRc = true;
						break;
	  case CONF_WRO :if (prlConflictData->bmLounges)  blRc = true;
						break;
	  case CONF_FLT :if (prlConflictData->bmFlightDia)  blRc = true;
						break;
	  default : ;
	  }

	}

	return blRc;
}


int CedaCfgData::GetConflictData(CCSPtrArray<CONFLICTDATA>& ropArray)
{
	ropArray.DeleteAll();

	int ilSize = omConflictData.GetSize();
	for (int ilC = 0; ilC < ilSize; ilC++)
	{
		ropArray.NewAt(ilC,omConflictData.ElementAt(ilC));
	}

	return ropArray.GetSize();
}

// 050506 MVy: create configuration
//	code consolidated from GatPosConflictSetupDlg::OnSaveDefault and GatPosConflictSetupDlg::OnSaveDefault
CFGDATA CedaCfgData::MakeConflictData( int ipIndex, const CONFLICTDATA& ropData, CString sUserId )
{
	CFGDATA rlCfg ;

	DWORD ulDisplay = 0;
	ulDisplay |= ropData.bmGeneralDesktop << 0;
	ulDisplay |= ropData.bmAircraftPositions << 1;
	ulDisplay |= ropData.bmGates << 2;
	ulDisplay |= ropData.bmCheckinCounters << 3;
	ulDisplay |= ropData.bmBaggageBelts << 4;
	ulDisplay |= ropData.bmLounges << 5;
//YYY		// <-- marker comes from GatPosConflictSetupDlg::OnSaveDefault
	ulDisplay |= ropData.bmFlightDia << 6;

	rlCfg.Urno = ogBasicData.GetNextUrno();
	sprintf( rlCfg.Ctyp, "CONFLICT%02d", ipIndex );
	sprintf( rlCfg.Pkno, sUserId );
	sprintf( rlCfg.Text, "%d;%d;%s;%d;%u;",
		ropData.imIndex,
		ropData.imType,
		ropData.omName,
		ropData.imDuration,
		ulDisplay
	);

	if( __CanSeeConflictPriorityColor() )		// 050323 MVy: PRF6981: conflict priority, make configurable, addendum
	{
		int length = strlen( rlCfg.Text );
		ASSERT( length + 11 < 2001 );		// fixed length in CFGDATA.Text
		length = sprintf( rlCfg.Text + length, "0x%0.8x;", (long )ropData.clrPriority );		// 050303 MVy: PRF6981: conflict priority, store as sedecimal formatted value
	};

	return rlCfg ;		// copy back
};	// MakeConflictData


// 050506 MVy: see similar code 
BOOL CedaCfgData::SetConflictData(int ipIndex,const CONFLICTDATA& ropData,bool blSaveDiffOnly)
{
	ASSERT(ipIndex == ropData.imIndex);
	if (ipIndex >= 0 && ipIndex < NUMCONFLICTS)
	{
		CONFLICTDATA *prlData = &omConflictData[ipIndex];
		if (blSaveDiffOnly == false || *prlData != ropData)
		{
			CFGDATA rlCfg = MakeConflictData( ipIndex, ropData, ogBasicData.omUserID );		// 050506 MVy: see similar code CedaCfgData::SetConflictData
			if (SaveConflictData(ogBasicData.omUserID,&rlCfg) == false)		// Save the data
				return FALSE;
			*prlData = ropData;
		}
		return TRUE;
	}
	else 
		return FALSE;
}

bool CedaCfgData::ReadXDaysSetup()
{
	char pclWhere[2048]="";
	bool ilRc = true;
    sprintf(pclWhere, "WHERE APPN='CCS_%s' AND (CTYP = 'XDAYS' AND PKNO = '%s')", ogAppName, ogBasicData.omUserID);

	rmXDaysSetup.Urno = ogBasicData.GetNextUrno();
	sprintf(rmXDaysSetup.Appn, "CCS_%s", ogAppName);
	strcpy(rmXDaysSetup.Ctyp, "XDAYS");
	strcpy(rmXDaysSetup.Ckey, "");
	rmXDaysSetup.Vafr = CTime::GetCurrentTime();
	rmXDaysSetup.Vato = CTime(2020, 12, 31, 23, 59, 00);
	strcpy(rmXDaysSetup.Pkno, ogBasicData.omUserID);
	rmXDaysSetup.IsChanged = DATA_NEW;
	sprintf(rmXDaysSetup.Text, "");
	
	if (CedaAction("RT", pclWhere) == false)
        return false;

	//Initialize the datastructure
	CFGDATA *prlCfg = new CFGDATA;
	bool blFound = true;
    for (int ilLc = 0; ilRc == true; ilLc++)
    {
		if ((ilRc = GetBufferRecord(ilLc,prlCfg)) == false)
		{
			blFound = false;
		}
		else
		{
			rmXDaysSetup = *prlCfg;
			rmXDaysSetup.IsChanged = DATA_UNCHANGED;
		}
	}
	delete prlCfg;

	int ilDays = atoi(rmXDaysSetup.Text);
	ogXDays = CTimeSpan(ilDays,0,0,0);

    return true;
}


CString CedaCfgData::GetXDaysSetup()
{
	return rmXDaysSetup.Text;
}


CString CedaCfgData::GetTimelineBufferSetup()
{
	return rmTimelineBuffer.Text;
}


bool CedaCfgData::ReadTimelineBufferSetup()
{
	char pclWhere[2048]="";
	bool ilRc = true;
    sprintf(pclWhere, "WHERE APPN='CCS_%s' AND (CTYP = 'TIMELINEBUFFER' AND PKNO = '%s')", ogAppName, ogBasicData.omUserID);

	rmTimelineBuffer.Urno = ogBasicData.GetNextUrno();
	sprintf(rmTimelineBuffer.Appn, "CCS_%s", ogAppName);
	strcpy(rmTimelineBuffer.Ctyp, "TIMELINEBUFFER");
	strcpy(rmTimelineBuffer.Ckey, "");
	rmTimelineBuffer.Vafr = CTime::GetCurrentTime();
	rmTimelineBuffer.Vato = CTime(2020, 12, 31, 23, 59, 00);
	strcpy(rmTimelineBuffer.Pkno, ogBasicData.omUserID);
	rmTimelineBuffer.IsChanged = DATA_NEW;
	sprintf(rmTimelineBuffer.Text, "");
	
	if (CedaAction("RT", pclWhere) == false)
	{
		ogTimelineBuffer = CTimeSpan(0,0,0,0);
        return false;
	}

	//Initialize the datastructure 
	CFGDATA *prlCfg = new CFGDATA; 
	bool blFound = true;
    for (int ilLc = 0; ilRc == true; ilLc++)
    {
		if ((ilRc = GetBufferRecord(ilLc,prlCfg)) == false)
		{
			blFound = false;
		}
		else
		{
			rmTimelineBuffer = *prlCfg;
			rmTimelineBuffer.IsChanged = DATA_UNCHANGED;
		}
	}
	delete prlCfg;

	int ilhours = atoi(rmTimelineBuffer.Text);
	ogTimelineBuffer = CTimeSpan(0,ilhours,0,0);

    return true;
}

