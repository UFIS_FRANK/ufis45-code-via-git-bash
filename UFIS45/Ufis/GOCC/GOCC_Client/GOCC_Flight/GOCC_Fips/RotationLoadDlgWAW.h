#ifndef AFX_IDD_ROTATIONLOADDLGWAW_H__6858C611_6588_11D1_80FD_0000B43C4B01__INCLUDED_
#define AFX_RIDD_ROTATIONLOADDLGWAW_H__6858C611_6588_11D1_80FD_0000B43C4B01__INCLUDED_

// RotationViaDlg.h : Header-Datei
//

#include <CCSTable.h> 
#include <CCSPtrArray.h> 
#include <CCSEdit.h> 
#include <resrc1.h> 
#include <CedaLoaTabData.h>
/////////////////////////////////////////////////////////////////////////////
class RotationLoadDlgWAW : public CDialog
{
public:
	RotationLoadDlgWAW(CWnd* pParent, CString opAdid, CString opFlno, long lpUrnoFlight, CString opList, CTime opRefDat, bool bpLocalTime = true, bool bpEnable = true, CString opDest = CString(""));
	~RotationLoadDlgWAW();

// Dialogfelddaten
	//{{AFX_DATA(RotationViaDlg)
	enum { IDD = IDD_ROTATIONLOADDLGWAW };
	CStatic	m_CS_GMBorder;
	//}}AFX_DATA

	void Enable(bool bpEnable);
	void SetSecState(char opSecState);
	void SetData   (CString opAdid, CString opFlno, long lpUrnoFlight, CString opList, CTime opRefDat, bool bpLocalTime, CString opDest);
	void GetViaList(char *pcpViaList);
	CString GetToolTip();
	CString GetStatus();
	void SetRefDate(CTime opRefDat);

// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(RotationLoadDlgWAW)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:
	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(RotationLoadDlgWAW)
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	virtual void OnOK();
    afx_msg LONG OnTableLButtonDown(UINT wParam, LONG lParam);
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	afx_msg LONG OnTableMenuSelect( UINT wParam, LPARAM lParam);
	afx_msg LONG OnTableIPEditKillfocus( UINT wParam, LPARAM lParam);
	afx_msg LONG OnEditChanged( UINT wParam, LPARAM lParam);
	afx_msg void OnSize(UINT nType, int cx, int cy); 
	afx_msg void OnCheckTotal();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CWnd* pomParent;
	CCSTable *pomTable;
	bool bmLocal;
	bool bmEnable;
	CString omViaList;
	CTime omRefDat;
	long omUrnoFlight;
	CStringArray omFields;
	CString omCaption;
	CString omAdid;
	CString omFlno;
	char omSecState;

	CString GetField(VIADATA *prpVia, CString opField);
	void GetAttrib(CCSEDIT_ATTRIB &ropAttrib, CString opField);
	void SetField(VIADATA *prpVia, CString opField, CString opValue, CTime opRefDat = TIMENULL);
	CString CreateViaList(CCSPtrArray<VIADATA> *popVias, CTime opRefDat = TIMENULL);
	void SetViaList(CString opAdid, CString opFlno, long lpUrnoFlight, CString opList, CTime opRefDat, bool bpLocalTime, CString opDest);
	BOOL SaveInApxTab(CString &ropId, CString &ropData, CString &ropApc3); 
	void StoreLoaData();
	void RecalcTotals();
	void RecalcTotalColors();
	void RecalcbmAutocalc();

	void InitTable();
	void DrawHeader();
	void ShowTable();
	bool bmAutocalc;

	CedaLoaTabData omLoaTabData;
	bool bmWakeUpFlight;
private:
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_IDD_ROTATIONLOADDLGWAWW_H__6858C611_6588_11D1_80FD_0000B43C4B01__INCLUDED_
