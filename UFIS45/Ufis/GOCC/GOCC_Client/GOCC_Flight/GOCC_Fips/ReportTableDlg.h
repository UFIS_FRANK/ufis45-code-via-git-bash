#ifndef AFX_REPORTTABLEDLG_H__CDB257E5_8BF5_11D1_8127_0000B43C4B01__INCLUDED_
#define AFX_REPORTTABLEDLG_H__CDB257E5_8BF5_11D1_8127_0000B43C4B01__INCLUDED_

// ReportTableDlg.h : Header-Datei
//
#include <CCSTable.h>
#include <Table.h>
#include <RotationDlgCedaFlightData.h>
#include <Report1TableViewer.h>
#include <Report16TableViewer.h>
#include <AdditionalReport11TableViewer.h>
#include <GanttChartReportWnd.h>
#include <ReportSameORGTableViewer.h>
#include <ReportSameDESTableViewer.h>
#include <ReportSameACTTableViewer.h>
#include <ReportSameREGNTableViewer.h>
#include <ReportSameTTYPTableViewer.h>
#include <ReportArrDepLoadPaxTableViewer.h>
#include <ReportOvernightTableViewer.h>
#include <ReportFlightGPUTableViewer.h>
#include <ReportDailyFlightLogViewer.h>
#include <ReportFlightDelayViewer.h>
#include <Report19DailyCcaTableViewer.h>
#include <ReportReasonForChange.h>
#include <ReportReasonForChangeViewer.h> 
#include <DlgResizeHelper.h>
/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CReportTableDlg 

class CReportTableDlg : public CDialog
{
// Konstruktion
public:
	CReportTableDlg(CWnd* pParent = NULL, int igTable = 0, CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popData = NULL,
					char *pcpInfo = NULL, char *pcpSelect = NULL, char *pcpInfo2 = NULL, int ipArrDep = 0, RotationDlgCedaFlightData* popRotDlgData = NULL, char *pcpInfoPlus = NULL);

	~CReportTableDlg();

// Dialogfelddaten
	//{{AFX_DATA(CReportTableDlg)
	enum { IDD = IDD_REPORTTABLE };
	CButton	m_CB_Beenden;
	CButton	m_CB_Papier;
	CButton	m_CB_Drucken;
	CButton	m_CB_Datei;
	//}}AFX_DATA
	CString omHeadline;	//Dialogbox-Überschrift


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CReportTableDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

public:

	int imDialogBarHeight;
	void SaveToReg();
	void SetReasonData(CCSPtrArray<REASON_FOR_CHANGE> *popData, char *pcpInfo); 

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CReportTableDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDatei();
	afx_msg void OnDrucken();
	afx_msg void OnPapier();
	afx_msg void OnBeenden();
	virtual void OnCancel();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	bool bmCommonDlg;
	CCSTable *pomReportTable;
	CTable *pomReportCTable;
	int miTable;
	char *pcmInfo;
	char *pcmInfo2;
	char *pcmInfoPlus;
	char *pcmSelect;
	CCSPtrArray<ROTATIONDLGFLIGHTDATA> *pomData;
	CCSPtrArray<REASON_FOR_CHANGE> *pomReasonData; 
	RotationDlgCedaFlightData* pomRotDlgData;

	GefundenefluegeTableViewer	*pomGefundenefluegeViewer;
	Report16TableViewer	*pomReport16Viewer;
	AdditionalReport11TableViewer* pomAdditionalReport11TableViewer;
	GanttChartReportWnd* pomGanttChartReportWnd;
	ReportSameORGTableViewer*   pomReportSameORGTableViewer;
	ReportSameDESTableViewer*   pomReportSameDESTableViewer;
	ReportSameACTTableViewer*   pomReportSameACTTableViewer;
	ReportSameREGNTableViewer*  pomReportSameREGNTableViewer;
	ReportSameTTYPTableViewer*  pomReportSameTTYPTableViewer;
	ReportArrDepLoadPaxTableViewer*    pomReportArrDepLoadPaxTableViewer;
	ReportOvernightTableViewer*	pomReportOvernightTableViewer;
	ReportFlightGPUTableViewer*	pomReportFlightGPUTableViewer;
	ReportDailyFlightLogViewer* pomReportDailyFlightLogViewer;
	ReportFlightDelayViewer* pomReportFlightDelayViewer;
	Report19DailyCcaTableViewer* pomReport19Viewer;
	ReportReasonForChangeViewer* pomReportReasonForChangeViewer; 

	int imArrDep;
	CStatic* omStatic;
	bool bmDailyRep ;	// FAG BVD Daily Reports 
	DlgResizeHelper m_resizeHelper;
	CString m_key;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_REPORTTABLEDLG_H__CDB257E5_8BF5_11D1_8127_0000B43C4B01__INCLUDED_
