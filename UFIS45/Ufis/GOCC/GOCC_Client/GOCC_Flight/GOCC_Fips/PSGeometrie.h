#if !defined(AFX_PSGEOMETRIE_H__1B055C53_8B1E_11D1_B444_0000B45A33F5__INCLUDED_)
#define AFX_PSGEOMETRIE_H__1B055C53_8B1E_11D1_B444_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// PSGeometrie.h : header file
//
#include <resource.h>
#include <CCSEdit.h>
#include <CCSButtonctrl.h> 
 
#define ONE_HOUR_POINT  (10 / 2)

#define VIEW_GEOMETRIE_POS_GROUP_ID 1
#define VIEW_GEOMETRIE_GAT_GROUP_ID 2
#define VIEW_GEOMETRIE_BLT_GROUP_ID 3
#define VIEW_GEOMETRIE_WRO_GROUP_ID 4
#define VIEW_GEOMETRIE_CCA_GROUP_ID 5
#define VIEW_GEOMETRIE_LAST_ID 7

/////////////////////////////////////////////////////////////////////////////
// PSGeometrie dialog

class PSGeometrie : public CPropertyPage
{
	//DECLARE_DYNCREATE(PSGeometrie)

// Construction
public:
	PSGeometrie(FipsModules ipMode);
	~PSGeometrie();

	// All Geometrie settings are saved in omValues in the following format:
	// measure settings: omValues[0] = 
	// POS groups: omValues[1] = "POSGRPNAME1,POSGRPNAME2,POSGRPNAME3,..."
	// GAT groups: omValues[2] = "GATGRPNAME1,GATGRPNAME2,GATGRPNAME3,..."
	// BLT groups: omValues[3] = "BLTGRPNAME1,BLTGRPNAME2,BLTGRPNAME3,..."
	// WRO groups: omValues[4] = "WROGRPNAME1,WROGRPNAME2,WROGRPNAME3,..."
	// CCA groups: omValues[5] = "CCAGRPNAME1,CCAGRPNAME2,CCAGRPNAME3,..."
	CStringArray omValues; 
	CString omCalledFrom;
	bool blIsInit;
    BOOL bmNotMoreThan12;
    CTime   m_TimeScale;
	int		m_Percent;
	int		m_Hour;
	int		m_FontHeight;

	bool bmLinesDefault,
		 bmTimeDefault,
		 bmVertDefault,
		 bmDisplayBeginDefault;
 
	void SetCalledFrom(CString opCalledFrom);
// Dialog Data
	//{{AFX_DATA(PSGeometrie)
	enum { IDD = IDD_PSGEOMETRIE };
	CStatic	m_TestText;
	CSliderCtrl	m_Font;
	CEdit	m_Date;
 	CSliderCtrl	m_Height;
	CButton	m_TimeLines;
 	CButton	m_24h;
	CButton	m_12h;
	CButton	m_10h;
	CButton	m_8h;
	CButton	m_6h;
	CButton	m_4h;
	CButton	m_100Proz;
	CButton	m_75Proz;
	CButton	m_50Proz;
 	CCSEdit	m_Time;
	CListBox m_LB_Groups;
	CListBox m_LB_IncGroups;
	CCSButtonCtrl m_IncludeItems;
	CCSButtonCtrl m_ExcludeItems;
	CCSButtonCtrl m_ItemUp;
	CCSButtonCtrl m_ItemDown;
	//}}AFX_DATA

	void SetData();
	void GetData();
// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(PSGeometrie)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(PSGeometrie)
	virtual BOOL OnInitDialog();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
 	afx_msg void On10h();
	afx_msg void On12h();
	afx_msg void On24h();
	afx_msg void On4h();
	afx_msg void On6h();
	afx_msg void On8h();
	afx_msg void OnIncludeItems();
	afx_msg void OnExcludeItems();
	afx_msg void OnItemUp();
	afx_msg void OnItemDown();
 	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	CString imResType;
	int imResValuesId;

	void ResetGroupsList();
	void InitGroupTables();
	void ResetMassData();
	void InitMassData();

	void GetGroupData(CString opMass);

	void SetSecState();

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PSGEOMETRIE_H__1B055C53_8B1E_11D1_B444_0000B45A33F5__INCLUDED_)
