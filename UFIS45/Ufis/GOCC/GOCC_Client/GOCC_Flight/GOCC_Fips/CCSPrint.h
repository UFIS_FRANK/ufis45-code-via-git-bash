

#ifndef _CCSPRINT_H_
#define _CCSPRINT_H_

#include <CCSPtrArray.h>
#include <CCSDefines.h>

enum egPrintInfo {PRINT_LANDSCAPE,PRINT_PORTRAET,PRINT_LEFT,PRINT_RIGHT,PRINT_CENTER,
					PRINT_NOFRAME,PRINT_FRAMETHIN,PRINT_FRAMEMEDIUM,PRINT_FRAMETHICK,
					PRINT_SMALL,PRINT_MEDIUM,PRINT_LARGE,
					PRINT_SMALLBOLD,PRINT_MEDIUMBOLD,PRINT_LARGEBOLD};





struct PRINTELEDATA
{
	int Alignment;
	int Length;
	int FrameLeft;
	int FrameRight;
	int FrameTop;
	int FrameBottom;
	CFont *pFont;
	CString Text;
};

struct PRINTBARDATA 
{
    CString Text;
    CTime StartTime;
    CTime EndTime;
    int FrameType;
    int MarkerType;
	int MarkerTri;
	CTime TimeForText;


	BOOL IsShadowBar;
	BOOL IsBackGroundBar;
    int OverlapLevel;    // zero for the top-most level, each level lowered by 4 pixels
    CBrush *MarkerBrush;
    CBrush *MarkerBrushRightSide;

	CBrush *BrushTriangleLeft;
	CBrush *BrushTriangleRight;

	//YYY
    CCSPtrArray<PRINTBARDATA> SubBars;		// in order of painting (leftmost is the bottommost)

	PRINTBARDATA(void)
	{
		MarkerBrush = NULL;
		MarkerBrushRightSide = NULL;
		MarkerTri = 0;
		TimeForText = TIMENULL;

		BrushTriangleLeft = NULL;
		BrushTriangleRight = NULL;
	}
};

extern	 CCSPtrArray <PRINTBARDATA> omDefBkBars;

class CCSPrint : public CObject
{
public:
	CCSPrint(CWnd *opParent = NULL);
	CCSPrint(CWnd *opParent,int ipOrientation,
		int ipLineHeight = 50,int ipFirstLine = 0,int ipLeftOffset = 0,
		CString opHeader1 = "",CString opHeader2 = "",CString opHeader3 = "",
		CString opHeader4 = "",
		CString opFooter1 = "",CString opFooter2 = "",CString opFooter3 = "", bool bpView35 = false, bool bpSubbar = false);
	~CCSPrint();

	// Implementation
	InitializePrinter(int ipOrientation = PRINT_PORTRAET);

	BOOL PrintHeader(void);
	BOOL PrintHeader(CString opHeader1,CString opHeader2 = "",CString opHeader3 = "",
		CString opHeader4 = "");
	BOOL PrintFooter();
	BOOL PrintFooter(CString opFooter1,CString opFooter2 = "");
	BOOL PrintLine(CCSPtrArray <PRINTELEDATA> &rlPrintLine);

//	 CCSPtrArray <PRINTBARDATA> omDefBkBars;
	BOOL PrintGanttLine(CString opText,CCSPtrArray<PRINTBARDATA> &ropPrintLine,
	CCSPtrArray<PRINTBARDATA> &ropDefBkBars = omDefBkBars);
	BOOL PrintGanttBar(PRINTBARDATA &ropBar, int ipSubBar = 0);
	BOOL PrintBkBar(PRINTBARDATA &ropBa,int ipLineHeight);
	BOOL PrintTimeScale(int ipStartX,int ipEndX,CTime opStartTime,CTime opEndTime,CString opText);
	BOOL PrintGanttHeader(int ipStartX,int ipEndX,CString opText);
	BOOL PrintGanttBottom(void);
	BOOL PrintText(int ipStartX,int ipStartY,int ipEndX,int ipEndY,int ipType,CString opText,BOOL ipUseOffset = FALSE);
	void SetBitmaps(CBitmap *popBitmap = NULL, CBitmap *popCcsBitmap = NULL);
	void PrintUIFFooter(CString opFooter1,CString opFooter2,CString opFooter3);
	void PrintUIFHeader(CString opHeader1,CString opHeader2,int ilFirstPos,bool blBitmap = false);
	void PrintUIFHeader3(const CString &ropHeaderLeft, const CString &ropHeaderMiddle, const CString &ropHeaderRight, int ilFirstPos);
	void PrintUIFHeaderNoLine(CString opHeader1,CString opHeader2,int ilFirstPos);

	// MBR Anfang
	int GetLogPixelsX();
	int GetLeftOffset();
	void PrintDoubleLine(CCSPtrArray <PRINTELEDATA> &rlPrintLine, int ipColumn);
	// MBR Ende


private:
	BOOL SelectFramePen(int ipFrameType);
	void PrintLeft(CRect opRect,CString opText);
	void PrintRight(CRect opRect,CString opText);
	void PrintCenter(CRect opRect,CString opText);
	int  GetXFromTime(CTime opTime);
	BOOL LoadBitmapFromBMPFile( LPTSTR szFileName, HBITMAP *phBitmap,HPALETTE *phPalette );

	// Attributes
public:
	CDC	 omCdc;
	int imLineNo;
	int imPageNo;
	int imMaxLines;

	int imLineHeight;
	int imGanttLineHeight;
	int imBarHeight;
	int imSubBarHeight;
	int imBarVerticalTextOffset;
	int imFirstLine;
	int imLeftOffset;
	int imYOffset;

	CFont omSmallFont_Regular;
	CFont omSmallFont_RegularForGantt;
	CFont omMediumFont_Regular;
	CFont omLargeFont_Regular;
	CFont omSmallFont_Bold;
	CFont omMediumFont_Bold;
	CFont omLargeFont_Bold;
	CFont omCCSFont;
	CFont ogCourierNew_Regular_4;
	CFont ogCourierNew_Bold_4;
	CFont ogCourierNew_Regular_5;
	CFont ogCourierNew_Bold_5;
	CFont ogCourierNew_Regular_6;
	CFont ogCourierNew_Bold_6;
	CFont ogCourierNew_Regular_7;
	CFont ogCourierNew_Bold_7;
	CFont ogCourierNew_Regular_8;
	CFont ogCourierNew_Bold_8;
	CFont ogCourierNew_Regular_11;
	CFont ogCourierNew_Bold_11;
	CFont ogCourierNew_Regular_12;
	CFont ogCourierNew_Bold_12;
    CPen ThinPen;
    CPen MediumPen;
    CPen ThickPen;
	CPen DottedPen;
	short smPaperSize;
	int imPaperLength;
	int imPaperWidth;

private:
	CRgn omRgn;
	int     imOrientation;
	CStringArray omHeader;
	CStringArray omFooter;
	CString	     omLogoPath;
	CWnd         *pomParent;
	CBitmap		 *pomBitmap;
	CBitmap		 omBitmap;
	CDC			omMemDc;
	CBitmap		 *pomCcsBitmap;
	CDC			omCcsMemDc;
	int imLogPixelsY;
	int imLogPixelsX;
	BOOL bmIsInitialized;
	double dmXFactor;

	int imGanttStartX;
	int imGanttEndX;
	int imGanttStartY;
	int imGanttEndY;

	CTime omStartTime;
	CTime omEndTime;
	HBITMAP		hmBitmap;
	HPALETTE	hmPalette;

	bool bmSubbar;
	bool bmView35;

private:
	BYTE lmCharSet;

};

extern double dgCCSPrintFactor;
extern int igCCSPrintMinLength;
extern int igCCSPrintMoreLength;

#endif


