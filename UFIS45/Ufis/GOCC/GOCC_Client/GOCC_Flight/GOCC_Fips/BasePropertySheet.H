#ifndef __BASEPROPERTYSHEET_H__
#define __BASEPROPERTYSHEET_H__

//#include "CCSGlobl.h"
 
// BasePropertySheet.h : header file
//
#define ID_BUTTON_SAVE		GetString(IDS_STRING1370) //"Speichern"
#define ID_BUTTON_DELETE	GetString(IDS_STRING1371) //"L�schen"
#define ID_BUTTON_APPLY		GetString(IDS_STRING1369) //"Anwenden"

#include <CViewer.h>
/////////////////////////////////////////////////////////////////////////////
// BasePropertySheet

class BasePropertySheet : public CPropertySheet
{
	DECLARE_DYNAMIC(BasePropertySheet)

// Construction
public:
	BasePropertySheet(LPCSTR pszCaption, CWnd* pParentWnd = NULL,
		CViewer *popViewer = NULL, UINT iSelectPage = 0);

// Attributes
protected:
	CWnd *pomParentWnd;	// require for PropertySheet protection logic
	CViewer *pomViewer;
	CStatic omSeparator;
	static int imCount;
	bool bmViews;		// Display the view-selection?
	RECT m_rctPage;
	CFont m_fntPage;

// Operations
public:
	virtual void LoadDataFromViewer();
	virtual void SaveDataToViewer(CString opViewName,BOOL bpSaveToDb = TRUE);
	virtual int QueryForDiscardChanges();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(BasePropertySheet)
	public:
	virtual int DoModal();
	virtual BOOL OnInitDialog();
	virtual BOOL DestroyWindow();
	virtual BOOL ContinueModal();
	protected:
	virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~BasePropertySheet();

	// Generated message map functions
protected:
	virtual void BuildPropPageArray ();
	//{{AFX_MSG(BasePropertySheet)
	afx_msg void OnViewSelChange();
	afx_msg void OnSave();
	afx_msg void OnDelete();
	afx_msg void OnOK();
	afx_msg void OnCancel();
	afx_msg bool OnApply();
	//}}AFX_MSG
	afx_msg LONG OnResizePage (UINT, LONG);
	DECLARE_MESSAGE_MAP()

// Help routines
protected:
	bool bmViewChanged;

	void UpdateComboBox();
	CString GetComboBoxText();
	BOOL IsIdentical(CStringArray &ropArray1, CStringArray &ropArray2);
	BOOL IsInArray(CString &ropString, CStringArray &ropArray);
	BOOL CheckViewName(CString &olViewName);
	bool CheckXDays();
};

/////////////////////////////////////////////////////////////////////////////
#endif //__BASEPROPERTYSHEET_H__
