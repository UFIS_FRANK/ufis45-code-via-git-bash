// Class for Fpeipment
#ifndef _CEDAFPEDATA_H_
#define _CEDAFPEDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct FpeDataStruct
{
	long	Urno;
	CTime	Vafr;
	CTime	Vato;
	char	Pern[33];
	char	Adid[2];
	char	Alco[4];
	char	Flno[11];
	char	Sufx[2];
	char	Regn[13];
	char	Doop[8];

	// internal fields
	int		Score;

	FpeDataStruct(void)
	{
		Urno = 0L;
		strcpy(Pern,"");
		strcpy(Adid,"");
		strcpy(Alco,"");
		strcpy(Flno,"");
		strcpy(Sufx,"");
		strcpy(Regn,"");
		strcpy(Doop,"");
	}
};

typedef struct FpeDataStruct FPEDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaFpeData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <FPEDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapStringToPtr omAlcoMap;

	char pcmFlightPermitsPath[256];
	CString omFtyps;
	bool bmFlightPermitsEnabled;

// Operations
public:
	CedaFpeData();
	~CedaFpeData();
	bool ReadFpeData();
	FPEDATA* GetFpeByUrno(long lpUrno);
	void ProcessFpeBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	bool FlightHasPermit(CTime opTime, bool bpArrival, CString opRegn, CString opAlco, CString opFlno, CString opSufx);
	FPEDATA* GetBestPermitForFlight(CTime opTime, bool bpArrival, CString opRegn, CString opAlco, CString opFlno, CString opSufx, CString *popPermitInfo = NULL);
	int TestMatch(FPEDATA *prpFpe, CTime opTime, bool bpArrival, CString opRegn, CString opFlno, CString opSufx, CString *popTrace = NULL);
	bool CheckDoop(CString opFpeDoop, CTime opFlightTime);
	bool StartFlightPermitsAppl(CTime opTime, bool bpArrival, CString opRegn, CString opAlco, CString opFlno, CString opSufx);
	bool FtypEnabled(const char *pcpFtyp);

private:
	FPEDATA *AddFpeInternal(FPEDATA &rrpFpe);
	void AddFpeToAlcoMap(FPEDATA *prpFpe);
	void DeleteFpeInternal(long lpUrno);
	void DeleteFpeFromAlcoMap(const char *pcpAlco, long lpFpeUrno);
	void ClearAll();
	CTime UtcToLocal(CTime opTime);
	void DisplayTime(CTime opTime);
};


extern CedaFpeData ogFpeData;
#endif _CEDAFPEDATA_H_
