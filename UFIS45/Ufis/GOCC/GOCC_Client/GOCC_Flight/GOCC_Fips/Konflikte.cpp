// Konflikte.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <fpms.h>
#include <Konflikte.h>
#include <ButtonListDlg.h>
#include <CCSGlobl.h>
#include <resrc1.h>
#include <SpotAllocation.h>
#include <BltDiagram.h>
#include <Utils.h>
#include <DataSet.h>
#include <SeasonDlgCedaFlightData.h>
#include <CcaDiagram.h>
#include <KonflikteDlg.h>
#include <GatDiagram.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld Konflikte 

static int CompareByRkey(const DIAFLIGHTDATA **e1, const DIAFLIGHTDATA **e2)
{
	if((**e1).Rkey != (**e2).Rkey)
	{
		return (((**e1).Rkey == (**e2).Rkey) ? 0 : (((**e1).Rkey > (**e2).Rkey) ? 1 : -1));
	}
	else
	{
		int ilRet;

		if(strcmp((**e2).Adid, "B") == 0 && strcmp((**e1).Adid, "B") == 0)
		{
			CTime olTime1;
			CTime olTime2;
				
			if((**e1).Tifa == TIMENULL)
				olTime1 = (**e1).Tifd;
			else
				olTime1 = (**e1).Tifa;

			if((**e2).Tifa == TIMENULL)
				olTime2 = (**e2).Tifd;
			else
				olTime2 = (**e2).Tifa;

			ilRet =  ((olTime1 == olTime2) ? 0 : ((olTime1 > olTime2) ? 1 : -1));
		}
		else
		{
			ilRet = strcmp((**e1).Adid, (**e2).Adid);

		}

		return ilRet;
	}
}

static int Time(const KONFDATA **e1, const KONFDATA **e2)
{
		return ((**e1).TimeOfConflict == (**e2).TimeOfConflict)? 0: 
				((**e1).TimeOfConflict >  (**e2).TimeOfConflict)? 1: -1;
}


static int CompareRotationFlight(const DIAFLIGHTDATA **e1, const DIAFLIGHTDATA **e2)
{
	CTime olTime1;
	CTime olTime2;
	int ilRet = 0;


	if(strcmp((**e2).Adid, "B") == 0 && strcmp((**e1).Adid, "B") == 0)
	{

		if((**e1).Stoa == TIMENULL)
			olTime1 = (**e1).Stod;
		else
			olTime1 = (**e1).Stoa;

		if((**e2).Stoa == TIMENULL)
			olTime2 = (**e2).Stod;
		else
			olTime2 = (**e2).Stoa;

		ilRet =  ((olTime1 == olTime2) ? 0 : ((olTime1 > olTime2) ? 1 : -1));
	}
	else
	{
		ilRet = strcmp((**e1).Adid, (**e2).Adid);

	}


	return ilRet;
}







#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))

Konflikte::Konflikte()
{

	omNoOnblAfterLand	= CTimeSpan(0,0,15,0);
	omCurrEtai			= CTimeSpan(0,0,5,0);
	omCurrDNxti = CTimeSpan(0,0,-5,0);
	omCurrANxti = CTimeSpan(0,0,-5,0);
	omStoaStod = CTimeSpan(0,0,5,0);
	omStodCurrAndNoAirb = CTimeSpan(0,0,5,0);
	omCurrOfbl = CTimeSpan(0,0,15,0);
	omCurrOfblStod = CTimeSpan(0,0,5,0);
	omStodEtdi = CTimeSpan(0,0,15,0);
	omStoaEtai = CTimeSpan(0,0,15,0);
	omCurrStodGd1x = CTimeSpan(0,0,30,0);


	bmNoOnblAfterLand = true;
	bmCurrEtai = true;
	bmCurrANxti = true;
	bmCurrDNxti = true;
	bmStoaStod = true;
	bmStodCurrAndNoAirb = true;
	bmCurrOfbl = true;
	bmCurrOfblStod = true;
	bmStodEtdi = true;
	bmCurrStodGd1x = true;

	bmJoinConf = true;
	bmAderConf = true;
	bmActRegnChangeConf = true;

	bmPstConf = true;
	bmGatConf = true;
	bmWroConf = true;
	bmBltConf = true;

	bmStoaEtai = true;
	bmFlightDataChanged = true;
	bmPosGateRelation = true;

	imBltConflicts = 0;

	bmAttentionButtonUpdate = true;
}


void Konflikte::SetConfiguration()
{
	if (bgGATPOS)
	{ // new GATPOS

		CCSPtrArray<CONFLICTDATA> olConflictData;

		ogCfgData.GetConflictData(olConflictData);

		int ilSize = olConflictData.GetSize();
		for (int ilC = 0; ilC < ilSize; ilC++)
		{
			CONFLICTDATA *prlConflictData = &olConflictData[ilC];

			switch(prlConflictData->imType)
			{

			case IDS_STRING1430 : 	// line 1	= start-landing order  IDS_STRING1430
					break;
			case IDS_STRING1236 : // line 2	= No onblock at touch down + n minutes IDS_STRING1236
						omNoOnblAfterLand	= CTimeSpan(0,0,prlConflictData->imDuration,0);
					break;
			case IDS_STRING1237 : // line 3	= Actual time > ETA + n minutes   IDS_STRING1237
						omCurrEtai			= CTimeSpan(0,0,prlConflictData->imDuration,0);
					break;
			case IDS_STRING1238 :	// line 4	= Actual time + n minutes > next information time (Arrival)  IDS_STRING1238
				omCurrANxti			= CTimeSpan(0,0,prlConflictData->imDuration,0);
					break;
			case IDS_STRING1428 : // line 5	= Pos:() A/C() error () from parking system  IDS_STRING1428
					break;
			case IDS_STRING1432 : // line 6	= STA / ETA >= STD / ETD   IDS_STRING1432
					omStoaStod			= CTimeSpan(0,0,prlConflictData->imDuration,0);
					break;
			case IDS_STRING1549 : // line 7	= STD / ETD + n minutes < actual time and no DEP telegram received  IDS_STRING1549
					omStodCurrAndNoAirb			= CTimeSpan(0,0,prlConflictData->imDuration,0);
					break;
			case IDS_STRING1239 : // line 8	= No take off time at offblock + n minutes    IDS_STRING1239
					omCurrOfbl			= CTimeSpan(0,0,prlConflictData->imDuration,0);
					break;
			case IDS_STRING1240 : // line 9	= No offblock at STD + n minutes  IDS_STRING1240
					omCurrOfblStod		= CTimeSpan(0,0,prlConflictData->imDuration,0);
					break;
			case IDS_STRING1241 : // line 10	= | ETD - STD | > n minutes  IDS_STRING1241
					omStodEtdi			= CTimeSpan(0,0,prlConflictData->imDuration,0);
					break;
			case IDS_STRING1242 : // line 11	= Actual time + n minutes > next information time (Departure)  IDS_STRING1242
					omCurrDNxti			= CTimeSpan(0,0,prlConflictData->imDuration,0);
					break;
			case IDS_STRING1548 : // line 12	= STD / ETD - n minutes < actual time and no boarding information received IDS_STRING1548
				omCurrStodGd1x			= CTimeSpan(0,0,prlConflictData->imDuration,0);
					break;
			case IDS_STRING1860 : // line 13	= | ETA - STA | > n minutes  IDS_STRING1860
				omStoaEtai			= CTimeSpan(0,0,prlConflictData->imDuration,0);
					break;
			case IDS_STRING2353 : // line 14	= CTOT > STD / ETD + n minutes  IDS_STRING2353
				omCtotEtod			= CTimeSpan(0,0,prlConflictData->imDuration,0);
					break;
			case IDS_STRING2354 : // line 15	= No offblock at CTOT + n minutes   IDS_STRING2354
				omOfblCtot			= CTimeSpan(0,0,prlConflictData->imDuration,0);
					break;
			case IDS_STRING2001 : // line 16	= Position - Gate relation  IDS_STRING2303  (IDS_STRING2001)
					break;
			case IDS_STRING2306 : // line 17	= Gate - Lounge relation IDS_STRING2306
					break;
			case IDS_STRING2304 : // line 18	= Gate/Port - Baggage belt relation IDS_STRING2304
					break;
			case IDS_STRING2305 : // line 19	= Baggage belt - Exit relation    IDS_STRING2305
					break;
			case IDS_STRING2340 : // line 20	= Check-In counter - position relation    IDS_STRING2340
					break;
			case IDS_STRING2341 : // line 21	= Check-In counter - gate relation  IDS_STRING2341
					break;
			case IDS_STRING2342 : // line 22	= Check-In counter - Lounge relation   IDS_STRING2342
					break;
			case IDS_STRING1697 : // line 23	= Return Flight  IDS_STRING1697
					break;
			case IDS_STRING1776 : // line 24	= diverted  IDS_STRING1776
					break;
			case IDS_STRING1777 : // line 25	= Returned from Taxi  IDS_STRING1777
					break;
			case IDS_STRING1890 : // line 26	= Departure/Towing before Arrival IDS_STRING1890
					break;
			case IDS_STRING1981 : // line 27	= Towing can not go off block if the flight is not on block! IDS_STRING1981
					break;
			default : ; // should never happen
		}

		bmJoinConf = true;
		bmNoOnblAfterLand = true;
		bmCurrEtai = true;
		bmCurrANxti = true;
		bmAderConf = true;
		bmStoaStod = true;
		bmStodCurrAndNoAirb = true;
		bmActRegnChangeConf = true;
		bmCurrOfbl = true;
		bmCurrOfblStod = true;
		bmStodEtdi = true;
		bmCurrDNxti = true;
		bmCurrStodGd1x = true;
		bmPstConf = true;
		bmStoaEtai = true;
		bmFlightDataChanged = true;
		bmPosGateRelation = true;


//		ogTimeSpanPostFlight = CTimeSpan(0,0,0,0);
//		ogTimeSpanPostFlightInFuture = CTimeSpan(0,0,0,0);

		}

		olConflictData.DeleteAll();
	}
	else
	{ // old GATPOS

		CString olList = ogCfgData.GetConflictSetup();

		CStringArray olConf;

		/* Conflict No. 1		Start-landing order!  */
		/* Conflict No. 2	15	No Onblock at Touch down + %d  Minutes */
		/* Conflict No. 3	5	Actual Time > ETA + %d Minutes  */
		/* Conflict No. 4	-5	Actual Time  + %d Min > Next Information Time  */
		/* Conflict No. 5		Pos: %s  A/C: %s  error (%s) from parking system !  */
		/* Conflict No. 6		STA/ETA    >=    STD/ETD  */
		/* Conflict No. 7		STD/ETD + 30 Min. < actual time and no DEP telegram received  */
		/* Conflict No. 8		Aircrafttype has been changed */
		/* Conflict No. 9	-5	No Take off time at Offblock + %d Minutes */
		/* Conflict No. 10	5	No Offblock at STD + %d Minutes */
		/* Conflict No. 11	15	ETD > STD + %d Minutes  */
		/* Conflict No. 12	-5	Actual Time + %d Min > Next Information Time  */
		/* Conflict No. 13	30	STD/ETD - 30 Min. < actual time and no boarding information received */
		/* Conflict No. 14	15	ETA > STA + %d Minutes  */

		if( !olList.IsEmpty() )
		{
			ExtractItemList(olList, &olConf, ';');

			if( olConf.GetSize() >= 31)
			{

				if( olConf[0] == "0")
					bmJoinConf = false;
				else
					bmJoinConf = true;

				if( olConf[2] == "0")
					bmNoOnblAfterLand = false;
				else
					bmNoOnblAfterLand = true;
				if( olConf[3] != "-")
					omNoOnblAfterLand	= CTimeSpan(0,0,atoi(olConf[3]),0);

				if( olConf[4] == "0")
					bmCurrEtai = false;
				else
					bmCurrEtai = true;
				if( olConf[5] != "-")
					omCurrEtai			= CTimeSpan(0,0,atoi(olConf[5]),0);

				if( olConf[6] == "0")
					bmCurrANxti = false;
				else
					bmCurrANxti = true;
				if( olConf[7] != "-")
					omCurrANxti			= CTimeSpan(0,0,atoi(olConf[7]),0);

				if( olConf[8] == "0")
					bmAderConf = false;
				else 
					bmAderConf = true;

				if( olConf[10] == "0")
					bmStoaStod = false;
				else
					bmStoaStod = true;
				if( olConf[11] != "-")
					omStoaStod			= CTimeSpan(0,0,atoi(olConf[11]),0);

				if( olConf[12] == "0")
					bmStodCurrAndNoAirb = false;
				else
					bmStodCurrAndNoAirb = true;
				if( olConf[13] != "-")
					omStodCurrAndNoAirb			= CTimeSpan(0,0,atoi(olConf[13]),0);

				if( olConf[14] == "0")
					bmActRegnChangeConf = false;
				else
					bmActRegnChangeConf = true;

				if( olConf[16] == "0")
					bmCurrOfbl = false;
				else
					bmCurrOfbl = true;

				if( olConf[17] != "-")
					omCurrOfbl			= CTimeSpan(0,0,atoi(olConf[17]),0);

				if( olConf[18] == "0")
					bmCurrOfblStod = false;
				else
					bmCurrOfblStod = true;

				if( olConf[19] != "-")
					omCurrOfblStod		= CTimeSpan(0,0,atoi(olConf[19]),0);


				if( olConf[20] == "0")
					bmStodEtdi = false;
				else
					bmStodEtdi = true;

				if( olConf[21] != "-")
					omStodEtdi			= CTimeSpan(0,0,atoi(olConf[21]),0);

				if( olConf[22] == "0")
					bmCurrDNxti = false;
				else
					bmCurrDNxti = true;

				if( olConf[23] != "-")
					omCurrDNxti			= CTimeSpan(0,0,atoi(olConf[23]),0);

				if( olConf[24] == "0")
					bmCurrStodGd1x = false;
				else
					bmCurrStodGd1x = true;

				if( olConf[25] != "-")
					omCurrStodGd1x			= CTimeSpan(0,0,atoi(olConf[25]),0);

				if( olConf[26] == "0")
					bmPstConf = false;
				else
					bmPstConf = true;

				if( olConf[28] == "0")
					bmPstConf = false;
				else
					bmPstConf = true;

				if( olConf[30] == "0")
					bmPstConf = false;
				else
					bmPstConf = true;

				if( olConf[32] == "0")
					bmPstConf = false;
				else
					bmPstConf = true;
			}

			if( olConf.GetSize() >= 36)
			{
				if( olConf[34] == "0")
					bmStoaEtai = false;
				else
					bmStoaEtai = true;
				if( olConf[35] != "-")
					omStoaEtai			= CTimeSpan(0,0,atoi(olConf[35]),0);
			}
			if( olConf.GetSize() >= 37)
			{
				if( olConf[36] == "0")
					bmFlightDataChanged = false;
				else
					bmFlightDataChanged = true;
			}
			if( olConf.GetSize() >= 39)
			{
				if( olConf[38] == "0")
					bmPosGateRelation = false;
				else
					bmPosGateRelation = true;
			}

			//rkr20042001
			// this part is only to perform the values to next version
			// the archivedays now stored in own records.
			// if conflicts must be added you can remove this part
			if( olConf.GetSize() >= 42)
				ogTimeSpanPostFlight = CTimeSpan(atoi(olConf[41]),0,0,0);
			else
				ogTimeSpanPostFlight = CTimeSpan(0,0,0,0);

			if( olConf.GetSize() >= 43)
				ogTimeSpanPostFlightInFuture = CTimeSpan(atoi(olConf[42]),0,0,0);
			else
				ogTimeSpanPostFlightInFuture = CTimeSpan(0,0,0,0);
			// if the conflicts must be added you can remove this part
			//rkr20042001


		}
	}
}





Konflikte::~Konflikte()
{

	ClearAll();

}


void Konflikte::ClearAll()
{
	omUrnoMap.RemoveAll();
	omNotConfirmedMap.RemoveAll();
	imBltConflicts = 0;

	KONFENTRY *prlEntry = NULL;

	for(int i = omData.GetSize() - 1; i >= 0; i--)
	{
		omData[i].Data.DeleteAll();
	}
	omData.DeleteAll();


}


void Konflikte::RemoveAll(DWORD dwOrigin)
{
	KONFENTRY *prlEntry = NULL;
	KONFDATA  *prlData = NULL;
	KonfIdent rlIdNotify;

	for(int j = omData.GetSize() - 1; j >= 0; j--)
	{
		prlEntry = &omData[j];

		ROTATIONFLIGHTDATA *prlFlight;
		if (ogRotationFlights.omUrnoMap.Lookup((void *)prlEntry->Urno,(void *& )prlFlight))
			continue;

		DIAFLIGHTDATA *prlFlightDia;
		if (ogPosDiaFlightData.omUrnoMap.Lookup((void *)prlEntry->Urno,(void *& )prlFlightDia))
			continue;


		for(int i = prlEntry->Data.GetSize() - 1; i >= 0; i--)
		{
			prlData = &prlEntry->Data[i];

			//if(prlData->SetBy & dwOrigin)
			{
				prlData->SetBy = prlData->SetBy & ~dwOrigin;
				if (omNotConfirmedMap.RemoveKey((void*) prlData) != 0)
				{
					if (IsBltKonfliktType(prlData->KonfId.Type))
					{
						imBltConflicts--;
					}
				}
				rlIdNotify = prlData->KonfId;
				ogDdx.DataChanged((void *)this, KONF_DELETE,(void *)&rlIdNotify );

				prlEntry->Data.DeleteAt(i);
			}
		}
		if (prlEntry->Data.GetSize() == 0)
		{
			omUrnoMap.RemoveKey((void*) prlEntry->Urno);
			omData.DeleteAt(j);
		}

	}



	/*
	for(int i = omData.GetSize() - 1; i >= 0; i--)
	{
		prlEntry = &omData[i];

		for(int i = prlEntry->Data.GetSize() - 1; i >= 0; i--)
		{
			prlData = &prlEntry->Data[i];
			prlData->SetBy = prlData->SetBy & ~dwOrigin;

			if(prlData->SetBy == 0)
			{
				if (omNotConfirmedMap.RemoveKey((void*) prlData) != 0)
				{
					if (IsBltKonfliktType(prlData->Type))
					{
						imBltConflicts--;
					}
				}

			}
		}
	}
	*/

	CheckAttentionButton();
}


void Konflikte::RemoveAllForFlight(long lpUrno, DWORD dwOrigin)
{
	KONFENTRY *prlEntry = NULL;

	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlEntry) == TRUE)
	{
		KONFDATA  *prlData = NULL;
		KonfIdent rlIdNotify;
		for(int i = prlEntry->Data.GetSize() - 1; i >= 0; i--)
		{
			prlData = &prlEntry->Data[i];
			if(prlData->SetBy & dwOrigin)
			{
				prlData->SetBy = prlData->SetBy & ~dwOrigin;
				if (omNotConfirmedMap.RemoveKey((void*) prlData) != 0)
				{
					if (IsBltKonfliktType(prlData->KonfId.Type))
					{
						imBltConflicts--;
					}
				}
				rlIdNotify = prlData->KonfId;
				ogDdx.DataChanged((void *)this, KONF_DELETE,(void *)&rlIdNotify );

				prlEntry->Data.DeleteAt(i);
			}
		}
		if (prlEntry->Data.GetSize() == 0)
		{
			omUrnoMap.RemoveKey((void*) lpUrno);
			for(int i = omData.GetSize() - 1; i >= 0; i--)
			{
				if(omData[i].Urno == lpUrno)
				{
					omData.DeleteAt(i);
					break;
				}

			}
		}
	}


	CheckAttentionButton();
}

void Konflikte::RemoveKonfliktForFlight(long lpUrno, DWORD dwOrigin, int ipType)
{
	KONFENTRY *prlEntry = NULL;

	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlEntry) == TRUE)
	{
		KONFDATA  *prlData = NULL;
		KonfIdent rlIdNotify;
		for(int i = prlEntry->Data.GetSize() - 1; i >= 0; i--)
		{
			prlData = &prlEntry->Data[i];
			//if(prlData->SetBy & dwOrigin)
			{
				prlData->SetBy = prlData->SetBy & ~dwOrigin;
				if (omNotConfirmedMap.RemoveKey((void*) prlData) != 0)
				{
				}

				rlIdNotify = prlData->KonfId;
				if (rlIdNotify.Type == ipType)
				{
					ogDdx.DataChanged((void *)this, KONF_DELETE,(void *)&rlIdNotify );
					prlEntry->Data.DeleteAt(i);
				}
			}
		}
		if (prlEntry->Data.GetSize() == 0)
		{
			omUrnoMap.RemoveKey((void*) lpUrno);
			for(int i = omData.GetSize() - 1; i >= 0; i--)
			{
				if(omData[i].Urno == lpUrno)
				{
					omData.DeleteAt(i);
					break;
				}

			}
		}
	}
}

bool Konflikte::CompareConflictText(CString opText, CString &ropRtab, char cpFPart, char cpRelFPart, long lpUrno, long lpRelUrno, DWORD dwOrigin, int ipType)
{
	KONFENTRY *prlEntry = NULL;
	bool blRet = true;

	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlEntry) == TRUE)
	{
		KONFDATA  *prlData = NULL;
		KonfIdent rlIdNotify;
		for(int i = prlEntry->Data.GetSize() - 1; i >= 0; i--)
		{
			prlData = &prlEntry->Data[i];
			//if(prlData->SetBy & dwOrigin)
			{
				rlIdNotify = prlData->KonfId;
				if (rlIdNotify.Type == ipType)
				{
					CString olTextOld("");
					CString olTextNew = opText;
					CString olTextTime = prlData->Text.Right(4);
					olTextTime = olTextTime.Left(1);
					if (olTextTime == ":")
						olTextOld = prlData->Text.Mid(0,prlData->Text.GetLength()-6);
					else
						olTextOld = prlData->Text;

					olTextOld.Remove(' ');

					olTextNew.Remove(' ');

					if (olTextNew.Find(olTextOld) == -1)
					{
						blRet = false;
						ogPosDiaFlightData.pomCflData->UnConfirm(ropRtab, cpFPart, cpRelFPart, lpUrno, lpRelUrno, ipType);
						ogRotationFlights.omCflData.UnConfirm(ropRtab, cpFPart, cpRelFPart, lpUrno, lpRelUrno, ipType);
					}
				}
			}
		}
		if (prlEntry->Data.GetSize() == 0)
		{
			omUrnoMap.RemoveKey((void*) lpUrno);
			for(int i = omData.GetSize() - 1; i >= 0; i--)
			{
				if(omData[i].Urno == lpUrno)
				{
					omData.DeleteAt(i);
					break;
				}

			}
		}
	}

	return blRet;
}



void Konflikte::CleanUp()
{
	CTime olTime = CTime::GetCurrentTime();

	ogBasicData.LocalToUtc(olTime);

	CTimeSpan olSpan(0,3,0,0);

	olTime -= olSpan;

	KONFENTRY *prlEntry = NULL;
	KONFDATA  *prlData = NULL;

	KonfIdent rlIdNotify;

	for(int i = omData.GetSize() - 1; i >= 0; i--)
	{
		prlEntry = &omData[i];

		for(int i = prlEntry->Data.GetSize() - 1; i >= 0; i--)
		{
			if((prlEntry->Data[i].SetBy == 0) && (prlEntry->Data[i].TimeOfConflict < olTime) && (prlEntry->Data[i].Confirmed == true))
			{
				rlIdNotify = prlEntry->Data[i].KonfId;

				ogDdx.DataChanged((void *)this, KONF_DELETE,(void *)&rlIdNotify );

				prlEntry->Data.DeleteAt(i);
			}
		}
		if(prlEntry->Data.GetSize() == 0)
		{
			omUrnoMap.RemoveKey((void *)prlEntry->Urno);

			for(int i = omData.GetSize() - 1; i >= 0; i--)
			{
				if(omData[i].Urno == prlEntry->Urno)
				{
					omData.DeleteAt(i);
					break;
				}

			}

		}
	}
	CheckAttentionButton();
}


KONFDATA *Konflikte::GetKonflikt(long lpFUrno, int ipType)
{
	KONFENTRY *prlEntry = NULL;
	KONFDATA  *prlData = NULL;

	if (omUrnoMap.Lookup((void*)lpFUrno, (void *& )prlEntry) == TRUE)
	{
		for(int i = prlEntry->Data.GetSize() - 1; i >= 0; i--)
		{
			if(prlEntry->Data[i].KonfId.Type == ipType)
			{
				return &prlEntry->Data[i];
			}
		}
	}
	return prlData;
}


KONFDATA *Konflikte::GetKonflikt(const KonfIdent &ropKonfId)
{
	KONFENTRY *prlEntry = NULL;
	KONFDATA  *prlData = NULL;

	if (omUrnoMap.Lookup((void *)ropKonfId.FUrno, (void *& )prlEntry) == TRUE)
	{
		for(int i = prlEntry->Data.GetSize() - 1; i >= 0; i--)
		{
			if(prlEntry->Data[i].KonfId == ropKonfId)
			{
				return &prlEntry->Data[i];
			}
		}
	}
	return prlData;
}




int Konflikte::GetStatus(long lpFUrno, char cpFPart, DWORD dwOrigin, int ipSubOrig)
{
	KONFENTRY *prlEntry = NULL;
	KONFDATA  *prlData = NULL;
	int ilCount = 0;
	int ilConfirmedCount = 0;

	if (omUrnoMap.Lookup((void *)lpFUrno,(void *& )prlEntry) == TRUE)
	{
		for(int i = prlEntry->Data.GetSize() - 1; i >= 0; i--)
		{
			prlData = &prlEntry->Data[i];
			if (prlData->KonfId.FPart != cpFPart)
				continue;

			if(prlData->SetBy & dwOrigin)
			{
				if(prlData->SubId & ipSubOrig || ipSubOrig == SUB_MOD_ALL || prlData->SubId == SUB_MOD_ALL)
				{
					ilCount++;
					if(prlData->Confirmed)
					{
						ilConfirmedCount++;
					}
					else
					{
						return 1;
				}
			}
		}
	}
	}

	if(ilCount == 0)
	{
		return -1;
	}
	else
	{
		if(ilCount == ilConfirmedCount)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
}


int Konflikte::GetStatusByDisplay(long lpFUrno, char cpFPart, DWORD dwOrigin, int ipSubOrig,  int ipDisplayType)
{
	KONFENTRY *prlEntry = NULL;
	KONFDATA  *prlData = NULL;
	int ilCount = 0;
	int ilConfirmedCount = 0;

	if (omUrnoMap.Lookup((void *)lpFUrno,(void *& )prlEntry) == TRUE)
	{
		for(int i = prlEntry->Data.GetSize() - 1; i >= 0; i--)
		{
			prlData = &prlEntry->Data[i];
			if (prlData->KonfId.FPart != cpFPart  || prlData->Flag == false)
				continue;

//			if(prlData->SetBy & dwOrigin)
			{
//				if(prlData->SubId & ipSubOrig || ipSubOrig == SUB_MOD_ALL || prlData->SubId == SUB_MOD_ALL)
				if (ogCfgData.CheckKonfliktForDisplay(prlData->KonfId.Type,ipDisplayType) == true)
				{
					ilCount++;
					if(prlData->Confirmed)
					{
						ilConfirmedCount++;
					}
					else
					{
						return 1;
					}

				}
			}
		}
	}

	if(ilCount == 0)
	{
		return -1;
	}
	else
	{
		if(ilCount == ilConfirmedCount)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
}


void Konflikte::GetKonflikte(long lpFUrno, char cpFPart, DWORD dwOrigin, int ipSubOrig, CCSPtrArray<KonfItem> &opKonfList)
{
	
	KonfItem *prlKonf;
	KONFENTRY *prlEntry = NULL;
	KONFDATA  *prlData = NULL;
	int ilCount = 0;
	int ilConfirmedCount = 0;

	if (omUrnoMap.Lookup((void *)lpFUrno,(void *& )prlEntry) == TRUE)
	{
		for(int i = prlEntry->Data.GetSize() - 1; i >= 0; i--)
		{
			prlData = &prlEntry->Data[i];
			if (prlData->KonfId.FPart != cpFPart)
				continue;

			if(prlData->SetBy & dwOrigin)
			{
				if(prlData->SubId & ipSubOrig || ipSubOrig == SUB_MOD_ALL || prlData->SubId == SUB_MOD_ALL)
				{
					

					prlKonf = new KonfItem;

					prlKonf->Text = prlData->Text;

					prlKonf->KonfId = prlData->KonfId;
					
					opKonfList.Add(prlKonf);

				}
			}
		}
	}

}

void Konflikte::GetKonflikteForDisplay(long lpFUrno, char cpFPart, DWORD dwOrigin, int ipSubOrig, int ipDisplayType, CCSPtrArray<KonfItem> &opKonfList)
{
	
	KonfItem *prlKonf;
	KONFENTRY *prlEntry = NULL;
	KONFDATA  *prlData = NULL;
	int ilCount = 0;
	int ilConfirmedCount = 0;

	if (omUrnoMap.Lookup((void *)lpFUrno,(void *& )prlEntry) == TRUE)
	{
		for(int i = prlEntry->Data.GetSize() - 1; i >= 0; i--)
		{
			prlData = &prlEntry->Data[i];
			if (prlData->KonfId.FPart != cpFPart || prlData->Flag == false)
				continue;

/*			if(prlData->SetBy & dwOrigin)
//			{
//				if(prlData->SubId & ipSubOrig || ipSubOrig == SUB_MOD_ALL || prlData->SubId == SUB_MOD_ALL)
				{
*/					
					if (ogCfgData.CheckKonfliktForDisplay(prlData->KonfId.Type,ipDisplayType) == true)
					{

						prlKonf = new KonfItem;

						prlKonf->Text = prlData->Text;

						prlKonf->KonfId = prlData->KonfId;
						
						opKonfList.Add(prlKonf);
					}

//				}
//			}
		}
	}

}



void Konflikte::ResetFlight(long lpFUrno, char cpFPart, DWORD dwOrigin )
{
	KONFENTRY *prlEntry = NULL;
	KONFDATA  *prlData = NULL;

	if (omUrnoMap.Lookup((void *)lpFUrno,(void *& )prlEntry) == TRUE)
	{
		for(int i = prlEntry->Data.GetSize() - 1; i >= 0; i--)
		{
			if (prlEntry->Data[i].KonfId.FPart != cpFPart)
				continue;

			if(prlEntry->Data[i].SetBy  & dwOrigin)
			{
				prlEntry->Data[i].Flag = false;
			}
		}
	}
}



void Konflikte::ResetAllNeighborConfFlight(long lpFUrno, char cpFPart)
{
	KONFENTRY *prlEntry = NULL;
	KONFDATA  *prlData = NULL;

	if (omUrnoMap.Lookup((void *)lpFUrno,(void *& )prlEntry) == TRUE)
	{
		for(int i = prlEntry->Data.GetSize() - 1; i >= 0; i--)
		{
			if (prlEntry->Data[i].KonfId.FPart != cpFPart)
				continue;

			if(prlEntry->Data[i].SetBy  & MOD_ID_DIA)
			{
				if (IsNeighborKonfliktType(prlEntry->Data[i].KonfId.Type))
					prlEntry->Data[i].Flag = false;
			}
		}
	}
}



void Konflikte::CheckFlightConf(long lpFUrno, char cpFPart, DWORD dwOrigin)
{
	KONFENTRY *prlEntry = NULL;

	if (omUrnoMap.Lookup((void *)lpFUrno,(void *& )prlEntry) == TRUE)
	{
		KONFDATA  *prlData = NULL;

		KonfIdent rlIdNotify;

		prlEntry->Data.Sort(Time);
		for(int i = 0; i < prlEntry->Data.GetSize(); i++)
		{
			prlData = &prlEntry->Data[i];

			if (prlData->KonfId.FPart != cpFPart)
				continue;

			if(prlData->Flag == false)
			{
				prlData->SetBy = prlData->SetBy & ~dwOrigin;

				if(prlData->SetBy == 0)
				{
					if (omNotConfirmedMap.RemoveKey((void*) prlData) != 0)
					{
//						if (ogCfgData.CheckKonfliktForDisplay(prlData->KonfId.Type,CONF_BLT) == true)
						if (IsBltKonfliktType(prlData->KonfId.Type))
						{
							imBltConflicts--;
						}
					}

					rlIdNotify = prlData->KonfId;
					ogDdx.DataChanged((void *)this, KONF_DELETE,(void *)&rlIdNotify );

					prlEntry->Data.DeleteAt(i);
				}
			}
		}
		if(prlEntry->Data.GetSize() == 0)
		{
			omUrnoMap.RemoveKey((void *)lpFUrno);

			for(int i = omData.GetSize() - 1; i >= 0; i--)
			{
				if(omData[i].Urno == lpFUrno)
				{
					omData.DeleteAt(i);
					break;
				}

			}

		}
	}
}


void Konflikte::DisableAttentionButtonsUpdate()
{
	bmAttentionButtonUpdate = false;
}


void Konflikte::EnableAttentionButtonsUpdate()
{
	bmAttentionButtonUpdate = true;
	CheckAttentionButton();
}


/*
void Konflikte::CheckAttentionButton() 
{
	if (bgPosDiaAutoAllocateInWork)
		return;

	int ilCountBlt = 0;

	if(pogButtonList != NULL)
	{
		if(IsWindow(pogButtonList->m_hWnd) )
		{
			int ilCount = 0;
			KONFENTRY *prlEntry = NULL;
			KONFDATA  *prlData = NULL;

			CPtrArray olConfirmKonfIds;
			DisableAttentionButtonsUpdate();

			for(int i = omData.GetSize() - 1; i >= 0; i--)
			{
				prlEntry = &omData[i];

				for(int j = prlEntry->Data.GetSize() - 1; j >= 0; j--)
				{
					if(prlEntry->Data[j].Confirmed != true)
					{
						if (ogCfgData.CheckKonfliktForDisplay(prlEntry->Data[j].KonfId.Type,CONF_ALL) == true)
						{
							ilCount++;
						}
						else if (ogCfgData.CheckKonfliktForDisplay(prlEntry->Data[j].KonfId.Type,CONF_BLT) == true)
						{
							ilCountBlt++;
						}
					}
				}
			}

		 	CString olText = GetString(IMFK_ATTENTION);
 
			if(ilCount > 0)
			{
 				olText.Format("%s (%d)", GetString(IMFK_ATTENTION), ilCount);
			}
			if(ilCount > 0)
				pogButtonList->m_CB_Achtung.SetColors(RGB(255, 0, 0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			else
				pogButtonList->m_CB_Achtung.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
 
			pogButtonList->m_CB_Achtung.SetWindowText(olText);
		}//end if
	}//end if


	// Update Blt-Attention-Button
	if(pogBltDiagram != NULL)
	{
		if(IsWindow(pogBltDiagram->m_hWnd) )
		{
		 	CString olText = GetString(IMFK_ATTENTION);
 
			if(ilCountBlt > 0)
			{
 				olText.Format("%s (%d)", GetString(IMFK_ATTENTION), ilCountBlt);
			}

			if(ilCountBlt > 0)
				pogBltDiagram->m_CB_BltAttention.SetColors(RGB(255, 0, 0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			else
				pogBltDiagram->m_CB_BltAttention.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
 
			pogBltDiagram->m_CB_BltAttention.SetWindowText(olText);
		}//end if
	}//end if
}
*/

void Konflikte::CheckAttentionButton() 
{
	if (bgPosDiaAutoAllocateInWork)
		return;

	int ilCountBlt = 0;
	int ilCountCca = 0;

	if(pogButtonList != NULL)
	{
		if(IsWindow(pogButtonList->m_hWnd) )
		{
	//		int ilCount =	omNotConfirmedMap.GetCount() - imBltConflicts;
	
			int ilCount = 0;
			KONFENTRY *prlEntry = NULL;
			KONFDATA  *prlData = NULL;

			CPtrArray olConfirmKonfIds;
			DisableAttentionButtonsUpdate();

			for(int i = omData.GetSize() - 1; i >= 0; i--)
			{
				prlEntry = &omData[i];

				for(int j = prlEntry->Data.GetSize() - 1; j >= 0; j--)
				{
					prlData = &prlEntry->Data[j] ;
					ASSERT( prlData );		// must exist I guess
					prlData->UpdatePriority( prlData->KonfId.Type );		// 050303 MVy: deal with conflict priority color, this is called updating or changing the conflicts in list, eg. if the color is changed and saved in rule setup

					if(prlEntry->Data[j].Confirmed != true)
					{
						if (ogCfgData.CheckKonfliktForDisplay(prlEntry->Data[j].KonfId.Type,CONF_ALL) == true)
						{
							ilCount++;
						}
						if (ogCfgData.CheckKonfliktForDisplay(prlEntry->Data[j].KonfId.Type,CONF_BLT) == true)
						{
							ilCountBlt++;
						}
						if (ogCfgData.CheckKonfliktForDisplay(prlEntry->Data[j].KonfId.Type,CONF_CCA) == true)
						{
							ilCountCca++;
						}
					}
				}
			}

		 	CString olText = GetString(IMFK_ATTENTION);
 

			if(ilCount > 0)
			{
 				olText.Format("%s (%d)", GetString(IMFK_ATTENTION), ilCount);
			}
			if(ilCount > 0)
				pogButtonList->m_CB_Achtung.SetColors(RGB(255, 0, 0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			else
				pogButtonList->m_CB_Achtung.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
 
			pogButtonList->m_CB_Achtung.SetWindowText(olText);

			if (pogKonflikteDlg)
			{
//				pogKonflikteDlg->UpdateDisplay();
				pogKonflikteDlg->ChangeViewTo();
			}
		}//end if
	}//end if


	// Update Blt-Attention-Button
	if(pogBltDiagram != NULL)
	{
		if(IsWindow(pogBltDiagram->m_hWnd) )
		{
		 	CString olText = GetString(IMFK_ATTENTION);
 
			if(ilCountBlt > 0)
			{
 				olText.Format("%s (%d)", GetString(IMFK_ATTENTION), ilCountBlt);
			}

			if(ilCountBlt > 0)
				pogBltDiagram->m_CB_BltAttention.SetColors(RGB(255, 0, 0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			else
				pogBltDiagram->m_CB_BltAttention.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
 
			pogBltDiagram->m_CB_BltAttention.SetWindowText(olText);

			pogBltDiagram->UpdateKonflikteDlg();

		}//end if
	}//end if

	// Update Cca-Attention-Button
	if(pogCcaDiagram != NULL)
	{
		if(IsWindow(pogCcaDiagram->m_hWnd) )
		{
		 	CString olText = GetString(IMFK_ATTENTION);
 
			if(ilCountCca > 0)
			{
 				olText.Format("%s (%d)", GetString(IMFK_ATTENTION), ilCountCca);
			}

			if(ilCountCca > 0)
				pogCcaDiagram->m_CB_CcaAttention.SetColors(RGB(255, 0, 0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			else
				pogCcaDiagram->m_CB_CcaAttention.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
 
			pogCcaDiagram->m_CB_CcaAttention.SetWindowText(olText);

			pogCcaDiagram->UpdateKonflikteDlg();


		}//end if
	}//end if
}

void Konflikte::ConfirmInternal(long lpFUrno, int ipType)
{
	KONFDATA *prlData = GetKonflikt(lpFUrno, ipType);
	if (prlData)
	{
		prlData->Confirmed = true;
		if (omNotConfirmedMap.RemoveKey((void*) prlData) != 0)
		{
			if (IsBltKonfliktType(prlData->KonfId.Type))
					imBltConflicts--;
		}
	}
/*


	bpRelHdl = true;
	KONFENTRY *prlEntry = NULL;
	KONFDATA  *prlData = NULL;

	for (int j = 0; j < ropKonfIds.GetSize(); j++)
	{
		KonfIdent* pl = (KonfIdent *)ropKonfIds[j];
		if (omUrnoMap.Lookup((void *)((KonfIdent *)ropKonfIds[j])->FUrno, (void *& )prlEntry) == TRUE)
		{
			for(int i = prlEntry->Data.GetSize() - 1; i >= 0; i--)
			{
				prlData = &prlEntry->Data[i];
				if (prlEntry->Data[i].KonfId != *(KonfIdent *)ropKonfIds[j])
					continue;
				
				// confirm conflict!
				prlEntry->Data[i].Confirmed = true;
				if (omNotConfirmedMap.RemoveKey((void*) &prlEntry->Data[i]) != 0)
				{
					
//					if (ogCfgData.CheckKonfliktForDisplay(prlEntry->Data[i].KonfId.Type,CONF_BLT) == true)
					if (IsBltKonfliktType(prlEntry->Data[i].KonfId.Type))
					{
							imBltConflicts--;
					}
				}
	
				// Do conflict exist in DB?
				// CFLTAB:URNO == ConflictType , when conflict was generated by the flight-hdl!
				if (ogPosDiaFlightData.pomCflData->GetCflByUrno(prlEntry->Data[i].KonfId.Type) == NULL &&
					ogRotationFlights.omCflData.GetCflByUrno(prlEntry->Data[i].KonfId.Type) == NULL)
				{
					// NO! Then create a confirmed record!
					// There are no not confirmed conflicts in the database which was generated from FIPS!!
 					ogPosDiaFlightData.pomCflData->CreateConfirmed(prlEntry->Data[i].KonfId.FUrno, prlEntry->Data[i].KonfId.FPart, prlEntry->Data[i].KonfId.RelFUrno, prlEntry->Data[i].KonfId.RelFPart, prlEntry->Data[i].KonfId.Type, bpRelHdl);
				}
				else
				{
					// only confirm the conflicts
					ogPosDiaFlightData.pomCflData->Confirm(prlEntry->Data[i].KonfId.Type, bpRelHdl);
					ogRotationFlights.omCflData.Confirm(prlEntry->Data[i].KonfId.Type);
				}
			}
//			ogDdx.DataChanged((void *)this, KONF_CHANGE,(void *)pl ); 
		}
	}
	// Release buffered operations
	if (bpRelHdl)
		ogPosDiaFlightData.pomCflData->ReleaseDBUpdate();

	CheckAttentionButton();
*/
}


void Konflikte::Confirm(const CPtrArray &ropKonfIds, bool bpRelHdl /* = false */)
{
	bpRelHdl = true;
	KONFENTRY *prlEntry = NULL;
	KONFDATA  *prlData = NULL;

	for (int j = 0; j < ropKonfIds.GetSize(); j++)
	{
		KonfIdent* pl = (KonfIdent *)ropKonfIds[j];
		if (omUrnoMap.Lookup((void *)((KonfIdent *)ropKonfIds[j])->FUrno, (void *& )prlEntry) == TRUE)
		{
			for(int i = prlEntry->Data.GetSize() - 1; i >= 0; i--)
			{
				prlData = &prlEntry->Data[i];
				if (prlEntry->Data[i].KonfId != *(KonfIdent *)ropKonfIds[j])
					continue;
				
				// confirm conflict!
				prlEntry->Data[i].Confirmed = true;
				if (omNotConfirmedMap.RemoveKey((void*) &prlEntry->Data[i]) != 0)
				{
					
//					if (ogCfgData.CheckKonfliktForDisplay(prlEntry->Data[i].KonfId.Type,CONF_BLT) == true)
					if (IsBltKonfliktType(prlEntry->Data[i].KonfId.Type))
					{
							imBltConflicts--;
					}
				}
	
				// Do conflict exist in DB?
				// CFLTAB:URNO == ConflictType , when conflict was generated by the flight-hdl!
				if (ogPosDiaFlightData.pomCflData->GetCflByUrno(prlEntry->Data[i].KonfId.Type) == NULL &&
					ogRotationFlights.omCflData.GetCflByUrno(prlEntry->Data[i].KonfId.Type) == NULL)
				{
					// NO! Then create a confirmed record!
					// There are no not confirmed conflicts in the database which was generated from FIPS!!
 					ogPosDiaFlightData.pomCflData->CreateConfirmed(prlEntry->Data[i].KonfId.FUrno, prlEntry->Data[i].KonfId.FPart, prlEntry->Data[i].KonfId.RelFUrno, prlEntry->Data[i].KonfId.RelFPart, prlEntry->Data[i].KonfId.Type, bpRelHdl);
				}
				else
				{
					// only confirm the conflicts
					ogPosDiaFlightData.pomCflData->Confirm(prlEntry->Data[i].KonfId.Type, bpRelHdl);
					ogRotationFlights.omCflData.Confirm(prlEntry->Data[i].KonfId.Type);
				}
			}
//			ogDdx.DataChanged((void *)this, KONF_CHANGE,(void *)pl ); 
		}
	}
	// Release buffered operations
	if (bpRelHdl)
		ogPosDiaFlightData.pomCflData->ReleaseDBUpdate();

	CheckAttentionButton();
}

void Konflikte::UnConfirm(const CPtrArray &ropKonfIds, bool bpRelHdl /* = false */)
{
	ASSERT(FALSE);//doesn't work, not finished
	return;


	KONFENTRY *prlEntry = NULL;
	KONFDATA  *prlData = NULL;

	for (int j = 0; j < ropKonfIds.GetSize(); j++)
	{
		KonfIdent* pl = (KonfIdent *)ropKonfIds[j];
		if (omUrnoMap.Lookup((void *)((KonfIdent *)ropKonfIds[j])->FUrno, (void *& )prlEntry) == TRUE)
		{
			for(int i = prlEntry->Data.GetSize() - 1; i >= 0; i--)
			{
				if (prlEntry->Data[i].KonfId != *(KonfIdent *)ropKonfIds[j])
					continue;
				
				// confirm conflict!
				prlEntry->Data[i].Confirmed = false;
				omNotConfirmedMap.SetAt((void*) &prlEntry->Data[i], NULL);

				if (IsBltKonfliktType(prlEntry->Data[i].KonfId.Type))
				{
						imBltConflicts++;
				}
				prlData = &prlEntry->Data[i];

				ogPosDiaFlightData.pomCflData->CreateConfirmed(prlEntry->Data[i].KonfId.FUrno, prlEntry->Data[i].KonfId.FPart, prlEntry->Data[i].KonfId.RelFUrno, prlEntry->Data[i].KonfId.RelFPart, prlEntry->Data[i].KonfId.Type, bpRelHdl);
/*	
				// Do conflict exist in DB?
				// CFLTAB:URNO == ConflictType , when conflict was generated by the flight-hdl!
				if (ogPosDiaFlightData.pomCflData->GetCflByUrno(prlEntry->Data[i].KonfId.Type) != NULL &&
					ogRotationFlights.omCflData.GetCflByUrno(prlEntry->Data[i].KonfId.Type) != NULL)
				{
					// NO! Then create a confirmed record!
					// There are no not confirmed conflicts in the database which was generated from FIPS!!
 					ogPosDiaFlightData.pomCflData->UnConfirm(prlEntry->Data[i].KonfId.FUrno, bpRelHdl);
				}
				else
				{
					// only confirm the conflicts
					ogPosDiaFlightData.pomCflData->Confirm(prlEntry->Data[i].KonfId.Type, bpRelHdl);
					ogRotationFlights.omCflData.Confirm(prlEntry->Data[i].KonfId.Type);
				}
*/
			}
		}
	}
	// Release buffered operations
	if (bpRelHdl)
		ogPosDiaFlightData.pomCflData->ReleaseDBUpdate();

	CheckAttentionButton();
}

void Konflikte::ConfirmAll(ConflictGroups ipGroup)
{
	TRACE("Konflikte::ConfirmAll: Start %s\n", CTime::GetCurrentTime().Format("%H:%M:%S"));
	KONFENTRY *prlEntry = NULL;
	KONFDATA  *prlData = NULL;
	//KonfNotify rlNotify;
	bool blConfirm=false;

	CPtrArray olConfirmKonfIds;
	DisableAttentionButtonsUpdate();

	for(int i = omData.GetSize() - 1; i >= 0; i--)
	{
		prlEntry = &omData[i];

		for(int j = prlEntry->Data.GetSize() - 1; j >= 0; j--)
		{
			if(prlEntry->Data[j].SetBy != 0 && prlEntry->Data[j].Confirmed != true)
			{
				// witch conflicts shall be confirmed
				switch (ipGroup)
				{
				case CONF_ALL: 
					blConfirm = true; 
					break;
				case CONF_BLT:
					if (IsBltKonfliktType(prlEntry->Data[j].KonfId.Type))
					{
						blConfirm = true;
					}
					break;
				}
 
				if (blConfirm)
				{
					// collect the conflicts to be confirmed
					blConfirm = false;
					olConfirmKonfIds.Add((void *)&prlEntry->Data[j].KonfId);
				}
			}
		}
	}
	// Confirm all collected conflicts!
	Confirm(olConfirmKonfIds, true);

	EnableAttentionButtonsUpdate();
	TRACE("Konflikte::ConfirmAll: End %s\n", CTime::GetCurrentTime().Format("%H:%M:%S"));
}



void Konflikte::AddKonflikt(const KonfIdent &ropKonfId, CString opText, DWORD dwOrigin, int ipSubOrig, CTime opTimeOfConf, int ipConfirmed)
{
	// Check setup configuration
	if (!IsConflictActivated(ropKonfId.Type))
		return;

	// Check if conflict is allready marked as confirmed in DB
	if (ipConfirmed != 1 &&
		(ogPosDiaFlightData.pomCflData->IsConfirmed("AFT", ropKonfId.FUrno, ropKonfId.FPart, ropKonfId.RelFUrno, ropKonfId.RelFPart, ropKonfId.Type) ||
		 ogRotationFlights.omCflData.IsConfirmed("AFT", ropKonfId.FUrno, ropKonfId.FPart, ropKonfId.RelFUrno, ropKonfId.RelFPart, ropKonfId.Type)))
		ipConfirmed = 1;

	if (bgReinstadedConflicts)
	{
		if (CompareConflictText(opText, CString("AFT"), ropKonfId.FPart, ropKonfId.RelFPart, ropKonfId.FUrno, ropKonfId.RelFUrno, dwOrigin, ropKonfId.Type))
		{
			//same conflict text
		}
		else
		{
			//not the same conflict tex, set to not confirmed
			// the rest hab be done in CompareConflictText()
			ipConfirmed = 0; 
		}
	}

	
	KONFENTRY *prlEntry = NULL;
	KONFDATA  *prlData;

	int ilDdxType = -1;

	if((prlData = GetKonflikt(ropKonfId)) != NULL)
	{
		prlData->Flag = true;
		
		prlData->SetBy = prlData->SetBy | dwOrigin;
		prlData->SubId = prlData->SubId | ipSubOrig;

		//CString olt = CTime::GetCurrentTime().Format("%d.%m.%y %H:%M");;
		//prlData->Text = olt + "   " + opText;

		prlData->Text = opText;
		
		if(ipConfirmed == 0)
			prlData->Confirmed = false;

		if(ipConfirmed == 1)
			prlData->Confirmed = true;

		
		if(prlData->Confirmed == false)
		{
			void *prlDummy;
			if (!omNotConfirmedMap.Lookup((void *)prlData, (void *&) prlDummy))
			{
				if (IsBltKonfliktType(prlData->KonfId.Type))
				//if (ogCfgData.CheckKonfliktForDisplay(prlData->KonfId.Type,CONF_BLT) == true)
					imBltConflicts++;
			}
			omNotConfirmedMap.SetAt((void*) prlData, NULL);
		}
		else
		{
			omNotConfirmedMap.RemoveKey((void*) prlData);

		}

		if(opTimeOfConf	== TIMENULL)
			prlData->TimeOfConflict = CTime::GetCurrentTime();
		else
			prlData->TimeOfConflict = opTimeOfConf;
		ogBasicData.LocalToUtc(prlData->TimeOfConflict);
/*
		CString olTmp;
		olTmp = prlData->TimeOfConflict.Format(" %d.%m.%y %H:%M:%S");
		prlData->Text += olTmp;
*/

		ilDdxType = KONF_CHANGE;

	}
	else
	{

		if (omUrnoMap.Lookup((void *)ropKonfId.FUrno, (void *& )prlEntry) == TRUE)
		{
			prlData = new KONFDATA;

			prlData->KonfId = ropKonfId;
			prlData->Text = opText;
			prlData->Confirmed = false;
			prlData->Flag = true;
			prlData->SetBy = dwOrigin;
			prlData->SubId = ipSubOrig;
			prlData->Confirmed = false;
	
			if(ipConfirmed == 1)
				prlData->Confirmed = true;

			
			if(opTimeOfConf	== TIMENULL)
				prlData->TimeOfConflict = CTime::GetCurrentTime();
			else
				prlData->TimeOfConflict = opTimeOfConf;

			ogBasicData.LocalToUtc(prlData->TimeOfConflict);
/*
			//test
			CString olTmp;
			olTmp = prlData->TimeOfConflict.Format(" %d.%m.%y %H:%M:%S");
			prlData->Text += olTmp;
*/

			prlEntry->Data.Add(prlData);
			if(prlData->Confirmed == false)
			{
				omNotConfirmedMap.SetAt((void*) prlData, NULL);
				if (IsBltKonfliktType(prlData->KonfId.Type))
				//if (ogCfgData.CheckKonfliktForDisplay(prlData->KonfId.Type,CONF_BLT) == true)
				{
					imBltConflicts++;
				}
			}
			ilDdxType = KONF_INSERT;
		}
		else
		{
			prlEntry = new KONFENTRY;
			omData.Add(prlEntry);
			prlEntry->Urno = ropKonfId.FUrno;
			prlData = new KONFDATA;

			omUrnoMap.SetAt((void *)prlEntry->Urno,(void *& )prlEntry);
			prlData->KonfId = ropKonfId;
			prlData->Text = opText;
			prlData->Confirmed = false;
			prlData->Flag = true;
			prlData->SetBy = dwOrigin;
			prlData->SubId = ipSubOrig;

			prlData->UpdatePriority( ropKonfId.Type );		// 050303 MVy: deal with conflict priority color, this is called on a new generated conflict
	
			if(ipConfirmed == 1)
				prlData->Confirmed = true;

			if(opTimeOfConf	== TIMENULL)
				prlData->TimeOfConflict = CTime::GetCurrentTime();
			else
				prlData->TimeOfConflict = opTimeOfConf;

			ogBasicData.LocalToUtc(prlData->TimeOfConflict);
/*
			//test
			CString olTmp;
			olTmp = prlData->TimeOfConflict.Format(" %d.%m.%y %H:%M:%S");
			prlData->Text += olTmp;
*/

			prlEntry->Data.Add(prlData);
			if(prlData->Confirmed == false)
			{
				omNotConfirmedMap.SetAt((void*) prlData, NULL);
				if (IsBltKonfliktType(prlData->KonfId.Type))
				{
					imBltConflicts++;
				}
			}
			ilDdxType = KONF_INSERT;
		}
	}


	if(ilDdxType != -1)
	{
		KonfIdent rlIdNotify;
		rlIdNotify = ropKonfId;
		ogDdx.DataChanged((void *)this, ilDdxType,(void *)&rlIdNotify );
	}

}



bool Konflikte::IsConflictActivated(int ipKonfType) const
{
	// examine all conflicts in the auto allocation process
	if (bgPosDiaAutoAllocateInWork)
		return true;

	switch (ipKonfType)
	{
	case IDS_STRING2001: // Position - Gate Relation
		{
			if (bmPosGateRelation)
				return true;
			else
				return false;
		}
		break;
	}

	return true;
}




bool Konflikte::IsBltKonfliktType(int ipKonfType) const
{
	//return (ipKonfType==IDS_STRING1735 || ipKonfType==IDS_STRING1736);
	return (ogCfgData.CheckKonfliktForDisplay(ipKonfType,CONF_BLT));
}


bool Konflikte::IsNeighborKonfliktType(int ipKonfType) const
{
	return (ipKonfType==IDS_STRING1723 || ipKonfType==IDS_STRING2030);
}


void Konflikte::CreateCflConflict(ROTATIONFLIGHTDATA *prpFlight, bool bpDep /*  = true */)
{
	if (IsArrival(prpFlight->Org3, prpFlight->Des3))
		CheckFlightCfl(prpFlight, 'A', bpDep);
	if (IsDeparture(prpFlight->Org3, prpFlight->Des3))
		CheckFlightCfl(prpFlight, 'D', bpDep);
}

void Konflikte::CreateCflConflict(DIAFLIGHTDATA *prpFlight, bool bpDep /*  = true */)
{
	if (IsArrival(prpFlight->Org3, prpFlight->Des3))
		CheckFlightCfl(prpFlight, 'A', bpDep);
	if (IsDeparture(prpFlight->Org3, prpFlight->Des3))
		CheckFlightCfl(prpFlight, 'D', bpDep);

}

void Konflikte::CheckFlightCfl(ROTATIONFLIGHTDATA *prpFlight, char cpFPart, bool bpDep /*  = true */)
{
	if (!bgConflictsForAirb)
	{
		if (prpFlight && prpFlight->Adid[0] == 'D' && prpFlight->Airb != TIMENULL)
			return;
	}

//	ResetFlight(prpFlight->Urno, cpFPart, MOD_ID_ROTATION);

	if(CString("O;T;G;B;Z;S").Find(prpFlight->Ftyp) < 0)
		return;

	CString olText;
	CString  olBez;
	if(cpFPart == 'A')
	{
		olBez.Format("%-9s (A): ", prpFlight->Flno);
		if (bmFlightDataChanged)
		{
			// check conflicts generated from flighthdl
			CString olExtConfBez;
			olExtConfBez.Format("%sSTA: %s", olBez, prpFlight->Stoa.Format("%H:%M %d.%m.%Y"));				
			CheckFlightExtConf(prpFlight->Urno, cpFPart, olExtConfBez, &ogRotationFlights.omCflData, MOD_ID_ROTATION);
		}

	}
	else
	{
		olBez.Format("%-9s (D): ", prpFlight->Flno);
		if (bmFlightDataChanged)
		{
			// check conflicts generated from flighthdl
			CString olExtConfBez;
			olExtConfBez.Format("%sSTD: %s", olBez, prpFlight->Stod.Format("%H:%M %d.%m.%Y"));				
			CheckFlightExtConf(prpFlight->Urno, cpFPart, olExtConfBez, &ogRotationFlights.omCflData, MOD_ID_ROTATION);
		}
	}
}

void Konflikte::CheckFlightCfl(DIAFLIGHTDATA *prpFlight, char cpFPart, bool bpDep /*  = true */)
{
	if (!bgConflictsForAirb)
	{
		if (prpFlight && prpFlight->Adid[0] == 'D' && prpFlight->Airb != TIMENULL)
			return;
	}

//	ResetFlight(prpFlight->Urno, cpFPart, MOD_ID_ROTATION);

	if(CString("O;T;G;B;Z;S").Find(prpFlight->Ftyp) < 0)
		return;

	CString olText;
	CString  olBez;
	if(cpFPart == 'A')
	{
		olBez.Format("%-9s (A): ", prpFlight->Flno);
		if (bmFlightDataChanged)
		{
			// check conflicts generated from flighthdl
			CString olExtConfBez;
			olExtConfBez.Format("%sSTA: %s", olBez, prpFlight->Stoa.Format("%H:%M %d.%m.%Y"));				
			CheckFlightExtConf(prpFlight->Urno, cpFPart, olExtConfBez, ogPosDiaFlightData.pomCflData, MOD_ID_ROTATION);
		}

	}
	else
	{
		olBez.Format("%-9s (D): ", prpFlight->Flno);
		if (bmFlightDataChanged)
		{
			// check conflicts generated from flighthdl
			CString olExtConfBez;
			olExtConfBez.Format("%sSTD: %s", olBez, prpFlight->Stod.Format("%H:%M %d.%m.%Y"));				
			CheckFlightExtConf(prpFlight->Urno, cpFPart, olExtConfBez, ogPosDiaFlightData.pomCflData, MOD_ID_ROTATION);
		}
	}
}


void Konflikte::CheckFlight(ROTATIONFLIGHTDATA *prpFlight, bool bpDep /*  = true */)
{
	if (IsArrival(prpFlight->Org3, prpFlight->Des3))
		CheckFlight(prpFlight, 'A', bpDep);
	if (IsDeparture(prpFlight->Org3, prpFlight->Des3))
		CheckFlight(prpFlight, 'D', bpDep);
}



void Konflikte::CheckFlight(ROTATIONFLIGHTDATA *prpFlight, char cpFPart, bool bpDep /*  = true */)
{
	CTime ol_local;
	if (!bgConflictsForAirb)
	{
		if (prpFlight && prpFlight->Adid[0] == 'D' && prpFlight->Airb != TIMENULL)
			return;
	}

	ResetFlight(prpFlight->Urno, cpFPart, MOD_ID_ROTATION);

	if(CString("O;T;G;B;Z;S").Find(prpFlight->Ftyp) < 0)
		return;

	CString olText;
	CString  olBez;
	if(cpFPart == 'A')
	{
		olBez.Format("%-9s (A): ", prpFlight->Flno);
		if (bmFlightDataChanged)
		{
			// check conflicts generated from flighthdl
			CString olExtConfBez;
			ol_local = prpFlight->Stoa;
			if(bgConflictUtctoLocal)
				ogBasicData.UtcToLocal(ol_local);
			olExtConfBez.Format("%sSTA: %s", olBez, ol_local.Format("%H:%M %d.%m.%Y"));				
			CheckFlightExtConf(prpFlight->Urno, cpFPart, olExtConfBez, &ogRotationFlights.omCflData, MOD_ID_ROTATION);
		}

	}
	else
	{
		olBez.Format("%-9s (D): ", prpFlight->Flno);
		if (bmFlightDataChanged)
		{
			// check conflicts generated from flighthdl
			CString olExtConfBez;
			ol_local = prpFlight->Stod;
			if(bgConflictUtctoLocal)
				ogBasicData.UtcToLocal(ol_local);
			olExtConfBez.Format("%sSTD: %s", olBez, ol_local.Format("%H:%M %d.%m.%Y"));				
			CheckFlightExtConf(prpFlight->Urno, cpFPart, olExtConfBez, &ogRotationFlights.omCflData, MOD_ID_ROTATION);
		}
	}

	//if(olBez.IsEmpty())
	//	return;

	CString oTmp;
	CString olPst;

	
	ROTATIONFLIGHTDATA *prlFlightA;
	ROTATIONFLIGHTDATA *prlFlightD;

	CTime olCurr = CTime::GetCurrentTime();

	ogBasicData.LocalToUtc(olCurr);
	char buffer[256];

	CTimeSpan ol30(0,0,30,0);
	CTimeSpan ol15(0,0,15,0);
	CTimeSpan ol5(0,0,5,0);


	/* Conflict No. 1		Start-landing order!  */
	/* Conflict No. 2	15	No Onblock at Touch down + %d  Minutes */
	/* Conflict No. 3	5	Actual Time > ETA + %d Minutes  */
	/* Conflict No. 4	-5	Actual Time  + %d Min > Next Information Time  */
	/* Conflict No. 5		Pos: %s  A/C: %s  error (%s) from parking system !  */
	/* Conflict No. 6		STA/ETA    >=    STD/ETD  */
	/* Conflict No. 7		STD/ETD + 30 Min. < actual time and no DEP telegram received  */
	/* Conflict No. 8		Aircrafttype has been changed */
	/* Conflict No. 9	-5	No Take off time at Offblock + %d Minutes */
	/* Conflict No. 10	5	No Offblock at STD + %d Minutes */
	/* Conflict No. 11	15	ETD > STD + %d Minutes  */
	/* Conflict No. 12	-5	Actual Time + %d Min > Next Information Time  */
	/* Conflict No. 13	30	STD/ETD - 30 Min. < actual time and no boarding information received */
	/* Conflict No. 14	15	ETA > STA + %d Minutes  */


	/*
	omNoOnblAfterLand	= CTimeSpan(0,0,15,0);
	omCurrEtai			= CTimeSpan(0,0,5,0);
	omCurrDNxti = CTimeSpan(0,0,5,0);
	omCurrANxti = CTimeSpan(0,0,5,0);
	omStoaStod = CTimeSpan(0,0,0,0);
	omStodCurrAndNoAirb = CTimeSpan(0,0,5,0);
	omCurrOfbl = CTimeSpan(0,0,15,0);
	omCurrOfblStod = CTimeSpan(0,0,5,0);
	omStodEtdi = CTimeSpan(0,0,15,0);
	omCurrStodGd1x = CTimeSpan(0,0,30,0);
	*/




	/////////////////////////////////////////////
//T	/////////// NEW FLIGHT
	if (bgConflictNewFlight)
	{
		CTime olSto;
		CTime olStart= CTime(olCurr.GetYear(), olCurr.GetMonth(), olCurr.GetDay(),0,0,0);
		CTime olEnd= CTime(olCurr.GetYear(), olCurr.GetMonth(), olCurr.GetDay(),23,59,59);

		if(IsBetween(prpFlight->Cdat , olStart, olEnd))
		{
			if(cpFPart == 'A')
			{
				ol_local = prpFlight->Stoa;
				if(bgConflictUtctoLocal)
					ogBasicData.UtcToLocal(ol_local);
				olText.Format("%sSTA: %s   %s (%s %s)", olBez, ol_local.Format("%H:%M %d.%m.%Y"), GetString(IDS_STRING1805), prpFlight->Usec, prpFlight->Cdat.Format("%H:%M"));
			}
			else			
			{
				ol_local = prpFlight->Stod;
				if(bgConflictUtctoLocal)
					ogBasicData.UtcToLocal(ol_local);
				olText.Format("%sSTD: %s   %s (%s %s)", olBez, ol_local.Format("%H:%M %d.%m.%Y"), GetString(IDS_STRING1805), prpFlight->Usec, prpFlight->Cdat.Format("%H:%M"));
			}
			
			AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0 , ' ', IDS_STRING1805), olText, MOD_ID_ROTATION);
		}
	}
	////////////////////////////

	if(cpFPart == 'A')
	{
		ol_local = prpFlight->Stoa;
		if(bgConflictUtctoLocal)
			ogBasicData.UtcToLocal(ol_local);
		olText.Format("%sSTA: %s     ", olBez, ol_local.Format("%H:%M %d.%m.%Y"));

//T		/* Conflict No. 5   Pos: %s  A/C: %s  error (%s) from parking system !  */
		if(bmAderConf)
		{
			if(strlen(prpFlight->Ader) > 0)
			{

				sprintf(buffer,GetString(IDS_STRING1428), prpFlight->Psta, prpFlight->Act3, prpFlight->Ader);
				//Andocksystem;
				AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1428), olBez + CString(buffer), MOD_ID_ROTATION);
			}
		}
	}

	if(cpFPart == 'D')
	{
		////////////////////////////////////////////////////////////////////////////////////////
//T		/* Conflict No. 9    No Offblock at STD + %d Minutes */
		
		if(bmCurrOfblStod)
		{
			if( prpFlight->Ftyp[0] != 'X' && prpFlight->Ftyp[0] != 'N')
			{
				if((prpFlight->Ofbl == TIMENULL) && (olCurr > prpFlight->Stod + omCurrOfblStod))
				{
					oTmp.Format(GetString(IDS_STRING1240), omCurrOfblStod.GetTotalMinutes());
					ol_local = prpFlight->Stod;
					if(bgConflictUtctoLocal)
						ogBasicData.UtcToLocal(ol_local);	
					olText = olBez + CString("STD: ") + ol_local.Format("%H:%M %d.%m.%Y     ") + oTmp;
					//CString("Kein Offblock bei STD + %d Minuten");
					AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1240), olText, MOD_ID_ROTATION);
				}
			}
		}

		////////////////////////////////////////////////////////////////////////////////////////
//T		/* Conflict No. 5    Pos: %s  A/C: %s  error (%s) from parking system ! */

		if(bmAderConf)
		{
			if(strlen(prpFlight->Ader) > 0)
			{
				sprintf(buffer,GetString(IDS_STRING1428), prpFlight->Pstd, prpFlight->Act3, prpFlight->Ader);
				ol_local = prpFlight->Stod;
				if(bgConflictUtctoLocal)
					ogBasicData.UtcToLocal(ol_local);	
				olText = olBez + CString("STD: ") + ol_local.Format("%H:%M %d.%m.%Y     ") + CString(buffer); //Andocksystem;
				AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1428), olText, MOD_ID_ROTATION);
			}
		}
	}


	if(CString(";T;G").Find(prpFlight->Ftyp) > 0)
	{
		CheckFlightConf(prpFlight->Urno, cpFPart, MOD_ID_ROTATION);
		//follwing conflicts are not relevant for T,G
		return;
	}


	if(cpFPart == 'A')
	{
		ol_local = prpFlight->Stoa;
		if(bgConflictUtctoLocal)
			ogBasicData.UtcToLocal(ol_local);	
		olText.Format("%sSTA: %s     ", olBez, ol_local.Format("%H:%M %d.%m.%Y"));
		
		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 14  |ETA - STA| > %d Minutes  */
		
		if(bmStoaEtai)
		{
			CTimeSpan olEtaStaDiff = prpFlight->Etai - prpFlight->Stoa;
			if((prpFlight->Etai != TIMENULL) && (labs(olEtaStaDiff.GetTotalMinutes()) > omStoaEtai.GetTotalMinutes() )  && (prpFlight->Airb == TIMENULL))
			{
				oTmp.Format(GetString(IDS_STRING1860), omStoaEtai.GetTotalMinutes());
 				AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1860), olText + oTmp, MOD_ID_ROTATION);
			}
		}






		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 14  Return flight */

		if( CString(prpFlight->Stat).GetLength() >= 10)
		{
			if( prpFlight->Stat[8] == '1')
			{
				AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1697), olBez + GetString(IDS_STRING1697), MOD_ID_ROTATION);

			}
		}
		
		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 1  start-landing order!  */
		
		if(bmJoinConf)
		{
			if(strcmp(prpFlight->Rtyp, "E") == 0)
			{
				ol_local = prpFlight->Stoa;
				if(bgConflictUtctoLocal)
					ogBasicData.UtcToLocal(ol_local);	
				olText.Format("%sSTA: %s  <%s>   ", olBez, ol_local.Format("%H:%M %d.%m.%Y"), prpFlight->Regn);
	
				olText += GetString(IDS_STRING1430);
			
				AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1430), olText, MOD_ID_ROTATION);
			}
		}
		


		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 2   No Onblock at Touch down + %d  Minutes */

		if(bmNoOnblAfterLand)
		{
			if((prpFlight->Land != TIMENULL) && (prpFlight->Onbl == TIMENULL) && (olCurr > prpFlight->Land + omNoOnblAfterLand))
			{

				oTmp.Format(GetString(IDS_STRING1236), omNoOnblAfterLand.GetTotalMinutes() );
				//CString("Kein Onblock bei Landung + %d Minuten");
				AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1236), olBez + oTmp, MOD_ID_ROTATION);
			}
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 3   Actual Time > ETA + %d Minutes  */

		if(bmCurrEtai)
		{
			if (strcmp(pcgHome, "PVG") == 0)
			{
				// Shanghai need other format and ETOA!
				if((prpFlight->Etoa != TIMENULL) && (olCurr > prpFlight->Etoa + omCurrEtai /*ol5*/) && (prpFlight->Land == TIMENULL))
				{
					CString olTmp2;
					olTmp2.Format(GetString(IDS_STRING2024), omCurrEtai.GetTotalMinutes(), prpFlight->Lstu.Format("%H:%M"));
					ol_local = prpFlight->Stoa;
					if(bgConflictUtctoLocal)
						ogBasicData.UtcToLocal(ol_local);	
					oTmp.Format("%s %s %s %s %s", GetString(IDS_STRING323), ol_local.Format("%H:%M"), GetString(IDS_STRING2028), prpFlight->Etoa.Format("%H:%M"), olTmp2);
					AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1237), olBez + oTmp, MOD_ID_ROTATION);
				}
			}
			else
			{
				if((prpFlight->Etai != TIMENULL) && (olCurr > prpFlight->Etai + omCurrEtai /*ol5*/) && (prpFlight->Land == TIMENULL))
				{
					oTmp.Format(GetString(IDS_STRING1237), omCurrEtai.GetTotalMinutes());
					AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1237), olBez + oTmp, MOD_ID_ROTATION);
				}
			}
		}
		

		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 4   Actual Time  + %d Min > Next Information Time  */
		
		if(bmCurrANxti)
		{
			if((prpFlight->Nxti != TIMENULL) && (olCurr + omCurrANxti > prpFlight->Nxti) && (prpFlight->Land == TIMENULL))
			{

				oTmp.Format(GetString(IDS_STRING1238), omCurrANxti.GetTotalMinutes());//CString("Aktuelle Zeit + %d Min > Next Information - Zeit ");
				AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1238), olBez + oTmp, MOD_ID_ROTATION);
			}
		}

		////////////////////////////////////////////////////////////////////////////////////////
//T		/* Conflict No. 5   Pos: %s  A/C: %s  error (%s) from parking system !  */
/*
		if(bmAderConf)
		{
			if(strlen(prpFlight->Ader) > 0)
			{

				sprintf(buffer,GetString(IDS_STRING1428), prpFlight->Psta, prpFlight->Act3, prpFlight->Ader);
				//Andocksystem;
				AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1428), olBez + CString(buffer), MOD_ID_ROTATION);
			}
		}
*/
		prlFlightD = ogRotationFlights.GetDeparture(prpFlight);

		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 6   STA/ETA    >=    STD/ETD  */
		if (prlFlightD)
		{
			CTime olTimeA = prpFlight->Etai;
			if (olTimeA == TIMENULL)
				olTimeA = prpFlight->Stoa;

			if (olTimeA != TIMENULL)
			{
				CTime olTimeD = prlFlightD->Etdi;
				if (olTimeD == TIMENULL)
					olTimeD = prlFlightD->Stod;

				if (olTimeD != TIMENULL)
				{
					if (olTimeA >= olTimeD)
					{
						CString a = olTimeA.Format( "%d.%m.%Y %H:%M:%S" );
						CString b = olTimeD.Format( "%d.%m.%Y %H:%M:%S" );
						olText = olBez + GetString(IDS_STRING1432);
						AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1432), olText, MOD_ID_ROTATION);
//						AddKonflikt(KonfIdent(prpFlight->Urno, 'D', 0, ' ', IDS_STRING1432), olText, MOD_ID_ROTATION);
					}
				}
			}
		}
//aiaaia

		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 7   STD/ETD + %d Min. < actual time and no DEP telegram received  */

		if(bmStodCurrAndNoAirb)
		{
			if (strcmp(pcgHome, "PVG") == 0)
			{
				// Shanghai need other format!
				if(prpFlight->Airb == TIMENULL && prpFlight->Stod != TIMENULL && (prpFlight->Stod + omStodCurrAndNoAirb < olCurr) &&  prpFlight->Aird == TIMENULL)
				{

					// STD+%d minutes still have not received DEP telex (%s)
					oTmp.Format(GetString(IDS_STRING2026), omStodCurrAndNoAirb.GetTotalMinutes(), prpFlight->Lstu.Format("%H:%M"));
					olText = olBez + oTmp;
										
					AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1549), olText, MOD_ID_ROTATION);
				}
			}
			else
			{
				if( ( prpFlight->Stod != TIMENULL &&  prpFlight->Airb == TIMENULL && (prpFlight->Stod + omStodCurrAndNoAirb < olCurr) &&  prpFlight->Aird == TIMENULL && prpFlight->Etdi == TIMENULL ) ||
					( prpFlight->Airb == TIMENULL && (prpFlight->Etdi + omStodCurrAndNoAirb < olCurr) &&  prpFlight->Aird == TIMENULL && prpFlight->Etdi != TIMENULL ) ) 
//				if( ( prpFlight->Airb == TIMENULL && (prpFlight->Stod + omStodCurrAndNoAirb < olCurr) &&  prpFlight->Aird == TIMENULL && prpFlight->Etdi == TIMENULL ) ||
//					( (prpFlight->Etdi + omStodCurrAndNoAirb  < olCurr) &&  prpFlight->Aird == TIMENULL && prpFlight->Etdi != TIMENULL ) ) 
				{

					oTmp.Format(GetString(IDS_STRING1549), omStodCurrAndNoAirb.GetTotalMinutes());
					olText = olBez + oTmp;
					
					//CString("STD/ETD + 30 Min. > actual time and No DEP telegram received");
					
					AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1549), olText,  MOD_ID_ROTATION);
				}
			}
		}

		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 8   Aircrafttype has been changed */

		/****************
		
		if( bmActRegnChangeConf)
		{
			// IDS_STRING1551  Registrierung hat sich ge�ndert
			if( CString(prpFlight->Regn) != CString(prpFlight->LastRegn)) 
			{
				olText = olBez + GetString(IDS_STRING1550);
				AddKonflikt(prpFlight->Urno, olText, IDS_STRING1550, MOD_ID_ROTATION);
			}
			else
			{
				// IDS_STRING1550  Flugzeugtyp hat sich ge�ndert
				if( CString(prpFlight->Act5) != CString(prpFlight->LastAct5)) 
				{
					olText = olBez + GetString(IDS_STRING1550);
					AddKonflikt(prpFlight->Urno, olText, IDS_STRING1550, MOD_ID_ROTATION);
				}
			}
		
		}

		*****************/
		olPst = prpFlight->Psta;

	}

	if(cpFPart == 'D')
	{
		prlFlightA = ogRotationFlights.GetArrival(prpFlight);


		if(prlFlightA != NULL)
		{
			if(bpDep) CheckFlight(prlFlightA, 'A', false);
		}


		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 1  start-landing order!  */

		if(bmJoinConf)
		{
			if(strcmp(prpFlight->Rtyp, "E") == 0)
			{
				ol_local = prpFlight->Stoa;
				if(bgConflictUtctoLocal)
					ogBasicData.UtcToLocal(ol_local);	
				olText = olBez + CString("STA: ") + ol_local.Format("%H:%M %d.%m.%Y  <") + CString(prpFlight->Regn) + CString(">   ");

				olText += GetString(IDS_STRING1430);//CString("Kein Onblock bei Landung + 15 Minuten");
				AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1430), olText, MOD_ID_ROTATION);
			}
		}
		
		


		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 8   No Take off time at Offblock + %d Minutes */

		if(bmCurrOfbl)
		{
			if((prpFlight->Airb == TIMENULL) && (prpFlight->Ofbl != TIMENULL) && (olCurr > prpFlight->Ofbl + omCurrOfbl/*ol15*/))
			{
				oTmp.Format(GetString(IDS_STRING1239), omCurrOfbl.GetTotalMinutes());
				ol_local = prpFlight->Stod;
				if(bgConflictUtctoLocal)
					ogBasicData.UtcToLocal(ol_local);	
				olText = olBez + CString("STD: ") + ol_local.Format("%H:%M %d.%m.%Y     ") + oTmp;
				//CString("Keine Startzeit bei Offblock + %d Minuten");
				AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1239), olText, MOD_ID_ROTATION);
			}
		}


		////////////////////////////////////////////////////////////////////////////////////////
//T		/* Conflict No. 9    No Offblock at STD + %d Minutes */
/*		
		if(bmCurrOfblStod)
		{
			if( prpFlight->Ftyp[0] != 'X' && prpFlight->Ftyp[0] != 'N')
			{
				if((prpFlight->Ofbl == TIMENULL) && (olCurr > prpFlight->Stod + omCurrOfblStod))
				{
					oTmp.Format(GetString(IDS_STRING1240), omCurrOfblStod.GetTotalMinutes());
					olText = olBez + CString("STD: ") + prpFlight->Stod.Format("%H:%M %d.%m.%Y     ") + oTmp;
					//CString("Kein Offblock bei STD + %d Minuten");
					AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1240), olText, MOD_ID_ROTATION);
				}
			}
		}
*/

		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 10  |ETD - STD| > %d Minutes  */
		
		if(bmStodEtdi)
		{
			CTimeSpan olEtdStdDiff = prpFlight->Etdi - prpFlight->Stod;
			if((prpFlight->Etdi != TIMENULL) && (labs(olEtdStdDiff.GetTotalMinutes()) > omStodEtdi.GetTotalMinutes() )  && (prpFlight->Airb == TIMENULL))
			{
				oTmp.Format(GetString(IDS_STRING1241), omStodEtdi.GetTotalMinutes());
				ol_local = prpFlight->Stod;
				if(bgConflictUtctoLocal)
					ogBasicData.UtcToLocal(ol_local);	
				olText = olBez + CString("STD: ") + ol_local.Format("%H:%M %d.%m.%Y     ") + oTmp;
				//CString("ETD ist gr��er STD  + 15 Minuten");
				AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1241), olText, MOD_ID_ROTATION);
			}
		}

		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 11   Actual Time + %d Min > Next Information Time  */

		if(bmCurrDNxti)
		{
			if((prpFlight->Nxti != TIMENULL) && (olCurr + omCurrDNxti > prpFlight->Nxti) && (prpFlight->Airb == TIMENULL))
			{

				oTmp.Format(GetString(IDS_STRING1242), omCurrDNxti.GetTotalMinutes());
				ol_local = prpFlight->Stod;
				if(bgConflictUtctoLocal)
					ogBasicData.UtcToLocal(ol_local);	
				olText = olBez + CString("STD: ") + ol_local.Format("%H:%M %d.%m.%Y     ") + oTmp;
				
				//CString("Aktuelle Zeit > Next Information - Zeit ");
				AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1242), olText, MOD_ID_ROTATION);
			}
		}

		////////////////////////////////////////////////////////////////////////////////////////
//T		/* Conflict No. 5    Pos: %s  A/C: %s  error (%s) from parking system ! */
/*
		if(bmAderConf)
		{
			if(strlen(prpFlight->Ader) > 0)
			{
				sprintf(buffer,GetString(IDS_STRING1428), prpFlight->Pstd, prpFlight->Act3, prpFlight->Ader);

				olText = olBez + CString("STD: ") + prpFlight->Stod.Format("%H:%M %d.%m.%Y     ") + CString(buffer); //Andocksystem;
				AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1428), olText, MOD_ID_ROTATION);
			}
		}
*/		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 14    CTOT > STD/ETD + %d Minutes */
		
		if(bmCurrOfblStod)
		{
			if( prpFlight->Ftyp[0] != 'X' && prpFlight->Ftyp[0] != 'N')
			{
				if(prpFlight->Ofbl == TIMENULL)
				{
					// check only if no OFBL
					CTime olTmp;
					CString olTimeString;

					if (prpFlight->Etod != TIMENULL)
					{
						olTmp = prpFlight->Etod;
						olTimeString = "ETD: ";
					}
					else
					{
						olTmp = prpFlight->Stod;
						olTimeString = "STD: ";
					}

					if(bgConflictUtctoLocal)
						ogBasicData.UtcToLocal(olTmp);	
										
					if(prpFlight->Slot > (olTmp  + omCtotEtod))
					{
						oTmp.Format(GetString(IDS_STRING2353), omCtotEtod.GetTotalMinutes());
						olText = olBez + olTimeString + olTmp.Format("%H:%M %d.%m.%Y     ") + oTmp;
						AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING2353), olText, MOD_ID_ROTATION);
					}
				}
			}
		}

		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 15    No Offblock at CTOT + %d Minutes */
		
		if(omOfblCtot.GetTotalMinutes() != 0)
		{
			if( prpFlight->Ftyp[0] != 'X' && prpFlight->Ftyp[0] != 'N')
			{
				if((prpFlight->Slot != TIMENULL) && (prpFlight->Ofbl == TIMENULL) && (olCurr > prpFlight->Slot + omOfblCtot))
				{
					oTmp.Format(GetString(IDS_STRING2354), omOfblCtot.GetTotalMinutes());
					olText = olBez + CString("CTOT: ") + prpFlight->Slot.Format("%H:%M %d.%m.%Y     ") + oTmp;
					//CString("Kein Offblock bei STD + %d Minuten");
					AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING2354), olText, MOD_ID_ROTATION);
				}
			}
		}
		olPst = prpFlight->Pstd;



	/* Conflict No. 6  STA/ETA    >=    STD/ETD  */
		if (prlFlightA)
		{
			CTime olTimeA = prlFlightA->Etai;
			if (olTimeA == TIMENULL)
				olTimeA = prlFlightA->Stoa;

			if (olTimeA != TIMENULL)
			{
				CTime olTimeD = prpFlight->Etdi;
				if (olTimeD == TIMENULL)
					olTimeD = prpFlight->Stod;

				if (olTimeD != TIMENULL)
				{
					if (olTimeA >= olTimeD)
					{
						olText = olBez + GetString(IDS_STRING1432);
						AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1432), olText, MOD_ID_ROTATION);
//						AddKonflikt(KonfIdent(prpFlight->Urno, 'D', 0, ' ', IDS_STRING1432), olText, MOD_ID_ROTATION);
					}
				}
			}
		}

	}  // if(cpFPart == 'D') ...


	////////////////////////////////////////////////////////////////////////////////////////

	if (strcmp(pcgHome, "PVG") != 0)
	{
		if(strcmp(prpFlight->Ftyp, "Z") == 0)
		{
			ol_local = prpFlight->Stod;
			if(bgConflictUtctoLocal)
				ogBasicData.UtcToLocal(ol_local);	
				
			olText.Format("%sSTD: %s%s%s", olBez, ol_local.Format("%H:%M %d.%m.%Y     "), GetString(IDS_STRING1776), CString(prpFlight->Stat).Mid(1,3));
			AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1776), olText, MOD_ID_ROTATION);
		}


		if(strcmp(prpFlight->Ftyp, "B") == 0)
		{
			ol_local = prpFlight->Stod;
			if(bgConflictUtctoLocal)
				ogBasicData.UtcToLocal(ol_local);	
			olText.Format("%sSTD: %s%s%s", olBez, ol_local.Format("%H:%M %d.%m.%Y     "), GetString(IDS_STRING1777), CString(prpFlight->Stat).Mid(1,3));
			AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1777), olText, MOD_ID_ROTATION);
		}
	}


	////////////////////////////////////////////////////////////////////////////////////////
	/* Conflict No. 1  start-landing order!  */

	if(strcmp(prpFlight->Adid, "B") == 0)
	{
		if(bmJoinConf)
		{
			if(strcmp(prpFlight->Rtyp, "E") == 0)
			{
				CTime ol_localtemp;
				ol_local = prpFlight->Stoa;
				ol_localtemp = prpFlight->Stod;
				if(bgConflictUtctoLocal)
				{
					ogBasicData.UtcToLocal(ol_local);	
					ogBasicData.UtcToLocal(ol_localtemp);	
				}
				olText.Format("%sSTA/STD: %s%s%s%s", olBez, ol_local.Format("%H:%M %d.%m.%Y / "), ol_localtemp.Format("%H:%M %d.%m.%Y  <"), CString(prpFlight->Regn), CString(">   "));

				olText += GetString(IDS_STRING1430);//CString("start-landing order!");
				AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1430), olText, MOD_ID_ROTATION);
			}
		}
	}

	CheckCommonConflicts(prpFlight, NULL, MOD_ID_ROTATION, SUB_MOD_NOTIMP, olCurr, olBez);

	CheckFlightConf(prpFlight->Urno, cpFPart, MOD_ID_ROTATION);
//	CheckAttentionButton();
}

bool Konflikte::CheckFlightWithCurrentTime(DIAFLIGHTDATA *prpFlight, char cpFPart)
{
	//not unsed
	ASSERT(FALSE);
	bool blRet = false;
	if(CString("O;T;G;B;Z;S").Find(prpFlight->Ftyp) < 0)
		return blRet;

	CString olText;
	CString  olBez;
	CString oTmp;

	if(cpFPart == 'A')
		olBez.Format("%-9s (A): ", prpFlight->Flno);
	else
		olBez.Format("%-9s (D): ", prpFlight->Flno);

	CTime olCurr = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olCurr);

	/* Conflict No. 1		Start-landing order!  */
	/* Conflict No. 2	15	No Onblock at Touch down + %d  Minutes */
	/* Conflict No. 3	5	Actual Time > ETA + %d Minutes  */
	/* Conflict No. 4	-5	Actual Time  + %d Min > Next Information Time  */
	/* Conflict No. 5		Pos: %s  A/C: %s  error (%s) from parking system !  */
	/* Conflict No. 6		STA/ETA    >=    STD/ETD  */
	/* Conflict No. 7		STD/ETD + 30 Min. < actual time and no DEP telegram received  */
	/* Conflict No. 8		Aircrafttype has been changed */
	/* Conflict No. 9	-5	No Take off time at Offblock + %d Minutes */
	/* Conflict No. 10	5	No Offblock at STD + %d Minutes */
	/* Conflict No. 11	15	ETD > STD + %d Minutes  */
	/* Conflict No. 12	-5	Actual Time + %d Min > Next Information Time  */
	/* Conflict No. 13	30	STD/ETD - 30 Min. < actual time and no boarding information received */
	/* Conflict No. 14	15	ETA > STA + %d Minutes  */


	/*
	omNoOnblAfterLand	= CTimeSpan(0,0,15,0);
	omCurrEtai			= CTimeSpan(0,0,5,0);
	omCurrDNxti = CTimeSpan(0,0,5,0);
	omCurrANxti = CTimeSpan(0,0,5,0);
	omStoaStod = CTimeSpan(0,0,0,0);
	omStodCurrAndNoAirb = CTimeSpan(0,0,5,0);
	omCurrOfbl = CTimeSpan(0,0,15,0);
	omCurrOfblStod = CTimeSpan(0,0,5,0);
	omStodEtdi = CTimeSpan(0,0,15,0);
	omCurrStodGd1x = CTimeSpan(0,0,30,0);
	*/




	/////////////////////////////////////////////
	/////////// NEW FLIGHT


	if(cpFPart == 'A')
	{

		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 2   No Onblock at Touch down + %d  Minutes */

		if(bmNoOnblAfterLand)
		{
			if((prpFlight->Land != TIMENULL) && (prpFlight->Onbl == TIMENULL) && (olCurr > prpFlight->Land + omNoOnblAfterLand))
			{

				oTmp.Format(GetString(IDS_STRING1236), omNoOnblAfterLand.GetTotalMinutes() );
				//CString("Kein Onblock bei Landung + %d Minuten");
				AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1236), olBez + oTmp, MOD_ID_DIA);
				blRet = true;
			}
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 3   Actual Time > ETA + %d Minutes  */

		if(bmCurrEtai)
		{
			if (strcmp(pcgHome, "PVG") == 0)
			{
				// Shanghai need other format and ETOA!
				if((prpFlight->Etoa != TIMENULL) && (olCurr > prpFlight->Etoa + omCurrEtai /*ol5*/) && (prpFlight->Land == TIMENULL))
				{
					CString olTmp2;
					olTmp2.Format(GetString(IDS_STRING2024), omCurrEtai.GetTotalMinutes(), prpFlight->Lstu.Format("%H:%M"));
					oTmp.Format("%s %s %s %s %s", GetString(IDS_STRING323), prpFlight->Stoa.Format("%H:%M"), GetString(IDS_STRING2028), prpFlight->Etoa.Format("%H:%M"), olTmp2);
					AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1237), olBez + oTmp, MOD_ID_DIA);
					blRet = true;
				}
			}
			else
			{
				if((prpFlight->Etai != TIMENULL) && (olCurr > prpFlight->Etai + omCurrEtai /*ol5*/) && (prpFlight->Land == TIMENULL))
				{
					oTmp.Format(GetString(IDS_STRING1237), omCurrEtai.GetTotalMinutes());
					AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1237), olBez + oTmp, MOD_ID_DIA);
					blRet = true;
				}
			}
		}
		

		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 4   Actual Time  + %d Min > Next Information Time  */
		
		if(bmCurrANxti)
		{
			if((prpFlight->Nxti != TIMENULL) && (olCurr + omCurrANxti > prpFlight->Nxti) && (prpFlight->Land == TIMENULL))
			{

				oTmp.Format(GetString(IDS_STRING1238), omCurrANxti.GetTotalMinutes());//CString("Aktuelle Zeit + %d Min > Next Information - Zeit ");
				AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1238), olBez + oTmp, MOD_ID_DIA);
				blRet = true;
			}
		}

		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 7   STD/ETD + %d Min. < actual time and no DEP telegram received  */

		if(bmStodCurrAndNoAirb)
		{
			if (strcmp(pcgHome, "PVG") == 0)
			{
				// Shanghai need other format!
				if(prpFlight->Airb == TIMENULL && prpFlight->Stod != TIMENULL && (prpFlight->Stod + omStodCurrAndNoAirb < olCurr) &&  prpFlight->Aird == TIMENULL)
				{

					// STD+%d minutes still have not received DEP telex (%s)
					oTmp.Format(GetString(IDS_STRING2026), omStodCurrAndNoAirb.GetTotalMinutes(), prpFlight->Lstu.Format("%H:%M"));
					olText = olBez + oTmp;
										
					AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1549), olText, MOD_ID_DIA);
					blRet = true;
				}
			}
			else
			{
				if( ( prpFlight->Stod != TIMENULL && prpFlight->Airb == TIMENULL && (prpFlight->Stod + omStodCurrAndNoAirb < olCurr) &&  prpFlight->Aird == TIMENULL && prpFlight->Etdi == TIMENULL ) ||
					( prpFlight->Airb == TIMENULL && (prpFlight->Etdi + omStodCurrAndNoAirb < olCurr) &&  prpFlight->Aird == TIMENULL && prpFlight->Etdi != TIMENULL ) ) 
//				if( ( prpFlight->Airb == TIMENULL && (prpFlight->Stod + omStodCurrAndNoAirb < olCurr) &&  prpFlight->Aird == TIMENULL && prpFlight->Etdi == TIMENULL ) ||
//					( (prpFlight->Etdi + omStodCurrAndNoAirb  < olCurr) &&  prpFlight->Aird == TIMENULL && prpFlight->Etdi != TIMENULL ) ) 
				{

					oTmp.Format(GetString(IDS_STRING1549), omStodCurrAndNoAirb.GetTotalMinutes());
					olText = olBez + oTmp;
					
					//CString("STD/ETD + 30 Min. > actual time and No DEP telegram received");
					
					AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1549), olText,  MOD_ID_DIA);
					blRet = true;
				}
			}
		}
	}

	if(cpFPart == 'D')
	{

		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 8   No Take off time at Offblock + %d Minutes */

		if(bmCurrOfbl)
		{
			if((prpFlight->Airb == TIMENULL) && (prpFlight->Ofbl != TIMENULL) && (olCurr > prpFlight->Ofbl + omCurrOfbl/*ol15*/))
			{
				oTmp.Format(GetString(IDS_STRING1239), omCurrOfbl.GetTotalMinutes());
				olText = olBez + CString("STD: ") + prpFlight->Stod.Format("%H:%M %d.%m.%Y     ") + oTmp;
				//CString("Keine Startzeit bei Offblock + %d Minuten");
				AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1239), olText, MOD_ID_DIA);
				blRet = true;
			}
		}


		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 9    No Offblock at STD + %d Minutes */
		
		if(bmCurrOfblStod)
		{
			if( prpFlight->Ftyp[0] != 'X' && prpFlight->Ftyp[0] != 'N')
			{
				if((prpFlight->Ofbl == TIMENULL) && (olCurr > prpFlight->Stod + omCurrOfblStod))
				{
					oTmp.Format(GetString(IDS_STRING1240), omCurrOfblStod.GetTotalMinutes());
					olText = olBez + CString("STD: ") + prpFlight->Stod.Format("%H:%M %d.%m.%Y     ") + oTmp;
					//CString("Kein Offblock bei STD + %d Minuten");
					AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1240), olText, MOD_ID_DIA);
					blRet = true;
				}
			}
		}


		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 11   Actual Time + %d Min > Next Information Time  */

		if(bmCurrDNxti)
		{
			if((prpFlight->Nxti != TIMENULL) && (olCurr + omCurrDNxti > prpFlight->Nxti) && (prpFlight->Airb == TIMENULL))
			{

				oTmp.Format(GetString(IDS_STRING1242), omCurrDNxti.GetTotalMinutes());
				olText = olBez + CString("STD: ") + prpFlight->Stod.Format("%H:%M %d.%m.%Y     ") + oTmp;
				
				//CString("Aktuelle Zeit > Next Information - Zeit ");
				AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1242), olText, MOD_ID_DIA);
				blRet = true;
			}
		}

		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 15    No Offblock at CTOT + %d Minutes */
		
		if(omOfblCtot.GetTotalMinutes() != 0)
		{
			if( prpFlight->Ftyp[0] != 'X' && prpFlight->Ftyp[0] != 'N')
			{
				if((prpFlight->Slot != TIMENULL) && (prpFlight->Ofbl == TIMENULL) && (olCurr > prpFlight->Slot + omOfblCtot))
				{
					oTmp.Format(GetString(IDS_STRING2354), omOfblCtot.GetTotalMinutes());
					olText = olBez + CString("CTOT: ") + prpFlight->Slot.Format("%H:%M %d.%m.%Y     ") + oTmp;
					//CString("Kein Offblock bei STD + %d Minuten");
					AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING2354), olText, MOD_ID_DIA);
					blRet = true;
				}
			}
		}


	}  // if(cpFPart == 'D') ...


	CheckAttentionButton();

	return blRet;
}



void Konflikte::CheckCommonConflicts(const ROTATIONFLIGHTDATA *prpRotFlight, const DIAFLIGHTDATA *prpDiaFlight, 
									 DWORD dwOrigin, int ipSubOrig, const CTime &ropTestTime, const CString &ropFlightText)
{
	if(prpDiaFlight && CString(";T;G").Find(prpDiaFlight->Ftyp) > 0)
		return;

	if (prpRotFlight == NULL && prpDiaFlight == NULL) return;

	const CTime *polStod;
	const CTime *polEtdi;
	const CTime *polEtod;
	const CTime *polGd1x;
	const CTime *polLstu;
	const char *pclAdid;
	long llUrno;
	char clFPart = ' '; 
	bool blAssignedGate = false;

	if (prpRotFlight != NULL)
	{
		llUrno = prpRotFlight->Urno;
		polStod = &prpRotFlight->Stod;
		polEtdi = &prpRotFlight->Etdi;
		polEtod = &prpRotFlight->Etod;
		polGd1x = &prpRotFlight->Gd1x;
		polLstu = &prpRotFlight->Lstu;
		pclAdid = prpRotFlight->Adid;
		if (IsArrivalFlight(prpRotFlight->Org3, prpRotFlight->Des3, prpRotFlight->Ftyp[0])) 
		{
			clFPart = 'A';
		}
		if (IsDepartureFlight(prpRotFlight->Org3, prpRotFlight->Des3, prpRotFlight->Ftyp[0])) 
		{
			clFPart = 'D';
			if (strlen(prpRotFlight->Gtd1) > 0 || strlen(prpRotFlight->Gtd2) > 0)
				blAssignedGate = true;
		}
	}
	if (prpDiaFlight != NULL)
	{
		llUrno = prpDiaFlight->Urno;
		polStod = &prpDiaFlight->Stod;
		polEtdi = &prpDiaFlight->Etdi;
		polEtod = &prpDiaFlight->Etod;
		polGd1x = &prpDiaFlight->Gd1x;
		polLstu = &prpDiaFlight->Lstu;
		pclAdid = prpDiaFlight->Adid;
		if (IsArrivalFlight(prpDiaFlight->Org3, prpDiaFlight->Des3, prpDiaFlight->Ftyp[0])) 
		{
			clFPart = 'A';
		}
		if (IsDepartureFlight(prpDiaFlight->Org3, prpDiaFlight->Des3, prpDiaFlight->Ftyp[0])) 
		{
			clFPart = 'D';
			if (strlen(prpDiaFlight->Gtd1) > 0 || strlen(prpDiaFlight->Gtd2) > 0)
				blAssignedGate = true;
		}
	}

	if (clFPart == ' ')
		return;

	CString olTmp;
	CString olConfText;

	if(clFPart == 'D' && blAssignedGate)
	{

		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 13    STD/ETD - 30 Min. < actual time and no boarding information received */

		if(bmCurrStodGd1x)
		{
			if (strcmp(pcgHome, "PVG") == 0)
			{
				// Shanghai need other format and ETOD
				bool blConf = false;
				CString olField;
				if( *polGd1x == TIMENULL && *polEtod == TIMENULL && (*polStod - omCurrStodGd1x < ropTestTime))
				{
					blConf = true;
					olField = GetString(IDS_STRING316);
				}
				if ( *polGd1x == TIMENULL && *polEtod != TIMENULL && (*polEtod - omCurrStodGd1x < ropTestTime)) 
				{
					blConf = true;
					olField = GetString(IDS_STRING2029);
				}
				if (blConf)
				{
					// %d minutes before %s but still does not has any Boarding Information (%s)
					olTmp.Format(GetString(IDS_STRING2025), omCurrStodGd1x.GetTotalMinutes(), olField, polLstu->Format("%H:%M"));
					olConfText.Format("%s%s %s  %s", ropFlightText, GetString(IDS_STRING316), polStod->Format("%H:%M"), olTmp);
		
					AddKonflikt(KonfIdent(llUrno, pclAdid[0], 0, ' ', IDS_STRING1548), olConfText, dwOrigin, SUB_MOD_GAT);
				}
			}
			else
			{
				if( ( *polGd1x == TIMENULL && *polEtdi == TIMENULL && (*polStod - omCurrStodGd1x < ropTestTime)) ||
					( *polGd1x == TIMENULL && *polEtdi != TIMENULL && (*polEtdi - omCurrStodGd1x < ropTestTime)) ) 
				{
					olTmp.Format(GetString(IDS_STRING1548), omCurrStodGd1x.GetTotalMinutes());
					olConfText = ropFlightText + olTmp;//CString("STD/ETD - 30 Min. > actual time and no boarding information received");
		
					AddKonflikt(KonfIdent(llUrno, pclAdid[0], 0, ' ', IDS_STRING1548), olConfText, dwOrigin, SUB_MOD_GAT);
				}
			}		
		
		
		}

	}
}




///////////////////////////////////////////////
// SAS


void Konflikte::SASCheckAll(bool bpUseButtonList, bool bpTimerList) 
{
	CTime olTestTime = CTime::GetCurrentTime();

	DisableAttentionButtonsUpdate();
 
	DiaCedaFlightData::RKEYLIST *prlRkey;


	DIAFLIGHTDATA *prlFlight1;
	DIAFLIGHTDATA *prlFlight2;

	int ilFl = 0;
	int ilMap = 0;
	

	if (bpUseButtonList)
	{
		POSITION pos;
		for( pos = ogUrnoMapConfCheck.GetStartPosition(); pos != NULL; )
		{
			DIAFLIGHTDATA *prlFlight = NULL;
			long llUrno = 0;
			ogUrnoMapConfCheck.GetNextAssoc(pos, (void *&)llUrno, (void *&)prlFlight);
			if(prlFlight != NULL)
			{
				DiaCedaFlightData::RKEYLIST *prlRkey = ogPosDiaFlightData.GetRotationByRkey(llUrno);

				if (!prlRkey)
					continue;

				int ilCount = prlRkey->Rotation.GetSize();

				prlRkey->Rotation.Sort(CompareByRkey);

				for(int i = 0; i < ilCount; i++)
				{
					prlFlight1 = &(*prlRkey).Rotation[i];

					prlFlight2 = NULL;

					if(i + 1 < ilCount )
					{
						prlFlight2 = &(*prlRkey).Rotation[i+1];

						if( prlFlight2->Adid[0] == 'D')
						{
							i++;
						}
					}

					if(prlFlight2 == NULL)
					{
						if( strcmp(prlFlight1->Org3, pcgHome) == 0)
						{
							prlFlight2 = prlFlight1;
							prlFlight1 = NULL;
						}
					}

					if (prlFlight2)
						ilFl++;
					if (prlFlight1)
						ilFl++;

					CheckFlightInternal( prlFlight1, prlFlight2);
					if (prlFlight1)
					{
	//TRACE("\n A: Flno,Onbl,Ofbl,Airb : %s, %s, %s, %s",CString(prlFlight1->Flno),  prlFlight1->Onbl.Format("%H:%M:%S"),prlFlight1->Ofbl.Format("%H:%M:%S"),prlFlight1->Airb.Format("%H:%M:%S"));

						//ogDdx.DataChanged((void *)this, D_FLIGHT_CHANGE,(void *)prlFlight1 );
					}
					if (prlFlight2)
					{
	//TRACE("\n D: Flno,Onbl,Ofbl,Airb : %s, %s, %s, %s",CString(prlFlight2->Flno), prlFlight2->Onbl.Format("%H:%M:%S"),prlFlight2->Ofbl.Format("%H:%M:%S"),prlFlight2->Airb.Format("%H:%M:%S"));
						//ogDdx.DataChanged((void *)this, D_FLIGHT_CHANGE,(void *)prlFlight2 ); 
					}
				}
			}
		}

		if(bpTimerList)
		{
			for( pos = ogUrnoMapConfCheckTimer.GetStartPosition(); pos != NULL; )
			{
				DIAFLIGHTDATA *prlFlight = NULL;
				long llUrno = 0;
				ogUrnoMapConfCheckTimer.GetNextAssoc(pos, (void *&)llUrno, (void *&)prlFlight);
				if(prlFlight != NULL)
				{
					DiaCedaFlightData::RKEYLIST *prlRkey = ogPosDiaFlightData.GetRotationByRkey(llUrno);

					if (!prlRkey)
						continue;

					int ilCount = prlRkey->Rotation.GetSize();

					prlRkey->Rotation.Sort(CompareByRkey);

					for(int i = 0; i < ilCount; i++)
					{
						prlFlight1 = &(*prlRkey).Rotation[i];

						prlFlight2 = NULL;

						if(i + 1 < ilCount )
						{
							prlFlight2 = &(*prlRkey).Rotation[i+1];

							if( prlFlight2->Adid[0] == 'D')
							{
								i++;
							}
						}

						if(prlFlight2 == NULL)
						{
							if( strcmp(prlFlight1->Org3, pcgHome) == 0)
							{
								prlFlight2 = prlFlight1;
								prlFlight1 = NULL;
							}
						}

						if (prlFlight2)
							ilFl++;
						if (prlFlight1)
							ilFl++;

						CheckFlightInternal( prlFlight1, prlFlight2);
					}
				}
			}
		}
//		ogUrnoMapConfCheckTimer.RemoveAll();
		ilMap = ogUrnoMapConfCheck.GetCount();
		ogUrnoMapConfCheck.RemoveAll();
	}
	else
	{

		POSITION pos;
		void *pVoid;
		for( pos = ogPosDiaFlightData.omRkeyMap.GetStartPosition(); pos != NULL; )
		{
			ogPosDiaFlightData.omRkeyMap.GetNextAssoc( pos, pVoid , (void *&)prlRkey );

			int ilCount = prlRkey->Rotation.GetSize();

//qqq			prlRkey->Rotation.Sort(CompareByRkey);

			for(int i = 0; i < ilCount; i++)
			{
				prlFlight1 = &(*prlRkey).Rotation[i];

				prlFlight2 = NULL;

				if(i + 1 < ilCount )
				{
					prlFlight2 = &(*prlRkey).Rotation[i+1];

					if( prlFlight2->Adid[0] == 'D')
					{
						i++;
					}
				}

				if(prlFlight2 == NULL)
				{
					if( strcmp(prlFlight1->Org3, pcgHome) == 0)
					{
						prlFlight2 = prlFlight1;
						prlFlight1 = NULL;
					}
				}

				if (prlFlight2)
					ilFl++;
				if (prlFlight1)
					ilFl++;

				CheckFlightInternal( prlFlight1, prlFlight2);
			}
		}
	}



	/*	
	int ilCount = ogBCD.GetDataCount("PST");

	CString olPst;

	for(int i = 0; i < ilCount; i++)
	{
		olPst = ogBCD.GetField("PST", i, "PNAM");

		CheckPosition(olPst);
	}

	ilCount = ogBCD.GetDataCount("GAT");

	CString olGat;

	for( i = 0; i < ilCount; i++)
	{
		olGat = ogBCD.GetField("GAT", i, "GNAM");

		CheckGate(olGat);
	}


	ilCount = ogBCD.GetDataCount("BLT");

	CString olBlt;

	for( i = 0; i < ilCount; i++)
	{
		olBlt = ogBCD.GetField("BLT", i, "BNAM");

		CheckBlt(olBlt);
	}


	ilCount = ogBCD.GetDataCount("WRO");
	CString olWro;

	for( i = 0; i < ilCount; i++)
	{
		olWro = ogBCD.GetField("WRO", i, "WNAM");

		CheckWro(olWro);
	}
	*/	

	
	EnableAttentionButtonsUpdate();
 
	CTime olTestTimeEnd =  CTime::GetCurrentTime();
	TRACE("Konflikte::SASCheckAll: Laufzeit: %d %d %d\n", (olTestTimeEnd-olTestTime).GetTotalSeconds(), ilFl, ilMap);

	ogUrnoMapConfCheck.RemoveAll();
}


void Konflikte::CheckRotationConlicts(DIAFLIGHTDATA *prlFlight) 
{
	DiaCedaFlightData::RKEYLIST *prlRkey;
	DIAFLIGHTDATA *prlFlight1;
	DIAFLIGHTDATA *prlFlight2;

	POSITION pos;
	void *pVoid;
	for( pos = ogPosDiaFlightData.omRkeyMap.GetStartPosition(); pos != NULL; )
	{
		ogPosDiaFlightData.omRkeyMap.GetNextAssoc( pos, pVoid , (void *&)prlRkey );

		int ilCount = prlRkey->Rotation.GetSize();

		prlRkey->Rotation.Sort(CompareByRkey);

		for(int i = 0; i < ilCount; i++)
		{
			prlFlight1 = &(*prlRkey).Rotation[i];

			prlFlight2 = NULL;

			if(i + 1 < ilCount )
			{
				prlFlight2 = &(*prlRkey).Rotation[i+1];

				if( prlFlight2->Adid[0] == 'D')
				{
					i++;
				}
			}

			if(prlFlight2 == NULL)
			{
				if( strcmp(prlFlight1->Org3, pcgHome) == 0)
				{
					prlFlight2 = prlFlight1;
					prlFlight1 = NULL;
				}
			}

			CheckFlightInternal( prlFlight1, prlFlight2);
		}
	}
}


void Konflikte::CheckPosition(CString opPst)
{
	DiaCedaFlightData::RKEYLIST *prlRkey;
	
	if(ogPosDiaFlightData.omPosMap.Lookup(opPst,(void *& )prlRkey) == TRUE)
	{
		DIAFLIGHTDATA *prlFlight1;
		DIAFLIGHTDATA *prlFlight2;

		int ilCount = prlRkey->Rotation.GetSize();

		prlRkey->Rotation.Sort(CompareByRkey);

		for(int i = 0; i < ilCount; i++)
		{
			prlFlight1 = &(*prlRkey).Rotation[i];

			prlFlight2 = NULL;

			if(i + 1 < ilCount )
			{
				prlFlight2 = &(*prlRkey).Rotation[i+1];

				if(prlFlight1->Rkey != prlFlight2->Rkey)
				{
					if( strcmp(prlFlight1->Des3, pcgHome) == 0)
					{
						prlFlight2 = NULL;
					}
					else
					{
						prlFlight2 = prlFlight1;
						prlFlight1 = NULL;
					}
				}
				else
				{
					i++;
				}
			}

			if(prlFlight2 == NULL)
			{
				if( strcmp(prlFlight1->Des3, pcgHome) == 0)
				{
					prlFlight2 = NULL;
				}
				else
				{
					prlFlight2 = prlFlight1;
					prlFlight1 = NULL;
				}
			}

			CheckFlightInternal( prlFlight1, prlFlight2);
		}
	}

}


void Konflikte::CheckGate(CString opGat)
{
	DiaCedaFlightData::RKEYLIST *prlRkey;

	
	if(ogPosDiaFlightData.omGatMap.Lookup(opGat,(void *& )prlRkey) == TRUE)
	{
		DIAFLIGHTDATA *prlFlight1;
		int ilCount = prlRkey->Rotation.GetSize();

		prlRkey->Rotation.Sort(CompareByRkey);

		for(int i = 0; i < ilCount; i++)
		{
			prlFlight1 = &(*prlRkey).Rotation[i];

			if( strcmp(prlFlight1->Des3, pcgHome) == 0)
			{
				CheckFlightInternal( prlFlight1, NULL);
			}
			else
			{
				CheckFlightInternal( NULL, prlFlight1);
			}

		}
	}

}



void Konflikte::CheckWro(CString opWro)
{
	DiaCedaFlightData::RKEYLIST *prlRkey;

	
	if(ogPosDiaFlightData.omWroMap.Lookup(opWro,(void *& )prlRkey) == TRUE)
	{
		DIAFLIGHTDATA *prlFlight1;
		int ilCount = prlRkey->Rotation.GetSize();

		prlRkey->Rotation.Sort(CompareByRkey);

		for(int i = 0; i < ilCount; i++)
		{
			prlFlight1 = &(*prlRkey).Rotation[i];

			if( strcmp(prlFlight1->Org3, pcgHome) == 0)
			{
				CheckFlightInternal( NULL, prlFlight1);
			}
		}
	}
}



void Konflikte::CheckBlt(CString opBlt)
{
	DiaCedaFlightData::RKEYLIST *prlRkey;
	
	if(ogPosDiaFlightData.omBltMap.Lookup(opBlt,(void *& )prlRkey) == TRUE)
	{
		DIAFLIGHTDATA *prlFlight1;
		int ilCount = prlRkey->Rotation.GetSize();

		prlRkey->Rotation.Sort(CompareByRkey);

		for(int i = 0; i < ilCount; i++)
		{
			prlFlight1 = &(*prlRkey).Rotation[i];

			if( strcmp(prlFlight1->Des3, pcgHome) == 0)
			{
				CheckFlightInternal( prlFlight1, NULL);
			}
		}
	}
}





void Konflikte::CheckFlight(DIAFLIGHTDATA *prpFlight)
{
	CString olPst;

	if( strcmp(prpFlight->Des3, pcgHome) == 0)
		olPst = prpFlight->Psta;
	else
		olPst = prpFlight->Pstd;
	
	if(!olPst.IsEmpty())
	{
		DiaCedaFlightData::RKEYLIST *prlRkey;

		if(ogPosDiaFlightData.omPosMap.Lookup(olPst,(void *& )prlRkey) == TRUE)
		{
			DIAFLIGHTDATA *prlFlight1;
			DIAFLIGHTDATA *prlFlight2;
			int ilCount = prlRkey->Rotation.GetSize();
	
			prlRkey->Rotation.Sort(CompareByRkey);

			for(int i = 0; i < ilCount; i++)
			{
				prlFlight1 = &(*prlRkey).Rotation[i];

				prlFlight2 = NULL;

				if(i + 1 < ilCount )
				{
					prlFlight2 = &(*prlRkey).Rotation[i+1];

					if(prlFlight1->Rkey != prlFlight2->Rkey)
					{
						if( strcmp(prlFlight1->Des3, pcgHome) == 0)
						{
							prlFlight2 = NULL;
						}
						else
						{
							prlFlight2 = prlFlight1;
							prlFlight1 = NULL;
						}
					}
					else
					{
						/*

						if(CString(prlFlight1->Adid) == "D")
						{
							prlFlight2 = prlFlight1;
							prlFlight1 = NULL;
						}
						else
						{
						
							if(prlFlight2->Tifd <= prlFlight1->Tifa)
							{
								prlFlight2 = NULL;
							}
							else
							{
								if(CString(prlFlight2->Adid) != "B")
									i++;
							}				
						}
						*/
						i++;
					}
				}
				if(prlFlight2 == NULL)
				{
					if( strcmp(prlFlight1->Des3, pcgHome) == 0)
					{
						prlFlight2 = NULL;
					}
					else
					{
						prlFlight2 = prlFlight1;
						prlFlight1 = NULL;
					}
				}

				CheckFlightInternal( prlFlight1, prlFlight2);
			}
		}
	}

}




void Konflikte::CheckFlightExtConf(long lpUrno, char cpFPart, const CString &ropBez, CedaCflData *popCflData, DWORD wpOrigin)
{
	//return;
	//flight generates no conflicts anymore

	if (!bmFlightDataChanged || lpUrno <= 0 || popCflData == NULL)
		return;

	CCSPtrArray<CFLDATA> olCflList;
	popCflData->GetCflDataByRurnUrno(olCflList, lpUrno);


	CFLDATA *prlCfl;
	CString olText;
	int ilConfirmed;
	CString olUser;

	for(int i = 0; i < olCflList.GetSize(); i++)
	{
		prlCfl = &olCflList[i];

		if (strcmp(prlCfl->Mety, "FI") == 0)
			// record was generated by FIPS! 
			// The conflict will be internal (not in DB) generated once more in later functions! 
			continue;


		CString olConfText = ogBCD.GetField("LBL", "TKEY", prlCfl->Meno, "TEXT"); 
		CString olConfUrno = ogBCD.GetField("LBL", "TKEY", prlCfl->Meno, "URNO"); 

		CString strKonftype("");
		ogMapLblUrnoToKonftype.Lookup(olConfUrno, strKonftype);
		long llUrno = atoi(strKonftype);

		CString olNewVal;
		CString olOldVal;

		CStringArray olArray;

		if(!olConfUrno.IsEmpty())
		{

			CString olTmp;

			olText.Format("%s   %s", ropBez, olConfText);				


			// get old value
			olOldVal.Empty();
			ExtractItemList(CString(prlCfl->Oval), &olArray, 30);
			for(int k = 0; k < olArray.GetSize() - 1; k++)
			{
				olOldVal += olArray[k] + CString(",");
			}
			// Remove last ','
			if(!olOldVal.IsEmpty())
				olOldVal = olOldVal.Left(olOldVal.GetLength() - 1); 

			olArray.RemoveAll();

			// get new value
			olNewVal.Empty();
			ExtractItemList(CString(prlCfl->Nval), &olArray, 30);
			for(k = 0; k < olArray.GetSize() - 1; k++)
			{
				olNewVal += olArray[k] + CString(",");
			}
			// Remove last ','
			if(!olNewVal.IsEmpty())
				olNewVal = olNewVal.Left(olNewVal.GetLength() - 1); 

			// get user who changed the value
			CString olUserName("");
			if (olArray.GetSize() > 1)
				olUserName = olArray[olArray.GetSize()-1];

			// format conflict string
			olTmp.Format(" %s -> %s (%s %s)", olOldVal, olNewVal, olUserName, olCflList[i].Time.Format("%H:%M"));
			olText += olTmp;

			olUser = prlCfl->Akus;
			olUser.TrimRight();


			if(olUser.IsEmpty())
				ilConfirmed = 0;
			else
				ilConfirmed = 1;

			AddKonflikt(KonfIdent(lpUrno, cpFPart, 0, ' ', llUrno/*IDS_STRING1550*/), olText, MOD_ID_ROTATION, -1, olCflList[i].Time);
			//AddKonflikt(KonfIdent(lpUrno, cpFPart, 0, ' ', llUrno/*IDS_STRING1550*/), olText, MOD_ID_ROTATION, -1, olCflList[i].Time);
	//		AddKonflikt(KonfIdent(lpUrno, cpFPart, 0, ' ', olCflList[i].Urno), olText, wpOrigin, SUB_MOD_ALL, olCflList[i].Time, ilConfirmed);

		}

	}

	olCflList.RemoveAll();
}




void Konflikte::CheckFlightInternal(DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD, bool bpOnlyNeighborConf /* = false */)
{

	DiaCedaFlightData::RKEYLIST *prpRkey ;


	long llRkey = 0;

	if(prpFlightA != NULL)
		llRkey = prpFlightA->Rkey;


	if(prpFlightD != NULL)
		llRkey = prpFlightD->Rkey;

	if (llRkey == 0)
		return;


	prpRkey = ogPosDiaFlightData.GetRotationByRkey(llRkey);


	if (prpRkey == NULL)
		return;



	DIAFLIGHTDATA *prlFlightA;
	DIAFLIGHTDATA *prlFlightD;

//	prpRkey->Rotation.Sort(DiaCedaFlightData::CompareByRkey);
	prpRkey->Rotation.Sort(DiaCedaFlightData::CompareRotationFlight);


	int ilCount = prpRkey->Rotation.GetSize();

	for(int i = 0; i < ilCount; i++)
	{
		prlFlightA = &(*prpRkey).Rotation[i];


		if(prpRkey->Rotation.GetSize() == 1 && prlFlightA->Adid[0] == 'D')
		{
			prlFlightD = prlFlightA;
			prlFlightA = NULL;
		}
		else
		{
			if(i+1 <  ilCount)
			{
				prlFlightD = &(*prpRkey).Rotation[i+1];
		
				
				if(prlFlightD->Tifd < prlFlightA->Tifa || strcmp(prlFlightD->Pstd, prlFlightA->Psta) != 0)
				{
					prlFlightD = NULL;
				}
				else
				{
					if(prlFlightD->Adid[0] != 'B')
						i++;
				}				
			
			
			}
			else
			{
				if(prlFlightA->Adid[0] == 'D')
				{
					prlFlightD = prlFlightA;
					prlFlightA = NULL;
				}
				else
				{
					prlFlightD = NULL;
				}
			}
		}


		if (prlFlightA != NULL && IsCircularFlight(prlFlightA->Org3, prlFlightA->Des3, prlFlightA->Ftyp[0]))
		{
			if (bpOnlyNeighborConf)
			{
				CheckFlightOnlyNeighborConf(prlFlightA, NULL);
				CheckFlightOnlyNeighborConf(NULL, prlFlightD);
			}
			else
			{
				CheckFlightInternal2(prlFlightA, NULL);
				CheckFlightInternal2(NULL, prlFlightA);
			}
		}
		else
		{
			if (bpOnlyNeighborConf)
			{
				CheckFlightOnlyNeighborConf(prlFlightA, prlFlightD);				
			}
			else
			{
				CheckFlightInternal2(prlFlightA, prlFlightD);
			}
		}

	}




}




void Konflikte::CheckFlightOnlyNeighborConf(DIAFLIGHTDATA *prlFlightA, DIAFLIGHTDATA *prlFlightD)
{

	CString olText;
	CString olPsta;
	CString olPstd;
	CString  olBezA;
	CString  olBezD;

	if(prlFlightA != NULL)
	{
		ResetAllNeighborConfFlight(prlFlightA->Urno, 'A');
		olPsta = prlFlightA->Psta;

		olBezA.Format("%-9s (A): ", prlFlightA->Flno);
		//olBez = CString(prlFlightA->Flno) + CString("  ");
	}


	if(prlFlightD != NULL)
	{
		ResetAllNeighborConfFlight(prlFlightD->Urno, 'D');
		olPstd = prlFlightD->Pstd;

		olBezD.Format("%-9s (D): ", prlFlightD->Flno);
	}

	/////////////////////////////////////////



	///////////////////////////////////////////
	///////////////////////////////////////////
	CCSPtrArray<KonfItem> opKonfList;

	KonfItem *prlKonf;

	int i;
	// get all !!neighbor related!! conflicts of the position
	if (!olPsta.IsEmpty())
	{
		ogSpotAllocation.CheckPstNeighborConf( prlFlightA, prlFlightD, olPsta, &opKonfList);
	 	for(i = 0; i < opKonfList.GetSize(); i++)
		{
 			prlKonf = &opKonfList[i];
			if (prlKonf->KonfId.FUrno > 0)
			{
				CString olBezTmp;
				if (prlKonf->KonfId.FUrno == prlFlightA->Urno)
					olBezTmp = olBezA;
				else
					olBezTmp = olBezD;
				AddKonflikt(prlKonf->KonfId, olBezTmp + CString(prlKonf->Text), MOD_ID_DIA, SUB_MOD_PST);
			}
			else
			{
				AddKonflikt(KonfIdent(prlFlightA->Urno, 'A', prlKonf->KonfId.RelFUrno, prlKonf->KonfId.RelFPart, prlKonf->KonfId.Type), olBezA + CString(prlKonf->Text), MOD_ID_DIA, SUB_MOD_PST);
			}
		}
		opKonfList.DeleteAll();
	}
	else if (!olPstd.IsEmpty())
	{
		ogSpotAllocation.CheckPstNeighborConf( prlFlightA, prlFlightD, olPstd, &opKonfList);
	 	for(i = 0; i < opKonfList.GetSize(); i++)
		{
 			prlKonf = &opKonfList[i];
			if (prlKonf->KonfId.FUrno > 0)
			{
				CString olBezTmp;
				if (prlKonf->KonfId.FUrno == prlFlightD->Urno)
					olBezTmp = olBezD;
				else
					olBezTmp = olBezA;
				AddKonflikt(prlKonf->KonfId, olBezTmp + CString(prlKonf->Text), MOD_ID_DIA, SUB_MOD_PST);
			}
			else
			{
				AddKonflikt(KonfIdent(prlFlightD->Urno, 'D', prlKonf->KonfId.RelFUrno, prlKonf->KonfId.RelFPart, prlKonf->KonfId.Type), 
					olBezD + CString(prlKonf->Text), MOD_ID_DIA, SUB_MOD_PST);
			}
		}
		opKonfList.DeleteAll();
	}


	if(prlFlightA != NULL)
		CheckFlightConf(prlFlightA->Urno, 'A', MOD_ID_DIA);

	if(prlFlightD != NULL)
		CheckFlightConf(prlFlightD->Urno, 'D', MOD_ID_DIA);

	CheckAttentionButton();
}






void Konflikte::CheckFlightInternal2(DIAFLIGHTDATA *prlFlightA, DIAFLIGHTDATA *prlFlightD)
{

	CString olText;
	CString olPsta;
	CString olPstd;
	CString  olBezA;
	CString  olBezD;
	CString  olGta1;
	CString  olGta2;
	CString  olGtd1;
	CString  olGtd2;
	CString  olWro1;
	CString  olWro2;
	CString  olBlt1;
	CString	 olBlt2;


	if (!bgConflictsForAirb)
	{
		//dont check airbored flights
		if (prlFlightD && prlFlightD->Adid[0] == 'D' && prlFlightD->Airb != TIMENULL)
			return;
	}

	bool blCheckA = true;
	bool blCheckD = true;
	if(prlFlightA && CString(";T;G").Find(prlFlightA->Ftyp) > 0)
		blCheckA = false;
	if(prlFlightD && CString(";T;G").Find(prlFlightD->Ftyp) > 0)
		blCheckD = false;


	if(prlFlightA != NULL)
	{
		ResetFlight(prlFlightA->Urno, 'A', MOD_ID_ROTATION);
		ResetFlight(prlFlightA->Urno, 'A', MOD_ID_DIA);
		olPsta = prlFlightA->Psta;
		olGta1 = prlFlightA->Gta1;
		olGta2 = prlFlightA->Gta2;
		olBlt1 = prlFlightA->Blt1;
		olBlt2 = prlFlightA->Blt2;

		olBezA.Format("%-9s (A): ", prlFlightA->Flno);
		//olBez = CString(prlFlightA->Flno) + CString("  ");

		if (bmFlightDataChanged)
		{
			CString olExtConfBez;
			olExtConfBez.Format("%sSTA: %s", olBezA, prlFlightA->Stoa.Format("%H:%M %d.%m.%Y"));				
			CheckFlightExtConf(prlFlightA->Urno, 'A', olExtConfBez, ogPosDiaFlightData.pomCflData, MOD_ID_DIA);
		}
	}


	if(prlFlightD != NULL)
	{
		ResetFlight(prlFlightD->Urno, 'D', MOD_ID_ROTATION);
		ResetFlight(prlFlightD->Urno, 'D', MOD_ID_DIA);
		olPstd = prlFlightD->Pstd;

		olGtd1 = prlFlightD->Gtd1;
		olGtd2 = prlFlightD->Gtd2;
		olWro1 = prlFlightD->Wro1;
		olWro2 = prlFlightD->Wro2;

		olBezD.Format("%-9s (D): ", prlFlightD->Flno);

		if (bmFlightDataChanged)
		{
			CString olExtConfBez;
			olExtConfBez.Format("%sSTD: %s", olBezD, prlFlightD->Stod.Format("%H:%M %d.%m.%Y"));				
			CheckFlightExtConf(prlFlightD->Urno, 'D', olExtConfBez, ogPosDiaFlightData.pomCflData, MOD_ID_DIA);
		}
	}

	/////////////////////////////////////////



	///////////////////////////////////////////
	///////////////////////////////////////////
	///////////////////////////////////////////
	///////////////////////////////////////////
	CCSPtrArray<KonfItem> opKonfList;

	KonfItem *prlKonf;

	int i;
	// get all conflicts of the position
	if (!olPsta.IsEmpty())
	{
		ogSpotAllocation.CheckPst( prlFlightA, prlFlightD, olPsta, &opKonfList);
	 	for(i = 0; i < opKonfList.GetSize(); i++)
		{
 			prlKonf = &opKonfList[i];
			if (prlKonf->KonfId.FUrno > 0)
			{
				CString olBezTmp;
				if (prlKonf->KonfId.FUrno == prlFlightA->Urno)
					olBezTmp = olBezA;
				else
					olBezTmp = olBezD;
				AddKonflikt(prlKonf->KonfId, olBezTmp + CString(prlKonf->Text), MOD_ID_DIA, SUB_MOD_PST);
			}
			else
			{
				AddKonflikt(KonfIdent(prlFlightA->Urno, 'A', prlKonf->KonfId.RelFUrno, prlKonf->KonfId.RelFPart, prlKonf->KonfId.Type), olBezA + CString(prlKonf->Text), MOD_ID_DIA, SUB_MOD_PST);
			}
		}
		opKonfList.DeleteAll();
	}
	else if (!olPstd.IsEmpty())
	{
		ogSpotAllocation.CheckPst( prlFlightA, prlFlightD, olPstd, &opKonfList);
	 	for(i = 0; i < opKonfList.GetSize(); i++)
		{
 			prlKonf = &opKonfList[i];
			if (prlKonf->KonfId.FUrno > 0)
			{
				CString olBezTmp;
				if (prlKonf->KonfId.FUrno == prlFlightD->Urno)
					olBezTmp = olBezD;
				else
					olBezTmp = olBezA;
				AddKonflikt(prlKonf->KonfId, olBezTmp + CString(prlKonf->Text), MOD_ID_DIA, SUB_MOD_PST);
			}
			else
			{
				AddKonflikt(KonfIdent(prlFlightD->Urno, 'D', prlKonf->KonfId.RelFUrno, prlKonf->KonfId.RelFPart, prlKonf->KonfId.Type), 
					olBezD + CString(prlKonf->Text), MOD_ID_DIA, SUB_MOD_PST);
			}
		}
		opKonfList.DeleteAll();
	}


	///////////////////////////////////////////
	///////////////////////////////////////////
	///////////////////////////////////////////
	///////////////////////////////////////////
//		return;

	// get all conflicts of the two gates
	if(!olGta1.IsEmpty())
	{
		ogSpotAllocation.CheckGat( prlFlightA, 'A', olGta1,1, &opKonfList);
	}

	if(!olGta2.IsEmpty())
	{
		ogSpotAllocation.CheckGat( prlFlightA, 'A', olGta2,2, &opKonfList);
	}
	
	for( i = 0; i < opKonfList.GetSize(); i++)
	{
		prlKonf = &opKonfList[i];
		AddKonflikt(prlKonf->KonfId, olBezA + CString(prlKonf->Text), MOD_ID_DIA, SUB_MOD_GAT);
	}
	
	opKonfList.DeleteAll();

	if(!olGtd1.IsEmpty())
	{
		ogSpotAllocation.CheckGat( prlFlightD, 'D', olGtd1,1, &opKonfList);
	}
	if(!olGtd2.IsEmpty())
	{
		ogSpotAllocation.CheckGat( prlFlightD, 'D', olGtd2,2, &opKonfList);
	}

	for( i = 0; i < opKonfList.GetSize(); i++)
	{
		prlKonf = &opKonfList[i];
		AddKonflikt(prlKonf->KonfId, olBezD + CString(prlKonf->Text), MOD_ID_DIA, SUB_MOD_GAT);
	}
	
	opKonfList.DeleteAll();


	// get all conflicts of the waiting room
	if(!olWro1.IsEmpty())
	{
		ogSpotAllocation.CheckWro( prlFlightD, olWro1, 1, &opKonfList);
	}
	if(!olWro2.IsEmpty())
	{
		ogSpotAllocation.CheckWro( prlFlightD, olWro2, 2, &opKonfList);
	}

	for( i = 0; i < opKonfList.GetSize(); i++)
	{
		prlKonf = &opKonfList[i];
		AddKonflikt(prlKonf->KonfId, olBezD + CString(prlKonf->Text), MOD_ID_DIA, SUB_MOD_WRO);
	}
	
	opKonfList.DeleteAll();


	// get all conflicts of the baggage belt
	if(!olBlt1.IsEmpty())
	{
		ogSpotAllocation.CheckBlt( prlFlightA, olBlt1, 1, &opKonfList, 0);
	}
	if(!olBlt2.IsEmpty())
	{
		ogSpotAllocation.CheckBlt( prlFlightA, olBlt2, 2, &opKonfList, 0);
	}

	for( i = 0; i < opKonfList.GetSize(); i++)
	{
		prlKonf = &opKonfList[i];
		AddKonflikt(prlKonf->KonfId, olBezA + CString(prlKonf->Text), MOD_ID_DIA, SUB_MOD_BLT);
	}
	
	opKonfList.DeleteAll();




	CTime olCurr = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olCurr);

	if (bgGATPOS)
		CheckGatPosConflicts(prlFlightA, prlFlightD, olBezA, olBezD);

	CheckCommonConflicts(NULL, prlFlightA, MOD_ID_DIA, SUB_MOD_NOTIMP, olCurr, olBezA);
	CheckCommonConflicts(NULL, prlFlightD, MOD_ID_DIA, SUB_MOD_NOTIMP, olCurr, olBezD);

	// test additional conflicts that are not related to any resources
	if(prlFlightA != NULL)
	{
		
		CString oTmp;

		////////////////////////////////////////////////////////////////////////////////////////
		// Conflict No. ??    Departure/Towing before Arrival 

		bool blConf = false;

		DiaCedaFlightData::RKEYLIST *prlRkey;

		prlRkey = ogPosDiaFlightData.GetRotationByRkey(prlFlightA->Rkey); 

		if( prlRkey != NULL)
		{
			DIAFLIGHTDATA *prlFlight1;
			DIAFLIGHTDATA *prlFlight2;

//			prlRkey->Rotation.Sort(CompareRotationFlight);

			for(int i = 1; i < prlRkey->Rotation.GetSize() ; i++)
			{
				prlFlight1 = &prlRkey->Rotation[i-1];
				prlFlight2 = &prlRkey->Rotation[i];

				if(prlFlight1->Tifa >= prlFlight2->Tifd)
				{
					blConf = true;
					break;
				}

				if( prlFlight1->Adid[0] == 'B' && prlFlight1->Tifa <= prlFlight1->Tifd)
				{
					blConf = true;
					break;
				}

				if(prlFlight2->Adid[0] == 'B' && prlFlight2->Tifa <= prlFlight2->Tifd)
				{
					blConf = true;
					break;
				}


			}


			if(blConf)
			{

				for(int i = 0; i < prlRkey->Rotation.GetSize() ; i++)
				{
					prlFlight1 = &prlRkey->Rotation[i];
					
					//CString("Departure/Towing before Arrival");
					AddKonflikt(KonfIdent(prlFlightA->Urno, 'A', 0, ' ', IDS_STRING1890), olBezA + GetString(IDS_STRING1890), MOD_ID_DIA);
//					AddKonflikt(KonfIdent(prlFlight1->Urno, 'A', 0, ' ', IDS_STRING1890), olBezA + GetString(IDS_STRING1890), MOD_ID_DIA);
//					AddKonflikt(KonfIdent(prlFlight2->Urno, 'A', 0, ' ', IDS_STRING1890), olBezA + GetString(IDS_STRING1890), MOD_ID_DIA);

					CString olBez1;
					olBez1.Format("%-9s (A): ", prlFlight1->Flno);
					AddKonflikt(KonfIdent(prlFlight1->Urno, 'A', 0, ' ', IDS_STRING1890), olBez1 + GetString(IDS_STRING1890), MOD_ID_DIA);

					CString olBez2;
					olBez2.Format("%-9s (A): ", prlFlight2->Flno);
					AddKonflikt(KonfIdent(prlFlight2->Urno, 'A', 0, ' ', IDS_STRING1890), olBez2 + GetString(IDS_STRING1890), MOD_ID_DIA);

				}
			}
		}
 
	}
	//qqq*/
//qqq12
	//return;
 	////////////////////////////////////////////////////////////////////////////////////////
	// Conflict No. ??    Towing can not go off block if the flight is not on block 
	if (prlFlightA != NULL && prlFlightA->Ofbl != TIMENULL && (prlFlightA->Ftyp[0] == 'T' || prlFlightA->Ftyp[0] == 'G'))
	{
 		DiaCedaFlightData::RKEYLIST *prlRkey;

		prlRkey = ogPosDiaFlightData.GetRotationByRkey(prlFlightA->Rkey);

		if( prlRkey != NULL)
		{
			DIAFLIGHTDATA *prlFlight1;
 			for(int i = 0; i < prlRkey->Rotation.GetSize() ; i++)
			{
				prlFlight1 = &prlRkey->Rotation[i];
				if (IsArrivalFlight(prlFlight1->Org3, prlFlight1->Des3, prlFlight1->Ftyp[0]) && 
					prlFlight1->Onbl == TIMENULL)
				{					
					//CString("Towing can not go off block if the flight is not on block!");
					AddKonflikt(KonfIdent(prlFlightA->Urno, 'A', 0, ' ', IDS_STRING1981), olBezA + GetString(IDS_STRING1981), MOD_ID_DIA);
					break;
				}
			}
		}
	}
				

/*	
	if(prlFlightD != NULL)
	{
		////////////////////////////////////////////////////////////////////////////////////////
		// Conflict No. 10    No Offblock at STD + %d Minutes 
		
		if(bmCurrOfblStod)
		{
			if((prlFlightD->Ofbl == TIMENULL) && (olCurr > prlFlightD->Stod + omCurrOfblStod))
			{
				CString oTmp;
				oTmp.Format(GetString(IDS_STRING1240), omCurrOfblStod.GetTotalMinutes());
				//CString("Kein Offblock bei STD + %d Minuten");
				AddKonflikt(KonfIdent(prlFlightD->Urno, 'D', 0, ' ', IDS_STRING1240), olBezD + oTmp, MOD_ID_DIA);
			}
		}
	}
*/
	//*qqq

	if(prlFlightD != NULL)
	{
		CString oTmp;

		////////////////////////////////////////////////////////////////////////////////////////
		// Conflict No. ??    Departure/Towing before Arrival 

		bool blConf = false;

		DiaCedaFlightData::RKEYLIST *prlRkey;

		prlRkey = ogPosDiaFlightData.GetRotationByRkey(prlFlightD->Rkey);

		if( prlRkey != NULL)
		{
			DIAFLIGHTDATA *prlFlight1;
			DIAFLIGHTDATA *prlFlight2;

//			prlRkey->Rotation.Sort(CompareRotationFlight);

			for(int i = 1; i < prlRkey->Rotation.GetSize() ; i++)
			{
				prlFlight1 = &prlRkey->Rotation[i-1];
				prlFlight2 = &prlRkey->Rotation[i];

				if(prlFlight1->Tifa >= prlFlight2->Tifd)
				{
					blConf = true;
					break;
				}

				if( prlFlight1->Adid[0] == 'B' && prlFlight1->Tifa <= prlFlight1->Tifd)
				{
					blConf = true;
					break;
				}

				if(prlFlight2->Adid[0] == 'B' && prlFlight2->Tifa <= prlFlight2->Tifd)
				{
					blConf = true;
					break;
				}


			}


			if(blConf)
			{

				for(int i = 0; i < prlRkey->Rotation.GetSize() ; i++)
				{
					prlFlight1 = &prlRkey->Rotation[i];
					
					//CString("Departure/Towing before Arrival");
					AddKonflikt(KonfIdent(prlFlightD->Urno, 'D', 0, ' ', IDS_STRING1890), olBezD + GetString(IDS_STRING1890), MOD_ID_DIA);

					CString olBez1;
					olBez1.Format("%-9s (D): ", prlFlight1->Flno);
					AddKonflikt(KonfIdent(prlFlight1->Urno, 'D', 0, ' ', IDS_STRING1890), olBez1 + GetString(IDS_STRING1890), MOD_ID_DIA);

					CString olBez2;
					olBez2.Format("%-9s (D): ", prlFlight2->Flno);
					AddKonflikt(KonfIdent(prlFlight2->Urno, 'D', 0, ' ', IDS_STRING1890), olBez2 + GetString(IDS_STRING1890), MOD_ID_DIA);
				}
			}
		}
	}
	//qqq*/

//###############################
	CString oTmp;
	CString olPst;
	char buffer[256];

//	CTime olCurr = CTime::GetCurrentTime();

	if(prlFlightA)
	{
		////////////////////////////////////////////////////////////////////////////////////////
		if (bgConflictNewFlight)
		{
			/* Conflict New flight*/
			CTime olSto;

			CTime olStart= CTime(olCurr.GetYear(), olCurr.GetMonth(), olCurr.GetDay(),0,0,0);
			CTime olEnd= CTime(olCurr.GetYear(), olCurr.GetMonth(), olCurr.GetDay(),23,59,59);
			if(IsBetween(prlFlightA->Cdat , olStart, olEnd))
			{
				olText.Format("%sSTA: %s   %s (%s %s)", olBezA, prlFlightA->Stoa.Format("%H:%M %d.%m.%Y"), GetString(IDS_STRING1805), prlFlightA->Usec, prlFlightA->Cdat.Format("%H:%M"));
				AddKonflikt(KonfIdent(prlFlightA->Urno, 'A', 0 , ' ', IDS_STRING1805), olText, MOD_ID_DIA);
			}
		}



		olText.Format("%sSTA: %s     ", olBezA, prlFlightA->Stoa.Format("%H:%M %d.%m.%Y"));
		
		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 14  |ETA - STA| > %d Minutes  */
		
		if(blCheckA && bmStoaEtai)
		{
			CTimeSpan olEtaStaDiff = prlFlightA->Etai - prlFlightA->Stoa;
			if((prlFlightA->Etai != TIMENULL) && (labs(olEtaStaDiff.GetTotalMinutes()) > omStoaEtai.GetTotalMinutes() )  && (prlFlightA->Airb == TIMENULL))
			{
				oTmp.Format(GetString(IDS_STRING1860), omStoaEtai.GetTotalMinutes());
 				AddKonflikt(KonfIdent(prlFlightA->Urno, 'A', 0, ' ', IDS_STRING1860), olText + oTmp, MOD_ID_DIA);
			}
		}






		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 14  Return flight */

		if( CString(prlFlightA->Stat).GetLength() >= 10)
		{
			if( prlFlightA->Stat[8] == '1')
			{
				AddKonflikt(KonfIdent(prlFlightA->Urno, 'A', 0, ' ', IDS_STRING1697), olBezA + GetString(IDS_STRING1697), MOD_ID_DIA);

			}
		}
		
		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 1  start-landing order!  */
		
		if(blCheckA && bmJoinConf)
		{
			if(strcmp(prlFlightA->Rtyp, "E") == 0)
			{
				olText.Format("%sSTA: %s  <%s>   ", olBezA, prlFlightA->Stoa.Format("%H:%M %d.%m.%Y"), prlFlightA->Regn);
	
				olText += GetString(IDS_STRING1430);
			
				AddKonflikt(KonfIdent(prlFlightA->Urno, 'A', 0, ' ', IDS_STRING1430), olText, MOD_ID_DIA);
			}
		}
		


		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 2   No Onblock at Touch down + %d  Minutes */

		if(blCheckA && bmNoOnblAfterLand)
		{
			if((prlFlightA->Land != TIMENULL) && (prlFlightA->Onbl == TIMENULL) && (olCurr > prlFlightA->Land + omNoOnblAfterLand))
			{

				oTmp.Format(GetString(IDS_STRING1236), omNoOnblAfterLand.GetTotalMinutes() );
				//CString("Kein Onblock bei Landung + %d Minuten");
				AddKonflikt(KonfIdent(prlFlightA->Urno, 'A', 0, ' ', IDS_STRING1236), olBezA + oTmp, MOD_ID_DIA);
			}
		}

		
		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 3   Actual Time > ETA + %d Minutes  */

		if(blCheckA && bmCurrEtai)
		{
			if (strcmp(pcgHome, "PVG") == 0)
			{
				// Shanghai need other format and ETOA!
				if((prlFlightA->Etoa != TIMENULL) && (olCurr > prlFlightA->Etoa + omCurrEtai /*ol5*/) && (prlFlightA->Land == TIMENULL))
				{
					CString olTmp2;
					olTmp2.Format(GetString(IDS_STRING2024), omCurrEtai.GetTotalMinutes(), prlFlightA->Lstu.Format("%H:%M"));
					oTmp.Format("%s %s %s %s %s", GetString(IDS_STRING323), prlFlightA->Stoa.Format("%H:%M"), GetString(IDS_STRING2028), prlFlightA->Etoa.Format("%H:%M"), olTmp2);
					AddKonflikt(KonfIdent(prlFlightA->Urno, 'A', 0, ' ', IDS_STRING1237), olBezA + oTmp, MOD_ID_DIA);
				}
			}
			else
			{
				if((prlFlightA->Etai != TIMENULL) && (olCurr > prlFlightA->Etai + omCurrEtai /*ol5*/) && (prlFlightA->Land == TIMENULL))
				{
					oTmp.Format(GetString(IDS_STRING1237), omCurrEtai.GetTotalMinutes());
					AddKonflikt(KonfIdent(prlFlightA->Urno, 'A', 0, ' ', IDS_STRING1237), olBezA + oTmp, MOD_ID_DIA);
				}
			}
		}
		

		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 4   Actual Time  + %d Min > Next Information Time  */
		
		if(blCheckA && bmCurrANxti)
		{
			if((prlFlightA->Nxti != TIMENULL) && (olCurr + omCurrANxti > prlFlightA->Nxti) && (prlFlightA->Land == TIMENULL))
			{

				oTmp.Format(GetString(IDS_STRING1238), omCurrANxti.GetTotalMinutes());//CString("Aktuelle Zeit + %d Min > Next Information - Zeit ");
				AddKonflikt(KonfIdent(prlFlightA->Urno, 'A', 0, ' ', IDS_STRING1238), olBezA + oTmp, MOD_ID_DIA);
			}
		}

		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 5   Pos: %s  A/C: %s  error (%s) from parking system !  */

		if(bmAderConf)
		{
			if(strlen(prlFlightA->Ader) > 0)
			{

				sprintf(buffer,GetString(IDS_STRING1428), prlFlightA->Psta, prlFlightA->Act3, prlFlightA->Ader);
				//Andocksystem;
				AddKonflikt(KonfIdent(prlFlightA->Urno, 'A', 0, ' ', IDS_STRING1428), olBezA + CString(buffer), MOD_ID_DIA);
			}
		}


		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 6   STA/ETA    >=    STD/ETD  */ 
		DIAFLIGHTDATA* prlFlightDep = ogPosDiaFlightData.GetDeparture(prlFlightA);
		if (prlFlightDep)
		{
			CTime olTimeA = prlFlightA->Etai;
			if (olTimeA == TIMENULL)
				olTimeA = prlFlightA->Stoa;

			if (olTimeA != TIMENULL)
			{
				CTime olTimeD = prlFlightDep->Etdi;
				if (olTimeD == TIMENULL)
					olTimeD = prlFlightDep->Stod;

				if (olTimeD != TIMENULL)
				{
					if (olTimeA >= olTimeD)
					{
						olText = olBezA + GetString(IDS_STRING1432);
						AddKonflikt(KonfIdent(prlFlightA->Urno, 'A', 0, ' ', IDS_STRING1432), olText, MOD_ID_DIA);
//						AddKonflikt(KonfIdent(prpFlight->Urno, 'D', 0, ' ', IDS_STRING1432), olText, MOD_ID_ROTATION);
					}
				}
			}
		}

		/**************************
		DIAFLIGHTDATA* prlFlightDep = ogPosDiaFlightData.GetDeparture(prlFlightA);
		if(bmStoaStod)
		{

			if(prlFlightDep != NULL)
			{
				if(prlFlightA->Etai != TIMENULL)
				{
					
					if(((prlFlightA->Etai >= prlFlightDep->Stod) && (prlFlightDep->Etdi == TIMENULL) ) 
						|| ( (prlFlightA->Etai >= prlFlightDep->Etdi) &&  (prlFlightDep->Etdi != TIMENULL)  ))
					{

						oTmp.Format(GetString(IDS_STRING1432), omStoaStod.GetTotalMinutes());//CString("STA/ETA + %d Min ist gr��er als  STD/ETD");
						olText = olBezA + oTmp;
						
						AddKonflikt(KonfIdent(prlFlightA->Urno, 'A', 0, ' ', IDS_STRING1432), olText, MOD_ID_DIA);
					}

				}
			}
		}
		***********************/

/*
		if(*prlFlightA->Adid == 'A') 
		{
			CTime olTmpSta = prlFlightA->Stoa;

			if(prlFlightA->Etai != TIMENULL)
			{
				olTmpSta = prlFlightA->Etai;
			}
				
			if(((olTmpSta < prlFlightA->Stod) && (prlFlightA->Etdi == TIMENULL)) ||
			   ((olTmpSta < prlFlightA->Etdi) && (prlFlightA->Etdi != TIMENULL)))
			{

				oTmp.Format(GetString(IDS_STRING1432), omStoaStod.GetTotalMinutes());//CString("STA/ETA + %d Min ist gr��er als  STD/ETD");
				olText = olBezA + oTmp;
				
				AddKonflikt(KonfIdent(prlFlightA->Urno, 'A', 0, ' ', IDS_STRING1432), olText, MOD_ID_DIA);
			}
		}

*/

		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 7   STD/ETD + %d Min. < actual time and no DEP telegram received  */

		if(blCheckA && bmStodCurrAndNoAirb)
		{
			if (strcmp(pcgHome, "PVG") == 0)
			{
				// Shanghai need other format!
				if(prlFlightA->Airb == TIMENULL && prlFlightA->Stod != TIMENULL && (prlFlightA->Stod + omStodCurrAndNoAirb < olCurr) &&  prlFlightA->Aird == TIMENULL)
				{

					// STD+%d minutes still have not received DEP telex (%s)
					oTmp.Format(GetString(IDS_STRING2026), omStodCurrAndNoAirb.GetTotalMinutes(), prlFlightA->Lstu.Format("%H:%M"));
					olText = olBezA + oTmp;
										
					AddKonflikt(KonfIdent(prlFlightA->Urno, 'A', 0, ' ', IDS_STRING1549), olText, MOD_ID_DIA);
				}
			}
			else
			{
				if( (prlFlightA->Stod != TIMENULL && prlFlightA->Airb == TIMENULL && (prlFlightA->Stod + omStodCurrAndNoAirb < olCurr) &&  prlFlightA->Aird == TIMENULL && prlFlightA->Etdi == TIMENULL ) ||
					(prlFlightA->Airb == TIMENULL && (prlFlightA->Etdi + omStodCurrAndNoAirb < olCurr) &&  prlFlightA->Aird == TIMENULL && prlFlightA->Etdi != TIMENULL ) ) 
//				if( ( prlFlightA->Airb == TIMENULL && (prlFlightA->Stod + omStodCurrAndNoAirb < olCurr) &&  prlFlightA->Aird == TIMENULL && prlFlightA->Etdi == TIMENULL ) ||
//					( (prlFlightA->Etdi + omStodCurrAndNoAirb  < olCurr) &&  prlFlightA->Aird == TIMENULL && prlFlightA->Etdi != TIMENULL ) ) 
				{

					oTmp.Format(GetString(IDS_STRING1549), omStodCurrAndNoAirb.GetTotalMinutes());
					olText = olBezA + oTmp;
					
					//CString("STD/ETD + 30 Min. > actual time and No DEP telegram received");
					
					AddKonflikt(KonfIdent(prlFlightA->Urno, 'A', 0, ' ', IDS_STRING1549), olText,  MOD_ID_DIA);
				}
			}
		}

		////////////////////////////////////////////////////////////////////////////////////////
		/* Conflict No. 8   Aircrafttype has been changed */

		/****************
		
		if( bmActRegnChangeConf)
		{
			// IDS_STRING1551  Registrierung hat sich ge�ndert
			if( CString(prpFlight->Regn) != CString(prpFlight->LastRegn)) 
			{
				olText = olBez + GetString(IDS_STRING1550);
				AddKonflikt(prpFlight->Urno, olText, IDS_STRING1550, MOD_ID_ROTATION);
			}
			else
			{
				// IDS_STRING1550  Flugzeugtyp hat sich ge�ndert
				if( CString(prpFlight->Act5) != CString(prpFlight->LastAct5)) 
				{
					olText = olBez + GetString(IDS_STRING1550);
					AddKonflikt(prpFlight->Urno, olText, IDS_STRING1550, MOD_ID_ROTATION);
				}
			}
		
		}

		*****************/
//		olPst = prpFlight->Psta;

	}

	if(prlFlightD)
	{
		////////////////////////////////////////////////////////////////////////////////////////
		if (bgConflictNewFlight)
		{
			// Conflict New flight
			CTime olSto;

			CTime olStart= CTime(olCurr.GetYear(), olCurr.GetMonth(), olCurr.GetDay(),0,0,0);
			CTime olEnd= CTime(olCurr.GetYear(), olCurr.GetMonth(), olCurr.GetDay(),23,59,59);
			if(IsBetween(prlFlightD->Cdat , olStart, olEnd))
			{
				olText.Format("%sSTD: %s   %s (%s %s)", olBezD, prlFlightD->Stod.Format("%H:%M %d.%m.%Y"), GetString(IDS_STRING1805), prlFlightD->Usec, prlFlightD->Cdat.Format("%H:%M"));
				AddKonflikt(KonfIdent(prlFlightD->Urno, 'D', 0 , ' ', IDS_STRING1805), olText, MOD_ID_DIA);
			}
		}


		DIAFLIGHTDATA* prlFlightArr = ogPosDiaFlightData.GetArrival(prlFlightD);

		////////////////////////////////////////////////////////////////////////////////////////
		// Conflict No. 1  start-landing order!  

		if(blCheckD && bmJoinConf)
		{
			if(strcmp(prlFlightD->Rtyp, "E") == 0)
			{
				olText = olBezD + CString("STA: ") + prlFlightD->Stoa.Format("%H:%M %d.%m.%Y  <") + CString(prlFlightD->Regn) + CString(">   ");

				olText += GetString(IDS_STRING1430);//CString("Kein Onblock bei Landung + 15 Minuten");
				AddKonflikt(KonfIdent(prlFlightD->Urno, 'D', 0, ' ', IDS_STRING1430), olText, MOD_ID_DIA);
			}
		}
		
		


		////////////////////////////////////////////////////////////////////////////////////////
		// Conflict No. 8   No Take off time at Offblock + %d Minutes 

		if(blCheckD && bmCurrOfbl)
		{
			if((prlFlightD->Airb == TIMENULL) && (prlFlightD->Ofbl != TIMENULL) && (olCurr > prlFlightD->Ofbl + omCurrOfbl))
			{
				oTmp.Format(GetString(IDS_STRING1239), omCurrOfbl.GetTotalMinutes());
				olText = olBezD + CString("STD: ") + prlFlightD->Stod.Format("%H:%M %d.%m.%Y     ") + oTmp;
				//CString("Keine Startzeit bei Offblock + %d Minuten");
				AddKonflikt(KonfIdent(prlFlightD->Urno, 'D', 0, ' ', IDS_STRING1239), olText, MOD_ID_DIA);
			}
		}

		// Conflict No. 6   STA/ETA > STD/ETD
/*		
		if(*prlFlightD->Adid != 'A') // 'D' or 'B'
		{
			CTime olTmpStd = prlFlightD->Stod;

			if(prlFlightD->Etdi != TIMENULL)
			{
				olTmpStd = prlFlightD->Etdi;
			}
				
			if(((olTmpStd >= prlFlightD->Stoa) && (prlFlightD->Etai == TIMENULL)) ||
			   ((olTmpStd >= prlFlightD->Etai) && (prlFlightD->Etai != TIMENULL)))
			{

				oTmp.Format(GetString(IDS_STRING1432), omStoaStod.GetTotalMinutes());//CString("STA/ETA + %d Min ist gr��er als  STD/ETD");
				olText = olBezD + oTmp;
				
				AddKonflikt(KonfIdent(prlFlightD->Urno, 'D', 0, ' ', IDS_STRING1432), olText, MOD_ID_DIA);
			}
		}
*/
		if (prlFlightArr)
		{
			CTime olTimeA = prlFlightArr->Etai;
			if (olTimeA == TIMENULL)
				olTimeA = prlFlightArr->Stoa;

			if (olTimeA != TIMENULL)
			{
				CTime olTimeD = prlFlightD->Etdi;
				if (olTimeD == TIMENULL)
					olTimeD = prlFlightD->Stod;

				if (olTimeD != TIMENULL)
				{
					if (olTimeA >= olTimeD)
					{
						olText = olBezD + GetString(IDS_STRING1432);
						AddKonflikt(KonfIdent(prlFlightD->Urno, 'D', 0, ' ', IDS_STRING1432), olText, MOD_ID_DIA);
//						AddKonflikt(KonfIdent(prpFlight->Urno, 'D', 0, ' ', IDS_STRING1432), olText, MOD_ID_ROTATION);
					}
				}
			}
		}


		////////////////////////////////////////////////////////////////////////////////////////
		// Conflict No. 9    No Offblock at STD + %d Minutes 
		
		if(bmCurrOfblStod)
		{
			if( prlFlightD->Ftyp[0] != 'X' && prlFlightD->Ftyp[0] != 'N')
			{
				if((prlFlightD->Ofbl == TIMENULL) && (olCurr > prlFlightD->Stod + omCurrOfblStod))
				{
					oTmp.Format(GetString(IDS_STRING1240), omCurrOfblStod.GetTotalMinutes());
					olText = olBezD + CString("STD: ") + prlFlightD->Stod.Format("%H:%M %d.%m.%Y     ") + oTmp;
					//CString("Kein Offblock bei STD + %d Minuten");
					AddKonflikt(KonfIdent(prlFlightD->Urno, 'D', 0, ' ', IDS_STRING1240), olText, MOD_ID_DIA);
				}
			}
		}


		////////////////////////////////////////////////////////////////////////////////////////
		// Conflict No. 10  |ETD - STD| > %d Minutes  
		
		if(blCheckD && bmStodEtdi)
		{
			CTimeSpan olEtdStdDiff = prlFlightD->Etdi - prlFlightD->Stod;
			if((prlFlightD->Etdi != TIMENULL) && (labs(olEtdStdDiff.GetTotalMinutes()) > omStodEtdi.GetTotalMinutes() )  && (prlFlightD->Airb == TIMENULL))
			{
				oTmp.Format(GetString(IDS_STRING1241), omStodEtdi.GetTotalMinutes());
				olText = olBezD + CString("STD: ") + prlFlightD->Stod.Format("%H:%M %d.%m.%Y     ") + oTmp;
				//CString("ETD ist gr��er STD  + 15 Minuten");
				AddKonflikt(KonfIdent(prlFlightD->Urno, 'D', 0, ' ', IDS_STRING1241), olText, MOD_ID_DIA);
			}
		}

		////////////////////////////////////////////////////////////////////////////////////////
		// Conflict No. 11   Actual Time + %d Min > Next Information Time  

		if(blCheckD && bmCurrDNxti)
		{
			if((prlFlightD->Nxti != TIMENULL) && (olCurr + omCurrDNxti > prlFlightD->Nxti) && (prlFlightD->Airb == TIMENULL))
			{

				oTmp.Format(GetString(IDS_STRING1242), omCurrDNxti.GetTotalMinutes());
				olText = olBezD + CString("STD: ") + prlFlightD->Stod.Format("%H:%M %d.%m.%Y     ") + oTmp;
				
				//CString("Aktuelle Zeit > Next Information - Zeit ");
				AddKonflikt(KonfIdent(prlFlightD->Urno, 'D', 0, ' ', IDS_STRING1242), olText, MOD_ID_DIA);
			}
		}

		////////////////////////////////////////////////////////////////////////////////////////
		// Conflict No. 5    Pos: %s  A/C: %s  error (%s) from parking system ! 

		if(bmAderConf)
		{
			if(strlen(prlFlightD->Ader) > 0)
			{
				sprintf(buffer,GetString(IDS_STRING1428), prlFlightD->Pstd, prlFlightD->Act3, prlFlightD->Ader);

				olText = olBezD + CString("STD: ") + prlFlightD->Stod.Format("%H:%M %d.%m.%Y     ") + CString(buffer); //Andocksystem;
				AddKonflikt(KonfIdent(prlFlightD->Urno, 'D', 0, ' ', IDS_STRING1428), olText, MOD_ID_DIA);
			}
		}
		////////////////////////////////////////////////////////////////////////////////////////
		// Conflict No. 14    CTOT > STD/ETD + %d Minutes 
		
		if(blCheckD && bmCurrOfblStod)
		{
			if( prlFlightD->Ftyp[0] != 'X' && prlFlightD->Ftyp[0] != 'N')
			{
				if(prlFlightD->Ofbl == TIMENULL)
				{
					// check only if no OFBL
					CTime olTmp;
					CString olTimeString;

					if (prlFlightD->Etod != TIMENULL)
					{
						olTmp = prlFlightD->Etod;
						olTimeString = "ETD: ";
					}
					else
					{
						olTmp = prlFlightD->Stod;
						olTimeString = "STD: ";
					}

					if((prlFlightD->Slot != TIMENULL) && (prlFlightD->Slot > (olTmp  + omCtotEtod)))
					{
						oTmp.Format(GetString(IDS_STRING2353), omCtotEtod.GetTotalMinutes());
						olText = olBezD + olTimeString + olTmp.Format("%H:%M %d.%m.%Y     ") + oTmp;
						AddKonflikt(KonfIdent(prlFlightD->Urno, 'D', 0, ' ', IDS_STRING2353), olText, MOD_ID_DIA);
					}
				}
			}
		}

		////////////////////////////////////////////////////////////////////////////////////////
		// Conflict No. 15    No Offblock at CTOT + %d Minutes 
		
		if(blCheckD && omOfblCtot.GetTotalMinutes() != 0)
		{
			if( prlFlightD->Ftyp[0] != 'X' && prlFlightD->Ftyp[0] != 'N')
			{
				if((prlFlightD->Slot != TIMENULL) && (prlFlightD->Ofbl == TIMENULL) && (olCurr > prlFlightD->Slot + omOfblCtot))
				{
					oTmp.Format(GetString(IDS_STRING2354), omOfblCtot.GetTotalMinutes());
					olText = olBezD + CString("CTOT: ") + prlFlightD->Slot.Format("%H:%M %d.%m.%Y     ") + oTmp;
					//CString("Kein Offblock bei STD + %d Minuten");
					AddKonflikt(KonfIdent(prlFlightD->Urno, 'D', 0, ' ', IDS_STRING2354), olText, MOD_ID_DIA);
				}
			}
		}


//		olPst = prpFlight->Pstd;
	}  // if(cpFPart == 'D') ...


/*
	if (strcmp(pcgHome, "PVG") != 0)
	{
		if(strcmp(prpFlight->Ftyp, "Z") == 0)
		{
			olText.Format("%sSTD: %s%s%s", olBez, prpFlight->Stod.Format("%H:%M %d.%m.%Y     "), GetString(IDS_STRING1776), CString(prpFlight->Stat).Mid(1,3));
			AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1776), olText, MOD_ID_ROTATION);
		}


		if(strcmp(prpFlight->Ftyp, "B") == 0)
		{
			olText.Format("%sSTD: %s%s%s", olBez, prpFlight->Stod.Format("%H:%M %d.%m.%Y     "), GetString(IDS_STRING1777), CString(prpFlight->Stat).Mid(1,3));
			AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1777), olText, MOD_ID_ROTATION);
		}
	}


	////////////////////////////////////////////////////////////////////////////////////////
	// Conflict No. 1  start-landing order!  

	if(strcmp(prpFlight->Adid, "B") == 0)
	{
		if(bmJoinConf)
		{
			if(strcmp(prpFlight->Rtyp, "E") == 0)
			{
				olText.Format("%sSTA/STD: %s%s%s%s", olBez, prpFlight->Stoa.Format("%H:%M %d.%m.%Y / "), prpFlight->Stod.Format("%H:%M %d.%m.%Y  <"), CString(prpFlight->Regn), CString(">   "));

				olText += GetString(IDS_STRING1430);//CString("start-landing order!");
				AddKonflikt(KonfIdent(prpFlight->Urno, cpFPart, 0, ' ', IDS_STRING1430), olText, MOD_ID_ROTATION);
			}
		}
	}

*/

//qqq30
//	return;



	if(prlFlightA != NULL)
		CheckFlightConf(prlFlightA->Urno, 'A', MOD_ID_DIA);

	if(prlFlightD != NULL)
		CheckFlightConf(prlFlightD->Urno, 'D', MOD_ID_DIA);
	
//	CheckAttentionButton();

}

void Konflikte::CheckGatPosConflicts(DIAFLIGHTDATA *prlFlightA, DIAFLIGHTDATA *prlFlightD, CString& opBezA, CString& opBezD)
{
	bool blCheckA = true;
	bool blCheckD = true;
	if (prlFlightA)
	{
		if (prlFlightA->Ftyp[0] == 'T' || prlFlightA->Ftyp[0] == 'G')
		blCheckA = false;
	}
	if (prlFlightD)
	{
		if (prlFlightD->Ftyp[0] == 'T' || prlFlightD->Ftyp[0] == 'G')
		blCheckD = false;
	}

	if (blCheckA && prlFlightA && prlFlightA->Adid[0] == 'A')
	{
		CStringArray ropBLUE;
		CMapStringToString ropMapBLUE;
		CStringArray ropRED;
		CMapStringToString ropMapRED;

		if ( strlen(prlFlightA->Psta) > 0 && strlen(prlFlightA->Gta1) > 0 )
		{
			if (ogSpotAllocation.GetResBy("GAT", "PST", CString(prlFlightA->Psta),ropBLUE,ropMapBLUE,ropRED,ropMapRED, prlFlightA, false, false, false, true))
			{
				CString olTmp;
				if (!ropMapBLUE.Lookup(CString(prlFlightA->Gta1), olTmp))
					AddKonflikt(KonfIdent(prlFlightA->Urno, 'A', 0, ' ', IDS_STRING2303), opBezA + GetString(IDS_STRING2303), MOD_ID_DIA);
			}
		}

		ropMapBLUE.RemoveAll();
		if(!bgBeltAlocWithoutGates)
		{
			if ( strlen(prlFlightA->Gta1) > 0 && strlen(prlFlightA->Blt1) > 0 )
			{
				if (ogSpotAllocation.GetResBy("BLT", "GAT", CString(prlFlightA->Gta1),ropBLUE,ropMapBLUE,ropRED,ropMapRED, prlFlightA, false, false, false, true))
				{
					CString olTmp;
					if (!ropMapBLUE.Lookup(CString(prlFlightA->Blt1), olTmp))
						AddKonflikt(KonfIdent(prlFlightA->Urno, 'A', 0, ' ', IDS_STRING2304), opBezA + GetString(IDS_STRING2304), MOD_ID_DIA);
				}
			}
		}

		ropMapBLUE.RemoveAll();
		if ( strlen(prlFlightA->Blt1) > 0 && strlen(prlFlightA->Ext1) > 0 )
		{
			if (ogSpotAllocation.GetResBy("EXT", "BLT", CString(prlFlightA->Blt1),ropBLUE,ropMapBLUE,ropRED,ropMapRED, prlFlightA, false, false, false, true))
			{
				CString olTmp;
				if (!ropMapBLUE.Lookup(CString(prlFlightA->Ext1), olTmp))
					AddKonflikt(KonfIdent(prlFlightA->Urno, 'A', 0, ' ', IDS_STRING2305), opBezA + GetString(IDS_STRING2305), MOD_ID_DIA);
			}
		}

		ropBLUE.RemoveAll();
		ropMapBLUE.RemoveAll();
		ropRED.RemoveAll();
		ropMapRED.RemoveAll();
	}
	if (blCheckD && prlFlightD && prlFlightD->Adid[0] == 'D')
	{
		CStringArray ropBLUE;
		CMapStringToString ropMapBLUE;
		CStringArray ropRED;
		CMapStringToString ropMapRED;

		if ( strlen(prlFlightD->Pstd) > 0 && strlen(prlFlightD->Gtd1) > 0 )
		{
			if (ogSpotAllocation.GetResBy("GAT", "PST", CString(prlFlightD->Pstd),ropBLUE,ropMapBLUE,ropRED,ropMapRED, prlFlightD, false, false, false, true))
			{
				CString olTmp;
				if (!ropMapBLUE.Lookup(CString(prlFlightD->Gtd1), olTmp))
					AddKonflikt(KonfIdent(prlFlightD->Urno, 'D', 0, ' ', IDS_STRING2303), opBezD + GetString(IDS_STRING2303), MOD_ID_DIA);
			}
		}

		ropMapBLUE.RemoveAll();
		if ( strlen(prlFlightD->Pstd) > 0 && strlen(prlFlightD->Gtd2) > 0 )
		{
			if (ogSpotAllocation.GetResBy("GAT", "PST", CString(prlFlightD->Pstd),ropBLUE,ropMapBLUE,ropRED,ropMapRED, prlFlightD, false, false, false, true))
			{
				CString olTmp;
				if (!ropMapBLUE.Lookup(CString(prlFlightD->Gtd2), olTmp))
					AddKonflikt(KonfIdent(prlFlightD->Urno, 'D', 0, ' ', IDS_STRING2303), opBezD + GetString(IDS_STRING2303), MOD_ID_DIA);
			}
		}

		ropMapBLUE.RemoveAll();
		if ( strlen(prlFlightD->Gtd1) > 0 && strlen(prlFlightD->Wro1) > 0 )
		{
			if (ogSpotAllocation.GetResBy("WRO", "GAT", CString(prlFlightD->Gtd1),ropBLUE,ropMapBLUE,ropRED,ropMapRED, prlFlightD, false, false, false, true))
			{
				CString olTmp;
				if (!ropMapBLUE.Lookup(CString(prlFlightD->Wro1), olTmp))
					AddKonflikt(KonfIdent(prlFlightD->Urno, 'D', 0, ' ', IDS_STRING2306), opBezD + GetString(IDS_STRING2306), MOD_ID_DIA);
			}
		}


		CCSPtrArray<DIACCADATA> olCcaData;
		ogPosDiaFlightData.omCcaData.GetCcasByFlnu(olCcaData, prlFlightD->Urno);

		ropMapBLUE.RemoveAll();
		CString olCtrPst;
		CString olCtrGat;
		CString olCtrWro;
		CString olKomma (", ");
		CString olPreFix (" - Counter: ");
		int ilCountCtr = olCcaData.GetSize();

		for (int i=0; i<olCcaData.GetSize(); i++)
		{
			DIACCADATA* prlCca = &olCcaData[i];
			if (prlCca)
			{
				bool blPst = false;
				bool blGat = false;
				bool blWro = false;
				ogDataSet.GetGatposCCAGhpDataForFlight(prlCca->Ghpu, prlFlightD, blPst, blGat, blWro);

				CString olCnam = CString(prlCca->Ckic);
				if (blPst && !olCnam.IsEmpty())
					olCtrPst += olCnam + olKomma;
				if (blGat && !olCnam.IsEmpty())
					olCtrGat += olCnam + olKomma;
				if (blWro && !olCnam.IsEmpty())
					olCtrWro += olCnam + olKomma;
			}
		}
		if (!olCtrPst.IsEmpty())
		{
			olCtrPst = olCtrPst.Left(olCtrPst.GetLength() -2 );
			AddKonflikt(KonfIdent(prlFlightD->Urno, 'D', 0, ' ', IDS_STRING2340), opBezD + GetString(IDS_STRING2340) + olPreFix + olCtrPst, MOD_ID_DIA);
		}
		if (!olCtrGat.IsEmpty())
		{
			olCtrGat = olCtrGat.Left(olCtrGat.GetLength() -2 );
			AddKonflikt(KonfIdent(prlFlightD->Urno, 'D', 0, ' ', IDS_STRING2341), opBezD + GetString(IDS_STRING2341) + olPreFix + olCtrGat, MOD_ID_DIA);
		}
		if (!olCtrWro.IsEmpty())
		{
			olCtrWro = olCtrWro.Left(olCtrWro.GetLength() -2 );
			AddKonflikt(KonfIdent(prlFlightD->Urno, 'D', 0, ' ', IDS_STRING2342), opBezD + GetString(IDS_STRING2342) + olPreFix + olCtrWro, MOD_ID_DIA);
		}

//		olCcaData.omData.RemoveAll();
//		olCcaData.ClearAll();

		ropBLUE.RemoveAll();
		ropMapBLUE.RemoveAll();
		ropRED.RemoveAll();
		ropMapRED.RemoveAll();
	}
}





