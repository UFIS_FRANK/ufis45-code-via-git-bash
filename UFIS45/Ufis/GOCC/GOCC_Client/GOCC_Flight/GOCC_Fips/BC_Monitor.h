#if !defined(AFX_BC_MONITOR_H__F4B6DA03_06A0_11D7_80B7_00D0B7E2A467__INCLUDED_)
#define AFX_BC_MONITOR_H__F4B6DA03_06A0_11D7_80B7_00D0B7E2A467__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BC_Monitor.h : header file
//
#include <resrc1.h>
#include <iostream.h>
#include <fstream.h>

/////////////////////////////////////////////////////////////////////////////
// BC_Monitor dialog

class BC_Monitor : public CDialog
{
// Construction
public:
	BC_Monitor(CWnd* pParent = NULL);   // standard constructor
	~BC_Monitor();

// Dialog Data
	//{{AFX_DATA(BC_Monitor)
	enum { IDD = IDD_BROADCASTMONITOR };
	CListBox	m_BcList;
	//}}AFX_DATA

	bool isCreated;
	CString omLastBc;
	void InsertBc(CString opString);
	void InsertCount();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(BC_Monitor)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	long lmCountReRead;
	ofstream of;

	// Generated message map functions
	//{{AFX_MSG(BC_Monitor)
	virtual void OnCancel();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BC_MONITOR_H__F4B6DA03_06A0_11D7_80B7_00D0B7E2A467__INCLUDED_)
