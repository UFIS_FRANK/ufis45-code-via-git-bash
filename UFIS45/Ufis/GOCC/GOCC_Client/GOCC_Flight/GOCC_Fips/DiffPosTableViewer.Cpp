// DiffPosTableViewer.cpp : implementation file
// 
// Modification History: 


#include <stdafx.h>
#include <DiffPosTableViewer.h>
#include <DiffPosTableDlg.h>
#include <CcsGlobl.h>
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include <BasicData.h>
#include <Utils.h>
#include <PosDiagram.h>

#include <resrc1.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif




// Local function prototype
static void FlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// DiffPosTableViewer
//

int DiffPosTableViewer::imTableColCharWidths[DIFFPOSTABLE_COLCOUNT]={9, 5, 5, 2, 3, 5, 5, 5, 5, 3, 7, 9, 5, 5, 2, 3, 5, 5, 5, 5};
 

DiffPosTableViewer::DiffPosTableViewer(DiffPosTableDlg *popParentDlg):
 	romTableHeaderFont(ogCourier_Bold_10), romTableLinesFont(ogCourier_Regular_10),
	fmTableHeaderFontWidth(9), fmTableLinesFontWidth(9)
{
 	// Table header strings
	int i=0;
	omTableHeadlines[i++]=GetString(IDS_STRING296);	// Flight
	omTableHeadlines[i++]=GetString(IDS_STRING323);	// STA
	omTableHeadlines[i++]=GetString(IDS_STRING302);	// ETA(i)
	omTableHeadlines[i++]=GetString(IDS_STRING1131);// D
	omTableHeadlines[i++]=GetString(IDS_STRING298);	// ORG
	omTableHeadlines[i++]=GetString(IDS_STRING1903);// NA(ture)
	omTableHeadlines[i++]=GetString(IDS_STRING308);	// POS
	omTableHeadlines[i++]=GetString(IDS_STRING1939);// Belt1
	omTableHeadlines[i++]=GetString(IDS_STRING1940);// Belt2
	omTableHeadlines[i++]=GetString(IDS_STRING311);	// A/C
	omTableHeadlines[i++]=GetString(IDS_STRING310);	// Reg
	omTableHeadlines[i++]=GetString(IDS_STRING296);	// Flight
	omTableHeadlines[i++]=GetString(IDS_STRING316);	// STD
	omTableHeadlines[i++]=GetString(IDS_STRING317); // ETD(i)
	omTableHeadlines[i++]=GetString(IDS_STRING1131);// D
	omTableHeadlines[i++]=GetString(IDS_STRING315);	// DES
	omTableHeadlines[i++]=GetString(IDS_STRING1903);// NA(ture)
	omTableHeadlines[i++]=GetString(IDS_STRING308);	// POS
	omTableHeadlines[i++]=GetString(IDS_STRING335); // Gate1
	omTableHeadlines[i++]=GetString(IDS_STRING1941);// Lounge
   	// calculate table column widths
	for (i=0; i < DIFFPOSTABLE_COLCOUNT; i++)
	{
 		imTableColWidths[i] = (int) max(imTableColCharWidths[i]*fmTableLinesFontWidth, 
								        omTableHeadlines[i].GetLength()*fmTableHeaderFontWidth);
	}

	pomParentDlg = popParentDlg;
    pomTable = NULL;
}


void DiffPosTableViewer::UnRegister()
{
	ogDdx.UnRegister(this, NOTUSED);
 
}


DiffPosTableViewer::~DiffPosTableViewer()
{
	UnRegister();
    DeleteAll();
}


void DiffPosTableViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}



void DiffPosTableViewer::ChangeViewTo()
{
	if (!pomTable) return;

	UnRegister();

	ogDdx.Register(this, D_FLIGHT_CHANGE, CString("FLIGHTDIAVIEWER"), CString("Flight Changed"), FlightTableCf);
	ogDdx.Register(this, D_FLIGHT_DELETE, CString("FLIGHTDIAVIEWER"), CString("Flight Changed"), FlightTableCf);
	ogDdx.Register(this, D_FLIGHT_INSERT, CString("FLIGHTDIAVIEWER"), CString("Flight Changed"), FlightTableCf);

	// rebuild table
    pomTable->ResetContent();
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}


// how many rotations have different positions?
int DiffPosTableViewer::GetCount() const
{
	return omLines.GetSize();
}





// Has the two flights different positions?
/*
bool DiffPosTableViewer::IsPassFilter(const DIAFLIGHTDATA &rrpFlightA, const DIAFLIGHTDATA &rrpFlightD) const
{
	// NO TOWINGS
	if (!IsRegularFlight(rrpFlightA.Ftyp[0]) || !IsRegularFlight(rrpFlightD.Ftyp[0]))
		return false;

	if (strcmp(rrpFlightA.Psta, rrpFlightD.Pstd) != 0)
		return true;

    return false;
}
*/

bool DiffPosTableViewer::CheckPosInRot(const DIAFLIGHTDATA &rrpFlightA)
{

	// get the whole rotation
	const DiaCedaFlightData::RKEYLIST *prlRkeyList = ogPosDiaFlightData.GetRotationByRkey(rrpFlightA.Rkey);
	if(prlRkeyList == NULL)
	{
		return false;		
	}

	///// search the arrival flight in rotation
	for (int i = 0; i < prlRkeyList->Rotation.GetSize(); i++)
	{
		if (prlRkeyList->Rotation[i].Urno == rrpFlightA.Urno)
			break;
	}

	if (i >= prlRkeyList->Rotation.GetSize())	
	{
		// arrival flight not found in rotations!
		return false;
	}

	const DIAFLIGHTDATA *prlFlightA;
	const DIAFLIGHTDATA *prlFlightD;
	//// check all position to the dep flight
	for (i++; i < prlRkeyList->Rotation.GetSize(); i++)
	{
		prlFlightA = &prlRkeyList->Rotation[i-1];
		prlFlightD = &prlRkeyList->Rotation[i];

		if (strcmp(prlFlightA->Psta, prlFlightD->Pstd) != 0)
			return false;

		if (IsDepartureFlight(prlFlightD->Org3, prlFlightD->Des3, prlFlightD->Ftyp[0]))
			break;
	}


	return true;
}



/////////////////////////////////////////////////////////////////////////////
// DiffPosTableViewer -- code specific to this class

void DiffPosTableViewer::MakeLines()
{
	int ilDummy;
	int ilFlightCount = ogPosDiaFlightData.omData.GetSize();
	const DIAFLIGHTDATA *prlFlight;

	// Scan all loaded flights
	for (int ilLc = 0; ilLc < ilFlightCount; ilLc++)
	{
		prlFlight = &(ogPosDiaFlightData.omData[ilLc]);

		if (IsArrivalFlight(prlFlight->Org3, prlFlight->Des3, prlFlight->Ftyp[0]))
		{
			MakeFlight(prlFlight, NULL, ilDummy);
		}
		if (IsDepartureFlight(prlFlight->Org3, prlFlight->Des3, prlFlight->Ftyp[0]))
		{
			MakeFlight(NULL, prlFlight, ilDummy);
		}
	}

}



bool DiffPosTableViewer::MakeFlight(const DIAFLIGHTDATA *prpAFlight, const DIAFLIGHTDATA *prpDFlight, int &ripLineNo)
{
	long llRkey;
	if (prpAFlight != NULL)
	{
		llRkey = prpAFlight->Rkey;
	}
	else if (prpDFlight != NULL)
	{
		llRkey = prpDFlight->Rkey;
	}
	else
	{
		return false;
	}

	// get the whole rotation
	DiaCedaFlightData::RKEYLIST *prlRkeyList = ogPosDiaFlightData.GetRotationByRkey(llRkey);
	if(prlRkeyList == NULL)
	{
		return false;		
	}

	// get the arrival and departure
	if (prpAFlight == NULL)
	{
		prpAFlight = prlRkeyList->GetArrivalFlight(prpDFlight->Urno);
	}

	if (prpDFlight == NULL)
	{
		prpDFlight = prlRkeyList->GetDepartureFlight(prpAFlight->Urno);
	}

	if (prpAFlight == NULL || prpDFlight == NULL)
		return false;

	// check the positions of all parts of the rotation
	if (!CheckPosInRot(*prpAFlight))
	{
		int ilDummy;
		if (!FindLine(prpAFlight->Urno, prpDFlight->Urno, ilDummy))
		{
			ripLineNo = MakeLine(prpAFlight, prpDFlight);
			return true;
		}
	}
	return false;
}




int DiffPosTableViewer::MakeLine(const DIAFLIGHTDATA *prpAFlight, const DIAFLIGHTDATA *prpDFlight)
{
    DIFFPOSTABLE_LINEDATA rlLine;
	
 	MakeLineData(prpAFlight, prpDFlight, rlLine);
	return CreateLine(rlLine);
 
}



void DiffPosTableViewer::MakeLineData(const DIAFLIGHTDATA *prpAFlight, const DIAFLIGHTDATA *prpDFlight, DIFFPOSTABLE_LINEDATA &rrpLine) const
{
	// for arrival flights
	if (prpAFlight) {
		rrpLine.AUrno = prpAFlight->Urno;
		rrpLine.AFlno = prpAFlight->Flno;
		rrpLine.AStoa = prpAFlight->Stoa;
		rrpLine.AEtai = prpAFlight->Etai;
		rrpLine.AOrg3 = prpAFlight->Org3;
		rrpLine.ATtyp = prpAFlight->Ttyp;
		rrpLine.APos = prpAFlight->Psta;
		rrpLine.ABelt1 = prpAFlight->Blt1;
		rrpLine.ABelt2 = prpAFlight->Blt2;
 
		rrpLine.Regn = prpAFlight->Regn;
		rrpLine.Act3 =  prpAFlight->Act3;
	}

	// for departure flights
	if (prpDFlight) {
		rrpLine.DUrno = prpDFlight->Urno;
		rrpLine.DFlno = prpDFlight->Flno;
		rrpLine.DStod = prpDFlight->Stod;
		rrpLine.DEtdi = prpDFlight->Etdi;
		rrpLine.DDes3 = prpDFlight->Des3;
		rrpLine.DTtyp = prpDFlight->Ttyp;
		rrpLine.DPos = prpDFlight->Pstd;
		rrpLine.DGate1 = prpDFlight->Gtd1;
		rrpLine.DWro = prpDFlight->Wro1;

		rrpLine.Regn = prpDFlight->Regn;
		rrpLine.Act3 =  prpDFlight->Act3;
	}
	
  
	// local times requested?
	if(bgGatPosLocal) UtcToLocal(rrpLine);

    return;
}


// convert all the times in the given line in local time
bool DiffPosTableViewer::UtcToLocal(DIFFPOSTABLE_LINEDATA &rrpLine) const
{
	if (rrpLine.AStoa != TIMENULL)
		ogBasicData.UtcToLocal(rrpLine.AStoa);
	if (rrpLine.AEtai != TIMENULL)
		ogBasicData.UtcToLocal(rrpLine.AEtai);
	if (rrpLine.DStod != TIMENULL)
		ogBasicData.UtcToLocal(rrpLine.DStod);
	if (rrpLine.DEtdi != TIMENULL)
		ogBasicData.UtcToLocal(rrpLine.DEtdi);
 
	return true;
}




int DiffPosTableViewer::CreateLine(DIFFPOSTABLE_LINEDATA &rrpLine)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareLines(rrpLine, omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]

    omLines.NewAt(ilLineno, rrpLine);
	return ilLineno;
}


void DiffPosTableViewer::DeleteLine(int ipLineno)
{
	// delete internal line
	omLines.DeleteAt(ipLineno);
	// delete table line
	pomTable->DeleteTextLine(ipLineno);

}



bool DiffPosTableViewer::FindLine(long lpAUrno, long lpDUrno, int &ripLineno) const
{
	ripLineno = -1;
	// scan all intern lines
    for (int i = omLines.GetSize() - 1; i >= 0; i--)
	{
		if(omLines[i].AUrno == lpAUrno && omLines[i].DUrno == lpDUrno)
		{
			ripLineno = i;
			return true;
		}
	}
	return false;
}


void DiffPosTableViewer::DeleteAll()
{
    omLines.DeleteAll();
}


 

// Load intern data to the table
void DiffPosTableViewer::UpdateDisplay()
{
	pomTable->ResetContent();

	DrawHeader();

	DIFFPOSTABLE_LINEDATA *prlLine;
    
	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		int ilColumnNo = 0;
		prlLine = &omLines[ilLc];
		CCSPtrArray<TABLE_COLUMN> olColList;
	
		MakeColList(prlLine, olColList);

		pomTable->AddTextLine(olColList, (void*)(prlLine));
		olColList.DeleteAll();
	}


    pomTable->DisplayTable();
}



// load the given line from the intern data to the table
void DiffPosTableViewer::InsertDisplayLine( int ipLineNo)
{
	if(!((ipLineNo >= 0) && (ipLineNo < omLines.GetSize())))
		return;
	CCSPtrArray<TABLE_COLUMN> olColList;
	MakeColList(&omLines[ipLineNo], olColList);
	pomTable->InsertTextLine(ipLineNo, olColList, &omLines[ipLineNo]);
	olColList.DeleteAll();
}






void DiffPosTableViewer::DrawHeader()
{
	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);
	pomTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Font = &romTableHeaderFont;

	// for all rows
	for (int i=0; i < DIFFPOSTABLE_COLCOUNT; i++)
	{
		rlHeader.Alignment = COLALIGN_CENTER;
		rlHeader.Length = imTableColWidths[i]; 
		rlHeader.Text = omTableHeadlines[i]; 
		omHeaderDataArray.New(rlHeader);
	}

	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();

}





void DiffPosTableViewer::MakeColList(DIFFPOSTABLE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList)
{
		TABLE_COLUMN rlColumnData;

		rlColumnData.VerticalSeparator = SEPA_NONE;
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.Font = &romTableLinesFont;

		// Arrival
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->AFlno;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

 		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->AStoa.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);

 		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->AEtai.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->AStoa.Format("%d");
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->AOrg3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->ATtyp;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
 
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->APos;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
 
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->ABelt1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->ABelt2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		// Common
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Act3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

 		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Regn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		// Departure
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->DFlno;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

 		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->DStod.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);

 		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->DEtdi.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->DStod.Format("%d");
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->DDes3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->DTtyp;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->DPos;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->DGate1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
 
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->DWro;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

}



// compare function for sorting
int DiffPosTableViewer::CompareLines(const DIFFPOSTABLE_LINEDATA &rrpLine1, const DIFFPOSTABLE_LINEDATA &rrpLine2) const
{
	// Sort order: Arr/Dep/Rot, Stoa, Stod

	// First calculate flight type 
	char clTyp1='R';
	if (rrpLine1.AUrno == 0) clTyp1 = 'D';
	if (rrpLine1.DUrno == 0) clTyp1 = 'A';
	char clTyp2='R';
	if (rrpLine2.AUrno == 0) clTyp2 = 'D';
	if (rrpLine2.DUrno == 0) clTyp2 = 'A';


	// flight type
	if (clTyp1 != clTyp2)
	{
		if (clTyp1 == 'A') return -1;
		if (clTyp2 == 'A') return 1;

		if (clTyp1 == 'R') return -1;
		if (clTyp2 == 'R') return 1;
	}
	// Stoa
	if (rrpLine1.AStoa < rrpLine2.AStoa) return -1;
	if (rrpLine1.AStoa > rrpLine2.AStoa) return 1;
	// Stod
	if (rrpLine1.DStod < rrpLine2.DStod) return -1;
	if (rrpLine1.DStod > rrpLine2.DStod) return 1;
 
	return 0;

}






////////////////////////////////////////////////////////////////////////////////
///  DiffPosTableViewer broadcast functions


static void FlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
	if (vpDataPointer == NULL || popInstance == NULL) 
		return;

    DiffPosTableViewer *polViewer = (DiffPosTableViewer *)popInstance;
	polViewer->ProcessFlightBC(ipDDXType, *(DIAFLIGHTDATA *)vpDataPointer);
}


void DiffPosTableViewer::ProcessFlightBC(int ipDDXType, const DIAFLIGHTDATA &rrpFlight)
{
	if (!pomTable) return;

	const DIAFLIGHTDATA *prlAFlight = &rrpFlight;
	if (!IsArrivalFlight(prlAFlight->Org3, prlAFlight->Des3, prlAFlight->Ftyp[0]))
	{
		// get the arrival part of the rotation
		DiaCedaFlightData::RKEYLIST *prlRkeyList = ogPosDiaFlightData.GetRotationByRkey(rrpFlight.Rkey);
		if(prlRkeyList == NULL)
		{
			return;		
		}
		prlAFlight = prlRkeyList->GetArrivalFlight(rrpFlight.Urno);
		if (prlAFlight == NULL)
		{
			// rebuild table in case of deleted towings
			pomTable->ResetContent();
			DeleteAll();    
			MakeLines();
			UpdateDisplay();
			return;
		}
	}


	if (ipDDXType == D_FLIGHT_CHANGE)
	{
		ProcessFlightChange(*prlAFlight);		
	}

	if (ipDDXType == D_FLIGHT_DELETE)
	{
		ProcessFlightDelete(*prlAFlight);
	}

	if (pogPosDiagram != NULL)
	{
		pogPosDiagram->UpdateGanttButtons();
	}
}


void DiffPosTableViewer::ProcessFlightChange(const DIAFLIGHTDATA &rrpAFlight)
{
	// Delete all lines with the urno of the arrival flight
	ProcessFlightDelete(rrpAFlight);

	// Insert the changed rotation
	InsertFlight(rrpAFlight);

	pomParentDlg->UpdateCaption();

	return;
}







void DiffPosTableViewer::ProcessFlightDelete(const DIAFLIGHTDATA &rrpAFlight)
{
	// Delete all lines with the urno of the changed flight
    for (int ilLineNo = omLines.GetSize() - 1; ilLineNo >= 0; ilLineNo--)
	{
	  if(omLines[ilLineNo].AUrno == rrpAFlight.Urno || omLines[ilLineNo].DUrno == rrpAFlight.Urno)
	  {
		DeleteLine(ilLineNo);
	  }
	}


	pomParentDlg->UpdateCaption();
}



void DiffPosTableViewer::InsertFlight(const DIAFLIGHTDATA &rrpFlight)
{

	// Insert flight
	int ilLineNo = -1;

	if (IsArrivalFlight(rrpFlight.Org3, rrpFlight.Des3, rrpFlight.Ftyp[0]))
	{
		if (MakeFlight(&rrpFlight, NULL, ilLineNo))
			InsertDisplayLine(ilLineNo);
	}
	if (IsDepartureFlight(rrpFlight.Org3, rrpFlight.Des3, rrpFlight.Ftyp[0]))
	{
		if (MakeFlight(NULL, &rrpFlight, ilLineNo))
			InsertDisplayLine(ilLineNo);
	}
	
}











