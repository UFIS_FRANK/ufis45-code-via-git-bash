// BltOverviewTableViewer.cpp : implementation file
// 
// Modification History: 


#include <stdafx.h>
#include <BltOverviewTableViewer.h>
#include <CcsGlobl.h>
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include <math.h>
#include <BasicData.h>
#include <CcaCedaFlightData.h>

#include <resrc1.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif




// Local function prototype
static void FlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// BltOverviewTableViewer
//


int BltOverviewTableViewer::imTableColCharWidths[BLTOVERVIEWTABLE_COLCOUNT]={4, 13, 3, 4, 3, 14, 14, 14, 14, 14};


BltOverviewTableViewer::BltOverviewTableViewer():
	imOrientation(PRINT_PORTRAET),
 	romTableHeaderFont(ogCourier_Bold_10), romTableLinesFont(ogCourier_Regular_9),
	fmTableHeaderFontWidth(10), fmTableLinesFontWidth(7.5)
{
	dgCCSPrintFactor = 3;
 
	// Table header strings
	omTableHeadlines[0]=GetString(IDS_STRING2736);	// Name
	omTableHeadlines[1]=GetString(IDS_STRING296);	// Flight 
	omTableHeadlines[2]=GetString(IDS_STRING2735);	// Id
	omTableHeadlines[3]=GetString(IDS_STRING2741);	// Nat.
	omTableHeadlines[4]=GetString(IDS_STRING2737);	// Org/Des 
	omTableHeadlines[5]=GetString(IDS_STRING1897);	// Open 
	omTableHeadlines[6]=GetString(IDS_STRING1898);  // Close
	omTableHeadlines[7]=GetString(IDS_STRING2738);	// Sta/Std
	omTableHeadlines[8]=GetString(IDS_STRING2739);	// Eta/Etd
	omTableHeadlines[9]=GetString(IDS_STRING2740);	// Onbl/Ofbl 
 
	// calculate table column widths
	for (int i=0; i < BLTOVERVIEWTABLE_COLCOUNT; i++)
	{
 		imTableColWidths[i] = (int) max(imTableColCharWidths[i]*fmTableLinesFontWidth, 
								        omTableHeadlines[i].GetLength()*fmTableHeaderFontWidth);
	}

    pomTable = NULL;
}


void BltOverviewTableViewer::UnRegister()
{
	ogDdx.UnRegister(this, NOTUSED);
 
}

BltOverviewTableViewer::~BltOverviewTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
}

void BltOverviewTableViewer::SetResource(CString opResource)
{
	myResource = opResource;

	if (myResource == "BLT")
		myCaption = GetString(IDS_STRING1934);
	else if (myResource == "GAT")
		myCaption = GetString(IDS_STRING2731);
	else if (myResource == "WRO")
		myCaption = GetString(IDS_STRING2732);
	else if (myResource == "POS")
		myCaption = GetString(IDS_STRING2734);
	else if (myResource == "CCA")
		myCaption = GetString(IDS_STRING2733);
}


void BltOverviewTableViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}



void BltOverviewTableViewer::ChangeViewTo(CString opView)
{
	// Register broadcasts
	ogDdx.UnRegister(this, NOTUSED);
	ogDdx.Register(this, D_FLIGHT_CHANGE, CString("FLIGHTDIAVIEWER"), CString("Flight Changed"), FlightTableCf);
	ogDdx.Register(this, D_FLIGHT_DELETE, CString("FLIGHTDIAVIEWER"), CString("Flight Changed"), FlightTableCf);
	ogDdx.Register(this, D_FLIGHT_INSERT, CString("FLIGHTDIAVIEWER"), CString("Flight Changed"), FlightTableCf);

	ogDdx.Register((void *)this,DIACCA_CHANGE,	CString("DIACCADATA"), CString("Cca-changed"),	FlightTableCf);
	ogDdx.Register((void *)this,CCA_FLIGHT_CHANGE,	CString("DIACCADATA"), CString("Cca-new"),		FlightTableCf);
	ogDdx.Register((void *)this,CCA_FLIGHT_DELETE,	CString("DIACCADATA"), CString("Cca-deleted"),	FlightTableCf);
	// Rebuild table
    pomTable->ResetContent();
    DeleteAll();  
	
	if (myResource == "CCA")
	{
//		return;
//	    MakeLines(&ogCcaDiaFlightData.omCcaData.omData);
	    MakeLines(&ogCcaDiaFlightData.omData);
	}
	else
	    MakeLines(&ogPosDiaFlightData.omData);

	UpdateDisplay();

	
	if (pomParentDlg)
	{
		CString olCaption = GetCaption();
		pomParentDlg->SetWindowText(olCaption);
	}
}



/////////////////////////////////////////////////////////////////////////////
// BltOverviewTableViewer -- code specific to this class

// make intern lines
void BltOverviewTableViewer::MakeLines(CCSPtrArray<DIACCADATA> *popDiaCca)
{
	ASSERT(FALSE);
	return;
	for (int ilLc = 0; ilLc < popDiaCca->GetSize(); ilLc++)
	{
		DIACCADATA* prlCca =  &(*popDiaCca)[ilLc];
		if (prlCca)
		{
			CString olCnam = CString(prlCca->Ckic);
			olCnam.TrimRight();
			CCAFLIGHTDATA* prlFlight = ogCcaDiaFlightData.GetFlightByUrno(prlCca->Flnu);
			if (prlFlight && !olCnam.IsEmpty())
			{
				DIAFLIGHTDATA rlDiaFlight;
				rlDiaFlight.Urno = prlFlight->Urno;
				strcpy(rlDiaFlight.Flno,prlFlight->Flno);
				strcpy(rlDiaFlight.Flti,prlFlight->Flti);
				strcpy(rlDiaFlight.Des3,prlFlight->Des3);
				rlDiaFlight.Stod = prlFlight->Stod;
				rlDiaFlight.Etdi = prlFlight->Etdi;
				rlDiaFlight.Ofbl = prlFlight->Ofbl;
				strcpy(rlDiaFlight.Adid,prlFlight->Adid);
				strcpy(rlDiaFlight.Ftyp,prlFlight->Ftyp);
				Make(&rlDiaFlight, prlCca, false);
			}
		}
	}
}

void BltOverviewTableViewer::MakeLines(CCSPtrArray<CCAFLIGHTDATA> *popFlights)
{
	for (int ilLc = 0; ilLc < popFlights->GetSize(); ilLc++)
	{
		CCAFLIGHTDATA* prlFlight =  &(*popFlights)[ilLc];
		if (prlFlight)
		{
			if (prlFlight)
			{
				DIAFLIGHTDATA rlDiaFlight;
				rlDiaFlight.Urno = prlFlight->Urno;
				strcpy(rlDiaFlight.Flno,prlFlight->Flno);
				strcpy(rlDiaFlight.Flti,prlFlight->Flti);
				strcpy(rlDiaFlight.Des3,prlFlight->Des3);
				rlDiaFlight.Stod = prlFlight->Stod;
				rlDiaFlight.Etdi = prlFlight->Etdi;
				rlDiaFlight.Ofbl = prlFlight->Ofbl;
				strcpy(rlDiaFlight.Adid,prlFlight->Adid);
				strcpy(rlDiaFlight.Ftyp,prlFlight->Ftyp);
				strcpy(rlDiaFlight.Ttyp,prlFlight->Ttyp);
				Make(&rlDiaFlight, NULL, false);
			}
		}
	}
}

void BltOverviewTableViewer::MakeLines(CCSPtrArray<DIAFLIGHTDATA> *popFlights)
{
	DIAFLIGHTDATA *prlFlight;

	int ilFlightCount = popFlights->GetSize();

	// Scan all loaded flights
	for (int ilLc = 0; ilLc < ilFlightCount; ilLc++)
	{
		prlFlight = &(*popFlights)[ilLc];
		Make (prlFlight, NULL, false);
	}
}


int BltOverviewTableViewer::Make(const DIAFLIGHTDATA *prlFlight, DIACCADATA* prpCca, bool bpInsertDisplay /*flase*/)
{		
		if (!prlFlight || CString(";T;G").Find(prlFlight->Ftyp) > 0)
			return -1;

		int ilLineNo;
		if (myResource == "BLT")
		{
			// only arrivals
			//if (strcmp(prlFlight->Des3, pcgHome) != 0) continue;

			// If baggage-belt no. 1 is assigned?
			if (strlen(prlFlight->Blt1) != 0)
			{
				ilLineNo = MakeLine(*prlFlight, 1, NULL, "(A) ");
				if(bpInsertDisplay)
					InsertDisplayLine(ilLineNo);
			}
			// If baggage-belt no. 2 is assigned?
			if (strlen(prlFlight->Blt2) != 0)
			{
				ilLineNo = MakeLine(*prlFlight, 2, NULL, "(A) ");
				if(bpInsertDisplay)
					InsertDisplayLine(ilLineNo);
			}
		}
		else if (myResource == "GAT")
		{
			if (strlen(prlFlight->Gta1) != 0)
			{
				ilLineNo = MakeLine(*prlFlight, 1, NULL, "(A) ");
				if(bpInsertDisplay)
					InsertDisplayLine(ilLineNo);
			}
			if (strlen(prlFlight->Gta2) != 0)
			{
				ilLineNo = MakeLine(*prlFlight, 2, NULL, "(A) ");
				if(bpInsertDisplay)
					InsertDisplayLine(ilLineNo);
			}
			if (strlen(prlFlight->Gtd1) != 0)
			{
				ilLineNo = MakeLine(*prlFlight, 1, NULL, "(D) ");
				if(bpInsertDisplay)
					InsertDisplayLine(ilLineNo);
			}
			if (strlen(prlFlight->Gtd2) != 0)
			{
				ilLineNo = MakeLine(*prlFlight, 2, NULL, "(D) ");
				if(bpInsertDisplay)
					InsertDisplayLine(ilLineNo);
			}
		}
		else if (myResource == "WRO")
		{
			if (strlen(prlFlight->Wro1) != 0)
			{
				ilLineNo = MakeLine(*prlFlight, 1, NULL, "(D) ");
				if(bpInsertDisplay)
					InsertDisplayLine(ilLineNo);
			}
			if (strlen(prlFlight->Wro2) != 0)
			{
				ilLineNo = MakeLine(*prlFlight, 2, NULL, "(D) ");
				if(bpInsertDisplay)
					InsertDisplayLine(ilLineNo);
			}
		}
		else if (myResource == "POS")
		{
			if (strlen(prlFlight->Psta) != 0)
			{
				ilLineNo = MakeLine(*prlFlight, 1, NULL, "(A) ");
				if(bpInsertDisplay)
					InsertDisplayLine(ilLineNo);
			}
			if (strlen(prlFlight->Pstd) != 0)
			{
				ilLineNo = MakeLine(*prlFlight, 2, NULL, "(D) ");
				if(bpInsertDisplay)
					InsertDisplayLine(ilLineNo);
			}
		}
		else if (myResource == "CCA")
		{
			CCSPtrArray<DIACCADATA> olFlightCcaList;
			ogCcaDiaFlightData.omCcaData.GetCcasByFlnu(olFlightCcaList, prlFlight->Urno);
			for(int i = 0; i < olFlightCcaList.GetSize(); i++)
			{
				DIACCADATA*	prlCca = &olFlightCcaList[i];
				if (prlCca)
				{
					CString olCnam = CString(prlCca->Ckic);
					olCnam.TrimRight();
					if (!olCnam.IsEmpty())
					{
						ilLineNo = MakeLine(*prlFlight, 1, prlCca, "(D) ");
						if(bpInsertDisplay)
							InsertDisplayLine(ilLineNo);
					}
				}
			}
		}

		return 0;
}


int BltOverviewTableViewer::MakeLine(const DIAFLIGHTDATA &rrpFlight, int ipBltNo, DIACCADATA* prpCca, CString opAdid)
{
    BLTOVERVIEWTABLE_LINEDATA rlLine;
 
 	MakeLineData(rrpFlight, rlLine, ipBltNo, prpCca, opAdid);
	return CreateLine(rlLine);
 
}



// fill the intern line data
void BltOverviewTableViewer::MakeLineData(const DIAFLIGHTDATA &rrpFlight, BLTOVERVIEWTABLE_LINEDATA &rrpLine, int ipBltNo, DIACCADATA* prpCca, CString opAdid)
{
	rrpLine.FUrno =  rrpFlight.Urno;
	rrpLine.Flti =  rrpFlight.Flti;
	rrpLine.Flno = opAdid + CString(rrpFlight.Flno);
	rrpLine.Ttyp = CString(rrpFlight.Ttyp);

	DIAFLIGHTDATA* temp;

	if(omUrnoMap.Lookup((void *)rrpLine.FUrno,(void *& ) temp) == FALSE)
		omUrnoMap.SetAt((void *)rrpLine.FUrno, temp);


	if (opAdid == "(A) ")
	{
		rrpLine.Org3 = CString(rrpFlight.Org3);
		rrpLine.Stoa = rrpFlight.Stoa;
		rrpLine.Etai = rrpFlight.Etai;
		rrpLine.Onbl = rrpFlight.Onbl;
	}
	else if (opAdid == "(D) ")
	{
		rrpLine.Org3 = CString(rrpFlight.Des3);
		rrpLine.Stoa = rrpFlight.Stod;
		rrpLine.Etai = rrpFlight.Etdi;
		rrpLine.Onbl = rrpFlight.Ofbl;
	}

	// for baggage-belt no. 1
	if (ipBltNo == 1)
	{
		if (myResource == "BLT")
		{
			rrpLine.Belt = CString(rrpFlight.Blt1);
			if (rrpFlight.B1ba != TIMENULL)
				rrpLine.Open = rrpFlight.B1ba;
			else
				rrpLine.Open = rrpFlight.B1bs;

			if (rrpFlight.B1ea != TIMENULL)
				rrpLine.Close = rrpFlight.B1ea;
			else
				rrpLine.Close = rrpFlight.B1es;
		}
		else if (myResource == "GAT" && opAdid == "(A) ")
		{
			rrpLine.Belt = CString(rrpFlight.Gta1);
			if (rrpFlight.Ga1x != TIMENULL)
				rrpLine.Open = rrpFlight.Ga1x;
			else
				rrpLine.Open = rrpFlight.Ga1b;

			if (rrpFlight.Ga1y != TIMENULL)
				rrpLine.Close = rrpFlight.Ga1y;
			else
				rrpLine.Close = rrpFlight.Ga1e;
		}
		else if (myResource == "GAT" && opAdid == "(D) ")
		{
			rrpLine.Belt = CString(rrpFlight.Gtd1);
			if (rrpFlight.Gd1x != TIMENULL)
				rrpLine.Open = rrpFlight.Gd1x;
			else
				rrpLine.Open = rrpFlight.Gd1b;

			if (rrpFlight.Gd1y != TIMENULL)
				rrpLine.Close = rrpFlight.Gd1y;
			else
				rrpLine.Close = rrpFlight.Gd1e;
		}
		else if (myResource == "WRO" && opAdid == "(D) ")
		{
			rrpLine.Belt = CString(rrpFlight.Wro1);
			if (rrpFlight.W1ba != TIMENULL)
				rrpLine.Open = rrpFlight.W1ba;
			else
				rrpLine.Open = rrpFlight.W1bs;

			if (rrpFlight.W1ea != TIMENULL)
				rrpLine.Close = rrpFlight.W1ea;
			else
				rrpLine.Close = rrpFlight.W1es;
		}
		else if (myResource == "POS" && opAdid == "(A) ")
		{
			rrpLine.Belt = CString(rrpFlight.Psta);
			if (rrpFlight.Onbl != TIMENULL)
				rrpLine.Open = rrpFlight.Onbl;
			else
				rrpLine.Open = rrpFlight.Pabs;

			if (rrpFlight.Paea != TIMENULL)
				rrpLine.Close = rrpFlight.Paea;
			else
				rrpLine.Close = rrpFlight.Paes;
		}
		else if (myResource == "CCA" && opAdid == "(D) ")
		{
			if (prpCca)
			{
				rrpLine.Belt = CString(prpCca->Ckic);
				if (prpCca->Ckba != TIMENULL)
					rrpLine.Open = prpCca->Ckba;
				else
					rrpLine.Open = prpCca->Ckbs;

				if (prpCca->Ckea != TIMENULL)
					rrpLine.Close = prpCca->Ckea;
				else
					rrpLine.Close = prpCca->Ckes;
			}
		}
	}
	// for baggage-belt no. 2
	else
	{
		if (myResource == "BLT")
		{
			rrpLine.Belt = CString(rrpFlight.Blt2);
			if (rrpFlight.B2ba != TIMENULL)
				rrpLine.Open = rrpFlight.B2ba;
			else
				rrpLine.Open = rrpFlight.B2bs;

			if (rrpFlight.B2ea != TIMENULL)
				rrpLine.Close = rrpFlight.B2ea;
			else
				rrpLine.Close = rrpFlight.B2es;
		}
		else if (myResource == "GAT" && opAdid == "(A) ")
		{
			rrpLine.Belt = CString(rrpFlight.Gta2);
			if (rrpFlight.Ga2x != TIMENULL)
				rrpLine.Open = rrpFlight.Ga2x;
			else
				rrpLine.Open = rrpFlight.Ga2b;

			if (rrpFlight.Ga2y != TIMENULL)
				rrpLine.Close = rrpFlight.Ga2y;
			else
				rrpLine.Close = rrpFlight.Ga2e;
		}
		else if (myResource == "GAT" && opAdid == "(D) ")
		{
			rrpLine.Belt = CString(rrpFlight.Gtd2);
			if (rrpFlight.Gd2x != TIMENULL)
				rrpLine.Open = rrpFlight.Gd2x;
			else
				rrpLine.Open = rrpFlight.Gd2b;

			if (rrpFlight.Gd2y != TIMENULL)
				rrpLine.Close = rrpFlight.Gd2y;
			else
				rrpLine.Close = rrpFlight.Gd2e;
		}
		else if (myResource == "WRO" && opAdid == "(D) ")
		{
			rrpLine.Belt = CString(rrpFlight.Wro2);
			if (rrpFlight.W2ba != TIMENULL)
				rrpLine.Open = rrpFlight.W2ba;
			else
				rrpLine.Open = rrpFlight.W2bs;

			if (rrpFlight.W2ea != TIMENULL)
				rrpLine.Close = rrpFlight.W2ea;
			else
				rrpLine.Close = rrpFlight.W2es;
		}
		else if (myResource == "POS" && opAdid == "(D) ")
		{
			rrpLine.Belt = CString(rrpFlight.Pstd);
			if (rrpFlight.Pdba != TIMENULL)
				rrpLine.Open = rrpFlight.Pdba;
			else
				rrpLine.Open = rrpFlight.Pdbs;

			if (rrpFlight.Ofbl != TIMENULL)
				rrpLine.Close = rrpFlight.Ofbl;
			else
				rrpLine.Close = rrpFlight.Pdes;
		}
	}
 
	// local times requested?
	if(bgGatPosLocal) UtcToLocal(rrpLine);

    return;
}


// convert all the times in the given line in local time
bool BltOverviewTableViewer::UtcToLocal(BLTOVERVIEWTABLE_LINEDATA &rrpLine)
{
	ogBasicData.UtcToLocal(rrpLine.Open);
	ogBasicData.UtcToLocal(rrpLine.Close);
	ogBasicData.UtcToLocal(rrpLine.Stoa);
	ogBasicData.UtcToLocal(rrpLine.Etai);
	ogBasicData.UtcToLocal(rrpLine.Onbl);

	return true;
}


// create one internal line
int BltOverviewTableViewer::CreateLine(BLTOVERVIEWTABLE_LINEDATA &rrpLine)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareLines(rrpLine, omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]

    omLines.NewAt(ilLineno, rrpLine);
	return ilLineno;
}


void BltOverviewTableViewer::DeleteLine(int ipLineno)
{
	// delete internal line
	omLines.DeleteAt(ipLineno);
	// delete table line
	pomTable->DeleteTextLine(ipLineno);

}



bool BltOverviewTableViewer::FindLine(long lpUrno, int &ripLineno) const
{
	ripLineno = -1;
	// scan all intern lines
    for (int i = omLines.GetSize() - 1; i >= 0; i--)
	{
	  if(omLines[i].FUrno == lpUrno)
	  {
		ripLineno = i;
		return true;
	  }
	}
	return false;
}






/////////////////////////////////////////////////////////////////////////////
// BltOverviewTableViewer - BLTOVERVIEWTABLE_LINEDATA array maintenance

void BltOverviewTableViewer::DeleteAll()
{
    omLines.DeleteAll();
	omUrnoMap.RemoveAll();
}


/////////////////////////////////////////////////////////////////////////////
// BltOverviewTableViewer - display drawing routine


void BltOverviewTableViewer::UpdateDisplay()
{
	pomTable->ResetContent();

	DrawHeader();

	BLTOVERVIEWTABLE_LINEDATA *prlLine;
    
	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		int ilColumnNo = 0;
		prlLine = &omLines[ilLc];
		CCSPtrArray<TABLE_COLUMN> olColList;
	
		MakeColList(*prlLine, olColList);
		// add table line
		pomTable->AddTextLine(olColList, (void*)(prlLine));
		olColList.DeleteAll();
	}


    pomTable->DisplayTable();
}

// Insert new lines for the given flight
void BltOverviewTableViewer::InsertFlight(const DIAFLIGHTDATA &rrpFlight)
{
	Make(&rrpFlight, NULL, true);
	return;
}



void BltOverviewTableViewer::InsertDisplayLine( int ipLineNo)
{
	if(!((ipLineNo >= 0) && (ipLineNo < omLines.GetSize())))
		return;
	CCSPtrArray<TABLE_COLUMN> olColList;
	MakeColList(omLines[ipLineNo], olColList);
	pomTable->InsertTextLine(ipLineNo, olColList, &omLines[ipLineNo]);
	olColList.DeleteAll();
}






void BltOverviewTableViewer::DrawHeader()
{
	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);
	pomTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Font = &romTableHeaderFont;

	// for all rows
	for (int i=0; i < BLTOVERVIEWTABLE_COLCOUNT; i++)
	{
		rlHeader.Alignment = COLALIGN_CENTER;
		rlHeader.Length = imTableColWidths[i]; 
		rlHeader.Text = omTableHeadlines[i]; 
		omHeaderDataArray.New(rlHeader);
	}

	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();

}


// fill the table line with the data of the given intern line
void BltOverviewTableViewer::MakeColList(const BLTOVERVIEWTABLE_LINEDATA &rrpLine, CCSPtrArray<TABLE_COLUMN> &ropColList)
{
	TABLE_COLUMN rlColumnData;

	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &romTableLinesFont;
	
	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.Belt;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);
 
	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.Flno;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.Flti;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.Ttyp;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);
	
	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.Org3;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.Open.Format("%d.%m.%y %H:%M");
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.Close.Format("%d.%m.%y %H:%M");
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.Stoa.Format("%d.%m.%y %H:%M");
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.Etai.Format("%d.%m.%y %H:%M");
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.Onbl.Format("%d.%m.%y %H:%M");
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

}

int BltOverviewTableViewer::GetFlightCount()
{
	return omUrnoMap.GetCount();
}

void BltOverviewTableViewer::SetParentDlg(CDialog* ppParentDlg)
{
	pomParentDlg = ppParentDlg;
}

CString BltOverviewTableViewer::GetCaption()
{
	CString olCaption;
	olCaption.Format(myCaption, omLines.GetSize(), GetFlightCount());
	CString olTimes;
	if (bgGatPosLocal)
		olTimes = GetString(IDS_STRING1921);
	else
		olTimes = GetString(IDS_STRING1920);
	olCaption += " ("+olTimes+")";

	return olCaption;
}

//////////////////////////////////////////////////////////////////
//// Printing routines
 
// Print table to paper
void BltOverviewTableViewer::PrintTableView(void) {

	CCSPrint olPrint(NULL, imOrientation, 45);

	// Set printing fonts
	pomPrintHeaderFont = &olPrint.ogCourierNew_Bold_8;
	pomPrintLinesFont = &olPrint.ogCourierNew_Regular_8;
	fmPrintHeaderFontWidth = 6;
	fmPrintLinesFontWidth = 6;

	// calculate table column widths for printing
	for (int i=0; i < BLTOVERVIEWTABLE_COLCOUNT; i++)
	{
  		imPrintColWidths[i] = (int) max(imTableColCharWidths[i]*fmPrintLinesFontWidth, 
								        omTableHeadlines[i].GetLength()*fmPrintHeaderFontWidth);
	}

	CString olFooter1,olFooter2;
	// Set left footer to: "printed at: <date>"
	CString olCaption = GetCaption();
 	olFooter1.Format("%s %s     %s", GetString(IDS_STRING1481),
		(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")), olCaption);

  	if (olPrint.InitializePrinter(imOrientation) == TRUE)
	{ 
		olPrint.imMaxLines = 54;	// Def. imMaxLines: 57 Portrait / 38 Landscape
		// Calculate number of pages
		const double dlPages = ceil((double)pomTable->GetLinesCount() / (double)(olPrint.imMaxLines - 1));
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		// At first a pagebreak
		olPrint.imLineNo = olPrint.imMaxLines + 1;	
		DOCINFO	rlDocInfo;
		memset(&rlDocInfo, 0, sizeof(DOCINFO));
		rlDocInfo.cbSize = sizeof( DOCINFO );
		rlDocInfo.lpszDocName = GetString(IDS_STRING1934);	
		olPrint.omCdc.StartDoc( &rlDocInfo );
		olPrint.imPageNo = 0;
		// Print all table lines
		for (int ilLc = 0; ilLc < pomTable->GetLinesCount(); ilLc++)
		{
			// Page break
 			if(olPrint.imLineNo >= olPrint.imMaxLines)
			{
				if(olPrint.imPageNo > 0)
				{
					// Set right footer to: "Page: %d"
					olFooter2.Format(GetString(IDS_STRING1199),olPrint.imPageNo, dlPages);
					// print footer
					olPrint.PrintUIFFooter(olFooter1,"",olFooter2);
					olPrint.omCdc.EndPage();
				}
				// print header
				PrintTableHeader(olPrint);
			}				
			// print line
			PrintTableLine(olPrint, ilLc);
		}
		// print footer
		olFooter2.Format(GetString(IDS_STRING1199),olPrint.imPageNo, dlPages);
		olPrint.PrintUIFFooter(olFooter1,"",olFooter2);
		olPrint.omCdc.EndPage();
		olPrint.omCdc.EndDoc();
	}  // if (olPrint.InitializePrin...
 	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}




bool BltOverviewTableViewer::PrintTableHeader(CCSPrint &ropPrint)
{
	ropPrint.omCdc.StartPage();
	ropPrint.imPageNo++;
	ropPrint.imLineNo = 0;
	//double dgCCSPrintFactor = 2.7 ;
	// Headline
// 	olHeadline.Format(GetString(IDS_STRING1934), pomTable->GetLinesCount());
// 	olHeadline.Format(GetString(IDS_STRING1934), omUrnoMap.GetCount());
	CString olHeadline = GetCaption();
	// print page headline
	ropPrint.imLeftOffset = 150;
	ropPrint.PrintUIFHeader("", olHeadline, ropPrint.imFirstLine-10);
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_NOFRAME;
	rlElement.pFont       = pomPrintHeaderFont;

	// Create table headline
	for (int ilCc = 0; ilCc < BLTOVERVIEWTABLE_COLCOUNT; ilCc++)
	{
		rlElement.Length = (int)(imPrintColWidths[ilCc]*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength) 
			rlElement.Length+=igCCSPrintMoreLength; 

		rlElement.Text = omTableHeadlines[ilCc]; 
		
		rlPrintLine.NewAt(rlPrintLine.GetSize(), rlElement);
 	}
	// Print table headline
 	ropPrint.PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();

	return true;
}


 

bool BltOverviewTableViewer::PrintTableLine(CCSPrint &ropPrint, int ipLineNo) {
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	
	//double dgCCSPrintFactor = 2.7 ;

	PRINTELEDATA rlElement;
	rlElement.pFont = pomPrintLinesFont;
	rlElement.FrameTop = PRINT_FRAMETHIN;
 	rlElement.FrameBottom = PRINT_FRAMETHIN;
	rlElement.FrameLeft  = PRINT_FRAMETHIN;
	rlElement.FrameRight = PRINT_FRAMETHIN;

	CString olCellValue;
	// create table line
	for (int ilCc = 0; ilCc < BLTOVERVIEWTABLE_COLCOUNT; ilCc++)
	{
		rlElement.Alignment  = PRINT_LEFT;
 		rlElement.Length = (int)(imPrintColWidths[ilCc]*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength) 
			rlElement.Length+=igCCSPrintMoreLength; 
		// Get printing text from table	
		pomTable->GetTextFieldValue(ipLineNo, ilCc, olCellValue);
		rlElement.Text = olCellValue; 
		
 		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		
	}
	// print table line
	ropPrint.PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll(); 
	
	return true;
}



// Print table to file
bool BltOverviewTableViewer::PrintPlanToFile(CString opFilePath) const
{

	if(opFilePath.GetLength() != 0)
	{
		// Get Seperator for Excel-File
		char pclConfigPath[256];
		char pclTrenner[2];
		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
		    strcpy(pclConfigPath, getenv("CEDA"));

		GetPrivateProfileString(ogAppName, "ExcelSeparator", ";",
		    pclTrenner, sizeof pclTrenner, pclConfigPath);

		CString olCaption;
		olCaption.Format(myCaption, omLines.GetSize(), omUrnoMap.GetCount());
		CString olTimes;
		if (bgGatPosLocal)
			olTimes = GetString(IDS_STRING1921);
		else
			olTimes = GetString(IDS_STRING1920);
		olCaption += " ("+olTimes+")";

		ofstream of;
		of.open(opFilePath.GetBuffer(0), ios::out);

		of << olCaption;
		of << endl;

		// Header
		for (int ilCc = 0; ilCc < BLTOVERVIEWTABLE_COLCOUNT; ilCc++)
		{
			of  << omTableHeadlines[ilCc];
			if (ilCc < BLTOVERVIEWTABLE_COLCOUNT-1)
			{
				of << pclTrenner;
			}
		}
		of << endl;

		CString olCellValue;
		 // Lines
		for (int ilLc = 0; ilLc < pomTable->GetLinesCount(); ilLc++)
		{
			for (int ilCc = 0; ilCc < BLTOVERVIEWTABLE_COLCOUNT; ilCc++)
			{
				// get text from table
				pomTable->GetTextFieldValue(ilLc, ilCc, olCellValue);
				of  << olCellValue;
				if (ilCc < BLTOVERVIEWTABLE_COLCOUNT-1)
				{
					of << pclTrenner;
				}
			}
			of << endl;
		}

		of.close();
 
		return true;
	} // 	if(GetSaveFileName(polOfn) != 0)

	return false;
}





// broadcast-function
static void FlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
	if (vpDataPointer == NULL) 
		return;

    BltOverviewTableViewer *polViewer = (BltOverviewTableViewer *)popInstance;

    if (ipDDXType == D_FLIGHT_CHANGE)
        polViewer->ProcessFlightChange(*(DIAFLIGHTDATA *)vpDataPointer);

	if (ipDDXType == D_FLIGHT_DELETE)
        polViewer->ProcessFlightDelete(*(DIAFLIGHTDATA *)vpDataPointer);

	if (ipDDXType == CCA_FLIGHT_CHANGE) //CCAFLIGHTDATA
	{
        polViewer->ProcessCcaFlightChange((CCAFLIGHTDATA *)vpDataPointer);
	}

	if (ipDDXType == CCA_FLIGHT_DELETE) //CCAFLIGHTDATA
	{
        polViewer->ProcessCcaFlightDelete((CCAFLIGHTDATA *)vpDataPointer);
	}

	if (ipDDXType == DIACCA_CHANGE)
	{
        polViewer->ProcessCcaChange(*(long *)vpDataPointer);
	}
}

void BltOverviewTableViewer::ProcessCcaFlightChange(CCAFLIGHTDATA* rrpFlight)
{
	DIAFLIGHTDATA prlDiaFlight;;
	CCAConvertDIA(rrpFlight, prlDiaFlight);
	if (rrpFlight)
	{
		ProcessFlightChange(prlDiaFlight);
	}
}

void BltOverviewTableViewer::ProcessCcaFlightDelete(CCAFLIGHTDATA* rrpFlight)
{
	DIAFLIGHTDATA prlDiaFlight;;
	CCAConvertDIA(rrpFlight, prlDiaFlight);
	if (rrpFlight)
	{
		ProcessFlightDelete(prlDiaFlight);
	}
}

void BltOverviewTableViewer::ProcessCcaChange(long plpCcaUrno)
{
	DIACCADATA *prlCca =ogCcaDiaFlightData.omCcaData.GetCcaByUrno(plpCcaUrno);
	if(prlCca)
	{
		CCAFLIGHTDATA* prlFlight = ogCcaDiaFlightData.GetFlightByUrno(prlCca->Flnu);
		if (prlFlight)
		{
			DIAFLIGHTDATA prlDiaFlight;;
			CCAConvertDIA(prlFlight, prlDiaFlight);
			ProcessFlightChange(prlDiaFlight);
		}
	}
}


void BltOverviewTableViewer::CCAConvertDIA(CCAFLIGHTDATA* prlFlight, DIAFLIGHTDATA& rlDiaFlight)
{
	if (prlFlight)
	{
		//DIAFLIGHTDATA* rlDiaFlight = new DIAFLIGHTDATA();;
		rlDiaFlight.Urno = prlFlight->Urno;
		strncpy(rlDiaFlight.Flno,prlFlight->Flno,10);
		strncpy(rlDiaFlight.Flti,prlFlight->Flti,2);
		strncpy(rlDiaFlight.Des3,prlFlight->Des3,3);
		rlDiaFlight.Stod = prlFlight->Stod;
		rlDiaFlight.Etdi = prlFlight->Etdi;
		rlDiaFlight.Ofbl = prlFlight->Ofbl;
		strncpy(rlDiaFlight.Adid,prlFlight->Adid,2);
		strncpy(rlDiaFlight.Ftyp,prlFlight->Ftyp,2);
		strncpy(rlDiaFlight.Ttyp,prlFlight->Ttyp,6);

	}
}



void BltOverviewTableViewer::ProcessFlightChange(const DIAFLIGHTDATA &rrpFlight)
{
	int ilTopIndex = pomTable->pomListBox->GetTopIndex();
 
	// Delete all lines with the urno of the changed flight
	ProcessFlightDelete(rrpFlight);

	// Insert the changed flight
	InsertFlight(rrpFlight);

	if (pomParentDlg)
	{
		CString olCaption = GetCaption();
		pomParentDlg->SetWindowText(olCaption);
	}

	if (ilTopIndex < omLines.GetSize())
		pomTable->pomListBox->SetTopIndex(ilTopIndex);

	return;

}



void BltOverviewTableViewer::ProcessFlightDelete(const DIAFLIGHTDATA &rrpFlight)
{
 
	// Delete all lines with the urno of the changed flight
    for (int ilLineNo = omLines.GetSize() - 1; ilLineNo >= 0; ilLineNo--)
	{
	  if(omLines[ilLineNo].FUrno == rrpFlight.Urno)
	  {
		DeleteLine(ilLineNo);
	  }
	}

 	omUrnoMap.RemoveKey((void *)rrpFlight.Urno);

	if (pomParentDlg)
	{
		CString olCaption = GetCaption();
		pomParentDlg->SetWindowText(olCaption);
	}

}




// compare function for sorting
int BltOverviewTableViewer::CompareLines(const BLTOVERVIEWTABLE_LINEDATA &rrpLine1, const BLTOVERVIEWTABLE_LINEDATA &rrpLine2) const
{
	if (rrpLine1.Belt > rrpLine2.Belt) return 1;
	if (rrpLine1.Belt < rrpLine2.Belt) return -1;

	if (rrpLine1.Flno > rrpLine2.Flno) return 1;
	if (rrpLine1.Flno < rrpLine2.Flno) return -1;

	return 0;
}








