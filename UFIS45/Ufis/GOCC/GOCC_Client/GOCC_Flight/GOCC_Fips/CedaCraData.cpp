// CedaCraData.cpp	//PRF 8379 

#include <stdafx.h>
#include <afxwin.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CedaCraData.h>
#include <BasicData.h>


CedaCraData ogCraData;

CedaCraData::CedaCraData()
{
	// Create an array of CEDARECINFO for CraData
	BEGIN_CEDARECINFO(CRADATA,CraDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_LONG(Curn,"CURN")
		FIELD_LONG(Furn,"FURN")
		FIELD_CHAR_TRIM(Aloc,"ALOC")
		FIELD_CHAR_TRIM(Oval,"OVAL")
		FIELD_CHAR_TRIM(Nval,"NVAL")
		FIELD_CHAR_TRIM(Usec,"USEC")
		FIELD_DATE(Cdat,"CDAT")
		FIELD_CHAR_TRIM(Hopo,"HOPO")
	END_CEDARECINFO //(CraData)

	// Copy the record structure
	for (int i=0; i< sizeof(CraDataRecInfo)/sizeof(CraDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&CraDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"CRA");
	pcmFieldList = "URNO,CURN,FURN,ALOC,OVAL,NVAL,USEC,CDAT,HOPO";
}; // end Constructor



CedaCraData::~CedaCraData()
{
	TRACE("CedaCraData::~CedaCraData called\n");
	ClearAll();
}

void CedaCraData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omData.DeleteAll();
}

bool CedaCraData::ReadCraData()
{
	// Select data from the database
	char pclWhere[100] = "";
	char pclCom[10] = "RT";
	if (CedaAction(pclCom, pclWhere) != true)
	{
		return false;
	}
	else
	{
		bool ilRc = true;
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			CRADATA *prlCra = new CRADATA;
			if ((ilRc = GetBufferRecord(ilLc,prlCra)) == true)
			{
				InsertInternal(prlCra);
			}
			else
			{
				delete prlCra;
			}
		}
	}

	return TRUE;
}


bool CedaCraData::InsertInternal(CRADATA *prpCra)
{
	omData.Add(prpCra);
	omUrnoMap.SetAt((void *)prpCra->Urno,prpCra);
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////
bool CedaCraData::UpdateInternal(CRADATA *prpCra)
{
	int ilCount = omData.GetSize();
	for (int i = 0; i < ilCount; i++)
	{
		if (omData[i].Urno == prpCra->Urno)
		{
			omData[i] = *prpCra;
			return true;
		}
	}
	return false;
}

void CedaCraData::DeleteInternal(CRADATA *prpCra)
{
	omUrnoMap.RemoveKey((void *)prpCra->Urno);
	int ilCount = omData.GetSize();
	for (int i = 0; i < ilCount; i++)
	{
		if (omData[i].Urno == prpCra->Urno)
		{
			omData.DeleteAt(i);//Update omData
			break;
		}
	}
}

//--INSERT-------------------------------------------------------------------------------------------------
bool CedaCraData::Insert(CRADATA *prpCra)
{
	prpCra->IsChanged = DATA_NEW;
	if(Save(prpCra) == false) return false; //Update Database
	InsertInternal(prpCra);
    return true;
}


bool CedaCraData::InsertDB(CRADATA *prpCra , bool bpDelete )
{
	prpCra->IsChanged = DATA_NEW;
	Save(prpCra); //Update Database
	if(bpDelete)
		delete prpCra;
    return true;
}



//--UPDATE-------------------------------------------------------------------------------------------------
bool CedaCraData::Update(CRADATA *prpCra)
{
	if (GetCraByUrno(prpCra->Urno) != NULL)
	{
		if (prpCra->IsChanged == DATA_UNCHANGED)
		{
			prpCra->IsChanged = DATA_CHANGED;
		}
		if(Save(prpCra) == false) return false; //Update Database
		UpdateInternal(prpCra);
	}
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------
bool CedaCraData::Delete(long lpUrno)
{
	CRADATA *prlCra = GetCraByUrno(lpUrno);
	if (prlCra != NULL)
	{
		prlCra->IsChanged = DATA_DELETED;
		if(Save(prlCra) == false) return false; //Update Database
		DeleteInternal(prlCra);
	}
    return true;
}

//--READSPECIAL-------------------------------------------------------------------------------------------------
bool CedaCraData::ReadSpecial(CCSPtrArray<CRADATA> &ropList, char *pspWhere)
{
	bool ilRc = true;
	if (pspWhere == NULL)
	{
		if (CedaAction("RT") == false)
		{
			return false;
		}
	}
	else
	{
		if (CedaAction("RT",pspWhere) == false)
		{
			return false;
		}
	}
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		CRADATA *prlCra = new CRADATA;
		if ((ilRc = GetBufferRecord(ilLc,prlCra)) == true)
		{
			ropList.Add(prlCra);
		}
		else
		{
			delete prlCra;
		}
	}
	return true;
}

//--SAVE---------------------------------------------------------------------------------------------------
bool CedaCraData::Save(CRADATA *prpCra)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpCra->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpCra->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpCra);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpCra->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpCra->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpCra);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpCra->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpCra->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}


//--GET-BY-URNO--------------------------------------------------------------------------------------------

CRADATA *CedaCraData::GetCraByUrno(long lpUrno)
{
	CRADATA  *prlCra;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlCra) == TRUE)
	{
		return prlCra;
	}
	return NULL;
}