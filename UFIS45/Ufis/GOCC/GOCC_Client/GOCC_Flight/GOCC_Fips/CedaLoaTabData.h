// HEADER 
#ifndef _CEDALOATABDATA_H_ 
#define _CEDALOATABDATA_H_ 

#include <CCSCedaData.h>

struct LOADATA 
{
	long 	 Flnu;
	long 	 Urno;
	CTime 	 Time;
	char 	 Rmrk[41];
	char 	 Dssn[4];
	char 	 Type[4]; 
	char 	 Styp[4]; 
	char 	 Sstp[4]; 
	char 	 Ssst[4]; 
	char 	 Valu[7]; 
	char 	 Apc3[4]; 

	//DataCreated by this class
	int      IsChanged;

	LOADATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged = DATA_UNCHANGED;
	}

}; // end APXDATA

class CedaLoaTabData : public CCSCedaData
{
public:
	CedaLoaTabData();
	~CedaLoaTabData();

	CCSPtrArray<LOADATA> omData;
    CMapPtrToPtr omUrnoMap;
	CMapPtrToPtr omFlnuMap;
	char pcmFList[512];
	
	void Read( long lpFlightUrno);
	bool ReadFlnus(const CString &ropFlnus); 
	void Clear();


	LOADATA *GetLoa(CString &opName, CString &opDssn = CString("USR"));
	LOADATA *GetLoa(long lpFlnu, CString &opName, CString &opDssn  = CString("USR,LDM,MVT,KRI"));
	LOADATA *GetLoaWAW(CString &opName, CString &opDssn = CString("LDM"), CString &opApc3 = CString("WAW"));
	LOADATA *GetLoaWAW(long lpFlnu, CString &opName, CString &opDssn  = CString("LDM,USR,MVT,KRI"), CString &opApc3 = CString(""));



	bool GetLoaValue(CString &opName, CString &opValue);
	bool GetLoaValue(long lpFlnu, CString &opName, CString &opValue);

	bool GetLoaKey(CString &opName, CString &opType, CString &opStyp, CString &opSstp, CString &opSsst);
	bool GetLoaKey(CString &opName, CString &opType, CString &opStyp, CString &opSstp, CString &opSsst, CString &opApc3);

	bool SetLoaData(LOADATA *prpLoaData, CString &opName, CString &opValue, long lpFlnu);
	bool SetLoaDataWAW(LOADATA *prpLoaData, CString &opName, CString &opValue, long lpFlnu, CString &opApc3 = CString(""));

	//////////////////////////////////////

	bool Save(LOADATA *prpStp);

	void DeleteInternal(LOADATA *prpStp);
	void InsertInternal(LOADATA *prpStp);
	bool UpdateInternal(LOADATA *prpStp);
};

extern CedaLoaTabData ogLoaTabData;

#endif // _CEDALOATABDATA_H_ 
