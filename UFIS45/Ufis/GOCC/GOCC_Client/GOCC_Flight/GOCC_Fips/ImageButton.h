// ImageButton.h: interface for the CImageButton class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_IMAGEBUTTON_H__7E65EF43_9F77_4BC0_9EC4_3318BED5DCA9__INCLUDED_)
#define AFX_IMAGEBUTTON_H__7E65EF43_9F77_4BC0_9EC4_3318BED5DCA9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <CCSDefines.h>
#include  <io.h>
#include  <stdio.h>
#include  <stdlib.h>

/////////////////////////////////////////////////////////////////////////////
// Orginally based on CHoverButton by Niek Albers
// A cool CBitmapButton derived class with 3 states (Up/Down/Hover).
//
// Following Added by Frederick Ackers:
// Movable\Resizeable Button code
// ToolTip support for Resizable\Moving Windows
// Horizontal or Vertical Image layout now possible
// Automatic Image Stretching to fit Window Size
// Draws Regular Button with ToolTip if no bitmap is loaded
// ToolTips now display OnHover even if parent window is not active
/////////////////////////////////////////////////////////////////////////////

class CImageButton : public CBitmapButton
{
	DECLARE_DYNAMIC(CImageButton);

	// Construction
public:
	CImageButton();
	void SetToolTipText(CString spText, BOOL bActivate = TRUE);
	void SetToolTipText(UINT nId, BOOL bActivate = TRUE);
	void SetHorizontal(bool ImagesAreLaidOutHorizontally = FALSE);
	BOOL IsMoveable() { return m_bAllowMove; }
	void SetMoveable(BOOL moveAble = TRUE) { m_bAllowMove = moveAble; }
	void DeleteToolTip();
	void SetFont(CString srtFntName_i, int nSize_i);

// Attributes
protected:
	void ActivateTooltip(BOOL bActivate = TRUE);
	BOOL m_bHover;						// indicates if mouse is over the button
	CSize m_ButtonSize;					// width and height of the button
	CBitmap mybitmap;
	CPoint m_point;
	CRectTracker *m_track;
	BOOL m_bTracking;
	BOOL m_bHorizontal;
	BOOL m_bAllowMove;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CImageButton)
	protected:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	//}}AFX_VIRTUAL

// Implementation
public:
	BOOL LoadBitmap(UINT bitmapid);
	BOOL LoadBitmap(LPCTSTR lpszResourceName);
	BOOL LoadBitmapFromFile(LPCTSTR lpszResourceName, int ipWidth, int ipHeight);
	virtual ~CImageButton();
	void SetBtnId(const int ipBtnId);
	int GetBtnId();
	void SetId(const CString cpId);
	CString GetId();
	void SetHasArr( BOOL bpHasArr );
	BOOL GetHasArr();
	void SetHasDep( BOOL bpHasDep );
	BOOL GetHasDep();

	// Generated message map functions
protected:
	CString m_tooltext;
	CToolTipCtrl* m_ToolTip;
	void InitToolTip();
	//{{AFX_MSG(CImageButton)
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg LRESULT OnMouseLeave(WPARAM wparam, LPARAM lparam);
	afx_msg void OnMouseHover(WPARAM wparam, LPARAM lparam) ;
	afx_msg void OnRButtonDown( UINT nFlags, CPoint point );
	afx_msg void OnLButtonDown( UINT nFlags, CPoint point );
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
private:
	void ShowArrDepIndicator(CDC* popDc, int ipWidth, int ipHeight);
	void DrawButtonWithText(CDC* pDC, const char* pcpText);
	int imBtnId;
	CString cmId;
	CBrush omBrushArr, omBrushDep;
	CRgn omRgnArr, omRgnDep;
	BOOL bmHasInitedRgn;//To indicate that the Region omRgnArr and omRgnDep
	BOOL bmHasArr, bmHasDep;//To indicate to show the Arrival and Departure Indicator on the button
	CFont omFont;

};

/////////////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_IMAGEBUTTON_H__7E65EF43_9F77_4BC0_9EC4_3318BED5DCA9__INCLUDED_)
