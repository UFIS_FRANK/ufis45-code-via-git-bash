// PopupIconMenu.h: interface for the CPopupIconMenu class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_POPUPICONMENU_H__A3468D41_A709_4D29_9A8D_8E8E1F1133A2__INCLUDED_)
#define AFX_POPUPICONMENU_H__A3468D41_A709_4D29_9A8D_8E8E1F1133A2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include <resrc1.h>
/////////////////////////////////////////////////////////////////////////////
// CPopupIconMenu dialog


struct ICONDATA
{
	char	iconChar;
	char    name[30];
	int		iconBmpId;
	int		iconBmpId_Arr,iconBmpId_Dep;
	int		iconBmpId_Arr_Half, iconBmpId_Dep_Half;
	int		iconMsgMapId;
	int		iconMsgMapId_Arr;
	int		iconMsgMapId_Dep;
	int		iconMsgMapId_Arr_Half;
	int		iconMsgMapId_Dep_Half;
	CBitmapButton iconBmpButton;
	CBitmapButton iconBmpButtonR;

	ICONDATA(void)
	{ 
		//memset(this,'\0',sizeof(*this));
		iconChar		= ' ';
		iconBmpId		= 0;
		iconMsgMapId	= 0;
		iconBmpId_Arr	= 0;
		iconBmpId_Dep	= 0;
		iconBmpId_Arr_Half	= 0;
		iconBmpId_Dep_Half	= 0;

		iconMsgMapId_Arr	= 0;
		iconMsgMapId_Dep	= 0;
		iconMsgMapId_Arr_Half	= 0;
		iconMsgMapId_Dep_Half	= 0;

		name[0] = '\0';
	}

};
	

class CPopupIconMenu : public CDialog
{
// Construction
public:
	CPopupIconMenu(CWnd* pParent = NULL);   // standard constructor
	void PostNcDestroy() ;

	//CBitmap m_bmpBitmap;
	//void ShowBitmap(CPaintDC *pdc, CWnd *pWnd);
	//bool ShowMenu( bool bpShow, const int left, const int top, const long lpArrUrno, const long lpDepUrno, const char* pspIconToShow );
	bool ShowMenu( const int ipLeft, const int ipTop, const long lpArrUrno, const char* pspArrIconToShow, const long lpDepUrno, const char* pspDepIconToShow, const int ipMaxWidth );
	bool HideMenu();
// Dialog Data
	//{{AFX_DATA(CPopupIconMenu)
	enum { IDD = IDD_POPUPICONMENU_DLG };
	CString	m_FLINFO;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPopupIconMenu)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CPopupIconMenu)
	afx_msg void OnCheckIn();
	afx_msg void OnMeal();
	afx_msg void OnGate();
	afx_msg void OnSorting();
	afx_msg void OnCleaning();
	afx_msg void OnMeal_Arr();
	afx_msg void OnMeal_ArrHalf();
	afx_msg void OnMeal_Dep();
	afx_msg void OnMeal_DepHalf();
	
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private :
	void ShowIcon( CBitmapButton& omBmpBtn, int &left, const int top,  const int ipBitMapId, const char* pspName, const int ipMsgMapId );
	void ShowIconHalf( CBitmapButton& omBmpBtn, int &left, const int top,  const int ipBitMapId, const char* pspName, const int ipMsgMapId );
	
	void RemoveButton(const int ipMsgMapId);

	ICONDATA omIconData[5];
	char psmArrIconToShow[50];
	char psmDepIconToShow[50];
	int  imLeft,imTop,imMaxWidth;

	CWnd* omParent;
	long omArrUrno,omDepUrno;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.


#endif // !defined(AFX_POPUPICONMENU_H__A3468D41_A709_4D29_9A8D_8E8E1F1133A2__INCLUDED_)
