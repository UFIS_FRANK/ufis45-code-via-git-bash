#if !defined(REPORTFLIGHTDELAYVIEWER_H__INCLUDED_)
#define REPORTFLIGHTDELAYVIEWER_H__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
 
//#pragma warning(disable: 4786)

#include <stdafx.h>
#include <CCSTable.h>
#include <CCSDefines.h>
#include <CViewer.h>
#include <CCSPrint.h>
 
#include <RotationDlgCedaFlightData.h>

#define REPORTFLIGHTDELAY_COLCOUNT 17

struct REPORTFLIGHTDELAY_LINEDATA
{
 	long	Urno;		// Urno des flights
 
 	CString	Flno;		// Flightnumber
	CString Type;		// Typ (Arrival oder Departure)
 	CString	OrgDes;		// Origin/Destination
	CTime	StoaStod;		// Schedule time
 	CTime	OnblOfbl;		// Onblock/Offblock time
	CString	Act3;		// Aircraft type
	CString	Dcd1;		// Delay code 1
	CString	Dtd1;		// Delay time 1
	CString	Dcd2;		// Delay code 2
	CString	Dtd2;		// Delay time 2
    CString	Gta1Gtd1;	// Gate 1
	CString PstaPstd;   //Position 
	CString	Ttyp;		// Nature
	CString	Regn;		// Aircraft registration
    CString Gta2Gtd2;   //Gate2  
    CString  Act5; 
	
	REPORTFLIGHTDELAY_LINEDATA()
	{ 
 		Urno = 0;
 		Flno.Empty();
		Type.Empty();
 		OrgDes.Empty();
		StoaStod = TIMENULL;
		OnblOfbl = TIMENULL;
		Act3.Empty();
		Dcd1.Empty();
		Dtd1.Empty();
		Dcd2.Empty();
		Dtd2.Empty();
		PstaPstd.Empty();
        Gta1Gtd1.Empty();
		Regn.Empty();
	    Ttyp.Empty();
 	    Gta2Gtd2.Empty();
		Act5.Empty();
	
	}
};

 



 
class ReportFlightDelayViewer : public CViewer
{
// Constructions
public:
    ReportFlightDelayViewer();
    ~ReportFlightDelayViewer();

	// Connects the viewer with a table
    void Attach(CCSTable *popTable);
	// Load the intern line data from the given data and displays the table
    void ChangeViewTo(const CCSPtrArray<ROTATIONDLGFLIGHTDATA> &ropData, char *popDateStr, char *popAirline, char *popDelayCode, int ipDelayTime);
 
	// Rebuild the table from the intern data
 	void UpdateDisplay(void);
	// Print table to paper
	void PrintTableView(void);
	// Print table to file
	bool PrintPlanToFile(char *popFilePath);
	
	int GetFlightCount(void) const;

	CString omTableName;
	CString omPrintName;
	CString omFileName;
 	
private:

	// columns width of the table in char
	static int imTableColCharWidths[REPORTFLIGHTDELAY_COLCOUNT];
	// columns width of the table in pixels
	int imTableColWidths[REPORTFLIGHTDELAY_COLCOUNT];
	int imPrintColWidths[REPORTFLIGHTDELAY_COLCOUNT];
	// table header strings
	CString omTableHeadlines[REPORTFLIGHTDELAY_COLCOUNT];


	const int imOrientation;

	// Table fonts and widths for displaying
	CFont &romTableHeaderFont; 
	CFont &romTableLinesFont;
	const float fmTableHeaderFontWidth; 
	const float fmTableLinesFontWidth;

	// Table fonts and widths for printing
	CFont *pomPrintHeaderFont; 
	CFont *pomPrintLinesFont;
	float fmPrintHeaderFontWidth; 
	float fmPrintLinesFontWidth;

	
	void DrawTableHeader(void);
	// Transfer the data of the database to the intern data structures
	void MakeLines(const CCSPtrArray<ROTATIONDLGFLIGHTDATA> &ropData);
	// Copy the data from the db-record to the table-record
	bool MakeLineData(REPORTFLIGHTDELAY_LINEDATA &rrpLineData, const ROTATIONDLGFLIGHTDATA &rrpFlight);
	// Create a intern table data record
	int  CreateLine(const REPORTFLIGHTDELAY_LINEDATA &rrpLine);
	// Fills one row of the table
	bool MakeColList(const REPORTFLIGHTDELAY_LINEDATA &rrpLine, CCSPtrArray<TABLE_COLUMN> &ropColList) const;
	// Compares two lines of the table
	int CompareLines(const REPORTFLIGHTDELAY_LINEDATA &rrpLine1, const REPORTFLIGHTDELAY_LINEDATA &rrpLine2) const;

	bool PrintTableHeader(CCSPrint &ropPrint);

 	bool PrintTableLine(CCSPrint &ropPrint, int ipLineNo);
	// delete intern table lines
    void DeleteAll(void);
	// Date string
	char *pomDateStr;
	char *pomAirline;
	char *pomDelayCode;
	int ipmDelayTime;
	int imNoArr;
	int imNoDep;
	CString omFooterName; 

 	// Table
 	CCSTable *pomTable;

	// Table Data
	CCSPtrArray<REPORTFLIGHTDELAY_LINEDATA> omLines;
 
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(REPORTFLIGHTDELAYVIEWER_H__INCLUDED_)
