#ifndef _SeasonDlgCedaFlightData_H_
#define _SeasonDlgCedaFlightData_H_
 
#include <BasicData.h>
#include <CCSGlobl.h>
#include <CCSDefines.h>
#include <CedaCcaData.h>
#include <DiaCedaFlightData.h>

//void ProcessFlightCf(void *vpInstance,enum enumDDXTypes ipDDXType, void *vpDataPointer, CString &ropInstanceName);




/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

//@Man: SEASONDLGFLIGHTDATAStruct
//@See: SeasonDlgCedaFlightData
/*@Doc:
  A structure for reading flight data. We read all data from database (or get 
  the data from AFTLSG) into this struct and store the data in omData.

*/

struct SEASONDLGFLIGHTDATA 
{
	char 	 Vial[1026]; 	// Liste der Zwischenstationen/-zeiten
	char 	 Act3[5]; 	// Flugzeug-3-Letter Code (IATA)
	char 	 Act5[7]; 	// Flugzeug-5-Letter Code
	char 	 Alc2[4]; 	// Fluggesellschaft (Airline 2-Letter Code)
	char 	 Alc3[5]; 	// Fluggesellschaft (Airline 3-Letter Code)
	CTime 	 B1bs; 	// Belegung Gep�ckband 1 geplanter Beginn
	CTime 	 B1es; 	// Belegung Gep�ckband 1 geplantes Ende
	CTime 	 B2bs; 	// Belegung Gep�ckband 2 geplanter Beginn
	CTime 	 B2es; 	// Belegung Gep�ckband 2 geplantes Ende
	char 	 Blt1[7]; 	// Gep�ckband 1
	char 	 Blt2[7]; 	// Gep�ckband 2
	CTime 	 Cdat; 	// Erstellungsdatum
	char 	 Ckif[7]; 	// Check-In From
	char 	 Ckit[7]; 	// Check-In To
	char 	 Des3[5]; 	// Bestimmungsflughafen 3-Lettercode
	char 	 Des4[6]; 	// Bestimmungsflughafen 4-Lettercode
	char 	 Dooa[3]; 	// Flugtag der Ankunft
	char 	 Dood[3]; 	// Flugtag des Abflugs
	char	 Dssf[129];  // Data Source Status Flag
	CTime 	 Etai; 	// ETA-Intern (Beste Zeit)
	CTime 	 Etau; 	// ETA- Anwender
	CTime 	 Etdi; 	// ETD-Intern (Beste Zeit)
	CTime 	 Etdu; 	// ETD-Anwender
	CTime 	 Etoa; 	// ETA-Puplikumsanzeige
	CTime 	 Etod; 	// ETD-Publikumsanzeige
	char 	 Ext1[7]; 	// Ausgang 1 Ankunft
	char 	 Ext2[7]; 	// Ausgang 2 Ankunft
	CTime 	 Fdat; 	// Fluko-Datum
	char 	 Flno[11]; 	// komplette Flugnummer (Airline, Nummer, Suffix)
	char 	 Flns[3]; 	// Suffix
	char 	 Fltn[7]; 	// Flugnummer
	char 	 Ftyp[3]; 	// Type des Flugs [F, P, R, X, D, T, ...]
	CTime 	 Ga1b; 	// Belegung Gate 1 Ankunft geplanter Beginn
	CTime 	 Ga1e; 	// Belegung Gate 1 Ankunft geplantes Ende
	CTime 	 Ga2b; 	// Belegung Gate 2 Ankunft geplanter Beginn
	CTime 	 Ga2e; 	// Belegung Gate 2 Ankunft geplantes Ende
	CTime 	 Gd1b; 	// Belegung Gate 1 Abflug geplanter Beginn
	CTime 	 Gd1e; 	// Belegung Gate 1 Abflug geplantes Ende
	CTime 	 Gd2b; 	// Belegung Gate 2 Abflug geplanter Beginn
	CTime 	 Gd2e; 	// Belegung Gate 2 Abflug geplantes Ende
	char 	 Gta1[7]; 	// Gate 1 Ankunft
	char 	 Gta2[7]; 	// Gate 2 Ankunft
	char 	 Gtd1[7]; 	// Gate 1 Abflug
	char 	 Gtd2[7]; 	// Doppelgate Abflug
	char 	 Htyp[4]; 	// Abfertigungsart
	char 	 Isre[3]; 	// Bemerkungskennzeichen intern
	char 	 Jcnt[3]; 	// Anzahl verbundene Flugnummern
	char 	 Jfno[112]; 	// Verbundene Flugnummern (max. 10)
	CTime 	 Lstu; 	// Datum letzte �nderung
	char 	 Ming[6]; 	// Mindestbodenzeit
	char 	 Org3[5]; 	// Ausgangsflughafen 3-Lettercode
	char 	 Org4[6]; 	// Ausgangsflughafen 4-Lettercode
	CTime 	 Pabs; 	// Belegung Position Ankunft geplanter Beginn
	CTime 	 Paes; 	// Belegung Position Ankunft geplantes Ende
	CTime 	 Pdbs; 	// Belegung Position Abflug geplanter Beginn
	CTime 	 Pdes; 	// Belegung Position Abflug geplantes Ende
	char 	 Psta[7]; 	// Position Ankunft
	char 	 Pstd[7]; 	// Position Abflug
	char 	 Regn[13]; 	// LFZ-Kennzeichen
	char 	 Rem1[258]; 	// Bemerkung zum Flug
	char 	 Rem2[258]; 	// Bemerkung zum Flug
	long 	 Rkey; 	// Rotationsschl�ssel
	char 	 Rtyp[3]; 	// Verkettungstyp (Art/Quelle der Verkettung)
	char 	 Seas[8]; 	// Saisonname
	long 	 Skey; 	// Eindeutiger Flugschl�ssel (Saisonschl.)
	char 	 Stat[12]; 	// Status
	char 	 Stev[3]; 	// Statistikkennung
	char 	 Stht[3]; 	// Datenstatus HTYP
	CTime 	 Stoa; 	// Planm��ige Ankunftszeit STA
	CTime 	 Stod; 	// Planm��ige Abflugzeit
	char 	 Sttt[3]; 	// Datenstatus TTYP
	char 	 Styp[4]; 	// Service Typ (Fluko)
	char 	 Tet1[3]; 	// Terminal Ausgang 1 Ankunft
	char 	 Tet2[3]; 	// Terminal Ausgang 2 Ankunft
	char 	 Tga1[3]; 	// Terminal Gate 1 Ankunft
	char 	 Tga2[3]; 	// Terminal Gate 2 Ankunft
	char 	 Tgd1[3]; 	// Terminal Gate 1 Abflug
	char 	 Tgd2[3]; 	// Terminal Doppelgate Abflug
	CTime 	 Tifa; 	// Zeitrahmen Ankunft
	CTime 	 Tifd; 	// Zeitrahmen Abflug
	char 	 Tisa[3]; 	// Statusdaten f�r TIFA
	char 	 Tisd[3]; 	// Statusdaten f�r TISD
	char 	 Tmb1[3]; 	// Terminal Gep�ckband 1
	char 	 Tmb2[3]; 	// Terminal Gep�ckband 2
	char 	 Ttyp[7]; 	// Verkehrsart
	char 	 Twr1[3]; 	// Warteraum Terminal
	long 	 Urno; 	// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	char 	 Vers[22]; 	// Flugplanversion
	char 	 Vian[4]; 	// Anzahl der Zwischenstationen - 1
	CTime 	 W1bs; 	// Belegung Warteraum 1 geplanter Beginn
	CTime 	 W1es; 	// Belegung Warteraum 1 geplantes Ende
	char 	 Wro1[7]; 	// Warteraum
	CTime 	 B1ba; 	// Belegung Gep�ckband 1 aktueller Beginn
	CTime 	 B1ea; 	// Belegung Gep�ckband 1 aktuelles Ende
	CTime 	 B2ba; 	// Belegung Gep�ckband 2 aktueller Beginn
	CTime 	 B2ea; 	// Belegung Gep�ckband 2 aktuelles Ende
	CTime 	 Ga1x; 	// Belegung Gate 1 Ankunft aktueller Beginn
	CTime 	 Ga1y; 	// Belegung Gate 1 Ankunft aktuelles Ende
	CTime 	 Ga2x; 	// Belegung Gate 2 Ankunft aktueller Beginn
	CTime 	 Ga2y; 	// Belegung Gate 2 Ankunft aktuelles Ende
	CTime 	 Gd1x; 	// Belegung Gate 1 Abflug aktueller Beginn
	CTime 	 Gd1y; 	// Belegung Gate 1 Abflug aktuelles Ende
	CTime 	 Gd2x; 	// Belegung Gate 2 Abflug aktueller Beginn
	CTime 	 Gd2y; 	// Belegung Gate 2 Abflug aktuelles Ende
	CTime 	 Paba; 	// Belegung Position Ankunft aktueller Beginn
	CTime 	 Paea; 	// Belegung Position Ankunft aktuelles Ende
	CTime 	 Pdba; 	// Belegung Position Abflug aktueller Beginn
	CTime 	 Pdea; 	// Belegung Position Abflug aktuelles Ende
	char	 Via3[4];//Via 3-Letter Code
	char 	 Via4[5]; 	// Letzte Station vor Ankunft - Abflug
	CTime 	 W1ba; 	// Belegung Warteraum 1 aktueller Beginn
	CTime 	 W1ea; 	// Belegung Warteraum 1 Aktuelles Ende
	char 	 Nose[5]; 	// Belegung Warteraum 1 Aktuelles Ende
	char 	 Cht3[5]; 	// Belegung Warteraum 1 Aktuelles Ende
	char 	 Remp[6]; 	// Public Remark FIDS
	char 	 Baz1[7]; 	// Public Remark FIDS
	char 	 Baz4[7]; 	// Public Remark FIDS
	CTime	 Bao1;
	CTime	 Bac1;
	CTime	 Bao4;
	CTime	 Bac4;
	char 	 Flti[3]; 	// Veranstalter 
	char 	 Adid[3]; 	// Veranstalter 
	CTime 	 Ofbl; 	// Offblock- Zeit (Beste Zeit)
	CTime 	 Onbl; 	// Onblock-Zeit (Beste Zeit)
	char 	 Csgn[10]; 	// Call- Sign
	char 	 Wro2[7]; 	// Warteraum2
	CTime 	 W2bs; 	// Belegung Warteraum 2 geplanter Beginn
	CTime 	 W2es; 	// Belegung Warteraum 2 geplantes Ende
	CTime 	 W2ba; 	// Belegung Warteraum 2 aktueller Beginn
	CTime 	 W2ea; 	// Belegung Warteraum 2 Aktuelles Ende
//	char 	 Twr2[3]; 	// Warteraum Terminal
	char 	 Stea[3]; 	// Datenstatus ETAI
	char 	 Sted[3]; 	// Datenstatus ETDI
	char 	 Baa4[5]; 	// Def. Groundtime
	char 	 Baa5[5]; 	// Def. Groundtime
//	char 	 Bad4[5]; 	// Def. Groundtime
//	char 	 Bad5[5]; 	// Def. Groundtime
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Gd2d[2]; 	// Protokollierungskennzeichen
	char 	 Ckf2[6];	// CKI second set (Terminal 2 CKI for mixed flights in Lisbon) 	
	char 	 Ckt2[6]; 	// CKI second set (Terminal 2 CKI for mixed flights in Lisbon)

	SEASONDLGFLIGHTDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		B1bs = -1;
		B1es = -1;
		B2bs = -1;
		B2es = -1;
		Cdat = -1;
		Etai = -1;
		Etau = -1;
		Etdi = -1;
		Etdu = -1;
		Etoa = -1;
		Etod = -1;
		Fdat = -1;
		Ga1b = -1;
		Ga1e = -1;
		Ga2b = -1;
		Ga2e = -1;
		Gd1b = -1;
		Gd1e = -1;
		Gd2b = -1;
		Gd2e = -1;
		Lstu = -1;
		Pabs = -1;
		Paes = -1;
		Pdbs = -1;
		Pdes = -1;
		Stoa = -1;
		Stod = -1;
		Tifa = -1;
		Tifd = -1;
		W1bs = -1;
		W1es = -1;
		B1ba = -1;
		B1ea = -1;
		B2ba = -1;
		B2ea = -1;
		Ga1x = -1;
		Ga1y = -1;
		Ga2x = -1;
		Ga2y = -1;
		Gd1x = -1;
		Gd1y = -1;
		Gd2x = -1;
		Gd2y = -1;
		Paba = -1;
		Paea = -1;
		Pdba = -1;
		Pdea = -1;
		W1ba = -1;
		W1ea = -1;
		Ofbl = -1;
		Onbl = -1;
		W2bs = -1; 	// Belegung Warteraum 2 geplanter Beginn
		W2es = -1; 	// Belegung Warteraum 2 geplantes Ende
		W2ba = -1; 	// Belegung Warteraum 2 aktueller Beginn
		W2ea = -1; 	// Belegung Warteraum 2 Aktuelles Ende
	}

	SEASONDLGFLIGHTDATA(const DIAFLIGHTDATA &rrpFlight);

	bool CheckBltOpeningTime(int ipBltNo, CString &ropText) const;
	bool CheckBltClosingTime(int ipBltNo, CString &ropText) const;
}; // end SEASONDLGFLIGHTDATA

struct SFLIGHTCHANGE
{
	SEASONDLGFLIGHTDATA Flight1;
	SEASONDLGFLIGHTDATA Flight2;
};


enum
{
	DIADATA = 0
};

/////////////////////////////////////////////////////////////////////////////
// Class declaration

//@Man:
//@Memo: Read and write flight data
//@See: CedaData
/*@Doc:
  Reads and writes flight data from and to database. Stores flight data in memory 
  and provides methods for searching and retrieving flights from its internal list. 
  Does some data preparations like pre-formatting the flight number, storing flight 
  number and gate information for the corresponding inbound/outbound flight.

  Data of this class has to be updated by CEDA through broadcasts. This procedure 
  and the necessary methods will be described later with the description of 
  broadcast handling.

  {\bf SeasonDlgCedaFlightData} handles data from the EIOHDL server process.
	See the specification for table descriptions.
*/
class SeasonDlgCedaFlightData: public CCSCedaData
{
// Attributes
public:
    //@ManMemo: A map, containing the Keya/Keyd fields of all loaded flights.
    //CMapStringToPtr omKeyMap;
    CMapPtrToPtr omUrnoMap;
	long lmRkey;
	long lmCalledBy;
	bool bmWithGround;

   //@ManMemo: SEASONDLGFLIGHTDATA records read by ReadAllFlight().
    CCSPtrArray<SEASONDLGFLIGHTDATA> omData;
	SFLIGHTCHANGE rmFlights;

	char pcmAftFieldList[1024];

	CString omFtyps;
	CTime omFrom;
	CTime omTo;


// Operations
public:

    SeasonDlgCedaFlightData();
	~SeasonDlgCedaFlightData();
	
	void Register();
	void UnRegister();

	void ClearAll(void);

   

    // internal data access.
	bool AddFlightInternal(SEASONDLGFLIGHTDATA *prpFlight);
	bool DeleteFlightInternal(long lpUrno);

	bool DeleteFlight(char *pcpSelection);

	bool ReadFlights( char *pcpSelection);

	bool ReadSpecial( CCSPtrArray<SEASONDLGFLIGHTDATA> *popFlights, char *pcpSelection, char *pcpFields);

	bool ReadRotation(long lpRkey = -1, long lpCalledBy = -1, bool bpWithGround = false);

	bool SetRotation(DIAFLIGHTDATA *prpDiaFlightA, DIAFLIGHTDATA *prpDiaFlightD);

	void SetPreSelection(CTime &opFrom, CTime &opTo, CString &opFtyps);
	bool PreSelectionCheck(CString &opData, CString &opFields);

	bool AddSeasonFlights(CTime opFromdate, CTime opToDate, CString opADaySel, CString opDDaySel, CString ipFreq, CTime opARefDat, CTime opDRefDat, SEASONDLGFLIGHTDATA *prpAFlight, SEASONDLGFLIGHTDATA *prpDFlight, CCSPtrArray<CCADATA> &opCca );

	CString CheckDupFlights(CTime opFromdate, CTime opToDate, CString opADaySel, CString opDDaySel, CString ipFreq, CTime opARefDat, CTime opDRefDat, SEASONDLGFLIGHTDATA *prpAFlight, SEASONDLGFLIGHTDATA *prpDFlight, CCSPtrArray<CCADATA> &opCca );

	bool UpdateFlight(SEASONDLGFLIGHTDATA *prpFlight, SEASONDLGFLIGHTDATA *prpFlightSave, int ipToMod = -1);
 		

    //@ManMemo: Handle Broadcasts for flights.
	void ProcessFlightBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	bool ProcessFlightUFR(BcStruct *prlBcStruct);
	bool ProcessFlightUPS(BcStruct *prlBcStruct);
	
	CString CreateFlno(CString opAlc, CString opFltn, CString opFlns);
	
	SEASONDLGFLIGHTDATA *GetArrival(SEASONDLGFLIGHTDATA *prpFlight);
	SEASONDLGFLIGHTDATA *GetDeparture(SEASONDLGFLIGHTDATA *prpFlight);
	SEASONDLGFLIGHTDATA *GetJoinFlight(SEASONDLGFLIGHTDATA *prpFlight);

	int GetJfnoArray(CCSPtrArray<JFNODATA> *opJfno, SEASONDLGFLIGHTDATA *prpFlight);
	int GetViaArray(CCSPtrArray<VIADATA> *opVias, SEASONDLGFLIGHTDATA *prpFlight);
	bool AddToKeyMap(SEASONDLGFLIGHTDATA *prpFlight );
	bool DeleteFromKeyMap(SEASONDLGFLIGHTDATA *prpFlight );
	SEASONDLGFLIGHTDATA *GetFlightByUrno(long lpUrno);

	void ReadRotationRAC(void *vpDataPointer);
	void ReloadCcaSBC(void *vpDataPointer);

	bool GetDefAllocForeCast(CString& opCmd, CString& opData, CString& opFields, CStringArray& opReqData, CStringArray& opReqFields);
};


#endif
