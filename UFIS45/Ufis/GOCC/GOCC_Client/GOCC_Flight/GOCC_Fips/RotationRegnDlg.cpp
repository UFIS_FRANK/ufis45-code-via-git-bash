// RotationRegnDlg.cpp : implementation file
//

#include <stdafx.h>
#include <fpms.h>
#include <RotationRegnDlg.h>

#include <BasicData.h>
#include <RotationActDlg.h> 

#include <CCSGlobl.h>
#include <CedaBasicData.h>
#include <AwBasicDataDlg.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void RegnCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);



/////////////////////////////////////////////////////////////////////////////
// RotationRegnDlg dialog


RotationRegnDlg::RotationRegnDlg(CWnd* pParent /*=NULL*/, CString opRegn, CString opAct3 , CString opAct5)
	: CDialog(RotationRegnDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(RotationRegnDlg)
	m_Act3 = _T("");
	m_Act5 = _T("");
	m_Regn = _T("");
	//}}AFX_DATA_INIT

	m_Act3 = opAct3;
	m_Act5 = opAct5;
	m_Regn = opRegn;
    ogDdx.Register(this, APP_EXIT, CString("AWDLG"), CString("Appli. exit"), RegnCf);
}



RotationRegnDlg::~RotationRegnDlg()
{
	ogDdx.UnRegister(this,NOTUSED);
}



static void RegnCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    RotationRegnDlg *polDlg = (RotationRegnDlg *)popInstance;

    if (ipDDXType == APP_EXIT)
        polDlg->AppExit();

}

void RotationRegnDlg::AppExit()
{
	EndDialog(IDCANCEL);
}



void RotationRegnDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(RotationRegnDlg)
	DDX_Control(pDX, IDC_REGN, m_CE_Regn);
	DDX_Control(pDX, IDC_ACT5, m_CE_Act5);
	DDX_Control(pDX, IDC_ACT35LIST, m_CB_Act35List);
	DDX_Control(pDX, IDC_ACT3, m_CE_Act3);
	DDX_Text(pDX, IDC_ACT3, m_Act3);
	DDX_Text(pDX, IDC_ACT5, m_Act5);
	DDX_Text(pDX, IDC_REGN, m_Regn);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(RotationRegnDlg, CDialog)
	//{{AFX_MSG_MAP(RotationRegnDlg)
	ON_BN_CLICKED(IDC_ACT35LIST, OnAct35list)
	ON_MESSAGE(WM_EDIT_KILLFOCUS, OnEditKillfocus)
	ON_MESSAGE(WM_EDIT_CHANGED, OnEditChanged)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// RotationRegnDlg message handlers

void RotationRegnDlg::OnOK() 
{

	m_CE_Act3.GetWindowText(m_Act3);
	m_CE_Act5.GetWindowText(m_Act5);
	m_CE_Regn.GetWindowText(m_Regn);


	m_Act3.TrimRight();
	if( ( m_Act3.IsEmpty() && m_Act5.IsEmpty() ) || !m_CE_Act3.GetStatus() || !m_CE_Act5.GetStatus())
	{
		if(MessageBox( GetString(IDS_STRING1514) , GetString(IDS_STRING908), MB_OK) != IDYES)
			return;
	}


	// insert record!
	RecordSet *prlRecord = new RecordSet(ogBCD.GetFieldCount("ACR"));
	
	prlRecord->Values[ogBCD.GetFieldIndex("ACR", "REGN")] = m_Regn;
	prlRecord->Values[ogBCD.GetFieldIndex("ACR", "ACT3")] = m_Act3;
	prlRecord->Values[ogBCD.GetFieldIndex("ACR", "ACT5")] = m_Act5;
	prlRecord->Values[ogBCD.GetFieldIndex("ACR", "UFIS")] = "1";

	ogBCD.InsertRecord("ACR", (*prlRecord), true);

	delete prlRecord;

	CDialog::OnOK();
}

/*
void RotationRegnDlg::OnAct35list() 
{
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ACT","ACT3,ACT5,ACFN", "ACFN+,ACT3+");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_Act3.SetInitText(polDlg->GetField("ACT3"), true);	
		m_CE_Act5.SetInitText(polDlg->GetField("ACT5"), true);	
	}
	delete polDlg;
}
*/

void RotationRegnDlg::OnAct35list() 
{
	CString olText3;
	m_CE_Act3.GetWindowText(olText3);
	CString olText5;
	m_CE_Act5.GetWindowText(olText5);

	if (olText5.IsEmpty() && !olText3.IsEmpty())
	{
		OnAct3list();
		return;
	}

	CCS_TRY
	
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ACT","ACT5,ACT3,ACFN", "ACT5+,ACT3,+ACFN+", olText5+","+olText3);
	polDlg->SetSecState("SEASONDLG_CE_Act3");

	if(polDlg->DoModal() == IDOK)
	{
		m_CE_Act3.SetInitText(polDlg->GetField("ACT3"), true);	
		m_CE_Act5.SetInitText(polDlg->GetField("ACT5"), true);	
		bmActSelect = true;
	}
	delete polDlg;

	CCS_CATCH_ALL	
}

void RotationRegnDlg::OnAct3list() 
{
	CString olText3;
	m_CE_Act3.GetWindowText(olText3);
	CString olText5;
	m_CE_Act5.GetWindowText(olText5);

	if (olText3.IsEmpty() && !olText5.IsEmpty())
	{
		OnAct35list();
		return;
	}

	CCS_TRY

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ACT","ACT3,ACT5,ACFN", "ACT3+,ACT5+,ACFN+", olText3+","+olText5);
	polDlg->SetSecState("SEASONDLG_CE_Act3");

	if(polDlg->DoModal() == IDOK)
	{
		m_CE_Act3.SetInitText(polDlg->GetField("ACT3"), true);	
		m_CE_Act5.SetInitText(polDlg->GetField("ACT5"), true);	

		bmActSelect = true;
	}
	delete polDlg;

	CCS_CATCH_ALL	

}


LONG RotationRegnDlg::OnEditKillfocus( UINT wParam, LPARAM lParam)
{
	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY*)lParam;
	prlNotify->Text.TrimRight();
	prlNotify->UserStatus = true;

	if(((UINT)m_CE_Act5.imID == wParam))
	{
		if (!bmActSelect)
		{
			prlNotify->UserStatus = true;

			CString olWhere;
			if (prlNotify->Text.IsEmpty())
				prlNotify->Text = " ";

			CString olTmp5 = prlNotify->Text;
			CString olTmp3;
			m_CE_Act3.GetWindowText(olTmp3);
			if (olTmp3.IsEmpty())
				olTmp3 = " ";

			CCSPtrArray<RecordSet> prlRecords;
			int ilCount = ogBCD.GetRecordsExt("ACT","ACT3","ACT5",olTmp3,olTmp5, &prlRecords);
			prlRecords.DeleteAll();

			if (ilCount != 1)
			{
				ilCount = ogBCD.GetRecordsExt("ACT","ACT5","HOPO",prlNotify->Text,CString(pcgHome), &prlRecords);
				prlRecords.DeleteAll();

				CString olTmp;
				if(prlNotify->Status = ogBCD.GetField("ACT", "ACT5", prlNotify->Text, "ACT3", olTmp ))
				{
					m_CE_Act3.SetInitText(olTmp);
					m_CE_Act3.SetStatus(true);
					prlNotify->Status = true;
				}

				if (ilCount > 1)
					OnAct35list();
			}
		}
		return 0L;
	}

	if(((UINT)m_CE_Act3.imID == wParam))
	{
		if (!bmActSelect)
		{
			prlNotify->UserStatus = true;

			CString olWhere;
			if (prlNotify->Text.IsEmpty())
				prlNotify->Text = " ";

			CString olTmp3 = prlNotify->Text;
			CString olTmp5;
			m_CE_Act5.GetWindowText(olTmp5);
			if (olTmp5.IsEmpty())
				olTmp5 = " ";

			CCSPtrArray<RecordSet> prlRecords;
			int ilCount = ogBCD.GetRecordsExt("ACT","ACT3","ACT5",olTmp3,olTmp5, &prlRecords);
			prlRecords.DeleteAll();

			if (ilCount != 1)
			{
				ilCount = ogBCD.GetRecordsExt("ACT","ACT3","HOPO",prlNotify->Text,CString(pcgHome), &prlRecords);
				prlRecords.DeleteAll();

				CString olTmp;
				if(prlNotify->Status = ogBCD.GetField("ACT", "ACT3", prlNotify->Text, "ACT5", olTmp ))
				{
					m_CE_Act5.SetInitText(olTmp);
					m_CE_Act5.SetStatus(true);
					prlNotify->Status = true;
				}

				if (ilCount > 1)
				{
					OnAct3list();
				}
			}
		}
	
		return 0L;
	}


	return 0L;


/*
	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY*)lParam;
	prlNotify->Text.TrimRight();
	prlNotify->UserStatus = true;

	if((UINT)m_CE_Act5.imID == wParam)
	{
		CString olAct3;
		if(prlNotify->Status = ogBCD.GetField("ACT", "ACT5", prlNotify->Text, "ACT3", olAct3 ) == true)
		{
			m_CE_Act3.SetWindowText(olAct3);
			m_CE_Act3.SetStatus(true);
			prlNotify->Status = true;
		}
		else
		{
			prlNotify->Status = false;
		}
		return 0L;
	}

	if((UINT)m_CE_Act3.imID == wParam)
	{
		CString olAct5;
		if(prlNotify->Status = ogBCD.GetField("ACT", "ACT3", prlNotify->Text, "ACT5", olAct5 ) == true)
		{
			m_CE_Act5.SetWindowText(olAct5);
			m_CE_Act5.SetStatus(true);
			prlNotify->Status = true;
		}
		else 
		{
			prlNotify->Status = false;
		}
		return 0L;
	}
	return 0L;
*/
}

LONG RotationRegnDlg::OnEditChanged( UINT wParam, LPARAM lParam)
{
	if((UINT)m_CE_Act3.imID == wParam || (UINT)m_CE_Act5.imID == wParam)
	{
		bmActSelect = false;
	}

	return 0L;
}


BOOL RotationRegnDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_CE_Regn.SetReadOnly();
	m_CE_Act5.SetTypeToString("XXXXX",5,0);
	m_CE_Act5.SetBKColor(YELLOW);
	m_CE_Act3.SetTypeToString("XXX",3,0);
	m_CE_Act3.SetBKColor(YELLOW);

	bmActSelect = true;

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
