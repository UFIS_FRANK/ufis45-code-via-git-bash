// RotationPlbDlg.cpp : implementation file
//

#include <stdafx.h>
#include <fpms.h>
#include <RotationPlbDlg.h>
#include <CedaPlbData.h>
#include <PrivList.h>
#include <CCSGlobl.h>
#include <CCSBcHandle.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// RotationPlbDlg dialog


RotationPlbDlg::RotationPlbDlg(CWnd* pParent /*=NULL*/, ROTATIONDLGFLIGHTDATA *prpFlight, bool bpLocal )
	: CDialog(RotationPlbDlg::IDD, pParent)
{
	prmFlight = prpFlight;
	bmLocal = bpLocal;

	//{{AFX_DATA_INIT(RotationPlbDlg)
	//}}AFX_DATA_INIT
}


void RotationPlbDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(RotationPlbDlg)
	DDX_Control(pDX, IDC_GAAE3, m_CE_Gaae3);
	DDX_Control(pDX, IDC_GAAE2, m_CE_Gaae2);
	DDX_Control(pDX, IDC_GAAE1, m_CE_Gaae1);
	DDX_Control(pDX, IDC_GAAB3, m_CE_Gaab3);
	DDX_Control(pDX, IDC_GAAB2, m_CE_Gaab2);
	DDX_Control(pDX, IDC_GAAB1, m_CE_Gaab1);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(RotationPlbDlg, CDialog)
	//{{AFX_MSG_MAP(RotationPlbDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// RotationPlbDlg message handlers

void RotationPlbDlg::OnOK() 
{

	if(!m_CE_Gaae1.GetStatus() || !m_CE_Gaae2.GetStatus() || !m_CE_Gaae3.GetStatus() ||
	   !m_CE_Gaab1.GetStatus() || !m_CE_Gaab2.GetStatus() || !m_CE_Gaab3.GetStatus())
	{
		MessageBox(GetString(ST_BADFORMAT), GetString(ST_FEHLER));

		return;
	}


	CString olGaab;
	CString olGaae;
		

	PLBDATA *prlGpa;

	CTime olSto;

	if(CString(prmFlight->Adid) == "A")
		olSto = prmFlight->Stoa;
	else
		olSto = prmFlight->Stod;

	CString olUser = CString(pcgUser); 
	CTime olTime = CTime::GetCurrentTime();
	if(bmLocal) ogBasicData.LocalToUtc(olTime);


	// 1 //////////////////////////////////

	m_CE_Gaae1.GetWindowText(olGaae);
	m_CE_Gaab1.GetWindowText(olGaab);



	prlGpa = ogPlbData.GetGpa(1);
	if(prlGpa != NULL)
	{
		prlGpa->Gaae = 	HourStringToDate( olGaae, olSto);
		prlGpa->Gaab = 	HourStringToDate( olGaab, olSto);
		prlGpa->IsChanged = DATA_CHANGED;
		if(bmLocal) ogBasicData.LocalToUtc(prlGpa->Gaab);
		if(bmLocal) ogBasicData.LocalToUtc(prlGpa->Gaae);
		prlGpa->Lstu = olTime;
		strcpy(prlGpa->Useu, olUser);

		ogPlbData.Save(prlGpa);
	}
	else
	{
		if(!olGaab.IsEmpty() || !olGaae.IsEmpty())
		{
			prlGpa = new PLBDATA;
			prlGpa->Flnu = prmFlight->Urno;
			prlGpa->Aseq = 1;
			//prlGpa->Urno = ogBasicData.GetNextUrno();
			prlGpa->Gaae = 	HourStringToDate( olGaae, olSto);
			prlGpa->Gaab = 	HourStringToDate( olGaab, olSto);
			prlGpa->IsChanged = DATA_NEW;
			if(bmLocal) ogBasicData.LocalToUtc(prlGpa->Gaab);
			if(bmLocal) ogBasicData.LocalToUtc(prlGpa->Gaae);
			prlGpa->Cdat = olTime;
			strcpy(prlGpa->Usec, olUser);

			ogPlbData.Save(prlGpa);
		}
	}

	// 2 //////////////////////////////////

	m_CE_Gaae2.GetWindowText(olGaae);
	m_CE_Gaab2.GetWindowText(olGaab);


	prlGpa = ogPlbData.GetGpa(2);
	if(prlGpa != NULL)
	{
		prlGpa->Gaae = 	HourStringToDate( olGaae, olSto);
		prlGpa->Gaab = 	HourStringToDate( olGaab, olSto);
		prlGpa->IsChanged = DATA_CHANGED;
		if(bmLocal) ogBasicData.LocalToUtc(prlGpa->Gaab);
		if(bmLocal) ogBasicData.LocalToUtc(prlGpa->Gaae);
		prlGpa->Lstu = olTime;
		strcpy(prlGpa->Useu, olUser);

		ogPlbData.Save(prlGpa);
	}
	else
	{
		if(!olGaab.IsEmpty() || !olGaae.IsEmpty())
		{
			prlGpa = new PLBDATA;
			prlGpa->Flnu = prmFlight->Urno;
			prlGpa->Aseq = 2;
			//prlGpa->Urno = ogBasicData.GetNextUrno();
			prlGpa->Gaae = 	HourStringToDate( olGaae, olSto);
			prlGpa->Gaab = 	HourStringToDate( olGaab, olSto);
			prlGpa->IsChanged = DATA_NEW;
			if(bmLocal) ogBasicData.LocalToUtc(prlGpa->Gaab);
			if(bmLocal) ogBasicData.LocalToUtc(prlGpa->Gaae);
			prlGpa->Cdat = olTime;
			strcpy(prlGpa->Usec, olUser);

			ogPlbData.Save(prlGpa);
		}
	}

	// 3 //////////////////////////////////

	m_CE_Gaae3.GetWindowText(olGaae);
	m_CE_Gaab3.GetWindowText(olGaab);


	prlGpa = ogPlbData.GetGpa(3);
	if(prlGpa != NULL)
	{
		prlGpa->Gaae = 	HourStringToDate( olGaae, olSto);
		prlGpa->Gaab = 	HourStringToDate( olGaab, olSto);
		prlGpa->IsChanged = DATA_CHANGED;
		if(bmLocal) ogBasicData.LocalToUtc(prlGpa->Gaab);
		if(bmLocal) ogBasicData.LocalToUtc(prlGpa->Gaae);
		prlGpa->Lstu = olTime;
		strcpy(prlGpa->Useu, olUser);
		ogPlbData.Save(prlGpa);
	}
	else
	{
		if(!olGaab.IsEmpty() || !olGaae.IsEmpty())
		{
			prlGpa = new PLBDATA;
			prlGpa->Flnu = prmFlight->Urno;
			prlGpa->Aseq = 3;
			//prlGpa->Urno = ogBasicData.GetNextUrno();
			prlGpa->Gaae = 	HourStringToDate( olGaae, olSto);
			prlGpa->Gaab = 	HourStringToDate( olGaab, olSto);
			prlGpa->IsChanged = DATA_NEW;
			if(bmLocal) ogBasicData.LocalToUtc(prlGpa->Gaab);
			if(bmLocal) ogBasicData.LocalToUtc(prlGpa->Gaae);
			prlGpa->Cdat = olTime;
			strcpy(prlGpa->Usec, olUser);

			ogPlbData.Save(prlGpa);
		}
	}




	CDialog::OnOK();
}

BOOL RotationPlbDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	

	m_CE_Gaab1.SetTypeToTime(false, true);
	m_CE_Gaab2.SetTypeToTime(false, true);
	m_CE_Gaab3.SetTypeToTime(false, true);

	m_CE_Gaae1.SetTypeToTime(false, true);
	m_CE_Gaae2.SetTypeToTime(false, true);
	m_CE_Gaae3.SetTypeToTime(false, true);



	m_CE_Gaae1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Plb"));	
	m_CE_Gaae2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Plb"));	
	m_CE_Gaae3.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Plb"));	


	m_CE_Gaab1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Plb"));	
	m_CE_Gaab2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Plb"));	
	m_CE_Gaab3.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Plb"));	


	ogPlbData.Read(prmFlight->Urno);
	
	
	PLBDATA *prlGpa;




	CTime olSto;

	if(CString(prmFlight->Adid) == "A")
		olSto = prmFlight->Stoa;
	else
		olSto = prmFlight->Stod;



	prlGpa = ogPlbData.GetGpa(1);
	if(prlGpa != NULL)
	{
		if(bmLocal) ogBasicData.UtcToLocal(prlGpa->Gaab);
		if(bmLocal) ogBasicData.UtcToLocal(prlGpa->Gaae);
		m_CE_Gaab1.SetInitText(DateToHourDivString(prlGpa->Gaab, olSto));
		m_CE_Gaae1.SetInitText(DateToHourDivString(prlGpa->Gaae, olSto));
	}


	prlGpa = ogPlbData.GetGpa(2);
	if(prlGpa != NULL)
	{
		if(bmLocal) ogBasicData.UtcToLocal(prlGpa->Gaab);
		if(bmLocal) ogBasicData.UtcToLocal(prlGpa->Gaae);
		m_CE_Gaab2.SetInitText(DateToHourDivString(prlGpa->Gaab, olSto));
		m_CE_Gaae2.SetInitText(DateToHourDivString(prlGpa->Gaae, olSto));
	}

	prlGpa = ogPlbData.GetGpa(3);
	if(prlGpa != NULL)
	{
		if(bmLocal) ogBasicData.UtcToLocal(prlGpa->Gaab);
		if(bmLocal) ogBasicData.UtcToLocal(prlGpa->Gaae);
		m_CE_Gaab3.SetInitText(DateToHourDivString(prlGpa->Gaab, olSto));
		m_CE_Gaae3.SetInitText(DateToHourDivString(prlGpa->Gaae, olSto));
	}


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
