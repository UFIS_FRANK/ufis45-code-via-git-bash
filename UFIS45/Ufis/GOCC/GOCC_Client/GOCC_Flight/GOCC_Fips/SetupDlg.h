#ifndef AFX_SETUPDLG_H__EC726D71_1630_11D2_8D95_0000C002916B__INCLUDED_
#define AFX_SETUPDLG_H__EC726D71_1630_11D2_8D95_0000C002916B__INCLUDED_

// SetupDlg.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld SetupDlg 
#include <Resource.h>
#include <resrc1.h>
#include <CCSGlobl.h>
#include <FPMS.h>
#include <CedaCfgData.h>
#include <CCSEdit.h>
#include <CCSButtonCtrl.h>
#include <GatPosConflictSetupDlg.h>
#include <DlgResizeHelper.h>

//#include "stdafx.h"

class CXButton : public CButton
{
private:
	void DrawItem( LPDRAWITEMSTRUCT lpDrawItemStruct );
	COLORREF m_rgbBackground ;

public:
	CXButton();
	COLORREF SetBkColor( COLORREF rgb );		// set button background color
};


class SetupDlg : public CDialog
{
// Konstruktion
public:
	SetupDlg(CWnd* pParent = NULL);   // Standardkonstruktor
	~SetupDlg();
	void SaveToReg();
// Dialogfelddaten
	//{{AFX_DATA(SetupDlg)
	enum { IDD = IDD_MONITOR_SETUP };
	CComboBox	m_cbHandlingTask;
	CXButton m_btnHandlingColor;
	CComboBox	m_cbHandlingAgent;
	CButton	m_CC_PostFlightTodayOnly;
	CCSButtonCtrl	m_CB_Save;
	CButton	m_CC_Conf20;
	CButton	m_CC_Conf19;
	CEdit	m_CE_Conf18_Min;
	CButton	m_CC_Conf18;
	CButton	m_CC_Conf17;
	CButton	m_CC_Conf16;
	CButton	m_CC_Conf15;
	CButton	m_CC_Conf14;
	CEdit	m_CE_Conf12_Min;
	CEdit	m_CE_Conf9_Min;
	CEdit	m_CE_Conf7_Min;
	CEdit	m_CE_Conf4_Min;
	CEdit	m_CE_Conf3_Min;
	CEdit	m_CE_Conf2_Min;
	CEdit	m_CE_Conf13_Min;
	CEdit	m_CE_Conf11_Min;
	CEdit	m_CE_Conf10_Min;
	CButton	m_CC_Conf9;
	CButton	m_CC_Conf8;
	CButton	m_CC_Conf7;
	CButton	m_CC_Conf6;
	CButton	m_CC_Conf5;
	CButton	m_CC_Conf4;
	CButton	m_CC_Conf3;
	CButton	m_CC_Conf2;
	CButton	m_CC_Conf13;
	CButton	m_CC_Conf12;
	CButton	m_CC_Conf11;
	CButton	m_CC_Conf10;
	CButton	m_CC_Conf1;
	CStatic	m_CS_Font;
	CComboBox	m_UserCB;
	int		m_Monitors;
	int		m_Batch1;
	int		m_Daily1;
	int		m_DailyRot1;
	int		m_FlightDia1;
	int		m_Season1;
	int		m_SeasonRot1;
	int		m_CcaDia1;
	CCSEdit	m_CE_Archive;
	CCSEdit	m_CE_Archive_Future;
	CCSButtonCtrl	m_CB_ResGroups;
	CCSEdit	m_CE_GateBuffer;
	CCSEdit	m_CE_PosBuffer;
	CButton	m_CC_ResChainDlg;
	CCSEdit	m_CE_XDays;
	CCSEdit	m_CE_TimelineBuffer;
	CCSEdit	m_CE_Xminutes; 
	//}}AFX_DATA


	// test reiner
	 CObList omMonitorButtons1;
	 CObList omMonitorButtons2;
	 CObList omMonitorButtons3;
	//CCSPtrArray<CButton> omMonitorButtons2;
	//CCSPtrArray<CButton> omMonitorButtons3;
// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(SetupDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	void SetSecState();
	void EnableAll();
	bool InitUserCB();
	bool m_superUser;
	bool m_initUserCB;


	char m_cSecState_ArchivePostFlightTodayOnly ;		// 050315 MVy: setup dialog GUI non C++ object item security states
	void InitPostFlightTodayOnly();		// 050315 MVy: initialize GUI item
	void UpdatePostFlightTodayOnly();		// 050309 MVy: today only or period, toggle postition check, set status of depending fields
	void DisablePostFlightTodayOnly();		// 050323 MVy: make GUI item invisible, usage dependent on ceda.ini entry

	char m_cSecState_HandlingAgent ;
	void InitHandlingAgent();		// 050314 MVy: PRF6903: Check-In Counter Restrictions for Handling Agents, initialize dialog items
	void UpdateHandlingAgent( unsigned long urnoHandlingAgent );		// 050311 MVy: PRF6903: Check-In Counter Restrictions for Handling Agents, set status of depending fields
	void DisableHandlingAgent();		// 050323 MVy: make GUI item invisible, usage dependent on ceda.ini entry

	char m_cSecState_HandlingTask ;		// 050318 MVy: 
	void InitHandlingTask();		// 050318 MVy: 
	void UpdateHandlingTask( unsigned long urnoHandlingTask );		// 050318 MVy: 
	void DisableHandlingTask();		// 050323 MVy: make GUI item invisible, usage dependent on ceda.ini entry

	void DisableHandlingColor();		// 050323 MVy: make GUI item invisible, usage dependent on ceda.ini entry
	void DisableHandlingGroupbox();		// 050404 MVy: the frame should not be visible if there is no element visible within
private:
	bool IsDlgItemInvisible( int nID );		// 050404 MVy: helper function to get information about visibility of dialog item window


	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(SetupDlg)
	afx_msg void OnSelchangePknoCb();
	afx_msg void OnRadio1();
	afx_msg void OnRadio2();
	afx_msg void OnRadio3();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnFont();
	afx_msg void OnResGroups();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnPostFlightTodayOrPeriod();
	afx_msg void OnSelchangeCbHandlingAgent();
	afx_msg void OnKillfocusCbHandlingAgent();
	afx_msg void OnBtnHandlingColor();
	afx_msg void OnKillfocusCbHandlingTask();
	afx_msg void OnSelchangeCbHandlingTask();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	GatPosConflictSetupDlg*	pomConflictDlg;	
	DlgResizeHelper m_resizeHelper;
	CString m_key;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_SETUPDLG_H__EC726D71_1630_11D2_8D95_0000C002916B__INCLUDED_
