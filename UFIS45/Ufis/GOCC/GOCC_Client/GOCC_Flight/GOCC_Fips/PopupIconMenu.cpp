// PopupIconMenu.cpp : implementation file
//

#include "stdafx.h"

#include "PopupIconMenu.h"

//#include "toolbar.h"
//#include "imagelist.h"
//#include "button.h"
//#include "buttonmenu.h"
//#include "buttonmenus.h"
//#include "buttons.h"
//#include "image.h"
//#include "images.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPopupIconMenu dialog

#define WM_MENU_RANGE_END	WM_USER+4000
#define IDC_CHKIN			WM_MENU_RANGE_END+1
#define IDC_MEAL			WM_MENU_RANGE_END+2
#define IDC_CLEANING		WM_MENU_RANGE_END+3
#define IDC_GATE			WM_MENU_RANGE_END+4
#define IDC_SORTING			WM_MENU_RANGE_END+5
#define IDC_MEAL_ARR		WM_MENU_RANGE_END+6
#define IDC_MEAL_DEP		WM_MENU_RANGE_END+7
#define IDC_MEAL_ARR_HALF	WM_MENU_RANGE_END+8
#define IDC_MEAL_DEP_HALF	WM_MENU_RANGE_END+9


CPopupIconMenu::CPopupIconMenu(CWnd* pParent /*=NULL*/)
	: CDialog(CPopupIconMenu::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPopupIconMenu)
	m_FLINFO = _T("");
	//}}AFX_DATA_INIT
	omParent = pParent;

	//Init IconData
	_tcscpy(omIconData[0].name, "Meal");//Name
	omIconData[0].iconBmpId		= IDB_BMP_CATERING;//Bitmap Id
	omIconData[0].iconBmpId_Arr = IDB_BMP_CATERING_ARR;
	omIconData[0].iconBmpId_Dep = IDB_BMP_CATERING_DEP;
	omIconData[0].iconBmpId_Arr_Half = IDB_BMP_CATERING_ARR_HALF;
	omIconData[0].iconBmpId_Dep_Half = IDB_BMP_CATERING_DEP_HALF;
	omIconData[0].iconChar		= 'm';//Icon Char
	omIconData[0].iconMsgMapId	= IDC_MEAL;//Message Map Id
	omIconData[0].iconMsgMapId_Arr		= IDC_MEAL_ARR;
	omIconData[0].iconMsgMapId_Arr_Half	= IDC_MEAL_ARR_HALF;
	omIconData[0].iconMsgMapId_Dep		= IDC_MEAL_DEP;
	omIconData[0].iconMsgMapId_Dep_Half	= IDC_MEAL_DEP_HALF;

	_tcscpy(omIconData[1].name, "Gate");
	omIconData[1].iconBmpId		= IDB_BMP_GATE;
	omIconData[1].iconBmpId_Arr			= IDB_BMP_GATE;
	omIconData[1].iconBmpId_Dep			= IDB_BMP_GATE;
	omIconData[1].iconBmpId_Arr_Half	= IDB_BMP_GATE;
	omIconData[1].iconBmpId_Dep_Half	= IDB_BMP_GATE;
	omIconData[1].iconChar		= 'g';
	omIconData[1].iconMsgMapId	= IDC_GATE;
	omIconData[1].iconMsgMapId_Arr		= IDC_GATE;
	omIconData[1].iconMsgMapId_Arr_Half	= IDC_GATE;
	omIconData[1].iconMsgMapId_Dep		= IDC_GATE;
	omIconData[1].iconMsgMapId_Dep_Half	= IDC_GATE;

	_tcscpy(omIconData[2].name, "Cleaning");
	omIconData[2].iconBmpId		= IDB_BMP_CLEANING;
	omIconData[2].iconBmpId_Arr			= IDB_BMP_CLEANING;
	omIconData[2].iconBmpId_Dep			= IDB_BMP_CLEANING;
	omIconData[2].iconBmpId_Arr_Half	= IDB_BMP_CLEANING;
	omIconData[2].iconBmpId_Dep_Half	= IDB_BMP_CLEANING;
	omIconData[2].iconChar		= 'c';
	omIconData[2].iconMsgMapId	= IDC_CLEANING;
	omIconData[2].iconMsgMapId_Arr		= IDC_CLEANING;
	omIconData[2].iconMsgMapId_Arr_Half	= IDC_CLEANING;
	omIconData[2].iconMsgMapId_Dep		= IDC_CLEANING;
	omIconData[2].iconMsgMapId_Dep_Half	= IDC_CLEANING;

	_tcscpy(omIconData[3].name, "Sorting");
	omIconData[3].iconBmpId		= IDB_BMP_SORTING;
	omIconData[3].iconBmpId_Arr			= IDB_BMP_SORTING;
	omIconData[3].iconBmpId_Dep			= IDB_BMP_SORTING;
	omIconData[3].iconBmpId_Arr_Half	= IDB_BMP_SORTING;
	omIconData[3].iconBmpId_Dep_Half	= IDB_BMP_SORTING;
	omIconData[3].iconChar		= 's';
	omIconData[3].iconMsgMapId	= IDC_SORTING;
	omIconData[3].iconMsgMapId_Arr		= IDC_SORTING;
	omIconData[3].iconMsgMapId_Arr_Half	= IDC_SORTING;
	omIconData[3].iconMsgMapId_Dep		= IDC_SORTING;
	omIconData[3].iconMsgMapId_Dep_Half	= IDC_SORTING;


	_tcscpy(omIconData[4].name, "Check-In");
	omIconData[4].iconBmpId		= IDB_BMP_CHECKIN;
	omIconData[4].iconBmpId_Arr			= IDB_BMP_CHECKIN;
	omIconData[4].iconBmpId_Dep			= IDB_BMP_CHECKIN;
	omIconData[4].iconBmpId_Arr_Half	= IDB_BMP_CHECKIN;
	omIconData[4].iconBmpId_Dep_Half	= IDB_BMP_CHECKIN;
	omIconData[4].iconChar		= 'k';
	omIconData[4].iconMsgMapId	= IDC_CHKIN;
	omIconData[4].iconMsgMapId_Arr		= IDC_CHKIN;
	omIconData[4].iconMsgMapId_Arr_Half	= IDC_CHKIN;
	omIconData[4].iconMsgMapId_Dep		= IDC_CHKIN;
	omIconData[4].iconMsgMapId_Dep_Half	= IDC_CHKIN;

	omArrUrno=0;
	omDepUrno=0;

	_tcscpy( psmDepIconToShow, "" );
	_tcscpy( psmArrIconToShow, "" );

	imLeft = 0;
	imTop = 0;
	imMaxWidth = 0;
}


void CPopupIconMenu::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPopupIconMenu)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPopupIconMenu, CDialog)
	//{{AFX_MSG_MAP(CPopupIconMenu)
	ON_BN_CLICKED(IDC_CHKIN, OnCheckIn)
	ON_BN_CLICKED(IDC_MEAL, OnMeal)
	ON_BN_CLICKED(IDC_MEAL_ARR, OnMeal_Arr)
	ON_BN_CLICKED(IDC_MEAL_ARR_HALF, OnMeal_ArrHalf)
	ON_BN_CLICKED(IDC_MEAL_DEP, OnMeal_Dep)
	ON_BN_CLICKED(IDC_MEAL_DEP_HALF, OnMeal_DepHalf)
	ON_BN_CLICKED(IDC_GATE, OnGate)
	ON_BN_CLICKED(IDC_SORTING, OnSorting)
	ON_BN_CLICKED(IDC_CLEANING, OnCleaning)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPopupIconMenu message handlers

void CPopupIconMenu::OnCheckIn() 
{
	// TODO: Add your control notification handler code here
	HideMenu();
	MessageBox("CheckIn");
	
}

void CPopupIconMenu::OnCleaning() 
{
	// TODO: Add your control notification handler code here
	HideMenu();
	MessageBox("OnCleaning");
	
}

void CPopupIconMenu::OnGate() 
{
	// TODO: Add your control notification handler code here
	HideMenu();
	MessageBox("OnGate");
	
}

void CPopupIconMenu::OnSorting() 
{
	// TODO: Add your control notification handler code here
	HideMenu();
	MessageBox("OnSorting");
	
}
void CPopupIconMenu::OnMeal() 
{
	// TODO: Add your control notification handler code here
	HideMenu();
	MessageBox("Meal");	
}

void CPopupIconMenu::OnMeal_Arr() 
{
	// TODO: Add your control notification handler code here
	HideMenu();
	MessageBox("OnMeal_Arr");	
}

void CPopupIconMenu::OnMeal_ArrHalf() 
{
	// TODO: Add your control notification handler code here
	HideMenu();
	MessageBox("OnMeal_ArrHalf");	
}

void CPopupIconMenu::OnMeal_Dep() 
{
	// TODO: Add your control notification handler code here
	HideMenu();
	MessageBox("OnMeal_Dep");	
}

void CPopupIconMenu::OnMeal_DepHalf() 
{
	// TODO: Add your control notification handler code here
	HideMenu();
	MessageBox("OnMeal_DepHalf");	
}


bool CPopupIconMenu::HideMenu()
{
	bool success=true;
	this->ShowWindow( SW_HIDE );
	return success;
}

bool CPopupIconMenu::ShowMenu( const int ipLeft, const int ipTop, const long lpArrUrno, const char* pspArrIconToShow, const long lpDepUrno, const char* pspDepIconToShow, const int ipMaxWidth )
{
	
	bool success=true;
	
	if ((omArrUrno==lpArrUrno) & 
		(omDepUrno==lpDepUrno) &
		(_tcscmp(pspArrIconToShow,psmArrIconToShow)==0) &
		(_tcscmp(pspDepIconToShow,psmDepIconToShow)==0) &
		(imLeft==ipLeft) & (imTop==ipTop) &
		(imMaxWidth==ipMaxWidth))
	{
		this->ShowWindow(SW_SHOW);
	}
	else
	{	
		
		omArrUrno = lpArrUrno;
		omDepUrno = lpDepUrno;
		_tcscpy( psmDepIconToShow, pspDepIconToShow );
		_tcscpy( psmArrIconToShow, pspArrIconToShow );
		imLeft = ipLeft;
		imTop = ipTop;
		imMaxWidth = ipMaxWidth;
	
		CString stArrIconToShow = CString(pspArrIconToShow);//Arrival flight related button chars
		CString stDepIconToShow = CString(pspDepIconToShow);//Departure flight related button chars
		stArrIconToShow.MakeLower();
		stDepIconToShow.MakeLower();

		int ilBtnCnt = _tcslen(pspArrIconToShow);//number of button to display
		int i = 0;
		int maxIcons = sizeof( omIconData )/sizeof(omIconData[0]);//max buttons count

		bool blShow = true;
		if (ilBtnCnt<0) blShow = false;
		else
		{
			ilBtnCnt = 0;
			for (i=0; i<maxIcons; i++)
			{
				try
				{//remove previous button
					RemoveButton(omIconData[i].iconMsgMapId);
					RemoveButton(omIconData[i].iconMsgMapId_Arr);
					RemoveButton(omIconData[i].iconMsgMapId_Arr_Half);
					RemoveButton(omIconData[i].iconMsgMapId_Dep);
					RemoveButton(omIconData[i].iconMsgMapId_Dep_Half);
				}
				catch(...)
				{
				}
				//Check whether need to show button for this and increase the button count to display
				if ((stArrIconToShow.Find(omIconData[i].iconChar)>=0) || 
					(stDepIconToShow.Find(omIconData[i].iconChar)>=0))
					ilBtnCnt++;
			}

			if (ilBtnCnt>maxIcons) ilBtnCnt = maxIcons;
		}

		if (ilBtnCnt<0) blShow = false;//Check again to make sure any button to display. In case of invalid icon char

		if (blShow)
		{
			const int ilSpaceBtwButton = 2;
			int ilLeft = ipLeft+30;
			int ilTop = ipTop;
			if (ilTop<0) ilTop = 0;
			int ilMenuWidth = 47 * ilBtnCnt;
			if ((ilLeft + ilMenuWidth) > (ipLeft + ipMaxWidth) )
			{
				ilLeft = ipLeft - ilMenuWidth + 8;
				if (ilLeft<0) ilLeft = 0;
			}
			CRect olRect;  			
			this->MoveWindow( ilLeft, ilTop, ilMenuWidth, 50 );
			GetClientRect( &olRect );

			int left = olRect.left ;
			int top = olRect.top ;
			for (i=0; i<maxIcons; i++)
			{
				if ( (stArrIconToShow.Find(omIconData[i].iconChar)>=0) &
					 (stDepIconToShow.Find(omIconData[i].iconChar)>=0))
				{
					ilBtnCnt++;
					ShowIconHalf( omIconData[i].iconBmpButton, left, top, omIconData[i].iconBmpId_Arr_Half, omIconData[i].name, omIconData[i].iconMsgMapId_Arr_Half );
					ShowIconHalf( omIconData[i].iconBmpButtonR, left, top, omIconData[i].iconBmpId_Dep_Half, omIconData[i].name, omIconData[i].iconMsgMapId_Dep_Half );
					left++;
				}
				else if (stArrIconToShow.Find(omIconData[i].iconChar)>=0)
				{
					ilBtnCnt++;
					ShowIcon( omIconData[i].iconBmpButton, left, top, omIconData[i].iconBmpId_Arr, omIconData[i].name, omIconData[i].iconMsgMapId_Arr );
				}
				else if (stDepIconToShow.Find(omIconData[i].iconChar)>=0)
				{
					ilBtnCnt++;
					ShowIcon( omIconData[i].iconBmpButton, left, top, omIconData[i].iconBmpId_Dep, omIconData[i].name, omIconData[i].iconMsgMapId_Dep );
				}
			}

			this->ShowWindow( SW_SHOW);
		}
		else
		{//No button to show
			this->ShowWindow( SW_HIDE );
		}
	}

	return success;
}

void CPopupIconMenu::RemoveButton(const int ipMsgMapId)
{
	try
	{
		this->GetDlgItem(ipMsgMapId)->DestroyWindow();
	}
	catch(...)
	{
	}
}

void CPopupIconMenu::ShowIcon( CBitmapButton& omBmpBtn, int &left, const int top,  const int ipBitMapId, const char* pspName, const int ipMsgMapId )
{
	const int WIDTH = 45;
	const int HEIGHT = 45;
	char pslName[100];
	if (!pspName) strcpy(pslName, "");
	else strcpy(pslName, pspName );
	omBmpBtn.DestroyWindow();
	omBmpBtn.Create( pslName, BS_DEFPUSHBUTTON | BS_OWNERDRAW | WS_CHILD | WS_VISIBLE,
		CRect( left, top, left + WIDTH, top + HEIGHT), this, ipMsgMapId );
	GetDlgItem( ipMsgMapId )->EnableWindow(TRUE);
	//omBmpBtn1.Attach( this );
	omBmpBtn.LoadBitmaps( ipBitMapId );
		
	omBmpBtn.ShowWindow( SW_SHOW );
	left = left + WIDTH + 2;
/*
	///--------------
	// One-stage
    CPen myPen1( PS_DOT, 5, RGB(0,0,0) );

// Two-stage: first construct the pen
    CPen myPen2;
    // Then initialize it
    if( myPen2.CreatePen( PS_DOT, 5, RGB(0,0,0) ) )   
        // Use the pen


		void CMyView::OnDraw( CDC* pDC )
{
    CPen penBlack;  // Construct it, then initialize
    if( newPen.CreatePen( PS_SOLID, 2, RGB(0,0,0) ) )
    {
        // Select it into the device context
        // Save the old pen at the same time
        CPen* pOldPen = pDC->SelectObject( &penBlack );

        // Draw with the pen
        pDC->MoveTo(...);
        pDC->LineTo(...);

        // Restore the old pen to the device context
        pDC->SelectObject( pOldPen );
    }
    else
    {
        // Alert the user that resources are low
    }

	///--------------*/

}

void CPopupIconMenu::ShowIconHalf( CBitmapButton& omBmpBtn, int &left, const int top,  const int ipBitMapId, const char* pspName, const int ipMsgMapId )
{
	const int WIDTH = 23;
	const int HEIGHT = 45;
	char pslName[100];
	if (!pspName) strcpy(pslName, "");
	else strcpy(pslName, pspName );
	omBmpBtn.DestroyWindow();
	omBmpBtn.Create( pslName, BS_DEFPUSHBUTTON | BS_OWNERDRAW | WS_CHILD | WS_VISIBLE,
		CRect( left, top, left + WIDTH, top + HEIGHT), this, ipMsgMapId );
	GetDlgItem( ipMsgMapId )->EnableWindow(TRUE);
	//omBmpBtn1.Attach( this );
	omBmpBtn.LoadBitmaps( ipBitMapId );
		
	omBmpBtn.ShowWindow( SW_SHOW );
	left = left + WIDTH;
}


void CPopupIconMenu::PostNcDestroy() 
{	
    CDialog::PostNcDestroy();
    delete this;
}

