#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/GOCC/GOCC_Server/Base/Server/Kernel/stahdl.c 1.1 2008/11/19 11:40:09SGT msi Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author	      : HAG                                                       */
/* Date           : 06.11.2003                                                */
/* Description    : Status Manager server process according to CS_Stahdl.doc  */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/* 20071127 AM: To use the RSMTAB to get the time updated from other system   */
/*              based on the additional Column "TSKN" (Task Name) in SDETAB   */
/*                                                                            */
/*              FLDR (OTAS,OTAE,OTAC,OTDS,OTDE,OTDC)                          */
/*                  Taking the scheduled Time from RSMTAB                     */
/*                  OTAS - Other Scheduled Arrival Start Time   (SDST,RSMTAB) */
/*                  OTAE - Other Scheduled Arrival End Time     (SDED,RSMTAB) */
/*                  OTAC - Other Scheduled Arrival CheckList    (PROG,RSMTAB) */
/*                  OTDS - Other Scheduled Departure Start Time (SDST,RSMTAB) */
/*                  OTDE - Other Scheduled Departure End Time   (SDED,RSMTAB) */
/*                  OTDC - Other Scheduled Departure CheckList  (PROG,RSMTAB) */
/*      Note: OTAC/OTDC, CONS will be set to blank in OSTTAB                  */
/*            Otherwise, get the data from RSMTAB and assign to CONS in OSTTAB*/
/*                                                                            */
/*              RETY (OTHS,OTHE,OTHC)                                         */
/*                  Taking the Actual Time from RSMTAB                        */
/*                  OTHS - Other Actual Start Time (ACST,RSMTAB)              */
/*                  OTHE - Other Actual End Time   (ACED,RSMTAB)              */
/*                  OTHC - Other Actual CheckList  (PROG,RSMTAB)              */
/*      Note: If PROG is <= 0, uncheck the task, "" CONA                      */
/*            Else fill the CONA with (ACST/ACED/Current System Time) in OSTTAB*/ 
/*                                                                            */
/*                                                                            */
/* 20071217 AM: To send the alert update to Flight Handler                    */
/*              Based on "ALRT" Column in SDETAB                              */
/*              - If one of the Task with particular Alert Char has conflict  */
/*                    set alert for that char for the flight                  */
/*              - If All of the Tasks with particular Alert Char does not have conflict    */
/*                    unset alert for that char for the flight                */
/*              - If All of the Tasks with particular Alert Char has acknowledge */
/*                    set alert for that char in lower case for the flight    */
/*                                                                            */
/*                                                                            */
/* 20080304 AM: To update the RSMTAB Scheduled Time                           */
/* 20080312 ipr: change in ProcessCONSChange: pclBusn[] from 8 to 64          */
/* 20080620 MSI: Added new command(EVL)to be sent by ntisch,to generate TASKS */
/* 20081105 AM: Fix for missing Actual Time (Ref. to RSMTAB)                  */
/*               when Flight Info Updated                                     */
/*                                                                            */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */


#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "fditools.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "rmschkrul.h"
#include "syslib.h"
#include "db_if.h"
#include "AATArray.h"
#include <time.h>
#include <cedatime.h>
#include "chkval.h"       

#ifdef _HPUX_SOURCE
	extern int daylight;
	extern long timezone;
	extern char *tzname[2];
#else
	extern int _daylight;
	extern long _timezone;
	extern char *_tzname[2];
#endif

#ifndef min
#define min(a,b) (a < b ? a : b)
#endif
#ifndef max
#define max(a,b) (a > b ? a : b)
#endif

extern char  cgSelStr[];

#define ARR_NAME_LEN    12
#define ARR_FLDLST_LEN  256
#define URNOS_TO_FETCH 100
#define ARR_MAX_IDX    6
#define MAX_UAFTINBC	500

/* flags for SetArrayInfo */
#define CEDAARRAY_ALL		0xFF
#define CEDAARRAY_FILL		0x01
#define CEDAARRAY_URNOIDX	0x02
#define CEDAARRAY_TRIGGER	0x04

#define COND_COUNT    4
#define SRH_COND_SINGLE	"ACTU,ALTU,APTU,NATU"
#define SRH_COND_GROUP  "ACTG,ALTG,APTG,NATG"
#define AFT_INB_FIELD	"ACT3,ALC3,ORG3,TTYP"
#define AFT_OUTB_FIELD	"ACT3,ALC3,DES3,TTYP"
#define BASIC_DATA_TAB	"ACT,ALT,APT,NAT"
#define BASIC_DATA_FLD	"ACT3,ALC3,APC3,TTYP"

#define AFT_FIELDS "URNO,RKEY,ADID,HOPO,ALC2,ALC3,TTYP,ORG3,DES3,ACT3,ACT5,STOA,ETAI,LAND,ONBE,ONBL,TIFA,STOD,ETDI,OFBL,AIRB,TIFD,GA1X,GD1X,GA1Y,GD1Y,B1BA,B1EA,FLNO,TMOA,GA1B,GD1B,GA1E,GD1E,B1BS,B1ES,FLTO,FLTC,BOAO,FCAL"
#define JOB_FIELDS "ACFR,ACTO,DETY,HOPO,STAT,UAFT,UJTY,URNO,UTPL,PLFR,PLTO"
#define CCA_FIELDS "CKBA,CKBS,CKEA,CKES,FLNO,FLNU,STOD,URNO"
#define SRH_FIELDS "ACTG,ACTU,ADID,ALTG,ALTU,APTG,APTU,NAME,NATG,NATU,UHSS,URNO,DELF,MAXT"
#define SGM_FIELDS "URNO,USGR,UVAL,HOPO"
#define OST_FIELDS "CDAT,CONA,CONS,COTY,RFLD,RTAB,RURN,SDAY,UAFT,UHSS,URNO,USDE,USEC,USRH,CONU"
/* #define OST_ADDFIELDS "RETY,TIFR,LEVL,UTPL,AFTF,FLT2"  */  
#define OST_ADDFIELDS "RETY,TIFR,LEVL,UTPL,AFTF,FLT2,TSKN,FLDR,ALRT" /* AM 20071127 */   
#define SDE_FIELDS "AFTF,CCAF,FLDR,NAME,RETY,RTIM,SDEU,TIFR,URNO,USRH,UTPL,DELF,TSKN,ALRT"
#define EVAL_FIELDS "RKEY,OURI,OURO,UHSS,SRHI,SRHO"
#define AF1_FIELDS "URNO,FCA1,FCA2,FLNU"
#define RSM_FIELDS "URNO,RURN,TSKN,SDST,SDED,ACST,ACED,PROG,STAT" /* AM 20071127 */  
#define HSS_FIELDS "URNO,NAME" /* AM 20080305 */ 

#define ACT_FIELDS "GA1X,GD1X,GA1Y,GD1Y,B1BA,B1EA,ACFR,ACTO,CKBA,CKEA"
#define NOM_FIELDS "GA1B,GD1B,GA1E,GD1E,B1BS,B1ES,PLFR,PLTO,CKBS,CKES"

#define AFT_INBOUND_FIELDS "TIFA,STOA,ETAI,TMOA,LAND,ONBE,ONBL,GA1X,GA1Y,B1BA,B1EA,GA1B,GA1E,B1BS,B1ES"

#define INBOUNDTIMES "TIFA,STOA,ETAI,TMOA,LAND,ONBE,ONBL"
#define IN_TIME_CNT 7
#define OUTBOUNDTIMES "TIFD,STOD,ETDI,OFBL,AIRB"
#define OUT_TIME_CNT 5
#define RC_DONT_CARE	-777

#define EVT_IN		1
#define EVT_OUT		2
#define EVT_TURN	3

#define FOR_INSERT 1
#define FOR_UPDATE 2
#define FOR_DELETE 3

#define MAX_NO_OF_ALERT_CHARS 26

int debug_level = 0;
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static EVENT *prgOutEvent  = NULL;

static char  cgProcessName[20] = "\0";
static char  cgConfigFile[512] = "\0";
static char  cgHopo[8] = "\0";                         /* default home airport    */
static char  cgTabEnd[8] ="\0";                       /* default table extension */
static char  cgTwEnd [33];

static long lgEvtCnt = 0;
static int  igStartUpMode = TRACE;
static int  igRuntimeMode = 0;

static time_t tgLoadStart;
static time_t tgLoadEnd;
static time_t tgCurrTime;
static time_t tgWorkTime;
static long lgRSMUpdOffStart = 240 * 60; /* AM 20080304, BST: Modified Long Minutes */  
static long lgRSMUpdOffEnd = 300 * 60; /* AM 20080304, BST */    

static char cgLoadStart[20];
static char cgLoadEnd[20];
static BOOL bgSingleBC = FALSE;
static BOOL bgSbcUaft = FALSE;
static BOOL bgSbcSday = FALSE;
static int  igMinUaftInSbc = 1;
static int  igMaxUaftInSbc = 100;
static char cgMinAftTime[20]="", cgMaxAftTime[20]="";
static char cgChangedRkeys[101]="";
static BOOL bgSkipFillOstRecord = FALSE; /* AM 20071011 */    
static BOOL bgUseRsm = TRUE; /* AM 20071127. To use the RSMTAB information */   
static char cgSetAlertChars[32]; /* AM 20071218. Chars to set the alert */   
static char cgUnSetAlertChars[32]; /* AM 20071218. Chars to unset the alert  */   
static char cgAckAlertChars[32]; /* AM 20071218. Chars to set the acknowledged alert */  

struct _arrayinfo
{
	HANDLE   rrArrayHandle;
	char     crArrayName[ARR_NAME_LEN+1];
	char	   crTableName[ARR_NAME_LEN+1];
	char     crArrayFieldList[ARR_FLDLST_LEN+1];
	long     lrArrayFieldCnt;
	char    *pcrArrayRowBuf;
	long     lrArrayRowLen;
	long    *plrArrayFieldOfs;
	long    *plrArrayFieldLen;
	HANDLE   rrIdxHandle[ARR_MAX_IDX];
	char     crIdxName	[ARR_MAX_IDX][ARR_NAME_LEN+1];
	char     crIdxFields[ARR_MAX_IDX][ARR_FLDLST_LEN+1];
	long     lrIdxLength[ARR_MAX_IDX];
};
typedef struct _arrayinfo ARRAYINFO;

typedef struct
{
	char crChar[2];
	int  crRecCnt;
	int  crNCnt;
	int  crACnt;
	int  crOthCnt;
}ALERT_CHAR_INFO; /* To keep the alert information */   

typedef struct
{
	char crUAFT[32];
	ALERT_CHAR_INFO crAlertChar[MAX_NO_OF_ALERT_CHARS];
	int   crAlertCharCnt;
}FLIGHT_ALERT_CHARS; /* To keep the flight alert chars information */   

FLIGHT_ALERT_CHARS rgFlightAlertChars;

ARRAYINFO rgAftArray;
ARRAYINFO rgAf1Array;
ARRAYINFO rgReqAftArray;
ARRAYINFO rgJobArray;
ARRAYINFO rgCcaArray;
ARRAYINFO rgSrhArray;
ARRAYINFO rgSdeArray;
ARRAYINFO rgSgmArray;
ARRAYINFO rgEvalResults;
ARRAYINFO rgOstArray;
ARRAYINFO rgNewOstArr;
ARRAYINFO rgRsmArray;/* AM 20071127 */
ARRAYINFO rgHssArray;/* AM 20080305 */

ARRAYINFO rgBasicData[COND_COUNT];
int rgCndFldIdxSgl[COND_COUNT];
int rgCndFldIdxGrp[COND_COUNT];
int rgAftInbFldIdx[COND_COUNT];
int rgAftOutFldIdx[COND_COUNT];
int rgInbTimeIdx[IN_TIME_CNT];
int rgOutbTimeIdx[OUT_TIME_CNT ];

static int   igReservedUrnoCnt = 0;
static long  lgActUrno =0 ;
static char  cgCurrentTime[21]="" ;
static char  cgFlagActive = '1';
static char  cgTdi1[24]="";
static char  cgTdi2[24]="";
static char  cgTich[24]="";

static char  cgBcData[MAX_UAFTINBC*11] = "";
static char  cgBcField[5] = "";

#define AFTFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgAftArray.plrArrayFieldOfs[ipFieldNo-1]])
#define AF1FIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgAf1Array.plrArrayFieldOfs[ipFieldNo-1]])
#define JOBFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgJobArray.plrArrayFieldOfs[ipFieldNo-1]])
#define SRHFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgSrhArray.plrArrayFieldOfs[ipFieldNo-1]])
#define SDEFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgSdeArray.plrArrayFieldOfs[ipFieldNo-1]])
#define OSTFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgOstArray.plrArrayFieldOfs[ipFieldNo-1]])
#define CCAFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgCcaArray.plrArrayFieldOfs[ipFieldNo-1]])
#define EVALFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgEvalResults.plrArrayFieldOfs[ipFieldNo-1]])
#define RSMFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgRsmArray.plrArrayFieldOfs[ipFieldNo-1]])/* AM 20071127 */   
#define HSSFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgHssArray.plrArrayFieldOfs[ipFieldNo-1]])/* AM 20080305 */  

static int igHssUrnoIdx;
static int igHssNameIdx;

static int igSrhAdidIdx;
static int igSrhMaxtIdx;
static int igSrhNameIdx;
static int igSrhUhssIdx;
static int igSrhUrnoIdx;

static int igSdeAdidIdx;
static int igSdeAftfIdx;
static int igSdeFldrIdx;
static int igSdeLevlIdx;
static int igSdeRetyIdx;
static int igSdeSdeuIdx;
static int igSdeTifrIdx;
static int igSdeUrnoIdx;
static int igSdeUsrhIdx;
static int igSdeUtplIdx;
static int igSdeTsknIdx;/* AM 20071128 */ 
static int igSdeAlrtIdx;/* AM 20071221 */   

static int igOstAftfIdx;
static int igOstConaIdx;
static int igOstConsIdx;
static int igOstConuIdx;
static int igOstCotyIdx;
static int igOstFlt2Idx;
static int igOstRetyIdx;
static int igOstRfldIdx;
static int igOstRtabIdx;
static int igOstRurnIdx;
static int igOstSdayIdx;
static int igOstTifrIdx;
static int igOstUrnoIdx;
static int igOstUaftIdx;
static int igOstUsdeIdx;
static int igOstUhssIdx;
static int igOstUsrhIdx;
static int igOstUtplIdx;
static int igOstLevlIdx;
static int igOstUsecIdx;/* AM 20070912 */  
static int igOstTsknIdx;/* AM 20071127   Index Number of 'Task Name' in OstArray */ 
static int igOstFldrIdx;/* AM 20071205   Index Number of 'FLDR' (Associated with SDETAB) in OstArray */  
static int igOstAlrtIdx;/* AM 20071221   Index Number of 'ALRT' (Associated with SDETAB) in OstArray */   

static int igAftUrnoIdx;
static int igAftAdidIdx;
static int igAftStoaIdx;
static int igAftStodIdx;
static int igAftFlnoIdx;
static int igAftRkeyIdx;
static int igAftTifaIdx;
static int igAftTifdIdx;

static int igAf1Fca1Idx;
static int igAf1Fca2Idx;
static int igAf1FlnuIdx;

static int igCcaCkbaIdx;
static int igCcaCkeaIdx;
static int igCcaFlnuIdx;
static int igCcaCkbsIdx;
static int igCcaCkesIdx;

static int igJobAcfrIdx;
static int igJobActoIdx;
static int igJobStatIdx;
static int igJobUaftIdx;
static int igJobUtplIdx;
static int igJobPlfrIdx;
static int igJobPltoIdx;

static int igEvalOuriIdx; 
static int igEvalOuroIdx;
static int igEvalSrhiIdx;
static int igEvalSrhoIdx;
static int igEvalUhssIdx;

/* Field index Number for RSMArray :: AM 20071127 */  
static int igRsmRurnIdx;/* AM 20071127 */ 
static int igRsmTsknIdx;/* AM 20071127 */ 
static int igRsmSdstIdx;
static int igRsmSdedIdx;
static int igRsmAcstIdx;
static int igRsmAcedIdx;
static int igRsmProgIdx;
static int igRsmStatIdx;

static int igCountOstUaft = 0;
static char pcgShortUaft[2048] = "";
static BOOL bgCheckAndRefillRSMData = TRUE;/*AM 20081105 - To Check and Refill RSM Data */

/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
static int  Init_Stahdl();
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,
			   char *pcpCfgBuffer);

static int	Reset(void);                       /* Reset program          */
static void	Terminate(int ipSleep);            /* Terminate program      */
static void	HandleSignal(int);                 /* Handles signals        */
static void	HandleErr(int);                    /* Handles general errors */
static void   HandleQueErr(int);                 /* Handles queuing errors */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/


static int	HandleData(EVENT *prpEvent);       /* Handles event data     */
static int GetRowLength(char *pcpTana, char *pcpFieldList, long *plpLen);
static int GetFieldLength(char *pcpTana, char *pcpFina, long *plpLen);
static int SetArrayInfo(char *pcpArrayName, char *pcpTableName, 
						char *pcpArrayFieldList,char *pcpAddFields,
						long *pcpAddFieldLens,char *pcpSelection, 
						ARRAYINFO *prpArrayInfo, short spFlags );
static int CreateIdx ( ARRAYINFO *prpArrayInfo, char *pcpIdxName,
					   char *pcpFields, char *pcpOrdering, int ipIdx );
static int GetTotalRowLength( char *pcpFieldList, ARRAYINFO *prpArray, long *plpLen);
static int SaveIndexInfo (ARRAYINFO *prpArrayInfo, int ipMode, int ipLogFile );
static int CheckArrayInfo (ARRAYINFO *prpArrayInfo, int ipMode, int ipLogFile );
static int StrToTime(char *pcpTime,time_t *plpTime);
static int TimeToStr(char *pcpTime,time_t lpTime);
static int EvaluateRules ( ARRAYINFO* prpFlightArray, char *pcpUhss );
static int EvaluateFlight ( char *pcpUrnoList );
static int IniPriority ( char * pcpUrno );
static int InitFieldIndices();
static BOOL CheckSingleValue ( ARRAYINFO* prpBasicData, char *pcpReqUrno,
							   char * pcpActValue );
static BOOL CheckGroupValue ( ARRAYINFO* prpBasicData, char *pcpReqGrpUrno,
							   char * pcpActValue );
static int SetAATArrayInfo ( char *pcpArrayName,char *pcpArrayFieldList, 
						     long *plpFieldLens, ARRAYINFO *prpArrayInfo );
static int CreateSkeleton ( ARRAYINFO* prpFlightArray );
static int FillOstRecord (ARRAYINFO* prpFlightArray, char *pcpBuf, int ipBufLen,
						  char *pcpSdePtr, char *pcpOuri, char *pcpOuro, char *pcpUhss);
void TrimRight(char *s);
static int GetNextUrno(char *pcpUrno);
static int UpdateOSTTAB ( ARRAYINFO* prpFlightArray );
static int PrepareOSTArray ( char *pcpUaft, char *pcpUhss/*, BOOL bpSave*/ );
static int SetCONS ( ARRAYINFO *prpFlightArray, char *pcpUaft, char *pcpUhss, 
					 BOOL bpSave );
static int GetStatusTime ( char *pcpUost, char *pcpTime );
static int GetFlightTime ( ARRAYINFO *prpFlightArray, char *pcpUaft, 
						   char *pcpFld, char *pcpTime );
static int SendAnswer(EVENT *prpEvent,int ipRc);
static int ProcessCFS ( char *pcpAftSelection );
static int ProcessSCC( char *pcpUaft, char *pcpUhss );
static int SaveOstTab ();
static int GetConaFlight ( char *pcpUaft, char *pcpRfld, char* pcpCona );
static int GetConaJob ( char *pcpUaft, char *pcpUtpl, char* pcpCona, char *pcpRety,
					    char *pcpUaft2, char *pcpConaTurn );
static int GetConaCca ( char *pcpUaft, char* pcpCona, char *pcpRety );
static int GetFieldByUrno ( ARRAYINFO *prpArray, int ipFldIdx, char *pcpUrno, char *pcpBuf );
static int HandleCCAUpdate ( char *pcpUaft );
static int HandleJobUpdate ( char *pcpUaft, char *pcpUtpl );
static int IsAftChangeImportant ( char *pcpFields, char *pcpData, BOOL *pbpDoEval, 
								  BOOL  *pbpSetCons, BOOL  *pbpSetCona );
static int SetInitialCONA ( char *pcpUaft, char *pcpUhss );
static int UtcToLocal(char *pcpTime);
static BOOL IsRuleValid(char *pcpUrno, char *pcpUTCDate );
static int InitTichTdi();
static int UpdateMinMax ( char *pcpTime );
static int GetNomField ( char *pcpActField, char *pcpNomField, char *pcpRety );
static int GetConsJob ( char *pcpUaft, char *pcpUaft2, char *pcpUtpl, char* pcpCons, char *pcpRety );
static int GetConsCca ( char *pcpUaft, char* pcpCons, char *pcpRety );
static int UpdateStatus ( long lpOldRow, char *pcpOstOld, char *pcpOstNew );
static int Find2ndFlight ( ARRAYINFO *prpFlightArr, char *pcpAftRow1, char **pcpAftRow2 );
static BOOL CheckOneRule ( char *pcpRule, char *pcpInbound, char *pcpOutbound, long lpActGroundTime );
static int CreateSkeleton4Rule ( ARRAYINFO *prpFlights, char *pcpRule, 
								 char *pcpOuri, char *pcpOuro, char *pcpUhss );
static int PrepareSdeArray ( char *pcpUsrh );
static int GetFlt2Value ( ARRAYINFO *prpFlightArr, char *pcpUaft1, 
						  char *pcpUsrh, char *pcpUaft2 );
static int Get2ndFlightUrno ( ARRAYINFO *prpFlightArr, char *pcpUaft1, char *pcpUaft2 );
static int HandleAf1Update ( char *pcpFlnu );

static int PrepareSBC ( char *pcpUaft, ARRAYINFO *prpAftArray );/* AM 20071128 */  
static int GetDebugLevel(char *pcpMode, int *pipMode);/* AM 20071128 */ 
static int SetConaForFlight ( char *pcpUaft, char *pcpUhss );/* AM 20071128 */  

static int HandleRsmUpdate ( char *pcpUaft, char *pcpTskn, char *pcpRsmUrno );
static int GetConsRsm( char* pcpUaft, char* pcpTskn , char* pcpFldr, char* pcpTime );
static int GetRsmFieldValue( char *pcpUaft, char *pcpTskn, 
						char* pcpRsmFldName, char* pcpValue );
						static int GetRsmInfo ( char *pcpUaft, char *pcpTskn, 
						char* pcpSchStart, char* pcpActStart,
						char* pcpSchEnd, char* pcpActEnd, 
						char* pcpProg, char* pcpStat );
static char* ToUpper( char* pcpSt );
static int ComputeAllAlertCharsForAFlight( char* pcpUAFT, char* pcpChgChar );
static int ComputeAllAlertCharsForOstRecords( char* pcpOstKeys );
static int AddAlertCharToString( char* pcpString, char* pcpCharToAdd);
static int Get3Fields_ByUrno ( ARRAYINFO *prpArray, char *pcpUrno, 
		int ipFldIdx1, char *pcpBuf1,
		int ipFldIdx2, char *pcpBuf2,
		int ipFldIdx3, char *pcpBuf3 );
static int EvalAlert( ALERT_CHAR_INFO* prpAlertChar, 
				char* pcpSetAlertChars, char* pcpUnsetAlertChars );		

static int PrepareIconChars( char* pcpFldName, char* pcpData, char* pcpCurIconChars, char* pcpNewIconChars );
static int ProcessIconChars( char* pcpUaft, char* pcpFldName, char* pcpData );

static int BuildSqlFldStrg (char *pcpSqlStrg, char *pcpFldLst, int ipTyp);
static int HandleGocSelect(char *pcpTable, char *pcpSqlKey, char *pcpSqlFld, char *pcpSqlDat);
static int HandleGocInsert(char *pcpTable, char *pcpSqlFld, char *pcpSqlDat);
static int HandleGocUpdate(char *pcpTable, char *pcpSqlKey, 
                           char *pcpFld, char *pcpNewDat, char *pcpCurDat, 
                           char *pcpUpdatedFld, char *pcpUpdatedDat);
static int ProcessRSMSchTimeUpdate();   

static int ProcessCONSChanges( char *pcpUOst, char *pcpUaft, char *pcpUsde, char* pcpUsrh, char *pcpUhss, 
             char *pcpCons, char *pcpTskn, char *pcpRety );
static int GetFlightInfo( char *pcpUaft, char *pcpFlno, char *pcpAdid, char *pcpFlDt );
static int GetRuleName( char *pcpUsrh, char *pcpRuleName );
static int GetSectionName( char *pcpUhss, char *pcpName );                     
static void TrimAndFilterCr(char *pclTextBuff,char *pcpFldSep, char *pcpRecSep);
static int CheckRuleNameFilter(char *pcpRuleName, char *pcpUaftArr, char *pcpUaftDep);

/* MSI: 20080624: Added Declaration for New Evaluation Command */    
static int RefillArraysForEvaluation(char *pcpOffsetStart, char *pcpOffsetEnd);   

BOOL IS_EMPTY (char *pcpBuf) 
{
	return	(!pcpBuf || *pcpBuf==' ' || *pcpBuf=='\0') ? TRUE : FALSE;
}

/*	get actual date in UTC, if test date is configured, 
	act. date = day(test date) + time (now)	*/
static void GetActDate ( char *pcpNow )
{
	GetServerTimeStamp("UTC", 1, 0, pcpNow );
	if ( cgCurrentTime[0] )		/* if test date filled copy day of test date */
		strncpy ( pcpNow, cgCurrentTime, 8 );

}


/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
	int	ilRc = RC_SUCCESS;			/* Return code			*/
	int	ilCnt = 0;
	int ilOldDebugLevel = 0;


	INITIALIZE;			/* General initialization	*/


	debug_level = TRACE;

	strcpy(cgProcessName,argv[0]);

	dbg(TRACE,"MAIN: version <%s>",mks_version);

	/* Attach to the MIKE queues */
	do
	{
		ilRc = init_que();
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		}/* end of if */
	}while((ilCnt < 10) && (ilRc != RC_SUCCESS));

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
		sleep(60);
		exit(1);
	}else{
		dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
	}/* end of if */

	do
	{
		ilRc = init_db();
		if (ilRc != RC_SUCCESS)
		{
			check_ret(ilRc);
			dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		} /* end of if */
	} while((ilCnt < 10) && (ilRc != RC_SUCCESS));

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
		sleep(60);
		exit(2);
	}else{
		dbg(TRACE,"MAIN: init_db() OK!");
	} /* end of if */

	/* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */

	sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
	dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
	ilRc = TransferFile(cgConfigFile);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */

	sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
	dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
	ilRc = TransferFile(cgConfigFile);
	if(ilRc != RC_SUCCESS)
	{ 
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */

	ilRc = SendRemoteShutdown(mod_id);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
	} /* end of if */

	if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
	{
		dbg(DEBUG,"MAIN: waiting for status switch ...");
		HandleQueues();
	}/* end of if */

	if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
	{
		dbg(TRACE,"MAIN: initializing ...");
			ilRc = Init_Stahdl(); 
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"Init_Stahdl: init failed!");
			} /* end of if */

	} else {
		Terminate(30);
	}/* end of if */



	dbg(TRACE,"=====================");
	dbg(TRACE,"MAIN: initializing OK");
	dbg(TRACE,"=====================");

	debug_level = igRuntimeMode;


	for(;;)
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;
				
		if( ilRc == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRc != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRc);
			} /* fi */

			
			lgEvtCnt++;

			switch( prgEvent->command )
			{
				case	HSB_STANDBY	:
					ctrl_sta = prgEvent->command;
					HandleQueues();
					break;	
				case	HSB_COMING_UP	:
					ctrl_sta = prgEvent->command;
					HandleQueues();
					break;	
				case	HSB_ACTIVE	:
					ctrl_sta = prgEvent->command;
					break;	
				case	HSB_ACT_TO_SBY	:
					ctrl_sta = prgEvent->command;
					/* CloseConnection(); */
					HandleQueues();
					break;	
				case	HSB_DOWN	:
					/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
					ctrl_sta = prgEvent->command;
					Terminate(1);
					break;	
				case	HSB_STANDALONE	:
					ctrl_sta = prgEvent->command;
					ResetDBCounter();
					break;	
				case	REMOTE_DB :
					/* ctrl_sta is checked inside */
					HandleRemoteDB(prgEvent);
					break;
				case	SHUTDOWN	:
					/* process shutdown - maybe from uutil */
					Terminate(1);
					break;
						
				case	RESET		:
					ilRc = Reset();
					break;
						
				case	EVENT_DATA	:
					if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
					{
						ilRc = HandleData(prgEvent);
						/*
						if(ilRc != RC_SUCCESS)
						{
							HandleErr(ilRc);
						}*/
					}
					else
					{
						dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
						DebugPrintItem(TRACE,prgItem);
						DebugPrintEvent(TRACE,prgEvent);
					}/* end of if */
					break;
					

				case	TRACE_ON :
					dbg_handle_debug(prgEvent->command);
					break;
				case	TRACE_OFF :
					dbg_handle_debug(prgEvent->command);
					break;
				case 	100:
					ilRc = RC_SUCCESS;
					for ( ilCnt=0; ilCnt<COND_COUNT; ilCnt++ )
						if ( rgBasicData[ilCnt].rrArrayHandle >= 0 )
							ilRc |= SaveIndexInfo ( &(rgBasicData[ilCnt]), 0, 0 );
					break;
				case	101:
					ilRc = SaveIndexInfo ( &rgAftArray, 0, 0 );
					ilRc = SaveIndexInfo ( &rgAf1Array, 0, 0 );
					break;
				case	102:
					ilRc = SaveIndexInfo ( &rgJobArray, 1, 0 );
					break;
				case	103:
					ilRc = SaveIndexInfo ( &rgSgmArray, 1, 0 );
					break;
				case	104:
					ilRc = SaveIndexInfo ( &rgSrhArray, 1, 0 );
					break;
				case	105:
					ilRc = SaveIndexInfo ( &rgSdeArray, 1, 0 );
					break;
				case	106:
					ilRc = SaveIndexInfo ( &rgEvalResults, 1, 0 );
					break;
				case	107:
					ilRc = SaveIndexInfo ( &rgOstArray, 1, 0 );
					break;
				case	108:
					ilRc = SaveIndexInfo ( &rgNewOstArr, 1, 0 );
					break;
				case	109:/* AM 20071127 */  
					if (bgUseRsm==TRUE)
					{
						ilRc = SaveIndexInfo ( &rgRsmArray, 1, 0 );
					}
					break;
				case    110:
				    ilRc = SaveIndexInfo ( &rgHssArray, 1, 0 );
				    break;
				case    777:
					ProcessRSMSchTimeUpdate();
					ilRc = RC_SUCCESS;
				    	break;
				case    778:
					ilRc = CheckArrayInfo ( &rgOstArray, 1, 0 );
					ilRc = RC_SUCCESS;
				    	break;
				default			:
					dbg(TRACE,"MAIN: unknown event");
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
					break;
			} /* end switch */
		} 
		else 
		{
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
		
	} /* end for */
	
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/



static int ReadConfigEntry(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer)
{
    int ilRc = RC_SUCCESS;

    char clSection[124] = "\0";
    char clKeyword[124] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRc = iGetConfigEntry(cgConfigFile,clSection,clKeyword,
			   CFG_STRING,pcpCfgBuffer);
    if(ilRc != RC_SUCCESS)
    {
	dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
    } 
    else
    {
	dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s",
	    clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
    }/* end of if */
    return ilRc;
}


static int Init_Stahdl()
{
	int  ilRc = RC_SUCCESS;			/* Return code */
	char clSection[64] = "\0";
	char clKeyword[64] = "\0";
        long pllAddFieldLens[10];
	char clFields[256] = "\0";
	char pclSqlBuf[2560] = "\0";
	char clSelection[2048] = "\0";
	char clValue[124],clCode[41];
	char clStartDay[9],clEndDay[9];
	char clBasicTables[101]="";
 	short slCursor = 0;
	short slSqlFunc = 0;
	time_t tlCurrTime;
	int  ilLoadOffSetStart = -3600 * 12;
	int  ilLoadOffSetEnd = 3600 * 24;
	int  ilOffSet = 0;
        int  ilCnt = 0, ilDummy;
        dbg(TRACE,"Init_Stahdl....");
	if(ilRc == RC_SUCCESS)
	{
		/* read HomeAirPort from SGS.TAB */
		ilRc = tool_search_exco_data("SYS", "HOMEAP", cgHopo);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"Init_Stahdl: EXTAB,SYS,HOMEAP not found in SGS.TAB");
			Terminate(30);
		} else {
			dbg(TRACE,"Init_Stahdl: home airport <%s>",cgHopo);
		}
		InitTichTdi();
	}

	if(ilRc == RC_SUCCESS)
	{
		ilRc = tool_search_exco_data("ALL","TABEND", cgTabEnd);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"Init_Stahdl: EXTAB,ALL,TABEND not found in SGS.TAB");
			Terminate(30);
		} else {
			dbg(TRACE,"Init_Stahdl: table extension <%s>",cgTabEnd);
		}
	}

	sprintf (cgTwEnd, "%s,%s,%s", cgHopo,cgTabEnd, cgProcessName);
	if(ilRc == RC_SUCCESS)
	{
		 GetDebugLevel("STARTUP_MODE", &igStartUpMode);
		 GetDebugLevel("RUNTIME_MODE", &igRuntimeMode);
	}
	debug_level = igStartUpMode;

	sprintf(clSection,"MAIN");
	sprintf(clKeyword,"CURRENT_TIME");
	if(ReadConfigEntry(clSection,clKeyword,clValue) != RC_SUCCESS)
	{
		GetServerTimeStamp("UTC", 1, 0, clValue);
	}
	else
	{
		strncpy ( cgCurrentTime, clValue, 14 );
		cgCurrentTime[14] = '\0';
	}
        StrToTime(clValue,&tlCurrTime);

	sprintf(clKeyword,"LOAD_OFFSET_START");  
	if(ReadConfigEntry(clSection,clKeyword,clValue) == RC_SUCCESS)
	{
	    ilOffSet = atoi(clValue);
	    ilLoadOffSetStart = (int) ilOffSet*60*60;
	}

	sprintf(clKeyword,"LOAD_OFFSET_END");  
	if(ReadConfigEntry(clSection,clKeyword,clValue) == RC_SUCCESS)
	{
	    ilOffSet = atoi(clValue);
	    ilLoadOffSetEnd = (int) ilOffSet*60*60;
	}
	tgLoadStart = tlCurrTime + ilLoadOffSetStart;
	tgLoadEnd = tlCurrTime + ilLoadOffSetEnd;
	TimeToStr(cgLoadStart,tgLoadStart);
	TimeToStr(cgLoadEnd,tgLoadEnd);
	
	/* AM 20080304 : Get the RSM Schedule Time Updating Offset Start and End */ 
	/* AM 20080304 : Start */ 
	sprintf(clKeyword,"RSMSCH_OFFSET_START");
	if(ReadConfigEntry(clSection,clKeyword,clValue)==RC_SUCCESS)
	{
		lgRSMUpdOffStart = atol( clValue ) * 60;
	}
	
	sprintf(clKeyword,"RSMSCH_OFFSET_END");
	if(ReadConfigEntry(clSection,clKeyword,clValue)==RC_SUCCESS)
	{
		lgRSMUpdOffEnd = atol( clValue ) * 60;
	}
	/* AM 20080304 : Finish */  

	/*  BC_TYPE possible values: SINGLE, SBCUAFT, SBCSDAY */
	sprintf(clKeyword,"BC_TYPE");
	if(ReadConfigEntry(clSection,clKeyword,clValue) == RC_SUCCESS)
	{
		if ( strstr ( clValue, "SINGLE" ) )
			bgSingleBC = TRUE;
		if ( strstr ( clValue, "SBCUAFT" ) )
			bgSbcUaft = TRUE;
		if ( strstr ( clValue, "SBCSDAY" ) )
			bgSbcSday = TRUE;
	}
	else
		bgSingleBC = bgSbcUaft = bgSbcSday = TRUE;
	dbg ( TRACE, "Init_Stahdl: BC-Types Single <%d> SBCUAFT <%d> SBCSDAY <%d>", 
		  bgSingleBC, bgSbcUaft, bgSbcSday );
    
	sprintf(clKeyword,"MAX_UAFTINBC");
	if(ReadConfigEntry(clSection,clKeyword,clValue) == RC_SUCCESS)
	{
		if ( ( sscanf ( clValue, "%d", &ilDummy ) > 0 ) && ( ilDummy > 0 ) )
			igMaxUaftInSbc = min( ilDummy, MAX_UAFTINBC ); 
	}
	sprintf(clKeyword,"MIN_UAFTINBC");
	if(ReadConfigEntry(clSection,clKeyword,clValue) == RC_SUCCESS)
	{
		if ( ( sscanf ( clValue, "%d", &ilDummy ) > 0 )  && ( ilDummy <= igMaxUaftInSbc ) )
			igMinUaftInSbc = ilDummy; 
	}
	dbg ( TRACE, "Init_Stahdl: MIN_UAFTINBC <%d> MAX_UAFTINBC <%d>", 
		  igMinUaftInSbc, igMaxUaftInSbc );

	dbg ( DEBUG, "Init_Stahdl: Going to Create Arrays ilRc <%d>", ilRc );
        if(ilRc == RC_SUCCESS)
        {
		ilRc = CEDAArrayInitialize(20,10);
		if(ilRc != RC_SUCCESS)
		    dbg(TRACE,"Init_Stahdl: CEDAArrayInitialize failed <%d>",ilRc);
        }/* end of if */

	/*  Initialise Basic Data Tables from BASIC_DATA_TAB */
	if(ilRc == RC_SUCCESS)
	{
		for ( ilCnt=0; (ilRc == RC_SUCCESS) && (ilCnt<COND_COUNT); ilCnt++ )
		{
			strcpy ( clFields, "URNO," );
			if ( get_real_item(clCode,BASIC_DATA_FLD,ilCnt+1 ) > 0 )
				strcat ( clFields, clCode);
			else
			{
				dbg ( TRACE, "Init_Stahdl: Unable to extract Basic Data Field no. <%d>", ilCnt+1 );
				continue;
			}
			if ( get_real_item(clValue,BASIC_DATA_TAB,ilCnt+1 ) <= 0 )
			{
				dbg ( TRACE, "Init_Stahdl: Unable to extract Basic Data Table no. <%d>", ilCnt+1 );
				continue;
			}			
			dbg ( DEBUG, "Init_Stahdl: SetArrayInfo Table <%s> Fields <%s>", 
				  clValue, clFields );
			ilRc = SetArrayInfo( clValue, clValue, clFields,0, 0, "",
								 &(rgBasicData[ilCnt]), CEDAARRAY_ALL );
			if ( ilRc != RC_SUCCESS )
			{
				dbg ( TRACE, "Init_Stahdl: SetArrayInfo Table <%s> Fields <%s> failed Rc <%d>", 
					  clValue, clFields, ilRc );
			}
			else
			{
				ilRc = CreateIdx ( &(rgBasicData[ilCnt]), "IDXCODE", clCode, "A", 1 );
				if (ilRc!=RC_SUCCESS)
					dbg(TRACE,"Init_discal: CreateIdx for <%s> failed <%d>",clValue, ilRc);		

				if ( clBasicTables[0] ) 
					strcat ( clBasicTables, "," );
				sprintf ( &(clBasicTables[strlen(clBasicTables)]), "'%s'", clValue );
			}
		}
	}      

	/*  Initialise Flights AFTTAB */
	if(ilRc == RC_SUCCESS)
	{
		sprintf(clSelection,"WHERE (((TIFA BETWEEN '%s' AND '%s') AND DES3 = '%s') OR ((TIFD BETWEEN '%s' AND '%s') AND ORG3 = '%s'))",
				cgLoadStart,cgLoadEnd,cgHopo,cgLoadStart,cgLoadEnd,cgHopo);

		dbg ( DEBUG, "Init_Stahdl: Selection for CEDAArrayFill AFTTAB: <%s>", clSelection );
		ilRc = SetArrayInfo( "AFT", "AFT", AFT_FIELDS, 0, 0, clSelection, 
							 &rgAftArray, CEDAARRAY_ALL );
		if ( ilRc != RC_SUCCESS )
		{
			dbg ( TRACE, "Init_Stahdl: SetArrayInfo <AFT> failed Rc <%d>", ilRc );
		}
		sprintf(clSelection,
				"(((TIFA >= %s)  && (TIFA <= %s)  && (DES3 == %s)) || ((TIFD >= %s)  && (TIFD <= %s)  && (ORG3 == %s))) ",
				cgLoadStart,cgLoadEnd,cgHopo,cgLoadStart,cgLoadEnd,cgHopo);
		dbg(TRACE,"Init_Stahdl: Filter for AFTTAB <%s>", clSelection );
		ilRc = CEDAArraySetFilter(&rgAftArray.rrArrayHandle,
								  rgAftArray.crArrayName,clSelection );
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"Init_Stahdl: CEDAArraySetFilter AFTTAB failed <%d>",ilRc);
		}
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CreateIdx ( &rgAftArray, "IDXAFT1", "RKEY,ADID", "A,A", 1 );
		if (ilRc!=RC_SUCCESS)
			dbg(TRACE,"Init_discal: CreateIdx for AFT failed <%d>",ilRc);		
	}
	/*  Initialise Array for Flights to be evaluated */
	if(ilRc == RC_SUCCESS)
	{
		ilRc = SetArrayInfo( "REQAFT", "AFT", AFT_FIELDS, 0, 0, "", 
								&rgReqAftArray, CEDAARRAY_URNOIDX  );
		if ( ilRc != RC_SUCCESS )
		{
			dbg ( TRACE, "Init_Stahdl: SetArrayInfo <REQAFT> failed Rc <%d>", ilRc );
		}
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CreateIdx ( &rgReqAftArray, "IDXREQAFT1", "RKEY,ADID", "A,A", 1 );
		if (ilRc!=RC_SUCCESS)
			dbg(TRACE,"Init_discal: CreateIdx for REQAFT failed <%d>",ilRc);		
	}

	/*  AF1TAB, extension of AFTTAB */
	if(ilRc == RC_SUCCESS)
	{
		sprintf(clSelection,"WHERE flnu in (select urno from afttab where (TIFD BETWEEN '%s' AND '%s') AND ADID!='A' AND ORG3 = '%s')",
				cgLoadStart,cgLoadEnd,cgHopo);

		dbg ( DEBUG, "Init_Stahdl: Selection for CEDAArrayFill AF1TAB: <%s>", clSelection );

		ilRc = SetArrayInfo( "AF1", "AF1", AF1_FIELDS, 0, 0, clSelection, &rgAf1Array, CEDAARRAY_ALL );
		if ( ilRc != RC_SUCCESS )
		{
			dbg ( TRACE, "Init_Stahdl: SetArrayInfo <AF1> failed Rc <%d>", ilRc );
		}
		if(ilRc == RC_SUCCESS)
		{
			ilRc = CreateIdx ( &rgAf1Array, "IDXAF1FLNU", "FLNU", "A", 1 );
			if (ilRc!=RC_SUCCESS)
				dbg(TRACE,"Init_discal: CreateIdx for IDXAF1FLNU failed <%d>",ilRc);		
		}
		ilRc = RC_SUCCESS;		/* process shall also work without AF1TAB */
	}
	/*  Initialise Jobs on Flights JOBTAB */
	if(ilRc == RC_SUCCESS)
	{
		sprintf(clSelection,"WHERE UJTY = '2000' AND ACTO > '%s' AND ACFR < '%s' AND HOPO = '%s'",
			   cgLoadStart,cgLoadEnd,cgHopo); 
		dbg ( DEBUG, "Init_Stahdl: Selection for CEDAArrayFill JOBTAB: <%s>", clSelection );

		ilRc = SetArrayInfo( "JOB", "JOB", JOB_FIELDS, 0, 0, clSelection,
							 &rgJobArray, CEDAARRAY_ALL );
		if ( ilRc != RC_SUCCESS )
		{
			dbg ( TRACE, "Init_Stahdl: SetArrayInfo <JOB> failed Rc <%d>", ilRc );
		}
	}
	if (ilRc == RC_SUCCESS)/* AM 20071127 */ 
	{/* Popluate RsmArray from RSMTAB */   
		if (bgUseRsm==TRUE)
		{
			sprintf(clSelection,"WHERE STDT >='%s' AND STDT <='%s' ",
					cgLoadStart,cgLoadEnd);

			dbg ( DEBUG, "Init_Stahdl: Selection for CEDAArrayFill RSMTAB: <%s>", clSelection );

			ilRc = SetArrayInfo( "RSM", "RSM", RSM_FIELDS, 0, 0, clSelection, &rgRsmArray, CEDAARRAY_ALL );
			if ( ilRc != RC_SUCCESS )
			{
				dbg ( TRACE, "Init_Stahdl: SetArrayInfo <RSM> failed Rc <%d>", ilRc );
			}
			if(ilRc == RC_SUCCESS)
			{
				ilRc = CreateIdx ( &rgRsmArray, "IDXRSM1", "RURN,TSKN", "A,A", 1 );
				if (ilRc!=RC_SUCCESS)
					dbg(TRACE,"Init_discal: CreateIdx for IDXRSM2 failed <%d>",ilRc);
			}
		}
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CreateIdx ( &rgJobArray, "IDXJOB1", "UAFT,UTPL", "A,A", 1 );
		if (ilRc!=RC_SUCCESS)
			dbg(TRACE,"Init_discal: CreateIdx for JOB failed <%d>",ilRc);		
	}
	/*  Initialise Counter Allocations CCATAB */
	if(ilRc == RC_SUCCESS)
	{
		sprintf(clSelection,"WHERE CKES > '%s' AND CKBS < '%s' AND HOPO = '%s'",
			   cgLoadStart,cgLoadEnd,cgHopo); 
		dbg ( DEBUG, "Init_Stahdl: Selection for CEDAArrayFill CCATAB: <%s>", clSelection );

		ilRc = SetArrayInfo( "CCA", "CCA", CCA_FIELDS, 0, 0, clSelection,
							 &rgCcaArray, CEDAARRAY_ALL );
		if ( ilRc != RC_SUCCESS )
		{
			dbg ( TRACE, "Init_Stahdl: SetArrayInfo <CCA> failed Rc <%d>", ilRc );
		}
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CreateIdx ( &rgCcaArray, "IDXCCA1", "FLNU,CKBA", "A,A", 1 );
		if (ilRc!=RC_SUCCESS)
			dbg(TRACE,"Init_discal: CreateIdx for CCA failed <%d>",ilRc);		
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CreateIdx ( &rgCcaArray, "IDXCCA2", "FLNU,CKEA", "A,D", 2 );
		if (ilRc!=RC_SUCCESS)
			dbg(TRACE,"Init_discal: CreateIdx for CCA failed <%d>",ilRc);		
	}
	/*  Initialise Static Group Members SGMTAB */
	if(ilRc == RC_SUCCESS)
	{
		sprintf(clSelection,"WHERE TABN IN (%s) AND HOPO = '%s'",
			    clBasicTables,cgHopo); 
		dbg ( DEBUG, "Init_Stahdl: Selection for CEDAArrayFill SGMTAB: <%s>", clSelection );

		ilRc = SetArrayInfo( "SGM", "SGM", SGM_FIELDS, 0, 0, clSelection,
							 &rgSgmArray, CEDAARRAY_FILL | CEDAARRAY_TRIGGER );
		if ( ilRc != RC_SUCCESS )
		{
			dbg ( TRACE, "Init_Stahdl: SetArrayInfo <SGM> failed Rc <%d>", ilRc );
		}
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CreateIdx ( &rgSgmArray, "IDXSGM1", "USGR,UVAL", "A,A", 1 );
		if (ilRc!=RC_SUCCESS)
			dbg(TRACE,"Init_discal: CreateIdx for SGM failed <%d>",ilRc);		
	}
	/*  Initialise Validity Module in dblib */
   	if (ilRc == RC_SUCCESS)
	{
		strcpy ( cgSelStr, "WHERE appl = 'STATMGR'" );
  		ilRc = InitValidityModule (CHKVAL_ARRAY, "VALTAB") ;
  		if (ilRc != RC_SUCCESS)
  		{
   			dbg (TRACE,"Init_Stahdl: InitValidityModule failed <%d>",ilRc) ;
   			ilRc = RC_SUCCESS ;
  		}
  		else
  		{
   			dbg (DEBUG,"Init_Stahdl: InitValidityModule OK") ;  

    			ilRc = ConfigureAction ("VALTAB", 0, 0 ) ;
    			if (ilRc != RC_SUCCESS)
     				dbg(TRACE,"Init_Stahdl: ConfigureAction failed <%d>", ilRc) ;
 		} /* end of if */
 	} /* end of if */
	/*  Initialise Status Rule Headers SRHTAB */
	if(ilRc == RC_SUCCESS)
	{
		pllAddFieldLens[0] = COND_COUNT;
		pllAddFieldLens[1] = 0;

		ilRc = SetArrayInfo( "SRH", "SRH", SRH_FIELDS, "PRIO", pllAddFieldLens, 
						"", &rgSrhArray, CEDAARRAY_ALL );
		if ( ilRc != RC_SUCCESS )
		{
			dbg ( TRACE, "Init_Stahdl: SetArrayInfo <SRH> failed Rc <%d>", ilRc );
		}
	}
	InitFieldIndices();
	IniPriority ( "" );
	
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CreateIdx ( &rgSrhArray, "IDXSRH1", "DELF,UHSS,ADID,PRIO,MAXT", "A,A,D,D,A", 1 );
		if (ilRc!=RC_SUCCESS)
			dbg(TRACE,"Init_Stahdl: CreateIdx for SRH failed <%d>",ilRc);		
	}
	/*  Initialise Status Definitions SDETAB */
	if(ilRc == RC_SUCCESS)
	{
		pllAddFieldLens[0] = 1;		/* ADID */
		pllAddFieldLens[1] = 2;		/* LEVL */
		pllAddFieldLens[2] = 0;
		
		ilRc = SetArrayInfo( "SDE", "SDE", SDE_FIELDS, "ADID,LEVL", 
							 pllAddFieldLens, "", &rgSdeArray, CEDAARRAY_ALL );
		if ( ilRc != RC_SUCCESS )
		{
			dbg ( TRACE, "Init_Stahdl: SetArrayInfo <SDE> failed Rc <%d>", ilRc );
		}
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CreateIdx ( &rgSdeArray, "IDXSDE1", "DELF,USRH,URNO", "A,A,A", 1 );
		if (ilRc!=RC_SUCCESS)
			dbg(TRACE,"Init_Stahdl: CreateIdx for SDE failed <%d>",ilRc);		
	}
	PrepareSdeArray ( "" );
	
	if (ilRc == RC_SUCCESS)
        {
	    ilRc = SetArrayInfo( "HSS", "HSS", HSS_FIELDS, 0, 0, "", &rgHssArray, CEDAARRAY_ALL );
	    if (ilRc!=RC_SUCCESS)
	    	dbg( TRACE,"Init_Stahdl: SetArrayInfo <HSS> failed Rc <%d>", ilRc );
        }

	/*  Initialise logical Array for rule evaluation results */
	if(ilRc == RC_SUCCESS)
	{
		pllAddFieldLens[0] = pllAddFieldLens[1] = pllAddFieldLens[2] = 10;
		pllAddFieldLens[3] = pllAddFieldLens[4] = pllAddFieldLens[5] = 10;
		pllAddFieldLens[6] = 0;
		/* EVAL_FIELDS "RETY,OURI,OURO,UHSS,SRHI,SRHO" */
		ilRc = SetAATArrayInfo ( "RESULTS", EVAL_FIELDS, pllAddFieldLens,
						         &rgEvalResults );
		if (ilRc!=RC_SUCCESS)
			dbg(TRACE,"Init_Stahdl: SetAATArrayInfo for RESULTS failed <%d>",ilRc);		
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CreateIdx ( &rgEvalResults, "IDXRKEY", "RKEY", "A", 1 );
		if (ilRc!=RC_SUCCESS)
			dbg(TRACE,"Init_Stahdl: CreateIdx for RESULTS failed <%d>",ilRc);		
	}
	/*  Initialise Array for existing Online Status OSTTAB */
	if(ilRc == RC_SUCCESS)
	{
		strncpy ( clStartDay, cgLoadStart, 8 );
		strncpy ( clEndDay, cgLoadEnd, 8 );
		clStartDay[8] = clEndDay[8] = '\0';

		/* OST_ADDFIELDS: RETY,TIFR,LEVL,UTPL,AFTF,FLT2,TSKN */
		if ( GetFieldLength("SDETAB","RETY",&(pllAddFieldLens[0]))!=RC_SUCCESS )
			pllAddFieldLens[0] = 10;
		pllAddFieldLens[1] = 7;		/* TIFR */
		pllAddFieldLens[2] = 2;		/* LEVL */
		if ( GetFieldLength("SDETAB","UTPL",&(pllAddFieldLens[3]))!=RC_SUCCESS )
			pllAddFieldLens[3] = 10;
		if ( GetFieldLength("SDETAB","AFTF",&(pllAddFieldLens[4]))!=RC_SUCCESS )
			pllAddFieldLens[4] = 4;
		pllAddFieldLens[5] = 10;
		if ( GetFieldLength("SDETAB","TSKN",&(pllAddFieldLens[6]))!=RC_SUCCESS )/* AM 20071127 */ 
		{
			dbg(TRACE,"Set default to 50");
			pllAddFieldLens[6] = 50;
		}
		if ( GetFieldLength("SDETAB","FLDR",&(pllAddFieldLens[7]))!=RC_SUCCESS )/* AM 20071205 */  
		{
			dbg(TRACE,"Set default to 4");
			pllAddFieldLens[7] = 4;
		}
		if ( GetFieldLength("SDETAB","ALRT",&(pllAddFieldLens[8]))!=RC_SUCCESS )/* AM 20071221 */  
		{
			dbg(TRACE,"Set default to 1");
			pllAddFieldLens[8] = 1;
		}
		pllAddFieldLens[9] = 0;
		
		
		sprintf(clSelection,"WHERE SDAY BETWEEN '%s' AND '%s'", clStartDay,clEndDay );
		dbg ( DEBUG, "Init_Stahdl: Selection for CEDAArrayFill OSTTAB: <%s>", clSelection );
		ilRc = SetArrayInfo( "OST", "OST", OST_FIELDS, OST_ADDFIELDS, pllAddFieldLens, 
							 clSelection, &rgOstArray, CEDAARRAY_ALL );
		if ( ilRc != RC_SUCCESS )
		{
			dbg ( TRACE, "Init_Stahdl: SetArrayInfo <OST> failed Rc <%d>", ilRc );
		}
		sprintf(clSelection, "(SDAY >= %s)  && (SDAY <= %s) ", clStartDay,clEndDay );
		dbg(TRACE,"Init_Stahdl: Filter for OSTTAB <%s>", clSelection );
		ilRc = CEDAArraySetFilter(&rgOstArray.rrArrayHandle,
								  rgOstArray.crArrayName,clSelection );
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"Init_Stahdl: CEDAArraySetFilter OSTTAB failed <%d>",ilRc);
		}
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CreateIdx ( &rgOstArray, "IDXOST1", "UAFT,UHSS,LEVL", "A,A,A", 1 );
		if (ilRc!=RC_SUCCESS)
			dbg(TRACE,"Init_Stahdl: CreateIdx for OST failed <%d>",ilRc);		
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CreateIdx ( &rgOstArray, "IDXOST2", "UAFT,UHSS,USDE", "A,A,A", 2 );
		if (ilRc!=RC_SUCCESS)
			dbg(TRACE,"Init_Stahdl: CreateIdx for OST failed <%d>",ilRc);		
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CreateIdx ( &rgOstArray, "IDXOST3", "RURN,UHSS", "A,A", 3 );
		if (ilRc!=RC_SUCCESS)
			dbg(TRACE,"Init_Stahdl: CreateIdx for OST failed <%d>",ilRc);		
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CreateIdx ( &rgOstArray, "IDXOST4", "FLT2", "A", 4 );
		if (ilRc!=RC_SUCCESS)
			dbg(TRACE,"Init_Stahdl: CreateIdx for OST failed <%d>",ilRc);		
	}

	if( ( ilRc == RC_SUCCESS) && 
		(ReadConfigEntry(clSection,"SEND_TO_ACTION",clValue) == RC_SUCCESS) )
	{
		if ( strstr ( clValue, "YES" ) )
			CEDAArraySendChanges2ACTION(&rgOstArray.rrArrayHandle,
   										rgOstArray.crArrayName, TRUE);
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = SetArrayInfo( "NEWOST", "OST", OST_FIELDS, OST_ADDFIELDS, 
							 pllAddFieldLens, "", &rgNewOstArr, 0 );
		if ( ilRc != RC_SUCCESS )
		{
			dbg ( TRACE, "Init_Stahdl: SetArrayInfo <NEWOST> failed Rc <%d>", ilRc );
		}
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CreateIdx ( &rgNewOstArr, "NEWOST1", "UAFT,USDE", "A,A", 1 );
		if (ilRc!=RC_SUCCESS)
			dbg(TRACE,"Init_Stahdl: CreateIdx for NEWOST failed <%d>",ilRc);		
	}
	PrepareOSTArray (0, 0 );
	if ( ilRc == RC_SUCCESS ) 
	{
		sprintf(clKeyword,"EVAL_AT_STARTUP");
       
         	if( (ReadConfigEntry(clSection,clKeyword,clValue) == RC_SUCCESS) &&
			 !strcmp(clValue,"YES") )
		{
			ilRc = EvaluateRules ( &rgAftArray, 0 );
			if ( ilRc != RC_SUCCESS )
				dbg ( TRACE, "Init_Stahdl: EvaluateRules failed RC <%d>", ilRc );
			else
			{
				ilRc = CreateSkeleton ( &rgAftArray );
				if ( ilRc != RC_SUCCESS )
					dbg ( TRACE, "Init_Stahdl: CreateSkeleton failed RC <%d>", ilRc );
			}
			if ( ilRc == RC_SUCCESS )
			{
				ilRc = UpdateOSTTAB ( &rgAftArray );
				if ( ilRc != RC_SUCCESS )
					dbg ( TRACE, "Init_Stahdl: UpdateOSTTAB failed RC <%d>", ilRc );
				PrepareSBC ( 0, &rgAftArray );
			}
                }
	}
	SaveOstTab (); 
	if (ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"Init_Stahdl failed");
	}

	return(ilRc);
	
} /* end of initialize */




/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
	int	ilRc = RC_SUCCESS;				/* Return code */
	
	dbg(TRACE,"Reset: now resetting");
	
	return ilRc;
	
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
	/* unset SIGCHLD ! DB-Child will terminate ! */

	dbg(TRACE,"Terminate: now DB logoff ...");

	signal(SIGCHLD,SIG_IGN);

	logoff();

	dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


	dbg(TRACE,"Terminate: now leaving ...");

	fclose(outp);

	sleep(ipSleep < 1 ? 1 : ipSleep);
	
	exit(0);
	
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/

static void HandleSignal(int pipSig)
{
	dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);

	switch(pipSig)
	{
	default	:
		Terminate(1);
		break;
	} /* end of switch */

	exit(1);
	
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
	dbg(TRACE,"HandleErr: <%d> : unknown ERROR",pipErr);

	return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
	switch(pipErr) {
	case	QUE_E_FUNC	:	/* Unknown function */
		dbg(TRACE,"<%d> : unknown function",pipErr);
		break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
		dbg(TRACE,"<%d> : malloc failed",pipErr);
		break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
		dbg(TRACE,"<%d> : msgsnd failed",pipErr);
		break;
	case	QUE_E_GET	:	/* Error using msgrcv */
		dbg(TRACE,"<%d> : msgrcv failed",pipErr);
		break;
	case	QUE_E_EXISTS	:
		dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
		break;
	case	QUE_E_NOFIND	:
		dbg(TRACE,"<%d> : route not found ",pipErr);
		break;
	case	QUE_E_ACKUNEX	:
		dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
		break;
	case	QUE_E_STATUS	:
		dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
		break;
	case	QUE_E_INACTIVE	:
		dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
		break;
	case	QUE_E_MISACK	:
		dbg(TRACE,"<%d> : missing ack ",pipErr);
		break;
	case	QUE_E_NOQUEUES	:
		dbg(TRACE,"<%d> : queue does not exist",pipErr);
		break;
	case	QUE_E_RESP	:	/* No response on CREATE */
		dbg(TRACE,"<%d> : no response on create",pipErr);
		break;
	case	QUE_E_FULL	:
		dbg(TRACE,"<%d> : too many route destinations",pipErr);
		break;
	case	QUE_E_NOMSG	:	/* No message on queue */
		dbg(TRACE,"<%d> : no messages on queue",pipErr);
		break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
		dbg(TRACE,"<%d> : invalid originator=0",pipErr);
		break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
		dbg(TRACE,"<%d> : queues are not initialized",pipErr);
		break;
	case	QUE_E_ITOBIG	:
		dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
		break;
	case	QUE_E_BUFSIZ	:
		dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
		break;
	default			:	/* Unknown queue error */
		dbg(TRACE,"<%d> : unknown error",pipErr);
		break;
	} /* end switch */
         
	return;

} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	int	ilBreakOut = FALSE;
	
	do
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;	

		if( ilRc == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRc != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRc);
			} /* fi */
		
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				ilBreakOut = TRUE;
				break;	

			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(1);
				break;	
	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				ilBreakOut = TRUE;
				break;	

			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;

			case	SHUTDOWN	:
				Terminate(1);
				break;
						
			case	RESET		:
				ilRc = Reset();
				break;
						
			case	EVENT_DATA	:
				dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
					
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;

			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;

			default			:
				dbg(TRACE,"HandleQueues: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
	} while (ilBreakOut == FALSE);

			ilRc = Init_Stahdl();
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitDemhdl: init failed!");
			} 
     

} /* end of HandleQueues */
	



/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{
	int	   ilRc           = RC_SUCCESS;			/* Return code */
	int      ilRc1          = 0;

	BC_HEAD *prlBchead       = NULL;
	CMDBLK  *prlCmdblk       = NULL;
	char    *pclSelection    = NULL;
	char    *pclFields       = NULL;
	char    *pclData         = NULL;
	char    *pclRow          = NULL;
	char 	clUrnoList[2400];
	char 	clTable[34];
	char 	clUaft[21]="", clUtpl[21]="", clTskn[64]="";
        char pclTmpSelKey[512];
	char clDestName[21] = "\0";
	char clRecvName[21] = "\0";
	char pclSqlKey[128];
	char *pclDel=0;
	BOOL bpDoEval = FALSE;
	BOOL bpSetCons = FALSE;
	BOOL bpSetCona = FALSE;

        /* MSI: 20080624: Added Declarations for New Command */  
        char clValue[50];    
        char clEvalStart[20];    
        char clEvalEnd[20];    

        int ilOffset = 0;    
        int ilEvalOffsetStart = 0;   
        int ilEvalOffsetEnd = 0;   

        time_t tlCurrTime;    
        time_t tlEvalStart;   
        time_t tlEvalEnd;            
        /* MSI: 20080624: Declarations END!!!   */ 

	prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
	prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
	pclSelection = prlCmdblk->data;
	pclFields    = pclSelection + strlen(pclSelection) + 1;
	pclData      = pclFields + strlen(pclFields) + 1;

	strcpy(clTable,prlCmdblk->obj_name);
        strcpy(clDestName,prlBchead->dest_name);
	strcpy(clRecvName,prlBchead->recv_name);

	dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);

	/****************************************/
	DebugPrintBchead(TRACE,prlBchead);
	DebugPrintCmdblk(TRACE,prlCmdblk);
	dbg(TRACE,"Command: <%s>",prlCmdblk->command);
	dbg(TRACE,"originator follows event = %p ",prpEvent);

	dbg(TRACE,"originator<%d>",prpEvent->originator);
	dbg(TRACE,"selection follows Selection = %p ",pclSelection);
	dbg(TRACE,"selection <%s>",pclSelection);
	dbg(TRACE,"fields    <%s>",pclFields);
	dbg(TRACE,"data      <%s>",pclData);
	/****************************************/

	if ( pclDel = strchr ( pclData, '\n' ) )
		*pclDel = '\0';

	cgBcField[0] = cgBcData[0] = '\0';
	cgMinAftTime[0] = cgMaxAftTime[0] = '\0';
	cgChangedRkeys[0] = '\0';

	if ( !strcmp(prlCmdblk->command,"IRT") || !strcmp(prlCmdblk->command,"URT") )
	{
		dbg(TRACE,"CALLING CEDAArrayEventUpdate2");
		ilRc = CEDAArrayEventUpdate2(prgEvent,clUrnoList,clTable);
		dbg( TRACE, "RESULT: TABLE <%s> UrnoList <%s>", clTable, clUrnoList);/* AM 20070911 */  
		ilRc1 = RC_DONT_CARE;
		if ( !strncmp ( clTable, "CCA", 3 ) )
		{	
			ilRc1 = GetFieldByUrno ( &rgCcaArray, igCcaFlnuIdx, clUrnoList, clUaft );
			if ( ilRc1 == RC_SUCCESS )
				ilRc1 = HandleCCAUpdate ( clUaft );
		}
		else if ( !strncmp ( clTable, "JOB", 3 ) )
		{	
			ilRc1 = GetFieldByUrno ( &rgJobArray, igJobUaftIdx, clUrnoList, clUaft );
			ilRc1 |= GetFieldByUrno ( &rgJobArray, igJobUtplIdx, clUrnoList, clUtpl );
			if ( ilRc1 == RC_SUCCESS )
			{
				TrimRight ( clUtpl );
				ilRc1 = HandleJobUpdate ( clUaft, clUtpl );
			}
		}
		else if (!strncmp( clTable, "RSM", 3) )
		{/* AM 20071127 */  
/* BERNI RSM */
			sprintf(pclSqlKey, "WHERE RURN=(SELECT RURN FROM RSMTAB WHERE URNO=%s)",  clUrnoList );
			ilRc = CEDAArrayRefill(&rgRsmArray.rrArrayHandle, 
					rgRsmArray.crArrayName, pclSqlKey, "", ARR_FIRST );
			ilRc1 = GetFieldByUrno ( &rgRsmArray, igRsmRurnIdx, clUrnoList, clUaft );/* Flight Urno */ 
			ilRc1 |= GetFieldByUrno ( &rgRsmArray, igRsmTsknIdx, clUrnoList, clTskn );/* Task Name */  
			if ( ilRc1 == RC_SUCCESS )
			{
				ilRc1 = HandleRsmUpdate ( clUaft, clTskn, clUrnoList );
			}

		}
		else if ( !strncmp ( clTable, "SRH", 3 ) )
		{	
			ilRc = IniPriority ( clUrnoList );
		}
		else if ( !strncmp ( clTable, "SDE", 3 ) )
		{	
			char *pclRow;
			long llRow=ARR_FIRST ;
			if ( CEDAArrayFindRowPointer(&(rgSdeArray.rrArrayHandle),
										  rgSdeArray.crArrayName,
										  &(rgSdeArray.rrIdxHandle[0]),
										  rgSdeArray.crIdxName[0],clUrnoList,
										  &llRow, (void*)&pclRow ) == RC_SUCCESS )
				ilRc = PrepareSdeArray ( SDEFIELD(pclRow,igSdeUsrhIdx)  );
		}
		else if ( !strncmp ( clTable, "OST", 3 ) )
		{	
			ilRc1 = GetFieldByUrno ( &rgOstArray, igOstUaftIdx, clUrnoList, clUaft );
			dbg( TRACE, "urno <%s>, uaft <%s>", clUrnoList, clUaft );/* AM 20070911 */  
		}
		else if ( !strncmp ( clTable, "AF1", 3 ) )
		{	
			ilRc1 = GetFieldByUrno ( &rgAf1Array, igAf1FlnuIdx, clUrnoList, clUaft );
			if ( ilRc1 == RC_SUCCESS )
			{
				ilRc1 = HandleAf1Update ( clUaft );
				if ( ilRc1 != RC_SUCCESS )
					dbg ( TRACE, "HandleData: HandleAf1Update FLNU <%s> failed", clUaft );
			}
		}
		else
			if ( ilRc1 != RC_DONT_CARE )
			{
				dbg ( TRACE, "HandleData: Processing of Update failed UAFT <%s> RC <%d>", 
					  clUaft, ilRc1 );
				ilRc = RC_FAIL;
			}

		if ( clUaft[0] )
		{	/*  Update on CCA, JOB or OST which has been handled */
			if ( ilRc1 == RC_SUCCESS )
			{
				/* dbg( TRACE, "SetCons <%s> UAFT", clUaft ); */ /* AM 20070911 */  
				ilRc1 = SetCONS ( &rgAftArray, clUaft, 0, FALSE );
				if ( ilRc1 != RC_SUCCESS )
					dbg ( TRACE, "HandleData: SetCONS UAFT <%s> UHSS <null> failed", 
						  clUaft );
			}		
			/* dbg( TRACE, "ProcessSCC UAFT <%s>.", clUaft ); */ /* AM 20070911 */   
			ilRc = ProcessSCC( clUaft, 0 );
			if ( ilRc != RC_SUCCESS )
				dbg ( TRACE, "HandleData: ProcessSCC UAFT <%s> UHSS <null> failed", 
					 clUaft );
			/* dbg( TRACE, "PrepareSBC <%s> UAFT", clUaft ); */ /* AM 20070911 */ 
			PrepareSBC ( clUaft, 0 );
			/* dbg( TRACE, "SaveOstTab <%s> UAFT", clUaft ); */ /* AM 20070911 */ 
			SaveOstTab ();
		}
	}
	else if ( !strcmp(prlCmdblk->command,"DRT") )
	{
		ilRc1 = RC_DONT_CARE;
		if ( !strncmp ( clTable, "CCA", 3 ) )
			ilRc1 = GetFieldByUrno ( &rgCcaArray, igCcaFlnuIdx, clUrnoList, clUaft );
		else if ( !strncmp ( clTable, "JOB", 3 ) )
		{	
			ilRc1 = GetFieldByUrno ( &rgJobArray, igJobUaftIdx, clUrnoList, clUaft );
			ilRc1 |= GetFieldByUrno ( &rgJobArray, igJobUtplIdx, clUrnoList, clUtpl );
		}
		else if (!strncmp( clTable, "RSM", 3) )
		{/* AM 20071127 */  
			if (bgUseRsm==TRUE)
			{
				ilRc1 = GetFieldByUrno ( &rgRsmArray, igRsmRurnIdx, clUrnoList, clUaft );/* Flight Urno */ 
				ilRc1 |= GetFieldByUrno ( &rgRsmArray, igRsmTsknIdx, clUrnoList, clTskn );/* Task Name */ 
			}
		}
		else if ( !strncmp ( clTable, "AF1", 3 ) )
		{	
			ilRc1 = GetFieldByUrno ( &rgAf1Array, igAf1FlnuIdx, clUrnoList, clUaft );
		}
		ilRc = CEDAArrayEventUpdate2(prgEvent,clUrnoList,clTable);
		if ( (ilRc==RC_SUCCESS) && (ilRc1==RC_SUCCESS) )
		{
			if ( !strncmp ( clTable, "CCA", 3 ) )
				ilRc1 = HandleCCAUpdate ( clUaft );
			else if ( !strncmp ( clTable, "JOB", 3 ) )
				ilRc1 = HandleJobUpdate ( clUaft, clUtpl );
			else if (!strncmp( clTable, "RSM", 3) )
			{/* AM 20071127 */ 
				if (bgUseRsm==TRUE)
				{
					TrimRight ( clTskn );
					ilRc1 = HandleRsmUpdate ( clUaft, clTskn, clUrnoList );
				}
			}
			else if ( !strncmp ( clTable, "AF1", 3 ) )
				ilRc1 = HandleAf1Update ( clUaft );

			if ( ilRc1 != RC_SUCCESS )
			{
				dbg ( TRACE, "HandleData: Processing of Update failed UAFT <%s> RC <%d>", 
					  clUaft, ilRc1 );
			}
			else
			{
				ilRc = ProcessSCC( clUaft, 0 );
				if ( ilRc != RC_SUCCESS )
					dbg ( TRACE, "HandleData: ProcessSCC UAFT <%s> UHSS <null> failed", 
						 clUaft );
			}
			PrepareSBC ( clUaft, 0 );
			SaveOstTab ();
		}
		if ( (ilRc1 != RC_SUCCESS) && (ilRc1 != RC_DONT_CARE) )
			ilRc = RC_FAIL;
	}
	else if ( !strcmp(prlCmdblk->command,"UFR") )
	{
		strcpy (prlCmdblk->command, "URT" );
		
		IsAftChangeImportant ( pclFields, pclData, &bpDoEval, &bpSetCons, &bpSetCona );
		dbg(TRACE,"===> CALLING CEDAArrayEventUpdate2");

		ilRc = CEDAArrayEventUpdate2(prgEvent,clUrnoList,clTable);
		if ( ilRc == RC_SUCCESS )
		{
			dbg(TRACE,"===> BACK FROM CEDAArrayEventUpdate2 (SUCCESS)");
			if ( !IS_EMPTY(cgChangedRkeys) )
			{
				dbg ( TRACE, "HandleData: Recognized changed RKEY, selection for recalculation <%s>",
					  cgChangedRkeys );
				ilRc = CEDAArrayRefill(&rgReqAftArray.rrArrayHandle, 
									   rgReqAftArray.crArrayName, cgChangedRkeys, "", ARR_FIRST );
				if ( ilRc != RC_SUCCESS )
					dbg ( TRACE, "HandleData: CEDAArrayRefill <AFT> failed, RC <%d>", ilRc );
				ilRc = EvaluateRules ( &rgReqAftArray, 0 );
				if ( ilRc == RC_SUCCESS )
				{
					ilRc = CreateSkeleton ( &rgReqAftArray );
					if ( ilRc != RC_SUCCESS )
						dbg ( TRACE, "HandleData: CreateSkeleton failed RC <%d>", ilRc );
					ilRc = UpdateOSTTAB ( &rgReqAftArray );
					if ( ilRc != RC_SUCCESS )
						dbg ( TRACE, "HandleData: UpdateOSTTAB failed RC <%d>", ilRc );
					PrepareSBC ( 0, &rgReqAftArray );
					SaveOstTab ();
				}
			}
			else
			{
				if ( bpDoEval )
				{
				        dbg(TRACE,"===> EVALUATE FLIGHT (bpDoEval WAS SET)"); /* MSI: 20080626: Changed from DEBUG to TRACE */    
					ilRc = EvaluateFlight ( clUrnoList );
					if ( ilRc == RC_SUCCESS )
					{
						ilRc = CreateSkeleton ( &rgReqAftArray );
					}
					if ( ilRc != RC_SUCCESS )
					{
						dbg ( TRACE, "HandleData: CreateSkeleton failed RC <%d>", ilRc );
					}
					else
					{
						ilRc = UpdateOSTTAB (&rgReqAftArray );
						PrepareSBC ( 0, &rgReqAftArray );
						SaveOstTab ();
					}
				}
				else if ( bpSetCons || bpSetCona )
				{
					if ( bpSetCona )
					{
						dbg(DEBUG,"===> SET CONA FOR FLIGHT (bpSetCona WAS SET)"); 
						ilRc = SetConaForFlight( clUrnoList, 0 );
						if ( ilRc != RC_SUCCESS )
							dbg ( TRACE, "HandleData: SetConaForFlight UAFT <%s> UHSS <null> failed", 
								  clUrnoList );
					}
					if ( bpSetCons )
					{
						dbg(DEBUG,"===> SET CONS FOR FLIGHT (bpSetCons WAS SET)"); 
						ilRc = SetCONS ( &rgAftArray, clUrnoList, 0, FALSE );
						if ( ilRc != RC_SUCCESS )
							dbg ( TRACE, "HandleData: SetCONS UAFT <%s> UHSS <null> failed", 
								clUrnoList );
					}
					if ( ilRc == RC_SUCCESS )
					{
						/* dbg(TRACE,"===> NOW CALCULATE THE STATUS RULES"); */
						ilRc = ProcessSCC( clUrnoList, 0 );
						if ( ilRc != RC_SUCCESS )
							dbg ( TRACE, "HandleData: ProcessSCC UAFT <%s> UHSS <null> failed", 
								  clUrnoList );
					}
					PrepareSBC ( clUrnoList, 0 );
					SaveOstTab ();
				}
			}
		}
		else
		{
			dbg(TRACE,"===> BACK FROM CEDAArrayEventUpdate2 (NOT FOUND)");
			dbg ( TRACE, "HandleData: CEDAArrayEventUpdate2 RC <%d>", ilRc );
		}
	}
	else if ( !strcmp(prlCmdblk->command,"IFR") )
	{
		strcpy (prlCmdblk->command, "IRT" );
		ilRc = CEDAArrayEventUpdate2(prgEvent,clUrnoList,clTable);
		if ( ilRc == RC_SUCCESS )
		{
			ilRc = EvaluateFlight ( clUrnoList );
			if ( ilRc == RC_SUCCESS )
				ilRc = CreateSkeleton ( &rgReqAftArray );
			if ( ilRc != RC_SUCCESS )
				dbg ( TRACE, "HandleData: CreateSkeleton failed RC <%d>", ilRc );
			else
			{
				ilRc = UpdateOSTTAB ( &rgReqAftArray );
				PrepareSBC ( clUrnoList, 0 );
				SaveOstTab ();
			}
		}
		else
			dbg ( TRACE, "HandleData: CEDAArrayEventUpdate2 RC <%d>", ilRc );

	}
	else if ( !strcmp(prlCmdblk->command,"CFS") )
	{
		SendAnswer(prgEvent,RC_SUCCESS);
		ProcessCFS(pclSelection);	
	}
	else if ( !strcmp(prlCmdblk->command,"SCC") )
	{	/* command 'SCC' = status conflict check */
		ProcessSCC( 0, 0 );	
		SaveOstTab ();
	}
	else if ( !strcmp(prlCmdblk->command,"UPS") || !strcmp(prlCmdblk->command,"UPJ") )
	{
		strcpy (prlCmdblk->command, "UFR" );
		ilRc = CEDAArrayEventUpdate2(prgEvent,clUrnoList,clTable);
	}
	else if ( !strcmp(prlCmdblk->command,"RSU" ) )
	{
		ProcessRSMSchTimeUpdate();
	}  
        /* MSI: 20080619: Added the command for Everyday One-Time Evaluation */   
        else if ( !strcmp(prlCmdblk->command, "EVL") )
        {   
                    if(ReadConfigEntry("MAIN", "EVAL_OFFSET_START", clValue) == RC_SUCCESS)  
                    {     
                       ilOffset = atoi(clValue);    
                       ilEvalOffsetStart = (int) ilOffset*60*60;    
                    }     
                    else  
                    {   
                       dbg(TRACE, "EVAL_OFFSET_START: Not Specified in stahdl.cfg!!! Using Defaults (-3)!!!");     
                       ilEvalOffsetStart = (int) (-3*60*60);   
                    }   

                    if(ReadConfigEntry("MAIN", "EVAL_OFFSET_END", clValue) == RC_SUCCESS)    
                    {    
                       ilOffset = atoi(clValue);     
                       ilEvalOffsetEnd = (int) ilOffset*60*60;    
                    }     
                    else        
                    {         
                       dbg(TRACE, "EVAL_OFFSET_END: Not Specified in stahdl.cfg!!! Using Defaults (+24)!!!");   
                       ilEvalOffsetEnd = (int) (24*60*60);   
                    }     

                    GetServerTimeStamp("UTC", 1, 0, clValue);     
                    StrToTime(clValue, &tlCurrTime);   

                    tlEvalStart = tlCurrTime + ilEvalOffsetStart;    
                    tlEvalEnd = tlCurrTime + ilEvalOffsetEnd;   

                    TimeToStr(clEvalStart, tlEvalStart);    
                    TimeToStr(clEvalEnd, tlEvalEnd);   

                    /* Refill arrays with One-Time Evaluation offset from config */        
                    ilRc = RefillArraysForEvaluation(clEvalStart, clEvalEnd);   

                    if(ilRc == RC_SUCCESS)   
                    {   
                       ilRc = EvaluateRules(&rgAftArray, 0);   
                       if(ilRc == RC_SUCCESS)     
                       {    
                        ilRc = CreateSkeleton(&rgAftArray);   
                        if(ilRc == RC_SUCCESS)   
                        {     
                            ilRc = UpdateOSTTAB(&rgAftArray);    
                            if(ilRc == RC_SUCCESS)   
                            {      
                                PrepareSBC(0, &rgAftArray);   
                                SaveOstTab();   
                            }    
                            else   
                            {   
                                dbg(TRACE, "<UpdateOSTTAB>: One-Time Evaluation Failed!!!");    
                            }     
                        }     
                        else   
                        {             
                            dbg(TRACE, "<CreateSkeleton>: One-Time Evaluation Failed!!!");   
                        }     
                      }       
                      else             
                      {      
                        dbg(TRACE, "<EvaluateRules>: One-Time Evaluation Failed!!!");    
                      }    
                    }    
                    else  
                    {   
                       dbg(TRACE, "Couldn't Refill Arrays for One-Time Evaluation.");  
                    }  

                    /* Refill AGAIN with actual(working) offset times for all arrays */    
                    ilRc = RefillArraysForEvaluation(cgLoadStart, cgLoadEnd);   
                    if(ilRc != RC_SUCCESS)   
                    {   
                       dbg(TRACE, "FAILED to REFILL Arrays with WORKING offset TIMES.");  
                    }     
        } /* MSI: 20080619: Command END!!! */   

	dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);

	return(ilRc);
} /* end of HandleData */

/* MSI: 20080624: Added Function Definition for Array Refill */     
/*****************************************************************************/
/* The Following function accepts the START and END time OFFSETs, and outputs*/
/* the REFILLED "AFTARRAY, RSMARRAY, and OSTARRAY" according to the given    */ 
/* offsets in the form of START/END respectively.                            */   
/*****************************************************************************/     
static int RefillArraysForEvaluation(char *pcpOffsetStart, char *pcpOffsetEnd)   
{   
  int ilRc = RC_SUCCESS;   
  char clSelection[2048];  
  char clStartDay[9];   
  char clEndDay[9];   

  sprintf(clSelection, "WHERE (((TIFA BETWEEN '%s' AND '%s') AND DES3 = '%s') OR ((TIFD BETWEEN '%s' AND '%s') AND ORG3 = '%s'))", pcpOffsetStart, pcpOffsetEnd, cgHopo, pcpOffsetStart, pcpOffsetEnd, cgHopo);  

  ilRc = CEDAArrayRefill(&rgAftArray.rrArrayHandle, rgAftArray.crArrayName, clSelection, "", ARR_FIRST);   
  if(ilRc == RC_SUCCESS)   
  {  
      memset(clSelection, 0x00, sizeof(clSelection));   
      sprintf(clSelection, "WHERE STDT >='%s' AND STDT <='%s'", pcpOffsetStart, pcpOffsetEnd);  
 
      ilRc = CEDAArrayRefill(&rgRsmArray.rrArrayHandle, rgRsmArray.crArrayName, clSelection, "", ARR_FIRST);  
      if(ilRc == RC_SUCCESS)   
      {   
         strncpy(clStartDay, pcpOffsetStart, 8);   
         strncpy(clEndDay, pcpOffsetEnd, 8);  
         clStartDay[8] = '\0';
         clEndDay[8] = '\0';    

         memset(clSelection, 0x00, sizeof(clSelection));   
         sprintf(clSelection, "WHERE SDAY BETWEEN '%s' AND '%s'", clStartDay, clEndDay); 

         ilRc = CEDAArrayRefill(&rgOstArray.rrArrayHandle, rgOstArray.crArrayName, clSelection, "", ARR_FIRST);  
         if(ilRc == RC_SUCCESS)   
         {   
           dbg(TRACE, "All Required Arrays Refilled successfully");   
         }   
         else  
         {    
           dbg(TRACE, "OST Array Refill FAILED!!!");   
         }    
      }    
      else  
      {    
         dbg(TRACE, "RSM Array Refill FAILED!!!");   
      }     
  }    
  else   
  {    
     dbg(TRACE, "AFT Array Refill FAILED!!!");   
  }     

  return ilRc;   
}  
/* MSI: 20080624: Definition END!!! */   

static int GetDebugLevel(char *pcpMode, int *pipMode)
{
	int		ilRC = RC_SUCCESS;
	char	clCfgValue[64];

	ilRC = iGetConfigRow(cgConfigFile, "SYSTEM", pcpMode, CFG_STRING,
							clCfgValue);

	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"SYSTEM %s not found in <%s>", pcpMode, cgConfigFile);
	}
	else
	{
		dbg(TRACE, "SYSTEM %s <%s>", pcpMode, clCfgValue);
		if (!strcmp(clCfgValue, "DEBUG"))
			*pipMode = DEBUG;
		else if (!strcmp(clCfgValue, "TRACE"))
			*pipMode = TRACE;
		else
			*pipMode = 0;
	}

	return ilRC;
}

/******************************************************************************/
/*  SetArrayInfo:                                                             */
/*  following flags activate/disactivate features to improve performance	  */
/*	CEDAARRAY_FILL:    fill array from DB; rgNewSDIArr doesn't need to be filled  */
/*	CEDAARRAY_URNOIDX: create an index for URNO; if not used, we have one	  */
/*					   Index less, which has to be sorted !					  */
/*	CEDAARRAY_TRIGGER: UCDTAB, that is used only by DISCAL doesn't need to be */
/*					   triggered											  */
/******************************************************************************/

static int SetArrayInfo(char *pcpArrayName, char *pcpTableName, 
						char *pcpArrayFieldList,char *pcpAddFields,
						long *pcpAddFieldLens,char *pcpSelection,
						ARRAYINFO *prpArrayInfo, short spFlags )
{
	int	i, ilRealFields=0, ilAddFields=0, ilRc = RC_SUCCESS;

	if(prpArrayInfo == NULL)
	{
		dbg(TRACE,"SetArrayInfo: invalid last parameter : null pointer not valid");
		ilRc = RC_FAIL;
	}
	else
	{
		memset(prpArrayInfo,0x00,sizeof(prpArrayInfo));

		prpArrayInfo->rrArrayHandle = -1;
		for ( i=0; i<ARR_MAX_IDX; i++ )
			prpArrayInfo->rrIdxHandle[i] = -1;

		if(pcpArrayName != NULL)
		{
			if(strlen(pcpArrayName) <= ARR_NAME_LEN)
			{
				strcpy(prpArrayInfo->crArrayName,pcpArrayName);
			}/* end of if */
		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		ilRealFields = get_no_of_items(pcpArrayFieldList);
		prpArrayInfo->lrArrayFieldCnt = ilRealFields ;
		if (pcpAddFields != NULL)
		{
			ilAddFields = get_no_of_items(pcpAddFields);
			prpArrayInfo->lrArrayFieldCnt += ilAddFields ;
		}

		prpArrayInfo->plrArrayFieldOfs = (long*) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
		if(prpArrayInfo->plrArrayFieldOfs == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrArrayFieldOfs calloc(%d,%d) failed",
				prpArrayInfo->lrArrayFieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		prpArrayInfo->plrArrayFieldLen = (long*) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
		if(prpArrayInfo->plrArrayFieldLen == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrArrayFieldLen calloc(%d,%d) failed",
				prpArrayInfo->lrArrayFieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}
		else
		{
			*(prpArrayInfo->plrArrayFieldLen) = -1;
		}/* end of if */
	}/* end of if */

	if(pcpTableName == NULL)
	{
		dbg(TRACE,"SetArrayInfo: invalid parameter TableName: null pointer not valid");
		ilRc = RC_FAIL;
	}
	else
	{
		strcpy ( prpArrayInfo->crTableName,pcpTableName );
		strcat ( prpArrayInfo->crTableName,cgTabEnd );
       	        dbg(TRACE,"SetArrayInfo: crTableName=<%s>",prpArrayInfo->crTableName );
        }
   	if(ilRc == RC_SUCCESS)
   	{
       	ilRc = GetRowLength(prpArrayInfo->crTableName, pcpArrayFieldList,
							&(prpArrayInfo->lrArrayRowLen));
       	if(ilRc != RC_SUCCESS)
       	{
           	dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpArrayName,pcpArrayFieldList);
      	}/* end of if */
   	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
		prpArrayInfo->lrArrayRowLen+=100;
		prpArrayInfo->pcrArrayRowBuf = (char *) calloc(1,(prpArrayInfo->lrArrayRowLen+10));
   		if(prpArrayInfo->pcrArrayRowBuf == NULL)
   		{
      			ilRc = RC_FAIL;
      			dbg(TRACE,"SetArrayInfo: calloc failed");
   		}/* end of if */
   	}/* end of if */


	if(ilRc == RC_SUCCESS)
	{
		dbg(TRACE,"CEDAArrayCreate follows Array <%s> Table <%s>",pcpArrayName,
													prpArrayInfo->crTableName);
		ilRc = CEDAArrayCreate( &(prpArrayInfo->rrArrayHandle),
								pcpArrayName,prpArrayInfo->crTableName,
								pcpSelection,pcpAddFields, pcpAddFieldLens, 
								pcpArrayFieldList,
								&(prpArrayInfo->plrArrayFieldLen[0]),
								&(prpArrayInfo->plrArrayFieldOfs[0]));

		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"SetArrayInfo: CEDAArrayCreate failed <%d>",ilRc);
		}/* end of if */
	}/* end of if */

	if(pcpArrayFieldList != NULL)
	{
		if(strlen(pcpArrayFieldList) <= ARR_FLDLST_LEN)
		{
			strcpy(prpArrayInfo->crArrayFieldList,pcpArrayFieldList);
		}/* end of if */
	}/* end of if */
	if(pcpAddFields != NULL)
	{
		if(strlen(pcpAddFields) + 
			strlen(prpArrayInfo->crArrayFieldList) <= 
					(size_t)ARR_FLDLST_LEN)
		{
			strcat(prpArrayInfo->crArrayFieldList,",");
			strcat(prpArrayInfo->crArrayFieldList,pcpAddFields);
			dbg(TRACE,"<%s>",prpArrayInfo->crArrayFieldList);
		}/* end of if */
		for ( i=0; i<ilAddFields; i++ )
		{
			prpArrayInfo->plrArrayFieldLen[i+ilRealFields] = pcpAddFieldLens[i];
		}
	}/* end of if */
	if ( (ilRc == RC_SUCCESS) && (spFlags&CEDAARRAY_FILL) )
	{
		ilRc = CEDAArrayFill(&(prpArrayInfo->rrArrayHandle),pcpArrayName,NULL);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"SetArrayInfo: CEDAArrayFill failed <%d>",ilRc);
		}
	}
	if ( (ilRc == RC_SUCCESS) && (spFlags&CEDAARRAY_URNOIDX) && 
		  strstr(pcpArrayFieldList,"URNO") )
	{
		strcpy ( prpArrayInfo->crIdxName[0], "URNOIDX" );
		strcpy ( prpArrayInfo->crIdxFields[0], "URNO" );
		prpArrayInfo->lrIdxLength[0] = 11;
		ilRc = CEDAArrayCreateSimpleIndexUp(&(prpArrayInfo->rrArrayHandle),
											  prpArrayInfo->crArrayName,
											  &(prpArrayInfo->rrIdxHandle[0]), 
											  prpArrayInfo->crIdxName[0], "URNO" );
		if(ilRc != RC_SUCCESS)                                             
		{
			dbg( TRACE,"SetArrayInfo: CEDAArrayCreateSimpleIndexDown for %s failed <%d>",
				 prpArrayInfo->crTableName, ilRc);		
		}
	}
	if ( (ilRc == RC_SUCCESS) && (spFlags&CEDAARRAY_TRIGGER) )
	{                                          /* send update msg. for active changes */ 
		if ( strcmp ( pcpTableName, "AFT" ) == 0 )
			ilRc = ConfigureAction (prpArrayInfo->crTableName,pcpArrayFieldList, "IFR,UFR,DFR,UPS,UPJ" );
		else
			ilRc = ConfigureAction (prpArrayInfo->crTableName,pcpArrayFieldList, NULL );

		if (ilRc != RC_SUCCESS)
			dbg (TRACE, "SetArrayInfo: ConfigureAction table<%s> failed <%d>", 
				  prpArrayInfo->crTableName, ilRc) ;
		else
			dbg (TRACE, "SetArrayInfo: ConfigureAction table<%s> succeeded.", 
				  prpArrayInfo->crTableName ) ; 
	}/* end of if */


	{
		int ilLc;
		char clFina[11];

		for(ilLc = 0; ilLc < prpArrayInfo->lrArrayFieldCnt; ilLc++)
		{
			get_real_item(clFina,prpArrayInfo->crArrayFieldList,ilLc+1);
			dbg(TRACE,"Field <%s> Idx <%02d> Offs <%d> Length <%d>",clFina, 
						ilLc,prpArrayInfo->plrArrayFieldOfs[ilLc],
						prpArrayInfo->plrArrayFieldLen[ilLc]);
		}
	}
	return(ilRc);
}



/******************************************************************************/
/******************************************************************************/
static int GetRowLength(char *pcpTana, char *pcpFieldList, long *plpLen)
{
	int	 ilRc = RC_SUCCESS;			/* Return code */
	int  ilNoOfItems = 0;
	int  ilLoop = 0;
	long llFldLen = 0;
	long llRowLen = 0;
	char clFina[8];
	
	ilNoOfItems = get_no_of_items(pcpFieldList);

	ilLoop = 1;
	do
	{
		ilRc = get_real_item(&clFina[0],pcpFieldList,ilLoop);
		if(ilRc > 0)
		{
			ilRc = GetFieldLength(pcpTana,clFina,&llFldLen);
			if(ilRc == RC_SUCCESS)
			{
				llRowLen++;
				llRowLen += llFldLen;
			}/* end of if */
		}/* end of if */
		ilLoop++;
	}while((ilRc == RC_SUCCESS) && (ilLoop <= ilNoOfItems));

	if(ilRc == RC_SUCCESS)
	{
		*plpLen = llRowLen;
	}/* end of if */

	return(ilRc);
	
} /* end of GetRowLength */

/****************************************************************************/
/* AM: 20081105:                                                            */
/*   Fill the RsmArray Information                                          */
/*   For the flights within giving time frame                               */
/* IN: pcpStartDateTime : Start Date Time of the Time Frame                 */
/*     pcpEndDateTime :   End Date Time of the Time Frame                   */
/*                                                                          */
/* OUT: NIL                                                                 */
/* UPD: rgRsmArray                                                          */
/****************************************************************************/
static int FillRsmArrayByTimeFrame( char *pcpStartDateTime, char * pcpEndDateTime )
{
	int ilRc = RC_SUCCESS;
	char clSelection[2048];
	
	sprintf(clSelection, "WHERE RURN IN (SELECT URNO FROM AFTTAB WHERE (((TIFA BETWEEN '%s' AND '%s') AND DES3 = '%s') OR ((TIFD BETWEEN '%s' AND '%s') AND ORG3 = '%s')))", pcpStartDateTime, pcpEndDateTime, cgHopo, pcpStartDateTime, pcpEndDateTime, cgHopo);  
    ilRc = CEDAArrayRefill(&rgRsmArray.rrArrayHandle, rgRsmArray.crArrayName, clSelection, "", ARR_FIRST);
	return ilRc;
}

/****************************************************************************/
/* AM: 20081105:                                                            */
/*   Fill the RsmArray Information for a flight                             */
/*   For the giving flight id                                               */
/* IN:  	pcpUaft : Flight Id                                             */
/*          bpCheckB4Load : Check whether records are existed before loading*/
/*                                                                          */
/* OUT:                                                                     */
/* UPD: rgRsmArray                                                          */
/****************************************************************************/
static int FillRsmArrayByFlightId( char *pcpUaft, BOOL bpCheckB4Load )
{
	int ilRc = RC_SUCCESS;
	char clSelection[2048];
	
	char clKey[64] = "";
	long llRow=ARR_FIRST ;
	char *pclRsm;
	BOOL blHasData = FALSE;
	
	if ( !pcpUaft || IS_EMPTY(pcpUaft))
	{
		dbg ( TRACE, "FillRsmArrayByFlightId: UAFT is NULL" );
		return RC_INVALID;
	}
	
	if (bpCheckB4Load)
	{
		sprintf ( clKey, "%s", pcpUaft );
		dbg ( TRACE, "FillRsmArrayByFlightId: Uaft<%s>:Checking", clKey);
		if ( CEDAArrayFindRowPointer(&(rgRsmArray.rrArrayHandle),
										rgRsmArray.crArrayName,
										&(rgRsmArray.rrIdxHandle[1]),
										rgRsmArray.crIdxName[1], clKey, &llRow,
										(void *)&pclRsm ) == RC_SUCCESS )
		{		
			blHasData = TRUE;
		}
	}
	
	if( blHasData == FALSE )
	{
	   dbg ( TRACE, "FillRsmArrayByFlightId: Refill Data");
	   sprintf(clSelection, "WHERE RURN=%s",  pcpUaft );
       if (CEDAArrayRefill(&rgRsmArray.rrArrayHandle, rgRsmArray.crArrayName, clSelection, "", ARR_FIRST)==RC_SUCCESS)
       {
	       dbg ( TRACE, "FillRsmArrayByFlightId: Refill Data. Fail.");
       }
    }
	return ilRc;
}


/****************************************************************************/
/* AM: 20081105:                                                            */
/*   Fill the RsmArray Information for a flight                             */
/*   with the giving rsmtab id                                              */
/* IN: pcpRsmId : Rsm Id                                                    */
/*                                                                          */
/* OUT:                                                                     */
/* UPD: rgRsmArray                                                          */
/****************************************************************************/
static int FillRsmArrayByRsmId( char *pcpRsmId )
{
	int ilRc = RC_SUCCESS;
	char clSelection[2048];
	
	sprintf(clSelection, "WHERE RURN=(SELECT RURN FROM RSMTAB WHERE URNO=%s)",  pcpRsmId );
    ilRc = CEDAArrayRefill(&rgRsmArray.rrArrayHandle, rgRsmArray.crArrayName, clSelection, "", ARR_FIRST);
	return ilRc;
}

/*  GetTotalRowLength: Get length of a row containing a specified fieldlist */
/*	IN:		pcpFieldList:	requested field list							*/
/*			prpArray:		array to be handled								*/
/*			pcpFields:		Field to be used for index						*/
/*	OUT:	plpLen:			requesed length including space for delimiters	*/
static int GetTotalRowLength( char *pcpFieldList, ARRAYINFO *prpArray, long *plpLen)
{
    int	 ilRc        = RC_SUCCESS;			/* Return code */
    int  ilNoOfItems = 0;
    int  ilItemNo = 0;
    int  ilLoop      = 0;
    long llRowLen    = 0;
    long llFldLen    = 0;
    char clFina[8];
	
    if (prpArray != NULL && pcpFieldList != NULL)
    {
		ilNoOfItems = get_no_of_items(pcpFieldList);
		dbg ( DEBUG, "GetTotalRowLength: Fields to calculate <%s>", pcpFieldList);
		dbg ( DEBUG, "GetTotalRowLength: Array-Fieldlist <%s>", prpArray->crArrayFieldList);
			
		ilLoop = 1;
		do
		{
			get_real_item(clFina,pcpFieldList,ilLoop);

			ilItemNo = get_item_no(prpArray->crArrayFieldList,clFina,strlen(clFina)+1 ); 
			/* if(GetItemNo(clFina,prpArray->crArrayFieldList,&ilItemNo) == RC_SUCCESS)  */
			if ( ilItemNo >= 0 )
			{
				llRowLen++;
				llRowLen += prpArray->plrArrayFieldLen[ilItemNo];
				dbg(DEBUG,"GetTotalRowLength: clFina <%s> Index <%d> Length <%ld>",
					clFina,ilItemNo, prpArray->plrArrayFieldLen[ilItemNo]);
			}
			else
				ilRc == RC_FAIL;
			ilLoop++;
		}while(ilLoop <= ilNoOfItems);
	}
	if(ilRc == RC_SUCCESS)
	{
		*plpLen = llRowLen;
	}/* end of if */

	dbg(DEBUG,"GetTotalRowLength:  FieldList <%s> Length <%ld>",pcpFieldList,*plpLen);
	return(ilRc);
	
} /* end of GetTotalRowLength */

/******************************************************************************/
/******************************************************************************/
static int GetFieldLength(char *pcpTana, char *pcpFina, long *plpLen)
{
    int  ilRc  = RC_SUCCESS;			/* Return code */
    int  ilCnt = 0;
    char clTaFi[32];
    char clFele[16];
    char clFldLst[16];

    ilCnt = 1;
    sprintf(clTaFi,"%s,%s",pcpTana,pcpFina);
    sprintf(clFldLst,"TANA,FINA"); /* wird von syslib zerstöonstante nicht mö*/

    ilRc = syslibSearchSystabData(cgTabEnd,&clFldLst[0],&clTaFi[0],"FELE",&clFele[0],&ilCnt,"");
    switch (ilRc)
    {
	case RC_SUCCESS :
	    *plpLen = atoi(&clFele[0]);
	    break;

	case RC_NOT_FOUND :

	    dbg(TRACE,"GetFieldLength: pcpTana <%s> pcpFina<%s>",pcpTana,pcpFina);
	    dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_NOT_FOUND <%s>",ilRc,&clTaFi[0]);
	    break;

	case RC_FAIL :
	    dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_FAIL",ilRc);
	    break;

	default :
	    dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> unexpected",ilRc);
	    break;
    }/* end of switch */

  return(ilRc);
} /* end of GetFieldLength */


/*  CreateIdx: create an index for an array									*/
/*	IN:	prpArrayInfo:	array to be handled									*/
/*		pcpIdxName:		Name of index										*/
/*		pcpFields:		Field to be used for index							*/
/*		pcpOrdering:	requesed ordering of fields, e. g. "A,D,A"			*/
/*		pcpOrdering:	number of index in the array of indices				*/
static int CreateIdx ( ARRAYINFO *prpArrayInfo, char *pcpIdxName,
					   char *pcpFields, char *pcpOrdering, int ipIdx )
{
	int		ilRC = RC_FAIL, ilLoop;
	char clOrderTxt[4];
	long	*pllOrdering=0, llFieldCnt;

	if ( !pcpFields )
		return ilRC;		/*  0-Pointer in parameter pcpFields */
	if ( ipIdx >= ARR_MAX_IDX )
		return ilRC;

	strcpy ( prpArrayInfo->crIdxName[ipIdx], pcpIdxName );
	strcpy ( prpArrayInfo->crIdxFields[ipIdx], pcpFields );
	
	ilRC = GetTotalRowLength( pcpFields, prpArrayInfo,
							  &(prpArrayInfo->lrIdxLength[ipIdx]) );

	if(ilRC != RC_SUCCESS)
        dbg(TRACE,"CreateIdx: GetTotalRowLength failed <%s> <%s>",
			prpArrayInfo->crTableName, pcpFields);

	if(ilRC == RC_SUCCESS)
	{
		llFieldCnt = get_no_of_items(pcpFields);
		pllOrdering = (long *) calloc(llFieldCnt,sizeof(long));
		if(!pllOrdering )
		{
			dbg(TRACE,"CreateIdx: pllOrdering calloc(%d,%d) failed", 
				llFieldCnt,sizeof(long));
			ilRC = RC_FAIL;
		}
		else
		{
			for(ilLoop=0; ilLoop < llFieldCnt; ilLoop++)
			{
				if ( !pcpOrdering ||
					 ( get_real_item(clOrderTxt,pcpOrdering,ilLoop+1)<=0 )
					)				 
					clOrderTxt[0] = 'D';
				if ( clOrderTxt[0] == 'A' )
					pllOrdering[ilLoop] = ARR_ASC;
				else
					pllOrdering[ilLoop] = ARR_DESC;
			}/* end of for */
		}/* end of else */
	}/* end of if */

	ilRC = CEDAArrayCreateMultiIndex( &(prpArrayInfo->rrArrayHandle), 
									  prpArrayInfo->crArrayName,
									  &(prpArrayInfo->rrIdxHandle[ipIdx]), 
									  prpArrayInfo->crIdxName[ipIdx],
									  prpArrayInfo->crIdxFields[ipIdx], 
									  pllOrdering );
	if ( pllOrdering )
		free (pllOrdering);
	pllOrdering = 0;
	return ilRC;									
}

/* BERNI */
static int CheckArrayInfo (ARRAYINFO *prpArrayInfo, int ipMode, int ipLogFile )
{
        int    ilRC       = RC_SUCCESS ;
        long   llRow      = 0 ;
        char *pclUaft = NULL;
        FILE  *prlFile    = NULL ;
        char   clDel      = ',' ;
        char   clFile[512] ;
        char  *pclTrimBuf = NULL ;
        long llRowCount;
        int ilCount = 0;
        char  *pclTestBuf = NULL ;
        char    *pclRowBuff=0;
        int     ilRowLen;
	int ilLen = 0;

        ilRowLen = prpArrayInfo->lrIdxLength[ipMode];
        dbg (TRACE,"CHECK ===> START CheckArrayInfo: ArrayName <%s>",prpArrayInfo->crArrayName) ;

        if (ilRC == RC_SUCCESS)
        {
                llRow = ARR_FIRST ;

                do
                {
                        ilRC = CEDAArrayGetRowPointer(&(prpArrayInfo->rrArrayHandle),
                        &(prpArrayInfo->crArrayName[0]),llRow,(void *)&pclTestBuf);
                        if (ilRC == RC_SUCCESS)
                        {
				pclUaft = OSTFIELD(pclTestBuf,igOstUaftIdx);
				ilLen = strlen(pclUaft);
				if (ilLen < 10)
				{
					if (strstr(pcgShortUaft,pclUaft)==NULL)
					{
						dbg(TRACE,"SHORT OST.UAFT <%s>",pclUaft);
						ilCount++;
						if (ilCount<200)
						{
							strcat(pcgShortUaft,pclUaft);
							strcat(pcgShortUaft,",");
						}
					}
				}
                                llRow = ARR_NEXT;
                        }
                } while (ilRC == RC_SUCCESS);

                if (ilRC == RC_NOTFOUND)
                        ilRC = RC_SUCCESS ;
		if (ilCount > 0)
		{
			dbg(TRACE,"WARNING: %d MORE SHORT UAFT IN ARRAY OSTTAB",ilCount);
			dbg(TRACE,"OLD COUNT=%d NEW COUNT=%d",igCountOstUaft,ilCount);
			igCountOstUaft += ilCount;
		}
        } /* end of if */

        dbg (TRACE,"CHECK ===> END   CheckArrayInfo: ArrayName <%s>",prpArrayInfo->crArrayName) ;
        ilRC = RC_SUCCESS ;
        return (ilRC) ;
}/* end of CheckArrayInfo */


/******************************************************************************/
static int SaveIndexInfo (ARRAYINFO *prpArrayInfo, int ipMode, int ipLogFile )
{
	int    ilRC       = RC_SUCCESS ;
	long   llRow      = 0 ;
	FILE  *prlFile    = NULL ;
	char   clDel      = ',' ;
	char   clFile[512] ;
	char  *pclTrimBuf = NULL ;
	long llRowCount;
	int ilCount = 0;
	char  *pclTestBuf = NULL ;
	char	*pclRowBuff=0;
	int     ilRowLen;

	ilRowLen = prpArrayInfo->lrIdxLength[ipMode];
	pclRowBuff = (char *) calloc(1, ilRowLen+1);
	dbg (TRACE,"SaveIndexInfo: ArrayName <%s>",prpArrayInfo->crArrayName) ;

	if ( !pclRowBuff )
	{
		dbg ( TRACE, "SaveIndexInfo: Calloc failed" );
		ilRC = RC_FAIL;
	}
		
	if (ilRC == RC_SUCCESS)
	{
		sprintf (&clFile[0],"%s/%s_%s.info",getenv("DBG_PATH"),mod_name,prpArrayInfo->crArrayName) ;

		errno = 0 ;
		prlFile = fopen (&clFile[0], "w") ;
		if (prlFile == NULL)
		{
			dbg (TRACE, "SaveIndexInfo: fopen <%s> failed <%d-%s>", clFile, 
				  errno, strerror(errno)) ;
			ilRC = RC_FAIL ;
		} /* end of if */
	} /* end of if */

	if (ilRC == RC_SUCCESS)
	{
		if ( ipLogFile )
		{
			dbg (DEBUG, "ArrayName      <%s>\n", prpArrayInfo->crArrayName) ;      
			dbg (DEBUG, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList) ;  
			dbg (DEBUG, "IdxFields <%s>\n", prpArrayInfo->crIdxFields[ipMode]) ;  
		}
		fprintf (prlFile, "ArrayName      <%s>\n", prpArrayInfo->crArrayName) ; /*fflush(prlFile) ;*/
		fprintf (prlFile, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList) ; /*fflush(prlFile) ;*/
		fprintf (prlFile, "IdxFields <%s>\n", prpArrayInfo->crIdxFields[ipMode]) ; 
		fflush(prlFile) ;

		if ( ipLogFile )
			dbg(DEBUG,"IndexData") ; 
		fprintf(prlFile,"IndexData\n"); fflush(prlFile);
		if ( prpArrayInfo->rrIdxHandle[ipMode] > -1) 
		{
			pclRowBuff[0] = '\0';
			llRow = ARR_FIRST ;
			do
			{
				ilRC = CEDAArrayFindKey ( &(prpArrayInfo->rrArrayHandle), 
										  prpArrayInfo->crArrayName, 
										  &(prpArrayInfo->rrIdxHandle[ipMode]), 
										  prpArrayInfo->crIdxName[ipMode], 
										  &llRow, ilRowLen, pclRowBuff );
				if (ilRC == RC_SUCCESS)
				{
					if ( ipLogFile )
						dbg(DEBUG, "SaveIndexInfo: Key <%ld> <%s>",llRow,pclRowBuff) ;
		
					if (strlen (pclRowBuff) == 0)
					{
						ilRC = CEDAArrayGetRow(&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), llRow, clDel, prpArrayInfo->lrArrayRowLen, prpArrayInfo->pcrArrayRowBuf);
						if( ipLogFile && (ilRC == RC_SUCCESS) )
							dbg (DEBUG, "SaveIndexInfo: Row <%ld> <%s>", llRow,prpArrayInfo->pcrArrayRowBuf) ;
					} /* end of if */

					pclTrimBuf = strdup (pclRowBuff) ;
					if (pclTrimBuf != NULL)
					{
						ilRC = GetDataItem (pclTrimBuf,pclRowBuff, 1, clDel, "", "  ") ;
						if (ilRC > 0)
						{
						   ilRC = RC_SUCCESS ;
						   fprintf (prlFile,"Key:: <%ld><%s>\n",llRow,pclTrimBuf) ; fflush(prlFile) ;
						} /* end of if */
						free (pclTrimBuf) ;
						pclTrimBuf = NULL ;
					} /* end of if */
					llRow = ARR_NEXT;
				}
				else
				{
					if ( ipLogFile )
						dbg (TRACE, "SaveIndexInfo: CEDAArrayFindKey failed <%d>", ilRC) ;
				}
			} while (ilRC == RC_SUCCESS) ;
		}
		if ( ipLogFile )
			dbg (DEBUG,"ArrayData") ;
	
		fprintf (prlFile,"ArrayData\n"); fflush(prlFile);
		llRow = ARR_FIRST ;

		CEDAArrayGetRowCount(&(prpArrayInfo->rrArrayHandle),prpArrayInfo->crArrayName,&llRowCount);

		llRow = ARR_FIRST;
		if ( ipLogFile )
			dbg(DEBUG,"Array  <%s> data follows Rows %ld", prpArrayInfo->crArrayName,llRowCount);
		do
		{
			ilRC = CEDAArrayGetRowPointer(&(prpArrayInfo->rrArrayHandle),
			&(prpArrayInfo->crArrayName[0]),llRow,(void *)&pclTestBuf);
			if (ilRC == RC_SUCCESS)
			{
				for( ilCount = 0; ilCount < prpArrayInfo->lrArrayFieldCnt; ilCount++)
				{
				
					fprintf (prlFile,"<%s>",&pclTestBuf[prpArrayInfo->plrArrayFieldOfs[ilCount]]) ; 
					fflush(prlFile) ;
					if ( ipLogFile )
						dbg (DEBUG, "SaveIndexInfo: ilCount <%ld> <%s>", ilCount,&pclTestBuf[prpArrayInfo->plrArrayFieldOfs[ilCount]]) ;
				}
				fprintf (prlFile,"\n") ; 
				if ( ipLogFile )
					dbg (DEBUG, "\n") ;
				fflush(prlFile) ;
				llRow = ARR_NEXT;
			}
			else
			{
				if ( ipLogFile )
					dbg(DEBUG,"GetRowPointer failed RC=%d",ilRC);
			}
		} while (ilRC == RC_SUCCESS);

		if (ilRC == RC_NOTFOUND)
			ilRC = RC_SUCCESS ;
	} /* end of if */

	if (prlFile != NULL)
	{
		fclose(prlFile) ;
		prlFile = NULL ;
		if ( ipLogFile )
			dbg (TRACE, "SaveIndexInfo: event saved - file <%s>", clFile) ;
	} /* end of if */
	if ( pclRowBuff )
		free ( pclRowBuff );
	return (ilRC) ;

}/* end of SaveIndexInfo */

/*correct timefunction HEB*/
static int StrToTime(char *pcpTime,time_t *plpTime)
{

    struct tm *prlTm;
    struct tm rlTm;
    int i = 0;
    time_t tlNow;
    int ilIsDst = 0;
    int ilYear,ilMon,ilDay,ilHour,ilMin,ilSec;
    char clTmpString[124];
    char clTime[124];


    tlNow = time(NULL);
    prlTm = (struct tm *)localtime_r((const time_t*)&tlNow,&rlTm);
    tlNow = mktime(&rlTm);
    for (i=1; i<=2; i++)
    {
	strcpy(clTime,pcpTime);

	strncpy(clTmpString,clTime,4);
	clTmpString[4] = '\0';
	ilYear = atoi(clTmpString)-1900;

	strncpy(clTmpString,&clTime[4],2);
	clTmpString[2] = '\0';
	ilMon = atoi(clTmpString)-1;

	strncpy(clTmpString,&clTime[6],2);
	clTmpString[2] = '\0';
	ilDay = atoi(clTmpString);

	strncpy(clTmpString,&clTime[6],2);
	clTmpString[2] = '\0';
	ilDay = atoi(clTmpString);

	strncpy(clTmpString,&clTime[8],2);
	clTmpString[2] = '\0';
	ilHour = atoi(clTmpString);

	strncpy(clTmpString,&clTime[10],2);
	clTmpString[2] = '\0';
	ilMin = atoi(clTmpString);

	strncpy(clTmpString,&clTime[12],2);
	clTmpString[2] = '\0';
	ilSec = atoi(clTmpString);

	rlTm.tm_year = ilYear;
	rlTm.tm_mon  = ilMon;
	rlTm.tm_mday = ilDay;
	rlTm.tm_hour = ilHour;
	rlTm.tm_min = ilMin;
	rlTm.tm_sec = ilSec;

	ilIsDst = rlTm.tm_isdst;
	tlNow = mktime(&rlTm);
	if ((i == 1) && (ilIsDst != rlTm.tm_isdst) && (ilHour == 2))
	{
	    rlTm.tm_isdst = 0;
	}
 
    } /* end of for*/

    if (tlNow != (time_t) -1)
    {
	dbg(DEBUG,"StrToTime: pcpTime <%s> plptime: <%d>", pcpTime, tlNow);
	*plpTime = tlNow;
	return RC_SUCCESS;
    }
    *plpTime = time(NULL);
    return RC_FAIL;
}

/*correct timefunction HEB*/
static int TimeToStr(char *pcpTime,time_t lpTime) 
{               
    struct tm *prlTm;
    struct tm rlTm;

    prlTm = (struct tm *)localtime_r(&lpTime,&rlTm);

    sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
	    rlTm.tm_year+1900,rlTm.tm_mon+1,rlTm.tm_mday,rlTm.tm_hour,
	    rlTm.tm_min,rlTm.tm_sec);
    return (rlTm.tm_wday == 0 ? 7 : rlTm.tm_wday) + '0';
}                      


/*  EvaluateRules: evaluate rules for all flights in an AFT-array			*/
/*	IN:	prpFlightArray:	array of flights to be evaluated					*/
/*		pcpUhss:		URNO of HSS to be evaluated, can be NULL for all	*/
static int EvaluateRules ( ARRAYINFO* prpFlightArray, char *pcpUhss )
{
  int ilGetRc = TRUE;
	char *pclFlight, *pclRule, clSrhUrno[21], *pcl2ndFlight;
	char *pclInbound, *pclOutbound, *pclRkey, *pclResult;
	long llAftRow, llSrhRow;
	char clKey[21], clActUhss[21], clRowBuff[71], clAdid[11], *pclSrhAdid; 
	int	 i, ilRc, il2ndFound;
	BOOL blGoon ;
	long llNext, llGroundTime;
	int	ilEvtType, ilRuleFound;
	char clOuri[21], clOuro[21], clSrhi[21], clSrho[21];
	time_t tlTifa, tlTifd;

	llAftRow = ARR_FIRST;

	CEDAArrayDelete ( &(rgEvalResults.rrArrayHandle), rgEvalResults.crArrayName );
	while ( CEDAArrayGetRowPointer(&(prpFlightArray->rrArrayHandle),
								   prpFlightArray->crArrayName,llAftRow,
								   (void *)&pclFlight) == RC_SUCCESS )
	{		
		llAftRow = ARR_NEXT;
		llNext = ARR_FIRST;
		pclRkey = AFTFIELD(pclFlight,igAftRkeyIdx);
		if ( CEDAArrayFindRowPointer ( &(rgEvalResults.rrArrayHandle), 
									   rgEvalResults.crArrayName,
									  &(rgEvalResults.rrIdxHandle[1]),
									  rgEvalResults.crIdxName[1],pclRkey,
									  &llNext, (void*)&pclResult ) == RC_SUCCESS )
		{
			dbg ( TRACE, "EvaluateRules: RKEY <%s> already evaluated UAFT <%s>",
				  pclRkey, AFTFIELD(pclFlight,igAftUrnoIdx) );
			continue;
		}
		strcpy ( clAdid, AFTFIELD(pclFlight,igAftAdidIdx) );
		pclInbound = pclOutbound = 0;
		llGroundTime = 0;
		il2ndFound = Find2ndFlight ( prpFlightArray, pclFlight, &pcl2ndFlight );
		if ( clAdid[0] == 'A') 
		{
			pclInbound = pclFlight;
			if ( il2ndFound == RC_SUCCESS )
			{
				pclOutbound = pcl2ndFlight;
				ilEvtType = EVT_TURN;
			}
			else
				ilEvtType = EVT_IN;
		}
		else
		{
			pclOutbound = pclFlight;
			if ( il2ndFound == RC_SUCCESS )
			{
				pclInbound = pcl2ndFlight;
				ilEvtType = EVT_TURN;
			}
			else
				ilEvtType = EVT_OUT;
		}
		if ( pclInbound )
		{
			strcpy ( clOuri, AFTFIELD(pclInbound,igAftUrnoIdx) );
			UpdateMinMax ( AFTFIELD(pclInbound,igAftTifaIdx) );
		}
		else
			strcpy ( clOuri, " " );
		if ( pclOutbound )
		{
			strcpy ( clOuro, AFTFIELD(pclOutbound,igAftUrnoIdx) );
			UpdateMinMax ( AFTFIELD(pclOutbound,igAftTifdIdx) );
		}
		else
			strcpy ( clOuro, " " );
			
		sprintf ( clKey, "%c", cgFlagActive ); 
		if ( pclInbound && pclOutbound )
		{	/* calculate actual ground time */
			StrToTime(AFTFIELD(pclInbound,igAftTifaIdx),&tlTifa);
			StrToTime(AFTFIELD(pclOutbound,igAftTifdIdx),&tlTifd);
			llGroundTime = tlTifd - tlTifa;
		}
		if ( pcpUhss )
		{
			strcat ( clKey, "," );
			strcat ( clKey, pcpUhss );
		}
		dbg ( TRACE, "====>> PROCESS FLIGHT <%s> UAFT1 <%s> UAFT2 <%s> Evt-Type <%d> SRH-Key <%s>",
			  AFTFIELD(pclFlight,igAftFlnoIdx), clOuri,  clOuro, ilEvtType, clKey );
		
		llSrhRow = ARR_FIRST;
		clActUhss[0]='\0';
		ilRuleFound = 0;
		strcpy ( clSrhi, " " );
		strcpy ( clSrho, " " );

		while ( CEDAArrayFindRowPointer(&(rgSrhArray.rrArrayHandle),
									    rgSrhArray.crArrayName,
										&(rgSrhArray.rrIdxHandle[1]),
										rgSrhArray.crIdxName[1],clKey,&llSrhRow,
										(void*)&pclRule ) == RC_SUCCESS )
		{
			strcpy ( clSrhUrno, SRHFIELD(pclRule,igSrhUrnoIdx) );
			pclSrhAdid = SRHFIELD(pclRule,igSrhAdidIdx) ;
			blGoon = TRUE;
			/* dbg( TRACE, "EvaluateRules: Act<%s>, New<%s>", clActUhss, SRHFIELD(pclRule,igSrhUhssIdx) ); */ /* AM 20071231 */  
			if ( strcmp ( clActUhss, SRHFIELD(pclRule,igSrhUhssIdx) ) == 0 )	
			{	/* same section */
				if ( ilRuleFound == ilEvtType ) 
				{	/* rule(s) already found */
					blGoon = FALSE;
					dbg(TRACE, "RULE SKIPPED <%s> UHSS <%s> SAME UHSS/EVT-TYPE", 
						  SRHFIELD(pclRule,igSrhNameIdx), SRHFIELD(pclRule,igSrhUhssIdx) );
				}
				else
					dbg (TRACE, "CURRENT RULE <%s> OF SAME UHSS <%s>", 
						  SRHFIELD(pclRule,igSrhNameIdx), SRHFIELD(pclRule,igSrhUhssIdx) );
			}	
			else
			{	/* change of section */
				if ( clActUhss[0] ) 
				{	/* EVAL_FIELDS "RETY,OURI,OURO,UHSS,SRHI,SRHO" */
					memset ( clRowBuff, ' ', sizeof(clRowBuff) );
					sprintf ( clRowBuff, "%s,%s,%s,%s,%s,%s", AFTFIELD(pclFlight,igAftRkeyIdx), 
													clOuri, clOuro, clActUhss, clSrhi,clSrho );
					delton(clRowBuff);
					llNext = ARR_NEXT;
					ilRc = AATArrayAddRow(&(rgEvalResults.rrArrayHandle),rgEvalResults.crArrayName,
										  &llNext,(void *)clRowBuff);
				}
				dbg ( TRACE, "CURRENT RULE <%s> OF NEW UHSS <%s>", 
					  SRHFIELD(pclRule,igSrhNameIdx), SRHFIELD(pclRule,igSrhUhssIdx) );
				ilRuleFound = 0;	/* reset variable  */
				strcpy ( clSrhi, " " );
				strcpy ( clSrho, " " );
				strcpy ( clActUhss, SRHFIELD(pclRule,igSrhUhssIdx) );
			}
			if ( blGoon )
			{
				if ( *pclSrhAdid == 'A' &&  (ilRuleFound & EVT_IN) )
					blGoon = FALSE;
				if ( *pclSrhAdid == 'D' &&  (ilRuleFound & EVT_OUT) )
					blGoon = FALSE;
				if ( *pclSrhAdid == 'T' &&  (ilRuleFound != 0) )
					blGoon = FALSE;
			}
			if ( blGoon )
			{	/*  We need to find a rule of this type */
				if ( CheckOneRule ( pclRule, pclInbound, pclOutbound, llGroundTime ) )
				{	/* all conditions are true */
					/* EVAL_FIELDS "RETY,OURI,OURO,UHSS,SRHI,SRHO" */
/* BERNI */
		dbg ( TRACE, "MATCHING RULE FOR FLIGHT <%s> UAFT1 <%s> UAFT2 <%s> Evt-Type <%d> SRH-Key <%s>",
			  AFTFIELD(pclFlight,igAftFlnoIdx), clOuri,  clOuro, ilEvtType, clKey );
		dbg ( TRACE, "CHECKING RULE NAME CODES <%s> UHSS <%s> ",
			  SRHFIELD(pclRule,igSrhNameIdx), SRHFIELD(pclRule,igSrhUhssIdx) );
					ilGetRc = CheckRuleNameFilter(SRHFIELD(pclRule,igSrhNameIdx), clOuri, clOuro);
					if (ilGetRc == RC_SUCCESS)
					{
						if ( (*pclSrhAdid == 'A') || (*pclSrhAdid == 'T') )
						{
							ilRuleFound |= EVT_IN;
							strcpy ( clSrhi, clSrhUrno );
						}
						if ( (*pclSrhAdid == 'D') || (*pclSrhAdid == 'T') )
						{
							ilRuleFound |= EVT_OUT;
							strcpy ( clSrho, clSrhUrno );
						}
						dbg ( TRACE, "RULE ACCEPTED: <%s> ALL CONDITIONS ARE TRUE", SRHFIELD(pclRule,igSrhNameIdx));
						dbg ( TRACE, "Evt-Type <%d> UHSS <%s> SRHI <%s> SRHO <%s> ilRuleFound <%d>",
						  ilEvtType, SRHFIELD(pclRule,igSrhUhssIdx), clSrhi, clSrho, ilRuleFound );
					}
					else
					{
						dbg(TRACE,"RULE IGNORED : <%s> FLIGHT NOT ON REMOTE BAY", SRHFIELD(pclRule,igSrhNameIdx));
					}
				}
			}
			llSrhRow = ARR_NEXT;
		}
		if ( clActUhss[0] ) 
		{		/* no rule in last section found */
			memset ( clRowBuff, ' ', sizeof(clRowBuff) );
			sprintf ( clRowBuff, "%s,%s,%s,%s,%s,%s", AFTFIELD(pclFlight,igAftRkeyIdx), 
								clOuri, clOuro, clActUhss, clSrhi,clSrho );
			delton(clRowBuff);
			llNext = ARR_NEXT;
			ilRc = AATArrayAddRow(&(rgEvalResults.rrArrayHandle),rgEvalResults.crArrayName,
								  &llNext,(void *)clRowBuff);
		}
	}
	return 0;
}

static int CheckRuleNameFilter(char *pcpRuleName, char *pcpUaftArr, char *pcpUaftDep)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  int ilChkBay = FALSE;
  char pclChkName[128];
  char pclChkChar[8];
  char pclTable[16];
  char pclFldName[64];
  char pclSqlKey[128];
  char pclDbsDat[1024];
  strcpy(pclChkName, pcpRuleName);
  str_chg_upc (pclChkName);
  strcpy(pclChkChar," -_");
  RemoveCharsFromString(pclChkName, pclChkChar );
  dbg(TRACE,"CHECK RULE PATTERN <%s>",pclChkName);
  ilChkBay = FALSE;
  if (strstr(pclChkName,"REMOTEBAY") != NULL)
  {
    ilChkBay = TRUE;
  }
  if (strstr(pclChkName,"PARKINGBAY") != NULL)
  {
    ilChkBay = TRUE;
  }
  if (ilChkBay == TRUE)
  {
    dbg(TRACE, "RULE <%s> WITH PARKING BAY CONDITION",pcpRuleName);
    ilRc = RC_FAIL;
    if ((pcpUaftArr[0] != '\0') && (pcpUaftArr[0] != ' '))
    {
      dbg(TRACE,"CHECK BAY OF ARR FLIGHT URNO <%s>",pcpUaftArr);
      strcpy(pclTable,"PSTTAB");
      strcpy(pclFldName,"PNAM,BRGS");
      sprintf(pclSqlKey,"WHERE PNAM=(SELECT PSTA FROM AFTTAB WHERE URNO=%s) AND BRGS=' '",pcpUaftArr);
      ilGetRc = HandleGocSelect(pclTable, pclSqlKey, pclFldName, pclDbsDat);
      if (ilGetRc == DB_SUCCESS)
      {
	dbg(TRACE,"ARR BAY RESULT <%s>",pclDbsDat);
        ilRc = RC_SUCCESS;
      }
    }
    if ((pcpUaftDep[0] != '\0') && (pcpUaftDep[0] != ' '))
    {
      dbg(TRACE,"CHECK BAY OF DEP FLIGHT URNO <%s>",pcpUaftDep);
      strcpy(pclTable,"PSTTAB");
      strcpy(pclFldName,"PNAM,BRGS");
      sprintf(pclSqlKey,"WHERE PNAM=(SELECT PSTD FROM AFTTAB WHERE URNO=%s) AND BRGS=' '",pcpUaftDep);
      ilGetRc = HandleGocSelect(pclTable, pclSqlKey, pclFldName, pclDbsDat);
      if (ilGetRc == DB_SUCCESS)
      {
	dbg(TRACE,"DEP BAY RESULT <%s>",pclDbsDat);
        ilRc = RC_SUCCESS;
      }
    }
    if (ilRc == RC_SUCCESS)
    {
      dbg(TRACE,"RULE IS RELATED TO A FLIGHT ON REMOTE BAY");
    }
  }
 
  return ilRc;
}

/*  EvaluateFlight: evaluate rules for one flight (list of flights later)   */
/*	IN:		pcpUrnoList:	URNO of flight, must not be NULL				*/
static int EvaluateFlight ( char *pcpUrnoList )
{
	int ilRC ;
	char *pclFlightRow = 0, *pcl2ndFlight=0;
	long llRow;

	dbg ( TRACE, "EvaluateFlight: URNO-List <%s>", pcpUrnoList );
	ilRC = CEDAArrayDelete(&(rgReqAftArray.rrArrayHandle), rgReqAftArray.crArrayName );
	if ( ilRC != RC_SUCCESS )
		dbg ( TRACE, "EvaluateFlight: CEDAArrayDelete failed RC <%d>", ilRC );
	else
	{	/* copy flight from rgAftArray to rgReqAftArray */
		llRow = ARR_FIRST;
		ilRC = CEDAArrayFindRowPointer(&(rgAftArray.rrArrayHandle),
									    rgAftArray.crArrayName,
										&(rgAftArray.rrIdxHandle[0]),
										rgAftArray.crIdxName[0], pcpUrnoList, 
										&llRow, (void *)&pclFlightRow ) ;
		if ( ilRC != RC_SUCCESS )
			dbg ( TRACE, "EvaluateFlight: Flight not found !!! RC <%d>", ilRC );
		else	
		{
			llRow = ARR_LAST;
			ilRC = CEDAArrayAddRow (&rgReqAftArray.rrArrayHandle, 
									 rgReqAftArray.crArrayName, &llRow, 
									 (void*)pclFlightRow );
			if ( ilRC != RC_SUCCESS )
				dbg ( TRACE, "EvaluateFlight: CEDAArrayAddRow failed RC <%d>", ilRC );
			if ( Find2ndFlight ( &rgAftArray, pclFlightRow, &pcl2ndFlight ) == RC_SUCCESS )
			{
				llRow = ARR_LAST;
				ilRC = CEDAArrayAddRow (&rgReqAftArray.rrArrayHandle, 
										 rgReqAftArray.crArrayName, &llRow, 
										 (void*)pcl2ndFlight );
				if ( ilRC != RC_SUCCESS )
					dbg ( TRACE, "EvaluateFlight: CEDAArrayAddRow 2n Flight failed RC <%d>", ilRC );
			}
		}
	}
	if ( ilRC == RC_SUCCESS )
	{
		if ( debug_level > TRACE )
		{
			CEDAArrayGetRowCount(&(rgReqAftArray.rrArrayHandle), 
								 rgReqAftArray.crArrayName, &llRow );
			dbg ( DEBUG, "EvaluateFlight: %d flights found for UAFT <%s>", llRow, pcpUrnoList );
		}
		ilRC = EvaluateRules ( &rgReqAftArray, 0 );
		dbg ( TRACE, "EvaluateFlight: EvaluateRules returned RC <%d>", ilRC );
	}
	return ilRC;
}

/*  IniPriority: initialise logical field PRIO of rules array				*/
/*	IN:		pcpUrno:	URNO of rule, can be NULL for all					*/
static int IniPriority ( char * pcpUrno )
{
	char *pclResult=0;
	long llRow;
	char clPrio[COND_COUNT];
	char clValue[21];
	long llFieldNo = -1;
	int  i, ilRC = RC_SUCCESS;

	dbg ( TRACE, "IniPriority: Start pcpUrno <%s>", pcpUrno );

	llRow = ARR_FIRST;
	while ( CEDAArrayFindRowPointer( &(rgSrhArray.rrArrayHandle),
									 rgSrhArray.crArrayName,
									 &(rgSrhArray.rrIdxHandle[0]),
									 rgSrhArray.crIdxName[0],
									 pcpUrno, &llRow, (void *) &pclResult) 
			== RC_SUCCESS )
	{
		memset ( clPrio, 0, sizeof(clPrio) );
		for ( i=0; i<COND_COUNT; i++ )
		{
			if ( rgCndFldIdxSgl[i] >= 0 )
			{		
				strcpy ( clValue, SRHFIELD(pclResult, rgCndFldIdxSgl[i]) );
				if ( clValue[0] && (clValue[0]!=' ') )
				{	
					clPrio[i] = '3';
					continue;
				}
			}
			if ( rgCndFldIdxGrp[i] >= 0 )
			{		
				strcpy ( clValue, SRHFIELD(pclResult, rgCndFldIdxGrp[i]) );
				if ( clValue[0] && (clValue[0]!=' ') )
				{	
					clPrio[i] = '2';
					continue;
				}
			}
			clPrio[i] = '0';
		}
		dbg ( DEBUG, "IniPriority: URNO <%s> PRIO <%s>",
			  SRHFIELD(pclResult,igSrhUrnoIdx), clPrio );
		ilRC = CEDAArrayPutField( &rgSrhArray.rrArrayHandle,
								rgSrhArray.crArrayName, &llFieldNo, "PRIO", 
								llRow, clPrio );	
		if ( ilRC != RC_SUCCESS )
		{
			dbg ( TRACE, "IniPriority: CEDAArrayPutField failed RC <%d>", ilRC );
			break;
		}
		llRow = ARR_NEXT;

	}
	ilRC = CEDAArrayWriteDB(&rgSrhArray.rrArrayHandle, rgSrhArray.crArrayName,
							NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);

	dbg ( TRACE, "IniPriority: End ilRC <%d>", ilRC );

	return ilRC;
}



static int InitFieldIndices()
{
	int ilRc = RC_SUCCESS;
	int ilCol,ilPos, i, ilRealFields ;
	char clFina1[11], clFina2[11];

	for ( i=0; i<COND_COUNT; i++ )
	{
		rgCndFldIdxSgl[i] = rgCndFldIdxGrp[i] = -1;
		get_real_item(clFina1,SRH_COND_SINGLE,i+1);
		FindItemInList( SRH_FIELDS, clFina1, ',', &(rgCndFldIdxSgl[i]), &ilCol,&ilPos);

		get_real_item(clFina2,SRH_COND_GROUP,i+1);
		FindItemInList( SRH_FIELDS, clFina2, ',', &(rgCndFldIdxGrp[i]), &ilCol,&ilPos);

		dbg ( DEBUG, "InitFieldIndices: SRH: <%s> <%d>  <%s> <%d>", clFina1, 
			  rgCndFldIdxSgl[i], clFina2, rgCndFldIdxGrp[i] );

		rgAftInbFldIdx[i] = rgAftOutFldIdx[i] = -1;
		get_real_item(clFina1,AFT_INB_FIELD,i+1);
		FindItemInList( AFT_FIELDS, clFina1, ',', &(rgAftInbFldIdx[i]), &ilCol,&ilPos);

		get_real_item(clFina2,AFT_OUTB_FIELD,i+1);
		FindItemInList( AFT_FIELDS, clFina2, ',', &(rgAftOutFldIdx[i]), &ilCol,&ilPos);

		dbg ( DEBUG, "InitFieldIndices: AFT: <%s> <%d>  <%s> <%d>", clFina1, 
			  rgAftInbFldIdx[i], clFina2, rgAftOutFldIdx[i] );
	
	}
	for ( i=0; i<IN_TIME_CNT; i++ )
	{
		get_real_item(clFina1,INBOUNDTIMES,i+1);
		FindItemInList( AFT_FIELDS, clFina1, ',', &(rgInbTimeIdx[i]), &ilCol,&ilPos);
		dbg ( DEBUG, "InitFieldIndices: INBOUND TIME: <%s> Idx <%d> ", clFina1, rgInbTimeIdx[i] );
	}
	for ( i=0; i<OUT_TIME_CNT; i++ )
	{
		get_real_item(clFina1,OUTBOUNDTIMES,i+1);
		FindItemInList( AFT_FIELDS, clFina1, ',', &(rgOutbTimeIdx[i]), &ilCol,&ilPos);
		dbg ( DEBUG, "InitFieldIndices: OUTBOUND TIME: <%s> Idx <%d> ", clFina1, rgOutbTimeIdx[i] );
	}

        FindItemInList(HSS_FIELDS,"URNO",',',&igHssUrnoIdx,&ilCol,&ilPos);
        FindItemInList(HSS_FIELDS,"NAME",',',&igHssNameIdx,&ilCol,&ilPos);
	
        FindItemInList(SRH_FIELDS,"ADID",',',&igSrhAdidIdx,&ilCol,&ilPos);
        FindItemInList(SRH_FIELDS,"MAXT",',',&igSrhMaxtIdx,&ilCol,&ilPos);
   	FindItemInList(SRH_FIELDS,"NAME",',',&igSrhNameIdx,&ilCol,&ilPos);
        FindItemInList(SRH_FIELDS,"UHSS",',',&igSrhUhssIdx,&ilCol,&ilPos);
        FindItemInList(SRH_FIELDS,"URNO",',',&igSrhUrnoIdx,&ilCol,&ilPos);

	FindItemInList(SDE_FIELDS,"AFTF",',',&igSdeAftfIdx,&ilCol,&ilPos);
	FindItemInList(SDE_FIELDS,"FLDR",',',&igSdeFldrIdx,&ilCol,&ilPos);
	FindItemInList(SDE_FIELDS,"RETY",',',&igSdeRetyIdx,&ilCol,&ilPos);
	FindItemInList(SDE_FIELDS,"SDEU",',',&igSdeSdeuIdx,&ilCol,&ilPos);
	FindItemInList(SDE_FIELDS,"TIFR",',',&igSdeTifrIdx,&ilCol,&ilPos);
	FindItemInList(SDE_FIELDS,"URNO",',',&igSdeUrnoIdx,&ilCol,&ilPos);
	FindItemInList(SDE_FIELDS,"USRH",',',&igSdeUsrhIdx,&ilCol,&ilPos);
	FindItemInList(SDE_FIELDS,"UTPL",',',&igSdeUtplIdx,&ilCol,&ilPos);
	FindItemInList(SDE_FIELDS,"TSKN",',',&igSdeTsknIdx,&ilCol,&ilPos);
	FindItemInList(SDE_FIELDS,"ALRT",',',&igSdeAlrtIdx,&ilCol,&ilPos);
	
	ilRealFields = get_no_of_items(SDE_FIELDS);
	igSdeAdidIdx = ilRealFields+1 ;
	igSdeLevlIdx = igSdeAdidIdx+1 ;


	FindItemInList(OST_FIELDS,"CONA",',',&igOstConaIdx,&ilCol,&ilPos);
	FindItemInList(OST_FIELDS,"CONS",',',&igOstConsIdx,&ilCol,&ilPos);
	FindItemInList(OST_FIELDS,"CONU",',',&igOstConuIdx,&ilCol,&ilPos);
	FindItemInList(OST_FIELDS,"COTY",',',&igOstCotyIdx,&ilCol,&ilPos);
	FindItemInList(OST_FIELDS,"RETY",',',&igOstRetyIdx,&ilCol,&ilPos);
	FindItemInList(OST_FIELDS,"RFLD",',',&igOstRfldIdx,&ilCol,&ilPos);
	FindItemInList(OST_FIELDS,"RTAB",',',&igOstRtabIdx,&ilCol,&ilPos);
	FindItemInList(OST_FIELDS,"RURN",',',&igOstRurnIdx,&ilCol,&ilPos);
	FindItemInList(OST_FIELDS,"SDAY",',',&igOstSdayIdx,&ilCol,&ilPos);
	FindItemInList(OST_FIELDS,"UAFT",',',&igOstUaftIdx,&ilCol,&ilPos);
	FindItemInList(OST_FIELDS,"URNO",',',&igOstUrnoIdx,&ilCol,&ilPos);
	FindItemInList(OST_FIELDS,"USDE",',',&igOstUsdeIdx,&ilCol,&ilPos);
	FindItemInList(OST_FIELDS,"UHSS",',',&igOstUhssIdx,&ilCol,&ilPos);
	FindItemInList(OST_FIELDS,"USRH",',',&igOstUsrhIdx,&ilCol,&ilPos);
	FindItemInList(OST_FIELDS,"USEC",',',&igOstUsecIdx,&ilCol,&ilPos);/* AM 20070912 */ 

	ilRealFields = get_no_of_items(OST_FIELDS);
	FindItemInList(OST_ADDFIELDS,"LEVL",',',&igOstLevlIdx,&ilCol,&ilPos);
	igOstLevlIdx += ilRealFields;
	FindItemInList(OST_ADDFIELDS,"TIFR",',',&igOstTifrIdx,&ilCol,&ilPos);
	igOstTifrIdx += ilRealFields;
	FindItemInList(OST_ADDFIELDS,"UTPL",',',&igOstUtplIdx,&ilCol,&ilPos);
	igOstUtplIdx += ilRealFields;
	FindItemInList(OST_ADDFIELDS,"AFTF",',',&igOstAftfIdx,&ilCol,&ilPos);
	igOstAftfIdx += ilRealFields;
	FindItemInList(OST_ADDFIELDS,"RETY",',',&igOstRetyIdx,&ilCol,&ilPos);
	igOstRetyIdx += ilRealFields;
	FindItemInList(OST_ADDFIELDS,"FLT2",',',&igOstFlt2Idx,&ilCol,&ilPos);
	igOstFlt2Idx += ilRealFields;
	FindItemInList(OST_ADDFIELDS,"TSKN",',',&igOstTsknIdx,&ilCol,&ilPos);
	igOstTsknIdx += ilRealFields;
	FindItemInList(OST_ADDFIELDS,"FLDR",',',&igOstFldrIdx,&ilCol,&ilPos);
	igOstFldrIdx += ilRealFields;
	FindItemInList(OST_ADDFIELDS,"ALRT",',',&igOstAlrtIdx,&ilCol,&ilPos);
	igOstAlrtIdx += ilRealFields;
	
	
	FindItemInList(AFT_FIELDS,"URNO",',',&igAftUrnoIdx,&ilCol,&ilPos);
	FindItemInList(AFT_FIELDS,"ADID",',',&igAftAdidIdx,&ilCol,&ilPos);
	FindItemInList(AFT_FIELDS,"STOA",',',&igAftStoaIdx,&ilCol,&ilPos);
	FindItemInList(AFT_FIELDS,"STOD",',',&igAftStodIdx,&ilCol,&ilPos);
	FindItemInList(AFT_FIELDS,"FLNO",',',&igAftFlnoIdx,&ilCol,&ilPos);
	FindItemInList(AFT_FIELDS,"RKEY",',',&igAftRkeyIdx,&ilCol,&ilPos);
	FindItemInList(AFT_FIELDS,"TIFA",',',&igAftTifaIdx,&ilCol,&ilPos);
	FindItemInList(AFT_FIELDS,"TIFD",',',&igAftTifdIdx,&ilCol,&ilPos);

	FindItemInList(AF1_FIELDS,"FCA1",',',&igAf1Fca1Idx,&ilCol,&ilPos);
	FindItemInList(AF1_FIELDS,"FCA2",',',&igAf1Fca2Idx,&ilCol,&ilPos);
	FindItemInList(AF1_FIELDS,"FLNU",',',&igAf1FlnuIdx,&ilCol,&ilPos);

	FindItemInList(CCA_FIELDS,"CKBA",',',&igCcaCkbaIdx,&ilCol,&ilPos);
	FindItemInList(CCA_FIELDS,"CKEA",',',&igCcaCkeaIdx,&ilCol,&ilPos);
	FindItemInList(CCA_FIELDS,"FLNU",',',&igCcaFlnuIdx,&ilCol,&ilPos);
	FindItemInList(CCA_FIELDS,"CKBS",',',&igCcaCkbsIdx,&ilCol,&ilPos);
	FindItemInList(CCA_FIELDS,"CKES",',',&igCcaCkesIdx,&ilCol,&ilPos);

	FindItemInList(JOB_FIELDS,"ACFR",',',&igJobAcfrIdx,&ilCol,&ilPos);
	FindItemInList(JOB_FIELDS,"ACTO",',',&igJobActoIdx,&ilCol,&ilPos);
	FindItemInList(JOB_FIELDS,"STAT",',',&igJobStatIdx,&ilCol,&ilPos);
	FindItemInList(JOB_FIELDS,"UAFT",',',&igJobUaftIdx,&ilCol,&ilPos);
	FindItemInList(JOB_FIELDS,"UTPL",',',&igJobUtplIdx,&ilCol,&ilPos);
	FindItemInList(JOB_FIELDS,"PLFR",',',&igJobPlfrIdx,&ilCol,&ilPos);
	FindItemInList(JOB_FIELDS,"PLTO",',',&igJobPltoIdx,&ilCol,&ilPos);

	FindItemInList(EVAL_FIELDS,"OURI",',',&igEvalOuriIdx,&ilCol,&ilPos);
	FindItemInList(EVAL_FIELDS,"OURO",',',&igEvalOuroIdx,&ilCol,&ilPos);
	FindItemInList(EVAL_FIELDS,"SRHI",',',&igEvalSrhiIdx,&ilCol,&ilPos);
	FindItemInList(EVAL_FIELDS,"SRHO",',',&igEvalSrhoIdx,&ilCol,&ilPos);
	FindItemInList(EVAL_FIELDS,"UHSS",',',&igEvalUhssIdx,&ilCol,&ilPos);

	if (bgUseRsm==TRUE)
	{/* Set the Index Number of fields for RsmArray */  /* AM: Added on 20071127 */   
		FindItemInList(RSM_FIELDS,"RURN",',',&igRsmRurnIdx,&ilCol,&ilPos);
		FindItemInList(RSM_FIELDS,"TSKN",',',&igRsmTsknIdx,&ilCol,&ilPos);
		FindItemInList(RSM_FIELDS,"SDST",',',&igRsmSdstIdx,&ilCol,&ilPos);
		FindItemInList(RSM_FIELDS,"SDED",',',&igRsmSdedIdx,&ilCol,&ilPos);
		FindItemInList(RSM_FIELDS,"ACST",',',&igRsmAcstIdx,&ilCol,&ilPos);
		FindItemInList(RSM_FIELDS,"ACED",',',&igRsmAcedIdx,&ilCol,&ilPos);
		FindItemInList(RSM_FIELDS,"PROG",',',&igRsmProgIdx,&ilCol,&ilPos);
		FindItemInList(RSM_FIELDS,"STAT",',',&igRsmStatIdx,&ilCol,&ilPos);
	}
}


/*  CheckSingleValue: check if a value and an URNO belong to the same record*/
/*	IN:		prpBasicData:	Basic data array regarded						*/
/*			pcpReqUrno:		URNO of basic data record						*/
/*			pcpActValue:	value to be tested (code field)					*/
/*	return:	TRUE if code field of record identified by URNO is pcpActValue	*/
static BOOL CheckSingleValue ( ARRAYINFO* prpBasicData, char *pcpReqUrno,
							   char * pcpActValue )
{
	long llRow=ARR_FIRST;
	BOOL blFound = FALSE;
	char *pclResult;
	char clBasicVal[101];
	
	if ( !pcpReqUrno || !pcpReqUrno[0] || (pcpReqUrno[0]==' ') )
		return TRUE;		/*  condition empty */
	
	/* get code of requested URNO pcpReqUrno from prpBasicData */
	while ( !blFound &&
		    ( CEDAArrayFindRowPointer(&(prpBasicData->rrArrayHandle),
									  prpBasicData->crArrayName,
									  &(prpBasicData->rrIdxHandle[0]),
									  prpBasicData->crIdxName[0], pcpReqUrno,
									  &llRow, (void*)&pclResult)  == RC_SUCCESS ) )
	{
		strcpy ( clBasicVal, &(pclResult[prpBasicData->plrArrayFieldOfs[1]]) );
		if ( !strcmp ( pcpActValue, clBasicVal ) )
		{
			blFound = TRUE;
			dbg ( DEBUG, "CheckSingleValue: Actual <%s> == Requested <%s>", 
				  pcpActValue, clBasicVal );
		}
		else
			dbg ( DEBUG, "CheckSingleValue: Actual <%s> != Requested <%s>", 
				  pcpActValue, clBasicVal );
		llRow = ARR_NEXT;
	}
	dbg ( DEBUG, "CheckSingleValue: Actual <%s> Req. URNO <%s> Result <%d>", 		
		  pcpActValue, pcpReqUrno, blFound );

	return blFound ;
}

/*  CheckGroupValue: check if a value is in a static group					*/
/*	IN:		prpBasicData:	Basic data array regarded						*/
/*			pcpReqGrpUrno:	URNO of static group							*/
/*			pcpActValue:	value to be tested (code field)					*/
/*	return:	TRUE if value is in group, else FALSE							*/
static BOOL CheckGroupValue ( ARRAYINFO* prpBasicData, char *pcpReqGrpUrno,
							   char * pcpActValue )
{
	long llRow=ARR_FIRST;
	long llSgmRow=ARR_FIRST;
	BOOL blFound = FALSE;
	char *pclResult, pclSgmRowPtr;
	char clActUrno[21],clKey[41];
	
	if ( !pcpReqGrpUrno || !pcpReqGrpUrno[0] || (pcpReqGrpUrno[0]==' ') )
		return TRUE;		/*  condition empty */
	
	/* get URNO of actual value clActUrno from prpBasicData */
	while ( !blFound &&
		    ( CEDAArrayFindRowPointer(&(prpBasicData->rrArrayHandle),
									  prpBasicData->crArrayName,
									  &(prpBasicData->rrIdxHandle[1]),
									  prpBasicData->crIdxName[1], pcpActValue,
									  &llRow, (void*)&pclResult)  == RC_SUCCESS ) )
	{
		strcpy ( clActUrno, &(pclResult[prpBasicData->plrArrayFieldOfs[0]]) ); 
		/* search in rgSgmArray for key pcpReqGrpUrno,clActUrno */
		sprintf ( clKey, "%s,%s", pcpReqGrpUrno, clActUrno );
		if ( CEDAArrayFindRowPointer(&(rgSgmArray.rrArrayHandle),
									  rgSgmArray.crArrayName,
									  &(rgSgmArray.rrIdxHandle[1]),
									  rgSgmArray.crIdxName[1], clKey,
									  &llSgmRow, (void*)&pclSgmRowPtr)  == RC_SUCCESS )
		{
			blFound = TRUE;
			dbg ( DEBUG, "CheckGroupValue: Found key <%s> in SGM-Array", clKey );
		}
		else
			dbg ( DEBUG, "CheckGroupValue: Key <%s> not found in SGM-Array", clKey );
		llRow = ARR_NEXT;
	}
	dbg ( DEBUG, "CheckGroupValue: Actual <%s> Req.group URNO <%s> Result <%d>", 		
		  pcpActValue, pcpReqGrpUrno, blFound );

	return blFound ;
}		
	

static int SetAATArrayInfo ( char *pcpArrayName,char *pcpArrayFieldList, 
						     long *plpFieldLens, ARRAYINFO *prpArrayInfo )
{
	int	ilRc   = RC_SUCCESS;				/* Return code */
	int ilLoop = 0;

	if(prpArrayInfo == NULL)
	{
		dbg(TRACE,"SetAATArrayInfo: invalid last parameter : null pointer not valid");
		return  RC_FAIL;
	}
	
	memset(prpArrayInfo,0x00,sizeof(prpArrayInfo));

	prpArrayInfo->rrArrayHandle = -1;
	for ( ilLoop=0; ilLoop<ARR_MAX_IDX; ilLoop++ )
		prpArrayInfo->rrIdxHandle[ilLoop] = -1;

	if( pcpArrayName && (strlen(pcpArrayName) <= ARR_NAME_LEN) )
		strcpy(prpArrayInfo->crArrayName,pcpArrayName);
	
	if(pcpArrayFieldList && (strlen(pcpArrayFieldList) <= ARR_FLDLST_LEN) )
		strcpy(prpArrayInfo->crArrayFieldList,pcpArrayFieldList);

	if(ilRc == RC_SUCCESS)
	{
		prpArrayInfo->lrArrayFieldCnt = get_no_of_items(pcpArrayFieldList);

		prpArrayInfo->plrArrayFieldLen = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
		prpArrayInfo->plrArrayFieldOfs = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));

		if( prpArrayInfo->plrArrayFieldOfs == NULL )
		{
			dbg(TRACE,"SetAATArrayInfo: plrArrayFieldOfs calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}/* end of if */
		else
		{
			prpArrayInfo->plrArrayFieldOfs[0] = 0;
			for(ilLoop = 1 ; ilLoop < prpArrayInfo->lrArrayFieldCnt ; ilLoop++)
			{
				prpArrayInfo->plrArrayFieldOfs[ilLoop] = prpArrayInfo->plrArrayFieldOfs[ilLoop -1] + (plpFieldLens[ilLoop-1] +1);
			}/* end of for */
			for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrArrayFieldCnt ; ilLoop++)
				prpArrayInfo->plrArrayFieldLen[ilLoop] = plpFieldLens[ilLoop];
		}
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		ilRc = AATArrayCreate( &(prpArrayInfo->rrArrayHandle), pcpArrayName,
							   TRUE, 100, prpArrayInfo->lrArrayFieldCnt,
							   plpFieldLens, pcpArrayFieldList, NULL );
		if(ilRc != RC_SUCCESS)
			dbg(TRACE,"SetAAtArrayInfo: CEDAArrayInitialize failed <%d>",ilRc);
	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
		ilRc = GetTotalRowLength( pcpArrayFieldList, prpArrayInfo, &(prpArrayInfo->lrArrayRowLen) );
       	if(ilRc != RC_SUCCESS)
           	dbg(TRACE,"SetAATArrayInfo: GetTotalRowLength failed <%s> <%s>",pcpArrayName,pcpArrayFieldList);
   	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
		prpArrayInfo->lrArrayRowLen++;
		prpArrayInfo->pcrArrayRowBuf = (char *) calloc(1,(prpArrayInfo->lrArrayRowLen+1));
   		if(prpArrayInfo->pcrArrayRowBuf == NULL)
   		{
      		ilRc = RC_FAIL;
      		dbg(TRACE,"SetAATArrayInfo: calloc failed");
   		}/* end of if */
   	}/* end of if */


	if ( (ilRc == RC_SUCCESS) && strstr(pcpArrayFieldList,"URNO") )
	{
		strcpy ( prpArrayInfo->crIdxName[0], "URNOIDX" );
		ilRc = CEDAArrayCreateSimpleIndexDown(&(prpArrayInfo->rrArrayHandle),
											  prpArrayInfo->crArrayName,
											  &(prpArrayInfo->rrIdxHandle[0]), 
											  prpArrayInfo->crIdxName[0], "URNO" );
		if(ilRc != RC_SUCCESS)                                             
		{
			dbg( TRACE,"SetAAtArrayInfo: CEDAArrayCreateSimpleIndexDown for %s failed <%d>",
				 pcpArrayName, ilRc);		
		}
	}

	return(ilRc);
	
} /* end of SetAAtArrayInfo */


/*  CreateSkeleton: create new status in NEWOST for results in rgEvalResults	*/
/*	IN:		prpFlightArray: array where flight data should be taken from		*/
static int CreateSkeleton ( ARRAYINFO* prpFlightArray )
{
	int		ilRc = RC_SUCCESS, ilRc1 = RC_SUCCESS;
	long	llEvalRow = ARR_FIRST;
	char	*pclResult;
	char	clOuri[21], clOuro[21], clUsrhI[21], clUsrhO[21], clUhss[21];

	CEDAArrayDelete ( &(rgNewOstArr.rrArrayHandle), rgNewOstArr.crArrayName );
	while ( CEDAArrayGetRowPointer( &(rgEvalResults.rrArrayHandle),
									rgEvalResults.crArrayName,
									llEvalRow, (void *)&pclResult) == RC_SUCCESS )
	{	/*  fields of pclResult "UAFT,UHSS,USRH",  SDE-Idx: "USRH,URNO" */
		strcpy ( clOuri, EVALFIELD(pclResult,igEvalOuriIdx) ); 
		strcpy ( clOuro, EVALFIELD(pclResult,igEvalOuroIdx) );
		strcpy ( clUsrhI, EVALFIELD(pclResult,igEvalSrhiIdx) );
		strcpy ( clUsrhO, EVALFIELD(pclResult,igEvalSrhoIdx) );
		strcpy ( clUhss, EVALFIELD(pclResult,igEvalUhssIdx) );
		
		if ( !IS_EMPTY (clOuri) && IS_EMPTY (clUsrhI) )
		{
			dbg ( DEBUG, "CreateSkeleton: No rule for inbound <%s> in section <%s>", 
				  clOuri, clUhss );	
		}
		if ( !IS_EMPTY (clOuro) && IS_EMPTY (clUsrhO) )
		{
			dbg ( DEBUG, "CreateSkeleton: No rule for outbound <%s> in section <%s>", 
				  clOuro, clUhss );	
		}

		if ( !IS_EMPTY (clUsrhI) || !IS_EMPTY (clUsrhO) )
		{	/* at least one rule found for rotation/flight */
			if ( !strcmp ( clUsrhI,clUsrhO )  )
			{
				dbg ( DEBUG, "CreateSkeleton: Going to apply Turnaround Rule <%s>", clUsrhI );
				ilRc1 = CreateSkeleton4Rule ( prpFlightArray, clUsrhI, clOuri, clOuro, clUhss );
				if ( ilRc1 != RC_SUCCESS )
				{
					dbg ( TRACE, "CreateSkeleton: CreateSkeleton4Rule TURN <%s><%s> USRH <%s> failed RC <%d>",
						 clOuri, clOuro, clUsrhI, ilRc1 );
				}
			}
			else
			{
				if ( !IS_EMPTY (clUsrhI) )
				{
					dbg ( DEBUG, "CreateSkeleton: Going to apply inbound Rule <%s>", clUsrhI );
					ilRc1 = CreateSkeleton4Rule ( prpFlightArray, clUsrhI, clOuri, "", clUhss );
					if ( ilRc1 != RC_SUCCESS )
					{
						dbg ( TRACE, "CreateSkeleton: CreateSkeleton4Rule INBOUND <%s> USRH <%s> failed RC <%d>",
							 clOuri, clUsrhI, ilRc1 );
					}
				}
				if ( !IS_EMPTY (clUsrhO) )
				{
					dbg ( DEBUG, "CreateSkeleton: Going to apply outbound Rule <%s>", clUsrhO ); /* MSI: 20080617: Changed from clUsrhI to clUsrhO */    
					ilRc1 = CreateSkeleton4Rule ( prpFlightArray, clUsrhO, "", clOuro, clUhss );
					if ( ilRc1 != RC_SUCCESS )
					{
						dbg ( TRACE, "CreateSkeleton: CreateSkeleton4Rule OUTBOUND <%s> USRH <%s> failed RC <%d>",
							 clOuro, clUsrhO, ilRc1 );
					}
				}
			}
		}
		llEvalRow = ARR_NEXT;
	}

	return ilRc;
}

/********************************************************************/
static int GetNextUrno(char *pcpUrno)
{
	int ilRC = RC_SUCCESS ;
	char  pclDataArea[IDATA_AREA_SIZE] ;
	
	if (igReservedUrnoCnt <= 0)
	{                            /* hole Urno buffer -> get new Urnos */
		memset( pclDataArea, 0, IDATA_AREA_SIZE) ;
		if ((ilRC = GetNextValues(pclDataArea, URNOS_TO_FETCH)) != RC_SUCCESS)
		{
			dbg ( TRACE,"GetNextValues failed RC <%d>", ilRC );
		}
		else
		{
			strcpy ( pcpUrno, pclDataArea );
			igReservedUrnoCnt = URNOS_TO_FETCH - 1 ;
			lgActUrno = atol(pcpUrno);
			dbg ( DEBUG, "GetNextUrno: fetched %d Urnos. Next is <%s>", 
				  URNOS_TO_FETCH, pcpUrno ) ;
		}
	}
	else
	{
		igReservedUrnoCnt-- ;
		lgActUrno++ ;
		sprintf ( pcpUrno, "%ld", lgActUrno );      /* get next reserved Urno */
		dbg ( DEBUG, "GetNextUrno: Next Urno <%s>, %d Urnos remaining", 
			  pcpUrno, igReservedUrnoCnt ) ;
	}
	
	return (ilRC) ;
} /* getNextUrno () */

void TrimRight(char *s)
{
    int i = 0;    /* search for last non-space character */
    for (i = strlen(s) - 1; i >= 0 && isspace(s[i]); i--); /* trim off right spaces */    
	s[++i] = '\0';
}

static char* ToUpper( char* pcpSt )
{
	char *pclDest = pcpSt;
    char *pclSrc = pcpSt;

	while (*pclSrc != '\0')
    {
		*pclDest = toupper(*pclSrc);
		pclDest++;         
		pclSrc++;
    } /* end while */
  return pcpSt;
}

/*  FillOstRecord: initialise new status record with data already known		*/
/*		   i.e. CDAT,RFLD,RTAB,RURN,SDAY,UAFT,UHSS,USDE,USEC,USRH			*/
/*	IN:	prpFlightArray: array where flight data should be taken from		*/
/*		pcpBuf:		result buffer											*/
/*		ipBufLen:	length of result buffer									*/
/*		pcpSdePtr:	row of used status definition, must not be NULL			*/
/*		pcpOuri:	inbound-URNO, must not be NULL, if "" single outbound	*/
/*		pcpUaft:	outbound-URNO, must not be NULL, if "" single inbound	*/
/*		pcpUhss:	URNO of handling status sect., must not be NULL			*/	
/*	OUT:pcpBuf:		result buffer filled									*/
static int FillOstRecord (ARRAYINFO* prpFlightArray, char *pcpBuf, int ipBufLen,
						  char *pcpSdePtr, char *pcpOuri, char *pcpOuro, char *pcpUhss)
{	/* "CDAT,CONA,CONS,COTY,RFLD,RTAB,RURN,SDAY,UAFT,UHSS,URNO,USDE,USEC,USRH"*/
	char	clNow[21], *pclFlight, clSday[21], clFldr[11]; 
	char	clRtab[4], clRurn[11], clUaft[11], *pclAftf, *pclRety; 
	long	llAftRow = ARR_FIRST;
	int		ilRc, ilIdx=-1, ilPos, ilCol;

        ilRc = RC_SUCCESS;/* AM 20071010 */  
        clUaft[0] = '\0';/* AM 20071010 */  
        bgSkipFillOstRecord = FALSE;/* AM 20071011 */   

	if ( !pcpOuri || !pcpOuro || !pcpUhss || !pcpBuf || !pcpSdePtr )
	{
		dbg ( TRACE, "FillOstRecord: At least one parameter is NULL" );
		return RC_INVALID;
	}
	strcpy ( clFldr, SDEFIELD(pcpSdePtr,igSdeFldrIdx) );
	pclAftf = SDEFIELD(pcpSdePtr,igSdeAftfIdx);
	pclRety = SDEFIELD(pcpSdePtr,igSdeRetyIdx);
	TrimRight ( clFldr );
	strcpy ( clRurn, " " );

	/*  set RTAB */
	strcpy ( clRtab, "AFT" );
	if ( strcmp ( clFldr, "PLAN" ) == 0 )
	{
		strncpy ( clRtab, pclRety, 3 );
		clRtab[3] = '\0';
		ilRc = GetNomField ( pclAftf, clFldr, pclRety );
		if ( ilRc != RC_SUCCESS )
			dbg ( TRACE, "FillOstRecord: GetNomField for AFTF <%s> RETY <%s> failed RC <%d>", 
						 pclAftf, pclRety, ilRc );

	}
	else if ( strcmp ( clFldr, "CONA" ) == 0 )
	{
		strcpy ( clRtab, "OST" );
	}
	else if (( strncmp (clFldr, "OTA", 3) ==0) || ( strncmp (clFldr, "OTD", 3) ==0))/* AM 20071127 */  
	{/* Schedule time is coming from other system. Refer to "RSM" tab */   
		strcpy ( clRtab, "RSM" );
	}
	

	/*  SET UAFT to the flight responsible for actual value (CONA) */
	if ( strncmp ( pclRety, "AFT", 3 ) == 0 )
	{
		FindItemInList( AFT_INBOUND_FIELDS, pclAftf, ',', &ilIdx, &ilCol,&ilPos);
		if ( ilIdx > 0 )
			strcpy ( clUaft, pcpOuri );
		else
			strcpy ( clUaft, pcpOuro );
	}			
	else if ( strncmp ( pclRety, "JOB", 3 ) == 0 )
	{
		if ( *SDEFIELD(pcpSdePtr,igSdeAdidIdx) == 'A' )
			strcpy ( clUaft, pcpOuri );
		else
			strcpy ( clUaft, pcpOuro );
	}
	else if ( strncmp ( pclRety, "CCA", 3 ) == 0 )
	{
		strcpy ( clUaft, pcpOuro );
	}
	else if ( strncmp ( pclRety, "OTH", 3 ) == 0 )   
	{
		if ( *SDEFIELD(pcpSdePtr,igSdeAdidIdx) == 'A' )
			strcpy ( clUaft, pcpOuri );
		else
		{
			strcpy ( clUaft, pcpOuro );
			if (IS_EMPTY(pcpOuro)) strcpy( clUaft, pcpOuri );/* AM 20071204 */  
		}
	}
	
	
	/*  SET RURN to the flight responsible for scheduled value (CONS) */
	/*  if FLDR='CONA' RURN is the URNO of predecessor OSTTAB-record -> filled later */
	if ( strcmp ( clFldr, "CONA" ) != 0 )
	{
		if ( strcmp ( clFldr, "PLAN" ) == 0 )
			strcpy ( clRurn, clUaft );
		else
		{
			FindItemInList( INBOUNDTIMES, clFldr, ',', &ilIdx, &ilCol,&ilPos);
			if ( ilIdx > 0 )
				strcpy ( clRurn, pcpOuri );
			else
				strcpy ( clRurn, pcpOuro );
		}
	}

	GetServerTimeStamp("UTC", 1, 0, clNow);

	memset ( pcpBuf, ' ', ipBufLen );

	if ( IS_EMPTY(clUaft) )
	{
		if (strncmp( pclRety, "OTH", 3 )==0)  
		{
			dbg( TRACE, "SDEADID<%c>" , *SDEFIELD(pcpSdePtr,igSdeAdidIdx));
		}
		dbg ( TRACE, "FillOstRecord: Failure UAFT is empty, USDE <%s> OURI <%s>  OURO <%s>, FLDR <%s>, RETY <%s>, AFTF <%s>, RTAB <%s>", 
			  SDEFIELD(pcpSdePtr,igSdeUrnoIdx), pcpOuri, pcpOuro, clFldr, pclRety, pclAftf, clRtab );
		return RC_FAILURE;
	}
	ilRc = CEDAArrayFindRowPointer(	&(prpFlightArray->rrArrayHandle),
									prpFlightArray->crArrayName,
									&(prpFlightArray->rrIdxHandle[0]),
									prpFlightArray->crIdxName[0], clUaft, 
									&llAftRow, (void *)&pclFlight ) ;
	if ( ilRc != RC_SUCCESS )
	{
		dbg ( TRACE, "FillOstRecord: Flight <%s> not found", clUaft );
	}
	else
	{
		if ( *AFTFIELD(pclFlight,igAftAdidIdx) == 'D' )
			strcpy ( clSday, AFTFIELD(pclFlight,igAftTifdIdx) );
		else
			strcpy ( clSday, AFTFIELD(pclFlight,igAftTifaIdx) );
		clSday[8] = '\0';
									
		/* "CDAT,CONA,CONS,COTY,RFLD,RTAB,RURN,SDAY,UAFT,UHSS,URNO,USDE,USEC,USRH"*/
		sprintf ( pcpBuf, "%s, ,UNDEF, ,%s,%s,%s,%s,%s,%s, ,%s,STAHDL,%s",
				  clNow, clFldr, clRtab, clRurn, clSday, clUaft, pcpUhss, 
				  SDEFIELD(pcpSdePtr,igSdeUrnoIdx), SDEFIELD(pcpSdePtr,igSdeUsrhIdx) );
		strcat ( pcpBuf, ", , , , , , " );  /* empty logical fields */
		dbg(TRACE,"===> NEW OSTTAB RECORD:");
		dbg(TRACE,"FIELDS: <CDAT,CONA,CONS,COTY,RFLD,RTAB,RURN,SDAY,UAFT,UHSS,URNO,USDE,USEC,USRH>");
		dbg(TRACE,"DATA  : <%s>",pcpBuf);
		dbg(DEBUG,"-------");
		delton ( pcpBuf );
	}
	return ilRc;
}


/*  UpdateOSTTAB: update OSTTAB from NEWOST array after rule evaluation		*/
static int UpdateOSTTAB ( ARRAYINFO* prpFlightArray )
{
	int		ilRc = RC_SUCCESS, ilRc1 = RC_SUCCESS, ilUsed;
	long	llEvalRow = ARR_FIRST, llSdeRow, llOldRow, llNewRow;
	char	*pclResult, *pclOstOld, *pclOstNew, *pclSdePtr, clUhss[21];
	char	clKeyOld[31], clKeyNew[41],clUsde[21], clUrno[21];
	char	clOuri[21], clOuro[21];
 	char	*pclUaft, *pclUsrh, *pclAftTmp;
	long	llFieldNo = -1, llCount;
	int		ilAdded=0, ilDeleted=0, ilKept=0, i;
	char	clUsec[32]="\0";

        dbg(TRACE,"ENTERING UpdateOSTTAB");

	while ( CEDAArrayGetRowPointer( &(rgEvalResults.rrArrayHandle),
									rgEvalResults.crArrayName,
									llEvalRow, (void *)&pclResult) == RC_SUCCESS )
	{	/*  fields of pclResult "UAFT,UHSS,USRH",  OST-Idx: "UAFT,UHSS" */
		strcpy ( clOuri, EVALFIELD(pclResult,igEvalOuriIdx) ); 
		strcpy ( clOuro, EVALFIELD(pclResult,igEvalOuroIdx) );
		strcpy ( clUhss, EVALFIELD(pclResult,igEvalUhssIdx) );
		
		for ( i=0; i<2; i++ )
		{
			if ( i==0 )
				pclUaft = clOuri;
			else
				pclUaft = clOuro;
			if ( !IS_EMPTY ( pclUaft )  )
			{
				sprintf ( clKeyOld, "%s,%s", pclUaft, clUhss ); 
				dbg ( DEBUG, "UpdateOSTTAB: Search old Status with key <%s>", clKeyOld );	
				llOldRow = ARR_LAST;
				while ( CEDAArrayFindRowPointer(&(rgOstArray.rrArrayHandle),
												rgOstArray.crArrayName,
												&(rgOstArray.rrIdxHandle[1]),
												rgOstArray.crIdxName[1],clKeyOld,
												&llOldRow, (void*)&pclOstOld ) == RC_SUCCESS )
				{
					strcpy ( clUsde, OSTFIELD(pclOstOld, igOstUsdeIdx) );
					strcpy ( clUsec, OSTFIELD(pclOstOld, igOstUsecIdx) );

					sprintf ( clKeyNew, "%s,%s", pclUaft, clUsde ); 
					dbg ( DEBUG, "UpdateOSTTAB: Search in new status with key <%s>", clKeyNew );
					
					llNewRow = ARR_FIRST;
					ilUsed = CEDAArrayFindRowPointer(&(rgNewOstArr.rrArrayHandle),
													 rgNewOstArr.crArrayName,
													 &(rgNewOstArr.rrIdxHandle[1]),
													 rgNewOstArr.crIdxName[1], clKeyNew,
													 &llNewRow, (void*)&pclOstNew );
					if ( ilUsed != RC_SUCCESS )
					{		
						/* dbg( TRACE, "Delete Row from Old Array <%s>, <%s> ", clKeyOld, clKeyNew ); */ /* AM 20070912 */ 
						ilRc1 = CEDAArrayDeleteRow ( &(rgOstArray.rrArrayHandle),  
													 rgOstArray.crArrayName, llOldRow );
						if ( ilRc1 != RC_SUCCESS )
						{
							dbg ( TRACE, "UpdateOSTTAB: CEDAArrayDeleteRow Row <%ld> of old status failed, RC <%d>",
								  llOldRow, ilRc1 );
							ilRc = RC_FAIL;
						}
						else
						{
							/* dbg ( TRACE, "UpdateOSTTAB: Old Status USDE <%s> is not used any longer", clUsde ); */ 
							ilDeleted++;
						}
					}
					else
					{
						/* TODO: possibly update old status row */
						ilRc1 = UpdateStatus ( llOldRow, pclOstOld, pclOstNew );
						if ( ilRc1 != RC_SUCCESS )
						{
							dbg ( TRACE, "UpdateOSTTAB: UpdateStatus Row <%ld> failed, RC <%d>", 
								  llOldRow, ilRc1 );
							ilRc = RC_FAIL;
						}
						/* dbg( TRACE, "Delete Row from New Array <%s>, <%s> ", clKeyOld, clKeyNew ); */ /* AM 20070912 */ 
						ilRc1 = CEDAArrayDeleteRow ( &(rgNewOstArr.rrArrayHandle),  
													 rgNewOstArr.crArrayName, llNewRow );
						if ( ilRc1 != RC_SUCCESS )
						{
							dbg ( TRACE, "UpdateOSTTAB: CEDAArrayDeleteRow Row <%ld> of new status failed, RC <%d>",
								  llNewRow, ilRc1 );
							ilRc = RC_FAIL;
						}
						else
						{
							/* dbg ( TRACE, "UpdateOSTTAB: New Status USDE <%s> already in DB", clUsde ); */ 
							ilKept++;
						}
					}
					llOldRow = ARR_PREV;
				}
			}
		}
		llEvalRow = ARR_NEXT;
	}
	/*  if more than 100 new status have to be inserted, we better			*/
	/*	disactivate indices on OSTTAB										*/
	CEDAArrayGetRowCount(&(rgNewOstArr.rrArrayHandle),
						 rgNewOstArr.crArrayName,&llCount);
	if ( llCount > 100 )
	{
		dbg ( DEBUG, "UpdateOSTTAB: Going to disactivate indices on <OST>" );	
		for ( i=0; i<=2; i++ )
			if ( CEDAArrayDisactivateIndex (&(rgOstArray.rrArrayHandle),	
											&(rgOstArray.crArrayName[0]),
											&(rgOstArray.rrIdxHandle[i]),
											rgOstArray.crIdxName[i] ) != RC_SUCCESS )
				dbg ( TRACE, "UpdateOSTTAB: CEDAArrayDisactivateIndex Index <%d> failed", i );


	}
	/* copy all new status to rgOstArray and set URNO */
	llNewRow = ARR_FIRST;
	while ( CEDAArrayGetRowPointer( &(rgNewOstArr.rrArrayHandle),
									rgNewOstArr.crArrayName,
									llNewRow, (void *)&pclOstNew) == RC_SUCCESS )
	{	
		llOldRow = ARR_LAST;
		ilRc1 = CEDAArrayAddRow(&rgOstArray.rrArrayHandle, rgOstArray.crArrayName,
								&llOldRow, (void*)pclOstNew );
		if ( ilRc1 != RC_SUCCESS )
		{
			dbg ( TRACE, "UpdateOSTTAB: CEDAArrayAddRow for new status failed RC <%d>", ilRc1 );
		}
		else
		{
			ilRc1 = GetNextUrno ( clUrno );
			if ( ilRc1 != RC_SUCCESS )
				dbg ( TRACE, "UpdateOSTTAB: GetNextUrno failed RC <%d>", ilRc1 );
		}
		if ( ilRc1 == RC_SUCCESS )
		{
			ilRc1 = CEDAArrayPutField( &rgOstArray.rrArrayHandle,
									   rgOstArray.crArrayName, &llFieldNo, 
									   "URNO", llOldRow, clUrno );	
			if ( ilRc1 != RC_SUCCESS )
				dbg ( TRACE, "UpdateOSTTAB: CEDAArrayPutField URNO failed Row <%ld> RC <%d>", 
					  llOldRow, ilRc1 );
			else
				ilAdded++;
		}
		if ( ilRc1 != RC_SUCCESS )
			ilRc = RC_FAIL;
		llNewRow = ARR_NEXT;
	}
	dbg ( DEBUG, "UpdateOSTTAB: Going to activate indices on <OST> again" );	
	if ( llCount > 100 )
	{
		for ( i=0; i<=2; i++ )
			if ( CEDAArrayActivateIndex(&(rgOstArray.rrArrayHandle),	
										&(rgOstArray.crArrayName[0]),
										&(rgOstArray.rrIdxHandle[i]),
										rgOstArray.crIdxName[i] ) != RC_SUCCESS )
				dbg ( TRACE, "UpdateOSTTAB: CEDAArrayActivateIndex Index <%d> failed", i );
	}

	dbg ( TRACE, "UpdateOSTTAB: Going to set predecessors" );	
	
        /* TODO: Set RURN for all status with SDE.FLDR=="CONA" and set CONS */
	llFieldNo = -1;
	llEvalRow = ARR_FIRST;
	while ( CEDAArrayGetRowPointer( &(rgEvalResults.rrArrayHandle),
									rgEvalResults.crArrayName,
									llEvalRow, (void *)&pclResult) == RC_SUCCESS )
	{	/*  fields of pclResult "UAFT,UHSS,USRH",  OST-Idx: "UAFT,UHSS,RTAB" */
		strcpy ( clOuri, EVALFIELD(pclResult,igEvalOuriIdx) ); 
		strcpy ( clOuro, EVALFIELD(pclResult,igEvalOuroIdx) );
		strcpy ( clUhss, EVALFIELD(pclResult,igEvalUhssIdx) );
		
		for ( i=0; i<2; i++ )
		{
			if ( i==0 )
				pclUaft = clOuri;
			else
				pclUaft = clOuro;
			if ( !IS_EMPTY ( pclUaft )  )
			{
				sprintf ( clKeyOld, "%s,%s", pclUaft, clUhss );
											  
				/* dbg ( DEBUG, "UpdateOSTTAB: Search Status to update RURN with key <%s>", clKeyOld );	 */ 
				llOldRow = ARR_FIRST;
				/* Search all status which are related to a predecessor status */
				while ( CEDAArrayFindRowPointer(&(rgOstArray.rrArrayHandle),
												rgOstArray.crArrayName,
												&(rgOstArray.rrIdxHandle[1]),
												rgOstArray.crIdxName[1],clKeyOld,
												&llOldRow, (void*)&pclOstOld ) == RC_SUCCESS )
				{
					if ( strcmp ( OSTFIELD (pclOstOld, igOstRtabIdx), "OST" ) == 0 )
					{
						strcpy ( clUsde, OSTFIELD(pclOstOld, igOstUsdeIdx) );
						dbg ( DEBUG, "UpdateOSTTAB: Found status USDE <%s> Row <%ld>", clUsde );
						
						/* Search definition of this status */
						llSdeRow = ARR_FIRST;
						ilRc1 = CEDAArrayFindRowPointer(&(rgSdeArray.rrArrayHandle),
														 rgSdeArray.crArrayName,
														 &(rgSdeArray.rrIdxHandle[0]),
														 rgSdeArray.crIdxName[0], clUsde,
														 &llSdeRow, (void*)&pclSdePtr );
						if ( ilRc1 != RC_SUCCESS )
						{		
							dbg ( TRACE, "UpdateOSTTAB: SDE-record key <%s> not found RC <%d>", 
								  clUsde, ilRc1 ) ;
						}
						else
						{	/* IDXOST2 fields: "UAFT,USDE" */
							/* Search predecessor status in online status */
							llNewRow = ARR_FIRST;
							sprintf ( clKeyNew, "%s,%s,%s", pclUaft, OSTFIELD (pclOstOld, igOstUhssIdx),  
									  SDEFIELD(pclSdePtr,igSdeSdeuIdx) );
							dbg ( DEBUG, "UpdateOSTTAB: Search predecessor status using 1st key <%s>", clKeyNew );
							ilRc1 = CEDAArrayFindRowPointer(&(rgOstArray.rrArrayHandle),
															rgOstArray.crArrayName,
															&(rgOstArray.rrIdxHandle[2]),
															rgOstArray.crIdxName[2], clKeyNew,
															&llNewRow, (void*)&pclOstNew );
							if ( ilRc1 != RC_SUCCESS )
							{
								pclAftTmp = (i==0) ? clOuro : clOuri;
								if ( !IS_EMPTY(pclAftTmp ))
								{
									llNewRow = ARR_FIRST;
									sprintf ( clKeyNew, "%s,%s,%s", pclAftTmp, OSTFIELD (pclOstOld, igOstUhssIdx),  
																 SDEFIELD(pclSdePtr,igSdeSdeuIdx) );
									dbg ( DEBUG, "UpdateOSTTAB: Search predecessor status using 1st key <%s>", clKeyNew );
									ilRc1 = CEDAArrayFindRowPointer(&(rgOstArray.rrArrayHandle),
																	rgOstArray.crArrayName,
																	&(rgOstArray.rrIdxHandle[2]),
																	rgOstArray.crIdxName[2], clKeyNew,
																	&llNewRow, (void*)&pclOstNew );	
									dbg ( TRACE, "UpdateOSTTAB: predecessor status using key <%s> not found RC <%d>", 
												  clKeyNew, ilRc1 ) ;
								}
							}
						}
						if ( ilRc1 == RC_SUCCESS )
						{
							/*  Set URNO of predecessor as RURN */
							strcpy ( clUrno, OSTFIELD(pclOstNew, igOstUrnoIdx) ); 
							ilRc1 = CEDAArrayPutField( &rgOstArray.rrArrayHandle,
													   rgOstArray.crArrayName, &llFieldNo, 
													   "RURN", llOldRow, clUrno );	
							if ( ilRc1 != RC_SUCCESS )
								dbg ( TRACE, "UpdateOSTTAB: CEDAArrayPutField RURN <%s> failed Row <%ld> RC <%d>", 
									  clUrno, llOldRow, ilRc1 );
							else
								dbg ( DEBUG, "UpdateOSTTAB: CEDAArrayPutField RURN <%s> Row <%ld>  OK", 
									  clUrno, llOldRow );
						}
					}
					llOldRow = ARR_NEXT;
				}	/* endof while-loop rgOstArray */

				/*  Fill logical fields */
				ilRc1 = PrepareOSTArray ( pclUaft, clUhss );
				if ( ilRc1 != RC_SUCCESS )
					dbg ( TRACE, "UpdateOSTTAB: PrepareOSTArray UAFT <%s> UHSS <%s> failed", 
								  pclUaft, clUhss );
				ilRc1 = SetInitialCONA ( pclUaft, clUhss );
				if ( ilRc1 != RC_SUCCESS )
					dbg ( TRACE, "UpdateOSTTAB: SetInitialCONA UAFT <%s> UHSS <%s> failed", 
								  pclUaft, clUhss );
				ilRc1 = SetCONS ( prpFlightArray, pclUaft, clUhss, FALSE );
				if ( ilRc1 != RC_SUCCESS )
					dbg ( TRACE, "UpdateOSTTAB: SetCONS UAFT <%s> UHSS <%s> failed", 
								  pclUaft, clUhss );
				ilRc1 = ProcessSCC( pclUaft, clUhss );
				if ( ilRc1 != RC_SUCCESS )
					dbg ( TRACE, "UpdateOSTTAB: ProcessSCC UAFT <%s> UHSS <%s> failed", 
								  pclUaft, clUhss );
			}	
		}	/* end for loop over the two flights of rotation */
		llEvalRow = ARR_NEXT;
	} /* endof while-loop rgEvalResults */

	dbg ( TRACE, "UpdateOSTTAB: Finished with RC <%d>, Added <%d> kept <%d> deleted <%d> rows", 
		  ilRc1, ilAdded, ilKept, ilDeleted ); /* MSI: 20080714: Changed ilRc to ilRc1   */ 

dbg(TRACE,"LEAVING UpdateOSTTAB");
	return ilRc;
}

/*  SetCONS: set logical fields of loaded online status						*/
/*	IN:	prpFlightArray:	flight array to be examined							*/	
/*		pcpUaft:		flight URNO, can be NULL for all flights			*/
/*		pcpUhss:		URNO of handling status sect., can be NULL for all	*/
/*		bpSave:			save OSTTAB if TRUE									*/
static int SetCONS ( ARRAYINFO *prpFlightArray, char *pcpUaft, char *pcpUhss, 
					 BOOL bpSave )
{
	char clOstKey[31]="", *pclOstPtr, clTime[21]="";
	char *pclRtab, *pclRurn, *pclUaft, *pclFlt2;
	char *pclRfld; /* AM 20071128 */ 
	char *pclUsec,*pclCons,pclUsecSt[32]="\0" ;/* AM 20070912 */ 
	long llOstRow;
	long llTifr, llFieldNo=-1;
	int ilRc1, ilRc = RC_SUCCESS;
	char *pclTskn;/* AM 20071127 */ 
	char *pclUost,*pclUsde,*pclUsrh,*pclUhss,*pclRety;/* AM 20080305 */ 

	BOOL bUpdCons = TRUE;

	/*  SetCONS must set times with increasing LEVL to avoid undefined times of successors */

	if ( pcpUaft && pcpUaft[0] )
	{
		strcpy ( clOstKey, pcpUaft );
		if ( pcpUhss && pcpUhss[0] )
		{
			strcat ( clOstKey, "," );
			strcat ( clOstKey, pcpUhss );
		}
	}


	dbg ( TRACE, "SetCONS: key for Status to update <%s>", clOstKey );

        dbg(TRACE,"LOOK for all status with UAFT=pcpUaft");

	/*  look for all status with UAFT=pcpUaft */
	llOstRow = ARR_FIRST;
	while ( CEDAArrayFindRowPointer(&(rgOstArray.rrArrayHandle),
								    rgOstArray.crArrayName,
									&(rgOstArray.rrIdxHandle[1]),
									rgOstArray.crIdxName[1],clOstKey,
									&llOstRow, (void*)&pclOstPtr ) == RC_SUCCESS )
	{
		bUpdCons = TRUE; /* AM 20070912 */ 

		llTifr = atol ( OSTFIELD(pclOstPtr,igOstTifrIdx ) );
		pclRtab = OSTFIELD(pclOstPtr,igOstRtabIdx);
		pclRurn = OSTFIELD(pclOstPtr,igOstRurnIdx);
		pclUaft = OSTFIELD(pclOstPtr,igOstUaftIdx);
		pclFlt2 = OSTFIELD(pclOstPtr,igOstFlt2Idx);
		pclUsec = OSTFIELD(pclOstPtr,igOstUsecIdx);/* AM 20070912 */ 
		pclCons = OSTFIELD( pclOstPtr,igOstConsIdx );/* AM 20070912 */ 
		pclRfld = OSTFIELD( pclOstPtr,igOstRfldIdx); /* AM 20071128 */  
		/* dbg( TRACE, "SetCONS:1 row data <%s,%s,%s,%s,%d,%s>", pclRtab, pclRurn, pclUaft, pclFlt2, llTifr, pclUsec ); */ /* AM 20070912 */ 
		if ( strncmp ( pclRtab,"AFT", 3 ) == 0 )
	        {   
         		ilRc1 = GetFlightTime ( prpFlightArray, pclRurn,  
								OSTFIELD(pclOstPtr,igOstRfldIdx), clTime );  
                }       
		else
			if ( strncmp ( pclRtab,"OST", 3 ) == 0 )
			{		
				/* Added by AM 20070912 - Start */ 
				/* To get back the original CONS if it is updated by 'GOCHDL' */ 
				if (strncmp(pclUsec,"GOCHDL",6)==0)
				{
					ilRc1 = RC_SUCCESS;
					bUpdCons = FALSE;
					dbg(TRACE, "SetCONS: Get back the org CONS<%s>", pclCons);
				}
				else
				{
                                        ilRc1 = GetStatusTime ( pclRurn, clTime );
					dbg(TRACE, "SetCONS: Get from Ref. field for CONS<%s>", pclCons);
                                }
				/* Added by AM 20070912 - End */ 
			}
		else
			if ( strncmp ( pclRtab,"JOB", 3 ) == 0 )
				ilRc1 = GetConsJob ( pclUaft, pclFlt2, OSTFIELD(pclOstPtr,igOstUtplIdx), 
									 clTime, OSTFIELD(pclOstPtr,igOstRetyIdx) );
		else
			if ( strncmp ( pclRtab,"CCA", 3 ) == 0 )
				ilRc1 = GetConsCca ( pclUaft, clTime, OSTFIELD(pclOstPtr,igOstRetyIdx) );
		else 
                        if (strncmp( pclRtab,"RSM",3 )==0)
		        {/* AM 20071128. */ 
			    /* Get Cons from Rsm table */
			    ilRc1 = GetConsRsm( pclUaft, OSTFIELD( pclOstPtr, igOstTsknIdx ), OSTFIELD(pclOstPtr, igOstFldrIdx), clTime );
		        }
		else 
                        bUpdCons = FALSE;/* AM 20070912 */ 

 
		if (bUpdCons == TRUE)
		{
			if ( ilRc1 != RC_SUCCESS )
			{
				dbg ( TRACE, "SetCONS: Evaluation of status time RTAB <%s> RURN <%s> failed RC <%d>", 
					  pclRtab, pclRurn, ilRc1 );
				strcpy ( clTime, "UNDEF" );   
			}
			else if (strcmp(clTime,"")!=0)
				AddSecondsToCEDATime(clTime,llTifr,1);

                        ilRc1 = CEDAArrayPutField( &rgOstArray.rrArrayHandle,
								   rgOstArray.crArrayName, &llFieldNo, 
									"CONS", llOstRow, clTime );  
		       
                        pclUost = OSTFIELD( pclOstPtr, igOstUrnoIdx );
			pclUsde = OSTFIELD( pclOstPtr, igOstUsdeIdx );
			pclUsrh = OSTFIELD( pclOstPtr, igOstUsrhIdx );
			pclUhss = OSTFIELD( pclOstPtr, igOstUhssIdx );
			pclTskn = OSTFIELD( pclOstPtr, igOstTsknIdx );
			pclRety = OSTFIELD( pclOstPtr, igOstRetyIdx );
		
            dbg(TRACE,"CALLING CONS CHANGES FROM: SetCONS PART 1");	

			if (ProcessCONSChanges( pclUost, pclUaft, pclUsde, pclUsrh, pclUhss, 
                                                                  clTime, pclTskn, pclRety )!=RC_SUCCESS)
            {
	            dbg(TRACE, "SetCONS: ProcessCONSChanges failed." );
            }
			if ( ilRc1 != RC_SUCCESS )
				ilRc = RC_FAIL;
		}
		
		llOstRow = ARR_NEXT;
	}

        dbg(TRACE,"LOOK for all status with RURN=pcpUaft AND RURN!=UAFT");

	/*  look for all status with RURN=pcpUaft AND RURN!=UAFT */
	llOstRow = ARR_FIRST;
	while ( CEDAArrayFindRowPointer(&(rgOstArray.rrArrayHandle),
								    rgOstArray.crArrayName,
									&(rgOstArray.rrIdxHandle[3]),
									rgOstArray.crIdxName[3],clOstKey,
									&llOstRow, (void*)&pclOstPtr ) == RC_SUCCESS )
	{
		llTifr = atol ( OSTFIELD(pclOstPtr,igOstTifrIdx ) );
		pclRtab = OSTFIELD(pclOstPtr,igOstRtabIdx);
		pclRurn = OSTFIELD(pclOstPtr,igOstRurnIdx);
		pclUaft = OSTFIELD(pclOstPtr,igOstUaftIdx);
		pclFlt2 = OSTFIELD(pclOstPtr,igOstFlt2Idx);
		/* dbg( TRACE, "SetCONS:2 row data <%s,%s,%s,%s,%d>", pclRtab, pclRurn, pclUaft, pclFlt2, llTifr ); */ /* AM 20070912 */
		if ( strcmp(pclRurn,pclUaft) && !strncmp ( pclRtab,"AFT", 3 ) )
		{
			ilRc1 = GetFlightTime ( prpFlightArray, pclRurn, 
									OSTFIELD(pclOstPtr,igOstRfldIdx), clTime );
			if ( ilRc1 != RC_SUCCESS )
			{
				dbg ( TRACE, "SetCONS: Evaluation of status time RTAB <%s> RURN <%s> failed RC <%d>", 
					  pclRtab, pclRurn, ilRc1 );
				strcpy ( clTime, "UNDEF" );
			}
			else
				AddSecondsToCEDATime(clTime,llTifr,1);
			
			ilRc1 = CEDAArrayPutField( &rgOstArray.rrArrayHandle,
									   rgOstArray.crArrayName, &llFieldNo, 
										"CONS", llOstRow, clTime );
			pclUost = OSTFIELD( pclOstPtr, igOstUrnoIdx );
			pclUsde = OSTFIELD( pclOstPtr, igOstUsdeIdx );
			pclUsrh = OSTFIELD( pclOstPtr, igOstUsrhIdx );
			pclUhss = OSTFIELD( pclOstPtr, igOstUhssIdx );
			pclTskn = OSTFIELD( pclOstPtr, igOstTsknIdx );
			pclRety = OSTFIELD( pclOstPtr, igOstRetyIdx );
			
                        dbg(TRACE,"CALLING CONS CHANGES FROM: SetCONS PART 2");	

			if (ProcessCONSChanges( pclUost, pclUaft, pclUsde, pclUsrh, pclUhss, 
                                                                  clTime, pclTskn, pclRety )!=RC_SUCCESS)
                        {
	                        dbg(TRACE, "SetCONS: ProcessCONSChanges failed." );
                        }
			if ( ilRc1 != RC_SUCCESS )
			{
				dbg ( TRACE, "SetCONS: CEDAArrayPutField failed Row <%ld> RC <%d>", 
					llOstRow, ilRc1 );
			}
			if ( ilRc1 != RC_SUCCESS )
				ilRc = RC_FAIL;
		}
		llOstRow = ARR_NEXT;
	}

	/* dbg ( TRACE, "SetCONS: ilRc <%d>", ilRc ); */ 
	return ilRc;
}

/*  PrepareOSTArray: set logical fields of loaded online status				*/
/*	IN:		pcpUaft:	flight URNO, can be NULL for all flights			*/
/*			pcpUhss:	URNO of handling status sect., can be NULL for all  */
/*			bpSave:		save OSTTAB if TRUE									*/
static int PrepareOSTArray ( char *pcpUaft, char *pcpUhss/*, BOOL bpSave */)
{
	char clOstKey[31]="", clUsde[21], clUrno[21], clUaft2[11];
	char *pclOstPtr, *pclSdePtr, *pclPredPtr, clTifr[8];
	long llOstRow, llRowPred, llSdeRow, llTifr;
	long llFldLevlNo=-1, llFldRetyNo=-1, llFldTifrNo=-1, llFldUtplNo=-1, llFldTskn=-1, llFldFldr = -1, llFldAlrt = -1;
	long llFldAftfNo=-1, llFldFlt2No=-1;
	int	 ilLevl, ilRc = RC_SUCCESS, ilRc1,ilRc2 ;
	BOOL blChangedSomething ;
	int ilSuccessors=0, ilIdx=3;
	char traceSt[128] = "";
	char pclNewData[128] = "";
	char pclOldData[128] = "";

	if ( pcpUaft && pcpUaft[0] )
	{
		strcpy ( clOstKey, pcpUaft );
	}
	dbg ( DEBUG, "PrepareOSTArray: key for Status to update <%s>", clOstKey );
	
	/*  first loop set RETY, TIFR UTPL, and level "00", if RTAB="AFT" */
	if ( !pcpUaft )
	{	/* discativate indices if all flights are going to be processed */
		ilRc1 = CEDAArrayDisactivateIndex ( &(rgOstArray.rrArrayHandle),	
											&(rgOstArray.crArrayName[0]),
											&(rgOstArray.rrIdxHandle[1]),
											rgOstArray.crIdxName[1] );
		ilRc1 |= CEDAArrayDisactivateIndex ( &(rgOstArray.rrArrayHandle),	
											&(rgOstArray.crArrayName[0]),
											&(rgOstArray.rrIdxHandle[4]),
											rgOstArray.crIdxName[4] );
		dbg ( DEBUG, "PrepareOSTArray: CEDAArrayDisactivateIndex returns <%d>", ilRc1 );	
	}

	llOstRow = ARR_FIRST; 
	while ( CEDAArrayFindRowPointer(&(rgOstArray.rrArrayHandle),
									rgOstArray.crArrayName, 
									&(rgOstArray.rrIdxHandle[2]),
									rgOstArray.crIdxName[2], clOstKey,
									&llOstRow, (void*)&pclOstPtr ) == RC_SUCCESS )
	{
		dbg ( DEBUG, "PrepareOSTArray: Found OST-Record URNO <%s> USDE <%s>", 
			  OSTFIELD(pclOstPtr, igOstUrnoIdx), OSTFIELD(pclOstPtr, igOstUsdeIdx) );

		if ( pcpUhss &&  pcpUhss[0] && 
			 strcmp (OSTFIELD(pclOstPtr, igOstUhssIdx), pcpUhss ) )
		{
			dbg ( DEBUG, "PrepareOSTArray: Found status with wrong UHSS <%s> != <%s>", 
				  OSTFIELD(pclOstPtr, igOstUhssIdx), pcpUhss ) ;	
		}
		else
		{			
			llSdeRow = ARR_FIRST;
			strcpy ( clUsde, OSTFIELD(pclOstPtr, igOstUsdeIdx) );
			ilRc1 = CEDAArrayFindRowPointer(&(rgSdeArray.rrArrayHandle),
											rgSdeArray.crArrayName,
											&(rgSdeArray.rrIdxHandle[0]),
											rgSdeArray.crIdxName[0], clUsde,
											&llSdeRow, (void*)&pclSdePtr );
			if ( ilRc1 != RC_SUCCESS )
			{		
				dbg ( TRACE, "PrepareOSTArray: SDE-record URNO <%s> not found RC <%d>", 
					  clUsde, ilRc1 ) ;
			}
			else
			{
				sprintf( traceSt, "SDEID: <%s> : ", SDEFIELD(pclSdePtr,igSdeUrnoIdx) );
				ilRc1 |= ilRc2 = CEDAArrayPutField( &rgOstArray.rrArrayHandle,
										   rgOstArray.crArrayName, &llFldRetyNo, 
											"RETY", llOstRow, 
											SDEFIELD(pclSdePtr,igSdeRetyIdx) );
				ilRc1 |= ilRc2 = CEDAArrayPutField( &rgOstArray.rrArrayHandle,
											rgOstArray.crArrayName, &llFldAftfNo, 
											"AFTF", llOstRow, 
											SDEFIELD(pclSdePtr,igSdeAftfIdx) );
				llTifr = atol ( SDEFIELD(pclSdePtr,igSdeTifrIdx) );
				llTifr *= 60;
				sprintf ( clTifr, "%ld", llTifr );
				ilRc1 |= ilRc2 = CEDAArrayPutField( &rgOstArray.rrArrayHandle,
											rgOstArray.crArrayName, &llFldTifrNo, 
											"TIFR", llOstRow, clTifr );
				ilRc1 |= ilRc2 = CEDAArrayPutField( &rgOstArray.rrArrayHandle,
											rgOstArray.crArrayName, &llFldUtplNo, 
											"UTPL", llOstRow, 
											SDEFIELD(pclSdePtr,igSdeUtplIdx) );
				ilRc1 |= ilRc2 = CEDAArrayPutField( &rgOstArray.rrArrayHandle,
											rgOstArray.crArrayName, &llFldLevlNo, 
											"LEVL", llOstRow, SDEFIELD(pclSdePtr,igSdeLevlIdx)  );
				if (ilRc2) sprintf( traceSt, "%s, LEVL<%d>", traceSt, ilRc2 );											
				if ( GetFlt2Value ( &rgAftArray, OSTFIELD(pclOstPtr, igOstUaftIdx), 
									OSTFIELD(pclOstPtr, igOstUsrhIdx), clUaft2 ) == RC_SUCCESS )
					ilRc1 |= ilRc2 = CEDAArrayPutField( &rgOstArray.rrArrayHandle,
												rgOstArray.crArrayName, &llFldFlt2No, 
												"FLT2", llOstRow, clUaft2 );
				if (ilRc2) sprintf( traceSt, "%s, FLT2<%d>", traceSt, ilRc2 );												
				if (bgUseRsm==TRUE)
				{/* AM 20071127, Put Task Name virtual field value in OstArray */ 
			            strcpy( pclNewData, SDEFIELD(pclSdePtr,igSdeTsknIdx));
				    TrimRight( pclNewData );
				    strcpy( pclOldData, OSTFIELD(pclOstPtr, igOstTsknIdx) );
				    TrimRight( pclOldData );
				    if ( strcmp( pclNewData, pclOldData ) )
				    {/* AM:20080226 - To Update only if there is any changes in TSKN */ 				    	
						ilRc1 |= ilRc2 = CEDAArrayPutField( &rgOstArray.rrArrayHandle, /* AM 20071127 */ 
													rgOstArray.crArrayName, &llFldTskn, 
													"TSKN", llOstRow, 
													pclNewData );
						if (ilRc2) sprintf( traceSt, "%s, TSKN<%d>", traceSt, ilRc2 );		
					}	
					
				    if (strcmp(ToUpper(SDEFIELD(pclSdePtr,igSdeFldrIdx)),OSTFIELD(pclOstPtr, igOstFldrIdx) ))
				    {/* AM:20080226 - To Update only if there is any changes in FLDR	*/ 
				        								
						ilRc1 |= ilRc2 = CEDAArrayPutField( &rgOstArray.rrArrayHandle, /* AM 20071127 */ 
													rgOstArray.crArrayName, &llFldFldr, 
													"FLDR", llOstRow, 
													SDEFIELD(pclSdePtr,igSdeFldrIdx) );
						if (ilRc2) sprintf( traceSt, "%s, <%s>, FLDR<%d>", traceSt, SDEFIELD(pclSdePtr,igSdeFldrIdx), ilRc2 );
						/* else dbg(TRACE, "PrepareOSTArray: OK : %s: <%s>", traceSt, SDEFIELD(pclSdePtr,igSdeFldrIdx)	); */ 
						/* if (ilRc2) sprintf( traceSt, "%s, FLDR<%d>", traceSt, ilRc2 );	*/ 
					}											
				}
				if (strcmp(ToUpper(SDEFIELD(pclSdePtr,igSdeAlrtIdx)),OSTFIELD(pclOstPtr, igOstAlrtIdx) ))
				{/* AM:20080226 - To Update only if there is any changes in ALRT.	 */ 
					ilRc1 |= ilRc2 = CEDAArrayPutField( &rgOstArray.rrArrayHandle, /* AM 20071221 */ 
													rgOstArray.crArrayName, &llFldAlrt, 
													"ALRT", llOstRow, 
													ToUpper(SDEFIELD(pclSdePtr,igSdeAlrtIdx)) );
					if (ilRc2) sprintf( traceSt, "%s, <%s>, ALRT<%d>", traceSt, SDEFIELD(pclSdePtr,igSdeAlrtIdx), ilRc2 );
				}									
				if ( ilRc1 != RC_SUCCESS )
				{
					dbg ( TRACE, "PrepareOSTArray: CEDAArrayPutField failed Row <%ld> RC <%d> : %s", 
						  llOstRow, ilRc1, traceSt );
					ilRc = RC_FAIL;
				}
			}
		}
		llOstRow = ARR_NEXT;
	}

	if ( !pcpUaft )
	{	/* discativate indices if all flights are going to be processed */
		ilRc1 = CEDAArrayActivateIndex( &(rgOstArray.rrArrayHandle),	
										&(rgOstArray.crArrayName[0]),
										&(rgOstArray.rrIdxHandle[1]),
										rgOstArray.crIdxName[1] );
		ilRc1 |= CEDAArrayActivateIndex ( &(rgOstArray.rrArrayHandle),	
										  &(rgOstArray.crArrayName[0]),
										  &(rgOstArray.rrIdxHandle[4]),
										  rgOstArray.crIdxName[4] );
		dbg ( DEBUG, "PrepareOSTArray: CEDAArrayActivateIndex returns <%d>", ilRc1 );		  		  
	}
	return ilRc;
}

/*  GetFlightTime:	Get content of time field if filled, otherwise best		*/
/*					time known, which is less precise than the requested 	*/
/*	IN:		prpFlightArray:	Array to search the flight in					*/
/*			pcpUaft:	flight URNO, must not be NULL						*/
/*			pcpFld:		requested time field,  must not be NULL				*/
/*	OUT:	pcpTime:	result buffer, must not be NULL						*/
static int GetFlightTime ( ARRAYINFO *prpFlightArray, char *pcpUaft, 
						   char *pcpFld, char *pcpTime )
{
	char *pclFlight, *pclVal;
	long llAftRow = ARR_FIRST;
	int  i, ilRc, *pilTimesIdx=0, ilStartIdx, ilPos, ilCol ;
	BOOL blNoArrivalDepartureTime = FALSE;			
    
	if ( !prpFlightArray || !pcpUaft || !pcpFld || !pcpTime )
	{
		dbg ( TRACE, "GetFlightTime: At least one parameter is null!" );
		return RC_INVALID ;
	}
	/* dbg( TRACE, "GetFlightTime:IN: UAFT <%s>, Fld <%s>", pcpUaft, pcpFld ); */ /* AM 20071204 */ 
	memset ( pcpTime, 0, 15 );

	ilRc = CEDAArrayFindRowPointer(&(prpFlightArray->rrArrayHandle),
									prpFlightArray->crArrayName,
									&(prpFlightArray->rrIdxHandle[0]),
									prpFlightArray->crIdxName[0],pcpUaft,
									&llAftRow, (void*)&pclFlight );
	if ( ilRc != RC_SUCCESS )
		dbg ( TRACE, "GetFlightTime: Flight <%s> not found, RC <%d>", pcpUaft, ilRc );
	else
	{
		ilStartIdx = get_item_no(INBOUNDTIMES,pcpFld,strlen(pcpFld)+1 ); 
		if ( ilStartIdx >= 0 )
		{	/* reference time is an inbound time */
			pilTimesIdx =rgInbTimeIdx;
		}
		else
		{
			ilStartIdx = get_item_no(OUTBOUNDTIMES,pcpFld,strlen(pcpFld)+1 ); 
			if ( ilStartIdx >= 0 )
			{	/* reference time is an inbound time */
				pilTimesIdx =rgOutbTimeIdx;
			}
		}		
		if ( !pilTimesIdx )
		{	/*  Maybe one of the new nominal fields is requested */
			FindItemInList( AFT_FIELDS, pcpFld, ',', &ilStartIdx, &ilCol,&ilPos);
			if ( ilStartIdx <= 0 )
			{
				dbg ( TRACE, "GetFlightTime: Requested field <%s> not found in used fieldlist", pcpFld );
				ilRc = RC_FAIL;
			}
			blNoArrivalDepartureTime = TRUE;			
		}
	}
	if ( ilRc == RC_SUCCESS )
	{
		if ( blNoArrivalDepartureTime )
		{
			pclVal = AFTFIELD ( pclFlight, ilStartIdx );
			if ( ( pclVal[0]!=' ' ) && ( strlen ( pclVal) >= 14 ) )
				strncpy ( pcpTime, pclVal, 14 );
		}
		else
			for ( i=ilStartIdx; !pcpTime[0] &&(i>=0); i-- )
			{
				pclVal = AFTFIELD ( pclFlight, pilTimesIdx[i] );
				if ( ( pclVal[0]!=' ' ) && ( strlen ( pclVal) >= 14 ) )
					strncpy ( pcpTime, pclVal, 14 );
			}
		if ( !pcpTime[0] )
		{
			dbg ( TRACE, "GetFlightTime: No Flight time filled, FLD <%s> IDX <%d> UAFT <%s>", 
				  pcpFld, ilStartIdx, pcpUaft );
			ilRc = RC_NOTFOUND;
		}
	}	
	/* dbg ( TRACE, "GetFlightTime:  UAFT <%s> FLD <%s> TIME <%s>", pcpUaft, pcpFld, pcpTime ); */  /* AM 20071207	*/ 
	return ilRc;
}

/*  GetStatusTime: Get the first status field filled from "CONU,CONA,CONS"  */
/*	IN:		pcpUost:	URNO of status, must not be NULL					*/
/*	OUT:	pcpTime:	result buffer,  must not be NULL					*/
static int GetStatusTime ( char *pcpUost, char *pcpTime )
{
	char *pclStatus, *pclVal;
	long llOstRow = ARR_FIRST;
	int ilRc, *pilTimesIdx=0, ilStartIdx ;

	if ( !pcpUost || !pcpTime )
	{
		dbg ( TRACE, "GetStatusTime: At least one parameter is null!" );
		return RC_INVALID ;
	}
	memset ( pcpTime, 0, 15 );

	ilRc = CEDAArrayFindRowPointer(&(rgOstArray.rrArrayHandle),
									rgOstArray.crArrayName,
									&(rgOstArray.rrIdxHandle[0]),
									rgOstArray.crIdxName[0],pcpUost,
									&llOstRow, (void*)&pclStatus );
	if ( ilRc != RC_SUCCESS )
		dbg ( TRACE, "GetStatusTime: Predecessor <%s> not found, RC <%d>", pcpUost, ilRc );
	else
	{
		pclVal = OSTFIELD ( pclStatus, igOstConuIdx );
		if ( ( pclVal[0]!=' ' ) && ( strlen ( pclVal) >= 14 ) )
	        {  
          		strncpy ( pcpTime, pclVal, 14 );    
                }   
		else
		{
			pclVal = OSTFIELD ( pclStatus, igOstConaIdx );
			if ( ( pclVal[0]!=' ' ) && ( strlen ( pclVal) >= 14 ) )
		        {   
                 		strncpy ( pcpTime, pclVal, 14 );      
                        }   
			else
			{
				pclVal = OSTFIELD ( pclStatus, igOstConsIdx );
				if ( ( pclVal[0]!=' ' ) && ( strlen ( pclVal) >= 14 ) )
                                {  
                         		strncpy ( pcpTime, pclVal, 14 );   
                                }   
			}
		}
		if ( !pcpTime[0] )
		{
			dbg ( TRACE, "GetStatusTime: No Status time filled, UOST <%s>", pcpUost );
			ilRc = RC_NOTFOUND;
		}
	}	
	dbg ( DEBUG, "GetStatusTime:  UOST <%s> TIME <%s>", pcpUost, pcpTime ); 
	
	return ilRc;
}

/*  SendAnswer: Send answer on incoming event to originator					*/
/*	IN:		prpEvent:	incoming event										*/
/*			ipRc:		return code to be set in outgoing event				*/
static int SendAnswer(EVENT *prpEvent,int ipRc)
{
	int	      ilRc           = RC_SUCCESS;			/* Return code */

	BC_HEAD *prlBchead       = NULL;
	CMDBLK  *prlCmdblk       = NULL;
	char    *pclSelection    = NULL;
	char    *pclFields       = NULL;
	char	*pclData         = NULL;
	int ilLen;
	
	prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
	prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
	pclSelection = prlCmdblk->data;
	pclFields    = pclSelection + strlen(pclSelection) + 1;
	pclData      = pclFields + strlen(pclFields) + 1;

	prlBchead->rc = ipRc;

	ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 
			strlen(pclSelection) + strlen(pclFields) + strlen(pclData) + 20; 

	dbg(TRACE,"SendAnswer to: %d",prpEvent->originator);
	if ((ilRc = que(QUE_PUT, prpEvent->originator, mod_id, PRIORITY_3, 
					ilLen, (char*)prpEvent)) != RC_SUCCESS)
	{
		dbg(TRACE,"SendAnswer: QUE_PUT failed RC <%d>", ilRc );
	}

	return ilRc;
}


/*  ProcessCFS: process CFS-command, i. e. creation of skeleton				*/
/*	IN:		pcpAftSelection:	where-clause for flights to be evaluated	*/
static int ProcessCFS ( char *pcpAftSelection )
{
	int ilRc = RC_SUCCESS, ilLen;
	char *pclSel = 0, *ps;
	BOOL blReload = FALSE;
	char clTmp[101], clStart[9], clEnd[9];

	ilLen = strlen (pcpAftSelection)+50;
	pclSel = malloc ( ilLen * sizeof(char) );
	if ( !pclSel )
	{
		dbg ( TRACE, "ProcessCFS: Malloc of <%d> char failed", ilLen );
		ilRc = RC_NOMEM;
	}
	else
	{
		sprintf ( pclSel, "where rkey in (select rkey from afttab %s",  pcpAftSelection );
		/* strcpy ( pclSel, pcpAftSelection ); */
		if ( ps = strchr ( pclSel, '|' ) )
			*ps  = '\0';
		strcat ( pclSel, ")" );
		ilRc = CEDAArrayRefill(&rgReqAftArray.rrArrayHandle, 
							 rgReqAftArray.crArrayName, pclSel, "", ARR_FIRST );
		if ( ilRc != RC_SUCCESS )
			dbg ( TRACE, "ProcessCFS: CEDAArrayRefill <AFT> failed, RC <%d>", ilRc );
	}
	ps = strchr ( pcpAftSelection, '|' );
	if ( ps )
		ps++;
	ilRc = EvaluateRules ( &rgReqAftArray, ps );
	if ( ilRc == RC_SUCCESS )
	{
		ilRc = CreateSkeleton ( &rgReqAftArray );
		if ( ilRc != RC_SUCCESS )
			dbg ( TRACE, "ProcessCFS: CreateSkeleton failed RC <%d>", ilRc );
	}
	if ( ilRc == RC_SUCCESS )
	{
		/*  Reload OSTTAB, if requested flights are not completely inside	*/
		/*	loaded time frame												*/
		if ( cgMinAftTime[0] && ( strncmp(cgMinAftTime,cgLoadStart,8) < 0 ) )
			blReload = TRUE;
		if ( strncmp(cgLoadEnd,cgMaxAftTime,8) < 0 ) 
			blReload = TRUE;
		if ( blReload )
		{
			strncpy ( clStart, cgMinAftTime, 8 );
			strncpy ( clEnd, cgMaxAftTime, 8 );
			clStart[8] = clEnd[8] = '\0';
			sprintf(clTmp, "WHERE SDAY BETWEEN '%s' AND '%s'", clStart,clEnd );
			dbg ( TRACE, "CALCULATION OUTSIDE LOADED TIMEFRAME !!!" );
			dbg ( TRACE, "ProcessCFS: Reloading time frame, selection <%s>", clTmp );
			ilRc = CEDAArrayRefill(&rgOstArray.rrArrayHandle, 
								rgOstArray.crArrayName, clTmp, "", ARR_FIRST );
			if ( ilRc != RC_SUCCESS )
				dbg ( TRACE, "ProcessCFS: CEDAArrayRefill <OST> failed RC <%d>", ilRc );
		}

		ilRc = UpdateOSTTAB ( &rgReqAftArray );
		if ( ilRc != RC_SUCCESS )
			dbg ( TRACE, "ProcessCFS: UpdateOSTTAB failed RC <%d>", ilRc );
	}
	PrepareSBC ( 0, &rgReqAftArray );
	SaveOstTab ();

	if ( blReload )
	{
		strncpy ( clStart, cgLoadStart, 8 );
		strncpy ( clEnd, cgLoadEnd, 8 );
		sprintf(clTmp, "WHERE SDAY BETWEEN '%s' AND '%s'", clStart,clEnd );
		dbg ( TRACE, "ProcessCFS: Reloading time frame, selection <%s>", clTmp );
		ilRc = CEDAArrayRefill( &rgOstArray.rrArrayHandle, 
								rgOstArray.crArrayName, clTmp, "", ARR_FIRST );
		if ( ilRc != RC_SUCCESS )
			dbg ( TRACE, "ProcessCFS: CEDAArrayRefill <OST> failed RC <%d>", ilRc );
		ilRc = PrepareOSTArray (0, 0 );
		SaveOstTab ();
	}
		
	if ( pclSel )
		free ( pclSel );
	return ilRc;
}
		
/*  ProcessSCC: set conflict field OSTTAB.COTY for flight and HSS			*/
/*	IN:		pcpUaft:	flight URNO, can be NULL for all flights			*/
/*			pcpUhss:	URNO of handling status sect., can be NULL for all  */
static int ProcessSCC( char *pcpUaft, char *pcpUhss )
{
	long llRow = ARR_FIRST, llFieldNo=-1;
	char *pclStat;
	char *pclCoty, *pclCona, *pclCons;
	char clNow[20], clCotyNew[2]=" ";
	int ilRc = RC_SUCCESS, ilRc1;
	char clKey[31]="";

	dbg(DEBUG,"PROCESS SCC START");
	GetActDate ( clNow );
    
	if ( pcpUaft && pcpUaft[0] )
	{
		strcpy ( clKey, pcpUaft );
		if ( pcpUhss && pcpUhss[0] )
		{
			strcat ( clKey, "," );
			strcat ( clKey, pcpUhss );
		}
	}

	dbg ( DEBUG, "ProcessSCC: actual date and time <%s> Key <%s>", clNow, clKey );
	while ( CEDAArrayFindRowPointer(&(rgOstArray.rrArrayHandle),
								   rgOstArray.crArrayName,
								   &(rgOstArray.rrIdxHandle[1]),
									rgOstArray.crIdxName[1], clKey, &llRow,
								   (void *)&pclStat) == RC_SUCCESS )
	{
		pclCoty = OSTFIELD(pclStat,igOstCotyIdx);
		pclCona = OSTFIELD(pclStat,igOstConuIdx);
		/*  confirmation time by User (CONU) has to be used, if filled,
			otherwise  confirmation time by system (CONA) has to be used */
		if ( IS_EMPTY(pclCona) )
			pclCona	= OSTFIELD(pclStat,igOstConaIdx);
		pclCons = OSTFIELD(pclStat,igOstConsIdx);
			
		clCotyNew[0] = *pclCoty;
		if ( IS_EMPTY(pclCons) || !strncmp( pclCons, "UNDEF", 5) )
		{	/*  scheduled time (CONS) is empty => no conflict */
			clCotyNew[0] = ' ';	
		}
		else if ( IS_EMPTY(pclCona) )
		{	/* actual confirmation (CONA) is empty */
			if ( strcmp ( clNow, pclCons ) > 0 ) 
			{
				if ( *pclCoty!='A' )
					clCotyNew[0] = 'N' ;
			}
			else
				clCotyNew[0] = ' ';
		}	
		else
		{	/*  actual confirmation (CONA) is filled */
			if ( strcmp ( pclCona, pclCons ) > 0 ) 
			{
				if ( *pclCoty!='A') 
					clCotyNew[0] = 'N';
			}
			else
			{
				clCotyNew[0] = 'C';
			}
		}
		if ( *pclCoty != *clCotyNew )
		{
			dbg ( DEBUG, "ProcessSCC: Now <%s> CONA <%s> CONS <%s> COTY <%s> -> <%s>",
				  clNow, pclCona, pclCons, pclCoty, clCotyNew );
			ilRc1 = CEDAArrayPutField( &rgOstArray.rrArrayHandle,
									   rgOstArray.crArrayName, &llFieldNo, 
									   "COTY", llRow, clCotyNew );				
			if ( ilRc1 != RC_SUCCESS )
			{
				dbg ( TRACE, "ProcessSCC: Set COTY <%s> Row <%ld> failed, RC <%d>", 
							  clCotyNew, llRow, ilRc1 );
				ilRc = RC_FAIL;
			}			
			else
				dbg ( DEBUG, "ProcessSCC: Set COTY <%s> Row <%ld> successful", 
							 clCotyNew, llRow );
		}
		else
			dbg ( DEBUG, "ProcessSCC: Now <%s> CONA <%s> CONS <%s> COTY remains <%s>",
				  clNow, pclCona, pclCons, pclCoty );
		llRow = ARR_NEXT;
	}
	dbg(DEBUG,"PROCESS SCC END");
	return ilRc;
}


static int SaveOstTab ()
{
	int ilRc;
	char	*pcpSelList=0;

	/* dbg( TRACE, "Start SaveOstTab:cgBcField<%s>,cgBcData<%s>",cgBcField ,cgBcData ); */   /* AM 20070912 */ 
	if ( cgBcField[0] && cgBcData[0] )
	{	/*  don't send single BC, we use SBC */
		CEDAArraySendChanges2BCHDL(&rgOstArray.rrArrayHandle, 
								   rgOstArray.crArrayName, FALSE );
	}
	else
		CEDAArraySendChanges2BCHDL(&rgOstArray.rrArrayHandle, 
								   rgOstArray.crArrayName, bgSingleBC );

	ilRc = CEDAArrayWriteDB(&rgOstArray.rrArrayHandle, rgOstArray.crArrayName,
							&pcpSelList,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);
	if ( ilRc != RC_SUCCESS )
		dbg ( TRACE, "SaveOstTab: CEDAArrayWriteDB returned RC <%d>", ilRc );
	else
	{
		/* dbg( TRACE, "SaveOstTab: selList: <%s>", pcpSelList ); */ 
		if (!IS_EMPTY( pcpSelList ))
		{
			int ilRc1 = RC_SUCCESS;
			ilRc1 = ComputeAllAlertCharsForOstRecords( pcpSelList );
			if (ilRc1!= RC_SUCCESS)
			{
				dbg( TRACE, "Fail to Save Alert Char." );
			}
		}
		if ( pcpSelList && pcpSelList[0] && cgBcField[0] && cgBcData[0] )
		{
			/*  send SBC, if cgBcField and cgBcData have been initialized	*/
			/*  and if pcpSelList is filled									*/
			ilRc = tools_send_info_flag( 1900, 0, "BCHDL", "STAHDL", "STAHDL", "", 
										"", "", cgTwEnd, "SBC","OSTTAB","",
										cgBcField, cgBcData, 0);          
			/* dbg ( TRACE, "SaveOstTab: Send SBC: <%s> RC <%d>", cgBcData, ilRc ); */ 
		}
	}
	return ilRc;
}


/*  GetConaFlight: get actual status confirmation time for flight 			*/
/*	IN:		pcpUaft:	flight URNO, must not be NULL						*/
/*			pcpRfld:	reference field (AFT), which is used as conf. time  */
/*	OUT:	pcpCona:	time from pcpRfld, if flight is found, else ""		*/
static int GetConaFlight ( char *pcpUaft, char *pcpRfld, char* pcpCona )
{
	char *pclFlight, *pclAf1Rec;
	long llRow = ARR_FIRST;
	int  ilFldIdx, ilRc = RC_SUCCESS, ilCol, ilPos;
	
	if ( !pcpUaft || !pcpCona || !pcpRfld )
	{
		dbg ( TRACE, "GetConaFlight: At least one parameter is NULL" );
		return RC_INVALID;
	}
	pcpCona[0] = '\0';

	FindItemInList( AFT_FIELDS, pcpRfld, ',', &ilFldIdx, &ilCol,&ilPos);
	if ( ilFldIdx > 0 )
	{
		ilRc = CEDAArrayFindRowPointer(&(rgAftArray.rrArrayHandle),
										rgAftArray.crArrayName,
										&(rgAftArray.rrIdxHandle[0]),
										rgAftArray.crIdxName[0], pcpUaft, 
										&llRow, (void *)&pclFlight );
		if ( ilRc != RC_SUCCESS ) 
		{
			dbg ( TRACE, "GetConaFlight: Flight <%s> not found", pcpUaft );
		}
		else
		{
			if ( !IS_EMPTY ( AFTFIELD(pclFlight,ilFldIdx) ) )
				strcpy ( pcpCona, AFTFIELD(pclFlight,ilFldIdx) );
		}
	}
	else
	{
		FindItemInList( AF1_FIELDS, pcpRfld, ',', &ilFldIdx, &ilCol,&ilPos);
		if ( ilFldIdx > 0 )
		{
			llRow = ARR_FIRST;
			ilRc = CEDAArrayFindRowPointer(&(rgAf1Array.rrArrayHandle),
											rgAf1Array.crArrayName,
											&(rgAf1Array.rrIdxHandle[1]),
											rgAf1Array.crIdxName[1], pcpUaft, 
											&llRow, (void *)&pclAf1Rec );
			if ( ilRc != RC_SUCCESS ) 
			{
				dbg ( TRACE, "GetConaFlight: No AF1 record found for flight <%s>", pcpUaft );
			}
			else
			{
				if ( !IS_EMPTY ( AF1FIELD(pclAf1Rec,ilFldIdx) ) )
					strcpy ( pcpCona, AF1FIELD(pclAf1Rec,ilFldIdx) );
			}
		}
		else
		{
			dbg ( TRACE, "GetConaFlight: Reference field <%s> not found in used fieldlists (AFT,AF1)", pcpRfld );
			return RC_INVALID;
		}
	}

	dbg ( TRACE, "GetConaFlight: RFLD <%s> UAFT <%s> => CONA <%s> RC <%d>", 
		  pcpRfld, pcpUaft, pcpCona, ilRc  );
	return ilRc;
}

/*  GetConaJob: get actual status confirmation time for jobs	 			*/
/*	IN:		pcpUaft:	flight URNO, must not be NULL						*/
/*			pcpUtpl:	template  of jobs that are used for the status		*/
/*			pcpRety:	type of reference JOBI(informed), JOBC(confirmed),	*/
/*						JOBF(finished)										*/
/*	OUT:	pcpCona:	confirmation time, if existing						*/
static int GetConaJob ( char *pcpUaft, char *pcpUtpl, char* pcpCona, char *pcpRety,
					    char *pcpUaft2, char *pcpConaTurn )
{
	char *pclJob, *pclStat, *pclActo, *pclAcfr;
	long llRow = ARR_FIRST;
	int  ilType=0, ilJobs=0, ilSet=0, i;
	char clCona[20], clKey[31], clNow[21], clEndTime[21];
	char *pclFlights[2];
	
	if ( !pcpUaft || !pcpCona || !pcpRety || !pcpUtpl || IS_EMPTY(pcpUtpl) )
	{
		dbg ( TRACE, "GetConaJob: At least one parameter is NULL" );
		return RC_INVALID;
	}

	GetActDate ( clNow );
	clCona[0] = '\0';
	pcpCona[0] = pcpConaTurn[0] = '\0';

	if ( !strncmp(pcpRety, "JOBI", 4 ) )
		ilType = 1;
	else if ( !strncmp(pcpRety, "JOBC", 4 ) )
	{
		ilType = 2;
	}
	else if ( !strncmp(pcpRety, "JOBF", 4 ) )
	{
		ilType = 3;
	}
	else
	{
		dbg ( TRACE, "GetConaJob: Unknown status type <%s>", pcpRety );
		return RC_INVALID;
	}
	pclFlights[0] = pcpUaft;
	pclFlights[1] = pcpUaft2;

	for ( i=0; (i<2) && !IS_EMPTY(pclFlights[i]); i++ )
	{
		sprintf ( clKey, "%s,%s", pclFlights[i], pcpUtpl );
		dbg ( DEBUG, "GetConaJob: Jobtab-Key <%s> ilType <%d>", clKey, ilType );
		while ( CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
										rgJobArray.crArrayName,
										&(rgJobArray.rrIdxHandle[1]),
										rgJobArray.crIdxName[1], clKey, &llRow,
										(void *)&pclJob ) == RC_SUCCESS )
		{
			ilJobs ++;
			pclStat = JOBFIELD(pclJob,igJobStatIdx);
			pclAcfr	= JOBFIELD(pclJob,igJobAcfrIdx);
			pclActo = JOBFIELD(pclJob,igJobActoIdx);
			dbg ( DEBUG, "GetConaJob: Found job STAT <%s> ACFR <%s> ACTO <%s>", 
				  pclStat, pclAcfr, pclActo );
			switch ( ilType )
			{
				case 1:		/*  Job informed, at least one must be informed */
					if ( ( (strlen(pclStat)>=5) && (pclStat[4]=='1') ) ||
						 (pclStat[0]=='C') || (pclStat[0]=='F')
					   )
					{
						ilSet++;
						if (!clCona[0] )
							strcpy ( clCona, clNow );
					}
					break;
				case 2:		/*  Job confirmed, at least one must be confirmed */
					if ( (pclStat[0]=='C') || (pclStat[0]=='F') )
					{
						ilSet++;
						if (!clCona[0] )
							strcpy ( clCona, clNow );
						if ( !IS_EMPTY(pclAcfr) && (strcmp(clCona,pclAcfr)> 0) )
							strcpy (clCona, pclAcfr ) ;
					}
					break;
				case 3:		/*  Job finished, all must be finished */
					if ( pclStat[0]=='F' )
					{
						ilSet++;
						/*  for actual job, endtime is min(now,Acto) */
						if ( !IS_EMPTY(pclActo) && (strcmp(clNow,pclActo)> 0) )
							strcpy (clEndTime, pclActo ) ;
						else
							strcpy ( clEndTime, clNow );
						/*  last endtime of all jobs is confirmation time */
						if ( strcmp(clEndTime,clCona)> 0 )
							strcpy (clCona, clEndTime ) ;
					}
					break;
			}
			llRow = ARR_NEXT;
		}
		if ( (ilType<=2) && (ilSet>0) )
		{	/* if at least one Job is informed resp. confirmed take first one */
			if ( i == 0)
				strcpy ( pcpCona, clCona );	
			else
				strcpy ( pcpConaTurn, clCona );	
		}
		else
			if ( ilSet>=ilJobs )
			{	/* all jobs are finished take latest one */
				if ( i == 0)
					strcpy ( pcpCona, clCona );	
				else
					strcpy ( pcpConaTurn, clCona );	
			}
	}
	dbg ( TRACE, "GetConaJob: RETY <%s> UAFT <%s> UTPL <%s> %d of %d jobs set => CONA <%s> CONA-Turn <%s>", 
		  pcpRety, pcpUaft, pcpUtpl, ilSet, ilJobs, pcpCona, pcpConaTurn );
	if ( ilJobs > 0 )
		return RC_SUCCESS;
	else
		return RC_NOTFOUND;
}


/*  GetConaCca: get actual status confirmation time for check-in counters	*/
/*	IN:		pcpUaft:	flight URNO, must not be NULL						*/
/*			pcpRety:	type of reference CCAB(open), CCAE(closed)			*/
/*	OUT:	pcpCona:	confirmation time, if existing						*/
static int GetConaCca ( char *pcpUaft, char* pcpCona, char *pcpRety )
{
	char *pclCcaRow, clCona[20] = "";
	long llRow = ARR_FIRST;
	int  ilFldIdx, ilKeyIdx=1;
	int  ilCounters = 0, ilFilled = 0;
	
	if ( !pcpUaft || !pcpCona || !pcpRety )
	{
		dbg ( TRACE, "GetConaCca: At least one parameter is NULL" );
		return RC_INVALID;
	}
	
	pcpCona[0] = '\0';

	if ( !strncmp(pcpRety, "CCAB", 4 ) )
		ilFldIdx = igCcaCkbaIdx;
	else if ( !strncmp(pcpRety, "CCAE", 4 ) )
	{
		ilFldIdx = igCcaCkeaIdx;
		ilKeyIdx=2;
	}
	else
	{
		dbg ( TRACE, "GetConaCca: Unknown status type <%s>", pcpRety );
		return RC_INVALID;
	}
	while ( CEDAArrayFindRowPointer(&(rgCcaArray.rrArrayHandle),
								    rgCcaArray.crArrayName,
								    &(rgCcaArray.rrIdxHandle[ilKeyIdx]),
									rgCcaArray.crIdxName[ilKeyIdx], pcpUaft, 
									&llRow,(void *)&pclCcaRow ) == RC_SUCCESS )
	{
		ilCounters++;
		if ( !IS_EMPTY ( CCAFIELD(pclCcaRow,ilFldIdx) ) )
		{
			ilFilled ++;			
			if ( !clCona[0] )
				strcpy ( clCona, CCAFIELD(pclCcaRow,ilFldIdx) );
		}
		llRow = ARR_NEXT;
	}
	if ( !ilCounters ) 
	{
		dbg ( TRACE, "GetConaCca: No CCA-record found for UAFT <%s>", pcpUaft );
		return RC_NOTFOUND;
	}
	if ( (ilFldIdx==igCcaCkbaIdx) && (ilFilled>0) )
	{	/* if at least one counter open time is filled take first one */
		strcpy ( pcpCona, clCona );	
	}
	if ( (ilFldIdx==igCcaCkeaIdx) && (ilFilled>=ilCounters) )
	{	/* all counter closed time are filled take latest one */
		strcpy ( pcpCona, clCona );	
	}
	dbg ( TRACE, "GetConaCca: RETY <%s> UAFT <%s> %d of %d times filled => CONA <%s>", 
		  pcpRety, pcpUaft, ilFilled, ilCounters, pcpCona  );
	return RC_SUCCESS;
}

/*  HandleCCAUpdate: set OST.CONA for all affected status					*/
/*	IN:		pcpUaft:	flight URNO, must not be NULL						*/
static int HandleCCAUpdate ( char *pcpUaft )
{
	char	clUrno[21], clConaBeg[21], clConaEnd[21], *pclOstPtr;
	long	llOstRow, llFieldNo=-1;
	int		ilRc, ilRc1=RC_SUCCESS;

	if ( !pcpUaft )
	{
		dbg ( TRACE, "HandleCCAUpdate: pcpUaft is NULL" );
		return RC_INVALID;
	}

	ilRc = GetConaCca ( pcpUaft, clConaBeg, "CCAB" );
	ilRc |= GetConaCca ( pcpUaft, clConaEnd, "CCAE" );

	if(ilRc != RC_SUCCESS)
	{
		dbg ( TRACE, "HandleCCAUpdate: GetConaCca failed for flight <%s>", pcpUaft );
	}
	else
	{	/*  search all OST-records for flight with RETY =='CCAB' or 'CCAE' */
		llOstRow = ARR_FIRST;
		while ( CEDAArrayFindRowPointer(&(rgOstArray.rrArrayHandle),
										rgOstArray.crArrayName,
										&(rgOstArray.rrIdxHandle[1]),
										rgOstArray.crIdxName[1],pcpUaft,
										&llOstRow, (void*)&pclOstPtr ) == RC_SUCCESS )
		{
			if ( !strncmp( OSTFIELD(pclOstPtr,igOstRetyIdx), "CCAB", 4 ) )
			{	
				 ilRc1 |= CEDAArrayPutField (&rgOstArray.rrArrayHandle,
											rgOstArray.crArrayName, &llFieldNo, 
											"CONA", llOstRow, clConaBeg );
			}
			if ( !strncmp( OSTFIELD(pclOstPtr,igOstRetyIdx), "CCAE", 4 ) )
			{	
				 ilRc1 |= CEDAArrayPutField (&rgOstArray.rrArrayHandle,
											rgOstArray.crArrayName, &llFieldNo, 
											"CONA", llOstRow, clConaEnd );
			}
			llOstRow = ARR_NEXT;
		}
	}/* end of if */
	if ( ilRc1 != RC_SUCCESS )
	{
		dbg ( TRACE, "HandleCCAUpdate: Update of Field <CONA> failed for flight <%s>", pcpUaft );
		ilRc = RC_FAIL;
	}/* end of if */
	
	return ilRc;
}

/*  GetFieldByUrno: Get content of field of a record from an array			*/
/*	IN:		prpArray:	CEDAArray that contains the record					*/
/*			ipFldIdx:	index of requested field							*/
/*			pcpUrno:	URNO of requested record							*/
/*	OUT:	pcpBuf:		result buffer, if record found and ipFldIdx valid	*/
static int GetFieldByUrno ( ARRAYINFO *prpArray, int ipFldIdx, char *pcpUrno, char *pcpBuf )
{
	long llRow=ARR_FIRST;
	char *pclResult;
	int ilRc;

	if ( !prpArray || !pcpUrno || !pcpBuf )
	{
		dbg ( TRACE, "GetFieldByUrno: At least one parameter is NULL" );
		return RC_INVALID;
	}
	if ( (ipFldIdx > prpArray->lrArrayFieldCnt) || (ipFldIdx<=0) )
	{
		dbg ( TRACE, "GetFieldByUrno: Invalid field index <%d> Array <%s has <%ld> fields",
			  ipFldIdx, prpArray->crArrayName, prpArray->lrArrayFieldCnt );
		return RC_INVALID;
	}
	if ( prpArray->rrIdxHandle[0] < 0 )
	{
		dbg ( TRACE, "GetFieldByUrno: URNO-index not initialised for array <%s>",
			  prpArray->crArrayName );
		return RC_NOTINITIALIZED;
	}
	pcpBuf[0] = '\0';
	ilRc = CEDAArrayFindRowPointer(&(prpArray->rrArrayHandle),
									prpArray->crArrayName,
									&(prpArray->rrIdxHandle[0]),
									prpArray->crIdxName[0],pcpUrno,
									&llRow, (void*)&pclResult );
	if ( ilRc != RC_SUCCESS )
		dbg ( TRACE, "GetFieldByUrno: URNO <%s> not found in array <%s>", 
			  pcpUrno, prpArray->crArrayName );
	else
		strcpy ( pcpBuf, &(pclResult[prpArray->plrArrayFieldOfs[ipFldIdx-1]]) );
	dbg ( DEBUG, "GetFieldByUrno: Array <%s> URNO <%s> FldIdx <%d> Data <%s> RC <%d>",
		  prpArray->crArrayName, pcpUrno, ipFldIdx, pcpBuf, ilRc );	
	return ilRc;
}

/*  HandleJobUpdate: set OST.CONA for all affected status					*/
/*	IN:		pcpUaft:	flight URNO, must not be NULL						*/
/*	   		pcpUtpl:	URNO of job's template, must not be NULL			*/
static int HandleJobUpdate ( char *pcpUaft, char *pcpUtpl )
{
	char	clUrno[21], clConaI[21], clConaC[21], clConaF[21], *pclOstPtr;
	char	clUaft2[11]="", clTurnConaI[21], clTurnConaC[21], clTurnConaF[21];
	long	llOstRow, llFieldNo=-1;
	int		ilRc, ilRc1=RC_SUCCESS;

	if ( !pcpUaft || !pcpUtpl )
	{
		dbg ( TRACE, "HandleJobUpdate: At least one parameter is NULL" );
		return RC_INVALID;
	}
	if ( IS_EMPTY(pcpUtpl) )
	{
		dbg ( TRACE, "HandleJobUpdate: URNO of Job not filled -> nothing to do" );
		return RC_SUCCESS;
	}
	Get2ndFlightUrno ( &rgAftArray, pcpUaft, clUaft2 );
	dbg ( TRACE, "HandleJobUpdate: UAFT <%s> UTPL <%s> UAFT2 <%s>", pcpUaft, pcpUtpl, clUaft2 );
	ilRc = GetConaJob ( pcpUaft, pcpUtpl, clConaI, "JOBI", clUaft2, clTurnConaI );
	ilRc |= GetConaJob ( pcpUaft, pcpUtpl, clConaC, "JOBC", clUaft2, clTurnConaC );
	ilRc |= GetConaJob ( pcpUaft, pcpUtpl, clConaF, "JOBF", clUaft2, clTurnConaF );

	if(ilRc != RC_SUCCESS)
	{
		dbg ( TRACE, "HandleJobUpdate: GetConaJob failed UAFT <%s> UTPL <%s>", 
			  pcpUaft, pcpUtpl );
	}
	else
	{	/*  search all OST-records for flight with RETY =='JOBI' or 'JOBC' or 'JOBF' */
		llOstRow = ARR_FIRST;
		while ( CEDAArrayFindRowPointer(&(rgOstArray.rrArrayHandle),
										rgOstArray.crArrayName,
										&(rgOstArray.rrIdxHandle[2]),
										rgOstArray.crIdxName[2],pcpUaft,
										&llOstRow, (void*)&pclOstPtr ) == RC_SUCCESS )
		{
			if ( !strncmp( OSTFIELD(pclOstPtr,igOstUtplIdx), pcpUtpl, strlen(pcpUtpl) ) )
			{	/*  Job's template fits to template of status */
				dbg ( DEBUG, "HandleJobUpdate: Found status with UTPL <%s> in Row <%ld>", 
					  OSTFIELD(pclOstPtr,igOstUtplIdx), llOstRow );	
				if ( !strncmp( OSTFIELD(pclOstPtr,igOstRetyIdx), "JOBI", 4 ) )
				{
					 ilRc1 = CEDAArrayPutField (&rgOstArray.rrArrayHandle,
												rgOstArray.crArrayName, &llFieldNo, 
												"CONA", llOstRow, clConaI );
					 dbg ( DEBUG, "HandleJobUpdate: Set CONA <%s> for RETY <%s> Row <%ld> RC <%d>",
						   clConaI, OSTFIELD(pclOstPtr,igOstRetyIdx), llOstRow, ilRc );
				}
				if ( !strncmp( OSTFIELD(pclOstPtr,igOstRetyIdx), "JOBC", 4 ) )
				{
					 ilRc1 = CEDAArrayPutField (&rgOstArray.rrArrayHandle,
												rgOstArray.crArrayName, &llFieldNo, 
												"CONA", llOstRow, clConaC );
					 dbg ( DEBUG, "HandleJobUpdate: Set CONA <%s> for RETY <%s> Row <%ld> RC <%d>",
						   clConaC, OSTFIELD(pclOstPtr,igOstRetyIdx), llOstRow, ilRc );
				}
				if ( !strncmp( OSTFIELD(pclOstPtr,igOstRetyIdx), "JOBF", 4 ) )
				{
					 ilRc1 = CEDAArrayPutField (&rgOstArray.rrArrayHandle,
												rgOstArray.crArrayName, &llFieldNo, 
												"CONA", llOstRow, clConaF );
					 dbg ( DEBUG, "HandleJobUpdate: Set CONA <%s> for RETY <%s> Row <%ld> RC <%d>",
						   clConaF, OSTFIELD(pclOstPtr,igOstRetyIdx), llOstRow, ilRc );
				}
				if ( ilRc1 != RC_SUCCESS )
					ilRc = RC_FAIL;
			} /* end of: if template ok */
			llOstRow = ARR_NEXT;
		} /* end of while */
		if ( ilRc != RC_SUCCESS )
			dbg ( TRACE, "HandleJobUpdate: Update of Field <CONA> failed UAFT <%s> UTPL <%s>", 
				  pcpUaft, pcpUtpl );
		/*  Update Status from Turnaround rules where pcpUaft is 2nd Flight */
		if ( !IS_EMPTY (clUaft2) )
		{
			llOstRow = ARR_FIRST;
			while ( CEDAArrayFindRowPointer(&(rgOstArray.rrArrayHandle),
											rgOstArray.crArrayName,
											&(rgOstArray.rrIdxHandle[4]),
											rgOstArray.crIdxName[4],pcpUaft,
											&llOstRow, (void*)&pclOstPtr ) == RC_SUCCESS )
			{
				dbg ( DEBUG, "HandleJobUpdate: 2nd loop found status URNO <%s> with UTPL <%s>", 
							 OSTFIELD(pclOstPtr,igOstUrnoIdx), OSTFIELD(pclOstPtr,igOstUtplIdx) );	
				if ( !strncmp( OSTFIELD(pclOstPtr,igOstUtplIdx), pcpUtpl, strlen(pcpUtpl) ) )
				{	/*  Job's template fits to template of status */
					if ( !strncmp( OSTFIELD(pclOstPtr,igOstRetyIdx), "JOBI", 4 ) )
					{
						 ilRc1 = CEDAArrayPutField (&rgOstArray.rrArrayHandle,
													rgOstArray.crArrayName, &llFieldNo, 
													"CONA", llOstRow, clTurnConaI );
						 dbg ( DEBUG, "HandleJobUpdate: Set CONA(TURN) <%s> for RETY <%s> Row <%ld> RC <%d>",
							   clTurnConaI, OSTFIELD(pclOstPtr,igOstRetyIdx), llOstRow, ilRc );
					}
					if ( !strncmp( OSTFIELD(pclOstPtr,igOstRetyIdx), "JOBC", 4 ) )
					{
						 ilRc1 = CEDAArrayPutField (&rgOstArray.rrArrayHandle,
													rgOstArray.crArrayName, &llFieldNo, 
													"CONA", llOstRow, clTurnConaC );
						 dbg ( DEBUG, "HandleJobUpdate: Set CONA(TURN) <%s> for RETY <%s> Row <%ld> RC <%d>",
							   clTurnConaC, OSTFIELD(pclOstPtr,igOstRetyIdx), llOstRow, ilRc );
					}
					if ( !strncmp( OSTFIELD(pclOstPtr,igOstRetyIdx), "JOBF", 4 ) )
					{
						 ilRc1 = CEDAArrayPutField (&rgOstArray.rrArrayHandle,
													rgOstArray.crArrayName, &llFieldNo, 
													"CONA", llOstRow, clTurnConaF );
						 dbg ( DEBUG, "HandleJobUpdate: Set CONA(TURN) <%s> for RETY <%s> Row <%ld> RC <%d>",
							   clTurnConaF, OSTFIELD(pclOstPtr,igOstRetyIdx), llOstRow, ilRc );
					}
					if ( ilRc1 != RC_SUCCESS )
						ilRc = RC_FAIL;
				} /* end of: if template ok */
				llOstRow = ARR_NEXT;
			} /* end of while */
		}
	}/* end of if */
	
	return ilRc;
}


/*  IsAftChangeImportant: Determine what has to be done on UFR-event		*/
/*	IN:		pcpFields:	field-list from event								*/
/*	   		pcpData:	data-list from event								*/
/*	OUT: 	pbpDoEval:	set to TRUE, if evaluation of rules is necessary	*/
/*	   		pbpSetCons:	set to TRUE, if status (OST.CONS) have to be moeved	*/
/*	   		pbpSetCona:	set to TRUE, if actual confirmation has to be set	*/
static int IsAftChangeImportant ( char *pcpFields, char *pcpData, BOOL *pbpDoEval, 
								  BOOL  *pbpSetCons, BOOL  *pbpSetCona )
{
	int		ilNoOfItems, ilIdx, ilLoop, ilCol,ilPos, ilRc; 
	char	clUaft[21], *pclRow=0;
        char    clAdid[5];  /* MSI: 20080924: Added   */ 
	long	llFunc = ARR_FIRST;
	char	clFina[11], clNew[81], clOld[81];
	/* char    clEvalFields[]="ADID,ALC2,ALC3,TTYP,ORG3,DES3,ACT3,ACT5,TIFA,TIFD,RKEY"; */ 
	/* char    clEvalFields[]="ADID,ALC2,ALC3,TTYP,ORG3,DES3,ACT3,ACT5,RKEY,ETAI,ETDI"; */  /* MSI: 20080618: Added  */ 
        char    clEvalArrFields[]="ADID,ALC2,ALC3,TTYP,ORG3,ACT3,ACT5,RKEY,ETAI"; /* MSI: 20080924: Added   */ 
        char    clEvalDepFields[]="ADID,ALC2,ALC3,TTYP,DES3,ACT3,ACT5,RKEY,ETDI"; /* MSI: 20080924: Added   */ 
 
        /* char    clConsFields[]="STOA,ETAI,LAND,ONBE,ONBL,TIFA,STOD,ETDI,OFBL,AIRB,TIFD,TMOA,GA1B,GD1B,GA1E,GD1E,B1BS,B1ES"; */ 
        char    clConsArrFields[]="STOA,ETAI,LAND,ONBE,ONBL,TIFA,TMOA,GA1B,GA1E,B1BS,B1ES"; /* MSI: 20080924: Added */ 
        char    clConsDepFields[]="STOD,ETDI,OFBL,AIRB,TIFD,GD1B,GD1E"; /* MSI: 20080924: Added */ 
        
        /* char    clConaFields[]="GA1X,GD1X,GA1Y,GD1Y,B1BA,B1EA,AIRB,ONBL,OFBL,FLTO,FLTC,BOAO,FCAL";   */ 
        char    clConaArrFields[]="GA1X,GA1Y,B1BA,B1EA,ONBL";  /* MSI: 20080924: Added    */ 
        char    clConaDepFields[]="GD1X,GD1Y,AIRB,OFBL,FLTO,FLTC,BOAO,FCAL";  /* MSI: 20080924: Added   */ 

        BOOL	blGoon ;
	
	*pbpDoEval = *pbpSetCons = *pbpSetCona = FALSE;

        /* MSI: 20080924: Added the section below: To solve the problem */    
        /*            created by the updates coming from MVT of DEP Flt */
        /* Problem: Set's CONA to empty whenever MVT for DEP Flt is     */    
        /*          received and ETA at DEST updated into AFTTAB fields */  
        /* Solution: IGNORE MVT updates related to DESTINATION station  */ 
        /*           Check and Differenciate between ARR/DEP Flights    */     

        ilRc = FindItemInList(pcpFields,"ADID",',',&ilIdx,&ilCol,&ilPos);   
        if(ilRc != RC_SUCCESS)  
                dbg(TRACE, "IsAftChangeImportant: ADID not in FieldList");    
        else   
        {    
             if(get_real_item(clAdid,pcpData,ilIdx) <= 0)   
             {    
                  ilRc = RC_FAIL;   
                  dbg(TRACE, "IsAftChangeImportant: ADID not filled");   
             }    
        }     
 
        /* MSI: END!!!!! */  
        if(ilRc == RC_SUCCESS)   
        {   /* MSI: 20080924: Added above two lines */    
	ilRc = FindItemInList(pcpFields,"URNO",',',&ilIdx,&ilCol,&ilPos);
	if ( ilRc != RC_SUCCESS )
		dbg ( TRACE, "IsAftChangeImportant: URNO not in Fieldlist" );
	else
	{
		if ( get_real_item(clUaft,pcpData,ilIdx)<=0 )
		{
			ilRc = RC_FAIL;
			dbg ( TRACE, "IsAftChangeImportant: URNO not filled" );
		}
	}
        } /* END-IF : MSI: 20080924   */ 
	
        if ( ilRc == RC_SUCCESS )
	{
		ilRc = CEDAArrayFindRowPointer (&(rgAftArray.rrArrayHandle),
										rgAftArray.crArrayName,
										&(rgAftArray.rrIdxHandle[0]),
										rgAftArray.crIdxName[0],
										clUaft ,&llFunc, (void *) &pclRow );
		if ( ilRc != RC_SUCCESS )
		{
			dbg ( TRACE, "IsAftChangeImportant: Flight URNO <%s> not found", clUaft );
		}
	}
	if ( ilRc == RC_SUCCESS )
	{
		ilNoOfItems = get_no_of_items(pcpFields);
		ilLoop = 1;
		blGoon = TRUE; 
		while( blGoon && (ilLoop <= ilNoOfItems) )
		{
			get_real_item(clFina,pcpFields,ilLoop);
			get_real_item(clNew,pcpData,ilLoop);
			if ( FindItemInList(AFT_FIELDS,clFina,',',&ilIdx,&ilCol,&ilPos) == RC_SUCCESS )
			{
				strcpy ( clOld, AFTFIELD(pclRow,ilIdx) );
				TrimRight ( clOld );
				if ( !strcmp ( clOld," " ) )
					clOld[0] = '\0';
				TrimRight ( clNew );
				if ( !strcmp ( clNew," " ) )
					clNew[0] = '\0';
				if ( strcmp ( clNew, clOld ) )  /* significant change */
				{
					dbg ( TRACE, "IsAftChangeImportant: Field <%s> Old <%s> New <%s> are different",
						  clFina, clOld, clNew );
	                                /* MSI: 20080924: ORIGINAL    	
                         		if ( strstr ( clEvalFields, clFina ) )
						*pbpDoEval = TRUE;
					if ( strstr ( clConsFields, clFina ) )
						*pbpSetCons = TRUE;
					if ( strstr ( clConaFields, clFina ) )
						*pbpSetCona = TRUE;
					if ( strstr ( clFina, "RKEY" ) )
						sprintf ( cgChangedRkeys, "Where rkey in ('%s','%s')", clOld, clNew );
                                        /* MSI: 20080924: END!!!! */   

                                        /* MSI: 20080924: Added  */  
                                        if(!strcmp(clAdid, "A"))   
                                        {    
                         		  if ( strstr ( clEvalArrFields, clFina ) )
						*pbpDoEval = TRUE;
					  if ( strstr ( clConsArrFields, clFina ) )
						*pbpSetCons = TRUE;
					  if ( strstr ( clConaArrFields, clFina ) )
						*pbpSetCona = TRUE;
					  if ( strstr ( clFina, "RKEY" ) )
						sprintf ( cgChangedRkeys, "Where rkey in ('%s','%s')", clOld, clNew );
                                        }    
                  
                                        if(!strcmp(clAdid, "D"))   
                                        {                            
                         		  if ( strstr ( clEvalDepFields, clFina ) )
						*pbpDoEval = TRUE;
					  if ( strstr ( clConsDepFields, clFina ) )
						*pbpSetCons = TRUE;
					  if ( strstr ( clConaDepFields, clFina ) )
						*pbpSetCona = TRUE;
					  if ( strstr ( clFina, "RKEY" ) )
						sprintf ( cgChangedRkeys, "Where rkey in ('%s','%s')", clOld, clNew );
			                }    
                                        /* MSI: 20080924: END!!!! */  	
                                }
				else
					dbg ( DEBUG, "IsAftChangeImportant: Field <%s> Old <%s> New <%s> are equal", 
						  clFina, clOld, clNew );
		
			}
			blGoon = (!*pbpDoEval || !*pbpSetCons || !*pbpSetCona );
			ilLoop++;
		}
	}
	dbg ( DEBUG, "IsAftChangeImportant: DoEval <%d>  SetCons <%d>  SetCona <%d>", 
				 *pbpDoEval, *pbpSetCons, *pbpSetCona );
	return ilRc;
}

/*  SetConaForFlight: Set actual confirmation for all status of type "AFT"	*/
/*	IN:		pcpUaft:	flight URNO, can be NULL for all flights			*/
/*			pcpUhss:	URNO of handling status sect., can be NULL for all  */
static int SetConaForFlight ( char *pcpUaft, char *pcpUhss )
{
	char clOstKey[31]="", *pclOstPtr, clCona[21];
	long llOstRow;
	long llTifr, llFieldNo=-1;
	int ilRc1, ilRc = RC_SUCCESS;
	char *pclUost, *pclRety, *pclUaft, *pclAftf;

	if ( pcpUaft && pcpUaft[0] )
	{
		strcpy ( clOstKey, pcpUaft );
		if ( pcpUhss && pcpUhss[0] )
		{
			strcat ( clOstKey, "," );
			strcat ( clOstKey, pcpUhss );
		}	
	}

	dbg ( TRACE, "SetConaForFlight: key for Status to update <%s>", clOstKey );

	llOstRow = ARR_FIRST;
	while ( CEDAArrayFindRowPointer(&(rgOstArray.rrArrayHandle),
								    rgOstArray.crArrayName,
									&(rgOstArray.rrIdxHandle[2]),
									rgOstArray.crIdxName[2],clOstKey,
									&llOstRow, (void*)&pclOstPtr ) == RC_SUCCESS )
	{
		pclUost = OSTFIELD(pclOstPtr,igOstUrnoIdx);
		pclRety = OSTFIELD(pclOstPtr,igOstRetyIdx);
		pclUaft = OSTFIELD(pclOstPtr,igOstUaftIdx);
		pclAftf = OSTFIELD(pclOstPtr,igOstAftfIdx);
		if ( !strncmp( pclRety, "AFT", 3 ) )
		{	
			dbg ( TRACE, "SetConaForFlight: Processing UOST <%s> RETY <%s> RURN <%s> AFTF <%s>",
				  pclUost, pclRety, pclUaft, pclAftf );
			ilRc1 = GetConaFlight ( pclUaft, pclAftf, clCona );
			if ( ilRc1 == RC_SUCCESS )
				ilRc1 = CEDAArrayPutField (&rgOstArray.rrArrayHandle,
										   rgOstArray.crArrayName, &llFieldNo, 
										   "CONA", llOstRow, clCona );
			if ( ilRc1 |= RC_SUCCESS )
			{
				dbg ( TRACE, "SetConaForFlight: Update of Field <CONA> failed URNO <%s> UAFT <%s>", 
					   OSTFIELD(pclOstPtr,igOstUrnoIdx), OSTFIELD(pclOstPtr,igOstUaftIdx) );
				ilRc = RC_FAIL;
			}
		}
		else
			dbg ( DEBUG, "SetConaForFlight: Skipping UOST <%s> RETY <%s> RURN <%s> AFTF <%s>",
				  pclUost, pclRety, pclUaft, pclAftf );
		
		llOstRow = ARR_NEXT;
	}
	return ilRc;
}	

/*  SetInitialCONA: Set actual confirmation for all status (after creation)	*/
/*	IN:		pcpUaft:	flight URNO, can be NULL for all flights			*/
/*			pcpUhss:	URNO of handling status sect., can be NULL for all  */
static int SetInitialCONA ( char *pcpUaft, char *pcpUhss )
{
	char clOstKey[31]="", *pclOstPtr ;
	long llOstRow;
	long llTifr, llFieldNo=-1;
	int ilRc1, ilRc = RC_SUCCESS;
	char clUtplList[500]="", *pclUtpl, *pclOstUaft, *pclRety, *pclOstRurn, *pclTskn ;
	BOOL blCcaStatDone = FALSE;
	BOOL blAftStatDone = FALSE;

	if ( pcpUaft && pcpUaft[0] )
	{
		strcpy ( clOstKey, pcpUaft );
		if ( pcpUhss && pcpUhss[0] )
		{
			strcat ( clOstKey, "," );
			strcat ( clOstKey, pcpUhss );
		}
	}

	dbg ( DEBUG, "SetInitialCONA: key for Status to update <%s>", clOstKey );

	llOstRow = ARR_FIRST;
	while ( CEDAArrayFindRowPointer(&(rgOstArray.rrArrayHandle),
								    rgOstArray.crArrayName,
									&(rgOstArray.rrIdxHandle[1]),
									rgOstArray.crIdxName[1],clOstKey,
									&llOstRow, (void*)&pclOstPtr ) == RC_SUCCESS )
	{
		pclRety = OSTFIELD(pclOstPtr,igOstRetyIdx);
		pclOstUaft = OSTFIELD(pclOstPtr,igOstUaftIdx);
		pclOstRurn = OSTFIELD(pclOstPtr,igOstRurnIdx);

		if ( !blCcaStatDone && !strncmp( pclRety, "CCA", 3 ) )
		{	/*  we found the first status type CCAB or CCAE */
			ilRc1 = HandleCCAUpdate ( pclOstUaft );
			/* if only one flight is handled, HandleCCAUpdate won't be called again */
			if ( pcpUaft )  
				blCcaStatDone = TRUE;
		}
		else if ( !blAftStatDone && !strncmp( pclRety, "AFT", 3 ) )
		{	/*  we found the first status type CCAB or CCAE */
			ilRc1 = SetConaForFlight ( pclOstUaft, pcpUhss );
			/* if only one flight is handled, HandleCCAUpdate won't be called again */
			if ( pcpUaft )  
				blAftStatDone  = TRUE;
		}
		else if ( !strncmp( pclRety, "JOB", 3 ) )
		{
			pclUtpl = OSTFIELD(pclOstPtr,igOstUtplIdx) ; 
			if ( !IS_EMPTY(pclUtpl) && !strstr( clUtplList, pclUtpl ) )
			{
				ilRc1 = HandleJobUpdate ( pclOstUaft, pclUtpl );
				if ( pcpUaft )  
				{
					strcat ( clUtplList, pclUtpl );
					strcat ( clUtplList, "," );
				}
			}
		}
		else if ( !strncmp( pclRety, "OTH", 3 ) )   
		{
			pclTskn = OSTFIELD(pclOstPtr,igOstTsknIdx) ; 
			if ( !IS_EMPTY(pclTskn) )
			{
				ilRc1 = HandleRsmUpdate ( pclOstUaft, pclTskn, "" );
			}
		}
		if ( ilRc1 |= RC_SUCCESS )     
		{
			dbg ( TRACE, "SetInitialCONA: Update of Field <CONA> failed URNO <%s> UAFT <%s>, RETY <%s>", 
				   OSTFIELD(pclOstPtr,igOstUrnoIdx), pclOstUaft, pclRety );
			ilRc = RC_FAIL;
		}
		llOstRow = ARR_NEXT;
	}
	return ilRc;
}	


/******************************************************************************/
static BOOL IsRuleValid(char *pcpUrno, char *pcpUTCDate )
{
	int		ilRC = RC_SUCCESS, ilValid=0;			/* Return code */
	char    clDate[24]; 
	BOOL	blReturn = FALSE;

	strcpy ( clDate, pcpUTCDate );
	UtcToLocal( clDate );

	ilRC = CheckValidity("STATUS", pcpUrno, clDate, &ilValid);
	if (ilRC != RC_SUCCESS)
	{
		dbg (DEBUG, "IsRuleValid: CheckValidity failed <%d>", ilRC);
	}
	else
	{
		dbg(DEBUG, "IsRuleValid: <%s> is valid at <%s>", pcpUrno, pcpUTCDate);
		blReturn = TRUE;
	} /* end of if */

	return blReturn;
	
} /* end of IsRuleValid */

static int UtcToLocal(char *pcpTime)
{
	int ilRc = RC_SUCCESS;
	char *pclTdi;
	char clUtc[32];

	strcpy(clUtc,pcpTime);
	if ( cgTich[0] )
	{
		pclTdi = (strcmp(pcpTime,cgTich) < 0) ? cgTdi1 : cgTdi2;
		AddSecondsToCEDATime(pcpTime,(time_t)0+atol(pclTdi),1);
		dbg(DEBUG,"UtcToLocal: <%s> -> <%s> ", clUtc, pcpTime);
	}
	return ilRc;
}


static int InitTichTdi()
{
	/** read TICH,TDI1,TDI2 ***********/
	char pclDataArea[2256];
	char pclSqlBuf[256];
	short slCursor = 0;
	int   ilRc=RC_SUCCESS;

	sprintf(pclSqlBuf,
		"SELECT TICH,TDI1,TDI2 FROM APTTAB WHERE APC3 = '%s'",cgHopo);
	ilRc = sql_if(START,&slCursor,pclSqlBuf,pclDataArea);
	if (ilRc != DB_SUCCESS)
	{
		dbg(TRACE,"Home Airport <%s> not found in APTTAB %d",cgHopo,ilRc);
	} /* end while */
	else
	{
		strcpy(cgTdi1,"60");
		strcpy(cgTdi2,"60");
		get_fld(pclDataArea,0,STR,14,cgTich);
		get_fld(pclDataArea,1,STR,14,cgTdi1);
		get_fld(pclDataArea,2,STR,14,cgTdi2);
		TrimRight(cgTich);
		if (*cgTich != '\0')
		{
			TrimRight(cgTdi1);
			TrimRight(cgTdi2);
			sprintf(cgTdi1,"%d",atoi(cgTdi1)*60);
			sprintf(cgTdi2,"%d",atoi(cgTdi2)*60);
		}
		dbg(TRACE,"Tich: <%s> Tdi1: <%s> Tdi2 <%s>",
			cgTich,cgTdi1,cgTdi2);
	}
	close_my_cursor(&slCursor);
	slCursor = 0;
	return ilRc;
}

static int PrepareSBC ( char *pcpUaft, ARRAYINFO *prpAftArray )
{
	int ilRc = RC_SUCCESS;
	long llFlights=0, llRow;
	HANDLE slIdxHdl;
	char *pclRow, *pclRow2, clUrno[12];
	char clIdxNam[11]= "SDAYIDX";
	char clValue[124]="";

	cgBcField[0] = cgBcData[0] = '\0';

	if ( debug_level > TRACE )
	{		
		if(ReadConfigEntry("MAIN","BC_TYPE",clValue) == RC_SUCCESS)
		{
			bgSingleBC = ( strstr ( clValue, "SINGLE" ) ) ? TRUE : FALSE;
			bgSbcUaft = ( strstr ( clValue, "SBCUAFT" ) ) ? TRUE : FALSE;
			bgSbcSday = ( strstr ( clValue, "SBCSDAY" ) ) ? TRUE : FALSE;
		}
		else
			bgSingleBC = bgSbcUaft = bgSbcSday = TRUE;
	}
	if ( pcpUaft && (igMinUaftInSbc<=1) )
	{
		if ( bgSbcUaft )
		{
			strcpy ( cgBcData, pcpUaft );
			strcpy ( cgBcField, "UAFT" );
		}
		else
			ilRc = RC_IGNORE;
	}
	else if ( prpAftArray )
	{
		CEDAArrayGetRowCount(&(prpAftArray->rrArrayHandle),
							 prpAftArray->crArrayName, &llFlights );
		if ( llFlights <= 0 ) 
			return RC_NODATA;
		if ( llFlights < igMinUaftInSbc )
			ilRc = RC_IGNORE;
		else if ( llFlights <= igMaxUaftInSbc ) 
		{
			if ( bgSbcUaft )
			{
				strcpy ( cgBcField, "UAFT" );
				llRow = ARR_FIRST;
				while ( CEDAArrayGetRowPointer(&(prpAftArray->rrArrayHandle),
												prpAftArray->crArrayName,llRow,
												(void *)&pclRow) == RC_SUCCESS )

				{							
					strcpy ( clUrno, AFTFIELD(pclRow,igAftUrnoIdx) );
					TrimRight (  clUrno );
					if ( cgBcData[0] )
						strcat ( cgBcData, "," );
					strcat ( cgBcData, clUrno );
					llRow = ARR_NEXT;
				}
			}
			else
				ilRc = RC_IGNORE;
		}
		else
		{
			if ( bgSbcSday )
			{
				ilRc = CEDAArrayCreateSimpleIndexUp(&(rgNewOstArr.rrArrayHandle),
													rgNewOstArr.crArrayName,
													&slIdxHdl, clIdxNam, "SDAY" );
				if ( ilRc == RC_SUCCESS )
				{
					llRow = ARR_FIRST;
					ilRc = CEDAArrayFindRowPointer( &(rgNewOstArr.rrArrayHandle),
													 rgNewOstArr.crArrayName, 
													 &slIdxHdl, clIdxNam, "", 
													 &llRow, (void *)&pclRow);
					llRow = ARR_LAST;
					ilRc |= CEDAArrayFindRowPointer( &(rgNewOstArr.rrArrayHandle),
													 rgNewOstArr.crArrayName,
													 &slIdxHdl, clIdxNam, "", 
													 &llRow, (void *)&pclRow2);
					if ( ilRc == RC_SUCCESS )
					{
						strcpy ( cgBcField, "SDAY" );
						sprintf ( cgBcData, "%s-%s", OSTFIELD (pclRow, igOstSdayIdx ), 
								  OSTFIELD (pclRow2, igOstSdayIdx ) );
					}
					if ( CEDAArrayDestroyIndex( &(rgNewOstArr.rrArrayHandle),
												rgNewOstArr.crArrayName, &slIdxHdl,
												clIdxNam ) != RC_SUCCESS )
					{
						dbg ( TRACE, "PrepareSBC: CEDAArrayDestroyIndex failed IdxHdl <%d> IdxNam <%s>",
							  slIdxHdl, clIdxNam );
					}
				}
			}
			else
				ilRc = RC_IGNORE;
		}
	}
	if ( pcpUaft )
		dbg ( DEBUG, "PrepareSBC: UAFT <%s> BC-Field <%s> BC-Data <%s> RC <%d>", 
			  pcpUaft, cgBcField, cgBcData, ilRc );		
	else
		dbg ( DEBUG, "PrepareSBC: #Flights <%ld> BC-Field <%s> BC-Data <%s> RC <%d>", 
			  llFlights, cgBcField, cgBcData, ilRc );		
	return ilRc;
}

/*  Store earliest and last flight time for later use (reloading of OSTTAB */
static int UpdateMinMax ( char *pcpTime )
{
	if ( !pcpTime )
		return RC_INVALID;

	if ( !cgMinAftTime[0] || ( strcmp ( pcpTime, cgMinAftTime ) < 0 ) )
		strcpy ( cgMinAftTime, pcpTime );
	if ( strcmp(cgMaxAftTime, pcpTime ) < 0 )
		strcpy ( cgMaxAftTime, pcpTime );
	return RC_SUCCESS;
}

static int GetNomField ( char *pcpActField, char *pcpNomField, char *pcpRety )
{
	int ilIdx, ilCol, ilPos;
	int ilRc = RC_INVALID;
	
	if ( pcpActField && pcpNomField )
	{
		pcpNomField[0] = '\0';
		if ( !strncmp( pcpRety, "AFT", 3 ) )
		{
			ilRc = FindItemInList( ACT_FIELDS, pcpActField, ',', &ilIdx, &ilCol,&ilPos);
			if ( ilRc == RC_SUCCESS )
				get_real_item(pcpNomField,NOM_FIELDS,ilIdx);
		}
		else
		{
			if ( !strncmp ( pcpRety, "JOBC", 4 ) )
				strcpy ( pcpNomField, "PLFR" );
			else if ( !strncmp ( pcpRety, "JOBF", 4 ) )
					strcpy ( pcpNomField, "PLTO" );
			else if ( !strncmp ( pcpRety, "CCAB", 4 ) )
					strcpy ( pcpNomField, "CKBS" );
			else if ( !strncmp ( pcpRety, "CCAE", 4 ) )
					strcpy ( pcpNomField, "CKES" );
		}
		if ( pcpNomField[0] )
			ilRc = RC_SUCCESS ;
		dbg ( DEBUG, "GetNomField ACT <%s> NOM <%s> RC <%d>", pcpActField, pcpNomField, ilRc );
	}
	return ilRc;
}

/*  GetConsJob: get actual status confirmation time for jobs	 			*/
/*	IN:		pcpUaft:	flight URNO, must not be NULL						*/
/*			pcpUtpl:	template  of jobs that are used for the status		*/
/*			pcpRety:	type of reference JOBI(informed), JOBC(confirmed),	*/
/*						JOBF(finished)										*/
/*	OUT:	pcpCons:	scheduled confirmation time, if existing			*/
static int GetConsJob ( char *pcpUaft, char *pcpUaft2, char *pcpUtpl, char* pcpCons, char *pcpRety )
{
	char *pclJob, *pclPlto, *pclPlfr, *pclFlights[2];
	long llRow = ARR_FIRST;
	int  i, ilType=0, ilJobs=0, ilRc = RC_SUCCESS;
	char clKey[31] ;
	
	if ( !pcpUaft || !pcpCons || !pcpRety || !pcpUtpl || IS_EMPTY(pcpUtpl) )
	{
		dbg ( TRACE, "GetConsJob: At least one parameter is NULL" );
		return RC_INVALID;
	}

	pcpCons[0] = '\0';
	if ( !strncmp(pcpRety, "JOBC", 4 ) )
	{
		ilType = 2;
	}
	else if ( !strncmp(pcpRety, "JOBF", 4 ) )
	{
		ilType = 3;
	}
	else
	{
		dbg ( TRACE, "GetConsJob: Unhandled status type <%s>", pcpRety );
		return RC_INVALID;
	}
	pclFlights[0] = pcpUaft;
	pclFlights[1] = pcpUaft2;
	for ( i=0; (i<2) && !IS_EMPTY(pclFlights[i]); i++ )
	{
		llRow = ARR_FIRST;
		sprintf ( clKey, "%s,%s", pclFlights[i], pcpUtpl );
		dbg ( DEBUG, "GetConsJob: Jobtab-Key <%s> ilType <%d>", clKey, ilType );
		while ( CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
										rgJobArray.crArrayName,
										&(rgJobArray.rrIdxHandle[1]),
										rgJobArray.crIdxName[1], clKey, &llRow,
										(void *)&pclJob ) == RC_SUCCESS )
		{
			ilJobs ++;
			pclPlfr	= JOBFIELD(pclJob,igJobPlfrIdx);
			pclPlto = JOBFIELD(pclJob,igJobPltoIdx);
			dbg ( DEBUG, "GetConsJob: Found job RETY <%s> PLFR <%s> PLTO <%s>", 
				  pcpRety, pclPlfr, pclPlto );
			switch ( ilType )
			{
				case 2:		/*  Job confirmed, at least one must be confirmed */
					if ( IS_EMPTY(pcpCons) || (strcmp(pcpCons,pclPlfr)> 0) )
						strcpy (pcpCons, pclPlfr ) ;
					break;
				case 3:		/*  Job finished, all must be finished */
					if ( IS_EMPTY(pcpCons) || (strcmp(pcpCons,pclPlto)< 0) )
						strcpy (pcpCons, pclPlto ) ;
					break;
			}
			llRow = ARR_NEXT;
		}
	}
	dbg ( TRACE, "GetConsJob: RETY <%s> UAFT <%s> FLT2 <%s> UTPL <%s> <%d> jobs found => CONS <%s>", 
		  pcpRety, pcpUaft, pcpUaft2, pcpUtpl, ilJobs, pcpCons  );
	if ( !ilJobs )
		ilRc = RC_NOTFOUND;
	return ilRc;
}


/*  GetConsCca: get scheduled status time for check-in counters				*/
/*	IN:		pcpUaft:	flight URNO, must not be NULL						*/
/*			pcpRety:	type of reference CCAB(open), CCAE(closed)			*/
/*	OUT:	pcpCons:	confirmation time, if existing						*/
static int GetConsCca ( char *pcpUaft, char* pcpCons, char *pcpRety )
{
	char *pclCcaRow, *pclVal;
	long llRow = ARR_FIRST;
	int  ilFldIdx, ilKeyIdx=1;
	int  ilCounters = 0;
	
	if ( !pcpUaft || !pcpCons || !pcpRety )
	{
		dbg ( TRACE, "GetConsCca: At least one parameter is NULL" );
		return RC_INVALID;
	}
	
	pcpCons[0] = '\0';

	if ( !strncmp(pcpRety, "CCAB", 4 ) )
		ilFldIdx = igCcaCkbsIdx;
	else if ( !strncmp(pcpRety, "CCAE", 4 ) )
	{
		ilFldIdx = igCcaCkesIdx;
		ilKeyIdx=2;
	}
	else
	{
		dbg ( TRACE, "GetConaCca: Unknown status type <%s>", pcpRety );
		return RC_INVALID;
	}
	while ( CEDAArrayFindRowPointer(&(rgCcaArray.rrArrayHandle),
								    rgCcaArray.crArrayName,
								    &(rgCcaArray.rrIdxHandle[ilKeyIdx]),
									rgCcaArray.crIdxName[ilKeyIdx], pcpUaft, 
									&llRow,(void *)&pclCcaRow ) == RC_SUCCESS )
	{
		ilCounters++;
		pclVal = CCAFIELD(pclCcaRow,ilFldIdx);
		if ( !IS_EMPTY ( pclVal ) )
		{
			switch ( ilKeyIdx )
			{
				case 1:
					if ( IS_EMPTY(pcpCons) || (strcmp(pcpCons,pclVal)> 0) )
						strcpy (pcpCons, pclVal ) ;
					break;
				case 2:
					if ( IS_EMPTY(pcpCons) || (strcmp(pcpCons,pclVal)< 0) )
						strcpy (pcpCons, pclVal ) ;
					break;
			}
		}
		llRow = ARR_NEXT;
	}
	if ( !ilCounters ) 
	{
		dbg ( TRACE, "GetConsCca: No CCA-record found for UAFT <%s>", pcpUaft );
		return RC_NOTFOUND;
	}
	dbg ( TRACE, "GetConsCca: RETY <%s> UAFT <%s> %d records found => CONS <%s>", 
		  pcpRety, pcpUaft, ilCounters, pcpCons  );
	return RC_SUCCESS;
}

static int UpdateStatus ( long lpOldRow, char *pcpOstOld, char *pcpOstNew )
{
	char *clCmpFlds[] = { "RTAB","RURN","RFLD",NULL };
	int i=0, ilIdx, ilPos, ilCol, ilRc = RC_SUCCESS, ilRc1;
	char *pclOld, *pclNew;
	long llFieldNo;
	char clBuffer[101];

	clBuffer[100] = '\0';

	while ( clCmpFlds[i] )
	{
		FindItemInList( OST_FIELDS, clCmpFlds[i], ',', &ilIdx, &ilCol,&ilPos);
		if ( ilIdx > 0 )
		{
			pclOld = OSTFIELD (pcpOstOld,ilIdx );
			pclNew = OSTFIELD (pcpOstNew,ilIdx );
			if ( strcmp ( pclOld, pclNew ) )
			{
				if ( debug_level > TRACE )
					strncpy ( clBuffer, pclOld, 100 ); /* save old value for dbg */
				llFieldNo = -1;
				ilRc1 = CEDAArrayPutField (&rgOstArray.rrArrayHandle,
										   rgOstArray.crArrayName, &llFieldNo, 
										   clCmpFlds[i], lpOldRow, pclNew );
				if ( ilRc1 != RC_SUCCESS )
				{
					ilRc = RC_FAIL;
					dbg ( TRACE, "UpdateStatus: CEDAArrayPutField FINA <%s> VALU <%s> Row <%ld> failed RC <%d>", 
						  clCmpFlds[i], pclNew, lpOldRow, ilRc1 );
				}		
				else
					dbg ( DEBUG, "UpdateStatus: updated field <%s> Old <%s> new <%s> successfully", 
						  clCmpFlds[i], clBuffer, pclNew );

			}
		}
		i++;
	}	
	return ilRc ;
}					
				

static int Find2ndFlight ( ARRAYINFO *prpFlightArr, char *pcpAftRow1, char **pcpAftRow2 )
{
	char clKey[21];
	long llRow=ARR_FIRST;
	char *pclAdid;
	int ilRc;

	pclAdid = AFTFIELD(pcpAftRow1,igAftAdidIdx );
	if ( *pclAdid == 'A' )
		sprintf ( clKey, "%s,D", AFTFIELD(pcpAftRow1,igAftRkeyIdx ) );
	else
		sprintf ( clKey, "%s,A", AFTFIELD(pcpAftRow1,igAftRkeyIdx ) );
	ilRc = CEDAArrayFindRowPointer (&(prpFlightArr->rrArrayHandle),
									prpFlightArr->crArrayName,
									&(prpFlightArr->rrIdxHandle[1]),
									prpFlightArr->crIdxName[1],
									clKey ,&llRow, (void *) pcpAftRow2 );
	if ( ilRc == RC_SUCCESS )
		dbg ( DEBUG, "Find2ndFlight: Found UAFT2 <%s> using key <%s>", 
			  AFTFIELD(*pcpAftRow2,igAftUrnoIdx), clKey );
	else
	{
		dbg ( DEBUG, "Find2ndFlight: Didn't find 2nd Flight using key <%s> RC <%d>", clKey, ilRc );
		*pcpAftRow2 = 0;
	}
	return ilRc;
}

static BOOL CheckOneRule ( char *pcpRule, char *pcpInbound, char *pcpOutbound, long lpActGroundTime )
{
	char *pclTime, *pclUsrh, *pclSrhAdid, *pclMaxt;
	BOOL blRet = TRUE;
	int i;
	long llMaxt;
	
	pclUsrh = SRHFIELD(pcpRule,igSrhUrnoIdx );
	pclSrhAdid = SRHFIELD(pcpRule,igSrhAdidIdx );
	pclMaxt = SRHFIELD(pcpRule,igSrhMaxtIdx );

	if ( !pcpInbound && (*pclSrhAdid != 'D') ) 
	{
		dbg ( DEBUG, "CheckOneRule: Event type mismatch, Single outbound vs. SRH.ADID <%s>", pclSrhAdid );
		return FALSE;
	}
	if ( !pcpOutbound && (*pclSrhAdid != 'A') ) 
	{
		dbg ( DEBUG, "CheckOneRule: Event type mismatch, Single inbound vs. SRH.ADID <%s>", pclSrhAdid );
		return FALSE;
	}
	if ( pcpInbound )
	{
		pclTime = AFTFIELD(pcpInbound,igAftTifaIdx);
		if ( !IsRuleValid( pclUsrh, pclTime ) ) 
		{
			dbg ( TRACE, "CheckOneRule: Rule USRH <%s> is not valid for inbound TIFA <%s>", pclUsrh, pclTime );/* AM 20071231 */ 
			return FALSE;
		}
	}
	if ( pcpOutbound )
	{
		pclTime = AFTFIELD(pcpOutbound,igAftTifdIdx);
		if ( !IsRuleValid( pclUsrh, pclTime ) ) 
		{
			dbg ( TRACE, "CheckOneRule: Rule USRH <%s> is not valid for outbound TIFD <%s>", pclUsrh, pclTime );/* AM 20071231 */ 
			dbg ( DEBUG, "CheckOneRule: Rule USRH <%s> is not valid for outbound TIFD <%s>", pclUsrh, pclTime );
			return FALSE;
		}
	}
	/* Check MAXT */
	if (  (*pclSrhAdid == 'T') && pcpInbound && pcpOutbound )
	{
		llMaxt = atol(pclMaxt) * 60;
		if ( lpActGroundTime > llMaxt )
		{
			dbg ( TRACE, "CheckOneRule: Act. Groundtime <%ld> > MAXT <%ld> [seconds]", lpActGroundTime, llMaxt );/* AM 20071231 */ 
			dbg ( DEBUG, "CheckOneRule: Act. Groundtime <%ld> > MAXT <%ld> [seconds]", lpActGroundTime, llMaxt );
			return FALSE;
		}
	}
	for ( i=0; blRet && (i<COND_COUNT); i++ )
	{		
		if ( pcpInbound && (*pclSrhAdid != 'D') )
		{
			blRet = CheckSingleValue ( &(rgBasicData[i]), 
										SRHFIELD(pcpRule,rgCndFldIdxSgl[i]),
										AFTFIELD(pcpInbound,rgAftInbFldIdx[i]) );
			if  ( blRet )
				blRet = CheckGroupValue ( &(rgBasicData[i]), 
										   SRHFIELD(pcpRule,rgCndFldIdxGrp[i]),
										   AFTFIELD(pcpInbound,rgAftInbFldIdx[i]) );
		}	
		if ( pcpOutbound && (*pclSrhAdid != 'A') )
		{
			blRet = CheckSingleValue (&(rgBasicData[i]), 
									  SRHFIELD(pcpRule,rgCndFldIdxSgl[i]),
									  AFTFIELD(pcpOutbound,rgAftOutFldIdx[i]) );
			if  ( blRet )
				blRet = CheckGroupValue (&(rgBasicData[i]), 
									     SRHFIELD(pcpRule,rgCndFldIdxGrp[i]),
										 AFTFIELD(pcpOutbound,rgAftOutFldIdx[i]) );
		}	
	}	
	return blRet;
}


static int CreateSkeleton4Rule ( ARRAYINFO *prpFlights, char *pcpRule, 
								 char *pcpOuri, char *pcpOuro, char *pcpUhss )
{
	char clSdeKey[21], clOstBuf[512];
	char	*pclSdePtr;
	long llSdeRow, llOstRow; 
	int ilRc = RC_SUCCESS, ilRc1;
 
	sprintf ( clSdeKey, "%c,%s", cgFlagActive, pcpRule );

	dbg(DEBUG, "CreateSkeleton4Rule: Create Status UAFTs <%s><%s> SDE-Key <%s>", 
				  pcpOuri, pcpOuro, clSdeKey );	
	llSdeRow = ARR_FIRST;
	while ( CEDAArrayFindRowPointer(&(rgSdeArray.rrArrayHandle),
									rgSdeArray.crArrayName,
									&(rgSdeArray.rrIdxHandle[1]),
									rgSdeArray.crIdxName[1],clSdeKey,
									&llSdeRow, (void*)&pclSdePtr ) == RC_SUCCESS )
	{
		ilRc1 = FillOstRecord( prpFlights, clOstBuf, sizeof(clOstBuf),
							   pclSdePtr, pcpOuri, pcpOuro, pcpUhss );
		if (bgSkipFillOstRecord==TRUE)   /* AM 20071011 */ 
		{
			dbg ( TRACE, "CreateSkeleton4Rule: FillOstRecord SKIP UAFTs for SDE URNO <%s>", SDEFIELD(pclSdePtr,igSdeUrnoIdx) );/* AM 20071011 */ 
		}
		else      /* AM 20071011 */ 
		{
			if ( ilRc1 != RC_SUCCESS )
			{
				dbg ( TRACE, "CreateSkeleton4Rule: FillOstRecord failed UAFTs <%s><%s> RC <%d>",
					  pcpOuri, pcpOuro, ilRc1 );
				ilRc = RC_FAIL;
			}
			else
			{
				llOstRow = ARR_LAST;
				ilRc1 = CEDAArrayAddRow (&rgNewOstArr.rrArrayHandle, 
										rgNewOstArr.crArrayName, &llOstRow, 
										(void*)clOstBuf );
				if ( ilRc1 != RC_SUCCESS )
				{
					dbg ( TRACE, "CreateSkeleton4Rule: FillOstRecord failed UAFTs <%s><%s> RC <%d>",
						  pcpOuri, pcpOuro, ilRc1 );
					ilRc = RC_FAIL;
				}	
			}
		}
		llSdeRow = ARR_NEXT;
	}
	return ilRc;
}

static int PrepareSdeArray ( char *pcpUsrh )
{
	char clKey[21]="1";		/* key on USRH */
	long llSdeRow, llSrhRow, llPredRow, llLevlIdx=-1, llAdidIdx=-1;
	char *pclSdePtr, *pclFldr, *pclAftf, *pclRety, *pclSrhPtr, *pclSrhAdid ;
	int ilPos, ilCol, ilIdx, ilRc = RC_SUCCESS;
	char clAdid[5], clLevl[5], clPredLevl[5];
	char *pclLevl, *pclAdid, *pclSdeu, *pclPredecessor, *pclUsrh;
	BOOL blAnythingChanged;

	dbg ( TRACE, "PrepareSdeArray: Start pcpUsrh <%s>", pcpUsrh );
	llSrhRow = ARR_FIRST;
	while ( CEDAArrayFindRowPointer(&(rgSrhArray.rrArrayHandle),
									rgSrhArray.crArrayName,
									&(rgSrhArray.rrIdxHandle[0]),
									rgSrhArray.crIdxName[0],pcpUsrh,
									&llSrhRow, (void*)&pclSrhPtr ) == RC_SUCCESS )
	{
		llSrhRow = ARR_NEXT;
		sprintf ( clKey, "1,%s", SRHFIELD(pclSrhPtr,igSrhUrnoIdx) );
		pclSrhAdid = SRHFIELD(pclSrhPtr,igSrhAdidIdx) ;
		dbg ( DEBUG, "PrepareSdeArray: Key for SDE-search <%s>", clKey );
		llSdeRow = ARR_FIRST;
		while ( CEDAArrayFindRowPointer(&(rgSdeArray.rrArrayHandle),
										rgSdeArray.crArrayName,
										&(rgSdeArray.rrIdxHandle[1]),
										rgSdeArray.crIdxName[1],clKey,
										&llSdeRow, (void*)&pclSdePtr ) == RC_SUCCESS )
		{
			pclFldr = SDEFIELD(pclSdePtr,igSdeFldrIdx);
			pclAftf = SDEFIELD(pclSdePtr,igSdeAftfIdx);
			pclRety = SDEFIELD(pclSdePtr,igSdeRetyIdx);
			strcpy ( clLevl, "00" );
			if ( strncmp	( pclFldr, "PLAN", 4 ) == 0 )
			{	/* reference time is planned time of event */
				if ( strncmp ( pclRety, "CCA", 3 ) == 0 )
					strcpy ( clAdid, "D" );		
				else if ( strncmp ( pclRety, "AFT", 3 ) == 0 )
				{
					ilIdx = -1;
					FindItemInList( INBOUNDTIMES, pclAftf, ',', &ilIdx, &ilCol,&ilPos);
					if ( ilIdx > 0 )
						strcpy ( clAdid, "A" );
					else
						strcpy ( clAdid, "D" );
				}
				else
				{	/* referenced object is JOB */
					strcpy ( clAdid, pclSrhAdid );	
				}

			}
			else if ( strcmp	( pclFldr, "CONA" ) == 0 )
			{
				strcpy ( clAdid, "-" );
				strcpy ( clLevl, "-1" );
			}
			else if (strncmp(pclFldr,"OTA",3)==0)
			{/* AM 20071204. Other Arrival Reference Time */ 
				strcpy ( clAdid, "A");
				strcpy ( clLevl, "-1" );
			}
			else if (strncmp(pclFldr,"OTD",3)==0)
			{/* AM 20071204. Other Arrival Reference Time */ 
				strcpy ( clAdid, "D");
				strcpy ( clLevl, "-1" );
			}			
			else
			{
				ilIdx = -1;
				FindItemInList( INBOUNDTIMES, pclFldr, ',', &ilIdx, &ilCol,&ilPos);
				if ( ilIdx > 0 )
					strcpy ( clAdid, "A" );
				else
					strcpy ( clAdid, "D" );
			}
			dbg ( DEBUG, "PrepareSdeArray: set LEVL <%s> ADID <%s> for SDE-URNO <%s>", 
						  clLevl, clAdid, SDEFIELD(pclSdePtr,igSdeUrnoIdx));

			CEDAArrayPutField( &rgSdeArray.rrArrayHandle,
								rgSdeArray.crArrayName, &llLevlIdx, "LEVL",
								llSdeRow, clLevl );	
			CEDAArrayPutField( &rgSdeArray.rrArrayHandle,
								rgSdeArray.crArrayName, &llAdidIdx, "ADID", 
								llSdeRow, clAdid );	
			llSdeRow = ARR_NEXT;
		}	
	}
	
	dbg ( TRACE, "PrepareSdeArray: First initialisation done" );
	do 
	{
		blAnythingChanged = FALSE;
		llSdeRow = ARR_FIRST;
		while ( CEDAArrayFindRowPointer(&(rgSdeArray.rrArrayHandle),
										rgSdeArray.crArrayName,
										&(rgSdeArray.rrIdxHandle[1]),
										rgSdeArray.crIdxName[1], "",
										&llSdeRow, (void*)&pclSdePtr ) == RC_SUCCESS )
		{
			pclLevl = SDEFIELD(pclSdePtr,igSdeLevlIdx);
			pclSdeu = SDEFIELD(pclSdePtr,igSdeSdeuIdx);
			pclUsrh = SDEFIELD(pclSdePtr,igSdeUrnoIdx);
			if ( *pclLevl == '-' )
			{
				llPredRow = ARR_FIRST;
				if ( CEDAArrayFindRowPointer(&(rgSdeArray.rrArrayHandle),
											 rgSdeArray.crArrayName,
											 &(rgSdeArray.rrIdxHandle[0]),
											 rgSdeArray.crIdxName[0],pclSdeu,
											 &llPredRow, (void*)&pclPredecessor ) == RC_SUCCESS )
				{
					strcpy ( clPredLevl, SDEFIELD(pclPredecessor,igSdeLevlIdx) );
					if ( *clPredLevl == '-' )
						dbg ( DEBUG, "PrepareSdeArray: Predecessor SDEU <%s> not yet initialised LEVL <%s>", 
							  pclSdeu, clPredLevl );
					else
					{
						pclAdid = SDEFIELD(pclPredecessor,igSdeAdidIdx);
						ilPos = atoi(clPredLevl);
						dbg ( DEBUG, "PrepareSdeArray: Found initialised predecessor SDEU <%s> LEVL <%s> ADID <%s>", 
							  pclSdeu, clPredLevl, pclAdid );
						sprintf ( clLevl, "%02d", ilPos + 1 );
						CEDAArrayPutField( &rgSdeArray.rrArrayHandle,
											rgSdeArray.crArrayName, &llLevlIdx, "LEVL",
											llSdeRow, clLevl );	
						CEDAArrayPutField( &rgSdeArray.rrArrayHandle,
											rgSdeArray.crArrayName, &llAdidIdx, "ADID", 
											llSdeRow, pclAdid );	
						blAnythingChanged = TRUE;
					}
				}
				else
				{
					dbg ( TRACE, "PrepareSdeArray: Didn't find predecessor SDEU <%s> USRH <%s>", 
						  pclSdeu, pclUsrh );
					ilRc = RC_NOTFOUND;
				}
			}
			llSdeRow = ARR_NEXT;
		}
	}while (blAnythingChanged);

	ilRc = CEDAArrayWriteDB(&rgSdeArray.rrArrayHandle, rgSdeArray.crArrayName,
							NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);

	dbg ( TRACE, "PrepareSdeArray: End ilRC <%d>", ilRc );
	return ilRc;
}

/*  if Status is based on a turnaround rule, get the 2nd flight URNO */
static int GetFlt2Value ( ARRAYINFO *prpFlightArr, char *pcpUaft1, 
						  char *pcpUsrh, char *pcpUaft2 )
{
	long llSrhRow = ARR_FIRST;
	char *pclSrhRes = 0;
	int ilRc;

	if ( !prpFlightArr || !pcpUaft1 || !pcpUsrh || !pcpUaft2 )
		return RC_INVALID;
	pcpUaft2[0] = '\0';
	dbg ( DEBUG, "GetFlt2Value: UAFT1 <%s>, USRH <%s>", pcpUaft1, pcpUsrh );
	ilRc = CEDAArrayFindRowPointer(&(rgSrhArray.rrArrayHandle),
								 rgSrhArray.crArrayName,
								 &(rgSrhArray.rrIdxHandle[0]),
								 rgSrhArray.crIdxName[0], pcpUsrh, 
								 &llSrhRow, (void*)&pclSrhRes ) ;
	if ( ilRc == RC_SUCCESS )
	{
		if ( *SRHFIELD(pclSrhRes,igSrhAdidIdx) != 'T' )
			ilRc = RC_DONT_CARE;
	}
	if ( ilRc == RC_SUCCESS )
	{	/* we found a turnaround rule responsible for the status */
		ilRc = Get2ndFlightUrno ( prpFlightArr, pcpUaft1, pcpUaft2 );
	}
	dbg ( DEBUG, "GetFlt2Value: UAFT1 <%s>, USRH <%s> UAFT2 <%s> RC <%d>", pcpUaft1, pcpUsrh, pcpUaft2, ilRc );
	return ilRc;
}		


static int Get2ndFlightUrno ( ARRAYINFO *prpFlightArr, char *pcpUaft1, char *pcpUaft2 )
{
	long llAftRow = ARR_FIRST;
	char *pclAftRes = 0, *pcl2ndFlightRow;
	int ilRc;

	if ( !pcpUaft1 || !pcpUaft2 || !prpFlightArr )
		return RC_INVALID;

	pcpUaft2[0] = '\0';
	ilRc = CEDAArrayFindRowPointer(&(prpFlightArr->rrArrayHandle),
								   prpFlightArr->crArrayName,
								   &(prpFlightArr->rrIdxHandle[0]),
								   prpFlightArr->crIdxName[0], pcpUaft1, 
								   &llAftRow, (void*)&pclAftRes ) ;
	if ( ilRc == RC_SUCCESS )
		ilRc = Find2ndFlight ( prpFlightArr, pclAftRes, &pcl2ndFlightRow );
	if (ilRc == RC_SUCCESS) 
	{
		if ( !pcl2ndFlightRow )
			ilRc = RC_INVALID;
		else
			strcpy ( pcpUaft2, AFTFIELD (pcl2ndFlightRow, igAftUrnoIdx) );
	}
	return ilRc;
}



static int HandleAf1Update ( char *pcpFlnu )
{
	char	clUrno[21], *pclFca1, *pclFca2, *pclOstPtr, *pclAf1Row;
	long	llOstRow, llFieldNo=-1, llAf1Row=ARR_FIRST;
	int		ilRc=RC_SUCCESS, ilRc1=RC_SUCCESS;

	if ( !pcpFlnu )
	{
		dbg ( TRACE, "HandleAf1Update: pcpFlnu is NULL" );
		return RC_INVALID;
	}
	if ( CEDAArrayFindRowPointer(&(rgAf1Array.rrArrayHandle),
								 rgAf1Array.crArrayName,
								 &(rgAf1Array.rrIdxHandle[1]),
								 rgAf1Array.crIdxName[1],pcpFlnu,
								 &llAf1Row, (void*)&pclAf1Row ) != RC_SUCCESS )
		return RC_NOTFOUND;

	pclFca1 = AF1FIELD(pclAf1Row, igAf1Fca1Idx);
	pclFca2 = AF1FIELD(pclAf1Row, igAf1Fca2Idx);

	/*  search all OST-records for flight with AFTF =='FCA1' or AFTF =='FCA2' */
	llOstRow = ARR_FIRST;
	while ( CEDAArrayFindRowPointer(&(rgOstArray.rrArrayHandle),
									rgOstArray.crArrayName,
									&(rgOstArray.rrIdxHandle[1]),
									rgOstArray.crIdxName[1],pcpFlnu,
									&llOstRow, (void*)&pclOstPtr ) == RC_SUCCESS )
	{
		if ( !strncmp( OSTFIELD(pclOstPtr,igOstAftfIdx), "FCA1", 4 ) )
		{	
			dbg ( TRACE, "HandleAf1Update: Setting <CONA> to <%s> UOST <%s> for flight <%s>", 
				 pclFca1, OSTFIELD(pclOstPtr,igOstUrnoIdx), pcpFlnu );
			ilRc1 |= CEDAArrayPutField (&rgOstArray.rrArrayHandle,
										rgOstArray.crArrayName, &llFieldNo, 
										"CONA", llOstRow, pclFca1 );
		}
		if ( !strncmp( OSTFIELD(pclOstPtr,igOstAftfIdx), "FCA2", 4 ) )
		{	
			dbg ( TRACE, "HandleAf1Update: Setting <CONA> to <%s> UOST <%s> for flight <%s>", 
	   					 pclFca2, OSTFIELD(pclOstPtr,igOstUrnoIdx), pcpFlnu );
			 ilRc1 |= CEDAArrayPutField (&rgOstArray.rrArrayHandle,
										rgOstArray.crArrayName, &llFieldNo, 
										"CONA", llOstRow, pclFca2 );
		}
		llOstRow = ARR_NEXT;
	}
	if ( ilRc1 != RC_SUCCESS )
	{
		dbg ( TRACE, "HandleAf1Update: Update of Field <CONA> failed for flight <%s>", pcpFlnu );
		ilRc = RC_FAIL;
	}/* end of if */
	
	return ilRc;
}

/* AM 20071127                                                                  */ 
/* Add Codes for RSMTAB Data                                                    */ 
/*  HandleRsmUpdate: set OST.CONA for all affected status			*/
/*	IN:		pcpUaft:	flight URNO, must not be NULL		*/
/*	   		pcpTskn:	Task Name, must not be NULL		*/
static int HandleRsmUpdate ( char *pcpUaft, char *pcpTskn, char *pcpRsmUrno )
{
	char	clUrno[21], clConaI[21], clConaC[21], clConaF[21], *pclOstPtr;
	char	clUaft2[11]="", clTurnConaI[21], clTurnConaC[21], clTurnConaF[21];
	long	llOstRow, llFieldNo=-1;
	int		ilRc, ilRc1=RC_SUCCESS;
	char	pclSchStart[32]="", pclActStart[32]="", pclSchEnd[32]="", pclActEnd[32]="", pclProg[32]="", pclStat[64]="";
	char	pclKey[128]="";
	char    pclCona[32]="";
	char  *pclOstFldPtr = NULL;
	char  pclRsmUaft[16];
	char  pclOstUaft[16];
	char  pclRsmTskn[128];
	char  pclOstTskn[128];
        char  pclOstUrno[16];
        char  pclOstUsrh[16];
        char  pclOstUsde[16];
        char  pclOstUhss[16];
        long llLineNo = 0;
	int     cnt = 0;
	BOOL    blUpdCona = FALSE;

	if ( !pcpUaft || !pcpTskn )
	{
		dbg ( TRACE, "HandleRsmUpdate: At least one parameter is NULL" );
		return RC_INVALID;
	}

	strcpy(pclRsmUaft,pcpUaft);
	TrimRight(pclRsmUaft);
	strcpy(pclRsmTskn,pcpTskn);
	TrimRight(ToUpper(pclRsmTskn));
	if ( IS_EMPTY(pcpTskn) )
	{
		dbg ( TRACE, "HandleRsmUpdate: Task Name is not filled -> nothing to do" );
		return RC_SUCCESS;
	}

	dbg ( TRACE, "HandleRsmUpdate: RSM.UAFT <%s> RSM.TSKN <%s> RSM.URNO <%s>", pclRsmUaft, pclRsmTskn,pcpRsmUrno );
	ilRc = GetRsmInfo ( pclRsmUaft, pclRsmTskn, 
				pclSchStart, pclActStart,
				pclSchEnd, pclActEnd, 
				pclProg, pclStat );

	if(ilRc != RC_SUCCESS)
	{
		dbg ( TRACE, "HandleRsmUpdate: GetRsmInfo failed UAFT <%s> TSKN <%s>", 
			  pclRsmUaft, pclRsmTskn );
	}
	else
	{	/*  search all OST-records for flight RSM.UAFT with Task Name RSM.TSKN */
/* BERNI */
          llOstRow = ARR_FIRST;
          llLineNo = -1;
          /* do */ 
        sprintf( pclKey, "%s", pclRsmUaft );
		while ( CEDAArrayFindRowPointer(&(rgOstArray.rrArrayHandle),
										rgOstArray.crArrayName,
										&(rgOstArray.rrIdxHandle[1]),
										rgOstArray.crIdxName[1],pclKey,
										&llOstRow, (void*)&pclOstPtr ) == RC_SUCCESS )
          {
	          /* AM 20081106 Commented out -Start
            ilRc = CEDAArrayGetRowPointer(&rgOstArray.rrArrayHandle,rgOstArray.crArrayName,llOstRow,(void *)&pclOstPtr);
            if (ilRc == RC_SUCCESS)
               AM 20081106 Commented out -Finish*/
            {
              llLineNo++;
              pclOstFldPtr = OSTFIELD(pclOstPtr,igOstUaftIdx);
              strcpy(pclOstUaft,pclOstFldPtr);
              TrimRight (pclOstUaft);
              if (strcmp(pclRsmUaft,pclOstUaft) == 0)
              {
                dbg(DEBUG,"FOUND OSTTAB RECORD WITH UAFT <%s>",pclRsmUaft);
                pclOstFldPtr = OSTFIELD(pclOstPtr,igOstTsknIdx);
                strcpy(pclOstTskn,pclOstFldPtr);
                TrimRight(ToUpper(pclOstTskn));
               	dbg(DEBUG,"COMPARE: RSM.TSKN <%s> OST.TSKN <%s>",pclRsmTskn,pclOstTskn);
                if (strcmp(pclRsmTskn,pclOstTskn) == 0)
			    {  /*  Task Name fits to task name of status */
				   dbg(TRACE,"MATCHING OSTTAB RECORD IDENTIFIED");
               	   dbg(TRACE,"METHOD TSKN: RSM.TSKN <%s> OST.TSKN <%s>",pclRsmTskn,pclOstTskn);

				   /*llOstRow = llLineNo; AM 20081106 */

                   pclOstFldPtr = OSTFIELD(pclOstPtr,igOstUrnoIdx);
                   dbg(TRACE,"KEY FIELD: OST.URNO <%s>",pclOstFldPtr);
/*
                   pclOstFldPtr = OSTFIELD(pclOstPtr,igOstUsrhIdx);
                   dbg(TRACE,"KEY FIELD: OST.USRH <%s>",pclOstFldPtr);
                   pclOstFldPtr = OSTFIELD(pclOstPtr,igOstUhssIdx);
                   dbg(TRACE,"KEY FIELD: OST.UHSS <%s>",pclOstFldPtr);
                   pclOstFldPtr = OSTFIELD(pclOstPtr,igOstUsdeIdx);
                   dbg(TRACE,"KEY FIELD: OST.USDE <%s>",pclOstFldPtr);
                */
                   cnt++;
				   pclCona[0] = '\0';
				   blUpdCona = FALSE;
				
				   char* pclRety = OSTFIELD(pclOstPtr,igOstRetyIdx);
				   /* dbg(TRACE,"KEY FIELD: OST.RETY <%s>",pclRety);*/
				   if (!IS_EMPTY(pclRety))
				   {
					  if ( !strncmp( pclRety, "OTHS", 4 ) )
					  {/* Start Time coming from Other System */ 
						strcpy( pclCona, pclActStart );
						blUpdCona = TRUE;
					  }
					  else if ( !strncmp( pclRety, "OTHE", 4 ) )
					  {/* End Time coming from Other System */ 
						strcpy( pclCona, pclActEnd );
						blUpdCona = TRUE;
					  }
					  else if ( !strncmp( pclRety, "OTHC", 4 ) )
					  {/* Check List from Other System */ 
						blUpdCona = TRUE;
						int ilProg = atoi( pclProg );
						if (ilProg<1) strcpy( pclCona, "" );
						else
						{
							if (!IS_EMPTY(pclActStart))	 strcpy( pclCona, pclActStart );/* If Has Actual Start, use as CONA */ 
							else if (!IS_EMPTY(pclActEnd))	 strcpy( pclCona, pclActEnd );/* If Has Actual End, use as CONA */ 
							else 
							{/* If NO Actual Start and End */ 
								char pclOstCons[32] = "";
								strcpy( pclOstCons, OSTFIELD(pclOstPtr, igOstConsIdx));
								if (!IS_EMPTY(pclOstCons)) strcpy( pclCona, pclOstCons );/* If CONS is exist, use as CONA */ 
								else GetServerTimeStamp("UTC", 1, 0, pclCona);/* Getting Current Date Time */ 
							}
						 }
					  }
					  if (blUpdCona == TRUE)
					  {
						if (strcmp(OSTFIELD(pclOstPtr,igOstConaIdx),pclCona))
						{
							ilRc1 = CEDAArrayPutField (&rgOstArray.rrArrayHandle,
													rgOstArray.crArrayName, &llFieldNo, 
													"CONA", llOstRow, pclCona );
						 	dbg ( TRACE, "HandleRsmUpdate: Set CONA <%s> for RETY <%s> Row <%ld> RC <%d>",
							      pclCona, pclRety, llOstRow, ilRc1 ); /* MSI: 20080701: changed from ilRc to ilRc1     */ 
						}
					  }
				   }
			     } /* end of: if Task Name */
              }
              llOstRow = ARR_NEXT;
            }
          } /* while (ilRc == RC_SUCCESS);*/
	}/* end of if */
	
	return ilRc;
}

/*--------------------------------------------------------------------------------------------------*/
/*  GetConsRsm: get schedule time for the task   	         				    */
/*  IN:	    pcpUaft:	flight URNO, must not be NULL				                    */
/*	    pcpTskn:	Task Name  of jobs that are used for the status, must not be NULL           */
/*	    pcpFldr:	Scheduled Virtual Field Name    			                    */
/*  OUT:    pcpTime:    Scheduled time     							    */
/*--------------------------------------------------------------------------------------------------*/
static int GetConsRsm( char* pcpUaft, char* pcpTskn , char* pcpFldr, char* pcpTime )
{
	int ilRc = RC_SUCCESS;
	pcpTime[0] = '\0';
	char pclRsmTabFldName[16]="";
	char pclTskn[64] = "";   /* AM 20081105 - to use the local variable */
	char pclFldr[16] = "";   /* AM 20081105 - to use the local variable */
	/* dbg ( TRACE, "GetConsRsm: Start. UAFT <%s>, TSKN <%s>, FLDR <%s>", pcpUaft, pcpTskn, pcpFldr ); */ 
	if ( !pcpUaft || IS_EMPTY(pcpUaft) || !pcpTskn || IS_EMPTY(pcpTskn) || 
		!pcpFldr || IS_EMPTY(pcpFldr) || !pcpTime )
	{
		dbg ( TRACE, "GetRsmFieldValue: At least one parameter is NULL" );
		ilRc = RC_INVALID;
	}
	
	if (ilRc == RC_SUCCESS)
	{
		strcpy( pclTskn, pcpTskn );/* AM 20081105 - to use the local variable */
		strcpy( pclFldr, pcpFldr );/* AM 20081105 - to use the local variable */
		TrimRight(ToUpper(pclTskn));
		TrimRight(pclFldr);
		dbg( TRACE, "GetConsRsm:IN:UAFT<%s>, TSKN<%s>, FLDR<%s>", pcpUaft, pclTskn, pclFldr );
		if (bgCheckAndRefillRSMData==TRUE)
		{
			FillRsmArrayByFlightId( pcpUaft, TRUE );
		}

		if ((strcmp(pclFldr, "OTAS")==0) || (strcmp(pclFldr, "OTDS")==0))
		{/* Schedule Start Time */ 
			strcpy( pclRsmTabFldName, "SDST" );
			ilRc = GetRsmFieldValue( pcpUaft, pclTskn , "SDST", pcpTime );
		}
		else if ((strcmp(pclFldr, "OTAE")==0) || (strcmp(pclFldr, "OTDE")==0))
		{/* Schedule End Time */ 
			strcpy( pclRsmTabFldName, "SDED" );
			ilRc = GetRsmFieldValue( pcpUaft, pclTskn , "SDED", pcpTime );
		}
		else if ((strcmp(pclFldr, "OTAC")==0) || (strcmp(pclFldr, "OTDC")==0))
		{/* Check List. No need Time */ 
			strcpy( pcpTime, "" );
		}
		else
		{/* Invalid Reference for Schedule Time */ 
			ilRc = RC_INVALID;
		}
	}

	if (ilRc==RC_SUCCESS)
		dbg( TRACE, "GetConsRsm: UAFT<%s>, TSKN<%s>, FLDR<%s>, Time<%s>", pcpUaft, pcpTskn, pcpFldr, pcpTime );
	else
		dbg( TRACE, "GetConsRsm: Err : UAFT<%s>, TSKN<%s>, FLDR<%s>", pcpUaft, pcpTskn, pcpFldr );
		
	return ilRc;
}

/*--------------------------------------------------------------------------------------------------*/
/*  GetRsmFieldValue: get value of the given field													*/
/*	IN:		pcpUaft:	flight URNO, must not be NULL												*/
/*			pcpTskn:	Task Name  of jobs that are used for the status, must not be NULL			*/
/*			pcpRsmFldName:	Field Name to get the value, must not be NULL or blank. must be valid	*/
/*  OUT:    pcpValue : value of the field															*/
/*--------------------------------------------------------------------------------------------------*/
static int GetRsmFieldValue( char *pcpUaft, char *pcpTskn, 
						char* pcpRsmFldName, char* pcpValue )
{
	char clKey[128]="";
	int  ilCnt = 0;
	int ilRsmFldIdx = -1, ilCol, ilPos;
	int ilRc = RC_SUCCESS;
	long llRow=ARR_FIRST ;
	char *pclRsm;
	pcpValue[0] = '\0';
	char pclTskn[64] = ""; /* AM 20081105 - to use the local variable */

	if ( !pcpUaft || IS_EMPTY(pcpUaft) || !pcpTskn || IS_EMPTY(pcpTskn) || 
		!pcpRsmFldName || IS_EMPTY(pcpRsmFldName) || !pcpValue )
	{
		dbg ( TRACE, "GetRsmFieldValue: At least one parameter is NULL" );
		ilRc = RC_INVALID;
	}
	strcpy( pclTskn, pcpTskn );/* AM 20081105 - to use the local variable */
    TrimRight(ToUpper( pclTskn));
    
	/* dbg( TRACE, "GetRsmFieldValue:IN:UAFT<%s>, TSKN<%s>, RFLD<%s>", pcpUaft, pclTskn, pcpRsmFldName ); */
	if (ilRc == RC_SUCCESS)
	{
		if (FindItemInList(RSM_FIELDS, pcpRsmFldName,',', &ilRsmFldIdx,&ilCol,&ilPos)!=RC_SUCCESS)
		{
			ilRc = RC_INVALID;
			dbg( TRACE, "GetRsmFieldValue: Invalid Field Name <%s>", pcpRsmFldName );
		}
	}

	if (ilRc == RC_SUCCESS)
	{
		sprintf ( clKey, "%s,%s", pcpUaft, pclTskn );
		dbg ( DEBUG, "GetRsmFieldValue: Rsmtab-Key <%s>", clKey);
		if ( CEDAArrayFindRowPointer(&(rgRsmArray.rrArrayHandle),
										rgRsmArray.crArrayName,
										&(rgRsmArray.rrIdxHandle[1]),
										rgRsmArray.crIdxName[1], clKey, &llRow,
										(void *)&pclRsm ) == RC_SUCCESS )
		{		
			strcpy( pcpValue, RSMFIELD( pclRsm, ilRsmFldIdx ));
		}
	}

	dbg( TRACE, "GetRsmFieldValue: UAFT <%s>, TSKN <%s>, RFLD <%s>, Value <%s>", pcpUaft, pclTskn, pcpRsmFldName, pcpValue );
	return ilRc;
}

/*  GetRsmInfo: get actual status confirmation time for Task	 			*/
/*	IN:		pcpUaft:	flight URNO, must not be NULL						*/
/*			pcpTskn:	template  of jobs that are used for the status		*/
/*	OUT:	pcpSchStart:	schedule start time								*/
/*		    pcpActStart:	actual start time								*/
/*		    pcpSchEnd:		schedule end time								*/
/*		    pcpActEnd:		actual end time									*/
/*          pcpProg:		progress										*/
/*			pcpStat:		status											*/
static int GetRsmInfo ( char *pcpUaft, char *pcpTskn, 
		char* pcpSchStart, char* pcpActStart,
		char* pcpSchEnd, char* pcpActEnd, 
		char* pcpProg, char* pcpStat )
{
	char *pclRsm, *pclSchStart, *pclActStart, *pclSchEnd, *pclActEnd, *pclProg, *pclStat;
	long llRow = ARR_FIRST;
	int  i, ilCnt = 0;
	char clKey[128], clNow[21];
	char pclTskn[64] = "";
	
	if ( !pcpUaft || IS_EMPTY(pcpUaft) || !pcpTskn || IS_EMPTY(pcpTskn) || !pcpSchStart || !pcpActStart || !pcpSchEnd || !pcpActEnd || !pcpProg || !pcpStat )
	{
		dbg ( TRACE, "GetRsmInfo: At least one parameter is NULL" );
		return RC_INVALID;
	}
	
	/* AM 20081105 - to fill the rsm info */
	if (bgCheckAndRefillRSMData==TRUE)
	{
		FillRsmArrayByFlightId( pcpUaft, TRUE );
	}
	
	strcpy( pclTskn, pcpTskn ); /* AM 20081105 - to use the local variable */
	TrimRight(ToUpper( pclTskn ));
	
    pcpSchStart[0] = pcpActStart[0] = '\0';
	pcpSchEnd[0] = pcpActEnd[0] = '\0';
	pcpProg[0] = pcpStat[0] = '\0';

	GetActDate ( clNow );

	sprintf ( clKey, "%s,%s", pcpUaft, pclTskn );
	dbg ( DEBUG, "GetRsmInfo: Rsmtab-Key <%s>", clKey);
	
	if ( CEDAArrayFindRowPointer(&(rgRsmArray.rrArrayHandle),
									rgRsmArray.crArrayName,
									&(rgRsmArray.rrIdxHandle[1]),
									rgRsmArray.crIdxName[1], clKey, &llRow,
									(void *)&pclRsm ) == RC_SUCCESS )
	{
		ilCnt++;

		strcpy( pcpStat, RSMFIELD(pclRsm,igRsmStatIdx) );
		strcpy( pcpProg, RSMFIELD(pclRsm,igRsmProgIdx) );
		strcpy( pcpSchStart, RSMFIELD(pclRsm,igRsmSdstIdx) );
		strcpy( pcpActStart, RSMFIELD(pclRsm,igRsmAcstIdx) );
		strcpy( pcpSchEnd, RSMFIELD( pclRsm, igRsmSdedIdx ) );
		strcpy( pcpActEnd, RSMFIELD( pclRsm, igRsmAcedIdx ) );
	}
	
	
	if ( ilCnt > 0 )
	{/* Found */ 
		dbg ( TRACE, "GetRsmInfo: UAFT<%s>, TSKN<%s>, STAT <%s> PROG <%s> SDST <%s> ACST <%s> SDED <%s> ACED <%s>", 
			pcpUaft, pclTskn, pcpStat, pcpProg, pcpSchStart, pcpActStart, pcpSchEnd, pcpActEnd );
		return RC_SUCCESS;
	}
	else
	{/* Not found */ 
		dbg( TRACE, "GetRsmInfo:UAFT<%s>, TSKN<%s> Not found.", pcpUaft, pclTskn );
		/* return RC_NOTFOUND; */ /* MSI: 20080626: May needs to be enabled because of the error seen in log files    */ 
		return RC_SUCCESS;
	}
}


/* Add given char into string */ 
/* String must not have duplicate char */ 
static int AddCharToString( char* pcpString, char cpCharToAdd)
{
	char* pclFirstChar = strchr(pcpString, cpCharToAdd);

	if (pclFirstChar==NULL)
	{/* Char not exist */ 
		/* Add char at front of the string */ 
		char pclTemp[32];
		strcpy( pclTemp, pcpString );
		pcpString[0] = cpCharToAdd;
		pcpString[1] = '\0';
		strcpy( pcpString, pclTemp );
	}
}

/* Remove given char from string */ 
static int RemoveCharsFromString( char* pcpString, char* pcpCharToRemove )
{
	if ((pcpString!=NULL) && (pcpCharToRemove!=NULL))
	{		
		char* pclOrg = pcpString;
		char* pclNew = pcpString;
		char* pclRemove;
		BOOL blRemoveFlag = FALSE;
		
		while (*pclOrg!='\0')
		{	
			pclRemove = pcpCharToRemove;
			blRemoveFlag = FALSE;
			while(*pclRemove!='\0')
			{
				if (*pclOrg==*pclRemove)
				{
					blRemoveFlag = TRUE;
					break;
				}
				pclRemove++;
			}
			
			if (!blRemoveFlag)
			{/* Copy */ 
				*pclNew = *pclOrg;
				pclNew++;
			}
			pclOrg++;
		}
		*pclNew = '\0';
	}
}


/* Compute Alert Chars */ 
static int ComputeAlertChars( char* pcpOldCoty, char* pcpNewCoty, char cpAlertChar )
{
	int ilRc = RC_SUCCESS;
	if ((pcpNewCoty==NULL) || (pcpOldCoty==NULL))
	{
		ilRc = RC_FAIL;
	}
	else if (pcpNewCoty[0] != pcpOldCoty[0])
	{		
		if (pcpNewCoty[0] == 'N')/* Non Compliant */ 
		{
			AddCharToString( cgSetAlertChars, cpAlertChar );
		}
		else if (pcpNewCoty[0] == 'A') /* Acknowledge */ 
		{
			AddCharToString( cgAckAlertChars, cpAlertChar );			
		}
		else
		{/* New status is Compliant or N/A */ 
			if (pcpOldCoty[0]=='N')/* Old status is non compliant */ 
			{
				AddCharToString( cgUnSetAlertChars, cpAlertChar );
			}
		}
	}
}


static char GetCharForAlertIdx( int ipIdx )
{
	if ((ipIdx<0) || (ipIdx>25))
	{
		return '\0';
	}
	else
	{
		return (char) ( (int) 'A' + ipIdx );
	}
}

static int GetIdxForAlertChar( char cpChar )
{
	return (int) cpChar - (int) 'A';
}

static int InitAnAlertChar( ALERT_CHAR_INFO* prpAlertChar, char pcpChar )
{
	int ilRc = RC_SUCCESS;
	
	prpAlertChar->crChar[0] = pcpChar;
	prpAlertChar->crChar[1] = '\0'; 
	prpAlertChar->crRecCnt = 0;
	prpAlertChar->crNCnt = 0;
	prpAlertChar->crACnt = 0;
	prpAlertChar->crOthCnt = 0;
	
	return ilRc;
}

static int InitFlightAlertChars( FLIGHT_ALERT_CHARS* prpFlightAlertChars )
{
	int ilRc = RC_SUCCESS;
	
	prpFlightAlertChars->crUAFT[0] = '\0';
	int clStartChar = (int) 'A';
	int i = 0;
	for( i=0; i<26; i++ )
	{
		InitAnAlertChar( &(prpFlightAlertChars->crAlertChar[i]), GetCharForAlertIdx( i )  );
	}
	prpFlightAlertChars->crAlertCharCnt = 0;
	
	return ilRc;
}

static int AddAlertChar( ALERT_CHAR_INFO* prpAlertChar, char* pcpCoty )
{
	int ilRc = RC_SUCCESS;
	
	if (pcpCoty==NULL)
	{
		ilRc = RC_FAIL;
	}
	else
	{	
		prpAlertChar->crRecCnt++;
		if (pcpCoty[0]=='N')
		{
			prpAlertChar->crNCnt++;
		}
		else if (pcpCoty[0]=='A')
		{
			prpAlertChar->crACnt++;
		}
		else
		{
			prpAlertChar->crOthCnt++;
		}
	}
	return ilRc;
}

static int AddStatusForAlert( FLIGHT_ALERT_CHARS* prpFlightAlertChars,
	char* pcpUAFT, char* pcpCoty, char* pcpAlertChar )
{
	int ilRc = RC_SUCCESS;
	
	if (!prpFlightAlertChars || IS_EMPTY(pcpUAFT) || IS_EMPTY(pcpAlertChar))
	{
		ilRc = RC_FAIL;
		dbg( TRACE, "AddStatusForAlert: Empty One or more paramaters.");
	}
	else
	{
		if (strcmp(prpFlightAlertChars->crUAFT,"")==0)
		{
			strcpy( prpFlightAlertChars->crUAFT, pcpUAFT );
		}
		
		
		if (strcmp(prpFlightAlertChars->crUAFT,pcpUAFT)==0)
		{
			int idx = GetIdxForAlertChar( pcpAlertChar[0] );
			if ((idx>=0) && (idx<MAX_NO_OF_ALERT_CHARS))
			{
				AddAlertChar( &(prpFlightAlertChars->crAlertChar[ idx ]), pcpCoty );
			}
		}
		else
		{
			ilRc = RC_FAIL;
			dbg( TRACE, "AddStatusForAlert:Err:Diff Uaft <%s>, <%s>", prpFlightAlertChars->crUAFT,pcpUAFT );
		}
	}
	
	return ilRc;
} 


#define RSM_SAVE_PROC_ID    7910
#define ALERT_SAVE_PROC_ID	7800
#define ALERT_SAVE_FLAG 	"F"

static int SaveAlertChar( char* pcpUaft, char* pcpUnsetAlertChars, char* pcpSetAlertChars )
{
	int ilRc = RC_SUCCESS;
	if (IS_EMPTY(pcpUaft) || !pcpUnsetAlertChars || !pcpSetAlertChars)
	{
		ilRc = RC_FAIL;
		dbg(TRACE, "SaveAlertChar: Empty One or more paramaters." );
	}
	else if (IS_EMPTY(pcpUnsetAlertChars) && IS_EMPTY(pcpSetAlertChars))
	{
		dbg(DEBUG, "SaveAlertChar: Nothing to update." );
	}
	else
	{
		char pclData[64];
		char pclWhere[128];
		sprintf( pclData, "%s;%s;%s", pcpUnsetAlertChars, pcpSetAlertChars, ALERT_SAVE_FLAG );
		sprintf( pclWhere, "WHERE URNO=%s", pcpUaft );
		dbg(TRACE, "*** Status Alert Processing UAFT <%s>, <%s>", pcpUaft, pclData );
		char pclCurIconChars[64] ;
		char pclNewIconChars[64]="" ;
		char pclCommand[32] = "UFR";
		char pclTableName[32] = "AFT";
		char pclFields[32] = "ICON";
		char pclRecvName[32] = "EXCO";
		char pclDestName[32] = "STAHDL";
	        ilRc = tools_send_info_flag(ALERT_SAVE_PROC_ID,0, pclDestName, cgProcessName, pclRecvName, 
	    							"", "", "", cgTwEnd,
								  	pclCommand, pclTableName, pclWhere, pclFields, pclData, NETOUT_NO_ACK);
	}
	return ilRc;
}


static int ComputeAllAlertCharsForAFlight( char* pcpUAFT, char* pcpChgChar )
{/* To scan all the status for the flight and set/unset the alert chars */ 
		
    int ilRc = RC_SUCCESS;
	char *pclOstPtr;
	long llOstRow = ARR_FIRST;
	BOOL blFilterFlag = FALSE;
	char *pclAlertChar, *pclCoty;
	int ilRc1 = RC_SUCCESS;

	if (pcpUAFT==NULL)
	{
		ilRc = RC_FAIL;
		dbg (TRACE, "ComputeAllAlertCharsForAFlight: No UAFT.");
	}
	else{
		if ((pcpChgChar!=NULL) && pcpChgChar[0]!='*' && (pcpChgChar[0]!='\0'))
		{
			blFilterFlag = TRUE;
			dbg( DEBUG, "ComputeAllAlertCharsForAFlight: uaft<%s>, ChgChar <%s> ", pcpUAFT, pcpChgChar );
		}
		else
		{
			dbg( DEBUG, "ComputeAllAlertCharsForAFlight: uaft<%s>, FILTER <%d> ", pcpUAFT, blFilterFlag );
		}
	
		FLIGHT_ALERT_CHARS prlFlightAlertChars;

		
		InitFlightAlertChars( &prlFlightAlertChars );/* Remove all previous info */ 
		while ( CEDAArrayFindRowPointer(&(rgOstArray.rrArrayHandle),
									    rgOstArray.crArrayName,
										&(rgOstArray.rrIdxHandle[1]),
										rgOstArray.crIdxName[1],pcpUAFT,
										&llOstRow, (void*)&pclOstPtr ) == RC_SUCCESS )
		{
			pclAlertChar = OSTFIELD( pclOstPtr,igOstAlrtIdx );
			if (!IS_EMPTY(pclAlertChar))
			{
				pclCoty = OSTFIELD( pclOstPtr, igOstCotyIdx ); 
				
				if (blFilterFlag==TRUE)
				{
					if (strchr( pcpChgChar, pclAlertChar[0] )!=NULL)
					{
						AddStatusForAlert( &prlFlightAlertChars, pcpUAFT, pclCoty, pclAlertChar );
					}
				}
				else
				{
					AddStatusForAlert( &prlFlightAlertChars, pcpUAFT, pclCoty, pclAlertChar );
				}
			}
			llOstRow = ARR_NEXT;
		}
	
		char pclSetAlertChars[32] = "";
		char pclUnSetAlertChars[32] = "";
		int i=0;
		for( i=0; i<MAX_NO_OF_ALERT_CHARS; i++ )
		{
			EvalAlert( &(prlFlightAlertChars.crAlertChar[i]), pclSetAlertChars, pclUnSetAlertChars );
		}

		if (ilRc == RC_SUCCESS)
		{
			ilRc |= SaveAlertChar( pcpUAFT, pclUnSetAlertChars, pclSetAlertChars );
		}
		else
		{
			dbg( TRACE, "ComputeAllAlertCharsForAFlight: Fail." );
		}
	}
	return ilRc;
}

static int ComputeAllAlertCharsForOstRecords( char* pcpOstKeys )
{/* To scan all the status for the flight(s) related to passing OstKeys */ 
		
    int ilRc = RC_SUCCESS;
    int ilRc1 = RC_SUCCESS;
    
	char *pclOstPtr;
	long llOstRow = ARR_FIRST;
	BOOL blFilterFlag = FALSE;
	char *pclAlertChar, *pclCoty;
	char pclChgChar[32] = "";
	char *pclOstId;
	char pclUaft[32] = "";
	char pclNewUaft[32] = "";
	char pclAlrt[32] = "";
	char pclDummy[32] = "";


	const char delimiters[] = " .,;:!-\r\n\0";
	BOOL blFirstTimeFlag = TRUE;
	
	
	if (IS_EMPTY(pcpOstKeys))
	{
		ilRc = RC_FAIL;
	}
	else{
		char pclOstKeys[92160];		
		strcpy( pclOstKeys, pcpOstKeys );
				
		pclOstId = strtok (pclOstKeys, delimiters);  /* pclOstId => 1st Ost record Id */
		while( pclOstId!=NULL)
		{
			ilRc1 = Get3Fields_ByUrno ( &rgOstArray, pclOstId, igOstUaftIdx, pclNewUaft, igOstAlrtIdx, pclAlrt, -1, pclDummy );
			if ( ilRc1 == RC_SUCCESS )
			{
				if (blFirstTimeFlag==TRUE)
				{
					strcpy( pclUaft, pclNewUaft );
					/* dbg( TRACE, "AddAlertChar" ); */ 
					AddAlertCharToString( pclChgChar, pclAlrt );
					blFirstTimeFlag = FALSE;
				}				
				else if (strcmp( pclNewUaft, pclUaft )==0)
				{/* Same Uaft */ 
					AddAlertCharToString( pclChgChar, pclAlrt );  
				}
				else
				{
					ilRc1 = ComputeAllAlertCharsForAFlight( pclUaft, pclChgChar );
					if (ilRc1!=RC_SUCCESS)
					{
						ilRc1 = RC_SUCCESS;
					}
					strcpy( pclUaft, pclNewUaft );
					strcpy( pclChgChar, pclAlrt ); 
					pclNewUaft[0] = '\0';
				}
			}
			else
			{
				dbg( DEBUG, "ComputeAllAlertCharsForOstRecords:Fail to get field." );
			}
			pclOstId = strtok (NULL, delimiters);    /* pclOstId => next Ost record Id */
		}
		if ((blFirstTimeFlag==FALSE) && (pclNewUaft[0] != '\0'))
		{
			ilRc1 = ComputeAllAlertCharsForAFlight( pclUaft, pclChgChar );
		}
	}
	return ilRc;
}


/* Append given char into string */ 
/* String must not have duplicate char */ 
static int AddAlertCharToString( char* pcpString, char* pcpCharToAdd)
{
	int ilRc = RC_SUCCESS;
	
	if (pcpCharToAdd!=NULL)
	{
		char pclCharToRemove[3];
		pclCharToRemove[0] = toupper(pcpCharToAdd[0]);
		pclCharToRemove[1] = tolower(pcpCharToAdd[0]);
		pclCharToRemove[2] = '\0';
		RemoveCharsFromString( pcpString, pclCharToRemove );
		pcpString = strcat( pcpString, pcpCharToAdd );	
	}
	else
	{
		ilRc = RC_FAIL;
		dbg(TRACE, "AddAlertCharToString: Nothing to add.");
	}
	
	return ilRc;;
}


static int EvalAlert( ALERT_CHAR_INFO* prpAlertChar, 
	char* pcpSetAlertChars, char* pcpUnsetAlertChars )
{
	int ilRc = RC_SUCCESS;
	
	if ((prpAlertChar->crNCnt==0) && (prpAlertChar->crACnt==0) && (prpAlertChar->crOthCnt==0))
	{/* No need to set/unset the alert for this char */ 
	}
	else
	{
		if (prpAlertChar->crNCnt>0)
		{/* Have one or more non-compliant status. Set Alert */ 
			AddAlertCharToString( pcpSetAlertChars, prpAlertChar->crChar );
		}
		else 
		{/* No non-compliant status  */ 
			if ((prpAlertChar->crACnt>0) && (prpAlertChar->crOthCnt==0))
			{/* All status are acknowledged. Set Acknowledge Alert */ 
				char tmpChar[32] = "";
				strcpy( tmpChar, prpAlertChar->crChar );
				tolower( tmpChar[0] );
				AddAlertCharToString( pcpSetAlertChars, tmpChar );
			}
			else
			{/* Compliant, N/A. Unset the alert */ 
				AddAlertCharToString( pcpUnsetAlertChars, prpAlertChar->crChar );
			}
		}	
	}
	return ilRc;
}


/*  Get3Fields_ByUrno: Get content of fields of a record from an array			*/
/*	IN:		prpArray:	CEDAArray that contains the record					*/
/*			pcpUrno:	URNO of requested record							*/
/*			ipFldIdx1:	index1 of requested field, if <=0 will not get data	*/
/*	OUT:	pcpBuf1:	result buffer for field index 1, if record found and ipFldIdxs valid	*/
static int Get3Fields_ByUrno ( ARRAYINFO *prpArray, char *pcpUrno, 
		int ipFldIdx1, char *pcpBuf1,
		int ipFldIdx2, char *pcpBuf2,
		int ipFldIdx3, char *pcpBuf3 )
{
	long llRow=ARR_FIRST;
	char *pclResult;
	int ilRc;
	
	if ( !prpArray || !pcpUrno || !pcpBuf1 || !pcpBuf2 || !pcpBuf3)
	{
		dbg ( TRACE, "Get3Fields_ByUrno: At least one parameter is NULL" );
		return RC_INVALID;
	}
	if ( (ipFldIdx1 > prpArray->lrArrayFieldCnt) || (ipFldIdx1<=0) )
	{
		dbg ( TRACE, "Get3Fields_ByUrno: Invalid field index1 <%d> Array <%s has <%ld> fields",
			  ipFldIdx1, prpArray->crArrayName, prpArray->lrArrayFieldCnt );
		return RC_INVALID;
	}
	if ( prpArray->rrIdxHandle[0] < 0 )
	{
		dbg ( TRACE, "Get3Fields_ByUrno: URNO-index not initialised for array <%s>",
			  prpArray->crArrayName );
		return RC_NOTINITIALIZED;
	}
	pcpBuf1[0] = pcpBuf2[0] = pcpBuf3[0]= '\0';
	ilRc = CEDAArrayFindRowPointer(&(prpArray->rrArrayHandle),
									prpArray->crArrayName,
									&(prpArray->rrIdxHandle[0]),
									prpArray->crIdxName[0],pcpUrno,
									&llRow, (void*)&pclResult );
	if ( ilRc != RC_SUCCESS )
		dbg ( TRACE, "Get3Fields_ByUrno: URNO <%s> not found in array <%s>", 
			  pcpUrno, prpArray->crArrayName );
	else
	{
		if ( (ipFldIdx1>0) && (ipFldIdx1 <= prpArray->lrArrayFieldCnt) )
		{
			strcpy ( pcpBuf1, &(pclResult[prpArray->plrArrayFieldOfs[ipFldIdx1-1]]) );
		}
		if ( (ipFldIdx2>0) && (ipFldIdx2 <= prpArray->lrArrayFieldCnt) )
		{
			strcpy ( pcpBuf2, &(pclResult[prpArray->plrArrayFieldOfs[ipFldIdx2-1]]) );
		}
		if ( (ipFldIdx3>0) && (ipFldIdx3 <= prpArray->lrArrayFieldCnt) )
		{
			strcpy ( pcpBuf3, &(pclResult[prpArray->plrArrayFieldOfs[ipFldIdx3-1]]) );
		}
	}
	return ilRc;
}


/***************************** Processing for Icon Char Updating *************************/
#define AFT_ICON_FIELD_NAME "VERS"
#define AFT_ICON_TBL_NAME   "AFTTAB"

static int GetCurIconChars( char* pcpUaft, char* pcpCurIconChars )
{
	int ilRc = RC_SUCCESS;
	
	if (IS_EMPTY(pcpUaft) || !pcpCurIconChars)
	{
		dbg( TRACE, "GetCurIconChars: Empty one or more Data." );
		ilRc = RC_FAIL;
	}
	else
	{
		char pclSqlKey[128];
		char pclFldName[32]=AFT_ICON_FIELD_NAME;
		char pclTable[32] = AFT_ICON_TBL_NAME;
		char pclDbsDat[128]="";
		pcpCurIconChars[0] = '\0';
		strcpy( pclFldName, AFT_ICON_FIELD_NAME );
		sprintf(pclSqlKey,"WHERE URNO='%s'",pcpUaft);  
		ilRc = HandleGocSelect(pclTable, pclSqlKey, pclFldName, pclDbsDat);
		if (ilRc == DB_SUCCESS)
		{
 		    (void) get_real_item(pcpCurIconChars, pclDbsDat, 1);
		}
		else
		{
			dbg( TRACE, "GetCurIconChars: Unable to get data for <%s> in <%s> <%s>", pclFldName, pclTable, pclSqlKey );
			ilRc = RC_FAIL;
		}
	}
	return ilRc;
}

static int UpdateIconChars( char* pcpUaft, char* pcpOrgIconChars, char* pclNewIconChars )
{
	int ilRc = RC_SUCCESS;
	
	if (IS_EMPTY(pcpUaft) || !pcpOrgIconChars || !pclNewIconChars)
	{
		dbg( TRACE, "UpdateIconChars: Empty one or more Data." );
		ilRc = RC_FAIL;
	}
	else
	{
		char pclSqlKey[128];
		char pclTable[32] = AFT_ICON_TBL_NAME;
		char pclFld[32]   = AFT_ICON_FIELD_NAME;
		char pclNewData[128]="";
		char pclOldData[128]="";
		char pclUpdData[128]="";
		char pclUpdFld[128] ="";
		
		strcpy( pclNewData, pclNewIconChars );
		strcpy( pclOldData, pcpOrgIconChars );
		sprintf(pclSqlKey,"WHERE URNO='%s'",pcpUaft);  
    	        ilRc = HandleGocUpdate(pclTable, pclSqlKey, pclFld, pclNewData, pclOldData, pclUpdFld, pclUpdData);
		if (ilRc == DB_SUCCESS)
		{
			dbg( TRACE,"UdateIconChars:Updated <%s>=<%s> in <%s> <%s>", pclFld,pclNewData, pclTable, pclSqlKey );
		}
		else
		{
			dbg( TRACE, "UpdateIconChars: Fail to update <%s>=<%s> in <%s> <%s>", pclFld,pclNewData, pclTable, pclSqlKey );
			ilRc = RC_FAIL;
		}
	}
	return ilRc;
}

/********************************************************************/
/*StrChCnt.                                                         */
/*IN :                                                              */
/*   pcpStr - string to searh the given char                        */
/*   chToCnt- Char to Count in the string                           */
/*OUT:                                                              */
/*                                                                  */
/*RETURN:                                                           */
/*   number of appeareance of the char in the string                */
/********************************************************************/
static int StrChCnt( char* pcpStr, int chToCnt )
{
	char* pclSt = pcpStr;
	int ilCnt = 0;
	while( *pclSt!='\0')
	{
		if (*pclSt==chToCnt) ilCnt++;
		pclSt++;
	}
	return ilCnt;
}

/********************************************************************/
/*GetItemAtIndex.                                                   */
/*IN :                                                              */
/*   pcpOrgData - Original Data                                     */
/*   idx        - 0 based index (i.e. to get 1st Item, pass 0)      */
/*   ipDelimiter- Item seperator char                               */
/*                                                                  */
/*OUT:                                                              */
/*   pcpItem - Item at given index                                  */
/*                                                                  */
/********************************************************************/
static int GetItemAtIndex( char* pcpOrgData, int idx, int ipDelimiter, char* pcpItem )
{
	int ilRc = RC_SUCCESS;
	if (IS_EMPTY(pcpOrgData) || !pcpItem)
	{
		dbg( TRACE, "GetItemAtIndex: Empty one or more items." );
		ilRc = RC_FAIL;
	}
	else if (idx<0) 
	{
		dbg( TRACE, "GetItemAtIndex: Invalid Index" );
		ilRc = RC_FAIL;
	}
	else
	{
		int ilDelCnt = 0;
		char* pclOrgCur=pcpOrgData;
		char* pclOrgToCopy=pcpOrgData;
		char* pclItemPtr= pcpItem;
		while( *pclOrgCur!='\0')
		{
			if ( *pclOrgCur==ipDelimiter)
			{
				 ilDelCnt++;	
				 if (ilDelCnt>idx) break;
			}
			else
			{
				if (ilDelCnt==idx)
				{
					*pclItemPtr = *pclOrgToCopy;
					pclItemPtr++;
				}
			}
			pclOrgCur++;
			pclOrgToCopy++;
		}
		*pclItemPtr = '\0';
	}
	
	return ilRc;
	
}

/********************************************************************/
/*MakeStReverseCases.                                               */
/*IN :                                                              */
/*   pcpSt - String to Reverse the cases                            */
/*OUT:                                                              */
/*   pcpSt - string with Reversed Cases                             */
/********************************************************************/
static int MakeStReverseCases( char* pcpSt )
{
	int ilRc = RC_SUCCESS;
	if (!pcpSt)
	{
		dbg( TRACE, "null string." );
		ilRc = RC_FAIL;
	}
	else
	{
		char* pcpPtr = pcpSt;
		while( *pcpPtr!='\0' )
		{
			if (islower(*pcpPtr)) *pcpPtr = toupper(*pcpPtr);
			else if (isupper(*pcpPtr)) *pcpPtr = tolower(*pcpPtr);
			pcpPtr++;
		}
		*pcpPtr = '\0';
	}
	return ilRc;
}

static int ProcessIconChars( char* pcpUaft, char* pcpFldName, char* pcpData )
{
	int ilRc = RC_SUCCESS;
	if (IS_EMPTY(pcpUaft) || IS_EMPTY(pcpFldName) || IS_EMPTY( pcpData) )
	{
		dbg( TRACE, "PrepareIconChars: Empty one or more parameters.");
	}
	else if( strcmp(pcpFldName,"ICON")!=0 )
	{
		dbg( TRACE, "No need to process." );
	}
	else
	{
		char pclNewIconChars[128] = "";
		char pclCurIconChars[128] = "";
		
		ilRc = GetCurIconChars( pcpUaft, pclCurIconChars );
		if (ilRc==RC_SUCCESS)
		{
			ilRc = PrepareIconChars( pcpFldName, pcpData, pclCurIconChars, pclNewIconChars );
			if (ilRc==RC_SUCCESS)
			{
				if( strcmp( pclCurIconChars, pclNewIconChars)!=0 )/* different data */ 
				{
					ilRc = UpdateIconChars( pcpUaft, pclCurIconChars, pclNewIconChars );
					if (ilRc!=RC_SUCCESS)
					{
						dbg( TRACE,"ProcessIconChars:Fail to update Icon Chars." );
					}
				}
			}
			else
			{
				dbg( TRACE, "ProcessIconChars: Fail to prepare Icon Chars." );
			}
		}
		else
		{
			dbg( TRACE,"ProcessIconChars: Could not get current icon chars." );
		}
	}
	return ilRc;
}

/********************************************************************/
/*PrepareIconChars Icon Chars.                                      */
/*    Get the current icon chars                                    */
/*      and unset and set the chars accordingly                     */
/*IN :                                                              */
/*   pcpFldName - Field Name (must be 'ICON')                       */
/*   pcpData    - Data for the virtual field ICON                   */
/*                UnSetChars;SetChars;Flag                          */
/*       UnSetChars - Char to Unset from Icon Chars                 */
/*                    Remove 'UnsetChars' from Icon Chars           */
/*       SetChars   - Char to Set to Icon Chars                     */
/*                    Add 'SetChars' to Icon Chars if not existed   */
/*   pcpCurIconChars - Current Icon Chars                           */
/*                                                                  */
/*OUT:                                                              */
/*   pcpNewIconChars - New Icon Chars                               */
/*                                                                  */
/********************************************************************/
static int PrepareIconChars( char* pcpFldName, char* pcpData, char* pcpCurIconChars, char* pcpNewIconChars )
{
	int ilRc = RC_SUCCESS;
	
	const char clSeperator = ';';
	char pclUnsetChars[64]="";
	char pclSetChars[64]="";
	char pclFlags[64] = "";
	char pclCharsToRemove[64] = "";
	char pclTemp[64];
	
	if (IS_EMPTY(pcpFldName) || IS_EMPTY( pcpData) || !pcpCurIconChars || !pcpNewIconChars)
	{
		dbg( TRACE, "PrepareIconChars: Empty one or more parameters.");
	}
	else if (strcmp( pcpFldName, "ICON" ) !=0)
	{
		dbg( TRACE,"PrepareIconChars: Not for ICON." );
	}
	else if (StrChCnt( pcpData, clSeperator )!=2)
	{
		dbg(TRACE,"PrepareIconChars: Incomplete Data <%s>", pcpData );
		ilRc = RC_FAIL;
	}
	else
	{
		GetItemAtIndex( pcpData, 0, clSeperator, pclUnsetChars );
		GetItemAtIndex( pcpData, 1, clSeperator, pclSetChars );
		GetItemAtIndex( pcpData, 2, clSeperator, pclFlags );
		strcpy( pclTemp, pcpCurIconChars );
		
		strcpy( pclCharsToRemove, pclUnsetChars );
		strcat( pclCharsToRemove, pclSetChars );/* To remove the "Char to Set" as well.	 */ 
		RemoveCharsFromString( pclTemp, pclCharsToRemove );
		
		MakeStReverseCases( pclCharsToRemove );
		RemoveCharsFromString( pclTemp, pclCharsToRemove );
		strcpy( pcpNewIconChars, pclSetChars );
		strcat( pcpNewIconChars, pclTemp );
		
		dbg( TRACE, "PrepareIconChars: Data <%s>, CurIconChars <%s>, NewIconChars<%s>, UnSetChars <%s>, SetChars<%s>, Flags<%s>", 
			pcpData, pcpCurIconChars, pcpNewIconChars, pclUnsetChars, pclSetChars, pclFlags );

	}
	return ilRc;
}

static int HandleGocSelect(char *pcpTable, char *pcpSqlKey, char *pcpSqlFld, char *pcpSqlDat)
{
  	int ilRc = RC_SUCCESS;
  	char pclSqlBuf[4096];
  	short slCursor = 0;
  	short slFkt = START;

  	sprintf(pclSqlBuf,"SELECT %s FROM %s %s", pcpSqlFld, pcpTable, pcpSqlKey);
  	pcpSqlDat[0] = 0x00;
  	ilRc = sql_if(slFkt,&slCursor,pclSqlBuf,pcpSqlDat);
  	if (ilRc == DB_SUCCESS)
  	{
    	BuildItemBuffer(pcpSqlDat,pcpSqlFld,-1,",");
    	ilRc = RC_SUCCESS;
  	}	
  	close_my_cursor(&slCursor);

  	return ilRc;
}

static int HandleGocInsert(char *pcpTable, char *pcpSqlFld, char *pcpSqlDat)
{
    int ilRc = RC_SUCCESS;
    char pclSqlBuf[4096];
    char pclTmpBuf[4096];
    short slCursor = 0;
    short slFkt = IGNORE;
    char pclCurTime[32]="";

    strcat(pcpSqlFld,",USEC");
    strcat(pcpSqlDat,",");
    strcat(pcpSqlDat,cgProcessName);
    strcat(pcpSqlFld,",CDAT");
    strcat(pcpSqlDat,",");
    
    GetServerTimeStamp ("UTC", 1, 0, pclCurTime);
    strcat(pcpSqlDat,pclCurTime);
    
    (void) BuildSqlFldStrg(pclTmpBuf, pcpSqlFld, FOR_INSERT);
    sprintf(pclSqlBuf,"INSERT INTO %s %s",pcpTable,pclTmpBuf);
    dbg(DEBUG,"INSERT INTO <%s> <%s> <%s>",pcpTable, pcpSqlFld, pcpSqlDat);
    delton(pcpSqlDat);
    ilRc = sql_if(slFkt, &slCursor, pclSqlBuf, pcpSqlDat);
    commit_work ();
    close_my_cursor(&slCursor);

    BuildItemBuffer(pcpSqlDat,pcpSqlFld,-1,",");

    return ilRc;
}

static int HandleGocUpdate(char *pcpTable, char *pcpSqlKey, 
                           char *pcpFld, char *pcpNewDat, char *pcpCurDat, 
                           char *pcpUpdatedFld, char *pcpUpdatedDat)
{
  int ilRc = RC_SUCCESS;
  int ilFldIdx = 0;
  int ilFldCnt = 0;
  int ilUpdCnt = 0;
  char pclSqlBuf[4096];
  char pclTmpBuf[4096];
  char pclFldNam[32];
  char pclRcvVal[4096];
  char pclDbsVal[4096];
  short slCursor = 0;
  char pclCurTime[32]="";
  short slFkt = IGNORE;
  
  pcpUpdatedFld[0] = '\0';
  pcpUpdatedDat[0] = '\0';
  char pclNewDat[4096] = "\0";
  ilUpdCnt = 0;

  ilFldCnt = field_count(pcpFld);
  for (ilFldIdx = 1; ilFldIdx <= ilFldCnt; ilFldIdx++)
  {
    (void) get_real_item (pclFldNam, pcpFld, ilFldIdx);
    (void) get_real_item (pclRcvVal, pcpNewDat, ilFldIdx);
    (void) get_real_item (pclDbsVal, pcpCurDat, ilFldIdx);
    if (strcmp(pclRcvVal,pclDbsVal) != 0)
    {
      ilUpdCnt++;
      if (ilUpdCnt > 1)
      {
        strcat(pcpUpdatedFld,",");
        strcat(pcpUpdatedDat,",");
      }
      strcat(pcpUpdatedFld,pclFldNam);
      if (pclRcvVal[0] == '\0')
      {
        strcpy(pclRcvVal," ");
      }
      strcat(pcpUpdatedDat,pclRcvVal);
    }
  }
  if (ilUpdCnt > 0)
  {
    dbg(DEBUG,"UPDATE: %d FIELDS CHANGED",ilUpdCnt);
    dbg(DEBUG,"FIELDS   <%s>",pcpUpdatedFld);
    dbg(DEBUG,"NEW DATA <%s>",pcpUpdatedDat);

    strcat(pcpUpdatedFld,",USEU");
    strcat(pcpUpdatedDat,",");
    strcat(pcpUpdatedDat,cgProcessName);
    strcat(pcpUpdatedFld,",LSTU");
    strcat(pcpUpdatedDat,",");
    
    GetServerTimeStamp ("UTC", 1, 0, pclCurTime);
    strcat(pcpUpdatedDat,pclCurTime);

    (void) BuildSqlFldStrg(pclTmpBuf, pcpUpdatedFld, FOR_UPDATE);
    sprintf(pclSqlBuf,"UPDATE %s SET %s %s",pcpTable,pclTmpBuf,pcpSqlKey);
    strcpy(pclNewDat, pcpUpdatedDat);
    delton(pclNewDat);
    ilRc = sql_if(slFkt, &slCursor, pclSqlBuf, pclNewDat);
    commit_work ();
    close_my_cursor(&slCursor);
  }
  return ilRc;
}


/********************************************************/
/********************************************************/
static int BuildSqlFldStrg (char *pcpSqlStrg, char *pcpFldLst, int ipTyp)
{
  int ilRC = RC_SUCCESS;
  int ilFldCnt = 0;
  int ilFld = 0;
  int ilStrLen = 0;
  char pclFldNam[8];
  char pclFldVal[16];
  char pclTmpBuf[4096];

  pcpSqlStrg[0] = 0x00;
  pclTmpBuf[0] = 0x00;
  ilFldCnt = field_count (pcpFldLst);
  switch (ipTyp)
  {
    case FOR_UPDATE:
      ilStrLen = 0;
      for (ilFld = 1; ilFld <= ilFldCnt; ilFld++)
      {
        (void) get_real_item (pclFldNam, pcpFldLst, ilFld);
        sprintf (pclFldVal, "%s=:V%s", pclFldNam, pclFldNam);
        StrgPutStrg (pcpSqlStrg, &ilStrLen, pclFldVal, 0, -1, ",");
      }
      if (ilStrLen > 0)
      {
        ilStrLen--;
        pcpSqlStrg[ilStrLen] = 0x00;
      }
      break;
    case FOR_INSERT:
      ilStrLen = 0;
      for (ilFld = 1; ilFld <= ilFldCnt; ilFld++)
      {
        (void) get_real_item (pclFldNam, pcpFldLst, ilFld);
        sprintf (pclFldVal, ":V%s", pclFldNam);
        StrgPutStrg (pclTmpBuf, &ilStrLen, pclFldVal, 0, -1, ",");
      }
      if (ilStrLen > 0)
      {
        ilStrLen--;
        pclTmpBuf[ilStrLen] = 0x00;
        sprintf (pcpSqlStrg, "(%s) VALUES (%s)", pcpFldLst, pclTmpBuf);
      }
      break;
    default:
      break;
  }
  return ilRC;
}



/*****************************************************************/
/* Process to update the RSMTAB                                  */
/*****************************************************************/
static int ProcessRSMSchTimeUpdate()
{
	int ilRc = RC_SUCCESS;
        long llBgn = 6 * 60 * 60;
        long llEnd = 7 * 60 * 60;
	char clValue[124], clStart[32], clEnd[32], clWhere[256];
	
	GetServerTimeStamp("UTC", 1, 0, clValue);
	GetServerTimeStamp("UTC", 1, lgRSMUpdOffStart, clStart);
	GetServerTimeStamp("UTC", 1, lgRSMUpdOffEnd, clEnd);
	sprintf( clWhere, "WHERE ((TIFA BETWEEN '%s' AND '%s') OR (TIFD BETWEEN '%s' AND '%s'))", clStart, clEnd, clStart, clEnd ); 	
	dbg( TRACE, "ProcessRSMSchTimeUpdate with selection <%s>, curTime<%s> Start<%d>, end<%d>", clWhere, clValue, llBgn, llEnd);

	ilRc = ProcessCFS( clWhere );
	if (ilRc==RC_SUCCESS)
	{
		dbg( TRACE, "ProcessRSMSchTimeUpdate with selection <%s> Completed", clWhere );
	}
	else
	{
		dbg( TRACE, "ProcessRSMSchTimeUpdate: Fail." );
	}
	return ilRc;
}


/********************************************************/
/* To send ScheduleTimes to update in RSMTAB            */
/*                                                      */
static int ProcessCONSChanges( char *pcpUOst, char *pcpUaft, char *pcpUsde, char* pcpUsrh, char *pcpUhss, 
             char *pcpCons, char *pcpTskn, char *pcpRety )
{
   	int ilRc = RC_SUCCESS;
   	int ilGetRc = RC_SUCCESS;
   	char pclRuleName[128] = "";
   	char pclFlDt[16] = "";
   	char pclRSMFld[32] = "";
   	char pclOldVal[128] = "";
   	BOOL blUpd = FALSE;
   	int ilRc1 = RC_SUCCESS;
   	int ilFld = 0;
   			
	char pclFlno[16] = "";
	char pclAdid[8] = "";	
	char pclStdt[16] = "";
	char pclBusn[64] = "";
	char pclTskn[64] = "";
	char pclAlrt[8] = "";
	char pclMoni[2] = "Y";
	char pclUsdes[16] = "";
	char pclUsdee[16] = "";
	    
	char pclSqlKey[256];
	char pclTable[16] = "RSMTAB";
	char pclRcvFld[4096]="URNO,SDST,SDED,MONI,USRH,UHSS,SDES,SDEE";
	char pclNewFld[4096]="";
	char pclDbsDat[4096*8]="";
	char pclRcvDat[4096*8]="";
	char pclNewDat[4096*8]="";
	char pclRsmUrno[16] = "";
	char pclRsmSdst[16] = "";
	char pclRsmSded[16] = "";	

   	
   	if (IS_EMPTY(pcpUOst) || IS_EMPTY(pcpUaft) || IS_EMPTY(pcpUsde) || IS_EMPTY(pcpUsrh) || IS_EMPTY(pcpUhss) || 
   				!pcpCons || !pcpTskn || IS_EMPTY(pcpRety))
   	{
	   	dbg( TRACE, "WARNING: ProcessCONSChanges: EMPTY One or More Data." );
	 	ilRc = RC_FAIL;
   	}

  dbg(TRACE,"====>>> HANDLE OSTTAB CONS UPDATES (BEGIN): ADJUST RELATED RSMTAB RECORDS");   	
  dbg(TRACE,"        PARAM UOST <%s>",pcpUOst);
  dbg(TRACE,"        PARAM UAFT <%s>",pcpUaft);
  dbg(TRACE,"        PARAM USDE <%s>",pcpUsde);
  dbg(TRACE,"        PARAM USRH <%s>",pcpUsrh);
  dbg(TRACE,"        PARAM UHSS <%s>",pcpUhss);
  dbg(TRACE,"        PARAM RETY <%s>",pcpRety);
  dbg(TRACE,"        PARAM CONS <%s>",pcpCons);

ilGetRc = GetFieldByUrno(&rgSdeArray, igSdeTsknIdx, pcpUsde, pclTskn);
/*TrimRight(pclTskn); */ 
TrimRight(ToUpper(pclTskn));  /* MSI: 20080512 */ 
dbg(TRACE,"TRIMMED TASK NANE (TSKN) OF SDE ARRAY: <%s>",pclTskn);
ilGetRc = GetFieldByUrno(&rgSdeArray, igSdeAlrtIdx, pcpUsde, pclAlrt);
dbg(TRACE,"TASK ALERT ICON (ALRT) OF SDE ARRAY: <%s>",pclAlrt);

 	TrimRight(pcpRety);
   	
   	if ((ilRc==RC_SUCCESS) && (strcmp(pclTskn,"")!=0))
   	{
		strcpy(pclRSMFld, "");
		strcpy( pclRcvFld,"URNO,HOPO,RURN,FLNO,ADID,STDT,BUSN,TSKN,SDST,SDED,MONI,USRH,UHSS,SDES,SDEE" );

	   	if (strcmp( pcpRety, "OTHS")==0)
	   	{/* Schedule Start */ 
	   		strcpy( pclRSMFld, "SDST" );	   		
			sprintf(pclSqlKey,"WHERE RURN=%s AND SDES='%s' AND USRH='%s' AND UHSS='%s'",
				pcpUaft,pcpUsde,pcpUsrh,pcpUhss);
	   	}
	   	else if (strcmp( pcpRety, "OTHE")==0)
	   	{/* Schedule End */ 
	   		strcpy( pclRSMFld, "SDED" );
			sprintf(pclSqlKey,"WHERE RURN=%s AND SDEE='%s' AND USRH='%s' AND UHSS='%s'",
				pcpUaft,pcpUsde,pcpUsrh,pcpUhss);
	   	}	   	

		if (strcmp(pclRSMFld,"")!=0)
		{
			if (ilRc == RC_SUCCESS) ilRc = GetSectionName( pcpUhss, pclBusn );
			if (ilRc == RC_SUCCESS) 
			{
				if (strcmp(pclBusn,"")==0)
				{
					ilRc = RC_FAIL;
					dbg( TRACE, "ProcessCONSChanges: No Busn." );
				}
				pclBusn[2] = '\0';/* Take first 2 chars */ 
			}
		 	if (ilRc==RC_SUCCESS)
			{
			    	ilRc = GetFlightInfo(pcpUaft, pclFlno, pclAdid, pclStdt);
   				TrimRight(pclFlno);
			}
			if (ilRc == RC_SUCCESS)
			{
				blUpd = TRUE;
				/* First we check the RSM record relation (created by Stahdl itself): */
				/* If the record exists we can easily update the task name or BU name. */
				/* If not we try to capture an imported RSM record. */ 
				ilRc1 = HandleGocSelect(pclTable, pclSqlKey, pclRcvFld, pclDbsDat);
				if (ilRc1 == DB_SUCCESS)
				{
			        	dbg(TRACE,"RELATED RSMTAB RECORD ALREADY EXISTS");
				}
				else
				{
					sprintf(pclSqlKey,"WHERE RURN=%s AND BUSN='%s' AND TSKN='%s'",pcpUaft,pclBusn,pclTskn);
					ilRc1 = HandleGocSelect(pclTable, pclSqlKey, pclRcvFld, pclDbsDat);
					if (ilRc1 == DB_SUCCESS)
					{
			        		dbg(TRACE,"NAMED RSMTAB RECORD ALREADY EXISTS");
					}
				}
			}

			if (ilRc == RC_SUCCESS)
			{

			    if (ilRc1 == DB_SUCCESS)
			    {
			        TrimAndFilterCr(pclDbsDat, ",", "\n");
			        ilFld = get_item_no(pclRcvFld, "URNO", 5) + 1;
			        (void) get_real_item(pclRsmUrno, pclDbsDat, ilFld);
			        ilFld = get_item_no(pclRcvFld, "SDST", 5) + 1;
			        (void) get_real_item(pclRsmSdst, pclDbsDat, ilFld);
			        ilFld = get_item_no(pclRcvFld, "SDED", 5) + 1;
			        (void) get_real_item(pclRsmSded, pclDbsDat, ilFld);
			        ilFld = get_item_no(pclRcvFld, "SDES", 5) + 1;
			        (void) get_real_item(pclUsdes, pclDbsDat, ilFld);
			        ilFld = get_item_no(pclRcvFld, "SDEE", 5) + 1;
			        (void) get_real_item(pclUsdee, pclDbsDat, ilFld);
			        
			        if (strcmp( pclRSMFld, "SDST")==0)
				{/* Schedule Start */ 
					strcpy( pclRsmSdst, pcpCons );
					strcpy( pclUsdes, pcpUsde );
				}
				else if (strcmp( pclRSMFld, "SDED" )==0)
				{/* Schedule End */ 
					strcpy( pclRsmSded, pcpCons );
					strcpy( pclUsdee, pcpUsde );
				}
			
				sprintf(pclSqlKey,"WHERE URNO=%s",pclRsmUrno);
				strcpy( pclRcvFld,"URNO,HOPO,RURN,FLNO,ADID,STDT,BUSN,TSKN,SDST,SDED,MONI,USRH,UHSS,SDES,SDEE" );
				sprintf( pclRcvDat, "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
				        	pclRsmUrno, cgHopo, pcpUaft, pclFlno, pclAdid, 
				        	pclStdt, pclBusn, pclTskn,  pclRsmSdst, pclRsmSded, 
				        	pclMoni, pcpUsrh, pcpUhss, pclUsdes, pclUsdee );
			        TrimAndFilterCr(pclRcvDat, ",", "\n");
			        dbg(TRACE, "UPDATE RSM Tab: SelKey  <%s>", pclSqlKey);
			        dbg(DEBUG, "       RSM Tab: Fields  <%s>", pclRcvFld);
			        dbg(DEBUG, "       RSM Tab: NewData <%s>", pclRcvDat);
			        dbg(DEBUG, "       RSM Tab: OldData <%s>", pclDbsDat);
			
			        ilRc = HandleGocUpdate(pclTable, pclSqlKey, pclRcvFld, pclRcvDat, pclDbsDat, pclNewFld, pclNewDat);
			        if (pclNewFld[0]!='\0')
				{
			        	dbg(TRACE, "UPDATED: Fields <%s>", pclNewFld);
			        	dbg(TRACE, "         Data   <%s>", pclNewDat);
/* BERNI BC */
					strcat(pclNewFld,",URNO");
					strcat(pclNewDat,",");
					strcat(pclNewDat,pclRsmUrno);
					ilRc = tools_send_info_flag(1900, 0, "BCHDL", "STAHDL", "STAHDL", "", 
						"", "", cgTwEnd, "URT","RSMTAB",pclSqlKey, pclNewFld, pclNewDat, 0);          
				}
				else
				{
					dbg(TRACE,"       NOTHING CHANGED");
					blUpd = FALSE;/* No changes.  */ 
				}
			    }
			    else
			    {/* no such record in RSMTAB. Need to insert a new record */ 
				if (ilRc = GetNextValues(pclRsmUrno, 1)!=RC_SUCCESS)
				{
					dbg( TRACE, "ProcessCONSChanges: Fail to get Urno.");
				}
			    	if (ilRc==RC_SUCCESS)
			    	{
				    	if (strcmp( pclRSMFld, "SDST")==0)
					{/* Schedule Start */ 
						strcpy( pclRsmSdst, pcpCons );
						strcpy( pclUsdes, pcpUsde );
					}
					else if (strcmp( pclRSMFld, "SDED" )==0)
					{/* Schedule End */ 
						strcpy( pclRsmSded, pcpCons );
						strcpy( pclUsdee, pcpUsde );
			 		}
				        
				        strcpy( pclRcvFld,"URNO,HOPO,RURN,FLNO,ADID,STDT,BUSN,TSKN,SDST,SDED,MONI,USRH,UHSS,SDES,SDEE" );
				        sprintf( pclRcvDat, "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
				        	pclRsmUrno, cgHopo, pcpUaft, pclFlno, pclAdid, 
				        	pclStdt, pclBusn, pclTskn,  pclRsmSdst, pclRsmSded, 
				        	pclMoni, pcpUsrh, pcpUhss, pclUsdes, pclUsdee );
						dbg( TRACE, "INSERT RSM Tab: <%s>", pclRcvFld);
						dbg( TRACE, "       RSM Tab: <%s>", pclRcvDat);
				        ilRc = HandleGocInsert(pclTable, pclRcvFld, pclRcvDat);
			        }
			    }
		    }
	    }
	}
	if (ilRc==RC_SUCCESS)
	{		
		if (blUpd)
		{
			dbg( DEBUG, "ProcessCONSChanges: Success <%s>, <%s>, <%s>, <%s>, <%s>, <%s>, <%s>, <%s>" , 
				pcpUOst, pcpUaft, pcpUsde, pcpUsrh, pcpUhss, 
	            pcpCons, pclTskn, pcpRety );
        	}
	}
	else
	{
		dbg( TRACE, "ProcessCONSChanges: Fail <%s>, <%s>, <%s>, <%s>, <%s>, <%s>, <%s>, <%s>" , 
			pcpUOst, pcpUaft, pcpUsde, pcpUsrh, pcpUhss, 
            pcpCons, pclTskn, pcpRety );
	}

        dbg(TRACE,"---->>> HANDLE OSTTAB CONS UPDATES (-END-): ADJUST RELATED RSMTAB RECORDS");   	

   	return ilRc;	
}

static int GetFlightInfo( char *pcpUaft, char *pcpFlno, char *pcpAdid, char *pcpFlDt )
{
	int ilRc = RC_SUCCESS;	
	if (IS_EMPTY(pcpUaft) || !pcpFlno || !pcpAdid || !pcpFlDt )
  	{
	  	dbg( TRACE, "GetFlightInfo: Empty one or more Data." );
		ilRc = RC_FAIL;
  	}
  	
  	char *pclFlightRow = 0;
	long llRow;
	char clKey[32] = "";
  	
  	pcpFlno[0] = pcpAdid[0] = pcpFlDt[0] = '\0';/* Init return values */ 
  	
	if (ilRc == RC_SUCCESS)
	{
		sprintf ( clKey, "%s", pcpUaft );
		llRow = ARR_FIRST;
		if (CEDAArrayFindRowPointer(&(rgAftArray.rrArrayHandle),
						rgAftArray.crArrayName,
						&(rgAftArray.rrIdxHandle[0]),
						rgAftArray.crIdxName[0], clKey, 
						&llRow, (void *)&pclFlightRow ) == RC_SUCCESS)
		{
			strcpy( pcpFlno, AFTFIELD( pclFlightRow, igAftFlnoIdx ));
			strcpy( pcpAdid, AFTFIELD( pclFlightRow, igAftAdidIdx ));
			if (strcmp( pcpAdid, "A" )==0)
			{
				strcpy( pcpFlDt, AFTFIELD( pclFlightRow, igAftStoaIdx ));
			}
			else if (strcmp( pcpAdid, "D" )==0)
			{
				strcpy( pcpFlDt, AFTFIELD( pclFlightRow, igAftStodIdx ));
			} 
		}
	}

	dbg( TRACE, "GetFlightInfo: UAFT <%s>, FLNO <%s>, ADID <%s>, FLDT <%s>", pcpUaft, pcpFlno, pcpAdid, pcpFlDt );
	return ilRc;
}


static int GetRuleName( char *pcpUsrh, char *pcpRuleName )
{
	int ilRc = RC_SUCCESS;	
	if (IS_EMPTY(pcpUsrh) || !pcpRuleName )
  	{
	  	dbg( TRACE, "GetRuleName: Empty one or more Data." );
		ilRc = RC_FAIL;
  	}
  	
  	char *pclDataRow = 0;
	long llRow;
	char clKey[32] = "";
  	
  	pcpRuleName[0] = '\0';/* Init return values */ 
  	
	if (ilRc == RC_SUCCESS)
	{
		sprintf ( clKey, "%s", pcpUsrh );
		llRow = ARR_FIRST;
		if (CEDAArrayFindRowPointer(&(rgSrhArray.rrArrayHandle),
						rgSrhArray.crArrayName,
						&(rgSrhArray.rrIdxHandle[0]),
						rgSrhArray.crIdxName[0], clKey, 
						&llRow, (void *)&pclDataRow ) == RC_SUCCESS)
		{
			strcpy( pcpRuleName, SRHFIELD( pclDataRow, igSrhNameIdx ));
		}
	}

	dbg( TRACE, "GetRuleName: USRH <%s>, RuleName <%s>", pcpUsrh, pcpRuleName );
	return ilRc;
}


static int GetSectionName( char *pcpUhss, char *pcpName )
{
	int ilRc = RC_SUCCESS;	
	if (IS_EMPTY(pcpUhss) || !pcpName )
  	{
	  	dbg( TRACE, "GetSectionName: Empty one or more Data." );
		ilRc = RC_FAIL;
  	}
  	
  	char *pclDataRow = 0;
	long llRow;
	char clKey[32] = "";
  	
  	pcpName[0] = '\0';/* Init return values */ 
  	
	if (ilRc == RC_SUCCESS)
	{
		sprintf ( clKey, "%s", pcpUhss );
		llRow = ARR_FIRST;
		if (CEDAArrayFindRowPointer(&(rgHssArray.rrArrayHandle),
						rgHssArray.crArrayName,
						&(rgHssArray.rrIdxHandle[0]),
						rgHssArray.crIdxName[0], clKey, 
						&llRow, (void *)&pclDataRow ) == RC_SUCCESS)
		{
			strcpy( pcpName, HSSFIELD( pclDataRow, igHssNameIdx ));
		}
	}
	return ilRc;
}

static int UpdateScheduleTimesForRSMTab(char *pcpUaft, char *pcpFlno, char *pcpFlDt, char *pcpAdid, 
    char *pcpRuleName, char *pcpTskn, char* pcpRsmFld, char *pcpNewVal )
{
    int ilRc = RC_SUCCESS;
    const int ilInternalUpdInd = 0;
    char pclFld[256] = "";
    char pclData[512] = "";
    char pclRecvName[128] = "";
    
  	if (IS_EMPTY(pcpUaft) || IS_EMPTY(pcpFlno) || IS_EMPTY(pcpFlDt) || IS_EMPTY(pcpAdid)
  	    || !pcpRuleName || IS_EMPTY(pcpTskn) || IS_EMPTY( pcpRsmFld) || !pcpNewVal )
  	{
	  	dbg( TRACE, "SendScheduleTimesForRSMTab: Empty one or more Data." );
		ilRc = RC_FAIL;
  	}
  
    dbg(TRACE,"UPDATE SCHEDULE TIMES FOR RSMTAB");
	
  	if ( (ilRc == RC_SUCCESS) && (!IS_EMPTY(pcpRsmFld)))
  	{
  		sprintf( pclFld, "HOPO,RURN,FLNO,ADID,STDT,TSKN,BUNM,IEFL,%s", pcpRsmFld );
  		sprintf( pclData, "%s,%s,%s,%s,%s,%s,%s,%d%s", cgHopo, pcpUaft
  		        , pcpFlno, pcpAdid, pcpFlDt
  				, pcpTskn, pcpRuleName, ilInternalUpdInd, pcpNewVal );
 		char pclCommand[32] = "MSGI";
		char pclTableName[32] = "RSM";
		char pclFields[32] = "ICON";
		char pclDestName[32] = "STAHDL";
		char pclWhere[256] = "";
	 
	        ilRc = tools_send_info_flag(RSM_SAVE_PROC_ID,0, pclDestName, cgProcessName, pclRecvName, 
	    							"", "", "", cgTwEnd,
								  	pclCommand, pclTableName, pclWhere, pclFld, pclData, NETOUT_NO_ACK); 				
	}
  	return ilRc;
}



static void TrimAndFilterCr(char *pclTextBuff,char *pcpFldSep, char *pcpRecSep)
{
        char *pclDest = NULL;
        char *pclSrc = NULL;
        char *pclLast = NULL; /* last non blank byte position */
        pclDest = pclTextBuff;
        pclSrc  = pclTextBuff;
        pclLast  = pclTextBuff - 1;
        while (*pclSrc != '\0')
        {
                if (*pclSrc != '\r')
                {
                        if ((*pclSrc == pcpFldSep[0]) || (*pclSrc == pcpRecSep[0]))
                        {
                                pclDest = pclLast + 1;
                        } /* end if */
                        *pclDest = *pclSrc;
                        if (*pclDest != ' ')
                        {
                                pclLast = pclDest;
                        } /* end if */
                        pclDest++;
                } /* end if */
                pclSrc++;
        } /* end while */
        pclDest = pclLast + 1;
        *pclDest = '\0';
  return;
} /* end TrimAndFilterCr */

