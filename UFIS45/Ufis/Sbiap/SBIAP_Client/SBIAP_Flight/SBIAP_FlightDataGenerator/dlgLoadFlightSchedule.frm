VERSION 5.00
Begin VB.Form dlgLoadFlightSchedule 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Load Flight Schedule..."
   ClientHeight    =   3855
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   3510
   Icon            =   "dlgLoadFlightSchedule.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3855
   ScaleWidth      =   3510
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame2 
      Caption         =   "Flight selection"
      Height          =   1095
      Left            =   240
      TabIndex        =   9
      Top             =   1920
      Width           =   3015
      Begin VB.TextBox txtFlnoLike 
         Height          =   285
         Left            =   240
         TabIndex        =   11
         ToolTipText     =   "Add additional SQL-statements, e.g. ""FLNO LIKE 'TG%'"" (without quotation marks)"
         Top             =   600
         Width           =   2535
      End
      Begin VB.Label Label3 
         Caption         =   "SQL: ...AND..."
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   360
         Width           =   2415
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Time frame (local times)"
      Height          =   1455
      Left            =   240
      TabIndex        =   2
      Top             =   240
      Width           =   3015
      Begin VB.TextBox txtToTime 
         Height          =   285
         Left            =   2040
         TabIndex        =   8
         Text            =   "16:00"
         Top             =   840
         Width           =   615
      End
      Begin VB.TextBox txtToDate 
         Height          =   285
         Left            =   960
         TabIndex        =   7
         Text            =   "01.12.2005"
         Top             =   840
         Width           =   975
      End
      Begin VB.TextBox txtFromTime 
         Height          =   285
         Left            =   2040
         TabIndex        =   5
         Text            =   "08:00"
         Top             =   480
         Width           =   615
      End
      Begin VB.TextBox txtFromDate 
         Height          =   285
         Left            =   960
         TabIndex        =   4
         Text            =   "01.12.2005"
         Top             =   480
         Width           =   975
      End
      Begin VB.Label Label2 
         Caption         =   "To:"
         Height          =   255
         Left            =   240
         TabIndex        =   6
         Top             =   855
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "From:"
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   495
         Width           =   615
      End
   End
   Begin VB.CommandButton CancelButton 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   375
      Left            =   1868
      TabIndex        =   1
      Top             =   3240
      Width           =   1215
   End
   Begin VB.CommandButton OKButton 
      Caption         =   "OK"
      Height          =   375
      Left            =   428
      TabIndex        =   0
      Top             =   3240
      Width           =   1215
   End
End
Attribute VB_Name = "dlgLoadFlightSchedule"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public olDateFrom As Date
Public olDateTo As Date
Public olFlnoLike As String
Public bCancel As Boolean

Private Sub CancelButton_Click()
    bCancel = True
    Me.Hide
End Sub

Private Sub Form_Activate()
    bCancel = True
End Sub

Private Sub Form_Load()
    Dim olDate As Date
    olDate = Now
    txtFromDate.Text = Format(Now, "dd.mm.yyyy")
    txtToDate.Text = Format(Now, "dd.mm.yyyy")
    txtFromDate.BackColor = vbYellow
    txtToDate.BackColor = vbYellow
    txtFromTime.BackColor = vbYellow
    txtToTime.BackColor = vbYellow
    bCancel = True
End Sub

Private Sub OKButton_Click()
    On Error GoTo err
    Dim strFrom As String
    Dim strTo As String
    Dim strMessage As String

    Dim blInputOk As Boolean
    blInputOk = True

    ' From
    strMessage = "From Date not valid! Please correct the input."
    strFrom = txtFromDate.Text
    olDateFrom = DateSerial(GetItem(strFrom, 3, "."), GetItem(strFrom, 2, "."), GetItem(strFrom, 1, "."))

    strMessage = "From Time not valid! Please correct the input."
    strFrom = txtFromTime.Text
    olDateFrom = olDateFrom + TimeSerial(GetItem(strFrom, 1, ":"), GetItem(strFrom, 2, ":"), 0)

    ' To
    strMessage = "To Date not valid! Please correct the input."
    strTo = txtToDate.Text
    olDateTo = DateSerial(GetItem(strTo, 3, "."), GetItem(strTo, 2, "."), GetItem(strTo, 1, "."))

    strMessage = "To Time not valid! Please correct the input."
    strTo = txtToTime.Text
    olDateTo = olDateTo + TimeSerial(GetItem(strTo, 1, ":"), GetItem(strTo, 2, ":"), 0)

    If Len(txtFlnoLike.Text) > 0 Then
'        strMessage = "LIKE-statement not valid. Example: TG%"
'        If InStr(txtFlnoLike.Text, "%") < 1 Then Error (0)
        olFlnoLike = txtFlnoLike.Text
    Else
        olFlnoLike = ""
    End If

    If blInputOk = True Then
        bCancel = False
        Me.Hide
    End If
    Exit Sub
err:
    MsgBox strMessage
    blInputOk = False
    Resume Next
End Sub
