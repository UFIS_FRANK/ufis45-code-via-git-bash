VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.ocx"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{7BDE1363-DF9A-4D96-A2A0-4E85E0191F0F}#1.0#0"; "AATLogin.ocx"
Object = "{A2F31E92-C74F-11D3-A251-00500437F607}#1.0#0"; "UfisCom.ocx"
Begin VB.Form frmMain 
   Caption         =   "UFIS� Flight Data Generator"
   ClientHeight    =   11385
   ClientLeft      =   165
   ClientTop       =   855
   ClientWidth     =   13050
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   11385
   ScaleWidth      =   13050
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer TimerEvents 
      Enabled         =   0   'False
      Interval        =   100
      Left            =   6120
      Top             =   10080
   End
   Begin UFISCOMLib.UfisCom UfisComCtrl 
      Left            =   9240
      Top             =   480
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   1296
      _StockProps     =   0
   End
   Begin AATLOGINLib.AatLogin LoginCtrl 
      Height          =   615
      Left            =   9960
      TabIndex        =   11
      Top             =   480
      Visible         =   0   'False
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   1085
      _StockProps     =   0
   End
   Begin VB.Frame Frame2 
      Caption         =   "Sent Events"
      Height          =   4455
      Left            =   120
      TabIndex        =   3
      Top             =   5280
      Width           =   12735
      Begin VB.CommandButton cmdSent 
         Caption         =   "&Resend"
         Height          =   375
         Index           =   1
         Left            =   11160
         TabIndex        =   13
         Tag             =   "1043"
         Top             =   840
         Width           =   1095
      End
      Begin VB.CheckBox chkLockUpdates 
         Caption         =   "&Lock"
         Height          =   255
         Left            =   11160
         TabIndex        =   10
         Tag             =   "1044"
         Top             =   1440
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.CommandButton cmdSent 
         Caption         =   "&View"
         Height          =   375
         Index           =   0
         Left            =   11160
         TabIndex        =   9
         Tag             =   "1043"
         Top             =   360
         Width           =   1095
      End
      Begin TABLib.TAB TAB_SentEvents 
         Height          =   3855
         Left            =   240
         TabIndex        =   5
         Top             =   360
         Width           =   10695
         _Version        =   65536
         _ExtentX        =   18865
         _ExtentY        =   6800
         _StockProps     =   64
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Upcoming Events"
      Height          =   4455
      Left            =   120
      TabIndex        =   2
      Top             =   600
      Width           =   12735
      Begin VB.CommandButton cmdEditEvents 
         Caption         =   "&NOW!"
         Height          =   375
         Index           =   3
         Left            =   11160
         TabIndex        =   12
         Tag             =   "1042"
         Top             =   1800
         Width           =   1095
      End
      Begin VB.CommandButton cmdEditEvents 
         Caption         =   "&Delete"
         Height          =   375
         Index           =   2
         Left            =   11160
         TabIndex        =   8
         Tag             =   "1042"
         Top             =   1320
         Width           =   1095
      End
      Begin VB.CommandButton cmdEditEvents 
         Caption         =   "&Edit"
         Height          =   375
         Index           =   1
         Left            =   11160
         TabIndex        =   7
         Tag             =   "1041"
         Top             =   840
         Width           =   1095
      End
      Begin VB.CommandButton cmdEditEvents 
         Caption         =   "&Insert"
         Height          =   375
         Index           =   0
         Left            =   11160
         TabIndex        =   6
         Tag             =   "1040"
         Top             =   360
         Width           =   1095
      End
      Begin TABLib.TAB TAB_UpcomingEvents 
         Height          =   3855
         Left            =   240
         TabIndex        =   4
         Top             =   360
         Width           =   10695
         _Version        =   65536
         _ExtentX        =   18865
         _ExtentY        =   6800
         _StockProps     =   64
      End
   End
   Begin MSComctlLib.Toolbar tbToolBar 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   13050
      _ExtentX        =   23019
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      ImageList       =   "imlToolbarIcons"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   11
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "New"
            Object.ToolTipText     =   "New"
            ImageKey        =   "New"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Open"
            Object.ToolTipText     =   "Open"
            ImageKey        =   "Open"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Save"
            Object.ToolTipText     =   "Save"
            ImageKey        =   "Save"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "LoadFlightSchedule"
            Object.ToolTipText     =   "Load Flight Schedule"
            ImageKey        =   "LoadSchedule"
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "GenerateRandomEvents"
            Object.ToolTipText     =   "Generate Random Events"
            ImageKey        =   "GenerateEvents"
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "StartSendingEvents"
            Object.ToolTipText     =   "Start sending of Events"
            ImageKey        =   "Start"
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "StopSendingEvents"
            Object.ToolTipText     =   "Stop Sending of Events"
            ImageKey        =   "Stop"
            Value           =   1
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "UTC_local"
            Object.ToolTipText     =   "Switch between UTC/local display"
            ImageKey        =   "UTC"
            Style           =   1
            Value           =   1
         EndProperty
      EndProperty
      Begin VB.Timer TimerSeconds 
         Interval        =   1000
         Left            =   9240
         Top             =   18840
      End
   End
   Begin MSComctlLib.StatusBar sbStatusBar 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   0
      Top             =   11115
      Width           =   13050
      _ExtentX        =   23019
      _ExtentY        =   476
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   17357
            Text            =   "Status"
            TextSave        =   "Status"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            AutoSize        =   2
            TextSave        =   "20/04/2006"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
         EndProperty
      EndProperty
   End
   Begin MSComDlg.CommonDialog dlgCommonDialog 
      Left            =   11160
      Top             =   480
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Left            =   10560
      Top             =   360
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   9
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":030A
            Key             =   "New"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":041C
            Key             =   "Open"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":052E
            Key             =   "Save"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0640
            Key             =   "Print"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0752
            Key             =   "Start"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":102C
            Key             =   "Stop"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":1906
            Key             =   "GenerateEvents"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":21E0
            Key             =   "LoadSchedule"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":2ABA
            Key             =   "UTC"
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNew 
         Caption         =   "&New"
         Shortcut        =   ^N
      End
      Begin VB.Menu mnuFileOpen 
         Caption         =   "&Open..."
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
      End
      Begin VB.Menu mnuFileBar1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuToolsStartSendingEvents 
         Caption         =   "Start Sending Events"
      End
      Begin VB.Menu mnuToolsStopSendingEvents 
         Caption         =   "Stop Sending Events"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuToolsBar0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsLoadFlightSchedule 
         Caption         =   "Load Flight Schedule"
      End
      Begin VB.Menu mnuToolsGenerateRandomEvents 
         Caption         =   "Generate Random Events"
      End
      Begin VB.Menu mnuToolsBar1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsUTC 
         Caption         =   "UTC Times"
         Checked         =   -1  'True
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About..."
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private iLastSecond As Integer
Private iEventsCount As Integer
Private iEventsPerSecond As Integer
Private sTimeColumns As String
Private bmScenarioSaved As Boolean
Private bmTimerRunning As Boolean

Private Sub cmdEditEvents_Click(Index As Integer)
    Dim llCurrentSelected As Long

    'If bmTimerRunning = True Then Exit Sub

    ' for edit, delete and now! check, if a line was selected
    llCurrentSelected = TAB_UpcomingEvents.GetCurrentSelected
    If Index > 0 Then
        If llCurrentSelected < 0 Then
            MsgBox "Please select an event!", vbInformation, "No selection"
            Exit Sub
        End If
    End If

    frmData.LoadBasicData

    Select Case Index
        Case 0: 'insert
            frmEvent.Visible = False
            frmEvent.SetMode "INSERT", ""
            frmEvent.ResetFields
            frmEvent.Show vbModal
            Dim strFields As String
            Dim strData As String
            Dim strKeyItems As String
            strKeyItems = frmEvent.GetValues
            If Len(strKeyItems) > 0 Then
                GetKeyItem strFields, strKeyItems, "{=FIELDS=}", "{="
                GetKeyItem strData, strKeyItems, "{=DATA=}", "{="
                TAB_UpcomingEvents.InsertTextLine "", False
                TAB_UpcomingEvents.SetFieldValues TAB_UpcomingEvents.GetLineCount - 1, strFields, strData
                GenerateEventTags TAB_UpcomingEvents.GetLineCount - 1
            End If
        Case 1: 'edit
            TAB_UpcomingEvents_SendLButtonDblClick llCurrentSelected, -1
        Case 2: 'delete
            TAB_UpcomingEvents.DeleteLine llCurrentSelected
            TAB_UpcomingEvents.SetCurrentSelection -1
        Case Else: 'NOW!
            TAB_SentEvents.InsertTextLineAt 0, TAB_UpcomingEvents.GetLineValues(llCurrentSelected), False
            TAB_SentEvents.SetLineTag 0, TAB_UpcomingEvents.GetLineTag(llCurrentSelected)
            SendEvent llCurrentSelected, TAB_UpcomingEvents.GetColumnValue(llCurrentSelected, 2)
            TAB_UpcomingEvents.DeleteLine llCurrentSelected
            TAB_SentEvents.RedrawTab
    End Select
    bmScenarioSaved = False
    If TAB_UpcomingEvents.CurrentSortColumn > -1 Then
        TAB_UpcomingEvents.Sort "0,1", True, True
        TAB_UpcomingEvents.Sort "0", True, True
    End If
    TAB_UpcomingEvents.AutoSizeColumns
    TAB_SentEvents.HeaderLengthString = TAB_UpcomingEvents.HeaderLengthString
    TAB_UpcomingEvents.RedrawTab
    TAB_SentEvents.RedrawTab
End Sub

Private Sub cmdSent_Click(Index As Integer)
    Dim llCurrentSelected As Long

    'If bmTimerRunning = True Then Exit Sub

    ' for adit, delete and now! check, if a line was selected
    llCurrentSelected = TAB_SentEvents.GetCurrentSelected
    If Index > 0 Then
        If llCurrentSelected < 0 Then
            MsgBox "Please select an event!", vbInformation, "No selection"
            Exit Sub
        End If
    End If

    Select Case Index
        Case 0: 'view
            TAB_SentEvents_SendLButtonDblClick llCurrentSelected, 0
        Case 1: 'resend
            Dim strLine As String
            Dim strLineTag As String
            Dim llLineNo As Long
            strLine = TAB_SentEvents.GetLineValues(llCurrentSelected)
            strLineTag = TAB_SentEvents.GetLineTag(llCurrentSelected)
            llLineNo = TAB_UpcomingEvents.GetLineCount

            TAB_UpcomingEvents.InsertTextLine strLine, False
            TAB_UpcomingEvents.SetLineTag llLineNo, strLineTag
            TAB_SentEvents.InsertTextLineAt 0, strLine, False
            TAB_SentEvents.SetLineTag 0, strLineTag
            SendEvent llLineNo, TAB_UpcomingEvents.GetColumnValue(llLineNo, 2)
            TAB_UpcomingEvents.DeleteLine llLineNo
    End Select

    TAB_UpcomingEvents.RedrawTab
    TAB_SentEvents.RedrawTab
End Sub

Private Sub Form_Load()
    Dim strDisplayTime As String

    bmScenarioSaved = False
    bmTimerRunning = False
    sExcelSeparator = GetIniEntry(sConfigFile, gsAppName, "", "EXCELSEPARATOR", ",")

    Load frmData
    InitTab TAB_UpcomingEvents
    InitTab TAB_SentEvents

    strDisplayTime = GetIniEntry(sConfigFile, gsAppName, "", "DISPLAY_TIME", "UTC")
    If strDisplayTime <> "UTC" Then mnuToolsUTC_Click
    Me.Left = GetSetting(App.Title, "Settings", "MainLeft", 1000)
    Me.Top = GetSetting(App.Title, "Settings", "MainTop", 1000)
    Me.Width = GetSetting(App.Title, "Settings", "MainWidth", 6500)
    TimerSeconds.Enabled = True
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If MsgBox("Do you really want to exit the application?", vbOKCancel, "Exit Application?") = vbCancel Then
        Cancel = True
    Else
        Form_Unload 0
    End If
End Sub

Private Sub Form_Resize()
    Dim ilTmp As Integer
    Dim i As Integer

    ' frames height & top position
    ilTmp = (frmMain.Height - 1800) / 2
    If ilTmp > 0 Then
        Frame1.Height = ilTmp
    Else
        Frame1.Height = 0
    End If
    Frame2.Height = Frame1.Height
    Frame2.Top = Frame1.Top + Frame1.Height + 100

    'TABs height & top position
    ilTmp = Frame1.Height - 500
    If ilTmp > 0 Then
        TAB_UpcomingEvents.Height = ilTmp
    Else
        TAB_UpcomingEvents.Height = 0
    End If
    TAB_SentEvents.Height = TAB_UpcomingEvents.Height

    ' width of frames
    ilTmp = frmMain.Width - 350
    If ilTmp > 0 Then
        Frame1.Width = ilTmp
    Else
        Frame1.Width = 0
    End If
    Frame2.Width = Frame1.Width

    ' width of TABs
    ilTmp = Frame1.Width - 1600
    If ilTmp > 0 Then
        TAB_UpcomingEvents.Width = ilTmp
    Else
        TAB_UpcomingEvents.Width = 0
    End If
    TAB_SentEvents.Width = TAB_UpcomingEvents.Width

    ' left position of the buttons
    ilTmp = Frame1.Left + TAB_UpcomingEvents.Width + 250
    For i = 0 To 3
        cmdEditEvents(i).Left = ilTmp
    Next
    cmdSent(0).Left = ilTmp
    cmdSent(1).Left = ilTmp
    chkLockUpdates.Left = ilTmp
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim i As Integer

    'close all sub forms
    For i = Forms.count - 1 To 1 Step -1
        Unload Forms(i)
    Next
    If Me.WindowState <> vbMinimized Then
        SaveSetting App.Title, "Settings", "MainLeft", Me.Left
        SaveSetting App.Title, "Settings", "MainTop", Me.Top
        SaveSetting App.Title, "Settings", "MainWidth", Me.Width
        SaveSetting App.Title, "Settings", "MainHeight", Me.Height
    End If
    End
End Sub

Private Sub mnuToolsUTC_Click()
    Dim slUTCoffsetMinutes As Single
    Dim ilTabColumn As Integer
    Dim i As Integer
    Dim strColumnName As String

    If mnuToolsUTC.Checked = True Then
        mnuToolsUTC.Checked = False
        tbToolBar.Buttons(11).Value = tbrUnpressed
        slUTCoffsetMinutes = sglUTCOffsetHours * 60
    Else
        mnuToolsUTC.Checked = True
        tbToolBar.Buttons(11).Value = tbrPressed
        slUTCoffsetMinutes = 0
    End If

    For i = 1 To ItemCount(sTimeColumns, ",")
        strColumnName = GetItem(sTimeColumns, i, ",")
        ilTabColumn = GetRealItemNo(TAB_UpcomingEvents.HeaderString, strColumnName)
        TAB_UpcomingEvents.DateTimeSetUTCOffsetMinutes ilTabColumn, slUTCoffsetMinutes
        TAB_SentEvents.DateTimeSetUTCOffsetMinutes ilTabColumn, slUTCoffsetMinutes
    Next
    TAB_UpcomingEvents.RedrawTab
    TAB_SentEvents.RedrawTab
End Sub
Private Sub InitEventForm(ByVal LineNo As Long, ByVal ColNo As Long, ByRef rTab As TABLib.Tab, sMode As String)
    If LineNo > -1 Then
        Dim strSTOX As String
        Dim strETOX As String
        Dim strOXBL As String
        Dim strLANDAIRB As String
        Dim strRes1 As String
        Dim strRes2 As String
        Dim strGate1 As String
        Dim strGate2 As String
        Dim strPosition As String
        Dim strEventType As String
        Dim strTmp As String

        frmEvent.Visible = False
        strEventType = rTab.GetColumnValue(LineNo, 2)
        frmEvent.SetMode sMode, strEventType
        frmEvent.SetTimestamp rTab.GetColumnValue(LineNo, 0), rTab.GetColumnValue(LineNo, 1)
        If rTab.GetFieldValue(LineNo, "ADID") = "A" Then
            strSTOX = rTab.GetFieldValue(LineNo, "STOA")
            strETOX = rTab.GetFieldValue(LineNo, "ETAA")
            strOXBL = rTab.GetFieldValue(LineNo, "ONBD")
            strLANDAIRB = rTab.GetFieldValue(LineNo, "LNDA")
            strRes1 = rTab.GetFieldValue(LineNo, "BLT1")
            strRes2 = rTab.GetFieldValue(LineNo, "BLT2")
            strGate1 = rTab.GetFieldValue(LineNo, "GTA1")
            strGate2 = rTab.GetFieldValue(LineNo, "GTA2")
            strPosition = rTab.GetFieldValue(LineNo, "PSTA")
            frmEvent.SetADID "A"
        Else
            strSTOX = rTab.GetFieldValue(LineNo, "STOD")
            strETOX = rTab.GetFieldValue(LineNo, "ETDA")
            strOXBL = rTab.GetFieldValue(LineNo, "OFBD")
            strLANDAIRB = rTab.GetFieldValue(LineNo, "AIRA")
            strRes1 = rTab.GetFieldValue(LineNo, "CKIF")
            strRes2 = rTab.GetFieldValue(LineNo, "CKIT")
            strGate1 = rTab.GetFieldValue(LineNo, "GTD1")
            strGate2 = rTab.GetFieldValue(LineNo, "GTD2")
            strPosition = rTab.GetFieldValue(LineNo, "PSTD")
            frmEvent.SetADID "D"
        End If

        ' special handling for actual time events
        If UCase(Right(strEventType, 4)) = "CIC TIME" Then
            frmEvent.SetResourcesTimes TAB_UpcomingEvents.GetFieldValue(LineNo, "ETAA"), _
                TAB_UpcomingEvents.GetFieldValue(LineNo, "ETDA"), "", ""
            strETOX = ""
        End If
        If UCase(Right(strEventType, 4)) = "GAT TIME" Then
            frmEvent.SetResourcesTimes TAB_UpcomingEvents.GetFieldValue(LineNo, "ETAA"), _
                TAB_UpcomingEvents.GetFieldValue(LineNo, "ETDA"), _
                TAB_UpcomingEvents.GetFieldValue(LineNo, "AIRA"), _
                TAB_UpcomingEvents.GetFieldValue(LineNo, "LNDA")
            strETOX = ""
            strLANDAIRB = ""
        End If

        frmEvent.SetFlightTimes strSTOX, strETOX, strOXBL, strLANDAIRB
        frmEvent.SetStatus rTab.GetFieldValue(LineNo, "FTYP")
        frmEvent.SetTTYP rTab.GetFieldValue(LineNo, "TTYP")
        frmEvent.SetRegistration rTab.GetFieldValue(LineNo, "REGN")
        frmEvent.SetResources strRes1, strRes2, strPosition, strGate1, strGate2
        frmEvent.SetFlightData rTab.GetFieldValue(LineNo, "FLNO"), rTab.GetFieldValue(LineNo, "ORG3"), _
            rTab.GetFieldValue(LineNo, "DES3"), rTab.GetFieldValue(LineNo, "DIVR")
    End If
End Sub

Private Sub TAB_SentEvents_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    If bmTimerRunning = True Then Exit Sub

    InitEventForm LineNo, ColNo, TAB_SentEvents, "VIEW"
    frmEvent.Show vbModal
End Sub

Private Sub TAB_UpcomingEvents_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    'If bmTimerRunning = True Then Exit Sub

    If bmTimerRunning = True Then
        InitEventForm LineNo, ColNo, TAB_UpcomingEvents, "VIEW"
    Else
        InitEventForm LineNo, ColNo, TAB_UpcomingEvents, "EDIT"
    End If
    frmEvent.Show vbModal
    Dim strFields As String
    Dim strData As String
    Dim strKeyItems As String
    strKeyItems = frmEvent.GetValues
    If Len(strKeyItems) > 0 Then
        GetKeyItem strFields, strKeyItems, "{=FIELDS=}", "{="
        GetKeyItem strData, strKeyItems, "{=DATA=}", "{="
        TAB_UpcomingEvents.SetFieldValues LineNo, strFields, strData
        GenerateEventTags LineNo
    End If
End Sub

Private Sub tbToolBar_ButtonClick(ByVal Button As MSComctlLib.Button)
    On Error Resume Next
    Select Case Button.Key
        Case "New"
            mnuFileNew_Click
        Case "Open"
            mnuFileOpen_Click
        Case "Save"
            mnuFileSave_Click
        Case "LoadFlightSchedule"
            mnuToolsLoadFlightSchedule_Click
        Case "GenerateRandomEvents"
            mnuToolsGenerateRandomEvents_Click
        Case "StartSendingEvents"
            mnuToolsStartSendingEvents_Click
        Case "StopSendingEvents"
            mnuToolsStopSendingEvents_Click
        Case "UTC_local"
            mnuToolsUTC_Click
    End Select
End Sub

Private Sub mnuHelpAbout_Click()
    frmAbout.Show vbModal, Me
End Sub

Private Sub mnuToolsLoadFlightSchedule_Click()
    If gbOffline = False Then
        dlgLoadFlightSchedule.Show (vbModal)
        If dlgLoadFlightSchedule.bCancel = False Then
            frmData.LoadAFT dlgLoadFlightSchedule.olDateFrom, dlgLoadFlightSchedule.olDateTo, dlgLoadFlightSchedule.olFlnoLike
        End If
    Else
        MsgBox "The application was started in OFFLINE mode." & _
        vbCrLf & "(second command parameter)" & vbCrLf & "Reading data from files.", vbInformation, "OFFLINE mode"
        frmData.cmdReadFromFiles_Click
    End If
End Sub

Private Sub mnuToolsGenerateRandomEvents_Click()
    If frmData.TabAFT.GetLineCount < 1 Then
        MsgBox "No flights loaded. Please load the flight schedule first.", vbInformation, "No flights"
    End If

    mdlEvents.GenerateRandomEvents

    TAB_UpcomingEvents.AutoSizeColumns
    TAB_SentEvents.HeaderLengthString = TAB_UpcomingEvents.HeaderLengthString
    TAB_UpcomingEvents.RedrawTab
    TAB_SentEvents.RedrawTab
End Sub

Private Sub mnuFileExit_Click()
    'unload the form
    Unload Me
End Sub

Private Sub mnuFileSave_Click()
    dlgCommonDialog.CancelError = True
    dlgCommonDialog.DefaultExt = "*.csv"
    dlgCommonDialog.FileName = "Scenario_" & Format(Now, "YYYYMMDD_hhmm") & ".csv"
    dlgCommonDialog.Filter = "Text files (*.csv;*.txt)|*.csv;*.txt"
    dlgCommonDialog.DialogTitle = "Save Scenario as..."
    dlgCommonDialog.ShowSave
    If dlgCommonDialog.CancelError = True Then
        TAB_UpcomingEvents.InsertTextLineAt 0, TAB_UpcomingEvents.HeaderString, False
        TAB_UpcomingEvents.WriteToFile dlgCommonDialog.FileName, False
        TAB_UpcomingEvents.DeleteLine 0
        bmScenarioSaved = True
    End If
End Sub

Private Sub mnuFileOpen_Click()
    Dim sFile As String

    With dlgCommonDialog
        .DialogTitle = "Open"
        .CancelError = True
        .Filter = "Text files (*.csv;*.txt)|*.csv;*.txt"
        .ShowOpen
        If Len(.FileName) = 0 Then
            Exit Sub
        End If
        sFile = .FileName

        If ExistFile(sFile) = True Then
            bmScenarioSaved = True
            TAB_UpcomingEvents.ResetContent
            TAB_SentEvents.ResetContent
            TAB_UpcomingEvents.ReadFromFile sFile
            TAB_UpcomingEvents.DeleteLine 0
            frmData.LoadBasicData
            GenerateEventTags -1
        Else
            MsgBox "File does not exist!", vbCritical
        End If
    End With
    TAB_UpcomingEvents.AutoSizeColumns
    TAB_SentEvents.HeaderLengthString = TAB_UpcomingEvents.HeaderLengthString
    TAB_UpcomingEvents.RedrawTab
    TAB_SentEvents.RedrawTab
End Sub

Private Sub mnuFileNew_Click()
    If bmScenarioSaved = False And TAB_UpcomingEvents.GetLineCount > 0 Then
        If MsgBox("The Scenario file has been modified. Do you want to save it?", vbYesNo, "Scenario changed") = vbYes Then
            mnuFileSave_Click
        Else
            TAB_UpcomingEvents.ResetContent
            TAB_SentEvents.ResetContent
        End If
    End If
    If bmScenarioSaved = True Then
        TAB_UpcomingEvents.ResetContent
        TAB_SentEvents.ResetContent
    End If
    TAB_UpcomingEvents.RedrawTab
    TAB_SentEvents.RedrawTab
End Sub

Private Sub InitTab(ByRef rTab As TABLib.Tab)
    Dim ilTabColumn As Integer
    Dim i As Integer
    Dim strInputFormat As String
    Dim strOutputFormat As String
    Dim strColumnName As String

    rTab.ResetContent

    rTab.HeaderLengthString = frmData.TabAFT.HeaderLengthString & ",10,10,10"
    rTab.HeaderString = "Timestamp" & sExcelSeparator & "ms" & sExcelSeparator & "Remark" & sExcelSeparator & frmData.TabAFT.HeaderString
    rTab.LogicalFieldList = rTab.HeaderString
    rTab.HeaderFontSize = frmData.TabAFT.HeaderFontSize
    rTab.FontSize = frmData.TabAFT.FontSize
    rTab.EnableHeaderSizing True
    rTab.ShowHorzScroller True
    rTab.AutoSizeByHeader = True
    rTab.AutoSizeColumns
    rTab.SetFieldSeparator sExcelSeparator

    ' get the position of the timecolumns for the UTC offset
    sTimeColumns = "Timestamp,STOA,STOD,ETAA,ETDA,LNDA,AIRA,ONBD,OFBD"
    strInputFormat = "YYYYMMDDhhmmss"
    strOutputFormat = "YYYYMMDDhhmmss"
    For i = 1 To ItemCount(sTimeColumns, ",")
        strColumnName = GetItem(sTimeColumns, i, ",")
        ilTabColumn = GetRealItemNo(rTab.HeaderString, strColumnName)
        rTab.DateTimeSetColumn ilTabColumn
        rTab.DateTimeSetColumnFormat ilTabColumn, strInputFormat, strOutputFormat
    Next
End Sub

Private Sub mnuToolsStartSendingEvents_Click()
    Dim myTime As SYSTEMTIME
    
    If MsgBox("Do you want to start sending events?", vbYesNo, "Start sending?") = vbNo Then Exit Sub

    ' init
    bmTimerRunning = True
    TAB_UpcomingEvents.Sort "0,1", True, True
    TAB_UpcomingEvents.Sort "0", True, True
    TAB_SentEvents.HeaderLengthString = TAB_UpcomingEvents.HeaderLengthString
    iEventsCount = 0
    TimerEvents.Interval = GetIniEntry(sConfigFile, gsAppName, "", "TIMER_MILLISECONDS", 100)
    iEventsPerSecond = GetIniEntry(sConfigFile, gsAppName, "", "EVENTS_PER_SECOND", 5)

    GetLocalTime myTime
    iLastSecond = myTime.wSecond
    sbStatusBar.Panels(1).Text = "Timer running - sending events..."
    frmData.ConnectUfisCom

    ' handling buttons & menu
    mnuToolsStartSendingEvents.Checked = True
    mnuToolsStopSendingEvents.Checked = False
    tbToolBar.Buttons(8).Value = tbrPressed
    tbToolBar.Buttons(9).Value = tbrUnpressed
    tbToolBar.Refresh

    ' start the timer
    TimerEvents.Enabled = True
End Sub

Private Sub mnuToolsStopSendingEvents_Click()
    TimerEvents.Enabled = False
    bmTimerRunning = False
    sbStatusBar.Panels(1).Text = "Ready."
    frmData.DisconnetUfisCom

    ' handling buttons & menu
    mnuToolsStartSendingEvents.Checked = False
    mnuToolsStopSendingEvents.Checked = True
    tbToolBar.Buttons(8).Value = tbrUnpressed
    tbToolBar.Buttons(9).Value = tbrPressed
    tbToolBar.Refresh
    DoEvents
End Sub

Private Sub TimerEvents_Timer()
    Dim myTime As SYSTEMTIME
    Dim strCompareTime As String
    Dim blEventsLeft As Boolean
    Dim olDate As Date
    GetLocalTime myTime

    ' init
    If iLastSecond <> myTime.wSecond Then
        iEventsCount = 0
        iLastSecond = myTime.wSecond
    End If
    blEventsLeft = True

    ' build the actual timestamp
    If gbOffline = False Then
        olDate = TimeSerial(myTime.wHour, myTime.wMinute, myTime.wSecond)
        olDate = olDate + DateSerial(myTime.wYear, myTime.wMonth, myTime.wDay)
        olDate = olDate - (sglUTCOffsetHours / 24)
        strCompareTime = Format(olDate, "YYYYMMDDhhmmss") & sExcelSeparator & Left(myTime.wMilliseconds, 1)
    Else
        ' offline testing
        strCompareTime = CStr(myTime.wYear) & "1231" & Format(myTime.wHour, "00") & _
        Format(myTime.wMinute, "00") & Format(myTime.wSecond, "00") & sExcelSeparator & Left(Format(myTime.wMilliseconds, "00"), 1)
    End If

    While blEventsLeft = True And iEventsCount < iEventsPerSecond And TAB_UpcomingEvents.GetLineCount > 0 And bmTimerRunning = True
        If TAB_UpcomingEvents.GetColumnValues(0, "0,1") < strCompareTime Then
            iEventsCount = iEventsCount + 1
            TAB_SentEvents.InsertTextLineAt 0, TAB_UpcomingEvents.GetLineValues(0), False
            TAB_SentEvents.SetLineTag 0, TAB_UpcomingEvents.GetLineTag(0)
            SendEvent 0, TAB_SentEvents.GetColumnValue(0, 2)
            TAB_UpcomingEvents.DeleteLine 0
            TAB_SentEvents.RedrawTab
            TAB_UpcomingEvents.RedrawTab
            DoEvents
        Else
            blEventsLeft = False
        End If
    Wend
'    TAB_SentEvents.RedrawTab
'    TAB_UpcomingEvents.RedrawTab
'    DoEvents
End Sub

Private Sub SendEvent(lLineNo As Long, sEventType As String)
    If gbOffline = True Then Exit Sub

    Dim olSendTime As SYSTEMTIME
    Dim strCommand As String
    Dim strFields As String
    Dim strData As String
    Dim strWhere As String
    Dim strTable As String
    Dim olSendDate As Date
    Dim strAdditionalRes As String
    Dim strLineStatusValue As String

    GetKeyItem strLineStatusValue, TAB_UpcomingEvents.GetLineTag(lLineNo), "{-EVENT_1-}", "{-"

    If Len(strLineStatusValue) > 0 Then
        GetKeyItem strCommand, strLineStatusValue, "{=CMD=}", "{="
        GetKeyItem strFields, strLineStatusValue, "{=FIELDS=}", "{="
        GetKeyItem strData, strLineStatusValue, "{=DATA=}", "{="
        GetKeyItem strWhere, strLineStatusValue, "{=WHERE=}", "{="
        GetKeyItem strTable, strLineStatusValue, "{=TABLE=}", "{="
        GetKeyItem strAdditionalRes, strLineStatusValue, "{=ADDITIONAL_RES=}", "{="

        GetLocalTime olSendTime
        UfisComCtrl.CallServer strCommand, strTable, strFields, strData, strWhere, "360"
        olSendDate = TimeSerial(olSendTime.wHour, olSendTime.wMinute, olSendTime.wSecond)
        olSendDate = olSendDate + DateSerial(olSendTime.wYear, olSendTime.wMonth, olSendTime.wDay)
        olSendDate = olSendDate - (sglUTCOffsetHours / 24)
        TAB_SentEvents.SetFieldValues 0, "Timestamp,ms", Format(olSendDate, "YYYYMMDDhhmmss") & sExcelSeparator & Left(olSendTime.wMilliseconds, 1)

        If UCase(sEventType) = "TOUCH AND GO" Then
            GetKeyItem strLineStatusValue, TAB_UpcomingEvents.GetLineTag(lLineNo), "{-EVENT_2-}", "{-"
            GetKeyItem strCommand, strLineStatusValue, "{=CMD=}", "{="
            GetKeyItem strFields, strLineStatusValue, "{=FIELDS=}", "{="
            GetKeyItem strData, strLineStatusValue, "{=DATA=}", "{="
            GetKeyItem strWhere, strLineStatusValue, "{=WHERE=}", "{="
            GetKeyItem strTable, strLineStatusValue, "{=TABLE=}", "{="
            UfisComCtrl.CallServer strCommand, strTable, strFields, strData, strWhere, "360"
        ElseIf Len(strAdditionalRes) > 0 Then
            If GetItem(strAdditionalRes, 1, ",") = "CIC" Then
                Dim strFrom As String
                Dim strTo As String
                Dim strTmp As String
                Dim ilTmp As Integer
                Dim strLine As String
                Dim i As Integer
                strFrom = GetItem(strAdditionalRes, 2, ",")
                strTo = GetItem(strAdditionalRes, 3, ",")
                While (IsNumeric(strFrom) = False And Len(strFrom) > 0)
                    strTmp = strTmp & Left(strFrom, 1)
                    strFrom = Mid(strFrom, 2)
                Wend
                While (IsNumeric(strTo) = False And Len(strTo) > 0)
                    strTo = Mid(strTo, 2)
                Wend
                For i = CInt(strFrom) + 1 To CInt(strTo)
                    strLine = frmData.TabCIC.GetLinesByColumnValue(1, strTmp & CStr(i), 0)
                    If Len(strLine) > 0 And IsNumeric(strLine) = True Then
                        strData = Mid(strData, InStr(strData, ","))
                        strData = frmData.TabCIC.GetColumnValue(CLng(strLine), 0) & strData
                        UfisComCtrl.CallServer strCommand, strTable, strFields, strData, strWhere, "360"
                    End If
                Next
            ElseIf GetItem(strAdditionalRes, 1, ",") = "GAT" Then
                strData = Mid(strData, InStr(strData, ","))
                strData = frmData.TabGAT.GetColumnValue(CLng(frmData.TabGAT.GetLinesByColumnValue(1, GetItem(strAdditionalRes, 3, ","), 0)), 0) & strData
                UfisComCtrl.CallServer strCommand, strTable, strFields, strData, strWhere, "360"
            ElseIf GetItem(strAdditionalRes, 1, ",") = "BLT" Then
                strData = Mid(strData, InStr(strData, ","))
                strData = frmData.TabBLT.GetColumnValue(CLng(frmData.TabBLT.GetLinesByColumnValue(1, GetItem(strAdditionalRes, 3, ","), 0)), 0) & strData
                UfisComCtrl.CallServer strCommand, strTable, strFields, strData, strWhere, "360"
            ElseIf GetItem(strAdditionalRes, 1, ",") = "CIC TIME" Then
                'Dim strFrom As String
'                Dim strTo As String
'                Dim strTmp As String
'                Dim ilTmp As Integer
'                Dim strLine As String
'                Dim i As Integer
                strFrom = GetItem(strAdditionalRes, 2, ",")
                strTo = GetItem(strAdditionalRes, 3, ",")
                While (IsNumeric(strFrom) = False And Len(strFrom) > 0)
                    strTmp = strTmp & Left(strFrom, 1)
                    strFrom = Mid(strFrom, 2)
                Wend
                While (IsNumeric(strTo) = False And Len(strTo) > 0)
                    strTo = Mid(strTo, 2)
                Wend
                For i = CInt(strFrom) + 1 To CInt(strTo)
'                    strWhere = strWhere & " AND CKIC = '" & strFrom & "'"
                    strWhere = Left(strWhere, InStr(1, strWhere, "CKIC") - 1) & "CKIC = '" & strTmp & Format(i, "00") & "'"
'                    strLine = frmData.TabCIC.GetLinesByColumnValue(1, strTmp & CStr(i), 0)
'                    If Len(strLine) > 0 And IsNumeric(strLine) = True Then
'                        strData = Mid(strData, InStr(strData, ","))
'                        strData = frmData.TabCIC.GetColumnValue(CLng(strLine), 0) & strData
                        UfisComCtrl.CallServer strCommand, strTable, strFields, strData, strWhere, "360"

                Next
            End If
        End If
    End If
End Sub

Public Sub GenerateEventTags(lLineNo As Long)
    Dim strEvent1 As String
    Dim strEvent2 As String
    Dim strEventType As String
    Dim l As Long
    Dim llStart As Long
    Dim llLineCount As Long
    Dim strCommand As String
    Dim strFields As String
    Dim strData As String
    Dim strWhere As String
    Dim strTable As String
    Dim strUrno As String
    Dim strTmp As String
    Dim ilETOX As Integer
    Dim ilSTOX As Integer
    Dim ilRes1 As Integer
    Dim ilRes2 As Integer
    Dim idxFLNO As Integer
    Dim idxUrno As Integer
    Dim strSTOX_Field As String
    Dim strSTOX_Value As String
    Dim strFLNO As String
    Dim strMINUTES_BEFORE_ETOX As String
    Dim strTime As String
    Dim strALC3 As String
    Dim strFLTN As String
    Dim strFLNS As String

    If lLineNo < 0 Then
        llStart = 0
        llLineCount = TAB_UpcomingEvents.GetLineCount - 1
    Else
        llStart = lLineNo
        llLineCount = lLineNo
    End If

    idxFLNO = GetRealItemNo(TAB_UpcomingEvents.LogicalFieldList, "FLNO")
    idxUrno = GetRealItemNo(TAB_UpcomingEvents.LogicalFieldList, "URNO")
    strMINUTES_BEFORE_ETOX = GetIniEntry(sConfigFile, gsAppName, "", "MINUTES_BEFORE_ETOX", "60")

    For l = llStart To llLineCount
        strEventType = UCase(TAB_UpcomingEvents.GetColumnValue(l, 2))
        strUrno = TAB_UpcomingEvents.GetColumnValue(l, idxUrno)
        If TAB_UpcomingEvents.GetFieldValue(l, "ADID") = "A" Then
            strSTOX_Field = "STOA"
        Else
            strSTOX_Field = "STOD"
        End If
        strSTOX_Value = TAB_UpcomingEvents.GetFieldValue(l, strSTOX_Field)
        strFLNO = TAB_UpcomingEvents.GetColumnValue(l, idxFLNO)
        strUrno = TAB_UpcomingEvents.GetColumnValue(l, idxUrno)
        strEvent1 = ""
        strEvent2 = ""

        Select Case strEventType
            Case "ETOD EVENT"
                strTmp = Replace(TAB_UpcomingEvents.GetFieldValues(l, "FLNO,ETDA"), ";", ",")
                strTable = "{=TABLE=}AFTTAB"
                strCommand = "{=CMD=}UFR"
                strFields = "{=FIELDS=}FLNO,ETDA,ETOD"
                strData = "{=DATA=}" & strTmp & "," & GetItem(strTmp, 2, ",")
                If Len(strUrno) > 0 Then
                    strWhere = "{=WHERE=}WHERE URNO=" & strUrno
                Else
                    strWhere = "{=WHERE=}WHERE FLNO='" & strFLNO & "' AND " & strSTOX_Field & "='" & strSTOX_Value & "'"
                End If
                strEvent1 = "{-EVENT_1-}" & strTable & strCommand & strFields & strData & strWhere
            Case "ETOA EVENT"
                strTmp = Replace(TAB_UpcomingEvents.GetFieldValues(l, "FLNO,ETAA"), ";", ",")
                strTable = "{=TABLE=}AFTTAB"
                strCommand = "{=CMD=}UFR"
                strFields = "{=FIELDS=}FLNO,ETAA,ETOA"
                strData = "{=DATA=}" & strTmp & "," & GetItem(strTmp, 2, ",")
                If Len(strUrno) > 0 Then
                    strWhere = "{=WHERE=}WHERE URNO=" & strUrno
                Else
                    strWhere = "{=WHERE=}WHERE FLNO='" & strFLNO & "' AND " & strSTOX_Field & "='" & strSTOX_Value & "'"
                End If
                strEvent1 = "{-EVENT_1-}" & strTable & strCommand & strFields & strData & strWhere
            Case "LAND EVENT"
                strTable = "{=TABLE=}AFTTAB"
                strCommand = "{=CMD=}UFR"
                strFields = "{=FIELDS=}FLNO,LNDA"
                strData = "{=DATA=}" & Replace(TAB_UpcomingEvents.GetFieldValues(l, "FLNO,LNDA"), ";", ",")
                If Len(strUrno) > 0 Then
                    strWhere = "{=WHERE=}WHERE URNO=" & strUrno
                Else
                    strWhere = "{=WHERE=}WHERE FLNO='" & strFLNO & "' AND " & strSTOX_Field & "='" & strSTOX_Value & "'"
                End If
                strEvent1 = "{-EVENT_1-}" & strTable & strCommand & strFields & strData & strWhere
            Case "AIRB EVENT"
                strTable = "{=TABLE=}AFTTAB"
                strCommand = "{=CMD=}UFR"
                strFields = "{=FIELDS=}FLNO,AIRA"
                strData = "{=DATA=}" & Replace(TAB_UpcomingEvents.GetFieldValues(l, "FLNO,AIRA"), ";", ",")
                If Len(strUrno) > 0 Then
                    strWhere = "{=WHERE=}WHERE URNO=" & strUrno
                Else
                    strWhere = "{=WHERE=}WHERE FLNO='" & strFLNO & "' AND " & strSTOX_Field & "='" & strSTOX_Value & "'"
                End If
                strEvent1 = "{-EVENT_1-}" & strTable & strCommand & strFields & strData & strWhere
            Case "ONBL EVENT"
                strTable = "{=TABLE=}AFTTAB"
                strCommand = "{=CMD=}UFR"
                strFields = "{=FIELDS=}FLNO,ONBD"
                strData = "{=DATA=}" & Replace(TAB_UpcomingEvents.GetFieldValues(l, "FLNO,ONBD"), ";", ",")
                If Len(strUrno) > 0 Then
                    strWhere = "{=WHERE=}WHERE URNO=" & strUrno
                Else
                    strWhere = "{=WHERE=}WHERE FLNO='" & strFLNO & "' AND " & strSTOX_Field & "='" & strSTOX_Value & "'"
                End If
                strEvent1 = "{-EVENT_1-}" & strTable & strCommand & strFields & strData & strWhere
            Case "OFBL EVENT"
                strTable = "{=TABLE=}AFTTAB"
                strCommand = "{=CMD=}UFR"
                strFields = "{=FIELDS=}FLNO,OFBD"
                strData = "{=DATA=}" & Replace(TAB_UpcomingEvents.GetFieldValues(l, "FLNO,OFBD"), ";", ",")
                If Len(strUrno) > 0 Then
                    strWhere = "{=WHERE=}WHERE URNO=" & strUrno
                Else
                    strWhere = "{=WHERE=}WHERE FLNO='" & strFLNO & "' AND " & strSTOX_Field & "='" & strSTOX_Value & "'"
                End If
                strEvent1 = "{-EVENT_1-}" & strTable & strCommand & strFields & strData & strWhere
            Case "CANCEL"
                strTable = "{=TABLE=}AFTTAB"
                strCommand = "{=CMD=}UFR"
                strFields = "{=FIELDS=}FLNO,FTYP"
                strData = "{=DATA=}" & Replace(TAB_UpcomingEvents.GetFieldValues(l, "FLNO,FTYP"), ";", ",")
                If Len(strUrno) > 0 Then
                    strWhere = "{=WHERE=}WHERE URNO=" & strUrno
                Else
                    strWhere = "{=WHERE=}WHERE FLNO='" & strFLNO & "' AND " & strSTOX_Field & "='" & strSTOX_Value & "'"
                End If
                strEvent1 = "{-EVENT_1-}" & strTable & strCommand & strFields & strData & strWhere
            Case "RETURN TAXI"
                strTable = "{=TABLE=}AFTTAB"
                strCommand = "{=CMD=}UFR"
                strFields = "{=FIELDS=}FLNO,FTYP"
                strData = "{=DATA=}" & Replace(TAB_UpcomingEvents.GetFieldValues(l, "FLNO,FTYP"), ";", ",")
                If Len(strUrno) > 0 Then
                    strWhere = "{=WHERE=}WHERE URNO=" & strUrno
                Else
                    strWhere = "{=WHERE=}WHERE FLNO='" & strFLNO & "' AND " & strSTOX_Field & "='" & strSTOX_Value & "'"
                End If
                strEvent1 = "{-EVENT_1-}" & strTable & strCommand & strFields & strData & strWhere
            Case "RETURN FLIGHT"
                strTable = "{=TABLE=}AFTTAB"
                strCommand = "{=CMD=}UFR"
                strFields = "{=FIELDS=}FLNO,FTYP"
                strData = "{=DATA=}" & Replace(TAB_UpcomingEvents.GetFieldValues(l, "FLNO,FTYP"), ";", ",")
                If Len(strUrno) > 0 Then
                    strWhere = "{=WHERE=}WHERE URNO=" & strUrno
                Else
                    strWhere = "{=WHERE=}WHERE FLNO='" & strFLNO & "' AND " & strSTOX_Field & "='" & strSTOX_Value & "'"
                End If
                strEvent1 = "{-EVENT_1-}" & strTable & strCommand & strFields & strData & strWhere
            Case "DIVERTED FLIGHT"
                strTable = "{=TABLE=}AFTTAB"
                strCommand = "{=CMD=}UFR"
                strFields = "{=FIELDS=}FLNO,DIVR,FTYP"
                strData = "{=DATA=}" & Replace(TAB_UpcomingEvents.GetFieldValues(l, "FLNO,DIVR,FTYP"), ";", ",")
                If Len(strUrno) > 0 Then
                    strWhere = "{=WHERE=}WHERE URNO=" & strUrno
                Else
                    strWhere = "{=WHERE=}WHERE FLNO='" & strFLNO & "' AND " & strSTOX_Field & "='" & strSTOX_Value & "'"
                End If
                strEvent1 = "{-EVENT_1-}" & strTable & strCommand & strFields & strData & strWhere
            Case "DELETE FLIGHT"
                strTable = "{=TABLE=}AFTTAB"
                strCommand = "{=CMD=}UFR"
                strFields = "{=FIELDS=}FLNO,FTYP"
                strData = "{=DATA=}" & Replace(TAB_UpcomingEvents.GetFieldValues(l, "FLNO,FTYP"), ";", ",")
                If Len(strUrno) > 0 Then
                    strWhere = "{=WHERE=}WHERE URNO=" & strUrno
                Else
                    strWhere = "{=WHERE=}WHERE FLNO='" & strFLNO & "' AND " & strSTOX_Field & "='" & strSTOX_Value & "'"
                End If
                strEvent1 = "{-EVENT_1-}" & strTable & strCommand & strFields & strData & strWhere
            Case "AC CHANGE"
                strTable = "{=TABLE=}AFTTAB"
                strCommand = "{=CMD=}UFR"
                strFields = "{=FIELDS=}FLNO,REGN"
                strData = "{=DATA=}" & Replace(TAB_UpcomingEvents.GetFieldValues(l, "FLNO,REGN"), ";", ",")
                ' add AC type
                strTmp = frmData.TabACR.GetLinesByColumnValue(2, TAB_UpcomingEvents.GetFieldValue(l, "REGN"), 0)
                If Len(strTmp) > 0 And IsNumeric(strTmp) = True Then
                    strFields = strFields & ",ACT3,ACT5"
                    strData = strData & "," & frmData.TabACR.GetFieldValue(strTmp, "ACT3") & "," & frmData.TabACR.GetFieldValue(strTmp, "ACT5")
                End If
                If Len(strUrno) > 0 Then
                    strWhere = "{=WHERE=}WHERE URNO=" & strUrno
                Else
                    strWhere = "{=WHERE=}WHERE FLNO='" & strFLNO & "' AND " & strSTOX_Field & "='" & strSTOX_Value & "'"
                End If
                strEvent1 = "{-EVENT_1-}" & strTable & strCommand & strFields & strData & strWhere
            Case "TOUCH AND GO"
                ' 1st event
                strTime = TAB_UpcomingEvents.GetFieldValue(l, "Timestamp")
                strCommand = "{=CMD=}UFR"
                strTable = "{=TABLE=}AFTTAB"
                strFields = "{=FIELDS=}FLNO,LNDU,ONBU"
                strData = "{=DATA=}" & strFLNO & "," & strTime & "," & strTime
                If Len(strUrno) > 0 Then
                    strWhere = "{=WHERE=}WHERE URNO=" & strUrno
                Else
                    strWhere = "{=WHERE=}WHERE FLNO='" & strFLNO & "' AND " & strSTOX_Field & "='" & strSTOX_Value & "'"
                End If
                strEvent1 = "{-EVENT_1-}" & strTable & strCommand & strFields & strData & strWhere

                ' 2nd event
                Dim strFields2 As String
                Dim strData2 As String
                Dim strTime2 As String
                Dim strWhere2 As String

                ' basic information to insert new flight record for touch 'n' go
                strFields2 = "FLNO,ADID,REGN,BLT1,BLT2,PSTA,GTA1,GTA2,FTYP,TTYP"
                strTime2 = TAB_UpcomingEvents.GetFieldValue(l, "Timestamp")
                strData2 = "{=DATA=}" & Replace(TAB_UpcomingEvents.GetFieldValues(l, strFields2), ";", ",")
                strData2 = strData2 & "," & strTime2 & "," & strTime2 & "," & strTime2 & "," & strTime2
                strFields2 = "{=FIELDS=}" & strFields2 & ",AIRU,OFBU,STOA,STOD"
                strWhere2 = "{=WHERE=}WHERE " & Left(strTime2, 8) & "," & Left(strTime2, 8) & ",1234567"

                ' add AC type
                strTmp = frmData.TabACR.GetLinesByColumnValue(2, TAB_UpcomingEvents.GetFieldValue(l, "REGN"), 0)
                strFields2 = strFields2 & ",ACT3,ACT5"
                If Len(strTmp) > 0 And IsNumeric(strTmp) = True Then
                    strData2 = strData2 & "," & frmData.TabACR.GetFieldValue(strTmp, "ACT3") & "," & frmData.TabACR.GetFieldValue(strTmp, "ACT5")
                Else
                    strData2 = strData2 & "," & Replace(TAB_UpcomingEvents.GetFieldValues(l, "ACT3,ACT5"), ";", ",")
                End If

                ' add invalid (!) DES3/DES4
                strFields2 = strFields2 & ",DES3,DES4,TTYP,FTYP"
                strData2 = strData2 & ",EDX,EDXX,05,O"

                ' copy old DES3 to ORG3
                strFields2 = strFields2 & ",ORG3"
                strData2 = strData2 & "," & TAB_UpcomingEvents.GetFieldValue(l, "DES3")

                ' handle fligh number
                If Len(strFLNO) > 4 Then
                    strFields2 = strFields2 & ",FLTN,FLNS"
                    strALC3 = Trim(Left(strFLNO, 3))
                    If Len(frmData.TabALT.GetLinesByColumnValue(2, strALC3, 0)) > 0 Then
                        strFields2 = strFields2 & ",ALC3"
                    Else
                        strFields2 = strFields2 & ",ALC2"
                    End If
                    If InStr(4, strFLNO, " ") > 4 Then
                        strFLTN = Mid(strFLNO, 4, InStr(4, strFLNO, " ") - 4)
                        strFLNS = Right(strFLNO, 1)
                    Else
                        strFLTN = Mid(strFLNO, 4)
                        strFLNS = ""
                    End If
                    strData2 = strData2 & "," & strFLTN & "," & strFLNS & "," & strALC3
                End If
                strEvent2 = "{-EVENT_2-}{=TABLE=}AFTTAB{=CMD=}IDF" & strFields2 & strData2 & strWhere2
            Case "GATE BLOCKED"
                If TAB_UpcomingEvents.GetFieldValue(l, "ADID") = "A" Then
                    ilETOX = GetRealItemNo(TAB_UpcomingEvents.LogicalFieldList, "ETAA")
                    ilSTOX = GetRealItemNo(TAB_UpcomingEvents.LogicalFieldList, "STOA")
                    ilRes1 = GetRealItemNo(TAB_UpcomingEvents.LogicalFieldList, "GTA1")
                    ilRes2 = GetRealItemNo(TAB_UpcomingEvents.LogicalFieldList, "GTA2")
                Else
                    ilETOX = GetRealItemNo(TAB_UpcomingEvents.LogicalFieldList, "ETDA")
                    ilSTOX = GetRealItemNo(TAB_UpcomingEvents.LogicalFieldList, "STOD")
                    ilRes1 = GetRealItemNo(TAB_UpcomingEvents.LogicalFieldList, "GTD1")
                    ilRes2 = GetRealItemNo(TAB_UpcomingEvents.LogicalFieldList, "GTD2")
                End If
                TAB_UpcomingEvents.SetLineTag l, mdlEvents.GetBlockingEventTag(l, "GAT", ilETOX, ilSTOX, strMINUTES_BEFORE_ETOX, _
                ilRes1, ilRes2, idxFLNO, frmData.TabGAT, 90, 90, strEventType)
            Case "CIC BLOCKED"
                ilRes1 = GetRealItemNo(TAB_UpcomingEvents.LogicalFieldList, "CKIF")
                ilRes2 = GetRealItemNo(TAB_UpcomingEvents.LogicalFieldList, "CKIT")
                If TAB_UpcomingEvents.GetFieldValue(l, "ADID") = "A" Then
                    ilETOX = GetRealItemNo(TAB_UpcomingEvents.LogicalFieldList, "ETAA")
                    ilSTOX = GetRealItemNo(TAB_UpcomingEvents.LogicalFieldList, "STOA")
                Else
                    ilETOX = GetRealItemNo(TAB_UpcomingEvents.LogicalFieldList, "ETDA")
                    ilSTOX = GetRealItemNo(TAB_UpcomingEvents.LogicalFieldList, "STOD")
                End If
                TAB_UpcomingEvents.SetLineTag l, mdlEvents.GetBlockingEventTag(l, "CIC", ilETOX, ilSTOX, strMINUTES_BEFORE_ETOX, _
                ilRes1, ilRes2, idxFLNO, frmData.TabCIC, 120, 30, strEventType)
            Case "POSITION BLOCKED"
                ilRes2 = -1
                If TAB_UpcomingEvents.GetFieldValue(l, "ADID") = "A" Then
                    ilETOX = GetRealItemNo(TAB_UpcomingEvents.LogicalFieldList, "ETAA")
                    ilSTOX = GetRealItemNo(TAB_UpcomingEvents.LogicalFieldList, "STOA")
                    ilRes1 = GetRealItemNo(TAB_UpcomingEvents.LogicalFieldList, "PSTA")
                Else
                    ilETOX = GetRealItemNo(TAB_UpcomingEvents.LogicalFieldList, "ETDA")
                    ilSTOX = GetRealItemNo(TAB_UpcomingEvents.LogicalFieldList, "STOD")
                    ilRes1 = GetRealItemNo(TAB_UpcomingEvents.LogicalFieldList, "PSTD")
                End If
                TAB_UpcomingEvents.SetLineTag l, mdlEvents.GetBlockingEventTag(l, "PST", ilETOX, ilSTOX, strMINUTES_BEFORE_ETOX, _
                ilRes1, ilRes2, idxFLNO, frmData.TabPST, 60, 60, strEventType)
            Case "BELT BLOCKED"
                ilRes1 = GetRealItemNo(TAB_UpcomingEvents.LogicalFieldList, "BLT1")
                ilRes2 = GetRealItemNo(TAB_UpcomingEvents.LogicalFieldList, "BLT2")
                If TAB_UpcomingEvents.GetFieldValue(l, "ADID") = "A" Then
                    ilETOX = GetRealItemNo(TAB_UpcomingEvents.LogicalFieldList, "ETAA")
                    ilSTOX = GetRealItemNo(TAB_UpcomingEvents.LogicalFieldList, "STOA")
                Else
                    ilETOX = GetRealItemNo(TAB_UpcomingEvents.LogicalFieldList, "ETDA")
                    ilSTOX = GetRealItemNo(TAB_UpcomingEvents.LogicalFieldList, "STOD")
                End If
                TAB_UpcomingEvents.SetLineTag l, mdlEvents.GetBlockingEventTag(l, "BLT", ilETOX, ilSTOX, strMINUTES_BEFORE_ETOX, _
                ilRes1, ilRes2, idxFLNO, frmData.TabBLT, 30, 120, strEventType)
            Case "INSERT FLIGHT"
                strTable = "{=TABLE=}AFTTAB"
                strCommand = "{=CMD=}IDF"
                If TAB_UpcomingEvents.GetFieldValue(l, "ADID") = "A" Then
                    strFields = "FLNO,ADID,STOA,ORG3,DES3,REGN,BLT1,BLT2,PSTA,GTA1,GTA2,FTYP,TTYP"
                    strTime = Left(TAB_UpcomingEvents.GetFieldValue(l, "STOA"), 8)
                Else
                    strFields = "FLNO,ADID,STOD,ORG3,DES3,REGN,CKIF,CKIT,PSTD,GTD1,GTD2,FTYP,TTYP"
                    strTime = Left(TAB_UpcomingEvents.GetFieldValue(l, "STOD"), 8)
                End If
                strData = "{=DATA=}" & Replace(TAB_UpcomingEvents.GetFieldValues(l, strFields), ";", ",")
                strFields = "{=FIELDS=}" & strFields
                strWhere = "{=WHERE=}WHERE " & strTime & "," & strTime & ",1234567"
                ' add AC type
                strTmp = frmData.TabACR.GetLinesByColumnValue(2, TAB_UpcomingEvents.GetFieldValue(l, "REGN"), 0)
                If Len(strTmp) > 0 And IsNumeric(strTmp) = True Then
                    strFields = strFields & ",ACT3,ACT5"
                    strData = strData & "," & frmData.TabACR.GetFieldValue(strTmp, "ACT3") & "," & frmData.TabACR.GetFieldValue(strTmp, "ACT5")
                End If
                strFLNO = TAB_UpcomingEvents.GetFieldValue(l, "FLNO")
                If Len(strFLNO) > 4 Then
                    strFields = strFields & ",FLTN,FLNS"
                    strALC3 = Trim(Left(strFLNO, 3))
                    If Len(frmData.TabALT.GetLinesByColumnValue(2, strALC3, 0)) > 0 Then
                        strFields = strFields & ",ALC3"
                    Else
                        strFields = strFields & ",ALC2"
                    End If
                    If InStr(4, strFLNO, " ") > 4 Then
                        strFLTN = Mid(strFLNO, 4, InStr(4, strFLNO, " ") - 4)
                        strFLNS = Right(strFLNO, 1)
                    Else
                        strFLTN = Mid(strFLNO, 4)
                        strFLNS = ""
                    End If
                    strData = strData & "," & strFLTN & "," & strFLNS & "," & strALC3
                    strEvent1 = "{-EVENT_1-}" & strTable & strCommand & strFields & strData & strWhere
                End If
            Case "CIC TIME"
                Dim strFrom As String
                Dim strTo As String

                strFrom = TAB_UpcomingEvents.GetFieldValue(l, "CKIF")
                strTo = TAB_UpcomingEvents.GetFieldValue(l, "CKIT")
                If Len(strFrom) > 0 And Len(strTo) > 0 Then
                    strTable = "{=TABLE=}CCATAB"
                    strCommand = "{=CMD=}URT"
                    strFields = "{=FIELDS=}CKBA,CKEA"
                    strData = "{=DATA=}" & Replace(TAB_UpcomingEvents.GetFieldValues(l, "ETAA,ETDA"), ";", ",")
                    strWhere = "{=WHERE=}WHERE FLNO='" & strFLNO & "' AND " & strSTOX_Field & "='" & strSTOX_Value & "'"
                    strWhere = strWhere & " AND CKIC = '" & strFrom & "'"
                    strEvent1 = "{-EVENT_1-}" & strTable & strCommand & strFields & strData & strWhere
                    strEvent1 = strEvent1 & "{=ADDITIONAL_RES=}CIC TIME," & strFrom & "," & strTo
                End If
            Case "GAT TIME"
                Dim strGate1From As String
                Dim strGate1To As String
                Dim strGate2From As String
                Dim strGate2To As String

                If TAB_UpcomingEvents.GetFieldValue(l, "ADID") = "A" Then
                    strFields = "{=FIELDS=}GA1X,GA1Y,GA2X,GA2Y"
                Else
                    strFields = "{=FIELDS=}GD1X,GD1Y,GD2X,GD2Y"
                End If

                strTable = "{=TABLE=}AFTTAB"
                strCommand = "{=CMD=}UFR"
                strData = "{=DATA=}" & Replace(TAB_UpcomingEvents.GetFieldValues(l, "ETAA,ETDA,AIRA,LNDA"), ";", ",")
                strWhere = "{=WHERE=}WHERE FLNO='" & strFLNO & "' AND " & strSTOX_Field & "='" & strSTOX_Value & "'"
                strEvent1 = "{-EVENT_1-}" & strTable & strCommand & strFields & strData & strWhere
        End Select

        If Len(strEvent1) > 0 Then
            TAB_UpcomingEvents.SetLineTag l, strEvent1 & strEvent2
        End If
    Next
End Sub


'            Case "ETOD EVENT"
'            Case "ETOA EVENT"
'            Case "LAND EVENT"
'            Case "AIRB EVENT"
'            Case "ONBL EVENT"
'            Case "OFBL EVENT"
'            Case "CANCEL"
'            Case "RETURN TAXI"
'            Case "RETURN FLIGHT"
'            Case "DIVERTED FLIGHT"
'            Case "AC CHANGE"
'            case "TOUCH AND GO"

Private Sub TimerSeconds_Timer()
    sbStatusBar.Panels(3).Text = Format$(Now, "hh:mm:ss")
    DoEvents
End Sub
