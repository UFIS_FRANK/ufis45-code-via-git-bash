rem Copies all generated files from the SCM-Structue into the Install-Shield directory.
rem 

rem set drive to J: (IS 6.2) or K: (IS 8.0) 
set drive=k:

rem Copy ini- and config-files
copy c:\ufis_bin\ceda.ini %drive%\B50_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\grp.ini %drive%\B50_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\*.bmp %drive%\B50_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\bdps_sec.cfg %drive%\B50_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\importtool.ini %drive%\B50_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\telexpool.ini %drive%\B50_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\loatabcfg.cfg %drive%\B50_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\loatabviewer.ini %drive%\B50_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\texttable.txt %drive%\B50_Build\ufis_client_appl\system\*.*

rem copy applications
copy c:\ufis_bin\debug\bdps_sec.exe %drive%\B50_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpspass.exe %drive%\B50_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpsuif.exe %drive%\B50_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\fips.exe %drive%\B50_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\rules.exe %drive%\B50_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\grp.exe %drive%\B50_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\importflights.exe %drive%\B50_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\loadtabviewer.exe %drive%\B50_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\logtab_viewer.exe %drive%\B50_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\export.exe %drive%\B50_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\RulesChecker.exe %drive%\B50_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\telexpool.exe %drive%\B50_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\HUB_Manager.exe %drive%\B50_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\MasterClock.exe %drive%\B50_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\Status_Manager.exe %drive%\B50_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\CheckListProcessing.exe %drive%\B50_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\AIMSViewer.exe %drive%\B50_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\SBIAP_StatusEventSimulator.exe %drive%\B50_Build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\FlightPermits.exe %drive%\B50_Build\ufis_client_appl\applications\*.*


rem copy files for .NET Appl
copy c:\ufis_bin\release\*lib.dll %drive%\B50_Build\ufis_client_appl\applications\interopdll\*.*
copy c:\ufis_bin\release\ufis.utils.dll %drive%\B50_Build\ufis_client_appl\applications\interopdll\*.*
copy c:\ufis_bin\release\PictureViewer.dll %drive%\B50_Build\ufis_client_appl\applications\interopdll\*.*


rem copy System Components
copy c:\ufis_bin\release\aatlogin.ocx %drive%\B50_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\aatlogin.tlb %drive%\B50_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcproxy.exe %drive%\B50_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcproxy.tlb %drive%\B50_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcserv32.exe %drive%\B50_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.ocx %drive%\B50_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.tlb %drive%\B50_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucom.ocx %drive%\B50_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucom.tlb %drive%\B50_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufis32.dll %drive%\B50_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.exe %drive%\B50_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisapplmgr.exe %drive%\B50_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.tlb %drive%\B50_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisapplmgr.tlb %drive%\B50_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.ocx %drive%\B50_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.tlb %drive%\B50_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.ocx %drive%\B50_Build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.tlb %drive%\B50_Build\ufis_client_appl\system\*.*


rem Copy Help-Files
rem copy c:\ufis_bin\Help\*.chm %drive%\B50_Build\ufis_client_appl\help\*.*

