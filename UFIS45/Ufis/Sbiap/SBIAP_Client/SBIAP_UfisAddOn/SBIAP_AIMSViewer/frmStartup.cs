using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Ufis.Utils;

namespace AIMSViewer
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class frmStartup : System.Windows.Forms.Form
	{
		private double myOpacity = 0.9f;


		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.PictureBox pictureBox2;
		private System.Windows.Forms.TextBox txtApplication;
		public System.Windows.Forms.TextBox txtCurrentStatus;
		public System.Windows.Forms.TextBox txtDoneStatus;
		public static frmStartup myThis;
		private System.Timers.Timer timerSplash;
		private System.Timers.Timer timerClose;
		private	string	startupLogo = string.Empty;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmStartup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			myThis = this;
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\ceda.ini");
			this.startupLogo = myIni.IniReadValue("AIMSVIEWER", "STARTUPLOGO");
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		/// <summary>
		/// returns the this reference to get access from outside the class
		/// </summary>
		/// <returns>Reference to frmStartup</returns>
		public static frmStartup GetThis()
		{
			return myThis;
		}
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmStartup));
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.txtApplication = new System.Windows.Forms.TextBox();
			this.txtCurrentStatus = new System.Windows.Forms.TextBox();
			this.txtDoneStatus = new System.Windows.Forms.TextBox();
			this.timerSplash = new System.Timers.Timer();
			this.timerClose = new System.Timers.Timer();
			((System.ComponentModel.ISupportInitialize)(this.timerSplash)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.timerClose)).BeginInit();
			this.SuspendLayout();
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(24, 73);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(460, 363);
			this.pictureBox1.TabIndex = 0;
			this.pictureBox1.TabStop = false;
			// 
			// pictureBox2
			// 
			this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
			this.pictureBox2.Location = new System.Drawing.Point(28, 12);
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new System.Drawing.Size(56, 52);
			this.pictureBox2.TabIndex = 1;
			this.pictureBox2.TabStop = false;
			// 
			// txtApplication
			// 
			this.txtApplication.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.txtApplication.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.txtApplication.Location = new System.Drawing.Point(104, 28);
			this.txtApplication.Name = "txtApplication";
			this.txtApplication.Size = new System.Drawing.Size(384, 25);
			this.txtApplication.TabIndex = 2;
			this.txtApplication.Text = "AIMS Viewer";
			this.txtApplication.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// txtCurrentStatus
			// 
			this.txtCurrentStatus.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.txtCurrentStatus.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.txtCurrentStatus.Location = new System.Drawing.Point(24, 444);
			this.txtCurrentStatus.Name = "txtCurrentStatus";
			this.txtCurrentStatus.Size = new System.Drawing.Size(464, 15);
			this.txtCurrentStatus.TabIndex = 3;
			this.txtCurrentStatus.Text = "Connecting ...";
			// 
			// txtDoneStatus
			// 
			this.txtDoneStatus.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.txtDoneStatus.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.txtDoneStatus.Location = new System.Drawing.Point(24, 472);
			this.txtDoneStatus.Name = "txtDoneStatus";
			this.txtDoneStatus.Size = new System.Drawing.Size(464, 15);
			this.txtDoneStatus.TabIndex = 4;
			this.txtDoneStatus.Text = "";
			// 
			// timerSplash
			// 
			this.timerSplash.Interval = 50;
			this.timerSplash.SynchronizingObject = this;
			this.timerSplash.Elapsed += new System.Timers.ElapsedEventHandler(this.timerSplash_Elapsed);
			// 
			// timerClose
			// 
			this.timerClose.Interval = 75;
			this.timerClose.SynchronizingObject = this;
			this.timerClose.Elapsed += new System.Timers.ElapsedEventHandler(this.timerClose_Elapsed);
			// 
			// frmStartup
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 13);
			this.ClientSize = new System.Drawing.Size(500, 498);
			this.Controls.Add(this.txtDoneStatus);
			this.Controls.Add(this.txtCurrentStatus);
			this.Controls.Add(this.txtApplication);
			this.Controls.Add(this.pictureBox2);
			this.Controls.Add(this.pictureBox1);
			this.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "frmStartup";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Startup ...";
			this.Load += new System.EventHandler(this.frmStartup_Load);
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.frmStartup_Paint);
			((System.ComponentModel.ISupportInitialize)(this.timerSplash)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.timerClose)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void frmStartup_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics , 90f, this, Color.WhiteSmoke , Color.DarkGray  );			
		}

		private void frmStartup_Load(object sender, System.EventArgs e)
		{
			this.Text = "Startup AIMS-Viewer on server " + UT.ServerName;
			try
			{
				if (this.startupLogo != string.Empty)
				{
					this.pictureBox1.Image = Image.FromFile(this.startupLogo);
					this.pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
				}
			}
			catch(Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(ex.Message);								
			}
		}

		private void timerSplash_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			myOpacity += 0.1;
			this.Opacity = myOpacity;
			if(myOpacity >= 1)
			{
				timerSplash.Enabled = false;
			}
		}
		public void PrepareClose()
		{
			timerClose.Enabled = true;
		}

		private void timerClose_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			myOpacity -= 0.2;
			this.Opacity = myOpacity;
			pictureBox1.Refresh();
			txtCurrentStatus.Refresh();
			txtDoneStatus.Refresh();
			if(myOpacity <= 0f)
			{
				timerClose.Stop();
				timerClose.Enabled = false;
				this.Close();
			}
		}

	}
}
