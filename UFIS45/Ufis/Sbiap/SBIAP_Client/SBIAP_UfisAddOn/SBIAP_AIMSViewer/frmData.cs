using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using System.Globalization;	
using Ufis.Data;
using Ufis.Utils;
using PictureViewer;

namespace AIMSViewer
{
	/// <summary>
	/// Summary description for frmData.
	/// </summary>
	public class frmData : System.Windows.Forms.Form
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private static frmData myThis;
		private static char[]	clientPatchString = {(char)34,(char)39,(char)44,(char)10,(char)13};
		private	static char[]	serverPatchString = {(char)23,(char)24,(char)25,(char)28,(char)29};	
		private	System.Globalization.NumberFormatInfo nfi;
		private	SortedList		imageList = new SortedList();
		private	ArrayList		symbolList= null;
		private AxTABLib.AxTAB tabViews;
		private AxAATLOGINLib.AxAatLogin axAatLogin1;
		private AxTABLib.AxTAB tabSeen;
		private System.Windows.Forms.Button btnStore;
		private	SortedList		symbolEvents = new SortedList();
		private	int				blinkInterval = 1000;
		private	SortedList		ignoreErrors = new SortedList();
		private string[]		otherSystems = null;
		private	bool			ufisSuperUser = false; //MWO: 12.04.05: When /CONNECTED was set then return always "1" for the privileges
		private ErrorList		theErrorList;

		public frmData(AxAATLOGINLib.AxAatLogin axAatLogin1)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			this.axAatLogin1 = axAatLogin1;
			myThis = this;
			nfi = new System.Globalization.NumberFormatInfo();
			nfi.NumberDecimalSeparator = ".";

			Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\ceda.ini");
			String interval = myIni.IniReadValue("AIMSViewer","BlinkInterval");
			try
			{
				this.blinkInterval = int.Parse(interval);
			}
			catch(Exception ex)
			{
				this.blinkInterval = 1000;
				System.Diagnostics.Debug.WriteLine(ex.Message);
			}


			String otherSystemsList = myIni.IniReadValue("AIMSViewer","OtherSystems");
			if (otherSystemsList != null && otherSystemsList.Length > 0)
			{
				this.otherSystems = otherSystemsList.Split(',');
			}

			DataExchange.OnViewNameChanged +=new AIMSViewer.DataExchange.ViewNameChanged(DataExchange_OnViewNameChanged);
			DataExchange.OnApplication_Shutdown +=new AIMSViewer.DataExchange.Application_Shutdown(DataExchange_OnApplication_Shutdown);
			theErrorList = new ErrorList();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				for (int i = 0; i < this.imageList.Count; i++)
				{
					Image image = (Image)this.imageList.GetByIndex(i);
					image.Dispose();
				}
				
				this.imageList.Clear();

				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmData));
			this.tabViews = new AxTABLib.AxTAB();
			this.tabSeen = new AxTABLib.AxTAB();
			this.btnStore = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.tabViews)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabSeen)).BeginInit();
			this.SuspendLayout();
			// 
			// tabViews
			// 
			this.tabViews.Location = new System.Drawing.Point(48, 40);
			this.tabViews.Name = "tabViews";
			this.tabViews.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabViews.OcxState")));
			this.tabViews.Size = new System.Drawing.Size(200, 136);
			this.tabViews.TabIndex = 0;
			// 
			// tabSeen
			// 
			this.tabSeen.Location = new System.Drawing.Point(48, 192);
			this.tabSeen.Name = "tabSeen";
			this.tabSeen.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabSeen.OcxState")));
			this.tabSeen.Size = new System.Drawing.Size(200, 168);
			this.tabSeen.TabIndex = 1;
			// 
			// btnStore
			// 
			this.btnStore.Location = new System.Drawing.Point(96, 368);
			this.btnStore.Name = "btnStore";
			this.btnStore.TabIndex = 2;
			this.btnStore.Text = "Store";
			// 
			// frmData
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(292, 430);
			this.Controls.Add(this.btnStore);
			this.Controls.Add(this.tabSeen);
			this.Controls.Add(this.tabViews);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "frmData";
			this.Text = "frmData";
			this.Load += new System.EventHandler(this.frmData_Load);
			((System.ComponentModel.ISupportInitialize)(this.tabViews)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabSeen)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// returns the this reference to get access to the class from outside
		/// </summary>
		/// <returns>Reference to frmData</returns>
		public static frmData GetThis()
		{
			return myThis;
		}

		public static NumberFormatInfo NFI
		{
			get
			{
				return myThis.nfi;
			}
		}

		public static string[] OtherSystems
		{
			get
			{
				if (myThis == null)
				{
					Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\ceda.ini");
					String otherSystemsList = myIni.IniReadValue("AIMSViewer","OtherSystems");
					if (otherSystemsList != null && otherSystemsList.Length > 0)
					{
						return otherSystemsList.Split(',');
					}
					else
					{
						return null;
					}

				}
				else
				{
					return myThis.otherSystems;
				}
			}
		}

		public bool UFISSuperUser
		{
			get
			{
				return this.ufisSuperUser;
			}
			set
			{
				this.ufisSuperUser = value;
			}
		}

		public static string GetPrivileges(string strItem)
		{
			if(myThis.UFISSuperUser == true)//MWO: 12.04.05: When /CONNECTED was set then return always "1" for the privileges
				return "1";
			else
				return myThis.axAatLogin1.GetPrivileges(strItem);
		}

		public bool LoadData()
		{
			IDatabase myDB = UT.GetMemDB();
			myDB.IndexUpdateImmediately = true;
			myDB.OnMainThreadBroadcastHandler = new MainThreadBroadcastHandler(SingleThreadedBcEvent);

			// load AIMS viewer general settings
			ITable myTable = myDB.Bind("AGS", "AGS", "URNO,WIDT,HEIG,FLOR,PATH,USEC,USEU,CDAT,LSTU", "10,10,10,3,255,32,32,14,14", "URNO,WIDT,HEIG,FLOR,PATH,USEC,USEU,CDAT,LSTU");

			frmStartup.GetThis().txtCurrentStatus.Text = "Loading AGSTAB (General settings)";
			frmStartup.GetThis().txtCurrentStatus.Refresh();
			myTable.Load("");
			myTable.CreateIndex("URNO", "URNO");
			frmStartup.GetThis().txtDoneStatus.Text = "AGSTAB " + myTable.Count + " Records loaded";
			frmStartup.GetThis().txtDoneStatus.Refresh();

			// load AIMS viewer viewpoint definitions
			myTable = myDB.Bind("AVP", "AVP", "URNO,NAME,XOFF,YOFF,WIDT,HEIG,FLOR,FOVE,PATH,USEC,USEU,CDAT,LSTU", "10,32,10,10,10,10,3,1,255,32,32,14,14", "URNO,NAME,XOFF,YOFF,WIDT,HEIG,FLOR,FOVE,PATH,USEC,USEU,CDAT,LSTU");
			frmStartup.GetThis().txtCurrentStatus.Text = "Loading AVPTAB (Viewpoint definitions)";
			frmStartup.GetThis().txtCurrentStatus.Refresh();
			myTable.Load("ORDER BY NAME");
			myTable.CreateIndex("URNO", "URNO");
			myTable.CreateIndex("NAME", "NAME");
			myTable.CreateIndex("FLOR", "FLOR");
			frmStartup.GetThis().txtDoneStatus.Text = "AVPTAB " + myTable.Count + " Records loaded";
			frmStartup.GetThis().txtDoneStatus.Refresh();

			// load AIMS viewer status group definitions
			myTable = myDB.Bind("ASG", "ASG", "URNO,NAME,SUBS,ORIG,TYPE,IMAG,WIDT,HEIG,USEC,USEU,CDAT,LSTU", "10,32,32,32,32,255,5,5,32,32,14,14", "URNO,NAME,SUBS,ORIG,TYPE,IMAG,WIDT,HEIG,USEC,USEU,CDAT,LSTU");
			frmStartup.GetThis().txtCurrentStatus.Text = "Loading ASGTAB (Status group definitions)";
			frmStartup.GetThis().txtCurrentStatus.Refresh();
			myTable.Load("ORDER BY NAME");
			myTable.CreateIndex("URNO", "URNO");
			myTable.CreateIndex("NAME", "NAME");
			myTable.CreateIndex("SUBS", "SUBS");
			myTable.CreateIndex("ORIG", "ORIG");
			myTable.CreateIndex("TYPE", "TYPE");
			frmStartup.GetThis().txtDoneStatus.Text = "ASGTAB " + myTable.Count + " Records loaded";
			frmStartup.GetThis().txtDoneStatus.Refresh();

			// load AIMS viewer status message definitions
			myTable = myDB.Bind("ASD", "ASD", "URNO,SGRP,IFCO,STAT,COLO,ADVI,USEC,USEU,CDAT,LSTU", "10,10,32,32,20,255,32,32,14,14", "URNO,SGRP,IFCO,STAT,COLO,ADVI,USEC,USEU,CDAT,LSTU");
			frmStartup.GetThis().txtCurrentStatus.Text = "Loading ASGTAB (Status message definitions)";
			frmStartup.GetThis().txtCurrentStatus.Refresh();
			myTable.Load("ORDER BY SGRP");
			myTable.CreateIndex("URNO", "URNO");
			myTable.CreateIndex("SGRP", "SGRP");
			frmStartup.GetThis().txtDoneStatus.Text = "ASDTAB " + myTable.Count + " Records loaded";
			frmStartup.GetThis().txtDoneStatus.Refresh();

			// load AIMS viewer tv camera definitions
			myTable = myDB.Bind("ATV", "ATV", "URNO,NAME,XPO1,YPO1,XPO2,YPO2,FLOR,USEC,USEU,CDAT,LSTU", "10,32,10,10,10,10,3,32,32,14,14", "URNO,NAME,XPO1,YPO1,XPO2,YPO2,FLOR,USEC,USEU,CDAT,LSTU");
			frmStartup.GetThis().txtCurrentStatus.Text = "Loading ATVTAB (TV camera definitions)";
			frmStartup.GetThis().txtCurrentStatus.Refresh();
			myTable.Load("ORDER BY NAME");
			myTable.CreateIndex("URNO", "URNO");
			myTable.CreateIndex("NAME", "NAME");
			myTable.CreateIndex("FLOR", "FLOR");
			frmStartup.GetThis().txtDoneStatus.Text = "ATVTAB " + myTable.Count + " Records loaded";
			frmStartup.GetThis().txtDoneStatus.Refresh();

			// load AIMS viewer current device status definitions
			myTable = myDB.Bind("ACS", "ACS", "URNO,NAME,SUBS,ORIG,DSCR,TYPE,XCOD,YCOD,FLOR,STAT,USEU,LSTU,ID,SEEN", "10,50,32,32,100,32,10,10,3,32,14,128,1", "URNO,NAME,SUBS,ORIG,DSCR,TYPE,XCOD,YCOD,FLOR,STAT,USEU,LSTU");
			frmStartup.GetThis().txtCurrentStatus.Text = "Loading ACSTAB (current device status)";
			frmStartup.GetThis().txtCurrentStatus.Refresh();
			myTable.Load("ORDER BY LSTU");

			for(int i = 0; i < myTable.Count; i++)
			{
				myTable[i]["ID"]   = this.CreateUniqueSymbolId(myTable[i]);
				myTable[i]["SEEN"] = this.HasEventBeenSeen(myTable[i]["URNO"]).ToString();
			}

			myTable.CreateIndex("URNO", "URNO");
			myTable.CreateIndex("NAME", "NAME");
			myTable.CreateIndex("SUBS", "SUBS");
			myTable.CreateIndex("ORIG", "ORIG");
			myTable.CreateIndex("TYPE", "TYPE");
			myTable.CreateIndex("ID", "ID");
			frmStartup.GetThis().txtDoneStatus.Text = "ACSTAB " + myTable.Count + " Records loaded";
			frmStartup.GetThis().txtDoneStatus.Refresh();


			myTable = myDB.Bind("ADC", "ADC", "SUBS,ORIG,DGID,NAME,XCOD,YCOD,FLOR,URNO,CDAT,LSTU,USEC,USEU", "32,32,32,32,10,10,10,10,14,14,32,32", "SUBS,ORIG,DGID,NAME,XCOD,YCOD,FLOR,URNO,CDAT,LSTU,USEC,USEU");
			myTable.CreateIndex("URNO", "URNO");
			myTable.CreateIndex("NAME", "NAME");
			myTable.CreateIndex("SUBS", "SUBS");
			myTable.CreateIndex("ORIG", "ORIG");
			myTable.CreateIndex("DGID", "DGID");
			return true;
									
		}

		public bool GetGeneralSettings(out SizeF size,out int floors,out string path)
		{
			IDatabase myDB = UT.GetMemDB();
			ITable myTable = myDB["AGS"];
			if (myTable.Count == 0)
			{
				size	= new SizeF(1500,1500);
				floors  = 3;
				path	= "c:\\AIMSViewer";	
				return false;
			}
			else
			{
				IRow row = myTable[0];
				size	= new SizeF(float.Parse(row["WIDT"],NumberStyles.Number,nfi),float.Parse(row["HEIG"],NumberStyles.Number,nfi));
				floors	= int.Parse(row["FLOR"]);
				path	= row["PATH"];
				return true;
			}
		}

		public bool SaveGeneralSettings(SizeF size,int floors,string path)
		{
			IDatabase myDB = UT.GetMemDB();
			ITable myTable = myDB["AGS"];
			if (myTable.Count == 0)
			{
				StringBuilder data = new StringBuilder();
				data.Append(myDB.GetNextUrno());
				data.Append(",");
				data.Append(size.Width.ToString("F",nfi));
				data.Append(",");
				data.Append(size.Height.ToString("F",nfi));
				data.Append(",");
				data.Append(floors.ToString());
				data.Append(",");
				data.Append(path);
				data.Append(",");

				data.Append(UT.UserName);
				data.Append(",");
				data.Append(UT.UserName);
				data.Append(",");
				data.Append(UT.DateTimeToCeda(DateTime.Now.ToUniversalTime()));
				data.Append(",");
				data.Append(UT.DateTimeToCeda(DateTime.Now.ToUniversalTime()));

				IRow row = myTable.Create(data.ToString());
				row.Status = State.Created;
				return myTable.Save(row);
			}
			else
			{
				IRow row = myTable[0];
				row["WIDT"] = size.Width.ToString("F",nfi);
				row["HEIG"] = size.Height.ToString("F",nfi);
				row["FLOR"] = floors.ToString();
				row["PATH"] = path;
				row["USEU"] = UT.UserName;
				row["LSTU"] = UT.DateTimeToCeda(DateTime.Now.ToUniversalTime());
				row.Status  = State.Modified;
				return myTable.Save(row);
			}
		}

		public bool	IsValidCameraPosition(RectangleF camera)
		{
			SizeF size;
			int	floors;
			string path;
			if (!this.GetGeneralSettings(out size,out floors,out path))
				return false;
			RectangleF world = new RectangleF(new PointF(0,0),size);
			return world.Contains(camera);
		}

		public bool	IsValidLocation(PointF location)
		{
			SizeF size;
			int	floors;
			string path;
			if (!this.GetGeneralSettings(out size,out floors,out path))
				return false;
			RectangleF world = new RectangleF(new PointF(0,0),size);
			return world.Contains(location);
		}

		public bool	IsValidViewpoint(RectangleF viewpoint)
		{
			SizeF size;
			int	floors;
			string path;
			if (!this.GetGeneralSettings(out size,out floors,out path))
				return false;
			RectangleF world = new RectangleF(new PointF(0,0),size);
			return world.Contains(viewpoint);
		}

		public string GetNextCamera(PointF origin,string floor)
		{
			IDatabase myDB = UT.GetMemDB();
			ITable myTable = myDB["ATV"];
			IRow[] myRows  = myTable.RowsByIndexValue("FLOR",floor);	
			if (myRows != null)
			{
				for (int i = 0; i < myRows.Length; i++)
				{
					IRow myRow = myRows[i];
					PointF	ul = new PointF(float.Parse(myRow["XPO1"],NumberStyles.Number,nfi),float.Parse(myRow["YPO1"],NumberStyles.Number,nfi));
					PointF	lr = new PointF(float.Parse(myRow["XPO2"],NumberStyles.Number,nfi),float.Parse(myRow["YPO2"],NumberStyles.Number,nfi));
					RectangleF rect = new RectangleF(ul,new SizeF(lr.X - ul.X,lr.Y - ul.Y));
					if (rect.Contains(origin))
					{
						return myRow["NAME"];
					}
				}
			}

			return string.Empty;
		}

		public ArrayList GetNextCameras(PointF origin,string floor)
		{
			ArrayList cameras = new ArrayList();
			IDatabase myDB = UT.GetMemDB();
			ITable myTable = myDB["ATV"];
			IRow[] myRows  = myTable.RowsByIndexValue("FLOR",floor);	
			if (myRows != null)
			{
				for (int i = 0; i < myRows.Length; i++)
				{
					IRow myRow = myRows[i];
					PointF	ul = new PointF(float.Parse(myRow["XPO1"],NumberStyles.Number,nfi),float.Parse(myRow["YPO1"],NumberStyles.Number,nfi));
					PointF	lr = new PointF(float.Parse(myRow["XPO2"],NumberStyles.Number,nfi),float.Parse(myRow["YPO2"],NumberStyles.Number,nfi));
					RectangleF rect = new RectangleF(ul,new SizeF(lr.X - ul.X,lr.Y - ul.Y));
					if (rect.Contains(origin))
					{
						cameras.Add ( myRow["NAME"] );
					}
				}
			}
			return cameras;
		}

		public string GetHandlingAdviceForGroup(string acsUrno)
		{
			IDatabase myDB = UT.GetMemDB();
			ITable myTable = myDB["ACS"];
			IRow[] myRows  = myTable.RowsByIndexValue("URNO",acsUrno);	
			if (myRows != null && myRows.Length == 1)
			{
				IRow asgRow = GetStatusGroupFromStatusEvent(myRows[0]);
				if (asgRow == null)
					return string.Empty;
				IRow asdRow = this.GetStatusMessageFromStatusEvent(asgRow["URNO"],myRows[0]);
				if (asdRow == null)
					return string.Empty;
				else
					return asdRow["ADVI"];
			}

			return string.Empty;			
		}

		public string GetHandlingAdviceForStatus(string acsUrno)
		{
			IDatabase myDB = UT.GetMemDB();
			ITable myTable = myDB["ACS"];
			IRow[] myRows  = myTable.RowsByIndexValue("URNO",acsUrno);	
			if (myRows != null && myRows.Length == 1)
			{
				return myRows[0]["DSCR"];
			}

			return string.Empty;			
		}

		public ArrayList GetViewpointDefinitions(PointF origin,string floor)
		{
			ArrayList viewpoints = new ArrayList();
			IDatabase myDB = UT.GetMemDB();
			ITable myTable = myDB["AVP"];
			IRow[] myRows  = myTable.RowsByIndexValue("FLOR",floor);	
			if (myRows != null)
			{
				for (int i = 0; i < myRows.Length; i++)
				{
					IRow myRow = myRows[i];
					if (ViewFilter.IsPassFilter(myRow))
					{
						PointF	ul	 = new PointF(float.Parse(myRow["XOFF"],NumberStyles.Number,nfi),float.Parse(myRow["YOFF"],NumberStyles.Number,nfi));
						SizeF	size = new SizeF(float.Parse(myRow["WIDT"],NumberStyles.Number,nfi),float.Parse(myRow["HEIG"],NumberStyles.Number,nfi));
						RectangleF rect = new RectangleF(ul,size);
						if (rect.Contains(origin))
						{
							viewpoints.Add(myRow["URNO"]);
						}
					}
				}
			}

			return viewpoints;
		}

		public string Clean(string myText,bool forClient)
		{
			string result = myText;
			if (forClient)
			{
				for(int i = 0; i < serverPatchString.Length; i++)
				{
					result = result.Replace(serverPatchString[i],clientPatchString[i]);		
				}
			}
			else
			{
				for(int i = 0; i < clientPatchString.Length; i++)
				{
					result = result.Replace(clientPatchString[i],serverPatchString[i]);		
				}
			}

			return result;
		}

		public	IRow	GetStatusGroupFromStatusEvent(IRow row)
		{
			IDatabase myDB = UT.GetMemDB();
			ITable myTable = myDB["ASG"];
			IRow[] myRows  = myTable.RowsByIndexValue("SUBS",row["SUBS"]);	
			if (myRows != null)
			{
				for (int i = 0; i < myRows.Length; i++)
				{
					if (myRows[i]["ORIG"] == row["ORIG"] && myRows[i]["TYPE"] == row["TYPE"])
//					if (myRows[i]["ORIG"] == row["ORIG"])
					{
						return myRows[i];
					}
				}
			}
			
			return null;												
		}

		public	IRow	GetStatusMessageFromStatusEvent(string statusGroupUrno,IRow row)
		{
			IDatabase myDB = UT.GetMemDB();
			ITable myTable = myDB["ASD"];
			IRow[] myRows  = myTable.RowsByIndexValue("SGRP",statusGroupUrno);	
			if (myRows != null)
			{
				for (int i = 0; i < myRows.Length; i++)
				{
					if (myRows[i]["IFCO"] == row["STAT"])
					{
						return myRows[i];
					}
				}
			}
			
			return null;												
		}

		public  bool IsEventStateToBeDisplayed(string statusGroupUrno,IRow row)
		{
			IDatabase myDB = UT.GetMemDB();
			ITable myTable = myDB["ASD"];
			IRow[] myRows  = myTable.RowsByIndexValue("SGRP",statusGroupUrno);	
			if (myRows != null)
			{
				for (int i = 0; i < myRows.Length; i++)
				{
					if (myRows[i]["IFCO"] == row["STAT"])
					{
						switch(myRows[i]["STAT"])
						{
							case "Attention":
							case "Alarm":
							case "Critical":
							case "Off":
							case "On":
								return true;
						}
					}
				}
			}
			
			return false;			
		}

		public	bool CreateSymbolFromStatusEvent(IRow row,ref PictureViewer.Symbol symbol)
		{
			IRow myASGRow = this.GetStatusGroupFromStatusEvent(row);
			if (myASGRow == null)
			{
				return false;
			}

			// additional security enhancements
			string prv = "Subsystem" + myASGRow["SUBS"];
			if (frmData.GetPrivileges(prv) != "1")
			{
				return false;
			}

			if (!ViewFilter.IsPassFilter(row,myASGRow))
			{
				return false;
			}

			if (!IsEventStateToBeDisplayed(myASGRow["URNO"],row))
			{
				return false;
			}

			IRow myASDRow = this.GetStatusMessageFromStatusEvent(myASGRow["URNO"],row);
			if (myASDRow == null)
			{
				return false;
			}

			symbol.name = CreateUniqueSymbolId(row);
			symbol.toolTip = CreateSymbolToolTip(row);
			symbol.urno = row["URNO"];
			/* HAG */
			if ( row["XCOD"]=="" || row["YCOD"]=="" )
			{
				if ( theErrorList!=null )
					theErrorList.Add ( ref row );
				return false;
			}
			symbol.origin = new PointF(float.Parse(row["XCOD"],NumberStyles.Number,nfi),float.Parse(row["YCOD"],NumberStyles.Number,nfi));
			symbol.floor  = row["FLOR"];

			int ind = this.imageList.IndexOfKey(myASGRow["IMAG"]);
			if (ind < 0)
			{
			again:
				try
				{
					Image image = Image.FromFile(myASGRow["IMAG"]);
					this.imageList.Add(myASGRow["IMAG"],image);
					ind = this.imageList.IndexOfKey(myASGRow["IMAG"]);
				}
				catch(Exception err)
				{
					if (this.IgnoreThisError("1") != DialogResult.Ignore)
					{
						string strMessage = "";
						strMessage += err.Message.ToString() + "\n\n";
						strMessage += "Bitmap file cannot be opened!!\n";
						strMessage += myASGRow["IMAG"] + "\n\n";
						strMessage += "Please copy the airport bitmaps to the correct location!!!\n";
						DialogResult result = MessageBox.Show(strMessage, "Error", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Exclamation,MessageBoxDefaultButton.Button3,MessageBoxOptions.DefaultDesktopOnly);
						switch(result)
						{
							case DialogResult.Abort:
								Application.Exit();
								break;
							case DialogResult.Ignore:
								this.IgnoreThisError("1",DialogResult.Ignore);
								break;
							case DialogResult.Retry:
								goto again;
								break;
						}
					}
					return false;
				}

			}
											
			symbol.image = (Image)this.imageList.GetByIndex(ind);
			symbol.size  = new Size(int.Parse(myASGRow["WIDT"]),int.Parse(myASGRow["HEIG"]));


			int rgb = 0;
			if (myASDRow["COLO"] != string.Empty)
			{
				rgb = int.Parse(myASDRow["COLO"]);
			}

			int b   = (((rgb) >> 16) & 0xff);
			int g	= (((rgb) >> 8) & 0xff);
			int r	= ((rgb) & 0xff);
			symbol.frame = Color.FromArgb(r,g,b);

			return true;
		}

		public ArrayList GetSymbolListFromInitialEventState()
		{
			if (symbolList == null)
			{
				frmStartup.GetThis().txtCurrentStatus.Text = "Creating Status symbols";
				frmStartup.GetThis().txtCurrentStatus.Refresh();

				symbolList = new ArrayList();

				IDatabase myDB = UT.GetMemDB();
				ITable myTable = myDB["ACS"];
				for (int i = 0; i < myTable.Count; i++)
				{
					UpdateSymbolList(myTable[i],State.Created);
				}

				frmStartup.GetThis().txtDoneStatus.Text = "Status symbols " + myTable.Count + " symbols created";
				frmStartup.GetThis().txtDoneStatus.Refresh();
			}
			
			return (ArrayList)symbolList.Clone();
		}

		public void UpdateSymbolList(IRow row,State state)
		{
			if (symbolList == null)
				return;

			if (state == State.Created)
			{
				string symbolName =	CreateUniqueSymbolId(row);
				int ind = this.symbolEvents.IndexOfKey(symbolName);
				if (ind >= 0)
				{
					PictureViewer.Symbol symbol = (PictureViewer.Symbol)this.symbolEvents.GetByIndex(ind);
					if (!this.CreateSymbolFromStatusEvent(row,ref symbol))						
					{
						symbolEvents.RemoveAt(ind);
						symbolList.Remove(symbol);
					}
					else
					{
						this.symbolEvents.SetByIndex(ind,symbol);
					}
				}
				else
				{
					PictureViewer.Symbol symbol = new PictureViewer.Symbol();
					if (this.CreateSymbolFromStatusEvent(row,ref symbol))
					{
						symbolList.Add(symbol);
						symbolEvents.Add(symbolName,symbol);
					}
				}
			}
			else if (state == State.Modified)
			{
				string symbolName =	CreateUniqueSymbolId(row);
				int ind = this.symbolEvents.IndexOfKey(symbolName);
				if (ind >= 0)
				{
					PictureViewer.Symbol symbol = (PictureViewer.Symbol)this.symbolEvents.GetByIndex(ind);
					if (!this.CreateSymbolFromStatusEvent(row,ref symbol))						
					{
						symbolEvents.RemoveAt(ind);
						symbolList.Remove(symbol);
					}
					else
					{
						this.symbolEvents.SetByIndex(ind,symbol);
					}
				}
				else
				{
					PictureViewer.Symbol symbol = new PictureViewer.Symbol();
					if (this.CreateSymbolFromStatusEvent(row,ref symbol))
					{
						symbolList.Add(symbol);
						symbolEvents.Add(symbolName,symbol);
					}
				}
			}
			else if (state == State.Deleted)
			{
				string symbolName =	CreateUniqueSymbolId(row);
				int ind = this.symbolEvents.IndexOfKey(symbolName);
				if (ind >= 0)
				{
					PictureViewer.Symbol symbol = (PictureViewer.Symbol)this.symbolEvents.GetByIndex(ind);
					symbolEvents.RemoveAt(ind);
					symbolList.Remove(symbol);
				}
			}
		}

		public bool UpdatePictureControl(PictureControlEx p1,ArrayList updateList)
		{
			IDatabase myDB = UT.GetMemDB();
			ITable acsTAB  = myDB["ACS"];

			foreach(DatabaseTableEventArgs eventArgs in updateList)
			{
				string symbolName = this.CreateUniqueSymbolId(eventArgs.row);

				if (acsTAB.Command("insert").IndexOf(","+eventArgs.command+",") >= 0)
				{
					int ind = this.symbolEvents.IndexOfKey(symbolName);
					if (ind >= 0)
					{
						PictureViewer.Symbol symbol = (PictureViewer.Symbol)this.symbolEvents.GetByIndex(ind);
						if (p1.Floor == "-99")	// overview 
						{
							if (p1.Dimension.Contains(symbol.origin))
							{
								p1.AddSymbol(symbol,false);
								if (!frmData.GetThis().HasEventBeenSeen(symbol.urno))
								{
									p1.SelectSymbol(symbol);
								}
							}
						}
						else if (symbol.floor == p1.Floor && p1.Dimension.Contains(symbol.origin))
						{
							p1.AddSymbol(symbol,false);
						}
					}
				}
				else if (acsTAB.Command("update").IndexOf(","+eventArgs.command+",") >= 0)
				{
					p1.RemoveSymbol(symbolName,false);
					
					int ind = this.symbolEvents.IndexOfKey(symbolName);
					if (ind >= 0)
					{
						PictureViewer.Symbol symbol = (PictureViewer.Symbol)this.symbolEvents.GetByIndex(ind);
						if (p1.Floor == "-99")	// overview 
						{
							if (p1.Dimension.Contains(symbol.origin))
							{
								p1.AddSymbol(symbol,false);
								if (!frmData.GetThis().HasEventBeenSeen(symbol.urno))
								{
									p1.SelectSymbol(symbol);
								}
							}
						}
						else if (symbol.floor == p1.Floor && p1.Dimension.Contains(symbol.origin))
						{
							p1.AddSymbol(symbol,false);
						}
					}
				}
				else if (acsTAB.Command("delete").IndexOf(","+eventArgs.command+",") >= 0)
				{
					p1.RemoveSymbol(symbolName,false);
				}
			}

			p1.Refresh();
			return true;
		}

		public PictureControlEx CreateOrUpdatePictureControl(PictureControlEx p1,IRow row,ArrayList symbolList,PictureViewer.PictureControlEx.ContextMenuItemHandler contextMenuItem,PictureViewer.PictureControlEx.DoubleClickHandler dblClick)
		{
			PointF offset = new PointF(float.Parse(row["XOFF"],NumberStyles.Number,frmData.NFI),float.Parse(row["YOFF"],NumberStyles.Number,frmData.NFI));
			SizeF  size   = new SizeF (float.Parse(row["WIDT"],NumberStyles.Number,frmData.NFI),float.Parse(row["HEIG"],NumberStyles.Number,frmData.NFI));
			RectangleF	rect = new RectangleF(offset,size);
			string	   floor  = "-99";
			if (row["FOVE"] != "2")
				floor = row["FLOR"];		

			if (p1 == null)
			{
				p1 = new PictureControlEx();
				p1.Dock = System.Windows.Forms.DockStyle.Fill;
				p1.BlinkInterval = this.blinkInterval;
				if (contextMenuItem != null)
				{
					p1.OnContextMenuItem = contextMenuItem;
				}
				if (dblClick != null)
				{
					p1.OnDoubleClickHandler = dblClick;
				}
			}
			else
			{
				p1.ClearSymbols();
			}

			again:
			try
			{
				p1.Image = new Bitmap(row["PATH"]);
			}
			catch(Exception err)
			{
				if (this.IgnoreThisError("2") == DialogResult.OK)
				{
					string strMessage = "";
					strMessage += err.Message.ToString() + "\n\n";
					strMessage += "Bitmap file cannot be opened!!\n";
					strMessage += row["PATH"] + "\n\n";
					strMessage += "Please copy the airport bitmaps to the correct location!!!\n";
					DialogResult result = MessageBox.Show(strMessage, "Error", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Exclamation,MessageBoxDefaultButton.Button3,MessageBoxOptions.DefaultDesktopOnly);
					switch(result)
					{
						case DialogResult.Abort:
							Application.Exit();
							break;
						case DialogResult.Ignore:
							this.IgnoreThisError("2",DialogResult.Ignore);
							p1.Image = null;
							break;
						case DialogResult.Retry:
							p1.Image = null;
							goto again;
					}
				}
			}
			
			p1.Tag	 = row["URNO"];
			if (p1.Image != null)
			{
				p1.SetTransformation(offset,size);
			}

			p1.Floor = floor;

			if (symbolList != null)
			{
				foreach (PictureViewer.Symbol symbol in symbolList)
				{
					if (row["FOVE"] == "2")
					{
						if (rect.Contains(symbol.origin))
						{
							p1.AddSymbol(symbol,false);
							if (!this.HasEventBeenSeen(symbol.urno))
							{
								p1.SelectSymbol(symbol);
							}
						}
					}
					else if (symbol.floor == floor && rect.Contains(symbol.origin))
					{
						p1.AddSymbol(symbol,false);
					}
				}
			}

			return p1;
		}

		public AxTABLib.AxTAB	Views
		{
			get
			{
				return this.tabViews;
			}
		}

		public	string	InitialDirectory
		{
			get
			{
				IDatabase myDB = UT.GetMemDB();
				ITable myTable = myDB["AGS"];
				if (myTable.Count == 0)
				{
					return string.Empty;
				}
				else
				{
					return myTable[0]["PATH"];
				}
			}
		}

		public string PathRelativeToInitialDirectory(string path)
		{
			string initial = this.InitialDirectory;
			if (initial.Length == 0)
				return path;
			if (path.IndexOf(initial) != 0)
				return path;
			return path.Substring(initial.Length);
		}

		public  bool GetSymbolFromUrno(string strACSUrno,ref PictureViewer.Symbol result)
		{
#if	OldBehavior
			foreach(PictureViewer.Symbol symbol in symbolList)
			{
				if (symbol.urno == strACSUrno)
				{
					result = symbol;
					return true;
				}
			}
			return false;
#else
			int ind = this.symbolEvents.IndexOfKey(strACSUrno);
			if (ind >= 0)
			{
				result = (PictureViewer.Symbol)this.symbolEvents.GetByIndex(ind);
				return true;
			}
			else
				return false;
#endif
		}

		public bool HasEventBeenSeen(string strAcsUrno)
		{
			string lines = this.tabSeen.GetLinesByIndexValue("URNO",strAcsUrno,0);
			if (lines.Length == 0)
				return false;
			else
				return true;
		}

		public void EventHasBeenSeen(string strAcsUrno,bool seen)
		{
			bool recalcIndex = false;
			string lines = this.tabSeen.GetLinesByIndexValue("URNO",strAcsUrno,0);
			if (lines.Length == 0)
			{
				if (seen)
				{
					this.tabSeen.InsertTextLine(strAcsUrno,false);
					recalcIndex = true;
					PictureViewer.Symbol symbol = new PictureViewer.Symbol();
					if (this.GetSymbolFromUrno(strAcsUrno,ref symbol))
					{
						DataExchange.Call_SymbolSelection_Changed(this,symbol,false);
					}

				}
			}
			else if (!seen)
			{
				int lineNo = int.Parse(lines);
				this.tabSeen.DeleteLine(lineNo);
				recalcIndex = true;
				PictureViewer.Symbol symbol = new PictureViewer.Symbol();
				if (this.GetSymbolFromUrno(strAcsUrno,ref symbol))
				{
					DataExchange.Call_SymbolSelection_Changed(this,symbol,true);
				}
			}

			if (recalcIndex)
			{
				this.tabSeen.ReorgIndexes();
			}
		}

		public string CreateUniqueSymbolId(IRow row)
		{
#if	OldBehavior
			return row["SUBS"] + row["ORIG"] + row["TYPE"] + row["NAME"];
#else
			return row["URNO"];
#endif
		}

		private string CreateSymbolToolTip(IRow row)
		{
			return "Subs: " + row["SUBS"] + " Orig:" + row["ORIG"] + " Group: " + row["TYPE"] + " Name: " + row["NAME"] + " STATUS: " + row["DSCR"];
		}

		private void frmData_Load(object sender, System.EventArgs e)
		{
			tabViews.ResetContent();
			tabViews.HeaderString = "NAME,ASG,AVP";
			tabViews.LogicalFieldList = "NAME,ASG,AVP";
			tabViews.ReadFromFile("C:\\Ufis\\System\\AIMSViewer_VIEWS.dat");
			tabViews.AutoSizeByHeader = true;
			tabViews.AutoSizeColumns();

			tabSeen.ResetContent();
			tabSeen.HeaderString = "URNO";
			tabSeen.LogicalFieldList = "URNO";
			tabSeen.ColumnWidthString= "10";
			tabSeen.ReadFromFile("C:\\Ufis\\System\\AIMSViewer_SEEN.dat");
			tabSeen.AutoSizeByHeader = true;
			tabSeen.IndexCreate("URNO",0);
//			tabSeen.SetInternalLineBuffer(true);

		}

		private void DataExchange_OnViewNameChanged()
		{
			if (this.symbolList != null)
			{
				this.symbolList.Clear();
				this.symbolEvents.Clear();
				this.symbolList = null;
			}
		}

		private void DataExchange_OnApplication_Shutdown()
		{
			this.tabSeen.WriteToFile("C:\\Ufis\\System\\AIMSViewer_SEEN.dat",false);
		}

		public DialogResult IgnoreThisError(string error)
		{
			int index = this.ignoreErrors.IndexOfKey(error);
			if (index < 0)
				return DialogResult.OK;
			else 
				return (DialogResult)this.ignoreErrors.GetByIndex(index);
		}

		public void IgnoreThisError(string error,DialogResult result)
		{
			this.ignoreErrors.Add(error,result);							
		}

		private void SingleThreadedBcEvent(string pReqId, string pDest1, string pDest2,string pCmd, string pObject, string pSeq,string pTws, string pTwe, string pSelection, string pFields, string pData, string pBcNum,string pAttachment,string pAdditional,SingleThreadedBroadcastHandler singleThreadedBroadcastHandler)
		{
			singleThreadedBroadcastHandler(pReqId,pDest1,pDest2,pCmd,pObject,pSeq,pTws,pTwe,pSelection,pFields,pData,pBcNum,pAttachment,pAdditional);			
		}
	}
}
