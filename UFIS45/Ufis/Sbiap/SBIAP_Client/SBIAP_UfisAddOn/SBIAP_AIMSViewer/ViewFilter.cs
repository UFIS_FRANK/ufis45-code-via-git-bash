using System;
using Ufis.Data;
using Ufis.Utils;

namespace AIMSViewer
{
	/// <summary>
	/// Summary description for ViewFilter.
	/// </summary>
	public class ViewFilter
	{
		private static string currentViewName = "";
		private static int myViewIdx = -1;
		private static IDatabase myDB = null;
		private static ITable myASG = null;
		private static ITable myAVP = null;
		private static AxTABLib.AxTAB myViewsTab = null;
		public static AxAATLOGINLib.AxAatLogin LoginControl = null;

		public ViewFilter()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static string CurrentViewName
		{
			get
			{ 
				return currentViewName;
			}
			set
			{
				DataExchange.Call_BeforeViewNameChanged();

				bool blFound = false;
				currentViewName = value;
				myViewIdx = -1;
				for (int i = 0; i < myViewsTab.GetLineCount() && blFound == false; i++)
				{
					if (currentViewName == myViewsTab.GetFieldValue(i, "NAME"))
					{
						myViewIdx = i;
						blFound = true;
					}
				}
				DataExchange.Call_ViewNameChanged();
			}
		}

		public static void MyInit(AxTABLib.AxTAB pTabViews)
		{
			myViewsTab = pTabViews;
			myDB = UT.GetMemDB(); 
			myASG = myDB["ASG"];
			myAVP = myDB["AVP"];
		}

		public static bool IsPassFilter(IRow aseRow,IRow asgRow)
		{
			bool blRet = false;

			if (currentViewName == "<DEFAULT>" || myViewIdx < 0)
			{
				blRet = true;
			}
			else
			{
				string strViewASG = myViewsTab.GetFieldValue(myViewIdx, "ASG");

				if (asgRow != null)
				{
					if (strViewASG != "")// check only for the specified status groups
					{
						if (strViewASG.IndexOf(asgRow["URNO"], 0, strViewASG.Length) > -1)
						{
							blRet = true;
						}
					}
					else
					{
						blRet = true;
					}
										
				}
				else
				{
					blRet = false;
				}
			}

			return blRet;
		}

		public static bool IsPassFilter(IRow avpRow)
		{
			bool blRet = false;

			if (currentViewName == "<DEFAULT>" || myViewIdx < 0)
			{
				blRet = true;
			}
			else
			{
				string strViewAVP = myViewsTab.GetFieldValue(myViewIdx, "AVP");

				if (avpRow != null)
				{
					if (strViewAVP != "")// check only for the specified view points
					{
						if (strViewAVP.IndexOf(avpRow["URNO"], 0, strViewAVP.Length) > -1)
						{
							blRet = true;
						}
					}
					else
					{
						blRet = true;
					}
										
				}
				else
				{
					blRet = false;
				}
			}

			return blRet;
		}

		public static bool IsPassFilter(string avpUrno)
		{
			bool blRet = false;

			if (currentViewName == "<DEFAULT>" || myViewIdx < 0)
			{
				blRet = true;
			}
			else
			{
				string strViewAVP = myViewsTab.GetFieldValue(myViewIdx, "AVP");

				if (avpUrno != null)
				{
					if (strViewAVP != "")// check only for the specified view points
					{
						if (strViewAVP.IndexOf(avpUrno, 0, strViewAVP.Length) > -1)
						{
							blRet = true;
						}
					}
					else
					{
						blRet = true;
					}
										
				}
				else
				{
					blRet = false;
				}
			}

			return blRet;
		}


		public static bool IsDefaultActive
		{
			get
			{
				return currentViewName == "<DEFAULT>" || myViewIdx < 0 ? true : false;
			}
		}
	}
}
