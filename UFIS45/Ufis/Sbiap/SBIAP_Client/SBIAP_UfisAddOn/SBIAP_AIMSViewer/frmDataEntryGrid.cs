using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Globalization;
using Ufis.Utils;
using Ufis.Data;

namespace AIMSViewer
{
	/// <summary>
	/// Summary description for frmDataEntryGrid.
	/// </summary>
	public class frmDataEntryGrid : System.Windows.Forms.Form
	{
		private System.Windows.Forms.PictureBox pictureBox1;
		private AxTABLib.AxTAB tabData;
		private System.Windows.Forms.Button btnPrint;
		private System.Windows.Forms.Label lblCaption;
		private string currTabelName = "ADC";
		private string myHeaderString = "Subsystem,Originator,Device group,Name,X-Cod,Y-Cod,Flor,URNO";
		private string myHeaderLen	= "100,100,100,100,100,100,100,0";
		private string myCaption	= "Symbol Locations ...";
		private string myFields		= "SUBS,ORIG,DGID,NAME,XCOD,YCOD,FLOR,URNO";
		private string myColumnWidth= "32,32,32,50,10,10,3,10";
		private int myWidth			= -1;
		private int myHeight		= -1;
		private IDatabase  myDB = null;
		private ITable myTable = null;

		private System.Windows.Forms.Button lblAdd;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Button bntDelete;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox cbSubsystem;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Button btnSelect;
		private System.Windows.Forms.TextBox txtOriginator;
		private System.ComponentModel.IContainer components;

		public frmDataEntryGrid()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmDataEntryGrid));
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.tabData = new AxTABLib.AxTAB();
			this.btnPrint = new System.Windows.Forms.Button();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.lblCaption = new System.Windows.Forms.Label();
			this.lblAdd = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnSelect = new System.Windows.Forms.Button();
			this.btnClose = new System.Windows.Forms.Button();
			this.btnSave = new System.Windows.Forms.Button();
			this.bntDelete = new System.Windows.Forms.Button();
			this.panel2 = new System.Windows.Forms.Panel();
			this.panel4 = new System.Windows.Forms.Panel();
			this.panel3 = new System.Windows.Forms.Panel();
			this.label2 = new System.Windows.Forms.Label();
			this.cbSubsystem = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.txtOriginator = new System.Windows.Forms.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.tabData)).BeginInit();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panel4.SuspendLayout();
			this.panel3.SuspendLayout();
			this.SuspendLayout();
			// 
			// pictureBox1
			// 
			this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pictureBox1.Location = new System.Drawing.Point(0, 0);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(680, 38);
			this.pictureBox1.TabIndex = 0;
			this.pictureBox1.TabStop = false;
			this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
			// 
			// tabData
			// 
			this.tabData.ContainingControl = this;
			this.tabData.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabData.Location = new System.Drawing.Point(0, 0);
			this.tabData.Name = "tabData";
			this.tabData.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabData.OcxState")));
			this.tabData.Size = new System.Drawing.Size(680, 312);
			this.tabData.TabIndex = 0;
			this.tabData.CloseInplaceEdit += new AxTABLib._DTABEvents_CloseInplaceEditEventHandler(this.tabData_CloseInplaceEdit);
			this.tabData.HitKeyOnLine += new AxTABLib._DTABEvents_HitKeyOnLineEventHandler(this.tabData_HitKeyOnLine);
			this.tabData.RowSelectionChanged += new AxTABLib._DTABEvents_RowSelectionChangedEventHandler(this.tabData_RowSelectionChanged);
			this.tabData.EditPositionChanged += new AxTABLib._DTABEvents_EditPositionChangedEventHandler(this.tabData_EditPositionChanged);
			// 
			// btnPrint
			// 
			this.btnPrint.BackColor = System.Drawing.Color.Transparent;
			this.btnPrint.Dock = System.Windows.Forms.DockStyle.Right;
			this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnPrint.ImageIndex = 5;
			this.btnPrint.ImageList = this.imageList1;
			this.btnPrint.Location = new System.Drawing.Point(608, 0);
			this.btnPrint.Name = "btnPrint";
			this.btnPrint.Size = new System.Drawing.Size(72, 38);
			this.btnPrint.TabIndex = 5;
			this.btnPrint.Text = "   &Print";
			this.btnPrint.Visible = false;
			// 
			// imageList1
			// 
			this.imageList1.ImageSize = new System.Drawing.Size(19, 19);
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.White;
			// 
			// lblCaption
			// 
			this.lblCaption.BackColor = System.Drawing.Color.Black;
			this.lblCaption.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblCaption.ForeColor = System.Drawing.Color.Lime;
			this.lblCaption.Location = new System.Drawing.Point(0, 0);
			this.lblCaption.Name = "lblCaption";
			this.lblCaption.Size = new System.Drawing.Size(680, 20);
			this.lblCaption.TabIndex = 0;
			this.lblCaption.Text = "Simple Data Editor";
			this.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblAdd
			// 
			this.lblAdd.BackColor = System.Drawing.Color.Transparent;
			this.lblAdd.Dock = System.Windows.Forms.DockStyle.Left;
			this.lblAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.lblAdd.ImageIndex = 0;
			this.lblAdd.ImageList = this.imageList1;
			this.lblAdd.Location = new System.Drawing.Point(0, 0);
			this.lblAdd.Name = "lblAdd";
			this.lblAdd.Size = new System.Drawing.Size(72, 38);
			this.lblAdd.TabIndex = 0;
			this.lblAdd.Text = "&Add";
			this.lblAdd.Click += new System.EventHandler(this.lblAdd_Click);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnSelect);
			this.panel1.Controls.Add(this.btnClose);
			this.panel1.Controls.Add(this.btnSave);
			this.panel1.Controls.Add(this.bntDelete);
			this.panel1.Controls.Add(this.btnPrint);
			this.panel1.Controls.Add(this.lblAdd);
			this.panel1.Controls.Add(this.pictureBox1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 364);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(680, 38);
			this.panel1.TabIndex = 8;
			// 
			// btnSelect
			// 
			this.btnSelect.BackColor = System.Drawing.Color.Transparent;
			this.btnSelect.Dock = System.Windows.Forms.DockStyle.Left;
			this.btnSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnSelect.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnSelect.ImageList = this.imageList1;
			this.btnSelect.Location = new System.Drawing.Point(288, 0);
			this.btnSelect.Name = "btnSelect";
			this.btnSelect.Size = new System.Drawing.Size(72, 38);
			this.btnSelect.TabIndex = 4;
			this.btnSelect.Text = "   &Select";
			this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
			// 
			// btnClose
			// 
			this.btnClose.BackColor = System.Drawing.Color.Transparent;
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnClose.Dock = System.Windows.Forms.DockStyle.Left;
			this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnClose.ImageIndex = 4;
			this.btnClose.ImageList = this.imageList1;
			this.btnClose.Location = new System.Drawing.Point(216, 0);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(72, 38);
			this.btnClose.TabIndex = 3;
			this.btnClose.Text = "   &Close";
			// 
			// btnSave
			// 
			this.btnSave.BackColor = System.Drawing.Color.Transparent;
			this.btnSave.Dock = System.Windows.Forms.DockStyle.Left;
			this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnSave.ImageIndex = 2;
			this.btnSave.ImageList = this.imageList1;
			this.btnSave.Location = new System.Drawing.Point(144, 0);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(72, 38);
			this.btnSave.TabIndex = 2;
			this.btnSave.Text = "   &Save";
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// bntDelete
			// 
			this.bntDelete.BackColor = System.Drawing.Color.Transparent;
			this.bntDelete.Dock = System.Windows.Forms.DockStyle.Left;
			this.bntDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.bntDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.bntDelete.ImageIndex = 1;
			this.bntDelete.ImageList = this.imageList1;
			this.bntDelete.Location = new System.Drawing.Point(72, 0);
			this.bntDelete.Name = "bntDelete";
			this.bntDelete.Size = new System.Drawing.Size(72, 38);
			this.bntDelete.TabIndex = 1;
			this.bntDelete.Text = "    &Delete";
			this.bntDelete.Click += new System.EventHandler(this.bntDelete_Click);
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.panel4);
			this.panel2.Controls.Add(this.panel3);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(0, 20);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(680, 344);
			this.panel2.TabIndex = 9;
			// 
			// panel4
			// 
			this.panel4.Controls.Add(this.tabData);
			this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel4.Location = new System.Drawing.Point(0, 32);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(680, 312);
			this.panel4.TabIndex = 1;
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.txtOriginator);
			this.panel3.Controls.Add(this.label2);
			this.panel3.Controls.Add(this.cbSubsystem);
			this.panel3.Controls.Add(this.label1);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel3.Location = new System.Drawing.Point(0, 0);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(680, 32);
			this.panel3.TabIndex = 0;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(240, 8);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(60, 16);
			this.label2.TabIndex = 2;
			this.label2.Text = "Originator:";
			// 
			// cbSubsystem
			// 
			this.cbSubsystem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbSubsystem.Items.AddRange(new object[] {
															 "BAS",
															 "CASS",
															 "FDA",
															 "SCADA"});
			this.cbSubsystem.Location = new System.Drawing.Point(72, 4);
			this.cbSubsystem.Name = "cbSubsystem";
			this.cbSubsystem.Size = new System.Drawing.Size(156, 21);
			this.cbSubsystem.Sorted = true;
			this.cbSubsystem.TabIndex = 1;
			this.cbSubsystem.SelectedIndexChanged += new System.EventHandler(this.cbSubsystem_SelectedIndexChanged);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(4, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(68, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Subsystem:";
			// 
			// txtOriginator
			// 
			this.txtOriginator.Location = new System.Drawing.Point(308, 4);
			this.txtOriginator.MaxLength = 32;
			this.txtOriginator.Name = "txtOriginator";
			this.txtOriginator.Size = new System.Drawing.Size(156, 20);
			this.txtOriginator.TabIndex = 3;
			this.txtOriginator.Text = "";
			this.txtOriginator.TextChanged += new System.EventHandler(this.cbOriginator_TextChanged);
			// 
			// frmDataEntryGrid
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(680, 402);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.lblCaption);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmDataEntryGrid";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Data Editor ...";
			this.Load += new System.EventHandler(this.frmDataEntryGrid_Load);
			((System.ComponentModel.ISupportInitialize)(this.tabData)).EndInit();
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.panel4.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void pictureBox1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, pictureBox1, Color.WhiteSmoke, Color.Gray);			
		}

		private void frmDataEntryGrid_Load(object sender, System.EventArgs e)
		{
			// security requirements
			switch(frmData.GetPrivileges("StatusLocationsDlgSave"))
			{
				case "-":
					this.btnSave.Enabled = false;
					this.btnSave.Visible = false;
					break;
				case "0":
					this.btnSave.Enabled = false;
					break;
			}

			// add other systems to list of sub systems
			if (frmData.OtherSystems != null)
			{
				this.cbSubsystem.Items.AddRange(frmData.OtherSystems);
			}

			// security requirement for subsystems
			for(int i = this.cbSubsystem.Items.Count - 1; i >= 0; i--)
			{
				string prv = "Subsystem" + (string)this.cbSubsystem.Items[i];
				switch(frmData.GetPrivileges(prv))
				{
					case "-":
						this.cbSubsystem.Items.RemoveAt(i);
						break;
					case "0":
						this.cbSubsystem.Items.RemoveAt(i);
						break;
				}
			}

			myDB = UT.GetMemDB();
			lblCaption.Text = myCaption;
			myTable = myDB[currTabelName];
			tabData.ResetContent();
			tabData.EnableHeaderSizing(true);
			tabData.EnableInlineEdit(true);
			tabData.AutoSizeByHeader = true;
			tabData.InplaceEditUpperCase = false;
			tabData.ShowHorzScroller(true);
			tabData.LifeStyle = true;

			lblAdd.Parent		= pictureBox1;
			btnClose.Parent		= pictureBox1;
			btnSave.Parent		= pictureBox1;
			bntDelete.Parent	= pictureBox1;
			btnSelect.Parent	= pictureBox1;
			btnPrint.Parent		= pictureBox1;

			if (myWidth != -1)
			{
				int ilLeft = this.Left;
				int ilWidth = this.Width;
				this.Width = myWidth;
				this.Left -= (int)(Math.Abs(myWidth - ilWidth ))/2;
			}
			if(myHeight != -1)
			{
				this.Height = myHeight;
			}
			//tabData.CursorLifeStyle = true;
			if(myTable != null)
			{
				myTable.FillVisualizer(tabData);
				tabData.HeaderString = myHeaderString;
				tabData.HeaderLengthString = myHeaderLen;
				tabData.LogicalFieldList = myFields;
			    tabData.FontName = "Arial";
				tabData.FontSize = 14;
				tabData.HeaderFontSize = 14;
				tabData.LineHeight = 18;
				tabData.ColumnWidthString = myColumnWidth;
				tabData.SetTabFontBold(true);

				tabData.InplaceEditUpperCase = false;
				tabData.EnableInlineEdit(true);

				string strColor = UT.colBlue + "," + UT.colBlue;
				tabData.CursorDecoration(tabData.LogicalFieldList, "B,T", "2,2", strColor);
				tabData.DefaultCursor = false;

				string  strNoFocusColumns = "0,1";
				int idx = UT.GetItemNo(myFields, "URNO");
				if(idx > -1)
					strNoFocusColumns += idx.ToString();
				idx = UT.GetItemNo(myFields, "USEC");
				if(idx > -1)
					strNoFocusColumns += "," + idx.ToString();
				idx = UT.GetItemNo(myFields, "USEU");
				if(idx > -1)
					strNoFocusColumns += "," + idx.ToString();
				idx = UT.GetItemNo(myFields, "LSTU");
				if(idx > -1)
				{
					strNoFocusColumns += "," + idx.ToString();
				    tabData.DateTimeSetColumnFormat(idx, "YYYYMMDDhhmmss", "DD'.'MM'.'YY'-'hh':'mm");
				}
				idx = UT.GetItemNo(myFields, "CDAT");
				if(idx > -1)
				{
					strNoFocusColumns += "," + idx.ToString();
					tabData.DateTimeSetColumnFormat(idx, "YYYYMMDDhhmmss", "DD'.'MM'.'YY'-'hh':'mm");
				}
				tabData.NoFocusColumns = strNoFocusColumns;

				for(int i = 0; i < tabData.GetLineCount(); i++)
				{
					tabData.SetLineTag(i, tabData.GetFieldValues(i, tabData.LogicalFieldList));
				}
				tabData.Refresh();

				this.cbSubsystem.SelectedIndex = 0;
			}
		}

		private void lblAdd_Click(object sender, System.EventArgs e)
		{
			string strEmptyLine = "";
			for (int i = 0; i < UT.ItemCount(myHeaderString, ",")-1; i++)
			{
				strEmptyLine += ",";
			}
			tabData.InsertTextLine(strEmptyLine, true);
			tabData.SetColumnValue(tabData.GetLineCount()-1,0,this.cbSubsystem.Text);
			tabData.SetColumnValue(tabData.GetLineCount()-1,1,this.txtOriginator.Text);
			tabData.SetLineColor(tabData.GetLineCount()-1, UT.colBlack, UT.colYellow);
			tabData.OnVScrollTo(tabData.GetLineCount());
		}

		private void CheckValuesChanged(int LineNo)
		{
			int tCol = -1;
			int bCol = -1;
			tabData.GetLineColor(LineNo, ref tCol, ref bCol);
			string strCurrValues = tabData.GetFieldValues(LineNo, tabData.LogicalFieldList);//tabData.GetLineValues(LineNo);
			if(strCurrValues != tabData.GetLineTag(LineNo))
			{
				if(bCol == UT.colWhite)// || bCol == UT.colRed)
				{
					tabData.SetLineColor(LineNo, UT.colBlack, UT.colLightGreen);
					tabData.Refresh();
				}
			}
			else
			{
				if(bCol == UT.colLightGreen)
				{
					tabData.SetLineColor(LineNo, UT.colBlack, UT.colWhite);
					tabData.Refresh();
				}
			}
		}
		private void btnSave_Click(object sender, System.EventArgs e)
		{
			//Call Validation
			string strMessage;
			if (!ValidateData(out strMessage))
			{
				MessageBox.Show(this, strMessage, "Save Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			int tColor = -1;
			int bColor = -1;
			for(int i = 0; i < tabData.GetLineCount(); i++)
			{
				tabData.GetLineColor(i, ref tColor, ref bColor);
				IRow row = null;
				string [] arrFlds = myFields.Split(',');
				if(bColor == UT.colYellow) //New
				{
					row = myTable.CreateEmptyRow();
					for(int j = 0; j < arrFlds.Length; j++)
					{
						if(arrFlds[j] != "URNO" && arrFlds[j] != "LSTU" && 
						   arrFlds[j] != "CDAT" && arrFlds[j] != "USEC" && arrFlds[j] != "USEU")
						{
							row[arrFlds[j]] = tabData.GetColumnValue(i, j);
						}
					}
					string strTime = UT.DateTimeToCeda( DateTime.Now);
					row["CDAT"] = strTime;
					tabData.SetFieldValues(i, "CDAT", strTime);
					row["USEC"] = UT.UserName;
					tabData.SetFieldValues(i, "USEC", UT.UserName);
					tabData.SetFieldValues(i, "URNO", row["URNO"]);
					row.Status = State.Created;
					myTable.Add(row);
					myTable.Save();
					tabData.SetLineColor(i, UT.colBlack , UT.colWhite);
					tabData.Refresh();
				}
				if(bColor == UT.colLightGreen)//Updated
				{
					int idxUrno = UT.GetItemNo(myFields, "URNO");
					if(idxUrno > -1)
					{
						IRow [] rows = myTable.RowsByIndexValue("URNO", tabData.GetColumnValue(i, idxUrno));
						if(rows.Length > 0)
						{
							for(int j = 0; j < arrFlds.Length; j++)
							{
								if(arrFlds[j] != "URNO" && arrFlds[j] != "LSTU" && 
									arrFlds[j] != "CDAT" && arrFlds[j] != "USEC" && arrFlds[j] != "USEU")
								{
									rows[0][arrFlds[j]] = tabData.GetColumnValue(i, j);
								}
							}
							string strTime = UT.DateTimeToCeda( DateTime.Now);
							rows[0]["LSTU"] = strTime;
							tabData.SetFieldValues(i, "LSTU", strTime);
							rows[0]["USEU"] = UT.UserName;
							tabData.SetFieldValues(i, "USEU", UT.UserName);
							rows[0].Status = State.Modified;
						}
					}
					myTable.Save();
					tabData.SetLineColor(i, UT.colBlack , UT.colWhite);
					tabData.Refresh();
				}
				if(bColor == UT.colRed) // Deleted
				{
					int idxUrno = UT.GetItemNo(myFields, "URNO");
					if(idxUrno > -1)
					{
						IRow [] rows = myTable.RowsByIndexValue("URNO", tabData.GetColumnValue(i, idxUrno));
						if(rows.Length > 0)
						{
							rows[0].Status = State.Deleted;
							myTable.Save();
						}
					}
				}
			}
			tabData.Refresh();
			//Remove the deleted (red) lines
			for(int i = tabData.GetLineCount()-1; i >= 0; i--)
			{
				tabData.GetLineColor(i, ref tColor, ref bColor);
				if(bColor == UT.colRed)
				{
					tabData.DeleteLine(i);
				}
			}
			for(int i = 0; i < tabData.GetLineCount(); i++)
			{
				string strValues = tabData.GetFieldValues(i, tabData.LogicalFieldList);//tabData.GetLineValues(i);
				tabData.SetLineTag(i, strValues);
			}
			myTable.ReorganizeIndexes();
			tabData.Refresh();
		}


		private void tabData_CloseInplaceEdit(object sender, AxTABLib._DTABEvents_CloseInplaceEditEvent e)
		{
			CheckValuesChanged(e.lineNo );
		}

		private void tabData_EditPositionChanged(object sender, AxTABLib._DTABEvents_EditPositionChangedEvent e)
		{
			CheckValuesChanged(e.lineNo );
		}

		private void tabData_RowSelectionChanged(object sender, AxTABLib._DTABEvents_RowSelectionChangedEvent e)
		{
			CheckValuesChanged(e.lineNo );
		}

		private void bntDelete_Click(object sender, System.EventArgs e)
		{
			HandleDelete();
		}
		private void HandleDelete()
		{
			int currSel = tabData.GetCurrentSelected();
			int bCol = -1;
			int tCol = -1;
			if(currSel > -1)
			{
				tabData.GetLineColor(currSel, ref tCol, ref bCol);
				if(bCol != UT.colRed)
				{
					tabData.SetLineColor(currSel, UT.colBlack, UT.colRed);
				}
				if(bCol == UT.colYellow)
				{
					tabData.DeleteLine(currSel);
				}
				if(bCol == UT.colRed)
				{
					tabData.SetLineColor(currSel, UT.colBlack, UT.colWhite);
					CheckValuesChanged(currSel);
				}
				tabData.Refresh();
			}
		}

		private void tabData_HitKeyOnLine(object sender, AxTABLib._DTABEvents_HitKeyOnLineEvent e)
		{
			if(e.key == Convert.ToInt16( Keys.Delete))
			{
				HandleDelete();
			}
		}

		private void cbSubsystem_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (this.CheckDataChanged())
			{
				DialogResult olResult;
				olResult = MessageBox.Show(this, "Do you want to save current changes?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question); 
				if (olResult == DialogResult.Yes)
				{
					this.btnSave_Click(sender,e);
				}
				
			}

			if (this.cbSubsystem.SelectedIndex < 0)
				return;

			this.myTable.Clear();
			string where;
			if (this.txtOriginator.Text.Length >= 0)
				where = string.Format("WHERE SUBS = '{0}' AND ORIG = '{1}'",this.cbSubsystem.Text,this.txtOriginator.Text);
			else
				where = string.Format("WHERE SUBS = '{0}' AND ORIG = '{1}'",this.cbSubsystem.Text,this.txtOriginator.Text);
			this.myTable.Load(where);

			this.tabData.ResetContent();
			myTable.FillVisualizer(tabData);
			tabData.HeaderString = myHeaderString;
			tabData.HeaderLengthString = myHeaderLen;
			tabData.LogicalFieldList = myFields;
			tabData.FontName = "Arial";
			tabData.FontSize = 14;
			tabData.HeaderFontSize = 14;
			tabData.LineHeight = 18;
			tabData.ColumnWidthString = myColumnWidth;
			tabData.SetTabFontBold(true);

			tabData.InplaceEditUpperCase = false;
			tabData.EnableInlineEdit(true);

			string strColor = UT.colBlue + "," + UT.colBlue;
			tabData.CursorDecoration(tabData.LogicalFieldList, "B,T", "2,2", strColor);
			tabData.DefaultCursor = false;
			string strNoFocusColumns = "0,1";
			int idx = UT.GetItemNo(myFields, "URNO");
			if(idx > -1)
				strNoFocusColumns += idx.ToString();
			idx = UT.GetItemNo(myFields, "USEC");
			if(idx > -1)
				strNoFocusColumns += "," + idx.ToString();
			idx = UT.GetItemNo(myFields, "USEU");
			if(idx > -1)
				strNoFocusColumns += "," + idx.ToString();
			idx = UT.GetItemNo(myFields, "LSTU");
			if(idx > -1)
			{
				strNoFocusColumns += "," + idx.ToString();
				tabData.DateTimeSetColumnFormat(idx, "YYYYMMDDhhmmss", "DD'.'MM'.'YY'-'hh':'mm");
			}
			idx = UT.GetItemNo(myFields, "CDAT");
			if(idx > -1)
			{
				strNoFocusColumns += "," + idx.ToString();
				tabData.DateTimeSetColumnFormat(idx, "YYYYMMDDhhmmss", "DD'.'MM'.'YY'-'hh':'mm");
			}
			tabData.NoFocusColumns = strNoFocusColumns;

			for(int i = 0; i < tabData.GetLineCount(); i++)
			{
				tabData.SetLineTag(i, tabData.GetFieldValues(i, tabData.LogicalFieldList));
			}
			tabData.Refresh();

		}

		private void cbOriginator_TextChanged(object sender, System.EventArgs e)
		{
			this.cbSubsystem_SelectedIndexChanged(sender,e);		
		}

		private bool CheckDataChanged()
		{
			int tColor = -1;
			int bColor = -1;
			for(int i = 0; i < tabData.GetLineCount(); i++)
			{
				tabData.GetLineColor(i, ref tColor, ref bColor);
				if(bColor == UT.colYellow) //New
					return true;
				if(bColor == UT.colLightGreen)//Updated
					return true;
				if(bColor == UT.colRed) // Deleted
					return true;
			}

			return false;
		}

		private bool ValidateData(out string strMessage)
		{
			strMessage = string.Empty;
			bool	blOK = true;
			int		tCol = -1;
			int		bCol = -1;

			for(int i = 0; i < this.tabData.GetLineCount(); i++)
			{
				this.tabData.GetLineColor(i, ref tCol, ref bCol);
				if(bCol == UT.colYellow || bCol == UT.colLightGreen)
				{
					if (this.tabData.GetFieldValue(i, "SUBS") == "")
					{
						strMessage += "Grid Line " + (i+1).ToString() + ": Subsystem is mandatory!\n";
						blOK = false;
					}
					else if (this.tabData.GetFieldValue(i, "SUBS") != this.cbSubsystem.Text)
					{
						strMessage += "Grid Line " + (i+1).ToString() + ": Subsystem doesn't match!\n";
						blOK = false;
					}

					if (this.tabData.GetFieldValue(i, "ORIG") == "")
					{
						strMessage += "Grid Line " + (i+1).ToString() + ": Originator is mandatory!\n";
						blOK = false;
					}
					else if (this.tabData.GetFieldValue(i, "ORIG") != this.txtOriginator.Text)
					{
						strMessage += "Grid Line " + (i+1).ToString() + ": Originator doesn't match!\n";
						blOK = false;
					}

					if (this.tabData.GetFieldValue(i, "NAME") == "")
					{
						strMessage += "Grid Line " + (i+1).ToString() + ": Name is mandatory!\n";
						blOK = false;
					}

					PointF ul = new PointF(0,0);
					if (this.tabData.GetFieldValue(i, "XCOD") == "")
					{
						strMessage += "Grid Line " + (i+1).ToString() + ": X - Position is mandatory!\n";
						blOK = false;
					}
					else
					{
						try
						{
							ul.X = 	float.Parse(this.tabData.GetFieldValue(i, "XCOD"),NumberStyles.Number,frmData.NFI);
						}
						catch(Exception ex)
						{
							strMessage += "Grid Line " + (i+1).ToString() + ":" + ex.Message + "!\n";
							blOK = false;

						}
					}

					if (this.tabData.GetFieldValue(i, "YCOD") == "")
					{
						strMessage += "Grid Line " + (i+1).ToString() + ": Y - Position is mandatory!\n";
						blOK = false;
					}
					else
					{
						try
						{
							ul.Y = 	float.Parse(this.tabData.GetFieldValue(i, "YCOD"),NumberStyles.Number,frmData.NFI);
						}
						catch(Exception ex)
						{
							strMessage += "Grid Line " + (i+1).ToString() + ":" + ex.Message + "!\n";
							blOK = false;

						}
					}

					if (!frmData.GetThis().IsValidLocation(ul))
					{
						strMessage += "Grid Line " + (i+1).ToString() + ": Location is outside of the world definition!\n";
						blOK = false;
					}

					if (this.tabData.GetFieldValue(i, "FLOR") == "")
					{
						strMessage += "Grid Line " + (i+1).ToString() + ": Floor number is mandatory!\n";
						blOK = false;
					}
					else
					{
						try
						{
							string floor = this.tabData.GetFieldValue(i, "FLOR");
						}
						catch(Exception ex)
						{
							strMessage += "Grid Line " + (i+1).ToString() + ":" + ex.Message + "!\n";
							blOK = false;

						}
					}

				}
			}

			return blOK;
		}

		private void btnSelect_Click(object sender, System.EventArgs e)
		{
			if (this.tabData.GetCurrentSelected() >= 0)
			{
				Location dlg = new Location();
				if (dlg.ShowDialog(this) == DialogResult.OK)
				{
					int LineNo = this.tabData.GetCurrentSelected();
					this.tabData.SetFieldValues(LineNo,"XCOD",dlg.Position.X.ToString("F",frmData.NFI));
					this.tabData.SetFieldValues(LineNo,"YCOD",dlg.Position.Y.ToString("F",frmData.NFI));
					this.tabData.SetFieldValues(LineNo,"FLOR",dlg.Floor);
					this.tabData.Refresh();
					this.CheckValuesChanged(LineNo);
				}
			}
		
		}

	}
}
