using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Globalization;
using Ufis.Utils;
using Ufis.Data;

namespace AIMSViewer
{
	/// <summary>
	/// Summary description for GeneralSettings.
	/// </summary>
	public class frmGeneralSettings : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.PictureBox picTop;
		private System.Windows.Forms.PictureBox picBody;
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Label lblWidth;
		private System.Windows.Forms.Label lblHeight;
		private System.Windows.Forms.NumericUpDown udWidth;
		private System.Windows.Forms.NumericUpDown udHeight;
		private System.Windows.Forms.Label lblInMeters;
		private System.Windows.Forms.Label lblBrace;
		private System.Windows.Forms.Label lblNumberOfFloors;
		private System.Windows.Forms.Label lblCommonPath;
		private System.Windows.Forms.TextBox txtCommonPath;
		private System.Windows.Forms.NumericUpDown udFloors;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.ComponentModel.IContainer components;

		private	IRow	currRow = null;
		private	IRow	oldRow	= null;
		private	IRow	currDBRow = null;
		private	string	myMode	= string.Empty;

		public frmGeneralSettings()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmGeneralSettings));
			this.panelTop = new System.Windows.Forms.Panel();
			this.btnClose = new System.Windows.Forms.Button();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.btnSave = new System.Windows.Forms.Button();
			this.picTop = new System.Windows.Forms.PictureBox();
			this.panelBody = new System.Windows.Forms.Panel();
			this.txtCommonPath = new System.Windows.Forms.TextBox();
			this.lblCommonPath = new System.Windows.Forms.Label();
			this.udFloors = new System.Windows.Forms.NumericUpDown();
			this.lblNumberOfFloors = new System.Windows.Forms.Label();
			this.lblBrace = new System.Windows.Forms.Label();
			this.lblInMeters = new System.Windows.Forms.Label();
			this.udHeight = new System.Windows.Forms.NumericUpDown();
			this.udWidth = new System.Windows.Forms.NumericUpDown();
			this.lblHeight = new System.Windows.Forms.Label();
			this.lblWidth = new System.Windows.Forms.Label();
			this.picBody = new System.Windows.Forms.PictureBox();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.panelTop.SuspendLayout();
			this.panelBody.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.udFloors)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.udHeight)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.udWidth)).BeginInit();
			this.SuspendLayout();
			// 
			// panelTop
			// 
			this.panelTop.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelTop.Controls.Add(this.btnClose);
			this.panelTop.Controls.Add(this.btnSave);
			this.panelTop.Controls.Add(this.picTop);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(0, 0);
			this.panelTop.Name = "panelTop";
			this.panelTop.Size = new System.Drawing.Size(416, 32);
			this.panelTop.TabIndex = 0;
			// 
			// btnClose
			// 
			this.btnClose.BackColor = System.Drawing.Color.Transparent;
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnClose.Dock = System.Windows.Forms.DockStyle.Left;
			this.btnClose.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnClose.ImageIndex = 4;
			this.btnClose.ImageList = this.imageList1;
			this.btnClose.Location = new System.Drawing.Point(72, 0);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(72, 28);
			this.btnClose.TabIndex = 11;
			this.btnClose.Text = "   &Close";
			this.toolTip1.SetToolTip(this.btnClose, "Close this window");
			// 
			// imageList1
			// 
			this.imageList1.ImageSize = new System.Drawing.Size(19, 19);
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.White;
			// 
			// btnSave
			// 
			this.btnSave.BackColor = System.Drawing.Color.Transparent;
			this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnSave.Dock = System.Windows.Forms.DockStyle.Left;
			this.btnSave.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnSave.ImageIndex = 2;
			this.btnSave.ImageList = this.imageList1;
			this.btnSave.Location = new System.Drawing.Point(0, 0);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(72, 28);
			this.btnSave.TabIndex = 10;
			this.btnSave.Text = "   &Save";
			this.toolTip1.SetToolTip(this.btnSave, "Save the current general settings");
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// picTop
			// 
			this.picTop.Dock = System.Windows.Forms.DockStyle.Fill;
			this.picTop.Location = new System.Drawing.Point(0, 0);
			this.picTop.Name = "picTop";
			this.picTop.Size = new System.Drawing.Size(412, 28);
			this.picTop.TabIndex = 0;
			this.picTop.TabStop = false;
			this.picTop.Paint += new System.Windows.Forms.PaintEventHandler(this.picTop_Paint);
			// 
			// panelBody
			// 
			this.panelBody.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelBody.Controls.Add(this.txtCommonPath);
			this.panelBody.Controls.Add(this.lblCommonPath);
			this.panelBody.Controls.Add(this.udFloors);
			this.panelBody.Controls.Add(this.lblNumberOfFloors);
			this.panelBody.Controls.Add(this.lblBrace);
			this.panelBody.Controls.Add(this.lblInMeters);
			this.panelBody.Controls.Add(this.udHeight);
			this.panelBody.Controls.Add(this.udWidth);
			this.panelBody.Controls.Add(this.lblHeight);
			this.panelBody.Controls.Add(this.lblWidth);
			this.panelBody.Controls.Add(this.picBody);
			this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelBody.Location = new System.Drawing.Point(0, 32);
			this.panelBody.Name = "panelBody";
			this.panelBody.Size = new System.Drawing.Size(416, 190);
			this.panelBody.TabIndex = 1;
			// 
			// txtCommonPath
			// 
			this.txtCommonPath.Location = new System.Drawing.Point(128, 132);
			this.txtCommonPath.Name = "txtCommonPath";
			this.txtCommonPath.Size = new System.Drawing.Size(268, 20);
			this.txtCommonPath.TabIndex = 10;
			this.txtCommonPath.Tag = "PATH";
			this.txtCommonPath.Text = "c:\\\\AIMSViewer";
			this.toolTip1.SetToolTip(this.txtCommonPath, "The default location of the AIMS viewer images");
			this.txtCommonPath.TextChanged += new System.EventHandler(this.txtCommonPath_TextChanged);
			this.txtCommonPath.Leave += new System.EventHandler(this.txtCommonPath_TextChanged);
			// 
			// lblCommonPath
			// 
			this.lblCommonPath.BackColor = System.Drawing.Color.Transparent;
			this.lblCommonPath.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblCommonPath.Location = new System.Drawing.Point(16, 136);
			this.lblCommonPath.Name = "lblCommonPath";
			this.lblCommonPath.Size = new System.Drawing.Size(92, 16);
			this.lblCommonPath.TabIndex = 9;
			this.lblCommonPath.Text = "Common Path:";
			// 
			// udFloors
			// 
			this.udFloors.Location = new System.Drawing.Point(128, 92);
			this.udFloors.Maximum = new System.Decimal(new int[] {
																	 15,
																	 0,
																	 0,
																	 0});
			this.udFloors.Minimum = new System.Decimal(new int[] {
																	 1,
																	 0,
																	 0,
																	 0});
			this.udFloors.Name = "udFloors";
			this.udFloors.Size = new System.Drawing.Size(72, 20);
			this.udFloors.TabIndex = 8;
			this.udFloors.Tag = "FLOR";
			this.toolTip1.SetToolTip(this.udFloors, "The total number of floors for this area");
			this.udFloors.Value = new System.Decimal(new int[] {
																   3,
																   0,
																   0,
																   0});
			this.udFloors.ValueChanged += new System.EventHandler(this.txtCommonPath_TextChanged);
			this.udFloors.Leave += new System.EventHandler(this.txtCommonPath_TextChanged);
			// 
			// lblNumberOfFloors
			// 
			this.lblNumberOfFloors.BackColor = System.Drawing.Color.Transparent;
			this.lblNumberOfFloors.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblNumberOfFloors.Location = new System.Drawing.Point(16, 96);
			this.lblNumberOfFloors.Name = "lblNumberOfFloors";
			this.lblNumberOfFloors.Size = new System.Drawing.Size(104, 16);
			this.lblNumberOfFloors.TabIndex = 7;
			this.lblNumberOfFloors.Text = "Number of Floors:";
			// 
			// lblBrace
			// 
			this.lblBrace.BackColor = System.Drawing.Color.Transparent;
			this.lblBrace.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblBrace.Location = new System.Drawing.Point(204, 10);
			this.lblBrace.Name = "lblBrace";
			this.lblBrace.Size = new System.Drawing.Size(24, 56);
			this.lblBrace.TabIndex = 6;
			this.lblBrace.Text = "}";
			// 
			// lblInMeters
			// 
			this.lblInMeters.BackColor = System.Drawing.Color.Transparent;
			this.lblInMeters.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblInMeters.Location = new System.Drawing.Point(236, 36);
			this.lblInMeters.Name = "lblInMeters";
			this.lblInMeters.Size = new System.Drawing.Size(64, 16);
			this.lblInMeters.TabIndex = 5;
			this.lblInMeters.Text = "in meters";
			// 
			// udHeight
			// 
			this.udHeight.DecimalPlaces = 2;
			this.udHeight.Location = new System.Drawing.Point(128, 44);
			this.udHeight.Maximum = new System.Decimal(new int[] {
																	 10000,
																	 0,
																	 0,
																	 0});
			this.udHeight.Minimum = new System.Decimal(new int[] {
																	 100,
																	 0,
																	 0,
																	 0});
			this.udHeight.Name = "udHeight";
			this.udHeight.Size = new System.Drawing.Size(72, 20);
			this.udHeight.TabIndex = 4;
			this.udHeight.Tag = "HEIG";
			this.toolTip1.SetToolTip(this.udHeight, "THe height of the whole area in world units");
			this.udHeight.Value = new System.Decimal(new int[] {
																   1500,
																   0,
																   0,
																   0});
			this.udHeight.ValueChanged += new System.EventHandler(this.txtCommonPath_TextChanged);
			this.udHeight.Leave += new System.EventHandler(this.txtCommonPath_TextChanged);
			// 
			// udWidth
			// 
			this.udWidth.DecimalPlaces = 2;
			this.udWidth.Location = new System.Drawing.Point(128, 20);
			this.udWidth.Maximum = new System.Decimal(new int[] {
																	10000,
																	0,
																	0,
																	0});
			this.udWidth.Minimum = new System.Decimal(new int[] {
																	100,
																	0,
																	0,
																	0});
			this.udWidth.Name = "udWidth";
			this.udWidth.Size = new System.Drawing.Size(72, 20);
			this.udWidth.TabIndex = 3;
			this.udWidth.Tag = "WIDT";
			this.toolTip1.SetToolTip(this.udWidth, "The width of the whole area in world units");
			this.udWidth.Value = new System.Decimal(new int[] {
																  1500,
																  0,
																  0,
																  0});
			this.udWidth.ValueChanged += new System.EventHandler(this.txtCommonPath_TextChanged);
			this.udWidth.Leave += new System.EventHandler(this.txtCommonPath_TextChanged);
			// 
			// lblHeight
			// 
			this.lblHeight.BackColor = System.Drawing.Color.Transparent;
			this.lblHeight.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblHeight.Location = new System.Drawing.Point(16, 48);
			this.lblHeight.Name = "lblHeight";
			this.lblHeight.Size = new System.Drawing.Size(72, 16);
			this.lblHeight.TabIndex = 2;
			this.lblHeight.Text = "Area height:";
			// 
			// lblWidth
			// 
			this.lblWidth.BackColor = System.Drawing.Color.Transparent;
			this.lblWidth.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblWidth.Location = new System.Drawing.Point(16, 24);
			this.lblWidth.Name = "lblWidth";
			this.lblWidth.Size = new System.Drawing.Size(96, 16);
			this.lblWidth.TabIndex = 1;
			this.lblWidth.Text = "Area width:";
			// 
			// picBody
			// 
			this.picBody.Dock = System.Windows.Forms.DockStyle.Fill;
			this.picBody.Location = new System.Drawing.Point(0, 0);
			this.picBody.Name = "picBody";
			this.picBody.Size = new System.Drawing.Size(412, 186);
			this.picBody.TabIndex = 0;
			this.picBody.TabStop = false;
			this.picBody.Paint += new System.Windows.Forms.PaintEventHandler(this.picBody_Paint);
			// 
			// frmGeneralSettings
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 13);
			this.ClientSize = new System.Drawing.Size(416, 222);
			this.Controls.Add(this.panelBody);
			this.Controls.Add(this.panelTop);
			this.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "frmGeneralSettings";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "GeneralSettings";
			this.Load += new System.EventHandler(this.frmGeneralSettings_Load);
			this.panelTop.ResumeLayout(false);
			this.panelBody.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.udFloors)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.udHeight)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.udWidth)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void picTop_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, picTop, Color.WhiteSmoke, Color.LightGray);		
		}

		private void picBody_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, picBody, Color.WhiteSmoke, Color.LightGray);		
		}

		private void frmGeneralSettings_Load(object sender, System.EventArgs e)
		{
			btnClose.Parent		= picTop;
			btnSave.Parent		= picTop;

			lblBrace.Parent		= picBody;
			lblHeight.Parent	= picBody;
			lblInMeters.Parent	= picBody;
			lblNumberOfFloors.Parent = picBody;
			lblWidth.Parent		= picBody;
			lblCommonPath.Parent= picBody;

			// security requirements
			switch(frmData.GetPrivileges("GeneralSettingsDlgSave"))
			{
				case "-":
					this.btnSave.Enabled = false;
					this.btnSave.Visible = false;
					break;
				case "0":
					this.btnSave.Enabled = false;
					break;
			}

			NewGeneralSettings();
			if (!OpenGeneralSettings())
			{
				NewGeneralSettings();
			}
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			this.SaveGeneralSettings();
		}

		private void txtCommonPath_TextChanged(object sender, System.EventArgs e)
		{
			Control ctl = (Control)sender;
			if (ctl.Tag != null)
			{
				string lType = ctl.GetType().ToString();
				if (lType == "System.Windows.Forms.NumericUpDown")
				{
					NumericUpDown udCtrl = (NumericUpDown)ctl;
					if (udCtrl.DecimalPlaces == 0)
					{
						currRow[ctl.Tag.ToString()] = udCtrl.Value.ToString();
					}
					else
					{
						currRow[ctl.Tag.ToString()] = udCtrl.Value.ToString("F",frmData.NFI);
					}
				}
				else
				{
					currRow[ctl.Tag.ToString()] =  frmData.GetThis().Clean(ctl.Text,false);
				}

				CheckDataChanged();
			}
		}

		private bool OpenGeneralSettings()
		{
			this.ClearControls(this);

			IDatabase myDB = UT.GetMemDB();
			ITable	myTable= myDB["AGS"];
			if (myTable.Count > 0)
			{
				string [] arrFields = "PATH,WIDT,HEIG,FLOR".Split(',');
				for(int i = 0; i < arrFields.Length; i++)					
				{
					Control ctl = null;
					ctl = GetControlByTag(arrFields[i], this, ref ctl);
					if (ctl != null)
					{
						string strText = myTable[0][arrFields[i]];
						strText = strText.Trim();

						string lType = ctl.GetType().ToString();
						if (lType == "System.Windows.Forms.NumericUpDown")
						{
							NumericUpDown udCtrl = (NumericUpDown)ctl;
							udCtrl.Value = decimal.Parse(strText,NumberStyles.Number,frmData.NFI);
						}
						else
						{
							ctl.Text = frmData.GetThis().Clean(strText,true);
						}
					}
				}

				currDBRow = myTable[0];
				currRow	  = currDBRow.CopyRaw();
				oldRow	  = currRow.CopyRaw();

				btnSave.BackColor = Color.Transparent;
				this.myMode = "UPDATE";

				return true;
			}
			else
			{
				return false;
			}
			
		}

		void NewGeneralSettings()
		{
			IDatabase myDB = UT.GetMemDB();
			ITable	myTable= myDB["AGS"];

			this.currDBRow = myTable.CreateEmptyRow();
			this.currDBRow["WIDT"] = this.udWidth.Value.ToString("F",frmData.NFI);
			this.currDBRow["HEIG"] = this.udHeight.Value.ToString("F",frmData.NFI);
			this.currDBRow["FLOR"] = this.udFloors.Value.ToString();
			this.currRow   = currDBRow.CopyRaw();
			this.oldRow    = currRow.CopyRaw();
			this.myMode	   = "NEW";

			ClearControls(this);
		}

		private bool SaveGeneralSettings()
		{
			if (btnSave.BackColor == Color.Transparent )
			{
				MessageBox.Show(this, "Nothing to save!!", "Info");
				return true;
			}

			string errMsg;
			if (!this.Validate(out errMsg))
			{
				MessageBox.Show(this,errMsg, "Error");
				return false;
			}

			IDatabase myDB = UT.GetMemDB();
			ITable	myTable= myDB["AGS"];	

			if (this.myMode == "NEW")
			{
				currDBRow.Status = State.Created;
			}
			else if (this.myMode == "UPDATE")
			{
				currDBRow.Status = State.Modified;
			}

			ArrayList arrCurr = currRow.FieldValueList();
			for(int i = 0; i < arrCurr.Count; i++)
			{
				currDBRow[i] = currRow[i];
				oldRow[i]	 = currRow[i]; //To remember the current values, because we are in updatemode from now on.
			}

			// must copy values first before adding the row to the database, else indexes will be invalid because values are empty
			if (this.myMode == "NEW")
			{
				int lineNo = myTable.Add(currDBRow);
			}

			if (myTable.Save())
			{
				myMode = "UPDATE";
				this.btnSave.BackColor = Color.Transparent;
				return true;
			}
			else
				return false;

		}

		void ClearControls(System.Windows.Forms.Control pControl)
		{
			foreach(System.Windows.Forms.Control olC in pControl.Controls )
			{
				string lType = olC.GetType().ToString();
				if (lType == "System.Windows.Forms.PictureBox" || lType == "System.Windows.Forms.Panel")
				{
					ClearControls(olC);
				}
				else if (lType == "System.Windows.Forms.TextBox")
				{
					if (olC.Tag != null && olC.Tag.ToString().Length > 0)
					{
						olC.Text = "";
					}
				}
				else if (lType == "System.Windows.Forms.NumericUpDown")
				{
					if (olC.Tag != null)
					{
						NumericUpDown udCtrl = (NumericUpDown)olC;
						udCtrl.Value = udCtrl.Value;
					}
				}
			}
		}

		private Control GetControlByTag(string tag, Control currCtl, ref Control myControl )
		{
			if(myControl == null)
			{
				foreach(System.Windows.Forms.Control olC in currCtl.Controls )
				{
					string lType = olC.GetType().ToString();
					if(lType == "System.Windows.Forms.PictureBox" || lType == "System.Windows.Forms.Panel")
					{
						if(olC.Tag != null)
							if(olC.Tag.ToString() != tag)
								myControl = GetControlByTag(tag.ToString(), olC, ref myControl);
							else
								myControl = olC;
						else
							myControl = GetControlByTag(tag.ToString(), olC, ref myControl);
					}
					else
					{
						if(olC.Tag != null)
						{
							if (olC.Tag.ToString() == tag)
								myControl = olC;
						}
					}
				}
			}
			return myControl;
		}

		private bool CheckDataChanged()
		{
			bool changed = false;
			ArrayList arrCurr = currRow.FieldValueList();
			ArrayList arrOld  = oldRow.FieldValueList();
			for(int i = 0; i < arrCurr.Count && changed == false; i++)
			{
				if(arrCurr[i].ToString() != arrOld[i].ToString())
				{
					changed = true;
				}
			}

			if (changed)
			{
				if (this.btnSave.Enabled)
				{
					this.btnSave.BackColor = Color.Red;
				}
			}
			else
			{
				this.btnSave.BackColor = Color.Transparent;
			}

			return changed;
		}


		private bool Validate(out string errMsg)
		{
			bool result = true;
			errMsg = string.Empty;

			return result;
		}

	}
}
