using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using System.Globalization;
using Ufis.Utils;
using Ufis.Data;

namespace AIMSViewer
{
	/// <summary>
	/// Summary description for SymbolGroups.
	/// </summary>
	public class Cameras : System.Windows.Forms.Form
	{
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Button bntDelete;
		private System.Windows.Forms.Button lblAdd;
		private AxTABLib.AxTAB tabList;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label lblName;
		private System.Windows.Forms.GroupBox grbRectangles;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button btnNewView;
		private System.Windows.Forms.Button btnPick;
		private System.Windows.Forms.ComboBox cbCameraNames;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.ComponentModel.IContainer components;

		public Cameras()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Cameras));
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.btnClose = new System.Windows.Forms.Button();
			this.btnSave = new System.Windows.Forms.Button();
			this.bntDelete = new System.Windows.Forms.Button();
			this.lblAdd = new System.Windows.Forms.Button();
			this.lblName = new System.Windows.Forms.Label();
			this.cbCameraNames = new System.Windows.Forms.ComboBox();
			this.tabList = new AxTABLib.AxTAB();
			this.panel1 = new System.Windows.Forms.Panel();
			this.grbRectangles = new System.Windows.Forms.GroupBox();
			this.panel2 = new System.Windows.Forms.Panel();
			this.btnPick = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.btnNewView = new System.Windows.Forms.Button();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			((System.ComponentModel.ISupportInitialize)(this.tabList)).BeginInit();
			this.panel1.SuspendLayout();
			this.grbRectangles.SuspendLayout();
			this.panel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// imageList1
			// 
			this.imageList1.ImageSize = new System.Drawing.Size(19, 19);
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.White;
			// 
			// btnClose
			// 
			this.btnClose.BackColor = System.Drawing.Color.Transparent;
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnClose.Dock = System.Windows.Forms.DockStyle.Left;
			this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnClose.ImageIndex = 4;
			this.btnClose.ImageList = this.imageList1;
			this.btnClose.Location = new System.Drawing.Point(160, 0);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(72, 48);
			this.btnClose.TabIndex = 9;
			this.btnClose.Text = "   &Close";
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// btnSave
			// 
			this.btnSave.BackColor = System.Drawing.Color.Transparent;
			this.btnSave.Dock = System.Windows.Forms.DockStyle.Left;
			this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnSave.ImageIndex = 2;
			this.btnSave.ImageList = this.imageList1;
			this.btnSave.Location = new System.Drawing.Point(232, 0);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(72, 48);
			this.btnSave.TabIndex = 10;
			this.btnSave.Text = "   &Save";
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// bntDelete
			// 
			this.bntDelete.BackColor = System.Drawing.Color.Transparent;
			this.bntDelete.Dock = System.Windows.Forms.DockStyle.Left;
			this.bntDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.bntDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.bntDelete.ImageIndex = 1;
			this.bntDelete.ImageList = this.imageList1;
			this.bntDelete.Location = new System.Drawing.Point(88, 0);
			this.bntDelete.Name = "bntDelete";
			this.bntDelete.Size = new System.Drawing.Size(72, 48);
			this.bntDelete.TabIndex = 8;
			this.bntDelete.Text = "    &Delete";
			this.bntDelete.Click += new System.EventHandler(this.bntDelete_Click);
			// 
			// lblAdd
			// 
			this.lblAdd.BackColor = System.Drawing.Color.Transparent;
			this.lblAdd.Dock = System.Windows.Forms.DockStyle.Left;
			this.lblAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.lblAdd.ImageIndex = 0;
			this.lblAdd.ImageList = this.imageList1;
			this.lblAdd.Location = new System.Drawing.Point(0, 0);
			this.lblAdd.Name = "lblAdd";
			this.lblAdd.Size = new System.Drawing.Size(88, 48);
			this.lblAdd.TabIndex = 7;
			this.lblAdd.Text = "  &New Camera";
			this.lblAdd.Click += new System.EventHandler(this.lblAdd_Click);
			// 
			// lblName
			// 
			this.lblName.BackColor = System.Drawing.Color.Transparent;
			this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblName.Location = new System.Drawing.Point(16, 24);
			this.lblName.Name = "lblName";
			this.lblName.Size = new System.Drawing.Size(76, 16);
			this.lblName.TabIndex = 1;
			this.lblName.Text = "Name:";
			// 
			// cbCameraNames
			// 
			this.cbCameraNames.Location = new System.Drawing.Point(96, 20);
			this.cbCameraNames.Name = "cbCameraNames";
			this.cbCameraNames.Size = new System.Drawing.Size(224, 21);
			this.cbCameraNames.TabIndex = 0;
			this.cbCameraNames.Tag = "NAME";
			this.cbCameraNames.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
			// 
			// tabList
			// 
			this.tabList.Location = new System.Drawing.Point(104, 72);
			this.tabList.Name = "tabList";
			this.tabList.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabList.OcxState")));
			this.tabList.Size = new System.Drawing.Size(512, 228);
			this.tabList.TabIndex = 3;
			this.tabList.CloseInplaceEdit += new AxTABLib._DTABEvents_CloseInplaceEditEventHandler(this.tabList_CloseInplaceEdit);
			this.tabList.RowSelectionChanged += new AxTABLib._DTABEvents_RowSelectionChangedEventHandler(this.tabList_RowSelectionChanged);
			this.tabList.EditPositionChanged += new AxTABLib._DTABEvents_EditPositionChangedEventHandler(this.tabList_EditPositionChanged);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnSave);
			this.panel1.Controls.Add(this.btnClose);
			this.panel1.Controls.Add(this.bntDelete);
			this.panel1.Controls.Add(this.lblAdd);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 374);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(640, 48);
			this.panel1.TabIndex = 11;
			this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
			// 
			// grbRectangles
			// 
			this.grbRectangles.BackColor = System.Drawing.Color.Transparent;
			this.grbRectangles.Controls.Add(this.panel2);
			this.grbRectangles.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.grbRectangles.Location = new System.Drawing.Point(96, 56);
			this.grbRectangles.Name = "grbRectangles";
			this.grbRectangles.Size = new System.Drawing.Size(528, 308);
			this.grbRectangles.TabIndex = 2;
			this.grbRectangles.TabStop = false;
			this.grbRectangles.Text = "View areas";
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.btnPick);
			this.panel2.Controls.Add(this.button3);
			this.panel2.Controls.Add(this.btnNewView);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel2.Location = new System.Drawing.Point(3, 257);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(522, 48);
			this.panel2.TabIndex = 12;
			this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
			// 
			// btnPick
			// 
			this.btnPick.BackColor = System.Drawing.Color.Transparent;
			this.btnPick.Dock = System.Windows.Forms.DockStyle.Left;
			this.btnPick.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnPick.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnPick.ImageList = this.imageList1;
			this.btnPick.Location = new System.Drawing.Point(160, 0);
			this.btnPick.Name = "btnPick";
			this.btnPick.Size = new System.Drawing.Size(72, 48);
			this.btnPick.TabIndex = 6;
			this.btnPick.Text = "&Select";
			this.toolTip1.SetToolTip(this.btnPick, "Select camera position ");
			this.btnPick.Click += new System.EventHandler(this.btnPick_Click);
			// 
			// button3
			// 
			this.button3.BackColor = System.Drawing.Color.Transparent;
			this.button3.Dock = System.Windows.Forms.DockStyle.Left;
			this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.button3.ImageIndex = 1;
			this.button3.ImageList = this.imageList1;
			this.button3.Location = new System.Drawing.Point(88, 0);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(72, 48);
			this.button3.TabIndex = 5;
			this.button3.Text = "    &Delete";
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// btnNewView
			// 
			this.btnNewView.BackColor = System.Drawing.Color.Transparent;
			this.btnNewView.Dock = System.Windows.Forms.DockStyle.Left;
			this.btnNewView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnNewView.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnNewView.ImageIndex = 0;
			this.btnNewView.ImageList = this.imageList1;
			this.btnNewView.Location = new System.Drawing.Point(0, 0);
			this.btnNewView.Name = "btnNewView";
			this.btnNewView.Size = new System.Drawing.Size(88, 48);
			this.btnNewView.TabIndex = 4;
			this.btnNewView.Text = "&Add";
			this.btnNewView.Click += new System.EventHandler(this.btnNewView_Click);
			// 
			// Cameras
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(640, 422);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.tabList);
			this.Controls.Add(this.cbCameraNames);
			this.Controls.Add(this.lblName);
			this.Controls.Add(this.grbRectangles);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "Cameras";
			this.Text = "Cameras ...";
			this.Load += new System.EventHandler(this.SymbolGroups_Load);
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.Cameras_Paint);
			((System.ComponentModel.ISupportInitialize)(this.tabList)).EndInit();
			this.panel1.ResumeLayout(false);
			this.grbRectangles.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void SymbolGroups_Load(object sender, System.EventArgs e)
		{
			IDatabase myDB = UT.GetMemDB();
			ITable	myTable= myDB["ATV"];	

			// security requirements
			switch(frmData.GetPrivileges("CamerasDlgSave"))
			{
				case "-":
					this.btnSave.Enabled = false;
					this.btnSave.Visible = false;
					break;
				case "0":
					this.btnSave.Enabled = false;
					break;
			}

			string strColor = "";
			strColor = UT.colBlue.ToString() + "," + UT.colBlue.ToString();
			tabList.ResetContent();
			tabList.FontName = "Arial";
			tabList.FontSize = 14;
			tabList.HeaderString = "UL - X,UL - Y, LR - X,LR - Y,Floor,Urno";
			tabList.SetFieldSeparator(",");
			tabList.LogicalFieldList = "XPO1,YPO1,XPO2,YPO2,FLOR,URNO";
			tabList.HeaderLengthString = "120,120,120,120,50,-1";
			tabList.ColumnWidthString  = "7,7,7,7,2,10";
			tabList.CursorDecoration(tabList.LogicalFieldList, "T,B", "2,2", strColor);
			tabList.LineHeight = 18;


			for (int i = 0; i < myTable.Count;i++)
			{
				IRow myRow = myTable[i];
				string name = myRow["NAME"];
				if (this.cbCameraNames.Items.IndexOf(name) < 0)
				{
					this.cbCameraNames.Items.Add(name);
				}
			}

			tabList.SetTabFontBold(true);
			tabList.DefaultCursor = false;
			tabList.NoFocusColumns = "5";
			tabList.InplaceEditUpperCase = false;
			tabList.EnableInlineEdit(true);
		}

		private void lblAdd_Click(object sender, System.EventArgs e)
		{
			this.NewCamera();		
		}

		private void bntDelete_Click(object sender, System.EventArgs e)
		{
			if (this.HandleDeleteCamera())
			{
				this.NewCamera();
			}
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
		
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			this.SaveCamera();		
		}

		private void comboBox1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (btnSave.BackColor == Color.Red)
			{
				DialogResult olResult;
				olResult = MessageBox.Show(this, "Do you want to save the current camera?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question); 
				if (olResult == DialogResult.Yes)
				{
					if (SaveCamera() == false) 
						return; // Do not NEW!!
				}
			}

			string name = this.cbCameraNames.Text;
			SetATVTab(name);
			btnSave.BackColor = Color.Transparent;
			this.cbCameraNames.DropDownStyle = ComboBoxStyle.DropDownList;
		}

		private void btnNewView_Click(object sender, System.EventArgs e)
		{
			NewCameraView();
		}

		private void button3_Click(object sender, System.EventArgs e)
		{
			this.HandleDeleteCameraView();
			this.CheckDataChanged();
		}

		private void btnPick_Click(object sender, System.EventArgs e)
		{
			if (this.tabList.GetCurrentSelected() >= 0)
			{
				CameraPosition dlg = new CameraPosition();
				if (dlg.ShowDialog(this) == DialogResult.OK)
				{
					int LineNo = this.tabList.GetCurrentSelected();
					this.tabList.SetFieldValues(LineNo,"XPO1",dlg.Position.Left.ToString("F",frmData.NFI));
					this.tabList.SetFieldValues(LineNo,"YPO1",dlg.Position.Top.ToString("F",frmData.NFI));
					this.tabList.SetFieldValues(LineNo,"XPO2",dlg.Position.Right.ToString("F",frmData.NFI));
					this.tabList.SetFieldValues(LineNo,"YPO2",dlg.Position.Bottom.ToString("F",frmData.NFI));
					this.tabList.SetFieldValues(LineNo,"FLOR",dlg.Floor);
					this.tabList.Refresh();
					this.CheckValuesChanged(LineNo);
				}
			}
		}

		private void NewCamera()
		{
			if (btnSave.BackColor == Color.Red )
			{
				DialogResult olResult;
				olResult = MessageBox.Show(this, "Do you want to save the current camera?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question); 
				if ( olResult == DialogResult.Yes)
				{
					if (SaveCamera() == false) 
						return;
				}
			}

			this.tabList.ResetContent();
			this.tabList.Refresh();

			IDatabase myDB = UT.GetMemDB();
			ITable myTab = myDB["ATV"];
			if (myTab != null)
			{
				string strValues = MakeEmptyString(this.tabList.LogicalFieldList,this.tabList.GetFieldSeparator()[0]);
				this.tabList.InsertTextLine(strValues, true);
				int cnt = this.tabList.GetLineCount() - 1;
				string strUrno = myDB.GetNextUrno();
				this.tabList.SetFieldValues(cnt, "URNO", strUrno);
				this.tabList.SetLineColor(cnt, UT.colBlack, UT.colYellow);
				this.tabList.OnVScrollTo(cnt);
				CheckDataChanged();
			}

			this.cbCameraNames.DropDownStyle = ComboBoxStyle.DropDown;
			this.cbCameraNames.Text = "";
			this.cbCameraNames.Refresh();
			this.cbCameraNames.Focus();
		}

		private bool SaveCamera()
		{
			bool isOK = true;
			string strMessage="";
			if (btnSave.BackColor == Color.Transparent )
			{
				MessageBox.Show(this, "Nothing to save!!", "Info");
				return true;
			}

			//Call Validation
			if (!ValidateData(out strMessage))
			{
				MessageBox.Show(this, strMessage, "Save Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				isOK = false;
			}
			else //Save the status messages
			{
				int tColor = -1;
				int bColor = -1;

				IDatabase myDB = UT.GetMemDB();
				ITable myTable = myDB["ATV"];

				string name = this.cbCameraNames.Text;

				for(int i = 0; i < this.tabList.GetLineCount(); i++)
				{
					this.tabList.GetLineColor(i, ref tColor, ref bColor);
					IRow row = null;
					ArrayList arrFlds = myTable.FieldList;
					if (bColor == UT.colYellow) // New
					{
						row = myTable.CreateEmptyRow();
						for(int j = 0; j < arrFlds.Count; j++)
						{
							string strFld = arrFlds[j].ToString();
							if (strFld == "NAME")
							{
								row[strFld] = name;
							}
							else if (strFld != "LSTU" && strFld != "CDAT" && strFld != "USEC" && strFld != "USEU")
							{
								row[strFld] = this.tabList.GetFieldValue(i, strFld);
							}
						}

						string strTime = UT.DateTimeToCeda( DateTime.Now);
						row["CDAT"] = strTime;
						row["USEC"] = UT.UserName;
						this.tabList.SetFieldValues(i, "URNO", row["URNO"]);
						row.Status = State.Created;
						myTable.Add(row);
						if (myTable.Save())
						{
							this.tabList.SetLineColor(i, UT.colBlack , UT.colWhite);
							this.tabList.Refresh();
						}
					}
					else if (bColor == UT.colLightGreen)//Updated
					{
						int idxUrno = UT.GetItemNo(this.tabList.LogicalFieldList, "URNO");
						if(idxUrno > -1)
						{
							IRow [] rows = myTable.RowsByIndexValue("URNO", this.tabList.GetColumnValue(i, idxUrno));
							if(rows.Length > 0)
							{
								for(int j = 0; j < arrFlds.Count; j++)
								{
									string strFld = arrFlds[j].ToString();
									if (strFld == "NAME")
									{
										rows[0][strFld] = name;
									}
									else if(strFld != "URNO" && strFld != "LSTU" && strFld != "CDAT" && strFld != "USEC" && strFld != "USEU")
									{
										rows[0][strFld] = this.tabList.GetFieldValue(i, strFld);
									}
								}

								string strTime = UT.DateTimeToCeda( DateTime.Now);
								rows[0]["LSTU"] = strTime;
								rows[0]["USEU"] = UT.UserName;
								rows[0].Status = State.Modified;
							}
						}
						if (myTable.Save())
						{
							this.tabList.SetLineColor(i, UT.colBlack , UT.colWhite);
							this.tabList.Refresh();
						}
					}
					else if (bColor == UT.colRed) // Deleted
					{
						int idxUrno = UT.GetItemNo(this.tabList.LogicalFieldList, "URNO");
						if(idxUrno > -1)
						{
							IRow [] rows = myTable.RowsByIndexValue("URNO", this.tabList.GetColumnValue(i, idxUrno));
							if(rows.Length > 0)
							{
								rows[0].Status = State.Deleted;
								myTable.Save();
							}
						}
					}
				}

				this.tabList.Refresh();
				//Remove the deleted (red) lines
				for(int i = this.tabList.GetLineCount()-1; i >= 0; i--)
				{
					this.tabList.GetLineColor(i, ref tColor, ref bColor);
					if (bColor == UT.colRed)
					{
						this.tabList.DeleteLine(i);
					}
				}

				for(int i = 0; i < this.tabList.GetLineCount(); i++)
				{
					string strValues = this.tabList.GetFieldValues(i, this.tabList.LogicalFieldList);
					this.tabList.SetLineTag(i, strValues);
				}

				myTable.ReorganizeIndexes();
				this.tabList.SetCurrentSelection(-1);
				this.tabList.Refresh();
				btnSave.BackColor = Color.Transparent;
				this.cbCameraNames.DropDownStyle = ComboBoxStyle.DropDownList;
				this.cbCameraNames.Items.Add(name);
				this.cbCameraNames.Text = name;
			}
			return isOK;
		}

		/// <summary>
		/// Crates an empty string for tab insertion
		/// </summary>
		/// <param name="strValueList">[separator] delimited string </param>
		/// <param name="separator">separator</param>
		/// <returns></returns>
		private string MakeEmptyString(string strValueList, char separator)
		{
			int i = 0;
			string [] strArr = strValueList.Split(separator);
			string strValues = "";
			for(i = 0; i < strArr.Length-1; i++)
			{
				strValues += separator.ToString();
			}
			return strValues;
		}

		private bool CheckDataChanged()
		{
			int tCol = -1;
			int bCol = -1;
			int i = 0;
			bool isChanged = false;

			for( i = 0; i < this.tabList.GetLineCount() && isChanged == false; i++)
			{
				this.tabList.GetLineColor(i, ref tCol, ref bCol);
				if (bCol == UT.colYellow || bCol == UT.colRed || bCol == UT.colLightGreen )
				{
					isChanged = true;
				}
			}

			if (isChanged)
			{
				if (btnSave.Enabled)
				{
					btnSave.BackColor = Color.Red;
				}

				return isChanged;
			}
			else
			{
				btnSave.BackColor = Color.Transparent;
				return isChanged;
			}
		}

		private bool ValidateData(out string strMessage)
		{
			strMessage = string.Empty;
			bool	blOK = true;
			int		tCol = -1;
			int		bCol = -1;

			this.cbCameraNames.Text = this.cbCameraNames.Text.Trim();
			if (this.cbCameraNames.Text.Length == 0)
			{
				strMessage += "Camera name is mandatory\n";
				blOK = false;
			}
			
			for(int i = 0; i < this.tabList.GetLineCount(); i++)
			{
				this.tabList.GetLineColor(i, ref tCol, ref bCol);
				if(bCol == UT.colYellow || bCol == UT.colLightGreen)
				{
					PointF ul = new PointF(0,0);
					if (this.tabList.GetFieldValue(i, "XPO1") == "")
					{
						strMessage += "Grid Line " + (i+1).ToString() + ": X - Position of upper left is mandatory!\n";
						blOK = false;
					}
					else
					{
						try
						{
							ul.X = 	float.Parse(this.tabList.GetFieldValue(i, "XPO1"),NumberStyles.Number,frmData.NFI);
						}
						catch(Exception ex)
						{
							strMessage += "Grid Line " + (i+1).ToString() + ":" + ex.Message + "!\n";
							blOK = false;

						}
					}

					if (this.tabList.GetFieldValue(i, "YPO1") == "")
					{
						strMessage += "Grid Line " + (i+1).ToString() + ": Y - Position of upper left is mandatory!\n";
						blOK = false;
					}
					else
					{
						try
						{
							ul.Y = 	float.Parse(this.tabList.GetFieldValue(i, "YPO1"),NumberStyles.Number,frmData.NFI);
						}
						catch(Exception ex)
						{
							strMessage += "Grid Line " + (i+1).ToString() + ":" + ex.Message + "!\n";
							blOK = false;

						}
					}

					PointF lr = new PointF(0,0);
					if (this.tabList.GetFieldValue(i, "XPO2") == "")
					{
						strMessage += "Grid Line " + (i+1).ToString() + ": X - Position of upper left is mandatory!\n";
						blOK = false;
					}
					else
					{
						try
						{
							lr.X = 	float.Parse(this.tabList.GetFieldValue(i, "XPO2"),NumberStyles.Number,frmData.NFI);
						}
						catch(Exception ex)
						{
							strMessage += "Grid Line " + (i+1).ToString() + ":" + ex.Message + "!\n";
							blOK = false;

						}
					}

					if (this.tabList.GetFieldValue(i, "YPO2") == "")
					{
						strMessage += "Grid Line " + (i+1).ToString() + ": Y - Position of upper left is mandatory!\n";
						blOK = false;
					}
					else
					{
						try
						{
							lr.Y = 	float.Parse(this.tabList.GetFieldValue(i, "YPO2"),NumberStyles.Number,frmData.NFI);
						}
						catch(Exception ex)
						{
							strMessage += "Grid Line " + (i+1).ToString() + ":" + ex.Message + "!\n";
							blOK = false;

						}
					}

					SizeF size = new SizeF(lr.X - ul.X,lr.Y - ul.Y);
					if (size.Width <= 0 || size.Height <= 0)
					{
						strMessage += "Grid Line " + (i+1).ToString() + ":" + "Not a valid rectangle" + "!\n";
						blOK = false;
					}

					RectangleF rect = new RectangleF(ul,size);
					if (!frmData.GetThis().IsValidCameraPosition(rect))
					{
						strMessage += "Grid Line " + (i+1).ToString() + ": Camera position is outside of the world definition!\n";
						blOK = false;
					}

					if (this.tabList.GetFieldValue(i, "FLOR") == "")
					{
						strMessage += "Grid Line " + (i+1).ToString() + ": Floor number is mandatory!\n";
						blOK = false;
					}
					else
					{
						try
						{
							string floor = this.tabList.GetFieldValue(i, "FLOR");
						}
						catch(Exception ex)
						{
							strMessage += "Grid Line " + (i+1).ToString() + ":" + ex.Message + "!\n";
							blOK = false;

						}
					}

				}
			}

			return blOK;
		}

		private void tabList_RowSelectionChanged(object sender, AxTABLib._DTABEvents_RowSelectionChangedEvent e)
		{
			this.CheckValuesChanged(e.lineNo);		
		}

		private void tabList_EditPositionChanged(object sender, AxTABLib._DTABEvents_EditPositionChangedEvent e)
		{
			this.CheckValuesChanged(e.lineNo);		
		}

		private void tabList_CloseInplaceEdit(object sender, AxTABLib._DTABEvents_CloseInplaceEditEvent e)
		{
			this.CheckValuesChanged(e.lineNo);		
		}

		private void CheckValuesChanged(int LineNo)
		{
			int tCol = -1;
			int bCol = -1;
			this.tabList.GetLineColor(LineNo, ref tCol, ref bCol);
			string strCurrValues = this.tabList.GetFieldValues(LineNo, this.tabList.LogicalFieldList);
			if (strCurrValues != this.tabList.GetLineTag(LineNo))
			{
				if(bCol == UT.colWhite)// || bCol == UT.colRed)
				{
					this.tabList.SetLineColor(LineNo, UT.colBlack, UT.colLightGreen);
					this.tabList.Refresh();
				}
			}
			else
			{
				if(bCol == UT.colLightGreen)
				{
					this.tabList.SetLineColor(LineNo, UT.colBlack, UT.colWhite);
					this.tabList.Refresh();
				}
			}
			
			CheckDataChanged();
		}

		private void NewCameraView()
		{
			IDatabase myDB = UT.GetMemDB();
			ITable myTab = myDB["ASD"];
			if (myTab != null)
			{
				string strValues = MakeEmptyString(this.tabList.LogicalFieldList,this.tabList.GetFieldSeparator()[0]);
				this.tabList.InsertTextLine(strValues, true);
				int cnt = this.tabList.GetLineCount() - 1;
				string strUrno = myDB.GetNextUrno();
				this.tabList.SetFieldValues(cnt, "URNO", strUrno);
				this.tabList.SetLineColor(cnt, UT.colBlack, UT.colYellow);
				this.tabList.OnVScrollTo(cnt);
				CheckDataChanged();
			}
		}

		private void HandleDeleteCameraView()
		{
			int currSel = this.tabList.GetCurrentSelected();
			int bCol = -1;
			int tCol = -1;
			if (currSel > -1)
			{
				this.tabList.GetLineColor(currSel, ref tCol, ref bCol);
				if(bCol != UT.colRed && bCol != UT.colOrange)
				{
					this.tabList.SetLineColor(currSel, UT.colBlack, UT.colRed);
				}
				if(bCol == UT.colYellow)
				{
					this.tabList.DeleteLine(currSel);
				}
				if( bCol == UT.colOrange)
				{
					this.tabList.SetLineColor(currSel, UT.colBlack, UT.colLightGreen);
					CheckValuesChanged(currSel);
				}

				if(bCol == UT.colRed )
				{
					this.tabList.SetLineColor(currSel, UT.colBlack, UT.colWhite);
					CheckValuesChanged(currSel);
				}
				this.tabList.Refresh();
			}
		}


		private void SetATVTab(string name)
		{
			this.tabList.ResetContent();

			IDatabase myDB = UT.GetMemDB();
			ITable myTab = myDB["ATV"];
			if (myTab != null)
			{
				IRow[]	myRows = myTab.RowsByIndexValue("NAME",name);
				if (myRows.Length > 0)
				{
					int LineNo = -1;
					for (int i = 0; i < myRows.Length; i++)
					{
						string strValues = MakeEmptyString(this.tabList.LogicalFieldList,this.tabList.GetFieldSeparator()[0]);
						this.tabList.InsertTextLine(strValues, false);
						LineNo = this.tabList.GetLineCount() - 1;
						string strFld = "";
						for(int j = 0; j < myTab.FieldList.Count; j++)
						{
							strFld = myTab.FieldList[j].ToString();
							this.tabList.SetFieldValues(LineNo, strFld, myRows[i][strFld]);
						}	
					}

					for(int i = 0; i < this.tabList.GetLineCount(); i++)
					{
						string strValues = this.tabList.GetFieldValues(i, this.tabList.LogicalFieldList);
						this.tabList.SetLineTag(i, strValues);
					}


					this.tabList.RedrawTab();
				}
			}

		}

		private bool HandleDeleteCamera()
		{
			DialogResult olResult;
			olResult = MessageBox.Show(this, "Do you really want to delete the current camera?", "Question", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
			if (olResult != DialogResult.Yes)
			{
				return false;
			}

			if (this.cbCameraNames.SelectedIndex >= 0)
			{
				string name = (string)this.cbCameraNames.SelectedItem;		
				IDatabase myDB = UT.GetMemDB();
				ITable	myTable= myDB["ATV"];	
				IRow[]	myRows = myTable.RowsByIndexValue("NAME",name);
				if (myRows != null)
				{
					for (int i = 0; i < myRows.Length; i++)
					{
						myRows[i]["USEU"]	= UT.UserName;
						myRows[i]["LSTU"]	= UT.DateTimeToCeda(DateTime.Now.ToUniversalTime());
						myRows[i].Status	= State.Deleted;
					}
					if (myTable.Save())
					{
						this.cbCameraNames.Items.RemoveAt(this.cbCameraNames.SelectedIndex);
						this.btnSave.BackColor = Color.Transparent;
						this.cbCameraNames.DropDownStyle = ComboBoxStyle.DropDown;
						return true;
					}
					else
						return false;
				}
				else
				{
					this.btnSave.BackColor = Color.Transparent;
					this.cbCameraNames.DropDownStyle = ComboBoxStyle.DropDown;
					return true;
				}
			}
			else
			{
				this.btnSave.BackColor = Color.Transparent;
				return true;
			}

		}

		private void panel1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, panel1, Color.WhiteSmoke, Color.LightGray);		
		}

		private void Cameras_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, this, Color.WhiteSmoke, Color.LightGray);		
		}

		private void grbRectangles_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f,grbRectangles, Color.WhiteSmoke, Color.LightGray);		
		}

		private void panel2_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, panel2, Color.WhiteSmoke, Color.LightGray);		
		}

	}
}
