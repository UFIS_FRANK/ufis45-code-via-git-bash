using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Globalization;
using Ufis.Utils;
using Ufis.Data;

namespace AIMSViewer
{
	/// <summary>
	/// Summary description for ViewpointList.
	/// </summary>
	public class ViewpointList : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label lblViewpoints;
		private System.Windows.Forms.Label lblEvent;
		private System.ComponentModel.IContainer components;
		private AxTABLib.AxTAB tabEvent;
		private AxTABLib.AxTAB tabList;


		private	ArrayList eventList;	
		private	ArrayList viewpoints;
		private	AxTABLib._DTABEvents_SendRButtonClickEvent	ev;
		private	AxTABLib.AxTAB								evTable;
		private System.Windows.Forms.Button btnOpen;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.ImageList imageButtons;
		private System.Windows.Forms.ContextMenu contextMenu1;
		private System.Windows.Forms.Button btnClose;

		public ViewpointList(PictureViewer.Symbol eventSymbol,ArrayList viewpoints)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			this.eventList	= new ArrayList();
			this.eventList.Add(eventSymbol);
			this.viewpoints = viewpoints;

			MenuItem item = new MenuItem("Copy",new EventHandler(ContextMenu_Click));
			this.contextMenu1.MenuItems.Add(item);

		}

		public ViewpointList(ArrayList eventList)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			this.viewpoints = null;
			this.eventList	= eventList;

			MenuItem item = new MenuItem("Copy",new EventHandler(ContextMenu_Click));
			this.contextMenu1.MenuItems.Add(item);

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ViewpointList));
			this.lblViewpoints = new System.Windows.Forms.Label();
			this.lblEvent = new System.Windows.Forms.Label();
			this.tabEvent = new AxTABLib.AxTAB();
			this.tabList = new AxTABLib.AxTAB();
			this.btnOpen = new System.Windows.Forms.Button();
			this.imageButtons = new System.Windows.Forms.ImageList(this.components);
			this.btnClose = new System.Windows.Forms.Button();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.contextMenu1 = new System.Windows.Forms.ContextMenu();
			((System.ComponentModel.ISupportInitialize)(this.tabEvent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabList)).BeginInit();
			this.SuspendLayout();
			// 
			// lblViewpoints
			// 
			this.lblViewpoints.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblViewpoints.Location = new System.Drawing.Point(8, 144);
			this.lblViewpoints.Name = "lblViewpoints";
			this.lblViewpoints.TabIndex = 1;
			this.lblViewpoints.Text = "Viewpoints :";
			// 
			// lblEvent
			// 
			this.lblEvent.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblEvent.Location = new System.Drawing.Point(8, 8);
			this.lblEvent.Name = "lblEvent";
			this.lblEvent.TabIndex = 2;
			this.lblEvent.Text = "Status event :";
			// 
			// tabEvent
			// 
			this.tabEvent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.tabEvent.Location = new System.Drawing.Point(8, 32);
			this.tabEvent.Name = "tabEvent";
			this.tabEvent.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabEvent.OcxState")));
			this.tabEvent.Size = new System.Drawing.Size(624, 96);
			this.tabEvent.TabIndex = 3;
			this.toolTip1.SetToolTip(this.tabEvent, "Click on entry to get the view points for this status event below");
			this.tabEvent.SendRButtonClick += new AxTABLib._DTABEvents_SendRButtonClickEventHandler(this.tabEvent_SendRButtonClick);
			this.tabEvent.RowSelectionChanged += new AxTABLib._DTABEvents_RowSelectionChangedEventHandler(this.tabEvent_RowSelectionChanged);
			// 
			// tabList
			// 
			this.tabList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.tabList.Location = new System.Drawing.Point(8, 176);
			this.tabList.Name = "tabList";
			this.tabList.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabList.OcxState")));
			this.tabList.Size = new System.Drawing.Size(624, 152);
			this.tabList.TabIndex = 4;
			this.toolTip1.SetToolTip(this.tabList, "Double click on an view point entry will open the selected view point");
			this.tabList.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabList_SendLButtonDblClick);
			this.tabList.SendRButtonClick += new AxTABLib._DTABEvents_SendRButtonClickEventHandler(this.tabList_SendRButtonClick);
			this.tabList.RowSelectionChanged += new AxTABLib._DTABEvents_RowSelectionChangedEventHandler(this.tabList_RowSelectionChanged);
			// 
			// btnOpen
			// 
			this.btnOpen.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnOpen.ImageIndex = 1;
			this.btnOpen.ImageList = this.imageButtons;
			this.btnOpen.Location = new System.Drawing.Point(243, 344);
			this.btnOpen.Name = "btnOpen";
			this.btnOpen.TabIndex = 5;
			this.btnOpen.Text = " &Open";
			this.toolTip1.SetToolTip(this.btnOpen, "Opens the currently selected view point");
			this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
			// 
			// imageButtons
			// 
			this.imageButtons.ImageSize = new System.Drawing.Size(19, 19);
			this.imageButtons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageButtons.ImageStream")));
			this.imageButtons.TransparentColor = System.Drawing.Color.White;
			// 
			// btnClose
			// 
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnClose.ImageIndex = 0;
			this.btnClose.ImageList = this.imageButtons;
			this.btnClose.Location = new System.Drawing.Point(323, 344);
			this.btnClose.Name = "btnClose";
			this.btnClose.TabIndex = 6;
			this.btnClose.Text = " &Close";
			this.toolTip1.SetToolTip(this.btnClose, "Close this window");
			// 
			// ViewpointList
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(640, 374);
			this.Controls.Add(this.btnClose);
			this.Controls.Add(this.btnOpen);
			this.Controls.Add(this.tabList);
			this.Controls.Add(this.tabEvent);
			this.Controls.Add(this.lblEvent);
			this.Controls.Add(this.lblViewpoints);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "ViewpointList";
			this.Text = "Viewpoint list";
			this.Load += new System.EventHandler(this.ViewpointList_Load);
			((System.ComponentModel.ISupportInitialize)(this.tabEvent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabList)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void ViewpointList_Load(object sender, System.EventArgs e)
		{
			string strColor = "";
			strColor = UT.colBlue.ToString() + "," + UT.colBlue.ToString();

			tabEvent.ResetContent();
			tabEvent.FontName = "Arial";
			tabEvent.FontSize = 14;
			tabEvent.HeaderString = "Device,Subsystem,Originator,Type,Interface code,Time,Urno";
			tabEvent.HeaderLengthString = "100,100,100,80,80,120,-1";
			tabEvent.ColumnWidthString  = "32,32,32,32,32,14,10";
			tabEvent.LogicalFieldList = "NAME,SUBS,ORIG,TYPE,STAT,LSTU,URNO";
			tabEvent.CursorDecoration(tabEvent.LogicalFieldList, "T,B", "2,2", strColor);
			tabEvent.LineHeight = 18;

			int idxField = UT.GetItemNo(tabEvent.LogicalFieldList, "LSTU");
			if (idxField > -1)
			{
				tabEvent.DateTimeSetColumnFormat(idxField, "YYYYMMDDhhmmss", "hh':'mm':'ss'/'DD");
			}

			tabList.ResetContent();
			tabList.FontName = "Arial";
			tabList.FontSize = 14;
			tabList.HeaderString = "Name,View type,Floor,X-Offset,Y-Offset,Width,Height,Urno";
			tabList.LogicalFieldList = "NAME,FOVE,FLOR,XOFF,YOFF,WIDT,HEIG,URNO";
			tabList.HeaderLengthString = "150,100,100,100,100,100,100,-1";
			tabEvent.ColumnWidthString  = "32,32,3,10,10,10,10,10";
			tabList.CursorDecoration(tabList.LogicalFieldList, "T,B", "2,2", strColor);
			tabList.LineHeight = 18;

			IDatabase myDB = UT.GetMemDB();

			ITable	myTable= myDB["ACS"];

			for (int i = 0; i < this.eventList.Count; i++)
			{
				PictureViewer.Symbol eventSymbol = (PictureViewer.Symbol)this.eventList[i];

				IRow[]	myRows = myTable.RowsByIndexValue("URNO",eventSymbol.urno);
				if (myRows != null && myRows.Length == 1)
				{
					string strValues = MakeEmptyString(this.tabEvent.LogicalFieldList, ',');
					this.tabEvent.InsertTextLine(strValues, false);
					int lineNo = this.tabEvent.GetLineCount() - 1;
					string strFld = "";
					for(int j = 0; j < myTable.FieldList.Count; j++)
					{
						strFld = myTable.FieldList[j].ToString();
						this.tabEvent.SetFieldValues(lineNo, strFld, myRows[0][strFld]);
					}	
				}
			}

			if (this.tabEvent.GetLineCount() > 0)
			{
				this.tabEvent.SetCurrentSelection(0);
			}
			else if (this.viewpoints != null)
			{
				myTable= myDB["AVP"];

				for (int i = 0; i < this.viewpoints.Count; i++)
				{
					IRow[] myRows = myTable.RowsByIndexValue("URNO",(string)this.viewpoints[i]);
					if (myRows != null && myRows.Length == 1)
					{
						string strValues = MakeEmptyString(this.tabList.LogicalFieldList, ',');
						this.tabList.InsertTextLine(strValues, false);
						int lineNo = this.tabList.GetLineCount() - 1;
						string strFld = "";
						for(int j = 0; j < myTable.FieldList.Count; j++)
						{
							strFld = myTable.FieldList[j].ToString();
							if (strFld == "FOVE")
							{
								switch(myRows[0][strFld])
								{
									case "2":
										this.tabList.SetFieldValues(lineNo, strFld,"Overview");
										break;
									case "1":
										this.tabList.SetFieldValues(lineNo, strFld,"Floor");
										break;
									default:
										this.tabList.SetFieldValues(lineNo, strFld,"Detail");
										break;
								}
							}
							else
							{
								this.tabList.SetFieldValues(lineNo, strFld, myRows[0][strFld]);
							}
						}	
					}
				}
			}

			this.btnOpen.Enabled = false;

		}

		/// <summary>
		/// Crates an empty string for tab insertion
		/// </summary>
		/// <param name="strValueList">[separator] delimited string </param>
		/// <param name="separator">separator</param>
		/// <returns></returns>
		private string MakeEmptyString(string strValueList, char separator)
		{
			int i = 0;
			string [] strArr = strValueList.Split(separator);
			string strValues = "";
			for(i = 0; i < strArr.Length-1; i++)
			{
				strValues += separator.ToString();
			}
			return strValues;
		}

		private void tabList_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			int eventLineNo = this.tabEvent.GetCurrentSelected();
			int viewLineNo  = e.lineNo;
			if (eventLineNo >= 0 && viewLineNo >= 0)
			{
				if (this.tabList.GetColumnValue(viewLineNo,7) == "0")
				{
					MessageBox.Show(this,"No view point available for the specified status event","Information");
				}
				else
				{
					/*Viewpoint dlg = new Viewpoint();
					dlg.Owner = this.Owner;
					dlg.Urno = this.tabList.GetColumnValue(viewLineNo,7);*/
					Viewpoint dlg ;
					dlg = Viewpoint.ShowViewPoint (this.Owner, this.tabList.GetColumnValue(viewLineNo,7) );

					PictureViewer.Symbol symbol = new PictureViewer.Symbol();
					string strACSUrno = string.Empty;
					if (this.tabEvent.GetCurrentSelected() >= 0)
						strACSUrno = this.tabEvent.GetColumnValue(this.tabEvent.GetCurrentSelected(),6);
					if (frmData.GetThis().GetSymbolFromUrno(strACSUrno,ref symbol))
					{
						dlg.SelectSymbol(symbol);
					}

					dlg.Show();
					this.Close();
					dlg.TopLevel = true;
				}
			}
		}

		private void btnOpen_Click(object sender, System.EventArgs e)
		{
			int eventLineNo = this.tabEvent.GetCurrentSelected();
			int viewLineNo = this.tabList.GetCurrentSelected();
			if (eventLineNo >= 0 && viewLineNo >= 0)
			{
				if (this.tabList.GetColumnValue(viewLineNo,7) == "0")
				{
					MessageBox.Show(this,"No view point available for the specified status event","Information");
				}
				else
				{
					/*Viewpoint dlg = new Viewpoint();
					dlg.Owner = this.Owner;
					dlg.Urno = this.tabList.GetColumnValue(viewLineNo,7);*/
					Viewpoint dlg ;
					dlg = Viewpoint.ShowViewPoint (this.Owner, this.tabList.GetColumnValue(viewLineNo,7) );

					PictureViewer.Symbol eventSymbol = (PictureViewer.Symbol)this.eventList[eventLineNo];
					dlg.SelectSymbol(eventSymbol);
					dlg.Show();
					this.Close();
					dlg.TopLevel = true;
				}
			}
		}

		private void tabList_RowSelectionChanged(object sender, AxTABLib._DTABEvents_RowSelectionChangedEvent e)
		{
			int lineNo = this.tabList.GetCurrentSelected();
			if (lineNo >= 0)
			{
				if (this.tabList.GetColumnValue(lineNo,7) == "0")
					this.btnOpen.Enabled = false;
				else
					this.btnOpen.Enabled = true;
			}
			else
				this.btnOpen.Enabled = false;
		
		}

		private void tabEvent_RowSelectionChanged(object sender, AxTABLib._DTABEvents_RowSelectionChangedEvent e)
		{
			int lineNo = this.tabEvent.GetCurrentSelected();
			if (lineNo >= 0)
			{
				PictureViewer.Symbol symbol = (PictureViewer.Symbol)this.eventList[e.lineNo];
				UpdateViewpointList(symbol);
			}
		
		}

		private void UpdateViewpointList(PictureViewer.Symbol symbol)
		{
			this.tabList.ResetContent();

			this.viewpoints = frmData.GetThis().GetViewpointDefinitions(symbol.origin,symbol.floor);

			IDatabase myDB = UT.GetMemDB();
			ITable myTable= myDB["AVP"];

			if (this.viewpoints.Count == 0)
			{
				string strValues = MakeEmptyString(this.tabList.LogicalFieldList, ',');
				this.tabList.InsertTextLine(strValues, false);
				int lineNo = this.tabList.GetLineCount() - 1;
				string strFld = "URNO";
				this.tabList.SetFieldValues(lineNo, strFld,"0");
				strFld = "NAME";
				this.tabList.SetFieldValues(lineNo, strFld,"No view point available for the specified status event");
			}
			else
			{
				for (int i = 0; i < this.viewpoints.Count; i++)
				{
					IRow[] myRows = myTable.RowsByIndexValue("URNO",(string)this.viewpoints[i]);
					if (myRows != null && myRows.Length == 1)
					{
						string strValues = MakeEmptyString(this.tabList.LogicalFieldList, ',');
						this.tabList.InsertTextLine(strValues, false);
						int lineNo = this.tabList.GetLineCount() - 1;
						string strFld = "";
						for(int j = 0; j < myTable.FieldList.Count; j++)
						{
							strFld = myTable.FieldList[j].ToString();
							if (strFld == "FOVE")
							{
								switch(myRows[0][strFld])
								{
									case "2":
										this.tabList.SetFieldValues(lineNo, strFld,"Overview");
										break;
									case "1":
										this.tabList.SetFieldValues(lineNo, strFld,"Floor");
										break;
									default:
										this.tabList.SetFieldValues(lineNo, strFld,"Detail");
										break;
								}
							}
							else
							{
								this.tabList.SetFieldValues(lineNo, strFld, myRows[0][strFld]);
							}
						}	
					}
				}
			}

			this.tabList.Refresh();
		}

		private void tabEvent_SendRButtonClick(object sender, AxTABLib._DTABEvents_SendRButtonClickEvent e)
		{
			if (e.lineNo >= 0)
			{
				Point pos = Control.MousePosition;
				pos = this.PointToClient(pos);
				this.ev = e;
				this.evTable = this.tabEvent;
				this.contextMenu1.Show(this,pos);
			}
		
		}

		private void tabList_SendRButtonClick(object sender, AxTABLib._DTABEvents_SendRButtonClickEvent e)
		{
			if (e.lineNo >= 0)
			{
				if (this.tabList.GetColumnValue(e.lineNo,7) != "0")
				{
					Point pos = Control.MousePosition;
					pos = this.PointToClient(pos);
					this.ev = e;
					this.evTable = this.tabList;
					this.contextMenu1.Show(this,pos);
				}
			}
		
		}

		private void ContextMenu_Click(object sender, System.EventArgs e)
		{
			MenuItem item = (MenuItem)sender;
			switch(item.Index)
			{
				case 0:	// Copy to clipboard
					string result = this.evTable.GetColumnValue(this.ev.lineNo,this.ev.colNo);
					Clipboard.SetDataObject(result,false);
					break;
				default:
					break;
			}
		}

	}
}
