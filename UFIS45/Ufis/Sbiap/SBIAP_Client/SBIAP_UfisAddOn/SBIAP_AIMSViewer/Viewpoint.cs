using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Globalization;
using Microsoft.Win32;
using Ufis.Utils;
using Ufis.Data;
using PictureViewer;

namespace AIMSViewer
{
	/// <summary>
	/// Summary description for Viewpoint.
	/// </summary>
	public class Viewpoint : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.PictureBox picTop;
		private PictureViewer.PictureControlEx picPreview;
		private System.Windows.Forms.PictureBox picBody;
		private System.ComponentModel.IContainer components;

		private	string	urno = string.Empty;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.CheckBox cbTop;
		private	string	fove = string.Empty;
		private	string	caption = string.Empty;
		private System.Windows.Forms.Label lblUTCTime;
		private System.Windows.Forms.Label lblLocalTime;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.ImageList imageList1;
		private static SortedList openViewPoints = new SortedList();

		public Viewpoint()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			DataExchange.OnACS_Changed +=new AIMSViewer.DataExchange.ACS_Changed(DataExchange_OnACS_Changed);
			DataExchange.OnBeforeViewNameChanged +=new AIMSViewer.DataExchange.BeforeViewNameChanged(DataExchange_OnBeforeViewNameChanged);
			DataExchange.OnViewNameChanged +=new AIMSViewer.DataExchange.ViewNameChanged(DataExchange_OnViewNameChanged);
			DataExchange.OnApplication_Shutdown +=new AIMSViewer.DataExchange.Application_Shutdown(DataExchange_OnApplication_Shutdown);
			DataExchange.OnSymbolSelection_Changed +=new AIMSViewer.DataExchange.SymbolSelection_Changed(DataExchange_OnSymbolSelection_Changed);
			this.picPreview.OnContextMenuItem = new PictureViewer.PictureControlEx.ContextMenuItemHandler(OnContextMenuItem);
			this.picPreview.OnDoubleClickHandler = new PictureViewer.PictureControlEx.DoubleClickHandler(OnDoubleClick);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Viewpoint));
			this.panelTop = new System.Windows.Forms.Panel();
			this.lblLocalTime = new System.Windows.Forms.Label();
			this.lblUTCTime = new System.Windows.Forms.Label();
			this.cbTop = new System.Windows.Forms.CheckBox();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.btnClose = new System.Windows.Forms.Button();
			this.picTop = new System.Windows.Forms.PictureBox();
			this.picPreview = new PictureViewer.PictureControlEx();
			this.picBody = new System.Windows.Forms.PictureBox();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.panelTop.SuspendLayout();
			this.SuspendLayout();
			// 
			// panelTop
			// 
			this.panelTop.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelTop.Controls.Add(this.lblLocalTime);
			this.panelTop.Controls.Add(this.lblUTCTime);
			this.panelTop.Controls.Add(this.cbTop);
			this.panelTop.Controls.Add(this.btnClose);
			this.panelTop.Controls.Add(this.picTop);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(0, 0);
			this.panelTop.Name = "panelTop";
			this.panelTop.Size = new System.Drawing.Size(552, 32);
			this.panelTop.TabIndex = 2;
			// 
			// lblLocalTime
			// 
			this.lblLocalTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.lblLocalTime.BackColor = System.Drawing.Color.Silver;
			this.lblLocalTime.Location = new System.Drawing.Point(428, 8);
			this.lblLocalTime.Name = "lblLocalTime";
			this.lblLocalTime.Size = new System.Drawing.Size(120, 23);
			this.lblLocalTime.TabIndex = 11;
			this.lblLocalTime.Text = "24.10.2003 10:10";
			this.toolTip1.SetToolTip(this.lblLocalTime, "Current local time");
			// 
			// lblUTCTime
			// 
			this.lblUTCTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.lblUTCTime.BackColor = System.Drawing.Color.Silver;
			this.lblUTCTime.ForeColor = System.Drawing.Color.Red;
			this.lblUTCTime.Location = new System.Drawing.Point(304, 8);
			this.lblUTCTime.Name = "lblUTCTime";
			this.lblUTCTime.Size = new System.Drawing.Size(120, 23);
			this.lblUTCTime.TabIndex = 10;
			this.lblUTCTime.Text = "24.10.2003 10:10z";
			this.toolTip1.SetToolTip(this.lblUTCTime, "Current UTC time");
			// 
			// cbTop
			// 
			this.cbTop.Appearance = System.Windows.Forms.Appearance.Button;
			this.cbTop.BackColor = System.Drawing.Color.Transparent;
			this.cbTop.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.cbTop.ImageIndex = 6;
			this.cbTop.ImageList = this.imageList1;
			this.cbTop.Location = new System.Drawing.Point(73, 0);
			this.cbTop.Name = "cbTop";
			this.cbTop.Size = new System.Drawing.Size(72, 28);
			this.cbTop.TabIndex = 9;
			this.cbTop.Text = " &Top";
			this.cbTop.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.toolTip1.SetToolTip(this.cbTop, "Set this window the top most one");
			this.cbTop.CheckedChanged += new System.EventHandler(this.cbTop_CheckedChanged);
			// 
			// imageList1
			// 
			this.imageList1.ImageSize = new System.Drawing.Size(19, 19);
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.White;
			// 
			// btnClose
			// 
			this.btnClose.BackColor = System.Drawing.Color.Transparent;
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnClose.Dock = System.Windows.Forms.DockStyle.Left;
			this.btnClose.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnClose.ImageIndex = 4;
			this.btnClose.ImageList = this.imageList1;
			this.btnClose.Location = new System.Drawing.Point(0, 0);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(72, 28);
			this.btnClose.TabIndex = 8;
			this.btnClose.Text = " &Close";
			this.toolTip1.SetToolTip(this.btnClose, "Close this window");
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// picTop
			// 
			this.picTop.Dock = System.Windows.Forms.DockStyle.Fill;
			this.picTop.Location = new System.Drawing.Point(0, 0);
			this.picTop.Name = "picTop";
			this.picTop.Size = new System.Drawing.Size(548, 28);
			this.picTop.TabIndex = 0;
			this.picTop.TabStop = false;
			this.picTop.Paint += new System.Windows.Forms.PaintEventHandler(this.picTop_Paint);
			// 
			// picPreview
			// 
			this.picPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.picPreview.BackColor = System.Drawing.SystemColors.Control;
			this.picPreview.BlinkInterval = 1000;
			this.picPreview.Floor = "-99";
			this.picPreview.Image = null;
			this.picPreview.Location = new System.Drawing.Point(0, 40);
			this.picPreview.MouseStatus = PictureViewer.MouseStateEx.None;
			this.picPreview.Name = "picPreview";
			this.picPreview.ScaleFactor = new System.Decimal(new int[] {
																		   1,
																		   0,
																		   0,
																		   0});
			this.picPreview.Size = new System.Drawing.Size(552, 536);
			this.picPreview.TabIndex = 46;
			this.picPreview.Paint += new System.Windows.Forms.PaintEventHandler(this.picPreview_Paint);
			// 
			// picBody
			// 
			this.picBody.Dock = System.Windows.Forms.DockStyle.Fill;
			this.picBody.Location = new System.Drawing.Point(0, 0);
			this.picBody.Name = "picBody";
			this.picBody.Size = new System.Drawing.Size(552, 582);
			this.picBody.TabIndex = 47;
			this.picBody.TabStop = false;
			this.picBody.Paint += new System.Windows.Forms.PaintEventHandler(this.picBody_Paint);
			// 
			// timer1
			// 
			this.timer1.Enabled = true;
			this.timer1.Interval = 60000;
			this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
			// 
			// Viewpoint
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 13);
			this.ClientSize = new System.Drawing.Size(552, 582);
			this.Controls.Add(this.picPreview);
			this.Controls.Add(this.panelTop);
			this.Controls.Add(this.picBody);
			this.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "Viewpoint";
			this.Text = "Viewpoint";
			this.Resize += new System.EventHandler(this.Viewpoint_Resize);
			this.Closing += new System.ComponentModel.CancelEventHandler(this.Viewpoint_Closing);
			this.Load += new System.EventHandler(this.Viewpoint_Load);
			this.LocationChanged += new System.EventHandler(this.Viewpoint_LocationChanged);
			this.Closed += new System.EventHandler(this.Viewpoint_Closed);
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.Viewpoint_Paint);
			this.panelTop.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		public string Urno
		{
			get
			{
				return this.urno;
			}
			set
			{
				if (this.urno != value)
				{
					this.urno = value;
					UpdateControl();
				}
			}
		}

		public string Caption
		{
			get
			{
				return this.caption;
			}
			set
			{
				this.caption = value;
			}
		}

		public bool SelectSymbol(PictureViewer.Symbol symbol)
		{
			return this.picPreview.SelectSymbol(symbol);			
		}

		private void UpdateControl()
		{
			ArrayList symbolList = frmData.GetThis().GetSymbolListFromInitialEventState();
			IDatabase myDB = UT.GetMemDB();
			ITable myTable = myDB["AVP"];
			IRow[] myRows  = myTable.RowsByIndexValue("URNO",this.urno);
			if (myRows != null && myRows.Length == 1)
			{
				frmData.GetThis().CreateOrUpdatePictureControl(this.picPreview,myRows[0],symbolList,null,null);
				this.Caption = myRows[0]["NAME"];
				this.Text = "Viewpoint: [" + this.Caption + "] --- Active View: [" + ViewFilter.CurrentViewName + "]";
				if (myRows[0]["FOVE"] == "2")
					this.cbTop.Checked = true;
				else
					this.cbTop.Checked = false;
			}
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			this.Close();		
		}

		private void btnTop_Click(object sender, System.EventArgs e)
		{
			this.TopMost = !this.TopMost;
		}

		private void DataExchange_OnACS_Changed(object sender, ArrayList updateList)
		{
			if (this.urno == string.Empty)
				return;

#if	OldBehavior
			this.picPreview.ClearSymbols();
			ArrayList symbolList = frmData.GetThis().GetSymbolListFromInitialEventState();
			foreach (PictureViewer.Symbol symbol in symbolList)
			{
				if (this.picPreview.Floor == -99)	// overview 
				{
					if (this.picPreview.Dimension.Contains(symbol.origin))
					{
						this.picPreview.AddSymbol(symbol,false);
						if (!frmData.GetThis().HasEventBeenSeen(symbol.urno))
						{
							this.picPreview.SelectSymbol(symbol);
						}
					}
				}
				else if (symbol.floor == this.picPreview.Floor && this.picPreview.Dimension.Contains(symbol.origin))
				{
					this.picPreview.AddSymbol(symbol,false);
				}
			}
			this.picPreview.Refresh();
#else
			frmData.GetThis().UpdatePictureControl(this.picPreview,updateList);
#endif
		}

		private void Viewpoint_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (  openViewPoints.Contains(this.urno) )
				openViewPoints.Remove(this.urno);

			DataExchange.OnACS_Changed			-=new AIMSViewer.DataExchange.ACS_Changed(DataExchange_OnACS_Changed);
			DataExchange.OnViewNameChanged		-=new AIMSViewer.DataExchange.ViewNameChanged(DataExchange_OnViewNameChanged);
			DataExchange.OnApplication_Shutdown -=new AIMSViewer.DataExchange.Application_Shutdown(DataExchange_OnApplication_Shutdown);
			DataExchange.OnSymbolSelection_Changed -=new AIMSViewer.DataExchange.SymbolSelection_Changed(DataExchange_OnSymbolSelection_Changed);

			this.WriteRegistry(this.caption);
		}

		private string OnContextMenuItem(int item,PointF origin,ArrayList selectedSymbols)
		{
			if (item == 0)	// get next camera information
			{
				StatusInformationList dlg = new StatusInformationList(selectedSymbols,"NextCamera");
				dlg.ShowDialog(this);
			}
			else if (item == 1)	// get handling advice for status group
			{
				StatusInformationList dlg = new StatusInformationList(selectedSymbols,"HandlingAdviceForGroup");
				dlg.ShowDialog(this);
			}
			else if (item == 2)	// get handling advice for status event
			{
				StatusInformationList dlg = new StatusInformationList(selectedSymbols,"HandlingAdviceForStatus");
				dlg.ShowDialog(this);
			}
			else if (item == 3)	// unselect symbol
			{
				foreach(PictureViewer.Symbol symbol in selectedSymbols)
				{
					this.picPreview.UnselectSymbol(symbol);
				}
			}
			return string.Empty;				

		}

		private void OnDoubleClick(PointF origin,ArrayList symbols)
		{
			if (symbols.Count == 1)
			{
				PictureViewer.Symbol symbol = (PictureViewer.Symbol)symbols[0];

				if (!frmData.GetThis().HasEventBeenSeen(symbol.urno))
				{
					frmData.GetThis().EventHasBeenSeen(symbol.urno,true);
				}

				ArrayList viewpoints = frmData.GetThis().GetViewpointDefinitions(symbol.origin,symbol.floor);
				if (viewpoints.Count == 1)
				{

					/*Viewpoint dlg = new Viewpoint();
					dlg.Owner = this;
					dlg.Urno = (string)viewpoints[0];*/
					Viewpoint dlg ;
					dlg = ShowViewPoint (this, (string)viewpoints[0]);
					dlg.SelectSymbol(symbol);
					//					DataExchange.Call_SymbolSelection_Changed(this,symbol,true);
					dlg.Show();
				}
				else if (viewpoints.Count > 1)
				{
					ViewpointList dlg = new ViewpointList(symbol,viewpoints);
					dlg.Owner = this;
					dlg.ShowDialog(this);
				}
				else
				{
					MessageBox.Show(this,"No view point available for the specified status event","Information");
				}
			}
			else if (symbols.Count > 1)
			{
				for (int i = 0; i < symbols.Count; i++)
				{
					PictureViewer.Symbol symbol = (PictureViewer.Symbol)symbols[i];

					if (!frmData.GetThis().HasEventBeenSeen(symbol.urno))
					{
						frmData.GetThis().EventHasBeenSeen(symbol.urno,true);
					}
				}

				ViewpointList dlg = new ViewpointList(symbols);
				dlg.Owner = this;
				dlg.ShowDialog(this);
			}
			else
			{
				MessageBox.Show(this,"No status event available at this location","Information");
			}
		}

		private void Viewpoint_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
		}

		private void picTop_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, picTop, Color.WhiteSmoke, Color.LightGray);		
		}

		private void picPreview_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, picPreview, Color.WhiteSmoke, Color.LightGray);		
		}

		private void picBody_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, picBody, Color.WhiteSmoke, Color.LightGray);		
		
		}

		private void Viewpoint_Load(object sender, System.EventArgs e)
		{
			this.btnClose.Parent = this.picTop;
			this.cbTop.Parent	 = this.picTop;
			this.ReadRegistry(this.caption);
			DateTime localTime = DateTime.Now;
			DateTime utcTime = DateTime.UtcNow;
			lblUTCTime.Text = utcTime.ToShortDateString() + " " + utcTime.ToShortTimeString() + "z";
			lblLocalTime.Text = localTime.ToShortDateString() + " " + localTime.ToShortTimeString();
			this.TopLevel = true;
			this.Refresh();
		}

		private void DataExchange_OnViewNameChanged()
		{
			if (this.urno == string.Empty)
				return;

			// check, if our viewpoint matches the view filter criteria
			if (ViewFilter.IsPassFilter(this.urno) == false)
			{
				this.Close();
			}
			else
			{
				this.picPreview.ClearSymbols();
				ArrayList symbolList = frmData.GetThis().GetSymbolListFromInitialEventState();
				foreach (PictureViewer.Symbol symbol in symbolList)
				{
					if (this.picPreview.Floor == "-99")	// overview 
					{
						if (this.picPreview.Dimension.Contains(symbol.origin))
						{
							this.picPreview.AddSymbol(symbol,false);
							if (!frmData.GetThis().HasEventBeenSeen(symbol.urno))
							{
								this.picPreview.SelectSymbol(symbol);
							}
						}
					}
					else if (symbol.floor == this.picPreview.Floor && this.picPreview.Dimension.Contains(symbol.origin))
					{
						this.picPreview.AddSymbol(symbol,false);
					}
				}

				this.picPreview.Refresh();
				this.Text = "Viewpoint: [" + this.Caption + "] --- Active View: [" + ViewFilter.CurrentViewName + "]";
			}
		}

		private void cbTop_CheckedChanged(object sender, System.EventArgs e)
		{
			this.TopMost = this.cbTop.Checked;
			if (this.cbTop.Checked)
			{
				this.cbTop.BackColor = Color.LightGreen;
				this.cbTop.Refresh();
			}
			else
			{
				this.cbTop.BackColor = Color.Transparent;
				this.cbTop.Refresh();
			}

		}

		private void ReadRegistry(string windowName)
		{
			int myX=-1, myY=-1, myW=-1, myH=-1;
			string regVal = "";
			string theKey = "Software\\AIMSViewer\\" + ViewFilter.CurrentViewName + "\\" + windowName;
			RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			if(rk != null)
			{
				regVal = rk.GetValue("X", "-1").ToString();
				myX = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Y", "-1").ToString();
				myY = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Width", "-1").ToString();
				myW = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Height", "-1").ToString();
				myH = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Top", "-1").ToString();
				if (regVal != "-1")
				{
					this.cbTop.Checked = bool.Parse(regVal);
				}

				regVal = rk.GetValue("Scale","-1").ToString();
				if (regVal != "-1")
				{
					this.picPreview.ScaleFactor = decimal.Parse(regVal);	
				}
			}

			if (myX >= 0 && myY >= 0 && myW != -1 && myH != -1)
			{
				this.Left = myX;
				this.Top = myY;
				this.Width = myW;
				this.Height = myH;
			}
			else if (myX == -4 && myY == -4)
			{
				this.WindowState = FormWindowState.Maximized;
			}
			else if (myX == -32000 && myY == -32000)
			{
				this.WindowState = FormWindowState.Minimized;
			}
		}

		private void WriteRegistry(string windowName)
		{
			string theKey = "Software\\AIMSViewer\\" + ViewFilter.CurrentViewName + "\\" + windowName;
			RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			if(rk == null)
			{
				rk = Registry.CurrentUser.OpenSubKey("Software\\AIMSViewer\\" + ViewFilter.CurrentViewName, true);
				if (rk == null)
				{
					rk = Registry.CurrentUser.OpenSubKey("Software\\AIMSViewer", true);
					if (rk == null)
					{
						rk = Registry.CurrentUser.OpenSubKey("Software", true);
						rk.CreateSubKey("AIMSViewer");
						rk.Close();
						rk = Registry.CurrentUser.OpenSubKey("Software\\AIMSViewer", true);
					}

					rk.CreateSubKey(ViewFilter.CurrentViewName);
					rk.Close();
					rk = Registry.CurrentUser.OpenSubKey("Software\\AIMSViewer\\" + ViewFilter.CurrentViewName, true);
				}

				rk.CreateSubKey(windowName);
				rk.Close();
				theKey = "Software\\AIMSViewer\\" + ViewFilter.CurrentViewName + "\\" + windowName;
				rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			}

			rk.SetValue("X", this.Left.ToString());
			rk.SetValue("Y", this.Top.ToString());
			rk.SetValue("Width", this.Width.ToString());
			rk.SetValue("Height", this.Height.ToString());
			rk.SetValue("Top",this.cbTop.Checked.ToString());
			rk.SetValue("Scale",this.picPreview.ScaleFactor.ToString());
		}

		private void Viewpoint_Closed(object sender, System.EventArgs e)
		{
		}

		private void Viewpoint_LocationChanged(object sender, System.EventArgs e)
		{
		}

		private void Viewpoint_Resize(object sender, System.EventArgs e)
		{
		}

		private void timer1_Tick(object sender, System.EventArgs e)
		{
			DateTime localTime = DateTime.Now;
			DateTime utcTime = DateTime.UtcNow;
			lblUTCTime.Text = utcTime.ToShortDateString() + " " + utcTime.ToShortTimeString() + "z";
			lblUTCTime.Refresh();
			lblLocalTime.Text = localTime.ToShortDateString() + " " + localTime.ToShortTimeString();
			lblLocalTime.Refresh();
		
		}

		private void DataExchange_OnApplication_Shutdown()
		{
			this.WriteRegistry(this.caption);
		}

		private void DataExchange_OnSymbolSelection_Changed(object sender,PictureViewer.Symbol symbol, bool selected)
		{
			if (selected)
				this.picPreview.SelectSymbol(symbol);
			else
				this.picPreview.UnselectSymbol(symbol);
		}

		protected override void OnResize(EventArgs e)
		{
			switch(this.WindowState)
			{
				case FormWindowState.Maximized:
					break;
				case FormWindowState.Minimized:
					break;
				case FormWindowState.Normal:
					break;
			}
			base.OnResize (e);
		}

		private void DataExchange_OnBeforeViewNameChanged()
		{
			this.WriteRegistry(this.caption);
		}

		public static Viewpoint ShowViewPoint (Form owner, string urno)
		{
			Viewpoint dlg;
			if (  openViewPoints.Contains(urno) )
			{
				dlg = (Viewpoint)openViewPoints[urno];
				dlg.BringToFront();
				if ( dlg.WindowState == FormWindowState.Minimized ) 
					dlg.WindowState = FormWindowState.Normal;
			}
			else
			{
				dlg = new Viewpoint();
				dlg.Owner = owner;
				dlg.Urno = urno;
				openViewPoints[urno]=dlg;
			}
			return dlg;
		}
	}
}
