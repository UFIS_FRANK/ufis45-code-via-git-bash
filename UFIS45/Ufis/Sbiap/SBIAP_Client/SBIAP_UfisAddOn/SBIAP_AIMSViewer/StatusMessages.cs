using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Ufis.Utils;
using Ufis.Data;

namespace AIMSViewer
{
	/// <summary>
	/// Summary description for SymbolGroups.
	/// </summary>
	public class StatusMessages : System.Windows.Forms.Form
	{
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Button btnDelete;
		private System.Windows.Forms.Button lblAdd;
		private AxTABLib.AxTAB tabList;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label lblGroupNames;
		private System.Windows.Forms.ComboBox cbGroupNames;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.ComponentModel.IContainer components;

		#region implementation
		#endregion
		public StatusMessages()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(StatusMessages));
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.btnClose = new System.Windows.Forms.Button();
			this.btnSave = new System.Windows.Forms.Button();
			this.btnDelete = new System.Windows.Forms.Button();
			this.lblAdd = new System.Windows.Forms.Button();
			this.lblGroupNames = new System.Windows.Forms.Label();
			this.cbGroupNames = new System.Windows.Forms.ComboBox();
			this.tabList = new AxTABLib.AxTAB();
			this.panel1 = new System.Windows.Forms.Panel();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			((System.ComponentModel.ISupportInitialize)(this.tabList)).BeginInit();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// imageList1
			// 
			this.imageList1.ImageSize = new System.Drawing.Size(19, 19);
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.White;
			// 
			// btnClose
			// 
			this.btnClose.BackColor = System.Drawing.Color.Transparent;
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnClose.Dock = System.Windows.Forms.DockStyle.Left;
			this.btnClose.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnClose.ImageIndex = 4;
			this.btnClose.ImageList = this.imageList1;
			this.btnClose.Location = new System.Drawing.Point(160, 0);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(72, 48);
			this.btnClose.TabIndex = 13;
			this.btnClose.Text = "   &Close";
			this.toolTip1.SetToolTip(this.btnClose, "Close this dialogue");
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// btnSave
			// 
			this.btnSave.BackColor = System.Drawing.Color.Transparent;
			this.btnSave.Dock = System.Windows.Forms.DockStyle.Left;
			this.btnSave.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnSave.ImageIndex = 2;
			this.btnSave.ImageList = this.imageList1;
			this.btnSave.Location = new System.Drawing.Point(232, 0);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(72, 48);
			this.btnSave.TabIndex = 12;
			this.btnSave.Text = "   &Save";
			this.toolTip1.SetToolTip(this.btnSave, "Save all status messages of the currently active group");
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// btnDelete
			// 
			this.btnDelete.BackColor = System.Drawing.Color.Transparent;
			this.btnDelete.Dock = System.Windows.Forms.DockStyle.Left;
			this.btnDelete.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnDelete.ImageIndex = 1;
			this.btnDelete.ImageList = this.imageList1;
			this.btnDelete.Location = new System.Drawing.Point(88, 0);
			this.btnDelete.Name = "btnDelete";
			this.btnDelete.Size = new System.Drawing.Size(72, 48);
			this.btnDelete.TabIndex = 11;
			this.btnDelete.Text = "    &Delete";
			this.toolTip1.SetToolTip(this.btnDelete, "Delete the currently selected message");
			this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
			// 
			// lblAdd
			// 
			this.lblAdd.BackColor = System.Drawing.Color.Transparent;
			this.lblAdd.Dock = System.Windows.Forms.DockStyle.Left;
			this.lblAdd.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.lblAdd.ImageIndex = 0;
			this.lblAdd.ImageList = this.imageList1;
			this.lblAdd.Location = new System.Drawing.Point(0, 0);
			this.lblAdd.Name = "lblAdd";
			this.lblAdd.Size = new System.Drawing.Size(88, 48);
			this.lblAdd.TabIndex = 10;
			this.lblAdd.Text = "&New";
			this.toolTip1.SetToolTip(this.lblAdd, "Create a new status message");
			this.lblAdd.Click += new System.EventHandler(this.lblAdd_Click);
			// 
			// lblGroupNames
			// 
			this.lblGroupNames.BackColor = System.Drawing.Color.Transparent;
			this.lblGroupNames.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblGroupNames.Location = new System.Drawing.Point(16, 24);
			this.lblGroupNames.Name = "lblGroupNames";
			this.lblGroupNames.Size = new System.Drawing.Size(76, 16);
			this.lblGroupNames.TabIndex = 14;
			this.lblGroupNames.Text = "Group Name:";
			// 
			// cbGroupNames
			// 
			this.cbGroupNames.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbGroupNames.Location = new System.Drawing.Point(96, 20);
			this.cbGroupNames.Name = "cbGroupNames";
			this.cbGroupNames.Size = new System.Drawing.Size(224, 22);
			this.cbGroupNames.TabIndex = 15;
			this.toolTip1.SetToolTip(this.cbGroupNames, "Select the status group ");
			this.cbGroupNames.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
			// 
			// tabList
			// 
			this.tabList.Location = new System.Drawing.Point(96, 72);
			this.tabList.Name = "tabList";
			this.tabList.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabList.OcxState")));
			this.tabList.Size = new System.Drawing.Size(496, 236);
			this.tabList.TabIndex = 16;
			this.tabList.CloseInplaceEdit += new AxTABLib._DTABEvents_CloseInplaceEditEventHandler(this.tabList_CloseInplaceEdit);
			this.tabList.RowSelectionChanged += new AxTABLib._DTABEvents_RowSelectionChangedEventHandler(this.tabList_RowSelectionChanged);
			this.tabList.SendLButtonClick += new AxTABLib._DTABEvents_SendLButtonClickEventHandler(this.tabList_SendLButtonClick);
			this.tabList.EditPositionChanged += new AxTABLib._DTABEvents_EditPositionChangedEventHandler(this.tabList_EditPositionChanged);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnSave);
			this.panel1.Controls.Add(this.btnClose);
			this.panel1.Controls.Add(this.btnDelete);
			this.panel1.Controls.Add(this.lblAdd);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 326);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(640, 48);
			this.panel1.TabIndex = 17;
			this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
			// 
			// StatusMessages
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 13);
			this.ClientSize = new System.Drawing.Size(640, 374);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.tabList);
			this.Controls.Add(this.cbGroupNames);
			this.Controls.Add(this.lblGroupNames);
			this.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "StatusMessages";
			this.Text = "Status messages ...";
			this.Load += new System.EventHandler(this.SymbolGroups_Load);
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.StatusMessages_Paint);
			((System.ComponentModel.ISupportInitialize)(this.tabList)).EndInit();
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void SymbolGroups_Load(object sender, System.EventArgs e)
		{
			IDatabase myDB = UT.GetMemDB();
			ITable	myTable= myDB["ASG"];	

			// security requirements
			switch(frmData.GetPrivileges("StatusMessagesDlgSave"))
			{
				case "-":
					this.btnSave.Enabled = false;
					this.btnSave.Visible = false;
					break;
				case "0":
					this.btnSave.Enabled = false;
					break;
			}

			string strColor = "";
			strColor = UT.colBlue.ToString() + "," + UT.colBlue.ToString();
			tabList.ResetContent();
			tabList.FontName = "Arial";
			tabList.FontSize = 14;
			tabList.HeaderString = "Interface Code,Status,Color,Handling Advice,Urno,Code";
			tabList.LogicalFieldList = "IFCO,STAT,COLO,ADVI,URNO,CODE";
			tabList.HeaderLengthString = "100,100,100,200,-1,-1";
			tabList.ColumnWidthString  = "32,32,10,255,10,10";
			tabList.CursorDecoration(tabList.LogicalFieldList, "T,B", "2,2", strColor);
			tabList.DefaultCursor = false;
			tabList.LineHeight = 18;


			for (int i = 0; i < myTable.Count;i++)
			{
				IRow myRow = myTable[i];
				string prv = "Subsystem" + myRow["SUBS"];
				if (frmData.GetPrivileges(prv) == "1")
				{
					this.cbGroupNames.Items.Add(myRow["NAME"]);
				}
			}

			if (this.cbGroupNames.Items.Count > 0)
			{
				this.cbGroupNames.SelectedIndex = 0;
			}

			tabList.ComboOwnerDraw = false;
			tabList.ComboResetObject( "STAT" );
			tabList.CreateComboObject("STAT", 1, 1, "", 300);
			tabList.ComboSetColumnLengthString("STAT", "64");
			tabList.SetComboColumn("STAT",1);
			tabList.ComboAddTextLines("STAT","Attention,Alarm,Critical,Regular,On,Off",",");

			tabList.NoFocusColumns = "1,2,4";
			tabList.InplaceEditUpperCase = false;
			tabList.EnableInlineEdit(true);

		}

		private void lblAdd_Click(object sender, System.EventArgs e)
		{
			if (this.cbGroupNames.SelectedIndex >= 0)
			{
				NewStatusMessage();
			}
		}

		private void btnDelete_Click(object sender, System.EventArgs e)
		{
			this.HandleDeleteStatusMessage();
			this.CheckDataChanged();
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
		
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			this.SaveStatusMessages();
		}

		private void comboBox1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (btnSave.BackColor == Color.Red)
			{
				DialogResult olResult;
				olResult = MessageBox.Show(this, "Do you want to save current group?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question); 
				if (olResult == DialogResult.Yes)
				{
					if (SaveStatusMessages() == false) 
						return; // Do not NEW!!
				}
			}

			string name = this.cbGroupNames.Text;
			SetASDTab(this.UrnoOfSymbolGroup(name));
			btnSave.BackColor = Color.Transparent;
		}

		private string NameOfSymbolGroup(string urno)
		{
			IDatabase myDB = UT.GetMemDB();
			ITable	myTable= myDB["ASG"];	
			IRow[]	myRows = myTable.RowsByIndexValue("URNO",urno);
			if (myRows.Length == 1)
			{
				return myRows[0]["NAME"];
			}
			else
			{
				return string.Empty;
			}
		}

		private string UrnoOfSymbolGroup(string name)
		{
			IDatabase myDB = UT.GetMemDB();
			ITable	myTable= myDB["ASG"];	
			IRow[]	myRows = myTable.RowsByIndexValue("NAME",name);
			if (myRows.Length == 1)
			{
				return myRows[0]["URNO"];
			}
			else
			{
				return string.Empty;
			}
		}

		private void tabList_SendLButtonClick(object sender, AxTABLib._DTABEvents_SendLButtonClickEvent e)
		{
			if (e.lineNo >= 0 && e.colNo == 2)
			{
				string color = this.tabList.GetColumnValue(e.lineNo,5);
				ColorDialog dlg = new ColorDialog();
				if (color.Length == 0)
				{
					dlg.Color = Color.Green;
				}
				else
				{
					int rgb = int.Parse(color);
					int b   = (((rgb) >> 16) & 0xff);
					int g	= (((rgb) >> 8) & 0xff);
					int r	= ((rgb) & 0xff);
					dlg.Color = Color.FromArgb(r,g,b);
				}
				if (dlg.ShowDialog(this) == DialogResult.OK)
				{
					string cellObj = e.lineNo.ToString();
					Color  cellCol = Color.FromArgb(dlg.Color.R,dlg.Color.G,dlg.Color.B);
					int rgb = dlg.Color.R | dlg.Color.G << 8 | dlg.Color.B << 16;
					this.tabList.SetColumnValue(e.lineNo,5,rgb.ToString());
					tabList.CreateCellObj(cellObj,rgb, UT.colBlack, 10, false, false, false, 10, "Arial");
					this.tabList.SetCellProperty(e.lineNo,e.colNo,cellObj);

				}

				this.tabList.Refresh();
			}
		}

		private bool CheckDataChanged()
		{
			int tCol = -1;
			int bCol = -1;
			int i = 0;
			bool isChanged = false;

			for( i = 0; i < this.tabList.GetLineCount() && isChanged == false; i++)
			{
				this.tabList.GetLineColor(i, ref tCol, ref bCol);
				if (bCol == UT.colYellow || bCol == UT.colRed || bCol == UT.colLightGreen )
				{
					isChanged = true;
				}
			}

			if (isChanged)
			{
				if (this.btnSave.Enabled)
				{
					btnSave.BackColor = Color.Red;
				}

				return isChanged;
			}
			else
			{
				btnSave.BackColor = Color.Transparent;
				return isChanged;
			}
		}

		/// <summary>
		/// Crates an empty string for tab insertion
		/// </summary>
		/// <param name="strValueList">[separator] delimited string </param>
		/// <param name="separator">separator</param>
		/// <returns></returns>
		private string MakeEmptyString(string strValueList, char separator)
		{
			int i = 0;
			string [] strArr = strValueList.Split(separator);
			string strValues = "";
			for(i = 0; i < strArr.Length-1; i++)
			{
				strValues += separator.ToString();
			}
			return strValues;
		}

		private void NewStatusMessage()
		{
			IDatabase myDB = UT.GetMemDB();
			ITable myTab = myDB["ASD"];
			if (myTab != null)
			{
				string strValues = MakeEmptyString(this.tabList.LogicalFieldList, ',');
				this.tabList.InsertTextLine(strValues, true);
				int cnt = this.tabList.GetLineCount() - 1;
				string strUrno = myDB.GetNextUrno();
				this.tabList.SetFieldValues(cnt, "URNO", strUrno);
				this.tabList.SetLineColor(cnt, UT.colBlack, UT.colYellow);
				this.tabList.OnVScrollTo(cnt);
				CheckDataChanged();
			}
		}

		private void HandleDeleteStatusMessage()
		{
			DialogResult olResult;
			olResult = MessageBox.Show(this, "Do you really want to delete the current status message ?", "Question", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
			if( olResult != DialogResult.Yes)
			{
				return;
			}

			int currSel = this.tabList.GetCurrentSelected();
			int bCol = -1;
			int tCol = -1;
			if (currSel > -1)
			{
				this.tabList.GetLineColor(currSel, ref tCol, ref bCol);
				if(bCol != UT.colRed && bCol != UT.colOrange)
				{
					this.tabList.SetLineColor(currSel, UT.colBlack, UT.colRed);
				}
				if(bCol == UT.colYellow)
				{
					this.tabList.DeleteLine(currSel);
				}
				if( bCol == UT.colOrange)
				{
					this.tabList.SetLineColor(currSel, UT.colBlack, UT.colLightGreen);
					CheckValuesChanged(currSel);
				}

				if(bCol == UT.colRed )
				{
					this.tabList.SetLineColor(currSel, UT.colBlack, UT.colWhite);
					CheckValuesChanged(currSel);
				}
				this.tabList.Refresh();
			}
		}

		private void CheckValuesChanged(int LineNo)
		{
			int tCol = -1;
			int bCol = -1;
			this.tabList.GetLineColor(LineNo, ref tCol, ref bCol);
			string strCurrValues = this.tabList.GetFieldValues(LineNo, this.tabList.LogicalFieldList);
			if(strCurrValues != this.tabList.GetLineTag(LineNo))
			{
				if(bCol == UT.colWhite)// || bCol == UT.colRed)
				{
					this.tabList.SetLineColor(LineNo, UT.colBlack, UT.colLightGreen);
					this.tabList.Refresh();
				}
			}
			else
			{
				if(bCol == UT.colLightGreen)
				{
					this.tabList.SetLineColor(LineNo, UT.colBlack, UT.colWhite);
					this.tabList.Refresh();
				}
			}
			
			CheckDataChanged();
		}

		private bool ValidateData(out string strMessage)
		{
			strMessage = string.Empty;
			bool	blOK = true;
			int		tCol = -1;
			int		bCol = -1;

			SortedList arrIFCode = new SortedList();
			for(int i = 0; i < this.tabList.GetLineCount(); i++)
			{
				this.tabList.GetLineColor(i, ref tCol, ref bCol);
				if(bCol == UT.colYellow || bCol == UT.colLightGreen)
				{
					if (this.tabList.GetFieldValue(i, "IFCO") == "")
					{
						strMessage += "Grid Line " + (i+1).ToString() + ": IF - code is mandatory!\n";
						blOK = false;
					}
					else
					{
						if (arrIFCode.IndexOfKey(this.tabList.GetFieldValue(i, "IFCO")) >= 0)
						{
							strMessage += "Grid Line " + (i+1).ToString() + ": IF - code is not unique!\n";
							blOK = false;
						}
						else
						{
							arrIFCode.Add(this.tabList.GetFieldValue(i, "IFCO"),null);
						}
					}

					if (this.tabList.GetFieldValue(i, "STAT") == "")
					{
						strMessage += "Grid Line " + (i+1).ToString() + ": Status is mandatory!\n";
						blOK = false;
					}

					if (this.tabList.GetFieldValue(i, "CODE") == "")
					{
						strMessage += "Grid Line " + (i+1).ToString() + ": Color is mandatory!\n";
						blOK = false;
					}

				}
				else
				{
					arrIFCode.Add(this.tabList.GetFieldValue(i, "IFCO"),null);
				}

			}

			return blOK;
		}

		private bool SaveStatusMessages()
		{
			bool isOK = true;
			string strMessage="";
			if (btnSave.BackColor == Color.Transparent )
			{
				MessageBox.Show(this, "Nothing to save!!", "Info");
				return true;
			}

			//Call Validation
			if (!ValidateData(out strMessage))
			{
				MessageBox.Show(this, strMessage, "Save Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				isOK = false;
			}
			else //Save the status messages
			{
				int tColor = -1;
				int bColor = -1;

				IDatabase myDB = UT.GetMemDB();
				ITable myTable = myDB["ASD"];

				string sgrpUrno = this.UrnoOfSymbolGroup(this.cbGroupNames.Text);

				for(int i = 0; i < this.tabList.GetLineCount(); i++)
				{
					this.tabList.GetLineColor(i, ref tColor, ref bColor);
					IRow row = null;
					ArrayList arrFlds = myTable.FieldList;
					if (bColor == UT.colYellow) // New
					{
						row = myTable.CreateEmptyRow();
						for(int j = 0; j < arrFlds.Count; j++)
						{
							string strFld = arrFlds[j].ToString();
							if (strFld == "COLO")
							{
								row["COLO"] = this.tabList.GetFieldValue(i,"CODE");
							}
							else if (strFld != "LSTU" && strFld != "CDAT" && strFld != "USEC" && strFld != "USEU")
							{
								row[strFld] = this.tabList.GetFieldValue(i, strFld);
							}
						}

						string strTime = UT.DateTimeToCeda( DateTime.Now);
						row["CDAT"] = strTime;
						row["USEC"] = UT.UserName;
						row["SGRP"] = sgrpUrno;
						this.tabList.SetFieldValues(i, "URNO", row["URNO"]);
						row.Status = State.Created;
						myTable.Add(row);
						if (myTable.Save())
						{
							this.tabList.SetLineColor(i, UT.colBlack , UT.colWhite);
							this.tabList.Refresh();
						}
					}
					else if (bColor == UT.colLightGreen)//Updated
					{
						int idxUrno = UT.GetItemNo(this.tabList.LogicalFieldList, "URNO");
						if(idxUrno > -1)
						{
							IRow [] rows = myTable.RowsByIndexValue("URNO", this.tabList.GetColumnValue(i, idxUrno));
							if(rows.Length > 0)
							{
								for(int j = 0; j < arrFlds.Count; j++)
								{
									string strFld = arrFlds[j].ToString();
									if (strFld == "COLO")
									{
										rows[0]["COLO"] = this.tabList.GetFieldValue(i,"CODE");
									}
									else if(strFld != "URNO" && strFld != "SGRP" && strFld != "LSTU" && strFld != "CDAT" && strFld != "USEC" && strFld != "USEU")
									{
										rows[0][strFld] = this.tabList.GetFieldValue(i, strFld);
									}
								}

								string strTime = UT.DateTimeToCeda( DateTime.Now);
								rows[0]["LSTU"] = strTime;
								rows[0]["USEU"] = UT.UserName;
								rows[0].Status = State.Modified;
							}
						}
						if (myTable.Save())
						{
							this.tabList.SetLineColor(i, UT.colBlack , UT.colWhite);
							this.tabList.Refresh();
						}
					}
					else if (bColor == UT.colRed) // Deleted
					{
						int idxUrno = UT.GetItemNo(this.tabList.LogicalFieldList, "URNO");
						if(idxUrno > -1)
						{
							IRow [] rows = myTable.RowsByIndexValue("URNO", this.tabList.GetColumnValue(i, idxUrno));
							if(rows.Length > 0)
							{
								rows[0].Status = State.Deleted;
								myTable.Save();
							}
						}
					}
				}

				this.tabList.Refresh();
				//Remove the deleted (red) lines
				for(int i = this.tabList.GetLineCount()-1; i >= 0; i--)
				{
					this.tabList.GetLineColor(i, ref tColor, ref bColor);
					if (bColor == UT.colRed)
					{
						this.tabList.DeleteLine(i);
					}
				}

				for(int i = 0; i < this.tabList.GetLineCount(); i++)
				{
					string strValues = this.tabList.GetFieldValues(i, this.tabList.LogicalFieldList);
					this.tabList.SetLineTag(i, strValues);
				}

				myTable.ReorganizeIndexes();
				this.tabList.SetCurrentSelection(-1);
				this.tabList.Refresh();
				btnSave.BackColor = Color.Transparent;
			}
			return isOK;

		}

		void SetASDTab(string strSGRP)
		{
			this.tabList.ResetContent();
			IDatabase myDB = UT.GetMemDB();
			ITable myTab = myDB["ASD"];
			if(myTab != null)
			{
				IRow [] rows = myTab.RowsByIndexValue("SGRP", strSGRP);
				//First insert the database field values
				int i = 0;
				if (rows != null)
				{
					int LineNo = -1;
					for(i = 0; i < rows.Length; i++)
					{
						string strValues = MakeEmptyString(this.tabList.LogicalFieldList, ',');
						this.tabList.InsertTextLine(strValues, false);
						LineNo = this.tabList.GetLineCount() - 1;
						string strFld = "";
						for(int j = 0; j < myTab.FieldList.Count; j++)
						{
							strFld = myTab.FieldList[j].ToString();
							if (strFld == "COLO")
							{
								this.SetColorForCell(LineNo,2,int.Parse(rows[i][strFld]));
							}
							else
							{
								this.tabList.SetFieldValues(LineNo, strFld, rows[i][strFld]);
							}
						}	
					}

					for(i = 0; i < this.tabList.GetLineCount(); i++)
					{
						string strValues = this.tabList.GetFieldValues(i, this.tabList.LogicalFieldList);
						this.tabList.SetLineTag(i, strValues);
					}
				}
			}
			this.tabList.Refresh();
		}


		void SetColorForCell(int lineNo,int colNo,int rgb)
		{
			int b = (((rgb) >> 16) & 0xff);
			int g = (((rgb) >> 8) & 0xff);
			int r = ((rgb) & 0xff);
			Color color = Color.FromArgb(r,g,b);
			string cellObj = lineNo.ToString();
			this.tabList.SetColumnValue(lineNo,5,rgb.ToString());
			tabList.CreateCellObj(cellObj,rgb, UT.colBlack, 10, false, false, false, 10, "Arial");
			this.tabList.SetCellProperty(lineNo,colNo,cellObj);
		}

		private void tabList_CloseInplaceEdit(object sender, AxTABLib._DTABEvents_CloseInplaceEditEvent e)
		{
			this.CheckValuesChanged(e.lineNo);		
		}

		private void tabList_EditPositionChanged(object sender, AxTABLib._DTABEvents_EditPositionChangedEvent e)
		{
			this.CheckValuesChanged(e.lineNo);		
		}

		private void tabList_RowSelectionChanged(object sender, AxTABLib._DTABEvents_RowSelectionChangedEvent e)
		{
			CheckValuesChanged(e.lineNo);
		}

		private void panel1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, panel1, Color.WhiteSmoke, Color.LightGray);		
		}

		private void StatusMessages_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, this, Color.WhiteSmoke, Color.LightGray);		
		}
	}
}
