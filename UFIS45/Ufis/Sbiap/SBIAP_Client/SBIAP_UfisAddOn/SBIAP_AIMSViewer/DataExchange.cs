using System;
using System.ComponentModel;
using System.Collections;
using System.Diagnostics;
using Ufis.Utils;
using Ufis.Data;


namespace AIMSViewer
{
	/// <summary>
	/// Summary description for DataExchange.
	/// </summary>
	public class DataExchange : System.ComponentModel.Component
	{
		#region implementation

		static IDatabase myDB = null;
		static BCBlinker myBCBlinker = null;
		static ITable	 avpTAB = null;	
		static ITable	 acsTAB	= null;
		static ArrayList acsList = new ArrayList();		
		static bool		 modified = false;	
		#endregion

		#region	delegate definitions
		/// <summary>
		/// Sends a delegate that the current view will be changed.
		/// </summary>
		public delegate void BeforeViewNameChanged();
		public static event BeforeViewNameChanged OnBeforeViewNameChanged;
		/// <summary>
		/// Sends a delegate that the current view has been changed.
		/// </summary>
		public delegate void ViewNameChanged();
		public static event ViewNameChanged OnViewNameChanged;
		/// <summary>
		/// Sends a delegate to That a UTC to LOCAL or LOCAL to UTC was performed
		/// </summary>
		public delegate void UTC_LocalTimeChanged();
		public static event UTC_LocalTimeChanged OnUTC_LocalTimeChanged;
		/// <summary>
		/// Sends a delegate that "CLO" message was sent by the server
		/// </summary>
		public delegate void CLO_Message();
		public static event CLO_Message OnCLO_Message;
		/// <summary>
		/// An viewpoint definition has been changed. The sender is necessary to ensure that the originator has
		/// the chance to identify, whether he was the originator to suppress e.g. an unnecessary update.
		/// </summary>
		public delegate void AVP_Changed( object sender, string strAvpUrno, State state);
		public static event AVP_Changed OnAVP_Changed;

		/// <summary>
		/// An camera definition has been changed. The sender is necessary to ensure that the originator has
		/// the chance to identify, whether he was the originator to suppress e.g. an unnecessary update.
		/// </summary>
		public delegate void ATV_Changed( object sender, string strAtvUrno, State state);
		public static event ATV_Changed OnATV_Changed;

		/// <summary>
		/// Some Status has been changed. The sender is necessary to ensure that the originator has
		/// the chance to identify, whether he was the originator to suppress e.g. an unnecessary update.
		/// </summary>
		public delegate void ACS_Changed( object sender, ArrayList updateList);
		public static event ACS_Changed OnACS_Changed;

		/// <summary>
		/// The application has been closed.
		/// </summary>
		public delegate void Application_Shutdown();
		public static event Application_Shutdown OnApplication_Shutdown;

		/// <summary>
		/// The selection of an symbol has been changed. The sender is necessary to ensure that the originator has
		/// the chance to identify, whether he was the originator to suppress e.g. an unnecessary update.
		/// </summary>
		public delegate void SymbolSelection_Changed( object sender,PictureViewer.Symbol symbol,bool selected);
		public static event SymbolSelection_Changed OnSymbolSelection_Changed;

		#endregion

		#region delegate worker methods

		static public void Call_BeforeViewNameChanged()
		{
			if(OnBeforeViewNameChanged != null)
				OnBeforeViewNameChanged();
		}
		static public void Call_ViewNameChanged()
		{
			if(OnViewNameChanged != null)
				OnViewNameChanged();
		}
		static public void Call_UTC_LocalTimeChanged(object sender)
		{
			if(OnUTC_LocalTimeChanged != null)
			{
				OnUTC_LocalTimeChanged();
			}
		}

		static public void Call_AVP_Changed(object sender, string strAvpUrno, State state)
		{
			if (OnAVP_Changed != null)
			{
				Delegate[] delegates = OnAVP_Changed.GetInvocationList();
				object[] args = new object[3];
				args[0] = sender;
				args[1] = strAvpUrno;
				args[2] = state;

				UT.Invoke(delegates,args);
			}
		}

		static public void Call_ACS_Changed(object sender, ArrayList updateList)
		{
			if (OnACS_Changed != null)
			{
				Delegate[] delegates = OnACS_Changed.GetInvocationList();
				object[] args = new object[2];
				args[0] = sender;
				args[1] = updateList;

				UT.Invoke(delegates,args);
			}
		}

		static public void Call_ATV_Changed(object sender, string strAtvUrno, State state)
		{
			if (OnATV_Changed != null)
			{
				Delegate[] delegates = OnATV_Changed.GetInvocationList();
				object[] args = new object[3];
				args[0] = sender;
				args[1] = strAtvUrno;
				args[2] = state;

				UT.Invoke(delegates,args);
			}
		}

		static public void Call_Application_Shutdown()
		{
			if (OnApplication_Shutdown != null)
				OnApplication_Shutdown();
		}

		static public void Call_SymbolSelection_Changed(object sender, PictureViewer.Symbol symbol,bool selected)
		{
			if(OnSymbolSelection_Changed != null)
				OnSymbolSelection_Changed(sender,symbol,selected);
		}

		#endregion --- Worker Methods

		private System.Windows.Forms.Timer timRefresh;
		private System.ComponentModel.IContainer components;

		public DataExchange(System.ComponentModel.IContainer container)
		{
			///
			/// Required for Windows.Forms Class Composition Designer support
			///
			container.Add(this);
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\ceda.ini");
			string strRefresh = myIni.IniReadValue("AIMSVIEWER", "REFRESHINTERVAL");
			int refresh = 30;
			try
			{
				refresh = int.Parse(strRefresh);
			}
			catch(Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(ex.Message);
				refresh = 30;
			}
							
			this.timRefresh.Interval = 1000 * refresh;
			this.timRefresh.Start();

		}

		public DataExchange()
		{
			///
			/// Required for Windows.Forms Class Composition Designer support
			///
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\ceda.ini");
			string strRefresh = myIni.IniReadValue("AIMSVIEWER", "REFRESHINTERVAL");
			int refresh = 30;
			try
			{
				refresh = int.Parse(strRefresh);
			}
			catch(Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(ex.Message);
				refresh = 30;
			}
							
			this.timRefresh.Interval = 1000 * refresh;
			this.timRefresh.Start();
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.timRefresh = new System.Windows.Forms.Timer(this.components);
			// 
			// timer1
			// 
			this.timRefresh.Tick += new System.EventHandler(this.timer1_Tick);

		}
		#endregion

		static public void InitDBObjects(Ufis.Utils.BCBlinker blinker)
		{
			myBCBlinker = blinker;
			myDB = UT.GetMemDB();
			avpTAB = myDB["AVP"];
			acsTAB = myDB["ACS"];
			myDB.OnEventHandler += new DatabaseEventHandler(DatabaseEventHandler);
			myDB.OnTableEventHandler += new DatabaseTableEventHandler(TableEventHandler); 
		}

		static public ArrayList	StatusEvents
		{
			get
			{
				return acsList;
			}
		}

		static public void ClearStatusEvents()
		{
			acsList.Clear();
			modified = false;
		}

		static void DatabaseEventHandler(object obj,DatabaseEventArgs eventArgs)
		{
			myBCBlinker.Blink();						
			if (eventArgs.command == "SBC")
			{
			}
			else if(eventArgs.command == "CLO")
			{
				//CLO_Message 
				if (OnCLO_Message != null)
				{
					OnCLO_Message();
				}
			}
		}

		static void TableEventHandler(object obj,DatabaseTableEventArgs eventArgs)
		{
			myBCBlinker.Blink();
			ITable myTable = myDB[eventArgs.table];
			if(myTable == avpTAB)
			{
				HandleAVPevent(obj,eventArgs);
			}
			else if (myTable == acsTAB)
			{
				HandleACSevent(obj,eventArgs);
			}
		}

		static void HandleAVPevent(object obj, DatabaseTableEventArgs eventArgs)
		{

			if (avpTAB.Command("insert").IndexOf(","+eventArgs.command+",") >= 0)
			{
				Call_AVP_Changed(avpTAB, eventArgs.row["URNO"], State.Created);
			}
			else if (avpTAB.Command("update").IndexOf(","+eventArgs.command+",") >= 0)
			{
				Call_AVP_Changed(obj,eventArgs.row["URNO"], State.Modified);
			}
			else if (avpTAB.Command("delete").IndexOf(","+eventArgs.command+",") >= 0)
			{
				Call_AVP_Changed(avpTAB, eventArgs.row["URNO"], State.Deleted);
			}
		}

		static void HandleACSevent(object obj, DatabaseTableEventArgs eventArgs)
		{
			if (acsTAB.Command("insert").IndexOf(","+eventArgs.command+",") >= 0)
			{
				frmData.GetThis().UpdateSymbolList(eventArgs.row,State.Created);
			}
			else if (acsTAB.Command("update").IndexOf(","+eventArgs.command+",") >= 0)
			{
				frmData.GetThis().UpdateSymbolList(eventArgs.row,State.Modified);
			}
			else if (acsTAB.Command("delete").IndexOf(","+eventArgs.command+",") >= 0)
			{
				frmData.GetThis().UpdateSymbolList(eventArgs.row,State.Deleted);
			}

			acsList.Add(eventArgs);
			modified = true;
		}

		private void timer1_Tick(object sender, System.EventArgs e)
		{
			if (modified)
			{
				this.timRefresh.Stop();
				Call_ACS_Changed(acsTAB,(ArrayList)acsList.Clone());
				acsList.Clear();
				modified = false;
				this.timRefresh.Start();
			}
		}
	}
}
