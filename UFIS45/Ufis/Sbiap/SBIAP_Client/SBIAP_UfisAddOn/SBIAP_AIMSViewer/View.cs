using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Ufis.Data;
using Ufis.Utils;

namespace AIMSViewer
{
	/// <summary>
	/// Summary description for View.
	/// </summary>
	public class View : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label lblViewText;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label lblCurrentActiveView;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnNew;
		private System.Windows.Forms.Button btnDelete;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Label lblViewName;
		private System.Windows.Forms.ComboBox cbViewName;
		private System.Windows.Forms.Panel panelStatusSections;
		private System.Windows.Forms.CheckBox cbStatusGroups;
		private AxTABLib.AxTAB tabGroupsTarget;
		private AxTABLib.AxTAB tabGroupsSource;
		private System.Windows.Forms.PictureBox picStatusSectionFilter;
		private System.ComponentModel.IContainer components;

		private IDatabase myDB	= null;
		private ITable	  myASG = null;
		private ITable	  myAVP = null;	
		private AxTABLib.AxTAB	myViewsTab;
		private System.Windows.Forms.Button btnGroupsSource;
		private System.Windows.Forms.Button bntGroupsTarget;
		private System.Windows.Forms.TextBox txtGroups;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.ImageList imageButtons;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.CheckBox cbViewpoints;
		private System.Windows.Forms.TextBox txtViewpoints;
		private AxTABLib.AxTAB tabViewpointsTarget;
		private System.Windows.Forms.Button btnViewpointsSource;
		private AxTABLib.AxTAB tabViewpointsSource;
		private System.Windows.Forms.Button btnViewpointsTarget;
		private StringBuilder bufferASG = new StringBuilder(10000);
		private StringBuilder bufferAVP = new StringBuilder(10000);

		public View(AxTABLib.AxTAB tabViews)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			this.myViewsTab = tabViews;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(View));
			this.lblViewText = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.lblCurrentActiveView = new System.Windows.Forms.Label();
			this.btnSave = new System.Windows.Forms.Button();
			this.imageButtons = new System.Windows.Forms.ImageList(this.components);
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnNew = new System.Windows.Forms.Button();
			this.btnDelete = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.lblViewName = new System.Windows.Forms.Label();
			this.cbViewName = new System.Windows.Forms.ComboBox();
			this.panelStatusSections = new System.Windows.Forms.Panel();
			this.cbStatusGroups = new System.Windows.Forms.CheckBox();
			this.txtGroups = new System.Windows.Forms.TextBox();
			this.tabGroupsTarget = new AxTABLib.AxTAB();
			this.btnGroupsSource = new System.Windows.Forms.Button();
			this.tabGroupsSource = new AxTABLib.AxTAB();
			this.bntGroupsTarget = new System.Windows.Forms.Button();
			this.picStatusSectionFilter = new System.Windows.Forms.PictureBox();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.panel2 = new System.Windows.Forms.Panel();
			this.cbViewpoints = new System.Windows.Forms.CheckBox();
			this.txtViewpoints = new System.Windows.Forms.TextBox();
			this.tabViewpointsTarget = new AxTABLib.AxTAB();
			this.btnViewpointsSource = new System.Windows.Forms.Button();
			this.tabViewpointsSource = new AxTABLib.AxTAB();
			this.btnViewpointsTarget = new System.Windows.Forms.Button();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.panel1.SuspendLayout();
			this.panelStatusSections.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabGroupsTarget)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabGroupsSource)).BeginInit();
			this.panel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabViewpointsTarget)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabViewpointsSource)).BeginInit();
			this.SuspendLayout();
			// 
			// lblViewText
			// 
			this.lblViewText.BackColor = System.Drawing.Color.Black;
			this.lblViewText.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblViewText.ForeColor = System.Drawing.Color.Lime;
			this.lblViewText.Location = new System.Drawing.Point(0, 0);
			this.lblViewText.Name = "lblViewText";
			this.lblViewText.Size = new System.Drawing.Size(784, 32);
			this.lblViewText.TabIndex = 0;
			this.lblViewText.Text = "The view definition has an impact on the visualisation of statuses. You can defin" +
				"e a filter condition to see and handle the statuses in your responsibility.";
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.Transparent;
			this.panel1.Controls.Add(this.lblCurrentActiveView);
			this.panel1.Controls.Add(this.btnSave);
			this.panel1.Controls.Add(this.btnCancel);
			this.panel1.Controls.Add(this.btnNew);
			this.panel1.Controls.Add(this.btnDelete);
			this.panel1.Controls.Add(this.btnOK);
			this.panel1.Location = new System.Drawing.Point(0, 32);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(784, 48);
			this.panel1.TabIndex = 1;
			this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
			// 
			// lblCurrentActiveView
			// 
			this.lblCurrentActiveView.BackColor = System.Drawing.Color.Transparent;
			this.lblCurrentActiveView.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblCurrentActiveView.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lblCurrentActiveView.Location = new System.Drawing.Point(328, 8);
			this.lblCurrentActiveView.Name = "lblCurrentActiveView";
			this.lblCurrentActiveView.Size = new System.Drawing.Size(276, 16);
			this.lblCurrentActiveView.TabIndex = 42;
			this.lblCurrentActiveView.Text = "The View \"<DEFAULT>\" is currently active";
			// 
			// btnSave
			// 
			this.btnSave.BackColor = System.Drawing.Color.Transparent;
			this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnSave.ImageIndex = 5;
			this.btnSave.ImageList = this.imageButtons;
			this.btnSave.Location = new System.Drawing.Point(228, 0);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(75, 48);
			this.btnSave.TabIndex = 41;
			this.btnSave.Text = "  &Save";
			this.toolTip1.SetToolTip(this.btnSave, "Save the current selected view");
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// imageButtons
			// 
			this.imageButtons.ImageSize = new System.Drawing.Size(19, 19);
			this.imageButtons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageButtons.ImageStream")));
			this.imageButtons.TransparentColor = System.Drawing.Color.White;
			// 
			// btnCancel
			// 
			this.btnCancel.BackColor = System.Drawing.Color.Transparent;
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Dock = System.Windows.Forms.DockStyle.Right;
			this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnCancel.ImageIndex = 0;
			this.btnCancel.ImageList = this.imageButtons;
			this.btnCancel.Location = new System.Drawing.Point(709, 0);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 48);
			this.btnCancel.TabIndex = 38;
			this.btnCancel.Text = "  &Close";
			this.toolTip1.SetToolTip(this.btnCancel, "Close this window");
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnNew
			// 
			this.btnNew.BackColor = System.Drawing.Color.Transparent;
			this.btnNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnNew.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnNew.ImageIndex = 3;
			this.btnNew.ImageList = this.imageButtons;
			this.btnNew.Location = new System.Drawing.Point(76, 0);
			this.btnNew.Name = "btnNew";
			this.btnNew.Size = new System.Drawing.Size(75, 48);
			this.btnNew.TabIndex = 39;
			this.btnNew.Text = "  &New";
			this.toolTip1.SetToolTip(this.btnNew, "Create a new View");
			this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
			// 
			// btnDelete
			// 
			this.btnDelete.BackColor = System.Drawing.Color.Transparent;
			this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnDelete.ImageIndex = 4;
			this.btnDelete.ImageList = this.imageButtons;
			this.btnDelete.Location = new System.Drawing.Point(152, 0);
			this.btnDelete.Name = "btnDelete";
			this.btnDelete.Size = new System.Drawing.Size(75, 48);
			this.btnDelete.TabIndex = 40;
			this.btnDelete.Text = "  &Delete";
			this.toolTip1.SetToolTip(this.btnDelete, "Delete the current selected view");
			this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
			// 
			// btnOK
			// 
			this.btnOK.BackColor = System.Drawing.Color.Transparent;
			this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnOK.ImageIndex = 1;
			this.btnOK.ImageList = this.imageButtons;
			this.btnOK.Location = new System.Drawing.Point(0, 0);
			this.btnOK.Name = "btnOK";
			this.btnOK.Size = new System.Drawing.Size(75, 48);
			this.btnOK.TabIndex = 37;
			this.btnOK.Text = "  &Apply";
			this.toolTip1.SetToolTip(this.btnOK, "Use the current selected View");
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// lblViewName
			// 
			this.lblViewName.BackColor = System.Drawing.Color.Transparent;
			this.lblViewName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblViewName.Location = new System.Drawing.Point(8, 104);
			this.lblViewName.Name = "lblViewName";
			this.lblViewName.Size = new System.Drawing.Size(72, 16);
			this.lblViewName.TabIndex = 29;
			this.lblViewName.Text = "View Name:";
			// 
			// cbViewName
			// 
			this.cbViewName.Location = new System.Drawing.Point(80, 96);
			this.cbViewName.Name = "cbViewName";
			this.cbViewName.Size = new System.Drawing.Size(200, 21);
			this.cbViewName.TabIndex = 28;
			this.cbViewName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbViewName_KeyPress);
			this.cbViewName.TextChanged += new System.EventHandler(this.cbViewName_TextChanged);
			this.cbViewName.SelectedValueChanged += new System.EventHandler(this.cbViewName_SelectedValueChanged);
			// 
			// panelStatusSections
			// 
			this.panelStatusSections.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelStatusSections.Controls.Add(this.cbStatusGroups);
			this.panelStatusSections.Controls.Add(this.txtGroups);
			this.panelStatusSections.Controls.Add(this.tabGroupsTarget);
			this.panelStatusSections.Controls.Add(this.btnGroupsSource);
			this.panelStatusSections.Controls.Add(this.tabGroupsSource);
			this.panelStatusSections.Controls.Add(this.bntGroupsTarget);
			this.panelStatusSections.Controls.Add(this.picStatusSectionFilter);
			this.panelStatusSections.Location = new System.Drawing.Point(16, 128);
			this.panelStatusSections.Name = "panelStatusSections";
			this.panelStatusSections.Size = new System.Drawing.Size(372, 364);
			this.panelStatusSections.TabIndex = 32;
			// 
			// cbStatusGroups
			// 
			this.cbStatusGroups.Appearance = System.Windows.Forms.Appearance.Button;
			this.cbStatusGroups.Enabled = false;
			this.cbStatusGroups.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cbStatusGroups.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cbStatusGroups.Location = new System.Drawing.Point(6, 4);
			this.cbStatusGroups.Name = "cbStatusGroups";
			this.cbStatusGroups.Size = new System.Drawing.Size(176, 20);
			this.cbStatusGroups.TabIndex = 6;
			this.cbStatusGroups.Text = "Status &Groups";
			this.cbStatusGroups.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// txtGroups
			// 
			this.txtGroups.Location = new System.Drawing.Point(188, 4);
			this.txtGroups.Name = "txtGroups";
			this.txtGroups.Size = new System.Drawing.Size(176, 20);
			this.txtGroups.TabIndex = 5;
			this.txtGroups.Text = "";
			this.txtGroups.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtGroups_KeyPress);
			this.txtGroups.TextChanged += new System.EventHandler(this.txtGroups_TextChanged);
			// 
			// tabGroupsTarget
			// 
			this.tabGroupsTarget.ContainingControl = this;
			this.tabGroupsTarget.Location = new System.Drawing.Point(188, 28);
			this.tabGroupsTarget.Name = "tabGroupsTarget";
			this.tabGroupsTarget.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabGroupsTarget.OcxState")));
			this.tabGroupsTarget.Size = new System.Drawing.Size(176, 304);
			this.tabGroupsTarget.TabIndex = 2;
			this.toolTip1.SetToolTip(this.tabGroupsTarget, "Doubleclick to remove an entry");
			this.tabGroupsTarget.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabGroupsTarget_SendLButtonDblClick);
			// 
			// btnGroupsSource
			// 
			this.btnGroupsSource.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnGroupsSource.Location = new System.Drawing.Point(6, 335);
			this.btnGroupsSource.Name = "btnGroupsSource";
			this.btnGroupsSource.Size = new System.Drawing.Size(176, 23);
			this.btnGroupsSource.TabIndex = 3;
			this.btnGroupsSource.Text = "0";
			this.toolTip1.SetToolTip(this.btnGroupsSource, "Accept all");
			this.btnGroupsSource.Click += new System.EventHandler(this.btnGroupsSource_Click);
			// 
			// tabGroupsSource
			// 
			this.tabGroupsSource.ContainingControl = this;
			this.tabGroupsSource.Location = new System.Drawing.Point(6, 28);
			this.tabGroupsSource.Name = "tabGroupsSource";
			this.tabGroupsSource.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabGroupsSource.OcxState")));
			this.tabGroupsSource.Size = new System.Drawing.Size(176, 304);
			this.tabGroupsSource.TabIndex = 1;
			this.toolTip1.SetToolTip(this.tabGroupsSource, "Doubleclick to accept an entry");
			this.tabGroupsSource.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabGroupsSource_SendLButtonDblClick);
			// 
			// bntGroupsTarget
			// 
			this.bntGroupsTarget.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.bntGroupsTarget.Location = new System.Drawing.Point(188, 335);
			this.bntGroupsTarget.Name = "bntGroupsTarget";
			this.bntGroupsTarget.Size = new System.Drawing.Size(176, 23);
			this.bntGroupsTarget.TabIndex = 4;
			this.bntGroupsTarget.Text = "0";
			this.toolTip1.SetToolTip(this.bntGroupsTarget, "Unselect all");
			this.bntGroupsTarget.Click += new System.EventHandler(this.bntGroupsTarget_Click);
			// 
			// picStatusSectionFilter
			// 
			this.picStatusSectionFilter.BackColor = System.Drawing.Color.Transparent;
			this.picStatusSectionFilter.Dock = System.Windows.Forms.DockStyle.Fill;
			this.picStatusSectionFilter.Location = new System.Drawing.Point(0, 0);
			this.picStatusSectionFilter.Name = "picStatusSectionFilter";
			this.picStatusSectionFilter.Size = new System.Drawing.Size(368, 360);
			this.picStatusSectionFilter.TabIndex = 0;
			this.picStatusSectionFilter.TabStop = false;
			// 
			// panel2
			// 
			this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel2.Controls.Add(this.cbViewpoints);
			this.panel2.Controls.Add(this.txtViewpoints);
			this.panel2.Controls.Add(this.tabViewpointsTarget);
			this.panel2.Controls.Add(this.btnViewpointsSource);
			this.panel2.Controls.Add(this.tabViewpointsSource);
			this.panel2.Controls.Add(this.btnViewpointsTarget);
			this.panel2.Controls.Add(this.pictureBox1);
			this.panel2.Location = new System.Drawing.Point(392, 128);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(372, 364);
			this.panel2.TabIndex = 33;
			// 
			// cbViewpoints
			// 
			this.cbViewpoints.Appearance = System.Windows.Forms.Appearance.Button;
			this.cbViewpoints.Enabled = false;
			this.cbViewpoints.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cbViewpoints.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cbViewpoints.Location = new System.Drawing.Point(6, 4);
			this.cbViewpoints.Name = "cbViewpoints";
			this.cbViewpoints.Size = new System.Drawing.Size(176, 20);
			this.cbViewpoints.TabIndex = 6;
			this.cbViewpoints.Text = "&Viewpoints";
			this.cbViewpoints.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// txtViewpoints
			// 
			this.txtViewpoints.Location = new System.Drawing.Point(188, 4);
			this.txtViewpoints.Name = "txtViewpoints";
			this.txtViewpoints.Size = new System.Drawing.Size(176, 20);
			this.txtViewpoints.TabIndex = 5;
			this.txtViewpoints.Text = "";
			this.txtViewpoints.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtViewpoints_KeyPress);
			this.txtViewpoints.TextChanged += new System.EventHandler(this.txtViewpoints_TextChanged);
			// 
			// tabViewpointsTarget
			// 
			this.tabViewpointsTarget.ContainingControl = this;
			this.tabViewpointsTarget.Location = new System.Drawing.Point(188, 28);
			this.tabViewpointsTarget.Name = "tabViewpointsTarget";
			this.tabViewpointsTarget.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabViewpointsTarget.OcxState")));
			this.tabViewpointsTarget.Size = new System.Drawing.Size(176, 304);
			this.tabViewpointsTarget.TabIndex = 2;
			this.toolTip1.SetToolTip(this.tabViewpointsTarget, "Doubleclick to remove an entry");
			this.tabViewpointsTarget.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabViewpointsTarget_SendLButtonDblClick);
			// 
			// btnViewpointsSource
			// 
			this.btnViewpointsSource.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnViewpointsSource.Location = new System.Drawing.Point(6, 335);
			this.btnViewpointsSource.Name = "btnViewpointsSource";
			this.btnViewpointsSource.Size = new System.Drawing.Size(176, 23);
			this.btnViewpointsSource.TabIndex = 3;
			this.btnViewpointsSource.Text = "0";
			this.toolTip1.SetToolTip(this.btnViewpointsSource, "Accept all");
			this.btnViewpointsSource.Click += new System.EventHandler(this.btnViewpointsSource_Click);
			// 
			// tabViewpointsSource
			// 
			this.tabViewpointsSource.ContainingControl = this;
			this.tabViewpointsSource.Location = new System.Drawing.Point(6, 28);
			this.tabViewpointsSource.Name = "tabViewpointsSource";
			this.tabViewpointsSource.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabViewpointsSource.OcxState")));
			this.tabViewpointsSource.Size = new System.Drawing.Size(176, 304);
			this.tabViewpointsSource.TabIndex = 1;
			this.toolTip1.SetToolTip(this.tabViewpointsSource, "Doubleclick to accept an entry");
			this.tabViewpointsSource.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabViewpointsSource_SendLButtonDblClick);
			// 
			// btnViewpointsTarget
			// 
			this.btnViewpointsTarget.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnViewpointsTarget.Location = new System.Drawing.Point(188, 335);
			this.btnViewpointsTarget.Name = "btnViewpointsTarget";
			this.btnViewpointsTarget.Size = new System.Drawing.Size(176, 23);
			this.btnViewpointsTarget.TabIndex = 4;
			this.btnViewpointsTarget.Text = "0";
			this.toolTip1.SetToolTip(this.btnViewpointsTarget, "Unselect all");
			this.btnViewpointsTarget.Click += new System.EventHandler(this.btnViewpointsTarget_Click);
			// 
			// pictureBox1
			// 
			this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
			this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pictureBox1.Location = new System.Drawing.Point(0, 0);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(368, 360);
			this.pictureBox1.TabIndex = 0;
			this.pictureBox1.TabStop = false;
			// 
			// View
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(778, 502);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panelStatusSections);
			this.Controls.Add(this.lblViewName);
			this.Controls.Add(this.cbViewName);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.lblViewText);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "View";
			this.Text = "View";
			this.Load += new System.EventHandler(this.View_Load);
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.View_Paint);
			this.panel1.ResumeLayout(false);
			this.panelStatusSections.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabGroupsTarget)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabGroupsSource)).EndInit();
			this.panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabViewpointsTarget)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabViewpointsSource)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void View_Load(object sender, System.EventArgs e)
		{
			// security requirements
			switch(frmData.GetPrivileges("ViewDlgSave"))
			{
				case "-":
					this.btnSave.Enabled = false;
					this.btnSave.Visible = false;
					break;
				case "0":
					this.btnSave.Enabled = false;
					break;
			}

			myDB = UT.GetMemDB();
			myASG = myDB["ASG"];
			myAVP = myDB["AVP"];

			this.lblCurrentActiveView.Text = "Active View in Main Window: " + ViewFilter.CurrentViewName;		

			NewView();
			int i = 0;
			cbViewName.Items.Add("<DEFAULT>");
			for( i = 0; i < myViewsTab.GetLineCount(); i++)
			{
				string strName = myViewsTab.GetFieldValue(i, "NAME");
				cbViewName.Items.Add(strName);
				if(strName == ViewFilter.CurrentViewName)
				{
					cbViewName_SelectedValueChanged(this, null);
					cbViewName.Text = ViewFilter.CurrentViewName;
				}
			}

			if (ViewFilter.CurrentViewName == "<DEFAULT>")
			{
				cbViewName.Text = ViewFilter.CurrentViewName;
				EnableControlsForDefault(false);
				btnDelete.Enabled = false;
				btnSave.Enabled = false;
			}

			cbViewName.Refresh();

			UpdateButtons();

		}

		private void InitTabs()
		{
			tabGroupsSource.ResetContent();
			tabGroupsSource.HeaderString = "Name,URNO";
			tabGroupsSource.HeaderLengthString = "180,80";
			tabGroupsSource.LogicalFieldList = "NAME,URNO";
			tabGroupsSource.LifeStyle = true;
			tabGroupsSource.CursorLifeStyle = true;
			tabGroupsSource.SetTabFontBold(true);
			tabGroupsSource.FontName = "Arial";
			tabGroupsSource.DisplayBackColor = UT.colYellow;

			tabGroupsTarget.ResetContent();
			tabGroupsTarget.HeaderString = "Name,URNO";
			tabGroupsTarget.HeaderLengthString = "180,80";
			tabGroupsTarget.LogicalFieldList = "NAME,URNO";
			tabGroupsTarget.LifeStyle = true;
			tabGroupsTarget.CursorLifeStyle = true;
			tabGroupsTarget.SetTabFontBold(true);
			tabGroupsTarget.FontName = "Arial";
			tabGroupsTarget.DisplayBackColor = UT.colLightGreen;
			tabGroupsTarget.DisplayTextColor = UT.colBlack;

			tabViewpointsSource.ResetContent();
			tabViewpointsSource.HeaderString = "Name,URNO";
			tabViewpointsSource.HeaderLengthString = "180,80";
			tabViewpointsSource.LogicalFieldList = "NAME,URNO";
			tabViewpointsSource.LifeStyle = true;
			tabViewpointsSource.CursorLifeStyle = true;
			tabViewpointsSource.SetTabFontBold(true);
			tabViewpointsSource.FontName = "Arial";
			tabViewpointsSource.DisplayBackColor = UT.colYellow;

			tabViewpointsTarget.ResetContent();
			tabViewpointsTarget.HeaderString = "Name,URNO";
			tabViewpointsTarget.HeaderLengthString = "180,80";
			tabViewpointsTarget.LogicalFieldList = "NAME,URNO";
			tabViewpointsTarget.LifeStyle = true;
			tabViewpointsTarget.CursorLifeStyle = true;
			tabViewpointsTarget.SetTabFontBold(true);
			tabViewpointsTarget.FontName = "Arial";
			tabViewpointsTarget.DisplayBackColor = UT.colLightGreen;
			tabViewpointsTarget.DisplayTextColor = UT.colBlack;

		}

		private void NewView()
		{
			int i = 0;
			string strValues = "";
			string strPrv = "";
			InitTabs();
		
			if (bufferASG.Length == 0)
			{
				for( i = 0; i < myASG.Count; i++)	
				{
					strPrv = "Subsystem" + myASG[i]["SUBS"];
					if (frmData.GetPrivileges(strPrv) == "1")
					{
						strValues = myASG[i]["NAME"] + "," + myASG[i]["URNO"];
						bufferASG.Append(strValues + "\n");
					}
				}
			}

			tabGroupsSource.InsertBuffer(bufferASG.ToString(), "\n");
			btnGroupsSource.Text = tabGroupsSource.GetLineCount().ToString();

			tabGroupsSource.Sort("0", true, true);
			tabGroupsTarget.Sort("0", true, true);

			if (bufferAVP.Length == 0)
			{
				for( i = 0; i < myAVP.Count; i++)	
				{
					strValues = myAVP[i]["NAME"] + "," + myAVP[i]["URNO"];
					bufferAVP.Append(strValues + "\n");
				}
			}

			tabViewpointsSource.InsertBuffer(bufferAVP.ToString(), "\n");
			btnViewpointsSource.Text = tabViewpointsSource.GetLineCount().ToString();

			tabViewpointsSource.Sort("0", true, true);
			tabViewpointsTarget.Sort("0", true, true);

			cbViewName.Focus();
		}

		private void UpdateButtons()
		{
			btnGroupsSource.Text = tabGroupsSource.GetLineCount().ToString();
			bntGroupsTarget.Text = tabGroupsTarget.GetLineCount().ToString();
			btnGroupsSource.Refresh();
			bntGroupsTarget.Refresh();
			tabGroupsSource.Refresh();
			tabGroupsTarget.Refresh();

			btnViewpointsSource.Text = tabViewpointsSource.GetLineCount().ToString();
			btnViewpointsTarget.Text = tabViewpointsTarget.GetLineCount().ToString();
			btnViewpointsSource.Refresh();
			btnViewpointsTarget.Refresh();
			tabViewpointsSource.Refresh();
			tabViewpointsTarget.Refresh();
		}

		private string CheckValidity()
		{
			string strMessage = "";
			if (cbViewName.Text == "")
			{
				strMessage = "Please insert a View Name!\n";
			}
			return strMessage;
		}

		private void EnableControlsForDefault(bool enable)
		{
			if(enable == true)
			{
				tabGroupsSource.Enabled = true;
				tabGroupsTarget.Enabled = true;
				tabGroupsSource.DisplayBackColor = UT.colYellow;

				txtGroups.Enabled = true;
				btnGroupsSource.Enabled = true;
				bntGroupsTarget.Enabled = true;

				tabViewpointsSource.Enabled = true;
				tabViewpointsTarget.Enabled = true;
				tabViewpointsSource.DisplayBackColor = UT.colYellow;

				txtViewpoints.Enabled = true;
				btnViewpointsSource.Enabled = true;
				btnViewpointsTarget.Enabled = true;

			}
			else
			{
				tabGroupsSource.Enabled = false;
				tabGroupsTarget.Enabled = false;
				tabGroupsSource.DisplayBackColor = UT.colLightGray;

				txtGroups.Enabled = false;
				btnGroupsSource.Enabled = false;
				bntGroupsTarget.Enabled = false;

				tabViewpointsSource.Enabled = false;
				tabViewpointsTarget.Enabled = false;
				tabViewpointsSource.DisplayBackColor = UT.colLightGray;

				txtViewpoints.Enabled = false;
				btnViewpointsSource.Enabled = false;
				btnViewpointsTarget.Enabled = false;

			}
		}

		private void cbViewName_SelectedValueChanged(object sender, System.EventArgs e)
		{
			string strName = cbViewName.Text;
			int i = 0;
			int j = 0;
			bool blFound = false;
			int idx = -1;
			NewView();
			cbViewName.Text = strName;
			if (strName == "<DEFAULT>")
			{
				EnableControlsForDefault(false);
				return;
			}

			EnableControlsForDefault(true);
			for( i = 0; i < myViewsTab.GetLineCount() && blFound == false; i++)
			{
				if (strName == myViewsTab.GetFieldValue(i, "NAME"))
				{
					idx = i;
					blFound = true;
				}
			}

			if(idx > -1) // then the view was found
			{
				string [] arrASG = myViewsTab.GetFieldValue(idx, "ASG").Split('|');

				if (arrASG.Length > 0)
				{
					j =  arrASG.Length - 1;
					for(i = tabGroupsSource.GetLineCount()-1; i >= 0 && j >= 0; i--)
					{
						if(tabGroupsSource.GetFieldValues(i, "URNO") == arrASG[j])
						{
							tabGroupsTarget.InsertTextLine(tabGroupsSource.GetFieldValues(i, "NAME,URNO"), false);
							tabGroupsSource.DeleteLine(i);
							j--;
						}
					}
				}

				tabGroupsSource.Sort("0", true, true);
				tabGroupsTarget.Sort("0", true, true);


				string [] arrAVP = myViewsTab.GetFieldValue(idx, "AVP").Split('|');

				if (arrAVP.Length > 0)
				{
					j =  arrAVP.Length - 1;
					for(i = tabViewpointsSource.GetLineCount()-1; i >= 0 && j >= 0; i--)
					{
						if(tabViewpointsSource.GetFieldValues(i, "URNO") == arrAVP[j])
						{
							tabViewpointsTarget.InsertTextLine(tabViewpointsSource.GetFieldValues(i, "NAME,URNO"), false);
							tabViewpointsSource.DeleteLine(i);
							j--;
						}
					}
				}

				tabViewpointsSource.Sort("0", true, true);
				tabViewpointsTarget.Sort("0", true, true);

				UpdateButtons();
			}
		
		}

		private void btnGroupsSource_Click(object sender, System.EventArgs e)
		{
			tabGroupsTarget.InsertBuffer(tabGroupsSource.GetBuffer(0, tabGroupsSource.GetLineCount()-1, "\n"), "\n");
			tabGroupsSource.ResetContent();
			tabGroupsSource.Sort("0", true, true);
			tabGroupsTarget.Sort("0", true, true);
			UpdateButtons();
		
		}

		private void bntGroupsTarget_Click(object sender, System.EventArgs e)
		{
			tabGroupsSource.InsertBuffer(tabGroupsTarget.GetBuffer(0, tabGroupsTarget.GetLineCount()-1, "\n"), "\n");
			tabGroupsTarget.ResetContent();
			tabGroupsSource.Sort("0", true, true);
			tabGroupsTarget.Sort("0", true, true);
			UpdateButtons();
		
		}

		private void tabGroupsSource_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo > -1)
			{
				tabGroupsTarget.InsertTextLine( tabGroupsSource.GetFieldValues(e.lineNo, "NAME,URNO"), true);
				tabGroupsSource.DeleteLine(e.lineNo);
				tabGroupsSource.Sort("0", true, true);
				tabGroupsTarget.Sort("0", true, true);
			}
			UpdateButtons();
		
		}

		private void tabGroupsTarget_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo > -1)
			{
				tabGroupsSource.InsertTextLine( tabGroupsTarget.GetFieldValues(e.lineNo, "NAME,URNO"), true);
				tabGroupsTarget.DeleteLine(e.lineNo);
				tabGroupsSource.Sort("0", true, true);
				tabGroupsTarget.Sort("0", true, true);
			}
			UpdateButtons();
		
		}

		private void btnNew_Click(object sender, System.EventArgs e)
		{
			NewView();
			UpdateButtons();
			cbViewName.Text = "";
		
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{

			string strMessage = SaveView();
			if (strMessage != "")
			{
				MessageBox.Show(this, strMessage, "Save Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
		
		}

		private void btnDelete_Click(object sender, System.EventArgs e)
		{
			DialogResult olResult;
			olResult = MessageBox.Show(this, "Do you really want to delete the current view?", "Question", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
			if( olResult != DialogResult.Yes)
			{
				return;
			}

			string strName = cbViewName.Text;
			if(strName == "<DEFAULT>")
			{
				return; //Nothing to do because it's <DEFAULT>
			}
			bool blFound = false;
			int idx = cbViewName.FindStringExact(strName, -1);
			if(idx > -1)
			{
				cbViewName.Items.RemoveAt(idx);
			}
			for(int i = 0; i < myViewsTab.GetLineCount() && blFound == false; i++)
			{
				if(strName == myViewsTab.GetFieldValue(i, "NAME"))
				{
					blFound = true; 
					myViewsTab.DeleteLine(i);
					myViewsTab.WriteToFile("C:\\Ufis\\System\\AIMSViewer_VIEWS.dat", false);
				}
			}
			cbViewName.Text = "";
			if(strName == ViewFilter.CurrentViewName)
			{
				ViewFilter.CurrentViewName = "<DEFAULT>";
			}
			NewView();
		}

		private void txtGroups_TextChanged(object sender, System.EventArgs e)
		{
			SearchForTabEntry(txtGroups, tabGroupsSource, 1);
		}

		private void SearchForTabEntry(System.Windows.Forms.TextBox txtBox, AxTABLib.AxTAB tabTab,int StartColumns)
		{
			string strText = "," + txtBox.Text;
			string strValues = "";
			int idxOrig = -1;
			bool blFound = false;
			int i = 0;
			int idx = tabTab.GetCurrentSelected();
			idxOrig = idx; //to remember for the wrap
			if(idx == -1) idx = 0;
			for( i = idx; i < tabTab.GetLineCount() && blFound == false; i++)
			{
				strValues = ",";
				for(int j = 0; j < StartColumns; j++)
				{
					strValues += tabTab.GetColumnValue(i, j) + ",";
				}
				if(strValues.IndexOf(strText, 0) > -1)
				{
					blFound = true;
					tabTab.SetCurrentSelection(i);
					tabTab.OnVScrollTo(i);
				}
			}
			if(blFound == false)
			{
				for( i = 0; i <= idxOrig && blFound == false; i++)
				{
					strValues = ",";
					for(int j = 0; j < StartColumns; j++)
					{
						strValues += tabTab.GetColumnValue(i, j) + ",";
					}
					if(strValues.IndexOf(strText, 0) > -1)
					{
						blFound = true;
						tabTab.SetCurrentSelection(i);
						tabTab.OnVScrollTo(i);
					}
				}
			}
		}

		private void txtGroups_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if(e.KeyChar == (char)13)
			{
				int currSel = tabGroupsSource.GetCurrentSelected();
				if(currSel >= 0)
				{
					tabGroupsTarget.InsertTextLine( tabGroupsSource.GetFieldValues(currSel, "NAME,URNO"), true);
					tabGroupsSource.DeleteLine(currSel);
					tabGroupsTarget.Sort("0", true, true);
					UpdateButtons();
				}
			}
		
		}

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			string strMessage = "";
			strMessage = SaveView();
			if (strMessage != "")
			{
				MessageBox.Show(this, strMessage, "Save Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				this.DialogResult = DialogResult.None;
			}
			else
			{
				this.Cursor = Cursors.WaitCursor;

				ViewFilter.CurrentViewName = cbViewName.Text;
				this.DialogResult = DialogResult.OK;
				this.Cursor = Cursors.Arrow;
			}
		
		}

		private string SaveView()
		{
			string strMessage = "";
			strMessage = CheckValidity();
			int i = 0;
			if (strMessage == "")
			{
				string strValues = "";
				string strName = cbViewName.Text;
				string strASG = "";
				string strAVP = "";

				if (strName != "<DEFAULT>")
				{
					strASG = tabGroupsTarget.GetBufferByFieldList(0, tabGroupsTarget.GetLineCount()-1, "URNO", "|");
					strAVP = tabViewpointsTarget.GetBufferByFieldList(0, tabViewpointsTarget.GetLineCount()-1, "URNO", "|");
					for (i = myViewsTab.GetLineCount()-1; i >= 0 ; i--)
					{
						if(myViewsTab.GetFieldValue(i, "NAME") == strName)
						{
							myViewsTab.DeleteLine(i);
						}
					}

					//"NAME,ASG";
					strValues = strName + "," + strASG + "," + strAVP;
					myViewsTab.InsertTextLine(strValues, true);
					myViewsTab.WriteToFile("C:\\Ufis\\System\\AIMSViewer_VIEWS.dat", false);
				}

				if(cbViewName.FindStringExact(strName) == -1)
				{
					cbViewName.Items.Add(strName);
				}
			}
			return strMessage;
		}

		private void cbViewName_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if(e.KeyChar == (char)44)// check for comma
			{
				e.Handled = true;
			}
		}

		private void panel1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, panel1, Color.WhiteSmoke, Color.LightGray);
		}

		private void cbViewName_TextChanged(object sender, System.EventArgs e)
		{
			string strText = cbViewName.Text;
			if(strText != "<DEFAULT>")
			{
				EnableControlsForDefault(true);
			}
			else
			{
				EnableControlsForDefault(false);
			}
			if(strText == "")
			{
				btnOK.Enabled = false;
				btnDelete.Enabled = false;
				btnSave.Enabled = false;
			}
			else
			{
				btnOK.Enabled = true;
				btnDelete.Enabled = true;
				btnSave.Enabled = true;
			}
		
		}

		private void View_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, this, Color.WhiteSmoke, Color.LightGray);
		}

		private void txtViewpoints_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if(e.KeyChar == (char)13)
			{
				int currSel = tabViewpointsSource.GetCurrentSelected();
				if(currSel >= 0)
				{
					tabViewpointsTarget.InsertTextLine( tabViewpointsSource.GetFieldValues(currSel, "NAME,URNO"), true);
					tabViewpointsSource.DeleteLine(currSel);
					tabViewpointsTarget.Sort("0", true, true);
					UpdateButtons();
				}
			}
		}

		private void txtViewpoints_TextChanged(object sender, System.EventArgs e)
		{
			SearchForTabEntry(txtViewpoints, tabViewpointsSource, 1);
		}

		private void btnViewpointsSource_Click(object sender, System.EventArgs e)
		{
			tabViewpointsTarget.InsertBuffer(tabViewpointsSource.GetBuffer(0, tabViewpointsSource.GetLineCount()-1, "\n"), "\n");
			tabViewpointsSource.ResetContent();
			tabViewpointsSource.Sort("0", true, true);
			tabViewpointsTarget.Sort("0", true, true);
			UpdateButtons();
		}

		private void btnViewpointsTarget_Click(object sender, System.EventArgs e)
		{
			tabViewpointsSource.InsertBuffer(tabViewpointsTarget.GetBuffer(0, tabViewpointsTarget.GetLineCount()-1, "\n"), "\n");
			tabViewpointsTarget.ResetContent();
			tabViewpointsSource.Sort("0", true, true);
			tabViewpointsTarget.Sort("0", true, true);
			UpdateButtons();
		}

		private void tabViewpointsSource_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo > -1)
			{
				tabViewpointsTarget.InsertTextLine( tabViewpointsSource.GetFieldValues(e.lineNo, "NAME,URNO"), true);
				tabViewpointsSource.DeleteLine(e.lineNo);
				tabViewpointsSource.Sort("0", true, true);
				tabViewpointsTarget.Sort("0", true, true);
			}
			UpdateButtons();
		
		}

		private void tabViewpointsTarget_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo > -1)
			{
				tabViewpointsSource.InsertTextLine( tabViewpointsTarget.GetFieldValues(e.lineNo, "NAME,URNO"), true);
				tabViewpointsTarget.DeleteLine(e.lineNo);
				tabViewpointsSource.Sort("0", true, true);
				tabViewpointsTarget.Sort("0", true, true);
			}
			UpdateButtons();
		
		}
	}
}
