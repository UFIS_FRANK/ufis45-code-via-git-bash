using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Ufis.Data;
using Ufis.Utils;

namespace AIMSViewer
{
	/// <summary>
	/// Summary description for StatusGroups.
	/// </summary>
	public class StatusGroups : System.Windows.Forms.Form
	{
		private System.Windows.Forms.NumericUpDown udHeight;
		private System.Windows.Forms.NumericUpDown udWidth;
		private System.Windows.Forms.Label lblHeight;
		private System.Windows.Forms.Label lblWidth;
		private System.Windows.Forms.Label lblBrace;
		private System.Windows.Forms.Label lblInMeters;
		private System.Windows.Forms.Label lblSubsystem;
		private System.Windows.Forms.ComboBox cbSubsystems;
		private System.Windows.Forms.Label lblOriginator;
		private System.Windows.Forms.TextBox txtOriginator;
		private System.Windows.Forms.TextBox txtStatusType;
		private System.Windows.Forms.Label lblStatusType;
		private System.Windows.Forms.TextBox txtImage;
		private System.Windows.Forms.Button btnBrowseImage;
		private System.Windows.Forms.ComboBox cbGroupNames;
		private System.Windows.Forms.Label lblGroupName;
		private System.Windows.Forms.Label lblPreview;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.Button bntDelete;
		private System.Windows.Forms.Label lblImagePath;
		private System.Windows.Forms.Button btnNew;
		private System.Windows.Forms.GroupBox grpImage;
		private System.Windows.Forms.PictureBox pbPreview;
		private System.Windows.Forms.ImageList imageList1;
		private System.ComponentModel.IContainer components;

		#region	implementation
		private	IRow	currRow = null;
		private	IRow	oldRow	= null;
		private	IRow	currDBRow = null;
		private	string	myMode	= string.Empty;
		private	int		nameInd = -1;
		private System.Windows.Forms.CheckBox cbOriginalSize;
		private static bool recursive = false;
		private System.Windows.Forms.ToolTip toolTip1;
		private	Size	previewSize;
		#endregion
		public StatusGroups()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			this.txtImage.TextChanged += new EventHandler(txtImage_TextChanged);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(StatusGroups));
			this.lblSubsystem = new System.Windows.Forms.Label();
			this.cbSubsystems = new System.Windows.Forms.ComboBox();
			this.lblOriginator = new System.Windows.Forms.Label();
			this.txtOriginator = new System.Windows.Forms.TextBox();
			this.txtStatusType = new System.Windows.Forms.TextBox();
			this.lblStatusType = new System.Windows.Forms.Label();
			this.txtImage = new System.Windows.Forms.TextBox();
			this.lblImagePath = new System.Windows.Forms.Label();
			this.btnBrowseImage = new System.Windows.Forms.Button();
			this.udHeight = new System.Windows.Forms.NumericUpDown();
			this.udWidth = new System.Windows.Forms.NumericUpDown();
			this.lblHeight = new System.Windows.Forms.Label();
			this.lblWidth = new System.Windows.Forms.Label();
			this.lblBrace = new System.Windows.Forms.Label();
			this.lblInMeters = new System.Windows.Forms.Label();
			this.lblPreview = new System.Windows.Forms.Label();
			this.cbGroupNames = new System.Windows.Forms.ComboBox();
			this.lblGroupName = new System.Windows.Forms.Label();
			this.pbPreview = new System.Windows.Forms.PictureBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnSave = new System.Windows.Forms.Button();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.btnClose = new System.Windows.Forms.Button();
			this.bntDelete = new System.Windows.Forms.Button();
			this.btnNew = new System.Windows.Forms.Button();
			this.grpImage = new System.Windows.Forms.GroupBox();
			this.cbOriginalSize = new System.Windows.Forms.CheckBox();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			((System.ComponentModel.ISupportInitialize)(this.udHeight)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.udWidth)).BeginInit();
			this.panel1.SuspendLayout();
			this.grpImage.SuspendLayout();
			this.SuspendLayout();
			// 
			// lblSubsystem
			// 
			this.lblSubsystem.BackColor = System.Drawing.Color.Transparent;
			this.lblSubsystem.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblSubsystem.Location = new System.Drawing.Point(16, 52);
			this.lblSubsystem.Name = "lblSubsystem";
			this.lblSubsystem.Size = new System.Drawing.Size(92, 16);
			this.lblSubsystem.TabIndex = 0;
			this.lblSubsystem.Text = "Subsystem:";
			// 
			// cbSubsystems
			// 
			this.cbSubsystems.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbSubsystems.Items.AddRange(new object[] {
															  "BAS",
															  "CASS",
															  "FDA",
															  "SCADA"});
			this.cbSubsystems.Location = new System.Drawing.Point(116, 48);
			this.cbSubsystems.Name = "cbSubsystems";
			this.cbSubsystems.Size = new System.Drawing.Size(192, 22);
			this.cbSubsystems.Sorted = true;
			this.cbSubsystems.TabIndex = 2;
			this.cbSubsystems.Tag = "SUBS";
			this.toolTip1.SetToolTip(this.cbSubsystems, "Select one of the subsystems");
			this.cbSubsystems.TextChanged += new System.EventHandler(this.cbSubsystems_TextChanged);
			this.cbSubsystems.SelectedIndexChanged += new System.EventHandler(this.cbSubsystems_TextChanged);
			this.cbSubsystems.Leave += new System.EventHandler(this.cbSubsystems_TextChanged);
			// 
			// lblOriginator
			// 
			this.lblOriginator.BackColor = System.Drawing.Color.Transparent;
			this.lblOriginator.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblOriginator.Location = new System.Drawing.Point(16, 80);
			this.lblOriginator.Name = "lblOriginator";
			this.lblOriginator.Size = new System.Drawing.Size(92, 16);
			this.lblOriginator.TabIndex = 2;
			this.lblOriginator.Text = "Originator:";
			// 
			// txtOriginator
			// 
			this.txtOriginator.Location = new System.Drawing.Point(116, 76);
			this.txtOriginator.Name = "txtOriginator";
			this.txtOriginator.Size = new System.Drawing.Size(192, 20);
			this.txtOriginator.TabIndex = 3;
			this.txtOriginator.Tag = "ORIG";
			this.txtOriginator.Text = "";
			this.toolTip1.SetToolTip(this.txtOriginator, "Key in the name of the originator");
			this.txtOriginator.TextChanged += new System.EventHandler(this.cbSubsystems_TextChanged);
			this.txtOriginator.Leave += new System.EventHandler(this.cbSubsystems_TextChanged);
			// 
			// txtStatusType
			// 
			this.txtStatusType.Location = new System.Drawing.Point(116, 104);
			this.txtStatusType.Name = "txtStatusType";
			this.txtStatusType.Size = new System.Drawing.Size(192, 20);
			this.txtStatusType.TabIndex = 4;
			this.txtStatusType.Tag = "TYPE";
			this.txtStatusType.Text = "";
			this.toolTip1.SetToolTip(this.txtStatusType, "Key in the type of the status");
			this.txtStatusType.TextChanged += new System.EventHandler(this.cbSubsystems_TextChanged);
			this.txtStatusType.Leave += new System.EventHandler(this.cbSubsystems_TextChanged);
			// 
			// lblStatusType
			// 
			this.lblStatusType.BackColor = System.Drawing.Color.Transparent;
			this.lblStatusType.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblStatusType.Location = new System.Drawing.Point(16, 108);
			this.lblStatusType.Name = "lblStatusType";
			this.lblStatusType.Size = new System.Drawing.Size(92, 16);
			this.lblStatusType.TabIndex = 4;
			this.lblStatusType.Text = "Device group:";
			// 
			// txtImage
			// 
			this.txtImage.Location = new System.Drawing.Point(112, 420);
			this.txtImage.Name = "txtImage";
			this.txtImage.ReadOnly = true;
			this.txtImage.Size = new System.Drawing.Size(268, 20);
			this.txtImage.TabIndex = 8;
			this.txtImage.Tag = "IMAG";
			this.txtImage.Text = "C:\\AIMSViewer\\Symbol Images\\Elevator1A23F4";
			this.txtImage.TextChanged += new System.EventHandler(this.cbSubsystems_TextChanged);
			// 
			// lblImagePath
			// 
			this.lblImagePath.BackColor = System.Drawing.Color.Transparent;
			this.lblImagePath.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblImagePath.Location = new System.Drawing.Point(12, 424);
			this.lblImagePath.Name = "lblImagePath";
			this.lblImagePath.Size = new System.Drawing.Size(92, 16);
			this.lblImagePath.TabIndex = 6;
			this.lblImagePath.Text = "Path:";
			// 
			// btnBrowseImage
			// 
			this.btnBrowseImage.BackColor = System.Drawing.Color.Transparent;
			this.btnBrowseImage.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnBrowseImage.Location = new System.Drawing.Point(316, 220);
			this.btnBrowseImage.Name = "btnBrowseImage";
			this.btnBrowseImage.Size = new System.Drawing.Size(64, 20);
			this.btnBrowseImage.TabIndex = 7;
			this.btnBrowseImage.Text = "Browse ...";
			this.toolTip1.SetToolTip(this.btnBrowseImage, "Browse for the image file which will be displayed for the current group");
			this.btnBrowseImage.Click += new System.EventHandler(this.btnBrowseImage_Click);
			// 
			// udHeight
			// 
			this.udHeight.Location = new System.Drawing.Point(116, 188);
			this.udHeight.Maximum = new System.Decimal(new int[] {
																	 10000,
																	 0,
																	 0,
																	 0});
			this.udHeight.Minimum = new System.Decimal(new int[] {
																	 1,
																	 0,
																	 0,
																	 0});
			this.udHeight.Name = "udHeight";
			this.udHeight.Size = new System.Drawing.Size(72, 20);
			this.udHeight.TabIndex = 6;
			this.udHeight.Tag = "HEIG";
			this.udHeight.Value = new System.Decimal(new int[] {
																   20,
																   0,
																   0,
																   0});
			this.udHeight.ValueChanged += new System.EventHandler(this.cbSubsystems_TextChanged);
			this.udHeight.Leave += new System.EventHandler(this.cbSubsystems_TextChanged);
			// 
			// udWidth
			// 
			this.udWidth.Location = new System.Drawing.Point(116, 164);
			this.udWidth.Maximum = new System.Decimal(new int[] {
																	10000,
																	0,
																	0,
																	0});
			this.udWidth.Minimum = new System.Decimal(new int[] {
																	1,
																	0,
																	0,
																	0});
			this.udWidth.Name = "udWidth";
			this.udWidth.Size = new System.Drawing.Size(72, 20);
			this.udWidth.TabIndex = 5;
			this.udWidth.Tag = "WIDT";
			this.udWidth.Value = new System.Decimal(new int[] {
																  20,
																  0,
																  0,
																  0});
			this.udWidth.ValueChanged += new System.EventHandler(this.cbSubsystems_TextChanged);
			this.udWidth.Leave += new System.EventHandler(this.cbSubsystems_TextChanged);
			// 
			// lblHeight
			// 
			this.lblHeight.BackColor = System.Drawing.Color.Transparent;
			this.lblHeight.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblHeight.Location = new System.Drawing.Point(16, 192);
			this.lblHeight.Name = "lblHeight";
			this.lblHeight.Size = new System.Drawing.Size(84, 16);
			this.lblHeight.TabIndex = 10;
			this.lblHeight.Text = "Height:";
			// 
			// lblWidth
			// 
			this.lblWidth.BackColor = System.Drawing.Color.Transparent;
			this.lblWidth.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblWidth.Location = new System.Drawing.Point(16, 168);
			this.lblWidth.Name = "lblWidth";
			this.lblWidth.Size = new System.Drawing.Size(84, 16);
			this.lblWidth.TabIndex = 9;
			this.lblWidth.Text = "Width:";
			// 
			// lblBrace
			// 
			this.lblBrace.BackColor = System.Drawing.Color.Transparent;
			this.lblBrace.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblBrace.Location = new System.Drawing.Point(176, 156);
			this.lblBrace.Name = "lblBrace";
			this.lblBrace.Size = new System.Drawing.Size(24, 60);
			this.lblBrace.TabIndex = 14;
			this.lblBrace.Text = "}";
			// 
			// lblInMeters
			// 
			this.lblInMeters.BackColor = System.Drawing.Color.Transparent;
			this.lblInMeters.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblInMeters.Location = new System.Drawing.Point(228, 180);
			this.lblInMeters.Name = "lblInMeters";
			this.lblInMeters.Size = new System.Drawing.Size(64, 16);
			this.lblInMeters.TabIndex = 13;
			this.lblInMeters.Text = "in pixels";
			// 
			// lblPreview
			// 
			this.lblPreview.BackColor = System.Drawing.Color.Transparent;
			this.lblPreview.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblPreview.Location = new System.Drawing.Point(16, 224);
			this.lblPreview.Name = "lblPreview";
			this.lblPreview.Size = new System.Drawing.Size(80, 16);
			this.lblPreview.TabIndex = 15;
			this.lblPreview.Text = "Preview:";
			// 
			// cbGroupNames
			// 
			this.cbGroupNames.Location = new System.Drawing.Point(116, 16);
			this.cbGroupNames.Name = "cbGroupNames";
			this.cbGroupNames.Size = new System.Drawing.Size(192, 22);
			this.cbGroupNames.Sorted = true;
			this.cbGroupNames.TabIndex = 1;
			this.cbGroupNames.Tag = "NAME";
			this.toolTip1.SetToolTip(this.cbGroupNames, "Select the name of an existing group for update ");
			this.cbGroupNames.TextChanged += new System.EventHandler(this.cbSubsystems_TextChanged);
			this.cbGroupNames.SelectedIndexChanged += new System.EventHandler(this.cbGroupNames_SelectedIndexChanged);
			this.cbGroupNames.Leave += new System.EventHandler(this.cbSubsystems_TextChanged);
			// 
			// lblGroupName
			// 
			this.lblGroupName.BackColor = System.Drawing.Color.Transparent;
			this.lblGroupName.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblGroupName.Location = new System.Drawing.Point(16, 20);
			this.lblGroupName.Name = "lblGroupName";
			this.lblGroupName.Size = new System.Drawing.Size(92, 16);
			this.lblGroupName.TabIndex = 19;
			this.lblGroupName.Text = "Group name:";
			// 
			// pbPreview
			// 
			this.pbPreview.Location = new System.Drawing.Point(116, 220);
			this.pbPreview.Name = "pbPreview";
			this.pbPreview.Size = new System.Drawing.Size(192, 192);
			this.pbPreview.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pbPreview.TabIndex = 21;
			this.pbPreview.TabStop = false;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnSave);
			this.panel1.Controls.Add(this.btnClose);
			this.panel1.Controls.Add(this.bntDelete);
			this.panel1.Controls.Add(this.btnNew);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 458);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(402, 48);
			this.panel1.TabIndex = 22;
			this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
			// 
			// btnSave
			// 
			this.btnSave.BackColor = System.Drawing.Color.Transparent;
			this.btnSave.Dock = System.Windows.Forms.DockStyle.Left;
			this.btnSave.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnSave.ImageIndex = 2;
			this.btnSave.ImageList = this.imageList1;
			this.btnSave.Location = new System.Drawing.Point(232, 0);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(72, 48);
			this.btnSave.TabIndex = 12;
			this.btnSave.Text = "   &Save";
			this.toolTip1.SetToolTip(this.btnSave, "Save the currently active group");
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// imageList1
			// 
			this.imageList1.ImageSize = new System.Drawing.Size(19, 19);
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.White;
			// 
			// btnClose
			// 
			this.btnClose.BackColor = System.Drawing.Color.Transparent;
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnClose.Dock = System.Windows.Forms.DockStyle.Left;
			this.btnClose.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnClose.ImageIndex = 4;
			this.btnClose.ImageList = this.imageList1;
			this.btnClose.Location = new System.Drawing.Point(160, 0);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(72, 48);
			this.btnClose.TabIndex = 11;
			this.btnClose.Text = "   &Close";
			this.toolTip1.SetToolTip(this.btnClose, "Close this window");
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// bntDelete
			// 
			this.bntDelete.BackColor = System.Drawing.Color.Transparent;
			this.bntDelete.Dock = System.Windows.Forms.DockStyle.Left;
			this.bntDelete.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.bntDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.bntDelete.ImageIndex = 1;
			this.bntDelete.ImageList = this.imageList1;
			this.bntDelete.Location = new System.Drawing.Point(88, 0);
			this.bntDelete.Name = "bntDelete";
			this.bntDelete.Size = new System.Drawing.Size(72, 48);
			this.bntDelete.TabIndex = 10;
			this.bntDelete.Text = "    &Delete";
			this.toolTip1.SetToolTip(this.bntDelete, "Delete the currently active status group");
			this.bntDelete.Click += new System.EventHandler(this.bntDelete_Click);
			// 
			// btnNew
			// 
			this.btnNew.BackColor = System.Drawing.Color.Transparent;
			this.btnNew.Dock = System.Windows.Forms.DockStyle.Left;
			this.btnNew.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnNew.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnNew.ImageIndex = 0;
			this.btnNew.ImageList = this.imageList1;
			this.btnNew.Location = new System.Drawing.Point(0, 0);
			this.btnNew.Name = "btnNew";
			this.btnNew.Size = new System.Drawing.Size(88, 48);
			this.btnNew.TabIndex = 9;
			this.btnNew.Text = "&New Group";
			this.toolTip1.SetToolTip(this.btnNew, "Create a new status group");
			this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
			// 
			// grpImage
			// 
			this.grpImage.BackColor = System.Drawing.Color.Transparent;
			this.grpImage.Controls.Add(this.cbOriginalSize);
			this.grpImage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.grpImage.Location = new System.Drawing.Point(8, 140);
			this.grpImage.Name = "grpImage";
			this.grpImage.Size = new System.Drawing.Size(388, 308);
			this.grpImage.TabIndex = 23;
			this.grpImage.TabStop = false;
			this.grpImage.Text = "Image";
			// 
			// cbOriginalSize
			// 
			this.cbOriginalSize.Appearance = System.Windows.Forms.Appearance.Button;
			this.cbOriginalSize.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cbOriginalSize.Location = new System.Drawing.Point(308, 28);
			this.cbOriginalSize.Name = "cbOriginalSize";
			this.cbOriginalSize.Size = new System.Drawing.Size(64, 36);
			this.cbOriginalSize.TabIndex = 0;
			this.cbOriginalSize.Text = "Original size";
			this.toolTip1.SetToolTip(this.cbOriginalSize, "Displays the selected image in preview or original size");
			this.cbOriginalSize.CheckedChanged += new System.EventHandler(this.cbOriginalSize_CheckedChanged);
			// 
			// StatusGroups
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 13);
			this.ClientSize = new System.Drawing.Size(402, 506);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.pbPreview);
			this.Controls.Add(this.cbGroupNames);
			this.Controls.Add(this.lblGroupName);
			this.Controls.Add(this.lblPreview);
			this.Controls.Add(this.lblBrace);
			this.Controls.Add(this.lblInMeters);
			this.Controls.Add(this.udHeight);
			this.Controls.Add(this.udWidth);
			this.Controls.Add(this.lblHeight);
			this.Controls.Add(this.lblWidth);
			this.Controls.Add(this.btnBrowseImage);
			this.Controls.Add(this.txtImage);
			this.Controls.Add(this.txtStatusType);
			this.Controls.Add(this.txtOriginator);
			this.Controls.Add(this.lblImagePath);
			this.Controls.Add(this.lblStatusType);
			this.Controls.Add(this.lblOriginator);
			this.Controls.Add(this.cbSubsystems);
			this.Controls.Add(this.lblSubsystem);
			this.Controls.Add(this.grpImage);
			this.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "StatusGroups";
			this.Text = "Status groups ...";
			this.Load += new System.EventHandler(this.StatusGroups_Load);
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.StatusGroups_Paint);
			((System.ComponentModel.ISupportInitialize)(this.udHeight)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.udWidth)).EndInit();
			this.panel1.ResumeLayout(false);
			this.grpImage.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void btnBrowseImage_Click(object sender, System.EventArgs e)
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Filter = "JPG-files (*.jpg)|*.jpg|TIF-filed (*.tif)|*.tif|bmp-files (*.bmp)|*.bmp|GIF-files (*.gif)|*.gif";
			dlg.InitialDirectory = frmData.GetThis().InitialDirectory;

			if (dlg.ShowDialog() == DialogResult.OK)
			{
				try
				{
					this.pbPreview.Image = new Bitmap(dlg.FileName);
					this.txtImage.Text	 = dlg.FileName;
				}
				catch(Exception err)
				{
					string strMessage = "";
					strMessage += err.Message.ToString() + "\n\n";
					strMessage += "Bitmap file cannot be opened!!\n";
					strMessage += dlg.FileName + "\n\n";
					strMessage += "Please check the correctness of the specified path and the access rights to the specified location!!!\n";
					MessageBox.Show(this, strMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
					this.pbPreview.Image = null;
					this.txtImage.Text	 = string.Empty;
				}

			}
		
		}

		private void btnNew_Click(object sender, System.EventArgs e)
		{
			NewGroup();
		}

		private void StatusGroups_Load(object sender, System.EventArgs e)
		{
						
			// security requirements
			switch(frmData.GetPrivileges("StatusGroupsDlgSave"))
			{
				case "-":
					this.btnSave.Enabled = false;
					this.btnSave.Visible = false;
					break;
				case "0":
					this.btnSave.Enabled = false;
					break;
			}

			// add other systems to list of sub systems
			if (frmData.OtherSystems != null)
			{
				this.cbSubsystems.Items.AddRange(frmData.OtherSystems);
			}

			// security requirement for subsystems
			for(int i = this.cbSubsystems.Items.Count - 1; i >= 0; i--)
			{
				string prv = "Subsystem" + (string)this.cbSubsystems.Items[i];
				switch(frmData.GetPrivileges(prv))
				{
					case "-":
						this.cbSubsystems.Items.RemoveAt(i);
						break;
					case "0":
						this.cbSubsystems.Items.RemoveAt(i);
						break;
				}

			}

			IDatabase myDB = UT.GetMemDB();
			ITable	myGrps = myDB["ASG"];
			for (int i = 0; i < myGrps.Count;i++)
			{
				IRow myRow = myGrps[i];
				if (this.cbSubsystems.Items.IndexOf(myRow["SUBS"]) >= 0)
				{
					this.cbGroupNames.Items.Add(myRow["NAME"]);
				}
			}

			this.nameInd = myGrps.FieldList.IndexOf("NAME");

			this.NewGroup();

			this.previewSize = this.pbPreview.Size;
		}

		private void cbGroupNames_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!recursive && this.cbGroupNames.SelectedIndex >= 0)
			{
				recursive = true;
				string name = (string)this.cbGroupNames.SelectedItem;		
				if (this.OpenGroup(name))
				{
					this.cbGroupNames.DropDownStyle = ComboBoxStyle.DropDownList;
				}

				recursive = false;
			}
		
		}

		private void bntDelete_Click(object sender, System.EventArgs e)
		{
			DialogResult olResult;
			olResult = MessageBox.Show(this, "Do you really want to delete the current group?", "Question", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
			if( olResult != DialogResult.Yes)
			{
				return;
			}

			if (this.cbGroupNames.SelectedIndex >= 0)
			{
				string name = (string)this.cbGroupNames.SelectedItem;		
				IDatabase myDB = UT.GetMemDB();
				ITable	myTable= myDB["ASG"];	
				IRow[]	myRows = myTable.RowsByIndexValue("NAME",name);
				if (myRows.Length == 1)
				{
					string urno = myRows[0]["URNO"];
					myRows[0].Status = State.Deleted;
					myTable.Save(myRows[0]);

					myTable = myDB["ASD"];
					myRows  = myTable.RowsByIndexValue("SGRP",urno);
					if (myRows != null)
					{
						for (int i = 0; i < myRows.Length; i++)
						{
							myRows[i]["USEU"]	= UT.UserName;
							myRows[i]["LSTU"]	= UT.DateTimeToCeda(DateTime.Now.ToUniversalTime());
							myRows[i].Status = State.Deleted;
						}

						if (myTable.Save())
						{
							this.cbGroupNames.Items.RemoveAt(this.cbGroupNames.SelectedIndex);
							NewGroup();
						}
					}
				}
			}
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
		
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			SaveGroup();				
		}

		private bool CheckDataChanged()
		{
			bool changed = false;
			ArrayList arrCurr = currRow.FieldValueList();
			ArrayList arrOld  = oldRow.FieldValueList();
			for(int i = 0; i < arrCurr.Count && changed == false; i++)
			{
				if (this.cbGroupNames.SelectedIndex != -1 && i == this.nameInd)
					continue;
				else if(arrCurr[i].ToString() != arrOld[i].ToString())
				{
					changed = true;
				}
			}

			// when mode is NEW -> check, if group name matches one of the group name list
			if (this.myMode == "NEW")
			{
				if (this.cbGroupNames.Items.IndexOf(this.cbGroupNames.Text) >= 0)
				{
					if (this.btnSave.BackColor == Color.Red)
					{
						this.btnSave.BackColor = Color.Transparent;
					}
					return false;
				}
			}

			if (changed)
			{
				if (this.btnSave.Enabled)
				{
					this.btnSave.BackColor = Color.Red;
				}
			}
			else
			{
				this.btnSave.BackColor = Color.Transparent;
			}

			return changed;
		}

		private bool Validate(out string errMsg)
		{
			bool result = true;
			errMsg = string.Empty;

			this.cbGroupNames.Text = this.cbGroupNames.Text.Trim();
			if (this.cbGroupNames.Text.Length == 0)
			{
				errMsg += "Group name is mandatory\n";
				result = false;
			}
			else
			{
				IDatabase myDB = UT.GetMemDB();
				ITable	myTable= myDB["ASG"];	
				IRow[]	myRows = myTable.RowsByIndexValue("NAME",this.cbGroupNames.Text);
				if (myRows != null)
				{
					for (int i = 0; i < myRows.Length; i++)
					{
						if (myRows[i]["URNO"] != this.currDBRow["URNO"])
						{
							errMsg += "Group name already exists\n";
							result = false;
						}
					}
				}
			}
			
			if (this.cbSubsystems.Text.Length == 0)
			{
				errMsg += "Subsystem is mandatory\n";
				result = false;
			}
						
			if (this.txtOriginator.Text.Length == 0)
			{
				errMsg += "Originator is mandatory\n";
				result = false;
			}
						
			if (this.txtStatusType.Text.Length == 0)
			{
				errMsg += "Type is mandatory\n";
				result = false;
			}
						
			if (this.txtImage.Text.Length == 0)
			{
				errMsg += "Image is mandatory\n";
				result = false;
			}
						
			return result;
		}

		private bool SaveGroup()
		{
			if (btnSave.BackColor == Color.Transparent )
			{
				MessageBox.Show(this, "Nothing to save!!", "Info");
				return true;
			}

			string errMsg;
			if (!this.Validate(out errMsg))
			{
				MessageBox.Show(this,errMsg, "Error");
				return false;
			}

			IDatabase myDB = UT.GetMemDB();
			ITable	myTable= myDB["ASG"];	

			if (this.myMode == "NEW")
			{
				string strTime = UT.DateTimeToCeda(DateTime.Now);
				currRow["CDAT"] = strTime;
				currRow["USEC"] = UT.UserName;
				currDBRow.Status = State.Created;
			}
			else if (this.myMode == "UPDATE")
			{
				string strTime = UT.DateTimeToCeda( DateTime.Now);
				currRow["LSTU"] = strTime;
				currRow["USEU"] = UT.UserName;
				currDBRow.Status = State.Modified;
			}

			ArrayList arrCurr = currRow.FieldValueList();
			for(int i = 0; i < arrCurr.Count; i++)
			{
				currDBRow[i] = currRow[i];
				oldRow[i]	 = currRow[i]; //To remember the current values, because we are in updatemode from now on.
			}

			// must copy values first before adding the row to the database, else indexes will be invalid because values are empty
			if (this.myMode == "NEW")
			{
				myTable.Add(currDBRow);
			}
			if (myTable.Save())
			{
				myMode = "UPDATE";
				this.btnSave.BackColor = Color.Transparent;
				this.cbGroupNames.DropDownStyle = ComboBoxStyle.DropDownList;
				int ind = this.cbGroupNames.Items.IndexOf(currRow["NAME"]);
				if (ind < 0)
				{
					ind = this.cbGroupNames.Items.Add(currRow["NAME"]);
				}

				this.cbGroupNames.SelectedIndex = ind;
				return true;
			}
			else
				return false;

		}

		void NewGroup()
		{
			if(btnSave.BackColor == Color.Red )
			{
				DialogResult olResult;
				olResult = MessageBox.Show(this, "Do you want to save current group?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question); 
				if ( olResult == DialogResult.Yes)
				{
					if (SaveGroup() == false) 
						return;
				}
			}

			IDatabase myDB = UT.GetMemDB();
			ITable	myTable= myDB["ASG"];

			this.currDBRow = myTable.CreateEmptyRow();
			this.currDBRow["WIDT"] = this.udWidth.Value.ToString();
			this.currDBRow["HEIG"] = this.udHeight.Value.ToString();
			this.currRow   = currDBRow.CopyRaw();
			this.oldRow    = currRow.CopyRaw();
			this.myMode	   = "NEW";

			ClearControls(this);
			this.cbSubsystems.SelectedIndex = -1;
			this.cbGroupNames.DropDownStyle = ComboBoxStyle.DropDown;
			this.cbGroupNames.Text = "";
			this.cbGroupNames.Refresh();
			this.cbGroupNames.Focus();
		}

		void ClearControls(System.Windows.Forms.Control pControl)
		{
			foreach(System.Windows.Forms.Control olC in pControl.Controls )
			{
				string lType = olC.GetType().ToString();
				if (lType == "System.Windows.Forms.PictureBox" || lType == "System.Windows.Forms.Panel")
				{
					ClearControls(olC);
				}
				else if (lType == "System.Windows.Forms.TextBox" || lType == "System.Windows.Forms.ComboBox" )
				{
					if (olC.Tag != null && olC.Tag.ToString().Length > 0)
					{
						olC.Text = "";
					}
				}
				else if (lType == "System.Windows.Forms.NumericUpDown")
				{
					if (olC.Tag != null)
					{
						NumericUpDown udCtrl = (NumericUpDown)olC;
						udCtrl.Value = udCtrl.Value;
					}
				}
			}
		}

		private Control GetControlByTag(string tag, Control currCtl, ref Control myControl )
		{
			if(myControl == null)
			{
				foreach(System.Windows.Forms.Control olC in currCtl.Controls )
				{
					string lType = olC.GetType().ToString();
					if(lType == "System.Windows.Forms.PictureBox" || lType == "System.Windows.Forms.Panel")
					{
						if(olC.Tag != null)
							if(olC.Tag.ToString() != tag)
								myControl = GetControlByTag(tag.ToString(), olC, ref myControl);
							else
								myControl = olC;
						else
							myControl = GetControlByTag(tag.ToString(), olC, ref myControl);
					}
					else
					{
						if(olC.Tag != null)
						{
							if (olC.Tag.ToString() == tag)
								myControl = olC;
						}
					}
				}
			}
			return myControl;
		}

		private bool OpenGroup(string name)
		{
			if (btnSave.BackColor == Color.Red )
			{
				DialogResult olResult;
				olResult = MessageBox.Show(this, "Do you want to save current group?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question); 
				if (olResult == DialogResult.Yes)
				{
					if (SaveGroup() == false) 
						return false;
				}
			}

			this.ClearControls(this);

			IDatabase myDB = UT.GetMemDB();
			ITable	myTable= myDB["ASG"];
			IRow[]	myRows = myTable.RowsByIndexValue("NAME",name);
			if (myRows != null && myRows.Length == 1)
			{
				string [] arrFields = "NAME,SUBS,ORIG,TYPE,IMAG,WIDT,HEIG".Split(',');
				for(int i = 0; i < arrFields.Length; i++)					
				{
					Control ctl = null;
					ctl = GetControlByTag(arrFields[i], this, ref ctl);
					if (ctl != null)
					{
						string strText = myRows[0][arrFields[i]];
						strText = strText.Trim();

						string lType = ctl.GetType().ToString();
						if (lType == "System.Windows.Forms.NumericUpDown")
						{
							NumericUpDown udCtrl = (NumericUpDown)ctl;
							udCtrl.Value = decimal.Parse(strText);
						}
						else
						{
							ctl.Text = frmData.GetThis().Clean(strText,true);
						}
					}
				}

				currDBRow = myRows[0];
				currRow	  = currDBRow.CopyRaw();
				oldRow	  = currRow.CopyRaw();

				btnSave.BackColor = Color.Transparent;
				this.myMode = "UPDATE";

				return true;
			}
			else
			{
				return false;
			}
		}

		private void cbSubsystems_TextChanged(object sender, System.EventArgs e)
		{
			Control ctl = (Control)sender;
			if (ctl.Tag != null)
			{
				string lType = ctl.GetType().ToString();
				if (lType == "System.Windows.Forms.NumericUpDown")
				{
					NumericUpDown udCtrl = (NumericUpDown)ctl;
					currRow[ctl.Tag.ToString()] = udCtrl.Value.ToString();
					cbOriginalSize_CheckedChanged(sender,e);
				}
				else
				{
					currRow[ctl.Tag.ToString()] = frmData.GetThis().Clean(ctl.Text,false);
				}

				CheckDataChanged();
			}
		}

		private void txtImage_TextChanged(object sender, EventArgs e)
		{
			try
			{
				if (this.txtImage.Text.Length > 0)
				{
					try
					{
						this.pbPreview.Image = new Bitmap(this.txtImage.Text);
					}
					catch(Exception err)
					{
						string strMessage = "";
						strMessage += err.Message.ToString() + "\n\n";
						strMessage += "Bitmap file cannot be opened!!\n";
						strMessage += this.txtImage.Text + "\n\n";
						strMessage += "Please check the correctness of the specified path and the access rights to the specified location!!!\n";
						MessageBox.Show(this, strMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
						this.pbPreview.Image = null;
						this.txtImage.Text	 = string.Empty;
					}

				}
				else
				{
					this.pbPreview.Image = null;
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show(this,ex.Message,"Error");
			}
		}

		private void panel1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f,panel1, Color.WhiteSmoke, Color.LightGray);		
		}

		private void StatusGroups_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, this, Color.WhiteSmoke, Color.LightGray);		
		}

		private void cbOriginalSize_CheckedChanged(object sender, System.EventArgs e)
		{
			if (this.cbOriginalSize.Checked)
			{
				int x = (int)this.udWidth.Value;
				int y = (int)this.udHeight.Value;	
				this.pbPreview.Size = new Size(x,y);
				this.cbOriginalSize.BackColor = Color.Lime;
				
			}
			else
			{
				this.pbPreview.Size = this.previewSize;
				this.cbOriginalSize.BackColor = Color.Transparent;

			}
		}
	}
}
