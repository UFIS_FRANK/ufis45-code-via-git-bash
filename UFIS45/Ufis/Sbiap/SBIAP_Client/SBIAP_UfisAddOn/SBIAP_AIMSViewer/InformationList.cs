using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Globalization;
using Ufis.Utils;
using Ufis.Data;

namespace AIMSViewer
{
	/// <summary>
	/// Summary description for ViewpointList.
	/// </summary>
	public class StatusInformationList : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label lblEvent;
		private System.ComponentModel.IContainer components;
		private AxTABLib.AxTAB tabEvent;
		private AxTABLib.AxTAB tabList;

		private	ArrayList eventList;
		private	string	  infoType;
		private	AxTABLib._DTABEvents_SendRButtonClickEvent	ev;
		private	AxTABLib.AxTAB								evTable;
		private System.Windows.Forms.Label lblInformation;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.ImageList imageButtons;
		private System.Windows.Forms.ContextMenu contextMenu1;
		private System.Windows.Forms.Button btnClose;

		public StatusInformationList(ArrayList eventList,string infoType)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			this.eventList	= eventList;
			this.infoType	= infoType;

			MenuItem item = new MenuItem("Copy",new EventHandler(ContextMenu_Click));
			this.contextMenu1.MenuItems.Add(item);

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(StatusInformationList));
			this.lblInformation = new System.Windows.Forms.Label();
			this.lblEvent = new System.Windows.Forms.Label();
			this.tabEvent = new AxTABLib.AxTAB();
			this.tabList = new AxTABLib.AxTAB();
			this.btnClose = new System.Windows.Forms.Button();
			this.imageButtons = new System.Windows.Forms.ImageList(this.components);
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.contextMenu1 = new System.Windows.Forms.ContextMenu();
			((System.ComponentModel.ISupportInitialize)(this.tabEvent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabList)).BeginInit();
			this.SuspendLayout();
			// 
			// lblInformation
			// 
			this.lblInformation.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblInformation.Location = new System.Drawing.Point(8, 184);
			this.lblInformation.Name = "lblInformation";
			this.lblInformation.TabIndex = 3;
			this.lblInformation.Text = "Information  :";
			// 
			// lblEvent
			// 
			this.lblEvent.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblEvent.Location = new System.Drawing.Point(8, 8);
			this.lblEvent.Name = "lblEvent";
			this.lblEvent.TabIndex = 1;
			this.lblEvent.Text = "Status event :";
			// 
			// tabEvent
			// 
			this.tabEvent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.tabEvent.Location = new System.Drawing.Point(8, 32);
			this.tabEvent.Name = "tabEvent";
			this.tabEvent.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabEvent.OcxState")));
			this.tabEvent.Size = new System.Drawing.Size(624, 136);
			this.tabEvent.TabIndex = 2;
			this.toolTip1.SetToolTip(this.tabEvent, "Click on entry to get the additional information for this entry below");
			this.tabEvent.SendRButtonClick += new AxTABLib._DTABEvents_SendRButtonClickEventHandler(this.tabEvent_SendRButtonClick);
			this.tabEvent.RowSelectionChanged += new AxTABLib._DTABEvents_RowSelectionChangedEventHandler(this.tabEvent_RowSelectionChanged);
			// 
			// tabList
			// 
			this.tabList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.tabList.Location = new System.Drawing.Point(8, 208);
			this.tabList.Name = "tabList";
			this.tabList.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabList.OcxState")));
			this.tabList.Size = new System.Drawing.Size(624, 120);
			this.tabList.TabIndex = 4;
			this.toolTip1.SetToolTip(this.tabList, "The additional information for the status event above");
			this.tabList.SendRButtonClick += new AxTABLib._DTABEvents_SendRButtonClickEventHandler(this.tabList_SendRButtonClick);
			this.tabList.RowSelectionChanged += new AxTABLib._DTABEvents_RowSelectionChangedEventHandler(this.tabList_RowSelectionChanged);
			// 
			// btnClose
			// 
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnClose.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnClose.ImageIndex = 0;
			this.btnClose.ImageList = this.imageButtons;
			this.btnClose.Location = new System.Drawing.Point(283, 344);
			this.btnClose.Name = "btnClose";
			this.btnClose.TabIndex = 0;
			this.btnClose.Text = " &Close";
			this.toolTip1.SetToolTip(this.btnClose, "Close this window");
			// 
			// imageButtons
			// 
			this.imageButtons.ImageSize = new System.Drawing.Size(19, 19);
			this.imageButtons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageButtons.ImageStream")));
			this.imageButtons.TransparentColor = System.Drawing.Color.White;
			// 
			// StatusInformationList
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(640, 374);
			this.Controls.Add(this.btnClose);
			this.Controls.Add(this.tabList);
			this.Controls.Add(this.tabEvent);
			this.Controls.Add(this.lblEvent);
			this.Controls.Add(this.lblInformation);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "StatusInformationList";
			this.Text = "Status information list";
			this.Load += new System.EventHandler(this.ViewpointList_Load);
			((System.ComponentModel.ISupportInitialize)(this.tabEvent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabList)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void ViewpointList_Load(object sender, System.EventArgs e)
		{
			string strColor = "";
			strColor = UT.colBlue.ToString() + "," + UT.colBlue.ToString();

			tabEvent.ResetContent();
			tabEvent.FontName = "Arial";
			tabEvent.FontSize = 14;
			tabEvent.HeaderString = "Device,Subsystem,Originator,Type,Interface code,Time,Urno";
			tabEvent.HeaderLengthString = "100,100,100,80,80,120,-1";
			tabEvent.ColumnWidthString  = "32,32,32,32,32,14,10";
			tabEvent.LogicalFieldList = "NAME,SUBS,ORIG,TYPE,STAT,LSTU,URNO";
			tabEvent.CursorDecoration(tabEvent.LogicalFieldList, "T,B", "2,2", strColor);
			tabEvent.LineHeight = 18;

			int idxField = UT.GetItemNo(tabEvent.LogicalFieldList, "LSTU");
			if(idxField > -1)
			{
				tabEvent.DateTimeSetColumnFormat(idxField, "YYYYMMDDhhmmss", "hh':'mm':'ss'/'DD");
			}


			IDatabase myDB = UT.GetMemDB();

			ITable	myTable= myDB["ACS"];

			for (int i = 0; i < this.eventList.Count; i++)
			{
				PictureViewer.Symbol eventSymbol = (PictureViewer.Symbol)this.eventList[i];

				IRow[]	myRows = myTable.RowsByIndexValue("URNO",eventSymbol.urno);
				if (myRows != null && myRows.Length == 1)
				{
					string strValues = MakeEmptyString(this.tabEvent.LogicalFieldList, ',');
					this.tabEvent.InsertTextLine(strValues, false);
					int lineNo = this.tabEvent.GetLineCount() - 1;
					string strFld = "";
					for(int j = 0; j < myTable.FieldList.Count; j++)
					{
						strFld = myTable.FieldList[j].ToString();
						this.tabEvent.SetFieldValues(lineNo, strFld, myRows[0][strFld]);
					}	
				}
			}

			if (this.tabEvent.GetLineCount() > 0)
			{
				this.tabEvent.SetCurrentSelection(0);
			}
		}

		/// <summary>
		/// Crates an empty string for tab insertion
		/// </summary>
		/// <param name="strValueList">[separator] delimited string </param>
		/// <param name="separator">separator</param>
		/// <returns></returns>
		private string MakeEmptyString(string strValueList, char separator)
		{
			int i = 0;
			string [] strArr = strValueList.Split(separator);
			string strValues = "";
			for(i = 0; i < strArr.Length-1; i++)
			{
				strValues += separator.ToString();
			}
			return strValues;
		}


		private void tabList_RowSelectionChanged(object sender, AxTABLib._DTABEvents_RowSelectionChangedEvent e)
		{
			int lineNo = this.tabList.GetCurrentSelected();
		
		}

		private void tabEvent_RowSelectionChanged(object sender, AxTABLib._DTABEvents_RowSelectionChangedEvent e)
		{
			int lineNo = this.tabEvent.GetCurrentSelected();
			if (lineNo >= 0)
			{
				PictureViewer.Symbol symbol = (PictureViewer.Symbol)this.eventList[e.lineNo];
				UpdateInformationList(symbol);
			}
		
		}

		private void UpdateInformationList(PictureViewer.Symbol symbol)
		{
			string strColor = "";
			strColor = UT.colBlue.ToString() + "," + UT.colBlue.ToString();

			if (this.infoType == "NextCamera")
			{
				tabList.ResetContent();
				tabList.FontName = "Arial";
				tabList.FontSize = 14;
				tabList.HeaderString = "Next CCTV Camera";
				tabList.LogicalFieldList = "NAME";
				tabList.HeaderLengthString = "500";
				tabEvent.ColumnWidthString  = "255";
				tabList.CursorDecoration(tabList.LogicalFieldList, "T,B", "2,2", strColor);
				tabList.LineHeight = 18;
				/*
				string strValues = MakeEmptyString(this.tabList.LogicalFieldList, ',');
				this.tabList.InsertTextLine(strValues, false);
				int lineNo = this.tabList.GetLineCount() - 1;

				string nextCameraInfo = frmData.GetThis().GetNextCamera(symbol.origin,symbol.floor);
				if (nextCameraInfo.Length > 0)
				{
					this.tabList.SetFieldValues(lineNo,"NAME",nextCameraInfo);
				}
				else
				{
					this.tabList.SetFieldValues(lineNo,"NAME","No camera available for this status event");
				}*/
				ArrayList cameras = frmData.GetThis().GetNextCameras(symbol.origin,symbol.floor);
				if ( cameras.Count > 0 )
				{
					for ( int i=0; i<cameras.Count; i++ )
					{
						string nextCameraInfo = cameras[i].ToString();
						this.tabList.InsertTextLine(nextCameraInfo, false);
					}
				}
				else
				{
					this.tabList.InsertTextLine("No camera available for this status event", false);
				}
			}
			else if (this.infoType == "HandlingAdviceForGroup")
			{
				tabList.ResetContent();
				tabList.FontName = "Arial";
				tabList.FontSize = 14;
				tabList.HeaderString = "Handling Advice for group";
				tabList.LogicalFieldList = "ADVI";
				tabList.HeaderLengthString = "500";
				tabEvent.ColumnWidthString  = "255";
				tabList.CursorDecoration(tabList.LogicalFieldList, "T,B", "2,2", strColor);
				tabList.LineHeight = 18;

				string strValues = MakeEmptyString(this.tabList.LogicalFieldList, ',');
				this.tabList.InsertTextLine(strValues, false);
				int lineNo = this.tabList.GetLineCount() - 1;

				string handlingAdviceInfo = frmData.GetThis().GetHandlingAdviceForGroup(symbol.urno);
				if (handlingAdviceInfo.Length > 0)
				{
					this.tabList.SetFieldValues(lineNo,"ADVI",handlingAdviceInfo);
				}
				else
				{
					this.tabList.SetFieldValues(lineNo,"ADVI","No handling advice available for this status event");
				}

			}
			else if (this.infoType == "HandlingAdviceForStatus")
			{
				tabList.ResetContent();
				tabList.FontName = "Arial";
				tabList.FontSize = 14;
				tabList.HeaderString = "Handling Advice for status";
				tabList.LogicalFieldList = "ADVI";
				tabList.HeaderLengthString = "500";
				tabEvent.ColumnWidthString  = "255";
				tabList.CursorDecoration(tabList.LogicalFieldList, "T,B", "2,2", strColor);
				tabList.LineHeight = 18;

				string strValues = MakeEmptyString(this.tabList.LogicalFieldList, ',');
				this.tabList.InsertTextLine(strValues, false);
				int lineNo = this.tabList.GetLineCount() - 1;

				string handlingAdviceInfo = frmData.GetThis().GetHandlingAdviceForStatus(symbol.urno);
				if (handlingAdviceInfo.Length > 0)
				{
					this.tabList.SetFieldValues(lineNo,"ADVI",handlingAdviceInfo);
				}
				else
				{
					this.tabList.SetFieldValues(lineNo,"ADVI","No handling advice available for this status event");
				}

			}
			this.tabList.Refresh();
		}

		private void tabEvent_SendRButtonClick(object sender, AxTABLib._DTABEvents_SendRButtonClickEvent e)
		{
			Point pos = Control.MousePosition;
			pos = this.PointToClient(pos);
			this.ev = e;
			this.evTable = this.tabEvent;
			this.contextMenu1.Show(this,pos);
			
		}

		private void tabList_SendRButtonClick(object sender, AxTABLib._DTABEvents_SendRButtonClickEvent e)
		{
			if (e.lineNo >= 0)
			{
				Point pos = Control.MousePosition;
				pos = this.PointToClient(pos);
				this.ev = e;
				this.evTable = this.tabList;
				this.contextMenu1.Show(this,pos);
			}
		}

		private void ContextMenu_Click(object sender, System.EventArgs e)
		{
			MenuItem item = (MenuItem)sender;
			switch(item.Index)
			{
				case 0:	// Copy to clipboard
					string result = this.evTable.GetColumnValue(this.ev.lineNo,this.ev.colNo);
					Clipboard.SetDataObject(result,false);
					break;
				default:
					break;
			}
		}

	}
}
