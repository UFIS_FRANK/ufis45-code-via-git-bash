using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace AIMSViewer
{
	/// <summary>
	/// Summary description for StatusDef.
	/// </summary>
	public class StatusDef : System.Windows.Forms.Form
	{
		private AxTABLib.AxTAB axTAB1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public StatusDef()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(StatusDef));
			this.axTAB1 = new AxTABLib.AxTAB();
			((System.ComponentModel.ISupportInitialize)(this.axTAB1)).BeginInit();
			this.SuspendLayout();
			// 
			// axTAB1
			// 
			this.axTAB1.Location = new System.Drawing.Point(48, 72);
			this.axTAB1.Name = "axTAB1";
			this.axTAB1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axTAB1.OcxState")));
			this.axTAB1.Size = new System.Drawing.Size(100, 50);
			this.axTAB1.TabIndex = 0;
			// 
			// StatusDef
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(448, 350);
			this.Controls.Add(this.axTAB1);
			this.Name = "StatusDef";
			this.Text = "Status Definition ...";
			((System.ComponentModel.ISupportInitialize)(this.axTAB1)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion
	}
}
