using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Globalization;
using System.Text;
using Microsoft.Win32;
using Ufis.Utils;
using Ufis.Data;

namespace AIMSViewer
{
	/// <summary>
	/// Summary description for StatusOverview.
	/// </summary>
	public class StatusOverview : System.Windows.Forms.Form
	{
		private System.ComponentModel.IContainer components;
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.Panel panelFill;
		private System.Windows.Forms.Panel panelFillInFill;
		private AxTABLib.AxTAB tabList;
		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.Button btnPrint;
		private System.Windows.Forms.Button btnSearch;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.PictureBox picTop;
		private System.Windows.Forms.Label lblName;
		private System.Windows.Forms.ComboBox comboName;
		private System.Windows.Forms.Label lblStatus;
		private System.Windows.Forms.ComboBox comboStatus;
		private	int	maxLines = 2000;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.CheckBox cbTop;
		private System.Windows.Forms.ContextMenu contextMenuOverview;
		private System.Windows.Forms.Label lblSubsystem;
		private System.Windows.Forms.ComboBox comboSubsystem;
		private System.Windows.Forms.Label lblOriginator;
		private System.Windows.Forms.ComboBox comboOriginator;
		private System.Windows.Forms.Label lblType;
		private System.Windows.Forms.ComboBox comboType;
		private bool[]	sortOrder = null;	

		public StatusOverview()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\ceda.ini");
			string strMaxLines = myIni.IniReadValue("AIMSVIEWER", "MAXLINES");
			try
			{
				this.maxLines = int.Parse(strMaxLines);
			}
			catch(Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(ex.Message);
				maxLines = 2000;
			}

			MenuItem item = new MenuItem("Next camera",new EventHandler(ContextMenu_Click));
			this.contextMenuOverview.MenuItems.Add(item);

			item = new MenuItem("Handling advice for group",new EventHandler(ContextMenu_Click));
			this.contextMenuOverview.MenuItems.Add(item);

			item = new MenuItem("Handling advice for status",new EventHandler(ContextMenu_Click));
			this.contextMenuOverview.MenuItems.Add(item);

			DataExchange.OnACS_Changed +=new AIMSViewer.DataExchange.ACS_Changed(DataExchange_OnACS_Changed);
			DataExchange.OnBeforeViewNameChanged +=new AIMSViewer.DataExchange.BeforeViewNameChanged(DataExchange_OnBeforeViewNameChanged);
			DataExchange.OnViewNameChanged +=new AIMSViewer.DataExchange.ViewNameChanged(DataExchange_OnViewNameChanged);
			DataExchange.OnApplication_Shutdown	+=new AIMSViewer.DataExchange.Application_Shutdown(DataExchange_OnApplication_Shutdown);
			DataExchange.OnSymbolSelection_Changed +=new AIMSViewer.DataExchange.SymbolSelection_Changed(DataExchange_OnSymbolSelection_Changed);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(StatusOverview));
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.panelFill = new System.Windows.Forms.Panel();
			this.panelFillInFill = new System.Windows.Forms.Panel();
			this.tabList = new AxTABLib.AxTAB();
			this.panelTop = new System.Windows.Forms.Panel();
			this.lblType = new System.Windows.Forms.Label();
			this.comboType = new System.Windows.Forms.ComboBox();
			this.lblOriginator = new System.Windows.Forms.Label();
			this.comboOriginator = new System.Windows.Forms.ComboBox();
			this.comboSubsystem = new System.Windows.Forms.ComboBox();
			this.lblSubsystem = new System.Windows.Forms.Label();
			this.cbTop = new System.Windows.Forms.CheckBox();
			this.lblStatus = new System.Windows.Forms.Label();
			this.comboStatus = new System.Windows.Forms.ComboBox();
			this.lblName = new System.Windows.Forms.Label();
			this.btnPrint = new System.Windows.Forms.Button();
			this.btnSearch = new System.Windows.Forms.Button();
			this.btnClose = new System.Windows.Forms.Button();
			this.comboName = new System.Windows.Forms.ComboBox();
			this.picTop = new System.Windows.Forms.PictureBox();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.contextMenuOverview = new System.Windows.Forms.ContextMenu();
			this.panelFill.SuspendLayout();
			this.panelFillInFill.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabList)).BeginInit();
			this.panelTop.SuspendLayout();
			this.SuspendLayout();
			// 
			// imageList1
			// 
			this.imageList1.ImageSize = new System.Drawing.Size(19, 19);
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.White;
			// 
			// panelFill
			// 
			this.panelFill.Controls.Add(this.panelFillInFill);
			this.panelFill.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelFill.Location = new System.Drawing.Point(0, 64);
			this.panelFill.Name = "panelFill";
			this.panelFill.Size = new System.Drawing.Size(992, 638);
			this.panelFill.TabIndex = 5;
			// 
			// panelFillInFill
			// 
			this.panelFillInFill.Controls.Add(this.tabList);
			this.panelFillInFill.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelFillInFill.Location = new System.Drawing.Point(0, 0);
			this.panelFillInFill.Name = "panelFillInFill";
			this.panelFillInFill.Size = new System.Drawing.Size(992, 638);
			this.panelFillInFill.TabIndex = 3;
			// 
			// tabList
			// 
			this.tabList.ContainingControl = this;
			this.tabList.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabList.Location = new System.Drawing.Point(0, 0);
			this.tabList.Name = "tabList";
			this.tabList.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabList.OcxState")));
			this.tabList.Size = new System.Drawing.Size(992, 638);
			this.tabList.TabIndex = 4;
			this.tabList.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabList_SendLButtonDblClick);
			this.tabList.SendRButtonClick += new AxTABLib._DTABEvents_SendRButtonClickEventHandler(this.tabList_SendRButtonClick);
			// 
			// panelTop
			// 
			this.panelTop.Controls.Add(this.lblType);
			this.panelTop.Controls.Add(this.comboType);
			this.panelTop.Controls.Add(this.lblOriginator);
			this.panelTop.Controls.Add(this.comboOriginator);
			this.panelTop.Controls.Add(this.comboSubsystem);
			this.panelTop.Controls.Add(this.lblSubsystem);
			this.panelTop.Controls.Add(this.cbTop);
			this.panelTop.Controls.Add(this.lblStatus);
			this.panelTop.Controls.Add(this.comboStatus);
			this.panelTop.Controls.Add(this.lblName);
			this.panelTop.Controls.Add(this.btnPrint);
			this.panelTop.Controls.Add(this.btnSearch);
			this.panelTop.Controls.Add(this.btnClose);
			this.panelTop.Controls.Add(this.comboName);
			this.panelTop.Controls.Add(this.picTop);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(0, 0);
			this.panelTop.Name = "panelTop";
			this.panelTop.Size = new System.Drawing.Size(992, 64);
			this.panelTop.TabIndex = 4;
			// 
			// lblType
			// 
			this.lblType.BackColor = System.Drawing.Color.Transparent;
			this.lblType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblType.Location = new System.Drawing.Point(76, 32);
			this.lblType.Name = "lblType";
			this.lblType.Size = new System.Drawing.Size(64, 16);
			this.lblType.TabIndex = 17;
			this.lblType.Text = "Type:";
			// 
			// comboType
			// 
			this.comboType.Location = new System.Drawing.Point(152, 32);
			this.comboType.Name = "comboType";
			this.comboType.Size = new System.Drawing.Size(136, 22);
			this.comboType.Sorted = true;
			this.comboType.TabIndex = 16;
			this.toolTip1.SetToolTip(this.comboType, "Key in a type name to search for or select an existing entry");
			this.comboType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboType_KeyPress);
			this.comboType.TextChanged += new System.EventHandler(this.comboType_TextChanged);
			// 
			// lblOriginator
			// 
			this.lblOriginator.BackColor = System.Drawing.Color.Transparent;
			this.lblOriginator.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblOriginator.Location = new System.Drawing.Point(536, 8);
			this.lblOriginator.Name = "lblOriginator";
			this.lblOriginator.Size = new System.Drawing.Size(64, 16);
			this.lblOriginator.TabIndex = 15;
			this.lblOriginator.Text = "Originator:";
			this.lblOriginator.Click += new System.EventHandler(this.lblOriginator_Click);
			// 
			// comboOriginator
			// 
			this.comboOriginator.Location = new System.Drawing.Point(624, 4);
			this.comboOriginator.Name = "comboOriginator";
			this.comboOriginator.Size = new System.Drawing.Size(136, 22);
			this.comboOriginator.Sorted = true;
			this.comboOriginator.TabIndex = 14;
			this.toolTip1.SetToolTip(this.comboOriginator, "Key in an originator name to search for or select an existing entry");
			this.comboOriginator.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboOriginator_KeyPress);
			this.comboOriginator.TextChanged += new System.EventHandler(this.comboOriginator_TextChanged);
			// 
			// comboSubsystem
			// 
			this.comboSubsystem.Location = new System.Drawing.Point(384, 4);
			this.comboSubsystem.Name = "comboSubsystem";
			this.comboSubsystem.Size = new System.Drawing.Size(136, 22);
			this.comboSubsystem.Sorted = true;
			this.comboSubsystem.TabIndex = 13;
			this.toolTip1.SetToolTip(this.comboSubsystem, "Key in a subsystem name to search for or select an existing entry");
			this.comboSubsystem.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboSubsystem_KeyPress);
			this.comboSubsystem.TextChanged += new System.EventHandler(this.comboSubsystem_TextChanged);
			// 
			// lblSubsystem
			// 
			this.lblSubsystem.BackColor = System.Drawing.Color.Transparent;
			this.lblSubsystem.Location = new System.Drawing.Point(296, 8);
			this.lblSubsystem.Name = "lblSubsystem";
			this.lblSubsystem.Size = new System.Drawing.Size(72, 23);
			this.lblSubsystem.TabIndex = 12;
			this.lblSubsystem.Text = "Subsystem:";
			// 
			// cbTop
			// 
			this.cbTop.Appearance = System.Windows.Forms.Appearance.Button;
			this.cbTop.BackColor = System.Drawing.Color.Transparent;
			this.cbTop.Dock = System.Windows.Forms.DockStyle.Right;
			this.cbTop.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.cbTop.ImageIndex = 4;
			this.cbTop.ImageList = this.imageList1;
			this.cbTop.Location = new System.Drawing.Point(770, 0);
			this.cbTop.Name = "cbTop";
			this.cbTop.Size = new System.Drawing.Size(72, 64);
			this.cbTop.TabIndex = 6;
			this.cbTop.Text = " &Top";
			this.cbTop.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.toolTip1.SetToolTip(this.cbTop, "Set this window the top most one");
			this.cbTop.CheckedChanged += new System.EventHandler(this.cbTop_CheckedChanged);
			// 
			// lblStatus
			// 
			this.lblStatus.BackColor = System.Drawing.Color.Transparent;
			this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblStatus.Location = new System.Drawing.Point(296, 40);
			this.lblStatus.Name = "lblStatus";
			this.lblStatus.Size = new System.Drawing.Size(80, 12);
			this.lblStatus.TabIndex = 10;
			this.lblStatus.Text = "Interface code :";
			// 
			// comboStatus
			// 
			this.comboStatus.Location = new System.Drawing.Point(384, 32);
			this.comboStatus.Name = "comboStatus";
			this.comboStatus.Size = new System.Drawing.Size(136, 22);
			this.comboStatus.Sorted = true;
			this.comboStatus.TabIndex = 3;
			this.toolTip1.SetToolTip(this.comboStatus, "Key in an interface code to search for or select and existing one");
			this.comboStatus.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboName_KeyPress);
			this.comboStatus.TextChanged += new System.EventHandler(this.comboStatus_TextChanged);
			// 
			// lblName
			// 
			this.lblName.BackColor = System.Drawing.Color.Transparent;
			this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblName.Location = new System.Drawing.Point(76, 8);
			this.lblName.Name = "lblName";
			this.lblName.Size = new System.Drawing.Size(44, 12);
			this.lblName.TabIndex = 8;
			this.lblName.Text = "Device:";
			// 
			// btnPrint
			// 
			this.btnPrint.BackColor = System.Drawing.Color.Transparent;
			this.btnPrint.Dock = System.Windows.Forms.DockStyle.Right;
			this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnPrint.ImageIndex = 3;
			this.btnPrint.ImageList = this.imageList1;
			this.btnPrint.Location = new System.Drawing.Point(842, 0);
			this.btnPrint.Name = "btnPrint";
			this.btnPrint.Size = new System.Drawing.Size(75, 64);
			this.btnPrint.TabIndex = 7;
			this.btnPrint.TabStop = false;
			this.btnPrint.Text = "   &Print";
			this.toolTip1.SetToolTip(this.btnPrint, "Print this status overview");
			this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
			// 
			// btnSearch
			// 
			this.btnSearch.BackColor = System.Drawing.Color.Transparent;
			this.btnSearch.Dock = System.Windows.Forms.DockStyle.Left;
			this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnSearch.ImageIndex = 0;
			this.btnSearch.ImageList = this.imageList1;
			this.btnSearch.Location = new System.Drawing.Point(0, 0);
			this.btnSearch.Name = "btnSearch";
			this.btnSearch.Size = new System.Drawing.Size(75, 64);
			this.btnSearch.TabIndex = 5;
			this.btnSearch.TabStop = false;
			this.btnSearch.Text = "   &Search";
			this.toolTip1.SetToolTip(this.btnSearch, "Search for the selected device or status");
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			// 
			// btnClose
			// 
			this.btnClose.BackColor = System.Drawing.Color.Transparent;
			this.btnClose.Dock = System.Windows.Forms.DockStyle.Right;
			this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnClose.ImageIndex = 1;
			this.btnClose.ImageList = this.imageList1;
			this.btnClose.Location = new System.Drawing.Point(917, 0);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(75, 64);
			this.btnClose.TabIndex = 8;
			this.btnClose.TabStop = false;
			this.btnClose.Text = "   &Close";
			this.toolTip1.SetToolTip(this.btnClose, "Close this window");
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// comboName
			// 
			this.comboName.Location = new System.Drawing.Point(152, 4);
			this.comboName.Name = "comboName";
			this.comboName.Size = new System.Drawing.Size(136, 22);
			this.comboName.Sorted = true;
			this.comboName.TabIndex = 1;
			this.toolTip1.SetToolTip(this.comboName, "Key in a device name to search for or select an existing entry");
			this.comboName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboName_KeyPress);
			this.comboName.TextChanged += new System.EventHandler(this.comboName_TextChanged);
			// 
			// picTop
			// 
			this.picTop.Dock = System.Windows.Forms.DockStyle.Fill;
			this.picTop.Location = new System.Drawing.Point(0, 0);
			this.picTop.Name = "picTop";
			this.picTop.Size = new System.Drawing.Size(992, 64);
			this.picTop.TabIndex = 0;
			this.picTop.TabStop = false;
			this.picTop.Paint += new System.Windows.Forms.PaintEventHandler(this.picTop_Paint);
			// 
			// StatusOverview
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 13);
			this.ClientSize = new System.Drawing.Size(992, 702);
			this.Controls.Add(this.panelFill);
			this.Controls.Add(this.panelTop);
			this.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "StatusOverview";
			this.Text = "Status Overview ...";
			this.Resize += new System.EventHandler(this.StatusOverview_Resize);
			this.Closing += new System.ComponentModel.CancelEventHandler(this.StatusOverview_Closing);
			this.Load += new System.EventHandler(this.StatusOverview_Load);
			this.MaximumSizeChanged += new System.EventHandler(this.StatusOverview_MaximumSizeChanged);
			this.LocationChanged += new System.EventHandler(this.StatusOverview_LocationChanged);
			this.panelFill.ResumeLayout(false);
			this.panelFillInFill.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabList)).EndInit();
			this.panelTop.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void StatusOverview_Load(object sender, System.EventArgs e)
		{
			btnPrint.Parent			= picTop;
			btnClose.Parent			= picTop;
			btnSearch.Parent		= picTop;
			lblName.Parent			= picTop;
			lblStatus.Parent		= picTop;
			lblSubsystem.Parent		= picTop;
			lblOriginator.Parent	= picTop;
			lblType.Parent			= picTop;
			cbTop.Parent			= picTop;
		
			int lineColor = UT.colRed;
			int textColor = UT.colBlack;


			string strColor = "";
			strColor = UT.colBlue.ToString() + "," + UT.colBlue.ToString();
			tabList.ResetContent();
			tabList.FontName = "Arial";
			tabList.FontSize = 14;
			tabList.HeaderString = "Device,Subsystem,Originator,Type,Interface code,Time,Urno";
			tabList.HeaderLengthString = "122,122,122,122,122,100,-1";
			tabList.ColumnWidthString  = "32,32,32,32,32,14,10";
			tabList.LogicalFieldList = "NAME,SUBS,ORIG,TYPE,STAT,LSTU,URNO";
			tabList.CursorDecoration(tabList.LogicalFieldList, "T,B", "2,2", strColor);
			tabList.DefaultCursor = false;
			tabList.LineHeight = 18;

			ReadRegistry();
			
			int idxField = UT.GetItemNo(tabList.LogicalFieldList, "LSTU");
			if(idxField > -1)
			{
				tabList.DateTimeSetColumnFormat(idxField, "YYYYMMDDhhmmss", "hh':'mm':'ss'/'DD");
			}

			this.sortOrder = new bool[7];
			for (int i = 0; i < this.sortOrder.Length; i++)
			{
				sortOrder[i] = false;
			}

			IDatabase myDB = UT.GetMemDB();
			ITable	myTable= myDB["ACS"];
			for(int i = 0; i < myTable.Count; i++)
			{
				IRow asgRow = frmData.GetThis().GetStatusGroupFromStatusEvent(myTable[i]);
				if (asgRow == null)
					continue;

				// additional security enhancements
				string prv = "Subsystem" + asgRow["SUBS"];
				if (frmData.GetPrivileges(prv) != "1")
				{
					continue;
				}

				if (ViewFilter.IsPassFilter(myTable[i],asgRow) && frmData.GetThis().IsEventStateToBeDisplayed(asgRow["URNO"],myTable[i]))
				{
					if (tabList.GetLineCount() == this.maxLines)
					{
						tabList.DeleteLine(tabList.GetLineCount()-1);
					}

					tabList.InsertTextLineAt(0,myTable[i].FieldValues(tabList.LogicalFieldList),false);
					if (!frmData.GetThis().HasEventBeenSeen(myTable[i]["URNO"]))
					{
						tabList.SetLineColor(0, textColor, lineColor);
					}

					UpdateComboBoxes(myTable[i]);

				}
			}

			tabList.IndexCreate("URNO",UT.GetItemNo(tabList.LogicalFieldList, "URNO"));
			tabList.Refresh();
			this.Text = "Status overview" + " --- Active View: [" + ViewFilter.CurrentViewName + "]";
		}

		private void UpdateComboBoxes(IRow acsRow)
		{
			if (!this.comboSubsystem.Items.Contains(acsRow["SUBS"]))
			{
				this.comboSubsystem.Items.Add(acsRow["SUBS"]);
			}

			if (!this.comboName.Items.Contains(acsRow["NAME"]))
			{
				this.comboName.Items.Add(acsRow["NAME"]);
			}

			if (!this.comboOriginator.Items.Contains(acsRow["ORIG"]))
			{
				this.comboOriginator.Items.Add(acsRow["ORIG"]);
			}

			if (!this.comboType.Items.Contains(acsRow["TYPE"]))
			{
				this.comboType.Items.Add(acsRow["TYPE"]);
			}

			if (!this.comboStatus.Items.Contains(acsRow["STAT"]))
			{
				this.comboStatus.Items.Add(acsRow["STAT"]);
			}
		}

		private void ResetComboBoxes()
		{
			this.comboName.Items.Clear();
			this.comboSubsystem.Items.Clear();
			this.comboOriginator.Items.Clear();
			this.comboType.Items.Clear();
			this.comboStatus.Items.Clear();
		}

		private void DataExchange_OnACS_Changed(object sender, ArrayList updateList)
		{
			IDatabase myDB = UT.GetMemDB();
			ITable	myTable= myDB["ACS"];

			foreach(DatabaseTableEventArgs eventArgs in updateList)
			{
				bool reorgIndexes = false;
				int idxField = UT.GetItemNo(tabList.LogicalFieldList, "URNO");

				if (myTable.Command("insert").IndexOf(","+eventArgs.command+",") >= 0)
				{
					IRow asgRow = frmData.GetThis().GetStatusGroupFromStatusEvent(eventArgs.row);
					if (asgRow == null)
						continue;

					// additional security enhancements
					string prv = "Subsystem" + asgRow["SUBS"];
					if (frmData.GetPrivileges(prv) != "1")
					{
						continue;
					}

					if (ViewFilter.IsPassFilter(eventArgs.row,asgRow) && frmData.GetThis().IsEventStateToBeDisplayed(asgRow["URNO"],eventArgs.row))
					{
						if (tabList.GetLineCount() == this.maxLines)
						{
							tabList.DeleteLine(tabList.GetLineCount()-1);
						}

						tabList.InsertTextLineAt(0,eventArgs.row.FieldValues(this.tabList.LogicalFieldList),false);
						if (!frmData.GetThis().HasEventBeenSeen(eventArgs.row["URNO"]))
						{
							tabList.SetLineColor(0, UT.colBlack, UT.colRed);
						}

						UpdateComboBoxes(eventArgs.row);

						reorgIndexes = true;
					}
				}
				else if (myTable.Command("update").IndexOf(","+eventArgs.command+",") >= 0)
				{
					string lines = tabList.GetLinesByIndexValue("URNO",eventArgs.row["URNO"],0);
					int lineNo = -1;
					if (lines.Length > 0)
					{
						lineNo = int.Parse(lines);	
					}

					IRow asgRow = frmData.GetThis().GetStatusGroupFromStatusEvent(eventArgs.row);
					if (asgRow == null)
						continue;

					// additional security enhancements
					string prv = "Subsystem" + asgRow["SUBS"];
					if (frmData.GetPrivileges(prv) != "1")
					{
						continue;
					}

					if (ViewFilter.IsPassFilter(eventArgs.row,asgRow) && frmData.GetThis().IsEventStateToBeDisplayed(asgRow["URNO"],eventArgs.row))
					{
						if (lineNo < 0)
						{
							if (tabList.GetLineCount() == this.maxLines)
							{
								tabList.DeleteLine(tabList.GetLineCount()-1);
							}

							tabList.InsertTextLineAt(0,eventArgs.row.FieldValues(this.tabList.LogicalFieldList),false);
							if (!frmData.GetThis().HasEventBeenSeen(eventArgs.row["URNO"]))
							{
								tabList.SetLineColor(0, UT.colBlack, UT.colRed);
							}

							UpdateComboBoxes(eventArgs.row);
							reorgIndexes = true;
						}
						else
						{
							tabList.SetFieldValues(lineNo,this.tabList.LogicalFieldList,eventArgs.row.FieldValues(this.tabList.LogicalFieldList));
						}
					}
					else
					{
						if (lineNo >= 0)
						{
							this.tabList.DeleteLine(lineNo);
							reorgIndexes = true;
						}
					}
				}
				else if (myTable.Command("delete").IndexOf(","+eventArgs.command+",") >= 0)
				{
					string lines = tabList.GetLinesByIndexValue("URNO",eventArgs.row["URNO"],0);
					if (lines.Length > 0)
					{
						int lineNo = int.Parse(lines);	
						tabList.DeleteLine(lineNo);
						reorgIndexes = true;
					}
				}

				if (reorgIndexes)
					this.tabList.ReorgIndexes();
			}

			Refresh();
		}

		private void StatusOverview_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			DataExchange.OnACS_Changed			-=new AIMSViewer.DataExchange.ACS_Changed(DataExchange_OnACS_Changed);
			DataExchange.OnViewNameChanged		-=new AIMSViewer.DataExchange.ViewNameChanged(DataExchange_OnViewNameChanged);
			DataExchange.OnApplication_Shutdown	-=new AIMSViewer.DataExchange.Application_Shutdown(DataExchange_OnApplication_Shutdown);
			DataExchange.OnSymbolSelection_Changed -=new AIMSViewer.DataExchange.SymbolSelection_Changed(DataExchange_OnSymbolSelection_Changed);

			this.WriteRegistry();
		}

		private void tabList_SendLButtonClick(object sender, AxTABLib._DTABEvents_SendLButtonClickEvent e)
		{
		
		}

		private void DataExchange_OnViewNameChanged()
		{
			IDatabase myDB = UT.GetMemDB();
			ITable	myTable= myDB["ACS"];

			tabList.ResetContent();
			ResetComboBoxes();
			for(int i = 0; i < myTable.Count; i++)
			{
				IRow asgRow = frmData.GetThis().GetStatusGroupFromStatusEvent(myTable[i]);
				if (asgRow == null)
					continue;

				// additional security enhancements
				string prv = "Subsystem" + asgRow["SUBS"];
				if (frmData.GetPrivileges(prv) != "1")
				{
					continue;
				}

				if (ViewFilter.IsPassFilter(myTable[i],asgRow)&& frmData.GetThis().IsEventStateToBeDisplayed(asgRow["URNO"],myTable[i]))
				{
					if (tabList.GetLineCount() == this.maxLines)
					{
						tabList.DeleteLine(tabList.GetLineCount()-1);
					}

					tabList.InsertTextLineAt(0,myTable[i].FieldValues(tabList.LogicalFieldList),false);
					if (!frmData.GetThis().HasEventBeenSeen(myTable[i]["URNO"]))
					{
						tabList.SetLineColor(0, UT.colBlack, UT.colRed);
					}

					UpdateComboBoxes(myTable[i]);
				}
			}

			tabList.Refresh();
			this.Text = "Status overview" + " --- Active View: [" + ViewFilter.CurrentViewName + "]";
		}

		private void tabList_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if (e.lineNo >= 0)
			{
				string strACSUrno = this.tabList.GetFieldValue(e.lineNo,"URNO");
				IDatabase myDB = UT.GetMemDB();
				ITable	myTable= myDB["ACS"];
				IRow[]	myRows = myTable.RowsByIndexValue("URNO",strACSUrno);
				PointF	origin = PointF.Empty;
				string		floor  = "-99";
	
				if (myRows != null && myRows.Length == 1)
				{
					if (!frmData.GetThis().HasEventBeenSeen(myRows[0]["URNO"]))
					{
						frmData.GetThis().EventHasBeenSeen(myRows[0]["URNO"],true);
						tabList.SetLineColor(e.lineNo, UT.colBlack, UT.colWhite);
					}

					origin.X = float.Parse(myRows[0]["XCOD"],NumberStyles.Number,frmData.NFI);
					origin.Y = float.Parse(myRows[0]["YCOD"],NumberStyles.Number,frmData.NFI);
					floor	 = myRows[0]["FLOR"];

					ArrayList viewpoints = frmData.GetThis().GetViewpointDefinitions(origin,floor);
					if (viewpoints.Count == 1)
					{
						/*Viewpoint dlg = new Viewpoint();
						dlg.Owner = this;
						dlg.Urno = (string)viewpoints[0];*/
						Viewpoint dlg ;
						dlg = Viewpoint.ShowViewPoint (this, (string)viewpoints[0]);

						PictureViewer.Symbol symbol = new PictureViewer.Symbol();
						if (frmData.GetThis().GetSymbolFromUrno(strACSUrno,ref symbol))
						{
							dlg.SelectSymbol(symbol);
						}

						dlg.Show();
					}
					else if (viewpoints.Count > 1)
					{
						PictureViewer.Symbol symbol = new PictureViewer.Symbol();
						if (frmData.GetThis().GetSymbolFromUrno(strACSUrno,ref symbol))
						{
							ViewpointList dlg = new ViewpointList(symbol,viewpoints);
							dlg.Owner = this;
							dlg.ShowDialog(this);
						}
					}
					else
					{
						MessageBox.Show(this,"No view point available for the specified status event","Information");
					}
				}
			}
			else	// header sorting
			{
				this.sortOrder[e.colNo] = !this.sortOrder[e.colNo];
				tabList.Sort(e.colNo.ToString(), this.sortOrder[e.colNo], true);
				tabList.Refresh();	
			}
		}

		private void StatusOverview_Resize(object sender, System.EventArgs e)
		{
			int len = this.Size.Width / 6;
			this.tabList.HeaderLengthString = string.Format("{0},{0},{0},{0},{0},{0}",len);
			this.tabList.RedrawTab();
		}

		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			Search();
		}

		private void btnPrint_Click(object sender, System.EventArgs e)
		{
			this.Cursor = Cursors.WaitCursor;
			rptStatusOverview rpt = new rptStatusOverview(tabList,ViewFilter.CurrentViewName);
			frmPreview pt = new frmPreview(rpt);
			pt.Show();
			this.Cursor = Cursors.Arrow;
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			this.Close();		
		}

		private void picTop_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, picTop, Color.WhiteSmoke , Color.DarkGray  );
		}

		private void Search()
		{
			string strSearchName		= comboName.Text;
			string strSearchStatus		= comboStatus.Text;
			string strSearchSubsystem	= comboSubsystem.Text;
			string strSearchOriginator	= comboOriginator.Text;
			string strSearchType		= comboType.Text;	
	
			if (strSearchName == "" && strSearchStatus == "" && strSearchSubsystem == "" && strSearchOriginator == "" && strSearchType == "")
			{
				MessageBox.Show(this, "No search text entered\nNothing to do.", "Annotation", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			else
			{
				if (strSearchName != "")
				{
					if(comboName.FindStringExact(strSearchName, -1) == -1)
					{
						comboName.Items.Add(strSearchName);
					}
				}

				if (strSearchSubsystem != "")
				{
					if(comboSubsystem.FindStringExact(strSearchSubsystem, -1) == -1)
					{
						comboSubsystem.Items.Add(strSearchSubsystem);
					}
				}

				if (strSearchOriginator != "")
				{
					if(comboOriginator.FindStringExact(strSearchOriginator, -1) == -1)
					{
						comboOriginator.Items.Add(strSearchOriginator);
					}
				}

				if (strSearchType != "")
				{
					if(comboType.FindStringExact(strSearchType, -1) == -1)
					{
						comboType.Items.Add(strSearchType);
					}
				}

				if (strSearchStatus != "")
				{
					if(comboStatus.FindStringExact(strSearchStatus, -1) == -1)
					{
						comboStatus.Items.Add(strSearchStatus);
					}
				}

			}

#if	OldSearch
			//Now start the ultimate search
			bool blFound = false;
			int LineNo = -1;
			if (strSearchName != "")
			{
				int cnt = tabList.GetLineCount();
				int curSel = tabList.GetCurrentSelected();
				int selStart = curSel;
				int idxCol = UT.GetItemNo(tabList.LogicalFieldList, "NAME");
				if(blFound == false && curSel < cnt)
				{
					string strRet = tabList.GetNextLineByColumnValue(idxCol, strSearchName, 2);
					if (strRet != "")
					{
						LineNo = Convert.ToInt32( strRet );
						blFound = true;
					}
					else
					{
						curSel++;
						tabList.SetCurrentSelection(curSel);
					}
				}

				if (blFound == false && selStart > 0)
				{
					curSel = -1;
					tabList.SetCurrentSelection(curSel);
					if(blFound == false && curSel <= selStart)
					{
						string strRet = tabList.GetNextLineByColumnValue(idxCol, strSearchName, 2);
						if(strRet != "")
						{
							LineNo = Convert.ToInt32( strRet );
							blFound = true;
						}
						else
						{
							curSel++;
							tabList.SetCurrentSelection(curSel);
						}
					}
				}

				if (blFound == true)
				{
					tabList.SetCurrentSelection(LineNo);
					tabList.OnVScrollTo(LineNo);
				}
			}

			if (strSearchSubsystem != "")
			{
				//First seach for gate
				int cnt = tabList.GetLineCount();
				int curSel = tabList.GetCurrentSelected();
				int selStart = curSel;
				int idxCol = UT.GetItemNo(tabList.LogicalFieldList, "SUBS");
				if (blFound == false && curSel < cnt)
				{
					string strRet = tabList.GetNextLineByColumnValue(idxCol, strSearchSubsystem, 2);
					if(strRet != "")
					{
						LineNo = Convert.ToInt32( strRet );
						blFound = true;
					}
					else
					{
						curSel++;
						tabList.SetCurrentSelection(curSel);
					}
				}
				if(blFound == false && selStart > 0)
				{
					curSel = 0;
					tabList.SetCurrentSelection(curSel);
					if(blFound == false && curSel <= selStart)
					{
						string strRet = tabList.GetNextLineByColumnValue(idxCol, strSearchSubsystem, 2);
						if(strRet != "")
						{
							LineNo = Convert.ToInt32( strRet );
							blFound = true;
						}
						else
						{
							curSel++;
							tabList.SetCurrentSelection(curSel);
						}
					}
				}
				if(blFound == true)
				{
					tabList.SetCurrentSelection(LineNo);
					tabList.OnVScrollTo(LineNo);
				}
			}

			if (strSearchOriginator != "")
			{
				//First seach for gate
				int cnt = tabList.GetLineCount();
				int curSel = tabList.GetCurrentSelected();
				int selStart = curSel;
				int idxCol = UT.GetItemNo(tabList.LogicalFieldList, "ORIG");
				if (blFound == false && curSel < cnt)
				{
					string strRet = tabList.GetNextLineByColumnValue(idxCol, strSearchOriginator, 2);
					if(strRet != "")
					{
						LineNo = Convert.ToInt32( strRet );
						blFound = true;
					}
					else
					{
						curSel++;
						tabList.SetCurrentSelection(curSel);
					}
				}
				if(blFound == false && selStart > 0)
				{
					curSel = 0;
					tabList.SetCurrentSelection(curSel);
					if(blFound == false && curSel <= selStart)
					{
						string strRet = tabList.GetNextLineByColumnValue(idxCol, strSearchOriginator, 2);
						if(strRet != "")
						{
							LineNo = Convert.ToInt32( strRet );
							blFound = true;
						}
						else
						{
							curSel++;
							tabList.SetCurrentSelection(curSel);
						}
					}
				}
				if(blFound == true)
				{
					tabList.SetCurrentSelection(LineNo);
					tabList.OnVScrollTo(LineNo);
				}
			}

			if (strSearchType != "")
			{
				//First seach for gate
				int cnt = tabList.GetLineCount();
				int curSel = tabList.GetCurrentSelected();
				int selStart = curSel;
				int idxCol = UT.GetItemNo(tabList.LogicalFieldList, "TYPE");
				if (blFound == false && curSel < cnt)
				{
					string strRet = tabList.GetNextLineByColumnValue(idxCol, strSearchType, 2);
					if(strRet != "")
					{
						LineNo = Convert.ToInt32( strRet );
						blFound = true;
					}
					else
					{
						curSel++;
						tabList.SetCurrentSelection(curSel);
					}
				}
				if(blFound == false && selStart > 0)
				{
					curSel = 0;
					tabList.SetCurrentSelection(curSel);
					if(blFound == false && curSel <= selStart)
					{
						string strRet = tabList.GetNextLineByColumnValue(idxCol, strSearchType, 2);
						if(strRet != "")
						{
							LineNo = Convert.ToInt32( strRet );
							blFound = true;
						}
						else
						{
							curSel++;
							tabList.SetCurrentSelection(curSel);
						}
					}
				}
				if(blFound == true)
				{
					tabList.SetCurrentSelection(LineNo);
					tabList.OnVScrollTo(LineNo);
				}
			}

			if (strSearchStatus != "")
			{
				//First seach for gate
				int cnt = tabList.GetLineCount();
				int curSel = tabList.GetCurrentSelected();
				int selStart = curSel;
				int idxCol = UT.GetItemNo(tabList.LogicalFieldList, "STAT");
				if (blFound == false && curSel < cnt)
				{
					string strRet = tabList.GetNextLineByColumnValue(idxCol, strSearchStatus, 2);
					if(strRet != "")
					{
						LineNo = Convert.ToInt32( strRet );
						blFound = true;
					}
					else
					{
						curSel++;
						tabList.SetCurrentSelection(curSel);
					}
				}
				if(blFound == false && selStart > 0)
				{
					curSel = 0;
					tabList.SetCurrentSelection(curSel);
					if(blFound == false && curSel <= selStart)
					{
						string strRet = tabList.GetNextLineByColumnValue(idxCol, strSearchStatus, 2);
						if(strRet != "")
						{
							LineNo = Convert.ToInt32( strRet );
							blFound = true;
						}
						else
						{
							curSel++;
							tabList.SetCurrentSelection(curSel);
						}
					}
				}
				if(blFound == true)
				{
					tabList.SetCurrentSelection(LineNo);
					tabList.OnVScrollTo(LineNo);
				}
			}

			tabList.Refresh();
#else
			bool found = false;
			int cnt = tabList.GetLineCount();
			int curSel = tabList.GetCurrentSelected();
			++curSel;

			int lineNo = 0;
			int idxColName		= UT.GetItemNo(tabList.LogicalFieldList, "NAME");
			int idxColSubsystem	= UT.GetItemNo(tabList.LogicalFieldList, "SUBS");
			int idxColOriginator= UT.GetItemNo(tabList.LogicalFieldList, "ORIG");
			int idxColType		= UT.GetItemNo(tabList.LogicalFieldList, "TYPE");
			int idxColStatus	= UT.GetItemNo(tabList.LogicalFieldList, "STAT");

			again:
			for (lineNo = curSel; lineNo < cnt; lineNo++)
			{
				if (strSearchName != "")
				{
					if (tabList.GetColumnValue(lineNo,idxColName) != strSearchName)
						continue;
				}

				if (strSearchSubsystem != "")
				{
					if (tabList.GetColumnValue(lineNo,idxColSubsystem) != strSearchSubsystem)
						continue;
				}

				if (strSearchOriginator != "")
				{
					if (tabList.GetColumnValue(lineNo,idxColOriginator) != strSearchOriginator)
						continue;
				}

				if (strSearchType != "")
				{
					if (tabList.GetColumnValue(lineNo,idxColType) != strSearchType)
						continue;
				}

				if (strSearchStatus != "")
				{
					if (tabList.GetColumnValue(lineNo,idxColStatus) != strSearchStatus)
						continue;
				}

				found = true;
				break;

			}


			if (found == true)
			{
				tabList.SetCurrentSelection(lineNo);
				tabList.OnVScrollTo(lineNo);
			}
			else
			{
				if (MessageBox.Show(this,"Reaching end of list!\nRestart from top of list?","Question",MessageBoxButtons.YesNo) == DialogResult.Yes)
				{
					curSel = 0;
					goto again;
				}
			}
			tabList.Refresh();					
#endif
		}

		private void comboName_TextChanged(object sender, System.EventArgs e)
		{
			if (comboName.Text != "")
			{
				Search();
			}
		
		}

		private void comboStatus_TextChanged(object sender, System.EventArgs e)
		{
			if (comboStatus.Text != "")
			{
				Search();
			}
		
		}

		private void ReadRegistry()
		{
			int myX=-1, myY=-1, myW=-1, myH=-1;
			string regVal = "";
			string theKey = "Software\\AIMSViewer\\" + ViewFilter.CurrentViewName + "\\StatusOverview";
			RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			if(rk != null)
			{
				regVal = rk.GetValue("X", "-1").ToString();
				myX = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Y", "-1").ToString();
				myY = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Width", "-1").ToString();
				myW = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Height", "-1").ToString();
				myH = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Top", "-1").ToString();
				if (regVal != "-1")
				{
					this.cbTop.Checked = bool.Parse(regVal);
				}
			}
			if (myX >= 0 && myY >= 0 && myW != -1 && myH != -1)
			{
				this.Left = myX;
				this.Top = myY;
				this.Width = myW;
				this.Height = myH;
			}
			else if (myX == -4 && myY == -4)
			{
				this.WindowState = FormWindowState.Maximized;
			}
			else if (myX == -32000 && myY == -32000)
			{
				this.WindowState = FormWindowState.Minimized;
			}

			StatusOverview_Resize(this,null);
		}
		private void WriteRegistry()
		{
			string theKey = "Software\\AIMSViewer\\"+ ViewFilter.CurrentViewName + "\\StatusOverview";
			RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			if(rk == null)
			{
				rk = Registry.CurrentUser.OpenSubKey("Software\\AIMSViewer\\" + ViewFilter.CurrentViewName, true);
				if (rk == null)
				{
					rk = Registry.CurrentUser.OpenSubKey("Software\\AIMSViewer", true);
					if (rk == null)
					{
						rk = Registry.CurrentUser.OpenSubKey("Software", true);
						rk.CreateSubKey("AIMSViewer");
						rk.Close();
						rk = Registry.CurrentUser.OpenSubKey("Software\\AIMSViewer", true);
					}

					rk.CreateSubKey(ViewFilter.CurrentViewName);
					rk.Close();
					rk = Registry.CurrentUser.OpenSubKey("Software\\AIMSViewer\\" + ViewFilter.CurrentViewName, true);
				}

				rk.CreateSubKey("StatusOverview");
				rk.Close();
				theKey = "Software\\AIMSViewer\\" + ViewFilter.CurrentViewName + "\\StatusOverview";
				rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			}

			rk.SetValue("X", this.Left.ToString());
			rk.SetValue("Y", this.Top.ToString());
			rk.SetValue("Width", this.Width.ToString());
			rk.SetValue("Height", this.Height.ToString());
			rk.SetValue("Top",this.cbTop.Checked.ToString());
		}

		private void StatusOverview_LocationChanged(object sender, System.EventArgs e)
		{
		}

		private void DataExchange_OnApplication_Shutdown()
		{
			this.WriteRegistry();
		}

		private void cbTop_CheckedChanged(object sender, System.EventArgs e)
		{
			this.TopMost = this.cbTop.Checked;
			if (this.cbTop.Checked)
			{
				this.cbTop.BackColor = Color.LightGreen;
				this.cbTop.Refresh();
			}
			else
			{
				this.cbTop.BackColor = Color.Transparent;
				this.cbTop.Refresh();
			}
		}

		private void comboName_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if(e.KeyChar == (char)13)
			{
				Search();
			}
		
		}

		private void DataExchange_OnSymbolSelection_Changed(object sender, PictureViewer.Symbol symbol, bool selected)
		{
			// select the line if found
			string line = this.tabList.GetLinesByIndexValue("URNO",symbol.urno,0);
			if (line.Length > 0)
			{
				int lineNo = int.Parse(line);
				int lineColor = 0;
				int textColor = 0;

				if (selected)		// redlining	
				{
					lineColor = UT.colRed;
					textColor = UT.colBlack;
				}
				else				// normal
				{
					lineColor = UT.colWhite;
					textColor = UT.colBlack;
				}

				this.tabList.SetLineColor(lineNo, textColor, lineColor);
				this.tabList.SetCurrentSelection(lineNo);
			}
		}

		private void ContextMenu_Click(object sender, System.EventArgs e)
		{
			int lineNo = this.tabList.GetCurrentSelected();
			if (lineNo >= 0)
			{
				string infoType;
				MenuItem item = (MenuItem)sender;
				switch(item.Index)
				{
					case 0:	// Next camera
						infoType = "NextCamera";
						break;
					case 1:	// Handling advice for Group
						infoType = "HandlingAdviceForGroup";
						break;
					case 2:	// Handling advice for Status
						infoType = "HandlingAdviceForStatus";
						break;
					default:
						infoType = string.Empty;
						break;
				}

				ArrayList selectedSymbols = new ArrayList();
				PictureViewer.Symbol symbol = new PictureViewer.Symbol();
				symbol.urno = tabList.GetFieldValue(lineNo,"URNO");
				selectedSymbols.Add(symbol);
				StatusInformationList dlg = new StatusInformationList(selectedSymbols,infoType);
				dlg.ShowDialog(this);
			}
			else
			{
				MessageBox.Show(this,"Sorry, no status event found at this location","Information");
			}
		}

		private void tabList_SendRButtonClick(object sender, AxTABLib._DTABEvents_SendRButtonClickEvent e)
		{
			Point pos = Control.MousePosition;
			pos = this.PointToClient(pos);
			this.tabList.SetCurrentSelection(e.lineNo);
			this.contextMenuOverview.Show(this,pos);
		}

		private void lblOriginator_Click(object sender, System.EventArgs e)
		{
		
		}

		private void comboSubsystem_TextChanged(object sender, System.EventArgs e)
		{
			if (comboSubsystem.Text != "")
			{
				Search();
			}
		}

		private void comboOriginator_TextChanged(object sender, System.EventArgs e)
		{
			if (comboOriginator.Text != "")
			{
				Search();
			}
		
		}

		private void comboType_TextChanged(object sender, System.EventArgs e)
		{
			if (comboType.Text != "")
			{
				Search();
			}
		
		}

		private void comboSubsystem_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if(e.KeyChar == (char)13)
			{
				Search();
			}
		
		}

		private void comboOriginator_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if(e.KeyChar == (char)13)
			{
				Search();
			}
		
		}

		private void comboType_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if(e.KeyChar == (char)13)
			{
				Search();
			}
		
		}

		private void StatusOverview_MaximumSizeChanged(object sender, System.EventArgs e)
		{
		
		}

		private void DataExchange_OnBeforeViewNameChanged()
		{
			this.WriteRegistry();
		}
	}
}
