using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Ufis.Data;
using Ufis.Utils;
using PictureViewer;

namespace AIMSViewer
{
	/// <summary>
	/// Summary description for SelectPoints.
	/// </summary>
	public class SelectPoints : System.Windows.Forms.Form,PictureViewer.ISelectPoints
	{
		private System.Windows.Forms.Label lblPicturepointX;
		private System.Windows.Forms.GroupBox grbPicturePoint;
		private System.Windows.Forms.Label lblPicturePointY;
		private System.Windows.Forms.GroupBox grbWorldPoint;
		private System.Windows.Forms.Label lblWorldPointY;
		private System.Windows.Forms.Label lblWorldPointX;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Button btnCancel;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private	Point	picturePoint = new Point(0,0);
		private System.Windows.Forms.NumericUpDown udPicturePointX;
		private System.Windows.Forms.NumericUpDown udPicturePointY;
		private System.Windows.Forms.NumericUpDown udWorldPointX;
		private System.Windows.Forms.NumericUpDown udWorldPointY;
		private System.Windows.Forms.Label lblViewpoints;
		private System.Windows.Forms.ComboBox cbViewpoints;
		private System.Windows.Forms.CheckBox cbSelectCamera;
		private System.Windows.Forms.Label lblPreview;
		private PictureViewer.PictureControlEx picPreview;
		private System.Windows.Forms.Label lblPath;
		private System.Windows.Forms.TextBox txtPath;
		private PointF	worldPoint = new PointF(0,0);

		public SelectPoints()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblPicturepointX = new System.Windows.Forms.Label();
			this.grbPicturePoint = new System.Windows.Forms.GroupBox();
			this.udPicturePointY = new System.Windows.Forms.NumericUpDown();
			this.udPicturePointX = new System.Windows.Forms.NumericUpDown();
			this.lblPicturePointY = new System.Windows.Forms.Label();
			this.grbWorldPoint = new System.Windows.Forms.GroupBox();
			this.lblPath = new System.Windows.Forms.Label();
			this.txtPath = new System.Windows.Forms.TextBox();
			this.lblPreview = new System.Windows.Forms.Label();
			this.cbSelectCamera = new System.Windows.Forms.CheckBox();
			this.cbViewpoints = new System.Windows.Forms.ComboBox();
			this.lblViewpoints = new System.Windows.Forms.Label();
			this.lblWorldPointY = new System.Windows.Forms.Label();
			this.lblWorldPointX = new System.Windows.Forms.Label();
			this.udWorldPointY = new System.Windows.Forms.NumericUpDown();
			this.udWorldPointX = new System.Windows.Forms.NumericUpDown();
			this.btnOK = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.picPreview = new PictureViewer.PictureControlEx();
			this.grbPicturePoint.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.udPicturePointY)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.udPicturePointX)).BeginInit();
			this.grbWorldPoint.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.udWorldPointY)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.udWorldPointX)).BeginInit();
			this.SuspendLayout();
			// 
			// lblPicturepointX
			// 
			this.lblPicturepointX.Location = new System.Drawing.Point(16, 24);
			this.lblPicturepointX.Name = "lblPicturepointX";
			this.lblPicturepointX.Size = new System.Drawing.Size(40, 23);
			this.lblPicturepointX.TabIndex = 1;
			this.lblPicturepointX.Text = "X :";
			// 
			// grbPicturePoint
			// 
			this.grbPicturePoint.Controls.Add(this.udPicturePointY);
			this.grbPicturePoint.Controls.Add(this.udPicturePointX);
			this.grbPicturePoint.Controls.Add(this.lblPicturePointY);
			this.grbPicturePoint.Controls.Add(this.lblPicturepointX);
			this.grbPicturePoint.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.grbPicturePoint.Location = new System.Drawing.Point(8, 8);
			this.grbPicturePoint.Name = "grbPicturePoint";
			this.grbPicturePoint.Size = new System.Drawing.Size(440, 96);
			this.grbPicturePoint.TabIndex = 0;
			this.grbPicturePoint.TabStop = false;
			this.grbPicturePoint.Text = "Picture point";
			// 
			// udPicturePointY
			// 
			this.udPicturePointY.Location = new System.Drawing.Point(112, 48);
			this.udPicturePointY.Maximum = new System.Decimal(new int[] {
																			2147483647,
																			0,
																			0,
																			0});
			this.udPicturePointY.Name = "udPicturePointY";
			this.udPicturePointY.Size = new System.Drawing.Size(144, 20);
			this.udPicturePointY.TabIndex = 4;
			// 
			// udPicturePointX
			// 
			this.udPicturePointX.Location = new System.Drawing.Point(112, 24);
			this.udPicturePointX.Maximum = new System.Decimal(new int[] {
																			2147483647,
																			0,
																			0,
																			0});
			this.udPicturePointX.Name = "udPicturePointX";
			this.udPicturePointX.Size = new System.Drawing.Size(144, 20);
			this.udPicturePointX.TabIndex = 2;
			// 
			// lblPicturePointY
			// 
			this.lblPicturePointY.Location = new System.Drawing.Point(16, 56);
			this.lblPicturePointY.Name = "lblPicturePointY";
			this.lblPicturePointY.Size = new System.Drawing.Size(40, 23);
			this.lblPicturePointY.TabIndex = 3;
			this.lblPicturePointY.Text = "Y :";
			// 
			// grbWorldPoint
			// 
			this.grbWorldPoint.Controls.Add(this.lblPath);
			this.grbWorldPoint.Controls.Add(this.txtPath);
			this.grbWorldPoint.Controls.Add(this.lblPreview);
			this.grbWorldPoint.Controls.Add(this.cbSelectCamera);
			this.grbWorldPoint.Controls.Add(this.cbViewpoints);
			this.grbWorldPoint.Controls.Add(this.lblViewpoints);
			this.grbWorldPoint.Controls.Add(this.lblWorldPointY);
			this.grbWorldPoint.Controls.Add(this.lblWorldPointX);
			this.grbWorldPoint.Controls.Add(this.udWorldPointY);
			this.grbWorldPoint.Controls.Add(this.udWorldPointX);
			this.grbWorldPoint.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.grbWorldPoint.Location = new System.Drawing.Point(8, 112);
			this.grbWorldPoint.Name = "grbWorldPoint";
			this.grbWorldPoint.Size = new System.Drawing.Size(440, 552);
			this.grbWorldPoint.TabIndex = 5;
			this.grbWorldPoint.TabStop = false;
			this.grbWorldPoint.Text = "World point";
			// 
			// lblPath
			// 
			this.lblPath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.lblPath.BackColor = System.Drawing.Color.Transparent;
			this.lblPath.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblPath.Location = new System.Drawing.Point(16, 520);
			this.lblPath.Name = "lblPath";
			this.lblPath.Size = new System.Drawing.Size(40, 16);
			this.lblPath.TabIndex = 14;
			this.lblPath.Text = "Path:";
			// 
			// txtPath
			// 
			this.txtPath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.txtPath.Location = new System.Drawing.Point(56, 520);
			this.txtPath.Name = "txtPath";
			this.txtPath.ReadOnly = true;
			this.txtPath.Size = new System.Drawing.Size(368, 20);
			this.txtPath.TabIndex = 15;
			this.txtPath.Tag = "PATH";
			this.txtPath.Text = "C:\\";
			// 
			// lblPreview
			// 
			this.lblPreview.BackColor = System.Drawing.Color.Transparent;
			this.lblPreview.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblPreview.Location = new System.Drawing.Point(16, 128);
			this.lblPreview.Name = "lblPreview";
			this.lblPreview.Size = new System.Drawing.Size(200, 16);
			this.lblPreview.TabIndex = 13;
			this.lblPreview.Text = "Image Preview:";
			// 
			// cbSelectCamera
			// 
			this.cbSelectCamera.Appearance = System.Windows.Forms.Appearance.Button;
			this.cbSelectCamera.Location = new System.Drawing.Point(320, 80);
			this.cbSelectCamera.Name = "cbSelectCamera";
			this.cbSelectCamera.TabIndex = 12;
			this.cbSelectCamera.Text = "&Select Position";
			this.cbSelectCamera.CheckedChanged += new System.EventHandler(this.cbSelectCamera_CheckedChanged);
			// 
			// cbViewpoints
			// 
			this.cbViewpoints.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbViewpoints.Location = new System.Drawing.Point(112, 80);
			this.cbViewpoints.Name = "cbViewpoints";
			this.cbViewpoints.Size = new System.Drawing.Size(192, 22);
			this.cbViewpoints.TabIndex = 11;
			this.cbViewpoints.SelectedIndexChanged += new System.EventHandler(this.cbViewpoints_SelectedIndexChanged);
			// 
			// lblViewpoints
			// 
			this.lblViewpoints.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblViewpoints.Location = new System.Drawing.Point(16, 80);
			this.lblViewpoints.Name = "lblViewpoints";
			this.lblViewpoints.Size = new System.Drawing.Size(72, 23);
			this.lblViewpoints.TabIndex = 10;
			this.lblViewpoints.Text = "Viewpoints :";
			// 
			// lblWorldPointY
			// 
			this.lblWorldPointY.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblWorldPointY.Location = new System.Drawing.Point(16, 56);
			this.lblWorldPointY.Name = "lblWorldPointY";
			this.lblWorldPointY.Size = new System.Drawing.Size(40, 23);
			this.lblWorldPointY.TabIndex = 8;
			this.lblWorldPointY.Text = "Y :";
			// 
			// lblWorldPointX
			// 
			this.lblWorldPointX.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblWorldPointX.Location = new System.Drawing.Point(16, 24);
			this.lblWorldPointX.Name = "lblWorldPointX";
			this.lblWorldPointX.Size = new System.Drawing.Size(40, 23);
			this.lblWorldPointX.TabIndex = 6;
			this.lblWorldPointX.Text = "X :";
			// 
			// udWorldPointY
			// 
			this.udWorldPointY.DecimalPlaces = 2;
			this.udWorldPointY.Location = new System.Drawing.Point(112, 48);
			this.udWorldPointY.Maximum = new System.Decimal(new int[] {
																		  268435456,
																		  1042612833,
																		  542101086,
																		  0});
			this.udWorldPointY.Minimum = new System.Decimal(new int[] {
																		  268435456,
																		  1042612833,
																		  542101086,
																		  -2147483648});
			this.udWorldPointY.Name = "udWorldPointY";
			this.udWorldPointY.Size = new System.Drawing.Size(144, 20);
			this.udWorldPointY.TabIndex = 9;
			// 
			// udWorldPointX
			// 
			this.udWorldPointX.DecimalPlaces = 2;
			this.udWorldPointX.Location = new System.Drawing.Point(112, 24);
			this.udWorldPointX.Maximum = new System.Decimal(new int[] {
																		  268435456,
																		  1042612833,
																		  542101086,
																		  0});
			this.udWorldPointX.Minimum = new System.Decimal(new int[] {
																		  268435456,
																		  1042612833,
																		  542101086,
																		  -2147483648});
			this.udWorldPointX.Name = "udWorldPointX";
			this.udWorldPointX.Size = new System.Drawing.Size(144, 20);
			this.udWorldPointX.TabIndex = 7;
			// 
			// btnOK
			// 
			this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOK.Location = new System.Drawing.Point(139, 672);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 10;
			this.btnOK.Text = "OK";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(219, 672);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 11;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// picPreview
			// 
			this.picPreview.BackColor = System.Drawing.SystemColors.Control;
			this.picPreview.Floor = "-99";
			this.picPreview.Image = null;
			this.picPreview.Location = new System.Drawing.Point(32, 264);
			this.picPreview.MouseStatus = PictureViewer.MouseStateEx.None;
			this.picPreview.Name = "picPreview";
			this.picPreview.Size = new System.Drawing.Size(400, 360);
			this.picPreview.TabIndex = 12;
			this.picPreview.OnEndWorldPointHandler += new PictureViewer.PictureControlEx.EndWorldPointHandler(this.picPreview_OnEndWorldPointHandler);
			this.picPreview.OnCancelWorldPointHandler += new PictureViewer.PictureControlEx.CancelWorldPointHandler(this.picPreview_OnCancelWorldPointHandler);
			// 
			// SelectPoints
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(456, 702);
			this.Controls.Add(this.picPreview);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOK);
			this.Controls.Add(this.grbPicturePoint);
			this.Controls.Add(this.grbWorldPoint);
			this.Name = "SelectPoints";
			this.Text = "SelectPoints";
			this.Load += new System.EventHandler(this.SelectPoints_Load);
			this.grbPicturePoint.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.udPicturePointY)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.udPicturePointX)).EndInit();
			this.grbWorldPoint.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.udWorldPointY)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.udWorldPointX)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		public Point PicturePoint
		{
			get
			{
				return this.picturePoint;
			}
			set
			{
				this.picturePoint = value;
				this.udPicturePointX.Value = (decimal)value.X;
				this.udPicturePointY.Value = (decimal)value.Y;
			}
		}

		public PointF WorldPoint
		{
			get
			{
				return this.worldPoint;
			}
			set
			{
				this.worldPoint = value;
				this.udWorldPointX.Value = (decimal)value.X;
				this.udWorldPointY.Value = (decimal)value.Y;
			}
		}

		private void btnSelectPoint_Click(object sender, System.EventArgs e)
		{
			this.Hide();
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
		}

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			this.picturePoint.X = (int)this.udPicturePointX.Value;
			this.picturePoint.Y = (int)this.udPicturePointY.Value;
			this.worldPoint.X	= (float)this.udWorldPointX.Value;
			this.worldPoint.Y	= (float)this.udWorldPointY.Value;
		}

		private void SelectPoints_Load(object sender, System.EventArgs e)
		{
			IDatabase myDB = UT.GetMemDB();
			ITable	myTable= myDB["AVP"];
			
			for (int i = 0; i < myTable.Count; i++)
			{
				this.cbViewpoints.Items.Add(myTable[i]["NAME"]);
			}

			if (this.cbViewpoints.Items.Count > 0)
			{
				int ind = 0;
				this.cbViewpoints.SelectedIndex = ind;
			}

		}

		private void cbViewpoints_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			IDatabase myDB = UT.GetMemDB();
			ITable	myTable= myDB["AVP"];
			IRow[]	myRows = myTable.RowsByIndexValue("NAME",(string)this.cbViewpoints.Text);
			if (myRows != null && myRows.Length == 1)
			{
				try
				{
					this.picPreview.Image = Image.FromFile(myRows[0]["PATH"]);
					this.txtPath.Text = myRows[0]["PATH"];	
					frmData.GetThis().CreateOrUpdatePictureControl(this.picPreview,myRows[0],null,null,null);
					this.cbSelectCamera.Enabled = true;
					this.cbSelectCamera.Checked = false;
				}
				catch(Exception ex)
				{
					System.Diagnostics.Debug.WriteLine(ex.Message);
				}
			}
		
		}

		private void cbSelectCamera_CheckedChanged(object sender, System.EventArgs e)
		{
			if (this.cbSelectCamera.Checked)
			{
				this.picPreview.CalculateWorldPoint();
			}
		}

		private void picPreview_OnCancelWorldPointHandler()
		{
			this.DialogResult = DialogResult.Cancel;
			this.Close();
		}

		private void picPreview_OnEndWorldPointHandler(System.Drawing.PointF origin)
		{
			this.WorldPoint   = origin;
			this.DialogResult = DialogResult.OK;
			this.Close();
		}
	}
}
