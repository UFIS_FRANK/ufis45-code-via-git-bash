using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Ufis.Data;
using Ufis.Utils;

namespace AIMSViewer
{
	/// <summary>
	/// Summary description for CameraPosition.
	/// </summary>
	public class Location : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.PictureBox picTop;
		private System.Windows.Forms.PictureBox picBody;
		private System.Windows.Forms.Panel panelBody;
		private PictureViewer.PictureControlEx picPreview;
		private System.Windows.Forms.Label lblPath;
		private System.Windows.Forms.TextBox txtPath;
		private System.Windows.Forms.Label lblPreview;
		private System.Windows.Forms.Label lblName;
		private System.Windows.Forms.ComboBox cbViewpoints;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private PointF		position;
		private	string		path;
		private System.Windows.Forms.CheckBox cbSelectLocation;
		private	string		floor;

		public Location()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			this.path = string.Empty;
			this.position = PointF.Empty;
			this.floor = "-99";

			this.picPreview.OnCancelWorldPointHandler += new PictureViewer.PictureControlEx.CancelWorldPointHandler(picPreview_OnCancelWorldPointHandler);
			this.picPreview.OnEndWorldPointHandler	  +=new PictureViewer.PictureControlEx.EndWorldPointHandler(picPreview_OnEndWorldPointHandler);
		}

		public PointF	Position
		{
			get
			{
				return this.position;
			}
			set
			{
				this.position = value;
			}
		}

		public string	Path
		{
			get
			{
				return this.path;
			}
			set
			{
				this.path = value;
			}
		}

		public string	Floor
		{
			get
			{
				return this.floor;
			}
			set
			{
				this.floor = value;
			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Location));
			this.panelTop = new System.Windows.Forms.Panel();
			this.btnClose = new System.Windows.Forms.Button();
			this.btnSave = new System.Windows.Forms.Button();
			this.picTop = new System.Windows.Forms.PictureBox();
			this.panelBody = new System.Windows.Forms.Panel();
			this.cbSelectLocation = new System.Windows.Forms.CheckBox();
			this.cbViewpoints = new System.Windows.Forms.ComboBox();
			this.picPreview = new PictureViewer.PictureControlEx();
			this.lblPath = new System.Windows.Forms.Label();
			this.txtPath = new System.Windows.Forms.TextBox();
			this.lblPreview = new System.Windows.Forms.Label();
			this.lblName = new System.Windows.Forms.Label();
			this.picBody = new System.Windows.Forms.PictureBox();
			this.panelTop.SuspendLayout();
			this.panelBody.SuspendLayout();
			this.SuspendLayout();
			// 
			// panelTop
			// 
			this.panelTop.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelTop.Controls.Add(this.btnClose);
			this.panelTop.Controls.Add(this.btnSave);
			this.panelTop.Controls.Add(this.picTop);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(0, 0);
			this.panelTop.Name = "panelTop";
			this.panelTop.Size = new System.Drawing.Size(516, 32);
			this.panelTop.TabIndex = 10;
			// 
			// btnClose
			// 
			this.btnClose.BackColor = System.Drawing.Color.Transparent;
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnClose.Dock = System.Windows.Forms.DockStyle.Left;
			this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnClose.ImageIndex = 4;
			this.btnClose.Location = new System.Drawing.Point(72, 0);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(72, 28);
			this.btnClose.TabIndex = 8;
			this.btnClose.Text = "   &Close";
			// 
			// btnSave
			// 
			this.btnSave.BackColor = System.Drawing.Color.Transparent;
			this.btnSave.Dock = System.Windows.Forms.DockStyle.Left;
			this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnSave.ImageIndex = 2;
			this.btnSave.Location = new System.Drawing.Point(0, 0);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(72, 28);
			this.btnSave.TabIndex = 7;
			this.btnSave.Text = "   &Save";
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// picTop
			// 
			this.picTop.Dock = System.Windows.Forms.DockStyle.Fill;
			this.picTop.Location = new System.Drawing.Point(0, 0);
			this.picTop.Name = "picTop";
			this.picTop.Size = new System.Drawing.Size(512, 28);
			this.picTop.TabIndex = 0;
			this.picTop.TabStop = false;
			this.picTop.Paint += new System.Windows.Forms.PaintEventHandler(this.picTop_Paint);
			// 
			// panelBody
			// 
			this.panelBody.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelBody.Controls.Add(this.cbSelectLocation);
			this.panelBody.Controls.Add(this.cbViewpoints);
			this.panelBody.Controls.Add(this.picPreview);
			this.panelBody.Controls.Add(this.lblPath);
			this.panelBody.Controls.Add(this.txtPath);
			this.panelBody.Controls.Add(this.lblPreview);
			this.panelBody.Controls.Add(this.lblName);
			this.panelBody.Controls.Add(this.picBody);
			this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelBody.Location = new System.Drawing.Point(0, 32);
			this.panelBody.Name = "panelBody";
			this.panelBody.Size = new System.Drawing.Size(516, 646);
			this.panelBody.TabIndex = 9;
			// 
			// cbSelectLocation
			// 
			this.cbSelectLocation.Appearance = System.Windows.Forms.Appearance.Button;
			this.cbSelectLocation.Location = new System.Drawing.Point(360, 8);
			this.cbSelectLocation.Name = "cbSelectLocation";
			this.cbSelectLocation.TabIndex = 2;
			this.cbSelectLocation.Text = "&Select Location";
			this.cbSelectLocation.CheckedChanged += new System.EventHandler(this.cbSelectCamera_CheckedChanged);
			// 
			// cbViewpoints
			// 
			this.cbViewpoints.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbViewpoints.Location = new System.Drawing.Point(96, 8);
			this.cbViewpoints.Name = "cbViewpoints";
			this.cbViewpoints.Size = new System.Drawing.Size(256, 21);
			this.cbViewpoints.TabIndex = 0;
			this.cbViewpoints.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
			// 
			// picPreview
			// 
			this.picPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.picPreview.BackColor = System.Drawing.SystemColors.Control;
			this.picPreview.BlinkInterval = 1000;
			this.picPreview.Floor = "-99";
			this.picPreview.Image = null;
			this.picPreview.Location = new System.Drawing.Point(28, 72);
			this.picPreview.MouseStatus = PictureViewer.MouseStateEx.None;
			this.picPreview.Name = "picPreview";
			this.picPreview.ScaleFactor = new System.Decimal(new int[] {
																		   1,
																		   0,
																		   0,
																		   0});
			this.picPreview.Size = new System.Drawing.Size(448, 520);
			this.picPreview.TabIndex = 4;
			// 
			// lblPath
			// 
			this.lblPath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.lblPath.BackColor = System.Drawing.Color.Transparent;
			this.lblPath.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblPath.Location = new System.Drawing.Point(24, 608);
			this.lblPath.Name = "lblPath";
			this.lblPath.Size = new System.Drawing.Size(36, 16);
			this.lblPath.TabIndex = 5;
			this.lblPath.Text = "Path:";
			// 
			// txtPath
			// 
			this.txtPath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.txtPath.Location = new System.Drawing.Point(64, 608);
			this.txtPath.Name = "txtPath";
			this.txtPath.ReadOnly = true;
			this.txtPath.Size = new System.Drawing.Size(336, 20);
			this.txtPath.TabIndex = 6;
			this.txtPath.Tag = "PATH";
			this.txtPath.Text = "C:\\";
			// 
			// lblPreview
			// 
			this.lblPreview.BackColor = System.Drawing.Color.Transparent;
			this.lblPreview.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblPreview.Location = new System.Drawing.Point(16, 40);
			this.lblPreview.Name = "lblPreview";
			this.lblPreview.Size = new System.Drawing.Size(200, 16);
			this.lblPreview.TabIndex = 3;
			this.lblPreview.Text = "Image Preview:";
			// 
			// lblName
			// 
			this.lblName.BackColor = System.Drawing.Color.Transparent;
			this.lblName.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblName.Location = new System.Drawing.Point(16, 12);
			this.lblName.Name = "lblName";
			this.lblName.Size = new System.Drawing.Size(72, 16);
			this.lblName.TabIndex = 1;
			this.lblName.Text = "Viewpoint :";
			// 
			// picBody
			// 
			this.picBody.Dock = System.Windows.Forms.DockStyle.Fill;
			this.picBody.Location = new System.Drawing.Point(0, 0);
			this.picBody.Name = "picBody";
			this.picBody.Size = new System.Drawing.Size(512, 642);
			this.picBody.TabIndex = 0;
			this.picBody.TabStop = false;
			this.picBody.Paint += new System.Windows.Forms.PaintEventHandler(this.picBody_Paint);
			// 
			// Location
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(516, 678);
			this.Controls.Add(this.panelBody);
			this.Controls.Add(this.panelTop);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "Location";
			this.Text = "Location";
			this.Load += new System.EventHandler(this.CameraPosition_Load);
			this.panelTop.ResumeLayout(false);
			this.panelBody.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void CameraPosition_Load(object sender, System.EventArgs e)
		{
			btnClose.Parent		= picTop;
			btnSave.Parent		= picTop;

			lblName.Parent		= picBody;
			lblPath.Parent		= picBody;
			lblPreview.Parent	= picBody;

			this.cbSelectLocation.Enabled = false;

			IDatabase myDB = UT.GetMemDB();
			ITable	myTable= myDB["AVP"];
			
			for (int i = 0; i < myTable.Count; i++)
			{
				if (myTable[i]["FOVE"] != "2")	// must not be Overview
				{
					if (ViewFilter.IsPassFilter(myTable[i]))
					{
						this.cbViewpoints.Items.Add(myTable[i]["NAME"]);
					}
				}
			}

			if (this.cbViewpoints.Items.Count > 0)
			{
				int ind = 0;
				if (this.path != string.Empty)
				{
					ind = this.cbViewpoints.Items.IndexOf(this.path);
				}

				this.cbViewpoints.SelectedIndex = ind;
			}
		}

		private void picBody_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, picBody, Color.WhiteSmoke, Color.LightGray);		
		}

		private void picTop_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, picTop, Color.WhiteSmoke, Color.LightGray);		
		}

		private void comboBox1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			IDatabase myDB = UT.GetMemDB();
			ITable	myTable= myDB["AVP"];
			IRow[]	myRows = myTable.RowsByIndexValue("NAME",(string)this.cbViewpoints.Text);
			if (myRows != null && myRows.Length == 1)
			{
				try
				{
					this.picPreview.Image = Image.FromFile(myRows[0]["PATH"]);
					this.txtPath.Text = myRows[0]["PATH"];	
					frmData.GetThis().CreateOrUpdatePictureControl(this.picPreview,myRows[0],null,null,null);
					this.cbSelectLocation.Enabled = true;
					this.cbSelectLocation.Checked = false;
					this.path = this.cbViewpoints.Text;
					this.floor= myRows[0]["FLOR"];
				}
				catch(Exception ex)
				{
				}
			}
		}

		private void cbSelectCamera_CheckedChanged(object sender, System.EventArgs e)
		{
			if (this.cbSelectLocation.Checked == true)
			{
				this.picPreview.CalculateWorldPoint();
			}
			else
			{
				this.picPreview.ClearCommandStack();
			}
		}

		private void picPreview_OnCancelWorldPointHandler()
		{
			this.DialogResult = DialogResult.Cancel;		
			this.Close();		

		}

		private void picPreview_OnEndWorldPointHandler(System.Drawing.PointF origin)
		{
			if (frmData.GetThis().IsValidLocation(origin))
			{
				this.position = origin;
				this.DialogResult = DialogResult.OK;
				this.Close();
			}
			else
			{
				MessageBox.Show(this,"Location is outside of the world definition!\nPlease check the correctness of the input parameters!");				
			}
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
		
		}
	}
}
