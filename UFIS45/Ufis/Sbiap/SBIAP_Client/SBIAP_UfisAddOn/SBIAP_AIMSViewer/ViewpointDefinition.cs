using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Globalization;
using System.Text;
using Ufis.Utils;
using Ufis.Data;
using PictureViewer;

namespace AIMSViewer
{
	/// <summary>
	/// Summary description for ViewpointDefinition.
	/// </summary>
	public class ViewpointDefinition : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.PictureBox picTop;
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.PictureBox picBody;
		private System.Windows.Forms.Label lblName;
		private System.Windows.Forms.TextBox txtName;
		private System.Windows.Forms.TextBox txtPath;
		private System.Windows.Forms.Label lblPath;
		private System.Windows.Forms.Button btnBrowse;
		private System.Windows.Forms.Label lblPreview;
		private System.Windows.Forms.Label lblBrace;
		private System.Windows.Forms.Label lblInMeters;
		private System.Windows.Forms.Label lblYOffset;
		private System.Windows.Forms.Label lblXOffset;
		private System.Windows.Forms.NumericUpDown udYOffset;
		private System.Windows.Forms.NumericUpDown udXOffset;
		private System.Windows.Forms.NumericUpDown udHeight;
		private System.Windows.Forms.NumericUpDown udWidth;
		private System.Windows.Forms.Label lblWidth;
		private System.Windows.Forms.Label lblHeight;
		private System.Windows.Forms.Label lblRotation;
		private System.Windows.Forms.NumericUpDown udRotation;
		private System.Windows.Forms.Label lblDegree;
		private System.Windows.Forms.Button btnCalculateTranslation;
		private PictureViewer.PictureControlEx picPreview;
		private System.ComponentModel.IContainer components;

		#region	implementation
		private	IRow	currRow = null;
		private	IRow	oldRow	= null;
		private	IRow	currDBRow = null;
		private	string	myMode	= string.Empty;
		private System.Windows.Forms.ComboBox cbViewType;
		private System.Windows.Forms.Label lblViewType;
		private System.Windows.Forms.Label lblFloor;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.TextBox txtFloor;
		private	string	name;	
		#endregion
		public ViewpointDefinition(string name)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			this.name = name;
			this.txtPath.TextChanged +=new EventHandler(txtPath_TextChanged);

			// create new view definition ?
			if (this.name.Length == 0)
			{
				NewViewpoint();				
			}
			else
			{
				NewViewpoint();
				OpenViewpoint(name);
			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ViewpointDefinition));
			this.panelTop = new System.Windows.Forms.Panel();
			this.btnClose = new System.Windows.Forms.Button();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.btnSave = new System.Windows.Forms.Button();
			this.picTop = new System.Windows.Forms.PictureBox();
			this.panelBody = new System.Windows.Forms.Panel();
			this.txtFloor = new System.Windows.Forms.TextBox();
			this.lblViewType = new System.Windows.Forms.Label();
			this.cbViewType = new System.Windows.Forms.ComboBox();
			this.picPreview = new PictureViewer.PictureControlEx();
			this.btnCalculateTranslation = new System.Windows.Forms.Button();
			this.lblDegree = new System.Windows.Forms.Label();
			this.udRotation = new System.Windows.Forms.NumericUpDown();
			this.lblRotation = new System.Windows.Forms.Label();
			this.udHeight = new System.Windows.Forms.NumericUpDown();
			this.udWidth = new System.Windows.Forms.NumericUpDown();
			this.lblHeight = new System.Windows.Forms.Label();
			this.lblWidth = new System.Windows.Forms.Label();
			this.btnBrowse = new System.Windows.Forms.Button();
			this.lblPath = new System.Windows.Forms.Label();
			this.txtPath = new System.Windows.Forms.TextBox();
			this.lblPreview = new System.Windows.Forms.Label();
			this.lblFloor = new System.Windows.Forms.Label();
			this.lblBrace = new System.Windows.Forms.Label();
			this.lblInMeters = new System.Windows.Forms.Label();
			this.udYOffset = new System.Windows.Forms.NumericUpDown();
			this.udXOffset = new System.Windows.Forms.NumericUpDown();
			this.lblYOffset = new System.Windows.Forms.Label();
			this.lblXOffset = new System.Windows.Forms.Label();
			this.txtName = new System.Windows.Forms.TextBox();
			this.lblName = new System.Windows.Forms.Label();
			this.picBody = new System.Windows.Forms.PictureBox();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.panelTop.SuspendLayout();
			this.panelBody.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.udRotation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.udHeight)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.udWidth)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.udYOffset)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.udXOffset)).BeginInit();
			this.SuspendLayout();
			// 
			// panelTop
			// 
			this.panelTop.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelTop.Controls.Add(this.btnClose);
			this.panelTop.Controls.Add(this.btnSave);
			this.panelTop.Controls.Add(this.picTop);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(0, 0);
			this.panelTop.Name = "panelTop";
			this.panelTop.Size = new System.Drawing.Size(516, 32);
			this.panelTop.TabIndex = 27;
			// 
			// btnClose
			// 
			this.btnClose.BackColor = System.Drawing.Color.Transparent;
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnClose.Dock = System.Windows.Forms.DockStyle.Left;
			this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnClose.ImageIndex = 4;
			this.btnClose.ImageList = this.imageList1;
			this.btnClose.Location = new System.Drawing.Point(72, 0);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(72, 28);
			this.btnClose.TabIndex = 25;
			this.btnClose.Text = "   &Close";
			this.toolTip1.SetToolTip(this.btnClose, "Close this window");
			// 
			// imageList1
			// 
			this.imageList1.ImageSize = new System.Drawing.Size(19, 19);
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.White;
			// 
			// btnSave
			// 
			this.btnSave.BackColor = System.Drawing.Color.Transparent;
			this.btnSave.Dock = System.Windows.Forms.DockStyle.Left;
			this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnSave.ImageIndex = 2;
			this.btnSave.ImageList = this.imageList1;
			this.btnSave.Location = new System.Drawing.Point(0, 0);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(72, 28);
			this.btnSave.TabIndex = 24;
			this.btnSave.Text = "   &Save";
			this.toolTip1.SetToolTip(this.btnSave, "Save the currently active view point definition");
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// picTop
			// 
			this.picTop.Dock = System.Windows.Forms.DockStyle.Fill;
			this.picTop.Location = new System.Drawing.Point(0, 0);
			this.picTop.Name = "picTop";
			this.picTop.Size = new System.Drawing.Size(512, 28);
			this.picTop.TabIndex = 0;
			this.picTop.TabStop = false;
			this.picTop.Paint += new System.Windows.Forms.PaintEventHandler(this.picTop_Paint);
			// 
			// panelBody
			// 
			this.panelBody.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelBody.Controls.Add(this.txtFloor);
			this.panelBody.Controls.Add(this.lblViewType);
			this.panelBody.Controls.Add(this.cbViewType);
			this.panelBody.Controls.Add(this.picPreview);
			this.panelBody.Controls.Add(this.btnCalculateTranslation);
			this.panelBody.Controls.Add(this.lblDegree);
			this.panelBody.Controls.Add(this.udRotation);
			this.panelBody.Controls.Add(this.lblRotation);
			this.panelBody.Controls.Add(this.udHeight);
			this.panelBody.Controls.Add(this.udWidth);
			this.panelBody.Controls.Add(this.lblHeight);
			this.panelBody.Controls.Add(this.lblWidth);
			this.panelBody.Controls.Add(this.btnBrowse);
			this.panelBody.Controls.Add(this.lblPath);
			this.panelBody.Controls.Add(this.txtPath);
			this.panelBody.Controls.Add(this.lblPreview);
			this.panelBody.Controls.Add(this.lblFloor);
			this.panelBody.Controls.Add(this.lblBrace);
			this.panelBody.Controls.Add(this.lblInMeters);
			this.panelBody.Controls.Add(this.udYOffset);
			this.panelBody.Controls.Add(this.udXOffset);
			this.panelBody.Controls.Add(this.lblYOffset);
			this.panelBody.Controls.Add(this.lblXOffset);
			this.panelBody.Controls.Add(this.txtName);
			this.panelBody.Controls.Add(this.lblName);
			this.panelBody.Controls.Add(this.picBody);
			this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelBody.Location = new System.Drawing.Point(0, 32);
			this.panelBody.Name = "panelBody";
			this.panelBody.Size = new System.Drawing.Size(516, 646);
			this.panelBody.TabIndex = 26;
			// 
			// txtFloor
			// 
			this.txtFloor.Location = new System.Drawing.Point(300, 148);
			this.txtFloor.MaxLength = 3;
			this.txtFloor.Name = "txtFloor";
			this.txtFloor.Size = new System.Drawing.Size(72, 20);
			this.txtFloor.TabIndex = 32;
			this.txtFloor.Tag = "FLOR";
			this.txtFloor.Text = "";
			this.toolTip1.SetToolTip(this.txtFloor, "The floor name of this viewpoint. 0 means the ground floor, -1 the basement.");
			this.txtFloor.TextChanged += new System.EventHandler(this.txtName_TextChanged);
			// 
			// lblViewType
			// 
			this.lblViewType.BackColor = System.Drawing.Color.Transparent;
			this.lblViewType.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblViewType.Location = new System.Drawing.Point(24, 152);
			this.lblViewType.Name = "lblViewType";
			this.lblViewType.Size = new System.Drawing.Size(68, 16);
			this.lblViewType.TabIndex = 15;
			this.lblViewType.Text = "View type :";
			// 
			// cbViewType
			// 
			this.cbViewType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbViewType.Items.AddRange(new object[] {
															"Detail",
															"Floor",
															"Overview"});
			this.cbViewType.Location = new System.Drawing.Point(100, 148);
			this.cbViewType.Name = "cbViewType";
			this.cbViewType.Size = new System.Drawing.Size(121, 22);
			this.cbViewType.TabIndex = 16;
			this.cbViewType.Tag = "FOVE";
			this.toolTip1.SetToolTip(this.cbViewType, "Select the type of view point you wish to define");
			this.cbViewType.TextChanged += new System.EventHandler(this.txtName_TextChanged);
			this.cbViewType.SelectedValueChanged += new System.EventHandler(this.cbViewType_SelectedValueChanged);
			this.cbViewType.Leave += new System.EventHandler(this.txtName_TextChanged);
			// 
			// picPreview
			// 
			this.picPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.picPreview.BackColor = System.Drawing.SystemColors.Control;
			this.picPreview.BlinkInterval = 1000;
			this.picPreview.Floor = "-99";
			this.picPreview.Image = null;
			this.picPreview.Location = new System.Drawing.Point(28, 220);
			this.picPreview.MouseStatus = PictureViewer.MouseStateEx.None;
			this.picPreview.Name = "picPreview";
			this.picPreview.ScaleFactor = new System.Decimal(new int[] {
																		   1,
																		   0,
																		   0,
																		   0});
			this.picPreview.Size = new System.Drawing.Size(448, 372);
			this.picPreview.TabIndex = 21;
			this.picPreview.OnCancelTransformationHandler += new PictureViewer.PictureControlEx.CancelTransformationHandler(this.picPreview_OnCancelTransformationHandler);
			this.picPreview.Paint += new System.Windows.Forms.PaintEventHandler(this.picPreview_Paint);
			this.picPreview.OnEndTransformationHandler += new PictureViewer.PictureControlEx.EndTransformationHandler(this.picPreview_OnEndTransformationHandler);
			// 
			// btnCalculateTranslation
			// 
			this.btnCalculateTranslation.BackColor = System.Drawing.Color.Transparent;
			this.btnCalculateTranslation.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnCalculateTranslation.Location = new System.Drawing.Point(408, 100);
			this.btnCalculateTranslation.Name = "btnCalculateTranslation";
			this.btnCalculateTranslation.TabIndex = 14;
			this.btnCalculateTranslation.Text = "&Calculate ...";
			this.toolTip1.SetToolTip(this.btnCalculateTranslation, "Allows the calculation of offset and area from two known locations within the vie" +
				"w point.");
			this.btnCalculateTranslation.Click += new System.EventHandler(this.btnCalculateTranslation_Click);
			// 
			// lblDegree
			// 
			this.lblDegree.BackColor = System.Drawing.Color.Transparent;
			this.lblDegree.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblDegree.Location = new System.Drawing.Point(224, 100);
			this.lblDegree.Name = "lblDegree";
			this.lblDegree.Size = new System.Drawing.Size(64, 16);
			this.lblDegree.TabIndex = 13;
			this.lblDegree.Text = "in degree";
			this.lblDegree.Visible = false;
			// 
			// udRotation
			// 
			this.udRotation.DecimalPlaces = 2;
			this.udRotation.Location = new System.Drawing.Point(100, 96);
			this.udRotation.Maximum = new System.Decimal(new int[] {
																	   10000,
																	   0,
																	   0,
																	   0});
			this.udRotation.Name = "udRotation";
			this.udRotation.Size = new System.Drawing.Size(72, 20);
			this.udRotation.TabIndex = 12;
			this.toolTip1.SetToolTip(this.udRotation, "The rotation angle of this view point definition");
			this.udRotation.Visible = false;
			this.udRotation.ValueChanged += new System.EventHandler(this.txtName_TextChanged);
			this.udRotation.Leave += new System.EventHandler(this.txtName_TextChanged);
			// 
			// lblRotation
			// 
			this.lblRotation.BackColor = System.Drawing.Color.Transparent;
			this.lblRotation.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblRotation.Location = new System.Drawing.Point(20, 96);
			this.lblRotation.Name = "lblRotation";
			this.lblRotation.Size = new System.Drawing.Size(72, 16);
			this.lblRotation.TabIndex = 11;
			this.lblRotation.Text = "Rotation:";
			this.lblRotation.Visible = false;
			// 
			// udHeight
			// 
			this.udHeight.DecimalPlaces = 2;
			this.udHeight.Location = new System.Drawing.Point(300, 68);
			this.udHeight.Maximum = new System.Decimal(new int[] {
																	 268435456,
																	 1042612833,
																	 542101086,
																	 0});
			this.udHeight.Name = "udHeight";
			this.udHeight.Size = new System.Drawing.Size(72, 20);
			this.udHeight.TabIndex = 10;
			this.udHeight.Tag = "HEIG";
			this.toolTip1.SetToolTip(this.udHeight, "The height of this view point definition");
			this.udHeight.ValueChanged += new System.EventHandler(this.txtName_TextChanged);
			this.udHeight.Leave += new System.EventHandler(this.txtName_TextChanged);
			// 
			// udWidth
			// 
			this.udWidth.DecimalPlaces = 2;
			this.udWidth.Location = new System.Drawing.Point(300, 44);
			this.udWidth.Maximum = new System.Decimal(new int[] {
																	268435456,
																	1042612833,
																	542101086,
																	0});
			this.udWidth.Name = "udWidth";
			this.udWidth.Size = new System.Drawing.Size(72, 20);
			this.udWidth.TabIndex = 8;
			this.udWidth.Tag = "WIDT";
			this.toolTip1.SetToolTip(this.udWidth, "The width of this view point definition");
			this.udWidth.ValueChanged += new System.EventHandler(this.txtName_TextChanged);
			this.udWidth.Leave += new System.EventHandler(this.txtName_TextChanged);
			// 
			// lblHeight
			// 
			this.lblHeight.BackColor = System.Drawing.Color.Transparent;
			this.lblHeight.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblHeight.Location = new System.Drawing.Point(224, 72);
			this.lblHeight.Name = "lblHeight";
			this.lblHeight.Size = new System.Drawing.Size(72, 16);
			this.lblHeight.TabIndex = 9;
			this.lblHeight.Text = "Area height:";
			this.toolTip1.SetToolTip(this.lblHeight, "The height of this view point definition");
			// 
			// lblWidth
			// 
			this.lblWidth.BackColor = System.Drawing.Color.Transparent;
			this.lblWidth.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblWidth.Location = new System.Drawing.Point(224, 48);
			this.lblWidth.Name = "lblWidth";
			this.lblWidth.Size = new System.Drawing.Size(68, 16);
			this.lblWidth.TabIndex = 7;
			this.lblWidth.Text = "Area width:";
			// 
			// btnBrowse
			// 
			this.btnBrowse.BackColor = System.Drawing.Color.Transparent;
			this.btnBrowse.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnBrowse.Location = new System.Drawing.Point(408, 188);
			this.btnBrowse.Name = "btnBrowse";
			this.btnBrowse.TabIndex = 20;
			this.btnBrowse.Text = "&Browse ...";
			this.toolTip1.SetToolTip(this.btnBrowse, "Browse for the file which contains the image for this view point definition");
			this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
			// 
			// lblPath
			// 
			this.lblPath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.lblPath.BackColor = System.Drawing.Color.Transparent;
			this.lblPath.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblPath.Location = new System.Drawing.Point(24, 608);
			this.lblPath.Name = "lblPath";
			this.lblPath.Size = new System.Drawing.Size(36, 16);
			this.lblPath.TabIndex = 22;
			this.lblPath.Text = "Path:";
			// 
			// txtPath
			// 
			this.txtPath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.txtPath.Location = new System.Drawing.Point(64, 608);
			this.txtPath.Name = "txtPath";
			this.txtPath.ReadOnly = true;
			this.txtPath.Size = new System.Drawing.Size(336, 20);
			this.txtPath.TabIndex = 23;
			this.txtPath.Tag = "PATH";
			this.txtPath.Text = "C:\\";
			this.txtPath.TextChanged += new System.EventHandler(this.txtName_TextChanged);
			this.txtPath.Leave += new System.EventHandler(this.txtName_TextChanged);
			// 
			// lblPreview
			// 
			this.lblPreview.BackColor = System.Drawing.Color.Transparent;
			this.lblPreview.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblPreview.Location = new System.Drawing.Point(24, 188);
			this.lblPreview.Name = "lblPreview";
			this.lblPreview.Size = new System.Drawing.Size(200, 16);
			this.lblPreview.TabIndex = 19;
			this.lblPreview.Text = "Image Preview:";
			// 
			// lblFloor
			// 
			this.lblFloor.BackColor = System.Drawing.Color.Transparent;
			this.lblFloor.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblFloor.Location = new System.Drawing.Point(224, 152);
			this.lblFloor.Name = "lblFloor";
			this.lblFloor.Size = new System.Drawing.Size(48, 16);
			this.lblFloor.TabIndex = 17;
			this.lblFloor.Text = "Floor:";
			// 
			// lblBrace
			// 
			this.lblBrace.BackColor = System.Drawing.Color.Transparent;
			this.lblBrace.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblBrace.Location = new System.Drawing.Point(380, 43);
			this.lblBrace.Name = "lblBrace";
			this.lblBrace.Size = new System.Drawing.Size(24, 45);
			this.lblBrace.TabIndex = 31;
			this.lblBrace.Text = "}";
			// 
			// lblInMeters
			// 
			this.lblInMeters.BackColor = System.Drawing.Color.Transparent;
			this.lblInMeters.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblInMeters.Location = new System.Drawing.Point(412, 59);
			this.lblInMeters.Name = "lblInMeters";
			this.lblInMeters.Size = new System.Drawing.Size(64, 16);
			this.lblInMeters.TabIndex = 30;
			this.lblInMeters.Text = "in meters";
			// 
			// udYOffset
			// 
			this.udYOffset.DecimalPlaces = 2;
			this.udYOffset.Location = new System.Drawing.Point(100, 67);
			this.udYOffset.Maximum = new System.Decimal(new int[] {
																	  268435456,
																	  1042612833,
																	  542101086,
																	  0});
			this.udYOffset.Minimum = new System.Decimal(new int[] {
																	  268435456,
																	  1042612833,
																	  542101086,
																	  -2147483648});
			this.udYOffset.Name = "udYOffset";
			this.udYOffset.Size = new System.Drawing.Size(72, 20);
			this.udYOffset.TabIndex = 6;
			this.udYOffset.Tag = "YOFF";
			this.toolTip1.SetToolTip(this.udYOffset, "The offset in Y - direction from the origin of the world definition");
			this.udYOffset.ValueChanged += new System.EventHandler(this.txtName_TextChanged);
			this.udYOffset.Leave += new System.EventHandler(this.txtName_TextChanged);
			// 
			// udXOffset
			// 
			this.udXOffset.DecimalPlaces = 2;
			this.udXOffset.Location = new System.Drawing.Point(100, 43);
			this.udXOffset.Maximum = new System.Decimal(new int[] {
																	  268435456,
																	  1042612833,
																	  542101086,
																	  0});
			this.udXOffset.Minimum = new System.Decimal(new int[] {
																	  268435456,
																	  1042612833,
																	  542101086,
																	  -2147483648});
			this.udXOffset.Name = "udXOffset";
			this.udXOffset.Size = new System.Drawing.Size(72, 20);
			this.udXOffset.TabIndex = 4;
			this.udXOffset.Tag = "XOFF";
			this.toolTip1.SetToolTip(this.udXOffset, "The offset in X - direction from the origin of the world definition");
			this.udXOffset.ValueChanged += new System.EventHandler(this.txtName_TextChanged);
			this.udXOffset.Leave += new System.EventHandler(this.txtName_TextChanged);
			// 
			// lblYOffset
			// 
			this.lblYOffset.BackColor = System.Drawing.Color.Transparent;
			this.lblYOffset.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblYOffset.Location = new System.Drawing.Point(20, 71);
			this.lblYOffset.Name = "lblYOffset";
			this.lblYOffset.Size = new System.Drawing.Size(72, 16);
			this.lblYOffset.TabIndex = 5;
			this.lblYOffset.Text = "Y-Offset:";
			// 
			// lblXOffset
			// 
			this.lblXOffset.BackColor = System.Drawing.Color.Transparent;
			this.lblXOffset.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblXOffset.Location = new System.Drawing.Point(20, 47);
			this.lblXOffset.Name = "lblXOffset";
			this.lblXOffset.Size = new System.Drawing.Size(64, 16);
			this.lblXOffset.TabIndex = 3;
			this.lblXOffset.Text = "X-Offset:";
			// 
			// txtName
			// 
			this.txtName.Location = new System.Drawing.Point(100, 12);
			this.txtName.Name = "txtName";
			this.txtName.Size = new System.Drawing.Size(212, 20);
			this.txtName.TabIndex = 0;
			this.txtName.Tag = "NAME";
			this.txtName.Text = "";
			this.toolTip1.SetToolTip(this.txtName, "The name of the view point definition");
			this.txtName.TextChanged += new System.EventHandler(this.txtName_TextChanged);
			// 
			// lblName
			// 
			this.lblName.BackColor = System.Drawing.Color.Transparent;
			this.lblName.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblName.Location = new System.Drawing.Point(16, 12);
			this.lblName.Name = "lblName";
			this.lblName.Size = new System.Drawing.Size(52, 16);
			this.lblName.TabIndex = 1;
			this.lblName.Text = "Name:";
			// 
			// picBody
			// 
			this.picBody.Dock = System.Windows.Forms.DockStyle.Fill;
			this.picBody.Location = new System.Drawing.Point(0, 0);
			this.picBody.Name = "picBody";
			this.picBody.Size = new System.Drawing.Size(512, 642);
			this.picBody.TabIndex = 0;
			this.picBody.TabStop = false;
			this.picBody.Paint += new System.Windows.Forms.PaintEventHandler(this.picBody_Paint);
			// 
			// ViewpointDefinition
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 13);
			this.ClientSize = new System.Drawing.Size(516, 678);
			this.Controls.Add(this.panelBody);
			this.Controls.Add(this.panelTop);
			this.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "ViewpointDefinition";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "ViewpointDefinition";
			this.Load += new System.EventHandler(this.ViewpointDefinition_Load);
			this.panelTop.ResumeLayout(false);
			this.panelBody.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.udRotation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.udHeight)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.udWidth)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.udYOffset)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.udXOffset)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void picBody_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, picBody, Color.WhiteSmoke, Color.LightGray);		
		}

		private void picTop_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, picTop, Color.WhiteSmoke, Color.LightGray);		
		}


		private void ViewpointDefinition_Load(object sender, System.EventArgs e)
		{
			btnClose.Parent		= picTop;
			btnSave.Parent		= picTop;

			lblBrace.Parent		= picBody;
			lblInMeters.Parent	= picBody;
			lblName.Parent		= picBody;
			lblFloor.Parent		= picBody;
			lblPath.Parent		= picBody;
			lblPreview.Parent	= picBody;
			lblXOffset.Parent	= picBody;
			lblYOffset.Parent	= picBody;
			btnBrowse.Parent	= picBody;
			lblHeight.Parent	= picBody;
			lblWidth.Parent		= picBody;
			this.lblViewType.Parent	= picBody;
			this.lblRotation.Parent = picBody;
			this.lblDegree.Parent	= picBody;
			this.btnCalculateTranslation.Parent = picBody;
		}

		private void btnBrowse_Click(object sender, System.EventArgs e)
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Filter = "JPG-files (*.jpg)|*.jpg|TIF-filed (*.tif)|*.tif|bmp-files (*.bmp)|*.bmp|GIF-files (*.gif)|*.gif";
			dlg.InitialDirectory = frmData.GetThis().InitialDirectory;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				try
				{
					picPreview.Image = new Bitmap(dlg.FileName);
					txtPath.Text = dlg.FileName;
				}
				catch(Exception err)
				{
					string strMessage = "";
					strMessage += err.Message.ToString() + "\n\n";
					strMessage += "Bitmap file cannot be opened!!\n";
					strMessage += dlg.FileName + "\n\n";
					strMessage += "Please check the correctness of the specified path and the access rights to the specified location!!!\n";
					MessageBox.Show(this, strMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
					this.picPreview.Image = null;
					this.txtPath.Text	 = string.Empty;
				}

			}
		}

		private void btnCalculateTranslation_Click(object sender, System.EventArgs e)
		{
			this.btnCalculateTranslation.Enabled = false;
//			this.picPreview.SizeMode = PictureBoxSizeMode.AutoSize;
			this.picPreview.OnSelectPoints = new PictureViewer.PictureControlEx.SelectPointsHandler(this.SelectPoints);
			this.picPreview.BeginCalculateTransformation();
		}

		private void picPreview_OnEndTransformationHandler(System.Drawing.PointF origin, System.Drawing.SizeF size)
		{
			RectangleF rect = new RectangleF(origin,size);
			if (frmData.GetThis().IsValidViewpoint(rect))
			{
				this.udXOffset.Value = (decimal)origin.X;
				this.udYOffset.Value = (decimal)origin.Y;

				this.udHeight.Value	 = (decimal)size.Height;
				this.udWidth.Value	 = (decimal)size.Width;

				this.btnCalculateTranslation.Enabled = true;
			}
			else
			{
				MessageBox.Show(this,"Viewpoint definition is outside of the world definition!\nPlease check the correctness of the input parameters!");				
				this.btnCalculateTranslation.Enabled = true;
			}
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			if (this.SaveViewpoint())
			{
				this.DialogResult = DialogResult.OK;
				this.Close();
			}
		}

		private bool OpenViewpoint(string name)
		{
			if (btnSave.BackColor == Color.Red )
			{
				DialogResult olResult;
				olResult = MessageBox.Show(this, "Do you want to save the current viewpoint?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question); 
				if (olResult == DialogResult.Yes)
				{
					if (SaveViewpoint() == false) 
						return false;
				}
			}

			this.ClearControls(this);

			IDatabase myDB = UT.GetMemDB();
			ITable	myTable= myDB["AVP"];
			IRow[]	myRows = myTable.RowsByIndexValue("NAME",name);
			if (myRows != null && myRows.Length == 1)
			{
				string [] arrFields = "NAME,PATH,XOFF,YOFF,WIDT,HEIG,FLOR,FOVE".Split(',');
				for(int i = 0; i < arrFields.Length; i++)					
				{
					Control ctl = null;
					ctl = GetControlByTag(arrFields[i], this, ref ctl);
					if (ctl != null)
					{
						string strText = myRows[0][arrFields[i]];
						strText = strText.Trim();

						string lType = ctl.GetType().ToString();
						if (lType == "System.Windows.Forms.NumericUpDown")
						{
							NumericUpDown udCtrl = (NumericUpDown)ctl;
							udCtrl.Value = decimal.Parse(strText,NumberStyles.Number,frmData.NFI);
						}
						else if (lType == "System.Windows.Forms.ComboBox")
						{
							ComboBox udCtrl = (ComboBox)ctl;
							switch(strText)
							{
								case "1" :
									udCtrl.Text = "Floor";
									break;
								case "2" :
									udCtrl.Text = "Overview";
									break;
								default:
									udCtrl.Text = "Detail";
									break;
							}
						}
						else
						{
							ctl.Text = frmData.GetThis().Clean(strText,true);
						}
					}
				}

				currDBRow = myRows[0];
				currRow	  = currDBRow.CopyRaw();
				oldRow	  = currRow.CopyRaw();

				btnSave.BackColor = Color.Transparent;
				this.myMode = "UPDATE";

				return true;
			}
			else
			{
				return false;
			}
			
		}

		void NewViewpoint()
		{
			this.btnCalculateTranslation.Enabled = false;

			if(btnSave.BackColor == Color.Red )
			{
				DialogResult olResult;
				olResult = MessageBox.Show(this, "Do you want to save the current viewpoint?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question); 
				if ( olResult == DialogResult.Yes)
				{
					if (SaveViewpoint() == false) 
						return;
				}
			}

			IDatabase myDB = UT.GetMemDB();
			ITable	myTable= myDB["AVP"];

			this.currDBRow = myTable.CreateEmptyRow();
			this.currDBRow["XOFF"] = this.udXOffset.Value.ToString("F",frmData.NFI);
			this.currDBRow["YOFF"] = this.udYOffset.Value.ToString("F",frmData.NFI);
			this.currDBRow["WIDT"] = this.udWidth.Value.ToString("F",frmData.NFI);
			this.currDBRow["HEIG"] = this.udHeight.Value.ToString("F",frmData.NFI);
			this.currDBRow["FLOR"] = this.txtFloor.Text;
			this.currRow   = currDBRow.CopyRaw();
			this.oldRow    = currRow.CopyRaw();
			this.myMode	   = "NEW";

			ClearControls(this);
		}

		private bool SaveViewpoint()
		{
			if (btnSave.BackColor == Color.Transparent )
			{
				MessageBox.Show(this, "Nothing to save!!", "Info");
				return true;
			}

			string errMsg;
			if (!this.Validate(out errMsg))
			{
				MessageBox.Show(this,errMsg, "Error");
				return false;
			}

			IDatabase myDB = UT.GetMemDB();
			ITable	myTable= myDB["AVP"];	
			int lineNo = -1;

			if (this.myMode == "NEW")
			{
				string strTime = UT.DateTimeToCeda(DateTime.Now);
				currRow["CDAT"] = strTime;
				currRow["USEC"] = UT.UserName;
				currDBRow.Status = State.Created;
			}
			else if (this.myMode == "UPDATE")
			{
				string strTime = UT.DateTimeToCeda( DateTime.Now);
				currRow["LSTU"] = strTime;
				currRow["USEU"] = UT.UserName;
				currDBRow.Status = State.Modified;
			}

			ArrayList arrCurr = currRow.FieldValueList();
			for(int i = 0; i < arrCurr.Count; i++)
			{
				currDBRow[i] = currRow[i];
				oldRow[i]	 = currRow[i]; //To remember the current values, because we are in updatemode from now on.
			}

			// must copy values first before adding the row to the database, else indexes will be invalid because values are empty
			if (this.myMode == "NEW")
			{
				lineNo = myTable.Add(currDBRow);
			}

			if (myTable.Save())
			{
				myMode = "UPDATE";
				this.btnSave.BackColor = Color.Transparent;
				return true;
			}
			else
			{
				if (lineNo >= 0 && this.myMode == "NEW")
				{
					myTable.Remove(lineNo);
				}

				return false;
			}

		}

		private bool CheckDataChanged()
		{
			bool changed = false;
			ArrayList arrCurr = currRow.FieldValueList();
			ArrayList arrOld  = oldRow.FieldValueList();
			for(int i = 0; i < arrCurr.Count && changed == false; i++)
			{
				if(arrCurr[i].ToString() != arrOld[i].ToString())
				{
					changed = true;
				}
			}

			if (changed)
			{
				this.btnSave.BackColor = Color.Red;
			}
			else
			{
				this.btnSave.BackColor = Color.Transparent;
			}

			return changed;
		}

		private bool Validate(out string errMsg)
		{
			bool result = true;
			errMsg = string.Empty;

			this.txtName.Text = this.txtName.Text.Trim();
			if (this.txtName.Text.Length == 0)
			{
				errMsg += "Viewpoint name is mandatory\n";
				result = false;
			}
			else
			{
				IDatabase myDB = UT.GetMemDB();
				ITable	myTable= myDB["AVP"];	
				IRow[]	myRows = myTable.RowsByIndexValue("NAME",this.txtName.Text);
				if (myRows != null)
				{
					for (int i = 0; i < myRows.Length; i++)
					{
						if (myRows[i]["URNO"] != this.currDBRow["URNO"])
						{
							errMsg += "Viewpoint name already exists\n";
							result = false;
						}
					}
				}
			}

			if (this.txtPath.Text.Length == 0)
			{
				errMsg += "Image path is mandatory\n";
				result = false;
			}

			if (this.udHeight.Value == 0)
			{
				errMsg += "Height value must not be null\n";
				result = false;
			}

			if (this.udWidth.Value == 0)
			{
				errMsg += "Width value must not be null\n";
				result = false;
			}

			if (result == true)
			{
				RectangleF rect = new RectangleF(new PointF((float)this.udXOffset.Value,(float)this.udYOffset.Value),new SizeF((float)this.udWidth.Value,(float)this.udHeight.Value));
				if (frmData.GetThis().IsValidViewpoint(rect) == false)
				{
					errMsg += "Viewpoint definition is outside of the world definition!\nPlease check the correctness of the input parameters!\n";				
				}
			}

			return result;
		}

		void ClearControls(System.Windows.Forms.Control pControl)
		{
			foreach(System.Windows.Forms.Control olC in pControl.Controls )
			{
				string lType = olC.GetType().ToString();
				if (lType == "System.Windows.Forms.PictureBox" || lType == "System.Windows.Forms.Panel")
				{
					ClearControls(olC);
				}
				else if (lType == "System.Windows.Forms.TextBox")
				{
					if (olC.Tag != null && olC.Tag.ToString().Length > 0)
					{
						olC.Text = "";
					}
				}
				else if (lType == "System.Windows.Forms.NumericUpDown")
				{
					if (olC.Tag != null)
					{
						NumericUpDown udCtrl = (NumericUpDown)olC;
						udCtrl.Value = udCtrl.Value;
					}
				}
				else if (lType == "System.Windows.Forms.ComboBox")
				{
					if (olC.Tag != null)
					{
						ComboBox udCtrl = (ComboBox)olC;
						udCtrl.Text  = "Detail";
					}
				}
			}
		}

		private Control GetControlByTag(string tag, Control currCtl, ref Control myControl )
		{
			if(myControl == null)
			{
				foreach(System.Windows.Forms.Control olC in currCtl.Controls )
				{
					string lType = olC.GetType().ToString();
					if(lType == "System.Windows.Forms.PictureBox" || lType == "System.Windows.Forms.Panel")
					{
						if(olC.Tag != null)
							if(olC.Tag.ToString() != tag)
								myControl = GetControlByTag(tag.ToString(), olC, ref myControl);
							else
								myControl = olC;
						else
							myControl = GetControlByTag(tag.ToString(), olC, ref myControl);
					}
					else
					{
						if(olC.Tag != null)
						{
							if (olC.Tag.ToString() == tag)
								myControl = olC;
						}
					}
				}
			}
			return myControl;
		}

		private void txtName_TextChanged(object sender, System.EventArgs e)
		{
			Control ctl = (Control)sender;
			if (ctl.Tag != null)
			{
				string lType = ctl.GetType().ToString();
				if (lType == "System.Windows.Forms.NumericUpDown")
				{
					NumericUpDown udCtrl = (NumericUpDown)ctl;
					if (udCtrl.DecimalPlaces == 0)
					{
						currRow[ctl.Tag.ToString()] = udCtrl.Value.ToString();
					}
					else
					{
						currRow[ctl.Tag.ToString()] = udCtrl.Value.ToString("F",frmData.NFI);
					}
				}
				else if (lType == "System.Windows.Forms.ComboBox")
				{
					ComboBox udCtrl = (ComboBox)ctl;
					switch(udCtrl.Text)
					{
						case "Detail" :
							currRow[ctl.Tag.ToString()] = "";
							break;
						case "Floor" :
							currRow[ctl.Tag.ToString()] = "1";
							break;
						case "Overview" :
							currRow[ctl.Tag.ToString()] = "2";
							break;
					}

				}
				else
				{
					currRow[ctl.Tag.ToString()] =  frmData.GetThis().Clean(ctl.Text,false);
				}

				CheckDataChanged();
			}
		}

		private void txtPath_TextChanged(object sender, EventArgs e)
		{
			try
			{
				if (this.txtPath.Text.Length > 0)
				{
					try
					{
						this.picPreview.Image = new Bitmap(this.txtPath.Text);
						this.btnCalculateTranslation.Enabled = true;
					}
					catch(Exception err)
					{
						string strMessage = "";
						strMessage += err.Message.ToString() + "\n\n";
						strMessage += "Bitmap file cannot be opened!!\n";
						strMessage += this.txtPath.Text + "\n\n";
						strMessage += "Please check the correctness of the specified path and the access rights to the specified location!!!\n";
						MessageBox.Show(this, strMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
						this.picPreview.Image = null;
						this.txtPath.Text	 = string.Empty;
					}

				}
				else
				{
					this.picPreview.Image = null;
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show(this,ex.Message,"Error");
			}

		}

		private void cbViewType_SelectedValueChanged(object sender, System.EventArgs e)
		{
			if (this.cbViewType.Text == "Overview")
			{
				this.lblFloor.Visible = false;
				this.txtFloor.Visible = false;
			}
			else
			{
				this.lblFloor.Visible = true;
				this.txtFloor.Visible = true;
			}

			txtName_TextChanged(sender,e);
		}

		private void picPreview_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, picPreview, Color.WhiteSmoke, Color.LightGray);		
		}

		private void picPreview_OnCancelTransformationHandler()
		{
			this.picPreview.ClearCommandStack();
			this.btnCalculateTranslation.Enabled = true;
		}

		public string Urno
		{
			get
			{
				return this.currDBRow["URNO"];
			}
		}

		private System.Windows.Forms.Form SelectPoints()
		{
			return new SelectPoints();
		}
	}
}
