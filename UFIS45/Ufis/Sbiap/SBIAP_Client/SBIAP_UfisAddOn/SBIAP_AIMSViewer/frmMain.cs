using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Globalization;
using System.Diagnostics.SymbolStore;
using System.Reflection;
using Microsoft.Win32;

using Ufis.Utils;
using Ufis.Data;
using PictureViewer;

namespace AIMSViewer
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class frmMain : System.Windows.Forms.Form
	{
		#region MY_MEMBERS
		StatusOverview frmOverview = null;

		private frmData		DataForm;
		private frmStartup	StartForm;
		private	string[]	args;
		private	SortedList	viewpoints;
		private	DataExchange dataExchange;
		private bool		init = false;
		private	bool		OverviewAsPage = false;
		private	TabPage		OverviewPage = null;
		private	string		helpPath = string.Empty;
		private	string		helpFile = string.Empty;

		#endregion MY_MEMBERS

		private System.Windows.Forms.ContextMenu contextMenu1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.MenuItem menuItem4;
		private System.Windows.Forms.MenuItem menuItem5;
		private System.ComponentModel.IContainer components;
		private IDatabase myDB = null;
		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.PictureBox picTop;
		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.Panel panelOverviewLabel;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Panel panelTab;
		private System.Windows.Forms.Label labelViewpoints;
		private System.Windows.Forms.Button btnNewViewPoint;
		private System.Windows.Forms.Button btnEditViewPoint;
		private System.Windows.Forms.Button btnDeleteViewPoint;
		private System.Windows.Forms.Button btnGeneralSettings;
		private System.Windows.Forms.TabControl tabFloors;
		private System.Windows.Forms.CheckBox cbOpen;
		private System.Windows.Forms.Button btnSymbolGroups;
		private System.Windows.Forms.Button btnOpenViewPoint;
		private System.Windows.Forms.ComboBox cbViewpoints;
		private AxAATLOGINLib.AxAatLogin axAatLogin1;
		private System.Timers.Timer timerCloseStartup;
		private System.Windows.Forms.Button btnCameras;
		private System.Windows.Forms.Button btnMessages;
		private Ufis.Utils.BCBlinker bcBlinker1;
		private System.Windows.Forms.Button btnAbout;
		private System.Windows.Forms.Button btnView;
		private System.Windows.Forms.Label lblLocalTime;
		private System.Windows.Forms.Label lblUTCTime;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.ImageList imageList1;
		private Screen[] screens = Screen.AllScreens;
		private System.Windows.Forms.Button btnHelp;
		private System.Windows.Forms.Button btnCoordinates;
		private Screen	 primary = Screen.PrimaryScreen; 

		public frmMain(string[] args)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			this.args = args;
			this.axAatLogin1.Hide();

			this.viewpoints = new SortedList();
			//Read ceda.ini and set it global to UT (UfisTools)
			String strServer="";
			String strHopo = "";
			Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\ceda.ini");
			strServer = myIni.IniReadValue("GLOBAL", "HOSTNAME");
			strHopo  = myIni.IniReadValue("GLOBAL", "HOMEAIRPORT");
			helpPath = myIni.IniReadValue("Global","HelpDirectory");
			helpFile = myIni.IniReadValue("AIMSViewer","HelpFile");
			if (helpFile.Length == 0)
			{
				this.btnHelp.Visible = false;
				this.btnHelp.Enabled = false;
			}

			UT.ServerName = strServer;
			UT.Hopo = strHopo;

			String strOverviewAsPage = myIni.IniReadValue("AIMSViewer","OverviewAsPage");
			strOverviewAsPage = strOverviewAsPage.ToUpper();
			if (strOverviewAsPage == "YES" || strOverviewAsPage == "TRUE")
			{
				this.OverviewAsPage = true;
			}
			
			if (screens.Length > 1)
			{
				this.StartPosition = FormStartPosition.Manual;
				this.Location = new Point(primary.WorkingArea.Left,primary.WorkingArea.Top);
				this.Size = new Size(primary.WorkingArea.Width,primary.WorkingArea.Height);		
			}

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (DataForm != null)
				{
					DataForm.Dispose();
				}

				if (components != null) 
				{
					components.Dispose();
				}

				UT.DisposeMemDB();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmMain));
			this.contextMenu1 = new System.Windows.Forms.ContextMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.menuItem3 = new System.Windows.Forms.MenuItem();
			this.menuItem4 = new System.Windows.Forms.MenuItem();
			this.menuItem5 = new System.Windows.Forms.MenuItem();
			this.panelTop = new System.Windows.Forms.Panel();
			this.btnHelp = new System.Windows.Forms.Button();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.lblLocalTime = new System.Windows.Forms.Label();
			this.lblUTCTime = new System.Windows.Forms.Label();
			this.btnView = new System.Windows.Forms.Button();
			this.btnAbout = new System.Windows.Forms.Button();
			this.bcBlinker1 = new Ufis.Utils.BCBlinker();
			this.btnCameras = new System.Windows.Forms.Button();
			this.btnSymbolGroups = new System.Windows.Forms.Button();
			this.btnMessages = new System.Windows.Forms.Button();
			this.cbOpen = new System.Windows.Forms.CheckBox();
			this.btnGeneralSettings = new System.Windows.Forms.Button();
			this.btnDeleteViewPoint = new System.Windows.Forms.Button();
			this.btnEditViewPoint = new System.Windows.Forms.Button();
			this.btnNewViewPoint = new System.Windows.Forms.Button();
			this.btnOpenViewPoint = new System.Windows.Forms.Button();
			this.cbViewpoints = new System.Windows.Forms.ComboBox();
			this.labelViewpoints = new System.Windows.Forms.Label();
			this.picTop = new System.Windows.Forms.PictureBox();
			this.axAatLogin1 = new AxAATLOGINLib.AxAatLogin();
			this.panelBody = new System.Windows.Forms.Panel();
			this.panelTab = new System.Windows.Forms.Panel();
			this.tabFloors = new System.Windows.Forms.TabControl();
			this.panelOverviewLabel = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.timerCloseStartup = new System.Timers.Timer();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.btnCoordinates = new System.Windows.Forms.Button();
			this.panelTop.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.axAatLogin1)).BeginInit();
			this.panelBody.SuspendLayout();
			this.panelTab.SuspendLayout();
			this.panelOverviewLabel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.timerCloseStartup)).BeginInit();
			this.SuspendLayout();
			// 
			// contextMenu1
			// 
			this.contextMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						 this.menuItem1,
																						 this.menuItem2,
																						 this.menuItem3,
																						 this.menuItem4,
																						 this.menuItem5});
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.Text = "Hallo";
			// 
			// menuItem2
			// 
			this.menuItem2.Index = 1;
			this.menuItem2.Text = "ich ";
			// 
			// menuItem3
			// 
			this.menuItem3.Index = 2;
			this.menuItem3.Text = "bin";
			// 
			// menuItem4
			// 
			this.menuItem4.Index = 3;
			this.menuItem4.Text = "ein";
			// 
			// menuItem5
			// 
			this.menuItem5.Index = 4;
			this.menuItem5.Text = "menu";
			// 
			// panelTop
			// 
			this.panelTop.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelTop.Controls.Add(this.btnCoordinates);
			this.panelTop.Controls.Add(this.btnHelp);
			this.panelTop.Controls.Add(this.lblLocalTime);
			this.panelTop.Controls.Add(this.lblUTCTime);
			this.panelTop.Controls.Add(this.btnView);
			this.panelTop.Controls.Add(this.btnAbout);
			this.panelTop.Controls.Add(this.bcBlinker1);
			this.panelTop.Controls.Add(this.btnCameras);
			this.panelTop.Controls.Add(this.btnSymbolGroups);
			this.panelTop.Controls.Add(this.btnMessages);
			this.panelTop.Controls.Add(this.cbOpen);
			this.panelTop.Controls.Add(this.btnGeneralSettings);
			this.panelTop.Controls.Add(this.btnDeleteViewPoint);
			this.panelTop.Controls.Add(this.btnEditViewPoint);
			this.panelTop.Controls.Add(this.btnNewViewPoint);
			this.panelTop.Controls.Add(this.btnOpenViewPoint);
			this.panelTop.Controls.Add(this.cbViewpoints);
			this.panelTop.Controls.Add(this.labelViewpoints);
			this.panelTop.Controls.Add(this.picTop);
			this.panelTop.Controls.Add(this.axAatLogin1);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(0, 0);
			this.panelTop.Name = "panelTop";
			this.panelTop.Size = new System.Drawing.Size(996, 64);
			this.panelTop.TabIndex = 0;
			// 
			// btnHelp
			// 
			this.btnHelp.BackColor = System.Drawing.Color.Transparent;
			this.btnHelp.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnHelp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnHelp.ImageList = this.imageList1;
			this.btnHelp.Location = new System.Drawing.Point(640, 0);
			this.btnHelp.Name = "btnHelp";
			this.btnHelp.Size = new System.Drawing.Size(90, 28);
			this.btnHelp.TabIndex = 44;
			this.btnHelp.Text = "&Help";
			this.toolTip1.SetToolTip(this.btnHelp, "Display online help information about the AIMS Viewer");
			this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
			// 
			// imageList1
			// 
			this.imageList1.ImageSize = new System.Drawing.Size(19, 19);
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.White;
			// 
			// lblLocalTime
			// 
			this.lblLocalTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.lblLocalTime.BackColor = System.Drawing.Color.Silver;
			this.lblLocalTime.Location = new System.Drawing.Point(876, 40);
			this.lblLocalTime.Name = "lblLocalTime";
			this.lblLocalTime.Size = new System.Drawing.Size(120, 23);
			this.lblLocalTime.TabIndex = 43;
			this.lblLocalTime.Text = "24.10.2003 10:10";
			this.toolTip1.SetToolTip(this.lblLocalTime, "The current time in local format");
			// 
			// lblUTCTime
			// 
			this.lblUTCTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.lblUTCTime.BackColor = System.Drawing.Color.Silver;
			this.lblUTCTime.ForeColor = System.Drawing.Color.Red;
			this.lblUTCTime.Location = new System.Drawing.Point(752, 40);
			this.lblUTCTime.Name = "lblUTCTime";
			this.lblUTCTime.Size = new System.Drawing.Size(120, 23);
			this.lblUTCTime.TabIndex = 42;
			this.lblUTCTime.Text = "24.10.2003 10:10z";
			this.toolTip1.SetToolTip(this.lblUTCTime, "The current time in UTC format");
			// 
			// btnView
			// 
			this.btnView.BackColor = System.Drawing.Color.Transparent;
			this.btnView.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnView.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnView.ImageIndex = 10;
			this.btnView.ImageList = this.imageList1;
			this.btnView.Location = new System.Drawing.Point(544, 32);
			this.btnView.Name = "btnView";
			this.btnView.Size = new System.Drawing.Size(90, 28);
			this.btnView.TabIndex = 41;
			this.btnView.Text = " &View";
			this.toolTip1.SetToolTip(this.btnView, "View, create, modify or delete view definitions");
			this.btnView.Click += new System.EventHandler(this.btnView_Click);
			// 
			// btnAbout
			// 
			this.btnAbout.BackColor = System.Drawing.Color.Transparent;
			this.btnAbout.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnAbout.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnAbout.ImageIndex = 2;
			this.btnAbout.ImageList = this.imageList1;
			this.btnAbout.Location = new System.Drawing.Point(544, 0);
			this.btnAbout.Name = "btnAbout";
			this.btnAbout.Size = new System.Drawing.Size(90, 28);
			this.btnAbout.TabIndex = 40;
			this.btnAbout.Text = " &About";
			this.toolTip1.SetToolTip(this.btnAbout, "About this application ...");
			this.btnAbout.Click += new System.EventHandler(this.btnAbout_Click);
			// 
			// bcBlinker1
			// 
			this.bcBlinker1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.bcBlinker1.BackColor = System.Drawing.SystemColors.Control;
			this.bcBlinker1.Location = new System.Drawing.Point(936, 0);
			this.bcBlinker1.Name = "bcBlinker1";
			this.bcBlinker1.Size = new System.Drawing.Size(44, 24);
			this.bcBlinker1.TabIndex = 39;
			this.bcBlinker1.Paint += new System.Windows.Forms.PaintEventHandler(this.bcBlinker1_Paint);
			// 
			// btnCameras
			// 
			this.btnCameras.BackColor = System.Drawing.Color.Transparent;
			this.btnCameras.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnCameras.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnCameras.ImageIndex = 17;
			this.btnCameras.ImageList = this.imageList1;
			this.btnCameras.Location = new System.Drawing.Point(448, 32);
			this.btnCameras.Name = "btnCameras";
			this.btnCameras.Size = new System.Drawing.Size(90, 28);
			this.btnCameras.TabIndex = 38;
			this.btnCameras.Text = "   &Cameras";
			this.toolTip1.SetToolTip(this.btnCameras, "View, create, modify or delete CCTV camera definitions");
			this.btnCameras.Click += new System.EventHandler(this.btnCameras_Click);
			// 
			// btnSymbolGroups
			// 
			this.btnSymbolGroups.BackColor = System.Drawing.Color.Transparent;
			this.btnSymbolGroups.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnSymbolGroups.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnSymbolGroups.ImageIndex = 15;
			this.btnSymbolGroups.ImageList = this.imageList1;
			this.btnSymbolGroups.Location = new System.Drawing.Point(448, 0);
			this.btnSymbolGroups.Name = "btnSymbolGroups";
			this.btnSymbolGroups.Size = new System.Drawing.Size(90, 28);
			this.btnSymbolGroups.TabIndex = 37;
			this.btnSymbolGroups.Text = "&Groups";
			this.toolTip1.SetToolTip(this.btnSymbolGroups, "View, create, modify or delete status group definitions");
			this.btnSymbolGroups.Click += new System.EventHandler(this.btnSymbolGroups_Click);
			// 
			// btnMessages
			// 
			this.btnMessages.BackColor = System.Drawing.Color.Transparent;
			this.btnMessages.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnMessages.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnMessages.ImageIndex = 8;
			this.btnMessages.ImageList = this.imageList1;
			this.btnMessages.Location = new System.Drawing.Point(352, 0);
			this.btnMessages.Name = "btnMessages";
			this.btnMessages.Size = new System.Drawing.Size(90, 28);
			this.btnMessages.TabIndex = 35;
			this.btnMessages.Text = "&Messages";
			this.toolTip1.SetToolTip(this.btnMessages, "View, create, modify or delete status message definitions");
			this.btnMessages.Click += new System.EventHandler(this.btnMessages_Click);
			// 
			// cbOpen
			// 
			this.cbOpen.Appearance = System.Windows.Forms.Appearance.Button;
			this.cbOpen.BackColor = System.Drawing.Color.Transparent;
			this.cbOpen.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cbOpen.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.cbOpen.ImageIndex = 3;
			this.cbOpen.ImageList = this.imageList1;
			this.cbOpen.Location = new System.Drawing.Point(352, 32);
			this.cbOpen.Name = "cbOpen";
			this.cbOpen.Size = new System.Drawing.Size(90, 28);
			this.cbOpen.TabIndex = 34;
			this.cbOpen.Text = "   &Overview";
			this.cbOpen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.toolTip1.SetToolTip(this.cbOpen, "Display or hide the status overview list");
			this.cbOpen.CheckedChanged += new System.EventHandler(this.cbOpen_CheckedChanged);
			// 
			// btnGeneralSettings
			// 
			this.btnGeneralSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnGeneralSettings.BackColor = System.Drawing.Color.Transparent;
			this.btnGeneralSettings.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnGeneralSettings.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnGeneralSettings.ImageIndex = 11;
			this.btnGeneralSettings.ImageList = this.imageList1;
			this.btnGeneralSettings.Location = new System.Drawing.Point(800, 0);
			this.btnGeneralSettings.Name = "btnGeneralSettings";
			this.btnGeneralSettings.Size = new System.Drawing.Size(128, 28);
			this.btnGeneralSettings.TabIndex = 33;
			this.btnGeneralSettings.Text = "&General Settings";
			this.btnGeneralSettings.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.toolTip1.SetToolTip(this.btnGeneralSettings, "View,create, modify or delete general settings");
			this.btnGeneralSettings.Click += new System.EventHandler(this.btnGeneralSettings_Click);
			// 
			// btnDeleteViewPoint
			// 
			this.btnDeleteViewPoint.BackColor = System.Drawing.Color.Transparent;
			this.btnDeleteViewPoint.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnDeleteViewPoint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnDeleteViewPoint.ImageIndex = 5;
			this.btnDeleteViewPoint.ImageList = this.imageList1;
			this.btnDeleteViewPoint.Location = new System.Drawing.Point(256, 32);
			this.btnDeleteViewPoint.Name = "btnDeleteViewPoint";
			this.btnDeleteViewPoint.Size = new System.Drawing.Size(64, 28);
			this.btnDeleteViewPoint.TabIndex = 32;
			this.btnDeleteViewPoint.Text = "&Delete";
			this.btnDeleteViewPoint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.toolTip1.SetToolTip(this.btnDeleteViewPoint, "Delete an already existing view point");
			this.btnDeleteViewPoint.Click += new System.EventHandler(this.btnDeleteViewPoint_Click);
			// 
			// btnEditViewPoint
			// 
			this.btnEditViewPoint.BackColor = System.Drawing.Color.Transparent;
			this.btnEditViewPoint.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnEditViewPoint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnEditViewPoint.ImageIndex = 6;
			this.btnEditViewPoint.ImageList = this.imageList1;
			this.btnEditViewPoint.Location = new System.Drawing.Point(192, 32);
			this.btnEditViewPoint.Name = "btnEditViewPoint";
			this.btnEditViewPoint.Size = new System.Drawing.Size(64, 28);
			this.btnEditViewPoint.TabIndex = 31;
			this.btnEditViewPoint.Text = "   &Edit";
			this.toolTip1.SetToolTip(this.btnEditViewPoint, "Modify an already existing view point");
			this.btnEditViewPoint.Click += new System.EventHandler(this.btnEditViewPoint_Click);
			// 
			// btnNewViewPoint
			// 
			this.btnNewViewPoint.BackColor = System.Drawing.Color.Transparent;
			this.btnNewViewPoint.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnNewViewPoint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnNewViewPoint.ImageIndex = 7;
			this.btnNewViewPoint.ImageList = this.imageList1;
			this.btnNewViewPoint.Location = new System.Drawing.Point(136, 32);
			this.btnNewViewPoint.Name = "btnNewViewPoint";
			this.btnNewViewPoint.Size = new System.Drawing.Size(56, 28);
			this.btnNewViewPoint.TabIndex = 30;
			this.btnNewViewPoint.Text = "New";
			this.btnNewViewPoint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.toolTip1.SetToolTip(this.btnNewViewPoint, "Create a new view point");
			this.btnNewViewPoint.Click += new System.EventHandler(this.btnNewViewPoint_Click);
			// 
			// btnOpenViewPoint
			// 
			this.btnOpenViewPoint.BackColor = System.Drawing.Color.Transparent;
			this.btnOpenViewPoint.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnOpenViewPoint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnOpenViewPoint.ImageIndex = 14;
			this.btnOpenViewPoint.ImageList = this.imageList1;
			this.btnOpenViewPoint.Location = new System.Drawing.Point(72, 32);
			this.btnOpenViewPoint.Name = "btnOpenViewPoint";
			this.btnOpenViewPoint.Size = new System.Drawing.Size(64, 28);
			this.btnOpenViewPoint.TabIndex = 29;
			this.btnOpenViewPoint.Text = "Open";
			this.btnOpenViewPoint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.toolTip1.SetToolTip(this.btnOpenViewPoint, "Open an already existing view point");
			this.btnOpenViewPoint.Click += new System.EventHandler(this.btnOpenViewPoint_Click);
			// 
			// cbViewpoints
			// 
			this.cbViewpoints.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cbViewpoints.Location = new System.Drawing.Point(72, 4);
			this.cbViewpoints.Name = "cbViewpoints";
			this.cbViewpoints.Size = new System.Drawing.Size(248, 22);
			this.cbViewpoints.TabIndex = 2;
			this.toolTip1.SetToolTip(this.cbViewpoints, "Select an already existing view point for opening, modyfing or delete ");
			this.cbViewpoints.TextChanged += new System.EventHandler(this.cbViewpoints_TextChanged);
			this.cbViewpoints.SelectedIndexChanged += new System.EventHandler(this.cbViewpoints_SelectedIndexChanged);
			// 
			// labelViewpoints
			// 
			this.labelViewpoints.BackColor = System.Drawing.Color.Transparent;
			this.labelViewpoints.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.labelViewpoints.Location = new System.Drawing.Point(0, 8);
			this.labelViewpoints.Name = "labelViewpoints";
			this.labelViewpoints.Size = new System.Drawing.Size(72, 16);
			this.labelViewpoints.TabIndex = 1;
			this.labelViewpoints.Text = "Viewpoints:";
			// 
			// picTop
			// 
			this.picTop.Dock = System.Windows.Forms.DockStyle.Fill;
			this.picTop.Location = new System.Drawing.Point(0, 0);
			this.picTop.Name = "picTop";
			this.picTop.Size = new System.Drawing.Size(992, 60);
			this.picTop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.picTop.TabIndex = 0;
			this.picTop.TabStop = false;
			this.picTop.Paint += new System.Windows.Forms.PaintEventHandler(this.picTop_Paint);
			// 
			// axAatLogin1
			// 
			this.axAatLogin1.ContainingControl = this;
			this.axAatLogin1.Enabled = true;
			this.axAatLogin1.Location = new System.Drawing.Point(592, 0);
			this.axAatLogin1.Name = "axAatLogin1";
			this.axAatLogin1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axAatLogin1.OcxState")));
			this.axAatLogin1.Size = new System.Drawing.Size(100, 50);
			this.axAatLogin1.TabIndex = 0;
			this.axAatLogin1.Visible = false;
			// 
			// panelBody
			// 
			this.panelBody.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelBody.Controls.Add(this.panelTab);
			this.panelBody.Controls.Add(this.panelOverviewLabel);
			this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelBody.Location = new System.Drawing.Point(0, 64);
			this.panelBody.Name = "panelBody";
			this.panelBody.Size = new System.Drawing.Size(996, 406);
			this.panelBody.TabIndex = 1;
			// 
			// panelTab
			// 
			this.panelTab.Controls.Add(this.tabFloors);
			this.panelTab.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelTab.Location = new System.Drawing.Point(0, 16);
			this.panelTab.Name = "panelTab";
			this.panelTab.Size = new System.Drawing.Size(992, 386);
			this.panelTab.TabIndex = 1;
			// 
			// tabFloors
			// 
			this.tabFloors.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabFloors.Location = new System.Drawing.Point(0, 0);
			this.tabFloors.Name = "tabFloors";
			this.tabFloors.SelectedIndex = 0;
			this.tabFloors.Size = new System.Drawing.Size(992, 386);
			this.tabFloors.TabIndex = 0;
			this.tabFloors.SelectedIndexChanged += new System.EventHandler(this.tabFloors_SelectedIndexChanged);
			// 
			// panelOverviewLabel
			// 
			this.panelOverviewLabel.Controls.Add(this.label1);
			this.panelOverviewLabel.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelOverviewLabel.Location = new System.Drawing.Point(0, 0);
			this.panelOverviewLabel.Name = "panelOverviewLabel";
			this.panelOverviewLabel.Size = new System.Drawing.Size(992, 16);
			this.panelOverviewLabel.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.Black;
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.Lime;
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(992, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Status and Event-Overview";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// timerCloseStartup
			// 
			this.timerCloseStartup.SynchronizingObject = this;
			this.timerCloseStartup.Elapsed += new System.Timers.ElapsedEventHandler(this.timerCloseStartup_Elapsed);
			// 
			// timer1
			// 
			this.timer1.Enabled = true;
			this.timer1.Interval = 60000;
			this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
			// 
			// btnCoordinates
			// 
			this.btnCoordinates.BackColor = System.Drawing.Color.Transparent;
			this.btnCoordinates.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnCoordinates.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnCoordinates.ImageIndex = 6;
			this.btnCoordinates.ImageList = this.imageList1;
			this.btnCoordinates.Location = new System.Drawing.Point(640, 32);
			this.btnCoordinates.Name = "btnCoordinates";
			this.btnCoordinates.Size = new System.Drawing.Size(90, 28);
			this.btnCoordinates.TabIndex = 45;
			this.btnCoordinates.TabStop = false;
			this.btnCoordinates.Text = "Coo&rdinates";
			this.btnCoordinates.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.btnCoordinates.Click += new System.EventHandler(this.btnStatusSection_Click);
			// 
			// frmMain
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(996, 470);
			this.Controls.Add(this.panelBody);
			this.Controls.Add(this.panelTop);
			this.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "frmMain";
			this.Text = "AIMS Viewer";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.Closed += new System.EventHandler(this.frmMain_Closed);
			this.panelTop.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.axAatLogin1)).EndInit();
			this.panelBody.ResumeLayout(false);
			this.panelTab.ResumeLayout(false);
			this.panelOverviewLabel.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.timerCloseStartup)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string [] args) 
		{
			Application.Run(new frmMain(args));
		}

		private void Form1_Load(object sender, System.EventArgs e)
		{
			System.Diagnostics.Debug.WriteLine("Main Thread =[" + System.Threading.Thread.CurrentThread.GetHashCode().ToString() + "]");

			labelViewpoints.Parent = picTop;
			btnOpenViewPoint.Parent = picTop;
			btnDeleteViewPoint.Parent = picTop;
			btnEditViewPoint.Parent = picTop;
			btnNewViewPoint.Parent = picTop;
			btnGeneralSettings.Parent = picTop;
			cbOpen.Parent = picTop;
			btnMessages.Parent = picTop;
			btnSymbolGroups.Parent = picTop;
			btnCoordinates.Parent = picTop;
			this.btnCameras.Parent = picTop;
			this.btnAbout.Parent = picTop;
			this.btnView.Parent  = picTop;
			this.lblUTCTime.Parent = picTop;
			this.lblLocalTime.Parent = picTop;
			this.btnHelp.Parent = picTop;

			this.Left = 0;
			this.Top = 0;

			myDB = UT.GetMemDB();
			bool result = myDB.Connect(UT.ServerName,UT.UserName,UT.Hopo,"TAB","AIMSVIEW");
			if (!result)
			{
				Application.Exit();
			}

			if (args.Length == 0) 
			{
				if (LoginProcedure() == false)
				{
					Application.Exit();
					return;
				}
			}
			else
			{
				if (args[0] != "/CONNECTED")
				{
					if (LoginProcedure() == false)
					{
						Application.Exit();
						return;
					}
				}	
			}

			HandleToolbar_BDPS_SEC();

			UT.UserName = axAatLogin1.GetUserName();
			if (UT.UserName.Length == 0)
				UT.UserName = "Default";

			StartForm = new frmStartup();
			if (screens.Length > 1)
			{
				StartForm.StartPosition = FormStartPosition.Manual;
				StartForm.Location = new Point(primary.WorkingArea.Left+primary.WorkingArea.Width / 2,primary.WorkingArea.Top+primary.WorkingArea.Height / 2);
			}
			StartForm.Show();
			StartForm.Refresh();

			DataForm = new frmData(axAatLogin1);
			if (screens.Length > 1)
			{
				DataForm.StartPosition = FormStartPosition.Manual;
				DataForm.Location = new Point(primary.WorkingArea.Left+primary.WorkingArea.Width / 2,primary.WorkingArea.Top+primary.WorkingArea.Height / 2);
			}
			DataForm.Show();
			if(args.Length > 0 && args[0] == "/CONNECTED")
			{
				DataForm.UFISSuperUser = true;
			}

			if (args.Length == 0 || args[0] != "/CONNECTED")
			{
				DataForm.Hide();
			}

			DataForm.LoadData();

			StartForm.txtCurrentStatus.Text = "Initialization completed";
			StartForm.txtCurrentStatus.Refresh();
			StartForm.txtDoneStatus.Refresh();

			this.dataExchange = new DataExchange(this.components);
			DataExchange.OnCLO_Message +=new AIMSViewer.DataExchange.CLO_Message(DataExchange_OnCLO_Message);
			DataExchange.OnAVP_Changed +=new AIMSViewer.DataExchange.AVP_Changed(DataExchange_OnAVP_Changed);
			DataExchange.OnACS_Changed +=new AIMSViewer.DataExchange.ACS_Changed(DataExchange_OnACS_Changed);
			DataExchange.OnBeforeViewNameChanged +=new AIMSViewer.DataExchange.BeforeViewNameChanged(DataExchange_OnBeforeViewNameChanged);
			DataExchange.OnViewNameChanged +=new AIMSViewer.DataExchange.ViewNameChanged(DataExchange_OnViewNameChanged);
			DataExchange.OnSymbolSelection_Changed +=new AIMSViewer.DataExchange.SymbolSelection_Changed(DataExchange_OnSymbolSelection_Changed);
			DataExchange.InitDBObjects(this.bcBlinker1);

			ViewFilter.MyInit(DataForm.Views);

			ReadRegistry();

//			InitializeRegisteredViewpoints();

			DateTime localTime = DateTime.Now;
			DateTime utcTime = DateTime.UtcNow;
			lblUTCTime.Text = utcTime.ToShortDateString() + " " + utcTime.ToShortTimeString() + "z";
			lblLocalTime.Text = localTime.ToShortDateString() + " " + localTime.ToShortTimeString();

			this.timerCloseStartup.Enabled = true;
			this.timerCloseStartup.Start();
			this.Text = "AIMSViewer" + " --- Active View: [" + ViewFilter.CurrentViewName + "]";

		}

		private void picTop_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, picTop, Color.WhiteSmoke, Color.LightGray /*Color.FromKnownColor(KnownColor.Control)*/);
		}

		private void btnGeneralSettings_Click(object sender, System.EventArgs e)
		{
			frmGeneralSettings dlg = new frmGeneralSettings();
			dlg.ShowDialog(this);
		}

		private void btnNewViewPoint_Click(object sender, System.EventArgs e)
		{
			ViewpointDefinition dlg = new ViewpointDefinition(string.Empty);
			dlg.Text = "Creating new Viewpoint";			
			if (dlg.ShowDialog(this) == DialogResult.OK)
			{
				// must handle this event by ourself because already inserts records don't fire events
				// this.DataExchange_OnAVP_Changed(this,dlg.Urno,State.Created);																
			}
		}

		private void btnSymbol_Click(object sender, System.EventArgs e)
		{
			Symbol dlg = new Symbol();
			dlg.ShowDialog(this);
		}

		private void btnSymbolGroups_Click(object sender, System.EventArgs e)
		{
			StatusGroups dlg = new StatusGroups();
			dlg.ShowDialog(this);
		}

		private void btnStatuses_Click(object sender, System.EventArgs e)
		{
			Symbol dlg = new Symbol();
			dlg.ShowDialog();
		}

		private void cbOpen_CheckedChanged(object sender, System.EventArgs e)
		{
			if (this.cbOpen.Checked)
			{
				if (frmOverview != null)
				{
					frmOverview.Show();
					frmOverview.BringToFront();
				}
				else
				{
					if (!IsRegisteredViewpoint(ViewFilter.CurrentViewName,"StatusOverview"))
					{
						frmOverview = new StatusOverview();
//						frmOverview.Owner = this;
						this.frmOverview.Closed +=new EventHandler(frmOverview_Closed);
						if (screens.Length > 1)
						{
							frmOverview.StartPosition = FormStartPosition.Manual;
							frmOverview.Location = new Point(primary.WorkingArea.Left+primary.WorkingArea.Width/2,primary.WorkingArea.Top+primary.WorkingArea.Height / 2);
						}
						frmOverview.Show();
						frmOverview.BringToFront();
					}
					else
					{
						frmOverview = new StatusOverview();
//						frmOverview.Owner = this;
						this.frmOverview.Closed +=new EventHandler(frmOverview_Closed);
						frmOverview.Show();
						frmOverview.BringToFront();
					}
				}
			}
			else if (this.frmOverview != null)
			{
				frmOverview.Close();
			}
		}

		private void HandleToolbar_BDPS_SEC()
		{
			string prv = this.axAatLogin1.GetPrivileges("ToolbarGeneralSettings");
			if(prv == "-" || prv == "0")
			{
				this.btnGeneralSettings.Tag = prv;
				this.btnGeneralSettings.Enabled = false;
				if (prv == "-")
					btnGeneralSettings.Visible = false;
			}
			prv = this.axAatLogin1.GetPrivileges("ToolbarStatusMessages");
			if(prv == "-" || prv == "0")
			{
				this.btnMessages.Tag = prv;
				this.btnMessages.Enabled = false;
				if (prv == "-")
					btnMessages.Visible = false;
			}
			prv = this.axAatLogin1.GetPrivileges("ToolbarStatusGroups");
			if(prv == "-" || prv == "0")
			{
				this.btnSymbolGroups.Tag = prv;
				this.btnSymbolGroups.Enabled = false;
				if (prv == "-")
					btnSymbolGroups.Visible = false;
			}
			prv = this.axAatLogin1.GetPrivileges("ToolbarCameras");
			if(prv == "-" || prv == "0")
			{
				this.btnCameras.Tag = prv;
				this.btnCameras.Enabled = false;
				if (prv == "-")
					btnCameras.Visible = false;
			}
			prv = this.axAatLogin1.GetPrivileges("ToolbarView");
			if(prv == "-" || prv == "0")
			{
				this.btnView.Tag = prv;
				this.btnView.Enabled = false;
				if (prv == "-")
					btnView.Visible = false;
			}

			prv = this.axAatLogin1.GetPrivileges("ToolbarCoordinates");
			if(prv == "-" || prv == "0")
			{
				this.btnCoordinates.Tag = prv;
				this.btnCoordinates.Enabled = false;
				if (prv == "-")
					btnCoordinates.Visible = false;
			}

			prv = this.axAatLogin1.GetPrivileges("ToolbarOverview");
			if(prv == "-" || prv == "0")
			{
				this.cbOpen.Tag = prv;
				this.cbOpen.Enabled = false;
				if (prv == "-")
					cbOpen.Visible = false;
			}
			prv = this.axAatLogin1.GetPrivileges("ToolbarOpenViewpoint");
			if(prv == "-" || prv == "0")
			{
				btnOpenViewPoint.Tag = prv;
				btnOpenViewPoint.Enabled = false;
				if (prv == "-")
					btnOpenViewPoint.Visible = false;
			}

			prv = this.axAatLogin1.GetPrivileges("ToolbarNewViewpoint");
			if(prv == "-" || prv == "0")
			{
				btnNewViewPoint.Tag = prv;
				btnNewViewPoint.Enabled = false;
				if (prv == "-")
					btnNewViewPoint.Visible = false;
			}

			prv = this.axAatLogin1.GetPrivileges("ToolbarEditViewpoint");
			if(prv == "-" || prv == "0")
			{
				btnEditViewPoint.Tag = prv;
				btnEditViewPoint.Enabled = false;
				if (prv == "-")
					btnEditViewPoint.Visible = false;
			}
			prv = this.axAatLogin1.GetPrivileges("ToolbarDeleteViewpoint");
			if(prv == "-" || prv == "0")
			{
				btnDeleteViewPoint.Tag = prv;
				btnDeleteViewPoint.Enabled = false;
				if (prv == "-")
					btnDeleteViewPoint.Visible = false;
			}
		}

		private bool LoginProcedure()
		{
			string LoginAnsw = "";
			string tmpRegStrg = "";
			string strRet = "";
			axAatLogin1.ApplicationName = "AIMSViewer";
			axAatLogin1.VersionString = Application.ProductVersion;
			axAatLogin1.InfoCaption = "Info about AIMS Viewer";
			axAatLogin1.InfoButtonVisible = true;
			axAatLogin1.InfoUfisVersion = "Ufis Version 4.5";
			axAatLogin1.InfoAppVersion = "AIMS Viewer " + Application.ProductVersion.ToString();
			axAatLogin1.InfoAAT = "UFIS Airport Solutions GmbH";
			axAatLogin1.UserNameLCase = true;

			// security registration
			tmpRegStrg = "AIMSViewer" + ",";
			tmpRegStrg += "InitModu,InitModu,Initialisieren (InitModu),B,-";

			// security registration for toolbar
			tmpRegStrg += ",Toolbar,ToolbarGeneralSettings,General Settings,B,-";
			tmpRegStrg += ",Toolbar,ToolbarStatusMessages,Status Messages,B,-";
			tmpRegStrg += ",Toolbar,ToolbarStatusGroups,Status Groups,B,-";
			tmpRegStrg += ",Toolbar,ToolbarCameras,Camera Definition,B,-";
			tmpRegStrg += ",Toolbar,ToolbarView,View Definition,B,-";
			tmpRegStrg += ",Toolbar,ToolbarCoordinates,Status Locations,B,-";
			tmpRegStrg += ",Toolbar,ToolbarOverview,Open Status-Overview,B,-";
			tmpRegStrg += ",Toolbar,ToolbarOpenViewpoint,Open Viewpoint,B,-";
			tmpRegStrg += ",Toolbar,ToolbarNewViewpoint,New Viewpoint,B,-";
			tmpRegStrg += ",Toolbar,ToolbarEditViewpoint,Edit Viewpoint,B,-";
			tmpRegStrg += ",Toolbar,ToolbarDeleteViewpoint,Delete Viewpoint,B,-";

			// security registration for subsystems
			tmpRegStrg += ",Subsystem,SubsystemBAS,BAS,B,-";
			tmpRegStrg += ",Subsystem,SubsystemCASS,CASS,B,-";
			tmpRegStrg += ",Subsystem,SubsystemFDA,FDA,B,-";
			tmpRegStrg += ",Subsystem,SubsystemSCADA,SCADA,B,-";

			if (frmData.OtherSystems != null)
			{
				foreach (string system in frmData.OtherSystems)
				{
					tmpRegStrg += ",Subsystem,Subsystem" + system + "," + system + ",B,-";
				}
			}

			// general settings
			tmpRegStrg += ",General Settings Dialog,GeneralSettingDlgSave,Save General Settings,B,-";	

			// Status messages
			tmpRegStrg += ",Status Messages Dialog,StatusMessagesDlgSave,Save Status Messages,B,-";	

			// Status groups
			tmpRegStrg += ",Status Groups Dialog,StatusGroupsDlgSave,Save Status Groups,B,-";	

			// Camera definitions
			tmpRegStrg += ",Camera Definition Dialog,CamerasDlgSave,Save Camera Defintion,B,-";	

			// View definitions
			tmpRegStrg += ",View Definition Dialog,ViewDlgSave,Save View Definition,B,-";	

			// Status locations
			tmpRegStrg += ",Status Locations Dialog,StatusLocationsDlgSave,Save Status Locations,B,-";	

			// Status Overview

			//Append any other Privileges
			//tmpRegStrg = tmpRegStrg & ",Entry,Entry,Entry,F,1"
			//tmpRegStrg = tmpRegStrg & ",Feature,Feature,Feature,F,1"

			axAatLogin1.RegisterApplicationString = tmpRegStrg;
			strRet = axAatLogin1.ShowLoginDialog();

			LoginAnsw = axAatLogin1.GetPrivileges("InitModu");
			UT.UserName = axAatLogin1.GetUserName();
			if( LoginAnsw != "" && strRet != "CANCEL" )
				return true; 
			else  
				return false;
		}

		private void btnOpenViewPoint_Click(object sender, System.EventArgs e)
		{
			if (this.cbViewpoints.SelectedIndex >= 0)
			{
				/*
				Viewpoint dlg = new Viewpoint();
				dlg.Owner = this;
				dlg.Urno = (string)this.viewpoints.GetKey(this.viewpoints.IndexOfValue((string)this.cbViewpoints.Text));*/
				Viewpoint dlg;
				dlg = Viewpoint.ShowViewPoint (this, (string)this.viewpoints.GetKey(this.viewpoints.IndexOfValue((string)this.cbViewpoints.Text)) );
				dlg.Show();
			}
		}

		private void timerCloseStartup_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			timerCloseStartup.Stop();
			timerCloseStartup.Enabled = false;
			StartForm.Refresh();
			StartForm.PrepareClose();
		}

		private void InitializePages()
		{
			this.tabFloors.TabPages.Clear();
			this.tabFloors.SelectedTab = null;

			this.cbViewpoints.Items.Clear();
			this.viewpoints.Clear();

			this.OverviewPage = null;

			ITable myTable = this.myDB["AVP"];
			ArrayList symbolList = frmData.GetThis().GetSymbolListFromInitialEventState();
			for (int i = 0; i < myTable.Count; i++)
			{
				IRow row = myTable[i];

				if (!ViewFilter.IsPassFilter(row))
					continue;

				if (this.cbViewpoints.Items.IndexOf(row["NAME"]) < 0)
				{
					this.cbViewpoints.Items.Add(row["NAME"]);
					this.viewpoints.Add(row["URNO"],row["NAME"]);
				}

				if (row["FOVE"] == "1")
				{
					PictureViewer.PictureControlEx p1 = frmData.GetThis().CreateOrUpdatePictureControl(null,row,symbolList,new PictureViewer.PictureControlEx.ContextMenuItemHandler(OnContextMenuItem),new PictureViewer.PictureControlEx.DoubleClickHandler(OnDblClick));
					p1.Paint +=new PaintEventHandler(p1_Paint);
					TabPage page = new TabPage(row["NAME"]);
					page.Tag = row["URNO"];
					page.BackColor = Color.Transparent;
					page.Paint += new PaintEventHandler(page_Paint);
					page.Controls.Add(p1);
					this.tabFloors.TabPages.Add(page);
					if (row["FOVE"] == "2")
					{
						this.tabFloors.SelectedTab = page;
					}
				}
				else if (row["FOVE"] == "2")
				{
					if (this.OverviewAsPage)
					{
						if (this.OverviewPage == null)
						{
							PictureViewer.PictureControlEx p1 = frmData.GetThis().CreateOrUpdatePictureControl(null,row,symbolList,new PictureViewer.PictureControlEx.ContextMenuItemHandler(OnContextMenuItem),new PictureViewer.PictureControlEx.DoubleClickHandler(OnDblClick));
							p1.Paint +=new PaintEventHandler(p1_Paint);
							TabPage page = new TabPage(row["NAME"]);
							page.Tag = row["URNO"];
							page.BackColor = Color.Transparent;
							page.Paint += new PaintEventHandler(page_Paint);
							page.Controls.Add(p1);
							this.tabFloors.TabPages.Add(page);
							this.tabFloors.SelectedTab = page;
							this.OverviewPage = page;
						}
					}
				}
			}

			this.tabFloors.SelectedIndex = -1;
			if (this.tabFloors.TabCount > 0)
			{
				this.tabFloors.SelectedIndex = 0;
			}

			if (!IsRegisteredViewpoint(ViewFilter.CurrentViewName,"StatusOverview"))
			{
				this.cbOpen.Checked = true;
			}
		}

		private void InitializeRegisteredViewpoints()
		{
			ITable myTable = this.myDB["AVP"];
			int count = GetCountOfRegisteredViewpoints(ViewFilter.CurrentViewName);
			for (int i = 0; i < count; i++)
			{
				string name = GetRegisteredViewpoint(ViewFilter.CurrentViewName,i);
				if (name == "MainWindow" || name == string.Empty)
					continue;
				else if (name == "StatusOverview")
				{
					this.cbOpen.Checked = true;
				}
				else
				{
					IRow[] myRows = myTable.RowsByIndexValue("NAME",name);
					if (myRows != null && myRows.Length == 1)
					{
						if (ViewFilter.IsPassFilter(myRows[0]))
						{
							/*Viewpoint dlg = new Viewpoint();
							dlg.Owner = this;
							dlg.Urno = myRows[0]["URNO"];*/
							Viewpoint dlg;
							dlg = Viewpoint.ShowViewPoint (this, myRows[0]["URNO"] );
							dlg.Show();
						}
					}
				}
			}

		}


		private void btnEditViewPoint_Click(object sender, System.EventArgs e)
		{
			if (this.cbViewpoints.SelectedIndex >= 0)
			{
				ViewpointDefinition dlg = new ViewpointDefinition(this.cbViewpoints.Text);
				dlg.Text = "Edit Viewpoint";			
				if (dlg.ShowDialog(this) == DialogResult.OK)
				{
//					this.DataExchange_OnAVP_Changed(this,dlg.Urno,State.Modified);																
				}
			}
		}

		private void btnCameras_Click(object sender, System.EventArgs e)
		{
			Cameras dlg = new Cameras();
			if (dlg.ShowDialog(this) == DialogResult.OK)
			{

			}
		}

		private void btnMessages_Click(object sender, System.EventArgs e)
		{
			StatusMessages dlg = new StatusMessages();
			if (dlg.ShowDialog(this) == DialogResult.OK)
			{

			}
		}

		private void bcBlinker1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, bcBlinker1, Color.WhiteSmoke, Color.LightGray);
		}

		private void DataExchange_OnCLO_Message()
		{
			MessageBox.Show(this, "The server has been switched!!\nPlease shutdown AIMS-Viewer and restart in 3 minutes","Attention", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			Application.Exit();
		}

		private void frmMain_Closed(object sender, System.EventArgs e)
		{
			DataExchange.OnCLO_Message -= new AIMSViewer.DataExchange.CLO_Message(DataExchange_OnCLO_Message);
			DataExchange.OnAVP_Changed -= new AIMSViewer.DataExchange.AVP_Changed(DataExchange_OnAVP_Changed);
			DataExchange.OnACS_Changed -= new AIMSViewer.DataExchange.ACS_Changed(DataExchange_OnACS_Changed);
			DataExchange.OnViewNameChanged -=new AIMSViewer.DataExchange.ViewNameChanged(DataExchange_OnViewNameChanged);
			DataExchange.OnSymbolSelection_Changed -=new AIMSViewer.DataExchange.SymbolSelection_Changed(DataExchange_OnSymbolSelection_Changed);
	
			ClearRegistry(ViewFilter.CurrentViewName);
			DataExchange.Call_Application_Shutdown();
			this.WriteRegistry();

			this.myDB.Disconnect();
		}

		private void DataExchange_OnAVP_Changed(object sender, string strAvpUrno, State state)
		{
			if (this.InvokeRequired)
			{
					int i = 4711;
			}

			myDB = UT.GetMemDB();
			ITable myTable = myDB["AVP"];
			TabPage page = null;

			switch(state)
			{
				case State.Created:
					IRow [] myRows = myTable.RowsByIndexValue("URNO", strAvpUrno);
					if (myRows.Length > 0)
					{
						IRow row = myRows[0];
						if (ViewFilter.IsPassFilter(row))
						{
							if (this.cbViewpoints.Items.IndexOf(row["NAME"]) < 0)
							{
								this.cbViewpoints.Items.Add(row["NAME"]);
								this.viewpoints.Add(strAvpUrno,row["NAME"]);
							}

							if (row["FOVE"] == "1")
							{
								ArrayList symbolList = frmData.GetThis().GetSymbolListFromInitialEventState();
								PictureControlEx p1 = frmData.GetThis().CreateOrUpdatePictureControl(null,row,symbolList,new PictureViewer.PictureControlEx.ContextMenuItemHandler(OnContextMenuItem),new PictureViewer.PictureControlEx.DoubleClickHandler(OnDblClick));												
								p1.Paint +=new PaintEventHandler(p1_Paint);
								page = new TabPage(row["NAME"]);
								page.Controls.Add(p1);
								page.Tag = strAvpUrno;
								page.BackColor = Color.Transparent;
								page.Paint += new PaintEventHandler(page_Paint);
								this.tabFloors.TabPages.Add(page);
							}
						}
					}
					break;
				case State.Deleted:
					int ind = this.viewpoints.IndexOfKey(strAvpUrno);
					if (ind >= 0)
					{
						this.cbViewpoints.Items.Remove(this.viewpoints.GetByIndex(ind));
						this.viewpoints.RemoveAt(ind);
					}

					page = this.FindByTag(strAvpUrno);
					if (page != null)
					{
						this.tabFloors.TabPages.Remove(page);
					}
					break;
				case State.Modified:
					myRows = myTable.RowsByIndexValue("URNO", strAvpUrno);
					if (myRows.Length > 0)
					{
						IRow row = myRows[0];

						// remove the current view name
						ind = this.viewpoints.IndexOfKey(strAvpUrno);
						if (ind >= 0)
						{
							this.cbViewpoints.Items.Remove(this.viewpoints.GetByIndex(ind));
							this.viewpoints.RemoveAt(ind);
						}

						if (ViewFilter.IsPassFilter(row))
						{
							if (this.cbViewpoints.Items.IndexOf(row["NAME"]) < 0)
							{
								this.cbViewpoints.Items.Add(row["NAME"]);
								this.viewpoints.Add(strAvpUrno,row["NAME"]);
							}

							page = FindByTag(strAvpUrno);
							if (row["FOVE"] == "1")
							{
								if (page == null)
								{
									ArrayList symbolList = frmData.GetThis().GetSymbolListFromInitialEventState();
									PictureControlEx p1 = frmData.GetThis().CreateOrUpdatePictureControl(null,row,symbolList,new PictureViewer.PictureControlEx.ContextMenuItemHandler(OnContextMenuItem),new PictureViewer.PictureControlEx.DoubleClickHandler(OnDblClick));
									p1.Paint +=new PaintEventHandler(p1_Paint);
									page = new TabPage(row["NAME"]);
									page.Controls.Add(p1);
									page.Tag = strAvpUrno;
									page.BackColor = Color.Transparent;
									page.Paint += new PaintEventHandler(page_Paint);
									this.tabFloors.TabPages.Add(page);
								}
								else
								{
									PictureControlEx p1 = (PictureControlEx)FindByTag(page,strAvpUrno);
									frmData.GetThis().CreateOrUpdatePictureControl(p1,row,null,null,null);
									page.Text = row["NAME"];
								}
							}
							else if (row["FOVE"] == "2")
							{
								/*Viewpoint dlg = new Viewpoint();
								dlg.Owner = this;
								dlg.Urno = row["URNO"];*/
								Viewpoint dlg;
								dlg = Viewpoint.ShowViewPoint (this, myRows[0]["URNO"] );
								dlg.Show();
							}
							else
							{
								if (page != null)
								{
									this.tabFloors.TabPages.Remove(page);
								}
							}
						}
					}
					break;
			}
		}

		private TabPage FindByTag(string tag)
		{
			for (int i = 0; i < this.tabFloors.TabPages.Count; i++)
			{
				TabPage page = this.tabFloors.TabPages[i];
				if ((string)page.Tag == tag)
				{
					return page;
				}
			}

			return null;
		}

		private void btnDeleteViewPoint_Click(object sender, System.EventArgs e)
		{
			if (this.cbViewpoints.SelectedIndex >= 0)
			{
				int ind = this.viewpoints.IndexOfValue(this.cbViewpoints.Text);
				if (ind >= 0)
				{
					DialogResult olResult;
					olResult = MessageBox.Show(this, "Do you really want to delete the current viewpoint?", "Question", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
					if (olResult == DialogResult.Yes)
					{
						myDB = UT.GetMemDB();
						ITable myTable = myDB["AVP"];
						IRow [] myRows = myTable.RowsByIndexValue("URNO",(string)this.viewpoints.GetKey(ind));
						if (myRows.Length > 0)
						{
							IRow row = myRows[0];
							row["USEU"]	= UT.UserName;
							row["LSTU"]	= UT.DateTimeToCeda(DateTime.Now.ToUniversalTime());
							row.Status = State.Deleted;
							if (myTable.Save(row))
							{
								// must handle this event by ourself because already deleted records don't fire events
								// this.DataExchange_OnAVP_Changed(this,row["URNO"],State.Deleted);																
							}
						}
					}
				}
			}
		}

		private Control FindByTag(Control parent,string tag)
		{
			for (int i = 0; i < parent.Controls.Count;i++)
			{
				Control ctrl = parent.Controls[i];
				if ((string)ctrl.Tag == tag)
					return ctrl;
			}

			return null;
		}

		private string OnContextMenuItem(int item,PointF origin,ArrayList selectedSymbols)
		{
			if (item == 0)	// get next camera information
			{
				StatusInformationList dlg = new StatusInformationList(selectedSymbols,"NextCamera");
				dlg.ShowDialog(this);
			}
			else if (item == 1)	// get handling advice for group
			{
				StatusInformationList dlg = new StatusInformationList(selectedSymbols,"HandlingAdviceForGroup");
				dlg.ShowDialog(this);
			}
			else if (item == 2)	// get handling advice for status
			{
				StatusInformationList dlg = new StatusInformationList(selectedSymbols,"HandlingAdviceForStatus");
				dlg.ShowDialog(this);
			}
			else if (item == 3)	// unselect symbol
			{
				TabPage page = this.tabFloors.SelectedTab;
				if (page != null)
				{
					PictureControlEx picPreview = (PictureControlEx)this.FindByTag(page,(string)page.Tag);
				
					foreach(PictureViewer.Symbol symbol in selectedSymbols)
					{
						picPreview.UnselectSymbol(symbol);
					}
				}
			}
			return string.Empty;				
		}

		private void OnDblClick(PointF origin,ArrayList symbols)
		{
			if (symbols.Count == 1)
			{
				PictureViewer.Symbol symbol = (PictureViewer.Symbol)symbols[0];

				if (this.OverviewAsPage == true && this.OverviewPage == this.tabFloors.SelectedTab)
				{
					if (!frmData.GetThis().HasEventBeenSeen(symbol.urno))
					{
						frmData.GetThis().EventHasBeenSeen(symbol.urno,true);
					}
				}

				ArrayList viewpoints = frmData.GetThis().GetViewpointDefinitions(symbol.origin,symbol.floor);
				if (viewpoints.Count == 1)
				{
					/*Viewpoint dlg = new Viewpoint();
					dlg.Owner = this;
					dlg.Urno = (string)viewpoints[0];*/
					Viewpoint dlg;
					dlg = Viewpoint.ShowViewPoint (this, (string)viewpoints[0] );
					dlg.SelectSymbol(symbol);
//					DataExchange.Call_SymbolSelection_Changed(this,symbol,true);
					dlg.Show();
				}
				else if (viewpoints.Count > 1)
				{
					ViewpointList dlg = new ViewpointList(symbol,viewpoints);
					dlg.ShowDialog(this);
				}
				else
				{
					MessageBox.Show(this,"No view point available for the specified status event","Information");
				}
			}
			else if (symbols.Count > 1)
			{
				for (int i = 0; i < symbols.Count; i++)
				{
					if (this.OverviewAsPage == true && this.OverviewPage == this.tabFloors.SelectedTab)
					{
						PictureViewer.Symbol symbol = (PictureViewer.Symbol)symbols[i];

						if (!frmData.GetThis().HasEventBeenSeen(symbol.urno))
						{
							frmData.GetThis().EventHasBeenSeen(symbol.urno,true);
						}
					}
				}

				ViewpointList dlg = new ViewpointList(symbols);
				dlg.Owner = this;
				dlg.ShowDialog(this);
			}
			else
			{
				MessageBox.Show(this,"No status event available at this location","Information");
			}

		}

		private void tabFloors_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			TabPage page = this.tabFloors.SelectedTab;
			if (page == null)
			{
				this.cbViewpoints.SelectedIndex = -1;
			}
			else
			{
				int index = this.viewpoints.IndexOfKey((string)page.Tag);
				if (index >= 0)
				{
					this.cbViewpoints.Text = (string)this.viewpoints.GetByIndex(index);	
				}
			}
		}

		private void DataExchange_OnACS_Changed(object sender, ArrayList updateList)
		{
			foreach(TabPage page in this.tabFloors.TabPages)
			{
				PictureControlEx picPreview = (PictureControlEx)this.FindByTag(page,(string)page.Tag);
#if	OldBehavior

				picPreview.ClearSymbols();
				ArrayList symbolList = frmData.GetThis().GetSymbolListFromInitialEventState();
				foreach (PictureViewer.Symbol symbol in symbolList)
				{
					if (picPreview.Floor == -99)	// overview 
					{
						if (picPreview.Dimension.Contains(symbol.origin))
						{
							picPreview.AddSymbol(symbol,false);
							if (!frmData.GetThis().HasEventBeenSeen(symbol.urno))
							{
								picPreview.SelectSymbol(symbol);
							}
						}
					}
					else if (symbol.floor == picPreview.Floor && picPreview.Dimension.Contains(symbol.origin))
					{
						picPreview.AddSymbol(symbol,false);
					}
				}

				picPreview.Refresh();
#else
				frmData.GetThis().UpdatePictureControl(picPreview,updateList);
#endif
			}

		}

		private void btnAbout_Click(object sender, System.EventArgs e)
		{
			frmAbout dlg = new frmAbout();
			dlg.strAppName = "AIMS Viewer";
			Assembly [] assemblies = AppDomain.CurrentDomain.GetAssemblies();
			foreach(Assembly assembly in assemblies)
			{
				if (assembly.GetName().Name == "AIMSViewer")
				{
					dlg.strAppVersion = assembly.GetName().Version.ToString();
				}
			}
				
			dlg.ShowDialog(this);
		
		}


		private void p1_Paint(object sender, PaintEventArgs e)
		{
			Control ctrl = (Control)sender;
			UT.DrawGradient(e.Graphics, 90f,ctrl, Color.WhiteSmoke, Color.LightGray);		
		}

		private void page_Paint(object sender, PaintEventArgs e)
		{
			TabPage page = (TabPage)sender;
			UT.DrawGradient(e.Graphics, 90f,page, Color.WhiteSmoke, Color.LightGray);		
		}

		private void btnView_Click(object sender, System.EventArgs e)
		{
			View dlg = new View(frmData.GetThis().Views);
			dlg.ShowDialog(this);

		}

		private void DataExchange_OnViewNameChanged()
		{
			this.InitializePages();
			this.InitializeRegisteredViewpoints();
			this.InitializeButtons();

			this.Text = "AIMSViewer" + " --- Active View: [" + ViewFilter.CurrentViewName + "]";
		}

		private int  GetCountOfRegisteredViewpoints(string viewName)
		{
			string theKey = "Software\\AIMSViewer\\" + viewName;
			RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			if (rk == null)
				return 0;
			int count = rk.SubKeyCount;
			rk.Close();
			return count;
		}

		private string  GetRegisteredViewpoint(string viewName,int ind)
		{
			string theKey = "Software\\AIMSViewer\\" + viewName;
			RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			if (rk == null)
				return string.Empty;
			int count = rk.SubKeyCount;
			if (ind >= count)
				return string.Empty;
			string[] names = rk.GetSubKeyNames();
			rk.Close();
			return names[ind];
		}

		private bool IsRegisteredViewpoint(string viewName,string name)
		{
			string theKey = "Software\\AIMSViewer\\" + viewName + "\\" + name;
			RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey,false);
			if (rk == null)
				return false;
			else
				return true;
		}

		private void ClearRegistry(string viewName)
		{
			string theKey = "Software";
			RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey,true);
			if(rk != null)
			{
				try
				{
					rk.DeleteSubKeyTree("AIMSViewer\\MainWindow");
					rk.DeleteSubKeyTree("AIMSViewer\\" + viewName);
				}
				catch(Exception ex)
				{
					System.Diagnostics.Debug.WriteLine(ex.Message);
				}
				finally
				{
					rk.Close();
				}
			}
		}

		private void ReadRegistry()
		{
			int myX=-1, myY=-1, myW=-1, myH=-1;
			string viewName = "<DEFAULT>";
			string regVal = "";
			string theKey = "Software\\AIMSViewer\\MainWindow";
			RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			if(rk != null)
			{
				regVal = rk.GetValue("X", "-1").ToString();
				myX = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Y", "-1").ToString();
				myY = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Width", "-1").ToString();
				myW = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Height", "-1").ToString();
				myH = Convert.ToInt32(regVal);
				viewName = rk.GetValue("ViewFilter", "<DEFAULT>").ToString();
			}

			ViewFilter.CurrentViewName = viewName;

			if (myX >= 0 && myY >= 0 && myW != -1 && myH != -1)
			{
				this.Left = myX;
				this.Top = myY;
				this.Width = myW;
				this.Height = myH;
			}
			else if (myX == -4 && myY == -4)
			{
				this.WindowState = FormWindowState.Maximized;
			}
			else if (myX == -32000 && myY == -32000)
			{
				this.WindowState = FormWindowState.Minimized;
			}

			this.init = true;
		}

		private void WriteRegistry()
		{
			if (!init)
				return;

			string theKey = "Software\\AIMSViewer\\MainWindow";
			RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			if(rk == null)
			{
				rk = Registry.CurrentUser.OpenSubKey("Software\\AIMSViewer", true);
				if (rk == null)
				{
					rk = Registry.CurrentUser.OpenSubKey("Software", true);
					rk.CreateSubKey("AIMSViewer");
					rk.Close();
					rk = Registry.CurrentUser.OpenSubKey("Software\\AIMSViewer", true);
				}

				rk.CreateSubKey("MainWindow");
				rk.Close();
				theKey = "Software\\AIMSViewer\\MainWindow";
				rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			}

			rk.SetValue("X", this.Left.ToString());
			rk.SetValue("Y", this.Top.ToString());
			rk.SetValue("Width", this.Width.ToString());
			rk.SetValue("Height", this.Height.ToString());
			rk.SetValue("ViewFilter",ViewFilter.CurrentViewName);

		}

		private void timer1_Tick(object sender, System.EventArgs e)
		{
			DateTime localTime = DateTime.Now;
			DateTime utcTime = DateTime.UtcNow;
			lblUTCTime.Text = utcTime.ToShortDateString() + " " + utcTime.ToShortTimeString() + "z";
			lblUTCTime.Refresh();
			lblLocalTime.Text = localTime.ToShortDateString() + " " + localTime.ToShortTimeString();
			lblLocalTime.Refresh();
		}

		private void frmOverview_Closed(object sender, EventArgs e)
		{
			this.frmOverview = null;
			this.cbOpen.Checked = false;
		}
		
		protected override void OnClosing(CancelEventArgs e)
		{
			if (MessageBox.Show("Are you sure that you want to exit from this application?","Question",MessageBoxButtons.YesNo,MessageBoxIcon.Question,MessageBoxDefaultButton.Button1,MessageBoxOptions.DefaultDesktopOnly) == DialogResult.No)
			{
				e.Cancel = true;
				base.OnClosing (e);
			}
			else
			{
				base.OnClosing (e);
			}
		}

		private void DataExchange_OnSymbolSelection_Changed(object sender, PictureViewer.Symbol symbol, bool selected)
		{
			if (this.OverviewPage != null)
			{
				PictureControlEx picPreview = (PictureControlEx)this.FindByTag(this.OverviewPage,(string)this.OverviewPage.Tag);
				if (selected)
					picPreview.SelectSymbol(symbol);
				else
					picPreview.UnselectSymbol(symbol);
			}

		}

		private void btnHelp_Click(object sender, System.EventArgs e)
		{
			string fileName = this.helpPath.Length == 0 ? this.helpFile : this.helpPath + "\\" + this.helpFile;
			Help.ShowHelp(this,fileName);
		}

		private void InitializeButtons()
		{
			if (ViewFilter.IsDefaultActive)	// must not overide BDPS_SEC settings
			{
				if (this.btnNewViewPoint.Tag == null)
				{
					this.btnNewViewPoint.Enabled = true;
				}

				if (this.btnDeleteViewPoint.Tag == null)
				{
					this.btnDeleteViewPoint.Enabled = true;
				}

				if (this.btnEditViewPoint.Tag == null)
				{
					this.btnEditViewPoint.Enabled = true;
				}

				if (this.btnSymbolGroups.Tag == null)
				{
					this.btnSymbolGroups.Enabled = true;
				}
			}
			else
			{
				if (this.btnNewViewPoint.Tag == null)
				{
					this.btnNewViewPoint.Enabled = false;
				}

				if (this.btnDeleteViewPoint.Tag == null)
				{
					this.btnDeleteViewPoint.Enabled = false;
				}

				if (this.btnEditViewPoint.Tag == null)
				{
					this.btnEditViewPoint.Enabled = false;
				}

				if (this.btnSymbolGroups.Tag == null)
				{
					this.btnSymbolGroups.Enabled = false;
				}
			}
		}

		private void cbViewpoints_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		}

		private void cbViewpoints_TextChanged(object sender, System.EventArgs e)
		{
			if (this.tabFloors == null || this.viewpoints == null)
				return;

			int index = this.viewpoints.IndexOfValue((string)this.cbViewpoints.Text);
			if (index < 0)
				return;
			string urno = (string)this.viewpoints.GetKey(index);

			foreach (TabPage page in this.tabFloors.TabPages)							
			{
				if ((string)page.Tag == urno)
				{
					this.tabFloors.SelectedTab = page;
					break;
				}
			}
		}

		private void btnStatusSection_Click(object sender, System.EventArgs e)
		{
			frmDataEntryGrid frm = new frmDataEntryGrid();
			frm.ShowDialog();
		}

		private void DataExchange_OnBeforeViewNameChanged()
		{
			if (this.init && ViewFilter.CurrentViewName != "")
			{
				this.ClearRegistry(ViewFilter.CurrentViewName);
				this.WriteRegistry();
			}
		}
	}
}
