using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Ufis.Data;
using Ufis.Utils;

namespace AIMSViewer
{
	/// <summary>
	/// Summary description for ErrorList.
	/// </summary>
	public class ErrorList : System.Windows.Forms.Form
	{
		private AxTABLib.AxTAB axStatusTAB;
		private System.Windows.Forms.Label label1;
		bool bmDisplay;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ErrorList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			axStatusTAB.SetHeaderText ( "X-Coord.,Y-Coord.,Status,Subsystem,Originator,Type,URNO" );
			axStatusTAB.EnableHeaderSizing(true);
			axStatusTAB.ResetContent();
			axStatusTAB.HeaderLengthString = "70,70,90,100,100,200,80";

			Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\ceda.ini");
			String ShowIgnored = myIni.IniReadValue("AIMSViewer","ShowIgnoredEvents");
			ShowIgnored = ShowIgnored.ToUpper();
			if (ShowIgnored == "NO" || ShowIgnored == "FALSE")
				bmDisplay = false;
			else
				bmDisplay = true;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ErrorList));
			this.axStatusTAB = new AxTABLib.AxTAB();
			this.label1 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.axStatusTAB)).BeginInit();
			this.SuspendLayout();
			// 
			// axStatusTAB
			// 
			this.axStatusTAB.Location = new System.Drawing.Point(0, 32);
			this.axStatusTAB.Name = "axStatusTAB";
			this.axStatusTAB.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axStatusTAB.OcxState")));
			this.axStatusTAB.Size = new System.Drawing.Size(712, 432);
			this.axStatusTAB.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(624, 16);
			this.label1.TabIndex = 1;
			this.label1.Text = "Following events could not be displayed:";
			// 
			// ErrorList
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(712, 469);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.axStatusTAB);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "ErrorList";
			this.Text = "Ignored Status Events ";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.OnClosing);
			((System.ComponentModel.ISupportInitialize)(this.axStatusTAB)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		public void Add ( ref IRow rowACS )
		{
			string myStr;
			myStr = rowACS["XCOD"] + "," +
					rowACS["YCOD"] + "," +
					rowACS["STAT"] + "," +
					rowACS["SUBS"] + "," +
					rowACS["ORIG"] + "," +
					rowACS["TYPE"] + "," +
					rowACS["URNO"] ;
			axStatusTAB.InsertTextLine ( myStr, true );
			if ( bmDisplay )
				this.Show ();
		}

		private void OnClosing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			e.Cancel = true;
			this.Hide();
		}
	}
}
