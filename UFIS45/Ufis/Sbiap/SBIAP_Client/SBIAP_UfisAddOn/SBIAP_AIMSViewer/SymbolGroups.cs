using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Ufis.Utils;
using Ufis.Data;

namespace AIMSViewer
{
	/// <summary>
	/// Summary description for SymbolGroups.
	/// </summary>
	public class SymbolGroups : System.Windows.Forms.Form
	{
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Button bntDelete;
		private System.Windows.Forms.Button lblAdd;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox comboBox1;
		private AxTABLib.AxTAB tabList;
		private System.Windows.Forms.Panel panel1;
		private System.ComponentModel.IContainer components;

		public SymbolGroups()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(SymbolGroups));
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.btnClose = new System.Windows.Forms.Button();
			this.btnSave = new System.Windows.Forms.Button();
			this.bntDelete = new System.Windows.Forms.Button();
			this.lblAdd = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.comboBox1 = new System.Windows.Forms.ComboBox();
			this.tabList = new AxTABLib.AxTAB();
			this.panel1 = new System.Windows.Forms.Panel();
			((System.ComponentModel.ISupportInitialize)(this.tabList)).BeginInit();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// imageList1
			// 
			this.imageList1.ImageSize = new System.Drawing.Size(19, 19);
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.White;
			// 
			// btnClose
			// 
			this.btnClose.BackColor = System.Drawing.Color.Transparent;
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnClose.Dock = System.Windows.Forms.DockStyle.Left;
			this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnClose.ImageIndex = 4;
			this.btnClose.ImageList = this.imageList1;
			this.btnClose.Location = new System.Drawing.Point(160, 0);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(72, 48);
			this.btnClose.TabIndex = 13;
			this.btnClose.Text = "   &Close";
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// btnSave
			// 
			this.btnSave.BackColor = System.Drawing.Color.Transparent;
			this.btnSave.Dock = System.Windows.Forms.DockStyle.Left;
			this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnSave.ImageIndex = 2;
			this.btnSave.ImageList = this.imageList1;
			this.btnSave.Location = new System.Drawing.Point(232, 0);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(72, 48);
			this.btnSave.TabIndex = 12;
			this.btnSave.Text = "   &Save";
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// bntDelete
			// 
			this.bntDelete.BackColor = System.Drawing.Color.Transparent;
			this.bntDelete.Dock = System.Windows.Forms.DockStyle.Left;
			this.bntDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.bntDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.bntDelete.ImageIndex = 1;
			this.bntDelete.ImageList = this.imageList1;
			this.bntDelete.Location = new System.Drawing.Point(88, 0);
			this.bntDelete.Name = "bntDelete";
			this.bntDelete.Size = new System.Drawing.Size(72, 48);
			this.bntDelete.TabIndex = 11;
			this.bntDelete.Text = "    &Delete";
			this.bntDelete.Click += new System.EventHandler(this.bntDelete_Click);
			// 
			// lblAdd
			// 
			this.lblAdd.BackColor = System.Drawing.Color.Transparent;
			this.lblAdd.Dock = System.Windows.Forms.DockStyle.Left;
			this.lblAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.lblAdd.ImageIndex = 0;
			this.lblAdd.ImageList = this.imageList1;
			this.lblAdd.Location = new System.Drawing.Point(0, 0);
			this.lblAdd.Name = "lblAdd";
			this.lblAdd.Size = new System.Drawing.Size(88, 48);
			this.lblAdd.TabIndex = 10;
			this.lblAdd.Text = "&New Grp";
			this.lblAdd.Click += new System.EventHandler(this.lblAdd_Click);
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(16, 24);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(76, 16);
			this.label1.TabIndex = 14;
			this.label1.Text = "Group Name:";
			// 
			// comboBox1
			// 
			this.comboBox1.Location = new System.Drawing.Point(96, 20);
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size(224, 21);
			this.comboBox1.TabIndex = 15;
			this.comboBox1.Text = "Elevators";
			this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
			// 
			// tabList
			// 
			this.tabList.Location = new System.Drawing.Point(96, 72);
			this.tabList.Name = "tabList";
			this.tabList.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabList.OcxState")));
			this.tabList.Size = new System.Drawing.Size(496, 236);
			this.tabList.TabIndex = 16;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnSave);
			this.panel1.Controls.Add(this.btnClose);
			this.panel1.Controls.Add(this.bntDelete);
			this.panel1.Controls.Add(this.lblAdd);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 326);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(640, 48);
			this.panel1.TabIndex = 17;
			// 
			// SymbolGroups
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(640, 374);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.tabList);
			this.Controls.Add(this.comboBox1);
			this.Controls.Add(this.label1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "SymbolGroups";
			this.Text = "Symbol Groups ...";
			this.Load += new System.EventHandler(this.SymbolGroups_Load);
			((System.ComponentModel.ISupportInitialize)(this.tabList)).EndInit();
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void SymbolGroups_Load(object sender, System.EventArgs e)
		{
			IDatabase myDB = UT.GetMemDB();
			ITable	myTable= myDB["ASG"];	

			string strColor = "";
			strColor = UT.colBlue.ToString() + "," + UT.colBlue.ToString();
			tabList.ResetContent();
			tabList.HeaderString = "IF-Symbol,Status,Color,Handling Advice";
			tabList.LogicalFieldList = "IF-Symbol,Status,Color,Handling Advice";
			tabList.HeaderLengthString = "100,100,100,200";
			tabList.ColumnWidthString  = "32,32,10,255";
			tabList.CursorDecoration("IF-Symbol,Status,Color,Handling Advice", "T,B", "2,2", strColor);
			tabList.LineHeight = 18;


			for (int i = 0; i < myTable.Count;i++)
			{
				IRow myRow = myTable[i];
				this.comboBox1.Items.Add(myRow["NAME"]);
			}

			tabList.CreateCellObj("YELLOW", UT.colYellow, UT.colBlack, 10, false, false, false, 10, "Arial");
			tabList.CreateCellObj("RED", UT.colRed, UT.colBlack, 10, false, false, false, 10, "Arial");
			tabList.CreateCellObj("GREEN", UT.colGreen, UT.colBlack, 10, false, false, false, 10, "Arial");
			tabList.CreateCellObj("BLUE", UT.colBlue, UT.colBlack, 10, false, false, false, 10, "Arial");
			tabList.SetTabFontBold(true);
			tabList.DefaultCursor = false;
			
			tabList.InsertTextLine("F,Warning, ,Keep attention", false);
			tabList.InsertTextLine("X,Emergency, ,Switch it off", false);
			tabList.InsertTextLine("99,Regular, , ", false);
			tabList.InsertTextLine("AM,Switced off, ,Restart it", false);
			tabList.SetCellProperty(0, 2, "YELLOW");
			tabList.SetCellProperty(1, 2, "RED");
			tabList.SetCellProperty(2, 2, "GREEN");
			tabList.SetCellProperty(3, 2, "BLUE");
		}

		private void lblAdd_Click(object sender, System.EventArgs e)
		{
		
		}

		private void bntDelete_Click(object sender, System.EventArgs e)
		{
		
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
		
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
		
		}

		private void comboBox1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}
	}
}
