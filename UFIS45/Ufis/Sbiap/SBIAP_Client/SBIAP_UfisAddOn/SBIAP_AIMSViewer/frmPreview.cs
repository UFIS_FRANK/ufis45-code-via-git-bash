using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Export.Pdf;
using DataDynamics.ActiveReports.Export.Xls;



namespace AIMSViewer
{
	/// <summary>
	/// Summary description for frmPreview.
	/// </summary>
	public class frmPreview : System.Windows.Forms.Form
	{
		private ActiveReport3 myReport = null;
		private DataDynamics.ActiveReports.Viewer.Viewer viewer1;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.PictureBox picToolBar;
		private System.Windows.Forms.Button btnExcelExport;
		private System.Windows.Forms.Button btnPdfExport;
		private System.Windows.Forms.ImageList imageButtons;
		private System.ComponentModel.IContainer components;

		public frmPreview(ActiveReport3 theReport)
		{
			myReport = theReport;
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmPreview));
			this.viewer1 = new DataDynamics.ActiveReports.Viewer.Viewer();
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.picToolBar = new System.Windows.Forms.PictureBox();
			this.btnExcelExport = new System.Windows.Forms.Button();
			this.btnPdfExport = new System.Windows.Forms.Button();
			this.imageButtons = new System.Windows.Forms.ImageList(this.components);
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// viewer1
			// 
			this.viewer1.BackColor = System.Drawing.SystemColors.Control;
			this.viewer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.viewer1.Location = new System.Drawing.Point(0, 0);
			this.viewer1.Name = "viewer1";
			this.viewer1.ReportViewer.CurrentPage = 0;
			this.viewer1.ReportViewer.MultiplePageCols = 3;
			this.viewer1.ReportViewer.MultiplePageRows = 2;
			this.viewer1.Size = new System.Drawing.Size(912, 558);
			this.viewer1.TabIndex = 0;
			this.viewer1.TableOfContents.Text = "Contents";
			this.viewer1.TableOfContents.Width = 200;
			this.viewer1.Toolbar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			// 
			// panel1
			// 
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel1.Controls.Add(this.btnPdfExport);
			this.panel1.Controls.Add(this.btnExcelExport);
			this.panel1.Controls.Add(this.picToolBar);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(912, 40);
			this.panel1.TabIndex = 1;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.viewer1);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(0, 40);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(912, 558);
			this.panel2.TabIndex = 2;
			// 
			// picToolBar
			// 
			this.picToolBar.Dock = System.Windows.Forms.DockStyle.Fill;
			this.picToolBar.Location = new System.Drawing.Point(0, 0);
			this.picToolBar.Name = "picToolBar";
			this.picToolBar.Size = new System.Drawing.Size(908, 36);
			this.picToolBar.TabIndex = 0;
			this.picToolBar.TabStop = false;
			// 
			// btnExcelExport
			// 
			this.btnExcelExport.Dock = System.Windows.Forms.DockStyle.Left;
			this.btnExcelExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnExcelExport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnExcelExport.ImageIndex = 0;
			this.btnExcelExport.ImageList = this.imageButtons;
			this.btnExcelExport.Location = new System.Drawing.Point(0, 0);
			this.btnExcelExport.Name = "btnExcelExport";
			this.btnExcelExport.Size = new System.Drawing.Size(120, 36);
			this.btnExcelExport.TabIndex = 1;
			this.btnExcelExport.Text = "&Excel-Export";
			this.btnExcelExport.Click += new System.EventHandler(this.btnExcelExport_Click);
			// 
			// btnPdfExport
			// 
			this.btnPdfExport.Dock = System.Windows.Forms.DockStyle.Left;
			this.btnPdfExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnPdfExport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnPdfExport.ImageIndex = 1;
			this.btnPdfExport.ImageList = this.imageButtons;
			this.btnPdfExport.Location = new System.Drawing.Point(120, 0);
			this.btnPdfExport.Name = "btnPdfExport";
			this.btnPdfExport.Size = new System.Drawing.Size(120, 36);
			this.btnPdfExport.TabIndex = 2;
			this.btnPdfExport.Text = "&PDF-Export";
			this.btnPdfExport.Click += new System.EventHandler(this.btnPdfExport_Click);
			// 
			// imageButtons
			// 
			this.imageButtons.ImageSize = new System.Drawing.Size(19, 19);
			this.imageButtons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageButtons.ImageStream")));
			this.imageButtons.TransparentColor = System.Drawing.Color.White;
			// 
			// frmPreview
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(912, 598);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "frmPreview";
			this.Text = "Print Preview ...";
			this.Load += new System.EventHandler(this.frmPreview_Load);
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void frmPreview_Load(object sender, System.EventArgs e)
		{
			if(myReport != null)
			{
				viewer1.ReportViewer.Zoom = 0.8f;
				//rptRules r = new rptRules();
				viewer1.Document = myReport.Document;
				myReport.Run();
			}
		}

		private void btnExcelExport_Click(object sender, System.EventArgs e)
		{
			try
			{
				string fileName = "";
				SaveFileDialog dlg = new SaveFileDialog();
				dlg.Filter = "Excel-files (*.xls)|*.xls|All filed (*.*)|*.*";
				if(dlg.ShowDialog() == DialogResult.OK)
				{
					fileName = dlg.FileName;
					DataDynamics.ActiveReports.Export.Xls.XlsExport myXLS = new DataDynamics.ActiveReports.Export.Xls.XlsExport();
					myXLS.UseCellMerging = false;
					//myXLS.FileFormat = FileFormat.Xls95;
					myXLS.Export(myReport.Document, dlg.FileName);
					//xPDF.Export(rptTemp.Document,Application.StartupPath.ToString().Substring(0,  Application.StartupPath.ToString().Length - 10 ) + @"\" + txtAttachName.Text);
				}
			}
			catch (Exception exx)
			{
				MessageBox.Show(this, "Error Creating Excel file.\n"+exx.ToString());
			}	
		}

		private void btnPdfExport_Click(object sender, System.EventArgs e)
		{
			DataDynamics.ActiveReports.Export.Pdf.PdfExport xPDF = new DataDynamics.ActiveReports.Export.Pdf.PdfExport();
			try
			{
				string fileName = "";
				SaveFileDialog dlg = new SaveFileDialog();
				dlg.Filter = "PDF-files (*.pdf)|*.pdf|All filed (*.*)|*.*";
				if(dlg.ShowDialog() == DialogResult.OK)
				{
					fileName = dlg.FileName;
					xPDF.Export(myReport.Document, fileName);
				}
			}
			catch (Exception exx)
			{
				MessageBox.Show(this, "Error Creating PDF file.\n"+exx.ToString());
			}	
		}
	}
}
