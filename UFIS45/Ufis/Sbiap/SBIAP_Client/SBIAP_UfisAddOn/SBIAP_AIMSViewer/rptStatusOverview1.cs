using System;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using Ufis.Utils;

namespace AIMSViewer
{
	public class rptStatusOverview : ActiveReport3
	{
		private AxTABLib.AxTAB tabStatus = null; 
		private string strViewName = "";
		private int currLine = 0;
		public rptStatusOverview(AxTABLib.AxTAB tStatus, string sViewName)
		{
			strViewName = sViewName;
			tabStatus = tStatus;
			InitializeReport();
		}

		private void rptStatusOverview_FetchData(object sender, DataDynamics.ActiveReports.ActiveReport3.FetchEventArgs eArgs)
		{
			DateTime dt;
			sep1.Y1 = 0; 
			sep2.Y1 =  0;
			sep4.Y1 =  0;
			sep9.Y1 =  0;
			sep10.Y1 =  0;
			sep11.Y1 =  0;
			sep12.Y1 =  0;

			this.txtDevice.Text		= " " + tabStatus.GetFieldValue(currLine, "NAME");
			this.txtSubsystem.Text	= " " + tabStatus.GetFieldValue(currLine, "SUBS");
			this.txtOriginator.Text	= " " + tabStatus.GetFieldValue(currLine, "ORIG");
			this.txtType.Text		= " " + tabStatus.GetFieldValue(currLine, "TYPE");
			this.txtStatus.Text		= " " + tabStatus.GetFieldValue(currLine, "STAT");
			if (tabStatus.GetFieldValue(currLine, "LSTU") != "")
			{
				dt = UT.CedaFullDateToDateTime(tabStatus.GetFieldValue(currLine, "LSTU"));
				this.txtTime.Text = " " + dt.ToShortTimeString() + "/" + dt.Day.ToString();
			}

			if (currLine == tabStatus.GetLineCount())
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
				currLine++;
			}
		}

		private void rptStatusOverview_ReportStart(object sender, System.EventArgs eArgs)
		{
			DateTime d		= DateTime.Now;
			txtDate.Text	= d.ToString();
			txtViewName.Text = "View Name: " + strViewName;
		}

		private void Detail_BeforePrint(object sender, System.EventArgs eArgs)
		{
			float ilHeight = this.txtStatus.Height; 
			sep1.Y2 = ilHeight;
			sep2.Y2 = ilHeight;
			sep4.Y2 = ilHeight;
			sep9.Y2 = ilHeight;
			sep10.Y2 = ilHeight;
			sep11.Y2 = ilHeight;
			sep12.Y2 = ilHeight;
			sepHorz.Y1 = ilHeight;
			sepHorz.Y2 = ilHeight;

		}



		#region ActiveReports Designer generated code
		private PageHeader PageHeader = null;
		private Label Label1 = null;
		private Label txtViewName = null;
		private Label lblSubsystem = null;
		private Label lblDevice = null;
		private Label lblOriginator = null;
		private Label lblType = null;
		private Label lblStatus = null;
		private Label LblTime = null;
		private Line Line1 = null;
		private Line Line2 = null;
		private Line Line3 = null;
		private Line Line9 = null;
		private Line Line10 = null;
		private Line Line11 = null;
		private Line Line12 = null;
		private Line Line13 = null;
		private Line Line14 = null;
		private Detail Detail = null;
		private TextBox txtOriginator = null;
		private Line sep9 = null;
		private TextBox txtType = null;
		private TextBox txtStatus = null;
		private TextBox txtTime = null;
		private TextBox txtSubsystem = null;
		private TextBox txtMin = null;
		private TextBox txtDevice = null;
		private Line sep1 = null;
		private Line sep2 = null;
		private Line sep4 = null;
		private Line sep10 = null;
		private Line sep11 = null;
		private Line sep12 = null;
		private Line sepHorz = null;
		private PageFooter PageFooter = null;
		private Label lblPrintedOn = null;
		private TextBox txtDate = null;
		private Label lblPageNo = null;
		private TextBox txtPageNo = null;
		private Label lblDelimiter = null;
		private TextBox txtPageTotal = null;
		public void InitializeReport()
		{
			this.LoadLayout(this.GetType(), "AIMSViewer.rptStatusOverview.rpx");
			this.PageHeader = ((DataDynamics.ActiveReports.PageHeader)(this.Sections["PageHeader"]));
			this.Detail = ((DataDynamics.ActiveReports.Detail)(this.Sections["Detail"]));
			this.PageFooter = ((DataDynamics.ActiveReports.PageFooter)(this.Sections["PageFooter"]));
			this.Label1 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[0]));
			this.txtViewName = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[1]));
			this.lblSubsystem = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[2]));
			this.lblDevice = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[3]));
			this.lblOriginator = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[4]));
			this.lblType = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[5]));
			this.lblStatus = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[6]));
			this.LblTime = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[7]));
			this.Line1 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[8]));
			this.Line2 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[9]));
			this.Line3 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[10]));
			this.Line9 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[11]));
			this.Line10 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[12]));
			this.Line11 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[13]));
			this.Line12 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[14]));
			this.Line13 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[15]));
			this.Line14 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[16]));
			this.txtOriginator = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[0]));
			this.sep9 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[1]));
			this.txtType = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[2]));
			this.txtStatus = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[3]));
			this.txtTime = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[4]));
			this.txtSubsystem = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[5]));
			this.txtMin = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[6]));
			this.txtDevice = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[7]));
			this.sep1 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[8]));
			this.sep2 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[9]));
			this.sep4 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[10]));
			this.sep10 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[11]));
			this.sep11 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[12]));
			this.sep12 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[13]));
			this.sepHorz = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[14]));
			this.lblPrintedOn = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[0]));
			this.txtDate = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[1]));
			this.lblPageNo = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[2]));
			this.txtPageNo = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[3]));
			this.lblDelimiter = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[4]));
			this.txtPageTotal = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[5]));
			// Attach Report Events
			this.FetchData += new DataDynamics.ActiveReports.ActiveReport3.FetchEventHandler(this.rptStatusOverview_FetchData);
			this.ReportStart += new System.EventHandler(this.rptStatusOverview_ReportStart);
			this.Detail.BeforePrint += new System.EventHandler(this.Detail_BeforePrint);
		}

		#endregion
	}
}
