using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Ufis.Data;
using Ufis.Utils;

namespace AIMSViewer
{
	/// <summary>
	/// Summary description for Symbol.
	/// </summary>
	public class Symbol : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.NumericUpDown udHeight;
		private System.Windows.Forms.NumericUpDown udWidth;
		private System.Windows.Forms.Label lblHeight;
		private System.Windows.Forms.Label lblWidth;
		private System.Windows.Forms.Label lblBrace;
		private System.Windows.Forms.Label lblInMeters;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Button btnBrowse;
		private System.Windows.Forms.Label lblSymbol;
		private System.Windows.Forms.ComboBox cbSymbols;
		private System.Windows.Forms.PictureBox pbPreview;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.Button bntDelete;
		private System.Windows.Forms.Button lblAdd;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.ComboBox cbSubsystem;
		private System.Windows.Forms.TextBox txtName;
		private System.Windows.Forms.TextBox txtLabel;
		private System.Windows.Forms.ComboBox cbSymbolGroup;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.TextBox txtPath;

		#region implementation
		#endregion
		public Symbol()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Symbol));
			this.label1 = new System.Windows.Forms.Label();
			this.cbSubsystem = new System.Windows.Forms.ComboBox();
			this.label2 = new System.Windows.Forms.Label();
			this.txtName = new System.Windows.Forms.TextBox();
			this.txtLabel = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.txtPath = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.btnBrowse = new System.Windows.Forms.Button();
			this.udHeight = new System.Windows.Forms.NumericUpDown();
			this.udWidth = new System.Windows.Forms.NumericUpDown();
			this.lblHeight = new System.Windows.Forms.Label();
			this.lblWidth = new System.Windows.Forms.Label();
			this.lblBrace = new System.Windows.Forms.Label();
			this.lblInMeters = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.cbSymbolGroup = new System.Windows.Forms.ComboBox();
			this.lblSymbol = new System.Windows.Forms.Label();
			this.cbSymbols = new System.Windows.Forms.ComboBox();
			this.pbPreview = new System.Windows.Forms.PictureBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnSave = new System.Windows.Forms.Button();
			this.btnClose = new System.Windows.Forms.Button();
			this.bntDelete = new System.Windows.Forms.Button();
			this.lblAdd = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.udHeight)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.udWidth)).BeginInit();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(16, 76);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(72, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Subsystem:";
			// 
			// cbSubsystem
			// 
			this.cbSubsystem.Items.AddRange(new object[] {
															 "BAS",
															 "FDA",
															 "CASS",
															 "SCADA"});
			this.cbSubsystem.Location = new System.Drawing.Point(96, 72);
			this.cbSubsystem.Name = "cbSubsystem";
			this.cbSubsystem.Size = new System.Drawing.Size(192, 21);
			this.cbSubsystem.TabIndex = 1;
			this.cbSubsystem.Text = "SCADA";
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label2.Location = new System.Drawing.Point(16, 104);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(72, 16);
			this.label2.TabIndex = 2;
			this.label2.Text = "Identifier:";
			// 
			// txtName
			// 
			this.txtName.Location = new System.Drawing.Point(96, 100);
			this.txtName.Name = "txtName";
			this.txtName.Size = new System.Drawing.Size(192, 20);
			this.txtName.TabIndex = 3;
			this.txtName.Text = "Elevator 1A23F4";
			// 
			// txtLabel
			// 
			this.txtLabel.Location = new System.Drawing.Point(96, 128);
			this.txtLabel.Name = "txtLabel";
			this.txtLabel.Size = new System.Drawing.Size(192, 20);
			this.txtLabel.TabIndex = 5;
			this.txtLabel.Text = "Elev.";
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label3.Location = new System.Drawing.Point(24, 132);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(72, 16);
			this.label3.TabIndex = 4;
			this.label3.Text = "Label:";
			// 
			// txtPath
			// 
			this.txtPath.Location = new System.Drawing.Point(96, 420);
			this.txtPath.Name = "txtPath";
			this.txtPath.Size = new System.Drawing.Size(264, 20);
			this.txtPath.TabIndex = 7;
			this.txtPath.Text = "C:\\AIMSViewer\\Symbol Images\\Elevator1A23F4";
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label4.Location = new System.Drawing.Point(16, 424);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(68, 16);
			this.label4.TabIndex = 6;
			this.label4.Text = "Image:";
			// 
			// btnBrowse
			// 
			this.btnBrowse.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnBrowse.Location = new System.Drawing.Point(296, 256);
			this.btnBrowse.Name = "btnBrowse";
			this.btnBrowse.Size = new System.Drawing.Size(64, 20);
			this.btnBrowse.TabIndex = 8;
			this.btnBrowse.Text = "Browse ...";
			this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
			// 
			// udHeight
			// 
			this.udHeight.Location = new System.Drawing.Point(96, 192);
			this.udHeight.Maximum = new System.Decimal(new int[] {
																	 10000,
																	 0,
																	 0,
																	 0});
			this.udHeight.Minimum = new System.Decimal(new int[] {
																	 1,
																	 0,
																	 0,
																	 0});
			this.udHeight.Name = "udHeight";
			this.udHeight.Size = new System.Drawing.Size(72, 20);
			this.udHeight.TabIndex = 12;
			this.udHeight.Value = new System.Decimal(new int[] {
																   20,
																   0,
																   0,
																   0});
			// 
			// udWidth
			// 
			this.udWidth.Location = new System.Drawing.Point(96, 168);
			this.udWidth.Maximum = new System.Decimal(new int[] {
																	10000,
																	0,
																	0,
																	0});
			this.udWidth.Minimum = new System.Decimal(new int[] {
																	1,
																	0,
																	0,
																	0});
			this.udWidth.Name = "udWidth";
			this.udWidth.Size = new System.Drawing.Size(72, 20);
			this.udWidth.TabIndex = 11;
			this.udWidth.Value = new System.Decimal(new int[] {
																  20,
																  0,
																  0,
																  0});
			// 
			// lblHeight
			// 
			this.lblHeight.BackColor = System.Drawing.Color.Transparent;
			this.lblHeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblHeight.Location = new System.Drawing.Point(16, 196);
			this.lblHeight.Name = "lblHeight";
			this.lblHeight.Size = new System.Drawing.Size(72, 16);
			this.lblHeight.TabIndex = 10;
			this.lblHeight.Text = "Height:";
			// 
			// lblWidth
			// 
			this.lblWidth.BackColor = System.Drawing.Color.Transparent;
			this.lblWidth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblWidth.Location = new System.Drawing.Point(16, 172);
			this.lblWidth.Name = "lblWidth";
			this.lblWidth.Size = new System.Drawing.Size(64, 16);
			this.lblWidth.TabIndex = 9;
			this.lblWidth.Text = "Width:";
			// 
			// lblBrace
			// 
			this.lblBrace.BackColor = System.Drawing.Color.Transparent;
			this.lblBrace.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblBrace.Location = new System.Drawing.Point(176, 160);
			this.lblBrace.Name = "lblBrace";
			this.lblBrace.Size = new System.Drawing.Size(24, 60);
			this.lblBrace.TabIndex = 14;
			this.lblBrace.Text = "}";
			// 
			// lblInMeters
			// 
			this.lblInMeters.BackColor = System.Drawing.Color.Transparent;
			this.lblInMeters.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblInMeters.Location = new System.Drawing.Point(208, 192);
			this.lblInMeters.Name = "lblInMeters";
			this.lblInMeters.Size = new System.Drawing.Size(64, 16);
			this.lblInMeters.TabIndex = 13;
			this.lblInMeters.Text = "in pixels";
			// 
			// label5
			// 
			this.label5.BackColor = System.Drawing.Color.Transparent;
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label5.Location = new System.Drawing.Point(16, 232);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(72, 16);
			this.label5.TabIndex = 15;
			this.label5.Text = "Group:";
			// 
			// cbSymbolGroup
			// 
			this.cbSymbolGroup.Items.AddRange(new object[] {
															   "BAS",
															   "FDA",
															   "CASS",
															   "SCADA"});
			this.cbSymbolGroup.Location = new System.Drawing.Point(96, 228);
			this.cbSymbolGroup.Name = "cbSymbolGroup";
			this.cbSymbolGroup.Size = new System.Drawing.Size(192, 21);
			this.cbSymbolGroup.TabIndex = 16;
			this.cbSymbolGroup.Text = "Elevators";
			// 
			// lblSymbol
			// 
			this.lblSymbol.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblSymbol.Location = new System.Drawing.Point(16, 24);
			this.lblSymbol.Name = "lblSymbol";
			this.lblSymbol.Size = new System.Drawing.Size(72, 23);
			this.lblSymbol.TabIndex = 17;
			this.lblSymbol.Text = "Symbol:";
			// 
			// cbSymbols
			// 
			this.cbSymbols.Location = new System.Drawing.Point(96, 24);
			this.cbSymbols.Name = "cbSymbols";
			this.cbSymbols.Size = new System.Drawing.Size(192, 21);
			this.cbSymbols.TabIndex = 18;
			this.cbSymbols.Text = "Symbol";
			this.cbSymbols.SelectedIndexChanged += new System.EventHandler(this.cbSymbols_SelectedIndexChanged);
			// 
			// pbPreview
			// 
			this.pbPreview.Location = new System.Drawing.Point(96, 260);
			this.pbPreview.Name = "pbPreview";
			this.pbPreview.Size = new System.Drawing.Size(192, 148);
			this.pbPreview.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pbPreview.TabIndex = 19;
			this.pbPreview.TabStop = false;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnSave);
			this.panel1.Controls.Add(this.btnClose);
			this.panel1.Controls.Add(this.bntDelete);
			this.panel1.Controls.Add(this.lblAdd);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 450);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(368, 48);
			this.panel1.TabIndex = 20;
			// 
			// btnSave
			// 
			this.btnSave.BackColor = System.Drawing.Color.Transparent;
			this.btnSave.Dock = System.Windows.Forms.DockStyle.Left;
			this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnSave.ImageIndex = 2;
			this.btnSave.Location = new System.Drawing.Point(232, 0);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(72, 48);
			this.btnSave.TabIndex = 16;
			this.btnSave.Text = "   &Save";
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// btnClose
			// 
			this.btnClose.BackColor = System.Drawing.Color.Transparent;
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnClose.Dock = System.Windows.Forms.DockStyle.Left;
			this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnClose.ImageIndex = 4;
			this.btnClose.Location = new System.Drawing.Point(160, 0);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(72, 48);
			this.btnClose.TabIndex = 17;
			this.btnClose.Text = "   &Close";
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// bntDelete
			// 
			this.bntDelete.BackColor = System.Drawing.Color.Transparent;
			this.bntDelete.Dock = System.Windows.Forms.DockStyle.Left;
			this.bntDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.bntDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.bntDelete.ImageIndex = 1;
			this.bntDelete.Location = new System.Drawing.Point(88, 0);
			this.bntDelete.Name = "bntDelete";
			this.bntDelete.Size = new System.Drawing.Size(72, 48);
			this.bntDelete.TabIndex = 15;
			this.bntDelete.Text = "    &Delete";
			this.bntDelete.Click += new System.EventHandler(this.bntDelete_Click);
			// 
			// lblAdd
			// 
			this.lblAdd.BackColor = System.Drawing.Color.Transparent;
			this.lblAdd.Dock = System.Windows.Forms.DockStyle.Left;
			this.lblAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.lblAdd.ImageIndex = 0;
			this.lblAdd.Location = new System.Drawing.Point(0, 0);
			this.lblAdd.Name = "lblAdd";
			this.lblAdd.Size = new System.Drawing.Size(88, 48);
			this.lblAdd.TabIndex = 14;
			this.lblAdd.Text = "&New Symbol";
			this.lblAdd.Click += new System.EventHandler(this.lblAdd_Click);
			// 
			// label6
			// 
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label6.Location = new System.Drawing.Point(16, 260);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(68, 16);
			this.label6.TabIndex = 21;
			this.label6.Text = "Preview:";
			// 
			// Symbol
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(368, 498);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.pbPreview);
			this.Controls.Add(this.cbSymbols);
			this.Controls.Add(this.lblSymbol);
			this.Controls.Add(this.cbSymbolGroup);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.lblBrace);
			this.Controls.Add(this.lblInMeters);
			this.Controls.Add(this.udHeight);
			this.Controls.Add(this.udWidth);
			this.Controls.Add(this.lblHeight);
			this.Controls.Add(this.lblWidth);
			this.Controls.Add(this.btnBrowse);
			this.Controls.Add(this.txtPath);
			this.Controls.Add(this.txtLabel);
			this.Controls.Add(this.txtName);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.cbSubsystem);
			this.Controls.Add(this.label1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "Symbol";
			this.Text = "Symbol Definition ...";
			this.Load += new System.EventHandler(this.Symbol_Load);
			((System.ComponentModel.ISupportInitialize)(this.udHeight)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.udWidth)).EndInit();
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void Symbol_Load(object sender, System.EventArgs e)
		{
			// add other systems to list of sub systems
			if (frmData.OtherSystems != null)
			{
				this.cbSubsystem.Items.AddRange(frmData.OtherSystems);
			}

			// security requirement for subsystems
			for(int i = this.cbSubsystem.Items.Count - 1; i >= 0; i--)
			{
				string prv = "Subsystem" + (string)this.cbSubsystem.Items[i];
				switch(frmData.GetPrivileges(prv))
				{
					case "-":
						this.cbSubsystem.Items.RemoveAt(i);
						break;
					case "0":
						this.cbSubsystem.Items.RemoveAt(i);
						break;
				}

			}

			IDatabase myDB = UT.GetMemDB();
			ITable	myGrps = myDB["ASG"];
			for (int i = 0; i < myGrps.Count;i++)
			{
				IRow myRow = myGrps[i];
				this.cbSymbolGroup.Items.Add(myRow["NAME"]);
			}

			ITable	myTable= myDB["ASD"];	
			for (int i = 0; i < myTable.Count;i++)
			{
				IRow myRow = myTable[i];
				this.cbSymbols.Items.Add(myRow["NAME"]);
			}

			this.cbSymbols.DropDownStyle = ComboBoxStyle.DropDownList;
			if (this.cbSymbols.Items.Count > 0)
			{
				this.cbSymbols.SelectedIndex = 0;
			}
			else
			{
			}
		}

		private void cbSymbols_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			string name = (string)this.cbSymbols.SelectedItem;		
			IDatabase myDB = UT.GetMemDB();
			ITable	myTable= myDB["ASD"];	
			IRow[]	myRows = myTable.RowsByIndexValue("NAME",name);
			if (myRows.Length == 1)
			{
				this.txtName.Text = name;		
				this.cbSubsystem.SelectedItem = myRows[0]["ORIG"];
				this.txtLabel.Text = myRows[0]["LABL"];
				this.txtPath.Text  = myRows[0]["IMAG"];	
				this.udWidth.Value= decimal.Parse(myRows[0]["WIDT"]);
				this.udHeight.Value= decimal.Parse(myRows[0]["HEIG"]);
				this.cbSymbolGroup.SelectedItem = this.NameOfSymbolGroup(myRows[0]["SGRP"]);
				this.pbPreview.Image = new Bitmap(myRows[0]["IMAG"]);
			}

			this.cbSymbols.DropDownStyle = ComboBoxStyle.DropDownList;

		}

		private void lblAdd_Click(object sender, System.EventArgs e)
		{
			this.cbSymbols.DropDownStyle = ComboBoxStyle.DropDown;
		}

		private void bntDelete_Click(object sender, System.EventArgs e)
		{
		
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
		
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			string name = (string)this.cbSymbols.SelectedItem;		
			IDatabase myDB = UT.GetMemDB();
			ITable	myTable= myDB["ASD"];	
			IRow[]	myRows = myTable.RowsByIndexValue("NAME",name);
			if (myRows.Length == 1)
			{
				this.txtName.Text	= name;		
				myRows[0]["ORIG"]	= (string)this.cbSubsystem.SelectedItem;
				myRows[0]["LABL"]	= this.txtLabel.Text;
				myRows[0]["IMAG"]	= this.txtPath.Text;	
				myRows[0]["WIDT"]	= this.udWidth.Value.ToString();
				myRows[0]["HEIG"]	= this.udHeight.Value.ToString();
				myRows[0]["SGRP"]	= this.UrnoOfSymbolGroup((string)this.cbSymbolGroup.SelectedItem);
				myRows[0]["USEU"]	= UT.UserName;
				myRows[0]["LSTU"]	= UT.DateTimeToCeda(DateTime.Now.ToUniversalTime());
				myRows[0].Status	= State.Modified;
				myTable.Save(myRows[0]);
			}
			else
			{
				StringBuilder data = new StringBuilder();
				
				data.Append(myDB.GetNextUrno());
				data.Append(",");

				data.Append(this.txtName.Text);
				data.Append(",");

				data.Append(this.txtLabel.Text);
				data.Append(",");

				data.Append(this.txtPath.Text);
				data.Append(",");

				data.Append(this.udWidth.Value.ToString());
				data.Append(",");

				data.Append(this.udHeight.Value.ToString());
				data.Append(",");

				data.Append(this.cbSymbolGroup.SelectedText);
				data.Append(",");

				data.Append(UT.UserName);
				data.Append(",");
				data.Append(UT.UserName);
				data.Append(",");
				data.Append(UT.DateTimeToCeda(DateTime.Now.ToUniversalTime()));
				data.Append(",");
				data.Append(UT.DateTimeToCeda(DateTime.Now.ToUniversalTime()));

				IRow myRow = myTable.Create(data.ToString());
				myRow.Status = State.Created;
				myTable.Save(myRow);
				
			}
		}
			
		private string NameOfSymbolGroup(string urno)
		{
			IDatabase myDB = UT.GetMemDB();
			ITable	myTable= myDB["ASG"];	
			IRow[]	myRows = myTable.RowsByIndexValue("URNO",urno);
			if (myRows.Length == 1)
			{
				return myRows[0]["NAME"];
			}
			else
			{
				return string.Empty;
			}
		}

		private string UrnoOfSymbolGroup(string name)
		{
			IDatabase myDB = UT.GetMemDB();
			ITable	myTable= myDB["ASG"];	
			IRow[]	myRows = myTable.RowsByIndexValue("NAME",name);
			if (myRows.Length == 1)
			{
				return myRows[0]["URNO"];
			}
			else
			{
				return string.Empty;
			}
		}

		private void btnBrowse_Click(object sender, System.EventArgs e)
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Filter = "TIF-filed (*.tif)|*.tif|bmp-files (*.bmp)|*.bmp|GIF-files (*.gif)|*.gif|JPG-files (*.jpg)|*.jpg";//"All filed (*.*)|*.*";
			dlg.InitialDirectory = frmData.GetThis().InitialDirectory;
			if(dlg.ShowDialog() == DialogResult.OK)
			{
				this.pbPreview.Image = new Bitmap(dlg.FileName);
				this.txtPath.Text	 = dlg.FileName;
			}
		}

	}
}
