using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace PictureViewer
{
	/// <summary>
	/// The PictureBoxEx3 class is derived from <see cref="System.Windows.Forms.UserControl"/> and implements an extended version of
	/// the <see cref="System.Windows.Forms.PictureBox"/>
	/// </summary>
	/// <remarks>None</remarks>
	public class PictureBoxEx3 : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.HScrollBar hScrollBar1;
		private System.Windows.Forms.VScrollBar vScrollBar1;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private	System.Drawing.Image			image = null;
		private	float							scale = 1.0F;
		private	Point							srcPos	= new Point(0,0);
		private	Size							srcSize	= new Size(0,0);
		private	Size							dstSize	= new Size(0,0);	
		private	Point							currMousePos = new Point(0,0);
		private	int								minScrollBarSize = 10;

		/// <summary>
		/// This delegate can be used to paint additional graphic on the image of the picture box
		/// </summary>
		/// <remarks>None.</remarks>
		public delegate void On_Paint(object sender, PaintEventArgs e);

		/// <summary>
		/// This is the <B>event</B> for the delegate <see cref="On_Paint"/>
		/// </summary>
		/// <remarks>None</remarks>
		public event	On_Paint	OnPaintHandler;
 	
		/// <summary>
		/// The default constructor for an instance of this class.
		/// </summary>
		/// <remarks>None</remarks>
		public PictureBoxEx3()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
			this.SetStyle(ControlStyles.UserPaint|ControlStyles.AllPaintingInWmPaint|ControlStyles.DoubleBuffer,true);

		}

		/// <summary> 
		/// Clean up any resources being used by this instance.
		/// </summary>
		/// <remarks>None</remarks>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.hScrollBar1 = new System.Windows.Forms.HScrollBar();
			this.vScrollBar1 = new System.Windows.Forms.VScrollBar();
			this.SuspendLayout();
			// 
			// hScrollBar1
			// 
			this.hScrollBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.hScrollBar1.Location = new System.Drawing.Point(0, 133);
			this.hScrollBar1.Name = "hScrollBar1";
			this.hScrollBar1.Size = new System.Drawing.Size(133, 17);
			this.hScrollBar1.TabIndex = 0;
			this.hScrollBar1.ValueChanged += new System.EventHandler(this.hScrollBar1_ValueChanged);
			this.hScrollBar1.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar1_Scroll);
			// 
			// vScrollBar1
			// 
			this.vScrollBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.vScrollBar1.Location = new System.Drawing.Point(133, 0);
			this.vScrollBar1.Name = "vScrollBar1";
			this.vScrollBar1.Size = new System.Drawing.Size(17, 133);
			this.vScrollBar1.TabIndex = 1;
			this.vScrollBar1.ValueChanged += new System.EventHandler(this.vScrollBar1_ValueChanged);
			this.vScrollBar1.Scroll += new System.Windows.Forms.ScrollEventHandler(this.vScrollBar1_Scroll);
			// 
			// PictureBoxEx3
			// 
			this.Controls.Add(this.vScrollBar1);
			this.Controls.Add(this.hScrollBar1);
			this.Name = "PictureBoxEx3";
			this.Resize += new System.EventHandler(this.PictureBoxEx3_Resize);
			this.Load += new System.EventHandler(this.PictureBoxEx3_Load);
			this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PictureBoxEx3_MouseMove);
			this.ResumeLayout(false);

		}
		#endregion

		private void PictureBoxEx3_Load(object sender, System.EventArgs e)
		{
		
		}

		/// <summary>
		/// Returns or sets the currently used image of this picture box.
		/// </summary>
		/// <remarks>None</remarks>
		public Image Image
		{
			get
			{
				return this.image;
			}
			set
			{
				if (this.image != value)
				{
					this.image = value;
					UpdateControl(this.scale);
				}
			}
		}

		/// <summary>
		/// Returns or sets the currently used scale factor of this picture box.
		/// </summary>
		/// <remarks>None</remarks>
		public new float	Scale
		{
			get
			{
				return this.scale;
			}
			set
			{
				if (this.scale != value)
				{
					float oldScale = this.scale;
					this.scale = value;
					UpdateControl(oldScale);
				}
			}
		}

		/// <summary>
		/// Returns the current mouse position in screen coordinates.
		/// </summary>
		/// <value>Point in screen coordinates</value>
		/// <remarks>None</remarks>
		public Point CurrentMousePosition
		{
			get
			{
				return this.currMousePos;
			}
		}

		/// <summary>
		/// Moves the sliders of the scroll bars to the exact position
		/// </summary>
		/// <param name="origin">The point of the image in image coordinates</param>
		/// <remarks>None</remarks>
		public void ScrollToExactPosition(Point origin)
		{
			if (this.vScrollBar1.Visible)
			{
				int newValue = (int)Math.Floor(origin.Y * this.scale);
				if (newValue < this.vScrollBar1.Minimum)
				{
					this.vScrollBar1.Value = this.vScrollBar1.Minimum;
				}
				else if (newValue + this.vScrollBar1.LargeChange > this.vScrollBar1.Maximum)
				{
					this.vScrollBar1.Value = this.vScrollBar1.Maximum + 1 - this.vScrollBar1.LargeChange;
				}
				else
				{
					this.vScrollBar1.Value = newValue;
				}
			}

			if (this.hScrollBar1.Visible)
			{
				int newValue = (int)Math.Floor(origin.X * this.scale);
				if (newValue < this.hScrollBar1.Minimum)
				{
					this.hScrollBar1.Value = 0;
				}
				else if (newValue + this.hScrollBar1.LargeChange > this.hScrollBar1.Maximum)
				{
					this.hScrollBar1.Value = this.hScrollBar1.Maximum + 1 - this.hScrollBar1.LargeChange;
				}
				else
				{
					this.hScrollBar1.Value	= newValue;
				}
			}

			this.Refresh();

		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="origin"></param>
		public void ScrollToPosition(Point origin)
		{
			if (this.vScrollBar1.Visible)
			{
				int newValue = (int)Math.Floor(origin.Y * this.scale) - this.vScrollBar1.LargeChange / 2;
				if (newValue < this.vScrollBar1.Minimum)
				{
					this.vScrollBar1.Value = this.vScrollBar1.Minimum;
				}
				else if (newValue + this.vScrollBar1.LargeChange > this.vScrollBar1.Maximum)
				{
					this.vScrollBar1.Value = this.vScrollBar1.Maximum + 1 - this.vScrollBar1.LargeChange;
				}
				else
				{
					this.vScrollBar1.Value = newValue;
				}
			}

			if (this.hScrollBar1.Visible)
			{
				int newValue = (int)Math.Floor(origin.X * this.scale) - this.hScrollBar1.LargeChange / 2;
				if (newValue < this.hScrollBar1.Minimum)
				{
					this.hScrollBar1.Value = 0;
				}
				else if (newValue + this.hScrollBar1.LargeChange > this.hScrollBar1.Maximum)
				{
					this.hScrollBar1.Value = this.hScrollBar1.Maximum + 1 - this.hScrollBar1.LargeChange;
				}
				else
				{
					this.hScrollBar1.Value	= newValue;
				}

			}

			this.Refresh();
		}

		/// <summary>
		/// Display a rectangular detail view of the image currently loaded.
		/// </summary>
		/// <param name="ul">Upper left point of the rectangle in image coordinates</param>
		/// <param name="lr">Lower right point of the rectangle in image coordinates</param>
		/// <returns>true if operation completes successfully, false if not.</returns>
		/// <remarks>None.</remarks>
		public bool ZoomWindow(Point ul,Point lr)
		{
			Size size = new Size(lr.X - ul.X,lr.Y - ul.Y);
			if (size.Width == 0 || size.Height == 0)
				return false;

			Rectangle rect;
			if (size.Width < 0)
			{
				if (size.Height < 0)
				{
					size.Width *= -1;
					size.Height *= -1;
					ul = (Point)lr;
					rect = new Rectangle(ul,size);
				}
				else
				{
					size.Width *= -1;
					ul.X = lr.X;
					rect = new Rectangle(ul,size);
				}
			}
			else if (size.Height < 0)
			{
				size.Height *= -1;
				ul.Y = lr.Y;
				rect = new Rectangle(ul,size);
			}
			else
			{
				rect = new Rectangle(ul,size);
			}

			float factorW = (float)this.Width  / rect.Width;
			float factorH = (float)this.Height / rect.Height;
			SizeF scrollSize = new SizeF(factorW,factorH);
			this.Scale = Math.Min(scrollSize.Width,scrollSize.Height);
			this.ScrollToExactPosition(ul);
			return true;
		}

		/// <summary>
		/// Returns the huge scale factor possible for this image with regard to the current control size
		/// </summary>
		/// <returns>huge scale factor</returns>
		/// <remarks>None</remarks>
		public float MaxScale()
		{
			float factorW = (float)this.Width  / this.minScrollBarSize;
			float factorH = (float)this.Height / this.minScrollBarSize;

			SizeF scrollSize = new SizeF(this.Width * factorW / this.image.Width,this.Height * factorH / this.image.Height);
			return Math.Min(scrollSize.Width,scrollSize.Height);
		}

		/// <summary>
		/// Returns the minimum scale factor possible for this image with regard to the current control size
		/// </summary>
		/// <returns>minimum scale factor</returns>
		/// <remarks>None</remarks>
		public float MinScale()
		{
			float factorW = (float)this.Width  / this.image.Width;
			float factorH = (float)this.Height / this.image.Height;
			SizeF scrollSize = new SizeF(factorW,factorH);
			return Math.Min(scrollSize.Width,scrollSize.Height);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public float FitScale()
		{
			float factorW = (float)this.Width  / this.image.Width;
			float factorH = (float)this.Height / this.image.Height;
			SizeF scrollSize = new SizeF(factorW,factorH);
			return Math.Min(scrollSize.Width,scrollSize.Height);
		}

		/// <summary>
		/// Fits the image so that that the width of the image is completely displayed without horizontal scroll bar.
		/// </summary>
		/// <remarks>None</remarks>
		public void FitToWidth()
		{
			float factorW = (float)this.Width  / this.image.Width;
			this.Scale = factorW;
		}

		/// <summary>
		/// Fits the image so that is it completely displayed within the control without any scroll bar.
		/// </summary>
		/// <remarks>None</remarks>
		public void FitToAll()
		{
			float factorW = (float)this.Width  / this.image.Width;
			float factorH = (float)this.Height / this.image.Height;
			SizeF scrollSize = new SizeF(factorW,factorH);
			this.Scale = Math.Min(scrollSize.Width,scrollSize.Height);
		}

		/// <summary>
		/// Transforms a point from screen coordinates to picture coordinates
		/// </summary>
		/// <param name="ic">The point in screen coordinates</param>
		/// <returns>The point in picture coordinates</returns>
		/// <remarks>None</remarks>
		public Point TransformPoint(Point ic)
		{
			Point pc = new Point((int)Math.Floor(ic.X * this.scale - this.srcPos.X * this.scale),(int)Math.Floor(ic.Y * this.scale - this.srcPos.Y * this.scale));
			return pc;
		}

		/// <summary>
		/// Transforms a point from screen coordinates to picture coordinates
		/// </summary>
		/// <param name="ic">The point in screen coordinates</param>
		/// <returns>The point in picture coordinates</returns>
		/// <remarks>None</remarks>
		public PointF TransformPoint(PointF ic)
		{
			PointF pc = new PointF(ic.X * this.scale - this.srcPos.X * this.scale,ic.Y * this.scale - this.srcPos.Y * this.scale);
			return pc;
		}

		private void UpdateControl(float oldScale)
		{
			if (this.image != null)
			{
				int	width = (int)Math.Floor(this.scale * this.image.Width);

				bool hScrollBarVisible;
				if (width > this.Size.Width)
				{
					this.hScrollBar1.Visible = true;					
					hScrollBarVisible = true;
				}
				else
				{
					this.hScrollBar1.Visible = false;
					hScrollBarVisible = false;
				}

				int	height= (int)Math.Floor(this.scale * this.image.Height);

				bool vScrollBarVisible;
				if (height > this.Size.Height)
				{
					this.vScrollBar1.Visible = true;					
					vScrollBarVisible = true;
				}
				else
				{
					this.vScrollBar1.Visible = false;					
					vScrollBarVisible = false;
				}

				int windowWidth = this.ClientSize.Width;
				if (vScrollBarVisible == true)
				{
					windowWidth -= this.vScrollBar1.Width;
				}

				int windowHeight = this.ClientSize.Height;
				if (hScrollBarVisible == true)
				{
					windowHeight -= this.hScrollBar1.Height;
				}

				if (windowWidth < 0 || windowHeight < 0)
					return;

				if (vScrollBarVisible == true)
				{
					this.vScrollBar1.Minimum = 0;
					this.vScrollBar1.Maximum = height;
					this.vScrollBar1.LargeChange = windowHeight;
					int newValue = (int)Math.Floor(this.srcPos.Y * this.scale);
					if (newValue < this.vScrollBar1.Minimum)
					{
						this.srcPos.Y = 0;
						this.vScrollBar1.Value	= 0;

					}
					else if (newValue + windowHeight > this.vScrollBar1.Maximum)
					{
						this.vScrollBar1.Value	= this.vScrollBar1.Maximum + 1 - windowHeight;
						this.srcPos.Y = (int)Math.Floor(this.vScrollBar1.Value / this.scale);
					}
					else
					{
						this.vScrollBar1.Value	= newValue;
					}
				}
				else
				{
					this.srcPos.Y = 0;
				}

				if (hScrollBarVisible == true)
				{
					this.hScrollBar1.Minimum = 0;
					this.hScrollBar1.Maximum = width;
					this.hScrollBar1.LargeChange = windowWidth;
					int newValue = (int)Math.Floor(this.srcPos.X * this.scale);
					if (newValue < this.hScrollBar1.Minimum)
					{
						this.srcPos.X = 0;
						this.hScrollBar1.Value = 0;
					}
					else if (newValue + windowWidth > this.hScrollBar1.Maximum)
					{
						this.hScrollBar1.Value = this.hScrollBar1.Maximum + 1 - windowWidth;
						this.srcPos.X = (int)Math.Floor(this.hScrollBar1.Value / this.scale);
					}
					else
					{
						this.hScrollBar1.Value	= newValue;
					}
				}
				else
				{
					this.srcPos.X = 0;
				}

				this.srcSize.Width	= (int)Math.Floor(windowWidth / this.scale);
				this.srcSize.Height	= (int)Math.Floor(windowHeight / this.scale);

				this.dstSize.Width  = windowWidth;
				this.dstSize.Height = windowHeight;

				this.Invalidate();
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="e"></param>
		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint (e);

			if (this.image != null)
			{
				try
				{
					Rectangle src = new Rectangle(this.srcPos.X,this.srcPos.Y,this.srcSize.Width,this.srcSize.Height);
					Rectangle dst = new Rectangle(0,0,dstSize.Width,dstSize.Height);
					e.Graphics.DrawImage(this.image,dst,src,GraphicsUnit.Pixel);
					if (this.OnPaintHandler != null)
						this.OnPaintHandler(this,e);
				}
				catch(Exception ex)
				{
					System.Diagnostics.Debug.WriteLine(ex.Message);
				}
			}
		}

		private void vScrollBar1_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			this.srcPos.Y = (int)Math.Floor(e.NewValue / this.scale);
			this.Invalidate();
		}

		private void hScrollBar1_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			this.srcPos.X = (int)Math.Floor(e.NewValue / this.scale);
			this.Invalidate();
		}

		private void PictureBoxEx3_Resize(object sender, System.EventArgs e)
		{
			this.UpdateControl(this.scale);
		}

		private void PictureBoxEx3_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			currMousePos.X = this.srcPos.X + (int)Math.Floor(e.X / this.scale);
			currMousePos.Y = this.srcPos.Y + (int)Math.Floor(e.Y / this.scale);
		}

		private void vScrollBar1_ValueChanged(object sender, System.EventArgs e)
		{
			this.srcPos.Y = (int)Math.Floor(this.vScrollBar1.Value / this.scale);
		}

		private void hScrollBar1_ValueChanged(object sender, System.EventArgs e)
		{
			this.srcPos.X = (int)Math.Floor(this.hScrollBar1.Value / this.scale);
		}

	}
}
