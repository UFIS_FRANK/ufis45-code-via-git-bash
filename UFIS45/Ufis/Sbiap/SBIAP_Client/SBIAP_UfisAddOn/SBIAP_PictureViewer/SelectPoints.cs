using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace PictureViewer
{
	/// <summary>
	/// The public interface to the point selection dialogue.
	/// </summary>
	public interface	ISelectPoints
	{
		/// <summary>
		/// Returns or sets a point in picture coordinates
		/// </summary>
		Point PicturePoint
		{
			get;
			set;
		}

		/// <summary>
		/// Returns or sets a point in world coordinates
		/// </summary>
		PointF WorldPoint
		{
			get;
			set;
		}
	}

	/// <summary>
	/// The SelectPoints dialogue implements the <see cref="ISelectPoints"/> interface.
	/// </summary>
	public class SelectPoints : System.Windows.Forms.Form,ISelectPoints
	{
		private System.Windows.Forms.Label lblPicturepointX;
		private System.Windows.Forms.GroupBox grbPicturePoint;
		private System.Windows.Forms.Label lblPicturePointY;
		private System.Windows.Forms.GroupBox grbWorldPoint;
		private System.Windows.Forms.Label lblWorldPointY;
		private System.Windows.Forms.Label lblWorldPointX;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Button btnCancel;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private	Point	picturePoint = new Point(0,0);
		private System.Windows.Forms.NumericUpDown udPicturePointX;
		private System.Windows.Forms.NumericUpDown udPicturePointY;
		private System.Windows.Forms.NumericUpDown udWorldPointX;
		private System.Windows.Forms.NumericUpDown udWorldPointY;
		private PointF	worldPoint = new PointF(0,0);

		/// <summary>
		/// The default constructor for an instance of this class.
		/// </summary>
		/// <remarks>None</remarks>
		public SelectPoints()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblPicturepointX = new System.Windows.Forms.Label();
			this.grbPicturePoint = new System.Windows.Forms.GroupBox();
			this.udPicturePointY = new System.Windows.Forms.NumericUpDown();
			this.udPicturePointX = new System.Windows.Forms.NumericUpDown();
			this.lblPicturePointY = new System.Windows.Forms.Label();
			this.grbWorldPoint = new System.Windows.Forms.GroupBox();
			this.lblWorldPointY = new System.Windows.Forms.Label();
			this.lblWorldPointX = new System.Windows.Forms.Label();
			this.udWorldPointY = new System.Windows.Forms.NumericUpDown();
			this.udWorldPointX = new System.Windows.Forms.NumericUpDown();
			this.btnOK = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.grbPicturePoint.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.udPicturePointY)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.udPicturePointX)).BeginInit();
			this.grbWorldPoint.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.udWorldPointY)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.udWorldPointX)).BeginInit();
			this.SuspendLayout();
			// 
			// lblPicturepointX
			// 
			this.lblPicturepointX.Location = new System.Drawing.Point(16, 24);
			this.lblPicturepointX.Name = "lblPicturepointX";
			this.lblPicturepointX.Size = new System.Drawing.Size(40, 23);
			this.lblPicturepointX.TabIndex = 1;
			this.lblPicturepointX.Text = "X :";
			// 
			// grbPicturePoint
			// 
			this.grbPicturePoint.Controls.Add(this.udPicturePointY);
			this.grbPicturePoint.Controls.Add(this.udPicturePointX);
			this.grbPicturePoint.Controls.Add(this.lblPicturePointY);
			this.grbPicturePoint.Controls.Add(this.lblPicturepointX);
			this.grbPicturePoint.Location = new System.Drawing.Point(8, 8);
			this.grbPicturePoint.Name = "grbPicturePoint";
			this.grbPicturePoint.Size = new System.Drawing.Size(224, 96);
			this.grbPicturePoint.TabIndex = 0;
			this.grbPicturePoint.TabStop = false;
			this.grbPicturePoint.Text = "Picture point";
			// 
			// udPicturePointY
			// 
			this.udPicturePointY.Location = new System.Drawing.Point(64, 48);
			this.udPicturePointY.Maximum = new System.Decimal(new int[] {
																			2147483647,
																			0,
																			0,
																			0});
			this.udPicturePointY.Name = "udPicturePointY";
			this.udPicturePointY.Size = new System.Drawing.Size(144, 20);
			this.udPicturePointY.TabIndex = 4;
			// 
			// udPicturePointX
			// 
			this.udPicturePointX.Location = new System.Drawing.Point(64, 24);
			this.udPicturePointX.Maximum = new System.Decimal(new int[] {
																			2147483647,
																			0,
																			0,
																			0});
			this.udPicturePointX.Name = "udPicturePointX";
			this.udPicturePointX.Size = new System.Drawing.Size(144, 20);
			this.udPicturePointX.TabIndex = 2;
			// 
			// lblPicturePointY
			// 
			this.lblPicturePointY.Location = new System.Drawing.Point(16, 56);
			this.lblPicturePointY.Name = "lblPicturePointY";
			this.lblPicturePointY.Size = new System.Drawing.Size(40, 23);
			this.lblPicturePointY.TabIndex = 3;
			this.lblPicturePointY.Text = "Y :";
			// 
			// grbWorldPoint
			// 
			this.grbWorldPoint.Controls.Add(this.lblWorldPointY);
			this.grbWorldPoint.Controls.Add(this.lblWorldPointX);
			this.grbWorldPoint.Controls.Add(this.udWorldPointY);
			this.grbWorldPoint.Controls.Add(this.udWorldPointX);
			this.grbWorldPoint.Location = new System.Drawing.Point(8, 112);
			this.grbWorldPoint.Name = "grbWorldPoint";
			this.grbWorldPoint.Size = new System.Drawing.Size(224, 96);
			this.grbWorldPoint.TabIndex = 5;
			this.grbWorldPoint.TabStop = false;
			this.grbWorldPoint.Text = "World point";
			// 
			// lblWorldPointY
			// 
			this.lblWorldPointY.Location = new System.Drawing.Point(16, 56);
			this.lblWorldPointY.Name = "lblWorldPointY";
			this.lblWorldPointY.Size = new System.Drawing.Size(40, 23);
			this.lblWorldPointY.TabIndex = 8;
			this.lblWorldPointY.Text = "Y :";
			// 
			// lblWorldPointX
			// 
			this.lblWorldPointX.Location = new System.Drawing.Point(16, 24);
			this.lblWorldPointX.Name = "lblWorldPointX";
			this.lblWorldPointX.Size = new System.Drawing.Size(40, 23);
			this.lblWorldPointX.TabIndex = 6;
			this.lblWorldPointX.Text = "X :";
			// 
			// udWorldPointY
			// 
			this.udWorldPointY.DecimalPlaces = 2;
			this.udWorldPointY.Location = new System.Drawing.Point(64, 48);
			this.udWorldPointY.Maximum = new System.Decimal(new int[] {
																		  268435456,
																		  1042612833,
																		  542101086,
																		  0});
			this.udWorldPointY.Minimum = new System.Decimal(new int[] {
																		  268435456,
																		  1042612833,
																		  542101086,
																		  -2147483648});
			this.udWorldPointY.Name = "udWorldPointY";
			this.udWorldPointY.Size = new System.Drawing.Size(144, 20);
			this.udWorldPointY.TabIndex = 9;
			// 
			// udWorldPointX
			// 
			this.udWorldPointX.DecimalPlaces = 2;
			this.udWorldPointX.Location = new System.Drawing.Point(64, 24);
			this.udWorldPointX.Maximum = new System.Decimal(new int[] {
																		  268435456,
																		  1042612833,
																		  542101086,
																		  0});
			this.udWorldPointX.Minimum = new System.Decimal(new int[] {
																		  268435456,
																		  1042612833,
																		  542101086,
																		  -2147483648});
			this.udWorldPointX.Name = "udWorldPointX";
			this.udWorldPointX.Size = new System.Drawing.Size(144, 20);
			this.udWorldPointX.TabIndex = 7;
			// 
			// btnOK
			// 
			this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOK.Location = new System.Drawing.Point(43, 216);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 10;
			this.btnOK.Text = "OK";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(123, 216);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 11;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// SelectPoints
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(240, 254);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOK);
			this.Controls.Add(this.grbPicturePoint);
			this.Controls.Add(this.grbWorldPoint);
			this.Name = "SelectPoints";
			this.Text = "SelectPoints";
			this.Load += new System.EventHandler(this.SelectPoints_Load);
			this.grbPicturePoint.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.udPicturePointY)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.udPicturePointX)).EndInit();
			this.grbWorldPoint.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.udWorldPointY)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.udWorldPointX)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// Returns or sets a point in picture coordinates
		/// </summary>
		/// <value>The point in picture coordinates</value>
		public Point PicturePoint
		{
			get
			{
				return this.picturePoint;
			}
			set
			{
				this.picturePoint = value;
				this.udPicturePointX.Value = (decimal)value.X;
				this.udPicturePointY.Value = (decimal)value.Y;
			}
		}

		/// <summary>
		/// Returns or sets a point in world coordinates
		/// </summary>
		/// <value>The point in world coordinates</value>
		public PointF WorldPoint
		{
			get
			{
				return this.worldPoint;
			}
			set
			{
				this.worldPoint = value;
				this.udWorldPointX.Value = (decimal)value.X;
				this.udWorldPointY.Value = (decimal)value.Y;
			}
		}

		private void btnSelectPoint_Click(object sender, System.EventArgs e)
		{
			this.Hide();
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
		}

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			this.picturePoint.X = (int)this.udPicturePointX.Value;
			this.picturePoint.Y = (int)this.udPicturePointY.Value;
			this.worldPoint.X	= (float)this.udWorldPointX.Value;
			this.worldPoint.Y	= (float)this.udWorldPointY.Value;
		}

		private void SelectPoints_Load(object sender, System.EventArgs e)
		{
		}
	}
}
