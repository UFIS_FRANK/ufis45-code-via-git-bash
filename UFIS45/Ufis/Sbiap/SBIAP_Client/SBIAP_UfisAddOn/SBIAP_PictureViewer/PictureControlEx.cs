using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Data;
using System.Windows.Forms;

namespace PictureViewer
{
	/// <summary>
	/// This <B>enum</B> reprsents the current state of the mouse pointer
	/// </summary>
	public enum	MouseStateEx
	{
		/// <summary>
		/// Arrow as default mouse pointer will be displayed
		/// </summary>
		None,
		/// <summary>
		/// Icon for <B>Select first point</B> will be displayed
		/// </summary>
		Point1,
		/// <summary>
		/// Icon for <B>Select second point</B> will be displayed
		/// </summary>
		Point2,
		/// <summary>
		/// Icon for <B>Select third point</B> will be displayed
		/// </summary>
		Point3,
		/// <summary>
		/// Icon for <B>Zoom in</B> will be displayed
		/// </summary>
		ZoomIn,
		/// <summary>
		/// Icon for <B>Zoom out</B> will be displayed
		/// </summary>
		ZoomOut,
		/// <summary>
		/// Icon for <B>Select first zoom point</B> will be displayed
		/// </summary>
		ZoomFirst,
		/// <summary>
		/// Icon for <B>Select second zoom point</B> will be displayed
		/// </summary>
		ZoomSecond,
		/// <summary>
		/// Icon for <B>Select first camera point</B> will be displayed
		/// </summary>
		CameraFirst,
		/// <summary>
		/// Icon for <B>Select next camera point</B> will be displayed
		/// </summary>
		CameraNext,
		/// <summary>
		/// Icon for <B>Select world point</B> will be displayed
		/// </summary>
		WorldPoint
	}

	/// <summary>
	/// 
	/// </summary>
	public struct Symbol
	{
		/// <summary>
		/// The unique record number of the status event
		/// </summary>
		public	string	urno;
		/// <summary>
		/// The name of the status event
		/// </summary>
		public	string	name;
		/// <summary>
		/// The tooltip 
		/// </summary>
		public	string	toolTip;
		/// <summary>
		/// The insertation point of the symbol to be displayed
		/// </summary>
		public	PointF	origin;
		/// <summary>
		/// The floor number of the symbol to be displayed
		/// </summary>
		public	string	floor;
		/// <summary>
		/// The display size of the symbol
		/// </summary>
		public	Size	size;
		/// <summary>
		/// The image to be displayed of the symbol
		/// </summary>
		public	Image	image;
		/// <summary>
		/// The frame color to be displayed for the symbol
		/// </summary>
		public	Color	frame;
	}

	internal enum CommandState
	{
		None,
		Transformation,
		Zoom,
		ZoomWindow,
		Camera,
		WorldPoint
	}


	/// <summary>
	/// Summary description for PictureControlEx.
	/// </summary>
	public class PictureControlEx : System.Windows.Forms.UserControl
	{
		internal class	Command
		{
			public CommandState	cmdState;
			public MouseStateEx	mouseState;
			public Point		picturePoint1;
			public PointF		worldPoint1;
			public Point		picturePoint2;
			public PointF		worldPoint2;
		}

		private System.Windows.Forms.Button btnMagnifier;
		private System.Windows.Forms.NumericUpDown udMagnifier;
		private System.Windows.Forms.Label lblPictureCoordinates;
		private System.Windows.Forms.Label lblWorldCoordinates;
		private PictureViewer.PictureBoxEx3 picView;
//		private System.Windows.Forms.Panel panView;
		private PictureViewer.PanelEx panView;
		private System.ComponentModel.IContainer components;

		#region	implementation
		private	int				minScrollBarSize	= 10;
		private	decimal			minScaleFactor		= 0.1M;
		private Cursor			zoomIn				= null;
		private Cursor			zoomOut				= null;
		private	Cursor			zoomWindow			= null;
		private	Cursor			pickPoint1			= null;
		private	Cursor			pickPoint2			= null;
		private	Image			magnifierImage		= null;
		private	Image			zoomInImage			= null;
		private	Image			zoomOutImage		= null;
		private	MouseStateEx	currMouseStat		= MouseStateEx.None;
		private	Point			currMouseScr = Point.Empty;	
		private	Point			currMousePos = Point.Empty;
		private	PointF			currMousePosF= PointF.Empty;
		private	SelectPoints	currMouseDlg = null;

		private	Point			picturePoint1;
		private	PointF			worldPoint1;
		private	Point			picturePoint2;
		private	PointF			worldPoint2;
		private	Matrix			transform	= null;
		private	SortedList		symbols		= new SortedList();
		private	RectangleF		dimension	= RectangleF.Empty;
		private	string			floor		= "-99";
		private	SortedList		selectedSymbols = new SortedList();
		private	bool			drawSelected= false;
			
		/// <summary>
		/// This delegate will be called by the picuture control when the <B>Calculate World point</B> command has been completed successfully
		/// </summary>
		public delegate	void	EndTransformationHandler(PointF origin,SizeF size);
		public delegate void	CancelTransformationHandler();
		public delegate	string	ContextMenuItemHandler(int index,PointF origin,ArrayList symbols);
		public delegate	void	DoubleClickHandler(PointF origin,ArrayList symbolList);
		public delegate	void	EndCameraPositionHandler(RectangleF rect);
		public delegate void	CancelCameraPositionHandler();
		public delegate void	EndWorldPointHandler(PointF origin);
		public delegate	void	CancelWorldPointHandler();
		public delegate System.Windows.Forms.Form	SelectPointsHandler();
				
		/// <summary>
		/// This event will be called by the picuture control when the <B>Calculate World point</B> command has been completed successfully
		/// </summary>
		public	event EndTransformationHandler		OnEndTransformationHandler;
		/// <summary>
		/// This event will be called by the picuture control when the <B>Calculate World point</B> command has been aborted
		/// </summary>
		public	event CancelTransformationHandler	OnCancelTransformationHandler;	
		/// <summary>
		/// This event will be called by the picuture control when the a context menu item has been selected by the user
		/// </summary>
		public	ContextMenuItemHandler				OnContextMenuItem;
		/// <summary>
		/// This event will be called by the picuture control when the <B>Calculate Camera Position</B> command has been completed successfully
		/// </summary>
		public	event EndCameraPositionHandler		OnEndCameraPositionHandler;
		/// <summary>
		/// This event will be called by the picuture control when the <B>Calculate Camera Position</B> command has been aborted
		/// </summary>
		public	event CancelCameraPositionHandler	OnCancelCameraPositionHandler;	
		/// <summary>
		/// This event will be called by the picuture control when the <B>Calculate World Point</B> command has been completed successfully
		/// </summary>
		public	event EndWorldPointHandler			OnEndWorldPointHandler;
		/// <summary>
		/// This event will be called by the picuture control when the <B>Calculate World Point</B> command has been aborted
		/// </summary>
		public	event CancelWorldPointHandler		OnCancelWorldPointHandler;	
		/// <summary>
		/// This event will be called by the picuture control internally, when a point selection dialogue is needed
		/// </summary>
		public	SelectPointsHandler					OnSelectPoints;
		/// <summary>
		/// This event will be called by the picuture control when the double clicks on a symbol of the symbol list
		/// </summary>
		public	DoubleClickHandler					OnDoubleClickHandler;

		private System.Windows.Forms.CheckBox cbZoomWindow;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.Button btnFitWidth;
		private System.Windows.Forms.Button btnFitAll;
		private	Stack	cmdStack;
		private	bool	txnActive = false;
		#endregion

		/// <summary>
		/// The default constructor for an instance of this class
		/// </summary>
		public PictureControlEx()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
			this.zoomIn			= new Cursor(GetType(),"ZoomIn.cur");
			this.zoomOut		= new Cursor(GetType(),"ZoomOut.cur");
			this.zoomWindow		= new Cursor(GetType(),"ZoomWindow.cur");
			this.pickPoint1		= new Cursor(GetType(),"PickPoint1.cur");
			this.pickPoint2		= new Cursor(GetType(),"PickPoint2.cur");

			this.zoomInImage	= new Bitmap(GetType(),"ZoomIn.bmp");
			this.zoomOutImage	= new Bitmap(GetType(),"ZoomOut.bmp");
			this.magnifierImage	= new Bitmap(GetType(),"Magnifier.bmp");

			this.udMagnifier.Value = (decimal)this.picView.Scale * 100;
			this.picView.MouseMove +=new MouseEventHandler(picView_MouseMove);
			this.picView.MouseDown +=new MouseEventHandler(picView_MouseDown);
//			this.picView.Paint	   +=new PaintEventHandler(picView_Paint);
			this.panView.Resize    +=new EventHandler(panView_Resize);
			this.picView.OnPaintHandler +=new PictureViewer.PictureBoxEx3.On_Paint(picView_Paint);
			this.picView.DoubleClick +=new EventHandler(picView_DoubleClick);
			this.timer1.Interval = 1000;
			this.timer1.Enabled	 = true;
			this.cmdStack = new Stack();

//			disable all button commands
			this.btnFitAll.Enabled		= false;
			this.btnFitWidth.Enabled	= false;
			this.cbZoomWindow.Enabled	= false;
			this.btnMagnifier.Enabled	= false;
			this.udMagnifier.Enabled	= false;

			this.lblWorldCoordinates.Visible = false;
			this.lblPictureCoordinates.Visible = false;


		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				this.zoomIn.Dispose();
				this.zoomInImage.Dispose();
				this.zoomOut.Dispose();
				this.zoomOutImage.Dispose();
				this.zoomWindow.Dispose();
				this.magnifierImage.Dispose();

				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(PictureControlEx));
			this.panView = new PictureViewer.PanelEx();
			this.picView = new PictureViewer.PictureBoxEx3();
			this.btnMagnifier = new System.Windows.Forms.Button();
			this.udMagnifier = new System.Windows.Forms.NumericUpDown();
			this.lblPictureCoordinates = new System.Windows.Forms.Label();
			this.lblWorldCoordinates = new System.Windows.Forms.Label();
			this.cbZoomWindow = new System.Windows.Forms.CheckBox();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.btnFitWidth = new System.Windows.Forms.Button();
			this.btnFitAll = new System.Windows.Forms.Button();
			this.panView.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.udMagnifier)).BeginInit();
			this.SuspendLayout();
			// 
			// panView
			// 
			this.panView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.panView.Controls.Add(this.picView);
			this.panView.Location = new System.Drawing.Point(0, 32);
			this.panView.Name = "panView";
			this.panView.Size = new System.Drawing.Size(384, 300);
			this.panView.TabIndex = 0;
			// 
			// picView
			// 
			this.picView.Image = null;
			this.picView.Location = new System.Drawing.Point(0, 0);
			this.picView.Name = "picView";
			this.picView.Scale = 1F;
			this.picView.Size = new System.Drawing.Size(384, 248);
			this.picView.TabIndex = 0;
			this.picView.TabStop = false;
			// 
			// btnMagnifier
			// 
			this.btnMagnifier.BackColor = System.Drawing.Color.Transparent;
			this.btnMagnifier.Image = ((System.Drawing.Image)(resources.GetObject("btnMagnifier.Image")));
			this.btnMagnifier.Location = new System.Drawing.Point(112, 8);
			this.btnMagnifier.Name = "btnMagnifier";
			this.btnMagnifier.Size = new System.Drawing.Size(32, 23);
			this.btnMagnifier.TabIndex = 1;
			this.toolTip1.SetToolTip(this.btnMagnifier, "Zoom in / out");
			this.btnMagnifier.Click += new System.EventHandler(this.btnMagnifier_Click);
			// 
			// udMagnifier
			// 
			this.udMagnifier.Location = new System.Drawing.Point(152, 8);
			this.udMagnifier.Minimum = new System.Decimal(new int[] {
																		1,
																		0,
																		0,
																		0});
			this.udMagnifier.Name = "udMagnifier";
			this.udMagnifier.Size = new System.Drawing.Size(56, 20);
			this.udMagnifier.TabIndex = 2;
			this.udMagnifier.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.toolTip1.SetToolTip(this.udMagnifier, "Magnifier in percent of original size");
			this.udMagnifier.Value = new System.Decimal(new int[] {
																	  1,
																	  0,
																	  0,
																	  0});
			this.udMagnifier.Click += new System.EventHandler(this.udMagnifier_Click);
			this.udMagnifier.ValueChanged += new System.EventHandler(this.udMagnifier_ValueChanged);
			// 
			// lblPictureCoordinates
			// 
			this.lblPictureCoordinates.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.lblPictureCoordinates.BackColor = System.Drawing.Color.Transparent;
			this.lblPictureCoordinates.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblPictureCoordinates.Location = new System.Drawing.Point(8, 336);
			this.lblPictureCoordinates.Name = "lblPictureCoordinates";
			this.lblPictureCoordinates.Size = new System.Drawing.Size(376, 16);
			this.lblPictureCoordinates.TabIndex = 3;
			this.lblPictureCoordinates.Text = "Picture Coordinate:";
			this.toolTip1.SetToolTip(this.lblPictureCoordinates, "Coordinates in picture units");
			// 
			// lblWorldCoordinates
			// 
			this.lblWorldCoordinates.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.lblWorldCoordinates.BackColor = System.Drawing.Color.Transparent;
			this.lblWorldCoordinates.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblWorldCoordinates.Location = new System.Drawing.Point(8, 352);
			this.lblWorldCoordinates.Name = "lblWorldCoordinates";
			this.lblWorldCoordinates.Size = new System.Drawing.Size(376, 16);
			this.lblWorldCoordinates.TabIndex = 4;
			this.lblWorldCoordinates.Text = "World Coordinate:";
			this.toolTip1.SetToolTip(this.lblWorldCoordinates, "Coordinates in world units");
			// 
			// cbZoomWindow
			// 
			this.cbZoomWindow.Appearance = System.Windows.Forms.Appearance.Button;
			this.cbZoomWindow.Image = ((System.Drawing.Image)(resources.GetObject("cbZoomWindow.Image")));
			this.cbZoomWindow.Location = new System.Drawing.Point(80, 8);
			this.cbZoomWindow.Name = "cbZoomWindow";
			this.cbZoomWindow.Size = new System.Drawing.Size(32, 23);
			this.cbZoomWindow.TabIndex = 5;
			this.toolTip1.SetToolTip(this.cbZoomWindow, "Zoom window ");
			this.cbZoomWindow.CheckedChanged += new System.EventHandler(this.cbZoomWindow_CheckedChanged);
			// 
			// timer1
			// 
			this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
			// 
			// toolTip1
			// 
			this.toolTip1.ShowAlways = true;
			// 
			// btnFitWidth
			// 
			this.btnFitWidth.BackColor = System.Drawing.Color.Transparent;
			this.btnFitWidth.Image = ((System.Drawing.Image)(resources.GetObject("btnFitWidth.Image")));
			this.btnFitWidth.Location = new System.Drawing.Point(48, 8);
			this.btnFitWidth.Name = "btnFitWidth";
			this.btnFitWidth.Size = new System.Drawing.Size(32, 23);
			this.btnFitWidth.TabIndex = 6;
			this.toolTip1.SetToolTip(this.btnFitWidth, "Fit width");
			this.btnFitWidth.Click += new System.EventHandler(this.btnFitWidth_Click);
			// 
			// btnFitAll
			// 
			this.btnFitAll.BackColor = System.Drawing.Color.Transparent;
			this.btnFitAll.Image = ((System.Drawing.Image)(resources.GetObject("btnFitAll.Image")));
			this.btnFitAll.Location = new System.Drawing.Point(16, 8);
			this.btnFitAll.Name = "btnFitAll";
			this.btnFitAll.Size = new System.Drawing.Size(32, 23);
			this.btnFitAll.TabIndex = 7;
			this.toolTip1.SetToolTip(this.btnFitAll, "Fit all");
			this.btnFitAll.Click += new System.EventHandler(this.button1_Click);
			// 
			// PictureControlEx
			// 
			this.BackColor = System.Drawing.SystemColors.Control;
			this.Controls.Add(this.btnFitAll);
			this.Controls.Add(this.btnFitWidth);
			this.Controls.Add(this.cbZoomWindow);
			this.Controls.Add(this.lblWorldCoordinates);
			this.Controls.Add(this.lblPictureCoordinates);
			this.Controls.Add(this.udMagnifier);
			this.Controls.Add(this.btnMagnifier);
			this.Controls.Add(this.panView);
			this.Name = "PictureControlEx";
			this.Size = new System.Drawing.Size(392, 368);
			this.toolTip1.SetToolTip(this, "Coordinates in picture units");
			this.Load += new System.EventHandler(this.PictureControlEx_Load);
			this.panView.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.udMagnifier)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void btnMagnifier_Click(object sender, System.EventArgs e)
		{
			if (this.picView.Cursor == zoomIn)
			{
				this.ActivateNextMouseCommand(CommandState.Zoom);
			}
			else if (this.picView.Cursor == zoomOut)
			{
				DeactivateCommand(CommandState.Zoom,true);
			}
			else
			{
				CreateCommand(CommandState.Zoom);
			}
		}

		private void ContextMenu_Click(object sender, System.EventArgs e)
		{
			// do we have an handler for this event ?
			if (this.OnContextMenuItem != null)
			{
				ArrayList selectedSymbols = this.GetSymbolsFromMousePoint();
				if (selectedSymbols.Count > 0)
				{
					MenuItem item = (MenuItem)sender;
					string contextMenuInfo = this.OnContextMenuItem(item.Index,this.currMousePosF,selectedSymbols);
					if (contextMenuInfo.Length > 0)
					{
						MessageBox.Show(this,contextMenuInfo,"Information");
					}
				}
				else
				{
					MessageBox.Show(this,"Sorry, no status event found at this location","Information");
				}
			}
			
		}

		private void ContextMenu_CancelCommand(object sender, System.EventArgs e)
		{
			if (this.cmdStack.Count > 0)
			{
				Command cmd = (Command)this.cmdStack.Peek();
				this.DeactivateCommand(cmd.cmdState,false);			
			}
		}

		private void udMagnifier_Click(object sender, System.EventArgs e)
		{
		
		}

		private void udMagnifier_ValueChanged(object sender, System.EventArgs e)
		{
			// get current view
			if (this.panView.AutoScroll)
			{
				Point lastPos = this.panView.AutoScrollPosition;
				float lastSca = this.picView.Scale * -1.0F;

				this.picView.Scale = (float)this.udMagnifier.Value / 100;	
				Point currPos = new Point((int)Math.Floor(lastPos.X * this.picView.Scale / lastSca),(int)Math.Floor(lastPos.Y * this.picView.Scale / lastSca));
				this.panView.AutoScrollPosition = currPos;
			}
			else
			{
				this.picView.Scale = (float)this.udMagnifier.Value / 100;	
			}
		}

		/// <summary>
		/// Returns or sets the current state of the mouse pointer. <see cref="MouseStateEx"/> for all valid states.
		/// </summary>
		public MouseStateEx	MouseStatus
		{
			get
			{
				return this.currMouseStat;
			}
			set
			{
				this.currMouseStat = value;
			}
		}

		/// <summary>
		/// Returns or sets the current image to be displayed within the picture control.
		/// </summary>
		public Image Image
		{
			get
			{
				return this.picView.Image;
			}
			set
			{
				this.picView.Scale = 1;
				this.picView.Image = value;
				if (value != null)
				{
					this.lblPictureCoordinates.Visible = true;
					this.btnFitAll.Enabled		= true;
					this.btnFitWidth.Enabled	= true;
					this.cbZoomWindow.Enabled	= true;
					this.btnMagnifier.Enabled	= true;
					this.udMagnifier.Enabled	= true;

					this.udMagnifier.Maximum = (decimal)this.MaxScale() * 100;
					this.udMagnifier.Minimum = (decimal)this.MinScale() * 100;
					this.udMagnifier.Value	 = (decimal)this.FitScale() * 100;
					this.picView.Scale		 = this.FitScale();
				}
				else
				{
					this.lblPictureCoordinates.Visible = false;
					this.btnFitAll.Enabled		= false;
					this.btnFitWidth.Enabled	= false;
					this.cbZoomWindow.Enabled	= false;
					this.btnMagnifier.Enabled	= false;
					this.udMagnifier.Enabled	= false;

					this.udMagnifier.Minimum = 1;
					this.udMagnifier.Maximum = 1;
					this.udMagnifier.Value	 = 1;
					this.picView.Scale		 = 1;

				}

				this.picView.Refresh();
			}
		}

		/// <summary>
		/// Returns or set the current scale factor for the image currently displayed within the picture control.
		/// </summary>
		public decimal ScaleFactor
		{
			get
			{
				return this.udMagnifier.Value;
			}

			set
			{
				if (value >= this.udMagnifier.Minimum && value <= this.udMagnifier.Maximum)
				{
					this.udMagnifier.Value = value;
				}
			}
		}

		/// <summary>
		/// Returns or sets the current blink interval
		/// </summary>
		/// <value>The blink interval in milliseconds</value>
		public int BlinkInterval
		{
			get
			{
				return this.timer1.Interval;
			}
			set
			{
				this.timer1.Interval = value;
			}
		}

		private void picView_MouseMove(object sender, MouseEventArgs e)
		{
			this.lblPictureCoordinates.Text = "Picture Coordinate: " + this.picView.CurrentMousePosition.X.ToString() + " | " + this.picView.CurrentMousePosition.Y.ToString();

			this.currMouseScr.X = e.X;
			this.currMouseScr.Y = e.Y;

			this.currMousePos = this.picView.CurrentMousePosition;

			if (this.transform != null)
			{
				PointF[] pts = new PointF[1];
				pts[0].X = this.currMousePos.X;
				pts[0].Y = this.currMousePos.Y;

				this.transform.TransformPoints(pts);
				this.currMousePosF = pts[0];
				this.lblWorldCoordinates.Text = "World Coordinate: " + pts[0].X.ToString() + " | " + pts[0].Y.ToString();
			}

			if (this.currMouseStat == MouseStateEx.ZoomSecond)
			{
				Graphics g = Graphics.FromHwnd(this.picView.Handle);
				Pen pen = new Pen(Color.Black);
				Point p1 = this.picView.TransformPoint(this.picturePoint1);
				Point p2 = this.picView.TransformPoint(this.currMousePos);
				int	width  = p2.X - p1.X;
				int	height = p2.Y - p1.Y;

				this.picView.Refresh();
				if (width < 0)
				{
					if (height < 0)
					{
						g.DrawRectangle(pen,p2.X,p2.Y,(int)Math.Abs(width),(int)Math.Abs(height));
					}
					else
					{
						g.DrawRectangle(pen,p2.X,p1.Y,(int)Math.Abs(width),(int)Math.Abs(height));
					}
				}
				else if (height < 0)
				{
					g.DrawRectangle(pen,p1.X,p2.Y,(int)Math.Abs(width),(int)Math.Abs(height));
				}
				else
				{
					g.DrawRectangle(pen,p1.X,p1.Y,width,height);
				}
				g.Dispose();
			}
			else if (this.currMouseStat == MouseStateEx.CameraNext)
			{
				Graphics g = Graphics.FromHwnd(this.picView.Handle);
				Pen pen = new Pen(Color.LightSkyBlue);
				Point p1 = this.picView.TransformPoint(this.picturePoint1);
				Point p2 = this.picView.TransformPoint(this.currMousePos);
				int	width  = p2.X - p1.X;
				int	height = p2.Y - p1.Y;

				this.picView.Refresh();
				if (width < 0)
				{
					if (height < 0)
					{
						g.DrawRectangle(pen,p2.X,p2.Y,(int)Math.Abs(width),(int)Math.Abs(height));
					}
					else
					{
						g.DrawRectangle(pen,p2.X,p1.Y,(int)Math.Abs(width),(int)Math.Abs(height));
					}
				}
				else if (height < 0)
				{
					g.DrawRectangle(pen,p1.X,p2.Y,(int)Math.Abs(width),(int)Math.Abs(height));
				}
				else
				{
					g.DrawRectangle(pen,p1.X,p1.Y,width,height);
				}
				g.Dispose();
			}

			ArrayList symbols = this.GetSymbolsFromMousePoint();
			if (symbols != null)
			{
				string tip = string.Empty;
				for (int i = 0; i < symbols.Count; i++)
				{
					Symbol symbol = (Symbol)symbols[i];
					if (tip == string.Empty)
					{
						tip += (i+1).ToString() + ":" + symbol.toolTip;	
					}
					else
					{
						tip +=  "\n" + (i+1).ToString() + ":" + symbol.toolTip;	
					}
				}

				this.toolTip1.SetToolTip(this.picView,tip);
			}

		}

		private void picView_MouseDown(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				if (this.currMouseStat == MouseStateEx.Point1)
				{
					if (this.OnSelectPoints == null)
					{
						this.currMouseDlg = new SelectPoints();
						this.currMouseDlg.Text = "Upper left point...";
						this.currMouseDlg.PicturePoint = this.currMousePos;
						if (this.currMouseDlg.ShowDialog(this) == DialogResult.OK)
						{
							this.AddParameterToCommand(this.currMouseDlg.PicturePoint);
							this.AddParameterToCommand(this.currMouseDlg.WorldPoint);
							this.ActivateNextMouseCommand(CommandState.Transformation);
						}
						else
						{
							this.DeactivateCommand(CommandState.Transformation,false);
						}
						this.currMouseDlg.Dispose();
						this.currMouseDlg = null;
					}
					else
					{
						System.Windows.Forms.Form dlg = this.OnSelectPoints();
						ISelectPoints iSelectPoints = (ISelectPoints)dlg;
						dlg.Text = "Upper left point...";
						iSelectPoints.PicturePoint = this.currMousePos;
						if (dlg.ShowDialog(this) == DialogResult.OK)
						{
							this.AddParameterToCommand(iSelectPoints.PicturePoint);
							this.AddParameterToCommand(iSelectPoints.WorldPoint);
							this.ActivateNextMouseCommand(CommandState.Transformation);
						}
						else
						{
							this.DeactivateCommand(CommandState.Transformation,false);
						}
					}
				}
				else if (this.currMouseStat == MouseStateEx.Point2)
				{
					if (this.OnSelectPoints == null)
					{
						this.currMouseDlg = new SelectPoints();
						this.currMouseDlg.Text = "Lower right point...";
						this.currMouseDlg.PicturePoint = this.currMousePos;
						if (this.currMouseDlg.ShowDialog(this) == DialogResult.OK)
						{
							this.AddParameterToCommand(this.currMouseDlg.PicturePoint);
							this.AddParameterToCommand(this.currMouseDlg.WorldPoint);
							this.DeactivateCommand(CommandState.Transformation,true);
						}
						else
						{
							this.DeactivateCommand(CommandState.Transformation,false);
						}

						this.currMouseDlg.Dispose();
						this.currMouseDlg = null;
					}
					else
					{
						System.Windows.Forms.Form dlg = this.OnSelectPoints();
						ISelectPoints iSelectPoints = (ISelectPoints)dlg;
						dlg.Text = "Lower right point...";
						iSelectPoints.PicturePoint = this.currMousePos;
						if (dlg.ShowDialog(this) == DialogResult.OK)
						{
							this.AddParameterToCommand(iSelectPoints.PicturePoint);
							this.AddParameterToCommand(iSelectPoints.WorldPoint);
							this.DeactivateCommand(CommandState.Transformation,true);
						}
						else
						{
							this.DeactivateCommand(CommandState.Transformation,false);
						}
					}
				}
				else if (this.currMouseStat == MouseStateEx.ZoomIn)
				{
					if ((this.udMagnifier.Value / 100) + minScaleFactor	> (this.udMagnifier.Maximum / 100))
					{
						this.ActivateNextMouseCommand(CommandState.Zoom);
					}
					else
					{
						this.udMagnifier.Value = (this.udMagnifier.Value / 100 + minScaleFactor) * 100;
						this.udMagnifier.Refresh();
						this.picView.ScrollToPosition(this.currMousePos);
					}
				}
				else if (this.currMouseStat == MouseStateEx.ZoomOut)
				{
					if ((this.udMagnifier.Value / 100) - minScaleFactor	< (this.udMagnifier.Minimum / 100))
					{
						this.ActivateNextMouseCommand(CommandState.Zoom);
					}
					else
					{
						this.udMagnifier.Value = (this.udMagnifier.Value / 100 - minScaleFactor) * 100;
						this.udMagnifier.Refresh();
						this.picView.ScrollToPosition(this.currMousePos);
					}
				}
				else if (this.currMouseStat == MouseStateEx.ZoomFirst)
				{
					if (AddParameterToCommand(this.currMousePos))
					{
						this.ActivateNextMouseCommand(CommandState.ZoomWindow);					
					}
				}
				else if (this.currMouseStat == MouseStateEx.ZoomSecond)
				{
					if (AddParameterToCommand(this.currMousePos))
					{
						this.ActivateNextMouseCommand(CommandState.ZoomWindow);					
					}
				}
				else if (this.currMouseStat == MouseStateEx.CameraFirst)
				{
					if (AddParameterToCommand(this.currMousePos) && AddParameterToCommand(this.currMousePosF))
					{
						this.ActivateNextMouseCommand(CommandState.Camera);					
					}
				}
				else if (this.currMouseStat == MouseStateEx.CameraNext)
				{
					if (AddParameterToCommand(this.currMousePos) && AddParameterToCommand(this.currMousePosF))
					{
						this.ActivateNextMouseCommand(CommandState.Camera);					
					}
				}
				else if (this.currMouseStat == MouseStateEx.WorldPoint)
				{
					if (AddParameterToCommand(this.currMousePosF))
					{
						this.ActivateNextMouseCommand(CommandState.WorldPoint);					
					}
				}

			}
			else if (e.Button == MouseButtons.Right)
			{
				ContextMenu myMenu = new ContextMenu(); 


				if (this.OnContextMenuItem != null)
				{
					ArrayList selectedSymbols = this.GetSymbolsFromMousePoint();
					if (selectedSymbols.Count > 0)
					{
						MenuItem item = new MenuItem("Next camera",new EventHandler(ContextMenu_Click));
						myMenu.MenuItems.Add(item);

						item = new MenuItem("Handling advice for group",new EventHandler(ContextMenu_Click));
						myMenu.MenuItems.Add(item);

						item = new MenuItem("Handling advice for status",new EventHandler(ContextMenu_Click));
						myMenu.MenuItems.Add(item);

						bool selected = false;
						foreach (Symbol symbol in selectedSymbols)
						{
							if (this.selectedSymbols.ContainsKey(symbol.name))
							{
								selected = true;
								break;
							}
						}
						if (selected)
						{
							item = new MenuItem("Unselect symbol",new EventHandler(ContextMenu_Click));
							myMenu.MenuItems.Add(item);
						}
					}
				}

				if (this.cmdStack.Count > 0)
				{
					MenuItem item = new MenuItem("Cancel current command",new EventHandler(ContextMenu_CancelCommand));
					myMenu.MenuItems.Add(item);
				}

				if (myMenu.MenuItems.Count > 0)
				{
					this.picView.ContextMenu = myMenu;
				}
				else
				{
					this.picView.ContextMenu = null;
				}
			}
		}

		private float MaxScale()
		{
			float factorW = (float)this.panView.Width  / this.minScrollBarSize;
			float factorH = (float)this.panView.Height / this.minScrollBarSize;

			SizeF scrollSize = new SizeF(this.panView.Width * factorW / this.Image.Width,this.panView.Height * factorH / this.Image.Height);
			return Math.Min(scrollSize.Width,scrollSize.Height);
		}

		private float MinScale()
		{
			float factorW = (float)this.panView.Width  / this.Image.Width;
			float factorH = (float)this.panView.Height / this.Image.Height;
			SizeF scrollSize = new SizeF(factorW,factorH);
			return Math.Min(scrollSize.Width,scrollSize.Height);
		}

		private float FitScale()
		{
			float factorW = (float)this.panView.Width  / this.Image.Width;
			float factorH = (float)this.panView.Height / this.Image.Height;
			SizeF scrollSize = new SizeF(factorW,factorH);
			return Math.Max(scrollSize.Width,scrollSize.Height);
		}

		private void panView_Resize(object sender, EventArgs e)
		{
			this.picView.Size = this.panView.Size;

			if (this.Image != null)
			{
				this.udMagnifier.Maximum = (decimal)this.MaxScale() * 100;
				this.udMagnifier.Minimum = (decimal)this.MinScale() * 100;
			}
			else
			{
				this.udMagnifier.Minimum = 1;
				this.udMagnifier.Maximum = 1;
			}
		}

		private void currMouseDlg_Click(object sender, System.EventArgs e)
		{
			if (this.currMouseStat == MouseStateEx.Point1)
			{
				this.picturePoint1 = this.currMouseDlg.PicturePoint;
				this.worldPoint1   = this.currMouseDlg.WorldPoint;
				this.currMouseStat = MouseStateEx.Point2;
				this.currMouseDlg.Text = "Lower right point...";
				this.currMouseDlg.PicturePoint = new Point(0,0);
				this.currMouseDlg.WorldPoint   = new PointF(0,0);
				this.currMouseDlg.Show();
			}
			else if (this.currMouseStat == MouseStateEx.Point2)
			{
				this.picturePoint2 = this.currMouseDlg.PicturePoint;
				this.worldPoint2   = this.currMouseDlg.WorldPoint;	
				this.currMouseStat = MouseStateEx.None;
				this.EndCalculateTransformation();
			}
		}

		/// <summary>
		/// Activates the <B>CalculateTransformation</B> command
		/// </summary>
		/// <returns>true if the new command is active, false if not</returns>
		public bool BeginCalculateTransformation()
		{
			return this.CreateCommand(CommandState.Transformation);
		}

		/// <summary>
		/// Activates the <B>CalculateCameraPosition</B> command
		/// </summary>
		/// <returns>true if the new command is active, false if not</returns>
		public bool BeginCalculateCameraPosition()
		{
			return this.CreateCommand(CommandState.Camera);
		}

		private void EndCalculateTransformation()
		{
			RectangleF rect = new RectangleF(this.picturePoint1.X,this.picturePoint1.Y,this.picturePoint2.X - this.picturePoint1.X,this.picturePoint2.Y - this.picturePoint1.Y);
			PointF[] pts = new PointF[3];
			pts[0] = this.worldPoint1;
			pts[1].X = this.worldPoint2.X;
			pts[1].Y = this.worldPoint1.Y;
			pts[2].X = this.worldPoint1.X;
			pts[2].Y = this.worldPoint2.Y;
			this.transform = new System.Drawing.Drawing2D.Matrix(rect,pts);			

			pts[0].X = 0;
			pts[0].Y = 0;
			pts[1].X = this.picView.Image.Width;
			pts[1].Y = this.picView.Image.Height;


			this.transform.TransformPoints(pts);
			SizeF  size	  = new SizeF(pts[1].X - pts[0].X,pts[1].Y - pts[0].Y);	
			this.dimension= new RectangleF(pts[0],size);

			this.lblWorldCoordinates.Visible = true;

			if (this.OnEndTransformationHandler != null)
				this.OnEndTransformationHandler(pts[0],size);
		}

		/// <summary>
		/// Sets the current transformation matrix for the conversion between world coordinates and picture coordinates
		/// </summary>
		/// <returns>true if the transformation is valid, false if not</returns>

		public bool SetTransformation(PointF offset,SizeF size)
		{
			if (this.picView.Image == null)
			{
				return false;
			}
			else
			{
				this.dimension   = new RectangleF(offset,size);
				this.worldPoint1 = offset;
				this.worldPoint2 = offset;
				this.worldPoint2.X += size.Width;
				this.worldPoint2.Y += size.Height;
				RectangleF rect = new RectangleF(0,0,this.picView.Image.Width,this.picView.Image.Height);
				PointF[] pts = new PointF[3];
				pts[0] = this.worldPoint1;
				pts[1].X = this.worldPoint2.X;
				pts[1].Y = this.worldPoint1.Y;
				pts[2].X = this.worldPoint1.X;
				pts[2].Y = this.worldPoint2.Y;
				this.transform = new System.Drawing.Drawing2D.Matrix(rect,pts);			
				this.lblWorldCoordinates.Visible = true;
				return true;
			}
		}

		/// <summary>
		/// Adds a new symbol to the list of symbols
		/// </summary>
		/// <param name="symbol"></param>
		/// <param name="redraw">Indicates whether to refresh the picture control immediately or not</param>
		public void AddSymbol(Symbol symbol,bool redraw)
		{
			if (!this.symbols.ContainsKey(symbol.name))
			{
				this.symbols.Add(symbol.name,symbol);							
				if (redraw)
				{
					this.Refresh();
				}
			}
		}

		/// <summary>
		/// Removes the symbol from the list of symbols
		/// </summary>
		/// <param name="name">The name of the symbol to be removed</param>
		/// <param name="redraw">Indicates whether to refresh the picture control immediately or not</param>
		/// <returns>true if the symbol has been successfully removed, false if not</returns>
		public bool RemoveSymbol(string name,bool redraw)
		{
			int ind = this.symbols.IndexOfKey(name);
			if (ind >= 0)
			{
				this.symbols.RemoveAt(ind);
				ind = this.selectedSymbols.IndexOfKey(name);
				if (ind >= 0)
				{
					this.selectedSymbols.RemoveAt(ind);
				}

				if (redraw)
				{
					this.Refresh();
				}
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// Updates the specified symbol of the list of symbols
		/// </summary>
		/// <param name="symbol"></param>
		/// <param name="redraw">Indicates whether to refresh the picture control immediately or not</param>
		public void UpdateSymbol(Symbol symbol,bool redraw)
		{
			RemoveSymbol(symbol.name,false);
			AddSymbol(symbol,redraw);
		}

		/// <summary>
		/// Clears the list of symbols
		/// </summary>
		public void ClearSymbols()
		{
			this.symbols.Clear();
			this.selectedSymbols.Clear();
		}

		/// <summary>
		/// Fits 
		/// </summary>
		public void FitToSize()
		{
			this.udMagnifier.Value	 = (decimal)this.FitScale() * 100;
			this.udMagnifier.Refresh();
		}

		/// <summary>
		/// Returns the outlines in world coordinates of the image currently displayed
		/// </summary>
		/// <value>The image outlines in world coordinates</value>
		public	RectangleF	Dimension
		{
			get
			{
				return this.dimension;
			}
		}

		/// <summary>
		/// Returns the floor of the image currently displayed.
		/// </summary>
		public	string	Floor
		{
			get
			{
				return this.floor;			
			}
			set
			{
				this.floor = value;
			}
		}

		private void picView_Paint(object sender, PaintEventArgs e)
		{
			if (this.transform != null && this.Image != null)
			{
				try
				{
					System.Drawing.Drawing2D.Matrix invMat = this.transform.Clone(); 					
					invMat.Invert();
					PointF[] pts = new PointF[1];
					for (int i = 0; i < this.symbols.Count; i++)
					{
						Symbol symbol = (Symbol)this.symbols.GetByIndex(i);
						pts[0]	= symbol.origin;
						invMat.TransformPoints(pts);
						pts[0] =  picView.TransformPoint(pts[0]);
						if (this.selectedSymbols.ContainsKey(symbol.name))
						{
							if (this.drawSelected == true)
							{
								Pen pen = new Pen(symbol.frame,2);
								e.Graphics.DrawImage(symbol.image,pts[0].X,pts[0].Y,symbol.size.Width,symbol.size.Height);
								e.Graphics.DrawRectangle(pen,pts[0].X-1,pts[0].Y-1,symbol.size.Width+2,symbol.size.Height+2);
							}
						}
						else
						{
							Pen pen = new Pen(symbol.frame,2);
							e.Graphics.DrawImage(symbol.image,pts[0].X,pts[0].Y,symbol.size.Width,symbol.size.Height);
							e.Graphics.DrawRectangle(pen,pts[0].X-1,pts[0].Y-1,symbol.size.Width+2,symbol.size.Height+2);
						}
					}

					if (this.drawSelected)
					{
						for (int i = 0; i < this.selectedSymbols.Count; i++)
						{
							Symbol symbol = (Symbol)this.selectedSymbols.GetByIndex(i);
							pts[0]	= symbol.origin;
							invMat.TransformPoints(pts);
							pts[0] =  picView.TransformPoint(pts[0]);
							Pen pen = new Pen(symbol.frame,2);
							e.Graphics.DrawImage(symbol.image,pts[0].X,pts[0].Y,symbol.size.Width,symbol.size.Height);
							e.Graphics.DrawRectangle(pen,pts[0].X-1,pts[0].Y-1,symbol.size.Width+2,symbol.size.Height+2);
						}
					}
					
				}
				catch(Exception ex)
				{
					System.Diagnostics.Debug.WriteLine(ex.Message);
				}
			}
		}

		private void picView_DoubleClick(object sender, EventArgs e)
		{
			if (this.transform != null && this.Image != null)
			{
				if (this.OnDoubleClickHandler != null)
				{
					ArrayList selectedSymbols = this.GetSymbolsFromMousePoint();
					if (selectedSymbols.Count > 0)
					{
						this.OnDoubleClickHandler(this.currMousePosF,selectedSymbols);
					}
				}
			}
		}

		private void cbZoomWindow_CheckedChanged(object sender, System.EventArgs e)
		{
			if (this.cbZoomWindow.Checked)
			{
				this.CreateCommand(CommandState.ZoomWindow);
			}
			else
			{
				this.DeactivateCommand(CommandState.ZoomWindow,false);
			}
		}

		private ArrayList GetSymbolsFromMousePoint()
		{
			ArrayList selectedSymbols = new ArrayList();

			if (this.transform != null && this.Image != null)
			{
				System.Drawing.Drawing2D.Matrix invMat = this.transform.Clone(); 					
				invMat.Invert();
				Rectangle rect = Rectangle.Empty;

				PointF[] pts = new PointF[1];

				for (int i = 0; i < this.symbols.Count; i++)
				{
					Symbol symbol = (Symbol)this.symbols.GetByIndex(i);
					pts[0]	= symbol.origin;
					invMat.TransformPoints(pts);
					pts[0] = this.picView.TransformPoint(pts[0]);
					rect.Location = new Point((int)pts[0].X,(int)pts[0].Y);
					rect.Size = symbol.size;
					if (rect.Contains(this.currMouseScr))
					{
						selectedSymbols.Add(symbol);
					}
				}
			}

			return selectedSymbols;
		}

		/// <summary>
		/// Select a symbol from the list of symbols
		/// </summary>
		/// <param name="symbol"></param>
		/// <returns>true if the symbol has been selected, false if not</returns>
		public bool SelectSymbol(Symbol symbol)
		{
			int ind = this.symbols.IndexOfKey(symbol.name);
			if (ind < 0)
				return false;

			if (!this.selectedSymbols.ContainsKey(symbol.name))
			{
				this.selectedSymbols.Add(symbol.name,symbol);
			}

			if (this.selectedSymbols.Count == 1)
			{
				this.timer1.Start();
			}
			return true;
		}

		/// <summary>
		/// Unselect a symbol from the list of selected symbols
		/// </summary>
		/// <param name="symbol"></param>
		/// <returns>true if the symbol has been selected, false if not</returns>
		public bool UnselectSymbol(Symbol symbol)
		{
			int ind = this.symbols.IndexOfKey(symbol.name);
			if (ind < 0)
				return false;

			this.selectedSymbols.Remove(symbol.name);
			this.picView.Refresh();
			if (this.selectedSymbols.Count == 0)
			{
				this.timer1.Stop();
			}
			return true;
		}

		/// <summary>
		/// Unselect all currently selected symbols
		/// </summary>
		public void UnselectAll()
		{
			this.selectedSymbols.Clear();
			this.timer1.Stop();
			this.picView.Refresh();
		}

		/// <summary>
		/// Returns whether the specified symbol is currently slected or not
		/// </summary>
		/// <param name="symbol"></param>
		/// <returns>true if specified symbol is currently selected, false if not</returns>
		public bool IsSelectedSymbol(Symbol symbol)
		{
			return this.selectedSymbols.ContainsKey(symbol.name);
		}

		/// <summary>
		/// Clears the command stack. Resets the command processor to the <B>None</B> state
		/// </summary>
		public void	ClearCommandStack()
		{
			this.cmdStack.Clear();
			this.CreateCommand(CommandState.None);
		}

		private void timer1_Tick(object sender, System.EventArgs e)
		{
			this.drawSelected = !this.drawSelected;
			this.picView.Invalidate();
		}

		private void btnFitWidth_Click(object sender, System.EventArgs e)
		{
			this.picView.FitToWidth();
			this.udMagnifier.Value = (decimal)(this.picView.Scale * 100);
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			this.picView.FitToAll();
			this.udMagnifier.Value = (decimal)(this.picView.Scale * 100);
		}

		private	bool CreateCommand(CommandState cmdState)
		{
			Command cmd = new Command();
			cmd.cmdState = cmdState;

			switch(cmd.cmdState)
			{
				case CommandState.Camera:
					cmd.mouseState = MouseStateEx.CameraFirst;
					break;
				case CommandState.None:
					cmd.mouseState = MouseStateEx.None;
					break;
				case CommandState.Transformation:
					cmd.mouseState = MouseStateEx.Point1;
					break;
				case CommandState.Zoom:
					if ((this.udMagnifier.Value / 100) + this.minScaleFactor < (this.udMagnifier.Maximum / 100))
					{
						this.picView.Cursor = zoomIn;
						this.btnMagnifier.Image = this.zoomInImage;
						cmd.mouseState  = MouseStateEx.ZoomIn;
					}
					else if ((this.udMagnifier.Value / 100) - this.minScaleFactor > (this.udMagnifier.Minimum / 100))
					{
						this.picView.Cursor = zoomOut;
						this.btnMagnifier.Image = this.zoomOutImage;
						cmd.mouseState  = MouseStateEx.ZoomOut;
					}
					break;
				case CommandState.ZoomWindow:
					cmd.mouseState = MouseStateEx.ZoomFirst;
					break;
				case CommandState.WorldPoint:
					cmd.mouseState = MouseStateEx.WorldPoint;
					break;
			}

			this.cmdStack.Push(cmd);

			return ActivateCommand(cmdState);
		}

		private	bool ActivateCommand(CommandState cmdState)
		{
			if (this.cmdStack.Count == 0)
				return false;

			Command cmd = (Command)this.cmdStack.Peek();
			if (cmd.cmdState != cmdState)
				return false;

			switch(cmd.mouseState)
			{
				case MouseStateEx.ZoomFirst :
					this.picView.Cursor = this.zoomWindow;
					this.MouseStatus = MouseStateEx.ZoomFirst;
					this.btnMagnifier.Enabled = false;
					this.udMagnifier.Enabled  = false;
					this.cbZoomWindow.Enabled = true;
					this.btnFitAll.Enabled	  = false;
					this.btnFitWidth.Enabled  = false;	
					break;
				case MouseStateEx.ZoomSecond :
					this.picView.Cursor = this.zoomWindow;
					this.MouseStatus = MouseStateEx.ZoomSecond;
					break;
				case MouseStateEx.ZoomIn:
					this.picView.Cursor = zoomIn;
					this.btnMagnifier.Image = this.zoomInImage;
					this.currMouseStat  = MouseStateEx.ZoomIn;
					this.btnMagnifier.Enabled = true;
					this.udMagnifier.Enabled  = false;
					this.cbZoomWindow.Enabled = false;
					this.btnFitAll.Enabled	  = false;
					this.btnFitWidth.Enabled  = false;	
					break;
				case MouseStateEx.ZoomOut:
					this.picView.Cursor = zoomOut;
					this.btnMagnifier.Image = this.zoomOutImage;
					this.currMouseStat  = MouseStateEx.ZoomOut;
					this.btnMagnifier.Enabled = true;
					this.udMagnifier.Enabled  = false;
					this.cbZoomWindow.Enabled = false;
					this.btnFitAll.Enabled	  = false;
					this.btnFitWidth.Enabled  = false;	
					break;
				case MouseStateEx.Point1:
					this.picView.Cursor = this.pickPoint1;
					break;
				case MouseStateEx.Point2:
					this.picView.Cursor = this.pickPoint2;
					break;
				case MouseStateEx.Point3:
					break;
				case MouseStateEx.CameraFirst:
					this.picView.Cursor = this.pickPoint1;
					break;
				case MouseStateEx.CameraNext:
					this.picView.Cursor = this.pickPoint2;
					this.picturePoint1 = cmd.picturePoint1;
					this.worldPoint1   = cmd.worldPoint1;
					break;
				case MouseStateEx.None:
					this.picView.Cursor = Cursors.Default;
					this.MouseStatus = MouseStateEx.None;
					this.btnMagnifier.Enabled = true;
					this.udMagnifier.Enabled  = true;
					this.cbZoomWindow.Enabled = true;
					this.btnFitAll.Enabled	  = true;
					this.btnFitWidth.Enabled  = true;	
					break;
				case MouseStateEx.WorldPoint:
					this.picView.Cursor = this.pickPoint1;
					break;
			}

			this.MouseStatus = cmd.mouseState;
			return true;
		}


		private	bool ActivateNextMouseCommand(CommandState cmdState)
		{
			if (this.cmdStack.Count == 0)
				return false;

			Command cmd = (Command)this.cmdStack.Peek();
			if (cmd.cmdState != cmdState)
				return false;

			if (cmd.mouseState != this.currMouseStat)
				return false;

			switch(cmd.mouseState)
			{
				case MouseStateEx.ZoomFirst :
					this.picView.Capture = true;
					cmd.mouseState = MouseStateEx.ZoomSecond;
					break;
				case MouseStateEx.ZoomSecond :
					return DeactivateCommand(cmdState,true);
					break;
				case MouseStateEx.ZoomIn:
					cmd.mouseState = MouseStateEx.ZoomOut;
					this.picView.Cursor = zoomOut;
					this.btnMagnifier.Image = this.zoomOutImage;
					break;
				case MouseStateEx.ZoomOut:
					cmd.mouseState = MouseStateEx.ZoomIn;
					this.picView.Cursor = zoomIn;
					this.btnMagnifier.Image = this.zoomInImage;
					break;
				case MouseStateEx.Point1:
					cmd.mouseState = MouseStateEx.Point2;
					this.picView.Cursor = this.pickPoint2;
					break;
				case MouseStateEx.Point2:
					return DeactivateCommand(cmdState,true);
					break;
				case MouseStateEx.Point3:
					break;
				case MouseStateEx.CameraFirst:
					cmd.mouseState = MouseStateEx.CameraNext;
					this.picView.Cursor = this.pickPoint2;
					this.picturePoint1 = cmd.picturePoint1;
					this.worldPoint1   = cmd.worldPoint1;
					break;
				case MouseStateEx.CameraNext:
					return DeactivateCommand(cmdState,true);
					this.picturePoint2 = cmd.picturePoint2;
					this.worldPoint2   = cmd.worldPoint2;
					break;
				case MouseStateEx.None:
					break;
				case MouseStateEx.WorldPoint:
					return DeactivateCommand(cmdState,true);
					break;
			}

			this.MouseStatus = cmd.mouseState;
			return true;
		}

		private bool DeactivateCommand(CommandState state,bool success)
		{
			if (this.txnActive == true)
				return false;
			
			if (this.cmdStack.Count == 0)
				return false;
			Command cmd = (Command)this.cmdStack.Peek();
			if (cmd.cmdState != state)
				return false;

			this.txnActive = true;
			switch(cmd.cmdState)
			{
				case CommandState.Camera:
					if (success)
					{
						if (this.OnEndCameraPositionHandler != null)
						{
							SizeF size = new SizeF(cmd.worldPoint2.X - cmd.worldPoint1.X,cmd.worldPoint2.Y - cmd.worldPoint1.Y);
							if (size.Width < 0)
							{
								if (size.Height < 0)
								{
									size.Width *= -1;
									size.Height*= -1;
									RectangleF rect = new RectangleF(cmd.worldPoint2,size);
									this.OnEndCameraPositionHandler(rect);
								}
								else
								{
									size.Width *= -1;
									cmd.worldPoint2.Y = cmd.worldPoint1.Y;
									RectangleF rect = new RectangleF(cmd.worldPoint2,size);
									this.OnEndCameraPositionHandler(rect);
								}
							}
							else if (size.Height < 0)
							{
								size.Height*= -1;
								cmd.worldPoint2.X = cmd.worldPoint1.X;
								RectangleF rect = new RectangleF(cmd.worldPoint2,size);
								this.OnEndCameraPositionHandler(rect);
							}
							else
							{
								RectangleF rect = new RectangleF(cmd.worldPoint1,size);
								this.OnEndCameraPositionHandler(rect);
							}
						}
					}
					else
					{
						if (this.OnCancelCameraPositionHandler != null)
						{
							this.OnCancelCameraPositionHandler();
						}
					}
					break;
				case CommandState.None:
					break;
				case CommandState.Transformation:
					if (success)
					{
						if (this.OnEndTransformationHandler != null)
						{
							try
							{
								// logical checks
								Size picSize = new Size(cmd.picturePoint2.X - cmd.picturePoint1.X,cmd.picturePoint2.Y - cmd.picturePoint1.Y);
								if (Math.Abs(picSize.Width) < 2 && Math.Abs(picSize.Height) < 2)
								{
									throw new InvalidOperationException("The two picture points must be different to get correct results!");
								}

								SizeF wldSize = new SizeF(cmd.worldPoint2.X - cmd.worldPoint1.X,cmd.worldPoint2.Y - cmd.worldPoint1.Y);
								if (Math.Abs(wldSize.Width) < 1.0e-5 && Math.Abs(wldSize.Height) < 1.0e-5)
								{
									throw new InvalidOperationException("The two world points must be different to get correct results!");
								}

								RectangleF rect = new RectangleF(cmd.picturePoint1.X,cmd.picturePoint1.Y,cmd.picturePoint2.X - cmd.picturePoint1.X,cmd.picturePoint2.Y - cmd.picturePoint1.Y);
								PointF[] pts = new PointF[3];
								pts[0] = cmd.worldPoint1;
								pts[1].X = cmd.worldPoint2.X;
								pts[1].Y = cmd.worldPoint1.Y;
								pts[2].X = cmd.worldPoint1.X;
								pts[2].Y = cmd.worldPoint2.Y;
								System.Drawing.Drawing2D.Matrix transform = new System.Drawing.Drawing2D.Matrix(rect,pts);			

								pts[0].X = 0;
								pts[0].Y = 0;
								pts[1].X = this.picView.Image.Width;
								pts[1].Y = this.picView.Image.Height;


								transform.TransformPoints(pts);
								SizeF  size	  = new SizeF(pts[1].X - pts[0].X,pts[1].Y - pts[0].Y);	
								if (size.Height < 1.0e-5 || size.Width < 1.0e-5)
								{
									throw new InvalidOperationException("Invalid transformation operation occured!\nPlease check the correctness of the input parameters!");
								}
								else
								{
									this.transform= transform;
									this.dimension= new RectangleF(pts[0],size);
									this.OnEndTransformationHandler(pts[0],size);
								}
							}
							catch(Exception ex)
							{
								MessageBox.Show(this,ex.Message,"Error");
								if (this.OnCancelTransformationHandler != null)
								{
									this.OnCancelTransformationHandler();
								}
							}
						}
					}
					else
					{
						if (this.OnCancelTransformationHandler != null)
						{
							this.OnCancelTransformationHandler();
						}
					}
					break;
				case CommandState.Zoom:
					this.picView.Cursor = Cursors.Default;
					this.btnMagnifier.Image = this.magnifierImage;
					this.currMouseStat  = MouseStateEx.None;
					this.btnMagnifier.Enabled = true;
					this.udMagnifier.Enabled  = true;
					this.cbZoomWindow.Enabled = true;
					this.btnFitAll.Enabled	  = true;
					this.btnFitWidth.Enabled  = true;
					if (success)
					{
						this.udMagnifier.Value = (decimal)(this.picView.Scale * 100);
					}
					break;
				case CommandState.ZoomWindow:
					this.picView.Capture = false;
					this.cbZoomWindow.Checked = false;
					this.btnMagnifier.Enabled = true;
					this.udMagnifier.Enabled  = true;
					this.cbZoomWindow.Enabled = true;
					this.btnFitAll.Enabled	  = true;
					this.btnFitWidth.Enabled  = true;	
					this.cbZoomWindow.Checked = false;
					if (success)
					{
						if (this.picView.ZoomWindow(cmd.picturePoint1,cmd.picturePoint2))
						{
							decimal newValue = (decimal)(this.picView.Scale * 100);
							if (newValue > this.udMagnifier.Maximum)
							{
								this.udMagnifier.Maximum = newValue;
							}
							this.udMagnifier.Value = newValue;
						}
					}
					break;
				case CommandState.WorldPoint:
					if (success)
					{
						if (this.OnEndWorldPointHandler != null)
						{
							this.OnEndWorldPointHandler(cmd.worldPoint1);
						}
					}
					else
					{
						if (this.OnCancelWorldPointHandler != null)
						{
							this.OnCancelWorldPointHandler();
						}
					}
					break;
			}

			this.cmdStack.Pop();
            
			if (this.cmdStack.Count > 0)
			{
				cmd = (Command)this.cmdStack.Peek();
				ActivateCommand(cmd.cmdState);
			}
			else
			{
				this.picView.Cursor = Cursors.Default;
				this.MouseStatus = MouseStateEx.None;
				this.btnMagnifier.Enabled = true;
				this.udMagnifier.Enabled  = true;
				this.cbZoomWindow.Enabled = true;
				this.btnFitAll.Enabled	  = true;
				this.btnFitWidth.Enabled  = true;	
			}

			this.txnActive = false;
			return true;
		}

		private bool AddParameterToCommand(Point picturePoint)
		{
			if (this.cmdStack.Count == 0)
				return false;
			Command cmd = (Command)this.cmdStack.Peek();
			if (cmd.mouseState != this.currMouseStat)
				return false;

			switch(cmd.mouseState)
			{
				case MouseStateEx.ZoomFirst :
					cmd.picturePoint1 = picturePoint;
					this.picturePoint1= picturePoint;
					break;
				case MouseStateEx.ZoomSecond :
					cmd.picturePoint2 = picturePoint;
					this.picturePoint2= picturePoint;
					break;
				case MouseStateEx.ZoomIn:
					cmd.picturePoint1 = picturePoint;
					break;
				case MouseStateEx.ZoomOut:
					cmd.picturePoint1 = picturePoint;
					break;
				case MouseStateEx.Point1:
					cmd.picturePoint1 = picturePoint;
					break;
				case MouseStateEx.Point2:
					cmd.picturePoint2 = picturePoint;
					break;
				case MouseStateEx.Point3:
					break;
				case MouseStateEx.CameraFirst:
					cmd.picturePoint1 = picturePoint;
					break;
				case MouseStateEx.CameraNext:
					cmd.picturePoint2 = picturePoint;
					break;
				case MouseStateEx.None:
					break;
				case MouseStateEx.WorldPoint:
					cmd.picturePoint1 = picturePoint;
					break;
			}
			return true;
		}

		private void PictureControlEx_Load(object sender, System.EventArgs e)
		{
		}

		private bool AddParameterToCommand(PointF worldPoint)
		{
			if (this.cmdStack.Count == 0)
				return false;
			Command cmd = (Command)this.cmdStack.Peek();
			if (cmd.mouseState != this.currMouseStat)
				return false;

			switch(cmd.mouseState)
			{
				case MouseStateEx.ZoomFirst :
					break;
				case MouseStateEx.ZoomSecond :
					break;
				case MouseStateEx.ZoomIn:
					break;
				case MouseStateEx.ZoomOut:
					break;
				case MouseStateEx.Point1:
					cmd.worldPoint1 = worldPoint;
					break;
				case MouseStateEx.Point2:
					cmd.worldPoint2 = worldPoint;
					break;
				case MouseStateEx.Point3:
					break;
				case MouseStateEx.CameraFirst:
					cmd.worldPoint1 = worldPoint;
					break;
				case MouseStateEx.CameraNext:
					cmd.worldPoint2 = worldPoint;
					break;
				case MouseStateEx.None:
					break;
				case MouseStateEx.WorldPoint:
					cmd.worldPoint1 = worldPoint;
					break;
			}
			return true;
		}

		/// <summary>
		/// Activates the <B>Calculate world point</B> command.
		/// </summary>
		public void	CalculateWorldPoint()
		{
			this.CreateCommand(CommandState.WorldPoint);
		}

	}
}
