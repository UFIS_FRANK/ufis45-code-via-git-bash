using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace PictureViewer
{
	/// <summary>
	/// The PanelEx class is derived from <see cref="System.Windows.Forms.UserControl"/> and implements the extended version of
	/// an <see cref="System.Windows.Forms.Panel"/>
	/// </summary>
	/// <remarks>The implementation of this class was necessary due to a bug in <B>Panel</B></remarks>
	public class PanelEx : System.Windows.Forms.UserControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private	Point	lastScrollPosition;

		/// <summary>
		/// The default constructor for an instance of this class
		/// </summary>
		/// <remarks>None</remarks>
		public PanelEx()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			// 
			// PanelEx
			// 
			this.Name = "PanelEx";
		}
		#endregion

		/// <summary>
		/// Returns the last valid scroll position saved
		/// </summary>
		/// <value>The scroll position in coordinates</value>
		public Point LastScrollPosition
		{
			get
			{
				return this.lastScrollPosition;
			}
		}

		/// <summary>
		/// Overwritten to store the current scroll position within a local member variable.
		/// </summary>
		/// <param name="e"></param>
		protected override void OnEnter(EventArgs e)
		{
			base.OnEnter (e);
			if (this.AutoScroll)
			{
				this.lastScrollPosition = new Point(-this.AutoScrollPosition.X,-this.AutoScrollPosition.Y);
			}
		}

		/// <summary>
		/// Overwritten to store the current scroll position within a local member variable.
		/// </summary>
		/// <param name="displayScrollbars"></param>
		protected override void AdjustFormScrollbars(bool displayScrollbars)
		{
			base.AdjustFormScrollbars (displayScrollbars);
			if (this.AutoScroll)
			{
				this.lastScrollPosition = new Point(-this.AutoScrollPosition.X,-this.AutoScrollPosition.Y);
			}
		}
	}
}
