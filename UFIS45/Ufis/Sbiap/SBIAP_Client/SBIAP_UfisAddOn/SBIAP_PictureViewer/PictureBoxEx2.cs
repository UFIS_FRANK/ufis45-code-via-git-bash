using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace PictureViewer
{
	public enum PictureBoxExSizeMode
	{
		AutoSize,		// The PictureBox is sized equal to the size of the image that it contains.
		CenterImage,	// The image is displayed in the center if the PictureBox is larger than the image. 
						// If the image is larger than the PictureBox, the picture is placed in the center of the PictureBox
						// and the outside edges are clipped
		Normal,			// The image is placed in the upper-left corner of the PictureBox. 
						// The image is clipped if it is larger than the PictureBox it is contained in.
		StretchImage,	// The image within the PictureBox is stretched or shrunk to fit the size of the PictureBox.
		FullScreen,		// image will be displayed in full screen mode
		Scaled			// image will be displayed with respect to the currently used scaling factor
	}

	/// <summary>
	/// Summary description for PictureBoxEx.
	/// </summary>
	public class PictureBoxEx2 : System.Windows.Forms.UserControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private	System.Drawing.Image			image = null;
		private	PictureBoxExSizeMode			mode  = PictureBoxExSizeMode.FullScreen;
		private	float							scale = 1.0F;

		public PictureBoxEx2()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
			this.SetStyle(ControlStyles.UserPaint|ControlStyles.AllPaintingInWmPaint|ControlStyles.DoubleBuffer,true);
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			// 
			// PictureBoxEx2
			// 
			this.Name = "PictureBoxEx2";

		}
		#endregion

		public Image Image
		{
			get
			{
				return this.image;
			}
			set
			{
				if (this.image != value)
				{
					this.image = value;
					UpdateControl();
				}
			}
		}

		public PictureBoxExSizeMode SizeMode
		{
			get
			{
				return this.mode;					
			}

			set
			{
				if (this.mode != value)
				{
					this.mode = value;
					UpdateControl();
				}
			}
		}

		public new float	Scale
		{
			get
			{
				return this.scale;
			}
			set
			{
				if (this.scale != value)
				{
					this.scale = value;
					UpdateControl();
				}
			}
		}


		private void UpdateControl()
		{
			if (this.image != null)
			{
				int width,height;
				switch(this.mode)
				{
					case PictureBoxExSizeMode.AutoSize :
						width = this.image.Width;
						height= this.image.Height;
						this.Size = new Size(width,height);
						break;
					case PictureBoxExSizeMode.CenterImage :
						this.Size = this.ClientSize;	// reset control size
						break;
					case PictureBoxExSizeMode.Normal :
						this.Size = this.ClientSize;	// reset control size
						break;
					case PictureBoxExSizeMode.StretchImage :
						this.Size = this.ClientSize;	// reset control size
						break;
					case PictureBoxExSizeMode.FullScreen :
						this.Size = this.ClientSize;	// reset control size
						break;
					case PictureBoxExSizeMode.Scaled :
						width = (int)Math.Floor(this.scale *this.image.Width);
						height= (int)Math.Floor(this.scale *this.image.Height);
						this.Size = new Size(width,height);
						break;
				}

				this.Invalidate();
				if (this.Parent != null)
				{
					this.Parent.Refresh();
				}
			}
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint (e);
			if (this.image != null)
			{
				switch(this.mode)
				{
					case PictureBoxExSizeMode.AutoSize:
						Rectangle src = new Rectangle(0,0,this.image.Width,this.image.Height);
						Rectangle dst = new Rectangle(0,0,this.Width,this.Height);
						e.Graphics.DrawImage(this.image,dst,src,GraphicsUnit.Pixel);
						break;
					case PictureBoxExSizeMode.CenterImage:
						Point cp = new Point(this.image.Width/2,this.image.Height/2);
						src = new Rectangle(cp.X - Math.Min(this.Width,this.image.Width) / 2,cp.Y - Math.Min(this.Height,this.image.Height) / 2,Math.Min(this.Width,this.image.Width),Math.Min(this.Height,this.image.Height));
						dst = new Rectangle(0,0,this.Width,this.Height);
						e.Graphics.DrawImage(this.image,dst,src,GraphicsUnit.Pixel);
						break;
					case PictureBoxExSizeMode.Normal:
						src = new Rectangle(0,0,Math.Min(this.Width,this.image.Width),Math.Min(this.Height,this.image.Height));
						dst = new Rectangle(0,0,this.Width,this.Height);
						e.Graphics.DrawImage(this.image,dst,src,GraphicsUnit.Pixel);
						break;
					case PictureBoxExSizeMode.StretchImage:
						src = new Rectangle(0,0,this.image.Width,this.image.Height);
						dst = new Rectangle(0,0,this.Width,this.Height);
						e.Graphics.DrawImage(this.image,dst,src,GraphicsUnit.Pixel);
						break;
					case PictureBoxExSizeMode.FullScreen:
						src = new Rectangle(0,0,this.image.Width,this.image.Height);
						dst = new Rectangle(0,0,this.Width,this.Height);
						e.Graphics.DrawImage(this.image,dst,src,GraphicsUnit.Pixel);
						break;
					case PictureBoxExSizeMode.Scaled:
						src = new Rectangle(0,0,this.image.Width,this.image.Height);
						dst = new Rectangle(0,0,(int)Math.Floor(this.scale *this.image.Width),(int)Math.Floor(this.scale *this.image.Height));
						e.Graphics.DrawImage(this.image,dst,src,GraphicsUnit.Pixel);
						break;
				}
			}
		}

	}
}
