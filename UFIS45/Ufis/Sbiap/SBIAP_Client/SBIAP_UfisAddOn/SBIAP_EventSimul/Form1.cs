using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Globalization;
using Ufis.Data;
using Ufis.Utils;

namespace SBIAP_StatusEventSimulator
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.CheckBox cbSimulation;
		private System.Windows.Forms.Label lblSimulation;
		private System.Windows.Forms.Timer timer1;
		private System.ComponentModel.IContainer components;
		private	System.Globalization.NumberFormatInfo nfi;
		private	Random random = new Random();
		private	float  width;
		private float  height;
		private	int	   floors;	
		private int	   asgIndex = -1;
		private System.Windows.Forms.Label lblInserts;
		private System.Windows.Forms.TextBox txtIRT;
		private System.Windows.Forms.TextBox txtURT;
		private System.Windows.Forms.Label lblURT;
		private System.Windows.Forms.TextBox txtDRT;
		private System.Windows.Forms.Label lblDRT;
		private System.Windows.Forms.GroupBox groupBox1;
		private int	   asdIndex = -1;
		private	int	   adcIndex = -1;
		private	int	   acsIRT = 0;
		private	int	   acsURT = 0;
		private System.Windows.Forms.Label lblTotal;
		private System.Windows.Forms.TextBox txtTotal;
		private System.Windows.Forms.Label lblSpeed;
		private System.Windows.Forms.TextBox txtSpeed;	
		private	int	   acsDRT = 0;
		private System.Windows.Forms.CheckBox cbDelete;
		private System.Windows.Forms.CheckBox cbInserts;
		private System.Windows.Forms.CheckBox cbUpdates;
		private System.Windows.Forms.CheckBox cbGenerateCoordinates;	
		private	DateTime	lastTime = DateTime.Now;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			nfi = new System.Globalization.NumberFormatInfo();
			nfi.NumberDecimalSeparator = ".";

			//Read ceda.ini and set it global to UT (UfisTools)
			String strServer="";
			String strHopo = "";
			Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\ceda.ini");
			strServer = myIni.IniReadValue("GLOBAL", "HOSTNAME");
			strHopo = myIni.IniReadValue("GLOBAL", "HOMEAIRPORT");
			UT.ServerName = strServer;
			UT.Hopo = strHopo;
			UT.UserName = "AIMSSIMU";
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				UT.DisposeMemDB();

				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.cbSimulation = new System.Windows.Forms.CheckBox();
			this.lblSimulation = new System.Windows.Forms.Label();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.lblInserts = new System.Windows.Forms.Label();
			this.txtIRT = new System.Windows.Forms.TextBox();
			this.txtURT = new System.Windows.Forms.TextBox();
			this.lblURT = new System.Windows.Forms.Label();
			this.txtDRT = new System.Windows.Forms.TextBox();
			this.lblDRT = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.cbUpdates = new System.Windows.Forms.CheckBox();
			this.cbInserts = new System.Windows.Forms.CheckBox();
			this.lblSpeed = new System.Windows.Forms.Label();
			this.txtSpeed = new System.Windows.Forms.TextBox();
			this.lblTotal = new System.Windows.Forms.Label();
			this.txtTotal = new System.Windows.Forms.TextBox();
			this.cbDelete = new System.Windows.Forms.CheckBox();
			this.cbGenerateCoordinates = new System.Windows.Forms.CheckBox();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// cbSimulation
			// 
			this.cbSimulation.Appearance = System.Windows.Forms.Appearance.Button;
			this.cbSimulation.Location = new System.Drawing.Point(304, 16);
			this.cbSimulation.Name = "cbSimulation";
			this.cbSimulation.Size = new System.Drawing.Size(48, 24);
			this.cbSimulation.TabIndex = 0;
			this.cbSimulation.Text = "&Start";
			this.cbSimulation.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.cbSimulation.CheckedChanged += new System.EventHandler(this.cbSimulation_CheckedChanged);
			// 
			// lblSimulation
			// 
			this.lblSimulation.Location = new System.Drawing.Point(24, 24);
			this.lblSimulation.Name = "lblSimulation";
			this.lblSimulation.Size = new System.Drawing.Size(100, 16);
			this.lblSimulation.TabIndex = 1;
			this.lblSimulation.Text = "Simulation :";
			// 
			// timer1
			// 
			this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
			// 
			// lblInserts
			// 
			this.lblInserts.Location = new System.Drawing.Point(160, 24);
			this.lblInserts.Name = "lblInserts";
			this.lblInserts.TabIndex = 2;
			this.lblInserts.Text = "Inserts";
			// 
			// txtIRT
			// 
			this.txtIRT.Location = new System.Drawing.Point(48, 24);
			this.txtIRT.Name = "txtIRT";
			this.txtIRT.ReadOnly = true;
			this.txtIRT.TabIndex = 3;
			this.txtIRT.Text = "";
			// 
			// txtURT
			// 
			this.txtURT.Location = new System.Drawing.Point(48, 48);
			this.txtURT.Name = "txtURT";
			this.txtURT.ReadOnly = true;
			this.txtURT.TabIndex = 5;
			this.txtURT.Text = "";
			// 
			// lblURT
			// 
			this.lblURT.Location = new System.Drawing.Point(160, 48);
			this.lblURT.Name = "lblURT";
			this.lblURT.TabIndex = 4;
			this.lblURT.Text = "Updates";
			// 
			// txtDRT
			// 
			this.txtDRT.Location = new System.Drawing.Point(48, 72);
			this.txtDRT.Name = "txtDRT";
			this.txtDRT.ReadOnly = true;
			this.txtDRT.TabIndex = 7;
			this.txtDRT.Text = "";
			// 
			// lblDRT
			// 
			this.lblDRT.Location = new System.Drawing.Point(160, 72);
			this.lblDRT.Name = "lblDRT";
			this.lblDRT.TabIndex = 6;
			this.lblDRT.Text = "Deletes";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.cbUpdates);
			this.groupBox1.Controls.Add(this.cbInserts);
			this.groupBox1.Controls.Add(this.lblSpeed);
			this.groupBox1.Controls.Add(this.txtSpeed);
			this.groupBox1.Controls.Add(this.lblTotal);
			this.groupBox1.Controls.Add(this.txtTotal);
			this.groupBox1.Controls.Add(this.txtIRT);
			this.groupBox1.Controls.Add(this.lblDRT);
			this.groupBox1.Controls.Add(this.txtDRT);
			this.groupBox1.Controls.Add(this.lblURT);
			this.groupBox1.Controls.Add(this.txtURT);
			this.groupBox1.Controls.Add(this.lblInserts);
			this.groupBox1.Controls.Add(this.cbDelete);
			this.groupBox1.Location = new System.Drawing.Point(16, 56);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(272, 176);
			this.groupBox1.TabIndex = 8;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Status events generated";
			// 
			// cbUpdates
			// 
			this.cbUpdates.Enabled = false;
			this.cbUpdates.Location = new System.Drawing.Point(16, 48);
			this.cbUpdates.Name = "cbUpdates";
			this.cbUpdates.Size = new System.Drawing.Size(16, 24);
			this.cbUpdates.TabIndex = 13;
			// 
			// cbInserts
			// 
			this.cbInserts.Checked = true;
			this.cbInserts.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbInserts.Location = new System.Drawing.Point(16, 24);
			this.cbInserts.Name = "cbInserts";
			this.cbInserts.Size = new System.Drawing.Size(16, 24);
			this.cbInserts.TabIndex = 12;
			// 
			// lblSpeed
			// 
			this.lblSpeed.Location = new System.Drawing.Point(160, 144);
			this.lblSpeed.Name = "lblSpeed";
			this.lblSpeed.TabIndex = 10;
			this.lblSpeed.Text = "Events / Second";
			// 
			// txtSpeed
			// 
			this.txtSpeed.Location = new System.Drawing.Point(48, 144);
			this.txtSpeed.Name = "txtSpeed";
			this.txtSpeed.ReadOnly = true;
			this.txtSpeed.TabIndex = 11;
			this.txtSpeed.Text = "";
			// 
			// lblTotal
			// 
			this.lblTotal.Location = new System.Drawing.Point(160, 104);
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.TabIndex = 8;
			this.lblTotal.Text = "Total";
			// 
			// txtTotal
			// 
			this.txtTotal.Location = new System.Drawing.Point(48, 104);
			this.txtTotal.Name = "txtTotal";
			this.txtTotal.ReadOnly = true;
			this.txtTotal.TabIndex = 9;
			this.txtTotal.Text = "";
			// 
			// cbDelete
			// 
			this.cbDelete.Enabled = false;
			this.cbDelete.Location = new System.Drawing.Point(16, 72);
			this.cbDelete.Name = "cbDelete";
			this.cbDelete.Size = new System.Drawing.Size(16, 24);
			this.cbDelete.TabIndex = 9;
			// 
			// cbGenerateCoordinates
			// 
			this.cbGenerateCoordinates.Location = new System.Drawing.Point(136, 16);
			this.cbGenerateCoordinates.Name = "cbGenerateCoordinates";
			this.cbGenerateCoordinates.Size = new System.Drawing.Size(136, 24);
			this.cbGenerateCoordinates.TabIndex = 14;
			this.cbGenerateCoordinates.Text = "Generate coordinates";
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(504, 446);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.lblSimulation);
			this.Controls.Add(this.cbSimulation);
			this.Controls.Add(this.cbGenerateCoordinates);
			this.Name = "Form1";
			this.Text = "AIMS Viewer Status event simulator";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void Form1_Load(object sender, System.EventArgs e)
		{
			this.LoadData();		
		}

		public bool LoadData()
		{
			IDatabase myDB = UT.GetMemDB();
			myDB.Multithreaded = true;
			myDB.IndexUpdateImmediately = true;
			myDB = (IDatabase) myDB.SyncRoot;
			bool result = myDB.Connect(UT.ServerName,UT.UserName,UT.Hopo,"TAB","AIMSSIMU");
			if (!result)
			{
				Application.Exit();
			}

			// load AIMS viewer general settings
			ITable myTable = myDB.Bind("AGS", "AGS", "URNO,WIDT,HEIG,FLOR,PATH,USEC,USEU,CDAT,LSTU", "10,10,10,3,255,32,32,14,14", "URNO,WIDT,HEIG,FLOR,PATH,USEC,USEU,CDAT,LSTU");
			myTable = (ITable)myTable.SyncRoot;
			myTable.Load("");
			myTable.CreateIndex("URNO", "URNO");
			if (myTable.Count == 1)
			{
				this.width  = float.Parse(myTable[0]["WIDT"],NumberStyles.Number,nfi);
				this.height = float.Parse(myTable[0]["HEIG"],NumberStyles.Number,nfi);
				this.floors = int.Parse(myTable[0]["FLOR"]);
			}

			// load AIMS viewer viewpoint definitions
			myTable = myDB.Bind("AVP", "AVP", "URNO,NAME,XOFF,YOFF,WIDT,HEIG,FLOR,FOVE,PATH,USEC,USEU,CDAT,LSTU", "10,32,10,10,10,10,3,1,255,32,32,14,14", "URNO,NAME,XOFF,YOFF,WIDT,HEIG,FLOR,FOVE,PATH,USEC,USEU,CDAT,LSTU");
			myTable = (ITable)myTable.SyncRoot;
			myTable.Load("ORDER BY NAME");
			myTable.CreateIndex("URNO", "URNO");
			myTable.CreateIndex("NAME", "NAME");
			myTable.CreateIndex("FLOR", "FLOR");

			// load AIMS viewer status group definitions
			myTable = myDB.Bind("ASG", "ASG", "URNO,NAME,SUBS,ORIG,TYPE,IMAG,WIDT,HEIG,USEC,USEU,CDAT,LSTU", "10,32,32,32,32,255,5,5,32,32,14,14", "URNO,NAME,SUBS,ORIG,TYPE,IMAG,WIDT,HEIG,USEC,USEU,CDAT,LSTU");
			myTable = (ITable)myTable.SyncRoot;
			myTable.Load("ORDER BY NAME");
			myTable.CreateIndex("URNO", "URNO");
			myTable.CreateIndex("NAME", "NAME");
			myTable.CreateIndex("SUBS", "SUBS");
			myTable.CreateIndex("ORIG", "ORIG");
			myTable.CreateIndex("TYPE", "TYPE");

			// load AIMS viewer status message definitions
			myTable = myDB.Bind("ASD", "ASD", "URNO,SGRP,IFCO,STAT,COLO,ADVI,USEC,USEU,CDAT,LSTU", "10,10,32,32,20,255,32,32,14,14", "URNO,SGRP,IFCO,STAT,COLO,ADVI,USEC,USEU,CDAT,LSTU");
			myTable = (ITable)myTable.SyncRoot;
			myTable.Load("ORDER BY SGRP");
			myTable.CreateIndex("URNO", "URNO");
			myTable.CreateIndex("SGRP", "SGRP");

			// load AIMS viewer tv camera definitions
			myTable = myDB.Bind("ATV", "ATV", "URNO,NAME,XPO1,YPO1,XPO2,YPO2,FLOR,USEC,USEU,CDAT,LSTU", "10,32,10,10,10,10,3,32,32,14,14", "URNO,NAME,XPO1,YPO1,XPO2,YPO2,FLOR,USEC,USEU,CDAT,LSTU");
			myTable = (ITable)myTable.SyncRoot;
			myTable.Load("ORDER BY NAME");
			myTable.CreateIndex("URNO", "URNO");
			myTable.CreateIndex("NAME", "NAME");
			myTable.CreateIndex("FLOR", "FLOR");

			// load AIMS viewer device coordinates
			myTable = myDB.Bind("ADC", "ADC", "DSCR,FLOR,NAME,ORIG,SUBS,XCOD,YCOD,URNO,DGID", "100,3,50,32,32,10,10,10,32", "DSCR,FLOR,NAME,ORIG,SUBS,XCOD,YCOD,URNO,DGID");
			myTable = (ITable)myTable.SyncRoot;
			myTable.Load("ORDER BY NAME");
			myTable.CreateIndex("URNO", "URNO");
			myTable.CreateIndex("NAME", "NAME");
			myTable.CreateIndex("SUBS", "SUBS");
			myTable.CreateIndex("ORIG", "ORIG");
			myTable.CreateIndex("DGID", "DGID");

			// load AIMS viewer status event definitions
			myTable = myDB.Bind("ASE", "ASE", "URNO,NAME,SUBS,DGID,ORIG,DSCR,XCOD,YCOD,FLOR,STAT,CDAT,SDAT,ID", "10,50,32,32,32,100,10,10,3,32,14,14,128", "URNO,NAME,SUBS,DGID,ORIG,DSCR,XCOD,YCOD,FLOR,STAT,CDAT,SDAT");
			myTable = (ITable)myTable.SyncRoot;
			myTable.Load("");
			myTable.CreateIndex("URNO", "URNO");
			myTable.CreateIndex("NAME", "NAME");
			myTable.CreateIndex("SUBS", "SUBS");
			myTable.CreateIndex("ORIG", "ORIG");

			return true;
									
		}

		private void Simulate()
		{
			IDatabase myDB = UT.GetMemDB();
			myDB = (IDatabase)myDB.SyncRoot;
			ITable	  myTable = myDB["ASE"];
			myTable = (ITable)myTable.SyncRoot;

			int	min = 1;
			int max = 4;
			int eventType = this.random.Next(min,max);

			switch(eventType)
			{
				case 1 : // create new event
					if (this.cbInserts.Checked == true)
					{
						if (!NextEvent(myTable,myDB))
						{
							this.cbSimulation.Checked = false;
						}
						else
						{
							this.txtIRT.Text = this.acsIRT.ToString();
							this.txtIRT.Refresh();
						}
					}
					break;
				case 2: // update existing event
					if (this.cbUpdates.Checked == true)
					{
						if (!this.UpdateEvent(myTable))
						{
							this.cbSimulation.Checked = false;
						}
						else
						{
							this.txtURT.Text = this.acsURT.ToString();
							this.txtURT.Refresh();
						}
					}
					break;
				case 3: // delete existing event -> status for this device is OK now
					if (this.cbDelete.Checked == true)
					{
						if (!this.DeleteEvent(myTable))
						{
							this.cbSimulation.Checked = false;
						}
						else
						{
							this.txtDRT.Text = this.acsDRT.ToString();
							this.txtDRT.Refresh();
						}
					}
					break;
			}
			
			int total = this.acsIRT+this.acsURT+this.acsDRT;
			this.txtTotal.Text = total.ToString();
			this.txtTotal.Refresh();

			TimeSpan diff = DateTime.Now - this.lastTime;
			double speed = total / diff.TotalSeconds;
			this.txtSpeed.Text = speed.ToString("F");
			this.txtSpeed.Refresh();
		}

		private bool UpdateEvent(ITable myTable)
		{
			int min = 0;
			int max = myTable.Count;

			if (max == 0)
				return true;

			int ind = this.random.Next(min,max);

			IRow row = myTable[ind];
			if (row == null)
				return true;

			row = (IRow)row.SyncRoot;
			row.Status = State.Modified;
			row["LSTU"]= UT.DateTimeToCeda(DateTime.UtcNow);
			row["USEU"]= UT.UserName;
			
			if (myTable.Save(row))
			{
				++this.acsURT;
				return true;
			}
			else
				return false;

		}

		private bool DeleteEvent(ITable myTable)
		{
			int min = 0;
			int max = myTable.Count;

			if (max == 0)
				return true;

			int ind = this.random.Next(min,max);

			IRow row = myTable[ind];
			if (row == null)
				return true;

			row = (IRow)row.SyncRoot;
			row.Status = State.Deleted;
			row["LSTU"]= UT.DateTimeToCeda(DateTime.UtcNow);
			row["USEU"]= UT.UserName;
			
			if (myTable.Save(row))
			{
				++this.acsDRT;
				return true;
			}
			else
				return false;
		}

		private string GenerateName(ITable myADCTable,IRow myASERow)
		{
			IRow[] rows = myADCTable.RowsByIndexValue("SUBS",myASERow["SUBS"]);
			for (int i = 0; i < rows.Length; i++)
			{
				if (rows[i]["ORIG"] == myASERow["ORIG"] && rows[i]["DGID"] == myASERow["DGID"])
					return rows[i]["NAME"];
			}
			return myASERow["URNO"];

		}

		private bool NextEvent(ITable myTable,IDatabase myDB)
		{
			int	min = -1;
			int max = this.floors;
			int floor = this.random.Next(min,max);
			float x = (float)this.random.NextDouble() * width;
			float y = (float)this.random.NextDouble() * height;

			IRow row = myTable.CreateEmptyRow();
			row = (IRow)row.SyncRoot;
	
			if (this.cbGenerateCoordinates.Checked)
			{
				row["XCOD"] = x.ToString("F",nfi);
				row["YCOD"] = y.ToString("F",nfi);
				row["FLOR"] = floor.ToString();
			}

			row["USEU"] = UT.UserName;
			row["LSTU"] = UT.DateTimeToCeda(DateTime.UtcNow);

			row["SDAT"] = UT.DateTimeToCeda(DateTime.UtcNow);


			ITable	  myASGTable = myDB["ASG"];	
			myASGTable = (ITable)myASGTable.SyncRoot;
			if (this.asgIndex == -1)
			{
				if (myASGTable.Count == 0)
					return false;
				this.asgIndex = 0;

			}
			else if (this.asdIndex == -1)
			{
				++this.asgIndex;
				if (this.asgIndex == myASGTable.Count)
				{
					this.asgIndex = -1;
					return this.NextEvent(myTable,myDB);
				}
			}

			row["SUBS"] = myASGTable[this.asgIndex]["SUBS"];
			row["DGID"] = myASGTable[this.asgIndex]["TYPE"];
			row["ORIG"] = myASGTable[this.asgIndex]["ORIG"];

			ITable myASDTable = myDB["ASD"];
			myASDTable = (ITable)myASDTable.SyncRoot;

			if (myASDTable.Count == 0)
				return false;

			if (this.asdIndex == -1)			
			{
				this.asdIndex = 0;
			}
			else
			{
				++this.asdIndex;
				if (this.asdIndex == myASDTable.Count)
				{
					this.asdIndex = -1;
					return this.NextEvent(myTable,myDB);
				}
			}

			// check whether this status definition matches the status group
			if (myASDTable[this.asdIndex]["SGRP"] != myASGTable[this.asgIndex]["URNO"])
			{
				return this.NextEvent(myTable,myDB);
			}
			else
			{
				row["STAT"] = myASDTable[this.asdIndex]["IFCO"];

				if (this.cbGenerateCoordinates.Checked)
				{
					row["NAME"] = row["URNO"];
				}
				else
				{
					ITable myADCTable = myDB["ADC"];
					myADCTable = (ITable)myADCTable.SyncRoot;

					if (myADCTable.Count == 0)
						return false;

					if (this.adcIndex == -1)			
					{
						this.adcIndex = 0;
					}
					else
					{
						++this.adcIndex;
						if (this.adcIndex == myADCTable.Count)
						{
							this.adcIndex = -1;
							return this.NextEvent(myTable,myDB);
						}
					}

					row["NAME"] = GenerateName(myADCTable,row);
				}

				row.Status  = State.Created;

				myTable.Add(row);
				if (myTable.Save(row))
				{
					++this.acsIRT;
					return true;
				}
				else
					return false;
			}
		}

		private void cbSimulation_CheckedChanged(object sender, System.EventArgs e)
		{
			if (this.cbSimulation.Checked)
			{
				lastTime = DateTime.Now;
				this.cbSimulation.Text = "&Stop";
				this.cbSimulation.Refresh();
				this.timer1.Start();
			}
			else
			{
				this.cbSimulation.Text = "&Start";
				this.cbSimulation.Refresh();
				this.timer1.Stop();
			}

		}

		private void timer1_Tick(object sender, System.EventArgs e)
		{
			this.Simulate();
		}

	}
}
