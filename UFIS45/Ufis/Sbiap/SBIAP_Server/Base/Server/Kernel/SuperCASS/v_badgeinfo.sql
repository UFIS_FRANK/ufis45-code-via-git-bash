create or replace view V_BADGEINFO as 
SELECT BADGENUMBER as BDNR,TITLE || ' ' || FIRSTNAME || '  ' || LASTNAME as OWNR,concat(ISSUEDATE,STARTTIME) as SDAT,concat(ENDDATE,ENDTIME) as EDAT FROM BADGEINFO;
