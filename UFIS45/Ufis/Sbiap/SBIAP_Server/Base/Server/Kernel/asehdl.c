#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Sbiap/SBIAP_Server/Base/Server/Kernel/asehdl.c 1.7 2005/09/19 20:00:33SGT tho Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :                                                           */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */
static char sccs_version[] ="@(#) UFIS (c) ABB AAT/I asehdl.c 44.2 / "__DATE__" "__TIME__" / VBL";
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "db_if.h"

/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp       = NULL;*/
int  debug_level = TRACE;
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
#define DATABLK_SIZE (256*1024)
#define RC_NODATA -129
#define URNO_SIZE 16
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static int   igInitOK      = FALSE;
static int   igPreserveBC  = 0;                   /* BC memory */
static int   igToBcHdl = 1900;
static char  cgConfigFile[512];
static char  pcgDataArea[DATABLK_SIZE];
static char  pcgSqlBuf[12 * 1024];
static char  cgHopo[8] = "\0";
static char  cgTabEnd[8] ="\0";
static long  lgEvtCnt = 0;
static char  pcgTwStart[128] ;   /* BC-Head used as Stamp */
static char  pcgTwEnd[128] ;     /* BC-Head used as Stamp */
static int   igUseMappingTable;
static int   igUrnoPoolSize;
static char  cgACSSelection[64];
static char  cgInternalStatus[34];
static char  cgFieldList [] = "DSCR,FLOR,NAME,ORIG,STAT,SUBS,TYPE,XCOD,YCOD,LSTU,USEU,URNO";
enum FieldStatus {SOK=0, SNOTOK};
enum BcDataType { INSR,UPDR,DELR };

typedef struct {
  int  needLocation;
  char Subs[34];
  char Orig[34];
  char Dgid[34];
  char Name[52];
  char Dscr[258];
  char Opst[32];
  char Valu[16];
  char Prio[16];
  char Flor[8];
  char Xcod[16];
  char Ycod[16];
  char Addi[66];
  char Stat[34];
} ASEDATA;

/******************************************************************************/
/* Function prototypes                                                          */
/******************************************************************************/
static int Init_Process();
static int    Reset(void);                       /* Reset program          */
static void    Terminate(int);                   /* Terminate program      */
static void    HandleSignal(int);                 /* Handles signals        */
static void    HandleErr(int);                    /* Handles general errors */
static void    HandleQueErr(int);                 /* Handles queuing errors */
static int    HandleData(EVENT *);                  /* Handles event data     */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/
static int getOrPutDBData(uint);
static int mapStatus(ASEDATA *);
static int getLocation(ASEDATA *);
static int performEventAction(char *,char *,int);
static int HandleData_User(BC_HEAD *,CMDBLK *,char *,char *,char *);
static void TrimRight(char *);
static int buildACSSelection(char *,ASEDATA *,char *);
static int updViewerData(ASEDATA *);
static int buildUpdateList(ASEDATA *,char *,char *);
static int buildInsertList(ASEDATA *,char *,char *);
static int sendBroadcast(int,char *);
static int fillData(char *,char *,int ,ASEDATA *);
static int ReadCfg(char *, char *, char *,char *,char *);
static int getFreeUrno(char *);
static int getStatus(void);
extern char *getItem(char *,char *,char *);

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/

MAIN
{
    int    ilRC = RC_SUCCESS;            /* Return code            */
    int    ilCnt = 0;
    
    INITIALIZE;            /* General initialization    */

    dbg(TRACE,"MAIN: version <%s>",sccs_version);
    /* Attach to the MIKE queues */
    do
    {
        ilRC = init_que();
        if(ilRC != RC_SUCCESS)
        {
            dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        }/* end of if */
    }while((ilCnt < 10) && (ilRC != RC_SUCCESS));
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
        sleep(60);
        exit(1);
    }else{
        dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }/* end of if */
    do
      {
	ilRC = init_db();
	if (ilRC != RC_SUCCESS)
	  {
	    check_ret(ilRC);
	    dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
	    sleep(6);
	    ilCnt++;
	  } /* end of if */
      } while((ilCnt < 10) && (ilRC != RC_SUCCESS));

    sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
    ilRC = TransferFile(cgConfigFile);
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */
    ilRC = SendRemoteShutdown(mod_id);
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    } /* end of if */
    if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
        dbg(DEBUG,"MAIN: waiting for status switch ...");
        HandleQueues();
    }/* end of if */
    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
        dbg(TRACE,"MAIN: initializing ...");
        if(igInitOK == FALSE)
        {
	  sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
	  dbg(TRACE,"MAIN: Config File: %s",cgConfigFile);
	  ilRC = Init_Process();
	  if(ilRC != RC_SUCCESS){
	    dbg(TRACE,"Init_Process: init failed!");
	  } /* end of if */
        }/* end of if */
    } else {
        Terminate(30);
    }/* end of if */
    dbg(TRACE,"MAIN: initializing OK");
    for(;;)
    {
        ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text;
                
        if( ilRC == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRC != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRC);
            } /* fi */
            
            switch( prgEvent->command )
            {
            case    HSB_STANDBY    :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;    
            case    HSB_COMING_UP    :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;    
            case    HSB_ACTIVE    :
                ctrl_sta = prgEvent->command;
                break;    
            case    HSB_ACT_TO_SBY    :
                ctrl_sta = prgEvent->command;
                /* CloseConnection(); */
                HandleQueues();
                break;    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate(30);
                break;    
            case    HSB_STANDALONE    :
                ctrl_sta = prgEvent->command;

                break;    

            case    SHUTDOWN    :
                /* process shutdown - maybe from uutil */
                Terminate(30);
                break;
                    
            case    RESET        :
                ilRC = Reset();
                break;
                    
            case    EVENT_DATA    :
                if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
                {
                    ilRC = HandleData(prgEvent);
                    if(ilRC != RC_SUCCESS)
                    {
                        HandleErr(ilRC);
                    }/* end of if */
                }else{
                    dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
                    DebugPrintItem(TRACE,prgItem);
                    DebugPrintEvent(TRACE,prgEvent);
                }/* end of if */
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
            default            :
                dbg(TRACE,"MAIN: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        } else {
            /* Handle queuing errors */
            HandleQueErr(ilRC);
        } /* end else */
        
    } /* end for */
    
    exit(0);
    
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int Init_Process()
{
    int  ilRC = RC_SUCCESS;            /* Return code */
    int  ilValue = 0;
    char pclCfgCode[32];
    char pclCfgValu[32];

    /* now reading from configfile or from database */
    SetSignals(HandleSignal);
    igInitOK = TRUE;
    /*debug_level =0;*/
    /* read HomeAirPort from SGS.TAB */
    ilRC = tool_search_exco_data("SYS", "HOMEAP", cgHopo);
    if (ilRC != RC_SUCCESS){
	dbg(TRACE,"<Init_Process> EXTAB,SYS,HOMEAP not found in SGS.TAB");
	Terminate(30);
      } else {
      dbg(TRACE,"<Init_Process> home airport <%s>",cgHopo);
    }

    if(ilRC == RC_SUCCESS){
      ilRC = tool_search_exco_data("ALL","TABEND", cgTabEnd);
      if (ilRC != RC_SUCCESS){
	dbg(TRACE,"<Init_Process> EXTAB,ALL,TABEND not found in SGS.TAB");
	Terminate(30);
      } else {
	dbg(TRACE,"<Init_Process> table extension <%s>",cgTabEnd);
      }
    }
    strcpy(pclCfgCode,"USE_MAPPING_TABLE");
    ReadCfg(cgConfigFile,pclCfgValu,"MAIN",pclCfgCode,"YES");
    dbg(TRACE,"USE_MAPPING_TABLE = %s (Boolean)",pclCfgValu);
    if (!strcmp(pclCfgValu,"YES")) igUseMappingTable = TRUE;
    else igUseMappingTable = FALSE;
    strcpy(pclCfgCode,"ACS_SELECTION_ITEMS");
    ReadCfg(cgConfigFile,pclCfgValu,"MAIN",pclCfgCode,"NAMES,SUBS,ORIG");
    dbg(TRACE,"ACS_SELECTION_ITEMS = %s (String)",pclCfgValu);
    strcpy(cgACSSelection,pclCfgValu);
    strcpy(pclCfgCode,"URNO_POOLSIZE");
    ReadCfg(cgConfigFile,pclCfgValu,"MAIN",pclCfgCode,"1024");
    dbg(TRACE,"URNO_POOLSIZE = %s (Integer)",pclCfgValu);
    igUrnoPoolSize = atoi(pclCfgValu);
    strcpy(pclCfgCode,"BCHDL_MODID");
    ReadCfg(cgConfigFile,pclCfgValu,"MAIN",pclCfgCode,"1900");
    dbg(TRACE,"BCHDL_MODID = %s (Integer)",pclCfgValu);
    igToBcHdl = atoi(pclCfgValu);
    return(ilRC);
    
} /* end of initialize */

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int    ilRC = RC_SUCCESS;                /* Return code */
    
    dbg(TRACE,"Reset: now resetting");
    
    return ilRC;
    
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
  signal(SIGCHLD,SIG_IGN);
  
  logoff();
  
  dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);
  

  dbg(TRACE,"Terminate: now leaving ...");
  
  fclose(outp);

  sleep(ipSleep < 1 ? 1 : ipSleep);
	
  exit(0);

} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
    int    ilRC = RC_SUCCESS;            /* Return code */
    dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);
    switch(pipSig)
    {
    default    :
        Terminate(1);
        break;
    } /* end of switch */
    exit(0);
    
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
    int    ilRC = RC_SUCCESS;
    return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    int    ilRC = RC_SUCCESS;
    
    switch(pipErr) {
    case    QUE_E_FUNC    :    /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :    /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND    :    /* Error using msgsnd */
        dbg(TRACE,"<%d> : msgsnd failed",pipErr);
        break;
    case    QUE_E_GET    :    /* Error using msgrcv */
        dbg(TRACE,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX    :
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE    :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES    :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP    :    /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL    :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG    :    /* No message on queue */
        dbg(TRACE,"<%d> : no messages on queue",pipErr);
        break;
    case    QUE_E_INVORG    :    /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :    /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    default            :    /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
         
    return;
} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
    int    ilRC = RC_SUCCESS;            /* Return code */
    int    ilBreakOut = FALSE;
    
    do
    {
        ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text;    
        if( ilRC == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRC != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRC);
            } /* fi */
        
            switch( prgEvent->command )
            {
            case    HSB_STANDBY    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_COMING_UP    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_ACTIVE    :
                ctrl_sta = prgEvent->command;
                ilBreakOut = TRUE;
                break;    
            case    HSB_ACT_TO_SBY    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate(30);
                break;    
    
            case    HSB_STANDALONE    :
                ctrl_sta = prgEvent->command;

                ilBreakOut = TRUE;
                break;    

            case    SHUTDOWN    :
                Terminate(30);
                break;
                        
            case    RESET        :
                ilRC = Reset();
                break;
                        
            case    EVENT_DATA    :
                dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
            default            :
                dbg(TRACE,"HandleQueues: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        } else {
            /* Handle queuing errors */
            HandleQueErr(ilRC);
        } /* end else */
    } while (ilBreakOut == FALSE);
    if(igInitOK == FALSE)
    {
            ilRC = Init_Process();
            if(ilRC != RC_SUCCESS)
            {
                dbg(TRACE,"Init_Process: init failed!");
            } /* end of if */
    }/* end of if */
    /* OpenConnection(); */
} /* end of HandleQueues */
    

/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{
  int    ilRC = RC_SUCCESS;            /* Return code */
  char 	  clTable[34];
  BC_HEAD *prlBchead       = NULL;
  CMDBLK  *prlCmdblk       = NULL;
  char    *pclSelection    = NULL;
  char    *pclFields       = NULL;
  char    *pclData         = NULL;
  char    *pclRow          = NULL;

  prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
  prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
  pclSelection = prlCmdblk->data;
  pclFields    = pclSelection + strlen(pclSelection) + 1;
  pclData      = pclFields + strlen(pclFields) + 1;
  
  strcpy(clTable,prlCmdblk->obj_name);
  dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);
  dbg(TRACE, "Cmd <%s> Que (%d) WKS <%s> Usr <%s>", prlCmdblk->command, prgEvent->originator, prlBchead->recv_name, prlBchead->dest_name);
  dbg(TRACE, "Special Info Prio (%d) TWS <%s> TWE <%s>", prgItem->priority, prlCmdblk->tw_start, prlCmdblk->tw_end);
  HandleData_User(prlBchead,prlCmdblk,pclSelection,pclFields,pclData);
  dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);
  return ilRC;
    
} /* end of HandleData */

/******************************************************************************/
/* HandleData_User	                                                      */
/*									      */
/* Handle process specific data						      */
/*									      */
/******************************************************************************/
static int HandleData_User(BC_HEAD *prlBchead,
			   CMDBLK *prlCmdblk,
			   char *pclSelection,
			   char *pclFields,
			   char *pclData)
{
  int  ilRC;
  unsigned int ilSeqNumber;
  char pclTwStart[64];
  char pclTwStartTmp[64];
  char clSelection[1024];

  /* SaveGlobal Info of CMDBLK */
  strcpy (pcgTwStart, prlCmdblk->tw_start) ; 
  strcpy (pcgTwEnd, prlCmdblk->tw_end) ;

  ilSeqNumber = atoi(pcgTwStart);
  /*
  ** trigger received
  ** fetch the next event from the table 
  */
  sprintf(clSelection,"SUBS,ORIG,DGID,NAME,DSCR,STAT,OPST,VALU,PRIO,FLOR,XCOD,YCOD,ADDI");
  sprintf(pcgSqlBuf,"SELECT %s FROM AWKTAB WHERE SEQN='%ld'",clSelection,ilSeqNumber);
  ilRC = getOrPutDBData(START);
  if (ilRC != RC_SUCCESS) {
    dbg(TRACE,"<HandleData_User> Fatal-- no data found for SEQN <%d>",ilSeqNumber);
    return RC_FAIL;
  }
  ilRC = performEventAction(pcgDataArea,clSelection,13);
  sprintf(pcgSqlBuf,"DELETE FROM AWKTAB WHERE SEQN='%ld'",ilSeqNumber);
  ilRC = getOrPutDBData(START|COMMIT);
}
/******************************************************************************/
/* performEventAction                                                         */
/*                                                                            */
/* Work on event data                                                         */
/*                                                                            */
/* Input:                                                                     */
/*       pcpData -- Buffer containing raw eventdata                           */
/*       pcpFields -- Buffer containing related field list                    */
/*                                                                            */
/******************************************************************************/
static int performEventAction(char *pcpData,char *pcpFld,int iplNum)
{
  int  needLocation = FALSE;
  int  ilRC;
  ASEDATA inData;
  char clStat[16];

  ilRC = fillData(pcpData,pcpFld,iplNum,&inData);
  ilRC = mapStatus(&inData);
  if (ilRC != RC_SUCCESS) {
    dbg(TRACE,"<performEventAction> Interface Status not found in ASDTAB");
  }
  if (inData.needLocation) {
    ilRC = getLocation(&inData);
    if (ilRC != RC_SUCCESS) {
      dbg(TRACE,"<performEventAction> No Location Information available for Device");
      dbg(TRACE,"DSCR: <%s>",inData.Dscr);
      dbg(TRACE,"FLOR: <%s> Name: <%s> ORIG: <%s> SUBS: <%s>",inData.Flor,inData.Name,inData.Orig,inData.Subs);
    }
  }
  ilRC = updViewerData(&inData);
}
/******************************************************************************/
/* updViewerData                                                              */
/*                                                                            */
/* fill data into AimsViewers ACSTAB                                          */
/*                                                                            */
/******************************************************************************/
static int updViewerData(ASEDATA *pcpInData)
{
  unsigned int ilStatus;
  int  ilRC;
  char clSelection[128];
  char clValueList[256];
  char clFieldList[256];
  char clDate[18];
  char clUrno[16];

  ilStatus = getStatus();
  ilRC = buildACSSelection(cgACSSelection,pcpInData,clSelection);
  if (ilStatus == SOK){
    sprintf(pcgSqlBuf,"SELECT URNO FROM ACSTAB WHERE %s",clSelection);
    ilRC = getOrPutDBData(START);
    if (ilRC != RC_SUCCESS) {
      dbg(TRACE,"<updViewerData> URNO not found for selection <%s>",clSelection);
      return RC_FAIL;
    }
    else {
      sprintf(clUrno,"%s",pcgDataArea);
    }
    sprintf(pcgSqlBuf,"DELETE FROM ACSTAB WHERE %s",clSelection);
    ilRC = getOrPutDBData(START|COMMIT);
    if (ilRC == RC_SUCCESS){
      sendBroadcast(DELR,clUrno);
    }
  }
  else {
    GetServerTimeStamp("LOC", 1, 0, clDate);
    dbg(DEBUG,"<updViewerData> Date: <%s>",clDate);
    sprintf(pcgSqlBuf,"SELECT URNO FROM ACSTAB WHERE %s",clSelection);
    ilRC = getOrPutDBData(START);
    if (ilRC == RC_NODATA) {
      ilRC = buildInsertList(pcpInData,&clValueList[0],&clUrno[0]);
      sprintf(pcgSqlBuf,"INSERT INTO ACSTAB %s",clValueList);
      dbg(DEBUG,"<buildACSSelection> <%s>",pcgSqlBuf);
      ilRC = getOrPutDBData(START|COMMIT);
      if (ilRC == RC_SUCCESS){
	sprintf(clValueList,"%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,ASEHDL,%s",
		pcpInData->Dscr,pcpInData->Flor,pcpInData->Name,pcpInData->Orig,pcpInData->Stat,
		pcpInData->Subs,pcpInData->Dgid,pcpInData->Xcod,pcpInData->Ycod,clDate,clUrno);
	sendBroadcast(INSR,clValueList);
      }
    }
    else {
      sprintf(clUrno,"%s",pcgDataArea);
      ilRC = buildUpdateList(pcpInData,&clValueList[0],&clDate[0]);
      sprintf(pcgSqlBuf,"UPDATE ACSTAB %s WHERE %s",clValueList,clSelection);
      dbg(DEBUG,"<updViewerData> <%s>",pcgSqlBuf);
      ilRC = getOrPutDBData(START|COMMIT);
      if (ilRC == RC_SUCCESS){
	sprintf(clValueList,"%s,%s,%s,%s,%s,%s,%s",
		pcpInData->Stat,pcpInData->Dscr,clDate,clUrno,
		pcpInData->Xcod,pcpInData->Ycod,pcpInData->Flor);
	sendBroadcast(UPDR,clValueList);
      }
    }
  }
}
/******************************************************************************/
/* sendBroadcast                                                              */
/*                                                                            */
/* Send a broadcast message                                                   */
/*                                                                            */
/******************************************************************************/
static int sendBroadcast(int ipType,char *pcpData)
{
  int ilRC;
  int ilSSize;
  int ilDSize;
  char clCmd[4];
  char clField[128];

  switch (ipType) {
  case INSR:
    sprintf(clCmd,"IRT");
    sprintf(clField,cgFieldList);
    break;
  case UPDR:
    sprintf(clCmd,"URT");
    sprintf(clField,"STAT,DSCR,LSTU,URNO,XCOD,YCOD,FLOR");
    break;
  case DELR:
    sprintf(clCmd,"DRT");
    sprintf(clField,"URNO");
    break;
  default:
    dbg(TRACE,"<sendBroadcast> Unknown Broadcast Type <%d>",ipType);
    break;
  }
  dbg(DEBUG,"<sendBroadcast> Sending Command: <%s> Field: <%s> Data: <%s>",clCmd,clField,pcpData);
  ilRC = tools_send_info_flag(igToBcHdl,0,"ASEHDL","","CEDA","","","AIMS","",clCmd,"ACSTAB","",clField,pcpData,0);
  return ilRC;
}
/******************************************************************************/
/* getStatus                                                                  */
/*                                                                            */
/* Retrieve status and check against a list of OK values                      */ 
/* The only (current) ok status is 'ON'                                       */
/*                                                                            */
/******************************************************************************/
/* static char okStatusList[] = "OK,VALID,ON";*/
static int getStatus(void)
{
  char *ptr;

  ptr = &cgInternalStatus[0];
  while (*ptr != '\0') {
    *ptr = toupper(*ptr);
    ptr++;
  }

  dbg(DEBUG,"<getStatus> Internal Status <%s>",cgInternalStatus);
  if (!strcmp("ON",cgInternalStatus)) {
    dbg(DEBUG,"<getStatus> Resulting status is OK");
    return SOK;
  }
  else return SNOTOK;
}
/******************************************************************************/
/* buildACSSelection                                                          */
/*                                                                            */
/* create WHERE clause for ACS Selection, usable fields are NAME,ORIG,SUBS    */
/* and DGID                                                                   */
/*                                                                            */
/* Input:                                                                     */
/*       cgACSSelection -- configured ACS selection                           */
/*       pcpInData      -- the input data                                     */
/*                                                                            */
/* Output:                                                                    */
/*       clSelection    -- resulting selection                                */
/*                                                                            */
/******************************************************************************/
static int buildACSSelection(char *cgACSSelection,ASEDATA *pcpInData,char *pclSelection)
{
  char *clPtr;
  char clAddon[128];

  clPtr = strstr(cgACSSelection,"DGID");
  sprintf(pclSelection,"NAME='%s' AND SUBS='%s' AND ORIG='%s'",
	  pcpInData->Name,pcpInData->Subs,pcpInData->Orig);
  if (clPtr != NULL) {
    sprintf(clAddon," AND DGID='%s'",pcpInData->Dgid);
    strcat(pclSelection,clAddon);
  }
  dbg(TRACE,"<buildACSSelection> Selection: <%s>",pclSelection);
}
/******************************************************************************/
/* buildInsertList                                                            */
/*                                                                            */
/******************************************************************************/
 static int buildInsertList(ASEDATA *pcpInData,char *clValueList,char *pclUrno)
{
  char clDate[18];
  char clUrno[URNO_SIZE];

  getFreeUrno(clUrno);
  GetServerTimeStamp("LOC", 1, 0, clDate);
  sprintf(clValueList,"(%s) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','ASEHDL','%s')",
	  cgFieldList,pcpInData->Dscr,pcpInData->Flor,pcpInData->Name,pcpInData->Orig,pcpInData->Stat,
	  pcpInData->Subs,pcpInData->Dgid,pcpInData->Xcod,pcpInData->Ycod,clDate,clUrno);
  sprintf(pclUrno,clUrno);
}
/******************************************************************************/
/* buildUpdateList                                                            */
/******************************************************************************/
 static int buildUpdateList(ASEDATA *pcpInData,char *clValueList,char *pclDate)
{
  /*GetServerTimeStamp("LOC", 1, 0, clDate); */
  sprintf(clValueList,"SET STAT='%s',DSCR='%s',LSTU='%s',XCOD='%s',YCOD='%s',FLOR='%s'",
	  pcpInData->Stat,pcpInData->Dscr,pclDate,pcpInData->Xcod,
	  pcpInData->Ycod,pcpInData->Flor);
}
/******************************************************************************/
/* fillData                                                                   */
/*                                                                            */
/* fill appropriate data from ASETAB into working structure                   */
/*                                                                            */
/* Input:                                                                     */
/*        pcpData -- raw data from SQL selection                              */
/*        pcpFld  -- list of fields in SQL selection                          */
/*        iplNum  -- number of items in SQL selection                         */
/*                                                                            */
/* Output:                                                                    */
/*        inData  -- struct is filled with appropriate data                   */                      
/*                                                                            */
/******************************************************************************/
static int fillData(char *pcpData,char *pcpFld,int iplNum,ASEDATA *inData)
{
  char *pclItem;

  inData->needLocation = FALSE;
  BuildItemBuffer(pcpData, NULL, iplNum, ",");
  pclItem = getItem(pcpFld,pcpData,"SUBS");
  if( pclItem != NULL ) {
    strcpy(inData->Subs,pclItem);
    TrimRight(inData->Subs);
  }
  else {
    dbg(DEBUG,"<performEventAction> No SUBS info");
    sprintf(inData->Subs,"");
  }
  pclItem = getItem(pcpFld,pcpData,"ORIG");
  if( pclItem != NULL ) {
    strcpy(inData->Orig,pclItem);
    TrimRight(inData->Orig);
  }
  else {
    dbg(DEBUG,"<performEventAction> No ORIG info");
    sprintf(inData->Orig,"");
  }
  pclItem = getItem(pcpFld,pcpData,"DGID");
  if( pclItem != NULL ) {
    strcpy(inData->Dgid,pclItem);
    TrimRight(inData->Dgid);
  }
  else {
    dbg(DEBUG,"<performEventAction> No DGID info");
    sprintf(inData->Dgid,"");
  }
  pclItem = getItem(pcpFld,pcpData,"NAME");
  if( pclItem != NULL ) {
    strcpy(inData->Name,pclItem);
    TrimRight(inData->Name);
  }
  else {
    dbg(DEBUG,"<performEventAction> No NAME info");
    sprintf(inData->Name,"");
  }
  pclItem = getItem(pcpFld,pcpData,"DSCR");
  if( pclItem != NULL ) {
    strcpy(inData->Dscr,pclItem);
    TrimRight(inData->Dscr);
  }
  else {
    dbg(DEBUG,"<performEventAction> No DSCR info");
    sprintf(inData->Dscr,"");
  }
  pclItem = getItem(pcpFld,pcpData,"STAT");
  if( pclItem != NULL ) {
    strcpy(inData->Stat,pclItem);
    TrimRight(inData->Stat);
  }
  else {
    dbg(DEBUG,"<performEventAction> No STAT info");
    sprintf(inData->Stat,"");
  }
  pclItem = getItem(pcpFld,pcpData,"OPST");
  if( pclItem != NULL ) {
    strcpy(inData->Opst,pclItem);
    TrimRight(inData->Opst);
  }
  else {
    dbg(DEBUG,"<performEventAction> No OPST info");
    sprintf(inData->Opst,"");
  }
  pclItem = getItem(pcpFld,pcpData,"VALU");
  if( pclItem != NULL ) {
    strcpy(inData->Valu,pclItem);
    TrimRight(inData->Valu);
  }
  else {
    dbg(DEBUG,"<performEventAction> No VALU info");
    sprintf(inData->Valu,"");
  }
  pclItem = getItem(pcpFld,pcpData,"PRIO");
  if( pclItem != NULL ) {
    strcpy(inData->Prio,pclItem);
    TrimRight(inData->Prio);
  }
  else {
    dbg(DEBUG,"<performEventAction> No PRIO info");
    sprintf(inData->Prio,"");
  }
  pclItem = getItem(pcpFld,pcpData,"FLOR");
  if( pclItem != NULL ) {
    TrimRight(pclItem);
    strcpy(inData->Flor,pclItem);
  }
  else {
    dbg(DEBUG,"<performEventAction> No FLOR info");
    sprintf(inData->Flor,"");
  }
  pclItem = getItem(pcpFld,pcpData,"XCOD");
  if( pclItem != NULL ) {
    strcpy(inData->Xcod,pclItem);
    TrimRight(inData->Xcod);
    if (!strcmp(inData->Xcod, " ")) inData->needLocation = TRUE;
  }
  else {
    dbg(DEBUG,"<performEventAction> No XCOD info");
    sprintf(inData->Xcod,"");
    inData->needLocation = TRUE;
  }
  pclItem = getItem(pcpFld,pcpData,"YCOD");
  if( pclItem != NULL ) {
    strcpy(inData->Ycod,pclItem);
    TrimRight(inData->Ycod);
    if (!strcmp(inData->Ycod, " ")) inData->needLocation = TRUE;
  }
  else {
    dbg(DEBUG,"<performEventAction> No YCOD info");
    sprintf(inData->Ycod,"");
    inData->needLocation = TRUE;
  }
  pclItem = getItem(pcpFld,pcpData,"ADDI");
  if( pclItem != NULL ) {
    strcpy(inData->Addi,pclItem);
    TrimRight(inData->Addi);
  }
  else {
    dbg(DEBUG,"<performEventAction> No ADDI info");
    sprintf(inData->Addi,"");
  }
} 
/******************************************************************************/
/* getLocation                                                                */
/*                                                                            */
/* get missing location info from internal ADCTAB and insert XCOD/YCOD        */
/* for the requested device                                                   */
/*                                                                            */
/* Input:                                                                     */
/*       clDscr,clFlor,clName,clOrig,clSubs -- unique system descriptor       */
/* Output:                                                                    */
/*       clXcod,clYCod -- retrieved coordinates from ADCTAB                   */
/* Return:                                                                    */
/*       RC_SUCCESS -- successfully inserted                                  */
/*       else       -- data not available                                     */
/*                                                                            */
/******************************************************************************/
static int getLocation(ASEDATA *pclData)
{
  int ilRC;
  char pclField[64];
  char *pclItem;

  sprintf(pclField,"XCOD,YCOD,FLOR");
  sprintf(pcgSqlBuf,"SELECT %s FROM ADCTAB WHERE DGID='%s' AND NAME='%s' AND ORIG='%s' AND SUBS='%s'",
	  pclField,pclData->Dgid,pclData->Name,pclData->Orig,pclData->Subs);
  dbg(DEBUG,"<getLocation> <%s>",pcgSqlBuf);
  ilRC = getOrPutDBData(START);
  if (ilRC == RC_SUCCESS) {
    BuildItemBuffer(pcgDataArea, NULL, 3, ",");
    pclItem = getItem(pclField,pcgDataArea,"XCOD");
    if( pclItem != NULL ) {
      strcpy(pclData->Xcod,pclItem);
    }
    pclItem = getItem(pclField,pcgDataArea,"YCOD");
    if( pclItem != NULL ) {
      strcpy(pclData->Ycod,pclItem);
    }
    pclItem = getItem(pclField,pcgDataArea,"FLOR");
    if( pclItem != NULL ) {
      strcpy(pclData->Flor,pclItem);
    }
  }
  else {
    dbg(DEBUG,"<getLocation> No Entry: <%s>",pcgSqlBuf);
    return RC_FAIL;
  }
}
/******************************************************************************/
/* mapStatus                                                                  */
/*                                                                            */
/* map interface status to internal status (mapping is taken from ASDTAB)     */
/* write internal status to global cgInternalStatus for status checking       */
/*                                                                            */
/* Input:                                                                     */
/*       pclStatus -- Status from Interface                                   */
/* Output:                                                                    */
/*       pclStatus -- related ASDTAB status                                   */
/*                                                                            */
/* return:                                                                    */
/*         RC_SUCCESS -- on succesful mapping                                 */
/*         RC_FAIL    -- else                                                 */
/******************************************************************************/
static int mapStatus(ASEDATA *pclData)
{
  int ilRC;

  sprintf(pcgSqlBuf,"SELECT STAT FROM ASDTAB WHERE IFCO='%s' AND SGRP=(SELECT URNO FROM ASGTAB WHERE SUBS='%s' AND ORIG='%s' AND TYPE='%s')",
	  pclData->Stat,pclData->Subs,pclData->Orig,pclData->Dgid);
  ilRC = getOrPutDBData(START);
  if (ilRC == RC_SUCCESS) {
    strcpy(cgInternalStatus,pcgDataArea);
    TrimRight(cgInternalStatus);
  }
  else {
    strcpy(cgInternalStatus,"UNKNOWN");
  }
  if (igUseMappingTable == TRUE) strcpy(pclData->Stat,cgInternalStatus);
  return ilRC;
}
/******************************************************************************/
/* getOrPutDBData                                                             */
/*                                                                            */
/* Fetch data as described in the global pcgSqlBuf and return the result in   */
/* global buffer pcgDataArea.                                                 */
/*                                                                            */
/* Input:                                                                     */
/*        ipMode -- function mode mask like START|COMMIT                      */
/*                                                                            */
/******************************************************************************/
static int getOrPutDBData(uint ipMode)
{
  int ilGetRc = DB_SUCCESS;
  short slFkt = 0;
  short slCursor = 0;
  char errBuff[128];

  slFkt = ipMode;
  slCursor = 0;
  ilGetRc = sql_if (slFkt, &slCursor, pcgSqlBuf, pcgDataArea);
  close_my_cursor (&slCursor);
  if (ilGetRc == DB_SUCCESS) {
    return RC_SUCCESS;
  }
  else {
    if (ilGetRc != NOTFOUND) {
      get_ora_err(ilGetRc, &errBuff[0]);
      dbg (TRACE, "<getOrPutDBData> Error getting Oracle data error <%s>", &errBuff[0]);
      return RC_FAIL;
    }
    else  return RC_NODATA;
  }
}
/******************************************************************************/
/* getFreeUrno                                                                */
/*                                                                            */
/* Maintains a pool of URNOS. If no more URNOS are available, the pool is     */
/* refilled with a set of URNO_POOL_SIZE.                                     */
/*                                                                            */
/* Input:                                                                     */
/*       Nothing                                                              */
/*                                                                            */
/* Return:                                                                    */
/*       cpUrno -- the buffer pointed to is filled with a new URNO            */
/*       RC_SUCCESS/RC_FAIL                                                   */
/*                                                                            */
/******************************************************************************/
static int getFreeUrno(char *cpUrno)
{
  static uint ilNumUrnos = 0;
  static uint ilFirstUrno = 0;
  static uint ilNextUrno = 0;
  char clNewUrno[URNO_SIZE];
  int ilRc;

  if (ilNumUrnos == 0) {
    ilRc = GetNextValues (clNewUrno, igUrnoPoolSize);
    if (ilRc == RC_SUCCESS) {
      ilFirstUrno = atoi(clNewUrno);
      ilNextUrno = ilFirstUrno;
      ilNumUrnos = igUrnoPoolSize;
    }
    else return RC_FAIL;
  }
  sprintf(cpUrno, "%d", ilNextUrno);
  ilNextUrno++;
  ilNumUrnos--;
  dbg(DEBUG,"<getFreeUrno> URNO First:<%d> Next:<%d> Num:<%d>", 
      ilFirstUrno, ilNextUrno, ilNumUrnos);
  return RC_SUCCESS;
}
/******************************************************************************/
/* TrimRight                                                                  */
/*                                                                            */
/* remove all appended ' ' from a string                                      */
/*                                                                            */
/* Input:                                                                     */
/*       pcpBuffer                                                            */
/*                                                                            */
/* Output:                                                                    */
/*       pcpBuffer -- stripped ' '                                            */
/*                                                                            */
/******************************************************************************/
static void TrimRight(char *pcpBuffer)
{
  char *pclBlank = &pcpBuffer[strlen(pcpBuffer)-1];

  if (strlen(pcpBuffer) == 0){
    strcpy(pcpBuffer," ");
  }
  else {
    while (isspace(*pclBlank) && pclBlank != pcpBuffer){
      *pclBlank = '\0';
      pclBlank--;
    }
  }
} /* End of TrimRight */
/******************************************************************************/
/******************************************************************************/
static int ReadCfg(char *pcpFile, char *pcpVar, char *pcpSection, 
	char *pcpEntry, char *pcpDefVar)
{
	int ilRc = RC_SUCCESS;
	ilRc = iGetConfigEntry(pcpFile,pcpSection,pcpEntry,1,pcpVar);
	if( ilRc != RC_SUCCESS || strlen(pcpVar) <= 0 )
	{
		strcpy(pcpVar,pcpDefVar); /* copy the default value */
		ilRc = RC_FAIL;
	}
	return ilRc;
} /* end ReadCfg() */

/******************************************************************************/
/* Space for Appendings (Source-Code Control)                                 */
/******************************************************************************/
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
