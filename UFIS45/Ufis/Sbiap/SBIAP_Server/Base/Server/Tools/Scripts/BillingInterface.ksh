# set -u
###
###  #     # #######   ###    #####             #     #####
###  #     # #          #    #     #           # #   #     #
###  #     # #          #    #                #   #  #
###  #     # #####      #     #####   #####  #     #  #####
###  #     # #          #          #         #######       #
###  #     # #          #    #     #         #     # #     #
###   #####  #         ###    #####          #     #  #####
###
### $Id: Ufis/Sbiap/SBIAP_Server/Base/Server/Tools/Scripts/BillingInterface.ksh 1.1 2006/06/12 17:31:13SGT jim Exp  $
### (c) UFIS - AS GmbH 2006
###
### Script to read a text file with records of flight data, collect
### the load data of the flights via EQIHDL and QPUT and append the
### load data to the records writing to a new file
### 
### Note: There is one SHELL printf to write the beginning of the
###       output line, and one AWK printf to complete that line.
###

# For error messages:
THIS=`basename $0 .ksh`

# set Standard Ufis Environment
CFG_PATH=/ceda/conf
ETC_PATH=/ceda/etc

# Get Configuration of fields, dirs and file names:
# the grep removes comments from config file, the sed removes blanks
# around "=", which is not valid in shells:
eval `grep "^[a-z,A-Z,0-9]" ${CFG_PATH}/${THIS}.cfg | sed 's/ *=/=/;s/= */=/'"`

# evaluate any beginning of parameter 'IN_REMOVE' with Y/y/J/j to YES:
case "${IN_REMOVE}" in
   Y*|y*|J*|j*) IN_REMOVE=YES
                ;;
esac

# Now do the work:
# change to dir:
cd ${IN_DIR}
if [ ! $? ] ; then
  echo "${THIS} :Error: Changing to working dir '${IN_DIR}' failed! Terminating...."
  return 2>/dev/null
  exit
fi

echo "${THIS} : starting work `date` ...."
for file in ${IN_FILE}*${IN_EXT} ; do
  # may be, there is no file:
  if [ -f ${file} ] ; then
    echo "${THIS} : processing '${file}' ...."
    TMPFILE=`echo ${file} | sed 's/'${IN_FILE}'/tempsap_/`
    OUTFILE=`echo ${file} | sed 's/'${IN_FILE}'/'${OUT_FILE}'/`
    WRKFILE=${file}${IN_PROGRESS}
    mv ${file} ${WRKFILE}
    #echo ${TMPFILE}
    #echo ${OUTFILE}
    if [ -f ${TMPFILE} ] ; then 
      rm ${TMPFILE}
    fi
    if [ -f ${TMPFILE} ] ; then 
      echo "${THIS}: Error: removing '${TMPFILE}' failed! Terminating...."
      return 2>/dev/null
      exit
    fi
    # first awk removes "^M", WHILE reads line by line:
    # (if input file does not contain ^M use "cat ${WRKFILE} | ..." 
    #  instead of "awk ... ${WRKFILE} | ... " )
    awk '{ppp=length($0) ; print substr($0,1,ppp-1)}' ${WRKFILE} | while read line ; do
      # use shell printf No 1 to write old line without linefeed:
      printf "%s" ${line} >> ${TMPFILE}
      URNO=`echo ${line} | cut -c6-20 | awk '{pp=index($1,"#") ; print substr($1,1,pp-1)}'`
      #echo $URNO
      #echo 'qput eqihdl GLED tab=" " dat=" " fld='${AddFields}' sel='${URNO}' | grep ","'

      # get the data, comma seperated, via qput from eqihdl:
      AddData=`${ETC_PATH}/qput eqihdl GLED tab=" " dat=" " fld=${AddFields} sel=${URNO} | grep ","`

      # and remix "Fieldlist\nDatalist" to "field_1#data_1#field_2#data_2#...", append
      # the result to the line from printf No 1
      ( echo ${AddFields} ; echo ${AddData} ) | awk '
         {
           nn=split($0,ff,",")
           getline
           mm=split($0,dd,",")
           for (ii=1 ; ii <= nn ; ii++)
           {
              printf "%s#%s#",ff[ii],dd[ii]
           }
           # if no ^M is needed in output file, use printf "\n" instead:
           printf "%c\n",13
      }' >>${TMPFILE}
    done

    # output file processed, now it is ready for FTP, so rename it
    mv ${TMPFILE} ${OUT_DIR}/${OUTFILE}

    if [ "${IN_REMOVE}" = "YES" ] ; then
      # don't want to use this file again, so delete it
      echo "${THIS} : removing '${WRKFILE}' ...."
      rm ${WRKFILE}
    else
      if [ ! -z "${IN_APPEND}" ] ; then
        # don't want to use this file again, so rename it
        echo "${THIS} : moving '${WRKFILE}' to ${file}${IN_APPEND} ...."
        mv ${WRKFILE} ${file}${IN_APPEND}
      fi
    fi
  fi
  ${ETC_PATH}/qput excmgr dst=FILEIF rcv=EXCO cmd=ACK twe=BKK,TAB,FILEIF,TRUE \
       sel=${OUT_DIR}/${OUTFILE} \
       fld=" " dat=" " tab=" " wait=-1 tws=""
done
echo "${THIS} : finnished work `date` ...."
